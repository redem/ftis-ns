/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_Pack_ScanQty.js,v $
 *     	   $Revision: 1.1.2.7.4.6.4.26.2.1 $
 *     	   $Date: 2015/11/13 15:32:14 $
 *     	   $Author: schepuri $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Pack_ScanQty.js,v $
 * Revision 1.1.2.7.4.6.4.26.2.1  2015/11/13 15:32:14  schepuri
 * case# 201415516
 *
 * Revision 1.1.2.7.4.6.4.26  2015/03/05 13:55:12  schepuri
 * issue fix # 201411610
 *
 * Revision 1.1.2.7.4.6.4.25  2014/09/26 15:07:01  sponnaganti
 * Case# 201410524
 * Stnd Bundle Issue fix
 *
 * Revision 1.1.2.7.4.6.4.24  2014/07/17 15:51:32  skavuri
 * Case # 20149510 SB Issue Fixed
 *
 * Revision 1.1.2.7.4.6.4.23  2014/07/10 07:26:09  skavuri
 * Case# 20148367 Compatibility Issue Fixed
 *
 * Revision 1.1.2.7.4.6.4.22  2014/07/04 14:28:04  skavuri
 * Case # 20148702 Compatibility Issue Fixed
 *
 * Revision 1.1.2.7.4.6.4.21  2014/06/27 07:48:46  skavuri
 * Case# 20149108 Compatability Issue Fixed
 *
 * Revision 1.1.2.7.4.6.4.20  2014/06/13 12:55:27  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.7.4.6.4.19  2014/06/12 14:48:20  grao
 * Case#: 20148846  New GUI account issue fixes
 *
 * Revision 1.1.2.7.4.6.4.18  2014/06/04 15:21:52  skavuri
 * Case# 20148702 SB Issue Fixed
 *
 * Revision 1.1.2.7.4.6.4.17  2014/05/30 00:41:02  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.7.4.6.4.16  2014/05/27 13:57:22  snimmakayala
 * Case#: 20148567
 * RF Packing issue fixes
 *
 * Revision 1.1.2.7.4.6.4.15  2014/04/15 15:48:32  nneelam
 * case#  20147993
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.7.4.6.4.14  2014/02/26 14:16:00  sponnaganti
 * case# 20127204
 * (cheking for entered item is valid or not)
 *
 * Revision 1.1.2.7.4.6.4.13  2014/02/25 15:37:12  nneelam
 * case#  20127242
 * Standard Bundle Issue Fix.
 *
 * Revision 1.1.2.7.4.6.4.12  2014/01/06 13:19:17  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.1.2.7.4.6.4.11  2013/12/10 14:28:46  snimmakayala
 * Case# : 20126177
 * MHP UAT Fixes.
 *
 * Revision 1.1.2.7.4.6.4.10  2013/12/02 08:58:26  schepuri
 * 20125974
 *
 * Revision 1.1.2.7.4.6.4.9  2013/11/14 15:42:27  skreddy
 * Case# 20125642
 * Afosa SB issue fix
 *
 * Revision 1.1.2.7.4.6.4.8  2013/11/12 06:40:15  skreddy
 * Case# 20125642
 * Afosa SB issue fix
 *
 * Revision 1.1.2.7.4.6.4.7  2013/10/01 16:08:10  rmukkera
 * Case# 20124698
 *
 * Revision 1.1.2.7.4.6.4.6  2013/08/20 15:56:55  grao
 * RF packing item screen repeated while qty is zero related issue fixes  20123935
 *
 * Revision 1.1.2.7.4.6.4.5  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.2.7.4.6.4.4  2013/06/04 15:31:28  skreddy
 * CASE201112/CR201113/LOG201121
 * RF packing issue for CWD
 *
 * Revision 1.1.2.7.4.6.4.3  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.7.4.6.4.2  2013/04/04 16:17:47  skreddy
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.1.2.7.4.6.4.1  2013/04/03 02:24:45  kavitha
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.1.2.7.4.6  2013/01/02 15:42:57  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Double Click restriction.
 * .
 *
 * Revision 1.1.2.7.4.5  2012/12/31 05:52:23  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Restrict Packing for Closed and Paymeny Hold Orders.
 *
 * Revision 1.1.2.7.4.4  2012/12/24 13:20:11  schepuri
 * CASE201112/CR201113/LOG201121
 * issue related to invalid cont lp
 *
 * Revision 1.1.2.7.4.3  2012/11/01 14:55:23  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.1.2.7.4.2  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.1.2.7.4.1  2012/09/25 07:13:53  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Language
 *
 * Revision 1.1.2.7  2012/09/03 13:52:07  schepuri
 * CASE201112/CR201113/LOG201121
 * added date stamp
 *
 * Revision 1.1.2.6  2012/07/27 12:58:26  schepuri
 * CASE201112/CR201113/LOG201121
 * Issue in close carton
 *
 * Revision 1.1.2.5  2012/07/04 09:57:44  gkalla
 * CASE201112/CR201113/LOG201121
 * changed the qty value sending to close carton function
 *
 * Revision 1.1.2.4  2012/06/28 08:32:25  schepuri
 * CASE201112/CR201113/LOG201121
 * packing issue fix
 *
 * Revision 1.1.2.3  2012/06/15 16:26:49  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Packing changes
 *
 * Revision 1.1.2.2  2012/06/15 10:08:49  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Packing changes
 *
 * Revision 1.1.2.1  2012/06/06 07:39:03  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Packing
 *
 * Revision 1.32.4.21  2012/05/14 14:37:58  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to validating LP
 *
 * Revision 1.32.4.20  2012/05/10 15:00:34  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to clicking previous screen was resolved.
 *
 * Revision 1.32.4.19  2012/05/09 15:58:22  spendyala
 * CASE201112/CR201113/LOG201121
 * Passing Trantype in query string was missing
 *
 * Revision 1.32.4.18  2012/04/27 07:25:07  gkalla
 * CASE201112/CR201113/LOG201121
 * QC check based on rule configuration
 *
 * Revision 1.32.4.17  2012/04/11 22:02:00  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related navigating to TO Item Status when the user click on Previous button.
 *
 * Revision 1.32.4.16  2012/04/10 22:59:15  spendyala
 * CASE201112/CR201113/LOG201121
 * issue related navigating to previous screen is resolved.
 *
 * Revision 1.32.4.15  2012/03/21 11:08:48  schepuri
 * CASE201112/CR201113/LOG201121
 * Added movetask
 *
 * Revision 1.32.4.14  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.32.4.13  2012/03/06 11:00:05  spendyala
 * CASE201112/CR201113/LOG201121
 * In Fetching StageLocation and DockLocation added few more Criteria's to get the records.
 *
 * Revision 1.32.4.12  2012/03/05 14:42:30  spendyala
 * CASE201112/CR201113/LOG201121
 * In Fetching StageLocation and DockLocation added few more Criteria's to get the records.
 *
 * Revision 1.32.4.11  2012/03/02 01:25:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.43  2012/03/02 01:11:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.42  2012/02/21 07:36:44  spendyala
 * CASE201112/CR201113/LOG201121
 * code merged upto 1.32.4.9
 *
 * Revision 1.41  2012/02/14 07:13:29  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged upto 1.32.4.8.
 *
 * Revision 1.40  2012/02/13 13:39:02  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 * Revision 1.39  2012/02/12 12:26:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Checkin MultiUOM issues
 *
 * Revision 1.38  2012/02/10 10:25:15  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fix is merged upto  1.32.4.6.
 *
 * Revision 1.37  2012/02/07 07:24:38  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fix is merged upto  1.32.4.4.
 *
 * Revision 1.36  2012/02/01 12:53:08  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fix is merged upto  1.32.4.3.
 *
 * Revision 1.35  2012/02/01 06:30:36  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fix is merged from  1.32.4.2
 *
 * Revision 1.34  2012/01/19 00:43:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Changes
 *
 * Revision 1.33  2012/01/09 13:11:22  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Issue if there a space in batch no
 *
 * Revision 1.32  2012/01/06 13:03:09  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing, redirecting to serialno entry
 *
 * Revision 1.31  2012/01/04 20:18:38  gkalla
 * CASE201112/CR201113/LOG201121
 * To add Item to Batch entry
 *
 * Revision 1.30  2012/01/04 15:08:45  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.29  2011/12/30 14:09:47  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.28  2011/12/27 23:24:07  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Merge LP Fixes
 *
 * Revision 1.27  2011/12/23 23:36:41  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.26  2011/12/23 16:21:51  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.25  2011/12/23 13:24:07  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.24  2011/12/23 13:19:38  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.23  2011/12/19 14:59:34  snimmakayala
 * CASE201112/CR201113/LOG201121
 * FIFO in RF Receiving.
 *
 * Revision 1.22  2011/12/15 14:10:40  schepuri
 * CASE201112/CR201113/LOG201121
 * modified field names in customrecord_ebiznet_trn_poreceipt
 *
 * Revision 1.21  2011/12/02 13:19:35  schepuri
 * CASE201112/CR201113/LOG201121
 * RF To generate location acc to the location defined in PO
 *
 * Revision 1.20  2011/11/17 08:44:28  spendyala
 * CASE201112/CR201113/LOG201121
 * wms status flag for create inventory is changed to 17 i .e, FLAG INVENTORY INBOUND
 *
 * Revision 1.19  2011/09/29 12:04:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.18  2011/09/28 16:08:26  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.17  2011/09/26 19:58:36  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.16  2011/09/24 19:47:31  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.14  2011/09/14 05:27:01  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.13  2011/09/12 08:24:00  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.12  2011/09/10 11:28:23  schepuri
 * CASE201112/CR201113/LOG201121
 *
 * redirecting to customscript_serial_no
 *
 * Revision 1.11  2011/07/05 12:36:44  schepuri
 * CASE201112/CR201113/LOG201121
 * RF CheckIN changes
 *
 * Revision 1.10  2011/06/30 13:06:55  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.9  2011/06/17 07:39:47  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * Confirm putaway changes
 *
 * Revision 1.8  2011/06/14 07:45:49  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.7  2011/06/06 14:10:04  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 * In Check-in, Remaining cube calculation and putaway logic.
 *
 * Revision 1.6  2011/06/06 07:10:13  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * WBC Issues
 *
 * Revision 1.5  2011/06/03 06:29:22  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 *
 * Revision 1.4  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.3  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/
function Packing_ScanQty(request, response)
{

	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);

	if (request.getMethod() == 'GET') 
	{   
		var getOrderno = request.getParameter('custparam_orderno');
		var getContlpno=request.getParameter('custparam_containerno');		
		var getContainerLpNo = request.getParameter('custparam_fetchedcontainerlp');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getActualQty = request.getParameter('custparam_actualquantity');
		nlapiLogExecution('DEBUG','getActualQty',getActualQty);
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var vcontlp = request.getParameter('custparam_containerno');
		var vcontsize = request.getParameter('custparam_containersize');
		var vcontsizevalue=request.getParameter('custparam_containersizevalue');	
		var vlineCount = request.getParameter('custparam_linecount');
		var vloopCount = request.getParameter('custparam_loopcount');
		var waveno=request.getParameter('custparam_waveno');
		var wmslocation=request.getParameter('custparam_wmslocation');

		var bulkqty=request.getParameter('custparam_bulkqty');

		var getNumber;
		if(request.getParameter('custparam_number')!= null && request.getParameter('custparam_number') != "")
			getNumber = request.getParameter('custparam_number');
		else
			getNumber=0;

		nlapiLogExecution('DEBUG', 'getItemInternalId', getItemInternalId);
		var fields = ['recordType', 'salesdescription'];

		var getItemDesc ='';
		var getItem='';

		if(getItemInternalId!=null && getItemInternalId!='' && getItemInternalId!='null')
		{
			var columns = nlapiLookupField('item', getItemInternalId, fields);


			//var columns = nlapiLookupField('item', getItemInternalId, fields);
			var Itype = columns.recordType;					
			getItemDesc = columns.salesdescription;
			var ItemRec = nlapiLoadRecord(Itype, getItemInternalId);

			getItem = ItemRec.getFieldValue('itemid');
			nlapiLogExecution('DEBUG', 'getItem', getItem);
		}

		var fetchedcontainerlp =request.getParameter('custparam_fetchedcontainerlp');

		var getLanguage = request.getParameter('custparam_language');	
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
		//case 20125642 start: spanish conversion,added "es_AR"
		var st0,st1,st2,st3,st4,st5,st6,st7,st8,st9,st10,st11,st12;
		// case 20125974
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			//case 20125642 end
			st0 = "PAQUETE DE SCAN CANT";
			st1 = "PEDIDO #";
			st2 = "CAJA #";
			st3 = "ART&#205;CULO";
			st4 = "CANT ANALIZAR";
			st5 = "ENTRAR ART&#205;CULO:";
			st6 = "ENTRAR CANT :";
			st7 = "ENVIAR";
			st8 = "PR&#211;XIMO";
			st9 = "ANTERIOR";
			st10 = "DESCRIPCI&#211;N";
			st11 = "CAJA CERRADO";
			st12 = "TAMA&#209;O";
			st13 = "CANTIDAD:";
		}
		else
		{
			st0 = "PACK SCAN QTY";
			st1 = "ORDER#";
			st2 = "CARTON#";
			st3 = "ITEM";
			st4 = "SCANNED QTY";
			st5 = "ENTER ITEM:";
			st6 = "ENTER QTY:";
			st7 = "SEND";
			st8 = "NEXT";
			st9 = "PREV";
			st10 = "DESCRIPTION";
			st11 = "CLOSE CARTON";
			st12 = "SIZE ";
			st13 = "QTY :";
		}



		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 	
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";   
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";

		//html = html + " document.getElementById('enterqty').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + ": <label>" + getOrderno + "</label>";	
		html = html + "				</td>";
		html = html + "			</tr>";  
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + ": <label>" + getContlpno + "</label>";		

		html = html + "				"+st12+": <label>" + vcontsize + "</label>";		
		html = html + "				</td>";
		html = html + "			</tr>";  
//		html = html + "			<tr>";
//		html = html + "				<td align = 'left'>CARTON SIZE: <label>" + vcontsize + "</label>";		
//		html = html + "				</td>";
//		html = html + "			</tr>";  
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 +": <label>" + getItem + "</label>";		
//		html = html + "				</td>";
		html = html + "				"+st13+" <label>" + getActualQty + "</label>";
		html = html + "			</tr>"; 
		html = html + "			<tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st10 +": <label>" + getItemDesc + "</label>";	
		html = html + "				</td>";
		html = html + "			</tr>"; 
		html = html + "				<input type='hidden' name='hdnOrderNo' value=" + getOrderno + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContlpno + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnActualQuantity' value=" + getActualQty + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnlinecount' value=" + vlineCount + ">";
		html = html + "				<input type='hidden' name='hdnloopcount' value=" + vloopCount + ">";
		html = html + "				<input type='hidden' name='hdncontlpno' value=" + getContlpno + ">";
		html = html + "				<input type='hidden' name='hdnwmslocation' value=" + wmslocation + ">";
		html = html + "				<input type='hidden' name='hdncontainervalue' value=" + vcontsizevalue + ">";
		html = html + "				<input type='hidden' name='hdnbulkqty' value=" + bulkqty + ">";
		html = html + "				<input type='hidden' name='hdnFetchedContainerLPNo' value=" + fetchedcontainerlp + ">";
		html = html + "				<input type='hidden' name='hdngetnumber' value=" + getNumber + ">";
		html = html + "				<input type='hidden' name='hdnwaveno' value=" + waveno + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdnclosecarton'>";
		html = html + "				<input type='hidden' name='hdnsubmit'>";
		html = html + "				<input type='hidden' name='hdnprevious'>";
		html = html + "			</tr>";
		if(bulkqty=='')
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st4 + ": <label>" + (parseFloat(getNumber)) + "</label>";

			html = html + "			</tr>";
			/*html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st5;	
			html = html + "				<input name='enteritem' type='text'/>";
			html = html + "				</td>";
			html = html + "			</tr>";  */
		}
		else
		{
			/*html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st5;	
			html = html + "				<input name='enteritem' type='text'/>";
			html = html + "				</td>";
			html = html + "			</tr>"; */
		}
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st5;	
		html = html + "				<input name='enteritem' id='enteritem'  type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>"; 

//		html = html + "			<tr>";
//		html = html + "				<td align = 'left'><input name='enteritem' type='text'/>";
//		html = html + "				</td>";
//		html = html + "			</tr>";	
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st6;		
//		html = html + "				</td>";
		html = html + "				<input name='enterqty' id='enterqty' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>"; 		

		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		if(bulkqty=='')
		{
			html = html + "              " + st7 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.hdnsubmit.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdclosecarton.disabled=true;return false'/>";
		}
		//html = html + "				  " + st8 + " <input name='cmdEXIT' type='submit' value='F6'/>";
		html = html + "				 " + st9 + " <input name='cmdPrevious' type='submit' value='F7' onclick='this.form.hdnprevious.value=this.value;this.form.submit();this.disabled=true;this.form.cmdclosecarton.disabled=true;this.form.cmdSend.disabled=true;return false'/> </br>";
		html = html + "				"+st11+" <input name='cmdclosecarton' type='submit' value='F8' onclick='this.form.hdnclosecarton.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdSend.disabled=true;return false'/>";        
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enteritem').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var varQty = request.getParameter('enterqty');
		var varitem = request.getParameter('enteritem');
		var getNumber = request.getParameter('hdngetnumber');
		if(bulkqty=='')
			varQty=getNumber;

		nlapiLogExecution('DEBUG', 'Entered Qty', varQty);        

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('hdnprevious');    
		var optENT = request.getParameter('hdnsubmit');
		var optclose = request.getParameter('hdnclosecarton');

		var OrderNo = request.getParameter('hdnOrderNo');
		var ContainerLPNo = request.getParameter('hdnContainerLpNo');
		var ExpectedQuantity = request.getParameter('hdnExpectedQuantity');
		var ActualQty = request.getParameter('hdnActualQuantity');
		var ItemInternalId = request.getParameter('hdnItemInternalId');
		var getCortonNo = request.getParameter('hdncontlpno');		
		var wmslocation= request.getParameter('hdnwmslocation');
		var containersize= request.getParameter('hdncontainervalue');
		var bulkqty=request.getParameter('hdnbulkqty');
		var contlp=request.getParameter('hdnFetchedContainerLPNo');
		var RecordInternalId=request.getParameter("custparam_recordinternalid");
		var Item=request.getParameter("hdnItem");

		var SOarray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		SOarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', SOarray["custparam_language"]);    	

		var st10,st11,st12,st13,st14;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{			
			st10 = "CANTIDAD INV&#193;LIDA";
			st11 = "CANTIDAD ESCANEADO MAYOR QUE LA CANTIDAD PICKED";
			st12 = "ART&#205;CULO INV&#193;LIDO";
			st13 = "CANTIDAD NO INFORMADO";
			st14 = "ART&#205;CULO NO REALIZADAS";
		}
		else
		{			
			st10 = "INVALID QUANTITY";
			st11 = "SCANNED QUANTITY GREATER THAN THE PICKED QTY";
			st12 = "INVALID ITEM";
			st13 = "QUANTITY NOT ENTERED";
			st14 = "ITEM NOT ENTERED";
		}

		SOarray["custparam_error"] = st10;
		SOarray["custparam_screenno"] = 'Pack4';
		SOarray["custparam_orderno"] = OrderNo;        
		SOarray["custparam_containerno"] = request.getParameter('custparam_containerno');
		SOarray["custparam_fetchedcontainerlp"]=request.getParameter('hdnFetchedContainerLPNo');
		SOarray["custparam_recordinternalid"]=request.getParameter('custparam_recordinternalid');
		SOarray["custparam_waveno"] = request.getParameter('hdnwaveno');
		SOarray["custparam_number"]=parseFloat(getNumber);
		SOarray["custparam_numbernew"]=request.getParameter('custparam_numbernew');
		nlapiLogExecution('DEBUG', 'SOarray["custparam_numbernew"]', SOarray["custparam_numbernew"]);
		nlapiLogExecution('DEBUG', 'OrderNo', OrderNo);
		nlapiLogExecution('DEBUG', 'contlp', contlp);

		/*
        if(SOarray["custparam_item"] == null)
        {
        	SOarray["custparam_item"] = Item;
        }
        else
        {
        	SOarray["custparam_item"] = SOarray["custparam_item"] + "," +  Item;
        }

        nlapiLogExecution('DEBUG', 'Entered Item information-->', SOarray["custparam_item"]);   
		 */

		var vLoopCount = request.getParameter('hdnloopcount');

		nlapiLogExecution('DEBUG', 'vLoopCount-->',vLoopCount);

		nlapiLogExecution('DEBUG', 'Qty Information-->', SOarray["custparam_expectedquantity"]);

		if(parseInt(vLoopCount)==1)
		{
			SOarray["custparam_expectedquantity"] = varQty;
			nlapiLogExecution('DEBUG', 'Inside If QTY information-->', SOarray["custparam_enterqty"]);   
		}
		else
		{
			SOarray["custparam_expectedquantity"] = SOarray["custparam_expectedquantity"] + "," +  varQty;
			nlapiLogExecution('DEBUG', 'Inside Else information-->',  SOarray["custparam_expectedquantity"]);
		}


		SOarray["custparam_fetchedcontainerlp"]=request.getParameter('hdnFetchedContainerLPNo');    
		SOarray["custparam_expectedquantity"] = request.getParameter('hdnExpectedQuantity');
		SOarray["custparam_actualquantity"] =  request.getParameter('hdnActualQuantity');
		SOarray["custparam_nextiteminternalid"] = request.getParameter('custparam_nextiteminternalid');
		SOarray["custparam_enteredqty"] =  request.getParameter('enterqty');
		SOarray["custparam_iteminternalid"] = request.getParameter('hdnItemInternalId');

		SOarray["custparam_linecount"] = request.getParameter('hdnlinecount');
		SOarray["custparam_loopcount"] = request.getParameter('hdnloopcount');
		SOarray["custparam_containerno"] = getCortonNo;

		SOarray['custparam_wmslocation']=wmslocation;

		SOarray['custparam_bulkqty']=bulkqty;
		SOarray['custparam_containersize']=request.getParameter('custparam_containersize');
		SOarray["custparam_containersizevalue"] = request.getParameter('hdncontainervalue');
		SOarray["custparam_actualquantity"]=request.getParameter("custparam_actualquantity");
		//var currItemArray = validateSKUId(varitem, wmslocation, null);
		var currItemflag='';
		var currItem='';

		if(varitem!=null && varitem!='') 
		{

			var currItemArray = validateSKUId(varitem, wmslocation, null);
			currItemflag=currItemArray[0];
			currItem=currItemArray[1];
			nlapiLogExecution('DEBUG', 'currItemArray main', currItemArray);
			nlapiLogExecution('DEBUG', 'currItem main', currItem);
			nlapiLogExecution('DEBUG', 'Item main', Item);
			nlapiLogExecution('DEBUG', 'currItemflag main', currItemflag);	
		}
		var gettotalscannedNumber='';
		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept SO #.
		nlapiLogExecution('DEBUG', 'optENT', optENT);


		if (sessionobj!=context.getUser()) {
			try
			{

				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}
				if (optedEvent == 'F7') {
					SOarray["custparam_nextiteminternalid"]='';
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_scanitem', 'customdeploy_ebiz_rf_pack_scanitem_di', false, SOarray);	
				}
				else if(optENT=='ENT')
				{	
					nlapiLogExecution('DEBUG', 'ItemInternalid', request.getParameter('hdnItemInternalId'));
					var itemId=request.getParameter('hdnItemInternalId');

					nlapiLogExecution('DEBUG', 'itemId', itemId);
					nlapiLogExecution('DEBUG', 'currItem', currItem);
					var Itype = nlapiLookupField('item', itemId, 'recordType');
					var ItemRec = nlapiLoadRecord(Itype, itemId);

					if(currItem!=null && currItem!='')
					{
						if(itemId != currItem )
						{
							SOarray["custparam_error"] = st12;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							nlapiLogExecution('DEBUG', 'Error: ', 'Invalid Item enterd');
							return;
						}
					}

					var currItemArray = validateSKUId(varitem, wmslocation, null);
					var currItemflag=currItemArray[0];
					var currItem=currItemArray[1];

					if((varitem!=null && varitem!='') && (varQty==''|| varQty==null))
					{
						var getItemName = ItemRec.getFieldValue('itemid');
						//var currItem = validateSKU(getItemName, wmslocation, null);
						nlapiLogExecution('DEBUG', 'currItemArray', currItemArray);
						nlapiLogExecution('DEBUG', 'currItem', currItem);
						nlapiLogExecution('DEBUG', 'Item', Item);
						nlapiLogExecution('DEBUG', 'currItemflag', currItemflag);					

						//	var ItemInternalid=eBiz_RF_GetItemForItemId(currItem,wmslocation)
						var ItemInternalid=currItem;
						var ItemCube='';
						var Baseuomqty='';

						if(itemId==currItem &&(currItem!=null && currItem!=''))
						{
							if(currItemflag=='F')
								var ItemInfo = eBiz_RF_GetItemCubeForItem(itemId);
							else
								var ItemInfo = eBiz_RF_GetItemCubeForItemAlias(itemId,varitem);
							nlapiLogExecution('DEBUG', 'ItemInfo[1]', ItemInfo[1]);						
							Baseuomqty=ItemInfo[1];
							SOarray["custparam_number"] = parseFloat(getNumber) + parseFloat(Baseuomqty);
							getNumber=parseFloat(getNumber) + parseFloat(Baseuomqty);


							SOarray["custparam_numbernew"]=parseFloat(getNumber);

							nlapiLogExecution('DEBUG', 'getNumber', getNumber);
							nlapiLogExecution('DEBUG', 'SOarray["custparam_numbernew"]', SOarray["custparam_numbernew"]);
							nlapiLogExecution('DEBUG', 'getItemQuantity', ExpectedQuantity);

							if ((parseFloat(getNumber)) <= parseFloat(ExpectedQuantity)) {
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_scanqty', 'customdeploy_ebiz_rf_pack_scanqty_di', false, SOarray);
								return;
							}
							else
							{
								//SOarray["custparam_number"]=parseFloat(ExpectedQuantity);
								SOarray["custparam_number"]=parseFloat(getNumber) - parseFloat(Baseuomqty);
								SOarray["custparam_numbernew"]=parseFloat(ExpectedQuantity);
								SOarray["custparam_error"] = st11;
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								nlapiLogExecution('DEBUG', 'Error: ', 'Qty Not Entered');
								return;
							}
						}
						else
						{   
							SOarray["custparam_number"] = parseFloat(getNumber);
							SOarray["custparam_error"] = st12;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							nlapiLogExecution('DEBUG', 'Error: ', 'Qty Not Entered');
							return;
						}
					}
					else if(varQty!=null && varQty!='' && parseFloat(varQty)>0)
					{
						//case# 20127204 starts (cheking for entered item is valid or not)
						if(currItem==itemId)
						{
							var vEnteredQty = request.getParameter('enterqty'); 
							if(vEnteredQty==null || vEnteredQty=='')
							{
								vEnteredQty=0;
							}
							nlapiLogExecution('DEBUG', 'Error: ', SOarray["custparam_numbernew"]);
							var totalscanned = SOarray["custparam_numbernew"];
							if(totalscanned == null || totalscanned == '')
							{
								totalscanned = 0;
							}
							if(bulkqty=='')
								vEnteredQty=parseFloat(totalscanned)+parseFloat(vEnteredQty);

							if (isNaN(vEnteredQty)) {
								SOarray["custparam_number"] = parseFloat(getNumber);
								SOarray["custparam_error"] = st12;
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								nlapiLogExecution('DEBUG', 'Error: ', 'Qty wrong Entered');
								return;
							}

							// Case# 20148702 starts 
							nlapiLogExecution('DEBUG', 'ExpectedQuantity is ',ExpectedQuantity);
							if(ExpectedQuantity != vEnteredQty)
							{	
								var ebizcntrlno,linenumber='';
								var salesOrderList = getSalesOrderlist(OrderNo,contlp,wmslocation);
								if(salesOrderList !='' && salesOrderList!=null && salesOrderList!='null')
								{
									for (var i = 0; i < salesOrderList.length; i++){
										ebizcntrlno = salesOrderList[i].getValue('custrecord_ebiz_cntrl_no');
										linenumber=salesOrderList[i].getValue('custrecord_line_no');
									}
								}
								nlapiLogExecution('DEBUG', 'ebizcntrlno', ebizcntrlno);
								nlapiLogExecution('DEBUG', 'linenumber', linenumber);
								var shipcomplete='N';
								var filtersfo = new Array();
								if(ebizcntrlno!=''&& ebizcntrlno!='null'&& ebizcntrlno!=null)
									filtersfo.push(new nlobjSearchFilter('internalid', null, 'is', ebizcntrlno));	
								if(linenumber!=''&& linenumber!='null'&& linenumber!=null)
									filtersfo.push(new nlobjSearchFilter('custrecord_ordline', null, 'is', linenumber));
								var columnsfo = new Array();
								columnsfo[0] = new nlobjSearchColumn('custrecord_shipcomplete');
								var searchresultsfo = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filtersfo, columnsfo);
								if(searchresultsfo!=null)
									shipcomplete=searchresultsfo[0].getValue('custrecord_shipcomplete');
								if(shipcomplete=='T')
								{
									SOarray["custparam_error"] = 'This SO/TO has created with Ship Complete set ON';
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
									nlapiLogExecution('DEBUG', 'Error: ', 'This SO/TO has created with Ship Complete set ON');
									return;
									//alert('This SO/TO has created with Ship Complete set ON');
									//return false;
								}
							}
							// Case# 20148702 ends
							nlapiLogExecution('DEBUG', 'vEnteredQty', vEnteredQty);
							if(parseFloat(vEnteredQty)>parseFloat(ExpectedQuantity))
							{
								SOarray["custparam_number"] = parseFloat(getNumber);
								SOarray["custparam_error"] = st11;
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								nlapiLogExecution('DEBUG', 'Error: ', 'Qty Not Entered');
								return;
							}
							/*	// Case# 20148702 starts 
				nlapiLogExecution('DEBUG', 'ExpectedQuantity is ',ExpectedQuantity);
				if(ExpectedQuantity != vEnteredQty)
				{	
					var ebizcntrlno,linenumber='';
					var salesOrderList = getSalesOrderlist(OrderNo,contlp,wmslocation);
					if(salesOrderList !='' && salesOrderList!=null && salesOrderList!='null')
					{
						for (var i = 0; i < salesOrderList.length; i++){
							ebizcntrlno = salesOrderList[i].getValue('custrecord_ebiz_cntrl_no');
							linenumber=salesOrderList[i].getValue('custrecord_line_no');
						}
					}
					nlapiLogExecution('DEBUG', 'ebizcntrlno', ebizcntrlno);
					nlapiLogExecution('DEBUG', 'linenumber', linenumber);
					var shipcomplete='N';
					var filtersfo = new Array();
					if(ebizcntrlno!=''&& ebizcntrlno!='null'&& ebizcntrlno!=null)
					filtersfo.push(new nlobjSearchFilter('internalid', null, 'is', ebizcntrlno));	
					if(linenumber!=''&& linenumber!='null'&& linenumber!=null)
					filtersfo.push(new nlobjSearchFilter('custrecord_ordline', null, 'is', linenumber));
					var columnsfo = new Array();
					columnsfo[0] = new nlobjSearchColumn('custrecord_shipcomplete');
					var searchresultsfo = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filtersfo, columnsfo);
					if(searchresultsfo!=null)
						shipcomplete=searchresultsfo[0].getValue('custrecord_shipcomplete');
					if(shipcomplete=='T')
					{
						SOarray["custparam_error"] = 'This SO/TO has created with Ship Complete set ON';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						nlapiLogExecution('DEBUG', 'Error: ', 'This SO/TO has created with Ship Complete set ON');
						return;
						//alert('This SO/TO has created with Ship Complete set ON');
						//return false;
					}
				}
				// Case# 20148702 ends */
							else
							{
								var BeginLocation='';
								var loopcount=parseFloat(SOarray["custparam_loopcount"]);//order no loop count
								var closestatus="";
								if(loopcount ==1 || loopcount == '1')
									closestatus="T";
								UpdateConfirmDate(OrderNo, RecordInternalId, ContainerLPNo, vEnteredQty, BeginLocation, '', '', ItemInternalId, '', '', '', '', '', '',contlp,wmslocation,containersize,closestatus,request);


								var loopcountnew;
								nlapiLogExecution('DEBUG', 'loopcount', loopcount);
								nlapiLogExecution('DEBUG', 'OrderNo1', OrderNo);
								nlapiLogExecution('DEBUG', 'ContainerLPNo1', ContainerLPNo);

								if(loopcount >= 1)
								{	
									var SOFilters = new Array();
									if(OrderNo!=null && OrderNo!='')
										SOFilters.push(new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', OrderNo));
									SOFilters.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is', 'T'));
									//SOFilters.push(new nlobjSearchFilter('name', null, 'is', vordername));

									//if(ContainerLPNo!=null && ContainerLPNo!='')
									//SOFilters.push(new nlobjSearchFilter( 'custrecord_container_lp_no',null, 'is', ContainerLPNo));

									SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));
									SOFilters.push(new nlobjSearchFilter( 'custrecord_pack_confirmed_date',null, 'isempty', null));

									SOFilters.push(new nlobjSearchFilter('custrecord_tasktype',null, 'anyof', [3]));	
									SOFilters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'greaterthan', 0));

									var SOColumns = new Array();
									SOColumns[0] = new nlobjSearchColumn('custrecord_lpno');
									SOColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
									SOColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
									SOColumns[3] = new nlobjSearchColumn('custrecord_sku');
									SOColumns[4] = new nlobjSearchColumn('custrecord_skudesc');
									SOColumns[5] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
									SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
									SOColumns[7] = new nlobjSearchColumn('custrecord_invref_no');
									SOColumns[8] = new nlobjSearchColumn('custrecord_line_no');
									SOColumns[9] = new nlobjSearchColumn('custrecord_actbeginloc');
									SOColumns[10] = new nlobjSearchColumn('custrecord_batch_no');	
									SOColumns[11] = new nlobjSearchColumn('custrecord_ebiz_order_no');
									SOColumns[12] = new nlobjSearchColumn('custrecord_container_lp_no');
									SOColumns[13] = new nlobjSearchColumn('custrecord_wms_location');
									SOColumns[3].setSort();

									var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns); 

									if(SOSearchResults==null || SOSearchResults=='')
									{
										SOarray["custparam_loopcount"] = 0;
										loopcount=parseFloat(SOarray["custparam_loopcount"]);
									}

									if(SOSearchResults!=null && SOSearchResults!='')
									{
										nlapiLogExecution('DEBUG', 'SOSearchResults', SOSearchResults.length);
										nlapiLogExecution('DEBUG', 'nextcontlp', SOSearchResults[0].getValue('custrecord_container_lp_no'));
										
										nlapiLogExecution('DEBUG', 'loopcountnew', loopcountnew);
										SOarray["custparam_containerno"] = SOSearchResults[0].getValue('custrecord_container_lp_no');
										SOarray["custparam_iteminternalid"] = SOSearchResults[0].getValue('custrecord_ebiz_sku_no');
										SOarray["custparam_nextlocation"] = SOSearchResults[0].getText('custrecord_actbeginloc');
										SOarray["custparam_loopcount"]=SOSearchResults.length;
										loopcount=parseFloat(SOarray["custparam_loopcount"]);

									}

									nlapiLogExecution('DEBUG', 'loopcountnew', loopcountnew);
									nlapiLogExecution('DEBUG', 'loopcount', loopcount);
									if(loopcount > 0)
									{	
										response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_scanitem', 'customdeploy_ebiz_rf_pack_scanitem_di', false, SOarray);
										return;
									}
									else
									{
										response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_closecarton', 'customdeploy_ebiz_rf_pack_closecarton_di', false, SOarray);
										return;
										// response.sendRedirect('SUITELET', 'customscript_rf_packing_contlp', 'customdeploy_rf_packing_contlp_di', false, SOarray);
									}
								}
								else
								{
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_closecarton', 'customdeploy_ebiz_rf_pack_closecarton_di', false, SOarray);
									// response.sendRedirect('SUITELET', 'customscript_rf_packing_contlp', 'customdeploy_rf_packing_contlp_di', false, SOarray);
								}
							}
						}
						else
						{
							SOarray["custparam_iteminternalid"] = itemId;
							SOarray["custparam_error"] = st12;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							nlapiLogExecution('DEBUG', 'Error: ', 'Invalidd Item');
							return;
						}
						//case# 20127204 end
					}
					else
					{
						if(parseFloat(varQty)<0)
						{
							SOarray["custparam_error"] = 'Please Enter Valid Qty';
						}
						else
						{
							//SOarray["custparam_error"] = st14;
							SOarray["custparam_error"] = 'Unable to perform packing for this quantity';

						}
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						nlapiLogExecution('DEBUG', 'Error: ', 'Qty Not Entered');
						return;
					}
				}
				else if(optclose=='F8')
				{
					try{
						nlapiLogExecution('DEBUG', 'gettotalscannedNumber in F8', SOarray["custparam_numbernew"]);
						var totalscanned = SOarray["custparam_numbernew"];
						if(varQty==null || varQty=='')
						{
							totalscanned=totalscanned+varQty;

						}
						if(request.getParameter('enterqty') != null && request.getParameter('enterqty') != '')
							totalscanned=parseFloat(request.getParameter('enterqty')) + parseFloat(getNumber);
						else
							totalscanned=parseFloat(getNumber);
						nlapiLogExecution('DEBUG', 'totalscanned', totalscanned);
						nlapiLogExecution('DEBUG', 'ActualQty', ActualQty);
						// Case# 20148702 starts 
						if(totalscanned != ActualQty)
						{	
							var ebizcntrlno,linenumber='';
							var salesOrderList = getSalesOrderlist(OrderNo,contlp,wmslocation);
							nlapiLogExecution('DEBUG', 'salesOrderList', salesOrderList);
							if(salesOrderList !='' && salesOrderList!=null && salesOrderList!='null')
							{
								for (var i = 0; i < salesOrderList.length; i++){
									ebizcntrlno = salesOrderList[i].getValue('custrecord_ebiz_cntrl_no');
									linenumber=salesOrderList[i].getValue('custrecord_line_no');
								}
							}
							nlapiLogExecution('DEBUG', 'ebizcntrlno', ebizcntrlno);
							nlapiLogExecution('DEBUG', 'linenumber', linenumber);
							var shipcomplete='N';
							var filtersfo = new Array();
							if(ebizcntrlno!=''&& ebizcntrlno!='null'&& ebizcntrlno!=null)
								filtersfo.push(new nlobjSearchFilter('internalid', null, 'is', ebizcntrlno));	
							if(linenumber!=''&& linenumber!='null'&& linenumber!=null)
								filtersfo.push(new nlobjSearchFilter('custrecord_ordline', null, 'is', linenumber));
							var columnsfo = new Array();
							columnsfo[0] = new nlobjSearchColumn('custrecord_shipcomplete');
							var searchresultsfo = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filtersfo, columnsfo);
							if(searchresultsfo!=null)
								shipcomplete=searchresultsfo[0].getValue('custrecord_shipcomplete');
							nlapiLogExecution('DEBUG', 'shipcomplete', shipcomplete);
							if(shipcomplete=='T')
							{
								SOarray["custparam_error"] = 'This SO/TO has created with Ship Complete set ON';
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								nlapiLogExecution('DEBUG', 'Error: ', 'This SO/TO has created with Ship Complete set ON');
								return;
								//alert('This SO/TO has created with Ship Complete set ON');
								//return false;
							}
						}
						// Case# 20148702 ends
						var BeginLocation='';
						var closestatus="T";
						//UpdateConfirmDate(OrderNo, RecordInternalId, ContainerLPNo, totalscanned, BeginLocation, '', '', ItemInternalId, '', '', '', '', '', '',contlp,wmslocation,containersize,closestatus); // Case# 20149510
						UpdateConfirmDate(OrderNo, RecordInternalId, ContainerLPNo, totalscanned, BeginLocation, '', '', ItemInternalId, '', '', '', '', '', '',contlp,wmslocation,containersize,closestatus,request);
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_closecarton', 'customdeploy_ebiz_rf_pack_closecarton_di', false, SOarray);
						return;
					}
					catch(e2)
					{
						nlapiLogExecution('DEBUG', 'e2', e2);
					}
				}
				else 
				{
					var vEnteredQty = request.getParameter('enterqty'); 
					nlapiLogExecution('DEBUG', 'vEnteredQty', vEnteredQty);
					nlapiLogExecution('DEBUG', 'getNumber', getNumber);
					if(vEnteredQty==null || vEnteredQty=='')
					{
						vEnteredQty=0;
					}
					nlapiLogExecution('DEBUG', 'Error: ', SOarray["custparam_numbernew"]);
					var totalscanned = SOarray["custparam_numbernew"];
					if(totalscanned == null || totalscanned == '')
					{
						totalscanned = 0;
					}
					if(bulkqty=='')
						vEnteredQty=parseFloat(totalscanned)+parseFloat(vEnteredQty);

					//vEnteredQty=parseFloat(getNumber)+parseFloat(vEnteredQty);
					nlapiLogExecution('DEBUG', 'vEnteredQtyAfter', vEnteredQty);
					if (vEnteredQty != '' && vEnteredQty != null) 
					{				

						if(parseFloat(vEnteredQty)>parseFloat(ExpectedQuantity))
						{
							SOarray["custparam_number"] = parseFloat(getNumber);
							SOarray["custparam_error"] = st11;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							nlapiLogExecution('DEBUG', 'Error: ', 'Qty Not Entered');
							return;
						}
						else
						{
							var BeginLocation='';
							var loopcount=parseFloat(SOarray["custparam_loopcount"]);
							nlapiLogExecution('DEBUG', 'loopcount', loopcount);
							var closestatus="";
							if(loopcount ==1 || loopcount == '1')
								closestatus="T";
							UpdateConfirmDate(OrderNo, RecordInternalId, ContainerLPNo, vEnteredQty, BeginLocation, '', '', ItemInternalId, '', '', '', '', '', '',contlp,wmslocation,containersize,closestatus,request);


							nlapiLogExecution('DEBUG', 'OrderNo1', OrderNo);
							nlapiLogExecution('DEBUG', 'ContainerLPNo1', ContainerLPNo);

							if(loopcount>=1)
							{	
								var SOFilters = new Array();
								if(OrderNo!=null && OrderNo!='')
									SOFilters.push(new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', OrderNo));
								SOFilters.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is', 'T'));
								SOFilters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'greaterthan', 0));
								//if(ContainerLPNo!=null && ContainerLPNo!='')
								//SOFilters.push(new nlobjSearchFilter( 'custrecord_container_lp_no',null, 'is', ContainerLPNo));

								SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//Status Flag - C (Picks confirmed)
								SOFilters.push(new nlobjSearchFilter( 'custrecord_pack_confirmed_date',null, 'isempty', null));
								SOFilters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'greaterthan', 0));
								SOFilters.push(new nlobjSearchFilter('custrecord_tasktype',null, 'anyof', [3]));	

								var SOColumns = new Array();
								SOColumns[0] = new nlobjSearchColumn('custrecord_lpno');
								SOColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
								SOColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
								SOColumns[3] = new nlobjSearchColumn('custrecord_sku');
								SOColumns[4] = new nlobjSearchColumn('custrecord_skudesc');
								SOColumns[5] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
								SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
								SOColumns[7] = new nlobjSearchColumn('custrecord_invref_no');
								SOColumns[8] = new nlobjSearchColumn('custrecord_line_no');
								SOColumns[9] = new nlobjSearchColumn('custrecord_actbeginloc');
								SOColumns[10] = new nlobjSearchColumn('custrecord_batch_no');	
								SOColumns[11] = new nlobjSearchColumn('custrecord_ebiz_order_no');
								SOColumns[12] = new nlobjSearchColumn('custrecord_container_lp_no');
								SOColumns[13] = new nlobjSearchColumn('custrecord_wms_location');
								SOColumns[3].setSort();

								var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns); 
								if(SOSearchResults==null || SOSearchResults=='')
								{
									SOarray["custparam_loopcount"] = 0;
									loopcount=parseFloat(SOarray["custparam_loopcount"]);
								}
								if(SOSearchResults!=null && SOSearchResults!='')
								{
									nlapiLogExecution('DEBUG', 'SOSearchResults', SOSearchResults.length);
									nlapiLogExecution('DEBUG', 'nextitem', SOSearchResults[0].getValue('custrecord_ebiz_sku_no'));
									SOarray["custparam_iteminternalid"] = SOSearchResults[0].getValue('custrecord_ebiz_sku_no');
									SOarray["custparam_nextlocation"] = SOSearchResults[0].getText('custrecord_actbeginloc');
									SOarray["custparam_loopcount"]=SOSearchResults.length;
								}


								if(loopcount > 0)
								{	
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_scanitem', 'customdeploy_ebiz_rf_pack_scanitem_di', false, SOarray);
									return;
								}
								else
								{
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_closecarton', 'customdeploy_ebiz_rf_pack_closecarton_di', false, SOarray);
									return;
									// response.sendRedirect('SUITELET', 'customscript_rf_packing_contlp', 'customdeploy_rf_packing_contlp_di', false, SOarray);
								}
							}
							else
							{
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_closecarton', 'customdeploy_ebiz_rf_pack_closecarton_di', false, SOarray);
								// response.sendRedirect('SUITELET', 'customscript_rf_packing_contlp', 'customdeploy_rf_packing_contlp_di', false, SOarray);
							}
						}

					}
					else if ((parseFloat(getNumber)) == parseFloat(ExpectedQuantity)) 
					{
						var BeginLocation='';
						if(bulkqty=='')
							var vEnteredQty = getNumber;
						else
							var vEnteredQty = request.getParameter('enterqty');                

						SOarray['custpage_multipleitemscan']='True';
						var loopcount=parseFloat(SOarray["custparam_loopcount"]);
						nlapiLogExecution('DEBUG', 'loopcount', loopcount);
						var closestatus="";
						if(loopcount ==1 || loopcount == '1')
							closestatus="T";

						UpdateConfirmDate(OrderNo, RecordInternalId, ContainerLPNo, vEnteredQty, BeginLocation, '', '', ItemInternalId, '', '', '', '', '', '',contlp,wmslocation,containersize,closestatus,request);



						nlapiLogExecution('DEBUG', 'OrderNo', OrderNo);
						nlapiLogExecution('DEBUG', 'ContainerLPNo', ContainerLPNo);

						if(loopcount>=1)
						{	
							var SOFilters = new Array();
							if(OrderNo!=null && OrderNo!='')
								SOFilters.push(new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', OrderNo));

							SOFilters.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is', 'T'));
							SOFilters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'greaterthan', 0));

							//if(ContainerLPNo!=null && ContainerLPNo!='')
							//SOFilters.push(new nlobjSearchFilter( 'custrecord_container_lp_no',null, 'is', ContainerLPNo));
							SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));

							SOFilters.push(new nlobjSearchFilter( 'custrecord_pack_confirmed_date',null, 'isempty', null));

							SOFilters.push(new nlobjSearchFilter('custrecord_tasktype',null, 'anyof', [3]));	

							var SOColumns = new Array();
							SOColumns[0] = new nlobjSearchColumn('custrecord_lpno');
							SOColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
							SOColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
							SOColumns[3] = new nlobjSearchColumn('custrecord_sku');
							SOColumns[4] = new nlobjSearchColumn('custrecord_skudesc');
							SOColumns[5] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
							SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
							SOColumns[7] = new nlobjSearchColumn('custrecord_invref_no');
							SOColumns[8] = new nlobjSearchColumn('custrecord_line_no');
							SOColumns[9] = new nlobjSearchColumn('custrecord_actbeginloc');
							SOColumns[10] = new nlobjSearchColumn('custrecord_batch_no');	
							SOColumns[11] = new nlobjSearchColumn('custrecord_ebiz_order_no');
							SOColumns[12] = new nlobjSearchColumn('custrecord_container_lp_no');
							SOColumns[13] = new nlobjSearchColumn('custrecord_wms_location');
							SOColumns[3].setSort();

							var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns); 
							if(SOSearchResults==null || SOSearchResults=='')
							{
								SOarray["custparam_loopcount"] = 0;
								loopcount=parseFloat(SOarray["custparam_loopcount"]);
							}
							if(SOSearchResults!=null && SOSearchResults!='')
							{
								nlapiLogExecution('DEBUG', 'SOSearchResults', SOSearchResults.length);
								nlapiLogExecution('DEBUG', 'nextitem', SOSearchResults[0].getValue('custrecord_ebiz_sku_no'));
								SOarray["custparam_iteminternalid"] = SOSearchResults[0].getValue('custrecord_ebiz_sku_no');
								SOarray["custparam_nextlocation"] = SOSearchResults[0].getText('custrecord_actbeginloc');
								SOarray["custparam_loopcount"]=SOSearchResults.length;
							}


							if(loopcount > 0)
							{	
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_scanitem', 'customdeploy_ebiz_rf_pack_scanitem_di', false, SOarray);
								return;
							}
							else
							{
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_closecarton', 'customdeploy_ebiz_rf_pack_closecarton_di', false, SOarray);
								return;
								// response.sendRedirect('SUITELET', 'customscript_rf_packing_contlp', 'customdeploy_rf_packing_contlp_di', false, SOarray);
							}
						}
						else
						{
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pack_closecarton', 'customdeploy_ebiz_rf_pack_closecarton_di', false, SOarray);
							// response.sendRedirect('SUITELET', 'customscript_rf_packing_contlp', 'customdeploy_rf_packing_contlp_di', false, SOarray);
						}
					}

					else 
					{
						SOarray["custparam_error"] = st13;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						nlapiLogExecution('DEBUG', 'Error: ', 'Qty Not Entered');
						return;
					}
				}
			}
			catch (e)  {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');

			}
		}
		else
		{
			SOarray["custparam_screenno"] = 'Pack1';
			SOarray["custparam_error"] = 'CARTON# ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);

		}
	}
}

/**
 * 
 * @param itemNo
 * @param location
 * @param company
 * @returns
 */


function validateSKU(itemNo, location, company){
	nlapiLogExecution('DEBUG', 'validateSKU', 'Start');

	var inputParams = 'Item = ' + itemNo + '<br>';
	inputParams = inputParams + 'Location = ' + location + '<br>';
	inputParams = inputParams + 'Company = ' + company;
	nlapiLogExecution('DEBUG', 'Input Parameters', inputParams);
	var currItemArray=new Array();
	currItemArray[0]='F';
	var currItem = eBiz_RF_GetItemForItemNo(itemNo);

	if(currItem == ""){
		currItem = eBiz_RF_GetItemBasedOnUPCCode(itemNo);
	}

	if(currItem == ""){
		currItem = eBiz_RF_GetItemFromSKUDims(itemNo, location);
		if(currItem != ""&&currItem !=null)
			currItemArray[0]='T';
	}

	var logMsg = "";
	if(currItem == ""){
		logMsg = 'Unable to retrieve item';
		currItem = null;
	} else {
		logMsg = 'Item = ' + currItem;
	}
	currItemArray[1]=currItem;
	nlapiLogExecution('DEBUG', 'Item Retrieved', logMsg);
	nlapiLogExecution('DEBUG', 'validateSKU', 'End');
//	return currItem;
	return currItemArray;
}

function eBiz_RF_GetItemForItemId(itemId,location){
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemForItemId', 'Start');

	var itemInternalId = null;

	if(itemId != null && itemId != ""){
		var logMsg = 'ItemId = |' + itemId + '|';
		nlapiLogExecution('DEBUG', 'Input Parameters', logMsg);

		var filters = new Array();
		filters.push(new nlobjSearchFilter('itemid', null, 'is', itemId));
		if(location!=null && location!='')
			filters.push(new nlobjSearchFilter('location', null, 'anyof', [null,location]));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('itemid');
		columns[0].setSort(true);

		var itemSearchResults = nlapiSearchRecord('item', null, filters, columns);
		if(itemSearchResults != null && itemSearchResults.length > 0){
			itemInternalId = itemSearchResults[0].getId();
			nlapiLogExecution('DEBUG', 'Internal Id', itemInternalId);

		}
	}

	nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemForItemId', 'End');
	return itemInternalId;
}

function UpdateConfirmDate(OrderNo, RecordInternalId, ContainerLPNo, ExpectedQuantity, BeginLocation, Item, 
		ItemDescription, ItemInternalId, DoLineId, InvoiceRefNo, BeginLocationName, EndLocInternalId, EndLocation, 
		OrderLineNo,contlp,wmslocation,containersize,closestatus,request)
{
	nlapiLogExecution('DEBUG', "Into UpdateConfirmDate...");

	var now = new Date();
	var lineCnt = request.getLineItemCount('custpage_items');
	nlapiLogExecution('DEBUG', "into update count" ,'hi');
	nlapiLogExecution('DEBUG', "Line Count" + lineCnt);
	nlapiLogExecution('DEBUG', "ExpectedQuantity" + ExpectedQuantity);
	var invtfalg = "";
	var ShortPick = "";
	var invnotfound = "";

	//getting time

	var a_p = "";
	var d = new Date();
	var curr_hour = now.getHours();
	if (curr_hour < 12) {
		a_p = "am";
	}
	else {
		a_p = "pm";
	}
	if (curr_hour == 0) {
		curr_hour = 12;
	}
	if (curr_hour > 12) {
		curr_hour = curr_hour - 12;
	}

	var curr_min = now.getMinutes();

	curr_min = curr_min + "";

	if (curr_min.length == 1) 
	{
		curr_min = "0" + curr_min;
	}
	nlapiLogExecution('DEBUG', "RecordInternalId" , RecordInternalId);
	var CurrentDate = DateStamp();
	var OpenTaskTransaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordInternalId);

	nlapiLogExecution('DEBUG', 'Current Date', CurrentDate);
	nlapiLogExecution('DEBUG', 'Record Internal ID', RecordInternalId);
	nlapiLogExecution('DEBUG', 'Container LP No', ContainerLPNo);
	nlapiLogExecution('DEBUG', 'OrderNo', OrderNo);
	nlapiLogExecution('DEBUG', 'ItemInternalId', ItemInternalId);
	nlapiLogExecution('DEBUG', 'contlp', contlp);
	nlapiLogExecution('DEBUG', 'containersize', containersize);

	if((ExpectedQuantity!=null && ExpectedQuantity!='') && ExpectedQuantity>0 )
	{
		var isItLastPick=OpenTaskTransaction.getFieldValue('custrecord_device_upload_flag');
		var TotalWeight=OpenTaskTransaction.getFieldValue('custrecord_total_weight');
		var TotalCube=OpenTaskTransaction.getFieldValue('custrecord_totalcube');
		nlapiLogExecution('DEBUG', 'TotalWeight', TotalWeight);
		nlapiLogExecution('DEBUG', 'TotalCube', TotalCube);
		nlapiLogExecution('DEBUG', 'ExpectedQuantity', parseFloat(ExpectedQuantity).toFixed(4));
		var Totweightupdate=(parseFloat(TotalWeight)/parseFloat(TotalCube))*parseFloat(ExpectedQuantity);

		try
		{	    
			OpenTaskTransaction.setFieldValue('custrecord_wms_status_flag', '28');//Statusflag='C'
			OpenTaskTransaction.setFieldValue('custrecord_act_qty', parseFloat(ExpectedQuantity).toFixed(4));
			OpenTaskTransaction.setFieldValue('custrecordact_end_date', DateStamp());
			OpenTaskTransaction.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
			//OpenTaskTransaction.setFieldValue('custrecord_actendloc', EndLocInternalId);
			OpenTaskTransaction.setFieldValue('custrecord_pack_confirmed_date', CurrentDate);
			OpenTaskTransaction.setFieldValue('custrecord_container_lp_no', ContainerLPNo);
			OpenTaskTransaction.setFieldValue('custrecord_total_weight', parseFloat(Totweightupdate).toFixed(4));
			OpenTaskTransaction.setFieldValue('custrecord_totalcube', parseFloat(ExpectedQuantity).toFixed(4));    

			nlapiSubmitRecord(OpenTaskTransaction, false, true);

			nlapiLogExecution('DEBUG', 'INTO MASTER LP INSERTION');
			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
			columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totcube');
			var filtersmlp = new Array();
			filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', ContainerLPNo);
			var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp,columns);
			if (SrchRecord != null && SrchRecord.length > 0) {
				nlapiLogExecution('DEBUG', 'LP FOUND');
				var lpRecid= SrchRecord[0].getId();
				var fields=new Array();
				fields[0]='custrecord_ebiz_lpmaster_totwght';
				fields[1]='custrecord_ebiz_lpmaster_totcube';	
				if(closestatus=='T'){
					fields[2]='custrecord_ebiz_lpmaster_wmsstatusflag';	
				}
				nlapiLogExecution('DEBUG', 'LP FOUND1');
				var lpweight=SrchRecord[0].getValue('custrecord_ebiz_lpmaster_totwght');
				var lpcube=SrchRecord[0].getValue('custrecord_ebiz_lpmaster_totcube');
				var Values=new Array();
				Values[0]=(parseFloat(lpweight)+parseFloat(Totweightupdate)).toFixed(4);
				Values[1]=(parseFloat(lpcube)+parseFloat(ExpectedQuantity)).toFixed(4);
				if(closestatus=='T'){
					Values[2]='28';//pack complete
				}nlapiLogExecution('DEBUG', 'LP FOUND2');
				var UpdatedRecID=nlapiSubmitField('customrecord_ebiznet_master_lp',lpRecid,fields,Values);
				nlapiLogExecution('DEBUG', 'LP FOUND3');
			}
			else {
				nlapiLogExecution('DEBUG', 'LP NOT FOUND');
				var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
				customrecord.setFieldValue('name', ContainerLPNo);
				customrecord.setFieldValue('custrecord_ebiz_lpmaster_totwght', parseFloat(Totweightupdate).toFixed(4));
				customrecord.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(ExpectedQuantity).toFixed(4));
				if(containersize!=null && containersize!='' && containersize!='undefined')
					customrecord.setFieldValue('custrecord_ebiz_lpmaster_sizeid', containersize);
				customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', ContainerLPNo);
				customrecord.setFieldValue('custrecord_ebiz_lpmaster_site', wmslocation);
				if(closestatus=='T'){
					customrecord.setFieldValue('custrecord_ebiz_lpmaster_wmsstatusflag', '28');//pack complete
				}

				var rec = nlapiSubmitRecord(customrecord, false, true);
			}


			var OpenTaskitemFilters = new Array();	

			//OpenTaskitemFilters.push(new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', OrderNo));
			OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_container_lp_no',null, 'is', ContainerLPNo));   	 	
			OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_pack_confirmed_date',null, 'isnotempty', null));
			OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [28]));//pack complete
			OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//pick
			var OpenTaskitemColumns = new Array();
			OpenTaskitemColumns[0] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			OpenTaskitemColumns[1] = new nlobjSearchColumn('custrecord_act_qty');
			OpenTaskitemColumns[2] = new nlobjSearchColumn('custrecord_expe_qty');
			OpenTaskitemColumns[3] = new nlobjSearchColumn('name');
			OpenTaskitemColumns[4] = new nlobjSearchColumn('custrecord_totalcube');
			OpenTaskitemColumns[5] = new nlobjSearchColumn('custrecord_total_weight');
			OpenTaskitemColumns[6] = new nlobjSearchColumn('custrecord_line_no');
			OpenTaskitemColumns[7] = new nlobjSearchColumn('custrecord_lpno');
			var  k=0;var Recid;
			var OpenTaskitemSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, OpenTaskitemFilters, OpenTaskitemColumns);
			if(OpenTaskitemSearchResults!=null && OpenTaskitemSearchResults!='')
			{
				nlapiLogExecution('DEBUG','OpenTaskitemSearchResults.length', OpenTaskitemSearchResults.length);
				for(var i=0;i<OpenTaskitemSearchResults.length;i++)
				{
					iteminternalid=OpenTaskitemSearchResults[i].getValue('custrecord_ebiz_sku_no');
					getExpQty = OpenTaskitemSearchResults[i].getValue('custrecord_expe_qty'); //Assign the Enter Qty here imp

					Recid= OpenTaskitemSearchResults[i].getId();
					//getExpQty
					fetchQty =OpenTaskitemSearchResults[i].getValue('custrecord_act_qty');
					contlpno = OpenTaskitemSearchResults[i].getValue('custrecord_container_lp_no');
					wmslocation = OpenTaskitemSearchResults[i].getValue('custrecord_wms_location');
					opentaskwt	= OpenTaskitemSearchResults[i].getValue('custrecord_total_weight');
					opentaskcube = OpenTaskitemSearchResults[i].getValue('custrecord_totalcube');
					varname = OpenTaskitemSearchResults[i].getValue('name');

					var skuname = OpenTaskitemSearchResults[i].getText('custrecord_sku');
					var vLineno = OpenTaskitemSearchResults[i].getValue('custrecord_line_no');
					var vLP = OpenTaskitemSearchResults[i].getValue('custrecord_lpno');

					nlapiLogExecution('DEBUG', 'skuname::', skuname);
					nlapiLogExecution('DEBUG', 'getExpQty::', getExpQty);
					nlapiLogExecution('DEBUG', 'fetchQty::', fetchQty);
					nlapiLogExecution('DEBUG', 'contlpno::', contlpno);
					nlapiLogExecution('DEBUG', 'wmslocation::', wmslocation);
					nlapiLogExecution('DEBUG', 'opentaskwt', opentaskwt);
					nlapiLogExecution('DEBUG', 'opentaskcube', opentaskcube);
					nlapiLogExecution('DEBUG', 'ExpectedQuantity', ExpectedQuantity);
					nlapiLogExecution('DEBUG', 'vLineno', vLineno);
					nlapiLogExecution('DEBUG', 'iteminternalid', iteminternalid);
					nlapiLogExecution('DEBUG', 'vLP', vLP);

					var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', Recid);								    	
					transaction.setFieldValue('custrecord_wms_status_flag', '28');//28 stands for packcomplete //Added on 24th
					transaction.setFieldValue('custrecord_container_lp_no', ContainerLPNo);
					//transaction.setFieldValue('custrecord_expe_qty', parseFloat(fetchQty).toFixed(4));
					//case# 201411610
					// as discussed with RR if we don't set exp qty here again is there is diff qty existing(like exp is old and acutual is present entered qty ) with this again there is a duplicate short pick record genrating with which there is a infinite loop of this item iterating
					transaction.setFieldValue('custrecord_expe_qty', parseFloat(fetchQty).toFixed(4));

					nlapiSubmitRecord(transaction, false, true);


					var diffqty=parseFloat(getExpQty)-parseFloat(fetchQty);
					var searchResults = CheckShortPicks(vLineno,vLP,iteminternalid,varname);
					nlapiLogExecution('DEBUG', 'diffqty', diffqty);
					nlapiLogExecution('DEBUG', 'searchResults', searchResults);

					// case # 20127242, creating a pick compeleted record in open task if there are short picks
					var diffqty1=parseFloat(getExpQty)-(parseFloat(fetchQty) + parseFloat(searchResults[1]));

					var remqty=(parseFloat(opentaskwt)/parseFloat(opentaskcube))*parseFloat(diffqty);
					var remqty1=(parseFloat(opentaskwt)/parseFloat(opentaskcube))*parseFloat(diffqty1);
					if((diffqty!='' && diffqty> 0)&&(searchResults[0] == null||searchResults[0] == ''))
					{
						nlapiLogExecution('DEBUG', 'creating diff', 'Record');
						var Remainqtyrecord =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',Recid);	
						Remainqtyrecord.setFieldValue('custrecord_act_qty', parseFloat(diffqty).toFixed(4)); 
						Remainqtyrecord.setFieldValue('custrecord_expe_qty', parseFloat(diffqty).toFixed(4)); 
						Remainqtyrecord.setFieldValue('custrecord_wms_status_flag', 8);
						Remainqtyrecord.setFieldValue('name', varname);
						//Remainqtyrecord.setFieldValue('custrecord_container_lp_no', contlp);
						Remainqtyrecord.setFieldValue('custrecord_container_lp_no', ContainerLPNo);
						Remainqtyrecord.setFieldValue('custrecord_pack_confirmed_date', null);
						Remainqtyrecord.setFieldValue('custrecord_total_weight', parseFloat(remqty).toFixed(4));
						Remainqtyrecord.setFieldValue('custrecord_totalcube', parseFloat(diffqty).toFixed(4));
						Remainqtyrecord.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', null);
						var rempick = nlapiSubmitRecord(Remainqtyrecord, false, true);
					}
					else if((diffqty1!='' && diffqty1> 0)&&(searchResults[0] != null||searchResults[0] != '')){
						nlapiLogExecution('DEBUG', 'creating diff', 'Record');
						var Remainqtyrecord =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',Recid);	
						Remainqtyrecord.setFieldValue('custrecord_act_qty', parseFloat(diffqty1).toFixed(4)); 
						Remainqtyrecord.setFieldValue('custrecord_expe_qty', parseFloat(diffqty1).toFixed(4)); 
						Remainqtyrecord.setFieldValue('custrecord_wms_status_flag', 8);
						Remainqtyrecord.setFieldValue('name', varname);
						//Remainqtyrecord.setFieldValue('custrecord_container_lp_no', contlp);
						Remainqtyrecord.setFieldValue('custrecord_container_lp_no', ContainerLPNo);
						Remainqtyrecord.setFieldValue('custrecord_pack_confirmed_date', null);
						Remainqtyrecord.setFieldValue('custrecord_total_weight', parseFloat(remqty1).toFixed(4));
						Remainqtyrecord.setFieldValue('custrecord_totalcube', parseFloat(diffqty1).toFixed(4));
						Remainqtyrecord.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', null);
						var rempick = nlapiSubmitRecord(Remainqtyrecord, false, true);
					}

				}

			}

			// if(isItLastPick=='T')
			// CreatePACKTask(null,null,OrderNo,OrderNo,ContainerLPNo,14,BeginLocation,EndLocation,28,DoLineId,ItemInternalId);//14 - Task Type - PACK
		}

		catch (e) 
		{
			if (e instanceof nlobjError) 
				nlapiLogExecution('DEBUG', 'system error', e.getCode() + '\n' + e.getDetails());
			else 
				nlapiLogExecution('DEBUG', 'unexpected error', e.toString());
		}
	}
}


function eBiz_RF_GetItemCubeForItemAlias(itemID,varitem){
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemCubeForItemAlias', 'Start');

	var BaseUOM=eBiz_RF_GetUOMAgainstItemAlias(itemID,varitem);
	var ItemInfo=new Array();
	var itemCube = 0;
	var BaseUomQty=0;
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemID);
	filters[1] = new nlobjSearchFilter('custrecord_ebizuomskudim', null, 'anyof', BaseUOM);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
	columns[1] = new nlobjSearchColumn('custrecord_ebizqty');

	var itemDimSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

	if(itemDimSearchResults != null && itemDimSearchResults.length > 0){
		itemCube = itemDimSearchResults[0].getValue('custrecord_ebizcube');
		//code added on 13 feb 2012 by suman
		//To get the baseuom qty .
		BaseUomQty=itemDimSearchResults[0].getValue('custrecord_ebizqty');
		//end of code as of 13 feb 2012.
	}
	ItemInfo.push(itemCube);
	ItemInfo.push(BaseUomQty);
	nlapiLogExecution('DEBUG', 'Retrieved Item Cube', 'Item Cube = ' + itemCube);
	nlapiLogExecution('DEBUG', 'Retrieved BaseUomQty', 'BaseUomQty = ' + BaseUomQty);
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemCubeForItemAlias', 'End');

	return ItemInfo;
}

function eBiz_RF_GetUOMAgainstItemAlias(itemID,varitem)
{
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetUOMAgainstItemAlias', 'Start');

	var baseuom = "";

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_item', null, 'anyof', itemID));
	filters.push(new nlobjSearchFilter('name', null, 'is', varitem));

	/*if(location!=null && location!='')
		filters.push(new nlobjSearchFilter('location', null, 'anyof', ['@NONE@',location]));*/

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_uom');

	var itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, filters, columns);

	if(itemSearchResults != null)
		baseuom = itemSearchResults[0].getValue('custrecord_ebiz_uom');

	var logMsg = 'baseuom = ' + baseuom;
	nlapiLogExecution('DEBUG', 'baseuom Retrieved', logMsg);
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetUOMAgainstItemAlias', 'End');

	return baseuom;
}



function CheckShortPicks(vlineno,LP,vItemId,fono)
{


	nlapiLogExecution('DEBUG', 'vlineno', vlineno);
	nlapiLogExecution('DEBUG', 'LP', LP);
	nlapiLogExecution('DEBUG', 'vItemId', vItemId);
	nlapiLogExecution('DEBUG', 'fono', fono);

	var arr1= new Array();

	var OpenTaskFilters = new Array();	

	OpenTaskFilters.push(new nlobjSearchFilter('name', null, 'is', fono));
	OpenTaskFilters.push(new nlobjSearchFilter('custrecord_line_no',null, 'is', vlineno));
	OpenTaskFilters.push(new nlobjSearchFilter('custrecord_sku',null, 'anyof', vItemId));
	OpenTaskFilters.push(new nlobjSearchFilter('custrecord_lpno',null, 'is', LP));
	OpenTaskFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [30]));//Outbound Shortpicks
	OpenTaskFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//pick

	var OpenTaskitemColumns = new Array();
	OpenTaskitemColumns[0] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	OpenTaskitemColumns[1] = new nlobjSearchColumn('custrecord_act_qty');
	OpenTaskitemColumns[2] = new nlobjSearchColumn('custrecord_expe_qty');
	OpenTaskitemColumns[3] = new nlobjSearchColumn('name');
	OpenTaskitemColumns[4] = new nlobjSearchColumn('custrecord_totalcube');
	OpenTaskitemColumns[5] = new nlobjSearchColumn('custrecord_total_weight');

	var OpenTaskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, OpenTaskFilters, OpenTaskitemColumns);
	//case # 20147993 
	if(OpenTaskSearchResults!=null && OpenTaskSearchResults!=''){
		var expqty=OpenTaskSearchResults[0].getValue('custrecord_expe_qty');

		arr1[0]=OpenTaskSearchResults;
		arr1[1]=expqty;
	}
	else{
		arr1[0]='';
		arr1[1]='0';
	}
	return arr1;
}
//Case# 20148702 starts
function getSalesOrderlist(orderno,ebizContlp,wmslocation){
	var ordertype='salesorder';
	var orderId=GetSOInternalId(orderno,ordertype);
	var filters = new Array();
	nlapiLogExecution('ERROR','orderno in getSalesOrderlist',orderno);
	nlapiLogExecution('DEBUG','ebizContlp',ebizContlp);
	nlapiLogExecution('DEBUG','wmslocation',wmslocation);
	var vRoleLocation=getRoledBasedLocation();
	nlapiLogExecution('ERROR','vRoleLocation',vRoleLocation);
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vRoleLocation));
	}
	//Case# 20148702
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof',['@NONE@',wmslocation]));
	}
	if(orderId!=""){
		filters.push( new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', orderId));
	}
	//case 20126716 start added Container LP filter
	if((ebizContlp!=null)&&(ebizContlp!=''))
	{
		filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', ebizContlp));
	}
	//case 20126716 end
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//Status Flag - C (Picks Confirmed)
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK
	filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'greaterthan', 0));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_line_no');
	columns[2] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[5] = new nlobjSearchColumn('custrecord_comp_id');
	columns[6] = new nlobjSearchColumn('custrecord_site_id');
	columns[7] = new nlobjSearchColumn('custrecord_container');
	columns[8] = new nlobjSearchColumn('custrecord_total_weight');
	columns[9] = new nlobjSearchColumn('custrecord_sku');
	columns[10] = new nlobjSearchColumn('custrecord_act_qty');
	columns[11] = new nlobjSearchColumn('custrecord_wms_location');
	columns[12] = new nlobjSearchColumn('custrecord_totalcube');
	columns[13] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
	/* The below code is merged from FactoryMation production account on 02-28-2013 by Radhika as part of Standard bundle*/	
	columns[14] = new nlobjSearchColumn('custrecord_parent_sku_no');
	columns[15] = new nlobjSearchColumn('custrecord_batch_no');
	/* Up to here */ 
	columns[0].setSort();	
	columns[1].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	
	return searchresults;
}
function GetSOInternalId(SOText,ordertype)
{
	nlapiLogExecution('ERROR','Into GetSOInternalId (Input)',SOText);
	nlapiLogExecution('ERROR','Into GetSOInternalId (ordertype)',ordertype);
	var ActualSoID='-1';
	var filter=new Array();
	filter.push(new nlobjSearchFilter('tranid',null,'is',SOText.toString()));
	filter.push(new nlobjSearchFilter('mainline',null,'is','F'));
	var columns=new Array();
	columns.push(new nlobjSearchColumn('shipmethod'));
	var searchrec=nlapiSearchRecord(ordertype,null,filter,columns);
	if(searchrec==null)
	{
		searchrec=nlapiSearchRecord('transferorder',null,filter,columns);
	}
	//case# 201410524 (Adding vendorreturnauthorization for search results)
	if(searchrec==null)
	{
		searchrec=nlapiSearchRecord('vendorreturnauthorization',null,filter,columns);
	}
	if(searchrec!=null && searchrec!='' && searchrec.length>0)
	{
		ActualSoID=searchrec[0].getId();
	}
	nlapiLogExecution('ERROR','Out of GetSOInternalId (Output)',ActualSoID);
	return ActualSoID;
}
