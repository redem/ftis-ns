/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/Attic/ebiz_WO_wave_release_QB.js,v $
 *     	   $Revision: 1.1.2.3.2.1 $
 *     	   $Date: 2015/12/02 14:54:33 $
 *     	   $Author: rmalraj $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_212 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 * *
 * DESCRIPTION
 *  	
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_WO_wave_release_QB.js,v $
 * Revision 1.1.2.3.2.1  2015/12/02 14:54:33  rmalraj
 * 201415201--work order wave creation and release screen changes
 *
 * Revision 1.1.2.3  2015/08/03 15:44:36  skreddy
 * Case# 201413753
 * 2015.2 compatibility issue fix
 *
 * Revision 1.1.2.2  2015/07/02 15:20:56  grao
 * SW issue fixes  201412999
 *
 * Revision 1.1.2.1  2015/02/13 08:00:36  skreddy
 * Case# 201410541
 * Work order location generation sch & wave generation for work order
 *
 * Revision 1.1.4.5.2.10.2.2  2014/09/18 13:40:27  sponnaganti
 * Case# 201410320
 * TPP SB Issue fix
 * 
 *
 **********************************************************************************************************************/


/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function WOWaveReleaseQB(request, response){
	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	context.setSessionObject('session', null);
	if (request.getMethod() == 'GET') {

		nlapiLogExecution('DEBUG', 'Time Stamp at the start of response',TimeStampinSec());

		var form = nlapiCreateForm('WO Wave Creation & Release');

		var woField = form.addField('custpage_qbwo', 'text', 'Work Order#');
		woField.setLayoutType('startrow', 'none');  		
 
		//Adding new filelds to WO form
		//var wojob= form.addField('custpage_qbwojob', 'select', 'Job/Project','job');
		var woprojectrouting= form.addField('custpage_qbwojobrouting', 'text', 'Project Routing');
		
		var woOrderType = form.addField('custpage_qbwoordertype', 'select', 'Order Type','customrecord_ebiznet_order_type');
		
		var woStartDate= form.addField('custpage_qbwostartdate', 'date', 'Start Date');
		var woToDate= form.addField('custpage_qbwoenddatedate', 'date', 'End Date');
		var woCustomer= form.addField('custpage_qbwocustomer', 'select', 'Customer','customer');
		var woManufactureRouting= form.addField('custpage_qbwomanufacturerouting', 'select', 'Manufacture Routing','manufacturingrouting');

		form.addSubmitButton('Display');
		nlapiLogExecution('DEBUG', 'Time Stamp at the end of response',TimeStampinSec());
		response.writePage(form);
	}
	else //this is the POST block
	{
		var vwo = request.getParameter('custpage_qbwo');
		//Additional fields 
		
		//var vwojob=request.getParameter('custpage_qbwojob');
		var vwoprojroute=request.getParameter('custpage_qbwojobrouting');
		var vwoordertype=request.getParameter('custpage_qbwoordertype');
		
		var vwostartdate=request.getParameter('custpage_qbwostartdate'); 
		var vwoenddate=request.getParameter('custpage_qbwoenddatedate'); 
		var vwocustomer=request.getParameter('custpage_qbwocustomer');
		var vwomanuroute=request.getParameter('custpage_qbwomanufacturerouting');
		 
		nlapiLogExecution('DEBUG','vwo',vwo);
		//nlapiLogExecution('DEBUG','vwojob',vwojob);
		nlapiLogExecution('DEBUG','vwoprojroute',vwoprojroute);
		nlapiLogExecution('DEBUG','vwoordertype',vwoordertype);
		nlapiLogExecution('DEBUG','vwostartdate',vwostartdate);
		nlapiLogExecution('DEBUG','vwoenddate',vwoenddate);
		nlapiLogExecution('DEBUG','vwocustomer',vwocustomer);
		nlapiLogExecution('DEBUG','vwomanuroute',vwomanuroute);
		var WaveQbparams = new Array();		
		if (vwo!=null &&  vwo != "") 
		{
			WaveQbparams["custpage_qbwo"] = vwo;
		}

		/*if (vwojob!=null &&  vwojob != "") 
		{
			WaveQbparams["custpage_qbwojob"] = vwojob;
		}*/
		
		if (vwoprojroute!=null &&  vwoprojroute != "") 
		{
			WaveQbparams["custpage_qbwojobrouting"] = vwoprojroute;
		}
		
		if (vwoordertype!=null &&  vwoordertype != "") 
		{
			WaveQbparams["custpage_qbwoordertype"] = vwoordertype;
		}
		
		
		if (vwostartdate!=null &&  vwostartdate != "") 
		{
			WaveQbparams["custpage_qbwostartdate"] = vwostartdate;
		}
		
		
		if (vwoenddate!=null &&  vwoenddate != "") 
		{
			WaveQbparams["custpage_qbwoenddatedate"] = vwoenddate;
		}
		
		if (vwocustomer!=null &&  vwocustomer != "") 
		{
			WaveQbparams["custpage_qbwocustomer"] = vwocustomer;
		}
		
		
		if (vwomanuroute!=null &&  vwomanuroute != "") 
		{
			WaveQbparams["custpage_qbwomanufacturerouting"] = vwomanuroute;
		}
		
		//end of the code
		response.sendRedirect('SUITELET', 'customscript_ebiz_wo_wave_release', 'customdeploy_ebiz_wo_wave_release', false, WaveQbparams );
		//response.sendRedirect('SUITELET', 'customscript_ebiz_hook_sl_7', 'customdeploy_ebiz_hook_sl_7', false, WaveQbparams );

	}
}
 
