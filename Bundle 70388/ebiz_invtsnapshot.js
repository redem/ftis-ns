/***************************************************************************
 eBizNET Solutions               
 ***************************************************************************
 **     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/Attic/ebiz_invtsnapshot.js,v $
 *     	   $Revision: 1.1.2.3 $
 *     	   $Date: 2014/09/18 14:56:27 $
 *     	   $Author: snimmakayala $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_invtsnapshot.js,v $
 * Revision 1.1.2.3  2014/09/18 14:56:27  snimmakayala
 * no message
 *
 * Revision 1.1.2.2  2014/07/02 13:39:35  snimmakayala
 * Case # : 20149246
 * Inventory Snap Shot Scheduler
 *
 * Revision 1.1.2.1  2013/10/15 06:29:41  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 20124920
 * Inventory Snapshot Scheduler
 *
 * Revision 1.1.2.1  2013/06/11 12:50:31  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Place Holders for Schedulers and Restlets
 *

 *****************************************/
function eBiz_Invt_Snapshot(type) 
{	

	var context = nlapiGetContext();

	nlapiLogExecution('DEBUG','Remaining Usage 1',context.getRemainingUsage());

	var vGetHours=GetHours();
	var vProcessSchedulerFlag='F';

	if(vGetHours == null || vGetHours == '')
	{
		vGetHours=0;
	}
	nlapiLogExecution('DEBUG','vGetHours',vGetHours);

	if(parseInt(vGetHours) >= 23 )
	{
		nlapiLogExecution('DEBUG','vGetHours2',vGetHours);		
		vProcessSchedulerFlag='T';	
	}
	else
	{
		nlapiLogExecution('DEBUG','vGetHours3',vGetHours);
		vProcessSchedulerFlag='F';	
		updateSeqNo('INVTSNAP',0);
	}

	nlapiLogExecution('DEBUG','vProcessSchedulerFlag',vProcessSchedulerFlag);

	if(vProcessSchedulerFlag=='T')
	{
		var vintrid = GetMaxRecordId('INVTSNAP');

		nlapiLogExecution('DEBUG','vintrid',vintrid);

		var vSearchresultsInv=new Array();

		var invtSearchResults = getInventoryDetails(vintrid, vSearchresultsInv);

		nlapiLogExecution('DEBUG','invtSearchResults',invtSearchResults);
		if(invtSearchResults!=null && invtSearchResults!='')
		{

			nlapiLogExecution('DEBUG','invtSearchResults length',invtSearchResults.length);
			var vinvtid = 0;

			var now = new Date();
			var month=now.getMonth() + 1;
			var date=now.getDate();
			var year=now.getFullYear().toString(10).substring(2, 4);
			var curr_hour = now.getHours();
			var curr_min = now.getMinutes();
			var curr_sec = now.getSeconds();
			var snapid = month+''+date+''+year+''+curr_hour+''+curr_min;//+''+curr_sec;

			for (var t = 0; t < invtSearchResults.length; t++) 
			{
				try
				{
					var vinvtid = invtSearchResults[t].getId();

					nlapiLogExecution('DEBUG','Remaining usage',context.getRemainingUsage());

					var exprdate=invtSearchResults[t].getValue('custrecord_ebiz_expdate');
					var binlocation=invtSearchResults[t].getValue('custrecord_ebiz_inv_binloc');
					var avlqty=invtSearchResults[t].getValue('custrecord_ebiz_avl_qty');
					var allocqty=invtSearchResults[t].getValue('custrecord_ebiz_alloc_qty');
					var qoh=invtSearchResults[t].getValue('custrecord_ebiz_qoh');
					var fifo=invtSearchResults[t].getValue('custrecord_ebiz_inv_fifo');
					var lot=invtSearchResults[t].getValue('custrecord_ebiz_inv_lot');
					var item=invtSearchResults[t].getValue('custrecord_ebiz_inv_sku');
					var lp=invtSearchResults[t].getValue('custrecord_ebiz_inv_lp');
					var skustatus=invtSearchResults[t].getValue('custrecord_ebiz_inv_sku_status');
					var whloc=invtSearchResults[t].getValue('custrecord_ebiz_inv_loc');
					var packcode=invtSearchResults[t].getValue('custrecord_ebiz_inv_packcode');
					var masterlp=invtSearchResults[t].getValue('custrecord_ebiz_inv_masterlp');
					var status=invtSearchResults[t].getValue('custrecord_wms_inv_status_flag');
					var company=invtSearchResults[t].getValue('custrecord_ebiz_inv_company');
					var note1=invtSearchResults[t].getValue('custrecord_ebiz_inv_note1');
					var note2=invtSearchResults[t].getValue('custrecord_ebiz_inv_note2');
					var invholdflag=invtSearchResults[t].getValue('custrecord_ebiz_invholdflg');
					var uomlvl=invtSearchResults[t].getValue('custrecord_ebiz_uomlvl');
					var cyccholdflag=invtSearchResults[t].getValue('custrecord_ebiz_cycl_count_hldflag');
					var model=invtSearchResults[t].getValue('custrecord_ebiz_model');
					var tasktype=invtSearchResults[t].getValue('custrecord_invttasktype');
					var transaction=invtSearchResults[t].getValue('custrecord_ebiz_transaction_no');
					var binlocname = invtSearchResults[t].getText('custrecord_ebiz_inv_binloc');
					var itemname= invtSearchResults[t].getText('custrecord_ebiz_inv_sku');

					var invtRec = nlapiCreateRecord('customrecord_ebiz_inventory_sanpshot');	

					invtRec.setFieldValue('name', vinvtid);
					invtRec.setFieldValue('custrecord_ebiz_invsnap_snapid', parseInt(snapid));
					invtRec.setFieldValue('custrecord_ebiz_invsnap_binlocation', binlocation);
					invtRec.setFieldValue('custrecord_ebiz_invsnap_masterlp', masterlp);
					invtRec.setFieldValue('custrecord_ebiz_invsnap_lp', lp);
					invtRec.setFieldValue('custrecord_ebiz_invsnap_item', item);
					invtRec.setFieldValue('custrecord_ebiz_invsnap_itemstatus', skustatus);
					invtRec.setFieldValue('custrecord_ebiz_invsnap_packcode', packcode);
					invtRec.setFieldValue('custrecord_ebiz_invsnap_lot', lot);
					invtRec.setFieldValue('custrecord_ebiz_invsnap_fifodate', fifo);
					invtRec.setFieldValue('custrecord_ebiz_invsnap_company', company);
					invtRec.setFieldValue('custrecord_ebiz_invsnap_location', whloc);
					invtRec.setFieldValue('custrecord_ebiz_invsnap_note1', note1);
					invtRec.setFieldValue('custrecord_ebiz_invsnap_note2', note2);
					invtRec.setFieldValue('custrecord_ebiz_invsnap_qtyallocated', allocqty);
					invtRec.setFieldValue('custrecord_ebiz_invsnap_invtholdflag', invholdflag);
					invtRec.setFieldValue('custrecord_ebiz_invsnap_uomlevel', uomlvl);
					invtRec.setFieldValue('custrecord_ebiz_invsnap_cyccholdflag', cyccholdflag);
					invtRec.setFieldValue('custrecord_ebiz_invsnap_qtyonhand', qoh);
					invtRec.setFieldValue('custrecord_ebiz_invsnap_modelno', model);
					invtRec.setFieldValue('custrecord_ebiz_invsnap_wmsstatusflag', status);
					invtRec.setFieldValue('custrecord_ebiz_invsnap_tasktype', tasktype);
					invtRec.setFieldValue('custrecord_ebiz_invsnap_qtyavailable', avlqty);
					invtRec.setFieldValue('custrecord_ebiz_invsnap_transactionno', transaction);
					invtRec.setFieldValue('custrecord_ebiz_invsnap_date', DateStamp());

					var invtrecid = nlapiSubmitRecord(invtRec, false, true);	

					if(context.getRemainingUsage()<50 || t==invtSearchResults.length-1)
					{				
						nlapiLogExecution('DEBUG','vinvtid',vinvtid);
						updateSeqNo('INVTSNAP',vinvtid);
						return;
					}

				}
				catch(exp)
				{
					nlapiLogExecution('DEBUG','Exception in creating snap record',exp);
					InsertExceptionLog('Inventory Snapshot',3, 'Inventory Snapshot', exp, lp, binlocname,itemname,vinvtid,null, nlapiGetContext().getUser())
				}
			}

			//updateSeqNo('INVTSNAP',vinvtid);

		}
	}
} 

function updateSeqNo(transType,seqno){
	nlapiLogExecution('DEBUG', 'Into updateSeqNo', seqno);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_seqno');

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_trantype', null, 'is', transType);

	// Search in ebiznet_wave custom record for all rows for the given transaction type
	var results = nlapiSearchRecord('customrecord_ebiznet_wave', null, filters, columns);
	if(results != null){
		for (var p = 0; p < results.length; p++) {
			nlapiSubmitField('customrecord_ebiznet_wave', results[p].getId(), 'custrecord_seqno', parseFloat(seqno));
		}
	}
	else
	{
		var seqRec = nlapiCreateRecord('customrecord_ebiznet_wave');	

		seqRec.setFieldValue('name', transType);
		seqRec.setFieldValue('custrecord_trantype', transType);
		seqRec.setFieldValue('custrecord_seqno', 0);

		var seqRecid = nlapiSubmitRecord(seqRec, false, true);	
	}

	nlapiLogExecution('DEBUG', 'Out of updateSeqNo', seqno);
}


function resetSeqNo(type)
{
	nlapiLogExecution('DEBUG', 'Into resetSeqNo');
	updateSeqNo('INVTSNAP',0);
	nlapiLogExecution('DEBUG', 'Out of resetSeqNo');
}

var allinvdet = new Array();

function getInventoryDetails(maxno,vSearchresults) 
{

	var result = nlapiLoadSearch('customrecord_ebiznet_createinv', 'customsearch_wms_invtsearch_snapshot');

	result.addFilter(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3,19]));
	result.addFilter(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	if(maxno != -1)
	{
		result.addFilter(new nlobjSearchFilter('internalidnumber', null, 'greaterthan', parseInt(maxno)));
	}

	var resLen = result.runSearch();

	var q = 0;
	var resultSet = resLen.forEachResult(function(searchResult)
			{
		q++;
		vSearchresults.push(searchResult);	
		if(q == 4000)
		{
			maxno = searchResult.getValue('internalid');
			nlapiLogExecution('ERROR', 'maxno',maxno);
			getInventoryDetails(maxno,vSearchresults);
			return false;
		}
		return true;
			}
	);
	nlapiLogExecution('ERROR', 'vSearchresults',vSearchresults);

	return vSearchresults;



	/*var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3,19]));

	if(parseFloat(maxid)>1)
	{
		nlapiLogExecution('DEBUG','In side internalid in search filter',maxid);
		filters.push(new nlobjSearchFilter('internalidnumber', null, 'lessthan', maxid));
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_expdate');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_avl_qty');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
	columns[5] = new nlobjSearchColumn('custrecord_ebiz_qoh');
	columns[6] = new nlobjSearchColumn('custrecord_ebiz_inv_fifo');	
	columns[7] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');
	columns[8] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[9] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columns[10] = new nlobjSearchColumn('custrecord_ebiz_inv_sku_status');
	columns[11] = new nlobjSearchColumn('custrecord_ebiz_inv_loc');
	columns[12] = new nlobjSearchColumn('custrecord_ebiz_inv_packcode');
	columns[13] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columns[14] = new nlobjSearchColumn('custrecord_ebiz_inv_masterlp');
	columns[15] = new nlobjSearchColumn('custrecord_wms_inv_status_flag');
	columns[16] = new nlobjSearchColumn('custrecord_ebiz_inv_company');
	columns[17] = new nlobjSearchColumn('custrecord_ebiz_inv_note1');
	columns[18] = new nlobjSearchColumn('custrecord_ebiz_inv_note2');
	columns[19] = new nlobjSearchColumn('custrecord_ebiz_invholdflg');
	columns[20] = new nlobjSearchColumn('custrecord_ebiz_uomlvl');
	columns[21] = new nlobjSearchColumn('custrecord_ebiz_cycl_count_hldflag');
	columns[22] = new nlobjSearchColumn('custrecord_ebiz_itemdesc');
	columns[23] = new nlobjSearchColumn('custrecord_ebiz_model');
	columns[24] = new nlobjSearchColumn('custrecord_invttasktype');
	columns[25] = new nlobjSearchColumn('custrecord_ebiz_transaction_no');

	columns[0].setSort(true);

	var invtSearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

	if(invtSearchResults!=null)
	{
		nlapiLogExecution('ERROR','invtSearchResults',invtSearchResults.length);

		if(invtSearchResults.length>=1000)
		{
			var maxno1=invtSearchResults[invtSearchResults.length-1].getValue('internalid');
			for(k=0;k<invtSearchResults.length;k++)
			{
				allinvdet.push(invtSearchResults[k]);
			}

			if(allinvdet!=null && allinvdet.length<5000)
			{
				getInventoryDetails(maxno1);
			}
		}
		else
		{			
			for(k=0;k<invtSearchResults.length;k++)
			{
				allinvdet.push(invtSearchResults[k]);
			}
		}
	}

	return allinvdet;

	 */
}

function GetHours()
{
	var now = new Date();	  
	var curr_hour = now.getHours();
	nlapiLogExecution('Debug', 'curr_hour : ', curr_hour);
	if(curr_hour != null && curr_hour != '')//Converting to EST time
	{	
		curr_hour=parseInt(curr_hour);// +3;
		if(parseInt(curr_hour)>=24)
		{
			curr_hour=parseInt(curr_hour)-24;

		}
		nlapiLogExecution('Debug', 'New curr_hour : ', curr_hour);

	}
	return curr_hour;
}

function GetMaxRecordId(transType){
	var clusterNo = 1;

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_seqno');

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_trantype', null, 'is', transType);

	// Search in ebiznet_wave custom record for all rows for the given transaction type
	var results = nlapiSearchRecord('customrecord_ebiznet_wave', null, filters, columns);
	if(results != null){
		for (var t = 0; t < results.length; t++) {
			clusterNo = results[t].getValue('custrecord_seqno');
			// Increment cluster number
			//clusterNo = parseFloat(clusterNo) + 1;
			clusterNo = parseFloat(clusterNo);
			nlapiSubmitField('customrecord_ebiznet_wave', results[t].getId(), 'custrecord_seqno', parseFloat(clusterNo));
		}
	}
	else
	{
		var seqRec = nlapiCreateRecord('customrecord_ebiznet_wave');	

		seqRec.setFieldValue('name', transType);
		seqRec.setFieldValue('custrecord_trantype', transType);
		seqRec.setFieldValue('custrecord_seqno', 0);

		var seqRecid = nlapiSubmitRecord(seqRec, false, true);	
	}

	nlapiLogExecution('Debug', 'GetMaxTransactionNo:New Cluster No.', clusterNo);
	return parseFloat(clusterNo);
}