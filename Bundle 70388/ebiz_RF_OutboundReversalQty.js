/***************************************************************************
  		   eBizNET Solutions Inc               
 ***************************************************************************
 **     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_OutboundReversalQty.js,v $
 *     	   $Revision: 1.1.2.5.4.3.4.4 $
 *     	   $Date: 2014/06/13 13:01:33 $
 *     	   $Author: skavuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_OutboundReversalQty.js,v $
 * Revision 1.1.2.5.4.3.4.4  2014/06/13 13:01:33  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.5.4.3.4.3  2014/05/30 00:41:02  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.5.4.3.4.2  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.2.5.4.3.4.1  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.5.4.3  2012/12/06 07:16:22  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Update Stage Inventory.
 *
 * Revision 1.1.2.5.4.2  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.1.2.5.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.1.2.5  2012/07/12 14:20:01  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Default Focus issues.
 *
 * Revision 1.1.2.4  2012/06/25 14:54:17  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 * Revision 1.1.2.3  2012/06/25 11:32:15  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 * Revision 1.1.2.2  2012/06/22 12:27:29  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 * Revision 1.1.2.1  2012/06/21 10:17:12  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 *****************************************************************************/

function OutboundReversalQty(request, response)
{
	if (request.getMethod() == 'GET') 
	{	
		nlapiLogExecution('DEBUG', 'Into Request','Into Request');
		
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		
		
		var st1,st2,st3,st4,st5,st6,st7,st8;
		
		if( getLanguage == 'es_ES')
		{
			st1 = "CANT INVERSI&#211;N DE ALEJAMIENTO";
			st2 = "VENTAS DE PEDIDO # :";
			st3 = "TEMA :";
			st4 = "UBICACI&#211;N : ";
			st5 = "PACKCODE :";
			st6 = "INVERSI&#211;N DE CANT:";
			st7 = "ENVIAR";
			st8 = "ANTERIOR";
			
			
			
		}
		else
		{
			st1 = "OUTBOUND REVERSAL QTY";
			st2 = "SALES ORDER # :";
			st3 = "ITEM :";
			st4 = "LOCATION :";
			st5 = "PACKCODE :";
			st6 = "REVERSAL QTY : ";
			st7 = "SEND";
			st8 = "PREV";
			
			
			
		}

		var getItemName = request.getParameter('custparam_uccitem');
		var getItemIntrId = request.getParameter('custparam_uccebizitem');
		var getPackcode = request.getParameter('custparam_uccpackcode');
		var getActQty = request.getParameter('custparam_uccactqty');
		var getOrdNo = request.getParameter('custparam_uccordno');
		var getOrdIntrId = request.getParameter('custparam_uccebizordno');
		var getOrdLine = request.getParameter('custparam_uccordlineno');
		var getWMSStatus = request.getParameter('custparam_uccwmsstatus');
		var getShipLpNo = request.getParameter('custparam_uccshiplpno');
		var getLocation = request.getParameter('custparam_ucclocation');        
		var getCompany = request.getParameter('custparam_ucccompany');
		var getNSRefNo = request.getParameter('custparam_uccnsrefno');
		var getInvRefNo = request.getParameter('custparam_uccinvrefno');
		var getFOIntrId = request.getParameter('custparam_uccfointrid');
		var getTaskIntrId=request.getParameter('custparam_ucctaskintrid');
		var getFoName=request.getParameter('custparam_uccname');
		var getTaskWeight=request.getParameter('custparam_ucctaskweight');
		var getTaskCube=request.getParameter('custparam_ucctaskcube');
		var getUOMLevel=request.getParameter('custparam_uccuomlevel');
		var getLPrecid=request.getParameter('custparam_ucclprecid');
		var getEndLocation = request.getParameter('custparam_uccendlocation');
		var getCartonNo = request.getParameter('custparam_ucccartonno');
		
		nlapiLogExecution('DEBUG', 'getFoName',getFoName);

		var salesordno='';
		if(getFoName!=null && getFoName!='')
			salesordno = getFoName.split('.')[0];
		else
			salesordno=getOrdNo;
		
		nlapiLogExecution('DEBUG', 'salesordno',salesordno);

		var functionkeyHtml=getFunctionkeyScript('_rfpickingmenu'); 
		var html = "<html><head><title>"+ st1 +"</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('cmdSend').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rfpickingmenu' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st2 +"<label>" + salesordno + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 +"<label>" + getItemName + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st4 +"<label>" + getEndLocation + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st5 +"<label>" + getPackcode + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st6 +"<label>" + getPackcode + "</label>";
		html = html + "				<input type='hidden' name='hdnOrdNo' value=" + getOrdNo + ">";
		html = html + "				<input type='hidden' name='hdnItemName' value=" + getItemName + ">";
		html = html + "				<input type='hidden' name='hdnPackcode' value=" + getPackcode + ">";
		html = html + "				<input type='hidden' name='hdnItemIntrId' value=" + getItemIntrId + ">";
		html = html + "				<input type='hidden' name='hdnActQty' value=" + getActQty + ">";
		html = html + "				<input type='hidden' name='hdnOrdIntrId' value=" + getOrdIntrId + ">";
		html = html + "				<input type='hidden' name='hdnOrdLine' value=" + getOrdLine + ">";
		html = html + "				<input type='hidden' name='hdnWMSStatus' value=" + getWMSStatus + ">";
		html = html + "				<input type='hidden' name='hdnShipLpNo' value=" + getShipLpNo + ">";
		html = html + "				<input type='hidden' name='hdnLocation' value=" + getLocation + ">";
		html = html + "				<input type='hidden' name='hdnCompany' value=" + getCompany + ">";
		html = html + "				<input type='hidden' name='hdnNSRefNo' value=" + getNSRefNo + ">";
		html = html + "				<input type='hidden' name='hdnInvRefNo' value=" + getInvRefNo + ">";
		html = html + "				<input type='hidden' name='hdnFOIntrId' value=" + getFOIntrId + ">";
		html = html + "				<input type='hidden' name='hdnTaskIntrId' value=" + getTaskIntrId + ">";
		html = html + "				<input type='hidden' name='hdnFoName' value=" + getFoName + ">";
		html = html + "				<input type='hidden' name='hdnTaskWeight' value=" + getTaskWeight + ">";
		html = html + "				<input type='hidden' name='hdnTaskCube' value=" + getTaskCube + ">";
		html = html + "				<input type='hidden' name='hdnUOMLevel' value=" + getUOMLevel + ">";
		html = html + "				<input type='hidden' name='hdnlprecid' value=" + getLPrecid + ">";
		html = html + "				<input type='hidden' name='hdnendlocation' value=" + getEndLocation + ">";
		html = html + "				<input type='hidden' name='hdncartonno' value=" + getCartonNo + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st7 +" <input name='cmdSend' id='cmdSend' type='submit' value='F8' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					"+ st8 +" <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('cmdSend').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else
	{
		nlapiLogExecution('DEBUG', 'Into Response','Into Response');

		var getLanguage =  request.getParameter('hdngetLanguage');
		
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		
		
		var st5;
		
		if( getLanguage == 'es_ES')
		{
			
			st5 = "V&#193;LIDA LA ETIQUETA UCC.";
			
			
		}
		else
		{	st5 = "INVALID UCC LABEL";
			
			
		}

		var optedEvent = request.getParameter('cmdPrevious');


		nlapiLogExecution('DEBUG', 'optedEvent',optedEvent);

		var UCCarray=new Array();
		UCCarray["custparam_language"] = getLanguage;
		UCCarray["custparam_screenno"] = 'UCC2';
		UCCarray["custparam_uccitem"] = request.getParameter('hdnItemName');
		UCCarray["custparam_uccebizitem"] = request.getParameter('hdnItemIntrId');
		UCCarray["custparam_uccpackcode"] = request.getParameter('hdnPackcode');
		UCCarray["custparam_uccactqty"] = request.getParameter('hdnActQty');
		UCCarray["custparam_uccordno"] = request.getParameter('hdnOrdNo');
		UCCarray["custparam_uccebizordno"] = request.getParameter('hdnOrdIntrId');
		UCCarray["custparam_uccordlineno"] = request.getParameter('hdnOrdLine');
		UCCarray["custparam_uccwmsstatus"] = request.getParameter('hdnWMSStatus');
		UCCarray["custparam_uccshiplpno"] = request.getParameter('hdnShipLpNo');
		UCCarray["custparam_ucclocation"] = request.getParameter('hdnLocation');
		UCCarray["custparam_ucccompany"] = request.getParameter('hdnCompany');
		UCCarray["custparam_uccnsrefno"] = request.getParameter('hdnNSRefNo');
		UCCarray["custparam_uccinvrefno"] = request.getParameter('hdnInvRefNo');
		UCCarray["custparam_uccfointrid"] = request.getParameter('hdnFOIntrId');
		UCCarray["custparam_ucctaskintrid"] = request.getParameter('hdnTaskIntrId');
		UCCarray["custparam_uccname"] = request.getParameter('hdnFoName');
		UCCarray["custparam_ucctaskweight"] = request.getParameter('hdnTaskWeight');
		UCCarray["custparam_ucctaskcube"] = request.getParameter('hdnTaskCube');
		UCCarray["custparam_uccuomlevel"] = request.getParameter('hdnUOMLevel');
		UCCarray["custparam_ucclprecid"] = request.getParameter('hdnlprecid');
		UCCarray["custparam_uccendlocation"] = request.getParameter('hdnendlocation');
		UCCarray["custparam_ucccartonno"] = request.getParameter('hdncartonno');

		if (request.getParameter('cmdPrevious') == 'F7') {
			response.sendRedirect('SUITELET', 'customscript_rf_scanucclabel', 'customdeploy_rf_scanucclabel', false, optedEvent);
		}
		else{

			var nsrefno = request.getParameter('hdnNSRefNo');
			var ordlineno = request.getParameter('hdnOrdLine');
			var reversalqty = request.getParameter('hdnPackcode');
			var invrefno = request.getParameter('hdnInvRefNo');
			var taskid = request.getParameter('hdnTaskIntrId');
			var foid = request.getParameter('hdnFOIntrId');
			var taskactqty = request.getParameter('hdnActQty');
			var taskweight = request.getParameter('hdnTaskWeight');
			var taskcube = request.getParameter('hdnTaskCube');
			var taskuomlevel = request.getParameter('hdnUOMLevel');
			var itemid=request.getParameter('hdnItemIntrId');
			var lprecid = request.getParameter('hdnlprecid');
			var ordintrid = request.getParameter('hdnOrdIntrId');
			var cartonno = request.getParameter('hdncartonno');
			
			updateItemFulfillment(nsrefno,ordlineno,reversalqty);
			updateOpentaskandInventory(invrefno,reversalqty,taskid,taskactqty,taskweight,taskcube,taskuomlevel,itemid,lprecid,ordintrid);
			updateFulfillmentOrder(foid,ordlineno,reversalqty);		
			ClearOutBoundInventory(cartonno,reversalqty,itemid);
			response.sendRedirect('SUITELET', 'customscript_rf_completereversal', 'customdeploy_rf_completereversal', false, UCCarray);
		}
		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}
}

function ClearOutBoundInventory(contlp,taskqty,itemid)
{
	nlapiLogExecution('DEBUG', 'Into ClearOutBoundInventory');

	var str = 'contlp. = ' + contlp + '<br>';
	str = str + 'taskqty. = ' + taskqty + '<br>';
	str = str + 'itemid. = ' + itemid + '<br>';	

	nlapiLogExecution('DEBUG', 'ClearOutBoundInventory Parameters', str);

	try
	{
		if(contlp!=null && contlp!='')
		{
			var filters = new Array();
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', contlp));
			//18-Inventory at Shipping Dock.
			filters.push(new nlobjSearchFilter( 'custrecord_wms_inv_status_flag', null, 'anyof', [18]));
			filters.push(new nlobjSearchFilter( 'custrecord_ebiz_inv_sku', null, 'anyof', itemid));

			var columns=new Array();
			columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');

			var inventorysearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv',null,filters,columns);
			if(inventorysearchresults!=null && inventorysearchresults!='' && inventorysearchresults.length>0)
			{
				for(var k=0;k < inventorysearchresults.length; k++)
				{
					var obqoh = inventorysearchresults[k].getValue('custrecord_ebiz_qoh');
					
					if(isNaN(obqoh))
						obqoh=0;

					var newqty = parseInt(obqoh)-parseInt(taskqty);

					if(isNaN(newqty))
						newqty=0;

					nlapiLogExecution('DEBUG', 'newqty', newqty);

					nlapiLogExecution('DEBUG', 'id', inventorysearchresults[k].getId());

					if(newqty<=0)
					{
						nlapiDeleteRecord('customrecord_ebiznet_createinv', inventorysearchresults[k].getId());
						nlapiLogExecution('DEBUG', 'Outbound Inventory Deleted');
					}
					else
					{
						nlapiSubmitField('customrecord_ebiznet_createinv', inventorysearchresults[k].getId(), 'custrecord_ebiz_qoh', newqty);
					}
				}
			}
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in ClearOutBoundInventory', exp);
	}

	nlapiLogExecution('DEBUG', 'Out of of ClearOutBoundInventory');
}