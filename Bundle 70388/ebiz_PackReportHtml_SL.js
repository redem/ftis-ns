/***************************************************************************
	 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_PackReportHtml_SL.js,v $
<<<<<<< ebiz_PickReportPDF_SL.js
 *     	   $Revision: 1.1.2.1 $
 *     	   $Date: 2015/05/07 14:38:02 $
 *     	   $Author: skreddy $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
=======
 *     	   $Revision: 1.1.2.1 $
 *     	   $Date: 2015/05/07 14:38:02 $
 *     	   $Author: skreddy $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
>>>>>>> 1.32.2.9.4.5.4.8.2.1
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_PackReportHtml_SL.js,v $
 * Revision 1.1.2.1  2015/05/07 14:38:02  skreddy
 * Case# 201412597
 * Packlist Printing after Packing completed
 *
 * Revision 1.32.2.9.4.5.4.8.2.2  2014/03/13 20:49:28  grao
 * Case# 20127718 related issue fixes in Leisure Living issue fixes
 *
 * Revision 1.32.2.9.4.5.4.8.2.1  2014/03/13 15:02:33  snimmakayala
 * Case #: 201218745
 *
 * Revision 1.32.2.9.4.5.4.8  2013/09/13 15:46:34  nneelam
 * Case#. 20124394
 * Wave Report  Issue Fix..
 *
 * Revision 1.32.2.9.4.5.4.7  2013/09/06 23:32:43  nneelam
 * Case#.20124184
 * Pick Report PDF Issue Fix..
 *
 * Revision 1.32.2.9.4.5.4.6  2013/09/03 00:04:37  nneelam
 * Case#.20124229Ã¯Â¿Â½
 * Pic Report Issue Fixed...
 *
 * Revision 1.32.2.9.4.5.4.5  2013/08/23 15:09:00  rmukkera
 * Case# 20123995Ã¯Â¿Â½
 *
 * Revision 1.32.2.9.4.5.4.4  2013/04/16 13:23:55  spendyala
 * CASE201112/CR201113/LOG2012392
 * Issue fixes.
 *
 * Revision 1.32.2.9.4.5.4.3  2013/04/02 16:02:08  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * TSg Issue fixes
 *
 * Revision 1.32.2.9.4.5.4.2  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.32.2.9.4.5.4.1  2013/03/01 14:34:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.32.2.9.4.5  2013/01/25 06:52:31  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related displaying wrong weight
 *
 * Revision 1.32.2.9.4.4  2012/12/06 07:18:16  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Performance Fine tuning.
 *
 * Revision 1.32.2.9.4.3  2012/11/27 19:08:29  spendyala
 * CASE201112/CR201113/LOG201121
 * Added new column i.e. no of cases to the report
 *
 * Revision 1.32.2.9.4.2  2012/11/08 14:01:30  schepuri
 * CASE201112/CR201113/LOG201121
 * added special instruction field
 *
 * Revision 1.32.2.9.4.1  2012/10/26 08:05:40  gkalla
 * CASE201112/CR201113/LOG201121
 * Pick report by order sorting
 *
 * Revision 1.32.2.9  2012/08/10 09:32:24  spendyala
 * CASE201112/CR201113/LOG201121
 * Replacing Special character with ASCII code.
 *
 * Revision 1.32.2.8  2012/08/01 07:29:40  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Routing
 *
 * Revision 1.32.2.7  2012/07/04 09:40:09  gkalla
 * CASE201112/CR201113/LOG201121
 * Added Addr2 in ship address
 *
 * Revision 1.32.2.6  2012/07/03 21:51:21  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Remove Special Characters.
 *
 * Revision 1.32.2.5  2012/05/11 14:52:19  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to SKU bar code printing.
 *
 * Revision 1.32.2.4  2012/04/20 14:23:39  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.32.2.3  2012/04/20 08:36:21  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Zone Picking
 *
 * Revision 1.32.2.2  2012/02/21 06:54:07  spendyala
 * CASE201112/CR201113/LOG201121
 * Code syn b/w Trunck and Branch.
 *
 * Revision 1.32.2.1  2012/02/08 12:35:51  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge to brach
 *
 * Revision 1.33  2012/01/13 00:34:18  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Pickreport
 *
 * Revision 1.32  2011/12/30 16:13:02  spendyala
 * CASE201112/CR201113/LOG201121
 * added fulfilment order to sublist and barcode to fulfillment order.
 *
 * Revision 1.31  2011/12/23 14:06:10  gkalla
 * CASE201112/CR201113/LOG201121
 * Added barcode and dynamic logo
 *
 * Revision 1.30  2011/12/23 12:45:10  gkalla
 * CASE201112/CR201113/LOG201121
 * Added barcode and dynamic logo
 *
 *****************************************************************************/
/**
 * This is the main function to print the PICK LIST against the given wave number  
 */
function PickReportPDFNEWSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		
		
		var newContainerLpNo=request.getParameter('custparam_ebiz_containerlp');
		var htmlstring="";
		htmlstring=gethtmlstringbyLp(newContainerLpNo);
		
		
		
		//start of getting logo from filecabinet
		var url;
		var ctx = nlapiGetContext();
		nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
		if (ctx.getEnvironment() == 'PRODUCTION') 
		{
			url = 'https://system.netsuite.com';			
		}
		else if (ctx.getEnvironment() == 'SANDBOX') 
		{
			url = 'https://system.sandbox.netsuite.com';				
		}
		nlapiLogExecution('ERROR', 'PDF URL',url);
		var imageurl;
		var filefound = nlapiLoadFile('Images/LOGOCOMP.jpg'); 
		if (filefound) 
		{ 
			nlapiLogExecution('ERROR', 'Event', 'file;'+filefound.getId()); 
			imageurl = filefound.getURL();
			nlapiLogExecution('ERROR','imageurl',imageurl);
			imageurl = url + imageurl;//+';';
			//finalimageurl=finalimageurl+ '&expurl=T;';
			nlapiLogExecution('ERROR','imageurl',imageurl);
			imageurl=imageurl.replace(/&/g,"&amp;");

		} 
		//End of getting logo from filecabinet


		//start of adding Print button for packlist printing
		
		
		if(htmlstring!=null && htmlstring!="")
		{
			htmlstring=htmlstring.replace('headerimage',imageurl);
		}

		
		nlapiLogExecution('ERROR', 'htmlstring', htmlstring);
		
		
		
		response.write( htmlstring );
	}
	else //this is the POST block
	{

	}
}

//start of getting the Html string by ContainerLp
function gethtmlstringbyLp(newContainerLpNo)
{
	var searchfilter=new Array();
	searchfilter.push(new nlobjSearchFilter('custrecord_label_lp',null,'is',newContainerLpNo));
	searchfilter.push(new nlobjSearchFilter('custrecord_labeltype',null,'is','PackList'));
	nlapiLogExecution('ERROR', 'Packlist ContainerLp', newContainerLpNo);

	var searchcolumn=new Array();
	searchcolumn[0]=new nlobjSearchColumn('custrecord_labeldata');

	var Labelprintingsearchrec=nlapiSearchRecord('customrecord_labelprinting',null,searchfilter,searchcolumn);
	nlapiLogExecution('ERROR', 'Labelprintingsearchrec', Labelprintingsearchrec);
	var htmlstring="";
	if(Labelprintingsearchrec!=null && Labelprintingsearchrec!="")
		htmlstring=Labelprintingsearchrec[0].getValue('custrecord_labeldata');
	return htmlstring;

}
//End of getting the Html string by ContainerLp


