/***************************************************************************
  	eBizNET Solutions               
 ***************************************************************************
 **     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_WO_PickingConfirm.js,v $
 *     	   $Revision: 1.1.2.4.2.13.2.1 $
 *     	   $Date: 2015/11/04 07:12:40 $
 *     	   $Author: nneelam $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_WO_PickingConfirm.js,v $
 * Revision 1.1.2.4.2.13.2.1  2015/11/04 07:12:40  nneelam
 * case# 201415259
 *
 * Revision 1.1.2.4.2.13  2015/04/13 09:25:58  rrpulicherla
 * Case#201412277
 *
 * Revision 1.1.2.4.2.12  2014/08/29 17:15:37  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#201410172
 *
 * Revision 1.1.2.4.2.11  2014/08/18 13:55:00  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * optimistic locking changes
 *
 * Revision 1.1.2.4.2.10  2014/08/15 15:36:36  snimmakayala
 * Case: 20149972 & 20149973
 * JAWBONE WO PICKING FIXES
 *
 * Revision 1.1.2.4.2.9  2014/08/04 15:16:40  skreddy
 * case # 20149765/20149766
 * jawbone SB issue fix
 *
 * Revision 1.1.2.4.2.8  2014/06/13 08:54:52  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.4.2.7  2014/06/06 07:22:05  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.1.2.4.2.6  2014/05/30 00:34:24  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.4.2.5  2014/03/20 14:14:04  skreddy
 * case 20127784
 * New Demo account issue fix
 *
 * Revision 1.1.2.4.2.4  2014/01/06 13:16:03  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.1.2.4.2.3  2013/06/28 19:16:07  grao
 * PCT SB Issuse Fixes
 *
 * picking screen asking to scan the Bin Location for every two items even though  all are picking from same Bin Location
 *
 * Revision 1.1.2.4.2.2  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.4.2.1  2013/03/08 14:38:41  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.1.2.4  2013/02/18 06:26:05  skreddy
 * CASE201112/CR201113/LOG201121
 *  Qty and Location exception in Wo Picking
 *
 * Revision 1.1.2.3  2012/12/15 00:29:31  skreddy
 * CASE201112/CR201113/LOG201121
 * added skip functionality
 *
 * Revision 1.1.2.2  2012/11/28 15:07:45  schepuri
 * CASE201112/CR201113/LOG201121
 * itemdecsription null issue
 *
 * Revision 1.1.2.1  2012/11/23 09:10:42  skreddy
 * CASE201112/CR201113/LOG201121
 * RF version of WO Assembly building process
 *
 *  *
 * 
 *****************************************************************************/
function PickingConfirm(request, response){


	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);

	if (request.getMethod() == 'GET') {

		nlapiLogExecution('ERROR', 'Into Request', TimeStampinSec());

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('Error', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7,st8,st9,st10,st11;

		if( getLanguage == 'es_ES')
		{
			st1 = "ELIJA LA CANTIDAD:";
			st2 = "UBICACI&#211;N:";
			st3 = "ART&#205;CULO:";
			st4 = "DESCRIPCI&#211;N DEL ART&#205;CULO:";
			st5 = "CANTIDAD RESTANTE:";
			st6 = "CANT EXCEPCI&#211;N";
			st7 = "EXCEPCI&#211;N LOC";
			st8 = "CONF";
			st9 = "ANTERIOR";
			st10 = "SKIP";
			st11 = "CONTENEDOR LP:";
		}
		else
		{
			st1 = "PICK QTY : ";
			st2 = "LOCATION : ";
			st3 = "ITEM : ";
			st4 = "ITEM DESCRIPTION: ";
			st5 = "REMAINING QTY : ";
			st6 = "QTY EXCEPTION ";
			st7 = "LOC EXCEPTION ";
			st8 = "CONF ";
			st9 = "PREV";
			st10 = "SKIP";
			st11 = " LP#:";
		}
		var getWOid = request.getParameter('custparam_woid');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocationId = request.getParameter('custparam_beginLocation');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getItemName = request.getParameter('custparam_itemname');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');        
		var getEnteredLocation = request.getParameter('custparam_endlocation');
		var getBeginBinLocation = request.getParameter('custparam_beginlocationname');
		var getFetchedLocation = request.getParameter('custparam_beginLocationname');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var vclusterNo = request.getParameter('custparam_clusterno');		
		var vBatchno = request.getParameter('custparam_batchno');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var name=request.getParameter('name');
		var ContainerSize=request.getParameter('custparam_containersize');
		var getOrderNo=request.getParameter('custparam_ebizordno');
		var pickType=request.getParameter('custparam_picktype');
		var whLocation = request.getParameter('custparam_whlocation');
		var getZoneNo=request.getParameter('custparam_ebizzoneno');
		var itemType=request.getParameter('custparam_itemType');
		var ActBatchno = request.getParameter('custparam_Actbatchno');
		var ExpBatchno=request.getParameter('custparam_Expbatchno');
		var EntLocRec=request.getParameter('custparam_recid');
		var ExceptionFlag = request.getParameter('custparam_Exc');
		var itemstatus = request.getParameter('custparam_itemstatus');

		var getnextExpectedQuantity = request.getParameter('custparam_nextexpectedquantity');
		nlapiLogExecution('ERROR', 'getnextExpectedQuantity', getnextExpectedQuantity);

		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');
		if(getBeginBinLocation==null)
		{
			getBeginBinLocation=NextLocation;
		}
		if(getItemInternalId==null)
		{
			getItemInternalId=NextItemInternalId;
		}

		nlapiLogExecution('ERROR', 'getItemName', getItemName);
		nlapiLogExecution('ERROR', 'vSkipId', getItemName);

		var getItem = '';
		var Itemdescription='';

		//if(getItemName==null || getItemName =='')
		//{
//		var Itype = nlapiLookupField('item', getItemInternalId, 'recordType');
//		var ItemRec = nlapiLoadRecord(Itype, getItemInternalId);

//		getItem = ItemRec.getFieldValue('itemid');

//		nlapiLogExecution('ERROR', 'getItem', getItem);

//		nlapiLogExecution('ERROR', 'Location Name is', getItem);
//		if(ItemRec.getFieldValue('description') != null && ItemRec.getFieldValue('description') != "")
//		{	
//		Itemdescription = ItemRec.getFieldValue('description');
//		}
//		else if(ItemRec.getFieldValue('salesdescription') != null && ItemRec.getFieldValue('salesdescription') != "")
//		{	
//		Itemdescription = ItemRec.getFieldValue('salesdescription');
//		}

		var filter=new Array();
		if(getItemInternalId!=null&&getItemInternalId!="")
			filter[0]=new nlobjSearchFilter("internalid",null,"anyof",getItemInternalId);

		var column=new Array();
		column[0]=new nlobjSearchColumn("itemid");
		column[1]=new nlobjSearchColumn("description");

		var searchres=nlapiSearchRecord("item",null,filter,column);
		if(searchres!=null&&searchres!="")
		{
			getItem=searchres[0].getValue("itemid");
			Itemdescription=searchres[0].getValue("description");
		}

		//}
		//else
		//{
		//	getItem=getItemName;
		//}

		nlapiLogExecution('ERROR', 'getItemdescription', Itemdescription);

		var str = 'getWOid. = ' + getWOid + '<br>';
		str = str + 'getOrderNo. = ' + getOrderNo + '<br>';	
		str = str + 'getItemInternalId. = ' + getItemInternalId + '<br>';
		str = str + 'getBeginLocationId. = ' + getBeginLocationId + '<br>';
		str = str + 'getEndLocInternalId. = ' + getEndLocInternalId + '<br>';
		str = str + 'NextLocation. = ' + NextLocation + '<br>';
		str = str + 'NextItemInternalId. = ' + NextItemInternalId + '<br>';
		str = str + 'getItem. = ' + getItem + '<br>';
		str = str + 'Itemdescription. = ' + Itemdescription + '<br>';

		nlapiLogExecution('ERROR', 'calculating Remaining Qty...', str);

		var totalallocqty=0;
		var remainpickqty=0;

		var filterOpentask = new Array();

		filterOpentask.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
		filterOpentask.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));	
		if(getWOid != null && getWOid != "")
		{
			filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', getWOid));
		}

		if(getItemInternalId != null && getItemInternalId != "")
		{
			filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_sku_no', null, 'is', getItemInternalId));
		}

		if(getEndLocInternalId != null && getEndLocInternalId != "")
		{
			filterOpentask.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', getEndLocInternalId));
		}
		if(getZoneNo!=null && getZoneNo!="" && getZoneNo!="null")
		{
			filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', getZoneNo));
		}

		/*	var WOcolumns = new Array();
		WOcolumns[0] = new nlobjSearchColumn('custrecord_line_no');
		WOcolumns[1] = new nlobjSearchColumn('custrecord_sku');
		WOcolumns[2] = new nlobjSearchColumn('custrecord_expe_qty');
		WOcolumns[3] = new nlobjSearchColumn('custrecord_lpno');
		WOcolumns[4] = new nlobjSearchColumn('custrecord_batch_no');
		WOcolumns[5] = new nlobjSearchColumn('custrecord_actbeginloc');
		WOcolumns[6] = new nlobjSearchColumn('custrecord_act_qty');   
		WOcolumns[7] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		WOcolumns[8] = new nlobjSearchColumn('custrecord_invref_no');
		WOcolumns[9] = new nlobjSearchColumn('custrecord_wms_location');
		WOcolumns[10] = new nlobjSearchColumn('custrecord_comp_id');
		WOcolumns[11] = new nlobjSearchColumn('custrecord_ebiz_order_no');		*/

		var SOColumns = new Array();				
		SOColumns[0] = new nlobjSearchColumn('custrecord_sku',null,'group');
		SOColumns[1] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
		SOColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');

		SOColumns[0].setSort();	//SKU
		SOColumns[2].setSort();	//Location

		var WOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterOpentask,SOColumns);	

		if(WOSearchResults!=null && WOSearchResults!='' && WOSearchResults.length>0)
		{
			totalallocqty = WOSearchResults[0].getValue('custrecord_expe_qty',null,'sum');
		}

		nlapiLogExecution('ERROR', 'Out of Request before html', TimeStampinSec());


		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";      
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		//html = html + " document.getElementById('cmdConfirm').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +"<label>" + getExpectedQuantity + "</label>";
		html = html + "				<input type='hidden' name='hdnWOid' value=" + getWOid + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
		html = html + "				<input type='hidden' name='hdnItem' value='" + getItem + "'>";
		html = html + "				<input type='hidden' name='hdnItemName' value=" + getItemName + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEnteredLocation' value=" + getEnteredLocation + ">";
		html = html + "				<input type='hidden' name='hdnBeginBinLocation' value=" + getBeginBinLocation + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationName' value=" + getFetchedLocation + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vclusterNo + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value=" + vBatchno + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnNextLocation' value=" + NextLocation + ">";
		html = html + "				<input type='hidden' name='hdnNextItemId' value=" + NextItemInternalId + ">";		
		html = html + "				<input type='hidden' name='hdnName' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + ContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnebizOrdNo' value=" + getOrderNo + ">";
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnebizzoneno' value=" + getZoneNo + ">";
		html = html + "				<input type='hidden' name='hdnextExpectedQuantity' value=" + getnextExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnflag'>";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdnitemtype' value=" + itemType + ">";
		html = html + "				<input type='hidden' name='hdnactlot' value=" + ActBatchno + ">";
		html = html + "				<input type='hidden' name='hdnexplot' value=" + ExpBatchno + ">";
		html = html + "				<input type='hidden' name='hdnEntLocRec' value=" + EntLocRec + ">";
		html = html + "				<input type='hidden' name='hdnExceptionFlag' value=" + ExceptionFlag + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdnitemstatus' value=" + itemstatus + ">";	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st2 +"<label>" + getEnteredLocation + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 +"<label>" + getItem + "</label><br>";
		html = html + "				ITEM DESCRIPTION: <label>" + Itemdescription + "</label><br>";
		html = html + "				EXPECTED LOT#: <label>" +ActBatchno + "</label><br>";
		html = html + "				ACTUAL LOT#: <label>" + ExpBatchno + "</label><br>";
		//html = html + "				"+ st5 +"<label>" + parseFloat(remainpickqty).toFixed(4) + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		//html = html + "			<tr>";
//		html = html + "				<td align = 'left'>SUSPEND <input name='cmdSuspend' type='submit' value='F10'/>";
		html = html + "				<td align = 'left'>";
		html = html + "				"+ st6 +"<input name='cmdPickException' type='submit' value='F2'/></td></tr>";
		html = html + "				<tr><td align = 'left'>"+ st7 +"<input name='cmdlocexc' type='submit' value='F3'/></td></tr>";
		html = html + "				<tr><td align = 'left'>"+ st8 +"<input name='cmdConfirm' type='submit' id='cmdConfirm' value='ENT' onclick='this.form.hdnflag.value=this.value;this.form.submit();this.focus();this.disabled=true; return false'/>";
		html = html + "				"+ st9 +" <input name='cmdPrevious' type='submit' value='F7'/>"+ st10 +"<input name='cmdSKIP' type='submit' value='F12'/></td></tr>";
		/*html = html + "				<tr><td align = 'left'>CONF AND STAGE <input name='cmdSuspend' type='submit' value='F11'/>";*/
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('cmdConfirm').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);

		nlapiLogExecution('ERROR', 'Out of Request', TimeStampinSec());
	}
	else {
		nlapiLogExecution('ERROR', 'Into Response', TimeStampinSec());

		var getLanguage = request.getParameter('hdngetLanguage');

		var st11;

		if( getLanguage == 'es_ES')
		{
			st11 = "CONFIRME LA REPLENS ABIERTAS";
		}
		else
		{
			st11 = "CONFIRM THE OPEN REPLENS";
		}

		var vZoneId=request.getParameter('hdnebizzoneno');
		// This variable is to hold the SO# entered.
		var SOarray = new Array();
		SOarray["custparam_language"] = getLanguage;
		var nextexpqty = request.getParameter('hdnextExpectedQuantity');
		var invrefno = request.getParameter('hdnInvoiceRefNo');
		var ItemType=request.getParameter('hdnitemtype');
		var getwoid =request.getParameter('hdnWOid');
		var vlineno =request.getParameter('hdnOrderLineNo');
		var vSkipId=request.getParameter('hdnskipid');
		var ExpectedQuantity = request.getParameter('hdnExpectedQuantity');

		SOarray["custparam_woid"] = request.getParameter('hdnWOid');
		SOarray["custparam_recordinternalid"] = request.getParameter('hdnRecordInternalId');
		SOarray["custparam_containerlpno"] = request.getParameter('hdnContainerLpNo');
		SOarray["custparam_expectedquantity"] = request.getParameter('hdnExpectedQuantity');
		SOarray["custparam_beginLocation"] = request.getParameter('hdnBeginLocationId');
		SOarray["custparam_item"] = request.getParameter('hdnItem');
		SOarray["custparam_itemname"] = request.getParameter('hdnItemName');
		SOarray["custparam_itemdescription"] = request.getParameter('hdnItemDescription');
		SOarray["custparam_iteminternalid"] = request.getParameter('hdnItemInternalId');
		SOarray["custparam_dolineid"] = request.getParameter('hdnDOLineId');
		SOarray["custparam_invoicerefno"] = request.getParameter('hdnInvoiceRefNo');
		SOarray["custparam_beginlocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_beginLocationname"] = request.getParameter('hdnBeginLocationName');
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
		SOarray["custparam_orderlineno"] = request.getParameter('hdnOrderLineNo');
		SOarray["custparam_clusterno"] = request.getParameter('hdnClusterNo');
		SOarray["custparam_batchno"] = request.getParameter('hdnbatchno');
		SOarray["custparam_noofrecords"] = request.getParameter('hdnRecCount');;
		SOarray["custparam_nextlocation"] = request.getParameter('hdnNextLocation');
		SOarray["custparam_nextiteminternalid"] = request.getParameter('hdnNextItemId');		
		SOarray["name"] = request.getParameter('hdnName');
		SOarray["custparam_containersize"] = request.getParameter('hdnContainerSize');
		SOarray["custparam_ebizordno"] = request.getParameter('hdnebizOrdNo');;
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		SOarray["custparam_nextexpectedquantity"] = request.getParameter('hdnextExpectedQuantity');
		SOarray["custparam_nextrecordid"]=request.getParameter('hdnRecordInternalId');
		SOarray["custparam_itemType"] = ItemType;
		SOarray["custparam_Actbatchno"]=request.getParameter('hdnactlot');
		SOarray["custparam_Expbatchno"] = request.getParameter('hdnexplot');
		SOarray["custparam_itemstatus"] = request.getParameter('hdnitemstatus');
		

		var EntLocRec = request.getParameter('hdnEntLocRec');
		var ExceptionFlag = request.getParameter('hdnExceptionFlag');
		SOarray["custparam_EntLocRec"] = EntLocRec;
		SOarray["custparam_Exc"]=ExceptionFlag;
		SOarray["custparam_screenno"] = 'WOPicking';
		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  request.getParameter('hdnebizzoneno');
		}
		else
			SOarray["custparam_ebizzoneno"] = '';

		SOarray["custparam_whlocation"] = request.getParameter('hdnwhlocation');

		if (sessionobj!=context.getUser()) {
			try
			{

				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}

				//	if the previous button 'F7' is clicked, it has to go to the previous screen 
				if (request.getParameter('cmdPrevious') == 'F7') {
					nlapiLogExecution('ERROR', 'Clicked on Previous', request.getParameter('cmdPrevious'));
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_item', 'customdeploy_ebiz_rf_wo_picking_item_di', false, SOarray);
				}
				else if(request.getParameter('cmdSKIP') == 'F12')
				{
					var RecordInternalId = request.getParameter('hdnRecordInternalId');
					var ContainerLPNo = request.getParameter('hdnContainerLpNo');
					var ExpectedQuantity = request.getParameter('hdnExpectedQuantity');
					var BeginLocation = request.getParameter('hdnBeginLocationId');
					var Item = request.getParameter('hdnItem');
					var ItemName = request.getParameter('hdnItemName');
					var ItemDescription = request.getParameter('hdnItemDescription');
					var ItemInternalId = request.getParameter('hdnItemInternalId');
					var DoLineId = request.getParameter('hdnDOLineId'); // internal id of DO
					var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
					var BeginLocationName = request.getParameter('hdnBeginBinLocation');
					var vPickType=request.getParameter('hdnpicktype');
					nlapiLogExecution('ERROR', 'vPickType',vPickType);
					nlapiLogExecution('ERROR', 'BeginLocation',BeginLocation);
					nlapiLogExecution('ERROR', 'BeginLocationName',BeginLocationName);
					
					var RecCount;
					var filterOpentask = new Array();
					filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', getwoid));
					filterOpentask.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
					filterOpentask.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));	
					if(vZoneId!=null && vZoneId!="" && vZoneId!="null")
					{
						filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', vZoneId));
					}

					var WOcolumns = new Array();
					WOcolumns.push(new nlobjSearchColumn('custrecord_line_no'));
					WOcolumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
					WOcolumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
					WOcolumns.push(new nlobjSearchColumn('custrecord_sku'));
					WOcolumns.push(new nlobjSearchColumn('custrecord_expe_qty'));				
					WOcolumns.push(new nlobjSearchColumn('custrecord_lpno'));
					WOcolumns.push(new nlobjSearchColumn('custrecord_batch_no'));
					WOcolumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
					WOcolumns.push(new nlobjSearchColumn('custrecord_act_qty'));   
					WOcolumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
					WOcolumns.push(new nlobjSearchColumn('custrecord_invref_no'));
					WOcolumns.push(new nlobjSearchColumn('custrecord_wms_location'));
					WOcolumns.push(new nlobjSearchColumn('custrecord_comp_id'));
					WOcolumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));				
					WOcolumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));	
					WOcolumns.push(new nlobjSearchColumn('custrecord_sku_status'));	

					WOcolumns[1].setSort();
					WOcolumns[2].setSort();
					WOcolumns[3].setSort();
					WOcolumns[4].setSort(true);

					var WOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterOpentask,WOcolumns);	
					if (WOSearchResults != null && WOSearchResults.length > 0) {
						nlapiLogExecution('ERROR', 'WOSearchResults.length', WOSearchResults.length);

						if(WOSearchResults.length <= parseInt(vSkipId))
						{
							vSkipId=0;
						}
						var SearchResult = WOSearchResults[parseInt(vSkipId)];
						if(WOSearchResults.length > parseInt(vSkipId) + 1)
						{
							var WOSearchnextResult = WOSearchResults[parseInt(vSkipId) + 1];
							SOarray["custparam_nextlocation"] = WOSearchnextResult.getText('custrecord_actbeginloc');
							SOarray["custparam_nextiteminternalid"] = WOSearchnextResult.getValue('custrecord_ebiz_sku_no');
							SOarray["custparam_nextitem"] = WOSearchnextResult.getText('custrecord_sku');
							SOarray["custparam_nextserialno"] = WOSearchnextResult.getValue('custrecord_batch_no');
							nlapiLogExecution('ERROR', 'Next Location', SOarray["custparam_nextlocation"]);
							nlapiLogExecution('ERROR', 'Next SerialNo', SOarray["custparam_nextserialno"]);
						}
						else
						{
							var WOSearchnextResult = WOSearchResults[0];
							SOarray["custparam_nextlocation"] = WOSearchnextResult.getText('custrecord_actbeginloc');
							SOarray["custparam_nextiteminternalid"] = WOSearchnextResult.getValue('custrecord_ebiz_sku_no');
							SOarray["custparam_nextitem"] = WOSearchnextResult.getText('custrecord_sku');
							SOarray["custparam_nextserialno"] = WOSearchnextResult.getValue('custrecord_batch_no');
						}
						SOarray["custparam_woid"] = getwoid; 
						SOarray["custparam_iteminternalid"] = SearchResult.getValue('custrecord_ebiz_sku_no');
						SOarray["custparam_item"] = SearchResult.getText('custrecord_sku');
						SOarray["custparam_recordinternalid"] = SearchResult.getId();
						SOarray["custparam_expectedquantity"] = SearchResult.getValue('custrecord_expe_qty');
						SOarray["custparam_beginLocation"] = SearchResult.getText('custrecord_actbeginloc');//text is passing from previous screen to this parameteer,that is why getvalue is removed and kept gettext
						SOarray["custparam_itemdescription"] = SearchResult.getValue('custrecord_skudesc');
						SOarray["custparam_dolineid"] = SearchResult.getValue('custrecord_ebiz_cntrl_no');
						SOarray["custparam_invoicerefno"] = SearchResult.getValue('custrecord_invref_no');
						SOarray["custparam_orderlineno"] = SearchResult.getValue('custrecord_line_no');
						SOarray["custparam_beginlocationname"] = SearchResult.getText('custrecord_actbeginloc');
						SOarray["custparam_batchno"] = SearchResult.getValue('custrecord_batch_no');
						SOarray["custparam_containerlpno"] = SearchResult.getValue('custrecord_container_lp_no');
						SOarray["custparam_whlocation"] = SearchResult.getValue('custrecord_wms_location');
						SOarray["custparam_whcompany"] = SearchResult.getValue('custrecord_comp_id');
						SOarray["custparam_noofrecords"] = SearchResult.length;
						SOarray["custparam_ebizordno"] =  SearchResult.getValue('custrecord_ebiz_order_no');
						SOarray["custparam_itemstatus"] =  SearchResult.getValue('custrecord_sku_status');
					}
					vSkipId= parseFloat(vSkipId) + 1;
					SOarray["custparam_skipid"] = vSkipId;	
					nlapiLogExecution('ERROR', 'SOarray["custparam_nextiteminternalid"]', SOarray["custparam_nextiteminternalid"]);
					nlapiLogExecution('ERROR', 'SOarray["custparam_nextlocation"]', SOarray["custparam_nextlocation"]);
					if(BeginLocation != SOarray["custparam_nextlocation"])
					{  
						SOarray["custparam_beginLocationname"]=null;
						SOarray["custparam_iteminternalid"]=null;
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_location', 'customdeploy_ebiz_rf_wo_picking_loc_di', false, SOarray);								
						nlapiLogExecution('ERROR', 'Navigating To1', 'Picking Location');
					}
					else if(ItemInternalId != SOarray["custparam_nextiteminternalid"])
					{ 
						SOarray["custparam_iteminternalid"] = SOarray["custparam_nextiteminternalid"];
						SOarray["custparam_beginLocationname"]=null;
						SOarray["custparam_nextexpectedquantity"] = SOarray["custparam_expectedquantity"];
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_item', 'customdeploy_ebiz_rf_wo_picking_item_di', false, SOarray);								
						nlapiLogExecution('ERROR', 'Navigating To1', 'Picking Item');
					}
					else if(ItemInternalId == SOarray["custparam_nextiteminternalid"])
					{
						SOarray["custparam_beginLocationname"]=null;
						SOarray["custparam_iteminternalid"]=ItemInternalId;
						SOarray["custparam_nextexpectedquantity"] = SOarray["custparam_expectedquantity"];
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_confirm', 'customdeploy_ebiz_rf_wo_pick_confirm_di', false, SOarray);								
						nlapiLogExecution('ERROR', 'Navigating To1', 'Picking Item');
					}
				}
				else 
				{
					if (request.getParameter('hdnflag') == 'ENT') {
						nlapiLogExecution('ERROR', 'Confirm Pick');

						var RecordInternalId = request.getParameter('hdnRecordInternalId');
						var ContainerLPNo = request.getParameter('hdnContainerLpNo');
						var ExpectedQuantity = request.getParameter('hdnExpectedQuantity');
						var BeginLocation = request.getParameter('hdnBeginLocationId');
						var Item = request.getParameter('hdnItem');
						var ItemName = request.getParameter('hdnItemName');
						var ItemDescription = request.getParameter('hdnItemDescription');
						var ItemInternalId = request.getParameter('hdnItemInternalId');
						var DoLineId = request.getParameter('hdnDOLineId'); // internal id of DO
						var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
						var BeginLocationName = request.getParameter('hdnBeginBinLocation');
						var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
						var EndLocation = request.getParameter('hdnEnteredLocation');
						var OrderLineNo = request.getParameter('hdnOrderLineNo');
						var NextShowLocation=request.getParameter('hdnNextLocation');
						var ContainerSize=request.getParameter('hdnContainerSize');
						var NextItemInternalId=request.getParameter('hdnNextItemId');
						var ClusNo = request.getParameter('hdnClusterNo');
						var ebizOrdNo=request.getParameter('hdnebizOrdNo');
						var OrdName=request.getParameter('hdnName');
						var getZoneNo=request.getParameter('hdnebizzoneno');
						var NextShowItem=request.getParameter('hdnNextItemId');
						var ItemNo = request.getParameter('hdnItemInternalId');
						var vbatch= request.getParameter('hdnexplot');
						var vZoneId=request.getParameter('hdnebizzoneno');

						var  vdono = DoLineId;

						//Get Open task Count
						opentaskcount=getOpenTasksCount(getwoid,vZoneId);
						nlapiLogExecution('ERROR', 'opentaskcount', opentaskcount);
if(parseFloat(opentaskcount)>0)
	{
	if(RecordInternalId != null && RecordInternalId != '')
	{	
	var vOpenStatus = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordInternalId);
	if(vOpenStatus != null && vOpenStatus != '')
	{
		var vOpenWMSStatus = vOpenStatus.getFieldValue('custrecord_wms_status_flag');
		if(vOpenWMSStatus != '9')
		{
			nlapiLogExecution('ERROR', 'Record already picked', RecordInternalId);
			SOarray["custparam_error"]='Already picked';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			return;	
		}	
	}	
	
	}
						//Update Status in Open task
						UpdateOpenTask(getwoid,vlineno,ExpectedQuantity,RecordInternalId,InvoiceRefNo,ItemInternalId);

						var NextBatch="";
						var filterOpentask = new Array();
						filterOpentask.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
						filterOpentask.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));

						if(getwoid != null && getwoid != "")
						{
							filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', getwoid));
						}

						if(vZoneId!=null && vZoneId!="" && vZoneId!="null")
						{
							filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', vZoneId));
						}

						var SOColumns=new Array();
						SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
						SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
						SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
						SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_skiptask')); 
						SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
						SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
						SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
						SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
						SOColumns.push(new nlobjSearchColumn('name'));
						SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
						SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));

						SOColumns[1].setSort();
						SOColumns[2].setSort();
						SOColumns[3].setSort();
						SOColumns[4].setSort(true);


						var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterOpentask, SOColumns);
						nlapiLogExecution('ERROR', 'Results', SOSearchResults);
						if (SOSearchResults != null && SOSearchResults.length > 0) 
						{
							nlapiLogExecution('ERROR', 'SOSearchResults length', SOSearchResults.length);

							var SOSearchResult = SOSearchResults[0];							
							SOarray["custparam_woid"] = getwoid;
							SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
							SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
							SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
							SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
							SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
							SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
							SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
							SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
							SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
							SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
							SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
							SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
							SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
							SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
							SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
							SOarray["custparam_noofrecords"] = SOSearchResults.length;		
							SOarray["name"] =  SOSearchResult.getValue('name');
							SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
							SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
							NextBatch=SOSearchResult.getValue('custrecord_batch_no');

							if(SOSearchResults.length > 1)
							{
								var SOSearchnextResult = SOSearchResults[1];
								SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
								SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
								SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
								SOarray["custparam_nextbatchno"] = SOSearchnextResult.getValue('custrecord_batch_no');
//								nlapiLogExecution('ERROR', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
//								nlapiLogExecution('ERROR', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));	
//								nlapiLogExecution('ERROR', 'Next Qty', SOSearchnextResult.getValue('custrecord_expe_qty'));	
							}
							else
							{
								var SOSearchnextResult = SOSearchResults[0];
								SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
								SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
								SOarray["custparam_nextbatchno"] = SOSearchnextResult.getValue('custrecord_batch_no');
//								nlapiLogExecution('ERROR', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
//								nlapiLogExecution('ERROR', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));	
							}

						}

						if(opentaskcount >  1)
						{
							nlapiLogExecution('ERROR', 'BeginLocation',BeginLocation);
							nlapiLogExecution('ERROR', 'NextShowLocation',NextShowLocation);
							nlapiLogExecution('ERROR', 'ItemNo',ItemNo);
							nlapiLogExecution('ERROR', 'NextShowItem',NextShowItem);
							nlapiLogExecution('ERROR', 'vbatch', vbatch);
							nlapiLogExecution('ERROR', 'NextBatch', NextBatch);

							if(BeginLocation != NextShowLocation)
							{ 
								SOarray["custparam_beginLocationname"]=NextShowLocation;
								SOarray["custparam_iteminternalid"]=NextShowItem;
								SOarray["custparam_expectedquantity"]=null;
								nlapiLogExecution('ERROR', 'Navigating To1', 'Picking Location');
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_location', 'customdeploy_ebiz_rf_wo_picking_loc_di', false, SOarray);								

							}
							else if(ItemNo != NextShowItem)
							{ 
								nlapiLogExecution('ERROR', 'Navigating To1', 'Picking Item');
								SOarray["custparam_iteminternalid"] = NextShowItem;
								SOarray["custparam_beginLocationname"]=BeginLocation;
								SOarray["custparam_expectedquantity"]=nextexpqty;
								nlapiLogExecution('ERROR', 'SOarray["custparam_beginLocationname"]', SOarray["custparam_beginLocationname"]);
								SOarray["custparam_expectedquantity"]=null;
								//SOarray["custparam_iteminternalid"]=null;
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_item', 'customdeploy_ebiz_rf_wo_picking_item_di', false, SOarray);								

							}
							else if(ItemNo == NextShowItem)
							{ 
								if(NextBatch == vbatch)
								{
									SOarray["custparam_iteminternalid"] = NextShowItem;
									SOarray["custparam_beginLocationname"]=null;
									SOarray["custparam_expectedquantity"]=nextexpqty;
									nlapiLogExecution('ERROR', 'Navigating To1', 'Same Item and  same Batch');
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_confirm', 'customdeploy_ebiz_rf_wo_pick_confirm_di', false, SOarray);
								}
								else
								{
									nlapiLogExecution('ERROR', 'Navigating To1', 'Same Item ,Diff Batch');
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_batch', 'customdeploy_ebiz_rf_wo_picking_batch_di', false, SOarray);
								}

							}

						}
						else
						{
							response.sendRedirect('SUITELET', 'customscript_rf_wo_picking_stage_loc', 'customdeploy_rf_wo_picking_stage_loc_di', false, SOarray);
						}
					}
					else
					{
	nlapiLogExecution('ERROR', 'Record already picked', RecordInternalId);
	SOarray["custparam_error"]='Already picked';
	response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
	return;
	}
					}
					else
					{
						nlapiLogExecution('ERROR', 'cmdPickException', request.getParameter('cmdPickException'));
						if (request.getParameter('cmdPickException') == 'F2') {

							nlapiLogExecution('ERROR', 'Clicked on cmdPickException', request.getParameter('cmdPickException'));
							nlapiLogExecution('ERROR', 'getItem in F2 after', SOarray["custparam_item"]);
							response.sendRedirect('SUITELET', 'customscript_ebi_rf_wo_picking_qty_excep', 'customdeploy_ebiz_rf_wo_pick_qty_exp_di', false, SOarray);
							return;
						}
						if (request.getParameter('cmdlocexc') == 'F3') {
							nlapiLogExecution('ERROR', 'Clicked on cmdLocException', request.getParameter('cmdlocexc'));
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_loc_excp', 'customdeploy_ebiz_rf_wo_picking_locexp_d', false, SOarray);
						}
					}

					nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
					nlapiLogExecution('ERROR', 'Time Stamp at the end of Response',TimeStampinSec());
				}
			}
			catch (e)  {
				nlapiLogExecution('ERROR', 'Done customrecord', 'catch');
				SOarray["custparam_error"]=e;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');

			}
		}
		else
		{

			//WOPicking
			SOarray["custparam_screenno"] = 'WOPicking';
			SOarray["custparam_error"] = 'ORDER ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);

		}
	}
}



function getOpenReplns(item,location)
{
	nlapiLogExecution('ERROR', 'Into getOpenReplns');
	var PickFaceSearchResults= new Array();
	var PickFaceFilters = new Array();
	var PickFaceColumns = new Array();

	PickFaceFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8])); // Task Type - RPLN	 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', item)); 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'is', location));	
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));

	PickFaceColumns[0] = new nlobjSearchColumn('custrecord_expe_qty');
	PickFaceColumns[1] = new nlobjSearchColumn('custrecord_taskpriority');

	PickFaceSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, PickFaceFilters, PickFaceColumns);



	return PickFaceSearchResults;

}

function getQOHForAllSKUsinPFLocation(location,item){

	nlapiLogExecution('ERROR','location',location);
	nlapiLogExecution('ERROR','item',item);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', location));	
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', item));
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19','3']));

	var columns = new Array();	
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty',null,'sum');

	var pfSKUInvList = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

	return pfSKUInvList;
}

function buildTaskArray(WaveNo,ClusNo,OrdName,ebizOrdNo,vZoneId,vPickType)
{
	var SOarray = new Array();
	var SOFilters = new Array();
	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

	if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && WaveNo != null && WaveNo != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
	}

	if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
	{
		nlapiLogExecution('ERROR', 'ClusNo inside If', ClusNo);

		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
	}
	if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
	{
		nlapiLogExecution('ERROR', 'OrdNo inside If', OrdName);
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
	}
	if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && ebizOrdNo!=null && ebizOrdNo!="" && ebizOrdNo!= "null")
	{
		nlapiLogExecution('ERROR', 'SO Id inside If', ebizOrdNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ebizOrdNo));
	}
	if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
	{
		nlapiLogExecution('ERROR', 'Zone # inside If', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
	}
	SOarray["custparam_ebizordno"] = vSOId;
	var SOColumns = new Array();
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
	SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
	SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
	SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
	SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
	SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
	SOColumns.push(new nlobjSearchColumn('name'));
	SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
	SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));

	SOColumns[0].setSort();
	SOColumns[1].setSort();
	SOColumns[2].setSort();
	SOColumns[3].setSort();
	SOColumns[4].setSort();
	SOColumns[5].setSort(true);

	SOarray["custparam_ebizordno"] = vSOId;
	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

	if (SOSearchResults != null && SOSearchResults.length > 0) {

		var SOSearchResult = SOSearchResults[0];
		if(SOSearchResults.length > 1)
		{
			var SOSearchnextResult = SOSearchResults[1];
			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			nlapiLogExecution('ERROR', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
			nlapiLogExecution('ERROR', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
		}
		else
		{
			var SOSearchnextResult = SOSearchResults[0];
			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
			nlapiLogExecution('ERROR', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
			nlapiLogExecution('ERROR', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
		}

		SOarray["custparam_waveno"] = WaveNo;
		SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
		SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
		SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
		SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
		SOarray["custparam_endlocinternalid"] = SOSearchResult.getValue('custrecord_actbeginloc');		
		SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
		SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
		SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
		SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
		SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
		SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
		SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
		SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
		SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
		SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
		SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
		SOarray["custparam_noofrecords"] = SOSearchResults.length;		
		SOarray["name"] =  SOSearchResult.getValue('name');
		SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
		SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
		SOarray["custparam_picktype"] = vPickType;
		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
		}
		else
			SOarray["custparam_ebizzoneno"] = '';
	}

	return SOarray;
}

function UpdateOpenTask(wointernalid,x,expectedQty,RecordInternalId,InvoiceRefNo,ItemInternalId)
{
	nlapiLogExecution('ERROR', 'Into UpdateOpenTask',TimeStampinSec());

	var str = 'wointernalid.' + wointernalid + '<br>';
	str = str + 'expectedQty.' + expectedQty + '<br>';
	str = str + 'RecordInternalId.' + RecordInternalId + '<br>';
	str = str + 'InvoiceRefNo.' + InvoiceRefNo + '<br>';
	str = str + 'ItemInternalId.' + ItemInternalId + '<br>';
	str = str + 'x.' + x + '<br>';

	nlapiLogExecution('ERROR', 'Function Parameters', str);

	// Create a Record in Opentask
	var now = new Date();
	//a Date object to be used for a random value
	var now = new Date();
	//now= now.getHours();
	//Getting time in hh:mm tt format.
	var a_p = "";
	var d = new Date();
	var curr_hour = now.getHours();
	if (curr_hour < 12) {
		a_p = "am";
	}
	else {
		a_p = "pm";
	}
	if (curr_hour == 0) {
		curr_hour = 12;
	}
	if (curr_hour > 12) {
		curr_hour = curr_hour - 12;
	}

	var curr_min = now.getMinutes();

	curr_min = curr_min + "";

	if (curr_min.length == 1) {
		curr_min = "0" + curr_min;
	}

	var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordInternalId);
	vitem = transaction.getFieldValue('custrecord_ebiz_sku_no');
	vline =  transaction.getFieldValue('custrecord_line_no');
	vqty =  transaction.getFieldValue('custrecord_expe_qty');
	vlocationid =  transaction.getFieldValue('custrecord_actbeginloc');
	vBatchno =  transaction.getFieldValue('custrecord_batch_no');
	vContlpNo =  transaction.getFieldValue('custrecord_container_lp_no');
	vcontainersize =  transaction.getFieldValue('custrecord_container');

	var str = 'vitem.' + vitem + '<br>';
	str = str + 'vline.' + vline + '<br>';
	str = str + 'vqty.' + vqty + '<br>';
	str = str + 'vlocationid.' + vlocationid + '<br>';
	str = str + 'vBatchno.' + vBatchno + '<br>';
	str = str + 'vContlpNo' + vContlpNo + '<br>';
	str = str + 'vcontainersize' + vcontainersize + '<br>';

	nlapiLogExecution('ERROR', 'Open Task Parameters', str);

	var arrDims = getSKUCubeAndWeightforconfirm(vitem, 1);
	var itemCube = 0;
	var itemWeight=0;			

	if (arrDims[0] != ""  && arrDims[0] != null && (!isNaN(arrDims[1]))) 
	{
		itemCube = (parseFloat(vqty) * parseFloat(arrDims[0]));
		itemWeight = (parseFloat(vqty) * parseFloat(arrDims[1]));			
	} 

	var arrContainerDetails=getContainerCubeAndTarWeight(vcontainersize,"");
	var ContainerCube=0;
	var TotalWeight=0;

	if (arrContainerDetails[0] != "" && arrContainerDetails[1] !=null && arrContainerDetails[1]!='' && (!isNaN(arrContainerDetails[1]))) 
	{
		ContainerCube =  parseFloat(arrContainerDetails[0]);
		TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));
	} 	

	transaction.setFieldValue('custrecord_wms_status_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')
	transaction.setFieldValue('custrecord_act_qty', vqty);
	transaction.setFieldValue('custrecord_actendloc', vlocationid);
	transaction.setFieldValue('custrecord_container', vcontainersize);
	transaction.setFieldValue('custrecord_total_weight', itemWeight);
	transaction.setFieldValue('custrecord_totalcube', itemCube);
	transaction.setFieldValue('custrecord_batch_no', vBatchno);
	transaction.setFieldValue('custrecord_container_lp_no', vContlpNo);
	transaction.setFieldValue('custrecord_act_end_date', DateStamp());
	transaction.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));

	var currentContext = nlapiGetContext();
	var currentUserID = currentContext.getUser();
	transaction.setFieldValue('custrecord_upd_ebiz_user_no',currentUserID);
	var vemployee = request.getParameter('custpage_employee');

	if (vemployee != null && vemployee != "") 
	{
		transaction.setFieldValue('custrecord_taskassignedto',vemployee);
	} 
	else 
	{
		transaction.setFieldValue('custrecord_taskassignedto',currentUserID);
	}
	nlapiSubmitRecord(transaction, false, true);

	//Added to transaction order line details
	var customtranrecord = nlapiCreateRecord('customrecord_ebiznet_order_line_details');					
	customtranrecord.setFieldValue('custrecord_orderlinedetails_pickgen_qty', parseInt(vqty)); 
	customtranrecord.setFieldValue('custrecord_orderlinedetails_pickconf_qty', parseInt(vqty)); 
	customtranrecord.setFieldValue('custrecord_orderlinedetails_ship_qty', parseInt(vqty)); 
	customtranrecord.setFieldValue('custrecord_orderlinedetails_status_flag', 14);  // 14 stands for outbound process
	customtranrecord.setFieldValue('custrecord_orderlinedetails_item', vitem);
	customtranrecord.setFieldValue('custrecord_orderlinedetails_ord_category',4); // 3 stands for workorder				
	customtranrecord.setFieldValue('custrecord_orderlinedetails_order_no','WO'+vitem+x); 
	customtranrecord.setFieldValue('name','WO'+vitem+x); 
	nlapiSubmitRecord(customtranrecord);

	updateInventory(InvoiceRefNo,expectedQty,wointernalid);

	nlapiLogExecution('ERROR', 'Out of UpdateOpenTask',TimeStampinSec());

}

function updateInventory(vinvrefno,expectedQty,wointernalid)
{
	nlapiLogExecution('ERROR', 'Into updateInventory',TimeStampinSec());

	var str = 'vinvrefno.' + vinvrefno + '<br>';
	str = str + 'expectedQty.' + expectedQty + '<br>';
	str = str + 'wointernalid.' + wointernalid + '<br>';

	nlapiLogExecution('ERROR', 'Function Parameters', str);

	var currentContext = nlapiGetContext();  
	var currentUserID = currentContext.getUser();
	var scount=1;
	LABL1: for(var i=0;i<scount;i++)
	{
		nlapiLogExecution('ERROR', 'CUSTOM_RECORD_COLLISION', i);
		try
		{	

	var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv',vinvrefno);
	var QOHqty = transaction.getFieldValue('custrecord_ebiz_qoh');
	var allocationQty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
	var vLP=transaction.getFieldValue('custrecord_ebiz_inv_lp');
	var vPackcode=transaction.getFieldValue('custrecord_ebiz_inv_packcode');
	var vSKUStatus=transaction.getFieldValue('custrecord_ebiz_inv_sku_status');
	var getlotnoid=transaction.getFieldValue('custrecord_ebiz_inv_lot');
	var desc=transaction.getFieldValue('custrecord_ebiz_itemdesc');
	var expdate=transaction.getFieldValue('custrecord_ebiz_expdate');
	var vfifodate=transaction.getFieldValue('custrecord_ebiz_inv_fifo');

	if(parseFloat(allocationQty)>parseFloat(expectedQty)){
		allocationQty = parseFloat(allocationQty) - parseFloat(expectedQty);
	} 
	else {
		allocationQty = 0;
	}

	if(parseFloat(QOHqty)>parseFloat(expectedQty)){
		QOHqty = parseFloat(QOHqty) - parseFloat(expectedQty);
	} 
	else {
		QOHqty = 0;
	}

	if(parseFloat(allocationQty) <= 0){
		allocationQty=0;
	}

	var str = 'QOHqty.' + QOHqty + '<br>';
	str = str + 'allocationQty.' + allocationQty + '<br>';
	str = str + 'expectedQty.' + expectedQty + '<br>';

	nlapiLogExecution('ERROR', 'Qty Parameters', str);

	transaction.setFieldValue('custrecord_ebiz_alloc_qty',parseFloat(allocationQty).toFixed(5));
	transaction.setFieldValue('custrecord_ebiz_qoh',parseFloat(QOHqty).toFixed(5));
	transaction.setFieldValue('custrecord_updated_user_no',currentUserID);
	var invtrecid=nlapiSubmitRecord(transaction, false, true);	

			nlapiLogExecution('ERROR', 'Out of updateInventory',TimeStampinSec());
		}
		catch(ex)
		{
			var exCode='CUSTOM_RECORD_COLLISION'; 
			var wmsE='Inventory record being updated by another user. Please try again...';
			if (ex instanceof nlobjError) 
			{	
				wmsE=ex.getCode() + '\n' + ex.getDetails();
				exCode=ex.getCode();
			}
			else
			{
				wmsE=ex.toString();
				exCode=ex.toString();
			}  

			nlapiLogExecution('ERROR', 'Exception in RF Inv move : ', ex);

			if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
			{ 
				scount=scount+1;
				continue LABL1;
			}
			else break LABL1;
		}
	}

	if(invtrecid!=null && invtrecid!='' && parseFloat(expectedQty)>0)
		CreateSTGInvtRecord(invtrecid,vLP,expectedQty,wointernalid,getlotnoid,expdate,vfifodate,desc);
	nlapiLogExecution('Debug', 'QOHqty', QOHqty);
	if((parseInt(QOHqty)) <= 0)
	{		
		nlapiLogExecution('Debug', 'Deleting record from inventory if QOH becomes zero', invtrecid);
		var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invtrecid);				
	}
}


function getOpenTasksCount(vWOId,vZoneId)
{
	nlapiLogExecution('ERROR', 'Into getOpenTasksCount');

	nlapiLogExecution('ERROR', 'vWOId',vWOId);
	nlapiLogExecution('ERROR', 'vZoneId',vZoneId);

	var openreccount=0;
	var filterOpentask = new Array();
	filterOpentask.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
	filterOpentask.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));

	if(vWOId != null && vWOId != "")
	{
		filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vWOId));
	}

	if(vZoneId!=null && vZoneId!="" && vZoneId!="null")
	{
		filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', vZoneId));
	}

	var WOcolumns = new Array();
	WOcolumns[0] = new nlobjSearchColumn('custrecord_line_no');
	WOcolumns[1] = new nlobjSearchColumn('custrecord_sku');
	WOcolumns[2] = new nlobjSearchColumn('custrecord_expe_qty');
	WOcolumns[3] = new nlobjSearchColumn('custrecord_lpno');
	WOcolumns[4] = new nlobjSearchColumn('custrecord_batch_no');
	WOcolumns[5] = new nlobjSearchColumn('custrecord_actbeginloc');
	WOcolumns[6] = new nlobjSearchColumn('custrecord_act_qty');   
	WOcolumns[7] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	WOcolumns[8] = new nlobjSearchColumn('custrecord_invref_no');
	WOcolumns[9] = new nlobjSearchColumn('custrecord_wms_location');
	WOcolumns[10] = new nlobjSearchColumn('custrecord_comp_id');
	WOcolumns[11] = new nlobjSearchColumn('custrecord_ebiz_order_no');				

	WOcolumns[0].setSort();
	var WOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterOpentask,WOcolumns);	

	if(WOSearchResults!=null && WOSearchResults!='' && WOSearchResults.length>0)
	{
		openreccount=WOSearchResults.length;
	}



	return openreccount;

}


//Create STG Records in Create inventory

function CreateSTGInvtRecord(invtrecid, vLp,vqty,WorkOrderInternalId,getlotnoid,expdate,vfifodate,desc) 
{
	nlapiLogExecution('Debug', 'Into CreateSTGInvtRecord',TimeStampinSec());
	var str = 'Inv Ref No. = ' + invtrecid + '<br>';
	str = str + 'vLp. = ' + vLp + '<br>';	
	str = str + 'Qty. = ' + vqty + '<br>';	
	str = str + 'workOrderInternalId. = ' + WorkOrderInternalId + '<br>';	
	str = str + 'getlotnoid = ' + getlotnoid + '<br>';	
	str = str + 'expdate. = ' + expdate + '<br>';
	str = str + 'desc. = ' + desc + '<br>';
	nlapiLogExecution('Debug', 'STG INVT Details', str);

	try
	{
		var stgmInvtRec = nlapiCopyRecord('customrecord_ebiznet_createinv',invtrecid);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_lp', vLp);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(vqty).toFixed(4));
		stgmInvtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(vqty).toFixed(4));
		stgmInvtRec.setFieldValue('custrecord_ebiz_alloc_qty', '0');
		stgmInvtRec.setFieldValue('custrecord_wms_inv_status_flag', '36');	//FLAG.INVENTORY.WIP		
		stgmInvtRec.setFieldValue('custrecord_invttasktype', '3'); //Task Type - PICK
		stgmInvtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
		stgmInvtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');	
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_note1', '');	
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_note2', '');	

		if(WorkOrderInternalId!=null && WorkOrderInternalId!='')
			stgmInvtRec.setFieldValue('custrecord_ebiz_transaction_no', WorkOrderInternalId);	
		if(getlotnoid!=null&&getlotnoid!="")
		{
			stgmInvtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
			stgmInvtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
			stgmInvtRec.setFieldValue('custrecord_ebiz_inv_fifo', vfifodate);
		}
		stgmInvtRec.setFieldValue('custrecord_ebiz_itemdesc',desc);
		nlapiSubmitRecord(stgmInvtRec, false, true);
	}
	catch(exp)
	{
		nlapiLogExecution('Debug', 'Exception in CreateSTGInvtRecord',exp);
	}

	nlapiLogExecution('ERROR', 'Out of CreateSTGInvtRecord',TimeStampinSec());
}
