/***************************************************************************
                           eBizNET
                  eBizNET Solutions Inc
****************************************************************************
*
*  $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inbound/Schedulers/Attic/ebiz_ClearInboundTask_sch.js,v $
*  $Revision: 1.1.2.2 $
*  $Date: 2014/03/04 12:49:16 $
*  $Author: spendyala $
*  $Name: t_NSWMS_2014_1_2_0 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_ClearInboundTask_sch.js,v $
*  Revision 1.1.2.2  2014/03/04 12:49:16  spendyala
*  CASE201112/CR201113/LOG201121
*  Issue fixed related to case# 20127479
*
*  Revision 1.1.2.1  2013/06/28 15:56:24  spendyala
*  case# 20120121
*  New file created for dafiti to reduce the putaway confirmation time.
*
*
****************************************************************************/

function ClearInboundTask(type)
{
	try
	{
		nlapiLogExecution('ERROR','Into  ClearInboundTask','');
		var context = nlapiGetContext();
		nlapiLogExecution('ERROR','Remaining Usage 1',context.getRemainingUsage());
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty'));	
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['1','3']));
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['1','2']));
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'isnotempty'));
		//filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isnotempty'));

		var columns=new Array();
		columns[0]=new nlobjSearchColumn("internalid");
		columns[1]=new nlobjSearchColumn("custrecord_tasktype");
		columns[2]=new nlobjSearchColumn("custrecord_ebiz_nsconfirm_ref_no");

		var searchresults=nlapiSearchRecord("customrecord_ebiznet_trn_opentask",null,filters,columns);

		if(searchresults!=null&&searchresults!="")
		{
			for(var count=0;count<searchresults.length;count++)
			{
				if(context.getRemainingUsage()> 30)
				{
					var vtasktype=searchresults[count].getValue("custrecord_tasktype");
					var vnsref=searchresults[count].getValue("custrecord_ebiz_nsconfirm_ref_no");
					if((vtasktype==2&&vnsref!=null&&vnsref!="")||(vtasktype==1))
					{
						nlapiLogExecution('ERROR','Remaining Usage before move',context.getRemainingUsage());
						var opentaskrecid=searchresults[count].getValue("internalid");
						MoveTaskRecord(opentaskrecid);
						nlapiLogExecution('ERROR','Remaining Usage after move',context.getRemainingUsage());
					}
				}
				else
					nlapiLogExecution('ERROR','Remaining Usage less than 30 units',context.getRemainingUsage());
			}
		}
	}
	catch(e)
	{
		nlapiLogExecution("ERROR","Exception in move",e);
	}
}