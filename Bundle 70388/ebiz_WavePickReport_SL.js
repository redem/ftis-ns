/***************************************************************************
	 eBizNET Solutions
 ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_WavePickReport_SL.js,v $
 *     	   $Revision: 1.9.2.3.4.1.4.2 $
 *     	   $Date: 2013/03/19 12:08:11 $
 *     	   $Author: schepuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 *
 *****************************************************************************/
function wavePickReportSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Pick Report');
		var vQbWave="";
		var vshortPick="N";
		if(request.getParameter('ebiz_wave_no')!=null && request.getParameter('ebiz_wave_no')!="")
		{
			vQbWave = request.getParameter('ebiz_wave_no');			
			nlapiLogExecution('ERROR', 'vQbWave',vQbWave);
		}
		if(request.getParameter('short_pick')!=null && request.getParameter('short_pick')!="")
		{
			vshortPick= request.getParameter('short_pick');			
		} 	

		var msg= form.addField('custpage_message', 'inlinehtml', null, null, null);
		//createNewReqLink.setDefaultValue('<table width="100%" cellspacing="0" cellpadding="0" ><tr style="background-color:#DAEBD5;"  ><td width="4%" ><img src="https://system.netsuite.com/core/media/media.nl?id=380&c=TSTDRV770123&h=3cb2d0419e9831ae48dd" /></td><td width="80%"><span >Wave generated successfully(Wave# '+ vQbWave +' )</span></td> </tr></table>');
		if (vshortPick == 'Y') {
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Wave generated successfully(" + vQbWave + ") with short Picks', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
		}
		else
		{
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Wave generated successfully(" + vQbWave + ")', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
		}

		var waveno=	form.addField('custpage_wave','text','wave');
		waveno.setDisplayType('hidden');
		waveno.setDefaultValue(vQbWave);		

		var SOwave = new Array();
		SOwave ["ebiz_wave_no"] = vQbWave;	

		form.addSubmitButton('Confirm Picks');
		form.addButton('custpage_print','Print','Printreport2('+vQbWave+')');
		//form.setScript('customscript_pickreport_cl');  
		form.setScript('customscript_pickreportprint');

		var sublist = form.addSubList("custpage_items", "staticlist", "ItemList");
		sublist.addField("custpage_sono", "text", "SO #");
		sublist.addField("custpage_lineno", "text", "SO Line #");
		sublist.addField("custpage_tasktype", "text", "TaskType").setDisplayType('hidden');
		sublist.addField("custpage_itemname", "text", "Item");
		sublist.addField("custpage_lot", "text", "LOT/Batch #");
		sublist.addField("custpage_itemstatus", "text", "Item Status");
		sublist.addField("custpage_packcode", "text", "Pack Code");		
		sublist.addField("custpage_container", "text", "Container LP");
		sublist.addField("custpage_containersize", "text", "Container Size");
		sublist.addField("custpage_ordqty", "text", "Qty");
		sublist.addField("custpage_lpno", "text", "From LP #");
		sublist.addField("custpage_actbeginloc", "text", "Bin Location");
		sublist.addField("custpage_cluster", "text", "Cluster #");	
		sublist.addField("custpage_status", "text", "Status").setDisplayType('hidden');	
		sublist.addField("custpage_remarks", "text", "Remarks");	

		var filters = new Array();

		if (vQbWave != "") {
			filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(vQbWave)));
		}				
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9,26]));

		var columns = new Array();
		// code added from Monobind on 27Feb13 by santosh
		// change the sorting
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		columns[1] = new nlobjSearchColumn('custrecord_skiptask');
		columns[2] = new nlobjSearchColumn('custrecord_line_no');
		columns[3] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[4] = new nlobjSearchColumn('custrecord_bin_locgroup_seq');
		columns[5] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
		columns[6] = new nlobjSearchColumn('custrecord_sku');
		columns[7] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[8] = new nlobjSearchColumn('custrecord_tasktype');
		columns[9] = new nlobjSearchColumn('custrecord_from_lp_no');
		columns[10] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[11] = new nlobjSearchColumn('custrecord_sku_status');
		columns[12] = new nlobjSearchColumn('custrecord_packcode');
		columns[13] = new nlobjSearchColumn('name');
		columns[14] = new nlobjSearchColumn('custrecord_batch_no');
		columns[15] = new nlobjSearchColumn('custrecord_ebiz_clus_no');
		columns[16] = new nlobjSearchColumn('custrecord_container_lp_no');
		columns[17] = new nlobjSearchColumn('custrecord_container');
		columns[18] = new nlobjSearchColumn('custrecord_wms_status_flag');
		columns[19] = new nlobjSearchColumn('custrecord_notes');

		columns[0].setSort();
		columns[1].setSort();
		columns[4].setSort();
		columns[5].setSort();
		columns[6].setSort();
		columns[7].setSort(true);

		//upto here 
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

		var vline, vitem, vqty, vTaskType, vmainline, vrecid, vlocation, vLpno, vSKU, vlocationid, vinvrefno, 
		vskustatus, vpackcode, vdono,vlotbatch,vClusno,vSizeId,vwmsststus,vnotes;

		if(searchresults != null){
			for (var i = 0; i < searchresults.length; i++) {
				var searchresult = searchresults[i];
				vlotbatch = searchresult.getValue('custrecord_batch_no');
				nlapiLogExecution('ERROR','vlotbatch',vlotbatch);
				vline = searchresult.getValue('custrecord_line_no');
				vitem = searchresult.getValue('custrecord_ebiz_sku_no');
				vqty = searchresult.getValue('custrecord_expe_qty');
				vTaskType = searchresult.getText('custrecord_tasktype');
				vLpno = searchresult.getValue('custrecord_from_lp_no');
				vlocationid = searchresult.getValue('custrecord_actbeginloc');
				vContainerLpNo = searchresult.getValue('custrecord_container_lp_no');
				vlocation = searchresult.getText('custrecord_actbeginloc');
				vSizeId = searchresult.getText('custrecord_container');
				nlapiLogExecution('ERROR', "vlocationid" + vlocationid);
				nlapiLogExecution('ERROR', "vlocation" + vlocation);

				vSKU = searchresult.getText('custrecord_sku');
				vskustatus= searchresult.getText('custrecord_sku_status');
				vpackcode= searchresult.getValue('custrecord_packcode');
				vdono= searchresult.getValue('name');
				vClusno= searchresult.getValue('custrecord_ebiz_clus_no');
				vwmsststus=searchresult.getText('custrecord_wms_status_flag');
				vnotes=searchresult.getValue('custrecord_notes');

				form.getSubList('custpage_items').setLineItemValue('custpage_lineno', i + 1, vline);
				form.getSubList('custpage_items').setLineItemValue('custpage_tasktype', i + 1, vTaskType);
				form.getSubList('custpage_items').setLineItemValue('custpage_itemname', i + 1, vSKU);
				form.getSubList('custpage_items').setLineItemValue('custpage_itemstatus', i + 1, vskustatus);
				form.getSubList('custpage_items').setLineItemValue('custpage_packcode', i + 1, vpackcode);
				form.getSubList('custpage_items').setLineItemValue('custpage_sono', i + 1, vdono.split('.')[0]);
				form.getSubList('custpage_items').setLineItemValue('custpage_lot', i + 1, vlotbatch);
				form.getSubList('custpage_items').setLineItemValue('custpage_ordqty', i + 1, parseFloat(vqty));
				form.getSubList('custpage_items').setLineItemValue('custpage_lpno', i + 1, vLpno);
				form.getSubList('custpage_items').setLineItemValue('custpage_container', i + 1, vContainerLpNo);
				form.getSubList('custpage_items').setLineItemValue('custpage_containersize', i + 1, vSizeId);
				form.getSubList('custpage_items').setLineItemValue('custpage_actbeginloc', i + 1, vlocation);
				form.getSubList('custpage_items').setLineItemValue('custpage_cluster', i + 1, vClusno);
				form.getSubList('custpage_items').setLineItemValue('custpage_status', i + 1, vwmsststus);
				form.getSubList('custpage_items').setLineItemValue('custpage_remarks', i + 1, vnotes);
			}
		}
		response.writePage(form);
	}
	else //this is the POST block
	{
		var vWaveno = request.getParameter('custpage_wave');
		var SOwave = new Array();
		SOwave ["ebiz_wave_no"] = vWaveno+".0";	
		response.sendRedirect('SUITELET', 'customscript_confirmpicks', 'customdeploy_confirmpicks', false, SOwave );

	}
}
//code added from Monobind on 27Feb13 by santosh
function Printreport2(ebizwaveno){
	var waveno = nlapiGetFieldValue('custpage_wave');
	var fullfillment = nlapiGetFieldValue('custpage_do');
	nlapiLogExecution('ERROR', 'into Printreport ebizwaveno',ebizwaveno);
	var WavePDFURL = nlapiResolveURL('SUITELET', 'customscript_pickreportpdf', 'customdeploy_pickreportpdf');
	/*var ctx = nlapiGetContext();
	nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
	if (ctx.getEnvironment() == 'PRODUCTION') {
		WavePDFURL = 'https://system.netsuite.com' + WavePDFURL;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			WavePDFURL = 'https://system.sandbox.netsuite.com' + WavePDFURL;				
		}*/
	nlapiLogExecution('ERROR', 'Wave PDF URL',WavePDFURL);					
	WavePDFURL = WavePDFURL + '&custparam_ebiz_wave_no='+ waveno+'&custparam_ebiz_fullfilmentno='+fullfillment;
	nlapiLogExecution('ERROR', 'WavePDFURL',WavePDFURL);
//	alert(WavePDFURL);
	window.open(WavePDFURL);

}
// upto here


