/***************************************************************************
  eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Client/Attic/ebiz_QuickShipQb_CL.js,v $
 *     	   $Revision: 1.1.2.3.4.2.2.1 $
 *     	   $Date: 2015/12/02 13:29:33 $
 *     	   $Author: schepuri $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_QuickShipQb_CL.js,v $
 * Revision 1.1.2.3.4.2.2.1  2015/12/02 13:29:33  schepuri
 * case# 201415945
 *
 * Revision 1.1.2.3.4.2  2015/09/01 15:13:12  aanchal
 * 2015.2 issue fixes
 * 201414107
 *
 * Revision 1.1.2.3.4.1  2013/03/05 14:55:23  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Lexjet production as part of Standard bundle
 *
 * Revision 1.1.2.3  2013/01/24 01:32:36  kavitha
 * CASE201112/CR201113/LOG201121
 * Quick ship screen - Query block changes
 *
 * Revision 1.1.2.2  2013/01/04 14:54:34  grao
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.1.2.1  2013/01/02 18:09:37  kavitha
 * CASE201112/CR201113/LOG201121
 * Quick ship screen - Query block changes
 *
 *
 *****************************************************************************/
function OnSave()
{
	
	var vorderType = nlapiGetFieldValue('custpage_ordertype');
	var vContLP = nlapiGetFieldValue('custpage_qbcontlp');
	var vSoMultiple = nlapiGetFieldValue('custpage_somulti');
	var vSoWave = nlapiGetFieldValue('custpage_qbwave');
	var vSoCluster = nlapiGetFieldValue('custpage_qbcluster');
	var vSOStatus = nlapiGetFieldValue('custpage_qbstatus');
	var vFromDate = nlapiGetFieldValue('custpage_qbfromdate');
	var vToDate = nlapiGetFieldValue('custpage_qbtodate');
	
	var vShipmethod = nlapiGetFieldValue('custpage_qbcarriershipmethod');
	var vShipDate = nlapiGetFieldValue('custpage_qbshipdate');
	var vCustomer = nlapiGetFieldValue('custpage_qbcustomer');

		
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('tranid', null, 'is', vSoMultiple);
	filters[1] = new nlobjSearchFilter('mainline', null, 'is', 'T');
	
	var searchresults =  nlapiSearchRecord(vorderType, null, filters, null);
	//alert(searchresults);
	if(searchresults==null||searchresults==''||searchresults=='null')
	{
		alert('invalid order');
		return false;
	}
	
	//if((vContLP != null && vContLP != '')  || (vSoMultiple != null && vSoMultiple != '')   || (vSoWave != null && vSoWave != '') || (vSOStatus != null && vSOStatus != '') || (vFromDate != null && vFromDate != '') || (vToDate != null && vToDate != ''))
	if(((vContLP == null || vContLP == '')  && (vSoMultiple == null || vSoMultiple == '')  && (vSoWave == null || vSoWave == '') && (vSoCluster == null || vSoCluster == '')  && (vSOStatus != null && vSOStatus != '') && (vFromDate == null || vFromDate == '') && (vToDate == null || vToDate == '') && (vShipmethod == null || vShipmethod == '') && (vShipDate == null || vShipDate == '') && (vCustomer == null || vCustomer == ''))
			|| ((vContLP == null || vContLP == '')  && (vSoMultiple == null || vSoMultiple == '')  && (vSoWave == null || vSoWave == '') && (vSoCluster == null || vSoCluster == '') && (vSOStatus == null || vSOStatus == '') && (vFromDate == null || vFromDate == '') && (vToDate == null || vToDate == '') && (vShipmethod == null || vShipmethod == '') && (vShipDate == null || vShipDate == '') && (vCustomer == null || vCustomer == '')))
	{
		alert('Please Enter From Date,To Date');
		return false;
	}
	else if((vFromDate!=null && vFromDate!='') && (vToDate==null || vToDate==''))
	{
		
		alert('Please Enter To Date ');			
		return false;
	}
	else if((vFromDate == null || vFromDate== '') && (vToDate!=null && vToDate!=''))
	{		
		alert('Please Enter From Date ');			
		return false;
	}
	
	/*else
	{
		return true;
	}	*/
	try // case# 201413568
	{
		
		return vcheckAtleast('custpage_items', 'custpage_select','custpage_shipcharges');
	}
	catch(exp)
	{
		alert('Expception'+exp);
	}
	return true;
	
}

function Onchange(type,name ,linenum)
{
	if(trim(name)==trim('custpage_selectpage'))
	{
		nlapiSetFieldValue('custpage_hiddenfieldselectpage','T');
		NLDoMainFormButtonAction("submitter",true);	
	}
	else
	{
		return true;
	}
}
function vcheckAtleast(subListID, checkBoxName,shipcharge){
	var AtleastonelinetoSelected="T";// case# 201413568
	var lineCnt = nlapiGetLineItemCount(subListID);
	var shipcheckvalid = "T";
	var hiddenField = nlapiGetFieldValue("custpage_hiddenfield");
	//alert(hiddenField);
	for (var s = 1; s <= lineCnt; s++) 
	{
		var linecheck= nlapiGetLineItemValue(subListID,checkBoxName,s);
		var shipcheck= nlapiGetLineItemValue(subListID,shipcharge,s);

		if(isNaN(shipcheck) || shipcheck < 0)
		{
		shipcheckvalid = 'F';
		}
		
		
		if (linecheck == "T") {	
			AtleastonelinetoSelected="T";
			break;

		}
		else
		{
			AtleastonelinetoSelected="F";
		}
	}
	if(AtleastonelinetoSelected=="F" && hiddenField =='F')
	{
		alert("Please Select atleast one value(s)");
		return false;
	}
	else{
		//alert(shipcheckvalid);

		if(shipcheckvalid == 'F')
		{
		alert("Please Select valid ship charges");
		return false;	
		}

	}
	
	return true;
}
