/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/Attic/ebiz_WOPickReport_SL.js,v $

 *     	   $Revision: 1.1.2.1.2.1 $
 *     	   $Date: 2015/11/13 15:55:05 $
 *     	   $Author: rmukkera $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_128 $

 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 * *
 * DESCRIPTION
 *  	
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_WOPickReport_SL.js,v $
 * Revision 1.1.2.1.2.1  2015/11/13 15:55:05  rmukkera
 * case # 201414431
 *
 * Revision 1.1.2.1  2015/02/13 15:03:30  sponnaganti
 * Jawbone Cluster Picking CR Changes
 * CR#  CR14US0323
 *
 * Revision 1.20.2.10.4.3.4.14  2014/04/29 06:54:24  sponnaganti
 * case# 20148207
 * (wave number is not displaying)
 *
 * Revision 1.20.2.10.4.3.4.13  2014/04/22 09:16:34  nneelam
 * case#  20127499
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.20.2.10.4.3.4.12  2014/03/14 19:46:21  grao
 * Case# 20127718 related issue fixes in Leisure Living issue fixes
 *
 * Revision 1.20.2.10.4.3.4.11  2014/02/03 14:54:25  snimmakayala
 * no message
 *
 * Revision 1.20.2.10.4.3.4.10  2014/01/10 16:00:35  skreddy
 * case#20126740
 * Afosa SB issue fix
 *
 * Revision 1.20.2.10.4.3.4.9  2013/09/03 00:03:58  nneelam
 * Case#.20124229ï¿½
 * Pic Report Issue Fixed...
 *
 * Revision 1.20.2.10.4.3.4.8  2013/09/02 15:48:46  skreddy
 * Case# 20124184
 * standard bundle issue fix
 *
 * Revision 1.20.2.10.4.3.4.7  2013/08/09 15:22:42  rmukkera
 * standard bundle issue fix
 *
 * Revision 1.20.2.10.4.3.4.6  2013/07/01 09:39:27  mbpragada
 * Case# 20123252
 * Script execution error
 *
 * Revision 1.20.2.10.4.3.4.5  2013/07/01 08:51:19  mbpragada
 * Case# 20123252
 * Script execution error
 *
 * Revision 1.20.2.10.4.3.4.4  2013/06/10 14:56:55  rmukkera
 * FIX for
 * After pick confirmation if pick report is reloaded or refreshed, displaying suite script error
 *
 * Revision 1.20.2.10.4.3.4.3  2013/06/10 06:19:01  skreddy
 * CASE201112/CR201113/LOG201121
 * issue rellated to pick report display
 *
 * Revision 1.20.2.10.4.3.4.2  2013/05/01 15:29:52  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.20.2.10.4.3.4.1  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.20.2.10.4.3  2012/11/27 19:08:29  spendyala
 * CASE201112/CR201113/LOG201121
 * Added new column i.e. no of cases to the report
 *
 * Revision 1.20.2.10.4.2  2012/11/08 14:01:30  schepuri
 * CASE201112/CR201113/LOG201121
 * added special instruction field
 *
 * Revision 1.20.2.10.4.1  2012/10/26 08:05:40  gkalla
 * CASE201112/CR201113/LOG201121
 * Pick report by order sorting
 *
 * Revision 1.20.2.10  2012/09/07 03:47:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.20.2.9  2012/08/03 00:50:11  gkalla
 * CASE201112/CR201113/LOG201121
 * Added header level remarks
 *
 * Revision 1.20.2.8  2012/08/01 07:29:40  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Routing
 *
 * Revision 1.20.2.7  2012/07/27 13:01:49  schepuri
 * CASE201112/CR201113/LOG201121
 * Modifying text to text area for item
 *
 * Revision 1.20.2.6  2012/07/26 06:04:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.20.2.5  2012/06/15 15:20:48  mbpragada
 * 20120490
 *
 * Revision 1.20.2.4  2012/05/03 21:17:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.20.2.3  2012/04/20 14:21:46  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.20.2.2  2012/04/20 08:36:21  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Zone Picking
 *
 * Revision 1.20  2012/01/06 13:00:50  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Added Remarks field to pick report to show failure for picks.
 *
 * Revision 1.19  2012/01/02 07:49:32  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.18  2011/10/31 11:39:48  gkalla
 * CASE201112/CR201113/LOG201121
 * To get the Wave#, fulfillment Order# and cluster#s in Sorting order
 *
 * Revision 1.17  2011/10/13 17:06:09  spendyala
 * CASE201112/CR201113/LOG201121
 * added printreport2 function for wavepickreport generation
 *
 * Revision 1.16  2011/10/12 20:00:15  spendyala
 * CASE201112/CR201113/LOG201121
 * uncommitted the display button
 *
 * Revision 1.15  2011/10/12 19:34:02  spendyala
 * CASE201112/CR201113/LOG201121
 * T&T issue is fixed
 *
 *
 *
 **********************************************************************************************************************/

function FillWave(form, WaveField,maxno,vRoleLocation){

	var filtersso = new Array();
	filtersso.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));  // Pick task
	//Status Flag='G'(PICK GENERATED),'F' (OUTBOUND.FAILED)
	filtersso.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','26'])); 
	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'isnotempty'));
	filtersso.push(new nlobjSearchFilter('type', 'custrecord_ebiz_order_no', 'is', 'WorkOrd'));	// To Get Only Work Order Records
	filtersso.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is', 'T'));
	//case# 20148207  starts (wave number is not displaying) 
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		filtersso.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vRoleLocation));

	}
	
	if(maxno!=-1)
	{
		filtersso.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno)));
	}
	
	var columnso = new Array();
	columnso.push(new nlobjSearchColumn('id').setSort('true'));	
	columnso.push(new nlobjSearchColumn('custrecord_ebiz_wave_no').setSort('true'));
	columnso.push(new nlobjSearchColumn('name'));
	//columnso.push(new nlobjSearchColumn('custrecord_ebiz_wave_no',null,'group').setSort('true'));
	//columnso.push(new nlobjSearchColumn('name',null,'group'));

	//WaveField.addSelectOption("", "");
	
	//case# 20148207 end

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso,columnso);

	for (var i = 0; searchresults != null && i < searchresults.length; i++) {

		//if(searchresults[i].getValue('custrecord_ebiz_wave_no',null,'group') != null && searchresults[i].getValue('custrecord_ebiz_wave_no',null,'group') != "" && searchresults[i].getValue('custrecord_ebiz_wave_no',null,'group') != " ")
		if(searchresults[i].getValue('custrecord_ebiz_wave_no') != null && searchresults[i].getValue('custrecord_ebiz_wave_no') != "" && searchresults[i].getValue('custrecord_ebiz_wave_no') != " ")
		{
			var resdo = form.getField('custpage_wave').getSelectOptions(searchresults[i].getValue('custrecord_ebiz_wave_no'), 'is');
			if (resdo != null) {
				if (resdo.length > 0) {
					continue;
				}
			}
		}
		WaveField.addSelectOption(searchresults[i].getValue('custrecord_ebiz_wave_no'), searchresults[i].getValue('custrecord_ebiz_wave_no'));
	}

	//case# 20148207  starts (wave number is not displaying) 
	if(searchresults!=null && searchresults.length>=1000)
	{
		var maxno=searchresults[searchresults.length-1].getValue('id');		
		FillWave(form, WaveField,maxno);	
	}
	//case# 20148207 end
}

function GetWaves()
{
	var filtersso = new Array();
	filtersso.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));  // Pick task
	//Status Flag='G'(PICK GENERATED),'F' (OUTBOUND.FAILED)
	filtersso.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','26'])); 
	filtersso.push(new nlobjSearchFilter('type', 'custrecord_ebiz_order_no', 'is', 'WorkOrd'));	// To Get Only Work Order Records
	filtersso.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is', 'T'));
	var columnso = new Array();
	columnso.push(new nlobjSearchColumn('custrecord_ebiz_wave_no').setSort('true'));
	columnso.push(new nlobjSearchColumn('name'));

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso,columnso);
	return searchresults;
}

function GetOrders()
{
	var filtersso = new Array();
	filtersso.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));  // Pick task
	//Status Flag='G'(PICK GENERATED),'F' (OUTBOUND.FAILED)
	filtersso.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','26']));
	filtersso.push(new nlobjSearchFilter('type', 'custrecord_ebiz_order_no', 'is', 'WorkOrd'));	// To Get Only Work Order Records
	filtersso.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is', 'T'));
	var columnso = new Array();
	columnso.push(new nlobjSearchColumn('name').setSort('true'));

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso,columnso);
	return searchresults;
}

function GetClusters(form, ClusterNoField,maxno,vRoleLocation)
{
	var filtersso = new Array();
	filtersso.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));  // Pick task
	//Status Flag='G'(PICK GENERATED),'F' (OUTBOUND.FAILED)
	filtersso.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','26']));  
	filtersso.push(new nlobjSearchFilter('type', 'custrecord_ebiz_order_no', 'is', 'WorkOrd'));	// To Get Only Work Order Records
	filtersso.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is', 'T'));
	
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		filtersso.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vRoleLocation));

	}
	
	if(maxno!=-1)
	{
		filtersso.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno)));
	}
	
	var columnso = new Array();
	columnso.push(new nlobjSearchColumn('id').setSort('true'));	
	columnso.push(new nlobjSearchColumn('custrecord_ebiz_clus_no').setSort('true'));

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso,columnso);
	nlapiLogExecution('ERROR', 'searchresults in getclusters',searchresults);
	if (searchresults != null && searchresults != "" && searchresults.length>0) {
		for (var i = 0; i < searchresults.length; i++) {
			//nlapiLogExecution('ERROR', 'searchresults[i].getValue(custrecord_ebiz_clus_no) ',searchresults[i].getValue('custrecord_ebiz_clus_no'));
			if(searchresults[i].getValue('custrecord_ebiz_clus_no')!=null && searchresults[i].getValue('custrecord_ebiz_clus_no')!="")
			{
				var res=  form.getField('custpage_custerno').getSelectOptions(searchresults[i].getValue('custrecord_ebiz_clus_no'), 'is');
				if (res != null) {				
					if (res.length > 0) {
						continue;
					}
				}

				ClusterNoField.addSelectOption(searchresults[i].getValue('custrecord_ebiz_clus_no'), searchresults[i].getValue('custrecord_ebiz_clus_no'));
			}
		}
	}
	if(searchresults!=null && searchresults.length>=1000)
	{
		var maxno=searchresults[searchresults.length-1].getValue('id');		
		GetClusters(form, ClusterNoField,maxno);	
	}
	
}

function PickReportSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('WO Pick Report');
		var vQbWave = "";
		form.setScript('customscript_wopickreport');

		var WaveField = form.addField('custpage_wave', 'select', 'Wave');
		WaveField.setLayoutType('startrow', 'none');
		WaveField.addSelectOption("", "");
		var vWave = request.getParameter('custpage_waveno');
		WaveField.setDefaultValue(vQbWave);
		/*var shipmentField = form.addField('custpage_qbshipmentno', 'select', 'Shipment #');
		shipmentField .setLayoutType('startrow', 'none');		
		fillfulfillorderField(form, shipmentField,-1);*/
		FillWave(form, WaveField,-1);

		/*var DoField = form.addField('custpage_do', 'select', 'Fulfillment Ord#');
		DoField.setLayoutType('startrow', 'none');
		DoField.addSelectOption("", "");*/

		var ClusterNoField = form.addField('custpage_custerno', 'select', 'Cluster #');
		ClusterNoField.setLayoutType('startrow', 'none');
		ClusterNoField.addSelectOption("", "");
		GetClusters(form, ClusterNoField,-1);
		/*var searchResultesOrder= GetOrders();
		if (searchResultesOrder != null && searchResultesOrder != "" && searchResultesOrder.length>0) {
			for (var i = 0; i < searchResultesOrder.length; i++) {
				if(searchResultesOrder[i].getValue('name')!=null && searchResultesOrder[i].getValue('name')!="")
				{
					var res=  form.getField('custpage_do').getSelectOptions(searchResultesOrder[i].getValue('name'), 'is');
					if (res != null) {				
						if (res.length > 0) {
							continue;
						}
					}

					DoField.addSelectOption(searchResultesOrder[i].getValue('name'), searchResultesOrder[i].getValue('name'));
				}
			}
		}*/

		/*var searchResultesCluster= GetClusters();
		nlapiLogExecution('ERROR', 'searchResultesCluster ',searchResultesCluster);
		if (searchResultesCluster != null && searchResultesCluster != "" && searchResultesCluster.length>0) {
			for (var i = 0; i < searchResultesCluster.length; i++) {
				nlapiLogExecution('ERROR', 'searchResultesCluster[i].getValue(custrecord_ebiz_clus_no) ',searchResultesCluster[i].getValue('custrecord_ebiz_clus_no'));
				if(searchResultesCluster[i].getValue('custrecord_ebiz_clus_no')!=null && searchResultesCluster[i].getValue('custrecord_ebiz_clus_no')!="")
				{
					var res=  form.getField('custpage_custerno').getSelectOptions(searchResultesCluster[i].getValue('custrecord_ebiz_clus_no'), 'is');
					if (res != null) {				
						if (res.length > 0) {
							continue;
						}
					}

					ClusterNoField.addSelectOption(searchResultesCluster[i].getValue('custrecord_ebiz_clus_no'), searchResultesCluster[i].getValue('custrecord_ebiz_clus_no'));
				}
			}
		}*/

		form.addSubmitButton('Display');

		response.writePage(form);
	}
	else //this is the POST block
	{
		var form = nlapiCreateForm('WO Pick Report');
		var vQbWave = request.getParameter('custpage_wave');
		var vQbdo = request.getParameter('custpage_do');
		var vQbClusterNo = request.getParameter('custpage_custerno');
		nlapiLogExecution('ERROR', 'vQbdo1', vQbdo);
		// If wave number is empty, then retrieve the wave number based on the fulfillment order no
		if((vQbWave == null || vQbWave == "") && (vQbdo != null || vQbdo != ""))
		{
			// fetch the wave number
			vQbWave = getWaveNoForFulfillOrder(vQbdo,vQbClusterNo);
		}
		nlapiLogExecution('ERROR', 'vQbClusterNo', vQbClusterNo);

		if((vQbWave == null || vQbWave == "") && (vQbClusterNo != null || vQbClusterNo != "") && (vQbdo == null || vQbdo == ""))
		{
			// fetch the wave number
			vQbWave = getWaveNoForFulfillOrder(vQbdo,vQbClusterNo);
		}
		nlapiLogExecution('ERROR', 'vQbWave', vQbWave);
		nlapiLogExecution('ERROR', 'vQbdo2', vQbdo);

		var pickgenflag=ispickgencompleted(vQbWave,vQbdo);

		if(pickgenflag=='T')
		{
			form.setScript('customscript_wopickreport');
			form.addButton('custpage_print','Print','Printreport('+vQbWave+')');

			var WaveField = form.addField('custpage_wave', 'select', 'Wave');
			WaveField.setLayoutType('startrow', 'none');
			WaveField.addSelectOption("", "");
			/*var shipmentField = form.addField('custpage_qbshipmentno', 'select', 'Shipment #');
			shipmentField .setLayoutType('startrow', 'none');		
			fillfulfillorderField(form, shipmentField,-1);*/
			FillWave(form, WaveField,-1);
			/*var DoField = form.addField('custpage_do', 'select', 'Fulfillment Ord#');
			DoField.setLayoutType('startrow', 'none');

			DoField.addSelectOption("", "");

			var searchResultesOrder= GetOrders();
			if (searchResultesOrder != null && searchResultesOrder != "" && searchResultesOrder.length>0) {
				for (var i = 0; i < searchResultesOrder.length; i++) {
					if(searchResultesOrder[i].getValue('name')!=null && searchResultesOrder[i].getValue('name')!="")
					{
						var res=  form.getField('custpage_do').getSelectOptions(searchResultesOrder[i].getValue('name'), 'is');
						if (res != null) {				
							if (res.length > 0) {
								continue;
							}
						}

						DoField.addSelectOption(searchResultesOrder[i].getValue('name'), searchResultesOrder[i].getValue('name'));
					}
				}
			}*/

			WaveField.setDefaultValue(vQbWave);
			//DoField.setDefaultValue(vQbdo);

			var sysdate=DateStamp();
			var date=form.addField('custpage_date', 'text', 'Date').setDisplayType('inline');
			date.setDefaultValue(sysdate);

			var ClusterNoField = form.addField('custpage_custerno', 'select', 'Cluster #');
			ClusterNoField.setLayoutType('startrow', 'none');
			ClusterNoField.addSelectOption("", "");
			GetClusters(form, ClusterNoField,-1);
			/*var searchResultesCluster= GetClusters();
			nlapiLogExecution('ERROR', 'searchResultesCluster ',searchResultesCluster);
			if (searchResultesCluster != null && searchResultesCluster != "" && searchResultesCluster.length>0) {
				for (var i = 0; i < searchResultesCluster.length; i++) {
					if(searchResultesCluster[i].getValue('custrecord_ebiz_clus_no')!=null && searchResultesCluster[i].getValue('custrecord_ebiz_clus_no')!="")
					{
						var res=  form.getField('custpage_custerno').getSelectOptions(searchResultesCluster[i].getValue('custrecord_ebiz_clus_no'), 'is');
						if (res != null) {				
							if (res.length > 0) {
								continue;
							}
						}

						ClusterNoField.addSelectOption(searchResultesCluster[i].getValue('custrecord_ebiz_clus_no'), searchResultesCluster[i].getValue('custrecord_ebiz_clus_no'));
					}
				}
			}*/
			ClusterNoField.setDefaultValue(vQbClusterNo);
			form.addSubmitButton('Display');

			var sublist = form.addSubList("custpage_items", "list", "ItemList");
			sublist.addField("custpage_tasktype", "text", "TaskType").setDisplayType('hidden');
			sublist.addField("custpage_waveno", "text", "Wave#");
			sublist.addField("custpage_sono", "text", "WO #");
			//sublist.addField("custpage_fulfillmentno", "text", "Fulfillment#");
			sublist.addField("custpage_lineno", "text", "WO Line #");
			sublist.addField("custpage_itemname", "text", "Item");
			sublist.addField("custpage_upccode", "text", "UPC Code");
			sublist.addField("custpage_itemdesc", "textarea", "Item Description");
			sublist.addField("custpage_itemstatus", "text", "Item Status");
			sublist.addField("custpage_packcode", "text", "Pack Code");	
			sublist.addField("custpage_lot", "text", "LOT/Batch #");
			sublist.addField("custpage_expdate", "text", "Expiry Date");			
			sublist.addField("custpage_ordqty", "text", "Qty");
			sublist.addField("custpage_noofcases", "text", "# of Cases");
			sublist.addField("custpage_actbeginloc", "text", "Bin Location");
			sublist.addField("custpage_lpno", "text", "From LP #");
			sublist.addField("custpage_container", "text", "Container LP");
			sublist.addField("custpage_containersize", "text", "Container Size");
			sublist.addField("custpage_clusternumber", "text", "Cluster #");
			sublist.addField("custpage_status", "text", "Status").setDisplayType('hidden');	
			sublist.addField("custpage_remarks", "text", "Remarks");	
			sublist.addField("custpage_splinstruction", "text", "Instructions");	

			var filters = new Array();

			var j = 0;
			if (vQbWave != "" && vQbWave!=null) {
				filters[j] = new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', vQbWave);
				j = j + 1;
			}
			if (vQbdo != "" && vQbdo !=null) {

				filters[j] = new nlobjSearchFilter('name', null, 'is', vQbdo);
				j = j + 1;
			}			
			if(vQbClusterNo!="" && vQbClusterNo!=null)
			{
				filters[j] = new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vQbClusterNo);
				j = j + 1;
			}
			//Status Flag='G'(PICK GENERATED),'F' (OUTBOUND.FAILED)
			filters[j] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','26']);
			j = j + 1;
			filters[j] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']); // PICK
			j = j + 1;
			filters[j]  = new nlobjSearchFilter('type', 'custrecord_ebiz_order_no', 'is', 'WorkOrd');	// To Get Only Work Order Records
			j = j + 1;
			filters[j]  = new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is', 'T');
			var columns = new Array();

			columns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');
			columns[1] = new nlobjSearchColumn('custrecord_skiptask');
			columns[2] = new nlobjSearchColumn('custrecord_line_no');
			columns[3] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			columns[4] = new nlobjSearchColumn('custrecord_bin_locgroup_seq');
			columns[5] = new nlobjSearchColumn('custrecord_ebiz_zoneid');
			columns[6] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
			columns[7] = new nlobjSearchColumn('custrecord_sku');
			columns[8] = new nlobjSearchColumn('custrecord_expe_qty');
			columns[9] = new nlobjSearchColumn('custrecord_tasktype');
			columns[10] = new nlobjSearchColumn('custrecord_lpno');
			columns[11] = new nlobjSearchColumn('custrecord_actbeginloc');
			columns[12] = new nlobjSearchColumn('custrecord_sku_status');
			columns[13] = new nlobjSearchColumn('custrecord_packcode');
			columns[14] = new nlobjSearchColumn('name');
			columns[15] = new nlobjSearchColumn('custrecord_batch_no');
			columns[16] = new nlobjSearchColumn('custrecord_container_lp_no');
			columns[17] = new nlobjSearchColumn('custrecord_container');
			columns[18] = new nlobjSearchColumn('custrecord_ebiz_clus_no');
			columns[19] = new nlobjSearchColumn('description','custrecord_sku');
			columns[20] = new nlobjSearchColumn('upccode','custrecord_sku');
			columns[21] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
			columns[22] = new nlobjSearchColumn('custrecord_wms_status_flag');
			columns[23] = new nlobjSearchColumn('custrecord_notes');
			//columns[23] = new nlobjSearchColumn('custrecord_ebiz_zoneid');
			columns[24] = new nlobjSearchColumn('custrecord_expirydate');
			columns[25] = new nlobjSearchColumn('custrecord_invref_no');


			/*columns[0].setSort();
			columns[1].setSort();
			columns[4].setSort();*/
			columns[5].setSort();
			columns[6].setSort();
			columns[7].setSort();


			var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

			var itemsarr = new Array();
			var itemdimsarr = new Array();

			if(searchresults!=null && searchresults.length > 0)
			{
				for (var i1 = 0; i1 < searchresults.length; i1++){
					var searchresult = searchresults[i1];
					itemsarr.push(searchresult.getValue('custrecord_ebiz_sku_no'));
				}
			}
			if(itemsarr.length>0)
			{
				itemsarr = removeDuplicateElement(itemsarr);

				nlapiLogExecution('ERROR', 'itemsarr',itemsarr);

				nlapiLogExecution('ERROR', 'Time Stamp Before calling  for all items',TimeStampinSec());
				itemdimsarr = getItemDimensions(itemsarr,-1);
				nlapiLogExecution('ERROR', 'Time Stamp After calling  for all items',TimeStampinSec());

			}
			var vline, vitem, vqty, vTaskType, vmainline, vrecid, vlocation, vLpno, vSKU, vlocationid, vinvrefno, 
			vskustatus, vpackcode, vdono,vlotbatch,vClusterNo,vsizeid,vwaveno,vwmsststus,vnotes,vcaseqty,vnoofcases,vebizsoid;
			var upccode="";
			var itemdesc="";
			var vMainRemarks;
			var vExpiryDate,trantype='',vspecialIns='',itemfardpickflag='';

			// CASE# 20123252 - Start
			if(searchresults != null)
				trantype = nlapiLookupField('transaction', searchresults[0].getValue('custrecord_ebiz_order_no'), 'recordType');

			var OrderrList = getDistinctOrdersList(searchresults);	
			var getallOrderresults=GetOrderDetails(OrderrList,trantype);

			var skulist = getDistinctSKUsFromTaskList(searchresults);
			var skuDetails = GetSKUDetails(skulist);
			// CASE# 20123252 - End
			for (var i = 0; searchresults != null && i < searchresults.length; i++) {
				var searchresult = searchresults[i];
				lotbatch = searchresult.getValue('custrecord_batch_no');
				line = searchresult.getValue('custrecord_line_no');
				item = searchresult.getValue('custrecord_ebiz_sku_no');
				qty = searchresult.getValue('custrecord_expe_qty');
				TaskType = searchresult.getText('custrecord_tasktype');
				Lpno = searchresult.getValue('custrecord_lpno');
				locationid = searchresult.getValue('custrecord_actbeginloc');
				ContainerLpNo = searchresult.getValue('custrecord_container_lp_no');
				location = searchresult.getText('custrecord_actbeginloc');
				SKU = searchresult.getText('custrecord_sku');
				skustatus = searchresult.getText('custrecord_sku_status');
				packcode = searchresult.getValue('custrecord_packcode');
				dono = searchresult.getValue('name');
				vClusterNo = searchresult.getValue('custrecord_ebiz_clus_no');
				vsizeid = searchresult.getText('custrecord_container');
				itemdesc=searchresult.getValue('description','custrecord_sku');
				upccode=searchresult.getValue('upccode','custrecord_sku');
				vwaveno=searchresult.getValue('custrecord_ebiz_wave_no');
				vwmsststus=searchresult.getText('custrecord_wms_status_flag');
				vnotes=searchresult.getValue('custrecord_notes');
				vzoneid=searchresult.getText('custrecord_ebiz_zoneid');
				vExpiryDate=searchresult.getValue('custrecord_expirydate');
				vebizsoid=searchresult.getValue('custrecord_ebiz_order_no');
				vinvRefNo=searchresult.getValue('custrecord_invref_no');	
				if (itemdimsarr != null && itemdimsarr.length > 0) 
				{	
					nlapiLogExecution('ERROR', 'AllItemDims.length', itemdimsarr.length);
					for(var p = 0; p < itemdimsarr.length; p++)
					{
						//var itemval = AllItemDims[p].getValue('custrecord_ebizitemdims');
						//var itemtext = AllItemDims[p].getText('custrecord_ebizitemdims');

						var itemval = itemdimsarr[p][0];

						if(itemdimsarr[p][0] == item)
						{
							nlapiLogExecution('ERROR', 'Item Dims configured for SKU: ', item);

							var skuDim = itemdimsarr[p][2];
							var skuQty = itemdimsarr[p][3];	
							var packflag = itemdimsarr[p][5];	
							var weight = itemdimsarr[p][6];
							var cube = itemdimsarr[p][7];
							var uom = itemdimsarr[p][1];

//							nlapiLogExecution('ERROR', 'UOM Level : ', skuDim);
//							nlapiLogExecution('ERROR', 'UOM Qty : ', skuQty);
//							nlapiLogExecution('ERROR', 'UOM Pack Flag : ', packflag);
//							nlapiLogExecution('ERROR', 'Weight : ', weight);
//							nlapiLogExecution('ERROR', 'Cube : ', cube);

							//UOM LEVEL(Internal ID)  : 1-EACH		2-CASE		3-PALLET	4-InnerPack

							//if(skuDim == '1'){
							//	eachQty = skuQty;
							//	eachpackflag=packflag;
							//	eachweight = weight;
							//	eachcube = cube;
							//}								
							//else 
							if(skuDim == '2'){
								vcaseqty = skuQty;
								//casepackflag=packflag;
								//caseweight = weight;
								//casecube=cube;
							}								
							//else if(skuDim == '3'){
							//	palletQty = skuQty;
							//	palletpackflag = packflag;
							//	palletweight=weight;
							//	palletcube=cube;
							//}	
							//else if(skuDim == '4'){
							//	InnerPackQty = skuQty;
							//	InnerPackflag = packflag;
							//	InnerPackweight=weight;
							//	InnerPackcube=cube;
							//}
						}	
					}
				}
				nlapiLogExecution('ERROR', 'Case Qty: ', vcaseqty);
				//20124184 start

				if(vcaseqty != 0 && vcaseqty!='' && vcaseqty!=null && !isNaN(vcaseqty) ){
					vnoofcases = parseFloat(qty / vcaseqty);

				}
				else{
					//case# 20124229 Start
					if(vqty==null || vqty=='')
						vqty=0;
					//case# 20124229 End
					vnoofcases = parseFloat(qty);
				}
				nlapiLogExecution('ERROR', '# of Cases: ', vnoofcases);

				//var trantype = nlapiLookupField('transaction', vebizsoid, 'recordType');
				//var vspecialIns = nlapiLookupField(trantype, vebizsoid, 'custbody2');
				var IndividualspecialInsList = GetIndividiualsplinstructDetails(getallOrderresults, vebizsoid);
				if(IndividualspecialInsList != null && IndividualspecialInsList !='')
					vspecialIns= IndividualspecialInsList[0][0]; 
				//var vspecialIns = nlapiLookupField(trantype, vebizsoid, 'custbody_nswmspoinstructions');
				nlapiLogExecution('ERROR','vspecialIns',vspecialIns);

				//var itemfardpickflag = nlapiLookupField('item', item, 'custitem_ebiz_forwardpick');
				IndividualfardpickflagList = GetfardpickflagInfo(skuDetails, item);			
				if(IndividualfardpickflagList != null && IndividualfardpickflagList != '')
					itemfardpickflag= IndividualfardpickflagList[0][0]; //SKU  	
				nlapiLogExecution('ERROR','itemfardpickflag',itemfardpickflag);

				if(vinvRefNo==null || vinvRefNo=='' && itemfardpickflag != null && itemfardpickflag != '' && itemfardpickflag != 'F')
				{
					if(vspecialIns!=null && vspecialIns!='null' && vspecialIns!='undefined' && vspecialIns!='')
					{
						vspecialIns=vspecialIns+" and Open Replens Available.";
					}
					else
					{
						vspecialIns="Open Replens Available.";
					}
				}
				else
				{
					if(vspecialIns!=null && vspecialIns!='null' && vspecialIns!='undefined' && vspecialIns!='')
					{
						vspecialIns=vspecialIns;
					}
				}


				//SpecialIns.setDefaultValue(vspecialIns);
				nlapiLogExecution('ERROR','UPCCODE',upccode);
				nlapiLogExecution('ERROR','ITEMDESC',itemdesc);
				form.getSubList('custpage_items').setLineItemValue('custpage_lineno', i + 1, line);
				form.getSubList('custpage_items').setLineItemValue('custpage_tasktype', i + 1, TaskType);
				form.getSubList('custpage_items').setLineItemValue('custpage_itemname', i + 1, SKU);
				form.getSubList('custpage_items').setLineItemValue('custpage_upccode', i + 1, upccode);
				form.getSubList('custpage_items').setLineItemValue('custpage_itemdesc', i + 1, itemdesc);
				form.getSubList('custpage_items').setLineItemValue('custpage_itemstatus', i + 1, skustatus);
				form.getSubList('custpage_items').setLineItemValue('custpage_packcode', i + 1, packcode);
				form.getSubList('custpage_items').setLineItemValue('custpage_sono', i + 1, dono.split('.')[0]);
				form.getSubList('custpage_items').setLineItemValue('custpage_lot', i + 1, lotbatch);
				form.getSubList('custpage_items').setLineItemValue('custpage_expdate', i + 1, vExpiryDate);
				form.getSubList('custpage_items').setLineItemValue('custpage_ordqty', i + 1, qty);
				form.getSubList('custpage_items').setLineItemValue('custpage_lpno', i + 1, Lpno);
				form.getSubList('custpage_items').setLineItemValue('custpage_container', i + 1, ContainerLpNo);
				form.getSubList('custpage_items').setLineItemValue('custpage_containersize', i + 1, vsizeid);
				form.getSubList('custpage_items').setLineItemValue('custpage_actbeginloc', i + 1, location);
				form.getSubList('custpage_items').setLineItemValue('custpage_clusternumber', i + 1, vClusterNo);
				form.getSubList('custpage_items').setLineItemValue('custpage_waveno', i + 1, vwaveno);
				//form.getSubList('custpage_items').setLineItemValue('custpage_fulfillmentno', i + 1, dono);
				form.getSubList('custpage_items').setLineItemValue('custpage_status', i + 1, vwmsststus);
				form.getSubList('custpage_items').setLineItemValue('custpage_splinstruction', i + 1, vspecialIns);
				if(vnotes != null && vnotes != '' && vnotes!='undefined')
				{
					form.getSubList('custpage_items').setLineItemValue('custpage_remarks', i + 1,'<font color=#ff0000><b>' +  vnotes + '</b></font>');
					vMainRemarks='T';
				}
				form.getSubList('custpage_items').setLineItemValue('custpage_zone', i + 1, vzoneid);
				form.getSubList('custpage_items').setLineItemValue('custpage_noofcases', i + 1, vnoofcases.toFixed(2));
			}
			if(vMainRemarks =='T')
			{
				var vMainNotes=form.addField('custpage_notes', 'text', 'Note').setDisplayType('inline');
				vMainNotes.setDefaultValue('<font color=#ff0000><b>Some of the pick tasks are with Insufficient Inventory</b></font>');
			}
		}
		else
		{
			var msg= form.addField('custpage_message', 'inlinehtml', null, null, null);
			var msgtext = " Pick report is not yet ready as the pick generation is in progress for this wave "+vQbWave;
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', '"+ msgtext + "', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
		}
		response.writePage(form);
	}
}

function getWaveNoForFulfillOrder(fulfillOrderNo,clusterno){
	nlapiLogExecution('ERROR', 'getWaveNoForFulfillOrder', 'Start');
	var waveNo = "";

	if((fulfillOrderNo != null && fulfillOrderNo != "")||(clusterno!=null && clusterno!='')){
		var filters = new Array();
		if(fulfillOrderNo!=null && fulfillOrderNo!='')
		{
			filters.push(new nlobjSearchFilter('name', null, 'is', fulfillOrderNo));
		}
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3')); //Pick Task
		//Status Flag='G'(PICK GENERATED),'F' (OUTBOUND.FAILED)
		//filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','26']));
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9']));

		if(clusterno!="" && clusterno!=null)
		{
			filters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', clusterno));

		}

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_wave_no');

		var fulfillOrderList = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

		if(fulfillOrderList != null && fulfillOrderList.length > 0){
			waveNo = fulfillOrderList[0].getValue('custrecord_ebiz_wave_no');
		}
	}

	nlapiLogExecution('ERROR', 'fulfillOrderNo | waveNo', fulfillOrderNo + '|' + waveNo);
	nlapiLogExecution('ERROR', 'getWaveNoForFulfillOrder', 'End');
	return waveNo;
}

function Printreport(ebizwaveno){
	var waveno = nlapiGetFieldValue('custpage_wave');
	//var fullfillment = nlapiGetFieldValue('custpage_do');
	var cluster = nlapiGetFieldValue('custpage_custerno');
	nlapiLogExecution('ERROR', 'into Printreport ebizwaveno',ebizwaveno);
	var vScreenType,vScriptId,vDeployId,WavePDFURL;

	/*var ruleDet = IsCustomScreenRequired('PICKREPORTPRINT');
	if(ruleDet!=null && ruleDet!='')
	{
		vScreenType = ruleDet[0].getValue('custrecord_ebizrulevalue');
		vScriptId = ruleDet[0].getValue('custrecord_ebizrule_scriptid');
		vDeployId = ruleDet[0].getValue('custrecord_ebizrule_deployid');
	}
	if(vScreenType=='CUSTOM')
	{
		WavePDFURL = nlapiResolveURL('SUITELET', vScriptId, vDeployId);
	}*/
	//else
	//{
		WavePDFURL = nlapiResolveURL('SUITELET', 'customscript_wopickreportpdf', 'customdeploy_wopickreportpdf');
	//}
	//var WavePDFURL = nlapiResolveURL('SUITELET', 'customscript_pickreportpdfdynacraft', 'customdeploy_pickreportpdfdynacraft_di');//to test for url change for dyncraft pdf
	/*var ctx = nlapiGetContext();
	nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
	if (ctx.getEnvironment() == 'PRODUCTION') {
		WavePDFURL = 'https://system.netsuite.com' + WavePDFURL;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			WavePDFURL = 'https://system.sandbox.netsuite.com' + WavePDFURL;				
		}*/
	nlapiLogExecution('ERROR', 'Wave PDF URL',WavePDFURL);					
	//WavePDFURL = WavePDFURL + '&custparam_ebiz_wave_no='+ waveno+'&custparam_ebiz_fullfilmentno='+fullfillment;
	WavePDFURL = WavePDFURL + '&custparam_ebiz_wave_no='+ waveno+'&custparam_ebiz_clus_no='+cluster;
	nlapiLogExecution('ERROR', 'WavePDFURL',WavePDFURL);
//	alert(WavePDFURL);
	window.open(WavePDFURL);

}
function Printreport2(ebizwaveno){
	var fullfillment=null;
	//var WavePDFURL = nlapiResolveURL('SUITELET', 'customscript_pickreportpdf', 'customdeploy_pickreportpdf');
	
	
	var vCustomReportRule = getSystemRuleCurstomReport('PICKREPORTPRINT');

	var WavePDFURL;
	if(vCustomReportRule !=null && vCustomReportRule != '' && vCustomReportRule != 'Y')
	{
		var vScriptId=vCustomReportRule.getValue('custrecord_ebizrule_scriptid');
		var vDeploymentId=vCustomReportRule.getValue('custrecord_ebizrule_deployid');
		var vRuleValue=vCustomReportRule.getValue('custrecord_ebizrulevalue');
		if((vRuleValue == 'CUSTOM' || vRuleValue == 'STANDARD') && vScriptId != null && vScriptId != '' && vDeploymentId != null && vDeploymentId != '')
			WavePDFURL = nlapiResolveURL('SUITELET', vScriptId, vDeploymentId);
		else
			WavePDFURL = nlapiResolveURL('SUITELET', 'customscript_pickreportpdf', 'customdeploy_pickreportpdf');
	}	
	else
		WavePDFURL = nlapiResolveURL('SUITELET', 'customscript_pickreportpdf', 'customdeploy_pickreportpdf');
	
	/*var ctx = nlapiGetContext();
	if (ctx.getEnvironment() == 'PRODUCTION') {
		WavePDFURL = 'https://system.netsuite.com' + WavePDFURL;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			WavePDFURL = 'https://system.sandbox.netsuite.com' + WavePDFURL;				
		}*/
	//var linkURL = "https://system.netsuite.com/app/site/hosting/scriptlet.nl?script=141&deploy=1";	

	WavePDFURL = WavePDFURL + '&custparam_ebiz_wave_no='+ ebizwaveno+'&custparam_ebiz_fullfilmentno=';
	window.open(WavePDFURL);
}
function ispickgencompleted(ebizwaveno,fulfillOrderNo)
{
	nlapiLogExecution('ERROR', 'Into ispickgencompleted');
	nlapiLogExecution('ERROR', 'ebizwaveno',ebizwaveno);
	nlapiLogExecution('ERROR', 'fulfillOrderNo',fulfillOrderNo);

	var pickgenflag='T';

	var filters = new Array();

	if(ebizwaveno!=null && ebizwaveno!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_waveorder_waveno', null, 'is', ebizwaveno));

	if(fulfillOrderNo!=null && fulfillOrderNo!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_waveorder_orderno', null, 'is', fulfillOrderNo)); 

	filters.push(new nlobjSearchFilter('custrecord_ebiz_waveorder_pickgenflag', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_waveorder_orderno');

	var fulfillOrderList = nlapiSearchRecord('customrecord_ebiz_waveord', null, filters, columns);

	if(fulfillOrderList != null && fulfillOrderList != '' && fulfillOrderList.length > 0){
		pickgenflag='F';
	}

	nlapiLogExecution('ERROR', 'pickgenflag',pickgenflag);
	nlapiLogExecution('ERROR', 'Out of ispickgencompleted');
	return pickgenflag;
}
function fillfulfillorderField(form, shipmentField,maxno){
	nlapiLogExecution('ERROR', 'Into fillfulfillorderField');
	nlapiLogExecution('ERROR', 'maxno',maxno);

	shipmentField.addSelectOption("","");

	var filtersso = new Array();		
	filtersso[0] = new nlobjSearchFilter( 'custrecord_ord_qty', null, 'greaterthan', '0' );
	//15-Selected Into Wave, 25-Edit, 26-Picks Failed
	filtersso[1] = new nlobjSearchFilter( 'custrecord_linestatus_flag', null, 'anyof', ['8']);
	if(maxno!=-1)
	{
		filtersso.push(new nlobjSearchFilter('internalid', null, 'lessthan', parseFloat(maxno)));
	}

	filtersso.push(new nlobjSearchFilter('custrecord_shipment_no', null, 'isnotempty'));

	var columnsinvt = new Array();
	columnsinvt[0] = new nlobjSearchColumn('internalid');	 
	//columnsinvt[1] = new nlobjSearchColumn('custrecord_lineord');
	columnsinvt[0].setSort(true);
	columnsinvt[1] = new nlobjSearchColumn('custrecord_shipment_no');
	//columnsinvt[3] = new nlobjSearchColumn('custrecord_ebiz_wave');
	//columnsinvt[2].setSort(true);
	//columnsinvt[2] = new nlobjSearchColumn('custrecord_route_no');




	var customerSearchResults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filtersso,columnsinvt);


	for (var j = 0; customerSearchResults != null && j < customerSearchResults.length; j++) {

		//nlapiLogExecution('ERROR', 'tranid', customerSearchResults[i].getValue('tranid')); 
		if(customerSearchResults[j].getValue('custrecord_shipment_no') != null && customerSearchResults[j].getValue('custrecord_shipment_no') != "" && customerSearchResults[j].getValue('custrecord_shipment_no') != " ")
		{
			var resdo2 = form.getField('custpage_qbshipmentno').getSelectOptions(customerSearchResults[j].getValue('custrecord_shipment_no'), 'is');
			if (resdo2 != null) {
				if (resdo2.length > 0) {
					continue;
				}
			}
			shipmentField.addSelectOption(customerSearchResults[j].getValue('custrecord_shipment_no'), customerSearchResults[j].getValue('custrecord_shipment_no'));
		}

	}


	if(customerSearchResults!=null && customerSearchResults.length>=1000)
	{	 
		nlapiLogExecution('ERROR', 'customerSearchResults length',customerSearchResults.length);
		var maxno=customerSearchResults[customerSearchResults.length-1].getId();
		nlapiLogExecution('ERROR', 'maxno',maxno);
		fillfulfillorderField(form, shipmentField,maxno);	
	}
}

var itemDimensionList = new Array();
function getItemDimensions(skuList,maxno){
	nlapiLogExecution('ERROR','into  getItemDimensions', skuList.length);

	var itemDimCount = 0;

	if(skuList !=null && skuList !=''&& skuList.length>0)
	{
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', skuList));		// ALL SKUs IN SKU LIST
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));							// ACTIVE RECORDS ONLY

		if(maxno!=-1)
		{
			filters.push(new nlobjSearchFilter('id', null, 'lessthan', parseInt(maxno)));
		}

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');								// ITEM/SKU
		columns[1] = new nlobjSearchColumn('custrecord_ebizuomskudim');								// UOM
		columns[2] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim');						// UOM LEVEL
		columns[3] = new nlobjSearchColumn('custrecord_ebizqty');									// QUANTITY
		columns[4] = new nlobjSearchColumn('custrecord_ebizbaseuom');								// BASE UOM FLAG
		columns[5] = new nlobjSearchColumn('custrecord_ebizdims_packflag');							// PACK FLAG	
		columns[6] = new nlobjSearchColumn('custrecord_ebizweight');								// WEIGHT	
		columns[7] = new nlobjSearchColumn('custrecord_ebizcube');									// CUBE	
		columns[8] = new nlobjSearchColumn('internalid');
		columns[8].setSort(true);

		// SEARCH FROM ITEM DIMENSIONS
		var searchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

		if(searchResults != null && searchResults != '' && searchResults.length >= 1000){
			var maxno1=searchResults[searchResults.length-1].getValue(columns[8]);
			for(var i = 0; i < searchResults.length; i++){
				var sku = searchResults[i].getValue('custrecord_ebizitemdims');
				var uom = searchResults[i].getValue('custrecord_ebizuomskudim');
				var uomLevel = searchResults[i].getValue('custrecord_ebizuomlevelskudim');
				var qty = searchResults[i].getValue('custrecord_ebizqty');
				var baseUOMFlag = searchResults[i].getValue('custrecord_ebizbaseuom');
				var packflag = searchResults[i].getValue('custrecord_ebizdims_packflag');
				var weight = searchResults[i].getValue('custrecord_ebizweight');
				var cube = searchResults[i].getValue('custrecord_ebizcube');

				var skuDimRecord = [sku, uom, uomLevel, qty, baseUOMFlag, packflag,weight,cube];
				itemDimensionList[itemDimCount++] = skuDimRecord;
			}

			getItemDimensions(skuList,maxno1);
		}
		else
		{
			if(searchResults != null && searchResults != '' && searchResults.length >0)
			{
				for(var i = 0; i < searchResults.length; i++){
					var sku = searchResults[i].getValue('custrecord_ebizitemdims');
					var uom = searchResults[i].getValue('custrecord_ebizuomskudim');
					var uomLevel = searchResults[i].getValue('custrecord_ebizuomlevelskudim');
					var qty = searchResults[i].getValue('custrecord_ebizqty');
					var baseUOMFlag = searchResults[i].getValue('custrecord_ebizbaseuom');
					var packflag = searchResults[i].getValue('custrecord_ebizdims_packflag');
					var weight = searchResults[i].getValue('custrecord_ebizweight');
					var cube = searchResults[i].getValue('custrecord_ebizcube');

					var skuDimRecord = [sku, uom, uomLevel, qty, baseUOMFlag, packflag,weight,cube];
					itemDimensionList[itemDimCount++] = skuDimRecord;
				}
			}

		}
	}
	nlapiLogExecution('ERROR','out of  getItemDimensions', skuList.length);
	return itemDimensionList;
}
function getDistinctOrdersList(OrderList){
	nlapiLogExecution('ERROR', 'getDistinctOrdersList', 'Start');
	var OrderIDList = new Array();
	try{

		if(OrderList != null && OrderList.length > 0)
		{
			for(var i = 0; i < OrderList.length; i++)
			{
				var OrderID = OrderList[i].getValue('custrecord_ebiz_order_no');
				if(!isDistinctOrder(OrderIDList, OrderID))
					OrderIDList.push(OrderID);
			}
		}
		nlapiLogExecution('ERROR', 'getDistinctOrdersList', 'End');
	}

	catch(exception) 
	{
		nlapiLogExecution('ERROR', 'getDistinctOrdersList', exception);
	}
	nlapiLogExecution('ERROR', 'getDistinctOrdersList', 'End');
	return OrderIDList;

}
function isDistinctOrder(OrderIDList, OrderID)
{
	var matchFound = false;
	if(OrderIDList != null && OrderIDList.length > 0){
		for(var i = 0; i < OrderIDList.length; i++){
			if(OrderIDList[i] == OrderID)
				matchFound = true;
		}
	}

	return matchFound;
}
function GetOrderDetails(Orderlist,Trantype){
	nlapiLogExecution('ERROR', 'GetOrderDetails Orderlist', Orderlist);
	nlapiLogExecution('ERROR', 'GetOrderDetails Trantype', Trantype);

	var OrderDetailsListT = new Array();
	try{

		var OrderDetailsList = new Array();	
		nlapiLogExecution('ERROR', 'Orderlist', Orderlist);
		var filters = new Array();
		var columns = new Array();

		filters.push(new nlobjSearchFilter('internalid', null, 'anyof', Orderlist));	

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custbody_nswmspoinstructions');	
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_order_no');	

		var ResOrderDetails = nlapiSearchRecord(Trantype, null, filters, columns);

		if(ResOrderDetails != null && ResOrderDetails.length > 0){
			for(var i = 0; i < ResOrderDetails.length; i++ ){
				OrderDetailsList = new Array();
				OrderDetailsList[0] = ResOrderDetails[i].getValue('custbody_nswmspoinstructions');			
				OrderDetailsListT.push (OrderDetailsList);
			}
		}
	}catch(exception){
		nlapiLogExecution('ERROR', 'Get Order Details', exception);
	}

	nlapiLogExecution('ERROR', 'GetOrderDetails', 'end');
	nlapiLogExecution('ERROR', 'OrderDetailsListT', OrderDetailsListT);
	return OrderDetailsListT;
}
function GetIndividiualsplinstructDetails(searchrecordsplinstruct,ebizorderno){

	var individiualsplinstructDetailsListT = new Array();
	var individiualsplinstructDetailsList = new Array();
	nlapiLogExecution('ERROR', 'GetIndividiualsplinstructDetails', 'Start');
	nlapiLogExecution('ERROR', 'searchrecordsplinstruct', searchrecordsplinstruct);
	nlapiLogExecution('ERROR', 'ebizorderno', ebizorderno);
	try{

		if (searchrecordsplinstruct != null && searchrecordsplinstruct.length >0 && ebizorderno != null && ebizorderno.length>0){
			for ( var i = 0 ; i < searchrecordsplinstruct.length ; i++){				
				if (searchrecordsplinstruct[i].getValue('custrecord_ebiz_order_no')  == ebizorderno )
				{
					individiualsplinstructDetailsList = new Array();
					individiualsplinstructDetailsList[0] = searchrecordsplinstruct[i].getValue('custbody_nswmspoinstructions');

					individiualsplinstructDetailsListT.push(individiualsplinstructDetailsList);
				}
			}
		}
	}
	catch(exception){
		nlapiLogExecution('ERROR', 'individiualsplinstructDetailsListT', exception);
	}

	nlapiLogExecution('ERROR', 'GetIndividualOrderInfo', 'end');
	nlapiLogExecution('ERROR', 'Individual order info-individiualsplinstructDetailsListT', individiualsplinstructDetailsListT);
	return individiualsplinstructDetailsListT;
}
function getDistinctSKUsFromTaskList(searchresult){
	nlapiLogExecution('ERROR', 'getDistinctSKUsFromTaskList', 'Start');
	var skusList = new Array();
	try{	
		if(searchresult != null && searchresult.length > 0){
			for(var i = 0; i < searchresult.length; i++){
				var skuID = searchresult[i].getValue('custrecord_ebiz_sku_no');
				//nlapiLogExecution('ERROR', 'skuID', skuID);
				if(!isDistinctSKU(skusList, skuID))
					skusList.push(skuID);

			}
		}
	}
	catch(exception) 
	{
		nlapiLogExecution('ERROR', 'getDistinctSKUsFromTaskList', exception);
	}

	nlapiLogExecution('ERROR', 'getDistinctSKUsFromTaskList', 'End');
	nlapiLogExecution('ERROR', 'skulist', skusList);
	return skusList;
}
function isDistinctSKU(skuList, sku) {
	var matchFound = false;
	//nlapiLogExecution('ERROR', 'isDistinctSKU', 'start');
	try {

		if (skuList != null && skuList.length > 0) {
			for ( var i = 0; i < skuList.length; i++) {
				if (skuList[i] == sku)
					matchFound = true;
			}
		}
	} catch (exception) {
		nlapiLogExecution('ERROR', 'isDistinctSKU', exception);
	}
	//nlapiLogExecution('ERROR', 'isDistinctSKU', 'end');
	return matchFound;
}
function GetSKUDetails(skulist){
	nlapiLogExecution('ERROR', 'GetSKUDetails', 'Start');

	var skuDetailsListT = new Array();
	try{
		// Get the list of distinct SKUs
		var skuDetailsList = new Array();
		//var skulist = getDistinctSKUsFromTaskList(closedTaskList);
		nlapiLogExecution('ERROR', 'skulist', skulist);
		var filters = new Array();
		var columns = new Array();
		filters.push(new nlobjSearchFilter('internalid', null, 'anyof', skulist));	
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));		
		// Getting sku type is pending
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custitem_ebiz_forwardpick');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		var ResSKUDetails = nlapiSearchRecord('item', null, filters, columns);

		if(ResSKUDetails != null && ResSKUDetails.length > 0){
			for(var i = 0; i < ResSKUDetails.length; i++ ){
				skuDetailsList = new Array();
				skuDetailsList[0] = ResSKUDetails[i].getValue('custitem_ebiz_forwardpick');				
				skuDetailsListT.push (ResSKUDetails);
			}
		}
	}catch(exception){
		nlapiLogExecution('ERROR', 'Get SKU Details', exception);
	}

	nlapiLogExecution('ERROR', 'GetSKUDetails', 'end');
	nlapiLogExecution('ERROR', 'skuDetailsListT', skuDetailsListT);
	return skuDetailsListT;
}
function GetfardpickflagInfo(skulist, sku ){

	var IndividualSKUInfoList = new Array();

	var IndividualSKUInfoListT = new Array();
	nlapiLogExecution('ERROR', 'GetIndividualSKUInfo', 'Start');
	nlapiLogExecution('ERROR', 'skulist', skulist);
	nlapiLogExecution('ERROR', 'sku', sku);
	try{

		if (skulist != null && skulist.length >0 && sku != null && sku.length>0){
			for ( var i = 0 ; i < skulist.length ; i++){

				if (skulist[i].getValue('custrecord_ebiz_sku_no')  == sku )
				{
					IndividualSKUInfoList = new Array();
					//nlapiLogExecution('ERROR', 'sku matches..internal id is', skulist[i][7]);
					IndividualSKUInfoList[0] = skulist[i][0]; //['itemid'];					
					IndividualSKUInfoListT.push(IndividualSKUInfoList);
					i = skulist.length; 	
				}

			}
		}


	}
	catch(exception){
		nlapiLogExecution('ERROR', 'GetIndividualSKUInfo', exception);

	}

	nlapiLogExecution('ERROR', 'GetIndividualSKUInfo', 'end');
	nlapiLogExecution('ERROR', 'IndividualSKUInfoListT', IndividualSKUInfoListT);
	return IndividualSKUInfoListT;
}

function IsCustomScreenRequired(vScreen)
{
	var filters = new Array();
	var columns = new Array();
	filters.push(new nlobjSearchFilter('name', null, 'is', vScreen));	
	filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));	

	columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
	columns[1] = new nlobjSearchColumn('custrecord_ebizrule_scriptid');
	columns[2] = new nlobjSearchColumn('custrecord_ebizrule_deployid');

	var ruleDetails = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters, columns);

	return ruleDetails;

}
function getSystemRuleCurstomReport(RuleValue)
{

	try{
		var filters = new Array();

		filters[0] = new nlobjSearchFilter('name', null, 'is', RuleValue);
		//filters[1] = new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', ['@NONE@', Site]);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
		columns[1] = new nlobjSearchColumn('custrecord_ebizsite');
		columns[2] = new nlobjSearchColumn('custrecord_ebizrule_scriptid');
		columns[3] = new nlobjSearchColumn('custrecord_ebizrule_deployid');
		
		

		columns[1].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters, columns);	
		if(searchresults != null && searchresults != '')
		{
			if(searchresults[0].getValue('custrecord_ebizrulevalue') != null && searchresults[0].getValue('custrecord_ebizrulevalue') != '')
			{
				return searchresults[0];
			}
			else
				return 'Y';
		}
		else
			return 'Y';
	}
	catch (exp) 
	{
		nlapiLogExecution('Debug', 'Exception in GetSystemRules: ', exp);
		return 'Y';
	}	
}