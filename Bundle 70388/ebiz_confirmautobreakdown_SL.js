/***************************************************************************
                                                                 
                                                  eBizNET Solutions Inc 
****************************************************************************
*
*  $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inbound/Suitelet/ebiz_confirmautobreakdown_SL.js,v $
*  $Revision: 1.2.14.1 $
*  $Date: 2013/09/11 15:23:51 $
*  $Author: rmukkera $
*  $Name: t_NSWMS_2014_1_1_174 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_confirmautobreakdown_SL.js,v $
*  Revision 1.2.14.1  2013/09/11 15:23:51  rmukkera
*  Case# 20124376
*
*
****************************************************************************/
function ConfirmAutoBreakDown(request, response)
{
		var form = nlapiCreateForm('Confirm Autobreak down');
		var cnt= request.getParameter('custparam_cnt');
		nlapiLogExecution('ERROR','Value',cnt);
		response.writePage(form);
}