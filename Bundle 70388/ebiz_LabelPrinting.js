/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Common/Suitelet/ebiz_LabelPrinting.js,v $
 *     	   $Revision: 1.4.4.1.4.1.4.2.2.6 $
 *     	   $Date: 2015/11/30 23:52:43 $
 *     	   $Author: svanama $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_LabelPrinting.js,v $
 * Revision 1.4.4.1.4.1.4.2.2.6  2015/11/30 23:52:43  svanama
 * case 201415939
 *
 * Revision 1.4.4.1.4.1.4.2.2.5  2015/11/30 16:06:03  svanama
 * case #201415905
 *
 * Revision 1.4.4.1.4.1.4.2.2.4  2015/11/27 11:47:39  svanama
 * Case # 201414979 and  201415291 Printing Pick Label and Packlabel from Bartender
 *
 * Revision 1.4.4.1.4.1.4.2.2.3  2015/11/19 15:43:38  rmalraj
 * 201413908 Need UCC labels generation functionality for 2015.2 account(Bartender and Zebra)
 *
 * Revision 1.4.4.1.4.1.4.2.2.2  2015/11/14 17:57:24  svanama
 * Case # 201413908 and Case # 201413908
 *
 * Revision 1.4.4.1.4.1.4.2.2.1  2015/11/12 16:45:20  rmalraj
 * case # 201413329,After doing packing through UI, for FIMS shipping method system is not displaying order details in external label details SO#SLS00000950
 *
 * Revision 1.4.4.1.4.1.4.2  2015/06/16 14:42:11  rmalraj
 * 201413139
 * FIMS (FedEx International Mail Service) label generation functionality , when carrier id is FIMS , FIMS label functionality is invoked for KRM
 *
 * Revision 1.4.4.1.4.1.4.1  2013/03/19 11:46:50  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.4.4.1.4.1  2012/12/11 11:47:21  svanama
 * CASE201112/CR201113/LOG201121
 * Code Changes in LabelPrinting for Shiplabel and Recevielabel generation
 *
 * Revision 1.4.4.1  2012/04/30 10:30:42  spendyala
 * CASE201112/CR201113/LOG201121
 * While Searching of Item in ItemMaster,
 * 'name' filter is changed to 'nameinternal' and also checking weather the item is inactive state or not.
 *
 * Revision 1.4  2011/10/03 08:12:38  skdokka
 * CASE201112/CR201113/LOG201121
 * Fetching zip code from shipzip filed of sales order.
 *
 * Revision 1.3  2011/09/30 12:06:18  skdokka
 * CASE201112/CR201113/LOG201121
 * Made changes to pick and ship label formats
 *
 * Revision 1.2  2011/09/29 12:19:13  rmukkera
 * CASE201112/CR201113/LOG201121
 * semicolon added
 *
 * Revision 1.1  2011/09/29 12:18:20  rmukkera
 * CASE201112/CR201113/LOG201121
 * new file for label printing
 ****************************************************************************/

function CreateLabelData(labeldata,labeltype,tasktype,refno,print,reprint,company,location,name)
{

	var labelrecord = nlapiCreateRecord('customrecord_labelprinting'); 
	labelrecord.setFieldValue('name', name); 
	labelrecord.setFieldValue('custrecord_labeldata',labeldata);  
	labelrecord.setFieldValue('custrecord_label_refno',refno);     
	labelrecord.setFieldValue('custrecord_label_tasktype',tasktype);//chkn task   
	labelrecord.setFieldValue('custrecord_labeltype',labeltype);                                                                                                                                                                     labelrecord.setFieldValue('custrecord_labeldata', labeldata);
	labelrecord.setFieldValue('custrecord_label_print', print);
	labelrecord.setFieldValue('custrecord_label_reprint', reprint);
	labelrecord.setFieldValue('custrecord_label_company', company);
	labelrecord.setFieldValue('custrecord_label_location', location);
	labelrecord.setFieldValue('custrecord_label_lp', name);
	var tranid = nlapiSubmitRecord(labelrecord);

	nlapiLogExecution('ERROR', 'tranid', tranid);
}

function generateShipLabel(vcontainerlp,vebizOrdNo,internalid)
{	
	nlapiLogExecution('ERROR', 'internalid', internalid);
	nlapiLogExecution('ERROR', 'vebizOrdNo', vebizOrdNo);
	nlapiLogExecution('ERROR', 'vcontainerlp', vcontainerlp);
	var salesorderlist= SalesOrderList(vebizOrdNo);
	var labeldata;
	var shipCarrierType,carrierName,carrierid;
	if(salesorderlist != null && salesorderlist.length > 0)
	{

		var companyname,customerpo,location;
		var shiptoAddress1,shiptoAddress2,shiptocity,shiptostae,shiptocountry,shiptocompany,shiptozipcode;
		var waveno,cluster,orderno,fulfillord;
		var wmsCarrierid,salesOrderRoute,shipcarrier,wmsCarrierNameId;
		var companyid=salesorderlist[0].getValue('custbody_nswms_company');
		companyname=salesorderlist[0].getText('custbody_nswms_company');

		shiptoAddress1=salesorderlist[0].getValue('shipaddress1');
		shiptoAddress2=salesorderlist[0].getValue('shipaddress2');
		shiptocity=salesorderlist[0].getValue('shipcity');
		shiptostae=salesorderlist[0].getValue('shipstate');
		shiptocountry=salesorderlist[0].getValue('shipcountry');
		shiptocompany=salesorderlist[0].getText('entity');
		orderno=salesorderlist[0].getValue('tranid');
		shiptozipcode=salesorderlist[0].getValue('shipzip');
		location=salesorderlist[0].getValue('location');
		nlapiLogExecution('ERROR', 'shiptoAddress1', location);
		wmsCarrierid=salesorderlist[0].getText('custbody_salesorder_carrier');
		wmsCarrierNameId=salesorderlist[0].getValue('custbody_salesorder_carrier');
		nlapiLogExecution('ERROR', 'wmsCarrierid', wmsCarrierid);
		var filterCarrier = new Array();
		filterCarrier .push(new nlobjSearchFilter('name', null,'is', wmsCarrierid));
		var columnCarrier = new Array();
		columnCarrier [0] = new nlobjSearchColumn('custrecord_carrier_type');
		columnCarrier [1] = new nlobjSearchColumn('custrecord_carrier_name');

		var carrierTypeSearchrecord= nlapiSearchRecord('customrecord_ebiznet_carrier', null, filterCarrier , columnCarrier);
		if(carrierTypeSearchrecord!=null)
		{
			shipCarrierType=carrierTypeSearchrecord[0].getText('custrecord_carrier_type');
			carrierName=carrierTypeSearchrecord[0].getValue('custrecord_carrier_name');

		}
		else
		{
			shipCarrierType="";
			carrierName="BESTWAY";
		}
		salesOrderRoute=salesorderlist[0].getText('custbody_nswmssoroute');
		var opentaskordersearchresult =GetopenTaskDetails(vcontainerlp,vebizOrdNo,internalid);
		if(opentaskordersearchresult !=null)
		{	
			fulfillord=opentaskordersearchresult[0].getValue('name');
			waveno=opentaskordersearchresult[0].getValue('custrecord_ebiz_wave_no');         
			cluster=opentaskordersearchresult[0].getValue('custrecord_ebiz_clus_no');
		}
		nlapiLogExecution('ERROR', 'shipCarrierType', wmsCarrierNameId);
		nlapiLogExecution('ERROR', 'shipCarrierType', carrierName);
		nlapiLogExecution('ERROR', 'shipCarrierType', wmsCarrierid);
		nlapiLogExecution('ERROR', 'shipCarrierType', shipCarrierType);
		nlapiLogExecution('ERROR', 'salesOrderRoute', salesOrderRoute);
		nlapiLogExecution('ERROR', 'fullfillmentNumber', fulfillord);
		nlapiLogExecution('ERROR', 'waveno', waveno);
		nlapiLogExecution('ERROR', 'orderno', orderno);

		var Label ="";

		Label += "~CC^";
		Label += "^XA^MCY^XZ^XA^FWN^FS^PW832^ML0";
		Label += "^FO0040,0030^A0,30^FDWave#" +waveno+"^FS";
		Label += "^FO0040,0060^BY3,3,150^B3N,N,0090,N,N^FD" +waveno+"^FS";
		Label += "^FO0040,0160^A0,30^FDShip to:^FS";
		Label += "^FO0140,0160^A0,30^FD"+shiptocompany+"^FS";
		Label += "^FO0140,0190^A0,30^FD"+shiptoAddress1+" " +shiptoAddress2+"^FS";
		Label += "^FO0140,0220^A0,30^FD"+shiptocity+" "+shiptostae+" "+shiptozipcode+" "+shiptocountry+"^FS";
		// Label += " ^FO0140,0250^A0,30^FD^FS";
		if(cluster!="" && cluster!=null)
		{
			Label += "^FO0040,0250^A0,30^FDCluster#"+cluster+"^FS";
			Label += "^FO0040,0280^BY3,3,150^B3N,N,0090,N,N^FD" +cluster+"^FS";
		}
		Label += "^FO0740,0030^A0R,45^FD"+shipCarrierType+"^FS";
		Label += "^FO0705,0030^A0R,35^FDRoute: "+salesOrderRoute+"^FS";
		Label += "^FO0670,0030^A0R,35^FDShip By: "+carrierName+"^FS";
		Label += "^FO0040,0340^A0,30^FDDelivery: "+fulfillord+"^FS";
		Label += "^FO0040,0380^BY3,3,150^B3N,N,0090,N,N^FD"+fulfillord+"^FS";
		Label += "^FO0040,0470^A0,15 ";
		//Label += " ^FDLLOPEZ 09/27/2011 16:52:21 NUP300^FS";
		Label += "^PQ01^XZ";

		var tasktype=4;
		var labeltype="SHIP";
		var print ="F";
		var reprint="F";
		CreateLabelData(Label,labeltype,tasktype,orderno,print,reprint,companyid,location,vcontainerlp);

	}
}






function generatePickLabel(vcontainerlp,vebizOrdNo,internalid)
{
	try{
		nlapiLogExecution('ERROR', 'internalid', internalid);
		nlapiLogExecution('ERROR', 'vebizOrdNo', vebizOrdNo);
		nlapiLogExecution('ERROR', 'vcontainerlp', vcontainerlp);
		var salesorderlist= SalesOrderList(vebizOrdNo);
		var labeldata;var actqty;
		if(salesorderlist != null && salesorderlist.length > 0)
		{
			var linenumber;
			var location;
			var companyname,customerpo,uom;

			var shiptoAddress1,shiptoAddress2,shiptocity,shiptostate,shiptocountry,shiptocompany,shiptozipcode;
			customerpo=salesorderlist[0].getValue('otherrefnum'); 

			var companyid=salesorderlist[0].getValue('custbody_nswms_company');
			//companyname=salesorderlist[0].getText('custbody_nswms_company');

			nlapiLogExecution('ERROR', 'companyname', companyid);

			shiptoAddress1=salesorderlist[0].getValue('shipaddress1');
			shiptoAddress2=salesorderlist[0].getValue('shipaddress2');
			shiptocity=salesorderlist[0].getValue('shipcity');
			shiptostate=salesorderlist[0].getValue('shipstate');
			shiptocountry=salesorderlist[0].getValue('shipcountry');
			shiptocompany=salesorderlist[0].getText('entity');
			shiptozipcode=salesorderlist[0].getValue('shipzip');
			location=salesorderlist[0].getValue('location');
			orderno=salesorderlist[0].getValue('tranid');


			//Search for Location address
			var shipfromcity,shipfromcountry,shipfromzipcode,shipfromaddress,shipfromphone,shipfromstate;
			//Search for ShipFromAdress  from Locations 
			if(location !="" && location !=null)
			{
				nlapiLogExecution('ERROR', 'location', location);

				var record = nlapiLoadRecord('location', location);
				nlapiLogExecution('ERROR', 'location', location);
				shipfromaddress=record.getFieldValue('addr1');
				var addr2=record.getFieldValue('addr2');
				shipfromaddress=shipfromaddress+" " + addr2;
				shipfromcity=record.getFieldValue('city');
				shipfromstate=record.getFieldValue('state');
				shipfromzipcode =record.getFieldValue('zip');
				companyname=record.getFieldValue('addressee');
				shipfromphone=record.getFieldValue('addrphone');
				shipfromcountry =record.getFieldValue('country');



			}


			//Search from openTaskDetails

			var skuname, skuno, actbeginloc,name,waveno,orderno,skudesc;
			var binlocation,binlocationid;
			var opentaskordersearchresult =GetopenTaskDetails(vcontainerlp,vebizOrdNo,internalid);
			if(opentaskordersearchresult !=null)
			{    

				uom=opentaskordersearchresult[0].getValue('custrecord_uom_id');
				skuno=opentaskordersearchresult[0].getValue('custrecord_sku');
				actqty=opentaskordersearchresult[0].getValue('custrecord_expe_qty');
				var filtersku = new Array();
				filtersku.push(new nlobjSearchFilter('internalid', null,'is', skuno));
				//Changed On 30/4/12 by suman
				filtersku.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
				//End of changes as on 30/4/12.
				var columnssku = new nlobjSearchColumn('custitem_ebizdescriptionitems');
				var searchdesc= nlapiSearchRecord('item', null, filtersku,columnssku);
				if(searchdesc!=null)
				{
					skudesc=searchdesc[0].getValue('custitem_ebizdescriptionitems');
					skuname=opentaskordersearchresult [0].getText('custrecord_sku');
				}
				actbeginloc=opentaskordersearchresult[0].getText('custrecord_actbeginloc');
				var actbeginlocid=opentaskordersearchresult[0].getValue('custrecord_actbeginloc');
				var filterlocation = new Array();
				filterlocation .push(new nlobjSearchFilter('internalid', null,'is', actbeginlocid));
				var columnlocation = new Array();
				columnlocation [0] = new nlobjSearchColumn('custrecord_outboundlocgroupid');
				var binlocationSearch= nlapiSearchRecord('customrecord_ebiznet_location', null, filterlocation , columnlocation );
				if(binlocationSearch!=null)
				{
					binlocation=binlocationSearch[0].getText('custrecord_outboundlocgroupid');
					binlocationid=binlocationSearch[0].getValue('custrecord_outboundlocgroupid');
				}	
				linenumber=opentaskordersearchresult[0].getValue('custrecord_line_no');

				//fullfillment order 
				name=opentaskordersearchresult[0].getValue('name'); 
				waveno=opentaskordersearchresult[0].getValue('custrecord_ebiz_wave_no');         

			}
			nlapiLogExecution('ERROR', 'binlocation+binlocationid', binlocation+binlocationid);
			nlapiLogExecution('ERROR', 'name', name);
			nlapiLogExecution('ERROR', 'skuno ', skuno );
			nlapiLogExecution('ERROR', 'actbeginloc', actbeginloc);
			nlapiLogExecution('ERROR', 'waveno', waveno);
			nlapiLogExecution('ERROR', 'skuname', skuname);
			nlapiLogExecution('ERROR', 'orderno', orderno);
			nlapiLogExecution('ERROR', 'qty', actqty);
			nlapiLogExecution('ERROR', 'uom', uom);

			var Label="";


			Label += "~CC^ ";
			Label += "^XA^MCY^XZ^XA^FWN^FS^PW832^ML0";
			Label += "^FO0030,0030^GB760,430,4^FS";
			Label += "^FO0030,0140^GB760,130,2^FS";
			Label += "^FO0380,0030^GB001,130,2^FS";
			Label += "^FO0030,0140^GB030,130,2^FS";
			Label += "^FO0380,0140^GB030,130,2^FS";
			Label += " ^FO0030,0300^GB760,001,2^FS";
			Label += " ^FO0380,0270^GB001,030,2^FS";
			Label += " ^FO0040,0040^A0,45";
			Label += " ^FDWave/Line:^FS";
			Label += " ^FO0040,0090^A0,45";
			Label += "^FD"+waveno+"/"+linenumber+"^FS";
			Label += "^FO0390,0040^A0,30^FDBinLoc: "+binlocation+"/"+actbeginloc+"^FS";
			Label += "^FO0390,0090^A0,30^FDDO: "+name+"^FS";
			Label += "^FO0040,0165^A0B,20^FDShip from^FS";
			Label += " ^FO0075,0145^A0,20^FD"+companyname+"^FS";
			Label += " ^FO0075,0170^A0,20^FD"+shipfromaddress+"^FS";
			Label += "^FO0075,0195^A0,20^FD"+shipfromcity+" "+shipfromstate+" "+shipfromzipcode+" "+shipfromcountry+"^FS";
			Label += "^FO0075,0220^A0,20^FD^FS";
			Label += "^FO0075,0245^A0,20^FD^FS";
			Label += " ^FO0385,0175,^A0B,20^FDShip to^FS";
			Label += "^FO0420,0145^A0,20^FD"+shiptocompany+"^FS";
			Label += "^FO0420,0170^A0,20^FD"+shiptoAddress1+""+shiptoAddress2+"^FS";
			Label += "^FO0420,0195^A0,20^FD"+shiptocity+" "+shiptostate+" "+shiptozipcode+" "+shiptocountry+"^FS";
			Label += " ^FO0420,0220^A0,20^FD^FS";
			Label += " ^FO0420,0245^A0,20^FD^FS";
			Label += " **Order/Purchase Order";
			Label += " ^FO0040,0275^A0,25^FDOrder: "+orderno+"^FS";
			Label += " ^FO0390,0275^A0,25^FDPO: "+customerpo+"^FS";
			Label += " ^FO0040,0310^A0,30^FD"+skuname+"^FS";
			Label += "^FO0040,0360^A0,30^FD"+skudesc+"^FS";
			Label += "^FO0040,0415^A0,25";
			Label += " ^FD Qty: "+actqty+" "+uom+"^FS";
			Label += " ^FO0040,0470^A0,15";
			//Label += "^FDLLOPEZ 09/27/2011 16:52:21 NUP300^FS";
			Label += "^PQ01^XZ";


			var tasktype=3;
			var labeltype="PICK";
			var print ="F";
			var reprint="F";
			CreateLabelData(Label,labeltype,tasktype,orderno,print,reprint,companyid,location,vcontainerlp);

		}

	}
	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in GeneratePickLabel (salesorder) : ', exp);		
	}
}

function SalesOrderList(vebizOrdNo) {
	nlapiLogExecution('ERROR', 'vebizOrdNo', vebizOrdNo);
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('Internalid', null, 'is', vebizOrdNo);
	filters[1] = new nlobjSearchFilter('mainline', null, 'is', 'T');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('otherrefnum');
	columns[1] = new nlobjSearchColumn('custbody_nswms_company');
	columns[2] = new nlobjSearchColumn('shipaddress1');
	columns[3] = new nlobjSearchColumn('shipaddress2');
	columns[4] = new nlobjSearchColumn('shipcity');
	columns[5] = new nlobjSearchColumn('shipstate');
	columns[6] = new nlobjSearchColumn('shipcountry');
	columns[7] = new nlobjSearchColumn('entity');
	columns[8] = new nlobjSearchColumn('shipzip');
	columns[9] = new nlobjSearchColumn('custbody_salesorder_carrier');
	columns[10] = new nlobjSearchColumn('custbody_nswmssoroute');
	columns[11] = new nlobjSearchColumn('location');
	columns[12] =new nlobjSearchColumn('tranid');
	columns[13] =new nlobjSearchColumn('shipattention');
	var trantype = nlapiLookupField('transaction', vebizOrdNo, 'recordType');
	nlapiLogExecution('ERROR', 'trantype', trantype);
	var salesorderlist = nlapiSearchRecord(trantype, null, filters, columns);
	return salesorderlist ;
}
function GetCompanyName(companyid)
{
	var filtername = new Array();
	filtername [0] = new nlobjSearchFilter('Internalid', null, 'is', companyid);
	var columnname= new Array();
	columnname[0] = new nlobjSearchColumn('custrecord_compcity');
	columnname[1] = new nlobjSearchColumn('custrecord_compcountry');
	columnname[2] = new nlobjSearchColumn('custrecord_compstate');
	columnname[3] = new nlobjSearchColumn('custrecord_compzipcode');
	columnname[4] = new nlobjSearchColumn('custrecord_compaddr');
	columnname[5] = new nlobjSearchColumn('custrecord_compphone1');

	var companylist= nlapiSearchRecord('customrecord_ebiznet_company', null, filtername , columnname);
	return companylist;
}
function createShiplabel(salesorderId)
{
	nlapiLogExecution('ERROR', 'createShiplabel', 'createShiplabel');

	var salesorderlist= SalesOrderList(salesorderId);
	try
	{
		if(salesorderlist != null)
		{
			var shiptoAddress1,shiptoAddress2,shiptocity,shiptostate,shiptocountry,shiptocompany,shiptozipcode;

			var companyid=salesorderlist[0].getValue('custbody_nswms_company');
			var companyname=salesorderlist[0].getText('custbody_nswms_company');
			nlapiLogExecution('ERROR', 'companyname', companyid);
			shiptoAddress1=salesorderlist[0].getValue('shipaddress1');
			shiptoAddress2=salesorderlist[0].getValue('shipaddress2');
			shiptocity=salesorderlist[0].getValue('shipcity');
			shiptostate=salesorderlist[0].getValue('shipstate');
			shiptocountry=salesorderlist[0].getValue('shipcountry');
			shiptocompany=salesorderlist[0].getText('entity');
			shiptozipcode=salesorderlist[0].getValue('shipzip');
			var location=salesorderlist[0].getValue('location');
			var locationtext=salesorderlist[0].getText('location');
			orderno=salesorderlist[0].getValue('tranid');
			var attention=salesorderlist[0].getValue('shipattention');
			//Search for Location address
			var shipfromcity,shipfromcountry,shipfromzipcode,shipfromaddress,shipfromphone,shipfromstate;
			//Search for ShipFromAdress  from Locations 
			var trantype = nlapiLookupField('transaction', salesorderId, 'recordType');
			nlapiLogExecution('ERROR', 'trantype', trantype);
			var salesorderrecord=nlapiLoadRecord(trantype,salesorderId);

			var location=salesorderrecord.getFieldValue('location');
			if((location !="") && (location !=null))
			{

				var record = nlapiLoadRecord('location', location);
				shipfromaddress=record.getFieldValue('addr1');

				if((shipfromaddress==null)||(shipfromaddress==''))
				{
					shipfromaddress='';
				}
				var addr2=record.getFieldValue('addr2');

				if((addr2==null)||(addr2==''))
				{
					addr2='';
				}
				shipfromaddress=shipfromaddress+" " + addr2;



				shipfromcity=record.getFieldValue('city');
				if((shipfromcity==null)||(shipfromcity))
				{
					shipfromcity='';
				}

				shipfromstate=record.getFieldValue('state');
				if((shipfromstate==null)||(shipfromstate==''))
				{
					shipfromstate='';
				}

				shipfromzipcode =record.getFieldValue('zip');
				if((shipfromzipcode==null)||(shipfromzipcode==''))
				{
					shipfromzipcode='';
				}

				companyname=record.getFieldValue('addressee');
				if((companyname==null)||(companyname==''))
				{
					companyname='';
				}

				shipfromphone=record.getFieldValue('addrphone');
				if((shipfromphone==null)||(shipfromphone==''))
				{
					shipfromphone='';
				}

				shipfromcountry =record.getFieldValue('country');
				if((shipfromcountry==null)||(shipfromcountry==''))
				{
					shipfromcountry='';
				}
			}
			var labeltype="SHIPLABEL";
			var extlabelrecord = nlapiCreateRecord('customrecord_ext_labelprinting'); 
			extlabelrecord.setFieldValue('custrecord_label_ebizorder',salesorderId);
			extlabelrecord.setFieldValue('custrecord_label_order',orderno); 

			extlabelrecord.setFieldValue('custrecord_label_addr1',shipfromaddress); 
			extlabelrecord.setFieldValue('custrecord_label_state',shipfromstate);
			extlabelrecord.setFieldValue('custrecord_label_zip',shipfromzipcode);
			extlabelrecord.setFieldValue('custrecord_label_city',shipfromcity);
			extlabelrecord.setFieldValue('custrecord_label_labeltype',labeltype);

			extlabelrecord.setFieldValue('name',orderno);  
			//Ship to Adress
			extlabelrecord.setFieldValue('custrecord_label_shipaddressee',shiptocompany); 
			extlabelrecord.setFieldValue('custrecord_label_shipaddr1',shiptoAddress1); 
			extlabelrecord.setFieldValue('custrecord_label_shipaddr2',shiptoAddress2); 

			extlabelrecord.setFieldValue('custrecord_label_shipcity',shiptocity); 
			extlabelrecord.setFieldValue('custrecord_label_shipstate',shiptostate); 
			extlabelrecord.setFieldValue('custrecord_label_shipcountry',shiptocountry); 
			extlabelrecord.setFieldValue('custrecord_label_shipzip',shiptozipcode); 
			extlabelrecord.setFieldValue('custrecord_label_shipattention',attention); 

			var tranid = nlapiSubmitRecord(extlabelrecord);

		}

	}
	catch(exp)
	{
		nlapiLogExecution('ERROR', 'createshiplabelexceptoin', exp.toString());
	}
}

function generateReceiveLabel(ItemId,vsalesOrderId,ebizPalleteno)
{
	nlapiLogExecution('ERROR', 'generateReceiveLabel', 'generateReceiveLabel');
	var chklabelarray=new Array();
	var opentaskChknrecords=getopentaskchkndetails(ItemId,vsalesOrderId);
	var itemstaus=opentaskChknrecords[0].getText('custrecord_sku_status');
	var batchnumber=opentaskChknrecords[0].getValue('custrecord_batch_no');
	var begindate=opentaskChknrecords[0].getValue('custrecordact_begin_date');
	var wmslocation=opentaskChknrecords[0].getValue('custrecord_wms_location');
	var expdate=opentaskChknrecords[0].getValue('custrecord_expirydate');
	//var ebizpaltno=opentaskChknrecords[0].getValue('custrecord_ebiz_lpno');
	var ebizpaltno=ebizPalleteno;
	var ebizsku=opentaskChknrecords[0].getText('custrecord_sku');
	var itemdesc=opentaskChknrecords[0].getValue('custrecord_skudesc');
	var ebizorder=opentaskChknrecords[0].getText('custrecord_ebiz_order_no');
	ebizorder=ebizorder.replace(/\s/g, "");
	var userId;
	userId = nlapiGetUser();
	var transaction = nlapiLoadRecord('Employee', userId);
	var variable=transaction.getFieldValue('email');
	var username=variable.split('@')[0]; 
	chklabelarray["itemstaus"]=itemstaus;
	chklabelarray["ebizsku"]=ebizsku;
	chklabelarray["ebizpaltno"]=ebizpaltno;
	chklabelarray["begindate"]=begindate;
	chklabelarray["batchnumber"]=batchnumber;
	chklabelarray["itemdesc"]=itemdesc;
	chklabelarray["user"]=username;
	chklabelarray["ebizorder"]=ebizorder;
	chklabelarray["expdate"]=expdate;
	var shiptotaladress;
	var shipfromaddress;
	if(wmslocation !="" && wmslocation !=null)
	{
		nlapiLogExecution('ERROR', 'location', wmslocation);

		var record = nlapiLoadRecord('location', wmslocation);

		var addr1=record.getFieldValue('addr1');
		var addr2=record.getFieldValue('addr2');
		shipfromaddress=addr1+" " + addr2;
		shipfromcity=record.getFieldValue('city');
		shipfromstate=record.getFieldValue('state');
		shipfromzipcode =record.getFieldValue('zip');
		companyname=record.getFieldValue('addressee');
		shipfromphone=record.getFieldValue('addrphone');
		shipfromcountry =record.getFieldValue('country');
		chklabelarray["companyname"]=companyname;
		chklabelarray["shipfromaddress"]=shipfromaddress;
		chklabelarray["shipfromcity"]=shipfromcity;
		chklabelarray["shipfromstate"]=shipfromstate;
		chklabelarray["shipfromcountry"]=shipfromcountry;
		chklabelarray["shipfromzipcode"]=shipfromzipcode;
	}
	chklabelarray["labeltype"]="PALLETLABEL";
	nlapiLogExecution('ERROR','companyname',companyname);
	nlapiLogExecution('ERROR','shipfromaddress',shipfromaddress);
	nlapiLogExecution('ERROR','shipfromcity',shipfromcity);
	nlapiLogExecution('ERROR','shipfromstate',shipfromstate);
	nlapiLogExecution('ERROR','shipfromcountry',shipfromcountry);
	nlapiLogExecution('ERROR','shipfromzipcode',shipfromzipcode);

	shiptotaladress=companyname+","+shipfromaddress+","+shipfromcity+","+shipfromstate+","+shipfromcountry+","+shipfromzipcode;

	CreatePackLabelDetails(chklabelarray,shiptotaladress,vsalesOrderId);

}

function CreatePackLabelDetails(chklabelarray,shiptotaladress,vsalesOrderId)
{
	var packlabelrecord = nlapiCreateRecord('customrecord_ext_labelprinting'); 
	packlabelrecord.setFieldValue('name',chklabelarray["ebizorder"]);  
	packlabelrecord.setFieldValue('custrecord_label_item',chklabelarray["ebizsku"]);  
	packlabelrecord.setFieldValue('custrecord_label_itemdesc',chklabelarray["itemdesc"]);  
	packlabelrecord.setFieldValue('custrecord_label_licenseplatenumber',chklabelarray["ebizpaltno"]);  
	packlabelrecord.setFieldValue('custrecord_label_receivedate',chklabelarray["begindate"]);  
	packlabelrecord.setFieldValue('custrecord_label_lotnumber',chklabelarray["batchnumber"]);  
	packlabelrecord.setFieldValue('custrecord_label_itempulldate',chklabelarray["expdate"]);  
	packlabelrecord.setFieldValue('custrecord_label_user',chklabelarray["user"]);  
	packlabelrecord.setFieldValue('custrecord_label_itemstatus',chklabelarray["itemstaus"]); 
	packlabelrecord.setFieldValue('custrecord_label_shipfromtotaladress',shiptotaladress);
	packlabelrecord.setFieldValue('custrecord_label_ebizorder',vsalesOrderId);
	packlabelrecord.setFieldValue('custrecord_label_order',chklabelarray["ebizorder"]); 
	packlabelrecord.setFieldValue('custrecord_label_shipfromaddressee',chklabelarray["companyname"]); 
	packlabelrecord.setFieldValue('custrecord_label_addr1',chklabelarray["shipfromaddress"]); 
	packlabelrecord.setFieldValue('custrecord_label_state',chklabelarray["shipfromstate"]);
	packlabelrecord.setFieldValue('custrecord_label_zip',chklabelarray["shipfromzipcode"]);
	packlabelrecord.setFieldValue('custrecord_label_city',chklabelarray["shipfromcity"]);
	packlabelrecord.setFieldValue('custrecord_label_labeltype',chklabelarray["labeltype"]);

	var tranid = nlapiSubmitRecord(packlabelrecord);
}

function getopentaskchkndetails(itemid,vsalesOrderId)
{
	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [1]));//Task Type - CHKN
	filter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [1]));
	filter.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', [itemid]));
	filter.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', [vsalesOrderId]));
	var columns = new Array();
	columns[0]=new nlobjSearchColumn('custrecord_batch_no');
	columns[1]=new nlobjSearchColumn('custrecord_sku_status');
	columns[2]=new nlobjSearchColumn('custrecordact_begin_date');
	columns[3]=new nlobjSearchColumn('custrecord_wms_location');
	columns[4]=new nlobjSearchColumn('custrecord_ebiz_lpno');
	columns[5]=new nlobjSearchColumn('custrecord_sku');
	columns[6]=new nlobjSearchColumn('custrecord_skudesc');
	columns[7]=new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[8]=new nlobjSearchColumn('custrecord_expirydate');
	var opentaskChknrecords = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filter,columns);

	return opentaskChknrecords;
}

function GetopenTaskDetails(containerlp,vebozOrdNo,internalid)
{
	nlapiLogExecution('ERROR', 'General Functions', 'In to getOpenTaskDetails');
	var filter = new Array();
	// filter.push(new nlobjSearchFilter('custrecord_act_end_date', null,'isempty', '@NONE@'));
	filter.push(new nlobjSearchFilter('internalid', null,'is', internalid));
	filter.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'is', vebozOrdNo ));
	filter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK
	filter.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'is', containerlp));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[1] = new nlobjSearchColumn('custrecord_sku');
	columns[2] = new nlobjSearchColumn('custrecord_line_no');
	columns[3] = new nlobjSearchColumn('custrecord_actbeginloc');
//	Full fill ment order number        
	columns[4] = new nlobjSearchColumn('name');
//	Wave number
	columns[5] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
	columns[6] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[7] = new nlobjSearchColumn('custrecord_act_qty');
	columns[8] = new nlobjSearchColumn('custrecord_uom_id');
	columns[9] = new nlobjSearchColumn('custrecord_ebiz_clus_no');

	var opentaskordersearchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filter, columns);
	return opentaskordersearchresult;
	
}
//code for GenerateFIMS Label
function GenerateFimsLabelContainerLevel(vebizOrdNo,fulfillmentnumber)
{
	
	nlapiLogExecution('Debug', 'vebizOrdNo',vebizOrdNo);
	nlapiLogExecution('Debug', 'fulfillmentnumber',fulfillmentnumber);
	
	var opentaskfilterarray= new Array();
	opentaskfilterarray.push( new nlobjSearchFilter('name', null, 'is', fulfillmentnumber));
	opentaskfilterarray.push( new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof',vebizOrdNo));
	opentaskfilterarray.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
	opentaskfilterarray.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [28]));
	var opentaskcolumnarray = new Array();        
	opentaskcolumnarray[0]=new nlobjSearchColumn('custrecord_container_lp_no',null,'group');
	opentaskcolumnarray[1]=new nlobjSearchColumn('formulanumeric',null,'SUM');
	opentaskcolumnarray[1].setFormula("TO_NUMBER({custrecord_act_qty})");		
	var opentasksearchresults= nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, opentaskfilterarray, opentaskcolumnarray);
	
	if(opentasksearchresults!=null)
	{
		for(var x=0;x<opentasksearchresults.length;x++)
		{
			nlapiLogExecution('Debug', 'Into opentasksearchresults Loop', opentasksearchresults.length);
			var vebizcontlp=opentasksearchresults[x].getValue('custrecord_container_lp_no', null, 'group');
            //container level FIMS Label
			GenerateFimsLabel(vebizOrdNo,vebizcontlp,fulfillmentnumber);

		}
	}

	
}

function getItemRecord(cartonlp)
{

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is',cartonlp));

	var columns = new Array();

	columns[0] = new nlobjSearchColumn('custrecord_sku');
	columns[1] = new nlobjSearchColumn('custrecord_expe_qty');
    columns[2] = new nlobjSearchColumn('countryofmanufacture','custrecord_sku');
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return searchresults;
}
//code for GenerateFIMS Label
function GenerateFimsLabel(salesorderno,cartonlp,fulfillmentnumber)
{

	nlapiLogExecution('ERROR','salesorderno',salesorderno);
	nlapiLogExecution('ERROR','cartonlp',cartonlp);
	nlapiLogExecution('ERROR','FulFillment number in fims label generation',fulfillmentnumber);
	var searchresults="";
	//searchresults=Getsalesorderdetails(salesorderno);

	//var vebizOrdNo=searchresults[0].getId();
	var vebizOrdNo=salesorderno;
	nlapiLogExecution('ERROR','vebizOrdNo',vebizOrdNo);

	var trantype = nlapiLookupField('transaction', vebizOrdNo, 'recordType');
	var salesorderdetails =nlapiLoadRecord(trantype, vebizOrdNo);
	var location=salesorderdetails.getFieldValue('location');
	nlapiLogExecution('DEBUG', 'location',location);

	if(location!=null && location!='')
	{
		locationRecord = nlapiLoadRecord('location', location);
	}

	var addr1="";var city="";var state="";var zip=""; var stateandzip=""; var returnadresse="";
	var shipfromcountry="";
	//ship from address
	addr1=locationRecord.getFieldValue('addr1');
	city=locationRecord.getFieldValue('city');
	state=locationRecord.getFieldValue('state');
	zip=locationRecord.getFieldValue('zip');
	returnadresse=locationRecord.getFieldValue('addressee');
	shipfromcountry=locationRecord.getFieldValue('country');
	stateandzip=city+","+state+" "+zip;
	//Ship To Address
	var shipaddressee="";var shipaddr1="";var shipaddr2="";var shipcity="";var shipcountry="";var shipstate="";var shipzip="";var shipstateandcountry="";
	var ordernumber=salesorderdetails.getFieldValue('tranid');
	shipaddressee=salesorderdetails.getFieldValue('shipaddressee'); 
	shipaddr1=salesorderdetails.getFieldValue('shipaddr1'); 
	shipaddr2=salesorderdetails.getFieldValue('shipaddr2');
	shipcity=salesorderdetails.getFieldValue('shipcity'); 
	shipcountry=salesorderdetails.getFieldValue('shipcountry'); 
	shipstate=salesorderdetails.getFieldValue('shipstate'); 
	shipzip=salesorderdetails.getFieldValue('shipzip'); 
	if((shipaddressee==null)||(shipaddressee==''))
	{
		shipaddressee="";
	}
	if((shipaddr1==null)||(shipaddr1==''))
	{
		shipaddr1="";
	}
	if((shipaddr2==null)||(shipaddr2==''))
	{
		shipaddr2="";
	}
	if((shipcity==null)||(shipcity==''))
	{
		shipcity="";
	}
	if((shipcountry==null)||(shipcountry==''))
	{
		shipcountry="";
	}
	if((shipstate==null)||(shipstate==''))
	{
		shipstate="";
	}
	if((shipzip==null)||(shipzip==''))
	{
		shipzip="";
	}
	if((shipaddr2==null)||(shipaddr2==''))
	{
		shipaddr1=shipaddr1;
	}
	else
	{
		shipaddr1=shipaddr1+","+shipaddr2;
	}
	shipstateandcountry=shipcity+","+shipstate+" "+shipzip;
	var orderdate="";
	orderdate=salesorderdetails.getFieldValue('trandate');
	var customer=salesorderdetails.getFieldValue('entity');
	if(customer!=null && customer!='')
	{
		customerRecord = nlapiLoadRecord('customer',customer);
	}
	var phonenumber="";
	phonenumber=customerRecord.getFieldValue('phone');
	var customerpo="";
	customerpo=salesorderdetails.getFieldValue('otherrefnum');

	var itemdescription="";
	var itemlength="";

	//code for items against carton
	var itemresults=getItemRecord(cartonlp);
	if(itemresults!=null && itemresults!='')
	{
		//for loop for items
		for(var i=0;i<itemresults.length;i++)
		{
			if(i==0)
			{
				itemdescription=itemresults[i].getText('custrecord_sku');
			}
			if(i>=1)
			{
				itemdescription=itemdescription+"\n"+itemresults[i].getText('custrecord_sku');
			}
			if(i==0)
			{
				itemlength=itemresults[i].getValue('custrecord_expe_qty');
			}
			if(i>=1)
			{
				itemlength=itemlength+"\n"+itemresults[i].getValue('custrecord_expe_qty');
			}
		}
	}
//}

///code for country of manufacture
var itemid=itemresults[0].getValue('custrecord_sku');
//var itemrecord=nlapiLoadRecord('item',itemid);
var countryofmanufacture="";
countryofmanufacture=itemresults[0].getValue('countryofmanufacture','custrecord_sku');
var netweight="";
var totalvalue="";

nlapiLogExecution('DEBUG', 'CreateExtLabelData', 'CreateExtLabelData');

var eXtlabelrecord = nlapiCreateRecord('customrecord_ext_labelprinting');
eXtlabelrecord.setFieldValue('name',cartonlp); 
//ship from address
eXtlabelrecord.setFieldValue('custrecord_label_addr1',addr1); 
eXtlabelrecord.setFieldValue('custrecord_label_city',city);
eXtlabelrecord.setFieldValue('custrecord_label_state',state);
eXtlabelrecord.setFieldValue('custrecord_label_country',shipfromcountry);
eXtlabelrecord.setFieldValue('custrecord_label_zip',zip);

//ship to address
eXtlabelrecord.setFieldValue('custrecord_label_shipaddressee',shipaddressee); 
eXtlabelrecord.setFieldValue('custrecord_label_shipaddr1',shipaddr1);
eXtlabelrecord.setFieldValue('custrecord_label_addr2',shipaddr2);
eXtlabelrecord.setFieldValue('custrecord_label_shipcity',shipcity);
eXtlabelrecord.setFieldValue('custrecord_label_shipstate',shipstate);
eXtlabelrecord.setFieldValue('custrecord_label_shipcountry',shipcountry);
eXtlabelrecord.setFieldValue('custrecord_label_shipzip',shipzip);
eXtlabelrecord.setFieldValue('custrecord_label_custom1',phonenumber);
eXtlabelrecord.setFieldValue('custrecord_label_custom2',orderdate);
eXtlabelrecord.setFieldValue('custrecord_label_order',fulfillmentnumber);
eXtlabelrecord.setFieldValue('custrecord_label_custom4',itemdescription);
eXtlabelrecord.setFieldValue('custrecord_label_custom5',itemlength);
eXtlabelrecord.setFieldValue('custrecord_label_custom8',countryofmanufacture);


//Inserting Id value
/*var numaricid;
var filter=new Array();
var column=new Array();
column[0] = new nlobjSearchColumn('custrecord_label_reference1').setSort(true);
var recordlist= nlapiSearchRecord('customrecord_ext_labelprinting',null,filter,column);
nlapiLogExecution('ERROR','resultslength',recordlist.length);
if((recordlist==null)||(recordlist==''))
{
	numaricid="1000";
	eXtlabelrecord.setFieldValue('custrecord_label_reference1',numaricid.toString());
}
else
{
	var externalLabelRecord=nlapiLoadRecord('customrecord_ext_labelprinting',recordlist[0].getId());
	numaricid=externalLabelRecord.getFieldValue('custrecord_label_reference1');
	nlapiLogExecution('DEBUG', 'numaricid',numaricid);
	var increment="1";
	numaricid=parseInt(numaricid)+parseInt(increment);
	eXtlabelrecord.setFieldValue('custrecord_label_reference1',numaricid.toString());
}*/
	var tranid = nlapiSubmitRecord(eXtlabelrecord);
	nlapiLogExecution('DEBUG', 'internalid', tranid);
}
function GenerateCustomUCCLabel(vWMSOrdNo,containerLpShip,salesorderrecords,getFONO)
{

	nlapiLogExecution('DEBUG', 'CreateExtLabelData', 'CreateExtLabelData');
	nlapiLogExecution('DEBUG', 'vOrderNo',vWMSOrdNo);
	nlapiLogExecution('DEBUG', 'cartonnumber',containerLpShip);



	var location;
	var customername,customerpo;	
	var shiptoAddressee,shiptoAddress1,shiptoAddress2,shiptocity,shiptostate,shiptocountry,shiptocompany,shiptozipcode;
	customerpo=salesorderrecords.getFieldValue('otherrefnum'); 

	shiptoAddressee=salesorderrecords.getFieldValue('shipaddressee');
	shiptoAddress1=salesorderrecords.getFieldValue('shipaddr1');
	shiptoAddress2=salesorderrecords.getFieldValue('shipaddr2');

	shiptocity=salesorderrecords.getFieldValue('shipcity');
	shiptostate=salesorderrecords.getFieldValue('shipstate');
	shiptocountry=salesorderrecords.getFieldValue('shipcountry');
	shiptocompany=salesorderrecords.getFieldValue('entity');
	shiptozipcode=salesorderrecords.getFieldValue('shipzip');
	location=salesorderrecords.getFieldValue('location');


	var shipfromcity,shipfromcountry,shipfromzipcode,shipfromaddress,shipfromphone,shipfromstate;
	nlapiLogExecution('DEBUG', 'location',location);


	if(location !="" && location !=null)
	{
		nlapiLogExecution('ERROR', 'location', location);

		var record = nlapiLoadRecord('location', location);
		nlapiLogExecution('ERROR', 'location', location);
		shipfromaddress=record.getFieldValue('addr1');
		var addr2=record.getFieldValue('addr2');
		shipfromaddress=shipfromaddress+" " + addr2;
		shipfromcity=record.getFieldValue('city');
		shipfromstate=record.getFieldValue('state');
		shipfromzipcode =record.getFieldValue('zip');
		companyname=record.getFieldValue('addressee');
		shipfromphone=record.getFieldValue('addrphone');
		shipfromcountry =record.getFieldValue('country');


	}



	var uccnumber=getUCCNumber(containerLpShip);



	var ExternalLabelRecord = nlapiCreateRecord('customrecord_ext_labelprinting');
	ExternalLabelRecord.setFieldValue('name',containerLpShip); 
	//shipFrom Address
	nlapiLogExecution('DEBUG', 'vSalesOrderId', vWMSOrdNo);
	//ExternalLabelRecord.setFieldValue('custrecord_label_shipfromaddressee',sOrderArray["shipfromcompanyname"]); 
	ExternalLabelRecord.setFieldValue('custrecord_label_addr1',shipfromaddress); 
	ExternalLabelRecord.setFieldValue('custrecord_label_city',shipfromcity); 
	ExternalLabelRecord.setFieldValue('custrecord_label_state',shipfromstate); 
	ExternalLabelRecord.setFieldValue('custrecord_label_zip',shipfromzipcode); 
	//ShipToAddress
	//ExternalLabelRecord.setFieldValue('custrecord_wmsse_label_shipaddressee',shipaddressee); 
	ExternalLabelRecord.setFieldValue('custrecord_label_shipaddr1',shiptoAddress1); 
	//ExternalLabelRecord.setFieldValue('custrecord_wmsse_label_addr2',shiptoAddress2); 
	ExternalLabelRecord.setFieldValue('custrecord_label_shipcity',shiptocity); 
	ExternalLabelRecord.setFieldValue('custrecord_label_shipstate',shiptostate); 
	ExternalLabelRecord.setFieldValue('custrecord_label_shipcountry',shiptocountry); 
	ExternalLabelRecord.setFieldValue('custrecord_label_shipzip',shiptozipcode); 
	ExternalLabelRecord.setFieldValue('custrecord_label_custom1',customerpo);
	ExternalLabelRecord.setFieldValue('custrecord_label_custom2',getFONO);
	ExternalLabelRecord.setFieldValue('custrecord_label_licenseplatenumber',uccnumber);
	var tranid = nlapiSubmitRecord(ExternalLabelRecord);
	nlapiLogExecution('DEBUG', 'internalid', tranid);
}
function getUCCNumber(carton)
{

	var filterlpmaster = new Array();
	filterlpmaster.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', carton));
	var columnlpmaster = new Array();
	columnlpmaster[0]= new nlobjSearchColumn('custrecord_ebiz_lpmaster_sscc');
	columnlpmaster[1]= new nlobjSearchColumn('custrecord_ebiz_lpmaster_masterlp');
	columnlpmaster[2]= new nlobjSearchColumn('internalid').setSort(true);
	columnlpmaster[3]= new nlobjSearchColumn('custrecord_ebiz_lpmaster_site');

	var searchlpmaster= new nlapiSearchRecord('customrecord_ebiznet_master_lp',null,filterlpmaster,columnlpmaster);
	if((searchlpmaster !=null)&&(searchlpmaster.length>0)&&(searchlpmaster !=''))
	{
		uccnumber=searchlpmaster[0].getValue('custrecord_ebiz_lpmaster_sscc');
		if(uccnumber=="" || uccnumber=="")
		{

			var vsiteid=searchlpmaster[0].getValue('custrecord_ebiz_lpmaster_site');
			var uccId=searchlpmaster[0].getId();
			uccnumber=GenerateLabel("",vsiteid);
			nlapiLogExecution('DEBUG', 'uccnumber', uccnumber);	
			var fields = new Array();
			var values = new Array();
			fields[1] ='custrecord_ebiz_lpmaster_sscc';
			values[1] = uccnumber;
			var updatefields = nlapiSubmitField('customrecord_ebiznet_master_lp',uccId,fields,values);

		}

	}

	return uccnumber;

}
function GenerateLabel(uompackflag,vsiteid)
{

	nlapiLogExecution('ERROR', 'CreateMasterLPRecord uompackflag', uompackflag);
	var finaltext="";
	var duns="";
	var label="",uom="",ResultText="";
	//added by mahesh


	try 
	{	
		var lpMaxValue=GetMaxLPNo('1', '5',vsiteid);
		nlapiLogExecution('ERROR', 'lpMaxValue', lpMaxValue);

		var prefixlength=lpMaxValue.length;
		nlapiLogExecution('ERROR', 'prefixlength', prefixlength);
//		Case # 201412758 Start
		if(prefixlength==0)
			label="00000";
		else if(prefixlength==1)
			label="0000"+lpMaxValue;
		else if(prefixlength==2)
			label="000"+lpMaxValue;
		else if(prefixlength==3)
			label="00"+lpMaxValue;
		else if(prefixlength==4)
			label="0"+lpMaxValue;
		else if(prefixlength==5)
			label=lpMaxValue;
 		else if(prefixlength==6)
 			 label="000"+lpMaxValue;
 		else if(prefixlength==7)
 			label=lpMaxValue;
 			label="00"+lpMaxValue;
//		else if(prefixlength==8)
//			label="0"+lpMaxValue;
//		else if(prefixlength==9)
//			label=lpMaxValue;
//		Case # 201412758 End
		//to get company id

		duns=GetCompanyDUNSnumber('');

		if(uompackflag == "1" || uompackflag == "EACH" ) 
			uom="0"; 
		else if(uompackflag == "3" || uompackflag == "PALLET") 
			uom="2";
		else
			uom="0";
		nlapiLogExecution('ERROR', 'uom', uom);
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord finaltext', finaltext);
		finaltext=uom+duns+label;

		//finaltext=duns+label;

		nlapiLogExecution('ERROR', 'CreateMasterLPRecord finaltext', finaltext);
		var str1=finaltext;
		var str2='31313131313131313';

		var res1=str1.split("");
		var res2=str2.split("");
		var total=0;
		for(var i=0;i < res1.length;i++)
		{ 
			var rchkdigit=parseInt(res1[i])*parseInt(res2[i]);
			total=parseInt(total)+parseInt(rchkdigit);
		} 

		var multiple=parseInt(Math.ceil(total / 10.0))*10;	
		var CheckDigitValue=(multiple-total).toString();
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord CheckDigit', CheckDigitValue.toString());
		ResultText="00"+finaltext+CheckDigitValue.toString();
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord ResultText', ResultText);
	} 
	catch (err) 
	{

	}
	return ResultText;
}

//Generating Zebra labels for advanced NSWMS Advanced
function GenerateZebraUccLabel(vWMSSeOrdNo,containerLpShip,salesorderrecord,getFONO)
{

	nlapiLogExecution('DEBUG', 'CreateZebraUccLabel', 'CreateZebraUccLabel');
	nlapiLogExecution('DEBUG', 'vOrderNo',vWMSSeOrdNo);
	nlapiLogExecution('DEBUG', 'cartonnumber',containerLpShip);


	//var salesorderrecord=nlapiLoadRecord('salesorder', vWMSSeOrdNo);
	var labeltype="UCCLABEL";
	var shiptocompanyid=salesorderrecord.getFieldValue('entity');

	var labeldata=GetSELabelTemplate("",labeltype);
	var location;
	var customername,customerpo;	
	if(labeldata!=null && labeldata!="")
	{
		var uccnumbersearchresults=getUCCNumber(containerLpShip);
		var uccnumber=uccnumbersearchresults;

		/*if(uccnumbersearchresults!=null && uccnumbersearchresults!="")
		{
			uccnumber=uccnumbersearchresults[0].getValue('custrecord_ebiz_lpmaster_sscc');
		}*/
		var labelcount="";
		nlapiLogExecution('DEBUG', 'uccnumber',uccnumber);
		GenerateZebraLabel(labeltype,labeldata,uccnumber,vWMSSeOrdNo,"",labelcount,salesorderrecord,containerLpShip);

	}
}
function GenerateZebraAddressLabel(vWMSSeOrdNo,salesorderrecord)
{

	nlapiLogExecution('DEBUG', 'CreateZebraUccLabel', 'CreateZebraUccLabel');
	nlapiLogExecution('DEBUG', 'vOrderNo',vWMSSeOrdNo);
	//var salesorderrecord=nlapiLoadRecord('salesorder', vWMSSeOrdNo);
	var labeltype="ADDRESSLABEL";
	var labeldata=GetSELabelTemplate("",labeltype);
	var location;
	var customername,customerpo;	
	var labelcount="";
	if(labeldata!=null && labeldata!="")
	{
		GenerateZebraLabel(labeltype,labeldata,"",vWMSSeOrdNo,"",labelcount,salesorderrecord,"");
	}
}
function GenerateZebraLabel(labeltype,Label,ucc,vWMSSeOrdNo,skuname,labelcount,salesorderrecord,containerLpShip)
{


	//shiptocompanyAdress		
	var shiptocity=salesorderrecord.getFieldValue('shipcity');
	var shiptostate=salesorderrecord.getFieldValue('shipstate');
	var shiptocountry=salesorderrecord.getFieldValue('shipcountry');
	var shiptocompany=salesorderrecord.getFieldValue('shipaddressee');
	var shiptozipcode=salesorderrecord.getFieldValue('shipzip');
	var shiptoAddress2=salesorderrecord.getFieldValue('shipaddr2');
	var shiptoAddress1=salesorderrecord.getFieldValue('shipaddr1');
	nlapiLogExecution('ERROR', 'shiptoAddress1',shiptoAddress1);
	var customerpo=salesorderrecord.getFieldValue('otherrefnum');
	var location=salesorderrecord.getFieldValue('location');
	var shiptocompanyid=salesorderrecord.getFieldValue('entity');


	var customerpo=salesorderrecord.getFieldValue('otherrefnum');
	var location=salesorderrecord.getFieldValue('location');
	var shipcarrier=salesorderrecord.getFieldText('shipmethod');
	var shiptocompanyid=salesorderrecord.getFieldValue('entity');


	nlapiLogExecution('ERROR', 'location', location);
	var record = nlapiLoadRecord('location', location);
	var salesorder=salesorderrecord.getFieldValue('tranid');
	var shipfromaddress1=record.getFieldValue('addr1');
	var shipfromaddress2=record.getFieldValue('addr2');
	var shipfromcity=record.getFieldValue('city');
	var shipfromstate=record.getFieldValue('state');
	var shipfromzipcode =record.getFieldValue('zip');
	var shipfromcompanyname=record.getFieldValue('addressee');
	var shipfromcountry =record.getFieldValue('country');
	//This code not in Dev.Code For Production Dynacraft
	var shipfromaddress3=record.getFieldValue('addr3');

	var shipdate=DateStamp();
	if((shiptoAddress1!=null)&&(shiptoAddress1!=""))
	{
		Label =Label.replace(/parameter01/,shiptoAddress1);
	}
	else
	{
		Label =Label.replace(/parameter01/,'');
	}
	if((shiptoAddress2!=null)&&(shiptoAddress2!=""))
	{
		Label =Label.replace(/parameter02/,shiptoAddress2);
	}
	else
	{
		Label =Label.replace(/parameter02/,'');
	}
	if((shiptocity!=null)&&(shiptocity!=""))
	{
		Label =Label.replace(/parameter03/,shiptocity);
	}
	else
	{
		Label =Label.replace(/parameter03/,'');
	}
	if((shiptostate!=null)&&(shiptostate!=""))
	{
		Label =Label.replace(/parameter04/,shiptostate);
	}
	else
	{
		Label =Label.replace(/parameter04/,'');
	}
	if((shiptocountry!=null)&&(shiptocountry!=""))
	{
		Label =Label.replace(/parameter05/,shiptocountry);
	}
	else
	{
		Label =Label.replace(/parameter05/,'');
	}
	if((shiptozipcode!=null)&&(shiptozipcode!=""))
	{
		Label =Label.replace(/parameter06/g,shiptozipcode);
	}
	else
	{  
		Label =Label.replace(/parameter06/g,'');
	}


	if((shiptocompany!=null)&&(shiptocompany!=""))
	{
		Label =Label.replace(/parameter07/g,shiptocompany);
	}
	else
	{
		Label =Label.replace(/parameter07/g,'');
	}


	if((shipfromaddress1!=null)&&(shipfromaddress1!=""))
	{
		Label =Label.replace(/parameter08/,shipfromaddress1);
	}
	else
	{
		Label =Label.replace(/parameter08/,'');
	}
	if((shipfromaddress2!=null)&&(shipfromaddress2!=""))
	{
		Label =Label.replace(/parameter09/,shipfromaddress2);
	}
	else
	{
		Label =Label.replace(/parameter09/,'');
	}
	if((shipfromcity!=null) &&(shipfromcity!=""))
	{
		Label =Label.replace(/parameter10/,shipfromcity);
	}
	else
	{
		Label =Label.replace(/parameter10/,'');
	}
	if((shipfromstate!=null)&&(shipfromstate!=""))
	{
		Label =Label.replace(/parameter11/,shipfromstate);
	}
	else
	{
		Label =Label.replace(/parameter11/,'');

	}
	if((shipfromcountry!=null) && (shipfromcountry!=""))
	{
		Label =Label.replace(/parameter12/,shipfromcountry);
	}
	else
	{
		Label =Label.replace(/parameter12/,'');
	}
	if((shipfromzipcode!=null) && (shipfromzipcode!=""))
	{
		Label =Label.replace(/parameter13/,shipfromzipcode);
	}
	else
	{
		Label =Label.replace(/parameter13/,'');
	}
	if((customerpo!=null)&&(customerpo!=""))
	{
		Label =Label.replace(/parameter14/g,customerpo);
	}
	else
	{
		Label =Label.replace(/parameter14/g,'');
	}


	if((shipcarrier!=null)&&(shipcarrier!=""))
	{
		Label =Label.replace(/parameter15/,shipcarrier);
	}
	else
	{
		Label =Label.replace(/parameter15/,'');
	}


	if((salesorder!=null)&&(salesorder!=""))
	{
		Label =Label.replace(/parameter16/g,salesorder);
	}
	else
	{
		Label =Label.replace(/parameter16/g,'');
	}

	if((skuname!=null)&&(skuname!=""))
	{
		Label =Label.replace(/parameter17/,skuname);
	}
	else
	{
		Label =Label.replace(/parameter17/,'');
	}
	if((ucc!=null)&&(ucc!=""))
	{
		Label =Label.replace(/parameter18/g,ucc);
	}
	else
	{
		Label =Label.replace(/parameter18/g,'');
	}
	if((shipfromcompanyname!=null)&&(shipfromcompanyname!=""))
	{
		Label =Label.replace(/parameter19/,shipfromcompanyname);
	}
	else
	{
		Label =Label.replace(/parameter19/,'');
	}
	if((shipdate!=null)&&(shipdate!=""))
	{
		Label =Label.replace(/parameter20/,shipdate);
	}
	else
	{
		Label =Label.replace(/parameter20/,'');
	}

	var print="F";
	var reprint="F"
		var refno="";
	var printername="";
	CreateLabelDataNew(Label,labeltype,refno,print,reprint,shiptocompanyid,salesorder,skuname,labelcount,printername,containerLpShip);

}




function CreateLabelDataNew(labeldata,labeltype,refno,print,reprint,company,salesorder,skuname,labelcount,printername,containerLpShip)
{
	nlapiLogExecution('ERROR','CreateLabelData','CreateLabelData');	
	var labelrecord = nlapiCreateRecord('customrecord_labelprinting'); 
	labelrecord.setFieldValue('name', salesorder); 
	labelrecord.setFieldValue('custrecord_labeldata',labeldata);  
	labelrecord.setFieldValue('custrecord_label_refno',labeltype);     

	labelrecord.setFieldValue('custrecord_labeltype',"ZebraLabel");                                                                                                                                                                     
	labelrecord.setFieldValue('custrecord_label_print', print);
	labelrecord.setFieldValue('custrecord_label_reprint', reprint);
	labelrecord.setFieldValue('custrecord_label_lp', containerLpShip);

	labelrecord.setFieldValue('custrecord_label_printername', printername);

	var tranid = nlapiSubmitRecord(labelrecord);
	nlapiLogExecution('ERROR','recordid',tranid);
}

function GetSELabelTemplate(shiptocompanyid,labeltype)
{
	var filtertempalte = new Array();

	nlapiLogExecution('ERROR', 'GetLabelTemplate','GetLabelTemplate');
	nlapiLogExecution('ERROR', 'shiptocompanyid', shiptocompanyid);
	if((shiptocompanyid!=null)&&(shiptocompanyid!=""))
	{
		filtertempalte.push(new nlobjSearchFilter('custrecord_labeltemplate_name',null,'anyof',shiptocompanyid));
	}
	filtertempalte.push(new nlobjSearchFilter('name',null,'is',labeltype)); 
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_labeltemplate_data');
	var searchtemplate= nlapiSearchRecord('customrecord_label_templates',null,filtertempalte,columns);
	var Label="";
	if((searchtemplate !=null)&&(searchtemplate!=""))
	{

		Label=searchtemplate[0].getValue('custrecord_labeltemplate_data');
	}	
	return Label;
}
function generatebartenderpicklabel(vwaveno)
{
	nlapiLogExecution('ERROR', 'Generate PickLabel: ',vwaveno);

	var waverfilterarray= new Array();
	waverfilterarray.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(vwaveno)));
	waverfilterarray.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
	waverfilterarray.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));
	waverfilterarray.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is',"T"));	
	var wavecolumnarray = new Array();
	wavecolumnarray [0] = new nlobjSearchColumn('custrecord_ebiz_clus_no', null, 'group');
	wavecolumnarray [1] = new nlobjSearchColumn('name', null, 'group');
	wavecolumnarray [2] = new nlobjSearchColumn('custrecord_container', null, 'group');
	wavecolumnarray [3] = new nlobjSearchColumn('custrecord_container_lp_no', null, 'group');
	wavecolumnarray[4]=new nlobjSearchColumn('Internalid','custrecord_ebiz_order_no','group');

	var wavesearchresults= nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, waverfilterarray, wavecolumnarray);	
	if(wavesearchresults!=null)
	{

		for(var g=0;g<wavesearchresults.length;g++)
		{
			var clusterno=wavesearchresults[g].getValue('custrecord_ebiz_clus_no', null, 'group');
			var name=wavesearchresults[g].getValue('name', null, 'group');
			var shipmentnumber=getShipmentfromfoline(name);
			var containerlp=wavesearchresults[g].getValue('custrecord_container_lp_no', null, 'group');
			var containersize=wavesearchresults[g].getText('custrecord_container', null, 'group');
			var vebizOrdNo=wavesearchresults[g].getValue('Internalid','custrecord_ebiz_order_no','group');
			var trantype = nlapiLookupField('transaction', vebizOrdNo, 'recordType');
			var salesorderdetails =nlapiLoadRecord(trantype, vebizOrdNo);
			var orderdate=salesorderdetails.getFieldValue('trandate');
			var shipmethod=salesorderdetails.getFieldText('shipmethod');
			var zonearrayvalues=getaileandzonerecords(name,containerlp);
			var aile=zonearrayvalues["Aile"];
			var zonevlaue=zonearrayvalues["ZoneString"];
			var zoneprinter=zonearrayvalues["ZonePrinter"];
			if(zoneprinter=="- None -"||zoneprinter=="")
			{
				zoneprinter="";
			}
			var stageloc="";
			var printdate="";
			var timeprint=TimeStampinSec();
			printdate=DateStamp();

			var vnewwave=vwaveno.split('.');
			vwaveno=vnewwave[0];
			var eXtlabelrecord = nlapiCreateRecord('customrecord_ext_labelprinting');
			eXtlabelrecord.setFieldValue('name', containerlp);
			eXtlabelrecord.setFieldValue('custrecord_label_reference3',clusterno);
			eXtlabelrecord.setFieldValue('custrecord_label_reference4',containerlp);
			eXtlabelrecord.setFieldValue('custrecord_label_reference5',containersize);	
			eXtlabelrecord.setFieldValue('custrecord_label_custom1',printdate);
			eXtlabelrecord.setFieldValue('custrecord_label_custom2',timeprint);
			eXtlabelrecord.setFieldValue('custrecord_label_custom3',shipmethod);
			eXtlabelrecord.setFieldValue('custrecord_label_custom4',orderdate);
			eXtlabelrecord.setFieldValue('custrecord_label_custom5',name);
			eXtlabelrecord.setFieldValue('custrecord_label_custom6',shipmentnumber);
			eXtlabelrecord.setFieldValue('custrecord_label_custom7',"");
			eXtlabelrecord.setFieldValue('custrecord_label_custom8',aile);
			eXtlabelrecord.setFieldValue('custrecord_label_custom9',vwaveno);
			eXtlabelrecord.setFieldValue('custrecord_label_ebizprinter',zoneprinter);
			eXtlabelrecord.setFieldValue('custrecord_label_ebizorder',vebizOrdNo);
			eXtlabelrecord.setFieldValue('custrecord_label_template',"PICKLABEL");

			var tranid = nlapiSubmitRecord(eXtlabelrecord);

		}

	}


}
function getaileandzonerecords(name,contlp)
{
	try{
		nlapiLogExecution('ERROR', 'Generate name and : contlp',name +" "+contlp);
		var zonerecordsarray = new Array();
		var printerfirstzoneId="";
		var stringappend="";
		var filterzone = new Array();
		filterzone.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is',contlp));
		filterzone.push(new nlobjSearchFilter('name', null, 'is',name));
		filterzone.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));

//carrie added columnzone[5] pallet position to have that print on label as well.  DO NOT CHANGE

	var columnzone = new Array();
	columnzone[0] = new nlobjSearchColumn('custrecord_bin_locgroup_seq',null,'group');
	columnzone[1]= new nlobjSearchColumn('custrecord_ebiz_zoneid',null,'group')
	columnzone[2]= new nlobjSearchColumn('custrecord_aisle_row','custrecord_actbeginloc','group')
		columnzone[3]= new nlobjSearchColumn('custrecord_wms_zoneprinter','custrecord_ebiz_zoneid','group');
	//columnzone[3] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group').setSort(true);
	columnzone[4]= new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
	//columnzone[4] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group').setSort(true);
        columnzone[5]= new nlobjSearchColumn('custrecord_palletposition','custrecord_actbeginloc','group');//.setSort(true);
   	 columnzone[0].setSort();
		columnzone[4].setSort();
		columnzone[5].setSort(true);

	var searchopentaskrecords =nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filterzone,columnzone);
	var firstzoneprinter="";
	if(searchopentaskrecords!=null)
	{
		var aislecount=0;
		var repeatvalue="";
		for(var i=0;i<searchopentaskrecords.length;i++)
		{
			var row=searchopentaskrecords[i].getValue('custrecord_aisle_row','custrecord_actbeginloc','group');
			var position=searchopentaskrecords[i].getValue('custrecord_palletposition','custrecord_actbeginloc','group');
			var aile=row+position;

			if(aile!=null)
			{
				//if(aile!=repeatvalue)
				//{
					stringappend +=aile;
					aislecount++;
					if(aislecount==6)
					{
						 
						stringappend +="\n";	
						aislecount=0;
					}
					else
					{
						stringappend +=",";
					}
					repeatvalue=aile;
				//}
			}
		}
		var stringappendzone="";
		var stringzonelevelprinter="";
		var repeatzonevalue="";
		var firstzoneIds="";
		for(var j=0;j<searchopentaskrecords.length;j++)
		{
			var zonevalue=searchopentaskrecords[j].getText('custrecord_ebiz_zoneid',null,'group');	
				firstzoneprinter=searchopentaskrecords[0].getValue('custrecord_wms_zoneprinter','custrecord_ebiz_zoneid','group');
			if(zonevalue=="- None -"||zonevalue=="")
			{
				zonevalue="";
			}	
			if(zonevalue!=null)
			{
				if(zonevalue!=repeatzonevalue)
				{ 
					stringappendzone+=zonevalue+",";
					repeatzonevalue=zonevalue;
				}
			}
		}

		zonerecordsarray["Aile"]=stringappend;
		zonerecordsarray["ZoneString"]=stringappendzone;
		zonerecordsarray["ZonePrinter"]=firstzoneprinter;


		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exp-getaileandzonerecords',exp)

	}
	return zonerecordsarray;
}

function getShipmentfromfoline(name)
{
	var shipmentnumber="";
	var fofilter = new Array();
	fofilter.push(new nlobjSearchFilter('custrecord_lineord', null, 'is',name));
	fofilter.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [9]));
	var focolumnarray = new Array();
	focolumnarray[0] = new nlobjSearchColumn('custrecord_shipment_no', null, 'group');

	var fosearchresults = new nlapiSearchRecord('customrecord_ebiznet_ordline', null, fofilter,focolumnarray);	
	if(fosearchresults!=null)
	{
		shipmentnumber=fosearchresults[0].getValue('custrecord_shipment_no', null, 'group');
	}
	return shipmentnumber;
}
function getPackedRecordsBasedCarton(containerlp,fulfillmentOrder,vebizOrdNo)
{

	var groupopentaskfilterarray = new Array();
	//Please uncheck  the fillementnumber code 
	groupopentaskfilterarray.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof',vebizOrdNo));
	groupopentaskfilterarray.push(new nlobjSearchFilter('name', null, 'is',fulfillmentOrder.toString()));
	groupopentaskfilterarray.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is',containerlp));
	groupopentaskfilterarray.push( new nlobjSearchFilter('custrecord_tasktype', null, 'anyof',[3])); 
	groupopentaskfilterarray.push( new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof',[28]));
	groupopentaskfilterarray.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is', "T"));
	var groupopentaskcolumnarray = new Array();
	groupopentaskcolumnarray[0] = new nlobjSearchColumn('salesdescription', 'custrecord_sku', 'group');
	groupopentaskcolumnarray[1] = new nlobjSearchColumn('custrecord_sku', null, 'group');
	groupopentaskcolumnarray[2] = new nlobjSearchColumn('custrecord_line_no', null, 'group');
	groupopentaskcolumnarray[3] = new nlobjSearchColumn('formulanumeric', null, 'sum');
	groupopentaskcolumnarray[3].setFormula("TO_NUMBER({custrecord_act_qty})");
	groupopentaskcolumnarray[4] = new nlobjSearchColumn('trandate', 'custrecord_ebiz_order_no', 'group');
	var groupopentasksearchresults  = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, groupopentaskfilterarray, groupopentaskcolumnarray);
	return groupopentasksearchresults;


}
function GetLabelSpecificPrintername(printertype)
{
	var printername="";

	var printerfilter= new Array();
	var userId;
	userId = nlapiGetUser();
	if(userId=="-4")
	{
		userId="";
	}
	nlapiLogExecution('Debug', 'DefaultUser', userId);
	var printerfilterarray = new Array();
	if(userId!=null && userId!="")
	{
		
		printerfilterarray.push(new nlobjSearchFilter('custrecord_printer_employee', null, 'anyof', userId));
	
	}
	else
	{
		printerfilterarray.push(new nlobjSearchFilter('custrecord_printer_carrier', null, 'is', "DefaultPrinter"));
		
	}
	printerfilter.push(new nlobjSearchFilter('name', null, 'is', printertype));
	var printercolumn = new Array();
	printercolumn.push(new nlobjSearchColumn('custrecord_printer_printername'));
	var printersearchresults = nlapiSearchRecord('customrecord_printer_preferences', null, printerfilter,printercolumn);
	if(printersearchresults!=null && printersearchresults!="")
	{
		printername=printersearchresults[0].getValue("custrecord_printer_printername");
	}


	return printername;

}
function GenerateBartenderPackingSlip(newRecord)
{

	nlapiLogExecution('ERROR', 'GenerateBartenderPackingSlip','GenerateBartenderPackingSlip');

	var addr1=newRecord.getFieldValue('custrecord_ship_addr1');
	var addr2=newRecord.getFieldValue('custrecord_ship_addr2');
	var zip=newRecord.getFieldValue('custrecord_ship_zip');
	var state=newRecord.getFieldValue('custrecord_ship_state');
	var city=newRecord.getFieldValue('custrecord_ship_city');
	var totalZippedaddress=city+","+state+","+zip;
	var ShipDate=DateStamp();
	var ordnumber=newRecord.getFieldValue('custrecord_ship_orderno');
	var Orderdate=""
		var fulfillmentOrder=newRecord.getFieldValue('custrecord_ship_ref3');
	var containerlp =newRecord.getFieldValue('custrecord_ship_contlp');
	var salesOrderID = newRecord.getFieldValue('custrecord_ship_order');
	var mastertrack=newRecord.getFieldValue('custrecord_ship_masttrackno');
	var consignee=newRecord.getFieldValue('custrecord_ship_consignee');
	var Carrier=newRecord.getFieldValue('custrecord_ship_servicelevel');
	var groupopentasksearchresults = getPackedRecordsBasedCarton(containerlp, fulfillmentOrder, salesOrderID);
	var stringbuilder = "";
	var qtybuilder = "";
	var itembuilder = "";
	var getPacklistPrintername = GetLabelSpecificPrintername("Packlist");
	
	var grouplength=0;
	
	var totalcount=groupopentasksearchresults.length;
	if (groupopentasksearchresults != null) {
		Orderdate = groupopentasksearchresults[0].getValue('trandate', 'custrecord_ebiz_order_no', 'group');
		while(0<totalcount)
		{
var strVar ="";
			strVar +="<html><body><table>";
			var count=0;
			
			for(var g=grouplength; g<groupopentasksearchresults.length;g++)
			{

				grouplength++;
				count++;
				




				var totalqty = groupopentasksearchresults[g].getValue('formulanumeric', null, 'sum');
				var itemText = groupopentasksearchresults[g].getText('custrecord_sku', null, 'group');
				var displayname = groupopentasksearchresults[g].getValue('salesdescription', 'custrecord_sku', 'group');
				strVar +=" <tr style=\"font-size: 10px;height: 18px; font-family:Arial;\"><td>"+totalqty+"</td><td></td><td>"+itemText+"</td><td>"+displayname+"</td></tr>";
				if(count==10)
				{
					break;
				}
			}
			strVar +="</table></body><html>";

			nlapiLogExecution('ERROR', 'totalcount', totalcount);
			totalcount=parseInt(totalcount)-parseInt(count);
			nlapiLogExecution('ERROR', 'totalcountafter', totalcount);
			var ExternalLabelRecord = nlapiCreateRecord('customrecord_ext_labelprinting');
			ExternalLabelRecord.setFieldValue('name',containerlp); 

			ExternalLabelRecord.setFieldValue('custrecord_label_addr2',addr2); 
			ExternalLabelRecord.setFieldValue('custrecord_label_shipaddr1',addr1); 
			//ExternalLabelRecord.setFieldValue('custrecord_wmsse_label_addr2',shiptoAddress2); 
			ExternalLabelRecord.setFieldValue('custrecord_label_shipzip',totalZippedaddress);

			ExternalLabelRecord.setFieldValue('custrecord_label_qty', qtybuilder);
			ExternalLabelRecord.setFieldValue('custrecord_label_item', itembuilder);
			
			ExternalLabelRecord.setFieldValue('custrecord_label_reference2',ordnumber);
			ExternalLabelRecord.setFieldValue('custrecord_label_shipaddressee',consignee);
			ExternalLabelRecord.setFieldValue('custrecord_label_itemdesc', strVar); 
			ExternalLabelRecord.setFieldValue('custrecord_label_custom3',ShipDate);
			ExternalLabelRecord.setFieldValue('custrecord_label_custom4',Orderdate);
			ExternalLabelRecord.setFieldValue('custrecord_label_custom5',mastertrack);
			ExternalLabelRecord.setFieldValue('custrecord_label_custom6',Carrier);
			ExternalLabelRecord.setFieldValue('custrecord_label_custom2',fulfillmentOrder);
			ExternalLabelRecord.setFieldValue('custrecord_label_ebizorder',salesOrderID);
			ExternalLabelRecord.setFieldValue('custrecord_label_ebizorder',salesOrderID);
			ExternalLabelRecord.setFieldValue('custrecord_label_licenseplatenumber',containerlp);
			ExternalLabelRecord.setFieldValue('custrecord_label_template',"PACKLABEL");
			ExternalLabelRecord.setFieldValue('custrecord_label_labeltype',"PACKLABEL");
			ExternalLabelRecord.setFieldValue('custrecord_label_ebizprinter',getPacklistPrintername);

			var tranid = nlapiSubmitRecord(ExternalLabelRecord);
			nlapiLogExecution('DEBUG', 'internalid', tranid);
		}
	}

}
function generateZebraitemlabel(expqty, ItemId,newRecord)
{
	var itemlabeltemplate=GetSELabelTemplate("","ITEMLABEL");
	if(itemlabeltemplate!=null && itemlabeltemplate!="")
	{
		nlapiLogExecution('Debug', 'Generate Item Label','Generate Item Label');	
		var fields = ['itemid', 'upccode','displayname'];
		var columns = nlapiLookupField('item',ItemId,fields);
		var itemName=columns.displayname;
		var upcnumber=columns.upccode;
		var skutext = newRecord.getFieldText('custrecord_sku');
		 
		var poname=newRecord.getFieldValue('name');
		if((expqty!=null)&&(expqty!=""))
		{
			itemlabeltemplate=itemlabeltemplate.replace(/parameter26/g,expqty);
		}
		else
		{
			itemlabeltemplate=itemlabeltemplate.replace(/parameter26/g,'');
		}
		if((upcnumber!=null)&&(upcnumber!=""))
		{
			itemlabeltemplate=itemlabeltemplate.replace(/parameter27/g,upcnumber);
		}
		else
		{
			itemlabeltemplate=itemlabeltemplate.replace(/parameter27/g,'');
		}
		if((itemName!=null)&&(itemName!=""))
		{
			itemlabeltemplate=itemlabeltemplate.replace(/parameter21/,itemName);
		}
		else
		{
			itemlabeltemplate=itemlabeltemplate.replace(/parameter21/,'');
		}
		if((skutext!=null)&&(skutext!=""))
		{
			itemlabeltemplate=itemlabeltemplate.replace(/parameter25/g,skutext);
		}
		else
		{
			itemlabeltemplate=itemlabeltemplate.replace(/parameter25/g,'');
		}
		
		var print="F";
		var reprint="F"
			var refno="";
		var printername="";
	
		CreateLabelData(itemlabeltemplate,"ZebraLabel","","ItemLabel",print,reprint,"","",poname)
	}
	else
	{
		nlapiLogExecution('DEBUG', 'item label template not configured');
		
	}
}
function generateitemlabel(expqty,ItemId,poName)
{
	try
	{
	var fields = ['itemid', 'upccode','displayname','upccode'];
	var columns = nlapiLookupField('item',ItemId,fields);

	var rcvQty=expqty;
	var itemName=columns.displayname;
	var upcnumber=columns.upccode;
    var itemtextname=columns.itemid;
	
    var ExternalLabelRecord = nlapiCreateRecord('customrecord_ext_labelprinting');
	ExternalLabelRecord.setFieldValue('name',poName);
	ExternalLabelRecord.setFieldValue('custrecord_label_custom3',itemtextname);
	ExternalLabelRecord.setFieldValue('custrecord_label_custom4',upcnumber);
	ExternalLabelRecord.setFieldValue('custrecord_label_custom5',itemName);
	ExternalLabelRecord.setFieldValue('custrecord_label_custom6',rcvQty);
	ExternalLabelRecord.setFieldValue('custrecord_label_custom2',poName);
	ExternalLabelRecord.setFieldValue('custrecord_label_labeltype',"ItemLabel");
	var tranid = nlapiSubmitRecord(ExternalLabelRecord);
	nlapiLogExecution('DEBUG', 'internalid', tranid)
	
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exp-ItemLabel',exp)
		
	}
}
function genarateBartenderPalletLabel(newRecord)
{

	var rcvQty=newRecord.getFieldValue('custrecord_expe_qty');
	var poName=newRecord.getFieldValue('name');
	var ItemId=newRecord.getFieldValue('custrecord_sku');
	var fields = ['itemid', 'upccode','displayname','upccode'];
	var columns = nlapiLookupField('item',ItemId,fields);
	var recevieddate=DateStamp();
	var itemName=columns.displayname;
	var upcnumber=columns.upccode;
	var itemtextname=columns.itemid;
	var labeltype="PalletLabel";
	var lpnumber=newRecord.getFieldValue('custrecord_ebiz_lpno');
	var printername=GetLabelSpecificPrintername(labeltype);
	var userid = getCurrentUser();
	nlapiLogExecution('ERROR','currentUserID',userid);
	var employeefields=['entityid'];


	var Emprecord = nlapiLookupField('Employee',userid,employeefields);
	var username=Emprecord.entityid;


	var ExternalLabelRecord = nlapiCreateRecord('customrecord_ext_labelprinting');
	ExternalLabelRecord.setFieldValue('name',poName);
	ExternalLabelRecord.setFieldValue('custrecord_label_custom1',lpnumber);
	ExternalLabelRecord.setFieldValue('custrecord_label_custom3',itemtextname);
	ExternalLabelRecord.setFieldValue('custrecord_label_custom4',upcnumber);
	ExternalLabelRecord.setFieldValue('custrecord_label_custom5',itemName);
	ExternalLabelRecord.setFieldValue('custrecord_label_custom6',rcvQty);
	ExternalLabelRecord.setFieldValue('custrecord_label_custom2',poName);
	ExternalLabelRecord.setFieldValue('custrecord_label_custom7',username);
	ExternalLabelRecord.setFieldValue('custrecord_label_custom8',recevieddate);
	ExternalLabelRecord.setFieldValue('custrecord_label_labeltype',labeltype);
	ExternalLabelRecord.setFieldValue('custrecord_label_template',labeltype);
	var tranid = nlapiSubmitRecord(ExternalLabelRecord);
	nlapiLogExecution('DEBUG', 'internalid', tranid);
}
function generateZebraPalletLabel(newRecord)
{
	var palletlabel=GetSELabelTemplate("","PALLETLABEL");
	if(palletlabel!=null && palletlabel!="")
	{
		var itemqty=newRecord.getFieldValue('custrecord_expe_qty');
		var poName=newRecord.getFieldValue('name');
		var ItemId=newRecord.getFieldValue('custrecord_sku');
		var fields = ['itemid', 'upccode','displayname','upccode'];
		var columns = nlapiLookupField('item',ItemId,fields);
		var recevieddate=DateStamp();
		var itemName=columns.displayname;
		var upcnumber=columns.upccode;
		var skuname=columns.itemid;
        var lpnumber=newRecord.getFieldValue('custrecord_ebiz_lpno');
		var userid = getCurrentUser();
		nlapiLogExecution('ERROR','currentUserID',userid);
		var employeefields=['entityid'];


		var Emprecord = nlapiLookupField('Employee',userid,employeefields);
		var username=Emprecord.entityid;

		if((lpnumber!=null)&&(lpnumber!=""))
		{
			palletlabel=palletlabel.replace(/parameter28/g,lpnumber);
		}
		else
		{
			palletlabel=palletlabel.replace(/parameter28/g,'');
		}

		if((itemName!=null)&&(itemName!=""))
		{
			palletlabel=palletlabel.replace(/parameter21/,itemName);
		}
		else
		{
			palletlabel=palletlabel.replace(/parameter21/,'');
		}
		if((poName!=null)&&(poName!=""))
		{
			palletlabel=palletlabel.replace(/parameter22/,poName);
		}
		else
		{
			palletlabel=palletlabel.replace(/parameter22/,'');
		}
		if((username!=null)&&(username!=""))
		{
			palletlabel=palletlabel.replace(/parameter23/,username);
		}
		else
		{
			palletlabel=palletlabel.replace(/parameter23/,'');
		}

		if((recevieddate!=null)&&(recevieddate!=""))
		{
			palletlabel=palletlabel.replace(/parameter24/,recevieddate);
		}
		else
		{

			palletlabel=palletlabel.replace(/parameter24/,recevieddate);
		}
		if((skuname!=null)&&(skuname!=""))
		{
			palletlabel=palletlabel.replace(/parameter25/g,skuname);
		}
		else
		{
			palletlabel=palletlabel.replace(/parameter25/g,'');
		}

		if((itemqty!=null)&&(itemqty!=""))
		{
			palletlabel=palletlabel.replace(/parameter26/,itemqty);
		}
		else
		{
			palletlabel=palletlabel.replace(/parameter26/,'');
		}

		var print="F";
		var reprint="F"
			var refno="";
		var printername="";

		CreateLabelData(palletlabel,"ZebraLabel","","PALLETLABEL",print,reprint,"","",poName)
	}

}