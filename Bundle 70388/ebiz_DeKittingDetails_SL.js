/***************************************************************************
 		eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/ebiz_DeKittingDetails_SL.js,v $
 *     	   $Revision: 1.3.4.1.4.1.4.1.8.1 $
 *     	   $Date: 2015/09/21 14:02:16 $
 *     	   $Author: deepshikha $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_8 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_DeKittingDetails_SL.js,v $
 * Revision 1.3.4.1.4.1.4.1.8.1  2015/09/21 14:02:16  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.3.4.1.4.1.4.1  2013/03/01 14:34:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.3.4.1.4.1  2012/11/01 14:54:57  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.3.4.1  2012/09/03 13:56:34  schepuri
 * CASE201112/CR201113/LOG201121
 * added date stamp
 *
 * Revision 1.3  2011/07/21 06:23:57  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/

function DekittingDtlsSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		//create the Suitelet form
		var form = nlapiCreateForm('De-Kitting');

		var vdtkwono = request.getParameter('custparam_dtkwono');
		var vputwwono = request.getParameter('custparam_putwwono');

		var ItemDTKSubList = form.addSubList("custpage_parent_items", "list", "Parent Item");
		ItemDTKSubList.addField("custpage_parent_sku", "select", "Parent Item", 'item').setDisplayType('readonly');
		ItemDTKSubList.addField("custpage_parent_skustatus", "text", "Status");
		ItemDTKSubList.addField("custpage_parent_lp", "text", "LP #");
		ItemDTKSubList.addField("custpage_parent_location", "text", "Bin Location");
		ItemDTKSubList.addField("custpage_parent_expqty", "text", "Qty");
		ItemDTKSubList.addField("custpage_invrefno", "text", "invrefno").setDisplayType('hidden');
		ItemDTKSubList.addField("custpage_recid", "text", "recid").setDisplayType('hidden');

		var filters = new Array();

		nlapiLogExecution('DEBUG',"vdtkwono "+vdtkwono );

		filters[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'is', '12');
		filters[1] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', vdtkwono);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('name');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		columns[4] = new nlobjSearchColumn('custrecord_lpno');
		columns[5] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[6] = new nlobjSearchColumn('custrecord_sku');
		columns[7] = new nlobjSearchColumn('custrecord_invref_no');
		columns[8] = new nlobjSearchColumn('custrecord_sku_status');
		columns[9] = new nlobjSearchColumn('custrecord_packcode');
		columns[10] = new nlobjSearchColumn('custrecord_from_lp_no');

		var vprebizskuno, vprexpqty, vprebizcntrlno, vprlpno, vprbeginloc, vprsku, vprinvrefno, vprskustatus, vprpackcode, vprfromlp;
		var vprrecid;

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
		for (var i = 0; searchresults != null && i < searchresults.length; i++) {
			var searchresult = searchresults[i];
			vprebizskuno = searchresult.getValue('custrecord_ebiz_sku_no');
			vprexpqty = searchresult.getValue('custrecord_expe_qty');
			vprebizcntrlno = searchresult.getValue('custrecord_ebiz_cntrl_no');
			vprlpno = searchresult.getValue('custrecord_lpno');
			vprbeginloc = searchresult.getText('custrecord_actbeginloc');
			vprsku = searchresult.getValue('custrecord_sku');
			vprinvrefno = searchresult.getValue('custrecord_invref_no');
			vprskustatus = searchresult.getValue('custrecord_sku_status');
			vprpackcode = searchresult.getValue('custrecord_packcode');
			vprfromlp = searchresult.getValue('custrecord_from_lp_no');
			vprrecid = searchresult.getId();

			form.getSubList('custpage_parent_items').setLineItemValue('custpage_parent_sku', i + 1, vprebizskuno);
			form.getSubList('custpage_parent_items').setLineItemValue('custpage_parent_skustatus', i + 1, vprskustatus);
			form.getSubList('custpage_parent_items').setLineItemValue('custpage_parent_lp', i + 1, vprfromlp);
			form.getSubList('custpage_parent_items').setLineItemValue('custpage_parent_location', i + 1, vprbeginloc);
			form.getSubList('custpage_parent_items').setLineItemValue('custpage_parent_expqty', i + 1, vprexpqty);
			form.getSubList('custpage_parent_items').setLineItemValue('custpage_invrefno', i + 1, vprinvrefno);
			form.getSubList('custpage_parent_items').setLineItemValue('custpage_recid', i + 1, vprrecid);
		}

		//Adding  ItemSubList
		var ItemSubList = form.addSubList("custpage_member_items", "inlineeditor", "Member Items");
		ItemSubList.addField("custpage_deliveryord_sku", "select", "Component", "item");
		ItemSubList.addField("custpage_deliveryord_skustatus", "select", "Item Status", "customrecord_ebiznet_sku_status");
		ItemSubList.addField("custpage_deliveryord_packcode", "text", "Pack Code").setDefaultValue('1');
		ItemSubList.addField("custpage_deliveryord_lp", "text", "LP #");
		ItemSubList.addField("custpage_deliveryord_location", "select", "Bin Location", "customrecord_ebiznet_location");
		ItemSubList.addField("custpage_deliveryord_expqty", "text", "Qty");
		ItemSubList.addField("custpage_putwrecid", "text", "recid").setDisplayType('hidden');


		var filtersputw = new Array();

		filtersputw[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'is', '2');
		filtersputw[1] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', parseFloat(vputwwono));

		var columnsputw = new Array();
		columnsputw[0] = new nlobjSearchColumn('name');
		columnsputw[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columnsputw[2] = new nlobjSearchColumn('custrecord_expe_qty');
		columnsputw[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		columnsputw[4] = new nlobjSearchColumn('custrecord_lpno');
		columnsputw[5] = new nlobjSearchColumn('custrecord_actbeginloc');
		columnsputw[6] = new nlobjSearchColumn('custrecord_sku');
		columnsputw[7] = new nlobjSearchColumn('custrecord_invref_no');
		columnsputw[8] = new nlobjSearchColumn('custrecord_sku_status');
		columnsputw[9] = new nlobjSearchColumn('custrecord_packcode');
		columnsputw[10] = new nlobjSearchColumn('custrecord_from_lp_no');

		var vebizskuno, vexpqty, vebizcntrlno, vlpno, vbeginloc, vsku, vinvrefno, vskustatus, vpackcode, vfromlp;
		var vrecid;

		var searchresultsputw = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersputw, columnsputw);
		for (var i = 0; searchresultsputw != null && i < searchresultsputw.length; i++) {
			var searchresult = searchresultsputw[i];
			vebizskuno = searchresult.getValue('custrecord_ebiz_sku_no');
			vexpqty = searchresult.getValue('custrecord_expe_qty');
			vebizcntrlno = searchresult.getValue('custrecord_ebiz_cntrl_no');
			vlpno = searchresult.getValue('custrecord_lpno');
			vbeginloc = searchresult.getValue('custrecord_actbeginloc');
			vsku = searchresult.getValue('custrecord_sku');
			vinvrefno = searchresult.getValue('custrecord_invref_no');
			vskustatus = searchresult.getValue('custrecord_sku_status');
			vpackcode = searchresult.getValue('custrecord_packcode');
			vfromlp = searchresult.getValue('custrecord_from_lp_no');
			vrecid = searchresult.getId();
			nlapiLogExecution('ERROR', 'vskustatus', vskustatus);
			nlapiLogExecution('ERROR', 'vrecid', vrecid);
			form.getSubList('custpage_member_items').setLineItemValue('custpage_deliveryord_sku', i + 1, vebizskuno);
			form.getSubList('custpage_member_items').setLineItemValue('custpage_deliveryord_skustatus', i + 1, vskustatus);
			form.getSubList('custpage_member_items').setLineItemValue('custpage_deliveryord_packcode', i + 1, vpackcode);
			form.getSubList('custpage_member_items').setLineItemValue('custpage_deliveryord_lp', i + 1, vlpno);
			form.getSubList('custpage_member_items').setLineItemValue('custpage_deliveryord_location', i + 1, vbeginloc);
			form.getSubList('custpage_member_items').setLineItemValue('custpage_deliveryord_expqty', i + 1, parseFloat(vexpqty));
			form.getSubList('custpage_member_items').setLineItemValue('custpage_putwrecid', i + 1, vrecid);
			nlapiLogExecution('ERROR', 'vrecid1', vrecid);
		}

		form.addSubmitButton('Submit');
		response.writePage(form);
	}
	else //this is the POST block Added by ramana from else part.
	{
		var now = new Date();

		var a_p = "";
		var d = new Date();
		var curr_hour = now.getHours();
		if (curr_hour < 12) {
			a_p = "am";
		}
		else {
			a_p = "pm";
		}
		if (curr_hour == 0) {
			curr_hour = 12;
		}
		if (curr_hour > 12) {
			curr_hour = curr_hour - 12;
		}

		var curr_min = now.getMinutes();

		curr_min = curr_min + "";

		if (curr_min.length == 1) {
			curr_min = "0" + curr_min;
		}
		nlapiLogExecution("ERROR", "Time of Day ", (curr_hour + " : " + curr_min + " " + a_p));

		var parentTotalQty = 0;
		var vparentMainSKU = "";

		var lineCnt = request.getLineItemCount('custpage_parent_items');
		for (var k = 1; k <= lineCnt; k++) {
			var varsku = request.getLineItemValue('custpage_parent_items', 'custpage_parent_sku', k);
			var varqty = request.getLineItemValue('custpage_parent_items', 'custpage_parent_expqty', k);
			var varRecid = request.getLineItemValue('custpage_parent_items', 'custpage_invrefno', k);
			var vartaskRecid = request.getLineItemValue('custpage_parent_items', 'custpage_recid', k);
			vparentMainSKU = varsku;
			parentTotalQty = parseFloat(parentTotalQty) + parseFloat(varqty);
			var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', varRecid);
			var qty = transaction.getFieldValue('custrecord_ebiz_inv_qty');
			transaction.setFieldValue('custrecord_ebiz_qoh', (parseFloat(qty) - parseFloat(varqty)).toFixed(5));
			transaction.setFieldValue('custrecord_ebiz_inv_qty', (parseFloat(qty) - parseFloat(varqty)).toFixed(5));
			nlapiSubmitRecord(transaction, false, true);

			var opntsktran = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', vartaskRecid);
			opntsktran.setFieldValue('custrecord_wms_status_flag', '8'); //Statusflag='C'
			opntsktran.setFieldValue('custrecord_act_qty', parseFloat(varqty).toFixed(5));
			opntsktran.setFieldValue('custrecordact_end_date', DateStamp());
			opntsktran.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
			nlapiSubmitRecord(opntsktran, false, true);
		}

		var memberlineCnt = request.getLineItemCount('custpage_member_items');
		for (var k = 1; k <= memberlineCnt; k++) {
			var varmembersku = request.getLineItemValue('custpage_member_items', 'custpage_deliveryord_sku', k);
			var varmemberskutext = "";  //request.getLineItemText('custpage_member_items', 'custpage_deliveryord_sku', k);
			var varmemberskustatus = request.getLineItemValue('custpage_member_items', 'custpage_deliveryord_skustatus', k);
			var varmemberpackcode = request.getLineItemValue('custpage_member_items', 'custpage_deliveryord_packcode', k);
			var varmemberlp = request.getLineItemValue('custpage_member_items', 'custpage_deliveryord_lp', k);
			var varmemberbinloc = request.getLineItemValue('custpage_member_items', 'custpage_deliveryord_location', k);
			var varmemberqty = request.getLineItemValue('custpage_member_items', 'custpage_deliveryord_expqty', k);
			var varmemberrecid = request.getLineItemValue('custpage_member_items', 'custpage_putwrecid', k);
			nlapiLogExecution('ERROR', 'varmembersku', varmembersku);
			nlapiLogExecution('ERROR', 'varmemberqty', varmemberqty);
			nlapiLogExecution('ERROR', 'varmemberrecid', varmemberrecid);
			var opntsktran = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', varmemberrecid);
			opntsktran.setFieldValue('custrecord_wms_status_flag', '8'); //Statusflag='C'
			opntsktran.setFieldValue('custrecord_act_qty',parseFloat(varmemberqty).toFixed(5));
			opntsktran.setFieldValue('custrecord_actendloc', varmemberbinloc);
			opntsktran.setFieldValue('custrecordact_end_date', DateStamp());
			opntsktran.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
			var idl=  nlapiSubmitRecord(opntsktran, false, true);

			//Creating Inventory Record.
			var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');
			nlapiLogExecution('ERROR', 'CreATIN INVT  REc ', 'INVT');
			invtRec.setFieldValue('name', idl);
			invtRec.setFieldValue('custrecord_ebiz_inv_binloc', varmemberbinloc);
			invtRec.setFieldValue('custrecord_ebiz_inv_lp', varmemberlp);
			invtRec.setFieldValue('custrecord_ebiz_inv_sku', varmembersku);
			invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', varmemberskustatus);
			invtRec.setFieldValue('custrecord_ebiz_inv_packcode', parseFloat(varmemberpackcode));
			invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(varmemberqty).toFixed(5));
			invtRec.setFieldValue('custrecord_inv_ebizsku_no', varmembersku);
			invtRec.setFieldValue('custrecord_ebiz_qoh',parseFloat(varmemberqty).toFixed(5));
			invtRec.setFieldValue('custrecord_ebiz_itemdesc', varmemberskutext);
			invtRec.setFieldValue('custrecord_invttasktype', 2);
			invtRec.setFieldValue('custrecord_wms_inv_status_flag', 19); //Inventory S
			invtRec.setFieldValue('custrecord_ebiz_inv_account_no', 1);
			invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');

			nlapiLogExecution('ERROR', 'Before Submitting invtrecid', 'INVTRECORDS');
			var invtrecid = nlapiSubmitRecord(invtRec, false, true);
		}

		var outAssembly = nlapiCreateRecord('assemblyunbuild');
		outAssembly.setFieldValue('date', DateStamp());
		outAssembly.setFieldValue('location', '3');
		outAssembly.setFieldValue('item', vparentMainSKU);
		outAssembly.setFieldValue('quantity', parentTotalQty);						
		nlapiSubmitRecord(outAssembly);		  
		var form = nlapiCreateForm('De Kitting');
		var msg= form.addField('custpage_message', 'inlinehtml', null, null, null);
		msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Transaction saved sucessfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");

		response.writePage(form);
	}    
}



function getRecord(itemid){
	var filtersso = new Array();
	var socount=0;		

	filtersso[0] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', itemid);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');

	var searchresultsLine = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersso, columns);
	if(searchresultsLine !=null)
	{
		nlapiLogExecution('ERROR', 'SO count', searchresultsLine.length);
		socount=searchresultsLine.length;  
	}        

	return searchresultsLine;
}



function PickStrategieKittoOrder(item, itemfamily, itemgroup, avlqty){
	var actallocqty = 0;
	var Resultarray = new Array();
	nlapiLogExecution('ERROR', 'item', item);
	nlapiLogExecution('ERROR', 'itemgroup', itemgroup);
	nlapiLogExecution('ERROR', 'itemfamily', itemfamily);
	nlapiLogExecution('ERROR', 'avlqty', avlqty);

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebizskupickrul', null, 'anyof', ['@NONE@', item]);
	var k = 1;
	if (itemgroup != "" && itemgroup != null) {
		filters[k] = new nlobjSearchFilter('custrecord_ebizskugrouppickrul', null, 'anyof', ['@NONE@', itemgroup]);
		k = k + 1;
	}
	if (itemfamily != "" && itemfamily != null) {
		filters[k] = new nlobjSearchFilter('custrecord_ebizskufamilypickrul', null, 'anyof', ['@NONE@', itemfamily]);
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_ebizpickzonerul');
	columns[2] = new nlobjSearchColumn('custrecord_ebizpickmethod');
	columns[3] = new nlobjSearchColumn('custrecord_ebizsequencenopickrul');

	nlapiLogExecution('ERROR', 'Pick rule Fectching');
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters, columns);
	for (var i = 0; searchresults != null && i < searchresults.length; i++) {
		var searchresultpick = searchresults[i];
		var vpickzone = searchresultpick.getValue('custrecord_ebizpickzonerul');
		nlapiLogExecution('ERROR', 'PickZone', vpickzone);
		var filterszone = new Array();
		filterszone[0] = new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'is', vpickzone, null);

		var columnzone = new Array();
		columnzone[0] = new nlobjSearchColumn('custrecord_locgroup_no');

		var searchresultszone = nlapiSearchRecord('customrecord_zone_locgroup', null, filterszone, columnzone);
		nlapiLogExecution('ERROR', 'Loc Group Fetching');

		for (var j = 0; searchresultszone != null && j < searchresultszone.length; j++) {
			var searchresultzone = searchresultszone[j];
			var vlocgroupno = searchresultzone.getValue('custrecord_locgroup_no');

			nlapiLogExecution('ERROR', 'LocGroup' + searchresultzone.getValue('custrecord_locgroup_no'), searchresultzone.getText('custrecord_locgroup_no'));
			var filtersinvt = new Array();

			filtersinvt[0] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', item);
			filtersinvt[1] = new nlobjSearchFilter('custrecord_outboundinvlocgroupid', null, 'is', vlocgroupno);

			var columnsinvt = new Array();
			columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
			columnsinvt[1] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
			columnsinvt[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
			columnsinvt[3] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
			columnsinvt[4] = new nlobjSearchColumn('custrecord_ebiz_qoh');
			columnsinvt[5] = new nlobjSearchColumn('custrecord_pickseqno');
			columnsinvt[5].setSort();

			var searchresultsinvt = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);
			for (var l = 0; searchresultsinvt != null && l < searchresultsinvt.length; l++) {
				var searchresult = searchresultsinvt[l];
				var actqty = searchresult.getValue('custrecord_ebiz_qoh');
				var allocqty = searchresult.getValue('custrecord_ebiz_alloc_qty');
				var LP = searchresult.getValue('custrecord_ebiz_inv_lp');
				var vactLocation = searchresult.getValue('custrecord_ebiz_inv_binloc');
				var vactLocationtext = searchresult.getText('custrecord_ebiz_inv_binloc');
				var Recid = searchresult.getId();
				if (isNaN(allocqty)) {
					allocqty = 0;
				}
				if (allocqty == "") {
					allocqty = 0;
				}
				if (parseFloat(actqty) < 0) {
					actqty = 0;
				}
				nlapiLogExecution('ERROR', "allocqty: " + allocqty);
				nlapiLogExecution('ERROR', "LP: " + LP);

				var remainqty = actqty - allocqty;
				nlapiLogExecution('ERROR', 'remainqty' + remainqty);

				var cnfmqty = 0;
				nlapiLogExecution('ERROR', 'LOOP BEGIN');

				if (remainqty > 0) {
					nlapiLogExecution('ERROR', 'INTO LOOP');
					if ((avlqty - actallocqty) <= remainqty) {
						cnfmqty = avlqty - actallocqty;
						actallocqty = avlqty;
					}
					else {
						cnfmqty = remainqty;
						actallocqty = actallocqty + remainqty;
					}

					if (cnfmqty > 0) {
						//Resultarray
						var invtarray = new Array();
						invtarray[0] = cnfmqty;
						invtarray[1] = vactLocation;
						invtarray[2] = LP;
						invtarray[3] = Recid;
						invtarray[4] = item;
						invtarray[5] = avlqty;						
						nlapiLogExecution('ERROR','Pick cnfmqty',cnfmqty);
						Resultarray.push(invtarray);
					}
				}

				if ((avlqty - actallocqty) == 0) {
					nlapiLogExecution('ERROR', 'Into Break:');
					return Resultarray;
				}
			}
		}
	}
	return Resultarray;
}