/***************************************************************************
   eBizNET Solutions Inc  
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_bolreportPDF_SL.js,v $
 *     	   $Revision: 1.4.10.2.4.6.4.1 $
 *     	   $Date: 2015/10/20 08:09:09 $
 *     	   $Author: nneelam $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_43 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_bolreportPDF_SL.js,v $
 * Revision 1.4.10.2.4.6.4.1  2015/10/20 08:09:09  nneelam
 * case# 201415059
 *
 * Revision 1.4.10.2.4.6  2014/05/27 14:10:41  sponnaganti
 * case# 20148460
 * Stnd Bundle Issue fix
 *
 * Revision 1.4.10.2.4.5  2014/05/15 15:51:45  skavuri
 * Case # 20148409 SB Issue Fixed
 *
 * Revision 1.4.10.2.4.4  2013/11/12 05:53:40  vmandala
 * Case# 20124760
 * ClicktoShop sb Ffx
 *
 * Revision 1.4.10.2.4.3  2013/10/24 14:26:25  schepuri
 * 20125171
 *
 * Revision 1.4.10.2.4.2  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.4.10.2.4.1  2013/03/08 14:45:50  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.4.10.2  2012/11/01 14:55:01  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.4.10.1  2012/09/26 22:43:33  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue Fixes for FISK,BOOMBAH and TDG.
 *
 * Revision 1.4  2011/12/13 09:45:38  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.3  2011/10/05 11:55:39  schepuri
 * CASE201112/CR201113/LOG201121
 * BOL Report PDF
 *
 * Revision 1.2  2011/10/03 06:53:20  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.1  2011/09/30 14:26:00  schepuri
 * CASE201112/CR201113/LOG201121
 * BOL Report PDF
 *
 *
 *****************************************************************************/

function bolReportPDFSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		nlapiLogExecution('ERROR', 'sttrat','sttrat');	
		var form = nlapiCreateForm('BOL Report PDF');
		var vQbBol ="";
		var vfulfillmentordno ="";
		var vfullfillno="";
		var vWaveno ="";
		var ordno ="",trailerName,masterShipperNo,vshipdate;

		if(request.getParameter('custparam_ebiz_bolno')!=null && request.getParameter('custparam_ebiz_bolno')!="")
		{
			vQbBol = request.getParameter('custparam_ebiz_bolno');
		} 
		if(request.getParameter('custparam_fulfillmentordno')!=null && request.getParameter('custparam_fulfillmentordno')!="")
		{
			vfulfillmentordno = request.getParameter('custparam_fulfillmentordno');
		} 
		if(request.getParameter('custparam_wave')!=null && request.getParameter('custparam_wave')!="")
		{
			vWaveno = request.getParameter('custparam_wave');
		} 
		if(request.getParameter('custparam_ordno')!=null && request.getParameter('custparam_ordno')!="")
		{
			ordno = request.getParameter('custparam_ordno');
		} 
		if(request.getParameter('custparam_or')!=null && request.getParameter('custparam_or')!="")
		{
			vfullfillno = request.getParameter('custparam_or');
		} 
		if(request.getParameter('custpage_shipdate')!=null && request.getParameter('custpage_shipdate')!="")
		{
			vshipdate = request.getParameter('custpage_shipdate');
		}
		
		nlapiLogExecution('ERROR', 'vQbBol',vQbBol);
		nlapiLogExecution('ERROR', 'vfulfillmentordno',vfulfillmentordno);
		nlapiLogExecution('ERROR', 'vWaveno',vWaveno);
		nlapiLogExecution('ERROR', 'vfullfillno',vfullfillno);
		var trailer = getTrailerName(vQbBol);

		if(trailer != null && trailer!= "")
		{
			trailerName = trailer[0].getValue('name');
			masterShipperNo = trailer[0].getValue('custrecord_ebizmastershipper');
			nlapiLogExecution('ERROR', 'main masterShipperNo', masterShipperNo);
		}


		nlapiLogExecution('ERROR', 'main ordno', ordno);

		var companyname,companyid;//customerpo;
		var locationlist;
		var shiptoAddress1="",shiptoAddress2="",shiptocity="",shiptostate="",shiptocountry="",shiptocompany="",shiptozipcode="",shiptophone="",shiptocompany1="",shiptoso="",shiptocarrier="";
		var shipfromcity="",shipfromcountry="",shipfromzipcode="",shipfromaddress1="",shipfromaddress2="",shipfromphone="",shipfromstate="",shipfromcompname="",locationid="",locationidname="";
		var shipadd="";
		var shiptoTotAdd="";

		// Case# 20148409 starts
		var salesorderlist='';
		var trantype = nlapiLookupField('transaction', ordno, 'recordType');
			if(trantype=="transferorder")
				{
				 salesorderlist = nlapiLoadRecord('transferorder', ordno);
				}
			else
				{
				 salesorderlist = nlapiLoadRecord('salesorder', ordno); 
				}
		// Case# 20148409 ends
		shiptoso = salesorderlist.getFieldValue('tranid');
		//	shippingadd=salesorderlist.getFieldValue('shipaddress'); 
		// customerpono=salesorderlist.getFieldValue('otherrefnum');	
		shiptocompany=salesorderlist.getFieldValue('billaddressee');
		if(shiptocompany == null)
			shiptocompany = "";

		companyid=salesorderlist.getFieldValue('custbody_nswms_company');
		if(companyid == null)
			companyid = "";

		companyname=salesorderlist.getFieldText('custbody_nswms_company');
		if(companyname == null)
			companyname = "";

		shiptocarrier=salesorderlist.getFieldText('custbody_salesorder_carrier');
		if(shiptocarrier == null)
			shiptocarrier = "";

		locationid=salesorderlist.getFieldValue('location');	
		if(locationid == null)
			locationid = "";

		nlapiLogExecution('ERROR','locationinternalid ',locationid); 


		shiptoAddress1 = salesorderlist.getFieldValue('shipaddr1');
		shiptoAddress2 = salesorderlist.getFieldValue('shipaddr2');
		if(shiptoAddress1 == null)
		{
			shiptoAddress1 ="";
		}
		if(shiptoAddress2 == null)
		{
			shiptoAddress2 ="";
		}

		shiptoTotAdd = shiptoAddress1 +"  "+ shiptoAddress2;

		shiptocity = salesorderlist.getFieldValue('shipcity');
		if(shiptocity == null)
			shiptocity = "";

		shiptostate = salesorderlist.getFieldValue('shipstate');
		if(shiptostate == null)
			shiptostate = "";

		shiptocountry = salesorderlist.getFieldValue('shipcountry');
		if(shiptocountry == null)
			shiptocountry = "";

		shiptozipcode = salesorderlist.getFieldValue('shipzip');
		if(shiptozipcode == null)
			shiptozipcode = "";

		shiptophone = salesorderlist.getFieldValue('custbody_customer_phone');
		if(shiptophone == null)
			shiptophone = "";

		if(locationid != null && locationid != ""){
			var companylist = nlapiLoadRecord('location', locationid); 
			shipfromcompname = companylist.getFieldValue('name');
			if(shipfromcompname == null)
				shipfromcompname = "";


			shipfromcity=companylist.getFieldValue('city');
			if(shipfromcity == null)
				shipfromcity = "";


			shipfromcountry=companylist.getFieldText('country');
			if(shipfromcountry == null)
				shipfromcountry = "";

			shipfromstate=companylist.getFieldValue('state');
			if(shipfromstate == null)
				shipfromstate = "";

			shipfromaddress1=companylist.getFieldValue('addr1');
			shipfromaddress2=companylist.getFieldValue('addr2');

			if(shipfromaddress1 == null)
			{
				shipfromaddress1 ="";
			}
			if(shipfromaddress2 == null)
			{
				shipfromaddress2 ="";
			}

			shipadd = shipfromaddress1 +"  "+ shipfromaddress2;

			shipfromphone=companylist.getFieldValue('addrphone');
			if(shipfromphone == null)
				shipfromphone = "";

			shipfromzipcode=companylist.getFieldValue('zip');
			if(shipfromzipcode == null)
				shipfromzipcode = "";
		}


		nlapiLogExecution('ERROR', 'shiptoso', shiptoso);			
		nlapiLogExecution('ERROR', 'shiptoAddress1', shiptoAddress1);
		nlapiLogExecution('ERROR', 'shiptoAddress2', shiptoAddress2);
		nlapiLogExecution('ERROR', 'shiptocity', shiptocity);
		nlapiLogExecution('ERROR', 'shiptostate', shiptostate);
		nlapiLogExecution('ERROR', 'shiptocountry', shiptocountry);
		nlapiLogExecution('ERROR', 'companyname', companyname);
		nlapiLogExecution('ERROR', 'shiptoTotAdd', shiptoTotAdd);	
		nlapiLogExecution('ERROR', 'shiptozipcode', shiptozipcode);	
		nlapiLogExecution('ERROR', 'shiptocompany', shiptocompany);	
		nlapiLogExecution('ERROR', 'shiptocompany1', shiptocompany1);



		nlapiLogExecution('ERROR', 'compstate', shipfromstate);
		nlapiLogExecution('ERROR', 'companycity', shipfromcity);
		nlapiLogExecution('ERROR', 'compcountry', shipfromcountry);
		nlapiLogExecution('ERROR', 'compzipcode', shipfromzipcode);
		nlapiLogExecution('ERROR', 'compaddress1', shipfromaddress1);
		nlapiLogExecution('ERROR', 'compaddress2', shipfromaddress2);
		nlapiLogExecution('ERROR', 'compphone', shipfromphone);


		var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body  font-size=\"7\">\n";
		nlapiLogExecution('ERROR','xml0 ',"<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body font-size=\"18\">\n");

		var replaceChar =/\$|,|@|&|#|~|`|\%|\*|\^|\&|\+|\=|\-|\_|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|\./g;
		
		var sysdate=DateStamp();
		var vdate = sysdate.toString();
		nlapiLogExecution('ERROR','vdate ',vdate);
		nlapiLogExecution('ERROR','sysdate ',sysdate);

		var url;
		/*var ctx = nlapiGetContext();
		nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
		if (ctx.getEnvironment() == 'PRODUCTION') 
		{
			url = 'https://system.netsuite.com';			
		}
		else if (ctx.getEnvironment() == 'SANDBOX') 
		{
			url = 'https://system.sandbox.netsuite.com';				
		}*/
		nlapiLogExecution('ERROR', 'PDF URL',url);	

		var filefound = nlapiLoadFile('Images/LOGOCOMP.jpg'); 
		var finalimageurl = '';
		if (filefound) 
		{ 
			nlapiLogExecution('ERROR', 'Event', 'file;'+filefound.getId()); 
			var imageurl = filefound.getURL();
			nlapiLogExecution('ERROR','imageurl',imageurl);
			//finalimageurl = url + imageurl;//+';';
			finalimageurl = imageurl;//+';';
			//finalimageurl=finalimageurl+ '&expurl=T;';
			nlapiLogExecution('ERROR','imageurl',finalimageurl);
			finalimageurl=finalimageurl.replace(/&/g,"&amp;");

		} 
		else 
		{
			nlapiLogExecution('ERROR', 'Event', 'No file;');
		}

		var strxml ="<table style=\"border-right:2;border-left:2;border-top:2;\" width='100%'> ";
		strxml += "<tr align='center'>";
		strxml += "<td style=\"border-right:0.5 em\" width='30%'>";
		strxml += "<img src='" + finalimageurl + "'></img>";
		strxml += "</td>";

		strxml += "<td style=\"border-right:0.5 em\" width='40%'>";
		strxml +="<table align='center' >";
		strxml += "<tr align='center'  style=\"font-weight:bold\">";
		strxml += "<td>";
		strxml += "UNIFORM STRAIGHT BILL OF LADING" ;
		strxml += "</td>";
		strxml += "</tr>";
		strxml += "<tr align='center'  style=\"font-weight:bold\">";
		strxml += "<td align='center'>";
		strxml += "Original -- Not Negotiable" ;
		strxml += "</td>";
		strxml += "</tr>";
		strxml += "<tr align='center'  style=\"font-weight:bold\">";
		strxml += "<td align='center'>";
		strxml += "SUBJECT TO THE TERMS AND CONDITIONS" ;
		strxml += "</td>";
		strxml += "</tr>";
		strxml += "<tr align='center'  style=\"font-weight:bold\">";
		strxml += "<td align='center'>";
		strxml += "OF THE UNIFORM BILL OF LADING" ;
		strxml += "</td>";
		strxml += "</tr>";
		strxml += "<tr align='center'>";
		strxml += "<td align='center'>";
		strxml += "</td>";
		strxml += "</tr>";
		strxml += "<tr align='center'  style=\"font-weight:bold\">";
		strxml += "<td align='center'>";
		strxml += "CARRIER" ;
		strxml += "</td>";
		strxml += "</tr>";
		strxml += "<tr align='center'  >";
		strxml += "<td align='center' style=\"font-weight:bold;font-size:10\">";
		if(shiptocarrier != null)
			strxml += shiptocarrier ;
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		strxml += "</tr>";
		strxml += "</table>";
		strxml += "</td>";

		strxml += "<td width='30%'>";
		strxml +="<table width='100%' >";
		strxml += "<tr  style=\"border-bottom:0.5 em;padding-bottom:15px;\">";
		strxml += "<td >";
		strxml += "Date" ;
		strxml += "</td>";
		strxml += "<td style=\"font-weight:bold;font-size:11pt;\">";
		strxml += vdate ;
		strxml += "</td>";
		strxml += "</tr>";
		strxml += "<tr style=\"border-bottom:0.5 em;padding-bottom:15px;\">";
		strxml += "<td>";
		strxml += "Shipper's Bill of Lading #" ;
		strxml += "</td>";
		strxml += "<td style=\"font-weight:bold;font-size:11pt;\">";
		strxml += trailerName+'.'+vfulfillmentordno ;
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		strxml += "</tr>";
		strxml += "<tr style=\"border-bottom:0.5 em;padding-bottom:15px;\">";
		strxml += "<td>";
		strxml += "Order #" ;
		strxml += "</td>";
		strxml += "<td style=\"font-weight:bold;font-size:11pt;\">";
		strxml += vfulfillmentordno ;//order no
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		strxml += "</tr>";
		strxml += "<tr >";
		strxml += "<td>";
		strxml += "Shipper #" ;
		strxml += "</td>";
		strxml += "<td style=\"font-weight:bold;font-size:11pt;\">";
		strxml += masterShipperNo +'.'+trailerName ;//this is ebizordno change it ord no
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		strxml += "</tr>";
		strxml += "</table>";
		strxml += "</td>";




		strxml += "</tr></table>";

		strxml +="<table width='100%' style=\"border-right:2;border-left:2;\">"; 

		strxml += "<tr  style=\"background-color:black;color:white\">";
		strxml += "<td  width='50%'  >";
		strxml += "SHIPPER(from) ";
		strxml += "</td>" ;
		strxml += "<td  width='50%' >";
		strxml += "CONSIGNEE(to) ";
		strxml += "</td>";
//		strxml += "<td  width='45%'></td>";
//		strxml += "<td  width='5%'></td>";
		strxml += "</tr>";
		strxml += "<tr >";
		strxml += "<td  width='50%'  >";
		strxml +="<table   width='100%'>";
		strxml += "<tr align='center' >";
		strxml += "<td colspan='3' style=\"border-top:0.5 em;border-right:0.5 em\">";
		strxml += "Shipper ";
		strxml += "</td></tr>";
		strxml += "<tr style=\"font-weight:bold\">";
		strxml += "<td colspan='3' style=\"border-right:0.5 em\">";
		strxml += shipfromcompname;//name
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";

		strxml += "<tr align='center'  >";
		strxml += "<td colspan='3' style=\"border-top:0.5 em;border-right:0.5 em\">";
		strxml += "Address ";
		strxml += "</td></tr>";
		strxml += "<tr style=\"font-weight:bold\">";
		strxml += "<td  colspan='3' style=\"border-right:0.5 em\">";
		
		nlapiLogExecution('ERROR','shipfromaddress1 old',shipfromaddress1);
		nlapiLogExecution('ERROR','shipfromaddress2 old',shipfromaddress2);
		
		//case no 20125171� 

		if(shipfromaddress1 != null && shipfromaddress1 !="")
			shipfromaddress1=shipfromaddress1.replace(replaceChar,'');
		else
			shipfromaddress1="";
		
		if(shipfromaddress2 != null && shipfromaddress2 !="")
			shipfromaddress2=shipfromaddress2.replace(replaceChar,'');
		else
			shipfromaddress2="";
		
		
		strxml += shipfromaddress1+ " "+ shipfromaddress2;//name
		
		nlapiLogExecution('ERROR','shipfromaddress1 new',shipfromaddress1);
		nlapiLogExecution('ERROR','shipfromaddress2 new',shipfromaddress2);
		
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";

		strxml += "<tr align='center'  >";
		strxml += "<td  width='50%' style=\"border-top:0.5 em;border-right:0.5 em\">";
		strxml += "City ";
		strxml += "</td>";
		strxml += "<td  width='40%' style=\"border-top:0.5 em;border-right:0.5 em\">";
		strxml += "State ";
		strxml += "</td>";
		strxml += "<td  width='10%' style=\"border-top:0.5 em;border-right:0.5 em\">";
		strxml += "Zip ";
		strxml += "</td>";
		strxml += "</tr>";
		strxml += "<tr style=\"font-weight:bold\">";
		strxml += "<td style=\"border-right:0.5 em\">";
		strxml += shipfromcity;//city
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;</td>" ;
		strxml += "<td style=\"border-right:0.5 em\">";
		strxml += shipfromstate;//state
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;</td>" ;
		strxml += "<td style=\"border-right:0.5 em\">";
		strxml += shipfromzipcode;//zip
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;</td>" ;
		strxml += "</tr>";


		strxml += "<tr align='center'  >";
		strxml += "<td   style=\"border-top:0.5 em;border-right:0.5 em;\">";
		strxml += "Country ";
		strxml += "</td>";
		strxml += "<td colspan='2' style=\"border-top:0.5 em;border-right:0.5 em;\">";
		strxml += "Phone No ";
		strxml += "</td>";
		strxml += "</tr>";
		strxml += "<tr style=\"font-weight:bold\">";
		strxml += "<td   style=\"border-right:0.5 em;border-bottom:0.5 em;\">";
		strxml += shipfromcountry;//city
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;</td>" ;
		strxml += "<td colspan='2' style=\"border-right:0.5 em;border-bottom:0.5 em;\">";
		strxml += shipfromphone;//state
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;</td>" ;
		strxml += "</tr>";

		strxml += "</table>";
		strxml += "</td>";
		strxml += "<td  width='50%'  >";
		strxml +="<table  width='100%'>";
		strxml += "<tr align='center' >";
		strxml += "<td colspan='3' style=\"border-top:0.5 em;\">";
		strxml += "Consignee ";
		strxml += "</td></tr>";
		strxml += "<tr style=\"font-weight:bold\">";
		strxml += "<td colspan='3' >";
		strxml += shiptocompany;//name
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";

		strxml += "<tr align='center'  >";
		strxml += "<td colspan='3' style=\"border-top:0.5 em;\">";
		strxml += "Address ";
		strxml += "</td></tr>";
		strxml += "<tr style=\"font-weight:bold\">";
		strxml += "<td colspan='3' >";
		
		nlapiLogExecution('ERROR','shiptoTotAdd old',shiptoTotAdd);
		strxml += shiptoTotAdd;//name
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";

		strxml += "<tr align='center' >";
		strxml += "<td width='50%' style=\"border-top:0.5 em;border-right:0.5 em;\">";
		strxml += "City ";
		strxml += "</td>";
		strxml += "<td width='45%' style=\"border-top:0.5 em;border-right:0.5 em;\">";
		strxml += "State ";
		strxml += "</td>";
		strxml += "<td width='5%' style=\"border-top:0.5 em;\">";
		strxml += "Zip ";
		strxml += "</td>";
		strxml += "</tr>";
		strxml += "<tr style=\"font-weight:bold\">";
		strxml += "<td style=\"border-right:0.5 em\">";
		strxml += shiptocity;//city
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;</td>" ;
		strxml += "<td style=\"border-right:0.5 em\">";
		strxml += shiptostate;//state
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;</td>" ;
		strxml += "<td >";
		strxml += shiptozipcode;//zip
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;</td>" ;
		strxml += "</tr>";


		strxml += "<tr align='center'  >";
		strxml += "<td  style=\"border-top:0.5 em;border-right:0.5 em;\">";
		strxml += "Country ";
		strxml += "</td>";
		strxml += "<td colspan='2' style=\"border-top:0.5 em;\">";
		strxml += "Phone No ";
		strxml += "</td>";
		strxml += "</tr>";
		strxml += "<tr style=\"font-weight:bold\">";
		strxml += "<td   style=\"border-right:0.5 em;border-bottom:0.5 em;\">";
		strxml += shiptocountry;//city
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;</td>" ;
		strxml += "<td colspan='2' style=\"border-bottom:0.5 em;\">";
		strxml += shiptophone;//state
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;</td>" ;
		strxml += "</tr>";

		strxml += "</table>";
		strxml += "</td>";
		strxml += "</tr>";

		strxml += "<tr style=\"font-weight:bold\">";
		strxml += "<td colspan='2'  style=\"border-bottom:0.5 em\">" ;
		strxml += "Special Instructions ";
		strxml += "</td>";
		strxml += "</tr>";
		strxml += "<tr >";
		strxml += "<td colspan='2' style=\"border-left:0.5 em;border-right:0.5 em\">" ;
		strxml += "Received, subject to individually determined rates or contracts that have been agreed upon in writing between the carrier and shipper and to all applicable state and federal regulations, the property";
		strxml += "described below, in apparent good order, except as noted (contents of packages unknown) marked, consigned, and destined as shown hereon, which said carrier agrees to carry to destination, if on";
		strxml += "its route, or otherwise to deliver to another carrier on the route to destination. Every service to be performed hereunder shall be subject to all the conditions not prohibited by law, whether printed or";
		strxml += "written, herein contained, including the conditions otherwise referenced, which are hereby agreed to by the shipper and accepted for himself and his assigns. ";
		strxml += "</td>";
		strxml += "</tr>";
		strxml += "</table>";


		strxml += "<table width='100%'  style=\"background-color:white;padding-top:10px;border-right:2;border-left:2;\">";
		strxml =strxml+  "<tr style=\"font-weight:bold;border-bottom:1;background-color:black;color:white;\"  ><td  align='left'>";
		strxml += "No. Cases";
		strxml += "</td>";
		strxml += "<td  align='left' >";
		strxml += "NMFC ITEM #";
		strxml += "</td>";
		strxml += "<td  align='left'>";
		strxml += "Description";
		strxml += "</td>";
		strxml += "<td  align='left'>";
		strxml += "Cubes";
		strxml += "</td>";
		strxml += "<td  align='left' >";
		strxml += "Total WEIGHT IN LBS";
		strxml += "</td>";
		strxml += "</tr>";







		var ExpQty,Item,NoofCases="",NMFCitem,Desc,Cube,Weight,waveno,vordno,TotWt=0,TotCubes=0;Palletcount=0,TotnoofCases=0,shipLP="";

		// to fetch wave no against trailerno from open task
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_ebiz_trailer_no', null, 'is', trailerName);
		filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', 4);//ship task
		filters[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['7','10','28']);//ship task
		//filters[2] = new nlobjSearchFilter('custrecord_current_date', null, 'on', shipdate);
		//case# 20148460 starts
		if(vshipdate!=null&&vshipdate!=''&&vshipdate!='null')
		{
			filters.push(new nlobjSearchFilter('custrecord_current_date', null, 'onorbefore', vshipdate));
		}
		else
		{
			nlapiLogExecution('ERROR', 'DateStamp()',DateStamp());
			filters.push(new nlobjSearchFilter('custrecord_current_date', null, 'onorbefore', DateStamp()));	
		}
		//case# 20148460 ends
		
		
		if(vfullfillno !=null && vfullfillno !=""){
			filters.push(new nlobjSearchFilter('name', null, 'is', vfullfillno));
			}
		
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
		columns[1] = new nlobjSearchColumn('custrecord_ship_lp_no');

		var searchresultsWaveNo = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

		var PrevItems;
		var PrevtotCubeQty=0,PrevtotWtQty=0;
		var totCubeQty=0,totWtQty=0;
		var NMFCitemGroup="",NMFCitemFamily="",NMFCitem="";
		
		//case 20124760 start
		if(searchresultsWaveNo != null && searchresultsWaveNo != "")
		{
			var ContLP;
			var vPrevContLP;
			var ContCount=0;
			var p=0;
			nlapiLogExecution('ERROR', 'before if',ContCount);
			for (var i = 0; searchresultsWaveNo != null && i < searchresultsWaveNo.length; i++) 
			{
				waveno = searchresultsWaveNo[i].getValue('custrecord_ebiz_wave_no');
				shipLP = searchresultsWaveNo[i].getValue('custrecord_ship_lp_no');
				nlapiLogExecution('ERROR', 'waveno',waveno);
				nlapiLogExecution('ERROR', 'shipLP',shipLP);
				
				
				var filters1 =new Array();
				nlapiLogExecution('ERROR', 'beforeinsideopentask',ContCount);

				//to fetch details of detailed block of PDF either from closed task or from open task
				if(trailerName !=null && trailerName !=""){
					filters1.push(new nlobjSearchFilter('custrecord_ebiz_trailer_no', null, 'is', trailerName));}				
				filters1.push(new nlobjSearchFilter('custrecord_ship_lp_no', null, 'is', shipLP));
				if(vfullfillno !=null && vfullfillno !=""){
					filters1.push(new nlobjSearchFilter('name', null, 'is', vfullfillno));	}	
				//filters1.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3,4]));//Pick Task
				filters1.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Pick Task
				//filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['7','10','14','28']));//Build Ship Units and Trailer Load
				filters1.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['7','10','28']));//Build Ship Units and Trailer Load
				
			

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
				columns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
				columns[2] = new nlobjSearchColumn('custrecord_skudesc');
				columns[3] = new nlobjSearchColumn('custrecord_sku');
				columns[4] = new nlobjSearchColumn('custrecord_wms_status_flag');
				columns[5] = new nlobjSearchColumn('custrecord_totalcube');
				columns[6] = new nlobjSearchColumn('custrecord_total_weight');
				columns[7] = new nlobjSearchColumn('custrecord_expe_qty');
				columns[8] = new nlobjSearchColumn('custrecord_ebiz_order_no');
				columns[9] = new nlobjSearchColumn('name');
				columns[2].setSort(true);
				columns[0].setSort(true);
				//case# 20148460 starts 
				//if(vfullfillno !=null && vfullfillno !=""){
				var searchresultsopentask = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters1, columns);	
//}
				//case# 20148460 ends

				
				
					for (var n = 0; searchresultsopentask != null && n < searchresultsopentask.length; n++) 
					{
						
						nlapiLogExecution('ERROR', 'beforeinsideopentask1',searchresultsopentask.length);
						ContLP = searchresultsopentask[n].getValue('custrecord_container_lp_no');
						//var searchresultsclosedtasks = searchresultsclosedtask[i];
						ExpQty = searchresultsopentask[n].getValue('custrecord_expe_qty');
						Item = searchresultsopentask[n].getValue('custrecord_sku');
						Desc = searchresultsopentask[n].getValue('custrecord_skudesc');
						ordno = searchresultsopentask[n].getValue('custrecord_ebiz_order_no');
						fulfillmentordno = searchresultsopentask[n].getValue('name');
						var k = searchresultsopentask[n].getValue('custrecord_wms_status_flag');
						nlapiLogExecution('ERROR', 'into opentask CTExpQty',ExpQty);
						//nlapiLogExecution('ERROR', 'into closedtask CTItem',Item);
						//nlapiLogExecution('ERROR', 'into closedtask CTk',k);
						//nlapiLogExecution('ERROR', 'into closedtask CTDesc',Desc);
						//nlapiLogExecution('ERROR', 'into closedtask ordno',ordno);
						//nlapiLogExecution('ERROR', 'into closedtask fulfillmentordno',fulfillmentordno);
						nlapiLogExecution('ERROR', 'into opentask ContLP',ContLP);



						//to fetch NMFC code from item/tem group/item family for a item
						if(Item != null && Item != "")
						{
							var filters = new Array();
							filters[0] = new nlobjSearchFilter('Internalid', null, 'is', Item);

							var columns = new Array();
							columns[0] = new nlobjSearchColumn('custitem_ebiz_skunmfccode');
							columns[1] = new nlobjSearchColumn('custitem_item_group');
							columns[2] = new nlobjSearchColumn('custitem_item_family');

							var ItemNMFCCoderesult = nlapiSearchRecord('inventoryitem', null, filters, columns);
						}

						if(ItemNMFCCoderesult != null && ItemNMFCCoderesult != "")
						{
							NMFCitemGroup = ItemNMFCCoderesult[0].getValue('custitem_item_group');
							NMFCitemFamily = ItemNMFCCoderesult[0].getValue('custitem_item_family');
							NMFCitem = ItemNMFCCoderesult[0].getValue('custitem_ebiz_skunmfccode');
						}
						//nlapiLogExecution('ERROR', 'NMFCitem item',NMFCitem);
						///nlapiLogExecution('ERROR', 'NMFCitemGroup item',NMFCitemGroup);
						//nlapiLogExecution('ERROR', 'NMFCitemFamily item',NMFCitemFamily);
						if(NMFCitem == null || NMFCitem == "" )
						{
							if(NMFCitemGroup != null && NMFCitemGroup != "" )
							{
								var filters = new Array();
								filters[0] = new nlobjSearchFilter('Internalid', null, 'is', NMFCitemGroup);

								var columns = new Array();
								columns[0] = new nlobjSearchColumn('custrecord_ebiz_skugrp_nmfccode');

								var ItemGroupNMFCCoderesult = nlapiSearchRecord('customrecord_ebiznet_sku_group', null, filters, columns);

								if(ItemGroupNMFCCoderesult != null && ItemGroupNMFCCoderesult != "")
								{

									NMFCitem = ItemGroupNMFCCoderesult[0].getValue('custrecord_ebiz_skugrp_nmfccode');
								}
								//nlapiLogExecution('ERROR', 'NMFCitem itemgroup',NMFCitem);
							}


							else if(NMFCitemFamily != null && NMFCitemFamily != "")
							{
								var filters = new Array();
								filters[0] = new nlobjSearchFilter('Internalid', null, 'is', NMFCitemFamily);

								var columns = new Array();
								columns[0] = new nlobjSearchColumn('custrecord_ebiz_skufam_nmfccode');

								var ItemFamNMFCCoderesult = nlapiSearchRecord('customrecord_ebiznet_sku_family', null, filters, columns);
								if(ItemFamNMFCCoderesult != null && ItemFamNMFCCoderesult != "")
								{
									NMFCitem = ItemFamNMFCCoderesult[0].getValue('custrecord_ebiz_skufam_nmfccode');
								}
								//nlapiLogExecution('ERROR', 'NMFCitem itemfamily',NMFCitem);
							}
						}

						//to fetch totcube,totwt from LP master 

						var filters = new Array();
						var filters = new Array();
						if (shipLP !="" || shipLP !=null)							
						filters[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', shipLP);
						//filters[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_masterlp', null, 'is', shipLP);
						nlapiLogExecution('ERROR', 'custrecord_ebiz_lpmaster_masterlp',shipLP);
						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totcube');
						columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');

						var searchresultscubewt = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);

						if(searchresultscubewt != null && searchresultscubewt != "")
						{
							Cube = searchresultscubewt[0].getValue('custrecord_ebiz_lpmaster_totcube');
							if(Cube== null || Cube == "")
								Cube='0';
							Weight = searchresultscubewt[0].getValue('custrecord_ebiz_lpmaster_totwght');
							if(Weight== null || Weight == "")
								Weight='0';
						}

						nlapiLogExecution('ERROR', 'Cube',Cube);
						nlapiLogExecution('ERROR', 'Weight',Weight);

						//to fetch no of cases 

						if((Item!=null ||Item!='') && PrevItems!=Item)
						{
							nlapiLogExecution('ERROR', 'if Item is not null',Item);
							if(PrevItems!= null && PrevItems!="")
							{
								nlapiLogExecution('ERROR', 'PrevItems',PrevItems);
								nlapiLogExecution('ERROR', 'Item',Item);
								nlapiLogExecution('ERROR', 'totCubeQty',totCubeQty);
								nlapiLogExecution('ERROR', 'totWtQty',totWtQty);
								nlapiLogExecution('ERROR', 'ContCount',ContCount);



							/*//form.getSubList('custpage_items').setLineItemValue('custpage_noofcases', p + 1, parseFloat(ContCount).toString());
								form.getSubList('custpage_items').setLineItemValue('custpage_nmfcitemno', p + 1, NMFCitem);
								form.getSubList('custpage_items').setLineItemValue('custpage_desc',p+1,  PrevItems);
								form.getSubList('custpage_items').setLineItemValue('custpage_cubes', p + 1, PrevtotCubeQty);
								form.getSubList('custpage_items').setLineItemValue('custpage_totalwt', p + 1, PrevtotWtQty);*/
								p=p+1;

								PrevItems=Item;
								totCubeQty =parseFloat(Cube);
								totWtQty =parseFloat(Weight);
								PrevtotCubeQty =parseFloat(totCubeQty);
								PrevtotWtQty =parseFloat(totWtQty);
								nlapiLogExecution('ERROR', 'PrevtotCubeQty',PrevtotCubeQty);
								nlapiLogExecution('ERROR', 'PrevtotWtQty',PrevtotWtQty);
								ContCount=1;
								vPrevContLP=ContLP;	

							}
							else
							{
								PrevItems=Item;

								nlapiLogExecution('ERROR', ' ContLP111',ContLP);

								vPrevContLP=ContLP;
								ContCount++;
								if(Cube!=null && Cube!='')
								{
									totCubeQty =parseFloat(Cube);	
								}
								if(Weight!=null && Weight!='')
								{
									totWtQty =parseFloat(Weight);	
									nlapiLogExecution('ERROR', 'totWtQty1',totWtQty);
								}
								PrevtotCubeQty =parseFloat(totCubeQty);
								PrevtotWtQty =parseFloat(totWtQty);	
								nlapiLogExecution('ERROR', 'PrevtotCubeQty1',PrevtotCubeQty);
								nlapiLogExecution('ERROR', 'PrevtotWtQty1',PrevtotWtQty);
							}
						}
						else  if(Item!=null && Item!='')
						{
							nlapiLogExecution('ERROR', 'else if Item is not null vPrevContLP',vPrevContLP);
							nlapiLogExecution('ERROR', 'else if Item is not null ContLP',ContLP);
							if(vPrevContLP!=ContLP)
							{
								nlapiLogExecution('ERROR', 'else if Item is not null',Item);
								if(Cube!=null && Cube!='')
								{
									totCubeQty =parseFloat(totCubeQty)+parseFloat(Cube);	
								}
								if(Weight!=null && Weight!='')
								{
									totWtQty =parseFloat(totWtQty)+parseFloat(Weight);	
									nlapiLogExecution('ERROR', 'totWtQty2',totWtQty);

								}
								PrevtotCubeQty =parseFloat(totCubeQty);
								PrevtotWtQty =parseFloat(totWtQty);	
								nlapiLogExecution('ERROR', 'PrevtotCubeQty2',PrevtotCubeQty);
								nlapiLogExecution('ERROR', 'PrevtotWtQty2',PrevtotWtQty);
								vPrevContLP=ContLP;	
								ContCount++;
							}
						}

					//}have to check again
					nlapiLogExecution('ERROR', 'PrevItems',PrevItems);
					nlapiLogExecution('ERROR', 'ContCount',ContCount);
					Palletcount = Palletcount + parseFloat(ContCount) ;
					TotnoofCases = searchresultsWaveNo.length;
				
					TotWt = parseFloat(TotWt)+parseFloat(PrevtotWtQty);
					TotCubes = parseFloat(TotCubes)+parseFloat(PrevtotCubeQty);
					if(PrevItems!= null && PrevItems != "")
					{
						strxml =strxml+  "<tr ><td style=\"border-right:0.5 em;border-bottom:0.5 em\">";
						//strxml += vdono.split('.')[0];
						strxml += parseFloat(ContCount);
						strxml += "</td>";
						strxml += "<td style=\"border-right:0.5 em;border-bottom:0.5 em\">";
						strxml += NMFCitem;
						strxml += "</td>";
						strxml += "<td style=\"border-right:0.5 em;border-bottom:0.5 em\">";
						strxml += PrevItems;
						strxml += "</td>";
						strxml += "<td style=\"border-right:0.5 em;border-bottom:0.5 em\">";
						strxml += PrevtotCubeQty;
						strxml += "</td>";
						strxml += "<td style=\"border-bottom:0.5 em\">";
						strxml += PrevtotWtQty;
						strxml += "</td>";
						strxml += "</tr>";
					}
				}


			}
		}
		
		//case 20124760 end
		if (searchresultsopentask=='' || searchresultsopentask==null){
		if(searchresultsWaveNo!= null && searchresultsWaveNo!= "")
		{
			var ContLP;
			var vPrevContLP;
			var ContCount=0;
			nlapiLogExecution('ERROR', 'before if',ContCount);
			for (var i = 0; searchresultsWaveNo != null && i < searchresultsWaveNo.length; i++) 
			{
				waveno = searchresultsWaveNo[i].getValue('custrecord_ebiz_wave_no');
				shipLP = searchresultsWaveNo[i].getValue('custrecord_ship_lp_no');
				nlapiLogExecution('ERROR', 'wavenofromsearchrecord',waveno);
				nlapiLogExecution('ERROR', 'shipLPfromsearchrecord',shipLP);

				//to fetch details of detailed block of PDF either from closed task or from open task
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_ebiztask_ship_lp_no', null, 'is', shipLP);
				filters[1] = new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', [3]);
					//filters[2] = new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag', null, 'anyof', ['7','10','14']);
					filters[2] = new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag', null, 'anyof', ['7','10']);


				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_contlp_no');
				columns[1] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_sku_no');
				columns[2] = new nlobjSearchColumn('custrecord_ebiztask_skudesc');
				columns[3] = new nlobjSearchColumn('custrecord_ebiztask_sku');
				columns[4] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_trailer_no');
				columns[5] = new nlobjSearchColumn('custrecord_ebiztask_wms_status_flag');
				columns[6] = new nlobjSearchColumn('custrecord_ebiztask_totalcube');
				columns[7] = new nlobjSearchColumn('custrecord_ebiztask_total_weight');
				columns[8] = new nlobjSearchColumn('custrecord_ebiztask_expe_qty');
				columns[9] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_order_no');
				columns[10] = new nlobjSearchColumn('name');
				columns[2].setSort(true);
				columns[0].setSort(true);

				var searchresultsclosedtask = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filters, columns);


				nlapiLogExecution('ERROR', 'into closedtask','else');
				if (searchresultsclosedtask != null && searchresultsclosedtask != "") {
					for (var n = 0; searchresultsclosedtask != null && n < searchresultsclosedtask.length; n++) 
					{
						//var searchresultsclosedtasks = searchresultsclosedtask[i];
						ContLP = searchresultsclosedtask[n].getValue('custrecord_ebiztask_ebiz_contlp_no');
						ExpQty = searchresultsclosedtask[n].getValue('custrecord_ebiztask_expe_qty');
						Item = searchresultsclosedtask[n].getValue('custrecord_ebiztask_sku');
						Desc = searchresultsclosedtask[n].getValue('custrecord_ebiztask_sku');
						vordno = searchresultsclosedtask[n].getValue('custrecord_ebiztask_ebiz_order_no');

						nlapiLogExecution('ERROR', 'into closedtask CTExpQty',ExpQty);
						//nlapiLogExecution('ERROR', 'into closedtask CTItem',Item);
						//nlapiLogExecution('ERROR', 'into closedtask CTNMFCitem',NMFCitem);
						//nlapiLogExecution('ERROR', 'into closedtask CTDesc',Desc);
						//nlapiLogExecution('ERROR', 'into closedtask CTCube',Cube);
						//nlapiLogExecution('ERROR', 'into closedtask CTWeight',Weight);
						//nlapiLogExecution('ERROR', 'into closedtask ordno',ordno);
						nlapiLogExecution('ERROR', 'into closedtask ContLP',ContLP);

						//to fetch NMFC item

						if(Item != null && Item != "")
						{
							var filters = new Array();
							filters[0] = new nlobjSearchFilter('Internalid', null, 'is', Item);

							var columns = new Array();
							columns[0] = new nlobjSearchColumn('custitem_ebiz_skunmfccode');
							columns[1] = new nlobjSearchColumn('custitem_item_group');
							columns[2] = new nlobjSearchColumn('custitem_item_family');

							var ItemNMFCCoderesult = nlapiSearchRecord('inventoryitem', null, filters, columns);
						}

						if(ItemNMFCCoderesult != null && ItemNMFCCoderesult != "")
						{
							NMFCitemGroup = ItemNMFCCoderesult[0].getValue('custitem_item_group');
							NMFCitemFamily = ItemNMFCCoderesult[0].getValue('custitem_item_family');
							NMFCitem = ItemNMFCCoderesult[0].getValue('custitem_ebiz_skunmfccode');
						}
						//nlapiLogExecution('ERROR', 'NMFCitem item',NMFCitem);
						//nlapiLogExecution('ERROR', 'NMFCitemGroup item',NMFCitemGroup);
						//nlapiLogExecution('ERROR', 'NMFCitemFamily item',NMFCitemFamily);
						if(NMFCitem == null || NMFCitem == "")
						{

							if(NMFCitemGroup != null && NMFCitemGroup != "" )
							{
								var filters = new Array();
								filters[0] = new nlobjSearchFilter('Internalid', null, 'is', NMFCitemGroup);

								var columns = new Array();
								columns[0] = new nlobjSearchColumn('custrecord_ebiz_skugrp_nmfccode');

								var ItemGroupNMFCCoderesult = nlapiSearchRecord('customrecord_ebiznet_sku_group', null, filters, columns);

								if(ItemGroupNMFCCoderesult != null && ItemGroupNMFCCoderesult != "")
								{

									NMFCitem = ItemGroupNMFCCoderesult[0].getValue('custrecord_ebiz_skugrp_nmfccode');
								}
								nlapiLogExecution('ERROR', 'NMFCitem itemgroup',NMFCitem);
							}


							else if(NMFCitemFamily != null && NMFCitemFamily != "")
							{
								var filters = new Array();
								filters[0] = new nlobjSearchFilter('Internalid', null, 'is', NMFCitemFamily);

								var columns = new Array();
								columns[0] = new nlobjSearchColumn('custrecord_ebiz_skufam_nmfccode');

								var ItemFamNMFCCoderesult = nlapiSearchRecord('customrecord_ebiznet_sku_family', null, filters, columns);
								if(ItemFamNMFCCoderesult != null && ItemFamNMFCCoderesult != "")
								{
									NMFCitem = ItemFamNMFCCoderesult[0].getValue('custrecord_ebiz_skufam_nmfccode');
								}
								nlapiLogExecution('ERROR', 'NMFCitem itemfamily',NMFCitem);
							}
						}

						//to fetch totcube,totwt from LP master 

						var filters = new Array();
						//filters[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', shipLP);
						filters[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_masterlp', null, 'is', shipLP);

						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totcube');
						columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');

						var searchresultscubewt = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);

						if(searchresultscubewt != null && searchresultscubewt != "")
						{

							Cube = searchresultscubewt[0].getValue('custrecord_ebiz_lpmaster_totcube');
							if(Cube== null || Cube == "")
								Cube='0';
							Weight = searchresultscubewt[0].getValue('custrecord_ebiz_lpmaster_totwght');
							if(Weight== null || Weight == "")
								Weight='0';
						}
						nlapiLogExecution('ERROR', 'Cube',Cube);
						nlapiLogExecution('ERROR', 'Weight',Weight);



						//to fetch no of cases 

						if((Item!=null ||Item!='') && PrevItems!=Item)
						{
							nlapiLogExecution('ERROR', 'if Item is not null',Item);
							if(PrevItems!= null && PrevItems!="")
							{
								nlapiLogExecution('ERROR', 'PrevItems',PrevItems);
								nlapiLogExecution('ERROR', 'Item',Item);
								nlapiLogExecution('ERROR', 'totCubeQty',totCubeQty);
								nlapiLogExecution('ERROR', 'totWtQty',totWtQty);
								nlapiLogExecution('ERROR', 'ContCount',ContCount);

								/*
								strxml =strxml+  "<tr ><td style=\"border-right:0.5 em;border-bottom:0.5 em\">";
								//strxml += vdono.split('.')[0];
								strxml += parseFloat(ContCount);
								strxml += "</td>";
								strxml += "<td style=\"border-right:0.5 em;border-bottom:0.5 em\">";
								strxml += NMFCitem;
								strxml += "</td>";
								strxml += "<td style=\"border-right:0.5 em;border-bottom:0.5 em\">";
								strxml += PrevItems;
								strxml += "</td>";
								strxml += "<td style=\"border-right:0.5 em;border-bottom:0.5 em\">";
								strxml += PrevtotCubeQty;
								strxml += "</td>";
								strxml += "<td style=\"border-bottom:0.5 em\">";
								strxml += PrevtotWtQty;
								strxml += "</td>";
								strxml += "</tr>";
								 */




								PrevItems=Item;
								totCubeQty =parseFloat(Cube);
								totWtQty =parseFloat(Weight);
								PrevtotCubeQty =parseFloat(totCubeQty);
								PrevtotWtQty =parseFloat(totWtQty);
								nlapiLogExecution('ERROR', 'PrevtotCubeQty',PrevtotCubeQty);
								nlapiLogExecution('ERROR', 'PrevtotWtQty',PrevtotWtQty);
								ContCount=1;
								vPrevContLP=ContLP;	

							}
							else
							{
								PrevItems=Item;

								nlapiLogExecution('ERROR', ' ContLP111',ContLP);

								vPrevContLP=ContLP;
								ContCount++;
								if(Cube!=null && Cube!='')
								{
									totCubeQty =parseFloat(Cube);	
								}
								if(Weight!=null && Weight!='')
								{
									totWtQty =parseFloat(Weight);	
									nlapiLogExecution('ERROR', 'totWtQty1',totWtQty);
								}
								PrevtotCubeQty =parseFloat(totCubeQty);
								PrevtotWtQty =parseFloat(totWtQty);	
								nlapiLogExecution('ERROR', 'PrevtotCubeQty1',PrevtotCubeQty);
								nlapiLogExecution('ERROR', 'PrevtotWtQty1',PrevtotWtQty);
							}
						}
						else  if(Item!=null && Item!='')
						{
							nlapiLogExecution('ERROR', 'else if Item is not null vPrevContLP',vPrevContLP);
							nlapiLogExecution('ERROR', 'else if Item is not null ContLP',ContLP);
							if(vPrevContLP!=ContLP)
							{
								nlapiLogExecution('ERROR', 'else if Item is not null',Item);
								if(Cube!=null && Cube!='')
								{
									totCubeQty =parseFloat(totCubeQty)+parseFloat(Cube);	
								}
								if(Weight!=null && Weight!='')
								{
									totWtQty =parseFloat(totWtQty)+parseFloat(Weight);	
									nlapiLogExecution('ERROR', 'totWtQty2',totWtQty);

								}
								PrevtotCubeQty =parseFloat(totCubeQty);
								PrevtotWtQty =parseFloat(totWtQty);	
								nlapiLogExecution('ERROR', 'PrevtotCubeQty2',PrevtotCubeQty);
								nlapiLogExecution('ERROR', 'PrevtotWtQty2',PrevtotWtQty);
								vPrevContLP=ContLP;	
								ContCount++;
							}
						}












					}

					Palletcount = Palletcount + parseFloat(ContCount) ;
					TotnoofCases = searchresultsWaveNo.length;
					TotWt = parseFloat(TotWt)+parseFloat(PrevtotWtQty);
					TotCubes = parseFloat(TotCubes)+parseFloat(PrevtotCubeQty);


					if(PrevItems!= null && PrevItems != "")
					{

						strxml =strxml+  "<tr ><td style=\"border-right:0.5 em;border-bottom:0.5 em\">";
						//strxml += vdono.split('.')[0];
						strxml += parseFloat(ContCount);
						strxml += "</td>";
						strxml += "<td style=\"border-right:0.5 em;border-bottom:0.5 em\">";
						strxml += NMFCitem;
						strxml += "</td>";
						strxml += "<td style=\"border-right:0.5 em;border-bottom:0.5 em\">";
						strxml += PrevItems;
						strxml += "</td>";
						strxml += "<td style=\"border-right:0.5 em;border-bottom:0.5 em\">";
						strxml += PrevtotCubeQty;
						strxml += "</td>";
						strxml += "<td style=\"border-bottom:0.5 em\">";
						strxml += PrevtotWtQty;
						strxml += "</td>";
						strxml += "</tr>";
					}


				}
			}
		}
	}
		
		nlapiLogExecution('ERROR', 'in saleslist vebizOrdNo',PrevtotWtQty );

		strxml += "<tr style=\"border-right:0.5 em;font-weight:bold\"  > <td colspan='4' >" ;
		strxml += "Pallet Weight";
		strxml += "</td>";
		strxml += "<td >" ;
		strxml += "&nbsp;";
		strxml += "</td >" ;
		strxml += " </tr>";
		strxml += "<tr style=\"border-top:2\"  > <td colspan='5' >" ;
		strxml += "</td >" ;
		strxml += " </tr>";

		strxml =strxml+  "<tr style=\"font-weight:bold\"  ><td  align='left' colspan='5'>";
		strxml += "<table width='100%' style=\"background-color:white\">";
		strxml =strxml+  "<tr style=\"font-weight:bold\"  ><td  align='left'>";
		strxml += "TOTAL HANDLING UNIT :       ";
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		strxml += "<td  align='left' >";
		strxml += "Total Pieces       " ;
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		strxml += "<td  align='left'>";
		strxml += "Total Pallets      ";
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		strxml += "<td  align='left' >";
		strxml += "Total Cubes     ";
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		strxml += "<td  align='left'>";
		strxml += "Total weight      ";
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		strxml += "</tr>";
		strxml =strxml+  "<tr style=\"font-weight:bold\"  ><td  align='left'>";
		strxml += "</td>";
		strxml += "<td  align='left' >";
		strxml += TotnoofCases;
		strxml += "</td>";
		strxml += "<td  align='left'>";
		strxml += Palletcount;
		strxml += "</td>";
		strxml += "<td  align='left' >";
		strxml += TotCubes;
		strxml += "</td>";
		strxml += "<td  align='left'>";
		strxml += TotWt;
		strxml += "</td>";
		strxml += "</tr>";

		strxml += "</table>";
		strxml += "</td>";
		strxml += "</tr>";






		strxml += "<tr style=\"border-right:0.5 em;font-weight:bold;color:white;background-color:black;font-size:7;\">";
		strxml += "<td colspan='2'></td>";
		strxml += "<td colspan='3'>" ;
		strxml += "<span >* Mark 'X' IN THE HM COLUMN TO DESIGNATE HAZARDOUS MATERIALS AS DEFINED IN DOT REGULATIONS.</span>";
		strxml += "</td>";
		strxml += "</tr>";
		strxml =strxml+"</table>";	


		strxml += "<table  width='100%' style=\"background-color:white;border-right:2;border-left:2;border-bottom:2\">";

		//start of first block

		strxml =strxml+  "<tr  ><td  align='left' width='20%' style=\"border-right:0.5 em\">";
		strxml += " NOTE(1) Where the rate and carrier's liability for loss or damage may be dependent on value, shippers must state specifically in writing the agreed or declared value of the property as follows: The agreed or declared value of the property is specifically stated by the shipper to be not exceeding _____________________ per _______________________. ";
		strxml += "</td>";


		strxml += "<td colspan='4' width='80%'>" ;

		strxml += "<table style=\"background-color:white\">";
		strxml =strxml+  "<tr style=\"font-weight:bold\"  ><td  align='center' >";
		strxml += "FOR FREIGHT COLLECT SHIPMENTS";
		strxml += "</td>";
		strxml += "</tr>";

		strxml =strxml+  "<tr  ><td  align='left'>";
		strxml += "Subject to Section 7 of conditions of applicable bill of lading. If this shipment is to be delivered to the consignee, without";
		strxml += "recourse on the consignor, the consignor shall sign the following statement. The carrier may decline to make delivery of";
		strxml += "this shipment without payment of freight and all other lawful charges.";
		strxml += "</td>";
		strxml += "</tr>";

		strxml =strxml+  "<tr ><td  >";
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		strxml += "</tr>";

		strxml =strxml+  "<tr style=\"font-weight:bold\"  ><td  align='left'>";
		strxml += "Consignor Signature: _________________________________________";
		strxml += "</td>";
		strxml += "</tr>";

		strxml += "</table>";
		strxml += "</td>";


		strxml += "</tr>";
		//end of first block

		//start of second block
		strxml =strxml+  "<tr ><td  align='left' width='20%' style=\"border-right:0.5 em\">";
		strxml += "NOTE(2) Liability Limitations for loss or damage on this shipment shall be applicable as provided by contract or in the current National Motor Freight Classification, STB NMF 100 series, or this carrier's governing tariffs. In no event shall carrier's liability exceed $100,000 per incident. ";
		strxml += "</td>";

		strxml += "<td width='80%' colspan='4'>" ;
		strxml += "<table style=\"background-color:white\" >";
		strxml =strxml+  "<tr style=\"font-weight:bold;color:white;background-color:black\"  ><td  align='center'>";
		strxml += "SHIPPER CERTIFICATION";
		strxml += "</td>";
		strxml += "</tr>";

		strxml =strxml+  "<tr ><td  align='center'>";
		strxml += "This is to certify that the above named materials are properly classified, described, packaged, marked and labeled, and are";
		strxml += "in the proper condition for transportation according to the applicable regulations of the Department of Transportation.";
		strxml += "</td>";
		strxml += "</tr>";

		strxml =strxml+  "<tr ><td  align='center'>";
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		strxml += "</tr>";

		strxml =strxml+  "<tr style=\"font-weight:bold\"  ><td  align='left'>";
		strxml += "Shipper Signature: _________________   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;       Date: _________________";
		strxml += "</td>";
		strxml += "</tr>";

		strxml += "</table>";
		strxml += "</td>";
		strxml += "</tr>";

		//end of second block
		//start of third block

		strxml =strxml+  "<tr ><td  align='left' width='20%' style=\"border-right:0.5 em\">";
		strxml += "NOTE(3) Commodities requiring special or additional care or attention in handling or stowing must be so marked and packaged as to ensure safe transportation with ordinary care. See Sec.2(e) of NMFC Item 360.";
		strxml += "</td>";

		strxml += "<td width='80%' colspan='4'>" ;
		strxml += "<table >";
		strxml =strxml+  "<tr style=\"font-weight:bold;color:white;background-color:black\"  ><td  align='center'>";
		strxml += "CARRIER CERTIFICATION";
		strxml += "</td>";
		strxml += "</tr>";

		strxml =strxml+  "<tr ><td  align='center'>";
		strxml += "Carrier acknowledges receipt of packages and required placards. Carrier certifies emergency response information was";
		strxml += "made available and/or carrier has the DOT emergency response guidebook or equivalent document in the vehicle.";
		strxml += "</td>";
		strxml += "</tr>";


		strxml += "</table>";
		strxml += "</td>";
		strxml += "</tr>";
		//end of third block


		strxml =strxml+  "<tr  style=\"font-weight:bold\"  >";
		strxml += "<td width='20%' style=\"border-right:0.5 em\">" ;
		strxml += "&nbsp;</td >" ;
		strxml += "<td width='10%' style=\"border-right:0.5 em;border-bottom:0.5 em;border-top:0.5 em\" >" ;
		strxml += "Single Shipment";
		strxml += "</td>";
		strxml += "<td width='10%' style=\"border-right:0.5 em;border-bottom:0.5 em;border-top:0.5 em\"  >" ;
		strxml += "DATE";
		strxml += "</td >" ;
		strxml += "<td  width='40%' style=\"border-right:0.5 em;border-bottom:0.5 em;border-top:0.5 em\" >" ;
		strxml += "DRIVER/EMPLOYEE NUMBER OR SIGNATURE";
		strxml += "</td >" ;
		strxml += "<td width='20%' style=\"border-bottom:0.5 em;border-top:0.5 em\" >" ;
		strxml += "TRAILER # " +trailerName;
		strxml += "</td >" ;
		strxml += "</tr>";

		strxml =strxml+  "<tr style=\"font-weight:bold\"  >";
		strxml += "<td width='20%' style=\"border-right:0.5 em\">" ;
		strxml += "&nbsp;</td >" ;
		strxml += "<td width='10%' style=\"border-right:0.5 em;border-bottom:0.5 em\">" ;
		strxml += "Circle One";
		strxml += "</td >";
		strxml += "<td width='70%' colspan='3'>" ;
		strxml += "</td >" ;
		strxml += "</tr>";

		strxml =strxml+  "<tr style=\"font-weight:bold\"  >";
		strxml += "<td width='20%' style=\"border-right:0.5 em\">" ;
		strxml += "&nbsp;</td >" ;
		strxml += "<td width='10%' style=\"border-right:0.5 em\" >" ;
		strxml += "Y";
		strxml += "</td >";
		strxml += "<td width='70%' colspan='3'>" ;
		strxml += "</td >" ;
		strxml += "</tr>";

		strxml =strxml+  "<tr style=\"font-weight:bold\"  >";
		strxml += "<td width='20%' style=\"border-right:0.5 em\">" ;
		strxml += "&nbsp;</td >" ;
		strxml += "<td width='10%' style=\"border-right:1;border-top:0.5 em\">" ;
		strxml += "N";
		strxml += "</td >";
		strxml += "<td width='70%' colspan='3'>" ;
		strxml += "</td >" ;
		strxml += "</tr>";


		strxml =strxml+"</table>";
		strxml =strxml+ "\n</body>\n</pdf>";		
		//nlapiLogExecution('ERROR', 'strxml', strxml);
		xml=xml +strxml;
		//nlapiLogExecution('ERROR','XML',xml);

		var file = nlapiXMLToPDF(xml);	
		response.setContentType('PDF','BOLReport.pdf');
		response.write( file.getValue() );
	}
	else //this is the POST block
	{

	}
}

function SalesOrderList(ordno) {
	//nlapiLogExecution('ERROR', 'in vQbBol',+vQbBol);
	nlapiLogExecution('ERROR', 'in saleslist vebizOrdNo', ordno);
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('Internalid', null, 'is', ordno);
	filters[1] = new nlobjSearchFilter('mainline', null, 'is', 'T');
	var columns = new Array();
	//columns[0] = new nlobjSearchColumn('otherrefnum');
	columns[0] = new nlobjSearchColumn('custbody_nswms_company');//company id
	columns[1] = new nlobjSearchColumn('shipaddress1');
	columns[2] = new nlobjSearchColumn('shipaddress2');
	columns[3] = new nlobjSearchColumn('shipcity');
	columns[4] = new nlobjSearchColumn('shipstate');
	columns[5] = new nlobjSearchColumn('shipcountry');
	columns[6] = new nlobjSearchColumn('entity');
	columns[7] = new nlobjSearchColumn('shipaddressee');
	columns[8] = new nlobjSearchColumn('cczipcode');
	columns[9] = new nlobjSearchColumn('shipaddress');//total address
	columns[10] = new nlobjSearchColumn('shipzip');
	columns[11] = new nlobjSearchColumn('billphone');//?
	columns[12] = new nlobjSearchColumn('shipphone');
	columns[13] = new nlobjSearchColumn('tranid');
	columns[14] = new nlobjSearchColumn('shipcarrier');
	columns[15] = new nlobjSearchColumn('location');
	var salesorderlist = nlapiSearchRecord('salesorder', null, filters, columns);

	return salesorderlist ;
}





function GetLocation(locationid)
{nlapiLogExecution('ERROR', 'locationid1', locationid);
var filtername = new Array();
filtername [0] = new nlobjSearchFilter('Internalid', null, 'is', locationid);
var columnname= new Array();
columnname[0] = new nlobjSearchColumn('name');
columnname[1] = new nlobjSearchColumn('state');
columnname[2] = new nlobjSearchColumn('addr1');
columnname[3] = new nlobjSearchColumn('addr2');
columnname[4] = new nlobjSearchColumn('city');
columnname[5] = new nlobjSearchColumn('country');
columnname[6] = new nlobjSearchColumn('state');
columnname[7] = new nlobjSearchColumn('zip');

var locationlist= nlapiSearchRecord('location', null, filtername , columnname);
nlapiLogExecution('ERROR', 'locationlist.length11', locationlist.length);
return locationlist;

}


function getTrailerName(trailerNo){

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('Internalid', null, 'is', trailerNo);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_ebizmastershipper');

	var trailer = nlapiSearchRecord('customrecord_ebiznet_trailer', null, filters, columns);

	return trailer ;

}


