/***************************************************************************
 eBizNET Solutions Inc 
�����������������������    
 ****************************************************************************
 *
 *� $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/ebiz_AdjustInventoryMain_SL.js,v $
 *� $Revision: 1.19.2.9.4.8.2.26.2.3 $
 *� $Date: 2015/11/16 15:36:50 $
 *� $Author: schepuri $
 *� $Name: t_WMS_2015_2_StdBundle_1_152 $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_AdjustInventoryMain_SL.js,v $
 *� Revision 1.19.2.9.4.8.2.26.2.3  2015/11/16 15:36:50  schepuri
 *� case# 201415195
 *�
 *� Revision 1.19.2.9.4.8.2.26.2.2  2015/11/09 14:42:39  snimmakayala
 *� 201415509
 *� 2015,2 Issues
 *�
 *� Revision 1.19.2.9.4.8.2.26.2.1  2015/09/21 13:08:47  deepshikha
 *� 2015.2 issueFix
 *� 201414466
 *�
 *� Revision 1.19.2.9.4.8.2.26  2015/04/13 09:25:57  rrpulicherla
 *� Case#201412277
 *�
 *� Revision 1.19.2.9.4.8.2.25  2014/10/29 14:56:28  skavuri
 *� Case# 201410765 Std bundle issue fixed
 *�
 *� Revision 1.19.2.9.4.8.2.24  2014/10/17 14:16:45  skavuri
 *� Case# 201410760 Std bundle Issue fixed
 *�
 *� Revision 1.19.2.9.4.8.2.23  2014/09/25 18:02:17  gkalla
 *� case#201410540
 *� Cesium inventory adjustment concurrency issue
 *�
 *� Revision 1.19.2.9.4.8.2.22  2014/07/10 06:54:51  skavuri
 *� Case# 20149315 Compatibility Issue Fixed
 *�
 *� Revision 1.19.2.9.4.8.2.21  2014/06/12 14:28:22  grao
 *� Case#: 20148785  New GUI account issue fixes
 *�
 *� Revision 1.19.2.9.4.8.2.20  2014/06/05 15:46:49  nneelam
 *� case#  20148727
 *� Stanadard Bundle Issue Fix.
 *�
 *� Revision 1.19.2.9.4.8.2.19  2014/05/30 15:14:45  skavuri
 *� Case # 20148621 SB Issue Fixed
 *�
 *� Revision 1.19.2.9.4.8.2.18  2014/05/22 15:35:33  nneelam
 *� case#  20148456
 *� Stanadard Bundle Issue Fix.
 *�
 *� Revision 1.19.2.9.4.8.2.17  2014/02/17 16:09:29  sponnaganti
 *� case# 20127152
 *� (serial numbers are passing to lot)
 *�
 *� Revision 1.19.2.9.4.8.2.16  2014/02/14 14:58:49  rmukkera
 *� Case # 20127151
 *�
 *� Revision 1.19.2.9.4.8.2.15  2014/01/23 06:50:25  snimmakayala
 *� Case# : 20126827
 *� False Allocation issues for NuCourse.
 *�
 *� Revision 1.19.2.9.4.8.2.14  2014/01/22 09:19:28  snimmakayala
 *� Case# : 20126827
 *� False Allocation issues for NuCourse.
 *�
 *� Revision 1.19.2.9.4.8.2.13  2014/01/07 13:53:51  schepuri
 *� 20126665
 *�
 *� Revision 1.19.2.9.4.8.2.12  2013/12/24 14:15:07  schepuri
 *� 20126517
 *�
 *� Revision 1.19.2.9.4.8.2.11  2013/12/19 16:33:09  gkalla
 *� case#20126218
 *� Standard bundle issue
 *�
 *� Revision 1.19.2.9.4.8.2.9.2.1  2013/12/13 14:52:56  rmukkera
 *� Case #  20126232�
 *�
 *� Revision 1.19.2.9.4.8.2.9  2013/10/23 16:16:01  nneelam
 *� Case# 20124977
 *� Adjust type fetched based on location...
 *�
 *� Revision 1.19.2.9.4.8.2.8  2013/10/08 15:48:21  rmukkera
 *� Case# 20124812
 *�
 *� Revision 1.19.2.9.4.8.2.7  2013/08/07 15:19:50  rmukkera
 *� case# 20123751
 *� System displayed as Script Execution usage limit exceeded while adjusting the qty for multiple lines.
 *�
 *� Revision 1.19.2.9.4.8.2.6  2013/07/03 15:11:32  grao
 *� Case# 20123212
 *� Inventory Adjm: Inventory adjustment screen throwing script error
 *�
 *� Revision 1.19.2.9.4.8.2.5  2013/05/01 12:45:21  rmukkera
 *� Net suite inventory adjustments posting issue
 *�
 *� Revision 1.19.2.9.4.8.2.4  2013/04/16 05:37:43  skreddy
 *� CASE201112/CR201113/LOG201121
 *� issue related to  no records found
 *�
 *� Revision 1.19.2.9.4.8.2.3  2013/04/02 16:02:08  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *� TSg Issue fixes
 *�
 *� Revision 1.19.2.9.4.8.2.2  2013/03/05 14:54:32  skreddy
 *� CASE201112/CR201113/LOG201121
 *� Code merged from Lexjet production as part of Standard bundle
 *�
 *� Revision 1.19.2.9.4.8.2.1  2013/03/01 14:34:54  skreddy
 *� CASE201112/CR201113/LOG201121
 *� Merged from FactoryMation and change the Company name
 *�
 *� Revision 1.19.2.9.4.8  2013/02/06 00:52:50  kavitha
 *� CASE201112/CR201113/LOG201121
 *� Serial # functionality - Inventory process
 *�
 *� Revision 1.19.2.9.4.7  2012/12/24 13:19:06  schepuri
 *� CASE201112/CR201113/LOG201121
 *� modified item,itemstatus to multiselect
 *�
 *� Revision 1.19.2.9.4.6  2012/12/17 15:12:46  schepuri
 *� CASE201112/CR201113/LOG201121
 *� caption of adjust inv (+/-) is modified
 *�
 *� Revision 1.19.2.9.4.5  2012/12/11 02:55:31  kavitha
 *� CASE201112/CR201113/LOG201121
 *� Incorporated Serial # functionality
 *�
 *� Revision 1.19.2.9.4.4  2012/11/23 06:28:25  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Issue related to NaN in Alloc Qty
 *�
 *� Revision 1.19.2.9.4.3  2012/11/01 14:54:57  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Decimal Qty Conversions
 *�
 *� Revision 1.19.2.9.4.2  2012/10/10 09:19:59  mbpragada
 *� 20120490
 *� issue with Remaining cube updation fixed
 *�
 *� Revision 1.19.2.9.4.1  2012/09/26 22:43:33  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Production Issue Fixes for FISK,BOOMBAH and TDG.
 *�
 *� Revision 1.19.2.9  2012/09/07 17:00:19  mbpragada
 *� 20120490
 *�
 *� Revision 1.19.2.8  2012/05/24 13:03:24  schepuri
 *� CASE201112/CR201113/LOG201121
 *� stable bundle issue fix
 *�
 *� Revision 1.19.2.7  2012/04/21 09:10:44  schepuri
 *� CASE201112/CR201113/LOG201121
 *� added binloc,lp,itemstatus fields in QB
 *�
 *
 ****************************************************************************/


/*
 * Function for adding entries in the item list for Inventory Adjustment
 */
function addFieldsToForm(form,request){    	
	//var itemSubList = form.addSubList("custpage_invadj_items", "inlineeditor", "Inline Editor Sublist");

	var itemSubList = form.addSubList("custpage_invadj_items", "list", "Item List"); //Case# 20149315
	itemSubList.addField("custpage_itemcheckbox", "checkbox", "Adjust").setDefaultValue('F');
	itemSubList.addField("custpage_invadj_siteloc", "text", "Location").setDisplayType('disabled');
	itemSubList.addField("custpage_invadj_item", "text", "Item").setDisplayType('disabled');
	itemSubList.addField("custpage_invadj_desciption", "text", "Description").setDisplayType('disabled');
	itemSubList.addField("custpage_invadj_lot", "text", "LOT#").setDisplayType('disabled');
//	itemSubList.addField("custpage_invadj_allocqty", "text", "Alloc Qty").setDisplayType('disabled');
//	itemSubList.addField("custpage_invadj_resvqty", "text", "Resv Qty").setDisplayType('disabled');
	itemSubList.addField("custpage_invadj_lp", "text", "LP #").setDisplayType('disabled');

	itemSubList.addField("custpage_invadj_itemqty", "text", "Quantity on Hand").setDisplayType('inline');
	itemSubList.addField("custpage_invadj_itemquantity", "text", "Quantity on Hand").setDisplayType('hidden');
	itemSubList.addField("custpage_invadj_allocqty", "text", "Allocated Quantity").setDisplayType('disabled');
	itemSubList.addField("custpage_invadj_allocquantity", "text", "Allocated Quantity").setDisplayType('hidden');
	itemSubList.addField("custpage_invadj_availqty", "text", "Available Quantity").setDisplayType('inline');
	itemSubList.addField("custpage_invadj_availhdnqty", "text", "Available Quantity").setDisplayType('hidden');// Case# 20148621

	// itemSubList.addField("custpage_invadj_itemstatus", "select", "Item Status", "customrecord_ebiznet_sku_status").setDisplayType('entry');
	itemSubList.addField("custpage_invadj_itemstatus", "text", "Item Status").setDisplayType('disabled');
	//itemSubList.addField("custpage_invadj_qty", "text", "Qty(Available Qty adjusted to)").setDisplayType('entry');
	itemSubList.addField("custpage_invadj_qty", "text", "Adjust Onhand Qty (-/+)").setDisplayType('entry');
	//itemSubList.addField("custpage_invadj_qty", "text", "New Adjust Qty OnHand(After Adjustment)").setDisplayType('entry');

	itemSubList.addField("custpage_invadj_loc", "text", "Bin Location").setMandatory(true).setDisplayType('disabled');

	var InvAdjType= itemSubList.addField("custpage_invadj_adjtype", "select", "Adjustment Type");
	InvAdjType.addSelectOption('', '');

	var locid=request.getParameter('custpage_loc');

	var AdjustmentTypeFilters = new Array();		
	AdjustmentTypeFilters.push(new nlobjSearchFilter( 'custrecord_adjusttasktype', null, 'anyOf', ['11']));//Task Type - ADJT
	AdjustmentTypeFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	//The below code is merged from Lexjet production account on 04thMar13 by santosh ; to get the location based on Roles
	//Case # 20124977 added filter to fetch records based on locationid.
	if(locid!=null && locid!='')
		AdjustmentTypeFilters.push(new nlobjSearchFilter('custrecord_ebiz_adjtype_location', null, 'anyof', locid));
	//End
	var vRoleLocation=getRoledBasedLocation();
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		AdjustmentTypeFilters.push(new nlobjSearchFilter('custrecord_ebiz_adjtype_location', null, 'anyof', vRoleLocation));
	}
	//upto here	
	var columnsinvt = new Array();
	columnsinvt[0] = new nlobjSearchColumn('name');
	columnsinvt[1] = new nlobjSearchColumn('internalid');
	columnsinvt[0].setSort();

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, AdjustmentTypeFilters,columnsinvt);

	for (var i = 0; searchresults != null && i < searchresults.length; i++) {
		//Searching for Duplicates		
		var res=  InvAdjType.getSelectOptions(searchresults[i].getValue('name'), 'is');
		if (res != null) {

			if (res.length > 0) {
				continue;
			}
		}		
		InvAdjType.addSelectOption(searchresults[i].getValue('internalid'), searchresults[i].getValue('name'));
	}

	InvAdjType.setMandatory(true).setDisplayType('entry');

	//itemSubList.addField("custpage_invadj_adjtype", "select", "Adjustment Type", "customrecord_ebiznet_stockadj_types").setMandatory(true).setDisplayType('entry');
	itemSubList.addField("custpage_invadj_adjacc", "select", "Adjust Acc", "account").setDisplayType('hidden');
	itemSubList.addField("custpage_invadj_posttons", "checkbox", "Post to NS").setDefaultValue('T');
	itemSubList.addField("custpage_invadj_notes", "textarea", "Notes").setDisplayType('entry');
	itemSubList.addField("custpage_invadj_locvalue", "text", "BinLocValue").setDisplayType('hidden');
	itemSubList.addField("custpage_invadj_skuvalue", "text", "SKUValue").setDisplayType('hidden');
	itemSubList.addField("custpage_invadj_skustatuvalue", "text", "SKUStatus").setDisplayType('hidden');
	itemSubList.addField("custpage_invadj_existqty", "text", "ExistsQty").setDisplayType('hidden');
	itemSubList.addField("custpage_invadj_id", "text", "Internal").setDisplayType('hidden');
	itemSubList.addField("custpage_invadj_pc", "text", "Pack Code").setDisplayType('hidden');
	itemSubList.addField("custpage_invadj_sitelocvalue", "text", "locvalue").setDisplayType('hidden');
	itemSubList.addField("custpage_invadj_oldqty", "text", "old Qty").setDisplayType('hidden');
	itemSubList.addField("custpage_invadj_itemstatusvalue", "text", "statusid").setDisplayType('hidden');
	itemSubList.addField("custpage_invadj_hiddenallocqty", "text", "hidden Alloc Qty").setDisplayType('hidden');
	itemSubList.addField("custpage_invadj_lotvalue", "text", "LOT Internal Id").setDisplayType('hidden');
	itemSubList.addField("custpage_invadj_lpno", "text", "LP").setDisplayType('hidden');
	itemSubList.addField("custpage_invadj_adjsku", "text", "SKU").setDisplayType('hidden');
	itemSubList.addField("custpage_invadj_transactiono", "text", "Order #").setDisplayType('hidden');
}

/*
 * Function to add search columns into an array
 */
function addColumnsForSearch(){	
	var columns=new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_sku_status');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_inv_packcode');
	columns[5] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columns[6] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');
	columns[7] = new nlobjSearchColumn('custrecord_ebiz_inv_fifo');
	columns[8] = new nlobjSearchColumn('custrecord_ebiz_inv_adjusttype');
	columns[9] = new nlobjSearchColumn('custrecord_ebiz_inv_account_no');
	columns[10] = new nlobjSearchColumn('custrecord_ebiz_inv_loc');
	columns[11] = new nlobjSearchColumn('custrecord_ebiz_inv_note1');
	columns[12] = new nlobjSearchColumn('custrecord_ebiz_inv_note2');
	columns[13] = new nlobjSearchColumn('custrecord_ebiz_itemdesc');
	columns[14] = new nlobjSearchColumn('custrecord_ebiz_qoh');
	columns[15] = new nlobjSearchColumn('custrecord_ebiz_avl_qty');
	columns[16] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');	
	columns[17] = new nlobjSearchColumn('custrecord_ebiz_transaction_no');
	columns[0].setSort();
	return columns;

}

/*
 * Get line item level values
 */
function getLineItemValues(request, siteLocn, sku, skuDesc, skuStatus, lot, allocQty, resQty,
		qty, lp, locn, adjustType, adjustAcc, notes, existQty, intId, packCode, siteLocnValue,
		resultQty, k){
	siteLocn = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_siteloc', k);
	nlapiLogExecution('DEBUG', 'siteLocn: ', siteLocn);
	sku = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_skuvalue', k);
	skuDesc = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_desciption', k);
	skuStatus = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_itemstatus', k);
	lot = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_lot', k);
	allocQty = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_allocqty', k);
	resQty = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_resvqty', k);
	qty = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_qty', k);
	lp = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_lp', k);
	locn = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_locvalue', k);
	adjustType = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_adjtype', k);
	adjustAcc = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_adjacc', k);
	notes = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_notes', k);
	existQty = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_existqty', k);
	intId = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_id', k);
	nlapiLogExecution('DEBUG', 'intID: ', intId);
	packCode = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_pc', k);

	nlapiLogExecution('DEBUG', 'Qty: ', qty);
	resultQty = parseFloat(qty) - parseFloat(existQty);
	nlapiLogExecution('DEBUG', 'resultQty: ', resultQty);
	siteLocnValue = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_sitelocvalue', k);
}

/*
 * Load Inventory Record
 */
function loadInventoryRecord(intId, intId, sku, locn, skuDesc, skuStatus, lot, qty,
		allocQty, resQty, lp, adjustType, adjustAcc, notes, siteLocnValue, resultQty, iter,vNewQOH){

//	var invCustomRecord = nlapiLoadRecord('customrecord_ebiznet_createinv', intId);

//	invCustomRecord.setFieldValue('name', sku + iter);
//	invCustomRecord.setFieldValue('custrecord_ebiz_inv_binloc', locn);
//	invCustomRecord.setFieldValue('custrecord_ebiz_inv_sku', sku);
//	invCustomRecord.setFieldValue('custrecord_ebiz_itemdesc', skuDesc);
//	invCustomRecord.setFieldValue('custrecord_ebiz_inv_sku_status', skuStatus);
//	invCustomRecord.setFieldValue('custrecord_ebiz_inv_lot', lot);
//	invCustomRecord.setFieldValue('custrecord_ebiz_resinventory', parseFloat(resQty).toFixed(5));
//	invCustomRecord.setFieldValue('custrecord_ebiz_qoh', parseFloat(vNewQOH).toFixed(5));
//	invCustomRecord.setFieldValue('custrecord_ebiz_inv_lp', lp);
//	invCustomRecord.setFieldValue('custrecord_ebiz_inv_adjusttype', adjustType);
//	invCustomRecord.setFieldValue('custrecord_ebiz_inv_account_no', adjustAcc);
//	invCustomRecord.setFieldValue('custrecord_ebiz_inv_note1', notes);
//	invCustomRecord.setFieldValue('custrecord_ebiz_inv_loc', siteLocnValue);    
//	invCustomRecord.setFieldValue('custrecord_updated_user_no', nlapiGetContext().getUser());
//	invCustomRecord.setFieldValue('custrecord_ebiz_displayfield', 'N');
//	invCustomRecord.setFieldValue('custrecord_ebiz_callinv', 'N'); 

//	var invRecId = nlapiSubmitRecord(invCustomRecord, false, true);

	var scount=1;
	var invtrecid;

	LABL1: for(var i=0;i<scount;i++)
	{
		nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);

		try
		{
			var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', intId);
			var InvOldQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');
			var Itemname = Invttran.getFieldText('custrecord_ebiz_inv_sku');

			nlapiLogExecution('Debug', 'InvOldQOH', InvOldQOH);
			nlapiLogExecution('Debug', 'Adjusted Qty', qty);

			var newQOH = parseFloat(InvOldQOH)+parseFloat(qty);

			nlapiLogExecution('Debug', 'newQOH', newQOH);			

			Invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(newQOH));  
			Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
			Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');
			Invttran.setFieldValue('name', sku + iter);
			Invttran.setFieldValue('custrecord_ebiz_inv_sku_status', skuStatus);
			Invttran.setFieldValue('custrecord_ebiz_inv_lot', lot);
			Invttran.setFieldValue('custrecord_ebiz_resinventory', parseFloat(resQty).toFixed(5));
			Invttran.setFieldValue('custrecord_ebiz_inv_lp', lp);
			Invttran.setFieldValue('custrecord_ebiz_inv_adjusttype', adjustType);
			Invttran.setFieldValue('custrecord_ebiz_inv_account_no', adjustAcc);
			Invttran.setFieldValue('custrecord_ebiz_inv_note1', notes);
			Invttran.setFieldValue('custrecord_ebiz_inv_loc', siteLocnValue);    
			Invttran.setFieldValue('custrecord_updated_user_no', nlapiGetContext().getUser());

			invtrecid = nlapiSubmitRecord(Invttran, false, true);

			if((parseFloat(newQOH)) <= 0)
			{		
				nlapiLogExecution('Debug', 'Deleting record from inventory if QOH becomes zero', invtrecid);
				var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invtrecid);				
			}
		}
		catch(ex)
		{
			var exCode='CUSTOM_RECORD_COLLISION'; 
			var wmsE='Inventory record being updated by another user. Please try again...';
			if (ex instanceof nlobjError) 
			{	
				wmsE=ex.getCode() + '\n' + ex.getDetails();
				exCode=ex.getCode();
			}
			else
			{
				wmsE=ex.toString();
				exCode=ex.toString();
			} 

			nlapiLogExecution('Debug', 'Exception in Inventory Adjustments : ', wmsE); 
			if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
			{
				scount=scount+1;
				continue LABL1;
			}
			else break LABL1;
		}
	}
}

/*
 * Create Inventory Adjustment Record
 */
function createInvAdjRecord(invAdjustCustRecord, locn, sku, skuStatus, lot, qty, lp, adjustType,
		notes, siteLocnValue, resultQty, packCode,serialnumbers, iter){
	invAdjustCustRecord.setFieldValue('name', sku + iter);
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_binloc', locn);
	invAdjustCustRecord.setFieldValue('custrecord_ebizskuno', sku);
	invAdjustCustRecord.setFieldValue('custrecord_skustatus', skuStatus);
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_batchno', lot);
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_expeqty', parseFloat(qty).toFixed(5));
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_invjlpno', lp);
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjtype', adjustType);
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjnotes1', notes);
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_siteid', siteLocnValue);
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjustqty', parseFloat(resultQty).toFixed(5));
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_tasktype', 11);
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_recorddate', DateStamp());
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_rectime', TimeStamp());
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_packcode', packCode);
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_updateduserno', nlapiGetContext().getUser());
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjserialnumbers', serialnumbers);
}

/*
 * Set line item level values based on search results
 */
function setLineItemValuesFromSearch(form, searchResults,i){
	var binLocationValue, binLocation, lp, skuValue, sku, skuStatusValue,  skuStatus_Value,skuStatus, packCode;
	var qty, lot,lotValue, fifo, adjustType;
	var accountNo, locnValue, locn, note1, note2, skuDesc;
	var existQty, intId;
	var allocQty,qtyAvailable;

//	if(searchResults == null)
//	return;

//	for (var i = 26; i < 29; i++) {
//	try{
	var searchRecord = searchResults;
	nlapiLogExecution('DEBUG','searchRecord',searchRecord);
	binLocationValue = searchRecord.getValue('custrecord_ebiz_inv_binloc');
	binLocation = searchRecord.getText('custrecord_ebiz_inv_binloc');
	lp = searchRecord.getValue('custrecord_ebiz_inv_lp');
	skuValue = searchRecord.getValue('custrecord_ebiz_inv_sku');
	sku = searchRecord.getText('custrecord_ebiz_inv_sku');
	skuStatus_Value = searchRecord.getValue('custrecord_ebiz_inv_sku_status');
	skuStatusValue = searchRecord.getText('custrecord_ebiz_inv_sku_status');
	skuStatus = searchRecord.getText('custrecord_ebiz_inv_sku_status');
	packCode = searchRecord.getValue('custrecord_ebiz_inv_packcode');
	qty = searchRecord.getValue('custrecord_ebiz_qoh');
	lot = searchRecord.getText('custrecord_ebiz_inv_lot');
	lotValue = searchRecord.getValue('custrecord_ebiz_inv_lot');
	fifo = searchRecord.getValue('custrecord_ebiz_inv_fifo');
	adjustType = searchRecord.getValue('custrecord_ebiz_inv_adjusttype');
	accountNo = searchRecord.getValue('custrecord_ebiz_inv_account_no');
	locnValue = searchRecord.getValue('custrecord_ebiz_inv_loc');
	locn = searchRecord.getText('custrecord_ebiz_inv_loc');
	note1 = searchRecord.getValue('custrecord_ebiz_inv_note1');
	note2 = searchRecord.getValue('custrecord_ebiz_inv_note1');
	skuDesc = searchRecord.getValue('custrecord_ebiz_itemdesc');
	existQty = searchRecord.getValue('custrecord_ebiz_qoh');
	intId = searchRecord.getId();
	allocQty = searchRecord.getValue('custrecord_ebiz_alloc_qty');
	qtyAvailable = searchRecord.getValue('custrecord_ebiz_avl_qty');

	if(allocQty==null || allocQty=='')
	{
		allocQty='0';
	}

	transactiono = searchRecord.getValue('custrecord_ebiz_transaction_no');

	// if(allocQty!=null && allocQty!="")
	//{
	// 	qty=parseFloat(qty)-parseFloat(allocQty);	
	//}

	var totQty = 0;
//	var allocQty = 0;						
	if(searchRecord.getValue('custrecord_ebiz_qoh') != null && searchRecord.getValue('custrecord_ebiz_qoh') != "")
	{
		totQty = parseFloat(searchRecord.getValue('custrecord_ebiz_qoh'));
		if(totQty<0)
			totQty=0;
	}
	else
		totQty=0;
	if(searchRecord.getValue('custrecord_ebiz_alloc_qty') != null && searchRecord.getValue('custrecord_ebiz_alloc_qty') != "")
	{
		allocQty = parseFloat(searchRecord.getValue('custrecord_ebiz_alloc_qty'));
		if(allocQty<0)
			allocQty=0;
	}
	else
		allocQty=0;

	var availQty=0;
	availQty=parseFloat(totQty)-parseFloat(allocQty);

	// setting the values into the form's sublist
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_siteloc', i + 1, locn);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_item', i + 1, sku);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_desciption', i + 1, skuDesc);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_lot', i + 1, lot);
//	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_allocqty', i + 1, allocQty);
//	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_resvqty', i + 1, null);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_lp', i + 1, lp);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_itemstatus', i + 1, skuStatusValue);
	//form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_qty', i + 1, qtyAvailable);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_qty', i + 1, allocQty);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_loc', i + 1, binLocation);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_adjtype', i + 1, adjustType);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_adjacc', i + 1, accountNo);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_notes', i + 1, note1);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_locvalue', i + 1, binLocationValue);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_skuvalue', i + 1, skuValue);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_skustatuvalue', i + 1, skuStatus_Value);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_existqty', i + 1, existQty);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_id', i + 1, intId);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_pc', i + 1, packCode);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_sitelocvalue', i + 1, locnValue);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_oldqty', i + 1, qtyAvailable);

	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_itemqty', i + 1, qty);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_itemquantity', i + 1, qty);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_allocqty', i + 1, allocQty);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_allocquantity', i + 1, allocQty);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_availqty', i + 1, availQty);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_availhdnqty', i + 1, availQty);// Case# 20148621
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_hiddenallocqty', i + 1, allocQty);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_lotvalue', i + 1, lotValue);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_lpno', i + 1, lp);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_adjsku', i + 1, sku);
	form.getSubList('custpage_invadj_items').setLineItemValue('custpage_invadj_transactiono', i + 1, transactiono);
//	}
//	catch(exp)
//	{
//	nlapiLogExecution('DEBUG','Exception',exp);
//	}
//	}
}

/*
 * Update Inventory Adjustment Record
 */
function updateInvAdjRecord(outAdj, adjustAcc, notes, sku, siteLocnValue,
		resultQty){
	outAdj.setFieldValue('account', adjustAcc);
	outAdj.setFieldValue('memo', notes);

	outAdj.insertLineItem('inventory', 1);
	outAdj.setLineItemValue('inventory', 'item', 1, sku);
	outAdj.setLineItemValue('inventory', 'location', 1, siteLocnValue);
	outAdj.setLineItemValue('inventory', 'adjustqtyby', 1, parseFloat(resultQty));

	nlapiSubmitRecord(outAdj, false, true);
	nlapiLogExecution('DEBUG', 'Inventory Adjustment record', 'is created');
}

/*
 * Processing HTTP GET request
 */
function processGETRequest(request,response){
	var form = nlapiCreateForm('Inventory Adjustment');
//	form.setScript('customscript_adj_inv_single_line_validat');
	//form.setScript('customscript_inventoryadjustment');
	form.setScript('customscript_ebiz_inventory_adjmain_cl');
	var item = request.getParameter('custpage_item');
	var loc = request.getParameter('custpage_loc');


	var binlocid = request.getParameter('custpage_binlocation');
	nlapiLogExecution('DEBUG', 'binLoc Internal ID', binlocid);

	var LPvalue = request.getParameter('custpage_invtlp');
	nlapiLogExecution('DEBUG', 'LP value', LPvalue);

	var vStatus = request.getParameter('custpage_itemstatus');
	nlapiLogExecution('DEBUG', 'Item Status ', vStatus);

	var hiddenQueryParams=form.addField('custpage_qeryparams', 'text', 'queryparams').setDisplayType('hidden');
	var hiddenfieldselectpage=form.addField('custpage_hiddenfieldselectpage', 'text', 'hiddenflag').setDisplayType('hidden');
	hiddenfieldselectpage.setDefaultValue('F');

	var date=form.addField('custpage_date', 'Date', 'Date').setDisplaySize('30', '30');
	
	var Period=form.addField('custpage_accountingperiod', 'select', 'Account Period');
	
	var filterLocation = new Array();
	filterLocation.push(new nlobjSearchFilter('closed', null, 'is', 'F'));
	filterLocation.push(new nlobjSearchFilter('isquarter', null, 'is', 'F'));
	filterLocation.push(new nlobjSearchFilter('isyear', null, 'is', 'F'));
	var columnLocation = new Array();
	columnLocation[0] = new nlobjSearchColumn('periodname');
	columnLocation[1] = new nlobjSearchColumn('internalId'); 
	columnLocation[1].setSort();
	Period.addSelectOption("", "");
	var searchLocationRecord = nlapiSearchRecord('AccountingPeriod', null, filterLocation, columnLocation);
	for(var count=0;count < searchLocationRecord.length ;count++)
	{
		Period.addSelectOption(searchLocationRecord[count].getId(),searchLocationRecord[count].getValue('periodname'));

	} 
	
	
	
	
	// logging the parameters
	nlapiLogExecution('DEBUG', 'Item: ', item);
	nlapiLogExecution('DEBUG', 'Loc: ', loc);

	// adding all fields to the form
	addFieldsToForm(form,request);


	var orderList = getOrdersForInvtAdj(request,0,form);

	if(orderList != null && orderList.length > 0){
		setPagingForSublist(orderList,form,request);//Case# 201410760
	}


	/*// adding dropdwn field for paging
	var selectRange=form.addField('custpage_selectpage','select','Select Records');

	//hidden variable which is set to true if user choose an option frm selectpage dropdwn.
	form.addField('custpage_userchoice','text','user selection').setDisplayType('hidden');
	form.addField('custpage_item','text','item').setDisplayType('hidden').setDefaultValue(item);
	form.addField('custpage_loc','text','loc').setDisplayType('hidden').setDefaultValue(loc);*/

	// Submit button; Label=Adjust
	form.addSubmitButton('Adjust');


	/*var filters = new Array();
	if(item != null && item != ''){
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null,
				'anyOf', [request.getParameter('custpage_item')]));
	}

	if (loc != null && loc != ''){
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null,
				'anyOf', [request.getParameter('custpage_loc')]));
	}

	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyOf', [3,19]));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0));

	// Adding search columns
	var columns = new Array();
	addColumnsForSearch(columns);

	// execute the  search
	var searchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, 
			filters, columns);


	if(searchResults)
	{
		nlapiLogExecution('DEBUG','searchResults',searchResults.length);
		var len=searchResults.length/25;
		for(var k=1;k<=Math.ceil(len);k++)
		{
			var from;var to;
			to=parseFloat(k)*parseFloat(25);
			from=(parseFloat(to)-parseFloat(25))+1;

			if(parseFloat(to)>searchResults.length)
			{
				to=searchResults.length;
				test=from.toString()+","+to.toString(); 
			}

			var temp=from.toString()+" to "+to.toString();
			var tempto=from.toString()+","+to.toString();
			selectRange.addSelectOption(tempto,temp);
		}
	}

	var minval=0;
	var maxval=25;
	if(parseFloat(25)>searchResults.length)
	{
		maxval=searchResults.length;
	}

	var selectno=request.getParameter('custpage_selectpage');
	if(selectno!=null )
	{
		var temp= request.getParameter('custpage_selectpage');
		var temparray=temp.split(',');
		nlapiLogExecution('DEBUG', 'temparray',temparray.length);

		var diff=parseFloat(temparray[1])-(parseFloat(temparray[0])-1);
		nlapiLogExecution('DEBUG', 'diff',diff);

			if(parseFloat(diff)==25|| test==selectno)
			{
				var temparray=selectno.split(',');	
				nlapiLogExecution('DEBUG', 'temparray.length ', temparray.length);  
				minval=parseFloat(temparray[0])-1;
				nlapiLogExecution('DEBUG', 'temparray[0] ', temparray[0]);  
				maxval=parseFloat(temparray[1]);
				nlapiLogExecution('DEBUG', 'temparray[1] ', temparray[1]);  
			}
	}

	setLineItemValuesFromSearch(form, searchResults,minval,maxval);*/

	response.writePage(form);
}

function setPagingForSublist(orderList,form,request)
{
	if(orderList != null && orderList.length > 0){
		nlapiLogExecution('DEBUG', 'orderList ', orderList.length);
		var orderListArray=new Array();		
		for(k=0;k<orderList.length;k++)
		{
			nlapiLogExecution('DEBUG', 'orderList[k]', orderList[k]); 
			var ordsearchresult = orderList[k];

			if(ordsearchresult!=null)
			{
				nlapiLogExecution('DEBUG', 'ordsearchresult.length ', ordsearchresult.length); 
				for(var j=0;j<ordsearchresult.length;j++)
				{
					orderListArray[orderListArray.length]=ordsearchresult[j];				
				}
			}
		}
		nlapiLogExecution('DEBUG', 'orderListArray.length ', orderListArray.length); 
		var test='';

		if(orderListArray.length>0)
		{

			if(orderListArray.length>20)
			{
				var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('hidden');
				pagesize.setDisplaySize(10,10);
				pagesize.setLayoutType('outsidebelow', 'startrow');
				var select= form.addField('custpage_selectpage','select', 'Select Records');	
				select.setLayoutType('outsidebelow', 'startrow');			
				select.setDisplaySize(200,30);
				if (request.getMethod() == 'GET'){
					pagesize.setDefaultValue("20");
					pagesizevalue=20;
				}
				else
				{
					if(request.getParameter('custpage_pagesize')!=null)
					{pagesizevalue= request.getParameter('custpage_pagesize');}
					else
					{pagesizevalue= 20;pagesize.setDefaultValue("20");}
				}

				//this is to add the pageno's to the dropdown.
				var len=orderListArray.length/parseFloat(pagesizevalue);
				for(var k=1;k<=Math.ceil(len);k++)
				{

					var from;var to;

					to=parseFloat(k)*parseFloat(pagesizevalue);
					from=(parseFloat(to)-parseFloat(pagesizevalue))+1;

					if(parseFloat(to)>orderListArray.length)
					{
						to=orderListArray.length;
						test=from.toString()+","+to.toString(); 
					}

					var temp=from.toString()+" to "+to.toString();
					var tempto=from.toString()+","+to.toString();
					select.addSelectOption(tempto,temp);

				} 

				if (request.getMethod() == 'POST'){

					if(request.getParameter('custpage_selectpage')!=null ){

						select.setDefaultValue(request.getParameter('custpage_selectpage'));	

					}
					if(request.getParameter('custpage_pagesize')!=null ){

						pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	

					}
					if(request.getParameter('custpage_date')!=null){
						
					}
				}
				else {

				}
			}
			else
			{
				pagesizevalue=parseFloat(orderListArray.length);
			}
			var minval=0;var maxval=parseFloat(pagesizevalue);
			if(parseFloat(pagesizevalue)>orderListArray.length)
			{
				maxval=orderListArray.length;
			}
			var selectno=request.getParameter('custpage_selectpage');
			if(selectno!=null )
			{
				var selectedPage= request.getParameter('custpage_selectpage');
				var selectedPageArray=selectedPage.split(',');			
				var diff=parseFloat(selectedPageArray[1])-(parseFloat(selectedPageArray[0])-1);
				nlapiLogExecution('DEBUG', 'diff',diff);

				var pagevalue=request.getParameter('custpage_pagesize');
				nlapiLogExecution('DEBUG', 'pagevalue',pagevalue);
				if(pagevalue!=null)
				{

					if(parseFloat(diff)==parseFloat(pagevalue)|| test==selectno)
					{

						var selectedPageArray=selectno.split(',');	
						nlapiLogExecution('DEBUG', 'selectedPageArray.length ', selectedPageArray.length);  
						minval=parseFloat(selectedPageArray[0])-1;
						nlapiLogExecution('DEBUG', 'selectedPageArray[0] ', selectedPageArray[0]);  
						maxval=parseFloat(selectedPageArray[1]);
						nlapiLogExecution('DEBUG', 'selectedPageArray[1] ', selectedPageArray[1]);  
					}
				}
			}


			var c=0;
			var minvalue;
			minvalue=minval;

			for(var j = minvalue; j < maxval; j++){		

				var currentOrder = orderListArray[j];
				setLineItemValuesFromSearch(form, currentOrder, c);
//				addFulfilmentOrderToSublist(form, currentOrder, c);
				c=c+1;
			}
		}
		else
		{
			nlapiLogExecution('DEBUG', 'ELSE1', 'ELSEXE--->');
			var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'No Records found for Selection', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
		}
	}
}

var searchResultArray=new Array();

function getOrdersForInvtAdj(request,maxno,form){

	var localVarArray;
	//this is to maintain the querystring parameters while refreshing the paging drop down
	if (request.getMethod() == 'POST') {
		var queryparams=request.getParameter('custpage_qeryparams');
		nlapiLogExecution('DEBUG', 'queryparams', queryparams);
		// queryparams=queryparams.
		var tempArray=new Array();
		tempArray.push(queryparams.split(','));
		localVarArray=tempArray;
		nlapiLogExecution('DEBUG', 'tempArray', tempArray.length);
	}
	else
	{
		localVarArray = validateRequestParams(request,form);
		nlapiLogExecution('DEBUG', 'localVarArrayinelse', localVarArray);
	}

	// Get all the search filters for wave generation
	var filters = new Array();   
	filters = specifyInvtFilters(localVarArray,maxno);


	nlapiLogExecution('DEBUG', 'afterspecifyfilters', localVarArray);
	// Adding search columns
	var columns = new Array();
	columns=addColumnsForSearch();
	nlapiLogExecution('DEBUG', 'afterspecifycolumns', localVarArray);
	var orderList = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
	if( orderList!=null && orderList.length>=1000)
	{ 
		nlapiLogExecution('DEBUG', 'orderList.length', orderList.length);
		searchResultArray.push(orderList); 
		//TSG SB Issue fixes 20123212 
		//start
		var maxno=orderList[orderList.length-1].getId();
		//end
		getOrdersForInvtAdj(request,maxno,form);	
	}
	else
	{
		nlapiLogExecution('DEBUG', 'orderList.length', 'orderList.length');
		searchResultArray.push(orderList); 
	}
	return searchResultArray;



}


function specifyInvtFilters(localVarArray,maxno){

	var vItem = new Array();
	var vItemStatus = new Array();
	var filters = new Array();

	if(localVarArray[0][0] != "")
	{

		var itemId = localVarArray[0][0];

		if(itemId != null && itemId != "")
		{
			vItem = itemId.split('');
		}
		nlapiLogExecution('DEBUG', 'item Internal ID Array', vItem);

		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyOf', vItem));
	}

	if(localVarArray[0][1] != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyOf', localVarArray[0][1]));

	if(localVarArray[0][2] != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyOf', localVarArray[0][2]));

	if(localVarArray[0][3] != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', localVarArray[0][3]));

	if(localVarArray[0][4] != "")
	{
		var vItemStatusId = localVarArray[0][4];

		if(vItemStatusId != null && vItemStatusId != "")
		{
			vItemStatus = vItemStatusId.split('');
		}
		nlapiLogExecution('DEBUG', 'itemstatus Internal ID Array', vItemStatus);

		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'anyOf', vItemStatus));
	}

	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyOf', [3,19]));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0));

	filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));
	
	var vRoleLocation=getRoledBasedLocationNew();
	nlapiLogExecution('ERROR', 'vRoleLocation', vRoleLocation);
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0){
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', vRoleLocation));	
	}
	
	

	return filters;

}

function validateRequestParams(request,form){

	var vItem = "";
	var vLoc = "";
	var vbinLoc = "";
	var vLP = "";
	var vItemStatus = "";

	var localVarArray = new Array();

	if (request.getParameter('custpage_item') != null && request.getParameter('custpage_item') != "") {
		vItem = request.getParameter('custpage_item');
	}

	if (request.getParameter('custpage_loc') != null && request.getParameter('custpage_loc') != "") {
		vLoc = request.getParameter('custpage_loc');
	}

	if (request.getParameter('custpage_binlocation') != null && request.getParameter('custpage_binlocation') != "") {
		vbinLoc = request.getParameter('custpage_binlocation');
	}

	if (request.getParameter('custpage_invtlp') != null && request.getParameter('custpage_invtlp') != "") {
		vLP = request.getParameter('custpage_invtlp');
	}

	if (request.getParameter('custpage_itemstatus') != null && request.getParameter('custpage_itemstatus') != "") {
		vItemStatus = request.getParameter('custpage_itemstatus');
	}

	var currentRow = [vItem, vLoc,vbinLoc,vLP,vItemStatus];

	localVarArray.push(currentRow);
	if (request.getParameter('custpage_qeryparams')==null )
	{
		var hiddenQueryParams=form.getField('custpage_qeryparams');
		nlapiLogExecution('DEBUG','localVarArray',localVarArray.toString());
		hiddenQueryParams.setDefaultValue(localVarArray.toString());
	}
	return localVarArray;
}



/*
 * Processing HTTP POST request
 */
function processPOSTRequest(request, response){			
	var siteLocn, sku, skuDesc, skuStatus,sku_Status, lot,lotValue, allocQty, resQty, qty, lp; 
	var locn, adjustType, adjustAcc, notes, existQty, intId, packCode; 
	var siteLocnValue, resultQty, oldQty;
	var lineCount = request.getLineItemCount('custpage_invadj_items');
	var tempflag='F';
	var form = nlapiCreateForm('Inventory Adjustment');

	form.setScript('customscript_ebiz_inventory_adjmain_cl');
	//form.setScript('customscript_inventoryadjustment');

	var date=form.addField('custpage_date', 'Date', 'Date').setDisplaySize('30', '30');
	if(request.getParameter('custpage_date')!=null)
	{
		date.setDefaultValue(request.getParameter('custpage_date'));
	}
	
	
	var Period=form.addField('custpage_accountingperiod', 'select', 'Account Period');
	
	var filterLocation = new Array();
	filterLocation.push(new nlobjSearchFilter('closed', null, 'is', 'F'));
	filterLocation.push(new nlobjSearchFilter('isquarter', null, 'is', 'F'));
	filterLocation.push(new nlobjSearchFilter('isyear', null, 'is', 'F'));
	var columnLocation = new Array();
	columnLocation[0] = new nlobjSearchColumn('periodname');
	columnLocation[1] = new nlobjSearchColumn('internalId'); 
	columnLocation[1].setSort();
	Period.addSelectOption("", "");
	var searchLocationRecord = nlapiSearchRecord('AccountingPeriod', null, filterLocation, columnLocation);
	for(var count=0;count < searchLocationRecord.length ;count++)
	{
		Period.addSelectOption(searchLocationRecord[count].getId(),searchLocationRecord[count].getValue('periodname'));

	} 
	
	if(request.getParameter('custpage_accountingperiod')!=null)
	{
		Period.setDefaultValue(request.getParameter('custpage_accountingperiod'));
	}
	
	
	var hiddenQueryParams=form.addField('custpage_qeryparams', 'text', 'queryparams').setDisplayType('hidden');
	if(request.getParameter('custpage_qeryparams')!=null)
	{
		hiddenQueryParams.setDefaultValue(request.getParameter('custpage_qeryparams'));
	}
	var hiddenfieldselectpage=form.addField('custpage_hiddenfieldselectpage', 'text', 'hiddenflag').setDisplayType('hidden');
	hiddenfieldselectpage.setDefaultValue('F');

	var newpageval = request.getParameter('custpage_hiddenfieldselectpage');// case# 201415195
	if(newpageval != 'T' && newpageval!=null && newpageval!='')
	{
		for (var k=1; k<=lineCount; k++){
			if(request.getLineItemValue('custpage_invadj_items', 'custpage_itemcheckbox', k) == 'T'){
				tempflag='T';
				siteLocn = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_siteloc', k);
				sku = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_skuvalue', k);
				skuDesc = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_desciption', k);
				skuStatus = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_itemstatus', k);
				sku_Status = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_skustatuvalue', k);
				lot = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_lot', k);
				lotValue = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_lotvalue', k);
				allocQty = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_allocqty', k);
				resQty = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_resvqty', k);
				qty = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_qty', k);
				var qtyAdj = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_qty', k);
				lp = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_lp', k);
				locn = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_locvalue', k);
				adjustType = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_adjtype', k);
				adjustAcc = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_adjacc', k);
				notes = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_notes', k);
				existQty = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_existqty', k);
				intId = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_id', k);
				packCode = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_pc', k);
				oldQty = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_oldqty', k);
				var oldQOH = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_itemqty', k);
				//resultQty = parseFloat(existQty) - parseFloat(qty);
				nlapiLogExecution('DEBUG', 'qtyAdj ', qtyAdj );
				nlapiLogExecution('DEBUG', 'oldQty', oldQty);
				nlapiLogExecution('DEBUG', 'oldQOH', oldQOH);
				nlapiLogExecution('ERROR', 'Returned ID', intId);
				var invCustomRecord = nlapiLoadRecord('customrecord_ebiznet_createinv', intId);
				if(invCustomRecord != null && invCustomRecord != '')
				{
					oldQty = invCustomRecord.getFieldValue('custrecord_ebiz_avl_qty');
					oldQOH = invCustomRecord.getFieldValue('custrecord_ebiz_qoh');
					allocQty = invCustomRecord.getFieldValue('custrecord_ebiz_alloc_qty');
					if(allocQty == null || allocQty == '')
						allocQty=0;
				}
				nlapiLogExecution('DEBUG', 'qtyAdj ', qtyAdj );
				nlapiLogExecution('DEBUG', 'oldQty', oldQty);
				nlapiLogExecution('DEBUG', 'oldQOH', oldQOH);

			if(qtyAdj != null && qtyAdj !='')
				qty=parseFloat(qtyAdj);
			else
				qty=0;

			//resultQty = parseFloat(qty) - parseFloat(oldQty);
			//var newQOH=parseFloat(oldQOH)+ parseFloat(resultQty);
			resultQty=qty;
			var newQOH=parseFloat(oldQOH)+ parseFloat(qty);
			nlapiLogExecution('DEBUG', 'New QOH', newQOH);
			nlapiLogExecution('DEBUG', 'allocQty', allocQty);

			nlapiLogExecution('DEBUG', 'qty', qty);

			siteLocnValue = request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_sitelocvalue', k);

			if(isNaN(allocQty))		    
			{
				allocQty=0;
			}

			if(isNaN(qty) == true)		    
			{		      					 
				var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);		  
				msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'Please enter Quantity as a number', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");		    		   								  
				response.writePage(form);
				return false;
			}
//			else if(parseFloat(qty)<0)
//			{
//			var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);		  
//			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'Quantity cannot be entered in -ve value', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");		    		   								  
//			response.writePage(form);
//			return false;
//			}

//			if(parseFloat(qty)<parseFloat(allocQty))
//			{
//			var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);		  
//			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'Quantity cannot be lessthan allocate quantity', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");		    		   								  
//			response.writePage(form);
//			return false;

//			}



			//Case # 20123751 start(System displayed as Script Execution usage limit exceeded while adjusting the qty for multiple lines.)
			/*	var itemCube = 0;
			var filters = new Array();
			filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', sku);
			filters[1] = new nlobjSearchFilter('custrecord_ebizuomskudim', null, 'is', 1);

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
			var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);


			if(skuDimsSearchResults != null){
				nlapiLogExecution('DEBUG', 'Search Length of SKU Dims in inventory adjustment',skuDimsSearchResults.length);
				for (var i = 0; i < skuDimsSearchResults.length; i++) {
					var skuDim = skuDimsSearchResults[i];
					itemCube = skuDim.getValue('custrecord_ebizcube');		    		
				}
			}

			nlapiLogExecution('DEBUG', 'Adjustment Inventory item cube details', itemCube);		    
			//itemCube = (parseFloat(itemCube) * parseFloat(qty));		    								
			nlapiLogExecution('DEBUG', 'Adjustment Inventory Remaining Cube ', voldLocRemainingCube);



			var filterLocation = new Array();
			filterLocation[0] = new nlobjSearchFilter('id', null, 'equalto', locn);          
			var LocationArray = new Array();
			LocationArray[0]=new nlobjSearchColumn('custrecord_remainingcube');     
			var LocationResults =nlapiSearchRecord('customrecord_ebiznet_location', null, filterLocation,LocationArray);
			var voldLocRemainingCube =  LocationResults[0].getValue('custrecord_remainingcube');

			nlapiLogExecution('DEBUG', 'Adjustment Inventory Remaining Cube ', voldLocRemainingCube);

			nlapiLogExecution('DEBUG', 'existQty', existQty);
			nlapiLogExecution('DEBUG', 'qty', qty);
			var rqty = parseFloat(qty) - parseFloat(existQty);

			nlapiLogExecution('DEBUG', 'Qty information', qty);
			nlapiLogExecution('DEBUG', 'Existing Qty information', existQty);
			nlapiLogExecution('DEBUG', 'rqty details', rqty);
			nlapiLogExecution('DEBUG', 'adjustType', adjustType);


			var LocTotalCube = 0;

			if(qty!=oldQty)
			{		    	
				if(parseFloat(rqty)< 0 )
				{		  
					nlapiLogExecution('DEBUG', 'into if', 'done');
					LocTotalCube = parseFloat(voldLocRemainingCube) + (rqty * parseFloat(itemCube));
				}
				else
				{	
					nlapiLogExecution('DEBUG', 'into else', 'done');
					LocTotalCube = parseFloat(voldLocRemainingCube) - (rqty * parseFloat(itemCube));
				}		    	
				nlapiLogExecution('DEBUG', 'Adjustment Inventory Total Cube value ', LocTotalCube);		    	 
				//var retValue1 = UpdateLocCube(locn, LocTotalCube);
			}*/
			//Case # 20123751 end
			try
			{
				nlapiLogExecution('DEBUG','sku',sku);
				//adjustType
				var fields = ['recordType', 'custitem_ebizserialin'];
				var columns = nlapiLookupField('item', sku, fields);
				var ItemType = columns.recordType;
				nlapiLogExecution('DEBUG','ItemType',ItemType);

				var serialInflg="F";		
				serialInflg = columns.custitem_ebizserialin;		

				var serialnumbers = "";
				var localSerialNoArray = new Array();
				if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") 
				{
					localSerialNoArray = AdjustSerialNumbers(localSerialNoArray,qty,sku,lp);

					if(localSerialNoArray != null && localSerialNoArray != "")
					{
						//nlapiLogExecution('DEBUG','localSerialNoArray',localSerialNoArray.ToString());
						if(localSerialNoArray.length != 0)
						{
							nlapiLogExecution('DEBUG','localSerialNoArray.length',localSerialNoArray.length);
							serialnumbers = getSerialNoCSV(localSerialNoArray);
							//case# 20127152 starts (serial numbers are passing to lot)
							lot = serialnumbers;
							//case# 20127152 end
							nlapiLogExecution('DEBUG','serialnumbers',serialnumbers);
						}
					}
				}

				nlapiLogExecution('DEBUG', 'Returned ID', intId);
//				var invCustomRecord = nlapiLoadRecord('customrecord_ebiznet_createinv', intId);
//				loadInventoryRecord(invCustomRecord, intId, sku, locn, skuDesc, sku_Status, 
//				lotValue, qty, allocQty, resQty, lp, adjustType, adjustAcc, notes, 
//				siteLocnValue, resultQty, k,newQOH);

				nlapiLogExecution('DEBUG', 'invAdjustCustRecord - skuStatus', skuStatus);

				// Inventory Adjustment custom record
				var invAdjustCustRecord = nlapiCreateRecord('customrecord_ebiznet_invadj');//Case# 201410765
				createInvAdjRecord(invAdjustCustRecord, locn, sku, sku_Status, lot, qty, lp,
						adjustType, notes, siteLocnValue, resultQty, packCode,serialnumbers, k);

				//	Updating the inventory adjustment record.                
				//var outAdj = nlapiCreateRecord('inventoryadjustment');
				//updateInvAdjRecord(outAdj, adjustAcc, notes, sku, siteLocnValue, resultQty);

				//Case # 20127151?Start
				/*if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") 
				{
					//case # 20126232� start
					if(parseFloat(qty)<0)
					{
						qty=parseFloat(qty)*parseFloat(-1);
					}
					lot = serialnumbers;
					//case # 20126232� end
				}*/
				//Case # 20127151?End
				var tasktype=11; //11 is the internalid value of tasktype "ADJT";
				nlapiLogExecution('DEBUG','sku_Status',sku_Status);
				var chkposttoNS=request.getLineItemValue('custpage_invadj_items', 'custpage_invadj_posttons', k);
				nlapiLogExecution('DEBUG','chkposttoNS',chkposttoNS);
				var date = request.getParameter('custpage_date');
				var period = request.getParameter('custpage_accountingperiod');
				nlapiLogExecution('ERROR','date',date);
				if(chkposttoNS!=null && chkposttoNS!='' && chkposttoNS=='T')
				{
					InvokeNSInventoryAdjustment(sku,sku_Status,siteLocnValue,resultQty,notes,tasktype,adjustType,lot,date,period);
				}


				// Update Inventory Adjustment custom record
				var invAdjRecId = nlapiSubmitRecord(invAdjustCustRecord, false, true);
				nlapiLogExecution('DEBUG', 'Inventory Adjust Record Insertion / Update for invAdjustRecId',
						invAdjRecId + ' is Success');

				nlapiLogExecution('DEBUG', 'Returned ID', intId);

				loadInventoryRecord(intId, intId, sku, locn, skuDesc, sku_Status, 
						lotValue, qty, allocQty, resQty, lp, adjustType, adjustAcc, notes, 
						siteLocnValue, resultQty, k,newQOH);

				//	Update Inventory custom record
				//var invRecId = nlapiSubmitRecord(invCustomRecord, false, true);
				//nlapiLogExecution('DEBUG', 'Inventory Record Insertion / Update for invRecId ', invCustomRecord + ' is Success');


			}
			catch(Exc)
			{
				nlapiLogExecution('DEBUG', 'exeption ', Exc);
			}


		}
	}
	}
	if(tempflag=='F')
	{

		//Create the sublist
		addFieldsToForm(form,request);

		var orderList = getOrdersForInvtAdj(request,0,form);		
		if(orderList != null && orderList.length > 0){
			setPagingForSublist(orderList,form,request);//Case# 201410760
		}

		form.addSubmitButton('Adjust');

		response.writePage(form);
		/*
		var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);		  
		msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'Please select atleast one line to adjust', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");		    		   								  
		response.writePage(form);
		return false;*/
	}
	else
		response.sendRedirect('TASKLINK', 'LIST_TRAN_INVADJST', null, null, null);
}

function AdjustInventoryMainSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		processGETRequest(request,response);
	} else {
		/*var userselection=request.getParameter('custpage_userchoice');
		nlapiLogExecution('DEBUG','userselection',userselection);
		if(userselection!='true')
		processPOSTRequest(request, response);
		else
			processGETRequest(request,response);*/
		processPOSTRequest(request, response);

	} //end of else part of post back
}

function AdjustSerialNumbers(localSerialNoArray,qty,item,lp)
{	
	nlapiLogExecution('DEBUG','AdjustSerialNumbers qty',qty);
	nlapiLogExecution('DEBUG','item',item);
	nlapiLogExecution('DEBUG','lp',lp);
	if(parseFloat(qty) <0)
	{
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
		filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
		filters[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'D');

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
		var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

		if(searchResults != null && searchResults != "")
		{ 
			for (var i = 0; i < searchResults.length; i++) 
			{   
				if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
				{
					var SerialNo = searchResults[i].getValue('custrecord_serialnumber');
					var InternalID = searchResults[i].getId();

					var currentRow = [SerialNo];					
					localSerialNoArray.push(currentRow);
					nlapiLogExecution('DEBUG', 'localSerialNoArray : ', localSerialNoArray.toString());

					var id = nlapiDeleteRecord('customrecord_ebiznetserialentry', InternalID);
					nlapiLogExecution('DEBUG', 'Deleted Actual Record : ', id);
				}
			}
		}
	}
	else if(parseFloat(qty) > 0)
	{
		nlapiLogExecution('DEBUG', 'parseFloat(qty) : ', parseFloat(qty));
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
		filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
		filters[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'I');

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
		var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

		if(searchResults != null && searchResults != "")
		{ 
			nlapiLogExecution('DEBUG', 'searchResults.length : ', searchResults.length);
			for (var i = 0; i < searchResults.length; i++) 
			{   
				if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
				{
					var SerialNo = searchResults[i].getValue('custrecord_serialnumber');
					var InternalID = searchResults[i].getId();

					var currentRow = [SerialNo];					
					localSerialNoArray.push(currentRow);
					nlapiLogExecution('DEBUG', 'localSerialNoArray : ', localSerialNoArray.toString());

					var LoadSerialNumbers = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
					LoadSerialNumbers.setFieldValue('custrecord_serialstatus', '');

					var recid = nlapiSubmitRecord(LoadSerialNumbers, false, true);
					nlapiLogExecution('DEBUG', 'Serial Entry rec id',recid);
				}
			}
		}
	}
	return localSerialNoArray;
}

function getSerialNoCSV(arrayLP){
	var csv = "";
	nlapiLogExecution('DEBUG', 'arrayLP.length',arrayLP.length);
	if(arrayLP.length == 1)
	{
		csv = arrayLP[0][0];
	}
	else
	{
		for (var i = 0; i < arrayLP.length; i++) {
			if(i == arrayLP.length -1)
			{
				csv += arrayLP[i][0];
			}
			else
			{
				csv += arrayLP[i][0] + ",";
			}
		}
	}
	return csv;
}
