/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/ebiz_inventoryMoveQB_SL.js,v $
 *     	   $Revision: 1.5.2.3.4.2.4.9 $
 *     	   $Date: 2015/06/09 13:49:29 $
 *     	   $Author: schepuri $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_inventoryMoveQB_SL.js,v $
 * Revision 1.5.2.3.4.2.4.9  2015/06/09 13:49:29  schepuri
 * case# 201413036
 *
 * Revision 1.5.2.3.4.2.4.8  2014/07/09 12:46:21  rmukkera
 * Case # 20149231
 * jawbone issues applicable to standard also
 *
 * Revision 1.5.2.3.4.2.4.7  2014/01/29 15:23:30  rmukkera
 * Case # 20126967
 *
 * Revision 1.5.2.3.4.2.4.6  2014/01/24 14:43:54  rmukkera
 * Case # 20126884
 *
 * Revision 1.5.2.3.4.2.4.5  2013/12/06 15:12:30  skreddy
 * Case# 20126182
 * 2014.1 stnd bundle issue fix
 *
 * Revision 1.5.2.3.4.2.4.4  2013/03/19 11:48:12  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.5.2.3.4.2.4.3  2013/03/15 09:28:45  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.5.2.3.4.2.4.2  2013/03/01 14:34:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.5.2.3.4.2.4.1  2013/02/26 13:02:23  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Marged from Boombah.
 *
 * Revision 1.5.2.3.4.2  2013/01/24 03:05:27  kavitha
 * CASE201112/CR201113/LOG201121
 * Converted Bin Location,LP,Location dropdown - DBCombo
 *
 * Revision 1.5.2.3.4.1  2012/11/01 14:54:57  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.5.2.3  2012/08/29 16:22:26  spendyala
 * CASE201112/CR201113/LOG201121
 * Site not populating with all the values.
 *
 * Revision 1.5.2.2  2012/08/29 00:50:56  gkalla
 * CASE201112/CR201113/LOG201121
 * To populate bin locations of morethan 1000
 *
 * Revision 1.5.2.1  2012/02/10 15:41:11  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Physical and Virtual Location changes
 *
 * Revision 1.6  2012/02/10 15:38:02  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Physical and Virtual Location changes
 *
 * Revision 1.5  2011/12/13 21:06:04  gkalla
 * CASE201112/CR201113/LOG201121
 * Add new fields in opentask
 *
 * Revision 1.3  2011/12/08 13:31:26  gkalla
 * CASE201112/CR201113/LOG201121
 * Added Item status drop down
 *
 * Revision 1.2  2011/11/25 20:37:58  gkalla
 * CASE201112/CR201113/LOG201121
 * multi select Item list
 *
 * Revision 1.1  2011/10/12 10:08:43  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Inventory Move  Files
 *
 * Revision 1.7  2011/07/21 05:39:06  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/
function inventoryMoveQbSuitelet(request, response){
	var ctx = nlapiGetContext();
	if (request.getMethod() == 'GET') 
	{
		var form = nlapiCreateForm('Inventory Move');

		var binLocationField = form.addField('custpage_binlocation', 'select', 'Bin Location','customrecord_ebiznet_location');
		//binLocationField.addSelectOption('', '');
		
		//Case # 20126884 start
		form.setScript('customscript_cs');
		//Case # 20126884 end
		
		//var itemField = form.addField('custpage_item', 'select', 'Item');
		//itemField.addSelectOption('', '');

		//var itemField = form.addField('custpage_item', 'select', 'item', 'item');
		//var itemField = form.addField('custpage_item', 'select', 'item', 'item');
		var itemField = form.addField('custpage_item', 'multiselect', 'Item','item');

		/*var columns = new Array();
		columns[0] = new nlobjSearchColumn('name'); 		
		var searchresults = nlapiSearchRecord('item', null, null, columns);
		for (var i = 0; searchresults != null && i < Math.min(500, searchresults.length); i++) {

			itemField.addSelectOption(searchresults[i].getId(), searchresults[i].getValue('name'));
		}*/

		//itemField.setLayoutType('startrow', 'none');

		var invtlp = form.addField('custpage_invtlp', 'select', 'LP','customrecord_ebiznet_master_lp');
		//invtlp.addSelectOption('', '');

		var varLoc = form.addField('custpage_location', 'select', 'Location','location');
		//varLoc.addSelectOption('', '');
		varLoc.setMandatory(true);

		var varComp = form.addField('custpage_company', 'select', 'Company','customrecord_ebiznet_company');
		var vItemStatus = form.addField('custpage_itemstatus', 'select', 'Item Status','customrecord_ebiznet_sku_status');
		var save = form.addField('custpage_save', 'checkbox','isfromlocation').setDisplayType('hidden');
		//varComp.addSelectOption('', '');

		/*var searchlocresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, null, new nlobjSearchColumn('custrecord_ebiz_inv_loc',null,'group').setSort());

		if (searchlocresults != null)
		{
			nlapiLogExecution('ERROR', 'Inventory count', searchlocresults.length);

			for (var i = 0; i < searchlocresults.length; i++) 
			{
				var res=searchlocresults[i].getValue('custrecord_ebiz_inv_loc',null,'group');
				if(res!=null&&res!="")
				varLoc.addSelectOption(searchlocresults[i].getValue('custrecord_ebiz_inv_loc',null,'group'), searchlocresults[i].getText('custrecord_ebiz_inv_loc',null,'group'));
			}
		}*/      

		/*	var searchcompresults = nlapiSearchRecord('customrecord_ebiznet_company', null, new nlobjSearchFilter('isinactive',null,'is','F'), new nlobjSearchColumn('Name').setSort());

		if (searchcompresults != null)
		{
			for (var i = 0; i < searchcompresults.length; i++) 
			{
				var res = form.getField('custpage_company').getSelectOptions(searchcompresults[i].getValue('Name'), 'is');
				if (res != null) 
				{
					if (res.length > 0) 
						continue;                
				}
				if(searchcompresults[i].getValue('Name') != "")
					varComp.addSelectOption(searchcompresults[i].getValue('Name'), searchcompresults[i].getValue('Name'));
			}
		}*/ 

		//3 - In bound Storage
		//19 - Inventory Storage
		//var filtersinv = new Array();
		//filtersinv[0] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3, 19]);

		//var columns = new Array();
		//columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
		//columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
		//columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
		//columns[1].setSort();
		//columns[1].setSort();		
		//columns[2].setSort();

		//var searchInventory = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinv, columns);
		//var searchInventoryMain=FillBinLocation(-1);

		//if (searchInventoryMain != null) 
		//{
		//var c=0;var p=0;
		//nlapiLogExecution('ERROR', 'searchInventoryMain count', searchInventoryMain.length);
		//for (var j = 0; j < searchInventoryMain.length; j++) {
		//	var searchInventory=searchInventoryMain[j];				
		//for (var i = 0; i < searchInventory.length; i++) 
		//{

		//	var res = form.getField('custpage_binlocation').getSelectOptions(searchInventory[i].getText('custrecord_ebiz_inv_binloc'), 'is');
		//	if (res != null) 
		//	{
		//		if (res.length > 0) 
		//			{
		//			c=c+1;
		//			continue;
		//			}
		//	}
		//	p=p+1;
		//	binLocationField.addSelectOption(searchInventory[i].getValue('custrecord_ebiz_inv_binloc'), searchInventory[i].getText('custrecord_ebiz_inv_binloc'));
		//}
		//nlapiLogExecution('ERROR', 'searchInventory count', p);
		//nlapiLogExecution('ERROR', 'searchInventory count', c);

		/*	for (var i = 0; i < searchInventory.length; i++) 
			{
				var res = form.getField('custpage_item').getSelectOptions(searchInventory[i].getText('custrecord_ebiz_inv_sku'), 'is');
				if (res != null) 
				{
					if (res.length > 0) 
						continue;                    
				}
				//itemField.addSelectOption(searchInventory[i].getValue('custrecord_ebiz_inv_sku'), searchInventory[i].getText('custrecord_ebiz_inv_sku'));
			} */

		//for (var i = 0; i < searchInventory.length; i++) 
		//{
		//	invtlp.addSelectOption(searchInventory[i].getValue('custrecord_ebiz_inv_lp'), searchInventory[i].getValue('custrecord_ebiz_inv_lp'));
		//}
		//}
		/*	var vItemStatus = form.addField('custpage_itemstatus', 'select', 'Item Status');
			vItemStatus.addSelectOption('', '');
			var filtersStatus = new Array();
			filtersStatus.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));


			var columnsstatus = new Array();
			columnsstatus.push(new nlobjSearchColumn('custrecord_statusdescriptionskustatus'));

			var searchStatus = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, filtersStatus, columnsstatus);

			if (searchStatus != null) 
			{
				for (var i = 0; i < searchStatus.length; i++) 
				{
					var res = form.getField('custpage_itemstatus').getSelectOptions(searchStatus[i].getValue('custrecord_statusdescriptionskustatus'), 'is');
					if (res != null) 
					{
						if (res.length > 0) 
							continue;                    
					}
					vItemStatus.addSelectOption(searchStatus[i].getId(), searchStatus[i].getValue('custrecord_statusdescriptionskustatus'));
				}
			}*/
		var button = form.addSubmitButton('Display');
		//}
		//else 
		//{
		//	var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
		//	msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'No Records', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
		//}

		response.writePage(form);
	} 
	else 
	{  
		var isfromlocatiochange=request.getParameter('custpage_save');

		if(isfromlocatiochange!='T')
		{
			var invArray = new Array();
			invArray["custpage_checkfield"] = request.getParameter('custpage_checkfield');
			invArray["custpage_binlocation"] = request.getParameter('custpage_binlocation');
			invArray["custpage_item"] = request.getParameter('custpage_item');

		var lpname = "";
		if(request.getParameter('custpage_invtlp') !=null && request.getParameter('custpage_invtlp') !=""){
			var fields = ['custrecord_ebiz_lpmaster_lp'];
			var columns = nlapiLookupField('customrecord_ebiznet_master_lp', request.getParameter('custpage_invtlp'), fields);
			lpname = columns.custrecord_ebiz_lpmaster_lp;
		}
		invArray["custpage_invtlp"] = lpname;

			invArray["custpage_location"] = request.getParameter('custpage_location');
			invArray["custpage_company"] = request.getParameter('custpage_company');
			invArray["custpage_itemstatus"] = request.getParameter('custpage_itemstatus');
			//case 20126182 start 
			invArray["custpage_invtlpid"] = request.getParameter('custpage_invtlp');
			//case 20126182 end
			response.sendRedirect('SUITELET', 'customscript_moveinventory',
					'customdeploy_moveinventory_di', false, invArray);
		}
		else
		{
			var form = nlapiCreateForm('Inventory Move');

			var binLocationField = form.addField('custpage_binlocation', 'select', 'Bin Location','customrecord_ebiznet_location');
			//binLocationField.addSelectOption('', '');

			//Case # 20126884 start
		//	form.setScript('customscript_cs');
			//Case # 20126884 end

			var itemField = form.addField('custpage_item', 'multiselect', 'Item','item');
			var invtlp = form.addField('custpage_invtlp', 'select', 'LP','customrecord_ebiznet_master_lp');
			//invtlp.addSelectOption('', '');
			if(request.getParameter('custpage_invtlp')!=null && request.getParameter('custpage_invtlp')!='')
			{
				invtlp.setDefaultValue(request.getParameter('custpage_invtlp'));
			}

			var varLoc = form.addField('custpage_location', 'select', 'Location','location');
			//varLoc.addSelectOption('', '');
			varLoc.setMandatory(true);

			var varComp = form.addField('custpage_company', 'select', 'Company','customrecord_ebiznet_company');
			if(request.getParameter('custpage_location')!='' && request.getParameter('custpage_location')!=null)
			{
				varLoc.setDefaultValue(request.getParameter('custpage_location'));	
				whloc = request.getParameter('custpage_location');
			}
			
			
			
			// case# 201413036
			if(request.getParameter('custpage_item')!='' && request.getParameter('custpage_item')!=null)
			{
				itemField.setDefaultValue(request.getParameter('custpage_item'));	
				
			}
			
			if(request.getParameter('custpage_binlocation')!='' && request.getParameter('custpage_binlocation')!=null)
			{
				binLocationField.setDefaultValue(request.getParameter('custpage_binlocation'));	
				
			}
			
			
			var vItemStatus = form.addField('custpage_itemstatus', 'select', 'Item Status');
			vItemStatus.addSelectOption('', '');
			var filtersStatus = new Array();
			filtersStatus.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			//Case # 20126884 Start
			if(whloc !=null && whloc!='')
			{
				filtersStatus.push(new nlobjSearchFilter('custrecord_ebizsiteskus', null, 'anyof', whloc));
			}
			//Case # 20126884 End
			var columnsstatus = new Array();
			columnsstatus.push(new nlobjSearchColumn('custrecord_statusdescriptionskustatus'));

			var searchStatus = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, filtersStatus, columnsstatus);

			if (searchStatus != null) 
			{
				for (var i = 0; i < searchStatus.length; i++) 
				{
					var res = form.getField('custpage_itemstatus').getSelectOptions(searchStatus[i].getValue('custrecord_statusdescriptionskustatus'), 'is');
					if (res != null) 
					{
						if (res.length > 0) 
							continue;                    
					}
					vItemStatus.addSelectOption(searchStatus[i].getId(), searchStatus[i].getValue('custrecord_statusdescriptionskustatus'));
				}
			}
			if(request.getParameter('custpage_itemstatus') !=null && request.getParameter('custpage_itemstatus')!='')
				{
				vItemStatus.setDefaultValue(request.getParameter('custpage_itemstatus'));
				}
			var save = form.addField('custpage_save', 'checkbox','isfromlocation').setDisplayType('hidden');

			var button = form.addSubmitButton('Display');


			response.writePage(form);
		}

	}
}
var searchResultsForBinLoc = new Array();
function FillBinLocation(maxno)
{
	var filtersinv = new Array();
	filtersinv[0] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3, 19]);
	if(parseFloat(maxno)!=-1)
	{
		nlapiLogExecution('ERROR', 'maxno', maxno);
		filtersinv[1]=new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno));
	}
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	//santosh
	//columns[3]= new nlobjSearchColumn('internalid');
	columns[3]= new nlobjSearchColumn('id');
	columns[3].setSort(true);
	var searchInventory = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinv, columns);
	if(searchInventory!=null && searchInventory.length>=1000)
	{
		nlapiLogExecution('ERROR', 'searchInventory for binloc', searchInventory.length);
		//var maxno=searchInventory[searchInventory.length-1].getValue(columns[1]);		
		var maxno=searchInventory[searchInventory.length-1].getValue('id');	
		searchResultsForBinLoc.push(searchInventory);
		FillBinLocation(maxno);	

	}
	else
	{
		nlapiLogExecution('ERROR', 'openTaskRecords for binloc2', searchInventory.length);
		searchResultsForBinLoc.push(searchInventory);
	}
	return searchResultsForBinLoc;
}
