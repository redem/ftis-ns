/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inbound/Suitelet/ebiz_putaway_strategy_SL.js,v $
 *     	   $Revision: 1.5 $
 *     	   $Date: 2011/07/21 08:04:08 $
 *     	   $Author: pattili $
 *     	   $Name:  $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_putaway_strategy_SL.js,v $
 * Revision 1.5  2011/07/21 08:04:08  pattili
 * CASE201112/CR201113/LOG201121
 * Corrected few minor issues
 *
 * Revision 1.4  2011/07/21 07:58:55  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 * Added this file again as it was deleted from CVS by mistake
 *
 *****************************************************************************/
 
function getLocationSuitelet(request, response){
    if (request.getMethod() == 'GET') {
        //create the Suitelet form
        var form = nlapiCreateForm('Get Location');
  
       var item_sel = form.addField('custpage_item', 'select', 'Item', 'item');
	    item_sel.setLayoutType('normal', 'startrow');
	    
	    var packcode_sel = form.addField('custpage_packcode', 'select', 'Pack Code', 'customlist73');
	    packcode_sel.setLayoutType('normal', 'startrow');
	    
	    var UOM_sel = form.addField('custpage_uom', 'select', 'UOM', 'customlist75');
	    UOM_sel.setLayoutType('normal', 'startrow');
	    
	    var status_sel = form.addField('custpage_status', 'select', 'Item Status', 'customlist_itemstsnew');
	    status_sel.setLayoutType('normal', 'startrow');
	  
	    var reqcube_txt = form.addField('custpage_reqcube', 'text', 'Required Cube');
	    reqcube_txt.setLayoutType('normal', 'startrow');
	    
		var TaskID_txt = form.addField('custpage_taskid', 'text', 'Task ID');
	    TaskID_txt.setLayoutType('normal', 'startrow');
	    
				      
        form.addSubmitButton('Get Locations');  
        response.writePage(form);
      
    }
    else 
    {  
       // Function Input Parameters
       var Item,ItemFamily,ItemGroup,PackCode,UOM,ItemStatus,ItemCube=0,RemCube=0;
       var location_found=false;
	   var Location,LocID;
	   var LocationCube=0;
	   var LocArry = new Array();
	   var TaskID;
       
       Item = request.getParameter('custpage_item');
       ItemCube=request.getParameter('custpage_reqcube');         
       PackCode=request.getParameter('custpage_packcode');
       UOM=request.getParameter('custpage_uom');
       ItemStatus=request.getParameter('custpage_status');
	   TaskID=request.getParameter('custpage_taskid');
	   TaskID=MoveTaskRecord(TaskID);
	   
	   response.write("TaskID= " + TaskID);
    }    
 }
 