/***************************************************************************
  eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Outbound/Suitelet/ebiz_RF_Shipping_ShipLP.js,v $
 *     	   $Revision: 1.2.2.2.4.3.2.10 $
 *     	   $Date: 2014/06/13 11:00:36 $
 *     	   $Author: skavuri $
 *     	   $Name: t_NSWMS_2014_1_3_125 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Shipping_ShipLP.js,v $
 * Revision 1.2.2.2.4.3.2.10  2014/06/13 11:00:36  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.2.2.2.4.3.2.9  2014/06/06 08:04:59  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.2.2.2.4.3.2.8  2014/05/30 00:41:06  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.2.2.2.4.3.2.7  2013/10/15 15:43:24  rmukkera
 * Case# 20124192�
 *
 * Revision 1.2.2.2.4.3.2.6  2013/08/23 05:59:00  snimmakayala
 * Case# 20124029
 * NLS - UAT ISSUES(RF BSU IS ACCEPTING ANY SHIP LP IRRESPECTIVE OF SITE)
 *
 * Revision 1.2.2.2.4.3.2.5  2013/06/11 14:30:20  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.2.2.2.4.3.2.4  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.2.2.2.4.3.2.3  2013/04/03 01:23:49  kavitha
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.2.2.2.4.3.2.2  2013/03/26 13:27:42  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.2.2.2.4.3.2.1  2013/03/05 14:47:50  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Lexjet production as part of Standard bundle
 *
 * Revision 1.2.2.2.4.3  2013/02/15 14:59:20  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.2.2.2.4.2  2012/12/24 13:22:22  schepuri
 * CASE201112/CR201113/LOG201121
 * added new screen for multiple sites
 *
 * Revision 1.2.2.2.4.1  2012/09/25 07:13:53  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Language
 *
 * Revision 1.2.2.2  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.2.2.1  2012/02/22 13:04:24  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.2  2011/12/12 06:52:15  rgore
 * CASE201112/CR201113/LOG201121
 * Sanitized error message for spelling and grammar.
 * - Ratnakar
 * 12 Dec 2011
 *
 * Revision 1.1  2011/09/23 16:22:25  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.4  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.3  2011/04/21 07:42:42  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * RF Packing Functionality
 *
 * Revision 1.2  2011/04/21 07:03:02  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * RF Packing Functionality
 *
 * Revision 1.1  2011/04/19 14:19:06  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * RF Packing Functionality
 *
 *
 *****************************************************************************/
function EnterSHIPLP(request, response)
{
	var matchFound = true;
	if (request.getMethod() == 'GET') 
	{   
		nlapiLogExecution('ERROR', 'Request Starts');
		var whLocation = request.getParameter('custparam_locationId');
		nlapiLogExecution('ERROR', 'whLocation', whLocation);
		/*var getOrderno = request.getParameter('custparam_orderno');
        var getRecordInternalId = request.getParameter('custparam_recordinternalid');
        var getContainerLpNo = request.getParameter('custparam_containerlpno');
        var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
        var getBeginLocationId = request.getParameter('custparam_beginLocation');
         var getItem = request.getParameter('custparam_item');
        var getItemDescription = request.getParameter('custparam_itemdescription');
        var getItemInternalId = request.getParameter('custparam_iteminternalid');
        var getDOLineId = request.getParameter('custparam_dolineid');
        var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
        var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');        
        var getEnteredLocation = request.getParameter('custparam_endlocation');
        var getBeginBinLocation = request.getParameter('custparam_beginlocationname');
        var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var vclusterNo = request.getParameter('custparam_clusterno');		
        var Itype = nlapiLookupField('item', getItemInternalId, 'recordType');
        var ItemRec = nlapiLoadRecord(Itype, getItemInternalId);
		var vBatchno = request.getParameter('custparam_batchno');
		var vlinecount = request.getParameter('custparam_linecount');
		var vloopcount = request.getParameter('custparam_loopcount'); 
		 */


		var getLanguage = request.getParameter('custparam_language');	
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);

		var st0,st1,st2,st3;
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "ENTER / SCAN LP SHIP #:";
			st2 = "ENVIAR";
			st3 = "ANTERIOR";

		}
		else
		{
			st0 = "";
			st1 = "ENTER/SCAN SHIP LP#:";
			st2 = "SEND";
			st3 = "PREV";
		}    	
		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head><title>" + st0 + " </title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends    
		//html = html + " document.getElementById('enterlp').focus();";        
		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		//html = html + "	  alert(document.getElementById('cmdSend').disabled);";

		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";        
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1;	
		html = html + "				<input type='hidden' name='hdnwhLocation' value=" + whLocation + ">";
		/*
        html = html + "				<input type='hidden' name='hdnOrderNo' value=" + getOrderno + ">";
        html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
        html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
        html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
        html = html + "				<input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
        html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
        html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
        html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
        html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
        html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
        html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";        
        html = html + "				<input type='hidden' name='hdnBeginBinLocation' value=" + getBeginBinLocation + ">";
        html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vclusterNo + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value=" + vBatchno + ">";
		html = html + "				<input type='hidden' name='hdnlinecount' value=" + vlinecount + ">";
		html = html + "				<input type='hidden' name='hdnloopcount' value=" + vloopcount + ">";
		 */
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlp' id='enterlp' type='text'/>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					" + st3 + " <input name='cmdPrevious' type='submit' value='F7'/>";        
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlp').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var varEnteredLP = request.getParameter('enterlp');
		var whLocation = request.getParameter('hdnwhLocation');
		nlapiLogExecution('DEBUG', 'Entered SHIP LP', varEnteredLP);
		nlapiLogExecution('DEBUG', 'whLocation', whLocation);
		var SOarray = new Array();
		SOarray["custparam_locationId"]=whLocation;
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');       

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to Shipping menu.
		if (optedEvent == 'F7') {
			//response.sendRedirect('SUITELET', 'customscriptrf_shiplp_location', 'customdeployrf_shiplp_location_di', false, SOarray);
			response.sendRedirect('SUITELET', 'customscript_rf_shipping_menu', 'customdeploy_rf_shipping_menu_di', false, SOarray);
			return;
		}
		else 
		{        
			if (varEnteredLP != '' && varEnteredLP != null) 
			{
				SOarray["custparam_shiplpno"] = varEnteredLP;
				nlapiLogExecution('DEBUG', 'Enterd SHIP LP', varEnteredLP);	 
				lpErrorMessage= MastLPExists(varEnteredLP,whLocation);//check for manual ship lp exists
				if(lpErrorMessage=="")
				{
					NewShipLPNo=varEnteredLP;
					SOarray["custparam_shiplpno"] = NewShipLPNo;
					nlapiLogExecution('DEBUG', 'SOarray["custparam_shiplpno"]', SOarray["custparam_shiplpno"]);	 
					response.sendRedirect('SUITELET', 'customscript_rf_shipping_cartonlp', 'customdeploy_rf_shipping_cartonlp_di', false, SOarray);
				}
				else
				{
					SOarray["custparam_error"] = lpErrorMessage;
					SOarray["custparam_locationId"]=whLocation;
					SOarray["custparam_screenno"] = '38';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				}

				return;
			}
			else //To go to Ship LP Confirmation
			{
				SOarray["custparam_shiplpno"] = '';
				response.sendRedirect('SUITELET', 'customscript_rf_shipping_lpconfirm', 'customdeploy_rf_shipping_lpconfirm_di', false, SOarray);
				nlapiLogExecution('DEBUG', 'SHIP LP Not Entered', varEnteredLP);
				nlapiLogExecution('DEBUG', 'Redirecting to confirmation for auto generated', varEnteredLP);
				return;
			}	 
		}
	}
}
/**
 * To check manually generated Ship LP# exists in master LP or not
 * 
 * @param manualShipLP   
 * @returns message with proper error message
 */
function MastLPExists(manualShipLP,whLocation)
{
	var message="";
	var filters =new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', manualShipLP));

	var columns = new Array();
	columns.push(new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_lpmaster_wmsstatusflag'));

	// To get the data from Master LP based on selection criteria
	searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
	if(searchresults!= null && searchresults.length>0)
	{
		// Case # 20124192�(System is allowing Existing(duplicate)LPs) Start
		/*var blnClosedLP=false;
		for(var i=0;i<searchresults.length;i++)
		{
			if(searchresults[i].getValue('custrecord_ebiz_lpmaster_wmsstatusflag')=='28') //STATUS.OUTBOUND.PACK_COMPLETE
			{
				blnClosedLP=true; 
				break;
			}					
		}
		if(blnClosedLP==true)
		{ 
			message="Ship LP# already exists, Please enter another Ship LP#";
			nlapiLogExecution('DEBUG',"message ",message );
			return message;

		}
		else
			return message;		*/	
		message="Ship LP# already exists, Please enter another Ship LP#";
		return message;
		
		// Case # 20124192� End
	}
	else
	{
		
		nlapiLogExecution('DEBUG',"whlocation ",whLocation );
		
		if(ebiznet_LPRange_CL_withLPType(manualShipLP,'2','3',whLocation))// for User Defined  with SHIP LPType
		{
			return message;
		}
		else
		{
			message = "Ship LP# is out of range.";
			return message;
			//showInlineMessage(form, 'DEBUG', 'Ship LP# is outof range', "");
			//response.writePage(form); 
		}
	}
}
