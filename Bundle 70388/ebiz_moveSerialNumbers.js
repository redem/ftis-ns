/***************************************************************************
 eBizNET Solutions.

 ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inbound/Suitelet/Attic/ebiz_moveSerialNumbers.js,v $
 *     	   $Revision: 1.1.2.1.2.1 $
 *     	   $Date: 2013/09/13 15:39:17 $
 *     	   $Author: rmukkera $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * *************************************/


function MoveserialNumbers(request, response){
	if (request.getMethod() == 'GET') 
	{

		var form = nlapiCreateForm('Move Serial Numbers');

		var lp=form.addField('custpage_lp','text','LP');
		//lp.addSelectOption('', '');

		form.addSubmitButton('Display');

//		var filters = new Array();
//		filters.push(new nlobjSearchFilter('custrecord_tempserialstatus', null, 'is','F',null));
//		filters.push(new nlobjSearchFilter('custrecord_tempserialparentid', null, 'isnotempty'));
//
//		var columns = new Array();
//		columns.push(new nlobjSearchColumn('custrecord_tempserialparentid'));
//
//		var searchlpresults = nlapiSearchRecord('customrecord_ebiznettempserialentry', null, filters, columns);
//
//		nlapiLogExecution('ERROR', 'searchlpresults.length', searchlpresults.length);
//
//		if (searchlpresults != null) 
//		{
//			for (var i = 0; i < searchlpresults.length; i++) 
//			{
//				var res = form.getField('custpage_lp').getSelectOptions(searchlpresults[i].getValue('custrecord_tempserialparentid'), 'is');
//				if (res != null) 
//				{
//					if (res.length > 0) 
//						continue;                    
//				}
//
//				lp.addSelectOption(searchlpresults[i].getValue('custrecord_tempserialparentid'), searchlpresults[i].getValue('custrecord_tempserialparentid'));
//			}
//		}
//		nlapiLogExecution('ERROR', 'searchlpresults.length', searchlpresults.length);


		response.writePage(form);
	}
	else{
		nlapiLogExecution('ERROR', 'Post', 'Post');
		var form = nlapiCreateForm('Move Serial Numbers');

		var tempflag = form.addField('custpage_tempflag', 'text','tempory flag').setDisplayType('hidden');


		form.setScript('customscript_ebiz_moveserialnumbers');
		form.addButton('custombutton','Move','moveSerialfunction()');

		var tempflag = request.getParameter('custpage_tempflag');
		if (tempflag == 'move'){
			var lp=form.addField('custpage_lp','text','LP');
//			lp.addSelectOption('', '');
//
		form.addSubmitButton('Display');
//
//			var filters = new Array();
//			filters.push(new nlobjSearchFilter('custrecord_tempserialstatus', null, 'is','F',null));
//			filters.push(new nlobjSearchFilter('custrecord_tempserialparentid', null, 'isnotempty'));
//
//			var columns = new Array();
//			columns.push(new nlobjSearchColumn('custrecord_tempserialparentid'));
//
//			var searchlpresults = nlapiSearchRecord('customrecord_ebiznettempserialentry', null, filters, columns);
//
//			nlapiLogExecution('ERROR', 'searchlpresults.length', searchlpresults.length);
//
//			if (searchlpresults != null) 
//			{
//				for (var i = 0; i < searchlpresults.length; i++) 
//				{
//					var res = form.getField('custpage_lp').getSelectOptions(searchlpresults[i].getValue('custrecord_tempserialparentid'), 'is');
//					if (res != null) 
//					{
//						if (res.length > 0) 
//							continue;                    
//					}
//
//					lp.addSelectOption(searchlpresults[i].getValue('custrecord_tempserialparentid'), searchlpresults[i].getValue('custrecord_tempserialparentid'));
//				}
//			}
//			nlapiLogExecution('ERROR', 'searchlpresults.length', searchlpresults.length);


			response.writePage(form);

			var count=request.getLineItemCount("custpage_serialnoslist");
			for(var i=0; i<count;i++)
			{
				var lineno=i+1;
				var isChecked=request.getLineItemValue("custpage_serialnoslist","custpage_vcnf",lineno);

				if(isChecked=="T")
				{


					nlapiLogExecution('ERROR', 'isCheckedT', 'isCheckedT');
					var vserial=request.getLineItemValue("custpage_serialnoslist","custpage_vserial",lineno);

					var filter=new Array();
					filter[0]=new nlobjSearchFilter('custrecord_serialnumber',null,'is',vserial);

					var column= new Array();

					column[0]=new nlobjSearchColumn('custrecord_serialnumber');

					var searchresults=nlapiSearchRecord('customrecord_ebiznetserialentry',null,filter,column);

					var vserialno='';
					if(searchresults!="" && searchresults!=null){
						for(var count=0;count<searchresults.length;count++)
						{

							var lineno=count+1;
							vserialno=searchresults[count].getValue('custrecord_serialnumber');

						}

					}



					if(vserial!=vserialno){

						nlapiLogExecution('ERROR', 'ischeckedtempserial', vserial);
						nlapiLogExecution('ERROR', 'ischeckedvserial', vserialno);

						var vlp=request.getLineItemValue("custpage_serialnoslist","custpage_vlp",lineno);


						var vorderno=request.getLineItemValue("custpage_serialnoslist","custpage_vtempserialorderno",lineno);
						var vebizorderno=request.getLineItemValue("custpage_serialnoslist","custpage_vtempserialebizorderno",lineno);
						var vpolineno=request.getLineItemValue("custpage_serialnoslist","custpage_vtempserialpolineno",lineno);
						var vitem=request.getLineItemValue("custpage_serialnoslist","custpage_vtempserialitem",lineno);
						var viteminternalid=request.getLineItemValue("custpage_serialnoslist","custpage_vtempserialiteminternalid",lineno);
						var vqty=request.getLineItemValue("custpage_serialnoslist","custpage_vtempserialqty",lineno);
						var vuomid=request.getLineItemValue("custpage_serialnoslist","custpage_vtempserialuomid",lineno);
						var vcontlpno=request.getLineItemValue("custpage_serialnoslist","custpage_vtempserialcontlpno",lineno);
						var vwmsstatus=request.getLineItemValue("custpage_serialnoslist","custpage_vtempserialwmsstatus",lineno);
						var vlpno=request.getLineItemValue("custpage_serialnoslist","custpage_vtempseriallpno",lineno);
						var vcompany=request.getLineItemValue("custpage_serialnoslist","custpage_vtempserialcompany",lineno);
						var vlocation=request.getLineItemValue("custpage_serialnoslist","custpage_vtempseriallocation",lineno);
						var vbinlocation=request.getLineItemValue("custpage_serialnoslist","custpage_vtempserialbinlocation",lineno);
						var vchild=request.getLineItemValue("custpage_serialnoslist","custpage_vtempserialchild",lineno);

						var vrecid=request.getLineItemValue("custpage_serialnoslist","custpage_vrecid",lineno);


						nlapiLogExecution('ERROR', 'ischeckedvlp', vlp);
						nlapiLogExecution('ERROR', 'ischeckedvserial', vserial);
						nlapiLogExecution('ERROR', 'ischeckedvrecid', vrecid);
						nlapiLogExecution('ERROR', 'vebizorderno', vebizorderno);

						//To move values from temp serial entry record to serial entry record.
						var res=nlapiCreateRecord("customrecord_ebiznetserialentry");
						var trantype = nlapiLookupField('transaction', vebizorderno, 'recordType');
						res.setFieldValue("name",vlp);
						if(trantype=='purchaseorder')
						{
						res.setFieldValue("custrecord_serialpono",vorderno);
						res.setFieldValue("custrecord_serialebizpono",vebizorderno);
						res.setFieldValue("custrecord_serialpolineno",vpolineno);
						}
						else
						{
							res.setFieldValue("custrecord_ebiz_serial_rmano",vorderno);
							res.setFieldValue("custrecord_serialebizrmano",vebizorderno);
							res.setFieldValue("custrecord_ebiz_serial_rmalineno",vpolineno);
						}
						res.setFieldValue("custrecord_serialitem",vitem);
						res.setFieldValue("custrecord_serialiteminternalid",viteminternalid);
						res.setFieldValue("custrecord_serialparentid",vlp);
						res.setFieldValue("custrecord_serialqty",vqty);
						res.setFieldValue("custrecord_serialnumber",vserial);
						res.setFieldValue("custrecord_serialuomid",vuomid);
						res.setFieldValue("custrecord_serialcontlpno",vcontlpno);
						res.setFieldValue("custrecord_serialwmsstatus",1);
						res.setFieldValue("custrecord_serial_lpno",vlpno);
						res.setFieldValue("custrecord_serial_company",vcompany);
						res.setFieldValue("custrecord_serial_location",vlocation);
						res.setFieldValue("custrecord_serialbinlocation",vbinlocation);
						res.setFieldValue("custrecord_ebiz_serial_child",vchild);

						nlapiSubmitRecord(res);


						//To update the status in temp serial entry custom record for moved serialno's.
						var restemp=nlapiLoadRecord("customrecord_ebiznettempserialentry",vrecid);
						restemp.setFieldValue("custrecord_tempserialstatus",'T');

						nlapiSubmitRecord(restemp);
					}
				}	

			}

		}
		else{


			var lp=form.addField('custpage_lp','text','LP');
			//lp.addSelectOption('', '');

			form.addSubmitButton('Display');



//			var filters = new Array();
//			filters.push(new nlobjSearchFilter('custrecord_tempserialstatus', null, 'is','F',null));
//			filters.push(new nlobjSearchFilter('custrecord_tempserialparentid', null, 'isnotempty'));
//
//			var columns = new Array();
//			columns.push(new nlobjSearchColumn('custrecord_tempserialparentid'));
//
//			var searchlpresults = nlapiSearchRecord('customrecord_ebiznettempserialentry', null, filters, columns);
//
//			nlapiLogExecution('ERROR', 'searchlpresults.length', searchlpresults.length);
//
//			if (searchlpresults != null) 
//			{
//				for (var i = 0; i < searchlpresults.length; i++) 
//				{
//					var res = form.getField('custpage_lp').getSelectOptions(searchlpresults[i].getValue('custrecord_tempserialparentid'), 'is');
//					if (res != null) 
//					{
//						if (res.length > 0) 
//							continue;                    
//					}
//
//					lp.addSelectOption(searchlpresults[i].getValue('custrecord_tempserialparentid'), searchlpresults[i].getValue('custrecord_tempserialparentid'));
//				}
//			}


			var vlp=request.getParameter('custpage_lp');

			nlapiLogExecution('ERROR', 'vlp', vlp);

			lp.setDefaultValue(vlp);

			var sublist = form.addSubList('custpage_serialnoslist', 'list', 'Move Serial');
			sublist.addMarkAllButtons();
			sublist.addField('custpage_vcnf', 'checkbox', 'Select');
			sublist.addField('custpage_vslno','text','SL');
			sublist.addField('custpage_vlp', 'text', 'LP');
			sublist.addField('custpage_vserial','text','Serial');
			

			sublist.addField('custpage_vtempserialorderno','text','orderno').setDisplayType('hidden');	
			sublist.addField('custpage_vtempserialebizorderno','text','ebizorderno').setDisplayType('hidden');
			sublist.addField('custpage_vtempserialpolineno', 'text', 'polineno').setDisplayType('hidden');	
			sublist.addField('custpage_vtempserialitem','text','item').setDisplayType('hidden');	
			sublist.addField('custpage_vtempserialiteminternalid', 'text', 'iteminternalid').setDisplayType('hidden');	
			sublist.addField('custpage_vtempserialqty','text','qty').setDisplayType('hidden');	
			sublist.addField('custpage_vtempserialuomid', 'text', 'uomid').setDisplayType('hidden');	
			sublist.addField('custpage_vtempserialcontlpno','text','contlpno').setDisplayType('hidden');	
			sublist.addField('custpage_vtempserialwmsstatus', 'text', 'wmsstatus').setDisplayType('hidden');	
			sublist.addField('custpage_vtempseriallpno','text','lpno').setDisplayType('hidden');	
			sublist.addField('custpage_vtempserialcompany', 'text', 'company').setDisplayType('hidden');	
			sublist.addField('custpage_vtempseriallocation','text','location').setDisplayType('hidden');	
			sublist.addField('custpage_vtempserialbinlocation', 'text', 'binlocation').setDisplayType('hidden');	
			sublist.addField('custpage_vtempserialchild','text','child').setDisplayType('hidden');	
			sublist.addField("custpage_vrecid", "text", "rec id").setDisplayType('hidden');



			var filter=new Array();
			filter[0]=new nlobjSearchFilter('custrecord_tempserialparentid',null,'is',vlp);
			filter[1]=new nlobjSearchFilter('custrecord_tempserialstatus',null,'is','F',null);


			var column= new Array();

			column[0]=new nlobjSearchColumn('custrecord_tempserialparentid');
			column[1]=new nlobjSearchColumn('custrecord_tempserialnumber');
			column[2]=new nlobjSearchColumn('custrecord_tempserialorderno');
			column[3]=new nlobjSearchColumn('custrecord_tempserialpolineno');
			column[4]=new nlobjSearchColumn('custrecord_tempserialitem');
			column[5]=new nlobjSearchColumn('custrecord_tempserialiteminternalid');
			column[6]=new nlobjSearchColumn('custrecord_tempserialqty');
			column[7]=new nlobjSearchColumn('custrecord_tempserialuomid');
			column[8]=new nlobjSearchColumn('custrecord_tempserialcontlpno');
			column[9]=new nlobjSearchColumn('custrecord_tempserialwmsstatus');
			column[10]=new nlobjSearchColumn('custrecord_tempserial_lpno');
			column[11]=new nlobjSearchColumn('custrecord_tempserial_company');
			column[12]=new nlobjSearchColumn('custrecord_tempserial_location');
			column[13]=new nlobjSearchColumn('custrecord_tempserialbinlocation');
			column[14]=new nlobjSearchColumn('custrecord_ebiz_tempserial_child');




			var searchresults=nlapiSearchRecord('customrecord_ebiznettempserialentry',null,filter,column);
			if(searchresults!="" && searchresults!=null){
				for(var count=0;count<searchresults.length;count++)
				{

					var lineno=count+1;

					var slno=parseInt(lineno);
					var vlpno=searchresults[count].getValue('custrecord_tempserialparentid');
					var vserialno=searchresults[count].getValue('custrecord_tempserialnumber');

					var vtempserialorderno=searchresults[count].getText('custrecord_tempserialorderno');
					var vtempserialebizorderno=searchresults[count].getValue('custrecord_tempserialorderno');
					var vtempserialpolineno=searchresults[count].getValue('custrecord_tempserialpolineno');
					var vtempserialitem=searchresults[count].getValue('custrecord_tempserialitem');
					var vtempserialiteminternalid=searchresults[count].getValue('custrecord_tempserialiteminternalid');
					var vtempserialqty=searchresults[count].getValue('custrecord_tempserialqty');
					var vtempserialuomid=searchresults[count].getValue('custrecord_tempserialuomid');
					var vtempserialcontlpno=searchresults[count].getValue('custrecord_tempserialcontlpno');
					var vtempserialwmsstatus=searchresults[count].getValue('custrecord_tempserialwmsstatus');
					var vtempseriallpno=searchresults[count].getValue('custrecord_tempserial_lpno');
					var vtempserialcompany=searchresults[count].getValue('custrecord_tempserial_company');
					var vtempseriallocation=searchresults[count].getValue('custrecord_tempserial_location');
					var vtempserialbinlocation=searchresults[count].getValue('custrecord_tempserialbinlocation');
					var vtempserialchild=searchresults[count].getValue('custrecord_ebiz_tempserial_child');
					var recid=searchresults[count].getId();


					nlapiLogExecution('ERROR', 'vlpno', vlpno);
					nlapiLogExecution('ERROR', 'vserialno', vserialno);

					form.getSubList('custpage_serialnoslist').setLineItemValue('custpage_vslno', lineno, slno);
					form.getSubList('custpage_serialnoslist').setLineItemValue('custpage_vlp', lineno, vlpno);
					form.getSubList('custpage_serialnoslist').setLineItemValue('custpage_vserial', lineno, vserialno);

					form.getSubList('custpage_serialnoslist').setLineItemValue('custpage_vtempserialorderno', lineno, vtempserialorderno);
					form.getSubList('custpage_serialnoslist').setLineItemValue('custpage_vtempserialebizorderno', lineno, vtempserialebizorderno);					
					form.getSubList('custpage_serialnoslist').setLineItemValue('custpage_vtempserialpolineno', lineno, vtempserialpolineno);
					form.getSubList('custpage_serialnoslist').setLineItemValue('custpage_vtempserialitem', lineno, vtempserialitem);
					form.getSubList('custpage_serialnoslist').setLineItemValue('custpage_vtempserialiteminternalid', lineno, vtempserialiteminternalid);
					form.getSubList('custpage_serialnoslist').setLineItemValue('custpage_vtempserialqty', lineno, vtempserialqty);
					form.getSubList('custpage_serialnoslist').setLineItemValue('custpage_vtempserialuomid', lineno, vtempserialuomid);
					form.getSubList('custpage_serialnoslist').setLineItemValue('custpage_vtempserialcontlpno', lineno, vtempserialcontlpno);
					form.getSubList('custpage_serialnoslist').setLineItemValue('custpage_vtempserialwmsstatus', lineno, vtempserialwmsstatus);
					form.getSubList('custpage_serialnoslist').setLineItemValue('custpage_vtempseriallpno', lineno, vtempseriallpno);
					form.getSubList('custpage_serialnoslist').setLineItemValue('custpage_vtempserialcompany', lineno, vtempserialcompany);
					form.getSubList('custpage_serialnoslist').setLineItemValue('custpage_vtempseriallocation', lineno, vtempseriallocation);
					form.getSubList('custpage_serialnoslist').setLineItemValue('custpage_vtempserialbinlocation', lineno, vtempserialbinlocation);
					form.getSubList('custpage_serialnoslist').setLineItemValue('custpage_vtempserialchild', lineno, vtempserialchild);
					form.getSubList('custpage_serialnoslist').setLineItemValue('custpage_vrecid', lineno, recid);





				}
			}

		}

	}

	response.writePage(form);
}
function moveSerialfunction(){

	nlapiSetFieldValue('custpage_tempflag','move');
	NLDoMainFormButtonAction("submitter",true);
}