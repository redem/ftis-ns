/***************************************************************************
 eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_Packing_SL_Bulk.js,v $
 *     	   $Revision: 1.1.2.1.2.3.8.1 $
 *     	   $Date: 2015/09/23 14:57:57 $
 *     	   $Author: deepshikha $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_13 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_Packing_SL_Bulk.js,v $
 * Revision 1.1.2.1.2.3.8.1  2015/09/23 14:57:57  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.1.2.1.2.3  2013/06/20 14:57:58  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * FM Multi location issues
 *
 * Revision 1.1.2.1.2.2  2013/03/19 11:48:12  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.1.2.1.2.1  2013/03/01 15:01:12  rmukkera
 * code is merged from FactoryMation production as part of Standard bundle
 *
 * Revision 1.1.2.1  2013/02/19 00:54:22  gkalla
 * CASE201112/CR201113/LOG201121
 * Issue fixing
 *
 * Revision 1.1.2.4  2012/08/22 15:52:16  gkalla
 * CASE201112/CR201113/LOG201121
 * Packing for TO and orders filling for morethan 1000
 *
 * Revision 1.1.2.3  2012/07/31 10:56:30  schepuri
 * CASE201112/CR201113/LOG201121
 * Issue related to Auto Assign LP
 *
 * Revision 1.1.2.2  2012/06/15 15:46:04  gkalla
 * CASE201112/CR201113/LOG201121
 * Packing
 *
 * Revision 1.1.2.1  2012/06/14 17:34:28  gkalla
 * CASE201112/CR201113/LOG201121
 * GUI Packing
 *
 * Revision 1.7.2.2  2012/04/20 09:35:32  vrgurujala
 * CASE201112/CR201113/LOG201121
 * t_NSWMS_LOG201121_120
 *
 * Revision 1.7.2.1  2012/04/12 21:44:55  spendyala
 * CASE201112/CR201113/LOG201121
 * Showing meaningful data instead of showing incomplete data.
 *
 * Revision 1.7  2011/12/22 18:42:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Shipping
 *
 * Revision 1.6  2011/09/12 07:04:55  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.5  2011/08/27 06:48:26  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Ship Manifest related fixes.
 *
 * Revision 1.4  2011/08/25 16:06:35  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Ship Manifest related fixes.
 *
 * Revision 1.3  2011/08/24 15:24:27  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pack Location is added to the sublist and PACK task creation.
 *
 * Revision 1.2  2011/08/23 13:30:27  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Comments and Logs added
 *
 * Revision 1.1  2011/08/23 12:54:38  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Assign Pack Station - New Screen developed
 * 
 *****************************************************************************/

function ebiz_AssignPackStation_SL(request, response)
{
	if (request.getMethod() == 'GET'){
		// Call method to render the Packing criteria
		showAssignPackStationCriteria(request,response);
	} 
	else{
		if (request.getParameter('custpage_orderno') != null){
			nlapiLogExecution('ERROR', 'Displaying fulfillment order search results');

			var form = nlapiCreateForm('Packing');
			form.setScript('customscript_ebiz_packing_bulk_cl');
			// Call method to display the selected fulfillment order candidates
			displayPossibleFulfillmentOrders(request, response, form);
		}
		else{
			nlapiLogExecution('ERROR', 'Perform Packing for Selected Containers');

			var form = nlapiCreateForm('Packing');
			form.setScript('customscript_ebiz_packing_bulk_cl');
			processSelectedContainers(request, response, form);
		}
		response.writePage(form);	
	}
}


/**
 * This method will create a ship manifest record
 * @param vOrdNo
 * @param containerlp
 */
function CreateShipManifestRecord(vOrdNo,containerlp,compid,siteid,weight)
{
	try {
		nlapiLogExecution('ERROR','into CreateShipManifestRecord');
		nlapiLogExecution('ERROR','vOrdNo',vOrdNo);
		nlapiLogExecution('ERROR','containerlp',containerlp); 
		if (vOrdNo != null && vOrdNo != "") { 
			var vCarrierType=getCarrierType(vOrdNo);
			if(vCarrierType == null || vCarrierType == '')
				vCarrierType='PC';
			CreateShippingManifestRecord(vOrdNo,containerlp,vCarrierType,null,weight);			
		}

	}
	catch (e) {
		if (e instanceof nlobjError) 
			lapiLogExecution('ERROR', 'system error', e.getCode() + '\n' + e.getDetails());
		else 
			nlapiLogExecution('ERROR', 'unexpected error', e.toString());
	}
}


/**
 * Function to render the search criteria to display all the possible containers
 * that can be packed based on the provided search criteria
 * 
 * @param request
 * @param response
 */
function showAssignPackStationCriteria(request, response){
	var form = nlapiCreateForm('Packing');
	//form.setScript('customscript_ebiz_packing_cl');
	// Fullfillment order number in search criteria
	//var selectSO = form.addField('custpage_orderno', 'select', 'Order #');
	var selectSO = form.addField('custpage_orderno', 'text', 'Order #');
	var eBizcontlp=form.addField('custpage_ebizcontlp','text','Carton #');

	//selectSO.addSelectOption("","");

	// Retrieve all sales orders
	//var salesOrderList = getAllSalesOrders(-1);

	// Add all sales orders to SO Field
	//addAllSalesOrdersToField(form,selectSO, salesOrderList);

	var button = form.addSubmitButton('Display');
	response.writePage(form);
}

/**
 * Function to retrieve sales orders
 * @returns
 */
/*function getAllSalesOrders(){
	var filtersSO = new Array();		
	filtersSO.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//Status Flag - C (Picks Confirmed)		
	filtersSO.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK
	filtersSO.push(new nlobjSearchFilter('recordType', 'custrecord_ebiz_order_no', 'is', 'salesorder'));
	var colsSO = new Array();
	//colsSO[0] = new nlobjSearchColumn('name');
	colsSO[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	colsSO[0].setSort(true);
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersSO, colsSO);

	return searchresults;
}*/
var vOrdArr=new Array();
function getAllSalesOrders(maxno){
	var filtersSO = new Array();		
	filtersSO.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//Status Flag - C (Picks Confirmed)		
	filtersSO.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK
	filtersSO.push(new nlobjSearchFilter('recordType', 'custrecord_ebiz_order_no', 'is', 'salesorder'));
	filtersSO.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is', 'T'));
	if(maxno!=-1)
	{
		filtersSO.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno)));
	}

	var columnsinvt = new Array();
	columnsinvt[0] = new nlobjSearchColumn('id');	 
	columnsinvt[1] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columnsinvt[1].setSort(true);

	//columnsinvt[2] = new nlobjSearchColumn('custrecord_route_no');

	//OrderField.addSelectOption("", "");


	var customerSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersSO,columnsinvt);

	for (var i = 0; customerSearchResults != null && i < customerSearchResults.length; i++) {


		vOrdArr.push(customerSearchResults[i]);
	}

	if(customerSearchResults!=null && customerSearchResults.length>=1000)
	{
		var maxno=customerSearchResults[customerSearchResults.length-1].getValue('id');		
		getAllSalesOrders(maxno);	
	}
	return vOrdArr;
}

/**
 * Function to add all sales orders in the list passed to the passed field
 * 
 * @param selectSO
 * @param salesOrderList
 */
function addAllSalesOrdersToField(form,selectSO, salesOrderList){
	if(salesOrderList != null && salesOrderList.length > 0){
		for (var i = 0; i < salesOrderList.length; i++) 
		{
			nlapiLogExecution('ERROR','orderno',salesOrderList[i].getText('custrecord_ebiz_order_no'));
			var res=  form.getField('custpage_orderno').getSelectOptions(salesOrderList[i].getText('custrecord_ebiz_order_no'), 'is');
			if (res != null) {
				if (res.length > 0) {
					continue;
				}
			}
			selectSO.addSelectOption(salesOrderList[i].getValue('custrecord_ebiz_order_no'), salesOrderList[i].getText('custrecord_ebiz_order_no'));
		}
	}
}
function GetSOInternalId(SOText)
{
	nlapiLogExecution('ERROR','Into GetSOInternalId (Input)',SOText);

	var ActualSoID='-1';

	var filter=new Array();
	filter.push(new nlobjSearchFilter('tranid',null,'is',SOText.toString()));
	filter.push(new nlobjSearchFilter('mainline',null,'is','F'));

	var columns=new Array();
	columns.push(new nlobjSearchColumn('shipmethod'));

	var searchrec=nlapiSearchRecord('salesorder',null,filter,columns);
	if(searchrec==null)
	{
		searchrec=nlapiSearchRecord('transferorder',null,filter,columns);
	}
	if(searchrec!=null && searchrec!='' && searchrec.length>0)
	{
		ActualSoID=searchrec[0].getId();
	}

	nlapiLogExecution('ERROR','Out of GetSOInternalId (Output)',ActualSoID);

	return ActualSoID;
}
/**
 * 
 * @param request
 * @param response
 * @param form
 */
function displayPossibleFulfillmentOrders(request, response, form){

	var ordernomain = request.getParameter('custpage_orderno');
	var ebizContlp=request.getParameter('custpage_ebizcontlp');
	nlapiLogExecution('ERROR','orderno',ordernomain);
	if(ordernomain!=null && ordernomain!='')
		ordernomain=GetSOInternalId(ordernomain);
	else
	{

		nlapiLogExecution('ERROR','ebizContlp',ebizContlp);
		if((ebizContlp!=null)&&(ebizContlp!=''))
		{
			ordernomain=getSalesOrderInternalId(ebizContlp);
		}

	}
	nlapiLogExecution('ERROR','ordernomain',ordernomain);
	var ordersFound = false;
	var salesOrderList = getSalesOrderlist(ordernomain);

	if(salesOrderList != null && salesOrderList.length > 0 && ordernomain!=""&&ordernomain!=null)
	{
		var trantype = nlapiLookupField('transaction', ordernomain, 'recordType');
		var vOrderMainText=ordernomain;
		var location;
		if(trantype=="salesorder")
		{
			var rec= nlapiLoadRecord('salesorder', ordernomain);
			if(rec != null && rec != '')
			{
				vOrderMainText=rec.getFieldValue('tranid');
				location=rec.getFieldValue('location');
			}
		}
		else if(trantype=="salesorder")
		{
			var rec= nlapiLoadRecord('transferorder', ordernomain);
			if(rec != null && rec != '')
				vOrderMainText=rec.getFieldValue('tranid');
		}
		// Creating a sublist and adding fields to the sublist
		addFieldsToSublist(form);

		var button = form.addSubmitButton('Pack/Close Carton');
		form.addButton('custpage_autoassignlp','Auto Assign LP','AutoAssignLP()');
		form.addButton('custpage_generatesearch','New Order','backtogeneratesearch()');

		var cartongroup=form.addFieldGroup("cartoninfo", "Carton Details");

		var vOrdField=form.addField('custpage_ordhead', 'text', 'Order #',null,'cartoninfo').setDisplayType('inline').setDefaultValue(vOrderMainText);
		form.addField('custpage_contlp', 'text', 'Carton #',null,'cartoninfo').setMandatory(true);

		form.addField('custpage_weight', 'text', 'Carton Weight',null,'cartoninfo');

		var contsize=form.addField('custpage_cartonsize', 'select', 'Carton Size',null,'cartoninfo').setMandatory(true);

		cartongroup.setShowBorder(true);

		var filtersloc = new Array();
		if(location!=null && location!='')
			filtersloc.push(new nlobjSearchFilter('custrecord_ebizsitecontainer', null, 'is', location));

		var columnsloc = new Array();
		columnsloc.push(new nlobjSearchColumn('name'));

		var searchlocresults = nlapiSearchRecord('customrecord_ebiznet_container', null, filtersloc, columnsloc);

		if (searchlocresults != null && searchlocresults!='')
		{

			nlapiLogExecution('ERROR','test1',searchlocresults.length);
			for (var i = 0; i < searchlocresults.length; i++) 
			{
				var res = form.getField('custpage_cartonsize').getSelectOptions(searchlocresults[i].getValue('name'), 'is');
				if (res != null) 
				{
					if (res.length > 0)	                
						continue;	                
				}
				contsize.addSelectOption(searchlocresults[i].getId(), searchlocresults[i].getValue('name'));
			}
		}      
		if(request.getParameter('custpage_cartonsize')!='' && request.getParameter('custpage_cartonsize')!=null)
		{
			contsize.setDefaultValue(request.getParameter('custpage_cartonsize'));	
		}

		//vScanQtyFld.setLayoutType('startrow');
		var vAddBtn=form.addButton('custpage_addpack','Add Items','AddPackNew()');
		/*var vBulk=form.addField('custpage_packtype', 'radio', 'By Bulk Qty', 'bb');

	form.addField('custpage_packtype', 'radio','By Item','bi');
	vBulk.setDefaultValue('bi');*/
		var itemgroup=form.addFieldGroup("iteminfo", "Add Item");
		var vScanItemFld=form.addField('custpage_scanitem', 'text', 'Scan Item',null,'iteminfo');
		vScanItemFld.setLayoutType('normal', 'startrow');
		//vScanItemFld.onChange('onChange();')
		itemgroup.setShowBorder(true);
		var vScanQtyFld=form.addField('custpage_scanqty', 'text', 'Pack Qty',null,'iteminfo');
		form.addField('custpage_repeat', 'text', 'repeat').setDisplayType('hidden').setDefaultValue('0');
		form.addField('custpage_ebizso', 'text', 'soid').setDisplayType('hidden').setDefaultValue(ordernomain);
		form.addField('custpage_lpcreate', 'text', 'repeat').setDisplayType('hidden');
		//var vitemimage=form.addField('custpage_image', 'inlinehtml', 'Item Image');	
		//vitemimage.setDefaultValue('image');
		//vAddBtn.setLayoutType('normal', 'startrow');
		// Get containers to pack based on search criteria defined
		//var salesOrderList = getSalesOrderlist(ordernomain);

		//var ordersFound = false;

		nlapiLogExecution('ERROR','salesOrderList',salesOrderList);
		//if(salesOrderList != null && salesOrderList.length > 0){
		var orderno, containerno,ebizorderno,ebizcntrlno,siteid,compid,containersize,totweight,skuno,qty,skutext;	
		for (var i = 0; i < salesOrderList.length; i++){
			orderno = salesOrderList[i].getValue('name');
			var waveno=salesOrderList[i].getValue('custrecord_ebiz_wave_no');
			containerno = salesOrderList[i].getValue('custrecord_container_lp_no');
			ebizorderno = salesOrderList[i].getValue('custrecord_ebiz_order_no');
			ebizcntrlno = salesOrderList[i].getValue('custrecord_ebiz_cntrl_no');
			compid = salesOrderList[i].getValue('custrecord_comp_id');
			siteid = salesOrderList[i].getValue('custrecord_wms_location');
			containersize = salesOrderList[i].getValue('custrecord_container');
			totweight = salesOrderList[i].getValue('custrecord_total_weight');
			skuno = salesOrderList[i].getValue('custrecord_sku');
			skutext = salesOrderList[i].getText('custrecord_sku');
			qty = salesOrderList[i].getValue('custrecord_act_qty');
			var totCube = salesOrderList[i].getValue('custrecord_totalcube');
			var lineno = salesOrderList[i].getValue('custrecord_line_no');
			var parentsku = salesOrderList[i].getValue('custrecord_parent_sku_no');

			form.getSubList('custpage_items').setLineItemValue('custpage_order', i + 1, orderno);
			form.getSubList('custpage_items').setLineItemValue('custpage_wave', i + 1, waveno);
			form.getSubList('custpage_items').setLineItemValue('custpage_container', i + 1, containerno);
			form.getSubList('custpage_items').setLineItemValue('custpage_internalid', i + 1, salesOrderList[i].getId());
			form.getSubList('custpage_items').setLineItemValue('custpage_ebizordno', i + 1, ebizorderno);
			form.getSubList('custpage_items').setLineItemValue('custpage_ebizcntrlno', i + 1, ebizcntrlno);
			form.getSubList('custpage_items').setLineItemValue('custpage_compid', i + 1, compid);
			form.getSubList('custpage_items').setLineItemValue('custpage_siteid', i + 1, siteid);
			form.getSubList('custpage_items').setLineItemValue('custpage_containersize', i + 1, containersize);
			form.getSubList('custpage_items').setLineItemValue('custpage_totalweight', i + 1, totweight);
			form.getSubList('custpage_items').setLineItemValue('custpage_ebizskuno', i + 1, skuno);
			form.getSubList('custpage_items').setLineItemValue('custpage_ebizsku', i + 1, skutext);
			//form.getSubList('custpage_items').setLineItemValue('custpage_qty', i + 1, qty);
			form.getSubList('custpage_items').setLineItemValue('custpage_actqty', i + 1, qty);
			form.getSubList('custpage_items').setLineItemValue('custpage_pickqty', i + 1, qty);
			form.getSubList('custpage_items').setLineItemValue('custpage_totcube', i + 1, totCube);
			form.getSubList('custpage_items').setLineItemValue('custpage_ordlineno', i + 1, lineno);
			form.getSubList('custpage_items').setLineItemValue('custpage_parentsku', i + 1, parentsku);



			ordersFound = true;
		}
	}

	if(!ordersFound){
		nlapiLogExecution('ERROR', 'No containers to pack', '0');
		var form = nlapiCreateForm('Packing');
		showInlineMessage(form, 'Confirmation', 'No containers to pack', null);
	}
}
function getSalesOrderInternalId(ebizContlp)
{
	ebizContlp=ebizContlp.trim();
	var ebizfilters = new Array();
	nlapiLogExecution('ERROR','eBizContainerlp',ebizContlp);
	ebizfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK
	if((ebizContlp!=null)&&(ebizContlp!=''))
	{
		ebizfilters.push( new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', ebizContlp));
	}
	var ebizcolumns=new Array();
	ebizcolumns[0]=new nlobjSearchColumn('custrecord_ebiz_order_no');
	var ebizopentasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, ebizfilters, ebizcolumns);	
	var ebizOrderId="";
	if(ebizopentasksearchresults!=null)
	{
		ebizOrderId=ebizopentasksearchresults[0].getValue('custrecord_ebiz_order_no');	
	}
	nlapiLogExecution('ERROR','return OrderInternalId',ebizOrderId);
	return ebizOrderId;
}
function addFieldsToSublist(form){

	//var sublist = form.addSubList("custpage_items", "inlineeditor",	"Packing");
	var sublist = form.addSubList("custpage_items", "list", "Packing");

	//sublist.addMarkAllButtons();
	//sublist.addField("custpage_select", "checkbox", "Assign").setDisplayType('hidden');
	sublist.addField("custpage_order", "text", "Order #").setDisplayType('inline');
	sublist.addField("custpage_wave", "text", "Wave #").setDisplayType('inline');
	sublist.addField("custpage_container", "text", "Container #").setDisplayType('inline');	
	sublist.addField("custpage_ebizsku", "text", "Item").setDisplayType('inline');
	sublist.addField("custpage_pickqty", "text", "Pick Qty").setDisplayType('inline');
	sublist.addField("custpage_qty", "text", "Pack Qty").setDisplayType('entry').setDefaultValue(0);
	sublist.addField("custpage_actqty", "text", "ActQty").setDisplayType('hidden');
	sublist.addField("custpage_internalid", "text", "Internal Id").setDisplayType('hidden');
	sublist.addField("custpage_ebizordno", "text", "FF Ebiz ORD #").setDisplayType('hidden');
	sublist.addField("custpage_ebizcntrlno", "text", "FF Ebiz Cntrl #").setDisplayType('hidden');
	sublist.addField("custpage_compid", "text", "Comp Id").setDisplayType('hidden');
	sublist.addField("custpage_siteid", "text", "Site Id").setDisplayType('hidden');
	sublist.addField("custpage_containersize", "text", "Container Size").setDisplayType('hidden');
	sublist.addField("custpage_totalweight", "text", "Total Weight").setDisplayType('hidden');
	sublist.addField("custpage_ebizskuno", "text", "SKU").setDisplayType('hidden');
	sublist.addField("custpage_totcube", "text", "cube").setDisplayType('hidden');
	sublist.addField("custpage_ordlineno", "text", "LineNo").setDisplayType('hidden');
	sublist.addField("custpage_parentsku", "text", "Parent SKU").setDisplayType('hidden');

}

/**
 * 
 * @param orderno
 */
function getSalesOrderlist(orderno){
	var filters = new Array();
	nlapiLogExecution('ERROR','orderno in getSalesOrderlist',orderno);

	if(orderno!=""){
		filters.push( new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', orderno));
	}
	var vRoleLocation=getRoledBasedLocation();
	nlapiLogExecution('ERROR','vRoleLocation',vRoleLocation);
	
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vRoleLocation));
	}

	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//Status Flag - C (Picks Confirmed)
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_line_no');
	columns[2] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[5] = new nlobjSearchColumn('custrecord_comp_id');
	columns[6] = new nlobjSearchColumn('custrecord_site_id');
	columns[7] = new nlobjSearchColumn('custrecord_container');
	columns[8] = new nlobjSearchColumn('custrecord_total_weight');
	columns[9] = new nlobjSearchColumn('custrecord_sku');
	columns[10] = new nlobjSearchColumn('custrecord_act_qty');
	columns[11] = new nlobjSearchColumn('custrecord_wms_location');
	columns[12] = new nlobjSearchColumn('custrecord_totalcube');
	columns[13] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
	columns[14] = new nlobjSearchColumn('custrecord_parent_sku_no');

	columns[0].setSort();	
	columns[1].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	
	return searchresults;
}

function processSelectedContainers(request, response, form){

	var lineCount = request.getLineItemCount('custpage_items');
	nlapiLogExecution('ERROR','linecount', lineCount);
	var isRowSelected="";
	var Container="";
	var internalId="";
	var ffordno="";
	var stageLoc="";
	var ebizordno="";
	var ebizcntrlno="";
	var siteid="";
	var compid="";
	var containersize="";
	var oldcontainer="";	
	var oldFFOrdNo="";
	var itemno='';
	var NewContLP = request.getParameter('custpage_contlp');
	var NewSizeId = request.getParameter('custpage_cartonsize');
	var NewWeight = request.getParameter('custpage_weight');
	var SalesOrdNo = request.getParameter('custpage_ordhead');
	var eBizSO = request.getParameter('custpage_ebizso');
	var vsiteid = request.getParameter('custpage_siteid');

	if(NewContLP == null || NewContLP == '')
		newContainerLpNo=GetMaxLPNo('1', '2',vsiteid);
	else
	{
		/*var LPReturnValue = ebiznet_LPRange_CL_withLPType(NewContLP.replace(/\s+$/,""), '2','2',null);//'2'UserDefiend,'2'PICK
		if(LPReturnValue==true)*/
		CreateLP(NewContLP,NewSizeId,NewWeight);
		newContainerLpNo=NewContLP;

	}
	var FirstTaskIndex=0;
	var currentUserID = getCurrentUser();
	var vCartTotWeight=0;
	var vCartTotCube=0;	
	var vLastIndex=1;
	for(var s = 1; s <= lineCount; s++){
		//isRowSelected= request.getLineItemValue('custpage_items', 'custpage_select', k);
		var vNewPackQtyTemp= request.getLineItemValue('custpage_items', 'custpage_qty', s);
		nlapiLogExecution('ERROR','vNewPackQty', vNewPackQtyTemp);
		var isRowSelectedTmp='F';
		if(vNewPackQtyTemp != null && vNewPackQtyTemp != '' && parseFloat(vNewPackQtyTemp) > 0)
		{	
			isRowSelectedTmp='T';
			vLastIndex=s;
		}
	}
	for(var k = 1; k <= lineCount; k++){
		//isRowSelected= request.getLineItemValue('custpage_items', 'custpage_select', k);
		var vNewPackQty= request.getLineItemValue('custpage_items', 'custpage_qty', k);

		isRowSelected='F';
		if(vNewPackQty != null && vNewPackQty != '' && parseFloat(vNewPackQty) > 0)
			isRowSelected='T';
		nlapiLogExecution('ERROR','vNewPackQty', vNewPackQty);
		nlapiLogExecution('ERROR','Record Selected?', isRowSelected);
		if(isRowSelected == "T"){
			FirstTaskIndex=1;
			Container = request.getLineItemValue('custpage_items', 'custpage_container', k);
			internalId= request.getLineItemValue('custpage_items', 'custpage_internalid', k);
			ffordno = request.getLineItemValue('custpage_items', 'custpage_order', k);
			//stageLoc = request.getLineItemValue('custpage_items', 'custpage_packlocation', k);
			ebizordno = request.getLineItemValue('custpage_items', 'custpage_ebizordno', k);
			ebizcntrlno = request.getLineItemValue('custpage_items', 'custpage_ebizcntrlno', k);					
			compid = request.getLineItemValue('custpage_items', 'custpage_compid', k);
			siteid = request.getLineItemValue('custpage_items', 'custpage_siteid', k);
			containersize = request.getLineItemValue('custpage_items', 'custpage_containersize', k);
			itemno = request.getLineItemValue('custpage_items', 'custpage_ebizskuno', k);
			var qty = request.getLineItemValue('custpage_items', 'custpage_qty', k);
			var actqty = request.getLineItemValue('custpage_items', 'custpage_actqty', k);
			var taskCube = request.getLineItemValue('custpage_items', 'custpage_totcube', k);
			var taskWeight = request.getLineItemValue('custpage_items', 'custpage_totalweight', k);


			nlapiLogExecution('ERROR','entered qty', qty);
			nlapiLogExecution('ERROR','actqty', actqty);
			nlapiLogExecution('ERROR','internalId', internalId);
			var vDeviceUpdFlag='F';
			if(k==vLastIndex)
				vDeviceUpdFlag='T';
			nlapiLogExecution('ERROR','vDeviceUpdFlag', vDeviceUpdFlag);
			if(qty!=null && qty !='' && parseFloat(qty) == parseFloat(actqty))
			{
				if(taskWeight != null && taskWeight != '')
					vCartTotWeight= parseFloat(vCartTotWeight)+parseFloat(taskWeight);
				if(taskCube != null && taskCube != '')
					vCartTotCube= parseFloat(vCartTotCube)+parseFloat(taskCube);

				updateStatusInOpenTaskNew(internalId,28,null,newContainerLpNo,NewSizeId,qty,currentUserID,null,vDeviceUpdFlag);//28 - PACK Complete
			}	
			else if(qty!=null && qty !='' && parseFloat(qty) != parseFloat(actqty))
			{
				var vEachCube=0;
				var vEachWeight=0;
				if(taskCube != null && taskCube != '')
					vEachCube=parseFloat(taskCube) / parseFloat(actqty);
				if(taskWeight != null && taskWeight != '')
					vEachWeight=parseFloat(taskWeight) / parseFloat(actqty);



				nlapiLogExecution('ERROR','Into Diff');
				var vDiffQty=parseFloat(actqty) - parseFloat(qty);

				nlapiLogExecution('ERROR','Creating open task for remaining');
				//var createopentaskrec =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',internalId);

				var createopentaskrec =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',internalId);		    	
				createopentaskrec.setFieldValue('custrecord_wms_status_flag', '8');//Statusflag='C'
				nlapiLogExecution('ERROR','OleContLP',createopentaskrec.getFieldValue('custrecord_container_lp_no'));
				//createopentaskrec.setFieldValue('custrecord_container_lp_no', newContainerLpNo);
				createopentaskrec.setFieldValue('custrecord_container', NewSizeId);	
				createopentaskrec.setFieldValue('custrecord_act_qty', parseFloat(vDiffQty).toFixed(5));
				createopentaskrec.setFieldValue('custrecord_expe_qty', parseFloat(vDiffQty).toFixed(5));
				createopentaskrec.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
				createopentaskrec.setFieldValue('custrecord_ebizuser', currentUserID);
				createopentaskrec.setFieldValue('custrecord_totalcube',(parseFloat(vEachCube)* parseFloat(vDiffQty)).toFixed(5));		
				createopentaskrec.setFieldValue('custrecord_total_weight',(parseFloat(vEachWeight)* parseFloat(vDiffQty)).toFixed(5));
				nlapiLogExecution('ERROR','ffordno',ffordno);
				createopentaskrec.setFieldValue('name',ffordno);

				//createopentaskrec.setFieldValue('custrecord_total_weight',parseFloat(varContainerTareWeight)+parseFloat(weightinfo2));	
				var vUpdWeight=parseFloat(vEachWeight)* parseFloat(qty);
				var vUpdCube=parseFloat(vEachCube)* parseFloat(qty);
				if(taskWeight != null && taskWeight != '')
					vCartTotWeight= parseFloat(vCartTotWeight)+parseFloat(vUpdWeight);
				if(taskCube != null && taskCube != '')
					vCartTotCube= parseFloat(vCartTotCube)+parseFloat(vUpdCube);

				nlapiSubmitRecord(createopentaskrec, false, true);
				updateStatusInOpenTaskNew(internalId,28,vUpdWeight,newContainerLpNo,NewSizeId,qty,currentUserID,vUpdCube,vDeviceUpdFlag);//28 - PACK Complete
				//updateStatusInOpenTaskNew(internalId,28,NewWeight,newContainerLpNo,NewSizeId,actqty);//28 - PACK Complete

			}
			//CreatePACKTask(compid,siteid,ebizordno,ffordno,newContainerLpNo,14,stageLoc,stageLoc,28,ebizcntrlno,itemno);//14 - Task Type - PACK

		}

	}
	if(FirstTaskIndex != null && FirstTaskIndex != '' && FirstTaskIndex != 0)
	{	
		if(NewWeight == null || NewWeight == '')
		{
			var tareWeightCube=getContainerCubeAndTarWeight(NewSizeId,null);
			if( tareWeightCube != null && tareWeightCube != '' && tareWeightCube.length>2)
			{
				if(tareWeightCube[1] != null && tareWeightCube[1] != '')
				{
					NewWeight= parseFloat(vCartTotWeight)+parseFloat(tareWeightCube[1]);

				}
				if(tareWeightCube[0] != null && tareWeightCube[0] != '')
				{
					vCartTotCube= parseFloat(vCartTotCube)+parseFloat(tareWeightCube[0]);
				}
			}

		}	

		nlapiLogExecution('ERROR', 'Creating Pack Task');
		var vFirstTaskIntId=request.getLineItemValue('custpage_items', 'custpage_internalid', FirstTaskIndex);
		var AssignPackStationrec =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',vFirstTaskIntId);
		AssignPackStationrec.setFieldValue('custrecord_tasktype', 14); //14 stands for pack
		AssignPackStationrec.setFieldValue('custrecord_act_qty', 0); //
		AssignPackStationrec.setFieldValue('custrecord_container_lp_no', newContainerLpNo);
		AssignPackStationrec.setFieldValue('custrecord_sku', null);
		AssignPackStationrec.setFieldValue('custrecord_wms_status_flag', 28); //28 stands for pack complete
		AssignPackStationrec.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		AssignPackStationrec.setFieldValue('custrecord_ebizuser', currentUserID);

		//AssignPackStationrec.setFieldValue('custrecord_pack_confirmed_date', CurrentDate);
		nlapiSubmitRecord(AssignPackStationrec, false, true);
		updateStatusInLPMasterNew(newContainerLpNo,NewWeight,vCartTotCube,28);

		CreateShipManifestRecord(eBizSO,newContainerLpNo,NewSizeId,request.getLineItemValue('custpage_items', 'custpage_compid', FirstTaskIndex),request.getLineItemValue('custpage_items', 'custpage_siteid', FirstTaskIndex),NewWeight);
	}
	showInlineMessage(form, 'Confirmation', 'Pallet LP  ' + newContainerLpNo + '  Packed successfully', "");
	form.addButton('custpage_generatesearch','New Order','backtogeneratesearch()');

	var searchfilter=new Array();
	searchfilter.push(new nlobjSearchFilter('custrecord_container_lp_no',null,'is',newContainerLpNo));
	searchfilter.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',null,'isnotempty'));

	var searchcolumn=new Array();
	searchcolumn[0]=new nlobjSearchColumn('custrecord_ebiz_nsconfirm_ref_no');

	var searchrec=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,searchfilter,searchcolumn);

	var ItemFulfillmentRecId="";
	if(searchrec!=null&&searchrec!="")
		ItemFulfillmentRecId=searchrec[0].getValue('custrecord_ebiz_nsconfirm_ref_no');
	nlapiLogExecution('ERROR','ItemFulfillmentRecId',ItemFulfillmentRecId);
	var POarray = new Array();
	if(ItemFulfillmentRecId==null||ItemFulfillmentRecId=="")
		nlapiSetRedirectURL('RECORD', 'ItemFulfillment', null, false, POarray);
	else
		nlapiSetRedirectURL('RECORD', 'ItemFulfillment', ItemFulfillmentRecId, false, POarray);
}

function CreateLP(varEnteredLP,containerInternalId,varEnteredweight)
{

	//nlapiLogExecution('ERROR', 'getItem', ItemInternalId);

	var contRec = nlapiLoadRecord('customrecord_ebiznet_container', containerInternalId);
	var vTareWeight=0;
	var ContainerCube;
	if(contRec != null && contRec != '')
	{
		vTareWeight=contRec.getFieldValue('custrecord_cubecontainer');
		ContainerCube=contRec.getFieldValue('custrecord_cubecontainer');
	}


	nlapiLogExecution('ERROR', 'ContainerSizeInternalId', containerInternalId);				
	//Insert LP Record
	CreateMasterLPRecord(varEnteredLP,containerInternalId,ContainerCube,varEnteredweight);

}

function CreateMasterLPRecord(vContLpNo,containerInternalId,ContainerCube,EnteredWeight)
{

	var MastLP=nlapiCreateRecord('customrecord_ebiznet_master_lp');
	MastLP.setFieldValue('name', vContLpNo);
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_lp', vContLpNo);
	if(containerInternalId!="" && containerInternalId!=null)
	{
		MastLP.setFieldValue('custrecord_ebiz_lpmaster_sizeid', containerInternalId);
	}	

	if(EnteredWeight!="" && EnteredWeight!=null)
		MastLP.setFieldValue('custrecord_ebiz_lpmaster_totwght', parseFloat(EnteredWeight).toFixed(5));
	if(ContainerCube!="" && ContainerCube!=null)
		MastLP.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(ContainerCube).toFixed(5));

	var retktoval = nlapiSubmitRecord(MastLP);
	nlapiLogExecution('ERROR', 'Master LP Insertion','Success');

}

/**
 * This method will update the status to pack complete in open task for all containers
 * @param recordId
 * @param statusflag
 */
function updateStatusInOpenTaskNew(recordId, statusflag,TotWeight,ContLP,SizeId,qty,currentUserID,TotCube,vDeviceUpdFlag){
	nlapiLogExecution('ERROR', 'In to updateStatusInOpenTask function');

	var fieldNames = new Array(); 
	fieldNames.push('custrecord_wms_status_flag');  
	fieldNames.push('custrecord_pack_confirmed_date');
	if(TotWeight != null && TotWeight != '')
		fieldNames.push('custrecord_total_weight');
	fieldNames.push('custrecord_container_lp_no');
	fieldNames.push('custrecord_act_qty');
	fieldNames.push('custrecord_expe_qty');	
	fieldNames.push('custrecord_upd_ebiz_user_no');
	fieldNames.push('custrecord_device_upload_flag');
	if(TotCube != null && TotCube != '')
		fieldNames.push('custrecord_totalcube');
	if(SizeId != null && SizeId !='')
		fieldNames.push('custrecord_container');



	var newValues = new Array(); 
	newValues.push(statusflag);
	newValues.push(DateStamp());
	if(TotWeight != null && TotWeight != '')
		newValues.push(parseFloat(TotWeight).toFixed(5));
	newValues.push(ContLP);
	newValues.push(parseFloat(qty).toFixed(5));
	newValues.push(parseFloat(qty).toFixed(5));
	newValues.push(parseFloat(currentUserID));
	newValues.push(vDeviceUpdFlag);
	if(TotCube != null && TotCube != '')
		newValues.push(parseFloat(TotCube).toFixed(5));
	if(SizeId != null && SizeId !='')
		newValues.push(SizeId);

	nlapiSubmitField('customrecord_ebiznet_trn_opentask', recordId, fieldNames, newValues);

//	var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recordId);
//	transaction.setFieldValue('custrecord_wms_status_flag', statusflag);
//	transaction.setFieldValue('custrecord_pack_confirmed_date', DateStamp());
//	nlapiSubmitRecord(transaction, false, true);
//	nlapiLogExecution('ERROR', 'Pack Complete - Status Flag Updated in open task',recordId);

	nlapiLogExecution('ERROR', 'Out of updateStatusInOpenTask function');
}

function getParentOrder(orderno){
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', orderno));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ns_ord');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
	return searchresults;
}

function getCarrierType(ebizOrd)
{
	var vOrdAutoPackFlag="";
	var vStgAutoPackFlag="";
	var vStgCarrierType="";
	var vCarrierType="";
	var vStgCarrierName="";
	var vCarrier="";
	var vordtype='';
	var whlocation='';
	var vOrderAutoPackFlag = getAutoPackFlagforOrdType(ebizOrd);
	if(vOrderAutoPackFlag!=null && vOrderAutoPackFlag.length>0){
		vOrdAutoPackFlag = vOrderAutoPackFlag[0].getValue('custrecord_autopackflag','custbody_nswmssoordertype');
		vCarrierType = vOrderAutoPackFlag[0].getText('custrecord_carrier_type','custbody_salesorder_carrier');
		vCarrier = vOrderAutoPackFlag[0].getText('custbody_salesorder_carrier');
		vordtype = vOrderAutoPackFlag[0].getValue('custbody_nswmssoordertype');
		whlocation = vOrderAutoPackFlag[0].getValue('location');
		nlapiLogExecution('ERROR', 'Order Type',vordtype);
		nlapiLogExecution('ERROR', 'WH Location',whlocation);
		nlapiLogExecution('ERROR', 'AutoPackFlag @ Order Type',vOrdAutoPackFlag);
		nlapiLogExecution('ERROR', 'Carrier Type @ Order Type',vCarrierType);
		nlapiLogExecution('ERROR', 'Carrier @ Order Type',vCarrier);
	}

	if(vOrdAutoPackFlag!='T' || (vCarrier=="" ||vCarrier==null)){
		var vStageAutoPackFlag = getAutoPackFlagforStage(whlocation,vordtype);
		if(vStageAutoPackFlag!=null && vStageAutoPackFlag.length>0){
			vStgCarrierType = vStageAutoPackFlag[0][0];
			vStgAutoPackFlag = vStageAutoPackFlag[0][1];  
			vStgCarrierName = vStageAutoPackFlag[0][2];
			nlapiLogExecution('ERROR', 'AutoPackFlag(Stage)',vStgAutoPackFlag);
			nlapiLogExecution('ERROR', 'Carrier Type(Stage)',vStgCarrierType);
			nlapiLogExecution('ERROR', 'Carrier(Stage)',vStgCarrierName);
		}
	}

	if((vCarrier=="" || vCarrier==null) && (vStgCarrierName!="" && vStgCarrierName!=null))
		vCarrier=vStgCarrierName;

	if((vCarrierType=="" || vCarrierType==null) && (vStgCarrierType!="" && vStgCarrierType!=null))
		vCarrierType=vStgCarrierType;

	nlapiLogExecution('ERROR', 'vCarrierType', vCarrierType);
	return vCarrierType;
}
function getAutoPackFlagforOrdType(vebizOrdNo){
	nlapiLogExecution('ERROR', 'into getAutoPackFlagforOrdType eBizOrdNo',vebizOrdNo);
	var searchresults = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter('internalid', null, 'is', vebizOrdNo));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_carrier_type','custbody_salesorder_carrier');
	columns[1] = new nlobjSearchColumn('custrecord_autopackflag','custbody_nswmssoordertype');
	searchresults = nlapiSearchRecord('salesorder', null, filters, columns);
	nlapiLogExecution('ERROR', 'out of getAutoPackFlagforOrdType', searchresults);
	return searchresults;

}
function getAutoPackFlagforStage(){
	nlapiLogExecution('ERROR', 'into getAutoPackFlagforStage');
	var vStgRule = new Array();
	vStgRule = getStageRule('', '', '', '', 'OUB');
	nlapiLogExecution('ERROR', 'Stage Rule', vStgRule);

	nlapiLogExecution('ERROR', 'out of getAutoPackFlagforStage');
	return vStgRule;
}
function updateStatusInLPMasterNew(ContainerLPNo,Totweightupdate,expectedCube,status)
{
	nlapiLogExecution('ERROR', 'Updating in LP Master');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totcube');
	var filtersmlp = new Array();
	filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', ContainerLPNo);
	var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp,columns);
	if (SrchRecord != null && SrchRecord.length > 0) {
		nlapiLogExecution('ERROR', 'LP FOUND');
		var lpRecid= SrchRecord[0].getId();
		var fields=new Array();
		fields[0]='custrecord_ebiz_lpmaster_totwght';
		fields[1]='custrecord_ebiz_lpmaster_totcube';
		fields[2]='custrecord_ebiz_lpmaster_wmsstatusflag';
		var lpweight=SrchRecord[0].getValue('custrecord_ebiz_lpmaster_totwght');
		var lpcube=SrchRecord[0].getValue('custrecord_ebiz_lpmaster_totcube');
		var Values=new Array();
		Values[0]=(parseFloat(lpweight)+parseFloat(Totweightupdate)).toFixed(5);
		Values[1]=(parseFloat(lpcube)+parseFloat(expectedCube)).toFixed(5);
		Values[2]=status;

		var UpdatedRecID=nlapiSubmitField('customrecord_ebiznet_master_lp',lpRecid,fields,Values);

	}	
}