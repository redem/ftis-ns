/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_LocationCompany.js,v $
 *     	   $Revision: 1.2.4.3.4.3.4.18.2.3 $
 *     	   $Date: 2015/11/25 15:33:11 $
 *     	   $Author: aanchal $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_184 $
 *This Suitelet is meant to scan or enter the licence plate # for which the item is created.
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 * REVISION HISTORY
 * $Log: ebiz_RF_LocationCompany.js,v $
 * Revision 1.2.4.3.4.3.4.18.2.3  2015/11/25 15:33:11  aanchal
 * 2015.2 Issue fix
 * 201415753
 *
 * Revision 1.2.4.3.4.3.4.18.2.2  2015/10/27 04:54:49  schepuri
 * case# 201415215
 *
 * Revision 1.2.4.3.4.3.4.18.2.1  2015/10/21 09:19:07  schepuri
 * case# 201415118
 *
 * Revision 1.2.4.3.4.3.4.18  2015/05/05 15:18:54  grao
 * SB issue fixes  201412638
 *
 * Revision 1.2.4.3.4.3.4.17  2015/04/17 15:27:08  grao
 * SB issue fixes  201412314
 *
 * Revision 1.2.4.3.4.3.4.16  2015/04/06 13:18:51  schepuri
 * case# 201412254
 *
 * Revision 1.2.4.3.4.3.4.15  2014/08/26 15:29:37  skavuri
 * Case# 201410154 Std Bundle Issue Fixed
 *
 * Revision 1.2.4.3.4.3.4.14  2014/08/13 15:53:38  skavuri
 * Case# 20149947 StdBundle Issue Fixed
 *
 * Revision 1.2.4.3.4.3.4.13  2014/08/04 15:53:07  skavuri
 * Case# 20149821 SB Issue Fixed
 *
 * Revision 1.2.4.3.4.3.4.12  2014/07/11 15:51:54  sponnaganti
 * Case# 20149398
 * Compatibility Issue fix
 *
 * Revision 1.2.4.3.4.3.4.11  2014/06/13 09:45:08  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.2.4.3.4.3.4.10  2014/05/30 00:34:22  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.2.4.3.4.3.4.9  2014/05/28 15:38:11  skreddy
 * case # 20148569
 * Sonic SB issue fix
 *
 * Revision 1.2.4.3.4.3.4.8  2014/05/21 15:24:28  sponnaganti
 * case# 20148462
 * Stnd Bundle Issue fix
 *
 * Revision 1.2.4.3.4.3.4.7  2014/05/19 06:28:25  skreddy
 * case # 20148195
 * Sonic SB issue fix
 *
 * Revision 1.2.4.3.4.3.4.6  2014/02/24 14:50:29  rmukkera
 * Case # 20127334
 *
 * Revision 1.2.4.3.4.3.4.5  2013/10/25 20:08:04  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 20125294
 *
 * Revision 1.2.4.3.4.3.4.4  2013/10/22 14:12:39  schepuri
 * 20125084
 *
 * Revision 1.2.4.3.4.3.4.3  2013/05/01 15:22:27  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fixes
 *
 * Revision 1.2.4.3.4.3.4.2  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.2.4.3.4.3.4.1  2013/03/26 13:27:42  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.2.4.3.4.3  2012/11/09 14:25:43  schepuri
 * CASE201112/CR201113/LOG201121
 * 20120845
 *
 * Revision 1.2.4.3.4.2  2012/09/26 12:36:25  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.2.4.3.4.1  2012/09/24 10:08:56  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.2.4.3  2012/05/08 23:24:41  gkalla
 * CASE201112/CR201113/LOG201121
 * To populate valid company
 *
 * Revision 1.2.4.2  2012/03/16 14:08:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.2.4.1  2012/02/21 13:24:50  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.2  2011/09/16 07:32:24  schepuri
 * CASE201112/CR201113/LOG201121
 * duplicate lps
 *
 * Revision 1.2  2011/09/16 07:22:56  schepuri
 * CASE201112/CR201113/LOG201121
 * duplicate lps
 *
 * 
 */

function LocationCompany(request, response)
{	
	//Case# 20149947 starts
	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);
	//Case# 20149947 ends
	//Case 20125294 starts

	var locationintrid=getRoledBasedLocation();

	nlapiLogExecution('ERROR', 'Role Based Location', locationintrid);

	var LocationName;
	var Locationinternalid;
	var LocationFilters = new Array();	
	LocationFilters.push(new nlobjSearchFilter('custrecord_ebizwhsite',null, 'is','T'));
	//Case # 20127334  Start
	if(locationintrid!=null && locationintrid!='' && locationintrid!= 0)
	{
		LocationFilters.push(new nlobjSearchFilter('internalid',null, 'is',locationintrid));
	}
	//Case # 20127334  End

	var LocationColumn = new Array();
	LocationColumn.push(new nlobjSearchColumn('name'));	
	var LocationResults = nlapiSearchRecord('location', null, LocationFilters, LocationColumn);		



	if (LocationResults != null) 
	{	
		//Case # 20127334  Start
		nlapiLogExecution('ERROR', 'LocationResults', LocationResults.length);
		//Case # 20127334  End
		LocationName= LocationResults[0].getValue('name');
		Locationinternalid = LocationResults[0].getId();
	}

	nlapiLogExecution('ERROR', 'LocationName::', LocationName);	
	nlapiLogExecution('ERROR', 'Locationinternalid', Locationinternalid);

	//Case 20125294 ends

	var CompName;	
	var CompanyFilters = new Array();
	//CompanyFilters.push(new nlobjSearchFilter('internalid',null,'is',1));
	CompanyFilters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
	var CompanyColumn = new Array();	
	CompanyColumn.push(new nlobjSearchColumn('custrecord_company'));		
	var CompanyResults = nlapiSearchRecord('customrecord_ebiznet_company', null, CompanyFilters,CompanyColumn);	    
	if(CompanyResults!=null)
	{
		CompName=CompanyResults[0].getValue('custrecord_company');
	}
	nlapiLogExecution('ERROR', 'CompName::', CompName);	

	if (request.getMethod() == 'GET') 
	{				
		var getLanguage = request.getParameter('custparam_language');
		var st1,st2,st3,st4,st5;

		if( getLanguage == 'es_ES')
		{		
			st1 = "UBICACI&#211;N";
			st2 = "EMPRESA";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";
			st5 = "CREAR INVENTARIO";

		}
		else
		{
			st1 = "LOCATION : ";
			st2 = "COMPANY : ";	
			st3 = "SEND";	
			st4 = "PREV";	
			st5 = "CREATE INVENTORY";// case# 201415215



		}
		var functionkeyHtml=getFunctionkeyScript('_rf_sitecomp'); 
		var html = "<html><head><title>" + st5 + "</title>"; // CASE# 201415118
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";   
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterlocation').focus();";           
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_sitecomp' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + " <label>" + LocationName + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlocation' id='enterlocation' type='text' />";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + " <label>" + CompName + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entercompany' type='text'  />";
		html = html + "				</td>";
		html = html + "				<input type='hidden' name='hdnLocationName' value=" + LocationName+ ">";
		html = html + "				<input type='hidden' name='hdnLocationInternalid' value=" + Locationinternalid + ">";
		html = html + "				<input type='hidden' name='hdnCompname' value=" + CompName + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "			</tr>";       
		html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st4 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlocation').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else
	{
		var CIarray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		CIarray["custparam_language"] = getLanguage;

		nlapiLogExecution('ERROR', 'getLanguage', CIarray["custparam_language"]);


		var st6,st7;
		if( getLanguage == 'es_ES')
		{
			st6 = "UBICACI&#211;N NO V&#193;LIDA ";
			st7= "Empresa V�LIDA";

		}
		else
		{
			st6 = "INVALID LOCATION";
			st7 = "INVALID COMPANY";
		}
		var gethidLocationName =  request.getParameter('hdnLocationName'); 
		var gethidLocInternalid=request.getParameter('hdnLocationInternalid');
		var gethidCompanyName = request.getParameter('hdnCompname');

		var getEnterLocation = request.getParameter('enterlocation');
		var getEnterCompany = request.getParameter('entercompany');

		var Locintid = gethidLocInternalid;
		CIarray["custparam_screenno"] = '15LOC';
		CIarray["custparam_locationId"] = gethidLocInternalid;

		//if Both are not equal then fetch internal id of new entered location
		var optedEvent = request.getParameter('cmdPrevious');
		if (sessionobj!=context.getUser()) {
			try{
				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}
		if (optedEvent == 'F7') {
			response.sendRedirect('SUITELET', 'customscript_rf_inventorymenu', 'customdeploy_rf_inventorymenu_di', false, CIarray);
		}
		else 
		{
			
			if((getEnterLocation==null||getEnterLocation=="")&&(getEnterCompany==null||getEnterCompany==""))
			{
				CIarray["custparam_error"] = "PLEASE ENTER VALUE";
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
			}
			else if((gethidLocationName!=getEnterLocation)&&(getEnterLocation!=""))
			{
				var newLocationFilters = new Array();	
				newLocationFilters.push(new nlobjSearchFilter('name',null, 'is',getEnterLocation));	
				//case no 20125084
				newLocationFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
				var RoleLocation=getRoledBasedLocationNew();
				var newLocationColumn = new Array();	    	
				newLocationColumn.push(new nlobjSearchColumn('internalid'));		
				var newLocationResults = nlapiSearchRecord('location', null, newLocationFilters, newLocationColumn);			    	

				if (newLocationResults != null && newLocationResults != '') 
				{	       		
					nlapiLogExecution('ERROR', 'newLocationResults', newLocationResults.length);
					Locintid = newLocationResults[0].getId();
					nlapiLogExecution('ERROR', 'Locintid', Locintid);
					//case# 20149398 starts
					//if((RoleLocation !=null && RoleLocation !='' && RoleLocation !=0) &&(RoleLocation.indexOf(Locintid)!=-1))
					if((RoleLocation !=null && RoleLocation !='' && RoleLocation !=0))
					{
						nlapiLogExecution('ERROR', 'RoleLocation3', RoleLocation);
						nlapiLogExecution('ERROR', 'getEnterCompany', getEnterCompany);
						if((RoleLocation.indexOf(parseInt(Locintid))!=-1))// case# 201412254
						{
							//Case# 20149821 starts
							if(getEnterCompany !='' && getEnterCompany !=null && getEnterCompany !='null')
							{
								var compid='';
								var CompanyFilters = new Array();	
								CompanyFilters.push(new nlobjSearchFilter('name',null, 'is',getEnterCompany));	
								//case no 20125084
								CompanyFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
								var CompanyColumn = new Array();	    	
								CompanyColumn.push(new nlobjSearchColumn('internalid'));		
								var CompanyResults = nlapiSearchRecord('customrecord_ebiznet_company', null, CompanyFilters, CompanyColumn);	
								if(CompanyResults !='' && CompanyResults!=null && CompanyResults!='null')
								{
									compid = CompanyResults[0].getId();
								}
								if(compid !='' && compid!=null && compid !='null')
								{
									var newLocationFilters = new Array();	
									newLocationFilters.push(new nlobjSearchFilter('name',null, 'is',getEnterLocation));	
									newLocationFilters.push(new nlobjSearchFilter('custrecordcompany',null, 'is',compid));
									newLocationFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
									var newLocationColumn = new Array();	    	
									newLocationColumn.push(new nlobjSearchColumn('internalid'));		
									var newLocationResults = nlapiSearchRecord('location', null, newLocationFilters, newLocationColumn);			    	
									if (newLocationResults == null || newLocationResults == '' || newLocationResults=='null') 
									{	      
										CIarray["custparam_locationId"] = Locintid;
										CIarray["custparam_error"] = 'Company not match with Location';
										nlapiLogExecution('ERROR', 'Company not match with Location');
										response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
										return; // Case# 201410154
									}
								}
								else
									{
									CIarray["custparam_locationId"] = Locintid;
									CIarray["custparam_error"] = st7;
									nlapiLogExecution('ERROR', ' Invalid Company ');
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
									return; // Case# 201410154
									}
							}
							//Case# 20149821 ends
							CIarray["custparam_locationId"] = Locintid;
							response.sendRedirect('SUITELET', 'customscript_rf_create_invt_binloc', 'customdeploy_rf_create_invt_binloc_di', false, CIarray);
						}
						else
						{
							CIarray["custparam_error"] = st6;
							nlapiLogExecution('ERROR', 'Location Does not Exists');
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
						}

					}
					else if(Locintid !=null && Locintid !='')
					{
						
						CIarray["custparam_locationId"] = Locintid;
						response.sendRedirect('SUITELET', 'customscript_rf_create_invt_binloc', 'customdeploy_rf_create_invt_binloc_di', false, CIarray);
						
					}
					else
					{
						CIarray["custparam_error"] = st6;
						nlapiLogExecution('ERROR', 'Location Does not Exists');
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);

					}

				}        		
				else
				{	
					CIarray["custparam_error"] = st6;
					nlapiLogExecution('ERROR', 'Location Does not Exists');
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
				}

				nlapiLogExecution('ERROR', 'newLocationinteralid', Locintid);	    		    	
			}
			else
			{
				response.sendRedirect('SUITELET', 'customscript_rf_create_invt_binloc', 'customdeploy_rf_create_invt_binloc_di', false, CIarray);
			}


		}
			}
			catch (e)  {
				CIarray["custparam_error"] = st6;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');

			}
		}
	}

}


