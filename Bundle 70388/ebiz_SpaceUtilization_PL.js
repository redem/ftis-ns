/***************************************************************************
  eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Portlet/Attic/ebiz_SpaceUtilization_PL.js,v $
 *     	   $Revision: 1.1.2.2.8.4 $
 *     	   $Date: 2013/09/30 15:59:51 $
 *     	   $Author: rmukkera $
 *     	   $Name: t_NSWMS_2014_1_1_174 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_SpaceUtilization_PL.js,v $
 * Revision 1.1.2.2.8.4  2013/09/30 15:59:51  rmukkera
 * Case#  20124376
 *
 * Revision 1.1.2.2.8.3  2013/09/20 15:22:53  rmukkera
 * Case# 20124546
 *
 * Revision 1.1.2.2.8.2  2013/08/19 15:01:26  gkalla
 * Case# 20123896
 * Issue fixed for Ryonet, added stage location type condition
 *
 * Revision 1.1.2.2.8.1  2013/03/05 14:56:05  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Lexjet production as part of Standard bundle
 *
 * Revision 1.1.2.2  2012/09/05 16:41:23  gkalla
 * CASE201112/CR201113/LOG201121
 * Portlet changes
 *
 * Revision 1.1.2.1  2012/09/04 16:45:25  gkalla
 * CASE201112/CR201113/LOG201121
 * Used cube vs empty cube portlet
 *
 * Revision 1.2.4.1  2012/04/02 12:47:17  schepuri
 * CASE201112/CR201113/LOG201121
 * fetching more than 1000 rec 
 *
 * 
 *****************************************************************************/

var socount=0;
var cocount=0;

function spaceUtilizePortlet(portlet, column)
{  

	portlet.setTitle('Sapce&nbsp;Utilization');  
	
	


		var content='';
		portlet.setHtml( content );


	
}
