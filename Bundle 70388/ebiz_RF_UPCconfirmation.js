/***************************************************************************
  		   eBizNET Solutions Ltd               
 ***************************************************************************
 **     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_UPCconfirmation.js,v $
 *     	   $Revision: 1.1.2.2 $
 *     	   $Date: 2014/05/30 00:34:23 $
 *     	   $Author: nneelam $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_UPCconfirmation.js,v $
 * Revision 1.1.2.2  2014/05/30 00:34:23  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.1  2014/01/07 15:46:21  rmukkera
 * Case # 20126443
 *
 * Revision 1.2.4.2  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.2.4.1  2012/02/22 12:39:53  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.4  2012/02/17 13:38:43  schepuri
 * CASE201112/CR201113/LOG201121
 * Added CVS Header
 *
 * Revision 1.3  2012/02/17 13:32:31  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 * 
 */
function UPCConfirmation(request,response){
	if(request.getMethod()=='GET'){

		var getPlanNo = request.getParameter('custparam_planno');
		var getsku = request.getParameter('custparam_sku');
		var gettasktype = request.getParameter('custparam_tasktype');
		var getlocation = request.getParameter('custparam_location');
		var getbeginlocationid = request.getParameter('custparam_actbeginlocationid');
		var getbeginlocationname = request.getParameter('custparam_actbeginlocationname');
		var getskuno = request.getParameter('custparam_skuno');
		var newupccode=request.getParameter('custparam_newupccode');
		
		var functionkeyHtml=getFunctionkeyScript('_rf_putaway_mergelp'); 
		
		var html="<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html+="<form name='_rf_putaway_mergelp' method='POST'>";
		html+="<b>ARE YOU SURE, WANT TO UPDATE UPC CODE?</b></br>";
		html+="ITEM : <label>" + getsku + "</label></br>";
		html = html + "		<table>";
		html = html + "			<tr>";			

		html = html + "				<input type='hidden' name='hdnplanno' value=" + getPlanNo + ">";		
		html = html + "				<input type='hidden' name='hdntasktype' value=" + gettasktype + ">";
		html = html + "				<input type='hidden' name='hdnlocation' value=" + getlocation + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + getbeginlocationid + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationname' value=" + getbeginlocationname + ">";	
		html = html + "				<input type='hidden' name='hdnItemId' value=" + getskuno + ">";	
		html = html + "				<input type='hidden' name='hdnItemname' value=" + getsku + ">";	
		html = html + "				<input type='hidden' name='hdnupccode' value=" + newupccode + ">";
		
		html = html + "				</td>";
		html = html + "			</tr>";		
		html = html + "		 </table>";
		html+="<input name='cmdyes' type='submit' value='YES'/>";
		html+="<input name='cmdno' type='submit' value='NO'/>";
		response.write(html);
	}
	else{
		
		var getplanno = request.getParameter('hdnplanno');		
		var getTasktype = request.getParameter('hdntasktype');
		var getlocation = request.getParameter('hdnlocation');
		var getBeginLocationid = request.getParameter('hdnBeginLocation');
		var getBeginLocationame = request.getParameter('hdnBeginLocationname');
		var ItemId=request.getParameter('hdnItemId');
		var ItemName=request.getParameter('hdnItemname');
		var upccode=request.getParameter('hdnupccode');
	
		
		var UPCarray = new Array();
		UPCarray["custparam_planno"] = getplanno;
		UPCarray["custparam_sku"] = ItemId;
		UPCarray["custparam_skuno"] = ItemName;
		UPCarray["custparam_tasktype"] = getTasktype;
		UPCarray["custparam_location"] = getlocation;
		UPCarray["custparam_actbeginlocationid"] = getBeginLocationid;
		UPCarray["custparam_actbeginlocationname"] = getBeginLocationame;		
		UPCarray["custparam_screenno"] = 'UPC3';	

		var optedEvent = request.getParameter('cmdno');
		if(optedEvent=='NO')
		{
			response.sendRedirect('SUITELET', 'customscript_rf_upccode_plan_no', 'customdeploy_rf_upccode_plan_no_di', false, UPCarray);
		}
		else
		{
			nlapiLogExecution('ERROR', 'ItemId', ItemId);
			nlapiLogExecution('ERROR', 'upccode', upccode);
			var Itype = nlapiLookupField('item', ItemId, 'recordType');
			var ItemRec = nlapiLoadRecord(Itype, ItemId);
			
			if(ItemRec!=null && ItemRec!='')
			{
				nlapiLogExecution('ERROR', 'test1', 'test1');
				ItemRec.setFieldValue('upccode', upccode);
				nlapiSubmitRecord(ItemRec, true);
			}
			
			var filters = new Array();
			
			filters[0] = new nlobjSearchFilter('custrecord_ebiz_report_no', null, 'is', getplanno);
			filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [22]);	
			filters[2] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');			
			
			var SOColumns = new Array();				
			SOColumns[0] = new nlobjSearchColumn('custrecord_sku');
			SOColumns[1] = new nlobjSearchColumn('custrecord_tasktype');
			SOColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
			SOColumns[3] = new nlobjSearchColumn('custrecord_wms_location');
			SOColumns[4] = new nlobjSearchColumn('custrecord_ebiz_report_no');	
			SOColumns[5] = new nlobjSearchColumn('custrecord_ebiz_sku_no');	
			SOColumns[6] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
			SOColumns[6].setSort(false);
			
			nlapiLogExecution('ERROR', 'Before Search Results Id', 'getSearchResultsId');
			var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, SOColumns);
			if(searchresults!=null && searchresults!='')
			{
				var getSearchResultsId = searchresults[0].getId();
				var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask',getSearchResultsId);				
				transaction.setFieldValue('custrecord_act_end_date', DateStamp());
				nlapiSubmitRecord(transaction, true);
				if(searchresults.length > 1)
				{
					var SearchnextResult = searchresults[1];
					UPCarray["custparam_nextlocation"] = SearchnextResult.getText('custrecord_actbeginloc');
					response.sendRedirect('SUITELET', 'customscript_rf_upccode_location', 'customdeploy_rf_upccode_location_di', false, UPCarray);

				}
				else
				{
					response.sendRedirect('SUITELET', 'customscript_rf_upccode_plan_no', 'customdeploy_rf_upccode_plan_no_di', false, UPCarray);
				}
				
			}
			else
			{
				response.sendRedirect('SUITELET', 'customscript_rf_upccode_plan_no', 'customdeploy_rf_upccode_plan_no_di', false, UPCarray);
			}
				
		}

	}

}