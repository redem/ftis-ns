/***************************************************************************
	  		   eBizNET SOLUTIONS Pvt LTD
****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
****************************************************************************
*
*     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Inbound/Suitelet/ebiz_RF_TOCheckin.js,v $
*     	   $Revision: 1.5.2.4.4.3.4.8 $
*     	   $Date: 2014/06/20 14:55:15 $
*     	   $Author: skavuri $
*
*   eBizNET version and checksum stamp.  Do not remove.
*   $eBiznet_VER: .............. $eBizNET_SUM: .....
* PRAMETERS
* DESCRIPTION
* REVISION HISTORY
* $Log: ebiz_RF_TOCheckin.js,v $
* Revision 1.5.2.4.4.3.4.8  2014/06/20 14:55:15  skavuri
* Case # 20148882 SB Issue Fixed
*
* Revision 1.5.2.4.4.3.4.7  2014/06/13 06:15:51  skavuri
* Case# 20148882 (added Focus Functionality for Textbox)
*
* Revision 1.5.2.4.4.3.4.6  2014/05/30 00:26:51  nneelam
* case#  20148622
* Stanadard Bundle Issue Fix.
*
* Revision 1.5.2.4.4.3.4.5  2014/05/19 06:27:33  skreddy
* case # 20148193
* Sonic SB issue fix
*
* Revision 1.5.2.4.4.3.4.4  2013/12/19 16:40:37  gkalla
* case#20126471
* Standard bundle issue
*
* Revision 1.5.2.4.4.3.4.3  2013/08/22 06:58:57  snimmakayala
* Case# 20123979
* NLS - UAT ISSUES(INETR COMPANY XFER)
*
* Revision 1.5.2.4.4.3.4.2  2013/06/11 14:30:41  schepuri
* Error Code Change ERROR to DEBUG
*
* Revision 1.5.2.4.4.3.4.1  2013/04/17 16:04:01  skreddy
* CASE201112/CR201113/LOG201121
* added meta tag
*
* Revision 1.5.2.4.4.3  2012/09/27 10:55:14  grao
* CASE201112/CR201113/LOG201121
*
* Converting multiple language with given Spanish terms
*
* Revision 1.5.2.4.4.2  2012/09/26 12:48:52  grao
* CASE201112/CR201113/LOG201121
* Converting Multi language without small characters
*
* Revision 1.5.2.4.4.1  2012/09/21 14:57:16  grao
* CASE201112/CR201113/LOG201121
* Converting Multilanguage
*
* Revision 1.5.2.4  2012/06/04 09:24:49  mbpragada
* CASE201112/CR201113/LOG201121
*
* Revision 1.5.2.3  2012/04/11 22:06:57  spendyala
* CASE201112/CR201113/LOG201121
* Navigation to previous screen issue is resolved.
*
* Revision 1.5.2.2  2012/03/16 13:56:23  spendyala
* CASE201112/CR201113/LOG201121
* Disable-button functionality is been added.
*
* Revision 1.5.2.1  2012/03/14 07:24:47  snimmakayala
* CASE201112/CR201113/LOG201121
* TO Checkin issues for WBC
*
* Revision 1.6  2012/03/14 07:11:06  snimmakayala
* CASE201112/CR201113/LOG201121
* TO Checkin issues for WBC
*
* Revision 1.5  2012/01/02 11:10:11  spendyala
* CASE201112/CR201113/LOG201121
*  spelling check.
*
* Revision 1.4  2011/12/30 14:33:14  spendyala
* CASE201112/CR201113/LOG201121
* Added code to validate TOstatus and ToLocation.
*
* Revision 1.3  2011/10/14 20:29:53  rrpulicherla
* CASE201112/CR201113/LOG201121
*
* Transfer Order  Files
*
* Revision 1.2  2011/09/29 14:46:42  rrpulicherla
* CASE201112/CR201113/LOG201121
*
* TO Functionality New Files
*
* Revision 1.1  2011/09/28 11:37:38  rrpulicherla
* CASE201112/CR201113/LOG201121
*
* RMA Functionality New Files
*
* Revision 1.3  2011/09/22 09:47:03  rrpulicherla
* CASE201112/CR201113/LOG201121
*
* RMA Functionality New Files
*
* Revision 1.2  2011/09/22 08:59:18  rrpulicherla
* CASE201112/CR201113/LOG201121
*
* RMA Functionality New Files
*
* Revision 1.1  2011/09/22 08:37:21  rrpulicherla
* CASE201112/CR201113/LOG201121
*
* RMA Functionality New Files
*
*
*****************************************************************************/

function TOCheckIn(request, response)
{
	if (request.getMethod() == 'GET') 
	{
		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);
		
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);

		var st0,st1,st2,st3;
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "INGRESAR ORDEN DE PEDIDO";
			st2 = "ENVIAR";
			st3 = "ANTERIOR";

			
		}
		else
		{
			st0 = "";
			st1 = "ENTER TRANSFER ORDER";
			st2 = "SEND";
			st3 = "PREV";
			
		}		
		
		
		
		//customscript_rf_general_script  
		var html = "<html><head><title>" + st0 +  "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript'>";
		html = html + "function OnKeyDown_CL() ";
		html = html + " { ";     
		html = html + "         if (";
		html = html + " event.keyCode == 112 || event.keyCode == 113 || event.keyCode == 114 || event.keyCode == 115 || event.keyCode == 116 || event.keyCode == 117 ||";
		html = html + " event.keyCode == 118 || event.keyCode == 119 || event.keyCode == 120 || event.keyCode == 121 || event.keyCode == 122) {";
		html = html + " var arrElements = document.getElementsByTagName('input');";
		html = html + " var keyFound = false;";
		html = html + " for (i = 0; i < arrElements.length; i++) {";
		html = html + " if (arrElements[i].type == 'submit') {";
		html = html + " switch (event.keyCode) {";
		html = html + " case 112:";
		html = html + " if (arrElements[i].value == 'F1')";	//F7 Key														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 113:";
		html = html + " if (arrElements[i].value == 'F2')";		//F8 Key													
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 114:";
		html = html + " if (arrElements[i].value == 'F3')";	//F9 Key														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 115:";
		html = html + " if (arrElements[i].value == 'F4')";		//F10 Key													
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 116:";
		html = html + " if (arrElements[i].value == 'F5')";		//F11 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 117:";
		html = html + " if (arrElements[i].value == 'F6')";		//F11 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 118:";
		html = html + " if (arrElements[i].value == 'F7')";		//F7 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 119:";
		html = html + " if (arrElements[i].value == 'F8')";		//F8 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 120:";
		html = html + " if (arrElements[i].value == 'F9')";		//F9 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 121:";
		html = html + " if (arrElements[i].value == 'F10')";		//F10 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 122:";
		html = html + " if (arrElements[i].value == 'F11')";		//F11 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " }";
		html = html + " if (keyFound == true) {";        
		html = html + " eval('document._rf_checkin_po.' + arrElements[i].name + '.click();');";
		html = html + " return false;";
		html = html + " }";
		html = html + " }";
		html = html + " }";
		html = html + " }    ";        
		html = html + "    return true; ";
		html = html + "    }";        
		html = html + " </SCRIPT>";

		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//html = html + " document.getElementById('enterpo').focus();";        
		html = html + "</script>";
		html = html + " </head>";
		html = html + "<body onkeydown='return OnKeyDown_CL()'>";
		html = html + "	<form name='_rf_checkin_po' method='POST'>"; //onkeydown='return OnKeyDown_CL()' >";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1;
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterpo' id='enterpo' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st3 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterpo').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');
		var POarray = new Array();
		
		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);
		
		var st4;
		if( getLanguage == 'es_ES')
		{
			st4 = "ORDEN DE TRANSFERENCIA NO V&#193;LIDO";
			
		}
		else
		{
			st4 = "INVALID TRANSFER ORDER";
			
		}
		

		var getPOid=request.getParameter('enterpo');
		nlapiLogExecution('DEBUG','getPOid',getPOid);

		var getOptedField = request.getParameter('hdnOptedField');
		POarray["custparam_option"] = getOptedField;

		nlapiLogExecution('DEBUG', 'optedField', POarray["custparam_option"]);

		var optedEvent = request.getParameter('cmdPrevious');
		
		if (optedEvent == 'F7') 
		{
			response.sendRedirect('SUITELET', 'customscript_rf_receiving_menu', 'customdeploy_rf_receiving_menu_di', false, POarray);
		}
		else
		{
			// This variable is to hold the PO# entered.
			POarray["custparam_poid"] = getPOid;
			POarray["custparam_error"] = st4;
			POarray["custparam_screenno"] = '1T';

			var POfilters=new Array();
			POfilters.push(new nlobjSearchFilter('tranid',null,'is',getPOid));
			POfilters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
			POfilters.push(new nlobjSearchFilter('recordtype',null,'is','transferorder'));
			var vRoleLocation=getRoledBasedLocation();
			nlapiLogExecution('DEBUG', 'vRoleLocation', vRoleLocation);
			if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
			{
				POfilters.push(new nlobjSearchFilter('transferlocation', null, 'anyof', vRoleLocation));
			}

			var POcols=new Array();
			POcols[0]=new nlobjSearchColumn('status');
			POcols[1]=new nlobjSearchColumn('transferlocation');
			POcols[2]= new nlobjSearchColumn('recordtype');

			var PORec=nlapiSearchRecord('transaction',null,POfilters,POcols);

			if(PORec!=null&&PORec!='')
			{
				var poStatus=PORec[0].getValue('status');
				nlapiLogExecution('DEBUG','poStatus',poStatus);
				var poToLocationID=PORec[0].getValue('transferlocation');
				nlapiLogExecution('DEBUG','poToLocation',poToLocationID);
				var recordType=PORec[0].getValue('recordtype');
				nlapiLogExecution('DEBUG','recordType',recordType);

				var fields = ['custrecord_ebizwhsite'];
				var locationcolumns = nlapiLookupField('Location', poToLocationID, fields);
				var Tomwhsiteflag = locationcolumns.custrecord_ebizwhsite;
				nlapiLogExecution('DEBUG','Tomwhsiteflag',Tomwhsiteflag);

				if((recordType=='transferorder' || recordType=='intercompanytransferorder') && 
						(poStatus=='pendingReceipt'||poStatus=='partiallyReceived' || poStatus=='pendingReceiptPartFulfilled'))
				{
					if(Tomwhsiteflag=='T')
					{
						nlapiLogExecution('DEBUG', 'POarray["custparam_poid"]', POarray["custparam_poid"]);

						var ActualBeginDate = DateStamp();
						var ActualBeginTime = TimeStamp();

						nlapiLogExecution('DEBUG', 'ActualBeginDate', ActualBeginDate);
						nlapiLogExecution('DEBUG', 'ActualBeginTime', ActualBeginTime);

						POarray["custparam_actualbegindate"] = ActualBeginDate;
						var TimeArray = new Array();
						TimeArray = ActualBeginTime.split(' ');

						POarray["custparam_actualbegintime"] = TimeArray[0];
						POarray["custparam_actualbegintimeampm"] = TimeArray[1];

						nlapiLogExecution('DEBUG', 'custparam_actualbegintime', POarray["custparam_actualbegintime"]);
						nlapiLogExecution('DEBUG', 'custparam_actualbegintimeampm', POarray["custparam_actualbegintimeampm"]);

//						var POfilters = new Array();
//						POfilters[0] = new nlobjSearchFilter ('tranid', null, 'is', POarray["custparam_poid"]);

						// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
						// to the previous screen.
						var optedEvent = request.getParameter('cmdPrevious');

						//	if the previous button 'F7' is clicked, it has to go to the previous screen 
						//  ie., it has to go to RF Main Menu.

//						var POcols = new Array();
//						POcols[0]= new nlobjSearchColumn('transferlocation');
//						POcols[1]= new nlobjSearchColumn('custbody_nswms_company');
//						//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
//						var searchresults = nlapiSearchRecord('transferorder', null, POfilters, POcols);

						if (PORec != null && PORec.length > 0) {
							nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is not null');

							if (POarray["custparam_poid"] != null) 
							{
								//var whLocation= searchresults[0].getValue('location');						
								var whLocation= PORec[0].getValue('transferlocation');
								var whLocationText= PORec[0].getText('transferlocation');
								nlapiLogExecution('DEBUG', 'whLocationText', whLocationText);
								nlapiLogExecution('DEBUG', 'whLocationValue', whLocation);
//								var whCompany= PORec[2].getValue('custbody_nswms_company');
								POarray["custparam_whlocation"] = whLocation;
//								POarray["custparam_company"] =whCompany;
								POarray["custparam_company"] ='';
								response.sendRedirect('SUITELET', 'customscript_rf_tocheckin_sku', 'customdeploy_rf_tocheckin_sku_di', false, POarray);
							}
							else 
							{
								//	if the 'Send' button is clicked without any option value entered,
								//  it has to remain in the same screen ie., Receiving Menu.
								nlapiLogExecution('DEBUG', 'PONo', POarray["custparam_poid"]);
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							}
						}
						else 
						{
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
						}

					}
					else 
					{
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('DEBUG', 'SearchResults ', 'ToLocation is not Make warhouse');
					}
				}
				else 
				{
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					nlapiLogExecution('DEBUG', 'Error ', 'TO Status is not Pending receipt or Partially received');
				}
			} 
			else 
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'Error ', 'rec doesnt exist');
			}
		}
	}//end of else loop.
}
