
/***************************************************************************
  eBizNET Solutions 
 ****************************************************************************
 *
 *Â  $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_QuickShip_SL.js,v $
 *Â  $Revision: 1.6.2.10.4.4.4.28.2.7 $
 *Â  $Date: 2015/11/16 22:54:39 $
 *Â  $Author: sponnaganti $
 *Â  $Name: b_WMS_2015_2_StdBundle_Issues $
 * DESCRIPTION
 *Â  Functionality
 *
 * REVISION HISTORY
 *Â  $Log: ebiz_QuickShip_SL.js,v $
 *Â  Revision 1.6.2.10.4.4.4.28.2.7  2015/11/16 22:54:39  sponnaganti
 *Â  201415701 201415702
 *Â  2015.2 issue fix
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.28.2.6  2015/11/14 12:49:07  aanchal
 *Â  2015.2 Issue fix
 *Â  201414241
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.28.2.5  2015/11/10 16:17:49  vamsyp
 *Â  201414107
 *Â  Redirection to the form is missing
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.28.2.4  2015/11/09 14:42:00  snimmakayala
 *Â  201415435
 *Â  2015,2 Issues
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.28.2.3  2015/10/30 13:10:54  schepuri
 *Â  case# 201415250
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.28.2.2  2015/10/13 15:44:07  deepshikha
 *Â  2015.2 issues
 *Â  201414332
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.28.2.1  2015/09/23 14:58:05  deepshikha
 *Â  2015.2 issueFix
 *Â  201414466
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.28  2015/08/24 14:40:31  schepuri
 *Â  case# 201414016,201414017
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.27  2015/07/29 12:00:46  rrpulicherla
 *Â  Case#201413343
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.26  2015/05/05 13:26:14  schepuri
 *Â  case# 201412598
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.25  2014/09/19 11:45:38  skavuri
 *Â  case # 201410194,201410230
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.24  2014/07/25 15:15:59  skavuri
 *Â  Case# 20149651Compatibility issue fixed
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.23  2014/06/25 14:47:12  rmukkera
 *Â  Case # 20149072
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.22  2014/06/10 13:17:50  rmukkera
 *Â  Case # 20148832
 *Â  VRA functionality added
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.21  2014/05/30 11:58:37  spendyala
 *Â  CASE201112/CR201113/LOG201121
 *Â  Issue fixed related to case#20148276
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.20  2014/05/27 14:58:11  grao
 *Â  Case#: 20147976  Standard  issue fixes
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.19  2014/04/24 16:04:02  nneelam
 *Â  case#  20147976
 *Â  Stanadard Bundle Issue Fix.
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.18  2014/04/21 15:58:39  skavuri
 *Â  Case # 20148099 SB Issue fixed
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.17  2014/04/18 16:05:55  gkalla
 *Â  case#20148068
 *Â  Quick ship to show already shipped orders message
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.16  2014/03/28 15:35:01  skavuri
 *Â  Case # 20127782 issue fixed
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.15  2014/02/20 10:06:41  snimmakayala
 *Â  no message
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.14  2014/02/18 06:06:03  gkalla
 *Â  case#20127176
 *Â  PMM Quick ship issue
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.13  2014/02/12 07:37:01  spendyala
 *Â  CASE201112/CR201113/LOG201121
 *Â  Issue fixed related to case#20127138
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.12  2013/12/20 14:33:03  skreddy
 *Â  case#20126519
 *Â  Tek prod issue fix
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.11  2013/11/25 06:52:01  vmandala
 *Â  Case# 20125912
 *Â  Fixed subnmittin page  on changing dropdown values
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.10  2013/10/10 15:46:57  rmukkera
 *Â  Case# 20124701
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.9  2013/10/03 17:02:30  gkalla
 *Â  Case# 20120597
 *Â  Enabled Shipping charges field
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.8  2013/09/30 15:57:25  rmukkera
 *Â  Case# 20124699
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.7  2013/08/05 14:23:09  spendyala
 *Â  Case# 20123654
 *Â 
 *Â  Added new Order type field
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.6  2013/05/15 13:36:33  rrpulicherla
 *Â  CASE201112/CR201113/LOG201121
 *Â  Quickship
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.5  2013/04/29 15:03:14  grao
 *Â  CASE201112/CR201113/LOG201121
 *Â  Standard bundle issues fixes
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.4  2013/04/19 15:42:29  skreddy
 *Â  CASE201112/CR201113/LOG201121
 *Â  issue fixes
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.3  2013/04/02 16:02:08  rrpulicherla
 *Â  CASE201112/CR201113/LOG201121
 *Â  TSg Issue fixes
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.2  2013/03/15 15:01:16  snimmakayala
 *Â  CASE201112/CR201113/LOG2012392
 *Â  Production and UAT issue fixes.
 *Â 
 *Â  Revision 1.6.2.10.4.4.4.1  2013/03/05 14:57:31  skreddy
 *Â  CASE201112/CR201113/LOG201121
 *Â  Code merged from Lexjet production as part of Standard bundle
 *Â 
 *Â  Revision 1.6.2.10.4.4  2013/01/24 01:32:36  kavitha
 *Â  CASE201112/CR201113/LOG201121
 *Â  Quick ship screen - Query block changes
 *Â 
 *Â  Revision 1.6.2.10.4.3  2013/01/02 18:04:22  kavitha
 *Â  CASE201112/CR201113/LOG201121
 *Â  Quick ship screen - Query block changes
 *Â 
 *Â  Revision 1.6.2.10.4.2  2012/11/23 06:34:26  schepuri
 *Â  CASE201112/CR201113/LOG201121
 *Â  Issue related to displaying the shipmethod in sublist
 *Â 
 *Â  Revision 1.6.2.10.4.1  2012/11/01 14:55:02  schepuri
 *Â  CASE201112/CR201113/LOG201121
 *Â  Decimal Qty Conversions
 *Â 
 *Â  Revision 1.6.2.10  2012/08/31 01:42:08  snimmakayala
 *Â  CASE201112/CR201113/LOG201121
 *Â  Fisk Production Issue Fixes
 *Â 
 *Â  Revision 1.6.2.9  2012/05/09 10:55:32  snimmakayala
 *Â  CASE201112/CR201113/LOG201121
 *Â  Partial Status Handling
 *Â 
 *Â  Revision 1.6.2.8  2012/05/09 09:17:12  snimmakayala
 *Â  CASE201112/CR201113/LOG201121
 *Â  Partial Status Handling
 *Â 
 *Â  Revision 1.6.2.7  2012/04/26 12:03:32  snimmakayala
 *Â  CASE201112/CR201113/LOG201121
 *Â  QuickShip
 *Â 
 *Â  Revision 1.6.2.6  2012/04/10 22:43:50  snimmakayala
 *Â  CASE201112/CR201113/LOG201121
 *Â  Quick Ship for Line Level Shipping
 *Â 
 *Â  Revision 1.11  2012/04/10 22:32:47  snimmakayala
 *Â  CASE201112/CR201113/LOG201121
 *Â  Production issue fixes
 *Â 
 *Â  Revision 1.10  2012/03/21 00:06:12  snimmakayala
 *Â  CASE201112/CR201113/LOG201121
 *Â  Quick Ship for line level shipping
 *Â 
 *Â  Revision 1.9  2012/03/09 07:14:27  rmukkera
 *Â  CASE201112/CR201113/LOG201121
 *Â  transferorder related code modifications .
 *Â 
 *Â  Revision 1.8  2012/02/28 11:29:28  rmukkera
 *Â  CASE201112/CR201113/LOG201121
 *Â  Quickship Error Message Change
 *Â 
 *Â  Revision 1.7  2012/02/24 15:14:51  rmukkera
 *Â  CASE201112/CR201113/LOG201121
 *Â  code changed while checking the quickshipconfiguration in the getSoInternalId Method
 *Â 
 *Â  Revision 1.6  2012/02/17 20:21:43  snimmakayala
 *Â  CASE201112/CR201113/LOG201121
 *Â  Stable bundle issue fixes
 *Â 
 *Â  Revision 1.5  2012/02/07 14:32:04  snimmakayala
 *Â  CASE201112/CR201113/LOG201121
 *Â  quickship issue fixes
 *Â 
 *Â  Revision 1.4  2012/02/06 14:21:32  rmukkera
 *Â  CASE201112/CR201113/LOG201121
 *Â  new fields were added to sublist(fulfiment Ord ,shipment method )
 *Â 
 *Â  Revision 1.3  2012/02/03 19:02:11  rrpulicherla
 *Â  CASE201112/CR201113/LOG201121
 *Â 
 *Â  Quickship message
 *Â 
 *Â  Revision 1.2  2012/02/03 13:57:33  rmukkera
 *Â  CASE201112/CR201113/LOG201121
 *Â    sofield made as mandatory
 *Â 
 *Â  Revision 1.1  2012/02/02 14:10:14  rmukkera
 *Â  CASE201112/CR201113/LOG201121
 *Â    QuickShip new file
 *Â 
 *
 ****************************************************************************/
var tempOpentaskResultsArray=null;
function getOpentaskSearchResults(SoId,maxno,trantype,vSoWave)
{
	nlapiLogExecution('ERROR', 'Into getOpentaskSearchResults SoId',SoId);

	var vContLP = request.getParameter('custpage_qbcontlp');
	/*** The below code is merged from Lexjet production account on 05thMar13 by Santosh as part of Standard bundle***/
	var vRoleLocation=getRoledBasedLocation();
	/*** up to here ***/
	var filtersPicktsk = new Array();
	filtersPicktsk.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SoId));
	if(vSoWave!=null && vSoWave!='')
		filtersPicktsk.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(vSoWave)));
	if(vContLP != null && vContLP != '')
		filtersPicktsk.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vContLP));
	filtersPicktsk.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [28]));//status.outbound.packcomplete
	filtersPicktsk.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));//pick task
	if(trantype!="vendorreturnauthorization")
	{
		filtersPicktsk.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',  null,'isnotempty'));
	}
	/*** The below code is merged from Lexjet production account on 05thMar13 by Santosh as part of Standard bundle***/
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		filtersPicktsk.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vRoleLocation));

	}
	/*** up to here ***/
	if(maxno!=-1)
	{
		filtersPicktsk.push(new nlobjSearchFilter('id',  null,'greaterthan', maxno));
	}
	/*** The below code is merged from Lexjet production account on 05thMar13 by Santosh as part of Standard bundle***/
	filtersPicktsk.push(new nlobjSearchFilter('mainline','custrecord_ebiz_order_no','is','T'));
	var trantypeid;
	if(trantype == 'vendorreturnauthorization')
		{
		trantypeid = 'VendAuth';
		}
	if(trantype == 'salesorder')
		{
		trantypeid = 'SalesOrd';
		}
	if(trantype == 'transferorder')
		{
		trantypeid = 'TrnfrOrd';
        }
	nlapiLogExecution('ERROR', 'Into trantypeid ',trantypeid);

	filtersPicktsk.push(new nlobjSearchFilter('type','custrecord_ebiz_order_no','is',trantypeid));
	/*** up to here ***/
	var columnsPicktsk = new Array();
	columnsPicktsk[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columnsPicktsk[1] = new nlobjSearchColumn('custrecord_container_lp_no');
	columnsPicktsk[2] = new nlobjSearchColumn('custrecord_total_weight');
	columnsPicktsk[3] = new nlobjSearchColumn('name');
	columnsPicktsk[4] = new nlobjSearchColumn('custrecord_line_no');
	columnsPicktsk[5] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columnsPicktsk[6] = new nlobjSearchColumn('id');
	columnsPicktsk[7] = new nlobjSearchColumn('shipdate','custrecord_ebiz_order_no');
	columnsPicktsk[8] = new nlobjSearchColumn('entity','custrecord_ebiz_order_no');
	columnsPicktsk[9] = new nlobjSearchColumn('shipmethod','custrecord_ebiz_order_no');
	columnsPicktsk[6].setSort();



	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersPicktsk, columnsPicktsk);
	nlapiLogExecution('ERROR', 'searchresults' ,searchresults);
	if(searchresults!=null)
	{
		if(tempOpentaskResultsArray==null)
		{
			tempOpentaskResultsArray=new Array();
		}
		if(searchresults.length>=1000)
		{
			nlapiLogExecution('ERROR', 'InTo More Than 1000 rec');
			var maxno1=searchresults[searchresults.length-1].getValue(columns[6]);
			tempOpentaskResultsArray.push(searchresults);
			getOpentaskSearchResults(SoId,maxno1,trantype);
		}
		else
		{
			nlapiLogExecution('ERROR', 'InTo Less Than 1000 rec');
			tempOpentaskResultsArray.push(searchresults);
		}
	}
	else
	{
		tempOpentaskResultsArray = null;
		nlapiLogExecution('ERROR', 'tempOpentaskResultsArray' ,tempOpentaskResultsArray);
	}
	return tempOpentaskResultsArray;
}

var vtempOpentaskResultsArray=new Array();
function getOpentaskSearchResults1(SoId,maxno)
{
	nlapiLogExecution('ERROR', 'Into getOpentaskSearchResults SoId',SoId);

	var vContLP = request.getParameter('custpage_qbcontlp');
	/*** The below code is merged from Lexjet production account on 05thMar13 by Santosh as part of Standard bundle***/
	var vRoleLocation=getRoledBasedLocation();
	/*** up to here ***/
	var filtersPicktsk = new Array();
	filtersPicktsk.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SoId));
	if(vContLP != null && vContLP != '')
		filtersPicktsk.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vContLP));
	filtersPicktsk.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [28]));//status.outbound.packcomplete
	filtersPicktsk.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));//pick task
	filtersPicktsk.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',  null,'isnotempty'));
	/*** The below code is merged from Lexjet production account on 05thMar13 by Santosh as part of Standard bundle***/
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		filtersPicktsk.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vRoleLocation));

	}
	/*** up to here ***/
	if(maxno!=-1)
	{
		filtersPicktsk.push(new nlobjSearchFilter('id',  null,'greaterthan', maxno));
	}
	/*** The below code is merged from Lexjet production account on 05thMar13 by Santosh as part of Standard bundle***/
	filtersPicktsk.push(new nlobjSearchFilter('mainline','custrecord_ebiz_order_no','is','T'));
	/*** up to here ***/
	var columnsPicktsk = new Array();
	columnsPicktsk[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columnsPicktsk[1] = new nlobjSearchColumn('custrecord_container_lp_no');
	columnsPicktsk[2] = new nlobjSearchColumn('custrecord_total_weight');
	columnsPicktsk[3] = new nlobjSearchColumn('name');
	columnsPicktsk[4] = new nlobjSearchColumn('custrecord_line_no');
	columnsPicktsk[5] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columnsPicktsk[6] = new nlobjSearchColumn('id');
	columnsPicktsk[7] = new nlobjSearchColumn('shipdate','custrecord_ebiz_order_no');
	columnsPicktsk[8] = new nlobjSearchColumn('entity','custrecord_ebiz_order_no');
	columnsPicktsk[9] = new nlobjSearchColumn('shipmethod','custrecord_ebiz_order_no');
	columnsPicktsk[6].setSort();



	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersPicktsk, columnsPicktsk);
	if(searchresults!=null)
	{
		if(searchresults.length>=1000)
		{
			nlapiLogExecution('ERROR', 'InTo More Than 1000 rec');
			var maxno1=searchresults[searchresults.length-1].getValue(columns[6]);
			vtempOpentaskResultsArray.push(searchresults);
			getOpentaskSearchResults(SoId,maxno1);
		}
		else
		{
			nlapiLogExecution('ERROR', 'InTo Less Than 1000 rec');
			vtempOpentaskResultsArray.push(searchresults);
		}
	}
	return vtempOpentaskResultsArray;
}


function QuickShipSL(request,response)
{
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Quick Ship');
		createform(form);
		form.addSubmitButton('Display');
		response.writePage(form);
	}
	else
	{
		//var vSo = request.getParameter('custpage_qbso');
		var vContLP = request.getParameter('custpage_qbcontlp');
		var vSoMultiple = request.getParameter('custpage_somulti');
		var vSoWave = request.getParameter('custpage_qbwave');
		var vSoCluster = request.getParameter('custpage_qbcluster');
		var vSOStatus = request.getParameter('custpage_qbstatus');
		var vFromDate = request.getParameter('custpage_qbfromdate');
		var vToDate = request.getParameter('custpage_qbtodate');		
		var vShipDate = request.getParameter('custpage_qbshipdate');
		var vCustomer = request.getParameter('custpage_qbcustomer');
		var vCarrier = request.getParameter('custpage_qbcarriershipmethod');
		var ordertype = request.getParameter('custpage_ordertype');
		nlapiLogExecution("ERROR","ordertype",ordertype);
		var form = nlapiCreateForm('Quick Ship');
		var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
		msg.setLayoutType('outside','startcol');

		createform(form);
		//Case 20125912 start submitting ppage on selectibg drpdownvalue
		var newpageval = request.getParameter('custpage_hiddenfield');
		nlapiLogExecution('ERROR', 'newpageval', newpageval);
		//case end
		var SoId=new Array();
		//var ordlines = new Array();
		form.addSubmitButton('Display');		

		var opentasksosearchresults = new Array();
		var shipmethod='';
		//if((vSo!=null && vSo!='') || (vContLP != null && vContLP != '')  || (vSoMultiple != null && vSoMultiple != '') )
		if((vContLP != null && vContLP != '') || (vSoMultiple != null && vSoMultiple != '') || (vSoWave != null && vSoWave != '') || (vSoCluster != null && vSoCluster != '') || (vSOStatus != null && vSOStatus != '') || (vFromDate != null && vFromDate != '') || (vToDate != null && vToDate != '') || (vShipDate != null && vShipDate != '') || (vCustomer != null && vCustomer != '') || (vCarrier != null && vCarrier != ''))
		{
			/*nlapiLogExecution('ERROR','vSo',vSo);
			if(vSo != null && vSo != '')
				SoId=GetSOInternalId(vSo);*/
			if((vContLP != null && vContLP != '') || (vSoWave != null && vSoWave != '') || (vSoCluster != null && vSoCluster != '') || (vSOStatus != null && vSOStatus != '') || (vFromDate != null && vFromDate != '') || (vToDate != null && vToDate != '') || (vShipDate != null && vShipDate != '') || (vCustomer != null && vCustomer != '') || (vCarrier != null && vCarrier != ''))
			{	
				var vOTDet = new Array();
				vOTDet=GetSOIntIdByContLP(vContLP,vSoWave,vSoCluster,vSOStatus,vFromDate,vToDate,vShipDate,vCustomer,vCarrier);
				nlapiLogExecution('ERROR', 'vOTDet', vOTDet);
				if(vOTDet != null && vOTDet != '')
				{
					for(var i=0;i<vOTDet.length;i++)
					{
						SoId.push(vOTDet[i].getValue('custrecord_ebiz_order_no'));
					}	
					SoId = removeDuplicateElement(SoId);
					nlapiLogExecution('ERROR', 'Order array Internal ID when SO not selected', SoId);
				}
				else
				{
					nlapiLogExecution('ERROR', 'vOTDet else', vOTDet);
					msg.setDefaultValue("<div id='div__alert' align='center'><script>showAlertBox('div__alert', 'Error', 'No Data found with this combination', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
					response.writePage(form);
					return;

					//var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);		  
					//msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'No Data for the given input', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");		    		   								  
					//response.writePage(form);
					//return false;
				}
			}
			if(vSoMultiple != null && vSoMultiple != '')
			{	
				//var vSOArr=new Array();
				if(vSoMultiple != null && vSoMultiple != "")
				{
					SoId=GetSOInternalId(vSoMultiple,ordertype);
					//SoId = vSoMultiple.split('');
				}
				nlapiLogExecution('ERROR', 'Order array Internal ID', SoId);
			}

			nlapiLogExecution('ERROR','SoId',SoId);
			var displaysublist = 'T';

			if(SoId!=null && SoId!="")
			{
				nlapiLogExecution('ERROR','SoId',SoId);
				nlapiLogExecution('ERROR','ordertype',ordertype);
				var vOrdStatusRec= getOrdStatus(SoId,ordertype);
				if(vOrdStatusRec != null && vOrdStatusRec != '')
				{
					var vBillSatus=false;
					for(var s=0;s<vOrdStatusRec.length;s++)
					{
						vStatus=vOrdStatusRec[s].getValue('status');
						if(vStatus == 'pendingBilling' || vStatus == 'fullyBilled' || vStatus == 'pendingReceipt' || vStatus == 'received'  )
						{
							nlapiLogExecution('ERROR','Order already shipped');
							vBillSatus=true;

							break;
						}	
					}
					if(vBillSatus==true)
					{
						msg.setDefaultValue("<div id='div__alert' align='center'><script>showAlertBox('div__alert', 'Error', 'Order already shipped', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
						response.writePage(form);
						return;
					}	
				}				
				else
				{
					nlapiLogExecution('ERROR','inside error msg for ordstatusrec');
					displaysublist = 'F';
					msg.setDefaultValue("<div id='div__alert' align='center'><script>showAlertBox('div__alert', 'Error', 'No Details found with this combination', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
					response.writePage(form);
					return;
				}

				var Orders = new Array();
				var QuickShipfalseOrders = new Array();
				Orders = IsQuickShipAllowed(SoId);
				var trantype = nlapiLookupField('transaction', SoId, 'recordType');
				nlapiLogExecution('Debug', 'trantype', trantype);
				if((Orders!=null && Orders!='' && Orders.length > 0) || trantype=="vendorreturnauthorization")
				{					
					var foresults = IsQuickShipAllowedforFO(SoId,vSoWave);
					var opentaskresults = IsQuickShipAllowedforopentask(SoId,vSoWave,vContLP);
					nlapiLogExecution('Debug', 'foresults', foresults);
					if((foresults=="F" && opentaskresults=="F") || trantype=="vendorreturnauthorization")
					{
						opentasksosearchresults = getOpentaskSearchResults(SoId,-1,trantype,vSoWave);
						// case# 201417159
						//nlapiLogExecution('ERROR','opentasksosearchresults.length',opentasksosearchresults.length);

					if (opentasksosearchresults != null && opentasksosearchresults.length > 0) 
					{
						var HoldStatus;
						var vordername;

						for(var u=0;u<opentasksosearchresults.length;u++)
						{
							nlapiLogExecution('ERROR', 'SOId Before Test',SoId);								
							HoldStatus = fnCreateFo(SoId);
							nlapiLogExecution('ERROR', 'HoldStatus before Test',HoldStatus );							

							//nlapiLogExecution('ERROR', 'HoldStatus after test',HoldStatus );

							if(HoldStatus == 'F')
							{
								nlapiLogExecution('ERROR', 'SOId after test',SoId);
								//nlapiLogExecution('ERROR', 'SOName after test', );
								vordername = SoId;
								break;
								//	msg.setDefaultValue("<div id='div__alert' align='center'><script>showAlertBox('div__alert', 'Error', 'SO/TO :- " + vordername + " Status is Closed/Cancelled/Payment Hold for Quickship', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>")
							}

						}


						var temparraynew=new Array();var tempindex=0;
						for(k=0;k<opentasksosearchresults.length;k++)
						{
							var opentasksosearchresults = opentasksosearchresults[k];

							for(var j=0;j<opentasksosearchresults.length;j++)
							{
								temparraynew[tempindex]=opentasksosearchresults[j];
								tempindex=tempindex+1;
							}
						}							


						if(HoldStatus == 'F')
						{
							//msg.setDefaultValue("<div id='div__alert' align='center'><script>showAlertBox('div__alert', 'Error', 'SO/TO :- " + vordername + " Status is Closed/Cancelled/Payment Hold for Quickship', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
							msg.setDefaultValue("<div id='div__alert' align='center'><script>showAlertBox('div__alert', 'Error', 'SO/TO Status is Closed/Cancelled/Payment Hold for Quickship', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
							response.writePage(form);
							return;
						}
						else
						{
							nlapiLogExecution('ERROR', 'displaysublist',displaysublist);
							if(displaysublist == 'T')
							{
								QuickShipfalseOrders = displaySublist(temparraynew,Orders,form,msg,trantype);
							}
						}
						//QuickShipfalseOrders = displaySublist(temparraynew,Orders,form,msg);

						if(QuickShipfalseOrders!=null && QuickShipfalseOrders!='' && QuickShipfalseOrders.length > 0)
						{
							QuickShipfalseOrders = removeDuplicateElement(QuickShipfalseOrders);
							nlapiLogExecution('ERROR','QuickShipfalseOrders',QuickShipfalseOrders);
							msg.setDefaultValue("<div id='div__alert' align='center'><script>showAlertBox('div__alert', 'Error', 'The Service Level for these SO/TO are not tagged for Quickship - "+QuickShipfalseOrders+"', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
							response.writePage(form);
							return;
							}
						}
					}
					else
					{
						nlapiLogExecution('Debug', 'test', 'test');
						msg.setDefaultValue("<div id='div__alert' align='center'><script>showAlertBox('div__alert', 'Error', 'SO/TO still having some open picks.', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
						//return;
					}
				}
				else
				{
					msg.setDefaultValue("<div id='div__alert' align='center'><script>showAlertBox('div__alert', 'Error', 'The Service Level for this SO/TO is not tagged for Quickship.', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
					response.writePage(form);					
					return;
				}
			}			
			else
			{
				msg.setDefaultValue("<div id='div__alert' align='center'><script>showAlertBox('div__alert', 'Error', 'The Service Level for this SO/TO is not tagged for Quickship.', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
				response.writePage(form);	
				return;
			}
			
		}
		else
		{

		}	

		var refreshpage=false;
		var contlpArray=new Array();
		var message = "";


		nlapiLogExecution('ERROR', 'newpageval111',newpageval);
		var refreshpage=false;
		var contlpArray=new Array();
		//Case 20125912 start submitting ppage on selectibg drpdownvalue
		if(newpageval != 'T'){
			//case end
			var lineCount = request.getLineItemCount('custpage_items');

			if(lineCount!=null && lineCount>0)	
			{
				for (var k=1; k<=lineCount; k++)
				{
					if(request.getLineItemValue('custpage_items', 'custpage_select', k) == 'T')
					{
						var contlp=request.getLineItemValue('custpage_items', 'custpage_container', k);
						var SalesOrder=request.getLineItemValue('custpage_items', 'custpage_sono', k);
						var SalesOrderId=request.getLineItemValue('custpage_items', 'custpage_sointernalid', k);

						var trackingno=request.getLineItemValue('custpage_items', 'custpage_trackingno', k);
						var actwght=request.getLineItemValue('custpage_items', 'custpage_weight', k);
						var shipcharges=request.getLineItemValue('custpage_items','custpage_shipcharges', k);
						nlapiLogExecution('ERROR','SHIPCharges',shipcharges);
						var	vcarrier='';
						CreateShipManifestRecord(SalesOrderId,contlp,vcarrier,trackingno,actwght,shipcharges);
						//contlpArray[contlpArray.length]=contlp;

						if(message == "")
						{
							message = ""+SalesOrder+" with containerlp(s) "+contlp.toString()+"";
						}
						else
						{
							message = message + "," + ""+SalesOrder+" with containerlp(s) "+contlp.toString()+"";						
						}						
						refreshpage=true;
						response.writePage(form);
					}		

				}
				//form.addSubmitButton('Submit');
			}	
		}
		if(refreshpage!=false)
		{
			nlapiLogExecution('ERROR', 'into refreshpage',SoId);
			if(SoId!=null && SoId!="")
			{
				var ordlines1 = new Array();
				var quickshipflagorders1 = new Array();
				ordlines1=IsQuickShipAllowed(SoId);

				//var trantype = nlapiLookupField('transaction', SoId, 'recordType');
				//nlapiLogExecution('ERROR', 'trantype', trantype);
				//var opentasksosearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersPicktsk, columnsPicktsk);
				//Empty the global array written by suman as on 30th May 2014
				nlapiLogExecution('ERROR', 'tempOpentaskResultsArrayBefore',tempOpentaskResultsArray);
				tempOpentaskResultsArray.length=0;
				vtempOpentaskResultsArray.length=0;// case no 201410194
				nlapiLogExecution('ERROR', 'tempOpentaskResultsArrayAfter',tempOpentaskResultsArray);
				//end of code to empty global array.
				var trantype = nlapiLookupField('transaction', SoId, 'recordType');
				nlapiLogExecution('Debug', 'trantype', trantype);
				opentasksosearchresults = getOpentaskSearchResults(SoId,-1,trantype,vSoWave);
				//nlapiLogExecution('ERROR','opentasksosearchresults.length',opentasksosearchresults);
				var temparraynew=new Array();
				if (opentasksosearchresults != null) 
				{
					var tempindex=0;
					for(k=0;k<opentasksosearchresults.length;k++)
					{
						var opentasksosearchresults = opentasksosearchresults[k];

						for(var j=0;j<opentasksosearchresults.length;j++)
						{
							temparraynew[tempindex]=opentasksosearchresults[j];
							tempindex=tempindex+1;
						}
					}	
					nlapiLogExecution('ERROR','temparraynew',temparraynew);
				}

				if((ordlines1!=null && ordlines1!='' && ordlines1.length > 0)|| trantype=="vendorreturnauthorization")
				{
					nlapiLogExecution('ERROR', 'ordlines1 length',ordlines1.length);
					form.getSubList('custpage_items').setLineItemValues(null);
					quickshipflagorders1 = displaySublist(temparraynew,ordlines1,form,msg,trantype);	
				}
				else
				{
					form.getSubList('custpage_items').setLineItemValues(null);
				}
				//if(trantype=='salesorder')
				//{
				msg.setDefaultValue("<div id='div__alert' align='center'><script>showAlertBox('div__alert', 'Confirmation','"+message+" Shipped Successfully' , NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
				response.writePage(form);	
				return;
				//}
				//else
				//{
				//	msg.setDefaultValue("<div id='div__alert' align='center'><script>showAlertBox('div__alert', 'Confirmation','"+message+" shipped sucessfully' , NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
				//}

			}
			response.writePage(form);
		}
		else
		{
			response.writePage(form);
		}
	}
}
if (!Array.prototype.indexOf)
{
	Array.prototype.indexOf = function(elt /*, from*/)
	{
		var len = this.length;

		var from = Number(arguments[1]) || 0;
		from = (from < 0)
		? Math.ceil(from)
				: Math.floor(from);
		if (from < 0)
			from += len;

		for (; from < len; from++)
		{
			if (from in this &&
					this[from] === elt)
				return from;
		}
		return -1;
	};
}

function displaySublist(opentasksosearchresults,OrderDetails,form,msg,trantype)
{
	var QuickShipFalseOrders = new Array();
	//var shipmethod='';
	var tempOrdersArray = new Array();

	if(opentasksosearchresults!=null && opentasksosearchresults!='' && opentasksosearchresults.length>0)
	{
		nlapiLogExecution('ERROR','opentasksosearchresults.length',opentasksosearchresults.length);
		nlapiLogExecution('ERROR','OrderDetails.length',OrderDetails.length);
		var strcntlp=new Array;var count=0;
		var ordlineSO,ordlineLineNo,quickshipflag = "";
		for(var i=0;i<opentasksosearchresults.length;i++)
		{
			//if(opentasksosearchresults[i].getValue('custrecord_ebiz_order_no')!=null && opentasksosearchresults[i].getValue('custrecord_ebiz_order_no')!='')
			//{
			//nlapiLogExecution('ERROR','opentasksosearchresults soid',opentasksosearchresults[i].getValue('custrecord_ebiz_order_no'));
			//var searchrec=nlapiLoadRecord('salesorder',opentasksosearchresults[i].getValue('custrecord_ebiz_order_no'));
			//if(searchrec!=null && searchrec!='')
			//{
			//	shipmethod=searchrec.getFieldText('shipmethod');
			//nlapiLogExecution('ERROR','shipmethod',shipmethod);
			//}
			//}
			var so=opentasksosearchresults[i].getText('custrecord_ebiz_order_no');
			var sovalue=opentasksosearchresults[i].getValue('custrecord_ebiz_order_no');
			var cntlp=opentasksosearchresults[i].getValue('custrecord_container_lp_no');
			var wgt=parseFloat(opentasksosearchresults[i].getValue('custrecord_total_weight'));
			var name=opentasksosearchresults[i].getValue('name');
			var lineno=opentasksosearchresults[i].getValue('custrecord_line_no');

			for(var j=0;j<OrderDetails.length;j++)
			{
				//nlapiLogExecution('ERROR','so',so);
				//nlapiLogExecution('ERROR','sovalue',sovalue);
				//nlapiLogExecution('ERROR','lineno',lineno);
				//nlapiLogExecution('ERROR','OrderDetails',OrderDetails[j]);

				var Ordersplit = OrderDetails[j].split('-');
				ordlineSO = Ordersplit[0];
				ordlineLineNo = Ordersplit[1];
				quickshipflag = Ordersplit[2];
				//nlapiLogExecution('ERROR','ordlineSO',ordlineSO);
				//nlapiLogExecution('ERROR','ordlineLineNo',ordlineLineNo);
				//nlapiLogExecution('ERROR','quickshipflag',quickshipflag);				

				if((ordlineSO == sovalue) && (ordlineLineNo == lineno))
				{
					if(trantype != "vendorreturnauthorization")
					{
						if(quickshipflag == "T")
						{
							if(strcntlp.indexOf(cntlp)==-1)
							{							
								strcntlp.push(cntlp);						

								tempOrdersArray.push(opentasksosearchresults[i]);
								//nlapiLogExecution('ERROR','tempOrdersArray',tempOrdersArray.length);	
								//nlapiLogExecution('ERROR','tempOrdersArray[0]',tempOrdersArray[0].getText('custrecord_ebiz_order_no'));								
								//count=count+1;						
							}
						}
						else
						{
							QuickShipFalseOrders.push(so);
						}
					}
					else
					{
						tempOrdersArray.push(opentasksosearchresults[i]);
					}
				}
			}
		}				

		nlapiLogExecution('ERROR', 'tempOrdersArray.length ', tempOrdersArray.length); 
		// paging dropdown  
		var test='';
		if(tempOrdersArray.length>0)
		{
			if(tempOrdersArray.length> 25)
			{
				if(form.getField('custpage_pagesize') == null)
				{
					var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('entry');
					pagesize.setDisplaySize(10,10);
					pagesize.setLayoutType('outsidebelow', 'startrow');
					var select= form.addField('custpage_selectpage','select', 'Select Records');	
					select.setLayoutType('outsidebelow', 'startrow');			
					select.setDisplaySize(200,30);
				}
				else
				{
					var pagesize = form.getField('custpage_pagesize');
					var select= form.getField('custpage_selectpage');
				}
				if (request.getMethod() == 'GET'){

					pagesize.setDefaultValue("25");							
					pagesizevalue=25;
				}
				else
				{
					if(request.getParameter('custpage_pagesize')!=null)
					{pagesizevalue= request.getParameter('custpage_pagesize');}
					else
					{pagesizevalue= 25;pagesize.setDefaultValue("25");}

				}
				form.setScript('customscript_inventoryclientvalidations');
				var len=tempOrdersArray.length/parseFloat(pagesizevalue);
				var options = select.getSelectOptions('to','contains');
				for(var k=1;k<=Math.ceil(len);k++)
				{

					var from;var to;

					to=parseFloat(k)*parseFloat(pagesizevalue);
					from=(parseFloat(to)-parseFloat(pagesizevalue))+1;

					if(parseFloat(to)>tempOrdersArray.length)
					{
						to=tempOrdersArray.length;
						test=from.toString()+","+to.toString(); 
					}

					var temp=from.toString()+" to "+to.toString();
					var tempto=from.toString()+","+to.toString();

					if(options.length == 0)
					{
						select.addSelectOption(tempto,temp);
					}
				} 
				if (request.getMethod() == 'POST'){

					if(request.getParameter('custpage_selectpage')!=null ){

						select.setDefaultValue(request.getParameter('custpage_selectpage'));	

					}
					if(request.getParameter('custpage_pagesize')!=null ){

						pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	

					}
				}
				else
				{

				}
			}
			else
			{
				pagesizevalue=25;
			}
			var minval=0;var maxval=parseFloat(pagesizevalue);
			if(parseFloat(pagesizevalue)>tempOrdersArray.length)
			{
				maxval=tempOrdersArray.length;
			}
			var selectno=request.getParameter('custpage_selectpage');
			if(selectno!=null )
			{
				var temp= request.getParameter('custpage_selectpage');
				var temparray=temp.split(',');
				//nlapiLogExecution('ERROR', 'temparray',temparray.length);

				var diff=parseFloat(temparray[1])-(parseFloat(temparray[0])-1);
				//nlapiLogExecution('ERROR', 'diff',diff);

				var pagevalue=request.getParameter('custpage_pagesize');					
				//nlapiLogExecution('ERROR', 'pagevalue',pagevalue);
				if(pagevalue!=null)
				{
					if(parseFloat(diff)==parseFloat(pagevalue)|| test==selectno)
					{
						var temparray=selectno.split(',');	
						//nlapiLogExecution('ERROR', 'temparray.length ', temparray.length);  
						minval=parseFloat(temparray[0])-1;
						//nlapiLogExecution('ERROR', 'temparray[0] ', temparray[0]);  
						maxval=parseFloat(temparray[1]);
						//nlapiLogExecution('ERROR', 'temparray[1] ', temparray[1]);  
					}
				}
			}
			//nlapiLogExecution('ERROR', 'invtsearchresults.length ', tempOrdersArray.length);
			var index=0;
			//nlapiLogExecution('ERROR', 'minval', minval);  

			//nlapiLogExecution('ERROR', 'maxval ', maxval); 

			if(maxval > tempOrdersArray.length)
				maxval=tempOrdersArray.length;

			var so,sovalue,cntlp,wgt,name,lineno,shipmethod,shipdate,customer = "";
			for (var s = minval; s < maxval; s++) 
			{
				try
				{
					var invtsearchresult = tempOrdersArray[s];					

					so=invtsearchresult.getText('custrecord_ebiz_order_no');
					sovalue=invtsearchresult.getValue('custrecord_ebiz_order_no');
					cntlp=invtsearchresult.getValue('custrecord_container_lp_no');
					wgt=parseFloat(invtsearchresult.getValue('custrecord_total_weight'));
					name=invtsearchresult.getValue('name');
					lineno=invtsearchresult.getValue('custrecord_line_no');
					shipmethod=invtsearchresult.getText('shipmethod','custrecord_ebiz_order_no');
					shipdate=invtsearchresult.getValue('shipdate','custrecord_ebiz_order_no');
					customer=invtsearchresult.getText('entity','custrecord_ebiz_order_no');
				
					for(var j=0 ;j<opentasksosearchresults.length;j++)
					{
						nlapiLogExecution('ERROR','j111',j);
						if((cntlp==opentasksosearchresults[j].getValue('custrecord_container_lp_no'))&& (s!=j))
						{
							nlapiLogExecution('ERROR','cntlp11111',cntlp);
							wgt=wgt+parseFloat(opentasksosearchresults[j].getValue('custrecord_total_weight'));
							nlapiLogExecution('ERROR','wgt1111',wgt);
						}
					}
					
							
					if(wgt == null || wgt =='' || wgt =='null' || wgt == 'undefined' || isNaN(wgt))
					{
						nlapiLogExecution('ERROR','Inside getting weight from pack task',wgt);
						var filtersPacktsk = new Array();
						filtersPacktsk.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', cntlp));
						filtersPacktsk.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['14']));//pack task
						var columnsPacktsk= new Array();
						columnsPacktsk[0]	= new nlobjSearchColumn('custrecord_total_weight');
						var searchresults1 = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersPacktsk, columnsPacktsk);
						if(searchresults1!=null&&searchresults1!='')
						{
							wgt=searchresults1[0].getValue('custrecord_total_weight');
						}
					}
					
					if(wgt == null || wgt =='' || wgt =='null' || wgt == 'undefined' || isNaN(wgt))
						wgt =0;
					nlapiLogExecution('ERROR','wgt before binding',wgt);
					form.getSubList('custpage_items').setLineItemValue('custpage_sono', index + 1, so);
					form.getSubList('custpage_items').setLineItemValue('custpage_sointernalid', index + 1, sovalue);
					form.getSubList('custpage_items').setLineItemValue('custpage_container', index + 1, cntlp);
					form.getSubList('custpage_items').setLineItemValue('custpage_weight', index + 1, parseFloat(wgt).toFixed(5));	
					form.getSubList('custpage_items').setLineItemValue('custpage_fo', index + 1, name);	
					form.getSubList('custpage_items').setLineItemValue('custpage_shippingmethod', index + 1, shipmethod);
					form.getSubList('custpage_items').setLineItemValue('custpage_shipdate', index + 1, shipdate);
					form.getSubList('custpage_items').setLineItemValue('custpage_customer', index + 1, customer);
					index=index+1;
				}
				catch(exp)
				{
					nlapiLogExecution('ERROR','Exception in Displaying Value',exp);
				}
			}
		}
		//Get the button
		var button = form.getButton('submitter');
		//Relabel the buttonâ€™s UI label
		button.setLabel('Submit');
	}
	else
	{
		form.getSubList('custpage_items').setLineItemValues(opentasksosearchresults);
		//Get the button
		var button = form.getButton('submitter');

		//Relabel the buttonâ€™s UI label
		button.setLabel('Display');
		msg.setDefaultValue("<div id='div__alert' align='center'><script>showAlertBox('div__alert', 'Error', 'Packing is not yet done for this SO/TO.', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
		response.writePage(form);	
		return;
	}
	return QuickShipFalseOrders;
}


function CreateShipManifestRecord(vebizOrdNo,vContLpNo,vCarrierType,vTrackingNo,vActualweight,shipcharges) {
	try 
	{
		if(vTrackingNo==null || vTrackingNo=='')
			vTrackingNo=vContLpNo;
		nlapiLogExecution('ERROR', 'into CreateShipManifestRecord','from inside');		
		nlapiLogExecution('ERROR', 'Order #',vebizOrdNo);	
		nlapiLogExecution('ERROR', 'Container LP #',vContLpNo);	
		nlapiLogExecution('ERROR', 'Carrier Type',vCarrierType);	
		nlapiLogExecution('ERROR', 'vTrackingNo',vTrackingNo);
		if (vebizOrdNo != null && vebizOrdNo != "") 
		{
			nlapiLogExecution('ERROR', 'test');
			if(IsContainerLpExist(vContLpNo)!='T')
			{
				nlapiLogExecution('ERROR', 'test');
				var freightterms ="";
				var otherrefnum="";

				var trantype = nlapiLookupField('transaction', vebizOrdNo, 'recordType');
				nlapiLogExecution('ERROR', 'trantype', trantype);
				//Get the records in SalesOrder
				nlapiLogExecution('ERROR', 'SalesOrderList','');
				var  searchresults;
				if(trantype=="salesorder")
				{
					searchresults =SalesOrderList(vebizOrdNo);
				}

				else if(trantype=="transferorder")
				{
					nlapiLogExecution('ERROR', 'SalesOrderList','');

					var filters = new Array();
					filters[0] = new nlobjSearchFilter('Internalid', null, 'is', vebizOrdNo);
					filters[1] = new nlobjSearchFilter('mainline', null, 'is', 'T');
					var columns = new Array();
					columns[0] = new nlobjSearchColumn('tranid');
					columns[1] = new nlobjSearchColumn('shipcarrier');
					columns[2] = new nlobjSearchColumn('shipaddress1');
					columns[3] = new nlobjSearchColumn('shipaddress2');
					columns[4] = new nlobjSearchColumn('shipcity');
					columns[5] = new nlobjSearchColumn('shipstate');
					columns[6] = new nlobjSearchColumn('shipcountry');
					columns[7] = new nlobjSearchColumn('shipzip');
					columns[8] = new nlobjSearchColumn('shipmethod');
					columns[9] = new nlobjSearchColumn('shipaddressee');
					columns[10] = new nlobjSearchColumn('custbody_salesorder_carrier');
					columns[11] = new nlobjSearchColumn('transferlocation');

					searchresults = nlapiSearchRecord('transferorder', null, filters, columns);
					nlapiLogExecution('ERROR', 'transferorder',searchresults.length);
				}
				else if(trantype=="vendorreturnauthorization")
				{
					var filters = new Array();
					filters[0] = new nlobjSearchFilter('Internalid', null, 'is', vebizOrdNo);
					filters[1] = new nlobjSearchFilter('mainline', null, 'is', 'T');
					var columns = new Array();
					columns[0] = new nlobjSearchColumn('tranid');
					columns[1] = new nlobjSearchColumn('shipcarrier');
					columns[2] = new nlobjSearchColumn('shipaddress1');
					columns[3] = new nlobjSearchColumn('shipaddress2');
					columns[4] = new nlobjSearchColumn('shipcity');
					columns[5] = new nlobjSearchColumn('shipstate');
					columns[6] = new nlobjSearchColumn('shipcountry');
					columns[7] = new nlobjSearchColumn('shipzip');
					columns[8] = new nlobjSearchColumn('shipmethod');
					columns[9] = new nlobjSearchColumn('shipaddressee');
					columns[10] = new nlobjSearchColumn('custbody_salesorder_carrier');
					columns[11] = new nlobjSearchColumn('transferlocation');

					searchresults = nlapiSearchRecord('vendorreturnauthorization', null, filters, columns);
					nlapiLogExecution('ERROR', 'transferorder',searchresults.length);
					if(vTrackingNo==null || vTrackingNo=='')
						vTrackingNo=vContLpNo;
				}
				nlapiLogExecution('ERROR', 'SalesOrderList',searchresults);
				//Get the records in customrecord_ebiznet_trn_opentask
				var opentaskordersearchresult=getOpenTaskDetails(vebizOrdNo,vContLpNo);
				nlapiLogExecution('ERROR', 'getOpenTaskDetails',opentaskordersearchresult);

				var ShipManifest = nlapiCreateRecord('customrecord_ship_manifest');
				ShipManifest.setFieldValue('custrecord_ship_orderno',searchresults[0].getValue('tranid'));
				ShipManifest.setFieldValue('custrecord_ship_carrier','QuickShip');
				ShipManifest.setFieldValue('custrecord_ship_city',   searchresults[0].getValue('shipcity'));
				ShipManifest.setFieldValue('custrecord_ship_state',	 searchresults[0].getValue('shipstate'));
				ShipManifest.setFieldValue('custrecord_ship_country',searchresults[0].getValue('shipcountry'));
				ShipManifest.setFieldValue('custrecord_ship_addr1',searchresults[0].getValue('shipaddress1'));
				ShipManifest.setFieldValue('custrecord_ship_trackno',vTrackingNo);
				if(vActualweight=='0' || vActualweight=='0.0')
					vActualweight='0.01';
				ShipManifest.setFieldValue('custrecord_ship_actwght',parseFloat(vActualweight).toFixed(5));
				ShipManifest.setFieldValue('name',vebizOrdNo);
				ShipManifest.setFieldValue('custrecord_ship_charges',shipcharges);


				//sales order specific code 
				if(trantype=="salesorder")
				{
					var contactName=searchresults[0].getText('entity');
					var entity=searchresults[0].getText('entity');
					if(contactName!=null && contactName!='')
						contactName=contactName.replace(","," ");

					if(entity!=null && entity!='')
						entity=entity.replace(","," ");

					ShipManifest.setFieldValue('custrecord_ship_contactname',contactName);					
					ShipManifest.setFieldValue('custrecord_ship_ordertype',searchresults[0].getText('custbody_nswmssoordertype'));
					freightterms=searchresults[0].getText('custbody_nswmsfreightterms');
					otherrefnum=searchresults[0].getValue('otherrefnum');			 
					ShipManifest.setFieldValue('custrecord_ship_paymethod',freightterms);
					ShipManifest.setFieldValue('custrecord_ship_phone',searchresults[0].getValue('custbody_customer_phone'));
					//ShipManifest.setFieldValue('custrecord_ship_zip',searchresults[0].getValue('shipzip'));
					var saturdaydelivery= searchresults[0].getValue('custbody_nswmssosaturdaydelivery');
					ShipManifest.setFieldValue('custrecord_ship_satflag',saturdaydelivery);
					var cashondelivery= searchresults[0].getValue('custbody_nswmscodflag');
					ShipManifest.setFieldValue('custrecord_ship_codflag',cashondelivery);
					//nlapiLogExecution('ERROR', 'searchresults[0].getValue(email)',searchresults[0].getValue('email'));
					//nlapiLogExecution('ERROR', 'searchresults[0].getTExt(email)',searchresults[0].getText('email'));
					ShipManifest.setFieldValue('custrecord_ship_email',searchresults[0].getValue('email'));

					var rec= nlapiLoadRecord('salesorder', vebizOrdNo);
					var zipvalue=rec.getFieldValue('shipzip');
					var servicelevelvalue=rec.getFieldText('shipmethod');
					var consignee=rec.getFieldValue('shipaddressee');
					var signaturerequired=rec.getFieldValue('custbody_nswmssignaturerequired');
					ShipManifest.setFieldValue('custrecord_ship_signature_req',signaturerequired);
					var shipcomplete=rec.getFieldValue('shipcomplete');
					var termscondition=rec.getFieldText('terms');
					nlapiLogExecution('ERROR', 'signaturerequired',shipcomplete);
					var shiptotal="0.00";
					if((shipcomplete=="T")&&(termscondition=="C.O.D."))
					{

						ShipManifest.setFieldValue('custrecord_ship_codflag','T');
						shiptotal=rec.getFieldValue('subtotal');
						ShipManifest.setFieldValue('custrecord_ship_codamount',shiptotal);
					}
					else
					{
						ShipManifest.setFieldValue('custrecord_ship_codflag','F');
						ShipManifest.setFieldValue('custrecord_ship_codamount',shiptotal);
					}
					nlapiLogExecution('ERROR', 'signaturerequired',signaturerequired);
					nlapiLogExecution('ERROR', 'zipvalue=',zipvalue);
					nlapiLogExecution('ERROR', 'servicelevelvalue=',servicelevelvalue);
					nlapiLogExecution('ERROR', 'Consignee=',consignee);

					if(consignee!="" || consignee!=null)
						ShipManifest.setFieldValue('custrecord_ship_consignee',consignee);
					else
						ShipManifest.setFieldValue('custrecord_ship_consignee',entity);


					ShipManifest.setFieldValue('custrecord_ship_servicelevel',servicelevelvalue);			
					ShipManifest.setFieldValue('custrecord_ship_zip',zipvalue);
					var companyname= searchresults[0].getText('custbody_nswms_company');
					ShipManifest.setFieldValue('custrecord_ship_company',companyname);
					ShipManifest.setFieldValue('custrecord_ship_ref2',otherrefnum);

				}

				var address1=searchresults[0].getValue('shipaddress1');
				var address2=searchresults[0].getValue('shipaddress2');
				var zip=searchresults[0].getValue('shipzip');
				var servicelevel=searchresults[0].getText('shipmethod');
				nlapiLogExecution('ERROR', 'Zip',zip);
				nlapiLogExecution('ERROR', 'Service Level',servicelevel);
				nlapiLogExecution('ERROR', 'Zip',zip);
				nlapiLogExecution('ERROR', 'City',searchresults[0].getValue('shipcity'));
				nlapiLogExecution('ERROR', 'State',searchresults[0].getValue('shipstate'));
				nlapiLogExecution('ERROR', 'Zip=',searchresults[0].getValue('shipzip'));
				nlapiLogExecution('ERROR', 'Service Level Value',searchresults[0].getText('shipmethod'));



				if(address1!=null && address1!='')
					address1=address1.replace(","," ");


				if(address2!=null && address2!='')
					address2=address2.replace(","," ");


				ShipManifest.setFieldValue('custrecord_ship_order',vebizOrdNo);
				ShipManifest.setFieldValue('custrecord_ship_custom5',"S");	
				//case #20127782
				ShipManifest.setFieldValue('custrecord_ship_void',"U");
				//case# 20149651 starts(ShipManifest is placed instead of ShipManifestRecord)
				//ShipManifestRecord.setFieldValue('custrecord_ship_trackno',vTrackingNo);
				ShipManifest.setFieldValue('custrecord_ship_trackno',vTrackingNo);
				//ShipManifestRecord.setFieldValue('custrecord_ship_actwght',vActualweight);
				//ShipManifestRecord.setFieldValue('custrecord_ship_masttrackno',vTrackingNo);
				ShipManifest.setFieldValue('custrecord_ship_masttrackno',vTrackingNo);
				//case# 20149651 ends
				//Cse# 20127782

				ShipManifest.setFieldValue('custrecord_ship_contactname',contactName);					
				ShipManifest.setFieldValue('custrecord_ship_ordertype',searchresults[0].getText('custbody_nswmssoordertype'));
				freightterms=searchresults[0].getText('custbody_nswmsfreightterms');
				otherrefnum=searchresults[0].getValue('otherrefnum');			 
				ShipManifest.setFieldValue('custrecord_ship_paymethod',freightterms);

				ShipManifest.setFieldValue('custrecord_ship_addr1',address1);
				ShipManifest.setFieldValue('custrecord_ship_addr2',address2);

				//ShipManifest.setFieldValue('custrecord_ship_servicelevel',searchresults[0].getText('custbody_nswmssoservicelevel'));
				//ShipManifest.setFieldValue('custrecord_ship_servicelevel',searchresults[0].getText('shipmethod'));
				//shipmethod 


				/*
				var servicelevelList=GetSerViceLevel(vCarrierType);
				if((servicelevelList!=null)&&(servicelevelList !='')&&(servicelevelList.length>0))
				{
					vserlevel=servicelevelList[0].getValue('custrecord_carrier_service_level'); 
					nlapiLogExecution('ERROR', 'vserlevel', vserlevel);
					ShipManifest.setFieldValue('custrecord_ship_servicelevel',vserlevel);
				}
				 */

				if(trantype=="transferorder")
				{
					var tolocation = searchresults[0].getValue('transferlocation');
					nlapiLogExecution('ERROR', 'tolocation', tolocation);

					var record = nlapiLoadRecord('location', tolocation);

					var shipfromaddress1=record.getFieldValue('addr1');
					var shipfromaddress2=record.getFieldValue('addr2');
					var shipfromcity=record.getFieldValue('city');
					var shipfromstate=record.getFieldValue('state');
					var shipfromzipcode =record.getFieldValue('zip');
					var shipfromcompanyname=record.getFieldValue('addressee');
					//var shipfromphone=record.getFieldValue('addrphone');
					var shipfromcountry =record.getFieldValue('country');

					ShipManifest.setFieldValue('custrecord_ship_carrier',searchresults[0].getText('custbody_salesorder_carrier'));
					ShipManifest.setFieldValue('custrecord_ship_city',   shipfromcity);
					ShipManifest.setFieldValue('custrecord_ship_state',	 shipfromstate);
					ShipManifest.setFieldValue('custrecord_ship_country',shipfromcountry);
					ShipManifest.setFieldValue('custrecord_ship_addr1',shipfromaddress1);
					ShipManifest.setFieldValue('custrecord_ship_zip',shipfromzipcode);
					ShipManifest.setFieldValue('custrecord_ship_addr2',shipfromaddress2);

				}


				if (opentaskordersearchresult != null && opentaskordersearchresult != "")
				{
					nlapiLogExecution('ERROR', 'inside opentask search results', opentaskordersearchresult);

					var oldcontainer="";
					for (l = 0; l < opentaskordersearchresult.length; l++) 
					{ 
						nlapiLogExecution('ERROR', 'inside opentask', containerid);

						var custlenght="";	
						var custheight="";
						var custwidht="";

						var sku="";
						var ebizskuno="";
						var uomlevel="";
						var containerlpno = opentaskordersearchresult[l].getValue('custrecord_container_lp_no');
						sku = opentaskordersearchresult[l].getText('custrecord_sku');
						ebizskuno = opentaskordersearchresult[l].getValue('custrecord_sku');
						uomlevel = opentaskordersearchresult[l].getValue('custrecord_uom_level');
						var FOName = opentaskordersearchresult[l].getValue('name');
						var vwaveno = opentaskordersearchresult[l].getValue('custrecord_ebiz_wave_no');
						nlapiLogExecution('ERROR', 'containerlpno', containerlpno);
						nlapiLogExecution('ERROR', 'oldcontainer', oldcontainer);
						ShipManifest.setFieldValue('custrecord_ship_waveno',vwaveno);
						if(oldcontainer!=containerlpno){
							ShipManifest.setFieldValue('custrecord_ship_contlp',containerlpno);
							ShipManifest.setFieldValue('custrecord_ship_ref5',containerlpno);
							ShipManifest.setFieldValue('custrecord_ship_ref1',sku);
							ShipManifest.setFieldValue('custrecord_ship_ref3',FOName);
							var containerid= opentaskordersearchresult[l].getValue('custrecord_container');
							var containername= opentaskordersearchresult[l].getText('custrecord_container');
							nlapiLogExecution('ERROR', 'container id', containerid);
							nlapiLogExecution('ERROR', 'container name', containername);
							nlapiLogExecution('ERROR', 'FOName name', FOName);
							if(containerid!="")
							{
								if(containername!="SHIPASIS")
								{
									//Lenght, Height,Width fields  in customrecord_ebiznet_container
									var containersearchresults = getContainerDims(containerid);
									if(containersearchresults != null)
									{
										custlenght=containersearchresults [0].getValue('custrecord_length');
										custwidht=containersearchresults [0].getValue('custrecord_widthcontainer');
										custheight=containersearchresults [0].getValue('custrecord_heightcontainer');								
										ShipManifest.setFieldValue('custrecord_ship_length',parseFloat(custlenght).toFixed(5));	
										ShipManifest.setFieldValue('custrecord_ship_width',parseFloat(custwidht).toFixed(5));
										ShipManifest.setFieldValue('custrecord_ship_height',parseFloat(custheight).toFixed(5));
										ShipManifest.setFieldValue('custrecord_ship_ref2',otherrefnum);
									} 
								}
								else
								{
									var containersearchresults = getSKUDims(ebizskuno,uomlevel);
									if(containersearchresults != null)
									{
										custlenght=containersearchresults [0].getValue('custrecord_ebizlength');
										custwidht=containersearchresults [0].getValue('custrecord_ebizwidth');
										custheight=containersearchresults [0].getValue('custrecord_ebizheight');								
										ShipManifest.setFieldValue('custrecord_ship_length',parseFloat(custlenght).toFixed(5));	
										ShipManifest.setFieldValue('custrecord_ship_width',parseFloat(custwidht).toFixed(5));
										ShipManifest.setFieldValue('custrecord_ship_height',parseFloat(custheight).toFixed(5));
										ShipManifest.setFieldValue('custrecord_ship_ref2',otherrefnum);
									} 
								}

							}	

							if (containerlpno != null && containerlpno != "") {
								var PackageWeight = getTotalWeight(containerlpno);
								nlapiLogExecution('ERROR', 'PackageWeight', PackageWeight);
								if(PackageWeight=='0')
								{
									PackageWeight='0.01';
								}
								ShipManifest.setFieldValue('custrecord_ship_pkgwght',parseFloat(PackageWeight).toFixed(5));
							}

							oldcontainer = containerlpno;


							var	ServiceLevelId=searchresults[0].getText('shipmethod');
							nlapiLogExecution('ERROR', 'ServiceLevelId',ServiceLevelId);
							if(ServiceLevelId!=null && ServiceLevelId!="")
							{
								servicelevellbyshipmethod=GetSerViceLevelByShipmethod(ServiceLevelId);
							}
							else
							{

								var foorder= opentaskordersearchresult[0].getValue('name');
								nlapiLogExecution('Debug', 'linelevelFoOrder',foorder);
								var site=opentaskordersearchresult[0].getValue('custrecord_wms_location');
								nlapiLogExecution('Debug', 'linelevelsite',site);
								var ServiceLevelId=getlinelevelshipmethod(foorder,site);
								servicelevellbyshipmethod=GetSerViceLevelByShipmethod(ServiceLevelId);
							}

							if((servicelevellbyshipmethod!=null)&&(servicelevellbyshipmethod !='')&&(servicelevellbyshipmethod.length>0))
							{
								nlapiLogExecution('Debug', 'servicelevellbyshipmethod', 'servicelevellbyshipmethod');

								var shipserviceLevel=servicelevellbyshipmethod[0].getValue('custrecord_carrier_service_level'); 
								var wmscarriertype=servicelevellbyshipmethod[0].getValue('custrecord_carrier_id'); 
								nlapiLogExecution('Debug', 'shipmethodshipserviceLevel',shipserviceLevel);
								nlapiLogExecution('Debug', 'shipmethodshipserviceLevel',wmscarriertype);
								ShipManifest.setFieldValue('custrecord_ship_servicelevel',shipserviceLevel);
								ShipManifest.setFieldValue('custrecord_ship_carrier',wmscarriertype);

							}


							var id=	nlapiSubmitRecord(ShipManifest, false, true);	
							nlapiLogExecution('ERROR', 'id', id);
							nlapiLogExecution('ERROR', 'unexpected error', 'I am success1');

						}					
					}
				}
				else
				{

					nlapiLogExecution('ERROR', 'unexpected error', 'I am success2');
					var id=nlapiSubmitRecord(ShipManifest, false, true);
					nlapiLogExecution('ERROR', 'id', id);
				}
			}

			else
			{
				var filter = new Array();//
				filter.push(new nlobjSearchFilter('custrecord_ship_contlp',null,'is',vContLpNo));
				filter.push(new nlobjSearchFilter('custrecord_ship_order',null,'anyof',vebizOrdNo));
				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_ship_orderno');
				columns[1] = new nlobjSearchColumn('custrecord_ship_void');
				var manifestList= nlapiSearchRecord('customrecord_ship_manifest',null,filter,columns);
				if(manifestList!=null && manifestList!='' && manifestList.length>0)
				{
					nlapiLogExecution('ERROR', 'vActualweight', vActualweight);
					if((vActualweight==null)||(vActualweight==''))
					{
						vActualweight="0.10";
					}
					if(vActualweight=="0.0")
					{
						vActualweight="0.10";
					}
					var voidflagValue=manifestList[0].getValue('custrecord_ship_void');
					var ShipManifestRecord=nlapiLoadRecord('customrecord_ship_manifest',manifestList[0].getId());
					nlapiLogExecution('ERROR', 'voidflagValue', voidflagValue);
					if(voidflagValue=="N")
					{
						nlapiLogExecution('ERROR', 'FlagvoidflagValue', vTrackingNo);
						ShipManifestRecord.setFieldValue('custrecord_ship_actwght',vActualweight);

						ShipManifestRecord.setFieldValue('custrecord_ship_masttrackno',vTrackingNo);
					}
					ShipManifestRecord.setFieldValue('custrecord_ship_trackno',vTrackingNo);
					ShipManifestRecord.setFieldValue('custrecord_ship_charges',shipcharges);
					ShipManifestRecord.setFieldValue('custrecord_ship_void',"U");
					var id=nlapiSubmitRecord(ShipManifestRecord, false, true);
					nlapiLogExecution('ERROR', 'id', id);
				}

			}	
		}
	}
	catch (e) {

		//InsertExceptionLog('General Functions',2, 'Create Shipping Manifest', e, vebizOrdNo, vContLpNo,vCarrierType,'','', nlapiGetUser());
		if (e instanceof nlobjError)
			nlapiLogExecution('ERROR', 'system error', e.getCode() + '\n'
					+ e.getDetails());
		//lapiLogExecution('ERROR','SYS ERROR','Hd')
		else
			nlapiLogExecution('ERROR', 'unexpected error', e.toString());
		nlapiLogExecution('ERROR', 'unexpected error', 'I am unsuccess3');
	}

	nlapiLogExecution('ERROR', 'Out of CreateShippingManifestRecord','');		
}
function createform(form)
{	
	form.setScript('customscript_ebiz_quickship_qb_cl');
	//var soField = form.addField('custpage_qbso', 'text', 'Sales/Transfer Order#');
	//var vSo = request.getParameter('custpage_qbso');
	//soField.setMandatory(true);
	//soField.setDefaultValue(vSo);
	//var SOMultiField = form.addField('custpage_somulti', 'multiselect', 'Sales/Transfer Order#','transaction');



	var SOMultiField = form.addField('custpage_somulti', 'text', 'Sales/Transfer Order#');
	if(request.getParameter('custpage_somulti')!='' && request.getParameter('custpage_somulti')!=null)
	{
		SOMultiField.setDefaultValue(request.getParameter('custpage_somulti'));	
	}
	var contlpField = form.addField('custpage_qbcontlp', 'text', 'Container LP#');
	if(request.getParameter('custpage_qbcontlp')!='' && request.getParameter('custpage_qbcontlp')!=null)
	{
		contlpField.setDefaultValue(request.getParameter('custpage_qbcontlp'));	
	}
	var waveField = form.addField('custpage_qbwave', 'text', 'Wave#');
	if(request.getParameter('custpage_qbwave')!='' && request.getParameter('custpage_qbwave')!=null)
	{
		waveField.setDefaultValue(request.getParameter('custpage_qbwave'));	
	}
	var clusterField = form.addField('custpage_qbcluster', 'text', 'Cluster#');
	if(request.getParameter('custpage_qbcluster')!='' && request.getParameter('custpage_qbcluster')!=null)
	{
		clusterField.setDefaultValue(request.getParameter('custpage_qbcluster'));	
	}
	// case# 201414016,201414017
	var OrderType = form.addField('custpage_ordertype', 'select', 'Transaction Type').setMandatory(true);
	OrderType.setDefaultValue("SO");
	OrderType.addSelectOption("salesorder","SO");
	OrderType.addSelectOption("transferorder","TO");
	OrderType.addSelectOption("vendorreturnauthorization","VRA");
	if(request.getParameter('custpage_ordertype')!='' && request.getParameter('custpage_ordertype')!=null)
	{
		OrderType.setDefaultValue(request.getParameter('custpage_ordertype'));	
	}

	var vContLP = request.getParameter('custpage_qbcontlp');
	//soField.setMandatory(true);
	contlpField.setDefaultValue(vContLP);
	var StatusField = form.addField('custpage_qbstatus', 'select', 'Status');
	StatusField.addSelectOption('', '');
	// Pending Fulfillment / Partially Fulfilled / Pending Billing_Partially Fulfilled
	StatusField.addSelectOption('SalesOrd:B','Pending Fulfillment');
	StatusField.addSelectOption('SalesOrd:D','Partially Fulfilled');
	StatusField.addSelectOption('SalesOrd:E','Pending Billing/Partially Fulfilled');
	if(request.getParameter('custpage_qbstatus')!='' && request.getParameter('custpage_qbstatus')!=null)
	{
		StatusField.setDefaultValue(request.getParameter('custpage_qbstatus'));	
	}

	var ShippingMethodField = form.addField('custpage_qbcarriershipmethod', 'select', 'Shipping Method');
	var ShipmethodFilter = new Array();
	var vRoleLocation=getRoledBasedLocation();
	nlapiLogExecution('ERROR', 'vRoleLocation',vRoleLocation );
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		ShipmethodFilter.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof', vRoleLocation));

	}
	
	ShipmethodFilter.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [13,11,8]));
	ShipmethodFilter.push(new nlobjSearchFilter('custrecord_do_carrier', null, 'isnotempty'));
	var column=new Array();
	column[0]=new nlobjSearchColumn('custrecord_do_carrier',null,'group');

	var selectshipmd= nlapiSearchRecord('customrecord_ebiznet_ordline',null,ShipmethodFilter,column);
	ShippingMethodField.addSelectOption("","");
	for(var i=0;  selectshipmd != null && i<selectshipmd.length;i++)
	{
		nlapiLogExecution('ERROR', selectshipmd[i].getValue('custrecord_do_carrier',null,'group') + ',' + selectshipmd[i].getText('custrecord_do_carrier',null,'group') );
		ShippingMethodField.addSelectOption(selectshipmd[i].getValue('custrecord_do_carrier',null,'group'),selectshipmd[i].getText('custrecord_do_carrier',null,'group'));
	}
	/*var record = nlapiCreateRecord('salesorder', {recordmode: 'dynamic'});

	var methodsField = record.getField('shipmethod');
	var methodOptions = methodsField.getSelectOptions(null, null);
	nlapiLogExecution('ERROR','methodOptions',methodOptions);

	ShippingMethodField.addSelectOption("",""); 

	for (var i = 0; methodOptions != null && i < methodOptions.length; i++) {
		nlapiLogExecution('ERROR', methodOptions[i].getId() + ',' + methodOptions[i].getText() );
		ShippingMethodField.addSelectOption(methodOptions[i].getId(),methodOptions[i].getText());
	}	
*/
	if(request.getParameter('custpage_qbcarriershipmethod')!='' && request.getParameter('custpage_qbcarriershipmethod')!=null)
	{
		ShippingMethodField.setDefaultValue(request.getParameter('custpage_qbcarriershipmethod'));	
	}

	var FromDateField = form.addField('custpage_qbfromdate', 'date', 'From Date');
	var ToDateField = form.addField('custpage_qbtodate', 'date', 'To Date');
	if(request.getParameter('custpage_qbfromdate')!='' && request.getParameter('custpage_qbfromdate')!=null)
	{
		FromDateField.setDefaultValue(request.getParameter('custpage_qbfromdate'));	
	}
	if(request.getParameter('custpage_qbtodate')!='' && request.getParameter('custpage_qbtodate')!=null)
	{
		ToDateField.setDefaultValue(request.getParameter('custpage_qbtodate'));	
	}

	var ShipDateField = form.addField('custpage_qbshipdate', 'date', 'Ship Date');
	if(request.getParameter('custpage_qbshipdate')!='' && request.getParameter('custpage_qbshipdate')!=null)
	{
		ShipDateField.setDefaultValue(request.getParameter('custpage_qbshipdate'));	
	}

	var CustomerField = form.addField('custpage_qbcustomer', 'select', 'Customer','customer');
	if(request.getParameter('custpage_qbcustomer')!='' && request.getParameter('custpage_qbcustomer')!=null)
	{
		CustomerField.setDefaultValue(request.getParameter('custpage_qbcustomer'));	
	}

	var hiddenfieldselectpage=form.addField('custpage_hiddenfieldselectpage', 'text', 'hiddenflag').setDisplayType('hidden');
	hiddenfieldselectpage.setDefaultValue('F');

//	Case 20125912 start submitting ppage on selectibg drpdownvalue
	var hiddenfieldselectpagee = form.addField('custpage_hiddenfield', 'text', '').setDisplayType('hidden');
	hiddenfieldselectpagee.setDefaultValue('F');
	form.setScript('customscript_inventoryclientvalidations');
	//Case 20125912 end

	var sublist = form.addSubList("custpage_items", "list", "ItemList");
	sublist.addMarkAllButtons();
	sublist.addField("custpage_select", "checkbox", "Select").setDefaultValue('F');
	sublist.addField("custpage_sono", "text", "Sales Ord #").setDisplayType('inline');
	sublist.addField("custpage_sointernalid", "text", "Sales Order Internal Id").setDisplayType('hidden');
	sublist.addField("custpage_container", "text", "Container LP").setDisplayType('inline');
	var actweight=sublist.addField("custpage_weight", "text", "Weight").setDisplayType('entry');
	actweight.setMandatory(true);
	var shipcharges=sublist.addField("custpage_shipcharges", "text", "ShipCharges").setDisplayType('entry').setDefaultValue("0");
	//sublist.addField("custpage_weight", "text", "Weight").setDisplayType('inline');
	sublist.addField("custpage_fo", "text", "Fulfillment order #").setDisplayType('inline');
	sublist.addField("custpage_shippingmethod", "text", "Shipping Method").setDisplayType('inline');
	sublist.addField("custpage_shipdate", "text", "Ship Date").setDisplayType('inline');
	sublist.addField("custpage_customer", "text", "Customer").setDisplayType('inline');
	var trackinno=	sublist.addField("custpage_trackingno", "text", "Tracking Number").setDisplayType('entry');
	//trackinno.setMandatory(true);
}

function IsContainerLpExist(vContLpNo)
{
	nlapiLogExecution('ERROR', 'Into IsContLpExist',vContLpNo);	
	var IsContLpExist='F';

	try
	{
		var filter = new Array();
		filter.push(new nlobjSearchFilter('custrecord_ship_contlp',null,'is',vContLpNo));
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ship_orderno');
		var manifestList= nlapiSearchRecord('customrecord_ship_manifest',null,filter,columns);
		if(manifestList!=null && manifestList.length>0)
			IsContLpExist='T';		
	}
	catch(e)
	{
		nlapiLogExecution('ERROR', 'unexpected error in IsContLpExist');	
	}
	nlapiLogExecution('ERROR', 'Out of IsContLpExist',IsContLpExist);	
	return IsContLpExist;
}



function IsQuickShipAllowedforFO(SOId,vSoWave)
{

	var searchrec='F';
	var filter=new Array();
	filter.push(new nlobjSearchFilter('custrecord_ns_ord', null, 'anyof', SOId));	
	filter.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [9]));	//Status - Pick Generated
	if(vSoWave!=null && vSoWave!='')
		filter.push(new nlobjSearchFilter('custrecord_ebiz_wave', null, 'is', parseInt(vSoWave)));	//Status - Pick Generated

	var columns=new Array();
	columns.push(new nlobjSearchColumn('custrecord_ns_ord'));
	columns.push(new nlobjSearchColumn('custrecord_ordline'));
	columns.push(new nlobjSearchColumn('custrecord_do_carrier'));
	columns.push(new nlobjSearchColumn('custrecord_do_wmscarrier'));	

	searchrec=nlapiSearchRecord('customrecord_ebiznet_ordline',null,filter,columns);
	if(searchrec!=null && searchrec!='')
	{
		searchrec='T';
	}
	else
	{
		searchrec='F';
	}
	nlapiLogExecution('ERROR', 'searchrec',searchrec);	

	return searchrec;
}


function IsQuickShipAllowedforopentask(SOId,vSoWave,vContLP)
{

	nlapiLogExecution('ERROR', 'Into IsQuickShipAllowedforopentask SOId',SOId);
	nlapiLogExecution('ERROR', 'vSoWave',vSoWave);
	nlapiLogExecution('ERROR', 'vContLP',vContLP);
	
	var searchrec='F';
	var filters=new Array();
//	filter.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SOId));	
//	filter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9,8]));	//Status - Pick Generated
//	if(vSoWave!=null && vSoWave!='')
//	filter.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(vSoWave)));	//Status - Pick Generated


	filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'is',SOId));
	if(vContLP!=null && vContLP!='')
		filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'is',vContLP));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'noneof', [26,30,29]));	
	filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null,'isempty'));
	filters.push(new nlobjSearchFilter('custrecord_act_qty', null,'greaterthan',0));

	var columns=new Array();
	columns.push(new nlobjSearchColumn('custrecord_container_lp_no'));
	columns.push(new nlobjSearchColumn('name'));


	searchrec=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,columns);
	if(searchrec!=null && searchrec!='')
	{
		searchrec='T';
	}
	else
	{
		searchrec='F';
	}
	
	nlapiLogExecution('ERROR', 'Into IsQuickShipAllowedforopentask searchrec',searchrec);

	return searchrec;
}

function IsQuickShipAllowed(SOId)
{
	var searchrec=new Array;
	var Orderdetails = new Array();
	var filter=new Array();
	filter.push(new nlobjSearchFilter('custrecord_ns_ord', null, 'anyof', SOId));
	//The below line is commented by Satish.N on 01/31/2014. 
	//No need to check with status as we are using the below results to check the quickship flag for a carrier only.
	//filter.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [8,11,13]));	//Status - Pick Confirmed

	var columns=new Array();
	columns.push(new nlobjSearchColumn('custrecord_ns_ord'));
	columns.push(new nlobjSearchColumn('custrecord_ordline'));
	columns.push(new nlobjSearchColumn('custrecord_do_carrier'));
	columns.push(new nlobjSearchColumn('custrecord_do_wmscarrier'));	

	searchrec=nlapiSearchRecord('customrecord_ebiznet_ordline',null,filter,columns);

	if(searchrec!=null && searchrec!='' && searchrec.length>0)
	{
		for (l = 0; l < searchrec.length; l++) 
		{ 
			var ordno=searchrec[l].getValue('custrecord_ns_ord');
			var orderno=searchrec[l].getText('custrecord_ns_ord');
			var lineno=searchrec[l].getValue('custrecord_ordline');
			var wmscarrier=searchrec[l].getValue('custrecord_do_wmscarrier');
			var shipmethod=searchrec[l].getValue('custrecord_do_carrier');
			var quickshipflag='F';

			nlapiLogExecution('ERROR', 'ordno', ordno);
			nlapiLogExecution('ERROR', 'orderno', orderno);

			var str = 'Order No. = ' + orderno + '<br>';
			str = str + 'Line No. = ' + lineno + '<br>';
			str = str + 'WMS Carrier. = ' + wmscarrier + '<br>';
			str = str + 'Ship Method. = ' + shipmethod + '<br>';

			nlapiLogExecution('ERROR', 'Log for Iteration', str);			

			if(wmscarrier!=null && wmscarrier!='')
				quickshipflag=GetQuickShipFlagbyCarrier(wmscarrier);
			else
				if(shipmethod!=null && shipmethod!='')
					quickshipflag=GetQuickShipFlagbyShipmethod(shipmethod);

			nlapiLogExecution('ERROR', 'Allow Quick Ship', quickshipflag);

			if(quickshipflag=='T')
			{	
				nlapiLogExecution('ERROR', 'ordno', ordno);
				nlapiLogExecution('ERROR', 'lineno', lineno);
				Orderdetails.push(ordno+'-'+lineno+'-'+quickshipflag);
			}
			else
			{
				Orderdetails.push(ordno+'-'+lineno+'-'+quickshipflag);
			}
		}
	}
	nlapiLogExecution('ERROR', 'Orderdetails', Orderdetails);
	return Orderdetails;
}

function GetQuickShipFlagbyShipmethod(shipmethod)
{
	nlapiLogExecution('ERROR', 'Into GetQuickShipFlagbyShipmethod (Input)', shipmethod);

	var quickship='F';
	var filter=new Array();
	filter.push(new nlobjSearchFilter('custrecord_carrier_nsmethod',null,'is',shipmethod));
	var columns=new Array();
	columns.push(new nlobjSearchColumn('custrecord_carrier_allow_quickship'));
	var carrierrec=nlapiSearchRecord('customrecord_ebiznet_carrier',null,filter,columns);
	if(carrierrec!=null && carrierrec!='' && carrierrec.length>0)
	{
		quickship=carrierrec[0].getValue('custrecord_carrier_allow_quickship');
	}
	else
	{
		quickship='T';
	}

	nlapiLogExecution('ERROR', 'Out of GetQuickShipFlagbyShipmethod (Output)', quickship);
	return quickship;
}

function GetQuickShipFlagbyCarrier(wmscarrier)
{
	nlapiLogExecution('ERROR', 'Into GetQuickShipFlagbyCarrier (Input)', wmscarrier);

	var quickship='F';
	var filter=new Array();
	filter.push(new nlobjSearchFilter('internalid',null,'is',wmscarrier));
	var columns=new Array();
	columns.push(new nlobjSearchColumn('custrecord_carrier_allow_quickship'));
	var carrierrec=nlapiSearchRecord('customrecord_ebiznet_carrier',null,filter,columns);
	if(carrierrec!=null && carrierrec!='' && carrierrec.length>0)
	{
		quickship=carrierrec[0].getValue('custrecord_carrier_allow_quickship');
	}

	nlapiLogExecution('ERROR', 'Out of GetQuickShipFlagbyCarrier (Output)', quickship);
	return quickship;
}

function GetSOInternalId(SOText,ordertype)
{
	nlapiLogExecution('ERROR','Into GetSOInternalId (Input)',SOText);
	nlapiLogExecution('ERROR','ordertype',ordertype);

	var ActualSoID='-1';

	var filter=new Array();
	filter.push(new nlobjSearchFilter('tranid',null,'is',SOText.toString()));
	filter.push(new nlobjSearchFilter('mainline',null,'is','F'));

	var columns=new Array();
	columns.push(new nlobjSearchColumn('shipmethod'));

	var searchrec=nlapiSearchRecord(ordertype,null,filter,columns);
	/*if(searchrec==null)
	{
		searchrec=nlapiSearchRecord('transferorder',null,filter,columns);
	}*/
	if(searchrec!=null && searchrec!='' && searchrec.length>0)
	{
		ActualSoID=searchrec[0].getId();
	}

	nlapiLogExecution('ERROR','Out of GetSOInternalId (Output)',ActualSoID);

	return ActualSoID;
}

function GetSOIntIdByContLP(vContLP,vSoWave,vSoCluster,vSOStatus,vFromDate,vToDate,vShipDate,vCustomer,vCarrier)
{
	var SoId;

	var columnsPicktsk = new Array();
	columnsPicktsk[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columnsPicktsk[1] = new nlobjSearchColumn('custrecord_container_lp_no');
	columnsPicktsk[2] = new nlobjSearchColumn('custrecord_total_weight');
	columnsPicktsk[3] = new nlobjSearchColumn('name');
	columnsPicktsk[4] = new nlobjSearchColumn('custrecord_line_no');
	columnsPicktsk[5] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columnsPicktsk[0].setSort();

	var filtersPicktsk = new Array();
	nlapiLogExecution('ERROR','vContLP',vContLP);
	nlapiLogExecution('ERROR','vSoWave',vSoWave);
	nlapiLogExecution('ERROR','vSoCluster',vSoCluster);
	nlapiLogExecution('ERROR','vSOStatus',vSOStatus);
	nlapiLogExecution('ERROR','vFromDate',vFromDate);
	nlapiLogExecution('ERROR','vToDate',vToDate);
	nlapiLogExecution('ERROR','vShipDate',vShipDate);
	nlapiLogExecution('ERROR','vCustomer',vCustomer);
	nlapiLogExecution('ERROR','vCarrier',vCarrier);

	if(vContLP!=null && vContLP!='')
		filtersPicktsk.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vContLP));
	if(vSoWave!=null && vSoWave!='')
		filtersPicktsk.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(vSoWave)));
	if(vSoCluster!=null && vSoCluster!='')
		filtersPicktsk.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vSoCluster));
	if(vSOStatus!=null && vSOStatus!='')
		filtersPicktsk.push(new nlobjSearchFilter('status', 'custrecord_ebiz_order_no', 'anyof', vSOStatus));
	if((vFromDate!=null && vFromDate!='') && (vToDate!=null && vToDate!=''))
		filtersPicktsk.push(new nlobjSearchFilter('trandate', 'custrecord_ebiz_order_no', 'within', vFromDate, vToDate));

	if(vShipDate!=null && vShipDate!='')
		filtersPicktsk.push(new nlobjSearchFilter('shipdate', 'custrecord_ebiz_order_no', 'on', vShipDate));
	if(vCustomer!=null && vCustomer!='')
		filtersPicktsk.push(new nlobjSearchFilter('entity', 'custrecord_ebiz_order_no', 'is', vCustomer));
	if(vCarrier!=null && vCarrier!='')
		filtersPicktsk.push(new nlobjSearchFilter('shipmethod', 'custrecord_ebiz_order_no', 'is', vCarrier));

	filtersPicktsk.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [28]));//status.outbound.packcomplete
	filtersPicktsk.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));//pick task
	filtersPicktsk.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',  null,'isnotempty'));

	var opentasksosearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersPicktsk, columnsPicktsk);
	if(opentasksosearchresults != null && opentasksosearchresults != '' && opentasksosearchresults.length>0)
	{
		return opentasksosearchresults;
	}	 
	else
		return null; 	
}
//Case # 20148099 starts
function getOrdStatus(OrdInternalid,OrdType)
{
	var filter=new Array();
	filter.push(new nlobjSearchFilter('internalid',null,'is', OrdInternalid));
	filter.push(new nlobjSearchFilter('mainline',null,'is','T'));
	var columns=new Array();	 
	columns.push(new nlobjSearchColumn('status'));
	var searchrec=nlapiSearchRecord(OrdType,null,filter,columns);
	return searchrec;
}
