/***************************************************************************
 eBizNET Solutions Inc               
 ****************************************************************************
/* 
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Client/ebiz_PickConfirmSerial_CL.js,v $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * 
 * PRAMETERS
 *
 * DESCRIPTION
 * 
 * REVISION HISTORY
 * $Log: ebiz_PickConfirmSerial_CL.js,v $
 * Revision 1.2.10.16.2.1  2015/12/04 08:40:29  aanchal
 * 2015.2 Issue fix
 * 201415444
 *
 * Revision 1.2.10.16  2014/09/08 16:19:33  sponnaganti
 * case# 20147957
 * Stnd Bundle issue fix
 *
 * Revision 1.2.10.15  2014/04/11 16:02:15  skreddy
 * case # 20140024
 * Dealmed sb  issue fix
 *
 * Revision 1.2.10.14  2014/04/09 11:01:14  nneelam
 * case#  20141267
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.2.10.13  2014/03/21 06:15:37  grao
 * Case# 20127178 related issue fixes in Sonic  electrox issue fixes
 *
 * Revision 1.2.10.12  2014/02/14 10:43:55  grao
 * Case# 20127178 related issue fixes in Sonic  electrox issue fixes
 *
 * Revision 1.2.10.11  2014/01/09 15:00:32  rmukkera
 * Case # 20126486
 *
 * Revision 1.2.10.10  2014/01/07 15:31:33  rmukkera
 * Case # 20126486
 *
 * Revision 1.2.10.9  2013/10/10 15:44:03  rmukkera
 * Case# 20124319�
 *
 * Revision 1.2.10.8  2013/09/16 14:26:14  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to serial out while pick confirmation.
 *
 * Revision 1.2.10.7  2013/09/11 15:23:51  rmukkera
 * Case# 20124376
 *
 * Revision 1.2.10.6  2013/09/03 15:43:01  skreddy
 * Case# 20124196,20124197
 * standard bundle issue fix
 *
 * Revision 1.2.10.5  2013/08/23 15:31:55  grao
 * SB Issue Fixes  20124019,20124020
 *
 * Revision 1.2.10.4  2013/05/20 15:21:28  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.2.10.3  2013/04/19 15:42:00  skreddy
 * CASE201112/CR201113/LOG201121
 * issue fixes
 *
 * Revision 1.2.10.2  2013/04/03 03:14:29  kavitha
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.2.10.1  2013/04/01 20:59:31  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * 
 *
 *****************************************************************************/
/**********************************************
 * This Suitelet page is used to filter SO line
 * .
 *
 * @author 
 * @version
 * @date
 *
 */
function SerialNoSelectSuitelet(request, response){
	if (request.getMethod() == 'GET') {

		var form = nlapiCreateForm('Serial Number Selection');
		var vQbSKU="";
		var vQbLPNo="";
		var vQbQty="";	
		var vopentask="";
		var vcontlp="";
		var vdointernid="";
		var vdoLineno="";	
		var vdoName="";	
		var voldLP="";
		var voldSerials="";
		if(request.getParameter('custparam_serialskuid')!=null && request.getParameter('custparam_serialskuid')!="")
		{
			vQbSKU = request.getParameter('custparam_serialskuid');			

		}        

		if(request.getParameter('custparam_serialskulp')!=null && request.getParameter('custparam_serialskulp')!="")
		{
			vQbLPNo=request.getParameter('custparam_serialskulp');
		}
		if(request.getParameter('custparam_serialskuchknqty')!=null && request.getParameter('custparam_serialskuchknqty')!="")
		{
			vQbQty=request.getParameter('custparam_serialskuchknqty');
		}
		if(request.getParameter('custparam_opentaskid')!=null && request.getParameter('custparam_opentaskid')!="")
		{
			vopentask=request.getParameter('custparam_opentaskid');
		}
		if(request.getParameter('custparam_contLpno')!=null && request.getParameter('custparam_contLpno')!="")
		{
			vcontlp=request.getParameter('custparam_contLpno');
		}
		if(request.getParameter('custparam_dointernno')!=null && request.getParameter('custparam_dointernno')!="")
		{
			vdointernid=request.getParameter('custparam_dointernno');
		}
		if(request.getParameter('custparam_dolineno')!=null && request.getParameter('custparam_dolineno')!="")
		{
			vdoLineno=request.getParameter('custparam_dolineno');
		}
		if(request.getParameter('custparam_doname')!=null && request.getParameter('custparam_doname')!="")
		{
			vdoName=request.getParameter('custparam_doname');
		}

		if(request.getParameter('custparam_actbeginloc')!=null && request.getParameter('custparam_actbeginloc')!="")
		{
			vbinLoc=request.getParameter('custparam_actbeginloc');
		}

		form.setScript('customscript_pickcnfmserialsave_cl') ;	
		var opentaskField = form.addField('custpage_opentask', 'text', 'Opentask');
		opentaskField.setDefaultValue(vopentask);
		opentaskField.setDisplayType('hidden'); 
		var contLpField = form.addField('custpage_contlp', 'text', 'ContLp');
		contLpField.setDefaultValue(vcontlp);
		contLpField.setDisplayType('hidden'); 

		var doField = form.addField('custpage_doid', 'text', 'do');
		doField.setDefaultValue(vdointernid);
		doField.setDisplayType('hidden');

		var dolineField = form.addField('custpage_dolineno', 'text', 'do line');
		dolineField.setDefaultValue(vdoLineno);
		dolineField.setDisplayType('hidden');

		var doNameField = form.addField('custpage_doname', 'text', 'do name');
		doNameField.setDefaultValue(vdoName);
		doNameField.setDisplayType('hidden');


		var binloc = form.addField('custpage_actbeginloc', 'text', 'Bin Location');
		binloc.setDefaultValue(vbinLoc);
		binloc.setDisplayType('hidden');
		nlapiLogExecution('ERROR','vbinLoc ',vbinLoc);





		var sublist = form.addSubList("custpage_items", "list", "ItemList");
		sublist.addField("custpage_select", "checkbox", "Confirm").setDefaultValue('F'); 
		sublist.addField("custpage_serialno", "text", "Serial #");
		sublist.addField("custpage_serialinternid", "text", "Serial #").setDisplayType('hidden');			

		var filters = new Array();	
		if(vQbSKU !=null && vQbSKU!='')
			filters.push(new nlobjSearchFilter('custrecord_serialitem', null, 'is', vQbSKU)); 	

		filters.push(new nlobjSearchFilter('custrecord_serialparentid', null, 'is', vQbLPNo));
		filters.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [3,19]));


		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_serialnumber');

		var searchresults = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);

		var vSerialNo;
		for (var i = 0; searchresults != null && i < searchresults.length; i++) {
			var searchresult = searchresults[i];

			vSerialNo = searchresult.getValue('custrecord_serialnumber');           
			form.getSubList('custpage_items').setLineItemValue('custpage_serialno', i + 1, vSerialNo);           
			form.getSubList('custpage_items').setLineItemValue('custpage_serialinternid', i + 1, searchresult.getId());

		}
		form.addSubmitButton('Confirm');

		response.writePage(form);
	}
	else //this is the POST block
	{

	}
}

function pickcnfmSerialRecSave()
{
	var tempvalue=nlapiGetFieldValue("custpage_vtempvalue");
	//case# 20141267 ..........below case was not commented properly so did it properly
	
	//case#:20127178 Starts
	var vserialout=nlapiGetFieldValue("custpage_serialout");
	//end
//	alert("vserialout : " + vserialout);
//	alert("tempvalue : " + tempvalue);
	
	if(vserialout=="F"&&tempvalue=="F")
	{
		
		//try {
		var cnt = nlapiGetLineItemCount('custpage_items');
		//alert("Count : " + cnt);
		var opentaskId = nlapiGetFieldValue('custpage_opentask');
		var contLpno = nlapiGetFieldValue('custpage_contlp');
		var doNo = nlapiGetFieldValue('custpage_doid');
		var doName = nlapiGetFieldValue('custpage_doname');		
		var dolineno = nlapiGetFieldValue('custpage_dolineno');
		var actualqty = nlapiGetFieldValue('custpage_actualqty');
		var binloc = nlapiGetFieldValue('custpage_actbeginloc');
		var invref=nlapiGetFieldValue('custpage_invref');
		var fromlp=nlapiGetFieldValue('custpage_fromlp');
		var oldLP=nlapiGetFieldValue('custpage_oldlp');
//		alert(binloc);

		var vbatchno="";
		var vactbinloc="";


		var filters = new Array();
		filters.push(new nlobjSearchFilter('id', null, 'equalto', opentaskId));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('name');
		columns[1] = new nlobjSearchColumn('custrecord_actbeginloc');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);


		var newbinloc = searchresults[0].getValue('custrecord_actbeginloc');
		//alert("newbinloc"+ newbinloc);
		//case 20124196,20124197 start
		if(binloc != newbinloc)
		{
			vactbinloc = binloc;
		}
		else
		{
			vactbinloc = newbinloc;
		}
		//end

		var count=0;
		for (var k = 1; k <= cnt; k++) {
			var vchkselect = nlapiGetLineItemValue('custpage_items', 'custpage_select', k);
			//var lineinternno = nlapiGetLineItemValue('custpage_items', 'custpage_serialinternid', k);
			//var vserialno = nlapiGetLineItemValue('custpage_items', 'custpage_serialno', k);			
			if (vchkselect == 'T') {
				count=parseFloat(count)+1;
			}
		}

		if(actualqty!=null && actualqty!='')
		{
			if(parseFloat(actualqty)!= parseFloat(count))
			{
				alert("Please select " + actualqty + " Serial No(s)");
				return false;
			}

		}


		//alert(doName);
		for (var p = 1; p <= cnt; p++) {
			var chkselect = nlapiGetLineItemValue('custpage_items', 'custpage_select', p);
			var lineinternno = nlapiGetLineItemValue('custpage_items', 'custpage_serialinternid', p);
			var vserialno = nlapiGetLineItemValue('custpage_items', 'custpage_serialno', p);			
			if (chkselect == 'T') {
				//alert(doName);
				/*var fields = new Array();
				var values = new Array();
				fields[0] = 'custrecord_serialcontlpno';
				values[0] = contLpno;
				fields[1] = 'custrecord_serialsono';
				values[1] = doNo;
				fields[2] = 'custrecord_serialsolineno';
				values[2] = dolineno;
				var updatefields = nlapiSubmitField('customrecord_ebiznetserialentry', lineinternno, fields, values);
				alert(updatefields);
				 */				 
				var trnserial = nlapiLoadRecord('customrecord_ebiznetserialentry', lineinternno);
				trnserial.setFieldValue('custrecord_serialcontlpno', contLpno);
				trnserial.setFieldValue('custrecord_serialparentid', contLpno);
				trnserial.setFieldValue('custrecord_serialsono', doNo);
				trnserial.setFieldValue('custrecord_serialsolineno', dolineno);
				trnserial.setFieldValue('custrecord_serialwmsstatus', '8');
				//trnserial.setFieldValue('custrecord_serialebizsono', doNo);

				// alert('Updation');
				var retval  =nlapiSubmitRecord(trnserial, true);

				//alert(retval);				  
				if (vbatchno == "") {
					vbatchno = vserialno;
				}
				else {
					vbatchno = vbatchno + "," + vserialno;
				}
			}
		}

		var opentask = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', opentaskId);
		opentask.setFieldValue('custrecord_serial_no', vbatchno);
		//case# 20147957 (we dont need to update invref and fromlp here, we are updating New LP field in open task)
		/*if(invref!=null && invref!='' && invref!='null')
		{
			
			opentask.setFieldValue('custrecord_invref_no', invref);
			if(fromlp!=null && fromlp!='')
			{
				opentask.setFieldValue('custrecord_from_lp_no', fromlp);
			}
		}*/
		if(fromlp!=null && fromlp!='')
		{
			opentask.setFieldValue('custrecord_from_lp_no', fromlp);
		}
		if(oldLP!=null && oldLP!='')
		opentask.setFieldValue('custrecord_ebiz_new_lp', oldLP);
		//case# 20147957 ends
//		case 20124196,20124197 start
		if(vactbinloc !=null && vactbinloc !='')
			opentask.setFieldValue('custrecord_actbeginloc', vactbinloc);
//		end

		var opentskretval  =nlapiSubmitRecord(opentask, true);

		if(vbatchno!=null && vbatchno!='')
		{
			var oldLP=nlapiGetFieldValue('custpage_oldlp');
			var oldSerials=nlapiGetFieldValue('custpage_oldserials');
			if(oldLP!=null && oldLP!='')
			{
				var filters=new Array();
				filters.push(new nlobjSearchFilter('custrecord_serialparentid',null,'is',oldLP));
				filters.push(new nlobjSearchFilter('custrecord_serialwmsstatus',null,'anyof',['8']));
				var columns=new Array();
				columns.push(new nlobjSearchColumn('custrecord_serialnumber'));
				var serialSearchRecords = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters,columns);
				if(serialSearchRecords !=null && serialSearchRecords!='')
				{
					var serialArray=new Array();
					var varr=oldSerials.split(",");
					if(varr==null || varr=='' || varr=='null')
					{
						serialArray.push(vSerialItems);
					}
					else
					{
						for(var k1=0;k1<varr.length;k1++)
						{
							serialArray.push(varr[k1]); 
						}
					}

					for(var k2=0;k2<serialArray.length;k2++)
					{
						var oldSerialNo=serialArray[k2];
						for(var k3=0;k3<serialSearchRecords.length;k3++)
						{
							var tSerial=serialSearchRecords[k3].getValue('custrecord_serialnumber');
							if(oldSerialNo==tSerial)
							{
								var SerialRec=nlapiSubmitField('customrecord_ebiznetserialentry',serialSearchRecords[k3].getId(),'custrecord_serialwmsstatus','3');
								break;
							}
						}
					}
				}

			}
		}
		alert('Updated Successfully');
		//window.opener.document.location.reload(true);
		window.opener.location.reload();
		window.close(); 


		/*alert('vbatchno  ' + vbatchno);
var fieldsinvt = new Array();
var valuesinvt = new Array();
fieldsinvt[0]='custrecord_batch_no';
valuesinvt[0] = vbatchno;
var updatefieldsinvt = nlapiSubmitField('customrecord_ebiznetserialentry', lineinternno, fieldsinvt, valuesinvt);
alert(updatefieldsinvt);
		 */
		/*}

	catch(exp)
	{
		alert('Into Exception. '+exp);
		return false;
	}*/
		return true;
	}
	else if(vserialout=="T"&&tempvalue=="F")
	{
		var vbatchno="";
		var count1=0;
		var p1="";
		var z1="";
		var tempArray = new Array();
		var cnt = nlapiGetLineItemCount('custpage_items');
		var opentaskId = nlapiGetFieldValue('custpage_opentask');
//		alert("cnt"+cnt);
		var actualqty = nlapiGetFieldValue('custpage_actualqty');
//		alert("actualqty"+actualqty);
		if(actualqty!=null && actualqty!='')
		{
			if(parseInt(actualqty)!= parseInt(cnt))
			{
				alert("Please select " + actualqty + " Serial No(s)");
				return false;
			}
			for ( p1 = 1; p1 <= cnt; p1++) {
				var vserialno = nlapiGetLineItemValue('custpage_items', 'custpage_serialno', p1);
				var chkselect = nlapiGetLineItemValue('custpage_items', 'custpage_select', p1);
				if (chkselect == 'T') {					
						tempArray[p1]=vserialno;					
					if (vbatchno == "") {
						vbatchno = vserialno;
					}
					else {
						vbatchno = vbatchno + "," + vserialno;						
					}
				}
				for(z1 = 1; z1 == p1; z1++)
				tempArray[z1]=tempArray[p1];

			}
			for(z1 = 1; z1 <= cnt; z1++)
			{
				for (p1 = 1; p1 <= cnt; p1++){					
						if(tempArray[z1]==tempArray[p1]&&z1!=p1)
						{
						count1++;
						}
				}
					
			}
			if(count1>0)
			{
				alert('Please donot enter same serial#');
				return false;
			}
			var opentask = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', opentaskId);
			opentask.setFieldValue('custrecord_serial_no', vbatchno);
			var opentskretval  =nlapiSubmitRecord(opentask, true);
			//alert('vbatchno'+vbatchno);
			alert('Updated Successfully');
			//window.opener.document.location.reload(true);
			window.opener.location.reload();
			window.close(); 
				return true;

		}

	}
	else if(tempvalue=="T")
	{
		window.opener.location.reload();
		window.close();
	}
}

var vserialout="F";
function OnInit()
{
	vserialout=nlapiGetFieldValue("custpage_serialout");
//	alert("vserialout"+vserialout);
}

function OnInsert()
{
	var getSerialNo=nlapiGetCurrentLineItemValue("custpage_items","custpage_serialno");
	var filterssertemp = new Array();
	filterssertemp.push(new nlobjSearchFilter('custrecord_tempserialnumber', null, 'is', getSerialNo));
	filterssertemp.push(new nlobjSearchFilter('custrecord_tempserialstatus', null, 'is', 'F'));


	var SrchRecordTmpSerial = nlapiSearchRecord('customrecord_ebiznettempserialentry', null, filterssertemp);

	if(SrchRecordTmpSerial != null && SrchRecordTmpSerial !='')
	{
		alert("Serial No. Already Scanned");
		return false;
	}
	else
	{
		//checking serial no's in records
		var filtersser = new Array();
		filtersser[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getSerialNo);
		filtersser[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [3,19]);

		var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser);
		nlapiLogExecution('DEBUG', 'SrchRecord', SrchRecord);

		if (SrchRecord!=null&& SrchRecord!="") 
		{
			alert("Invalid SerialNo");
			return false;
		}
		else
			return true;
	}
}
