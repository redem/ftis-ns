/***************************************************************************
  eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Suitelet/ebiz_AssignPutawayForm_SL.js,v $
 *     	   $Revision: 1.11.2.1.4.1.4.9 $
 *     	   $Date: 2015/05/04 14:07:18 $
 *     	   $Author: schepuri $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_AssignPutawayForm_SL.js,v $
 * Revision 1.11.2.1.4.1.4.9  2015/05/04 14:07:18  schepuri
 * case# 201412590
 *
 * Revision 1.11.2.1.4.1.4.8  2015/04/10 21:28:10  skreddy
 * Case# 201412323�
 * changed the url path which was hard coded
 *
 * Revision 1.11.2.1.4.1.4.7  2014/05/01 06:53:37  rmukkera
 * Case # 20148211
 *
 * Revision 1.11.2.1.4.1.4.6  2013/08/29 15:57:33  nneelam
 * Case#.20124119�
 * Serail No not dispalyed in PutAway Report.
 *
 * Revision 1.11.2.1.4.1.4.5  2013/07/24 15:26:04  nneelam
 * Case#. 20123560
 * Checking Condition if Receipt Type is not selected in PO..
 *
 * Revision 1.11.2.1.4.1.4.4  2013/04/24 15:20:31  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.11.2.1.4.1.4.3  2013/04/23 15:30:12  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.11.2.1.4.1.4.2  2013/04/09 08:42:19  grao
 * CASE201112/CR201113/LOG201121
 * Issue fixed for Url missing
 *
 * Revision 1.11.2.1.4.1.4.1  2013/03/05 14:52:17  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Lexjet production as part of Standard bundle
 *
 * Revision 1.11.2.1.4.1  2012/11/01 14:55:15  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.11.2.1  2012/04/20 12:29:32  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.11  2011/12/26 13:01:46  gkalla
 * CASE201112/CR201113/LOG201121
 * PO Receipt related changes
 *
 * Revision 1.10  2011/12/06 11:54:00  schepuri
 * CASE201112/CR201113/LOG201121
 * To fetch PO Receipt Type
 *
 * Revision 1.9  2011/08/31 11:13:29  schepuri
 * CASE201112/CR201113/LOG201121
 * Inventory Report updating PutGen Qty
 *
 * Revision 1.8  2011/07/19 12:35:35  schepuri
 * CASE201112/CR201113/LOG201121
 * adding cartlp in sublist
 *
 * Revision 1.7  2011/06/24 11:53:18  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * Inventory Check In Record Deletion changes
 *
 * Revision 1.6  2011/05/30 15:26:35  skota
 * CASE201112/CR201113/LOG201121
 * PO# drop down converted to non-editable text field
 *
 * Revision 1.5  2011/05/30 15:24:26  skota
 * CASE201112/CR201113/LOG201121
 * PO# drop down converted to non-editable textfield
 * 
 *
 *****************************************************************************/
function AssignPutAway(request, response){
	if (request.getMethod() == 'GET') {
		try {

			var form = nlapiCreateForm('Putaway Report');

			var getPOid = request.getParameter('custparam_po');
			nlapiLogExecution('ERROR', 'getPOid', getPOid);
			var getPoValue = request.getParameter('custparam_povalue');
			nlapiLogExecution('ERROR', 'getPoValue', getPoValue);

			var getttype = request.getParameter('custparam_ttype');
			//var getReciptType = request.getParameter('custparam_ReciptType');
			nlapiLogExecution('ERROR', 'getPoValue', getPoValue);
			nlapiLogExecution('ERROR', 'getttype', getttype);

			var getTrailer = request.getParameter('custparam_trailer');
			nlapiLogExecution('ERROR', 'getTrailer', getTrailer);

			var getReceipt = request.getParameter('custparam_receipt');
			nlapiLogExecution('ERROR', 'getReceipt', getReceipt);

			if(getPOid != null && getPOid != "")
				trantype = nlapiLookupField('transaction', getPOid, 'recordType');
			else if((getTrailer != null && getTrailer != "") || (getReceipt != null && getReceipt != ""))
			{
				nlapiLogExecution('ERROR', 'trantype inif', trantype);
				trantype ='purchaseorder';
			}	

			selectRecipt = form.addField('custpage_receiptno', 'text', 'Receipt #').setDisplayType('disabled');

			selectRecipt.setLayoutType('endrow');

			selectTrailer = form.addField('custpage_trailerno', 'text', 'Trailer #').setDisplayType('disabled');

			selectTrailer.setLayoutType('endrow');

			if(getTrailer != null && getTrailer != "")
				selectTrailer.setDefaultValue(getTrailer);
			if(getReceipt != null && getReceipt != "")
				selectRecipt.setDefaultValue(getReceipt);
			nlapiLogExecution('ERROR', 'getTrailer', getTrailer);
			nlapiLogExecution('ERROR', 'getReceipt', getReceipt);

			form.setScript('customscript_assignputaway');
			//Case # 20148211 start
			if(getPOid != null && getPOid != "")
			{
				form.addButton('custpage_print', 'Print',"Printreport('"+getPOid+"','"+getPoValue+"')");

			}
			//Case # 20148211 end
			/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh as part of Standard bundle***/
			if((getReceipt != null && getReceipt != "")||(getTrailer !=null && getTrailer !=""))
				form.addButton('custpage_poreceiptprint', 'Print Receipt', 'PrintreportReceipt()');
			/*** Up to here ***/ 
			var selectReceive = form.addField('custpage_selectreceive', 'select', 'Receive Mode').setLayoutType('startrow');

			selectReceive.addSelectOption('P', 'PO');
			selectReceive.addSelectOption('A', 'ASN');

			//var selectRType = form.addField('custpage_receive_type', 'select', 'Receipt Type').setLayoutType('midrow');
			//selectRType.addSelectOption('PO', 'Purchase Order');
			var poFields = ['custbody_nswmsporeceipttype'];
			var poColumns = nlapiLookupField('transaction', getPOid, poFields);

			var receiptTypeValue = poColumns.custbody_nswmsporeceipttype;

			nlapiLogExecution('ERROR','here', receiptTypeValue);
			//case# 20123560  start
			var receiptType='';

			if(receiptTypeValue!=null && receiptTypeValue!='')
			{
				var receiptTypeRes = nlapiLoadRecord('customrecord_ebiznet_receipt_type', receiptTypeValue);
				receiptType = receiptTypeRes.getFieldValue('name');

			}
			//case# 20123560  end


			var selectRType = form.addField('custpage_receive_type', 'text', 'Receipt Type').setDisplayType('disabled');            
			selectRType.setDefaultValue(receiptType);

			/* 
            //Get distinct po's and its IDs into dropdown  
            var selectPO = form.addField('custpage_po', 'select', 'PO #').setLayoutType('endrow');
            var filterspo = new Array();
            filterspo[0] = new nlobjSearchFilter('mainline', null, 'is', 'T');
            var searchresults = nlapiSearchRecord('purchaseorder', null, filterspo, new nlobjSearchColumn('tranid'));
            for (var i = 0; i < Math.min(500, searchresults.length); i++) {

                selectPO.addSelectOption(searchresults[i].getId(), searchresults[i].getValue('tranid'));
            }            
			 */

			var selectPO = form.addField('custpage_po', 'text', 'PO #').setDisplayType('disabled');            
			selectPO.setDefaultValue(getPoValue);

			/*
             Defining SubList lines
			 */
			var sublist = form.addSubList("custpage_items", "list", "ItemList");
			if(trantype=='purchaseorder')
			{
				sublist.addField("custpage_po_asn", "text", "PO/ASN#");
				sublist.addField("custpage_receipt", "text", "Receipt#");
				sublist.addField("custpage_trailerno", "text", "Appointment#");
			}
			else
			{
				sublist.addField("custpage_po_asn", "text", "PO/ASN#");
			}
			sublist.addField("custpage_line", "text", "Line#");
			sublist.addField("custpage_itemname", "text", "Item");
			sublist.addField("custpage_itemdesc", "text", "Item Description");			
			sublist.addField("custpage_itemstatus", "select", "Item Status", "customrecord_ebiznet_sku_status").setDisplayType('inline');
			/*
  Commented on 03/31/2011 by Phani as Task Type is not required to be displayed on the report

            sublist.addField("custpage_taskstatus", "text", "Task Status");
			 */
			sublist.addField("custpage_uom", "text", "UOM");
			sublist.addField("custpage_quantity", "text", "Quantity");
			sublist.addField("custpage_lp", "text", "LP#");
			sublist.addField("custpage_transport_lp", "text", "Cart LP #");
			sublist.addField("custpage_serialno", "textarea", "Serial #");
			sublist.addField("custpage_seriallot", "text", "LOT# Numbers");
			sublist.addField("custpage_location", "select", "Bin Location", "customrecord_ebiznet_location").setDisplayType('inline');
			sublist.addField("custpage_poid", "text", "PO ID").setDisplayType('hidden');
			sublist.addField("custpage_ttype", "text", "Task Type").setDisplayType('hidden');


			var autobutton = form.addSubmitButton('Confirm Putaway');

			//LN,Serial # were comma separated here...!
			var arrayLP = new Array();


			var lpcolumns = new Array();
			lpcolumns[0] = new nlobjSearchColumn('custrecord_serialparentid');
			lpcolumns[1] = new nlobjSearchColumn('custrecord_serialnumber');

			//case# 20124119�  start
			var lpfilters=new Array();
			if(getPOid!=null && getPOid!='')
				lpfilters = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', getPOid);
			//case# 20124119�  end


			var lpsearchresults = nlapiSearchRecord('customrecord_ebiznetserialentry', null, lpfilters, lpcolumns);
			for (var i = 0; lpsearchresults != null && i < lpsearchresults.length; i++) {
				var SerialLP = new Array();
				SerialLP[0] = lpsearchresults[i].getValue('custrecord_serialparentid');
				SerialLP[1] = lpsearchresults[i].getValue('custrecord_serialnumber');
				arrayLP.push(SerialLP);

			}
			for (var i = 0; i < arrayLP.length; i++) 
				nlapiLogExecution('ERROR', 'arrayLP - [LP#|Serial#]', '[' + arrayLP[i][0] + '|' + arrayLP[i][1] + ']');

			// define search filters
			var filters = new Array();
			//var status = new Array("N","L");
			//filters[0] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', getPOid);
			if((getTrailer != null && getTrailer != "") || (getReceipt != null && getReceipt != ""))
			{
				if(getTrailer != null && getTrailer != "")
				{
					nlapiLogExecution('ERROR', 'getTrailer', getTrailer);
					filters.push(new nlobjSearchFilter('custrecord_ebiz_trailer_no', null, 'is', getTrailer));
				}	
				if(getReceipt != null && getReceipt != "")
				{
					nlapiLogExecution('ERROR', 'getReceipt', getReceipt);
					filters.push(new nlobjSearchFilter('custrecord_ot_receipt_no', null, 'is', getReceipt));
				}
			}
			else
			{
				if(getPOid != null && getPOid != "")
					filters.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', getPOid));
			}

			filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);
			// filters[2] = new nlobjSearchFilter('custrecord_status_flag', null, 'is', 'L');
			filters[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2, 6]);


			//filters[2] = new nlobjSearchFilter('custrecord_actendloc', null, 'is', isNaN);
			//filters[2] = new nlobjSearchFilter('custrecord_actendloc', null, 'noneof', '@NONE@');
			//filters[3] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');

			// return opportunity sales rep, customer custom field, and customer ID
			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
			columns[1] = new nlobjSearchColumn('custrecord_line_no');
			columns[2] = new nlobjSearchColumn('custrecord_sku');
			columns[3] = new nlobjSearchColumn('custrecord_sku_status');
			columns[4] = new nlobjSearchColumn('custrecord_tasktype');
			columns[5] = new nlobjSearchColumn('custrecord_uom_id');
			columns[6] = new nlobjSearchColumn('custrecord_expe_qty');
			//columns[7] = new nlobjSearchColumn('custrecord_ebiz_lpno');custrecord_lpno
			columns[7] = new nlobjSearchColumn('custrecord_lpno');
			columns[8] = new nlobjSearchColumn('custrecord_actbeginloc');
			columns[9] = new nlobjSearchColumn('custrecord_batch_no');
			columns[10] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			columns[11] = new nlobjSearchColumn('custrecord_transport_lp');

			columns[12] = new nlobjSearchColumn('custrecord_ebiz_trailer_no');
			columns[13] = new nlobjSearchColumn('custrecord_ot_receipt_no');
			columns[14] = new nlobjSearchColumn('custrecord_act_qty');
			columns[15] = new nlobjSearchColumn('custrecord_skudesc');// case# 201412590
			columns[1].setSort();


			// execute the  search, passing null filter and return columns
			var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
			var po_asn, line, itemname, itemstatus, taskstatus, uom, qty, lp, loc, seriallot, retserialcsv, itemintid,cartlp,itemdesc;
			var trailer,receipt;
			/*
             Searching records from custom record and dynamically adding to the sublist lines
			 */
			for (var i = 0; searchresults != null && i < searchresults.length; i++) {
				var searchresult = searchresults[i];

				po_asn = searchresult.getValue('custrecord_ebiz_cntrl_no');
				if(po_asn != null && po_asn != "")
				{
					if((getTrailer != null && getTrailer != "") || (getReceipt != null && getReceipt != ""))
					{
						getPOid = searchresult.getValue('custrecord_ebiz_cntrl_no');
						getPoValue= nlapiLookupField('purchaseorder',getPOid,'tranid');
					}
				}


				line = searchresult.getValue('custrecord_line_no');
				itemname = searchresult.getText('custrecord_sku');
				itemstatus = searchresult.getValue('custrecord_sku_status');
				taskstatus = searchresult.getText('custrecord_tasktype');
				uom = searchresult.getValue('custrecord_uom_id');
				qty = searchresult.getValue('custrecord_expe_qty');
				//qty = searchresult.getValue('custrecord_act_qty');
				//lp = searchresult.getValue('custrecord_ebiz_lpno');
				lp = searchresult.getValue('custrecord_lpno');       
				cartlp = searchresult.getValue('custrecord_transport_lp');    
				loc = searchresult.getValue('custrecord_actbeginloc');
				seriallot = searchresult.getValue('custrecord_batch_no');
				itemintid = searchresult.getValue('custrecord_ebiz_sku_no');
				itemdesc = searchresult.getValue('custrecord_skudesc');
				nlapiLogExecution('ERROR', 'seriallot', seriallot);
				nlapiLogExecution('ERROR', 'lp', lp);

				trailer = searchresult.getValue('custrecord_ebiz_trailer_no');
				receipt = searchresult.getValue('custrecord_ot_receipt_no');

				var serialInflg = "F";
				var batchflg = "F";
				var fields = ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot'];
				var columns = nlapiLookupField('item', itemintid, fields);
				var ItemType = columns.recordType;
				serialInflg = columns.custitem_ebizserialin;
				batchflg = columns.custitem_ebizbatchlot;


				form.getSubList('custpage_items').setLineItemValue('custpage_po_asn', i + 1, getPoValue);
				form.getSubList('custpage_items').setLineItemValue('custpage_line', i + 1, line);
				form.getSubList('custpage_items').setLineItemValue('custpage_itemname', i + 1, itemname);
				form.getSubList('custpage_items').setLineItemValue('custpage_itemdesc', i + 1, itemdesc);
				form.getSubList('custpage_items').setLineItemValue('custpage_itemstatus', i + 1, itemstatus);
				/*
Commented on 03/31/2011 by Phani as Task Type is not required to be displayed on the report

                form.getSubList('custpage_items').setLineItemValue('custpage_taskstatus', i + 1, taskstatus);
				 */

				form.getSubList('custpage_items').setLineItemValue('custpage_uom', i + 1, uom);
				form.getSubList('custpage_items').setLineItemValue('custpage_quantity', i + 1, qty);
				if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") {
					retserialcsv = getSerialNoCSV(arrayLP, lp);
					form.getSubList('custpage_items').setLineItemValue('custpage_serialno', i + 1, retserialcsv);
				}
				form.getSubList('custpage_items').setLineItemValue('custpage_lp', i + 1, lp);
				form.getSubList('custpage_items').setLineItemValue('custpage_transport_lp', i + 1, cartlp);
				form.getSubList('custpage_items').setLineItemValue('custpage_location', i + 1, loc);
				form.getSubList('custpage_items').setLineItemValue('custpage_poid', i + 1, getPOid);
				form.getSubList('custpage_items').setLineItemValue('custpage_ttype', i + 1, getttype);

				form.getSubList('custpage_items').setLineItemValue('custpage_receipt', i + 1, receipt);
				form.getSubList('custpage_items').setLineItemValue('custpage_trailerno', i + 1, trailer);


				//  form.getSubList('custpage_items').setLineItemValue('custpage_seriallot', i + 1, seriallot);
				if (ItemType == "lotnumberedinventoryitem" || batchflg == "T" || ItemType=="lotnumberedassemblyitem") {// case# 201416896
					form.getSubList('custpage_items').setLineItemValue('custpage_seriallot', i + 1, seriallot);
				}
				nlapiLogExecution('ERROR', 'POInternalId in Request Object', getPOid);


			}
			//form.addPageLink('breadcrumb', 'Back to PO Check-In', nlapiResolveURL('RECORD', 'purchaseorder',getPOid))
			form.addPageLink('crosslink', 'Back to PO Check-In', nlapiResolveURL('RECORD', 'purchaseorder', getPOid));
			response.writePage(form);
		} 
		catch (expss) {
			nlapiLogExecution('ERROR', 'Request Error', expss);
		}


	}
	else {
		nlapiLogExecution('ERROR', 'Into Response of assing Putaway', 'Assign Putaway');
		var POInternalId = request.getLineItemValue('custpage_items', 'custpage_poid', 1);
		var ttype = request.getLineItemValue('custpage_items', 'custpage_ttype', 1);
		var POarrayAP = new Array();
		POarrayAP["custparam_poid"] = POInternalId;
		// POarrayAP["custparam_ttype"] = ttype;
		nlapiLogExecution('ERROR', 'POInternalId', POInternalId);

		var trailerno=request.getParameter('custpage_trailerno');
		var receiptno=request.getParameter('custpage_receiptno');

		nlapiLogExecution('ERROR','trailerno', trailerno);
		nlapiLogExecution('ERROR','receiptno', receiptno);
		POarrayAP["custparam_trailer"]=trailerno;
		POarrayAP["custparam_receipt"]=receiptno;

		var i = -1;
		var arrQty = new Array();
		var arrLine = new Array();
		// To Consolidate Quantity and Send to TRN table.
		try {
			var lineCnt = request.getLineItemCount('custpage_items');
			nlapiLogExecution('ERROR', 'lineCnt', lineCnt);
			var Lineno;
			var TempLineno;
			var boolFlag = false;
			var Qty = 0;
			var stempLine = 0;

			for (var s = 1; s <= lineCnt + 1; s++) {
				if (s == lineCnt + 1) {
					i++;
					arrQty[i] = Qty;
					arrLine[i] = Lineno;
					nlapiLogExecution('ERROR', 'Value of i and Qty if s is cnt', arrQty[i] + 'vale of i ' + i + 'In last case' + Lineno);
					break;
				}
				Lineno = request.getLineItemValue('custpage_items', 'custpage_line', s);
				nlapiLogExecution('ERROR', 'Lineno', Lineno);
				if (s == 1) {
					TempLineno = Lineno;
				}
				if (TempLineno == Lineno) {
					if (boolFlag == false) {
						boolFlag = true;
						Qty = parseFloat(request.getLineItemValue('custpage_items', 'custpage_quantity', s));
						nlapiLogExecution('ERROR', 'Qty1', Qty);
					}
					else {
						Qty += parseFloat(request.getLineItemValue('custpage_items', 'custpage_quantity', s));
						nlapiLogExecution('ERROR', 'Qty2', Qty);
					}
				}
				else {
					i = i + 1;

					nlapiLogExecution('ERROR', 'Value of i and Qty in else', Qty + 'vale of i ' + i);
					arrQty[i] = Qty;
					arrLine[i] = TempLineno;
					Qty = 0; //make it null for further loop.
					boolFlag = true;
					Qty = parseFloat(request.getLineItemValue('custpage_items', 'custpage_quantity', s));
					Lineno = request.getLineItemValue('custpage_items', 'custpage_line', s);
					TempLineno = Lineno;
				}

			}
		} 
		catch (exp) {
			nlapiLogExecution('ERROR', 'Err in summing Qty', exp);
		}
		//To get tran type for a record.
		var tranType = nlapiLookupField('transaction', POInternalId, 'recordType');
		//Transaction Line Updation.
		var type = tranType;
		var putgenqty = 0;
		var putconfqty = 0;
		// var ttype = "ASPW";
		if(ttype == "ASPW")
		{
			for (var p = 0; p < arrLine.length; p++) {
				// var type = "purchaseorder";

				var confqty = arrQty[p]; //checkinLineQty;
				var lineno = arrLine[p];
				///nlapiLogExecution('ERROR', '-->quan', quan);
				nlapiLogExecution('ERROR', 'confqty', confqty);
				//TrnLineUpdation(orderType, ttype, poValue, poid, lineno, ItemId, quan, confqty,chkAssignputW,ItemStatus){
				var tranValue = TrnLineUpdation(type, ttype, POInternalId, POInternalId, lineno, null, null, confqty, null, 1);
			}
		}

		response.sendRedirect('SUITELET', 'customscript_confirmputaway', 'customdeploy1', false, POarrayAP);


	}
}

function getSerialNoCSV(arrayLP, lp){
	var csv = "";
	for (var i = 0; i < arrayLP.length; i++) {
		if (arrayLP[i][0] == lp) {
			csv += arrayLP[i][1] + " , ";
		}
	}
	return csv;
}

/*** The below code is merged from Lexjet production account on 02-26-2013 by Santosh as part of Standard bundle***/
function Printreport(poid, povalue){
	var povalue= nlapiGetFieldValue('custpage_po');
	var linkURL = nlapiResolveURL('SUITELET', 'customscript_putw_report_pdf', 'customdeploy_putw_report_pdf_di');
	var ctx = nlapiGetContext();
	nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
	/*if (ctx.getEnvironment() == 'PRODUCTION') {
		linkURL = 'https://system.netsuite.com' + linkURL;			
	}
	else if (ctx.getEnvironment() == 'SANDBOX') {
		linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
	}*/
	linkURL = linkURL + '&custparam_po=' + poid;
	linkURL = linkURL + '&custparam_povalue=' + povalue;
	window.open(linkURL);
}
function PrintreportReceipt()
{ 
	var getReceipt= nlapiGetFieldValue('custpage_receiptno');
	var getTrailer= nlapiGetFieldValue('custpage_trailerno');
//	alert(getReceipt.toString());
	var linkURL = nlapiResolveURL('SUITELET', 'customscript_ebiz_poreceipt_report_pdf', 'customdeploy_ebiz_poreceipt_report_pdf_d');
	var ctx = nlapiGetContext();
	nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
	/*if (ctx.getEnvironment() == 'PRODUCTION') {
		linkURL = 'https://system.netsuite.com' + linkURL;			
	}
	else if (ctx.getEnvironment() == 'SANDBOX') {
		linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
	}*/
	linkURL = linkURL + '&custparam_poreceipt='+ getReceipt.toString()+ '&custparam_potrailer='+ getTrailer.toString();
	window.open(linkURL);
}
/*** Up to here ***/ 