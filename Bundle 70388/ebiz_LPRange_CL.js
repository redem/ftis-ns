/***************************************************************************
 ebiz_LPRange_CL.js
�eBizNET Solutions Inc 
 ****************************************************************************
 *
 *� $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Client/ebiz_LPRange_CL.js,v $
 *� $Revision: 1.18.2.20.4.6.2.65.2.14 $
 *� $Date: 2015/12/02 15:11:35 $
 *� $Author: grao $
 *� $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_LPRange_CL.js,v $
 *� Revision 1.18.2.20.4.6.2.65.2.14  2015/12/02 15:11:35  grao
 *� 2015.2 Issue Fixes 201415582
 *�
 *� Revision 1.18.2.20.4.6.2.65.2.13  2015/11/17 10:08:54  grao
 *� 2015.2 Issue Fixes 201415686
 *�
 *� Revision 1.18.2.20.4.6.2.65.2.12  2015/11/16 23:27:30  snimmakayala
 *� 201415693
 *�
 *� Revision 1.18.2.20.4.6.2.65.2.11  2015/11/16 17:07:54  deepshikha
 *� 2015.2 issues
 *� 201415665
 *�
 *� Revision 1.18.2.20.4.6.2.65.2.10  2015/11/10 15:39:47  grao
 *� 2015.2 Issue Fixes 201415528
 *�
 *� Revision 1.18.2.20.4.6.2.65.2.9  2015/11/09 23:29:21  nneelam
 *� case# 201413097
 *�
 *� Revision 1.18.2.20.4.6.2.65.2.8  2015/11/09 16:16:02  deepshikha
 *� 2015.2 issues
 *� 201415323
 *�
 *� Revision 1.18.2.20.4.6.2.65.2.7  2015/10/21 15:40:57  deepshikha
 *� 2015.2 issue fixes
 *� 201415066
 *�
 *� Revision 1.18.2.20.4.6.2.65.2.6  2015/10/14 13:56:47  deepshikha
 *� 2015.2 issues
 *� 201415042
 *�
 *� Revision 1.18.2.20.4.6.2.65.2.5  2015/10/09 15:42:55  aanchal
 *� 2015.2 issues
 *� 201414959
 *�
 *� Revision 1.18.2.20.4.6.2.65.2.4  2015/10/09 15:34:01  deepshikha
 *� 2015.2 issue fixes
 *� 201414882
 *�
 *� Revision 1.18.2.20.4.6.2.65.2.3  2015/10/07 13:12:39  schepuri
 *� case# 201414898
 *�
 *� Revision 1.18.2.20.4.6.2.65.2.2  2015/09/22 13:59:25  aanchal
 *� 201414368
 *� 2015.2 Issue FIx
 *�
 *� Revision 1.18.2.20.4.6.2.65.2.1  2015/09/18 15:33:17  aanchal
 *� 201414370
 *� 2015.2 Issue FIx
 *�
 *� Revision 1.18.2.20.4.6.2.65  2015/07/30 13:26:51  schepuri
 *� case# 201413579
 *�
 *� Revision 1.18.2.20.4.6.2.64  2015/07/16 13:09:57  schepuri
 *� case# 201413352
 *�
 *� Revision 1.18.2.20.4.6.2.63  2015/06/12 15:47:20  grao
 *� SB issue fixes 201413040
 *�
 *� Revision 1.18.2.20.4.6.2.62  2015/02/17 13:10:10  schepuri
 *� issue# 201411362
 *�
 *� Revision 1.18.2.20.4.6.2.61  2015/02/03 13:51:07  schepuri
 *� issue # 201411385
 *�
 *� Revision 1.18.2.20.4.6.2.60  2014/12/05 14:13:42  skavuri
 *� Case# 201411182 std bundle issue fixed
 *�
 *� Revision 1.18.2.20.4.6.2.59  2014/11/07 12:10:13  vmandala
 *� case#  201410987 Stdbundle issue fixed
 *�
 *� Revision 1.18.2.20.4.6.2.58  2014/09/24 16:01:04  sponnaganti
 *� Case# 201410440
 *� Stnd Bundle Issue fix
 *�
 *� Revision 1.18.2.20.4.6.2.57  2014/08/27 15:31:25  sponnaganti
 *� Case# 201410028
 *� Stnd Bundle Issue fix
 *�
 *� Revision 1.18.2.20.4.6.2.56  2014/08/20 15:27:53  rmukkera
 *� Case # 201410028
 *�
 *� Revision 1.18.2.20.4.6.2.55  2014/07/29 15:02:15  grao
 *� Case#: 20149594 New 2014.2 Compatibilty issue fixes
 *�
 *� Revision 1.18.2.20.4.6.2.54  2014/06/16 06:31:29  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Issue fixed related to case#20148887
 *�
 *� Revision 1.18.2.20.4.6.2.53  2014/06/11 15:02:02  grao
 *� Case#: 20148803  and  20148819  Standard  issue fixes
 *�
 *� Revision 1.18.2.20.4.6.2.52  2014/06/03 15:40:16  skavuri
 *� Case# 20148651, 20148626 SB Issue Fixed
 *�
 *� Revision 1.18.2.20.4.6.2.51  2014/05/29 15:36:21  skavuri
 *� Case #20148592 SB Issue Fixed
 *�
 *� Revision 1.18.2.20.4.6.2.50  2014/05/27 15:25:02  skreddy
 *� case # 20148489
 *� Standard Bundle issue fix
 *�
 *� Revision 1.18.2.20.4.6.2.49  2014/05/16 15:05:28  skavuri
 *� Case # 20148385 SB issue fixed
 *�
 *� Revision 1.18.2.20.4.6.2.48  2014/04/28 15:51:20  skavuri
 *� Case # 20148053 SB Issue Fixed
 *�
 *� Revision 1.18.2.20.4.6.2.47  2014/04/25 16:06:02  nneelam
 *� case#  20148161
 *� Stanadard Bundle Issue Fix.
 *�
 *� Revision 1.18.2.20.4.6.2.46  2014/04/16 15:35:09  skavuri
 *� Case # 20148002 SB issue Fixed
 *�
 *� Revision 1.18.2.20.4.6.2.45  2014/03/26 14:37:46  rmukkera
 *� Case # 20127157
 *�
 *� Revision 1.18.2.20.4.6.2.44  2014/03/13 14:19:17  nneelam
 *� case#  20127693
 *� Dealmed Issue Fix.
 *�
 *� Revision 1.18.2.20.4.6.2.43  2014/03/12 06:35:44  skreddy
 *� case 20127561
 *� Deal med SB issue fixs
 *�
 *� Revision 1.18.2.20.4.6.2.42  2014/03/10 16:10:46  skavuri
 *� Case# 20127618 issue fixed
 *�
 *� Revision 1.18.2.20.4.6.2.41  2014/03/04 08:59:17  rmukkera
 *� Case # 20127194,20127206 ,20127485
 *�
 *� Revision 1.18.2.20.4.6.2.40  2014/02/14 15:28:15  skavuri
 *� Case # 20127165 RMA chkn is working fine
 *�
 *� Revision 1.18.2.20.4.6.2.39  2014/02/12 15:07:09  sponnaganti
 *� case# 20127129
 *� (check for tantype)
 *�
 *� Revision 1.18.2.20.4.6.2.38  2014/02/11 15:22:28  sponnaganti
 *� case# 20127114
 *� (Lp validation filter added)
 *�
 *� Revision 1.18.2.20.4.6.2.37  2014/02/10 16:36:50  skavuri
 *� Case#20127061
 *�
 *� Revision 1.18.2.20.4.6.2.36  2014/01/16 13:37:28  schepuri
 *� 20126832
 *� standard bundle issue fix
 *�
 *� Revision 1.18.2.20.4.6.2.35  2014/01/15 13:44:22  schepuri
 *� 20126736
 *� standard bundle issue fix
 *�
 *� Revision 1.18.2.20.4.6.2.34  2014/01/07 15:26:17  rmukkera
 *� Case # 20126677
 *�
 *� Revision 1.18.2.20.4.6.2.33  2013/12/27 15:37:58  rmukkera
 *� Case # 20126576
 *�
 *� Revision 1.18.2.20.4.6.2.32  2013/12/24 14:12:16  schepuri
 *� 20126461
 *�
 *� Revision 1.18.2.20.4.6.2.31  2013/12/20 16:15:20  gkalla
 *� case#20126479
 *� Standard bundle issue
 *�
 *� Revision 1.18.2.20.4.6.2.30  2013/12/18 15:23:23  nneelam
 *� case# 20126362
 *� Multiple line level location in PO
 *�
 *� Revision 1.18.2.20.4.6.2.29  2013/12/18 15:17:06  schepuri
 *� 20126190
 *�
 *� Revision 1.18.2.20.4.6.2.28  2013/12/12 15:02:46  rmukkera
 *� Case# 20126158
 *�
 *� Revision 1.18.2.20.4.6.2.27  2013/12/12 07:26:23  nneelam
 *� Case# 20125980
 *� std bundle issue fix..
 *�
 *� Revision 1.18.2.20.4.6.2.26  2013/12/02 15:13:57  skreddy
 *� Case# 20125985 & 20125986
 *� 2014.1 stnd bundle issue fix
 *�
 *� Revision 1.18.2.20.4.6.2.25  2013/11/01 12:50:15  rmukkera
 *� Case# 20124936
 *�
 *� Revision 1.18.2.20.4.6.2.24  2013/10/25 16:12:43  skreddy
 *� Case# 20125113
 *� standard bundle  issue fix
 *�
 *� Revision 1.18.2.20.4.6.2.23  2013/10/04 15:44:45  rmukkera
 *� Case# 20124672
 *�
 *� Revision 1.18.2.20.4.6.2.22  2013/09/18 15:27:59  nneelam
 *� Case#. 20124476
 *� Serial No. Status Updation Issue in Serial entry reocrd Issue Fix.
 *�
 *� Revision 1.18.2.20.4.6.2.21  2013/08/30 16:39:18  skreddy
 *� Case# 20123969 ,20124104
 *� standard bundle issue fix
 *�
 *� Revision 1.18.2.20.4.6.2.20  2013/08/29 07:09:35  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *�
 *� stdchanges
 *� case#20123755
 *�
 *� Revision 1.18.2.20.4.6.2.17.2.7  2013/08/28 16:32:11  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *�
 *� stdissuefixes
 *� case#20123755
 *�
 *� Revision 1.18.2.20.4.6.2.17.2.6  2013/08/24 15:08:35  grao
 *� issue fixes 20124050, 20124053
 *�
 *� Revision 1.18.2.20.4.6.2.17.2.5  2013/08/23 15:08:58  grao
 *� Case#:20124015�,20124021�
 *� � issue fixes
 *�
 *� Revision 1.18.2.20.4.6.2.17.2.4  2013/08/21 16:10:21  rmukkera
 *� Case#  20123904 ,20123908
 *�
 *� Revision 1.35  2013/08/21 15:43:58  rmukkera
 *� Issue Fix related to 20123904�,20123908�
 *�
 *� Revision 1.34  2013/08/20 15:31:32  rmukkera
 *� Issue Fix related to 20123932
 *�
 *� Revision 1.33  2013/08/19 16:19:01  rmukkera
 *� Issue Fix related to FIXED--->20123785�,20123902�,20123903,20123906�,20123907�,20123910�,�
 *�
 *� Revision 1.32  2013/08/07 15:18:13  rmukkera
 *� case# 20123783
 *� While performing check in through link system displayed unexpected suite script error occurred in a script running on this page.
 *�
 *� Revision 1.31  2013/08/02 15:45:25  rmukkera
 *� Issue fix for
 *� In UI Check in system allows to perform check in successfully by giving LP prefix only(With out giving any number followed by Prefix
 *�
 *� Revision 1.30  2013/05/01 13:14:48  rmukkera
 *� Fix related spaces in the entered lot
 *�
 *� Revision 1.18.2.20.4.6.2.7  2013/04/17 15:54:21  skreddy
 *� CASE201112/CR201113/LOG201121
 *� issue fixes
 *�
 *� Revision 1.18.2.20.4.6.2.6  2013/04/09 08:48:30  grao
 *� CASE201112/CR201113/LOG201121
 *� Issue fixed for set alert for serial numbers
 *�
 *� Revision 1.18.2.20.4.6.2.5  2013/04/03 20:40:48  kavitha
 *� CASE201112/CR201113/LOG2012392
 *� TSG Issue fixes
 *�
 *� Revision 1.18.2.20.4.6.2.4  2013/03/19 12:21:59  schepuri
 *� CASE201112/CR201113/LOG201121
 *� change url path
 *�
 *� Revision 1.18.2.20.4.6.2.3  2013/03/19 11:46:48  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Production and UAT issue fixes.
 *�
 *� Revision 1.18.2.20.4.6.2.2  2013/03/05 13:35:46  rmukkera
 *� Merging of lexjet Bundle files to Standard bundle
 *�
 *� Revision 1.18.2.20.4.6.2.1  2013/02/26 12:56:04  skreddy
 *� CASE201112/CR201113/LOG201121
 *� merged boombah changes
 *�
 *� Revision 1.18.2.20.4.6  2013/02/22 06:42:18  skreddy
 *� CASE201112/CR201113/LOG201121
 *� issue related to accepting invalid lp Prefix
 *�
 *� Revision 1.18.2.20.4.5  2013/02/07 08:49:54  skreddy
 *� CASE201112/CR201113/LOG201121
 *� Date Validation
 *�
 *� Revision 1.18.2.20.4.4  2012/12/14 07:42:52  spendyala
 *� CASE201112/CR201113/LOG201121
 *� moved from 2012.2 branch
 *�
 *� Revision 1.18.2.20.4.3  2012/11/01 14:55:22  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Decimal Qty Conversions
 *�
 *� Revision 1.18.2.20.4.2  2012/10/03 11:29:53  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Auto Gen Lot No
 *�
 *� Revision 1.18.2.20.4.1  2012/09/26 21:41:39  svanama
 *� CASE201112/CR201113/LOG201121
 *� item label generation code added
 *�
 *� Revision 1.18.2.20  2012/09/14 07:20:47  schepuri
 *� CASE201112/CR201113/LOG201121
 *� checking for null
 *�
 *� Revision 1.18.2.19  2012/08/29 16:19:23  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Changed Prompt message.
 *�
 *� Revision 1.18.2.18  2012/08/23 23:16:10  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Change of sentence which we are prompting at the time of UPC code update.
 *�
 *� Revision 1.18.2.17  2012/08/22 14:48:04  schepuri
 *� no message
 *�
 *� Revision 1.18.2.16  2012/08/21 22:41:33  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Prompting user weather Change in UPC code is updated to item master or not
 *�
 *� Revision 1.18.2.15  2012/08/03 13:20:07  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Validating recommended quantity
 *�
 *� Revision 1.18.2.14  2012/06/19 07:13:59  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Issue related to creating same batch# for multiple Sku's.
 *�
 *� Revision 1.18.2.13  2012/06/18 12:59:34  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *� validation on location
 *�
 *� Revision 1.18.2.12  2012/05/29 12:45:24  schepuri
 *� CASE201112/CR201113/LOG201121
 *� issue fix for vaidating qty
 *�
 *� Revision 1.18.2.11  2012/05/21 13:04:16  schepuri
 *� CASE201112/CR201113/LOG201121
 *� validation on checkin once checkin already completed
 *�
 *� Revision 1.18.2.10  2012/05/07 13:43:44  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Showing a popup window to scan serialitem# in putaway confirmation screen.
 *�
 *� Revision 1.18.2.9  2012/04/26 07:14:43  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Removing unwanted alert Messages.
 *�
 *� Revision 1.18.2.8  2012/04/25 15:41:15  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Validating entered Batch Item issue is resolved.
 *�
 *� Revision 1.18.2.7  2012/04/20 12:26:36  schepuri
 *� CASE201112/CR201113/LOG201121
 *� changing the Label of Batch #  field to Lot#
 *�
 *� Revision 1.18.2.6  2012/04/06 17:11:24  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Unwanted alert Msg are removed.
 *�
 *� Revision 1.18.2.5  2012/03/30 05:02:25  vrgurujala
 *� t_NSWMS_LOG201121_91
 *�
 *� Revision 1.23  2012/03/30 05:00:16  vrgurujala
 *� CASE201112/CR201113/LOG201121
 *�
 *� Revision 1.22  2012/02/08 12:03:08  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Code Merge
 *�
 *� Revision 1.21  2012/01/30 07:05:05  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *�
 *� Revision 1.20  2012/01/23 10:18:50  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Added SearchColumn to fetch the record with highest UOMLevel from item dimensions.
 *� 
 *
 ****************************************************************************/

/**
 * @author LN
 *@version
 *@date
 *@Description: This is a Client Script acting as a Library function to Check-In Screen line
 level LP validation
 */
function ebiznet_LPRange_CL(fld)
{	

	//    if (fld == "custpage_chkn_lp") {
	// alert(fld)
	// var boolStatus= validateQuantity(fld);
	// if(boolStatus)
	var ctx = nlapiGetContext();
	var overreceiptPreference = ctx.getPreference('OVERRECEIPTS');
	// {
	var lpno = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_chkn_lp');

	var rcvqty = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_chkn_rcvngqty');
	var chkqty = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_chkn_qty');
	var itmid = nlapiGetCurrentLineItemValue('custpage_items','custpage_iteminternalid');
	var actPoOverage = nlapiGetCurrentLineItemValue('custpage_items','custpage_pooverage');
	var Checkedin = nlapiGetFieldValue('custpage_checkin_quan');
	var Qty = nlapiGetFieldValue('custpage_item_quan');
	var chknQty = nlapiGetCurrentLineItemValue('custpage_items','custpage_pochkn_qty');// Case # 20148626
//	var recommendedQuantity = nlapiGetFieldValue('custpage_itemrecmd_qty_withpooverage');
//	var totRecommendedQuantity = parseInt(Checkedin)+parseInt(recommendedQuantity);
	var recommendedQuantity = nlapiGetFieldValue('custpage_recommended_quan');
	var totRecommendedQuantity = nlapiGetFieldValue('custpage_itemrecmd_qty_withpooverage');
	//var totCheckedin = parseInt(rcvqty)+parseInt(Checkedin);
//	var quantity=parseInt(Qty)-parseInt(Checkedin);
	var quantity=parseInt(recommendedQuantity);
	var totalLineCnt = nlapiGetLineItemCount('custpage_items');
	//Case # 20126576,20126832 Start
	//totalrecommentedqty is not declared here in this branch 
	var totalrecommentedqty=parseFloat(recommendedQuantity)+parseFloat(Checkedin);
	//Case # 20126576,20126832 end
	var totalLineQty ="";
	var type = "PALT";
	var vargetlpno = lpno;
	var vResult = "";
	var PoOverageVal = 0;
	var lineno ="";
	var count = 0;
	var lpnonew='';
	var lpnonew1='';
	var lotbatch=nlapiGetCurrentLineItemValue('custpage_items','custpage_lotbatch');
	var loc = nlapiGetFieldValue('custpage_loc_id');

	
	lineno = nlapiGetCurrentLineItemValue('custpage_items','custpage_chkn_line');
	var poID = nlapiGetFieldValue('custpage_po');
	var poName = '';
	var vsearchrecord=GetPOcheckinqty(poName,lineno,itmid,poID);
	var checkinqty = '';
	var vputgenQuantity = '';
	if(vsearchrecord!=null && vsearchrecord!='')
	{
		checkinqty=vsearchrecord[0].getValue('custrecord_orderlinedetails_checkin_qty');
		vputgenQuantity = vsearchrecord[0].getValue('custrecord_orderlinedetails_putgen_qty');
	}

//	alert("Qty" + Qty);
	//alert("vputgenQuantity" + vputgenQuantity);
	if(actPoOverage > 0)
	{
		if(parseFloat(Qty) == parseFloat(vputgenQuantity))
		{
			alert('Ordered quantity received completely');
			return false;
		}
	}
	//Case# 20148385 starts
	var itemFilters = new Array();
	//alert('itmid '+itmid);
	//alert('loc '+loc);
	itemFilters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itmid));
	itemFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	//itemFilters.push(new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'anyof', ['3','2','1']));//3=Pallet
	if(loc!=null&&loc!='')
		itemFilters.push(new nlobjSearchFilter('custrecord_ebizsiteskudim',null,'anyof',['@NONE@',loc]));
	var itemColumns = new Array();
	itemColumns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	itemColumns[1] = new nlobjSearchColumn('custrecord_ebizsiteskudim');
	itemColumns[2] = new nlobjSearchColumn('custrecord_ebizcompanyskudimension');
	itemColumns[3] = new nlobjSearchColumn('custrecord_ebizqty'); 
	itemColumns[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim'); 	
	itemColumns[4].setSort(true);
	var itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, itemFilters, itemColumns);
	if(itemSearchResults==null || itemSearchResults=='' || itemSearchResults=='null')
	{
		alert('Item Dimensions are not match with Location');
		return false;
	}
	/*var fields = ['recordType', 'custitem_ebizserialin','custitem_ebizbatchlot'];
	var columns = nlapiLookupField('item', itmid, fields);
	var ItemType = columns.recordType;
	var batchflag="F";
	batchflag= columns.custitem_ebizbatchlot;
	//alert('ItemType :' +ItemType);
	if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflag=="T" )
		{
	var itemFilters = new Array();

	itemFilters.push(new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', lotbatch));
	itemFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	if(loc!=null&&loc!='')
		itemFilters.push(new nlobjSearchFilter('custrecord_ebizsitebatch',null,'anyof',['@NONE@',loc]));
	var itemColumns = new Array();
	itemColumns[0] = new nlobjSearchColumn('custrecord_ebizlotbatch');
	itemColumns[1] = new nlobjSearchColumn('custrecord_ebizsitebatch');
	itemColumns[2] = new nlobjSearchColumn('custrecord_ebizsku'); 

	var itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, itemFilters, itemColumns);
	if(itemSearchResults==null || itemSearchResults=='' || itemSearchResults=='null')
	{
		alert('This is an inactive lot');
		return false;
	}

		}*/
	// Case# 20148385 ends
	//case#20127061 stars
	//case# 20127129 starts (check for tantype)
	var PONum = nlapiGetFieldValue('custpage_po');
	// srikanth added
	/*var trantype = nlapiLookupField('transaction', PONum, 'recordType');

	if(trantype!='transferorder')	
	{*/
	//Case # 20127157�Start
	if (parseFloat(recommendedQuantity)<=0){
		alert("Checkin Already Completed");
		return false;	
	} // for overage if condition close
	// } // srikanth added for remove the if condion of trantype 
	//case # 20127061 ends

	//case# 20127129 end
	//Case # 20127157�end
	if(loc == null || loc == '')
	{
		alert('Please Select Location');
		return false;
	}
	if(lotbatch!=null && lotbatch!='')
	{
		var res= /\s/g.test(lotbatch);
		if(res==true)
		{
			alert("Please Check the lot For Spaces");
			return false;
		}

	}

	var GridLine = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_chkn_line');
	var vCurrentLineIndex= nlapiGetCurrentLineItemIndex('custpage_items');

	var loc = nlapiGetFieldValue('custpage_loc_id');



	//case # 20126362. Getting Line level location to check with Lp Range

	var lineloc = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_polinelocation');

	//alert('lineloc' + lineloc);
	//alert('loc' + loc);
	if(lineloc!=null && lineloc!='')
		loc=lineloc;

	//End



	for (var s = 1; s <= totalLineCnt; s++) {
		if(parseFloat(vCurrentLineIndex) != parseFloat(s))
		{
			if (totalLineQty == "") 
			{
				totalLineQty = parseFloat(nlapiGetLineItemValue('custpage_items', 'custpage_chkn_rcvngqty', s));
			}
			else 
			{
				totalLineQty = parseFloat(totalLineQty) + parseFloat(nlapiGetLineItemValue('custpage_items', 'custpage_chkn_rcvngqty', s));
			}		
		}
	}
	for (var s1 = 1; s1 <= totalLineCnt; s1++) 
	{
		
		var prevlpno = nlapiGetLineItemValue('custpage_items', 'custpage_chkn_lp',s1);
		if (rcvqty>0) {

			var totalCHKDQty ="";
			if(totalLineQty=="")
			{

				totalCHKDQty = parseFloat(Checkedin) + parseFloat(rcvqty);	
			}
			else
			{

				totalCHKDQty = parseFloat(Checkedin) + parseFloat(totalLineQty) + parseFloat(rcvqty);
			}
			
			//alert('totalCHKDQty:' + totalCHKDQty);
		//	alert('Qty' + Qty);
			if(parseFloat(totalCHKDQty) <=parseFloat(Qty))
			{
				if(prevlpno!=lpno)
				{
					totalCHKDQty = 0;
				}
			}
		}
		else if(rcvqty == "")
		{
			alert('Please Enter Receive Qty');
			return false;
		}//Case # 20126677 Start
		else if(rcvqty < 0 || rcvqty == 0 )
		{
			alert('Please Enter Valid Receive Qty');
			return false;
		}
		//Case # 20126677 End

	//If overage is allowed then qty should not be greater than recommended qty.
	//alert('actPoOverageVal:'+actPoOverage);
	if(actPoOverage > 0)
	{
//		PoOverageVal = parseInt(recommendedQuantity)-parseInt(quantity);
		PoOverageVal = parseFloat(totRecommendedQuantity)-parseFloat(quantity);
	}
	//alert('PoOverageVal:'+PoOverageVal);
	//alert('totalCHKDQty:'+totalCHKDQty);
	//alert('Qty:'+Qty);
	if (overreceiptPreference == "F"){  //If overage is not allowed then qty should not be greater than orderd qty.
		if (parseFloat(totalCHKDQty) <= parseFloat(Qty)) {
			//return true;
		}
		else {
			alert('PO Overage is not allowed. Please change the quantity.');
			return false;
		}
	}
	else{ //If overage is allowed then qty should not be greater than recommended qty.
		//alert(parseInt(PoOverageVal));
		//Case # 20126158� Start
		if(parseFloat(actPoOverage) == 0){
			//Case # 20126158� End
			if(parseFloat(totalCHKDQty) > parseFloat(totalrecommentedqty)){
				alert('QUANTITY EXCEEDS OVERAGE LIMIT');
				return false;
			}
			if (parseFloat(totalCHKDQty) <= parseFloat(Qty)) {
				//return true;
			}
			else{
				alert('PO Overage is not allowed. Please change the quantity.');
				return false;
			}
		}
		else{

			//	for (var s1 = 1; s1 <= totalLineCnt; s1++){

			if(parseFloat(totalCHKDQty) >parseFloat(totalrecommentedqty)){
				alert('Quantity exceeds overage limit.');
				return false;
			}
			//else 
			if(isNaN(rcvqty) == true)
			{
				alert('Please enter the receive quantity in number format');		
				return false;
			}
			else if(rcvqty < 0)
			{
				alert('Receive Quantity cannot be less than zero');		
				return false;
			}
			else if(rcvqty == 0)
			{
				alert('Receive Quantity cannot be 0');		
				return false;
			}

			//}
		}

		}
	}

	if (vargetlpno.length > 0) {

		//Code Add for Checking Qty from SKU Dimensions Record Set by Ramana on 27th May 2011  


		var vitem =  nlapiGetCurrentLineItemValue('custpage_items','custpage_chkn_sku'); 
		var chkQty = nlapiGetCurrentLineItemValue('custpage_items','custpage_chkn_rcvngqty');
		var pCode = nlapiGetCurrentLineItemValue('custpage_items','custpage_chkn_pc'); 


		var vUOMLevel=3; // this is internal id for pallet in UOM LOV value
		var itemQty = 0;
		var vUOM = "PALLET";

		var filters = new Array();          
		filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is',itmid);
//		filters[1] = new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'is',parseInt(vUOMLevel));		  
		filters[1] = new nlobjSearchFilter('custrecord_ebizpackcodeskudim', null, 'is',pCode);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizqty');
		//Added by suman on 19-01-12
		columns[1] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim').setSort(true);//it fetches the records with highest uomlevel first. 

		var searchskuresults = nlapiSearchRecord('customrecord_ebiznet_skudims',null, filters, columns);
		//Case# 201411182 starts
		if(searchskuresults == null || searchskuresults =='null')
		{
			alert("Item Dimensions are not Configured for Item "+vitem);
			return false;
		}
		//Case# 201411182 ends
		if(searchskuresults!=null)
		{ 
			itemQty = searchskuresults[0].getValue('custrecord_ebizqty');  
		}    	
		//alert("itemQty :"+itemQty);
		//alert("chkQty :"+chkQty);
		if(parseInt(chkQty)>parseInt(itemQty))
		{
			alert("Qty is greater than Pallet Qty");
			return false;    		
		}    	


		//upto to here for checking SKU Dimensions Ramana  on 27th May 2011

		//Added by Ramana on May 30 2011 for overage qty purpose

		/*
    	  var PONum = nlapiGetFieldValue('custpage_po'); 


    	    var posearchresults = nlapiLoadRecord('purchaseorder', PONum); //1020
            var vponum= posearchresults.getFieldValue('tranid');

            //alert(vponum);

    	  var Qtyfilters = new Array();          
    	  Qtyfilters[0] = new nlobjSearchFilter('custrecord_orderlinedetails_order_no', null, 'is',vponum);
    	  var Qtycolumns = new Array();
    	  Qtycolumns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty');

    	  var searchQtyresults = nlapiSearchRecord('customrecord_ebiznet_order_line_details',null, Qtyfilters, Qtycolumns);
    	  var PutwQty = 0;
	      if(searchQtyresults!=null)
	      {    		
	      		PutwQty = searchQtyresults[0].getValue('custrecord_orderlinedetails_checkin_qty');  
	      }    	

	     // alert("PutwQty"+PutwQty);
	    //  alert("chkQty"+chkQty);
    	  if(PutwQty>=chkQty)
    	  {
    		  alert('PO Overage is not allowed Change the quantity');
			  return false;  	
    	  }
		 */
		//upto to here on May 30 2011

		//var searchresults = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, null, new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix'));
		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix');
		column[1]=new nlobjSearchColumn('custrecord_ebiznet_lprange_begin');
		column[2]=new nlobjSearchColumn('custrecord_ebiznet_lprange_end');
		column[3]=new nlobjSearchColumn('custrecord_ebiznet_lprange_lpgentype');
		column[4]=new nlobjSearchColumn('custrecord_ebiznet_lprange_lptype');
		column[5]=new nlobjSearchColumn('custrecord_ebiznet_lprange_site');





		var lpRangefilters = new Array();
		lpRangefilters[0] = new nlobjSearchFilter('custrecord_ebiznet_lprange_site', null, 'anyof', loc);

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, lpRangefilters, column);

		if (searchresults) {
			for (var i = 0; i < Math.min(50, searchresults.length); i++) {
				try {
					var getLPPrefix = searchresults[i].getValue('custrecord_ebiznet_lprange_lpprefix');


					var recid = searchresults[i].getId();

					//var transaction = nlapiLoadRecord('customrecord_ebiznet_lp_range', recid);

					//	var varBeginLPRange = transaction.getFieldValue('custrecord_ebiznet_lprange_begin');

					//	var varEndRange = transaction.getFieldValue('custrecord_ebiznet_lprange_end');

					//	var getLPGenerationTypeValue = transaction.getFieldValue('custrecord_ebiznet_lprange_lpgentype');

					//	var getLPTypeValue = transaction.getFieldValue('custrecord_ebiznet_lprange_lptype');

					var varBeginLPRange = searchresults[i].getValue('custrecord_ebiznet_lprange_begin');

					var varEndRange =searchresults[i].getValue('custrecord_ebiznet_lprange_end');

					var getLPGenerationTypeValue = searchresults[i].getValue('custrecord_ebiznet_lprange_lpgentype');

					var getLPTypeValue = searchresults[i].getValue('custrecord_ebiznet_lprange_lptype');

					var lploc;

					//case 20125986 Start :added condition for LP generation type = "userdefined"


					if (getLPTypeValue == "1" && getLPGenerationTypeValue == "2") {
						//var getLPrefix = (searchresults[i].getValue('custrecord_ebiznet_lprange_lpprefix')).toUpperCase();
//						case 20123561 start

						
						var getLPrefix = searchresults[i].getValue('custrecord_ebiznet_lprange_lpprefix');
						
						//alert('getLPrefix:'  + getLPrefix);
						
						if(getLPrefix!=null && getLPrefix!='')
						{
							getLPrefix = getLPrefix.toUpperCase();
						}
						if(getLPrefix == null || getLPrefix == '')
							getLPrefix=0;

						if(getLPrefix != null && getLPrefix != '' && getLPrefix != 0)
						{
							var LPprefixlen = getLPrefix.length;
							var vLPLen = vargetlpno.substring(0, LPprefixlen).toUpperCase();

							if(vLPLen==lpno)
							{
								alert('PLs give no along with LpPrefix');
								return false;
							}
						}
						else
						{
							var LPprefixlen = 0;
							var vLPLen = 0;
						}


						var columnlpprefix=new Array();

						columnlpprefix[0]=new nlobjSearchColumn('custrecord_ebiznet_lprange_site');

						var lpprefixfilters = new Array();
						if(getLPrefix!=null && getLPrefix!='')
						lpprefixfilters[0] = new nlobjSearchFilter('custrecord_ebiznet_lprange_lpprefix', null, 'is', getLPrefix);

						var searchresultsprefixloc = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, lpprefixfilters, columnlpprefix);
						if(searchresultsprefixloc!=null && searchresultsprefixloc!='')
						{
							lploc=searchresults[0].getValue('custrecord_ebiznet_lprange_site');
						}

//						case 20123561 end

						if(lploc!=loc)
						{
							vResult = "N";
							break;
						}

						if (vLPLen == getLPrefix) {

							var varnum = vargetlpno.substring(LPprefixlen, vargetlpno.length);

							if (varnum.length > varEndRange.length) {

								vResult = "N";
								break;
							}


							if ((parseInt(varnum,10) < parseInt(varBeginLPRange)) || (parseInt(varnum,10) > parseInt(varEndRange)) || (isNaN(varnum))) {

								vResult = "N";
								break;
							}
							else {
								vResult = "Y";
								break;
							}
						}
						else {
							vResult = "N";
						}

					} //end of if statement
					else {
						//alert("in else");
					}

				} 
				catch (err) {
					// alert("exception" + err + "value is ");

				}
			} //end of for loop
		}



		if (vResult == "Y") {

			var filters1 = new Array();				
			//filters1[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lpno);
			filters1[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lpno);

			var columns1 = new Array();
			columns1[0] = new nlobjSearchColumn('name');

			var searchresults1 = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters1,columns1);		
			if(searchresults1!=null &&  searchresults1.length>0)
			{
				alert('LP Already Exists');
				return false;
			}
			//alert('LP is with in the range of numbers');
			var fields = ['recordType', 'custitem_ebizserialin','custitem_ebizbatchlot'];
			var columns = nlapiLookupField('item', itmid, fields);
			var ItemType = columns.recordType;	
			var serialInflg="F";		
			serialInflg = columns.custitem_ebizserialin;
			var batchflag="F";
			batchflag= columns.custitem_ebizbatchlot;

			if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") {

				var newqty = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_chkn_rcvngqty');
				
				if(parseInt(newqty) != newqty)
				{
					alert("For Serial item ,Decimal quantities not allowed");
					nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_invlocmove','F');
					return false;
				}
				
				//Case # 20127194 Start
				//Case # 20127485 Start
				var trantype = nlapiLookupField('transaction', nlapiGetFieldValue('custpage_po'), 'recordType');
				var filterSerialEntry=new Array();
				if(trantype=='purchaseorder'||trantype=='transferorder')
				{
					filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialitem',null,'anyof',nlapiGetCurrentLineItemValue('custpage_items','custpage_iteminternalid')));
					filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialebizpono',null,'is',nlapiGetFieldValue('custpage_po')));
					filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialpolineno',null,'is',nlapiGetCurrentLineItemValue('custpage_items','custpage_chkn_line')));
					filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialparentid',null,'is',nlapiGetCurrentLineItemValue('custpage_items','custpage_chkn_lp')));// Case # 20148626
				}
				else 
				{
					filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialitem',null,'anyof',nlapiGetCurrentLineItemValue('custpage_items','custpage_iteminternalid')));
					filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialebizrmano',null,'is',nlapiGetFieldValue('custpage_po')));
					filterSerialEntry.push(new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno',null,'is',nlapiGetCurrentLineItemValue('custpage_items','custpage_chkn_line')));
					filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialparentid',null,'is',nlapiGetCurrentLineItemValue('custpage_items','custpage_chkn_lp')));
				}
				filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialwmsstatus',null,'anyof',1));
				//Case # 20127485 End
				var SearchRec=nlapiSearchRecord('customrecord_ebiznetserialentry',null,filterSerialEntry,null);

//				var vserialNo=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_serialnos');
//				alert('vserailNo'+vserialNo);
				if(SearchRec!=null&&SearchRec!="")
				{
					// Case# 20148053 starts
					//	alert('chknQty'+chknQty)
					//	alert('SearchRec.length'+SearchRec.length);
					//if(Qty > SearchRec.length )// Case# 20148626

					var checktotalqty = Math.floor(parseFloat(chknQty));
					//alert('checktotalqty'+checktotalqty);
					if(checktotalqty > SearchRec.length )
					{
						alert("Please enter value(s)for: Serial Items in Serial Entry");
						var linkURL = nlapiResolveURL('SUITELET', 'customscript_serialnumconfigsuitelet', 'customdeploy_serialnumberconfig');
						linkURL = linkURL + '&custparam_serialpoid=' + nlapiGetFieldValue('custpage_po');
						linkURL = linkURL + '&custparam_serialskuid=' + nlapiGetFieldValue('custpage_line_item');
						linkURL = linkURL + '&custparam_serialskulineno=' + nlapiGetFieldValue('custpage_line_itemlineno');
						linkURL = linkURL + '&custparam_serialskulp=' + nlapiGetCurrentLineItemValue('custpage_items', 'custpage_chkn_lp');
						linkURL = linkURL + '&custparam_serialskuchknqty=' + nlapiGetCurrentLineItemValue('custpage_items', 'custpage_chkn_rcvngqty');
						window.open(linkURL);
						return false;
					}
					// Case# 20148053 ends
					return true;
				}
				else
				{
					alert("Please enter value(s)for: Serial Items in Serial Entry");

					var linkURL = nlapiResolveURL('SUITELET', 'customscript_serialnumconfigsuitelet', 'customdeploy_serialnumberconfig');

					linkURL = linkURL + '&custparam_serialpoid=' + nlapiGetFieldValue('custpage_po');
					linkURL = linkURL + '&custparam_serialskuid=' + nlapiGetFieldValue('custpage_line_item');
					linkURL = linkURL + '&custparam_serialskulineno=' + nlapiGetFieldValue('custpage_line_itemlineno');
					linkURL = linkURL + '&custparam_serialskulp=' + nlapiGetCurrentLineItemValue('custpage_items', 'custpage_chkn_lp');
					linkURL = linkURL + '&custparam_serialskuchknqty=' + nlapiGetCurrentLineItemValue('custpage_items', 'custpage_chkn_rcvngqty');
					window.open(linkURL);
					return false;
				}
				//Case # 20127194 End
			}
			else if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflag=="T" )
				//    lotnumberedinventoryitem

			{

				try {
					var ChknBatchNo = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_lotbatch');
					//alert("ChknBatchNo"+ChknBatchNo);
					if (ChknBatchNo) {
						var POBatchNo = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_serialno');

						/*
alert(" POBatchNo " + POBatchNo);
						var poId = nlapiGetFieldValue('custpage_po');
						if (POBatchNo!=null)
						{
							if (POBatchNo == ChknBatchNo) {
								boolFlag = "T";
								alert("boolFlag" + boolFlag);
							}
						}
						 */

						//Check in Batch Custom Records.
						var batchCustRecChk = BatchCustRecordChk(ChknBatchNo);
						if (batchCustRecChk) {
							//	 alert("Inside batchCustRecChk" + batchCustRecChk);
							return batchCustRecChk;
						}
						else {
							var PONum = nlapiGetFieldValue('custpage_po');
							if(PONum !=null && PONum!='' )
							{
								trantype = nlapiLookupField('transaction', PONum, 'recordType');

								if(trantype!='transferorder')
								{
									//alert("Please enter value(s)for: Lot\Batch Items in Lot\Batch Entry");
									// case no 20126190 
									var CapExpiryDate = nlapiGetCurrentLineItemValue('custpage_items','custpage_hdncatchexpdate');
									var vSKUID = nlapiGetCurrentLineItemValue('custpage_items','custpage_iteminternalid');
									var  linkURL = nlapiResolveURL('RECORD', 'customrecord_ebiznet_batch_entry');
									linkURL = linkURL + '&custparam_serialskuid='+ nlapiGetCurrentLineItemValue('custpage_items','custpage_iteminternalid');				
									//linkURL = linkURL + '&custparam_locationid='+ nlapiGetCurrentLineItemValue('custpage_items','custpage_location');
									linkURL = linkURL + '&custparam_locationid='+ nlapiGetFieldValue('custpage_loc_id');
									linkURL = linkURL + '&custparam_lot='+ nlapiGetCurrentLineItemValue('custpage_items','custpage_lotbatch');
									linkURL = linkURL + '&custparam_name='+ nlapiGetCurrentLineItemValue('custpage_items','custpage_lotbatch');
									var Itype = nlapiLookupField('item', vSKUID, 'recordType');

									var itemRecord = nlapiLoadRecord(Itype, vSKUID);
									var itemShelflife = itemRecord.getFieldValue('custitem_ebiz_item_shelf_life');
									var d = new Date();
									var vExpiryDate='';
									if(itemShelflife !=null && itemShelflife!='')
									{
										//Case # 20126458 Start
										var ctx = nlapiGetContext();
										var setpreferencesdateformate = ctx.getPreference('DATEFORMAT');

										d.setDate(d.getDate()+parseInt(itemShelflife));
										nlapiLogExecution('DEBUG', 'setpreferencesdateformate',setpreferencesdateformate);
										if(setpreferencesdateformate=='DD/MM/YYYY' || setpreferencesdateformate=='DD-MM-YYYY')
										{
											vExpiryDate=((d.getDate())+"/"+(d.getMonth()+1)+"/"+(d.getFullYear()));

										}
										else
										{
											vExpiryDate=((d.getMonth()+1)+"/"+(d.getDate())+"/"+(d.getFullYear()));
										}
										//Case # 20126458 End
									}
									//case# 201410440 (when capture expiry date is enable then we are considering the entered expiry date)
									else if(CapExpiryDate!=null && CapExpiryDate!='' && CapExpiryDate !='null')
									{
										var newexpirydate1=CapExpiryDate.substring(0,2);
										var newexpirydate2=CapExpiryDate.substring(2,4);
										var newexpirydate3=CapExpiryDate.substring(4,8);

										vExpiryDate=newexpirydate1+"/"+newexpirydate2+"/"+newexpirydate3;	
									}
									else
									{
										vExpiryDate='01/01/2099';										     
									}
									nlapiLogExecution('DEBUG', 'vExpiryDate',vExpiryDate);

									linkURL = linkURL + '&custparam_expdate='+ vExpiryDate;
									//var linkURL = "https://system.netsuite.com/app/common/custom/custrecordentry.nl?rectype=74";
									/*var ctx = nlapiGetContext();
							if (ctx.getEnvironment() == 'PRODUCTION') {
								//linkURL = 'https://system.netsuite.com' + linkURL;			
							}
							else 
								if (ctx.getEnvironment() == 'SANDBOX') {
									//linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
								}*/
									window.open(linkURL);
								}
								else
									return false;
							}

							//alert("Please enter valid value(s)for: LOT# Items in LOT# Entry");
							alert("Enter the valid lot in the Batch Entry screen opened in the Next tab");
							return false;
						}

					}
					else
					{
						alert("Please enter value(s)for: Lot# Items in Lot# Entry");
						return false;
					}
				}
				catch(exps)
				{
					alert("Error "+exps);
				}
			}
			else
			{

				var ChknBatchNo = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_lotbatch');
				if(ChknBatchNo!=null && ChknBatchNo!='')
				{
					nlapiSetCurrentLineItemValue('custpage_items','custpage_lotbatch','',false,false);
					alert("This is not a lot Item");

					return false;
				}
				var serialno=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_serialnos');
				if(serialno!=null && serialno!='')
				{
					nlapiSetCurrentLineItemValue('custpage_items','custpage_serialnos','',false,false);
					alert("This is not a Serial Item");

					return false;
				}
			}

			var lineArray = new Array();
			var lpnoArry = new Array();

			var vlpcnt=0;
			for(var b=1;b<=totalLineCnt;b++)
			{
				var linelpno = nlapiGetLineItemValue('custpage_items', 'custpage_chkn_lp', b);
				var sublistlineno = nlapiGetLineItemValue('custpage_items', 'custpage_chkn_line', b);
				
				var SublistIndex = nlapiGetCurrentLineItemIndex('custpage_items');

				/*alert('SublistIndex '+SublistIndex);
				alert('b '+b);
				alert('lpno '+lpno);
					alert('linelpno '+linelpno);*/
				if((linelpno==lpno) && (b!=SublistIndex))
				{
					vlpcnt++;
				}

				//	lineArray.push(sublistlineno);


				//lpnoArry.push(linelpno);
				//alert('b '+b);
				//alert('lpno '+lpno);
				//	alert('lpnoArry '+lpnoArry);
				//alert('lineArray.indexOf(GridLine) '+lineArray.indexOf(GridLine));

//				if(lineArray.indexOf(GridLine)!=-1)
//				{
//				if(lpnoArry.indexOf(lpno)!=-1)
//				{
//				alert('Duplicate LP# is not allowed');
//				nlapiSetCurrentLineItemValue('custpage_items','custpage_chkn_lp','');
//				//nlapiSetLineItemValue('custpage_items','custpage_chkn_lp',b,' ');
//				return false;
//				}
//				}
			}
		//	alert('vlpcnt '+vlpcnt);
			if(vlpcnt>0)
			{
				alert('Duplicate LP# is not allowed');
				nlapiSetCurrentLineItemValue('custpage_items','custpage_chkn_lp','');
				return false;
			}




			var receiveQuantity = 0;
			var pickfaceQuantity = 0;
			var enteredPickfaceQuantity = 0;
			var remainingPickfaceQuantity = 0;
			var totalLineCnt = nlapiGetLineItemCount('custpage_items');
			//alert("Hi");
			//alert(totalLineCnt);

			enteredPickfaceQuantity = parseInt(nlapiGetCurrentLineItemValue('custpage_items', 'custpage_chkn_rcvngqty'));
			pickfaceQuantity = nlapiGetFieldValue('custpage_priority_quan');
			if(parseInt(pickfaceQuantity) != 0)
			{
				for (var s = 1; s <= totalLineCnt; s++) 
				{				
					if(parseInt(nlapiGetLineItemValue('custpage_items', 'custpage_chkn_rcvngqty',s)) <= parseInt(pickfaceQuantity))
					{
						receiveQuantity = parseInt(receiveQuantity) +  parseInt(nlapiGetLineItemValue('custpage_items', 'custpage_chkn_rcvngqty',s));
						pickfaceQuantity = parseInt(pickfaceQuantity) - parseInt(nlapiGetLineItemValue('custpage_items', 'custpage_chkn_rcvngqty',s));
					}					
				}

				//if(enteredPickfaceQuantity <= pickfaceQuantity)
				//{
				//enteredPickfaceQuantity = parseInt(receiveQuantity) +  parseInt(enteredPickfaceQuantity);
				//	pickfaceQuantity = parseInt(pickfaceQuantity) - parseInt(enteredPickfaceQuantity);
				//}	

				if(parseInt(pickfaceQuantity)<parseInt(enteredPickfaceQuantity))
				{
					if(confirm("Do You Want To Move Total Qty To Bulk Bin Location.?"))
					{
						nlapiSetCurrentLineItemValue('custpage_items','custpage_chkn_pfvalidateflag',"Y");
						//alert(nlapiGetCurrentLineItemValue('custpage_items', 'custpage_chkn_pfvalidateflag'));
						return true;
					}
					else
					{
						nlapiSetCurrentLineItemValue('custpage_items','custpage_chkn_pfvalidateflag',"");
						return false;
					}
				}
				else
				{
					return true;
				}
			}
			else
			{
				return true;
			}
		}
		else {
			alert('Invalid LP Range!!!');
			return false;
		}
	}
	else {
		alert('Invalid LP No');
		return false;
	}
	// }
	//   else 
	//	{
	//		alert("Overages ");
	//		 return false;
	//	}






	//alert("hellio");
	// alert(nlapiGetCurrentLineItemValue('custpage_items', 'custpage_chkn_rcvngqty'));
	// alert("bye");
	//alert(nlapiGetCurrentLineItemValue('custpage_items', 'custpage_checkin_quan'));
	//nlapiSetCurrentLineItemValue('custpage_items','custpage_checkin_quan',parseInt(nlapiGetLineItemValue('custpage_items', 'custpage_chkn_rcvngqty')),true,true);
	//alert(nlapiGetCurrentLineItemValue('custpage_items', 'custpage_checkin_quan'));

}


function ebiznet_LPRangePutw_CL(fld){

	//    if (fld == "custpage_chkn_lp") {


	var lpno = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_chkn_lp');
	var Oldlpno = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_oldlp');
	var Displayedqty = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_displayedquantity');
	var Putqty = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_quantity');
	var loc = nlapiGetFieldValue('custpage_loc_id');


	//alert("Displayedqty::"+Displayedqty);
	//alert("Putqty::"+Putqty);
	// Case# 20148592 starts
	if(parseInt(Putqty)<= 0)
	{
		alert("Qty should be greater than 0");
		return false;
	}
	// Case# 20148592 ends
	if(parseInt(Putqty)>parseInt(Displayedqty))
	{
		alert("Please enter correct put quantity");
		return false;
	}

	if(isNaN(Putqty) == true) // case# 201411362
	{
		alert('Please enter the quantity in number format');		
		return false;
	}
	//case 20125985 start : validating binlocation for site
	var lineLoc = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_location');
	//alert(loc);
	//alert(lineLoc);
	if(lineLoc !=null && lineLoc !='')
	{
		var filterLoc = new Array();
		filterLoc.push(new nlobjSearchFilter('internalid',null,'anyof',lineLoc));
		filterLoc.push(new nlobjSearchFilter('custrecord_ebizsitelocf',null,'anyof',loc));
		//Case# 20125980�start
		filterLoc.push(new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'anyof', ['6','7']));
		//Case# 20125980�End
		var SearchRec=nlapiSearchRecord('customrecord_ebiznet_location',null,filterLoc,null);
		if(SearchRec == null || SearchRec == '')
		{
			alert('Select valid Bin Location');
			return false;
		}

	}
	//case 20125985 end
	//var itmid = nlapiGetFieldValue('custpage_line_item');
	var itmid = nlapiGetCurrentLineItemValue('custpage_items','custpage_iteminternalid');

	var type = "PALT";
	var vargetlpno = lpno;
	var vResult = "";

	if (vargetlpno.length > 0) {

		if(Oldlpno!=lpno){
//			var searchresults = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, null, new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix'));
			var column=new Array();
			column[0]=new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix');
			//var searchresults = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, null, column);
			// case no 20126461
			var lpRangefilters = new Array();
			lpRangefilters[0] = new nlobjSearchFilter('custrecord_ebiznet_lprange_site', null, 'anyof', loc);

			var searchresults = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, lpRangefilters, column);

			if (searchresults) {
				for (var i = 0; i < Math.min(50, searchresults.length); i++) {
					try {
						var getLPPrefix = searchresults[i].getValue('custrecord_ebiznet_lprange_lpprefix');

						var recid = searchresults[i].getId();

						// alert("recid::"+recid);

						var transaction = nlapiLoadRecord('customrecord_ebiznet_lp_range', recid);

						var varBeginLPRange = transaction.getFieldValue('custrecord_ebiznet_lprange_begin');

						var varEndRange = transaction.getFieldValue('custrecord_ebiznet_lprange_end');

						var getLPGenerationTypeValue = transaction.getFieldValue('custrecord_ebiznet_lprange_lpgentype');

						var getLPTypeValue = transaction.getFieldValue('custrecord_ebiznet_lprange_lptype');



						if (getLPTypeValue == "1") {
							var getLPrefix = (transaction.getFieldValue('custrecord_ebiznet_lprange_lpprefix')).toUpperCase();

							var LPprefixlen = getLPrefix.length;
							var vLPLen = vargetlpno.substring(0, LPprefixlen).toUpperCase();


							if (vLPLen == getLPrefix) {

								var varnum = vargetlpno.substring(LPprefixlen, vargetlpno.length);

								if (varnum.length > varEndRange.length) {

									vResult = "N";
									break;
								}


								if ((parseInt(varnum,10) < parseInt(varBeginLPRange)) || (parseInt(varnum,10) > parseInt(varEndRange)) || (isNaN(varnum))) {

									vResult = "N";
									break;
								}
								else {
									vResult = "Y";
									break;
								}
							}
							else {
								vResult = "N";
							}

						} //end of if statement
						else {
							//alert("in else");
						}

					} 
					catch (err) {
						// alert("exception" + err + "value is ");

					}
				} //end of for loop
			}
		}
		else
			vResult = "Y";
		//  alert("vResult::"+vResult);

		if (vResult == "Y") {

			//alert('LP is with in the range of numbers');
			var fields = ['recordType', 'custitem_ebizserialin','custitem_ebizbatchlot'];
			var columns = nlapiLookupField('item', itmid, fields);
			var ItemType = columns.recordType;	
			var serialInflg="F";		
			serialInflg = columns.custitem_ebizserialin;
			var batchflag="F";
			batchflag= columns.custitem_ebizbatchlot;           

			//alert("batchflag::"+batchflag);
			// alert("ItemType::"+ItemType);
			//case # 20124476�Start
			var vserialNo=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_serialnos');
			var vOrdQty = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_quantity');
			//alert("vserialNo::"+vserialNo);
			var ChknBatchNo1 = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_lotbatch');
		
			if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") {
				
				if(ChknBatchNo1!=null && ChknBatchNo1!='')
				{
					nlapiSetCurrentLineItemValue('custpage_items','custpage_lotbatch','',false,false);
					alert('Lot numbers are not allowed for Serial Item');
					return false;
				}
				if(vserialNo=='' || vserialNo==null)
				{
					//alert("Please enter value(s)for: Serial Items in Serial Entry");// Case # 20148651
					var filterSerialEntry=new Array();
					filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialitem',null,'anyof',nlapiGetCurrentLineItemValue('custpage_items','custpage_iteminternalid')));
					filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialebizpono',null,'is',nlapiGetCurrentLineItemValue('custpage_items','custpage_pointid')));
					filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialpolineno',null,'is',nlapiGetCurrentLineItemValue('custpage_items','custpage_line')));
					filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialparentid',null,'is',nlapiGetCurrentLineItemValue('custpage_items','custpage_chkn_lp')));// Case # 20148651 
					filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialwmsstatus',null,'anyof',1));

					var SearchRec=nlapiSearchRecord('customrecord_ebiznetserialentry',null,filterSerialEntry,null);

//					var vserialNo=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_serialnos');
//					alert('vserailNo'+vserialNo);
					if(SearchRec!=null&&SearchRec!="")
					{
						//	var linkURL = "https://system.netsuite.com/app/site/hosting/scriptlet.nl?script=159&deploy=1";
						//var linkURL = nlapiResolveURL('SUITELET', 'customscript_lprelated_serial_no', 'customdeploy_lprelated_serial_no');// Case # 20148651
					}
					else
					{
						alert("Please enter value(s)for: Serial Items in Serial Entry"); // Case # 20148651
						var linkURL = nlapiResolveURL('SUITELET', 'customscript_serialnumconfigsuitelet', 'customdeploy_serialnumberconfig');

						linkURL = linkURL + '&custparam_serialpoid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_pointid');
						linkURL = linkURL + '&custparam_serialskuid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_iteminternalid');
						linkURL = linkURL + '&custparam_serialskulineno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_line');
						linkURL = linkURL + '&custparam_serialskulp=' + nlapiGetCurrentLineItemValue('custpage_items', 'custpage_chkn_lp');
						linkURL = linkURL + '&custparam_serialskuchknqty=' + nlapiGetCurrentLineItemValue('custpage_items', 'custpage_quantity');
						window.open(linkURL);
					}
				}
				else
				{
					if(vOrdQty!= null && vOrdQty!="")
					{
						var varr='';
						if(vserialNo!='null' && vserialNo!='' && vserialNo!=null)// Case# 20148002
							varr = vserialNo.split(",");
						if(parseFloat(vOrdQty)!= (varr.length-1))
						{
							alert("Please select " + vOrdQty + " Serial No(s)");
							var SerialURL;
							var ctx = nlapiGetContext();
							nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());

							var linkURL = nlapiResolveURL('SUITELET', 'customscript_putawaycnfmserialscan', 'customdeploy_putawaycnfmserialscan_di');
							linkURL = linkURL + '&custparam_serialskuid='+ nlapiGetCurrentLineItemValue('custpage_items','custpage_iteminternalid');					
							linkURL = linkURL + '&custparam_serialskulp=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_chkn_lp');
							linkURL = linkURL + '&custparam_serialskuchknqty=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_quantity');
							linkURL = linkURL + '&custparam_opentaskid=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_opentaskid');
							linkURL = linkURL + '&custparam_contLpno=' ,'';
							linkURL = linkURL + '&custparam_dointernno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_pointid');
							linkURL = linkURL + '&custparam_dolineno=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_line');
							//case 20125113 start :passing binlocation value to serial number screen
							linkURL = linkURL + '&custparam_binlocation=' + nlapiGetCurrentLineItemValue('custpage_items','custpage_location');
							//case 20125113 end
							window.open(linkURL);
							return false;
						}

					}
					//case # 20124476�End
				}
			}
			else if (ItemType == "lotnumberedinventoryitem" || batchflag=="T" || ItemType == "lotnumberedassemblyitem")
			{

				try {
					var ChknBatchNo = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_lotbatch');
					//alert("ChknBatchNo::"+ChknBatchNo);
					if (ChknBatchNo) {					
						var POBatchNo = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_serialno');
						//alert("POBatchNo::"+POBatchNo);

						/*
alert(" POBatchNo " + POBatchNo);
						var poId = nlapiGetFieldValue('custpage_po');
						if (POBatchNo!=null)
						{
							if (POBatchNo == ChknBatchNo) {
								boolFlag = "T";
								alert("boolFlag" + boolFlag);
							}
						}
						 */

						//Check in Batch Custom Records.

						var batchCustRecChk = BatchCustRecordChk(ChknBatchNo);
						if (batchCustRecChk) {
//							alert("Inside batchCustRecChk" + batchCustRecChk);
							return batchCustRecChk;
						}
						else {
							var PONum = nlapiGetFieldValue('custpage_po');
							if(PONum !=null && PONum!='' )
							{
								trantype = nlapiLookupField('transaction', PONum, 'recordType');

								if(trantype!='transferorder')
								{
//									alert("Please enter value(s)for: LOT# Items in LOT# Entry");
									//var linkURL = "https://system.netsuite.com/app/common/custom/custrecordentry.nl?rectype=74";
									// case no 20126190 
									var CapExpiryDate = nlapiGetCurrentLineItemValue('custpage_items','custpage_hdncatchexpdate');
									var vSKUID = nlapiGetCurrentLineItemValue('custpage_items','custpage_iteminternalid');
									var  linkURL = nlapiResolveURL('RECORD', 'customrecord_ebiznet_batch_entry');
									linkURL = linkURL + '&custparam_serialskuid='+ nlapiGetCurrentLineItemValue('custpage_items','custpage_iteminternalid');				
									//linkURL = linkURL + '&custparam_locationid='+ nlapiGetCurrentLineItemValue('custpage_items','custpage_location');
									linkURL = linkURL + '&custparam_locationid='+ nlapiGetFieldValue('custpage_loc_id');
									linkURL = linkURL + '&custparam_lot='+ nlapiGetCurrentLineItemValue('custpage_items','custpage_lotbatch');
									linkURL = linkURL + '&custparam_name='+ nlapiGetCurrentLineItemValue('custpage_items','custpage_lotbatch');


									var Itype = nlapiLookupField('item', vSKUID, 'recordType');

									var itemRecord = nlapiLoadRecord(Itype, vSKUID);
									var itemShelflife = itemRecord.getFieldValue('custitem_ebiz_item_shelf_life');
									var d = new Date();
									var vExpiryDate='';
									if(itemShelflife !=null && itemShelflife!='')
									{
										//Case # 20126458 Start
										var ctx = nlapiGetContext();
										var setpreferencesdateformate = ctx.getPreference('DATEFORMAT');

										d.setDate(d.getDate()+parseInt(itemShelflife));
										nlapiLogExecution('DEBUG', 'setpreferencesdateformate',setpreferencesdateformate);
										if(setpreferencesdateformate=='DD/MM/YYYY' || setpreferencesdateformate=='DD-MM-YYYY')
										{
											vExpiryDate=((d.getDate())+"/"+(d.getMonth()+1)+"/"+(d.getFullYear()));

										}
										else
										{
											vExpiryDate=((d.getMonth()+1)+"/"+(d.getDate())+"/"+(d.getFullYear()));
										}
										//Case # 20126458 End
									}
									//case# 201410440 (when capture expiry date is enable then we are considering the entered expiry date)
									else if(CapExpiryDate!=null && CapExpiryDate!='' && CapExpiryDate !='null')
									{
										var newexpirydate1=CapExpiryDate.substring(0,2);
										var newexpirydate2=CapExpiryDate.substring(2,4);
										var newexpirydate3=CapExpiryDate.substring(4,8);

										vExpiryDate=newexpirydate1+"/"+newexpirydate2+"/"+newexpirydate3;	
									}
									else
									{
										vExpiryDate='01/01/2099';										     
									}
									nlapiLogExecution('DEBUG', 'vExpiryDate',vExpiryDate);

									linkURL = linkURL + '&custparam_expdate='+ vExpiryDate;

									//var linkURL = "https://system.netsuite.com/app/common/custom/custrecordentry.nl?rectype=74";
									/*var ctx = nlapiGetContext();
							if (ctx.getEnvironment() == 'PRODUCTION') {
								//linkURL = 'https://system.netsuite.com' + linkURL;			
							}
							else 
								if (ctx.getEnvironment() == 'SANDBOX') {
									//linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
								}*/
									window.open(linkURL);
								}
								else
									return false;
							}
							//alert("Please enter valid value(s)for: Lot# Items in Lot# Entry");
							alert("Enter the valid lot in the Batch Entry screen opened in the Next tab");
							return false;
						}

					}
					else
					{
						alert("Please enter value(s)for: Lot# Items in Lot# Entry");
						return false;
					}
				}
				catch(exps)
				{
					alert("Error "+exps);
				}
			}
			else
			{
				var ChknBatchNo = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_lotbatch');
				if(ChknBatchNo!=null && ChknBatchNo!='')
				{
					nlapiSetCurrentLineItemValue('custpage_items','custpage_lotbatch','',false,false);
					alert("This is not a lot Item");

					return false;
				}
				var serialno=nlapiGetCurrentLineItemValue('custpage_items', 'custpage_serialnos');
				if(serialno!=null && serialno!='')
				{
					nlapiSetCurrentLineItemValue('custpage_items','custpage_serialnos','',false,false);
					alert("This is not a Serial Item");

					return false;
				}
			}

			return true;

		}
		else {
			alert('Invalid LP Range');
			return false;
		}
	}
	else {
		alert('Invalid LP No');
		return false;
	}
	//    }
	//    else 
	//        return true;



}


function BatchValidate(type, fld){
	try {
		var boolFlag = "F";
		if (fld == "custpage_lotbatch") {
			var itemId = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_iteminternalid');
			var ItemType = nlapiLookupField('item', itemId, 'recordType');
//			alert("ItemType " + ItemType);
			if (ItemType == "lotnumberedinventoryitem") {

				var ChknBatchNo = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_lotbatch');

				if (ChknBatchNo) {
					//var itemId = nlapiGetFieldValue('custpage_line_item');


					//  alert("ok");
					var POArrBatchNo = new Array();
					var POBatchNo = POBatchNo + "," + nlapiGetCurrentLineItemValue('custpage_items', 'custpage_serialno');
					if(POBatchNo!='null' && POBatchNo!='' && POBatchNo!=null)// Case# 20148002
						POArrBatchNo = POBatchNo.split(',');
					var poId = nlapiGetFieldValue('custpage_po');
					for (var s = 1; s < POArrBatchNo.length; s++) {

						if (POArrBatchNo[s] == ChknBatchNo) {
							boolFlag = "T";
							s = POArrBatchNo.length;
						}
					}
					if (boolFlag == "F") {
						//Check in Batch Custom Records.
						var batchCustRecChk = BatchCustRecordChk(ChknBatchNo);
						if (batchCustRecChk) {
							//  alert("Inside batchCustRecChk" + batchCustRecChk);
							return batchCustRecChk;
						}
						else {
							var PONum = nlapiGetFieldValue('custpage_po');
							if(PONum !=null && PONum!='' )
							{
								trantype = nlapiLookupField('transaction', PONum, 'recordType');

								if(trantype!='transferorder')
								{

									//var linkURL = "https://system.netsuite.com/app/common/custom/custrecordentry.nl?rectype=74";
									var  linkURL = nlapiResolveURL('RECORD', 'customrecord_ebiznet_batch_entry');
									//var linkURL = "https://system.netsuite.com/app/common/custom/custrecordentry.nl?rectype=74";
									/*var ctx = nlapiGetContext();
							if (ctx.getEnvironment() == 'PRODUCTION') {
								//linkURL = 'https://system.netsuite.com' + linkURL;			
							}
							else 
								if (ctx.getEnvironment() == 'SANDBOX') {
									//linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
								}*/
									window.open(linkURL);
								}
								else
									return false;
							}
						}

						//window.open("POST","https://system.netsuite.com/app/common/custom/custrecordentry.nl?rectype=74");
					}


				}
				else {
					//alert("value is null");

				}
			} //End if Item type is LOT .          
		} //End to check for Serial or LOT Field.
	} 
	catch (err) {
		//alert("God Error"+err);
	}
	return true;
}


function BatchCustRecordChk(ChknBatchNo){

//	alert(ChknBatchNo); 

//	Case # 20124740� Start
	var PONum = nlapiGetFieldValue('custpage_po');

	if(PONum !=null && PONum!='' )
	{
		trantype = nlapiLookupField('transaction', PONum, 'recordType');
		// start case no 20126736
//		validating bathno from item fulfillment for advanced bin management
		var ctx = nlapiGetContext();
		if(ctx != null && ctx != '')
		{
			if(ctx.getFeature('advbinseriallotmgmt').toString() != null && ctx.getFeature('advbinseriallotmgmt').toString() != '')
				vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
			nlapiLogExecution('ERROR', 'vAdvBinManagement_if', vAdvBinManagement);
		}
		var itemId = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_iteminternalid');

		if(trantype=='transferorder')
		{
			if(!vAdvBinManagement)
			{		


				var fields = ['recordType', 'custitem_ebizbatchlot'];
				var columns = nlapiLookupField('item', itemId, fields);
				if(columns != null && columns != '')
				{
					ItemType = columns.recordType;					
					batchflg = columns.custitem_ebizbatchlot;
				}

				if(ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || ItemType=="serializedinventoryitem")
				{
					var IsValidSerailNumber='F';
					var trecord = nlapiLoadRecord('transferorder', PONum);
					var tolocation=trecord.getFieldValue('transferlocation');
					var links=trecord.getLineItemCount('links');
					if(links!=null  && links!='')
					{
						for(var j=0;j<links &&  IsValidSerailNumber=='F';j++)
						{
							var id=trecord.getLineItemValue('links','id',(parseInt(j)+1));
							var linktype = trecord.getLineItemValue('links','type',(parseInt(j)+1));

							nlapiLogExecution('DEBUG', 'linktype',linktype);
							nlapiLogExecution('DEBUG', 'id',id);

							if(linktype=='Item Fulfillment')
							{
								var frecord = nlapiLoadRecord('itemfulfillment', id);
								var fitemcount=frecord.getLineItemCount('item');
								for(var f=1;f<=fitemcount;f++)
								{
									var fitem=frecord.getLineItemValue('item','item',f);
									var fline=frecord.getLineItemValue('item','orderline',f);
									var pofline= fline-1;

									nlapiLogExecution('DEBUG', 'fitem',fitem);
									nlapiLogExecution('DEBUG', 'itemId',itemId);

									if(fitem==itemId) //&& parseInt(getPOLineNo)==parseInt(pofline))
									{
										var  serialnumbers=frecord.getLineItemValue('item','serialnumbers',(parseInt(f)));

										/*if(serialnumbers!=null && serialnumbers!='')
								{
									if(serialnumbers.indexOf(ActualBatch)!=-1)
									{
										IsValidSerailNumber='T';
										break;
									}
								}*/
										if(serialnumbers!=null && serialnumbers!='')
										{
											var tserials='';
											if(serialnumbers!='null' && serialnumbers!='' && serialnumbers!=null)// Case# 20148002
												tserials=serialnumbers.split('');
											//nlapiLogExecution('DEBUG', 'serialnumbers',tserials[0]);
											if(tserials!=null && tserials!='' && tserials.length>0)
											{
												if(tserials.indexOf(ChknBatchNo)!=-1)
												{

													IsValidSerailNumber='T';
													break;
												}
											}
										}
									}
								}
							}
						}

						if(IsValidSerailNumber=='F')
						{
							alert("Lot# is not matching with Lot# picked on the same TO ");
							return false; 
						}
						else //Case # 201410028 start
						{
							var filtersbat = new Array();
							filtersbat[0] = new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', ChknBatchNo);
							filtersbat[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
							if(itemId!=null && itemId!='')
								filtersbat[2] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemId);
							var columnsbat=new Array();
							columnsbat[0] = new nlobjSearchColumn('custrecord_ebizlotbatch');
							columnsbat[1] = new nlobjSearchColumn('custrecord_ebizsitebatch');
							columnsbat[2] = new nlobjSearchColumn('custrecord_ebizexpirydate');
							columnsbat[3] = new nlobjSearchColumn('custrecord_ebizfifodate');
							columnsbat[4] = new nlobjSearchColumn('custrecord_ebizmfgdate');
							columnsbat[5] = new nlobjSearchColumn('custrecord_ebizbestbeforedate');
							columnsbat[6] = new nlobjSearchColumn('custrecord_ebizfifocode');
							columnsbat[7] = new nlobjSearchColumn('custrecord_ebizlastavldate');
							columnsbat[8] = new nlobjSearchColumn('custrecord_ebiz_manufacturelot');
							columnsbat[9] = new nlobjSearchColumn('custrecord_ebizpackcode');
							var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filtersbat,columnsbat);

							if (SrchRecord) {
								var batchRecId= SrchRecord[0].getId();
								/*var batchRec = nlapiLoadRecord('customrecord_ebiznet_batch_entry',batchRecId);
								batchRec.setFieldValue('custrecord_ebizsitebatch',tolocation);
								nlapiSubmitRecord('customrecord_ebiznet_batch_entry',true);
								return true;*/
								var filtersbat1 = new Array();
								filtersbat1[0] = new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', ChknBatchNo);
								filtersbat1[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

								if(itemId!=null && itemId!='')
									filtersbat1[2] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemId);
								if(tolocation!=null && tolocation!='')
									filtersbat1[3] = new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', tolocation);
								var SrchRecord1 = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filtersbat1,null);
								if(SrchRecord1)
								{
									return true;
								}
								else
								{
									var fifodate=SrchRecord[0].getValue('custrecord_ebizfifodate');
									var expirydate=SrchRecord[0].getValue('custrecord_ebizexpirydate');
									var site=SrchRecord[0].getValue('custrecord_ebizsitebatch');
									var batchno=SrchRecord[0].getValue('custrecord_ebizlotbatch');

									var customrecord = nlapiCreateRecord('customrecord_ebiznet_batch_entry');

									customrecord.setFieldValue('name', batchno);
									customrecord.setFieldValue('custrecord_ebizlotbatch', batchno);
									customrecord.setFieldValue('custrecord_ebizmfgdate', SrchRecord[0].getValue('custrecord_ebizmfgdate'));
									customrecord.setFieldValue('custrecord_ebizbestbeforedate', SrchRecord[0].getValue('custrecord_ebizbestbeforedate'));
									customrecord.setFieldValue('custrecord_ebizexpirydate', expirydate);
									customrecord.setFieldValue('custrecord_ebizfifodate', fifodate);
									customrecord.setFieldValue('custrecord_ebizfifocode', SrchRecord[0].getValue('custrecord_ebizfifocode'));
									customrecord.setFieldValue('custrecord_ebizlastavldate', SrchRecord[0].getValue('custrecord_ebizlastavldate'));

									customrecord.setFieldValue('custrecord_ebizsku', itemId);
									customrecord.setFieldValue('custrecord_ebizpackcode',SrchRecord[0].getValue('custrecord_ebizpackcode'));
									customrecord.setFieldValue('custrecord_ebizsitebatch',tolocation);
									customrecord.setFieldValue('custrecord_ebiz_manufacturelot',SrchRecord[0].getValue('custrecord_ebiz_manufacturelot'));													

									var rec = nlapiSubmitRecord(customrecord, false, true);

								}
							}
						}//Case # 201410028 end
					}
				}
			}
			else
			{

				var itemfulfillserialno = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_serial_fulfillserialno');
				var serialArray = new Array();
				if(itemfulfillserialno!='null' && itemfulfillserialno!='' && itemfulfillserialno!=null)// Case# 20148002
					serialArray = itemfulfillserialno.split(',');
				var getEnteredBatchid = '';

				var trecord = nlapiLoadRecord('transferorder', PONum);
				var tolocation=trecord.getFieldValue('transferlocation');
				if(trantype=='transferorder')
				{			
					if(serialArray!=null && serialArray!='')
					{

						var vSerialMatch='F';
						for( var k = 0;k<serialArray.length;k++)
						{ 	 
							//alert("ChknBatchNo:" + ChknBatchNo);
							//alert("serialArray[k]:" + serialArray[k]);
							if(ChknBatchNo==serialArray[k])
							{
								vSerialMatch='T';
								break;
							}
						}
						//	alert("vSerialMatch:" + vSerialMatch);
						if(vSerialMatch =='F')
						{
							alert('Matching Serial Number required.The Serial number on a transfer order receipt must have been fulfilled.');
							return false;
						}
						else //Case # 201410028 start
						{
							var filtersbat = new Array();
							filtersbat[0] = new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', ChknBatchNo);

							if(itemId!=null && itemId!='')
								filtersbat[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemId);
							var columnsbat=new Array();
							columnsbat[0] = new nlobjSearchColumn('custrecord_ebizlotbatch');
							columnsbat[1] = new nlobjSearchColumn('custrecord_ebizsitebatch');
							columnsbat[2] = new nlobjSearchColumn('custrecord_ebizexpirydate');
							columnsbat[3] = new nlobjSearchColumn('custrecord_ebizfifodate');
							columnsbat[4] = new nlobjSearchColumn('custrecord_ebizmfgdate');
							columnsbat[5] = new nlobjSearchColumn('custrecord_ebizbestbeforedate');
							columnsbat[6] = new nlobjSearchColumn('custrecord_ebizfifocode');
							columnsbat[7] = new nlobjSearchColumn('custrecord_ebizlastavldate');
							columnsbat[8] = new nlobjSearchColumn('custrecord_ebiz_manufacturelot');
							columnsbat[9] = new nlobjSearchColumn('custrecord_ebizpackcode');
							var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filtersbat,columnsbat);

							if (SrchRecord) {
								var batchRecId= SrchRecord[0].getId();
								/*var batchRec = nlapiLoadRecord('customrecord_ebiznet_batch_entry',batchRecId);
								batchRec.setFieldValue('custrecord_ebizsitebatch',tolocation);
								nlapiSubmitRecord('customrecord_ebiznet_batch_entry',true);
								return true;*/
								var filtersbat1 = new Array();
								filtersbat1[0] = new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', ChknBatchNo);

								if(itemId!=null && itemId!='')
									filtersbat1[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemId);
								if(tolocation!=null && tolocation!='')
									filtersbat1[2] = new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', tolocation);
								var SrchRecord1 = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filtersbat1,null);
								if(SrchRecord1)
								{
									return true;
								}
								else
								{
									var fifodate=SrchRecord[0].getValue('custrecord_ebizfifodate');
									var expirydate=SrchRecord[0].getValue('custrecord_ebizexpirydate');
									var site=SrchRecord[0].getValue('custrecord_ebizsitebatch');
									var batchno=SrchRecord[0].getValue('custrecord_ebizlotbatch');

									var customrecord = nlapiCreateRecord('customrecord_ebiznet_batch_entry');

									customrecord.setFieldValue('name', batchno);
									customrecord.setFieldValue('custrecord_ebizlotbatch', batchno);
									customrecord.setFieldValue('custrecord_ebizmfgdate', SrchRecord[0].getValue('custrecord_ebizmfgdate'));
									customrecord.setFieldValue('custrecord_ebizbestbeforedate', SrchRecord[0].getValue('custrecord_ebizbestbeforedate'));
									customrecord.setFieldValue('custrecord_ebizexpirydate', expirydate);
									customrecord.setFieldValue('custrecord_ebizfifodate', fifodate);
									customrecord.setFieldValue('custrecord_ebizfifocode', SrchRecord[0].getValue('custrecord_ebizfifocode'));
									customrecord.setFieldValue('custrecord_ebizlastavldate', SrchRecord[0].getValue('custrecord_ebizlastavldate'));

									customrecord.setFieldValue('custrecord_ebizsku', itemId);
									customrecord.setFieldValue('custrecord_ebizpackcode',SrchRecord[0].getValue('custrecord_ebizpackcode'));
									customrecord.setFieldValue('custrecord_ebizsitebatch',tolocation);
									customrecord.setFieldValue('custrecord_ebiz_manufacturelot',SrchRecord[0].getValue('custrecord_ebiz_manufacturelot'));													

									var rec = nlapiSubmitRecord(customrecord, false, true);
								}
							}
						}//Case # 201410028 end
					}

				}
			}

		}
		else
		{
			//case # 20148161� 

			var curitem=nlapiGetCurrentLineItemValue('custpage_items','custpage_iteminternalid');

			//	alert('curitem ' + curtitem);

			var filtersbat = new Array();
			filtersbat[0] = new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', ChknBatchNo);
			filtersbat[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

			if(curitem!=null && curitem!='')
				filtersbat[2] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', curitem);

			var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filtersbat);

			if (SrchRecord) {
				//	alert("yes");
				return true;
			}
			else {
				//alert("no");
				return false;
			}
		}
	}
	//Case # 20124740� END





}


/*
 This Script is called when a PUTaway Record is Saved with out a Location.
 */
function onSave(type)
{
	var linevalue = nlapiGetLineItemValue('custpage_items', 'custpage_line', 1);
	var potext = nlapiGetFieldValue('custpage_po');
	// alert("potext " + potext );



	var poid=GetSOInternalId(potext);
	
	//alert("poid " + poid );
	var Pointernalid = nlapiGetFieldValue('custpage_poidpost');

	//alert("Pointernalid : " + Pointernalid );
	if(poid == null || poid == '' || poid == '-1')
	{
		poid = Pointernalid;
	}
	//alert("poid " + poid );
	var trantype = nlapiLookupField('transaction', poid, 'recordType');
//	alert("trantype " + trantype );
	//var trantype = nlapiLookupField('transaction', nlapiGetFieldValue('custpage_po'), 'recordType');
	//alert("trantype " + trantype );

	var validate=CheckOrderStatus(poid,trantype);
	//alert("validate " + validate );
	var lineCnt = nlapiGetLineItemCount('custpage_items');// case# 201413579
	if (linevalue != "" && linevalue != null) 
	{

		var i = 0;
		var chkcount = 0;
		//alert("Cnt "+lineCnt);

		for (var s = 1; s <= lineCnt; s++) 
		{
			var linecheck = nlapiGetLineItemValue('custpage_items', 'custpage_polocgen', s);
			if (linecheck == "T") 
			{
				chkcount = 1;
				var lineLoc = nlapiGetLineItemValue('custpage_items', 'custpage_location', s);
				//alert("lineLoc "+lineLoc);
				//var confirmFlag = nlapiGetLineItemValue('custpage_items', 'custpage_polocgen', s);
				if (lineLoc == "" || lineLoc == null) {
					alert("Please enter value(s)for: Bin Location");
					i = 1;
					s = lineCnt;
					return false;
				}
				// Case# 20148592 starts
				var qty = nlapiGetLineItemValue('custpage_items', 'custpage_quantity', s);
				if(qty <= 0)
				{
					alert("Qty should be greater than 0");
					return false;
				}
				if(validate=="F")
				{

					//	alert("trantype : " + trantype);
					alert("Invalid Order Status,Can't proceed");
					return false;

				}
				
				var vserialno=nlapiGetLineItemValue('custpage_items', 'custpage_serialnos', s);
				var ItemInternlId=nlapiGetLineItemValue('custpage_items', 'custpage_iteminternalid', s);
				
				//alert(vserialno);
			//	alert(ItemInternlId);
				
				
				var fields = ['recordType', 'custitem_ebizserialin','custitem_ebizbatchlot'];
				var columns = nlapiLookupField('item', ItemInternlId, fields);
				var ItemType = columns.recordType;	
				var serialInflg="F";		
				serialInflg = columns.custitem_ebizserialin;
				var batchflag="F";
				batchflag= columns.custitem_ebizbatchlot;
				
				
				//if (vserialno==null || vserialno == "" ) { 
					if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T")
					{
						
						
						/*alert("Item" + nlapiGetLineItemValue('custpage_items','custpage_iteminternalid',s));
						alert("PO" + poid);
						
						alert("LP #" + nlapiGetLineItemValue('custpage_items','custpage_chkn_lp',s));
						alert("trantype" + trantype);*/
						
						
						var vLineno = nlapiGetLineItemValue('custpage_items','custpage_chkn_line',s)
						if(vLineno == '' || vLineno == null || vLineno == 'null')
						{
							vLineno =  nlapiGetLineItemValue('custpage_items','custpage_line',s)
						}
						//alert("LineNi" + vLineno);
						
						
						var filterSerialEntry = new Array();
						if(trantype=='purchaseorder'||trantype=='transferorder')
						{
							filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialitem',null,'anyof',nlapiGetLineItemValue('custpage_items','custpage_iteminternalid',s)));
							filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialebizpono',null,'is',poid));
							filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialpolineno',null,'is',vLineno));
							filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialparentid',null,'is',nlapiGetLineItemValue('custpage_items','custpage_chkn_lp',s)));// Case # 20148626
						}
						else 
						{
							filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialitem',null,'anyof',nlapiGetLineItemValue('custpage_items','custpage_iteminternalid',s)));
							filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialebizrmano',null,'is',poid));
							filterSerialEntry.push(new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno',null,'is',vLineno));
							filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialparentid',null,'is',nlapiGetLineItemValue('custpage_items','custpage_chkn_lp',s)));
						}

						var SearchRec=nlapiSearchRecord('customrecord_ebiznetserialentry',null,filterSerialEntry,null);
					//	alert("SearchRec" + SearchRec);
						if(SearchRec == null || SearchRec == '')
						{
							alert("Please enter serial no ");
							return false;
						}
					}
				//}

				// Case# 20148592 starts
			}

		}
		if(chkcount == 0)
		{
			alert("Please Select Atleast One Line");            
			return false;
		}
		else if (i == 0) 
		{
			return true;
		}
	}
	else {

		if(parseInt(nlapiGetFieldValue('custpage_recommended_quan'))>0)
		{
			if(nlapiGetLineItemCount('custpage_items')==0)
			{
				alert("Please Enter Atleast One Line");            
				return false;
			}
		}
		for (var i = 1; i <= lineCnt; i++) 
		{
			var lpno = nlapiGetLineItemValue('custpage_items', 'custpage_chkn_lp', i);


			// Case# 20148592 starts
			var filters1 = new Array();				
			//filters1[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lpno);
			filters1[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lpno);

			var columns1 = new Array();
			columns1[0] = new nlobjSearchColumn('name');

			var searchresults1 = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters1,columns1);		
			if(searchresults1!=null &&  searchresults1.length>0)
			{
				alert('LP Already Exists');
				return false;
			}
			
			var vserialno=nlapiGetLineItemValue('custpage_items', 'custpage_serialnos', i);
			var ItemInternlId=nlapiGetLineItemValue('custpage_items', 'custpage_iteminternalid', i);
			
			/*alert(vserialno);
			alert(ItemInternlId);
			*/
			
			var fields = ['recordType', 'custitem_ebizserialin','custitem_ebizbatchlot'];
			var columns = nlapiLookupField('item', ItemInternlId, fields);
			var ItemType = columns.recordType;	
			var serialInflg="F";		
			serialInflg = columns.custitem_ebizserialin;
			var batchflag="F";
			batchflag= columns.custitem_ebizbatchlot;
			
			
			//if (vserialno==null || vserialno == "" ) { 
				if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T")
				{
					/*
					alert("Item" + nlapiGetLineItemValue('custpage_items','custpage_iteminternalid',i));
					alert("PO" + nlapiGetFieldValue('custpage_po'));
					alert("LineNi" + nlapiGetLineItemValue('custpage_items','custpage_chkn_line',i));
					alert("LP #" + nlapiGetLineItemValue('custpage_items','custpage_chkn_lp',i));
					alert("trantype" + trantype);*/
				
					
					var vLineno = nlapiGetLineItemValue('custpage_items','custpage_chkn_line',i)
					if(vLineno == '' || vLineno == null || vLineno == 'null')
					{
						vLineno =  nlapiGetLineItemValue('custpage_items','custpage_line',i)
					}
					
					
					var filterSerialEntry=new Array();
					if(trantype=='purchaseorder'||trantype=='transferorder')
					{
						filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialitem',null,'anyof',nlapiGetLineItemValue('custpage_items','custpage_iteminternalid',i)));
						filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialebizpono',null,'is',poid));
						filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialpolineno',null,'is',vLineno));
						filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialparentid',null,'is',nlapiGetLineItemValue('custpage_items','custpage_chkn_lp',i)));// Case # 20148626
					}
					else 
					{
						filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialitem',null,'anyof',nlapiGetLineItemValue('custpage_items','custpage_iteminternalid',i)));
						filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialebizrmano',null,'is',nlapiGetFieldValue('custpage_po')));
						filterSerialEntry.push(new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno',null,'is',vLineno));
						filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialparentid',null,'is',nlapiGetLineItemValue('custpage_items','custpage_chkn_lp',i)));
					}

					var SearchRec=nlapiSearchRecord('customrecord_ebiznetserialentry',null,filterSerialEntry,null);
					
					//alert("SearchRec@@" + SearchRec);
					if(SearchRec == null || SearchRec == '')
					{
						alert("Please enter serial no ");
						return false;
					}
				}
		}

		return true;
	}
}

/* This function is called to check for Quantity overages.

 */
function validateQuantity(type,fld,linenum)
{
	var ctx = nlapiGetContext();
	var overreceiptPreference = ctx.getPreference('OVERRECEIPTS');

	if (fld == "custpage_items") 
	{
		var totalLineQty = "";
		var Qty = nlapiGetFieldValue('custpage_item_quan');
		var Checkedin = nlapiGetFieldValue('custpage_checkin_quan');
		var quantity=parseInt(Qty)-parseInt(Checkedin);
		var currentLineQty = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_chkn_rcvngqty');
		var totalLineCnt = nlapiGetLineItemCount('custpage_items');
		var recommendedQuantity = nlapiGetFieldValue('custpage_itemrecmd_qty_withpooverage');
		var totRecommendedQuantity = parseInt(Checkedin)+parseInt(recommendedQuantity);
		for (var s = 1; s <= totalLineCnt; s++) {
			if (totalLineQty == "") {

				totalLineQty = parseInt(nlapiGetLineItemValue('custpage_items', 'custpage_chkn_rcvngqty', s));
			}
			else {
				totalLineQty = parseInt(totalLineQty) + parseInt(nlapiGetLineItemValue('custpage_items', 'custpage_chkn_rcvngqty', s));
			}				
		}
		if(parseInt(currentLineQty)<0)
		{
			alert('Qty should be greater than 0');
			return false;
		}
		if (currentLineQty) {
			//alert('currentLineQty :'+currentLineQty);
			var totalCHKDQty ="";
			if(totalLineQty=="")
			{
				totalCHKDQty = parseInt(Checkedin) + parseInt(currentLineQty);	
			}
			else
			{
				totalCHKDQty = parseInt(Checkedin) + parseInt(totalLineQty) + parseInt(currentLineQty);
			}
		}
//		if (overreceiptPreference == "F"){  //If overage is not allowed then qty should not be greater than orderd qty.
//		if (parseInt(totalCHKDQty) <= parseInt(Qty)) {
//		return true;
//		}
//		else {
//		alert('PO Overage is not allowed Change the quantity');
//		return false;
//		}
//		}
//		else{ //If overage is allowed then qty should not be greater than recommended qty.
//		var PoOverageVal = parseInt(recommendedQuantity)-parseInt(quantity);
//		if(parseInt(PoOverageVal) == 0){
//		if (parseInt(totalCHKDQty) <= parseInt(Qty)) {
//		return true;
//		}
//		else{
//		alert('PO Overage is not allowed Change the quantity');
//		return false;
//		}
//		}
//		else{
//		//alert('totalCHKDQty :'+totalCHKDQty);
//		//alert('totRecommendedQuantity :'+totRecommendedQuantity);
//		if(parseInt(totalCHKDQty) > parseInt(totRecommendedQuantity)){
//		alert('QUANTITY EXCEEDS OVERAGE LIMIT');
//		return false;
//		}
//		}
//		}

	}

	if (fld == "custpage_pochkn_qty") 
	{
		//alert(nlapiGetCurrentLineItemValue('custpage_items', 'custpage_pochkn_qty'));
		//alert(nlapiGetCurrentLineItemValue('custpage_items', 'custpage_uomqty'));
		//alert(nlapiGetCurrentLineItemValue('custpage_items', 'custpage_baseuomqty'));

		var vpochknqty = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_pochkn_qty');
		if(vpochknqty == 0)// case# 201411385
		{

			alert('Please enter valid quantity');
			//nlapiSetCurrentLineItemValue('custpage_items','custpage_pochkn_qty','');
			//nlapiSetCurrentLineItemValue('custpage_items','custpage_chkn_rcvngqty','');//Case # 20127618
			return false;
		}	
		//case 201410987
		if(isNaN(vpochknqty)||(vpochknqty<0))
		{
			alert('Please enter valid quantity');
			nlapiSetCurrentLineItemValue('custpage_items','custpage_pochkn_qty','');
			nlapiSetCurrentLineItemValue('custpage_items','custpage_chkn_rcvngqty','');//Case # 20127618
			return false;
		}

		var vuomqty = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_uomqty');
		var vbaseuomqty = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_baseuomqty');

		if(vuomqty==null || vuomqty=='' || isNaN(vuomqty))
			vuomqty=1;

		if(vbaseuomqty==null || vbaseuomqty=='' || isNaN(vbaseuomqty))
			vbaseuomqty=1;

		var vpobaseQtyChkn = (parseFloat(vpochknqty)*parseFloat(vuomqty))/parseFloat(vbaseuomqty);

		//alert(vpobaseQtyChkn);

		nlapiSetCurrentLineItemValue('custpage_items','custpage_chkn_rcvngqty',vpobaseQtyChkn);
	}
	if (fld == "custpage_chkn_binlocation") 
	{//custpage_chkn_lp

		var newBinLoc = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_chkn_binlocation');
		var itmid = nlapiGetCurrentLineItemValue('custpage_items','custpage_iteminternalid');
		var putQty = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_chkn_rcvngqty');

		var arrDims = getSKUCubeAndWeight(itmid, 1);
		var itemCube = 0;
		//alert(arrDims[0]);
		//alert(parseFloat(arrDims[1]));
		if (arrDims[0] != "" && (!isNaN(arrDims[0]))) 
		{
			var uomqty = ((parseFloat(putQty))/(parseFloat(arrDims[1])));			
			itemCube = (parseFloat(uomqty) * parseFloat(arrDims[0]));

		} 
		var vNewLocationRemainingCube = GeteLocCube(newBinLoc);   
		if(vNewLocationRemainingCube == '')
			vNewLocationRemainingCube=0;		
		var vTotalNewLocationCube =  parseFloat(vNewLocationRemainingCube)- parseFloat(itemCube);



		if(vTotalNewLocationCube<0)
		{
			alert('Insufficent remaning cube');
			return false;
		}
	}
}

function onDelete()
{	alert("Record Cannot Be Deleted.");
return false;
}

function onInsert()
{	
	alert("Record Cannot Be Inserted.");
	return false;
}

function onChange(type,name,line)
{ 
	var soId=nlapiGetFieldValue('id'); 
	if(name=='item')
	{ 
		var lineNo = nlapiGetCurrentLineItemIndex('item'); 
		var seleteditem = nlapiGetCurrentLineItemValue('item', 'item'); 
		var fields = ['custitem_ebizdefpackcode','custitem_ebizdefskustatus'];
		var columns = nlapiLookupField('item', seleteditem, fields);
		var skuStatus = columns.custitem_ebizdefskustatus;
		var packCode= columns.custitem_ebizdefpackcode; 
		nlapiSetCurrentLineItemValue('item','custcol_nswmspackcode',packCode,false);
		nlapiSetCurrentLineItemValue('item','custcol_ebiznet_item_status',skuStatus,false); 
	} 
}

function ValidateLine(type, name) {
	//alert(name);
	//case# 20127114 starts
	if (name == "custpage_chkn_lp") {
		var lpno = nlapiGetLineItemValue('custpage_items',
		'custpage_chkn_lp');
		//alert(lpno);
		var getLPNo = nlapiGetCurrentLineItemValue('custpage_items',
		'custpage_chkn_lp');
		//alert(getLPNo);
		var filters = new Array();
		// filters[0] = new nlobjSearchFilter('custrecord_status_flag',
		// null, 'is', 'L');
		filters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null,
				'anyof', [ 2 ]);
		filters[1] = new nlobjSearchFilter('custrecord_lpno', null, 'is',
				getLPNo);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_lpno');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		columns[2] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		var searchresults = nlapiSearchRecord(
				'customrecord_ebiznet_trn_opentask', null, filters, columns);
		if (searchresults == null) {
			alert('Invalid LP');
			nlapiSetCurrentLineItemValue('custpage_items', 'custpage_chkn_lp',lpno);
			return false;
		}
	}
	//case# 20127114 end
	// Case# 20148592 starts

	var pointid="";
	var actQuantity="";
	var line1="";
	var trantype="";
	if (nlapiGetCurrentLineItemValue('custpage_items','custpage_pointid')) 
	{
		pointid = nlapiGetCurrentLineItemValue('custpage_items','custpage_pointid');
		var fields = ['recordType', 'location','custbody_nswms_company'];
		var columns = nlapiLookupField('transaction', nlapiGetCurrentLineItemValue('custpage_items','custpage_pointid'), fields);
		trantype = columns.recordType;

	}

	var lineno = nlapiGetCurrentLineItemValue('custpage_items','custpage_line');

	if(pointid!=null&&pointid!=''&&pointid!='null'&&trantype!=null&&trantype!=''&&trantype!='null')
	{
		var poLocRecres = nlapiLoadRecord(trantype,pointid);

		var polinecountres = poLocRecres.getLineItemCount('item');

		if(polinecountres!=null && polinecountres!='')
		{
			for(var m1=1;m1<=polinecountres;m1++)
			{
				line1 = poLocRecres.getLineItemValue('item','line',m1);
				if (line1==lineno)
				{
					actQuantity = poLocRecres.getLineItemValue('item','quantity',m1);
					break;
				}
			}
		}
	}				
	if (name == "custpage_quantity") {
		var qty = nlapiGetCurrentLineItemValue('custpage_items','custpage_quantity');
		if(qty <= 0)
		{
			alert("Qty should be greater than 0");
			return false;
		}
		if(actQuantity!=null&&actQuantity!='')
		{
			if (parseFloat(qty)>parseFloat(actQuantity))
			{
				alert("Qty should be less than or equal to checked in quantity");
				return false;
			}
		}
	}
	// Case# 20148592 ends
	if (trim(name) != trim('custpage_selectpage')) {
		var lincount = nlapiGetLineItemCount('custpage_items');
		var totnooflines=0;
		for (var p = 0; p < lincount; p++) 
		{
			var lineSelected = nlapiGetLineItemValue('custpage_items', 'custpage_polocgen', p);

			if(lineSelected=='T')
			{
				var nooflines=nlapiGetLineItemValue('custpage_items', 'custpage_lineno', p);
				totnooflines=totnooflines+parseFloat(nooflines);

				if(parseFloat(totnooflines)>100)
				{
					alert('Total no. of lines selected cannot be greater than 100.');
					nlapiSetCurrentLineItemValue('custpage_items','custpage_polocgen','F');
					return false;
				}
			}
		}
	}
	else
	{
		if(trim(name)==trim('custpage_selectpage'))
		{
			nlapiSetFieldValue('custpage_hiddenfieldselectpage','T');
			NLDoMainFormButtonAction("submitter",true);	
		}
		else
		{
			return true;
		}
	}
	if (name == "custpage_location") 
	{
		var newBinLoc = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_location');
		var itmid = nlapiGetCurrentLineItemValue('custpage_items','custpage_iteminternalid');
		var putQty = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_quantity');

		var arrDims = getSKUCubeAndWeight(itmid, 1);
		var itemCube = 0;
		//alert(arrDims[0]);
		//alert(parseFloat(arrDims[1]));
		if (arrDims[0] != "" && (!isNaN(arrDims[0]))) 
		{
			var uomqty = ((parseFloat(putQty))/(parseFloat(arrDims[1])));			
			itemCube = (parseFloat(uomqty) * parseFloat(arrDims[0]));

		} 
		var vNewLocationRemainingCube = GeteLocCube(newBinLoc);   
		if(vNewLocationRemainingCube == '')
			vNewLocationRemainingCube=0;		
		var vTotalNewLocationCube =  parseFloat(vNewLocationRemainingCube)- parseFloat(itemCube);

		if(vTotalNewLocationCube<0)
		{
			nlapiSetCurrentLineItemValue('custpage_items','custpage_location','',false,false);
			alert('Insufficent remaning cube');
			return false;
		}
	}
}
//upto here
if (!Array.prototype.indexOf)
{
	Array.prototype.indexOf = function(elt /*, from*/)
	{
		var len = this.length;

		var from = Number(arguments[1]) || 0;
		from = (from < 0)
		? Math.ceil(from)
				: Math.floor(from);
		if (from < 0)
			from += len;

		for (; from < len; from++)
		{
			if (from in this &&
					this[from] === elt)
				return from;
		}
		return -1;
	};
}


function GetSOInternalId(SOText)
{
	//alert("into GetSOInternalId");

	var ActualSoID='-1';

	var filter=new Array();
	filter.push(new nlobjSearchFilter('tranid',null,'is',SOText.toString()));


	var columns=new Array();
	columns.push(new nlobjSearchColumn('shipmethod'));

	var searchrec=nlapiSearchRecord('purchaseorder',null,filter,columns);
	if(searchrec==null)
	{
		searchrec=nlapiSearchRecord('returnauthorization',null,filter,columns);
	}
	//alert("into GetSOInternalId here");

	if(searchrec==null)
	{
		searchrec=nlapiSearchRecord('transferorder',null,filter,columns);
	}
	if(searchrec!=null && searchrec!='' && searchrec.length>0)
	{
		ActualSoID=searchrec[0].getId();
	}


	return ActualSoID;
}




function GetPOcheckinqty(poName,lineno,item,poid)
{

	var POcheckinqty;

	var filter=new Array();
//case # 20124143 
	if(poid!=null && poid!='')
	{
		filter.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no',null,'equalto',poid));
		//filter.push(new nlobjSearchFilter('custrecord_orderlinedetails_order_no',null,'is',poName));
		filter.push(new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no',null,'equalto',lineno));
		filter.push(new nlobjSearchFilter('custrecord_orderlinedetails_item',null,'anyof',item));
		nlapiLogExecution('ERROR','poName', poName);
		nlapiLogExecution('ERROR','lineno', lineno);
		nlapiLogExecution('ERROR','item', item);

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty');
		column[1]=new nlobjSearchColumn('custrecord_orderlinedetails_putgen_qty');

		var searchrecord=nlapiSearchRecord('customrecord_ebiznet_order_line_details',null,filter,column);
		if(searchrecord!=null && searchrecord!='')
		{
			POcheckinqty=searchrecord[0].getValue('custrecord_orderlinedetails_checkin_qty');
			vputgenQuantity = searchrecord[0].getValue('custrecord_orderlinedetails_putgen_qty');
		}
	}
// case #  20124143 end
	return searchrecord;
}