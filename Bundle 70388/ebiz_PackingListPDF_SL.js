/***************************************************************************
	  		  eBizNET Solutions Inc            
 ****************************************************************************/
/* Prologue - 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Reports/Suitelet/ebiz_PackingListPDF_SL.js,v $
 *     	   $Revision: 1.10.4.3.4.1.4.2.8.1 $
 *     	   $Date: 2015/12/02 13:28:54 $
 *     	   $Author: schepuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_PackingListPDF_SL.js,v $
 * Revision 1.10.4.3.4.1.4.2.8.1  2015/12/02 13:28:54  schepuri
 * case# 201415889
 *
 * Revision 1.10.4.3.4.1.4.2  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.10.4.3.4.1.4.1  2013/02/27 13:06:28  rmukkera
 * monobid production bundle changes were merged to cvs with tag
 * t_eBN_2013_1_StdBundle_2
 *
 * Revision 1.10.4.3.4.1  2012/09/24 22:45:46  spendyala
 * CASE201112/CR201113/LOG201121
 * Issues related to transaction type is resolved.
 *
 * Revision 1.10.4.3  2012/08/31 01:42:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Fisk Production Issue Fixes
 *
 * Revision 1.10.4.2  2012/04/30 10:41:37  spendyala
 * CASE201112/CR201113/LOG201121
 * While Searching of Item in ItemMaster,
 * 'name' filter is changed to 'nameinternal' and also checking weather the item is inactive state or not.
 *
 * Revision 1.10.4.1  2012/03/20 20:42:32  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Moved from Trunk to Branch
 *
 * Revision 1.11  2012/03/20 16:16:45  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Dynamic Image
 *
 * Revision 1.10  2011/10/04 13:15:48  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.9  2011/10/03 09:27:17  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.8  2011/10/03 06:10:31  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.7  2011/10/03 05:30:24  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.6  2011/09/30 13:41:07  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.5  2011/09/30 13:16:54  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.4  2011/09/30 12:24:18  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.3  2011/09/27 14:13:02  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.2  2011/09/25 11:44:56  mbpragada
 * CASE201112/CR201113/LOG201121
 * Completed Packing List Report  Development
 *
 * Revision 1.1  2011/09/24 06:41:39  mbpragada
 * CASE201112/CR201113/LOG201121
 * Completed Packing List Report  Development
 *

 *****************************************************************************/

function PackingReportPDFSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('PackingList Report');	

		//var replaceChar =/\$|,|@|#|~|`|\%|\*|\^|\&|\+|\=|\-|\_|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|\./g;

			
			var replaceChar =/\$|,|@|#|~|`|\%|\*|\^|\&|\+|\=|\-|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|\./g;

		var vQbfullfillno='',shipfromcity='',shipfromcountry='',shipfromzipcode='',shipfromaddress='',shipfromphone='',shipfromstate='',vQbFullfillmentName;

		if(request.getParameter('custparam_ebiz_fullfill_no')!=null && request.getParameter('custparam_ebiz_fullfill_no')!="")
		{							
			vQbfullfillno = request.getParameter('custparam_ebiz_fullfill_no');
			nlapiLogExecution('ERROR', 'pdf : vQbfullfillno',vQbfullfillno);	
		} 		
		if(vQbfullfillno != null && vQbfullfillno != ""){
			vQbFullfillmentName = getFullfillmentName(vQbfullfillno);
		}
		//for getting SO internal id
		var sointernalfilters = new Array();
		if (vQbFullfillmentName != "") {
			nlapiLogExecution('ERROR', 'vQbFullfillmentName', vQbFullfillmentName);
			sointernalfilters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', vQbFullfillmentName));			 
		}			 
		var sointernalcolumns = new Array();
		sointernalcolumns[0] = new nlobjSearchColumn('custrecord_ns_ord');

		var sointernalsearchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, sointernalfilters, sointernalcolumns);
		var saleordInternalId=sointernalsearchresults[0].getValue('custrecord_ns_ord');
		nlapiLogExecution('ERROR','saleordInternalId ',saleordInternalId);

		//for getting line details
		var linesearchfilters = new Array();
		if (vQbFullfillmentName != "" && vQbFullfillmentName != null) {
			nlapiLogExecution('ERROR', 'vQbFullfillmentName', vQbFullfillmentName);
			linesearchfilters.push(new nlobjSearchFilter('name', null, 'is', vQbFullfillmentName));			 
		}	
		linesearchfilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', '28'));
		linesearchfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));
		var linecolumns = new Array();
		linecolumns[0] = new nlobjSearchColumn('custrecord_line_no');
		linecolumns[1] = new nlobjSearchColumn('custrecord_sku');
		linecolumns[2] = new nlobjSearchColumn('custrecord_skudesc');	
		linecolumns[3] = new nlobjSearchColumn('custrecord_act_qty');
		linecolumns[4] = new nlobjSearchColumn('name');	
		/* new search column merged from monobid bundle on feb 27th 2013 by radhika */		
		linecolumns[5] = new nlobjSearchColumn('custrecord_batch_no');
		var linesearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, linesearchfilters, linecolumns);

		//for getting header details
		var vtrantype = nlapiLookupField('transaction', saleordInternalId, 'recordType');
		var salesorderheadresearch = nlapiLoadRecord(vtrantype, saleordInternalId); 
		var soname = salesorderheadresearch.getFieldValue('tranid');
		var	shippingadd=salesorderheadresearch.getFieldValue('shipaddress'); 
		var customerpono=salesorderheadresearch.getFieldValue('otherrefnum');	
		var customerpo=salesorderheadresearch.getFieldValue('entity');	
		var companyid=salesorderheadresearch.getFieldValue('custbody_nswms_company');
		var shippingCondition=salesorderheadresearch.getFieldText('custbody_salesorder_carrier');
		var locationinternalid=salesorderheadresearch.getFieldValue('location');
		var incoterms=salesorderheadresearch.getFieldText('custbody_nswmsfreightterms');
		nlapiLogExecution('ERROR','locationinternalid ',locationinternalid); 

		if(soname != null && soname !="")
			soname=soname.replace(replaceChar,'');
		else
			soname="";

		if(shippingadd != null && shippingadd !="")
			shippingadd=shippingadd.replace(replaceChar,'');
		else
			shippingadd="";

		if(customerpono != null && customerpono !="")
			customerpono=customerpono.replace(replaceChar,'');
		else
			customerpono="";

		if(companyid != null && companyid !="")
			companyid=companyid.replace(replaceChar,'');
		else
			companyid="";

		if(shippingCondition != null && shippingCondition !="")
			shippingCondition=shippingCondition.replace(replaceChar,'');
		else
			shippingCondition="";

		if(incoterms != null && incoterms !="")
			incoterms=incoterms.replace(replaceChar,'');
		else
			incoterms="";


		var	shippingadd1 = salesorderheadresearch.getFieldValue('shipaddressee');
		var	shippingadd2 = salesorderheadresearch.getFieldValue('shipaddr1');
		var	shippingadd3 = salesorderheadresearch.getFieldValue('shipcity');
		var	shippingadd4 = salesorderheadresearch.getFieldValue('shipstate');
		var	shippingadd5 = salesorderheadresearch.getFieldValue('shipzip');	

		if(shippingadd1 != null && shippingadd1 !="")
			shippingadd1=shippingadd1.replace(replaceChar,'');
		else
			shippingadd1="";

		if(shippingadd2 != null && shippingadd2 !="")
			shippingadd2=shippingadd2.replace(replaceChar,'');
		else
			shippingadd2="";

		if(shippingadd3 != null && shippingadd3 !="")
			shippingadd3=shippingadd3.replace(replaceChar,'');
		else
			shippingadd3="";

		if(shippingadd4 != null && shippingadd4 !="")
			shippingadd4=shippingadd4.replace(replaceChar,'');
		else
			shippingadd4="";

		if(shippingadd5 != null && shippingadd5 !="")
			shippingadd5=shippingadd5.replace(replaceChar,'');
		else
			shippingadd5="";

		if(locationinternalid != null && locationinternalid != ""){
			var companylist = nlapiLoadRecord('location', locationinternalid); 
			var attention = companylist.getFieldValue('attention');
			var addressee = companylist.getFieldValue('addressee');
			var state = companylist.getFieldValue('state');
			var zip = companylist.getFieldValue('zip');

			var add1 = companylist.getFieldValue('addr1');
			var	add2=companylist.getFieldValue('addr2'); 
			//var shipfromstate=companylist.getFieldValue('state');
			var shipfromcity=companylist.getFieldValue('city');
			var shipfromcountry=companylist.getFieldText('country');	

			if(attention != null && attention !="")
				attention=attention.replace(replaceChar,'');
			else
				attention="";

			if(addressee != null && addressee !="")
				addressee=addressee.replace(replaceChar,'');
			else
				addressee="";

			if(state != null && state !="")
				state=state.replace(replaceChar,'');
			else
				state="";

			if(zip != null && zip !="")
				zip=zip.replace(replaceChar,'');
			else
				zip="";

			if(add1 != null && add1 !="")
				add1=add1.replace(replaceChar,'');
			else
				add1="";

			if(add2 != null && add2 !="")
				add2=add2.replace(replaceChar,'');
			else
				add2="";

			if(shipfromcity != null && shipfromcity !="")
				shipfromcity=shipfromcity.replace(replaceChar,'');
			else
				shipfromcity="";

			if(shipfromcountry != null && shipfromcountry !="")
				shipfromcountry=shipfromcountry.replace(replaceChar,'');
			else
				shipfromcountry="";

		}

		//for getting header details SHIPPING FROM
//		nlapiLogExecution('ERROR','companyid ',companyid);		 
//		if(companyid != null && companyid != ""){
//		var shipfromfilter = new Array();
//		shipfromfilter [0] = new nlobjSearchFilter('Internalid', null, 'is', companyid);
//		var shipfromcolumn= new Array();
//		shipfromcolumn[0] = new nlobjSearchColumn('custrecord_compcity');
//		shipfromcolumn[1] = new nlobjSearchColumn('custrecord_compcountry');
//		shipfromcolumn[2] = new nlobjSearchColumn('custrecord_compstate');
//		shipfromcolumn[3] = new nlobjSearchColumn('custrecord_compzipcode');
//		shipfromcolumn[4] = new nlobjSearchColumn('custrecord_compaddr');
//		shipfromcolumn[5] = new nlobjSearchColumn('custrecord_compphone1');

//		var companylist= nlapiSearchRecord('customrecord_ebiznet_company', null, shipfromfilter , shipfromcolumn);

//		if(companylist !=null)
//		{
//		shipfromcity=companylist[0].getValue('custrecord_compcity');
//		shipfromcountry=companylist[0].getText('custrecord_compcountry');
//		shipfromstate=companylist[0].getText('custrecord_compstate');
//		shipfromaddress=companylist[0].getValue('custrecord_compaddr');
//		shipfromphone=companylist[0].getValue('custrecord_compphone1');
//		shipfromzipcode=companylist[0].getValue('custrecord_compzipcode');
//		}
//		}

		var sysdate=DateStamp();		 
		nlapiLogExecution('ERROR','sysdate ',sysdate);

		var vline, vitem, vqty, vTaskType, vmainline, vrecid, vlocation, vLpno, vSKU, vlocationid, vinvrefno, vskustatus, vpackcode, vdono;
		var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body  font-size=\"9\">\n";
		nlapiLogExecution('ERROR','xml0 ',"<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body font-size=\"10\">\n");

		var strxml ="<table width='60%' >";
		//strxml += "<tr ><td valign='top' align='left'>";//<img src='https://system.netsuite.com/c.TSTDRV840430/suitebundle14109/cable55.jpg' /></td><td valign='bottom' align='center'  style='font-size:xx-large;'>";

		var url;
		/*var ctx = nlapiGetContext();
		nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
		if (ctx.getEnvironment() == 'PRODUCTION') 
		{
			url = 'https://system.netsuite.com';			
		}
		else if (ctx.getEnvironment() == 'SANDBOX') 
		{
			url = 'https://system.sandbox.netsuite.com';				
		}*/
		nlapiLogExecution('ERROR', 'PDF URL',url);	


		var filefound = nlapiLoadFile('Images/LOGOCOMP.jpg'); 
		if (filefound) 
		{ 
			nlapiLogExecution('ERROR', 'Event', 'file;'+filefound.getId()); 
			var imageurl = filefound.getURL();
			nlapiLogExecution('ERROR','imageurl',imageurl);
			//var finalimageurl = url + imageurl;//+';';
			var finalimageurl = imageurl;//+';';
			//finalimageurl=finalimageurl+ '&expurl=T;';
			nlapiLogExecution('ERROR','imageurl',finalimageurl);
			finalimageurl=finalimageurl.replace(/&/g,"&amp;");

		} 
		else 
		{
			nlapiLogExecution('ERROR', 'Event', 'No file;');
		}
		strxml += "<tr ><td valign='middle' align='left'><img src='" + finalimageurl + "'></img></td><td valign='middle' align='left'  style='font-size:xx-large;'>";
		strxml += "Packing List ";
		strxml += "</td>        </tr>   <tr><td>&nbsp;</td></tr> </table>";


		strxml += "<table width='100%' height='200px'><tr><td height='200px'><table><tr>";
		strxml += "<td><table border='1' width='100%' height='100'><tr><td style=\"font-weight:bold\">Shipping Address";
		strxml += "</td></tr>" ;
		if(shippingadd1 !=null && shippingadd1 !=""){
			strxml += "<tr><td>"+shippingadd1+" </td></tr>" ;
		}
		if(shippingadd2 !=null && shippingadd2 !=""){
			strxml += "<tr><td>"+shippingadd2+"</td></tr>" ;
		}
		if((shippingadd3 !=null && shippingadd3 !="") ||(shippingadd4 !=null && shippingadd4 !="") ||(shippingadd5 !=null && shippingadd5 !="")  ){
			strxml += "<tr><td>"+shippingadd3+" , "+shippingadd4+","+shippingadd5+" </td></tr>" ;
		}
		strxml += "</table></td></tr><tr><td><table height='10'>";
		strxml += "<tr><td>&nbsp;</td></tr></table></td></tr><tr><td><table border='1' width='100%'  height='100'>";
		strxml += "<tr><td style=\"font-weight:bold\">Shipping From</td></tr>";
		if(attention !=null && attention !=""){
			strxml += "<tr><td>"+attention+" </td></tr>";
		}
		if(addressee !=null && addressee !=""){
			strxml += "<tr><td>"+addressee+"</td></tr>";
		}
		if(add1 !=null && add1 !=""){
			strxml += "<tr><td>"+add1+"</td></tr>";
		}
		if(add2 !=null && add2 !=""){
			strxml += "<tr><td>"+add2+"</td></tr>";
		}
		if((shipfromcity !=null && shipfromcity !="") ||(state !=null && state !="") ||(zip !=null && zip !="")  ){
			strxml += "<tr><td>"+shipfromcity+" "+state+" "+zip+" </td></tr>";
		}
		if(shipfromcountry !=null && shipfromcountry !=""){
			strxml += "<tr><td>"+shipfromcountry+" </td></tr>";
		}
		strxml += "</table></td></tr></table></td><td height='50%'><table border='1' width='100%' height='200px'><tr>";
		strxml += "<td rowspan='1' style=\"font-weight:bold\">Information</td></tr><tr><td style=\"font-weight:bold\">Delivery no.</td>";
		if(vQbFullfillmentName !=null && vQbFullfillmentName !=""){
			strxml += "<td>&nbsp;"+vQbFullfillmentName+"</td>" ;
		}
		strxml += "</tr><tr><td style=\"font-weight:bold\">Document Date</td>" ;
		if(sysdate !=null && sysdate !=""){
			strxml += "<td>&nbsp;"+sysdate+"</td>";}
		strxml += "</tr><tr><td style=\"font-weight:bold\">Customer PO No</td>" ;
		if(customerpono !=null && customerpono !=""){
			strxml += "<td>&nbsp;"+customerpono+"</td>";}
		strxml += "</tr><tr><td style=\"font-weight:bold\">Sales Order No</td>" ;
		if(soname !=null && soname !=""){
			strxml += "<td>&nbsp;"+soname+"</td>";}
		strxml += "</tr><tr><td style=\"font-weight:bold\">Customer No</td>" ;
		if(customerpo !=null && customerpo !=""){
			strxml += "<td>&nbsp;"+customerpo+"</td>" ;}
		strxml += "</tr><tr><td style=\"font-weight:bold\">";
		strxml += "Shipping Conditions</td>" ;
		if(shippingCondition !=null && shippingCondition !=""){
			strxml += "<td>&nbsp;"+shippingCondition+"</td>" ;}
		strxml += "</tr><tr><td style=\"font-weight:bold\">";
		strxml += "Inco Terms</td>" ;
		if(incoterms !=null && incoterms !=""){
			strxml += "<td>&nbsp;"+incoterms+"</td>";}
		strxml += "</tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr><tr><td>&nbsp;</td></tr></table></td></tr>";
		strxml += "</table>";

		strxml +="<table  border='1' width='100%'> ";
		strxml =strxml+  "<tr style=\"font-weight:bold\"><td  >";
		strxml += "Fulfillment Ord#";
		strxml += "</td>";
		strxml += "<td>";
		strxml += "Line #";
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		strxml += "<td>";
		strxml += "Item";
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		strxml += "<td>";
		strxml += "Description";
		strxml += "</td>";
		strxml += "<td>";
		/* code merged from monobid on feb 27th 2013 by Radhika */
		strxml += "LOT#";
		strxml += "</td>";
		strxml += "<td>";
		/* upto here */
		strxml += "Customer SKU";
		strxml += "</td>";	
		strxml += "<td>";			
		strxml += "UPC";
		strxml += "</td>";		
		strxml += "<td>";
		strxml += "Qty";
		strxml =strxml+  "</td></tr><tr> <td colspan='8' ></td> </tr>";
		var item,material,description,customersku='',quantity,upccode,itemdesc,fulfillment,lot='';
		if (linesearchresults != null) {
			for (var i = 0; i < linesearchresults.length; i++) {				 

				var linesearchresultscount = linesearchresults[i];
				item = linesearchresults[i].getValue('custrecord_line_no');
				fulfillment= linesearchresults[i].getValue('name');
				material = linesearchresults[i].getText('custrecord_sku');
				description = linesearchresults[i].getValue('custrecord_skudesc');
				//customer = linesearchresults.getValue('custrecord_sku');
				//skuno = linesearchresults.getValue('custrecord_ebiz_sku_no');
				quantity = linesearchresults[i].getValue('custrecord_act_qty');
				/* code merged from monobid on feb 27th 2013 by Radhika */				
				lot = linesearchresults[i].getValue('custrecord_batch_no');
				/* upto here */
				//upc			
				var upcfilters = new Array();
				//Changed as on 30/4/12 by suman.
				upcfilters.push(new nlobjSearchFilter('nameinternal', null, 'is', material));	
				upcfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
				//End of changes as on 30/4/12.

				var upccolumns = new Array();
				upccolumns[0] = new nlobjSearchColumn('upccode');
				upccolumns[1] = new nlobjSearchColumn('custitem_ebizdescriptionitems');
				var upcsearchresults = nlapiSearchRecord('item', null, upcfilters, upccolumns);
				if(upcsearchresults != null && upcsearchresults != ""){
					upccode=upcsearchresults[0].getValue('upccode');
					itemdesc=upcsearchresults[0].getValue('custitem_ebizdescriptionitems');
				}

				if(item != null && item !="")
					item=item.replace(replaceChar,'');
				else
					item="";

				if(material != null && material !="")
					material=material.replace(replaceChar,'');
				else
					material="";

				if(description != null && description !="")
					description=description.replace(replaceChar,'');
				else
					description="";



				strxml =strxml+  "<tr><td>";				
				strxml += fulfillment;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td>";
				strxml +=  item;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td>";
				strxml += material;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td>";
				strxml += itemdesc;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td>";
				/* code merged from monobid on feb 27th 2013 by Radhika */				
				strxml += lot;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td>";
				/* upto here */
				strxml += customersku;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";			
				strxml += "<td>";
				strxml += upccode;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td>";			
				strxml += quantity;				
				strxml =strxml+  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
			}
		}
		strxml =strxml+"</table>";		
		strxml =strxml+ "\n</body>\n</pdf>";		
		nlapiLogExecution('ERROR', 'strxml', strxml);
		xml=xml +strxml;
		nlapiLogExecution('ERROR','XML',xml);

		var file = nlapiXMLToPDF(xml);	
		response.setContentType('PDF','PackingList.pdf');
		response.write( file.getValue() );
	}
	else //this is the POST block
	{

	}
}
function getFullfillmentName(FullfillmentInternalid){ 
	var columns = nlapiLookupField('customrecord_ebiznet_ordline',FullfillmentInternalid, 'custrecord_lineord');	 
	nlapiLogExecution('ERROR', "Fullfillmentname", columns);	
	return columns;
}
