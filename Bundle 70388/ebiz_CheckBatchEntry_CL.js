
/***************************************************************************
                                                                 
   eBizNET Solutions Inc 
****************************************************************************
*
*  $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Masters/Client/ebiz_CheckBatchEntry_CL.js,v $
*  $Revision: 1.3.4.2.8.1.4.2 $
*  $Date: 2014/07/30 15:28:15 $
*  $Author: skavuri $
*  $Name: t_eBN_2014_2_StdBundle_0_11 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_CheckBatchEntry_CL.js,v $
*  Revision 1.3.4.2.8.1.4.2  2014/07/30 15:28:15  skavuri
*  Case# 20149573 SB Issue Fixed
*
*  Revision 1.3.4.2.8.1.4.1  2014/07/23 14:30:20  nneelam
*  case#  20149573
*  Restrict company of a different site..
*
*  Revision 1.3.4.2.8.1  2013/09/11 15:23:51  rmukkera
*  Case# 20124376
*
*
****************************************************************************/

function fnCheckBatchEntry(type)
{

    var varBatchName = nlapiGetFieldValue('name');
    var vSite = nlapiGetFieldValue('custrecord_ebizsitebatch');
    var vCompany = nlapiGetFieldValue('custrecord_ebizcompanybatch');
    var vItem=nlapiGetFieldValue('custrecord_ebizsku');
    if(varBatchName != "" && varBatchName != null && vSite != "" && vSite != null && vCompany != ""  && vCompany != null)
	{
    	//Case# 20149573 starts
		var vSiteText = nlapiGetFieldText('custrecord_ebizsitebatch');
		var vcompanyText = nlapiGetFieldText('custrecord_ebizcompanybatch');
		var column = new Array();
		column[0]=new nlobjSearchColumn('custrecordcompany');
		var filter = new Array();
		filter[0] = new nlobjSearchFilter('internalid', null, 'is', vSite);
		filter[1]=new nlobjSearchFilter('custrecordcompany',null, 'anyof',['@NONE@',vCompany]);
		var searchresult = nlapiSearchRecord('location',null, filter,column);
		if(searchresult == null || searchresult == ''){
			alert( vcompanyText + ' is not specific to Location ' + vSiteText);
			nlapiSetFieldValue('custrecord_ebizcompanybatch', '');
			return false;
		}
    	//Case# 20149573 ends
    var filters = new Array();
    filters[0] = new nlobjSearchFilter('name', null, 'is', varBatchName);
    filters[1] = new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', [vSite]);
    filters[2] = new nlobjSearchFilter('custrecord_ebizcompanybatch', null, 'anyof', [vCompany]);
    filters[3] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof',vItem);
    filters[4] = new nlobjSearchFilter('isinactive', null, 'is','F');
    
    var searchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filters);
    
    if (searchresults != null && searchresults.length > 0) 
	{
        alert("The LOT# already exists.");
        return false;
    }
    else {
    
        var varMfgDate = nlapiGetFieldValue('custrecord_ebizmfgdate');
        var varExpiryDate = nlapiGetFieldValue('custrecord_ebizexpirydate');
        var varFIFODate = nlapiGetFieldValue('custrecord_ebizfifodate');
        var varBestBeforeDate = nlapiGetFieldValue('custrecord_ebizbestbeforedate');
        
		if (varExpiryDate != '') 
		{
			var vExpiryCurrentDate = CompareDates(DateStamp(), varExpiryDate);
			
			if (varMfgDate != '') 
			{
				var vExpiryManufactureDate = CompareDates(varMfgDate, varExpiryDate);
			}
			
			if (varBestBeforeDate != '') 
			{
				var vExpiryBestBeforeDate = CompareDates(varBestBeforeDate, varExpiryDate);
			}
		}
		
		if (varMfgDate != '')
		{
			if (varBestBeforeDate != '') {
				var vManufactureBestBeforeDate = CompareDates(varMfgDate, varBestBeforeDate);
			}
		}
		
		
		
        if ((varMfgDate == "" && varExpiryDate == "" && varFIFODate == "") ||
        (varMfgDate == "" && varExpiryDate == "" && varFIFODate != "")) {
            alert("Enter Manufacture / Expiry Date");
            return false;
        }
        else 
			if (vExpiryCurrentDate == false || vExpiryManufactureDate == false || vExpiryBestBeforeDate == false) {
				alert("Expiry date cannot be less than Current Date / Manufacturing Date / Best Before Date");
                return false;
            }
            else 
                if (vManufactureBestBeforeDate == false) {
                    alert("Manufacture date should be less than Best Before Date");
                    return false;
                }
                else 
                    if (varMfgDate != "" && varExpiryDate == "" && varFIFODate == "") {
                        nlapiSetFieldValue('custrecord_ebizfifodate', varMfgDate);
                        return true;
                    }
                    else 
                        if (varMfgDate == "" && varExpiryDate != "" && varFIFODate == "") {
                            nlapiSetFieldValue('custrecord_ebizfifodate', varExpiryDate);
                            return true;
                        }
                        else 
                            if (varMfgDate != "" && varExpiryDate != "" && varFIFODate == "") {
                                nlapiSetFieldValue('custrecord_ebizfifodate', varExpiryDate);
                                return true;
                            }
                        else
                        {
                        	return true;
                        }
    }
	}
    else
	{
    	return true;
	}
}


function fnCheckBatchEntryEdit(type,name)
{

	var vId = nlapiGetFieldValue('id');
    var varBatchName = nlapiGetFieldValue('name');
    var vSite = nlapiGetFieldValue('custrecord_ebizsitebatch');
    var vCompany = nlapiGetFieldValue('custrecord_ebizcompanybatch');
    
    if(varBatchName != "" && varBatchName != null && vId != "" && vId != null && vSite != "" && vSite != null && vCompany != ""  && vCompany != null)
	{
    var filters = new Array();
    filters[0] = new nlobjSearchFilter('name', null, 'is', varBatchName);
	//filters[1] = new nlobjSearchFilter('internalid', null, 'noneof',vId);
    filters[1] = new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', [vSite]);
    filters[2] = new nlobjSearchFilter('custrecord_ebizcompanybatch', null, 'anyof', [vCompany]);
    filters[3] = new nlobjSearchFilter('isinactive', null, 'is','F');
    
    if(vId!=null && vId!="")
        {
    	    	filters[4] = new nlobjSearchFilter('internalid', null, 'noneof',vId);	
    	}
    
    var searchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filters);
    
    if (searchresults != null && searchresults.length > 0) 
	{
        alert("The LOT# already exists.");
        return false;
    }
    else 
	{
	
        var varMfgDate = nlapiGetFieldValue('custrecord_ebizmfgdate');
        var varExpiryDate = nlapiGetFieldValue('custrecord_ebizexpirydate');
        var varFIFODate = nlapiGetFieldValue('custrecord_ebizfifodate');
        var varBestBeforeDate = nlapiGetFieldValue('custrecord_ebizbestbeforedate');
        
		if (varExpiryDate != '') 
		{
			var vExpiryCurrentDate = CompareDates(DateStamp(),varExpiryDate);
			
			if (varMfgDate != '') 
			{
				var vExpiryManufactureDate = CompareDates(varMfgDate,varExpiryDate);
			}
			
			if (varBestBeforeDate != '') 
			{
				var vExpiryBestBeforeDate = CompareDates(varBestBeforeDate,varExpiryDate);
			}
		}
		
		if (varMfgDate != '')
		{
			if (varBestBeforeDate != '') {
				var vManufactureBestBeforeDate = CompareDates(varMfgDate, varBestBeforeDate);
			}
		}
		
		
        if ((varMfgDate == "" && varExpiryDate == "" && varFIFODate == "") ||
	        (varMfgDate == "" && varExpiryDate == "" && varFIFODate != "")) 
	{
	            alert("Enter Manufacture / Expiry Date");
	            return false;
        }
        else 
	    if (vExpiryCurrentDate == false || vExpiryManufactureDate == false || vExpiryBestBeforeDate == false) 
	    {
			alert("Expiry date cannot be less than Current Date / Manufacturing Date / Best Before Date");
                return false;
            }
            else 
                if (vManufactureBestBeforeDate == false) {
                    alert("Manufacture date should be less than Best Before Date");
                    return false;
                }
                else 
                    if (varMfgDate != "" && varExpiryDate == "" && varFIFODate == "") {
                        nlapiSetFieldValue('custrecord_ebizfifodate', varMfgDate);
                        return true;
                    }
                    else 
                        if (varMfgDate == "" && varExpiryDate != "" && varFIFODate == "") {
                            nlapiSetFieldValue('custrecord_ebizfifodate', varExpiryDate);
                            return true;
                        }
                        else 
                            if (varMfgDate != "" && varExpiryDate != "" && varFIFODate == "") {
                                nlapiSetFieldValue('custrecord_ebizfifodate', varExpiryDate);
                                return true;
                            }
                        else
                        {
                        	return true;
                        }
    }
	}
    else
	{
    	return true;
	}
}

function onchange(type,name,linenum)
{
	//Case# 20149573 starts
	/* if(name == 'custrecord_ebizcompanybatch'){
		//var vSite = nlapiGetFieldValue('custrecord_ebizsitebatch');
		var vSiteText = nlapiGetFieldText('custrecord_ebizsitebatch');
		var vcompany = nlapiGetFieldValue('custrecord_ebizcompanybatch');
		var vcompanyText = nlapiGetFieldText('custrecord_ebizcompanybatch');
		
	//	alert('vSite ' + vSite);
	//	alert('vSiteText ' + vSiteText);
	

		var column = new Array();
		column[0]=new nlobjSearchColumn('custrecordcompany');


		var filter = new Array();
		filter[0] = new nlobjSearchFilter('name', null, 'is', vSiteText);
		filter[1]=new nlobjSearchFilter('custrecordcompany',null, 'anyof',['@NONE@',vcompany]);

		var searchresult = nlapiSearchRecord('location',null, filter,column);
		
		if(searchresult == null || searchresult == ''){
			alert( vcompanyText + ' is not specific to Location ' + vSiteText);
			return false;
		}
		
		
	} */
	//Case# 20149573 ends
	
	if(name=='custrecord_ebizsku')	
	{
		var vSite = nlapiGetFieldValue('custrecord_ebizsitebatch');
		var vCompany = nlapiGetFieldValue('custrecord_ebizcompanybatch');
		var vitem = nlapiGetFieldValue('custrecord_ebizsku');
            
		var filters = new Array();

		var columns = new Array();

		columns[0]=new nlobjSearchColumn('custrecord_ebizfifodate');
		columns[1]=new nlobjSearchColumn('custrecord_ebizexpirydate');
		
		var i=0;
		if(vSite!=null && vSite!='')
		{
			
			filters[i] = new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', [vSite]);
			i++;
			
		}
		if(vCompany!=null && vCompany!='')
		{
		
			filters[i] = new nlobjSearchFilter('custrecord_ebizcompanybatch', null, 'anyof', [vCompany]);
			i++;

		}

		filters[i] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', [vitem]);
		
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry',null, filters,columns);
		if(searchresults!=null && searchresults.length>0)
		{
			
			var	expiryDate = searchresults[0].getValue('custrecord_ebizexpirydate');  
			var	FifoDate = searchresults[0].getValue('custrecord_ebizfifodate');  
		

			nlapiSetFieldValue('custrecord_ebizfifodate', FifoDate);
			nlapiSetFieldValue('custrecord_ebizexpirydate', expiryDate);
		}
		else
			{
			 nlapiSetFieldValue('custrecord_ebizfifodate', '');
			 nlapiSetFieldValue('custrecord_ebizexpirydate', '');
			}

	}
}
