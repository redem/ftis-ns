/***************************************************************************
   eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_WO_Picking_LocException.js,v $
 *     	   $Revision: 1.1.2.1.2.16 $
 *     	   $Date: 2015/08/11 15:35:28 $
 *     	   $Author: grao $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_WO_Picking_LocException.js,v $
 * Revision 1.1.2.1.2.16  2015/08/11 15:35:28  grao
 * 2015.2   issue fixes  201413821
 *
 * Revision 1.1.2.1.2.15  2015/08/07 16:03:29  grao
 * 2015.2   issue fixes  201413821
 *
 * Revision 1.1.2.1.2.14  2015/08/07 15:41:13  grao
 * 2015.2   issue fixes  201413821
 *
 * Revision 1.1.2.1.2.13  2015/06/29 15:40:36  skreddy
 * Case# 201413188
 * JB Prod issue fix
 *
 * Revision 1.1.2.1.2.12  2015/04/13 09:25:59  rrpulicherla
 * Case#201412277
 *
 * Revision 1.1.2.1.2.11  2015/03/13 15:29:07  skreddy
 * Case# 201412029
 * JB Prod issue fix
 *
 * Revision 1.1.2.1.2.10  2014/09/01 16:14:42  gkalla
 * case#201410172
 * JB Commented last pick task code
 *
 * Revision 1.1.2.1.2.9  2014/08/18 13:55:00  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * optimistic locking changes
 *
 * Revision 1.1.2.1.2.8  2014/08/15 15:36:36  snimmakayala
 * Case: 20149972 & 20149973
 * JAWBONE WO PICKING FIXES
 *
 * Revision 1.1.2.1.2.7  2014/06/13 08:57:21  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.1.2.6  2014/05/30 00:34:24  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.1.2.5  2013/12/04 16:17:07  skreddy
 * Case# 20126150
 * 2014.1 stnd bundle issue fix
 *
 * Revision 1.1.2.1.2.4  2013/09/19 15:18:30  rmukkera
 * Case# 20124446
 *
 * Revision 1.1.2.1.2.3  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.1.2.2  2013/03/19 11:48:12  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.1.2.1.2.1  2013/03/08 14:38:41  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.1.2.1  2013/02/18 06:26:50  skreddy
 * CASE201112/CR201113/LOG201121
 *  Qty exception in Wo Picking
 *
 * Revision 1.1.2.11  2012/09/11 00:45:23  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 *
 *****************************************************************************/

function WOPickingLocException(request, response)
{
	if (request.getMethod() == 'GET') 
	{
		//var getItemName="",getItemNo="",getQuantity="";

		var getWOid = request.getParameter('custparam_woid');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocation = request.getParameter('custparam_beginLocation');
		var getItem = request.getParameter('custparam_item');
		var getItemName = request.getParameter('custparam_itemdescription');
		var getItemNo = request.getParameter('custparam_iteminternalid');
		var getdoLoineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getBeginLocationName = request.getParameter('custparam_beginlocationname');
		var getEndLocationInternalId = request.getParameter('custparam_endlocinternalid');
		var getEndLocation = request.getParameter('custparam_endlocation');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var getClusterNo = request.getParameter('custparam_clusterno');
		var getBatchNo = request.getParameter('custparam_batchno');
		var getNoofRecords = request.getParameter('custparam_noofrecords');
		var getNextLocation = request.getParameter('custparam_nextlocation');
		var name = request.getParameter('name');
		var getRecCount = request.getParameter('custparam_RecCount');
		var getContainerSize = request.getParameter('custparam_containersize');
		var getEbizOrdNo = request.getParameter('custparam_ebizordno');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var whLocation = request.getParameter('custparam_whlocation');
		//parameters erquired if serializedinventoryitem
		var getnumber = request.getParameter('custparam_number');
		var getRecType = request.getParameter('custparam_RecType');
		var getSerOut = request.getParameter('custparam_SerOut');
		var getSerIn = request.getParameter('custparam_SerIn');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var getZoneNo=request.getParameter('custparam_ebizzoneno');
		nlapiLogExecution('ERROR', 'NextItemInternalId', NextItemInternalId);
		var pickType=request.getParameter('custparam_picktype');
		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');

		var NextExptdQty = request.getParameter('custparam_nextexpectedquantity');
		var itemType=request.getParameter('custparam_itemType');
		var ActBatchno = request.getParameter('custparam_Actbatchno');
		var ExpBatchno=request.getParameter('custparam_Expbatchno');

		nlapiLogExecution('ERROR', 'getRecordInternalId', getRecordInternalId);
		nlapiLogExecution('ERROR', 'getContainerLpNo', getContainerLpNo);
		nlapiLogExecution('ERROR', 'getQuantity', getQuantity);
		nlapiLogExecution('ERROR', 'getBeginLocation', getBeginLocation);
		nlapiLogExecution('ERROR', 'getItem', getItem);
		nlapiLogExecution('ERROR', 'getItemName', getItemName);
		nlapiLogExecution('ERROR', 'getItemNo', getItemNo);
		nlapiLogExecution('ERROR', 'getdoLoineId', getdoLoineId);
		nlapiLogExecution('ERROR', 'getInvoiceRefNo', getInvoiceRefNo);
		nlapiLogExecution('ERROR', 'getBeginLocationName', getBeginLocationName);
		nlapiLogExecution('ERROR', 'getEndLocationInternalId', getEndLocationInternalId);
		nlapiLogExecution('ERROR', 'getEndLocation', getEndLocation);
		nlapiLogExecution('ERROR', 'getOrderLineNo', getOrderLineNo);
		nlapiLogExecution('ERROR', 'getClusterNo', getClusterNo);
		nlapiLogExecution('ERROR', 'getBatchNo', getBatchNo);
		nlapiLogExecution('ERROR', 'getNoofRecords', getNoofRecords);
		nlapiLogExecution('ERROR', 'getNextLocation', getNextLocation);
		nlapiLogExecution('ERROR', 'name', name);
		nlapiLogExecution('ERROR', 'getRecCount', getRecCount);
		nlapiLogExecution('ERROR', 'getContainerSize', getContainerSize);
		nlapiLogExecution('ERROR', 'getEbizOrdNo', getEbizOrdNo);
		nlapiLogExecution('ERROR', 'NextExptdQty', NextExptdQty);


		nlapiLogExecution('ERROR', 'getnumber', getnumber);
		nlapiLogExecution('ERROR', 'getRecType', getRecType);
		nlapiLogExecution('ERROR', 'getSerOut', getSerOut);
		nlapiLogExecution('ERROR', 'getSerIn', getSerIn);

		nlapiLogExecution('ERROR', 'getBeginLocation', getBeginLocation);
		nlapiLogExecution('ERROR', 'getItemNo', getItemNo);
		nlapiLogExecution('ERROR', 'NextItemInternalId', NextItemInternalId);

		if(getBeginLocation==null)
		{
			getBeginLocation=getNextLocation;
		}
		if(getItemNo==null)
		{
			getItemNo=NextItemInternalId;
		}
		var LocArr=new Array();
		var IsPickFaceLoc = 'N';
		var LotId='';
		if(ActBatchno!='' && ActBatchno!=null)
		{
			LotId=GetBatchId(ActBatchno,getItemNo);
			nlapiLogExecution('ERROR', 'LotId', LotId);
		}


		var SOarray = new Array();

		SOarray["custparam_woid"] = request.getParameter('custparam_woid');
		SOarray["custparam_recordinternalid"] = request.getParameter('custparam_recordinternalid');
		SOarray["custparam_containerlpno"] = request.getParameter('custparam_containerlpno');
		SOarray["custparam_expectedquantity"] = request.getParameter('custparam_expectedquantity');
		SOarray["custparam_beginLocation"] = request.getParameter('custparam_beginLocation');
		SOarray["custparam_item"] = request.getParameter('custparam_item');
		SOarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
		SOarray["custparam_iteminternalid"] = request.getParameter('custparam_iteminternalid');
		SOarray["custparam_dolineid"] = request.getParameter('custparam_dolineid');
		SOarray["custparam_invoicerefno"] = request.getParameter('custparam_invoicerefno');
		SOarray["custparam_beginlocationname"] = request.getParameter('custparam_beginLocationname');
		SOarray["custparam_endlocinternalid"] = request.getParameter('custparam_endlocinternalid');
		SOarray["custparam_endlocation"] = request.getParameter('custparam_endlocation');
		SOarray["custparam_orderlineno"] = request.getParameter('custparam_orderlineno');
		SOarray["custparam_nextlocation"]=request.getParameter('custparam_nextlocation');
		SOarray["custparam_picktype"]=request.getParameter('custparam_picktype');
		SOarray["custparam_nooflocrecords"]=request.getParameter('custparam_nooflocrecords');
		SOarray["custparam_nextiteminternalid"]=request.getParameter('custparam_nextiteminternalid');
		SOarray["custparam_nextitem"]=request.getParameter('custparam_nextitem');
		SOarray["name"]=request.getParameter('name');
		SOarray["custparam_clusterno"] = request.getParameter('custparam_clusterno');        
		SOarray["custparam_number"]= request.getParameter('custparam_number');   
		SOarray["custparam_whlocation"]= request.getParameter('custparam_whlocation');   
		SOarray["custparam_skipid"] = request.getParameter('custparam_skipid');
		SOarray["custparam_ebizzoneno"] = request.getParameter('custparam_ebizzoneno');
		SOarray["custparam_nextexpectedquantity"] = request.getParameter('custparam_nextexpectedquantity');
		SOarray["custparam_itemType"] = itemType;
		SOarray["custparam_Actbatchno"]=request.getParameter('custparam_Actbatchno');
		SOarray["custparam_Expbatchno"] = request.getParameter('custparam_Expbatchno');
		SOarray["custparam_ebizordno"] = request.getParameter('custparam_ebizordno');




		IsPickFaceLoc = isPickFaceLocation(getItemNo,getEndLocationInternalId);
		if(IsPickFaceLoc=='Y')
		{

			var pfLocSearch = getPickFaceLocation(getItemNo,getEndLocationInternalId);
			if(pfLocSearch!=null && pfLocSearch!='' && pfLocSearch.length>0)
			{
				for(var j=0;j<pfLocSearch.length;j++)
				{
					LocArr.push(pfLocSearch[j].getValue('custrecord_pickbinloc'));
				}

				if(LocArr!=null && LocArr!='' && LocArr.length>0)
				{
					var arryinvt=getInvtDetails(getItemNo,LocArr);
					if(arryinvt==null || arryinvt=='')
					{
						var Loc = pfLocSearch[0].getText('custrecord_pickbinloc');
						SOarray["custparam_screenno"] = 'WOPickConfirm';
						SOarray["custparam_error"]='EXCEPTION IS NOT POSSIBLE AS THERE IS NO INVENTORY IN LOCATION '+Loc+' FOR THIS ITEM';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
					}
				}
			}
			else
			{
				SOarray["custparam_screenno"] = 'WOPickConfirm';
				SOarray["custparam_error"]='EXCEPTION IS NOT POSSIBLE AS THERE IS NO OTHER PICKFACE LOCATION FOR THIS ITEM';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				return;
			}
		}

		if(IsPickFaceLoc!='Y')
		{
			var arryinvt=PickStrategy(getItemNo, getQuantity,getEndLocationInternalId,whLocation,LotId);
		}

		if(arryinvt == null || arryinvt == '')
		{
			SOarray["custparam_screenno"] = 'WOPickConfirm';
			SOarray["custparam_error"]='AS PER PICKSTRATEGIES LOCATONS ARE NOT FOUND FOR THIS ITEM';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			return;

		}

		var functionkeyHtml=getFunctionkeyScript('_rf_pickinglocexception'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterloc').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head>";
		html = html + "<body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_pickinglocexception' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>Inventory availability for Item : <label>" + getItem + "</label>";
		html = html + "				<input type='hidden' name='hdnWOid' value=" + getWOid + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + getBeginLocation + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemName' value=" + getItemName + ">";
		html = html + "				<input type='hidden' name='hdnItemNo' value=" + getItemNo + ">";
		html = html + "				<input type='hidden' name='hdndoLoineId' value=" + getdoLoineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationName' value=" + getBeginLocationName + ">";
		html = html + "				<input type='hidden' name='hdnEndLocationInternalId' value=" + getEndLocationInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEndLocation' value=" + getEndLocation + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + getClusterNo + ">";
		html = html + "				<input type='hidden' name='hdnBatchNo' value=" + getBatchNo + ">";
		html = html + "				<input type='hidden' name='hdnNoofRecords' value=" + getNoofRecords + ">";
		html = html + "				<input type='hidden' name='hdnNextLocation' value=" + getNextLocation + ">";
		html = html + "				<input type='hidden' name='hdnname' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + getRecCount + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + getContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnEbizOrdNo' value=" + getEbizOrdNo + ">";
		html = html + "				<input type='hidden' name='hdnNextItemId' value=" + NextItemInternalId + ">";	
		html = html + "				<input type='hidden' name='hdnnumber' value=" + getnumber + ">";
		html = html + "				<input type='hidden' name='hdnRecType' value=" + getRecType + ">";
		html = html + "				<input type='hidden' name='hdnSerOut' value=" + getSerOut + ">";
		html = html + "				<input type='hidden' name='hdnSerIn' value=" + getSerIn + ">";
		html = html + "				<input type='hidden' name='hdnNextItemId' value=" + NextItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnflag'>";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdnZoneNo' value=" + getZoneNo + ">";
		html = html + "				<input type='hidden' name='hdnNextExptdQty' value=" + NextExptdQty + ">";
		html = html + "				<input type='hidden' name='hdnitemtype' value=" + itemType + ">";
		html = html + "				<input type='hidden' name='hdnactlot' value=" + ActBatchno + ">";
		html = html + "				<input type='hidden' name='hdnexplot' value=" + ExpBatchno + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr><td><table>";
		html = html + "			<tr>";
		html = html + "				<td>";
		html = html + "				<label><u>Loc</u></label>";
		html = html + "				</td>";
		html = html + "				<td>";
		html = html + "				<label><u>LoT</u></label>";
		html = html + "				</td>";
		html = html + "				<td>";
		html = html + "				<label><u>Qty</u></label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		var vInvtLoc='';
		var vInvtQty='';
		var vInvtRecId='';
		var vInvtLocId='';
		var vInvtLotId='';
		var vInvtLotText='';
		var vLot='';


		var vInvttotLoc='';
		var vInvttotQty='';
		var vInvttotRecId='';
		var vInvttotLocId='';
		var vInvttotLotId='';
		var vInvttotLotText='';
		var vtotLot='';
		var vInvtLP='';

		if(arryinvt != null && arryinvt != '' && arryinvt.length>0)
		{	
			for (var i = 0; i <  arryinvt.length ; i++) {
				var invtarray= arryinvt[i];

				if (invtarray != null)
				{
					var vtotLoc = invtarray[3];
					var vtotQty = invtarray[0];
					var vtotLot = invtarray[4];

					nlapiLogExecution('ERROR', 'vLoc', vLoc);
					nlapiLogExecution('ERROR', 'vQty', vQty);
					if(i==0)
					{
						if(arryinvt.length==1)
						{
							vInvttotLoc=vtotLoc+',';
							vInvttotQty=vtotQty+',';
							vInvttotRecId=invtarray[2]+',';
							vInvttotLocId=invtarray[1]+',';
							vInvttotLotId=invtarray[5]+',';
							vInvttotLotText=invtarray[4]+',';
							vInvtLP=invtarray[6]+',';
						}
						else
						{
							nlapiLogExecution('ERROR', 'vtotLoc1', vtotLoc);
							vInvttotLoc=vtotLoc;

							vInvttotQty=vtotQty;
							vInvttotRecId=invtarray[2];
							vInvttotLocId=invtarray[1];
							vInvttotLotId=invtarray[5];
							vInvttotLotText=invtarray[4];
							vInvtLP=invtarray[6];
						}
					}
					else
					{
						nlapiLogExecution('ERROR', 'vtotLoc2', vtotLoc);
						vInvttotLoc += ','+ vtotLoc;
						vInvttotQty+=','+vtotQty;
						vInvttotRecId+=','+invtarray[2];
						vInvttotLocId+=','+invtarray[1];
						vInvttotLotId+=','+invtarray[5];
						vInvttotLotText+=','+invtarray[4];
						vInvtLP+=','+invtarray[6];
					}	
				} 

			} 
		}

		nlapiLogExecution('ERROR', 'vInvttotLoc', vInvttotLoc);
		if(arryinvt != null && arryinvt != '' && arryinvt.length>0)
		{	
			for (var i = 0; i < Math.min(5, arryinvt.length); i++) {
				var invtarray= arryinvt[i];

				if (invtarray != null)
				{
					var vLoc = invtarray[3];
					var vQty = invtarray[0];
					var vLot = invtarray[4];
					var vLP = invtarray[6];

					html = html + "			<tr><td align = 'left' width='50%'>"+ vLoc +"</td>" ;
					html = html + "			<td align = 'left' width='15%'>"+ vLot +"</td>" ;
					html = html + "			<td align = 'left' width='15%'>"+ vQty.toFixed(4);
					html = html + "	</td></tr>";
					nlapiLogExecution('ERROR', 'vLoc', vLoc);
					nlapiLogExecution('ERROR', 'vQty', vQty);
					if(i==0)
					{
						if(arryinvt.length==1)
						{
							vInvtLoc=vLoc+',';
							vInvtQty=vQty+',';
							vInvtRecId=invtarray[2]+',';
							vInvtLocId=invtarray[1]+',';
							vInvtLotId=invtarray[5]+',';
							vInvtLotText=invtarray[4]+',';
							vInvtLP=invtarray[6]+',';
						}
						else
						{
							vInvtLoc=vLoc;
							vInvtQty=vQty;
							vInvtRecId=invtarray[2];
							vInvtLocId=invtarray[1];
							vInvtLotId=invtarray[5];
							vInvtLotText=invtarray[4];
							vInvtLP=invtarray[6];
						}
					}
					else
					{
						vInvtLoc += ','+ vLoc;
						vInvtQty+=','+vQty;
						vInvtRecId+=','+invtarray[2];
						vInvtLocId+=','+invtarray[1];
						vInvtLotId+=','+invtarray[5];
						vInvtLotText+=','+invtarray[4];
						vInvtLP+=','+invtarray[6];
					}	
				} 

			} 
		}

		html = html + "			</table></td></tr><tr>";
		html = html + "				<td align = 'left'>ENTER/SCAN LOCATION";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterloc' id='enterloc' type='text'/>";
		html = html + "				<input type='hidden' name='hdninvloc' value=" + vInvtLoc + ">";
		html = html + "				<input type='hidden' name='hdninvqty' value=" + vInvtQty + ">";
		html = html + "				<input type='hidden' name='hdninvrec' value=" + vInvtRecId + ">";
		html = html + "				<input type='hidden' name='hdninvlocid' value=" + vInvtLocId + ">";
		html = html + "				<input type='hidden' name='hdninvlotid' value=" + vInvtLotId + ">";
		html = html + "				<input type='hidden' name='hdninvlottext' value=" + vInvtLotText + ">";
		html = html + "				<input type='hidden' name='hdnvInvtLP' value=" + vInvtLP + ">";
		nlapiLogExecution('DEBUG', 'vInvttotLoc', vInvttotLoc);
		html = html + "				<input type='hidden' name='hdntotinvloc' value=" + vInvttotLoc + ">";
		html = html + "				<input type='hidden' name='hdntotinvqty' value=" + vInvttotQty + ">";
		html = html + "				<input type='hidden' name='hdntotinvrec' value=" + vInvttotRecId + ">";
		html = html + "				<input type='hidden' name='hdntotinvlocid' value=" + vInvttotLocId + ">";
		html = html + "				<input type='hidden' name='hdntotinvlotid' value=" + vInvttotLotId + ">";
		html = html + "				<input type='hidden' name='hdntotinvlottext' value=" + vInvttotLotText + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		/*	if(itemType=='lotnumberedinventoryitem')
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>ENTER/SCAN ACTUAL LOT#";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'><input name='enterlot' type='text'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
		}*/
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>SEND <input name='cmdConfirm' type='submit' value='ENT' onclick='this.form.hdnflag.value=this.value;this.form.submit();this.disabled=true; return false'/>";
		html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterloc').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('ERROR', 'SearchResults ', 'Length is not null');

		// Entered Location
		var getEnteredLocation = request.getParameter('enterloc');
		//var getEnteredLoT = request.getParameter('enterlot');
		var getInvLocs = request.getParameter('hdninvloc');
		var getInvQtys = request.getParameter('hdninvqty');
		var getInvRecs = request.getParameter('hdninvrec');
		var getInvLocsId = request.getParameter('hdninvlocid');
		var getInvLotsId = request.getParameter('hdninvlotid');
		var getInvLotText = request.getParameter('hdninvlottext');

		var getInvtotLocs = request.getParameter('hdntotinvloc');
		var getInvtotQtys = request.getParameter('hdntotinvqty');
		var getInvtotRecs = request.getParameter('hdntotinvrec');
		var getInvtotLocsId = request.getParameter('hdntotinvlocid');
		var getInvtotLotsId = request.getParameter('hdntotinvlotid');
		var getInvtotLotText = request.getParameter('hdntotinvlottext');

//		nlapiLogExecution('ERROR', 'getEnteredLocation', getEnteredLocation);
//		nlapiLogExecution('ERROR', 'getInvLocs', getInvLocs);
//		nlapiLogExecution('ERROR', 'getInvQtys', getInvQtys);
//		nlapiLogExecution('ERROR', 'getInvRecs', getInvRecs);
//		nlapiLogExecution('ERROR', 'getInvLocsId', getInvLocsId);
//		nlapiLogExecution('ERROR', 'getInvLotId', getInvLotsId);
//		nlapiLogExecution('ERROR', 'getInvLotText', getInvLotText);
//		nlapiLogExecution('ERROR', 'getInvtotLocs', getInvtotLocs);
//		nlapiLogExecution('ERROR', 'getInvtotQtys', getInvtotQtys);
//		nlapiLogExecution('ERROR', 'getInvtotRecs', getInvtotRecs);
//		nlapiLogExecution('ERROR', 'getInvtotLocsId', getInvtotLocsId);
//		nlapiLogExecution('ERROR', 'getInvtotLotsId', getInvtotLotsId);
//		nlapiLogExecution('ERROR', 'getInvtotLotText', getInvtotLotText);

		/*var getEnteredReason = request.getParameter('enterreason');
		nlapiLogExecution('ERROR', 'getEnteredReason', getEnteredReason);
		 */
		//var optedEvent = request.getParameter('cmdPrevious');

		var SOarray = new Array();

		var getwoid =request.getParameter('hdnWOid');
		var RecordInternalId = request.getParameter('hdnRecordInternalId');
		var ContainerLpNo = request.getParameter('hdnContainerLpNo');
		var FetchedQuantity = request.getParameter('hdnQuantity');
		var BeginLocation = request.getParameter('hdnBeginLocation');
		var Item = request.getParameter('hdnItem');
		var ItemName = request.getParameter('hdnItemName');
		var ItemNo = request.getParameter('hdnItemNo');
		var doLoineId = request.getParameter('hdndoLoineId');
		var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var BeginLocationName = request.getParameter('hdnBeginLocationName');
		var EndLocationInternalId = request.getParameter('hdnEndLocationInternalId');
		var EndLocation = request.getParameter('hdnEndLocation');
		var OrderLineNo = request.getParameter('hdnOrderLineNo');
		var ClusterNo = request.getParameter('hdnClusterNo');
		var BatchNo = request.getParameter('hdnBatchNo');
		var NoofRecords = request.getParameter('hdnNoofRecords');
		var NextLocation = request.getParameter('hdnNextLocation');
		var name = request.getParameter('hdnname');
		var RecCount = request.getParameter('hdnRecCount');
		var ContainerSize = request.getParameter('hdnContainerSize');
		var EbizOrdNo = request.getParameter('hdnEbizOrdNo');
		var NextItemInternalId=request.getParameter('hdnNextItemId');
		var NextExptdQty=request.getParameter('hdnNextExptdQty');
		var ItemType=request.getParameter('hdnitemtype');

		var number = request.getParameter('hdnnumber');
		var RecType = request.getParameter('hdnRecType');
		var SerOut = request.getParameter('hdnSerOut');
		var SerIn = request.getParameter('hdnSerIn');

		var Picktype = request.getParameter('hdnpicktype');
		var vZoneId=request.getParameter('hdnZoneNo');
		var nextexpqty = request.getParameter('hdnNextexpectedqty');
		SOarray["custparam_nextiteminternalid"] = request.getParameter('hdnNextItemId');
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		SOarray["custparam_whlocation"] = request.getParameter('hdnwhlocation');

//		nlapiLogExecution('ERROR', 'getFetchedQuantity', FetchedQuantity);
//		nlapiLogExecution('ERROR', 'RecordInternalId', RecordInternalId);
//		nlapiLogExecution('ERROR', 'ContainerLpNo', ContainerLpNo);
//		nlapiLogExecution('ERROR', 'FetchedQuantity', FetchedQuantity);
//		nlapiLogExecution('ERROR', 'BeginLocation', BeginLocation);
//		nlapiLogExecution('ERROR', 'Item', Item);
//		nlapiLogExecution('ERROR', 'ItemName', ItemName);
//		nlapiLogExecution('ERROR', 'ItemNo', ItemNo);
//		nlapiLogExecution('ERROR', 'doLoineId', doLoineId);
//		nlapiLogExecution('ERROR', 'InvoiceRefNo', InvoiceRefNo);
//		nlapiLogExecution('ERROR', 'BeginLocationName', BeginLocationName);
//		nlapiLogExecution('ERROR', 'EndLocationInternalId', EndLocationInternalId);
//		nlapiLogExecution('ERROR', 'EndLocation', EndLocation);
//		nlapiLogExecution('ERROR', 'OrderLineNo', OrderLineNo);
//		nlapiLogExecution('ERROR', 'ClusterNo', ClusterNo);
//		nlapiLogExecution('ERROR', 'BatchNo', BatchNo);
//		nlapiLogExecution('ERROR', 'NoofRecords', NoofRecords);
//		nlapiLogExecution('ERROR', 'NextLocation', NextLocation);
//		nlapiLogExecution('ERROR', 'name', name);
//		nlapiLogExecution('ERROR', 'ContainerSize', ContainerSize);
//		nlapiLogExecution('ERROR', 'EbizOrdNo', EbizOrdNo);
//		nlapiLogExecution('ERROR', 'number', number);
//		nlapiLogExecution('ERROR', 'RecType', RecType);
//		nlapiLogExecution('ERROR', 'SerOut', SerOut);
//		nlapiLogExecution('ERROR', 'SerIn', SerIn);
//		nlapiLogExecution('ERROR', 'Picktype', Picktype);
//		nlapiLogExecution('ERROR', 'vZoneId', vZoneId);

		//SOarray["custparam_enteredReason"] = getEnteredReason;
		//SOarray["custparam_fetchedQty"] = FetchedQuantity;
		SOarray["custparam_woid"] = request.getParameter('hdnWOid');
		SOarray["custparam_screenno"] = 'WOPickLocEXP';
		//SOarray["custparam_waveno"] = WaveNo;
		SOarray["custparam_recordinternalid"] = RecordInternalId;
		SOarray["custparam_containerlpno"] = ContainerLpNo;
		SOarray["custparam_expectedquantity"] = FetchedQuantity;
		SOarray["custparam_beginLocation"] = BeginLocation;
		SOarray["custparam_item"] = Item;
		SOarray["custparam_itemdescription"] = ItemName;
		SOarray["custparam_iteminternalid"] = ItemNo;
		SOarray["custparam_dolineid"] = doLoineId;
		SOarray["custparam_invoicerefno"] = InvoiceRefNo;
		SOarray["custparam_beginlocationname"] = BeginLocationName;
		SOarray["custparam_endlocinternalid"] = EndLocationInternalId;
		SOarray["custparam_endlocation"] = request.getParameter('custparam_endlocation');
		SOarray["custparam_orderlineno"] = OrderLineNo;
		SOarray["custparam_clusterno"] = ClusterNo;
		SOarray["custparam_batchno"] = BatchNo;
		SOarray["custparam_noofrecords"] = NoofRecords;
		SOarray["custparam_nextlocation"] = NextLocation;
		SOarray["name"] = name;
		SOarray["custparam_containersize"] = ContainerSize;
		SOarray["custparam_ebizordno"] = EbizOrdNo;
		SOarray["custparam_number"] = number;
		SOarray["custparam_RecType"] = RecType;
		SOarray["custparam_SerOut"] = SerOut;
		SOarray["custparam_SerIn"] = SerIn;
		SOarray["custparam_nextiteminternalid"] = request.getParameter('hdnNextItemId');
		SOarray["custparam_nextexpectedquantity"] = request.getParameter('hdnNextExptdQty');
		var vSkipId=request.getParameter('hdnskipid');
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');
		SOarray["custparam_ebizzoneno"] = request.getParameter('hdnZoneNo');
		SOarray["custparam_itemType"] = ItemType;
		SOarray["custparam_Actbatchno"]=request.getParameter('hdnactlot');
		SOarray["custparam_Expbatchno"] = request.getParameter('hdnexplot');
		nlapiLogExecution('ERROR', 'hdnNextItemId', request.getParameter('hdnNextItemId'));


		var vinventoryresults=PickStrategy(ItemNo, FetchedQuantity,EndLocationInternalId,SOarray["custparam_whlocation"],getInvLotsId);
		
		
		if(vinventoryresults!=null && vinventoryresults!='')
		{
			vinventoryresults=removeDuplicateLocations(vinventoryresults);
		}
		nlapiLogExecution('DEBUG', 'vinventoryresults after remove duplicate is ', vinventoryresults);
		
		
		
//		if the previous button 'F7' is clicked, it has to go to the previous screen 
		if (request.getParameter('cmdPrevious') == 'F7') {
			nlapiLogExecution('ERROR', 'Clicked on Previous', request.getParameter('cmdPrevious'));
			//response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_confirm', 'customdeploy_ebiz_rf_wo_pick_confirm_di', false, SOarray);			

		}
		else 
			nlapiLogExecution('ERROR', 'Clicked on Confirm', request.getParameter('cmdConfirm'));
		if (request.getParameter('hdnflag') == 'ENT') {

			var EmptyLotEntered='F';
			/*if(ItemType=='lotnumberedinventoryitem')
			{
				if(getEnteredLoT == null || getEnteredLoT == '')
				{
					EmptyLotEntered='T';
				}
			}*/
			if(getEnteredLocation == null || getEnteredLocation == '')
			{
				SOarray["custparam_error"] = 'PLEASE ENTER LOCATION';
				if(EmptyLotEntered=='T')
					SOarray["custparam_error"] = 'PLEASE ENTER LOT';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				nlapiLogExecution('ERROR', 'lOCATION NOT ENTERED');
			}
			else
			{	
				var tempflag='T';
				var errorflag='F';
				var vRecId='';
				var vLocId='';
				var vLotId='';
				var vNewLP='';
				//if(getInvtotLocsId != null && getInvtotLocsId  != "" && getInvtotLocsId.indexOf(',') != -1)
				/*if(getInvtotLocsId != null && getInvtotLocsId  != "")
				{	*/
				
				if(vinventoryresults != null && vinventoryresults != '' && vinventoryresults.length>0)
				{	
					/*nlapiLogExecution('DEBUG', 'Length', vinventoryresults.length);
					var vLocArr=getInvtotLocs.split(',');
					var vQtyArr=getInvtotQtys.split(',');
					var vRecArr=getInvtotRecs.split(',');
					var vLocIdArr=getInvtotLocsId.split(',');
					var vLotIdArr=getInvtotLotsId.split(',');
					var vLotTextArr=getInvtotLotText.split(',');
					var vNewLPArr=getvInvtLP.split(',');
					nlapiLogExecution('ERROR', 'vLotTextArr',vLotTextArr);
					var tempflag='F';
					var tempcount=0;

					var getEnteredBinLocId = GetBinlocationId(getEnteredLocation);
					nlapiLogExecution('ERROR', 'getLocId',getEnteredBinLocId);

					for(var p=0;p<vLocArr.length;p++)
					{
						nlapiLogExecution('ERROR', 'vLocArr[p]',vLocArr[p]);
						var vOldLoc=vLocArr[p].toUpperCase();
						if(vOldLoc== getEnteredLocation.toUpperCase())
						{
							tempflag='T';
							tempcount=p;
							vRecId=vRecArr[p];
							vLocId=vLocIdArr[p];
							vLotId=vLotIdArr[p];
							vNewLP=vNewLPArr[p];
							nlapiLogExecution('ERROR', 'vRecId',vRecId);
							nlapiLogExecution('ERROR', 'vLocId',vLocId);
							nlapiLogExecution('ERROR', 'vLotId',vLotId);
							break;
						}	
					}
					nlapiLogExecution('ERROR', 'tempcount',tempcount);
					/*if(ItemType=='lotnumberedinventoryitem')
					{
						tempflag='F';
						for(var s=0;s<vLotTextArr.length;s++)
						{
							if(vLotTextArr[s]==getEnteredLoT)
								if(tempcount==s)
									tempflag='T';
							}
						}
					}
*/
					
					
					
					for (var i1 = 0; i1 <  vinventoryresults.length ; i1++) 
					{
						var vinvtarray= vinventoryresults[i1];

						if (vinvtarray != null)
						{
							var vQtyArr = vinvtarray[0];
							var vRecArr = vinvtarray[1]; 
							var vLocIdArr = vinvtarray[2];
							var vLocArr = vinvtarray[3];
							var vLotIdArr = vinvtarray[5];							
							var vLotTextArr = vinvtarray[4];

							nlapiLogExecution('DEBUG', 'vQtyArr', vQtyArr);
							nlapiLogExecution('DEBUG', 'vRecArr', vRecArr);
							nlapiLogExecution('DEBUG', 'vLocIdArr', vLocIdArr);
							nlapiLogExecution('DEBUG', 'vtotLoc', vtotLoc);
							nlapiLogExecution('DEBUG', 'vLotIdArr', vLotIdArr);
							nlapiLogExecution('DEBUG', 'vLotTextArr', vLotTextArr);

							var tempcount=0;
							var vLotId=null;
							var tempflag='F';
							var templocflag = 'F';

							nlapiLogExecution('DEBUG', 'vLocArr', vLocArr);
							nlapiLogExecution('DEBUG', 'getEnteredLocation', getEnteredLocation);
							if(vLocArr==getEnteredLocation.toUpperCase())
							{
								templocflag='T';
								tempcount=i1;
								vRecId=vRecArr;
								vLocId=vLocIdArr;
								vLotId=vLotIdArr;
								nlapiLogExecution('DEBUG', 'vRecId',vRecId);
								nlapiLogExecution('DEBUG', 'vLocId',vLocId);
								nlapiLogExecution('DEBUG', 'vLotId',vLotId);
								break;
							}
							



							nlapiLogExecution('DEBUG', 'vLotTextArr', vLotTextArr);
						//	nlapiLogExecution('DEBUG', 'getEnteredLoT', getEnteredLoT);
							//var vOldLot=vLotTextArr[p1].toUpperCase();

						/*	if(getEnteredLoT!=null && getEnteredLoT!='')
							{

								if(vLotTextArr==getEnteredLoT)
								{
									tempflag='T';
									//break;
								}	
							}

							nlapiLogExecution('DEBUG', 'ItemType', ItemType);

							if(ItemType=='lotnumberedinventoryitem' || ItemType =='lotnumberedassemblyitem')
							{
								tempflag='F';
									if(getEnteredLoT!=null && getEnteredLoT!='')
									{
										if(vLotTextArr==getEnteredLoT)
										{
											nlapiLogExecution('DEBUG', 'tempcount', tempcount);
											nlapiLogExecution('DEBUG', 'i1', i1);
											if(tempcount==i1){
												tempflag='T';
												InvalidLot='F';
											}
										}
									}

								
							}*/



						}
					}
					if(templocflag=='F')
					{
						nlapiLogExecution('DEBUG', 'Invalid Location',getEnteredLocation);
						SOarray["custparam_error"] = "INVALID LOCATION / LOT#";//'INVALID LOCATION';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return false;
					}
					
					nlapiLogExecution('DEBUG', 'tempflag',tempflag);
					nlapiLogExecution('DEBUG', 'vLotId',vLotId);
					var opentaskcount=0;
					/*if(tempflag == 'T')   //this condition is removed because ,if we have other than lot item then tempflag never becomes 'T'
					{*/						
						var locfilters = new Array();	
						locfilters[0] = new nlobjSearchFilter('name', null, 'is', getEnteredLocation);
						var loccolumns = new Array();
						loccolumns[0] = new nlobjSearchColumn('name');						
						var locsearchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, locfilters, loccolumns);

						if(locsearchresults != null && locsearchresults != '')
						{	
							vLocId=locsearchresults[0].getId();
						}
						nlapiLogExecution('ERROR', 'else vLocId',vLocId);
						//vRecId
						var recfilters = new Array();	
						recfilters[0] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', vLocId);

						//code added on 17th July 2012 by suman.
						recfilters[1] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', ItemNo);
						recfilters[2] = new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0);
						recfilters[3] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['3','19']);//3=putaway complete 19-storage
						if(vLotId!=null&&vLotId!="")
							recfilters[4] = new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', vLotId);
						//end of code as of 17th July 2012.

						var reccolumns = new Array();
						reccolumns[0] = new nlobjSearchColumn('name');		
						reccolumns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
						var recsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, recfilters, reccolumns);

						if(recsearchresults != null && recsearchresults != '')
						{
							errorflag='T';
							vRecId=recsearchresults[0].getId();
							vNewLP=recsearchresults[0].getValue('custrecord_ebiz_inv_lp');
						}
					//}
				}
				nlapiLogExecution('DEBUG', 'errorflag',errorflag);
				if(errorflag=='T')
				{

					var RcId=RecordInternalId;
					var EndLocation = getEndLocationInternalId;
					var TotalWeight=0;
					var getReason=request.getParameter('custparam_enteredReason');
					var SORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordInternalId);
					var getContainerLPNo= SORec.getFieldValue('custrecord_container_lp_no');
					getLPContainerSize= SORec.getFieldText('custrecord_container');

					//SOarray["custparam_newcontainerlp"] = getContainerLPNo;
					var vPickType = request.getParameter('hdnpicktype');
					var vBatchno = request.getParameter('hdnbatchno');
					var PickQty = FetchedQuantity;
					var vActqty = FetchedQuantity;
					var SalesOrderInternalId = EbizOrdNo;
					var getContainerSize = getLPContainerSize;
					var  vdono = doLoineId;
					nlapiLogExecution('ERROR', 'getContainerLPNo', getContainerLPNo);
					var opentaskcount=0;

					var IsitLastPick='F';
					/*if(opentaskcount > parseFloat(vSkipId) + 1)
						IsitLastPick='F';
					else
						IsitLastPick='T';*/

					SOarray["custparam_entloc"] = getEnteredLocation;
					SOarray["custparam_recid"] = vRecId;
					SOarray["custparam_entlocid"] = vLocId; 


					opentaskcount=getOpenTasksCount(getwoid,vZoneId);
					ConfirmPickTask(ItemNo,getContainerSize,vdono,RcId,vActqty,getContainerLPNo,EndLocationInternalId,getReason,PickQty,vBatchno,
							IsitLastPick,null,null,SalesOrderInternalId,null,vLocId,vRecId);				

					var filterOpentask = new Array();
					filterOpentask.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
					filterOpentask.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));

					if(getwoid != null && getwoid != "")
					{
						filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', getwoid));
					}
					if(vZoneId!=null && vZoneId!="" && vZoneId!="null")
					{
						filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', vZoneId));
					}

					var SOColumns=new Array();
					SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
					SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
					SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_skiptask')); 
					SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
					SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
					SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
					SOColumns.push(new nlobjSearchColumn('name'));
					SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
					SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));

					SOColumns[1].setSort();
					SOColumns[2].setSort();
					SOColumns[3].setSort();
					SOColumns[4].setSort(true);


					var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterOpentask, SOColumns);
					nlapiLogExecution('ERROR', 'Results', SOSearchResults);
					if (SOSearchResults != null && SOSearchResults.length > 0) 
					{
						nlapiLogExecution('ERROR', 'SOSearchResults length', SOSearchResults.length);

						var SOSearchResult = SOSearchResults[0];							
						SOarray["custparam_woid"] = getwoid;
						SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
						SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
						SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
						SOarray["custparam_beginLocation"] = SOSearchResult.getText('custrecord_actbeginloc');
						SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
						SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
						SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
						SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
						SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
						SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
						SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
						SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
						SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
						SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
						SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
						SOarray["custparam_noofrecords"] = SOSearchResults.length;		
						SOarray["name"] =  SOSearchResult.getValue('name');
						SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
						SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
						NextBatch=SOSearchResult.getValue('custrecord_batch_no');

						if(SOSearchResults.length > 1)
						{
							var SOSearchnextResult = SOSearchResults[1];
							SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
							SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
							SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
							SOarray["custparam_nextbatchno"] = SOSearchnextResult.getValue('custrecord_batch_no');
							nlapiLogExecution('ERROR', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
							nlapiLogExecution('ERROR', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));	
							nlapiLogExecution('ERROR', 'Next Qty', SOSearchnextResult.getValue('custrecord_expe_qty'));	
						}
						else
						{
							var SOSearchnextResult = SOSearchResults[0];
							SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
							SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
							SOarray["custparam_nextbatchno"] = SOSearchnextResult.getValue('custrecord_batch_no');
							nlapiLogExecution('ERROR', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
							nlapiLogExecution('ERROR', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));	
						}

					}	

					if(opentaskcount >  1)
					{

						nlapiLogExecution('ERROR', 'BeginLocation',BeginLocation);
						nlapiLogExecution('ERROR', 'NextLocation',NextLocation);
						nlapiLogExecution('ERROR', 'ItemNo',ItemNo);
						nlapiLogExecution('ERROR', 'NextItemInternalId',NextItemInternalId);
						nlapiLogExecution('ERROR', 'BatchNo', BatchNo);
						nlapiLogExecution('ERROR', 'NextBatch', NextBatch);

						if(BeginLocation != NextLocation)
						{ 
							SOarray["custparam_beginLocationname"]=NextLocation;
							SOarray["custparam_iteminternalid"]=NextItemInternalId;
							SOarray["custparam_expectedquantity"]=null;
							nlapiLogExecution('ERROR', 'Navigating To1', 'Picking Location');
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_location', 'customdeploy_ebiz_rf_wo_picking_loc_di', false, SOarray);								

						}
						else if(ItemNo != NextItemInternalId)
						{ 
							nlapiLogExecution('ERROR', 'Navigating To1', 'Picking Item');
							SOarray["custparam_iteminternalid"] = NextItemInternalId;
							SOarray["custparam_beginLocationname"]=null;
							SOarray["custparam_expectedquantity"]=null;
							//SOarray["custparam_iteminternalid"]=null;
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_item', 'customdeploy_ebiz_rf_wo_picking_item_di', false, SOarray);								

						}  //case 20126150�start :changed NextShowItem to NextItemInternalId  
						else if(ItemNo == NextItemInternalId)
						{ //case 20126150�end
							if(NextBatch == BatchNo)
							{
								SOarray["custparam_iteminternalid"] = NextItemInternalId;
								SOarray["custparam_beginLocationname"]=null;
								SOarray["custparam_expectedquantity"]=nextexpqty;
								nlapiLogExecution('ERROR', 'Navigating To1', 'Same Item and  same Batch');
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_confirm', 'customdeploy_ebiz_rf_wo_pick_confirm_di', false, SOarray);
							}
							else
							{
								nlapiLogExecution('ERROR', 'Navigating To1', 'Same Item ,Diff Batch');
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_batch', 'customdeploy_ebiz_rf_wo_picking_batch_di', false, SOarray);
							}

						}

					}
					else{


						nlapiLogExecution('ERROR', 'Navigating To1', 'Stage Location');
						response.sendRedirect('SUITELET', 'customscript_rf_wo_picking_stage_loc', 'customdeploy_rf_wo_picking_stage_loc_di', false, SOarray);					
					}


				}
				else
				{
					SOarray["custparam_error"] = 'INVALID LOCATION';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				}
			}
		}
	}
}


var tempPikzoneResultsArray=new Array();
function getpikzone(maxno,whLocation)
{


	var filterszone = new Array();	
	//filterszone[0] = new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'anyof', [vpickzone]);
	filterszone[0] = new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', ['@NONE@',whLocation]);
	filterszone[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
	if(maxno!=-1)
	{
		filterszone[2] = new nlobjSearchFilter('id', null,'lessthan', maxno);
	}


	var columnzone = new Array();

	columnzone[0] = new nlobjSearchColumn('id');
	columnzone[0].setSort(true);
	columnzone[1] = new nlobjSearchColumn('custrecordcustrecord_putzoneid');
	columnzone[2] = new nlobjSearchColumn('custrecord_locgroup_no');
	columnzone[3] = new nlobjSearchColumn('custrecord_zone_seq');//.setSort();				

	var searchresults = nlapiSearchRecord('customrecord_zone_locgroup', null, filterszone, columnzone);

	if(searchresults!=null)
	{
		if(searchresults.length>=1000)
		{	
			var maxno1=searchresults[searchresults.length-1].getId();	
			for (var y = 0;  y < searchresults.length; y++) 
			{
				tempPikzoneResultsArray.push(searchresults[y]);
			}
			getpikzone(maxno1,whLocation);
		}
		else
		{
			for (var y = 0;  y < searchresults.length; y++) 
			{
				tempPikzoneResultsArray.push(searchresults[y]);
			}
		}
	}

	nlapiLogExecution('ERROR', 'tempPikzoneResultsArray', tempPikzoneResultsArray.length);
	return tempPikzoneResultsArray;


}


var tempLocGrouResultsArray=new Array();
function getLocGroup(maxno,item,OldLocationInternalId,LotId)
{

	nlapiLogExecution('ERROR', 'LotId', LotId);
	var filtersinvt = new Array();

	filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', item));
	filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['19']));
	if(OldLocationInternalId != null && OldLocationInternalId != '')
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'noneof',OldLocationInternalId));
	//filtersinvt.push(new nlobjSearchFilter('custrecord_outboundlocgroupid','custrecord_ebiz_inv_binloc', 'is', vlocgroupno));				 

	if(LotId != null && LotId != '')
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof',LotId));


	if(maxno!=-1)
	{
		filtersinvt.push(new nlobjSearchFilter('id', null,'lessthan', maxno));
	}
	//nlapiLogExecution('ERROR', "custrecord_ebiz_inv_sku: " + item);

	var columnsinvt = new Array();

	columnsinvt.push(new nlobjSearchColumn('id'));
	columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_inv_fifo'));					
	columnsinvt.push(new nlobjSearchColumn('custrecord_pickseqno'));
	columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_inv_binloc'));
	columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_qoh'));
	columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_inv_qty'));
	columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_alloc_qty'));
	columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_inv_lp'));				 
	columnsinvt.push(new nlobjSearchColumn('custrecord_ebiz_inv_lot'));
	columnsinvt.push(new nlobjSearchColumn('custrecord_outboundlocgroupid','custrecord_ebiz_inv_binloc'));

	columnsinvt[0].setSort(true);
	columnsinvt[1].setSort();
	columnsinvt[2].setSort();				
	columnsinvt[3].setSort();
	columnsinvt[4].setSort(false);
	columnsinvt[5].setSort();

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);


	if(searchresults!=null)
	{
		if(searchresults.length>=1000)
		{	
			var maxno1=searchresults[searchresults.length-1].getId();	
			for (var y = 0;  y < searchresults.length; y++) 
			{
				tempLocGrouResultsArray.push(searchresults[y]);
			}
			getLocGroup(maxno,item,OldLocationInternalId);
		}
		else
		{
			for (var y = 0;  y < searchresults.length; y++) 
			{
				tempLocGrouResultsArray.push(searchresults[y]);
			}
		}
	}

	nlapiLogExecution('ERROR', 'tempLocGrouResultsArray', tempLocGrouResultsArray.length);
	return tempLocGrouResultsArray;


}

function PickStrategy(item,avlqty,OldLocationInternalId,whLocation,LotId){
	//alert("5");
	var actallocqty = 0;
	var Resultarray = new Array();
	nlapiLogExecution('ERROR','item',item); 

	var fields = ['custitem_item_family','custitem_item_group','custitem_ebizdefskustatus',
	              'custitem_item_info_1','custitem_item_info_2','custitem_item_info_3','custitem_ebizabcvelitem'];


	var columns= nlapiLookupField('item',item,fields);
	var ItemFamily = columns.custitem_item_family;
	var ItemGroup = columns.custitem_item_group;
	var ItemInfo1 = columns.custitem_item_info_1;
	var ItemInfo2 = columns.custitem_item_info_2;
	var ItemInfo3 = columns.custitem_item_info_3;

	nlapiLogExecution('ERROR', 'Item', item);
	nlapiLogExecution('ERROR', 'ItemFamily', ItemFamily);
	nlapiLogExecution('ERROR', 'ItemGroup', ItemGroup);
	nlapiLogExecution('ERROR', 'ItemInfo1', ItemInfo1);
	nlapiLogExecution('ERROR', 'ItemInfo2', ItemInfo2);
	nlapiLogExecution('ERROR', 'ItemInfo3', ItemInfo3);
	nlapiLogExecution('ERROR', 'whLocation', whLocation);

	var filters = new Array();	
	filters.push(new nlobjSearchFilter('custrecord_ebizskupickrul', null, 'anyof', ['@NONE@',item]));
	filters.push(new nlobjSearchFilter('custrecord_ebizsitepickrule', null, 'anyof', ['@NONE@',whLocation]));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	if(ItemFamily!=null && ItemFamily != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskufamilypickrul', null, 'anyof', ['@NONE@', ItemFamily]));
	}

	if(ItemGroup!=null && ItemGroup != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskugrouppickrul', null, 'anyof', ['@NONE@', ItemGroup]));
	}

	if(ItemInfo1!=null && ItemInfo1 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo1', null, 'anyof',['@NONE@', ItemInfo1]));
	}

	if(ItemInfo2!=null && ItemInfo2 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo2', null, 'anyof',['@NONE@', ItemInfo2]));
	}

	if(ItemInfo3!=null && ItemInfo3 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo3', null, 'anyof',['@NONE@', ItemInfo3]));
	}

	var k=1; 

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_ebizpickzonerul');
	columns[2] = new nlobjSearchColumn('custrecord_ebizpickmethod');
	//columns[3] = new nlobjSearchColumn('custrecord_ebizsequencenopickrul').setSort();
	columns[3]=new nlobjSearchColumn('formulanumeric');
	columns[3].setFormula("TO_NUMBER({custrecord_ebizsequencenopickrul})").setSort();

	//Fetching pick rule
	nlapiLogExecution('ERROR','Pick rule Fectching');
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters, columns);
	var vTotRecCount=0;
	if(searchresults != null && searchresults != '')
	{	
		nlapiLogExecution('ERROR','whLocation',whLocation);	
		var searchresultszone=getpikzone(-1,whLocation);
		nlapiLogExecution('ERROR','searchresultszone',searchresultszone.length);	

		var searchresultsinvt=getLocGroup(-1,item,OldLocationInternalId,LotId);
		nlapiLogExecution('ERROR','searchresultsinvt',searchresultsinvt.length);

		nlapiLogExecution('ERROR','pick rule searchresults',searchresults.length);
		for (var i = 0;  i < searchresults.length; i++) {

			var searchresultpick = searchresults[i];				
			var vpickzone=searchresultpick.getValue('custrecord_ebizpickzonerul');			

			//nlapiLogExecution('ERROR', "LP: " + LP);
			//nlapiLogExecution('ERROR','PickZone',vpickzone);
			nlapiLogExecution('ERROR','vpickzone',vpickzone);	
			if(vpickzone != null && vpickzone != '')
			{
				for (var j = 0; searchresultszone != null && j < searchresultszone.length; j++) {
					var pikzone = searchresultszone[j].getValue('custrecordcustrecord_putzoneid');
					nlapiLogExecution('ERROR','pikzone',pikzone);	
					//if(vpickzone == searchresultszone[j][1])
					if(vpickzone == pikzone	)
					{
						var searchresultzone = searchresultszone[j];
						var vlocgroupno = searchresultzone.getValue('custrecord_locgroup_no');
						//var vlocgroupno = searchresultzone[2];
						nlapiLogExecution('ERROR','Loc Group Fetching',vlocgroupno);
						var vOldLocation='';
						var vOldRec='';
						var vOldLocId='';
						var vTotRemQty=0;
						var vIntCount=0;
						var vOldlot='';
						var vOldlotId='';
						var vOldlp='';
						for (var l = 0; searchresultsinvt != null && l < searchresultsinvt.length; l++) {

							var LocGrp = searchresultsinvt[l].getValue('custrecord_outboundlocgroupid','custrecord_ebiz_inv_binloc');
							//var LocGrp = searchresultsinvt[l].getValue(columns[9]);

							//nlapiLogExecution('ERROR','Loc Group Fetching new',LocGrp);
							if(vlocgroupno == LocGrp)
							{
								nlapiLogExecution('ERROR','entered1',LocGrp);
								var searchresult = searchresultsinvt[l];
								var actqty = searchresult.getValue('custrecord_ebiz_qoh');
								var allocqty = searchresult.getValue('custrecord_ebiz_alloc_qty');
								var LP = searchresult.getValue('custrecord_ebiz_inv_lp');
								var vactLocation = searchresult.getValue('custrecord_ebiz_inv_binloc');
								var vactLocationtext = searchresult.getText('custrecord_ebiz_inv_binloc');
								var vlotno = searchresult.getValue('custrecord_ebiz_inv_lot');
								var vlotText = searchresult.getText('custrecord_ebiz_inv_lot');
								var Recid = searchresult.getId();  

								if (isNaN(allocqty)) {
									allocqty = 0;
								}
								if (allocqty == "") {
									allocqty = 0;
								}
								if( parseFloat(actqty)<0)
								{
									actqty=0;
								}

								var remainqty=0;

								//Added by Ganesh not to allow negative or zero Qtys 
								if(parseFloat(actqty) >=0 && parseFloat(allocqty)>=0)
									remainqty = parseFloat(actqty) - parseFloat(allocqty);
								nlapiLogExecution('ERROR', 'remainqty:',remainqty); 
								nlapiLogExecution('ERROR', 'avlqty:',avlqty); 
								var cnfmqty;


								if (parseFloat(remainqty) >= parseFloat(avlqty)) { 
									nlapiLogExecution('ERROR', 'vOldLocation:',vOldLocation);
									nlapiLogExecution('ERROR', 'vactLocationtext:',vactLocationtext);
									if(vOldLocation!=vactLocationtext)
									{
										if(vIntCount==0)
										{
											vTotRemQty=remainqty;
											vOldRec=Recid;
											vOldLocation=vactLocationtext;
											vOldLocId=vactLocation;
											vOldlot=vlotText;
											vOldlotId=vlotno;
											vIntCount=1;
											vOldlp=LP;
										}
										else
										{
											//nlapiLogExecution('ERROR', 'vTotRemQty :',vTotRemQty); 
											//nlapiLogExecution('ERROR', 'vOldLocation:',vOldLocation); 
											//nlapiLogExecution('ERROR', 'vTotRecCount :',vTotRecCount); 
											var invtarray = new Array();
											////alert('cnfmqty3 ' +cnfmqty);
											invtarray[0]= vTotRemQty; 
											invtarray[1]= vOldLocId; 
											invtarray[2] = vOldRec; 
											invtarray[3] =vOldLocation; 
											invtarray[4] =vOldlot;
											invtarray[5] =vOldlotId;
											invtarray[6] =vOldlp;
											//nlapiLogExecution('ERROR', 'RecId:',vOldRec); 
											Resultarray.push(invtarray);
											vTotRecCount=parseFloat(vTotRecCount) +1;
											//if(vTotRecCount ==5)
											//return Resultarray;
											vOldLocation=vactLocationtext;
											vTotRemQty=remainqty;
											vOldLocId=vactLocation;
											vOldRec=Recid;
											vOldlot=vlotText;
											vOldlotId=vlotno;
											vOldlp=LP;
										}								

									}
									else if(vOldlot==vlotText)
									{
										vTotRemQty= parseFloat(vTotRemQty) + parseFloat(remainqty);

									}
									else
									{
										var invtarray = new Array();
										////alert('cnfmqty3 ' +cnfmqty);
										invtarray[0]= vTotRemQty; 
										invtarray[1]= vOldLocId; 
										invtarray[2] = vOldRec; 
										invtarray[3] =vOldLocation; 
										invtarray[4] =vOldlot;
										invtarray[5] =vOldlotId;
										//nlapiLogExecution('ERROR', 'RecId:',vOldRec); 
										Resultarray.push(invtarray);
										vTotRecCount=parseInt(vTotRecCount) +1;
										//	if(vTotRecCount ==5)
										//	return Resultarray;
										vOldLocation=vactLocationtext;
										vTotRemQty=remainqty;
										vOldLocId=vactLocation;
										vOldRec=Recid;
										vOldlot=vlotText;
										vOldlotId=vlotno;
										vOldlp=LP;
									}
								} 
							}
						} 
						if(vTotRemQty != null && vTotRemQty != '' && vTotRemQty != '0' && vTotRemQty != 0)
						{	nlapiLogExecution('ERROR','entered2',LocGrp);
						//nlapiLogExecution('ERROR', 'vTotRemQty in out :',vTotRemQty); 
						//nlapiLogExecution('ERROR', 'vOldLocation:',vOldLocation); 
						//nlapiLogExecution('ERROR', 'vTotRecCount in out :',vTotRecCount); 

						var invtarray = new Array();
						////alert('cnfmqty3 ' +cnfmqty);
						invtarray[0]= vTotRemQty; 
						invtarray[1]= vOldLocId; 
						invtarray[2] = vOldRec; 
						invtarray[3] =vOldLocation; 
						invtarray[4] =vOldlot;
						invtarray[5] =vOldlotId;
						invtarray[6] =vOldlp;
						//nlapiLogExecution('ERROR', 'RecId:',vOldRec); 
						Resultarray.push(invtarray);
						vTotRecCount=parseFloat(vTotRecCount) +1;				 
						vOldLocation='';
						vTotRemQty='';
						vOldLocId='';
						vOldRec='';
						vOldlot='';
						vOldlotId='';
						vOldlp="";
						}
					}
				}
			}
		} 
	}
	nlapiLogExecution('ERROR','Resultarray',Resultarray);
	return Resultarray;  
}

function getPickFaceLocation(item,location)
{
	nlapiLogExecution('ERROR', 'Into getPickFaceLocation');
	var PickFaceSearchResults = new Array();
	var PickFaceFilters = new Array();
	var PickFaceColumns = new Array();

	PickFaceFilters.push(new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', item)); 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_pickbinloc', null, 'noneof', location)); 
	PickFaceFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	PickFaceColumns[0] = new nlobjSearchColumn('custrecord_pickbinloc');

	PickFaceSearchResults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, PickFaceFilters, PickFaceColumns);

	nlapiLogExecution('ERROR', 'Return Value',PickFaceSearchResults);
	nlapiLogExecution('ERROR', 'Out of getPickFaceLocation');

	return PickFaceSearchResults;
}

function getInvtDetails(item,location)
{
	nlapiLogExecution('ERROR', 'Into getInvtDetails');
	var InvtSearchResults = new Array();
	var InvtResultsArr = new Array();
	var InvtFilters = new Array();
	var InvtColumns = new Array();

	InvtFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', item)); 
	InvtFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', location)); 
	InvtFilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['3','19']));

	InvtColumns[0] = new nlobjSearchColumn('custrecord_ebiz_avl_qty');
	InvtColumns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');

	InvtSearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, InvtFilters, InvtColumns);

	if(InvtSearchResults!=null && InvtSearchResults!='' && InvtSearchResults.length>0)
	{
		for(var g=0;g<InvtSearchResults.length;g++)
		{
			var invtarray = new Array();
			invtarray[0] = InvtSearchResults[g].getValue('custrecord_ebiz_avl_qty');
			invtarray[1] = InvtSearchResults[g].getValue('custrecord_ebiz_inv_binloc');
			invtarray[2] = InvtSearchResults[g].getId();
			invtarray[3] = InvtSearchResults[g].getText('custrecord_ebiz_inv_binloc');
			InvtResultsArr.push(invtarray);
		}
	}

	nlapiLogExecution('ERROR', 'Return Value',InvtResultsArr);
	nlapiLogExecution('ERROR', 'Out of getInvtDetails');

	return InvtResultsArr;
}






function GetBinlocationId(binLocation)
{
	nlapiLogExecution('ERROR', 'GetBinlocationId',binLocation);
	var binlocationId;
	var Filters = new Array();
	Filters.push(new nlobjSearchFilter('name', null, 'is', binLocation)); 

	var SearchResults = nlapiSearchRecord('customrecord_ebiznet_location', null, Filters, null);

	if(SearchResults!=null && SearchResults!='' && SearchResults.length>0)
	{
		binlocationId = SearchResults[0].getId();

	}
	nlapiLogExecution('ERROR', 'binlocationId',binlocationId);

	return binlocationId;
}


function GetBatchId(vbatchno,Itemno)
{
	nlapiLogExecution('ERROR', 'vbatchno',vbatchno);
	var batchId;
	var Filters = new Array();
	Filters.push(new nlobjSearchFilter('name', null, 'is', vbatchno)); 
	Filters.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', Itemno)); 

	var SearchResults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, Filters, null);

	if(SearchResults!=null && SearchResults!='' && SearchResults.length>0)
	{
		batchId = SearchResults[0].getId();
	}
	nlapiLogExecution('ERROR', 'batchId',batchId);
	return batchId;
}



function ConfirmPickTask(ItemInternalId,getContainerSize,vdono,RcId,vActqty,getContainerLPNo,EndLocation,getReason,PickQty,
		vBatchno,IsitLastPick,remainqty,kititemTypesku,SalesOrderInternalId,vkititem,vNewLocId,vNewRecId,vNewLP)
{

	nlapiLogExecution('ERROR', 'ConfirmPickTask', 'sucess');

	var arrDims = getSKUCubeAndWeightforconfirm(ItemInternalId, 1);
	var itemCube = 0;
	var itemWeight=0;	

	if (arrDims[0] != "" && (!isNaN(arrDims[1]))){
		//	these are commented because here  PickQty is excepted qty but we have to cal based on act qty
		itemCube = (parseFloat(PickQty) * parseFloat(arrDims[0]));
		itemWeight = (parseFloat(PickQty) * parseFloat(arrDims[1]));//	


	} 
	nlapiLogExecution('ERROR', 'checkInPOSTRequest:itemCube', itemCube);
	nlapiLogExecution('ERROR', 'checkInPOSTRequest:itemweight', itemWeight);
	nlapiLogExecution('ERROR', 'ContainerSize:ContainerSize', getContainerSize);		


	var ContainerCube;					
	var containerInternalId;
	var ContainerSize;
	var vRemaningqty=0;
	nlapiLogExecution('ERROR', 'getContainerSize', getContainerSize);	

	if(getContainerSize=="" || getContainerSize==null)
	{
		getContainerSize=ContainerSize;
		nlapiLogExecution('ERROR', 'ContainerSize', ContainerSize);	
	}
	nlapiLogExecution('ERROR', 'getContainerSize', getContainerSize);	
	var arrContainerDetails = getContainerCubeAndTarWeight("",getContainerSize);					
	if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) {

		ContainerCube =  parseFloat(arrContainerDetails[0]);						
		containerInternalId = arrContainerDetails[3];
		ContainerSize=arrContainerDetails[4];
		TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));//now added
	} 
	nlapiLogExecution('ERROR', 'ContainerSizeInternalId', containerInternalId);	
	nlapiLogExecution('ERROR', 'ContainerSize', ContainerSize);	

	//UpdateOpenTask,fulfillmentorder
	nlapiLogExecution('ERROR', 'vdono', vdono);	

	UpdateRFOpenTask(RcId,vActqty, containerInternalId,getContainerLPNo,itemCube,itemWeight,EndLocation,
			getReason,PickQty,IsitLastPick,remainqty,vNewLocId,vNewRecId,vNewLP);
	//UpdateRFFulfillOrdLine(vdono,vActqty);

	//create new record in opentask,fullfillordline when we have qty exception
	//vRemaningqty = vDisplayedQty-vActqty;

}



function UpdateRFOpenTask(RcId, PickQty, containerInternalId, getEnteredContainerNo, itemCube,
		itemWeight, EndLocation,vReason,ActPickQty,IsItLastPick,remainqty,vNewLocId,vNewRecId,vNewLP)
{
	var vRemQty=0;
	nlapiLogExecution('ERROR', 'into UpdateRFOpenTask containerlp: ', getEnteredContainerNo);
	var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RcId);
	var vinvrefno=transaction.getFieldValue('custrecord_invref_no');
	var vebizordno = transaction.getFieldValue('custrecord_ebiz_order_no');
	transaction.setFieldValue('custrecord_wms_status_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')
	/*	if(PickQty!= null && PickQty !="")
		transaction.setFieldValue('custrecord_act_qty', parseFloat(PickQty).toFixed(4));
	transaction.setFieldValue('custrecord_expe_qty', parseFloat(ActPickQty).toFixed(4));*/
	if(containerInternalId!= null && containerInternalId !="")
		transaction.setFieldValue('custrecord_container', containerInternalId);

	if(vNewLocId!=null && vNewLocId!="")
	{
		if(EndLocation!=vNewLocId)
			transaction.setFieldValue('custrecord_actendloc', vNewLocId);	
		else
			transaction.setFieldValue('custrecord_actendloc', EndLocation);	

	}

	if(PickQty!= null && PickQty !="")
		transaction.setFieldValue('custrecord_act_qty', parseFloat(PickQty).toFixed(4));
	if(itemWeight!=null && itemWeight!="")
		transaction.setFieldValue('custrecord_total_weight', parseFloat(itemWeight).toFixed(4));
	if(itemCube!=null && itemCube!="")
		transaction.setFieldValue('custrecord_totalcube', parseFloat(itemCube).toFixed(4));	
	if(getEnteredContainerNo!=null && getEnteredContainerNo!="")
		transaction.setFieldValue('custrecord_container_lp_no', getEnteredContainerNo);
	transaction.setFieldValue('custrecord_act_end_date', DateStamp());
	transaction.setFieldValue('custrecord_actualendtime',TimeStamp());
	if(vReason!=null && vReason!="")
		transaction.setFieldValue('custrecord_notes', vReason);	
	var currentContext = nlapiGetContext();  
	var currentUserID = currentContext.getUser();
	transaction.setFieldValue('custrecord_upd_ebiz_user_no',currentUserID);
	nlapiLogExecution('ERROR', 'IsItLastPick: ', IsItLastPick);
	transaction.setFieldValue('custrecord_device_upload_flag',IsItLastPick);
	if(vNewRecId !=null && vNewRecId !='')
	transaction.setFieldValue('custrecord_invref_no',vNewRecId);
	if(vNewLP !=null && vNewLP !='')
		transaction.setFieldValue('custrecord_lpno',vNewLP);
	var vemployee = request.getParameter('custpage_employee');
	nlapiLogExecution('ERROR', 'vemployee :', vemployee);
	nlapiLogExecution('ERROR', 'currentUserID :', currentUserID);
//	if (vemployee != null && vemployee != "") 
//	{
//	transaction.setFieldValue('custrecord_taskassignedto',vemployee);
//	} 
//	else if (currentUserID != null && currentUserID != "") 
//	{
//	transaction.setFieldValue('custrecord_taskassignedto',currentUserID);
//	}

	nlapiLogExecution('ERROR', 'Updating RF Open Task Record Id: ', RcId);
	nlapiSubmitRecord(transaction, false, true);
	nlapiLogExecution('ERROR', 'Updated RF Open Task successfully');

	//deleteAllocations(vinvrefno,PickQty,getEnteredContainerNo,ActPickQty,remainqty);

	try
	{
		if(vinvrefno!=null && vinvrefno!='')
			deleteOldAllocations(vinvrefno,ActPickQty,getEnteredContainerNo,PickQty,vRemQty);
		deleteNewAllocations(vNewRecId,ActPickQty,getEnteredContainerNo,PickQty,vRemQty,vebizordno);
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR', 'Exception in deleteOldNewAllocations',exp);
	}

}




function deleteOldAllocations(invrefno,actPickQty,contlpno,expPickQty,vRemQty)
{
	nlapiLogExecution('ERROR', 'Into deleteAllocations (Inv Ref NO)',invrefno);
	var scount=1;
	LABL1: for(var i=0;i<scount;i++)
	{
		nlapiLogExecution('ERROR', 'CUSTOM_RECORD_COLLISION', i);
		try
		{
			var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
			var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
			var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

			if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {
				Invttran.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(Invallocqty)- parseFloat(expPickQty)).toFixed(5));
			}

			//Invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(InvQOH) - parseFloat(ActPickQty)); 
			Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
			Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

			var invtrecid = nlapiSubmitRecord(Invttran, false, true);
			nlapiLogExecution('ERROR', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);


			if((parseFloat(InvQOH) - parseFloat(actPickQty)) == 0)
			{		
				nlapiLogExecution('Error', 'Deleting record from inventory if QOH becomes zero', invrefno);
				var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
			}
		}
		catch(ex)
		{
			nlapiLogExecution('ERROR', 'Exception in deleteAllocations',ex);

			var exCode='CUSTOM_RECORD_COLLISION'; 
			var wmsE='Inventory record being updated by another user. Please try again...';
			if (ex instanceof nlobjError) 
			{	
				wmsE=ex.getCode() + '\n' + ex.getDetails();
				exCode=ex.getCode();
			}
			else
			{
				wmsE=ex.toString();
				exCode=ex.toString();
			}  

			nlapiLogExecution('ERROR', 'Exception in RF Inv move : ', ex);

			if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
			{ 
				scount=scount+1;
				continue LABL1;
			}
			else break LABL1;

		}
	}
}

/**
 * This function clear the allocation qty from inventory.
 * @param invrefno
 * @param PickQty
 */
function deleteNewAllocations(invrefno,actPickQty,contlpno,expPickQty,vRemQty,vebizordno)
{
	nlapiLogExecution('ERROR', 'Into deleteNewAllocations (Inv Ref NO)',invrefno);
	nlapiLogExecution('ERROR', 'actPickQty',actPickQty);
	nlapiLogExecution('ERROR', 'expPickQty',expPickQty);

	var scount=1;
	LABL1: for(var i=0;i<scount;i++)
	{
		try
		{
			var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
			var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
			var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');
			var vLP=Invttran.getFieldValue('custrecord_ebiz_inv_lp');

			//Invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(InvQOH) - (parseFloat(expPickQty)-parseFloat(vRemQty))); 
			//Invttran.setFieldValue('custrecord_ebiz_qoh',(parseFloat(Invallocqty)- parseFloat(expPickQty)).toFixed(5)); 
			Invttran.setFieldValue('custrecord_ebiz_qoh',(parseFloat(InvQOH)- parseFloat(expPickQty)).toFixed(5));
			Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
			Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

		var invtrecid = nlapiSubmitRecord(Invttran, false, true);
		nlapiLogExecution('ERROR', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);

		if(invtrecid!=null && invtrecid!='')
			CreateSTGInvtRecord(invtrecid, vLP,expPickQty,vebizordno);


			if((parseFloat(InvQOH) - parseFloat(expPickQty)) == 0)
			{		
				nlapiLogExecution('Error', 'Deleting record from inventory if QOH becomes zero', invrefno);
				var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
			}
		}
		catch(ex)
		{
			nlapiLogExecution('ERROR', 'Exception in deleteAllocations',ex);

			var exCode='CUSTOM_RECORD_COLLISION'; 
			var wmsE='Inventory record being updated by another user. Please try again...';
			if (ex instanceof nlobjError) 
			{	
				wmsE=ex.getCode() + '\n' + ex.getDetails();
				exCode=ex.getCode();
			}
			else
			{
				wmsE=ex.toString();
				exCode=ex.toString();
			}  

			nlapiLogExecution('ERROR', 'Exception in RF Inv move : ', ex);

			if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
			{ 
				scount=scount+1;
				continue LABL1;
			}
			else break LABL1;
		}
	}

	nlapiLogExecution('ERROR', 'Out of deleteNewAllocations (Inv Ref NO)',invrefno);
}

function CreateSTGInvtRecord(invtrecid, vContLp,vqty,vebizordno) 
{
	nlapiLogExecution('ERROR', 'Into CreateSTGInvtRecord (Container LP)',vContLp);
	nlapiLogExecution('ERROR', 'vebizordno',vebizordno);

	try
	{
		var stgmInvtRec = nlapiCopyRecord('customrecord_ebiznet_createinv',invtrecid);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_lp', vContLp);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(vqty).toFixed(4));
		stgmInvtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(vqty).toFixed(4));
		stgmInvtRec.setFieldValue('custrecord_ebiz_alloc_qty', '0');
		stgmInvtRec.setFieldValue('custrecord_wms_inv_status_flag', '36');	//FLAG.INVENTORY.WIP	
		stgmInvtRec.setFieldValue('custrecord_invttasktype', '3'); //Task Type - PICK
		stgmInvtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
		stgmInvtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');		
		if(vebizordno!=null && vebizordno!='')
			stgmInvtRec.setFieldValue('custrecord_ebiz_transaction_no', vebizordno);	
		nlapiSubmitRecord(stgmInvtRec, false, true);
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR', 'Exception in CreateSTGInvtRecord',exp);
	}

	nlapiLogExecution('ERROR', 'Out of CreateSTGInvtRecord');
}


function getOpenTasksCount(vWOId,vZoneId)
{
	nlapiLogExecution('ERROR', 'Into getOpenTasksCount');


	var openreccount=0;
	var filterOpentask = new Array();
	filterOpentask.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
	filterOpentask.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));

	if(vWOId != null && vWOId != "")
	{
		filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vWOId));
	}
	if(vZoneId!=null && vZoneId!="" && vZoneId!="null")
	{
		filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', vZoneId));
	}

	var WOcolumns = new Array();
	WOcolumns[0] = new nlobjSearchColumn('custrecord_line_no');
	WOcolumns[1] = new nlobjSearchColumn('custrecord_sku');
	WOcolumns[2] = new nlobjSearchColumn('custrecord_expe_qty');
	WOcolumns[3] = new nlobjSearchColumn('custrecord_lpno');
	WOcolumns[4] = new nlobjSearchColumn('custrecord_batch_no');
	WOcolumns[5] = new nlobjSearchColumn('custrecord_actbeginloc');
	WOcolumns[6] = new nlobjSearchColumn('custrecord_act_qty');   
	WOcolumns[7] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	WOcolumns[8] = new nlobjSearchColumn('custrecord_invref_no');
	WOcolumns[9] = new nlobjSearchColumn('custrecord_wms_location');
	WOcolumns[10] = new nlobjSearchColumn('custrecord_comp_id');
	WOcolumns[11] = new nlobjSearchColumn('custrecord_ebiz_order_no');				

	WOcolumns[0].setSort();
	var WOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterOpentask,WOcolumns);	

	if(WOSearchResults!=null && WOSearchResults!='' && WOSearchResults.length>0)
	{
		openreccount=WOSearchResults.length;
	}

	return openreccount;

}

function removeDuplicateLocations(arrayName,flag){
	var newArray = new Array();
	label:for (var i = 0; i < arrayName.length; i++) {
		for (var j = 0; j < newArray.length; j++) {
			if(flag=='batchitem'){
				if (newArray[j][4] == arrayName[i][4]) 
					continue label;
			}
			else
				if (newArray[j][3] == arrayName[i][3]) 
					continue label;
		}

		var invtarray = new Array();
		invtarray[0] = arrayName[i][0]; 
		invtarray[1] = arrayName[i][1]; 
		invtarray[2] = arrayName[i][2]; 
		invtarray[3] = arrayName[i][3]; 
		invtarray[4] = arrayName[i][4]; 
		invtarray[5] = arrayName[i][5]; 
		newArray.push(invtarray);
	}
	return newArray;
}

