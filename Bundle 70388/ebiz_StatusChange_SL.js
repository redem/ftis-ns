/***************************************************************************
   eBizNET Solutions Inc
 ****************************************************************************
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/ebiz_StatusChange_SL.js,v $
 *     	   $Revision: 1.1.2.11.4.4.2.23.2.3 $
 *     	   $Date: 2015/11/24 11:13:05 $
 *     	   $Author: deepshikha $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_180 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *76
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_StatusChange_SL.js,v $
 * Revision 1.1.2.11.4.4.2.23.2.3  2015/11/24 11:13:05  deepshikha
 * 2015.2 Issue Fix
 * 201414429
 *
 * Revision 1.1.2.11.4.4.2.23.2.2  2015/11/05 15:33:12  aanchal
 * 2015.2 Issue Fix
 * 201415271
 *
 * Revision 1.1.2.11.4.4.2.23.2.1  2015/09/21 14:02:23  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.1.2.11.4.4.2.23  2015/07/15 15:14:53  grao
 * 2015.2 ssue fixes  201413055
 *
 * Revision 1.1.2.11.4.4.2.22  2015/04/10 21:28:10  skreddy
 * Case# 201412323ï¿½
 * changed the url path which was hard coded
 *
 * Revision 1.1.2.11.4.4.2.21  2014/07/04 15:32:22  sponnaganti
 * case# 20149245
 * Compatibility Issue Fix
 *
 * Revision 1.1.2.11.4.4.2.20  2014/06/02 15:45:20  sponnaganti
 * case# 20148659 20148655
 * Stand Bundle Issue Fix
 *
 * Revision 1.1.2.11.4.4.2.19  2014/05/28 15:13:15  sponnaganti
 * case# 20148561
 * Stnd Bundle Issue fix
 *
 * Revision 1.1.2.11.4.4.2.18  2014/05/12 14:49:05  sponnaganti
 * case# 20148143
 * Standerd Bundle Issue fix
 *
 * Revision 1.1.2.11.4.4.2.17  2014/04/28 15:53:06  skavuri
 * Case # 20148182 SB Issue Fixed
 *
 * Revision 1.1.2.11.4.4.2.16  2014/04/18 15:42:14  skavuri
 * Case# 20148048 SB Issue fixed
 *
 * Revision 1.1.2.11.4.4.2.15  2014/04/16 15:50:59  gkalla
 * case#201218858
 * Ryonet Inventory disappears in status change screen
 *
 * Revision 1.1.2.11.4.4.2.14  2014/03/05 12:05:44  snimmakayala
 * Case #: 20127451
 *
 * Revision 1.1.2.11.4.4.2.13  2014/01/31 15:33:44  gkalla
 * case#20127033   ï¿½
 * MHP issue
 *
 * Revision 1.1.2.11.4.4.2.12  2013/12/04 16:19:24  skreddy
 * Case# 20126153
 * 2014.1 stnd bundle issue fix
 *
 * Revision 1.1.2.11.4.4.2.11  2013/11/29 10:16:55  snimmakayala
 * Case# : 20125973
 * MHP UAT Fixes.
 *
 * Revision 1.1.2.11.4.4.2.10  2013/11/18 16:57:16  gkalla
 * case#20125760
 * Deleting zero qty inv record
 *
 * Revision 1.1.2.11.4.4.2.9  2013/10/25 16:11:59  skreddy
 * Case# 20124017  & 20124016
 * standard bundle  issue fix
 *
 * Revision 1.1.2.11.4.4.2.8  2013/10/24 06:46:51  skreddy
 * Case# 20124016
 * standard bundle issue fix
 *
 * Revision 1.1.2.11.4.4.2.7  2013/09/11 15:23:51  rmukkera
 * Case# 20124376
 *
 * Revision 1.1.2.11.4.4.2.6  2013/08/23 15:30:05  grao
 * SB Issue Fixes  20124016, 20124017
 *
 * Revision 1.1.2.11.4.4.2.5  2013/05/07 15:12:19  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.1.2.11.4.4.2.4  2013/04/18 08:32:44  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.1.2.11.4.4.2.3  2013/04/04 16:02:57  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related to update invt to eBn and NS
 *
 * Revision 1.1.2.11.4.4.2.2  2013/04/01 20:59:31  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.1.2.11.4.4.2.1  2013/03/01 14:34:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.1.2.11.4.4  2013/02/16 12:11:10  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.1.2.11.4.3  2013/02/05 12:46:38  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.1.2.11.4.2  2012/12/20 07:48:46  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form 2012.2 branch.
 *
 * Revision 1.1.2.11.4.1  2012/11/01 14:54:57  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.1.2.11  2012/09/03 13:56:34  schepuri
 * CASE201112/CR201113/LOG201121
 * added date stamp
 *
 * Revision 1.1.2.10  2012/08/06 07:11:26  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to Status Change was resolved.
 *
 * Revision 1.1.2.9  2012/07/04 14:03:53  schepuri
 * CASE201112/CR201113/LOG201121
 * packing issues
 *
 * Revision 1.1.2.8  2012/05/15 22:54:48  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Status change Qty updations
 *
 * Revision 1.1.2.7  2012/05/14 15:16:34  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Status Change
 *
 * Revision 1.1.2.6  2012/04/30 11:38:54  spendyala
 * CASE201112/CR201113/LOG201121
 * While Searching of Item in ItemMaster,
 * 'name' filter is changed to 'nameinternal' and also checking weather the item is inactive state or not.
 *
 * Revision 1.1.2.5  2012/04/27 13:17:34  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Status Change
 *
 * Revision 1.1.2.4  2012/04/20 13:33:19  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.1.2.3  2012/04/18 13:43:40  schepuri
 * CASE201112/CR201113/LOG201121
 * added tasktype dropdown in qb
 *
 * Revision 1.1.2.2  2012/04/18 06:38:52  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 *****************************************************************************/
var tempInventoryResultsArray=new Array();
var result=new Array();
function getInventorySearchResults(maxno,request)
{
	var binlocid = request.getParameter('custpage_binlocation');
	nlapiLogExecution('ERROR', 'binLoc Internal ID', binlocid);

	var itemId = request.getParameter('custpage_item');
	var vItemArr=new Array();
	if(itemId != null && itemId != "")
	{
		vItemArr = itemId.split('');
	}
	nlapiLogExecution('ERROR', 'item Internal ID', vItemArr);

	var LPvalue = request.getParameter('custpage_invtlp');
	nlapiLogExecution('ERROR', 'LP value', LPvalue);

	var pono=request.getParameter('custpage_pono');

	var vtasktype = request.getParameter('custpage_tasktype');
	//var vtasktype=2;
	nlapiLogExecution('ERROR', 'vtasktype', vtasktype);

	var vfromdate=request.getParameter('custpage_fromdate');
	var vtodate=request.getParameter('custpage_todate');

	var vStatus = request.getParameter('custpage_itemstatus');
	nlapiLogExecution('ERROR', 'Item Status ', vStatus);

	var i = 0;
	var filtersinv = new Array();

	nlapiLogExecution('ERROR', 'tst pono ', pono);
	if (pono!=null && pono != "") {
		filtersinv[i] = new nlobjSearchFilter('custrecord_ebiz_transaction_no', null, 'anyof', pono);
		i++;
	}
	nlapiLogExecution('ERROR', 'tst vfromdate ', vfromdate);
	nlapiLogExecution('ERROR', 'tst vtodate ', vtodate);
	if ((vfromdate != null && vfromdate != "") && (vtodate != null && vtodate != "") ){


		var polist=getpono(vfromdate,vtodate);

		nlapiLogExecution('ERROR', 'polist ', polist);
		if (polist!=null && polist != "") {
			nlapiLogExecution('ERROR', 'polist_if ', 'done');
			filtersinv[i] = new nlobjSearchFilter('custrecord_ebiz_transaction_no',null, 'anyof', polist);
			i++;
		}

	}

	nlapiLogExecution('ERROR', 'tst binlocid ', binlocid);
	if (binlocid!=null && binlocid != "") {
		filtersinv[i] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', binlocid);
		i++;
	}

	nlapiLogExecution('ERROR', 'tst itemId ', itemId);
	nlapiLogExecution('ERROR', 'tst vItemArr ', vItemArr);
	if ( itemId!=null && itemId != "" && vItemArr!=null &&  vItemArr !="") {
		filtersinv[i] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', vItemArr);
		i++;
	}

	nlapiLogExecution('ERROR', 'tst LPvalue ', LPvalue);
	if (LPvalue!=null && LPvalue != "") {
		filtersinv[i] = new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', LPvalue);
		i++;
	}

	nlapiLogExecution('ERROR', 'tst vtasktype ', vtasktype);
	if (vtasktype!=null && vtasktype != "") {
		filtersinv[i] = new nlobjSearchFilter('custrecord_invttasktype', null, 'is', vtasktype);
		i++;
	}

	nlapiLogExecution('ERROR', 'tst request.getParameter(custpage_location) ', request.getParameter('custpage_location'));
	if (request.getParameter('custpage_location') !=null && request.getParameter('custpage_location') != "") {
		filtersinv[i] = new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'is', request.getParameter('custpage_location'));
		i++;
	}

	nlapiLogExecution('ERROR', 'tst request.getParameter(custpage_company) ', request.getParameter('custpage_company'));
	if (request.getParameter('custpage_company') !=null && request.getParameter('custpage_company') != "") {
		filtersinv[i] = new nlobjSearchFilter('custrecord_ebiz_inv_company', null, 'is', request.getParameter('custpage_company'));
		i++;
	}

	nlapiLogExecution('ERROR', 'tst vStatus ', vStatus);
	if (vStatus != null && vStatus != "") {
		filtersinv[i] = new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', vStatus);
		i++;
	}
	/*else
	{
		filtersinv[i] = new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'anyof', [6]);//HOLD
		i++;
	}*/

	filtersinv[i] = new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0);
	i++;
	filtersinv[i] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['3','19','17']);
	if(maxno!=-1)
	{
		i++;
		filtersinv[i] = new nlobjSearchFilter('id', null, 'greaterthan', maxno);
	}


	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_qoh');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');
	columns[5] = new nlobjSearchColumn('custrecord_ebiz_inv_sku_status');
	columns[6] = new nlobjSearchColumn('custrecord_ebiz_inv_packcode');
	columns[7] = new nlobjSearchColumn('custrecord_ebiz_inv_account_no');
	columns[8] = new nlobjSearchColumn('custrecord_ebiz_inv_loc');
	columns[9] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columns[10] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
	columns[11] = new nlobjSearchColumn('internalid');
	columns[12] = new nlobjSearchColumn('custrecord_ebiz_inv_company');
	columns[13] = new nlobjSearchColumn('custrecord_ebiz_inv_fifo');
	columns[14] = new nlobjSearchColumn('custrecord_ebiz_itemdesc');
	columns[15] = new nlobjSearchColumn('custrecord_ebiz_transaction_no');
	columns[16] = new nlobjSearchColumn('custrecord_invttasktype');
	columns[17] = new nlobjSearchColumn('custrecord_wms_inv_status_flag');
	columns[18] = new nlobjSearchColumn('custrecord_inboundinvlocgroupid');
	columns[19] = new nlobjSearchColumn('custrecord_outboundinvlocgroupid');
	

	columns[11].setSort();

	var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinv, columns);
	if(invtsearchresults!=null)
	{
		//result=removeDuplicateElements(invtsearchresults);
		if(invtsearchresults.length>=1000)
		{
			var maxno1=invtsearchresults[invtsearchresults.length-1].getValue(columns[11]);
			tempInventoryResultsArray.push(invtsearchresults);
			getInventorySearchResults(maxno1,request);
		}
		else
		{
			tempInventoryResultsArray.push(invtsearchresults);
		}
	}

	return tempInventoryResultsArray;
}


function getpono(vfromdate,vtodate)
{
	var polist=new Array();

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('trandate',null, 'within', vfromdate,vtodate);
	filters[1] = new nlobjSearchFilter('mainline', null, 'is', 'T');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	var searchresults = nlapiSearchRecord('purchaseorder', null, filters, columns);

	if(searchresults!=null && searchresults!='')
	{
		for(var k=0;k<searchresults.length;k++)
		{

			polist.push(searchresults[k].getValue('internalid')); 

		}
	}
	return polist;

}
//function removeDuplicateElements(arrayName){
//nlapiLogExecution('ERROR', 'test1','test1');
//nlapiLogExecution('ERROR', 'arrayName.length',arrayName.length);
//var newArray = new Array();
//var newArraycount = 0;
//label:for (var i = 0; i < arrayName.length; i++) {
//nlapiLogExecution('ERROR', 'inside i',i);
//var loc = arrayName[i].getValue('custrecord_ebiz_inv_binloc');
//var sku = arrayName[i].getValue('custrecord_ebiz_inv_sku');
//var lp = arrayName[i].getValue('custrecord_ebiz_inv_lp');
//var qoh = arrayName[i].getValue('custrecord_ebiz_qoh');
//var lot = arrayName[i].getValue('custrecord_ebiz_inv_lot');
//var skustatus = arrayName[i].getValue('custrecord_ebiz_inv_sku_status');
//var packcode = arrayName[i].getValue('custrecord_ebiz_inv_packcode');
//var accountno = arrayName[i].getValue('custrecord_ebiz_inv_account_no');
//var invloc = arrayName[i].getValue('custrecord_ebiz_inv_loc');
//var invqty = arrayName[i].getValue('custrecord_ebiz_inv_qty');
//var allocqty = arrayName[i].getValue('custrecord_ebiz_alloc_qty');
//var internalid = arrayName[i].getValue('internalid');
//var invcompany = arrayName[i].getValue('custrecord_ebiz_inv_company');
//var fifo = arrayName[i].getValue('custrecord_ebiz_inv_fifo');
//var itemdesc = arrayName[i].getValue('custrecord_ebiz_itemdesc');

//for (var j = 0; j < newArray.length; j++) {
//nlapiLogExecution('ERROR', 'inside j',j);
//nlapiLogExecution('ERROR', 'newArray[j][0]',newArray[j][0]);
//nlapiLogExecution('ERROR', 'arrayName[i][0]',loc);
//nlapiLogExecution('ERROR', 'newArray[j][1]',newArray[j][1]);
//nlapiLogExecution('ERROR', 'arrayName[i][1]',sku);
//nlapiLogExecution('ERROR', 'newArray[j][2]',newArray[j][2]);
//nlapiLogExecution('ERROR', 'arrayName[i][2]',lp);
//if ((newArray[j][0] == loc) && (newArray[j][1] == sku) && (newArray[j][2] == lp)) 
//continue label;
//}
//nlapiLogExecution('ERROR', 'outside j');
//var currentrow=[loc,sku,lp,qoh,lot,skustatus,packcode,accountno,invloc,invqty,allocqty,internalid,invcompany,fifo,itemdesc];
//newArray[newArraycount++] = currentrow;
//}
//return newArray;
//}
function FillForm(request,response)
{
	var pagesizevalue;
	var whloc='';
	var form = nlapiCreateForm('Status Change');

	var selectPO = form.addField('custpage_pono', 'select', 'PO#/RMA#/TO#');
	selectPO.addSelectOption('','');

	if(request.getParameter('custpage_pono')!='' && request.getParameter('custpage_pono')!=null)
	{
		selectPO.setDefaultValue(request.getParameter('custpage_pono'));	
	}

	var binLocationField = form.addField('custpage_binlocation', 'select', 'Bin Location');
	binLocationField.addSelectOption('', '');
	if(request.getParameter('custpage_binlocation')!='' && request.getParameter('custpage_binlocation')!=null)
	{
		binLocationField.setDefaultValue(request.getParameter('custpage_binlocation'));	
	}
	nlapiLogExecution('ERROR', 'multiselect',request.getParameter('custpage_item'));

	var itemField = form.addField('custpage_item', 'multiselect', 'Item','item');

	if(request.getParameter('custpage_item')!='' && request.getParameter('custpage_item')!=null)
	{
		var itemValue=request.getParameter('custpage_item');
		var itemArray = new Array();
		itemArray = itemValue.split('');
		nlapiLogExecution('ERROR', 'multiselect',itemArray.length);	
	}
	itemField.setDefaultValue(request.getParameter('custpage_item'));

	var invtlp = form.addField('custpage_invtlp', 'select', 'LP');
	invtlp.addSelectOption('', '');
	if(request.getParameter('custpage_invtlp')!='' && request.getParameter('custpage_invtlp')!=null)
	{
		invtlp.setDefaultValue(request.getParameter('custpage_invtlp'));	
	}



	nlapiLogExecution('ERROR', 'Location',request.getParameter('custpage_location'));
	var varLoc = form.addField('custpage_location', 'select', 'Location');
	varLoc.addSelectOption('', '');
	//varLoc.setMandatory(true);
	//case# 20148143 starts 
	var vRoleLocation=getRoledBasedLocation();
	FillLocation(form,varLoc,invtlp,-1,vRoleLocation);
	//case# 20148143 end
	if(request.getParameter('custpage_location')!='' && request.getParameter('custpage_location')!=null)
	{
		varLoc.setDefaultValue(request.getParameter('custpage_location'));	
		whloc = request.getParameter('custpage_location');
	}

	var varComp = form.addField('custpage_company', 'select', 'Company');
	varComp.addSelectOption('', '');

	if(request.getParameter('custpage_company')!='' && request.getParameter('custpage_company')!=null)
	{
		varComp.setDefaultValue(request.getParameter('custpage_company'));	
	}

	var varTaskType = form.addField('custpage_tasktype', 'select', 'Task Type');
	varTaskType.addSelectOption('', '');

	var filterstasktype = new Array();
	filterstasktype[0] = new nlobjSearchFilter('internalid', null, 'anyof', [1,2]);		

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');

	var searchTaskTyperesults = nlapiSearchRecord('customrecord_ebiznet_tasktype', null, filterstasktype, columns);


	if (searchTaskTyperesults != null) 
	{
		for (var i = 0; i < searchTaskTyperesults.length; i++) 
		{
			var res = form.getField('custpage_tasktype').getSelectOptions(searchTaskTyperesults[i].getValue('name'), 'is');
			if (res != null) 
			{
				if (res.length > 0) 
					continue;                    
			}
			varTaskType.addSelectOption(searchTaskTyperesults[i].getId(), searchTaskTyperesults[i].getValue('name'));
			//varTaskType.setDefaultValue(searchTaskTyperesults[i].getId());
		}
	}
	/*if(request.getParameter('custpage_tasktype')!='' && request.getParameter('custpage_tasktype')!=null)
	{
		varTaskType.setDefaultValue(request.getParameter('custpage_tasktype'));	
	}*/

	var vItemStatus = form.addField('custpage_itemstatus', 'select', 'Item Status');
	vItemStatus.addSelectOption('', '');

	var fromdate = form.addField('custpage_fromdate', 'date', 'From Date');
	if(request.getParameter('custpage_fromdate')!='' && request.getParameter('custpage_fromdate')!=null)
	{
		fromdate.setDefaultValue(request.getParameter('custpage_fromdate'));
	}


	var todate = form.addField('custpage_todate', 'date', 'To Date');	
	if(request.getParameter('custpage_todate')!='' && request.getParameter('custpage_todate')!=null)
	{
		todate.setDefaultValue(request.getParameter('custpage_todate'));
	}




	var filtersStatus = new Array();
	filtersStatus.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filtersStatus.push(new nlobjSearchFilter('custrecord_allowrcvskustatus', null, 'is', 'T'));

	var columnsstatus = new Array();
	columnsstatus.push(new nlobjSearchColumn('name'));

	var searchStatus = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, filtersStatus, columnsstatus);

	if (searchStatus != null) 
	{
		for (var i = 0; i < searchStatus.length; i++) 
		{
			var res = form.getField('custpage_itemstatus').getSelectOptions(searchStatus[i].getValue('name'), 'is');
			if (res != null) 
			{
				if (res.length > 0) 
					continue;                    
			}
			vItemStatus.addSelectOption(searchStatus[i].getId(), searchStatus[i].getValue('name'));
		}
	}

	nlapiLogExecution('ERROR', 'Item Status', request.getParameter('custpage_itemstatus'));

	if(request.getParameter('custpage_itemstatus')!='' && request.getParameter('custpage_itemstatus')!=null)
	{
		vItemStatus.setDefaultValue(request.getParameter('custpage_itemstatus'));	
	}
	else
	{
		// Case# 20148182 starts
		//vItemStatus.setDefaultValue('6');
		vItemStatus.setDefaultValue(' ');
		// Case# 20148182 ends
	}

	//case# 20148143 starts
	/*var searchlocresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, null, new nlobjSearchColumn('custrecord_ebiz_inv_loc').setSort());

	if (searchlocresults != null)
	{
		nlapiLogExecution('ERROR', 'Inventory count', searchlocresults.length);

		for (var i = 0; i < searchlocresults.length; i++) 
		{
			var res = form.getField('custpage_location').getSelectOptions(searchlocresults[i].getText('custrecord_ebiz_inv_loc'), 'is');
			if (res != null) 
			{
				if (res.length > 0)	                
					continue;	                
			}
			varLoc.addSelectOption(searchlocresults[i].getValue('custrecord_ebiz_inv_loc'), searchlocresults[i].getText('custrecord_ebiz_inv_loc'));
		}
	}      */
	//case# 20148143 end
	var searchcompresults = nlapiSearchRecord('customrecord_ebiznet_company', null, new nlobjSearchFilter('isinactive',null,'is','F'), new nlobjSearchColumn('Name').setSort());

	if (searchcompresults != null)
	{
		for (var i = 0; i < searchcompresults.length; i++) 
		{
			var res = form.getField('custpage_company').getSelectOptions(searchcompresults[i].getValue('Name'), 'is');
			if (res != null) 
			{
				if (res.length > 0) 
					continue;                
			}
			if(searchcompresults[i].getValue('Name') != "")
				varComp.addSelectOption(searchcompresults[i].getValue('Name'), searchcompresults[i].getValue('Name'));
		}
	}

	var filtersinv = new Array();
	filtersinv[0] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3,17,19]);
	filtersinv[1] = new nlobjSearchFilter('custrecord_invttasktype', null, 'anyof', [1,2]);
	//filtersinv[2] = new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'anyof', [1]);


	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_transaction_no');
	columns[0].setSort(true);

	var searchporesults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinv, columns);

	if (searchporesults != null) 
	{
		for (var i = 0; i < searchporesults.length; i++) 
		{
			var res = form.getField('custpage_pono').getSelectOptions(searchporesults[i].getText('custrecord_ebiz_transaction_no'), 'is');
			if (res != null) 
			{
				if (res.length > 0) 
					continue;                    
			}
			selectPO.addSelectOption(searchporesults[i].getValue('custrecord_ebiz_transaction_no'), searchporesults[i].getText('custrecord_ebiz_transaction_no'));
		}
	}



	//3 - In bound Storage
	//19 - Inventory Storage
	nlapiLogExecution('ERROR', 'Test1');
	var filtersinv = new Array();
	filtersinv[0] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3,17, 19]);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');

	var searchInventory = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinv, columns);

	if (searchInventory != null) 
	{
		for (var i = 0; i < searchInventory.length; i++) 
		{
			var res = form.getField('custpage_binlocation').getSelectOptions(searchInventory[i].getText('custrecord_ebiz_inv_binloc'), 'is');
			if (res != null) 
			{
				if (res.length > 0) 
					continue;                    
			}
			binLocationField.addSelectOption(searchInventory[i].getValue('custrecord_ebiz_inv_binloc'), searchInventory[i].getText('custrecord_ebiz_inv_binloc'));
		}
		//case# 20148561 starts
		/*for (var i = 0; i < searchInventory.length; i++) 
		{
			invtlp.addSelectOption(searchInventory[i].getValue('custrecord_ebiz_inv_lp'), searchInventory[i].getValue('custrecord_ebiz_inv_lp'));
		}*/
		//case# 20148561 ends
		nlapiLogExecution('ERROR', 'Test2',request.getParameter('custpage_checkfield'));



		if (request.getParameter('custpage_checkfield') == null || request.getParameter('custpage_checkfield') == '') //&& request.getParameter('custpage_checkfield') == "") {
		{  
			form.setScript('customscript_ebiz_statuschange_cl');
			form.addField('custpage_checkfield', 'select', 'check').setDisplayType('hidden');

			var sublist = form.addSubList("custpage_invtmovelist", "list", "Inventory Move List");
			sublist.addField("custpage_sno", "text", "SL NO").setDisplayType('disabled');
			sublist.addMarkAllButtons();
			sublist.addField("custpage_invlocmove", "checkbox", "Move");
			sublist.addField("custpage_pono", "text", "PO#").setDisplayType('inline');
			sublist.addField("custpage_invlocation", "text", "Bin Location").setDisplayType('inline');
			sublist.addField("custpage_itemname", "text", "Item", "items").setDisplayType('inline');
			sublist.addField("custpage_itemstatus", "select", "Item Status", "customrecord_ebiznet_sku_status").setDisplayType('inline');
			sublist.addField("custpage_pc", "select", "Pack Code", "customlist_ebiznet_packcode").setDisplayType('inline');
			sublist.addField("custpage_lot", "select", "LOT#", "customrecord_ebiznet_batch_entry").setDisplayType('inline');
			sublist.addField("custpage_lp", "text", "LP#").setDisplayType('inline');
			sublist.addField("custpage_totqty", "text", "Total Quantity").setDisplayType('hidden');
			sublist.addField("custpage_itemqty", "text", "Quantity on Hand").setDisplayType('inline');
			sublist.addField("custpage_allocqty", "text", "Allocated Quantity").setDisplayType('inline');
			sublist.addField("custpage_availqty", "text", "Available Quantity").setDisplayType('inline');
			//sublist.addField("custpage_itemnewstatus", "select", "New Item Status", "customrecord_ebiznet_sku_status").setDisplayType('entry');

			nlapiLogExecution('ERROR', 'warehouse location',whloc);

			var InvItemStatus = sublist.addField("custpage_itemnewstatus", "select", "New Item Status");
			InvItemStatus.addSelectOption('', '');
			var InvItemStatusFilters = new Array();	
			if(whloc!=null && whloc!='')
				InvItemStatusFilters.push(new nlobjSearchFilter( 'custrecord_ebizsiteskus', null, 'anyOf', [whloc]));

			InvItemStatusFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

			var columnsitemstatus = new Array();
			columnsitemstatus[0] = new nlobjSearchColumn('name');
			columnsitemstatus[1] = new nlobjSearchColumn('internalid');
			columnsitemstatus[0].setSort();

			var itemstatusresults = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, InvItemStatusFilters,columnsitemstatus);
			for (var k = 0; itemstatusresults != null && k < itemstatusresults.length; k++) {
				//Searching for Duplicates		
				var res =  InvItemStatus.getSelectOptions(itemstatusresults[k].getValue('name'), 'is');
				if (res != null) {

					if (res.length > 0) {
						continue;
					}
				}		
				InvItemStatus.addSelectOption(itemstatusresults[k].getValue('internalid'), itemstatusresults[k].getValue('name'));
			}


			sublist.addField("custpage_itemnewloc", "select", "New Bin Location", "customrecord_ebiznet_location");
			sublist.addField("custpage_itemnewqty", "text", "New Quantity").setDisplayType('entry');
			sublist.addField("custpage_itemnewlp", "text", "New LP").setDisplayType('entry');
			sublist.addField("custpage_lottext", "text", "LOT").setDisplayType('hidden');

//			var InvAdjType= sublist.addField("custpage_invadjtype", "select", "Adjustment Type");
//			InvAdjType.addSelectOption('', '');
//			var AdjustmentTypeFilters = new Array();		
//			AdjustmentTypeFilters.push(new nlobjSearchFilter( 'custrecord_adjusttasktype', null, 'anyOf', '18'));
//			AdjustmentTypeFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

//			var columnsinvt = new Array();
//			columnsinvt[0] = new nlobjSearchColumn('name');
//			columnsinvt[1] = new nlobjSearchColumn('internalid');
//			columnsinvt[0].setSort();

//			var searchresults = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, AdjustmentTypeFilters,columnsinvt);

//			for (var i = 0; searchresults != null && i < searchresults.length; i++) {
//			//Searching for Duplicates		
//			var res=  InvAdjType.getSelectOptions(searchresults[i].getValue('name'), 'is');
//			if (res != null) {

//			if (res.length > 0) {
//			continue;
//			}
//			}		
//			InvAdjType.addSelectOption(searchresults[i].getValue('internalid'), searchresults[i].getValue('name'));
//			}


			sublist.addField("custpage_recid", "text", "RecId").setDisplayType('hidden');
			sublist.addField("custpage_rectype", "text", "RecType").setDisplayType('hidden');

			sublist.addField("custpage_itemid", "text", "itemid").setDisplayType('hidden');
			sublist.addField("custpage_locid", "text", "locid").setDisplayType('hidden');
			sublist.addField("custpage_oldqty", "text", "oldquantity").setDisplayType('hidden');
			sublist.addField("custpage_oldlp", "text", "LP#").setDisplayType('hidden');
			sublist.addField("custpage_olditemstatus", "text", "ItemStatus").setDisplayType('hidden');
			sublist.addField("custpage_accno", "text", "accountno").setDisplayType('hidden');
			sublist.addField("custpage_siteloc", "text", "siteloc").setDisplayType('hidden');
			sublist.addField("custpage_allocqtyhid", "text", "qtyalloc").setDisplayType('hidden');
			sublist.addField("custpage_company", "text", "Company").setDisplayType('hidden');
			sublist.addField("custpage_fifodate", "text", "fifodate").setDisplayType('hidden');
			sublist.addField("custpage_itemdesc", "text", "ItemDesc").setDisplayType('hidden');
			sublist.addField("custpage_povalue", "text", "povalue").setDisplayType('hidden');
			sublist.addField("custpage_tasktype", "text", "tasktype").setDisplayType('hidden');
			//case# 20148659
			sublist.addField("custpage_wmsstatusflag", "text", "wmsstatusflag").setDisplayType('hidden');
			sublist.addField("custpage_inboundlocgroup", "text", "inboundlocgroup").setDisplayType('hidden');
			sublist.addField("custpage_outboundlocgroup", "text", "outboundlocgroup").setDisplayType('hidden');
			//case# 20148659


			var invtsearchresults=getInventorySearchResults(-1,request);

			if (invtsearchresults != null) 
			{
				nlapiLogExecution('ERROR', 'invtsearchresults ', invtsearchresults.length);
				var temparraynew=new Array();var tempindex=0;
				for(k=0;k<invtsearchresults.length;k++)
				{
					var invtsearchresult = invtsearchresults[k];

					for(var j=0;j<invtsearchresult.length;j++)
					{
						temparraynew[tempindex]=invtsearchresult[j];
						tempindex=tempindex+1;
					}
				}
				nlapiLogExecution('ERROR', 'temparraynew.length ', temparraynew.length); 
				// paging dropdown  
				var test='';
				var vbulkItemStatus = form.addField('custpage_bulkitemstatus', 'select', 'New Item Status');
				vbulkItemStatus.addSelectOption('', '');
				vbulkItemStatus.setLayoutType('outsidebelow', 'startrow');

				var filtersStatus = new Array();
				filtersStatus.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

				var columnsstatus = new Array();
				columnsstatus.push(new nlobjSearchColumn('name'));

				var searchStatus = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, filtersStatus, columnsstatus);

				if (searchStatus != null) 
				{
					for (var i = 0; i < searchStatus.length; i++) 
					{
						var res = form.getField('custpage_bulkitemstatus').getSelectOptions(searchStatus[i].getValue('name'), 'is');
						if (res != null) 
						{
							if (res.length > 0) 
								continue;                    
						}
						vbulkItemStatus.addSelectOption(searchStatus[i].getId(), searchStatus[i].getValue('name'));
					}
				}
				if(temparraynew.length>0)
				{

					if(temparraynew.length>25)
					{

						var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('entry');
						pagesize.setDisplaySize(10,10);
						pagesize.setLayoutType('outsidebelow', 'startrow');



						var select= form.addField('custpage_selectpage','select', 'Select Records');	
						select.setLayoutType('outsidebelow', 'startrow');			
						select.setDisplaySize(200,30);


						if (request.getMethod() == 'GET'){

							pagesize.setDefaultValue("25");
							pagesizevalue=25;
						}
						else
						{
							if(request.getParameter('custpage_pagesize')!=null)
							{pagesizevalue= request.getParameter('custpage_pagesize');}
							else
							{pagesizevalue= 25;pagesize.setDefaultValue("25");}

						}
						form.setScript('customscript_inventoryclientvalidations');
						var len=temparraynew.length/parseFloat(pagesizevalue);
						for(var k=1;k<=Math.ceil(len);k++)
						{

							var from;var to;

							to=parseFloat(k)*parseFloat(pagesizevalue);
							from=(parseFloat(to)-parseFloat(pagesizevalue))+1;

							if(parseFloat(to)>temparraynew.length)
							{
								to=temparraynew.length;
								test=from.toString()+","+to.toString(); 
							}

							var temp=from.toString()+" to "+to.toString();
							var tempto=from.toString()+","+to.toString();
							select.addSelectOption(tempto,temp);

						} 
						if (request.getMethod() == 'POST'){

							if(request.getParameter('custpage_selectpage')!=null ){

								select.setDefaultValue(request.getParameter('custpage_selectpage'));	

							}
							if(request.getParameter('custpage_pagesize')!=null ){

								pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	

							}
						}
						else
						{

						}
					}
					else
					{
						pagesizevalue=25;
					}
					var minval=0;var maxval=parseFloat(pagesizevalue);
					if(parseFloat(pagesizevalue)>temparraynew.length)
					{
						maxval=temparraynew.length;
					}
					var selectno=request.getParameter('custpage_selectpage');
					if(selectno!=null )
					{
						var temp= request.getParameter('custpage_selectpage');
						var temparray=temp.split(',');
						nlapiLogExecution('ERROR', 'temparray',temparray.length);

						var diff=parseFloat(temparray[1])-(parseFloat(temparray[0])-1);
						nlapiLogExecution('ERROR', 'diff',diff);

						var pagevalue=request.getParameter('custpage_pagesize');
						nlapiLogExecution('ERROR', 'pagevalue',pagevalue);
						if(pagevalue!=null)
						{
							if(parseFloat(diff)==parseFloat(pagevalue)|| test==selectno)
							{

								var temparray=selectno.split(',');	
								nlapiLogExecution('ERROR', 'temparray.length ', temparray.length);  
								minval=parseFloat(temparray[0])-1;
								nlapiLogExecution('ERROR', 'temparray[0] ', temparray[0]);  
								maxval=parseFloat(temparray[1]);
								nlapiLogExecution('ERROR', 'temparray[1] ', temparray[1]);  
							}
						}
					}
					nlapiLogExecution('ERROR', 'invtsearchresults.length ', temparraynew.length);
					var index=1;
					nlapiLogExecution('ERROR', 'minval', minval);  

					nlapiLogExecution('ERROR', 'maxval ', maxval);  
					for (var s = minval; s < maxval; s++) 
					{
						var invtsearchresult = temparraynew[s];
						if(invtsearchresult!=null)
						{
							location = invtsearchresult.getValue('custrecord_ebiz_inv_binloc');
							locationname = invtsearchresult.getText('custrecord_ebiz_inv_binloc');
							pono = invtsearchresult.getValue('custrecord_ebiz_transaction_no');
							poname = invtsearchresult.getText('custrecord_ebiz_transaction_no');
							item = invtsearchresult.getValue('custrecord_ebiz_inv_sku');
							itemname = invtsearchresult.getText('custrecord_ebiz_inv_sku');
							lp = invtsearchresult.getValue('custrecord_ebiz_inv_lp');
							qty = invtsearchresult.getValue('custrecord_ebiz_qoh');
							lot = invtsearchresult.getValue('custrecord_ebiz_inv_lot');
							var lotText = invtsearchresult.getText('custrecord_ebiz_inv_lot');
							//nlapiLogExecution('ERROR', 'lot ', lot);
							//nlapiLogExecution('ERROR', 'lot text', lotText);
							itemstatus = invtsearchresult.getValue('custrecord_ebiz_inv_sku_status');
							packcode = invtsearchresult.getValue('custrecord_ebiz_inv_packcode');
							accontno = invtsearchresult.getValue('custrecord_ebiz_inv_account_no');
							invsiteloc = invtsearchresult.getValue('custrecord_ebiz_inv_loc');
							invcompany = invtsearchresult.getValue('custrecord_ebiz_inv_company');
							invfifodate = invtsearchresult.getValue('custrecord_ebiz_inv_fifo');
							invitemdesc = invtsearchresult.getValue('custrecord_ebiz_itemdesc');
							tasktype=invtsearchresult.getValue('custrecord_invttasktype');
							//case# 20148659
							wmsstatusflag=invtsearchresult.getValue('custrecord_wms_inv_status_flag');
							inboundbingroup=invtsearchresult.getValue('custrecord_inboundinvlocgroupid');
							outboundbingroup=invtsearchresult.getValue('custrecord_outboundinvlocgroupid');
							//case# 20148659
							var totQty = 0;
							var allocQty = 0;						
							if(invtsearchresult.getValue('custrecord_ebiz_qoh') != null && invtsearchresult.getValue('custrecord_ebiz_qoh') != "")
							{
								totQty = parseFloat(invtsearchresult.getValue('custrecord_ebiz_qoh'));
								if(totQty<0)
									totQty=0;
							}
							if(invtsearchresult.getValue('custrecord_ebiz_alloc_qty') != null && invtsearchresult.getValue('custrecord_ebiz_alloc_qty') != "")
							{
								allocQty = parseFloat(invtsearchresult.getValue('custrecord_ebiz_alloc_qty'));
								if(allocQty<0)
									allocQty=0;
							}


							var availQty=0;
							availQty=parseFloat(totQty)-parseFloat(allocQty);

							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_sno',index, (parseFloat(s)+1).toString());
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_pono',index, poname);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_invlocation',index, locationname);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_itemname', index, itemname);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_lp', index, lp);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_oldlp', index, lp);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_itemqty', index, qty);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_oldqty', index, qty);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_olditemstatus', index, itemstatus);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_accno', index, accontno);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_siteloc', index, invsiteloc);
							//for record Id.
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_recid', index, invtsearchresult.getId());
							//for loc id and itemid.
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_rectype', index, invtsearchresult.getRecordType());
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_itemid', index, item);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_locid', index, location);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_lot', index, lot);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_itemstatus', index, itemstatus);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_pc', index, packcode);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_lottext', index, lotText);

							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_totqty', index, totQty);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_allocqty', index, allocQty);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_allocqtyhid', index, allocQty);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_availqty', index, availQty);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_company', index, invcompany);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_fifodate', index, invfifodate);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_itemdesc', index, invitemdesc);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_povalue', index, pono);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_tasktype', index, tasktype);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_itemnewloc', index, location);
							//case# 20148659
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_wmsstatusflag', index, wmsstatusflag);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_inboundlocgroup', index, inboundbingroup);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_outboundlocgroup', index, outboundbingroup);
							//case#20148659
							//setDefaultValue


							index=index+1;
						}

					}
				}
			}

		}
		var tempflag = form.addField('custpage_tempflag', 'text','tempory flag').setDisplayType('hidden');
		var button = form.addSubmitButton('Save');
		//button.setDisabled(true);
		//var display=form.addButton('Display');
		form.addButton('custpage_displaybtn','Display','Display()');

	}
	else 
	{
		var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
		msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'No Records', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
	}
	response.writePage(form);
}

function statuschangeSuitelet(request, response){
	var ctx = nlapiGetContext();
	if (request.getMethod() == 'GET') 
	{
		FillForm(request,response);

	} 
	else 
	{    
		try{
		var form = nlapiCreateForm("Status Change");
		var tempflag = request.getParameter('custpage_tempflag');
		nlapiLogExecution('ERROR', 'Temp Flag', tempflag);
		if (tempflag != 'Display') {
			//form.addPageLink('crosslink', 'Back to Inventory Move', 'https://system.netsuite.com/app/site/hosting/scriptlet.nl?script=83&deploy=1');
			var linkURL = nlapiResolveURL('SUITELET', 'customscript_statuschange', 'customdeploy_statuschange_di');
			form.addPageLink('crosslink', 'Back to Inventory Move',linkURL);
			/*
		var ctx = nlapiGetContext();
		if (ctx.getExecutionContext() == 'userinterface' &&
			    type == 'view') {
		var InvURL = nlapiResolveURL('SUITELET', 'customscript_moveinventory', 'customdeploy_moveinventory_di');
        nlapiLogExecution('Error', 'Inv url', InvURL);

        InvURL = getFQDNForHost(ctx.getEnvironment()) + InvURL;
        nlapiLogExecution('Error', 'InvURL', InvURL);
		}
		form.addPageLink('crosslink', 'Back to Inventory Move', InvURL);
			 */
			var lpExists = 'N';		
			var userselection = false;
			var oldlocs = new Array();
			var newlocs = "";//new Array();
			var items = "";//new Array();
			var selectedcount = 0;
			var moveFlag;
			nlapiLogExecution('ERROR', 'Test3');
			var lincount = request.getLineItemCount('custpage_invtmovelist');
			var reportno=GetMaxTransactionNo('MOVEREPORT');
			nlapiLogExecution('ERROR', 'reportno', reportno);
			for (var p = 1; p <= lincount; p++) 
			{
				moveFlag = request.getLineItemValue('custpage_invtmovelist', 'custpage_invlocmove', p);
				if (moveFlag == 'T') 
				{
					userselection = true;	        	
				}

				nlapiLogExecution('ERROR', 'moveFlag', moveFlag);
				nlapiLogExecution('ERROR', 'lincount', lincount);


				//alert(moveFlag);
				if (moveFlag == 'T') {
					var ItemLocId = request.getLineItemValue('custpage_invtmovelist', 'custpage_locid', p);
					nlapiLogExecution('ERROR', 'ItemLocId', ItemLocId);
					var itemLocName = request.getLineItemValue('custpage_invtmovelist', 'custpage_invlocation', p);
					nlapiLogExecution('ERROR', 'itemLocName', itemLocName);
					var ItemName = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemname', p);
					var ItemId = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemid', p);
					var ItemLP = request.getLineItemValue('custpage_invtmovelist', 'custpage_lp', p);
					var ItemQty = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemqty', p);
					var ItemNewLocId = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemnewloc', p);
					var ItemNewLocName = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemnewloc', p);
					var ItemNewQty = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemnewqty', p);
					var ItemOldQty = request.getLineItemValue('custpage_invtmovelist', 'custpage_oldqty', p);				
					var ItemNewLP = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemnewlp', p);
					var RecdID = request.getLineItemValue('custpage_invtmovelist', 'custpage_recid', p);
					//var RecdType = nlapiGetLineItemValue('custpage_invtmovelist', 'custpage_rectype', p);
					var LotBatch = request.getLineItemValue('custpage_invtmovelist', 'custpage_lot', p);
					var LotBatchText = request.getLineItemValue('custpage_invtmovelist', 'custpage_lottext', p);

					var itmstatus = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemstatus', p);
					var packcode = request.getLineItemValue('custpage_invtmovelist', 'custpage_pc', p);
					var accountno = request.getLineItemValue('custpage_invtmovelist', 'custpage_accno', p);
					var siteloc = request.getLineItemValue('custpage_invtmovelist', 'custpage_siteloc', p);


					var ItemNewStatus = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemnewstatus', p);
					if(ItemNewStatus==null || ItemNewStatus=='')
						var ItemNewStatus=request.getParameter('custpage_bulkitemstatus');

					//var InvAdjType = request.getLineItemValue('custpage_invtmovelist', 'custpage_invadjtype', p);
					var Invcompany = request.getLineItemValue('custpage_invtmovelist', 'custpage_company', p);
					var Invfifodate = request.getLineItemValue('custpage_invtmovelist', 'custpage_fifodate', p);
					var Invitemdesc = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemdesc', p);
					var povalue = request.getLineItemValue('custpage_invtmovelist', 'custpage_povalue', p);
					var tasktype=request.getLineItemValue('custpage_invtmovelist', 'custpage_tasktype', p);
					//case# 20148659 starts
					var wmsstatusflag=request.getLineItemValue('custpage_invtmovelist', 'custpage_wmsstatusflag', p);
					var inboundbingroup=request.getLineItemValue('custpage_invtmovelist', 'custpage_inboundlocgroup', p);
					var outboundbingroup=request.getLineItemValue('custpage_invtmovelist', 'custpage_outboundlocgroup', p);
					//case# 20148659 ends
					nlapiLogExecution('ERROR', 'Item LP', ItemNewLP);
					var itemCube = 0;			  

					var filters = new Array();
					filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', ItemId);
					filters[1] = new nlobjSearchFilter('custrecord_ebizuomskudim', null, 'is', 1);

					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_ebizcube'); 
					var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

					//nlapiLogExecution('ERROR', 'Search Length of SKU Dims in inventory Move Screen',skuDimsSearchResults.length);

					if(skuDimsSearchResults != null && skuDimsSearchResults != "" ){
						nlapiLogExecution('ERROR', 'skuDimsSearchResults', skuDimsSearchResults.length);
						for (var i = 0; i < skuDimsSearchResults.length; i++) {
							var skuDim = skuDimsSearchResults[i];
							itemCube  =  skuDim.getValue('custrecord_ebizcube');		    	
						}
					}


					//alert("OLD QTY::"+ItemQty);
					//alert("NEW QTY::"+ItemNewQty);			    
					//alert("itemCube::"+itemCube);
					if(ItemLocId!=null && ItemLocId!='')
					{
						var filterLocation = new Array();
						filterLocation[0] = new nlobjSearchFilter('id', null, 'equalto', ItemLocId);          
						var OldLocationColumns = new Array();
						OldLocationColumns[0]=new nlobjSearchColumn('custrecord_remainingcube');     
						var LocationResults =nlapiSearchRecord('customrecord_ebiznet_location', null, filterLocation,OldLocationColumns);
						var voldLocRemainingCube =  LocationResults[0].getValue('custrecord_remainingcube');
						nlapiLogExecution('ERROR', 'LocationResults', LocationResults.length);
					}
					//alert("voldLocRemainingCube::"+voldLocRemainingCube);
					//alert("old Location" + itemLocName + "," + ItemLocId);


					//Changes done by Satish.N
					//case start 20124016ï¿½ added if condtion
					if(ItemNewLocId!=null && ItemNewLocId!='' && ItemLocId!=ItemNewLocId)
					{
						//case start end
						var newfilterLocation = new Array();
						newfilterLocation[0] = new nlobjSearchFilter('id', null, 'equalto', ItemNewLocId);          
						var newLocationColumns = new Array();
						newLocationColumns[0]=new nlobjSearchColumn('custrecord_remainingcube');     
						var newLocationResults =nlapiSearchRecord('customrecord_ebiznet_location', null, newfilterLocation,newLocationColumns);
						var vnewLocRemainingCube =  newLocationResults[0].getValue('custrecord_remainingcube');
						nlapiLogExecution('ERROR', 'newLocationResults', newLocationResults.length);
						nlapiLogExecution('ERROR', 'vnewLocRemainingCube ',vnewLocRemainingCube);
						//alert("New Location" + ItemNewLocName + "," + ItemNewLocId);

						if(parseFloat(vnewLocRemainingCube)<0)
						{
							nlapiLogExecution('ERROR', 'Qty Exceeds Location Capacity in To Location',ItemNewLocId);
							alert("Qty Exceeds Location Capacity in To Location" + vnewLocRemainingCube);
							showInlineMessage(form, 'Error', 'Qty Exceeds Location Capacity in To Location',' LP# = ' + ItemLP.toString());
							response.writePage(form);
							return false;
						}

						var voldLocCube;
						var vnewLocCube;

						voldLocCube =  parseFloat(voldLocRemainingCube)  + (ItemNewQty * parseFloat(itemCube));
						vnewLocCube = parseFloat(vnewLocRemainingCube)  - (ItemNewQty * parseFloat(itemCube));

						nlapiLogExecution('ERROR', 'vnewLocCube ',vnewLocCube);

						if(parseFloat(vnewLocCube)<0)
						{
							showInlineMessage(form, 'Error', 'Qty Exceeds Location Capacity in To Location',' LP# = ' + ItemLP.toString());
							alert("Qty Exceeds Location Capacity,Max move Qty : " + parseFloat(parseFloat(vnewLocRemainingCube)/parseFloat(itemCube)));
							response.writePage(form);
							return false;
						}

						//alert("voldLocCube::"+voldLocCube);
						//alert("vnewLocCube::"+vnewLocCube);

						var oldretValue = UpdateLocCube(ItemLocId, voldLocCube);	

						var newretValue = UpdateLocCube(ItemNewLocId, vnewLocCube);

						nlapiLogExecution('ERROR', 'oldretValue ',oldretValue);
						nlapiLogExecution('ERROR', 'newretValue ',newretValue);
					}
					//upto to here


					//LP Checking in masterlp record starts
					//if (LPReturnValue == true) 
					//{
					try {
						nlapiLogExecution('ERROR', 'INTO MASTER LP INSERTION',ItemNewLP);
						if(ItemNewLP!=null && ItemNewLP!='')
						{
							var filtersmlp = new Array();
							filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', ItemNewLP);

							var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

							if (SrchRecord != null && SrchRecord.length > 0) 
							{
								nlapiLogExecution('ERROR', 'LP FOUND');
								if(ItemQty==ItemNewQty)
								{
									lpExists = 'N';
								}
								else
								{
									lpExists = 'Y';
								}
							}
							else 
							{
								nlapiLogExecution('ERROR', 'LP NOT FOUND');
								var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
								customrecord.setFieldValue('name', ItemNewLP);
								customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', ItemNewLP);
								var rec = nlapiSubmitRecord(customrecord, false, true);
							}

							if(lpExists == 'Y')
							{
								var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
								msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'LP Already Exists', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
								response.writePage(form);
								/*alert('LP Already Exists!!!');*/
								return false;				
							}
						}
						else
						{
							if(parseFloat(ItemQty)!= parseFloat(ItemNewQty))
							{
								ItemNewLP=GetMaxLPNo('1', '1',siteloc);
								nlapiLogExecution('ERROR','ItemNewLP',ItemNewLP);

								var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
								customrecord.setFieldValue('name', ItemNewLP);
								customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', ItemNewLP);
								var rec = nlapiSubmitRecord(customrecord, false, true);
							}
						}
					} 
					catch (e) 
					{
						nlapiLogExecution('ERROR', 'Failed to Update/Insert into Master LP Record : '+e);
					}
					var lockrecid='';
					try
					{
						// To lock sales order
							
							var trantype = 'inventory';
							nlapiLogExecution('DEBUG','ItemNewLP before Locking',ItemNewLP);
							nlapiLogExecution('DEBUG','trantype before Locking',trantype);
                      
							var lpRec= nlapiCreateRecord('customrecord_ebiznet_lockrecs');
                      if(ItemNewLP != '' && ItemNewLP != null)
                        {
							lpRec.setFieldValue('name', ItemNewLP);	
                        }else{
                          lpRec.setFieldValue('name', ItemLP);
                        }
							lpRec.setFieldValue('custrecord_ebiznet_trantype', trantype);
							lpRec.setFieldValue('externalid', ItemNewLP);
							lpRec.setFieldValue('custrecord_ebiznet_notes', 'Locked by UE');
							lockrecid = nlapiSubmitRecord(lpRec,false,true );
							nlapiLogExecution('DEBUG','Locked successfully',lockrecid);
						
					}
					catch(e)
					{
						nlapiLogExecution('DEBUG', 'DEBUG', e.getCode());
						var wmsE = nlapiCreateError('FULFILLMENT_ORDER_RACE_COND_DETECTED', 'Another thread is currently processing this fulfillment order, which prevents your request from executed.', true);
						throw wmsE;
					}
					
					try{

						var recLoad = nlapiLoadRecord('customrecord_ebiznet_createinv', RecdID);
						ItemQty = recLoad.getFieldValue('custrecord_ebiz_qoh');
						
						nlapiLogExecution('ERROR', 'ItemQty ', ItemQty);
						nlapiLogExecution('ERROR', 'ItemNewQty ', ItemNewQty);
						if((parseFloat(ItemQty) - parseFloat(ItemNewQty)).toFixed(5)>=0)
							{
						recLoad.setFieldValue('custrecord_ebiz_inv_qty', (parseFloat(ItemQty) - parseFloat(ItemNewQty)).toFixed(5));
						recLoad.setFieldValue('custrecord_ebiz_qoh', (parseFloat(ItemQty) - parseFloat(ItemNewQty)).toFixed(5));
						recLoad.setFieldValue('custrecord_updated_user_no', nlapiGetContext().getUser());
						recLoad.setFieldValue('custrecord_ebiz_displayfield','N');								
						nlapiSubmitRecord(recLoad, false, true);
						//}
						nlapiLogExecution('ERROR', 'ItemQty ', ItemQty);
						nlapiLogExecution('ERROR', 'ItemNewQty ', ItemNewQty);
						if((parseFloat(ItemQty) - parseFloat(ItemNewQty)) == 0)
						{					
							var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', RecdID);				
						}

						//fnUpdateputawaytask(povalue,ItemNewStatus)
						//fnMoveInventory(itmstatus,ItemNewStatus,ItemLocId,itemLocName,ItemName,ItemId,ItemLP,ItemQty,ItemNewLocId,ItemNewLocName,ItemNewQty,ItemNewLP,RecdID,LotBatch,LotBatchText,itmstatus,packcode,accountno,siteloc,ItemNewStatus,reportno,ItemOldQty,siteloc,Invcompany,Invfifodate,Invitemdesc,tasktype,povalue,wmsstatusflag,inboundbingroup,outboundbingroup);
						fnMoveInventory(itmstatus,ItemNewStatus,ItemLocId,itemLocName,ItemName,ItemId,ItemLP,ItemQty,ItemNewLocId,ItemNewLocName,ItemNewQty,ItemNewLP,RecdID,LotBatch,LotBatchText,itmstatus,packcode,accountno,siteloc,ItemNewStatus,reportno,ItemOldQty,siteloc,Invcompany,Invfifodate,Invitemdesc,tasktype,povalue,wmsstatusflag,inboundbingroup,outboundbingroup);
					}
					else{
						var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'ERROR', 'Inventory is processing by Other User.', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
						response.writePage(form);
						return false;
						}
					}
					catch(e)
					{
						nlapiLogExecution('DEBUG', 'DEBUG', e.getCode());
						var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'ERROR', 'Inventory in this LP has already moved.', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
						response.writePage(form);
						return false;			
						nlapiLogExecution('DEBUG', 'DEBUG', e.getCode());
					}
					//fnUpdateputawaytask(povalue,ItemNewStatus)
				
				}
			}

			var invmoveDetails = new Array();
			invmoveDetails["custpage_reportno"] = reportno;

			if(userselection==true)
			{
				response.sendRedirect('SUITELET', 'customscript_inv_move_report', 'customdeploy_inv_move_report_di', false, invmoveDetails);
//				var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
//				msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Inventory Move Successful', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
//				response.writePage(form);
			}
			else
			{
				FillForm(request,response);
			}

			//response.writePage(form);
		}
		else
		{
			FillForm(request,response);
		}
		}
		catch (e) 
		{
				nlapiLogExecution('ERROR', 'Exception occured: '+e);
		}
		finally
		{
			nlapiLogExecution('DEBUG','ItemNewLP before unlocking',ItemNewLP);
			nlapiLogExecution('DEBUG','lockrecid before unlocking',lockrecid);	

          if(lockrecid != '' && lockrecid != null)
            {
				nlapiDeleteRecord('customrecord_ebiznet_lockrecs',lockrecid);
            }
				nlapiLogExecution('DEBUG','after unlocking','after unlocking');
			
		}
	}

}

function fnUpdateputawaytask(povalue,itemStatus)
{

	var filters = new Array();

	filters[0] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', povalue);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'is', 2);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_sku_status'); 

	var opentaskresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	if(opentaskresults!=null && opentaskresults!='')
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', opentaskresults[0].getId());

	if(transaction!=null && transaction!='')
	{
		transaction.setFieldValue('custrecord_sku_status',itemStatus);	
		nlapiSubmitRecord(transaction);	
	}



}

function fnMoveInventory(itmstatus,ItemNewStatus,ItemLocId,itemLocName,ItemName,ItemId,ItemLP,ItemQty,ItemNewLocId,ItemNewLocName,
		ItemNewQty,ItemNewLP,RecdID,LotBatch,LotBatchText,itmstatus,packcode,accountno,siteloc,ItemNewStatus,reportno,ItemOldQty,
		siteloc,Invcompany,Invfifodate,Invitemdesc,tasktype,vOrderNo,wmsstatusflag,inboundbingroup,outboundbingroup)
{
	try
	{
		var now = new Date();
		//now= now.getHours();
		//Getting time in hh:mm tt format.
		var a_p = "";
		var d = new Date();
		var curr_hour = now.getHours();
		if (curr_hour < 12) {
			a_p = "am";
		}
		else {
			a_p = "pm";
		}
		if (curr_hour == 0) {
			curr_hour = 12;
		}
		if (curr_hour > 12) {
			curr_hour = curr_hour - 12;
		}

		var curr_min = now.getMinutes();

		curr_min = curr_min + "";

		if (curr_min.length == 1) {
			curr_min = "0" + curr_min;
		}

		var CallFlag='N';
		var OldMakeWHFlag='Y';
		var NewMakeWHFlag='Y';
		var vNewWHlocatin;
		var vOldWHlocatin;
		var vNSFlag;
		var UpdateFlag;
		var vNSCallFlag='N';
		if(itmstatus==ItemNewStatus)
			CallFlag='N';
		else
		{

			vNSFlag=GetNSCallFlag(itmstatus,ItemNewStatus);
			nlapiLogExecution('ERROR', 'vNSFlag',vNSFlag);

			if(vNSFlag!= null && vNSFlag!= "" && vNSFlag.length>0)
			{
				CallFlag=vNSFlag[0];
				nlapiLogExecution('ERROR', 'vNSFlag[0]',vNSFlag[0]);
			}
			if(vNSFlag[0]=='N')
				CallFlag='N';
			else if(vNSFlag[0]=='Y') 
			{ 
				OldMakeWHFlag= GetMWFlag(vNSFlag[1]);
				NewMakeWHFlag= GetMWFlag(vNSFlag[2]);
				vNewWHlocatin=vNSFlag[2]; 
				vOldWHlocatin=vNSFlag[1]; 
			}


		}
		var vTaskType='9';
		if(CallFlag=='N')
			vTaskType='9';
		else
			vTaskType='18';


		//Check condition if old LP not equal to New LP.
		//If LP is not equal need to create new record in trn_opentask
		//reduce the quantity in old LP.
		//if (ItemLP != ItemNewLP) {
		//Create a new record in trn_opentask with task type='MOVE' and new record in trn_inventory .
		var opentaskrec = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');


		nlapiLogExecution('Error', 'itmstatus :', itmstatus);
		nlapiLogExecution('Error', 'ItemNewStatus :', ItemNewStatus);
		nlapiLogExecution('Error', 'packcode :', packcode);
		nlapiLogExecution('Error', 'LotBatch :', LotBatch);
		nlapiLogExecution('Error', 'LotBatchText :', LotBatchText);
		nlapiLogExecution('Error', 'ItemNewQty :', ItemNewQty);
		nlapiLogExecution('Error', 'ItemOldQty :', ItemOldQty);
		nlapiLogExecution('Error', 'ItemNewLP :', ItemNewLP);
		//nlapiLogExecution('Error', 'InvAdjType :', InvAdjType);
		nlapiLogExecution('Error', 'ItemLP :', ItemLP);
		nlapiLogExecution('Error', 'ItemLocId :', ItemLocId);
		nlapiLogExecution('Error', 'itemLocName :', itemLocName);
		nlapiLogExecution('Error', 'reportno :', reportno);
		nlapiLogExecution('Error', 'siteloc :', siteloc);



		//Added for Item id.
		opentaskrec.setFieldValue('name', itemLocName);
		opentaskrec.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		opentaskrec.setFieldValue('custrecord_act_qty', parseFloat(ItemQty).toFixed(5));//Assigning new quantity

		if( (ItemNewLP == "" || ItemNewLP == null))
		{
			//if(parseFloat(ItemOldQty) == parseFloat(ItemNewQty))
			//{
			ItemNewLP=ItemLP;
			//}
			//else
			//{
			//	ItemNewLP=GetMaxLPNo('1', '1');
			//	nlapiLogExecution('Error', 'auto generated lp :', ItemNewLP);
			//}
		}

		opentaskrec.setFieldValue('custrecord_lpno', ItemNewLP);
		opentaskrec.setFieldValue('custrecord_ebiz_lpno', ItemNewLP);


		opentaskrec.setFieldValue('custrecord_tasktype', vTaskType);

		opentaskrec.setFieldValue('custrecord_actbeginloc', ItemLocId);
		opentaskrec.setFieldValue('custrecord_actendloc', ItemNewLocId);

		//Added for Item name and desc
		opentaskrec.setFieldValue('custrecord_sku', ItemId);
		opentaskrec.setFieldValue('custrecord_skudesc', ItemName);

		opentaskrec.setFieldValue('custrecordact_begin_date', DateStamp());
		opentaskrec.setFieldValue('custrecord_act_end_date', DateStamp());

		//Adding fields to update time zones.
		opentaskrec.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
		opentaskrec.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
		opentaskrec.setFieldValue('custrecord_recordtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
		opentaskrec.setFieldValue('custrecord_recordupdatetime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
		opentaskrec.setFieldValue('custrecord_current_date', DateStamp());
		opentaskrec.setFieldValue('custrecord_upd_date', DateStamp());
		//Status flag .
		opentaskrec.setFieldValue('custrecord_wms_status_flag', 19);
		opentaskrec.setFieldValue('custrecord_batch_no', LotBatchText);
		opentaskrec.setFieldValue('custrecord_upd_ebiz_user_no', nlapiGetContext().getUser());

		//added by mahesh


		if(ItemNewQty == null || ItemNewQty == "")
			ItemNewQty=ItemOldQty;
		opentaskrec.setFieldValue('custrecord_sku_status', itmstatus);
		opentaskrec.setFieldValue('custrecord_ebiz_new_sku_status', ItemNewStatus);
		opentaskrec.setFieldValue('custrecord_packcode', packcode);
		if(LotBatchText != null && LotBatchText !=""){
			opentaskrec.setFieldValue('custrecord_lotnowithquantity', LotBatchText+'('+ItemNewQty+')');}
		opentaskrec.setFieldValue('custrecord_expe_qty', parseFloat(ItemNewQty).toFixed(5));
		opentaskrec.setFieldValue('custrecord_ebiz_new_lp', ItemNewLP);
		//	opentaskrec.setFieldValue('custrecord_ebiz_ot_adjtype', InvAdjType);
		opentaskrec.setFieldValue('custrecord_from_lp_no', ItemLP);
		opentaskrec.setFieldValue('custrecord_wms_location', siteloc);
		opentaskrec.setFieldValue('custrecord_ebiz_report_no', reportno.toString());

		nlapiLogExecution('Error', 'Submitting MOVE record', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(opentaskrec, false, true);
		nlapiLogExecution('Error', 'Done MOVE Record Insertion :', 'Success');
		//alert("Done MOVE Record Insertion :");

		nlapiLogExecution('Error', 'NewMakeWHFlag :', NewMakeWHFlag);
		nlapiLogExecution('Error', 'itmstatus :', itmstatus);
		nlapiLogExecution('Error', 'ItemNewStatus :', ItemNewStatus);

		nlapiLogExecution('Error', 'ItemLocId :', ItemLocId);		
		nlapiLogExecution('Error', 'ItemNewLocId :', ItemNewLocId);

		if(ItemNewStatus==null||ItemNewStatus=='')
			ItemNewStatus=itmstatus;
nlapiLogExecution('Error', 'ItemLP :', ItemLP);
		if(NewMakeWHFlag=='Y')
		{ 
			if(vNewWHlocatin!=null && vNewWHlocatin!='')
			{
				CreateINVTRecord(itemLocName, ItemLocId,ItemNewLocId, ItemNewLP, ItemId, ItemNewQty, ItemName, ItemNewStatus, packcode, LotBatch, vNewWHlocatin, accountno,RecdID,vNSCallFlag,vTaskType,Invcompany,Invfifodate,Invitemdesc,vOrderNo,ItemLP,wmsstatusflag,inboundbingroup,outboundbingroup);
			}
			else
			{
				CreateINVTRecord(itemLocName, ItemLocId,ItemNewLocId, ItemNewLP, ItemId, ItemNewQty, ItemName, ItemNewStatus, packcode, LotBatch, siteloc, accountno,RecdID,vNSCallFlag,vTaskType,Invcompany,Invfifodate,Invitemdesc,vOrderNo,ItemLP,wmsstatusflag,inboundbingroup,outboundbingroup);
			}
		}
		nlapiLogExecution('Error', 'CallFlag', CallFlag);
		nlapiLogExecution('Error', 'tasktype', tasktype);
		//case# 20148655 starts
		var fields = ['recordType', 'custitem_ebizserialin'];
		var columns = nlapiLookupField('item', ItemId, fields);
		var ItemType = columns.recordType;
		nlapiLogExecution('Error','ItemType',ItemType);

		var serialInflg="F";		
		serialInflg = columns.custitem_ebizserialin;		

		var localSerialNoArray = new Array();
		if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") 
		{
			localSerialNoArray = AdjustSerialNumbers(localSerialNoArray,ItemNewQty,ItemId,ItemLP,ItemNewLP,"Y",ItemNewLocId,ItemOldQty,vNewWHlocatin);
		}
		//case# 20148655 ends
		
		if(CallFlag=='Y')//To trigger NS automatically
		{
			//if(tasktype=='2')
			//case# 20148655 starts
			var serialnumbers = "";
			if(localSerialNoArray != null || localSerialNoArray != "")
			{
				//nlapiLogExecution('DEBUG','localSerialNoArray',localSerialNoArray.ToString());
				if(localSerialNoArray.length != 0)
				{
					nlapiLogExecution('DEBUG','localSerialNoArray.length',localSerialNoArray.length);
					serialnumbers = getSerialNoCSV(localSerialNoArray);
					nlapiLogExecution('DEBUG','serialnumbers',serialnumbers);
				}
			}
			nlapiLogExecution('DEBUG','lot before',LotBatchText);
			if(LotBatchText == '' || LotBatchText == null)
			{
				LotBatchText = serialnumbers;
			}
			nlapiLogExecution('DEBUG','lot after',LotBatchText);
			//case# 20148655 ends
			InvokeNSInventoryTransfer(ItemId,ItemNewStatus,vOldWHlocatin,vNewWHlocatin,ItemNewQty,LotBatchText);

			//Code for fetching account no from stockadjustment custom record	
			/*
			 * This part of code is commented as per inputs from NS team
			 * For all vitual location inventory move NS team suggested us to us Inventory Transfer object instead of inventory adjustment object
			 * Averate cost/price of the item will not change if we use Inventory Transfer object

			//Code for fetching account no from stockadjustment custom record	
			var AccountNo = getStockAdjustmentAccountNoNew(InvAdjType);

			if(AccountNo !=null && AccountNo != "" && AccountNo.length>0)
			{
				var FromAccNo=AccountNo[0];
				var ToAccNo=AccountNo[1];
				nlapiLogExecution('ERROR', 'vOldWHlocatin',vOldWHlocatin);
				nlapiLogExecution('ERROR', 'vNewWHlocatin',vNewWHlocatin);

				nlapiLogExecution('ERROR', 'FromAccNo',FromAccNo);
				nlapiLogExecution('ERROR', 'ToAccNo',ToAccNo);
				nlapiLogExecution('ERROR', 'LotBatchText',LotBatchText);
				InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vOldWHlocatin,-(parseFloat(ItemNewQty)),"",vTaskType,InvAdjType,LotBatchText,FromAccNo);
				InvokeNSInventoryAdjustmentNew(ItemId,ItemNewStatus,vNewWHlocatin,ItemNewQty,"",vTaskType,InvAdjType,LotBatchText,ToAccNo);

			}
			 */ 
		}
		//Need to load that Record and update the qty.
nlapiLogExecution('ERROR', 'ItemQty',ItemQty);
nlapiLogExecution('ERROR', 'ItemNewQty',ItemNewQty);

nlapiLogExecution('ERROR', 'RecdID',RecdID);

		var vremqty=parseFloat(ItemQty) - parseFloat(ItemNewQty);
		nlapiLogExecution('ERROR', 'vremqty ', vremqty);
		/*if(vremqty==0)
		{
			nlapiDeleteRecord('customrecord_ebiznet_createinv', RecdID);
		}
		else
		{*/
		/*var recLoad = nlapiLoadRecord('customrecord_ebiznet_createinv', RecdID);
		recLoad.setFieldValue('custrecord_ebiz_inv_qty', (parseFloat(ItemQty) - parseFloat(ItemNewQty)).toFixed(5));
		recLoad.setFieldValue('custrecord_ebiz_qoh', (parseFloat(ItemQty) - parseFloat(ItemNewQty)).toFixed(5));
		recLoad.setFieldValue('custrecord_updated_user_no', nlapiGetContext().getUser());
		recLoad.setFieldValue('custrecord_ebiz_displayfield','N');								
		nlapiSubmitRecord(recLoad, false, true);
		//}
		nlapiLogExecution('ERROR', 'ItemQty ', ItemQty);
		nlapiLogExecution('ERROR', 'ItemNewQty ', ItemNewQty);
		if((parseFloat(ItemQty) - parseFloat(ItemNewQty)) == 0)
		{					
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', RecdID);				
		}*/
	}
	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in fnMoveInventory ', exp);	

	}
//	return true;
}

function CreateINVTRecord(itemLocName, ItemLocId,ItemNewLocId, ItemNewLP, ItemId, ItemNewQty, ItemName, itemstatus, packcode, LotBatch, siteloc, accountno,RecdID,CallFlag,vTaskType,Invcompany,Invfifodate,Invitemdesc,vOrderNo,ItemLP,wmsstatusflag,inboundbingroup,outboundbingroup)
{
	try{
		nlapiLogExecution('ERROR', 'Into CreateINVTRecord',itemLocName);

		vTaskType=2;
		var varPaltQty = getMaxUOMQty(ItemId,packcode);
		nlapiLogExecution('ERROR', 'varPaltQty', varPaltQty);
		nlapiLogExecution('ERROR', 'itemstatus',itemstatus);
		nlapiLogExecution('ERROR', 'ItemNewLP',ItemNewLP);
		nlapiLogExecution('ERROR', 'itemLocName', itemLocName);
		nlapiLogExecution('ERROR', 'ItemLocId', ItemLocId);
		nlapiLogExecution('ERROR', 'ItemNewLocId', ItemNewLocId);
		nlapiLogExecution('ERROR', 'ItemNewLP', ItemNewLP);
		nlapiLogExecution('ERROR', 'ItemId', ItemId);
		nlapiLogExecution('ERROR', 'ItemNewQty', ItemNewQty);
		nlapiLogExecution('ERROR', 'ItemName', ItemName);
		nlapiLogExecution('ERROR', 'siteloc', siteloc);
		nlapiLogExecution('ERROR', 'accountno', accountno);
		nlapiLogExecution('ERROR', 'itemstatus', itemstatus);
		nlapiLogExecution('ERROR', 'packcode', packcode);
		nlapiLogExecution('ERROR', 'CallFlag', CallFlag);
		nlapiLogExecution('ERROR', 'LotBatch', LotBatch);
		nlapiLogExecution('ERROR', 'Invcompany', Invcompany);
		nlapiLogExecution('ERROR', 'Invfifodate', Invfifodate);
		nlapiLogExecution('ERROR', 'Invitemdesc', Invitemdesc);
		nlapiLogExecution('ERROR', 'vTaskType', vTaskType);
		nlapiLogExecution('ERROR', 'vOrderNo', vOrderNo);
		nlapiLogExecution('ERROR', 'vOrderNo', ItemLP);
		//if item,itemstatus,lot and packcode already exists in inventory then 
		//update the qty else create a new record in inventory
		//if(ItemLocId!=ItemNewLocId && ItemNewLocId!=null && ItemNewLocId!='')

		var expdate;
		if(LotBatch!="" && LotBatch!=null)
		{

			var filterspor = new Array();
			//case 20124017 Start : change filter condition
			filterspor.push(new nlobjSearchFilter('internalid', null, 'is', LotBatch));
			////case 20124017 end
			/*if(whLocation!=null && whLocation!="")
				filterspor[1] = new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', whLocation);*/
			if(ItemId!=null && ItemId!="")
				filterspor.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', ItemId));

			var column=new Array();
			column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
			column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');

			var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
			if(receiptsearchresults!=null)
			{
				//getlotnoid= receiptsearchresults[0].getId();
				//nlapiLogExecution('DEBUG', 'getlotnoid', getlotnoid);
				expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');
				if(Invfifodate !=null && Invfifodate !='')
					Invfifodate = receiptsearchresults[0].getValue('custrecord_ebizfifodate');
				//vfifoDate = receiptsearchresults[0].getValue('custrecord_ebizfifodate');
			}
		}

		if(ItemNewLocId!=null && ItemNewLocId!='')
		{
			var invExistQty=0;
			var filters = new Array();
			var i = 0;
			if (ItemId != "") {		
				filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', ItemId);
				i++;
			}
			if(itemstatus!="")
			{
				filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', itemstatus);
				i++;
			}
			// Case# 20148048 starts
//			if(ItemNewLP !=null && ItemNewLP!="")
//			{
//			filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', ItemNewLP);
//			i++;
//			}
			if(ItemNewLP !=null && ItemNewLP!="")
			{
				filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', ItemNewLP);
				i++;
			}
			// Case# 20148048 ends
			if(ItemLP !=null && ItemLP!="")
			{
			filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'isnot', ItemLP);
			i++;
			}

			nlapiLogExecution('ERROR', 'packcode',packcode);
			if(packcode !=null && packcode!="")
			{
				filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_packcode', null, 'is', packcode);
				i++;
			}
			nlapiLogExecution('ERROR', 'ItemNewLocId',ItemNewLocId);
			if(ItemNewLocId!=null&&ItemNewLocId!="")
			{
				filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', ItemNewLocId);
				i++;
			}

			filters[i] = new nlobjSearchFilter('custrecord_invttasktype', null, 'noneof', 18);

//			if(fifodate!=null && fifodate!='')
//			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_fifo', null, 'on', fifodate));

			if(LotBatch!=null && LotBatch!='')
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'is', LotBatch));

			if(varPaltQty!=null && varPaltQty!='')
				filters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'lessthan', varPaltQty));

			filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['3','19','17']));

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh'); 
			var inventorysearch = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

			var vid=0;
			var invExistLp='';
			if(inventorysearch != null)
			{
				nlapiLogExecution('ERROR', 'Inventory found in same location for same item',ItemId);
				//for (var k = 0; k < inventorysearch.length; k++) {
				//var invqty = inventorysearch[k];
				//invExistQty  =  invqty.getValue('custrecord_ebiz_avl_qty');		    	
				//}
				//custrecord_ebiz_avl_qty

				invExistQty  =  inventorysearch[0].getValue('custrecord_ebiz_qoh');	
				invExistLp = inventorysearch[0].getValue('custrecord_ebiz_inv_lp');	
				vid = inventorysearch[0].getId();
			}

			nlapiLogExecution('ERROR', 'invExistQty',invExistQty);
			nlapiLogExecution('ERROR', 'invExistLp',invExistLp);
			nlapiLogExecution('ERROR', 'vid ',vid );

			//alert("vid::"+vid);
			//alert("ItemNewLocId::"+ItemNewLocId);
			//alert("ItemNewQty::"+ItemNewQty);	    
			//alert("invExistQty::"+invExistQty);

			if(parseFloat(invExistQty) > 0&&vid!=null&&vid!=""){  
				nlapiLogExecution('ERROR', 'If invExistQty > 0',invExistQty);
				nlapiLogExecution('ERROR', 'ItemNewQty',ItemNewQty);
				var invLoad = nlapiLoadRecord('customrecord_ebiznet_createinv', vid);
				var updatedInventory = parseFloat(invExistQty) + parseFloat(ItemNewQty);

				invLoad.setFieldValue('custrecord_ebiz_qoh', parseFloat(updatedInventory).toFixed(5));		
				//alert("Sum Value::" + (parseFloat(invExistQty) + parseFloat(ItemNewQty)));		
				invLoad.setFieldValue('custrecord_updated_user_no', nlapiGetContext().getUser());
				invLoad.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
				invLoad.setFieldValue('custrecord_ebiz_displayfield','N');	
				invLoad.setFieldValue('custrecord_ebiz_callinv', CallFlag);


				var tresult = nlapiSubmitRecord(invLoad, false, true);
				nlapiLogExecution('ERROR','updated invt record ',tresult);

			}
			else
			{	
				nlapiLogExecution('ERROR', 'If invExistQty Not Greater than 0',invExistQty);
				if(ItemNewLocId == null || ItemNewLocId == "")
					ItemNewLocId=ItemLocId;
				if(ItemNewLP == null || ItemNewLP == "")
					ItemNewLP=ItemLP;

				//Create Trn_inventory Record.
				var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');
				//var vIntValue="N";
				nlapiLogExecution('ERROR', 'Creating Inventory Record', 'INVT');
				invtRec.setFieldValue('name', vOrderNo);

				if(ItemNewLocId!=null&&ItemNewLocId!="")
					invtRec.setFieldValue('custrecord_ebiz_inv_binloc', ItemNewLocId);
				invtRec.setFieldValue('custrecord_ebiz_inv_lp', ItemNewLP);

				if(ItemId!=null&&ItemId!="")
					invtRec.setFieldValue('custrecord_ebiz_inv_sku', ItemId);
				invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(ItemNewQty).toFixed(5));
				invtRec.setFieldValue('custrecord_inv_ebizsku_no', ItemId);
				invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(ItemNewQty).toFixed(5));
				invtRec.setFieldValue('custrecord_ebiz_itemdesc', ItemName);
				//invtRec.setFieldValue('custrecord_invttasktype', 9);
				nlapiLogExecution('ERROR', 'chkpt');
				if(siteloc!=null&&siteloc!="")
					invtRec.setFieldValue('custrecord_ebiz_inv_loc', siteloc);
				if(accountno!=null&&accountno!="")
					invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountno);
				if(itemstatus!=null&&itemstatus!="")
					invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
				if(packcode!=null&&packcode!="")
					invtRec.setFieldValue('custrecord_ebiz_inv_packcode', packcode);			
				invtRec.setFieldValue('custrecord_ebiz_callinv', CallFlag);
				invtRec.setFieldValue('custrecord_ebiz_displayfield','N');				
				//case# 20148659
				//invtRec.setFieldValue('custrecord_wms_inv_status_flag',19);
				invtRec.setFieldValue('custrecord_wms_inv_status_flag',wmsstatusflag);
				invtRec.setFieldValue('custrecord_inboundinvlocgroupid',inboundbingroup);
				invtRec.setFieldValue('custrecord_outboundinvlocgroupid',outboundbingroup);
				//case# 20148659
				nlapiLogExecution('ERROR', 'Before Submitting inventory LotBatch', LotBatch);
				if(LotBatch!=null&&LotBatch!="")
					invtRec.setFieldValue('custrecord_ebiz_inv_lot', LotBatch);
				if(vTaskType!=null&&vTaskType!="")
					invtRec.setFieldValue('custrecord_invttasktype', vTaskType);
				//invtRec.setFieldValue('custrecord_ebiz_inv_adjusttype', vAdjType);
				if(Invcompany!=null&&Invcompany!="")
					invtRec.setFieldValue('custrecord_ebiz_inv_company', Invcompany);
				if(Invfifodate !=null && Invfifodate !='')
					invtRec.setFieldValue('custrecord_ebiz_inv_fifo', Invfifodate);
				invtRec.setFieldValue('custrecord_ebiz_itemdesc', Invitemdesc);
				if(vOrderNo!=null&&vOrderNo!="")
					invtRec.setFieldValue('custrecord_ebiz_transaction_no', vOrderNo);
				if(expdate !=null && expdate !='')
					invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);	

				nlapiLogExecution('ERROR', 'ItemNewLP ',ItemNewLP);

				nlapiLogExecution('ERROR', 'Before Submitting inventory recid', 'Inventory Records');
				var invtrecid = nlapiSubmitRecord(invtRec, false, true);
				nlapiLogExecution('ERROR','created invt record ',invtrecid);
				//var invtrecid = nlapiSubmitRecord(invtRec,true);
			}
		}
		else
		{

			if(ItemNewLocId==null || ItemNewLocId=='')
				ItemNewLocId=ItemLocId;

			//Create Trn_inventory Record.
			var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');
			//var vIntValue="N";
			nlapiLogExecution('ERROR', 'Creating Inventory Record', 'INVT');
			invtRec.setFieldValue('name', itemLocName);
			invtRec.setFieldValue('custrecord_ebiz_inv_binloc', ItemNewLocId);
			invtRec.setFieldValue('custrecord_ebiz_inv_lp', ItemNewLP);
			invtRec.setFieldValue('custrecord_ebiz_inv_sku', ItemId);
			invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(ItemNewQty).toFixed(5));
			invtRec.setFieldValue('custrecord_inv_ebizsku_no', ItemId);
			invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(ItemNewQty).toFixed(5));
			invtRec.setFieldValue('custrecord_ebiz_itemdesc', ItemName);
			//invtRec.setFieldValue('custrecord_invttasktype', 9);
			invtRec.setFieldValue('custrecord_ebiz_inv_loc', siteloc);
			invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountno);
			invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
			invtRec.setFieldValue('custrecord_ebiz_inv_packcode', packcode);
			//invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
			//invtRec.setFieldValue('custrecord_ebiz_callinv', vIntValue);
			invtRec.setFieldValue('custrecord_ebiz_callinv', CallFlag);
			invtRec.setFieldValue('custrecord_ebiz_displayfield','N');				
			invtRec.setFieldValue('custrecord_wms_inv_status_flag',19);		
			//invtRec.setFieldValue('custrecord_ebiz_inv_recorddate',(parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
			//invtRec.setFieldValue('custrecord_ebiz_inv_rectime',((curr_hour) + ":" + (curr_min) + " " + a_p));
			//invtRec.setFieldValue('custrecord_invt_ebizlp',putLP);
			//invtRec.setFieldValue('custrecord_ebiz_uomlvl','1');
			nlapiLogExecution('ERROR', 'Before Submitting inventory LotBatch', LotBatch);
			invtRec.setFieldValue('custrecord_ebiz_inv_lot', LotBatch);
			invtRec.setFieldValue('custrecord_invttasktype', vTaskType);
			//invtRec.setFieldValue('custrecord_ebiz_inv_adjusttype', vAdjType);
			invtRec.setFieldValue('custrecord_ebiz_inv_company', Invcompany);
			if(Invfifodate !=null && Invfifodate !='')
				invtRec.setFieldValue('custrecord_ebiz_inv_fifo', Invfifodate);
			invtRec.setFieldValue('custrecord_ebiz_itemdesc', Invitemdesc);
			if(expdate !=null && expdate !='')
				invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);	


			nlapiLogExecution('ERROR', 'Before Submitting inventory recid', 'Inventory Records');
			var invtrecid = nlapiSubmitRecord(invtRec, false, true);
			nlapiLogExecution('ERROR','created invt record2 ',invtrecid);
			//var invtrecid = nlapiSubmitRecord(invtRec,true);
		}
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in create inventory',exp);
	}
}
function GetStockAdjustTypes(AdjType)
{
	var filtersso = new Array();		
	filtersso[0] = new nlobjSearchFilter( 'custrecord_ebiz_adjtype', null, 'anyOf', AdjType );

	var columnsinvt = new Array();
	columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_tasktype');
	columnsinvt[0].setSort();

	//columnsinvt[2] = new nlobjSearchColumn('custrecord_route_no');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_invadj', null, filtersso,columnsinvt);
	return searchresults;
}

function GetNSCallFlag(Status,NewStatus)
{
	nlapiLogExecution('ERROR', 'Into GetNSCallFlag', '');
	nlapiLogExecution('ERROR', 'Status', Status);
	nlapiLogExecution('ERROR', 'New Status', NewStatus);
	var result=new Array();
	var retFlag='N';
	var OldMapLocation;
	var NewMapLocation;
	if(Status!=null && Status!="")
	{
		var OldIStatus = nlapiLoadRecord('customrecord_ebiznet_sku_status', Status);
		if(OldIStatus!= null && OldIStatus != "")
		{
			OldMapLocation=OldIStatus.getFieldValue('custrecord_item_status_location_map');
			OldIStatus=null;
		} 
	}
	if(NewStatus!=null && NewStatus!="")
	{
		var NewIStatus = nlapiLoadRecord('customrecord_ebiznet_sku_status', NewStatus);
		if(NewIStatus!= null && NewIStatus != "")
		{
			NewMapLocation=NewIStatus.getFieldValue('custrecord_item_status_location_map');
		} 
	}
	if(OldMapLocation != null && OldMapLocation !="" && NewMapLocation != null && NewMapLocation != "")
	{
		if(OldMapLocation==NewMapLocation)
		{
			retFlag='N';
			result.push('N');
		}
		else
		{
			result.push('Y');
		}
		result.push(OldMapLocation);
		result.push(NewMapLocation);
	}
	nlapiLogExecution('ERROR', 'retFlag', retFlag);
	nlapiLogExecution('ERROR', 'OldMapLocation', OldMapLocation);
	nlapiLogExecution('ERROR', 'NewMapLocation', NewMapLocation);
	nlapiLogExecution('ERROR', 'Out of GetNSCallFlag', '');
	return result;
}


function GetMWFlag(WHLocation)
{
	nlapiLogExecution('ERROR', 'Into GetMWFlag : WHLocation', WHLocation);
	var retFlag='Y';
	var MWHSiteFlag;

	if(WHLocation!=null && WHLocation!="")
	{
		var filters = new Array();
		filters.push(new nlobjSearchFilter('internalid', null, 'is', WHLocation));
		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_ebizwhsite')); 

		var WHOld = nlapiSearchRecord('location', null, filters, columns);

		if(WHOld!= null && WHOld != "" && WHOld.length>0)
		{
			MWHSiteFlag=WHOld[0].getValue('custrecord_ebizwhsite');
			nlapiLogExecution('ERROR', 'MWHSiteFlag', MWHSiteFlag);
			WHOld=null;
		} 
	} 
	if(MWHSiteFlag != null && MWHSiteFlag !="" )
	{
		if(MWHSiteFlag=='F')
			retFlag='N';
		else
			retFlag='Y';

	}
	nlapiLogExecution('ERROR', 'retFlag', retFlag);
	nlapiLogExecution('ERROR', 'Out of GetMWFlag ', '');
	return retFlag;
}
function getStockAdjustmentAccountNoNew(adjusttype)
{	
	var StAdjustmentAccountNo=new Array();
	var filterStAccNo = new Array();
	var colsStAccNo = new Array();



	if(adjusttype!=null && adjusttype!="")
		filterStAccNo.push(new nlobjSearchFilter('internalid',null,'is',adjusttype));

	filterStAccNo.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	colsStAccNo[0] = new nlobjSearchColumn('custrecord_ebiz_mapto_ns_glaccount');
	colsStAccNo[1] = new nlobjSearchColumn('custrecord_ebiz_to_ns_glaccount');

	var StockAdjustAccResults = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, filterStAccNo, colsStAccNo);
	if(StockAdjustAccResults!=null && StockAdjustAccResults != "" && StockAdjustAccResults.length>0)
	{
		StAdjustmentAccountNo.push(StockAdjustAccResults[0].getValue('custrecord_ebiz_mapto_ns_glaccount'));
		StAdjustmentAccountNo.push(StockAdjustAccResults[0].getValue('custrecord_ebiz_to_ns_glaccount'));
	}

	return StAdjustmentAccountNo;	
}

/**
 * Passing item,item status, qty and location to NS
 * 
 * @param item
 * @param itemstatus
 * @param loc
 * @param qty
 * @param notes
 */
function  InvokeNSInventoryAdjustmentNew(item,itemstatus,loc,qty,notes,tasktype,adjusttype,lot,AccNo)
{
	try{
		nlapiLogExecution('ERROR', 'Location info::', loc);
		nlapiLogExecution('ERROR', 'Task Type::', tasktype);
		nlapiLogExecution('ERROR', 'Adjustment Type::', adjusttype);
		nlapiLogExecution('ERROR', 'qty::', qty);
		nlapiLogExecution('ERROR', 'AccNo::', AccNo);
		nlapiLogExecution('ERROR', 'lot::', lot);
		var confirmLotToNS='N';
		var vItemname;
		confirmLotToNS=GetConfirmLotToNS(loc);

		var filters = new Array();          
		filters.push(new nlobjSearchFilter('internalid', null, 'is',item));
		// Changed On 30/4/12 by Suman
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		// End of Changes as On 30/4/12
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('cost');
		columns[1] = new nlobjSearchColumn('averagecost');
		columns[2] = new nlobjSearchColumn('itemid');

		var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
		if (itemdetails !=null) 
		{
			vItemname=itemdetails[0].getValue('itemid');
			vCost = itemdetails[0].getValue('cost');
			nlapiLogExecution('ERROR', 'vCost', vCost);
			vAvgCost = itemdetails[0].getValue('averagecost');
			nlapiLogExecution('ERROR', 'Average Cost', vAvgCost);	         
		}		

		var outAdj = nlapiCreateRecord('inventoryadjustment');			
		outAdj.setFieldValue('account', AccNo);
		outAdj.setFieldValue('memo', notes);
		outAdj.setFieldValue('adjlocation', loc);
		outAdj.insertLineItem('inventory', 1);
		outAdj.setLineItemValue('inventory', 'item', 1, item);
		outAdj.setLineItemValue('inventory', 'location', 1, loc);	
		outAdj.setLineItemValue('inventory', 'adjustqtyby', 1, parseFloat(qty));
		if(vAvgCost != null &&  vAvgCost != "")	
		{
			nlapiLogExecution('ERROR', 'into if cond vAvgCost', vAvgCost);
			outAdj.setLineItemValue('inventory', 'unitcost', 1, vAvgCost);
		}
		else
		{
			nlapiLogExecution('ERROR', 'into else cond.unit cost', vCost);
			outAdj.setLineItemValue('inventory', 'unitcost', 1, vCost);
		}
		//outAdj.setFieldValue('subsidiary', 4);

		//Added by Sudheer on 093011 to send lot# for inventory posting

		if(lot!=null && lot!='')
		{
			//vItemname=vItemname.replace(" ","-");
			vItemname=vItemname.replace(/ /g,"-");
			if(parseFloat(qty)<0)
				qty=parseFloat(qty)*(-1);
			if(confirmLotToNS=='N')
				lot=vItemname;		

			nlapiLogExecution('ERROR', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");
			outAdj.setLineItemValue('inventory', 'serialnumbers', 1, lot + "(" + parseFloat(qty) + ")");
		}

		nlapiSubmitRecord(outAdj, true, true);
		nlapiLogExecution('ERROR', 'type argument', 'type is create');
	}


	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in InvokeNSInventoryAdjustmentNew ', exp);	

	}
}


function  InvokeNSInventoryTransfer(item,itemstatus,loc,toloc,qty,lot)
{
	try{
		nlapiLogExecution('ERROR', 'Location info::', loc);		
		nlapiLogExecution('ERROR', 'qty::', qty);		
		nlapiLogExecution('ERROR', 'lot::', lot);
		var confirmLotToNS='N';
		var vItemname;	

		confirmLotToNS=GetConfirmLotToNS(loc);
		if(item!= null && item != "")
		{
			var columns = nlapiLookupField('item', item, [ 'recordType','custitem_ebizserialout' ]);
			ItemType = columns.recordType;
			serialOutflg = columns.custitem_ebizserialout;
		}
		var filters = new Array();          
//		filters[0] = new nlobjSearchFilter('internalid', null, 'is',item);
		filters.push(new nlobjSearchFilter('internalid', null, 'is',item));
		// Changed On 30/4/12 by Suman
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		// End of Changes as On 30/4/12
		var columns = new Array();		
		columns[0] = new nlobjSearchColumn('itemid');
		var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
		if (itemdetails !=null) 
		{
			vItemname=itemdetails[0].getValue('itemid');
		}

		var invttransfer = nlapiCreateRecord('inventorytransfer');			
		var subs = nlapiGetContext().getFeature('subsidiaries');
		if(subs==true)
		{
			var vSubsidiaryVal=getSubsidiaryNew(loc);
			if(vSubsidiaryVal != null && vSubsidiaryVal != '')
				invttransfer.setFieldValue('subsidiary', vSubsidiaryVal);
		}
		invttransfer.setFieldValue('location', loc);//from Location
		invttransfer.setFieldValue('transferlocation', toloc);
		invttransfer.selectNewLineItem('inventory');
		invttransfer.setCurrentLineItemValue('inventory', 'item', item);		
		invttransfer.setCurrentLineItemValue('inventory', 'adjustqtyby', parseFloat(qty));


		if(lot!=null && lot!='')
		{
			//vItemname=vItemname.replace(" ","-");
			vItemname=vItemname.replace(/ /g,"-");
			if(parseFloat(qty)<0)
				qty=parseFloat(qty)*(-1);
			if(confirmLotToNS=='N')
				lot=vItemname;		
			
			//For advanced Bin serial Lot management check
			var vAdvBinManagement=false;
			var ctx = nlapiGetContext();
			if(ctx != null && ctx != '')
			{
				if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
					vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
			}  
			nlapiLogExecution('DEBUG', 'vAdvBinManagement', vAdvBinManagement);

			if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
			{
				// case no 20126516
				var templot = lot.split(',');

				nlapiLogExecution('ERROR', 'templot', templot.length);

				var compSubRecord = invttransfer.createCurrentLineItemSubrecord('inventory','inventorydetail');
				if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem") 
				{
					for(var t=0; t< templot.length; t++)
					{
						nlapiLogExecution('ERROR', 'test11', templot[t]);
						compSubRecord.selectNewLineItem('inventoryassignment');
						compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
						//compSubRecord.setCurrentLineItemValue('inventoryassignment', 'binnumber', vSubsidiaryVal);
						compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', templot[t]);

						compSubRecord.commitLineItem('inventoryassignment');
					}
				}
				else
				{
					nlapiLogExecution('ERROR', 'test1', 'test1');
					compSubRecord.selectNewLineItem('inventoryassignment');
					compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', qty);
					//compSubRecord.setCurrentLineItemValue('inventoryassignment', 'binnumber', vSubsidiaryVal);
					compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lot);

					compSubRecord.commitLineItem('inventoryassignment');
				}

				compSubRecord.commit();
			}
			else
			{
				if(parseInt(qty)<0)
					qty=parseInt(qty)*(-1);

				if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem") 
				{
					nlapiLogExecution('DEBUG', 'LotNowithQty', lot);
					invttransfer.setCurrentLineItemValue('inventory', 'serialnumbers',  lot);
				}	
				else
				{
					nlapiLogExecution('DEBUG', 'LotNowithQty', lot + "(" + parseInt(qty) + ")");
					invttransfer.setCurrentLineItemValue('inventory', 'serialnumbers', lot + "(" + parseInt(qty) + ")");
				}
			}

			
		}
		

		invttransfer.commitLineItem('inventory'); 
		nlapiSubmitRecord(invttransfer, true, true);
		nlapiLogExecution('ERROR', 'type argument', 'type is create');
	}


	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in InvokeNSInventoryTransferNew ', exp);	

	}
}

function GeteLocCube(LocID){
	var remainingCube = 0;
	try {
		var fields = ['custrecord_remainingcube'];
		var columns= nlapiLookupField('customrecord_ebiznet_location',LocID,fields);
		remainingCube = columns.custrecord_remainingcube;
	}catch(exception) {
		nlapiLogExecution('Error', 'GeteLocCube:Lookup for remainingCube failed', exception);
	}
	nlapiLogExecution('Error', 'GeteLocCube:Remaining Cube for Location', remainingCube);
	return remainingCube;
}
function UpdateLocCube(locnId, remainingCube){
	var fields = new Array();
	var values = new Array();
	fields[0] = 'custrecord_remainingcube';
	values[0] = parseFloat(remainingCube).toFixed(5);

	nlapiLogExecution('ERROR','Location Internal Id', locnId);

	var t1 = new Date();
	nlapiSubmitField('customrecord_ebiznet_location', locnId, fields, values);
	var t2 = new Date();
	nlapiLogExecution('ERROR', 'nlapiSubmitField:UpdateLocCube:location', getElapsedTimeDuration(t1, t2));
}
function getElapsedTimeDuration(timestamp1, timestamp2){
	return (parseFloat(timestamp2.getTime()) - parseFloat(timestamp1.getTime()));
}


function getMaxUOMQty(putItemId,putItemPC)
{
	var varPaltQty=0;
	var searchresults = new Array();

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', putItemId));
	if(putItemPC!=null && putItemPC!='')
		filters.push(new nlobjSearchFilter('custrecord_ebizpackcodeskudim', null, 'is', putItemPC));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizqty');
	columns[1] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim'); 	
	columns[1].setSort(true);

	searchresults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);
	if(searchresults != null && searchresults != '' && searchresults.length > 0)
	{
		varPaltQty = searchresults[0].getValue('custrecord_ebizqty');
	}

	return varPaltQty;
}

//case# 20148143 starts
function FillLocation(form,varLoc,invtlp,maxno,vRoleLocation)
{
	var filtersso = new Array();
	filtersso.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3,17, 19]));
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', vRoleLocation));

	}
	
	if(maxno!=-1)
	{
		filtersso.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno)));
	}

	var columnso = new Array();
	columnso.push(new nlobjSearchColumn('internalid').setSort(true));
	columnso.push(new nlobjSearchColumn('custrecord_ebiz_inv_loc').setSort());
	columnso.push(new nlobjSearchColumn('custrecord_ebiz_inv_lp'));
	
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersso,columnso);

	if (searchresults != null)
	{


		for (var i = 0; i < searchresults.length; i++) 
		{
			var res = form.getField('custpage_location').getSelectOptions(searchresults[i].getText('custrecord_ebiz_inv_loc'), 'is');
			if (res != null) 
			{
				if (res.length > 0)	                
					continue;	                
			}
			varLoc.addSelectOption(searchresults[i].getValue('custrecord_ebiz_inv_loc'), searchresults[i].getText('custrecord_ebiz_inv_loc'));
			
		}
		for (var i = 0; i < searchresults.length; i++) 
		{
			invtlp.addSelectOption(searchresults[i].getValue('custrecord_ebiz_inv_lp'), searchresults[i].getValue('custrecord_ebiz_inv_lp'));
		}
	}     



	if(searchresults!=null && searchresults.length>=1000)
	{
		var maxno=searchresults[searchresults.length-1].getValue('internalid');		
		FillLocation(form, varLoc,invtlp,maxno,vRoleLocation);	
	}

}

//case# 20148143 ends



//case# 20148655 starts
function AdjustSerialNumbers(localSerialNoArray,qty,item,lp,newlp,Continuemove,NewLoc,oldqty,NewWHSite)
{	
	nlapiLogExecution('DEBUG','AdjustSerialNumbers qty',qty);
	nlapiLogExecution('DEBUG','item',item);
	nlapiLogExecution('DEBUG','lp',lp);
	if(parseInt(qty) > 0)
	{
		if(Continuemove == "Y")
		{
			if(parseFloat(qty) == parseFloat(oldqty))
			{
				var filters1 = new Array();
				filters1[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
				filters1[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
				//filters1[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'D');

				var columns1 = new Array();
				columns1[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults1 = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters1, columns1);             

				if(searchResults1 != null && searchResults1 != "")
				{ 
					for (var i = 0; i < searchResults1.length; i++) 
					{   
						if(searchResults1[i].getValue('custrecord_serialnumber') != null && searchResults1[i].getValue('custrecord_serialnumber') != "")
						{
							var SerialNo1 = searchResults1[i].getValue('custrecord_serialnumber');
							var InternalID1 = searchResults1[i].getId();

							var currentRow1 = [SerialNo1];					
							localSerialNoArray.push(currentRow1);
							nlapiLogExecution('DEBUG', 'localSerialNoArray : ', localSerialNoArray.toString());

							var LoadSerialNumbers1 = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID1);
							LoadSerialNumbers1.setFieldValue('custrecord_serialparentid', newlp);
							LoadSerialNumbers1.setFieldValue('custrecord_serialbinlocation', NewLoc);
							if(NewWHSite != null && NewWHSite != "")
								LoadSerialNumbers1.setFieldValue('custrecord_serial_location', NewWHSite);
							LoadSerialNumbers1.setFieldValue('custrecord_serialstatus', '');

							var recid1 = nlapiSubmitRecord(LoadSerialNumbers1, false, true);
							nlapiLogExecution('DEBUG', 'Serial Entry rec id',recid1);
						}
					}
				}
			}
			else
			{
				var filters1 = new Array();
				filters1[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
				filters1[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
				filters1[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'D');

				var columns1 = new Array();
				columns1[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults1 = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters1, columns1);             

				if(searchResults1 != null && searchResults1 != "")
				{ 
					for (var i = 0; i < searchResults1.length; i++) 
					{   
						if(searchResults1[i].getValue('custrecord_serialnumber') != null && searchResults1[i].getValue('custrecord_serialnumber') != "")
						{
							var SerialNo1 = searchResults1[i].getValue('custrecord_serialnumber');
							var InternalID1 = searchResults1[i].getId();

							var currentRow1 = [SerialNo1];					
							localSerialNoArray.push(currentRow1);
							nlapiLogExecution('DEBUG', 'localSerialNoArray : ', localSerialNoArray.toString());

							var LoadSerialNumbers1 = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID1);
							LoadSerialNumbers1.setFieldValue('custrecord_serialparentid', newlp);
							LoadSerialNumbers1.setFieldValue('custrecord_serialbinlocation', NewLoc);
							LoadSerialNumbers1.setFieldValue('custrecord_serialstatus', '');
							if(NewWHSite != null && NewWHSite != "")
								LoadSerialNumbers1.setFieldValue('custrecord_serial_location', NewWHSite);

							var recid1 = nlapiSubmitRecord(LoadSerialNumbers1, false, true);
							nlapiLogExecution('DEBUG', 'Serial Entry rec id',recid1);
						}
					}
				}
			}
		}

		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
		filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
		//filters[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'isnot', 'D');

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
		var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

		if(searchResults != null && searchResults != "")
		{ 
			for (var i = 0; i < searchResults.length; i++) 
			{   
				if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
				{
					var SerialNo = searchResults[i].getValue('custrecord_serialnumber');
					var InternalID = searchResults[i].getId();

					var LoadSerialNumbers = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
					LoadSerialNumbers.setFieldValue('custrecord_serialstatus', '');

					var recid = nlapiSubmitRecord(LoadSerialNumbers, false, true);
					nlapiLogExecution('DEBUG', 'Serial Entry rec id',recid);
				}
			}
		}


	}
	return localSerialNoArray;
}

function getSerialNoCSV(arrayLP){
	var csv = "";
	nlapiLogExecution('DEBUG', 'arrayLP.length',arrayLP.length);
	if(arrayLP.length == 1)
	{
		csv = arrayLP[0][0];
	}
	else
	{
		for (var i = 0; i < arrayLP.length; i++) {
			if(i == arrayLP.length -1)
			{
				csv += arrayLP[i][0];
			}
			else
			{
				csv += arrayLP[i][0] + ",";
			}
		}
	}
	return csv;
}
//case# 20148655 ends
