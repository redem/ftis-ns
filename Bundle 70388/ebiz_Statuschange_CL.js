/***************************************************************************
                                                                 
                                                  eBizNET Solutions Inc 
****************************************************************************
*
*  $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inventory/Client/Attic/ebiz_Statuschange_CL.js,v $
*  $Revision: 1.1.2.2.4.1.2.8.2.2 $
*  $Date: 2014/07/31 15:53:12 $
*  $Author: sponnaganti $
*  $Name: t_eBN_2014_2_StdBundle_0_13 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_Statuschange_CL.js,v $
*  Revision 1.1.2.2.4.1.2.8.2.2  2014/07/31 15:53:12  sponnaganti
*  Case# 20149773
*  Stnd Bundle Issue fix
*
*  Revision 1.1.2.2.4.1.2.8.2.1  2014/07/28 14:54:31  grao
*  Case#: 20149702   New 2014.2 Compatibilty issue fixes
*
*  Revision 1.1.2.2.4.1.2.8  2014/06/16 06:48:52  spendyala
*  CASE201112/CR201113/LOG201121
*  Issue fixed related to case#20148851
*
*  Revision 1.1.2.2.4.1.2.7  2014/06/03 15:33:34  sponnaganti
*  case# 20148662 20148663
*  Stnd Bundle issue fix
*
*  Revision 1.1.2.2.4.1.2.6  2014/06/02 15:33:30  sponnaganti
*  case# 20148660 20148655
*
*  Revision 1.1.2.2.4.1.2.5  2014/04/28 15:54:03  skavuri
*  Case # 20148182 SB Issue Fixed
*
*  Revision 1.1.2.2.4.1.2.4  2013/10/29 15:22:00  grao
*  Issue fix related to the 20125389
*
*  Revision 1.1.2.2.4.1.2.3  2013/10/22 14:09:57  snimmakayala
*  GSUSA PROD ISSUE
*  Case# : 20125154
*  Item Status Change
*
*  Revision 1.1.2.2.4.1.2.2  2013/10/08 15:48:32  rmukkera
*  Case# 20124810
*
*  Revision 1.1.2.2.4.1.2.1  2013/09/11 15:23:51  rmukkera
*  Case# 20124376
*
*
****************************************************************************/
function Fieldchange(type,name)
{

	//nlapiSetFieldValue('custpage_tempflag','display');

	if(name=='custpage_selectpage'||name=='custpage_pagesize')
	{
		NLDoMainFormButtonAction("submitter",true);
	}
	else
	{

		return true;
	}




}

function Display()
{
	nlapiSetFieldValue('custpage_tempflag','Display');
	NLDoMainFormButtonAction("submitter",true);
}

function OnSave(type,name)
{
	var ItemStatus = nlapiGetFieldValue('custpage_bulkitemstatus');
	var onGenerateClick=nlapiGetFieldValue('custpage_tempflag');
	var lineCnt = nlapiGetLineItemCount('custpage_invtmovelist');
	
	//var loc=nlapiGetFieldValue('custpage_location');
	var userselection = false;
	var vResult = "";
	for (var s = 1; s <= lineCnt; s++) 
	{
		var linecheck= nlapiGetLineItemValue('custpage_invtmovelist','custpage_invlocmove',s);

		if(linecheck=='T')
		{
			userselection = true;			
			var Linetemstatus = nlapiGetLineItemValue('custpage_invtmovelist','custpage_itemnewstatus',s);
			var oldLinetemstatus = nlapiGetLineItemValue('custpage_invtmovelist','custpage_itemstatus',s);
			if((ItemStatus == null || ItemStatus =='') && (onGenerateClick!='Display'))
			{
				if(Linetemstatus == null || Linetemstatus =='')
				{
					alert('Please Select Item Status');
					nlapiSetLineItemValue('custpage_invtmovelist','custpage_invlocmove',s,'F');
					return false;
				}

			}
			// Case# 20148182 starts
			var avialableQty = nlapiGetLineItemValue('custpage_invtmovelist','custpage_availqty',s);
			var newQuantity = nlapiGetLineItemValue('custpage_invtmovelist','custpage_itemnewqty',s);
			var oldQuantity=nlapiGetLineItemValue('custpage_invtmovelist','custpage_oldqty',s);
			var oldlp=nlapiGetLineItemValue('custpage_invtmovelist','custpage_lp',s);
			var newlp=nlapiGetLineItemValue('custpage_invtmovelist','custpage_itemnewlp',s);
			var oldbinloc=nlapiGetLineItemValue('custpage_invtmovelist','custpage_locid',s);
			var newbinloc=nlapiGetLineItemValue('custpage_invtmovelist','custpage_itemnewloc',s);
			//newQuantity=parseInt(newQuantity);
			//avialableQty=parseInt(avialableQty);
			//alert('newQuantity '+newQuantity);
			//alert('avialableQty '+avialableQty);
			/*if(newQuantity == null || newQuantity =='' || newQuantity=='NaN')
			{
				alert('Please Enter New Quantity');
				nlapiSetLineItemValue('custpage_invtmovelist','custpage_invlocmove',s,'F');
				return false;
			}*/
			
			if(isNaN(newQuantity) == true)
			{
				alert('Please enter the new quantity in number format');	
				nlapiSetLineItemValue('custpage_invtmovelist','custpage_invlocmove',s,'F');
				return false;
			}
			else if(newQuantity < 0)
			{
				alert('New Quantity cannot be less than zero');	
				nlapiSetLineItemValue('custpage_invtmovelist','custpage_invlocmove',s,'F');
				return false;
			}
			else if(newQuantity == 0)
			{
				alert('New Quantity cannot be 0');
				nlapiSetLineItemValue('custpage_invtmovelist','custpage_invlocmove',s,'F');
				return false;
			}
			
			
			
			if( parseFloat(newQuantity) > avialableQty)
			{
				alert('New Quantity must not be Greater than Avialable Qty ');
				nlapiSetLineItemValue('custpage_invtmovelist','custpage_invlocmove',s,'F');
				return false;
			}
			//Case# 20148182 ends
			
			
			
			
			
			//Case Start 20125389�  LP Validation
			var lpno = nlapiGetLineItemValue('custpage_invtmovelist', 'custpage_itemnewlp',s);
			var loc=nlapiGetLineItemValue('custpage_invtmovelist','custpage_siteloc',s);
			//alert('ItemNewLP:' + ItemNewLP);
			/*if(lpno == null || lpno =='')
			{
				alert('Please Enter LP');
				nlapiSetLineItemValue('custpage_invtmovelist','custpage_invlocmove',s,'F');
				return false;
			}*/
			var vargetlpno = lpno;
			/*var filtersmlp = new Array();
			filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', ItemNewLP);

			var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);
			var lpExists = 'N';
			if (SrchRecord != null && SrchRecord.length > 0) 
			{
				//alert('LP FOUND:');		
					lpExists = 'Y';		
			}
			if(lpExists == 'Y')
			{
				alert('LP Already Exists!!!');
				return false;					
			}*/
			//end
			
			//case# 20148660 starts
			
			//alert('lpno '+lpno);
			//alert('loc '+loc);
			if(vargetlpno.length>0)
			{
				var column=new Array();
				column[0]=new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix');
				column[1]=new nlobjSearchColumn('custrecord_ebiznet_lprange_begin');
				column[2]=new nlobjSearchColumn('custrecord_ebiznet_lprange_end');
				column[3]=new nlobjSearchColumn('custrecord_ebiznet_lprange_lpgentype');
				column[4]=new nlobjSearchColumn('custrecord_ebiznet_lprange_lptype');
				column[5]=new nlobjSearchColumn('custrecord_ebiznet_lprange_site');





				var lpRangefilters = new Array();
				lpRangefilters[0] = new nlobjSearchFilter('custrecord_ebiznet_lprange_site', null, 'anyof', loc);

				var searchresults = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, lpRangefilters, column);

				if (searchresults) {
					for (var i = 0; i < Math.min(50, searchresults.length); i++) {
						try {
							var getLPPrefix = searchresults[i].getValue('custrecord_ebiznet_lprange_lpprefix');


							var recid = searchresults[i].getId();


							var varBeginLPRange = searchresults[i].getValue('custrecord_ebiznet_lprange_begin');

							var varEndRange =searchresults[i].getValue('custrecord_ebiznet_lprange_end');

							var getLPGenerationTypeValue = searchresults[i].getValue('custrecord_ebiznet_lprange_lpgentype');

							var getLPTypeValue = searchresults[i].getValue('custrecord_ebiznet_lprange_lptype');

							var lploc;




							if (getLPTypeValue == "1" && getLPGenerationTypeValue == "2") {
								var getLPrefix = (searchresults[i].getValue('custrecord_ebiznet_lprange_lpprefix')).toUpperCase();


								if(getLPrefix == null || getLPrefix == '')
									getLPrefix=0;

								if(getLPrefix != null && getLPrefix != '' && getLPrefix != 0)
								{
									var LPprefixlen = getLPrefix.length;
									var vLPLen = vargetlpno.substring(0, LPprefixlen).toUpperCase();
									//alert('vLPLen '+vLPLen);
									if(vLPLen==lpno)
									{
										alert('PLs give no along with LpPrefix');
										nlapiSetLineItemValue('custpage_invtmovelist','custpage_invlocmove',s,'F');
										return false;
									}
								}
								else
								{
									var LPprefixlen = 0;
									var vLPLen = 0;
								}


								var columnlpprefix=new Array();

								columnlpprefix[0]=new nlobjSearchColumn('custrecord_ebiznet_lprange_site');

								var lpprefixfilters = new Array();
								lpprefixfilters[0] = new nlobjSearchFilter('custrecord_ebiznet_lprange_lpprefix', null, 'is', getLPrefix);

								var searchresultsprefixloc = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, lpprefixfilters, columnlpprefix);
								if(searchresultsprefixloc!=null && searchresultsprefixloc!='')
								{
									lploc=searchresults[0].getValue('custrecord_ebiznet_lprange_site');
								}


								//alert('lploc '+lploc);
								if(lploc!=loc)
								{
									vResult = "N";
									break;
								}

								if (vLPLen == getLPrefix) {

									var varnum = vargetlpno.substring(LPprefixlen, vargetlpno.length);

									if (varnum.length > varEndRange.length) {

										vResult = "N";
										break;
									}


									if ((parseInt(varnum,10) < parseInt(varBeginLPRange)) || (parseInt(varnum,10) > parseInt(varEndRange)) || (isNaN(varnum))) {

										vResult = "N";
										break;
									}
									else {
										vResult = "Y";
										break;
									}
								}
								else {
									vResult = "N";
								}

							} //end of if statement
							else {
								//alert("in else");
							}

						} 
						catch (err) {
							// alert("exception" + err + "value is ");

						}
					} //end of for loop
				}



				if (vResult == "Y") {

					var filters1 = new Array();				
					//filters1[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lpno);
					filters1[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lpno);

					var columns1 = new Array();
					columns1[0] = new nlobjSearchColumn('name');

					var searchresults1 = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters1,columns1);		
					if(searchresults1!=null &&  searchresults1.length>0)
					{
						alert('LP Already Exists');
						nlapiSetLineItemValue('custpage_invtmovelist','custpage_invlocmove',s,'F');
						return false;
					}
				}
				else
				{
					alert('INVALID LP');
					nlapiSetLineItemValue('custpage_invtmovelist','custpage_invlocmove',s,'F');
					return false;
				}
			}	//case# 20148660 ends
			
			//case# 20148655 starts
			
			
			if(oldbinloc == newbinloc)
			{	
				var result=confirm('Are you sure you want to move the inventory to the same bin location?');
				if(result==false)
				{
					nlapiSetLineItemValue('custpage_invtmovelist','custpage_invlocmove',s,'F');
					return false;
				}

			}		

			else if(parseFloat(oldQuantity) > parseFloat(newQuantity))
			{ 
				if(newlp==null || newlp=='')
					newlp=oldlp;
				if(oldlp == newlp)
				{

					alert('Partial quantity cannot be moved to different location with the same LP');
					nlapiSetLineItemValue('custpage_invtmovelist','custpage_invlocmove',s,'F');
					return false;
				}
			}
			
			
			if(parseFloat(oldQuantity) > parseFloat(newQuantity))
			{
				if(oldLinetemstatus!=Linetemstatus)
				{
					if(newlp==null || newlp=='')
						newlp=oldlp;
					if(oldlp==newlp)
					{
						alert('Partial quantity cannot be moved to same LP');
						nlapiSetLineItemValue('custpage_invtmovelist','custpage_invlocmove',s,'F');
						return false;
					}
				}

			}
			
			
			var itmid = nlapiGetCurrentLineItemValue('custpage_invtmovelist', 'custpage_itemid');
			var itemname = nlapiGetCurrentLineItemValue('custpage_invtmovelist', 'custpage_itemname');
			var LP = ""; 

			var fields = ['recordType', 'custitem_ebizserialin'];
			var columns = nlapiLookupField('item', itmid, fields);
			var ItemType = columns.recordType;	

			var serialInflg="F";		
			serialInflg = columns.custitem_ebizserialin;		

			if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") {			
				/*if(newlp.length > 0)
				{
					LP = newlp;
				}
				else
				{
					LP = oldlp;

				}*/
				
				LP = oldlp;
				//Case # 20126344� Start,20126654,20126999
				if(parseFloat(newQuantity) != 0 && parseFloat(newQuantity)<parseFloat(avialableQty))
				{
					var filterSerialEntry=new Array();
					filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialiteminternalid',null,'anyof',itmid));
					filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialparentid',null,'is',LP));
					filterSerialEntry.push(new nlobjSearchFilter('custrecord_serialstatus',null,'is','D'));
					var SearchRec=nlapiSearchRecord('customrecord_ebiznetserialentry',null,filterSerialEntry,null);
					if(SearchRec!=null && SearchRec!='')
					{

					}
					//alert('SearchRec.length' +SearchRec.length)
					else
					{
						alert("Please select required Serial numbers to move");



						var linkURL = nlapiResolveURL('SUITELET', 'customscript_serialnumber_adjustment', 'customdeploy_serialnumber_adjustment_di');
						/*var ctx = nlapiGetContext();
					if (ctx.getEnvironment() == 'PRODUCTION') {
						linkURL = 'https://system.netsuite.com' + linkURL;			
					}
					else 
						if (ctx.getEnvironment() == 'SANDBOX') {
							linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
						}*/
						//linkURL = linkURL + '&custparam_locationid=' + nlapiGetLineItemValue('custpage_invtmovelist','custpage_polocationid',p);
						//linkURL = linkURL + '&custparam_companyid=' + nlapiGetLineItemValue('custpage_invtmovelist','custpage_pocompanyid',p);
						linkURL = linkURL + '&custparam_serialskuid=' + itmid;
						linkURL = linkURL + '&custparam_serialsku=' + itemname;
						linkURL = linkURL + '&custparam_serialskulp=' + LP;
						linkURL = linkURL + '&custparam_serialskuqty=' + newQuantity;	
						//linkURL = linkURL + '&custparam_lot=' + BatchNo;
						//linkURL = linkURL + '&custparam_name=' + BatchNo;
						//linkURL = linkURL + '&custparam_itemstatus=' + nlapiGetLineItemValue('custpage_invtmovelist','custpage_poitemstatus',p);
						//linkURL = linkURL + '&custparam_packcode=' + nlapiGetLineItemValue('custpage_invtmovelist','custpage_popackcode',p);
						window.open(linkURL);
						return false;
					}
				}
				
			}
			
			
			
			
		}

	}
	//case# 20149773 starts(adding (onGenerateClick!='Display') to if condition)
	//if(userselection == false)
	if((userselection == false) && (onGenerateClick!='Display'))
	{
		alert('Please select atleast one row');
		return false;
	}
	return true;
}


function OnChange(type,name,linenum)
{
	try
	{
		//alert(name);
		if(name=="custpage_itemnewstatus")
		{
			var NewItemStatus=nlapiGetCurrentLineItemValue("custpage_invtmovelist","custpage_itemnewstatus");
			//alert(NewItemStatus);
			var vItemStatus=new Array();
			var whloc=nlapiGetCurrentLineItemValue("custpage_invtmovelist","custpage_siteloc");
			//alert(whloc);
			var filtersStatus = new Array();
			filtersStatus.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			if(whloc !=null && whloc!='')
			{
				filtersStatus.push(new nlobjSearchFilter('custrecord_ebizsiteskus', null, 'anyof', whloc));
			}
			var columnsstatus = new Array();
			columnsstatus.push(new nlobjSearchColumn('internalid'));

			var searchStatus = nlapiSearchRecord('customrecord_ebiznet_sku_status',null, filtersStatus, columnsstatus);
			//alert(searchStatus);
			if(searchStatus!=null&&searchStatus!="")
			{
				for(var i=0;i<searchStatus.length;i++)
				{
					vItemStatus.push(searchStatus[i].getValue("internalid"));
				}
			}
			//alert(vItemStatus);
			if(vItemStatus!=null&&vItemStatus!="")
			{
				if(vItemStatus.indexOf(NewItemStatus)==-1)
				{	
					alert("You can't Select Item status of different Site/location");
					nlapiSetCurrentLineItemValue("custpage_invtmovelist","custpage_itemnewstatus","",false);
					return false;
				}
				else
					return true;
			}
		}
	}
	catch(exp)
	{
		alert(exp);
		return false;
	}
}