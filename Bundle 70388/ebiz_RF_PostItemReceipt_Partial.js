/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************
 *
 * $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/Attic/ebiz_RF_PostItemReceipt_Partial.js,v $
 * $Revision: 1.1.2.20.2.3 $
 * $Date: 2015/11/19 10:18:25 $
 * $Author: gkalla $
 * $Name: t_WMS_2015_2_StdBundle_1_170 $
 *
 * DESCRIPTION
 * Functionality
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_PostItemReceipt_Partial.js,v $
 * Revision 1.1.2.20.2.3  2015/11/19 10:18:25  gkalla
 * case# 201415764
 * If serial quantity is in two records then at the time of assign second serial number to inventory detail then no need to create a new subrecord.
 *
 * Revision 1.1.2.20.2.2  2015/11/18 11:52:28  gkalla
 * case# 201415748
 * Added wms status flag filter with putaway completed, check in.
 *
 * Revision 1.1.2.20.2.1  2015/11/07 12:24:01  grao
 * 2015.2 Issue Fixes 201415361
 *
 * Revision 1.1.2.20  2015/08/17 10:56:26  skreddy
 * Case# 201414022
 * Bedgear SB issue fix
 *
 * Revision 1.1.2.19  2015/08/13 15:35:44  skreddy
 * Case# 201413992,201413993
 * Bedgear SB issue fix
 *
 * Revision 1.1.2.18  2015/07/28 15:29:16  skreddy
 * Case# 201413628
 * Sighwarehouse SB issue fix
 *
 * Revision 1.1.2.17  2015/07/23 15:50:23  nneelam
 * case# 201413493
 *
 * Revision 1.1.2.16  2015/07/23 15:30:34  grao
 * 2015.2   issue fixes  201412877
 *
 * Revision 1.1.2.15  2015/07/16 15:34:18  skreddy
 * Case# 201413492
 * Briggs SB1 issue fix
 *
 * Revision 1.1.2.14  2015/02/23 17:51:34  rrpulicherla
 * Case#201411317
 *
 * Revision 1.1.2.13  2015/02/18 14:26:02  rrpulicherla
 * Case#201411317
 *
 * Revision 1.1.2.12  2014/09/18 13:21:35  sponnaganti
 * Case# 201410421
 * Stnd Bundle Issue fix
 *
 * Revision 1.1.2.11  2014/08/25 06:38:55  skreddy
 * case # 20148852
 * post item receipt by scheduler
 *
 * Revision 1.1.2.10  2014/07/08 15:54:16  sponnaganti
 * case# 20149324
 * Compatibility issue fix
 *
 * Revision 1.1.2.9  2014/07/07 16:08:36  sponnaganti
 * case# 20149312
 * Comaptibility Issue fix
 *
 * Revision 1.1.2.8  2014/06/24 06:06:10  skreddy
 * case # 20148971
 * Sportshq prod  issue fix
 *
 * Revision 1.1.2.7  2014/06/23 06:53:45  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox) Issue Fixed.
 *
 * Revision 1.1.2.6  2014/06/12 13:42:01  skreddy
 * case # 20148827
 * MHP Prod issue fix
 *
 * Revision 1.1.2.5  2014/05/30 00:26:49  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.4  2014/02/13 15:14:03  rmukkera
 * Case # 20127042,20127117
 *
 * Revision 1.1.2.3  2013/11/12 06:39:39  skreddy
 * Case# 20125518 & 20125608
 * Afosa SB issue fix
 *
 * Revision 1.1.2.2  2013/11/08 14:35:40  schepuri
 * 20125520
 *
 * Revision 1.1.2.1  2013/07/25 09:00:07  rmukkera
 * landed cost cr new file
 *
 * Revision 1.1.2.5.4.7.4.2  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.5.4.7.4.1  2013/03/19 11:47:21  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.1.2.5.4.7  2013/01/02 15:42:57  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Double Click restriction.
 * .
 *
 * Revision 1.1.2.5.4.6  2012/12/31 05:52:23  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Restrict Packing for Closed and Paymeny Hold Orders.
 *
 * Revision 1.1.2.5  2012/07/30 13:43:00  svanama
 * CASE201112/CR201113/LOG201
 * CodeChanges in printPackList
 *
 * Revision 1.1.2.4  2012/07/19 14:16:29  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to PackageCount was resolved.
 *
 * Revision 1.1.2.3  2012/07/17 15:34:12  spendyala
 * CASE201112/CR201113/LOG201121
 * While creating shipmanifest record updating Packageno and totalpackage count.
 *
 * Revision 1.1.2.2  2012/07/11 08:26:41  spendyala
 * CASE201112/CR201113/LOG201121
 * While creating shipmanifest record updating Packageno and totalpackage count.
 *
 * Revision 1.1.2.1  2012/05/21 13:05:48  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Auto Packing
 *
 * Revision 1.2  2012/05/08 16:18:42  svanama
 * CASE201112/CR201113/LOG201121
 * packcode changes or added
 *
 * Revision 1.1  2012/04/24 13:45:19  svanama
 * CASE201112/CR201113/LOG201121
 * new pack code file added
 *
 *
 ****************************************************************************/

function postItemReceiptPartial(request, response)
{
	if (request.getMethod() == 'GET')
	{
		//case 20125518 Start :spanish Conversion
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
		
		var st0,st1,st2,st3;

		if( getLanguage == 'es_ES' || getLanguage == 'es_AR' )
		{
			st0 = "";
			st1 = "Hay abiertas Tareas entrada en stock en contra de este PO, �A�n desea continuar?"; 
			st2 = "S�";
			st3 = "NO";
						

		}
		else
		{
			st0 = "";
			st1 = "There are Open Putaway Tasks against this PO, Do you still want to continue?"; 
			st2 = "YES";
			st3 = "NO";
						

		}
		//case 20125518  end
		var functionkeyHtml=getFunctionkeyScript('_rfpostitemreceiptpartially'); 
		var html = "<html><head><title>Post Item Receipt Partially</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//html = html + " document.getElementById('cmdYes').focus();";        
		html = html + "</script>";		
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rfpostitemreceiptpartially' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st1+"";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> <input name='cmdYes' id='cmdYes' type='submit' value='" + st2 + "' onclick='this.form.submit();this.disabled=true;this.form.cmdNo.disabled=true; return false'/>";
		html = html + "					 <input name='cmdNo' type='submit' value='"+st3+"'/>";
		
		html = html + "				</td>";
		html = html + "			</tr>";
		/*html = html + "			<tr><td align = 'left'>";
		  html = html + "		PREV";
        html = html + "		<input name='cmdPrevious' type='submit' value='F7' /> ";
		html = html + "				</td>";
		html = html + "			</tr>";*/
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('cmdYes').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";
		response.write(html);
	}
	else
	{
		var optedEvent1 = request.getParameter('cmdNo');
		nlapiLogExecution('ERROR', 'optedEvent', optedEvent1);
		var POarray=new Array();
		var trantype='';
		var poid=request.getParameter('custparam_poid');
		POarray["custparam_poid"] = poid;
		var po=request.getParameter('custparam_po');
		var poToLocationID = request.getParameter("custparam_locationId");
		POarray["custparam_po"] = po;
		if(poid!=null&&poid!="")
			trantype = nlapiLookupField('transaction', poid, 'recordType');
		var optedEvent = request.getParameter('cmdPrevious');
		//case 20125518 Start :spanish Conversion
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
		POarray["custparam_language"]=getLanguage;
		var templocation=request.getParameter('custparam_locationId');
		POarray["custparam_poid"] = templocation;
		nlapiLogExecution('DEBUG', 'templocation', templocation);
		
		var st0,st1,st2,st3,st4,st5,st6;

		if( getLanguage == 'es_ES' || getLanguage == 'es_AR' )
		{
			st0 = "";
			st1 = "Confirmaci�n: Recibo Art�culo Publicado Sucessfully";
			st2 = "S�";
			st3 = "NO";
			st4 = "Primero complete las tareas putaway abiertas contra esta OC";
			st5 ="ANTERIOR";
			st6 =  "Confirmation:Item Receipt posting has been initiated";
						

		}
		else
		{
			st0 = "";
			st1 = "Confirmation:Item Receipt Posted Sucessfully"; 
			st2 = "YES";
			st3 = "NO";
			st4 ="First Complete Open Putaway Tasks against this PO";
			st5 ="PREV";
			st6 = "Confirmation:Item Receipt posting has been initiated successfully";
						

		}
		//case 20125518 end
		if (optedEvent !=null && optedEvent!='' && optedEvent == 'F7') {
			nlapiLogExecution('ERROR', 'optedEvent if', optedEvent);
			//case# 20149324 starts
			//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_postitemreceipt', 'customdeploy_ebiz_rf_postitemreceipt_di', false, POarray);
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_postitemreceipt', 'customdeploy_ebiz_rf_postitemreceipt_di', false, null);
			//case# 20149324 ends
			
		}
		else {

		if(optedEvent1=='NO')
		{
			
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_postitemreceipt', 'customdeploy_ebiz_rf_postitemreceipt_di', false, POarray);
		}
		else
		{

			var filters = new Array();
			filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
			filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty'));
			filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty')); 
			filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'isnotempty'));
			filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', poid));

			var columns=new Array();
			columns[0]=new nlobjSearchColumn('custrecord_sku',null,'group');
			columns[1]=new nlobjSearchColumn('custrecord_line_no',null,'group');
			columns[2]=new nlobjSearchColumn('formulanumeric',null,'SUM');
			columns[2].setFormula("TO_NUMBER({custrecord_act_qty})");
			//columns[3]=new nlobjSearchColumn('custrecord_sku_status',null,'group');
			columns[3]=new nlobjSearchColumn('custrecord_wms_location',null,'group');
			columns[4]=new nlobjSearchColumn('custrecord_item_status_location_map','custrecord_sku_status','group');
			columns[5]=new nlobjSearchColumn('custrecord_sku_status',null,'group');

			var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,columns);
			nlapiLogExecution('ERROR','opentaskSearchResults',opentaskSearchResults);

			if(opentaskSearchResults!=null && opentaskSearchResults!='' && opentaskSearchResults.length>0)
			{
				nlapiLogExecution('ERROR','opentaskSearchResults.length',opentaskSearchResults.length);
				if(parseInt(opentaskSearchResults.length)>=300)
				{
					nlapiLogExecution('ERROR','invoke Scheduler','done');
					var param = new Array();
					param['custscript_poid'] = poid;
					param['custscript_trantype'] = trantype;
					param['custscript_locationid']=templocation
					nlapiScheduleScript('customscript_ebiz_postitem_receipt_sch', null,param);
					
					var functionkeyHtml=getFunctionkeyScript('_rf_postitemreceipt'); 
					var html = "<html><head><title>Post Item Receipt</title>";
					html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
					html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
					html = html + "nextPage = new String(history.forward());";          
					html = html + "if (nextPage == 'undefined')";     
					html = html + "{}";     
					html = html + "else";     
					html = html + "{  location.href = window.history.forward();"; 
					html = html + "} ";
					html = html + " document.getElementById('enterlocation').focus();";        
					html = html + "function stopRKey(evt) { ";
					//html = html + "	  alert('evt');";
					html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
					html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
					html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
					html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
					html = html + "	  alert('System Processing, Please wait...');";
					html = html + "	  return false;}} ";
					html = html + "	} ";

					html = html + "	document.onkeypress = stopRKey; ";
					html = html + "</script>";
					html = html +functionkeyHtml;
					html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
					html = html + "	<form name='_rf_putaway_lp' method='POST'>";
					html = html + "		<table>";

					html = html + "			<tr>";
					html = html + "				<td align = 'left'>"+st6+"";

					html = html + "				</td>";
					html = html + "			</tr>";
					html = html + "			<tr>";
					html = html + "				<td align = 'left'>";
					html = html + "		"+st5;
					html = html + "		<input name='cmdPrevious' type='submit' value='F7' /> ";
					html = html + "				</td>";
					html = html + "			</tr>";

					html = html + "		 </table>";
					html = html + "	</form>";
					html = html + "</body>";
					html = html + "</html>";

					response.write(html);

				}
				else
				{
					var ItemStatusArray =new Array();
					var itemReceiptArray =new Array();
					for(var k=0;k<opentaskSearchResults.length;k++)
					{
						var	itemstatus1=opentaskSearchResults[k].getValue('custrecord_sku_status',null,'group');
						nlapiLogExecution('ERROR','itemstatus1',itemstatus1);

						if(ItemStatusArray.indexOf(itemstatus1) == -1)
						{
							ItemStatusArray.push(itemstatus1);
						}
					}
					
					for(j=0;j<ItemStatusArray.length;j++)
					{
						var vItemStatus = ItemStatusArray[j];
					var trecord = nlapiTransformRecord(trantype, poid, 'itemreceipt');
					for(var i=0;i<opentaskSearchResults.length;i++)
					{
						var ActQuantity=opentaskSearchResults[i].getValue('formulanumeric',null,'SUM');
						var linenum=opentaskSearchResults[i].getValue('custrecord_line_no',null,'group');
						var	itemid=opentaskSearchResults[i].getValue('custrecord_sku',null,'group');
						var	itemstatus=opentaskSearchResults[i].getValue('custrecord_sku_status',null,'group');

					generateItemReceipt(trecord,trantype,poid,ActQuantity,linenum,itemstatus,itemid,trantype,poid,templocation);

					}
					if(trecord != null && trecord != '')
					{
						idl = nlapiSubmitRecord(trecord);
						var currentrow=[ItemStatusArray[j],idl];
						itemReceiptArray.push(currentrow);
						if(SerialArray !=null && SerialArray !='' && SerialArray.length>0)
						{
							UpdateSerialNumbersStatus(SerialArray);// updating serial numbers status 
							SerialArray.length=0; //clear the Serial# Array
						}
					}
					}
					nlapiLogExecution('ERROR','idl',idl);
					if(idl!=null && idl!='')
					{	
						if(itemReceiptArray !=null && itemReceiptArray !='' && itemReceiptArray.length>0)
						{
							nlapiLogExecution('ERROR','itemReceiptArray',itemReceiptArray);
							for (var cnt=0;cnt<itemReceiptArray.length ;cnt++)
							{
								var vItemstatus=itemReceiptArray[cnt][0];
								var IRid=itemReceiptArray[cnt][1];
								//nlapiLogExecution('ERROR','IRid',IRid);
								var SeachResults=OpentaskrecordsforItemStatus(poid,vItemstatus);

								for(var M=0;M<SeachResults.length;M++)
								{
									nlapiLogExecution('ERROR','SeachResults[M]',SeachResults[M]);
									var updateStatus = nlapiSubmitField('customrecord_ebiznet_trn_opentask', SeachResults[M].getId(), 'custrecord_ebiz_nsconfirm_ref_no', IRid);
								}

							}

						}
						var filters2 = new Array();
						filters2.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
						filters2.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty'));
						//filters2.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty')); 
						filters2.push(new nlobjSearchFilter('custrecord_actendloc', null, 'isnotempty'));
						filters2.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', poid));
						var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters2,null);
						
						//var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,null);
					if(opentaskSearchResults!=null && opentaskSearchResults!='' && opentaskSearchResults.length>0)
					{
						nlapiLogExecution('ERROR','opentaskSearchResults.length',opentaskSearchResults.length);
						for(var j=0;j<opentaskSearchResults.length;j++)
						{
							nlapiLogExecution('ERROR','opentaskSearchResultsID',opentaskSearchResults[j].getId());
							//MoveTaskRecord(opentaskSearchResults[j].getId(),idl);
							//case# 201410421 (updating Ns confirmation num in open task)
							nlapiSubmitField('customrecord_ebiznet_trn_opentask', opentaskSearchResults[j].getId(), 'custrecord_ebiz_nsconfirm_ref_no', idl);
							
						}
					}

					var functionkeyHtml=getFunctionkeyScript('_rf_postitemreceipt'); 
					var html = "<html><head><title>Post Item Receipt</title>";
					html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
					html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
					html = html + "nextPage = new String(history.forward());";          
					html = html + "if (nextPage == 'undefined')";     
					html = html + "{}";     
					html = html + "else";     
					html = html + "{  location.href = window.history.forward();"; 
					html = html + "} ";
					html = html + " document.getElementById('enterlocation').focus();";        
					html = html + "function stopRKey(evt) { ";
					//html = html + "	  alert('evt');";
					html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
					html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
					html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
					html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
					html = html + "	  alert('System Processing, Please wait...');";
					html = html + "	  return false;}} ";
					html = html + "	} ";

					html = html + "	document.onkeypress = stopRKey; ";
					html = html + "</script>";
					html = html +functionkeyHtml;
					html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
					html = html + "	<form name='_rf_postitemreceipt' method='POST'>";
					html = html + "		<table>";

					html = html + "			<tr>";
					html = html + "				<td align = 'left'>"+st1+"";

					html = html + "				</td>";
					html = html + "			</tr>";
					html = html + "			<tr>";
					html = html + "				<td align = 'left'>";
					html = html + "		"+st5;
					html = html + "		<input name='cmdPrevious' type='submit' value='F7' /> ";
					html = html + "				</td>";
					html = html + "			</tr>";

					html = html + "		 </table>";
					html = html + "	</form>";
					html = html + "</body>";
					html = html + "</html>";

					response.write(html);


					}
				}

			}
			else
			{
				//case 20125518 start : 

				var functionkeyHtml=getFunctionkeyScript('_rf_putaway_lp'); 
				var html = "<html><head><title>Post Item Receipt</title>";
				html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
				html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
				html = html + "nextPage = new String(history.forward());";          
				html = html + "if (nextPage == 'undefined')";     
				html = html + "{}";     
				html = html + "else";     
				html = html + "{  location.href = window.history.forward();"; 
				html = html + "} ";
				html = html + " document.getElementById('enterlocation').focus();";        
				html = html + "function stopRKey(evt) { ";
				//html = html + "	  alert('evt');";
				html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
				html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
				html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
				html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
				html = html + "	  alert('System Processing, Please wait...');";
				html = html + "	  return false;}} ";
				html = html + "	} ";

				html = html + "	document.onkeypress = stopRKey; ";
				html = html + "</script>";
				html = html +functionkeyHtml;
				html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
				html = html + "	<form name='_rf_putaway_lp' method='POST'>";
				html = html + "		<table>";

				html = html + "			<tr>";
				html = html + "				<td align = 'left'>"+st4+"";

				html = html + "				</td>";
				html = html + "			</tr>";
				html = html + "			<tr>";
				html = html + "				<td align = 'left'>";
				html = html + "		"+st5;
				html = html + "		<input name='cmdPrevious' type='submit' value='F7' /> ";
				html = html + "				</td>";
				html = html + "			</tr>";

				html = html + "		 </table>";
				html = html + "	</form>";
				html = html + "</body>";
				html = html + "</html>";

				response.write(html);
			}
			//case 20125518 end
		}
	   }
	}
}

var SerialArray=new Array();
function generateItemReceipt(trecord,fromrecord,fromid,ActQuantity,linenum,itemstatus,itemid,trantype,pointid,batchno,templocation)
{
	try {


		
		var polinelength = trecord.getLineItemCount('item');
		nlapiLogExecution('ERROR', "polinelength", polinelength);
		var idl;
		var vAdvBinManagement=true;

		var ctx = nlapiGetContext();
		if(ctx != null && ctx != '')
		{
			if(ctx.getFeature('advbinseriallotmgmt').toString() != null && ctx.getFeature('advbinseriallotmgmt').toString() != '')
				vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
			nlapiLogExecution('ERROR', 'vAdvBinManagement_if', vAdvBinManagement);
		}
		//}  
		nlapiLogExecution('ERROR', 'vAdvBinManagement', vAdvBinManagement);
		for (var j = 1; j <= polinelength; j++) {
			nlapiLogExecution('ERROR', "Get Item Id", trecord.getLineItemValue('item', 'item', j));
			nlapiLogExecution('ERROR', "Get Item Text", trecord.getLineItemText('item', 'item', j));

			var item_id = trecord.getLineItemValue('item', 'item', j);
			var itemrec = trecord.getLineItemValue('item', 'itemreceive', j);
			var itemLineNo = trecord.getLineItemValue('item', 'line', j);
			//var poItemUOM = trecord.getLineItemValue('item', 'unitsdisplay', j);//text
			var poItemUOM = trecord.getLineItemValue('item', 'units', j);//value

			var commitflag = 'N';
			var quantity=0;
			quantity = ActQuantity;

			nlapiLogExecution('ERROR', 'quantity', quantity);

			var inventorytransfer="";
			nlapiLogExecution('ERROR', "itemLineNo(PO/TO)", itemLineNo);
			nlapiLogExecution('ERROR', "linenum(Task)", linenum);
			nlapiLogExecution('ERROR', "poItemUOM", poItemUOM);

			if(trantype!='transferorder')
			{
				if (itemLineNo == linenum) {

					//This part of the code will fetch the line level location.
					//If line level is empty then header level location is considered.
					//code added by suman on 080513.
					whLocation=trecord.getLineItemValue('item', 'location', j);//value
					if(whLocation==null||whLocation=="")
						whLocation=templocation;
					//End of code as of 080513.

					//var vrcvqty=arrQty[i];
					var vrcvqty=quantity;
					if(poItemUOM!=null && poItemUOM!='')
					{
						var vbaseuomqty=0;
						var vuomqty=0;
						var eBizItemDims=geteBizItemDimensions(item_id);
						if(eBizItemDims!=null&&eBizItemDims.length>0)
						{
							nlapiLogExecution('ERROR', 'Dims Length', eBizItemDims.length);
							for(var z=0; z < eBizItemDims.length; z++)
							{
								if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
								{
									vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
								}
								nlapiLogExecution('ERROR', 'poItemUOM', poItemUOM);
								nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
								nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
								if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
								{
									vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
								}
							}
							if(vuomqty==null || vuomqty=='')
							{
								vuomqty=vbaseuomqty;
							}
							nlapiLogExecution('ERROR', 'vbaseuomqty', vbaseuomqty);
							nlapiLogExecution('ERROR', 'vuomqty', vuomqty);

							if(quantity==null || quantity=='' || isNaN(quantity))
								quantity=0;
							else
								quantity = (parseFloat(quantity)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

						}
					}
					//Code Added by Ramana
					var varItemlocation;
					nlapiLogExecution('ERROR', 'itemstatus', itemstatus);
					nlapiLogExecution('ERROR', 'quantity', quantity);
					varItemlocation = getItemStatusMapLoc(itemstatus);
					//upto to here

					commitflag = 'Y';

					var fields = ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot'];
					var columns = nlapiLookupField('item', itemid, fields);
					if(columns != null && columns != '')
					{
						Itype = columns.recordType;	


						serialInflg = columns.custitem_ebizserialin;
					}

					nlapiLogExecution('ERROR', 'Value of J', j);
					if (Itype != "serializedinventoryitem" || serialInflg != "T")
					{
						trecord.selectLineItem('item', j);
						trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
						trecord.setCurrentLineItemValue('item', 'quantity', parseFloat(quantity).toFixed(5));

						trecord.setCurrentLineItemValue('item', 'location', varItemlocation); //Added by ramana
					}

					nlapiLogExecution('ERROR', 'Serialquantity', parseFloat(quantity).toFixed(5));
					nlapiLogExecution('ERROR', 'Into SerialNos');
					var totalconfirmedQty=0;
					//if (Itype == "serializedinventoryitem" || serialInflg == "T") 
					
					if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem" )
					{
						nlapiLogExecution('ERROR', 'Into SerialNos');
						//nlapiLogExecution('ERROR', 'invtlp', invtlp);	
						nlapiLogExecution('ERROR', 'trantype', trantype);		
						var serialline = new Array();
						var serialId = new Array();
//case no 20125520
						var tempSerial = "";
						var tempserialId = "";
						var filters = new Array();


//						filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
//						filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
//						filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
//						filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);

						if(trantype=='returnauthorization')
						{
							filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['1','3','19']);
							filters[2] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);

						}
						else
						{
							filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['1','3','19']);
							filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
						//	filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
						}
						filters[3] = new nlobjSearchFilter('custrecord_serialstatus', null, 'doesnotstartwith', 'P');
						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
						var serialnumcsv = "";
						var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
						var serialnumArray=new Array();
						var tempQtyforinvDetail=0;
						if (serchRec) {
							if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
							{
								compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');

								tempQtyforinvDetail=1;

							}
							for (var n = 0; n < serchRec.length; n++) {
								//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
								if (tempserialId == "") {
									tempserialId = serchRec[n].getId();
									SerialArray.push(serchRec[n].getId());
								}
								else {
									tempserialId = tempserialId + "," + serchRec[n].getId();
									SerialArray.push(serchRec[n].getId());
								}

								//This is for Serial num loopin with Space separated.
								if (tempSerial == "") {
									tempSerial = serchRec[n].getValue('custrecord_serialnumber');

									serialnumArray.push(serchRec[n].getValue('custrecord_serialnumber'));

								}
								else {

									tempSerial = tempSerial + " " + serchRec[n].getValue('custrecord_serialnumber');
									if(tempSerial.length>3500)
									{
										tempSerial='';
										trecord.selectLineItem('item', j);
										trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
										var tempqty=serialnumArray.length;//parseInt(n-1)+parseInt(1);
										trecord.setCurrentLineItemValue('item', 'quantity', tempqty);

										trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
										trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumArray);
										nlapiLogExecution('ERROR', 'serialnumArray', serialnumArray);
										if(vAdvBinManagement)
										{
											compSubRecord.commit();
										}
										trecord.commitLineItem('item');
										idl = nlapiSubmitRecord(trecord);
										serialnumArray=new Array();
										totalconfirmedQty=parseInt(totalconfirmedQty)+parseInt(tempqty);
										if(n<serchRec.length)
										{
											var tQty=parseInt(quantity)-parseInt(totalconfirmedQty);
											nlapiLogExecution('ERROR', 'tQty1', tQty);
											var trecord = nlapiTransformRecord(fromrecord, fromid, 'itemreceipt');
											var polinelength = trecord.getLineItemCount('item');
											/*for (var j3 = 1; j3 <= polinelength; j3++) {*/
											var item_id = trecord.getLineItemValue('item', 'item', itemLineNo);
											var itemrec = trecord.getLineItemValue('item', 'itemreceive', itemLineNo);
											var itemLineNo = trecord.getLineItemValue('item', 'line', itemLineNo);
											var poItemUOM = trecord.getLineItemValue('item', 'unitsdisplay', itemLineNo);

											/*if (itemLineNo == linenum)
											{*/
											if(poItemUOM!=null && poItemUOM!='')
											{
												var vbaseuomqty=0;
												var vuomqty=0;
												var eBizItemDims=geteBizItemDimensions(item_id);
												if(eBizItemDims!=null&&eBizItemDims.length>0)
												{
													nlapiLogExecution('ERROR', 'Dims Length', eBizItemDims.length);
													for(var z=0; z < eBizItemDims.length; z++)
													{
														if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
														{
															vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
														}
														nlapiLogExecution('ERROR', 'poItemUOM', poItemUOM);
														nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
														nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
														if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
														{
															vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
														}
													}
													if(vuomqty==null || vuomqty=='')
													{
														vuomqty=vbaseuomqty;
													}
													nlapiLogExecution('ERROR', 'vbaseuomqty', vbaseuomqty);
													nlapiLogExecution('ERROR', 'vuomqty', vuomqty);

													if(tQty==null || tQty=='' || isNaN(tQty))
														tQty=0;
													else
														tQty = (parseFloat(tQty)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

												}
											}
											nlapiLogExecution('ERROR', 'tQtylast', tQty);
											trecord.selectLineItem('item', itemLineNo);
											trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
											var tempqty=parseInt(n)+parseInt(1);
											trecord.setCurrentLineItemValue('item', 'quantity', tQty);

											trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
											//}

											//}
										}






									}
									serialnumArray.push(serchRec[n].getValue('custrecord_serialnumber'));

								}
								if(vAdvBinManagement)
								{
									//var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
									compSubRecord.selectNewLineItem('inventoryassignment');
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serchRec[n].getValue('custrecord_serialnumber'));

									compSubRecord.commitLineItem('inventoryassignment');
									//compSubRecord.commit();
								}

								//if(parseInt(n)== parseInt(quantity-1))
								//	break;




							}
							if(vAdvBinManagement)
							{
								compSubRecord.commit();	
							}
							nlapiLogExecution('ERROR', 'qtyexceptionFlag', qtyexceptionFlag);
							var qtyexceptionFlag=true;
							if(qtyexceptionFlag == "true")
							{
								serialnumcsv = getScannedSerialNoinQtyexception;
								nlapiLogExecution('ERROR', 'Serial nums assigned when qtyexceptionFlag is true', serialnumcsv);
							}
							else
							{
								serialnumcsv = tempSerial;
								nlapiLogExecution('ERROR', 'Serial nums assigned when qtyexceptionFlag is false', serialnumcsv);
							}


						}
						tempSerial = "";
						nlapiLogExecution('ERROR', 'Serial nums assigned', serialnumcsv);
						nlapiLogExecution('ERROR', 'Serial nums tempserialId', tempserialId);
						if(!vAdvBinManagement)
						{
							if ((Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem")  && (trecord!=null && trecord!='')) {
								//trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumArray);
								trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumcsv);
							}
						}


					}
					else 
						//if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem"  || Itype == "assemblyitem") {
						if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem" ){
							nlapiLogExecution('ERROR', 'Into LOT/Batch');
							//nlapiLogExecution('ERROR', 'LP:', invtlp);
							var tempBatch = "";
							var batchcsv = "";
							var confirmLotToNS='Y';

							confirmLotToNS=GetConfirmLotToNS(varItemlocation);
							nlapiLogExecution('ERROR', 'confirmLotToNS:', confirmLotToNS);
							var batfilter = new Array();
							batfilter[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);
							batfilter[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
							batfilter[2] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', pointid);
							batfilter[3] = new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum);
							//batfilter[4] = new nlobjSearchFilter('custrecord_lpno', null, 'is', invtlp);

							var vatColumns=new Array();
							vatColumns[0] = new nlobjSearchColumn('custrecord_lotnowithquantity');
							vatColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
							/*var batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, batfilter, vatColumns);

							if (batchsearchresults) {
								for (var n = 0; n < batchsearchresults.length; n++) {


									//This is for Batch num loopin with Space separated.
									if (tempBatch == "") {
										if(confirmLotToNS == 'Y')
											tempBatch = batchsearchresults[n].getValue('custrecord_lotnowithquantity');
										else
										{
											nlapiLogExecution('ERROR', 'item_id:', item_id);
											var filters1 = new Array();          
											filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
											//Changed on 30/4/12 by suman
											filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
											//End of Chages as of 30/4/12

											var columns = new Array();

											columns[0] = new nlobjSearchColumn('itemid');
											var vItemname;
											var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
											if (itemdetails !=null) 
											{
												vItemname=itemdetails[0].getValue('itemid');
												nlapiLogExecution('ERROR', 'vItemname:', vItemname);
												var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
												vItemname=vItemname.replace(/ /g,"-");
												tempBatch=vItemname + '('+ vItQty + ')';


											}
										}
									}
									else {
										if(confirmLotToNS == 'Y')
											tempBatch = tempBatch + " " + batchsearchresults[n].getValue('custrecord_lotnowithquantity');
										else
										{
											var filters1 = new Array();          
											filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
											//Changed on 30/4/12 by suman
											filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
											//End of Chages as of 30/4/12
											var columns = new Array();

											columns[0] = new nlobjSearchColumn('itemid');
											var vItemname;
											var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
											if (itemdetails !=null) 
											{
												vItemname=itemdetails[0].getValue('itemid');
												nlapiLogExecution('ERROR', 'vItemname:', vItemname);
												var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
												vItemname=vItemname.replace(/ /g,"-");
												tempBatch=tempBatch + " " + vItemname + '('+ vItQty + ')';


											}
										}

									}

								}

								batchcsv = tempBatch;
								tempBatch = "";
							}*/
							var vItemname;
							var tempBatch='';
							if(confirmLotToNS == 'Y')
								{
								  var filters = new Array();
								  filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
								  filters.push(new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum));
								  filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof',itemid));								 
								  filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', pointid));
								  
								  var columns=new Array();
								  columns[0]=new nlobjSearchColumn('custrecord_lotnowithquantity');							  
								 
								  var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,columns);
								  nlapiLogExecution('ERROR','opentaskSearchResults',opentaskSearchResults);
								 // var tempBatch='';
								  if(opentaskSearchResults!=null && opentaskSearchResults.length>0)
								  {
									  nlapiLogExecution('ERROR','opentaskSearchResults.length',opentaskSearchResults.length);
									  for(var k1=0;k1<opentaskSearchResults.length;k1++)
									  {
										  if(tempBatch!='')
											  tempBatch = tempBatch +','+opentaskSearchResults[k1].getValue('custrecord_lotnowithquantity');
										  else
											  tempBatch = opentaskSearchResults[k1].getValue('custrecord_lotnowithquantity');  
									  }
								  }
								  nlapiLogExecution('ERROR','tempbatch',tempBatch);
								
								
								}
							else
							{
								nlapiLogExecution('ERROR', 'item_id:', item_id);
								var filters1 = new Array();          
								filters1[0] = new nlobjSearchFilter('internalid', null, 'is',item_id);
								var columns = new Array();

								columns[0] = new nlobjSearchColumn('itemid');

								var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
								if (itemdetails !=null) 
								{
									vItemname=itemdetails[0].getValue('itemid');
									nlapiLogExecution('ERROR', 'vItemname:', vItemname);
									//var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
									vItemname=vItemname.replace(/ /g,"-");
									tempBatch=vItemname + '('+ quantity + ')';


								}
							}

							if(!vAdvBinManagement)
							{	
								batchcsv = tempBatch;
								nlapiLogExecution('ERROR', 'LOT/Batch nums assigned', batchcsv);
								trecord.setCurrentLineItemValue('item', 'serialnumbers', batchcsv);
							}
							else
							{
//								var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
//								compSubRecord.selectNewLineItem('inventoryassignment');
//								//case# 20149312 starts (vLotQty is replaced with quantity because vLotQty is not defined)
//								nlapiLogExecution('ERROR', 'batchno ', batchno);
//								if(batchno==null||batchno=='')
//								{
//									if(tempBatch!=null && tempBatch!='')
//									{
//										var vtemp=tempBatch.indexOf('(');
//										batchno=tempBatch.substring(0,vtemp);
//										nlapiLogExecution('ERROR', 'batchno assigned', batchno);
//									}
//								}
//								//compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vLotQty);
//								compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', quantity);
//								//case# 20149312 ends
//								if(confirmLotToNS == 'Y')
//									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', batchno);
//								else
//									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
//								compSubRecord.commitLineItem('inventoryassignment');
//								compSubRecord.commit();
								
								

								var vItemname;
								nlapiLogExecution('ERROR', 'item_id1:', itemid);
								var filters1 = new Array();          
								filters1[0] = new nlobjSearchFilter('internalid', null, 'is',itemid);
								var columns = new Array();

								columns[0] = new nlobjSearchColumn('itemid');

								var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
								if (itemdetails !=null) 
								{
									vItemname=itemdetails[0].getValue('itemid');
									nlapiLogExecution('ERROR', 'vItemname1:', vItemname);
									//var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
									vItemname=vItemname.replace(/ /g,"-");
									tempBatch=vItemname + '('+ quantity + ')';


								}



								var filters = new Array();
								filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
								filters.push(new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum));
								filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof',itemid));								 
								filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', pointid));
								nlapiLogExecution('ERROR', 'itemstatus:', itemstatus);
								if(itemstatus!=null && itemstatus!='')
									filters.push(new nlobjSearchFilter('custrecord_sku_status', null, 'anyof',itemstatus));

								var columns=new Array();
								columns[0]=new nlobjSearchColumn('custrecord_lotnowithquantity');							  
								columns[1]=new nlobjSearchColumn('custrecord_batch_no');
								columns[2]=new nlobjSearchColumn('custrecord_act_qty');
								var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,columns);
								nlapiLogExecution('ERROR','opentaskSearchResults123',opentaskSearchResults);
								var tempBatch='';
								if(opentaskSearchResults!=null && opentaskSearchResults.length>0)
								{
									var compSubRecord = trecord.editCurrentLineItemSubrecord('item','inventorydetail');
									var	editSubrecord='T';
									nlapiLogExecution('ERROR','compSubRecord ',compSubRecord);
									var subrecordcount=0;
									if(compSubRecord !=null && compSubRecord !='')
									{
										subrecordcount=compSubRecord.getLineItemCount('inventoryassignment')
										nlapiLogExecution('ERROR','subrecordcount ',subrecordcount);
									}
									if(compSubRecord ==null || compSubRecord =='')
									{
										nlapiLogExecution('ERROR','compSubRecord create',compSubRecord);
										editSubrecord='F';
										compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
									}
									nlapiLogExecution('ERROR','opentaskSearchResults.length',opentaskSearchResults.length);
									for(var k1=0;k1<opentaskSearchResults.length;k1++)
									{

										//case # 20141363ÃƒÂ¯Ã‚Â¿Ã‚Â½start
										var batch = opentaskSearchResults[k1].getValue('custrecord_batch_no');
										//var batch = opentaskSearchResults[0].getValue('custrecord_batch_no');
										var quantity =opentaskSearchResults[k1].getValue('custrecord_act_qty');
											nlapiLogExecution('ERROR','lotquantity',quantity);
										if(poItemUOM!=null && poItemUOM!='')
										{
											/*var vbaseuomqty=0;
											var vuomqty=0;
											var eBizItemDims=geteBizItemDimensions(item_id);*/
											if(eBizItemDims!=null&&eBizItemDims.length>0)
											{
												nlapiLogExecution('ERROR', 'Dims Length', eBizItemDims.length);
												for(var z=0; z < eBizItemDims.length; z++)
												{
													if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
													{
														vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
													}
													nlapiLogExecution('ERROR', 'poItemUOM', poItemUOM);
													nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
													nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
													if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
													{
														vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
													}
												}
												if(vuomqty==null || vuomqty=='')
												{
													vuomqty=vbaseuomqty;
												}
												nlapiLogExecution('ERROR', 'vbaseuomqty', vbaseuomqty);
												nlapiLogExecution('ERROR', 'vuomqty', vuomqty);

												if(quantity==null || quantity=='' || isNaN(quantity))
													quantity=0;
												else
													quantity = (parseFloat(quantity)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

											}
										}
										nlapiLogExecution('ERROR', 'after conversion', quantity);	
										if(editSubrecord=='F')
										{
											nlapiLogExecution('ERROR','editSubrecord',editSubrecord);
											compSubRecord.selectNewLineItem('inventoryassignment');

										}
										else
										{
											try
											{
												if(subrecordcount<=opentaskSearchResults.length)
												{
												
													compSubRecord.selectLineItem('inventoryassignment',(parseInt(k1)+1));
												}
												else
												{
													nlapiLogExecution('ERROR','Newline','Newline');
													compSubRecord.selectNewLineItem('inventoryassignment');
												}

											}
											catch(e)
											{												
												compSubRecord.selectNewLineItem('inventoryassignment');
											}

										}
										//compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', lotquantity);
										nlapiLogExecution('ERROR','batchno',batchno);
										nlapiLogExecution('ERROR','quantity',quantity);
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', parseFloat(quantity).toFixed(5));
										if(confirmLotToNS == 'Y')
											compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', batch);
										else
											compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
										compSubRecord.commitLineItem('inventoryassignment');

										//case # 20141363ÃƒÂ¯Ã‚Â¿Ã‚Â½end


									}
									//if(compSubRecord != null)
									compSubRecord.commit();
								}
								//nlapiLogExecution('ERROR','tempbatch',tempBatch);

							
							}


						}
					if(trecord!=null  && trecord!='')
						trecord.commitLineItem('item');
				}
			}
			else
			{ 

				nlapiLogExecution('ERROR', 'itemid (Task)', itemid);	
				nlapiLogExecution('ERROR', 'item_id (Line)', item_id);	
				//if(vTORestrictFlag =='F')
				//{	
				itemLineNo=parseFloat(itemLineNo)-2;
				if (itemid == item_id && itemLineNo == linenum) {

					commitflag = 'Y';

					var varItemlocation;				
					varItemlocation = getItemStatusMapLoc(itemstatus);

					var fields = ['recordType', 'custitem_ebizserialin'];
					var columns = nlapiLookupField('item', itemid, fields);
					if(columns != null && columns != '')
					{
						Itype = columns.recordType;	

						//	var serialInflg="F";		
						serialInflg = columns.custitem_ebizserialin;
						nlapiLogExecution('ERROR', 'Itype', Itype);
						nlapiLogExecution('ERROR', 'columns', columns);
					}

					nlapiLogExecution('ERROR', 'Value of J', j);
					trecord.selectLineItem('item', j);
					trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
					trecord.setCurrentLineItemValue('item', 'quantity', parseFloat(quantity).toFixed(5));
					if(trantype!='transferorder')
						trecord.setCurrentLineItemValue('item', 'location', varItemlocation); //Added by ramana

					nlapiLogExecution('ERROR', 'Serialquantity', quantity);
					nlapiLogExecution('ERROR', 'Into SerialNos');
					if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem" || serialInflg == "T") {
						nlapiLogExecution('ERROR', 'Into SerialNos');
						//nlapiLogExecution('ERROR', 'invtlp', invtlp);					
						var serialline = new Array();
						var serialId = new Array();



						var tempSerial = "";
						var tempserialId = "";
						var filters = new Array();
						/*if(trantype=='returnauthorization' || trantype=='transferorder')
						{*/
						if(trantype=='returnauthorization')
						{
							filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
						}
						else if(trantype=='transferorder')
						{
							filters[0] = new nlobjSearchFilter('custrecord_serialebizsono', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['1','3','19','8']);
							filters[2] = new nlobjSearchFilter('custrecord_serialsolineno', null, 'is', linenum);
						}
						else
						{
							filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['1','3','19']);
							filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
						}
//						filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
//						filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);

						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
						var serialnumcsv = "";
						var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
						var serialnumArray=new Array();var totalconfirmedQty=0;
						if (serchRec) {
							for (var n = 0; n < serchRec.length; n++) {
								var tempserialcount=parseInt(n)+1;
								if(parseInt(tempserialcount)<=parseInt(quantity)){
									//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
									if (tempserialId == "") {
										tempserialId = serchRec[n].getId();
									}
									else {
										tempserialId = tempserialId + "," + serchRec[n].getId();
									}

									//This is for Serial num loopin with Space separated.
									if (tempSerial == "") {
										tempSerial = serchRec[n].getValue('custrecord_serialnumber');

										serialnumArray.push(serchRec[n].getValue('custrecord_serialnumber'));

									}
									else {

										tempSerial = tempSerial + " " + serchRec[n].getValue('custrecord_serialnumber');
										if(tempSerial.length>3500)
										{
											tempSerial='';
											trecord.selectLineItem('item', j);
											trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
											var tempqty=serialnumArray.length;//parseInt(n-1)+parseInt(1);
											trecord.setCurrentLineItemValue('item', 'quantity', tempqty);

											trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
											trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumArray);
											nlapiLogExecution('ERROR', 'serialnumArray', serialnumArray);

											trecord.commitLineItem('item');
											idl = nlapiSubmitRecord(trecord);
											serialnumArray=new Array();
											totalconfirmedQty=parseInt(totalconfirmedQty)+parseInt(tempqty);
											if(n<serchRec.length)
											{
												var tQty=parseInt(quantity)-parseInt(totalconfirmedQty);
												nlapiLogExecution('ERROR', 'tQty1', tQty);
												var trecord = nlapiTransformRecord(fromrecord, fromid, 'itemreceipt');
												var polinelength = trecord.getLineItemCount('item');
												/*for (var j3 = 1; j3 <= polinelength; j3++) {*/
												var item_id = trecord.getLineItemValue('item', 'item', itemLineNo);
												var itemrec = trecord.getLineItemValue('item', 'itemreceive', itemLineNo);
												//var itemLineNo = trecord.getLineItemValue('item', 'line', itemLineNo);
												var poItemUOM = trecord.getLineItemValue('item', 'unitsdisplay', itemLineNo);

												/*if (itemLineNo == linenum)
											{*/
												if(poItemUOM!=null && poItemUOM!='')
												{
													var vbaseuomqty=0;
													var vuomqty=0;
													var eBizItemDims=geteBizItemDimensions(item_id);
													if(eBizItemDims!=null&&eBizItemDims.length>0)
													{
														nlapiLogExecution('ERROR', 'Dims Length', eBizItemDims.length);
														for(var z=0; z < eBizItemDims.length; z++)
														{
															if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
															{
																vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
															}
															nlapiLogExecution('ERROR', 'poItemUOM', poItemUOM);
															nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
															nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
															if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
															{
																vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
															}
														}
														if(vuomqty==null || vuomqty=='')
														{
															vuomqty=vbaseuomqty;
														}
														nlapiLogExecution('ERROR', 'vbaseuomqty', vbaseuomqty);
														nlapiLogExecution('ERROR', 'vuomqty', vuomqty);

														if(tQty==null || tQty=='' || isNaN(tQty))
															tQty=0;
														else
															tQty = (parseFloat(tQty)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

													}
												}
												nlapiLogExecution('ERROR', 'tQtylast', tQty);
												trecord.selectLineItem('item', itemLineNo);
												trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
												var tempqty=parseInt(n)+parseInt(1);
												trecord.setCurrentLineItemValue('item', 'quantity', tQty);

												trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
												//}

												//}
											}






										}
										serialnumArray.push(serchRec[n].getValue('custrecord_serialnumber'));

									}
									//Case # 20127042 Start
									/*if(vAdvBinManagement)
									{
										var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
										compSubRecord.selectNewLineItem('inventoryassignment');
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serchRec[n].getValue('custrecord_serialnumber'));

										compSubRecord.commitLineItem('inventoryassignment');
										compSubRecord.commit();
									}	*/
									//Case # 20127042 End
								}
							}

							serialnumcsv = tempSerial;
							tempSerial = "";
						}

						nlapiLogExecution('ERROR', 'Serial nums assigned', serialnumcsv);
						nlapiLogExecution('ERROR', 'Serial nums tempserialId', tempserialId);
						if(!vAdvBinManagement)
						{
							if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem") {
								trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumArray);
							}
						}
					}
					else 
						if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem"  || Itype == "assemblyitem") {
							nlapiLogExecution('ERROR', 'Into LOT/Batch');
							//nlapiLogExecution('ERROR', 'LP:', invtlp);
							var tempBatch = "";
							var batchcsv = "";

							var confirmLotToNS='Y';

							confirmLotToNS=GetConfirmLotToNS(varItemlocation);

							nlapiLogExecution('ERROR', 'confirmLotToNS:', confirmLotToNS);


							/*var batfilter = new Array();
								batfilter[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);
								batfilter[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
								batfilter[2] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', pointid);
								batfilter[3] = new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum);
								batfilter[4] = new nlobjSearchFilter('custrecord_lpno', null, 'is', invtlp);

								var vatColumns=new Array();
								vatColumns[0] = new nlobjSearchColumn('custrecord_lotnowithquantity');
								vatColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
								var batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, batfilter, vatColumns);
								if (batchsearchresults) {
									for (var n = 0; n < batchsearchresults.length; n++) {
										//This is for Batch num loopin with Space separated.
										if (tempBatch == "") {
											if(confirmLotToNS == 'Y')
												tempBatch = batchsearchresults[n].getValue('custrecord_lotnowithquantity');
											else
											{

												var filters1 = new Array();          
												filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
												//Changed on 30/4/12 by suman
												filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
												//End of Chages as of 30/4/12
												var columns = new Array();

												columns[0] = new nlobjSearchColumn('itemid');
												var vItemname;
												var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
												if (itemdetails !=null) 
												{
													vItemname=itemdetails[0].getValue('itemid');
													nlapiLogExecution('ERROR','vItemName',vItemname);
													vItemname=vItemname.replace(/ /g,"-");
													var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
													tempBatch=vItemname + '('+ vItQty + ')';


												}
											}
										}
										else {
											if(confirmLotToNS == 'Y')
												tempBatch = tempBatch + " " + batchsearchresults[n].getValue('custrecord_lotnowithquantity');
											else
											{
												var filters1 = new Array();          
												filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
												//Changed on 30/4/12 by suman
												filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
												//End of Chages as of 30/4/12
												var columns = new Array();

												columns[0] = new nlobjSearchColumn('itemid');
												var vItemname;
												var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
												if (itemdetails !=null) 
												{
													vItemname=itemdetails[0].getValue('itemid');
													nlapiLogExecution('ERROR','vItemName1',vItemname);
													vItemname=vItemname.replace(/ /g,"-");
													var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
													tempBatch=tempBatch + " " + vItemname + '('+ vItQty + ')';


												}
											}

										}
									}

									batchcsv = tempBatch;
									tempBatch = "";
								}*/
							var vItemname;

							if(confirmLotToNS == 'Y')
								tempBatch = batchno + '('+ quantity + ')';
							else
							{
								nlapiLogExecution('ERROR', 'item_id:', item_id);
								var filters1 = new Array();          
								filters1[0] = new nlobjSearchFilter('internalid', null, 'is',item_id);
								var columns = new Array();

								columns[0] = new nlobjSearchColumn('itemid');

								var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
								if (itemdetails !=null) 
								{
									vItemname=itemdetails[0].getValue('itemid');
									nlapiLogExecution('ERROR', 'vItemname:', vItemname);
									//var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
									vItemname=vItemname.replace(/ /g,"-");
									tempBatch=vItemname + '('+ quantity + ')';


								}
							}
							if(!vAdvBinManagement)
							{	
								batchcsv = tempBatch;
								nlapiLogExecution('ERROR', 'LOT/Batch nums assigned', batchcsv);
								trecord.setCurrentLineItemValue('item', 'serialnumbers', batchcsv);
							}
							else
							{
								//Case # 20127042 Start
								/*var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
								compSubRecord.selectNewLineItem('inventoryassignment');
								compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vLotQty);
								if(confirmLotToNS == 'Y')
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', batchno);
								else
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
								compSubRecord.commitLineItem('inventoryassignment');
								compSubRecord.commit();*/
								//Case # 20127042 End
							}	
						}
					if(trecord!=null  && trecord!='')
						trecord.commitLineItem('item');

				}
				//Code Added by Ramana
				/*}
				else
				{
					trecord=null;
					idl=pointid;
				}*/	
			}
			if (commitflag == 'N' && trecord != null && trecord != '') {
				nlapiLogExecution('ERROR', 'commitflag is N', commitflag);
				trecord.selectLineItem('item', j);
				trecord.setCurrentLineItemValue('item', 'itemreceive', 'F');
				trecord.commitLineItem('item');
			}
		}
		nlapiLogExecution('ERROR', 'Before Item Receipt---', 'Before');

		/*if(trecord != null && trecord != '')
			idl = nlapiSubmitRecord(trecord);*/


		//nlapiLogExecution('ERROR', 'Item Receipt idl', idl);

		/*return idl;*/
	}
	catch(exp)
	{
		var exceptionname=trantype;
		var functionality='Creating ItemReceipt';
		var trantype2=1;//1 for Inbound and 2 for outbound
		var vebizOrdNo=pointid;
		var vcontainerLp='';
		var POarray = new Array();
		nlapiLogExecution('ERROR', 'DetailsError', functionality);	
		nlapiLogExecution('ERROR', 'vcontainerLp', vcontainerLp);
		nlapiLogExecution('ERROR', 'vebizOrdNo35', vebizOrdNo);
		var reference2="";
		var reference3="";
		var reference4 ="";
		var reference5 ="";
		var alertType=1;//1 for exception and 2 for report
		errSendMailByProcess(trantype2,exp,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
		//InsertExceptionLog(exceptionname,trantype, functionality, exp, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId);
		nlapiLogExecution('ERROR', 'Exception in TransformRec (PurchaseOrder) : ', exp);

		if (exp instanceof nlobjError) 
			nlapiLogExecution('ERROR', 'system error', exp.getCode() + '\n' + exp.getDetails());
		else 
			nlapiLogExecution('ERROR', 'unexpected error', exp.toString());

		/*nlapiLogExecution('ERROR', 'Updating PUTW task with end location - Record Id', RecordId);
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordId);
		transaction.setFieldValue('custrecord_actendloc', EndLocationId);
		nlapiSubmitRecord(transaction, false, true);
		nlapiLogExecution('ERROR', 'PUTW task updated with act end location', EndLocationId);*/

		nlapiLogExecution('ERROR', 'Putaway Failed ');
		//case# 20149312 starts
		//POarray["custparam_error"] = exp.getCode() + '\n' + exp.getDetails();
		if (exp instanceof nlobjError) 
			POarray["custparam_error"] = exp.getCode() + '\n' + exp.getDetails();
		else
			POarray["custparam_error"] = exp.toString();
		//case# 20149312 ends
		response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
		return;
	}	
}
function OpentaskrecordsforItemStatus(poid,vItemStatus)
{
	nlapiLogExecution('ERROR','in to OpentaskrecordsforItemStatus',poid);
	nlapiLogExecution('ERROR','vItemStatus',vItemStatus);
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty')); 
	filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'isnotempty'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', poid));
	filters.push(new nlobjSearchFilter('custrecord_sku_status', 'null', 'anyof', vItemStatus));
	var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,null);
	nlapiLogExecution('ERROR','opentaskSearchResults',opentaskSearchResults);
	return opentaskSearchResults;
}
