/***************************************************************************

   eBizNET Solutions Inc 
 ****************************************************************************
 *
 *  $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_InventoryMoveMain.js,v $
 *  $Revision: 1.6.2.6.4.4.2.14 $
 *  $Date: 2015/04/28 16:00:35 $
 *  $Author: nneelam $
 *  $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 * DESCRIPTION
 *  Functionality
 *
 * REVISION HISTORY
 *  $Log: ebiz_RF_InventoryMoveMain.js,v $
 *  Revision 1.6.2.6.4.4.2.14  2015/04/28 16:00:35  nneelam
 *  case# 201412525
 *
 *  Revision 1.6.2.6.4.4.2.13  2014/06/13 09:51:41  skavuri
 *  Case# 20148882 (added Focus Functionality for Textbox)
 *
 *  Revision 1.6.2.6.4.4.2.12  2014/05/30 00:34:21  nneelam
 *  case#  20148622
 *  Stanadard Bundle Issue Fix.
 *
 *  Revision 1.6.2.6.4.4.2.11  2014/04/29 06:28:26  spendyala
 *  CASE201112/CR201113/LOG201121
 *  Issue fixed related to case#20148209
 *
 *  Revision 1.6.2.6.4.4.2.10  2014/03/07 15:54:10  skavuri
 *  Case# 20127589 issue fixed
 *
 *  Revision 1.6.2.6.4.4.2.9  2013/11/28 15:18:03  grao
 *  Case# 20125874 related issue fixes in SB 2014.1
 *
 *  Revision 1.6.2.6.4.4.2.8  2013/11/25 15:19:41  grao
 *  Case# 20125874 related issue fixes in SB 2014.1
 *
 *  Revision 1.6.2.6.4.4.2.7  2013/11/22 14:47:48  grao
 *  Case# 20125874  related issue fixes in SB 2014.1
 *
 *  Revision 1.6.2.6.4.4.2.6  2013/10/25 20:02:30  snimmakayala
 *  GSUSA PROD ISSUE
 *  Case# : 20125294
 *
 *  Revision 1.6.2.6.4.4.2.5  2013/10/04 15:45:22  rmukkera
 *  Case# 20124750
 *
 *  Revision 1.6.2.6.4.4.2.4  2013/09/26 05:12:34  schepuri
 *  issue related to allowing to move one Location inventory to another Location.
 *
 *  Revision 1.6.2.6.4.4.2.3  2013/09/11 15:23:51  rmukkera
 *  Case# 20124376
 *
 *
 ****************************************************************************/
function InventoryMoveMain(request, response){
	if (request.getMethod() == 'GET') {
		var context = nlapiGetContext();
		context.setSessionObject('session', null);

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4,st5,st6,st7;
		if( getLanguage == 'es_ES')
		{

			st0 = "INVENTARIO DE MUDANZA";
			st1 = "ENTRAR";
			st2 = "UBICACI&#211;N";
			st3 = "LP";
			st4 = "BUSCAR";
			st5 = "ANTERIOR";
			st6 = "TEMA";
			st7 = "DESCRIPCI&#211;N";

		}
		else
		{
			st0 = "INVENTORY MOVE";
			st1 = "ENTER";
			st2 = "LOCATION";
			st3 = "LP";
			st4 = "SEARCH";
			st5 = "PREV";
			st6 = "ITEM";
			st7 = "DESCRIPTION";



		}		
		var getBinLocation=request.getParameter('custparam_imbinlocationname');
		if(getBinLocation==null || getBinLocation=='')
		{
			getBinLocation="";
		}
		var getLp=request.getParameter('custparam_lp');
		if(getLp==null || getLp=='')
		{
			getLp="";
		}
		var getItem=request.getParameter('custparam_imitem');
		if(getItem==null || getItem=='')
		{
			getItem="";
		}
		var functionkeyHtml=getFunctionkeyScript('_rf_inventorymove_main');
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";     
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterlp').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head>";
		html = html + "<body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_inventorymove_main' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1; //ANY/ALL";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlocation' type='text' value='"+getBinLocation+"'>";//Case# 20127589
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3;		
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlp' id='enterlp' type='text' value='"+getLp+"'>";//Case# 20127589
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>ITEM";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enteritemname' type='text' value='"+getItem+"'>";// Case# 20127589 Item must be pass as string through url 
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "				<tr><td align = 'left'>SEARCH <input name='cmdSearch' type='submit' value='F8' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr style='visibility:hidden;'>";
		html = html + "				<td align = 'left'>" + st6;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr style='visibility:hidden;'>";
		html = html + "				<td align = 'left'><input name='enteritem' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr style='visibility:hidden;'>";
		html = html + "				<td align = 'left'>" + st7;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr style='visibility:hidden;'>";
		html = html + "				<td align = 'left'><input name='enterdescription' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";		
		html = html + "			<tr>";
		html = html + "				<td>";
		html = html + "				    <input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				</td>";
		html = html + "			</tr>";		
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlp').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		var getLP = request.getParameter('enterlp');
		var getBinLocation = request.getParameter('enterlocation');
		//var getItem = request.getParameter('enteritem');
		var getItem = request.getParameter('enteritemname');
		var getItemDesc = request.getParameter('enterdescription');
		var getLPId = ''; 
		var getBinLocationId =''; 
		var getItemId='';
		var getItemDescId='';

		nlapiLogExecution('ERROR', 'enterlocation', request.getParameter('enterlocation'));       

		var ActualBeginDate = DateStamp();
		var ActualBeginTime = TimeStamp();

		var IMarray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		IMarray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR', 'getLanguage', IMarray["custparam_language"]);


		var st8,st9,st10,st11;
		if( getLanguage == 'es_ES')
		{

			st8 = "LP NO V&#193;LIDO";
			st9 = "UBICACI&#211;N NO V&#193;LIDA";
			st10 = "ART&#205;CULO INV&#193;LIDO ";
			st11 = "INGRESE LP NUEVA";
		}
		else
		{

			st8 = "INVALID LP";
			st9 = "INVALID LOCATION";
			st10 = "INVALID ITEM";
			st11 = "ENTER LOCATION/LP";
		}

		IMarray["custparam_actualbegindate"] = ActualBeginDate;

		var TimeArray = new Array();
		TimeArray = ActualBeginTime.split(' ');

		IMarray["custparam_actualbegintime"] = TimeArray[0];
		IMarray["custparam_actualbegintimeampm"] = TimeArray[1];

		IMarray["custparam_screenno"] = '18';

		var optedEvent = request.getParameter('cmdPrevious');

		if (optedEvent == 'F7') {
			response.sendRedirect('SUITELET', 'customscript_rf_inventorymenu', 'customdeploy_rf_inventorymenu_di', false, IMarray);
		}
		else 
		{
			try 
			{
				nlapiLogExecution('ERROR', 'LP entered', getLP);
				nlapiLogExecution('ERROR', 'BinLocation entered', getBinLocation);
				nlapiLogExecution('ERROR', 'Item entered', getItem);

				if (getLP != null && getLP != '') 
				{
					var LPFilters = new Array();
					LPFilters[0] = new nlobjSearchFilter('name', null, 'is', getLP);

					var LPSearchResults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, LPFilters, null);  	

					nlapiLogExecution('ERROR', 'LPSearchResults', LPSearchResults);

					if (LPSearchResults != null && LPSearchResults.length > 0) 
					{						
						nlapiLogExecution('ERROR', 'LPSearchResults', LPSearchResults.length);
						var getLPId = LPSearchResults[0].getId();
						nlapiLogExecution('ERROR', 'LP Id', getLPId );
						IMarray["custparam_lpid"] = getLPId;
						IMarray["custparam_lp"] = getLP;
						IMarray["custparam_hdntemplp"] = getLP;
						IMarray["custparam_searchlp"] = getLP; //for djn
					}
					else 
					{
						IMarray["custparam_error"] = st8;
						nlapiLogExecution('ERROR', 'SearchResults of given LP', 'LP is not found');

						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);

						return;
					}
				}



				if (getBinLocation != null && getBinLocation != '') 
				{
					var BinLocationFilters = new Array();
					BinLocationFilters[0] = new nlobjSearchFilter('name', null, 'is', getBinLocation);
					BinLocationFilters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
					var vRoleLocation=getRoledBasedLocationNew();
					nlapiLogExecution('ERROR', 'vRoleLocation', vRoleLocation);
					if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
					{
						//Case #20125874 start
						vRoleLocation.push('@NONE@');
						BinLocationFilters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof', vRoleLocation));
						//end
					}
					var LocationSearchResults = nlapiSearchRecord('customrecord_ebiznet_location', null, BinLocationFilters, null);    				
					nlapiLogExecution('ERROR', 'LocationSearchResults', LocationSearchResults);
					if (LocationSearchResults != null && LocationSearchResults.length > 0) 
					{
						nlapiLogExecution('ERROR', 'LocationSearchResults', LocationSearchResults.length);
						var getBinLocationId = LocationSearchResults[0].getId();
						nlapiLogExecution('ERROR', 'Location Id', getBinLocationId );
						IMarray["custparam_imbinlocationid"] = getBinLocationId;
						IMarray["custparam_hdntmplocation"] = getBinLocationId;
						IMarray["custparam_imbinlocationname"] = getBinLocation;
						IMarray["custparam_searchimbinlocationid"] = getBinLocationId; //for djn
					}
					else 
					{
						IMarray["custparam_error"] = st9;
						nlapiLogExecution('ERROR', 'SearchResults of given Location', 'Location is not found');

						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
						nlapiLogExecution('ERROR', 'Bin Location is not entered', getBinLocation);
						return;
					}	
				}


				if (getItem != null && getItem != '') 
				{
					/*var ItemFilters = new Array();
					ItemFilters[0] = new nlobjSearchFilter('name', null, 'is', getItem);

					var ItemSearchResults = nlapiSearchRecord('item', null, ItemFilters, null);   		        
					 */
					// Changed On 30/4/12 by Suman
					/*var ItemFilters=new Array();
					ItemFilters.push(new nlobjSearchFilter('nameinternal', null, 'is', getItem));
					ItemFilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));

					var ItemSearchResults = nlapiSearchRecord('item', null, ItemFilters, null);   	

					// End of Changes as On 30/4/12
*/
					
					var ItemSearchResults=validateSKUEntered(getItem);
					if (ItemSearchResults != null && ItemSearchResults!="") 
					{
						nlapiLogExecution('ERROR', 'ItemSearchResults', ItemSearchResults);
						var getItemId = ItemSearchResults;
						nlapiLogExecution('ERROR', 'Item Id', getItemId);
						IMarray["custparam_imitemid"] = getItemId;
						IMarray["custparam_hdntempitem"] = getItemId;
						IMarray["custparam_imitem"] = getItem;
					}
					else 
					{
						IMarray["custparam_error"] = st10;
						nlapiLogExecution('ERROR', 'SearchResults of given Item', 'Item is found');

						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
						nlapiLogExecution('ERROR', 'Item is not entered', getItem);
						return;
					}
				}


				if (getItemDesc != null && getItemDesc != '') 
				{
					var ItemDescFilters = new Array();
					ItemDescFilters[0] = new nlobjSearchFilter('custitem_ebizdescriptionitems', null, 'is', getItemDesc);

					var ItemDescSearchResults = nlapiSearchRecord('item', null, ItemDescFilters, null);   		        

					if (ItemDescSearchResults != null && ItemDescSearchResults.length > 0) 
					{
						nlapiLogExecution('ERROR', 'ItemDescSearchResults', ItemDescSearchResults.length);
						var getItemDescId = ItemDescSearchResults[0].getId();
						nlapiLogExecution('ERROR', 'Item Id', getItemDescId);
						IMarray["custparam_itemDescid"] = getItemDescId;
						IMarray["custparam_itemDesc"] = getItemDesc;
					}
					else 
					{
						IMarray["custparam_error"] = st10;
						nlapiLogExecution('ERROR', 'SearchResults of given Item', 'Item is found');

						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
						nlapiLogExecution('ERROR', 'Item is not entered', getItem);
						//return;
					}
				}


				if((getLP!=null & getLP!='')||  (getBinLocation!=null && getBinLocation!='') || (getItem!= null & getItem!=''))
				{
					response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_cont', 'customdeploy_rf_inventory_move_cont_di', false, IMarray);
					nlapiLogExecution('ERROR', 'Navigating to SKU/LOCATION Selected', 'Success');
				}
				else
				{
					IMarray["custparam_error"] = 'ENTER LOCATION/LP/ITEM';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
					nlapiLogExecution('ERROR', 'Catching the Search Results', 'Length is null');
				}
			}
			catch (e) 
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
				nlapiLogExecution('ERROR', 'Catching the Search Results', 'Length is null');
			}
			nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
		}        
	}
}

function IMSearch(LP, BinLocation, Item, ItemDescription)
{
	var IMFilter = new Array();
	IMFilter[0] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3, 19]);
	IMFilter[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

	var i = 3;


	if (LP != '') {
		IMFilter[i] = new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', LP);
		i++;
	}
	else 
		if (BinLocation != '') {
			IMFilter[i] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'equalto', BinLocation);
			i++;
		}
		else 
			if (Item != '') {
				IMFilter[i] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'equalto', Item);
				i++;
			}
			else 
				if (ItemDescription != '') {
					IMFilter[i] = new nlobjSearchFilter('custrecord_ebiz_itemdesc', null, 'is', ItemDescription);
				}

	nlapiLogExecution('DEBUG', 'IMFilter', IMFilter);

	var IMSearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, IMFilter, null);

	if (IMSearchResults != null && IMSearchResults.length != 0) 
	{   
		var IMId = IMSearchResults[0].getId();
		var IMRecord = nlapiLoadRecord('customrecord_ebiznet_createinv', IMId);
	}

	return IMFilter;
}

function SearchLP(LP){
	var LPId;
	var LPFilter = new Array();
	LPFilter.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', LP));
	LPFilter.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3, 19]));
	LPFilter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var LPSearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, LPFilter, null);

	if (LPSearchResults != null && LPSearchResults.length != 0) {
		LPId = LPSearchResults[0].getId();
	}

	return LPId;
}

function SearchBinLocation(BinLocation){
	var BinLocationId;

	var BinLocationFilter = new Array();
	BinLocationFilter[0] = new nlobjSearchFilter('name', null, 'is', BinLocation);
	BinLocationFilter[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

	var BinLocationSearchResults = nlapiSearchRecord('customrecord_ebiznet_location', null, BinLocationFilter, null);

	if (BinLocationSearchResults != null && BinLocationSearchResults.length != 0) {
		BinLocationId = BinLocationSearchResults[0].getId();
	}

	return BinLocationId;
}

function SearchItem(Item, ItemDescription){
	var ItemId;

	var ItemFilter = new Array();
	ItemFilter[0] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

	if (Item != null) {
		//Changed as on 30/4/12 by suman
		ItemFilter[1] = new nlobjSearchFilter('nameinternal', null, 'is', Item);
		if (ItemDescription != null) {
			ItemFilter[2] = new nlobjSearchFilter('custitem_ebizdescriptionitems', null, 'is', ItemDescription);
		}
	}
	else {
		if (ItemDescription != null) {
			ItemFilter[1] = new nlobjSearchFilter('custitem_ebizdescriptionitems', null, 'is', ItemDescription);
		}

	}
	var ItemSearchResults = nlapiSearchRecord('item', null, ItemFilter, null);

	if (ItemSearchResults != null && ItemSearchResults.length != 0) {
		ItemId = ItemSearchResults[0].getId();
	}

	return ItemId;
}

function validateSKUEntered(itemNo)
{
	try{
		nlapiLogExecution('Debug', 'Into validateSKU',itemNo);

		var actItem="";

		var invitemfilters = new Array();
		invitemfilters.push(new nlobjSearchFilter('nameinternal',null, 'is',itemNo));
		invitemfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		var invLocCol = new Array();
		invLocCol[0] = new nlobjSearchColumn('internalid');

		var invitemRes = nlapiSearchRecord('item',null,invitemfilters,invLocCol);
		if(invitemRes!=null && invitemRes!='')
		{
			actItem=invitemRes[0].getId();
		}
		else
		{
			nlapiLogExecution('Debug', 'inSide UPCCODE',itemNo);

			var invLocfilters = new Array();
			invLocfilters.push(new nlobjSearchFilter('upccode', null, 'is', itemNo));
			invLocfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));

			var invtRes = nlapiSearchRecord('item', null, invLocfilters, invLocCol);

			if(invtRes!=null && invtRes!='')
			{
				actItem=invtRes[0].getId();
			}
			else
			{
				nlapiLogExecution('Debug', 'inSide SKUALIAS',itemNo);
				var skuAliasFilters = new Array();
				skuAliasFilters.push(new nlobjSearchFilter('name', null, 'is', itemNo));

				var skuAliasCols = new Array();
				skuAliasCols[0] = new nlobjSearchColumn('custrecord_ebiz_item');

				var skuAliasResults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, skuAliasFilters, skuAliasCols);

				if(skuAliasResults != null)
					actItem=skuAliasResults[0].getValue('custrecord_ebiz_item');
			}			
		}

		nlapiLogExecution('Debug', 'Out of validateSKU',actItem);

		return actItem;
	}
	catch(exp)
	{
		nlapiLogExecution("ERROR","Exception",exp);

	}
}