/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Client/Attic/ebiz_Packing_CL_Bulk.js,v $
 *     	   $Revision: 1.1.2.3 $
 *     	   $Date: 2013/04/04 14:32:22 $
 *     	   $Author: schepuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_Packing_CL_Bulk.js,v $
 * Revision 1.1.2.3  2013/04/04 14:32:22  schepuri
 * alert for ship complete issue fix
 *
 * Revision 1.1.2.2  2013/03/19 11:46:05  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.1.2.1  2013/03/06 09:52:49  gkalla
 * CASE201112/CR201113/LOG201121
 * Lexjet Files merging for Standard bundle
 *
 * Revision 1.1.2.5  2012/07/31 10:55:52  schepuri
 * CASE201112/CR201113/LOG201121
 * Issue related to Auto Assign LP
 *
 * Revision 1.1.2.4  2012/06/15 15:45:01  gkalla
 * CASE201112/CR201113/LOG201121
 * Packing
 *
 * Revision 1.1.2.3  2012/06/14 18:15:04  gkalla
 * CASE201112/CR201113/LOG201121
 * GUI Packing
 *
 * Revision 1.1.2.2  2012/06/14 17:43:22  gkalla
 * CASE201112/CR201113/LOG201121
 * GUI Packing
 *
 * Revision 1.1.2.1  2012/06/14 17:36:24  gkalla
 * CASE201112/CR201113/LOG201121
 * GUI Packing
 *  
 *
 * 
 *****************************************************************************/



/**
 * @returns {Boolean}
 */
function OnPalletClose(type,name)
{
	var ScanItem=nlapiGetFieldValue('custpage_scanitem');
	if(ScanItem == null || ScanItem =='')
	{
		var NewContLP=nlapiGetFieldValue('custpage_contlp');
		var IsAuto=nlapiGetFieldValue('custpage_lpcreate');
		var soid=nlapiGetFieldValue('custpage_ebizso');
		//alert('soid:' + soid);
		var commtdqty = 0;
		var orderqty = 0;
		var fulfildQty = 0;
		var kititemTypesku = '';

		if(NewContLP==null || NewContLP =='')
		{
			alert('Please enter value for: Carton#');
			return false;
		}

		var trantype = nlapiLookupField('transaction', soid, 'recordType');

		var filters = new Array();
		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));	
		filters.push(new nlobjSearchFilter('internalidnumber', null, 'equalto', soid));
		filters.push(new nlobjSearchFilter('shipping', null, 'is', 'F'));//To avoid shipping lines.
		filters.push(new nlobjSearchFilter('taxline', null, 'is', 'F'));//To avoid tax lines.

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('internalid');	
		columns[1] = new nlobjSearchColumn('line');
		columns[2] = new nlobjSearchColumn('quantitycommitted');
		columns[3] = new nlobjSearchColumn('quantity');
		columns[4] = new nlobjSearchColumn('item');
		columns[5] = new nlobjSearchColumn('quantityshiprecv');

		var searchresults = new nlapiSearchRecord(trantype, null, filters, columns);

		var siteid='';
		var lineCnt = nlapiGetLineItemCount('custpage_items');
		//alert('lineCnt:' + lineCnt);
		var vLineChecked=false;
		var vLineqtyEqual=false;
		for (var s = 1; s <= lineCnt; s++) {
			//alert('s:' + s);
			var vPackQty1 = nlapiGetLineItemValue('custpage_items','custpage_qty',s);
			var vPickQty1= nlapiGetLineItemValue('custpage_items','custpage_pickqty',s);
			var linenumber = nlapiGetLineItemValue('custpage_items','custpage_ordlineno',s);	
			var parentsku = nlapiGetLineItemValue('custpage_items','custpage_parentsku',s);	
			var vfono=nlapiGetLineItemValue('custpage_items', 'custpage_ebizcntrlno',s);
			
//			alert('linenumber:' + linenumber);
//			alert('parentsku:' + parentsku);


			if(vPackQty1 != null && vPackQty1 != '' && parseFloat(vPackQty1)>0)		 
			{
				vLineChecked=true;				
			}
			//alert('vPickQty1  :' + vPickQty1);
			//alert('vPackQty1  :' + vPackQty1);

			if(vPickQty1 != vPackQty1)
			{	
				//alert('if');
				var shipcomplete='N';
				var filtersfo = new Array();

				filtersfo.push(new nlobjSearchFilter('internalid', null, 'is', vfono));				
				filtersfo.push(new nlobjSearchFilter('custrecord_ordline', null, 'is', linenumber));

				var columnsfo = new Array();
				columnsfo[0] = new nlobjSearchColumn('custrecord_shipcomplete');

				var searchresultsfo = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filtersfo, columnsfo);
				if(searchresultsfo!=null)
					shipcomplete=searchresultsfo[0].getValue('custrecord_shipcomplete');

				if(shipcomplete=='T')
				{
					
					alert('This SO/TO has created with Ship Complete set ON');
					return false;
				}

			}	

			if(parseFloat(vPackQty1) < parseFloat(vPickQty1))		 
			{
				vLineqtyEqual=true;
			}

		}
		if(vLineqtyEqual == true)
		{
			if( confirm('Are you sure you want to proceed ? Pack qty is less than pick qty') )
				return true;
			else
				return false;
		}
		if(vLineChecked == false)
		{
			alert('Please add atleast one task to pack');
			return false;
		}
		else
		{
			nlapiSetFieldValue('custpage_lpcreate','AUTO');	

			//alert('searchresults:' + searchresults.length);

			for (var t = 0; searchresults!=null && t<searchresults.length; t++) {

				var solineno = searchresults[t].getValue('line');
				commtdqty=parseFloat(searchresults[t].getValue('quantitycommitted'));
				fulfildQty=parseFloat(searchresults[t].getValue('quantityshiprecv'));
				var vPackQty2=0;
				var otparentsku = '';

				if(fulfildQty==null || fulfildQty=='' || isNaN(fulfildQty))
				{
					fulfildQty=0;
				}
				if(commtdqty==null || commtdqty=='' || isNaN(commtdqty))
				{
					commtdqty=0;
				}

				var totalcommtdqty=parseFloat(commtdqty)+parseFloat(fulfildQty);

				//alert('lineCnt:' + lineCnt);

				for (var x = 1; x <= lineCnt; x++) {

					var otlinenumber = nlapiGetLineItemValue('custpage_items','custpage_ordlineno',x);						
					//alert('solineno:' + solineno);
					//alert('otlinenumber:' + otlinenumber);
					if(solineno==otlinenumber)
					{
						otparentsku = nlapiGetLineItemValue('custpage_items','custpage_parentsku',x);	
						vPackQty2=parseFloat(vPackQty2)+parseFloat(nlapiGetLineItemValue('custpage_items','custpage_qty',x));	

//						alert('fulfildQty:' + fulfildQty);
//						alert('commtdqty:' + commtdqty);
//						alert('totalcommtdqty:' + totalcommtdqty);
//						alert('vPackQty2:' + vPackQty2);

						if((parseFloat(fulfildQty)+parseFloat(vPackQty2))>parseFloat(totalcommtdqty))
						{
							//alert('otparentsku:' + otparentsku);

							if(otparentsku!=null && otparentsku!='')
							{
								kititemTypesku = nlapiLookupField('item', otparentsku, 'recordType');
							}
							//alert('kititemTypesku:' + kititemTypesku);
							if(kititemTypesku!='kititem')
							{
								alert('Excess pack qty entered');
								nlapiSetLineItemValue('custpage_items','custpage_qty',x,0);
								nlapiSetFieldValue('custpage_scanitem','');
								nlapiSetFieldValue('custpage_scanqty','');
								document.getElementById('custpage_scanitem').focus();
								return false;
							}
						}
					}
				}

			}

			return true;
		}

		var lpExists='N';
		var filtersmlp = new Array();
		filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', NewContLP);

		var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

		if (SrchRecord != null && SrchRecord.length > 0) {
			nlapiLogExecution('ERROR', 'LP FOUND');

			lpExists = 'Y';
		}

		if(lpExists =='N')
		{	
			var LPReturnValue = ebiznet_LPRange_CL_withLPType(NewContLP.replace(/\s+$/,""), '2','2',siteid);//'2'UserDefiend,'2'PICK
			if(LPReturnValue==false && IsAuto != 'AUTO')
			{

				if(LPReturnValue1==false)
				{	
					alert('Invalid LP Range');
					return false;
				}
			}
		}
		else
		{
			alert('Entered Closed LP');
			return false;
		}	
	}
	else
		return false;
}

function backtogeneratesearch()
{ 	 
	var PackingURL = nlapiResolveURL('SUITELET', 'customscript_ebiz_packing', 'customdeploy_ebiz_packing_di');
	var ctx = nlapiGetContext();
	if (ctx.getEnvironment() == 'PRODUCTION') {
		PackingURL = 'https://system.netsuite.com' + PackingURL;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			PackingURL = 'https://system.sandbox.netsuite.com' + PackingURL;				
		}
	//window.open(WavePDFURL);

	window.location.href = PackingURL;
}


function backtogeneratesearchNew()
{ 	 
	var PackingURL = nlapiResolveURL('SUITELET', 'customscript_ebiz_packingnew', 'customdeploy_ebiz_packingnew_di');
	var ctx = nlapiGetContext();
	if (ctx.getEnvironment() == 'PRODUCTION') {
		PackingURL = 'https://system.netsuite.com' + PackingURL;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			PackingURL = 'https://system.sandbox.netsuite.com' + PackingURL;				
		}
	//window.open(WavePDFURL);

	window.location.href = PackingURL;
}

function onChange(type,name)
{
	//alert('name' + name);
	//alert('Hi');    
	//alert('repeat ' + nlapiGetFieldValue('custpage_repeat'));


	var Itemexist=false;
	var item=nlapiGetFieldValue('custpage_scanitem');


	if(item!=null && item!='')
	{

		var vitemimage=nlapiGetFieldValue('custpage_image');
		var vItemArr = validateSKUId(item);
		var vItemType;
		var Itemimage;
		var currItem;
		nlapiLogExecution('ERROR','vItemArr',vItemArr);
		if(vItemArr != null && vItemArr != '' )
		{
			vItemType=vItemArr[0];
			currItem=vItemArr[1];
			//	Itemimage=vItemArr[2];
		}
//		if(Itemimage!=null && Itemimage!='')
//		{
//		var url;
//		var ctx = nlapiGetContext();
//		nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
//		if (ctx.getEnvironment() == 'PRODUCTION') 
//		{
//		url = 'https://system.netsuite.com';			
//		}
//		else if (ctx.getEnvironment() == 'SANDBOX') 
//		{
//		url = 'https://system.sandbox.netsuite.com';				
//		}


//		var file = url+Itemimage;

//		var html = "<html><body>";
//		html=html +"<img src='" + file + "'></img>";
//		html = html + "</body></html>";

//		nlapiSetFieldValue('custpage_image',html);

//		}

		var ordId=nlapiGetFieldValue('custpage_ordhead');
		//alert('name' + name);
		//alert('ordId' + ordId);
		if(ordId != null && ordId != '')
		{
			if(name=='custpage_scanitem')
			{

				document.getElementById('custpage_scanqty').focus();



			}
			if(name=='custpage_scanqty')
			{	  
				//alert('scan qty');
				AddPackNew(type,name);
				document.getElementById('custpage_scanitem').focus();
				nlapiSetFieldValue('custpage_scanitem','');
				nlapiSetFieldValue('custpage_scanqty','');

			}

		}

		nlapiSetFieldValue('custpage_repeat','1');
		return false;
	}


	//document.getElementById('custpage_scanitem').focus();
}




function AddPackNew(type,name)
{
	//alert('Into Packing' + name);
	var ordId=nlapiGetFieldValue('custpage_ordhead');
	var vItem=nlapiGetFieldValue('custpage_scanitem');
	var vPackQty=nlapiGetFieldValue('custpage_scanqty');
	//var vPackType=nlapiGetFieldValue('custpage_packtype','bb');
	var Itemexist=false;

	if(vItem == null || vItem == '')
	{
		//alert("Please scan Item");
		document.getElementById('custpage_scanitem').focus();
		return false;
	}
	else if(vPackQty == null || vPackQty ==''  || vPackQty =='0' || parseFloat(vPackQty)==0)
	{
		vPackQty=1;
	}	
	else if(vPackQty != null && vPackQty != '' && parseFloat(vPackQty)<0)
	{
		//alert("Pack Qty should not be less than zero");
		nlapiSetFieldValue('custpage_scanqty',0);
		document.getElementById('custpage_scanqty').focus();
		return false;
	}
	//alert('ordId' + ordId);
	if(ordId != null && ordId != '')
	{



		//alert('vItemCount' + vItemCount);
		//alert('TaskCount' + TaskCount);

		var vItemArr = validateSKUId(vItem);
		//alert('vItemArr' + vItemArr);
		var vItemType='F';
		if(vItemArr != null && vItemArr != '' && vItemArr.length>1)
		{
			vItemType=vItemArr[0];
			currItem=vItemArr[1];
		}
		var vItemInfo;

		if(currItem != null && currItem != '')
		{
			var lineCnt = nlapiGetLineItemCount('custpage_items');
			//alert('lineCnt' + lineCnt);
			//alert('Scanned Item id ' + currItem);
			var vRemQty=vPackQty;
			for(var p=1;p<=lineCnt && parseFloat(vRemQty)>0 ;p++)
			{
				var OldPackQty= nlapiGetLineItemValue('custpage_items','custpage_qty',p);
				//alert('OldPackQty' + OldPackQty);
				var OldPickQty= nlapiGetLineItemValue('custpage_items','custpage_pickqty',p);
				//alert('OldPickQty' + OldPickQty);
				var EbizSKU= nlapiGetLineItemValue('custpage_items','custpage_ebizskuno',p);
				//alert('EbizSKU' + EbizSKU);
				if(EbizSKU==currItem && parseFloat(OldPickQty) > parseFloat(OldPackQty))
				{
					var RemQtyPack=parseFloat(OldPickQty) - parseFloat(OldPackQty);
					if(RemQtyPack != null && RemQtyPack != '' && parseFloat(RemQtyPack)< parseFloat(vRemQty))
					{
						nlapiSetLineItemValue('custpage_items','custpage_qty',p,parseFloat(OldPackQty) + parseFloat(RemQtyPack));
						vRemQty=parseFloat(vRemQty)-parseFloat(RemQtyPack);
					}	
					else
					{
						nlapiSetLineItemValue('custpage_items','custpage_qty',p,parseFloat(OldPackQty) + parseFloat(vRemQty));
						vRemQty=0;
					}	
				}
				if(EbizSKU==currItem)
					Itemexist=true;

			}
			if(Itemexist==false)
			{
				alert('Invalid Item');
				nlapiSetFieldValue('custpage_scanitem','');
				nlapiSetFieldValue('custpage_scanqty','');
				document.getElementById('custpage_scanitem').focus();
				return false;
			}	
			if(vRemQty != null && vRemQty != '' && parseFloat(vRemQty)>0)
			{
				alert('Excess ' + vRemQty + ' pack qty entered');
				nlapiSetFieldValue('custpage_scanitem','');
				nlapiSetFieldValue('custpage_scanqty','');
				document.getElementById('custpage_scanitem').focus();
				return false;
			}	

			//nlapiSetLineItemValue('custpage_items','custpage_qty',1,vPackQty);
			/*var filters=new Array();
				filters.push( new nlobjSearchFilter('name', null, 'is', ordId));


				filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//Status Flag - C (Picks Confirmed)
				filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK
				filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', vItem));//Task Type - PICK

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('name');
				columns[1] = new nlobjSearchColumn('custrecord_container_lp_no');
				columns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
				columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
				columns[4] = new nlobjSearchColumn('custrecord_comp_id');
				columns[5] = new nlobjSearchColumn('custrecord_site_id');
				columns[6] = new nlobjSearchColumn('custrecord_container');
				columns[7] = new nlobjSearchColumn('custrecord_total_weight');
				columns[8] = new nlobjSearchColumn('custrecord_sku');
				columns[9] = new nlobjSearchColumn('custrecord_act_qty');
				columns[10] = new nlobjSearchColumn('custrecord_wms_location');


				columns[0].setSort();
				columns[1].setSort();
				columns[10].setSort();
				var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
				if(searchresults != null && searchresults != '')
				{
					nlapiSetCurrentLineItemValue('custpage_items','custpage_internalid',searchresults[0].getId(),false);
					nlapiSetCurrentLineItemValue('custpage_items','custpage_skuin',currItem,false);
					nlapiSetCurrentLineItemValue('custpage_items','custpage_qty',searchresults[0].getValue('custrecord_act_qty'),false);
					nlapiSetCurrentLineItemValue('custpage_items','custpage_actqty',searchresults[0].getValue('custrecord_act_qty'),false);
				}*/	
		}
		else
		{
			alert('Invalid Item');
			nlapiSetFieldValue('custpage_scanitem','');
			document.getElementById('custpage_scanitem').focus();
			return false;
		}	


		/*alert("Name: " + name);
		alert("type: " +type);*/
	}
	//alert('Hi');
	nlapiSetFieldValue('custpage_scanitem','');
	nlapiSetFieldValue('custpage_scanqty','');
	nlapiSetFieldValue('custpage_repeat','1');

	document.getElementById('custpage_scanitem').focus();
	//return false;
}

function AutoAssignLP()
{

	var ordId=nlapiGetFieldValue('custpage_ordhead');
	var cntLP=nlapiGetFieldValue('custpage_contlp');

	var lineCnt = nlapiGetLineItemCount('custpage_items');
	var vLineChecked=false;
	for (var s = 1; s <= lineCnt; s++) {

		var vsiteid= nlapiGetLineItemValue('custpage_items','custpage_siteid',s);

	}

	var newContainerLpNo=GetMaxLPNo('1', '2',vsiteid);
	nlapiSetFieldValue('custpage_contlp',newContainerLpNo);
	nlapiSetFieldValue('custpage_lpcreate','AUTO');

}

function validateSKUId(itemNo)
{
	nlapiLogExecution('Error', 'Into validateSKU',itemNo);

	var actItem=new Array();

	var invitemfilters = new Array();
	invitemfilters.push(new nlobjSearchFilter('nameinternal',null, 'is',itemNo));
	// Changed On 30/4/12 by Suman
	invitemfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
	// End of Changes as On 30/4/12
	var invLocCol = new Array();
	//invLocCol[0] = new nlobjSearchColumn('itemid');
	invLocCol[0] = new nlobjSearchColumn('externalid');
	//invLocCol[1] = new nlobjSearchColumn('custitem_djn_internal_thumbnail');
	//invLocCol[0] = new nlobjSearchColumn('itemid');
	actItem[0]='F';
	var invitemRes = nlapiSearchRecord('item',null,invitemfilters,invLocCol);
	if(invitemRes!=null && invitemRes!='')
	{

		//actItem=invitemRes[0].getValue('externalid');

		actItem[1]=invitemRes[0].getId();
		//actItem[2]=invitemRes[0].getText('custitem_djn_internal_thumbnail');
	}
	else
	{

		nlapiLogExecution('ERROR', 'inSide UPCCODE',itemNo);

		var invLocfilters = new Array();
		invLocfilters[0] = new nlobjSearchFilter('upccode', null, 'is', itemNo);

		var invtRes = nlapiSearchRecord('item', null, invLocfilters, invLocCol);

		if(invtRes!=null && invtRes!='')
		{

			//actItem=invtRes[0].getValue('externalid');
			actItem[1]=invtRes[0].getId();
		}
		else
		{

			nlapiLogExecution('ERROR', 'inSide SKUALIAS',itemNo);

			var skuAliasFilters = new Array();
			skuAliasFilters.push(new nlobjSearchFilter('name', null, 'is', itemNo));

			var skuAliasCols = new Array();
			skuAliasCols[0] = new nlobjSearchColumn('custrecord_ebiz_item');

			var skuAliasResults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, skuAliasFilters, skuAliasCols);

			if(skuAliasResults != null)
			{

				actItem[0]='T';
				actItem[1]=skuAliasResults[0].getValue('custrecord_ebiz_item');
				/*var Itype = nlapiLookupField('item', actItem, 'recordType');
				var ItemRec = nlapiLoadRecord(Itype, actItem);			
				actItem=	ItemRec.getFieldValue('itemid');*/
			}

		}			
	}



	return actItem;
}
function eBiz_RF_GetItemCubeForItemAlias(itemID){
	nlapiLogExecution('ERROR', 'eBiz_RF_GetItemCubeForItem', 'Start');

	var BaseUOM=eBiz_RF_GetUOMAgainstItemAlias(itemID);
	var ItemInfo=new Array();
	var itemCube = 0;
	var BaseUomQty=0;
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemID);
	filters[1] = new nlobjSearchFilter('custrecord_ebizuomskudim', null, 'anyof', BaseUOM);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
	columns[1] = new nlobjSearchColumn('custrecord_ebizqty');

	var itemDimSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

	if(itemDimSearchResults != null && itemDimSearchResults.length > 0){
		itemCube = itemDimSearchResults[0].getValue('custrecord_ebizcube');
		//code added on 13 feb 2012 by suman
		//To get the baseuom qty .
		BaseUomQty=itemDimSearchResults[0].getValue('custrecord_ebizqty');
		//end of code as of 13 feb 2012.
	}
	ItemInfo.push(itemCube);
	ItemInfo.push(BaseUomQty);
	nlapiLogExecution('ERROR', 'Retrieved Item Cube', 'Item Cube = ' + itemCube);
	nlapiLogExecution('ERROR', 'Retrieved BaseUomQty', 'BaseUomQty = ' + BaseUomQty);
	nlapiLogExecution('ERROR', 'eBiz_RF_GetItemCubeForItem', 'End');

	return ItemInfo;
}
function eBiz_RF_GetUOMAgainstItemAlias(itemID)
{
	nlapiLogExecution('ERROR', 'eBiz_RF_GetUOMAgainstItemAlias', 'Start');

	var baseuom = "";

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_item', null, 'anyof', itemID));
	/*if(location!=null && location!='')
		filters.push(new nlobjSearchFilter('location', null, 'anyof', ['@NONE@',location]));*/

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_uom');

	var itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, filters, columns);

	if(itemSearchResults != null)
		baseuom = itemSearchResults[0].getValue('custrecord_ebiz_uom');

	var logMsg = 'baseuom = ' + baseuom;
	nlapiLogExecution('ERROR', 'baseuom Retrieved', logMsg);
	nlapiLogExecution('ERROR', 'eBiz_RF_GetUOMAgainstItemAlias', 'End');

	return baseuom;
}
/**
 * 
 * @param skuNo
 * @param uom
 * @returns {dimArray0}
 */
function getSKUCubeAndWeight(skuNo, uom){


	var timestamp1 = new Date();
	var cube = 0;
	var BaseUOMQty = 0;

	var dimArray = new Array();
	if (uom == "")
		uom = 1;



	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', skuNo));
	filters.push(new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
	columns[1] = new nlobjSearchColumn('custrecord_ebizqty');

	var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

	if(skuDimsSearchResults != null){
		for (var i = 0; i < skuDimsSearchResults.length; i++) {
			var skuDim = skuDimsSearchResults[i];
			cube = skuDim.getValue('custrecord_ebizcube');
			BaseUOMQty = skuDim.getValue('custrecord_ebizqty');
		}
	}

	//dimArray["BaseUOMItemCube"] = parseFloat(cube);
	//dimArray["BaseUOMQty"] = BaseUOMQty;
	dimArray[0] = parseFloat(cube);//BaseUOMItemCube
	dimArray[1] = BaseUOMQty;//BaseUOMQty

	var timestamp2 = new Date();


	return dimArray;
}