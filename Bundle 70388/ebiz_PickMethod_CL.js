/***************************************************************************
�����������������������������eBizNET
����������������������eBizNET Solutions Inc
 ****************************************************************************
 *
 *� $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Masters/Client/ebiz_PickMethod_CL.js,v $
 *� $Revision: 1.4.4.2.10.1 $
 *� $Date: 2013/10/10 15:29:09 $
 *� $Author: rmukkera $
 *� $Name: t_NSWMS_2013_1_10_16 $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_PickMethod_CL.js,v $
 *� Revision 1.4.4.2.10.1  2013/10/10 15:29:09  rmukkera
 *� Case# 20124675
 *�
 *� Revision 1.4.4.2  2012/02/09 16:10:57  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Physical and Virtual Location changes
 *�
 *� Revision 1.6  2012/02/09 14:43:43  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Physical and Virtual Location changes
 *�
 *� Revision 1.5  2012/01/23 09:45:48  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Added code to trim the space at right in name field.
 *�
 ****************************************************************************/

/**
 * @param type
 * @returns {Boolean}
 */
function fnCheckPickMethodEntry(type)
{
	var varPickMethodname = nlapiGetFieldValue('name');
	var varPickMethod = nlapiGetFieldValue('custrecord_ebizmethodid');    
	var vSite = nlapiGetFieldValue('custrecord_ebizsitepick');
	var vCompany = nlapiGetFieldValue('custrecord_ebizcompanypickm');

	/*
	 * Added on 1/4/2011 by Phani
	 * The below variable will fetch the internal id of the record selected.
	 */    
	if(varPickMethodname != "" && varPickMethodname != null && varPickMethod != "" && varPickMethod != null && vSite != "" && vSite != null && vCompany != ""  && vCompany != null)
	{
		var vId = nlapiGetFieldValue('id');

		var filters = new Array();
		filters[0] = new nlobjSearchFilter('name', null, 'is', varPickMethodname.replace(/\s+$/, ""));//Added by suman on 09/01/12 to trim spaces at right.
		filters[1] = new nlobjSearchFilter('custrecord_ebizmethodid', null, 'is', varPickMethod);    
		filters[2] = new nlobjSearchFilter('custrecord_ebizsitepick', null, 'anyof', ['@NONE@',vSite]);
		filters[3] = new nlobjSearchFilter('custrecord_ebizcompanypickm', null, 'anyof', ['@NONE@',vCompany]);

		/*
		 * Added on 1/4/2011 by Phani
		 * The below variable will filter based on the internal id of the record selected.
		 */
		if (vId != null && vId != "") {
			filters[4] = new nlobjSearchFilter('internalid', null, 'noneof', vId);
		}

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_pick_method', null, filters);

		if (searchresults != null && searchresults.length > 0) 
		{
			alert("The Pick Method # already exists.");
			return false;
		}  
		var createCluster=nlapiGetFieldValue('custrecord_ebizcreatecluster');
		var noOfMaxOrders=nlapiGetFieldValue('custrecord_ebiz_pickmethod_maxorders');
		var maxPicks=nlapiGetFieldValue('custrecord_ebiz_pickmethod_maxpicks');
		
		if(createCluster=='T' && (noOfMaxOrders==null || noOfMaxOrders=='') && (maxPicks==null || maxPicks==''))
			{
			alert("Since you have enabled the cluster please enter either maxorders or maxpicks.");
			return false;
			}
		return true;  
	}
	else
	{
		return true;
	}
}


/**
 * @param type
 * @param name
 * @returns {Boolean}
 */
function fnCheckPickMethodEdit(type,name)
{
	var vId = nlapiGetFieldValue('id');
	var varPickMethod = nlapiGetFieldValue('custrecord_ebizmethodid');    
	var vSite = nlapiGetFieldValue('custrecord_ebizsitepick');
	var vCompany = nlapiGetFieldValue('custrecord_ebizcompanypickm');

	// Added by Phani on 1/4/2011 as it was missing
	var varPickMethodname = nlapiGetFieldValue('name');

	if(varPickMethodname != "" && varPickMethodname != null && vId != "" && vId != null && varPickMethod != "" && varPickMethod != null && vSite != "" && vSite != null && vCompany != ""  && vCompany != null)
	{
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('name', null, 'is', varPickMethodname.replace(/\s+$/, ""));//Added by suman on 09/01/12 to trim spaces at right.
		filters[1] = new nlobjSearchFilter('custrecord_ebizmethodid', null, 'is', varPickMethod);    
		filters[2] = new nlobjSearchFilter('custrecord_ebizsitepick', null, 'anyof', ['@NONE@',vSite]);
		filters[3] = new nlobjSearchFilter('custrecord_ebizcompanypickm', null, 'anyof', ['@NONE@',vCompany]);
		filters[4] = new nlobjSearchFilter('internalid', null, 'noneof',vId);    

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_pick_method', null, filters);

		if (searchresults != null && searchresults.length > 0) 
		{
			alert("The Pick Method already exists.");
			return false;
		}
		var createCluster=nlapiGetFieldValue('custrecord_ebizcreatecluster');
		var noOfMaxOrders=nlapiGetFieldValue('custrecord_ebiz_pickmethod_maxorders');
		var maxPicks=nlapiGetFieldValue('custrecord_ebiz_pickmethod_maxpicks');
		
		if(createCluster=='T' && (noOfMaxOrders==null || noOfMaxOrders=='') && (maxPicks==null || maxPicks==''))
			{
			alert("Since you have enabled the cluster please enter either maxorders or maxpicks.");
			return false;
			}
		return true;   
	}
	else
	{
		return true;
	}
}
