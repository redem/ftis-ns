/***************************************************************************
       eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/ebiz_BOM_ReportPDF_SL.js,v $
 *     	   $Revision: 1.3.2.1.4.3.4.6 $
 *     	   $Date: 2014/08/04 15:19:33 $
 *     	   $Author: skreddy $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_BOM_ReportPDF_SL.js,v $
 * Revision 1.3.2.1.4.3.4.6  2014/08/04 15:19:33  skreddy
 * case # 20149745
 * jawbone SB issue fix
 *
 * Revision 1.3.2.1.4.3.4.5  2014/07/25 13:37:15  skreddy
 * case # 20149396
 * jawbone SB issue fix
 *
 * Revision 1.3.2.1.4.3.4.4  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.3.2.1.4.3.4.3  2013/03/08 14:43:37  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.3.2.1.4.3.4.2  2013/03/01 14:34:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.3.2.1.4.3.4.1  2013/02/27 13:06:28  rmukkera
 * monobid production bundle changes were merged to cvs with tag
 * t_eBN_2013_1_StdBundle_2
 *
 * Revision 1.3.2.1.4.3  2013/01/25 06:50:28  skreddy
 * CASE201112/CR201113/LOG201121
 * Added Expiry Date
 *
 * Revision 1.3.2.1.4.2  2012/11/01 14:54:57  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.3.2.1.4.1  2012/10/26 08:02:57  gkalla
 * CASE201112/CR201113/LOG201121
 * Lot# exception enhancement in work orders
 *
 * Revision 1.10  2012/02/01 09:41:32  gkalla
 * CASE201112/CR201113/LOG201121
 * Work order Bin location issues fixed
 *
 * Revision 1.9  2012/01/31 12:42:28  gkalla
 * CASE201112/CR201113/LOG201121
 * Work order Bin location issues fixed
 *
 * Revision 1.6  2012/01/17 17:25:12  gkalla
 * CASE201112/CR201113/LOG201121
 * changed the order of data columns in PDF report
 *
 * Revision 1.5  2012/01/17 15:18:36  gkalla
 * CASE201112/CR201113/LOG201121
 * changed the order of data columns in PDF report
 *
 * Revision 1.4  2012/01/17 09:13:27  gkalla
 * CASE201112/CR201113/LOG201121
 * changed the order of data columns in PDF report
 *
 * Revision 1.3  2012/01/04 13:41:59  gkalla
 * CASE201112/CR201113/LOG201121
 * For WO allocation
 *
 * Revision 1.2  2011/12/30 20:36:41  gkalla
 * CASE201112/CR201113/LOG201121
 * For WO allocation
 *
 * Revision 1.1  2011/12/30 16:57:00  gkalla
 * CASE201112/CR201113/LOG201121
 * For WO allocation
 *
 * Revision 1.31  2011/12/23 14:06:10  gkalla
 * CASE201112/CR201113/LOG201121
 * Added barcode and dynamic logo
 *
 * Revision 1.30  2011/12/23 12:45:10  gkalla
 * CASE201112/CR201113/LOG201121
 * Added barcode and dynamic logo
 *
 *****************************************************************************/
/**
 * This is the main function to print the BOM LIST against the given WO number  
 */
function BOMReportPDFSuitelet(request, response){
	if (request.getMethod() == 'GET') {

		var form = nlapiCreateForm('WO Pick Report'); 
		var woid=request.getParameter('custparam_wo_no');

		nlapiLogExecution('ERROR','woid',woid);

		if(woid!=null && woid!='')
		{
			nlapiLogExecution('ERROR','INTOwoid1',woid);
			var searchresults = nlapiLoadRecord('workorder', woid);
			if(searchresults!=null && searchresults!='') 
			{ 
				var qty = searchresults.getFieldValue('quantity');    	     
				var itemid=searchresults.getFieldValue('assemblyitem');
				var itemText=searchresults.getFieldText('assemblyitem');
				var WoCreatedDate=searchresults.getFieldValue('trandate');
				var WoDueDate=searchresults.getFieldValue('custbody_ebiz_woduedate');
				var WoExpcompletionDate=searchresults.getFieldValue('custbody_ebiz_expcompletiondate');
				var itemQty=searchresults.getFieldValue('quantity');
				var itemStatus=searchresults.getFieldValue('status');
				var itemLocation=searchresults.getFieldValue('location');
				var WOName=searchresults.getFieldValue('tranid');
				var assmblyinstruction=searchresults.getFieldValue('custitem_assmblyinstr');
				var vMemo=searchresults.getFieldValue('memo');
				var built='';
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('internalid', null, 'is', woid);
				filters[1] = new nlobjSearchFilter('mainline', null, 'is', 'T');//ship task


				var columns = new Array();
				columns[0] = new nlobjSearchColumn('quantityshiprecv');

				var searchresultsWaveNo = nlapiSearchRecord('workorder', null, filters, columns);
				if(searchresultsWaveNo!=null && searchresultsWaveNo!=''){
					built = searchresultsWaveNo[0].getValue('quantityshiprecv');
				}
				nlapiLogExecution('ERROR','qty',qty);
				nlapiLogExecution('ERROR','built',built);
				if(built==null || built =="")
					built=0;
				qty = parseInt(qty) - parseInt(built);
				nlapiLogExecution('ERROR','qty',qty);
				if(assmblyinstruction==null||assmblyinstruction=="")
					assmblyinstruction="";
				var itemLocationtext="";
				if(itemLocation == null || itemLocation =="")
				{
					itemLocation="&nbsp;";
				}
				else
				{

					var searchresultslocation = nlapiLoadRecord('location', itemLocation);
					if(searchresultslocation!=null && searchresultslocation!='') 
					{
						itemLocationtext=searchresultslocation.getFieldValue('name');
					}
				}
				var url;
				/*var ctx = nlapiGetContext();
				nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
				if (ctx.getEnvironment() == 'PRODUCTION') 
				{
					url = 'https://system.netsuite.com';			
				}
				else if (ctx.getEnvironment() == 'SANDBOX') 
				{
					url = 'https://system.sandbox.netsuite.com';				
				}*/
				nlapiLogExecution('ERROR', 'PDF URL',url);	
				var filefound = nlapiLoadFile('Images/LOGOCOMP.jpg'); 
				if (filefound) 
				{ 
					nlapiLogExecution('ERROR', 'Event', 'file;'+filefound.getId()); 
					var imageurl = filefound.getURL();
					nlapiLogExecution('ERROR','imageurl',imageurl);
					//var finalimageurl = url + imageurl;//+';';
					var finalimageurl = imageurl;//+';';
					//finalimageurl=finalimageurl+ '&expurl=T;';
					nlapiLogExecution('ERROR','imageurl',finalimageurl);
					finalimageurl=finalimageurl.replace(/&/g,"&amp;");

				} 
				else 
				{
					nlapiLogExecution('ERROR', 'Event', 'No file;');
				}

				//date
				var TempArray=new Array();
				var sysdate=DateStamp();
				var systime=TimeStamp();
				var Timez=calcTime('-5.00');
				nlapiLogExecution('ERROR','TimeZone',Timez);
				var datetime= new Date();
				datetime=datetime.toLocaleTimeString() ;

				var filterOpentask = new Array();
				filterOpentask[0] = new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', woid);
				filterOpentask[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]);

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_line_no', null, 'group');
				columns[1] = new nlobjSearchColumn('custrecord_sku', null, 'group');
				columns[2] = new nlobjSearchColumn('custrecord_expe_qty', null ,'sum');
				columns[3] = new nlobjSearchColumn('custrecord_lpno', null, 'group');
				columns[4] = new nlobjSearchColumn('custrecord_batch_no', null, 'group');
				columns[5] = new nlobjSearchColumn('custrecord_actbeginloc', null, 'group');
				columns[6] = new nlobjSearchColumn('custrecord_act_qty', null, 'group');                                    
				columns[7] = new nlobjSearchColumn('custrecord_expirydate', null, 'group');				
				columns[8] = new nlobjSearchColumn('custrecord_ebiz_zoneid', null, 'group');
				columns[0].setSort();
				var Opentaskresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterOpentask,columns);
				if(Opentaskresults!=null && Opentaskresults!="")
				{

					var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<head><macrolist><macro id='myfooter'><p align='center'>Page <pagenumber/> of <totalpages/></p></macro></macrolist></head><body  font-size=\"12\"  size=\"A4-landscape\"    padding-top=\" 0mm\"   footer='myfooter' footer-height='20mm'>\n";
					//var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<head></head><body  font-size=\"7\"  size=\"A4-landscape\"    padding-top=\" 0mm\"   footer='myfooter' footer-height='20mm'>\n";
//					nlapiLogExecution('ERROR','xml0 ',"<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body font-size=\"7\">\n");

					var LineArray=new Array();
					var LineCount=new Array();
					//var vCount=1;
					var vLineCount=searchresults.getLineItemCount('item');
					var count=0;
					for (k=0; k<Opentaskresults.length; k++) 
					{var LineNo =Opentaskresults[k].getValue('custrecord_line_no', null, 'group');
					if(LineArray.indexOf(LineNo)==-1)
					{

						LineArray.push(LineNo);
						LineCount.push(1);
					}
					else
					{
						var c=LineArray.indexOf(LineNo);
						LineCount[c]=parseFloat(LineCount[c]+1);

					}
					}
					nlapiLogExecution('ERROR', 'LineArray',LineArray.length);
					nlapiLogExecution('ERROR', 'LineCount',LineCount.length);
					nlapiLogExecution('ERROR', 'LineCount',LineCount);


				/*** The below code is merged from Endochoice account on 08thMar13 by Santosh as part of Standard bundle***/
				// addeded  '\.' character to replaceChar
					var replaceChar =/\$|,|@|#|~|`|\%|\*|\^|\&|\+|\=|\-|\_|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|\./g;
				/*** up to here ***/
					var strxml= "";



					var rowcount=0;

					var totalcount=Opentaskresults.length;
					nlapiLogExecution('ERROR','totalcount',totalcount);
					while(0<totalcount)
					{
						var vcount=0;

						strxml += "<table width='100%' >";
						strxml += "<tr><td><table width='100%' >";
						strxml += "<tr ><td valign='middle' align='left'><img src='" + finalimageurl + "'></img></td><td valign='middle' align='left'  style='font-size:xx-large;'>";
						strxml += "WO Pick Report ";
						strxml += "</td><td align='right'>&nbsp;</td></tr></table></td></tr>";
						strxml += "<tr><td><table width='100%' >";
						strxml += "<tr><td style='width:41px;'>";
						strxml += "<p align='right'>Date/Time:"+Timez+"</p>"; 
						strxml += "</td></tr>";
						strxml += "</table>";

						strxml += "</td></tr>";
						strxml += "<tr><td><table width='100%' valign='top'>";
						strxml += "<tr><td>";
						strxml +="<table align='left' style='width:100%;' border='1'>";
						strxml +="<tr><td align='right' style='width:41px'>WO# :</td>"; 
						strxml +="<td>";
						strxml += WOName;
						strxml +="</td>";
						strxml +="<td align='right' style='width:51px'>Item :</td>"; 

						strxml +="<td>";
						/* code merged from monobid bundle to replace the characeters on feb 27th 2013nby Radhika */
						strxml += itemText.replace(replaceChar,'');
						strxml +="</td>";
						strxml +="<td align='right' style='width:51px'>Qty :</td>"; 

						strxml +="<td>";
						strxml += qty;
						strxml +="</td></tr>";
						strxml +="<tr><td align='right' style='width:41px'>Location :</td>"; 
						strxml +="<td>";
						strxml += itemLocationtext;
						strxml +="</td>";
						strxml +="<td align='right' style='width:51px'>Created Date :</td>"; 

						strxml +="<td>";
						strxml += WoCreatedDate;
						strxml +="</td>";
						strxml +="<td align='right' style='width:51px'>Due Date :</td>"; 

						strxml +="<td>";
						if(WoDueDate != null && WoDueDate != "")
							strxml += WoDueDate;
						strxml +="</td></tr>";
						strxml +="<tr><td align='right' style='width:41px'>Expected Completion date :</td>"; 
						strxml +="<td>";
						if(WoExpcompletionDate != null && WoExpcompletionDate != "")
							strxml += WoExpcompletionDate;
						strxml +="</td>";
						strxml +="<td align='right' style='width:51px'>Item Status:</td>"; 

						strxml +="<td>";
						strxml += itemStatus;
						strxml +="</td>";
						strxml +="<td align='right' style='width:51px'>Assembly Instructions :</td>"; 

						strxml +="<td>";
						strxml += assmblyinstruction;
						strxml +="</td></tr>";
						strxml += "<tr><td align='right' style='width:51px'>Memo :</td>"; 

						strxml +="<td>";
						if(vMemo != null && vMemo != '')
							strxml += vMemo;
						strxml +="</td></tr>";
						/*strxml +="<tr><td align='right'>Is Printed :</td>";
				strxml +="<td>";
				strxml += searchresults.getFieldValue('custbody_ebiz_isprinted');
				strxml +="</td><td align='right'>No. of Times Printed :</td>";
				strxml +="<td>";
				strxml +=searchresults.getFieldValue('custbody_ebiz_printcount');
				strxml +="</td><td></td></tr>";*/
						strxml +="</table>";
						strxml += "</td></tr>";
						strxml += "</table>";

						strxml += "</td></tr>";

						strxml += "<tr><td><table width='100%'>";
						strxml += "<tr><td>";

						strxml +="<table  width='100%'>";
						strxml +="<tr  style=\"font-weight:bold;background-color:gray;color:white;padding:0;\">";

						strxml += "<td width='23%' height='1%' style='border-width: 1px; border-color: #000000;font-size:10pt;'>";
						strxml += "Item";
						strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

						strxml += "<td width='23%' height='1%' style='border-width: 1px; border-color: #000000;font-size:10pt;'>";
						strxml += " Description";
						strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

						strxml += "<td width='10%' height='1%' style='border-width: 1px; border-color: #000000;font-size:10pt;'>";
						strxml += "Qty";
						strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

						strxml += "<td width='10%' height='1%' style='border-width: 1px; border-color: #000000;font-size:10pt;'>";
						strxml += "Serial/Lot Numbers";	
						strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

						strxml += "<td width='8%' height='1%' style='border-width: 1px; border-color: #000000;font-size:10pt;'>";
						strxml += "LP#";
						strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

						strxml += "<td width='9%' height='1%' style='border-width: 1px; border-color: #000000;font-size:10pt;'>";//uncommented
						strxml += "Bin Location";
						strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>"; 

						strxml += "<td width='7%' height='1%' style='border-width: 1px; border-color: #000000;font-size:10pt;white-space:nowrap;'>";
						strxml += "<p> Pick Allocated Qty </p>";
						strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

						strxml += "<td width='7%' height='1px' style='border-width: 1px; border-color: #000000;font-size:10pt;white-space:nowrap;'>";
						strxml += "<p> BOM Qty </p>";
						strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>"; 
						strxml += "<td width='7%' height='1px' style='border-width: 1px; border-color: #000000;font-size:10pt;white-space:nowrap;'>";
						strxml += "<p> Expiration Date </p>";
						strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>"; 
						strxml += "<td width='7%' height='1px' style='border-width: 1px; border-color: #000000;font-size:10pt;white-space:nowrap;'>";
						strxml += "<p> Zone Id </p>";
						strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>"; 

						strxml =strxml+  "</tr>";


						var previtem1="";
						var previtem2="";
						for (m=rowcount; m<Opentaskresults.length; m++) 
						{

							vcount++;
							rowcount++;
							var WOBackOrdQty="";
							var WOCommittedQty="";
							var WOBomQty="";
							var WOItemDesc="";
							var WOLineQty=0;
							var ItemDesc="";


							nlapiLogExecution('ERROR', 'm',m);
							nlapiLogExecution('ERROR', 'previtem1',previtem1);
							nlapiLogExecution('ERROR', 'Opentaskresults.length',Opentaskresults.length);

							var lineItem=Opentaskresults[m].getValue('custrecord_sku', null, 'group');
							var itemtext=Opentaskresults[m].getText('custrecord_sku', null, 'group');
							var LineNo =Opentaskresults[m].getValue('custrecord_line_no', null, 'group');
							var LineQty=Opentaskresults[m].getValue('custrecord_expe_qty', null ,'sum');
							var LineBinLOcation=Opentaskresults[m].getText('custrecord_actbeginloc', null, 'group');
							var LineBinLOcationVal=Opentaskresults[m].getValue('custrecord_actbeginloc', null, 'group');
							var LineLP=Opentaskresults[m].getValue('custrecord_lpno', null, 'group');
							var LineQuantity=Opentaskresults[m].getValue('custrecord_act_qty', null, 'group');
							var LineLotNo=Opentaskresults[m].getValue('custrecord_batch_no', null, 'group');
							var ExpiryDate=Opentaskresults[m].getValue('custrecord_expirydate', null, 'group');
							var ZoneId=Opentaskresults[m].getText('custrecord_ebiz_zoneid', null, 'group');
							nlapiLogExecution('ERROR', 'ExpiryDate',ExpiryDate);
							if(LineLotNo== null || LineLotNo ==""||LineLotNo =="- None -")
								LineLotNo="";
							if(ZoneId== null || ZoneId =="")
								ZoneId="";
							var vNextLineNo;
							if(m!=Opentaskresults.length-1)
								vNextLineNo =Opentaskresults[m+1].getValue('custrecord_line_no', null, 'group');
							for(var i=1;i<=vLineCount;i++)
							{	
								var WOLineNo =searchresults.getLineItemValue('item','line',i)+".0";
								if(WOLineNo== null || WOLineNo =="")
									WOLineNo="&nbsp;";

								var WOlineItemvalue = searchresults.getLineItemValue('item', 'item', i);

								var WOlineItem = searchresults.getLineItemText('item', 'item', i);
								if(WOlineItem== null || WOlineItem =="")
									WOlineItem="";					

								/*var LineAvailQty =searchresults.getLineItemValue('item','custcol_ebizwoavalqty',i);
							if(LineAvailQty== null || LineAvailQty =="")
								LineAvailQty="&nbsp;";*/



//								var ItemDesc = searchresults.getLineItemValue('item', 'description', i);
//								if(ItemDesc== null || ItemDesc =="")
//								ItemDesc="";

								var BackOrdQty = 0;
								if(searchresults.getLineItemValue('item', 'quantitybackordered', i) != null && searchresults.getLineItemValue('item', 'quantitybackordered', i) != "")
									BackOrdQty=parseFloat(searchresults.getLineItemValue('item', 'quantitybackordered', i));

								var CommittedQty = searchresults.getLineItemValue('item', 'quantitycommitted', i);
								if(CommittedQty== null || CommittedQty =="" || CommittedQty == 'undefined')
									CommittedQty="";
								nlapiLogExecution('ERROR', 'BackOrdQty',BackOrdQty);
								nlapiLogExecution('ERROR', 'WOLineNo',WOLineNo);
								nlapiLogExecution('ERROR', 'LineNo',LineNo);
								nlapiLogExecution('ERROR', 'WOlineItemvalue',WOlineItemvalue);
								nlapiLogExecution('ERROR', 'lineItem',lineItem);


								if(parseFloat(WOLineNo)==parseFloat(LineNo) && parseFloat(WOlineItemvalue)==parseFloat(lineItem))
								{
									WOLineQty = searchresults.getLineItemValue('item','quantity',i);
									if(WOLineQty== null || WOLineQty =="")
										WOLineQty="&nbsp;";

									var BomQty =WOLineQty/itemQty;
									if(BomQty== null || BomQty =="")
										BomQty="&nbsp;";

									WOBackOrdQty=BackOrdQty;
									WOCommittedQty=CommittedQty;
									WOBomQty=BomQty;
									ItemDesc = searchresults.getLineItemValue('item', 'description', i);
									if(ItemDesc== null || ItemDesc =="")
										ItemDesc="";
									nlapiLogExecution('ERROR', 'a9_ItemDesc',ItemDesc);
									nlapiLogExecution('ERROR', 'a9','a9');

								}
							}

							strxml =strxml+  "<tr>";
							strxml += "<td width='7%' style='border-width: 1px; border-color: #000000; font-size:10pt;'>";
							/* code merged from monobid to replace the special chars wth space on feb 27th 2013 by Radhika */						
							strxml += itemtext.replace(replaceChar,'');
							strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

							strxml += "<td width='8%' style='border-width: 1px; border-color: #000000; font-size:10pt;'>";
							if(ItemDesc != null || ItemDesc != "")
								strxml += ItemDesc.replace(replaceChar,''); 
							strxml += "</td>";

							var index=LineArray.indexOf(LineNo);
							var count=parseFloat(LineCount[index]);
							if(count>1 && previtem1!=lineItem)
							{

								nlapiLogExecution('ERROR', 'count',count);
								nlapiLogExecution('ERROR', 'index',index);
								strxml += "<td width='8%' style='border-width: 1px; border-color: #000000; font-size:10pt;' rowspan='"+ count +"'>";
								strxml += WOLineQty;
								strxml += "</td>";
								previtem1=lineItem;

							}
							else if(count==1 && previtem1!=lineItem)
							{
								strxml += "<td width='8%' style='border-width: 1px; border-color: #000000; font-size:10pt;'>";
								strxml += WOLineQty;
								strxml += "</td>";
								previtem1=lineItem;
							}

							strxml += "<td width='8%' style='border-width: 1px; border-color: #000000; font-size:10pt;'><p>";
							strxml += LineLotNo;
							strxml += "</p></td>";

							strxml += "<td width='8%' style='border-width: 1px; border-color: #000000; font-size:10pt;'>";
							strxml += LineLP;
							strxml += "</td>";

							strxml += "<td width='8%' style='border-width: 1px; border-color: #000000; font-size:10pt;'>";
							strxml += LineBinLOcation;
							strxml += "</td>";

							strxml += "<td width='8%' style='border-width: 1px; border-color: #000000; font-size:10pt;'>";
							strxml += LineQty;
							strxml += "</td>";

							if(count>1 && previtem2!=lineItem)
							{
								strxml += "<td width='8%' style='border-width: 1px; border-color: #000000; font-size:10pt;' rowspan='"+ count +"'>";
								strxml += WOBomQty; 
								strxml += "</td>";
								previtem2=lineItem;
							}
							else if(count==1 && previtem2!=lineItem)
							{
								strxml += "<td width='8%' style='border-width: 1px; border-color: #000000; font-size:10pt;'>";
								strxml += WOBomQty; 
								strxml += "</td>";
								previtem2=lineItem;
							}

							strxml += "<td width='8%' style='border-width: 1px; border-color: #000000; font-size:10pt;'>";
							strxml += ExpiryDate;
							strxml += "</td>";		

							strxml += "<td width='8%' style='border-width: 1px; border-color: #000000; font-size:10pt;'>";
							strxml += ZoneId;
							strxml += "</td>";

							strxml += "</tr>";
							if(vNextLineNo!= null && vNextLineNo != '' && vNextLineNo != LineNo)
							{	
								var Nextindex=LineArray.indexOf(vNextLineNo);
								var NextCount=parseFloat(LineCount[Nextindex]);
								if(vcount+NextCount>9)
									break;
							}
							if(vcount==9)
							{
								break;
							}

						}

						strxml =strxml+"</table>";

						strxml += "</td></tr>";
						strxml += "</table>";

						strxml += "</td></tr>";

						strxml += "</table>";

						totalcount=parseFloat(totalcount)-parseFloat(vcount);
						if(rowcount==Opentaskresults.length)
						{
							strxml +="<p style=\" page-break-after:avoid\"></p>";
						}
						else
						{
							strxml +="<p style=\" page-break-after:always\"></p>";
						}


					}	
					strxml =strxml+ "\n</body>\n</pdf>";
					xml=xml +strxml;

					//nlapiLogExecution('ERROR','xml',xml);

					searchresults.setFieldValue('custbody_ebiz_isprinted', 'T');
					searchresults.setFieldValue('custbody_ebiz_update_confirm','T');
					var vPrintCount=searchresults.getFieldValue('custbody_ebiz_printcount');

					if(vPrintCount != null && vPrintCount != "")
					{
						searchresults.setFieldValue('custbody_ebiz_printcount', parseFloat(vPrintCount) + 1);

					}
					else
					{
						searchresults.setFieldValue('custbody_ebiz_printcount', '1');
					}
					nlapiLogExecution('ERROR', 'vPrintCount ', vPrintCount);
					var CommittedId = nlapiSubmitRecord(searchresults, true);
					nlapiLogExecution('ERROR', 'CommittedId ', CommittedId);	

					var file = nlapiXMLToPDF(xml);	
					response.setContentType('PDF','WOPickReport.pdf');
					response.write( file.getValue() );
				}
			}
		}
	}
	else //this is the POST block
	{

	}
}

function SetPrintFlag(waveno,fullfillment)
{
	nlapiLogExecution('ERROR','GETwaveno',waveno);
	nlapiLogExecution('ERROR','GETfullfillment',fullfillment);
	var filter=new Array();

	if(waveno!=null && waveno!="")
		filter.push(new nlobjSearchFilter('custrecord_ebiz_wave','null','equalto',parseFloat(waveno)));

	if(fullfillment!=null&&fullfillment!="")
		filter.push(new nlobjSearchFilter('custrecord_lineord','null','is',fullfillment.toString()));

	var column=new Array();
	column[0]=new nlobjSearchColumn('custrecord_printflag');
	column[1]=new nlobjSearchColumn('custrecord_print_count');

	var searchrecord=nlapiSearchRecord('customrecord_ebiznet_ordline',null,filter,column);
	if(searchrecord!=null)
	{
		for ( var count = 0; count < searchrecord.length; count++) {
			var intId=searchrecord[count].getId();
			var flagcount=searchrecord[count].getValue('custrecord_print_count');
			var Newflagcount;
			if(flagcount==null || flagcount=="")
				Newflagcount=1;
			else
				Newflagcount=parseFloat(flagcount)+1;

			nlapiSubmitField('customrecord_ebiznet_ordline', intId, 'custrecord_printflag','T');
			nlapiSubmitField('customrecord_ebiznet_ordline', intId, 'custrecord_print_count',Newflagcount);
			nlapiSubmitField('customrecord_ebiznet_ordline', intId, 'custrecord_ebiz_pr_dateprinted',DateStamp());
		}
	}

}
function calcTime(offset) {

	//var vtime=new Date();

	//nlapiLogExecution('ERROR', 'TimeOffset', vtime.getTimezoneOffset());

	// create Date object for current location
	d = new Date();

	// convert to msec
	// add local time zone offset 
	// get UTC time in msec
	utc = d.getTime() + (d.getTimezoneOffset() * 60000);

	// create new Date object for different city
	// using supplied offset
	nd = new Date(utc + (3600000*offset));

	// return time as a string
	//return "The local time in " + city + " is " + nd.toLocaleString();

	var stringDt=((parseFloat(nd.getMonth()) + 1) + '/' + (parseFloat(nd.getDate())) + '/' + nd.getFullYear());

	var timestamp;
	var a_p = "";

	//Getting time in hh:mm tt format.
	var curr_hour = nd.getHours();
	var curr_min = nd.getMinutes();

	// determining the am/pm indicator
	if (curr_hour < 12)
		a_p = "am";
	else
		a_p = "pm";

	// finalizing hours depending on 24hr clock
	if (curr_hour == 0)
		curr_hour = 12;
	else if(curr_hour > 12)
		curr_hour -= 12;

	if (curr_min.length == 1)
		curr_min = "0" + curr_min;

	//Adding fields to update time zones.
	timestamp = curr_hour + ":" + curr_min + " " + a_p;

	return stringDt + " " + timestamp;


}


