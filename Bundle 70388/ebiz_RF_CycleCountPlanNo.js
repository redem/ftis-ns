/***************************************************************************
 eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_CycleCountPlanNo.js,v $
 *     	   $Revision: 1.3.4.2.4.2.4.11.2.1 $
 *     	   $Date: 2015/12/01 15:45:12 $
 *     	   $Author: aanchal $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_209 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 * 
 * REVISION HISTORY
 * $Log: ebiz_RF_CycleCountPlanNo.js,v $
 * Revision 1.3.4.2.4.2.4.11.2.1  2015/12/01 15:45:12  aanchal
 * 2015.2 Issue fIx
 * 201415688
 *
 * Revision 1.3.4.2.4.2.4.11  2015/04/14 09:59:18  snimmakayala
 * Case#:201411573
 * Add Item to cycle count plan even after completion of count
 *
 * Revision 1.3.4.2.4.2.4.10  2015/04/14 09:57:08  snimmakayala
 * Case#:201411573
 * Add Item to cycle count plan even after completion of count
 *
 * Revision 1.3.4.2.4.2.4.9  2014/12/24 12:18:08  snimmakayala
 * Case#: 201411302
 *
 * Revision 1.3.4.2.4.2.4.8.2.1  2014/12/23 12:30:59  snimmakayala
 * Case#: 201411302
 *
 * Revision 1.3.4.2.4.2.4.8  2014/06/13 10:06:42  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.3.4.2.4.2.4.7  2014/05/30 00:34:20  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.3.4.2.4.2.4.6  2014/05/19 06:28:15  skreddy
 * case # 20148197
 * Sonic SB issue fix
 *
 * Revision 1.3.4.2.4.2.4.5  2014/01/30 15:17:34  skavuri
 * case# 20127001,20127000 (for cyclecount planno through RF doubleclick throgh keyboard is working fine)
 *
 * Revision 1.3.4.2.4.2.4.4  2013/12/26 15:49:07  gkalla
 * case#20126566
 * Standard bundle issue
 *
 * Revision 1.3.4.2.4.2.4.3  2013/12/23 14:09:23  schepuri
 * 20126442
 *
 * Revision 1.3.4.2.4.2.4.2  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.3.4.2.4.2.4.1  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.3.4.2.4.2  2012/10/05 06:34:12  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting Multiple language into given suggestions
 *
 * Revision 1.3.4.2.4.1  2012/09/24 10:08:56  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.3.4.2  2012/03/16 14:08:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.3.4.1  2012/02/21 13:23:31  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.4  2012/02/21 11:44:22  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 */
function CycleCountPlanNo(request, response){
	if (request.getMethod() == 'GET') {

//		var ctx = nlapiGetContext();
//		var getLanguage = ctx.getPreference('LANGUAGE');
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);

		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);

		var st0,st1,st2,st3;


		if( getLanguage == 'es_ES')
		{
			st0 = "INVENTARIO C&#205;CLICO NO. DE PLAN";
			st1 = "INGRESAR NO. DE PLAN:";
			st2 = "CONT";
			st3 = "ANTERIOR";

		}
		else
		{
			st0 = "CYCLE COUNT PLAN NO";
			st1 = "ENTER PLAN NO:";
			st2 = "CONT";
			st3 = "PREV";

		}

		var functionkeyHtml=getFunctionkeyScript('_rf_cyclecountplan_no'); 
		var html = "<html><head><title>"+st0+"</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterplanno').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cyclecountplan_no' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st1;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterplanno' id='enterplanno' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "				<td align = 'left'>"+st2+" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";//case#20127000 for cyclecount planno through RF doubleclick throgh keyboard is working fine 
		html = html + "					"+st3+" <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterplanno').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {

		var getCycleCountPlanNo = request.getParameter('enterplanno');
		nlapiLogExecution('ERROR', 'getCycleCountPlanNo_equalto', getCycleCountPlanNo);
		var CCarray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		CCarray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR', 'getLanguage', CCarray["custparam_language"]);
		var st4;
		if( getLanguage == 'es_ES')
		{
			st4 = "CICLO DE VALIDEZ DEL PLAN NO CUENTAN";

		}
		else
		{
			st4 = "INVALID CYCLE COUNT PLAN NO";;
		}


		CCarray["custparam_error"] = st4;
		CCarray["custparam_screenno"] = '28';

//		case no 20126442
		//Case 20127001 starts
		/*if( getCycleCountPlanNo==null || getCycleCountPlanNo=='' || isNaN(getCycleCountPlanNo) )
		{
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
			return;
		}*/
		if(isNaN(getCycleCountPlanNo) )
		{
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
			return;
		}

		var filter=new Array();
		filter.push(new nlobjSearchFilter('custrecord_ebiz_processstatus',null,'is','Submitted'));
		filter.push(new nlobjSearchFilter('custrecord_ebiz_processname',null,'is','Cyclecount'));


		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebiz_processtranrefno');
		column[1]=new nlobjSearchColumn('id').setSort();

		var searchresultwavestatus=nlapiSearchRecord('customrecord_ebiz_schedulescripts_status',null,filter,column);
		var cyccscheduleplanno;
		if(searchresultwavestatus!=null && searchresultwavestatus!='')
		{
			cyccscheduleplanno=searchresultwavestatus[0].getValue('custrecord_ebiz_processtranrefno')
		}


		//case# 20127001 ends
		var filters = new Array();
		//filters[0] = new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto', getCycleCountPlanNo);
		//srikanth adding for 20126442
		filters[0] = new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [20]);
		if( getCycleCountPlanNo!=null || getCycleCountPlanNo!='')
			filters[1] = new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto', getCycleCountPlanNo);
		//srikanth adding for 20126442 close
		var vRoleLocation=getRoledBasedLocation();
		if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
		{
			filters[2] = new nlobjSearchFilter('custrecord_cyclesite_id', null, 'anyof', vRoleLocation);
		}
		if(cyccscheduleplanno!=null && cyccscheduleplanno!='')
			filters.push(new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'notequalto', cyccscheduleplanno));


		nlapiLogExecution('ERROR', 'Before Search Results Id', 'getSearchResultsId');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, null);
		nlapiLogExecution('ERROR', 'After Search Results Id', 'getSearchResultsId');


		var optedEvent = request.getParameter('cmdPrevious');

		if (optedEvent == 'F7') {
			response.sendRedirect('SUITELET', 'customscript_rf_inventorymenu', 'customdeploy_rf_inventorymenu_di', false, CCarray);
		}
		else {
			try {
				if (getCycleCountPlanNo != '') {

					var ccfilters = new Array();
					ccfilters.push(new nlobjSearchFilter('internalidnumber', null, 'equalto', parseInt(getCycleCountPlanNo)));

					var cccolumns = new Array();
					cccolumns[0] = new nlobjSearchColumn('custrecord_cyccplan_close');

					var ccsearchresults = nlapiSearchRecord('customrecord_ebiznet_cylc_createplan', null, ccfilters, cccolumns);
					nlapiLogExecution('ERROR', 'ccsearchresults', ccsearchresults);
					if(ccsearchresults==null || ccsearchresults=='')
						{
						CCarray["custparam_error"] = 'PLAN DOES NOT EXIST';
						CCarray["custparam_screenno"] = '28';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
						return;
						}
					if(ccsearchresults!=null && ccsearchresults!='')
					{
						var cyccclose = ccsearchresults[0].getValue('custrecord_cyccplan_close');
						nlapiLogExecution('ERROR', 'cyccclose', cyccclose);
						if(cyccclose=='T')
						{
							CCarray["custparam_error"] = 'This Cycle Count Plan is already closed.';
							CCarray["custparam_screenno"] = '28';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
							return;
						}
					}

					if (searchresults != null && searchresults.length > 0) {
						var getSearchResultsId = searchresults[0].getId();
						nlapiLogExecution('ERROR', 'Search Results Id', getSearchResultsId);

						CCarray["custparam_planno"] = getCycleCountPlanNo;
						CCarray["custparam_fetchedrecordid"] = getSearchResultsId;
						/*
                         var Rec = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe', getSearchResultsId);

                         CCarray["custparam_begin_location_id"] = Rec.getFieldValue('custrecord_cycact_beg_loc');
                         CCarray["custparam_begin_location_name"] = Rec.getFieldText('custrecord_cycact_beg_loc');

                         nlapiLogExecution('ERROR', 'Location Text', CCarray["custparam_begin_location_name"]);

                         CCarray["custparam_lpno"] = Rec.getFieldValue('custrecord_cyclelpno');
                         CCarray["custparam_exp_item_internalid"] = Rec.getFieldValue('custrecord_cyclecount_exp_ebiz_sku_no');
                         CCarray["custparam_exp_item_name"] = Rec.getFieldValue('custrecord_cyclesku');
                         CCarray["custparam_pack_code"] = Rec.getFieldValue('custrecord_cyclepc');
                         CCarray["custparam_exp_quantity"] = Rec.getFieldValue('custrecord_cycleexp_qty');
						 */
						response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_location', 'customdeploy_rf_cyclecount_location_di', false, CCarray);
					}
					else {
						CCarray["custparam_planno"] = getCycleCountPlanNo;
						nlapiLogExecution('ERROR', 'to Confirm screen', getCycleCountPlanNo);
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cycc_confirmadditem', 'customdeploy_ebiz_rf_cycc_confirmadditem', false, CCarray);
						return
					}
				}
				else {
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
					nlapiLogExecution('ERROR', 'Cycle Plan No. not entered', getCycleCountPlanNo);
				}
			} 
			catch (e) {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
				nlapiLogExecution('ERROR', 'Catching the Search Results', 'Length is null');
			}
			nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
		}
	}
}
