/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Inventory/Suitelet/ebiz_RF_CreateInventoryComplete.js,v $
 *     	   $Revision: 1.3.4.1.4.1.4.4 $
 *     	   $Date: 2014/06/13 10:25:08 $
 *     	   $Author: skavuri $
 *     	   $Name: t_NSWMS_2014_1_3_125 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 * 
* REVISION HISTORY
 * $Log: ebiz_RF_CreateInventoryComplete.js,v $
 * Revision 1.3.4.1.4.1.4.4  2014/06/13 10:25:08  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.3.4.1.4.1.4.3  2014/06/06 07:43:34  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.3.4.1.4.1.4.2  2014/05/30 00:34:18  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.3.4.1.4.1.4.1  2013/04/17 16:02:35  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.3.4.1.4.1  2012/09/21 15:01:30  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.3.4.1  2012/02/21 13:21:59  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.4  2012/02/21 11:40:36  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 */

function CreateInventoryComplete(request, response)
{
    if (request.getMethod() == 'GET') 
	{
    	
    	var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('ERROR', 'getLanguage', getLanguage);
	    
		var st0,st1,st2,st3;
		if( getLanguage == 'es_ES')
		{
			
			st0 = "CREAR INVENTARIO";
			st1 = "INVENTARIO DE CREACI&#211;N";
			st2 = "EXITOSA";
			st3 = "CONTINUAR";
					
		}
		else
		{
			st0 = "CREATE INVENTORY";
			st1 = "INVENTORY CREATION ";
			st2 = "SUCCESSFUL";
			st3 = "CONTINUE";

		}    	
    	
    	locationId = request.getParameter('custparam_locationId');
    	
    	var functionkeyHtml=getFunctionkeyScript('_rf_putaway_lp'); 
    	var html = "<html><head><title>" + st0 + "</title>";
    	html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
        html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
      //Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
       // html = html + " document.getElementById('cmdSend').focus();";        
        html = html + "</script>";
        html = html +functionkeyHtml;
      	 html = html + "</head><body onkeydown='OnKeyDown_CL();'>";
        html = html + "	<form name='_rf_putaway_lp' method='POST'>";
        html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
        html = html + "			<tr>";
        html = html + "				<td align = 'left'>" + st1 + "</td></tr>";
        html = html + "				<tr><td align = 'left'>" + st2 + " </td></tr>";
		html = html + "			<tr>";
		html = html + "				<input type='hidden' name='hdnLocationName' value=" + locationId+ ">";
	    html = html + "				<input type='hidden' name='hdnLocationInternalid' value=" + locationId+ ">";
	    html = html + "				<input type='hidden' name='hdnCompname' value=" + locationId + ">";
	    html = html + "			    	<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
        html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' id='cmdSend' type='submit' value='F8'/>";      
        html = html + "				</td>";
        html = html + "			</tr>";
        html = html + "		 </table>";
        html = html + "	</form>";
      //Case# 20148882 (added Focus Functionality for Textbox)
        html = html + "<script type='text/javascript'>document.getElementById('cmdSend').focus();</script>";
        html = html + "</body>";
        html = html + "</html>";
        
        response.write(html);
    }
    else 
	{
        nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is not null');
        
        // This variable is to hold the Quantity entered.
        var locationId = request.getParameter('hdnLocationInternalid');
        var CIarray = new Array();
        var getLanguage = request.getParameter('hdngetLanguage');
		CIarray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR', 'getLanguage', CIarray["custparam_language"]);
    			
		 CIarray["custparam_locationId"] = locationId;
        
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
        // to the previous screen.
        var optedEvent = request.getParameter('cmdSend');
        
        //	if the previous button 'F7' is clicked, it has to go to the previous screen 
        //  ie., it has to go to create inventory screen.
        if (optedEvent == 'F8') 
		{
			//response.sendRedirect('SUITELET', 'customscript_rf_create_invt_binloc', 'customdeploy_rf_create_invt_binloc_di', false, optedEvent);
        	response.sendRedirect('SUITELET', 'customscript_rf_create_invt_binloc', 'customdeploy_rf_create_invt_binloc_di', false, CIarray);
		}
        nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}
}
