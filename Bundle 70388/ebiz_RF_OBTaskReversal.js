/***************************************************************************
  		   eBizNET Solutions Inc               
 ***************************************************************************
 **     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_OBTaskReversal.js,v $
 *     	   $Revision: 1.1.2.2.4.4.4.8.2.1 $
 *     	   $Date: 2015/11/10 15:42:12 $
 *     	   $Author: rmukkera $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_OBTaskReversal.js,v $
 * Revision 1.1.2.2.4.4.4.8.2.1  2015/11/10 15:42:12  rmukkera
 * case # 201415283
 *
 * Revision 1.1.2.2.4.4.4.8  2014/08/14 14:55:55  skavuri
 * Case# 201410034 Std Bundle Issue Fixed
 *
 * Revision 1.1.2.2.4.4.4.7  2014/06/13 13:05:09  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.2.4.4.4.6  2014/05/30 00:41:02  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.2.4.4.4.5  2014/05/09 16:00:02  skavuri
 * Case # 20148342 SB Issue Fixed
 *
 * Revision 1.1.2.2.4.4.4.4  2013/11/28 06:06:31  skreddy
 * Case# 20125929
 * Afosa SB issue fix
 *
 * Revision 1.1.2.2.4.4.4.3  2013/10/24 13:06:07  rmukkera
 * Case# 20124841
 *
 * Revision 1.1.2.2.4.4.4.2  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.2.2.4.4.4.1  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.2.4.4  2012/12/17 23:00:13  kavitha
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal Process Issue fixes
 *
 * Revision 1.1.2.2.4.3  2012/11/30 23:56:48  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA UAT Fixes.
 *
 * Revision 1.1.2.2.4.2  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.1.2.2.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.1.2.2  2012/09/14 20:12:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production Issue Fixes for FISK and BOOMBAH.
 *
 * Revision 1.1.2.1  2012/06/25 14:54:17  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 * 
 *****************************************************************************/
function OBReversalByTask(request, response){
	nlapiLogExecution('DEBUG', 'Into Request', 'Into Request');

	if (request.getMethod() == 'GET') 
	{
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7;
		//case 20125929 start : for spanish conversion
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			//case 20125929 end
			st1 = "VENTAS DE PEDIDO # ";
			st2 = "Y / O ";
			//st3 = "CAJA DE CART&#211;N #";
			st3 = "CAJA #";
			st4 = "TEMA";
			st5 = "ENVIAR";
			st6 = "ANTERIOR";
			st8 = "INVERSI&#211;N POR TAREA INOUTBOUND";
			

		}
		else
		{
			st1 = "SALES/TRANSFER ORDER#/ VRA# ";// Case# 20148342
			st2 = "AND/OR ";
			st3 = "CARTON #";
			st4 = "ITEM";
			st5 = "SEND";
			st6 = "PREV";
			st8 = "OUTBOUND REVERSAL BY TASK";
			
			
		}
		
		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 		
		var html = "<html><head><title>"+ st8 +"</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterwaveno').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +"";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entersono' id='entersono' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st2 +"";
		html = html + "								   "+ st3 +"";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entercartonno' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st2 +"";
		html = html + "								   "+ st4 +"";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enteritem' type='text'/>";
		html = html + "				</td>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";		
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st5 +" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					"+ st6 +" <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('entersono').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var getLanguage =  request.getParameter('hdngetLanguage');
		
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7,st9,st10;
		
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			
			st7 = "ART&#205;CULO INV&#193;LIDO";			
			st9 = "VENTAS DE PEDIDO NO V&#193;LIDO #";
			st10 = "VENTAS DE PEDIDO NO V&#193;LIDO # / CAJA # / TEMA ";

		}
		else
		{
			
			st7 = "INVALID ITEM";
			st9 = "INVALID SALES ORDER # / TRANSFER ORDER #";// Case# 20148342
			st10 = "INVALID SALES ORDER # / TRANSFER ORDER #/CARTON #/ITEM";// Case# 20148342
			
		}
		var getOrdNo = request.getParameter('entersono');
		var getCartonNo=request.getParameter('entercartonno');
		var getItem=request.getParameter('enteritem');

		var UCCarray = new Array();
		UCCarray["custparam_language"] = getLanguage;
		UCCarray["custparam_error"] = st9;//'INVALID SALES ORDER #';
		UCCarray["custparam_screenno"] = 'UCCTASK1';


		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept PO #.
		if (optedEvent == 'F7') 
		{
			response.sendRedirect('SUITELET', 'customscript_rf_outboundreversalmenu', 'customdeploy_rf_outboundreversalmenu', false, UCCarray);
		}
		else 
		{
			nlapiLogExecution('DEBUG', 'getOrdNo', getOrdNo);
			nlapiLogExecution('DEBUG', 'getCartonNo', getCartonNo);
			nlapiLogExecution('DEBUG', 'getItem', getItem);
			var vSOId;
			var vReversalby='TASK';
			var vItemId;
			if(getOrdNo != null && getOrdNo != '')
			{	
				var SOFilters = new Array();
				var SOColumns = new Array();


				SOFilters.push(new nlobjSearchFilter('tranid', null, 'is', getOrdNo));
				SOFilters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

				var SOSearchResults = nlapiSearchRecord('salesorder', null, SOFilters, SOColumns);
				if(SOSearchResults != null && SOSearchResults != '')
				{
					vSOId=SOSearchResults[0].getId();
				}
				//Case# 201410034 starts
				else
				{
					var TOFilters = new Array();
					var TOColumns = new Array();
					TOFilters.push(new nlobjSearchFilter('tranid', null, 'is', getOrdNo));
					TOFilters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
					var TOSearchResults = nlapiSearchRecord('transferorder', null, TOFilters, TOColumns);
					if(TOSearchResults !=null && TOSearchResults !='')
					{
						vSOId=TOSearchResults[0].getId();
					}
					//Case# 201410034 ends
					else{
						var VRAFilters = new Array();
						var VRAColumns = new Array();
						VRAFilters.push(new nlobjSearchFilter('tranid', null, 'is', getOrdNo));
						VRAFilters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
						var VRASearchResults = nlapiSearchRecord('vendorreturnauthorization', null, VRAFilters, VRAColumns);
						if(VRASearchResults !=null && VRASearchResults !='')
						{
							vSOId=VRASearchResults[0].getId();
						}
						else
						{
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, UCCarray);
							return;
						}



					}
				}
			}

			if(getItem != null && getItem != '')
			{	
				var ItemFilters = new Array();
				var ItemColumns = new Array();

				ItemFilters.push(new nlobjSearchFilter('nameinternal', null, 'is', getItem));
				ItemFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

				var ItemSearchResults = nlapiSearchRecord('item', null, ItemFilters, ItemColumns);
				if(ItemSearchResults != null && ItemSearchResults != '')
				{
					vItemId=ItemSearchResults[0].getId();
				}
				else
				{
					//Caseno 20125189 added filter condition for upccode
					var ItemFilter = new Array();
					var ItemColumn = new Array();
					nlapiLogExecution('DEBUG', 'getItem', getItem);
					ItemFilter.push(new nlobjSearchFilter('upccode', null, 'is', getItem));	             
					var ItemSearchResults = nlapiSearchRecord('item', null, ItemFilter, ItemColumn);
					if(ItemSearchResults != null && ItemSearchResults != '')
					{
						vItemId=ItemSearchResults[0].getId();
					}
					//Caseno 20125189 end
					else
					{
						UCCarray["custparam_error"] = st7;//'INVALID ITEM';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, UCCarray);
						return;
					}
				}
			}

			if (getOrdNo != null || getCartonNo!=null || getItem!=null) 
			{
				UCCarray["custparam_uccebizitem"] = vItemId;
				UCCarray["custparam_uccebizordno"] = vSOId;
				UCCarray["custparam_uccreversalby"] = vReversalby;
				UCCarray["custparam_ucccontlpno"] = getCartonNo;

				if(getOrdNo==null)
					getOrdNo='';

				if(getCartonNo==null)
					getCartonNo='';

				if(getItem==null)
					getItem='';

				if (getOrdNo != '' && getCartonNo!='' && getItem!='') 
				{
					UCCarray["custparam_uccreversaltype"] = 'OCI';
				}				
				else if (getOrdNo != '' && getCartonNo=='' && getItem=='') 
				{
					UCCarray["custparam_uccreversaltype"] = 'O';
				}				
				else if (getOrdNo == '' && getCartonNo!='' && getItem=='') 
				{
					UCCarray["custparam_uccreversaltype"] = 'C';
				}				
				else if (getOrdNo == '' && getCartonNo=='' && getItem!='') 
				{
					UCCarray["custparam_uccreversaltype"] = 'I';
				}
				else if (getOrdNo == '' && getCartonNo!='' && getItem!='') 
				{
					UCCarray["custparam_uccreversaltype"] = 'CI';
				}
				else if (getOrdNo != '' && getCartonNo=='' && getItem!='') 
				{
					UCCarray["custparam_uccreversaltype"] = 'OI';
				}
				else if (getOrdNo != '' && getCartonNo!='' && getItem=='') 
				{
					UCCarray["custparam_uccreversaltype"] = 'OC';
				}
				else
				{
					UCCarray["custparam_error"] = st10;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, UCCarray);
					return;
				}

				response.sendRedirect('SUITELET', 'customscript_rf_outboundreversalcontinue', 'customdeploy_rf_outboundreversalcontinue', false, UCCarray);

			}
			else
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, UCCarray);
			}
		}
	}
}


