/***************************************************************************
 eBizNET Solutions               
 ***************************************************************************
 **     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Restlets/Attic/ebiz_hook_restlet5.js,v $
 *     	   $Revision: 1.1.2.1 $
 *     	   $Date: 2013/06/11 12:50:31 $
 *     	   $Author: snimmakayala $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_hook_restlet5.js,v $
 * Revision 1.1.2.1  2013/06/11 12:50:31  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Place Holders for Schedulers and Restlets
 *

 *****************************************/
function eBiz_Hook_Rest_GET(request, response) 
{
}

function eBiz_Hook_Rest_PUT(request, response) 
{
}

function eBiz_Hook_Rest_POST(request, response) 
{
}

function eBiz_Hook_Rest_DELETE(request, response) 
{
}