/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Inbound/Suitelet/Attic/ebiz_RF_CheckInLP_Confirm.js,v $
 *     	   $Revision: 1.1.2.4.4.3.4.7 $
 *     	   $Date: 2014/06/16 14:59:26 $
 *     	   $Author: rmukkera $
 *     	   $Name: t_NSWMS_2014_1_3_125 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_CheckInLP_Confirm.js,v $
 * Revision 1.1.2.4.4.3.4.7  2014/06/16 14:59:26  rmukkera
 * Case # 20148708
 *
 * Revision 1.1.2.4.4.3.4.6  2014/06/13 07:09:17  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.4.4.3.4.5  2014/06/06 06:29:28  skavuri
 * Case# 20148749 (Refresh Functionality ) SB Issue Fixed
 *
 * Revision 1.1.2.4.4.3.4.4  2014/05/30 00:26:49  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.4.4.3.4.3  2014/04/11 16:03:11  skreddy
 * case # 20144839
 * Dealmed sb  issue fix
 *
 * Revision 1.1.2.4.4.3.4.2  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.2.4.4.3.4.1  2013/04/17 16:04:01  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.4.4.3  2012/09/27 10:56:31  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.1.2.4.4.2  2012/09/26 12:47:27  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.1.2.4.4.1  2012/09/24 10:07:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.1.2.4  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.1.2.3  2012/02/22 12:31:02  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.1.2.2  2012/02/06 14:17:23  spendyala
 * CASE201112/CR201113/LOG201121
 * semicolon missing.
 *
 * Revision 1.1.2.1  2012/01/24 07:34:24  schepuri
 * CASE201112/CR201113/LOG201121
 * lp confirmation
 *
 * Revision 1.4  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.3  2011/04/21 07:42:42  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * RF Packing Functionality
 *
 * Revision 1.2  2011/04/21 07:03:02  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * RF Packing Functionality
 *
 * Revision 1.1  2011/04/19 14:19:06  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * RF Packing Functionality
 *
 *
 *****************************************************************************/
function CheckInConfirm(request, response)
{
	var matchFound = true;
	if (request.getMethod() == 'GET') 
	{    
		
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
	    
	    var getCartLP = request.getParameter('custparam_cartlpno');
	    nlapiLogExecution('DEBUG', 'custparam_cartlpno', getCartLP);
	    
		var st0,st1,st2,st3,st4;
		if( getLanguage == 'es_ES')
		{

			st0 = "";
			st1 = "VERIFICAR DIMENSIONES SKU";
			st2 = "VERIFICAR LAS REGLAS";
			st3 = "PARA ESTE USO LP GENERAR PANTALLA DE UBICACI&#211;N.";
			st4 = "ENVIAR";
			st5 = "UBICACI&#211;N NO GENERA";
			
		}
		else
		{
			st0 = "";
			st1 = "VERIFY SKU DIMENSIONS";
			st2 = "VERIFY RULES";
			st3 = "FOR THIS LP USE GENERATE LOCATION SCREEN.";
			st4 = "SEND";
			st5 = "LOCATION NOT GENERATED.";

		}

		//var getShipLP = request.getParameter('custparam_shiplpno');

		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('cmdSend').focus();";        
		html = html + "</script>";        
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> " + st5 + "<br>" +
		"Reasons could be: <br>" +
		"	1. " + st1 +" <br>" +
		"	2. " + st2 + "  <br>" +
		""	+ st3 ;
		html = html + "				</td>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				<input type='hidden' name='hdngetCartLP' value=" + getCartLP + ">";	
		html = html + "			</tr>"; 
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st4+ " <input name='cmdSend' id='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";			        
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('cmdSend').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);

	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var SOarray =new Array();
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		//var optedEvent = request.getParameter('cmdPrevious');       
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var optedField = request.getParameter('selectoption');

		// This variable is to hold the Quantity entered.
		var POarray = new Array();
		var POarray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		var getCartLP = request.getParameter('hdngetCartLP');
		nlapiLogExecution('DEBUG', 'getCartLP', getCartLP);
		
		POarray["custparam_cartlpno"] = getCartLP;
		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);
    	
		
		
		//var getOptedField = request.getParameter('hdnOptedField');
		//POarray["custparam_option"] = getOptedField;

		//	Get the PO# from the previous screen, which is passed as a parameter		
		POarray["custparam_poid"] = request.getParameter('custparam_poid');
		POarray["custparam_poitem"] = request.getParameter('custparam_poitem');
		POarray["custparam_lineno"] = request.getParameter('custparam_lineno');
		POarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
		POarray["custparam_pointernalid"] = request.getParameter('custparam_pointernalid');
		POarray["custparam_poqtyentered"] = request.getParameter('custparam_poqtyentered');
		POarray["custparam_poitemremainingqty"] = request.getParameter('custparam_poitemremainingqty');
		POarray["custparam_polinepackcode"] = request.getParameter('custparam_polinepackcode');
		POarray["custparam_polinequantity"] = request.getParameter('custparam_polinequantity');
		POarray["custparam_polinequantityreceived"] = request.getParameter('custparam_polinequantityreceived');
		POarray["custparam_polineitemstatus"] = request.getParameter('custparam_polineitemstatus');

		var ActualBeginDate = DateStamp();
		var ActualBeginTime = TimeStamp();

		nlapiLogExecution('DEBUG', 'ActualBeginDate', ActualBeginDate);
		nlapiLogExecution('DEBUG', 'ActualBeginTime', ActualBeginTime);

		POarray["custparam_actualbegindate"] = ActualBeginDate;
		var TimeArray = new Array();
		TimeArray = ActualBeginTime.split(' ');

		POarray["custparam_actualbegintime"] = TimeArray[0];
		POarray["custparam_actualbegintimeampm"] = TimeArray[1];

		nlapiLogExecution('DEBUG', 'custparam_actualbegintime', POarray["custparam_actualbegintime"]);
		nlapiLogExecution('DEBUG', 'custparam_actualbegintimeampm', POarray["custparam_actualbegintimeampm"]);

		POarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
		//POarray["custparam_itemquantity"] = request.getParameter('hdnQuantity');
		POarray["custparam_itemcube"] = request.getParameter('custparam_itemcube');
		POarray["custparam_whlocation"] = request.getParameter('custparam_whlocation');
		if(getCartLP !=null && getCartLP !='' && getCartLP !='null')
		{
			nlapiLogExecution('DEBUG', 'navigate', 'cartcheckin process');
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_criteriamenu', 'customdeploy_ebiz_rf_cart_criteriamenu', false, POarray);
		}
		else
		{
			response.sendRedirect('SUITELET', 'customscript_rf_checkin_criteria_menu', 'customdeploy_rf_checkin_criteria_menu_di', false, POarray);
		}
	}
}
