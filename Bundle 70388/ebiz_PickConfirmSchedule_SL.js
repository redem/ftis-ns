/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/*******************************************************************************
 * ***************************************************************************
 * 
 * $Source:
 * /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_PickConfirmation_SL.js,v $
 * $Revision: 1.1.2.2.4.1.4.11.2.3 $ $Date: 2015/12/01 14:42:23 $ $Author: deepshikha $ $Name: b_WMS_2015_2_StdBundle_Issues $
 * 
 * eBizNET version and checksum stamp. Do not remove. $eBiznet_VER:
 * .............. $eBizNET_SUM: ..... PRAMETERS
 * 
 * 
 * DESCRIPTION
 * 
 * Default Data for Interfaces
 * 
 * NOTES AND WARNINGS
 * 
 * INITATED FROM
 *  * REVISION HISTORY $Log: ebiz_PickConfirmSchedule_SL.js,v $
 *  * REVISION HISTORY Revision 1.1.2.2.4.1.4.11.2.3  2015/12/01 14:42:23  deepshikha
 *  * REVISION HISTORY 2015.2 Issue Fix
 *  * REVISION HISTORY 201415525
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.1.2.2.4.1.4.11.2.2  2015/11/06 15:47:20  grao
 *  * REVISION HISTORY 2015.2 Issue Fixes 201414344
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.1.2.2.4.1.4.11.2.1  2015/09/10 13:31:50  schepuri
 *  * REVISION HISTORY case# 201414342
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.1.2.2.4.1.4.11  2015/09/02 08:03:44  rrpulicherla
 *  * REVISION HISTORY Case#201413343
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.1.2.2.4.1.4.10  2015/08/25 10:58:21  sponnaganti
 *  * REVISION HISTORY Case# 201413989
 *  * REVISION HISTORY LP SB2 Issue fix
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.1.2.2.4.1.4.9  2015/08/24 14:38:18  schepuri
 *  * REVISION HISTORY case# 201414010
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.1.2.2.4.1.4.8  2015/08/11 15:52:00  grao
 *  * REVISION HISTORY 2015.2   issue fixes  201413958
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.1.2.2.4.1.4.7  2015/08/04 14:13:31  grao
 *  * REVISION HISTORY 2015.2   issue fixes  201413284
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.1.2.2.4.1.4.6  2015/06/12 13:08:20  schepuri
 *  * REVISION HISTORY case# 201413075
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.1.2.2.4.1.4.5  2015/04/06 12:12:20  gkalla
 *  * REVISION HISTORY Case# 201223632
 *  * REVISION HISTORY TF For multiple deployments
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.1.2.2.4.1.4.4  2014/05/27 07:36:58  snimmakayala
 *  * REVISION HISTORY Case#: 20148135
 *  * REVISION HISTORY Schedule Script Status
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.1.2.2.4.1.4.3  2013/04/16 13:23:20  spendyala
 *  * REVISION HISTORY CASE201112/CR201113/LOG2012392
 *  * REVISION HISTORY Issue fixes.
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.1.2.2.4.1.4.2  2013/04/04 20:20:53  spendyala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Issue fixes
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.1.2.2.4.1.4.1  2013/03/21 14:18:14  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG2012392
 *  * REVISION HISTORY Prod and UAT issue fixes.
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.1.2.2.4.1  2012/11/01 14:55:02  schepuri
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Decimal Qty Conversions
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.1.2.2  2012/07/12 07:24:29  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Schedule script for confirming picks
 *  * REVISION HISTORY
 *  * REVISION HISTORY Revision 1.1.2.1  2012/07/04 18:52:46  snimmakayala
 *  * REVISION HISTORY CASE201112/CR201113/LOG201121
 *  * REVISION HISTORY Schedule script for confirming picks
 ******************************************************************************/

function ConfirmPicksSuiteletforSchedule(request, response) {
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Confirm Picks');
		var vQbWave = "";
		var vQbdo = "";

		var vMsg;
		nlapiLogExecution('ERROR', 'msg', request.getParameter('custpage_msg'));
		if (request.getParameter('custpage_msg') != null && request.getParameter('custpage_msg') != "") {
			vMsg = request.getParameter('custpage_msg');
			nlapiLogExecution('ERROR', 'vMsg', vMsg);
			var msgtype=request.getParameter ["custpage_msgtype"];
			nlapiLogExecution('ERROR', 'msgtype', msgtype);
			var msg= form.addField('custpage_message', 'inlinehtml', null, null, null); 
			if((msgtype!=null && msgtype!='')&&(msgtype=='ERROR'))
			{
				msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', '" + vMsg + "', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
			}
			else
			{
				msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', '" + vMsg + "', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
			}


		}
		else
		{

			if (request.getParameter('ebiz_wave_no') != null
					&& request.getParameter('ebiz_wave_no') != "") {
				vQbWave = request.getParameter('ebiz_wave_no');
				nlapiLogExecution('ERROR', 'QbWave', vQbWave);//case# 201417279 
			}

			if (request.getParameter('custpage_do') != null
					&& request.getParameter('custpage_do') != "") {
				vQbdo = request.getParameter('custpage_do');
			}
			var hiddenQueryParams=form.addField('custpage_qeryparams', 'text', 'queryparams').setDisplayType('hidden');

			form.setScript('customscript_pickserialnovalidation');
			var empfeild = form.addField('custpage_employee', 'select',
					'Performed By', 'Employee');
			empfeild.setLayoutType('startrow', 'none');

			var currentContext = nlapiGetContext();
			var currentUserID = currentContext.getUser();
			empfeild.setDefaultValue(currentUserID);
			//Begin Ship complete flag check changes 03/Jan/2012
			var vMsg;
			nlapiLogExecution('ERROR', 'msg', request.getParameter('custpage_msg'));
			if (request.getParameter('custpage_msg') != null && request.getParameter('custpage_msg') != "") {
				vMsg = request.getParameter('custpage_msg');
				nlapiLogExecution('ERROR', 'vMsg', vMsg);
				var msg= form.addField('custpage_message', 'inlinehtml', null, null, null); 
				msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', '" + vMsg + "', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
			}
			//End Ship complete flag check changes 03/Jan/2012

			var waveno=	form.addField('custpage_wave','text','wave');
			waveno.setDisplayType('hidden');
			waveno.setDefaultValue(vQbWave);

			var ordno=	form.addField('custpage_ord','text','ord');
			ordno.setDisplayType('hidden');
			ordno.setDefaultValue(vQbdo);

			var tempflag=form.addField('custpage_flag','text','tempory flag').setDisplayType('hidden').setDefaultValue("nonpaging");

			var vline, vitem, vqty, vso, vmainline, vrecid, vlocation, vLpno, vSKU, vlocationid, vinvrefno, vskustatus, vpackcode, vdono, vBatchno, vfromlp, vWMSCarrier;
			var vkitrefno, lastkititem, kititem, vserialno, vCompany, vContlpNo, vEbizWaveNo, vSointernid, warehouseLocation,vcontainersize;

			// Create the sublist
			createPickConfirmSublist(form);

			var searchResultArray = getPickComfirmDetails(request,form);

			var vDueDaysList=getDueDaysList();
			var vCreditExceedList=getCreditExceedList();
			var vConfig=nlapiLoadConfiguration('accountingpreferences');
			var ruleValue=vConfig.getFieldValue('CUSTCREDLIMHANDLING');
			var vDueDaysRestrict;
			if(ruleValue!=null && ruleValue!='' && ruleValue!='null' && ruleValue!='NONE')
			{
				if(vConfig != null && vConfig != '')
				{
					vDueDaysRestrict=vConfig.getFieldValue('CREDLIMDAYS');
				}
			}
			if(searchResultArray != null && searchResultArray.length > 0)
			{
				setPagingForSublist(searchResultArray,form,vDueDaysList,vCreditExceedList,vDueDaysRestrict,ruleValue);
			}

			form.addSubmitButton('Confirm');
		}

		response.writePage(form);
	} 
	else // this is the POST block
	{
		var context = nlapiGetContext();
		nlapiLogExecution('ERROR','Remaining usage at the start of POST',context.getRemainingUsage());
		nlapiLogExecution('ERROR', 'Time Stamp at the start of POST',TimeStampinSec());
		var form = nlapiCreateForm('Confirm Picks');
		//var msg= form.addField('custpage_message', 'inlinehtml', null, null, null); 
		var lineCnt = request.getLineItemCount('custpage_items');
		var sysdate=DateStamp();
		var systime=TimeStamp();
		var invtfalg = "";
		var vDisplayedQty = "";
		var invnotfound = "";

		var arrSOdets = new Array();
		var arrSOno = new Array();
		var arrSORecno = new Array();
		var arrSOnoType = new Array();
		var arrebizOrdNo = new Array();
		var pickqtyfinal=0;
		var vRemaningqtyfinal=0;
		var getfulfillrecid;
		var fullFillmentorderno;
		var oldsono;
		var arrclosedso=new Array();var c=0;
		var trantype = '';
		var vOrdArr= new Array();
		var vOrdInfoArr= new Array();
		var vOrdDetailsArr=new Array();
		var oldord;
		var oldline;
		var oldPicQty;
		var vcnt=0;
		var vSONotCompleted;
		var vBlnShipComp=true;
		var vBlnOrderClosed=false;
		var vwave='';
		var pagingFlag='';
		form.setScript('customscript_pickserialnovalidation');
		var hiddenQueryParams=form.addField('custpage_qeryparams', 'text', 'queryparams').setDisplayType('hidden');
		if(request.getParameter('custpage_qeryparams')!=null)
		{
			hiddenQueryParams.setDefaultValue(request.getParameter('custpage_qeryparams'));
		}
		var tempflag=form.addField('custpage_flag','text','tempory flag').setDisplayType('hidden').setDefaultValue("nonpaging");
		if (request.getParameter('custpage_wave') != null && request.getParameter('custpage_wave') != "") {
			vwave = request.getParameter('custpage_wave');
		}
		if (request.getParameter('custpage_ord') != null && request.getParameter('custpage_ord') != "") {
			vQbdo = request.getParameter('custpage_ord');
		}

		var waveno=	form.addField('custpage_wave','text','wave');
		waveno.setDisplayType('hidden');
		if (request.getParameter('custpage_wave') != null && request.getParameter('custpage_wave') != "") {
			waveno.setDefaultValue(request.getParameter('custpage_wave'));
		}
		var ordno=	form.addField('custpage_ord','text','ord');
		ordno.setDisplayType('hidden');
		if (request.getParameter('custpage_ord') != null && request.getParameter('custpage_ord') != "") {
			ordno.setDefaultValue(request.getParameter('custpage_ord'));
		}

		if (request.getParameter('custpage_flag') != null && request.getParameter('custpage_flag') != "")
		{
			pagingFlag = request.getParameter('custpage_flag');
			if(pagingFlag == 'paging')
			{
				var empfeild = form.addField('custpage_employee', 'select',
						'Performed By', 'Employee');
				empfeild.setLayoutType('startrow', 'none');
				var currentContext = nlapiGetContext();
				var currentUserID = currentContext.getUser();
				empfeild.setDefaultValue(currentUserID);
				createPickConfirmSublist(form);
				var searchResultArray = getPickComfirmDetails(request,form);
				if(searchResultArray != null && searchResultArray.length > 0)
				{
					setPagingForSublist(searchResultArray,form,vDueDaysList,vCreditExceedList);
				}
				form.addSubmitButton('Confirm');
				response.writePage(form);
			}
			else
			{
				for (var j = 1; j <= lineCnt; j++) 
				{

					var chkVal = request.getLineItemValue('custpage_items','custpage_so', j);
					var dorefno = request.getLineItemValue('custpage_items','custpage_salesordinternid', j);
					var dorefname = request.getLineItemValue('custpage_items','custpage_sono', j);
					var Lineno = request.getLineItemValue('custpage_items','custpage_lineno', j);
					var pickqty = request.getLineItemValue('custpage_items','custpage_ordqty', j);
					var itemname = request.getLineItemValue('custpage_items','custpage_itemname', j);
					var itemno = request.getLineItemValue('custpage_items','custpage_itemno', j);
					var EndLocId = request.getLineItemValue('custpage_items','custpage_actbeginloc', j);
					var itemstatus = request.getLineItemValue('custpage_items','custpage_skustatusvalue', j);// case# 201413892
					if(pickqty == null || pickqty == '')
						pickqty=0;
					var WHLocId = request.getLineItemValue('custpage_items','custpage_wmslocation', j);	
					if (chkVal == 'T')
					{
						vcnt++;
						var invtqty=0;

						var IsPickFaceLoc = 'F';

						IsPickFaceLoc = isPickFaceLocation(itemno,EndLocId);
						nlapiLogExecution('DEBUG', 'itemstatus', itemstatus);
						if(IsPickFaceLoc=='T')
						{
							//var openinventory=getQOHForAllSKUsinPFLocation(EndLocId,itemno);
							var openinventory=vngetQOHForAllSKUsinPFLocation(EndLocId,itemno,WHLocId,itemstatus);
							//case# 20148436 ends
							if(openinventory!=null && openinventory!='')
							{
								nlapiLogExecution('DEBUG', 'Pick Face Inventory Length', openinventory.length);

								for(var x=0; x< openinventory.length;x++)
								{
									var vinvtqty=openinventory[x].getValue('custrecord_ebiz_qoh');
									var itemstatus123 = openinventory[x].getValue('custrecord_ebiz_inv_sku_status');

									if(vinvtqty==null || vinvtqty=='' || isNaN(vinvtqty))
									{
										vinvtqty=0;
									}

									invtqty=parseInt(invtqty)+parseInt(vinvtqty);
								}						
							}
						}

						if(invtqty==null || invtqty=='' || isNaN(invtqty))
						{
							invtqty=0;
						}

						nlapiLogExecution('DEBUG', 'invtqty', invtqty);
						nlapiLogExecution('DEBUG', 'itemstatus123', itemstatus123);

						if(IsPickFaceLoc=='T' && (parseInt(pickqty)>parseInt(invtqty)))
						{
							var openreplns = new Array();
							openreplns=getOpenReplns(itemno,EndLocId);
							if(openreplns!=null && openreplns!='')
							{
								showInlineMessage(form, 'Error','CONFIRM THE OPEN REPLENS FOR THE ITEM : '+ itemname , '');
								response.writePage(form);
								return false;
							}
							//}// CASE # 201410713
							else
							{
								var replengen = generateReplenishment(EndLocId,itemno,WHLocId,waveno);
								nlapiLogExecution('DEBUG', 'replengen',replengen);
								if(replengen!=true)
								{
									showInlineMessage(form, 'Error','Replen Generation Failed.Insufficient inventory in Bulk location : '+ itemname , '');
									response.writePage(form);
									return false;
								}

								else 
								{
									showInlineMessage(form, 'Error','CONFIRM THE OPEN REPLENS FOR THE ITEM : '+ itemname , '');
									response.writePage(form);
									return false;
								}


							}
						}
						if(oldord!=dorefno)
						{
							vOrdArr.push(dorefno);
							oldord=dorefno;

							var currentRow = [dorefno,dorefname];
							vOrdInfoArr.push(currentRow);
						}
					} 
				}

				var vtrantype = nlapiLookupField('transaction', vOrdArr, 'recordType');// even though order array is passed consider the trantype of only first order// case# 201414342
				var isShipCompleteFlag=isSoHasShipcompeteFlag(vOrdArr,vtrantype);
				var blnpickconfirm = IsOrderClosed(vOrdArr,vtrantype);	

				var vpickconfirm=fnShipCompleteValidate(vOrdArr,vtrantype);
				nlapiLogExecution('ERROR','Remaining usage after 4 functions',context.getRemainingUsage());
				for(var z=0;z<vOrdInfoArr.length;z++)
				{
					var vsoid=vOrdInfoArr[z][0];
					var vsoname=vOrdInfoArr[z][1];
					/*var vtrantype = nlapiLookupField('transaction', vsoid, 'recordType');
					var isShipCompleteFlag=isSoHasShipcompeteFlag(vsoid,vtrantype);
					var blnpickconfirm = IsOrderClosed(vsoid,vtrantype);	*/
					nlapiLogExecution('ERROR', 'blnpickconfirmArray new', blnpickconfirm);
					//if(blnpickconfirm=='F')
					if(blnpickconfirm.indexOf('F')!=-1)
					{
						vBlnOrderClosed=true;
						nlapiLogExecution('ERROR', 'vBlnOrderClosed', vBlnOrderClosed);
						if(vSONotCompleted != null && vSONotCompleted != "")
						{
							vSONotCompleted = vSONotCompleted + ',' +  vsoname;
						}	
						else
							vSONotCompleted = vsoname;
					}

					nlapiLogExecution('ERROR', 'vBlnOrderClosed', vBlnOrderClosed);
					if(vBlnOrderClosed != false)
					{
						nlapiLogExecution('ERROR','Remaining usage at the end of order status validation',context.getRemainingUsage());

						if (request.getParameter('custpage_wave') != null && request.getParameter('custpage_wave') != "") {
							vwave = request.getParameter('custpage_wave');
						}

						if (request.getParameter('custpage_ord') != null && request.getParameter('custpage_ord') != "") {
							vQbdo = request.getParameter('custpage_ord');
						}

						var SOarray=new Array();
						SOarray ["ebiz_wave_no"] = vwave;
						SOarray ["custpage_do"] = vQbdo;
						SOarray ["custpage_msgtype"] = 'ERROR';
						SOarray ["custpage_msg"] ='Order# '+ vSONotCompleted +' is closed, you cannot perform further operations in Warehouse';
						response.sendRedirect('SUITELET', 'customscript_pickconfirmsch', 'customdeploy_pickconfirmsch', false, SOarray );
						return;

					}
					//if(isShipCompleteFlag=='T')
					if(isShipCompleteFlag.indexOf('T')!=-1)
					{
						//

						for (var j = 1; j <= lineCnt; j++) 
						{
							var sointid=request.getLineItemValue('custpage_items','custpage_salesordinternid',j);
							if(vsoid==sointid)
							{
								var chkselect=request.getLineItemValue('custpage_items','custpage_so',j);
								{
									if(chkselect=='F')
									{
										var SOarray=new Array();
										SOarray ["ebiz_wave_no"] = vwave;
										SOarray ["custpage_do"] = vQbdo;
										SOarray ["custpage_msgtype"] = 'ERROR';
										var vMsg ='Order# '+ vsoname +' is marked for shipcompete,please select all the lines of this SO. ';
										msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', '" + vMsg + "', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
										//response.sendRedirect('SUITELET', 'customscript_pickconfirmsch', 'customdeploy_pickconfirmsch', false, SOarray );
										//return;
										createPickConfirmSublist(form);
										var searchResultArray = getPickComfirmDetails(request,form);
										if(searchResultArray != null && searchResultArray.length > 0)
										{
											setPagingForSublist(searchResultArray,form,vDueDaysList,vCreditExceedList);
										}
										form.addSubmitButton('Confirm');
										response.writePage(form);
										return;
									}
								}


							}
						}
					}

					else
					{
						//var vpickconfirm=fnShipCompleteValidate(vsoid,vtrantype);

						if(vpickconfirm=='F')
						{
							vBlnShipComp=false;
							if(vSONotCompleted != null && vSONotCompleted != "")
							{
								vSONotCompleted = vSONotCompleted + ',' +  vsoname;
							}	
							else
								vSONotCompleted = vsoname;
							break;
						}

						nlapiLogExecution('ERROR', 'SO Name & Ship Complete', vsoname+' & '+vBlnShipComp);
					}
				}
				if(vBlnShipComp!=false)
				{
					nlapiLogExecution('ERROR', 'Into Ship Complete validation at Fulfillment Order Line level......');
					var filters = new Array();

					if(vOrdArr!=null&&vOrdArr!="")
						filters.push(new nlobjSearchFilter('custrecord_ns_ord', null,'anyof', vOrdArr)); 

					var columns = new Array();

					columns.push(new nlobjSearchColumn('custrecord_ord_qty',null,'sum'));
					columns.push(new nlobjSearchColumn('custrecord_pickgen_qty',null,'sum'));
					columns.push(new nlobjSearchColumn('custrecord_shipcomplete',null,'group'));
					columns.push(new nlobjSearchColumn('name',null,'group'));
					columns.push(new nlobjSearchColumn('custrecord_ordline',null,'group'));
					columns[3].setSort();

					var searchresultsordline = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
					if(searchresultsordline != null && searchresultsordline != "" && searchresultsordline.length>0)
					{
						var vlineOrdQty=0;
						var vlinePickQty=0;
						var vlineShipCompFlag;
						var vline;
						var vlineSO;
						for(var v=0;v<searchresultsordline.length;v++)
						{
							vlineOrdQty = parseFloat(searchresultsordline[v].getValue('custrecord_ord_qty',null,'sum'));
							vlinePickQty = parseFloat(searchresultsordline[v].getValue('custrecord_pickgen_qty',null,'sum'));
							vlineShipCompFlag = searchresultsordline[v].getValue('custrecord_shipcomplete',null,'group');
							vline = searchresultsordline[v].getValue('custrecord_ordline',null,'group');
							vlineSO= searchresultsordline[v].getValue('name',null,'group');

							if((vlineShipCompFlag=='T') && (parseFloat(vlineOrdQty) > parseFloat(vlinePickQty)))
							{
								vBlnShipComp=false;
								for(var u=0;u<vOrdInfoArr.length;u++)
								{
									var vsointrid = vOrdInfoArr[u][0];
									var vso=vOrdInfoArr[u][1];

									if(vsointrid==vlineSO)
									{
										if(vSONotCompleted != null && vSONotCompleted != "")
										{
											vSONotCompleted = vSONotCompleted + ',' +  vso;
										}	
										else
											vSONotCompleted = vso;

									}
								}
								nlapiLogExecution('ERROR', 'Ship Complete Validation failed for Sales Orders ',vSONotCompleted);
								break;
							}
						}

					}
				}
				nlapiLogExecution('ERROR','Remaining usage after Ship Complete and Closed Status Validate',context.getRemainingUsage());
				if(vBlnShipComp == false)
				{
					nlapiLogExecution('ERROR','Remaining usage at the end of ship complete validation',context.getRemainingUsage());

					if (request.getParameter('custpage_wave') != null && request.getParameter('custpage_wave') != "") {
						vwave = request.getParameter('custpage_wave');
					}
					if (request.getParameter('custpage_ord') != null && request.getParameter('custpage_ord') != "") {
						vQbdo = request.getParameter('custpage_ord');
					}

					var SOarray=new Array();
					SOarray ["ebiz_wave_no"] = vwave;
					SOarray ["custpage_do"] = vQbdo;
					SOarray ["custpage_msgtype"] = 'CONFIRM';
					SOarray ["custpage_msg"] ='Order# '+ vSONotCompleted +' is now marked as Ship complete and you are attempting to ship partial, cancel wave and Ship complete';
					response.sendRedirect('SUITELET', 'customscript_pickconfirmsch', 'customdeploy_pickconfirmsch', false, SOarray );

				}
				else //End Ship complete flag check changes 03/Jan/2012
				{
					var cnt=0;
					var vtaskid='';
					var opentaskrecarray = new Array();
					for (var k = 1; k <= lineCnt; k++) 
					{
						var chkVal = request.getLineItemValue('custpage_items','custpage_so', k);
						var EndLocId = request.getLineItemValue('custpage_items','custpage_actbeginloc', k);
						var newinvrefno = request.getLineItemValue('custpage_items','custpage_invrefno', k);
					
						var Recid = request.getLineItemValue('custpage_items','custpage_recid', k);


						var pickqty = request.getLineItemValue('custpage_items','custpage_ordqty', k);
						if(Recid!=null && Recid!='' && Recid!='null')
						{
							var opentaskRec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', Recid);
							var expqtyn = opentaskRec.getFieldValue('custrecord_expe_qty');
							var actbeginloc = opentaskRec.getFieldValue('custrecord_actbeginloc');
							//var ActQty = opentaskRec.getFieldValue('custrecord_act_qty');
							if((EndLocId!=null && EndLocId!='') && (actbeginloc!=EndLocId))
							{
								opentaskRec.setFieldValue('custrecord_actendloc', EndLocId);
								//opentaskRec.setFieldValue('custrecord_invref_no', newinvrefno);
								
								nlapiSubmitRecord(opentaskRec, false, true);
							}

							if(pickqty!=expqtyn)
							{
								nlapiLogExecution('ERROR', 'vqty 1', pickqty);   

								opentaskRec.setFieldValue('custrecord_act_qty', pickqty);
								nlapiSubmitRecord(opentaskRec, false, true);
								//nlapiLogExecution('ERROR', 'ActQty ', ActQty);

							}
							else
							{
								nlapiLogExecution('ERROR', 'vqty 11', pickqty);   

								opentaskRec.setFieldValue('custrecord_act_qty', expqtyn);
								nlapiSubmitRecord(opentaskRec, false, true);
							}

						}


						if (chkVal == 'T')
						{
//							var curRow=[Recid];
//							opentaskrecarray.push(curRow);	

							if(cnt==0)
							{
								vtaskid=Recid;
								cnt++;
							}
							else
							{
								vtaskid=vtaskid+','+Recid;
								cnt++;
							}
						}
					}

					var param = new Array();
					param['custscript_taskids'] = vtaskid;

					nlapiScheduleScript('customscript_pickconfirm_scheduler', null,param);

					nlapiLogExecution('ERROR','Remaining usage after calling Schedule script',context.getRemainingUsage());

					updateScheduleScriptStatus('Confirm Picks',currentUserID,'Submitted',vsoname,vtrantype);

					var SOarray=new Array();
					SOarray ["ebiz_wave_no"] = null;
					SOarray ["custpage_do"] = null;
					SOarray ["custpage_msgtype"] = 'CONFIRM';
					SOarray ["custpage_msg"] ='Pick Confirmation process has been initiated successfully';

					response.sendRedirect('SUITELET', 'customscript_pickconfirmsch', 'customdeploy_pickconfirmsch', false, SOarray );

					//response.sendRedirect('SUITELET', 'customscript_pickconfirmschqb', 'customdeploy_pickconfirmschqb', false, null );

				}
			}
		}
	}
}



/**
 * To check Ship Complete or not
 * @param vSOInternalId
 * returns array first item is bool value and second value is SO#
 * returns true if Ship complete and all qty picked
 * returns true if ship complete flag is false
 * returns false if ship not complete 
 * returns SO#(Parent Order#) as second parameter if it fails
 */
function blnShipComplete(vSOInternalId)
{
	var vBlnShipComp=true;
	var filters = new Array();

	filters.push(new nlobjSearchFilter('name', null,
			'is', vSOInternalId)); 


	var columns = new Array();
	columns.push(new nlobjSearchColumn('custrecord_ord_qty'));
	columns.push(new nlobjSearchColumn('custrecord_pickgen_qty'));
	columns.push(new nlobjSearchColumn('custrecord_shipcomplete'));
	columns.push(new nlobjSearchColumn('custrecord_ns_ord'));


	var searchresultsordline = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);

	if(searchresultsordline != null && searchresultsordline != "" && searchresultsordline.length>0)
	{
		nlapiLogExecution('ERROR', 'searchresultsordline.length',searchresultsordline.length);
		for(var v=0;v<searchresultsordline.length;v++)
		{
			var vlinePickQty=0;
			var vlineOrdQty=0;
			var vlineSONo;
			if(searchresultsordline[v].getValue('custrecord_ord_qty')!= null && searchresultsordline[v].getValue('custrecord_ord_qty') != "")
				vlineOrdQty=parseFloat(searchresultsordline[v].getValue('custrecord_ord_qty'));

			if(searchresultsordline[v].getValue('custrecord_pickgen_qty') != null && searchresultsordline[v].getValue('custrecord_pickgen_qty') != "")
				vlinePickQty= parseFloat(searchresultsordline[v].getValue('custrecord_pickgen_qty'));

			var vlineShipCompFlag=searchresultsordline[v].getValue('custrecord_shipcomplete');

			if(searchresultsordline[v].getValue('custrecord_ns_ord') != null && searchresultsordline[v].getValue('custrecord_ns_ord') != "")
				vlineSONo= searchresultsordline[v].getText('custrecord_ns_ord');

			nlapiLogExecution('ERROR', 'vlineOrdQty',vlineOrdQty);
			nlapiLogExecution('ERROR', 'vlinePickQty',vlinePickQty);
			nlapiLogExecution('ERROR', 'vlineShipCompFlag',vlineShipCompFlag);

			if(vlineShipCompFlag!= null && vlineShipCompFlag != "" && vlineShipCompFlag =='T' && vlineOrdQty >0)
			{
				if(vlineOrdQty<vlinePickQty)
				{
					vBlnShipComp=false;
					break;
				}
			}

		}
	}
	else
		vBlnShipComp=false;

	var vArrResults=new Array();

	vArrResults.push(vBlnShipComp);
	vArrResults.push(vlineSONo);

	nlapiLogExecution('ERROR', 'vBlnShipComp',vBlnShipComp);
	nlapiLogExecution('ERROR', 'vlineSONo',vlineSONo);
	return vArrResults;
}

function fnShipCompleteValidate(sointrid,trantype)
{
	nlapiLogExecution('ERROR', 'Into fnShipCompleteValidate - SO id', sointrid);
	var blnpickconfirm='T';
	var blnpickconfirmArray= new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));		// Retrieve order lines
	filters.push(new nlobjSearchFilter('quantity', null, 'greaterthan', 0));// Order Quantity > 0
	filters.push(new nlobjSearchFilter('shipping', null, 'is', 'F'));//To avoid shipping lines.
	filters.push(new nlobjSearchFilter('taxline', null, 'is', 'F'));//To avoid tax lines.
	filters.push(new nlobjSearchFilter('shipcomplete', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('internalid', null, 'is', sointrid));//To avoid shipping lines.
	filters.push(new nlobjSearchFilter('custrecord_ebizwhsite', 'location', 'is', 'T'));//Location should be warehouse site

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('tranid');	
	columns[1] = new nlobjSearchColumn('line');
	columns[2] = new nlobjSearchColumn('quantitycommitted');
	columns[3] = new nlobjSearchColumn('quantity');
	columns[4] = new nlobjSearchColumn('internalid');
	columns[5] = new nlobjSearchColumn('shipcomplete');//Added 08-Nov-2011
	columns[6] = new nlobjSearchColumn('quantityshiprecv');	

	columns[0].setSort(false);
	columns[1].setSort();

	var salesOrderLineDetails = nlapiSearchRecord(trantype, null, filters, columns);
	if(salesOrderLineDetails!=null)
	{
		for(var i=0;i<salesOrderLineDetails.length;i++)
		{
			var soname = salesOrderLineDetails[i].getValue('tranid');			
			var orderqty = salesOrderLineDetails[i].getValue('quantity');
			var salesOrderLineNo = salesOrderLineDetails[i].getValue('line');
			var commetedQty = salesOrderLineDetails[i].getValue('quantitycommitted');
			var shippedqty = salesOrderLineDetails[i].getValue('quantityshiprecv');
			var shpcompleteflag = salesOrderLineDetails[i].getValue('shipcomplete'); 

			if(commetedQty==null || commetedQty=='' || isNaN(commetedQty))
				commetedQty=0;	

			if(shippedqty == null || shippedqty == '' || isNaN(shippedqty))
				shippedqty = 0;

			var totalcomtdqty=parseFloat(commetedQty)+parseFloat(shippedqty);
			nlapiLogExecution('ERROR', 'shippedqty', shippedqty);
			nlapiLogExecution('ERROR', 'commetedQty', commetedQty);
			nlapiLogExecution('ERROR', 'orderqty', orderqty);
			nlapiLogExecution('ERROR', 'totalcomtdqty', totalcomtdqty);
			if(parseFloat(orderqty) > parseFloat(totalcomtdqty))
			{
				blnpickconfirm='F';
				//blnpickconfirmArray.push(blnpickconfirm);
				nlapiLogExecution('ERROR', 'Out of fnShipCompleteValidate - Return in if', blnpickconfirm);
				return blnpickconfirm;
				//return blnpickconfirmArray;
			}

		}

	}
	nlapiLogExecution('ERROR', 'Out of fnShipCompleteValidate - Return', blnpickconfirm);
	blnpickconfirmArray.push(blnpickconfirm);
	nlapiLogExecution('ERROR', 'Out of fnShipCompleteValidate blnpickconfirmArray - Return', blnpickconfirmArray);
	return blnpickconfirm;
	//return blnpickconfirmArray;
}

function IsOrderClosed(sointrid,trantype)
{
	nlapiLogExecution('ERROR', 'Into IsOrderClosed - SO id', sointrid);
	var blnpickconfirm='T';
	var blnpickconfirmArray = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('internalid', null, 'is', sointrid));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('tranid');	
	columns[1] = new nlobjSearchColumn('status');

	var salesOrderLineDetails = nlapiSearchRecord(trantype, null, filters, columns);
	if(salesOrderLineDetails!=null)
	{
		for(var i=0;i<salesOrderLineDetails.length;i++)
		{
			var soname = salesOrderLineDetails[i].getValue('tranid');		
			var orderstatus = salesOrderLineDetails[i].getValue('status'); 

			nlapiLogExecution('ERROR', 'orderstatus', orderstatus);

			if(orderstatus=='closed')
			{
				blnpickconfirm='F';
				nlapiLogExecution('ERROR', 'Out of IsOrderClosed - Return', blnpickconfirm);
				blnpickconfirmArray.push(blnpickconfirm);
				//return blnpickconfirm;
				return blnpickconfirmArray;
			}

		}

	}
	nlapiLogExecution('ERROR', 'Out of IsOrderClosed - Return', blnpickconfirm);
	return blnpickconfirm;
}
function isSoHasShipcompeteFlag(soId,trantype)
{
	var filters = new Array();
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('internalid', null, 'is', soId));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('shipcomplete');	
	var isShipcompete="F";
	var isShipcompeteArray =  new Array();

	var salesOrderLineDetails =nlapiSearchRecord(trantype, null, filters, columns);
	nlapiLogExecution("ERROR","salesOrderLineDetails",salesOrderLineDetails);

	if(salesOrderLineDetails!=null&&salesOrderLineDetails!="")
	{
		for(var i=0;i<salesOrderLineDetails.length;i++)
		{
			isShipcompete=salesOrderLineDetails[i].getValue('shipcomplete');
			isShipcompeteArray.push(isShipcompete);
		}

	}
	nlapiLogExecution("ERROR","isShipcompeteArray",isShipcompeteArray);

	//return isShipcompete;
	return isShipcompeteArray;
}

function setPagingForSublist(orderList,form,vDueDaysList,vCreditExceedList,vDueDaysRestrict,ruleValue)
{
	if(orderList != null && orderList.length > 0){
		nlapiLogExecution('Error', 'orderList ', orderList.length);
		var orderListArray=new Array();		
		for(k=0;k<orderList.length;k++)
		{
			nlapiLogExecution('ERROR', 'orderList[k]', orderList[k]); 
			var ordsearchresult = orderList[k];

			if(ordsearchresult!=null)
			{
				nlapiLogExecution('ERROR', 'ordsearchresult.length ', ordsearchresult.length); 
				for(var j=0;j<ordsearchresult.length;j++)
				{
					orderListArray[orderListArray.length]=ordsearchresult[j];				
				}
			}
		}
		nlapiLogExecution('ERROR', 'orderListArray.length ', orderListArray.length); 
		var test='';

		if(orderListArray.length>0)
		{
			if(orderListArray.length>500)
			{
				var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('hidden');
				pagesize.setDisplaySize(10,10);
				pagesize.setLayoutType('outsidebelow', 'startrow');
				var select= form.addField('custpage_selectpage','select', 'Select Records');	
				select.setLayoutType('outsidebelow', 'startrow');			
				select.setDisplaySize(200,30);
				if (request.getMethod() == 'GET'){
					pagesize.setDefaultValue("500");//30
					pagesizevalue=500;//30
				}
				else
				{
					if(request.getParameter('custpage_pagesize')!=null)
					{pagesizevalue= request.getParameter('custpage_pagesize');}
					else
					{pagesizevalue= 500;//30
					pagesize.setDefaultValue("500");}//30
				}
				nlapiLogExecution('ERROR', 'tst','tst');
				//this is to add the pageno's to the dropdown.
				var len=orderListArray.length/parseFloat(pagesizevalue);
				nlapiLogExecution('ERROR', 'len',len);
				for(var k=1;k<=Math.ceil(len);k++)
				{
					nlapiLogExecution('ERROR', 'len IN FOR',Math.ceil(len));
					var from;var to;
					to=parseFloat(k)*parseFloat(pagesizevalue);
					from=(parseFloat(to)-parseFloat(pagesizevalue))+1;
					if(parseFloat(to)>orderListArray.length)
					{
						to=orderListArray.length;
						test=from.toString()+","+to.toString(); 
					}
					var temp=from.toString()+" to "+to.toString();
					var tempto=from.toString()+","+to.toString();
					select.addSelectOption(tempto,temp);
				} 
				if (request.getMethod() == 'POST'){
					if(request.getParameter('custpage_selectpage')!=null ){
						select.setDefaultValue(request.getParameter('custpage_selectpage'));	
					}
					if(request.getParameter('custpage_pagesize')!=null ){
						pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	
					}
				}
			}
			else
			{
				pagesizevalue=parseFloat(orderListArray.length);
			}
			var minval=0;var maxval=parseFloat(pagesizevalue);
			if(parseFloat(pagesizevalue)>orderListArray.length)
			{
				maxval=orderListArray.length;
			}
			var selectno=request.getParameter('custpage_selectpage');
			if(selectno!=null )
			{
				var selectedPage= request.getParameter('custpage_selectpage');
				var selectedPageArray=selectedPage.split(',');			
				var diff=parseFloat(selectedPageArray[1])-(parseFloat(selectedPageArray[0])-1);
				nlapiLogExecution('ERROR', 'diff',diff);
				var pagevalue=request.getParameter('custpage_pagesize');
				nlapiLogExecution('ERROR', 'pagevalue',pagevalue);
				if(pagevalue!=null)
				{
					if(parseFloat(diff)==parseFloat(pagevalue)|| test==selectno)
					{
						var selectedPageArray=selectno.split(',');	
						nlapiLogExecution('ERROR', 'selectedPageArray.length ', selectedPageArray.length);  
						minval=parseFloat(selectedPageArray[0])-1;
						nlapiLogExecution('ERROR', 'selectedPageArray[0] ', selectedPageArray[0]);  
						maxval=parseFloat(selectedPageArray[1]);
						nlapiLogExecution('ERROR', 'selectedPageArray[1] ', selectedPageArray[1]);  
					}
				}
			}
			var c=0;
			var minvalue;
			minvalue=minval;
			for(var j = minvalue; j < maxval; j++){		
				var currentOrder = orderListArray[j];
				addPickConfirmOrderToSublist(form, currentOrder, c,vDueDaysList,vCreditExceedList,vDueDaysRestrict,ruleValue);
				c=c+1;
			}
		}
	}
}

/**
 * Create the sublist that will display the items for wave generation
 * 
 * @param form
 */

function createPickConfirmSublist(form){
	var sublist = form.addSubList("custpage_items", "inlineeditor",
	"ItemList");
	sublist.addField("custpage_so", "checkbox", "Confirm").setDefaultValue(
	'T');
	sublist.addField("custpage_sono", "text", "SO #");
	sublist.addField("custpage_lineno", "text", "SO Line #");
	sublist.addField("custpage_itemname", "text", "Item");
	sublist.addField("custpage_serialized", "checkbox", "Serialized");
	sublist.addField("custpage_itemstatus", "text", "Item Status");
	sublist.addField("custpage_itempackcode", "text", "Pack Code");
	sublist.addField("custpage_itembatchno", "text", "LOT#");
	sublist.addField("custpage_itemserialno", "text", "Serial No");
	sublist.addField("custpage_ordqty", "text", "Qty");
	sublist.addField("custpage_lpno", "text", "Container LP #");
	sublist.addField("custpage_actbeginloc", "select", "Location",'customrecord_ebiznet_location');
	sublist.addField("custpage_recid", "text", "recid").setDisplayType('hidden');
	sublist.addField("custpage_itemno", "text", "itemno").setDisplayType('hidden');
	sublist.addField("custpage_invrefno", "text", "invrefno").setDisplayType('hidden');
	sublist.addField("custpage_previnvrefno", "text", "Prev invrefno").setDisplayType('hidden');
	sublist.addField("custpage_dorefno", "text", "dorefno").setDisplayType('hidden');
	sublist.addField("custpage_dono", "text", "dono").setDisplayType('hidden');
	sublist.addField("custpage_fromlp", "text", "dono").setDisplayType('hidden');
	sublist.addField("custpage_kitrefno", "text", "Kit Falg").setDisplayType('hidden');
	sublist.addField("custpage_kilastitem", "text", "Kit lastittem").setDisplayType('hidden');
	sublist.addField("custpage_kititem", "text", "Kit item").setDisplayType('hidden');
	sublist.addField("custpage_actloc", "select", "Location").setDisplayType('hidden');
	sublist.addField("custpage_contlpno", "select", "ContLpNo").setDisplayType('hidden');
	sublist.addField("custpage_ebizwaveno", "select", "WaveNo").setDisplayType('hidden');
	sublist.addField("custpage_salesordinternid", "select", "sointernid").setDisplayType('hidden');
	sublist.addField("custpage_wmslocation", "text", "WMS Location").setDisplayType('hidden');
	sublist.addField("custpage_company", "select", "Company").setDisplayType('hidden');
	sublist.addField("custpage_skustatusvalue", "select", "Skustatusvalue").setDisplayType('hidden');
	sublist.addField("custpage_sizeid", "select", "Container Size Id",'customrecord_ebiznet_container');
	sublist.addField("custpage_itemtype", "text", "ItemType").setDisplayType('hidden');
	sublist.addField("custpage_serialout", "text", "SerialOut").setDisplayType('hidden');
	sublist.addField("custpage_serialin", "text", "SerialIn").setDisplayType('hidden');
	sublist.addField("custpage_displyedordqty", "text", "Displayed Qty").setDisplayType('hidden');
	sublist.addField("custpage_wmscarrier", "text", "WMS Carrier").setDisplayType('hidden');	 
	sublist.addField("custpage_paymenthold", "text", "Notes");
	//sublist.addField("custpage_confirmflag", "text", "Confirm Flag").setDisplayType('hidden');
	sublist.addField("custpage_presentqoh", "text", "Present QOH").setDisplayType('hidden');// case# 201413075
}

function addPickConfirmOrderToSublist(form, currentOrder, i,vDueDaysList,vCreditExceedList,vDueDaysRestrict,ruleValue){
	var vnotes = currentOrder.getValue('paymenteventresult','custrecord_ebiz_order_no');
	vline = currentOrder.getValue('custrecord_line_no');
	vitem = currentOrder.getValue('custrecord_ebiz_sku_no');
	vqty = currentOrder.getValue('custrecord_expe_qty');
	vso = currentOrder.getValue('custrecord_ebiz_cntrl_no');
	vLpno = currentOrder.getValue('custrecord_container_lp_no');
	vlocationid = currentOrder.getValue('custrecord_actbeginloc');
	vlocation = currentOrder.getText('custrecord_actbeginloc');
	vskustatusvalue = currentOrder.getValue('custrecord_sku_status');
	vSKU = currentOrder.getText('custrecord_sku');
	vinvrefno = currentOrder.getValue('custrecord_invref_no');
	vrecid = currentOrder.getId();
	vskustatus = currentOrder.getText('custrecord_sku_status');
	vpackcode = currentOrder.getValue('custrecord_packcode');
	vdono = currentOrder.getValue('name');
	vfromlp = currentOrder.getValue('custrecord_from_lp_no');
	vBatchno = currentOrder.getValue('custrecord_batch_no');
	vkitrefno = currentOrder.getValue('custrecord_kit_refno');
	lastkititem = currentOrder.getValue('custrecord_last_member_component');
	kititem = currentOrder.getValue('custrecord_parent_sku_no');
	vserialno = currentOrder.getValue('custrecord_serial_no');
	vCompany = currentOrder.getValue('custrecord_comp_id');
	vContlpNo = currentOrder.getValue('custrecord_container_lp_no');
	vEbizWaveNo = currentOrder.getValue('custrecord_ebiz_wave_no');
	vSointernid = currentOrder.getValue('custrecord_ebiz_order_no');
	warehouseLocation = currentOrder.getValue('custrecord_wms_location');
	vcontainersize = currentOrder.getValue('custrecord_container');
	vWMSCarrier = currentOrder.getValue('custrecord_ebizwmscarrier');


	var ItemType = currentOrder.getText('type','custrecord_sku');
	var serialout = currentOrder.getValue('custitem_ebizserialout','custrecord_sku');
	var serialin = currentOrder.getValue('custitem_ebizserialin','custrecord_sku');

	var vserialized = "";
	/*var columns = nlapiLookupField('item', vitem, [ 'recordType',
	                                                'custitem_ebizserialout','custitem_ebizserialin' ]);
	var ItemType = columns.recordType;
	var serialout = columns.custitem_ebizserialout;
	var serialin = columns.custitem_ebizserialin;*/
	if (ItemType == "serializedinventoryitem" || serialout == 'T') {
		vserialized = 'T';
	} else {
		vserialized = 'F';
	}
	var vPresentQOH=vqty;
	try
	{
		if(vinvrefno != null && vinvrefno != '')
		{
			/*var vInvRec=nlapiLoadRecord('customrecord_ebiznet_createinv',vinvrefno);
			if(vInvRec != null && vInvRec != '')
			{
				vPresentQOH = vInvRec.getFieldValue('custrecord_ebiz_qoh');
			}	*/


			var filtersinvt = new Array();

			filtersinvt.push(new nlobjSearchFilter('internalid', null, 'anyof', vinvrefno));
			var columnsinvt = new Array();
			columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_qoh'); 
			var vinvtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);
			if(vinvtsearchresults!=null && vinvtsearchresults!='')
			{
				vPresentQOH = vinvtsearchresults[0].getValue('custrecord_ebiz_qoh');

			}

			nlapiLogExecution('ERROR', 'vPresentQOH', vPresentQOH);
		}
	}
	catch(e)
	{
		vPresentQOH = 0;
		nlapiLogExecution('ERROR', 'Exception in catch', vinvrefno+":"+e);
	}
	var vsono = vdono.split('.')[0];
	form.getSubList('custpage_items').setLineItemValue('custpage_lineno', i + 1, vline);
	form.getSubList('custpage_items').setLineItemValue('custpage_itemname', i + 1, vSKU);
	form.getSubList('custpage_items').setLineItemValue('custpage_ordqty', i + 1, vqty);
	form.getSubList('custpage_items').setLineItemValue('custpage_sono',i + 1, vsono);
	form.getSubList('custpage_items').setLineItemValue('custpage_recid', i + 1, vrecid);
	form.getSubList('custpage_items').setLineItemValue('custpage_lpno',i + 1, vLpno);
	form.getSubList('custpage_items').setLineItemValue('custpage_actbeginloc', i + 1, vlocationid);
	form.getSubList('custpage_items').setLineItemValue('custpage_actloc', i + 1, vlocationid);
	form.getSubList('custpage_items').setLineItemValue('custpage_itemno', i + 1, vitem);
	form.getSubList('custpage_items').setLineItemValue('custpage_invrefno', i + 1, vinvrefno);
	form.getSubList('custpage_items').setLineItemValue('custpage_previnvrefno', i + 1, vinvrefno);
	form.getSubList('custpage_items').setLineItemValue('custpage_itemstatus', i + 1, vskustatus);
	form.getSubList('custpage_items').setLineItemValue('custpage_itempackcode', i + 1, vpackcode);
	form.getSubList('custpage_items').setLineItemValue('custpage_dorefno', i + 1, vso);
	form.getSubList('custpage_items').setLineItemValue('custpage_dono',i + 1, vdono);
	form.getSubList('custpage_items').setLineItemValue('custpage_itembatchno', i + 1, vBatchno);
	form.getSubList('custpage_items').setLineItemValue('custpage_fromlp', i + 1, vfromlp);
	form.getSubList('custpage_items').setLineItemValue('custpage_kitrefno', i + 1, vkitrefno);
	form.getSubList('custpage_items').setLineItemValue('custpage_kilastitem', i + 1, lastkititem);
	form.getSubList('custpage_items').setLineItemValue('custpage_kititem', i + 1, kititem);
	form.getSubList('custpage_items').setLineItemValue('custpage_itemserialno', i + 1, vserialno);
	form.getSubList('custpage_items').setLineItemValue('custpage_serialized', i + 1, vserialized);
	form.getSubList('custpage_items').setLineItemValue('custpage_contlpno', i + 1, vContlpNo);
	form.getSubList('custpage_items').setLineItemValue('custpage_ebizwaveno', i + 1, vEbizWaveNo);
	form.getSubList('custpage_items').setLineItemValue('custpage_salesordinternid', i + 1, vSointernid);
	form.getSubList('custpage_items').setLineItemValue('custpage_wmslocation', i + 1, warehouseLocation);
	form.getSubList('custpage_items').setLineItemValue('custpage_company', i + 1, vCompany);
	form.getSubList('custpage_items').setLineItemValue('custpage_skustatusvalue', i + 1, vskustatusvalue);
	form.getSubList('custpage_items').setLineItemValue('custpage_sizeid', i + 1, vcontainersize);
	form.getSubList('custpage_items').setLineItemValue('custpage_itemtype', i + 1, ItemType);
	form.getSubList('custpage_items').setLineItemValue('custpage_serialout', i + 1, serialout);
	form.getSubList('custpage_items').setLineItemValue('custpage_serialin', i + 1, serialin);
	form.getSubList('custpage_items').setLineItemValue('custpage_displyedordqty', i + 1, vqty);
	form.getSubList('custpage_items').setLineItemValue('custpage_wmscarrier', i + 1, vWMSCarrier);
	form.getSubList('custpage_items').setLineItemValue('custpage_presentqoh', i + 1, vPresentQOH);

	var vCustomer=currentOrder.getValue('entity','custrecord_ebiz_order_no');
	var vStatus = currentOrder.getValue('status','custrecord_ebiz_order_no');

	if(vStatus == 'closed' || vStatus == 'cancelled')
	{
		vnotes = vStatus.toUpperCase();
		form.getSubList('custpage_items').setLineItemValue('custpage_paymenthold', i + 1, vnotes);
	}
	else
	{
		if(vnotes == 'HOLD')
			form.getSubList('custpage_items').setLineItemValue('custpage_paymenthold', i + 1, vnotes);	//Payment hold status
		else if((vnotes == null || vnotes == '') && (vDueDaysList != null && vDueDaysList != '') || (vCreditExceedList != null && vCreditExceedList != ''))
		{
			if(vDueDaysRestrict != null && vDueDaysRestrict != '' && parseInt(vDueDaysRestrict)>0)
			{
				for(var k=0;k<vDueDaysList.length;k++)
				{
					if(vDueDaysList[k].getValue('internalid',null,'group')==vCustomer)
					{					
						vnotes='Days Over Due';
						form.getSubList('custpage_items').setLineItemValue('custpage_paymenthold', i + 1, vnotes);	//Payment hold status
						return;
					}	
				} 
			}
			if(vCreditExceedList!=null && vCreditExceedList!='')
			{
				for(var k=0;k<vCreditExceedList.length;k++)
				{				
					if(vCreditExceedList[k].getValue('internalid',null,'group')==vCustomer)
					{
						vnotes='Credit Limit Exceed';
						form.getSubList('custpage_items').setLineItemValue('custpage_paymenthold', i + 1, vnotes);	//Payment hold status
						return;
					}	
				}  
			}
		}
		else
			form.getSubList('custpage_items').setLineItemValue('custpage_paymenthold', i + 1, vnotes);
	}
}

var searchResultArray=new Array();

//function getPickComfirmDetails(vQbWave,vQbdo)
function getPickComfirmDetails(request,form)
{
	// Validating all the request parameters and pushing to a local array
	var localVarArray;
	//this is to maintain the querystring parameters while refreshing the paging drop down
	if (request.getMethod() == 'POST') {
		var queryparams=request.getParameter('custpage_qeryparams');
		nlapiLogExecution('Error', 'queryparams', queryparams);
		var tempArray=new Array();
		tempArray.push(queryparams.split(','));
		localVarArray=tempArray;
		nlapiLogExecution('Error', 'tempArray', tempArray.length);
	}
	else
	{
		localVarArray = validateRequestParams(request,form);
		nlapiLogExecution('Error', 'localVarArray', localVarArray);
	}
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,
			'is', '9')); // statusflag='G'
	nlapiLogExecution('ERROR', 'vQbWave in func', localVarArray[0][1]);
	if (localVarArray[0][0] != "" && localVarArray[0][0] != null)
	{
		filters.push(new nlobjSearchFilter('name', null, 'is', localVarArray[0][0]));//sono
	}
	if (localVarArray[0][1] != "" && localVarArray[0][1] != null) 
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null,'is', localVarArray[0][1]));	//waveno
	}
	filters.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no',
			'is', 'T'));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[4] = new nlobjSearchColumn('custrecord_lpno');
	columns[5] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[6] = new nlobjSearchColumn('custrecord_sku');
	columns[7] = new nlobjSearchColumn('custrecord_invref_no');
	columns[8] = new nlobjSearchColumn('custrecord_sku_status');
	columns[9] = new nlobjSearchColumn('custrecord_packcode');		
	columns[10] = new nlobjSearchColumn('custrecord_line_no');
	columns[11] = new nlobjSearchColumn('custrecord_batch_no');
	columns[12] = new nlobjSearchColumn('custrecord_from_lp_no');
	columns[13] = new nlobjSearchColumn('custrecord_kit_refno');
	columns[14] = new nlobjSearchColumn('custrecord_last_member_component');
	columns[15] = new nlobjSearchColumn('custrecord_parent_sku_no');
	columns[16] = new nlobjSearchColumn('custrecord_serial_no');
	columns[17] = new nlobjSearchColumn('custrecord_comp_id');
	columns[18] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[19] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
	columns[20] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[21] = new nlobjSearchColumn('custrecord_wms_location');
	columns[22] = new nlobjSearchColumn('custrecord_container');
	columns[23] = new nlobjSearchColumn('custrecord_ebizwmscarrier');
	columns[24] = new nlobjSearchColumn('paymenteventresult','custrecord_ebiz_order_no');
	columns[25] = new nlobjSearchColumn('entity','custrecord_ebiz_order_no');
	columns[26] = new nlobjSearchColumn('status','custrecord_ebiz_order_no');

	columns[27] = new nlobjSearchColumn('custitem_ebizserialout','custrecord_sku');
	columns[28] = new nlobjSearchColumn('custitem_ebizserialin','custrecord_sku');
	columns[29] = new nlobjSearchColumn('type','custrecord_sku');

	columns[0].setSort();
	columns[10].setSort();
	columns[23].setSort();
	var searchResultArray = new Array();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if( searchresults!=null && searchresults!= "")
	{ 
		searchResultArray.push(searchresults); 
	}
	return searchResultArray;
}

function validateRequestParams(request,form){
	var soNo = "";
	var waveno = "";
	var localVarArray = new Array();
	if (request.getParameter('custpage_do') != null && request.getParameter('custpage_do') != "") {
		soNo = request.getParameter('custpage_do');
	}
	if (request.getParameter('ebiz_wave_no') != null && request.getParameter('ebiz_wave_no') != "") {
		waveno = request.getParameter('ebiz_wave_no');
	}
	nlapiLogExecution('ERROR','request.getParameter(ebiz_wave_no)',request.getParameter('ebiz_wave_no'));
	var currentRow = [soNo,waveno];
	localVarArray.push(currentRow);
	if (request.getParameter('custpage_qeryparams')==null )
	{
		var hiddenQueryParams=form.getField('custpage_qeryparams');
		nlapiLogExecution('ERROR','localVarArray',localVarArray.toString());
		hiddenQueryParams.setDefaultValue(localVarArray.toString());
	}
	return localVarArray;
}

function removeDuplicates(arrayName){
	var newArray = new Array();
	label:for (var i = 0; i < arrayName.length; i++) {
		for (var j = 0; j < newArray.length; j++) {
			if (newArray[j] == arrayName[i][0]) 
				continue label;
		}
		newArray[newArray.length] = arrayName[i][0];
	}
	return newArray;
}

function getCreditExceedList()
{ 	 
	var CrExceedCustSearch = nlapiSearchRecord('customer', 'customsearch_ebiz_customer_overdue', null, null);
	return CrExceedCustSearch;
}
function getDueDaysList()
{  
	var CrDueCustSearch = nlapiSearchRecord('customer', 'customsearch_ebiz_customer_due_days', null, null);
	return CrDueCustSearch;
}

function isPickFaceLocation(item,location)
{
	nlapiLogExecution('DEBUG', 'Into isPickFaceLocation');

	var str = 'item. = ' + item + '<br>';
	str = str + 'location. = ' + location + '<br>';	

	nlapiLogExecution('DEBUG', 'Function Parameters', str);

	var isPickFace='F';
	var PickFaceFilters = new Array();
	var PickFaceColumns = new Array();

	PickFaceFilters.push(new nlobjSearchFilter('custrecord_pickbinloc', null, 'anyof', location)); 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', item)); 
	PickFaceFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	
	PickFaceColumns[0] = new nlobjSearchColumn('custrecord_autoreplen');
	var PickFaceSearchResults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, PickFaceFilters, PickFaceColumns);
	if(PickFaceSearchResults!=null && PickFaceSearchResults!=''&& PickFaceSearchResults.length>0)
	{
		isPickFace='T';
	}

	nlapiLogExecution('DEBUG', 'Out of isPickFaceLocation - Return Value',isPickFace);

	return isPickFace;
}
//case# 20148436 starts
function vngetQOHForAllSKUsinPFLocation(location,item,WHLocId,itemstatus){

	nlapiLogExecution('DEBUG','Into vngetQOHForAllSKUsinPFLocation');

	var str = 'item. = ' + item + '<br>';
	str = str + 'location. = ' + location + '<br>';	
	str = str + 'WHLocId. = ' + WHLocId + '<br>';
	str = str + 'itemstatus. = ' + itemstatus + '<br>';
	nlapiLogExecution('DEBUG', 'Function Parameters', str);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', location));	
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', item));
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19','3']));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', WHLocId));	
	if(itemstatus !=null && itemstatus !='' && itemstatus!='undefined')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'anyof', itemstatus));
	//case# 20148436 ends
	var columns = new Array();	
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_sku_status');

	var pfSKUInvList = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

	nlapiLogExecution('DEBUG','Out of vngetQOHForAllSKUsinPFLocation - Return Value',pfSKUInvList);

	return pfSKUInvList;
}