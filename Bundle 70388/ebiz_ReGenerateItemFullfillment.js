/***************************************************************************
 eBizNET Solutions Inc .
 ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_ReGenerateItemFullfillment.js,v $
 *     	   $Revision: 1.3.4.1.8.2 $
 *     	   $Date: 2014/12/24 12:15:38 $
 *     	   $Author: snimmakayala $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_ReGenerateItemFullfillment.js,v $
 * Revision 1.3.4.1.8.2  2014/12/24 12:15:38  snimmakayala
 * Case#: 201411311
 *
 * Revision 1.3.4.1.8.1.4.1  2014/12/23 12:32:43  snimmakayala
 * Case#: 201411311
 *
 * Revision 1.3.4.1.8.1  2013/09/11 12:17:13  rmukkera
 * Case# 20124374
 *
 * Revision 1.3.4.1  2012/07/18 13:31:35  schepuri
 * CASE201112/CR201113/LOG201121
 * wbc issuefix
 *
 * Revision 1.3  2011/11/11 11:38:50  spendyala
 * CASE201112/CR201113/LOG201121
 * added functionality for display the exception and task details
 *
 * Revision 1.2  2011/11/07 08:50:07  schepuri
 * CASE201112/CR201113/LOG201121
 * to regenerate item fullfillment
 *
 * Revision 1.1  2011/11/04 12:36:20  schepuri
 * CASE201112/CR201113/LOG201121
 * to regenerate item fullfillment
 *
 *
 *****************************************************************************/

function ReGenerateItemFullfillmentSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Regenerate Itemfulfillment');


		var FullfillOrdNoField = form.addField('custpage_fullfillordno', 'select', 'Fulfillment Ord#');
		FullfillOrdNoField.setLayoutType('startrow', 'none');  		

		var filtersforderno = new Array();		
		filtersforderno[0] = new nlobjSearchFilter( 'custrecord_ebiz_nsconfirm_ref_no', null,'isempty');
		filtersforderno[1] = new nlobjSearchFilter( 'custrecord_tasktype', null, 'anyof', [3]);
		filtersforderno[2] = new nlobjSearchFilter( 'custrecord_wms_status_flag', null, 'anyof', [28] );
		filtersforderno[3] = new nlobjSearchFilter('status','custrecord_ebiz_order_no', 'noneof', ['SalesOrd:F','SalesOrd:G','SalesOrd:H']);

		FullfillOrdNoField.addSelectOption("","");

		var columnsforderno = new Array();
		columnsforderno[0] = new nlobjSearchColumn('name',null,'group').setSort(true);
		//columnsforderno[0].setSort();


		var fOrdsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersforderno,columnsforderno);

		/*for (var i = 0; fOrdsearchresults != null && i < fOrdsearchresults.length; i++) {
			//Searching for Duplicates		
			var res=  form.getField('custpage_fullfillordno').getSelectOptions(fOrdsearchresults[i].getId(), 'is');
			if (res != null) {
				//nlapiLogExecution('ERROR', 'res.length', res.length +fOrdsearchresults[i].getId() );
				if (res.length > 0) {
					continue;
				}
			}		
			FullfillOrdNoField.addSelectOption(fOrdsearchresults[i].getId(), fOrdsearchresults[i].getValue('name'));
		}		*/

		for (var i = 0; fOrdsearchresults != null && i < fOrdsearchresults.length; i++) {
			//Searching for Duplicates		
			var res=  form.getField('custpage_fullfillordno').getSelectOptions(fOrdsearchresults[i].getValue('name',null,'group'), 'is');
			if (res != null) {
				//nlapiLogExecution('ERROR', 'res.length', res.length +fOrdsearchresults[i].getId() );
				if (res.length > 0) {
					continue;
				}
			}		
			FullfillOrdNoField.addSelectOption(fOrdsearchresults[i].getValue('name',null,'group'), fOrdsearchresults[i].getValue('name',null,'group'));
		}		
		form.addSubmitButton('Display');


		nlapiLogExecution('ERROR','get','get');

		response.writePage(form);
	}
	else //this is the POST block
	{
		var vfullfillord = request.getParameter('custpage_fullfillordno');
		var FOarray = new Array();
		FOarray["custparm_fullfillordno"] = vfullfillord;

		response.sendRedirect('SUITELET', 'customscript_display_regn_itemfullfill', 'customdeploy_displayregn_itemfullfill_di', false, FOarray);
	}
}
