/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_CycleCountFIFODateConfirm.js,v $
 *     	   $Revision: 1.1.2.1.2.3.4.1 $
 *     	   $Date: 2015/11/16 15:35:43 $
 *     	   $Author: rmukkera $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_152 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 *
 *
 *
 *****************************************************************************/

function CycleCountFIFODateConfirm(request, response){
	if (request.getMethod() == 'GET') {
		//	Get the Error Message from the previous screen, which is passed as a parameter		
		var getError = request.getParameter('custparam_error');
		nlapiLogExecution('DEBUG', 'Into Request', getError);
		var screenNo=request.getParameter('custparam_screenno');
		var getPlanNo = request.getParameter('custparam_planno');
		nlapiLogExecution('ERROR', 'getPlanNo', getPlanNo);

		var getRecordId = request.getParameter('custparam_recordid');
		nlapiLogExecution('ERROR','getRecprdId',getRecordId);
		var getBeginLocationId = request.getParameter('custparam_begin_location_id');
		var getBeginLocationName =  request.getParameter('custparam_begin_location_name');
		var getPackCode =  request.getParameter('custparam_packcode');
		var getExpectedQuantity =  request.getParameter('custparam_expqty');
		var getExpLPNo = request.getParameter('custparam_lpno');
		var getActLPNo = request.getParameter('custparam_actlp');
		var getExpItem = request.getParameter('custparam_expitem');
		var getExpItemDescription = request.getParameter('custparam_expitemdescription');

		var getActualItem = request.getParameter('custparam_actitem');
		var itemInternalid = request.getParameter('custparam_expiteminternalid');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var getActualPackCode = request.getParameter('custparam_actpackcode');	
		var getBatchNo = request.getParameter('custparam_actbatch');
		var getMfgDate = request.getParameter('custparam_mfgdate');
		var getExpDate = request.getParameter('custparam_expdate');
		var getBestBeforeDate = request.getParameter('custparam_bestbeforedate');
		var ItemShelfLife = request.getParameter('custparam_shelflife');
		var CaptureExpiryDate = request.getParameter('custparam_captureexpirydate');
		var CaptureFifoDate = request.getParameter('custparam_capturefifodate');
		
		var getFifoDate = request.getParameter('custparam_fifodate');
		var getLastDate = request.getParameter('custparam_lastdate');
		var getFifocode = request.getParameter('custparam_fifocode');
		
		var planitem=request.getParameter('custparam_planitem');
		

		var functionkeyHtml=getFunctionkeyScript('_rf_error'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_error' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>CONFIRMATION: <br><label>" + getError + "</label>";
		html = html + "				<td><input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + "></td>";
		html = html + "				<input type='hidden' name='hdnCycleCountPlanNo' value=" + getPlanNo + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationName' value=" + getBeginLocationName + ">";
		html = html + "				<input type='hidden' name='hdnRecordId' value=" + getRecordId + ">";
		html = html + "				<input type='hidden' name='hdnPackCode' value=" + getPackCode + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";		
		html = html + "				<input type='hidden' name='hdnExpLPNo' value=" + request.getParameter('custparam_lpno') + "></td>";
		html = html + "				<input type='hidden' name='hdnActLPNo' value=" + request.getParameter('custparam_actlpno') + "></td>";
		html = html + "				<input type='hidden' name='hdnExpectedItemDescription' value=" + getExpItemDescription + ">";	
		html = html + "				<input type='hidden' name='hdniteminternalid' value=" + itemInternalid + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnnext' value=" + NextLocation + ">";
		html = html + "				<input type='hidden' name='hdnBatchNo' value='" + getBatchNo + "'>";
		html = html + "				<input type='hidden' name='hdnMfgDate' value=" + getMfgDate + ">";
		html = html + "				<input type='hidden' name='hdnExpDate' value=" + getExpDate + ">";
		html = html + "				<input type='hidden' name='hdnBestBeforeDate' value=" + getBestBeforeDate + ">";
		html = html + "				<input type='hidden' name='hdnItemShelfLife' value=" + ItemShelfLife + ">";
		html = html + "				<input type='hidden' name='hdnCaptureExpiryDate' value=" + CaptureExpiryDate + ">";
		html = html + "				<input type='hidden' name='hdnCaptureFifoDate' value=" + CaptureFifoDate + ">";
		html = html + "				<input type='hidden' name='hdnFifoDate' value=" + getFifoDate + ">";
		html = html + "				<input type='hidden' name='hdnLastDate' value=" + getLastDate + ">";
		html = html + "				<input type='hidden' name='hdnFifocode' value=" + getFifocode + ">";
		html = html + "				<input type='hidden' name='hdnplanitem' value='" + planitem + "'>";	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>CONTINUE <input name='cmdContinue' type='submit' value='F8'/>";
		html = html + "					BACK <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('ERROR', 'Into Response', 'Into Response');

		var optedEvent = request.getParameter('cmdPrevious');
		var getError = request.getParameter('custparam_error');
		var getScreenNo = request.getParameter('custparam_screenno');
		nlapiLogExecution('ERROR', 'Screen No', getScreenNo);
		nlapiLogExecution('ERROR', 'optedEvent', optedEvent);

		var getBatchNo=request.getParameter('hdnBatchNo');
		nlapiLogExecution('ERROR','getBatchNo',getBatchNo);
		var ItemId= request.getParameter('hdnItemId');
		var locationId = request.getParameter('hdnLocationId');
		nlapiLogExecution('ERROR', 'locationId tst ', locationId);
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var Itemtype= request.getParameter('hdnitemType');

		var CCarray = new Array();
		CCarray["custparam_error"] ='ERROR IN FIFO DATE';
		CCarray["custparam_screenno"] = '35B';
		CCarray["custparam_recordid"] = request.getParameter('custparam_recordid');
		CCarray["custparam_planno"] = request.getParameter('custparam_planno');
		CCarray["custparam_begin_location_id"] = request.getParameter('custparam_begin_location_id');
		CCarray["custparam_begin_location_name"] = request.getParameter('custparam_begin_location_name');
		CCarray["custparam_lpno"] = request.getParameter('custparam_lpno');
		CCarray["custparam_packcode"] = request.getParameter('custparam_packcode');
		CCarray["custparam_skustatus"] = request.getParameter('custparam_skustatus');
		CCarray["custparam_expqty"] = request.getParameter('custparam_expqty');
		CCarray["custparam_actlp"] = request.getParameter('custparam_actlp');
		CCarray["custparam_expitem"] = request.getParameter('custparam_expitem');
		CCarray["custparam_expitemdescription"] = request.getParameter('custparam_expitemdescription');

		CCarray["custparam_actitem"] = request.getParameter('custparam_actitem');
		CCarray["custparam_actpackcode"] = request.getParameter('custparam_actpackcode');
		CCarray["custparam_expiteminternalid"] = request.getParameter('hdniteminternalid');
		CCarray["custparam_noofrecords"] = request.getParameter('hdnRecCount');
		CCarray["custparam_nextlocation"] = request.getParameter('hdnnext');
		CCarray["custparam_skustatus"] = request.getParameter('custparam_skustatus');
		CCarray["custparam_actbatch"] = request.getParameter('hdnBatchNo');
		CCarray["custparam_mfgdate"] = request.getParameter('hdnMfgDate');
		CCarray["custparam_expdate"] = request.getParameter('hdnExpDate');
		CCarray["custparam_bestbeforedate"] = request.getParameter('hdnBestBeforeDate');			
		CCarray["custparam_shelflife"] = request.getParameter('hdnItemShelfLife');
		CCarray["custparam_captureexpirydate"] = request.getParameter('hdnCaptureExpiryDate');
		CCarray["custparam_capturefifodate"] = request.getParameter('hdnCaptureFifoDate');
		CCarray["custparam_lastdate"]=request.getParameter('hdnLastDate');
		CCarray["custparam_fifodate"]=request.getParameter('hdnFifoDate');
		CCarray["custparam_fifocode"]=request.getParameter('hdnFifocode');
		CCarray["custparam_planitem"] = request.getParameter('hdnplanitem');
		var itemShelflife = request.getParameter('hdnItemShelfLife');
		var CaptureExpirydate = request.getParameter('hdnCaptureExpiryDate');
		var CaptureFifodate = request.getParameter('hdnCaptureFifoDate');

		nlapiLogExecution('ERROR', 'CCarray["custparam_lastdate"]', CCarray["custparam_lastdate"]);
		nlapiLogExecution('ERROR', 'CCarray["custparam_fifodate"]', CCarray["custparam_fifodate"]);
		nlapiLogExecution('ERROR', 'CCarray["custparam_fifocode"]', CCarray["custparam_fifocode"]);


		if (optedEvent == 'F7') {
			nlapiLogExecution('ERROR', 'CHECK IN FIFO DATE F7 Pressed');
			response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_fifo_date', 'customdeploy_rf_cyclecount_fifo_date_di', false, CCarray);

		}
		else
		{
			var rec=CreateBatchEntryRec(CCarray);
			response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_quantity', 'customdeploy_rf_cyclecount_quantity_di', false, CCarray);
		}
		nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
		nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
	}
}


/**
 * @param CCarray
 */
function CreateBatchEntryRec(CCarray)
{
	try {
		var site="";
		nlapiLogExecution('ERROR', 'in');

		nlapiLogExecution('ERROR', 'BATCH NOT FOUND');
		var customrecord = nlapiCreateRecord('customrecord_ebiznet_batch_entry');

		customrecord.setFieldValue('name', CCarray["custparam_actbatch"]);
		customrecord.setFieldValue('custrecord_ebizlotbatch', CCarray["custparam_actbatch"]);
		customrecord.setFieldValue('custrecord_ebizmfgdate', CCarray["custparam_mfgdate"]);
		customrecord.setFieldValue('custrecord_ebizbestbeforedate', CCarray["custparam_bestbeforedate"]);
		customrecord.setFieldValue('custrecord_ebizexpirydate', CCarray["custparam_expdate"]);
		customrecord.setFieldValue('custrecord_ebizfifodate', CCarray["custparam_fifodate"]);
		customrecord.setFieldValue('custrecord_ebizfifocode', CCarray["custparam_fifocode"]);
		customrecord.setFieldValue('custrecord_ebizlastavldate', CCarray["custparam_lastdate"]);
		nlapiLogExecution('ERROR', '1');
		
		//customrecord.setFieldText('custrecord_ebizsku', getPOItem);
		customrecord.setFieldValue('custrecord_ebizsku', CCarray["custparam_actitem"]);
		customrecord.setFieldValue('custrecord_ebizpackcode',CCarray["custparam_packcode"]);


		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto', CCarray["custparam_planno"]);

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_cyclesite_id');

		nlapiLogExecution('ERROR', 'Before Search Results Id', 'getSearchResultsId');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, column);
		if(searchresults)
			site=searchresults[0].getValue('custrecord_cyclesite_id');
		if(site!=null&&site!="")
			//customrecord.setFieldValue('custrecord_ebizsitebatch',site);
		var rec = nlapiSubmitRecord(customrecord, false, true);
		nlapiLogExecution('ERROR', 'rec',rec);

	} 
	catch (e) {
		nlapiLogExecution('ERROR', 'Failed to Update/Insert into BATCH ENTRY Record',e);
	}
}













