/***************************************************************************
������������������������������� � ����������������������������� ��eBizNET
�����������������������                           eBizNET Solutions Inc
****************************************************************************
*
*� $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Suitelet/ebiz_WMSLicenseusagereport_SL.js,v $
*� $Revision: 1.1.2.2.4.1.4.2 $
*� $Date: 2013/09/11 14:14:42 $
*� $Author: gkalla $
*� $Name: t_NSWMS_2014_1_1_174 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_WMSLicenseusagereport_SL.js,v $
*� Revision 1.1.2.2.4.1.4.2  2013/09/11 14:14:42  gkalla
*� Case# 20124369
*� Added inactive filter
*�
*� Revision 1.1.2.2.4.1.4.1  2013/05/14 14:49:54  grao
*� CASE201112/CR201113/LOG201121
*� Standard bundle issues fixes
*�
*� Revision 1.1.2.2.4.1  2012/11/01 14:55:15  schepuri
*� CASE201112/CR201113/LOG201121
*� Decimal Qty Conversions
*�
*� Revision 1.1.2.2  2012/02/27 14:42:22  rrpulicherla
*� CASE201112/CR201113/LOG201121
*�
*� WMSLicenseReport
*�
*� Revision 1.1  2012/02/27 14:35:38  rmukkera
*� CASE201112/CR201113/LOG201121
*� new file
*�
*
****************************************************************************/

function ebiznet_WMSLicenseReport_SL(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('WMS License Usage Report');     

		var sublist = form.addSubList("custpage_wmslicusgereport", "staticlist", "Report");
				sublist.addField("custpage_noofusers", "text", "# of users with WMS Admin Role");
		sublist.addField("custpage_noofuniqueusers", "text", "# of unique user");
		sublist.addField("custpage_noofmaxusersperday", "text", "# of max users / day");
		sublist.addField("custpage_nooflocations", "text", "# of locations with WH flag");
		
//		var noofuserswithWMSAdminRole=form.addField("custpage_noofusers", "text", "# of users with WMS Admin Role :").setDisplayType("inline");	
//		var noofUniqueUser=form.addField("custpage_noofuniqueusers", "text", "# of unique user :").setDisplayType("inline");
//		var noofMaxUsersPerDay=form.addField("custpage_noofmaxusersperday", "text", "# of max users / day : ").setDisplayType("inline");
//		var noofLocationsWithWHFlag=form.addField("custpage_nooflocations", "text", "# of locations with WH flag :").setDisplayType("inline");
//		noofuserswithWMSAdminRole.setLayoutType('outside','startrow');
//		noofUniqueUser.setLayoutType('outside','startrow');
//		noofMaxUsersPerDay.setLayoutType('outside','startrow');
		
		var roles=form.addField("custpage_no", "select", "roles :",'-118').setDisplayType('hidden');
		var options = roles.getSelectOptions('WMS Admin');
		nlapiLogExecution('ERROR', options[0].getId() + ',' + options[0].getText() ); 
		var wmsadminRoleId=options[0].getId();
		
		//# of unique user id in open task + closed task
		var opentaskFilters = new Array();		
		opentaskFilters[0] = new nlobjSearchFilter('isinactive', 'custrecord_ebizuser', 'is' ,'F');
		var opentaskColumns = new Array();
		opentaskColumns[0] = new nlobjSearchColumn('custrecord_ebizuser',null,'count'); 
		
		var opentaskSearchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,opentaskFilters,opentaskColumns);
		var opentaskUserCount = opentaskSearchresult[0].getValue('custrecord_ebizuser',null,'count');

		var closedtaskFilters = new Array();		
		closedtaskFilters[0] = new nlobjSearchFilter('isinactive', 'custrecord_ebiztask_ebizuser_no', 'is' ,'F');
		var closetaskColumns = new Array();
		closetaskColumns[0] = new nlobjSearchColumn('custrecord_ebiztask_ebizuser_no',null,'count'); 
		var closedtaskSearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask',null,closedtaskFilters,closetaskColumns );
		var closedtaskUserCount = closedtaskSearchresults[0].getValue('custrecord_ebiztask_ebizuser_no',null,'count');


		var uniqueUserId=0;
		if(opentaskUserCount!=null && opentaskUserCount!='')
		{
			uniqueUserId=parseFloat(uniqueUserId)+parseFloat(opentaskUserCount);
		}
		if(closedtaskUserCount!=null && closedtaskUserCount!='')
		{
			uniqueUserId=parseFloat(uniqueUserId)+parseFloat(closedtaskUserCount);
		}
		form.getSubList('custpage_wmslicusgereport').setLineItemValue('custpage_noofuniqueusers', 1, uniqueUserId.toString());
		//noofUniqueUser.setDefaultValue(uniqueUserId.toString());

		//# of max users / day in Open+Closed  Task 
		var date=DateStamp();
		
		
		
		var opentaskmaxusersFilters = new Array();
		opentaskmaxusersFilters[0] = new nlobjSearchFilter('custrecord_current_date', null, 'on' ,date);
		opentaskmaxusersFilters[1] = new nlobjSearchFilter('isinactive', 'custrecord_ebizuser', 'is' ,'F');
		var opentaskmaxusersColumns = new Array();
		opentaskmaxusersColumns[0] = new nlobjSearchColumn('custrecord_ebizuser',null,'count'); 
		var opentaskMaxUsersSearchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,opentaskmaxusersFilters,opentaskmaxusersColumns);
		var opentaskMaxUserCountPerDay;
		if(opentaskMaxUsersSearchresult!=null)
		{
			opentaskMaxUserCountPerDay = opentaskMaxUsersSearchresult[0].getValue('custrecord_ebizuser',null,'count');
		}
		var closetaskmaxusersFilters = new Array();
		closetaskmaxusersFilters[0] = new nlobjSearchFilter('custrecord_ebiztask_current_date', null, 'on' ,date);
		closetaskmaxusersFilters[1] = new nlobjSearchFilter('isinactive', 'custrecord_ebiztask_ebizuser_no', 'is' ,'F');
		var closetaskmaxusersColumns = new Array();
		closetaskmaxusersColumns[0] = new nlobjSearchColumn('custrecord_ebiztask_ebizuser_no',null,'count'); 
		var closetaskMaxUsersSearchresult = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask',null,closetaskmaxusersFilters,closetaskmaxusersColumns);
		var closetaskMaxUserCountPerDay ;
		if(closetaskMaxUsersSearchresult!=null)
		{

			closetaskMaxUserCountPerDay = closetaskMaxUsersSearchresult[0].getValue('custrecord_ebiztask_ebizuser_no',null,'count');
		}

		var maxUserPerDay=0;
		if(opentaskMaxUserCountPerDay!=null && opentaskMaxUserCountPerDay!='')
		{
			maxUserPerDay=parseFloat(maxUserPerDay)+parseFloat(opentaskMaxUserCountPerDay);
		}
		if(closetaskMaxUserCountPerDay!=null && closetaskMaxUserCountPerDay!='')
		{
			maxUserPerDay=parseFloat(maxUserPerDay)+parseFloat(closetaskMaxUserCountPerDay);
		}
		
		form.getSubList('custpage_wmslicusgereport').setLineItemValue('custpage_noofmaxusersperday', 1, maxUserPerDay.toString());
		//noofMaxUsersPerDay.setDefaultValue(maxUserPerDay.toString());

		//# of users with WMS Admin Role
		var employeeFilters = new Array();
		employeeFilters[0] = new nlobjSearchFilter('role', null, 'anyof' ,wmsadminRoleId);
		employeeFilters[1] = new nlobjSearchFilter('isinactive', null, 'is' ,'F');
		var employeeColumns = new Array();
		employeeColumns[0] = new nlobjSearchColumn('firstname'); 
		var employeeSearchresult = nlapiSearchRecord('employee',null,employeeFilters ,employeeColumns );
		var noofusersWithWmsRole;
		if(employeeSearchresult!=null)
		{
			noofusersWithWmsRole = employeeSearchresult.length;
		}
		if(noofusersWithWmsRole==null || noofusersWithWmsRole=='')
		{
			noofusersWithWmsRole='0';
		}

		form.getSubList('custpage_wmslicusgereport').setLineItemValue('custpage_noofusers', 1, noofusersWithWmsRole.toString());
		//noofuserswithWMSAdminRole.setDefaultValue(noofusersWithWmsRole.toString());

		//# of locations where Make WH Flag = �Y� (True)
		var locationFilters = new Array();
		locationFilters[0] = new nlobjSearchFilter('custrecord_ebizwhsite', null, 'is' ,'T');
		locationFilters[1] = new nlobjSearchFilter('isinactive', null, 'is' ,'F');
		var locationColumns = new Array();
		locationColumns[0] = new nlobjSearchColumn('name'); 
		var locationSearchresult = nlapiSearchRecord('location',null,locationFilters ,locationColumns );
		var nooflocWithWHflag;
		if(locationSearchresult!=null)
		{
			nooflocWithWHflag = locationSearchresult.length;
		}
		if(nooflocWithWHflag==null || nooflocWithWHflag=='')
		{
			nooflocWithWHflag='0';
		}
		form.getSubList('custpage_wmslicusgereport').setLineItemValue('custpage_nooflocations', 1, nooflocWithWHflag.toString());
		//noofLocationsWithWHFlag.setDefaultValue(nooflocWithWHflag.toString());

		response.writePage(form);
	}


}