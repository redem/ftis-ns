/***************************************************************************
  eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_CheckInSerial.js,v $
 *     	   $Revision: 1.5.4.13.4.6.2.31.2.1 $
 *     	   $Date: 2015/12/01 13:03:53 $
 *     	   $Author: schepuri $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_CheckInSerial.js,v $
 * Revision 1.5.4.13.4.6.2.31.2.1  2015/12/01 13:03:53  schepuri
 * case# 201415871,201415916
 *
 * Revision 1.5.4.13.4.6.2.31  2015/08/07 15:38:44  grao
 * 2015.2   issue fixes  201413897
 *
 * Revision 1.5.4.13.4.6.2.30  2015/07/22 15:42:42  grao
 * 2015.2   issue fixes  201413426
 *
 * Revision 1.5.4.13.4.6.2.29  2014/06/13 07:05:17  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.5.4.13.4.6.2.28  2014/06/06 14:58:06  skavuri
 * Case# 20148759, 20148761 SB Issue Fixed
 *
 * Revision 1.5.4.13.4.6.2.27  2014/06/06 08:02:13  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.5.4.13.4.6.2.26  2014/05/30 00:26:49  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.5.4.13.4.6.2.25  2014/02/27 15:27:45  skavuri
 * Case # 20127404 issue fixed
 *
 * Revision 1.5.4.13.4.6.2.24  2014/02/10 15:18:05  skreddy
 * case 20127054 & 20127097
 * Standard Bundle issue fix
 *
 * Revision 1.5.4.13.4.6.2.23  2014/01/07 14:42:03  grao
 * Case# 20126675 related issue fixes in Sb issue fixes
 *
 * Revision 1.5.4.13.4.6.2.22  2013/12/02 08:59:59  schepuri
 * 20126048
 *
 * Revision 1.5.4.13.4.6.2.21  2013/11/22 14:17:47  schepuri
 * 20125825
 *
 * Revision 1.5.4.13.4.6.2.20  2013/10/31 15:29:27  rmukkera
 * no message
 *
 * Revision 1.5.4.13.4.6.2.19  2013/10/18 15:47:20  skreddy
 * Case# 20124997
 * Affosa SB  issue fix
 *
 * Revision 1.5.4.13.4.6.2.18  2013/09/26 15:47:37  rmukkera
 * Case# 20124514�
 *
 * Revision 1.5.4.13.4.6.2.17  2013/09/18 14:34:46  rmukkera
 * Case# 20124352
 *
 * Revision 1.5.4.13.4.6.2.16  2013/09/05 00:04:18  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Afosa changes
 * case#20123755
 *
 * Revision 1.5.4.13.4.6.2.15  2013/07/03 15:58:14  nneelam
 * Case# 20123249
 * Cart put away through RF Issue Fix.
 *
 * Revision 1.5.4.13.4.6.2.14  2013/06/24 15:21:14  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *  Case# 20123159
 * Surftech serial#issue
 *
 * Revision 1.5.4.13.4.6.2.13  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.5.4.13.4.6.2.12  2013/06/07 15:37:17  mbpragada
 * 20120490
 * Location not generating for TO
 *
 * Revision 1.5.4.13.4.6.2.11  2013/05/31 15:38:24  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle Issue Fixes
 *
 * Revision 1.5.4.13.4.6.2.10  2013/05/16 14:41:00  schepuri
 * TO check in with duplicate items  - surftech sb
 *
 * Revision 1.5.4.13.4.6.2.9  2013/05/15 01:31:23  kavitha
 * CASE201112/CR201113/LOG201121
 * TSG Issue fixes
 *
 * Revision 1.5.4.13.4.6.2.8  2013/05/06 15:11:39  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.5.4.13.4.6.2.7  2013/05/02 14:24:47  schepuri
 * updating remaning cube issue fix
 *
 * Revision 1.5.4.13.4.6.2.6  2013/05/01 12:45:35  rmukkera
 * Fix related to posting the serial numbers that exists on the item fulfillment
 *
 * Revision 1.5.4.13.4.6.2.5  2013/04/17 16:04:02  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.5.4.13.4.6.2.4  2013/04/01 20:59:31  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.5.4.13.4.6.2.3  2013/03/15 09:28:43  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.5.4.13.4.6.2.2  2013/03/12 06:44:57  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Merged from Monobind.
 *
 * Revision 1.5.4.13.4.6.2.1  2013/03/01 14:34:59  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.5.4.13.4.6  2013/02/07 15:10:34  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.5.4.13.4.5  2012/11/27 17:50:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Serial Capturing file.
 *
 * Revision 1.5.4.13.4.4  2012/11/01 14:55:35  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.5.4.13.4.3  2012/09/27 10:56:31  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.5.4.13.4.2  2012/09/26 12:48:01  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.5.4.13.4.1  2012/09/24 10:07:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.5.4.13  2012/09/11 13:18:46  spendyala
 * CASE201112/CR201113/LOG201121
 * Depending Upon the cart lp, we redirect user to there respective menu screen.
 * i.e., if cartlp is not null then it is redirected to cart criteria menu if not if will redirect to checkin criteria menu.
 *
 * Revision 1.5.4.12  2012/09/06 06:34:09  spendyala
 * CASE201112/CR201113/LOG201121
 * Missing parameter CartonLp while passing to query string.
 *
 * Revision 1.5.4.11  2012/09/03 13:45:29  schepuri
 * CASE201112/CR201113/LOG201121
 * added date stamp
 *
 * Revision 1.5.4.10  2012/06/11 13:30:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Passing parameter value to query string.
 *
 * Revision 1.5.4.9  2012/05/28 09:35:17  gkalla
 * CASE201112/CR201113/LOG201121
 * Issue fix for QC Check
 *
 * Revision 1.5.4.8  2012/05/14 14:43:01  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to QC check.
 *
 * Revision 1.5.4.7  2012/04/27 07:47:42  gkalla
 * CASE201112/CR201113/LOG201121
 * QC check based on rule configuration
 *
 * Revision 1.5.4.6  2012/04/16 15:33:14  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Checkin Serial
 *
 * Revision 1.5.4.5  2012/04/13 13:51:52  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to Creating invt record with Inbound stage is added.
 *
 * Revision 1.5.4.4  2012/03/27 09:14:29  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing
 *
 * Revision 1.5.4.3  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.5.4.2  2012/03/12 14:00:56  spendyala
 * CASE201112/CR201113/LOG201121
 *  Transaction order line table is not updating For a serial item.
 *
 * Revision 1.5.4.1  2012/02/22 12:33:21  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.5  2011/09/24 15:51:43  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RMA Functionality New Files
 *
 * Revision 1.4  2011/06/30 15:20:33  rgore
 * CASE201112/CR201113/LOG201121
 * Added necessary ';' to remove any code warnings
 * Ratnakar
 * 30 June 2011
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/

function CheckInSerial(request, response){
	if (request.getMethod() == 'GET') {
		nlapiLogExecution('DEBUG', 'Into CheckinSerial');
		var getUOMIDText = request.getParameter('custparam_uomidtext');
		var getUOMID = request.getParameter('custparam_uomid');
		var getNumber = request.getParameter('custparam_number');
		var getLP = request.getParameter('custparam_polineitemlp');
		var getItemLP = getLP;
		var getReceipt = request.getParameter('custparam_poreceiptno');
		var getUOMQty = request.getParameter('custparam_uomqty');
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');

		var cartprocess = request.getParameter('custparam_cartprocess');

		var getPONo = request.getParameter('custparam_poid');
		var getPOItem = request.getParameter('custparam_poitem');
		var getPOLineNo = request.getParameter('custparam_lineno');
		var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		var getPOInternalId = request.getParameter('custparam_pointernalid');
		var getPOQtyEntered = request.getParameter('custparam_poqtyentered');
		var getPOItemRemainingQty = request.getParameter('custparam_poitemremainingqty');
		var getPOLinePackCode = request.getParameter('custparam_polinepackcode');
		var getPOLineItemStatus = request.getParameter('custparam_polineitemstatus');
		var getItemCube = request.getParameter('custparam_itemcube');
		var getItemQuantity = request.getParameter('custparam_poqtyentered');
		var getItemQuantityReceived = request.getParameter('hdnQuantityReceived');
		var getBaseUomQty = request.getParameter('custparam_baseuomqty');
		nlapiLogExecution('DEBUG', 'getBaseUomQty', getBaseUomQty);
		var getBatchNo = request.getParameter('custparam_batchno');
		var getMfgDate = request.getParameter('custparam_mfgdate');
		var getExpDate = request.getParameter('custparam_expdate');
		var getBestBeforeDate = request.getParameter('custparam_bestbeforedate');
		var getLastAvlDate = request.getParameter('custparam_lastdate');
		var getFifoDate = request.getParameter('custparam_fifodate');
		var getFifoCode = request.getParameter('custparam_fifocode');
		var getWHLocation = request.getParameter('custparam_whlocation');
		var getPOLineItemStatusValue = request.getParameter('custparam_polineitemstatusValue');
		nlapiLogExecution('DEBUG','getPOLineItemStatusValue',getPOLineItemStatusValue);

		var putmethod = request.getParameter('custparam_putmethod');
		var putrule = request.getParameter('custparam_putrule');
		var getSerialArr = request.getParameter('custparam_serialno');
		nlapiLogExecution('DEBUG', 'putmethod in get',putmethod);
		nlapiLogExecution('DEBUG', 'putrule in get',putrule);

		var getremainingCube = request.getParameter('custparam_remainingCube');
		nlapiLogExecution('DEBUG', 'getremainingCube', getremainingCube);

		var vCartLpno = request.getParameter('custparam_cartlpno');
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
		nlapiLogExecution('ERROR', 'getNumber', getNumber);
		if(getNumber != null && getNumber != '' && parseInt(getNumber)==0)
		{
			var filterssertemp1 = new Array();
			//filterssertemp1[0] = new nlobjSearchFilter('custrecord_tempserialnumber', null, 'is', getSerialNo);
			filterssertemp1[0] = new nlobjSearchFilter('custrecord_tempserialstatus', null, 'is', 'F');
			filterssertemp1[1] = new nlobjSearchFilter('custrecord_tempserialpolineno', null, 'equalto', getPOLineNo);
			filterssertemp1[2] = new nlobjSearchFilter('custrecord_tempserialorderno', null, 'anyof', getPOInternalId);
			var columnssertemp1 = new Array();
			columnssertemp1[0] = new nlobjSearchColumn('custrecord_tempserialnumber');
			columnssertemp1[1] = new nlobjSearchColumn('name');
			var SrchRecordTmpSerial1 = nlapiSearchRecord('customrecord_ebiznettempserialentry', null, filterssertemp1,columnssertemp1);
			if(SrchRecordTmpSerial1 != null && SrchRecordTmpSerial1 != '')
			{	
				var parent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); //create new parent record on fo line.
				var foparentid = nlapiSubmitRecord(parent); 
				var foparent = nlapiLoadRecord('customrecord_ebiz_throwaway_parent', foparentid);

				for (var j = 0; j < SrchRecordTmpSerial1.length; j++) {
					var TempRecord=SrchRecordTmpSerial1[j];
					foparent.selectNewLineItem('recmachcustrecord_ebiz_tempserial_child');
					nlapiLogExecution('ERROR', 'TempRecord.getId()',TempRecord.getId());
					nlapiLogExecution('ERROR', 'TempRecord.getValue(name)',TempRecord.getValue('name'));
					foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_tempserial_child', 'id', TempRecord.getId());
					foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_tempserial_child','name', TempRecord.getValue('name'));
					foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_tempserial_child','custrecord_ebiz_tempserial_note1', 'because of discontinue of serial number scanning we have marked this serial number as closed');
					foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_tempserial_child','custrecord_tempserialstatus', 'T');

					foparent.commitLineItem('recmachcustrecord_ebiz_tempserial_child');
				}
				nlapiSubmitRecord(foparent);
			}	
		}
		var st0,st1,st2,st3,st4,st5;
		// 20126048
		//case 20124997 Start : added one more if condition
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{//case 20124997 end
			st0 = "";
			st1 = "UOM ID";
			st2 = "INGRESAR N&#218;MERO DE SERIE:";
			st3 = "ID PADRE";
			st4 = "ENVIAR";
			st5 = "ANTERIOR";
			st6 = "DE";

		}
		else
		{
			st0 = "";
			st1 = "UOM ID ";
			st2 = "ENTER SERIAL NO:";
			st3 = "PARENT ID ";
			st4 = "SEND";
			st5 = "PREV";
			st6 = "OF";

		}
		var functionkeyHtml=getFunctionkeyScript('_rf_checkin_serial_no'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";		
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";     
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends 
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		//html = html + " document.getElementById('enterserialno').focus();";        
		html = html + "function stopRKey(evt) { ";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";		
		html = html + "	} ";
		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "	document.onkeydown = function(){";
		html = html + "		if(window.event && window.event.keyCode == 116)";
		html = html + "		        {";
		html = html + "		    window.event.keyCode = 505;";
		html = html + "		      }";
		html = html + "		if(window.event && window.event.keyCode == 505)";
		html = html + "		        { ";
		html = html + "		    return false;";
		html = html + "		    }";
		html = html + "		}";
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_checkin_serial_no' method='POST' >";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 +": <label>" + getUOMIDText + "</label>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				<input type='hidden' name='hdnItemStatus' value=" + getPOLineItemStatus + ">";
		html = html + "				<input type='hidden' name='hdnItemPackCode' value=" + getPOLinePackCode + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getItemQuantity + ">";
		html = html + "				<input type='hidden' name='hdnQuantityReceived' value=" + getItemQuantityReceived + ">";
		html = html + "				<input type='hidden' name='hdnItemCube' value=" + getItemCube + ">";
		html = html + "				<input type='hidden' name='hdnbaseuomqty' value=" + getBaseUomQty + ">";
		html = html + "				<input type='hidden' name='hdncartpro' value=" + cartprocess + ">";

		html = html + "				<input type='hidden' name='hdnItemRemaininingQuantity' value=" + parseFloat(getPOItemRemainingQty) + ">";
		html = html + "				<input type='hidden' name='hdnPOQuantityEntered' value=" + parseFloat(getPOItemRemainingQty) + ">";
		html = html + "				<input type='hidden' name='hdnPOInternalId' value=" + getPOInternalId + ">";
		html = html + "				<input type='hidden' name='hdnBatchNo' value=" + getBatchNo + ">";
		html = html + "				<input type='hidden' name='hdnMfgDate' value=" + getMfgDate + ">";
		html = html + "				<input type='hidden' name='hdnExpDate' value=" + getExpDate + ">";
		html = html + "				<input type='hidden' name='hdnBestBeforeDate' value=" + getBestBeforeDate + ">";
		html = html + "				<input type='hidden' name='hdnLastAvlDate' value=" + getLastAvlDate + ">";
		html = html + "				<input type='hidden' name='hdnFifoDate' value=" + getFifoDate + ">";
		html = html + "				<input type='hidden' name='hdnFifoCode' value=" + getFifoCode + ">";
		html = html + "				<input type='hidden' name='hdnUOMID' value=" + getUOMID + ">";
		html = html + "				<input type='hidden' name='hdnUOMText' value=" + getUOMIDText + ">";
		html = html + "				<input type='hidden' name='hdnNumber' value=" + getNumber + ">";
		html = html + "				<input type='hidden' name='hdnLP' value=" + getLP + ">";
		html = html + "				<input type='hidden' name='hdnItemQuantity' value=" + getItemQuantity + ">";
		html = html + "				<input type='hidden' name='hdnReceipt' value=" + getReceipt + ">";
		html = html + "				<input type='hidden' name='hdnUOMQty' value=" + getUOMQty + ">";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + getWHLocation + ">";
		html = html + "				<input type='hidden' name='hdnvCartLpno' value=" + vCartLpno + ">";
		html = html + "				<input type='hidden' name='hdnSerialArr' value=" + getSerialArr + ">";
		html = html + "				<input type='hidden' name='hdnvputmethod' value='" + putmethod + "'>";
		html = html + "				<input type='hidden' name='hdnvputrule' value='" + putrule + "'>";
		html = html + "				<input type='hidden' name='hdnremainingCube' value=" + getremainingCube + ">";
		html = html + "				<input type='hidden' name='hdnItemStatusNo' value=" + getPOLineItemStatusValue + "></td>";


		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + (parseFloat(getNumber) + 1) + " OF <label>" + getItemQuantity + "</label>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> " + st2;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterserialno' id='enterserialno' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + ": <label>" + getLP + "</label>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st4 +" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					" + st5 +" <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterserialno').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var getUOMID = request.getParameter('hdnUOMID');
		var getNumber = request.getParameter('hdnNumber');
		var getBeginLocation = request.getParameter('custparam_location');
		var getSerialNo = request.getParameter('enterserialno');
		var getItemQuantity = request.getParameter('hdnItemQuantity');
		var getLP = request.getParameter('hdnLP');
		var getReceipt = request.getParameter('hdnReceipt');
		var getUOMQty = request.getParameter('hdnUOMQty');

		nlapiLogExecution('DEBUG', 'getReceipt', getReceipt);
		nlapiLogExecution('DEBUG', 'getNumber', getNumber);
		nlapiLogExecution('DEBUG', 'getSerialNo', getSerialNo);
		nlapiLogExecution('DEBUG', 'getItemQuantity', getItemQuantity);
		nlapiLogExecution('DEBUG', 'getLP', getLP);
		var POarray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);

		//case 20124997 Start :spanish conversion
		var st5,st6,st7;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st5 = "NO V&#193;LIDA DE SERIE";
			st6 = "SERIAL NO. YA EXISTE";
			st7 = "SERIAL NO. ya escaneados";
			st8 = "N�MERO DE SERIE NO PUEDE CONTENER EL CAR&#193;CTER espacio";

		}
		else
		{
			st5 = "INVALID SERIAL NO";
			st6 = "SERIAL NO. ALREADY EXISTS";
			st7 = "SERIAL NO. ALREADY SCANNED"; 
			st8 = "Serial numbers may not contain the 'Space' character"; 

		}
		//case 20124997 end
		var EndLocationArray = new Array();

		//	Get the PO# from the previous screen, which is passed as a parameter		
		var getPONo = request.getParameter('custparam_poid');
		var getPOItem = request.getParameter('custparam_poitem');
		var getPOLineNo = request.getParameter('custparam_lineno');
		var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		var getPOInternalId = request.getParameter('custparam_pointernalid');
		var getPOQtyEntered = request.getParameter('custparam_poqtyentered');
		var getPOItemRemainingQty = request.getParameter('custparam_poitemremainingqty');
		var getPOLinePackCode = request.getParameter('custparam_polinepackcode');
		var getPOLineItemStatus = request.getParameter('custparam_polineitemstatus');
		var getPOLineQuantity = request.getParameter('custparam_polinequantity');
		var getPOLineQuantityReceived = request.getParameter('custparam_polinequantityreceived');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemCube = request.getParameter('custparam_itemcube');
		var getActualBeginDate = request.getParameter('hdnActualBeginDate');
		var getBaseUomQty = request.getParameter('custparam_baseuomqty');
		var getActualBeginTime = request.getParameter('hdnActualBeginTime');
		var getActualBeginTimeAMPM = request.getParameter('hdnActualBeginTimeAMPM');
		var remainingCube = request.getParameter('hdnremainingCube');
		var getLocId = request.getParameter('custparam_locid');
		var putmethod = request.getParameter('hdnvputmethod');
		var putrule = request.getParameter('hdnvputrule');
		var cartprocess = request.getParameter('hdncartpro');
		POarray["custparam_cartprocess"] = cartprocess;

		nlapiLogExecution('DEBUG', 'cart in post',cartprocess);

		nlapiLogExecution('DEBUG', 'putmethod in post',putmethod);
		nlapiLogExecution('DEBUG', 'putrule in post',putrule);

		POarray["custparam_putmethod"] = putmethod;
		POarray["custparam_putrule"] = putrule;

		nlapiLogExecution('DEBUG', 'getItemCube', getItemCube);
		var trantype = nlapiLookupField('transaction', getPOInternalId, 'recordType');
		nlapiLogExecution('DEBUG', 'getBaseUomQty', getBaseUomQty);

		nlapiLogExecution('DEBUG', 'getPOLineItemStatus', getPOLineItemStatus);
		nlapiLogExecution('DEBUG', 'remainingCube', remainingCube);

		var getLineCount = 1;
		var getBaseUOM = 'EACH';
		var getBinLocation = "";
		var getuomlevel = "";

		var eBizItemDims=geteBizItemDimensions(getFetchedItemId);
		if(eBizItemDims!=null&&eBizItemDims.length>0)
		{
			for(z=0; z < eBizItemDims.length; z++)
			{
				if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
				{
					getBaseUOM = eBizItemDims[z].getText('custrecord_ebizuomskudim');
					getuomlevel = eBizItemDims[z].getText('custrecord_ebizuomlevelskudim');
				}
			}
		}

		nlapiLogExecution('DEBUG', 'getBaseUOM', getBaseUOM);
		nlapiLogExecution('DEBUG', 'getuomlevel', getuomlevel);


		var getLineCount = 1;
		var getBaseUOM = getUOMID;
		var getBinLocation = "";

		POarray["custparam_error"] = st5;

		POarray["custparam_poid"] = request.getParameter('custparam_poid');
		POarray["custparam_poitem"] = request.getParameter('custparam_poitem');
		POarray["custparam_lineno"] = request.getParameter('custparam_lineno');
		POarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
		POarray["custparam_pointernalid"] = request.getParameter('custparam_pointernalid');
		POarray["custparam_poqtyentered"] = request.getParameter('custparam_poqtyentered');
		POarray["custparam_poitemremainingqty"] = request.getParameter('custparam_poitemremainingqty');
		POarray["custparam_polinepackcode"] = request.getParameter('custparam_polinepackcode');
		POarray["custparam_polinequantity"] = request.getParameter('custparam_polinequantity');
		POarray["custparam_polinequantityreceived"] = request.getParameter('custparam_polinequantityreceived');
		POarray["custparam_polineitemstatus"] = request.getParameter('custparam_polineitemstatus');
		POarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
		POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
		POarray["custparam_number"] = getNumber;

		POarray["custparam_polineitemstatusValue"] = request.getParameter('hdnItemStatusNo');

		nlapiLogExecution('DEBUG', 'custparam_poqtyentered', POarray["custparam_poqtyentered"]);

		POarray["custparam_itemcube"] = request.getParameter('custparam_itemcube');
		POarray["custparam_baseuomqty"] = getBaseUomQty;
		POarray["custparam_actualbegindate"] = request.getParameter('custparam_actualbegindate');

		POarray["custparam_actualbegintime"] = getActualBeginTime; //TimeArray[0];
		POarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM; //TimeArray[1];
		nlapiLogExecution('DEBUG', 'custparam_actualbegintime', POarray["custparam_actualbegintime"]);
		nlapiLogExecution('DEBUG', 'custparam_actualbegintimeampm', POarray["custparam_actualbegintimeampm"]);

		POarray["custparam_screenno"] = '5a';

		POarray["custparam_batchno"] = request.getParameter('hdnBatchNo');
		POarray["custparam_mfgdate"] = request.getParameter('hdnMfgDate');
		POarray["custparam_expdate"] = request.getParameter('hdnExpDate');
		POarray["custparam_bestbeforedate"] = request.getParameter('hdnBestBeforeDate');
		POarray["custparam_lastdate"] = request.getParameter('hdnLastAvlDate');
		POarray["custparam_fifodate"] = request.getParameter('hdnFifoDate');
		POarray["custparam_fifocode"] = request.getParameter('hdnFifoCode');
		POarray["custparam_polineitemlp"] = request.getParameter('custparam_polineitemlp');
		POarray["custparam_uomid"] = request.getParameter('hdnUOMID');
		POarray["custparam_uomidtext"] = request.getParameter('hdnUOMText');
		POarray["custparam_uomqty"] = request.getParameter('hdnUOMQty');
		POarray["custparam_poreceiptno"] = getReceipt;
		POarray["custparam_locid"] = getLocId;
		POarray["custparam_remainingCube"] = remainingCube;

		POarray["custparam_recordtype"]=request.getParameter('custparam_recordtype');
		nlapiLogExecution('DEBUG', 'POarray["custparam_recordtype"]', POarray["custparam_recordtype"]);
		POarray["custparam_qccheck"] = request.getParameter('custparam_qccheck');


		POarray["custparam_number"] = parseFloat(request.getParameter('custparam_number'));
		POarray["custparam_polineitemlp"]= request.getParameter('custparam_polineitemlp');
		POarray["custparam_serialno"] = request.getParameter('hdnSerialArr');

		//POarray["custparam_serialno"] = request.getParameter('custparam_serialno');

		POarray["custparam_cartlpno"] = request.getParameter('hdnvCartLpno');
		var cartlp=request.getParameter('hdnvCartLpno');
		nlapiLogExecution('DEBUG','CARTLP',cartlp);
		//        if (POarray["custparam_serialno"] == null || POarray["custparam_serialno"] == "") {
		//            POarray["custparam_serialno"] = getSerialNo;
		//        }
		//        else {
		//            POarray["custparam_serialno"] = request.getParameter('custparam_serialno');
		//        }


		nlapiLogExecution('DEBUG', 'POarray["custparam_batchno"]', POarray["custparam_batchno"]);
		nlapiLogExecution('DEBUG', 'POarray["custparam_mfgdate"]', POarray["custparam_mfgdate"]);
		nlapiLogExecution('DEBUG', 'POarray["custparam_expdate"]', POarray["custparam_expdate"]);
		nlapiLogExecution('DEBUG', 'POarray["custparam_bestbeforedate"]', POarray["custparam_bestbeforedate"]);
		nlapiLogExecution('DEBUG', 'POarray["custparam_lastdate"]', POarray["custparam_lastdate"]);
		nlapiLogExecution('DEBUG', 'POarray["custparam_fifodate"]', POarray["custparam_fifodate"]);
		nlapiLogExecution('DEBUG', 'POarray["custparam_fifocode"]', POarray["custparam_fifocode"]);

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to RF Main Menu.
		if (optedEvent == 'F7') 
		{
			if(cartprocess == 'CARTSCREEN')
			{
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_checkinitemsts', 'customdeploy_ebiz_rf_cart_checkinitemsts', false, POarray);
			}
			else

				response.sendRedirect('SUITELET', 'customscript_rf_checkin_lp', 'customdeploy_rf_checkin_lp_di', false, POarray);
		}
		//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
		//            if (optedEvent != '' && optedEvent != null) {
		else 
			if (getSerialNo == "") {
				//	if the 'Send' button is clicked without any option value entered,
				//  it has to show an error message. The next screen to which it has to navigate is to the error screen.
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'Enter Serial No', getSerialNo);
				POarray["custparam_error"] = st5;
			}
			else {
				nlapiLogExecution('DEBUG', 'Serial No', getSerialNo);
				var getparsingSerialNo=SerialNoIdentification(getFetchedItemId,getSerialNo);
				if(getparsingSerialNo!=null && getparsingSerialNo!='')
					getSerialNo=getparsingSerialNo;


				if(getSerialNo.indexOf(' ') != -1)
				{
					POarray["custparam_error"] = st8;
					nlapiLogExecution('DEBUG', 'Enter Serial No without spaces', getSerialNo);
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					return;
					
				}






				nlapiLogExecution('DEBUG', 'After Serial No Parsing', getSerialNo);
				nlapiLogExecution('DEBUG', 'trantype', trantype);
				nlapiLogExecution('DEBUG', 'getPOInternalId', getPOInternalId);
				nlapiLogExecution('DEBUG', 'getPOLineNo', getPOLineNo);

				var vAdvBinManagement=false;

				var ctx = nlapiGetContext();
				if(ctx != null && ctx != '')
				{
					if(ctx.getFeature('advbinseriallotmgmt').toString() != null && ctx.getFeature('advbinseriallotmgmt').toString() != '')
						vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
					nlapiLogExecution('ERROR', 'vAdvBinManagement_if', vAdvBinManagement);
				}
				if(trantype=='transferorder')
				{
					var IsValidSerailNumber='F';
					var trecord = nlapiLoadRecord('transferorder', getPOInternalId);

					var links=trecord.getLineItemCount('links');
					if(links!=null  && links!='')
					{
						nlapiLogExecution('DEBUG', 'links', links);
						for(var j=0;j<links &&  IsValidSerailNumber=='F';j++)
						{
							var id=trecord.getLineItemValue('links','id',(parseInt(j)+1));
							var linktype = trecord.getLineItemValue('links','type',(parseInt(j)+1));
							nlapiLogExecution('DEBUG', 'id', id);
							nlapiLogExecution('DEBUG', 'linktype', linktype);
							if(linktype=='Item Fulfillment' || linktype=='Item Shipment' || linktype=='Ejecuci�n de pedido de art�culo')
							{
								var frecord = nlapiLoadRecord('itemfulfillment', id);
								var fitemcount=frecord.getLineItemCount('item');
								for(var f=1;f<=fitemcount;f++)
								{
									var fitem=frecord.getLineItemValue('item','item',f);
									var fline=frecord.getLineItemValue('item','orderline',f);
									var pofline= fline-1;

									if(fitem==getFetchedItemId) //&& parseInt(getPOLineNo)==parseInt(pofline))
									{
										var  serialnumbers=frecord.getLineItemValue('item','serialnumbers',(parseInt(f)));
										//Case # 20124514? Start(while doing TO  CART  check in,system allows to give any lot# system accepting the lot other than fulfilled.) 
										var itemfulfilserialno;
										var tserials = new Array();
										/*for(var j=1;j<=polinelength ;j++)
										{*/


										if(vAdvBinManagement)
										{
											//frecord.selectLineItem('item', 1);
											//frecord.selectLineItem('item', getPOLineNo);// case# 201416927
											frecord.selectLineItem('item', f);

											nlapiLogExecution('DEBUG', 'vAdvBinManagement', vAdvBinManagement);

											var itemSubtype = nlapiLookupField('item', fitem, ['recordType', 'custitem_ebizserialin','custitem_ebizserialout', 'custitem_ebizbatchlot']);
											nlapiLogExecution('DEBUG', 'itemSubtype.recordType', itemSubtype.recordType);
											nlapiLogExecution('DEBUG', 'itemSubtype.custitem_ebizserialout', itemSubtype.custitem_ebizserialout);

											nlapiLogExecution('DEBUG', 'Here', itemSubtype.custitem_ebizserialout);
											if (itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.recordType =='serializedassemblyitem' )
											{
												nlapiLogExecution('ERROR', 'polinelength', polinelength);
												var compSubRecord = frecord.viewCurrentLineItemSubrecord('item','inventorydetail');
												// case #20127404 starts
												var polinelength =0;
												//var polinelength = compSubRecord.getLineItemCount('inventoryassignment');
												//if(compSubRecord.length!=null||compSubRecord.length!=''||compSubRecord.length!='null')// Case# 20148759,20148761
												if(compSubRecord!=null && compSubRecord!=''&& compSubRecord!='null')
													polinelength = compSubRecord.getLineItemCount('inventoryassignment');
												//case #20127404 ends
												for(var j1=1;j1<=polinelength ;j1++)
												{
													//Case # 20127097 Start
//													itemfulfilserialno=compSubRecord.getLineItemValue('inventoryassignment','issueinventorynumber_display',j1);
													//itemfulfilserialno=compSubRecord.getLineItemText('inventoryassignment','issueinventorynumber',j1);
													tserials.push(compSubRecord.getLineItemText('inventoryassignment','issueinventorynumber',j1));
													//Case # 20127097 End
													if(tserials!=null && tserials!='')
													{
														//var tserials=itemfulfilserialno.split('');
														//nlapiLogExecution('DEBUG', 'serialnumbers',tserials[0]);
														//if(tserials!=null && tserials!='' && tserials.length>0)
														if(tserials!=null && tserials!='' && tserials.length>0)
														{
															if(tserials.indexOf(getSerialNo)!=-1)
															{
																nlapiLogExecution('DEBUG', 'getSerialNo',getSerialNo);
																IsValidSerailNumber='T';
																break;
															}
														}
													}
												}
												nlapiLogExecution('DEBUG', 'serialnumbers insdide advancebinmgmt',serialnumbers);
											}
											else if(itemSubtype.recordType == 'inventoryitem' && itemSubtype.custitem_ebizserialout == 'T')
											{
												nlapiLogExecution('DEBUG', 'serialnumbers new','inside');
												var filters = new Array();

												//filters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_cntrl_no', null, 'is', putPOId));
												filters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no', null, 'anyof', getPOInternalId));
												filters.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', [3])); //taskType = pick
												filters.push(new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag', null, 'anyof', [14])); // wmsStatusFlag=INBOUND/CHECK-IN
												filters.push(new nlobjSearchFilter('custrecord_ebiztask_sku', null, 'anyof',getFetchedItemId)); 
												//filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconf_refno_ebiztask', null, 'is', id));
												var columns = new Array();
												columns[0] = new nlobjSearchColumn('custrecord_ebiztask_serial_no');
												var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filters, columns);

												nlapiLogExecution('DEBUG', 'serialnumbers new1',serialnumbers);
												var serialnumbers='';

												if(searchresults != null && searchresults != '')
												{
													for(var m=0;m<searchresults.length;m++){
														nlapiLogExecution('DEBUG', 'into searchresults');
														serialnumbers = searchresults[m].getValue('custrecord_ebiztask_serial_no');
														nlapiLogExecution('DEBUG', 'serialnumbers',serialnumbers);
														var tserials1 = new Array();
														if(serialnumbers!=''&&serialnumbers!=null){
															tserials1=serialnumbers.split(',');
															nlapiLogExecution('DEBUG', 'tserials1',tserials1);
															nlapiLogExecution('DEBUG', 'tserials1.length',tserials1.length);
														}

														for(var p=0;p<tserials1.length;p++)
														{
															if(getSerialNo==tserials1[p])
															{
																IsValidSerailNumber='T';
																break;

															}
														}
													}
												}
											}

										}
										if(serialnumbers!=null && serialnumbers!='')
										{
											var tserials=serialnumbers.split('');
											//nlapiLogExecution('DEBUG', 'serialnumbers',tserials[0]);
											if(tserials!=null && tserials!='' && tserials.length>0)
											{
												if(tserials.indexOf(getSerialNo)!=-1)
												{
													nlapiLogExecution('DEBUG', 'getSerialNo111111',getSerialNo);
													IsValidSerailNumber='T';
													break;
												}
											}
										}
										//Case # 20124514� END

									}
								}
							}
						}

						if(IsValidSerailNumber=='F')
						{
							POarray["custparam_error"] = "Matching Serial Number required.</br>The Serial number on a transfer order receipt must have been fulfilled.";
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							return; 
						}
					}


				}

				nlapiLogExecution('ERROR', 'request.getParameter(custparam_serialno) before', request.getParameter('custparam_serialno'));

				var getActualEndDate = DateStamp();
				var getActualEndTime = TimeStamp();
				var TempSerialNoArray = new Array();
				if (request.getParameter('custparam_serialno') != null) 
					TempSerialNoArray = request.getParameter('custparam_serialno').split(',');

//				if (getBeginLocation != '') 
//				{
				nlapiLogExecution('ERROR', 'INTO SERIAL ENTRY');
				//checking serial no's in already scanned one's from temp table
				var filterssertemp = new Array();
				filterssertemp.push(new nlobjSearchFilter('custrecord_tempserialnumber', null, 'is', getSerialNo));
				filterssertemp.push(new nlobjSearchFilter('custrecord_tempserialstatus', null, 'is', 'F'));
				filterssertemp.push(new nlobjSearchFilter('custrecord_tempserialpolineno', null, 'equalto', getPOLineNo));
				filterssertemp.push(new nlobjSearchFilter('custrecord_tempserialorderno', null, 'anyof', getPOInternalId));

				var SrchRecordTmpSerial = nlapiSearchRecord('customrecord_ebiznettempserialentry', null, filterssertemp);
				nlapiLogExecution('ERROR', 'SrchRecordTmpSerial', SrchRecordTmpSerial);
				if(SrchRecordTmpSerial != null && SrchRecordTmpSerial !='')
				{
					//POarray["custparam_error"] = "Serial No. Already Scanned";
					POarray["custparam_error"] = st7;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					return;
				}	
				/*for (var t = 0; t < TempSerialNoArray.length; t++) {
					if (getSerialNo == TempSerialNoArray[t]) {
						POarray["custparam_error"] = "Serial No. Already Scanned";
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						return;
					}
				}*/

				//checking serial no's in records
				var filtersser = new Array();
				filtersser[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getSerialNo);

				var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser);
				nlapiLogExecution('DEBUG', 'SrchRecord', SrchRecord);
				for ( var i = 0; SrchRecord!= null && i < SrchRecord.length; i++) 
				{
					var searchresultserial = SrchRecord[i];

					var serrialid=searchresultserial.getId();
				}
				nlapiLogExecution('DEBUG', 'serrialid', serrialid);
				if (SrchRecord && (trantype!='returnauthorization' && trantype != 'transferorder')) {

					nlapiLogExecution('DEBUG', 'serrialid1', serrialid);
					POarray["custparam_error"] = st6;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					return;
				}
				else {
					// nlapiLogExecution('ERROR', 'SERIAL NO NOT FOUND');
					//if (request.getParameter('hdnSerialArr') == null || request.getParameter('hdnSerialArr') == "") {
					POarray["custparam_serialno"] = getSerialNo;
					/*}
					else {
						POarray["custparam_serialno"] = request.getParameter('hdnSerialArr') + ',' + getSerialNo;
					}*/

					POarray["custparam_number"] = parseFloat(request.getParameter('custparam_number')) + 1;
				}
				//Here we are creating serial# into new Temp Serial entry custom record

				var customrecord = nlapiCreateRecord('customrecord_ebiznettempserialentry');
				customrecord.setFieldValue('name', getSerialNo);
				customrecord.setFieldValue('custrecord_tempserialorderno', getPOInternalId);
				customrecord.setFieldValue('custrecord_tempserialpolineno', getPOLineNo);
				customrecord.setFieldValue('custrecord_tempserialitem', getFetchedItemId);
				customrecord.setFieldValue('custrecord_tempserialparentid', getLP);
				customrecord.setFieldValue('custrecord_tempserialqty', 1);
				customrecord.setFieldValue('custrecord_tempserialnumber', getSerialNo);				 
				customrecord.setFieldValue('custrecord_tempserialstatus', 'F');
				var rec = nlapiSubmitRecord(customrecord, false, true);
//				Record created into temp serial entry

				//TempSerialNoArray.push(getSerialNo);
				nlapiLogExecution('ERROR', '(getNumber + 1)', (parseInt(getNumber) + 1));
				nlapiLogExecution('ERROR', 'getItemQuantity', getItemQuantity);
				if ((parseInt(getNumber) + 1) < parseInt(getItemQuantity)) {
					nlapiLogExecution('ERROR', 'Scanning Serial No.');
					response.sendRedirect('SUITELET', 'customscript_serial_no', 'customdeploy_serial_no_di', false, POarray);
					return;

				}
				else {
					nlapiLogExecution('ERROR', 'Inserting Into Records');

					nlapiLogExecution('ERROR', 'trantype', trantype);
					var filterssertemp1 = new Array();
					//filterssertemp1[0] = new nlobjSearchFilter('custrecord_tempserialnumber', null, 'is', getSerialNo);
					filterssertemp1[0] = new nlobjSearchFilter('custrecord_tempserialstatus', null, 'is', 'F');
					filterssertemp1[1] = new nlobjSearchFilter('custrecord_tempserialpolineno', null, 'equalto', getPOLineNo);
					filterssertemp1[2] = new nlobjSearchFilter('custrecord_tempserialorderno', null, 'anyof', getPOInternalId);
					filterssertemp1[3] = new nlobjSearchFilter('custrecord_tempserialparentid', null, 'is', getLP);
					var columnssertemp1 = new Array();
					columnssertemp1[0] = new nlobjSearchColumn('custrecord_tempserialnumber');
					columnssertemp1[1] = new nlobjSearchColumn('name');
					var SrchRecordTmpSerial1 = nlapiSearchRecord('customrecord_ebiznettempserialentry', null, filterssertemp1,columnssertemp1);
					if(SrchRecordTmpSerial1 != null && SrchRecordTmpSerial1 != '')
					{	
						var parent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); //create new parent record on fo line.
						var foparentid = nlapiSubmitRecord(parent); 
						var foparent = nlapiLoadRecord('customrecord_ebiz_throwaway_parent', foparentid);

						for (var j = 0; j < SrchRecordTmpSerial1.length; j++) {
							var vInsSerialNo=SrchRecordTmpSerial1[j].getValue('custrecord_tempserialnumber');
							nlapiLogExecution('ERROR', 'Inserting Serial NO.:', vInsSerialNo);

							var filtersser = new Array();
							filtersser[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', vInsSerialNo);

							var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser);
							nlapiLogExecution('ERROR', 'SrchRecord', SrchRecord);
							for ( var i = 0; SrchRecord!= null && i < SrchRecord.length; i++) 
							{
								var searchresultserial = SrchRecord[i];

								var serrialid=searchresultserial.getId();
							}
							if (SrchRecord==null  ) {
								nlapiLogExecution('ERROR', 'Inserting Serial NO2.:', vInsSerialNo);
								/*var customrecord = nlapiCreateRecord('customrecord_ebiznetserialentry');
							if(trantype=='returnauthorization' || trantype=='transferorder')
							{
								customrecord.setFieldValue('name', vInsSerialNo);
								customrecord.setFieldValue('custrecord_ebiz_serial_rmano', getPONo);
								customrecord.setFieldValue('custrecord_ebiz_serial_rmalineno', getPOLineNo);
								customrecord.setFieldValue('custrecord_serialebizrmano', getPOInternalId);
							}
							else
							{
								customrecord.setFieldValue('name', vInsSerialNo);
								customrecord.setFieldValue('custrecord_serialpono', getPONo);
								customrecord.setFieldValue('custrecord_serialebizpono', getPOInternalId);
								customrecord.setFieldValue('custrecord_serialpolineno', getPOLineNo);
							}                           
							customrecord.setFieldValue('custrecord_serialitem', getFetchedItemId);
							customrecord.setFieldValue('custrecord_serialiteminternalid', getFetchedItemId);
							customrecord.setFieldValue('custrecord_serialparentid', getLP);
							customrecord.setFieldValue('custrecord_serialqty', POarray["custparam_uomqty"]);
							customrecord.setFieldValue('custrecord_serialnumber', vInsSerialNo);
							customrecord.setFieldValue('custrecord_serialuomid', POarray["custparam_uomidtext"]);
							customrecord.setFieldValue('custrecord_serialwmsstatus', '1');
							var rec = nlapiSubmitRecord(customrecord, false, true);*/
								//case # 20123249 start
								foparent.selectNewLineItem('recmachcustrecord_ebiz_serial_parent');
								if(trantype=='returnauthorization' || trantype=='transferorder')
								{
									foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_serial_parent','name', vInsSerialNo);
									foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_serial_parent','custrecord_ebiz_serial_rmano', getPONo);
									foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_serial_parent','custrecord_ebiz_serial_rmalineno', getPOLineNo);
									foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_serial_parent','custrecord_serialebizrmano', getPOInternalId);
								}
								else
								{
									foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_serial_parent','name', vInsSerialNo);
									foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_serial_parent','custrecord_serialpono', getPONo);
									foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_serial_parent','custrecord_serialebizpono', getPOInternalId);
									foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_serial_parent','custrecord_serialpolineno', getPOLineNo);
								}                           
								foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_serial_parent','custrecord_serialitem', getFetchedItemId);
								foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_serial_parent','custrecord_serialiteminternalid', getFetchedItemId);
								foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_serial_parent','custrecord_serialparentid', getLP);
								foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_serial_parent','custrecord_serialqty', POarray["custparam_uomqty"]);
								foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_serial_parent','custrecord_serialnumber', vInsSerialNo);
								foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_serial_parent','custrecord_serialuomid', POarray["custparam_uomidtext"]);
								foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_serial_parent','custrecord_serialwmsstatus', '1');

								foparent.commitLineItem('recmachcustrecord_ebiz_serial_parent');




								//case # 20123249 end;
							}
							else
							{

								var customrecord= nlapiLoadRecord('customrecord_ebiznetserialentry', serrialid);
								if(trantype=='returnauthorization' || trantype=='transferorder')
								{
									customrecord.setFieldValue('name', "Return Authorization  #" + getPONo);
									customrecord.setFieldValue('custrecord_ebiz_serial_rmano', getPONo);
									customrecord.setFieldValue('custrecord_ebiz_serial_rmalineno', getPOLineNo);
									customrecord.setFieldValue('custrecord_serialebizrmano', getPOInternalId);
									customrecord.setFieldValue('custrecord_serialparentid', getLP);
								}
								var rec = nlapiSubmitRecord(customrecord, false, true);

							}

							var TempRecord=SrchRecordTmpSerial1[j];
							foparent.selectNewLineItem('recmachcustrecord_ebiz_tempserial_child');
							nlapiLogExecution('ERROR', 'TempRecord.getId()',TempRecord.getId());
							nlapiLogExecution('ERROR', 'TempRecord.getValue(name)',TempRecord.getValue('name'));
							foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_tempserial_child', 'id', TempRecord.getId());
							foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_tempserial_child','name', TempRecord.getValue('name'));

							foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_tempserial_child','custrecord_tempserialstatus', 'T');

							foparent.commitLineItem('recmachcustrecord_ebiz_tempserial_child');


							if(POarray["custparam_serialnonew"] == null || POarray["custparam_serialnonew"] =='')
								POarray["custparam_serialnonew"] =  vInsSerialNo;
							else
								POarray["custparam_serialnonew"] = POarray["custparam_serialnonew"] + ',' + vInsSerialNo;

						}
						nlapiSubmitRecord(foparent);
					}


					//Added on 12Mar by suman.
					/*
					 * This is to insert a record in Transaction Line Details custom record
					 * The purpose of this is to identify the quantity checked-in, location generated for and putaway confirmation
					 * */                    
					var trantype= nlapiLookupField('transaction',POarray["custparam_pointernalid"],'recordType');
					TrnLineUpdation(trantype, 'CHKN', POarray["custparam_poid"], POarray["custparam_pointernalid"], 
							POarray["custparam_lineno"], POarray["custparam_fetcheditemid"], 
							POarray["custparam_polinequantity"], POarray["custparam_poqtyentered"],"",getPOLineItemStatus);

					var vRecType=POarray["custparam_recordtype"];
					if (vRecType == 'lotnumberedinventoryitem' || vRecType == "lotnumberedassemblyitem" || vRecType == "assemblyitem") {
						//checks for Batch/Lot exists in Batch Entry, Update if exists if not insert
						try {
							nlapiLogExecution('DEBUG', 'in');
							var filtersbat = new Array();
							filtersbat[0] = new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', POarray["custparam_batchno"]);

							var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filtersbat);
							nlapiLogExecution('DEBUG', 'SrchRecord', SrchRecord);
							if (SrchRecord) {

								nlapiLogExecution('DEBUG', ' BATCH FOUND');
								var transaction = nlapiLoadRecord('customrecord_ebiznet_batch_entry', SrchRecord[0].getId());
								transaction.setFieldValue('custrecord_ebizmfgdate', POarray["custparam_mfgdate"]);
								transaction.setFieldValue('custrecord_ebizbestbeforedate', POarray["custparam_bestbeforedate"]);
								transaction.setFieldValue('custrecord_ebizexpirydate', POarray["custparam_expdate"]);
								transaction.setFieldValue('custrecord_ebizfifodate', POarray["custparam_fifodate"]);
								transaction.setFieldValue('custrecord_ebizfifocode', POarray["custparam_fifocode"]);
								transaction.setFieldValue('custrecord_ebizlastavldate', POarray["custparam_lastdate"]);
								var rec = nlapiSubmitRecord(transaction, false, true);

							}
							else {
								nlapiLogExecution('DEBUG', 'BATCH NOT FOUND');
								var customrecord = nlapiCreateRecord('customrecord_ebiznet_batch_entry');

								customrecord.setFieldValue('name', POarray["custparam_batchno"]);
								customrecord.setFieldValue('custrecord_ebizlotbatch', POarray["custparam_batchno"]);
								customrecord.setFieldValue('custrecord_ebizmfgdate', POarray["custparam_mfgdate"]);
								customrecord.setFieldValue('custrecord_ebizbestbeforedate', POarray["custparam_bestbeforedate"]);
								customrecord.setFieldValue('custrecord_ebizexpirydate', POarray["custparam_expdate"]);
								customrecord.setFieldValue('custrecord_ebizfifodate', POarray["custparam_fifodate"]);
								customrecord.setFieldValue('custrecord_ebizfifocode', POarray["custparam_fifocode"]);
								customrecord.setFieldValue('custrecord_ebizlastavldate', POarray["custparam_lastdate"]);
								nlapiLogExecution('DEBUG', '1');
								nlapiLogExecution('DEBUG', 'Item Id',getFetchedItemId);
								//customrecord.setFieldText('custrecord_ebizsku', getPOItem);
								customrecord.setFieldValue('custrecord_ebizsku', getFetchedItemId);
								customrecord.setFieldValue('custrecord_ebizpackcode',getPOLinePackCode);
								customrecord.setFieldValue('custrecord_ebizsitebatch',POarray["custparam_whlocation"]);
								//custrecord_ebizsitebatch
								var rec = nlapiSubmitRecord(customrecord, false, true);
							}
						} 
						catch (e) {
							nlapiLogExecution('DEBUG', 'Failed to Update/Insert into BATCH ENTRY Record');
						}
					}


					var qcRuleCheck = request.getParameter('custparam_qccheck');
					nlapiLogExecution('DEBUG', 'qcRuleCheck', qcRuleCheck);
					var vValid=custChknPutwRecCreation(getPOInternalId, getPONo, getPOQtyEntered, getPOLineNo, getPOItemRemainingQty, 
							getLineCount, getFetchedItemId, getPOItem, getItemDescription, getPOLineItemStatus, getPOLinePackCode, getBaseUOM, 
							getLP, getBinLocation, getPOLineQuantityReceived, getActualEndDate, getActualEndTime, getReceipt, getBeginLocation,
							getActualBeginDate, getActualBeginTime, getActualBeginTimeAMPM, POarray["custparam_locid"], POarray["custparam_batchno"], 
							POarray["custparam_mfgdate"], POarray["custparam_expdate"], POarray["custparam_bestbeforedate"], POarray["custparam_lastdate"], 
							POarray["custparam_fifodate"], POarray["custparam_fifocode"], POarray["custparam_recordtype"],POarray["custparam_whlocation"],
							qcRuleCheck,cartlp,putmethod,putrule,POarray["custparam_serialnonew"]);
					nlapiLogExecution('DEBUG', 'vValid', vValid);
					if(vValid==false)
					{
						response.sendRedirect('SUITELET', 'customscript_rf_checkin_cnf', 'customdeploy_rf_checkin_cnf', false, POarray);
					}
					else
					{

						TrnLineUpdation(trantype, 'ASPW', POarray["custparam_poid"], POarray["custparam_pointernalid"], 
								POarray["custparam_lineno"], POarray["custparam_fetcheditemid"], 
								POarray["custparam_polinequantity"], POarray["custparam_poqtyentered"],"","");

						if(getLocId!=null && getLocId!='')
						{
							UpdateLocCube(getLocId, remainingCube);
						}

						rf_checkin(getPOInternalId, getFetchedItemId, getItemDescription, getPOLineItemStatus, 
								getPOLinePackCode, getPOQtyEntered, getLP, POarray["custparam_whlocation"],
								POarray["custparam_batchno"], POarray["custparam_mfgdate"], 
								POarray["custparam_expdate"], POarray["custparam_bestbeforedate"], 
								POarray["custparam_lastdate"], POarray["custparam_fifodate"], 
								POarray["custparam_fifocode"],getuomlevel,trantype);

						POarray["custparam_trantype"] = trantype;
						nlapiLogExecution('DEBUG', 'Done cartlp', cartlp);
						// case no 20125825
						if(cartlp!=null&&cartlp!=""&&cartlp!='null')
						{
							nlapiLogExecution('DEBUG', 'Done customrecord1', 'Success1');
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_criteriamenu', 'customdeploy_ebiz_rf_cart_criteriamenu', false, POarray);
						}

						else
						{
							nlapiLogExecution('DEBUG', 'Done customrecord2', 'Success2');
							response.sendRedirect('SUITELET', 'customscript_rf_checkin_criteria_menu', 'customdeploy_rf_checkin_criteria_menu_di', false, POarray);
						}

					}
				}
//				}
//				else {
//				POarray["custparam_error"] = "Location Not Found";
//				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
//				nlapiLogExecution('DEBUG', 'Enter Location');
//				}
			}
	}

	nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
}

function custChknPutwRecCreation(po, poValue, quan, lineno, remQty, lineCnt, ItemId, ItemName, ItemDesc, ItemStatus, PackCode, 
		BaseUOM, LP, BinLoc, RcvQty, ActualEndDate, ActualEndTime, poreceiptno, BeginLocation, ActualBeginDate, ActualBeginTime, 
		ActualBeginTimeAMPM, BeginLocationId, BatchNo, MfgDate, ExpDate, BestBeforeDate, LastDate, FifoDate, FifoCode, 
		itemrectype,WHLocation,qcRuleCheck,cartlp,putmethod,putrule,TempSerialNoArray){
	nlapiLogExecution('DEBUG', 'Into custChknPutwRecCreation', 'custChknPutwRecCreation');
	nlapiLogExecution('DEBUG', 'Into WHLocation', WHLocation);
	nlapiLogExecution('DEBUG', 'Into bin loc id', BeginLocationId);
	nlapiLogExecution('DEBUG', 'Into TempSerialNoArray loc id', TempSerialNoArray);
	var now = new Date();
	var stagelocid, docklocid;
//	var stagesearchresult = new nlapiSearchRecord('customrecord_ebiznet_location', null, new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'anyof', [8]));
//	if (stagesearchresult != null && stagesearchresult.length > 0) {
//	stagelocid = stagesearchresult[0].getId();
//	}


	try
	{
		stagelocid = GetInboundStageLocation(WHLocation, null, 'INB');
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in  GetInboundStageLocation', exp);
	}

	if(stagelocid==null || stagelocid=='')
		stagelocid = getStagingLocation(WHLocation);

	nlapiLogExecution('DEBUG', 'Stage Location', stagelocid);

	if(stagelocid==null || stagelocid=='')
		stagelocid=BeginLocationId;

	docklocid = getDockLocation(WHLocation);

	/*var docksearchresult = new nlapiSearchRecord('customrecord_ebiznet_location', null, new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'anyof', [3]));
	if (docksearchresult != null && docksearchresult.length > 0) {
		docklocid = docksearchresult[0].getId();
	}*/
	//if (BinLoc != null && BinLoc != '') //Creating custom record with CHKN and PUTW task,
	//if (BinLoc != null && BinLoc != '') //Creating custom record with CHKN and PUTW task,
	if (BeginLocationId != null && BeginLocationId != '') 
	{
		//create the openTask record With CHKN Task Type
		var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating CHKN Record', 'TRN_OPENTASK');

		//populating the fields
		customrecord.setFieldValue('name', poValue);
		customrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		//        customrecord.setFieldValue('custrecord_ebiz_sku_no', ItemName);
		customrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		customrecord.setFieldValue('custrecord_act_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_lpno', LP);
		customrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		customrecord.setFieldValue('custrecord_line_no', lineno);
		customrecord.setFieldValue('custrecord_packcode', PackCode);
		customrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		customrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		customrecord.setFieldValue('custrecord_tasktype', 1); //For Check-In (CHKN)
		//        customrecord.setFieldValue('custrecord_actbeginloc', BinLoc);
		nlapiLogExecution('DEBUG', 'Fetched stagelocid', stagelocid);
		nlapiLogExecution('DEBUG', 'Fetched docklocid', docklocid);
		customrecord.setFieldValue('custrecord_actbeginloc', docklocid);
		customrecord.setFieldValue('custrecord_actendloc', stagelocid);
		//        customrecord.setFieldValue('custrecord_actendloc', 'INB-1');
		customrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		//Added for Item name and desc
		customrecord.setFieldValue('custrecord_sku', ItemId);
		customrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		customrecord.setFieldValue('custrecord_wms_location', WHLocation);
		//code added on 12Mar by suman
		if(po!=null && po!="")
			customrecord.setFieldValue('custrecord_ebiz_order_no', po);

		//For eBiznet Receipt Number .
		customrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		//        customrecord.setFieldValue('custrecordact_begin_date', (parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
		customrecord.setFieldValue('custrecordact_begin_date', ActualBeginDate);
		//        customrecord.setFieldValue('custrecord_act_end_date', (parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
		customrecord.setFieldValue('custrecord_act_end_date', ActualEndDate);

		/*        
         //Adding fields to update time zones.
         //        customrecord.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
         customrecord.setFieldValue('custrecord_actualbegintime', ActualBeginDate);
         //        customrecord.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
         customrecord.setFieldValue('custrecord_actualendtime', ActualEndTime);
         customrecord.setFieldValue('custrecord_recordtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
         customrecord.setFieldValue('custrecord_recordupdatetime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
         customrecord.setFieldValue('custrecord_current_date', (parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
         customrecord.setFieldValue('custrecord_upd_date', (parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
		 */
		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		customrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		nlapiLogExecution('DEBUG', 'getActualBeginTime', getActualBeginTime);
		customrecord.setFieldValue('custrecord_actualendtime', ActualEndTime);
		customrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		customrecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
		customrecord.setFieldValue('custrecord_current_date', DateStamp());
		customrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		//        customrecord.setFieldValue('custrecord_status_flag', 'C');
		customrecord.setFieldValue('custrecord_wms_status_flag', 1);

		//LN,Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" || itemrectype == "assemblyitem")
		{
			customrecord.setFieldValue('custrecord_batch_no', BatchNo);
			customrecord.setFieldValue('custrecord_expirydate', ExpDate);
			customrecord.setFieldValue('custrecord_fifodate', FifoDate);
			customrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");
		}

		// case# 201416506
		var currentUserID = getCurrentUser();

		customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		customrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		customrecord.setFieldValue('custrecord_taskassignedto', currentUserID);
		
		
		customrecord.setFieldValue('custrecord_transport_lp', cartlp);
		nlapiLogExecution('DEBUG', 'Submitting CHKN record', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(customrecord);
		nlapiLogExecution('DEBUG', 'Done CHKN Record Insertion :', 'Success');

		//create the openTask record With PUTW Task Type

		var putwrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating PUTW record', 'TRN_OPENTASK');

		//populating the fields
		putwrecord.setFieldValue('name', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		//putwrecord.setFieldValue('custrecord_ebiz_sku_no', ItemName);
		putwrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		//        putwrecord.setFieldValue('custrecord_act_qty', quan);
		putwrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		putwrecord.setFieldValue('custrecord_lpno', LP);
		putwrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		putwrecord.setFieldValue('custrecord_line_no', lineno);
		putwrecord.setFieldValue('custrecord_packcode', PackCode);
		putwrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		putwrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		putwrecord.setFieldValue('custrecord_tasktype', 2); //For Putaway (PUTW)
		nlapiLogExecution('DEBUG', 'Fetched Begin Location', BeginLocation);

		//putwrecord.setFieldValue('custrecord_actbeginloc', BeginLocationId);
		putwrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		//For eBiznet Receipt Number .
		putwrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		putwrecord.setFieldValue('custrecordact_begin_date', DateStamp());

		//Adding fields to update time zones.
		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		putwrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		putwrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		putwrecord.setFieldValue('custrecord_current_date', DateStamp());
		putwrecord.setFieldValue('custrecord_upd_date', DateStamp());
		if(putmethod!=null && putmethod!='')
			putwrecord.setFieldValue('custrecord_ebizmethod_no', putmethod);
		if(putrule!=null && putrule!='')
			putwrecord.setFieldValue('custrecord_ebizrule_no', putrule);
		//Status flag .
		//        putwrecord.setFieldValue('custrecord_status_flag', 'L');
		nlapiLogExecution('DEBUG', 'qcRuleCheck', qcRuleCheck);
		//nlapiLogExecution('DEBUG', 'qcRuleCheck=FALSE', qcRuleCheck==false);
		nlapiLogExecution('DEBUG', 'qcRuleCheck=FALSE', qcRuleCheck=='FALSE');
		if(qcRuleCheck==true || qcRuleCheck=='TRUE')
		{
			putwrecord.setFieldValue('custrecord_wms_status_flag', 23);
		}
		else
		{ 	 
			putwrecord.setFieldValue('custrecord_wms_status_flag', 2);			  
			putwrecord.setFieldValue('custrecord_actbeginloc', BeginLocationId);
		}

		//putwrecord.setFieldValue('custrecord_wms_status_flag', 2);

		//Added for Item name and desc
		putwrecord.setFieldValue('custrecord_sku', ItemId);
		putwrecord.setFieldValue('custrecord_skudesc', ItemDesc);
		putwrecord.setFieldValue('custrecord_serial_no', TempSerialNoArray);
		//LN,Fields for Lot/Batch Sku's

		nlapiLogExecution('DEBUG', 'itemrectype', itemrectype);
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" || itemrectype == "assemblyitem")
		{
			nlapiLogExecution('DEBUG', 'custrecord_batch_no', BatchNo);
			putwrecord.setFieldValue('custrecord_batch_no', BatchNo);
			putwrecord.setFieldValue('custrecord_expirydate', ExpDate);
			putwrecord.setFieldValue('custrecord_fifodate', FifoDate);
			putwrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");
		}

		var currentUserID = getCurrentUser();

		putwrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		putwrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		putwrecord.setFieldValue('custrecord_taskassignedto', currentUserID);
		
		putwrecord.setFieldValue('custrecord_transport_lp', cartlp);

		putwrecord.setFieldValue('custrecord_wms_location', WHLocation);

		//code added on 12Mar by suman
		if(po!=null && po!="")
			putwrecord.setFieldValue('custrecord_ebiz_order_no', po);

		//commit the record to NetSuite
		var putwrecordid = nlapiSubmitRecord(putwrecord);
		try
		{
			MoveTaskRecord(recid);
		}
		catch(exp)
		{
			nlapiLogExecution('DEBUG', 'Exception in  MoveTaskRecord');
		}
		nlapiLogExecution('DEBUG', 'Done PUTW Record Insertion ', 'TRN_OPENTASK');
		return true;

	}//end if binloc is not null
	else {
		//create the openTask record With CHKN Task Type
		var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating customrecord', 'TRN_OPENTASK');

		//populating the fields
		customrecord.setFieldValue('name', poValue);
		customrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		//        customrecord.setFieldValue('custrecord_ebiz_sku_no', ItemName);
		customrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		customrecord.setFieldValue('custrecord_act_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_lpno', LP);
		customrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		customrecord.setFieldValue('custrecord_line_no', lineno);
		customrecord.setFieldValue('custrecord_packcode', PackCode);
		customrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		customrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		customrecord.setFieldValue('custrecord_tasktype', 1); //For Check-In (CHKN)
		customrecord.setFieldValue('custrecord_actbeginloc', docklocid);
		customrecord.setFieldValue('custrecord_actendloc', stagelocid);
		customrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		customrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);


		customrecord.setFieldValue('custrecordact_begin_date', ActualBeginDate);
		customrecord.setFieldValue('custrecord_act_end_date', ActualEndDate);
		//Adding fields to update time zones.
		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		customrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		customrecord.setFieldValue('custrecord_actualendtime', ActualEndTime);
		customrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		customrecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
		customrecord.setFieldValue('custrecord_current_date', DateStamp());
		customrecord.setFieldValue('custrecord_upd_date', DateStamp());
		customrecord.setFieldValue('custrecord_upd_date', DateStamp());

		customrecord.setFieldValue('custrecord_transport_lp', cartlp);
		//Status flag .
		//        customrecord.setFieldValue('custrecord_status_flag', 'C');
		customrecord.setFieldValue('custrecord_wms_status_flag', 1);
		customrecord.setFieldValue('custrecord_wms_location', WHLocation);
		//Added for Item name and desc
		customrecord.setFieldValue('custrecord_sku', ItemId);
		customrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		if(po!=null && po!="")
			customrecord.setFieldValue('custrecord_ebiz_order_no', po);

		//LN,Fields for Lot/Batch Sku's
		if (itemrectype == 'lotnumberedinventoryitem') {
			customrecord.setFieldValue('custrecord_batch_no', BatchNo);
			customrecord.setFieldValue('custrecord_expirydate', MfgDate);
			customrecord.setFieldValue('custrecord_fifodate', FifoDate);
		}

		nlapiLogExecution('DEBUG', 'Submitting customrecord', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(customrecord);
		nlapiLogExecution('DEBUG', 'Done CHKN Record Insertion :', 'Success');

		//create the openTask record With PUTW Task Type
		var putwrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating customrecord', 'TRN_OPENTASK');

		//populating the fields
		putwrecord.setFieldValue('name', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		//        putwrecord.setFieldValue('custrecord_act_qty', quan);
		putwrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		putwrecord.setFieldValue('custrecord_lpno', LP);
		putwrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		putwrecord.setFieldValue('custrecord_line_no', lineno);
		putwrecord.setFieldValue('custrecord_packcode', PackCode);
		putwrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		putwrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		putwrecord.setFieldValue('custrecord_tasktype', 2); //For Putaway (PUTW)
		//customrecord.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
		// customrecord.setFieldValue('custrecord_actbeginloc', BinLoc);
		//customrecord.setFieldValue('custrecord_actendloc', BinLoc);
		putwrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);
		putwrecord.setFieldValue('custrecord_wms_location', WHLocation);
		putwrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);
		putwrecord.setFieldValue('custrecord_serial_no', TempSerialNoArray);

		//Adding fields to update time zones.

		//customrecord.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
		// customrecord.setFieldValue('custrecord_actualendtime',   ((curr_hour)+":"+(curr_min)+" " +a_p));
		//customrecord.setFieldValue('custrecord_recordtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
		//customrecord.setFieldValue('custrecord_recordupdatetime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
		/*
         putwrecord.setFieldValue('custrecord_current_date', (parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
         putwrecord.setFieldValue('custrecord_upd_date', (parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
		 */
		putwrecord.setFieldValue('custrecord_current_date', DateStamp());
		putwrecord.setFieldValue('custrecord_upd_date', DateStamp());
		if(putmethod!=null && putmethod!='')
			putwrecord.setFieldValue('custrecord_ebizmethod_no', putmethod);
		if(putrule!=null && putrule!='')
			putwrecord.setFieldValue('custrecord_ebizrule_no', putrule);
		//Status flag .
		//        customrecord.setFieldValue('custrecord_status_flag', 'N');

		if(qcRuleCheck)
		{
			putwrecord.setFieldValue('custrecord_wms_status_flag', 23);
		}
		else
		{ 	 
			putwrecord.setFieldValue('custrecord_wms_status_flag', 6); 
		}

		//putwrecord.setFieldValue('custrecord_wms_status_flag', 6);

		//Added for Item name and desc
		putwrecord.setFieldValue('custrecord_sku', ItemId);
		putwrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//LN,Fields for Lot/Batch Sku's
		if (itemrectype == 'lotnumberedinventoryitem') {
			putwrecord.setFieldValue('custrecord_batch_no', BatchNo);
			putwrecord.setFieldValue('custrecord_expirydate', MfgDate);
			putwrecord.setFieldValue('custrecord_fifodate', FifoDate);
		}

		putwrecord.setFieldValue('custrecord_transport_lp', cartlp);
		nlapiLogExecution('DEBUG', 'Submitting customrecord', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(putwrecord);
		nlapiLogExecution('DEBUG', 'Done CHKN Record Insertion :', 'Success');
		return false;
	}
}

function getDockLocation(site){
	var retDockLocation = "";

	/*
	 * To fetch dock location,
	 *   Search customrecord_ebiznet_location for all location whose
	 *   location group id has location type as 'DOCK' and location group type as 'INBOUND' or 'BOTH'
	 */
	var dockFilters = new Array();
	// LOCATION TYPE = DOCK
	dockFilters.push(new nlobjSearchFilter('custrecord_grplocationtype', 'custrecord_inboundlocgroupid', 'is', '3'));

	// LOCATION GROUP TYPE = INBOUND or BOTH
	dockFilters.push(new nlobjSearchFilter('custrecord_locgrouptype', 'custrecord_inboundlocgroupid', 'anyof', ['1', '2']));

	//code added on 5Mar By suman
	//Searching for the record with the site id and lowest seqNo.

	//SITE
	if(site!=null && site!="")
		dockFilters.push(new nlobjSearchFilter('custrecord_ebizsiteloc', 'custrecord_inboundlocgroupid', 'anyof', ["NONE",site]));

	//SEQUENCE NO.
	var dockColumn=new Array();
	dockColumn[0]=new nlobjSearchColumn('custrecord_sequenceno','custrecord_inboundlocgroupid');
	dockColumn[0].setSort();

	// Results for inbound staging location
	var dockLocationResults = nlapiSearchRecord('customrecord_ebiznet_location', null, dockFilters,dockColumn);

	//end of modifing code as of 5Mar.

	// This search could return more than one inbound staging location.  We are using the first one.
	if(dockLocationResults != null)
		retDockLocation = dockLocationResults[0].getId();

	return retDockLocation;
}

function rf_checkin(pointid, itemid, itemdesc, itemstatus, itempackcode, quantity, invtlp, whLocation,
		batchno, mfgdate, expdate, bestbeforedate, lastdate, fifodate, fifocode,uomlevel,trantype){
	nlapiLogExecution('DEBUG', 'inside rf_checkin', 'Success');
	nlapiLogExecution('DEBUG', 'pointid', pointid);
	nlapiLogExecution('DEBUG', 'itemid', itemid);
	nlapiLogExecution('DEBUG', 'itemdesc', itemdesc);
	nlapiLogExecution('DEBUG', 'itemstatus', itemstatus);
	nlapiLogExecution('DEBUG', 'itempackcode', itempackcode);
	nlapiLogExecution('DEBUG', 'quantity', quantity);
	nlapiLogExecution('DEBUG', 'invtlp', invtlp);
	nlapiLogExecution('DEBUG', 'whLocation', whLocation);
	nlapiLogExecution('DEBUG', 'batchno', batchno);
	nlapiLogExecution('DEBUG', 'mfgdate', mfgdate);
	nlapiLogExecution('DEBUG', 'expdate', expdate);
	nlapiLogExecution('DEBUG', 'bestbeforedate', bestbeforedate);
	nlapiLogExecution('DEBUG', 'lastdate', lastdate);
	nlapiLogExecution('DEBUG', 'fifodate', fifodate);
	nlapiLogExecution('DEBUG', 'fifocode', fifocode);
	nlapiLogExecution('DEBUG', 'uomlevel', uomlevel);




//	nlapiLogExecution('DEBUG', 'whLocation', whLocation);
//	nlapiLogExecution('DEBUG', 'PO Internal ID', pointid);

	if(whLocation==null || whLocation=="")
	{
		whLocation = nlapiLookupField(trantype, pointid, 'location');

		nlapiLogExecution('DEBUG', 'whLocation (from lookup)', whLocation);
	}

	/*
	 * Get the stage location. This will be the bin location to which the item will be 
	 * placed at the time of check-in. The same data is to be inserted in inventory record after it is
	 * done in opentask record.
	 */
	var stagelocid;

	try
	{
		stagelocid = GetInboundStageLocation(whLocation, null, 'INB');
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in  GetInboundStageLocation', exp);
	}

	if(stagelocid==null || stagelocid=='')
		stagelocid = getStagingLocation(whLocation);

	nlapiLogExecution('DEBUG', 'Stage Location', stagelocid);

	/*
	 * To create inventory record.
	 */
	var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');

	nlapiLogExecution('DEBUG', 'Creating Inventory Record', 'INVT');

	var filtersAccount = new Array();
	if(whLocation!=null && whLocation!=""){
		filtersAccount.push(new nlobjSearchFilter('custrecord_location', null, 'anyof', whLocation));
	}

	var accountNumber = "";
//	var columnsAccount = new Array();
//	columnsAccount[0]=new nlobjSearchColumn('custrecord_accountno');                

//	var accountSearchResults = nlapiSearchRecord('customrecord_inventoryaccountno', null, filtersAccount, columnsAccount);
//	if(accountSearchResults!= null && accountSearchResults.length > 0)
//	accountNumber = accountSearchResults[0].getValue('custrecord_accountno');
//	if(accountSearchResults!= null && accountSearchResults.length > 0)
//	{

	//nlapiLogExecution('DEBUG', 'Account # incondition',accountNumber);

	invtRec.setFieldValue('name', pointid);
	invtRec.setFieldValue('custrecord_ebiz_inv_binloc', stagelocid);
	invtRec.setFieldValue('custrecord_ebiz_inv_lp', invtlp);
	invtRec.setFieldValue('custrecord_ebiz_inv_sku', itemid);
	invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
	invtRec.setFieldValue('custrecord_ebiz_inv_packcode', itempackcode);
	invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(quantity).toFixed(4));
	invtRec.setFieldValue('custrecord_ebiz_inv_loc', whLocation);
	invtRec.setFieldValue('custrecord_inv_ebizsku_no', itemid);
	//invtRec.setFieldValue('custrecord_ebiz_qoh', quantity);
	invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
//	invtRec.setFieldValue('custrecord_wms_inv_status_flag','1');
	invtRec.setFieldValue('custrecord_wms_inv_status_flag','17');//17=FLAG.INVENTORY.INBOUND
	invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
	invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(quantity).toFixed(4));
	invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemdesc);
	invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);
	invtRec.setFieldValue('custrecord_invttasktype', 1);
	if(uomlevel!=null && uomlevel!='')
		invtRec.setFieldText('custrecord_ebiz_uomlvl', uomlevel);
	//custrecord_ebiz_uomlvl


	if(batchno!=null && batchno!=''&& batchno!='null')
	{
		nlapiLogExecution('DEBUG', 'batchno into if', batchno);
		try
		{
			//Added on 02/02/12 by suman.
			//since custrecord_ebiz_in_lot is a list need to pass InternalId.
			var filterBatch=new Array();
			filterBatch[0]=new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',batchno);
			var rec=nlapiSearchRecord('customrecord_ebiznet_batch_entry',null,filterBatch,null);
			var BatchID=rec[0].getId();
			//end of code additon as on 02/02/12.
//			invtRec.setFieldValue('custrecord_ebiz_inv_lot', batchno);
			invtRec.setFieldValue('custrecord_ebiz_inv_lot', BatchID);
			invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
			invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifodate);
		}
		catch(exp)
		{
			nlapiLogExecution('DEBUG', 'Exception', exp);
		}
	}

	//code added on 16Feb by suman
	if(pointid!=null && pointid!="")
		invtRec.setFieldValue('custrecord_ebiz_transaction_no', pointid);

	var invtRecordId = nlapiSubmitRecord(invtRec, false, true);
//	}
//	else
//	{
//	response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, "Invalid Account #");
//	//nlapiLogExecution('DEBUG', 'Entered Item LP', getItemLP);
//	}
	nlapiLogExecution('DEBUG', 'After Submitting invtrecid', invtRecordId);
	if (invtRecordId != null) {
		nlapiLogExecution('DEBUG', 'Inventory record creation is successful', invtRecordId);
	}
	else {
		nlapiLogExecution('DEBUG', 'Inventory record creation is failure', 'Fail');
	}
} 


function getStagingLocation(site){
	var retInboundStagingLocn = "";

	/*
	 * To fetch inbound staging location,
	 *   Search customrecord_ebiznet_location for all location whose
	 *   location group id has location type as 'STAGE' and location group type as 'INBOUND' or 'BOTH'
	 */
	var stageFilters = new Array();
	// LOCATION TYPE = STAGE
	stageFilters.push(new nlobjSearchFilter('custrecord_grplocationtype', 'custrecord_inboundlocgroupid', 'is', '8'));

	// LOCATION GROUP TYPE = INBOUND or BOTH
	stageFilters.push(new nlobjSearchFilter('custrecord_locgrouptype', 'custrecord_inboundlocgroupid', 'anyof', ['1', '2']));

	//code added on 5Mar By suman
	//Searching for the record with the site id and lowest seqNo.

	//SITE
	if(site!=null && site!="")
		stageFilters.push(new nlobjSearchFilter('custrecord_ebizsiteloc', 'custrecord_inboundlocgroupid', 'anyof', ["NONE",site]));

	//SEQUENCE NO.
	var stageColumn=new Array();
	stageColumn[0]=new nlobjSearchColumn('custrecord_sequenceno','custrecord_inboundlocgroupid');
	stageColumn[0].setSort();

	// Results for inbound staging location
	var stageLocnResults = nlapiSearchRecord('customrecord_ebiznet_location', null, stageFilters, stageColumn);

	//end of modifing code as of 5Mar.

	// This search could return more than one inbound staging location.  We are using the first one.
	if(stageLocnResults != null)
		retInboundStagingLocn = stageLocnResults[0].getId();

	return retInboundStagingLocn;
}

function GetInboundStageLocation(vSite, vCompany, stgDirection){

	nlapiLogExecution('ERROR', 'Into GetInboundStageLocation');

	var str = 'vSite. = ' + vSite + '<br>';
	str = str + 'vCompany. = ' + vCompany + '<br>';	
	str = str + 'stgDirection. = ' + stgDirection + '<br>';

	nlapiLogExecution('ERROR', 'GetInboundStageLocation Parameters', str);

	var direction = "";
	if (stgDirection == 'INB') {
		direction = "1";
	}
	else 
		if (stgDirection == 'OUB') {
			direction = "2";
		}
		else
		{
			direction = "3";
		}


	var columns = new Array();
	var filters = new Array();

	if (vSite != null && vSite != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizsitestage', null, 'anyof', ['@NONE@', vSite]));

	}
	if (vCompany != null && vCompany != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizcompanystage', null, 'anyof', ['@NONE@', vCompany]));

	}

	filters.push(new nlobjSearchFilter('custrecord_directionstgrule', null, 'anyof', [direction, '3']));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	columns[0] = new nlobjSearchColumn('custrecord_sequencenostgrule');
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationstgrule');
	columns[2] = new nlobjSearchColumn('custrecord_inboundlocationgroupstg');
	columns[3] = new nlobjSearchColumn('custrecord_outboundlocationgroup');
	columns[4] = new nlobjSearchColumn('custrecord_nooffootprints', 'custrecord_locationstgrule');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters, columns);
	if (searchresults != null) {
		for (var i = 0; i < searchresults.length; i++) {

			var vstgLocation = searchresults[i].getValue('custrecord_locationstgrule');

			var vstgLocationText = searchresults[i].getText('custrecord_locationstgrule');

			var str = 'Stage Location ID. = ' + vstgLocation + '<br>';
			str = str + 'Stage Location Text. = ' + vstgLocationText + '<br>';	

			nlapiLogExecution('ERROR', 'Stage Location Details', str);

			if (vstgLocation != null && vstgLocation != "") {
				nlapiLogExecution('ERROR', 'Out of GetInboundStageLocation',vstgLocation);
				return vstgLocation;
			}

			var LocGroup = "";
			if (stgDirection == 'INB') {
				LocGroup = searchresults[i].getValue('custrecord_inboundlocationgroupstg');
			}
			else {
				LocGroup = searchresults[i].getValue('custrecord_outboundlocationgroup');
			}

			nlapiLogExecution('Error', 'LocGroup ', LocGroup);

			if (LocGroup != null && LocGroup != "") {

				var collocGroup = new Array();
				var filtersLocGroup = new Array();
				collocGroup[0] = new nlobjSearchColumn('custrecord_startingpickseqno');
				collocGroup[0].setSort();
				collocGroup[1] = new nlobjSearchColumn('name');
				collocGroup[2] = new nlobjSearchColumn('custrecord_nooffootprints');

				if (stgDirection = 'INB') {
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [LocGroup]));
				}
				else { 
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_outboundlocgroupid', null, 'anyof', [LocGroup]));
				}

				var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
				nlapiLogExecution('Error', 'searchresultsloc ', searchresultsloc);
				for (var k = 0; searchresultsloc != null && k < searchresultsloc.length; k++) {

					var vLocid=searchresultsloc[0].getId();
					nlapiLogExecution('ERROR', 'Out of GetInboundStageLocation',vLocid);
					return vLocid;

				}				
			}
		}
	}

	nlapiLogExecution('ERROR', 'Out of GetInboundStageLocation');

}



