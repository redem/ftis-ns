/***************************************************************************
����������������� eBizNET Solutions Inc
 ****************************************************************************
 *
 *� $Source: /cvs/products/NSWMS/EoN/WMS_UI/Reports/Suitelet/Attic/ebiz_OrderManagement_SL.js,v $
 *� $Revision: 1.1.2.7.4.2.4.1.6.1 $
 *� $Date: 2015/04/13 09:26:47 $
 *� $Author: skreddy $
 *� $Name: t_eBN_2015_1_StdBundle_1_39 $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_OrderManagement_SL.js,v $
 *� Revision 1.1.2.7.4.2.4.1.6.1  2015/04/13 09:26:47  skreddy
 *� Case# 201412323�
 *� changed the url path which was hard coded
 *�
 *� Revision 1.1.2.7.4.2.4.1  2013/03/01 14:34:54  skreddy
 *� CASE201112/CR201113/LOG201121
 *� Merged from FactoryMation and change the Company name
 *�
 *� Revision 1.1.2.7.4.2  2012/12/14 07:42:52  spendyala
 *� CASE201112/CR201113/LOG201121
 *� moved from 2012.2 branch
 *�
 *� Revision 1.1.2.7.4.1  2012/11/01 14:55:15  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Decimal Qty Conversions
 *�
 *� Revision 1.1.2.7  2012/09/11 00:45:23  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Production Issue Fixes for FISK and BOOMBAH.
 *�
 *� Revision 1.1.2.6  2012/09/07 03:47:56  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Production issue fixes
 *�
 *� Revision 1.1.2.4  2012/07/26 06:02:04  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� UAT Issue Fixes
 *�
 *� Revision 1.1.2.2  2012/07/12 14:40:50  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Added paging and Print PDF and Export Excel.
 *�
 *� Revision 1.1.2.1  2012/07/10 06:31:51  spendyala
 *� New Scripts for Order Management.
 *�
 *
 ****************************************************************************/

/**
 * @param request
 * @param response
 */
function OrderManagement(request,response)
{
	if(request.getMethod()=='GET')
	{
		//create the Suitelet form
		var form = nlapiCreateForm('Order Management');

		var orderso = form.addField('custpage_orderso', 'text', 'SO');

		/*
		//Get distinct so's and its IDs into dropdown  
		var orderso = form.addField('custpage_orderso', 'select', 'SO');
		var searchResult=fillsalesorderField(form, orderso,-1);
		addValuesToFields(orderso, searchResult);

		for (var j = 0; searchResult != null && j < searchResult.length; j++)
		{
			var customerSearchResults=searchResult[j];
			for (var i = 0; customerSearchResults != null && i < customerSearchResults.length; i++) {
				if(customerSearchResults[i].getValue('custrecord_ns_ord') != null && customerSearchResults[i].getValue('custrecord_ns_ord') != "" && customerSearchResults[i].getValue('custrecord_ns_ord') != " ")
				{
					var resdo = form.getField('custpage_orderso').getSelectOptions(customerSearchResults[i].getValue('custrecord_ns_ord'), 'is');
					if (resdo != null) {
						if (resdo.length > 0) {
							continue;
						}
					}
				}
				orderso.addSelectOption(customerSearchResults[i].getValue('custrecord_ns_ord'), customerSearchResults[i].getText('custrecord_ns_ord'));
			}
		}*/
		var fromdate = form.addField('custpage_fromdate', 'date', 'From Date');
		var taskstatus = form.addField('custpage_taskstatus', 'select', 'Status');
		taskstatus.addSelectOption('','');
		taskstatus.addSelectOption('1','FO Created but Wave not generated');
		taskstatus.addSelectOption('2','Wave Generated but Pick Report not printed');
		taskstatus.addSelectOption('3','Pick Report Printed but Pick yet to being');
		taskstatus.addSelectOption('4','Pick Completed but no Shipping label');
		taskstatus.addSelectOption('5','Shipping label generated but no Fulfillment confirmation');
		taskstatus.addSelectOption('6','Failed Picks');
		taskstatus.addSelectOption('7','Picking completed but item fulfillment not sent to NS');
		taskstatus.addSelectOption('8','Pack completed but item fulfillment is not posted to NS');
		taskstatus.addSelectOption('9','Payment hold failed but picking completed');
		
		taskstatus.setMandatory(true);
		var todate = form.addField('custpage_todate', 'date', 'To Date');

		form.addSubmitButton('Display');

		response.writePage(form);
	}
	else
	{
		try
		{
			var result;
			var vRecCount=0;
			var ordno="";
			var sku="";
			var ordqty="";
			var nsOrdQty="";
			var Customer="";
			var ctx = nlapiGetContext();
			var vSO = request.getParameter('custpage_orderso');
			var vWave = request.getParameter('custpage_wave');
			var vFromdate = request.getParameter('custpage_fromdate');
			var vTodate = request.getParameter('custpage_todate');
			var vTaskStatus = request.getParameter('custpage_taskstatus');
			nlapiLogExecution('ERROR','vTaskStatus',vTaskStatus);
			var form = nlapiCreateForm("Order Management");

			var orderso = form.addField('custpage_orderso', 'text', 'SO');
			orderso.setDefaultValue(request.getParameter('custpage_orderso'));

			/*//Get distinct so's and its IDs into dropdown  
			var orderso = form.addField('custpage_orderso', 'select', 'SO');
			var searchResult=fillsalesorderField(form, orderso,-1);
			addValuesToFields(orderso, searchResult);*/

			var fromdate = form.addField('custpage_fromdate', 'date', 'From Date');
			fromdate.setDefaultValue(request.getParameter('custpage_fromdate'));
			var taskstatus = form.addField('custpage_taskstatus', 'select', 'Status');
			taskstatus.addSelectOption('','');
			taskstatus.addSelectOption('1','FO Created but Wave not generated');
			taskstatus.addSelectOption('2','Wave Generated but Pick Report not printed');
			taskstatus.addSelectOption('3','Pick Report Printed but Pick yet to being');
			taskstatus.addSelectOption('4','Pick Completed but no Shipping label');
			taskstatus.addSelectOption('5','Shipping label generated but no Fulfillment confirmation');
			taskstatus.addSelectOption('6','Failed Picks');
			taskstatus.addSelectOption('7','Picking competed but item fulfillment not sent to NS');
			taskstatus.addSelectOption('8','Pack completed but item fulfillment is not posted to NS');
			taskstatus.addSelectOption('9','Payment hold failed but picking completed');

			taskstatus.setDefaultValue(vTaskStatus);
			taskstatus.setMandatory(true);

			var formsoid=form.addField('custpage_sointid', 'text', 'SO Id').setDisplayType('hidden');
			
			var todate = form.addField('custpage_todate', 'date', 'To Date');
			todate.setDefaultValue(request.getParameter('custpage_todate'));

			//create a sublist to display values.
			var sublist = form.addSubList("custpage_items", "list", "Order List");
			sublist.addField("custpage_nsorderno", "text", "NS OrderNo#");
			sublist.addField("custpage_fo", "text", "FO#");
			sublist.addField("custpage_item", "text", "Item");
			if(vTaskStatus==2)
			{
				sublist.addField("custpage_waveno", "text", "Wave#");
				sublist.addField("custpage_pickgenqty", "text", "PickGen Qty");
			}

			sublist.addField("custpage_nsordqty", "text", "NS Order Qty");
			sublist.addField("custpage_ordqty", "text", "eBiz Order Qty");
			sublist.addField("custpage_customer", "text", "Customer");
			if(vTaskStatus==6)
			{
				sublist.addField("custpage_allocation_link", "text", "");
				sublist.addField("custpage_invt_link", "text", "");
			}
			var soid="";
			var soText=request.getParameter('custpage_orderso');
			if(soText!=null&&soText!="")
				soid = GetSOInternalId(soText);
			nlapiLogExecution('ERROR','soid',soid);
			if((soid != null && soid != "")||(soText==""))
			{
				formsoid.setDefaultValue(soid);
				//1=FO Created but Wave not generated.
				//2=Wave Generated but Pick Report not printed.
				//3=Pick Report Printed but Pick yet to being.

				if(vTaskStatus==1||vTaskStatus==2||vTaskStatus==3)
				{
					result=OrderLineRecord(request,vTaskStatus,soid,-1);
					setPagingForSublist(result,form,vTaskStatus,vFromdate,vTodate);
					nlapiLogExecution('ERROR','results',result);

					/*if(result!=null && result!="")
						{
							for(var i=0;i<result.length;i++)
							{
								var salessearchresults=result;
								nlapiLogExecution('ERROR', 'i', i);
								var NSOrderNo=salessearchresults[i].getText('custrecord_ns_ord');
								var Customer=salessearchresults[i].getText('custrecord_do_customer');
								var ordno=salessearchresults[i].getValue('custrecord_lineord');
								var ordqty=salessearchresults[i].getValue('custrecord_ord_qty');
								var sku=salessearchresults[i].getText('custrecord_ebiz_linesku');
								var wave=salessearchresults[i].getValue('custrecord_ebiz_wave');
								var pickgenQty=salessearchresults[i].getValue('custrecord_pickgen_qty');

								form.getSubList('custpage_items').setLineItemValue('custpage_nsorderno', vRecCount + 1, NSOrderNo);
								form.getSubList('custpage_items').setLineItemValue('custpage_fo', vRecCount + 1, ordno);
								form.getSubList('custpage_items').setLineItemValue('custpage_item', vRecCount + 1, sku);
								form.getSubList('custpage_items').setLineItemValue('custpage_nsordqty', vRecCount + 1, nsOrdQty);
								form.getSubList('custpage_items').setLineItemValue('custpage_ordqty', vRecCount + 1, ordqty);
								form.getSubList('custpage_items').setLineItemValue('custpage_customer', vRecCount + 1, Customer);
								if(vTaskStatus==2)
								{
									form.getSubList('custpage_items').setLineItemValue('custpage_waveno', vRecCount + 1, wave);
									form.getSubList('custpage_items').setLineItemValue('custpage_pickgenqty', vRecCount + 1, pickgenQty);
								}
								vRecCount++;
							}
						}*/
				}
				//4=Pick Completed but no Shipping label; 
				//5=Shipping label generated but no Fulfillment confirmation
				else if(vTaskStatus==4||vTaskStatus==5)
				{
					nlapiLogExecution('ERROR','vtaskstatus',vTaskStatus);
					var result=ShipManifestRecord(request,vTaskStatus,soid);
					if(result!=null&&result!="")
					{
						nlapiLogExecution('ERROR','Manifest Record Length',result.length);

						var resultArray=new Array();

						var uniqueSo=GetUniqueSO(result);
						var openSo = removeClosedandBilledOrders(uniqueSo);
						nlapiLogExecution('ERROR','removeClosedandBilledOrders',openSo);
						if(openSo!=null && openSo!='')
						{
							var OpenTaskRec=GetOpenTaskRecord(openSo,request);
							resultArray.push(OpenTaskRec);
							setPagingForSublist(resultArray,form,vTaskStatus,vFromdate,vTodate);
							/*	if(OpenTaskRec!=null&&OpenTaskRec!="")
							{
								for ( var count = 0; count < OpenTaskRec.length; count++) 
								{

									var Customer="";
									var ordno=OpenTaskRec[count].getValue('name',null,'group');
//									var NSOrderNo=ordno.split('.')[0];
									var NSOrderNo=OpenTaskRec[count].getValue('custrecord_ebiz_order_no',null,'max');
									var ordqty=OpenTaskRec[count].getValue('custrecord_expe_qty',null,'sum');
									var actqty=OpenTaskRec[count].getValue('custrecord_act_qty',null,'sum');
									var sku=OpenTaskRec[count].getText('custrecord_sku',null,'group');
									Customer=OpenTaskRec[count].getText('entity','custrecord_ebiz_order_no','group');
									if(parseFloat(actqty)>0)
									{
										form.getSubList('custpage_items').setLineItemValue('custpage_nsorderno', vRecCount + 1, NSOrderNo);
										form.getSubList('custpage_items').setLineItemValue('custpage_fo', vRecCount + 1, ordno);
										form.getSubList('custpage_items').setLineItemValue('custpage_item', vRecCount + 1, sku);
										form.getSubList('custpage_items').setLineItemValue('custpage_nsordqty', vRecCount + 1, nsOrdQty);
										form.getSubList('custpage_items').setLineItemValue('custpage_ordqty', vRecCount + 1, ordqty);
										form.getSubList('custpage_items').setLineItemValue('custpage_customer', vRecCount + 1, Customer);
										vRecCount++;
									}
								}
							}*/
						}
					}
				}
				//6=Pick Failed
				else if(vTaskStatus==6)
				{
					var Searchresult=GetpickFailedRecord(request,soid);
					var resultArray=new Array();
					resultArray.push(Searchresult);
					setPagingForSublist(resultArray,form,vTaskStatus,vFromdate,vTodate);
					/*if(Searchresult!=null&&Searchresult!="")
						{
							for ( var count = 0; count < Searchresult.length; count++) 
							{

								var NSOrderNo=Searchresult[count].getText('custrecord_ebiz_order_no');
						var Customer=Searchresult[count].getText('entity','custrecord_ebiz_order_no');
						var ordno=Searchresult[count].getValue('name');
						var ordqty=Searchresult[count].getValue('custrecord_expe_qty');
						var sku=Searchresult[count].getText('custrecord_sku');
						var skuid=Searchresult[count].getValue('custrecord_sku');

								var Customer="";
								var ordno=Searchresult[count].getValue('name',null,'group');
//								var NSOrderNo=ordno.split('.')[0];
								var NSOrderNo=Searchresult[count].getValue('custrecord_ebiz_order_no',null,'max');
								var ordqty=Searchresult[count].getValue('custrecord_expe_qty',null,'sum');
								var actqty=Searchresult[count].getValue('custrecord_act_qty',null,'sum');
								var sku=Searchresult[count].getText('custrecord_sku',null,'group');
								var skuid=Searchresult[count].getValue('custrecord_sku',null,'group');
								Customer=Searchresult[count].getText('entity','custrecord_ebiz_order_no','group');
								if(parseFloat(actqty)>0)
								{
									form.getSubList('custpage_items').setLineItemValue('custpage_nsorderno', vRecCount + 1, NSOrderNo);
									form.getSubList('custpage_items').setLineItemValue('custpage_fo', vRecCount + 1, ordno);
									form.getSubList('custpage_items').setLineItemValue('custpage_item', vRecCount + 1, sku);
									form.getSubList('custpage_items').setLineItemValue('custpage_nsordqty', vRecCount + 1, nsOrdQty);
									form.getSubList('custpage_items').setLineItemValue('custpage_ordqty', vRecCount + 1, ordqty);
									form.getSubList('custpage_items').setLineItemValue('custpage_customer', vRecCount + 1, Customer);

									LinkInvt=addURL(ctx.getEnvironment(), skuid ,vFromdate,vTodate, '1');
									LinkAlloc=addURL(ctx.getEnvironment(), skuid,vFromdate,vTodate, '2');
									nlapiLogExecution('ERROR', 'linkURL', LinkInvt);
									nlapiLogExecution('ERROR', 'linkURL', LinkAlloc);

									form.getSubList('custpage_items').setLineItemValue('custpage_allocation_link', vRecCount + 1, '<a href="' + LinkAlloc + '">View Current Allocations</a>');
									form.getSubList('custpage_items').setLineItemValue('custpage_invt_link', vRecCount + 1, '<a href="' + LinkInvt + '">View Inventory Report</a>');

									vRecCount++;
								}
							}
						}*/
				}
				else if(vTaskStatus==7)
				{
					var Searchresult=Getpickcomplete_ItemFullFailedRecord(request,soid);
					var resultArray=new Array();
					resultArray.push(Searchresult);
					setPagingForSublist(resultArray,form,vTaskStatus,vFromdate,vTodate);
				}
				else if(vTaskStatus==8)
				{
					var Searchresult=Getpackcomplete_ItemFullFailedRecord(request,soid);
					var resultArray=new Array();
					resultArray.push(Searchresult);
					setPagingForSublist(resultArray,form,vTaskStatus,vFromdate,vTodate);
				}
				else if(vTaskStatus==9)
				{
					var Searchresult=PaymentHoldFailed_PickingCompleted(request,soid);
					var resultArray=new Array();
					resultArray.push(Searchresult);
					setPagingForSublist(resultArray,form,vTaskStatus,vFromdate,vTodate);
				}
			}
			else
			{
				//Invalid SOid
				var msg= form.addField('custpage_message', 'inlinehtml', null, null, null);
				var msgtext = "Invalid Order# : "+soText;
				msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', '"+ msgtext + "', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
			}

			var printid="1";
			var excelid="2";
			form.addSubmitButton('Display');
			// code added on 11July2012 by santosh	
			form.setScript('customscript_ebiz_ordermanagement_cl');		
			//form.addButton('custpage_print','Print','Printreport('+printid+')');
			form.addButton('custpage_excel','ExportToExcel','Printreport('+excelid+')');
			// end of code on 11July2012 
			response.writePage(form);
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR','Exception in Main else',exp);	
		}
	}
}

var ordersAlreadyAddedList = new Array();
/**
 * @param form
 * @param salesorderField
 * @param maxno
 * @returns {Array}
 */
function fillsalesorderField(form, salesorderField,maxno){
	var salesorderFilers = new Array();
	if(maxno!=-1)
	{
		salesorderFilers.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno)));
	}
	salesorderField.addSelectOption("", "");
	var columns=new Array();
	columns[0]=new nlobjSearchColumn('custrecord_ns_ord');
	columns[1]=new nlobjSearchColumn('id').setSort(true);

	var customerSearchResults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, salesorderFilers,columns);

	if(customerSearchResults!=null && customerSearchResults.length>=1000)
	{
		ordersAlreadyAddedList.push(customerSearchResults);
		var maxno=customerSearchResults[customerSearchResults.length-1].getValue('id');		
		fillsalesorderField(form, salesorderField,maxno);	
	}
	else
	{
		ordersAlreadyAddedList.push(customerSearchResults);
	}
	nlapiLogExecution('ERROR','ordersAlreadyAddedList',ordersAlreadyAddedList);
	return ordersAlreadyAddedList;
}

var tempOTResultsArray=new Array();
/**
 * @param request
 * @param option
 * @returns
 */
function OrderLineRecord(request,option,soid,maxno)
{
	nlapiLogExecution('ERROR', 'option',option);
	try{
		var i = 0;
		var filters = new Array();
		if (soid != ""&&soid!=null) {
			nlapiLogExecution('ERROR', 'Inside SO', soid);
			filters.push(new nlobjSearchFilter('custrecord_ns_ord', null, 'anyof', soid));
		}

		if ((request.getParameter('custpage_fromdate') != "" && request.getParameter('custpage_fromdate') != null) && (request.getParameter('custpage_todate') != "" && request.getParameter('custpage_todate') != null) ) {
			nlapiLogExecution('ERROR', 'fromdate', request.getParameter('custpage_fromdate'));
			nlapiLogExecution('ERROR', 'todate', request.getParameter('custpage_todate'));

			filters.push(new nlobjSearchFilter('custrecord_record_linedate', null, 'within', request.getParameter('custpage_fromdate'), request.getParameter('custpage_todate')));
		} 
		if(option==1)
			filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof',25 ));//25 STATUS.INBOUND_OUTBOUND.EDIT 
		else if(option==2)
		{
			filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof',[9] ));//9 STATUS.OUTBOUND.PICK_GENERATED
			filters.push(new nlobjSearchFilter('custrecord_printflag', null, 'is','F' ));
		}
		else if(option==3)
		{
			filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof',[9,11] ));//9 STATUS.OUTBOUND.PICK_GENERATED;11=STATUS.OUTBOUND.ORDER_PARTIALLY_PROCESSED
			filters.push(new nlobjSearchFilter('custrecord_printflag', null, 'is','T' ));
			filters.push(new nlobjSearchFilter('custrecord_ord_qty', null, 'greaterthan', 0));
			filters.push(new nlobjSearchFilter('custrecord_pickgen_qty', null, 'greaterthan', 0));
		}

		if(maxno!=-1)
		{

			filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));
		}



		var columns=new Array();
		columns.push(new nlobjSearchColumn('internalid'));
		columns.push(new nlobjSearchColumn('custrecord_ns_ord'));
		columns.push(new nlobjSearchColumn('custrecord_lineord'));
		columns.push(new nlobjSearchColumn('custrecord_ordline'));
		columns.push(new nlobjSearchColumn('custrecord_ord_qty'));
		columns.push(new nlobjSearchColumn('custrecord_pickgen_qty'));
		columns.push(new nlobjSearchColumn('custrecord_pickqty'));
		columns.push(new nlobjSearchColumn('custrecord_ship_qty'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_linesku'));
		columns.push(new nlobjSearchColumn('custrecord_record_linedate'));
		columns.push(new nlobjSearchColumn('custrecord_linestatus_flag'));
		columns.push(new nlobjSearchColumn('custrecord_printflag'));
		columns.push(new nlobjSearchColumn('custrecord_record_linetime'));
		columns.push(new nlobjSearchColumn('lastmodified'));
		columns.push(new nlobjSearchColumn('custrecord_do_customer'));
		columns[0].setSort();
		columns[1].setSort();
		columns.push(new nlobjSearchColumn('custrecord_ebiz_wave'));
//		columns.push(new nlobjSearchColumn('entity','custrecord_ns_ord'));

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters,columns);
		if(searchresults!=null)
		{
			nlapiLogExecution('ERROR','searchresults',searchresults.length);
			if(searchresults.length>=1000)
			{
				nlapiLogExecution('ERROR','searchresults','more than 1000');
				var maxno1=searchresults[searchresults.length-1].getValue(columns[0]);
				tempOTResultsArray.push(searchresults);
				OrderLineRecord(request,option,soid,maxno1);

			}
			else
			{
				tempOTResultsArray.push(searchresults);
			}
		}
		return tempOTResultsArray;
//		return SearchResults;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in OrderLineRecord',exp);	
	}
}

/**
 * @param request
 * @param vTaskStatus
 * @returns
 */
function ShipManifestRecord(request,vTaskStatus,soid)
{
	nlapiLogExecution('ERROR', 'Into ShipManifestRecord', vTaskStatus);
	try
	{	
		var filters = new Array();
		if (soid != ""&&soid!=null) {
			nlapiLogExecution('ERROR', 'Inside SO', soid);
			filters.push(new nlobjSearchFilter('custrecord_ship_order', null, 'is', soid));
		}
		if ((request.getParameter('custpage_fromdate') != "" && request.getParameter('custpage_fromdate') != null) && (request.getParameter('custpage_todate') != "" && request.getParameter('custpage_todate') != null) ) 
		{
			nlapiLogExecution('ERROR', 'fromdate', request.getParameter('custpage_fromdate'));
			nlapiLogExecution('ERROR', 'todate', request.getParameter('custpage_todate'));

			filters.push(new nlobjSearchFilter('lastmodified', null, 'within', request.getParameter('custpage_fromdate'), request.getParameter('custpage_todate')));
		} 

		if(vTaskStatus==4)
			filters.push(new nlobjSearchFilter('custrecord_ship_masttrackno',null,'isempty'));

		else if(vTaskStatus==5)
		{
			filters.push(new nlobjSearchFilter('custrecord_ship_masttrackno',null,'isnotempty'));
			filters.push(new nlobjSearchFilter('custrecord_ship_nsconf_no',null,'isempty'));
		}

		var columns=new Array();
		columns[0]=new nlobjSearchColumn('custrecord_ship_order');
		columns[1]=new nlobjSearchColumn('custrecord_ship_consignee');
		columns[2]=new nlobjSearchColumn('custrecord_ship_orderno');
//		columns[1]=new nlobjSearchColumn('created');
		var searchresult=nlapiSearchRecord('customrecord_ship_manifest',null,filters,columns);

		return searchresult;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in OpentaskRecord',exp);	
	}
}

/**
 * @param OrderList
 * @param searchResult
 */
function addValuesToFields(OrderList, searchResult)
{
	nlapiLogExecution('ERROR', 'addValuesToFields', 'Start');
	var ordersAlreadyAddedList = new Array();

	if(searchResult != null && searchResult.length > 0)
	{
		nlapiLogExecution('ERROR', 'searchResult.length', searchResult.length);
		for(var j = 0; j < searchResult.length; j++)
		{
			var openTaskRecords=searchResult[j];
			nlapiLogExecution('ERROR', 'openTaskRecords', openTaskRecords.length);
			for(var i = 0; i < openTaskRecords.length; i++)
			{
				var currentTask = openTaskRecords[i];
				var OrderNo = currentTask.getValue('custrecord_ns_ord');
				var OrderNoText = currentTask.getText('custrecord_ns_ord');
//				nlapiLogExecution('ERROR', 'OrderNo', OrderNo);
				if(OrderList != null){
					if(!isValueAlreadyAdded(ordersAlreadyAddedList, OrderNo)){

						OrderList.addSelectOption(OrderNo, OrderNoText);
						ordersAlreadyAddedList.push(OrderNo);
					}
				}
			}
		}
	}
}

/**
 * @param searchResult
 * @returns {Array}
 */
function GetUniqueSO(searchResult)
{
	nlapiLogExecution('ERROR', 'GetUniqueSO', 'GetUniqueSO');
	var ordersAlreadyAddedList = new Array();

	if(searchResult != null && searchResult.length > 0)
	{
		nlapiLogExecution('ERROR', 'searchResult.length', searchResult.length);

		for(var i = 0; i < searchResult.length; i++)
		{
			var currentTask = searchResult[i];
			var OrderNo = currentTask.getValue('custrecord_ship_order');
			if(!isValueAlreadyAdded(ordersAlreadyAddedList, OrderNo))
			{

				ordersAlreadyAddedList.push(OrderNo);

			}
		}
	}
	return ordersAlreadyAddedList;
}

/**
 * @param alreadyAddedList
 * @param currentValue
 * @returns {Boolean}
 */
function isValueAlreadyAdded(alreadyAddedList, currentValue)
{
	var alreadyAdded = false;

	for(var i = 0; i < alreadyAddedList.length; i++){
		if(alreadyAddedList[i] == currentValue){
			alreadyAdded = true;
			break;
		}
	}
	return alreadyAdded;
}


function removeClosedandBilledOrders(ordarray)
{
	nlapiLogExecution('ERROR', 'Into removeClosedandBilledOrders', ordarray);

	var filter = new Array();
	var column = new Array();
	var soarray = new Array();

	filter.push(new nlobjSearchFilter('internalid',null,'anyof',ordarray));			
	filter.push(new nlobjSearchFilter('mainline',null,'is','T'));

	column[0] = new nlobjSearchColumn('status');	
	column[1] = new nlobjSearchColumn('internalid');

	var searchRecord = nlapiSearchRecord('salesorder',null,filter,column);

	if(searchRecord!=null && searchRecord!='')
	{
		for(var i = 0; i < searchRecord.length; i++)
		{
			var status = searchRecord[i].getValue('status');
			nlapiLogExecution('ERROR', 'status', status);


			if(status!='fullyBilled' && status!='closed' && status!='cancelled')
			{
				//nlapiLogExecution('ERROR', 'status', status);

				soarray.push(searchRecord[i].getValue('internalid'));
			}
		}
	}

	nlapiLogExecution('ERROR', 'Out of removeClosedandBilledOrders', soarray);

	return soarray;
}

/**
 * @param uniqueSo
 * @param request
 * @returns
 */
function GetOpenTaskRecord(uniqueSo,request)
{
	nlapiLogExecution('ERROR', 'Into GetOpenTaskRecord',uniqueSo);
	try
	{
		var filter=new Array();
		if(uniqueSo!=null&&uniqueSo!="")
			filter.push(new nlobjSearchFilter('custrecord_ebiz_order_no',null,'anyof',uniqueSo));
		if ((request.getParameter('custpage_fromdate') != "" && request.getParameter('custpage_fromdate') != null) && (request.getParameter('custpage_todate') != "" && request.getParameter('custpage_todate') != null) ) 
		{
			nlapiLogExecution('ERROR', 'fromdate', request.getParameter('custpage_fromdate'));
			nlapiLogExecution('ERROR', 'todate', request.getParameter('custpage_todate'));

			filter.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'within', request.getParameter('custpage_fromdate'), request.getParameter('custpage_todate')));
		} 
		filter.push(new nlobjSearchFilter('custrecord_wms_status_flag',null,'anyof',28));//Pack-complete
		filter.push(new nlobjSearchFilter('custrecord_tasktype',null,'anyof',3));//pick
		filter.push(new nlobjSearchFilter('mainline','custrecord_ebiz_order_no','is','T'));
		//filter.push(new nlobjSearchFilter('custrecord_act_qty',null,'greaterthan',0));

		var column=new Array();
		//column[0]=new nlobjSearchColumn('custrecord_ebiz_order_no',null,'group');
		column[0]=new nlobjSearchColumn('name',null,'group');
		column[1]=new nlobjSearchColumn('custrecord_sku',null,'group');
		column[2]=new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
		column[3]=new nlobjSearchColumn('custrecord_ebiz_order_no',null,'max');
		column[4]=new nlobjSearchColumn('entity','custrecord_ebiz_order_no','group');
		column[5]=new nlobjSearchColumn('custrecord_act_qty',null,'max');
		column[0].setSort();
		var searchRecord=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filter,column);

		return searchRecord;

	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetOpenTaskRecord',exp);
	}
}

/**
 * @param request
 * @returns
 */
function GetpickFailedRecord(request,soid)
{
	try
	{
		var filtersso= new Array();
		if (request.getParameter('custpage_orderso') != "") 
			filtersso.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'anyof',soid));

		if ((request.getParameter('custpage_fromdate') != "" && request.getParameter('custpage_fromdate') != null) && (request.getParameter('custpage_todate') != "" && request.getParameter('custpage_todate') != null) ) 
		{
			filtersso.push(new nlobjSearchFilter('custrecordact_begin_date', null, 'within', request.getParameter('custpage_fromdate'), request.getParameter('custpage_todate')));
		}

		filtersso.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['26']));//STATUS.OUTBOUND.FAILED
		filtersso.push(new nlobjSearchFilter('mainline','custrecord_ebiz_order_no','is', 'T'));

		/*var columnso = new Array();
		columnso[0]=new nlobjSearchColumn('custrecord_ebiz_order_no');
		columnso[1]=new nlobjSearchColumn('custrecord_sku');
		columnso[2]=new nlobjSearchColumn('custrecord_expe_qty');
		columnso[3]=new nlobjSearchColumn('name');
		columnso[4]=new nlobjSearchColumn('entity','custrecord_ebiz_order_no');*/
		var columnso = new Array();
		columnso[0]=new nlobjSearchColumn('custrecord_sku',null,'group');
		columnso[1]=new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
		columnso[2]=new nlobjSearchColumn('name',null,'group');
		columnso[3]=new nlobjSearchColumn('entity','custrecord_ebiz_order_no','group');
		columnso[4]=new nlobjSearchColumn('custrecord_ebiz_order_no',null,'max');
		columnso[5]=new nlobjSearchColumn('custrecord_act_qty',null,'max');

		columnso[2].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso, columnso);
		return searchresults;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetpickFailedRecord',exp);
	}
}


/**
 * @param request
 * @returns
 */
function Getpickcomplete_ItemFullFailedRecord(request,soid)
{
	try
	{
		var filtersso= new Array();
		if (request.getParameter('custpage_orderso') != "") 
			filtersso.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'anyof',soid));

		if ((request.getParameter('custpage_fromdate') != "" && request.getParameter('custpage_fromdate') != null) && (request.getParameter('custpage_todate') != "" && request.getParameter('custpage_todate') != null) ) 
		{
			filtersso.push(new nlobjSearchFilter('custrecordact_begin_date', null, 'within', request.getParameter('custpage_fromdate'), request.getParameter('custpage_todate')));
		}

		filtersso.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', [8,28]));//28 STATUS.OUTBOUND.PACK_COMPLETE ;8 STATUS.OUTBOUND.PICK_CONFIRMED 
		filtersso.push(new nlobjSearchFilter('custrecord_tasktype', null,'anyof',3));//3=Pick
		filtersso.push(new nlobjSearchFilter('mainline','custrecord_ebiz_order_no','is', 'T'));
		filtersso.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',null,'isempty'));
		filtersso.push(new nlobjSearchFilter('custrecord_act_qty',null,'isnot','0'));
		filtersso.push(new nlobjSearchFilter('custrecord_container_lp_no',null,'isnotempty'));

		var columnso = new Array();
		columnso[0]=new nlobjSearchColumn('custrecord_sku',null,'group');
		columnso[1]=new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
		columnso[2]=new nlobjSearchColumn('name',null,'group');
		columnso[3]=new nlobjSearchColumn('entity','custrecord_ebiz_order_no','group');
		columnso[4]=new nlobjSearchColumn('custrecord_ebiz_order_no',null,'max');
		columnso[5]=new nlobjSearchColumn('custrecord_act_qty',null,'max');

		columnso[2].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso, columnso);
		return searchresults;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in Getpickcomplete_ItemFullFailedRecord',exp);
	}
}

function Getpackcomplete_ItemFullFailedRecord(request,soid)
{
	try
	{
		var filtersso= new Array();
		if (request.getParameter('custpage_orderso') != "") 
			filtersso.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'anyof',soid));

		if ((request.getParameter('custpage_fromdate') != "" && request.getParameter('custpage_fromdate') != null) && (request.getParameter('custpage_todate') != "" && request.getParameter('custpage_todate') != null) ) 
		{
			filtersso.push(new nlobjSearchFilter('custrecordact_begin_date', null, 'within', request.getParameter('custpage_fromdate'), request.getParameter('custpage_todate')));
		}

		filtersso.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', 28));//28 STATUS.OUTBOUND.PACK_COMPLETE ;8 STATUS.OUTBOUND.PICK_CONFIRMED 
		filtersso.push(new nlobjSearchFilter('custrecord_tasktype', null,'anyof',3));//3=Pick
		filtersso.push(new nlobjSearchFilter('mainline','custrecord_ebiz_order_no','is', 'T'));
		filtersso.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',null,'isempty'));

		var columnso = new Array();
		columnso[0]=new nlobjSearchColumn('custrecord_sku',null,'group');
		columnso[1]=new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
		columnso[2]=new nlobjSearchColumn('name',null,'group');
		columnso[3]=new nlobjSearchColumn('entity','custrecord_ebiz_order_no','group');
		columnso[4]=new nlobjSearchColumn('custrecord_ebiz_order_no',null,'max');
		columnso[5]=new nlobjSearchColumn('custrecord_act_qty',null,'max');

		columnso[2].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso, columnso);
		return searchresults;
	}
	catch(exp)
	{
	nlapiLogExecution('ERROR','Exception in Getpackcomplete_ItemFullFailedRecord',exp);	
	}
}


function PaymentHoldFailed_PickingCompleted(request,soid)
{
	try
	{
		var filtersso= new Array();
		if (request.getParameter('custpage_orderso') != "") 
			filtersso.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'anyof',soid));

		if ((request.getParameter('custpage_fromdate') != "" && request.getParameter('custpage_fromdate') != null) && (request.getParameter('custpage_todate') != "" && request.getParameter('custpage_todate') != null) ) 
		{
			filtersso.push(new nlobjSearchFilter('custrecordact_begin_date', null, 'within', request.getParameter('custpage_fromdate'), request.getParameter('custpage_todate')));
		}

		filtersso.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', 8));//28 STATUS.OUTBOUND.PACK_COMPLETE ;8 STATUS.OUTBOUND.PICK_CONFIRMED 
		filtersso.push(new nlobjSearchFilter('custrecord_tasktype', null,'anyof',3));//3=Pick
		filtersso.push(new nlobjSearchFilter('mainline','custrecord_ebiz_order_no','is', 'T'));
		filtersso.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',null,'isempty'));
		filtersso.push(new nlobjSearchFilter('paymenteventresult','custrecord_ebiz_order_no','is', 'HOLD'));

		var columnso = new Array();
		columnso[0]=new nlobjSearchColumn('custrecord_sku',null,'group');
		columnso[1]=new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
		columnso[2]=new nlobjSearchColumn('name',null,'group');
		columnso[3]=new nlobjSearchColumn('entity','custrecord_ebiz_order_no','group');
		columnso[4]=new nlobjSearchColumn('custrecord_ebiz_order_no',null,'max');
		columnso[5]=new nlobjSearchColumn('custrecord_act_qty',null,'max');

		columnso[2].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso, columnso);
		return searchresults;
	}
	catch(exp)
	{
	nlapiLogExecution('ERROR','Exception in Getpackcomplete_ItemFullFailedRecord',exp);	
	}
}


/**
 * @param environment
 * @param Item
 * @param fromdate
 * @param todate
 * @param type
 * @returns
 */
function addURL(environment, Item,fromdate,todate,type) 
{
	var SoURL;
	if(type =='1')
	{
		nlapiLogExecution('ERROR', 'Type', 'type1');
//		SoURL = nlapiResolveURL('SUITELET','customscript_ebiz_ordermgmt_invtreport_s', 'customdeploy_ebiz_ordermgmt_invtreport_d');
		SoURL = nlapiResolveURL('SUITELET','customscript_inventoryreport', 'customdeploy1');
		nlapiLogExecution('ERROR', 'Type', 'InvtLink');
		nlapiLogExecution('ERROR', 'Link', 'LinkEnter');
		// Get the FQDN depending on the context environment
		/*var fqdn = getFQDNForHost(environment);

		var ctx = nlapiGetContext();

		if (fqdn == '')
			nlapiLogExecution('ERROR', 'Unable to retrieve FQDN based on context');
		else
			SoURL = getFQDNForHost(ctx.getEnvironment()) + SoURL;*/

		SoURL = SoURL + '&custparam_Item=' + Item;
		SoURL = SoURL + '&custparam_Fromdate=' + fromdate;
		SoURL = SoURL + '&custparam_Todate=' + todate;
		nlapiLogExecution('ERROR', 'UrL', SoURL);
		return SoURL;

	}
	if(type =='2')
	{
		SoURL = nlapiResolveURL('SUITELET','customscript_ebiz_allocreport', 'customdeploy_ebiz_allocreport_di');
		nlapiLogExecution('ERROR', 'Type', 'AllocLink');
		nlapiLogExecution('ERROR', 'Link', 'LinkEnter');
		// Get the FQDN depending on the context environment
		/*var fqdn = getFQDNForHost(environment);

		var ctx = nlapiGetContext();

		if (fqdn == '')
			nlapiLogExecution('ERROR', 'Unable to retrieve FQDN based on context');
		else
			SoURL = getFQDNForHost(ctx.getEnvironment()) + SoURL;*/

		SoURL = SoURL + '&custparam_Item=' + Item;
		SoURL = SoURL + '&custparam_Fromdate=' + fromdate;
		SoURL = SoURL + '&custparam_Todate=' + todate;
		nlapiLogExecution('ERROR', 'UrL', SoURL);
		return SoURL;

	}

}


/**
 * 
 * @param SOid // To get SO internal Id
 */
function GetSOInternalId(SOid)
{
	var salesorderFilers = new Array();
	salesorderFilers.push(new nlobjSearchFilter('mainline', null, 'is', 'T')); 
	salesorderFilers.push(new nlobjSearchFilter('tranid', null, 'is', SOid)); 
	//salesorderFilers.push(new nlobjSearchFilter('line', null, 'is', ordline)); 


	var customerSearchResults = nlapiSearchRecord('salesorder', null, salesorderFilers,null);	
	if(customerSearchResults != null && customerSearchResults != "")
	{
		nlapiLogExecution('ERROR', 'customerSearchResults.length ',customerSearchResults.length);
		return customerSearchResults[0].getId();
	}
	else
		return null;
}




function setPagingForSublist(orderList,form,vTaskStatus,vFromdate,vTodate)
{
	if(orderList != null && orderList.length > 0){
		nlapiLogExecution('Error', 'orderList ', orderList.length);
		var orderListArray=new Array();		
		for(k=0;k<orderList.length;k++)
		{
			//nlapiLogExecution('ERROR', 'orderList[k]', orderList[k]); 
			var ordsearchresult = orderList[k];

			if(ordsearchresult!=null)
			{
				//nlapiLogExecution('ERROR', 'ordsearchresult.length ', ordsearchresult.length); 
				for(var j=0;j<ordsearchresult.length;j++)
				{
					orderListArray[orderListArray.length]=ordsearchresult[j];				
				}
			}
		}
		//nlapiLogExecution('ERROR', 'orderListArray.length ', orderListArray.length); 
		var test='';
		form.setScript('customscript_ebiz_ordermanagement_cl');
		if(orderListArray.length>0)
		{

			if(orderListArray.length>25)
			{
				var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('hidden');
				pagesize.setDisplaySize(10,10);
				pagesize.setLayoutType('outsidebelow', 'startrow');
				var select= form.addField('custpage_selectpage','select', 'Select Records(# of Lines)');	
				select.setLayoutType('outsidebelow', 'startrow');			
				select.setDisplaySize(200,30);
				if (request.getMethod() == 'GET'){
					pagesize.setDefaultValue("100");
					pagesizevalue=25;
				}
				else
				{
					if(request.getParameter('custpage_pagesize')!=null)
					{
						pagesizevalue= request.getParameter('custpage_pagesize');
					}
					else
					{
						pagesizevalue=25;
						pagesize.setDefaultValue("25");
					}
				}

				//this is to add the pageno's to the dropdown.
				var len=orderListArray.length/parseFloat(pagesizevalue);
				for(var k=1;k<=Math.ceil(len);k++)
				{

					var from;var to;

					to=parseFloat(k)*parseFloat(pagesizevalue);
					from=(parseFloat(to)-parseFloat(pagesizevalue))+1;

					if(parseFloat(to)>orderListArray.length)
					{
						to=orderListArray.length;
						test=from.toString()+","+to.toString(); 
					}

					var temp=from.toString()+" to "+to.toString();
					var tempto=from.toString()+","+to.toString();
					select.addSelectOption(tempto,temp);

				} 

				if (request.getMethod() == 'POST'){

					if(request.getParameter('custpage_selectpage')!=null ){

						select.setDefaultValue(request.getParameter('custpage_selectpage'));	

					}
					if(request.getParameter('custpage_pagesize')!=null ){

						pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	

					}
				}
			}
			else
			{
				pagesizevalue=parseFloat(orderListArray.length);
			}

			var minval=0;var maxval=parseFloat(pagesizevalue);
			if(parseFloat(pagesizevalue)>orderListArray.length)
			{
				maxval=orderListArray.length;
			}
			var selectno=request.getParameter('custpage_selectpage');
			nlapiLogExecution('ERROR', 'selectno',selectno);
			if(selectno!=null )
			{
				var selectedPage= request.getParameter('custpage_selectpage');
				var selectedPageArray=selectedPage.split(',');			
				var diff=parseFloat(selectedPageArray[1])-(parseFloat(selectedPageArray[0])-1);
				//nlapiLogExecution('ERROR', 'diff',diff);

				var pagevalue=request.getParameter('custpage_pagesize');
				nlapiLogExecution('ERROR', 'pagevalue',pagevalue);
				nlapiLogExecution('ERROR', 'selectedPageArray[1]',selectedPageArray[1]);
				nlapiLogExecution('ERROR', 'orderListArray.length',orderListArray.length);
				if(pagevalue!=null&&(parseFloat(selectedPageArray[1])<=orderListArray.length))
				{
					nlapiLogExecution('ERROR', 'checkpt','chkpt');
					if(parseFloat(diff)==parseFloat(pagevalue)|| test==selectno)
					{
						nlapiLogExecution('ERROR', 'diff',diff);
						var selectedPageArray=selectno.split(',');	
						//nlapiLogExecution('ERROR', 'selectedPageArray.length ', selectedPageArray.length);  
						minval=parseFloat(selectedPageArray[0])-1;
						//nlapiLogExecution('ERROR', 'selectedPageArray[0] ', selectedPageArray[0]);  
						maxval=parseFloat(selectedPageArray[1]);
						//nlapiLogExecution('ERROR', 'selectedPageArray[1] ', selectedPageArray[1]);  
					}
				}
			}

			var c=1;
			var minvalue;
			minvalue=minval;
			var prevVal=null;var currVal=null;
			nlapiLogExecution('ERROR', 'minvalue ', minvalue);  
			nlapiLogExecution('ERROR', 'maxval ', maxval); 
			for(var j = minvalue; j < maxval; j++){		
//				nlapiLogExecution('ERROR', 'orderListArray ', orderListArray[j]); 
				var currentOrder = orderListArray[j];
				if(vTaskStatus==1||vTaskStatus==2||vTaskStatus==3||vTaskStatus==6||vTaskStatus==7)
				{
					addOrderToSublist(form, currentOrder, c,vTaskStatus,vFromdate,vTodate);
					c=c+1;
				}
				else
				{
					var actqty=currentOrder.getValue('custrecord_act_qty',null,'max');
					if(parseFloat(actqty)>0)
					{
						addOrderToSublist(form, currentOrder, c,vTaskStatus,vFromdate,vTodate);
						c=c+1;
					}
				}
			}
		}
	}
}


/**
 * Function to add one fulfillment order to the sublist on the form
 * 
 * @param form
 * @param currentOrder
 * @param i
 */
function addOrderToSublist(form, searchresult, i,vTaskStatus,vFromdate,vTodate)
{
	var salessearchresults=searchresult;
	if(vTaskStatus==1||vTaskStatus==2||vTaskStatus==3)
	{

//		nlapiLogExecution('ERROR', 'i', i);
		var NSOrderNo=salessearchresults.getText('custrecord_ns_ord');
		var Customer=salessearchresults.getText('custrecord_do_customer');
		var ordno=salessearchresults.getValue('custrecord_lineord');
		var ordqty=salessearchresults.getValue('custrecord_ord_qty');
		var sku=salessearchresults.getText('custrecord_ebiz_linesku');
		var wave=salessearchresults.getValue('custrecord_ebiz_wave');
		var pickgenQty=salessearchresults.getValue('custrecord_pickgen_qty');
		var nsOrdQty="";
		form.getSubList('custpage_items').setLineItemValue('custpage_nsorderno', i, NSOrderNo);
		form.getSubList('custpage_items').setLineItemValue('custpage_fo', i, ordno);
		form.getSubList('custpage_items').setLineItemValue('custpage_item', i, sku);
		form.getSubList('custpage_items').setLineItemValue('custpage_nsordqty', i, nsOrdQty);
		form.getSubList('custpage_items').setLineItemValue('custpage_ordqty', i, ordqty);
		form.getSubList('custpage_items').setLineItemValue('custpage_customer', i, Customer);
		if(vTaskStatus==2)
		{
			form.getSubList('custpage_items').setLineItemValue('custpage_waveno', i, wave);
			form.getSubList('custpage_items').setLineItemValue('custpage_pickgenqty', i, pickgenQty);
		}
	}
	else
	{
		var Customer="";
		var ordno=salessearchresults.getValue('name',null,'group');

//		var NSOrderNo=ordno.split('.')[0];
		var NSOrderNo=salessearchresults.getValue('custrecord_ebiz_order_no',null,'max');
		var ordqty=salessearchresults.getValue('custrecord_expe_qty',null,'sum');
		var actqty=salessearchresults.getValue('custrecord_act_qty',null,'max');
		//nlapiLogExecution('ERROR','actqty',actqty);
		var sku=salessearchresults.getText('custrecord_sku',null,'group');
		var skuid=salessearchresults.getValue('custrecord_sku',null,'group');
		Customer=salessearchresults.getText('entity','custrecord_ebiz_order_no','group');
//		if(parseFloat(actqty)>0)
//		{
		var nsOrdQty="";
		form.getSubList('custpage_items').setLineItemValue('custpage_nsorderno', i, NSOrderNo);
		form.getSubList('custpage_items').setLineItemValue('custpage_fo', i, ordno);
		form.getSubList('custpage_items').setLineItemValue('custpage_item', i, sku);
		form.getSubList('custpage_items').setLineItemValue('custpage_nsordqty', i, nsOrdQty);
		form.getSubList('custpage_items').setLineItemValue('custpage_ordqty', i, ordqty);
		form.getSubList('custpage_items').setLineItemValue('custpage_customer', i, Customer);
		if(vTaskStatus==6)
		{
			var ctx = nlapiGetContext();
			LinkInvt=addURL(ctx.getEnvironment(), skuid ,vFromdate,vTodate, '1');
			LinkAlloc=addURL(ctx.getEnvironment(), skuid,vFromdate,vTodate, '2');
			nlapiLogExecution('ERROR', 'linkURL', LinkInvt);
			nlapiLogExecution('ERROR', 'linkURL', LinkAlloc);

			form.getSubList('custpage_items').setLineItemValue('custpage_allocation_link', i, '<a href="' + LinkAlloc + '">View Current Allocations</a>');
			form.getSubList('custpage_items').setLineItemValue('custpage_invt_link', i, '<a href="' + LinkInvt + '">View Inventory Report</a>');
		}
//		}
	}
}