/***************************************************************************
 eBizNET Solutions LTD               
 ****************************************************************************/
/* 
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/Attic/ebiz_CYCC_ScheduleResolve.js,v $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * 
 * PRAMETERS
 *
 * DESCRIPTION
 * 
 * REVISION HISTORY
 * $Log: ebiz_CYCC_ScheduleResolve.js,v $
 * Revision 1.1.4.3.2.1  2015/09/21 14:02:11  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.1.4.3  2015/04/10 21:28:09  skreddy
 * Case# 201412323�
 * changed the url path which was hard coded
 *
 * Revision 1.1.4.2  2015/02/04 05:44:09  snimmakayala
 * Case#: 201411397
 * Cycle Count Resolve Scheduler
 *
 *****************************************************************************/

function ScheduleResolve(request, response){

	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Resolve Scheduler');
//		var selcyclecount = form.addField('custpage_cyclecountplan', 'select', 'Cycle Count Plan#', 'customrecord_ebiznet_cylc_createplan');

		var varCycleCountPlanNo =  form.addField('custpage_cyclecountplan','select', 'Cycle Count Plan#');

		var CycWhLocation =  form.addField('custpage_cyclecountwhlocation','select', 'Location','location');
		var Cycitem =  form.addField('custpage_cyclecountitem','select', 'Item','item');
		var CycbinLoc = form.addField('custpage_cyclecountbinloc', 'select', 'Bin Location','customrecord_ebiznet_location');

		var filtersitem = new Array();

		varCycleCountPlanNo.addSelectOption("","");
		var filtersitem = new Array();
		//filtersitem.push(new nlobjSearchFilter('custrecord_cyclerec_date',null, 'isnotempty', null));
		filtersitem.push(new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [31]));
		var vRoleLocation=getRoledBasedLocation();
		if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
		{
			filtersitem.push(new nlobjSearchFilter('custrecord_cyclesite_id', null, 'is', vRoleLocation));
		}

		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_cycle_count_plan',null,'group'));
		columns[0].setSort(true);
		var result = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filtersitem,columns);

		for (var i = 0; result != null && i < result.length; i++) {
			var soresult=result[i];
			var vplan=soresult.getValue('custrecord_cycle_count_plan',null,'group');			
			var res=  form.getField('custpage_cyclecountplan').getSelectOptions(vplan, 'is');
			if (res != null) {				
				if (res.length > 0) {
					continue;
				}
			}		
			varCycleCountPlanNo.addSelectOption(vplan, vplan);
		}
		form.addField('restypeflag', 'radio', 'Show All', 'cyclecountsa').setLayoutType('outsidebelow', 'startcol');
		form.addField('restypeflag', 'radio', 'Show Discrepancy', 'cyclecountsd').setLayoutType('outsidebelow', 'startcol');
		form.addField('restypeflag', 'radio', 'Show Non Discrepancy', 'cyclecountsnd').setLayoutType('outsidebelow', 'startcol');

		form.getField('restypeflag', 'cyclecountsd' ).setDefaultValue( 'cyclecountsd' );

		var flagselected = form.getField('restypeflag').getSelectOptions();

		var buttondisplay = form.addSubmitButton('Display');
		response.writePage(form);
	}
	else {
		var form = nlapiCreateForm('Resolve Scheduler');

		var vsa = form.addField('restypeflag', 'radio', 'Show All', 'cyclecountsa').setLayoutType('outsidebelow', 'startcol');
		var vsd = form.addField('restypeflag', 'radio', 'Show Discrepancy', 'cyclecountsd').setLayoutType('outsidebelow', 'startcol');
		var vsnd = form.addField('restypeflag', 'radio', 'Show Non Discrepancy', 'cyclecountsnd').setLayoutType('outsidebelow', 'startcol');

		nlapiLogExecution('ERROR', 'request.getParameter(restypeflag) before', request.getParameter('restypeflag'));


		if(request.getParameter('restypeflag') != null && request.getParameter('restypeflag') != '')
		{
			if(request.getParameter('custpage_hiddenfieldshow')!=null && request.getParameter('custpage_hiddenfieldshow')!='')
			{
				if((request.getParameter('restypeflag')=='cyclecountsa') || (request.getParameter('custpage_hiddenfieldshow')=='ALL'))
				{
					form.getField('restypeflag', 'cyclecountsa' ).setDefaultValue( 'cyclecountsa' );
				}
				else if((request.getParameter('restypeflag')=='cyclecountsd') || (request.getParameter('custpage_hiddenfieldshow')=='D'))
				{
					form.getField('restypeflag', 'cyclecountsd' ).setDefaultValue( 'cyclecountsd' );
				}
				else if((request.getParameter('restypeflag')=='cyclecountsnd') || (request.getParameter('custpage_hiddenfieldshow')=='ND'))
				{
					form.getField('restypeflag', 'cyclecountsnd' ).setDefaultValue('cyclecountsnd' );
				}
				else
				{

				}
			}
			else
			{
				if(request.getParameter('restypeflag')=='cyclecountsa')
					form.getField('restypeflag', 'cyclecountsa' ).setDefaultValue( 'cyclecountsa' );

				if(request.getParameter('restypeflag')=='cyclecountsd')
					form.getField('restypeflag', 'cyclecountsd' ).setDefaultValue( 'cyclecountsd' );

				if(request.getParameter('restypeflag')=='cyclecountsnd')
					form.getField('restypeflag', 'cyclecountsnd' ).setDefaultValue('cyclecountsnd' );	
			}

		}

		vsa.setDisplayType('hidden');
		vsd.setDisplayType('hidden');
		vsnd.setDisplayType('hidden');

		nlapiLogExecution('ERROR', 'request.getParameter(restypeflag) after', request.getParameter('restypeflag'));

		var varCCplanNo = request.getParameter("custpage_cyclecountplan");
		var isplanclosed='F';

		form.setScript('customscript_cyclecountresolvesubmit');
		//form.addPageLink('crosslink', 'Go Back', 'https://system.netsuite.com/app/site/hosting/scriptlet.nl?script=142&deploy=1');
		var linkURL = nlapiResolveURL('SUITELET', 'customscript_cyclecountresolve', 'customdeploy_cyclecountresolve_di');
        form.addPageLink('crosslink', 'Go Back',linkURL);
		try {
			nlapiLogExecution('ERROR', 'Resolve Plan No1', request.getParameter('custpage_cyclecountplan'));
			nlapiLogExecution('ERROR','hdn1',request.getParameter('custpage_hiddenfieldselectpage'));
			if(varCCplanNo!=null && varCCplanNo!='')
			{
				var fields = ['custrecord_cyccplan_close', 'custrecord_ebiz_include_emploc'];
				var columns = nlapiLookupField('customrecord_ebiznet_cylc_createplan', varCCplanNo, fields);
				isplanclosed = columns.custrecord_cyccplan_close;
			}
			nlapiLogExecution('ERROR', 'isplanclosed', isplanclosed);
			if(isplanclosed=='F'){
				//nlapiLogExecution('ERROR', 'custpage_hiddenfieldselectpage', custpage_hiddenfieldselectpage);
				if (request.getParameter('custpage_hiddenfieldselectpage') != 'F') {

					var buttonsave = form.addSubmitButton('Save');
					var planno = form.addField('custpage_planno', 'text', 'Cycle Count Plan No:').setDisplayType('disabled');
					planno.setDefaultValue(varCCplanNo);
					var buttonsa = form.addButton('custpage_cyclecountsabtn','Show All','showall()');
					var buttonsd = form.addButton('custpage_cyclecountsdbtn','Show Discrepancy','showdiscrepancy()');
					var buttonsnd = form.addButton('custpage_cyclecountsndbtn','Show Non Discrepancy','shownondiscrepancy()');

					nlapiLogExecution('ERROR', 'Resolve Plan No2', request.getParameter('custpage_cyclecountplan'));

					//form.addField('restype', 'radio', 'Resolve', 'resolve').setLayoutType('endrow').setDefaultValue('resolve');
					form.addField('restype', 'radio', 'Resolve', 'resolve').setLayoutType('outsidebelow', 'startrow');
					form.addField('restype', 'radio', 'Resolve & Close', 'close').setLayoutType('outsidebelow', 'startcol');
					form.addField('restype', 'radio', 'Ignore', 'ignore').setLayoutType('outsidebelow', 'startcol');
					form.addField('restype', 'radio', 'Recount', 'recount').setLayoutType('outsidebelow', 'startcol');

					form.getField('restype', 'resolve' ).setDefaultValue( 'resolve' );
					var hiddenfieldshowvalue;
					var hiddenfieldshow=form.addField('custpage_hiddenfieldshow', 'text', 'hiddenflag1').setDisplayType('hidden');
					//hiddenfieldshow.setDefaultValue('F');
					if(request.getParameter('custpage_hiddenfieldshow') != null && request.getParameter('custpage_hiddenfieldshow') != '')
					{
						hiddenfieldshowvalue = request.getParameter('custpage_hiddenfieldshow');
					}

					hiddenfieldshow.setDefaultValue(hiddenfieldshowvalue);

					nlapiLogExecution('ERROR', 'custpage_hiddenfieldshow', request.getParameter('custpage_hiddenfieldshow'));
					nlapiLogExecution('ERROR', 'hiddenfieldshowvalue', hiddenfieldshowvalue);
					nlapiLogExecution('ERROR', 'hiddenfieldshow', hiddenfieldshow);

					var hiddenCycleCountPlan=form.addField('custpage_cyclecountplan', 'text', 'CycleCountPlan').setDisplayType('hidden');
					hiddenCycleCountPlan.setDefaultValue(request.getParameter('custpage_cyclecountplan'));
					var hiddenQueryParams=form.addField('custpage_qeryparams', 'text', 'queryparams').setDisplayType('hidden');
					var hiddenfieldselectpage=form.addField('custpage_hiddenfieldselectpage', 'text', 'hiddenflag').setDisplayType('hidden');
					hiddenfieldselectpage.setDefaultValue('F');

					var k = form.getField('restype').getSelectOptions();

					var chkconfirmtohost=form.addField('custpage_confirmtohost', 'checkbox','Update GL#').setDefaultValue('T');

					addFieldsToForm(form);

					var orderList = getOrdersForCycleCount(request,0,form);

					if(((request.getParameter('restypeflag')=='cyclecountsa' && hiddenfieldshowvalue != 'D') && (request.getParameter('restypeflag')=='cyclecountsa' && hiddenfieldshowvalue != 'ND'))|| hiddenfieldshowvalue == 'ALL')
					{
						nlapiLogExecution('ERROR', 'orderList.lenght tst in all', orderList.length);
						if(orderList != null && orderList.length > 0){
							setPagingForSublist(orderList,form);
						}
					}
					if((request.getParameter('restypeflag')=='cyclecountsd' || hiddenfieldshowvalue == 'D') || (request.getParameter('restypeflag')=='cyclecountsnd' || hiddenfieldshowvalue == 'ND'))
					{
						nlapiLogExecution('ERROR', 'orderList.lenght tst in D or ND', orderList.length);
						if(orderList != null && orderList.length > 0){
							displaysublist(form,orderList,hiddenfieldshowvalue);
						}
					}
				}
				else {
					nlapiLogExecution('ERROR','hdn2',request.getParameter('custpage_hiddenfieldselectpage'));
					var hiddenfieldselectpage=form.addField('custpage_hiddenfieldselectpage', 'text', 'hiddenflag').setDisplayType('hidden');
					hiddenfieldselectpage.setDefaultValue('F');

					var hiddenfieldshow=form.addField('custpage_hiddenfieldshow', 'text', 'hiddenflag1').setDisplayType('hidden');

					nlapiLogExecution('ERROR','count1',request.getLineItemCount('custpage_items'));
					if(request.getLineItemCount('custpage_items')!=null&&request.getLineItemCount('custpage_items')!="")
					{
						var Values=ebiznet_CycleCoundResolveSave_CS(request,form,varCCplanNo);
					}

					nlapiLogExecution('ERROR', 'ELSE', 'ELSEXE--->');

					var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
					if(Values !=null && Values !='') {
						if (Values[0]== false)
						{
							nlapiLogExecution('ERROR', 'nto values[0]', 'done');
							//msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'Allocated Qty is greater than Actual qty for LP's:', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
							msg.setDefaultValue("<div id='div__alert' align='center'><script>showAlertBox('div__alert', 'Error', 'Allocated Qty is greater than Actual qty "+Values[1]+".', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
							response.writePage(form);
							//return;
						}
					}
					//case # 20124324 Start
					else if (request.getParameter('restype')!=null && request.getParameter('restype') == 'resolve')
					{
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Cycle Count Resolved Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
					}
					else if(request.getParameter('restype')!=null && request.getParameter('restype') == 'ignore')
					{
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Cycle Count Ignored Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
					}
					else if(request.getParameter('restype')!=null && request.getParameter('restype') == 'recount')
					{
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Cycle Count Recounted Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
					}
					else
					{
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Cycle Count Resolved Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
					}

					//case # 20124324 End.

					/*else if (form.getField == 'resolve')
					{
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Cycle Count Resolved Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
					}
					else if(form.getField == 'ignore')
					{
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Cycle Count Ignored Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
					}
					else if(form.getField == 'recount')
					{
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Cycle Count Ignored Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
					}
					else
					{
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Cycle Count Resolved Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
					}*/
				}
			}
			else
			{
				showInlineMessage(form, 'Confirmation', 'Plan# :  ' + varCCplanNo + ' is Closed ' , '');
			}
		} 
		catch (e) {
			nlapiLogExecution('ERROR', 'Catch', e);
		}

		response.writePage(form);
		//nlapiLogExecution('ERROR', 'after page submit', ' --->');

	}
}
function ebiznet_CycleCoundResolveSave_CS(request,form,varCCplanNo)
{
	var venteredfields="";
	var vcycrecid,invtrecid,cycplanno,actqty,lp,vcartlpentered,actitem,expitem,whloc,opentaskid;
	var hostid,actbatch,expbatch,expsku,expqty,explp,binloc,qtytobeadjust,actskustatus,qtyvariance




	for (var i1 = 1; i1 <= request.getLineItemCount('custpage_items'); i1++)
	{			
		var chkflag = request.getLineItemValue('custpage_items', 'custpage_rec', i1);
		if(chkflag == 'T')
		{			
			nlapiLogExecution('ERROR', 'chkflag', chkflag);
			vcycrecid =request.getLineItemValue('custpage_items', 'custpage_id', i1);			
			qtytobeadjust =request.getLineItemValue('custpage_items', 'custpage_qtytobeadjust', i1);			
			hostid = request.getParameter('custpage_confirmtohost');

			if(venteredfields==""||venteredfields==null)
				venteredfields=vcycrecid+"&"+qtytobeadjust+"&"+hostid+"$";			  
			else
				venteredfields=venteredfields + vcycrecid+"&"+qtytobeadjust+"&"+hostid+"$";
		}
	}
	nlapiLogExecution('ERROR', 'param', param);
	var restype = request.getParameter('restype');
	var param = new Array();
	param['custscript_nsrestype'] = restype;
	param['custscript_ns_params'] = venteredfields;	
	var context = nlapiGetContext();
	var curuserId = context.getUser();	
	updateScheduleScriptStatus('CYC Resolve',curuserId,'Submitted',varCCplanNo,null);

	nlapiScheduleScript('customscript_ebiz_cycc_resolvescheduler',null,param);
	showInlineMessage(form, 'Confirmation', 'Cycle count resolve has been initiated successfully, It will take some time to resolve, please wait...! ');
	nlapiLogExecution('ERROR', 'Invoke schduler Successfully', 'Start');
}


function GetNSCallFlag(Status,NewStatus)
{
	nlapiLogExecution('ERROR', 'Into GetNSCallFlag', '');
	nlapiLogExecution('ERROR', 'Status', Status);
	nlapiLogExecution('ERROR', 'New Status', NewStatus);
	var result=new Array();
	var retFlag='N';
	var OldMapLocation;
	var NewMapLocation;
	var OldLocation;
	var NewLocation;
	if(Status!=null && Status!="")
	{
		var OldIStatus = nlapiLoadRecord('customrecord_ebiznet_sku_status', Status);
		if(OldIStatus!= null && OldIStatus != "")
		{
			OldMapLocation=OldIStatus.getFieldValue('custrecord_item_status_location_map');
			OldLocation=OldIStatus.getFieldValue('custrecord_ebizsiteskus');
			OldIStatus=null;
		} 
	}
	if(NewStatus!=null && NewStatus!="")
	{
		var NewIStatus = nlapiLoadRecord('customrecord_ebiznet_sku_status', NewStatus);
		if(NewIStatus!= null && NewIStatus != "")
		{
			NewMapLocation=NewIStatus.getFieldValue('custrecord_item_status_location_map');
			NewLocation=NewIStatus.getFieldValue('custrecord_ebizsiteskus');
		} 
	}
	nlapiLogExecution('ERROR', 'OldLocation', OldLocation);
	nlapiLogExecution('ERROR', 'NewLocation', NewLocation);
	if(OldMapLocation != null && OldMapLocation !="" && NewMapLocation != null && NewMapLocation != "")
	{
		if(OldMapLocation==NewMapLocation)
			//if(OldLocation==NewLocation)
		{
			retFlag='N';
			result.push('N');
		}
		else
		{
			result.push('Y');
		}
		result.push(OldMapLocation);
		result.push(NewMapLocation);
		result.push(OldLocation);
		result.push(NewLocation);
	}
	nlapiLogExecution('ERROR', 'retFlag', retFlag);
	nlapiLogExecution('ERROR', 'OldMapLocation', OldMapLocation);
	nlapiLogExecution('ERROR', 'NewMapLocation', NewMapLocation);
	nlapiLogExecution('ERROR', 'Out of GetNSCallFlag', '');
	return result;
}


function GetMWFlag(WHLocation)
{
	nlapiLogExecution('ERROR', 'Into GetMWFlag : WHLocation', WHLocation);
	var retFlag='Y';
	var MWHSiteFlag;

	if(WHLocation!=null && WHLocation!="")
	{
		var filters = new Array();
		filters.push(new nlobjSearchFilter('internalid', null, 'is', WHLocation));
		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_ebizwhsite')); 

		var WHOld = nlapiSearchRecord('location', null, filters, columns);

		if(WHOld!= null && WHOld != "" && WHOld.length>0)
		{
			MWHSiteFlag=WHOld[0].getValue('custrecord_ebizwhsite');
			nlapiLogExecution('ERROR', 'MWHSiteFlag', MWHSiteFlag);
			WHOld=null;
		} 
	} 
	if(MWHSiteFlag != null && MWHSiteFlag !="" )
	{
		if(MWHSiteFlag=='F')
			retFlag='N';
		else
			retFlag='Y';

	}
	nlapiLogExecution('ERROR', 'retFlag', retFlag);
	nlapiLogExecution('ERROR', 'Out of GetMWFlag ', '');
	return retFlag;
}

function  InvokeNSInventoryTransfer(item,itemstatus,loc,toloc,qty,lot)
{
	try{
		nlapiLogExecution('ERROR', 'Into InvokeNSInventoryTransfer');		
		nlapiLogExecution('ERROR', 'item', item);		
		nlapiLogExecution('ERROR', 'itemstatus', itemstatus);
		nlapiLogExecution('ERROR', 'loc', loc);
		nlapiLogExecution('ERROR', 'toloc', toloc);
		nlapiLogExecution('ERROR', 'qty', qty);
		nlapiLogExecution('ERROR', 'lot', lot);

		var ItemType='';

		if(item!= null && item != "")
		{
			var columns = nlapiLookupField('item', item, [ 'recordType','custitem_ebizserialout' ]);
			ItemType = columns.recordType;
		}

		nlapiLogExecution('ERROR', 'ItemType', ItemType);
		var confirmLotToNS='Y';
		confirmLotToNS=GetConfirmLotToNS(loc);
		var vItemname;	

		var filters = new Array();          
		filters.push(new nlobjSearchFilter('internalid', null, 'is',item));
		// Changed On 30/4/12 by Suman
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		// End of Changes as On 30/4/12
		var columns = new Array();		
		columns[0] = new nlobjSearchColumn('itemid');
		var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
		if (itemdetails !=null) 
		{
			vItemname=itemdetails[0].getValue('itemid');
		}

		var invttransfer = nlapiCreateRecord('inventorytransfer');			

		var subs = nlapiGetContext().getFeature('subsidiaries');		
		if(subs==true)
		{
			var vSubsidiaryVal=getSubsidiaryNew(loc);
			if(vSubsidiaryVal != null && vSubsidiaryVal != '')
				invttransfer.setFieldValue('subsidiary', vSubsidiaryVal);
		}		

		invttransfer.setFieldValue('location', loc);//from Location
		invttransfer.setFieldValue('transferlocation', toloc);
		invttransfer.selectNewLineItem('inventory');
		invttransfer.setCurrentLineItemValue('inventory', 'item', item);		
		invttransfer.setCurrentLineItemValue('inventory', 'adjustqtyby', parseFloat(qty));

//		case no 20126110
		//if(lot!=null && lot!='')
		if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem") 
		{

			vItemname=vItemname.replace(/ /g,"-");

			if(confirmLotToNS=='N')
				lot=vItemname;	

			//For advanced Bin serial Lot management check
			var vAdvBinManagement=false;
			var ctx = nlapiGetContext();
			if(ctx != null && ctx != '')
			{
				if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
					vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
			}  
			nlapiLogExecution('ERROR', 'vAdvBinManagement', vAdvBinManagement);

			if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
			{
				var compSubRecord = invttransfer.createCurrentLineItemSubrecord('inventory','inventorydetail');
				nlapiLogExecution('ERROR', 'test1', 'test1');
				compSubRecord.selectNewLineItem('inventoryassignment');
				compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', qty);
				//compSubRecord.setCurrentLineItemValue('inventoryassignment', 'binnumber', vSubsidiaryVal);
				compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lot);

				compSubRecord.commitLineItem('inventoryassignment');
				compSubRecord.commit();
			}
			else
			{
				if(parseFloat(qty)<0)
					qty=parseFloat(qty)*(-1);
				nlapiLogExecution('ERROR', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");
				invttransfer.setCurrentLineItemValue('inventory', 'serialnumbers', lot + "(" + parseFloat(qty) + ")");
			}
		}
		else if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem") 
		{
			if(parseFloat(qty)<0)
				qty=parseFloat(qty)*(-1);
			nlapiLogExecution('ERROR', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");
			invttransfer.setCurrentLineItemValue('inventory', 'serialnumbers', lot);
		}

		invttransfer.commitLineItem('inventory'); 
		nlapiSubmitRecord(invttransfer, true, true);
		nlapiLogExecution('ERROR', 'Out of InvokeNSInventoryTransfer');
	}


	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in InvokeNSInventoryTransferNew ', exp);	

	}
}

function createInvtAdjtRecord(binloc,skuno,expqty,explp,accoutno,whsite,adjustqty,serialnumbers,NSAdjustid)
{
	var invAdjustCustRecord = nlapiCreateRecord('customrecord_ebiznet_invadj'); 
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_binloc', binloc);
	invAdjustCustRecord.setFieldValue('custrecord_ebizskuno', skuno);
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_expeqty', parseFloat(expqty).toFixed(5));
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_invjlpno', explp);

	if(accoutno!=null && accoutno!='')
	{
		invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjtype', accoutno);
	}

	invAdjustCustRecord.setFieldValue('custrecord_ebiz_siteid', whsite);
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjustqty', parseFloat(adjustqty).toFixed(5));
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_tasktype', 7);
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_recorddate', DateStamp());
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_rectime', TimeStamp());
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_updateduserno', nlapiGetContext().getUser());
	if(NSAdjustid != null && NSAdjustid != '')
		invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjustno', NSAdjustid);//NS asdjustment id
	if(serialnumbers != null && serialnumbers != '')
	{
		invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjserialnumbers', serialnumbers);
	}

	var id = nlapiSubmitRecord(invAdjustCustRecord, false, true);
}

function getSKUCubeAndWeight(skuNo, uom){
	nlapiLogExecution('ERROR', 'getSKUCubeAndWeight', 'Start');
	var timestamp1 = new Date();
	var cube = 0;
	var BaseUOMQty = 0;    
	var dimArray = new Array();
	if (uom == "")
		uom = 1;

	nlapiLogExecution('DEBUG', 'getSKUCubeAndWeight:SKU info', skuNo);
	nlapiLogExecution('DEBUG', 'getSKUCubeAndWeight:UOM', uom);

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', skuNo);
	//case # 20123248 start
	filters[1] = new nlobjSearchFilter('custrecord_ebizuomskudim', null, 'is', uom);
	//case # 20123248 end
	filters[2] = new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
	columns[1] = new nlobjSearchColumn('custrecord_ebizqty');

	var t1 = new Date();
	var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);
	var t2 = new Date();
	nlapiLogExecution('ERROR', 'nlapiSearchRecord:getSKUCubeAndWeight:skudims',
			getElapsedTimeDuration(t1, t2));

	if(skuDimsSearchResults != null){
		for (var i = 0; i < skuDimsSearchResults.length; i++) {
			var skuDim = skuDimsSearchResults[i];
			cube = skuDim.getValue('custrecord_ebizcube');
			BaseUOMQty = skuDim.getValue('custrecord_ebizqty');
		}
	}

	dimArray[0] = cube;
	dimArray[1] = BaseUOMQty;

	//dimArray["BaseUOMItemCube"] = cube;
	//dimArray["BaseUOMQty"] = BaseUOMQty;

	var timestamp2 = new Date();
	nlapiLogExecution('DEBUG', 'getSKUCubeAndWeight Duration', getElapsedTimeDuration(timestamp1, timestamp2));

	nlapiLogExecution('ERROR', 'getSKUCubeAndWeight:Cube', cube);
	nlapiLogExecution('ERROR', 'getSKUCubeAndWeight:qty', BaseUOMQty);
	nlapiLogExecution('ERROR', 'getSKUCubeAndWeight:length', dimArray.length);
	nlapiLogExecution('ERROR', 'getSKUCubeAndWeight:Cube1', dimArray[0]);
	nlapiLogExecution('ERROR', 'getSKUCubeAndWeight:qty1', dimArray[1]);
	return dimArray;
}

function GeteLocCube(LocID){
	var remainingCube = 0;
	try {
		var fields = ['custrecord_remainingcube'];
		var columns= nlapiLookupField('customrecord_ebiznet_location',LocID,fields);
		remainingCube = columns.custrecord_remainingcube;
	}catch(exception) {
		nlapiLogExecution('DEBUG', 'GeteLocCube:Lookup for remainingCube failed', exception);
	}
	nlapiLogExecution('DEBUG', 'GeteLocCube:Remaining Cube for Location', remainingCube);
	return remainingCube;
}

function getLotBatchId(lotbatchtext,skuid)
{
	var batchid='';
	var filters = new Array();          
	filters.push(new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is',lotbatchtext));
	if(skuid!=null&&skuid!="")
		filters.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof',skuid));

	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	var batchdetails = new nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filters, columns);
	if (batchdetails !=null && batchdetails !='' && batchdetails.length>0) 
	{
		batchid=batchdetails[0].getValue('internalid');
	}
	return batchid;
}


/**
 * Passing item,item status, qty and location to NS
 * 
 * @param item
 * @param itemstatus
 * @param loc
 * @param qty
 * @param notes
 */
function  InvokeNSInventoryAdjustmentNew(item,itemstatus,loc,qty,notes,tasktype,adjusttype,lot,AccNo)
{
	try{
		var vCost;
		var vAvgCost;
		var vItemname;
		var NSid='';
		var confirmLotToNS='N';
		nlapiLogExecution('ERROR', 'item::', item);
		nlapiLogExecution('ERROR', 'itemstatus::', itemstatus);
		nlapiLogExecution('ERROR', 'Location info::', loc);
		nlapiLogExecution('ERROR', 'Task Type::', tasktype);
		nlapiLogExecution('ERROR', 'Adjustment Type::', adjusttype);
		nlapiLogExecution('ERROR', 'qty::', qty);
		nlapiLogExecution('ERROR', 'AccNo::', AccNo);
		nlapiLogExecution('ERROR', 'lot::', lot);
		//alert("AccNo :: "+ AccNo);
		confirmLotToNS=GetConfirmLotToNS(loc);

		var filters = new Array();          
		filters.push(new nlobjSearchFilter('internalid', null, 'is',item));

		// Changed On 30/4/12 by Suman
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		// End of Changes as On 30/4/12
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('cost');
		columns[1] = new nlobjSearchColumn('averagecost');
		columns[2] = new nlobjSearchColumn('itemid');

		var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
		//alert("itemdetails " + itemdetails);
		if (itemdetails !=null) 
		{
			vItemname=itemdetails[0].getValue('itemid');
			vCost = itemdetails[0].getValue('cost');
			nlapiLogExecution('ERROR', 'vCost', vCost);
			vAvgCost = itemdetails[0].getValue('averagecost');
			nlapiLogExecution('ERROR', 'Average Cost', vAvgCost);	         
		}		

		var vItemMapLocation =  getItemStatusMapLoc(itemstatus);	
		nlapiLogExecution('ERROR', 'vItemMapLocation', vItemMapLocation);
		if(AccNo==null || AccNo=="")
		{		
			AccNo = getAccountNo(loc,vItemMapLocation);
			nlapiLogExecution('ERROR', 'Fetcht Account from Inventory AccountNo', vAccountNo);
		}
		//alert("NS Qty "+ parseFloat(qty));
		//	if(lot==null || lot=='' || lot=='null')
		//	{
		var outAdj = nlapiCreateRecord('inventoryadjustment');			

		outAdj.setFieldValue('account', AccNo);
		outAdj.setFieldValue('memo', notes);
		outAdj.setFieldValue('adjlocation', vItemMapLocation);
		outAdj.selectNewLineItem('inventory');
		outAdj.setCurrentLineItemValue('inventory', 'item', item);
		outAdj.setCurrentLineItemValue('inventory', 'location', vItemMapLocation);	
		outAdj.setCurrentLineItemValue('inventory', 'adjustqtyby', parseFloat(qty));
		var subs = nlapiGetContext().getFeature('subsidiaries');
		nlapiLogExecution('Debug', 'subs', subs);
		if(subs==true)
		{
			var vSubsidiaryVal=getSubsidiaryNew(loc);
			nlapiLogExecution('Debug','vSubsidiaryVal',vSubsidiaryVal);
			if(vSubsidiaryVal != null && vSubsidiaryVal != '')
				outAdj.setFieldValue('subsidiary', vSubsidiaryVal);
		}
		//	}


		//alert('Hi1');
		//alert("vAvgCost "+ vAvgCost);
		if(vAvgCost != null &&  vAvgCost != "" && outAdj!=null && outAdj!='' && outAdj!='null')	
		{
			nlapiLogExecution('ERROR', 'into if cond vAvgCost', vAvgCost);
			outAdj.setCurrentLineItemValue('inventory', 'unitcost', vAvgCost);
		}
		else
		{
			nlapiLogExecution('ERROR', 'into else cond.unit cost', vCost);
			if(outAdj!=null && outAdj!='' && outAdj!='null')	
				outAdj.setCurrentLineItemValue('inventory', 'unitcost',  vCost);
		}

		var vAdvBinManagement=false;

		var ctx = nlapiGetContext();
		if(ctx != null && ctx != '')
		{
			if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
				vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
		}  
		nlapiLogExecution('ERROR', 'vAdvBinManagement', vAdvBinManagement);

		var vItemType="";


		if(lot!=null && lot!='')
		{
			var fields = ['recordType','custitem_ebizserialin'];
			var columns = nlapiLookupField('item', item, fields);
			vItemType = columns.recordType;
			nlapiLogExecution('ERROR','vItemType',vItemType);

			var serialInflg="F";		
			serialInflg = columns.custitem_ebizserialin;	

			if(vAdvBinManagement)
			{
				if (vItemType == "lotnumberedinventoryitem" || vItemType == "lotnumberedassemblyitem")
				{
					var compSubRecord = outAdj.createCurrentLineItemSubrecord('inventory','inventorydetail');

					if(confirmLotToNS=='N')
						lot=vItemname;			

					nlapiLogExecution('ERROR', 'lot', lot);

					compSubRecord.selectNewLineItem('inventoryassignment');
					compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', qty);
					compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lot);

					compSubRecord.commitLineItem('inventoryassignment');

					compSubRecord.commit();

				}
			}
			if (vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem")
			{
				vItemname=vItemname.replace(/ /g,"-");

				if(parseFloat(qty)<0)
					qty=parseFloat(qty)*(-1);

				if(confirmLotToNS=='N')
					lot=vItemname;			

				nlapiLogExecution('ERROR', 'lot', lot);

				nlapiLogExecution('ERROR', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");
				outAdj.setCurrentLineItemValue('inventory', 'serialnumbers', lot + "(" + parseFloat(qty) + ")");
			}
			else if (vItemType == "serializedinventoryitem" || vItemType == "serializedassemblyitem" || serialInflg == "T")
			{
				//case # 20127185ÃƒÆ’Ã‚Â¯Ãƒâ€šÃ‚Â¿Ãƒâ€šÃ‚Â½,split based on ,.
				var cnt=lot.split(',');
				nlapiLogExecution('ERROR', 'cnt', cnt);

				/*	for(var n=0;n<cnt.length;n++)
				{*/

				//var lotnumbers=cnt[n];
				var lotnumbers = lot.split(',');
				nlapiLogExecution('ERROR', 'lotnumbers', lotnumbers);
				if(lotnumbers!=null && lotnumbers!='' && lotnumbers!='null')
				{

					var vItemMapLocation =  getItemStatusMapLoc(itemstatus);	
					nlapiLogExecution('ERROR', 'vItemMapLocation', vItemMapLocation);
					nlapiLogExecution('ERROR', 'vItemMapLocation', vItemMapLocation);
					if(AccNo==null || AccNo=="")
					{		
						AccNo = getAccountNo(loc,vItemMapLocation);
						nlapiLogExecution('ERROR', 'Fetcht Account from Inventory AccountNo', vAccountNo);
					}
					var outAdj = nlapiCreateRecord('inventoryadjustment');	
					outAdj.setFieldValue('account', AccNo);
					outAdj.setFieldValue('memo', notes);
					outAdj.setFieldValue('adjlocation', vItemMapLocation);
					outAdj.selectNewLineItem('inventory');
					outAdj.setCurrentLineItemValue('inventory', 'item', item);
					outAdj.setCurrentLineItemValue('inventory', 'location', vItemMapLocation);	
					outAdj.setCurrentLineItemValue('inventory', 'adjustqtyby', parseFloat(qty));
					//Case # 20126291,20126107,20126190  start
					if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
					{

						var	compSubRecord = outAdj.createCurrentLineItemSubrecord('inventory','inventorydetail');

						//var tempQtyArray=lotnumbers.split(',');
						var tempQtyArray=lotnumbers;

						for (var x2 = 0; x2 < tempQtyArray.length; x2++)
						{
							var tempQty;

							if(parseFloat(qty)<0)
							{
								tempQty=-1;
							}
							else
							{
								tempQty=1;
							}

							/*if(x==0)
								var compSubRecord = outAdj.createCurrentLineItemSubrecord('inventory','inventorydetail');*/
							compSubRecord.selectNewLineItem('inventoryassignment');
							//case # 20126232ÃƒÆ’Ã‚Â¯Ãƒâ€šÃ‚Â¿Ãƒâ€šÃ‚Â½ start
							compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', tempQty);
							//case # 20126232ÃƒÆ’Ã‚Â¯Ãƒâ€šÃ‚Â¿Ãƒâ€šÃ‚Â½ end
							compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', tempQtyArray[x2]);

							compSubRecord.commitLineItem('inventoryassignment');

						}
						compSubRecord.commit();
					}
					//Case # 20126291,20126107,20126190  End

					var subs = nlapiGetContext().getFeature('subsidiaries');
					nlapiLogExecution('Debug', 'subs', subs);
					if(subs==true)
					{
						var vSubsidiaryVal=getSubsidiaryNew(loc);
						nlapiLogExecution('Debug','vSubsidiaryVal',vSubsidiaryVal);
						if(vSubsidiaryVal != null && vSubsidiaryVal != '')
							outAdj.setFieldValue('subsidiary', vSubsidiaryVal);
					}


					if(vAvgCost != null &&  vAvgCost != "" && outAdj!=null && outAdj!='' && outAdj!='null')	
					{
						nlapiLogExecution('ERROR', 'into if cond vAvgCost', vAvgCost);
						outAdj.setCurrentLineItemValue('inventory', 'unitcost', vAvgCost);
					}
					else
					{
						nlapiLogExecution('ERROR', 'into else cond.unit cost', vCost);
						if(outAdj!=null && outAdj!='' && outAdj!='null')	
							outAdj.setCurrentLineItemValue('inventory', 'unitcost',  vCost);
					}

					nlapiLogExecution('ERROR', 'lot with serial no', lotnumbers);

					nlapiLogExecution('ERROR', 'LotNowithQty', lotnumbers);
					outAdj.setCurrentLineItemValue('inventory', 'serialnumbers', lotnumbers);


					var subs = nlapiGetContext().getFeature('subsidiaries');
					nlapiLogExecution('ERROR', 'subs', subs);
					if(subs==true)
					{
						var vSubsidiaryVal=getSubsidiaryNew(loc);
						if(vSubsidiaryVal != null && vSubsidiaryVal != '')
							outAdj.setFieldValue('subsidiary', vSubsidiaryVal);
						nlapiLogExecution('ERROR', 'vSubsidiaryVal', vSubsidiaryVal);
					}
					outAdj.commitLineItem('inventory');
					NSid=nlapiSubmitRecord(outAdj, true, true);
					//alert("Success NS");
					nlapiLogExecution('ERROR', 'NSid', NSid);
				}
				//}	
			}
			//}
		}
		//alert('Hi3');
		if (vItemType != "serializedinventoryitem" && vItemType != "serializedassemblyitem" && serialInflg != "T")
		{
			var vItemMapLocation =  getItemStatusMapLoc(itemstatus);	
			nlapiLogExecution('ERROR', 'vItemMapLocation', vItemMapLocation);
			if(AccNo==null || AccNo=="")
			{		
				AccNo = getAccountNo(loc,vItemMapLocation);
				nlapiLogExecution('ERROR', 'Fetcht Account from Inventory AccountNo', vAccountNo);
			}
			var subs = nlapiGetContext().getFeature('subsidiaries');
			nlapiLogExecution('ERROR', 'subs', subs);
			if(subs==true)
			{
				var vSubsidiaryVal=getSubsidiaryNew(loc);
				if(vSubsidiaryVal != null && vSubsidiaryVal != '')
					outAdj.setFieldValue('subsidiary', vSubsidiaryVal);
				nlapiLogExecution('ERROR', 'vSubsidiaryVal', vSubsidiaryVal);
			}
			outAdj.commitLineItem('inventory');
			NSid=nlapiSubmitRecord(outAdj, true, true);
			//alert("Success NS");
			nlapiLogExecution('ERROR', 'NSid', NSid);

			nlapiLogExecution('ERROR', 'type argument', 'type is create');
		}
	}


	catch(e) {
		if (e instanceof nlobjError) 
		{
			// nlapiLogExecution('ERROR', 'system error', e.getCode() + '\n' + e.getDetails());
			nlapiLogExecution('ERROR','system error' + '\n' + 'Error: ' + e.getCode()+ '\n' + e.getDetails() );
		}

		else 
		{
			//nlapiLogExecution('ERROR', 'unexpected error', e.toString());
			nlapiLogExecution('ERROR','unexpected error' + '\n' + 'Error: ' + e.toString() );
		}

		nlapiLogExecution('ERROR', 'Exception in InvokeNSInventoryAdjustmentNew ', e);
		//alert("Error" + exp.toString());

	}
	return NSid;
}

function getAccountNo(location,itemstatusmaploc)
{
	nlapiLogExecution('ERROR', 'location:', location);
	nlapiLogExecution('ERROR', 'itemstatusmaploc:', itemstatusmaploc);
	var accountno;	
	var filtersAccNo = new Array(); 
	filtersAccNo.push(new nlobjSearchFilter('custrecord_location', null, 'is', location));
	filtersAccNo.push(new nlobjSearchFilter('custrecord_inventorynslocation', null, 'is', itemstatusmaploc));        						
	var colsAcc = new Array();
	colsAcc[0]=new nlobjSearchColumn('custrecord_accountno');                
	var Accsearchresults = nlapiSearchRecord('customrecord_inventoryaccountno', null, filtersAccNo, colsAcc);
	nlapiLogExecution('ERROR', 'Accsearchresults', Accsearchresults);
	if(Accsearchresults!=null)
		accountno = Accsearchresults[0].getValue('custrecord_accountno');	
	return accountno;
}

function getStockAdjustmentAccountNoNew(loc,tasktype,adjusttype)
{	
	var StAdjustmentAccountNo=new Array();
	var filterStAccNo = new Array();
	var colsStAccNo = new Array();
	//alert("loc : "+loc);
	if(loc!=null)
		filterStAccNo.push(new nlobjSearchFilter('custrecord_ebiz_adjtype_location',null,'anyof',[loc]));	

	if(tasktype!=null)
		filterStAccNo.push(new nlobjSearchFilter('custrecord_adjusttasktype',null,'anyof',[tasktype]));


	if(adjusttype!=null && adjusttype!="")
		filterStAccNo.push(new nlobjSearchFilter('internalid',null,'is',adjusttype));

	filterStAccNo.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	colsStAccNo[0] = new nlobjSearchColumn('custrecord_ebiz_mapto_ns_glaccount');
	colsStAccNo[1] = new nlobjSearchColumn('custrecord_ebiz_to_ns_glaccount');
	colsStAccNo[2] = new nlobjSearchColumn('internalid');

	var StockAdjustAccResults = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, filterStAccNo, colsStAccNo);
	//alert("StockAdjustAccResults " + StockAdjustAccResults);
	//alert("StockAdjustAccResults " + StockAdjustAccResults.length);
	if(StockAdjustAccResults !=null && StockAdjustAccResults != "" && StockAdjustAccResults.length>0)
	{
		//alert("1 " + StockAdjustAccResults[0].getValue('custrecord_ebiz_mapto_ns_glaccount'));
		StAdjustmentAccountNo.push(StockAdjustAccResults[0].getValue('custrecord_ebiz_mapto_ns_glaccount'));
		StAdjustmentAccountNo.push(StockAdjustAccResults[0].getValue('internalid'));

		//alert("2 " + StockAdjustAccResults[0].getValue('custrecord_ebiz_to_ns_glaccount'));
		if(StockAdjustAccResults[0].getValue('custrecord_ebiz_to_ns_glaccount') != null && StockAdjustAccResults[0].getValue('custrecord_ebiz_to_ns_glaccount') != "")
			StAdjustmentAccountNo.push(StockAdjustAccResults[0].getValue('custrecord_ebiz_to_ns_glaccount'));


	}	

	return StAdjustmentAccountNo;	
}

/**
 * Updates the bin location volume for the specified location
 * @param locnId
 * @param remainingCube
 */
function UpdateLocCubeNew(locnId, remainingCube){
	//alert("Val1 "+ locnId);
	//alert("Val2 "+ remainingCube);
	var fields = new Array();
	var values = new Array();
	fields[0] = 'custrecord_remainingcube';
	values[0] = parseFloat(remainingCube).toFixed(5);
	//alert("parse "+ parseFloat(remainingCube));
	//nlapiLogExecution('ERROR','Location Internal Id', locnId);

	//nlapiLogExecution('ERROR','Location Internal IdremainingCube', remainingCube);

	//alert("into");
	//var t1 = new Date();
	nlapiSubmitField('customrecord_ebiznet_location', locnId, fields, values);
	//alert("Success");
	//var t2 = new Date();
	//nlapiLogExecution('ERROR', 'nlapiSubmitField:UpdateLocCube:location', getElapsedTimeDuration(t1, t2));
}

var searchResultArray=new Array();
function getOrdersForCycleCount(request,maxno,form){

	var localVarArray;
	//this is to maintain the querystring parameters while refreshing the paging drop down
	if (request.getMethod() != 'POST') {
		var queryparams=request.getParameter('custpage_qeryparams');
		nlapiLogExecution('Error', 'queryparams', queryparams);
		// queryparams=queryparams.
		var tempArray=new Array();
		tempArray.push(queryparams.split(','));
		localVarArray=tempArray;
		nlapiLogExecution('Error', 'tempArray', tempArray.length);
	}
	else
	{
		localVarArray = validateRequestParams(request,form);
		nlapiLogExecution('Error', 'localVarArrayinelse', localVarArray);
	}

	// Get all the search filters for wave generation
	var filters = new Array();   
	filters = specifyInvtFilters(localVarArray,maxno);
	nlapiLogExecution('Error', 'afterspecifyfilters', localVarArray);
	// Adding search columns
	var columns = new Array();
	columns=addColumnsForSearch();
	nlapiLogExecution('Error', 'afterspecifycolumns', localVarArray);
	var orderList = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, columns);
	if( orderList!=null && orderList.length>=1000)
	{ 
		nlapiLogExecution('Error', 'orderList.length', orderList.length);
		searchResultArray.push(orderList); 
		var maxno=orderList[orderList.length-1].getId();
		getOrdersForCycleCount(request,maxno,form);	
	}
	else
	{
		if(orderList!=null && orderList!='' && orderList.length>0)
		{
			nlapiLogExecution('Error', 'orderList.length', orderList.length);
			searchResultArray.push(orderList);
		}
	}
	return searchResultArray;

}


function setPagingForSublist(orderList,form)
{
	if(orderList != null && orderList.length > 0){
		nlapiLogExecution('Error', 'orderList ', orderList.length);
		var orderListArray=new Array();		
		for(k=0;k<orderList.length;k++)
		{
			//nlapiLogExecution('ERROR', 'orderList[k]', orderList[k]); 
			var ordsearchresult = orderList[k];

			if(ordsearchresult!=null)
			{
				//nlapiLogExecution('ERROR', 'ordsearchresult.length ', ordsearchresult.length); 
				for(var j=0;j<ordsearchresult.length;j++)
				{
					orderListArray[orderListArray.length]=ordsearchresult[j];				
				}
			}
		}
		//nlapiLogExecution('ERROR', 'orderListArray.length ', orderListArray.length); 
		var test='';

		if(orderListArray.length>0)
		{

			if(orderListArray.length>500)
			{
				var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('hidden');
				pagesize.setDisplaySize(10,10);
				pagesize.setLayoutType('outsidebelow', 'startrow');
				var select= form.addField('custpage_selectpage','select', 'Select Records');	
				select.setLayoutType('outsidebelow', 'startrow');			
				select.setDisplaySize(200,30);
				if (request.getMethod() == 'GET'){
					pagesize.setDefaultValue("500");
					pagesizevalue=500;
				}
				else
				{
					if(request.getParameter('custpage_pagesize')!=null)
					{
						pagesizevalue= request.getParameter('custpage_pagesize');
					}
					else
					{
						pagesizevalue= 500;
						pagesize.setDefaultValue("500");
					}
				}

				//this is to add the pageno's to the dropdown.
				var len=orderListArray.length/parseFloat(pagesizevalue);
				for(var k=1;k<=Math.ceil(len);k++)
				{

					var from;var to;

					to=parseFloat(k)*parseFloat(pagesizevalue);
					from=(parseFloat(to)-parseFloat(pagesizevalue))+1;

					if(parseFloat(to)>orderListArray.length)
					{
						to=orderListArray.length;
						test=from.toString()+","+to.toString(); 
					}

					var temp=from.toString()+" to "+to.toString();
					var tempto=from.toString()+","+to.toString();
					select.addSelectOption(tempto,temp);

				} 

				if (request.getMethod() == 'POST'){

					if(request.getParameter('custpage_selectpage')!=null ){

						select.setDefaultValue(request.getParameter('custpage_selectpage'));	

					}
					if(request.getParameter('custpage_pagesize')!=null ){

						pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	

					}
				}
			}
			else
			{
				pagesizevalue=parseFloat(orderListArray.length);
			}
			var minval=0;var maxval=parseFloat(pagesizevalue);
			if(parseFloat(pagesizevalue)>orderListArray.length)
			{
				maxval=orderListArray.length;
			}
			var selectno=request.getParameter('custpage_selectpage');
			if(selectno!=null )
			{
				var selectedPage= request.getParameter('custpage_selectpage');
				var selectedPageArray=selectedPage.split(',');			
				var diff=parseFloat(selectedPageArray[1])-(parseFloat(selectedPageArray[0])-1);
				//nlapiLogExecution('ERROR', 'diff',diff);

				var pagevalue=request.getParameter('custpage_pagesize');
				//nlapiLogExecution('ERROR', 'pagevalue',pagevalue);
				if(pagevalue!=null)
				{

					if(parseFloat(diff)==parseFloat(pagevalue)|| test==selectno)
					{

						var selectedPageArray=selectno.split(',');	
						//nlapiLogExecution('ERROR', 'selectedPageArray.length ', selectedPageArray.length);  
						minval=parseFloat(selectedPageArray[0])-1;
						//nlapiLogExecution('ERROR', 'selectedPageArray[0] ', selectedPageArray[0]);  
						maxval=parseFloat(selectedPageArray[1]);
						//nlapiLogExecution('ERROR', 'selectedPageArray[1] ', selectedPageArray[1]);  
					}
				}
			}


			var c=0;
			var minvalue;
			minvalue=minval;

			for(var j = minvalue; j < maxval; j++){		

				var currentOrder = orderListArray[j];
				setLineItemValuesFromSearch(form, currentOrder, c);
//				addFulfilmentOrderToSublist(form, currentOrder, c);
				c=c+1;
			}
		}
	}
}


function addFieldsToForm(form)
{
	/*
	 * Defining SubList lines
	 */
	var sublist = form.addSubList("custpage_items", "list", "Resolve");
	sublist.addMarkAllButtons();
	sublist.addField("custpage_rec", "checkbox", "Record").setDefaultValue('T');

	sublist.addField("custpage_locationtext", "text", "Location", "customrecord_ebiznet_location").setDisplayType('inline');
	sublist.addField("custpage_location", "text", "Location", "customrecord_ebiznet_location").setDisplayType('hidden');
	sublist.addField("custpage_expsku", "text", "Expected Item", "item").setDisplayType('hidden');
	sublist.addField("custpage_expskutext", "text", "Expected Item", "item").setDisplayType('inline');
	//var actsku = sublist.addField("custpage_actsku", "select", "Actual SKU", "item").setDisplayType('inline');
	sublist.addField("custpage_actsku", "text", "Actual Item", "item").setDisplayType('hidden');
	//sublist.addField("custpage_actskutext", "text", "Actual Item", "item").setDisplayType('inline');

	sublist.addField("custpage_actskutextlink", "textarea", "Actual Item", "item");

	sublist.addField("custpage_expqty", "text", "Expected Qty");
	var actqty = sublist.addField("custpage_actqty", "text", "Actual Qty");
	sublist.addField("custpage_qtyvariance", "text", "Qty Variance");
	sublist.addField("custpage_dollarvariance", "text", "$ Variance");
	sublist.addField("custpage_explp", "text", "Expected LP");
	var actlp = sublist.addField("custpage_actlp", "text", "Actual LP"); 
	sublist.addField("custpage_expbatch", "text", "Expected LOT#").setDisplayType('inline');
	sublist.addField("custpage_actbatch", "text", "Actual LOT#").setDisplayType('inline');
	sublist.addField("custpage_expskustatus", "text", "Expected Item Status");
	sublist.addField("custpage_expskustatusvalue", "text", "Expected SKU Status value").setDisplayType('hidden');
	sublist.addField("custpage_actskustatus", "text", "Actual Item Status").setDisplayType('inline');
	sublist.addField("custpage_actskustatusvalue", "text", "Actual SKU Status value").setDisplayType('hidden');
	sublist.addField("custpage_currentinvt", "text", "Current Inventory");
	sublist.addField("custpage_qtytobeadjust", "text", "Qty to be Adjusted").setDisplayType('entry');	

	sublist.addField("custpage_id", "text", "ID").setDisplayType('hidden');
	sublist.addField("custpage_planno", "text", "Plan#").setDisplayType('hidden');
	sublist.addField("custpage_opentaskid", "text", "Open Task ID").setDisplayType('hidden');
	sublist.addField("custpage_invtid", "text", "Invt ID").setDisplayType('hidden');
	sublist.addField("custpage_name", "text", "Name").setDisplayType('hidden');
	sublist.addField("custpage_date", "text", "date").setDisplayType('hidden').setDefaultValue(DateStamp());
	sublist.addField("custpage_time", "text", "time").setDisplayType('hidden').setDefaultValue(TimeStamp());
	sublist.addField("custpage_site", "text", "Site").setDisplayType('hidden');	
	sublist.addField("custpage_expserial", "textarea", "Expected Serial#").setDisplayType('hidden');	
	sublist.addField("custpage_actserial", "textarea", "Actual Serial#").setDisplayType('hidden');	
}


function addColumnsForSearch()
{	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_cycact_beg_loc');
	columns[1] = new nlobjSearchColumn('custrecord_cyclecount_exp_ebiz_sku_no');
	columns[2] = new nlobjSearchColumn('custrecord_cycleact_sku');
	columns[3] = new nlobjSearchColumn('custrecord_cycleexp_qty');
	columns[4] = new nlobjSearchColumn('custrecord_cycle_act_qty');
	columns[5] = new nlobjSearchColumn('custrecord_cyclelpno');
	columns[6] = new nlobjSearchColumn('custrecord_cycact_lpno');
	columns[7] = new nlobjSearchColumn('custrecord_cycle_count_plan');
	columns[8] = new nlobjSearchColumn('custrecord_opentaskid');
	columns[9] = new nlobjSearchColumn('custrecord_invtid');                
	columns[10] = new nlobjSearchColumn('custrecord_expcyclesku_status');
	columns[11] = new nlobjSearchColumn('custrecord_cyclesku_status');
	columns[12] = new nlobjSearchColumn('custrecord_cyclesite_id');
	columns[13] = new nlobjSearchColumn('custrecord_cyclesku'); 
	columns[14] = new nlobjSearchColumn('custrecord_cyclecount_act_ebiz_sku_no');
	columns[15] = new nlobjSearchColumn('custrecord_expcycleabatch_no');
	columns[16] = new nlobjSearchColumn('custrecord_cycleabatch_no');
	columns[17] = new nlobjSearchColumn('custrecord_cycle_serialno');
	columns[18] = new nlobjSearchColumn('custrecord_cycle_expserialno');
	columns[19] = new nlobjSearchColumn('averagecost','custrecord_cycleact_sku');
	columns[20] = new nlobjSearchColumn('internalid').setSort();
	return columns;


}

function specifyInvtFilters(localVarArray,maxno)
{
	nlapiLogExecution('ERROR','localVarArray[0]',localVarArray[0]);
	nlapiLogExecution('ERROR','maxno',maxno);
	// define search filters
	var filters = new Array();

	if(localVarArray[0][0] != "" && localVarArray[0][0] != null)
		filters.push(new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto',localVarArray[0][0]));

	if(localVarArray[0][1] != "" && localVarArray[0][1] != null)
		filters.push(new nlobjSearchFilter('custrecord_cyclesite_id', null, 'anyof',localVarArray[0][1]));
	if(localVarArray[0][2] != "" && localVarArray[0][2] != null)
		filters.push(new nlobjSearchFilter('custrecord_cyclesku', null, 'anyof',localVarArray[0][2]));
	if(localVarArray[0][3] != "" && localVarArray[0][3] != null)
		filters.push(new nlobjSearchFilter('custrecord_cycact_beg_loc', null, 'anyof',localVarArray[0][3]));

	filters.push(new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [31]));//Cycle Count Record
	var vRoleLocation=getRoledBasedLocation();
	if((localVarArray[0][1]==null || localVarArray[0][1]=='') && (vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0))
	{
		filters.push(new nlobjSearchFilter('custrecord_cyclesite_id', null, 'is', vRoleLocation));
	}
	if(maxno != "" && maxno != null)
		filters.push(new nlobjSearchFilter('id', null, 'greaterthan',parseInt(maxno)));
	return filters;


	// define search filters
//	var filters = new Array();

//	var filters = new Array();

//	if(vfilterarr[0] != "" && vfilterarr[0] != null)
	//	filters.push(new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto',vfilterarr[0]));

//	if(vfilterarr[1] != "" && vfilterarr[1] != null)
	//	filters.push(new nlobjSearchFilter('custrecord_cyclesite_id', null, 'anyof',vfilterarr[1]));
//	if(vfilterarr[2] != "" && vfilterarr[2] != null)
	//	filters.push(new nlobjSearchFilter('custrecord_cyclesku', null, 'anyof',vfilterarr[2]));
//	if(vfilterarr[3] != "" && vfilterarr[3] != null)
	//	filters.push(new nlobjSearchFilter('custrecord_cycact_beg_loc', null, 'anyof',vfilterarr[3]));

//	filters.push(new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [31]));//Cycle Count Record
//	var vRoleLocation=getRoledBasedLocation();
//	if((vfilterarr[1]==null || vfilterarr[1]=='') && (vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0))
//	{
//	filters.push(new nlobjSearchFilter('custrecord_cyclesite_id', null, 'is', vRoleLocation));
//	}
//	return filters;







}

function validateRequestParams(request,form)
{
	var CyclecountPlan = "";
	var CycwhLoc = "";
	var CycItem = "";
	var CycBinloc = "";
	var localVarArray = new Array();
	if (request.getParameter('custpage_cyclecountplan') != null && request.getParameter('custpage_cyclecountplan') != "") {
		CyclecountPlan = request.getParameter('custpage_cyclecountplan');
	}

	if (request.getParameter('custpage_cyclecountwhlocation') != null && request.getParameter('custpage_cyclecountwhlocation') != "") {
		CycwhLoc = request.getParameter('custpage_cyclecountwhlocation');
	}

	if (request.getParameter('custpage_cyclecountitem') != null && request.getParameter('custpage_cyclecountitem') != "") {

		CycItem = request.getParameter('custpage_cyclecountitem');
	}

	if (request.getParameter('custpage_cyclecountbinloc') != null && request.getParameter('custpage_cyclecountbinloc') != "") {
		CycBinloc = request.getParameter('custpage_cyclecountbinloc');
	}

	if (request.getParameter('custpage_qeryparams')==null )
	{
		var hiddenQueryParams=form.getField('custpage_qeryparams');
		nlapiLogExecution('ERROR','localVarArray',localVarArray.toString());
		hiddenQueryParams.setDefaultValue(localVarArray.toString());
	}

	nlapiLogExecution('ERROR', 'CycwhLoc', CycwhLoc);
	nlapiLogExecution('ERROR', 'CycItem', CycItem);
	nlapiLogExecution('ERROR', 'CycBinloc', CycBinloc);

	var currentRow = [CyclecountPlan,CycwhLoc,CycItem,CycBinloc];

	localVarArray.push(currentRow);
	return localVarArray;

}


/*
 * Set line item level values based on search results
 */
function setLineItemValuesFromSearch(form, searchResults,i){

	nlapiLogExecution('ERROR', 'Into setLineItemValuesFromSearch', searchresult);

	var loc,locText,expsku,expskutext,actskutext,actsku,expqty,actqty,explp,actlp,expskustatus,actskustatus;
	var actskustatusvalue,planno,name,opentaskid,intid,siteis,actlot,explot,expskustatusvalue,expserialno,actserialno;
	var dollarvariance;

	/*
	 *  Searching records from custom record and dynamically adding to
	 * the sublist lines
	 */
	var searchresult = searchResults;
	nlapiLogExecution('ERROR', 'searchresults.length', searchresult.length);

	loc = searchresult.getValue('custrecord_cycact_beg_loc');
	locText = searchresult.getText('custrecord_cycact_beg_loc');
	expsku = searchresult.getValue('custrecord_cyclesku');
	expskutext = searchresult.getText('custrecord_cyclesku');
	//actsku = searchresult.getValue('custrecord_cycleact_sku');
	actskutext = searchresult.getText('custrecord_cycleact_sku');
	actsku = searchresult.getValue('custrecord_cycleact_sku');
	expqty = searchresult.getValue('custrecord_cycleexp_qty');
	actqty = searchresult.getValue('custrecord_cycle_act_qty');
	explp = searchresult.getValue('custrecord_cyclelpno');
	actlp = searchresult.getValue('custrecord_cycact_lpno');

	expserialno = searchresult.getValue('custrecord_cycle_expserialno');
	actserialno = searchresult.getValue('custrecord_cycle_serialno');

	nlapiLogExecution('ERROR', 'expserialno in show all', expserialno);		
	nlapiLogExecution('ERROR', 'actserialno in show all', actserialno);		
	nlapiLogExecution('ERROR', 'expsku', expsku);		
	nlapiLogExecution('ERROR', 'actsku', actsku);		

	expskustatus = searchresult.getText('custrecord_expcyclesku_status');
	expskustatusvalue = searchresult.getValue('custrecord_expcyclesku_status');
	actskustatus = searchresult.getText('custrecord_cyclesku_status');
	actskustatusvalue = searchresult.getValue('custrecord_cyclesku_status');

	if(actskustatus == null || actskustatus == "")
		actskustatus = expskustatus;
	if(actskustatusvalue == null || actskustatusvalue == "")
		actskustatusvalue = expskustatusvalue;

	planno = searchresult.getValue('custrecord_cycle_count_plan');
	name = searchresult.getValue('custrecord_cycle_count_plan');
	opentaskid = searchresult.getValue('custrecord_opentaskid');
	invtid = searchresult.getValue('custrecord_invtid');
	siteid = searchresult.getValue('custrecord_cyclesite_id');
	actlot = searchresult.getValue('custrecord_cycleabatch_no');
	explot = searchresult.getValue('custrecord_expcycleabatch_no');

	var vItemname='';
	var vCost=0;
	var vAvgCost=0;

	//Code Added By Ganapathi on 26th Nov 2012
	if(actsku!=null && actsku!='')
	{
		var filters = new Array();          
		filters.push(new nlobjSearchFilter('internalid', null, 'is',actsku));
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('cost');
		columns[1] = new nlobjSearchColumn('averagecost');
		columns[2] = new nlobjSearchColumn('itemid');

		var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
		if (itemdetails !=null) 
		{
			vItemname=itemdetails[0].getValue('itemid');
			vCost = itemdetails[0].getValue('cost');
			nlapiLogExecution('ERROR', 'vCost', vCost);
			vAvgCost = itemdetails[0].getValue('averagecost');
			nlapiLogExecution('ERROR', 'Average Cost', vAvgCost);          
		}
	}

	if(actqty==null||actqty=="")
	{
		nlapiLogExecution('ERROR', 'actqty:', actqty);
		actqty=0;
	}

	if(expqty==null||expqty=="")
	{
		nlapiLogExecution('ERROR', 'expqty: ', expqty);
		expqty=0;
	}

	dollarvariance = (parseFloat(actqty) - parseFloat(expqty)) * vAvgCost;

	nlapiLogExecution('ERROR','dollarvariance',dollarvariance);

	if(isNaN(dollarvariance)||dollarvariance==null||dollarvariance=="")
	{
		nlapiLogExecution('ERROR', 'dollarvariance: ', dollarvariance);
		dollarvariance=0;
	}
	else
	{
		//dollarvariance=Math.round(dollarvariance,2);	
		dollarvariance = Math.round(dollarvariance * 100) / 100;
		//parseFloat(dollarvariance).round(2);

	}

	//code to get custpage_qtytobeadjust value based on RF picking done between record and resolve

	nlapiLogExecution('ERROR','actsku',actsku);
	nlapiLogExecution('ERROR','loc',loc);
	nlapiLogExecution('ERROR','actskustatusvalue',actskustatusvalue);
	nlapiLogExecution('ERROR','expskustatusvalue',expskustatusvalue);
	nlapiLogExecution('ERROR','actlp',actlp);

	var QoH=0;
	var NewExpQty=0,NewActQty=0,ExpQuantity=0,QtytobeAdjusted=0,SkuStatusValue;

	if(actskustatusvalue == null || actskustatusvalue == '' || actskustatusvalue=='null')
	{
		actskustatusvalue = expskustatusvalue;
	}
	var filterslp = new Array();
	//filterslp[0] = new nlobjSearchFilter('custrecord_invttasktype', null, 'anyof', [2, 10]);
	filterslp.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',[19]));

	if(actsku!=null && actsku!='')	
		filterslp.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof',actsku));

	if(loc!=null && loc!='')	
		filterslp.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof',loc));

	if(actskustatusvalue!=null && actskustatusvalue!='')
		filterslp.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'anyof',actskustatusvalue));

	if(actlp!=null && actlp!='')
		filterslp.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is',actlp));


	var columnlp=new Array();
	columnlp[0]= new nlobjSearchColumn('custrecord_ebiz_qoh');
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filterslp,columnlp);

	if(searchresults != null && searchresults != '')
	{
		QoH = searchresults[0].getValue('custrecord_ebiz_qoh');
		if(parseFloat(QoH) != parseFloat(expqty))
		{
			nlapiLogExecution('ERROR', 'in not equal if QoH', QoH);  
			nlapiLogExecution('ERROR', 'in not equal if expqty', expqty);  
			nlapiLogExecution('ERROR', 'in not equal if actqty', actqty);  

			ExpQuantity = parseFloat(expqty) - parseFloat(QoH);
			nlapiLogExecution('ERROR', 'in not equal if ExpQuantity', ExpQuantity);  
			NewActQty = parseFloat(actqty) - parseFloat(ExpQuantity);
			nlapiLogExecution('ERROR', 'in not equal if NewActQty', NewActQty);  
			/*if(parseFloat(NewActQty) == 0)
			{
				QtytobeAdjusted = 0;
			}
			else
			{*/
			QtytobeAdjusted = parseFloat(NewActQty) - parseFloat(QoH);
			//	}

			nlapiLogExecution('ERROR', 'after ExpQuantity', ExpQuantity);    
			nlapiLogExecution('ERROR', 'after NewActQty', NewActQty);    
			nlapiLogExecution('ERROR', 'after QtytobeAdjusted', QtytobeAdjusted);    

		}
		else
		{
			nlapiLogExecution('ERROR', 'in not equal else', 'else');    
			QtytobeAdjusted = parseFloat(actqty) - parseFloat(expqty);
		}
	}
	else
	{
		nlapiLogExecution('ERROR', 'in search result null else', 'else');  
		var tempqty = 0;
		if(parseFloat(expqty) < parseFloat(actqty))
		{
			NewActQty = parseFloat(actqty) - parseFloat(expqty);
			QtytobeAdjusted = parseFloat(NewActQty) - parseFloat(tempqty);
		}	
		else
		{
			QtytobeAdjusted = parseFloat(tempqty) - parseFloat(actqty);
		}
//		if(dollarvariance=='0')
//		{
//		QtytobeAdjusted = '0';
//		}
	}

	//var serialnourl = nlapiResolveURL('SUITELET', 'customscript_lprelated_serial', 'customdeploy_lprelated_serial') + "&custparam_item=" + actsku + "&custparam_planno=" + planno;
	var serialnourl = nlapiResolveURL('SUITELET', 'customscript_lprelated_serial', 'customdeploy_lprelated_serial') + "&custparam_expserial=" + null + "&custparam_actserial=" + null + "&custparam_expskustatus=" + expskustatusvalue + "&custparam_actskustatus=" + actskustatusvalue +"&custparam_recid=" + searchresult.getId();
	var Invturl; //= nlapiResolveURL('SUITELET', 'customscript_ebiz_resolve_expitemurl', 'customdeploy_ebiz_resolve_expitemurl_di') + "&custparam_expsku=" + actsku + "&custparam_planno=" + planno;
	var BinlocInvturl; //= nlapiResolveURL('SUITELET', 'customscript_ebiz_cycbinloc_invt', 'customdeploy_ebiz_cycbinloc_invt_di') + "&custparam_binloc=" + loc + "&custparam_planno=" + planno;


	nlapiLogExecution('ERROR', 'actsku', actsku);
	nlapiLogExecution('ERROR', 'loc', loc);
	nlapiLogExecution('ERROR', 'actlp', actlp);

	if(actsku!=null&&actsku!="")
	{
		var invtfilters = new Array();		
		invtfilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', loc));
		invtfilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', actsku));
		invtfilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', actlp));
		invtfilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

		var invtcolumns = new Array();
		invtcolumns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
		invtcolumns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_loc');
		invtcolumns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
		invtcolumns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
		invtcolumns[4] = new nlobjSearchColumn('custrecord_ebiz_avl_qty');
		invtcolumns[0].setSort();
		var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, invtfilters, invtcolumns);
		if(invtsearchresults!=null && invtsearchresults!='')
			var currentinvt = invtsearchresults[0].getValue('custrecord_ebiz_avl_qty');
	}
	if(currentinvt=='undefined'||currentinvt==null||currentinvt==''||isNaN(currentinvt)){
		currentinvt=0;
	}
	//form.getSubList('custpage_items').setLineItemValue('custpage_locationtext', i + 1, locText);
	form.getSubList('custpage_items').setLineItemValue('custpage_locationtext', i + 1, "<a href='"+ BinlocInvturl +"' target='_blank'>"+ locText + "</a>");
	form.getSubList('custpage_items').setLineItemValue('custpage_location', i + 1, loc);
	form.getSubList('custpage_items').setLineItemValue('custpage_expsku', i + 1, expsku);
	form.getSubList('custpage_items').setLineItemValue('custpage_expskutext', i + 1, expskutext);
	nlapiLogExecution('ERROR','actsku before bind',actsku);
	form.getSubList('custpage_items').setLineItemValue('custpage_actsku', i + 1, actsku);                    
	//form.getSubList('custpage_items').setLineItemValue('custpage_actskutext', i + 1, actskutext);
	//form.getSubList('custpage_items').setLineItemValue('custpage_actskutext', i + 1, "<a href='"+ Invturl +"' target='_blank'>"+ actskutext + "</a>");
	form.getSubList('custpage_items').setLineItemValue('custpage_actskutextlink', i + 1, "<a href=\"#\" onclick=\"javascript:window.open('" + Invturl + "');\" >" + actskutext + "</a>");
	form.getSubList('custpage_items').setLineItemValue('custpage_expqty', i + 1, expqty);
	form.getSubList('custpage_items').setLineItemValue('custpage_actqty', i + 1, actqty);

	form.getSubList('custpage_items').setLineItemValue('custpage_dollarvariance', i + 1, dollarvariance);
	//case 20124779, Deciaml quantity fixed to 2 points.
	form.getSubList('custpage_items').setLineItemValue('custpage_qtyvariance', i + 1, (parseFloat(actqty) - parseFloat(expqty)).toFixed(2));
//	End
	//form.getSubList('custpage_items').setLineItemValue('custpage_qtytobeadjust', i + 1, parseFloat(actqty) - parseFloat(expqty));
	form.getSubList('custpage_items').setLineItemValue('custpage_qtytobeadjust', i + 1, parseFloat(QtytobeAdjusted).toFixed(2));
	form.getSubList('custpage_items').setLineItemValue('custpage_explp', i + 1, explp);
	form.getSubList('custpage_items').setLineItemValue('custpage_actlp', i + 1, actlp);

	//form.getSubList('custpage_items').setLineItemValue('custpage_expserial', i + 1, expserialno);
	//form.getSubList('custpage_items').setLineItemValue('custpage_actserial', i + 1, actserialno);


	form.getSubList('custpage_items').setLineItemValue('custpage_expbatch', i + 1, explot);
	form.getSubList('custpage_items').setLineItemValue('custpage_actbatch', i + 1, actlot);

	form.getSubList('custpage_items').setLineItemValue('custpage_expskustatus', i + 1, expskustatus);
	form.getSubList('custpage_items').setLineItemValue('custpage_expskustatusvalue', i + 1, expskustatusvalue);
	form.getSubList('custpage_items').setLineItemValue('custpage_actskustatus', i + 1, actskustatus);
	form.getSubList('custpage_items').setLineItemValue('custpage_actskustatusvalue', i + 1, actskustatusvalue);

	form.getSubList('custpage_items').setLineItemValue('custpage_id', i + 1, searchresult.getId());
	form.getSubList('custpage_items').setLineItemValue('custpage_planno', i + 1, planno);
	form.getSubList('custpage_items').setLineItemValue('custpage_name', i + 1, name);
	form.getSubList('custpage_items').setLineItemValue('custpage_opentaskid', i + 1, opentaskid);
	form.getSubList('custpage_items').setLineItemValue('custpage_invtid', i + 1, invtid);
	form.getSubList('custpage_items').setLineItemValue('custpage_site', i + 1, siteid);
	form.getSubList('custpage_items').setLineItemValue('custpage_currentinvt', i + 1, currentinvt);
	nlapiLogExecution('ERROR', 'siteid ', siteid);
	nlapiLogExecution('ERROR', 'after loc', loc);
	nlapiLogExecution('ERROR', 'after searchresultinvtid', invtid);
	nlapiLogExecution('ERROR', 'after expsku', expsku);
	nlapiLogExecution('ERROR', 'after actsku', actsku);     

}

function AdjustSerialNumbers(localSerialNoArray,qty,item,lp,restype,CyccRecID,varianceinExpserialNo)
{	
	try
	{
		nlapiLogExecution('ERROR','AdjustSerialNumbers qty',parseFloat(qty));
		nlapiLogExecution('ERROR','item',item);
		nlapiLogExecution('ERROR','lp',lp);
		nlapiLogExecution('ERROR','restype',restype);
		nlapiLogExecution('ERROR','CyccRecID',CyccRecID);
		if(restype == 'resolve'|| restype == 'close')
		{
			var fields = ['custrecord_cycle_serialno','custrecord_cycle_expserialno'];
			var columns = nlapiLookupField('customrecord_ebiznet_cyclecountexe', CyccRecID, fields);
			var ActSerialNos = columns.custrecord_cycle_serialno;
			var ActSerialNosArrayTemp=new Array();

			if(ActSerialNos!=null && ActSerialNos!='')
			{
				ActSerialNosArrayTemp=ActSerialNos.split(',');
			}

			nlapiLogExecution('ERROR', 'ActSerialNos in AdjustSerialNumbers: ', ActSerialNos);
			nlapiLogExecution('ERROR', 'ActSerialNosArrayTemp in AdjustSerialNumbers: ', ActSerialNosArrayTemp);
			var ExpSerialNos = columns.custrecord_cycle_expserialno;
			nlapiLogExecution('ERROR', 'ExpSerialNos in AdjustSerialNumbers: ', ExpSerialNos);


			if(parseFloat(qty) < 0)
			{
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
				filters[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'isnot', 'D');
				filters[3] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof',[3,19]);

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

				if(searchResults != null && searchResults != "" && ActSerialNos == "")
				{ nlapiLogExecution('ERROR', 'searchResults.length in qty<0 in if: ', searchResults.length);
				for (var i = 0; i < searchResults.length; i++) 
				{   
					if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
					{
						var SerialNo = searchResults[i].getValue('custrecord_serialnumber');
						var InternalID = searchResults[i].getId();

						//var currentRow = [SerialNo];					
						//localSerialNoArray.push(currentRow);
						//nlapiLogExecution('ERROR', 'localSerialNoArray : ', localSerialNoArray.toString());

						//var id = nlapiDeleteRecord('customrecord_ebiznetserialentry', InternalID);
						//nlapiLogExecution('ERROR', 'Deleted Actual Record : ', id);
					}
				}
				}
				else
				{


					var filters = new Array();
					filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
					filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);

					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
					var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

					if(searchResults != null && searchResults != "")
					{ nlapiLogExecution('ERROR', 'searchResults.length in qty<0 in else: ', searchResults.length);
					for (var i = 0; i < searchResults.length; i++) 
					{   
						if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
						{
							var SerialNo = searchResults[i].getValue('custrecord_serialnumber');

							nlapiLogExecution('ERROR', 'searchResults.length : SerialNo ', SerialNo);
							nlapiLogExecution('ERROR', 'searchResults.length : ActSerialNos', ActSerialNos);


							//case # 20126283
							if(ActSerialNosArrayTemp.indexOf(SerialNo) == -1)
							{
								//if(varianceinExpserialNo.indexOf(SerialNo) != -1)
								//{
								nlapiLogExecution('ERROR', 'SerialNo in ActSerialNos : ', SerialNo);
								var InternalID = searchResults[i].getId();

								//var currentRow = [SerialNo];					
								//localSerialNoArray.push(currentRow);
								//nlapiLogExecution('ERROR', 'localSerialNoArray : ', localSerialNoArray.toString());

								/*var LoadSerialNumbers = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
									LoadSerialNumbers.setFieldValue('custrecord_serialwmsstatus', '32');

									var recid = nlapiSubmitRecord(LoadSerialNumbers, false, true);
									nlapiLogExecution('ERROR', 'Serial Entry rec id',recid);*/									

								var fieldNames = new Array(); 
								var newValues = new Array(); 

								fieldNames.push('custrecord_serialwmsstatus'); 
								newValues.push('32');

								nlapiSubmitField('customrecord_ebiznetserialentry', InternalID, fieldNames, newValues);
								//}

								//var id = nlapiDeleteRecord('customrecord_ebiznetserialentry', InternalID);
								//nlapiLogExecution('ERROR', 'Deleted Actual Record : ', id);
							}
						}
					}
					}
				}
			}
			else if(parseFloat(qty) > 0)
			{
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
				//Case #  20126109  Start,
				filters[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'I');
				//Case # 20126109 End.

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				columns[1] = new nlobjSearchColumn('custrecord_serialstatus');
				var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

				if(searchResults != null && searchResults != "")
				{ 
					nlapiLogExecution('ERROR', 'searchResults.length : ', searchResults.length);
					for (var i = 0; i < searchResults.length; i++) 
					{   
						if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
						{
							var SerialNo = searchResults[i].getValue('custrecord_serialnumber');
							var SerialStatus=searchResults[i].getValue('custrecord_serialstatus');
							/*var InternalID = searchResults[i].getId();

							var currentRow = [SerialNo];					
							localSerialNoArray.push(currentRow);
							nlapiLogExecution('ERROR', 'localSerialNoArray : ', localSerialNoArray.toString());

							var LoadSerialNumbers = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
							LoadSerialNumbers.setFieldValue('custrecord_serialstatus', '');

							var recid = nlapiSubmitRecord(LoadSerialNumbers, false, true);
							nlapiLogExecution('ERROR', 'Serial Entry rec id',recid);*/
							if(ActSerialNosArrayTemp.indexOf(SerialNo) == -1)
							{
								/*nlapiLogExecution('ERROR', 'Index of SerialNo in ExpSerialNosArrayTemp  ', ExpSerialNosArrayTemp.indexOf(SerialNo));
								if(ExpSerialNosArrayTemp.indexOf(SerialNo) != -1)
								{*/
								nlapiLogExecution('ERROR', 'SerialNo in ActSerialNos if qty>0: ', SerialNo);
								var InternalID = searchResults[i].getId();

								//var currentRow = [SerialNo];					
								//localSerialNoArray.push(currentRow);
								//nlapiLogExecution('ERROR', 'localSerialNoArray : ', localSerialNoArray.toString());

								/*var LoadSerialNumbers = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
									LoadSerialNumbers.setFieldValue('custrecord_serialwmsstatus', '32');

									var recid = nlapiSubmitRecord(LoadSerialNumbers, false, true);
									nlapiLogExecution('ERROR', 'Serial Entry rec id',recid);*/
								//case #  20126283 Added If condition.
								/*if(SerialStatus!='I'){*/

								var fieldNames = new Array(); 
								var newValues = new Array(); 

								fieldNames.push('custrecord_serialwmsstatus'); 
								newValues.push('32');

								nlapiSubmitField('customrecord_ebiznetserialentry', InternalID, fieldNames, newValues);
								//}
								//}

								//var id = nlapiDeleteRecord('customrecord_ebiznetserialentry', InternalID);
								//nlapiLogExecution('ERROR', 'Deleted Actual Record : ', id);
							}
						}
					}
				}
			}
			else if(parseFloat(qty) == 0)
			{
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

				if(searchResults != null && searchResults != "")
				{ 
					for (var i = 0; i < searchResults.length; i++) 
					{   
						if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
						{
							var SerialNo = searchResults[i].getValue('custrecord_serialnumber');							
							//var InternalID = searchResults[i].getId();



							//added now
							if(ActSerialNosArrayTemp.indexOf(SerialNo) == -1)
							{
								var InternalID = searchResults[i].getId();
								//nlapiLogExecution('ERROR', 'Serial Entry rec id to tst',InternalID);

								//var currentRow = [SerialNo];					
								//localSerialNoArray.push(currentRow);
								//nlapiLogExecution('ERROR', 'localSerialNoArray in resolve and qty ==0: ', localSerialNoArray.toString());

								/*var LoadSerialNumbers = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
								LoadSerialNumbers.setFieldValue('custrecord_serialwmsstatus', '32');

								var recid = nlapiSubmitRecord(LoadSerialNumbers, false, true);*/
								//nlapiLogExecution('ERROR', 'Serial Entry rec id',recid);

								var fieldNames = new Array(); 
								var newValues = new Array(); 

								fieldNames.push('custrecord_serialwmsstatus'); 
								newValues.push('32');

								nlapiSubmitField('customrecord_ebiznetserialentry', InternalID, fieldNames, newValues);

							}
							//added now


							//var id = nlapiDeleteRecord('customrecord_ebiznetserialentry', InternalID);
							//nlapiLogExecution('ERROR', 'Deleted Actual Record : ', id);	


						}
					}
				}
			}
		}
		if(restype == 'ignore' || restype == 'recount')
		{
			if(parseFloat(qty) < 0)
			{
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
				filters[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'D');

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

				if(searchResults != null && searchResults != "")
				{ 
					for (var i = 0; i < searchResults.length; i++) 
					{   
						if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
						{
							var SerialNo = searchResults[i].getValue('custrecord_serialnumber');
							var InternalID = searchResults[i].getId();

							//var currentRow = [SerialNo];					
							//localSerialNoArray.push(currentRow);
							//nlapiLogExecution('ERROR', 'localSerialNoArray : ', localSerialNoArray.toString());

							var LoadSerialNumbers = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
							LoadSerialNumbers.setFieldValue('custrecord_serialstatus', '');

							var recid = nlapiSubmitRecord(LoadSerialNumbers, false, true);
							nlapiLogExecution('ERROR', 'Serial Entry rec id',recid);
						}
					}
				}
			}
			else if(parseFloat(qty) > 0)
			{
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
				filters[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'I');

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

				if(searchResults != null && searchResults != "")
				{ 
					nlapiLogExecution('ERROR', 'searchResults.length : ', searchResults.length);
					for (var i = 0; i < searchResults.length; i++) 
					{   
						if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
						{
							var SerialNo = searchResults[i].getValue('custrecord_serialnumber');
							var InternalID = searchResults[i].getId();

							//var currentRow = [SerialNo];					
							//localSerialNoArray.push(currentRow);
							//nlapiLogExecution('ERROR', 'localSerialNoArray : ', localSerialNoArray.toString());							

							var id = nlapiDeleteRecord('customrecord_ebiznetserialentry', InternalID);
							nlapiLogExecution('ERROR', 'Deleted Actual Record : ', id);
						}
					}
				}
			}		
		}		

	}
	catch (e) {
		nlapiLogExecution('ERROR', 'Catch', e);
	}
	return localSerialNoArray;
}

function getSerialNoCSV(arrayLP){
	var csv = "";
	nlapiLogExecution('ERROR', 'arrayLP.length',arrayLP.length);
	if(arrayLP.length == 1)
	{
		csv = arrayLP[0][0];
	}
	else
	{
		for (var i = 0; i < arrayLP.length; i++) {
			if(i == arrayLP.length -1)
			{
				csv += arrayLP[i][0];
			}
			else
			{
				csv += arrayLP[i][0] + ",";
			}
		}
	}
	return csv;
}

function fetchPalletQuantity(itemId, location, company)
{
	nlapiLogExecution('ERROR','itemId',itemId);
	nlapiLogExecution('ERROR','location in fetchPalletQuantity',location);
	var itemPalletQuantity = 0;
	var itemFilters = new Array();
	itemFilters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemId);
	itemFilters[1] = new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'is', 3);

	var itemColumns = new Array();
	itemColumns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	itemColumns[1] = new nlobjSearchColumn('custrecord_ebizsiteskudim');
	itemColumns[2] = new nlobjSearchColumn('custrecord_ebizcompanyskudimension');
	itemColumns[3] = new nlobjSearchColumn('custrecord_ebizqty'); 

	var itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, itemFilters, itemColumns);

	if (itemSearchResults != null)
	{
		itemPalletQuantity = itemSearchResults[0].getValue(itemColumns[3]);
		nlapiLogExecution('ERROR','Item Pallet Quantity',itemPalletQuantity);
	}
	return itemPalletQuantity;
}

function displaysublist(form, searchResults,hiddenfieldshowvalue){
	var loc,locText,expsku,expskutext,actskutext,actsku,expqty,actqty,explp,actlp,expskustatus,actskustatus;
	var actskustatusvalue,planno,name,opentaskid,intid,siteis,actlot,explot,expskustatusvalue,expserialno ,actserialno ;
	var dollarvariance;

	/*
	 *  Searching records from custom record and dynamically adding to
	 * the sublist lines
	 */
	var i=0;

	if(searchResults != null && searchResults != "")
	{
		var orderListArray=new Array();		
		for(k=0;k<searchResults.length;k++)
		{
			//nlapiLogExecution('ERROR', 'searchResults[k]', searchResults[k]); 
			var ordsearchresult = searchResults[k];

			if(ordsearchresult!=null)
			{
				//nlapiLogExecution('ERROR', 'ordsearchresult.length new tst', ordsearchresult.length); 
				for(var j=0;j<ordsearchresult.length;j++)
				{
					orderListArray[orderListArray.length]=ordsearchresult[j];				
				}
			}
		}

		nlapiLogExecution('ERROR', 'searchresults.length in new sublist bind', searchResults.length);
		var ShowAll,ShowDis,ShowNonDis;
		if(request.getParameter('restypeflag')=='cyclecountsa' && (request.getParameter('custpage_hiddenfieldshow') == '' || request.getParameter('custpage_hiddenfieldshow') == null))
		{
			nlapiLogExecution('ERROR', 'ShowAll if', ShowAll);
			//vsa = request.getParameter('restypeflag');
			ShowAll = 'T';
		}
		else if(request.getParameter('custpage_hiddenfieldshow') == 'ALL')
		{
			nlapiLogExecution('ERROR', 'ShowAll else', ShowAll);
			ShowAll = 'T';
		}


		if(request.getParameter('restypeflag')=='cyclecountsd' && (request.getParameter('custpage_hiddenfieldshow') == '' || request.getParameter('custpage_hiddenfieldshow') == null))
		{
			nlapiLogExecution('ERROR', 'ShowDis if', ShowDis);
			//vsa = request.getParameter('restypeflag');
			ShowDis = 'T';
		}
		else if(request.getParameter('custpage_hiddenfieldshow') == 'D')
		{
			nlapiLogExecution('ERROR', 'ShowDis else', ShowDis);
			ShowDis = 'T';
		}

		if(request.getParameter('restypeflag')=='cyclecountsnd' && (request.getParameter('custpage_hiddenfieldshow') == '' || request.getParameter('custpage_hiddenfieldshow') == null))
		{
			nlapiLogExecution('ERROR', 'ShowNonDis if', ShowNonDis);
			//vsa = request.getParameter('restypeflag');
			ShowNonDis = 'T';
		}
		else if(request.getParameter('custpage_hiddenfieldshow') == 'ND')
		{
			nlapiLogExecution('ERROR', 'ShowNonDis else', ShowNonDis);
			ShowNonDis = 'T';
		}

		nlapiLogExecution('ERROR', 'ShowAll in bind', ShowAll);
		nlapiLogExecution('ERROR', 'ShowDis in bind', ShowDis);
		nlapiLogExecution('ERROR', 'ShowNonDis in bind', ShowNonDis);


		for(var m=0;m<orderListArray.length;m++)
		{
			nlapiLogExecution('ERROR', 'm', m);			

			loc = orderListArray[m].getValue('custrecord_cycact_beg_loc');
			locText = orderListArray[m].getText('custrecord_cycact_beg_loc');
			expsku = orderListArray[m].getValue('custrecord_cyclesku');
			expskutext = orderListArray[m].getText('custrecord_cyclesku');
			//actsku = orderListArray[m].getValue('custrecord_cycleact_sku');
			actskutext = orderListArray[m].getText('custrecord_cycleact_sku');
			actsku = orderListArray[m].getValue('custrecord_cycleact_sku');
			expqty = orderListArray[m].getValue('custrecord_cycleexp_qty');
			actqty = orderListArray[m].getValue('custrecord_cycle_act_qty');
			explp = orderListArray[m].getValue('custrecord_cyclelpno');
			actlp = orderListArray[m].getValue('custrecord_cycact_lpno');

			expserialno = orderListArray[m].getValue('custrecord_cycle_expserialno');
			actserialno = orderListArray[m].getValue('custrecord_cycle_serialno');

//			nlapiLogExecution('ERROR', 'expserialno in show discrepancy', expserialno);		
//			nlapiLogExecution('ERROR', 'actserialno in show discrepancy', actserialno);		

			expskustatus = orderListArray[m].getText('custrecord_expcyclesku_status');
			expskustatusvalue = orderListArray[m].getValue('custrecord_expcyclesku_status');
			actskustatus = orderListArray[m].getText('custrecord_cyclesku_status');
			actskustatusvalue = orderListArray[m].getValue('custrecord_cyclesku_status');

			if(actskustatus == null || actskustatus == "")
				actskustatus = expskustatus;
			if(actskustatusvalue == null || actskustatusvalue == "")
				actskustatusvalue = expskustatusvalue;

//			nlapiLogExecution('ERROR', 'actskustatus in show discrepancy', actskustatus);		
//			nlapiLogExecution('ERROR', 'expskustatus in show discrepancy', expskustatus);	


			planno = orderListArray[m].getValue('custrecord_cycle_count_plan');
			name = orderListArray[m].getValue('custrecord_cycle_count_plan');
			opentaskid = orderListArray[m].getValue('custrecord_opentaskid');
			invtid = orderListArray[m].getValue('custrecord_invtid');
			siteid = orderListArray[m].getValue('custrecord_cyclesite_id');
			actlot = orderListArray[m].getValue('custrecord_cycleabatch_no');
			explot = orderListArray[m].getValue('custrecord_expcycleabatch_no');

			//Code Added By Ganapathi on 26th Nov 2012
			/*var filters = new Array();   
			if(actsku!=null && actsku!='')
				filters.push(new nlobjSearchFilter('internalid', null, 'is',actsku));
			filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('cost');
			columns[1] = new nlobjSearchColumn('averagecost');
			columns[2] = new nlobjSearchColumn('itemid');

			var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
			if (itemdetails !=null) 
			{
				vItemname=itemdetails[0].getValue('itemid');
				vCost = itemdetails[0].getValue('cost');
				nlapiLogExecution('ERROR', 'vCost', vCost);
				vAvgCost = itemdetails[0].getValue('averagecost');
				nlapiLogExecution('ERROR', 'Average Cost', vAvgCost);          
			}*/
			var vAvgCost=orderListArray[m].getValue('averagecost','custrecord_cycleact_sku');
			nlapiLogExecution('ERROR', 'vAvgCost:', vAvgCost);
			if(actqty==null||actqty=="")
			{
				nlapiLogExecution('ERROR', 'actqty:', actqty);
				actqty=0;
			}

			if(expqty==null||expqty=="")
			{
				nlapiLogExecution('ERROR', 'expqty: ', expqty);
				expqty=0;
			}
			if(vAvgCost==null||vAvgCost=="")
			{
				//nlapiLogExecution('ERROR', 'vAvgCost: ', vAvgCost);
				vAvgCost=0;
			}

			dollarvariance = (parseFloat(actqty) - parseFloat(expqty)) * vAvgCost;

			nlapiLogExecution('ERROR','dollarvariance',dollarvariance);

			if(isNaN(dollarvariance)||dollarvariance==null||dollarvariance=="")
			{
				//nlapiLogExecution('ERROR', 'dollarvariance: ', dollarvariance);
				dollarvariance=0;
			}
			else
			{
				dollarvariance=Math.round(dollarvariance,2);
			}

			nlapiLogExecution('ERROR','planno',planno);
			var varianceinExpserialNo='';
			var varianceinActserialNo='';		
			if(actserialno != null && actserialno != '' && expserialno != null && expserialno != '')
			{
				actserialno = actserialno.trim();
				expserialno = expserialno.trim();

				if(expserialno != null && expserialno != '')
					var getexpserialArr = expserialno.split(',');

				if(actserialno != null && actserialno != '')
					var getactserialArr = actserialno.split(',');


				if(getexpserialArr != null && getexpserialArr != '')
				{
					for (var p = 0; p < getexpserialArr.length; p++) 
					{				
						if(actserialno != null && actserialno != '')
						{
							if(actserialno.indexOf(getexpserialArr[p])==-1)//not found temSeriIdArr
							{
								if (varianceinExpserialNo == "") {
									varianceinExpserialNo= getexpserialArr[p];
								}
								else {
									varianceinExpserialNo = varianceinExpserialNo + "," + getexpserialArr[p];
								}
							}
						}
					}
				}
				//nlapiLogExecution('ERROR', 'varianceinExpserialNo', varianceinExpserialNo);

				if(getactserialArr != null && getactserialArr != '')
				{
					for (var q = 0; q < getactserialArr.length; q++) 
					{
						if(getexpserialArr != null && getexpserialArr != '')
						{

							if(getexpserialArr.indexOf(getactserialArr[q])==-1)//not found temSeriIdArr
							{
								if (varianceinActserialNo == "") {
									varianceinActserialNo= getactserialArr[q];
								}
								else {
									varianceinActserialNo = varianceinActserialNo + "," + getactserialArr[q];
								}
							}
						}
					}
				}
				//nlapiLogExecution('ERROR', 'varianceinActserialNo', varianceinActserialNo);
			}

			/*nlapiLogExecution('ERROR', 'expqty', expqty);
			nlapiLogExecution('ERROR', 'actqty', actqty);
			nlapiLogExecution('ERROR', 'expsku', expsku);
			nlapiLogExecution('ERROR', 'actsku', actsku);
			nlapiLogExecution('ERROR', 'explot', explot);
			nlapiLogExecution('ERROR', 'actlot', actlot);
			nlapiLogExecution('ERROR', 'explp', explp);
			nlapiLogExecution('ERROR', 'actlp', actlp);
			nlapiLogExecution('ERROR', 'expskustatusvalue', expskustatusvalue);
			nlapiLogExecution('ERROR', 'actskustatusvalue', actskustatusvalue);*/

			nlapiLogExecution('ERROR', 'orderListArray[m].getId()', orderListArray[m].getId());
			//var serialnourl = nlapiResolveURL('SUITELET', 'customscript_lprelated_serial', 'customdeploy_lprelated_serial') + "&custparam_item=" + actsku+ "&custparam_planno=" + planno;
			var serialnourl = nlapiResolveURL('SUITELET', 'customscript_lprelated_serial', 'customdeploy_lprelated_serial') + "&custparam_expserial=" + null + "&custparam_actserial=" + null + "&custparam_expskustatus=" + expskustatusvalue + "&custparam_actskustatus=" + actskustatusvalue +"&custparam_recid=" + orderListArray[m].getId();
//			case 20124074 start
			var Invturl; //= nlapiResolveURL('SUITELET', 'customscript_ebiz_resolve_expitemurl', 'customdeploy_ebiz_resolve_expitemurl_di') + "&custparam_expsku=" + actsku + "&custparam_planno=" + planno;
//			end

			//if((request.getParameter('restypeflag')=='cyclecountsd' || hiddenfieldshowvalue == 'D') && (expqty != actqty ||  expsku != actsku || expserialno != actserialno || explot != actlot || explp != actlp ))
			if((ShowDis == 'T') && (expqty != actqty ||  expsku != actsku || explot != actlot || explp != actlp || expskustatusvalue != actskustatusvalue || varianceinExpserialNo != "" ||  varianceinActserialNo != ""))
			{
				i = i+1;
				nlapiLogExecution('ERROR', 'request.getParameter(restypeflag) in if binding', request.getParameter('restypeflag'));
				nlapiLogExecution('ERROR', 'hiddenfieldshowvalue in if binding', hiddenfieldshowvalue);
				nlapiLogExecution('ERROR', 'orderListArray[m].getId() inside', orderListArray[m].getId());


				form.getSubList('custpage_items').setLineItemValue('custpage_locationtext', i, locText);
				form.getSubList('custpage_items').setLineItemValue('custpage_location', i, loc);
				form.getSubList('custpage_items').setLineItemValue('custpage_expsku', i, expsku);
				form.getSubList('custpage_items').setLineItemValue('custpage_expskutext', i, expskutext);
				form.getSubList('custpage_items').setLineItemValue('custpage_actsku', i, actsku);                    
				//form.getSubList('custpage_items').setLineItemValue('custpage_actskutext', i, actskutext);
//				case 20124074 start
				form.getSubList('custpage_items').setLineItemValue('custpage_actskutextlink', i, "<a href=\"#\" onclick=\"javascript:window.open('" + Invturl + "');\" >" + actskutext + "</a>");
//				end
				form.getSubList('custpage_items').setLineItemValue('custpage_expqty', i, expqty);
				form.getSubList('custpage_items').setLineItemValue('custpage_actqty', i, actqty);

				form.getSubList('custpage_items').setLineItemValue('custpage_dollarvariance', i, dollarvariance);
				form.getSubList('custpage_items').setLineItemValue('custpage_qtyvariance', i, (parseFloat(actqty) - parseFloat(expqty)).toFixed(2));
				form.getSubList('custpage_items').setLineItemValue('custpage_qtytobeadjust', i, (parseFloat(actqty) - parseFloat(expqty)).toFixed(2));
				form.getSubList('custpage_items').setLineItemValue('custpage_explp', i, explp);
				form.getSubList('custpage_items').setLineItemValue('custpage_actlp', i, actlp);

				//form.getSubList('custpage_items').setLineItemValue('custpage_expserial', i, expserialno);
				//form.getSubList('custpage_items').setLineItemValue('custpage_actserial', i, actserialno);



				form.getSubList('custpage_items').setLineItemValue('custpage_expbatch', i, explot);
				form.getSubList('custpage_items').setLineItemValue('custpage_actbatch', i, actlot);

				form.getSubList('custpage_items').setLineItemValue('custpage_expskustatus', i, expskustatus);
				form.getSubList('custpage_items').setLineItemValue('custpage_expskustatusvalue', i, expskustatusvalue);
				form.getSubList('custpage_items').setLineItemValue('custpage_actskustatus', i, actskustatus);
				form.getSubList('custpage_items').setLineItemValue('custpage_actskustatusvalue', i, actskustatusvalue);

				form.getSubList('custpage_items').setLineItemValue('custpage_id', i, orderListArray[m].getId());
				form.getSubList('custpage_items').setLineItemValue('custpage_planno', i, planno);
				form.getSubList('custpage_items').setLineItemValue('custpage_name', i, name);
				form.getSubList('custpage_items').setLineItemValue('custpage_opentaskid', i, opentaskid);
				form.getSubList('custpage_items').setLineItemValue('custpage_invtid', i, invtid);
				form.getSubList('custpage_items').setLineItemValue('custpage_site', i, siteid);

			}

			//else if((request.getParameter('restypeflag')=='cyclecountsnd' || hiddenfieldshowvalue == 'ND') && (expqty == actqty &&  expsku == actsku && expserialno == actserialno && explot == actlot && explp == actlp ))
			else if((ShowNonDis == 'T') && (expqty == actqty &&  expsku == actsku && explot == actlot && explp == actlp && expskustatusvalue == actskustatusvalue && varianceinExpserialNo == "" && varianceinActserialNo == ""))
			{

				i =i+1;
				nlapiLogExecution('ERROR', 'request.getParameter(restypeflag) in else binding', request.getParameter('restypeflag'));
				nlapiLogExecution('ERROR', 'hiddenfieldshowvalue in else binding', hiddenfieldshowvalue);
				nlapiLogExecution('ERROR', 'orderListArray[m].getId() else inside', orderListArray[m].getId());


				form.getSubList('custpage_items').setLineItemValue('custpage_locationtext', i, locText);
				form.getSubList('custpage_items').setLineItemValue('custpage_location', i, loc);
				form.getSubList('custpage_items').setLineItemValue('custpage_expsku', i, expsku);
				form.getSubList('custpage_items').setLineItemValue('custpage_expskutext', i, expskutext);
				form.getSubList('custpage_items').setLineItemValue('custpage_actsku', i, actsku);                    
				//form.getSubList('custpage_items').setLineItemValue('custpage_actskutext', i, actskutext);
				form.getSubList('custpage_items').setLineItemValue('custpage_actskutextlink', i, "<a href=\"#\" onclick=\"javascript:window.open('" + serialnourl + "');\" >" + actskutext + "</a>");
				form.getSubList('custpage_items').setLineItemValue('custpage_expqty', i, expqty);
				form.getSubList('custpage_items').setLineItemValue('custpage_actqty', i, actqty);

				form.getSubList('custpage_items').setLineItemValue('custpage_dollarvariance', i, dollarvariance);
				form.getSubList('custpage_items').setLineItemValue('custpage_qtyvariance', i, (parseFloat(actqty) - parseFloat(expqty)).toFixed(2));
				form.getSubList('custpage_items').setLineItemValue('custpage_qtytobeadjust', i, (parseFloat(actqty) - parseFloat(expqty)).toFixed(2));
				form.getSubList('custpage_items').setLineItemValue('custpage_explp', i, explp);
				form.getSubList('custpage_items').setLineItemValue('custpage_actlp', i, actlp);

				//form.getSubList('custpage_items').setLineItemValue('custpage_expserial', i, expserialno);
				//form.getSubList('custpage_items').setLineItemValue('custpage_actserial', i, actserialno);


				form.getSubList('custpage_items').setLineItemValue('custpage_expbatch', i, explot);
				form.getSubList('custpage_items').setLineItemValue('custpage_actbatch', i, actlot);

				form.getSubList('custpage_items').setLineItemValue('custpage_expskustatus', i, expskustatus);
				form.getSubList('custpage_items').setLineItemValue('custpage_expskustatusvalue', i, expskustatusvalue);
				form.getSubList('custpage_items').setLineItemValue('custpage_actskustatus', i, actskustatus);
				form.getSubList('custpage_items').setLineItemValue('custpage_actskustatusvalue', i, actskustatusvalue);

				form.getSubList('custpage_items').setLineItemValue('custpage_id', i, orderListArray[m].getId());
				form.getSubList('custpage_items').setLineItemValue('custpage_planno', i, planno);
				form.getSubList('custpage_items').setLineItemValue('custpage_name', i, name);
				form.getSubList('custpage_items').setLineItemValue('custpage_opentaskid', i, opentaskid);
				form.getSubList('custpage_items').setLineItemValue('custpage_invtid', i, invtid);
				form.getSubList('custpage_items').setLineItemValue('custpage_site', i, siteid);
			}
		}
	}
}


function getSerialNoCSVvalues(ItemId,LP)
{
	var filters = new Array();

	filters[0] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', ItemId);	
	filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', LP);
	filters[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', '32');
	var tempSerial = "";

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
	var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
	if(serchRec!=null && serchRec!='')
	{
		for (var z = 0; z < serchRec.length; z++) {

			if (tempSerial == "") {
				tempSerial = serchRec[z].getValue('custrecord_serialnumber');

			}
			else {

				tempSerial = tempSerial + "," + serchRec[z].getValue('custrecord_serialnumber');
			}

		}


	}
	return tempSerial;
}

