/***************************************************************************
 eBizNET SOLUTIONS LTD
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/ebiz_RF_Packing_Qty.js,v $
 *     	   $Revision: 1.3.4.8.4.3.4.8 $
 *     	   $Date: 2014/06/13 12:49:21 $
 *     	   $Author: skavuri $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Packing_Qty.js,v $
 * Revision 1.3.4.8.4.3.4.8  2014/06/13 12:49:21  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.3.4.8.4.3.4.7  2014/05/30 00:41:03  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.3.4.8.4.3.4.6  2014/04/22 15:40:29  nneelam
 * case#  20148098
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.3.4.8.4.3.4.5  2014/03/13 15:12:18  sponnaganti
 * case# 20127682
 * (Invalid qty issue)
 *
 * Revision 1.3.4.8.4.3.4.4  2014/01/09 14:08:21  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.3.4.8.4.3.4.3  2014/01/06 13:19:17  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.3.4.8.4.3.4.2  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.3.4.8.4.3.4.1  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.3.4.8.4.3  2012/11/01 14:55:23  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.3.4.8.4.2  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.3.4.8.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.3.4.8  2012/09/03 13:52:07  schepuri
 * CASE201112/CR201113/LOG201121
 * added date stamp
 *
 * Revision 1.3.4.7  2012/08/01 07:16:42  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Picking Routing based on Loc group seq
 *
 * Revision 1.3.4.6  2012/06/02 09:30:42  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related packing was resolved.
 *
 * Revision 1.3.4.5  2012/04/30 10:10:24  spendyala
 * CASE201112/CR201113/LOG201121
 * While Searching of Item in ItemMaster,
 * 'name' filter is changed to 'nameinternal' and also checking weather the item is inactive state or not.
 *
 * Revision 1.3.4.4  2012/04/20 07:35:04  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Packing
 *
 * Revision 1.3.4.3  2012/04/17 10:52:40  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Packing
 *
 * Revision 1.3.4.1  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/21 07:03:02  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * RF Packing Functionality
 *
 * Revision 1.1  2011/04/19 14:19:06  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * RF Packing Functionality
 *
 *
 *****************************************************************************/
function EnterItemQty(request, response)
{
	if (request.getMethod() == 'GET') 
	{   

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7,st8,st9,st10,st11,st12,st13,st14,st15;

		if( getLanguage == 'es_ES')
		{
			st1 = "TEMA:";
			st2 = "SCANNED CANTIDAD MAYOR QUE EL PEDIDO CANTIDAD";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";
			st5 = "PEDIDO #: ";
			st6 = "CAJA #: ";
			st7 = "ART&#205;CULO INV&#193;LIDO";
			st8 = "CANT ANALIZAR";
			st9 = "CANT";
			st10 = "CANT ENTER: ";
			st11 = "PR&#211;XIMO";


		}
		else
		{
			st1 = "ITEM: ";
			st2 = "SCANNED QUANTITY GREATER THAN THE ORDER QTY";
			st3 = "SEND";
			st4 = "PREV";
			st5 = "ORDER#:";
			st6 = "CARTON#: ";
			st7 = "INVALID ITEM";
			st8 = "SCANNED QTY: ";
			st9 = "QTY: ";
			st10 = "ENTER/SCAN ITEM ";
			st11 = "NEXT";
			st15 = "ENTER QTY:";

		}
		var getOrderno = request.getParameter('custparam_orderno');
		var getContlpno=request.getParameter('custparam_contlpno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocationId = request.getParameter('custparam_beginLocation');
		var getItem = request.getParameter('custparam_item');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');        
		var getEnteredLocation = request.getParameter('custparam_endlocation');
		var getBeginBinLocation = request.getParameter('custparam_beginlocationname');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var vclusterNo = request.getParameter('custparam_clusterno');		
		//	var Itype = nlapiLookupField('item', getItemInternalId, 'recordType');
		//	var ItemRec = nlapiLoadRecord(Itype, getItemInternalId);
		var vBatchno = request.getParameter('custparam_batchno');
		var vcontlp = request.getParameter('custparam_contlp');
		var vcontsize = request.getParameter('custparam_contsize');
		var vlineCount = request.getParameter('custparam_linecount');
		var vloopCount = request.getParameter('custparam_loopcount');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var resultsCount =request.getParameter('custparam_count');
		var wmslocation=request.getParameter('custparam_wmslocation');
		var name=request.getParameter('custparam_name');
		var bulkqty=request.getParameter('custparam_bulkqty');
		var cortonNo=request.getParameter('custparam_corton');
		var MultipleItemScaned =request.getParameter('custpage_multipleitemscan');
		var getNumber;
		if(request.getParameter('custparam_number')!= null && request.getParameter('custparam_number') != "")
			getNumber = request.getParameter('custparam_number');
		else
			getNumber=0;

		nlapiLogExecution('DEBUG', 'bulkqty', bulkqty);
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";   
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";

		//	html = html + " document.getElementById('enterqty').focus();";        
		html = html + "</script>";
		html = html + "</head><body>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st5 +"<label>" + name + "</label>";	
		html = html + "				</td>";
		html = html + "			</tr>";  
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st6 +"<label>" + getContlpno + "</label>";		
		html = html + "				</td>";
		html = html + "			</tr>";  
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +"<label>" + getItem + "</label>";		
		html = html + "				</td>";
		html = html + "			</tr>"; 
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st9 +"<label>" + getExpectedQuantity + "</label>";
		html = html + "				<input type='hidden' name='hdnOrderNo' value=" + getOrderno + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnLpNo' value=" + vcontlp + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
		//case# 20127682 starts (Invalid qty issue)
		html = html + "				<input type='hidden' name='hdnItem' value='" + getItem + "'>";
		//case# 20127682 end
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEnteredLocation' value=" + getEnteredLocation + ">";
		html = html + "				<input type='hidden' name='hdnBeginBinLocation' value=" + getBeginBinLocation + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vclusterNo + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value=" + vBatchno + ">";
		html = html + "				<input type='hidden' name='hdnlinecount' value=" + vlineCount + ">";
		html = html + "				<input type='hidden' name='hdnloopcount' value=" + vloopCount + ">";
		html = html + "				<input type='hidden' name='hdnnextitem' value=" + NextItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdncontlpno' value=" + getContlpno + ">";
		html = html + "				<input type='hidden' name='hdnresultscount' value=" + resultsCount + ">";
		html = html + "				<input type='hidden' name='hdnwmslocation' value=" + wmslocation + ">";
		html = html + "				<input type='hidden' name='hdngetnumber' value=" + getNumber + ">";
		html = html + "				<input type='hidden' name='hdnname' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnname' value=" + cortonNo + ">";
		html = html + "				<input type='hidden' name='hdnbulkqty' value=" + bulkqty + ">";
		html = html + "				<input type='hidden' name='hdnMultipleItemScaned' value=" + MultipleItemScaned + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		


		html = html + "			</tr>";
		if(bulkqty=='')
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>"+ st8 +"<label>" + (parseFloat(getNumber)) + "</label>";

			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>"+ st10 +"";		
			html = html + "				</td>";
			html = html + "			</tr>"; 
		}
		else
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>"+ st15 +"";		
			html = html + "				</td>";
			html = html + "			</tr>";
		}


		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterqty' id='enterqty' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" ;
		if(bulkqty=='')
		{
			html = html + "              "+ st3 +" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.hdnsubmit.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdCompleteExit.disabled=true;return false' />";
		}
		html = html + "				 "+ st11 +" <input name='cmdEXIT' type='submit' value='F8'/>"+ st4 +" <input name='cmdPrevious' type='submit' value='F7'/>EXIT <input name='cmdCompleteExit' type='submit' value='EXIT'/>";        
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterqty').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');
		nlapiLogExecution('DEBUG', 'bulkqty', bulkqty);

		var getLanguage =  request.getParameter('hdngetLanguage');

		var varQty = request.getParameter('enterqty');
		var getNumber = request.getParameter('hdngetnumber');
//		if(bulkqty=='')
//		varQty=getNumber;
		if(bulkqty=='' || bulkqty==null)
		{
			varQty=getNumber;
		}

		nlapiLogExecution('DEBUG', 'Entered Qty', varQty);        

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');    
		var optENT = request.getParameter('cmdSend');
		var optExit=request.getParameter('cmdCompleteExit');
		nlapiLogExecution('DEBUG', 'optExit', optExit); 
		var OrderNo = request.getParameter('hdnOrderNo');
		var RecordInternalId = request.getParameter('hdnRecordInternalId');
		var ContainerLPNo = request.getParameter('hdnContainerLpNo');
		var ExpectedQuantity = request.getParameter('hdnExpectedQuantity');
		var BeginLocation = request.getParameter('hdnBeginLocationId');
		if(bulkqty=='' || bulkqty==null)
		{
			var Item = request.getParameter('enterqty');

		}

		else
		{
			var Item = request.getParameter('hdnItem');
		}

		var ItemDescription = request.getParameter('hdnItemDescription');
		var ItemInternalId = request.getParameter('hdnItemInternalId');
		var DoLineId = request.getParameter('hdnDOLineId'); // internal id of DO
		var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var BeginLocationName = request.getParameter('hdnBeginBinLocation');
		var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
		var EndLocation = request.getParameter('hdnEnteredLocation');
		var OrderLineNo = request.getParameter('hdnOrderLineNo');
		var getCortonNo = request.getParameter('hdncontlpno');
		var getresultscount= request.getParameter('hdnresultscount');
		var wmslocation= request.getParameter('hdnwmslocation');
		var lpno= request.getParameter('hdnLpNo');

		var name=request.getParameter('hdnname');
		var bulkqty=request.getParameter('hdnbulkqty');
		var tempItem=Item;
		var SOarray = new Array();
		var getLanguage =  request.getParameter('hdngetLanguage');

		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);

		//case # 20148098
		var st12,st13,st14;

		if( getLanguage == 'es_ES')
		{

			st12 = "CANTIDAD INV&#193;LIDA";
			st13  = "CANTIDAD SIN MENCI&#211;N";
			st14 = "SCANNED CANTIDAD MAYOR QUE EL PEDIDO CANTIDAD";

		}
		else
		{	
			st12 = "INVALID QUANTITY";
			st13 = "QTY NOT ENTERED";		
			st14 = "SCANNED QUANTITY GREATER THAN THE ORDER QTY";
		}

		SOarray["custparam_language"] = getLanguage;
		SOarray["custparam_error"] = st12;//'INVALID Quantity';
		SOarray["custparam_screenno"] = '27';
		SOarray["custparam_orderno"] = OrderNo;        
		SOarray["custparam_containerlpno"] = request.getParameter('hdnContainerLpNo');
		SOarray['custpage_multipleitemscan']=request.getParameter('hdnMultipleItemScaned');



		if(SOarray["custparam_item"] == null)
		{
			SOarray["custparam_item"] = Item;
		}
		else
		{
			SOarray["custparam_item"] = SOarray["custparam_item"] + "," +  Item;
		}

		nlapiLogExecution('DEBUG', 'Entered Item information-->', SOarray["custparam_item"]);   




		var vLoopCount = request.getParameter('hdnloopcount');

		nlapiLogExecution('DEBUG', 'vLoopCount-->',vLoopCount);

		nlapiLogExecution('DEBUG', 'Qty Information-->', SOarray["custparam_expectedquantity"]);



		if(parseFloat(vLoopCount)==1)
		{
			SOarray["custparam_expectedquantity"] = varQty;


			nlapiLogExecution('DEBUG', 'Inside If QTY information-->', SOarray["custparam_enterqty"]);   
		}
		else
		{
			SOarray["custparam_expectedquantity"] = SOarray["custparam_expectedquantity"] + "," +  varQty;



			nlapiLogExecution('DEBUG', 'Inside Else information-->',  SOarray["custparam_enterqty"]);
		}



		SOarray["custparam_expectedquantity"] = request.getParameter('hdnExpectedQuantity');

		SOarray["custparam_enteredqty"] =  request.getParameter('enterqty');
		SOarray["custparam_beginLocation"] = request.getParameter('hdnBeginLocationId');
		SOarray["custparam_item"] = request.getParameter('hdnItem');
		SOarray["custparam_itemdescription"] = request.getParameter('hdnItemDescription');
		SOarray["custparam_iteminternalid"] = request.getParameter('hdnItemInternalId');
		SOarray["custparam_dolineid"] = request.getParameter('hdnDOLineId');
		SOarray["custparam_invoicerefno"] = request.getParameter('hdnInvoiceRefNo');
		SOarray["custparam_beginlocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
		SOarray["custparam_orderlineno"] = request.getParameter('hdnOrderLineNo');
		SOarray["custparam_clusterno"] = request.getParameter('hdnClusterNo');
		SOarray["custparam_batchno"] = request.getParameter('hdnbatchno');

		SOarray["custparam_recordinternalid"] = RecordInternalId;                
		SOarray["custparam_linecount"] = request.getParameter('hdnlinecount');
		SOarray["custparam_loopcount"] = request.getParameter('hdnloopcount');
		SOarray["custparam_nextiteminternalid"] = request.getParameter('hdnnextitem');
		SOarray["custparam_contlpno"] = getCortonNo;
		SOarray['custparam_count']=getresultscount;
		SOarray['custparam_wmslocation']=wmslocation;
		SOarray['custparam_name']=name;
		SOarray['custparam_bulkqty']=bulkqty;
		SOarray['custparam_number']=request.getParameter('hdngetnumber');

		nlapiLogExecution('DEBUG', 'getresultscount', getresultscount);
		nlapiLogExecution('DEBUG', 'Line Count', SOarray["custparam_linecount"]);
		nlapiLogExecution('DEBUG', 'Loop Count', SOarray["custparam_loopcount"]);
		nlapiLogExecution('DEBUG', 'bulkqty',bulkqty);
		nlapiLogExecution('DEBUG', 'optExit',optExit);
		nlapiLogExecution('DEBUG', 'optedEvent',optedEvent);
		nlapiLogExecution('DEBUG', 'optENT',optENT);


		//code tocheck if the entered qty is greater than the expected qty.

		var varQty1 = request.getParameter('enterqty');

		/*if(parseFloat(varQty1)>parseFloat(ExpectedQuantity)){
			SOarray["custparam_error"] = "SCANNED QUANTITY GREATER THAN THE ORDER QTY11";//'Qty Not Entered';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			nlapiLogExecution('DEBUG', 'Error: ', 'Qty Not Entered');
			return;
		}

		 */

		//case# 20127682 starts (Invalid quantity issue)
		//if(bulkqty==null||bulkqty=="")
		//tempItem=varQty;
		//case# 20127682 end
		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept SO #.

		if (optedEvent == 'F7') {
			SOarray["custparam_nextiteminternalid"]='';
			//response.sendRedirect('SUITELET', 'customscript_rf_picking_container_no', 'customdeploy_rf_picking_container_no_di', false, SOarray);
			response.sendRedirect('SUITELET', 'customscript_rf_packing_item', 'customdeploy_rf_packing_item_di', false, SOarray);
		}
		else if(optENT=='ENT')
		{	
			//var currItem = validateSKU(Item, wmslocation, null);
			nlapiLogExecution('DEBUG', '!isNaN(varQty',!isNaN(varQty));
			if(varQty!=null && varQty!='')
			{
				//if user enter the sku instead of qty then validating the sku entered .
				var currItemArray = validateSKU(tempItem, wmslocation, null);
				var currItemflag=currItemArray[0];
				var currItem=currItemArray[1];
				nlapiLogExecution('DEBUG', 'currItemArray', currItemArray);
				nlapiLogExecution('DEBUG', 'currItem', currItem);
				nlapiLogExecution('DEBUG', 'currItemflag', currItemflag);
				var ItemCube='';
				var Baseuomqty='';

				if((currItem!=null && currItem!='')&&(currItem==SOarray["custparam_item"]) && !isNaN(varQty))
				{
					var ItemInternalid=eBiz_RF_GetItemForItemId(currItem,wmslocation);
					if(currItemflag=='F')
						var ItemInfo = eBiz_RF_GetItemCubeForItem(ItemInternalid);
					else
						var ItemInfo = eBiz_RF_GetItemCubeForItemAlias(ItemInternalid);
					nlapiLogExecution('DEBUG', 'ItemInfo[1]', ItemInfo[1]);
					Baseuomqty=ItemInfo[1];
					SOarray["custparam_number"] = parseFloat(getNumber) + parseFloat(Baseuomqty);
					getNumber=parseFloat(getNumber) + parseFloat(Baseuomqty);
					nlapiLogExecution('DEBUG', 'getNumber', getNumber);
					nlapiLogExecution('DEBUG', 'getItemQuantity', ExpectedQuantity);
					if ((parseFloat(getNumber)) <= parseFloat(ExpectedQuantity)) {
						response.sendRedirect('SUITELET', 'customscript_rf_packing_qty', 'customdeploy_rf_packing_qty_di', false, SOarray);
						return;
					}
					else
					{
						SOarray["custparam_number"] = parseFloat(getNumber) - parseFloat(Baseuomqty);

						SOarray["custparam_error"] = st14;//'Scanned quantiry greater than the Order qty';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						nlapiLogExecution('DEBUG', 'Error: ', 'Qty Not Entered');
						return;
					}
				}
				else
				{
					SOarray["custparam_error"] = st12;//'INVALID QTY';
					if(bulkqty=='' || bulkqty==null)
					{
						SOarray["custparam_error"]="INVALID ITEM";
					}
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					nlapiLogExecution('DEBUG', 'Error: ', 'Qty Not Entered');
					return;
				}
			}
			else
			{
				SOarray["custparam_error"] = st13;//'Qty Not Entered';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				nlapiLogExecution('DEBUG', 'Error: ', 'Qty Not Entered');
				return;
			}
		}
		else if(optExit=='EXIT')
		{
			nlapiLogExecution('DEBUG', 'optExit', optExit);
			if(bulkqty=='' || bulkqty==null )
				SOarray["custparam_expectedquantity"] = getNumber;
			else
			{
				nlapiLogExecution('DEBUG', 'test1', 'test1');
				SOarray["custparam_expectedquantity"] = request.getParameter('enterqty');
				var vEnteredQty = request.getParameter('enterqty');   
				UpdateConfirmDate(OrderNo, RecordInternalId, ContainerLPNo, vEnteredQty, BeginLocation, Item, ItemDescription, ItemInternalId, DoLineId, InvoiceRefNo, BeginLocationName, EndLocInternalId, EndLocation, OrderLineNo,getCortonNo);
			}

			response.sendRedirect('SUITELET', 'customscript_rf_packing_contlp', 'customdeploy_rf_packing_contlp_di', false, SOarray);

		}
		else
		{
			nlapiLogExecution('DEBUG', 'bulkqty new', bulkqty);
			if (ExpectedQuantity != '' && ExpectedQuantity != null) 
			{	
				var vEnteredQty = request.getParameter('enterqty');
				if(isNaN(vEnteredQty))
				{
					SOarray["custparam_error"] = st12;//'INVALID QTY';
					if(bulkqty=='' || bulkqty==null)
					{
						SOarray["custparam_error"]="INVALID ITEM";
					}

					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					nlapiLogExecution('DEBUG', 'Error: ', 'Qty Not Entered2222');
					return;
				}
				else if((vEnteredQty==''||vEnteredQty==null) && (bulkqty!='' && bulkqty!=null))// case# 201416364 validation should only in case of bulk qty
				{
					SOarray["custparam_error"] = st12;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					nlapiLogExecution('DEBUG', 'Error: ', 'Qty Not Entered11111111');
					return;
				}
				else if((vEnteredQty<=0) && (bulkqty!='' && bulkqty!=null))// case# 201416667
				{
					SOarray["custparam_error"] = "ENTERED QTY SHOULD NOT BE NEGATIVE/ZERO";
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					nlapiLogExecution('DEBUG', 'Error: ', 'ENTERED QTY SHOULD NOT BE NEGATIVE');
					return;
				}
				if(bulkqty=='' || bulkqty==null)
					vEnteredQty = getNumber;

				nlapiLogExecution('DEBUG', 'vEnteredQty', vEnteredQty);
//				case# 201416560
				if(parseFloat(vEnteredQty)>parseFloat(ExpectedQuantity)){
					SOarray["custparam_error"] = "SCANNED QUANTITY GREATER THAN THE ORDER QTY";//'Qty Not Entered';//201416677
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					nlapiLogExecution('DEBUG', 'Error: ', 'more Qty Not Entered');
					return;
				}


				SOarray['custpage_multipleitemscan']='True';

				UpdateConfirmDate(OrderNo, RecordInternalId, ContainerLPNo, vEnteredQty, BeginLocation, Item, ItemDescription, ItemInternalId, DoLineId, InvoiceRefNo, BeginLocationName, EndLocInternalId, EndLocation, OrderLineNo,getCortonNo);


				var loopcount=parseFloat(SOarray["custparam_loopcount"]);
				nlapiLogExecution('DEBUG', 'loopcount', loopcount);
				nlapiLogExecution('DEBUG', 'OrderNo', OrderNo);

				if(loopcount>=1)
				{	
					var SOFilters = new Array();
					if(OrderNo!=null && OrderNo!='')
						SOFilters.push(new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', OrderNo));

					if(getCortonNo!=null && getCortonNo!='')
						SOFilters.push(new nlobjSearchFilter( 'custrecord_container_lp_no',null, 'is', getCortonNo));

					SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//Status Flag - C (Picks confirmed)
					SOFilters.push(new nlobjSearchFilter('custrecord_pack_confirmed_date',null, 'isempty', null));

					SOFilters.push(new nlobjSearchFilter('custrecord_tasktype',null, 'anyof', [3]));	

					var SOColumns = new Array();
					SOColumns[0] = new nlobjSearchColumn('custrecord_lpno');
					SOColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
					SOColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
					SOColumns[3] = new nlobjSearchColumn('custrecord_sku');
					SOColumns[4] = new nlobjSearchColumn('custrecord_skudesc');
					SOColumns[5] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
					SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
					SOColumns[7] = new nlobjSearchColumn('custrecord_invref_no');
					SOColumns[8] = new nlobjSearchColumn('custrecord_line_no');
					SOColumns[9] = new nlobjSearchColumn('custrecord_actbeginloc');
					SOColumns[10] = new nlobjSearchColumn('custrecord_batch_no');	
					SOColumns[11] = new nlobjSearchColumn('custrecord_ebiz_order_no');
					SOColumns[12] = new nlobjSearchColumn('custrecord_container_lp_no');
					SOColumns[13] = new nlobjSearchColumn('custrecord_wms_location');
					SOColumns[3].setSort();

					var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns); 
					if(SOSearchResults==null || SOSearchResults=='')
					{
						SOarray["custparam_loopcount"] = 0;
						loopcount=parseFloat(SOarray["custparam_loopcount"]);
					}
					if(SOSearchResults!=null && SOSearchResults!='')
					{
						nlapiLogExecution('DEBUG', 'SOSearchResults', SOSearchResults.length);
						nlapiLogExecution('DEBUG', 'nextitem', SOSearchResults[0].getValue('custrecord_ebiz_sku_no'));
						SOarray["custparam_iteminternalid"] = SOSearchResults[0].getValue('custrecord_ebiz_sku_no');
						SOarray["custparam_nextlocation"] = SOSearchResults[0].getText('custrecord_actbeginloc');
						SOarray["custparam_loopcount"]=SOSearchResults.length;
					}


					if(loopcount > 0)
					{	
						response.sendRedirect('SUITELET', 'customscript_rf_packing_item', 'customdeploy_rf_packing_item_di', false, SOarray);
						return;
					}
					else
					{
						response.sendRedirect('SUITELET', 'customscript_rf_packing_contlp', 'customdeploy_rf_packing_contlp_di', false, SOarray);
						return;
						// response.sendRedirect('SUITELET', 'customscript_rf_packing_contlp', 'customdeploy_rf_packing_contlp_di', false, SOarray);
					}
				}
				else
				{
					response.sendRedirect('SUITELET', 'customscript_rf_packing_contlp', 'customdeploy_rf_packing_contlp_di', false, SOarray);
					return;
				}


			}
			else 
			{
				SOarray["custparam_error"] = st13;//'Qty Not Entered';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				nlapiLogExecution('DEBUG', 'Error: ', 'Qty Not Entered');
				return;
			}
		}
	}
}

/**
 * 
 * @param itemNo
 * @param location
 * @param company
 * @returns
 */
function validateSKU(itemNo, location, company){
	nlapiLogExecution('DEBUG', 'validateSKU', 'Start');

	var inputParams = 'Item = ' + itemNo + '<br>';
	inputParams = inputParams + 'Location = ' + location + '<br>';
	inputParams = inputParams + 'Company = ' + company;
	nlapiLogExecution('DEBUG', 'Input Parameters', inputParams);
	var currItemArray=new Array();
	currItemArray[0]='F';
	var currItem = eBiz_RF_GetItemForItemNo(itemNo);

	if(currItem == ""){
		currItem = eBiz_RF_GetItemBasedOnUPCCode(itemNo);
	}

	if(currItem == ""){
		currItem = eBiz_RF_GetItemFromSKUDims(itemNo, location);
		if(currItem != ""&&currItem !=null)
			currItemArray[0]='T';
	}

	var logMsg = "";
	if(currItem == ""){
		logMsg = 'Unable to retrieve item';
		currItem = null;
	} else {
		logMsg = 'Item = ' + currItem;
	}
	currItemArray[1]=currItem;
	nlapiLogExecution('DEBUG', 'Item Retrieved', logMsg);
	nlapiLogExecution('DEBUG', 'validateSKU', 'End');
//	return currItem;
	return currItemArray;
}

function eBiz_RF_GetItemForItemId(itemId,location){
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemForItemId', 'Start');

	var itemInternalId = null;

	if(itemId != null && itemId != ""){
		var logMsg = 'ItemId = |' + itemId + '|location|'+location;
		nlapiLogExecution('DEBUG', 'Input Parameters', logMsg);

		var filters = new Array();
//		filters.push(new nlobjSearchFilter('itemid', null, 'is', itemId));
		//changed on 30/4/12 by suman
		filters.push(new nlobjSearchFilter('nameinternal', null, 'is', itemId));
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		//End of changes as on 30/4/12.

		if(location!=null && location!='')
			filters.push(new nlobjSearchFilter('location', null, 'anyof', ['@NONE@',location]));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('itemid');
		columns[0].setSort(true);

		var itemSearchResults = nlapiSearchRecord('item', null, filters, columns);
		if(itemSearchResults != null && itemSearchResults.length > 0){
			itemInternalId = itemSearchResults[0].getId();
			nlapiLogExecution('DEBUG', 'Internal Id', itemInternalId);

		}
	}

	nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemForItemId', 'End');
	return itemInternalId;
}

function UpdateConfirmDate(OrderNo, RecordInternalId, ContainerLPNo, ExpectedQuantity, BeginLocation, Item, ItemDescription, ItemInternalId, DoLineId, InvoiceRefNo, BeginLocationName, EndLocInternalId, EndLocation, OrderLineNo,lpno)
{
	var now = new Date();
	var lineCnt = request.getLineItemCount('custpage_items');

	nlapiLogExecution('DEBUG', "Line Count" + lineCnt);
	var invtfalg = "";
	var ShortPick = "";
	var invnotfound = "";

	//getting time

	var a_p = "";
	var d = new Date();
	var curr_hour = now.getHours();
	if (curr_hour < 12) {
		a_p = "am";
	}
	else {
		a_p = "pm";
	}
	if (curr_hour == 0) {
		curr_hour = 12;
	}
	if (curr_hour > 12) {
		curr_hour = curr_hour - 12;
	}

	var curr_min = now.getMinutes();

	curr_min = curr_min + "";

	if (curr_min.length == 1) 
	{
		curr_min = "0" + curr_min;
	}
	nlapiLogExecution('DEBUG', "RecordInternalId" , RecordInternalId);
	var CurrentDate = DateStamp();
	var OpenTaskTransaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordInternalId);

	nlapiLogExecution('DEBUG', 'Current Date', CurrentDate);
	nlapiLogExecution('DEBUG', 'Record Internal ID', RecordInternalId);
	nlapiLogExecution('DEBUG', 'Container LP No', ContainerLPNo);
	nlapiLogExecution('DEBUG', 'lpno', lpno);

	var isItLastPick=OpenTaskTransaction.getFieldValue('custrecord_device_upload_flag');
	var TotalWeight=OpenTaskTransaction.getFieldValue('custrecord_total_weight');
	var TotalCube=OpenTaskTransaction.getFieldValue('custrecord_totalcube');
	nlapiLogExecution('DEBUG', 'TotalWeight', TotalWeight);
	nlapiLogExecution('DEBUG', 'TotalCube', TotalCube);
	nlapiLogExecution('DEBUG', 'ExpectedQuantity', ExpectedQuantity);
	var Totweightupdate=(parseFloat(TotalWeight)/parseFloat(TotalCube))*parseFloat(ExpectedQuantity);
	try
	{	    
		OpenTaskTransaction.setFieldValue('custrecord_wms_status_flag', '28');//Statusflag='C'
		OpenTaskTransaction.setFieldValue('custrecord_act_qty', parseFloat(ExpectedQuantity).toFixed(4));
		OpenTaskTransaction.setFieldValue('custrecordact_end_date', DateStamp());
		OpenTaskTransaction.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
		//OpenTaskTransaction.setFieldValue('custrecord_actendloc', EndLocInternalId);
		OpenTaskTransaction.setFieldValue('custrecord_pack_confirmed_date', CurrentDate);
		OpenTaskTransaction.setFieldValue('custrecord_container_lp_no', ContainerLPNo);
		OpenTaskTransaction.setFieldValue('custrecord_total_weight', parseFloat(Totweightupdate).toFixed(4));
		OpenTaskTransaction.setFieldValue('custrecord_totalcube', parseFloat(ExpectedQuantity).toFixed(4));    
		OpenTaskTransaction.setFieldValue('custrecord_notes','T');
		nlapiSubmitRecord(OpenTaskTransaction, false, true);

		var OpenTaskitemFilters = new Array();	

		//OpenTaskitemFilters.push(new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', OrderNo));
		OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_container_lp_no',null, 'is', ContainerLPNo));   	 	
		OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_pack_confirmed_date',null, 'isnotempty', null));
		OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [28]));//pack complete
		OpenTaskitemFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//pick
		var OpenTaskitemColumns = new Array();
		OpenTaskitemColumns[0] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		OpenTaskitemColumns[1] = new nlobjSearchColumn('custrecord_act_qty');
		OpenTaskitemColumns[2] = new nlobjSearchColumn('custrecord_expe_qty');
		OpenTaskitemColumns[3] = new nlobjSearchColumn('name');
		OpenTaskitemColumns[4] = new nlobjSearchColumn('custrecord_totalcube');
		OpenTaskitemColumns[5] = new nlobjSearchColumn('custrecord_total_weight');
		var  k=0;var Recid;
		var OpenTaskitemSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, OpenTaskitemFilters, OpenTaskitemColumns);
		if(OpenTaskitemSearchResults!=null && OpenTaskitemSearchResults!='')
		{
			nlapiLogExecution('DEBUG','OpenTaskitemSearchResults.length', OpenTaskitemSearchResults.length);
			for(var i=0;i<OpenTaskitemSearchResults.length;i++)
			{
				iteminternalid=OpenTaskitemSearchResults[i].getValue('custrecord_ebiz_sku_no');
				getExpQty = OpenTaskitemSearchResults[i].getValue('custrecord_expe_qty'); //Assign the Enter Qty here imp

				Recid= OpenTaskitemSearchResults[i].getId();
				//getExpQty
				fetchQty =OpenTaskitemSearchResults[i].getValue('custrecord_act_qty');
				contlpno = OpenTaskitemSearchResults[i].getValue('custrecord_container_lp_no');
				wmslocation = OpenTaskitemSearchResults[i].getValue('custrecord_wms_location');
				opentaskwt	= OpenTaskitemSearchResults[i].getValue('custrecord_total_weight');
				opentaskcube = OpenTaskitemSearchResults[i].getValue('custrecord_totalcube');
				varname = OpenTaskitemSearchResults[i].getValue('name');

				var skuname = OpenTaskitemSearchResults[i].getText('custrecord_sku');

				nlapiLogExecution('DEBUG', 'skuname::', skuname);
				nlapiLogExecution('DEBUG', 'getExpQty::', getExpQty);
				nlapiLogExecution('DEBUG', 'fetchQty::', fetchQty);
				nlapiLogExecution('DEBUG', 'contlpno::', contlpno);
				nlapiLogExecution('DEBUG', 'wmslocation::', wmslocation);
				nlapiLogExecution('DEBUG', 'opentaskwt', opentaskwt);
				nlapiLogExecution('DEBUG', 'opentaskcube', opentaskcube);

				var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', Recid);								    	
				transaction.setFieldValue('custrecord_wms_status_flag', '28');//28 stands for packcomplete //Added on 24th
				transaction.setFieldValue('custrecord_container_lp_no', ContainerLPNo);
				transaction.setFieldValue('custrecord_expe_qty', parseFloat(fetchQty).toFixed(4));
				nlapiSubmitRecord(transaction, false, true);


				var diffqty=parseFloat(getExpQty)-parseFloat(fetchQty);
				nlapiLogExecution('DEBUG', 'diffqty11', diffqty);

				var remqty=(parseFloat(opentaskwt)/parseFloat(opentaskcube))*parseFloat(diffqty);
				var OpenTaskitemFilters1 = new Array();	
				OpenTaskitemFilters1.push(new nlobjSearchFilter('custrecord_container_lp_no',null, 'is', ContainerLPNo));   	 	
				
				OpenTaskitemFilters1.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [30]));//short pick exist
				//OpenTaskitemFilters1.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//pick
				var OpenTaskitemColumns1 = new Array();
				OpenTaskitemColumns1[0] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
				OpenTaskitemColumns1[1] = new nlobjSearchColumn('custrecord_act_qty');
				OpenTaskitemColumns1[2] = new nlobjSearchColumn('custrecord_expe_qty');
				OpenTaskitemColumns1[3] = new nlobjSearchColumn('name');
				OpenTaskitemColumns1[4] = new nlobjSearchColumn('custrecord_totalcube');
				OpenTaskitemColumns1[5] = new nlobjSearchColumn('custrecord_total_weight');
				
				var OpenTaskitemSearchResults1 = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, OpenTaskitemFilters1, OpenTaskitemColumns1);
				var expQty1=0;
				if(OpenTaskitemSearchResults1!=null && OpenTaskitemSearchResults1!='' && i==0)
				{
					for (var j=0;j<OpenTaskitemSearchResults1.length;j++){
					var wmsstatus= OpenTaskitemSearchResults1[j].getValue('custrecord_wms_status_flag');
					expQty1 = OpenTaskitemSearchResults1[j].getValue('custrecord_expe_qty');
					nlapiLogExecution('DEBUG', 'wmsstatus111', wmsstatus);
					nlapiLogExecution('DEBUG', 'expQty1', expQty1);
					}
					
				}
				if(diffqty==expQty1){
					var Remainqtyrecord =  nlapiLoadRecord('customrecord_ebiznet_trn_opentask',Recid);	
					Remainqtyrecord.setFieldValue('name', varname);
					nlapiSubmitRecord(Remainqtyrecord, false, true);
				}
				else if(diffqty!='' && diffqty> 0)
				{
					diffqty=diffqty-expQty1;
					nlapiLogExecution('DEBUG', 'diffqty11111', diffqty);
					var Remainqtyrecord =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',Recid);	
					Remainqtyrecord.setFieldValue('custrecord_act_qty', parseFloat(diffqty).toFixed(4)); 
					Remainqtyrecord.setFieldValue('custrecord_expe_qty', parseFloat(diffqty).toFixed(4)); 
					Remainqtyrecord.setFieldValue('custrecord_wms_status_flag', 8);
					Remainqtyrecord.setFieldValue('name', varname);
					Remainqtyrecord.setFieldValue('custrecord_container_lp_no', ContainerLPNo);
					Remainqtyrecord.setFieldValue('custrecord_pack_confirmed_date', null);
					Remainqtyrecord.setFieldValue('custrecord_total_weight', parseFloat(remqty).toFixed(4));
					Remainqtyrecord.setFieldValue('custrecord_totalcube', parseFloat(diffqty).toFixed(4));
					Remainqtyrecord.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', null);
					var rempick = nlapiSubmitRecord(Remainqtyrecord, false, true);
				}

			}

		}



		// if(isItLastPick=='T')
		// CreatePACKTask(null,null,OrderNo,OrderNo,ContainerLPNo,14,BeginLocation,EndLocation,28,DoLineId,ItemInternalId);//14 - Task Type - PACK
	}
	catch (e) 
	{
		if (e instanceof nlobjError) 
			nlapiLogExecution('DEBUG', 'system error', e.getCode() + '\n' + e.getDetails());
		else 
			nlapiLogExecution('DEBUG', 'unexpected error', e.toString());
	}
}

function eBiz_RF_GetItemCubeForItemAlias(itemID){
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemCubeForItem', 'Start');

	var BaseUOM=eBiz_RF_GetUOMAgainstItemAlias(itemID);
	var ItemInfo=new Array();
	var itemCube = 0;
	var BaseUomQty=0;
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemID);
	filters[1] = new nlobjSearchFilter('custrecord_ebizuomskudim', null, 'anyof', BaseUOM);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
	columns[1] = new nlobjSearchColumn('custrecord_ebizqty');

	var itemDimSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

	if(itemDimSearchResults != null && itemDimSearchResults.length > 0){
		itemCube = itemDimSearchResults[0].getValue('custrecord_ebizcube');
		//code added on 13 feb 2012 by suman
		//To get the baseuom qty .
		BaseUomQty=itemDimSearchResults[0].getValue('custrecord_ebizqty');
		//end of code as of 13 feb 2012.
	}
	ItemInfo.push(itemCube);
	ItemInfo.push(BaseUomQty);
	nlapiLogExecution('DEBUG', 'Retrieved Item Cube', 'Item Cube = ' + itemCube);
	nlapiLogExecution('DEBUG', 'Retrieved BaseUomQty', 'BaseUomQty = ' + BaseUomQty);
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemCubeForItem', 'End');

	return ItemInfo;
}

function eBiz_RF_GetUOMAgainstItemAlias(itemID)
{
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetUOMAgainstItemAlias', 'Start');

	var baseuom = "";

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_item', null, 'anyof', itemID));
	/*if(location!=null && location!='')
		filters.push(new nlobjSearchFilter('location', null, 'anyof', ['@NONE@',location]));*/

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_uom');

	var itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, filters, columns);

	if(itemSearchResults != null)
		baseuom = itemSearchResults[0].getValue('custrecord_ebiz_uom');

	var logMsg = 'baseuom = ' + baseuom;
	nlapiLogExecution('DEBUG', 'baseuom Retrieved', logMsg);
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetUOMAgainstItemAlias', 'End');

	return baseuom;
}