/***************************************************************************
 eBizNET Solutions              
 ***************************************************************************
 **     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_FastPickingSerialNo.js,v $
 *     	   $Revision: 1.1.4.11.2.1 $
 *     	   $Date: 2015/11/10 16:39:16 $
 *     	   $Author: sponnaganti $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_FastPickingSerialNo.js,v $
 * Revision 1.1.4.11.2.1  2015/11/10 16:39:16  sponnaganti
 * case# 201415562
 * 2015.2 issue fix
 *
 * Revision 1.1.4.11  2015/08/31 15:51:05  aanchal
 * 2015.2 issue fixes
 * 201414168
 *
 * Revision 1.1.4.10  2015/07/14 15:25:53  grao
 * 2015.2 ssue fixes  201412916
 *
 * Revision 1.1.4.9  2015/06/29 15:38:18  skreddy
 * Case# 201413266,201413264
 * sighwarehouse SB issue fix
 *
 * Revision 1.1.4.8  2014/11/12 15:34:44  schepuri
 * 201411045�  issue fix
 *
 * Revision 1.1.4.7  2014/10/22 14:10:50  schepuri
 * 201410794 issue fix
 *
 * Revision 1.1.4.6  2014/07/11 15:03:22  rmukkera
 * Case # 20149416�
 *
 * Revision 1.1.4.5  2014/06/13 13:15:13  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.4.4  2014/06/06 08:19:37  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.1.4.3  2014/05/30 00:41:01  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.4.2  2014/01/07 09:37:54  rmukkera
 * Case # 20126621
 *
 * Revision 1.1.2.2  2014/01/07 08:07:18  rmukkera
 * Case # 20126621
 *
 * Revision 1.1.2.1  2013/12/20 15:37:06  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF past picking
 *
 * Revision 1.6.4.9.4.5.2.19  2013/06/25 10:07:08  gkalla
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fix
 *
 * Revision 1.6.4.9.4.5.2.18  2013/06/24 15:13:35  gkalla
 * CASE201112/CR201113/LOG201121
 * As part of Standard Bundle case# 20122757,20123135,20123137
 *
 * Revision 1.6.4.9.4.5.2.17  2013/06/19 22:56:07  gkalla
 * CASE201112/CR201113/LOG201121
 * As part of Demo issue
 *
 * Revision 1.6.4.9.4.5.2.16  2013/06/19 15:30:26  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * pickingserialno
 *
 * Revision 1.6.4.9.4.5.2.15  2013/06/17 14:07:05  skreddy
 * CASE201112/CR201113/LOG201121
 * issue rellated to serial numbers
 *
 * Revision 1.6.4.9.4.5.2.14  2013/06/11 14:30:20  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.6.4.9.4.5.2.13  2013/06/10 06:12:36  skreddy
 * CASE201112/CR201113/LOG201121
 * RF order picking for serial Item
 *
 * Revision 1.6.4.9.4.5.2.12  2013/06/05 12:02:07  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.6.4.9.4.5.2.11  2013/05/13 15:08:06  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle Issue Fixes
 *
 * Revision 1.6.4.9.4.5.2.10  2013/05/07 15:37:53  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle Issue Fixes
 *
 * Revision 1.6.4.9.4.5.2.9  2013/04/26 15:49:51  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fixes
 *
 * Revision 1.6.4.9.4.5.2.8  2013/04/24 15:54:26  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fixes
 *
 * Revision 1.6.4.9.4.5.2.7  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.6.4.9.4.5.2.6  2013/04/15 14:59:41  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.6.4.9.4.5.2.5  2013/04/12 11:04:06  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.6.4.9.4.5.2.4  2013/04/10 15:49:39  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related to accepting invalid serial no
 *
 * Revision 1.6.4.9.4.5.2.3  2013/04/02 14:15:44  rmukkera
 * issue Fix related to vaidating the serailno
 *
 * Revision 1.6.4.9.4.5.2.2  2013/03/22 11:44:30  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.6.4.9.4.5.2.1  2013/03/18 06:45:56  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.6.4.9.4.5  2013/02/06 14:38:20  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.6.4.9.4.4  2012/11/27 17:56:03  spendyala
 * CASE201112/CR201113/LOG201121
 * Serial Capturing file.
 *
 * Revision 1.6.4.9.4.3  2012/11/01 14:55:23  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.6.4.9.4.2  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.6.4.9.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.6.4.9  2012/09/03 13:52:07  schepuri
 * CASE201112/CR201113/LOG201121
 * added date stamp
 *
 * Revision 1.6.4.8  2012/06/18 07:37:13  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT and Prod issue fixes
 *
 * Revision 1.6.4.7  2012/06/11 13:45:52  spendyala
 * CASE201112/CR201113/LOG201121
 * passing parameters to query string was missing.
 *
 * Revision 1.6.4.6  2012/05/28 09:37:11  gkalla
 * CASE201112/CR201113/LOG201121
 * Issue fix for QC Check
 *
 * Revision 1.6.4.5  2012/04/18 14:27:31  mbpragada
 * CASE201112/CR201113/LOG201121
 * added Skip Location functionality
 *
 * Revision 1.6.4.4  2012/04/17 10:52:40  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Packing
 *
 * Revision 1.6.4.3  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.6.4.2  2012/02/08 15:27:30  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * functional keys
 *
 * Revision 1.6.4.1  2012/02/08 12:35:51  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge to brach
 *
 * Revision 1.6  2011/10/04 14:38:51  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * TO Functionality New Files
 *
 * Revision 1.5  2011/10/03 14:41:35  snimmakayala
 * CASE201112/CR201113/LOG20112
 * Error Screen added for Serial# wrong entry
 *
 * Revision 1.4  2011/09/30 14:31:51  gkalla
 * CASE201112/CR201113/LOG201121
 * For BOM Report in Work order screen
 *
 * Revision 1.3  2011/09/29 16:17:23  gkalla
 * CASE201112/CR201113/LOG201121
 * To integrate Serial no entry in RF outbound
 *
 * Revision 1.2  2011/09/10 14:21:13  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.1  2011/09/10 07:20:05  skota
 * CASE201112/CR201113/LOG201121
 * New suitelet to capture serial nos during outbound
 *
 * 
 *****************************************************************************/
function PickSerial(request, response){
	if (request.getMethod() == 'GET') {
		nlapiLogExecution('DEBUG', 'Into CheckinSerial');
		
		var fastpick = request.getParameter('custparam_fastpick');

		nlapiLogExecution('ERROR', 'fastpick', fastpick);


		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4;

		if( getLanguage == 'es_ES')
		{
			st1 = "ENTRAR EN SERIE NO : ";
			st2 = "PADRE ID : ";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";

		}
		else
		{
			st1 = "ENTER SERIAL NO: ";
			st2 = "PARENT ID : ";
			st3 = "SEND ";
			st4 = "PREV";

		}

		var getWaveno = request.getParameter('custparam_waveno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocationId = request.getParameter('custparam_beginLocation');
		//        var getItem = request.getParameter('custparam_item');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');
		var vClusterNo = request.getParameter('custparam_clusterno');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var getItemType = request.getParameter('custparam_RecType');
		var getSerialOut = request.getParameter('custparam_SerOut');
		var getSerialIn = request.getParameter('custparam_SerIn');
		var getOrdName = request.getParameter('name');
		var getEbizOrd = request.getParameter('custparam_ebizordno');
		nlapiLogExecution('DEBUG', 'getEbizSO', getEbizOrd);
		var getEnteredLocation = request.getParameter('custparam_endlocation');
		var getBeginBinLocation = request.getParameter('custparam_beginlocationname');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var Itype = nlapiLookupField('item', getItemInternalId, 'recordType');
		var ItemRec = nlapiLoadRecord(Itype, getItemInternalId);
		var whLocation = request.getParameter('custparam_whlocation');
		var vBatchno = request.getParameter('custparam_batchno');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var name=request.getParameter('name');
		var ContainerSize=request.getParameter('custparam_containersize');
		var getOrderNo=request.getParameter('custparam_ebizordno');
		var detailTask=request.getParameter('custparam_detailtask');
		var getEnteredContainerNo=request.getParameter('custparam_newcontainerlp');
		var pickType=request.getParameter('custparam_picktype');
		var RecordCount=request.getParameter('custparam_nooflocrecords');
		
		var EntLoc = request.getParameter('custparam_EntLoc');
		var EntLocRec=request.getParameter('custparam_EntLocRec');
		var EntLocID=request.getParameter('custparam_EntLocId');
		var ExceptionFlag = request.getParameter('custparam_Exc');
		var EntLotID=request.getParameter('custparam_entLotId');


		var getItem = ItemRec.getFieldValue('itemid');
		nlapiLogExecution('DEBUG', 'Location Name is', getItem);
		nlapiLogExecution('DEBUG', 'detailTask  is', detailTask);
		nlapiLogExecution('DEBUG', 'pickType  is', pickType);

		nlapiLogExecution('DEBUG', 'getRecordInternalId', getRecordInternalId);
		var NextRecordId=request.getParameter('custparam_nextrecordid');
		nlapiLogExecution('DEBUG', 'NextRecordId', NextRecordId);
		var getContainerLPNo='';
		var getParentLPNo='';
		if(pickType!='CL')
		{
		var SORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', getRecordInternalId);
			//getContainerLPNo= SORec.getFieldValue('custrecord_lpno');
			getContainerLPNo= SORec.getFieldValue('custrecord_container_lp_no');
			getParentLPNo =  SORec.getFieldValue('custrecord_lpno');
		}
		else
		{


			var SOFilters = new Array();

			SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
			SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK

			if(getWaveno != null && getWaveno != "")
			{
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(getWaveno)));
			}

			if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
			{
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo.toString()));
			}

			if(getItemInternalId != null && getItemInternalId != "")
			{
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_sku_no', null, 'is', getItemInternalId));
			}

			if(getBeginBinLocation != null && getBeginBinLocation != "")
			{
				SOFilters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', getBeginLocationId));
			}

			var SOColumns = new Array();
			//Case # 20126621 Start
			SOColumns[0] = new nlobjSearchColumn('custrecord_sku',null,'group');
			//Case # 20126621 End
			SOColumns[1] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
			SOColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
			SOColumns[3] = new nlobjSearchColumn('custrecord_skudesc',null,'group');
			SOColumns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');					
			SOColumns[5] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
			SOColumns[6] = new nlobjSearchColumn('custrecord_comp_id',null,'group');
			SOColumns[7] = new nlobjSearchColumn('custrecord_container_lp_no',null,'group');
			SOColumns[8] = new nlobjSearchColumn('custrecord_container',null,'group');
			SOColumns[9] = new nlobjSearchColumn('custrecord_from_lp_no',null,'group');
			SOColumns[10] = new nlobjSearchColumn('custrecord_container_lp_no',null,'group');

			SOColumns[0].setSort();	//SKU
			SOColumns[2].setSort();	//Location
			SOColumns[7].setSort();//Container LP

			var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
			if(SOSearchResults!=null && SOSearchResults!='')
			{
				//getContainerLPNo= SOSearchResults[0].getValue('custrecord_from_lp_no',null,'group');
				getContainerLPNo= SOSearchResults[0].getValue('custrecord_container_lp_no',null,'group');
				getParentLPNo = SOSearchResults[0].getValue('custrecord_from_lp_no',null,'group');

			}

		}

		var getNumber;
		if(request.getParameter('custparam_number')!= null && request.getParameter('custparam_number') != "")
			getNumber = request.getParameter('custparam_number');
		else
			getNumber=0;
		nlapiLogExecution('ERROR', 'getNumber00', getNumber);
		var getLPNo;
		if(request.getParameter('custrecord_from_lp_no')!= null && request.getParameter('custrecord_from_lp_no') != "")
			getLPNo = request.getParameter('custrecord_from_lp_no');

		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		nlapiLogExecution('DEBUG', 'NextItemInternalId', NextItemInternalId);
		//var pickType=request.getParameter('custparam_picktype');
		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');
		nlapiLogExecution('ERROR', 'getContainerLPNo', getContainerLPNo);
		nlapiLogExecution('ERROR', 'getParentLPNo', getParentLPNo);
		
		var getnextExpectedQuantity = request.getParameter('custparam_nextexpectedquantity');
		var getRemQty = request.getParameter('custparam_remqty');
		var getPickQty=request.getParameter('custparam_enteredqty');
		var functionkeyHtml=getFunctionkeyScript('_rf_checkin_lp'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends     
		//html = html + " document.getElementById('enterserialno').focus();";        



		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";



		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_checkin_lp' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		//  html = html + "				<td align = 'left'>UOM ID : <label>" + getUOMIDText + "</label>";
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";
		html = html + "				<input type='hidden' name='hdnFetchedContainerLPNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";		
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLPNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEnteredLocation' value=" + getEnteredLocation + ">";
		html = html + "				<input type='hidden' name='hdnBeginBinLocation' value=" + getBeginBinLocation + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdngetnumber' value=" + getNumber + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vClusterNo + ">";
		html = html + "				<input type='hidden' name='hdnItemType' value=" + getItemType + ">";
		html = html + "				<input type='hidden' name='hdnSerOut' value=" + getSerialOut + ">";
		html = html + "				<input type='hidden' name='hdnSerIn' value=" + getSerialIn + ">";
		html = html + "				<input type='hidden' name='hdnOrdName' value=" + getOrdName + ">";
		html = html + "				<input type='hidden' name='hdnEbizOrdNo' value=" + getEbizOrd + ">";
		html = html + "				<input type='hidden' name='hdnFromLp' value=" + getLPNo + ">";	
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value='" + vBatchno + "'>";
		html = html + "				<input type='hidden' name='hdnNextLocation' value=" + NextLocation + ">";
		html = html + "				<input type='hidden' name='hdnName' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + ContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnebizOrdNo' value=" + getOrderNo + ">";
		html = html + "				<input type='hidden' name='hdnNextItemId' value=" + NextItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				<input type='hidden' name='hdnextExpectedQuantity' value=" + getnextExpectedQuantity + ">";	
		html = html + "				<input type='hidden' name='hdnActQty' value=" + getPickQty + ">";
		html = html + "				<input type='hidden' name='hdnremqty' value=" + getRemQty + ">";
		html = html + "				<input type='hidden' name='hdnEntLoc' value=" + EntLoc + ">";
		html = html + "				<input type='hidden' name='hdnEntLocRec' value=" + EntLocRec + ">";
		html = html + "				<input type='hidden' name='hdnEntLocID' value=" + EntLocID + ">";
		html = html + "				<input type='hidden' name='hdnExceptionFlag' value=" + ExceptionFlag + ">";
		html = html + "				<input type='hidden' name='hdnEntLotID' value=" + EntLotID + ">";
		html = html + "				<input type='hidden' name='hdndetailTask' value=" + detailTask + ">";
		html = html + "				<input type='hidden' name='hdnfastpick' value=" + fastpick + ">";

		html = html + "				<input type='hidden' name='hdnParentLP' value=" + getParentLPNo + ">";
		html = html + "				<input type='hidden' name='hdnNextrecordid' value=" + NextRecordId + ">";

		html = html + "				</td>";
		html = html + "			</tr>";
		if(getPickQty != null && getPickQty !='' && !isNaN(getPickQty))
		{
		html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + (parseFloat(getNumber) + 1) + " OF <label>" + getPickQty + "</label>";
			html = html + "			</tr>";
		}
		else
		{
			html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + (parseFloat(getNumber) + 1) + " OF <label>" + getExpectedQuantity + "</label>";
		html = html + "			</tr>";
		}
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +"";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterserialno' id='enterserialno' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'>"+ st2 +"<label>" + getContainerLPNo + "</label>";
		html = html + "				<td align = 'left'>"+ st2 +"<label>" + getParentLPNo + "</label>";		
		html = html + "			</tr>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'>"+ st2 +"<input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "				<td align = 'left'>"+ st3 +"<input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterserialno').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');
		var fastpick = request.getParameter('hdnfastpick');

		nlapiLogExecution('ERROR', 'fastpick', fastpick);

		var getLanguage = request.getParameter('hdngetLanguage');
		var detailTask=request.getParameter('hdndetailTask');
		var SOarray = new Array();
		var st1,st5,st6,st7;

		if( getLanguage == 'es_ES')
		{
			st1 = "ENTRAR EN SERIE NO : ";
			st5 = "SERIAL # YA ESCANEADOS";
			st6 = "SERIAL # NO EXISTE";
			st7 = "SERIAL # YA EXISTE";

		}
		else
		{
			st1 = "ENTER SERIAL NO: ";
			st5 = "SERIAL # ALREADY SCANNED";
			st6 = "INVALID SERIAL NO";
			st7 = "SERIAL # ALREADY EXISTS";
		}


		SOarray["custparam_language"] = getLanguage;
		var RecCount=request.getParameter('hdnRecCount');
		SOarray["custparam_detailtask"] = request.getParameter('hdndetailTask');
		SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
		SOarray["custparam_recordinternalid"] = request.getParameter('hdnRecordInternalId');
		SOarray["custparam_containerlpno"] = request.getParameter('hdnContainerLpNo');
		SOarray["custparam_newcontainerlp"] = request.getParameter('hdnEnteredContainerLPNo');
		SOarray["custparam_expectedquantity"] = request.getParameter('hdnExpectedQuantity');
		SOarray["custparam_beginLocation"] = request.getParameter('hdnBeginLocationId');
		SOarray["custparam_item"] = request.getParameter('hdnItem');
		SOarray["custparam_itemdescription"] = request.getParameter('hdnItemDescription');
		SOarray["custparam_iteminternalid"] = request.getParameter('hdnItemInternalId');
		SOarray["custparam_dolineid"] = request.getParameter('hdnDOLineId');
		SOarray["custparam_invoicerefno"] = request.getParameter('hdnInvoiceRefNo');
		SOarray["custparam_beginlocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
		SOarray["custparam_orderlineno"] = request.getParameter('hdnOrderLineNo');
		SOarray["custparam_clusterno"] = request.getParameter('hdnClusterNo');
		SOarray["custparam_noofrecords"] = RecCount;
		SOarray["custparam_batchno"] = request.getParameter('hdnbatchno');
		SOarray["custparam_nextlocation"] = request.getParameter('hdnNextLocation');
		SOarray["name"] = request.getParameter('hdnName');
		SOarray["custparam_containersize"] = request.getParameter('hdnContainerSize');
		SOarray["custparam_ebizordno"] = request.getParameter('hdnebizOrdNo');
		SOarray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
		SOarray["custparam_RecType"] = request.getParameter('hdnItemType');
		SOarray["custparam_fastpick"] = fastpick;
		SOarray["custparam_nooflocrecords"] = request.getParameter('hdnRecCount');
		
		SOarray["custparam_nextrecordid"]=request.getParameter('hdnNextrecordid');
		
		var getOrdName=request.getParameter('hdnOrdName');
		var getEbizOrd=request.getParameter('hdnEbizOrdNo');
		var getSerialNo = request.getParameter('enterserialno');
		var getNumber = request.getParameter('hdngetnumber');
		var getFromLP= request.getParameter('hdnFromLp'); 
		var getOrdLineNo = request.getParameter('hdnOrderLineNo');
		var getContainerLP = request.getParameter('hdnContainerLpNo');
		var vSkipId=request.getParameter('hdnskipid');
		var getParentLPNo=request.getParameter('hdnParentLP');

		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');
		SOarray["custparam_serialno"] = request.getParameter('custparam_serialno');
		SOarray["custparam_number"] = parseFloat(request.getParameter('custparam_number'));
		SOarray["custparam_nextexpectedquantity"] = request.getParameter('hdnextExpectedQuantity');
		var getPickQty = request.getParameter('hdnActQty');
		var getItemQuantity;
		if(getPickQty != null && getPickQty !='' && !isNaN(getPickQty))
			getItemQuantity =request.getParameter('hdnActQty');
		else
			getItemQuantity =request.getParameter('hdnExpectedQuantity');

		nlapiLogExecution('DEBUG', 'SOarray["custparam_nextrecordid"]', SOarray["custparam_nextrecordid"]);
		nlapiLogExecution('DEBUG', 'getItemQuantity', getItemQuantity);
		var getItemType =request.getParameter('hdnItemType');		
		var getSerOut =request.getParameter('hdnSerOut');
		var getSerIn =request.getParameter('hdnSerIn');
		var getItemId =request.getParameter('hdnItemInternalId');

		SOarray["custparam_nextiteminternalid"] = request.getParameter('hdnNextItemId');
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');

		SOarray["name"] = getOrdName;
		SOarray["custparam_ebizordno"] = getEbizOrd;
		SOarray["custrecord_from_lp_no"] = getNumber;
		SOarray["custparam_SerIn"] = getSerIn;
		SOarray["custparam_enteredQty"] = request.getParameter('hdnActQty');
		SOarray["custparam_enteredqty"] = request.getParameter('hdnActQty');
		SOarray["custparam_remqty"] = request.getParameter('hdnremqty');
		SOarray["custparam_SerOut"] = getSerOut;
		
		var EntLoc = request.getParameter('hdnEntLoc');
		var EntLocRec = request.getParameter('hdnEntLocRec');
		var EntLocID = request.getParameter('hdnEntLocID');
		var ExceptionFlag = request.getParameter('hdnExceptionFlag');
		var EntLotID = request.getParameter('hdnEntLotID');
	
		SOarray["custparam_EntLoc"] = request.getParameter('hdnEntLoc');
		SOarray["custparam_EntLocRec"] = request.getParameter('hdnEntLocRec');
		SOarray["custparam_EntLocId"] = request.getParameter('hdnEntLocID');
		SOarray["custparam_Exc"] = request.getParameter('hdnExceptionFlag');
		SOarray["custparam_entLotId"] = request.getParameter('hdnEntLotID');
		
		

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');
		var TempSerialNoArray = new Array();
		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to RF Main Menu.
		if (optedEvent == 'F7') {
			nlapiLogExecution('DEBUG', 'hdnpicktype',request.getParameter('hdnpicktype'));
			//201413266,201413264  start
			nlapiLogExecution('DEBUG', 'number',request.getParameter('custparam_number'));
			if(TempSerialNoArray !=null && TempSerialNoArray !='' && TempSerialNoArray.length>0)
			{


				SOarray["custparam_number"] = parseFloat(TempSerialNoArray.length);
				nlapiLogExecution('DEBUG', 'SOarray["custparam_number"]',SOarray["custparam_number"]);
			}
			//201413266,201413264  end
			var vPickType = "";
			if(request.getParameter('hdnpicktype') != null && request.getParameter('hdnpicktype') != "")
			{
				vPickType = request.getParameter('hdnpicktype');
			}
			if(vPickType == "CL")
			{
				response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_summtask', 'customdeploy_rf_cluspicking_summtask', false, SOarray);						
			}
			else
			{
				//case # 20149416�start
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pickingcarton', 'customdeploy_ebiz_rf_pickingcarton', false, SOarray);
				//	response.sendRedirect('SUITELET', 'customscript_rf_picking_container_no', 'customdeploy_rf_picking_container_no_di', false, SOarray);
					//case # 20149416�end
			}
		}
		//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
		//            if (optedEvent != '' && optedEvent != null) {
		else 
			if (getSerialNo == "") {
				//	if the 'Send' button is clicked without any option value entered,
				//  it has to show an error message. The next screen to which it has to navigate is to the error screen.
				SOarray["custparam_error"] = st1;//'ENTER SERIAL NO';
				SOarray["custparam_screenno"] = '41FAST';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				nlapiLogExecution('DEBUG', 'Enter Serial No', getSerialNo);

			}
			else {
				nlapiLogExecution('DEBUG', 'Serial No', getSerialNo);
				getSerialNo=SerialNoIdentification(getItemId,getSerialNo);
				nlapiLogExecution('DEBUG', 'After Serial No Parsing', getSerialNo);
				var getActualEndDate = DateStamp();
				var getActualEndTime = TimeStamp();
				var TempSerialNoArray = new Array();
				if (request.getParameter('custparam_serialno') != null) 
					TempSerialNoArray = request.getParameter('custparam_serialno').split(',');



				nlapiLogExecution('DEBUG', 'INTO SERIAL ENTRY');
				//checking serial no's in already scanned one's
				for (var t = 0; t < TempSerialNoArray.length; t++) {
					if (getSerialNo == TempSerialNoArray[t]) {
						SOarray["custparam_error"] = st5;//"Serial # Already Scanned";//"Serial No. Already Scanned";
						SOarray["custparam_screenno"] = '41FAST';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
					}
				}
				/*if(getItemType != 'serializedinventoryitem' && getSerOut == 'T' && getSerIn != 'T')
				{
					SOarray["custparam_number"] = parseFloat(request.getParameter('custparam_number')) + 1;
				}
				else*/ 
				//if(getItemType == 'serializedinventoryitem' || (getSerOut == 'T'  && getSerIn == 'T'))
				nlapiLogExecution('DEBUG', 'getSerOut', getSerOut);
				//if(getItemType == 'serializedinventoryitem' || getSerIn == 'T' || getSerOut == 'T')
				if(getItemType == 'serializedinventoryitem' || getSerIn == 'T')// case# 201416526
				{
					nlapiLogExecution('DEBUG', 'getSerialNo', getSerialNo);
					nlapiLogExecution('DEBUG', 'getItemId', getItemId);
					nlapiLogExecution('DEBUG', 'getContainerLP', getContainerLP);

					//checking serial no's in records
					var filtersser = new Array();
					filtersser[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getSerialNo);
					
					if(getItemId !=null && getItemId!='')
						filtersser[1] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', getItemId);
					filtersser[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['3','19']);
					if(getParentLPNo !=null && getParentLPNo!='')
					{
						//filtersser[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', getContainerLP);
						filtersser[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', getParentLPNo);

					}

					var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser);
					nlapiLogExecution('DEBUG', 'SrchRecord', SrchRecord);

					if (SrchRecord==null) {
						SOarray["custparam_error"] = st6;//"Serial # Does Not Exist";//"Serial No. Does Not Exists";
						SOarray["custparam_screenno"] = '41FAST';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
					}
					for ( var i = 0; i < SrchRecord.length; i++) {
						var searchresultserial = SrchRecord[i];
						var vserialid = searchresultserial.getId();
						nlapiLogExecution('DEBUG', 'Serial ID Internal Id :',vserialid);

						nlapiLogExecution('DEBUG', "ebizSO" , getEbizOrd);
						nlapiLogExecution('DEBUG', "vLineNo" , getOrdLineNo);
						nlapiLogExecution('DEBUG', "Item" , getItemId);

						var fields = new Array();
						var values = new Array();
						fields[0] = 'custrecord_serialwmsstatus';
						fields[1] = 'custrecord_serialebizsono';
						fields[2] = 'custrecord_serialsono';
						fields[3] = 'custrecord_serialsolineno';
						fields[4] = 'custrecord_serialparentid';
						fields[5] = 'custrecord_serialitem';
						fields[6] = 'custrecord_serialcontlpno';// case no 201410794
						
						values[0] = '8';//STATUS.OUTBOUND.PICK_CONFIRMED
						values[1] = getEbizOrd;
						values[2] = getOrdName;
						values[3] = getOrdLineNo;
						values[4] = getContainerLP;
						values[5] = getItemId;
						values[6] = getContainerLP;

						var updatefields = nlapiSubmitField(
								'customrecord_ebiznetserialentry',
								vserialid, fields, values);
						nlapiLogExecution('DEBUG', 'Record Updated :');
					}
					/*else {
						// nlapiLogExecution('DEBUG', 'SERIAL NO NOT FOUND');
						if (request.getParameter('custparam_serialno') == null || request.getParameter('custparam_serialno') == "") {
							SOarray["custparam_serialno"] = getSerialNo;
						}
						else {
							SOarray["custparam_serialno"] = request.getParameter('custparam_serialno') + ',' + getSerialNo;
						}

						//SOarray["custparam_number"] = parseFloat(request.getParameter('custparam_number')) + 1;
					}*/
				}
				else
				{
					nlapiLogExecution('DEBUG', 'getItemId', getItemId);
					var filtersser = new Array();
					filtersser[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getSerialNo);
					if(getItemId !=null && getItemId!='')
						filtersser[1] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', getItemId);
					//filtersser[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['19']);
					filtersser[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['8','19']);
				
					var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser);
					nlapiLogExecution('DEBUG', 'SrchRecord', SrchRecord);

					if (SrchRecord!=null && SrchRecord!='' ) {
						nlapiLogExecution('ERROR', 'SERIAL NUMBER', 'EXISTS');
						SOarray["custparam_error"] = st7;//"Serial # Already Exists";
						SOarray["custparam_screenno"] = '41FAST';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
					}
						
							/*for ( var i = 0; i < SrchRecord.length; i++) {
							var searchresultserial = SrchRecord[i];
							var vserialid = searchresultserial.getId();
							nlapiLogExecution('ERROR', 'Serial ID Internal Id :',vserialid);

							nlapiLogExecution('ERROR', "ebizSO" , getEbizOrd);
							nlapiLogExecution('ERROR', "vLineNo" , getOrdLineNo);
							nlapiLogExecution('ERROR', "Item" , getItemId);

							var fields = new Array();
							var values = new Array();
							fields[0] = 'custrecord_serialwmsstatus';
							fields[1] = 'custrecord_serialebizsono';
							fields[2] = 'custrecord_serialsono';
							fields[3] = 'custrecord_serialsolineno';
							fields[4] = 'custrecord_serialparentid';
							fields[5] = 'custrecord_serialitem';
							values[0] = '8';//STATUS.OUTBOUND.PICK_CONFIRMED
							values[1] = getEbizOrd;
							values[2] = getOrdName;
							values[3] = getOrdLineNo;
							values[4] = getContainerLP;
							values[5] = getItemId;
							
							fields[0] = 'custrecord_serialparentid';
							fields[1] = 'custrecord_serialitem';
							fields[2] = 'custrecord_serialcontlpno';
							
							values[0] = getContainerLP
							values[1] = getItemId;
							values[2] = getContainerLP;

							var updatefields = nlapiSubmitField(
									'customrecord_ebiznetserialentry',
									vserialid, fields, values);
							nlapiLogExecution('ERROR', 'Record Updated :');*/	
					
					else
					{

					nlapiLogExecution('DEBUG', 'getOrdName ',getOrdName);

					nlapiLogExecution('DEBUG', "ebizSO" , getEbizOrd);
					nlapiLogExecution('DEBUG', "vLineNo" , getOrdLineNo);
					nlapiLogExecution('DEBUG', "Item Id " , getItemId);

					var customrecord = nlapiCreateRecord('customrecord_ebiznetserialentry'); 
					customrecord.setFieldValue('custrecord_serialnumber', getSerialNo);					
						//customrecord.setFieldValue('custrecord_serialwmsstatus', '19');//with status STORAGE
						customrecord.setFieldValue('custrecord_serialwmsstatus', '8');//with picking completed
						customrecord.setFieldValue('custrecord_serialebizsono', getEbizOrd);
						customrecord.setFieldValue('custrecord_serialsono', getOrdName);
						customrecord.setFieldValue('custrecord_serialsolineno', getOrdLineNo);
						customrecord.setFieldValue('name', getSerialNo);
						customrecord.setFieldValue('custrecord_serialparentid', getContainerLP);
						customrecord.setFieldValue('custrecord_serialitem', getItemId);
						customrecord.setFieldValue('custrecord_serialcontlpno', getContainerLP);
						
						//commit the record to NetSuite
						var recid = nlapiSubmitRecord(customrecord, true);
					}
				}


				if (request.getParameter('custparam_serialno') == null || request.getParameter('custparam_serialno') == "") 
				{
					SOarray["custparam_serialno"] = getSerialNo;
				}
				else 
				{
					SOarray["custparam_serialno"] = request.getParameter('custparam_serialno') + ',' + getSerialNo;
				}

				SOarray["custparam_number"] = parseFloat(request.getParameter('custparam_number')) + 1;
				TempSerialNoArray.push(getSerialNo);
				nlapiLogExecution('DEBUG', '(getNumber + 1)', (parseFloat(getNumber) + 1));
				nlapiLogExecution('DEBUG', 'getItemQuantity', getItemQuantity);
				if ((parseFloat(getNumber) + 1) < parseFloat(getItemQuantity)) {
					nlapiLogExecution('DEBUG', 'Scanning Serial No.');
					response.sendRedirect('SUITELET', 'customscript_rf_fastpick_serialscan', 'customdeploy_rf_fastpick_serialscan_di', false, SOarray);
					return;

				}
				else {
					nlapiLogExecution('DEBUG', 'Inserting Into Records');
					nlapiLogExecution('DEBUG', 'hdnpicktype',request.getParameter('hdnpicktype'));
					nlapiLogExecution('DEBUG', 'hdnpicktype',request.getParameter('hdnpicktype'));
					nlapiLogExecution('DEBUG', 'detailTask',detailTask);
					nlapiLogExecution('DEBUG', 'SOarray["custparam_detailtask"]',SOarray["custparam_detailtask"]);
					var vPickType = "";
					if(request.getParameter('hdnpicktype') != null && request.getParameter('hdnpicktype') != "")
					{
						vPickType = request.getParameter('hdnpicktype');
					}
					if(vPickType == "CL" && detailTask != '' && detailTask !='F')
					{
						SOarray["custparam_serialscanned"] = "true";
						response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_dettask', 'customdeploy_rf_cluspicking_dettask', false, SOarray);						
					}
					else if(vPickType != "CL"  )
					{
						SOarray["custparam_serialscanflag"]="TRUE";
						//case # 20149416�start
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pickingcarton', 'customdeploy_ebiz_rf_pickingcarton', false, SOarray);
						//	response.sendRedirect('SUITELET', 'customscript_rf_picking_container_no', 'customdeploy_rf_picking_container_no_di', false, SOarray);
							//case # 20149416�end
					}
					else if(vPickType == "CL" && detailTask != '' && detailTask =='F')
					{
						SOarray["custparam_serialscanned"] = "true";
						response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_summtask', 'customdeploy_rf_cluspicking_summtask', false, SOarray);
						return;
					}
				}
			}
	}

	nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
}

function custChknPutwRecCreation(po, poValue, quan, lineno, remQty, lineCnt, ItemId, ItemName, ItemDesc, ItemStatus, PackCode, BaseUOM, LP, BinLoc, RcvQty, ActualEndDate, ActualEndTime, poreceiptno, BeginLocation, ActualBeginDate, ActualBeginTime, ActualBeginTimeAMPM, BeginLocationId, BatchNo, MfgDate, ExpDate, BestBeforeDate, LastDate, FifoDate, FifoCode, itemrectype){
	nlapiLogExecution('DEBUG', 'Into custChknPutwRecCreation', 'custChknPutwRecCreation');
	var now = new Date();
	var stagelocid, docklocid;
	var stagesearchresult = new nlapiSearchRecord('customrecord_ebiznet_location', null, new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'anyof', [8]));
	if (stagesearchresult != null && stagesearchresult.length > 0) {
		stagelocid = stagesearchresult[0].getId();
	}
	var docksearchresult = new nlapiSearchRecord('customrecord_ebiznet_location', null, new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'anyof', [3]));
	if (docksearchresult != null && docksearchresult.length > 0) {
		docklocid = docksearchresult[0].getId();
	}
	if (BinLoc != null) //Creating custom record with CHKN and PUTW task,
	{
		//create the openTask record With CHKN Task Type
		var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating CHKN Record', 'TRN_OPENTASK');

		//populating the fields
		customrecord.setFieldValue('name', poValue);
		customrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		//        customrecord.setFieldValue('custrecord_ebiz_sku_no', ItemName);
		customrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		customrecord.setFieldValue('custrecord_act_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_lpno', LP);
		customrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		customrecord.setFieldValue('custrecord_line_no', lineno);
		customrecord.setFieldValue('custrecord_packcode', PackCode);
		customrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		customrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		customrecord.setFieldValue('custrecord_tasktype', 1); //For Check-In (CHKN)
		//        customrecord.setFieldValue('custrecord_actbeginloc', BinLoc);
		customrecord.setFieldValue('custrecord_actbeginloc', stagelocid);
		customrecord.setFieldValue('custrecord_actendloc', docklocid);
		//        customrecord.setFieldValue('custrecord_actendloc', 'INB-1');
		customrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		//Added for Item name and desc
		customrecord.setFieldValue('custrecord_sku_2', ItemId);
		customrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//For eBiznet Receipt Number .
		customrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		//        customrecord.setFieldValue('custrecordact_begin_date', (parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
		customrecord.setFieldValue('custrecordact_begin_date', ActualBeginDate);
		//        customrecord.setFieldValue('custrecord_act_end_date', (parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
		customrecord.setFieldValue('custrecord_act_end_date', ActualEndDate);

		/*        
         //Adding fields to update time zones.
         //        customrecord.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
         customrecord.setFieldValue('custrecord_actualbegintime', ActualBeginDate);
         //        customrecord.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
         customrecord.setFieldValue('custrecord_actualendtime', ActualEndTime);
         customrecord.setFieldValue('custrecord_recordtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
         customrecord.setFieldValue('custrecord_recordupdatetime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
         customrecord.setFieldValue('custrecord_current_date', (parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
         customrecord.setFieldValue('custrecord_upd_date', (parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
		 */
		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		customrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		nlapiLogExecution('DEBUG', 'getActualBeginTime', getActualBeginTime);
		customrecord.setFieldValue('custrecord_actualendtime', ActualEndTime);
		customrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		customrecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
		customrecord.setFieldValue('custrecord_current_date', DateStamp());
		customrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		//        customrecord.setFieldValue('custrecord_status_flag', 'C');
		customrecord.setFieldValue('custrecord_wms_status_flag', 1);

		//LN,Fields for Lot/Batch Sku's
		if (itemrectype == 'lotnumberedinventoryitem') {
			customrecord.setFieldValue('custrecord_batch_no', BatchNo);
			customrecord.setFieldValue('custrecord_expirydate', MfgDate);
			customrecord.setFieldValue('custrecord_fifodate', FifoDate);
		}

		nlapiLogExecution('DEBUG', 'Submitting CHKN record', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(customrecord);
		nlapiLogExecution('DEBUG', 'Done CHKN Record Insertion :', 'Success');

		//create the openTask record With PUTW Task Type

		var putwrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating PUTW record', 'TRN_OPENTASK');

		//populating the fields
		putwrecord.setFieldValue('name', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		//putwrecord.setFieldValue('custrecord_ebiz_sku_no', ItemName);
		putwrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		//        putwrecord.setFieldValue('custrecord_act_qty', quan);
		putwrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		putwrecord.setFieldValue('custrecord_lpno', LP);
		putwrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		putwrecord.setFieldValue('custrecord_line_no', lineno);
		putwrecord.setFieldValue('custrecord_packcode', PackCode);
		putwrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		putwrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		putwrecord.setFieldValue('custrecord_tasktype', 2); //For Putaway (PUTW)
		nlapiLogExecution('DEBUG', 'Fetched Begin Location', BeginLocation);

		putwrecord.setFieldValue('custrecord_actbeginloc', BeginLocationId);
		putwrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		//For eBiznet Receipt Number .
		putwrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		putwrecord.setFieldValue('custrecordact_begin_date', DateStamp());


		//Adding fields to update time zones.
		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		putwrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		putwrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		putwrecord.setFieldValue('custrecord_current_date', DateStamp());
		putwrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		//        putwrecord.setFieldValue('custrecord_status_flag', 'L');
		putwrecord.setFieldValue('custrecord_wms_status_flag', 2);

		//Added for Item name and desc
		putwrecord.setFieldValue('custrecord_sku_2', ItemId);
		putwrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//LN,Fields for Lot/Batch Sku's
		if (itemrectype == 'lotnumberedinventoryitem') {
			putwrecord.setFieldValue('custrecord_batch_no', BatchNo);
			putwrecord.setFieldValue('custrecord_expirydate', MfgDate);
			putwrecord.setFieldValue('custrecord_fifodate', FifoDate);
		}

		//commit the record to NetSuite
		var putwrecordid = nlapiSubmitRecord(putwrecord);
		nlapiLogExecution('DEBUG', 'Done PUTW Record Insertion ', 'TRN_OPENTASK');

	}//end if binloc is not null
	else {
		//create the openTask record With CHKN Task Type
		var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating customrecord', 'TRN_OPENTASK');

		//populating the fields
		customrecord.setFieldValue('name', poValue);
		customrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		//        customrecord.setFieldValue('custrecord_ebiz_sku_no', ItemName);
		customrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		customrecord.setFieldValue('custrecord_act_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_lpno', LP);
		customrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		customrecord.setFieldValue('custrecord_line_no', lineno);
		customrecord.setFieldValue('custrecord_packcode', PackCode);
		customrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		customrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		customrecord.setFieldValue('custrecord_tasktype', 1); //For Check-In (CHKN)
		customrecord.setFieldValue('custrecord_actbeginloc', stagelocid);
		customrecord.setFieldValue('custrecord_actendloc', docklocid);
		customrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		customrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);


		customrecord.setFieldValue('custrecordact_begin_date', ActualBeginDate);
		customrecord.setFieldValue('custrecord_act_end_date', ActualEndDate);
		//Adding fields to update time zones.
		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		customrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		customrecord.setFieldValue('custrecord_actualendtime', ActualEndTime);
		customrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		customrecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
		customrecord.setFieldValue('custrecord_current_date', DateStamp());
		customrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		//        customrecord.setFieldValue('custrecord_status_flag', 'C');
		customrecord.setFieldValue('custrecord_wms_status_flag', 1);

		//Added for Item name and desc
		customrecord.setFieldValue('custrecord_sku_2', ItemId);
		customrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//LN,Fields for Lot/Batch Sku's
		if (itemrectype == 'lotnumberedinventoryitem') {
			customrecord.setFieldValue('custrecord_batch_no', BatchNo);
			customrecord.setFieldValue('custrecord_expirydate', MfgDate);
			customrecord.setFieldValue('custrecord_fifodate', FifoDate);
		}

		nlapiLogExecution('DEBUG', 'Submitting customrecord', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(customrecord);
		nlapiLogExecution('DEBUG', 'Done CHKN Record Insertion :', 'Success');

		//create the openTask record With PUTW Task Type
		var putwrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating customrecord', 'TRN_OPENTASK');

		//populating the fields
		putwrecord.setFieldValue('name', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		//        putwrecord.setFieldValue('custrecord_act_qty', quan);
		putwrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		putwrecord.setFieldValue('custrecord_lpno', LP);
		putwrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		putwrecord.setFieldValue('custrecord_line_no', lineno);
		putwrecord.setFieldValue('custrecord_packcode', PackCode);
		putwrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		putwrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		putwrecord.setFieldValue('custrecord_tasktype', 2); //For Putaway (PUTW)
		//customrecord.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
		// customrecord.setFieldValue('custrecord_actbeginloc', BinLoc);
		//customrecord.setFieldValue('custrecord_actendloc', BinLoc);
		putwrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		putwrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		//Adding fields to update time zones.

		//customrecord.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
		// customrecord.setFieldValue('custrecord_actualendtime',   ((curr_hour)+":"+(curr_min)+" " +a_p));
		//customrecord.setFieldValue('custrecord_recordtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
		//customrecord.setFieldValue('custrecord_recordupdatetime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
		/*
         putwrecord.setFieldValue('custrecord_current_date', (parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
         putwrecord.setFieldValue('custrecord_upd_date', (parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
		 */
		putwrecord.setFieldValue('custrecord_current_date', DateStamp());
		putwrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		//        customrecord.setFieldValue('custrecord_status_flag', 'N');
		putwrecord.setFieldValue('custrecord_wms_status_flag', 6);

		//Added for Item name and desc
		putwrecord.setFieldValue('custrecord_sku_2', ItemId);
		putwrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//LN,Fields for Lot/Batch Sku's
		if (itemrectype == 'lotnumberedinventoryitem') {
			putwrecord.setFieldValue('custrecord_batch_no', BatchNo);
			putwrecord.setFieldValue('custrecord_expirydate', MfgDate);
			putwrecord.setFieldValue('custrecord_fifodate', FifoDate);
		}

		nlapiLogExecution('DEBUG', 'Submitting customrecord', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(putwrecord);
		nlapiLogExecution('DEBUG', 'Done CHKN Record Insertion :', 'Success');
	}
}
