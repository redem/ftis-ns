/***************************************************************************
 eBizNET Solutions 
 ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_TOCheckinSKU.js,v $
 *     	   $Revision: 1.4.2.11.4.4.2.14 $
 *     	   $Date: 2014/06/23 06:39:46 $
 *     	   $Author: skavuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 * DESCRIPTION
 * REVISION HISTORY
 * $Log: ebiz_RF_TOCheckinSKU.js,v $
 * Revision 1.4.2.11.4.4.2.14  2014/06/23 06:39:46  skavuri
 * Case# 20148882 Issue Fixed
 *
 * Revision 1.4.2.11.4.4.2.13  2014/06/13 05:54:02  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.4.2.11.4.4.2.12  2014/06/06 07:55:24  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.4.2.11.4.4.2.11  2014/05/30 00:26:51  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.4.2.11.4.4.2.10  2014/02/17 14:47:07  rmukkera
 * Case # 20127192
 *
 * Revision 1.4.2.11.4.4.2.9  2013/10/17 06:59:16  gkalla
 * Case# 20124611
 * TSG and CWD TO issue
 *
 * Revision 1.4.2.11.4.4.2.8  2013/09/27 16:17:36  skreddy
 * Case# 20124592
 * nucourse issue fix
 *
 * Revision 1.4.2.11.4.4.2.7  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.4.2.11.4.4.2.6  2013/05/15 01:22:32  kavitha
 * CASE201112/CR201113/LOG201121
 * TSG Issue fixes
 *
 * Revision 1.4.2.11.4.4.2.5  2013/05/02 09:16:36  gkalla
 * CASE201112/CR201113/LOG201121
 * As part of standard bundle. Fixed screen error
 *
 * Revision 1.4.2.11.4.4.2.4  2013/04/18 11:53:12  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.4.2.11.4.4.2.3  2013/04/17 16:04:01  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.4.2.11.4.4.2.2  2013/03/15 09:28:43  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.4.2.11.4.4.2.1  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.4.2.11.4.4  2013/02/15 14:59:20  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.4.2.11.4.3  2012/11/01 14:55:35  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.4.2.11.4.2  2012/09/27 10:55:14  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.4.2.11.4.1  2012/09/21 14:57:16  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.4.2.11  2012/09/04 16:50:57  gkalla
 * CASE201112/CR201113/LOG201121
 * Invelid length error
 *
 * Revision 1.4.2.10  2012/06/13 22:23:28  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT and Prod issue fixes
 *
 * Revision 1.4.2.9  2012/05/28 15:31:09  spendyala
 * CASE201112/CR201113/LOG201121
 * sku validation issue related to wbc.
 *
 * Revision 1.4.2.8  2012/05/16 06:41:45  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.4.2.7  2012/05/03 07:40:45  gkalla
 * CASE201112/CR201113/LOG201121
 * Added functionality to allow same item in more than one TO line.
 *
 * Revision 1.4.2.6  2012/04/30 10:00:49  spendyala
 * CASE201112/CR201113/LOG201121
 * While Searching of Item in ItemMaster,
 * 'name' filter is changed to 'nameinternal'
 *
 * Revision 1.4.2.5  2012/04/27 13:13:04  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Invaid SKU
 *
 * Revision 1.4.2.4  2012/04/11 22:06:57  spendyala
 * CASE201112/CR201113/LOG201121
 * Navigation to previous screen issue is resolved.
 *
 * Revision 1.4.2.3  2012/04/09 14:35:38  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * DUplicate line items in Transactions
 *
 * Revision 1.4.2.2  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.4.2.1  2012/02/22 12:44:49  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.4  2011/12/27 15:36:01  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.3  2011/10/14 20:29:53  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Transfer Order  Files
 *
 * Revision 1.2  2011/09/29 14:46:42  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * TO Functionality New Files
 *
 * Revision 1.1  2011/09/28 11:37:38  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RMA Functionality New Files
 *
 * Revision 1.2  2011/09/24 15:51:43  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RMA Functionality New Files
 *
 * Revision 1.1  2011/09/22 08:37:21  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RMA Functionality New Files
 *
 *
 *****************************************************************************/

function TOCheckInSKU(request, response){
	if (request.getMethod() == 'GET') 
	{
		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);

		//Variable Declaration
		var html = '';

		//	Get the PO# from the previous screen, which is passed as a parameter		
		var getPONo = request.getParameter('custparam_poid');
		nlapiLogExecution('DEBUG', 'Into Request', getPONo);

		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime'); 
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var whLocation = request.getParameter('custparam_whlocation');
		nlapiLogExecution('DEBUG', 'whLocation', whLocation);
		var whCompany= request.getParameter('custparam_company');

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4;
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "N&#218;MERO DE ORDEN DE TRANSFERENCIA";
			st2 = "INGRESAR / ESCANEO DEL ART&#205;CULO ";	
			st3 = "ENVIAR";
			st4 = "ANTERIOR";


		}
		else
		{
			st0 = "";
			st1 = "TRANSFER ORDER#";
			st2 = "ENTER/SCAN ITEM";
			st3 = "SEND";
			st4 = "PREV";


		}



		var functionkeyHtml=getFunctionkeyScript('_rf_checkin_sku'); 
		html = "<html><head><title>" + st0 +  "</title>" ;
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends      
		//html = html + " document.getElementById('enteritem').focus();";        
		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_checkin_sku' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> " + st1 + ": <label>" + getPONo + "</label>";
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnWhCompany' value=" + whCompany + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enteritem' id='enteritem' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;  return false'/>";
		html = html + "					" + st4 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";

		//Case# 20148882 (added Focus Functionality for Textbox)
		 html = html + "<script type='text/javascript'>document.getElementById('enteritem').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {

		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var ActualBeginTime;
		var getItem = request.getParameter('enteritem');
		var getActualBeginTime = request.getParameter('hdnActualBeginTime'); 
		var getActualBeginTimeAMPM = request.getParameter('hdnActualBeginTimeAMPM');
		var whLocation  = request.getParameter('hdnWhLocation');
		nlapiLogExecution('DEBUG', 'whLocation', whLocation);
		var whCompany  = request.getParameter('hdnWhCompany');		

		var POarray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;

		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);

		var st5;
		if( getLanguage == 'es_ES')
		{
			st5 = "ART&#205;CULO INV&#193;LIDO";
		}
		else
		{
			st5 = "INVALID ITEM";
		}
		POarray["custparam_poid"] = request.getParameter('custparam_poid');
		POarray["custparam_poitem"] = request.getParameter('enteritem');
		POarray["custparam_error"] = st5;
		POarray["custparam_screenno"] = '2T';
		POarray["custparam_actualbegindate"] = request.getParameter('hdnActualBeginDate');	
		POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
		POarray["custparam_company"] = request.getParameter('hdnWhCompany');


		var getOptedField = request.getParameter('hdnOptedField');
		POarray["custparam_option"] = getOptedField;

		nlapiLogExecution('DEBUG', 'WH Location', POarray["custparam_whlocation"]);

		POarray["custparam_actualbegintime"] = getActualBeginTime; //TimeArray[0];
		POarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM;	//TimeArray[1];

		//  try {
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept PO #.
		if (optedEvent != 'F7') {
			//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen

			nlapiLogExecution('DEBUG', 'custparam_poitem', POarray["custparam_poitem"]);

			if (POarray["custparam_poitem"] != "") {

				var actItemid = validateCHECKINSKU(POarray["custparam_poitem"],  POarray["custparam_whlocation"], POarray["custparam_company"]);
				nlapiLogExecution('DEBUG', 'After validateSKU',actItemid);

				if((actItemid==null) || (actItemid==""))
				{
					nlapiLogExecution('DEBUG', 'After validateSKU return Value::',actItemid);
				}


				// Changed On 30/4/12 by Suman

				var filteritem=new Array();
				filteritem.push(new nlobjSearchFilter('nameinternal', null, 'is', actItemid));// case# 201417192 
				filteritem.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
				var columnsitem = new Array();
				columnsitem[0] = new nlobjSearchColumn('itemid');
				columnsitem[0].setSort(false);
				var itemSearch = nlapiSearchRecord('item', null,filteritem,columnsitem);
				// End of Changes as On 30/4/12


				if (itemSearch == null) 
				{
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
				}
				else 
				{
					for (var s = 0; s < itemSearch.length; s++) 
					{
						var ItemSearchedId = itemSearch[s].getId();
						nlapiLogExecution('DEBUG', 'Fetched Item ID', ItemSearchedId);
						//var ItemDescription = itemSearch[s].getFieldValue('purchasedescription');
					}
					var fields = ['recordType', 'custitem_ebizserialin','custitem_ebizbatchlot'];
					var columns = nlapiLookupField('item', ItemSearchedId, fields);
					var ItemType = columns.recordType;	
					var serialInflg="F";		
					serialInflg = columns.custitem_ebizserialin;
					var batchflag="F";
					batchflag= columns.custitem_ebizbatchlot;
					var ItemRec = nlapiLoadRecord(ItemType, ItemSearchedId);
					var ItemDescription = ItemRec.getFieldValue('purchasedescription');

					nlapiLogExecution('DEBUG', 'POarray["custparam_poitem"]', POarray["custparam_poitem"]);

					var Transactionlinedetails=eBiz_RF_GetTransactionlinedetails(POarray["custparam_poid"], ItemSearchedId);

					var RoleLocation=getRoledBasedLocation();
					nlapiLogExecution('DEBUG', 'RoleLocation', RoleLocation);
					
					var POfilters = new Array();
					POfilters[0] = new nlobjSearchFilter('mainline', null, 'is', 'F');
					POfilters[1] = new nlobjSearchFilter('tranid', null, 'is', POarray["custparam_poid"]);
					POfilters[2] = new nlobjSearchFilter('item', null, 'is', ItemSearchedId);
					if(RoleLocation != null && RoleLocation != '' && RoleLocation != 0)
						POfilters.push(new nlobjSearchFilter('transferlocation', null, 'anyof', RoleLocation));
					var POColumns = new Array();
					POColumns[0] = new nlobjSearchColumn('item');
					POColumns[1] = new nlobjSearchColumn('line');
					POColumns[2] = new nlobjSearchColumn('custcol_ebiz_po_cube'); 
					POColumns[3] = new nlobjSearchColumn('transferlocation');					 
					POColumns[4] = new nlobjSearchColumn('quantity');
					POColumns[5] = new nlobjSearchColumn('quantityshiprecv');
					var searchresults1 = nlapiSearchRecord('transferorder', null, POfilters, POColumns);
					var TOInternalId;
					if(searchresults1 != null && searchresults1 != '' && searchresults1.length>0)
					{
						TOInternalId=searchresults1[0].getId();
					}	
					nlapiLogExecution('DEBUG', 'TOId',POarray["custparam_poid"]);
					nlapiLogExecution('DEBUG', 'TOInternalId',TOInternalId);
					var searchresults;
					if(TOInternalId != null && TOInternalId != '')
						searchresults=nlapiLoadRecord('transferorder', TOInternalId);

					var vIndex=0;
					var vBoolFount=false;
					var vTempIndex=1;
					//Case # 20127182�Start
					var vTOCount=0;
					//Case # 20127182�End
					if(Transactionlinedetails!=null && Transactionlinedetails!='')
					{
						//Case # 20127182�Start
						if(searchresults !=null && searchresults!='' && searchresults.length >0)
						{
							vTOCount=searchresults.getLineItemCount('item');
						}
						//Case # 20127182�End
						nlapiLogExecution('DEBUG', 'vTOCount',vTOCount);
						nlapiLogExecution('DEBUG', 'vBoolFount1', vBoolFount);
						for(var j=0;vBoolFount==false && j<vTOCount; j++)
						{
							var vTempFlag=false;
							var polineno=searchresults.getLineItemValue('item', 'line', j+1);
							var poitemid=searchresults.getLineItemValue('item', 'item', j+1);

							nlapiLogExecution('DEBUG', 'polinenoStart',polineno);
							if(poitemid == ItemSearchedId)
							{

								for(var k=0; k<Transactionlinedetails.length;k++)
								{
									var tranlineno=Transactionlinedetails[k].getValue('custrecord_orderlinedetails_orderline_no');
									nlapiLogExecution('DEBUG', 'tranlineno', tranlineno);
									nlapiLogExecution('DEBUG', 'polineno', polineno);
									if(tranlineno==polineno)
									{
										var pocheckinqty=searchresults.getLineItemValue('item', 'quantity', j+1);
										var trancheckinqty=Transactionlinedetails[k].getValue('custrecord_orderlinedetails_checkin_qty');
										if(parseFloat(pocheckinqty)>parseFloat(trancheckinqty))
										{
											if(!vBoolFount){
												vIndex=j+1;
												nlapiLogExecution('DEBUG', 'POInternalId', searchresults1[j].getValue('line'));
												vBoolFount=true;
											}
											//goto Skiphere;
										}
										vTempFlag=true;
										nlapiLogExecution('DEBUG', 'vTempFlag1', vTempFlag);
										break; 
									}									 	
								}	
								nlapiLogExecution('DEBUG', 'vTempFlag2', vTempFlag);
								if(vTempFlag==false)
								{
									vIndex=j+1;
									vBoolFount=true;
									break;
								}	
							}
						}  
					}
					nlapiLogExecution('DEBUG', 'vBoolFount2', vBoolFount);
					nlapiLogExecution('DEBUG', 'vIndex', vIndex);
					/* Up to here */ 

					if (searchresults != null ) {
						nlapiLogExecution('DEBUG', 'TOInternalId', TOInternalId);

						nlapiLogExecution('DEBUG', 'searchresults',searchresults.length);
						if(vIndex!=0)
						{
							POarray["custparam_lineno"] = searchresults.getLineItemValue('item', 'line', vIndex);
							POarray["custparam_whlocation"] = searchresults.getFieldValue('transferlocation');
						}
						else
						{
							for(var h=0;h<searchresults1.length;h++)
							{
								var vPOTempRcvQty=searchresults1[h].getValue('quantityshiprecv');
								var vPOTempOrdQty=searchresults1[h].getValue('quantity');
								nlapiLogExecution('DEBUG', 'vPOTempRcvQty', vPOTempRcvQty);
								nlapiLogExecution('DEBUG', 'vPOTempOrdQty', vPOTempOrdQty);
								if(vPOTempOrdQty ==null || vPOTempOrdQty == '')
									vPOTempOrdQty=0;
								if(vPOTempRcvQty ==null || vPOTempRcvQty == '')
									vPOTempRcvQty=0;
								if(parseInt(vPOTempRcvQty)<parseInt(vPOTempOrdQty))
								{
									var vTOTempLine=searchresults1[h].getValue(POColumns[1]);
									nlapiLogExecution('DEBUG', 'vTOTempLine', vTOTempLine);
									if(vTOTempLine == null || vTOTempLine == '')
										vTOTempLine=0;
									if(vTOTempLine > 0)
										vTOTempLine=vTOTempLine-2;
									//POarray["custparam_lineno"] = searchresults1[h].getValue(POColumns[1]);
									POarray["custparam_lineno"] = vTOTempLine;
									POarray["custparam_whlocation"] = searchresults1[h].getValue(POColumns[3]); //searchresults[vIndex].getValue(POColumns[3]);
									break;
								}
							}
						}
						if(POarray["custparam_lineno"] == null || POarray["custparam_lineno"] == '')
						{
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							nlapiLogExecution('DEBUG', 'SearchResults ', 'Invalid Item');
							return false;
						}	
						POarray["custparam_fetcheditemid"] = ItemSearchedId;
						POarray["custparam_pointernalid"] = TOInternalId;
						POarray["custparam_fetcheditemname"] = getItem;
						POarray["custparam_itemdescription"] = ItemDescription;

						nlapiLogExecution('DEBUG', 'Line #', POarray["custparam_lineno"]);
						nlapiLogExecution('DEBUG', 'Fetched Item id', POarray["custparam_fetcheditemid"]);
						nlapiLogExecution('DEBUG', 'TOInternalId', POarray["custparam_pointernalid"]);

						// Added by Phani on 03-25-2011

						nlapiLogExecution('DEBUG', 'POarray["custparam_whlocation"]', POarray["custparam_whlocation"]);
						nlapiLogExecution('DEBUG', 'WH Location', POarray["custparam_whlocation"]);
						nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is not null');
						nlapiLogExecution('DEBUG', 'custrecord_ebizitemdims ', getItem);
						nlapiLogExecution('DEBUG', 'custrecord_ebizitemdims ', ItemSearchedId);

						var ItemDimensionsFilter = new Array();
						ItemDimensionsFilter[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', ItemSearchedId);
						ItemDimensionsFilter[1] = new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T');

						var ItemDimensionsColumns = new Array();
						ItemDimensionsColumns[0] = new nlobjSearchColumn('custrecord_ebizcube');
						ItemDimensionsColumns[1] = new nlobjSearchColumn('custrecord_ebizqty');

						var ItemDimensionsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, ItemDimensionsFilter, ItemDimensionsColumns);
						nlapiLogExecution('DEBUG', 'before sumdims search ', '');

						if (ItemDimensionsSearchResults != null && ItemDimensionsSearchResults.length > 0) 
						{
							POarray["custparam_itemcube"] = ItemDimensionsSearchResults[0].getValue('custrecord_ebizcube');
							//Added on 9Mar By suman.
							POarray["custparam_baseuomqty"]=ItemDimensionsSearchResults[0].getValue('custrecord_ebizqty');
							nlapiLogExecution('DEBUG', 'Item Cube', POarray["custparam_itemcube"]);

							if (POarray["custparam_poitem"] != "") {
								response.sendRedirect('SUITELET', 'customscript_rf_to_checkinqty', 'customdeploy_rf_tocheckin_qty_di', false, POarray);
							}
							else {
								//	if the 'Send' button is clicked without any option value entered,
								//  it has to show an error message. The next screen to which it has to navigate is to the error screen.
								nlapiLogExecution('DEBUG', 'No SKU value is entered ', POarray["custparam_poitem"]);
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							}
							nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
						}
					}
					else {
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
					}
				}
			}                
			else {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
			}
		}
		else {
			response.sendRedirect('SUITELET', 'customscript_rf_to_checkin', 'customdeploy_rf_to_checkin_di', false, POarray);
		}
		/*    } 
        catch (e) {
            response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
            nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
        }*/
	} //end of first if condition


} //end of function.


function validateCHECKINSKU(itemNo,location,company)
{
	nlapiLogExecution('DEBUG', 'validateSKU:Start',itemNo);

	nlapiLogExecution('DEBUG', 'Calling After Method calling Location ',location);

	var actItem="";

	/*var invitemfilters = new Array();
	invitemfilters[0] = new nlobjSearchFilter('name',null, 'is',itemNo);
	var invLocCol = new Array();
	invLocCol[0] = new nlobjSearchColumn('itemid');

	var invitemRes = nlapiSearchRecord('item',null,invitemfilters,invLocCol);*/

	// Changed On 30/4/12 by Suman

	var invitemfilters=new Array();
	invitemfilters.push(new nlobjSearchFilter('nameinternal', null, 'is', itemNo));
	invitemfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
	
	if(location!=null && location!='')
		invitemfilters.push(new nlobjSearchFilter('location', null, 'anyof', ['@NONE@',location]));

	
	var invLocCol = new Array();
	invLocCol[0] = new nlobjSearchColumn('itemid');
	var invitemRes = nlapiSearchRecord('item', null,invitemfilters,invLocCol);

	// End of Changes as On 30/4/12


	if(invitemRes!=null)
	{
		actItem=invitemRes[0].getValue('itemid');
	}
	else
	{					
		var invLocfilters = new Array();
		invLocfilters.push(new nlobjSearchFilter('upccode', null, 'is', itemNo));

		//Changed On 30/4/12 by suman
		invLocfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		//End of Changes as on 30/4/12.

		var invtRes = nlapiSearchRecord('item', null, invLocfilters, invLocCol);
		if(invtRes != null)
		{
			actItem=invtRes[0].getValue('itemid');
		}
		else
		{

			nlapiLogExecution('DEBUG', 'inSide SKUALIAS',itemNo);

			var skuAliasFilters = new Array();
			skuAliasFilters[0] = new nlobjSearchFilter('name', null, 'is', itemNo);

			//if(location!=null && location!="")					
			skuAliasFilters[1] = new nlobjSearchFilter('custrecord_ebiz_location', null, 'is', location);

			//if(company != null && company != "")
			//	skuAliasFilters[2] = new nlobjSearchFilter('custrecord_ebiz_company', null, 'is', company);

			var skuAliasCols = new Array();
			skuAliasCols[0] = new nlobjSearchColumn('custrecord_ebiz_item');

			var skuAliasResults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, skuAliasFilters, skuAliasCols);

			if(skuAliasResults != null)
				actItem=skuAliasResults[0].getText('custrecord_ebiz_item');

		}			
	}
	nlapiLogExecution('DEBUG', 'validateSKU:end',actItem);
	return actItem;
}
