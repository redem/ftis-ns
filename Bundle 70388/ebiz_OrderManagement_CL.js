/***************************************************************************
eBizNET Solutions
****************************************************************************
*
*� $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Client/Attic/ebiz_OrderManagement_CL.js,v $
*� $Revision: 1.1.2.2.4.1.4.2 $
*� $Date: 2013/03/19 12:08:11 $
*� $Author: schepuri $
*� $Name: t_NSWMS_2013_1_3_9 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_OrderManagement_CL.js,v $
*� Revision 1.1.2.2.4.1.4.2  2013/03/19 12:08:11  schepuri
*� CASE201112/CR201113/LOG201121
*� change url path
*�
*� Revision 1.1.2.2.4.1.4.1  2013/03/01 14:34:54  skreddy
*� CASE201112/CR201113/LOG201121
*� Merged from FactoryMation and change the Company name
*�
*� Revision 1.1.2.2.4.1  2012/12/14 07:45:29  spendyala
*� CASE201112/CR201113/LOG201121
*� moved from 2012.2 branch
*�
*� Revision 1.1.2.2  2012/07/19 12:49:48  skreddy
*� CASE201112/CR201113/LOG201121
*� Change Printreport Method, to navigate to Pdf script
*�
*� Revision 1.1.2.1  2012/07/12 14:38:27  spendyala
*� CASE201112/CR201113/LOG201121
*� New client script for ordermangement.
*�
*
****************************************************************************/

function changePageValues(type,name)
{	
	if(name=='custpage_selectpage'||name=='custpage_pagesize')
	{
		var sublist=nlapiGetLineItemCount('custpage_wavecancellist');		
		if(sublist!=null)
		{	        	  
			nlapiSetFieldValue('custpage_hiddenfield','F');  
		}
		var transfersublist  = nlapiGetLineItemCount('custpage_replen_items');	 
		if(transfersublist!=null)
		{
			nlapiSetFieldValue('custpage_hiddenfield','F');  
		}
		var putawaygen_locs  = nlapiGetLineItemCount('custpage_items');	 
		if(putawaygen_locs!=null)
		{
			nlapiSetFieldValue('custpage_hiddenfield','F');  
		}
		NLDoMainFormButtonAction("submitter",true);	
	}
	else
	{
		return true;
	}
}

/* 
* @param SOid // navigate to pdf generate script
*/
function Printreport(soid)
{	
	//alert(soid);
	var fromdate=nlapiGetFieldValue('custpage_fromdate');
	var todate=nlapiGetFieldValue('custpage_todate');
	var vTaskStatus = nlapiGetFieldValue('custpage_taskstatus');
	var SOID=nlapiGetFieldValue('custpage_sointid');
	nlapiLogExecution('ERROR', 'print button','print');
	nlapiLogExecution('ERROR', 'into Printreport wono',soid);
	
	var PDFURL = nlapiResolveURL('SUITELET', 'customscript_ebiz_ordermangpdfreport', 'customdeploy_ebiz_ordermangpdf_di');
	/*var ctx = nlapiGetContext();
	nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
	if (ctx.getEnvironment() == 'PRODUCTION') {
		PDFURL = 'https://system.netsuite.com' + PDFURL;			
	}
	else 
	if (ctx.getEnvironment() == 'SANDBOX') {
			PDFURL = 'https://system.sandbox.netsuite.com' + PDFURL;				
	}*/
	
	nlapiLogExecution('ERROR', 'WO PDF URL',PDFURL);					
	PDFURL = PDFURL + '&custparam_soid='+ SOID;
	PDFURL = PDFURL + '&custparam_taskstatus='+ vTaskStatus;
	PDFURL = PDFURL + '&custparam_fromdate='+ fromdate;
	PDFURL = PDFURL + '&custparam_todate='+ todate;
	PDFURL = PDFURL + '&custparam_id='+ soid;

//	alert(WavePDFURL);
	window.open(PDFURL);
}
