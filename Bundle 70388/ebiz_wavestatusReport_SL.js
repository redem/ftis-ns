/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_wavestatusReport_SL.js,v $
 *     	   $Revision: 1.1.2.2.4.1.4.9.2.1 $
 *     	   $Date: 2015/09/11 22:34:29 $
 *     	   $Author: rrpulicherla $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_4 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 * *
 * DESCRIPTION
 *  	
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_wavestatusReport_SL.js,v $
 * Revision 1.1.2.2.4.1.4.9.2.1  2015/09/11 22:34:29  rrpulicherla
 * Case#201413343
 *
 * Revision 1.1.2.2.4.1.4.8  2015/07/28 14:39:38  rmalraj
 * case # 201413335
 * when custom specific pick report is required , based on PICKREPORTPRINT rule value custom specific pickreportPDF url is invoked
 *
 * Revision 1.1.2.2.4.1.4.7  2015/06/29 15:40:49  skreddy
 * Case# 201413186
 * CT Prod issue fix
 *
 * Revision 1.1.2.2.4.1.4.6  2015/03/16 10:29:22  schepuri
 * case# 201411866
 *
 * Revision 1.1.2.2.4.1.4.5  2014/05/23 06:45:47  gkalla
 * case#20148461
 * Dynacraft SB issue
 *
 * Revision 1.1.2.2.4.1.4.4  2014/04/16 15:53:15  gkalla
 * case#20127928
 * Dynacraft pick report link before cluster number generation
 *
 * Revision 1.1.2.2.4.1.4.3  2013/09/02 15:49:01  skreddy
 * Case# 20124183
 * standard bundle issue fix
 *
 * Revision 1.1.2.2.4.1.4.2  2013/08/22 07:00:52  snimmakayala
 * Case# 20123981
 * NLS - UAT ISSUES(WAVE STATUS REPORT)
 *
 * Revision 1.1.2.2.4.1.4.1  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.1.2.2.4.1  2012/10/29 20:43:34  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GUUSA UAT ISSUE FIXES
 *
 * Revision 1.1.2.2  2012/03/20 20:42:32  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Moved from Trunk to Branch
 *
 * Revision 1.2  2012/03/19 23:41:53  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes - TPP
 *
 * Revision 1.1.2.1  2012/03/19 06:08:55  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Wave statusreport
 *
 * Revision 1.1.2.2  2012/03/16 15:13:06  spendyala
 * CASE201112/CR201113/LOG201121
 * Added one more filter criteria to search the record .
 *
 * Revision 1.1.2.1  2012/03/15 23:05:34  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * cart checkin
 *
 * Revision 1.20  2012/01/06 13:00:50  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Added Remarks field to pick report to show failure for picks.
 *
 * Revision 1.19  2012/01/02 07:49:32  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.18  2011/10/31 11:39:48  gkalla
 * CASE201112/CR201113/LOG201121
 * To get the Wave#, fulfillment Order# and cluster#s in Sorting order
 *
 * Revision 1.17  2011/10/13 17:06:09  spendyala
 * CASE201112/CR201113/LOG201121
 * added printreport2 function for wavepickreport generation
 *
 * Revision 1.16  2011/10/12 20:00:15  spendyala
 * CASE201112/CR201113/LOG201121
 * uncommitted the display button
 *
 * Revision 1.15  2011/10/12 19:34:02  spendyala
 * CASE201112/CR201113/LOG201121
 * T&T issue is fixed
 *
 *
 *
 **********************************************************************************************************************/

function GetWavesOrders()
{
	var columnso = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_wave', null, 'greaterthan', 0));
	filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', ['15','9','26'])); // added just now

	columnso[0]=new nlobjSearchColumn('custrecord_ebiz_wave',null,'group').setSort(true);
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters,columnso);
	return searchresults;
}

function PickgeneratedReportSuitelet(request, response){
	if (request.getMethod() == 'GET') {

		var form = nlapiCreateForm('Wave Status Report');
		var vQbWave = "";

		form.setScript('customscript_pickreportprint');
		var WaveField = form.addField('custpage_wave', 'select', 'Wave');
		WaveField.setLayoutType('startrow', 'none');
		WaveField.addSelectOption("", "");

		form.addField('custpage_fromdate', 'date', 'From Date').setDisplayType('hidden');
		form.addField('custpage_todate', 'date', 'To Date').setDisplayType('hidden');

		var searchResultesRec= GetWavesOrders();
		if (searchResultesRec != null && searchResultesRec != "" && searchResultesRec.length>0) {
			for (var i = 0; i < searchResultesRec.length; i++) {
				if(searchResultesRec[i].getValue('custrecord_ebiz_wave',null,'group')!=null && searchResultesRec[i].getValue('custrecord_ebiz_wave',null,'group')!="")
				{
					var res=  form.getField('custpage_wave').getSelectOptions(searchResultesRec[i].getValue('custrecord_ebiz_wave',null,'group'), 'is');
					if (res != null) {				
						if (res.length > 0) {
							continue;
						}
					}

					WaveField.addSelectOption(searchResultesRec[i].getValue('custrecord_ebiz_wave',null,'group'), searchResultesRec[i].getValue('custrecord_ebiz_wave',null,'group'));
				}

			}
		}

		form.addSubmitButton('Display');

		response.writePage(form);

	}
	else
	{

		var form = nlapiCreateForm('Wave Status Report');
		var vQbWave = request.getParameter('custpage_wave');
		nlapiLogExecution('ERROR','vQbWave',vQbWave);
		//	var vQbdo = request.getParameter('custpage_do');
		var vQbFromDate = request.getParameter('custpage_fromdate');
		var vQbToDate = request.getParameter('custpage_todate');

		form.setScript('customscript_pickreportprint');


		var WaveField = form.addField('custpage_wave', 'select', 'Wave');
		WaveField.setLayoutType('startrow', 'none');

		var fromdate=form.addField('custpage_fromdate', 'date', 'From Date');

		fromdate.setDefaultValue(vQbFromDate);
		fromdate.setDisplayType('hidden');

		//adding To date
		var todate=form.addField('custpage_todate', 'date', 'To Date');
		todate.setDefaultValue(vQbToDate);
		todate.setDisplayType('hidden');

		WaveField.addSelectOption("", "");

		var searchResultesRec= GetWavesOrders();
		if (searchResultesRec != null && searchResultesRec != "" && searchResultesRec.length>0) {
			for (var i = 0; i < searchResultesRec.length; i++) {
				if(searchResultesRec[i].getValue('custrecord_ebiz_wave',null,'group')!=null && searchResultesRec[i].getValue('custrecord_ebiz_wave',null,'group')!="")
				{
					var res=  form.getField('custpage_wave').getSelectOptions(searchResultesRec[i].getValue('custrecord_ebiz_wave',null,'group'), 'is');
					if (res != null) {				
						if (res.length > 0) {
							continue;
						}
					}

					WaveField.addSelectOption(searchResultesRec[i].getValue('custrecord_ebiz_wave',null,'group'), searchResultesRec[i].getValue('custrecord_ebiz_wave',null,'group'));
				}

			}
		}


		WaveField.setDefaultValue(vQbWave);

		form.addSubmitButton('Display');

		var sublist = form.addSubList("custpage_items", "list", "ItemList");

		sublist.addField("custpage_waveno", "text", "Wave#");		
	//	sublist.addField("custpage_shipmentno", "text", "Fulfillment Order #");	
		sublist.addField("custpage_pickgenflag", "text", "Pick Gen Status");
		//sublist.addField("custpage_pickgendate", "text", "Pick Gen Date");
		//sublist.addField("custpage_pickgenstime", "text", "Pick Gen Start Time");
		//sublist.addField("custpage_pickgenetime", "text", "Pick Gen End Time");
		sublist.addField("custpage_remarks", "text", "Remarks");
		sublist.addField("custpage_checkin_link", "text", "");
		//sublist.addField("custpage_palletsign_link", "text", "");

		var otfilters = new Array();
		otfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', 3));
		//otfilters.push(new nlobjSearchFilter('custrecord_notes', null, 'isnotempty'));
		if (vQbWave != "" && vQbWave!=null) {			
			otfilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'contains', vQbWave));
		}
		else
		{
			otfilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'isnotempty'));
		}
		
		otfilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['15','9','26'])); // added just now
		
		
//		if((vQbFromDate!=null && vQbFromDate!="") && (vQbToDate!="" && vQbToDate!=null))
//		otfilters.push(new nlobjSearchFilter('custrecordact_begin_date', null, 'within', vQbFromDate, vQbToDate));


		var otcolumns = new Array();
		otcolumns[0] = new nlobjSearchColumn('custrecord_ebiz_wave_no',null,'group');		
		//otcolumns[1] = new nlobjSearchColumn('custrecord_notes',null,'group');
		//otcolumns[2] = new nlobjSearchColumn('name',null,'group');
		otcolumns[1] = new nlobjSearchColumn('custrecord_ebizmethod_no',null,'group');
		otcolumns[0].setSort();


		var otsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, otfilters, otcolumns);

		if(otsearchresults!=null && otsearchresults!='')
		{
			nlapiLogExecution('ERROR', 'otsearchresults',otsearchresults.length);
		}

		var filters = new Array();

		if (vQbWave != "" && vQbWave!=null) {			
			filters.push(new nlobjSearchFilter('custrecord_ebiz_wave', null, 'equalto', vQbWave));
		}
		else
		{
			filters.push(new nlobjSearchFilter('custrecord_ebiz_wave', null, 'isnotempty'));
		}
		
		filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', ['15','9','26'])); // added just now

//		if((vQbFromDate!=null && vQbFromDate!="") && (vQbToDate!="" && vQbToDate!=null))
//		filters.push(new nlobjSearchFilter('custrecord_ebiz_pickgen_date', null, 'within', vQbFromDate, vQbToDate));
		//filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof',['15','9']));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_wave',null,'group');		
		columns[1] = new nlobjSearchColumn('custrecord_linestatus_flag',null,'group');	
		//columns[2] = new nlobjSearchColumn('custrecord_lineord',null,'group');
		//columns[3] = new nlobjSearchColumn('custrecord_do_order_type',null,'group');
		//columns[4] = new nlobjSearchColumn('custrecord_ebiz_pickgen_date',null,'group');	
		//columns[5] = new nlobjSearchColumn('custrecord_ebiz_pickgen_starttime',null,'group');
		//columns[6] = new nlobjSearchColumn('custrecord_ebiz_pickgen_endttime',null,'group');
		columns[0].setSort();


		var searchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);

		var vwaveno,vfulfillmentno,vpickgenflag,vordlineno,vpickgenstatus,vshipmentno,vordertype;
		var vremarks='';
		var k=0;
		var vcompletewave='';
		var vcompletefo='';
		//201413186 start
		var pickMethodArray =new Array();
		for (var i = 0; otsearchresults != null && i < otsearchresults.length; i++)
		{
			var vPickMethod = otsearchresults[i].getValue('custrecord_ebizmethod_no',null,'group');	
			if(vPickMethod !=null && vPickMethod !='')
			{
				if(pickMethodArray.indexOf(vPickMethod)==-1)
				{
					pickMethodArray.push(vPickMethod);
				}
			}

		}
		nlapiLogExecution('ERROR', 'pickMethodArray',pickMethodArray);
		//var pickMethodIds=getpickmethod();
		var pickMethodIds=getpickmethod_New(pickMethodArray);
		//201413186 end
		if(searchresults!=null && searchresults!='')
		nlapiLogExecution('ERROR', 'searchresults.length',searchresults.length);
		
		
		
		
		for (var i = 0; searchresults != null && i < searchresults.length; i++) {
			var searchresult = searchresults[i];
			vwaveno = searchresult.getValue('custrecord_ebiz_wave',null,'group');			
			vpickgenflag = searchresult.getValue('custrecord_linestatus_flag',null,'group');	
		//	vshipmentno = searchresult.getValue('custrecord_lineord',null,'group');	
			//vordertype = searchresult.getValue('custrecord_do_order_type',null,'group');
			//vPickGenDate = searchresult.getValue('custrecord_ebiz_pickgen_date',null,'group');
			//vPickGenSTime = searchresult.getValue('custrecord_ebiz_pickgen_starttime',null,'group');
			//vPickGenETime = searchresult.getValue('custrecord_ebiz_pickgen_endttime',null,'group');

			//nlapiLogExecution('ERROR', 'vordertype',vordertype);
		//	nlapiLogExecution('Debug', 'Beforenullcluster',TimeStampinSec());
			var vNullClusters='';
			if( pickMethodIds !=null && pickMethodIds !='')
			 vNullClusters = vGetNullClusterRecs(vwaveno,pickMethodIds);
			//nlapiLogExecution('ERROR', 'vNullClusters',vNullClusters);
			var ctx = nlapiGetContext();
			var pickreport = '';
			
			var vScreenType,vScriptId,vDeployId,WavePDFURL;

			var ruleDet = IsCustomScreenRequired('PICKREPORTPRINT');
			if(ruleDet!=null && ruleDet!='')
			{
				vScreenType = ruleDet[0].getValue('custrecord_ebizrulevalue');
				vScriptId = ruleDet[0].getValue('custrecord_ebizrule_scriptid');
				vDeployId = ruleDet[0].getValue('custrecord_ebizrule_deployid');
			}
			if(vScreenType=='CUSTOM')
			{
				pickreport = nlapiResolveURL('SUITELET', vScriptId, vDeployId);
			}
			else
			{
				pickreport = nlapiResolveURL('SUITELET', 'customscript_pickreportpdf', 'customdeploy_pickreportpdf');
			}

			//pickreport = nlapiResolveURL('SUITELET', 'customscript_pickreportpdf', 'customdeploy_pickreportpdf');
			
			
			//case 20124183 start
			//pickreport = getFQDNForHost(ctx.getEnvironment()) + pickreport+ '&custparam_ebiz_wave_no='+ vwaveno;		
			pickreport = pickreport+ '&custparam_ebiz_wave_no='+ vwaveno;
			//end
			nlapiLogExecution('ERROR', 'vwaveno',vwaveno);
			var schfilter=new Array();
			schfilter.push(new nlobjSearchFilter('custrecord_ebiz_processtranrefno',null,'startswith',vwaveno));
			schfilter.push(new nlobjSearchFilter('custrecord_ebiz_processname',null,'is','WAVE RELEASE'));

			var Wavestatus;
			var schcolumn=new Array();
			schcolumn[0]=new nlobjSearchColumn('custrecord_ebiz_processstatus');
			schcolumn[1]=new nlobjSearchColumn('custrecord_ebiz_processname');
			schcolumn[2]=new nlobjSearchColumn('custrecord_ebiz_processtranrefno').setSort(true);

			var schedulersearchresult=nlapiSearchRecord('customrecord_ebiz_schedulescripts_status',null,schfilter,schcolumn);

			if(schedulersearchresult!=null && schedulersearchresult!='')
			{
				 Wavestatus = schedulersearchresult[0].getValue('custrecord_ebiz_processstatus');	

			}
			nlapiLogExecution('ERROR', 'vpickgenflag',vpickgenflag);
			nlapiLogExecution('ERROR', 'Wavestatus',Wavestatus);
			nlapiLogExecution('ERROR', 'vNullClusters',vNullClusters);
			if(vpickgenflag!=15 && Wavestatus=='Completed' && (vNullClusters == null || vNullClusters == '') )
			{
				
				nlapiLogExecution('ERROR', 'vpickgenflag',vpickgenflag);
				vpickgenstatus='Completed';

				for (var j = 0; searchresults != null && j < searchresults.length; j++) {
					var currwaveno =	searchresults[j].getValue('custrecord_ebiz_wave',null,'group');	
					//var currordno =	searchresults[j].getValue('custrecord_lineord',null,'group');	

					if(vwaveno==currwaveno )
					{
						var currpickgenflag = searchresults[j].getValue('custrecord_linestatus_flag',null,'group');	
						if(currpickgenflag==15)
							vpickgenstatus='In Progress';							
					}
				}

				if(vpickgenstatus=='Completed')
				{
					
					nlapiLogExecution('ERROR', 'vshipmentno',vshipmentno);
					nlapiLogExecution('ERROR', 'vcompletefo',vcompletefo);
					
					if(vwaveno!=vcompletewave)
					//if(vshipmentno!=vcompletefo)
					{
						vremarks='';
						for (var z = 0; otsearchresults != null && z < otsearchresults.length; z++) {
							var otwaveno=otsearchresults[z].getValue('custrecord_ebiz_wave_no',null,'group');	
							//var otfono=otsearchresults[z].getValue('name',null,'group');	

//							if(parseInt(vwaveno)==parseInt(otwaveno) && otfono==vshipmentno)
//							{
//								vremarks = otsearchresults[z].getValue('custrecord_notes',null,'group');													
//							}
						}
						nlapiLogExecution('ERROR', 'vcompletefo123',vcompletefo);
						k=k+1;
						form.getSubList('custpage_items').setLineItemValue('custpage_waveno', k, vwaveno);	
						form.getSubList('custpage_items').setLineItemValue('custpage_pickgenflag', k, vpickgenstatus);
						//form.getSubList('custpage_items').setLineItemValue('custpage_shipmentno', k, vshipmentno);
						form.getSubList('custpage_items').setLineItemValue('custpage_remarks', k, '<font color=#ff0000><b>' +  vremarks + '</b></font>');
						form.getSubList('custpage_items').setLineItemValue('custpage_checkin_link', k, '<a href="' + pickreport + '">Pick Report</a>');
//						form.getSubList('custpage_items').setLineItemValue('custpage_palletsign_link', k, '<a href="' + palletsignreport + '">Pallet Sign Report</a>');
//						form.getSubList('custpage_items').setLineItemValue('custpage_pickgendate', k, vPickGenDate);
//						form.getSubList('custpage_items').setLineItemValue('custpage_pickgenstime', k, vPickGenSTime);
//						form.getSubList('custpage_items').setLineItemValue('custpage_pickgenetime', k, vPickGenETime);
						vcompletewave = vwaveno;
						vcompletefo = vshipmentno;
					}
				}

			}
			else
			{
				vremarks='';
				k=k+1;
				vpickgenstatus='In Progress';					
				form.getSubList('custpage_items').setLineItemValue('custpage_waveno', k, vwaveno);				
				form.getSubList('custpage_items').setLineItemValue('custpage_pickgenflag', k, 'In Progress');
				//form.getSubList('custpage_items').setLineItemValue('custpage_shipmentno', k, vshipmentno);
				form.getSubList('custpage_items').setLineItemValue('custpage_remarks', k, '<font color=#ff0000><b>' +  vremarks + '</b></font>');
//				form.getSubList('custpage_items').setLineItemValue('custpage_pickgendate', k, vPickGenDate);
//				form.getSubList('custpage_items').setLineItemValue('custpage_pickgenstime', k, vPickGenSTime);
			}
		}
		response.writePage(form);

	}

}

function Printreport(ebizwaveno){

	nlapiLogExecution('ERROR', 'into Printreport ebizwaveno',ebizwaveno);
	var WavePDFURL = nlapiResolveURL('SUITELET', 'customscript_pickreportpdf', 'customdeploy_pickreportpdf');

	/*var ctx = nlapiGetContext();
	nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
	if (ctx.getEnvironment() == 'PRODUCTION') {
		WavePDFURL = 'https://system.netsuite.com' + WavePDFURL;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			WavePDFURL = 'https://system.sandbox.netsuite.com' + WavePDFURL;				
		}*/
	nlapiLogExecution('ERROR', 'Wave PDF URL',WavePDFURL);					
	WavePDFURL = WavePDFURL + '&custparam_ebiz_wave_no='+ ebizwaveno;
	nlapiLogExecution('ERROR', 'WavePDFURL',WavePDFURL);
	WavePDFURL;
	//window.open(WavePDFURL);

}

function GetWaves()
{
	var waveNo = "";
	var columnso = new Array();
	columnso[0]=new nlobjSearchColumn('custrecord_ebiz_wave').setSort('true');
	columnso[1]=new nlobjSearchColumn('custrecord_lineord');

	var searchresultswave = nlapiSearchRecord('customrecord_ebiznet_ordline', null, null,columnso);
	if(searchresultswave != null && searchresultswave.length > 0){
		waveNo = searchresultswave[0].getValue('custrecord_ebiz_wave');
	}
	return waveNo;
}

function vGetNullClusterRecs(vWave,pickMethodIds)
{
	var waveNo = "";

	nlapiLogExecution('Debug','pickMethodIds',pickMethodIds);
	if(pickMethodIds != null && pickMethodIds != '')
	{
		for(var s=0;s<pickMethodIds.length;s++)
		{
			var filters = new Array();
			filters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'isempty'));
			filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null,'contains', vWave));
			filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', 3));
			filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'noneof', 30));


			filters.push(new nlobjSearchFilter('custrecord_ebizmethod_no', null, 'is', pickMethodIds[s]));
			var searchresultsClust = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters,null);
			if(searchresultsClust != null && searchresultsClust != '')
				return searchresultsClust;
		}
		return null;
		
	}
	else
	{
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'isnotempty'));// case# 201411866
		filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null,'contains', vWave));
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', 3));

		var searchresultsClust = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters,null);
		return searchresultsClust;
	}	
}
function getpickmethod()
{
	nlapiLogExecution('Debug','ClustergetPickMethod','chkptTrue');
	var pickInternalId=new Array();
	var filter=new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizcreatecluster',null,'is','T'));
	var column=new Array();
	column[0]=new nlobjSearchColumn('internalid').setSort();
	column[1]=new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxorders');
	column[2]=new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxpicks');
	var searchresult=nlapiSearchRecord('customrecord_ebiznet_pick_method', null, filter, column);

	if(searchresult!=null)
	{
		for ( var i = 0; i < searchresult.length; i++)
		{

			pickInternalId.push(searchresult[i].getValue('internalid'));

		}
	}

	return pickInternalId;
}
function getpickmethod_New(pickMethodArray)
{
	nlapiLogExecution('Debug','ClustergetPickMethod _New',pickMethodArray);
	var pickInternalId=new Array();
	
	if(pickMethodArray !=null && pickMethodArray !=''  &&  pickMethodArray!='- None -' && pickMethodArray.length>0)
	{	

		var filter=new Array();
		filter.push(new nlobjSearchFilter('internalid',null,'anyof',pickMethodArray));
		filter.push(new nlobjSearchFilter('custrecord_ebizcreatecluster',null,'is','T'));
		var column=new Array();
		column[0]=new nlobjSearchColumn('internalid').setSort();
		column[1]=new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxorders');
		column[2]=new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxpicks');
		var searchresult=nlapiSearchRecord('customrecord_ebiznet_pick_method', null, filter, column);

		if(searchresult!=null && searchresult!='')
		{
			for ( var i = 0; i < searchresult.length; i++)
			{

				pickInternalId.push(searchresult[i].getValue('internalid'));

			}
		}
	}

	nlapiLogExecution('Debug','pickInternalId',pickInternalId);
	return pickInternalId;
}


function IsCustomScreenRequired(vScreen)
{
	var filters = new Array();
	var columns = new Array();
	filters.push(new nlobjSearchFilter('name', null, 'is', vScreen));	
	filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));	

	columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
	columns[1] = new nlobjSearchColumn('custrecord_ebizrule_scriptid');
	columns[2] = new nlobjSearchColumn('custrecord_ebizrule_deployid');

	var ruleDetails = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters, columns);

	return ruleDetails;

}