/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_ContainerManagement_SL.js,v $
 *     	   $Revision: 1.12.2.1.4.1.12.2 $
 *     	   $Date: 2015/11/14 13:01:35 $
 *     	   $Author: rrpulicherla $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_137 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_ContainerManagement_SL.js,v $
 * Revision 1.12.2.1.4.1.12.2  2015/11/14 13:01:35  rrpulicherla
 * 2015.2 issues
 *
 * Revision 1.12.2.1.4.1.12.1  2015/09/23 14:57:50  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.12.2.1.4.1  2012/11/01 14:55:02  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.12.2.1  2012/04/20 13:59:24  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.12  2011/12/21 09:05:52  schepuri
 * CASE201112/CR201113/LOG201121
 * multiselecting of checbox issue is fixed
 *
 * Revision 1.11  2011/12/12 13:04:13  snimmakayala
 * CASE201112/CR201113/LOG201121
 * spelling corrections in success message
 *
 * Revision 1.10  2011/12/12 08:41:22  rgore
 * CASE201112/CR201113/LOG201121
 * Sanitized error message for spelling and grammar.
 * - Ratnakar
 * 12 Dec 2011
 *
 * Revision 1.9  2011/09/12 15:18:17  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.8  2011/09/08 10:15:37  mbpragada
 * CASE201112/CR201113/LOG201121
 * Completed Container Management Logic.
 *
 * Revision 1.7  2011/09/08 10:06:21  mbpragada
 * CASE201112/CR201113/LOG201121
 * Completed Container Management Logic.
 *
 * Revision 1.6  2011/08/29 07:44:32  mbpragada
 * CASE201112/CR201113/LOG201121
 * Completed Container Management Logic.
 * Modularized the Container Management Logic.
 *
 * Revision 1.5  2011/08/26 07:55:19  mbpragada
 * CASE201112/CR201113/LOG201121
 * Completed Container Management Logic.
 * Modularized the Container Management Logic.
 *
 * Revision 1.4  2011/08/25 14:17:10  mbpragada
 * CASE201112/CR201113/LOG201121
 * Completed Container Management Logic.
 * Modularized the Container Management Logic.
 *
 * Revision 1.3  2011/08/24 15:04:15  mbpragada
 * CASE201112/CR201113/LOG201121
 * Completed Container Management Logic.
 *
 * Revision 1.2  2011/07/21 06:38:04  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/

/**
 * This function is to create Container Management Sublist.
 * This requires form to be sent as a parameter to build the sublist.
 */
function createContainerManagementSubList(form){
	var sublist = form.addSubList("custpage_items", "list", "Container Management");
	sublist.addField("custpage_select", "checkbox", "Confirm");
	sublist.addField("custpage_order", "text", "order #");
	sublist.addField("custpage_orderline", "text", "order Line #");
	sublist.addField("custpage_itemno", "select", "Item", "item");
	sublist.addField("custpage_lotbatch", "text", "LOT#");
	sublist.addField("custpage_existingcontainer", "text", "Existing Container");
	sublist.addField("custpage_newcontainer", "text", "New Container").setDisplayType('entry');
	sublist.addField("custpage_newcontainersize", "select", "New Container Size","customrecord_ebiznet_container").setDisplayType('entry');
	sublist.addField("custpage_quantity", "text", "Quantity");
	sublist.addField("custpage_newquantity", "text", "New Quantity").setDisplayType('entry');
	sublist.addField("custpage_internalid", "text", "Internal Id").setDisplayType('hidden');
	sublist.addField("custpage_sointernalid", "text", "SO Internal Id").setDisplayType('hidden');
	sublist.addField("custpage_wmslocation", "text", "Wms location").setDisplayType('hidden');
}

/**
 * This function is to specify the container management filters.
 * 	This function returns an array of filter criteria specified.
 * @param orderno
 * @returns {Array}
 */
function specifyContainerManagementFilters(orderno,containerno,itemno){
	var filters = new Array();

	if(orderno != ""){
		nlapiLogExecution('ERROR','Inside orderno',orderno);
		filters.push(new nlobjSearchFilter('name', null, 'is', orderno));			
	}
	if(containerno != ""){
		nlapiLogExecution('ERROR','Inside containerno',containerno);		 
		filters.push(new nlobjSearchFilter('custrecord_lpno', null, 'is', containerno));		 
	}
	if(itemno != ""){
		nlapiLogExecution('ERROR','Inside itemno',itemno);		 
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'is', itemno));
	}

	filters.push( new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));

	return filters;
}

/**
 * This function is to specify the container management columns.
 * 	This returns an array of columns to be fetched from the search record.
 * @returns {Array}
 */
function specifyContainerManagementColumns(){
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_lpno');
	columns[2] = new nlobjSearchColumn('custrecord_sku');
	columns[3] = new nlobjSearchColumn('custrecord_act_qty');
	columns[4] = new nlobjSearchColumn('custrecord_line_no');
	columns[5] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[6] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[7] = new nlobjSearchColumn('custrecord_wms_location');

	columns[0].setSort();
	
	return columns;
}

/**
 * This function is to add the container details to the sublist.
 * 	The container details to the container management screen include order, order line number,
 * 		container LP number, quantity, record id, sales order internal id
 * @param form
 * @param currentRecord
 * @param i
 */
function addContainerDetailsToSubList(form, currentRecord, i){
	form.getSubList('custpage_items').setLineItemValue('custpage_order', i + 1, currentRecord.getValue('name'));
	form.getSubList('custpage_items').setLineItemValue('custpage_orderline', i + 1, 
			currentRecord.getValue('custrecord_line_no'));
	form.getSubList('custpage_items').setLineItemValue('custpage_itemno', i + 1, 
			currentRecord.getValue('custrecord_sku'));
	form.getSubList('custpage_items').setLineItemValue('custpage_existingcontainer', i + 1, 
			currentRecord.getValue('custrecord_container_lp_no'));
	form.getSubList('custpage_items').setLineItemValue('custpage_quantity', i + 1, 
			currentRecord.getValue('custrecord_act_qty'));
	form.getSubList('custpage_items').setLineItemValue('custpage_internalid', i + 1, 
			currentRecord.getId());
	form.getSubList('custpage_items').setLineItemValue('custpage_sointernalid', i + 1, 
			currentRecord.getValue('custrecord_ebiz_order_no'));
	form.getSubList('custpage_items').setLineItemValue('custpage_wmslocation', i + 1, 
			currentRecord.getValue('custrecord_wms_location'));
}

/**
 * This function is to perform insert fields in LP Master and update values into Open task if it is new LP .
 * Other wise we will check whether container LP is exists in open task or not if it exists in open task we will
 * check for the order number corresponding to the LP.If both are same then we we will proceed to update other wise we will throw an error.
 * @param request
 * @param form
 */

function performContainerManagementOperations(request,form)
{
	try
	{
		var LineCount = request.getLineItemCount('custpage_items');
		var newContainer ="";
		var newContainerSize ="";
		var newQty = "";
		var internalId = ""; 
		var flag = "";
		var LPInOpentask = new Array();
		var matchFound = true;
		var totalCube = 0;
		var totalWeight = 0;
		var falseCount = 0;
		var oldContainer="";
		var wmslocation="";

		for(var s=1; s <= LineCount; s++){
			flag = request.getLineItemValue('custpage_items', 'custpage_select', s);

			if(flag == "T"){
				newContainer = request.getLineItemValue('custpage_items', 'custpage_newcontainer', s);
				newContainerSize = request.getLineItemValue('custpage_items', 'custpage_newcontainersize', s);
				newQty = request.getLineItemValue('custpage_items', 'custpage_newquantity', s);
				itemId = request.getLineItemValue('custpage_items', 'custpage_itemno', s);
				internalId = request.getLineItemValue('custpage_items', 'custpage_internalid', s);
				salesOrderInternalId = request.getLineItemValue('custpage_items', 'custpage_sointernalid', s);
				oldContainer = request.getLineItemValue('custpage_items', 'custpage_existingcontainer', s);
				wmslocation=request.getLineItemValue('custpage_items', 'custpage_wmslocation', s);
				nlapiLogExecution('ERROR', 'internalId ', internalId);
				nlapiLogExecution('ERROR', 'newContainer ', newContainer);
				nlapiLogExecution('ERROR', 'newContainerSize ', newContainerSize);
				nlapiLogExecution('ERROR', 'newQty ', newQty);
				nlapiLogExecution('ERROR', 'oldContainer ', oldContainer);
				var contLPExists = containerLPExists(newContainer);
				if(contLPExists != null){
					LPInOpentask = checkLPInOpentask(form, newContainer, itemId, newQty, salesOrderInternalId);
				}
				else{
					createNewLPRecord(itemId, newContainerSize, newContainer,newQty);
					falseCount = 0;
				}
				/*	update values to opentask with the values newContainer, 
				 * 		newContainerSize, newQty, internalId
				 */
				if(LPInOpentask != null && LPInOpentask.length > 0){
					nlapiLogExecution('ERROR','LPInOpentask FOR LOOP');
					for(var i = 0; i < LPInOpentask.length; i++){
						matchFound = LPInOpentask[i][1];
						totalCube = LPInOpentask[i][2];
						totalWeight = LPInOpentask[i][3];
						if(matchFound == false){
							falseCount = parseFloat(falseCount) + 1;
						}
					}
				}
				if(falseCount == 0){
					updateInOpenTask(internalId, newContainer, newContainerSize, totalCube, totalWeight);
					form.getSubList('custpage_items').setLineItemValue('custpage_existingcontainer', s, newContainer);
					/*var message = 'Sales Order has been assigned to a Container# ';
					//var messageVar = ' Container# = ' + newContainer;
					var messageVar ="";
					nlapiLogExecution('ERROR', 'showInlineMessage message', message);
					nlapiLogExecution('ERROR', 'showInlineMessage messageVar', messageVar);
					showInlineMessage(form, 'Confirmation', message, messageVar);*/
					
					
					var filtersinvt = new Array();
					var columnsinvt = new Array();
					columnsinvt[0]=new nlobjSearchColumn('custrecord_ebiz_qoh');
					filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', oldContainer));
					filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [18]));
					filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', itemId));
					


					var searchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);

					if(searchresults!=null && searchresults!='')
					{
						for (var x = 0; searchresults != null && x < searchresults.length; x++) 
						{
						
						var fieldinvt = new Array(); 
						fieldinvt.push('custrecord_ebiz_inv_lp');  
						

						var newValuesinvt = new Array(); 
						newValuesinvt.push(newContainer);
						

						nlapiSubmitField('customrecord_ebiznet_createinv', searchresults[x].getId(), fieldinvt, newValuesinvt);

						}

					}
					
					
					var msg1 = form.addField('custpage_messages', 'inlinehtml', null, null, null);
					msg1.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Sales Order has been assigned to Container(s) ', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
					//response.writePage(form);
				}
				else{
					var message = 'Container LP already assigned to another sales order ';
					var messageVar = ' Container LP# = ' + newContainer;
					showInlineMessage(form, 'Error', message, messageVar);
					response.writePage(form);
				}
			}
		}
	}	
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception',exp);
	}
	nlapiLogExecution('ERROR','Container Move Successful');
}

/**
 * This function is to update Container,container size and quantity in Open Task.
 * @param newContainer
 * @param newContainerSize
 * @param newQty
 * @param internalId
 */
 
function updateValuesInOpentask(newContainer, newContainerSize, newQty, internalId){
	var fields = new Array();
	var values = new Array();
	fields[0] = 'custrecord_lpno';
	fields[1] = 'custrecord_contlp_no';
	fields[2] = 'custrecord_pick_qty';

	values[0] = newContainer;
	values[1] = newContainerSize;
	values[2] = parseFloat(newQty).toFixed(5); 

	var updatefields = nlapiSubmitField('customrecord_ebiznet_trn_opentask', internalId, fields, values);
}

/**
 * This function is to create a new LP record in LP Master.
 * @param itemId
 * @param newContainerSize
 * @param newContainer
 */
function createNewLPRecord(itemId, newContainerSize, newContainer,newQty){
	var totalItemCubeWeight = getTotalCubeWeight(itemId,newQty);
	var arrContainerDetails = getContainerCubeAndTarWeight(newContainerSize,"");
	var containerCube=0;
	var totalWeight=0;
	if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))){
		containerCube =  parseFloat(arrContainerDetails[0]);
		totalWeight = (parseFloat(totalItemCubeWeight[1]) + parseFloat(arrContainerDetails[1]));
	} 
	//nlapiLogExecution('ERROR', 'ContainerManagement:ContainerCube', containerCube);
	//nlapiLogExecution('ERROR', 'ContainerManagement:TotalWeight', totalWeight);
	createLPRecord(newContainer,newContainerSize,containerCube,totalWeight);
}

/**
 * This function checks whether the given LP is exists in Open task or not
 * @param newContainer
 * @param itemId
 * @param newContainerSize
 * @param internalId
 * @returns {Boolean}
 */
function checkLPInOpentask(form, newContainer, itemId, newQty, salesOrderInternalId){
	var matchFound = false;
	var LPInOpentask = new Array();
	var totalItemCubeWeight = new Array();
	
	var opentaskLPExist = opentaskLPExists(newContainer);
	if(opentaskLPExist != null){
		for (var i = 0; i < opentaskLPExist.length; i++){
			var salesOrderNumber = opentaskLPExist[i].getValue('custrecord_ebiz_order_no');
			if (salesOrderNumber == salesOrderInternalId){

				matchFound = true;
			}
			else{
				matchFound = false;
			}
			totalItemCubeWeight = getTotalCubeWeight(itemId,newQty);

			var currentRow = [i, matchFound, totalItemCubeWeight[0], totalItemCubeWeight[1]];
			LPInOpentask.push(currentRow);
		}
	}
	return LPInOpentask;
}

/**
 * This function returns Cube and weight for given Item.
 * @param itemId
 * @returns {Array}
 */
function getTotalCubeWeight(itemId,newQty){

	var itemCubeAndWeight = getSKUCubeAndWeightforconfirm(itemId,1);

	var itemCube = 0;
	var itemWeight=0;
	var totalItemCubeWeight = new Array();

	if (itemCubeAndWeight[0] != "" && (!isNaN(itemCubeAndWeight[1]))){
		itemCube = (parseFloat(newQty) * parseFloat(itemCubeAndWeight[0]));
		itemWeight = (parseFloat(newQty) * parseFloat(itemCubeAndWeight[1]));
		//nlapiLogExecution('ERROR', 'ContainerManagement:itemCube', itemCube);
		//nlapiLogExecution('ERROR', 'ContainerManagement:itemweight', itemWeight);

		totalItemCubeWeight[0] = itemCube;
		totalItemCubeWeight[1] = itemWeight;
	}

	return totalItemCubeWeight;
}


/**
 * This function will update record in open task if given given container LP matched with the LP number of existing order.
 * @param recordId
 * @param container
 * @param containerSize
 */
function updateInOpenTask(recordId, container, containerSize, totalItemCube, totalItemWeight){
	nlapiLogExecution('ERROR', 'open task recordId ', recordId);
	var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recordId);
	transaction.setFieldValue('custrecord_container_lp_no', container);
	transaction.setFieldValue('custrecord_container', containerSize);
	transaction.setFieldValue('custrecord_totalcube', parseFloat(totalItemCube).toFixed(5));
	transaction.setFieldValue('custrecord_total_weight', parseFloat(totalItemWeight).toFixed(5));
	 
	nlapiSubmitRecord(transaction, false, true);
}

/**
 * This function will create a new record in LP Master if the LP does not exists.
 * @param lp
 * @param newContainerSize
 * @returns recid
 */
function createLPRecord(lp, newContainerSize,ContainerCube,TotalWeight) {
	var lpRecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');

	lpRecord.setFieldValue('name', lp);
	lpRecord.setFieldValue('custrecord_ebiz_lpmaster_lp', lp);
	lpRecord.setFieldValue('custrecord_ebiz_lpmaster_sizeid', newContainerSize);
	lpRecord.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(ContainerCube).toFixed(5));
	lpRecord.setFieldValue('custrecord_ebiz_lpmaster_totwght', parseFloat(TotalWeight).toFixed(5));

	var recid = nlapiSubmitRecord(lpRecord);
}

/**
 * This function is to check if the container LP entered exists in LP Master
 * NOTE: This function returns search results. search results may be NULL.
 * @param lp
 * @returns search results
 */

function containerLPExists(lp){
	var filters = new Array();
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lp));	 

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
	columns[0].setSort();

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);	
	return searchresults;
}

/**
 * This function is to check if the container LP exists in the open task . If exists, check for the 
 * 	order number corresponding to the LP.
 * NOTE: This function returns search results. search results may return NULL.
 * @param lp
 * @returns search results
 */
function opentaskLPExists(lp){
	var filters = new Array();
	filters.push( new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', lp));	 

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_order_no');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns); 
	return searchresults;
}
/**
 * This function returns the  searchResults to fill Order,container and Item drop downs  in header
 */
function getAllheaderDetails()
{
	var filters = new Array();
	// Add filter criteria for WMS Status Flag ;
	// Search for C:'Pick Confirmed';
	filters.push( new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));
	// Add filter criteria for Task Type;
	// Search for Task Type = 'PICK';
	filters.push( new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_lpno');
	columns[2] = new nlobjSearchColumn('custrecord_sku');
	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	
	
	return searchresults;
}
/**
 * This is the main function which performs the complete Container Management process.
 */

function ebiz_ContainerManagement_SL(request, response)
{
	if (request.getMethod() == 'GET'){
		var form = nlapiCreateForm('Container Management');

		var order = form.addField('custpage_order', 'select', 'Order #');
		var container= form.addField('custpage_container', 'select', 'Container #');
		var item = form.addField('custpage_item', 'select', 'Item');

		order.addSelectOption("", "");
		container.addSelectOption("", "");
		item.addSelectOption("", "");
		 
		var searchresults = getAllheaderDetails();

		if(searchresults){
			for (var i = 0; i < searchresults.length; i++){
				// This is for order numbers to avoid duplicates.
				var orderResult = form.getField('custpage_order').getSelectOptions(searchresults[i].getValue('name'), 'is');
				if (orderResult != null) {
					if (orderResult.length > 0) {
						continue;
					}
				}
				order.addSelectOption(searchresults[i].getValue('name'), searchresults[i].getValue('name'));

				// This is for container numbers to avoid duplicates.
				var ContainerResult = form.getField('custpage_container').getSelectOptions(searchresults[i].getValue('custrecord_lpno'), 'is');
				if (ContainerResult != null) {
					if (ContainerResult.length > 0) {
						continue;
					}
				}
				container.addSelectOption(searchresults[i].getValue('custrecord_lpno'), searchresults[i].getValue('custrecord_lpno'));

				// This is for Item numbers to avoid duplicates.
				var ItemResult = form.getField('custpage_item').getSelectOptions(searchresults[i].getText('custrecord_sku'), 'is');
				if (ItemResult != null) {
					if (ItemResult.length > 0) {
						continue;
					}
				}
				item.addSelectOption(searchresults[i].getValue('custrecord_sku'), searchresults[i].getText('custrecord_sku'));
			}
		}	

		var button = form.addSubmitButton('Display');
		response.writePage(form);
	}
	else
	{
		
		var orderno;
		var form = nlapiCreateForm('Container Management');
		form.setScript('customscript_containermangement_cl');
		var orderno = request.getParameter('custpage_order');
		var containerno = request.getParameter('custpage_container');
		var itemno = request.getParameter('custpage_item');
		
		//inline
		var ordernos = form.addField('custpage_order', 'text', 'Order #').setDisplayType('hidden').setDefaultValue(orderno);
		var containernos = form.addField('custpage_container', 'text', 'Container #').setDisplayType('hidden').setDefaultValue(containerno);
		var itemnos = form.addField('custpage_item', 'select', 'Item','item').setDisplayType('hidden').setDefaultValue(itemno);

		createContainerManagementSubList(form);
		
		var filters = specifyContainerManagementFilters(orderno,containerno,itemno);
		var columns = specifyContainerManagementColumns();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

		if(searchresults){
			for (var i = 0; i < searchresults.length; i++) {
				var currentRecord = searchresults[i];
				addContainerDetailsToSubList(form, currentRecord, i);
			}
		}
		performContainerManagementOperations(request,form);
		var button = form.addSubmitButton('Submit');
		response.writePage(form);		 
	}
}