/***************************************************************************
  eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *  $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_TwoStepReplenQty.js,v $
<<<<<<< ebiz_RF_TwoStepReplenQty.js
 *  $Revision: 1.1.2.5 $
 *  $Date: 2015/07/21 15:07:09 $
 *  $Author: grao $
 *  $Name: b_WMS_2015_2_StdBundle_Issues $
=======
 *  $Revision: 1.1.2.5 $
 *  $Date: 2015/07/21 15:07:09 $
 *  $Author: grao $
 *  $Name: b_WMS_2015_2_StdBundle_Issues $
>>>>>>> 1.1.4.3
 *
 * eBizNET version and checksum stamp.  Do not remove.
 * $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_TwoStepReplenQty.js,v $
 * Revision 1.1.2.5  2015/07/21 15:07:09  grao
 * 2015.2   issue fixes  201413101
 *
 * Revision 1.1.2.4  2015/05/08 13:52:17  snimmakayala
 * Case#:201412644
 * Posting adjustments wrongly when performing qty exception.
 *
 * Revision 1.1.4.4  2015/04/30 15:20:35  grao
 * SB issue fixes  201412546
 *
 * Revision 1.1.4.3  2015/04/29 09:53:38  skreddy
 * Case# 201412469
 * Stnadard bundle issue fix
 *
 * Revision 1.1.4.2  2015/04/13 16:14:07  snimmakayala
 * Case#:201411349
 * Two Step Replenishment
 *
 * Revision 1.1.2.1  2015/01/02 15:05:35  skreddy
 * Case# 201411349
 * Two step replen process
 *

 *****************************************************************************/

function Replenishment_Qty(request, response){
	if (request.getMethod() == 'GET') {
		var reportNo = request.getParameter('custparam_repno');
		var beginLoc = request.getParameter('custparam_repbegloc');
		var sku = request.getParameter('custparam_repsku');
		var skuNo = request.getParameter('custparam_repskuno');
		var expQty = request.getParameter('custparam_repexpqty');
		var repInternalId = request.getParameter('custparam_repid');
		var clustNo = request.getParameter('custparam_clustno');
		var whlocation = request.getParameter('custparam_whlocation');
		var actEndLocationId = request.getParameter('custparam_reppicklocno');
		var actEndLocation = request.getParameter('custparam_reppickloc');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var nextlocation=request.getParameter('custparam_nextlocation');
		var nextqty=request.getParameter('custparam_nextqty');
		var batchno=request.getParameter('custparam_batchno');
		var fromlpno=request.getParameter('custparam_fromlpno');
		var tolpno=request.getParameter('custparam_tolpno');
		var nextitem=request.getParameter('custparam_nextitem');
		var nextitemno=request.getParameter('custparam_nextitemno');
		var taskpriority = request.getParameter('custparam_entertaskpriority');
		var getreportNo=request.getParameter('custparam_enterrepno');
		var getitem = request.getParameter('custparam_entersku');
		var getitemid = request.getParameter('custparam_enterskuid');
		var getloc = request.getParameter('custparam_enterpickloc');
		var getlocid = request.getParameter('custparam_enterpicklocid');
		var replenType = request.getParameter('custparam_replentype');
		var getNumber = request.getParameter('custparam_number');
		var beginLocId = request.getParameter('custparam_repbeglocId');
		var enterfromloc = request.getParameter('custparam_enterfromloc');
		var getLanguage = request.getParameter('custparam_language');
		var cartlp = request.getParameter('custparam_cartlpno');
		var btnValue = request.getParameter('hdnclickedbtnid');
		var vzone = request.getParameter('custparam_zoneno');
		var vbatchId = request.getParameter('custparam_batchnoId');

		var html=getQTYScan(beginLoc, reportNo, sku, expQty, skuNo, repInternalId, 
				clustNo, whlocation, actEndLocationId, actEndLocation, beginLocId,RecordCount,fromlpno,tolpno,
				nextlocation,nextqty,batchno,nextitem,nextitemno,taskpriority,replenType,getitem,getitemid,getloc,
				getlocid,getreportNo,getNumber,enterfromloc,getLanguage,cartlp,btnValue,vzone,vbatchId);        
		response.write(html);
	}
	else
	{

		try {

			var Reparray = new Array();

			var optedEvent = request.getParameter('hdnclickedbtnid');
			nlapiLogExecution('ERROR', 'optedEvent', optedEvent);
			if(optedEvent==null || optedEvent  =='null' || optedEvent  == '' )
			{
				optedEvent =request.getParameter('cmdPutaway');
			}
			if(optedEvent==null || optedEvent  =='null' || optedEvent  == '' )
			{
				optedEvent =request.getParameter('cmdStage');
			}
			if(optedEvent==null || optedEvent  =='null' || optedEvent  == '' )
			{
				optedEvent =request.getParameter('cmdException');
			}
			if(optedEvent==null || optedEvent  =='null' || optedEvent  == '' )
			{
				optedEvent =request.getParameter('cmdEnt');
			}
			if(optedEvent==null || optedEvent  =='null' || optedEvent  == '' )
			{
				optedEvent =request.getParameter('cmdPrev');
			}

			var fromlp = request.getParameter('enterreplenlp');		
			var hdnBeginLocation = request.getParameter('hdnBeginLocation');
			var hdnBeginLocationId = request.getParameter('hdnBeginLocationId');
			var hdnrepLpno=request.getParameter('hdnfromLpno');			
			var clusterNo = request.getParameter('hdnclustno');		
			var RecordCount = request.getParameter('hdnRecCount');			
			var ExpQty=request.getParameter('hdnExpQty');
			var ReportNo=request.getParameter('hdnNo');
			var recid=request.getParameter('hdnid');
			var sku=request.getParameter('hdnSku');
			var skuno=request.getParameter('hdnSkuNo');
			var replnQty=request.getParameter('enterreplenqty');
			if(replnQty==null || replnQty=='')
				replnQty=0;
			var vZoneId=request.getParameter('hdnvzone');

			var remainingreplenqty = request.getParameter('enterremainingreplenqty');
			var reportNo = request.getParameter('custparam_repno');
			//case20125658 and 20125602 start:spanish conversion
			var getLanguage = request.getParameter('hdngetLanguage');
			Reparray["custparam_language"] = getLanguage;
			nlapiLogExecution('ERROR', 'getLanguage', Reparray["custparam_language"]);
			nlapiLogExecution('ERROR', 'vZoneId', vZoneId);
			var vBatchID = request.getParameter('hdnbatchId');
			nlapiLogExecution('ERROR', 'vBatchID', vBatchID);
			//case20125658 and 20125602 end
			var st0,st1,st2,st3;
			if( getLanguage == 'es_ES' || getLanguage =='es_AR')
			{
				st0 = "LP NO V&#193;LIDA";
				st1 = "LP no coinciden";
				st2 = "LP ES NULO";
				st3 = "Cantidad entr&#243; no debe ser mayor que Replen genera Cantidad";
			}
			else
			{
				st0 = "INVALID LP";
				st1 = "LP DO NOT MATCH";
				st2 = "LP IS NULL ";
				st3 = "Quantity entered should not be greater than Replen generated qty";
			}

			if(request.getParameter('hdngetNumber')!= null && request.getParameter('hdngetNumber') != "")
				getNumber = request.getParameter('hdngetNumber');
			else
				getNumber=0;	

			if(fromlp==null || fromlp=='')
				fromlp=hdnrepLpno;

			Reparray["custparam_repno"] = request.getParameter('hdnNo');
			Reparray["custparam_repbegloc"] = request.getParameter('hdnBeginLocation');
			Reparray["custparam_repsku"] = request.getParameter('custparam_repsku');
			Reparray["custparam_torepexpqty"] = request.getParameter('hdnExpQty');
			Reparray["custparam_repexpqty"] = request.getParameter('hdnnextqty');
			Reparray["custparam_repid"] = request.getParameter('hdnid');
			Reparray["custparam_repskuno"] = request.getParameter('hdnSkuNo');
			Reparray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
			Reparray["custparam_error"] =st0; //'INVALID LP';
			Reparray["custparam_screenno"] = '2StepRpQty';
			Reparray["custparam_clustno"] = clusterNo;	
			Reparray["custparam_reppicklocno"] = request.getParameter('hdnactendlocationid');
			Reparray["custparam_reppickloc"] = request.getParameter('hdnactendlocation');
			Reparray["custparam_repbeglocId"] = request.getParameter('hdnBeginLocationId');
			Reparray["custparam_noofrecords"] = request.getParameter('hdnRecCount');
			Reparray["custparam_fromlpno"] = request.getParameter('hdnfromLpno');
			Reparray["custparam_tolpno"] = request.getParameter('hdntoLpno');
			Reparray["custparam_nextlocation"] = request.getParameter('hdnnextloc');
			Reparray["custparam_nextqty"] = request.getParameter('hdnnextqty');
			Reparray["custparam_batchno"] = request.getParameter('hdnbatchno');
			Reparray["custparam_nextitem"] = request.getParameter('hdnnitem');
			Reparray["custparam_nextitemno"] = request.getParameter('hdnnextitemno');
			Reparray["custparam_repexpqty"] = request.getParameter('hdnExpQty');
			Reparray["custparam_entertaskpriority"] = request.getParameter('hdntaskpriority');
			Reparray["custparam_number"] = request.getParameter('hdngetNumber');
			Reparray["custparam_enterskuid"] = request.getParameter('hdngetitemid');
			Reparray["custparam_entersku"] = request.getParameter('hdngetitem');
			Reparray["custparam_enterpicklocid"] = request.getParameter('hdngetlocid');
			Reparray["custparam_enterpickloc"] = request.getParameter('hdngetloc');
			Reparray["custparam_replentype"] = request.getParameter('hdnReplenType');
			Reparray["custparam_enterrepno"] = request.getParameter('hdngetreportno');
			Reparray["custparam_remainingreplenqty"] = remainingreplenqty;
			Reparray["custparam_enterfromloc"] = request.getParameter('hdnenterfromloc');
			Reparray["custparam_cartlpno"] = request.getParameter('hdnCartlpno');
			Reparray["custparam_zoneno"] = vZoneId;	
			Reparray["custparam_batchnoId"] = vBatchID;
			var getitemid = request.getParameter('hdngetitemid');
			var getlocid = request.getParameter('hdngetlocid');
			var taskpriority = request.getParameter('hdntaskpriority');
			var taskpriority = request.getParameter('hdntaskpriority');
			var getreportNo = request.getParameter('hdnNo');
			var replenType = request.getParameter('hdnReplenType');
			var getBinLocationId=request.getParameter('hdngetlocid');
			var getCartLP = request.getParameter('hdnCartlpno');
			var vBatchName=request.getParameter('hdnbatchno');
			nlapiLogExecution('ERROR', 'getCartLP' ,getCartLP);
			nlapiLogExecution('ERROR', 'optedEvent' ,optedEvent);
			nlapiLogExecution('ERROR', 'batch' ,vBatchName);
			if (optedEvent == 'F10') {
				nlapiLogExecution('ERROR', 'PREV  F10 Pressed');
				nlapiLogExecution('ERROR', 'request.getParametercustparam_repskuno' ,request.getParameter('custparam_repskuno'));

				var ItemType = nlapiLookupField('item', request.getParameter('custparam_repskuno'), ['recordType','custitem_ebizbatchlot']);
				nlapiLogExecution('ERROR', 'ItemType.recordTyp',ItemType.recordType);
				if (ItemType.recordType == 'lotnumberedinventoryitem' ||ItemType.recordType == "lotnumberedassemblyitem" || ItemType.custitem_ebizbatchlot=='T' ) 
				{
					response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplen_batch', 'customdeploy_ebiz_twostepreplen_batch_di', false, Reparray);
				}
				else
					response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenitem', 'customdeploy_ebiz_twostepreplenitem', false, Reparray);

			}
			else if(optedEvent == 'F9')
			{
				try
				{
					var filters = new Array();
					filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['8']));
					if(getreportNo !=null && getreportNo !='' && getreportNo !='null')
					{
						filters.push(new nlobjSearchFilter('name', null, 'is', getreportNo));
					}
					if(request.getParameter('custparam_repskuno')!=null && request.getParameter('custparam_repskuno')!='' && request.getParameter('custparam_repskuno')!='null')
					{
						nlapiLogExecution('ERROR','Replenitem',request.getParameter('custparam_repskuno'));
						filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', request.getParameter('custparam_repskuno')));

					}

					if(getlocid !=null && getlocid !='' && getlocid !='null')
					{
						nlapiLogExecution('ERROR','Replenprimaryloc',getlocid);
						filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', getlocid));

					}

					if(request.getParameter('custparam_whlocation')!=null && request.getParameter('custparam_whlocation')!='' && request.getParameter('custparam_whlocation')!='null')
					{
						nlapiLogExecution('ERROR','custrecord_wms_location',request.getParameter('custparam_whlocation'));
						filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', request.getParameter('custparam_whlocation')));

					}

					if(vZoneId !=null && vZoneId !='' && vZoneId !='null')
					{
						nlapiLogExecution('ERROR','vZoneId',vZoneId);
						filters.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', vZoneId));

					}

					if(request.getParameter('hdnbatchno')!=null && request.getParameter('hdnbatchno')!='' && request.getParameter('hdnbatchno')!='null')
					{
						nlapiLogExecution('ERROR','batchno',request.getParameter('hdnbatchno'));
						filters.push(new nlobjSearchFilter('custrecord_batch_no', null, 'is', request.getParameter('hdnbatchno')));

					}



					var columns = new Array();
					columns.push(new nlobjSearchColumn('custrecord_expe_qty'));
					var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
					nlapiLogExecution('DEBUG', 'searchresults', searchresults);       

					if(searchresults != null && searchresults != '')
					{

						var fields = new Array();
						fields[0]=['custrecord_wms_status_flag'];
						fields[1]=['custrecord_act_end_date'];
						fields[2]=['custrecord_actualendtime'];
						fields[3]=['custrecord_transport_lp'];
						fields[4]=['custrecord_act_qty'];
						var values = new Array();
						values[0]='24';
						values[1]= DateStamp();
						values[2]=TimeStamp();
						values[3]=getCartLP;

						for(var m=0;m<searchresults.length;m++)
						{
							values[4]=searchresults[m].getValue('custrecord_expe_qty');
							nlapiSubmitField('customrecord_ebiznet_trn_opentask',searchresults[m].getId(),fields,values);
							nlapiLogExecution('ERROR', 'searchresults[m].getId()' ,searchresults[m].getId());

						}
					}
					var filtersmlp = new Array(); 
					filtersmlp[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', request.getParameter('hdnCartlpno'));

					var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

					if (SrchRecord != null && SrchRecord.length > 0) {
						var rec=nlapiLoadRecord('customrecord_ebiznet_master_lp',SrchRecord[0].getId());
						rec.setFieldValue('custrecord_ebiz_cart_closeflag', 'T');

						var recid = nlapiSubmitRecord(rec, false, true);
						if(recid!=null)
						{
							Reparray["custparam_cartlp"] = request.getParameter('hdnCartlpno');
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_twostp_scanloc', 'customdeploy_ebiz_rf_twostp_scanloc', false, Reparray);
							return;
						}


					}
					else {
						nlapiLogExecution('DEBUG', 'cart LP NOT FOUND');
						Reparray["custparam_error"] = 'Invalid CartLP.';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
						return;
					}
					nlapiLogExecution('ERROR', 'PREV  F9 Pressed');
				}
				catch(e)
				{
					nlapiLogExecution('ERROR', 'exception ',e);
				}

			}
			else if (optedEvent =='F8')
			{
				nlapiLogExecution('ERROR', 'STAGE  F8 Pressed');
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_twostp_cnfcartclose', 'customdeploy_ebiz_rf_twostp_cnfcartclose', false, Reparray);
			}
			else if(optedEvent =='F7')
			{
				nlapiLogExecution('ERROR', 'EXCEPTION  F7 Pressed');


				if((isNaN(replnQty) || replnQty==''))
				{
					Reparray["custparam_error"] = 'PLS ENTER VALID QTY';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
					return;
				}

				if(replnQty != null && replnQty!='')
				{
					if(parseInt(replnQty)<0)
					{
						Reparray["custparam_error"] = 'ENTERED QTY SHOULD BE GREATER THAN ZERO.';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
						return;
					}

					if(parseInt(replnQty)>parseInt(ExpQty))
					{
						Reparray["custparam_error"] = 'ENTERED QTY SHOULD NOT BE GREATER THAN REPLEN QTY.';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
						return;
					}
				}


				// case# 20127635 ends


				var opentaskcount=0;

				var filters = new Array();
				var vOrdFormat='';
				if(ReportNo!=null && ReportNo!='' && ReportNo!='null')
				{
					nlapiLogExecution('ERROR','replenreportNo',ReportNo);
					filters.push(new nlobjSearchFilter('name', null, 'is', ReportNo));
					vOrdFormat='R';
				}
				if(request.getParameter('custparam_repskuno')!=null && request.getParameter('custparam_repskuno')!='' && request.getParameter('custparam_repskuno')!='null')
				{
					nlapiLogExecution('ERROR','Replenitem',request.getParameter('custparam_repskuno'));
					filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', request.getParameter('custparam_repskuno')));
					vOrdFormat=vOrdFormat + 'I';
				}

				if(request.getParameter('custparam_repbeglocId')!=null && request.getParameter('custparam_repbeglocId')!='' && request.getParameter('custparam_repbeglocId')!='null')
				{
					nlapiLogExecution('ERROR','Replenprimaryloc',request.getParameter('custparam_repbeglocId'));
					filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', request.getParameter('custparam_repbeglocId')));
					vOrdFormat=vOrdFormat + 'L';
				}

				if(request.getParameter('custparam_whlocation')!=null && request.getParameter('custparam_whlocation')!='' && request.getParameter('custparam_whlocation')!='null')
				{
					nlapiLogExecution('ERROR','custrecord_wms_location',request.getParameter('custparam_whlocation'));
					filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', request.getParameter('custparam_whlocation')));

				}

				if(request.getParameter('hdnenterfromloc')!=null && request.getParameter('hdnenterfromloc')!='' && request.getParameter('hdnenterfromloc')!='null')
				{
					nlapiLogExecution('ERROR','custrecord_actbeginloc',request.getParameter('hdnenterfromloc'));
					filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', request.getParameter('hdnenterfromloc')));

				}

				if(vZoneId !=null && vZoneId !='' && vZoneId !='null')
				{
					nlapiLogExecution('ERROR','vZoneId',vZoneId);
					filters.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', vZoneId));

				}

				if(request.getParameter('hdnbatchno')!=null && request.getParameter('hdnbatchno')!='' && request.getParameter('hdnbatchno')!='null' && request.getParameter('hdnbatchno')!='- None -')
				{
					nlapiLogExecution('ERROR','batchno',request.getParameter('hdnbatchno'));
					filters.push(new nlobjSearchFilter('custrecord_batch_no', null, 'is', request.getParameter('hdnbatchno')));

				}

				filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
				filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20])); 

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
				columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
				columns[2] = new nlobjSearchColumn('custrecord_sku');
				columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
				columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
				columns[5] = new nlobjSearchColumn('custrecord_wms_location');
				columns[6] = new nlobjSearchColumn('custrecord_actendloc');
				columns[7] = new nlobjSearchColumn('custrecord_lpno');
				columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
				columns[9] = new nlobjSearchColumn('name');	
				columns[10] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');	

				columns[1].setSort(false);
				columns[2].setSort(false);
				columns[3].setSort();

				var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
				if(searchresults != null && searchresults != '')
				{
					nlapiLogExecution('DEBUG', 'searchresultststt1', searchresults.length);
					var tempqty=request.getParameter('enterreplenqty');

					var tcols=new Array();
					tcols[0]=new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
					var searchresultstemp = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, tcols);

					var QTY1=searchresultstemp[0].getValue('custrecord_expe_qty',null,'sum');

					if(parseInt(tempqty)>parseInt(QTY1))
					{
						nlapiLogExecution('ERROR', 'replen qty is greater than gen qty');
						Reparray["custparam_error"] = 'Quantity entered should not be greater than Replen generated qty';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
						return;
					}

					for(var m1=0;m1<searchresults.length;m1++)
					{

						var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', searchresults[m1].getId());
						var InvtRef = transaction.getFieldValue('custrecord_invref_no');
						var vSkuStatus = transaction.getFieldValue('custrecord_sku_status');
						var tasktype = transaction.getFieldValue('custrecord_tasktype');
						var expqty = transaction.getFieldValue('custrecord_expe_qty');
						nlapiLogExecution('ERROR', 'InvtRef', InvtRef);
						var vContLpNo = transaction.getFieldValue('custrecord_container_lp_no');
						var dorefno = transaction.getFieldValue('custrecord_ebiz_cntrl_no');
						var whLocation=transaction.getFieldValue('custrecord_wms_location');
						if(parseInt(expqty)<=parseInt(tempqty))
						{
							nlapiLogExecution('ERROR', 'tempqty', tempqty);
							tempqty=parseInt(tempqty)-parseInt(expqty);
							transaction.setFieldValue('custrecord_act_qty', expqty);
							transaction.setFieldValue('custrecord_transport_lp', getCartLP);
							transaction.setFieldValue('custrecord_wms_status_flag', '24');
						}
						else
						{
							nlapiLogExecution('ERROR', 'tempqty', tempqty);

							if(parseInt(tempqty) >0)
							{
								transaction.setFieldValue('custrecord_act_qty', tempqty.toString());
								transaction.setFieldValue('custrecord_transport_lp', getCartLP);
								transaction.setFieldValue('custrecord_wms_status_flag', '24');
								/*if(parseFloat(expqty) != parseFloat(tempqty))
								{
									var TotalWeight=0;
									//	var accountNumber = getAccountNumber(whLocation);
									var vBatchno = transaction.getFieldValue('custrecord_batch_no');
									qty = parseFloat(expqty) - parseFloat(tempqty);							
									var Systemrules = SystemRuleForStockAdjustment(whLocation);	
									dorefno='';
									nlapiLogExecution('ERROR', 'Systemrules', Systemrules);
									if(Systemrules == null || Systemrules =='' || Systemrules.length==0)
										Systemrules=null;							
									CreateNewShortPickTwosteprpln(searchresults[m1].getId(),expqty,tempqty,dorefno,m1,vContLpNo,TotalWeight,vBatchno,qty,Systemrules,request);//have to check for wt and cube calculation
								}*/
							}
							else
							{
								transaction.setFieldValue('custrecord_act_qty', 0);
								transaction.setFieldValue('custrecord_wms_status_flag', '29');
							}

							if(parseFloat(expqty) != parseFloat(tempqty))
							{
								var TotalWeight=0;
								//	var accountNumber = getAccountNumber(whLocation);
								var vBatchno = transaction.getFieldValue('custrecord_batch_no');
								qty = parseFloat(expqty) - parseFloat(tempqty);							
								var Systemrules = SystemRuleForStockAdjustment(whLocation);	
								dorefno='';
								nlapiLogExecution('ERROR', 'Systemrules', Systemrules);
								if(Systemrules == null || Systemrules =='' || Systemrules.length==0)
									Systemrules=null;							
								CreateNewShortPickTwosteprpln(searchresults[m1].getId(),expqty,tempqty,dorefno,m1,vContLpNo,TotalWeight,vBatchno,qty,Systemrules,request);//have to check for wt and cube calculation
							}


							tempqty=parseInt(tempqty)-parseInt(expqty);

							if(parseInt(tempqty)<0)
								tempqty=0;

						}

						//transaction.setFieldValue('custrecord_transport_lp', getCartLP);
						//transaction.setFieldValue('custrecord_wms_status_flag', '24');
						transaction.setFieldValue('custrecord_act_end_date', DateStamp());
						transaction.setFieldValue('custrecord_actualendtime', TimeStamp());


						var result = nlapiSubmitRecord(transaction);

						nlapiLogExecution('ERROR', 'result', result);
						nlapiLogExecution('ERROR', 'expqty', expqty);
						nlapiLogExecution('ERROR', 'tempqty', tempqty);

					}

				}

				nlapiLogExecution('ERROR', 'opentaskcount', opentaskcount);
				opentaskcount=0;									
				var	opentasksearchresults=getConsolidatedopentaskcount(ReportNo,request.getParameter('custparam_repskuno'),getlocid,vZoneId);
				if(opentasksearchresults!=null && opentasksearchresults.length>0)
				{
					opentaskcount=opentasksearchresults.length;
				}
				nlapiLogExecution('ERROR', 'opentaskcount', opentaskcount);

				Reparray["custparam_enterfromloc"] = request.getParameter('hdnenterfromloc');

				if(opentaskcount >= 1)
				{

					var item=opentasksearchresults[0].getValue('custrecord_sku',null,'group');
					var binloc=opentasksearchresults[0].getText('custrecord_actbeginloc',null,'group');

					nlapiLogExecution('ERROR', 'item', item);
					nlapiLogExecution('ERROR', 'binloc', binloc);
					nlapiLogExecution('ERROR', 'skuno', skuno);
					nlapiLogExecution('ERROR', 'hdnBeginLocation', hdnBeginLocation);
					/*var ItemType = nlapiLookupField('item', skuno, ['recordType','custitem_ebizbatchlot']);
								if(item==skuno && hdnBeginLocation==binloc && (ItemType.recordType == 'lotnumberedinventoryitem' ||ItemType.recordType == "lotnumberedassemblyitem" || ItemType.custitem_ebizbatchlot=='T' ))
								{
									Reparray['custparam_repbatchno']=opentasksearchresults[0].getValue('custrecord_batch_no',null,'group');
									Reparray['custparam_repexpqty']=opentasksearchresults[0].getValue('custrecord_expe_qty',null,'sum');

									response.sendRedirect('SUITELET', 'customscript_rf_replen_batch', 'customdeploy_rf_replen_batch_di', false, Reparray);
									return;
								}*/
					if(item!=skuno && hdnBeginLocation==binloc )
					{
						response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenitem', 'customdeploy_ebiz_twostepreplenitem', false, Reparray);
						return;
					}
					else
					{

						nlapiLogExecution('ERROR', 'customscript','else');
						response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenlocation', 'customdeploy_ebiz_twostepreplenlocation', false, Reparray);
						return;
						//response.sendRedirect('SUITELET', ' customscript_ebiz_twostepreplenlocation', 'customdeploy_ebiz_twostepreplenlocation', false, Reparray);
						//return;
					}

				}
				else
				{
					Reparray["custparam_enterfromloc"] = request.getParameter('hdnenterfromloc');

					var searchresults=getOpenTasksCountlp(ReportNo,request);
					if (searchresults != null && searchresults!='') {

						var recNo=0;
						if(getNumber!=0)
							recNo=parseFloat(getNumber);
						nlapiLogExecution('ERROR', 'recNo in taskpriority ',recNo);	

						var SOSearchResult = searchresults[recNo];

						Reparray["custparam_repbegloc"] = SOSearchResult.getText('custrecord_actbeginloc',null,'group');
						Reparray["custparam_repbeglocId"] = SOSearchResult.getValue('custrecord_actbeginloc',null,'group');
						Reparray["custparam_repsku"] = SOSearchResult.getText('custrecord_sku',null,'group');
						Reparray["custparam_repexpqty"] = SOSearchResult.getValue('custrecord_expe_qty',null,'sum');
						Reparray["custparam_repskuno"] = SOSearchResult.getValue('custrecord_ebiz_sku_no',null,'group');
						Reparray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location',null,'group');
						Reparray["custparam_reppicklocno"] = SOSearchResult.getValue('custrecord_actendloc',null,'group');
						Reparray["custparam_reppickloc"] = SOSearchResult.getText('custrecord_actendloc',null,'group');
						Reparray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no',null,'group');
						Reparray["custparam_noofrecords"] = SOSearchResult.length;
						Reparray["custparam_clustno"] = clusterNo;
						Reparray["custparam_repno"] = ReportNo;
						response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenlocation', 'customdeploy_ebiz_twostepreplenlocation', false, Reparray);
						return;

					}
					else
					{
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_twostep_stageloc', 'customdeploy_ebiz_rf_twostep_stageloc', false, Reparray);
						//response.sendRedirect('SUITELET', 'customscript_ebiz_hook_sl_3', 'customdeploy_ebiz_hook_sl_3', false, Reparray);

					}
				}


			}
			else {

				if((isNaN(replnQty) || replnQty==''))
				{
					Reparray["custparam_error"] = 'PLS ENTER VALID QTY';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
					return;
				}

				if(replnQty != null && replnQty!='')
				{
					if(parseInt(replnQty)<0)
					{
						Reparray["custparam_error"] = 'ENTERED QTY SHOULD BE GREATER THAN ZERO.';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
						return;
					}

					if(parseInt(replnQty)>parseInt(ExpQty))
					{
						Reparray["custparam_error"] = 'ENTERED QTY SHOULD NOT BE GREATER THAN REPLEN QTY.';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
						return;
					}
				}


				if(parseInt(replnQty)!=parseInt(ExpQty))
				{
					Reparray["custparam_error"] = 'CLICK ON EXCEPTION, ENTERED QTY IS LESS THAN THE REPLEN QTY';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
					return;
				}

				var opentaskcount=0;

				var filters = new Array();

				if(ReportNo!=null && ReportNo!='' && ReportNo!='null')
				{
					nlapiLogExecution('ERROR','replenreportNo',ReportNo);
					filters.push(new nlobjSearchFilter('name', null, 'is', ReportNo));
					vOrdFormat='R';
				}
				if(request.getParameter('custparam_repskuno')!=null && request.getParameter('custparam_repskuno')!='' && request.getParameter('custparam_repskuno')!='null')
				{
					nlapiLogExecution('ERROR','Replenitem',request.getParameter('custparam_repskuno'));
					filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', request.getParameter('custparam_repskuno')));
					vOrdFormat=vOrdFormat + 'I';
				}

				if(request.getParameter('custparam_repbeglocId')!=null && request.getParameter('custparam_repbeglocId')!='' && request.getParameter('custparam_repbeglocId')!='null')
				{
					nlapiLogExecution('ERROR','Replenprimaryloc',request.getParameter('custparam_repbeglocId'));
					filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', request.getParameter('custparam_repbeglocId')));
					vOrdFormat=vOrdFormat + 'L';
				}

				if(request.getParameter('custparam_whlocation')!=null && request.getParameter('custparam_whlocation')!='' && request.getParameter('custparam_whlocation')!='null')
				{
					nlapiLogExecution('ERROR','custrecord_wms_location',request.getParameter('custparam_whlocation'));
					filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', request.getParameter('custparam_whlocation')));

				}

				if(request.getParameter('hdnenterfromloc')!=null && request.getParameter('hdnenterfromloc')!='' && request.getParameter('hdnenterfromloc')!='null')
				{
					nlapiLogExecution('ERROR','custrecord_actbeginloc',request.getParameter('hdnenterfromloc'));
					filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', request.getParameter('hdnenterfromloc')));

				}

				if(vZoneId !=null && vZoneId !='' && vZoneId !='null')
				{
					nlapiLogExecution('ERROR','vZoneId',vZoneId);
					filters.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', vZoneId));

				}

				if(request.getParameter('hdnbatchno')!=null && request.getParameter('hdnbatchno')!='' && request.getParameter('hdnbatchno')!='null' &&  request.getParameter('hdnbatchno')!='- None -')
				{
					nlapiLogExecution('ERROR','batchno',request.getParameter('hdnbatchno'));
					filters.push(new nlobjSearchFilter('custrecord_batch_no', null, 'is', request.getParameter('hdnbatchno')));

				}


				filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
				filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20])); 

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
				columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
				columns[2] = new nlobjSearchColumn('custrecord_sku');
				columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
				columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
				columns[5] = new nlobjSearchColumn('custrecord_wms_location');
				columns[6] = new nlobjSearchColumn('custrecord_actendloc');
				columns[7] = new nlobjSearchColumn('custrecord_lpno');
				columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
				columns[9] = new nlobjSearchColumn('name');	
				columns[10] = new nlobjSearchColumn('custrecord_taskpriority');	

				columns[1].setSort(false);
				columns[2].setSort(false);
				columns[3].setSort(false);

				var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
				if(searchresults != null && searchresults != '')
				{
					nlapiLogExecution('DEBUG', 'searchresultststt1', searchresults.length);
					var tempqty=request.getParameter('enterreplenqty');

					var tcols=new Array();
					tcols[0]=new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
					var searchresultstemp = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, tcols);

					var QTY1=searchresultstemp[0].getValue('custrecord_expe_qty',null,'sum');

					if(parseInt(tempqty)>parseInt(QTY1))
					{
						nlapiLogExecution('ERROR', 'replen qty is greater than gen qty');
						Reparray["custparam_error"] = 'Quantity entered should not be greater than Replen generated qty';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
						return;
					}

					for(var m1=0;m1<searchresults.length;m1++)
					{

						var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', searchresults[m1].getId());
						var InvtRef = transaction.getFieldValue('custrecord_invref_no');
						var vSkuStatus = transaction.getFieldValue('custrecord_sku_status');
						var tasktype = transaction.getFieldValue('custrecord_tasktype');
						var expqty = transaction.getFieldValue('custrecord_expe_qty');
						nlapiLogExecution('ERROR', 'InvtRef', InvtRef);

						if(parseInt(expqty)<=parseInt(tempqty))
						{
							nlapiLogExecution('ERROR', 'tempqty', tempqty);
							tempqty=parseInt(tempqty)-parseInt(expqty);
							transaction.setFieldValue('custrecord_act_qty', expqty);
						}
						else
						{
							nlapiLogExecution('ERROR', 'tempqty', tempqty);
							transaction.setFieldValue('custrecord_act_qty', tempqty.toString());
						}

						transaction.setFieldValue('custrecord_wms_status_flag', '24');
						transaction.setFieldValue('custrecord_transport_lp', getCartLP);
						transaction.setFieldValue('custrecord_act_end_date', DateStamp());
						transaction.setFieldValue('custrecord_actualendtime', TimeStamp());
						var result = nlapiSubmitRecord(transaction);

						nlapiLogExecution('ERROR', 'result', result);
						nlapiLogExecution('ERROR', 'expqty', expqty);
						nlapiLogExecution('ERROR', 'tempqty', tempqty);

					}
				}

				nlapiLogExecution('ERROR', 'opentaskcount', opentaskcount);
				opentaskcount=0;									
				var	opentasksearchresults=getConsolidatedopentaskcount(ReportNo,request.getParameter('custparam_repskuno'),getlocid,vZoneId);
				if(opentasksearchresults!=null && opentasksearchresults.length>0)
				{
					opentaskcount=opentasksearchresults.length;
				}
				nlapiLogExecution('ERROR', 'opentaskcount', opentaskcount);

				Reparray["custparam_enterfromloc"] = request.getParameter('hdnenterfromloc');

				if(opentaskcount >= 1)
				{

					var item=opentasksearchresults[0].getValue('custrecord_sku',null,'group');
					var binloc=opentasksearchresults[0].getText('custrecord_actbeginloc',null,'group');
					nlapiLogExecution('ERROR', 'item', item);
					nlapiLogExecution('ERROR', 'binloc', binloc);
					nlapiLogExecution('ERROR', 'skuno', skuno);
					nlapiLogExecution('ERROR', 'hdnBeginLocation', hdnBeginLocation);
					/*var ItemType = nlapiLookupField('item', skuno, ['recordType','custitem_ebizbatchlot']);
								if(item==skuno && hdnBeginLocation==binloc && (ItemType.recordType == 'lotnumberedinventoryitem' ||ItemType.recordType == "lotnumberedassemblyitem" || ItemType.custitem_ebizbatchlot=='T' ))
								{
									Reparray['custparam_repbatchno']=opentasksearchresults[0].getValue('custrecord_batch_no',null,'group');
									Reparray['custparam_repexpqty']=opentasksearchresults[0].getValue('custrecord_expe_qty',null,'sum');

									response.sendRedirect('SUITELET', 'customscript_rf_replen_batch', 'customdeploy_rf_replen_batch_di', false, Reparray);
									return;
								}*/
					if(item!=skuno && hdnBeginLocation==binloc )
					{
						nlapiLogExecution('ERROR', 'customscript_ebiz_twostepreplenitem');
						response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenitem', 'customdeploy_ebiz_twostepreplenitem', false, Reparray);
						return;
					}
					else
					{
						nlapiLogExecution('ERROR', 'customscript_ebiz_twostepreplenlocation');
						response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenlocation', 'customdeploy_ebiz_twostepreplenlocation', false, Reparray);
						return;
					}

				}
				else
				{
					Reparray["custparam_enterfromloc"] = request.getParameter('hdnenterfromloc');

					var searchresults=getOpenTasksCountlp(ReportNo,request);
					if (searchresults != null && searchresults!='') {

						var recNo=0;
						if(getNumber!=0)
							recNo=parseFloat(getNumber);
						nlapiLogExecution('ERROR', 'recNo in taskpriority ',recNo);	

						var SOSearchResult = searchresults[recNo];

						Reparray["custparam_repbegloc"] = SOSearchResult.getText('custrecord_actbeginloc',null,'group');
						Reparray["custparam_repbeglocId"] = SOSearchResult.getValue('custrecord_actbeginloc',null,'group');
						Reparray["custparam_repsku"] = SOSearchResult.getText('custrecord_sku',null,'group');
						Reparray["custparam_repexpqty"] = SOSearchResult.getValue('custrecord_expe_qty',null,'sum');
						Reparray["custparam_repskuno"] = SOSearchResult.getValue('custrecord_ebiz_sku_no',null,'group');
						Reparray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location',null,'group');
						Reparray["custparam_reppicklocno"] = SOSearchResult.getValue('custrecord_actendloc',null,'group');
						Reparray["custparam_reppickloc"] = SOSearchResult.getText('custrecord_actendloc',null,'group');
						Reparray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no',null,'group');
						Reparray["custparam_noofrecords"] = SOSearchResult.length;
						Reparray["custparam_clustno"] = clusterNo;
						Reparray["custparam_repno"] = ReportNo;
						response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenlocation', 'customdeploy_ebiz_twostepreplenlocation', false, Reparray);
						return;

					}
					else
					{
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_twostep_stageloc', 'customdeploy_ebiz_rf_twostep_stageloc', false, Reparray);
						//response.sendRedirect('SUITELET', 'customscript_ebiz_hook_sl_3', 'customdeploy_ebiz_hook_sl_3', false, Reparray);
					}
				}

			}
		} 
		catch (e) {
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
			nlapiLogExecution('ERROR', 'Catch: LP not found',e);
		}
	}
}

function getOpenTasksCount(ReportNo,sku,getitemid,getlocid,taskpriority,getreportNo,replenType)
{
	nlapiLogExecution('ERROR', 'ReportNo', ReportNo);
	nlapiLogExecution('ERROR', 'sku', sku);

	nlapiLogExecution('ERROR', 'getitemid in replen task',getitemid);
	nlapiLogExecution('ERROR', 'getlocid in replen task',getlocid);
	nlapiLogExecution('ERROR', 'taskpriority in replen task',taskpriority);
	nlapiLogExecution('ERROR', 'getreportNo in replen task',getreportNo);
	nlapiLogExecution('ERROR', 'replenType in replen task',replenType);

	var openreccount=0;
	var filters = new Array();

	if(getreportNo != null && getreportNo != "" && (replenType.indexOf('R') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','name',getreportNo);
		filters.push(new nlobjSearchFilter('name', null, 'is', getreportNo));

	}

	if(getitemid != null && getitemid != "" && (replenType.indexOf('I') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenitem',getitemid);
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getitemid));

	}

	//if(getBinLocationId!=null && getBinLocationId!='')
	if(getlocid!=null && getlocid!=''&& (replenType.indexOf('L') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenprimaryloc',getlocid);
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getlocid));

	}

	//if(getPriority!=null && getPriority!='')
	if(taskpriority!=null && taskpriority!='' && (replenType.indexOf('P') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenpriority',taskpriority);
		filters.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'is', taskpriority));

	}
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]));
	filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'isempty'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
	columns[2] = new nlobjSearchColumn('custrecord_sku');
	columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[5] = new nlobjSearchColumn('custrecord_wms_location');
	columns[6] = new nlobjSearchColumn('custrecord_actendloc');
	columns[7] = new nlobjSearchColumn('custrecord_lpno');
	columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
	columns[9] = new nlobjSearchColumn('name');	
	columns[10] = new nlobjSearchColumn('custrecord_batch_no');
	columns[11] = new nlobjSearchColumn('custrecord_taskpriority');	

	columns[1].setSort(false);
	columns[2].setSort(false);


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(searchresults!=null && searchresults!=''&& searchresults.length>0)
		openreccount=searchresults.length;

	return openreccount;
}

function getOpenTasksCountlp(ReportNo,request)
{
	nlapiLogExecution('ERROR', 'ReportNo', ReportNo);

	try
	{
		var openreccount=0;
		var filters = new Array();

		if(ReportNo != null && ReportNo != "" && ReportNo !='null')
		{
			nlapiLogExecution('ERROR','name',ReportNo);
			filters.push(new nlobjSearchFilter('name', null, 'is', ReportNo));
		}


		filters.push(new nlobjSearchFilter('custrecord_act_qty',null, 'isempty'));
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]));

		var columns = new Array();

		columns[0] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
		columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
		columns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
		columns[3] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
		columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
		columns[5] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
		columns[6] = new nlobjSearchColumn('custrecord_actendloc',null,'group');
		var fields=new Array();
		//nlapiLogExecution('ERROR','request.getParameter(custparam_repbegloc)',request.getParameter('custparam_repbegloc'));
		if(request.getParameter('custparam_repbegloc') != null && request.getParameter('custparam_repbegloc') !='' && request.getParameter('custparam_repbegloc') !='null')
		{
			fields[0]=new nlobjSearchFilter('name', null, 'is', request.getParameter('custparam_repbegloc'));
		}
		var sresult=nlapiSearchRecord('customrecord_ebiznet_location', null, fields, null);
		if(sresult!=null && sresult.length>0)
			filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', sresult[0].getId()));
		if(request.getParameter('custparam_repsku') != null && request.getParameter('custparam_repsku') !='' && request.getParameter('custparam_repsku') !='null')
		{
			var ItemType = nlapiLookupField('item', request.getParameter('custparam_repsku'), ['recordType','custitem_ebizbatchlot']);
			if (ItemType.recordType == 'lotnumberedinventoryitem' ||ItemType.recordType == "lotnumberedassemblyitem" || ItemType.custitem_ebizbatchlot=='T' ) 
			{
				columns[7] = new nlobjSearchColumn('custrecord_batch_no',null,'group');
			}
		}	


		columns[1].setSort(false);
		columns[2].setSort(false);

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

		if(searchresults != null && searchresults != '')
			nlapiLogExecution('ERROR','searchresults',searchresults.length);
		return searchresults;
	}
	catch(e)
	{
		nlapiLogExecution('ERROR','exception',e);
	}

}

function getQTYScan(beginLoc,reportNo,sku,expQty,skuNo,repInternalId,clustNo, whlocation, actEndLocationId, actEndLocation,  
		beginLocId,RecordCount,fromlpno,tolpno,nextlocation,nextqty,batchno,nextitem,nextitemno,taskpriority,replenType,
		getitem,getitemid,getloc,getlocid,getreportNo,getNumber,enterfromloc,getLanguage,cartLpNo,btnValue,vzone,vbatchId)
{
	if(batchno=='null'||batchno==null || batchno=='- None -')
		batchno="";
	var fields=new Array();
	fields[0]='name';
	fields[1]='custitem_ebizdescriptionitems';
	/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh as part of Standard bundle ***/
	fields[2]='description';
	/*** upto here ***/
	var skudesc='';

	nlapiLogExecution('ERROR', 'sku',sku);
	nlapiLogExecution('ERROR', 'skuNo',skuNo);
	nlapiLogExecution('ERROR', 'getLanguage',getLanguage);
	nlapiLogExecution('ERROR', 'batchno',batchno);



	var st0,st1,st2,st3,st4,st5,st6,st7,st8,st9;
	if( getLanguage == 'es_ES' || getLanguage =='es_AR')
	{
		st0 = "LP REPOSICI&#211;N";
		st1 = "ART&#205;CULO :";
		st2 = "DESCRIPCI&#211;N DE ART&#205;CULO :";
		st3 = "CANTIDAD :";
		st4 = "ENTER / SCAN DE LP";
		st5 = "ENVIAR";
		st6 = "ANTERIOR";
		st7 = "SIGUIENTE";
		st8 = "LOTE";
		st9 = "Introduzca la cantidad";

	}
	else
	{
		st0 = "REPLENISHMENT QTY";
		st1 = "ITEM:";
		st2 = "ITEM DESCRIPTION: ";
		st3 = "QUANTITY: ";
		st4 = "ENTER/SCAN FROM LP:";
		st5 = "SEND";
		st6 = "PREV";
		st7 = "NEXT";
		st8 = "BATCH";
		st9 = "ENTER QUANTITY";

	}

	var rec=nlapiLookupField('item', skuNo, fields);

	//case no 20125087
	if(rec != null && rec != '')
	{
		sku=rec.name;
		skudesc=rec.custitem_ebizdescriptionitems;
		/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh as part of Standard bundle ***/
		if(skudesc==null || skudesc=='')
		{
			skudesc=rec.description;
			skudesc=skudesc.substring(0, 20)
		}
	}

	/*** upto here ***/
	nlapiLogExecution('ERROR', 'sku',sku);


	var functionkeyHtml=getFunctionkeyScript('_rf_replenishment_LP');
	var html = "<html><head><title>"+st0+"</title>";
	html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
	html = html + "<SCRIPT LANGUAGE='javascript' type='text/javascript' for='window' EVENT='onload()'>";  
	html= html +"function DisableSubmitButton(buttonid) {  var forms = document.getElementsByTagName('form');  for (var i = 0; i < forms.length; i++) {  var input = forms[i].getElementsByTagName('input'); for (var y = 0; y < input.length; y++) { if (input[y].type == 'submit') {input[y].disabled = 'disabled';}}} document.getElementById('hdnclickedbtnid').value  = buttonid;alert(document.getElementById('hdnclickedbtnid').value);alert(buttonid);}";
	html = html + "var version = navigator.appVersion;";
	html = html + "document.onkeydown = function (e) {";
	html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
	html = html + "if ((version.indexOf('MSIE') != -1)) { ";
	html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
	html = html + "else {if (keycode == 116)return false;}";
	html = html + "};";
	html = html + "nextPage = new String(history.forward());";          
	html = html + "if (nextPage == 'undefined')";     
	html = html + "{}";     
	html = html + "else";     
	html = html + "{  location.href = window.history.forward();"; 
	html = html + "} ";
	html = html + "function stopRKey(evt) { ";
	html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
	html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
	html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
	html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
	html = html + "	  alert('System Processing, Please wait...');";
	html = html + "	  return false;}} ";
	html = html + "	} ";
	html = html + "	document.onkeypress = stopRKey; ";
	html = html + "</script>";
	html = html +functionkeyHtml;
	html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
	html = html + "<body>";
	html = html + "	<form name='_rf_replenishment_LP' method='POST'>";
	html = html + "		<table>";
	html = html + "			<tr>";

	html = html + "				<input type='hidden' name='hdnNo' value=" + reportNo + ">";
	html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + beginLoc + ">";
	html = html + "				<input type='hidden' name='hdnSku' value='" + sku + "'>";
	html = html + "				<input type='hidden' name='hdnExpQty' value=" + expQty + ">";
	html = html + "				<input type='hidden' name='hdnSkuNo' value=" + skuNo + ">";
	html = html + "				<input type='hidden' name='hdnid' value=" + repInternalId + ">";
	html = html + "				<input type='hidden' name='hdnclustno' value=" + clustNo + ">";
	html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whlocation + ">";
	html = html + "				<input type='hidden' name='hdnactendlocationid' value=" + actEndLocationId + ">";
	html = html + "				<input type='hidden' name='hdnactendlocation' value=" + actEndLocation + ">";
	html = html + "				<input type='hidden' name='hdnbeginLocId' value=" + beginLocId + ">";
	html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
	html = html + "				<input type='hidden' name='hdnfromLpno' value=" + fromlpno + ">";
	html = html + "				<input type='hidden' name='hdntoLpno' value=" + tolpno + ">";
	html = html + "				<input type='hidden' name='hdnnextloc' value=" + nextlocation + ">";
	html = html + "				<input type='hidden' name='hdnnextqty' value=" + nextqty + ">";
	html = html + "				<input type='hidden' name='hdnbatchno' value='" + batchno + "'>";
	html = html + "				<input type='hidden' name='hdnnitem' value='" + nextitem + "'>";
	html = html + "				<input type='hidden' name='hdnnextitemno' value=" + nextitemno + ">";
	html = html + "				<input type='hidden' name='hdntaskpriority' value=" + taskpriority + ">";
	html = html + "				<input type='hidden' name='hdngetNumber' value=" + getNumber + ">";
	html = html + "				<input type='hidden' name='hdnReplenType' value=" + replenType + ">";
	html = html + "				<input type='hidden' name='hdngetitem' value=" + getitem + ">";
	html = html + "				<input type='hidden' name='hdngetitemid' value=" + getitemid + ">";
	html = html + "				<input type='hidden' name='hdngetloc' value=" + getloc + ">";
	html = html + "				<input type='hidden' name='hdngetlocid' value=" + getlocid + ">";
	html = html + "				<input type='hidden' name='hdngetreportno' value=" + getreportNo + ">";
	html = html + "				<input type='hidden' name='hdnenterfromloc' value=" + enterfromloc + ">";
	html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
	html = html + "				<input type='hidden' name='hdnCartlpno' value=" + cartLpNo + ">";

	html = html + "				<input type='hidden' name='hdnclickedbtnid' value="+ btnValue +" >";
	html = html + "				<input type='hidden' name='hdnvzone' value=" + vzone + ">";// case# 201416058
	html = html + "				<input type='hidden' name='hdnbatchId' value="+ vbatchId +" >";

	//html = html + "				<input type='hidden' id='hdnclickedbtnid' value="+ btnValue +" >";
	//html = html + "				<input type='hidden' id='hdnvzone' value="+ vzone +" >";
	//html = html + "				<input type='hidden' id='hdnbatchId' value="+ vbatchId +" >";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'> "+st1+" <label>" + sku + "</label></td>";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'> "+st2+":  <label>" + skudesc + "</label></td>";
	html = html + "				</td>";
	html = html + "			</tr>";
	if(batchno!="" && batchno!=null)
	{
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> "+st8+":  <label>" + batchno + "</label></td>";
		html = html + "				</td>";
		html = html + "			</tr>";
	}
	html = html + "			<tr>";
	html = html + "				<td align = 'left'> "+st3+" :  <label>" + expQty + "</label></td>";
	html = html + "				</td>";
	html = html + "			</tr>";

	html = html + "			<tr>";
	html = html + "				<td align = 'left'> "+st9+" :";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'><input name='enterreplenqty' type='text'/>";
	html = html + "				</td>";
	html = html + "			</tr>";	

	html = html + "			<tr>";
	html = html + "				<td align = 'left'>"+st5+" <input name='cmdEnt' type='submit' value='ENT'  /> EXCEPTION <input name='cmdException'  type='submit' value='F7'  /> STAGE  <input name='cmdStage' id='cmdStage' type='submit' value='F8'  />";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td>";	
	html = html + "					PUTAWAY <input name='cmdPutaway' type='submit' value='F9' />";
	html = html + "					"+st6+" <input name='cmdPrev' type='submit' value='F10' />";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "		 </table>";
	html = html + "	</form>";
	html = html + "<script type='text/javascript'>document.getElementById('enterreplenlp').focus();</script>";
	html = html + "</body>";
	html = html + "</html>";
	return html;
}

function DeallocateRemainingInvt(RemQty,InvtRef)
{
	try{
		nlapiLogExecution('ERROR', 'Deallocation of Inventory', 'CreateInvt');
		nlapiLogExecution('ERROR', 'RemQty', RemQty);
		nlapiLogExecution('ERROR', 'InvtRef', InvtRef);
		var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv',InvtRef);
		var qty = transaction.getFieldValue('custrecord_ebiz_qoh');
		var allocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
		var vLP=transaction.getFieldValue('custrecord_ebiz_inv_lp');

		nlapiLogExecution('ERROR', 'qty', qty);
		nlapiLogExecution('ERROR', 'allocqty', allocqty);
		nlapiLogExecution('ERROR', 'RemQty', RemQty);
		transaction.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(allocqty)- parseFloat(RemQty)));
		transaction.setFieldValue('custrecord_ebiz_qoh',(parseFloat(qty)- parseFloat(RemQty)));
		nlapiSubmitRecord(transaction, false, true); 
	}
	catch(e)
	{
		nlapiLogExecution('ERROR', 'Exception in Deallocation', e);
	}
}


function getConsolidatedopentaskcount(reportNo,getitemid,getlocid,vZoneId)
{

	nlapiLogExecution('ERROR', 'reportNo',reportNo);
	nlapiLogExecution('ERROR', 'getitemid in replen task',getitemid);
	nlapiLogExecution('ERROR', 'getlocid in replen task',getlocid);
	nlapiLogExecution('ERROR', 'vZoneId',vZoneId);


	var filters = new Array();

	if(reportNo != null && reportNo != "" && reportNo !='null')
	{
		nlapiLogExecution('ERROR','name',reportNo);
		filters.push(new nlobjSearchFilter('name', null, 'is', reportNo));
	}

	/*if(getitemid != null && getitemid != ""  && getitemid !='null')
	{
		nlapiLogExecution('ERROR','Replenitem',getitemid);
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getitemid));
	}*/

	if(getlocid!=null && getlocid!='' && getlocid !='null')
	{
		nlapiLogExecution('ERROR','Replenprimaryloc',getlocid);
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getlocid));
	}

	if(request.getParameter('custparam_whlocation')!=null && request.getParameter('custparam_whlocation')!='')
	{
		nlapiLogExecution('ERROR','custrecord_wms_location',request.getParameter('custparam_whlocation'));
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', request.getParameter('custparam_whlocation')));
	}

	if(vZoneId !=null && vZoneId !='' && vZoneId !='null')
	{
		nlapiLogExecution('ERROR','vZoneId',vZoneId);
		filters.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', vZoneId));

	}




	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]));
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
	filters.push(new nlobjSearchFilter('custrecord_act_qty',null, 'isempty'));


	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
	columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
	columns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
	columns[3] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
	columns[5] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
	columns[6] = new nlobjSearchColumn('custrecord_actendloc',null,'group');

	if(getitemid!=null && getitemid!='' && getitemid !='null')
	{
		var ItemType = nlapiLookupField('item', getitemid, ['recordType','custitem_ebizbatchlot']);
		if (ItemType.recordType == 'lotnumberedinventoryitem' ||ItemType.recordType == "lotnumberedassemblyitem" || ItemType.custitem_ebizbatchlot=='T' ) 
		{
			columns[7] = new nlobjSearchColumn('custrecord_batch_no',null,'group');
		}
	}

	columns[1].setSort(false);
	columns[2].setSort(false);
	columns[3].setSort(false);

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(searchresults != null && searchresults != '')
		nlapiLogExecution('ERROR', 'searchresults.length ingetreplen',searchresults.length);
	return searchresults;
}



function CreateNewShortPickTwosteprpln(RcId,vDisplayedQty,pickqty,vdono,recordcount,getEnteredContainerNo,TotalWeight,newLot,remainqty,Systemrules,request)
{
	try
	{
		nlapiLogExecution('DEBUG', 'into CreateNewShortPickOpenTask @same file: ', 'CreateNewShortPickOpenTask');

		var vRemaningqty=0,vname,vCompany1,vcontainer,vcontrolno,vcontainerLPno,vItemno,vEbizItemNo,vEbizWaveNo1,vLineno1,vlpno1,
		vpackcode1,vSkuStatus1,vTaskType1,vUOMid1;
		var vEbizZone1,vUOMLevel1,vEbizReceiptNo1,vInvRefNo1,vEbizUser1,vFromLP1,vEbizOrdNo1=0,vWMSLocation1,vBeginLoc1,vLot;
		var vname2,vsku2,vlineord2,vlineord2,vordlineno2,vOrdQty2,vPackCode2,vPickGenFlag2,vPickGenFlag2,vPickGenQty2,vPickQty2,
		vlineskustatus2,vlinestatusflag2;
		var vUOMid2,vBackOrdFlag2,vParentOrd2,vOrdType2,vCustomer2,vDoCarrirer2,vWMSLocation2,vLineCompany2,vWMSCarrier2,vOrderPriority2;
		var vAdjustType1;
		var pickqtyfinal=0;
		var ordqtyfinal=0;
		var currentContext = nlapiGetContext();
		var currentUserID = currentContext.getUser();
		var invref='';

		if(parseInt(pickqty) < 0)
			pickqty=0;

		var Opentasktransaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RcId);

		vname = Opentasktransaction.getFieldValue('name');
		vCompany1 = Opentasktransaction.getFieldValue('custrecord_comp_id');
		vContainer = Opentasktransaction.getFieldValue('custrecord_container');
		vControlno = Opentasktransaction.getFieldValue('custrecord_ebiz_cntrl_no');
		vContainerLPno = Opentasktransaction.getFieldValue('custrecord_container_lp_no');
		vItemno = Opentasktransaction.getFieldValue('custrecord_sku');
		vEbizItemNo = Opentasktransaction.getFieldValue('custrecord_ebiz_sku_no');
		vEbizWaveNo1 = Opentasktransaction.getFieldValue('custrecord_ebiz_wave_no');
		vLineno1 = Opentasktransaction.getFieldValue('custrecord_line_no');
		vlpno1 = Opentasktransaction.getFieldValue('custrecord_lpno');
		vpackcode1 = Opentasktransaction.getFieldValue('custrecord_packcode');
		vSkuStatus1 = Opentasktransaction.getFieldValue('custrecord_sku_status');
		vTaskType1 = Opentasktransaction.getFieldValue('custrecord_tasktype');
		vBeginLoc1 = Opentasktransaction.getFieldValue('custrecord_actbeginloc');
		vUOMLevel1 = Opentasktransaction.getFieldValue('custrecord_uom_level');
		vEbizReceiptNo1 = Opentasktransaction.getFieldValue('custrecord_ebiz_receipt_no');
		vInvRefNo1 = Opentasktransaction.getFieldValue('custrecord_invref_no');
		vFromLP1 = Opentasktransaction.getFieldValue('custrecord_from_lp_no'); 
		vEbizOrdNo1 = Opentasktransaction.getFieldValue('custrecord_ebiz_order_no');
		vWMSLocation1 = Opentasktransaction.getFieldValue('custrecord_wms_location');
		vEbizRule1 = Opentasktransaction.getFieldValue('custrecord_ebizrule_no');
		vEbizMethod1 = Opentasktransaction.getFieldValue('custrecord_ebizmethod_no');
		vEbizZone1 = Opentasktransaction.getFieldValue('custrecord_ebizzone_no');
		vEbizUser1 = Opentasktransaction.getFieldValue('custrecord_ebizuser');
		vLot = Opentasktransaction.getFieldValue('custrecord_batch_no');

		invref = Opentasktransaction.getFieldValue('custrecord_invref_no');

		nlapiLogExecution('ERROR', 'vBeginLoc1', vBeginLoc1);
		nlapiLogExecution('ERROR', 'vWMSLocation1', vWMSLocation1);
		nlapiLogExecution('ERROR', 'vItemno', vItemno);
		nlapiLogExecution('DEBUG', 'pickqty', pickqty);
		nlapiLogExecution('DEBUG', 'vDisplayedQty', vDisplayedQty);
		nlapiLogExecution('DEBUG', 'remainqty', remainqty);
		nlapiLogExecution('DEBUG', 'invref', invref);
		if(remainqty==null || remainqty=='' || remainqty=='null' || isNaN(remainqty))
			remainqty=0;
		vRemaningqty = (parseFloat(pickqty)+parseFloat(remainqty))-parseFloat(vDisplayedQty);
		nlapiLogExecution('ERROR', 'vRemaningqty before', vRemaningqty);

		if( Systemrules!=null && Systemrules!=''& Systemrules.length>0 && Systemrules[0]=='Y' && (Systemrules[1] =="STND" ||Systemrules[1] =="NOADJUST" ))
		{
			nlapiLogExecution('ERROR', 'Into STND', vInvRefNo1);
			vRemaningqty = pickqty-vDisplayedQty;
		}
		else
		{
			//for Adjust Binlocation Inventory

			//nlapiLogExecution('ERROR', 'vInvRefNo1', vInvRefNo1);	
			//Case # 20127804�  Start
			/*var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', vInvRefNo1);
		var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
		var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

		if(Invallocqty==null || Invallocqty=='')
			Invallocqty=0;

		nlapiLogExecution('ERROR', 'Invallocqty', Invallocqty);
		nlapiLogExecution('ERROR', 'InvQOH', InvQOH);

//		if(remainqty ==0)
//		vRemaningqty = parseFloat(InvQOH)- parseFloat(vActqty);
//		else
//		vRemaningqty = parseFloat(InvQOH-vActqty)+parseFloat(remainqty);

		vRemaningqty = parseFloat(Invallocqty)-parseFloat(InvQOH);

		var vNewQOH =  Invallocqty;

		if(vNewQOH<=0)
		{
			nlapiLogExecution('Debug', 'Deleting record from inventory if QOH becomes zero', vInvRefNo1);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', vInvRefNo1);			
		}
		else
		{
			Invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(vNewQOH));  
			Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
			Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

			var invtrecid = nlapiSubmitRecord(Invttran, false, true);
			nlapiLogExecution('Debug', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);
		}*/
			nlapiLogExecution('ERROR', 'else clearBinlevelInvt', "Calling");
			vRemaningqty=clearBinlevelInvt(vItemno,vBeginLoc1,vWMSLocation1);
			//Case # 20127804�  End
		}

		nlapiLogExecution('ERROR', 'vRemaningqty after', vRemaningqty);

		var vRemPickQty = parseFloat(vDisplayedQty)- parseFloat(pickqty);

		var PickExpOpenTaskRec=nlapiCreateRecord('customrecord_ebiznet_trn_opentask');

		PickExpOpenTaskRec.setFieldValue('name', vname);
		PickExpOpenTaskRec.setFieldValue('custrecordact_begin_date', DateStamp());
		PickExpOpenTaskRec.setFieldValue('custrecord_comp_id', vCompany1);
		PickExpOpenTaskRec.setFieldValue('custrecord_container', vContainer);
		PickExpOpenTaskRec.setFieldValue('custrecord_current_date', DateStamp());
		PickExpOpenTaskRec.setFieldValue('custrecord_ebiz_cntrl_no', vControlno);
		PickExpOpenTaskRec.setFieldValue('custrecord_container_lp_no', vContainerLPno);
		PickExpOpenTaskRec.setFieldValue('custrecord_sku', vItemno);
		PickExpOpenTaskRec.setFieldValue('custrecord_ebiz_sku_no', vEbizItemNo);
		PickExpOpenTaskRec.setFieldValue('custrecord_ebiz_wave_no', vEbizWaveNo1);
		PickExpOpenTaskRec.setFieldValue('custrecord_expe_qty', parseFloat(vRemPickQty).toFixed(4));
		PickExpOpenTaskRec.setFieldValue('custrecord_line_no', vLineno1);
		PickExpOpenTaskRec.setFieldValue('custrecord_lpno', vlpno1);
		PickExpOpenTaskRec.setFieldValue('custrecord_packcode', vpackcode1);
		PickExpOpenTaskRec.setFieldValue('custrecord_sku_status', vSkuStatus1);
		PickExpOpenTaskRec.setFieldValue('custrecord_wms_status_flag', '30'); //Status Flag - Short Pick 
		PickExpOpenTaskRec.setFieldValue('custrecord_tasktype', vTaskType1);
		PickExpOpenTaskRec.setFieldValue('custrecord_totalcube', '');
		PickExpOpenTaskRec.setFieldValue('custrecord_total_weight', '');
		PickExpOpenTaskRec.setFieldValue('custrecord_ebizzone_no', vEbizZone1);
		PickExpOpenTaskRec.setFieldValue('custrecord_uom_level', vUOMLevel1);
		PickExpOpenTaskRec.setFieldValue('custrecord_ebiz_receipt_no', vEbizReceiptNo1);
		PickExpOpenTaskRec.setFieldValue('custrecord_actualbegintime', TimeStamp());
		PickExpOpenTaskRec.setFieldValue('custrecord_invref_no', vInvRefNo1);
		PickExpOpenTaskRec.setFieldValue('custrecord_from_lp_no', vFromLP1);
		PickExpOpenTaskRec.setFieldValue('custrecord_ebiz_order_no', vEbizOrdNo1);
		PickExpOpenTaskRec.setFieldValue('custrecord_wms_location', vWMSLocation1);
		PickExpOpenTaskRec.setFieldValue('custrecord_ebizrule_no', vEbizRule1);
		PickExpOpenTaskRec.setFieldValue('custrecord_ebizmethod_no', vEbizMethod1);
		PickExpOpenTaskRec.setFieldValue('custrecord_ebizuser', currentUserID);					
		PickExpOpenTaskRec.setFieldValue('custrecord_upd_ebiz_user_no',currentUserID);
		var vemployee = request.getParameter('custpage_employee');

		if (vemployee != null && vemployee != "") 
		{
			PickExpOpenTaskRec.setFieldValue('custrecord_taskassignedto',vemployee);
		} 
		else 
		{
			PickExpOpenTaskRec.setFieldValue('custrecord_taskassignedto',currentUserID);
		}
		nlapiLogExecution('DEBUG', 'before CreateNewShortPickOpenTask : ', 'before CreateNewShortPickOpenTask');
		if(vdono!= null && vdono !='' && vdono !='null')
		{

			var FulfillOrdLinetransaction = nlapiLoadRecord('customrecord_ebiznet_ordline',vdono);

			vname2 = FulfillOrdLinetransaction.getFieldValue('name');
			vsku2 = FulfillOrdLinetransaction.getFieldValue('custrecord_ebiz_linesku');
			vlineord2 = FulfillOrdLinetransaction.getFieldValue('custrecord_lineord');

			var newfulfillordnoarray = vlineord2.split('.');
			var ordername = newfulfillordnoarray[0];

			//generate new fullfillment order no.
			var fullFillmentorderno=fulfillmentOrderDetails(vEbizOrdNo1,ordername);

			vordlineno2 = FulfillOrdLinetransaction.getFieldValue('custrecord_ordline');
			vOrdQty2 = FulfillOrdLinetransaction.getFieldValue('custrecord_ord_qty');
			vPackCode2 = FulfillOrdLinetransaction.getFieldValue('custrecord_linepackcode');
			vPickGenFlag2 = FulfillOrdLinetransaction.getFieldValue('custrecord_pickgen_flag');
			vPickGenQty2 = FulfillOrdLinetransaction.getFieldValue('custrecord_pickgen_qty');
			vlineskustatus2 = FulfillOrdLinetransaction.getFieldValue('custrecord_linesku_status');
			vUOMid2 = FulfillOrdLinetransaction.getFieldValue('custrecord_lineuom_id');
			vBackOrdFlag2 = FulfillOrdLinetransaction.getFieldValue('custrecord_back_order_flag');
			vParentOrd2 = FulfillOrdLinetransaction.getFieldValue('custrecord_ns_ord');
			vOrdType2 = FulfillOrdLinetransaction.getFieldValue('custrecord_do_order_type');
			vCustomer2 = FulfillOrdLinetransaction.getFieldValue('custrecord_do_customer');
			vDoCarrirer2 = FulfillOrdLinetransaction.getFieldValue('custrecord_do_carrier'); 
			vWMSLocation2 = FulfillOrdLinetransaction.getFieldValue('custrecord_ordline_wms_location');
			vLineCompany2 = FulfillOrdLinetransaction.getFieldValue('custrecord_ordline_company');
			vWMSCarrier2 = FulfillOrdLinetransaction.getFieldValue('custrecord_do_wmscarrier');
			vOrderPriority2 = FulfillOrdLinetransaction.getFieldValue('custrecord_do_order_priority');
			var NSRefNo = FulfillOrdLinetransaction.getFieldValue('custrecord_nsconfirm_ref_no');


			var oldpickQty=FulfillOrdLinetransaction.getFieldValue('custrecord_pickqty');

			ordqtyfinal = parseFloat(vOrdQty2)-parseFloat(vRemPickQty);
			var pickGenqtyfinal = parseFloat(vPickGenQty2)-parseFloat(vRemPickQty);

			if(parseFloat(ordqtyfinal)<0)
				ordqtyfinal=0;
			if(parseFloat(pickGenqtyfinal)<0)
				pickGenqtyfinal=0;
			FulfillOrdLinetransaction.setFieldValue('custrecord_ord_qty', parseFloat(ordqtyfinal));
			FulfillOrdLinetransaction.setFieldValue('custrecord_pickgen_qty', parseFloat(pickGenqtyfinal));

			if(parseFloat(oldpickQty)<parseFloat(ordqtyfinal))
				FulfillOrdLinetransaction.setFieldValue('custrecord_linestatus_flag', '11'); //STATUS.OUTBOUND.PARTIALLY_PROCESSED
			else
				FulfillOrdLinetransaction.setFieldValue('custrecord_linestatus_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')
			if(isNaN(oldpickQty))
				oldpickQty=0;

			pickqtyfinal=parseFloat(oldpickQty)+parseFloat(pickqty);
			vRemaningqtyfinal = parseFloat(vRemaningqty);

			nlapiLogExecution('DEBUG', 'vRemaningqtyfinal', vRemaningqtyfinal);
			nlapiLogExecution('DEBUG', 'vRemaningqty', vRemaningqty);
			nlapiLogExecution('DEBUG', 'vWMSLocation1', vWMSLocation1);
		}

//		var PickExpFulfillOrdLineRec=nlapiCreateRecord('customrecord_ebiznet_ordline');

//		PickExpFulfillOrdLineRec.setFieldValue('name', vname2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_ebiz_linesku', vsku2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_lineord', fullFillmentorderno);//new fullfillord
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_ordline', vordlineno2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_ord_qty',parseFloat(vRemPickQty));//vOrdQty2
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_linepackcode', vPackCode2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_pickgen_flag', vPickGenFlag2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_linesku_status', vlineskustatus2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_linestatus_flag', '25');//status.outbound.edit
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_lineuom_id', vUOMid2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_back_order_flag', vBackOrdFlag2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_ns_ord', vParentOrd2);//have check the values in parameter
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_do_order_type', vOrdType2);//have check the values in parameter
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_do_customer', vCustomer2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_do_carrier', vDoCarrirer2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_ordline_wms_location', vWMSLocation2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_ordline_company', vLineCompany2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_do_wmscarrier', vWMSCarrier2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_do_order_priority', vOrderPriority2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_nsconfirm_ref_no', NSRefNo);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_linenotes2', 'Created by Confirm picks qty Exception');
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_linenotes1', vOrdQty2);
//		nlapiLogExecution('DEBUG', 'after PickExpFulfillOrdLineRec : ', 'after PickExpFulfillOrdLineRec');

		updateWtInLPMaster(getEnteredContainerNo,TotalWeight);

		vAdjustType1=getAdjustmentType(vWMSLocation1);

		var vsonamearr = vname.split('.');
		var vsoname = vsonamearr[0];


		var vnotes1="This is from Replen qty Exception. Report #: "+ vname;

		nlapiSubmitRecord(PickExpOpenTaskRec, false, true);

//		var getfulfillrecid = nlapiSubmitRecord(PickExpFulfillOrdLineRec, false, true);

		if(vdono!= null && vdono !='' && vdono !='null')
		{
			FulfillOrdLinetransaction.setFieldValue('custrecord_linenotes1', vnotes1);
			var getOldfulfillrecid = nlapiSubmitRecord(FulfillOrdLinetransaction, false, true);
		}
		if(parseFloat(vRemaningqty)!=0)
		{
			/*var invAdjustCustRecord = nlapiCreateRecord('customrecord_ebiznet_invadj');
		createInvAdjRecord(invAdjustCustRecord, vBeginLoc1, vItemno, vSkuStatus1,newLot , vDisplayedQty, vlpno1,
				vAdjustType1, vnotes1, vWMSLocation1, parseFloat(vRemaningqty), vpackcode1, recordcount);
		nlapiLogExecution('ERROR', 'adjustType in adjust main ', vAdjustType1);	*/    
			var tasktype=11; //11 is the internalid value of tasktype "ADJT"; 

			nlapiLogExecution('DEBUG', 'pickqty ', pickqty);	
			nlapiLogExecution('DEBUG', 'parseFloat(pickqty) ', parseFloat(pickqty));	
			nlapiLogExecution('DEBUG', 'vDisplayedQty ', vDisplayedQty);	
			nlapiLogExecution('DEBUG', 'parseFloat(vDisplayedQty) ', parseFloat(vDisplayedQty));	
			nlapiLogExecution('DEBUG', 'vRemaningqty ', vRemaningqty);	
			nlapiLogExecution('DEBUG', 'parseFloat(vRemaningqty) ', parseFloat(vRemaningqty));	
			if(Systemrules!=null && Systemrules!='')
			{
				nlapiLogExecution('ERROR', 'Systemrules[0] ', Systemrules[0]);
				nlapiLogExecution('ERROR', 'Systemrules[1]', Systemrules[1]);
				nlapiLogExecution('ERROR', 'Systemrules[1]', Systemrules[1]);
				nlapiLogExecution('ERROR', 'vWMSLocation1', vWMSLocation1);
			}


			if(PostAdjustmenttoNS(vWMSLocation1)=='Y')
			{
				var fields = ['recordType', 'custitem_ebizserialin'];
				var columns = nlapiLookupField('item', vItemno, fields);
				var vItemType = columns.recordType;
				nlapiLogExecution('ERROR','vItemType',vItemType);
				var serialInflg="F";		
				serialInflg = columns.custitem_ebizserialin;
				nlapiLogExecution('ERROR','serialInflg',serialInflg);
				if (vItemType == "serializedinventoryitem" || vItemType == "serializedassemblyitem" || serialInflg == "T") {
					var filtersser = new Array();


					if(vItemno !=null && vItemno!='')
						filtersser.push(new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', vItemno));

					filtersser.push( new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['3','19']));

					if(vlpno1 !=null && vlpno1!='')
					{
						//filtersser[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', getContainerLP);
						filtersser.push(new nlobjSearchFilter('custrecord_serialparentid', null, 'is', vlpno1));

					}
					var cols= new Array();
					cols[0]=new nlobjSearchColumn('custrecord_serialnumber');
					var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser,cols);
					nlapiLogExecution('DEBUG', 'SrchRecord', SrchRecord);
					var tempqty=parseFloat(vDisplayedQty)-parseFloat(pickqty);
					nlapiLogExecution('DEBUG', 'tempqty', tempqty);
					var serialNumbers= new Array();

					if(SrchRecord !=null && SrchRecord!='')
					{
						for(var k=0;k<SrchRecord.length && k<parseFloat(tempqty);k++)
						{
							serialNumbers[serialNumbers.length]=SrchRecord[k].getValue('custrecord_serialnumber');
							var SerialRecord= nlapiLoadRecord('customrecord_ebiznetserialentry',SrchRecord[k].getId() );
							SerialRecord.setFieldValue('custrecord_serialstatus','D');
							nlapiSubmitRecord(SerialRecord);
						}
					}


					newLot=serialNumbers.toString();
					nlapiLogExecution('ERROR', 'newLot ', newLot);	
				}//Case # 20127171�Start
				else if (vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem")
				{
					newLot=vLot;
				}//Case # 20127171�End
				/*var netsuiteadjustId = getIdInvokeNSInventoryAdjustment(vItemno,vSkuStatus1,vWMSLocation1,parseFloat(vRemaningqty),vnotes1,tasktype,vAdjustType1,newLot);
			nlapiLogExecution('DEBUG', 'netsuiteadjustId main11', netsuiteadjustId);	
			invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjustno',netsuiteadjustId);*/

				if(Systemrules!=null && Systemrules!='' && Systemrules.length>0 && Systemrules[0]=='Y' && Systemrules[1] !="NOADJUST")
				{


					//to fetch Stock adjustment Details from Stock Adjustments 
					var adjtasktype=22; //Tasktype : LOST
					var GetStockAdjustResults = getStockAdjustmentDetails(vWMSLocation1,adjtasktype,null);
					if(GetStockAdjustResults !=null && GetStockAdjustResults !='' && GetStockAdjustResults.length >0)
					{
						nlapiLogExecution('ERROR', 'GetStockAdjustResults.length ', GetStockAdjustResults.length);	
						var transactiontype =  GetStockAdjustResults[0].getValue('custrecord_ebiz_adjtype_trantype');
						var MapNSlocation =  GetStockAdjustResults[0].getValue('custrecord_ebiz_adjtype_mapnslocation');
						var MoveWmsBinlocation =  GetStockAdjustResults[0].getValue('custrecord_ebiz_adtype_movewmsbinloc');

						nlapiLogExecution('ERROR', 'transactiontype ', transactiontype);	
						nlapiLogExecution('ERROR', 'MapNSlocation', MapNSlocation);	
						nlapiLogExecution('ERROR', 'MoveWmsBinlocation', MoveWmsBinlocation);

						//tranaction Type =" Inventory Transfer", perform Inventory Transfer
						if(transactiontype == "2")
						{

							var transferQty = -(vRemaningqty);
							nlapiLogExecution('ERROR', 'transferQty', transferQty);

							var InvtranfId = InvokeNSInventoryTransfer(vItemno,vSkuStatus1,vWMSLocation1,MapNSlocation,parseFloat(transferQty),newLot);
							var Invtnotes1 = "Created from Invt Transfers";
							//create inventory mapped location
							createInvtwithStockadjBinloc(MoveWmsBinlocation, vItemno, vSkuStatus1,newLot , vDisplayedQty, vlpno1,
									vAdjustType1, Invtnotes1, vWMSLocation1, parseFloat(transferQty), vpackcode1, recordcount);

							CreateOpentaskRecord(vBeginLoc1, vItemno, vSkuStatus1,newLot, vDisplayedQty, vlpno1,vAdjustType1, Invtnotes1, vWMSLocation1, parseFloat(transferQty), vpackcode1, recordcount,MoveWmsBinlocation);


						}
						else
						{
							// Inventory Adjustment

							var invAdjustCustRecord = nlapiCreateRecord('customrecord_ebiznet_invadj');
							createInvAdjRecord(invAdjustCustRecord, vBeginLoc1, vItemno, vSkuStatus1,newLot , vDisplayedQty, vlpno1,
									vAdjustType1, vnotes1, vWMSLocation1, parseFloat(vRemaningqty), vpackcode1, recordcount);
							nlapiLogExecution('ERROR', 'adjustType in adjust main ', vAdjustType1);	
							var netsuiteadjustId='';
							//if(pickqty!=0)
							netsuiteadjustId = InvokeNSInventoryAdjustment(vItemno,vSkuStatus1,vWMSLocation1,parseFloat(vRemaningqty),vnotes1,tasktype,vAdjustType1,newLot);
							//	var netsuiteadjustId = getIdInvokeNSInventoryAdjustment(vItemno,vSkuStatus1,vWMSLocation1,parseFloat(vRemaningqty),vnotes1,tasktype,vAdjustType1,newLot);
							nlapiLogExecution('ERROR', 'netsuiteadjustId main11', netsuiteadjustId);
							var adjustnote ="Not posted to GL Account";
							if(netsuiteadjustId == null || netsuiteadjustId =='')
								invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjnotes1',adjustnote);
							if(netsuiteadjustId != null && netsuiteadjustId !='')
								invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjustno',netsuiteadjustId);
							invAdjustCustRecord.setFieldValue('custrecord_ebiz_cntrlno',vEbizOrdNo1);

							// Update Inventory Adjustment custom record
							var invAdjRecId = nlapiSubmitRecord(invAdjustCustRecord, false, true);
							nlapiLogExecution('ERROR', 'Inventory Adjust Record Insertion / Update for invAdjustRecId',
									invAdjRecId + ' is Success');

						}



					}
					else
					{

						nlapiLogExecution('ERROR', 'into else ','sucess');

						var invAdjustCustRecord = nlapiCreateRecord('customrecord_ebiznet_invadj');
						createInvAdjRecord(invAdjustCustRecord, vBeginLoc1, vItemno, vSkuStatus1,newLot , vDisplayedQty, vlpno1,
								vAdjustType1, vnotes1, vWMSLocation1, parseFloat(vRemaningqty), vpackcode1, recordcount);
						nlapiLogExecution('ERROR', 'adjustType in adjust main ', vAdjustType1);	//
						var netsuiteadjustId='';
						//if(pickqty!=0)
						netsuiteadjustId = InvokeNSInventoryAdjustment(vItemno,vSkuStatus1,vWMSLocation1,parseFloat(vRemaningqty),vnotes1,tasktype,vAdjustType1,newLot);
						//	var netsuiteadjustId = getIdInvokeNSInventoryAdjustment(vItemno,vSkuStatus1,vWMSLocation1,parseFloat(vRemaningqty),vnotes1,tasktype,vAdjustType1,newLot);
						nlapiLogExecution('ERROR', 'netsuiteadjustId main11', netsuiteadjustId);
						var adjustnote ="Not posted to GL Account";
						if(netsuiteadjustId == null || netsuiteadjustId =='')
							invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjnotes1',adjustnote);
						if(netsuiteadjustId != null && netsuiteadjustId !='')
							invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjustno',netsuiteadjustId);
						invAdjustCustRecord.setFieldValue('custrecord_ebiz_cntrlno',vEbizOrdNo1);

						// Update Inventory Adjustment custom record
						var invAdjRecId = nlapiSubmitRecord(invAdjustCustRecord, false, true);
						nlapiLogExecution('ERROR', 'Inventory Adjust Record Insertion / Update for invAdjustRecId',
								invAdjRecId + ' is Success');

						/*var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invref);
						var inventoryAllocQty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
						if (inventoryAllocQty == null || isNaN(inventoryAllocQty) || inventoryAllocQty == "") 
						{
							inventoryAllocQty = 0;
						}
						vNewAllocQty = parseInt(inventoryAllocQty)-parseInt(vDisplayedQty);

						transaction.setFieldValue('custrecord_ebiz_alloc_qty',vNewAllocQty);
						transaction.setFieldValue('custrecord_ebiz_qoh',vDisplayedQty);
						transaction.setFieldValue('custrecord_ebiz_avl_qty',vDisplayedQty);
						transaction.setFieldValue('custrecord_ebiz_displayfield', 'N');

						nlapiSubmitRecord(transaction);
						nlapiLogExecution('Debug', 'Replen Confirmed for inv ref ','Success');*/


						var scount=1;
						var newqoh=0;
						var vNewAllocQty=0;
						var newqoh=0;
						var avialqty = 0;
						LABL1: for(var i=0;i<scount;i++)
						{	
							nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);
							try
							{

								var transactionnew = nlapiLoadRecord('customrecord_ebiznet_createinv', invref);
								var inventoryavailQty = transactionnew.getFieldValue('custrecord_ebiz_avl_qty');
								var inventoryAllocQty = transactionnew.getFieldValue('custrecord_ebiz_alloc_qty');
								var inventoryQOHQty= transactionnew.getFieldValue('custrecord_ebiz_qoh');


								if (inventoryavailQty == null || isNaN(inventoryavailQty) || inventoryavailQty == "") 
								{
									inventoryavailQty = 0;
								}
								if (inventoryAllocQty == null || isNaN(inventoryAllocQty) || inventoryAllocQty == "") 
								{
									inventoryAllocQty = 0;
								}
								if (inventoryQOHQty == null || isNaN(inventoryQOHQty) || inventoryQOHQty == "") 
								{
									inventoryQOHQty = 0;
								}

								vNewAllocQty = parseInt(inventoryAllocQty)- parseInt(vDisplayedQty);

								newqoh = parseInt(inventoryQOHQty) - parseInt(vDisplayedQty);

								avialqty = parseInt(inventoryavailQty) + parseInt(vDisplayedQty);

								nlapiLogExecution('ERROR', 'vNewAllocQty', vNewAllocQty);
								nlapiLogExecution('ERROR', 'newqoh', newqoh);
								nlapiLogExecution('ERROR', 'avialqty', avialqty);

								transactionnew.setFieldValue('custrecord_ebiz_alloc_qty',vNewAllocQty);
								transactionnew.setFieldValue('custrecord_ebiz_qoh',newqoh);
								//transactionnew.setFieldValue('custrecord_ebiz_avl_qty',avialqty);
								transactionnew.setFieldValue('custrecord_ebiz_displayfield', 'N');

								var invid = nlapiSubmitRecord(transactionnew);
								nlapiLogExecution('Debug', 'Replen Confirmed for inv ref ',invid);

							}
							catch(ex)
							{
								var exCode='CUSTOM_RECORD_COLLISION'; 
								var wmsE='Inventory record being updated by another user. Please try again...';
								if (ex instanceof nlobjError) 
								{	
									wmsE=ex.getCode() + '\n' + ex.getDetails();
									exCode=ex.getCode();
								}
								else
								{
									wmsE=ex.toString();
									exCode=ex.toString();
								} 

								nlapiLogExecution('Debug', 'Exception in RF Inv move : ', wmsE); 
								if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
								{ 
									scount=scount+1;
									continue LABL1;
								}
								else break LABL1;
							}
						}



					}


//					var netsuiteadjustId = getIdInvokeNSInventoryAdjustment(vItemno,vSkuStatus1,vWMSLocation1,parseFloat(vRemaningqty),vnotes1,tasktype,vAdjustType1,newLot);
//					nlapiLogExecution('ERROR', 'netsuiteadjustId main11', netsuiteadjustId);	
//					invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjustno',netsuiteadjustId);
				}


			}

			/*invAdjustCustRecord.setFieldValue('custrecord_ebiz_cntrlno',vEbizOrdNo1);

		// Update Inventory Adjustment custom record
		var invAdjRecId = nlapiSubmitRecord(invAdjustCustRecord, false, true);
		nlapiLogExecution('DEBUG', 'Inventory Adjust Record Insertion / Update for invAdjustRecId',
				invAdjRecId + ' is Success');*/
		}
	}
	catch(e)
	{
		nlapiLogExecution('ERROR', 'Exception: ', e);
	}
}