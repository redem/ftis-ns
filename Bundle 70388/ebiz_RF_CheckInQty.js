/***************************************************************************
 eBizNET Solutions 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_CheckInQty.js,v $
<<<<<<< ebiz_RF_CheckInQty.js
<<<<<<< ebiz_RF_CheckInQty.js
<<<<<<< ebiz_RF_CheckInQty.js
 *     	   $Revision: 1.18.2.20.4.5.2.45.2.11 $
 *     	   $Date: 2015/11/24 11:27:41 $
 *     	   $Author: aanchal $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
=======
 *     	   $Revision: 1.18.2.20.4.5.2.45.2.11 $
 *     	   $Date: 2015/11/24 11:27:41 $
 *     	   $Author: aanchal $
=======
 *     	   $Revision: 1.18.2.20.4.5.2.45.2.11 $
 *     	   $Date: 2015/11/24 11:27:41 $
=======
 *     	   $Revision: 1.18.2.20.4.5.2.45.2.11 $
 *     	   $Date: 2015/11/24 11:27:41 $
>>>>>>> 1.18.2.20.4.5.2.14
 *     	   $Author: aanchal $
>>>>>>> 1.18.2.20.4.5.2.13
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
>>>>>>> 1.18.2.20.4.5.2.11
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_CheckInQty.js,v $
 * Revision 1.18.2.20.4.5.2.45.2.11  2015/11/24 11:27:41  aanchal
 * 2015.2 issue fix
 * 201415722
 *
 * Revision 1.18.2.20.4.5.2.45.2.10  2015/11/18 07:37:04  grao
 * 2015.2 Issue Fixes 201415745
 *
 * Revision 1.18.2.20.4.5.2.45.2.9  2015/11/17 15:15:12  grao
 * 2015.2 Issue Fixes 201415686
 *
 * Revision 1.18.2.20.4.5.2.45.2.8  2015/11/17 10:07:36  grao
 * 2015.2 Issue Fixes 201415686
 *
 * Revision 1.18.2.20.4.5.2.45.2.7  2015/11/14 15:41:07  schepuri
 * case# 201415555
 *
 * Revision 1.18.2.20.4.5.2.45.2.6  2015/11/14 06:03:31  grao
 * 2015.2 Issue Fixes 201415585
 *
 * Revision 1.18.2.20.4.5.2.45.2.5  2015/11/13 15:41:22  grao
 * 2015.2 Issue Fixes 201415585
 *
 * Revision 1.18.2.20.4.5.2.45.2.4  2015/11/13 15:31:46  schepuri
 * case# 201415533
 *
 * Revision 1.18.2.20.4.5.2.45.2.3  2015/11/12 13:18:37  schepuri
 * case# 201415532
 *
 * Revision 1.18.2.20.4.5.2.45.2.2  2015/11/10 16:56:45  sponnaganti
 * case# 201415557
 * 2015.2 issue fix
 *
 * Revision 1.18.2.20.4.5.2.45.2.1  2015/11/10 14:08:34  skreddy
 * case 201413681
 * 2015.2  issue fix
 *
 * Revision 1.18.2.20.4.5.2.45  2015/08/21 10:58:08  nneelam
 * case# 201414102
 *
 * Revision 1.18.2.20.4.5.2.44  2015/07/16 15:34:31  skreddy
 * Case# 201413495
 * Briggs SB1 issue fix
 *
 * Revision 1.18.2.20.4.5.2.43  2015/05/06 10:57:56  grao
 * SB issue fixes  201412520
 *
 * Revision 1.18.2.20.4.5.2.42  2015/01/07 13:56:36  schepuri
 * issue#   201411156,201411346
 *
 * Revision 1.18.2.20.4.5.2.41  2014/11/07 12:28:12  vmandala
 * case#  201410959 Stdbundle issue fixed
 *
 * Revision 1.18.2.20.4.5.2.40  2014/11/04 12:25:25  vmandala
 * case#  201410929 Stdbundle issue fixed
 *
 * Revision 1.18.2.20.4.5.2.39  2014/10/31 13:21:41  vmandala
 * Case# 2014108473 Stdbundle issue fixed
 *
 * Revision 1.18.2.20.4.5.2.38  2014/10/29 14:47:26  skavuri
 * Case# 201410607 Std bundle issue fixed
 *
 * Revision 1.18.2.20.4.5.2.37  2014/10/17 13:24:26  skavuri
 * Case# 201410607 Std bundle Issue fixed
 *
 * Revision 1.18.2.20.4.5.2.36  2014/09/26 16:02:13  sponnaganti
 * Case# 201410548
 * DC Dental SB CR Expected Bin Size
 *
 * Revision 1.18.2.20.4.5.2.35  2014/09/19 11:19:22  skavuri
 * case # 201410296  dcd issue fixed
 *
 * Revision 1.18.2.20.4.5.2.34  2014/08/28 11:23:46  sponnaganti
 * Case# 201410152
 * Stnd Bundle Issue fix
 *
 * Revision 1.18.2.20.4.5.2.33  2014/07/29 16:02:33  skavuri
 * Case# 20148708 SB Issue
 *
 * Revision 1.18.2.20.4.5.2.32  2014/07/18 15:22:16  skavuri
 * Case# 20148708 SB Issue Fixed
 *
 * Revision 1.18.2.20.4.5.2.31  2014/07/11 15:02:49  rmukkera
 * Case # 20149382�
 *
 * Revision 1.18.2.20.4.5.2.30  2014/07/04 23:06:05  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20149188
 *
 * Revision 1.18.2.20.4.5.2.29  2014/06/17 15:06:33  rmukkera
 * Case # 20148455�,20148471,20148705�,20148706,20148707
 *
 * Revision 1.18.2.20.4.5.2.28  2014/06/16 14:58:52  rmukkera
 * Case # 20148708
 *
 * Revision 1.18.2.20.4.5.2.27  2014/06/13 14:32:02  rmukkera
 * Case # 20148455�
 *
 * Revision 1.18.2.20.4.5.2.26  2014/06/13 07:06:47  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.18.2.20.4.5.2.25  2014/06/04 14:44:06  rmukkera
 * Case # 20148706,20148707
 *
 * Revision 1.18.2.20.4.5.2.24  2014/06/03 13:57:28  skreddy
 * case # 20148476
 * Sonic SB issue fix
 *
 * Revision 1.18.2.20.4.5.2.23  2014/05/30 12:19:54  rmukkera
 * Case # 20148159�
 *
 * Revision 1.18.2.20.4.5.2.22  2014/05/30 00:26:49  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.18.2.20.4.5.2.21  2014/05/26 15:04:10  skreddy
 * case # 20148476
 * Sonic SB issue fix
 *
 * Revision 1.18.2.20.4.5.2.20  2014/05/12 15:28:07  skavuri
 * Case# 20148344 SB Issue fixed
 *
 * Revision 1.18.2.20.4.5.2.19  2014/04/23 15:53:55  skavuri
 * Case# 20148128 issue fixed
 *
 * Revision 1.18.2.20.4.5.2.18  2014/02/21 15:05:06  rmukkera
 * Case# 20125411
 * qty-upcdode
 *
 * Revision 1.18.2.20.4.5.2.17  2014/02/19 12:56:08  snimmakayala
 * Case #: 20125411}
 * Convert the Master UPC Code scanned to Qty.
 *
 * Revision 1.18.2.20.4.5.2.16  2014/02/11 15:13:51  sponnaganti
 * case# 20127115
 * (single quotes added for item)
 *
 * Revision 1.18.2.20.4.5.2.15  2014/01/09 14:05:17  grao
 * Case# 20126677  &&  20126340 related issue fixes in Sb issue fixes
 *
 * Revision 1.18.2.20.4.5.2.14  2013/12/04 16:18:28  skreddy
 * Case# 20126215 & 20126181
 * 2014.1 stnd bundle issue fix
 *
 * Revision 1.18.2.20.4.5.2.13  2013/12/03 15:32:11  skreddy
 * Case# 20126063
 * 2014.1 stnd bundle issue fix
 *
 * Revision 1.18.2.20.4.5.2.12  2013/11/28 14:54:12  rmukkera
 * Case# 20126037�,20126038�
 *
 * Revision 1.18.2.20.4.5.2.11  2013/11/01 13:38:26  rmukkera
 * Case# 20125480
 *
 * Revision 1.18.2.20.4.5.2.10  2013/08/29 15:20:43  spendyala
 * CASE201112/CR201113/LOG201121
 * Check in with POoverage
 *
 * Revision 1.18.2.20.4.5.2.9  2013/08/23 07:15:34  schepuri
 * case no 20123558
 *
 * Revision 1.18.2.20.4.5.2.8  2013/07/25 06:56:40  skreddy
 * Case# 20123368
 * PCT SB  issue fixes
 *
 * Revision 1.18.2.20.4.5.2.7  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.18.2.20.4.5.2.6  2013/06/04 10:54:02  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Country of origin changes
 *
 * Revision 1.18.2.20.4.5.2.5  2013/05/15 07:36:54  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.18.2.20.4.5.2.4  2013/04/17 16:04:01  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.18.2.20.4.5.2.3  2013/03/19 11:48:12  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.18.2.20.4.5.2.2  2013/03/15 09:28:43  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.18.2.20.4.5.2.1  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.18.2.20.4.5  2013/02/07 15:10:34  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.18.2.20.4.4  2012/12/17 15:09:53  schepuri
 * CASE201112/CR201113/LOG201121
 * upc code scanning
 *
 * Revision 1.18.2.20.4.3  2012/11/01 14:55:35  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.18.2.20.4.2  2012/09/27 10:56:31  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.18.2.20.4.1  2012/09/21 14:57:16  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.18.2.20  2012/09/03 19:09:37  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Fisk Production Issue Fixes
 *
 * Revision 1.18.2.19  2012/08/24 18:13:58  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * NSUOMfield code changes
 *
 * Revision 1.18.2.18  2012/07/30 23:17:32  gkalla
 * CASE201112/CR201113/LOG201121
 * NSUOM issue
 *
 * Revision 1.18.2.17  2012/07/19 12:29:37  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * checkin qty NAN validation
 *
 * Revision 1.18.2.16  2012/06/15 07:10:43  spendyala
 * CASE201112/CR201113/LOG201121
 * passing parameters to query string was missing.
 *
 * Revision 1.18.2.15  2012/05/16 13:22:17  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Multi UOM changes
 *
 * Revision 1.18.2.14  2012/05/14 14:41:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to showing recommended qty fixed.
 *
 * Revision 1.18.2.13  2012/05/10 15:00:34  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to clicking previous screen was resolved.
 *
 * Revision 1.18.2.12  2012/04/27 16:03:55  spendyala
 * CASE201112/CR201113/LOG201121
 * issue realted to pack code is resolved.
 *
 * Revision 1.18.2.11  2012/04/27 15:09:19  spendyala
 * CASE201112/CR201113/LOG201121
 * If packcode is not mentioned at line level need to take from Item dim.
 *
 * Revision 1.18.2.10  2012/04/25 15:37:13  spendyala
 * CASE201112/CR201113/LOG201121
 * While Showing Recommended qty POoverage is not considered.
 *
 * Revision 1.18.2.9  2012/04/23 14:01:41  schepuri
 * CASE201112/CR201113/LOG201121
 *  validation on checkin once after checkin already completed
 *
 * Revision 1.18.2.8  2012/04/20 09:35:32  vrgurujala
 * t_NSWMS_LOG201121_120
 *
 * Revision 1.18.2.7  2012/04/16 14:59:35  spendyala
 * CASE201112/CR201113/LOG201121
 * Calculating POoverage is moved to general function.
 *
 * Revision 1.18.2.6  2012/04/13 22:18:00  spendyala
 * CASE201112/CR201113/LOG201121
 * Depending upon POoverage Value mentioned in the item master
 * value of poOverage will be calculated accordingly.
 *
 * Revision 1.18.2.5  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.18.2.4  2012/03/02 01:25:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.23  2012/03/02 01:11:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.22  2012/02/16 10:40:08  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 * Revision 1.21  2012/02/14 07:05:23  spendyala
 * CASE201112/CR201113/LOG201121
 * merged upto 1.18.2.2.
 *
 * Revision 1.20  2012/02/13 13:37:41  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 * Revision 1.19  2012/02/12 12:26:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Checkin MultiUOM issues
 *
 * Revision 1.18  2012/01/04 15:08:45  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.17  2011/12/24 15:34:14  gkalla
 * CASE201112/CR201113/LOG201121
 * Changed the message
 *
 * Revision 1.16  2011/12/02 13:18:22  schepuri
 * CASE201112/CR201113/LOG201121
 * RF inbound issue fixing to fetch poline item status
 *
 * Revision 1.15  2011/09/24 19:47:31  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.13  2011/09/12 08:24:00  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.12  2011/08/25 12:42:44  schepuri
 * CASE201112/CR201113/LOG201121
 * Variable 'poOverage' initialization changed from 'N' to 0 (zero) as the field type changed from checkbox to freeform text
 *
 * Revision 1.11  2011/08/25 05:53:36  schepuri
 * CASE201112/CR201113/LOG201121
 * PO Overage Acc to % and through error if pallet quantity is not defined
 *
 * Revision 1.10  2011/08/24 12:38:35  schepuri
 * CASE201112/CR201113/LOG201121
 * PO Overage Acc to % and through error if pallet quantity is not defined
 *
 * Revision 1.9  2011/07/28 12:31:02  schepuri
 * CASE201112/CR201113/LOG201121
 * PO Overage Acc to %
 *
 * Revision 1.8  2011/07/07 09:15:42  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.7  2011/06/14 07:45:49  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.6  2011/06/06 14:10:04  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 * In Check-in, Remaining cube calculation and putaway logic.
 *
 * Revision 1.5  2011/06/03 06:29:22  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 *
 * Revision 1.4  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.3  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/

function CheckInQty(request, response){
	if (request.getMethod() == 'GET') {
		var ItemDescription;
		var ItemQuantity;
		var ItemQuantityReceived;
		var ItemPackCode;
		var ItemPackCodeId;
		var ItemStatus;
		var ItemStatusValue;
		var ItemCube;

		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);

		//	Get the PO#, PO Line Item, Line#, Entered Item and PO Internal Id 
		//  from the previous screen, which is passed as a parameter	
		var getPONo = request.getParameter('custparam_poid');
		var getPOItem = request.getParameter('custparam_poitem');
		nlapiLogExecution('DEBUG','getPOItem', getPOItem);
		var getPOLineNo = request.getParameter('custparam_lineno');
		var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		var getPOInternalId = request.getParameter('custparam_pointernalid');
		var getItemCube = request.getParameter('custparam_itemcube');
		var getItemBaseUomQty=request.getParameter('custparam_baseuomqty');
		var getWHLocation = request.getParameter('custparam_whlocation');
		var getCountryOfOrigin=request.getParameter('custparam_countryoforigin');
		var getCountryid=request.getParameter('custparam_countryid');
		var ctx = nlapiGetContext(); 
		var userAccountId = ctx.getCompany();
			nlapiLogExecution('DEBUG', 'userAccountId', userAccountId);
		nlapiLogExecution('DEBUG','WH Location', getWHLocation);
		nlapiLogExecution('DEBUG','getFetchedItemId', getFetchedItemId);
		nlapiLogExecution('DEBUG','getPOLineNo', getPOLineNo);


		var enteredOption = "";
		var poItemUOM='';
		var vbaseuom='';
		var vbaseuomqty='';
		var vuomqty='';
		var vuomlevel='';
		var item_id='';
		enteredOption = request.getParameter('custparam_enteredOption');

		var trantype = nlapiLookupField('transaction', getPOInternalId, 'recordType');

		nlapiLogExecution('DEBUG','trantype', trantype);

		// Load a record into a variable from Purchase Order for the PO Internal Id
		var PORec = nlapiLoadRecord(trantype, getPOInternalId);

		// Fetched the count of lines available in the Purchase Order
		var LineItemCount = PORec.getLineItemCount('item');

		// Loop into fetch the PO Line details for the line# passed.
		for (var i = 1; i <= LineItemCount; i++) {
			var lineno = PORec.getLineItemValue('item', 'line', i);

			if (lineno == getPOLineNo) {
				ItemDescription = PORec.getLineItemValue('item', 'description', i);
				poItemUOM = PORec.getLineItemValue('item', 'units', i);
				item_id = PORec.getLineItemText('item', 'item', i);

				nlapiLogExecution('DEBUG','getPOItem item_id', item_id);

				if (ItemDescription == null) {
					var poItemFields = ['custitem_ebizdescriptionitems'];
					var poItemColumns = nlapiLookupField('item', getFetchedItemId, poItemFields);

					ItemDescription = poItemColumns.custitem_ebizdescriptionitems;

					if (ItemDescription == null)
						ItemDescription = '';

				}
				ItemDescription = ItemDescription.substring(0, 20);

				ItemQuantity = PORec.getLineItemValue('item', 'quantity', i);
				if (ItemQuantity == null) {
					ItemQuantity = '';
				}

				ItemQuantityReceived = PORec.getLineItemValue('item', 'quantityreceived', i);
				if (ItemQuantityReceived == null) {
					ItemQuantityReceived = '';
				}

				ItemPackCode = PORec.getLineItemText('item', 'custcol_nswmspackcode', i);
				if (ItemPackCode == null) {
					ItemPackCode='';

				}
				ItemPackCodeId = PORec.getLineItemValue('item', 'custcol_nswmspackcode', i);

				nlapiLogExecution('DEBUG','ItemPackCodeId', ItemPackCodeId);

				ItemStatus = PORec.getLineItemText('item', 'custcol_ebiznet_item_status', i);

				ItemStatusValue = PORec.getLineItemValue('item', 'custcol_ebiznet_item_status', i);
				if (ItemStatus == null) {
					ItemStatus = '';
				}
			}
		}

		if(poItemUOM!=null && poItemUOM!='')
		{
			var eBizItemDims=geteBizItemDimensions(getFetchedItemId);
			if(eBizItemDims!=null&&eBizItemDims.length>0)
			{
				nlapiLogExecution('DEBUG', 'Item Dimesions Length', eBizItemDims.length);
				for(z=0; z < eBizItemDims.length; z++)
				{

					if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
					{
						vbaseuom = eBizItemDims[z].getText('custrecord_ebizuomskudim');
						vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
					}
					nlapiLogExecution('DEBUG', 'poItemUOM', poItemUOM);
					nlapiLogExecution('DEBUG', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
					nlapiLogExecution('DEBUG', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
					if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
					{
						vuomlevel = eBizItemDims[z].getText('custrecord_ebizuomlevelskudim');
						vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
					}
				}

				if(vuomqty==null || vuomqty=='')
				{
					vuomqty=vbaseuomqty;
				}

				if(ItemQuantity==null || ItemQuantity=='' || isNaN(ItemQuantity))
					ItemQuantity=0;
				else
					ItemQuantity = (parseFloat(ItemQuantity)*parseFloat(vuomqty))/parseFloat(vbaseuomqty);

				if(ItemQuantityReceived==null || ItemQuantityReceived=='' || isNaN(ItemQuantityReceived))
					ItemQuantityReceived=0;
				else
					ItemQuantityReceived = (parseFloat(ItemQuantityReceived)*parseFloat(vuomqty))/parseFloat(vbaseuomqty);			

			}
		}

		nlapiLogExecution('DEBUG', 'poItemUOM', poItemUOM);
		nlapiLogExecution('DEBUG', 'baseuom', vbaseuom);
		nlapiLogExecution('DEBUG', 'baseuomqty', vbaseuomqty);
		nlapiLogExecution('DEBUG', 'uomqty', vuomqty);
		nlapiLogExecution('DEBUG', 'uomlevel', vuomlevel);
		nlapiLogExecution('DEBUG', 'ItemQuantity', ItemQuantity);
		nlapiLogExecution('DEBUG', 'ItemQuantityReceived', ItemQuantityReceived);

		/*
		 * The below part of the code is to check if the PO Overage is allowed or not. 
		 * If the PO Overages are allowed, then the recommended quantity should be the pallet quantity.
		 * If the PO Overages are not allowed, then the recommended quantity should be the remaining quantity.
		 */   

		var poOverage=GetPoOverage(getFetchedItemId,getPOInternalId,getWHLocation);

		//if(poOverage==null || poOverage=='')
		if((poOverage==null || poOverage=='') && poOverage!=0)
		{
			poOverage = checkPOOverage(getPOInternalId,getWHLocation, null);
		}
		//var poOverage = checkPOOverage(getPOInternalId,getWHLocation, null);
		nlapiLogExecution('DEBUG','poOverage', poOverage);
//		var ItemRemaininingQuantity = parseFloat(ItemQuantity) - parseFloat(ItemQuantityReceived);

		var ItemRemaininingQuantity = itemRemainingQuantity(getPOInternalId, getFetchedItemId, getPOLineNo,
				ItemQuantity, ItemQuantityReceived, getWHLocation, null);
		
		
		var vOrderQuantity = 0;
		var vputgenQuantity = 0;
		
		var vtransactionFilters = new Array();
		vtransactionFilters[0] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', getPOInternalId);
		vtransactionFilters[1] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_sku_no', null, 'equalto', getFetchedItemId);
		vtransactionFilters[2] = new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', getPOLineNo);

		var vtransactionColumns = new Array();
		vtransactionColumns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_putgen_qty');
		vtransactionColumns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_order_qty');
		

		var vtransactionSearchresults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, vtransactionFilters, vtransactionColumns);
		
		if (vtransactionSearchresults != null && vtransactionSearchresults.length > 0)
		{
			for(var i = 0; i <= vtransactionSearchresults.length; i++)
			{
				vputgenQuantity = vtransactionSearchresults[0].getValue('custrecord_orderlinedetails_putgen_qty');
				vOrderQuantity =  vtransactionSearchresults[0].getValue('custrecord_orderlinedetails_order_qty');
			}
		}
		
		
		

		var palletQuantity = fetchPalletQuantity(getFetchedItemId,getWHLocation,null); //'9999';
		var ItemRecommendedQuantity =0;
		var hdnItemRecmdQtyWithPOoverage=0;
		/*nlapiLogExecution('DEBUG','ItemRemaininingQuantity', ItemRemaininingQuantity);
		nlapiLogExecution('DEBUG','palletQuantity', palletQuantity);
		nlapiLogExecution('DEBUG','ItemQuantity', ItemQuantity);
		 */
		if (poOverage == null){
			poOverage = 0;	
		}
		//else{
		hdnItemRecmdQtyWithPOoverage = Math.min (ItemRemaininingQuantity + (poOverage * ItemQuantity)/100, palletQuantity);
		ItemRecommendedQuantity = Math.min (hdnItemRecmdQtyWithPOoverage, palletQuantity);
//		ItemRecommendedQuantity = Math.min (ItemRemaininingQuantity, palletQuantity);
		/*
			if(palletQuantity > ItemRemaininingQuantity){
				ItemRecommendedQuantity = ItemRemaininingQuantity;
			}
			else if (palletQuantity < ItemRemaininingQuantity){
				ItemRecommendedQuantity =  palletQuantity;
			}*/		

		//}
		nlapiLogExecution('DEBUG','ItemRecommendedQuantity', parseFloat(ItemRecommendedQuantity.toString()));
		var pickfaceRecommendedQuantity = priorityPutawayQuantity(getFetchedItemId,getWHLocation,null);

		if(pickfaceRecommendedQuantity=='' || pickfaceRecommendedQuantity==null)
			pickfaceRecommendedQuantity=0;

		nlapiLogExecution('DEBUG','Returned Pickface Recommended Quantity', pickfaceRecommendedQuantity);

		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime'); 
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');

		if(ItemRemaininingQuantity < 0)
			ItemRemaininingQuantity=0;


		nlapiLogExecution('DEBUG','ItemRemaininingQuantity', ItemRemaininingQuantity);
		nlapiLogExecution('DEBUG','palletQuantity', palletQuantity);
		nlapiLogExecution('DEBUG','ItemQuantity', ItemQuantity);

		var POarrayget = new Array();

		POarrayget["custparam_error"] = 'Check in already completed';
		POarrayget["custparam_screenno"] = '3SKU';
		POarrayget["custparam_poid"] = request.getParameter('custparam_poid');
		//POarrayget["custparam_poitem"] = request.getParameter('custparam_poitem');
		POarrayget["custparam_poitem"] = item_id;
		POarrayget["custparam_lineno"] = request.getParameter('custparam_lineno');
		POarrayget["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
		POarrayget["custparam_pointernalid"] = request.getParameter('custparam_pointernalid');
		POarrayget["custparam_trantype"] = request.getParameter('custparam_trantype');
		POarrayget["custparam_itemcube"] = request.getParameter('custparam_itemcube');
		POarrayget["custparam_whlocation"] = request.getParameter('custparam_whlocation');
		POarrayget["custparam_baseuomqty"] = request.getParameter('custparam_baseuomqty');
		POarrayget["custparam_actualbegintime"] = getActualBeginTime; //TimeArray[0];
		POarrayget["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM;	//TimeArray[1];
		POarrayget["custparam_actualbegindate"] = getActualBeginDate;
		var trantype= request.getParameter('custparam_trantype');

//		Added by Narasimha inorder to get the receipt type based on po.

		var POblindreceiptfilters=new Array();
		POblindreceiptfilters.push(new nlobjSearchFilter('tranid',null,'is',getPONo));
		POblindreceiptfilters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

		var blindreceiptColumns = new Array();
		blindreceiptColumns[0] = new nlobjSearchColumn('custbody_nswmsporeceipttype');

		var blindreceiptSearchResults = nlapiSearchRecord('purchaseorder', null, POblindreceiptfilters, blindreceiptColumns);

		var receiptType='';
		if(blindreceiptSearchResults!=null && blindreceiptSearchResults!='')
		{
			receiptType=blindreceiptSearchResults[0].getValue('custbody_nswmsporeceipttype');
		}

		var poBlindReceipt='';


		nlapiLogExecution('DEBUG','receiptType',receiptType);

		if (receiptType != "" && receiptType != null) 
		{

			var receiptFieldsRec = ['custrecord_ebiz_blindreceipt'];
			var receiptColumns = nlapiLookupField('customrecord_ebiznet_receipt_type', receiptType, receiptFieldsRec);

			poBlindReceipt = receiptColumns.custrecord_ebiz_blindreceipt;

			nlapiLogExecution('DEBUG','poBlindReceipt', poBlindReceipt);
		}

	if(poOverage > 0)
	{
	
		if(ItemQuantity!=0 && vputgenQuantity!=0)
		{
			if(parseFloat(ItemQuantity) == parseFloat(vputgenQuantity))
			{
				POarrayget["custparam_error"] = 'Ordered quantity received completely';
	POarrayget["custparam_screenno"] = '2';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarrayget);
				return;
			}
		}
	}

		//	Upto here.
		   //case 2014108473
		if(parseFloat(ItemRemaininingQuantity) == 0 && (parseFloat(ItemRecommendedQuantity) == 0||parseFloat(ItemRecommendedQuantity) < 0||parseFloat(ItemRemaininingQuantity) < 0))
		{
			nlapiLogExecution('DEBUG','cknin completed ItemRemaininingQuantity', ItemRemaininingQuantity);
			nlapiLogExecution('DEBUG','cknin completed ItemRecommendedQuantity', ItemRecommendedQuantity);
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarrayget);
			return;
		}

		else
		{

			var getLanguage = request.getParameter('custparam_language');
			nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);


			var st0,st1,st2,st3,st4,st5,st6,st7,st8,st9,st10,st11,st12;

			if( getLanguage == 'es_ES')
			{
				st0 = "RECEPCI&#211;N DE MEN&#218;";
				st1 = "ART&#205;CULO";
				st2 = "DESCRIPCI&#211;N DEL ART&#205;CULO";
				st3 = "PAQUETE DE C&#211;DIGO";
				st4 = "STATUS DEL ART&#205;CULO";
				st5 = "CANTIDAD RESTANTE";
				st6 = "CANTIDAD RECOMENDADA";
				st7 = "PICKFACE RECOMENDADO CANT";
				st8 = "CANTIDAD";
				st9 = "ENVIAR";
				st10 = "ANTERIOR";
				st11 = "UPC";
				st12 = "MANF#";


			}
			else
			{
				st0 = "RECEIVING MENU";
				st1 = "ITEM";
				st2 = "ITEM DESC";
				st3 = "PACK CODE";
				st4 = "ITEM STATUS";
				st5 = "REMAINING QTY";
				st6 = "RECOMMENDED QTY";
				st7 = "PICKFACE RECOMMENDED QTY";
				st8 = "QTY";
				st9 = "SEND";
				st10 = "PREV";
				st11 = "UPC";
				st12 = "MANF#";


			}

			var getNumber;
			if(request.getParameter('custparam_number')!= null && request.getParameter('custparam_number') != "")
				getNumber = request.getParameter('custparam_number');
			else if(request.getParameter('custparam_poqtyentered')!= null && request.getParameter('custparam_poqtyentered') != "")
			{
				getNumber = request.getParameter('custparam_poqtyentered');
			}
			else
			{
				getNumber=0;
			}

			var UPC="";
			var ManufacturerNo="";

			var PORec = nlapiLoadRecord(trantype, getPOInternalId);
			var LineItemCount = PORec.getLineItemCount('item');
			//var poItemField = ['upccode','mpn'];
			nlapiLogExecution('DEBUG','getFetchedItemId', getFetchedItemId);
		//	var poItemColumn = nlapiLookupField('item', getFetchedItemId, poItemField);
			
			
			var poItemField = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group',
			              'custitem_ebizserialin','custitem_ebiz_merge_fifodates','upccode','mpn'];
			var poItemColumn = nlapiLookupField('item', getFetchedItemId, poItemField);
			
			var ItemType = poItemColumn.recordType;					
			var batchflg = poItemColumn.custitem_ebizbatchlot;
			var itemfamId= poItemColumn.custitem_item_family;
			var itemgrpId= poItemColumn.custitem_item_group;
			var serialInflg = poItemColumn.custitem_ebizserialin;
			var mergefifodates=poItemColumn.custitem_ebiz_merge_fifodates;
			UPC=poItemColumn.upccode;
			ManufacturerNo=poItemColumn.mpn;
			
			nlapiLogExecution('DEBUG','ItemType', ItemType);

			var caseQty = fetchCaseQuantity(getFetchedItemId,getWHLocation,null);
			var functionkeyHtml=getFunctionkeyScript('_rf_checkin_qty'); 
			var html = "<html><head><title>" + st0 + "</title>";
			html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
			html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
			//html = html + " document.getElementById('enterqty').focus();";        
			html = html + "function stopRKey(evt) { ";
			//html = html + "	  alert('evt');";
			html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
			html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
			html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
			html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
			html = html + "	  alert('System Processing, Please wait...');";
			html = html + "	  return false;}} ";
			html = html + "	  if(document.getElementById('cmdNext').disabled==true){";
			html = html + "	  alert('System Processing, Please wait...');";
			html = html + "	  return false;}} ";
			html = html + "	} ";

			html = html + "	document.onkeypress = stopRKey; ";

			html = html + "</script>";
			html = html +functionkeyHtml;
			html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
			html = html + "	<form name='_rf_checkin_qty' method='POST'>";
			html = html + "		<table>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st1 +" : <label>" +  item_id + "</label>";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st2 +": <label>" + ItemDescription + "</label>";
			html = html + "				</td>";
			html = html + "			</tr><tr>";
			html = html + "				<td align = 'left'>" + st11 +  ": <label>" + UPC + "</label>";
			html = html + "				</td>";
			html = html + "			</tr><tr>";
			html = html + "				<td align = 'left'> " + st12 + ": <label>" + ManufacturerNo + "</label>";
			html = html + "				</td>";
			html = html + "			</tr><tr>";
			html = html + "				<td align = 'left'>" + st3 +": <label>" + ItemPackCode + "</label>";
			html = html + "				<input type='hidden' name='hdnItemPackCode' value=" + ItemPackCodeId + ">";
			html = html + "				<input type='hidden' name='hdnQuantity' value=" + ItemQuantity + ">";
			html = html + "				<input type='hidden' name='hdnQuantityReceived' value=" + ItemQuantityReceived + ">";
			nlapiLogExecution('DEBUG', 'ItemStatus113311', ItemStatus);
			html = html + "				<input type='hidden' name='hdnItemStatusValue' value=" + ItemStatusValue + "></td>";
			nlapiLogExecution('DEBUG', 'hdnItemStatus111', ItemStatusValue);
			html = html + "				<input type='hidden' name='hdnItemCube' value=" + getItemCube + "></td>";

			html = html + "				<input type='hidden' name='hdnBaseUomQty' value=" + getItemBaseUomQty + ">";

			html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
			html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
			html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
			html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
			html = html + "				<input type='hidden' name='hdnEnteredOption' value=" + enteredOption + ">";
			html = html + "				<input type='hidden' name='hdnPalletQuantity' value=" + palletQuantity + "></td>";
			//html = html + "				<input type='hidden' name='hdngetPOItem' value=" + getPOItem + "></td>";
			//case# 20127115 starts (single quotes added for item)
			html = html + "				<input type='hidden' name='hdngetPOItem' value='" + item_id + "'></td>";
			//case# 20127115 end
			//Added by Phani 03-25-2011
			html = html + "				<input type='hidden' name='hdnWhLocation' value=" + getWHLocation + ">";
			html = html + "				<input type='hidden' name='hdnpoOverage' value=" + poOverage + ">";
			html = html + "				<input type='hidden' name='hdnItemRecmdQtyWithPOoverage' value=" + hdnItemRecmdQtyWithPOoverage + ">";

			html = html + "				<input type='hidden' name='hdncountryoforigin' value=" + getCountryOfOrigin+ ">";
			html = html + "				<input type='hidden' name='hdncountryid' value=" + getCountryid+ ">";
			html = html + "				<input type='hidden' name='hdntrantype' value=" + trantype + ">";
			html = html + "				<input type='hidden' name='hdnuserAccountId' value=" + userAccountId + ">";
			html = html + "				<input type='hidden' name='hdnitemtype' value=" + ItemType + ">";
			html = html + "				<input type='hidden' name='hdnserialin' value=" + serialInflg + ">";
			
			
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st4 +": <label>" + ItemStatus + "</label></td>";
			nlapiLogExecution('DEBUG', 'ItemStatus1', ItemStatus);
			html = html + "			</tr>";
			if(poBlindReceipt!='T'){
				html = html + "			<tr>";
				if(ItemRemaininingQuantity < 0)
					ItemRemaininingQuantity=0;
				//html = html + "				<td align = 'left'>" + st5 +": <label>" + parseFloat(ItemRemaininingQuantity.toString()) + "</label>";
				html = html + "				<td align = 'left'>" + st5 +": <label>" + parseFloat(ItemRemaininingQuantity).toFixed(4) + " Each</label>";
				var pikQtyBreakup = getQuantityBreakdown(caseQty,ItemRemaininingQuantity);
				html = html + "	("+caseQty+"/Case:"+pikQtyBreakup+")";
				html = html + "				<input type='hidden' name='hdnItemRemaininingQuantity' value=" + parseFloat(ItemRemaininingQuantity).toFixed(4) + "></td>";
				html = html + "			</tr>";
				html = html + "			<tr>";
				//html = html + "				<td align = 'left'>" + st6 +": <label>" + parseFloat(ItemRecommendedQuantity.toString()) + "</label>";
				html = html + "				<td align = 'left'>" + st6 +": <label>" + parseFloat(ItemRecommendedQuantity).toFixed(4) + " Each</label>";
				var pikQtyBreakup = getQuantityBreakdown(caseQty,ItemRecommendedQuantity);
				html = html + "	("+caseQty+"/Case:"+pikQtyBreakup+")";
				html = html + "				<input type='hidden' name='hdnRecommendedQuantity' value=" + parseFloat(ItemRecommendedQuantity).toFixed(4) + "></td>";
				html = html + "			</tr>";
			}
			if(pickfaceRecommendedQuantity > 0)
			{
				html = html + "			<tr>";
				html = html + "				<td align = 'left'>" + st7 +": <label>" + pickfaceRecommendedQuantity + "</label>";
				html = html + "				<input type='hidden' name='hdnPickfaceRecommendedQuantity' value=" + parseFloat(pickfaceRecommendedQuantity) + "></td>";
				html = html + "			</tr>";			
			}
			if(getNumber > 0)
			{
				html = html + "			<tr>";
				html = html + "				<td align = 'left'> SCANNED QTY: <label>" + getNumber + "</label>";
				html = html + "				<input type='hidden' name='hdngetnumber' value=" + getNumber + ">";
				html = html + "			</tr>";		
			}
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st8 +": ";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
//			html = html + "				<td align = 'left'><input name='enterqty' type='text' value='"+ parseFloat(ItemRecommendedQuantity.toString()) +"'/>";
			html = html + "				<td align = 'left'><input name='enterqty' id='enterqty' type='text' value=''/>";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>";
			html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st9 +" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; this.form.cmdNext.disabled=true;return false'/>";
			html= html +"            NEXT <input name='cmdNext' type='submit' value='F8' />";
			html = html + "					" + st10 +" <input name='cmdPrevious' type='submit' value='F7'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "		 </table>";
			html = html + "	</form>";
			//Case# 20148882 (added Focus Functionality for Textbox)
			html = html + "<script type='text/javascript'>document.getElementById('enterqty').focus();</script>";
			html = html + "</body>";
			html = html + "</html>";

			response.write(html);
		}
	}
	else {
		nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is not null');

		var getActualBeginTime = request.getParameter('hdnActualBeginTime'); 
		var getActualBeginTimeAMPM = request.getParameter('hdnActualBeginTimeAMPM');

		var ActualBeginTime;

		// This variable is to hold the Quantity entered.
		var POarray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;

		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);


		var st11;
		if( getLanguage == 'es_ES')
		{
			st11 = "CANTIDADA INV&#193;LIDA";
		}
		else
		{
			st11 = "INVALID QUANTITY";
		}



		var userAccountId = request.getParameter('hdnuserAccountId');
		nlapiLogExecution('DEBUG', 'userAccountId', userAccountId);
		POarray["custparam_userAccountId"] = userAccountId;

		POarray["custparam_poid"] = request.getParameter('custparam_poid');
		//POarray["custparam_poitem"] = request.getParameter('custparam_poitem');
		POarray["custparam_poitem"] =  request.getParameter('hdngetPOItem');
		POarray["custparam_lineno"] = request.getParameter('custparam_lineno');
		POarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
		POarray["custparam_pointernalid"] = request.getParameter('custparam_pointernalid');
		POarray["custparam_poqtyentered"] = request.getParameter('enterqty');
		POarray["custparam_poitemremainingqty"] = request.getParameter('hdnItemRemaininingQuantity');
		POarray["custparam_polinepackcode"] = request.getParameter('hdnItemPackCode');
		//nlapiLogExecution('DEBUG', 'request.getParameter(hdnItemStatus)', request.getParameter('hdnItemStatus'));
		POarray["custparam_polineitemstatusValue"] = request.getParameter('hdnItemStatusValue');
		//nlapiLogExecution('DEBUG', 'POarray["custparam_polineitemstatus"]', POarray["custparam_polineitemstatus"]);
		POarray["custparam_polinequantity"] = request.getParameter('hdnQuantity');
		POarray["custparam_polinequantityreceived"] = request.getParameter('hdnQuantityReceived');
		POarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
		POarray["custparam_countryoforigin"] = request.getParameter('hdncountryoforigin');
		POarray["custparam_countryid"] = request.getParameter('hdncountryid');
		POarray["custparam_itemcube"] = request.getParameter('hdnItemCube');

		//code added on 13 feb 2012 by suman
		//To get the baseuom qty .
		POarray["custparam_baseuomqty"] = request.getParameter('hdnBaseUomQty');
		nlapiLogExecution('DEBUG','ITEMINFO[0]',POarray["custparam_itemcube"]);
		nlapiLogExecution('DEBUG','ITEMINFO[1]',POarray["custparam_baseuomqty"]);
		//end of code as of 13 feb 2012.

		nlapiLogExecution('DEBUG', 'Itemcubeparam', POarray["custparam_itemcube"]);
		nlapiLogExecution('DEBUG', 'Item quantity entered', POarray["custparam_poqtyentered"]);

		POarray["custparam_actualbegindate"] = request.getParameter('hdnActualBeginDate');
		POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
		nlapiLogExecution('DEBUG', 'WH Location', POarray["custparam_whlocation"]);

		var poOverage = request.getParameter('hdnpoOverage');
//		var recommendedQuantity = request.getParameter('hdnRecommendedQuantity');
		var recommendedQuantity = request.getParameter('hdnItemRecmdQtyWithPOoverage');
		var palletQuantity = request.getParameter('hdnPalletQuantity');
		var pickfaceRecommendedQuantity = request.getParameter('hdnPickfaceRecommendedQuantity');

		POarray["custparam_actualbegintime"] = getActualBeginTime; //TimeArray[0];
		POarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM;	//TimeArray[1];
		POarray["custparam_getPOItem"] = request.getParameter('hdngetPOItem');
		
		var getItemType = request.getParameter('hdnitemtype');
		var serialin = request.getParameter('hdnserialin');		
		
		nlapiLogExecution('DEBUG', 'getItemType', getItemType);
		nlapiLogExecution('DEBUG', 'serialin', serialin);
		
		nlapiLogExecution('DEBUG', 'custparam_actualbegintime', POarray["custparam_actualbegintime"]);
		nlapiLogExecution('DEBUG', 'custparam_actualbegintimeampm', POarray["custparam_actualbegintimeampm"]);
		nlapiLogExecution('DEBUG', 'recommendedQuantity ', recommendedQuantity );

		POarray["custparam_error"] = st11;

		POarray["custparam_screenno"] = '3';

		var getOptedField = request.getParameter('hdnOptedField');
		POarray["custparam_option"] = getOptedField;
		POarray["custparam_trantype"] = request.getParameter('hdntrantype');
		var getEnteredQty=request.getParameter('enterqty');
		// to the previous screen.
		var getNumber = request.getParameter('hdngetnumber');
		POarray["custparam_number"]=getNumber;
		var optedEvent = request.getParameter('cmdPrevious');
		var optedEventNext = request.getParameter('cmdNext');
		nlapiLogExecution('DEBUG', 'optedEvent',optedEvent);
		nlapiLogExecution('DEBUG', 'optedEventNext',optedEventNext);
		
		
		
		
		
		
	

	
		

		var ItemScaninQTY = getSystemRuleValue('Allow Item scan in Quantity field');
		nlapiLogExecution('DEBUG', 'ItemScaninQTY', ItemScaninQTY);
// case# 201415555
		/*if(parseFloat(request.getParameter('hdnQuantity')) == parseFloat(request.getParameter('hdnQuantityReceived')))
		{
			POarray["custparam_error"] = 'Total Order quantity already received';
			POarray["custparam_number"]="";
			POarray["custparam_poqtyentered"]="";
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			nlapiLogExecution('DEBUG', 'Entered Quantity in error', POarray["custparam_poqtyentered"]);
			return;			
		}*/
		var ExpectedQuantity=request.getParameter('hdnItemRemaininingQuantity');
		if (optedEvent == 'F7') {

			response.sendRedirect('SUITELET', 'customscript_rf_checkin_sku', 'customdeploy_rf_checkin_sku_di', false, POarray);
			return;
		}
		else if(optedEventNext=='F8')
		{
			
			
			
		
			



			if(getNumber==null || getNumber=='' || getNumber=='null')
			{
				getNumber=0;
			}
			if(getEnteredQty=='' || getEnteredQty==null || getEnteredQty=='null')
			{
				getEnteredQty=0;
			}
			//case 201410959
			if(ItemScaninQTY!='Y')
			{
				
				
				if(getItemType == 'serializedinventoryitem' || getItemType == 'serializedassemblyitem' || serialin == 'T')
				{
					if(parseInt(getEnteredQty) != getEnteredQty)
					{
						POarray["custparam_error"] = "Decimal qty is not allowed for serial item"; 
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('ERROR', 'Decimal qty is not allowed for serial item', getEnteredQty);
						return;
					}
				}
				
				if (isNaN(POarray["custparam_poqtyentered"]) || isNaN(getEnteredQty))		
				{
					POarray["custparam_error"] = 'Invalid Qty/UPC';
					POarray["custparam_number"]="";
					POarray["custparam_poqtyentered"]="";
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					nlapiLogExecution('DEBUG', 'Invalid Quantity', POarray["custparam_poqtyentered"]);
					return;
				}
				//Case# 201410607 starts
				if(getEnteredQty!=null && getEnteredQty!='' && getEnteredQty!='null' )
				{
					var result=ValidateSplCharacter(getEnteredQty,'');
					nlapiLogExecution('ERROR','result',result);
					if(result == false)
					{
						POarray["custparam_error"] = 'Invalid Qty/UPC';
						POarray["custparam_number"]=getNumber;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('DEBUG', 'Invalid Quantity', POarray["custparam_poqtyentered"]);
						return;
					}
				}
			}
			//Case# 201410607 ends
			// Case# 20148344 starts
			//if((parseFloat(getEnteredQty)+parseFloat(getNumber)) <= recommendedQuantity)
			nlapiLogExecution('DEBUG', 'Entered Quantity', getEnteredQty);
			nlapiLogExecution('DEBUG', 'recommendedQuantity', recommendedQuantity);

			//Case # 20148706? Start
			var UomQTY=getEnteredQty;

			var ItemId=request.getParameter('custparam_fetcheditemid');
			//case # 201410296
			if(ItemScaninQTY == 'Y')
			{
				//Case# 20148708 strats
				//var returnedItemId=validateSKU1(getEnteredQty, POarray["custparam_whlocation"], POarray["custparam_company"],POarray["custparam_pointernalid"]);
				//var returnedItemId=validateSKU1(ItemId, POarray["custparam_whlocation"], POarray["custparam_company"],POarray["custparam_pointernalid"]);
				var itemRecord;
				var returnedItemId='';
				var itemRecordArr = eBiz_RF_GetItemForItemIdWithArrNew(getEnteredQty,POarray["custparam_whlocation"]);
				nlapiLogExecution('DEBUG', 'itemRecordArr', itemRecordArr);
				var poLineDetails ='';
				if(itemRecordArr !=null && itemRecordArr.length>0)
				{
					poLineDetails =eBiz_RF_GetPOLineDetailsForItemArr(POarray["custparam_poid"], itemRecordArr,POarray["custparam_trantype"],null);
					if(poLineDetails != null && poLineDetails != '')
					{
						returnedItemId=poLineDetails[0].getValue('item');

					}				

					if(returnedItemId==null  || returnedItemId=="" )
					{
						POarray["custparam_error"] = 'Invalid Qty/UPC';
						POarray["custparam_number"]=getNumber;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('DEBUG', 'returnedItemId', returnedItemId);
						return;
					}
				//Case# 20148708 ends
				nlapiLogExecution('DEBUG', 'ItemId', ItemId);
				nlapiLogExecution('DEBUG', 'returnedItemId', returnedItemId);

				if(returnedItemId!=null && returnedItemId!='' )
				{
					if(returnedItemId==null  || parseInt(returnedItemId)!=parseInt(ItemId) )
					{
						POarray["custparam_error"] = 'Invalid Qty/UPC';
						POarray["custparam_number"]=getNumber;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_poqtyentered"]);
						return;
					}
					var itemUomQty=checkForItemAlias(returnedItemId,getEnteredQty);

						if(parseInt(itemUomQty)!=0)
						{
							UomQTY =itemUomQty;
						}
						else 
						{
							UomQTY =1;
						}
						getNumber=parseFloat(getNumber) + parseFloat(UomQTY);
					}
					else
					{
						getNumber=getEnteredQty;
						nlapiLogExecution('DEBUG', 'getNumber123@', getNumber);

						if(getItemType == 'serializedinventoryitem' || getItemType == 'serializedassemblyitem' || serialin == 'T')
						{
							if(parseInt(getNumber) != getNumber)
							{
								POarray["custparam_error"] = "Decimal qty is not allowed for serial item"; 
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								nlapiLogExecution('ERROR', 'Decimal qty is not allowed for serial item', getNumber);
								return;
							}
						}
					}
				}
				else
				{
					getNumber=getEnteredQty;
					if(getItemType == 'serializedinventoryitem' || getItemType == 'serializedassemblyitem' || serialin == 'T')
					{
						if(parseInt(POarray["custparam_poqtyentered"]) != POarray["custparam_poqtyentered"])
						{
							POarray["custparam_error"] = "Decimal qty is not allowed for serial item"; 
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							nlapiLogExecution('ERROR', 'Decimal qty is not allowed for serial item', getNumber);
							return;
						}
					}
				}
				
				

			}
			//case 201410929 start
			else
			{
				getNumber=getEnteredQty;
			}
			//case 201410929 end

			nlapiLogExecution('DEBUG', 'getNumber', getNumber);

			//case# 201410152 (Not allowing to process when qty is Zero)
			if(parseFloat(getNumber)!=0)
			{
				if(parseFloat(getNumber) <= recommendedQuantity)
					//Case# 20148344 starts
				{	


					//POarray["custparam_poqtyentered"]=parseFloat(getEnteredQty)+parseFloat(getNumber);
					POarray["custparam_poqtyentered"]=parseFloat(getNumber);//Case# 20148344
					response.sendRedirect('SUITELET', 'customscript_rf_checkin_item_status', 'customdeploy_rf_checkin_item_status_di', false, POarray);
					return;
				}
				else
				{
					POarray["custparam_error"] = "Quantity Exceeds Overage limit";
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);	
					return;	
				}
			}
			//case# 201410152 starts
			else
			{
				POarray["custparam_error"] = "Invalid Qty";
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);	
				return;	
			}
			//case# 201410152 ends
			//Case # 20148706� End
			//}
		}
		else
		{
			nlapiLogExecution('ERROR','ItemScaninQTY new',ItemScaninQTY);
			nlapiLogExecution('ERROR','getEnteredQty new',getEnteredQty);
			if(getEnteredQty <= 0)
			{
				POarray["custparam_error"] = 'Invalid Quantity';
				POarray["custparam_number"]="";
				POarray["custparam_poqtyentered"]="";
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'Invalid Quantity', POarray["custparam_poqtyentered"]);
				return;
			}
			if(ItemScaninQTY!='Y')
			{
				//case 201410959
				if (isNaN(POarray["custparam_poqtyentered"]) || isNaN(getEnteredQty))		
				{
					POarray["custparam_error"] = 'Invalid Qty/UPC';
					POarray["custparam_number"]="";
					POarray["custparam_poqtyentered"]="";
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					nlapiLogExecution('DEBUG', 'Invalid Quantity', POarray["custparam_poqtyentered"]);
					return;
				}
				
				
				if(getItemType == 'serializedinventoryitem' || getItemType == 'serializedassemblyitem' || serialin == 'T')
				{
					if(parseInt(getEnteredQty) != getEnteredQty)
					{
						POarray["custparam_error"] = "Decimal qty is not allowed for serial item"; 
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('ERROR', 'Decimal qty is not allowed for serial item', getEnteredQty);
						return;
					}
				}
				
				
				if(getEnteredQty==null  || getEnteredQty=='' ||getEnteredQty=='null' )
				{
					POarray["custparam_error"] = 'Invalid Qty/UPC';
					POarray["custparam_number"]=getNumber;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_poqtyentered"]);
					return;
				}
			}
			//Case# 201410607 starts
			/*if(getEnteredQty!=null && getEnteredQty!=''&& getEnteredQty!='null' )
			{
				var result=ValidateSplCharacter(getEnteredQty,'');
				nlapiLogExecution('ERROR','result',result);
				if(result == false)
				{
					POarray["custparam_error"] = 'Invalid Qty/UPC';
					POarray["custparam_number"]=getNumber;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					nlapiLogExecution('DEBUG', 'Invalid Quantity', POarray["custparam_poqtyentered"]);
					return;
				}
			}*/
			//Case# 201410607 ends
			nlapiLogExecution('DEBUG', 'poqty1', isNaN(POarray["custparam_poqtyentered"]));

			if(getNumber==null || getNumber=='' || getNumber=='null')
			{
				getNumber=0;
			}
			var UomQTY=0;

//			if user scan a barcode in the Qty screen then system should first check if there is an Item Alias record
			var ItemId=request.getParameter('custparam_fetcheditemid');

			if(ItemScaninQTY == 'Y')
			{
				//var returnedItemId=validateSKU1(getEnteredQty, POarray["custparam_whlocation"], POarray["custparam_company"],POarray["custparam_pointernalid"]);
				//above code is commented becuase system failed when ever two items have same UPC code.
				var itemRecord;
				var returnedItemId='';
				var itemRecordArr = eBiz_RF_GetItemForItemIdWithArrNew(getEnteredQty,POarray["custparam_whlocation"]);
				var poLineDetails ='';
				if(itemRecordArr != null && itemRecordArr.length>0){
					poLineDetails =eBiz_RF_GetPOLineDetailsForItemArr(POarray["custparam_poid"], itemRecordArr,POarray["custparam_trantype"],null);
					if(poLineDetails != null && poLineDetails != '')
					{
						returnedItemId=poLineDetails[0].getValue('item');

					}				

					if(returnedItemId==null  || returnedItemId=="" )
					{
						POarray["custparam_error"] = 'Invalid Qty/UPC';
						POarray["custparam_number"]=getNumber;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('DEBUG', 'returnedItemId', returnedItemId);
						return;
					}
				nlapiLogExecution('DEBUG', 'ItemId', ItemId);
				nlapiLogExecution('DEBUG', 'returnedItemId', returnedItemId);

				if(returnedItemId!=null && returnedItemId!='' )
				{
					if(returnedItemId==null  || parseInt(returnedItemId)!=parseInt(ItemId) )
					{
						POarray["custparam_error"] = 'Invalid Qty/UPC';
						POarray["custparam_number"]=getNumber;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_poqtyentered"]);
						return;
					}
					var itemUomQty=checkForItemAlias(returnedItemId,getEnteredQty);

					if(parseInt(itemUomQty)!=0)
					{
						UomQTY =itemUomQty;
					}
					else 
					{
						UomQTY =1;
					}

					}
				}
				
			}
			if(parseInt(UomQTY) !=0)
			{

				getNumber=parseFloat(getNumber) + parseFloat(UomQTY);
				nlapiLogExecution('DEBUG', 'getNumber', getNumber);
				nlapiLogExecution('DEBUG', 'getItemQuantity', ExpectedQuantity);
				var tempQty;
				/*if (poOverage == 0)
				{
					tempQty=parseFloat(ExpectedQuantity);

				}
				else
				{
					tempQty=parseFloat(recommendedQuantity);
				}*/

				tempQty=parseFloat(recommendedQuantity);
				if ((parseFloat(getNumber)) < parseFloat(tempQty)) {
					POarray["custparam_number"]=getNumber;
					POarray["custparam_poqtyentered"] = parseFloat(getNumber) ;
					response.sendRedirect('SUITELET', 'customscript_rf_checkin_qty', 'customdeploy_rf_checkin_qty_di', false, POarray);
					return;
				}
				else
				{
					POarray["custparam_poqtyentered"] = parseFloat(getNumber) ;//- parseFloat(itemUomQty);		



					//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
					//            if (optedEvent != '' && optedEvent != null) {

					nlapiLogExecution('DEBUG','Pickface Recommended Quantity', pickfaceRecommendedQuantity);

					if (poOverage == 0)
					{
						var recommendedQuantity1 = parseFloat(recommendedQuantity.toString());
						nlapiLogExecution('DEBUG','Quantity Entered', POarray["custparam_poqtyentered"]);
						nlapiLogExecution('DEBUG','Recommended Quantity', parseFloat(recommendedQuantity));

						/*if (parseFloat(POarray["custparam_poqtyentered"]) <= ExpectedQuantity)
						{*/
							if (POarray["custparam_poqtyentered"] > 0 && POarray["custparam_poqtyentered"] != "" && parseFloat(POarray["custparam_poqtyentered"]) <= ExpectedQuantity)
							{
								if(userAccountId!="TSTDRV909212")
								{
									response.sendRedirect('SUITELET', 'customscript_rf_checkin_item_status', 'customdeploy_rf_checkin_item_status_di', false, POarray);
									return;
								}
								else
								{									
									response.sendRedirect('SUITELET', 'customscript_rf_expectedbinsize', 'customdeploy_rf_expectedbinsize', false, POarray);
									return;
								}
							}
							else{
								if(parseFloat(getNumber) > parseFloat(ExpectedQuantity))
								{
								getNumber = parseFloat(getNumber)- parseFloat(itemUomQty);
							}


							POarray["custparam_number"]=getNumber;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_poqtyentered"]);
							return;
						}
						/*}*/					

						if (pickfaceRecommendedQuantity > 0)
						{
							if (parseFloat(POarray["custparam_poqtyentered"]) > parseFloat(pickfaceRecommendedQuantity))
							{	
								POarray["custparam_pickfaceexception"] = pickfaceRecommendedQuantity;
								/*
								 * If the option entered in the pickface exception is 'N', then allow the user to enter
								 * 	only the pickface recommended quantity
								 * else if the option entered in the pickface exception is 'Y', then generate a binlocation
								 * 	from the bulk location. Do not putaway to the pickface location.
								 */
								nlapiLogExecution('DEBUG','Erroflag', Erroflag);
								if ((bulkLocation != 'Y' || bulkLocation == null) && Erroflag=='F')
								{
									nlapiLogExecution('DEBUG','Into Pickface Recommended Quantity Validation','here');
									response.sendRedirect('SUITELET', 'customscript_rf_pickface_exception', 'customdeploy_rf_pickface_exception_di', false, POarray);
									nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_pickfaceexception"]);
									return;
								}
							}
						}
					}
					else{
						var palletQuantity1 = parseFloat(palletQuantity.toString());
						var recommendedQuantity1 = parseFloat(recommendedQuantity.toString());

						nlapiLogExecution('DEBUG', 'palletQuantity1 ',palletQuantity1 );
						nlapiLogExecution('DEBUG', 'recommendedQuantity1 ',recommendedQuantity1 );
						if(palletQuantity1 >= 1)
						{
							if (POarray["custparam_poqtyentered"] > 0 && POarray["custparam_poqtyentered"] != "" 
								&& parseFloat(POarray["custparam_poqtyentered"]) <= recommendedQuantity1)
							{
								if(userAccountId!="TSTDRV909212")
								{
									response.sendRedirect('SUITELET', 'customscript_rf_checkin_item_status', 'customdeploy_rf_checkin_item_status_di', false, POarray);
									return;
								}
								else
								{									
									response.sendRedirect('SUITELET', 'customscript_rf_expectedbinsize', 'customdeploy_rf_expectedbinsize', false, POarray);
									return;
								}
							}
							else{
								POarray["custparam_error"] = 'QUANTITY EXCEEDS OVERAGE LIMIT';
								if(parseFloat(getNumber) > parseFloat(recommendedQuantity1))
								{
									getNumber = parseFloat(getNumber)-parseFloat(itemUomQty);
								}
								POarray["custparam_number"]=getNumber;
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_poqtyentered"]);
								return;
							}
						}
						else
						{
							POarray["custparam_number"]=getNumber;
							POarray["custparam_error"] = 'PALLET QUANTITY IS NOT DEFINED FOR ITEM : '+POarray["custparam_getPOItem"];
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							return;
						}
					}
				}
			}					
			else
			{
				nlapiLogExecution('DEBUG', 'optedField', POarray["custparam_option"]);

				// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate

				var bulkLocation = request.getParameter('hdnEnteredOption');
				nlapiLogExecution('DEBUG','Entered Option', bulkLocation);
				var Erroflag='F';

				var getLanguage = request.getParameter('hdngetLanguage');
				POarray["custparam_language"] = getLanguage;

				nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);
				if(request.getParameter('custparam_number')!=null && request.getParameter('custparam_number')!='' && request.getParameter('custparam_number')!='null')
				{

					//POarray["custparam_poqtyentered"]= parseFloat(request.getParameter('custparam_number')) + parseFloat(getEnteredQty);//Case# 20148344
					POarray["custparam_poqtyentered"]= parseFloat(getEnteredQty);
					nlapiLogExecution('DEBUG', 'POarray["custparam_poqtyentered"]', POarray["custparam_poqtyentered"]);
				}
				else if(request.getParameter("custparam_poqtyentered") !=null && request.getParameter("custparam_poqtyentered")!='' && request.getParameter("custparam_poqtyentered")!='null')
				{
					//POarray["custparam_poqtyentered"]= parseFloat(request.getParameter('custparam_poqtyentered')) + parseFloat(getEnteredQty);//Case# 20148344
					POarray["custparam_poqtyentered"]= parseFloat(getEnteredQty);
				}
				else
				{

				}

				if (isNaN(POarray["custparam_poqtyentered"]) || isNaN(getEnteredQty))			
				{
					POarray["custparam_error"] = "Invalid Qty/UPC";
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);	
					return;
				}

				else {
					//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
					//            if (optedEvent != '' && optedEvent != null) {

					nlapiLogExecution('DEBUG','Pickface Recommended Quantity', pickfaceRecommendedQuantity);
					
					
					//nlapiLogExecution('DEBUG', 'getNumber', getNumber);
					
					if(getItemType == 'serializedinventoryitem' || getItemType == 'serializedassemblyitem' || serialin == 'T')
					{
						if(parseInt(POarray["custparam_poqtyentered"]) != POarray["custparam_poqtyentered"])
						{
							POarray["custparam_error"] = "Decimal qty is not allowed for serial item"; 
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							nlapiLogExecution('ERROR', 'Decimal qty is not allowed for serial item', getEnteredQty);
							return;
						}
					}

					if (poOverage == 0)
					{
						var recommendedQuantity1 = parseFloat(recommendedQuantity.toString());
						nlapiLogExecution('DEBUG','Quantity Entered', POarray["custparam_poqtyentered"]);
						nlapiLogExecution('DEBUG','Recommended Quantity', parseFloat(recommendedQuantity));
						nlapiLogExecution('DEBUG','ExpectedQuantity', parseFloat(ExpectedQuantity));

						if (parseFloat(POarray["custparam_poqtyentered"]) <= parseFloat(recommendedQuantity))
						{
							if (POarray["custparam_poqtyentered"] > 0 && POarray["custparam_poqtyentered"] != "")
							{
								if(userAccountId!="TSTDRV909212")
								{
									response.sendRedirect('SUITELET', 'customscript_rf_checkin_item_status', 'customdeploy_rf_checkin_item_status_di', false, POarray);
									return;
								}
								else
								{									
									response.sendRedirect('SUITELET', 'customscript_rf_expectedbinsize', 'customdeploy_rf_expectedbinsize', false, POarray);
									return;
								}
									
							}
							else{
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_poqtyentered"]);
							}
						}
						else if((parseFloat(POarray["custparam_poqtyentered"]) >  parseFloat(recommendedQuantity)))
						{
							//	if the 'Send' button is clicked without any option value entered,
							//  it has to show an error message. The next screen to which it has to navigate is to the error screen.
							POarray["custparam_error"] = 'OVERAGE NOT ALLOWED';
							Erroflag='T';
							nlapiLogExecution('DEBUG','Test4A', 'Test4A');

							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_poqtyentered"]);
						}
						else
						{
							POarray["custparam_error"] = 'INVALID QUANTITY';
							Erroflag='T';
							nlapiLogExecution('DEBUG','Test4AA', 'Test4AA');

							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_poqtyentered"]);
						}

						/*
						 * Check the pickface recommended quantity is less than the quantity entered.
						 * If the quantity entered is more than the pickface recommended quantity, ask the user to
						 * 		confirm if the quantity is to be putaway to the bulk location
						 * else
						 * 		follow the normal process of putting away the item to the assigned fixed pick location. 
						 */

						if (pickfaceRecommendedQuantity > 0)
						{
							if (parseFloat(POarray["custparam_poqtyentered"]) > parseFloat(pickfaceRecommendedQuantity))
							{	
								POarray["custparam_pickfaceexception"] = pickfaceRecommendedQuantity;
								/*
								 * If the option entered in the pickface exception is 'N', then allow the user to enter
								 * 	only the pickface recommended quantity
								 * else if the option entered in the pickface exception is 'Y', then generate a binlocation
								 * 	from the bulk location. Do not putaway to the pickface location.
								 */
								nlapiLogExecution('DEBUG','Erroflag', Erroflag);
								if ((bulkLocation != 'Y' || bulkLocation == null) && Erroflag=='F')
								{
									nlapiLogExecution('DEBUG','Into Pickface Recommended Quantity Validation','here');
									response.sendRedirect('SUITELET', 'customscript_rf_pickface_exception', 'customdeploy_rf_pickface_exception_di', false, POarray);
									nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_pickfaceexception"]);
								}
							}
						}
					}
					else{
						var palletQuantity1 = parseFloat(palletQuantity.toString());
						var recommendedQuantity1 = parseFloat(recommendedQuantity.toString());

						nlapiLogExecution('DEBUG', 'palletQuantity1 ',palletQuantity1 );
						nlapiLogExecution('DEBUG', 'recommendedQuantity1 ',recommendedQuantity1 );
						if(palletQuantity1 >= 1)
						{
							if (POarray["custparam_poqtyentered"] > 0 && POarray["custparam_poqtyentered"] != "" 
								&& parseFloat(POarray["custparam_poqtyentered"]) <= recommendedQuantity1)
							{
								if(userAccountId!="TSTDRV909212")
								{
									response.sendRedirect('SUITELET', 'customscript_rf_checkin_item_status', 'customdeploy_rf_checkin_item_status_di', false, POarray);
									return;
								}
								else
								{									
									response.sendRedirect('SUITELET', 'customscript_rf_expectedbinsize', 'customdeploy_rf_expectedbinsize', false, POarray);
									return;
								}
							}
							else{
								POarray["custparam_error"] = 'QUANTITY EXCEEDS OVERAGE LIMIT';
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_poqtyentered"]);
							}
						}
						else
						{
							POarray["custparam_error"] = 'PALLET QUANTITY IS NOT DEFINED FOR ITEM : '+POarray["custparam_getPOItem"];
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						}
					}
				}
				nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
			}
		}
	}//end of else loop.
}

function fetchPalletQuantity(itemId, location, company)
{
	nlapiLogExecution('DEBUG','itemId',itemId);
	nlapiLogExecution('DEBUG','location in fetchPalletQuantity',location);
	var itemPalletQuantity = 0;

	var itemFilters = new Array();
	itemFilters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemId));
	itemFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
//	case 20123368
	itemFilters.push(new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'anyof', 3));//3=Pallet
//	end
	if(location!=null&&location!='')
		itemFilters.push(new nlobjSearchFilter('custrecord_ebizsiteskudim',null,'anyof',['@NONE@',location]));

	var itemColumns = new Array();
	itemColumns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	itemColumns[1] = new nlobjSearchColumn('custrecord_ebizsiteskudim');
	itemColumns[2] = new nlobjSearchColumn('custrecord_ebizcompanyskudimension');
	itemColumns[3] = new nlobjSearchColumn('custrecord_ebizqty'); 	
	itemColumns[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim').setSort(true); 	

	var itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, itemFilters, itemColumns);

	if (itemSearchResults != null && itemSearchResults != '')
	{
		itemPalletQuantity = itemSearchResults[0].getValue(itemColumns[3]);
		nlapiLogExecution('DEBUG','Item Pallet Quantity',itemPalletQuantity);
	}
	return itemPalletQuantity;
}

function checkPOOverage(POId,location,company){
	nlapiLogExecution('DEBUG','PO Internal Id', POId);
	var poOverage = 0;

	var poFields = ['custbody_nswmsporeceipttype'];
	var poColumns = nlapiLookupField('transaction', POId, poFields);

	var receiptType = poColumns.custbody_nswmsporeceipttype;

	nlapiLogExecution('DEBUG','here', receiptType);

	if (receiptType != "" && receiptType != null) 
	{
		var receiptFields = ['custrecord_po_overages'];
		var receiptColumns = nlapiLookupField('customrecord_ebiznet_receipt_type', receiptType, receiptFields);

		nlapiLogExecution('DEBUG','here', receiptType);

		poOverage = receiptColumns.custrecord_po_overages;
	}
	nlapiLogExecution('DEBUG','Out of check PO Overage', poOverage);
	return poOverage;
}

function itemRemainingQuantity(poInternalId, itemId, lineno, itemQuantity, itemQuantityReceived,location, company){
	/*
	 * Fetch the check-in, putconfirm quantity from the transaction order details for the PO# and the line#
	 * if check-in quantity is 0 (Zero)
	 * 	remaining quantity = order quantity
	 * else if check-in quantity has value 
	 *		remaining quantity = order quantity - (check-in quantity) - quantity received
	 */

	nlapiLogExecution('DEBUG','itemId',itemId);
	nlapiLogExecution('DEBUG','lineno',lineno);
	nlapiLogExecution('DEBUG','poInternalId',poInternalId);

	nlapiLogExecution('DEBUG','Item Remaining Quantity','About to calculate');
	var systemRule= GetSystemRuleForPostItemReceiptby(location);
	nlapiLogExecution('DEBUG','systemRule',systemRule);
	var checkinQuantity = 0;
	var remainingQuantity = 0;
	var putConfirmQuantity = 0;

	var transactionFilters = new Array();
	transactionFilters[0] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', poInternalId);
	transactionFilters[1] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_sku_no', null, 'equalto', itemId);
	transactionFilters[2] = new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', lineno);

	var transactionColumns = new Array();
	transactionColumns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_ebiz_sku_no');
	transactionColumns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_orderline_no');
	transactionColumns[2] = new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty');
	transactionColumns[3] = new nlobjSearchColumn('custrecord_orderlinedetails_putconf_qty');

	var transactionSearchresults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, transactionFilters, transactionColumns);

	if (transactionSearchresults != null && transactionSearchresults.length > 0)
	{
		for(var i = 0; i <= transactionSearchresults.length; i++)
		{
			checkinQuantity = transactionSearchresults[0].getValue('custrecord_orderlinedetails_checkin_qty');
			putConfirmQuantity = transactionSearchresults[0].getValue('custrecord_orderlinedetails_putconf_qty');
		}
	}

	if(checkinQuantity == "")
	{
		checkinQuantity = 0;
	}

	if(putConfirmQuantity == "")
	{
		putConfirmQuantity = 0;
	}

	nlapiLogExecution('DEBUG','Check-In Quantity',checkinQuantity);
	nlapiLogExecution('DEBUG','Quantity',itemQuantity);
	nlapiLogExecution('DEBUG','Received Quantity',itemQuantityReceived);
	nlapiLogExecution('DEBUG','Put Confirm Quantity',putConfirmQuantity);

	if (checkinQuantity == 0)
	{
		remainingQuantity = parseFloat(itemQuantity);
	}
	else if(systemRule !='PO')
	{
		nlapiLogExecution('DEBUG','if systemRule',systemRule);
		remainingQuantity = parseFloat(itemQuantity) - (parseFloat(checkinQuantity)-parseFloat(putConfirmQuantity)) - parseFloat(itemQuantityReceived);
		nlapiLogExecution('DEBUG','Remaining Quantity',remainingQuantity);

		/*		if(putConfirmQuantity == 0)		
		{
			remainingQuantity = parseFloat(itemQuantity) - parseFloat(checkinQuantity) - parseFloat(itemQuantityReceived);
			nlapiLogExecution('DEBUG','Remaining Quantity: Checked-in',remainingQuantity);
		}
		else
		{
			remainingQuantity = parseFloat(itemQuantity) - parseFloat(itemQuantityReceived);
			nlapiLogExecution('DEBUG','Remaining Quantity: Putaway Confirmed',remainingQuantity);
		}
		 */
	}
	else
	{
		var GetTotalQty = GetTotalReceivedQty(poInternalId, itemId, lineno);
		nlapiLogExecution('DEBUG','GetTotalQty',GetTotalQty);
		var DirectQty = parseFloat(itemQuantityReceived)-(parseFloat(GetTotalQty));
		nlapiLogExecution('DEBUG','DirectQty',DirectQty);

		remainingQuantity = parseFloat(itemQuantity) - (parseFloat(checkinQuantity)+parseFloat(DirectQty));
		nlapiLogExecution('DEBUG','remainingQuantity',remainingQuantity);
	}
	return remainingQuantity;
}

function priorityPutawayQuantity(itemId, location, company)
{
	nlapiLogExecution('DEBUG','Priority Putaway Quantity','Beginning');
	nlapiLogExecution('DEBUG','itemId',itemId);
	nlapiLogExecution('DEBUG','location',location);
	nlapiLogExecution('DEBUG','company',company);

//	/*
//	* Search for the item in Pick Face Location custom record. Fetch the required columns 
//	* viz., Replen Quantity, Maximum Quantity, Bin Location


	var priorityPutawayQuantity = 0;

	var priorityPutawayFilters = new Array();
	priorityPutawayFilters.push(new nlobjSearchFilter('custrecord_pickfacesku', null,'is', itemId));
	priorityPutawayFilters.push(new nlobjSearchFilter('custrecord_priorityputaway', null, 'is', 'T'));	
	priorityPutawayFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	
	if(location!=null && location!='')
		priorityPutawayFilters.push(new nlobjSearchFilter('custrecord_pickface_location', null, 'anyof', [location]));

	var priorityPutawayColumns = new Array();
	priorityPutawayColumns[0] = new nlobjSearchColumn('custrecord_replenqty');
	priorityPutawayColumns[1] = new nlobjSearchColumn('custrecord_maxqty');
	priorityPutawayColumns[2] = new nlobjSearchColumn('custrecord_pickfacesku');
	priorityPutawayColumns[3] = new nlobjSearchColumn('custrecord_pickbinloc');

	var priorityPutawaySearchresults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc',null,priorityPutawayFilters,priorityPutawayColumns);

	if (priorityPutawaySearchresults != null)
	{
		for(var i = 0; i < priorityPutawaySearchresults.length; i++)
		{
			var ReplenQuantity = priorityPutawaySearchresults[0].getValue('custrecord_replenqty');
			var MaximumQuantity = priorityPutawaySearchresults[0].getValue('custrecord_maxqty');
			var pickfaceItem = priorityPutawaySearchresults[0].getValue('custrecord_pickfacesku');
			var binLocation = priorityPutawaySearchresults[0].getValue('custrecord_pickbinloc');
		}

		nlapiLogExecution('DEBUG','Maximum Quantity',MaximumQuantity);
		nlapiLogExecution('DEBUG','binLocation',binLocation);

//		/*
//		* Search for the data in inventory for the item, bin location and in storage locations. 
//		* Retrieve the quantity for those locations and sum the quantity for the individual lines fetched.

		var inventoryQuantity = 0;

		var inventoryFilters = new Array();
		inventoryFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', itemId));
		inventoryFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', binLocation));
		inventoryFilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3,19]));
		if(location!=null && location!='')
			inventoryFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [location]));



		var inventoryColumns = new Array();
		inventoryColumns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');

		var inventorySearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, inventoryFilters, inventoryColumns);

		if (inventorySearchResults != null)
		{
			for(var i = 0; i < inventorySearchResults.length; i++)
			{
				inventoryQuantity = parseFloat(inventoryQuantity) + parseFloat(inventorySearchResults[i].getValue('custrecord_ebiz_qoh'));
			}
		}

		nlapiLogExecution('DEBUG','Inventory Quantity',parseFloat(inventoryQuantity));

//		/*
//		* Search for any open tasks viz., Putaway, Replenishment, Inventory Move tasks

		var opentaskInventoryQuantity = 0;

		var opentaskInventoryFilters = new Array();
		opentaskInventoryFilters.push(new nlobjSearchFilter('custrecord_sku', null, 'is', itemId));
		opentaskInventoryFilters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', binLocation));
		opentaskInventoryFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2,8,9]));
		opentaskInventoryFilters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', ['@NONE@']));
		if(location!=null && location!='')
			opentaskInventoryFilters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', [location]));

		var opentaskInventoryColumns = new Array();
		opentaskInventoryColumns[0] = new nlobjSearchColumn('custrecord_expe_qty');

		var opentaskInventorySearch = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, opentaskInventoryFilters, opentaskInventoryColumns);

		if (opentaskInventorySearch != null)
		{
			for (var i = 0; i < opentaskInventorySearch.length; i++)
			{
				opentaskInventoryQuantity = opentaskInventoryQuantity + parseFloat(opentaskInventorySearch[i].getValue('custrecord_expe_qty')); 
			}
		}
		nlapiLogExecution('DEBUG','Opentask Inventory Quantity',opentaskInventoryQuantity);
//		/*
//		* Calculate the Recommended quantity for the pick face location from the above quantity retrieved.

		var pickfaceRecommendedQuantity = parseFloat(MaximumQuantity) - (parseFloat(inventoryQuantity)+ parseFloat(opentaskInventoryQuantity));

		nlapiLogExecution('DEBUG','Pickface Recommended Quantity',pickfaceRecommendedQuantity);
	}
	return pickfaceRecommendedQuantity;
}


function geteBizItemDimensions(itemid)
{
	var searchRec = new Array();
	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemid));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim') ;
	column[1] = new nlobjSearchColumn('custrecord_ebizqty') ;
	column[2] = new nlobjSearchColumn('custrecord_ebizbaseuom') ;
	column[3] = new nlobjSearchColumn('custrecord_ebiznsuom') ;
	column[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim') ;
	searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);

	return searchRec;

}
function checkForItemAlias(ItemId,ItemAlias)
{

	return eBiz_RF_GetItemQTYForItemAlias(ItemId,ItemAlias);

}
function eBiz_RF_GetItemQTYForItemAlias(ItemId,ItemAlias){
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemCubeForItem', 'Start');

	var BaseUOM=eBiz_RF_GetUOMAgainstItemAlias(ItemId,ItemAlias);

	var BaseUomQty=0;
	if(BaseUOM !='')
	{
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', ItemId);
		filters[1] = new nlobjSearchFilter('custrecord_ebizuomskudim', null, 'anyof', BaseUOM);

		var columns = new Array();	
		columns[0] = new nlobjSearchColumn('custrecord_ebizqty');

		var itemDimSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

		if(itemDimSearchResults != null && itemDimSearchResults.length > 0){

			//To get the baseuom qty .
			BaseUomQty=itemDimSearchResults[0].getValue('custrecord_ebizqty');
			//end of code as of 13 feb 2012.
		}



		nlapiLogExecution('DEBUG', 'Retrieved BaseUomQty', 'BaseUomQty = ' + BaseUomQty);
		nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemCubeForItem', 'End');
	}
	return BaseUomQty;
}

function eBiz_RF_GetUOMAgainstItemAlias(ItemId,ItemAlias)
{
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetUOMAgainstItemAlias', 'Start');

	var baseuom = "";

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_item', null, 'anyof', ItemId));
	if(ItemAlias !=null && ItemAlias!='')
		filters.push(new nlobjSearchFilter('name', null, 'is', ItemAlias));
	/*if(location!=null && location!='')
		filters.push(new nlobjSearchFilter('location', null, 'anyof', ['@NONE@',location]));*/

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_uom');

	var itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, filters, columns);

	if(itemSearchResults != null)
		baseuom = itemSearchResults[0].getValue('custrecord_ebiz_uom');

	var logMsg = 'baseuom = ' + baseuom;
	nlapiLogExecution('DEBUG', 'baseuom Retrieved', logMsg);
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetUOMAgainstItemAlias', 'End');

	return baseuom;
}



function validateSKU1(itemNo, location, company,poid){
	nlapiLogExecution('DEBUG', 'validateSKU', 'Start');

	var inputParams = 'Item = ' + itemNo + '<br>';
	inputParams = inputParams + 'Location = ' + location + '<br>';
	inputParams = inputParams + 'Company = ' + company + '<br>';;
	inputParams = inputParams + 'PO Id = ' + poid;
	nlapiLogExecution('DEBUG', 'Input Parameters', inputParams);

	var currItem = eBiz_RF_GetItemForItemNo1(itemNo);



	if(currItem==""){

		var skuAliasFilters = new Array();
		skuAliasFilters.push(new nlobjSearchFilter('name',null, 'is',itemNo));
		skuAliasFilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		if(location !=null && location!='')
			skuAliasFilters.push(new nlobjSearchFilter('custrecord_ebiz_location',null, 'is',location));


		var skuAliasCols = new Array();
		skuAliasCols[0] = new nlobjSearchColumn('custrecord_ebiz_item');

		var skuAliasResults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, skuAliasFilters, skuAliasCols);
		if(skuAliasResults !=null && skuAliasResults!='')
		{
			currItem = skuAliasResults[0].getValue('custrecord_ebiz_item');
		}
	}

	if(currItem == ""){
		currItem = eBiz_RF_GetItemBasedOnUPCCode(itemNo, location);
	}

	if(currItem == ""){
		var vendor='';
		if(poid!=null && poid!='')
		{
			var po  = nlapiLoadRecord('purchaseorder',poid);
			vendor=po.getFieldValue('entity');
		}
		currItem = eBiz_RF_GetItemFromSKUDims(itemNo, location,vendor);


	}

	var logMsg = "";
	if(currItem == ""){
		logMsg = 'Unable to retrieve item';
		currItem = null;
	} else {

		logMsg = 'Item = ' + currItem;
		nlapiLogExecution('DEBUG', 'Item Retrieved', logMsg);
		if(currItem !='' && isNaN(currItem))
		{
			var itemRecord = eBiz_RF_GetItemForItemId(currItem, location);
			currItem=itemRecord.getId();
			logMsg = 'Item = ' + currItem;
		}
	}

	nlapiLogExecution('DEBUG', 'Item Retrieved', logMsg);
	nlapiLogExecution('DEBUG', 'validateSKU', 'End');
	return currItem;
}

function eBiz_RF_GetItemForItemNo1(itemNo,location){
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemForItemNo', 'Start');
	nlapiLogExecution('DEBUG', 'Input Item No', itemNo);

	var currItem = "";

	var sitemno = "";
	var context = nlapiGetContext();
	var userAccountId = context.getCompany();
	if(userAccountId=='1285441')
	{
		sitemno = checkserialritem(itemNo);
	}
	if(sitemno != "")
	{
		itemNo = sitemno;
	}	

	var filters = new Array();
	filters.push(new nlobjSearchFilter('nameinternal', null, 'is', itemNo));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(location!=null && location!='' && location!='null')
		filters.push(new nlobjSearchFilter('location', null, 'anyof', ['@NONE@',location]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('itemid');
	columns[0].setSort(true);


	var itemSearchResults = nlapiSearchRecord('item', null, filters, columns);

	if(itemSearchResults != null)
		currItem = itemSearchResults[0].getId();

	var logMsg = 'Item = ' + currItem;
	nlapiLogExecution('DEBUG', 'Item Retrieved', logMsg);
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemForItemNo', 'End');

	return currItem;
}




function GetSystemRuleForPostItemReceiptby(whloc) //Case# 20149154
{
	try
	{
		var rulevalue='';	
		var filter=new Array();
		filter.push(new nlobjSearchFilter('name',null,'is','Post Item Receipt by'));
		//filter.push(new nlobjSearchFilter('custrecord_ebizsite',null,'anyof',['@NONE@',whLocation]));

		// case no start 20126968
		var vRoleLocation=getRoledBasedLocation();
		var resloc=new Array();
		resloc.push("@NONE@");

		nlapiLogExecution('ERROR','vRoleLocation',vRoleLocation);
		//case# 20149824
		if(whloc!=null && whloc!='')
		{
			filter.push(new nlobjSearchFilter('custrecord_ebizsite',null,'anyof',['@NONE@',whloc]));
		}
		else if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
		{
			for(var count=0;count<vRoleLocation.length;count++)
				resloc.push(vRoleLocation[count]);
			filter.push(new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', resloc));
		}
		// Case# 20149154 starts
		/*else
			{
			filter.push(new nlobjSearchFilter('custrecord_ebizsite',null,'anyof',['@NONE@',whloc]));
			}*/
		// Case# 20149154 ends
		// case no end 20126968
		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizrulevalue');
		column[1]=new nlobjSearchColumn('custrecord_ebizsite');

		var searchresult=nlapiSearchRecord('customrecord_ebiznet_sysrules',null,filter,column);
		var siteval = '';
		if(searchresult!=null && searchresult!="")
		{
			for(var k = 0;k<searchresult.length;k++)
			{
				siteval = searchresult[k].getValue('custrecord_ebizsite');
				nlapiLogExecution('ERROR','whloc',whloc);
				nlapiLogExecution('ERROR','siteval',siteval);

				if(whloc!=null && whloc!='')
				{
					if(whloc == siteval)
					{
						rulevalue=searchresult[k].getValue('custrecord_ebizrulevalue');
						break;
					}
				}
			}

			if(rulevalue==null || rulevalue=='')
			{
				for(var z = 0;z<searchresult.length;z++)
				{
					siteval = searchresult[z].getValue('custrecord_ebizsite');

					if(siteval==null || siteval=='')
					{
						rulevalue=searchresult[z].getValue('custrecord_ebizrulevalue');
						break;
					}
				}

			}
		}

		nlapiLogExecution('ERROR','rulevalue',rulevalue);
		if(rulevalue!=null && rulevalue!='undefined' && rulevalue!='null' && rulevalue!='')
		{
			if(rulevalue.trim()!='LP' && rulevalue.trim()!='PO')
			{
				rulevalue='LP';
			}
		}
		else
		{
			rulevalue='LP';
		}
		nlapiLogExecution('ERROR','rulevalue',rulevalue);
		return rulevalue;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetSystemRuleForPostItemReceiptby',exp);
	}
}
function ValidateSplCharacter(string,name)
{
	try
	{
		//var iChars = "*|,\":<>[]{}`\';()@&$#% ";
		var iChars = "*|,\"<>[]{}`\';()@&$#%";
		var length=string.length;
		var flag = 'N';
		for(var i=0;i<length;i++)
		{
			if(iChars.indexOf(string.charAt(i))!=-1)
			{
				flag='Y';
				break;
			}
		}
		if(flag == 'Y')
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	catch(e)
	{
		nlapiLogExecution("ERROR","ValidateSplCharacter",e);
		return false;
	}
}