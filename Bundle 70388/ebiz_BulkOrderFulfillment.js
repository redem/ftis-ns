/***************************************************************************
                                                                 
                                                  eBizNET Solutions Inc 
****************************************************************************
*
*  $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_BulkOrderFulfillment.js,v $
*  $Revision: 1.9.6.1.4.1.6.1 $
*  $Date: 2015/09/23 14:57:48 $
*  $Author: deepshikha $
*  $Name: t_WMS_2015_2_StdBundle_1_13 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_BulkOrderFulfillment.js,v $
*  Revision 1.9.6.1.4.1.6.1  2015/09/23 14:57:48  deepshikha
*  2015.2 issueFix
*  201414466
*
*  Revision 1.9.6.1.4.1  2013/09/11 15:23:51  rmukkera
*  Case# 20124376
*
*
****************************************************************************/
function addAllSalesOrdersToField(soField, salesOrderList){
	if(salesOrderList != null && salesOrderList.length > 0){
		for(var i = 0; i < salesOrderList.length; i++){
			soField.addSelectOption(salesOrderList[i].getValue('tranid'), salesOrderList[i].getValue('tranid'));
		}
	}
}

/**
 * Function to retrieve sales orders
 * @returns
 */
function getAllSalesOrders(){
	nlapiLogExecution('ERROR', '      getAllSalesOrders', 'start');
	
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('mainline', null, 'is', 'T');
	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('tranid');
	columns[0].setSort(true);
	
	var searchResults = nlapiSearchRecord('salesorder', null, filters, columns);
	
	nlapiLogExecution('ERROR', '      Cound of sales orders retrieved', searchResults.length);
	nlapiLogExecution('ERROR', '      getAllSalesOrders', 'end');

	return searchResults;
}

/**
 * Function to return the list of sales order numbers in the form of an array
 * 
 * @param salesOrderList
 * @returns {Array}
 */
function getSalesOrderNos(salesOrderList){
	var salesOrderNoList = new Array();
	
	if(salesOrderList != null && salesOrderList.length > 0){
		for(var i = 0; i < salesOrderList.length; i++){
			var currentOrder = salesOrderList[i];
			salesOrderNoList.push(currentOrder.getId());
		}
	}
	
	return salesOrderNoList;
}

/**
 * Function to retrieve order line information for all sales orders 
 * 
 * @param salesOrderList
 * @returns {Array}
 */
function getSalesOrderLineDetails(salesOrderList){
	nlapiLogExecution('ERROR', '      getSalesOrderLineDetails', 'Start');
	
	var salesOrderLineDetails = new Array();
	
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('mainline', null, 'is', 'F');						// Retrieve order lines
	filters[1] = new nlobjSearchFilter('quantitycommitted', null, 'greaterthan', 0);		// Committed Quantity > 0
	
	// Pending Fulfillment / Partially Fulfilled / Pending Billing_Partially Fulfilled
	filters[2] = new nlobjSearchFilter('status', null, 'anyof', ['SalesOrd:B','SalesOrd:D','SalesOrd:E']);
	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('tranid');
	columns[1] = new nlobjSearchColumn('trandate');
	columns[2] = new nlobjSearchColumn('entity');
	columns[3] = new nlobjSearchColumn('custbody_nswmspriority');
	columns[4] = new nlobjSearchColumn('custbody_nswmssoordertype');
	columns[5] = new nlobjSearchColumn('shipcarrier');
	columns[6] = new nlobjSearchColumn('custbody_nswms_company');
	columns[7] = new nlobjSearchColumn('custbody_nswmssoroute');
	columns[8] = new nlobjSearchColumn('line');
	columns[9] = new nlobjSearchColumn('item');
	columns[10] = new nlobjSearchColumn('custcol_ebiznet_item_status');
	columns[11] = new nlobjSearchColumn('serialnumbers');
	columns[12] = new nlobjSearchColumn('custcol_nswmspackcode');
	columns[13] = new nlobjSearchColumn('custcol_nswmssobaseuom');
	columns[14] = new nlobjSearchColumn('quantity');
	columns[15] = new nlobjSearchColumn('quantitycommitted');
	columns[0].setSort(true);
	
	salesOrderLineDetails = new nlapiSearchRecord('salesorder', null, filters, columns);
	
	logCountMessage('ERROR', salesOrderLineDetails);
	nlapiLogExecution('ERROR', '      getSalesOrderLineDetails', 'End');
	return salesOrderLineDetails;
}

function addFieldsToSublist(form){
	var itemSubList = form.addSubList("custpage_items",  "list", "ItemList");
	
	// Add 'Mark All' buttons
	itemSubList.addMarkAllButtons();
	
	itemSubList.addField("custpage_bulkorder_sel", "checkbox","Bulk SO").setDefaultValue('T');
	itemSubList.addField("custpage_bulkorder_salesorder", "text","Sales Order #").setDisplayType('hidden');
	itemSubList.addField("custpage_bulkorder_fulfillorder", "text", "Fulfill Order #");
	itemSubList.addField("custpage_bulkorder_lineno", "text", "Line #");
	itemSubList.addField("custpage_bulkorder_customer", "text", "Customer");
	itemSubList.addField("custpage_bulkorder_customer_id", "text", "Customer Id").setDisplayType('hidden');
	itemSubList.addField("custpage_bulkorder_orderpriority", "text", "Order Priority");
	itemSubList.addField("custpage_bulkorder_ordertype", "text", "Order Type");
	itemSubList.addField("custpage_bulkorder_carrier", "text", "Carrier");
	itemSubList.addField("custpage_bulkorder_item", "text", "Item");
	itemSubList.addField("custpage_bulkorder_item_id", "text", "Item Id").setDisplayType('hidden');
	itemSubList.addField("custpage_bulkorder_itemstatus", "text", "Item Status");
	itemSubList.addField("custpage_bulkorder_lotbatch", "text", "Lot/Batch");
	itemSubList.addField("custpage_bulkorder_packcode", "text", "Pack Code");
	itemSubList.addField("custpage_bulkorder_baseuom", "text", "Base UOM");
	itemSubList.addField("custpage_bulkorder_orderqty", "text", "Order Qty");
	itemSubList.addField("custpage_bulkorder_customervalue", "text", "customervalue").setDisplayType('hidden');
	itemSubList.addField("custpage_bulkorder_itemvalue", "text", "itemvalue").setDisplayType('hidden');
	itemSubList.addField("custpage_bulkorder_itemstatusvalue", "text", "itemstatusvalue").setDisplayType('hidden');		              
	itemSubList.addField("custpage_bulkorder_fulfillmentqty", "text", "Fulfillment Qty").setDisplayType('entry');
	itemSubList.addField("custpage_bulkorder_wmslocation", "text", "wmsLocation").setDisplayType('hidden');
}

function getCriteriaForOrdersToFulfill(salesOrderNo, customer, priority, orderType, carrier, company, route, location){
	var filters = new Array();
	
	if(salesOrderNo != null && salesOrderNo != "")
		filters.push(new nlobjSearchFilter('tranid', null, 'is', salesOrderNo));
	
	if(customer != null && customer != "")
		filters.push(new nlobjSearchFilter('entity', null, 'is', customer));
	
	if(priority != null && priority != "")
		filters.push(new nlobjSearchFilter('custbody_nswmspriority', null, 'is', priority));

	if(orderType != null && orderType != "")
		filters.push(new nlobjSearchFilter('custbody_nswmssoordertype', null, 'is', orderType));
	
	if(carrier != null && carrier != "")
		filters.push(new nlobjSearchFilter('shipcarrier', null, 'is', carrier));

	if(company != null && company != "")
		filters.push(new nlobjSearchFilter('custbody_nswms_company', null, 'is', company));

	if(route != null && route != "")
		filters.push(new nlobjSearchFilter('custbody_nswmssoroute', null, 'is', route));

	if(location != null && location != "")
		filters.push(new nlobjSearchFilter('location', null, 'is', location));
	
	// Add filter criteria for order status;
	// Search for B:'pendingFulfillment'; D:'partiallyFulfilled', E:'pendingBilling/partially fulfilled'
	filters.push(new nlobjSearchFilter('status', null, 'anyof', ['SalesOrd:B','SalesOrd:D','SalesOrd:E']));
	
	// Get only order headers
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
	
	return filters;
}

function defineColumnsForFulfillmentOrders(){
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('tranid');
	columns[1] = new nlobjSearchColumn('trandate');
	columns[2] = new nlobjSearchColumn('entity');
	columns[3] = new nlobjSearchColumn('custbody_nswmspriority');
	columns[4] = new nlobjSearchColumn('custbody_nswmssoordertype');
	columns[5] = new nlobjSearchColumn('shipcarrier');
	columns[6] = new nlobjSearchColumn('custbody_nswms_company');
	columns[7] = new nlobjSearchColumn('custbody_nswmssoroute');
	columns[8] = new nlobjSearchColumn('line');
	columns[9] = new nlobjSearchColumn('item');
	columns[10] = new nlobjSearchColumn('custcol_ebiznet_item_status');
	columns[11] = new nlobjSearchColumn('serialnumbers');
	columns[12] = new nlobjSearchColumn('custcol_nswmspackcode');
	columns[13] = new nlobjSearchColumn('custcol_nswmssobaseuom');
	columns[14] = new nlobjSearchColumn('quantity');
	columns[0].setSort(true);

	return columns;
}

/**
 * Get the list of fulfillment orders already generated for the given list of sales order numbers
 * 
 * @param salesOrderNoList
 * @returns {Array}
 */
function getExistingFulfillmentOrders(salesOrderNoList){
	nlapiLogExecution('ERROR', '      getExistingFulfillmentOrders', 'Start');
	var fulfillOrderList = new Array();
	nlapiLogExecution('ERROR', 'salesOrderNoList', salesOrderNoList);
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ns_ord', null, 'anyof', salesOrderNoList);
	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_lineord');
	columns[1] = new nlobjSearchColumn('custrecord_ordline');
	columns[2] = new nlobjSearchColumn('custrecord_ord_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ns_ord');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_linesku');
	columns[5] = new nlobjSearchColumn('custrecord_linesku_status');
	columns[6] = new nlobjSearchColumn('custrecord_lineuom_id');
	columns[7] = new nlobjSearchColumn('custrecord_do_customer');
	columns[8] = new nlobjSearchColumn('custrecord_pickqty');
	
	fulfillOrderList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
	
	logCountMessage('ERROR', fulfillOrderList);
	nlapiLogExecution('ERROR', '      getExistingFulfillmentOrders', 'Start');
	return fulfillOrderList;
}

/**
 * 
 * @param salesOrderNo
 * @param customer
 * @param priority
 * @param orderType
 * @param carrier
 * @param company
 * @param route
 * @param location
 * @returns
 */
function getSalesOrderDetails(salesOrderNo, customer, priority, orderType, carrier, company, route, location){
	nlapiLogExecution('ERROR', '      getSalesOrderDetails', 'Start');
	
	// Get filter criteria and output columns for sales order search
	var filters = getCriteriaForOrdersToFulfill(salesOrderNo, customer, priority, orderType, carrier, company, route, location);
	var columns = defineColumnsForFulfillmentOrders();

	var salesOrderDetails = nlapiSearchRecord('salesorder', null, filters, columns);
	
	logCountMessage('ERROR', salesOrderDetails);
	
	nlapiLogExecution('ERROR', '      getSalesOrderDetails', 'End');
	return salesOrderDetails;
}

/**
 * 
 * @param salesOrderLineDtls
 * @param salesOrderId
 * @returns {Array}
 */
function getCurrentOrderLineDtls(salesOrderLineDtls, salesOrderId){
	var lineIndexList = new Array();
	
	if(salesOrderLineDtls != null && salesOrderLineDtls.length > 0){
		for(var i = 0; i < salesOrderLineDtls.length; i++){
			var parentOrderId = salesOrderLineDtls[i].getId();
			if(parseFloat(salesOrderId) == parseFloat(parentOrderId)){
				lineIndexList.push(i);
			}
		}
	}
	
	return lineIndexList;
}

/**
 * Function to consolidate order and order line information into a single array
 * 
 * @param salesOrderDtls
 * @param salesOrderLineDtls
 * @returns {Array}
 */
function getCompleteOrderDetails(salesOrderDtls, salesOrderLineDtls, customer, priority, 
		orderType, carrier, company, route, location){
	nlapiLogExecution('ERROR', '      getCompleteOrderDetails', 'Start');
	
	var orderDtls = new Array();
	
	if(salesOrderDtls != null && salesOrderDtls.length > 0){
		for(var i = 0; i < salesOrderDtls.length; i++){
			var currentOrder = salesOrderDtls[i];
			var salesOrderNo = currentOrder.getValue('tranid');
			var salesOrderId = currentOrder.getId();
			
			// Get index list for the current order in order line details
			var lineIndexList = getCurrentOrderLineDtls(salesOrderLineDtls, salesOrderId);
			
			// Continue to the next order if no line details are returned
			if(lineIndexList.length == 0)
				continue;
			
			var customer = currentOrder.getText('entity');
			var customerId = currentOrder.getValue('entity');
			var carrier = currentOrder.getValue('shipcarrier');
			
			for(var j = 0; j < lineIndexList.length; j++){
				var index = lineIndexList[j];
				var lineNo = salesOrderLineDtls[index].getValue('line');
				var item = salesOrderLineDtls[index].getText('item');
				var itemId = salesOrderLineDtls[index].getValue('item');
				var itemStatus = salesOrderLineDtls[index].getText('custcol_ebiznet_item_status');
				var itemStatusvalue = salesOrderLineDtls[index].getValue('custcol_ebiznet_item_status');
				var serialNos = salesOrderLineDtls[index].getValue('serialnumbers');
				var packcode = salesOrderLineDtls[index].getValue('custcol_nswmspackcode');
				var baseUOM = salesOrderLineDtls[index].getValue('custcol_nswmssobaseuom');
				var orderLineQty = salesOrderLineDtls[index].getValue('quantity');
				var committedQty = salesOrderLineDtls[index].getValue('quantitycommitted');
				
				var currentRow = [salesOrderNo, salesOrderId, customer, carrier,
				                  lineNo, item, itemStatus, serialNos, packcode, baseUOM, 
				                  orderLineQty, committedQty, priority, orderType, company,
				                  route, location, itemId, customerId, itemStatusvalue];
				
				// Adding information to the order details
				orderDtls.push(currentRow);
			}
		}
	}

	logCountMessage('ERROR', orderDtls);
	nlapiLogExecution('ERROR', '      getCompleteOrderDetails', 'Start');
	return orderDtls;
}

/**
 * Function to return the quantity already fulfilled for an order-orderline combination
 * 
 * @param currOrderLine
 * @param fulfillOrderList
 * @returns {Number}
 */
function getQtyFulfilledForLine(currOrderLine, fulfillOrderList){
	var qtyFulfilled = -1;
	
	if(fulfillOrderList != null && fulfillOrderList.length > 0){
		for(var i = 0; i < fulfillOrderList.length; i++){
			var currFulfillOrder = fulfillOrderList[i];
			var currOrderId = currOrderLine[1];
			var currOrderLineNo = currOrderLine[4];
			var fulfillOrderId = currFulfillOrder.getText('custrecord_ns_ord');
			var fulfillOrderNo = currFulfillOrder.getValue('custrecord_ns_ord');
			var fulfillOrderLine = currFulfillOrder.getValue('custrecord_ordline');
			//var fulfillOrderQty = currFulfillOrder.getValue('custrecord_ord_qty');//commented by shylaja
			var fulfillOrderQty = currFulfillOrder.getValue('custrecord_pickqty');
			
			if (parseFloat(currOrderId) == parseFloat(fulfillOrderNo) &&
				parseFloat(currOrderLineNo) == parseFloat(fulfillOrderLine)){
				if(parseFloat(qtyFulfilled) == -1)
					qtyFulfilled = parseFloat(fulfillOrderQty);
				else
					qtyFulfilled = parseFloat(qtyFulfilled) + parseFloat(fulfillOrderQty);
			}
		}
	}

	return qtyFulfilled;
}

/**
 * 
 * @param completeOrderDtls
 * @param fulfillOrderList
 * @returns {Array}
 */
function determineCommittedQtyForOrders(completeOrderDtls, fulfillOrderList){
	nlapiLogExecution('ERROR', '      determineCommittedQtyForOrders', 'Start');
	var newCommittedQtyDtls = new Array();
	
	// Determine pending fulfillment qty for order and line
	if(completeOrderDtls != null && completeOrderDtls.length > 0){
		for(var i = 0; i < completeOrderDtls.length; i++){
			var newFulfillOrderQty = -1;

			var currOrderLine = completeOrderDtls[i];
			var orderNo = currOrderLine[0];
			var orderId = currOrderLine[1];
			var customer = currOrderLine[2];
			var carrier = currOrderLine[3];
			var lineNo = currOrderLine[4];
			var item = currOrderLine[5];
			var itemStatus = currOrderLine[6];
			var serialNos = currOrderLine[7];
			var packCode = currOrderLine[8];
			var baseUOM = currOrderLine[9];
			var orderLineQty = currOrderLine[10];
			var committedQty = currOrderLine[11];
			var priority = currOrderLine[12];
			var orderType = currOrderLine[13];
			var company = currOrderLine[14];
			var route = currOrderLine[15];
			var location = currOrderLine[16];
			var itemId = currOrderLine[17];
			var customerId = currOrderLine[18];
			var itemStatusValue = currOrderLine[19];

			// Determining the qty already fulfilled for this order line
			var qtyFulfilled = getQtyFulfilledForLine(currOrderLine, fulfillOrderList);
			
			nlapiLogExecution('ERROR', '      committedQty', committedQty);
			nlapiLogExecution('ERROR', '      qtyFulfilled', qtyFulfilled);
			
			if(parseFloat(qtyFulfilled) == -1){
				newFulfillOrderQty = parseFloat(committedQty);
			} else if(parseFloat(committedQty) > parseFloat(qtyFulfilled)){
				newFulfillOrderQty = parseFloat(committedQty) - parseFloat(qtyFulfilled);
			}/*//added by shylaja ch
			else if(parseFloat(qtyFulfilled) > parseFloat(committedQty)){
				newFulfillOrderQty = parseFloat(qtyFulfilled) - parseFloat(committedQty);
			}*/
			
			nlapiLogExecution('ERROR', '      newFulfillOrderQty', newFulfillOrderQty);

			if(parseFloat(newFulfillOrderQty) > -1){
				// Get the fulfillment order number along with the suffix
				var suffix = generateNewFulfillOrderSuffix(orderId, fulfillOrderList);
				var newFulfillmentOrderNumber = orderNo + '.' + suffix;
				nlapiLogExecution('ERROR', '      newFulfillmentOrderNumber', newFulfillmentOrderNumber);
				var currentRow = [newFulfillmentOrderNumber, orderId, lineNo, item, newFulfillOrderQty, customer,
				                  carrier, priority, orderType, company, route, location, itemStatus,
				                  serialNos, packCode, baseUOM, orderLineQty, committedQty, itemId, customerId,
				                  itemStatusValue];
				newCommittedQtyDtls.push(currentRow);
			}
		}
	}

	logCountMessage('ERROR', newCommittedQtyDtls);
	nlapiLogExecution('ERROR', '      determineCommittedQtyForOrders', 'Start');
	return newCommittedQtyDtls;
}

/**
 * 
 * @param lineOrderNo
 * @returns
 */
function getSuffixForLineOrd(lineOrderNo){
	return parseFloat(lineOrderNo.split('.')[1]);
}

/**
 * 
 * @param orderId
 * @param fulfillOrderList
 * @returns {String}
 */
function generateNewFulfillOrderSuffix(orderId, fulfillOrderList){
	var largestSuffix = '0';
	
	if(fulfillOrderList != null && fulfillOrderList.length > 0){
		for(var i = 0; i < fulfillOrderList.length; i++){
			var currFulfillOrder = fulfillOrderList[i];
			var lineOrderNo = currFulfillOrder.getValue('custrecord_lineord');
			var salesOrderId = currFulfillOrder.getValue('custrecord_ns_ord');
			if(parseFloat(salesOrderId) == parseFloat(orderId)){
				var suffix = getSuffixForLineOrd(lineOrderNo);
				if(parseFloat(suffix) > parseFloat(largestSuffix))
					largestSuffix = parseFloat(suffix);
			}
		}
	}
	
	// Incrementing to get the new fulfillment order suffix
	largestSuffix++;
	
	return largestSuffix;
}

/**
 * This function will retrieve all the sales order lines that are yet to be fulfilled.
 * 
 * Pseudo Code:
 * 	For all sales order with status = pending fulfillment, partially fulfilled or pending billing/partially fulfilled
 * 		Get the list of fulfillment orders created from ordline
 * 		For each of these fulfillment orders,
 * 			Determine the fulfillment order quantity and compare with the committed quantity of the order line
 * 			If the fulfilled quantity is greater than equal to the committed quantity
 * 				Generate a new fulfillment order for the remaining order line quantity
 * 				Specify the fulfillment order number by incrementing the last used number
 * 			End if
 * 		End For
 * 	End For
 * 
 * @param salesOrderNo
 * @param customer
 * @param priority
 * @param orderType
 * @param carrier
 * @param company
 * @param route
 * @param location
 * @returns
 */
function getOrdersToFulfillByCriteria(salesOrderNo, customer, priority, orderType, carrier, company, route, location){
	nlapiLogExecution('ERROR', '      getOrdersToFulfillByCriteria', 'Start');
	
	// Get Sales Order Details
	var salesOrderDetails = getSalesOrderDetails(salesOrderNo, customer, priority, orderType, carrier, company, route, location);
	
	// Get the order line details for the list of sales orders
	var salesOrderLineDetails = getSalesOrderLineDetails(salesOrderDetails);

	// Consolidate order and order line information
	var completeOrderDtls = getCompleteOrderDetails(salesOrderDetails, salesOrderLineDetails,
			customer, priority, orderType, carrier, company, route, location);
	
	// Get list of sales order numbers
	var salesOrderNoList = getSalesOrderNos(salesOrderDetails);
	
	// Get all the fulfillment orders for these sales orders
	var fulfillOrderList = getExistingFulfillmentOrders(salesOrderNoList);
	
	// Get new committed qty details for all sales orders
	// The return array has order#, orderId, orderLine#, item and pendingCommittedQty 
	nlapiLogExecution('ERROR', 'hello', 'hello');
	var newCommittedQtyDtls = determineCommittedQtyForOrders(completeOrderDtls, fulfillOrderList);

	nlapiLogExecution('ERROR', '      getOrdersToFulfillByCriteria', 'End');
	return newCommittedQtyDtls;
}

/**
 * 
 * @param fulfillmentQty
 * @returns {Boolean}
 */
function validateFulfillmentQty(fulfillmentQty){
	var isValidQty = true;
	
	if(isNaN(fulfillmentQty) && parseFloat(fulfillmentQty) <= 0){
		nlapiLogExecution('ERROR', 'Fulfillment order qty is not a number');
		var message = 'Invalid Fulfillment Order Quantity; Fulfillment Order Quantity should be greater than 0';
		var messageVar = ' Fulfillment Order Qty = ' + fulfillmentQty;
		showInlineMessage(form, 'Error', message, messageVar);
		isValidQty = false;
	}
	
	return isValidQty;
}

/**
 * Function to render the search criteria to display all the possible fulfillment
 * orders that can be created based on the provided search criteria
 * 
 * @param request
 * @param response
 */
function showBulkFulfillmentOrderCriteria(request, response){
	nlapiLogExecution('ERROR', '   showBulkFulfillmentOrderCriteria', 'START');
	
	var form = nlapiCreateForm('Bulk Order Fulfillment Screen');

	// Select Location in Search Criteria
	var trlLocation = form.addField('custpage_location', 'select', 'Location','location');
	trlLocation.setLayoutType('startrow', 'none');
	trlLocation.setMandatory(true);

	// Sales order number in search criteria
	var soField  = form.addField('custpage_so', 'select', 'Order #').setLayoutType('startrow', 'none');
	soField.addSelectOption("","");
	
	// Retrieve all sales orders
	var salesOrderList = getAllSalesOrders();
	
	// Add all sales orders to SO Field
	addAllSalesOrdersToField(soField, salesOrderList);

	// Customer
	var trlCustomer = form.addField('custpage_customer', 'select', 'Customer', 'customer');
	trlCustomer.setLayoutType('startrow', 'none');

	// Order Priority
	var trlPriority = form.addField('custpage_priority', 'select', 'Order Priority', 'customlist_ebiznet_order_priority');
	trlPriority.setLayoutType('startrow', 'none');	

	// Order Type
	var trlOrderType = form.addField('custpage_ordertype', 'select', 'Order Type', 'customrecord_ebiznet_order_type');
	trlOrderType.setLayoutType('startrow', 'none');

	// Carrier
	var trlCarrier = form.addField('custpage_carrier', 'select', 'Carrier');	
	trlCarrier.setLayoutType('startrow', 'none');

	// Company
	var trlcompany = form.addField('custpage_company', 'select', 'Company', 'customrecord_ebiznet_company');
	trlcompany.setLayoutType('startrow', 'none');

	// Route
	var trlRouteField = form.addField('custpage_routeno', 'select', 'Route','customlist_ebiznetroutelov');
	trlRouteField.setLayoutType('startrow', 'none');

	// INDICATOR FIELD OF IN PROCESS STATUS; DO NOT DELETE
	var vData = form.addField('custpage_data', 'text', 'ProcessCriteria', 'InSearchCriteria').setDisplayType('hidden');
	
	var button = form.addSubmitButton('Display');

	nlapiLogExecution('ERROR', '   showBulkFulfillmentOrderCriteria', 'END');

	response.writePage(form);
}

/**
 * 
 * @param request
 * @param response
 * @param form
 */
function displayPossibleFulfillmentOrders(request, response, form){
	nlapiLogExecution('ERROR', '   displayPossibleFulfillmentOrders', 'Start');

	var salesOrderNo = request.getParameter('custpage_so');
	var customer = request.getParameter('custpage_customer');
	var priority = request.getParameter('custpage_priority');
	var orderType = request.getParameter('custpage_ordertype');
	var carrier = request.getParameter('custpage_carrier');
	var company =  request.getParameter('custpage_company');
	var route = request.getParameter('custpage_routeno');
	var location = request.getParameter('custpage_location');
	
	nlapiLogExecution('ERROR', '   salesOrderNo', salesOrderNo);
	nlapiLogExecution('ERROR', '   customer', customer);
	nlapiLogExecution('ERROR', '   priority', priority);
	nlapiLogExecution('ERROR', '   orderType', orderType);
	nlapiLogExecution('ERROR', '   carrier', carrier);
	nlapiLogExecution('ERROR', '   company', company);
	nlapiLogExecution('ERROR', '   route', route);
	nlapiLogExecution('ERROR', '   location', location);

	// Creating a sublist and adding fields to the sublist
	addFieldsToSublist(form);

	var button = form.addSubmitButton('Generate Fulfillment Order(s)');

	// Get ordered to fulfill based on search criteria defined
	var salesOrderList = getOrdersToFulfillByCriteria(salesOrderNo, customer, priority,
			orderType, carrier, company, route, location);
	nlapiLogExecution('ERROR', '   getOrdersToFulfillByCriteria - New Fulfillment Order Count',
			salesOrderList.length);
	
	var ordersFound = false;

	if(salesOrderList != null && salesOrderList.length > 0){
		index = 1;	// Used for indexing the sublist on the fulfillment order screen
		
		for (var i = 0; i < salesOrderList.length; i++){					    	  
			var currentOrder = salesOrderList[i];

			var fulfillOrderId = currentOrder[0];				// Fulfillment Order Id
			var orderId = currentOrder[1];						// Parent Order Id
			var lineNo = currentOrder[2];						// Order Line No.
			var item = currentOrder[3];							// Item / SKU
			var fulfillOrderQty = currentOrder[4];				// Fulfillment Order Qty
			var customer = currentOrder[5];						// Customer
			var carrier = currentOrder[6];						// Carrier
			var priority = currentOrder[7];						// Priority
			var orderType = currentOrder[8];					// Order Type
			var company = currentOrder[9];						// Company
			var route = currentOrder[10];						// Route
			var location = currentOrder[11];					// Location
			var itemStatus = currentOrder[12];					// Item Status
			var serialNos = currentOrder[13];					// Serial Nos.
			var packCode = currentOrder[14];					// Pack Code
			var baseUOM = currentOrder[15];						// Base UOM
			var orderLineQty = currentOrder[16];				// Order Line Qty
			var committedQty = currentOrder[17];				// Committed Qty
			var itemId = currentOrder[18];						// Item Id
			var customerId = currentOrder[19];					// Customer Id
			var itemStatusValue = currentOrder[20];				// Item Status Value

			form.getSubList('custpage_items').setLineItemValue('custpage_bulkorder_salesorder', index, orderId);
			form.getSubList('custpage_items').setLineItemValue('custpage_bulkorder_fulfillorder', index, fulfillOrderId);
			form.getSubList('custpage_items').setLineItemValue('custpage_bulkorder_lineno', index, lineNo);
			form.getSubList('custpage_items').setLineItemValue('custpage_bulkorder_customer', index, customer);
			form.getSubList('custpage_items').setLineItemValue('custpage_bulkorder_customer_id', index, customerId);
			form.getSubList('custpage_items').setLineItemValue('custpage_bulkorder_orderpriority', index, priority);
			form.getSubList('custpage_items').setLineItemValue('custpage_bulkorder_ordertype', index, orderType);
			form.getSubList('custpage_items').setLineItemValue('custpage_bulkorder_carrier', index, carrier);
			form.getSubList('custpage_items').setLineItemValue('custpage_bulkorder_item', index, item);
			form.getSubList('custpage_items').setLineItemValue('custpage_bulkorder_item_id', index, itemId);
			form.getSubList('custpage_items').setLineItemValue('custpage_bulkorder_itemstatus', index, itemStatus);			    	  
			form.getSubList('custpage_items').setLineItemValue('custpage_bulkorder_lotbatch', index, "");
			form.getSubList('custpage_items').setLineItemValue('custpage_bulkorder_packcode', index, packCode);
			form.getSubList('custpage_items').setLineItemValue('custpage_bulkorder_baseuom', index, baseUOM);
			form.getSubList('custpage_items').setLineItemValue('custpage_bulkorder_orderqty', index, fulfillOrderQty.toString());
			form.getSubList('custpage_items').setLineItemValue('custpage_bulkorder_customervalue', index, customer);
			form.getSubList('custpage_items').setLineItemValue('custpage_bulkorder_itemvalue', index, "");
			form.getSubList('custpage_items').setLineItemValue('custpage_bulkorder_itemstatusvalue', index, itemStatusValue);
			form.getSubList('custpage_items').setLineItemValue('custpage_bulkorder_fulfillmentqty', index, fulfillOrderQty.toString());
			form.getSubList('custpage_items').setLineItemValue('custpage_bulkorder_wmslocation', index, location);
				
			// Increment the index on the fulfillment order sublist
			index++;
			ordersFound = true;
		} // end of for loop
	}  //end of search condition

	if(!ordersFound){
		nlapiLogExecution('ERROR', 'No fulfillment orders to create', '0');
		showInlineMessage(form, 'Confirmation', 'No pending fulfillment orders', null);
	}
	
	nlapiLogExecution('ERROR', '   displayPossibleFulfillmentOrders', 'End');
}

/**
 * 
 * @param request
 * @param response
 * @param form
 */
function processSelectedFulfillmentOrders(request, response, form){
	nlapiLogExecution('ERROR', '   processSelectedFulfillmentOrders', 'Start');
	
	var route = request.getParameter('custpage_routeno');
	var so = request.getParameter('custpage_so');
	
	nlapiLogExecution('ERROR', '   route', route);
	nlapiLogExecution('ERROR', '   so', so);

	// data will be inserted into custom record customrecord_ebiznet_ordline
	var lineCnt = request.getLineItemCount('custpage_items');
	nlapiLogExecution('ERROR', '   lineCnt', lineCnt);
	
	// Process all the selected fulfillment orders
	for (var s = 1; s <= lineCnt; s++){
		var isSelected = request.getLineItemValue('custpage_items', 'custpage_bulkorder_sel', s);
		
		// Check if the fulfillment order is selected for generation
		if(isSelected == 'T'){
			var tranId = request.getLineItemValue('custpage_items', 'custpage_bulkorder_salesorder', s);
			var fulfillOrderId = request.getLineItemValue('custpage_items', 'custpage_bulkorder_fulfillorder', s);
			var lineNo = request.getLineItemValue('custpage_items', 'custpage_bulkorder_lineno', s);
			var customer = request.getLineItemValue('custpage_items', 'custpage_bulkorder_customer', s);
			var customerId = request.getLineItemValue('custpage_items', 'custpage_bulkorder_customer_id', s);
			var prority = request.getLineItemValue('custpage_items', 'custpage_bulkorder_orderpriority', s);
			var orderType = request.getLineItemValue('custpage_items', 'custpage_bulkorder_ordertype', s);
			var carrier = request.getLineItemValue('custpage_items', 'custpage_bulkorder_carrier', s);
			var item = request.getLineItemValue('custpage_items', 'custpage_bulkorder_item', s);
			var itemId = request.getLineItemValue('custpage_items', 'custpage_bulkorder_item_id', s);
			var itemStatus = request.getLineItemValue('custpage_items', 'custpage_bulkorder_itemstatus', s);
			var lotBatch = request.getLineItemValue('custpage_items', 'custpage_bulkorder_lotbatch', s);
			var packCode = request.getLineItemValue('custpage_items', 'custpage_bulkorder_packcode', s);
			var baseUOM = request.getLineItemValue('custpage_items', 'custpage_bulkorder_baseuom', s);
			var quantity = request.getLineItemValue('custpage_items', 'custpage_bulkorder_orderqty', s);
			var customerValue = request.getLineItemValue('custpage_items', 'custpage_bulkorder_customervalue', s);
			var itemValue = request.getLineItemValue('custpage_items', 'custpage_bulkorder_itemvalue', s);
			var itemStatusValue = request.getLineItemValue('custpage_items', 'custpage_bulkorder_itemstatusvalue', s);
			var fulfillmentQty = request.getLineItemValue('custpage_items', 'custpage_bulkorder_fulfillmentqty', s);
			var location = request.getLineItemValue('custpage_items', 'custpage_bulkorder_wmslocation', s);
			
			nlapiLogExecution('ERROR', '   tranId', tranId);
			nlapiLogExecution('ERROR', '   fulfillOrderId', fulfillOrderId);
			nlapiLogExecution('ERROR', '   lineNo', lineNo);
			nlapiLogExecution('ERROR', '   item', item);
			nlapiLogExecution('ERROR', '   quantity', quantity);
			nlapiLogExecution('ERROR', '   fulfillmentQty', fulfillmentQty);
			
			//Creating a record in orderline fulfillment custome record.
			// Check if entered fulfillment quantity is valid
			if(validateFulfillmentQty(fulfillmentQty)){
				// Creating a new order line record for the current fulfillment order
				var orderLineRecord = nlapiCreateRecord('customrecord_ebiznet_ordline');
				
				orderLineRecord.setFieldValue('name', parseFloat(tranId));
				orderLineRecord.setFieldValue('custrecord_ns_ord', parseFloat(tranId));
				orderLineRecord.setFieldValue('custrecord_ordline', lineNo);
				orderLineRecord.setFieldValue('custrecord_lineord', fulfillOrderId);
				orderLineRecord.setFieldValue('custrecord_ebiz_linesku', itemId);
				orderLineRecord.setFieldValue('custrecord_ord_qty', parseFloat(fulfillmentQty).toFixed(5));

				orderLineRecord.setFieldValue('custrecord_do_order_priority', prority);
				orderLineRecord.setFieldValue('custrecord_do_order_type', orderType);
				orderLineRecord.setFieldValue('custrecord_do_customer', customerId);
//				orderLineRecord.setFieldValue('custrecord_do_carrier', carrier);
				//OrderlineRecord.setFieldValue('custrecord_linestatus_flag', 5);
				orderLineRecord.setFieldValue('custrecord_linestatus_flag', 25);
				orderLineRecord.setFieldValue('custrecord_ordline_wms_location', location);


				orderLineRecord.setFieldValue('custrecord_linepackcode', packCode);
				orderLineRecord.setFieldValue('custrecord_lineuom_id', 1);
				orderLineRecord.setFieldValue('custrecord_linesku_status', itemStatusValue);
				orderLineRecord.setFieldText('custrecord_linesku_status', itemStatus);
				orderLineRecord.setFieldValue('custrecord_batch', lotBatch);
				//OrderlineRecord.setFieldValue('custrecord_ordline_company',customervalue);
				orderLineRecord.setFieldValue('custrecord_route_no',route);
				
				// Submit the custom record for persistence
				var rcptrecordid = nlapiSubmitRecord(orderLineRecord);
//				response.sendRedirect('SUITELET', 'customscript_waveconfirm', 'customdeploy_waveconfirm', false);
			}
		}
	} // end of the for loop inside else part

	nlapiLogExecution('ERROR', '   processSelectedFulfillmentOrders', 'End');

	showInlineMessage(form, 'Confirmation', 'Selected fulfillment orders created successfully', null);
}

/**
 * @author Ramana
 *@version
 *@date
 */
function BulkOrderFulFillment(request, response){
	if (request.getMethod() == 'GET') {
		nlapiLogExecution('ERROR', 'Bulk Fulfillment Order - Query Criteria', 'Start');
		
		// Call method to render the bulk fulfillment order criteria
		showBulkFulfillmentOrderCriteria(request, response);
		nlapiLogExecution('ERROR', 'Bulk Fulfillment Order - Query Criteria', 'End');
	} else {
		var inProcessStatus = request.getParameter('custpage_data');

		// Check if to render fulfillment order candidates or generate fulfillment orders
		if(inProcessStatus != null){
			nlapiLogExecution('ERROR', 'Bulk Fulfillment Order - Candidate Orders', 'Start');
			var form = nlapiCreateForm('Bulk Order Fulfillment Screen');
			// Call method to display the selected fulfillment order candidates
			displayPossibleFulfillmentOrders(request, response, form);
			nlapiLogExecution('ERROR', 'Bulk Fulfillment Order - Candidate Orders', 'End');
		} else {
			nlapiLogExecution('ERROR', 'Generating selected fulfillment orders', 'Start');
			var form = nlapiCreateForm('Fulfillment Order(s)');
			processSelectedFulfillmentOrders(request, response, form);
			nlapiLogExecution('ERROR', 'Generating selected fulfillment orders', 'End');
		}
		
		response.writePage(form);		  	  		  
	}
}

