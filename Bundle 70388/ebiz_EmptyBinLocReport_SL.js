/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Suitelet/Attic/ebiz_EmptyBinLocReport_SL.js,v $
 *     	   $Revision: 1.1.2.1.4.2.4.3 $
 *     	   $Date: 2014/06/12 14:49:08 $
 *     	   $Author: grao $
 *     	   $Name: t_NSWMS_2014_1_3_125 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_EmptyBinLocReport_SL.js,v $
 * Revision 1.1.2.1.4.2.4.3  2014/06/12 14:49:08  grao
 * Case#: 20148791  New GUI account issue fixes
 *
 * Revision 1.1.2.1.4.2.4.2  2014/05/12 14:02:43  skreddy
 * case # 20148357
 * nucourse prod issue fix
 *
 * Revision 1.1.2.1.4.2.4.1  2013/02/26 13:02:23  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Marged from Boombah.
 *
 * Revision 1.1.2.1  2012/08/03 13:19:33  schepuri
 * CASE201112/CR201113/LOG201121
 * new report
 *
 *
 *****************************************************************************/

var tempEmptyBinLocResultsArray=new Array();
var tempInvtResultsArray=new Array();

function getEmptyBinLocSearchResults(maxno,invtsearchresults,vBinlocGroup)
{
	nlapiLogExecution('Error', 'Into getEmptyBinLocSearchResults');
	nlapiLogExecution('Error', 'maxno',maxno);
	nlapiLogExecution('Error', 'invtsearchresults',invtsearchresults);
	nlapiLogExecution('Error', 'vBinlocGroup',vBinlocGroup);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('internalid',null,'noneof',invtsearchresults));
	filters.push(new nlobjSearchFilter('custrecord_ebizlocationtype',null,'noneof',8));
	if(vBinlocGroup!=null && vBinlocGroup!='')
		filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid',null,'anyof',vBinlocGroup));// added by santosh

	if(maxno!=-1)
	{
		filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));
	}


	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_inboundlocgroupid');
	column[1] = new nlobjSearchColumn('id');
	column[1].setSort();   
	column[2] = new nlobjSearchColumn('name');
	column[3] = new nlobjSearchColumn('custrecord_ebizlocationtype');
	column[4] = new nlobjSearchColumn('custrecord_cube');
	column[5] = new nlobjSearchColumn('custrecord_remainingcube');
	column[6] = new nlobjSearchColumn('custrecord_height');
	column[7] = new nlobjSearchColumn('custrecord_width');
	column[8] = new nlobjSearchColumn('custrecord_depth');
	column[9] = new nlobjSearchColumn('custrecordweight');
	column[2].setSort();  

	var Binsearchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, column);
	if(Binsearchresults!=null)
	{
		if(Binsearchresults.length>=1000)
		{
			var maxno1=Binsearchresults[Binsearchresults.length-1].getValue(column[1]);
			nlapiLogExecution('Error', 'maxno1',maxno1);
			tempEmptyBinLocResultsArray.push(Binsearchresults);
			getEmptyBinLocSearchResults(maxno1,invtsearchresults,vBinlocGroup);
		}
		else
		{
			tempEmptyBinLocResultsArray.push(Binsearchresults);
		}
	}

	nlapiLogExecution('Error', 'Out of getEmptyBinLocSearchResults');
	return tempEmptyBinLocResultsArray;
}

function getInventorySearchResults(maxno,vBinlocGroup)
{
	nlapiLogExecution('Error', 'Into getInventorySearchResults');
	nlapiLogExecution('Error', 'maxno',maxno);
	nlapiLogExecution('Error', 'vBinlocGroup',vBinlocGroup);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [18,19]));	
	if(vBinlocGroup!=null && vBinlocGroup!='')
		filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'anyof', vBinlocGroup));	
	filters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0));
	if(maxno!=-1)
	{
		filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));
	}

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	column[1] = new nlobjSearchColumn('internalid');
	column[1].setSort(false);
	var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, column);
	if(invtsearchresults!=null)
	{
		if(invtsearchresults.length>=1000)
		{
			nlapiLogExecution('Error', 'invtsearchresults',invtsearchresults.length);

			var maxno1=invtsearchresults[invtsearchresults.length-1].getId();
			for(var j=0;j<invtsearchresults.length;j++)
			{
				if(tempInvtResultsArray.indexOf(invtsearchresults[j].getValue('custrecord_ebiz_inv_binloc'))==-1)
					tempInvtResultsArray.push(invtsearchresults[j].getValue('custrecord_ebiz_inv_binloc'));
			}
			getInventorySearchResults(maxno1,vBinlocGroup);
		}
		else
		{
			for(var j=0;j<invtsearchresults.length;j++)
			{
				if(tempInvtResultsArray.indexOf(invtsearchresults[j].getValue('custrecord_ebiz_inv_binloc'))==-1)
					tempInvtResultsArray.push(invtsearchresults[j].getValue('custrecord_ebiz_inv_binloc'));
			}
		}
	}

	nlapiLogExecution('Error', 'Out of getInventorySearchResults');
	return tempInvtResultsArray;
}

function setPagingForSublist(orderList,form,request)
{
	nlapiLogExecution('Error', 'Into setPagingForSublist');
	if(orderList != null && orderList.length > 0){
		nlapiLogExecution('Error', 'orderList ', orderList.length);
		var orderListArray=new Array();		
		for(k=0;k<orderList.length;k++)
		{
			var ordsearchresult = orderList[k];

			if(ordsearchresult!=null)
			{
				for(var j=0;j<ordsearchresult.length;j++)
				{
					orderListArray[orderListArray.length]=ordsearchresult[j];				
				}
			}
		}
		nlapiLogExecution('ERROR', 'orderListArray.length ', orderListArray.length); 
		var test='';

		if(orderListArray.length>0)
		{

			if(orderListArray.length>50)
			{
				var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('hidden');
				pagesize.setDisplaySize(10,10);
				pagesize.setLayoutType('outsidebelow', 'startrow');
				var select= form.addField('custpage_selectpage','select', 'Select Records');	
				select.setLayoutType('outsidebelow', 'startrow');			
				select.setDisplaySize(200,30);
				if (request.getMethod() == 'GET'){
					pagesize.setDefaultValue("50");
					pagesizevalue=50;
				}
				else
				{
					if(request.getParameter('custpage_pagesize')!=null)
					{
						pagesizevalue= request.getParameter('custpage_pagesize');
					}
					else
					{
						pagesizevalue= 50;
						pagesize.setDefaultValue("50");
					}
				}

				//this is to add the pageno's to the dropdown.
				var len=orderListArray.length/parseInt(pagesizevalue);
				for(var k=1;k<=Math.ceil(len);k++)
				{

					var from;var to;

					to=parseInt(k)*parseInt(pagesizevalue);
					from=(parseInt(to)-parseInt(pagesizevalue))+1;

					if(parseInt(to)>orderListArray.length)
					{
						to=orderListArray.length;
						test=from.toString()+","+to.toString(); 
					}

					var temp=from.toString()+" to "+to.toString();
					var tempto=from.toString()+","+to.toString();
					select.addSelectOption(tempto,temp);

				} 


				nlapiLogExecution('Error', 'request.getMethod()',request.getMethod());

				if (request.getMethod() == 'POST'){

					if(request.getParameter('custpage_selectpage')!=null ){

						select.setDefaultValue(request.getParameter('custpage_selectpage'));	

					}
					if(request.getParameter('custpage_pagesize')!=null ){

						pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	

					}
				}
			}
			else
			{
				pagesizevalue=parseInt(orderListArray.length);
			}
			var minval=0;var maxval=parseInt(pagesizevalue);
			if(parseInt(pagesizevalue)>orderListArray.length)
			{
				maxval=orderListArray.length;
			}
			var selectno=request.getParameter('custpage_selectpage');
			if(selectno!=null )
			{
				var selectedPage= request.getParameter('custpage_selectpage');
				var selectedPageArray=selectedPage.split(',');			
				var diff=parseInt(selectedPageArray[1])-(parseInt(selectedPageArray[0])-1);

				var pagevalue=request.getParameter('custpage_pagesize');
				if(pagevalue!=null)
				{

					if(parseInt(diff)==parseInt(pagevalue)|| test==selectno)
					{
						var selectedPageArray=selectno.split(',');	
						minval=parseInt(selectedPageArray[0])-1;
						maxval=parseInt(selectedPageArray[1]);
					}
				}
			}


			var c=0;
			var minvalue;
			minvalue=minval;
			var index=1;
			for(var j = minvalue; j < maxval; j++){		

				var currentOrder = orderListArray[j];
				form.getSubList('custpage_results').setLineItemValue('custpage_slno', index,
						(parseInt(j)+1).toString());
				form.getSubList('custpage_results').setLineItemValue('custpage_binloc', index,
						currentOrder.getValue('name'));
				form.getSubList('custpage_results').setLineItemValue('custpage_locgrpid', index,
						currentOrder.getText('custrecord_inboundlocgroupid'));
				form.getSubList('custpage_results').setLineItemValue('custpage_loctype', index,
						currentOrder.getText('custrecord_ebizlocationtype'));
				form.getSubList('custpage_results').setLineItemValue('custpage_cube', index,
						currentOrder.getValue('custrecord_cube'));
				form.getSubList('custpage_results').setLineItemValue('custpage_remcube', index,
						currentOrder.getValue('custrecord_remainingcube'));
				form.getSubList('custpage_results').setLineItemValue('custpage_height', index,
						currentOrder.getValue('custrecord_height'));
				form.getSubList('custpage_results').setLineItemValue('custpage_width', index,
						currentOrder.getValue('custrecord_width'));
				form.getSubList('custpage_results').setLineItemValue('custpage_depth', index,
						currentOrder.getValue('custrecord_depth'));
				form.getSubList('custpage_results').setLineItemValue('custpage_weight', index,
						currentOrder.getValue('custrecordweight'));

				index=index+1;
			}
		}
	}

	nlapiLogExecution('Error', 'Out of setPagingForSublist');
}


function ebiznet_EmptyBinLocation_SL(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Empty Bin Location');     
		form.setScript('customscript_ebiz_emptyloc_cl');

		var binLocnGrpField = form.addField('custpage_binlocgroup', 'select', 'Bin Location Group','customrecord_ebiznet_loc_group');//.setMandatory(true);
		form.addSubmitButton('Display');

		var excelid="1";
		response.writePage(form);
	}
	else{
		var form = nlapiCreateForm('Empty Bin Location2'); 
		nlapiLogExecution('Error', 'in response');
		var form = nlapiCreateForm('Empty Bin Location');     
		form.setScript('customscript_ebiz_emptyloc_cl');

		var vBinlocGroup=request.getParameter('custpage_binlocgroup');
		var binLocnGrpField = form.addField('custpage_binlocgroup', 'select', 'Bin Location Group','customrecord_ebiznet_loc_group');//.setMandatory(true);
		binLocnGrpField .setDefaultValue(vBinlocGroup);

		form.addSubmitButton('Display');


		var sublist = form.addSubList("custpage_results", "list", "Item List");
		sublist.addField("custpage_slno", "text", "Sl No");
		sublist.addField("custpage_binloc", "text", "Bin Location");
		sublist.addField("custpage_locgrpid", "text", "Location Group");
		sublist.addField("custpage_loctype", "text", "Location Type");
		sublist.addField("custpage_cube", "text", "Cube");
		sublist.addField("custpage_remcube", "text", "Remaining Cube");
		sublist.addField("custpage_height", "text", "Height (in mts)");
		sublist.addField("custpage_width", "text", "Width (in mts)");
		sublist.addField("custpage_depth", "text", "Depth (in mts)");
		sublist.addField("custpage_weight", "text", "Weight (in mts)");

		var invtsearchresults =  getInventorySearchResults(-1,vBinlocGroup);
		if(invtsearchresults != null && invtsearchresults != '')
			nlapiLogExecution('Error', 'invtsearchresults.length',invtsearchresults.length);

		if(invtsearchresults != null && invtsearchresults != ''){
			nlapiLogExecution('Error', 'invtsearchresults.length',invtsearchresults.length);
			var binlocsearchresult = getEmptyBinLocSearchResults(-1,invtsearchresults,vBinlocGroup);
			var excelid="1";
			form.addButton('custpage_excel','ExportToExcel','Printreport('+excelid+')');
		}
		if(binlocsearchresult != null && binlocsearchresult.length > 0){
			nlapiLogExecution('Error', 'binlocsearchresult.length',binlocsearchresult.length);
			setPagingForSublist(binlocsearchresult,form,request);
		}
		response.writePage(form);
	}

}
