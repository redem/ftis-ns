/***************************************************************************
eBizNET Solutions
 ****************************************************************************
 *	
 *  $Source: /cvs/products/NSWMS/EoN/WMS_UI/Common/Suitelet/eBizDataFetchLibrary.js,v $
 *  $Revision: 1.2.2.13.8.10.2.1 $
 *  $Date: 2015/11/06 15:52:30 $
 *  $Author: nneelam $
 *  $Name: t_WMS_2015_2_StdBundle_1_87 $
 *
 * DESCRIPTION
 *  Functionality
 *
 * REVISION HISTORY
 *  $Log: eBizDataFetchLibrary.js,v $
 *  Revision 1.2.2.13.8.10.2.1  2015/11/06 15:52:30  nneelam
 *  case# 201415438
 *
 *  Revision 1.2.2.13.8.10  2014/11/27 14:00:23  sponnaganti
 *  case# 201411072
 *  One Industries Sb issue fix
 *
 *  Revision 1.2.2.13.8.9  2014/07/10 09:47:12  sponnaganti
 *  Case# 20149365
 *  Compatibility issue fix
 *
 *  Revision 1.2.2.13.8.8  2014/05/23 06:49:42  gkalla
 *  case#20148480
 *  Sonic SB issue. Not checkin inactive flag while scanning UPC code
 *
 *  Revision 1.2.2.13.8.7  2014/04/24 14:04:41  snimmakayala
 *  Case #: 20148157
 *
 *  Revision 1.2.2.13.8.6  2014/02/10 14:53:08  sponnaganti
 *  case# 20127101
 *  (@NONE@ value added to the RoleLocation)
 *
 *  Revision 1.2.2.13.8.5  2014/01/07 06:43:44  grao
 *  Case# 20126606 related issue fixes in Sb issue fixes
 *
 *  Revision 1.2.2.13.8.4  2013/12/02 07:17:32  snimmakayala
 *  CASE#:20125764
 *  MHP: PO2TO Conversion.
 *
 *  Revision 1.2.2.13.8.3  2013/06/03 07:47:37  gkalla
 *  CASE201112/CR201113/LOG201121
 *  For TSG  added role based restriction
 *
 *  Revision 1.2.2.13.8.2  2013/05/06 15:51:17  skreddy
 *  CASE201112/CR201113/LOG201121
 *  scanning serial no for Item
 *
 *  Revision 1.2.2.13.8.1  2013/03/15 09:28:43  snimmakayala
 *  CASE201112/CR201113/LOG2012392
 *  Production and UAT issue fixes.
 *
 *  Revision 1.2.2.13  2012/05/28 15:34:20  spendyala
 *  CASE201112/CR201113/LOG201121
 *  issue related to passing location to filter criteria when it is not null or empty.
 *
 *  Revision 1.2.2.12  2012/05/25 22:25:26  snimmakayala
 *  CASE201112/CR201113/LOG201121
 *  Production issue fixes
 *
 *  Revision 1.2.2.11  2012/05/16 06:02:54  snimmakayala
 *  CASE201112/CR201113/LOG201121
 *  Monobind UAT issue fixes
 *
 *  Revision 1.2.2.10  2012/05/03 14:35:54  rrpulicherla
 *  CASE201112/CR201113/LOG201121
 *
 *  RF Checkin changes
 *
 *  Revision 1.2.2.9  2012/04/30 22:36:43  snimmakayala
 *  CASE201112/CR201113/LOG201121
 *  Production issue fixes
 *
 *  Revision 1.2.2.8  2012/04/30 10:30:42  spendyala
 *  CASE201112/CR201113/LOG201121
 *  While Searching of Item in ItemMaster,
 *  'name' filter is changed to 'nameinternal' and also checking weather the item is inactive state or not.
 *
 *  Revision 1.2.2.7  2012/04/27 13:11:51  rrpulicherla
 *  CASE201112/CR201113/LOG201121
 *
 *  RF Invaid SKU
 *
 *  Revision 1.2.2.6  2012/04/25 21:22:02  rrpulicherla
 *  CASE201112/CR201113/LOG201121
 *
 *  RF cart Changes
 *
 *  Revision 1.2.2.5  2012/04/13 23:12:46  snimmakayala
 *  CASE201112/CR201113/LOG201121
 *  RF CART PUTAWAY issues.
 *
 *  Revision 1.2.2.4  2012/04/10 22:44:59  snimmakayala
 *  CASE201112/CR201113/LOG201121
 *  RF CART CHECKIN ITEM issue
 *
 *  Revision 1.5  2012/04/10 22:32:47  snimmakayala
 *  CASE201112/CR201113/LOG201121
 *  Production issue fixes
 *
 *  Revision 1.4  2012/02/28 01:24:15  snimmakayala
 *  CASE201112/CR201113/LOG201121
 *  RF Checkin issue fixes
 *
 *  Revision 1.3  2012/02/14 07:15:06  spendyala
 *  CASE201112/CR201113/LOG201121
 *  Merged upto 1.2.2.1.
 *
 *  Revision 1.2  2011/12/22 11:37:57  rgore
 *  CASE201112/CR201113/LOG201121
 *  Modularized code to call method from data access library.
 *  - Ratnakar
 *  22 Dec 2011
 *
 *  Revision 1.1  2011/12/12 09:06:03  rgore
 *  CASE201112/CR201113/LOG201121
 *  New suitelet defined to house all functions related to data access from custom records
 *  - Ratnakar
 *  12 Dec 2011
 *
 *
 ****************************************************************************/

/**
 * Function to retrieve the PO for the specified tranID.  This should return only 1 row.
 *  
 * @param tranID
 * @returns PO Search Results
 */
function eBiz_RF_GetPOListForTranID(tranID,trantype){
	logTime('eBizGetPOListForTranID', 'Start');

	var filters = new Array();
	filters.push(new nlobjSearchFilter ('tranid', null, 'is', tranID));
	filters.push(new nlobjSearchFilter ('mainline', null, 'is', 'T'));

	var columns = new Array();
	columns.push(new nlobjSearchColumn('location'));
	columns.push(new nlobjSearchColumn('custbody_nswms_company'));
	if(trantype!=null && trantype!='')
	{
		if(trantype=='transferorder')
			columns.push(new nlobjSearchColumn('transferlocation'));
	}


	var poSearchResults = nlapiSearchRecord(trantype, null, filters, columns);

	logCountMessage('DEBUG', poSearchResults);
	logTime('eBizGetPOListForTranID', 'End');
	return poSearchResults;
}

/**
 * 
 * @param itemNo
 * @returns {String}
 */
function eBiz_RF_GetItemForItemNo(itemNo,location){
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemForItemNo', 'Start');
	nlapiLogExecution('DEBUG', 'Input Item No', itemNo);

	var currItem = "";

	var sitemno = "";
	var context = nlapiGetContext();
	var userAccountId = context.getCompany();
	if(userAccountId=='1285441')
	{
		sitemno = checkserialritem(itemNo);
	}
	if(sitemno != "")
	{
		itemNo = sitemno;
	}	

	var filters = new Array();
	filters.push(new nlobjSearchFilter('nameinternal', null, 'is', itemNo));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(location!=null && location!='' && location!='null')
		filters.push(new nlobjSearchFilter('location', null, 'anyof', ['@NONE@',location]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('itemid');
	columns[0].setSort(true);


	var itemSearchResults = nlapiSearchRecord('item', null, filters, columns);

	if(itemSearchResults != null)
		currItem = itemSearchResults[0].getValue('itemid');

	var logMsg = 'Item = ' + currItem;
	nlapiLogExecution('DEBUG', 'Item Retrieved', logMsg);
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemForItemNo', 'End');

	return currItem;
}

/**
 * 
 * @param itemNo
 * @returns {String}
 */
function eBiz_RF_GetItemBasedOnUPCCode(itemNo,location){
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemBasedOnUPCCode', 'Start');

	var currItem = "";

	var filters = new Array();
	filters.push(new nlobjSearchFilter('upccode', null, 'is', itemNo));
	if(location!=null && location!='' && location!='null')
		filters.push(new nlobjSearchFilter('location', null, 'anyof', ['@NONE@',location]));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
//	var columns = new Array();
	//columns.push(new nlobjSearchColumn('itemid'));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('itemid');
	columns[0].setSort(true);

	var itemSearchResults = nlapiSearchRecord('item', null, filters, columns);

	if(itemSearchResults != null)
		currItem = itemSearchResults[0].getValue('itemid');

	var logMsg = 'Item = ' + currItem;
	nlapiLogExecution('DEBUG', 'Item Retrieved', logMsg);
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemBasedOnUPCCode', 'End');

	return currItem;
}

/**
 * 
 * @param itemNo
 * @param location
 * @returns {String}
 */
function eBiz_RF_GetItemFromSKUDims(itemNo, location,vendor){
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemFromSKUDims', 'Start');
	nlapiLogExecution('DEBUG', 'vendor', vendor);
	var currItem = "";

	var filters = new Array();
	filters.push(new nlobjSearchFilter('name', null, 'is', itemNo));
	if(location!=null && location!='' && location!='null')
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiz_location', null, 'anyof', ['@NONE@',location]));
	}
	if(vendor!=null && vendor!='')
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiz_vendor', null, 'anyof', ['@NONE@',vendor]));
	}

	var columns = new Array();
	columns.push(new nlobjSearchColumn('custrecord_ebiz_item'));

	var itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, filters, columns);

	if(itemSearchResults != null)
		currItem = itemSearchResults[0].getText('custrecord_ebiz_item');

	var logMsg = 'Item = ' + currItem;
	nlapiLogExecution('DEBUG', 'Item Retrieved', logMsg);
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemFromSKUDims', 'End');
	return currItem;
}

function eBiz_RF_GetItemForItemId(itemId,location){
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemForItemId', 'Start');

	var itemRecord = null;

	if(itemId != null && itemId != ""){
		var logMsg = 'ItemId = |' + itemId + '|';
		nlapiLogExecution('DEBUG', 'Input Parameters', logMsg);
		nlapiLogExecution('DEBUG', 'location', location);

		var filters = new Array();
		filters.push(new nlobjSearchFilter('nameinternal', null, 'is', itemId));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		if(location!=null && location!='' && location!='null')
			filters.push(new nlobjSearchFilter('location', null, 'anyof', ['@NONE@',location]));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('itemid');
		columns[0].setSort(true);

		var itemSearchResults = nlapiSearchRecord('item', null, filters, columns);
		if(itemSearchResults != null && itemSearchResults.length > 0){
			var itemInternalId = itemSearchResults[0].getId();
			nlapiLogExecution('DEBUG', 'Internal Id', itemInternalId);
			var itemcolumns = nlapiLookupField('item', itemInternalId, [ 'recordType']);
			var itemType = itemcolumns.recordType;

			nlapiLogExecution('DEBUG', 'itemType', itemType);

			itemRecord = nlapiLoadRecord(itemType, itemInternalId);
		}
	}

	nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemForItemId', 'End');
	return itemRecord;
}

function eBiz_RF_GetPOLineDetailsForItem(poID, itemID,trantype){
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetPOLineDetailsForItem', 'Start');
	var logMsg = 'PO ID = ' + poID + '<br>';
	logMsg = logMsg + 'Item ID = ' + itemID;
	nlapiLogExecution('DEBUG', 'Input Parameters', logMsg);

	if(trantype=='' || trantype==null || trantype=='null' || trantype=='undefined')
		trantype='purchaseorder';

	var RoleLocation=getRoledBasedLocation();
	nlapiLogExecution('DEBUG', 'RoleLocation', RoleLocation);
	var filters = new Array();
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('tranid', null, 'is', poID));
	filters.push(new nlobjSearchFilter('item', null, 'is', itemID));
	filters.push(new nlobjSearchFilter('closed', null, 'is', 'F'));
	if(RoleLocation != null && RoleLocation != '' && RoleLocation != 0)
	{
		//case# 20127101 starts (@NONE@ value added to the RoleLocation)
		RoleLocation.push('@NONE@');
		//case# 20127101 end
		filters.push(new nlobjSearchFilter('location', null, 'anyof', RoleLocation));
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('item');
	columns[1] = new nlobjSearchColumn('line');
	columns[2] = new nlobjSearchColumn('custcol_ebiz_po_cube');
	columns[3] = new nlobjSearchColumn('location');
	columns[4] = new nlobjSearchColumn('quantity');

	var poLineSearchResults = nlapiSearchRecord(trantype, null, filters, columns);

	if(poLineSearchResults != null && poLineSearchResults.length > 0)
		nlapiLogExecution('DEBUG', 'No. of PO Lines Retrieved', poLineSearchResults.length);

	nlapiLogExecution('DEBUG', 'eBiz_RF_GetPOLineDetailsForItem', 'End');

	return poLineSearchResults;
}

function eBiz_RF_GetTOLineDetailsForItem(poID, itemID,trantype){
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetTOLineDetailsForItem', 'Start');
	var logMsg = 'PO ID = ' + poID + '<br>';
	logMsg = logMsg + 'Item ID = ' + itemID;
	nlapiLogExecution('DEBUG', 'Input Parameters', logMsg);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('tranid', null, 'is', poID));
	filters.push(new nlobjSearchFilter('item', null, 'is', itemID));
	filters.push(new nlobjSearchFilter('transactionlinetype', null, 'anyof', 'ITEM'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('item');
	columns[1] = new nlobjSearchColumn('line');
	columns[2] = new nlobjSearchColumn('custcol_ebiz_po_cube');
	columns[3] = new nlobjSearchColumn('location');
	columns[4] = new nlobjSearchColumn('transferlocation');
	columns[5] = new nlobjSearchColumn('formulanumeric');
	columns[5].setFormula('ABS({quantity})');

	var poLineSearchResults = nlapiSearchRecord(trantype, null, filters, columns);

	if(poLineSearchResults != null && poLineSearchResults.length > 0)
		nlapiLogExecution('DEBUG', 'No. of PO Lines Retrieved', poLineSearchResults.length);

	nlapiLogExecution('DEBUG', 'eBiz_RF_GetTOLineDetailsForItem', 'End');

	return poLineSearchResults;
}

function eBiz_RF_GetTransactionlinedetails(poID, itemID){
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetPOLineDetailsForItem', 'Start');
	var logMsg = 'PO ID = ' + poID + '<br>';
	logMsg = logMsg + 'Item ID = ' + itemID;
	nlapiLogExecution('DEBUG', 'Input Parameters', logMsg);

	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_order_no', null, 'is', poID));
	filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_item', null, 'anyof', [itemID]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_item');
	columns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_orderline_no');
	columns[2] = new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty');


	var transactionLineSearchResults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, filters, columns);

	if(transactionLineSearchResults != null && transactionLineSearchResults.length > 0)
		nlapiLogExecution('DEBUG', 'No. of PO Lines Retrieved', transactionLineSearchResults.length);

	nlapiLogExecution('DEBUG', 'eBiz_RF_GetPOLineDetailsForItem', 'End');

	return transactionLineSearchResults;
}

/**
 * @param itemID
 * @returns {Array}
 */
function eBiz_RF_GetItemCubeForItem(itemID){
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemCubeForItem', 'Start');
	var ItemInfo=new Array();
	var itemCube = 0;
	var BaseUomQty=0;
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemID);
	filters[1] = new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
	columns[1] = new nlobjSearchColumn('custrecord_ebizqty');

	var itemDimSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

	if(itemDimSearchResults != null && itemDimSearchResults.length > 0){
		itemCube = itemDimSearchResults[0].getValue('custrecord_ebizcube');
		//code added on 13 feb 2012 by suman
		//To get the baseuom qty .
		BaseUomQty=itemDimSearchResults[0].getValue('custrecord_ebizqty');
		//end of code as of 13 feb 2012.
	}
	ItemInfo.push(itemCube);
	ItemInfo.push(BaseUomQty);
	nlapiLogExecution('DEBUG', 'Retrieved Item Cube', 'Item Cube = ' + itemCube);
	nlapiLogExecution('DEBUG', 'Retrieved BaseUomQty', 'BaseUomQty = ' + BaseUomQty);
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemCubeForItem', 'End');

	return ItemInfo;
}

function eBiz_RF_GetPOLineDetailsForItemArr(poID, itemIDArr,trantype,vlinelocation){
	nlapiLogExecution('ERROR', 'eBiz_RF_GetPOLineDetailsForItemArr', 'Start');
	var logMsg = 'PO ID = ' + poID + '<br>';
	logMsg = logMsg + 'Item ID = ' + itemIDArr;
	nlapiLogExecution('ERROR', 'Input Parameters', logMsg);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('tranid', null, 'is', poID));
	filters.push(new nlobjSearchFilter('item', null, 'anyof', itemIDArr));
	filters.push(new nlobjSearchFilter('closed', null, 'is', 'F'));
	if(vlinelocation!=null && vlinelocation!='')
	filters.push(new nlobjSearchFilter('custrecord_ebizwhsite', 'location', 'is', 'T'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('item');
	columns[1] = new nlobjSearchColumn('line');
	columns[2] = new nlobjSearchColumn('custcol_ebiz_po_cube');
	columns[3] = new nlobjSearchColumn('location');
	columns[4] = new nlobjSearchColumn('quantity');
	columns[5] = new nlobjSearchColumn('quantityshiprecv');

	var poLineSearchResults = nlapiSearchRecord(trantype, null, filters, columns);

	if(poLineSearchResults != null && poLineSearchResults.length > 0)
		nlapiLogExecution('ERROR', 'No. of PO Lines Retrieved', poLineSearchResults.length);

	nlapiLogExecution('ERROR', 'eBiz_RF_GetPOLineDetailsForItemArr', 'End');

	return poLineSearchResults;
}


function eBiz_RF_GetItemForItemIdWithArr(itemId,location){
	nlapiLogExecution('ERROR', 'eBiz_RF_GetItemForItemIdWithArr', 'Start');

	var itemRecordArr = new Array();

	if(itemId != null && itemId != ""){
		var logMsg = 'ItemId = |' + itemId + '|';
		nlapiLogExecution('ERROR', 'Input Parameters', logMsg);
		nlapiLogExecution('ERROR', 'location', location);

		var filters = new Array();
		filters.push(new nlobjSearchFilter('nameinternal', null, 'is', itemId));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		if(location!=null && location!='')
			filters.push(new nlobjSearchFilter('location', null, 'anyof', ['@NONE@',location]));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('itemid');
		columns[0].setSort(true);

		var itemSearchResults = nlapiSearchRecord('item', null, filters, columns);
		if(itemSearchResults != null && itemSearchResults.length > 0){
			for(var i=0;i<itemSearchResults.length;i++)
			{	
				var itemInternalId = itemSearchResults[i].getId();
				nlapiLogExecution('ERROR', 'Internal Id', itemInternalId);
				itemRecordArr.push(itemInternalId);
				//var itemcolumns = nlapiLookupField('item', itemInternalId, [ 'recordType']);
				//var itemType = itemcolumns.recordType;

				//nlapiLogExecution('ERROR', 'itemType', itemType);

				//itemRecord = nlapiLoadRecord(itemType, itemInternalId);
			}
		}
	}

	nlapiLogExecution('ERROR', 'eBiz_RF_GetItemForItemId', 'End');
	return itemRecordArr;
}


function checkserialritem(itemNo)
{

	nlapiLogExecution('DEBUG', 'checkserialritem', 'Start');
	nlapiLogExecution('DEBUG', 'Input Item No', itemNo);

	var currItem = "";

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_nls_sns_serial_number', null, 'is', itemNo));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));


	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_nls_sns_item_no');



	var itemSearchResults = nlapiSearchRecord('customrecord_nls_serial_no_status', null, filters, columns);

	//nlapiLogExecution('DEBUG', 'result Item No', itemSearchResults[0].getValue('custrecord_nls_sns_item_no'));

	if(itemSearchResults != null)
		currItem = itemSearchResults[0].getText('custrecord_nls_sns_item_no');


	var logMsg = 'Item = ' + currItem;
	nlapiLogExecution('DEBUG', 'Item Retrieved', logMsg);
	nlapiLogExecution('DEBUG', 'checkserialritem', 'End');

	return currItem;
}



function createWO()
{

	var record = nlapiCreateRecord('workorder');
	nlapiLogExecution('ERROR', 'record', record);

	for (var fieldname in datain)
	{
		if (datain.hasOwnProperty(fieldname))
		{
			if (fieldname != 'RecordType' && fieldname != 'id')
			{
				var vMWorkOrderID = datain.WorkOrderID;
				var vMAssemblyItem=datain.AssemblyItemID;
				var vMQuantity=datain.Quantity;
				var vMLocationId=datain.LocationID;
				var vMMasterLotNo=datain.MasterLotNumber;
				var vMLicensePlate=datain.LicensePlate;
				var vMBinLocation=datain.BinLocationID;

				record.setFieldValue('assemblyitem', vMAssemblyItem);
				record.setFieldValue('quantity', vMQuantity);
				record.setFieldValue('WorkOrderID', vMWorkOrderID);
				record.setFieldValue('location', vMLocationId);
				record.setFieldValue('custbody_ebiz_lotnumber', vMMasterLotNo);
				record.setFieldValue('custbody_ebiz_expirydate', datain.MasterLotExpDate);

			}
		}
	}
}

function buildAssembly()
{
	var fromRecord = 'workorder'; 
	var toRecord = 'assemblybuild'; 
	var record = nlapiTransformRecord(fromRecord, 1851388, toRecord);//1851388-WO Intr ID
	record.setFieldValue('quantity', 1);
	record.setFieldValue('location', 15);	

	record.setLineItemValue('component', 'item', 1,1487) ; 
	record.setLineItemValue('component', 'item', 2,1486) ; 
	record.setLineItemValue('component', 'item', 3,1496) ; 
	var id = nlapiSubmitRecord(record, false);
}