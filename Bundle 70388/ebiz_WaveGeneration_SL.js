/***************************************************************************
	  eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_WaveGeneration_SL.js,v $
 *     	   $Revision: 1.71.2.10.4.1.4.5.2.1 $
 *     	   $Date: 2015/09/23 14:58:06 $
 *     	   $Author: deepshikha $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_13 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_WaveGeneration_SL.js,v $
 * Revision 1.71.2.10.4.1.4.5.2.1  2015/09/23 14:58:06  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.71.2.10.4.1.4.5  2015/07/29 18:29:48  mpragada
 * Case# 201413734
 * UCC Issue Fixes
 *
 * Revision 1.71.2.10.4.1.4.4  2014/07/22 06:33:53  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20149462
 *
 * Revision 1.71.2.10.4.1.4.3  2013/10/16 14:53:59  grao
 * Case# 20124280
 * Issue fixes related to the 20124280
 *
 * Revision 1.71.2.10.4.1.4.2  2013/09/19 15:17:23  rmukkera
 * Case# 20124446
 *
 * Revision 1.71.2.10.4.1.4.1  2013/03/01 14:34:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.71.2.10.4.1  2012/11/01 14:55:02  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.71.2.10  2012/04/30 11:42:13  spendyala
 * CASE201112/CR201113/LOG201121
 * While Searching of Item in ItemMaster,
 * 'name' filter is changed to 'nameinternal' and also checking weather the item is inactive state or not.
 *
 * Revision 1.71.2.9  2012/04/20 14:34:22  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.71.2.8  2012/03/09 08:19:27  spendyala
 * CASE201112/CR201113/LOG201121
 * Code Merged from trunk.
 *
 * Revision 1.71.2.7  2012/03/02 01:25:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.84  2012/03/02 01:11:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.83  2012/02/23 17:58:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.82  2012/02/21 15:04:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.81  2012/02/20 13:37:28  gkalla
 * CASE201112/CR201113/LOG201121
 * Added Line level WMS Carrier
 *
 * Revision 1.80  2012/02/16 00:55:58  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.79  2012/02/01 15:13:36  snimmakayala
 * CASE201112/CR201113/LOG201121
 * update uomlevel text instead of internal id for pick task.
 *
 * Revision 1.78  2012/01/30 14:36:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.77  2012/01/30 06:08:16  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.76  2012/01/27 23:09:45  gkalla
 * CASE201112/CR201113/LOG201121
 * Pallet Label generation by Shiva
 *
 * Revision 1.75  2012/01/13 16:50:36  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Generation User Event changes
 *
 * Revision 1.74  2012/01/13 13:44:15  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Wavegeneration
 *
 * Revision 1.73  2012/01/12 13:24:02  spendyala
 * CASE201112/CR201113/LOG201121
 * added search filter based on the wmsStatusflag for fetching records from order line table .
 *
 * Revision 1.72  2012/01/08 15:03:23  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code tunning
 *
 * Revision 1.71  2012/01/06 13:00:50  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Added Remarks field to pick report to show failure for picks.
 *
 * Revision 1.70  2012/01/05 14:01:49  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code tunning
 *
 * Revision 1.69  2011/12/30 17:09:12  spendyala
 * CASE201112/CR201113/LOG201121
 * added OrderLine Status field to sublist
 *
 * Revision 1.68  2011/12/30 00:11:07  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.67  2011/12/22 18:42:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Shipping
 *
 * Revision 1.63  2011/12/16 12:30:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.62  2011/12/12 13:16:11  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.61  2011/12/12 08:41:22  rgore
 * CASE201112/CR201113/LOG201121
 * Sanitized error message for spelling and grammar.
 * - Ratnakar
 * 12 Dec 2011
 *
 * Revision 1.60  2011/12/05 14:58:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.59  2011/11/28 12:20:38  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Confirm Exceptions
 *
 * Revision 1.58  2011/11/22 12:53:10  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Alloc Qty override issue
 *
 * Revision 1.57  2011/11/17 22:44:31  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Overallocation against a lot.
 *
 * Revision 1.56  2011/11/15 15:54:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * set display flag to 'N' when allocating inventory
 *
 * Revision 1.55  2011/11/15 09:06:33  spendyala
 * CASE201112/CR201113/LOG201121
 * added filter for ship date
 *
 * Revision 1.54  2011/11/02 11:09:36  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.53  2011/10/25 22:29:26  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cluster Pick Issue Fixes
 *
 * Revision 1.52  2011/10/24 21:25:52  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cartonization code tunning
 *
 * Revision 1.51  2011/10/17 10:47:29  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Container Generation by Order
 *
 * Revision 1.50  2011/10/17 08:53:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Container Generation by Order
 *
 * Revision 1.49  2011/10/14 16:19:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Gen Issue fixes
 *
 * Revision 1.48  2011/10/07 08:57:40  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.47  2011/10/05 23:15:58  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Zero pick qty issue fixes
 *
 * Revision 1.46  2011/10/05 19:20:58  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Zero pick qty issue fixes
 *
 * Revision 1.45  2011/10/05 11:25:09  gkalla
 * CASE201112/CR201113/LOG201121
 * For Lot# updation to Opentask
 *
 * Revision 1.44  2011/10/04 13:34:43  sallampati
 * CASE2011263/CR2011260/LOG2011188
 * Wave generation failed for lotcontrolledassembly item. code is changed to work for all kinds of items
 *
 * Revision 1.43  2011/10/03 16:02:20  spendyala
 * CASE201112/CR201113/LOG20112
 * cluster generation for max pickOrder No. and MaxOrderNo.
 *
 * Revision 1.42  2011/10/03 13:30:47  snimmakayala
 * CASE201112/CR201113/LOG20112
 * Carrier Combo added
 *
 * Revision 1.41  2011/09/30 14:15:47  snimmakayala
 * CASE201112/CR201113/LOG20112
 * Pack Factor related changes
 *
 * Revision 1.40  2011/09/29 11:22:02  gkalla
 * CASE201112/CR201113/LOG201121
 * To set G status in Fulfillment ord line
 *
 * Revision 1.39  2011/09/28 16:45:31  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.38  2011/09/28 07:55:05  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.37  2011/09/27 14:42:50  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RMA Functionality New Files
 *
 * Revision 1.36  2011/09/27 09:42:23  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.35  2011/09/26 20:12:45  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.34  2011/09/24 11:04:22  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.33  2011/09/22 13:25:14  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.32  2011/09/22 13:15:26  spendyala
 * CASE201112/CR201113/LOG201121
 * added cluster number generation functionality
 *
 * Revision 1.29  2011/09/15 18:57:23  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UOM Conversion
 *
 * Revision 1.27  2011/09/06 22:07:18  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cartonization changes.
 * Task Split
 *
 * Revision 1.26  2011/09/06 12:13:19  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cartonization changes.
 * Task Split
 *
 * Revision 1.25  2011/09/05 11:04:09  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cartonization changes.
 * Task Split
 *
 * Revision 1.24  2011/09/05 10:46:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cartonization changes.
 * Task Split
 *
 * Revision 1.23  2011/09/02 14:49:34  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cartonization changes.
 * Task Split
 *
 * Revision 1.22  2011/09/02 12:21:27  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cartonization changes.
 * Task Split
 *
 * Revision 1.21  2011/09/02 11:37:03  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cartonization changes.
 * Task Split
 *
 * Revision 1.20  2011/09/02 11:23:02  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cartonization changes.
 * Task Split
 *
 * Revision 1.19  2011/08/31 14:59:03  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cartonization changes.(Cube only)
 * Total Order wise and Total Task wise
 *
 * Revision 1.18  2011/08/31 10:35:47  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cartonization changes.(Cube only)
 * Total Order wise and Total Task wise
 *
 * Revision 1.17  2011/08/31 09:46:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cartonization changes.(Cube only)
 * Total Order wise and Total Task wise
 *
 * Revision 1.16  2011/08/30 17:29:01  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cartonization changes.(Cube only)
 * Total Order wise and Total Task wise
 *
 * Revision 1.15  2011/08/30 16:26:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Cartonization changes.(Cube only)
 *
 * Revision 1.14  2011/08/25 12:25:45  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Pick Confirmation Changes
 *
 * Revision 1.13  2011/07/29 11:03:23  pattili
 * CASE201112/CR201113/LOG201121
 * Corrected few minor issues
 *
 *****************************************************************************/

/**
 * 
 */
function isItemSelected(request, subList, element, index){
	var retVal = false;
	if(request.getLineItemValue(subList, element, index) == 'T')
		retVal = true;

	return retVal;
}

/**
 * 
 * @param skuList
 * @param sku
 * @returns {Boolean}
 */
function updateSKUList(skuList, sku, orderQty){
	var skuFound = false;

	// Search for the SKU in the SKU list and add the order quantity if the SKU already exists
	for(var i = 0; i < skuList.length; i++){
		if(parseFloat(skuList[i][0]) == parseFloat(sku)){
			skuList[i][1] = parseFloat(skuList[i][1]) + parseFloat(orderQty);
			skuFound = true;
		}
	}

	// Adding a new item to the list if the SKU is not found in this list
	if(!skuFound){
		skuList[skuList.length] = [sku, orderQty];
	}
}

/**
 * Return the fulfillment order information to be used for wave generation
 * NOTE: WILL NEVER RETURN NULL; WILL ALWAYS RETURN ATLEAST A BLANK ARRAY
 * 
 * @param request
 * @returns {Array}
 */
function getSelectedFulfillmentOrderInfo(request){
	nlapiLogExecution('ERROR','into getSelectedFulfillmentOrderInfo', '');
	var fulfillOrderInfoArray = new Array();
	var orderInfoCount = 0;

	// Retrieve the number of items for which wave needs to be created
	var lineCount = request.getLineItemCount('custpage_items');

	for(var k = 1; k <= lineCount; k++){
		var selectValue = request.getLineItemValue('custpage_items', 'custpage_so', k);
		//nlapiLogExecution('ERROR','selectValue', selectValue);

		if(isItemSelected(request,'custpage_items', 'custpage_so', k)){
			var itemName = request.getLineItemValue('custpage_items', 'custpage_itemname', k);
			var itemNo = request.getLineItemValue('custpage_items', 'custpage_itemno', k);
			var availQty = request.getLineItemValue('custpage_items', 'custpage_avlqty', k);
			var orderQty = request.getLineItemValue('custpage_items', 'custpage_ordqty', k);
			var doNo = request.getLineItemValue('custpage_items', 'custpage_doinernno', k);
			var lineNo = request.getLineItemValue('custpage_items', 'custpage_lineno', k);
			var doName = request.getLineItemValue('custpage_items', 'custpage_sono', k);
			var soInternalID = request.getLineItemValue('custpage_items', 'custpage_soinernno', k);
			var itemStatus = request.getLineItemValue('custpage_items', 'custpage_skustatus', k);
			var packCode = request.getLineItemValue('custpage_items', 'custpage_packcode', k);
			var uom = request.getLineItemValue('custpage_items', 'custpage_uomid', k);
			var lotBatchNo = request.getLineItemValue('custpage_items', 'custpage_lotbatch', k);
			var location = request.getLineItemValue('custpage_items', 'custpage_location', k);
			var company = request.getLineItemValue('custpage_items', 'custpage_company', k);
			var itemInfo1 = request.getLineItemValue('custpage_items', 'custpage_iteminfo1', k);
			var itemInfo2 = request.getLineItemValue('custpage_items', 'custpage_iteminfo2', k);
			var itemInfo3 = request.getLineItemValue('custpage_items', 'custpage_iteminfo2', k);
			var orderType = request.getLineItemValue('custpage_items', 'custpage_ordtype', k);
			var orderPriority = request.getLineItemValue('custpage_items', 'custpage_ordpriority', k);
			var wmsCarrier = request.getLineItemValue('custpage_items', 'custpage_wmscarrier', k);

			var currentRow = [k, soInternalID, lineNo, doNo, doName, itemName, itemNo, itemStatus, 
			                  availQty, orderQty, packCode, uom, lotBatchNo, company, 
			                  itemInfo1, itemInfo2, itemInfo3, orderType, orderPriority,location,wmsCarrier];

			fulfillOrderInfoArray[orderInfoCount++] = currentRow;
		}
	}
	nlapiLogExecution('ERROR','fulfillOrderInfoArray', fulfillOrderInfoArray.length);

	nlapiLogExecution('ERROR','out of getSelectedFulfillmentOrderInfo', '');

	return fulfillOrderInfoArray;

}

/**
 * Function to return the configured item dimensions for a list of SKUs
 * NOTE: WILL NEVER RETURN NULL; WILL ALWAYS RETURN ATLEAST AN EMPTY ARRAY
 * 
 * @param skuList
 * @returns {Array}
 */
function getItemDimensions(skuList){
	nlapiLogExecution('ERROR','into getItemDimensions ', skuList.length);
	var itemDimensionList = new Array();
	var itemDimCount = 0;

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', skuList));	// ALL SKUs IN SKU LIST
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));							// ACTIVE RECORDS ONLY

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');								// ITEM/SKU
	columns[1] = new nlobjSearchColumn('custrecord_ebizuomskudim');								// UOM
	columns[2] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim');						// UOM LEVEL
	columns[3] = new nlobjSearchColumn('custrecord_ebizqty');									// QUANTITY
	columns[4] = new nlobjSearchColumn('custrecord_ebizbaseuom');								// BASE UOM FLAG
	columns[5] = new nlobjSearchColumn('custrecord_ebizdims_packflag');							// PACK FLAG	
	columns[6] = new nlobjSearchColumn('custrecord_ebizweight');								// WEIGHT	
	columns[7] = new nlobjSearchColumn('custrecord_ebizcube');									// CUBE	
	columns[2].setSort(false);
	// SEARCH FROM ITEM DIMENSIONS
	var searchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

	if(searchResults != null && searchResults.length > 0){
		for(var i = 0; i < searchResults.length; i++){
			var sku = searchResults[i].getValue('custrecord_ebizitemdims');
			var uom = searchResults[i].getValue('custrecord_ebizuomskudim');
			var uomLevel = searchResults[i].getValue('custrecord_ebizuomlevelskudim');
			var qty = searchResults[i].getValue('custrecord_ebizqty');
			var baseUOMFlag = searchResults[i].getValue('custrecord_ebizbaseuom');
			var packflag = searchResults[i].getValue('custrecord_ebizdims_packflag');
			var weight = searchResults[i].getValue('custrecord_ebizweight');
			var cube = searchResults[i].getValue('custrecord_ebizcube');

			var skuDimRecord = [sku, uom, uomLevel, qty, baseUOMFlag, packflag,weight,cube];
			itemDimensionList[itemDimCount++] = skuDimRecord;
		}
	}
	nlapiLogExecution('ERROR','out of getItemDimensions ', skuList.length);
	return itemDimensionList;
}

/**
 * Function to retrieve a list of unique SKUs and their total order quantity to pick
 * from the list of fulfillment orders
 * NOTE: WILL NEVER RETURN NULL; WILL ATLEAST RETURN AN EMPTY ARRAY
 * 
 * @param fulfillOrderList
 * @returns {Array}
 */
function getSKUList(fulfillOrderList){
	var skuList = new Array();

	if(fulfillOrderList != null && fulfillOrderList.length > 0){
		for(var i = 0; i < fulfillOrderList.length; i++){
			var currSKU = fulfillOrderList[i][6];
			var orderQty = fulfillOrderList[i][9];
			updateSKUList(skuList, currSKU, orderQty);
		}
	}

	return skuList;
}

/**
 * 
 * @param itemList
 * @returns {Array}
 */
function getItemTypesForList(itemList){
	var itemTypeArray = new Array();
	var itemTypeCount = 0;

	for(var i = 0; i < itemList.length; i++){
		nlapiLogExecution('ERROR', 'itemList.length',itemList.length);
		nlapiLogExecution('ERROR', 'Printing Itemtype');
		var itemType = nlapiLookupField('item', itemList[i], 'recordType');
		nlapiLogExecution('ERROR', 'Printing Itemtype value ',itemType );
		nlapiLogExecution('ERROR', 'Printing Item value ',itemList[i]);
		var currentRow = [itemList[i], itemType];
		itemTypeArray[itemTypeCount] = currentRow;
		itemTypeCount=itemTypeCount+1;
		nlapiLogExecution('ERROR', 'End of getItemTypeForList' );
	}

	return itemTypeArray;
}

/**
 * 
 * @param item
 * @param itemType
 * @returns
 */
function getItemRecord(item, itemType){
	var itemRecord = null;

	//This code is generalized to work for any kind of item types on 100411 ..Sudheer &Ganesh
	itemRecord = nlapiLoadRecord(itemType, item);


	/*if(itemType == 'inventoryitem')
		itemRecord = nlapiLoadRecord('inventoryitem', item);
	else if(itemType == 'assemblyitem')
		itemRecord = nlapiLoadRecord('assemblyitem', item);
	else if (itemType == 'lotnumberedinventoryitem')
		itemRecord = nlapiLoadRecord('lotnumberedinventoryitem', item);
	else if (ItemType == 'serializedinventoryitem')
		itemRecord = nlapiLoadRecord('serializedinventoryitem', item);
	else if (ItemType == 'lotnumberedassemblyitem')
		itemRecord = nlapiLoadRecord('serializedinventoryitem', item);
	 */
	return itemRecord;
}

/**
 * 
 * @param itemTypeArray
 * @returns {Array}
 */
function getItemGroupAndFamily(itemTypeArray){
	var itemGroupFamilyArray = new Array();
	var itemGroupFamilyCount = 0;

	for(var i = 0; i < itemTypeArray.length; i++){
		var itemTypeElement = itemTypeArray[i];
		var item = itemTypeElement[0];
		var itemType = itemTypeElement[1];
		nlapiLogExecution('ERROR', 'Item 1 ', item);
		var itemRecord = getItemRecord(item, itemType);

		var currentRow = [item, itemType, itemRecord.getFieldValue('custitem_item_group'),
		                  itemRecord.getFieldValue('custitem_item_family')];
		itemGroupFamilyArray[itemGroupFamilyCount++] = currentRow;
	}

	return itemGroupFamilyArray;
}

function validateRequestParams(request){
	var soNo = "";
	var companyName = "";
	var itemName= "";
	var customerName = "";
	var orderPriority = "";
	var orderType = "";
	var itemGroup = "";
	var itemFamily = "";
	var packCode = "";
	var UOM = "";
	var itemStatus = "";
	var itemInfo1 = "";
	var itemInfo2 = "";
	var itemInfo3 = "";
	var shippingCarrier = "";
	var shipCountry = ""; 
	var shipState = "";
	var shipCity = "";
	var shipAddr1 = "";
	var shipmentNo = "";
	var routeNo = "";
	var carrier = "";
	var shipdate = "";
	var wmsstatusflag = "";

	var localVarArray = new Array();

	if (request.getParameter('custpage_qbso') != null && request.getParameter('custpage_qbso') != "") {
		soNo = request.getParameter('custpage_qbso');
	}

	if (request.getParameter('custpage_company') != null && request.getParameter('custpage_company') != "") {
		companyName = request.getParameter('custpage_company');
	}

	if (request.getParameter('custpage_item') != null && request.getParameter('custpage_item') != "") {
		itemName = request.getParameter('custpage_item');
	}

	if (request.getParameter('custpage_consignee') != null && request.getParameter('custpage_consignee') != "") {
		customerName = request.getParameter('custpage_consignee');
	}

	if (request.getParameter('custpage_ordpriority') != null && request.getParameter('custpage_ordpriority') != "") {
		orderPriority = request.getParameter('custpage_ordpriority');
	}

	if (request.getParameter('custpage_ordtype') != null && request.getParameter('custpage_ordtype') != "") {
		orderType = request.getParameter('custpage_ordtype');
	}

	if (request.getParameter('custpage_itemgroup') != null && request.getParameter('custpage_itemgroup') != "") {
		itemGroup = request.getParameter('custpage_itemgroup');
	}

	if (request.getParameter('custpage_itemfamily') != null && request.getParameter('custpage_itemfamily') != "") {
		itemFamily = request.getParameter('custpage_itemfamily');
	}

	if (request.getParameter('custpage_packcode') != null && request.getParameter('custpage_packcode') != "") {
		packCode = request.getParameter('custpage_packcode');//custrecord_linepackcode
	}

	if (request.getParameter('custpage_uom') != null && request.getParameter('custpage_uom') != "") {
		UOM = request.getParameter('custpage_uom');//custrecord_lineuom_id
	}

	if (request.getParameter('custpage_itemstatus') != null && request.getParameter('custpage_itemstatus') != "") {
		itemStatus = request.getParameter('custpage_itemstatus'); //custrecord_linesku_status
	}

	if (request.getParameter('custpage_siteminfo1')!=null && request.getParameter('custpage_siteminfo1')!="" ){
		itemInfo1=request.getParameter('custpage_siteminfo1'); 
	}

	if (request.getParameter('custpage_siteminfo2')!=null && request.getParameter('custpage_siteminfo2')!="" ){
		itemInfo2=request.getParameter('custpage_siteminfo2'); 
	}

	if (request.getParameter('custpage_siteminfo3')!=null && request.getParameter('custpage_siteminfo3')!="" ){
		itemInfo3=request.getParameter('custpage_siteminfo3'); 
	}

	if (request.getParameter('custpage_shippingcarrier')!=null && request.getParameter('custpage_shippingcarrier')!="" ){
		shippingCarrier=request.getParameter('custpage_shippingcarrier'); 
	}

	if (request.getParameter('custpage_country')!=null && request.getParameter('custpage_country')!="" ){
		shipCountry=request.getParameter('custpage_country'); 
	}

	if (request.getParameter('custpage_state')!=null && request.getParameter('custpage_state')!="" ){
		shipState=request.getParameter('custpage_country'); 
	}

	if (request.getParameter('custpage_city')!=null && request.getParameter('custpage_city')!="" ){
		shipCity=request.getParameter('custpage_city'); 
	}

	if (request.getParameter('custpage_addr1')!=null && request.getParameter('custpage_addr1')!="" ){
		shipAddr1=request.getParameter('custpage_addr1'); 
	}

	if (request.getParameter('custpage_shipmentno')!=null && request.getParameter('custpage_shipmentno')!="" ){
		shipmentNo=request.getParameter('custpage_shipmentno'); 
	}

	if (request.getParameter('custpage_routeno')!=null && request.getParameter('custpage_routeno')!="" ){
		routeNo=request.getParameter('custpage_routeno'); 
	}
	//added carrier By Suman
	if (request.getParameter('custpage_carrier')!=null && request.getParameter('custpage_carrier')!="" ){
		carrier=request.getParameter('custpage_carrier'); 
	}

	if (request.getParameter('custpage_soshipdate')!=null && request.getParameter('custpage_soshipdate')!="" ){
		shipdate=request.getParameter('custpage_soshipdate'); 
	}

	if (request.getParameter('custpage_wmsstatusflag')!=null && request.getParameter('custpage_wmsstatusflag')!="" ){
		wmsstatusflag=request.getParameter('custpage_wmsstatusflag'); 
	}
	nlapiLogExecution('ERROR','request.getParameter(custpage_wmsstatusflag)',request.getParameter('custpage_wmsstatusflag'));
	var currentRow = [soNo, companyName, itemName, customerName, orderPriority, orderType, itemGroup, itemFamily,
	                  packCode, UOM, itemStatus, itemInfo1, itemInfo2, itemInfo3, shippingCarrier, shipCountry, 
	                  shipState, shipCity, shipAddr1, shipmentNo, routeNo, carrier, shipdate,wmsstatusflag];
	localVarArray.push(currentRow);
	return localVarArray;
}

/**
 * Function to retrieve all replen zones and form them into a comma 
 * separated string
 * NOTE: WILL RETURN NULL IF THERE ARE NO ZONES CONFIGURED
 * 
 * @returns List of Replen Zones
 */
function getListForAllPickZones(){

	nlapiLogExecution('ERROR', 'into getListForAllPickZones','');
	var pickRuleList = new Array();
	var pickRuleCount = 0;

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizruleidpick');
	columns[1] = new nlobjSearchColumn('custrecord_ebizpickmethod');
	columns[2] = new nlobjSearchColumn('custrecord_ebizpickzonerul');
	columns[3] = new nlobjSearchColumn('custrecord_ebizlocationgrouppickrul');
	columns[4] = new nlobjSearchColumn('custrecord_ebizlocationpickrul');
	//columns[5] = new nlobjSearchColumn('custrecord_ebizsequencenopickrul');
	columns[5]=new nlobjSearchColumn('formulanumeric');
	columns[5].setFormula("TO_NUMBER({custrecord_ebizsequencenopickrul})");
	columns[6] = new nlobjSearchColumn('custrecord_ebizskufamilypickrul');
	columns[7] = new nlobjSearchColumn('custrecord_ebizskugrouppickrul');
	columns[8] = new nlobjSearchColumn('custrecord_ebizskupickrul');
	columns[9] = new nlobjSearchColumn('custrecord_ebizskuinfo1');
	columns[10] = new nlobjSearchColumn('custrecord_ebizskuinfo2');
	columns[11] = new nlobjSearchColumn('custrecord_ebizskuinfo3');
	columns[12] = new nlobjSearchColumn('custrecord_ebizpackcodepickrul');
	columns[13] = new nlobjSearchColumn('custrecord_ebizuomlevelpickrul');
	columns[14] = new nlobjSearchColumn('custrecord_ebizskustatspickrul');
	columns[15] = new nlobjSearchColumn('custrecord_ebizordertypepickrul');
	columns[16] = new nlobjSearchColumn('custrecord_ebizuompickrul');
	columns[17] = new nlobjSearchColumn('custrecord_allocation_strategy','custrecord_ebizpickmethod');
	columns[18] = new nlobjSearchColumn('custrecord_ebizpickfacelocation','custrecord_ebizpickmethod');
	columns[19] = new nlobjSearchColumn('custrecord_ebizcreatecluster','custrecord_ebizpickmethod');
	columns[20] = new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxorders','custrecord_ebizpickmethod');
	columns[21] = new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxpicks','custrecord_ebizpickmethod');
	columns[22] = new nlobjSearchColumn('custrecord_ebizabcvel');
	columns[23] = new nlobjSearchColumn('custrecord_cartonization_method','custrecord_ebizpickmethod');
	columns[24] = new nlobjSearchColumn('custrecord_ebizsitepickrule');
	columns[5].setSort();

	var pickRuleSearchResult = new nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters, columns);

	if(pickRuleSearchResult != null){
		for (var i=0; i<pickRuleSearchResult.length; i++){
			var pickRuleId = pickRuleSearchResult[i].getValue('custrecord_ebizruleidpick');
			var pickMethodId = pickRuleSearchResult[i].getValue('custrecord_ebizpickmethod');
			var pickZoneId = pickRuleSearchResult[i].getValue('custrecord_ebizpickzonerul');
			var locationGroupId = pickRuleSearchResult[i].getValue('custrecord_ebizlocationgrouppickrul');
			var binLocationId = pickRuleSearchResult[i].getValue('custrecord_ebizlocationpickrul');
			var sequenceNo = pickRuleSearchResult[i].getValue('custrecord_ebizsequencenopickrul');
			var itemFamilyId = pickRuleSearchResult[i].getValue('custrecord_ebizskufamilypickrul');
			var itemGroupId = pickRuleSearchResult[i].getValue('custrecord_ebizskugrouppickrul');
			var itemId = pickRuleSearchResult[i].getValue('custrecord_ebizskupickrul');
			var itemInfo1 = pickRuleSearchResult[i].getValue('custrecord_ebizskuinfo1');
			var itemInfo2 = pickRuleSearchResult[i].getValue('custrecord_ebizskuinfo2');
			var itemInfo3 = pickRuleSearchResult[i].getValue('custrecord_ebizskuinfo3');
			var packCode = pickRuleSearchResult[i].getValue('custrecord_ebizpackcodepickrul');
			var uomLevel = pickRuleSearchResult[i].getValue('custrecord_ebizuomlevelpickrul');
			var itemStatus = pickRuleSearchResult[i].getValue('custrecord_ebizskustatspickrul');
			var orderType = pickRuleSearchResult[i].getValue('custrecord_ebizordertypepickrul');
			var uom = pickRuleSearchResult[i].getValue('custrecord_ebizuompickrul');
			var allocationStrategy = pickRuleSearchResult[i].getValue('custrecord_allocation_strategy','custrecord_ebizpickmethod');
			var pickfaceLocationFlag = pickRuleSearchResult[i].getValue('custrecord_ebizpickfacelocation','custrecord_ebizpickmethod');
			var clusterPickFlag = pickRuleSearchResult[i].getValue('custrecord_ebizcreatecluster','custrecord_ebizpickmethod');
			var maxOrders = pickRuleSearchResult[i].getValue('custrecord_ebiz_pickmethod_maxorders','custrecord_ebizpickmethod');
			var maxPicks = pickRuleSearchResult[i].getValue('custrecord_ebiz_pickmethod_maxpicks','custrecord_ebizpickmethod');
			var abcVel = pickRuleSearchResult[i].getValue('custrecord_abcvelpickrule');
			var cartonizationmethod = pickRuleSearchResult[i].getValue('custrecord_cartonization_method','custrecord_ebizpickmethod');
			var picklocation=pickRuleSearchResult[i].getValue('custrecord_ebizsitepickrule');

			var currentRow = [i, pickRuleId, pickMethodId, pickZoneId, locationGroupId, binLocationId, 
			                  sequenceNo, itemFamilyId, itemGroupId, itemId, itemInfo1, itemInfo2, itemInfo3, 
			                  packCode, uomLevel, itemStatus, orderType, uom, 
			                  allocationStrategy, pickfaceLocationFlag, clusterPickFlag, maxOrders, maxPicks, abcVel,cartonizationmethod,picklocation];


//			nlapiLogExecution('ERROR', 'pickRule ID', pickRuleId);
//			nlapiLogExecution('ERROR', 'pickMethod ID', pickMethodId);
//			nlapiLogExecution('ERROR', 'pickZone ID', pickZoneId);
//			nlapiLogExecution('ERROR', 'Location Group ID', locationGroupId);
//			nlapiLogExecution('ERROR', 'Location ID', binLocationId);
//			nlapiLogExecution('ERROR', 'pickRule - Current Row', currentRow);

			pickRuleList.push(currentRow);
		}
	}

	nlapiLogExecution('ERROR', 'out of getListForAllPickZones','');

	return pickRuleList;
}

/**
 * 
 * @param pickRulesList
 * @returns {String}
 */
function getZoneList(pickRulesList){
	var zoneList = "";
	var zoneTextList = "";

	if (pickRulesList != null){
		for (var i = 0; i < pickRulesList.length; i++){

			if (i == 0){
				zoneList += pickRulesList[i].getValue('custrecord_putawayzonepickrule');
				zoneTextList += pickRulesList[i].getText('custrecord_putawayzonepickrule');
			} else {
				zoneList += ',';
				zoneList += pickRulesList[i].getValue('custrecord_putawayzonepickrule');

				zoneTextList += ',';
				zoneTextList += pickRulesList[i].getText('custrecord_putawayzonepickrule');
			}
		}
	} 

	return zoneList;
}

/**
 * get the item list from the skuList and build an array of all items
 * @param skuList
 * @returns {String}
 */
function getItemList(skuList){
	var itemList = new Array();

	if (skuList != null && skuList.length > 0){
		for (var i = 0; i < skuList.length; i++){
			itemList.push(skuList[i][0]);
			nlapiLogExecution('ERROR', 'Item', skuList[i][0]);
		}
	} 

	return itemList;
}

/**
 * Function to return the group and family for an item from the item group family list
 * NOTE: WILL RETURN ATLEAST AN EMPTY ARRAY; WILL NEVER RETURN NULL
 * 
 * @param item
 * @param itemGroupFamilyList
 * @returns
 */
function getItemGroupFamilyForItem(item, itemGroupFamilyList){
	var groupFamilyArray = new Array();
	var itemFound = false;

	if(itemGroupFamilyList != null && itemGroupFamilyList.length > 0){
		for(var i = 0; i < itemGroupFamilyList.length; i++){
			if(!itemFound){
				if(item == parseFloat(itemGroupFamilyList[i][0])){
					groupFamilyArray.push(itemGroupFamilyList[i][2]);	// ITEM GROUP
					groupFamilyArray.push(itemGroupFamilyList[i][3]);	// ITEM FAMILY

					itemFound = true;
				}
			}
		}
	}
	nlapiLogExecution('ERROR','itemFound',itemFound);
	return groupFamilyArray;
}

function getPickStrategyForFulfilmentOrder(currentOrder, pickRulesList, itemGroupFamilyList){
	var pickStrategyIndex = -1;
	var matchFound = false;

	nlapiLogExecution('ERROR', 'currentOrder in getItemGroupFamilyForItem ',currentOrder[6]);

	// Get item group and family for item
	var groupFamilyList = getItemGroupFamilyForItem(currentOrder[6], itemGroupFamilyList);

	// Traverse through all the pick strategies and determine the match
	if(pickRulesList != null && pickRulesList.length > 0){
		for(var i = 0; i < pickRulesList.length; i++){
			if(!matchFound){
				if(pickRulesList[i][25]==currentOrder[19])
				{
					if(parseFloat(pickRulesList[i][9]) == parseFloat(currentOrder[6]) ||				// ITEM
							parseFloat(pickRulesList[i][15]) == parseFloat(currentOrder[7]) ||			// ITEM STATUS
							pickRulesList[i][10] == currentOrder[14] ||								// ITEM INFO 1
							pickRulesList[i][11] == currentOrder[15] ||								// ITEM INFO 2
							pickRulesList[i][12] == currentOrder[16] ||								// ITEM INFO 3
							parseFloat(pickRulesList[i][17]) == parseFloat(currentOrder[11]) ||			// UOM
							parseFloat(pickRulesList[i][8]) == parseFloat(groupFamilyList[0]) ||		// ITEM GROUP
							parseFloat(pickRulesList[i][16]) == parseFloat(currentOrder[9]) ||			// ORDER TYPE
							parseFloat(pickRulesList[i][7]) == parseFloat(groupFamilyList[1])){			// ITEM FAMILY
						pickStrategyIndex = i;
						matchFound = true;
					}
				}

				if(! matchFound && i == parseFloat(pickRulesList.length - 1)){
					pickStrategyIndex = 0;
					matchFound = true;
				}
			}
		}
	}

	return pickStrategyIndex;
}

function getAllocConfigForOrder(allocConfigDtls, index){
	var allocConfig = new Array();
	var matchFound = false;

	if(allocConfigDtls != null && allocConfigDtls.length > 0){
		for (var i = 0; i < allocConfigDtls.length; i++){
			if(!matchFound){
				if(parseFloat(allocConfigDtls[i][0]) == parseFloat(index)){
					allocConfig = allocConfigDtls[i];
					matchFound = true;
				}
			}
		}
	}

	return allocConfig;
}

/**
 * 
 */

function getAlreadyAllocatedInv(allocatedInv, binLocation,fromLP,item,lot){
	nlapiLogExecution('ERROR', 'Into getAlreadyAllocatedInv',allocatedInv);
	var alreadyAllocQty = 0;
	if(allocatedInv != null && allocatedInv.length > 0){
		for(var i = 0; i < allocatedInv.length; i++){
			nlapiLogExecution('ERROR', 'fromLP',fromLP);
			nlapiLogExecution('ERROR', 'allocatedInv[i][2]',allocatedInv[i][2]);
			nlapiLogExecution('ERROR', 'binLocation',binLocation);
			nlapiLogExecution('ERROR', 'allocatedInv[i][0]',allocatedInv[i][0]);
			nlapiLogExecution('ERROR', 'item',item);
			nlapiLogExecution('ERROR', 'allocatedInv[i][3]',allocatedInv[i][3]);
			nlapiLogExecution('ERROR', 'lot',lot);
			nlapiLogExecution('ERROR', 'allocatedInv[i][4]',allocatedInv[i][4]);

			if(fromLP == allocatedInv[i][2] && binLocation==allocatedInv[i][0] && item==allocatedInv[i][3] && lot==allocatedInv[i][4])
				alreadyAllocQty = parseFloat(alreadyAllocQty) + parseFloat(allocatedInv[i][1]);
		}
	}
	nlapiLogExecution('ERROR', 'Out of getAlreadyAllocatedInv');
	return alreadyAllocQty;
}


/**
 * 
 * @param inventorySearchResults
 * @param binLocation
 * @param allocatedInv
 * @returns {Array}
 */
function getAvailQtyFromInventory(inventorySearchResults, binLocation, allocatedInv,dowhlocation,fulfilmentItem){
	var binLocnInvDtls = new Array();
	var matchFound = false;

	if(inventorySearchResults != null && inventorySearchResults.length > 0){
		for(var i = 0; i < inventorySearchResults.length; i++){
			if(!matchFound){
				invBinLocnId = inventorySearchResults[i].getValue('custrecord_ebiz_inv_binloc');
				var InvWHLocation = inventorySearchResults[i].getValue('custrecord_ebiz_inv_loc');
				nlapiLogExecution('ERROR', 'Inventory WH Location ',InvWHLocation);
				nlapiLogExecution('ERROR', 'FulFillment WH Location ',dowhlocation);
				var invItem=inventorySearchResults[i].getValue('custrecord_ebiz_inv_sku');
				if((dowhlocation==InvWHLocation)&&(parseFloat(invBinLocnId) == parseFloat(binLocation)) && fulfilmentItem==invItem){
					var actualQty = inventorySearchResults[i].getValue('custrecord_ebiz_qoh');
					var inventoryAvailableQuantity = inventorySearchResults[i].getValue('custrecord_ebiz_avl_qty');
					var allocationQuantity = inventorySearchResults[i].getValue('custrecord_ebiz_alloc_qty');
					var packCode = inventorySearchResults[i].getValue('custrecord_ebiz_inv_packcode');
					var itemStatus = inventorySearchResults[i].getValue('custrecord_ebiz_inv_sku_status');
					var whLocation = inventorySearchResults[i].getValue('custrecord_ebiz_inv_loc');
					var fromLP = inventorySearchResults[i].getValue('custrecord_ebiz_inv_lp');
					var inventoryItem=inventorySearchResults[i].getValue('custrecord_ebiz_inv_sku');
					var fromLot = inventorySearchResults[i].getText('custrecord_ebiz_inv_lot');
					nlapiLogExecution('ERROR','fromLot From invloc',fromLot);
					nlapiLogExecution('ERROR','fromLot From invloc',inventorySearchResults[i].getValue('custrecord_ebiz_inv_lot'));

					var recordId = inventorySearchResults[i].getId();

					// Get allocation already done
					var alreadyAllocQty = getAlreadyAllocatedInv(allocatedInv, binLocation,fromLP,inventoryItem,fromLot);

					// Adjusting for allocations already done
					inventoryAvailableQuantity = parseFloat(inventoryAvailableQuantity) - parseFloat(alreadyAllocQty);

					var currentRow = [i, actualQty, inventoryAvailableQuantity, allocationQuantity, packCode,
					                  itemStatus, whLocation, fromLP, recordId,inventoryItem,fromLot];

					binLocnInvDtls.push(currentRow);

					matchFound = true;
				}
			}
		}
	}

	return binLocnInvDtls;
}


function getContainerLPNo(fulfillOrderList, salesOrderList){
	var containerLP = "";

//	for (var i = 0; i<)
	containerLP = GetMaxLPNo('1', '2');

	return containerLP;
}

function isSalesOrderDistinct(salesOrderNo, salesOrderList){
	var isDistinct = false;
	if(salesOrderList.length == 0){
		isDistinct = true;
	} else if(salesOrderList.length > 0){
		var vCnt = 0;
		for(var i = 0; i < salesOrderList.length; i++){
			if(parseFloat(salesOrderNo) == parseFloat(salesOrderList[i]))
				vCnt++;
		}
		if(vCnt == 0){
			isDistinct=true;
		}
		else{
			isDistinct=false;
		}
	}

	return isDistinct;
}

function updateInventoryWithAllocation(recordId, allocationQuantity){
	nlapiLogExecution('ERROR', 'Into updateInventoryWithAllocation - Qty', allocationQuantity);
	var inventoryTransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', recordId);
	var alreadyallocqty = inventoryTransaction.getFieldValue('custrecord_ebiz_alloc_qty');
	if (isNaN(alreadyallocqty))
	{
		alreadyallocqty = 0;
	}
	var totalallocqty=parseFloat(allocationQuantity)+parseFloat(alreadyallocqty);
	inventoryTransaction.setFieldValue('custrecord_ebiz_alloc_qty', parseFloat(totalallocqty).toFixed(5));
	inventoryTransaction.setFieldValue('custrecord_ebiz_callinv', 'N');
	inventoryTransaction.setFieldValue('custrecord_ebiz_displayfield', 'N');

	nlapiSubmitRecord(inventoryTransaction,false,true);
	nlapiLogExecution('ERROR', 'Out of updateInventoryWithAllocation - Record ID',recordId);

}

function getClusterConfig(transOrderNo, transPickMethod, fulfillOrderList){
	var clusterConfig = new Array();

	// Determine the cluster method
	var clusterMethod = "";
	if(maxOrders != null && (!isNaN(maxOrders))){
		clusterMethod = "BY_ORDER";
		limitCount = maxOrders;
	} else if(maxPicks != null && (!isNaN(maxPicks))){
		clusterMethod = "BY_PICKS";
		limitCount = maxPicks;
	} else {
		clusterMethod = "CLUSTER_METHOD_NOT_SPECIFIED";
		limitCount = -1;
	}

	var currentRow = [clusterMethod, limitCount];
	clusterConfig.push(currentRow);
	return clusterConfig;
}

function getOpenTasksForWave(ebizWaveNo){
	var pickTaskColumns = new Array();
	pickTaskColumns[0] = new nlobjSearchColumn('custrecord_ebizzone_no');
	pickTaskColumns[1] = new nlobjSearchColumn('custrecord_ebizmethod_no');
	pickTaskColumns[2] = new nlobjSearchColumn('name');
	pickTaskColumns[3] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	pickTaskColumns[2].setSort();

	var pickTaskFilters = new Array();
	pickTaskFilters[0] = new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', ebizWaveNo);
	pickTaskFilters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3');			// TASKTYPE = PICK

	var pickTaskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, pickTaskFilters, pickTaskColumns);

	return pickTaskSearchResults;
}

function clusterPicking(ebizWaveNo, fulfillOrderList, allocConfigDtls){
	// Get all transaction
	var pickTaskSearchResults = getOpenTasksForWave(ebizWaveNo);

	if(pickTaskSearchResults != null && pickTaskSearchResults.length > 0){
		for(var i = 0; i < pickTaskSearchResults.length; i++){
			var taskPickMethod = pickTaskSearchResults[i].getValue('custrecord_ebizmethod_no');
			var orderNo = pickTaskSearchResults[i].getValue('custrecord_ebiz_order_no');
		}
	}

	//Fectching distinct Pick Zone and Pick Method 
	var pickMethodFields = ['custrecord_ebizcreatecluster', 'custrecord_ebiz_pickmethod_maxorders', 'custrecord_ebiz_pickmethod_maxpicks'];
	var pickMethodColumns = nlapiLookupField('customrecord_ebiznet_pick_method', pickMethodId, pickMethodFields);
	var checkCluster 	= pickMethodColumns.custrecord_ebizcreatecluster;
	var maxOrders 	= pickMethodColumns.custrecord_ebiz_pickmethod_maxorders;
	var maxPicks 	= pickMethodColumns.custrecord_ebiz_pickmethod_maxpicks;
	var limitCount = 0;
	var noOfPicks 	= 0;
	var noOfOrders = 0;
	nlapiLogExecution('ERROR', 'checkCluster', checkCluster);

	if (checkCluster == 'T') {
		var pickTaskColumns = new Array();
		pickTaskColumns[0] = new nlobjSearchColumn('custrecord_ebizzone_no');
		pickTaskColumns[1] = new nlobjSearchColumn('custrecord_ebizmethod_no');
		pickTaskColumns[2] = new nlobjSearchColumn('name');
		pickTaskColumns[2].setSort();

		var pickTaskFilters = new Array();
		pickTaskFilters[0] = new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', ebizWaveNo);
		pickTaskFilters[1] = new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', pickZoneId);
		pickTaskFilters[2] = new nlobjSearchFilter('custrecord_ebizmethod_no', null, 'is', pickMethodId);
		pickTaskFilters[3] = new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3');			// TASKTYPE = PICK

		var previousOrderNo = "";
		var clusterNo = "";

		var pickTaskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, pickTaskFilters, pickTaskColumns);
		/*
		 *	If maxOrders is specified,
		 * 		Group PICK tasks by the number of orders in maxOrders
		 * 	If maxOrder is not specified and maxPicks is specified
		 * 		Group all pick tasks by tasks
		 */

		// Get cluster configuration
		var clusterConfig = getClusterConfig(maxOrders, maxPicks);

		if (pickTaskSearchResults != null){
			for (var s = 0; s < pickTaskSearchResults.length; s++) {
				if(limitCount > 0){
					var currentOrderNo = pickTaskSearchResults[s].getValue('name');

					// Create the first cluster
					if(parseFloat(s) == 0)
						clusterNo = GetMaxTransactionNo('WAVECLUSTER');

					if(clusterConfig[0] == "BY_ORDER" && previousOrderNo != currentOrderNo){
						noOfOrders = parseFloat(noOfOrders) + 1;
						previousOrderNo = currentOrderNo;
						if(parseFloat(noOfOrders) > parseFloat(limitCount)){
							noOfOrders = 1;
							clusterNo = GetMaxTransactionNo('WAVECLUSTER');
						}
					} else if(clusterConfig[0] == "BY_PICK") {
						noOfPicks = parseFloat(noOfPicks) + 1;
						if(parseFloat(noOfPicks) > parseFloat(limitCount)){
							noOfPicks = 1;
							clusterNo = GetMaxTransactionNo('WAVECLUSTER');
						}
					}

					// Update the cluster no in the open task
					var clusterFields = new Array();
					var clusterValues = new Array();
					clusterFields[0] = 'custrecord_ebiz_clus_no';
					clusterValues[0] = clusterNo.toString();
					nlapiSubmitField('customrecord_ebiznet_trn_opentask', pickTaskSearchResults[s].getId(), clusterFields, clusterValues);
				}
			}
		}
	}
}


//fulfillOrderList:  [0=k, 1=soInternalID, 2=lineNo, 3=doNo, 4=doName, 5=itemName, 6=itemNo, 7=itemStatus, 
//8=availQty, 9=orderQty, 10=packCode, 11=uom, 12=lotBatchNo, 13=company, 
//14=itemInfo1, 15=itemInfo2, 16=itemInfo3, 17=orderType, 18=orderPriority];

function createRecordInOpenTask(fulfillOrderList,expectedQuantity, waveNo, containerLP, beginLocationId, recordId,packCode,
		itemStatus, pickMethod, pickZone, pickStrategyId, whLocation, LP,uomlevel,totalweight,totalcube,containersize,fromLot,vnotes,wmsCarrier){
	nlapiLogExecution('ERROR', 'into createRecordInOpenTask with container lp : ',containerLP);
	nlapiLogExecution('ERROR', 'into createRecordInOpenTask with beginLocationId : ',beginLocationId);
//	nlapiLogExecution('ERROR', 'item Name',fulfillOrderList[5]);
//	nlapiLogExecution('ERROR', 'pickMethod',pickMethod);
//	nlapiLogExecution('ERROR', 'pickZone',pickZone);
//	nlapiLogExecution('ERROR', 'pickStrategyId',pickStrategyId);


	var openTaskRecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');

	openTaskRecord.setFieldValue('name', fulfillOrderList[4]);							// DELIVERY ORDER NAME
	openTaskRecord.setFieldValue('custrecord_ebiz_sku_no', fulfillOrderList[6]);		// ITEM INTERNAL ID
	openTaskRecord.setFieldValue('custrecord_sku', fulfillOrderList[6]);				// ITEM NAME
	openTaskRecord.setFieldValue('custrecord_expe_qty', parseFloat(expectedQuantity).toFixed(5));				// FULFILMENT ORDER QUANTITY
	openTaskRecord.setFieldValue('custrecord_container_lp_no', containerLP);
	openTaskRecord.setFieldValue('custrecord_ebiz_lpno', containerLP);
	openTaskRecord.setFieldValue('custrecord_line_no', fulfillOrderList[2]);
	openTaskRecord.setFieldValue('custrecord_tasktype', '3'); 							// TASK TYPE = PICK

	openTaskRecord.setFieldValue('custrecord_actbeginloc', beginLocationId);
	openTaskRecord.setFieldValue('custrecord_ebiz_cntrl_no', fulfillOrderList[3]);
	openTaskRecord.setFieldValue('custrecord_ebiz_order_no', parseFloat(fulfillOrderList[1]));
	openTaskRecord.setFieldValue('custrecord_ebiz_receipt_no', fulfillOrderList[3]);
	openTaskRecord.setFieldValue('custrecord_ebiz_wave_no',  waveNo);
	openTaskRecord.setFieldValue('custrecordact_begin_date',DateStamp());
	if(wmsCarrier != null && wmsCarrier != "")
		openTaskRecord.setFieldValue('custrecord_ebizwmscarrier',  wmsCarrier);
	// Setting for failed pick
	if(beginLocationId == "")
		openTaskRecord.setFieldValue('custrecord_wms_status_flag', '26'); 					// STATUS FLAG = F
	else
		openTaskRecord.setFieldValue('custrecord_wms_status_flag', '9'); 					// STATUS FLAG = G

	openTaskRecord.setFieldValue('custrecord_from_lp_no', LP);
	openTaskRecord.setFieldValue('custrecord_lpno', LP);
	openTaskRecord.setFieldValue('custrecord_actualbegintime', TimeStamp());
	openTaskRecord.setFieldValue('custrecord_invref_no', recordId);
	openTaskRecord.setFieldValue('custrecord_packcode', packCode);
	openTaskRecord.setFieldValue('custrecord_sku_status', itemStatus);
	openTaskRecord.setFieldValue('custrecord_uom_id', fulfillOrderList[11]);
	openTaskRecord.setFieldValue('custrecord_batch_no', fulfillOrderList[12]);
	openTaskRecord.setFieldValue('custrecord_ebizrule_no', pickStrategyId);
	openTaskRecord.setFieldValue('custrecord_ebizmethod_no', pickMethod);
	openTaskRecord.setFieldValue('custrecord_ebizzone_no', pickZone);
	openTaskRecord.setFieldValue('custrecord_comp_id', fulfillOrderList[13]);
	nlapiLogExecution('ERROR', 'whLocation',whLocation);

	if(whLocation != null && whLocation!='')
		openTaskRecord.setFieldValue('custrecord_wms_location', whLocation);	

	var currentContext = nlapiGetContext();  
	var currentUserID = currentContext.getUser();		
	openTaskRecord.setFieldValue('custrecord_ebizuser', currentUserID);		
	openTaskRecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);						
	openTaskRecord.setFieldValue('custrecord_taskassignedto', currentUserID);
	openTaskRecord.setFieldValue('custrecord_uom_level', uomlevel);
	openTaskRecord.setFieldValue('custrecord_total_weight', parseFloat(totalweight).toFixed(5));
	openTaskRecord.setFieldValue('custrecord_totalcube', parseFloat(totalcube).toFixed(5));
	openTaskRecord.setFieldValue('custrecord_container', containersize);
	openTaskRecord.setFieldValue('custrecord_batch_no', fromLot);
	openTaskRecord.setFieldValue('custrecord_notes', vnotes);

	nlapiLogExecution('ERROR', 'fromLot',fromLot);
	nlapiSubmitRecord(openTaskRecord,false,true);
	nlapiLogExecution('ERROR', 'Opentask updation Completed');
	nlapiLogExecution('ERROR', 'out of createRecordInOpenTask with container lp : ',containerLP);
}

/**
 * Retrieve all pickface locations
 * 
 * @returns {Array}
 */
function getPFLocationsForOrder(){
	var pfLocationResults = new Array();
	pfLocationResults = getSKUQtyInPickfaceLocns();
	return pfLocationResults;
}

function getLocnGroupListForOrder(locnGroup, binLocation, pickZone){
//	nlapiLogExecution('ERROR','AlocnGroup',locnGroup);
//	nlapiLogExecution('ERROR','AbinLocation',binLocation);
//	nlapiLogExecution('ERROR','ApickZone',pickZone);

	var locnGroupList = new Array();
	// Get the bin location group list
	if (locnGroup == "")
		locnGroupList = getZoneLocnGroupsList(pickZone);
	else{
		if (binLocation == null){
			locnGroupList.push(locnGroup);
		}
	}
//	nlapiLogExecution('ERROR','getLocnGroupListForOrder: locnGroupList',locnGroupList.length);
	return locnGroupList;
}

/**
 * Function to retrieve the pick strategy details for each fulfillment order
 * Every element has the composition as below,
 * Fulfillment Order Index, pickStrategyId, pickMethod, pickZone, locnGroup, binLocation, allocationStrategy, pfLocationResults
 * 
 * NOTE: THIS WILL NEVER RETURN NULL; WILL ATLEAST RETURN AN EMPTY ARRAY; pfLocationResults CAN BE NULL
 * 
 * @param fulfillOrderList
 * @returns {Array}
 */
function getPickConfigForAllOrder(fulfillOrderList, pickRulesList, itemGroupFamilyArray){

	nlapiLogExecution('ERROR','into getPickConfigForAllOrder : pickRulesList',pickRulesList);
	var allocConfigDtls = new Array();

	// Get all pickface locations
	var pfLocationResults = getPFLocationsForOrder();

	// For all fulfilment orders
	for (var i = 0; i < fulfillOrderList.length; i++){
		// Get index of matching pick strategy
		var index = getPickStrategyForFulfilmentOrder(fulfillOrderList[i], pickRulesList,
				itemGroupFamilyArray);
		nlapiLogExecution('ERROR','index',index);

		var pickStrategyId = pickRulesList[index][1];
		var pickMethod = pickRulesList[index][2];
		var pickZone = pickRulesList[index][3];
		var locnGroup = pickRulesList[index][4];
		var binLocation = pickRulesList[index][5];
		var allocationStrategy = pickRulesList[index][18];
		var pickfaceLocationFlag = pickRulesList[index][19];
		var clusterPickFlag = pickRulesList[index][20];
		var maxOrders = pickRulesList[index][21];
		var maxPicks = pickRulesList[index][22];
		var cartonizationmethod =  pickRulesList[index][24];

		var tempPFLocationResults = new Array();
		if(pickfaceLocationFlag == 'T')
			tempPFLocationResults = pfLocationResults;
		else
			tempPFLocationResults = null;

		var locnGroupList = getLocnGroupListForOrder(locnGroup, binLocation, pickZone);

		// Pick strategy details for the fulfillment order; Using index in the order list to identify the order
		var currentRow = [i, pickStrategyId, pickMethod, pickZone, locnGroup, binLocation, allocationStrategy,
		                  tempPFLocationResults, locnGroupList, clusterPickFlag, maxOrders, maxPicks,cartonizationmethod];

		allocConfigDtls.push(currentRow);
	}

	nlapiLogExecution('ERROR','getPickConfigForAllOrder: allocConfigDtls',allocConfigDtls.length);

	nlapiLogExecution('ERROR','out of getPickConfigForAllOrder : allocConfigDtls.length ',allocConfigDtls.length);	


	return allocConfigDtls;
}

function getLocnGroupAsString(locnGroupList){
	var locnGroupStr = "";
	if(locnGroupList != null && locnGroupList.length > 0){
		for (var i = 0; i < locnGroupList.length; i++){
			if(locnGroupStr == "")
				locnGroupStr = locnGroupList[i];
			else
				locnGroupStr = locnGroupStr + ',' + locnGrouList[i];
		}
	}

	return locnGroupStr;
}

function getAllLocnGroups(allocConfigDtls){
	nlapiLogExecution('ERROR', 'into getAllLocnGroups : allocConfigDtls  ', allocConfigDtls);
	var locnGroupListForAll = new Array();

	if(allocConfigDtls != null && allocConfigDtls.length > 0){
		for(var i = 0; i < allocConfigDtls.length; i++){
			var tempLocnGroupList = allocConfigDtls[i][8];
			nlapiLogExecution('ERROR', 'tempLocnGroupList : allocConfigDtls[i][8]  ', allocConfigDtls[i][8]);
			locnGroupListForAll.push(tempLocnGroupList.toString());
		}
	}


	nlapiLogExecution('ERROR', 'out of getAllLocnGroups : locnGroupListForAll  ', locnGroupListForAll);


	return locnGroupListForAll;
}

/**
 * 
 * @param request
 * @param response
 */
function generateWave(request, response){
	try{

		var context = nlapiGetContext();
		nlapiLogExecution('ERROR','Remaining usage at the start',context.getRemainingUsage());
		/*
		 * get the list of fulfillment orders selected
		 * retrieve the fulfillment order#, sku, order qty, committed qty
		 * retrieve the applicable pick strategy, pick method, pick zone, location groups for skus selected
		 * retrieve the item dimensions for skus selected
		 * generate the breakdown for the committed qty for skus selected
		 * 
		 */
		// Validate all request parameters
		var form = nlapiCreateForm('Wave Generation');
		
		var hiddenField_selectpage = form.addField('custpage_hiddenfieldselectpage', 'text', '').setDisplayType('hidden');
		hiddenField_selectpage.setDefaultValue('F');
		
		var newpageval = request.getParameter('custpage_hiddenfieldselectpage');

		// Get the next WAVE and REPORT sequences
		var eBizWaveNo;
		var eBizReportNo;
		if(newpageval != 'T')//Without selecting any order wave# is created when paging to the next page.
		{
		   eBizWaveNo = GetMaxTransactionNo('WAVE');
		   eBizReportNo = GetMaxTransactionNo('REPORT');
		}
		nlapiLogExecution('ERROR', 'Wave & Report No.', eBizWaveNo + '|', eBizReportNo);

		// Get the list of fulfillment orders selected for wave generation
		// Every element of the returned list contains index, soInternalID, lineNo, doNo, doName, 
		// itemName, itemNo, itemStatus, availQty, orderQty, packCode, uom, lotBatchNo, company, 
		// itemInfo1, itemInfo2, itemInfo3, orderType and orderPriority
		var fulfillOrderList = getSelectedFulfillmentOrderInfo(request);

		// Proceed only if there is atleast 1 fulfillment order selected
		if(newpageval != 'T'){
			if(fulfillOrderList!=null && fulfillOrderList.length > 0){
				// Get list of unique SKUs
				var skuQtyList = getSKUList(fulfillOrderList);

			// Build the SKU List from the Array retrieved with the item and quantity
			var skuList = getItemList(skuQtyList);
			//nlapiLogExecution('ERROR','skuList',skuList.length);

			// Get item dimensions for SKU list
			var itemDimList = getItemDimensions(skuList);
			//nlapiLogExecution('ERROR','itemDimList',itemDimList.length);

			// Retrieving item types
			var itemTypeArray = getItemTypesForList(skuList);

			// Retrieve the item group and item family for all items
			var itemGroupFamilyArray = getItemGroupAndFamily(itemTypeArray);
			nlapiLogExecution('ERROR', 'After ItemGroupFamilyArrary' );
			/*
			 * Retrieve the list of all pick Strategies for the item family/item group/item from the list
			 * Based on the Pick Stregies, fetch the pick method and pick zone
			 * Based on the pick zones, retrieve all the bin location groups
			 * From the list of location groups, retrieve the list of bin locations
			 * For the bin locations, update the allocation quantity
			 */

			// Retrieve the Pick Rules list
			var pickRulesList = getListForAllPickZones();

			nlapiLogExecution('ERROR', 'After pickRulesList');
//			fulfillOrderList:  [0=k, 1=soInternalID, 2=lineNo, 3=doNo, 4=doName, 5=itemName, 6=itemNo, 7=itemStatus, 
//			8=availQty, 9=orderQty, 10=packCode, 11=uom, 12=lotBatchNo, 13=company, 
//			14=itemInfo1, 15=itemInfo2, 16=itemInfo3, 17=orderType, 18=orderPriority];

//			pickRulesList	:	[0=i, 1=pickRuleId, 2=pickMethodId, 3=pickZoneId, 4=locationGroupId, 5=binLocationId, 
//			6=sequenceNo, 7=itemFamilyId, 8=itemGroupId, 9=itemId, 10=itemInfo1, 11=itemInfo2, 
//			12=itemInfo3, 13=packCode, 14=uomLevel, 15=itemStatus, 16=orderType, 17=uom, 
//			18=allocationStrategy, 19=pickfaceLocationFlag];
			/**
			 * Loop through the fulfilment order array
			 * 	Loop through the pick rules array
			 * 		check the item or itemFamily or itemGroup or itemStatus or itemInfo1 or 
			 * 			itemInfo2 or itemInfo3 or UOM Level or packCode or orderType from pick rule array matches 
			 * 			with the same from fulfilment order array.
			 * 			if the record is found
			 * 				capture the index from the pick rule
			 */
//			var pickStrategyId = "";
//			var pickMethod = "";
//			var pickZone = "";
//			var locnGroup = "";
//			var binLocation = "";
//			var allocationStrategy = "";
//			var pickfaceLocationFlag = "";

			// Pick Strategy Details for all Fulfillment Orders
			var allocConfigDtls = getPickConfigForAllOrder(fulfillOrderList, pickRulesList, itemGroupFamilyArray);
			nlapiLogExecution('ERROR', 'getPickConfigForAllOrder');
			var allLocnGroups = getAllLocnGroups(allocConfigDtls);
			// Get the list of replen locn groups
			//Commneted by Satish.N 09/05/2011
			//var replenLocnGroupList = getReplenLocnGroups();
			//nlapiLogExecution('ERROR', 'replenLocnGroupList', replenLocnGroupList);

			// Fetch the inventory data from the bin location group list for the corresponding items list
			var inventorySearchResults = getItemDetails(allLocnGroups, skuList);

			if (inventorySearchResults != null && inventorySearchResults.length >0){
				//nlapiLogExecution('ERROR', 'inventorySearchResults: if condition', 'here');
				nlapiLogExecution('ERROR', 'custrecord_ebiz_inv_lot',inventorySearchResults[0].getValue('custrecord_ebiz_inv_lot'));
				//allocateQuantity(inventorySearchResults, fulfillOrderList, eBizWaveNo, allocConfigDtls);

				allocateQuantity(inventorySearchResults, fulfillOrderList, eBizWaveNo, allocConfigDtls);

//				clusterPicking(eBizWaveNo, fulfillOrderList, allocConfigDtls);

				var waveDetails = new Array();
				waveDetails["ebiz_wave_no"] = eBizWaveNo;
				waveDetails["short_pick"] = 'N';

				showInlineMessage(form, 'Confirmation', 'Wave generation completed successfully ', ' Wave# = ' + eBizWaveNo.toString());
				response.sendRedirect('SUITELET', 'customscript_wavepickrpt', 'customdeploy_wavepickrpt', false, waveDetails);
			}
			else
			{
				showInlineMessage(form, 'Error', 'Not enough inventory in the pick zone(s)', "");
				response.writePage(form);

				}
			}
		}

		nlapiLogExecution('ERROR','Remaining usage at the end',context.getRemainingUsage());
	}


	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in generatewave ', exp);	
		showInlineMessage(form, 'Error', 'Wave Generation Failed', "");
		response.writePage(form);
	}
}

/**
 * 
 * @param fulfilmentOrderNo
 * @param orderQuantity
 * @param eBizWaveNo
 */
function updateFulfilmentOrder(fulfilmentOrderNo, fulfilmentOrderLineNo, orderQuantity, eBizWaveNo){

//	var filters = new Array();
//	//commented by Ganesh on 29/09/2011 to get orderline details based on internal id
//	//filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', fulfilmentOrderNo));
//	filters.push(new nlobjSearchFilter('internalid', null, 'is', fulfilmentOrderNo));
//	filters.push(new nlobjSearchFilter('custrecord_ordline', null, 'is', fulfilmentOrderLineNo));

//	var columns =  new Array();
//	columns[0] = new nlobjSearchColumn('custrecord_linestatus_flag');
//	columns[1] = new nlobjSearchColumn('custrecord_pickgen_qty');
//	columns[2] = new nlobjSearchColumn('custrecord_ebiz_wave');
//	columns[3] = new nlobjSearchColumn('name');

//	var searchResults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);

//	if(searchResults != null){
//	for (var i = 0; i < searchResults.length; i++){

//	var fulfilmentOrderPickGenQty = searchResults[i].getValue('custrecord_pickgen_qty');
//	var salesorderinternalid = searchResults[i].getValue('name');
//	nlapiLogExecution('ERROR', 'fulfilmentOrderPickGenQty ', fulfilmentOrderPickGenQty);
//	if (isNaN(fulfilmentOrderPickGenQty) || fulfilmentOrderPickGenQty == "") {
//	fulfilmentOrderPickGenQty = 0;
//	}

	if (orderQuantity == "" || isNaN(orderQuantity)) {
		orderQuantity = 0;
	}
	var fulfilmentOrderline = nlapiLoadRecord('customrecord_ebiznet_ordline', fulfilmentOrderNo);
	var prevpickgenqty=fulfilmentOrderline.getFieldValue('custrecord_pickgen_qty');
	var totalPickGeneratedQuantity = parseFloat(prevpickgenqty) + parseFloat(orderQuantity);
	nlapiLogExecution('ERROR', 'totalPickGeneratedQuantity ', totalPickGeneratedQuantity);
	nlapiLogExecution('ERROR', 'orderQuantity', orderQuantity);
	fulfilmentOrderline.setFieldValue('custrecord_ebiz_wave', parseFloat(eBizWaveNo));		

	if(parseFloat(orderQuantity)>0)
		fulfilmentOrderline.setFieldValue('custrecord_linestatus_flag', '9'); //status flag--'G' (Picks Generated)	
	else
		fulfilmentOrderline.setFieldValue('custrecord_linestatus_flag', '26'); //status flag--'F' (Picks Failed)

	fulfilmentOrderline.setFieldValue('custrecord_pickgen_qty', parseFloat(totalPickGeneratedQuantity).toFixed(5));

	nlapiSubmitRecord(fulfilmentOrderline, false, true);
	nlapiLogExecution('ERROR', 'updateFulfilmentOrder ', 'Success');

//	if(parseFloat(orderQuantity)>0)
//	updatesalesorderline(salesorderinternalid,fulfilmentOrderLineNo,orderQuantity,'G');
	//}
	//}
}

/**
 * 
 * @returns {Array}
 */
function getWaveColumns(){
	var columns = new Array();

	columns[0] = new nlobjSearchColumn('custrecord_linesku_status');
	columns[1] = new nlobjSearchColumn('custrecord_do_customer');
	columns[2] = new nlobjSearchColumn('custrecord_do_carrier');
	columns[3] = new nlobjSearchColumn('custrecord_do_order_type');
	columns[4] = new nlobjSearchColumn('custrecord_do_order_priority');
	columns[5] = new nlobjSearchColumn('custrecord_lineord');
	columns[6] = new nlobjSearchColumn('custrecord_ebiz_linesku');
	columns[7] = new nlobjSearchColumn('custrecord_ord_qty');
	columns[8] = new nlobjSearchColumn('name');
	columns[8].setSort();
	columns[9] = new nlobjSearchColumn('custrecord_lineord');
	columns[9].setSort();		
	columns[10] = new nlobjSearchColumn('custrecord_ordline');
	columns[10].setSort();
	columns[11] = new nlobjSearchColumn('custrecord_linepackcode');
	columns[12] = new nlobjSearchColumn('custrecord_lineuom_id');
	columns[13] = new nlobjSearchColumn('custrecord_batch');
	columns[14] = new nlobjSearchColumn('custrecord_ordline_wms_location');
	columns[15] = new nlobjSearchColumn('custrecord_ordline_company');
	columns[16] = new nlobjSearchColumn('custrecord_fulfilmentiteminfo1');
	columns[17] = new nlobjSearchColumn('custrecord_fulfilmentiteminfo2');
	columns[18] = new nlobjSearchColumn('custrecord_fulfilmentiteminfo3');	
	columns[19] = new nlobjSearchColumn('custrecord_pickgen_qty');	
	columns[20] = new nlobjSearchColumn('custrecord_linestatus_flag');
	columns[21] = new nlobjSearchColumn('custrecord_do_wmscarrier');
	return columns;
}

/**
 * Function to articulate all the search filters for wave generation
 * 
 * @param localVarArray
 * @returns {Array}
 */
function specifyWaveFilters(localVarArray){
	var filters = new Array();

	// Sales Order No
	if(localVarArray[0][0] != "")
		filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', localVarArray[0][0]));

	// Company
	if(localVarArray[0][1] != "")
		filters.push(new nlobjSearchFilter('custrecord_ordline_company', null, 'is', localVarArray[0][1]));

	// Item Name
	if(localVarArray[0][2] != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_linesku', null, 'is', localVarArray[0][2]));

	// Customer Name
	if(localVarArray[0][3] != "")
		filters.push(new nlobjSearchFilter('custrecord_do_customer', null, 'is', localVarArray[0][3]));

	// Order Priority
	if(localVarArray[0][4] != "")
		filters.push(new nlobjSearchFilter('custrecord_do_order_priority', null, 'is', localVarArray[0][4]));

	// Order Type
	if(localVarArray[0][5] != "")
		filters.push(new nlobjSearchFilter('custrecord_do_order_type', null, 'is', localVarArray[0][5]));

	// Item Group
	if(localVarArray[0][6] != "")
		filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', localVarArray[0][6]));

	// Item Family
	if(localVarArray[0][7] != "")
		filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', localVarArray[0][7]));

	// Pack Code
	if(localVarArray[0][8] != "")
		filters.push(new nlobjSearchFilter('custrecord_linepackcode', null, 'is', localVarArray[0][8]));

	// UOM
	if(localVarArray[0][9] != "")
		filters.push(new nlobjSearchFilter('custrecord_lineuom_id', null, 'is', localVarArray[0][9]));

	// Item Status
	if(localVarArray[0][10] != "")
		filters.push(new nlobjSearchFilter('custrecord_linesku_status', null, 'is', localVarArray[0][10]));

	// Item Info 1
	if(localVarArray[0][11] != "")
		filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo1', null, 'is', localVarArray[0][11]));

	// Item Info 2
	if(localVarArray[0][12] != "")
		filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo2', null, 'is', localVarArray[0][12]));

	// Item Info 3
	if(localVarArray[0][13] != "")
		filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo3', null, 'is', localVarArray[0][13]));

	// Shipping Carrier
	if(localVarArray[0][14] != "")
		filters.push(new nlobjSearchFilter('shipmethod', 'custrecord_ns_ord', 'anyof', [localVarArray[0][14]]));

	// Ship Country
	if(localVarArray[0][15] != "")
		filters.push(new nlobjSearchFilter('shipcountry', 'custrecord_ns_ord', 'anyof', [localVarArray[0][15]]));

	// Ship State
	if(localVarArray[0][16] != "")
		filters.push(new nlobjSearchFilter('shipstate', 'custrecord_ns_ord', 'is', localVarArray[0][16]));

	// Ship City
	if(localVarArray[0][17] != "")
		filters.push(new nlobjSearchFilter('shipcity', 'custrecord_ns_ord', 'is', localVarArray[0][17]));

	// Masquerading the ship address with ship city
	if(localVarArray[0][18] != "")
		filters.push(new nlobjSearchFilter('shipaddress', 'custrecord_ns_ord', 'is', localVarArray[0][17]));

	// Shipment No.
	if(localVarArray[0][19] != "")
		filters.push(new nlobjSearchFilter('custrecord_shipment_no', null, 'is', localVarArray[0][19]));

	// Route No.
	if(localVarArray[0][20] != "")
		filters.push(new nlobjSearchFilter('custrecord_route_no', null, 'anyof', [localVarArray[0][20]]));

	// WMS Status Flag : 15-Selected Into Wave, 25-Edit, 26-Picks Failed
	filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [15,25,26]));

	// Indicates the Sales Order header
	filters.push(new nlobjSearchFilter('mainline', 'custrecord_ns_ord', 'is', 'T'));			
	// carrier Added By Suman
	if(localVarArray[0][21] != "")
		filters.push(new nlobjSearchFilter('custrecord_do_wmscarrier', null, 'anyof', [localVarArray[0][21]]));

//	if(localVarArray[0][22] != "")
//		filters.push(new nlobjSearchFilter('custrecord_ebiz_shipdate', null, 'onorbefore', [localVarArray[0][22]]));
	nlapiLogExecution('DEBUG','Shipdate',localVarArray[0][22]);
	if((localVarArray[0][22] != "" && localVarArray[0][22] != null))
	{
		//modified as on 15th July by suman.
		//If ship date is empty then replace the value with the created date of FO
		filters.push(new nlobjSearchFilter('formuladate', null, 'onorbefore', [localVarArray[0][22]]).setFormula("nvl({custrecord_ebiz_shipdate},{created})"));
	}

	if(localVarArray[0][23] != "")
		filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [localVarArray[0][23]]));

	filters.push(new nlobjSearchFilter('custrecord_ord_qty', null, 'greaterthan', 0));

	return filters;
}

function getOrdersForWaveGen(request){
	var orderList = new Array();

	// Validating all the request parameters and pushing to a local array
	var localVarArray = validateRequestParams(request);

	// Get all the search filters for wave generation
	var filters = new Array();
	filters = specifyWaveFilters(localVarArray);

	// Get all the columns that the search should return
	var columns = new Array();
	columns = getWaveColumns();

	orderList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);

	return orderList;
}

/**
 * Create the sublist that will display the items for wave generation
 * 
 * @param form
 */
function createWaveGenSublist(form){
	var sublist = form.addSubList("custpage_items", "list", "ItemList");
	sublist.addMarkAllButtons();
	sublist.addField("custpage_so", "checkbox", "Confirm").setDefaultValue('F');
	sublist.addField("custpage_sono", "text", "Fulfillment Ord #");
	sublist.addField("custpage_lineno", "text", "Line #");
	sublist.addField("custpage_customer", "text", "Customer");
	sublist.addField("custpage_carrier", "text", "Carrier");
	sublist.addField("custpage_ordtype", "text", "Order Type");
	sublist.addField("custpage_ordpriority", "text", "Order Priority");
	sublist.addField("custpage_itemname", "text", "Item");
	sublist.addField("custpage_skustatus", "text", "Item Status").setDisplayType('hidden');
	sublist.addField("custpage_skustatustext", "text", "Item Status");
	sublist.addField("custpage_packcode", "text", "Packcode");
	sublist.addField("custpage_lotbatch", "text", "LOT#");
	sublist.addField("custpage_ordqty", "integer", "Fulfilment Order Qty");
	sublist.addField("custpage_itemno", "text", "Item No").setDisplayType('hidden');
	sublist.addField("custpage_doinernno", "text", "DONo").setDisplayType('hidden');
	sublist.addField("custpage_soinernno", "text", "SONo").setDisplayType('hidden');
	sublist.addField("custpage_uomid", "text", "uomid").setDisplayType('hidden');
	sublist.addField("custpage_location", "text", "location").setDisplayType('hidden');
	sublist.addField("custpage_company", "text", "Company").setDisplayType('hidden');
	sublist.addField("custpage_iteminfo1", "text", "Item Info1").setDisplayType('hidden');
	sublist.addField("custpage_iteminfo2", "text", "Item Info2").setDisplayType('hidden');
	sublist.addField("custpage_iteminfo3", "text", "Item Info3").setDisplayType('hidden');
	sublist.addField("custpage_sointernid", "text", "Item Info3").setDisplayType('hidden');
	sublist.addField("custpage_pickgenqty", "text", "Pick Gen Qty").setDisplayType('hidden');
	sublist.addField("custpage_wmsstatus", "text", "OrderLine Status");
	sublist.addField("custpage_wmscarrier", "text", "WMS Carrier").setDisplayType('hidden');
}

/**
 * Function to add one fulfillment order to the sublist on the form
 * 
 * @param form
 * @param currentOrder
 * @param i
 */
function addFulfilmentOrderToSublist(form, currentOrder, i){

	if(currentOrder.getText('custrecord_do_order_type')!='Work Order')
	{
		try{
			var vOrdQty = parseFloat(currentOrder.getValue('custrecord_ord_qty'));

			nlapiLogExecution('Error', 'vOrdQty', vOrdQty);

			var vPickGenQty=0;

			if(currentOrder.getValue('custrecord_pickgen_qty')!=null && currentOrder.getValue('custrecord_pickgen_qty')!='')	
				vPickGenQty = parseFloat(currentOrder.getValue('custrecord_pickgen_qty'));
			var vCarrier='';
			if(currentOrder.getText('custrecord_do_carrier') != null && currentOrder.getText('custrecord_do_carrier') != '')
				vCarrier=currentOrder.getText('custrecord_do_carrier');
			else if(currentOrder.getText('custrecord_do_wmscarrier') != null && currentOrder.getText('custrecord_do_wmscarrier') != '')
				vCarrier=currentOrder.getText('custrecord_do_wmscarrier');	

			nlapiLogExecution('Error', 'vPickGenQty', vPickGenQty);

			vOrdQty = parseFloat(vOrdQty)-parseFloat(vPickGenQty);

			nlapiLogExecution('Error', 'vOrdQty', vOrdQty);

			form.getSubList('custpage_items').setLineItemValue('custpage_lineno', i + 1,
					currentOrder.getValue('custrecord_ordline'));
			form.getSubList('custpage_items').setLineItemValue('custpage_customer', i + 1,
					currentOrder.getText('custrecord_do_customer'));
			form.getSubList('custpage_items').setLineItemValue('custpage_carrier', i + 1,
					vCarrier);
			form.getSubList('custpage_items').setLineItemValue('custpage_ordtype', i + 1,
					currentOrder.getText('custrecord_do_order_type'));
			form.getSubList('custpage_items').setLineItemValue('custpage_ordpriority', i + 1, 
					currentOrder.getText('custrecord_do_order_priority'));
			form.getSubList('custpage_items').setLineItemValue('custpage_itemno', i + 1, 
					currentOrder.getValue('custrecord_ebiz_linesku'));
			form.getSubList('custpage_items').setLineItemValue('custpage_itemname', i + 1, 
					currentOrder.getText('custrecord_ebiz_linesku'));
			form.getSubList('custpage_items').setLineItemValue('custpage_ordqty', i + 1, 
					vOrdQty.toString());
			form.getSubList('custpage_items').setLineItemValue('custpage_sono', i + 1, 
					currentOrder.getValue('custrecord_lineord'));
			form.getSubList('custpage_items').setLineItemValue('custpage_doinernno', i + 1, currentOrder.getId());
			form.getSubList('custpage_items').setLineItemValue('custpage_soinernno', i + 1, 
					currentOrder.getValue('name'));
			form.getSubList('custpage_items').setLineItemValue('custpage_skustatustext', i + 1, 
					currentOrder.getText('custrecord_linesku_status'));			
			form.getSubList('custpage_items').setLineItemValue('custpage_skustatus', i + 1, 
					currentOrder.getValue('custrecord_linesku_status'));
			form.getSubList('custpage_items').setLineItemValue('custpage_packcode', i + 1, 
					currentOrder.getText('custrecord_linepackcode'));
			form.getSubList('custpage_items').setLineItemValue('custpage_uomid', i + 1, 
					currentOrder.getText('custrecord_lineuom_id'));
			form.getSubList('custpage_items').setLineItemValue('custpage_lotbatch', i + 1, 
					currentOrder.getValue('custrecord_batch'));
			form.getSubList('custpage_items').setLineItemValue('custpage_location', i + 1, 
					currentOrder.getValue('custrecord_ordline_wms_location'));
			form.getSubList('custpage_items').setLineItemValue('custpage_iteminfo1', i + 1, 
					currentOrder.getText('custrecord_fulfilmentiteminfo1'));
			form.getSubList('custpage_items').setLineItemValue('custpage_iteminfo2', i + 1, 
					currentOrder.getText('custrecord_fulfilmentiteminfo2'));
			form.getSubList('custpage_items').setLineItemValue('custpage_iteminfo3', i + 1, 
					currentOrder.getText('custrecord_fulfilmentiteminfo3'));		
			form.getSubList('custpage_items').setLineItemValue('custpage_company', i + 1, 
					currentOrder.getValue('custrecord_ordline_company'));	
			form.getSubList('custpage_items').setLineItemValue('custpage_pickgenqty', i + 1, 
					currentOrder.getValue('custrecord_pickgen_qty'));
			form.getSubList('custpage_items').setLineItemValue('custpage_wmsstatus', i + 1, 
					currentOrder.getText('custrecord_linestatus_flag'));
			form.getSubList('custpage_items').setLineItemValue('custpage_wmscarrier', i + 1, 
					currentOrder.getValue('custrecord_do_wmscarrier'));

		}
		catch(exp)
		{
			nlapiLogExecution('Error', 'Exception in addFulfilmentOrderToSublist');
		}
	}

	nlapiLogExecution('Error', 'Out of addFulfilmentOrderToSublist');
}

/**
 * 
 * @param request
 * @param response
 */
function WaveGenSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Wave Generation');
		var hiddenField_selectpage = form.addField('custpage_hiddenfieldselectpage', 'text', '').setDisplayType('hidden');
		hiddenField_selectpage.setDefaultValue('F');
		form.setScript('customscript_wavegeneration_cl');

		// Create the sublist
		createWaveGenSublist(form);

		var orderList = getOrdersForWaveGen(request);

		if(orderList != null && orderList.length > 0){
			for(var i = 0; i < orderList.length; i++){
				var currentOrder = orderList[i];

				// Call function to add one fulfillment order to the sublist
				addFulfilmentOrderToSublist(form, currentOrder, i);
			}
		}

		form.addSubmitButton('Generate Wave');

		response.writePage(form);
	}
	else{
		generateWave(request, response);
	}
}


function PickStrategie(varsalesordInernId,item, itemfamily, itemgroup, avlqty, ebizWaveNo, Dono, 
		vDoname, itemname, Lineno, vpackcode, vuomid, vskustatus, vlotbatchno,ItemType,vlocation,
		vCompany,vitemInfo1,vitemInfo2,vitemInfo3,Reportno){
	var now = new Date();
	var Resultarray = new Array();
	var Pickarray = new Array();

	var actallocqty = 0;


	//var LocArray= PickStrategieMain(item,itemfamily,itemgroup,avlqty,ItemType,
	//	vlocation,vlotbatchno,vskustatus,
	//	vitemInfo1,vitemInfo2,vitemInfo3,Reportno);
	var LocArray= getInventoryByPickStrategy(item, itemfamily, itemgroup, ItemType, avlqty,
			vlocation, vlotbatchno, vskustatus, vitemInfo1, vitemInfo2, vitemInfo3, Reportno);
	nlapiLogExecution('ERROR','LocArray.length',LocArray.length);
	if(LocArray!=null && LocArray.length>0)
	{
		for (k = 0; k < LocArray.length; k++) {
			//for (k = 0; k < LocArray.length-1; k++) {
			var arrpick=LocArray[k];
			var cnfmqty=arrpick[0];
			var vactLocation=arrpick[1];
			var LP=arrpick[2];
			var Recid=arrpick[3];
			var vpickzone=arrpick[5];
			var vpicmethod=arrpick[6];
			var vPickRule=arrpick[7];
			var vLotbatch =arrpick[8];
			var Replntype =arrpick[9];
			var pickfaceqty =arrpick[10];


//			nlapiLogExecution('ERROR','cnfmqty',cnfmqty);
//			nlapiLogExecution('ERROR','vactLocation',vactLocation);
//			nlapiLogExecution('ERROR','LP',LP);
//			nlapiLogExecution('ERROR','Recid',Recid);
//			nlapiLogExecution('ERROR','vpickzone',vpickzone);
//			nlapiLogExecution('ERROR','vpicmethod',vpicmethod);
//			nlapiLogExecution('ERROR','vPickRule',vPickRule);
//			nlapiLogExecution('ERROR','vLotbatch',vLotbatch);

			if (cnfmqty > 0) {
				actallocqty=actallocqty + parseFloat(cnfmqty);
				//updating allocqty in Inventory
				var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', Recid);
				var invtallocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
				if (invtallocqty == null || isNaN(invtallocqty) || invtallocqty == "") {
					invtallocqty = 0;
				}
				if (Replntype == "REPLN") {
					transaction.setFieldValue('custrecord_ebiz_alloc_qty', (parseFloat(invtallocqty) + parseFloat(pickfaceqty)).toFixed(5));
				}
				else
				{
					transaction.setFieldValue('custrecord_ebiz_alloc_qty', (parseFloat(invtallocqty) + parseFloat(cnfmqty)).toFixed(5));
				}

				//Updateting Doline
				var doline = nlapiLoadRecord('customrecord_ebiznet_ordline', Dono);
				var dopickgenqty = doline.getFieldValue('custrecord_pickgen_qty');
				if (isNaN(dopickgenqty) || dopickgenqty == "") {
					dopickgenqty = 0;
				}
				doline.setFieldValue('custrecord_linestatus_flag', '6'); //status flag--'G'
				doline.setFieldValue('custrecord_pickgen_qty', (parseFloat(dopickgenqty) + parseFloat(cnfmqty)).toFixed(5));
				doline.setFieldValue('custrecord_ebiz_wave', ebizWaveNo);


				//create the openTask record With PICK Task Type
				var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
				nlapiLogExecution('DEBUG', 'Creating PICK Record', 'TRN_OPENTASK');

				//populating the fields
				customrecord.setFieldValue('name', vDoname);
				customrecord.setFieldValue('custrecord_ebiz_concept_po', Dono);
				customrecord.setFieldValue('custrecord_ebiz_sku_no', item);
				customrecord.setFieldValue('custrecord_sku', item);
				//customrecord.setFieldValue('custrecord_act_qty', cnfmqty);
				customrecord.setFieldValue('custrecord_expe_qty', parseFloat(cnfmqty).toFixed(5));
//				customrecord.setFieldValue('custrecord_lpno', 'PICK' + ebizWaveNo);
				customrecord.setFieldValue('custrecord_container_lp_no', 'PICK' + ebizWaveNo);
				customrecord.setFieldValue('custrecord_ebiz_lpno', 'PICK' + ebizWaveNo);
				customrecord.setFieldValue('custrecord_line_no', Lineno);
				customrecord.setFieldValue('custrecord_tasktype', '3'); //Pick Task
				customrecord.setFieldValue('custrecord_actbeginloc', vactLocation);
				customrecord.setFieldValue('custrecord_ebiz_cntrl_no', Dono);
				customrecord.setFieldValue('custrecord_ebiz_order_no', varsalesordInernId);
				customrecord.setFieldValue('custrecord_ebiz_receipt_no', Dono);
				customrecord.setFieldValue('custrecord_ebiz_wave_no',  ebizWaveNo);
				customrecord.setFieldValue('custrecordact_begin_date',DateStamp());
				customrecord.setFieldValue('custrecord_wms_status_flag', '9'); //statusflag--'G'
				customrecord.setFieldValue('custrecord_from_lp_no', LP);
				customrecord.setFieldValue('custrecord_actualbegintime', TimeStamp());
				customrecord.setFieldValue('custrecord_invref_no', Recid);
				customrecord.setFieldValue('custrecord_packcode', vpackcode);
				customrecord.setFieldValue('custrecord_sku_status', vskustatus);
				customrecord.setFieldValue('custrecord_uom_id', vuomid);
				customrecord.setFieldValue('custrecord_batch_no', vLotbatch);
				customrecord.setFieldValue('custrecord_ebizrule_no', vPickRule);
				customrecord.setFieldValue('custrecord_ebizmethod_no', vpicmethod);
				customrecord.setFieldValue('custrecord_ebizzone_no', vpickzone);
				customrecord.setFieldValue('custrecord_comp_id', vCompany);
				customrecord.setFieldValue('custrecord_wms_location', vlocation);						
				var currentContext = nlapiGetContext();  
				var currentUserID = currentContext.getUser();		
				customrecord.setFieldValue('custrecord_ebizuser', currentUserID);		
				customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);						
				customrecord.setFieldValue('custrecord_taskassignedto', currentUserID);

				//Pickarray
				var invtarray = new Array();
				invtarray[0] = vpicmethod;
				invtarray[1] = vpickzone;
				Pickarray.push(invtarray);
				nlapiLogExecution('DEBUG', 'Submitting PICK record', 'TRN_OPENTASK');

				//commit the record to NetSuite
				var recid = nlapiSubmitRecord(customrecord, false, true);
				nlapiSubmitRecord(transaction, false, true);
				nlapiSubmitRecord(doline, false, true);
				nlapiLogExecution('DEBUG', 'Done PICK Record Insertion :', 'Success');

			}
		}

	}
//	nlapiLogExecution('ERROR', 'actallocqty:',actallocqty);
//	nlapiLogExecution('ERROR', 'avlqty:',avlqty);
//	nlapiLogExecution('ERROR', 'Pickarray:',Pickarray);

	if ((avlqty - actallocqty) == 0) {
		nlapiLogExecution('ERROR', 'Into Break:');

		nlapiLogExecution('ERROR', 'Pickarray:',Pickarray.length);
		var vallocarr = new Array();
		vallocarr= actallocqty;
		Resultarray[0] = vallocarr;
		Resultarray[1] = Pickarray;
		return Resultarray;
	}
	var vallocarr = new Array();
	vallocarr = actallocqty;
	Resultarray[0] = vallocarr;
	Resultarray[1] = Pickarray;
	//return actallocqty;  
	return Resultarray;
}



/**
 * 
 * @param vsoid
 * @param ebizWaveNo
 */
function GenerateContLpNo(vsoid,ebizWaveNo){
	var columnsPicktsk = new Array();
	columnsPicktsk[0] = new nlobjSearchColumn('custrecord_ebizzone_no');
	columnsPicktsk[1] = new nlobjSearchColumn('custrecord_ebizmethod_no');
	columnsPicktsk[2] = new nlobjSearchColumn('name');
	columnsPicktsk[2].setSort();

	var filtersPicktsk = new Array();
	filtersPicktsk[0] = new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', ebizWaveNo);
	filtersPicktsk[1] = new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', vsoid);

	var searchresultsinvt = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersPicktsk, columnsPicktsk);
	var contLPNo ;
	nlapiLogExecution('ERROR', 'searchresultsinvt', searchresultsinvt);
	if (searchresultsinvt != null) {
		contLPNo = GetMaxLPNo('1', '2');
	}
	for (var s = 0; s < searchresultsinvt.length; s++) {		 
		var fields = new Array();
		var values = new Array();
		fields[0] = 'custrecord_ebiz_contlp_no';
		values[0] = contLPNo.toString();

		var updatefields = nlapiSubmitField('customrecord_ebiznet_trn_opentask', searchresultsinvt[s].getId(), fields, values);
	}   
}

function GetMaxClusterNo(){
	var vclusterNo = 1;
	var columnsclust = new Array();
	columnsclust[0] = new nlobjSearchColumn('custrecord_seqno');
	var filtersClust = new Array();
	filtersClust[0] = new nlobjSearchFilter('custrecord_trantype', null, 'is', 'CLUSTER');
	var searchresultsclust = nlapiSearchRecord('customrecord_ebiznet_wave', null, filtersClust, columnsclust);
	for (var t = 0; searchresultsclust != null && t < searchresultsclust.length; t++) {
		vclusterNo = searchresultsclust[t].getValue('custrecord_seqno');
		vclusterNo = parseFloat(vclusterNo) + 1;

		var fields = new Array();
		var values = new Array();
		fields[0] = 'custrecord_seqno';
		values[0] = vclusterNo;
		var updatefields = nlapiSubmitField('customrecord_ebiznet_wave', searchresultsclust[t].getId(), fields, values);
	}

	nlapiLogExecution('ERROR', 'GetMaxClusterNo', vclusterNo);
	return vclusterNo;
}

function kitpackageitemPickgen(ebizWaveNo,varsalesordInernId, itemNo, itemname, reqqty, vDoname, Dono, Lineno, vskustatus, vuomid, vpackcode,ItemType,vlocation,vCompany,vlotbatchno,vitemInfo1,vitemInfo2,vitemInfo3,Reportno){
	nlapiLogExecution('ERROR', 'kitpackageitemPickgen');
	var now = new Date();
	var vallocarr = new Array();
	var Resultarray = new Array();
	var Pickarray = new Array();

	//getting time

	var a_p = "";
	var d = new Date();
	var curr_hour = now.getHours();
	if (curr_hour < 12) {
		a_p = "am";
	}
	else {
		a_p = "pm";
	}
	if (curr_hour == 0) {
		curr_hour = 12;
	}
	if (curr_hour > 12) {
		curr_hour = curr_hour - 12;
	}

	var curr_min = now.getMinutes();

	curr_min = curr_min + "";

	if (curr_min.length == 1) {
		curr_min = "0" + curr_min;
	}
	nlapiLogExecution('ERROR', 'itemNo', itemNo);
	var searchresultsitem = nlapiLoadRecord('kititem', itemNo);
	var SkuNo = searchresultsitem.getFieldValue('itemid');
	nlapiLogExecution('ERROR', 'SkuNo', SkuNo);
	var filters = new Array();
//	filters.push(new nlobjSearchFilter('itemid', null, 'is', SkuNo));//kit_id is the parameter name
	filters.push(new nlobjSearchFilter('nameinternal', null, 'is', SkuNo));//kit_id is the parameter name
	// Changed On 30/4/12 by Suman
	filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
	// End of Changes as On 30/4/12
	
	var columns1 = new Array();
	columns1[0] = new nlobjSearchColumn('memberitem');
	columns1[1] = new nlobjSearchColumn('memberquantity');
	var searchresults = nlapiSearchRecord('item', null, filters, columns1);

	var arrretrun = new Array();
	var arritem = new Array();
	for (var i = 0; searchresults != null && i < searchresults.length; i++) {

		strItem = searchresults[i].getValue('memberitem');
		strItemtext = searchresults[i].getText('memberitem');
		strQty = searchresults[i].getValue('memberquantity');
		nlapiLogExecution('ERROR', 'memberitem', strItem);
		nlapiLogExecution('ERROR', 'memberitem Qty', strQty);
		var fields = ['custitem_item_family', 'custitem_item_group'];
		var columns = nlapiLookupField('inventoryitem', strItem, fields);
		var itemfamily = columns.custitem_item_family;
		var itemgroup = columns.custitem_item_group;
		avlqty = parseFloat(reqqty) * parseFloat(strQty);


		// var arryinvt = PickStrategieMain(strItem, itemfamily, itemgroup, avlqty,ItemType,vlocation,
		//		vlotbatchno,vskustatus,vitemInfo1,vitemInfo2,vitemInfo3,Reportno);
		var LocArray= getInventoryByPickStrategy(item, itemfamily, itemgroup, itemType, avlqty,
				vlocation, vlotbatchno, vskustatus, vitemInfo1, vitemInfo2, vitemInfo3, Reportno);
		var retqty = 0;
		if (arryinvt.length > 0) {

			nlapiLogExecution('DEBUG', 'Inside If=', i);
			for (j = 0; j < arryinvt.length; j++) {
				var invtarray = arryinvt[j];
				arrretrun.push(invtarray);
				nlapiLogExecution('ERROR', 'invtarray[0]', invtarray[0]);
				retqty = retqty + invtarray[0];
			}
		}
		else {
			//return reqqty;
			vallocarr = reqqty;
			Resultarray[0] = vallocarr;
			Resultarray[1] = Pickarray;	
			//return reqqty;
			return Resultarray;
		}

		if (parseFloat(retqty) != parseFloat(avlqty)) {
			reqqty = Math.floor(parseFloat(retqty) / parseFloat(strQty));

		}

		if (reqqty == 0) {

			vallocarr = reqqty;
			Resultarray[0] = vallocarr;
			Resultarray[1] = Pickarray;

			//return reqqty;
			return Resultarray;
		}

		var retval = new Array();
		retval[0] = strItem;
		retval[1] = parseFloat(strQty);
		retval[2] = strItemtext;
		arritem.push(retval);

	}
	//KTO task
	var ktotask = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
	nlapiLogExecution('DEBUG', 'Creating PICK Record', 'TRN_OPENTASK');
	nlapiLogExecution('ERROR', 'Opentask updation');
	//populating the fields
	ktotask.setFieldValue('name', vDoname);
	ktotask.setFieldValue('custrecord_ebiz_concept_po', Dono);
	/*   ktotask.setFieldValue('custrecord_ebiz_sku_no', itemNo);
                ktotask.setFieldValue('custrecord_sku', itemname);*/

	ktotask.setFieldValue('custrecord_ebiz_sku_no', itemNo);
	ktotask.setFieldValue('custrecord_sku', itemNo);

	//customrecord.setFieldValue('custrecord_act_qty', cnfmqty);
	ktotask.setFieldValue('custrecord_expe_qty', parseFloat(reqqty).toFixed(5));
//	ktotask.setFieldValue('custrecord_lpno', 'PICK' + ebizWaveNo);
	ktotask.setFieldValue('custrecord_container_lp_no', 'PICK' + ebizWaveNo);
	ktotask.setFieldValue('custrecord_ebiz_lpno', 'PICK' + ebizWaveNo);
	ktotask.setFieldValue('custrecord_line_no', Lineno);
	ktotask.setFieldValue('custrecord_tasktype', '6'); //Pick Task
	// ktotask.setFieldValue('custrecord_actbeginloc', vRetLocation);
	ktotask.setFieldValue('custrecord_ebiz_cntrl_no', Dono);
	ktotask.setFieldValue('custrecord_ebiz_order_no', varsalesordInernId);				
	ktotask.setFieldValue('custrecord_ebiz_receipt_no', Dono);				
	ktotask.setFieldValue('custrecord_ebiz_wave_no', ebizWaveNo);
	/* code added from FactoryMation on 28Feb13 by santosh
	as part of Datestamp */
	ktotask.setFieldValue('custrecordact_begin_date', DateStamp());
	/* upto here */
	//ktotask.setFieldValue('custrecord_wms_status_flag', '9'); //statusflag--'G'
	//  ktotask.setFieldValue('custrecord_from_lp_no', vfromLP);
	ktotask.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
	//  ktotask.setFieldValue('custrecord_invref_no', vinvtrecid);
	// ktotask.setFieldValue('custrecord_packcode', vpackcode);
	// ktotask.setFieldValue('custrecord_sku_status', vskustatus);
	// ktotask.setFieldValue('custrecord_uom_id', vuomid);
	//opentaskrecord.setFieldValue('custrecord_batch_no', vLotbatch);
	//  ktotask.setFieldValue('custrecord_parent_sku_no', itemNo);
	ktotask.setFieldValue('custrecord_comp_id', vCompany);			  
	var currentContext = nlapiGetContext();  
	var currentUserID = currentContext.getUser();		
	ktotask.setFieldValue('custrecord_ebizuser', currentUserID);

	//vCompany
	var retktoval = nlapiSubmitRecord(ktotask);


	nlapiLogExecution('ERROR', 'arritem.length', arritem.length);
	nlapiLogExecution('ERROR', 'arrretrun.length', arrretrun.length);


	var itemcnt = arritem.length;
	for (k = 0; k < arritem.length; k++) {
		var itemarray = arritem[k];
		var vitemno = itemarray[0];
		var vitemqty = parseFloat(itemarray[1]) * parseFloat(reqqty);
		var vmemberitemaname = itemarray[2];
		nlapiLogExecution('ERROR', 'vitemno', vitemno);
		nlapiLogExecution('ERROR', 'vitemqty', vitemqty);

		var vitemallocqty = 0;
		for (l = 0; l < arrretrun.length; l++) {
			var retinvarr = arrretrun[l];
			//nlapiLogExecution('ERROR', 'retinvarr[0]', retinvarr[0]);
			// nlapiLogExecution('ERROR', 'retinvarr[1]', retinvarr[1]);
			// nlapiLogExecution('ERROR', 'retinvarr[2]', retinvarr[2]);
			// nlapiLogExecution('ERROR', 'retinvarr[3]', retinvarr[3]);
			// nlapiLogExecution('ERROR', 'retinvarr[4]', retinvarr[4]);

			if (retinvarr[4] == vitemno) {
				if (parseFloat(vitemqty) <= 0) {
					break;
				}

				var vRetLocation = retinvarr[1];
				var vinvtrecid = retinvarr[3];
				var vfromLP = retinvarr[2];

				var vactallocqty = retinvarr[0];
				var pickqty = 0;
				if (parseFloat(vactallocqty) <= parseFloat(vitemqty)) {
					vitemqty = parseFloat(vitemqty) - parseFloat(vactallocqty);
					pickqty = parseFloat(vactallocqty);

				}
				else {
					pickqty = parseFloat(vitemqty);
					vitemqty = 0;

				}
				var vLotbatch=retinvarr[8];
				var vPickRule=retinvarr[7];
				var vpicmethod=retinvarr[6];
				var vpickzone=retinvarr[5];

				var opentaskrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
				nlapiLogExecution('DEBUG', 'Creating PICK Record', 'TRN_OPENTASK');
				nlapiLogExecution('ERROR', 'Opentask updation');
				//populating the fields
				opentaskrecord.setFieldValue('name', vDoname);
				opentaskrecord.setFieldValue('custrecord_ebiz_concept_po', Dono);
				opentaskrecord.setFieldValue('custrecord_ebiz_sku_no', vitemno);
				//opentaskrecord.setFieldValue('custrecord_sku', vmemberitemaname);
				opentaskrecord.setFieldValue('custrecord_sku', vitemno);
				//customrecord.setFieldValue('custrecord_act_qty', cnfmqty);
				opentaskrecord.setFieldValue('custrecord_expe_qty', parseFloat(pickqty).toFixed(5));
//				opentaskrecord.setFieldValue('custrecord_lpno', 'PICK' + ebizWaveNo);
				opentaskrecord.setFieldValue('custrecord_container_lp_no', 'PICK' + ebizWaveNo);
				opentaskrecord.setFieldValue('custrecord_ebiz_lpno', 'PICK' + ebizWaveNo);
				opentaskrecord.setFieldValue('custrecord_line_no', Lineno);
				opentaskrecord.setFieldValue('custrecord_tasktype', '3'); //Pick Task
				opentaskrecord.setFieldValue('custrecord_actbeginloc', vRetLocation);
				opentaskrecord.setFieldValue('custrecord_ebiz_cntrl_no', Dono);
				opentaskrecord.setFieldValue('custrecord_ebiz_order_no', varsalesordInernId);
				opentaskrecord.setFieldValue('custrecord_ebiz_receipt_no', Dono);

				opentaskrecord.setFieldValue('custrecord_ebiz_wave_no', ebizWaveNo);
				/* code added from FactoryMation on 28Feb13 by santosh
				as part of Datestamp */
				opentaskrecord.setFieldValue('custrecordact_begin_date', DateStamp());
				/* upto here */
				opentaskrecord.setFieldValue('custrecord_wms_status_flag', '9'); //statusflag--'G'
				opentaskrecord.setFieldValue('custrecord_from_lp_no', vfromLP);
				opentaskrecord.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
				opentaskrecord.setFieldValue('custrecord_invref_no', vinvtrecid);
				opentaskrecord.setFieldValue('custrecord_packcode', vpackcode);
				opentaskrecord.setFieldValue('custrecord_sku_status', vskustatus);
				opentaskrecord.setFieldValue('custrecord_uom_id', vuomid);
				//opentaskrecord.setFieldValue('custrecord_batch_no', vLotbatch);
				opentaskrecord.setFieldValue('custrecord_parent_sku_no', itemNo);
				opentaskrecord.setFieldValue('custrecord_kit_refno', retktoval);
				opentaskrecord.setFieldValue('custrecord_comp_id', vCompany);

				opentaskrecord.setFieldValue('custrecord_batch_no', vLotbatch);
				opentaskrecord.setFieldValue('custrecord_ebizrule_no', vPickRule);
				opentaskrecord.setFieldValue('custrecord_ebizmethod_no', vpicmethod);
				opentaskrecord.setFieldValue('custrecord_ebizzone_no', vpickzone);						
				var currentContext = nlapiGetContext();  
				var currentUserID = currentContext.getUser();		
				opentaskrecord.setFieldValue('custrecord_ebizuser', currentUserID);		
				opentaskrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);						
				opentaskrecord.setFieldValue('custrecord_taskassignedto', currentUserID);				

				var invtarray = new Array();
				invtarray[0] = vpicmethod;
				invtarray[1] = vpickzone;
				Pickarray.push(invtarray);		

				var currentContext = nlapiGetContext();  
				var currentUserID = currentContext.getUser();
				opentaskrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);		

				if (k == (itemcnt - 1)) {
					opentaskrecord.setFieldValue('custrecord_last_member_component', 'T');
				}

				nlapiLogExecution('ERROR', 'Inventory updation');
				var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', vinvtrecid);
				var invtallocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
				if (invtallocqty == null || isNaN(invtallocqty) || invtallocqty == "") {
					invtallocqty = 0;
				}

				transaction.setFieldValue('custrecord_ebiz_alloc_qty', (parseFloat(invtallocqty) + parseFloat(pickqty)).toFixed(5));

				nlapiSubmitRecord(transaction,false,true);
				nlapiLogExecution('ERROR', 'Inventory updation Completed');
				nlapiSubmitRecord(opentaskrecord,false,true);
				nlapiLogExecution('ERROR', 'Opentask updation Completed');

			}

		}

	}


	vallocarr = reqqty;
	Resultarray[0] = vallocarr;
	Resultarray[1] = Pickarray;

	return Resultarray;
}

/*
 * Function to retrieve the item dimension for the specified availQty
 * Logic:
 * Retrieve the item sku dimensions in descending order of PALLET, CASE, EACH
 * Determine the number of pallets, cases and/or eaches required to pick this available quantity
 */
function getItemDims(item, location, availQty)
{
	var itemDimensions = new Array();

	// Creating search criteria to retrieve item dims based on item and location
	var filterCriteria = new Array();
	filterCriteria.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', item, null));
	filterCriteria.push(new nlobjSearchFilter('custrecord_ebizsiteskudim', null, 'is', location, '@NONE@'));

	// Specifying the columns to return
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizqty');
	columns[0].setSort(true); // TRUE specifies a descending order sort
	columns[1] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim');

	// making a working copy of the available quantity
	var workingItemQty = availQty;

	// Retrieve search results
	var searchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filterCriteria, columns);
	if (searchResults != null){
		for(var r = 0; r < searchResults.length; r++) {
			var currentResult = searchResults[r];
			var itemQty = currentResult.getValue('custrecord_ebizqty');
			//nlapiLogExecution('ERROR', 'Dims Qty', currentResult.getValue('custrecord_ebizqty'));
			if (parseFloat(itemQty) <= parseFloat(workingItemQty)) {
				var remainder = Math.floor(parseFloat(workingItemQty)/parseFloat(itemQty));

				var dims = new Array();
				dims[0] = itemQty;
				dims[1] = currentResult.getId();
				dims[2] = parseFloat(remainder) * parseFloat(itemQty);
				dims[3] = currentResult.getValue('custrecord_ebizuomlevelskudim');

				// Determine the remaining quantity to pick
				workingItemQty = workingItemQty - (parseFloat(remainder) * parseFloat(itemQty));

				//nlapiLogExecution('ERROR', 'Remaining Qty to Pick', workingItemQty);

				// push the dimensions to the return array
				itemDimensions.push(dims);
			}
		}
	}
	nlapiLogExecution('ERROR', 'Item Dimensions', itemDimensions);

	return itemDimensions;
}

/*
 * Determine filters for getting the pick rules
 */
function getPickRuleFilters(item, itemGroup, itemFamily, location, uomLevel, itemStatus,
		itemInfo1, itemInfo2, itemInfo3){
	var pickRuleFilters = new Array();
	pickRuleFilters.push(new nlobjSearchFilter('custrecord_ebizskupickrul', null,
			'anyof', ['@NONE@', item]));
	pickRuleFilters.push(new nlobjSearchFilter('custrecord_ebizsitepickrule', null,
			'anyof', ['@NONE@', location]));

	if (itemGroup != null && itemGroup != "") {
		pickRuleFilters.push(new nlobjSearchFilter('custrecord_ebizskugrouppickrul', null, 
				'anyof', ['@NONE@', itemGroup]));
	}

	if (itemFamily != null && itemFamily != "") {
		pickRuleFilters.push(new nlobjSearchFilter('custrecord_ebizskufamilypickrul', null,
				'anyof', ['@NONE@', itemFamily]));
	}

	if (uomLevel != null && uomLevel != "") {
		nlapiLogExecution('ERROR', 'UOM Level', uomLevel);
		pickRuleFilters.push(new nlobjSearchFilter('custrecord_ebizuomlevelpickrul', null,
				'anyof', ['@NONE@', uomLevel]));
	}

	if (itemStatus != null && itemStatus != ""){
		nlapiLogExecution('ERROR', 'Item Status', itemStatus);
		pickRuleFilters.push(new nlobjSearchFilter('custrecord_ebizskustatspickrul', null,
				'anyof', ['@NONE@', itemStatus]));
	}

	if (itemInfo1 != null && itemInfo1 != "") {
		nlapiLogExecution('ERROR', 'Item Info 1', itemInfo1);
		pickRuleFilters.push(new nlobjSearchFilter('custrecord_ebizskuinfo1', null,
				'anyof', [ '@NONE@', itemInfo1 ]));
	}

	if (itemInfo2 != null && itemInfo2 != "") {
		nlapiLogExecution('ERROR', 'Item Info 2', itemInfo2);
		pickRuleFilters.push(new nlobjSearchFilter('custrecord_ebizskuinfo1', null,
				'anyof', [ '@NONE@', itemInfo2 ]));
	}

	if (itemInfo3 != null && itemInfo3 != "") {
		nlapiLogExecution('ERROR', 'Item Info 3', itemInfo3);
		pickRuleFilters.push(new nlobjSearchFilter('custrecord_ebizskuinfo1', null,
				'anyof', [ '@NONE@', itemInfo3 ]));
	}

	return pickRuleFilters;
}

/*
 * Get columns for searching pick rules
 */
function getPickRuleSearchColumns(){
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_ebizpickzonerul');
	columns[2] = new nlobjSearchColumn('custrecord_ebizpickmethod');
	columns[3] = new nlobjSearchColumn('custrecord_ebizsequencenopickrul');
	columns[4] = new nlobjSearchColumn('custrecord_allocation_strategy',
	'custrecord_ebizpickmethod');
	columns[5] = new nlobjSearchColumn('custrecord_ebizpickfacelocation',
	'custrecord_ebizpickmethod');
	return columns;
}

function generateReturnArrayRecord(confQtyForIteration, binLocn, lp, recordId, item, pickZone, pickMethod,
		pickRuleId, lotBatch){
	var invtArray = new Array();
	invtArray[0] = confQtyForIteration;
	invtArray[1] = binLocn;
	invtArray[2] = lp;
	invtArray[3] = recordId;
	invtArray[4] = item;
	invtArray[5] = pickZone;
	invtArray[6] = pickMethod;
	invtArray[7] = pickRuleId;
	invtArray[8] = lotBatch;
	invtArray[9] = "NORMAL";
	invtArray[10] = confQtyForIteration;
	return invtArray;
}

/*
 * Get inventory for location group
 */
function getInventoryForLocnGroup(item, locnGroupNo, location, itemType, lotOrBatchNo,allocStrategy){

//	nlapiLogExecution('ERROR','@ITEM',item);
//	nlapiLogExecution('ERROR','@locngroupno',locnGroupNo);
//	nlapiLogExecution('ERROR','@itemType',itemType);
//	nlapiLogExecution('ERROR','@lotorbatchno',lotOrBatchNo);
//	nlapiLogExecution('ERROR','@allocstarategy',allocStrategy);

	var inventoryFilter = new Array();
	inventoryFilter.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', item));
	//modified by satish.N
	inventoryFilter.push(new nlobjSearchFilter('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'anyof',
			['@NONE@', locnGroupNo]));
	inventoryFilter.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'is',
			location));

	if (itemType == "lotnumberedinventoryitem" &&
			lotOrBatchNo != null && lotOrBatchNo != "") {
		inventoryFilter.push(new nlobjSearchFilter('name', 'custrecord_ebiz_inv_lot', 'is', lotOrBatchNo));
		nlapiLogExecution('ERROR', 'vlocgroupno', lotOrBatchNo);
	}

	inventoryFilter.push(new nlobjSearchFilter('custrecord_ebiz_avl_qty', null, 'greaterthan', 0));

	var columnsInvt = new Array();
	columnsInvt[0] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columnsInvt[1] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columnsInvt[2] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columnsInvt[3] = new nlobjSearchColumn('custrecord_ebiz_avl_qty');
	// Specify sort criteria on allocation strategy	
	if (allocStrategy != null) {
		if (allocStrategy == 1) {
			columnsInvt[3].setSort(false); // ascending
		} else if (allocStrategy == 2) {
			columnsInvt[3].setSort(true); // descending
		}
	}

	columnsInvt[4] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_ebiz_inv_binloc');
	columnsInvt[4].setSort(false); // ascending
	columnsInvt[5] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');
	if(itemType == "lotnumberedinventoryitem") {
		columnsInvt[6] = new nlobjSearchColumn('custrecord_ebizexpirydate','custrecord_ebiz_inv_lot');
		columnsInvt[7] = new nlobjSearchColumn('custrecord_ebizfifodate','custrecord_ebiz_inv_lot');
		columnsInvt[7].setSort(false); // ascending
		columnsInvt[8] = new nlobjSearchColumn('name', 'custrecord_ebiz_inv_lot');
	}

	var inventorySearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, inventoryFilter, columnsInvt);

	return inventorySearchResults;
}


/**********************************************
 * This function splits the line
 * based on Item Dimensions for each items
 * sublist in a Sales Order.
 *
 */
function pickgen_palletisation(itemId,ordqty)
{
	nlapiLogExecution('ERROR', 'into pickgen_palletisation', itemId);
	nlapiLogExecution('ERROR', 'SKU', itemId);
	nlapiLogExecution('ERROR', 'QTY', ordqty);

	try {
		var skulist=new Array();
		skulist.push(itemId);
		var arrLPRequired=new Array();			
		var AllItemDims = getItemDimensions(skulist);
		var eachQty = 0;
		var caseQty = 0;
		var palletQty = 0;
		var noofPallets = 0;
		var noofCases = 0;
		var remQty = 0;
		var tOrdQty = ordqty;
		var eachpackflag="";
		var casepackflag="";
		var palletpackflag="";
		var eachweight="";
		var caseweight="";
		var palletweight="";
		var eachcube="";
		var casecube="";
		var palletcube="";

		//AllItemDims = [sku, uom, uomLevel, qty, baseUOMFlag, packflag,weight,cube];

		if (AllItemDims != null && AllItemDims.length > 0) 
		{	
			nlapiLogExecution('ERROR', 'AllItemDims.length', AllItemDims.length);
			for(var p = 0; p < AllItemDims.length; p++)
			{
				//var itemval = AllItemDims[p].getValue('custrecord_ebizitemdims');
				//var itemtext = AllItemDims[p].getText('custrecord_ebizitemdims');

				var itemval = AllItemDims[p][0];

				if(AllItemDims[p][0] == itemId)
				{
					nlapiLogExecution('ERROR', 'Item Dims configured for SKU: ', itemId);

					var skuDim = AllItemDims[p][2];
					var skuQty = AllItemDims[p][3];	
					var packflag = AllItemDims[p][5];	
					var weight = AllItemDims[p][6];
					var cube = AllItemDims[p][7];

//					nlapiLogExecution('ERROR', 'UOM Level : ', skuDim);
//					nlapiLogExecution('ERROR', 'UOM Qty : ', skuQty);
//					nlapiLogExecution('ERROR', 'UOM Pack Flag : ', packflag);
//					nlapiLogExecution('ERROR', 'Weight : ', weight);
//					nlapiLogExecution('ERROR', 'Cube : ', cube);

					//UOM LEVEL(Internal ID)  : 1-EACH(1)	5-CASE(2)	9-PALLET(3)

					if(skuDim == '1'){
						eachQty = skuQty;
						eachpackflag=packflag;
						eachweight = weight;
						eachcube = cube;
					}								
					else if(skuDim == '2'){
						caseQty = skuQty;
						casepackflag=packflag;
						caseweight = weight;
						casecube=cube;
					}								
					else if(skuDim == '3'){
						palletQty = skuQty;
						palletpackflag = packflag;
						palletweight=weight;
						palletcube=cube;
					}							
				}	
			}

			if(parseFloat(eachQty) == 0)
			{
				nlapiLogExecution('ERROR', 'UOM Dims', 'Item dim EACH is not configured for SKU: '+ itemId);
			}
			if(parseFloat(caseQty) == 0)
			{
				nlapiLogExecution('ERROR', 'UOM Dims', 'Item dim CASE is Not Configured for SKU: '+ itemId);
			}
			if(parseFloat(palletQty) == 0)
			{
				nlapiLogExecution('ERROR', 'UOM Dims', 'Item dim PALLET is not Configured for SKU: '+ itemId);
			}

			//Packflag : 1-Ship Cartons		2-Build Cartons		3-Ship Pallets

			if(eachQty>0 ||caseQty>0 || palletQty>0) 
			{
				var noofLpsRequired = 0;

				// Get the number of pallets required for this item (there will be one LP per pallet)
				noofPallets = getSKUDimCount(ordqty, palletQty);
				remQty = parseFloat(ordqty) - (noofPallets * palletQty);				

				var skuPltRecord=new Array();
				if(noofPallets > 0){
					if(palletpackflag==1 || palletpackflag==3)
					{
						noofLpsRequired = parseFloat(noofPallets);
						skuPltRecord = [Math.round(noofPallets), '9', palletQty, palletpackflag,palletweight,palletcube,palletQty];					
					}
					else
					{
						var palletTaskQty=Math.round(noofPallets * palletQty);
						noofPallets=1;
						noofLpsRequired = parseFloat(noofPallets);
						skuPltRecord = [Math.round(noofPallets), '9', palletTaskQty, palletpackflag,palletweight,palletcube,palletQty];
					}
				}
				else{						
					skuPltRecord = [Math.round(noofPallets), '9', palletQty, palletpackflag,palletweight,palletcube,palletQty];
				}
				arrLPRequired[0] = skuPltRecord ;

				nlapiLogExecution('ERROR', 'pickgen_palletisation:No. of Pallets', noofPallets);
				nlapiLogExecution('ERROR', 'pickgen_palletisation:Remaining Quantity', remQty);


				// check whether we need to break down into cases or not
				if (parseFloat(remQty) > 0 && parseFloat(caseQty) != 0) 
				{
					// Get the number of cases required for this item (only one LP for all cases)
					noofCases = getSKUDimCount(remQty, caseQty);
					remQty = parseFloat(remQty) - (noofCases * caseQty);

					var skuCaseRecord=new Array();

					if(noofCases > 0){
						noofLpsRequired = parseFloat(noofLpsRequired) + 1;

						if(casepackflag==1 || casepackflag==3)
						{
							skuCaseRecord = [Math.round(noofCases), '5', caseQty, casepackflag,caseweight,casecube,caseQty];							
						}
						else{
							var caseTaskQty=Math.round(noofCases * caseQty);
							noofCases=1;
							skuCaseRecord = [Math.round(noofCases), '5', caseTaskQty, casepackflag,caseweight,casecube,caseQty];
						}
					}
					else{						
						skuCaseRecord = [Math.round(noofCases), '5', caseQty, casepackflag,caseweight,casecube,caseQty];
					}

					arrLPRequired[1] = skuCaseRecord ;

					nlapiLogExecution('ERROR', 'pickgen_palletisation:No. of Cases', noofCases);
					nlapiLogExecution('ERROR', 'pickgen_palletisation:Remaining Quantity', remQty);
				}
				else{
					var skuCaseRecord = [Math.round(0), '5', caseQty, casepackflag,caseweight,casecube,caseQty];
					arrLPRequired[1] = skuCaseRecord ;					
				}

				// check whether we need to further break down into eaches or not (there will be one LP for all eaches)
				if (parseFloat(remQty) > 0)  {
					var skuEachRecord = [Math.round(1), '1', remQty, eachpackflag,eachweight,eachcube,eachQty];
					arrLPRequired[2] = skuEachRecord ;		
				}
			}
		} 
		else 
		{ 
			nlapiLogExecution('ERROR', 'UOM Dims', 'For all items, Dimensions are not configured');
			nlapiLogExecution('ERROR', 'Take Each as default UOM');
			//showInlineMessage(form, 'Error', 'Dimensions are not configured', "");
			//response.writePage(form);
			if (parseFloat(ordqty) > 0)  {

				var fields = ['weight'];
				var Itemweight = 0;
				var ItemCube = 0.001;

				if(itemId!=null && itemId!="")
				{
					var columns = nlapiLookupField('item', itemId, fields);
					if(columns!=null){
						Itemweight = columns.weight;
					}
				}


				var skuEachRecord = [Math.round(1), '1', ordqty, '',Itemweight,ItemCube,1];
				arrLPRequired[0] = skuEachRecord ;		
			}
		}		
	} 
	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in pickgen_palletisation:', exp);	
		return arrLPRequired;
	}
	nlapiLogExecution('ERROR', 'out of pickgen_palletisation', itemId);
	return arrLPRequired;
}

/**
 * 
 * @param orderQty
 * @param palletQty
 * @returns {Number}
 */
function getSKUDimCount(orderQty, dimQty){
	nlapiLogExecution('ERROR', 'into getSKUDimCount', '');
	nlapiLogExecution('ERROR', 'order qty ', orderQty);
	nlapiLogExecution('ERROR', 'dimensions qty ', dimQty);
	var retSKUDimCount = 0;

	if(dimQty > 0 && (parseFloat(orderQty) >= parseFloat(dimQty)))
	{
		skuDimCount = parseFloat(orderQty)/parseFloat(dimQty);
		retSKUDimCount = Math.floor(skuDimCount);
		if(retSKUDimCount == 0)
			retSKUDimCount = 1;
	}

	nlapiLogExecution('ERROR', 'out of getSKUDimCount', '');

	return retSKUDimCount;
}


/**
 * 
 * @param inventorySearchResults
 * @param fulfillOrderList
 * @param eBizWaveNo
 * @param allocConfigDtls
 */
function allocateQuantity(inventorySearchResults, fulfillOrderList, eBizWaveNo, allocConfigDtls)
{
	nlapiLogExecution('ERROR', 'into allocateQuantity', eBizWaveNo);
	var fulfilmentItem;
	var inventoryItem;
	var inventoryQuantity = 0;
	var allocationQuantity = 0;
	var recordId;
	var remainingQuantity = 0;
	var actualQty = 0; 
	var salesOrderList = new Array();
	var whLocation = "";
	var DOwhLocation = "";
	var fulfillmentItemStatus;
	// Maintaining the allocation by bin location
	var allocatedInv = new Array();
	var containerLP="";
	var cartonizationmethod="";
	var vnotes='';
	var wmsCarrier="";
	/*
	 *	For all fulfillment orders
	 *		If PF Location Flag is set, then allocate to PF Locations
	 *		Allocate remaining quantity to inventory search results
	 */
	try{
		//var containerslist=getAllContainers(0,0,'0');
		var containerslist=getAllContainersList();
		var prvSalesord='';
		var prvWMSCarrier='';
		var invtcontLP = "";
		var pfcontLP="";
		for (var i = 0; i < fulfillOrderList.length; i++){
			fulfilmentItem = fulfillOrderList[i][6];				// ITEM/SKU
			fulfilmentQuantity = fulfillOrderList[i][9];			// ORDER QUANTITY
			remainingQuantity = parseFloat(fulfilmentQuantity);		
			fulfilmentOrderNo = fulfillOrderList[i][3]; 			// ORDER NO.
			salesOrderNo = fulfillOrderList[i][1]; 					// SALES ORDER INTERNAL ID.
			fulfilmentOrderLineNo = fulfillOrderList[i][2];			// FULFILMENT ORDER LINE NO.
			fulfillmentItemStatus=fulfillOrderList[i][7];			// FULFILMENT Item Status.
			DOwhLocation = fulfillOrderList[i][19];					// FULFILMENT WAREHOUSE LOCATION
			wmsCarrier = fulfillOrderList[i][20];					// FULFILMENT WMS Carrier


			nlapiLogExecution('ERROR', 'fulfillmentItemStatus : ' , fulfillmentItemStatus);
			nlapiLogExecution('ERROR', 'fulfillment Warehouse Location : ' , DOwhLocation);
			nlapiLogExecution('ERROR', 'wmsCarrier : ' , wmsCarrier);

			var allocConfig = getAllocConfigForOrder(allocConfigDtls, i);
			if(allocConfig==null || allocConfig==''||allocConfig.lenth==0)
			{
				vnotes='Pick Strategy Error';
				nlapiLogExecution('ERROR', 'CallingFunction1', 'Sucess');
				createRecordInOpenTask(fulfillOrderList[i], remainingQuantity, eBizWaveNo, "", "", "",
						"", "", "", "", "", DOwhLocation, "","","","","","",vnotes,"");

				updateFulfilmentOrder(fulfilmentOrderNo, fulfilmentOrderLineNo, 0, eBizWaveNo);

			}
			else
			{

				//nlapiLogExecution('ERROR', 'allocConfig1 (pickStrategyId) : ', allocConfig[1]);
				nlapiLogExecution('ERROR', 'allocConfig2 (pickMethod) : ', allocConfig[2]);
				//nlapiLogExecution('ERROR', 'allocConfig3 (pickZone) : ', allocConfig[3]);
				//nlapiLogExecution('ERROR', 'allocConfig4 (Location Group) : ', allocConfig[4]);
				//nlapiLogExecution('ERROR', 'allocConfig5 (Location) : ' , allocConfig[5]);
				//nlapiLogExecution('ERROR', 'allocConfig12(Cartonization Method) : ' , allocConfig[12]);

				var pfLocationResults = allocConfig[7];
				var pickMethod = allocConfig[2];
				var pickZone = allocConfig[3];
				var pickStrategyId = allocConfig[1];
				var allocationStrategy = allocConfig[6];
				cartonizationmethod=allocConfig[12];
				nlapiLogExecution('ERROR', 'cartonizationmethod', cartonizationmethod);
				if(cartonizationmethod=="" && cartonizationmethod==null)
					cartonizationmethod='4';
				nlapiLogExecution('ERROR', 'cartonizationmethod', cartonizationmethod);
				var arrLPRequired = pickgen_palletisation(fulfilmentItem,fulfilmentQuantity);

				if(arrLPRequired==null || arrLPRequired==''||arrLPRequired.lenth==0)
				{
					vnotes='Missing Dimensions';
					nlapiLogExecution('ERROR', 'CallingFunction1', 'Sucess');
					createRecordInOpenTask(fulfillOrderList[i], remainingQuantity, eBizWaveNo, "", "", "",
							"", "", "", "", "", DOwhLocation, "","","","","","",vnotes,"");

					updateFulfilmentOrder(fulfilmentOrderNo, fulfilmentOrderLineNo, 0, eBizWaveNo);

				}
				else
				{
					var nooflps = "";
					var uomlevel = "";
					var lpbrkqty = "";
					var uompackflag = "";
					var uomweight="";
					var uomcube="";
					var uomqty="";

					for(s = 0;s < parseFloat(arrLPRequired.length);s++)
					{
						nlapiLogExecution('ERROR', 'arrLPRequired.length',arrLPRequired.length);
						nooflps = arrLPRequired[s][0];
						uomlevel = arrLPRequired[s][1];
						lpbrkqty = arrLPRequired[s][2];
						uompackflag = arrLPRequired[s][3];
						uomweight = arrLPRequired[s][4];
						uomcube= arrLPRequired[s][5];
						uomqty = arrLPRequired[s][6];

						for(t=1; t<=parseFloat(nooflps);t++)
						{

							nlapiLogExecution('ERROR', 'nooflps',nooflps);
							nlapiLogExecution('ERROR', 'lpbrkqty',lpbrkqty);
							nlapiLogExecution('ERROR', 'uomlevel',uomlevel);
							remainingQuantity = parseFloat(lpbrkqty);

							if (pfLocationResults != null && pfLocationResults.length > 0)
							{
								for(var z = 0; z < pfLocationResults.length; z++)
								{
									if(parseFloat(remainingQuantity) > 0)
									{
										//nlapiLogExecution('ERROR', 'Within PF Location Result search');
										//nlapiLogExecution('ERROR', 'remainingQuantity', remainingQuantity);

										var pfItem = pfLocationResults[z].getValue('custrecord_pickfacesku');
										var pfSite = pfLocationResults[z].getValue('custrecord_pickface_location');
										if (pfItem == fulfilmentItem && pfSite == DOwhLocation)
										{
											nlapiLogExecution('ERROR', 'item Match',pfItem);
											pfBinLocationId = pfLocationResults[z].getValue('custrecord_pickbinloc');
											pfMaximumQuantity = pfLocationResults[z].getValue('custrecord_maxqty');
											pfMaximumPickQuantity = pfLocationResults[z].getValue('custrecord_maxpickqty');

											if(parseFloat(pfMaximumPickQuantity) > parseFloat(remainingQuantity))
											{

												var availableQty ="";
												var allocQty = "";
												var recordId = "";
												var packCode = "";
												var itemStatus = "";
												var fromLP = "";
												var inventoryItem="";
												var fromLot="";
												// Get inventory details for this pickface location
												var binLocnInvDtls = getAvailQtyFromInventory(inventorySearchResults, pfBinLocationId, allocatedInv,DOwhLocation,fulfilmentItem);

												if(binLocnInvDtls!=null && binLocnInvDtls.length>0)
												{
													availableQty = binLocnInvDtls[0][2];
													allocQty = binLocnInvDtls[0][3];
													recordId = binLocnInvDtls[0][8];
													packCode = binLocnInvDtls[0][4];
													itemStatus = binLocnInvDtls[0][5];										
													whLocation = binLocnInvDtls[0][6];
													fromLP = binLocnInvDtls[0][7];											
													inventoryItem = binLocnInvDtls[0][9];
													fromLot = binLocnInvDtls[0][10];
													nlapiLogExecution('ERROR', 'binLocnInvDtls[0][10] For Batch',binLocnInvDtls[0][10]);

												}


												/*
												 * If the allocation strategy is not null check in inventory for that item.						 
												 * 		if the allocation strategy = min quantity on the pallet
												 * 			search for the inventory where the inventory has the least quantity
												 * 		else
												 * 			search for the inventory where the inventory has the maximum quantity 
												 * 			on the pallet 
												 */
												nlapiLogExecution('ERROR', 'Inventory Item Status : ', itemStatus);
												nlapiLogExecution('ERROR', 'Fulfillment Item Status : ', fulfillmentItemStatus);
												if(parseFloat(availableQty) > 0 && itemStatus== fulfillmentItemStatus)
												{
													// allocate to this bin location
													var actualAllocQty = Math.min(parseFloat(availableQty), parseFloat(remainingQuantity));
													var newAllocQty = parseFloat(allocQty) + parseFloat(actualAllocQty);
													remainingQuantity = parseFloat(remainingQuantity) - parseFloat(actualAllocQty);

													var expectedQuantity = parseFloat(actualAllocQty);

													var contsize="";								
													var totalweight = (expectedQuantity*uomweight)/parseFloat(uomqty);
													var totalcube = (expectedQuantity*uomcube)/parseFloat(uomqty);


													//nlapiLogExecution('ERROR', 'actualAllocQty', actualAllocQty);
													//nlapiLogExecution('ERROR', 'newAllocQty', newAllocQty);
													//nlapiLogExecution('ERROR', 'remainingQuantity', remainingQuantity);

													// Allocate the quantity to the pickface location if the allocation quantity is 
													// 	less than or equal to the pick face maximum quantity
													if(actualAllocQty <= pfMaximumPickQuantity)
													{
														//1-Ship Cartons	2-Build Cartons		3-Ship Pallets
														if((uompackflag=="1" || uompackflag=="3") &&(parseFloat(expectedQuantity) == parseFloat(uomqty)))
														{	
															pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);

															for(l=0;l<containerslist.length;l++){                                         
																var containername = containerslist[l].getValue('custrecord_containername');

																if(containername=='SHIPASIS')
																	contsize = containerslist[l].getValue('Internalid');												           
															}

															nlapiLogExecution('ERROR', 'Container Size in Pick Face : ', contsize);
															CreateMasterLPRecord(pfcontLP,contsize,totalweight,totalcube,uompackflag,DOwhLocation);
														}
														else if(cartonizationmethod=='4')
														{
															if(prvSalesord!=salesOrderNo)
															{
																pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																prvSalesord=salesOrderNo; 
																CreateMasterLPRecord(pfcontLP,contsize,totalweight,totalcube,uompackflag,DOwhLocation);
																prvWMSCarrier=wmsCarrier;
															}
															else
															{
																nlapiLogExecution('ERROR', 'prvWMSCarrier : ', prvWMSCarrier);
																nlapiLogExecution('ERROR', 'wmsCarrier : ', wmsCarrier);
																if(prvWMSCarrier != wmsCarrier)
																{	
																	pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																	CreateMasterLPRecord(pfcontLP,contsize,totalweight,totalcube,uompackflag);
																	prvWMSCarrier = wmsCarrier;
																}
															}

														}

														nlapiLogExecution('ERROR', 'Container LP in Pick Face : ', pfcontLP);

														// Update custom records
														//updateInventoryWithAllocation(recordId, newAllocQty);

														updateInventoryWithAllocation(recordId, actualAllocQty);

														createRecordInOpenTask(fulfillOrderList[i], expectedQuantity, eBizWaveNo, pfcontLP, 
																pfBinLocationId, recordId, packCode, itemStatus, pickMethod, pickZone, pickStrategyId,
																whLocation, fromLP,uomlevel,totalweight,totalcube,contsize,fromLot,'',wmsCarrier);
														updateFulfilmentOrder(fulfilmentOrderNo, fulfilmentOrderLineNo, actualAllocQty, eBizWaveNo);

														// Maintain the allocation temporarily
														var allocatedInvRow = [pfBinLocationId, expectedQuantity,fromLP,inventoryItem,fromLot];
														allocatedInv.push(allocatedInvRow);
													}
												}

											}
										}
									}
								}
							}//

							if(inventorySearchResults != null && inventorySearchResults.length > 0)
							{
								nlapiLogExecution('ERROR', 'inventorySearchResults.length : ', inventorySearchResults.length);
								for (var j = 0; j < inventorySearchResults.length; j++)
								{
									inventoryItem = inventorySearchResults[j].getValue('custrecord_ebiz_inv_sku');
									var InvWHLocation = inventorySearchResults[j].getValue('custrecord_ebiz_inv_loc');
//									nlapiLogExecution('ERROR', 'Inventory Warehouse Location', InvWHLocation);
//									nlapiLogExecution('ERROR', 'Fulfillment Warehouse Location', DOwhLocation);
//									nlapiLogExecution('ERROR', 'Inventory Item Location', inventoryItem);
//									nlapiLogExecution('ERROR', 'Fulfillment Item Location', fulfilmentItem);
//									nlapiLogExecution('ERROR', 'remainingQuantity', remainingQuantity);

									if ((DOwhLocation==InvWHLocation)&&(inventoryItem == fulfilmentItem) && (parseFloat(remainingQuantity) > 0))
									{
										actualQty = inventorySearchResults[j].getValue('custrecord_ebiz_qoh');
										inventoryAvailableQuantity = inventorySearchResults[j].getValue('custrecord_ebiz_avl_qty');
										allocationQuantity = inventorySearchResults[j].getValue('custrecord_ebiz_alloc_qty');
										recordId = inventorySearchResults[j].getId();
										beginLocationId = inventorySearchResults[j].getValue('custrecord_ebiz_inv_binloc');
										packCode = inventorySearchResults[j].getValue('custrecord_ebiz_inv_packcode');
										itemStatus = inventorySearchResults[j].getValue('custrecord_ebiz_inv_sku_status');
										whLocation = inventorySearchResults[j].getValue('custrecord_ebiz_inv_loc');
										fromLP = inventorySearchResults[j].getValue('custrecord_ebiz_inv_lp');
										fromLot = inventorySearchResults[j].getText('custrecord_ebiz_inv_lot');
										nlapiLogExecution('ERROR', 'fromLot If : ', fromLot);


										// Get already allocated inv
										var alreadyAllocQty = getAlreadyAllocatedInv(allocatedInv, beginLocationId,fromLP,inventoryItem,fromLot);

										nlapiLogExecution('ERROR', 'alreadyAllocQty : ', alreadyAllocQty);

										// Adjust for already allocated inv
										inventoryAvailableQuantity = parseFloat(inventoryAvailableQuantity) - parseFloat(alreadyAllocQty);
										nlapiLogExecution('ERROR', 'Inventory Item Status : ', itemStatus);
										nlapiLogExecution('ERROR', 'Fulfillment Item Status : ', fulfillmentItemStatus);
										nlapiLogExecution('ERROR', 'inventory Available Quantity: ', inventoryAvailableQuantity);
										nlapiLogExecution('ERROR', 'remainingQuantity: ', remainingQuantity);
										nlapiLogExecution('ERROR', 'cartonizationmethod at last', cartonizationmethod);

										if(parseFloat(inventoryAvailableQuantity) > 0 && itemStatus==fulfillmentItemStatus){
											var actualAllocationQuantity = Math.min(parseFloat(inventoryAvailableQuantity), parseFloat(remainingQuantity));
											allocationQuantity = parseFloat(alreadyAllocQty) + parseFloat(actualAllocationQuantity);
											remainingQuantity = parseFloat(remainingQuantity) - parseFloat(actualAllocationQuantity);
											var expectedQuantity = parseFloat(actualAllocationQuantity);

											var contsize="";	

											var totalweight = (expectedQuantity*uomweight)/parseFloat(uomqty);
											var totalcube = (expectedQuantity*uomcube)/parseFloat(uomqty);

											//var totalweight = expectedQuantity*uomweight;
											//var totalcube = expectedQuantity*uomcube;

											nlapiLogExecution('ERROR', 'uompackflag : ', uompackflag);
											nlapiLogExecution('ERROR', 'expectedQuantity : ', expectedQuantity);
											nlapiLogExecution('ERROR', 'allocationQuantity : ', allocationQuantity);
											nlapiLogExecution('ERROR', 'uomqty : ', uomqty);
											nlapiLogExecution('ERROR', 'cartonizationmethod : ', cartonizationmethod);
											nlapiLogExecution('ERROR', 'prvSalesord : ', prvSalesord);
											nlapiLogExecution('ERROR', 'salesOrderNo : ', salesOrderNo);
											//1-Ship Cartons	2-Build Cartons		3-Ship Pallets
											if((uompackflag=="1" || uompackflag=="3")&&(parseFloat(expectedQuantity) == parseFloat(uomqty))) 
											{
												invtcontLP = GetMaxLPNo('1', '2',DOwhLocation);

												for(l=0;l<containerslist.length;l++)
												{                                         
													var containername = containerslist[l].getValue('custrecord_containername');
													if(containername=='SHIPASIS')
														contsize = containerslist[l].getValue('Internalid');												           
												}

												CreateMasterLPRecord(invtcontLP,contsize,totalweight,totalcube,uompackflag);
											}
											else if(cartonizationmethod=='4') 
											{
												if(prvSalesord!=salesOrderNo)
												{													
													invtcontLP = GetMaxLPNo('1', '2',DOwhLocation);
													prvSalesord=salesOrderNo;	
													CreateMasterLPRecord(invtcontLP,contsize,totalweight,totalcube,uompackflag);
													prvWMSCarrier=wmsCarrier;
												}
												else
												{
													nlapiLogExecution('ERROR', 'prvWMSCarrier1 : ', prvWMSCarrier);
													nlapiLogExecution('ERROR', 'wmsCarrier1 : ', wmsCarrier);
													if(prvWMSCarrier != wmsCarrier)
													{	
														invtcontLP = GetMaxLPNo('1', '2',DOwhLocation);
														CreateMasterLPRecord(invtcontLP,contsize,totalweight,totalcube,uompackflag);
														prvWMSCarrier = wmsCarrier;
													}
												}

											}

											nlapiLogExecution('ERROR', 'Container LP in Inventory : ', invtcontLP);

											//updateInventoryWithAllocation(recordId, allocationQuantity);
											updateInventoryWithAllocation(recordId, actualAllocationQuantity);

											createRecordInOpenTask(fulfillOrderList[i], expectedQuantity, eBizWaveNo, invtcontLP, beginLocationId, recordId,
													packCode, itemStatus, pickMethod, pickZone, pickStrategyId, whLocation, fromLP,uomlevel,
													totalweight,totalcube,contsize,fromLot,'',wmsCarrier);
											updateFulfilmentOrder(fulfilmentOrderNo, fulfilmentOrderLineNo, actualAllocationQuantity, eBizWaveNo);

											// Maintain the allocation temporarily
											var allocatedInvRow = [beginLocationId, expectedQuantity,fromLP,inventoryItem,fromLot];
											allocatedInv.push(allocatedInvRow);

										}
									}
								}
							}

							//If there is still some quantity remaining, then insert a failed task
							if(parseFloat(remainingQuantity) > 0)
							{
								vnotes='Insufficient Inventory';
								nlapiLogExecution('ERROR', 'CallingFunction1', 'Sucess');
								createRecordInOpenTask(fulfillOrderList[i], remainingQuantity, eBizWaveNo, "", "", "",
										"", "", "", "", "", DOwhLocation, "",uomlevel,"","","","",vnotes,"");
								updateFulfilmentOrder(fulfilmentOrderNo, fulfilmentOrderLineNo, 0, eBizWaveNo);

							}
						}
					}
				}
			}
		}

		ActualCartonization(fulfillOrderList,allocConfigDtls,containerslist,uompackflag);

		setClusterNo(eBizWaveNo);
		try
		{
			GenerateChildLabel("","",eBizWaveNo,uompackflag);
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR', 'Exception in UccLabelGeneration: ',exp);	

		}
	}
	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in allocateQuantity : ', exp);		
	}

	nlapiLogExecution('ERROR', 'out of allocateQuantity ', eBizWaveNo);
}
/**
 * 
 * @param fulfillOrderList
 * @param allocConfigDtls
 */
function ActualCartonization(fulfillOrderList,allocConfigDtls,containerslist,uompackflag)
{
	try {

		var distinctorderslist = new Array();	
		var distinctorders = new Array();	
		var allocConfig = new Array();
		var wmsCarrier='';
		for (var n = 0; n < fulfillOrderList.length; n++){

			var EbizOrderNo = fulfillOrderList[n][1]; 
			nlapiLogExecution('ERROR', 'Ebiz Order Number', EbizOrderNo);

			allocConfig = getAllocConfigForOrder(allocConfigDtls, n);

			//2-Pick Method, 12-Cartonization, 3-Pick Zone 
			var currentRow = [EbizOrderNo, allocConfig[2],allocConfig[12],allocConfig[3]];

			distinctorderslist[n] = currentRow;
		}

		var distinctorders = getDistinctMethodsList(distinctorderslist);		
		var prvWMSCarrier='';

		for (var m = 0; m < distinctorders.length; m++){
			nlapiLogExecution('ERROR', 'Distinct Ebiz Order Number', distinctorders[m][0]);
			nlapiLogExecution('ERROR', 'Distinct Ebiz Method No', distinctorders[m][1]);
			nlapiLogExecution('ERROR', 'Distinct Cartonization Method No', distinctorders[m][2]);
			var ebizorderno = distinctorders[m][0];
			var ebizmethodno = distinctorders[m][1];
			var cartonizationmethod = distinctorders[m][2];

			wmsCarrier = distinctorders[m][20];					// FULFILMENT WMS Carrier
			nlapiLogExecution('ERROR', 'wmsCarrier : ' , wmsCarrier);
			if(cartonizationmethod==null || cartonizationmethod=="")					
				cartonizationmethod='4'; // By Order

			nlapiLogExecution('ERROR', 'cartonizationmethod', cartonizationmethod);
			if(cartonizationmethod!='4') 
			{
				var opentasklist = getTaskDetailsforCartonization(ebizorderno,ebizmethodno);
				nlapiLogExecution('ERROR', 'opentasklist', opentasklist);
				if(opentasklist != null){

					var totaltaskweight="0";
					var totaltaskcube="0";
					var contsize = "";			
					var whsite="";

					for(i=0;i<opentasklist.length;i++){
						totaltaskweight = parseFloat(totaltaskweight)+parseFloat(opentasklist[i].getValue('custrecord_total_weight'));
						totaltaskcube = parseFloat(totaltaskcube)+parseFloat(opentasklist[i].getValue('custrecord_totalcube'));						
						whsite = opentasklist[i].getValue('custrecord_wms_location');
					}	

					//var containerslist = getAllContainers(totaltaskweight,totaltaskcube,cartonizationmethod);
					if(containerslist != null && containerslist.length>0){
						contsize = getContainerSize(containerslist,totaltaskcube,totaltaskweight,cartonizationmethod);
						nlapiLogExecution('ERROR', 'whsite : ', whsite);
						var containerLP1 = GetMaxLPNo('1', '2',whsite);
						nlapiLogExecution('ERROR', 'Container LP in Cube(Total Order) : ', containerLP1);
						CreateMasterLPRecord(containerLP1,contsize,totaltaskweight,totaltaskcube,uompackflag,whsite);
						for(j=0;j<opentasklist.length;j++){
							var internalid = opentasklist[j].getId();							
							updatesizeidinopentask(internalid,containerLP1,contsize);
						}
					}	
					else{
						for(k=0;k<opentasklist.length;k++){
							totaltaskweight = parseFloat(opentasklist[k].getValue('custrecord_total_weight'));
							totaltaskcube = parseFloat(opentasklist[k].getValue('custrecord_totalcube'));
							whsite = opentasklist[k].getValue('custrecord_wms_location');
							//var containerslist=getAllContainers(totaltaskweight,totaltaskcube,cartonizationmethod);
							if(containerslist != null && containerslist.length>0){	
								contsize = getContainerSize(containerslist,totaltaskcube,totaltaskweight,cartonizationmethod);
								var containerLP2 = GetMaxLPNo('1', '2',whsite);
								nlapiLogExecution('ERROR', 'Container LP in Cube(Total Task) : ', containerLP2);
								var internalid = opentasklist[k].getId();
								CreateMasterLPRecord(containerLP2,contsize,totaltaskweight,totaltaskcube,uompackflag,whsite);
								updatesizeidinopentask(internalid,containerLP2,contsize);
							}
							else{
								if(cartonizationmethod=="1"){
									buildcartonsforcubeandweight(opentasklist[k],containerslist,uompackflag);
								}
								else if(cartonizationmethod=="2"){
									buildcartonsforweight(opentasklist[k],containerslist,uompackflag);
								} 
								else if(cartonizationmethod=="3"){
									buildcartonsforcube(opentasklist[k],containerslist,uompackflag);
								}								
							}					
						}
					}
				}
			}

		}//distinct orders loop		
	}
	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in ActualCartonization : ', exp);		
	}
}


/**
 * 
 * @param vEbizOrdNo
 * @returns {Array}
 */
function getTaskDetailsforCartonization(vEbizOrdNo,vEbizMethodNo)
{
	nlapiLogExecution('ERROR', 'into getTaskDetailsforCartonization (Ord No)', vEbizOrdNo);
	nlapiLogExecution('ERROR', 'into getTaskDetailsforCartonization (Method No)', vEbizMethodNo);

	var opentasks = new Array();

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', vEbizOrdNo));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
	filters.push(new nlobjSearchFilter('custrecord_container', null, 'anyof', ['@NONE@']));
	filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'noneof', ['@NONE@']));
	filters.push(new nlobjSearchFilter('custrecord_ebizmethod_no', null, 'is', vEbizMethodNo));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[1] = new nlobjSearchColumn('custrecord_total_weight');
	columns[2] = new nlobjSearchColumn('custrecord_totalcube');	
	columns[3] = new nlobjSearchColumn('name');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[5] = new nlobjSearchColumn('custrecord_sku');
	columns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[7] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
	columns[8] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[9] = new nlobjSearchColumn('custrecord_line_no');
	columns[10] = new nlobjSearchColumn('custrecord_lpno');
	columns[11] = new nlobjSearchColumn('custrecord_packcode');
	columns[12] = new nlobjSearchColumn('custrecord_sku_status');
	columns[13] = new nlobjSearchColumn('custrecord_wms_status_flag');
	columns[14] = new nlobjSearchColumn('custrecord_tasktype');	
	columns[15] = new nlobjSearchColumn('custrecord_uom_id');
	columns[16] = new nlobjSearchColumn('custrecord_uom_level');
	columns[17] = new nlobjSearchColumn('custrecord_ebiz_receipt_no');
	columns[18] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[19] = new nlobjSearchColumn('custrecord_invref_no');	
	columns[20] = new nlobjSearchColumn('custrecord_from_lp_no');
	columns[21] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[22] = new nlobjSearchColumn('custrecord_wms_location');
	columns[23] = new nlobjSearchColumn('custrecord_ebizrule_no');	
	columns[24] = new nlobjSearchColumn('custrecord_ebizmethod_no');
	columns[25] = new nlobjSearchColumn('custrecord_ebizzone_no');
	//columns[26] = new nlobjSearchColumn('custrecord_cartonization_method','custrecord_ebizmethod_no');


	opentasks = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	

	nlapiLogExecution('ERROR', 'out of getTaskDetailsforCartonization', '');

	return opentasks;
}


/**
 * 
 * @param totalweight
 * @param totalcube
 * @param cartonizationmethod
 * @returns {Array}
 */
function getAllContainers(totalweight,totalcube,cartonizationmethod)
{
	nlapiLogExecution('ERROR', 'into getAllContainers', '');
	nlapiLogExecution('ERROR', 'cartonizationmethod ', cartonizationmethod);

	var containerslist = new Array();

	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_containertype', null, 'anyof', ['1','4']));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(cartonizationmethod!='0'){
		nlapiLogExecution('ERROR', 'cartonizationmethod in filter ', cartonizationmethod);
		filters.push(new nlobjSearchFilter('custrecord_containername', null, 'isnot', 'SHIPASIS'));	
	}

	if(cartonizationmethod=='1'){
		filters.push(new nlobjSearchFilter('custrecord_maxweight', null, 'greaterthanorequalto', totalweight));
		filters.push(new nlobjSearchFilter('custrecord_cubecontainer', null, 'greaterthanorequalto', totalcube));		
	}	
	else if(cartonizationmethod=='2'){
		filters.push(new nlobjSearchFilter('custrecord_maxweight', null, 'greaterthanorequalto', totalweight));	
	}
	else{
		filters.push(new nlobjSearchFilter('custrecord_cubecontainer', null, 'greaterthanorequalto', totalcube));	
	}	


	var columns = new Array();
	columns[0] = new nlobjSearchColumn('Internalid');
	columns[1] = new nlobjSearchColumn('custrecord_maxweight');
	columns[2] = new nlobjSearchColumn('custrecord_cubecontainer');
	columns[3] = new nlobjSearchColumn('custrecord_tareweight');
	columns[4] = new nlobjSearchColumn('custrecord_containername');
	columns[5] = new nlobjSearchColumn('custrecord_packfactor');
	if(parseFloat(totalcube)==0){
		columns[2].setSort(true);
	}
	else{
		columns[2].setSort(false);
	}

	containerslist = nlapiSearchRecord('customrecord_ebiznet_container', null, filters, columns);

	nlapiLogExecution('ERROR', 'out of getAllContainers', '');

	return containerslist;
}


function getAllContainersList()
{
	nlapiLogExecution('ERROR', 'into getAllContainersList', '');

	var containerslist = new Array();

	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_containertype', null, 'anyof', ['1','4']));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('Internalid');
	columns[1] = new nlobjSearchColumn('custrecord_maxweight');
	columns[2] = new nlobjSearchColumn('custrecord_cubecontainer');
	columns[3] = new nlobjSearchColumn('custrecord_tareweight');
	columns[4] = new nlobjSearchColumn('custrecord_containername');
	columns[5] = new nlobjSearchColumn('custrecord_packfactor');
	columns[2].setSort(false);
	columns[3].setSort(false);

	containerslist = nlapiSearchRecord('customrecord_ebiznet_container', null, filters, columns);

	nlapiLogExecution('ERROR', 'out of getAllContainersList', '');

	return containerslist;
}


/**
 * 
 * @param opentasklist
 * @param cartonizationmethod
 */
function buildcartonsforcube(opentasklist,containerslist,uompackflag)
{
	nlapiLogExecution('ERROR', 'into buildcartonsforcube Task Split', '');
	try { 
		var totaltaskweight = parseFloat(opentasklist.getValue('custrecord_total_weight'));
		var totaltaskcube = parseFloat(opentasklist.getValue('custrecord_totalcube'));	
		var whsite=opentasklist.getValue('custrecord_wms_location');
		nlapiLogExecution('ERROR', 'Total Task Weight', totaltaskweight);
		nlapiLogExecution('ERROR', 'Total Task Cube', totaltaskcube);
		var remcube = parseFloat(totaltaskcube);
		var remweight = parseFloat(totaltaskweight);
		var contsize="";
		var containerLP="";
		var cube="";
		var weight="";
		var exptdqty=0;

		var uomlevel = opentasklist.getValue('custrecord_uom_level');
		var ebizskuno = opentasklist.getValue('custrecord_ebiz_sku_no');
		var uomcube = getUOMCube(uomlevel,ebizskuno);

		//var containerslist=getAllContainers(0,0,cartonizationmethod);

		if(containerslist != null){ 
			//for(l=0;l<containerslist.length;l++){  
			for(l=(containerslist.length)-1;l>=0;l--){                                        
				var containercube = containerslist[l].getValue('custrecord_cubecontainer');
				var packfactor = containerslist[l].getValue('custrecord_packfactor');
				if(packfactor!=null && packfactor!='')
				{
					containercube = parseFloat(containercube)-(parseFloat(containercube)*(parseFloat(packfactor)/100));
				}
				nlapiLogExecution('ERROR', 'Container Cube  ',containercube);
				nlapiLogExecution('ERROR', 'SKU Uom Cube  ',uomcube);
				exptdqty = Math.floor(parseFloat(containercube)/parseFloat(uomcube));
				nlapiLogExecution('ERROR', 'exptdqty  ',exptdqty);
				while (parseFloat(remcube)> 0 && parseFloat(remcube)>= parseFloat(containercube) && (exptdqty>0)){
					contsize =  containerslist[l].getValue('Internalid');
					containerLP = GetMaxLPNo('1', '2',whsite);
					cube = containerslist[l].getValue('custrecord_cubecontainer');
					weight = containerslist[l].getValue('custrecord_maxweight');
					CreateMasterLPRecord(containerLP,contsize,weight,cube,uompackflag,whsite);
					nlapiLogExecution('ERROR', 'containerLP1 in buildcartonsforcube ', containerLP);
					createNewPickTask(opentasklist,containerLP,cube,weight,contsize,exptdqty);
					remcube = parseFloat(remcube) - parseFloat(cube);
					remweight = parseFloat(remweight) - parseFloat(weight);
					nlapiLogExecution('ERROR', 'Task Remaining Cube', remcube);
				}
			}

			//if still there is some qty exists and the cube is less than the container(s) cube(no matching container),
			//then place it in the smallest container.
			if(parseFloat(remcube)> 0){

				nlapiLogExecution('ERROR', 'Task Remaining Cube', remcube);

				exptdqty = Math.floor(parseFloat(remcube)/parseFloat(uomcube));

				nlapiLogExecution('ERROR', 'exptdqty  ',exptdqty);

				if(parseFloat(exptdqty)>0)
				{
					var k = containerslist.length;
					contsize =  containerslist[k-1].getValue('Internalid');
					containerLP = GetMaxLPNo('1', '2',whsite);				   
					cube = containerslist[k-1].getValue('custrecord_cubecontainer');
					weight = containerslist[k-1].getValue('custrecord_maxweight');	
					CreateMasterLPRecord(containerLP,contsize,weight,cube,uompackflag,whsite);
					nlapiLogExecution('ERROR', 'containerLP2 in buildcartonsforcube ', containerLP);
					createNewPickTask(opentasklist,containerLP,remcube,remweight,contsize,exptdqty);
				}
			}
			if(parseFloat(exptdqty)>0)
			{
				//delete the actual record
				var id = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', opentasklist.getId());
				nlapiLogExecution('ERROR', 'Deleted Actual Record : ', id);	
			}
		}
	}
	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in buildcartonsforcube : ', exp);		
	}

	nlapiLogExecution('ERROR', 'out of  buildcartonsforcube Task Split', '');
}


/**
 * 
 * @param opentasklist
 * @param cartonizationmethod
 */

function buildcartonsforweight(opentasklist,containerslist,uompackflag)
{
	nlapiLogExecution('ERROR', 'into  buildcartonsforweight Task Split', '');
	try {
		var totaltaskweight = parseFloat(opentasklist.getValue('custrecord_total_weight'));
		var totaltaskcube = parseFloat(opentasklist.getValue('custrecord_totalcube'));	
		var whsite=opentasklist.getValue('custrecord_wms_location');
		var remweight = parseFloat(totaltaskweight);
		var remcube = parseFloat(totaltaskcube);
		var contsize="";
		var containerLP="";
		var cube="";
		var weight="";
		var exptdqty=0;

		//var containerslist=getAllContainers(0,0,cartonizationmethod);
		var uomlevel = opentasklist.getValue('custrecord_uom_level');
		var ebizskuno = opentasklist.getValue('custrecord_ebiz_sku_no');
		var uomcube = getUOMCube(uomlevel,ebizskuno);

		if(containerslist != null){ 
			//for(l=0;l<containerslist.length;l++){  
			for(l=(containerslist.length)-1;l>=0;l--){                                      
				var containerweight = containerslist[l].getValue('custrecord_maxweight');
				var containercube = containerslist[l].getValue('custrecord_cubecontainer');
				nlapiLogExecution('ERROR', 'Container Cube  ',containercube);
				nlapiLogExecution('ERROR', 'SKU UOM Cube  ',uomcube);

				exptdqty = Math.floor(parseFloat(containercube)/parseFloat(uomcube));

				nlapiLogExecution('ERROR', 'exptdqty  ',exptdqty);
				while (parseFloat(remweight)> 0 && parseFloat(remweight)>= parseFloat(containerweight) 
						&& parseFloat(exptdqty)>0){
					contsize =  containerslist[l].getValue('Internalid');
					containerLP = GetMaxLPNo('1', '2',whsite);		
					cube = containerslist[l].getValue('custrecord_cubecontainer');
					weight = containerslist[l].getValue('custrecord_maxweight');	
					CreateMasterLPRecord(containerLP,contsize,weight,cube,uompackflag,whsite);
					nlapiLogExecution('ERROR', 'containerLP1 in buildcartonsforweight ', containerLP);
					createNewPickTask(opentasklist,containerLP,cube,weight,contsize,exptdqty);
					remweight = parseFloat(remweight) - parseFloat(weight);
					remcube = parseFloat(remcube) - parseFloat(cube);
				}
			} 

			//if still there is some qty exists and the cube is less than the container(s) cube(no matching container),
			//then place it in the smallest container.
			if(parseFloat(remweight)> 0){

				nlapiLogExecution('ERROR', 'remweight  ',remweight);
				nlapiLogExecution('ERROR', 'remcube  ',remcube);

				exptdqty = Math.floor(parseFloat(remcube)/parseFloat(uomcube));

				nlapiLogExecution('ERROR', 'exptdqty  ',exptdqty);

				if(parseFloat(exptdqty)>0)
				{
					var k = containerslist.length;
					contsize =  containerslist[k-1].getValue('Internalid');
					containerLP = GetMaxLPNo('1', '2',whsite);
					CreateMasterLPRecord(containerLP,contsize,remweight,remcube,uompackflag,whsite);
					nlapiLogExecution('ERROR', 'containerLP2 in buildcartonsforweight ', containerLP);
					createNewPickTask(opentasklist,containerLP,remcube,remweight,contsize,exptdqty);
					cube = containerslist[k-1].getValue('custrecord_cubecontainer');
					weight = containerslist[k-1].getValue('custrecord_maxweight');
				}
			}
			if(parseFloat(exptdqty)>0)
			{
				//delete the actual record
				var id = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', opentasklist.getId());
				nlapiLogExecution('ERROR', 'Deleted Actual Record : ', id);	
			}
		}
	}
	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in buildcartonsforweight : ', exp);		
	}

	nlapiLogExecution('ERROR', 'out of  buildcartonsforweight Task Split', '');
}


/**
 * 
 * @param opentasklist
 * @param cartonizationmethod
 */
function buildcartonsforcubeandweight(opentasklist,containerslist,uompackflag)
{
	nlapiLogExecution('ERROR', 'into  buildcartonsforcubeandweight Task Split', '');
	try{
		var totaltaskweight = parseFloat(opentasklist.getValue('custrecord_total_weight'));
		var totaltaskcube = parseFloat(opentasklist.getValue('custrecord_totalcube'));	
		var remweight = parseFloat(totaltaskweight);
		var remcube = parseFloat(totaltaskcube);
		var contsize="";
		var containerLP="";
		var cube="";
		var weight="";
		var exptdqty =0;
		var whsite=opentasklist.getValue('custrecord_wms_location');

		//var containerslist=getAllContainers(0,0,cartonizationmethod);

		var uomlevel = opentasklist.getValue('custrecord_uom_level');
		var ebizskuno = opentasklist.getValue('custrecord_ebiz_sku_no');
		var uomcube = getUOMCube(uomlevel,ebizskuno);
		if(containerslist != null){ 
			//for(l=0;l<containerslist.length;l++){  
			for(l=(containerslist.length)-1;l>=0;l--){
				var containerweight = containerslist[l].getValue('custrecord_maxweight');
				var containercube = containerslist[l].getValue('custrecord_cubecontainer');
				var packfactor = containerslist[l].getValue('custrecord_packfactor');
				if(packfactor!=null && packfactor!='')
				{
					containercube = parseFloat(containercube)-(parseFloat(containercube)*(parseFloat(packfactor)/100));
				}

				nlapiLogExecution('ERROR', 'containercube', containercube);
				nlapiLogExecution('ERROR', 'uomcube', uomcube);
				exptdqty = Math.floor(parseFloat(containercube)/parseFloat(uomcube));
				nlapiLogExecution('ERROR', 'exptdqty  ',exptdqty);
				nlapiLogExecution('ERROR', 'remweight', remweight);
				nlapiLogExecution('ERROR', 'remcube', remcube);

				while ((parseFloat(remweight)> 0) && (parseFloat(remweight)>= parseFloat(containerweight)) && 
						(parseFloat(remcube)> 0) && (parseFloat(remcube)>= parseFloat(containercube)) &&
						(parseFloat(exptdqty)>0)){
					contsize =  containerslist[l].getValue('Internalid');
					containerLP = GetMaxLPNo('1', '2',whsite);
					cube = containerslist[l].getValue('custrecord_cubecontainer');
					weight = containerslist[l].getValue('custrecord_maxweight');
					CreateMasterLPRecord(containerLP,contsize,weight,cube,uompackflag,whsite);
					nlapiLogExecution('ERROR', 'containerLP1 in buildcartonsforcubeandweight ', containerLP);
					createNewPickTask(opentasklist,containerLP,cube,weight,contsize,exptdqty);		              
					remweight = parseFloat(remweight) - parseFloat(weight);
					remcube = parseFloat(remcube) - parseFloat(cube);
				}
			} 

			nlapiLogExecution('ERROR', 'remweight', remweight);
			nlapiLogExecution('ERROR', 'remcube', remcube);
			nlapiLogExecution('ERROR', 'uomcube', uomcube);
			//if still there is some qty exists and the cube is less than the container(s) cube(no matching container),
			//then place it in the smallest container.
			if(parseFloat(remweight)> 0 || parseFloat(remcube)> 0){

				exptdqty = Math.floor(parseFloat(remcube)/parseFloat(uomcube));

				nlapiLogExecution('ERROR', 'exptdqty  ',exptdqty);

				if(parseFloat(exptdqty)>0)
				{
					var k = containerslist.length;
					contsize =  containerslist[k-1].getValue('Internalid');
					containerLP = GetMaxLPNo('1', '2',whsite);
					CreateMasterLPRecord(containerLP,contsize,remweight,remcube,uompackflag,whsite);
					nlapiLogExecution('ERROR', 'containerLP2 in buildcartonsforcubeandweight ', containerLP);
					createNewPickTask(opentasklist,containerLP,remcube,remweight,contsize,exptdqty);	             
					cube = containerslist[k-1].getValue('custrecord_cubecontainer');
					weight = containerslist[k-1].getValue('custrecord_maxweight');
				}
			}
			if(parseFloat(exptdqty)>0)
			{
				//delete the actual record
				var id = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', opentasklist.getId());
				nlapiLogExecution('ERROR', 'Deleted Actual Record : ', id);	
			}
		}
	}
	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in buildcartonsforcubeandweight : ', exp);		
	}
	nlapiLogExecution('ERROR', 'out of  buildcartonsforcubeandweight Task Split', '');
}


/**
 * 
 * @param recordId
 * @param containerLP
 * @param sizeid
 */
function updatesizeidinopentask(recordId, containerLP,sizeid){
	nlapiLogExecution('ERROR', 'In to updatesizeidinopentask function : sizeid',sizeid);
	var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recordId);
	transaction.setFieldValue('custrecord_container', sizeid);
	transaction.setFieldValue('custrecord_container_lp_no', containerLP);

	nlapiSubmitRecord(transaction, false, true);
	nlapiLogExecution('ERROR', 'Size Id Updated in open task ',containerLP);
	nlapiLogExecution('ERROR', 'Out of updatesizeidinopentask function');
}


/**
 * 
 * @param oldTaskList
 * [0]  - containerlpno, [1]  - totalweight, [2]  - totalcube,  [3]  - fforderno,    [4]  - fforderinternalno, [5]  - sku,       [6]  - ebizskuno
 * [7]  - ebizwaveno,    [8]  - expqty,      [9]  - linenumber, [10] - lpno,         [11] - packcode ,         [12] - skustatus, [13] - statusflag   
 * [14] - tasktype,      [15] - uom,         [16] - uomlevel,   [17] - ebizreceiptno,[18] - beginlocation,     [19] - invrefno,  [20] - fromlpno
 * [21] - ebizordno,     [22] - wmslocation, [23] - ebizruleno, [24] - ebizmethodno, [25] - pickzone
 */
function createNewPickTask(oldTaskList,containerLP,cube,weight,containersize,exptdqty){

	nlapiLogExecution('ERROR', 'into createNewPickTask - exptdqty ',exptdqty);	

	if(oldTaskList!=null){

//		var uomlevel = oldTaskList.getValue('custrecord_uom_level');
//		var ebizskuno = oldTaskList.getValue('custrecord_ebiz_sku_no');

//		var uomcube = getUOMCube(uomlevel,ebizskuno);

//		nlapiLogExecution('ERROR', 'cube  ',cube);
//		nlapiLogExecution('ERROR', 'uomcube  ',uomcube);

//		var exptdqty = Math.floor(parseFloat(cube)/parseFloat(uomcube));
//		nlapiLogExecution('ERROR', 'exptdqty  ',exptdqty);

		if(exptdqty>0)
		{

			var openTaskRecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');

			openTaskRecord.setFieldValue('name', oldTaskList.getValue('name'));							// DELIVERY ORDER NAME
			openTaskRecord.setFieldValue('custrecord_ebiz_sku_no', oldTaskList.getValue('custrecord_ebiz_sku_no'));			// ITEM INTERNAL ID
			openTaskRecord.setFieldValue('custrecord_sku', oldTaskList.getValue('custrecord_sku'));					// ITEM NAME
			openTaskRecord.setFieldValue('custrecord_expe_qty', parseFloat(exptdqty).toFixed(5));			// FULFILMENT ORDER QUANTITY
			openTaskRecord.setFieldValue('custrecord_container_lp_no', containerLP);
			openTaskRecord.setFieldValue('custrecord_ebiz_lpno', containerLP);
			openTaskRecord.setFieldValue('custrecord_line_no', oldTaskList.getValue('custrecord_line_no'));
			openTaskRecord.setFieldValue('custrecord_tasktype', '3'); 						// TASK TYPE = PICK

			openTaskRecord.setFieldValue('custrecord_actbeginloc', oldTaskList.getValue('custrecord_actbeginloc'));
			openTaskRecord.setFieldValue('custrecord_ebiz_cntrl_no', oldTaskList.getValue('custrecord_ebiz_cntrl_no'));
			openTaskRecord.setFieldValue('custrecord_ebiz_order_no', oldTaskList.getValue('custrecord_ebiz_order_no'));
			openTaskRecord.setFieldValue('custrecord_ebiz_receipt_no', oldTaskList.getValue('custrecord_ebiz_receipt_no'));
			openTaskRecord.setFieldValue('custrecord_ebiz_wave_no',  oldTaskList.getValue('custrecord_ebiz_wave_no'));
			openTaskRecord.setFieldValue('custrecordact_begin_date',DateStamp());
			openTaskRecord.setFieldValue('custrecord_wms_status_flag', '9'); 				// STATUS FLAG = G

			openTaskRecord.setFieldValue('custrecord_from_lp_no', oldTaskList.getValue('custrecord_from_lp_no'));
			openTaskRecord.setFieldValue('custrecord_lpno', oldTaskList.getValue('custrecord_from_lp_no'));
			openTaskRecord.setFieldValue('custrecord_actualbegintime', TimeStamp());
			openTaskRecord.setFieldValue('custrecord_invref_no', oldTaskList.getValue('custrecord_invref_no'));
			openTaskRecord.setFieldValue('custrecord_packcode', oldTaskList.getValue('custrecord_packcode'));
			openTaskRecord.setFieldValue('custrecord_sku_status', oldTaskList.getValue('custrecord_sku_status'));
			openTaskRecord.setFieldValue('custrecord_uom_id', oldTaskList.getValue('custrecord_uom_id'));
			//openTaskRecord.setFieldValue('custrecord_batch_no', oldTaskList[10]);
			openTaskRecord.setFieldValue('custrecord_ebizrule_no', oldTaskList.getValue('custrecord_ebizrule_no'));
			openTaskRecord.setFieldValue('custrecord_ebizmethod_no', oldTaskList.getValue('custrecord_ebizmethod_no'));
			openTaskRecord.setFieldValue('custrecord_ebizzone_no', oldTaskList.getValue('custrecord_ebizzone_no'));
			//openTaskRecord.setFieldValue('custrecord_comp_id', oldTaskList[10]);
			openTaskRecord.setFieldValue('custrecord_wms_location', oldTaskList.getValue('custrecord_wms_location'));						
			var currentContext = nlapiGetContext();  
			var currentUserID = currentContext.getUser();		
			openTaskRecord.setFieldValue('custrecord_ebizuser', currentUserID);		
			openTaskRecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);						
			openTaskRecord.setFieldValue('custrecord_taskassignedto', currentUserID);
			openTaskRecord.setFieldValue('custrecord_uom_level', oldTaskList.getValue('custrecord_uom_level'));
			openTaskRecord.setFieldValue('custrecord_total_weight', parseFloat(weight).toFixed(5));
			openTaskRecord.setFieldValue('custrecord_totalcube', parseFloat(cube).toFixed(5));
			openTaskRecord.setFieldValue('custrecord_container', containersize);
			openTaskRecord.setFieldValue('custrecord_notes', '');

			nlapiSubmitRecord(openTaskRecord,false,true);
			nlapiLogExecution('ERROR', 'Opentask updation Completed');
		}
	}
	nlapiLogExecution('ERROR', 'out of createNewPickTask ','');
}

function getUOMCube(uomlevel,ebizskuno) {
	nlapiLogExecution('ERROR', 'into getUOMCube', '');

	var cubelist = new Array();
	var uomcube="";

	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', ebizskuno));
	filters.push(new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'is', uomlevel));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizcube');

	cubelist = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);
	if(cubelist!=null)
		uomcube = cubelist[0].getValue('custrecord_ebizcube');

	nlapiLogExecution('ERROR', 'UOM Cube', uomcube);
	nlapiLogExecution('ERROR', 'out of getUOMCube', '');

	return uomcube;

}

function getDistinctMethodsList(distinctorderslist){
	var distinctorders = new Array();
	if(distinctorderslist!=null && distinctorderslist.length>0){	
		label:for (var z = 0; z < distinctorderslist.length; z++){
			for (var y = 0; y < distinctorders.length; y++){
				if((distinctorderslist[z][0]==distinctorders[y][0]) && (distinctorderslist[z][1]==distinctorders[y][1])
						&& (distinctorderslist[z][2]==distinctorders[y][2])){
					continue label;
				}					
			}	
			distinctorders[distinctorders.length]=distinctorderslist[z];
		}
	}
	return distinctorders;
}

function getpickmethod()
{
	nlapiLogExecution('ERROR','ClustergetPickMethod','chkptTrue');
	var pickInternalId=new Array();
	var filter=new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizcreatecluster',null,'is','T'));
	var column=new Array();
	column[0]=new nlobjSearchColumn('internalid').setSort();
	column[1]=new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxorders');
	column[2]=new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxpicks');
	var searchresult=nlapiSearchRecord('customrecord_ebiznet_pick_method', null, filter, column);

	if(searchresult!=null)
	{
		for ( var i = 0; i < searchresult.length; i++)
		{
			pickInternalId[i]=new Array();
			pickInternalId[i][0]=searchresult[i].getValue('internalid');
			pickInternalId[i][1]=searchresult[i].getValue('custrecord_ebiz_pickmethod_maxorders');
			pickInternalId[i][2]=searchresult[i].getValue('custrecord_ebiz_pickmethod_maxpicks');
		}
	}

	return pickInternalId;
}
//function CreateMasterLPRecord(vContLpNo,vContainerSize,TotalWeight,ContainerCube,uompackflag)
//{
//nlapiLogExecution('ERROR', 'CreateMasterLPRecord uompackflag', uompackflag);
//var finaltext="";
//var duns="";
//var label="",uom="",ResultText="";
////added by mahesh

//var filters = new Array();
//filters[0] = new nlobjSearchFilter('custrecord_ebiznet_lprange_lptype', null, 'is', 5);
//filters[1] = new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'is', 1);

//var columns = new Array();
//columns[0] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix');
//columns[1] = new nlobjSearchColumn('custrecord_ebiznet_lprange_begin');
//columns[2] = new nlobjSearchColumn('custrecord_ebiznet_lprange_end');
//columns[3] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpgentype');
//columns[4] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lptype');
//columns[5] = new nlobjSearchColumn('custrecord_ebiznet_lprange_company');

//var searchresults = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, filters, columns);
//nlapiLogExecution('ERROR', 'Lp Range Searchresults', searchresults.length);
//if (searchresults !=null && searchresults !="") 
//{
////for (var i = 0; i < searchresults.length; i++) 
////{
//try 
//{
//var company = searchresults[0].getText('custrecord_ebiznet_lprange_company');	
//var getLPrefix = searchresults[0].getValue('custrecord_ebiznet_lprange_lpprefix');					
//var varBeginLPRange = searchresults[0].getValue('custrecord_ebiznet_lprange_begin');
//var varEndRange = searchresults[0].getValue('custrecord_ebiznet_lprange_end');
//var getLPGenerationTypeValue = searchresults[0].getValue('custrecord_ebiznet_lprange_lpgentype');
//var getLPTypeValue = searchresults[0].getText('custrecord_ebiznet_lprange_lptype');

//var lpMaxValue=GetMaxLPNo('1', '5');
//nlapiLogExecution('ERROR', 'lpMaxValue', lpMaxValue);

//var prefixlength=lpMaxValue.length;
//nlapiLogExecution('ERROR', 'prefixlength', prefixlength);

//if(prefixlength==0)
//label="000000000";
//else if(prefixlength==1)
//label="00000000"+lpMaxValue;
//else if(prefixlength==2)
//label="0000000"+lpMaxValue;
//else if(prefixlength==3)
//label="000000"+lpMaxValue;
//else if(prefixlength==4)
//label="00000"+lpMaxValue;
//else if(prefixlength==5)
//label="0000"+lpMaxValue;
//else if(prefixlength==6)
//label="000"+lpMaxValue;
//else if(prefixlength==7)
//label="00"+lpMaxValue;
//else if(prefixlength==8)
//label="0"+lpMaxValue;
//else if(prefixlength==9)
//label=lpMaxValue;

//nlapiLogExecution('ERROR', 'label', label);
//var dunsfilters = new Array();
//dunsfilters[0] = new nlobjSearchFilter('custrecord_company', null, 'is', company);
//var dunscolumns = new Array();
//dunscolumns[0] = new nlobjSearchColumn('custrecord_compduns');			 

//var dunssearchresults = nlapiSearchRecord('customrecord_ebiznet_company', null, dunsfilters, dunscolumns);
//if (dunssearchresults !=null && dunssearchresults !="") 
//{
//duns = dunssearchresults[0].getValue('custrecord_compduns');
//nlapiLogExecution('ERROR', 'duns', duns);
//}

//if(uompackflag == "1" ) 
//uom="0"; 
//else if(uompackflag == "3") 
//uom="2";
//else
//uom="0";
//nlapiLogExecution('ERROR', 'uom', uom);
//nlapiLogExecution('ERROR', 'CreateMasterLPRecord finaltext', finaltext);
//finaltext=uom+duns+label;
//nlapiLogExecution('ERROR', 'CreateMasterLPRecord finaltext', finaltext);



////to get chk digit
//var checkStr=finaltext;
//var ARL=0;
//var BRL=0;
//var CheckDigitValue="";
//for (i = checkStr.length-1;  i > 0;  i--)
//{
//ARL = ARL+parseFloat(checkStr.charAt(i));
//i--;
//}
////alert(ARL);
//ARL=ARL*3;
//for (i = checkStr.length-2;  i > 0;  i--)
//{
//BRL = BRL+parseFloat(checkStr.charAt(i));
//i--;
//}
////alert(BRL);
//var sumOfARLBRL=ARL+BRL;
//var CheckDigit=0;

//while(CheckDigit<10)
//{
//if(sumOfARLBRL%10==0)
//{ 
//CheckDigitValue=CheckDigit; 
//break; 
//} 

//sumOfARLBRL++;
//CheckDigit++;
//}
//nlapiLogExecution('ERROR', 'CreateMasterLPRecord CheckDigit', CheckDigitValue.toString());
//ResultText="00"+finaltext+CheckDigitValue.toString();
//nlapiLogExecution('ERROR', 'CreateMasterLPRecord ResultText', ResultText);
//} 
//catch (err) 
//{

//}
////}
//} 

//var MastLP=nlapiCreateRecord('customrecord_ebiznet_master_lp');
//MastLP.setFieldValue('name', vContLpNo);
//MastLP.setFieldValue('custrecord_ebiz_lpmaster_lp', vContLpNo);
//MastLP.setFieldValue('custrecord_ebiz_lpmaster_sizeid', vContainerSize);
//MastLP.setFieldValue('custrecord_ebiz_lpmaster_totwght', TotalWeight);
//MastLP.setFieldValue('custrecord_ebiz_lpmaster_totcube', ContainerCube);
//MastLP.setFieldValue('custrecord_ebiz_lpmaster_sscc', ResultText);
////MastLP.setFieldValue('custrecord_ebiz_lpmaster_company', vCompany);

//var currentContext = nlapiGetContext();		

//var retktoval = nlapiSubmitRecord(MastLP);
//nlapiLogExecution('ERROR', 'Return Value from CreateMasterLPRecord',retktoval);
//}

function GenerateLable(uompackflag){

	nlapiLogExecution('ERROR', 'CreateMasterLPRecord uompackflag', uompackflag);
	var finaltext="";
	var duns="";
	var label="",uom="",ResultText="";
	//added by mahesh


	try 
	{	
		var lpMaxValue=GetMaxLPNo('1', '5','');
		nlapiLogExecution('ERROR', 'lpMaxValue', lpMaxValue);

		var prefixlength=lpMaxValue.length;
		nlapiLogExecution('ERROR', 'prefixlength', prefixlength);

		if(prefixlength==0)
			label="000000000";
		else if(prefixlength==1)
			label="00000000"+lpMaxValue;
		else if(prefixlength==2)
			label="0000000"+lpMaxValue;
		else if(prefixlength==3)
			label="000000"+lpMaxValue;
		else if(prefixlength==4)
			label="00000"+lpMaxValue;
		else if(prefixlength==5)
			label="0000"+lpMaxValue;
		else if(prefixlength==6)
			label="000"+lpMaxValue;
		else if(prefixlength==7)
			label="00"+lpMaxValue;
		else if(prefixlength==8)
			label="0"+lpMaxValue;
		else if(prefixlength==9)
			label=lpMaxValue;

		//to get company id

		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_ebiznet_lprange_lptype', null, 'is', 5);
		filters[1] = new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'is', 1);

		var columns = new Array();	
		columns[0] = new nlobjSearchColumn('custrecord_ebiznet_lprange_company');

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, filters, columns);
		nlapiLogExecution('ERROR', 'Lp Range Searchresults', searchresults.length);
		if (searchresults !=null && searchresults !="") 
		{
			var company = searchresults[0].getText('custrecord_ebiznet_lprange_company');	
		}
		nlapiLogExecution('ERROR', 'label', label);
		var dunsfilters = new Array();
		dunsfilters[0] = new nlobjSearchFilter('custrecord_company', null, 'is', company);
		var dunscolumns = new Array();
		dunscolumns[0] = new nlobjSearchColumn('custrecord_compduns');			 

		var dunssearchresults = nlapiSearchRecord('customrecord_ebiznet_company', null, dunsfilters, dunscolumns);
		if (dunssearchresults !=null && dunssearchresults !="") 
		{
			duns = dunssearchresults[0].getValue('custrecord_compduns');
			nlapiLogExecution('ERROR', 'duns', duns);
		}

		if(uompackflag == "1" || uompackflag == "EACH" ) 
			uom="0"; 
		else if(uompackflag == "3" || uompackflag == "PALLET") 
			uom="2";
		else
			uom="0";
		nlapiLogExecution('ERROR', 'uom', uom);
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord finaltext', finaltext);
//Case Start 201413734 	
	//finaltext=uom+duns+label;
		finaltext=duns+label;
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord finaltext', finaltext);
//Case End 201413734 	


		//to get chk digit
		var checkStr=finaltext;
		var ARL=0;
		var BRL=0;
		var CheckDigitValue="";
		for (i = checkStr.length-1;  i > 0;  i--)
		{
			ARL = ARL+parseFloat(checkStr.charAt(i));
			i--;
		}		
		ARL=ARL*3;
		for (i = checkStr.length-2;  i > 0;  i--)
		{
			BRL = BRL+parseFloat(checkStr.charAt(i));
			i--;
		}		
		var sumOfARLBRL=ARL+BRL;
		var CheckDigit=0;

		while(CheckDigit<10)
		{
			if(sumOfARLBRL%10==0)
			{ 
				CheckDigitValue=CheckDigit; 
				break; 
			} 

			sumOfARLBRL++;
			CheckDigit++;
		}
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord CheckDigit', CheckDigitValue.toString());
		ResultText="00"+finaltext+CheckDigitValue.toString();
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord ResultText', ResultText);
	} 
	catch (err) 
	{

	}
	return ResultText;
}

function CreateMasterLPRecord(vContainerLpNo,vContainerSize,TotalWeight,ContainerCube,uompackflag,location)
{
	nlapiLogExecution('ERROR', 'location',location);
	nlapiLogExecution('ERROR', 'vContainerLpNo',vContainerLpNo);

	var ResultText=GenerateLable(uompackflag);
	var MastLP=nlapiCreateRecord('customrecord_ebiznet_master_lp');		
	MastLP.setFieldValue('name', vContainerLpNo);
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_lp', vContainerLpNo);
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_sizeid', vContainerSize);
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_totwght', parseFloat(TotalWeight).toFixed(5));
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(ContainerCube).toFixed(5));
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_sscc', ResultText);	
	if(location!=null && location!='')
		MastLP.setFieldValue('custrecord_ebiz_lpmaster_site', location);	

	var currentContext = nlapiGetContext();
	var retktoval = nlapiSubmitRecord(MastLP);
	nlapiLogExecution('ERROR', 'Return Value from CreateMasterLPRecord',retktoval);

}

function containerLPExists(lp){

	var filters = new Array();
	nlapiLogExecution('ERROR', 'lp:',lp); 
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lp));	 
	nlapiLogExecution('ERROR', 'Master LP Feaching','Success');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
	columns[0].setSort();

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);

	//nlapiLogExecution('ERROR', 'Master LP Feaching',searchresults.length);
	return searchresults;
}
function getContainerSize(containerslist,totaltaskcube,totaltaskweight,cartonizationmethod)
{
	nlapiLogExecution('ERROR', 'Into getContainerSize',containerslist);
	nlapiLogExecution('ERROR', 'totaltaskcube',totaltaskcube);
	nlapiLogExecution('ERROR', 'totaltaskweight',totaltaskweight);
	nlapiLogExecution('ERROR', 'cartonizationmethod',cartonizationmethod);

	var contsize='';

	for(l=0;l<containerslist.length;l++)
	{      
		var containercube = containerslist[l].getValue('custrecord_cubecontainer');
		var containerweight= containerslist[l].getValue('custrecord_maxweight');
		var containername = containerslist[l].getValue('custrecord_containername');
		var packfactor=containerslist[l].getValue('custrecord_packfactor');
		if(packfactor==null || packfactor=='')
			packfactor=0;
		var actcube=parseFloat(containercube)-(parseFloat(containercube)*(parseFloat(packfactor)/100));
		nlapiLogExecution('ERROR', 'actcube',actcube);

		if(cartonizationmethod=='1')
		{
			if((containername!='SHIPASIS') && (actcube>=totaltaskcube) && (containerweight>=totaltaskweight))
			{
				contsize=containerslist[l].getValue('Internalid');
				nlapiLogExecution('ERROR', 'contsize',contsize);
				return contsize;
			}
		}
		else if(cartonizationmethod=='2')
		{
			if((containername!='SHIPASIS') && (containerweight>=totaltaskweight))
			{
				contsize=containerslist[l].getValue('Internalid');
				nlapiLogExecution('ERROR', 'contsize',contsize);
				return contsize;
			}
		}
		else if(cartonizationmethod=='3')
		{
			if((containername!='SHIPASIS') && (actcube>=totaltaskcube))
			{
				contsize=containerslist[l].getValue('Internalid');
				nlapiLogExecution('ERROR', 'contsize',contsize);
				return contsize;
			}
		}

	}
	nlapiLogExecution('ERROR', 'contsize',contsize);
	nlapiLogExecution('ERROR', 'Out of getContainerSize',containerslist);

	return contsize;
}

function setClusterNo(eBizWaveNo)
{
	nlapiLogExecution('ERROR','ClusterWaveNo',eBizWaveNo);
	var pickMethodIds=new Array();
	pickMethodIds=getpickmethod();
	if(pickMethodIds!=null && pickMethodIds.length>0)
	{
		var ebizMethodNo=new Array();
		var ebizInternalRecordId=new Array();
		var ebizOrderId=new Array();

		var filter=new Array();
		filter[0]=new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is',eBizWaveNo);
		filter[1]=new nlobjSearchFilter('custrecord_tasktype',null,'anyof',[3]);
		filter[2]=new nlobjSearchFilter('custrecord_actualendtime',null,'isempty');

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizmethod_no').setSort();
		column[1]=new nlobjSearchColumn('internalid');
		column[2]=new nlobjSearchColumn('name').setSort();

		var searchRecord=nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filter, column);
		nlapiLogExecution('ERROR','ClusterSearchRecord',searchRecord.length);

		for(var count=0;count < searchRecord.length; count++)
		{
			ebizMethodNo[count]=searchRecord[count].getValue('custrecord_ebizmethod_no');
			ebizInternalRecordId[count]=searchRecord[count].getValue('internalid');
			ebizOrderId[count]=searchRecord[count].getValue('name');
		}

		nlapiLogExecution('ERROR','ClusterpickMethodIds',pickMethodIds);
		nlapiLogExecution('ERROR','ebizMethodNo',ebizMethodNo);
		nlapiLogExecution('ERROR','A_pickMethodIds[itemcount][1]',pickMethodIds[0][1]);
		nlapiLogExecution('ERROR','A_pickMethodIds[itemcount][2]',pickMethodIds[0][2]);
		var ordno="";
		var clusterno = GetMaxTransactionNo('WAVECLUSTER');
		nlapiLogExecution('ERROR','clusternobeforefor',clusterno);
		for(var itemcount=0;itemcount<pickMethodIds.length;itemcount++)
		{
			var maxPickFlagCount=0;
			var tempcount=0;
			for(var loopcount=0;loopcount<searchRecord.length;loopcount++)
			{  
				nlapiLogExecution('ERROR','clustervalues',pickMethodIds[itemcount][0]+','+ebizMethodNo[loopcount]);
				if(pickMethodIds[itemcount][0]==ebizMethodNo[loopcount])
				{


					//for a particular pickmethod ,if max order no. is not null and max pickno. is null
					//then the below code execute.
					if(pickMethodIds[itemcount][1]!="" && pickMethodIds[itemcount][2]=="")
					{
						nlapiLogExecution('ERROR','BlockOfCodeForMaxPickIsNULL','chkpt');
						if(ordno!=ebizOrderId[loopcount])
						{
							tempcount=parseFloat(tempcount)+1;
							if(parseFloat(tempcount)<=pickMethodIds[itemcount][1])
							{
								nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
								ordno = ebizOrderId[loopcount];
							}
							else
							{
								clusterno = GetMaxTransactionNo('WAVECLUSTER');
								nlapiLogExecution('ERROR','clusternoafterelse',clusterno);
								nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
								ordno = ebizOrderId[loopcount];
								tempcount=1;
							}
						}
						else
						{
							nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
						}
					}



					//for a particular pickmethod ,if max orderno. is null and max pickno.is not null 
					//then the below code execute.
					if(pickMethodIds[itemcount][1] == "" && pickMethodIds[itemcount][2] != "")
					{
						nlapiLogExecution('ERROR','BlockOfCodeForMaxorderIsNULL','chkpt');
						tempcount=parseFloat(tempcount)+1;
						if(parseFloat(tempcount)<=pickMethodIds[itemcount][2])
						{
							nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
						}
						else
						{
							clusterno = GetMaxTransactionNo('WAVECLUSTER');
							nlapiLogExecution('ERROR','clusternoafterelse',clusterno);
							nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
							tempcount=1;
						}
					}



					//for a pickmethod, if max orderno. and max pickno is not null 
					//then the below code execute.
					if(pickMethodIds[itemcount][1]!= "" && pickMethodIds[itemcount][2] != "")
					{
						nlapiLogExecution('ERROR','BlockOfCodeForNONEIsNULL','chkpt');
						maxPickFlagCount= parseFloat(maxPickFlagCount)+1;
						if(parseFloat(maxPickFlagCount)<=pickMethodIds[itemcount][2])
						{
							if(ordno!=ebizOrderId[loopcount])
							{
								tempcount=parseFloat(tempcount)+1;

								if(parseFloat(tempcount)<=pickMethodIds[itemcount][1])
								{
									nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
									ordno = ebizOrderId[loopcount];
								}
								else
								{
									clusterno = GetMaxTransactionNo('WAVECLUSTER');
									nlapiLogExecution('ERROR','clusternoafterelse',clusterno);
									nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
									ordno = ebizOrderId[loopcount];
									tempcount=1;
								}
							}
							else
							{
								nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
							}
						}
						else
						{
							clusterno = GetMaxTransactionNo('WAVECLUSTER');
//							nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
							maxPickFlagCount= 1;

							if(ordno!=ebizOrderId[loopcount])
							{
								tempcount=parseFloat(tempcount)+1;

								if(parseFloat(tempcount)<=pickMethodIds[itemcount][1])
								{
									nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
									ordno = ebizOrderId[loopcount];
								}
								else
								{
									clusterno = GetMaxTransactionNo('WAVECLUSTER');
									nlapiLogExecution('ERROR','clusternoafterelse',clusterno);
									nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
									ordno = ebizOrderId[loopcount];
									tempcount=1;
								}
							}
							else
							{
								nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
							}
						}
					}
				}
			}
		}
	}	
}			




function removeDuplicateElement(arrayName)
{
	var newArray=new Array();
	label:for(var i=0; i<arrayName.length;i++ )
	{  
		for(var j=0; j<newArray.length;j++ )
		{
			if(newArray[j]==arrayName[i]) 
				continue label;
		}
		newArray[newArray.length] = arrayName[i];
	}
	return newArray;
} 
/*
function GenerateChildLabel(vContLpNo,vOrderNo,vwaveno,uompackflag)
{

	nlapiLogExecution('ERROR','into childlable gen','done');
	nlapiLogExecution('ERROR','uompackflag',uompackflag);
	nlapiLogExecution('ERROR','vwaveno',vwaveno);

	//get qty from opentask against to containerlp
	var nooflps=0;var uomId="",itemId="",itemText="";
	var qtyfilters = new Array();
	if(vContLpNo !=null && vContLpNo !=""){
		qtyfilters.push( new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vContLpNo));
	}
	if(vOrderNo !=null && vOrderNo !=""){
		qtyfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', [vOrderNo]));
	}
	if(vwaveno !=null && vwaveno !=""){
		qtyfilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', [vwaveno]));
	}
	qtyfilters.push( new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));               

	var qtycolumns = new Array();
	qtycolumns[0] = new nlobjSearchColumn('custrecord_container_lp_no', null, 'group');
	qtycolumns[1] = new nlobjSearchColumn('custrecord_sku', null, 'group');
	qtycolumns[2] = new nlobjSearchColumn('custrecord_uom_id', null, 'group');
	qtycolumns[3] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');	
	qtycolumns[1].setSort();

	var qtysearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, qtyfilters, qtycolumns);

	for(var i=0;qtysearchresults !=null && i< qtysearchresults.length;i++)
	{		
		var vContainerLpNo = qtysearchresults[i].getValue('custrecord_container_lp_no', null, 'group');
		var itemText = qtysearchresults[i].getText('custrecord_sku', null, 'group');
		var itemId = qtysearchresults[i].getValue('custrecord_sku', null, 'group');
		var uomId = qtysearchresults[i].getValue('custrecord_uom_id', null, 'group');
		var nooflps = qtysearchresults[i].getValue('custrecord_expe_qty', null, 'sum');

		nlapiLogExecution('ERROR','nooflps',nooflps);
		nlapiLogExecution('ERROR','uomId',uomId);
		nlapiLogExecution('ERROR','itemId',itemId);
		nlapiLogExecution('ERROR','vContainerLpNo',vContainerLpNo);
		nlapiLogExecution('ERROR','qtysearchresults.length',qtysearchresults.length);

		if(i==0 && qtysearchresults !=null)
		{
			for(var i=0; i < nooflps.toString(); i++ ){
				nlapiLogExecution('ERROR', 'into for loop','done');

				var ResultText=GenerateLable(uomId);
				var substring=ResultText.substring(10, 20);	
				var vContLpNo='PICK'+substring;
				nlapiLogExecution('ERROR', 'substring',substring);

				var MastLP=nlapiCreateRecord('customrecord_ebiznet_master_lp');		
				MastLP.setFieldValue('name', vContLpNo);
				MastLP.setFieldValue('custrecord_ebiz_lpmaster_lp', vContLpNo);			
				MastLP.setFieldValue('custrecord_ebiz_lpmaster_sscc', ResultText);
				MastLP.setFieldValue('custrecord_ebiz_lpmaster_masterlp', vContainerLpNo);

				var currentContext = nlapiGetContext();
				var retktoval = nlapiSubmitRecord(MastLP);
				//nlapiLogExecution('ERROR', 'Return Value from CreateMasterLPRecord',retktoval);
			}
		}
		else if(qtysearchresults.length >1 && qtysearchresults[i].getText('custrecord_sku', null, 'group') != qtysearchresults[i+1].getText('custrecord_sku', null, 'group') )
		{

			for(var i=0; i < nooflps.toString(); i++ ){
				nlapiLogExecution('ERROR', 'into for loop','done');

				var ResultText=GenerateLable(uomId);
				var substring=ResultText.substring(10, 20);	
				var vContLpNo='PICK'+substring;
				nlapiLogExecution('ERROR', 'substring',substring);

				var MastLP=nlapiCreateRecord('customrecord_ebiznet_master_lp');		
				MastLP.setFieldValue('name', vContLpNo);
				MastLP.setFieldValue('custrecord_ebiz_lpmaster_lp', vContLpNo);
				MastLP.setFieldValue('custrecord_ebiz_lpmaster_sizeid', vContainerSize);
				MastLP.setFieldValue('custrecord_ebiz_lpmaster_totwght', TotalWeight);
				MastLP.setFieldValue('custrecord_ebiz_lpmaster_totcube', ContainerCube);
				MastLP.setFieldValue('custrecord_ebiz_lpmaster_sscc', ResultText);
				MastLP.setFieldValue('custrecord_ebiz_lpmaster_masterlp', vContainerLpNo);

				var currentContext = nlapiGetContext();
				var retktoval = nlapiSubmitRecord(MastLP);
				//nlapiLogExecution('ERROR', 'Return Value from CreateMasterLPRecord',retktoval);
			}

		}

	}
}*/
function GenerateChildLabel(vContLpNo,vOrderNo,vwaveno,uompackflag)
{
	nlapiLogExecution('ERROR','Into GenerateChildLabel......');
	nlapiLogExecution('ERROR','uompackflag',uompackflag);
	nlapiLogExecution('ERROR','vwaveno',vwaveno);
	nlapiLogExecution('ERROR','vOrderNo',vOrderNo);
	nlapiLogExecution('ERROR','vContLpNo',vContLpNo);

	//get qty from opentask against to containerlp
	var nooflps=0;var uomId="",itemId="",itemText="";
	var qtyfilters = new Array();

	if(vContLpNo !=null && vContLpNo !=""){
		qtyfilters.push( new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vContLpNo));
	}
	if(vOrderNo !=null && vOrderNo !=""){
		qtyfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', [vOrderNo]));
	}
	if(vwaveno !=null && vwaveno !=""){
		qtyfilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', [vwaveno]));
	}
	qtyfilters.push( new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));               

	var qtycolumns = new Array();
	qtycolumns[0] = new nlobjSearchColumn('custrecord_container_lp_no', null, 'group');
	qtycolumns[1] = new nlobjSearchColumn('custrecord_sku', null, 'group');
	qtycolumns[2] = new nlobjSearchColumn('custrecord_uom_id', null, 'group');
	qtycolumns[3] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');	

	qtycolumns[1].setSort();

	var qtysearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, qtyfilters, qtycolumns);
	var oldsku="";
	if(qtysearchresults!=null && qtysearchresults!='')
	{
		var MaxLpNoArray = new Array();

		for(var g=0;g<qtysearchresults.length;g++)
		{		
			var vContainerLpNo = qtysearchresults[g].getValue('custrecord_container_lp_no', null, 'group');
			var itemText = qtysearchresults[g].getText('custrecord_sku', null, 'group');
			var itemId = qtysearchresults[g].getValue('custrecord_sku', null, 'group');
			var itemName=qtysearchresults[g].getText('custrecord_sku', null, 'group');
			var uomId = qtysearchresults[g].getValue('custrecord_uom_id', null, 'group');
			var itemtotalqty = qtysearchresults[g].getValue('custrecord_expe_qty', null, 'sum');
			var packcode = qtysearchresults[g].getValue('custrecord_packcode', null, 'group');
			nlapiLogExecution('ERROR','itemtotalqty',itemtotalqty);
			var nooflps="";
			if((packcode!=null)&&(packcode!=""))
			{
				nooflps=parseFloat(itemtotalqty/packcode);
			}
			else
			{
				nooflps=itemtotalqty;
			}
			nlapiLogExecution('ERROR','nooflps',nooflps);
			nlapiLogExecution('ERROR','packcode',packcode);
			nlapiLogExecution('ERROR','vebizOrdNoId',vOrderNo);
			nlapiLogExecution('ERROR','uomId',uomId);
			nlapiLogExecution('ERROR','itemId',itemId);
			nlapiLogExecution('ERROR','itemName',itemName);
			nlapiLogExecution('ERROR','vContainerLpNo',vContainerLpNo);
			nlapiLogExecution('ERROR','qtysearchresults.length',qtysearchresults.length);

			MaxLpNoArray=GetMaxLPNoType('1', '5','');
			var lpMaxValue=MaxLpNoArray["maxLpPrefix"];
			nlapiLogExecution('ERROR','sMaxValue',lpMaxValue);
			var lpinternlaid=MaxLpNoArray["maxInternalId"];
			var company=MaxLpNoArray["maxCompany"];
			var companyDUNSnumber='';
			if(company!=null && company!='')
				companyDUNSnumber=GetCompanyDUNSnumber(company);

			if((qtysearchresults !=null))
			{
				var seriallineno=1;
				nlapiLogExecution('ERROR', 'AcySearch',qtysearchresults.length);
				nlapiLogExecution('ERROR', 'AcFirstLoop','AcFirstLoop');
				var lpMaxNo=lpMaxValue;
				for(var i=0; i < nooflps.toString();i++)
				{
					lpMaxNo = parseFloat(lpMaxNo) +parseFloat(1);
					nlapiLogExecution('ERROR', 'into for loop','done');
					nlapiLogExecution('ERROR', 'lpMaxNo',lpMaxNo);
					nlapiLogExecution('ERROR', 'companyDUNSnumber',companyDUNSnumber);
					var ResultText=GenerateLabelCode(uomId,parseFloat(lpMaxNo),companyDUNSnumber);
					var substring=ResultText.substring(10, 20);	
					var vContLpNo='PICK'+substring;
					nlapiLogExecution('ERROR', 'substring',substring);
					var MastLP=nlapiCreateRecord('customrecord_ebiznet_master_lp');		
					MastLP.setFieldValue('name', vContLpNo);
					MastLP.setFieldValue('custrecord_ebiz_lpmaster_lp', vContLpNo);	
					MastLP.setFieldValue('custrecord_ebiz_lpmaster_sscc', ResultText);
					MastLP.setFieldValue('custrecord_ebiz_lpmaster_item', itemName);
					MastLP.setFieldValue('custrecord_ebiz_lpmaster_masterlp', vContainerLpNo);
					var retktoval = nlapiSubmitRecord(MastLP);
					//genarateCartonLabelTemplate(vContainerLpNo,sOrderDetailsArray,itemName,labeltype,ResultText,seriallineno,nooflps,Label);
					nlapiLogExecution('ERROR', 'Acseriallineno',seriallineno);
					seriallineno++;
				}

				//Insert MaxLp number in LP Range List 
				InsertMaxLpNo(lpinternlaid,lpMaxNo);
			}
		}

	}



}
function InsertMaxLpNo(internalid,lpMaxValue)
{

	// update the new max LP in the custom record
	nlapiSubmitField('customrecord_ebiznet_lp_range', internalid,'custrecord_ebiznet_lprange_lpmax', parseFloat(lpMaxValue));
}

function GetMaxLPNoType(lpGenerationType, lpType,whsite)
{

	var maxLP = 1;
	var maxLPPrefix = "";
	var internalid="";
	var company="";
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpmax');
	columns[1] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix');
	columns[2] = new nlobjSearchColumn('custrecord_ebiznet_lprange_company');

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'is', lpGenerationType));
	filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lptype', null, 'anyof', [lpType]));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(whsite!=null && whsite!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_site', null, 'anyof', ['@NONE@', whsite]));

	var results = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, filters, columns);

	if(results != null)
	{
		if(results.length > 1)
		{
			alert('More records returned than expected');
			nlapiLogExecution('ERROR', 'GetMaxLPNo:LP Max Query returned more than 1 row');
		}
		else
		{
			// Incrementing the max LP number only if the search returns 1 row
			for (var t = 0; t < results.length; t++)
			{
				if (results[t].getValue('custrecord_ebiznet_lprange_lpmax') != null) 
				{
					internalid=results[t].getId();
					maxLP = results[t].getValue('custrecord_ebiznet_lprange_lpmax');
					maxLPPrefix = results[t].getValue('custrecord_ebiznet_lprange_lpprefix'); 
					company=results[t].getText('custrecord_ebiznet_lprange_company'); 
				}

			}
		}
		if((maxLP==null)||(maxLP==''))
		{
			maxLP=0;
		}
		maxLP = maxLP.toString();
		nlapiLogExecution('DEBUG', 'GetMaxLPNo:New MaxLP', maxLP);
		var maxArray = new Array();
		maxArray["maxLpPrefix"]=maxLPPrefix+parseFloat(maxLP);
		nlapiLogExecution('DEBUG', 'GetMaxLPNo:maxLpPrefix',maxArray["maxLpPrefix"]);
		maxArray["maxInternalId"]=internalid;
		maxArray["maxCompany"]=company;
		return maxArray;

	}
}

function GenerateLabelCode(uompackflag,lpvalue,duns)
{

	var finaltext="";

	var label="",uom="",ResultText="";
	nlapiLogExecution('ERROR', 'CreateMasterLPRecord uompackflag',uompackflag);
	nlapiLogExecution('ERROR', 'lpValue',lpvalue);
	//added by mahesh
	try 
	{	
		var lpMaxValue=lpvalue.toString();
		nlapiLogExecution('ERROR', 'lpMaxValueInnercode',parseFloat(lpMaxValue.length));
		var prefixlength=parseFloat(lpMaxValue.length);
		nlapiLogExecution('ERROR', 'prefixlength',prefixlength);
		if(prefixlength==0)
			label="000000000";
		else if(prefixlength==1)
			label="00000000"+lpMaxValue;
		else if(prefixlength==2)
			label="0000000"+lpMaxValue;
		else if(prefixlength==3)
			label="000000"+lpMaxValue;
		else if(prefixlength==4)
			label="00000"+lpMaxValue;
		else if(prefixlength==5)
			label="0000"+lpMaxValue;
		else if(prefixlength==6)
			label="000"+lpMaxValue;
		else if(prefixlength==7)
			label="00"+lpMaxValue;
		else if(prefixlength==8)
			label="0"+lpMaxValue;
		else if(prefixlength==9)
			label=lpMaxValue;


		nlapiLogExecution('ERROR', 'label', label);


		if(uompackflag == "1" || uompackflag == "EACH" ) 
			uom="0"; 
		else if(uompackflag == "3" || uompackflag == "PALLET") 
			uom="2";
		else
			uom="0";
		nlapiLogExecution('ERROR', 'uom', uom);
		nlapiLogExecution('ERROR', 'duns', duns);

		finaltext=uom+duns+label;
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord finaltext', finaltext);



		//to get chk digit
		var checkStr=finaltext;
		var ARL=0;
		var BRL=0;
		var CheckDigitValue="";
		for (i = checkStr.length-1;  i > 0;  i--)
		{
			ARL = ARL+parseFloat(checkStr.charAt(i));
			i--;
		}		
		ARL=ARL*3;
		for (i = checkStr.length-2;  i > 0;  i--)
		{
			BRL = BRL+parseFloat(checkStr.charAt(i));
			i--;
		}		
		var sumOfARLBRL=ARL+BRL;
		var CheckDigit=0;

		while(CheckDigit<10)
		{
			if(sumOfARLBRL%10==0)
			{ 
				CheckDigitValue=CheckDigit; 
				break; 
			} 

			sumOfARLBRL++;
			CheckDigit++;
		}
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord CheckDigit', CheckDigitValue.toString());
		ResultText="00"+finaltext+CheckDigitValue.toString();
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord ResultText', ResultText);


	} 
	catch (err) 
	{

	}
	return ResultText;
}
function GetCompanyDUNSnumber(company)
{
	var dunsfilters = new Array();
	dunsfilters[0] = new nlobjSearchFilter('custrecord_company', null, 'is', company);
	var dunscolumns = new Array();
	dunscolumns[0] = new nlobjSearchColumn('custrecord_compduns');			 
	var duns='';
	var dunssearchresults = nlapiSearchRecord('customrecord_ebiznet_company', null, dunsfilters, dunscolumns);
	if (dunssearchresults !=null && dunssearchresults !="") 
	{
		duns = dunssearchresults[0].getValue('custrecord_compduns');
		nlapiLogExecution('ERROR', 'duns', duns);
	}
	return duns;
}
