/***************************************************************************
                                                                 
   eBizNET Solutions Inc 
****************************************************************************
*
*  $Source: /cvs/products/eBizNET4.5/EoN/RF/Inbound/Suitelet/MainMenu.js,v $
*  $Revision: 1.5.4.5.4.4.4.10.2.1 $
*  $Date: 2014/09/10 15:49:20 $
*  $Author: sponnaganti $
*  $Name: t_eBN_2014_2_StdBundle_0_90 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: MainMenu.js,v $
*  Revision 1.5.4.5.4.4.4.10.2.1  2014/09/10 15:49:20  sponnaganti
*  Case# 201410277
*  Stnd Bundle issue fixed
*
*  Revision 1.5.4.5.4.4.4.10  2014/06/13 05:46:43  skavuri
*  Case# 20148882 (added Focus Functionality for Textbox)
*
*  Revision 1.5.4.5.4.4.4.9  2014/06/12 15:23:42  skavuri
*  Case # 20148880 (LinkButton functionality added to options in RF Screens)
*
*  Revision 1.5.4.5.4.4.4.8  2014/05/30 00:26:51  nneelam
*  case#  20148622
*  Stanadard Bundle Issue Fix.
*
*  Revision 1.5.4.5.4.4.4.7  2014/02/13 15:14:27  sponnaganti
*  case# 20127056
*  For Keyboard Enter button to work
*
*  Revision 1.5.4.5.4.4.4.6  2013/09/11 15:23:51  rmukkera
*  Case# 20124376
*
*
****************************************************************************/
function RFMainMenu(request, response){
	if (request.getMethod() == 'GET') {
		
		
		var ctx = nlapiGetContext();
	    var getLanguage = ctx.getPreference('LANGUAGE');
	    
	    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
    
	    var userAccountId = ctx.getCompany();
		nlapiLogExecution('DEBUG', 'userAccountId', userAccountId);
	    //alert(getLanguage);
		

		//var vlangaugae = "SPANISH";

		var st0,st1,st2,st3,st4,st5,st6,st7,st8,st9,st10;


		//if( getLanguage == 'es_ES')
		if( getLanguage == 'es_ES' || getLanguage == 'es_AR')
		{
			st0 = "MEN&#218; PRINCIPAL";
			st1 = "RECEPCI&#211;N";
			st2 = "RECOLECCION";
			st3 = "INVENTARIO";
			st4 = "EMBALAJE";
			st5 = "ENV&#205;O";
			st6 = "PRODUCCI&#211;N";
			st7 = "SALIR";
			st8 = "INGRESAR SELECCI&#211;N";
			st9 = "ENVIAR";
			st10 = "ANTERIOR";
		
			
		}
		else
		{
			st0 = "MAIN MENU";
			st1 = "RECEIVING";
			st2 = "PICKING";
			st3 = "INVENTORY";
			st4 = "PACKING";
			st5 = "SHIPPING";
			st6 = "PRODUCTION";
			st7 = "EXIT";
			st8 = "ENTER SELECTION";
			st9 = "SEND";
			st10 = "PREV";
		}
		//Case# 20148880 (LinkButton Functionality)
		var checkInURL_1 = nlapiResolveURL('SUITELET', 'customscript_rf_receiving_menu', 'customdeploy_rf_receiving_menu_di');
		var linkURL_1 = checkInURL_1; 
		var checkInURL_2 = nlapiResolveURL('SUITELET', 'customscript_rf_picking_menu', 'customdeploy_rf_picking_menu_di');
		var linkURL_2 = checkInURL_2; 
		var checkInURL_3 = nlapiResolveURL('SUITELET', 'customscript_rf_inventorymenu', 'customdeploy_rf_inventorymenu_di');
		var linkURL_3 = checkInURL_3; 
		var checkInURL_4 = nlapiResolveURL('SUITELET', 'customscript_rf_packing_menu', 'customdeploy_rf_packing_menu_di');
		var linkURL_4 = checkInURL_4; 
		var checkInURL_5 = nlapiResolveURL('SUITELET', 'customscript_rf_shipping_menu', 'customdeploy_rf_shipping_menu_di');
		var linkURL_5 = checkInURL_5; 
		var checkInURL_6 ='';
		if(userAccountId=="1157875")
			checkInURL_6 = nlapiResolveURL('SUITELET', 'customscript_rf_production_menu_hubpen', 'customdeploy_rf_production_menu_hubpen_d');
		else
			checkInURL_6 = nlapiResolveURL('SUITELET', 'customscript_rf_production_menu', 'customdeploy_rf_production_menu_di');
		var linkURL_6 = checkInURL_6;
		//case# 20127056 starts (Now form name is passed correctly in function to work keyboard enter button)
		var functionkeyHtml=getFunctionkeyScript('_rfmainmenu');
		//case# 20127056
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//html = html + " document.getElementById('selectoption').focus();";     //Case# 20148882   
		html = html + "</script>";		
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rfmainmenu' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 1." + st1;
		html = html + "				<td align = 'left'> 1. <a href='" + linkURL_1 + "' style='text-decoration: none'>" + st1 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 2." + st2;
		html = html + "				<td align = 'left'> 2. <a href='"+linkURL_2+"' style='text-decoration: none'>" + st2+"</a>";
		html = html + "				</td>";
		html = html + "			</tr> ";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 3." + st3;
		html = html + "				<td align = 'left'> 3. <a href='" + linkURL_3 + "' style='text-decoration: none'>" + st3 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 4." + st4;
		html = html + "				<td align = 'left'> 4. <a href='" + linkURL_4 + "' style='text-decoration: none'>" + st4 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 5." + st5;
		html = html + "				<td align = 'left'> 5. <a href='" + linkURL_5 + "' style='text-decoration: none'>" + st5 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr>";


		html = html + "			<tr>";
		//html = html + "				<td align = 'left'>6." + st6;
		html = html + "				<td align = 'left'> 6. <a href='" + linkURL_6 + "' style='text-decoration: none'>" + st6 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>99." + st7;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st8;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='selectoption' id='selectoption' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>SEND <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
	//	html = html + "				"	+ st10 + "<input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('selectoption').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}


	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var optedField = request.getParameter('selectoption');
		var POarray=new Array();
//		POarray["custparam_error"] = 'Invalid Option';
//		POarray["custparam_screenno"] = 'MainMenu';

		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;

		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);

		var context = nlapiGetContext();

		var userAccountId = context.getCompany();

		var st11,st12;
		if( getLanguage == 'es_ES')
		{
			st11 = "OPCI&#211;N V&#193;LIDA";
			st12 = "MainMenu";
		}
		else
		{
			st11 = "INVALID OPTION";
			st12 = "MainMenu";
		}



		POarray["custparam_error"] = st11;
		POarray["custparam_screenno"] = st12;





		if (optedField == '1') {
			response.sendRedirect('SUITELET', 'customscript_rf_receiving_menu', 'customdeploy_rf_receiving_menu_di', false, POarray);
		}
		else 
			if (optedField == '2') {
				response.sendRedirect('SUITELET', 'customscript_rf_picking_menu', 'customdeploy_rf_picking_menu_di', false, POarray);
			}
			else 
				if (optedField == '3') {
					response.sendRedirect('SUITELET', 'customscript_rf_inventorymenu', 'customdeploy_rf_inventorymenu_di', false, POarray);
				}
				else 
					if (optedField == '4') {
//						response.sendRedirect('SUITELET', 'customscript_rf_packing_orderno', 'customdeploy_rf_packing_orderno_di', false, optedField);
						response.sendRedirect('SUITELET', 'customscript_rf_packing_menu', 'customdeploy_rf_packing_menu_di', false, POarray);
					}
					else // Code written by Ganesh Kalla
						if (optedField == '5') {
							response.sendRedirect('SUITELET', 'customscript_rf_shipping_menu', 'customdeploy_rf_shipping_menu_di', false, POarray);
						}
						else 
							if (optedField == '6') {
								if(userAccountId=="1157875")
									response.sendRedirect('SUITELET', 'customscript_rf_production_menu_hubpen', 'customdeploy_rf_production_menu_hubpen_d', false, POarray);
								else
									response.sendRedirect('SUITELET', 'customscript_rf_production_menu', 'customdeploy_rf_production_menu_di', false, POarray);

							}
							else 
								if (optedField == '99') {
									nlapiLogExecution('DEBUG', 'optedField inside IF', POarray);                            	
									//response.write('<script language="javascript">window.location = "https://system.netsuite.com/pages/nllogoutnoback.jsp"</script>');
									response.write('<script language="javascript">window.location = "/pages/nllogoutnoback.jsp"</script>');
								}
								else 
								{
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								}

		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}
}
