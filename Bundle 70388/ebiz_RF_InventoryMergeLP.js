/***************************************************************************
 eBizNET Solutions 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_InventoryMergeLP.js,v $
 *     	   $Revision: 1.3.4.15.4.6.4.10.2.3 $
 *     	   $Date: 2015/07/23 13:39:07 $
 *     	   $Author: schepuri $
 *     	   $Name: t_eBN_2015_1_StdBundle_1_214 $
 *This Suitelet is meant to scan or enter the licence plate # for which the item is created.
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 * REVISION HISTORY
 * $Log: ebiz_RF_InventoryMergeLP.js,v $
 * Revision 1.3.4.15.4.6.4.10.2.3  2015/07/23 13:39:07  schepuri
 * case# 201413594
 *
 * Revision 1.3.4.15.4.6.4.10.2.2  2015/07/21 15:08:52  grao
 * 2015.2   issue fixes  201413117
 *
 * Revision 1.3.4.15.4.6.4.10.2.1  2014/11/14 11:57:47  skavuri
 * Case# 201410962 Std Bundle Issue Fixed
 *
 * Revision 1.3.4.15.4.6.4.10  2014/05/30 00:34:21  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.3.4.15.4.6.4.9  2014/05/23 06:46:51  gkalla
 * case#20148480
 * Sonic SB issue
 *
 * Revision 1.3.4.15.4.6.4.8  2014/02/13 14:52:09  sponnaganti
 * case# 20127150
 * (getting company based on location)
 *
 * Revision 1.3.4.15.4.6.4.7  2014/01/06 13:16:03  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.3.4.15.4.6.4.6  2013/06/05 22:11:22  spendyala
 * CASE201112/CR201113/LOG201121
 * FIFO field updating with data stamp is removed.
 *
 * Revision 1.3.4.15.4.6.4.5  2013/05/02 14:24:47  schepuri
 * updating remaning cube issue fix
 *
 * Revision 1.3.4.15.4.6.4.4  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.3.4.15.4.6.4.3  2013/04/16 15:06:21  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.3.4.15.4.6.4.2  2013/03/26 13:27:42  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.3.4.15.4.6.4.1  2013/03/01 14:34:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.3.4.15.4.6  2012/11/27 17:50:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Serial Capturing file.
 *
 * Revision 1.3.4.15.4.5  2012/11/09 14:14:58  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form 2012.2 branch.
 *
 * Revision 1.3.4.15.4.4  2012/11/01 14:55:42  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.3.4.15.4.3  2012/10/05 06:34:12  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting Multiple language into given suggestions
 *
 * Revision 1.3.4.15.4.2  2012/09/26 12:34:15  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.3.4.15.4.1  2012/09/21 15:01:30  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.3.4.15  2012/09/05 11:11:54  schepuri
 * CASE201112/CR201113/LOG201121
 * added date stamp
 *
 * Revision 1.3.4.14  2012/08/16 12:40:00  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fix as a part of 339 bundle i.e.,
 * merging the inventory with existing LP# in that location even though
 * we select option not to merging.
 *
 * Revision 1.3.4.13  2012/08/15 16:26:05  mbpragada
 * 20120490
 * Issue fix as a part of 339 bundle
 *
 * Revision 1.3.4.10  2012/07/04 07:10:29  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to creating inventory for Monobind SB is resolved.
 *
 * Revision 1.3.4.9  2012/06/11 22:25:10  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT and Prod issue fixes
 *
 * Revision 1.3.4.8  2012/05/16 23:34:17  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Create inventory issue fixed
 *
 * Revision 1.3.4.7  2012/04/18 09:55:25  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Invt Merge in Create Inventory
 *
 * Revision 1.3.4.6  2012/03/16 14:08:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.3.4.5  2012/02/29 10:36:48  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Invt Move
 *
 * Revision 1.3.4.4  2012/02/21 13:24:10  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.3.4.3  2012/02/12 12:59:45  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing related create inventory for batch item
 *
 * Revision 1.3.4.2  2012/02/07 12:52:23  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Inventory Move Changes
 *
 * Revision 1.4  2012/02/01 13:00:40  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fix is merged upto  1.3.4.1 from branch.
 *
 * Revision 1.3  2011/10/05 15:18:40  spendyala
 * CASE201112/CR201113/LOG201121
 * added with Merge Lp functionality
 *
 * Revision 1.2  2011/09/16 07:32:33  schepuri
 * CASE201112/CR201113/LOG201121
 * duplicate lps
 *
 * Revision 1.2  2011/09/16 07:22:56  schepuri
 * CASE201112/CR201113/LOG201121
 * duplicate lps
 *
 * 
 */

function InventoryMergeLp(request,response){
	
	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);
	
	if(request.getMethod()=='GET'){

		nlapiLogExecution('ERROR', 'INTO Inventory Merge Lp');

		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime')+ ' ' + request.getParameter('custparam_actualbegintimeampm');

		var getBinLocationId = request.getParameter('custparam_binlocationid');
		var getBinLocation = request.getParameter('custparam_binlocationname');
		var getItemId = request.getParameter('custparam_itemid');
		var getItem = request.getParameter('custparam_item');
		var getQuantity = request.getParameter('custparam_quantity');
		var getItemStatus = request.getParameter('custparam_itemstatus');
		var getItemStatusId = request.getParameter('custparam_itemstatusid');
		var getPackCode = request.getParameter('custparam_packcode');
		var getPackCodeId = request.getParameter('custparam_packcodeid');
		var getAdjustmentType = request.getParameter('custparam_adjustmenttype');
		var getAdjustmentTypeId = request.getParameter('custparam_adjustmenttypeid');
		var sitelocation = request.getParameter('custparam_sitelocation');		
		var locationId = request.getParameter('custparam_locationId');
		var getLp = request.getParameter('custparam_itemlp');
		var getInventoryId= request.getParameter('custparam_inventoryId');
		var tempflag=request.getParameter('custparam_flag');
		var notes=request.getParameter('custparam_notes');

		var getBatchId = request.getParameter('custparam_BatchId');
		var getBatchNo = request.getParameter('custparam_BatchNo');

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4,st5;
		if( getLanguage == 'es_ES')
		{

			st0 = "UBICACI&#211;N";
			st1 = "PLACA";
			st2 = "&#191;QUIERE UNIR LP:";
			st3 = "SI";
			st4 = "NO";


		}
		else
		{
			st0 = "LOCATION";
			st1 = "LP";
			st2 = "DO YOU WANT TO MERGE LP:";
			st3 = "YES";
			st4 = "NO";

		}



		var functionkeyHtml=getFunctionkeyScript('_rf_sitecomp'); 
		var html = "<html><head><title>" + st0 + "</title>";    
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";

		html = html + "<SCRIPT LANGUAGE='javascript'>";

		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		html = html + " </SCRIPT>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_sitecomp' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + ": <label>" + getLp + "</label>";
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnBinLocationId' value=" + getBinLocationId + ">";
		html = html + "				<input type='hidden' name='hdnBinLocation' value=" + getBinLocation + ">";
		html = html + "				<input type='hidden' name='hdnItemId' value=" + getItemId + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getQuantity + ">";
		html = html + "				<input type='hidden' name='hdnPackCode' value=" + getPackCode + ">";
		html = html + "				<input type='hidden' name='hdnPackCodeId' value=" + getPackCodeId + ">";
		html = html + "				<input type='hidden' name='hdnItemStatus' value=" + getItemStatus + ">";
		html = html + "				<input type='hidden' name='hdnItemStatusId' value=" + getItemStatusId + ">";
		html = html + "				<input type='hidden' name='hdnAdjustmentType' value=" + getAdjustmentType + ">";
		html = html + "				<input type='hidden' name='hdnAdjustmentTypeId' value=" + getAdjustmentTypeId + ">";
		html = html + "				<input type='hidden' name='hdnsiteLocation' value=" + sitelocation + ">";
		html = html + "				<input type='hidden' name='hdnLocationInternalid' value=" + locationId+ ">";
		html = html + "				<input type='hidden' name='hdnlp' value=" + getLp + ">";
		html = html + "				<input type='hidden' name='hdninventoryid' value=" + getInventoryId + ">";
		html = html + "				<input type='hidden' name='hdntempflag' value=" + tempflag + ">";
		html = html + "				<input type='hidden' name='hdnnotes' value=" + notes + ">";

		html = html + "				<input type='hidden' name='hdnBatchId' value=" + getBatchId + ">";
		html = html + "				<input type='hidden' name='hdnBatchNo' value=" + getBatchNo + ">";
		html = html + "			    <input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				</td>";
		html = html + "			</tr>";           
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2;
		html = html + "				</td>";      
		html = html + "			</tr>";  
		
		html = html + "				<td align = 'left'>"+ st3 +" <input name='cmdSend' type='submit' value='F8' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					"+ st4 +" <input name='cmdPrevious' type='submit' value='F7'/>";
			
		/*html = html + "				<td align = 'left'><input name='cmdSend' type='submit' value='YES' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					 <input name='cmdPrevious' type='submit' value='NO'/>";*/
		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else
	{

		var optedEvent = request.getParameter('cmdPrevious');	
		nlapiLogExecution('ERROR', 'optedEvent1 ::', optedEvent);
		var CIarray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		CIarray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR', 'getLanguage', CIarray["custparam_language"]);

		//	Get the Inveentory data from the previous screen, which is passed as a parameter		
		var getBinLocationId = request.getParameter('hdnBinLocationId');
		var getBinLocation = request.getParameter('hdnBinLocation');
		var getItemId = request.getParameter('hdnItemId');
		var getItem = request.getParameter('hdnItem');
		var getQuantity = request.getParameter('hdnQuantity');
		var getPackCode = request.getParameter('hdnPackCode');
		var getPackCodeId = request.getParameter('hdnPackCodeId');
		var getItemStatus = request.getParameter('hdnItemStatus');
		var getItemStatusId = request.getParameter('hdnItemStatusId');
		var getAdjustmentType = request.getParameter('hdnAdjustmentType');
		var getAdjustmentTypeId = request.getParameter('hdnAdjustmentTypeId');
		var SiteLocation = request.getParameter('hdnsiteLocation');
		var locationId = request.getParameter('hdnLocationInternalid');
		var inventoryId=request.getParameter('hdninventoryid');
		var getItemLp = request.getParameter('hdnlp');

		var getBatchId = request.getParameter('hdnBatchId');
		var getBatchNo = request.getParameter('hdnBatchNo');
		var getNotes = request.getParameter('hdnnotes');


		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime')+ ' ' + request.getParameter('custparam_actualbegintimeampm');
		var tempflag = request.getParameter('hdntempflag');
		var getBaseUOM = 'EACH';
		var RecordExists="N";

		var getActualEndDate = DateStamp();
		var getActualEndTime = TimeStamp();				

		nlapiLogExecution('ERROR', 'Inventory internal id ::', inventoryId);	

		CIarray["custparam_screenno"] = '17B';
		CIarray["custparam_binlocationid"] = getBinLocationId;
		CIarray["custparam_binlocationname"] = getBinLocation;
		CIarray["custparam_itemid"] = getItemId;
		CIarray["custparam_item"] = getItem;
		CIarray["custparam_quantity"] = getQuantity;
		CIarray["custparam_itemstatus"] = getItemStatus;
		CIarray["custparam_itemstatusid"] = getItemStatusId;
		CIarray["custparam_packcode"] = getPackCode;
		CIarray["custparam_packcodeid"] = getPackCodeId;
		CIarray["custparam_adjustmenttype"] = getAdjustmentType;
		CIarray["custparam_adjustmenttypeid"] = getAdjustmentTypeId;

		CIarray["custparam_actualbegindate"] = request.getParameter('custparam_actualbegindate');
		CIarray["custparam_locationId"] = locationId;
		var TimeArray = new Array();
		TimeArray = getActualBeginTime.split(' ');
		CIarray["custparam_actualbegintime"] = TimeArray[0];
		CIarray["custparam_actualbegintimeampm"] = TimeArray[1];
		CIarray["custparam_itemlp"] = getItemLp.toString();
		CIarray["custparam_sitelocation"] =SiteLocation;
		CIarray["custparam_BatchId"] =getBatchId;
		CIarray["custparam_BatchNo"]=getBatchNo;
		CIarray["custparam_notes"]=getNotes;
		nlapiLogExecution('ERROR', 'optedEvent ::', optedEvent);
		
		if (sessionobj!=context.getUser()) {
			try
			{

				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}
		if(optedEvent == 'F7')
		{		

			nlapiLogExecution('ERROR','FLAG',tempflag);

			if(tempflag != 1)
				response.sendRedirect('SUITELET', 'customscript_rf_create_invt_lp', 'customdeploy_rf_create_invt_lp_di', false, CIarray);

			else
			{
				var itemSubtype = nlapiLookupField('item', getItemId, ['recordType','custitem_ebizserialin']);
				CIarray["custparam_itemtype"] = itemSubtype.recordType;
				nlapiLogExecution('ERROR', 'itemSubtype.recordType =', itemSubtype);
				if (itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.recordType== 'serializedassemblyitem' || itemSubtype.custitem_ebizserialin == 'T') 
				{
					CIarray["custparam_inventoryId"] = "";
					response.sendRedirect('SUITELET', 'customscript_rf_create_invt_serialscan', 'customdeploy_rf_create_invt_serialscan_d', false, CIarray);
					return;
				}
				else
				{
					CreateInventory(getItemId, getItem, getItemStatusId, getItemStatus, getPackCodeId, getPackCode, 
							getAdjustmentTypeId, getAdjustmentType, getItemLp, getBinLocation, getBinLocationId, 
							getQuantity, getActualEndDate, getActualEndTime, getActualBeginDate, getActualBeginTime, 
							SiteLocation,locationId,getBatchId);


					response.sendRedirect('SUITELET', 'customscript_rf_create_invt_complete', 'customdeploy_rf_create_invt_complete_di', false, CIarray);
				}
			}
			/*
					nlapiLogExecution('ERROR', 'LP NOT FOUND');
					var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
					customrecord.setFieldValue('name', getItemLp);
					customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', getItemLp);
					var rec = nlapiSubmitRecord(customrecord, false, true);																					
					CreateInventory(getItemId, getItem, getItemStatusId, getItemStatus, getPackCodeId, getPackCode, getAdjustmentTypeId, getAdjustmentType, getItemLp, getBinLocation, getBinLocationId, getQuantity, getActualEndDate, getActualEndTime, getActualBeginDate, getActualBeginTime, SiteLocation,locationId);				                				                
					nlapiLogExecution('ERROR', 'Created', 'Successfully');				
					response.sendRedirect('SUITELET', 'customscript_rf_create_invt_complete', 'customdeploy_rf_create_invt_complete_di', false, CIarray);
			 */



		}
		else
		{
			//Case# 201410962 starts
			nlapiLogExecution("ERROR","getItemId",getItemId);
			var pallet_qty='';
			if(getItemId !='' && getItemId !=null && getItemId !='null')
			{
				var itemdimFilter=new Array();
				itemdimFilter.push(new nlobjSearchFilter('custrecord_ebizitemdims',null,'anyof',getItemId));
				itemdimFilter.push(new nlobjSearchFilter("custrecord_ebizuomskudim",null,"anyof",3));//3=Pallet

				var itemdimColumn=new Array();
				itemdimColumn[0]=new nlobjSearchColumn('custrecord_ebizqty');

				var searchrecItemDim=nlapiSearchRecord('customrecord_ebiznet_skudims',null,itemdimFilter,itemdimColumn);
				if(searchrecItemDim !='' && searchrecItemDim !=null && searchrecItemDim !='null')
				{
					pallet_qty=searchrecItemDim[0].getValue('custrecord_ebizqty');
					nlapiLogExecution("ERROR","pallet_qty",pallet_qty);
				}
			}
			//Case# 201410962 ends
			var InvrecLoad = nlapiLoadRecord('customrecord_ebiznet_createinv', inventoryId);
			var lot=InvrecLoad.getFieldText('custrecord_ebiz_inv_lot');
			var getQty = InvrecLoad.getFieldValue('custrecord_ebiz_qoh');
			var loc=InvrecLoad.getFieldValue('custrecord_ebiz_inv_loc');
			var TotalQty = parseFloat(getQty) + parseFloat(getQuantity);
			//Case# 201410962 starts
			nlapiLogExecution('DEBUG', 'TotalQty', TotalQty);
			if(TotalQty > pallet_qty)
			{
				CIarray["custparam_error"]="Item Quantity Should not be greater than Pallet Quantity";
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
				return;
			}
			//Case# 201410962 ends
			
			var newLP=InvrecLoad.getFieldValue('custrecord_ebiz_inv_lp');
			nlapiLogExecution('ERROR','newLP',newLP);
			InvrecLoad.setFieldValue('custrecord_ebiz_qoh', parseFloat(TotalQty).toFixed(4));
			InvrecLoad.setFieldValue('custrecord_wms_inv_status_flag', 19);
			InvrecLoad.setFieldValue('custrecord_ebiz_callinv', 'N');
			InvrecLoad.setFieldValue('custrecord_ebiz_displayfield', 'N');
			nlapiSubmitRecord(InvrecLoad, false, true);		
			var tasktype=10;

			var itemSubtype = nlapiLookupField('item', getItemId, ['recordType','custitem_ebizserialin']);
			CIarray["custparam_itemtype"] = itemSubtype.recordType;
			nlapiLogExecution('ERROR', 'itemSubtype.recordType =', itemSubtype);
			if (itemSubtype.recordType == 'serializedinventoryitem'  || itemSubtype.recordType == 'serializedassemblyitem'|| itemSubtype.custitem_ebizserialin == 'T') 
			{
				CIarray["custparam_inventoryId"] = inventoryId;
				CIarray["custparam_itemlp"] = newLP;
				response.sendRedirect('SUITELET', 'customscript_rf_create_invt_serialscan', 'customdeploy_rf_create_invt_serialscan_d', false, CIarray);
				return;
			}
			else
			{
				InvokeNSInventoryAdjustment(getItemId,getItemStatusId,loc,getQuantity,"",tasktype,getAdjustmentTypeId,lot);
				nlapiLogExecution('ERROR', 'Inventory Update Successfully::', 'Successfully');

				response.sendRedirect('SUITELET', 'customscript_rf_create_invt_complete', 'customdeploy_rf_create_invt_complete_di', false, CIarray);
			}
		}
			}
			catch (e)  {
				nlapiLogExecution('DEBUG', 'catch', e);
				CIarray["custparam_error"]=e;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');

			}
		}
		else
		{
			CIarray["custparam_screenno"] = '18';
			CIarray["custparam_error"] = 'LP ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
		}
	}

}


/**
 * @param ItemId
 * @param Item
 * @param ItemStatusId
 * @param ItemStatus
 * @param PackCodeId
 * @param PackCode
 * @param AdjustmentTypeId
 * @param AdjustmentType
 * @param ItemLP
 * @param BinLocation
 * @param BinLocationId
 * @param Quantity
 * @param ActualEndDate
 * @param ActualEndTime
 * @param ActualBeginDate
 * @param ActualBeginTime
 * @param SiteLocation
 * @param locationId
 */
function CreateInventory(ItemId, Item, ItemStatusId,ItemStatus, PackCodeId, PackCode, AdjustmentTypeId, AdjustmentType, ItemLP, BinLocation, BinLocationId, Quantity, ActualEndDate, ActualEndTime, ActualBeginDate, ActualBeginTime, SiteLocation,locationId,lot)
{
	var fifodate='';
	var fifovalue='';
	//create a inventory record
	var CreateInventoryRecord = nlapiCreateRecord('customrecord_ebiznet_createinv');
	nlapiLogExecution('ERROR', 'Create Inventory Record', 'Inventory Record');

	CreateInventoryRecord.setFieldValue('custrecordact_begin_date', ActualBeginDate);
	CreateInventoryRecord.setFieldValue('custrecord_act_end_date', ActualEndDate);
	//Adding fields to update time zones.
	CreateInventoryRecord.setFieldValue('custrecord_actualbegintime', ActualBeginTime);
	CreateInventoryRecord.setFieldValue('custrecord_actualendtime', ActualEndTime);
	CreateInventoryRecord.setFieldValue('custrecord_recordtime', TimeStamp());
	CreateInventoryRecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
	CreateInventoryRecord.setFieldValue('custrecord_current_date', DateStamp());
	CreateInventoryRecord.setFieldValue('custrecord_upd_date', DateStamp());
	CreateInventoryRecord.setFieldValue('custrecord_wms_inv_status_flag', 19);
	CreateInventoryRecord.setFieldValue('custrecord_invttasktype', 10);

	//Added for Item name and desc
	CreateInventoryRecord.setFieldValue('custrecord_sku', Item);
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_sku', ItemId);
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(Quantity).toFixed(4));
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_qoh', parseFloat(Quantity).toFixed(4));
//	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_lot', lot);
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_sku_status', ItemStatusId);
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_packcode', 1);//PackCodeId);

	var Itype,batchflg='F',itemfamId,itemgrpId;
	if(ItemId !=null && ItemId !=''){

		var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group',
		              'custitem_ebizserialin','custitem_ebiz_merge_fifodates'];
		var columns = nlapiLookupField('item', ItemId, fields);
		Itype = columns.recordType;					
		batchflg = columns.custitem_ebizbatchlot;
		itemfamId= columns.custitem_item_family;
		itemgrpId= columns.custitem_item_group;
	}

	if(lot!=null&&lot!="")
	{
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_lot', lot);	
		var rec=nlapiLookupField('customrecord_ebiznet_batch_entry',lot,['custrecord_ebizfifodate','custrecord_ebizexpirydate']);
		fifodate=rec.custrecord_ebizfifodate;
		var expdate=rec.custrecord_ebizexpirydate;
		if(fifodate!=null && fifodate!='')
			CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_fifo',fifodate);
		/*else
			CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_fifo',DateStamp());*/
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_expdate', expdate);
	}
	if(fifodate==null || fifodate=='')
	{
		//fifodate = FifovalueCheck(Itype,ItemId,itemfamId,itemgrpId,linenum,pointid,lot);
		fifodate = FifovalueCheck(Itype,ItemId,itemfamId,itemgrpId,'','',lot);
		fifovalue=fifodate;
	}
	else
	{
		fifovalue=fifodate;
	}
	if(fifovalue!=null && fifovalue!="" && (fifovalue != 'NaN/NaN/NaN'))
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
	else
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());
	/*else
	{
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_fifo',DateStamp());
	}*/
	/*
		var filtersAccNo = new Array();
        filtersAccNo[0] = new nlobjSearchFilter('custrecord_location', null, 'is', SiteLocation);
        var colsAcc = new Array();
        colsAcc[0]=new nlobjSearchColumn('custrecord_accountno');                
        var Accsearchresults = nlapiSearchRecord('customrecord_inventoryaccountno', null, filtersAccNo, colsAcc);              
        var varAccountNo = Accsearchresults[0].getValue('custrecord_accountno');                                
        nlapiLogExecution('ERROR', 'Account ',varAccountNo);
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_account_no', varAccountNo);//Account #);
	 */

	//code added on 01/02/12 by suman.
	//without passing this value system throughing an error stating that a/c no ref is invalid .
	//So to overcome that bug v are passing empty value.
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_account_no', '');
	//end of code added on 01/02/12

	//Added by suman as on 090812
	//updating Adjustment type and Company while creating invt record.
	nlapiLogExecution('ERROR','AdjustmentTypeId',AdjustmentTypeId+'/'+AdjustmentType);
	if(AdjustmentTypeId!=null&&AdjustmentTypeId!="")
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_adjusttype', AdjustmentTypeId);

	var compId=getCompanyId();
	//case# 20127150 starts (getting company based on location)
	var newLocationFilters = new Array();	
	newLocationFilters.push(new nlobjSearchFilter('internalid',null, 'is',locationId));	
	
	newLocationFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var newLocationColumn = new Array();	    	
	newLocationColumn.push(new nlobjSearchColumn('custrecordcompany'));		
	var newLocationResults = nlapiSearchRecord('location', null, newLocationFilters, newLocationColumn);
	if(newLocationResults!=null && newLocationResults!='')
	{
		compId=newLocationResults[0].getValue('custrecordcompany');
		nlapiLogExecution('ERROR','compId',compId);
	}
	//case# 20127150 end
	if(compId!=null&&compId!="")
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_company', compId);

	//end of code as of 090812.


	//code added on 130812 by suman.
	//Code to update user id and recorded time.
	var currentContext = nlapiGetContext();
	var currentUserID = currentContext.getUser();
	CreateInventoryRecord.setFieldValue('custrecord_updated_user_no',currentUserID);
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_updatedtime',TimeStamp());
	//end of code as of 130812.



	CreateInventoryRecord.setFieldValue('custrecord_ebiz_callinv', 'Y'); //Added by ramana for invoking NSInventory;
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_displayfield', 'N');

	nlapiLogExecution('ERROR', 'Location id fetched are', locationId);

	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_loc',locationId);

	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_lp', ItemLP);//Item LP #);
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_binloc', BinLocationId);//Bin Location);

	nlapiLogExecution('ERROR', 'Submitting Create Inventory Record', 'Inventory Record');

	//commit the record to NetSuite
	var CreateInventoryRecordId = nlapiSubmitRecord(CreateInventoryRecord, false, true);

	nlapiLogExecution('ERROR', 'Inventory Creation', 'Success');

	try 
	{
		nlapiLogExecution('ERROR', 'INTO MASTER LP INSERTION');
		var filtersmlp = new Array();
		filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', ItemLP);

		var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

		if (SrchRecord != null && SrchRecord.length > 0) 
		{
			nlapiLogExecution('ERROR', 'LP FOUND');			
		}
		else 
		{
			nlapiLogExecution('ERROR', 'LP NOT FOUND');
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
			customrecord.setFieldValue('name', ItemLP);
			customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', ItemLP);
			var rec = nlapiSubmitRecord(customrecord, false, true);
			nlapiLogExecution('ERROR', 'Master LP Insertion', 'Success');
		}
	} 
	catch (e) 
	{
		nlapiLogExecution('ERROR', 'Failed to Update/Insert into Master LP Record');
	}

	/*var arrDims = getSKUCubeAndWeight(ItemId, 1);
	var itemCube = 0;
	if (arrDims["BaseUOMItemCube"] != "" && (!isNaN(arrDims["BaseUOMItemCube"]))) 
	{
		var uomqty = ((parseFloat(Quantity))/(parseFloat(arrDims["BaseUOMQty"])));			
		itemCube = (parseFloat(uomqty) * parseFloat(arrDims["BaseUOMItemCube"]));
		nlapiLogExecution('ERROR', 'itemCube', itemCube);
	} 

	var vOldRemainingCube = GeteLocCube(BinLocationId);
	nlapiLogExecution('ERROR', 'vOldRemainingCube', vOldRemainingCube);

	var vTotalCubeValue = parseFloat(itemCube) - parseFloat(vOldRemainingCube);
	nlapiLogExecution('ERROR', 'vOldLocationRemainingCube', vTotalCubeValue);

	var retValue =  UpdateLocCube(BinLocationId,vTotalCubeValue);
	nlapiLogExecution('ERROR', 'Loc Cube Updation', 'Success');*/
}


function getCompanyId()
{
	try{
		var CompName;	
		var CompanyFilters = new Array();
		//CompanyFilters.push(new nlobjSearchFilter('internalid',null,'is',1));
		CompanyFilters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
		var CompanyColumn = new Array();	
		CompanyColumn.push(new nlobjSearchColumn('custrecord_company'));		
		var CompanyResults = nlapiSearchRecord('customrecord_ebiznet_company', null, CompanyFilters,CompanyColumn);	    
		if(CompanyResults!=null)
		{
			CompName=CompanyResults[0].getId();
		}
		return CompName;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in getCompanyId',exp);

	}
}

/*function DateStamp(){
	var now = new Date();
	return ((parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
}*/