/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inbound/Suitelet/ebiz_GenerateLocationList_SL.js,v $
 *     	   $Revision: 1.7.2.1 $
 *     	   $Date: 2012/07/11 08:33:16 $
 *     	   $Author: spendyala $
 *     	   $Name: t_NSWMS_2012_2_1_1 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_GenerateLocationList_SL.js,v $
 * Revision 1.7.2.1  2012/07/11 08:33:16  spendyala
 * CASE201112/CR201113/LOG201121
 * Added new column i.e., Trailer#.
 *
 * Revision 1.7  2011/12/26 22:50:01  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Checkin changes
 *
 * Revision 1.6  2011/07/21 05:44:03  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/

function ebiznet_GenerateLocation_list_SL(request, response){
	var list = nlapiCreateList('Generate Location List');
	list.setStyle('normal');
	//  list.setScript(239);    
	var column = list.addColumn('name', 'text', 'PO', 'LEFT');
	column.setURL(nlapiResolveURL('SUITELET', 'customscript_generatelocation', 'customdeploy_generatelocation'));
	column.addParamToURL('custparam_po', 'custrecord_ebiz_cntrl_no', true);
	column.addParamToURL('custparam_povalue', 'name', true);

	//Added new column for trailer on 9/7/12 by suman
	try
	{
		var columntrailer =list.addColumn('custrecord_ebiz_trailer_no', 'text', 'Trailer#', 'LEFT');
		columntrailer.setURL(nlapiResolveURL('SUITELET', 'customscript_generatelocation', 'customdeploy_generatelocation'));
		columntrailer.addParamToURL('custparam_trailer', 'custrecord_ebiz_trailer_no', true);
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','exception',exp);
	}
    //end of code as of 9/7/12.

	// list.addColumn('custrecord_ebiz_cntrl_no', 'text', 'Line#', 'LEFT');
	list.addColumn('custrecord_sku', 'text', 'Item', 'LEFT');
	list.addColumn('custrecord_lpno', 'text', 'LP #', 'LEFT');
	list.addColumn('custrecord_transport_lp', 'text', 'Cart LP #', 'LEFT');


	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);
	filters[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2,6]);
	filters[2] = new nlobjSearchFilter('custrecord_actbeginloc',null,'is','@NONE@');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_sku');
	columns[2] = new nlobjSearchColumn('custrecord_sku_status');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[4] = new nlobjSearchColumn('custrecord_lpno');
	columns[5] = new nlobjSearchColumn('custrecord_transport_lp');
	columns[6] = new nlobjSearchColumn('custrecord_ebiz_trailer_no');
	columns[0].setSort(false);
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	var po = new Object();


	for (var j = 0; searchresults != null && j < searchresults.length; j++) {

		po["name"] = searchresults[j].getValue('name');
		// po["custrecord_sku"] = searchresults[j].getValue('custrecord_sku');
		po["custrecord_sku"] = searchresults[j].getText('custrecord_sku');
		po["custrecord_lpno"] = searchresults[j].getValue('custrecord_lpno');
		po["custrecord_ebiz_cntrl_no"] = searchresults[j].getValue('custrecord_ebiz_cntrl_no');
		po["custrecord_transport_lp"] = searchresults[j].getValue('custrecord_transport_lp');
		po["custrecord_ebiz_trailer_no"] = searchresults[j].getValue('custrecord_ebiz_trailer_no');

		list.addRow(po);
	}


	nlapiLogExecution('DEBUG', 'END OF LIST');

	response.writePage(list);

}
