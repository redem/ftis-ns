/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Reports/Suitelet/Attic/ebiz_EmptyBinLocExcel_SL.js,v $
 *     	   $Revision: 1.1.2.1.4.1.4.3.8.1 $
 *     	   $Date: 2015/12/02 15:07:14 $
 *     	   $Author: skreddy $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_214 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 * * REVISION HISTORY
 * $Log: ebiz_EmptyBinLocExcel_SL.js,v $
 * Revision 1.1.2.1.4.1.4.3.8.1  2015/12/02 15:07:14  skreddy
 * case# 201415218
 * 2015.2 issue fix
 *
 * Revision 1.1.2.1.4.1.4.3  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.1.2.1.4.1.4.2  2013/02/26 14:54:02  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Merged from Boombah.
 *
 * Revision 1.1.2.1.4.1.4.1  2013/02/26 12:56:04  skreddy
 * CASE201112/CR201113/LOG201121
 * merged boombah changes
 *
 *
 *
 *****************************************************************************/
/**
 * This is the main function to Generate PDF and Excel  
 */
function EmptyBinLocExcel(request, response){
	if (request.getMethod() == 'GET') {

		try
		{

			var form = nlapiCreateForm('Empty Bin Location Report'); 
			/*var soid=request.getParameter('custparam_sotext');
			var fromdate=request.getParameter('custparam_fromdate');
			var todate=request.getParameter('custparam_todate');
			var vTaskStatus=request.getParameter('custparam_taskstatus');*/
			var id=request.getParameter('custparam_id');
			var vBinlocGroup=request.getParameter('custparam_binlocgroup');

			/*nlapiLogExecution('ERROR','soid',soid);
			nlapiLogExecution('ERROR','fromdate',fromdate);
			nlapiLogExecution('ERROR','vTaskStatus',vTaskStatus);
			nlapiLogExecution('ERROR','todate',todate);*/
			nlapiLogExecution('ERROR','id',id);

			var vRecCount=0;
			var result;
			var invtsearchresults =  getInventorySearchResults(-1,vBinlocGroup);

			var Searchresult = getEmptyBinLocSearchResults(-1,invtsearchresults,vBinlocGroup);
			//var Searchresult=getEmptyBinLocSearchResults(-1);
			if(Searchresult!=null&&Searchresult!="")
			{
				if(id=="1")
				{
					nlapiLogExecution('ERROR','Searchresult.length',Searchresult.length);
					GenerateExcel(Searchresult);
				}

			}
			else
			{

				showInlineMessage(form, 'Error', 'No Data Found');
				response.writePage(form);
			}


		}
		catch(exp)
		{
			nlapiLogExecution('ERROR','Exception ',exp);
		}

	}
	else
	{

	}
}



/**
 * To generate To Excel 
 * @parameters search results ,status value   
 */

function GenerateExcel(result)
{
	var EmptyBinLocation="";
	var LocGrpid="";
	var LocType="";
	var Cube="";
	var RemaniningCube="";
	var Height="";
	var Width="";
	var Depth="";
	var Weight="";

	var url;
	/*var ctx = nlapiGetContext();
	nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
	if (ctx.getEnvironment() == 'PRODUCTION') 
	{
		url = 'https://system.netsuite.com';			
	}
	else if (ctx.getEnvironment() == 'SANDBOX') 
	{
		url = 'https://system.sandbox.netsuite.com';				
	}*/
	nlapiLogExecution('ERROR', 'PDF URL',url);	
	var filefound = nlapiLoadFile('Images/LOGOCOMP.jpg'); 
	if (filefound) 
	{ 
		nlapiLogExecution('ERROR', 'Event', 'file;'+filefound.getId()); 
		var imageurl = filefound.getURL();
		nlapiLogExecution('ERROR','imageurl',imageurl);
		var finalimageurl = url + imageurl;//+';';
		//finalimageurl=finalimageurl+ '&expurl=T;';
		nlapiLogExecution('ERROR','imageurl',finalimageurl);
		finalimageurl=finalimageurl.replace(/&/g,"&amp;");

	} 
	else 
	{
		nlapiLogExecution('ERROR', 'Event', 'No file;');
	}

	//date
	//var sysdate=DateStamp();
	//var systime=TimeStamp();
	var Timez=calcTime('-5.00');
	nlapiLogExecution('ERROR','TimeZone',Timez);
	var datetime= new Date();
	datetime=datetime.toLocaleTimeString() ;


	var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<head><macrolist><macro id='myfooter'><p align='center'>Page <pagenumber/> of <totalpages/></p></macro></macrolist></head><body  font-size=\"7\"  size=\"A4-landscape\"  padding-top=\" 0mm\"   footer='myfooter' footer-height='20mm'>\n";

	//var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<head></head><body  font-size=\"7\"  size=\"A4-landscape\"    padding-top=\" 0mm\"   footer='myfooter' footer-height='20mm'>\n";
//	nlapiLogExecution('ERROR','xml0 ',"<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body font-size=\"7\">\n");

	var replaceChar =/\$|,|@|#|~|`|\%|\*|\^|\&|\+|\=|\-|\_|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|\./g;
	var strxml= "";


	if(result!=null && result!="")
	{
		var rowcount=0;

		var totalcount=result.length;
		nlapiLogExecution('ERROR','totalcount',totalcount);

		strxml += "<table width='100%'>";
		strxml += "<tr><td><table width='100%' >";
		strxml += "<tr><td valign='middle' align='left'><img src='" + finalimageurl + "'></img></td> ";
		strxml += "<td valign='middle' align='left'  style='font-size:large;'>";
		strxml += "Empty Bin Locations";
		strxml += "</td>";
		strxml += "<td align='right'>&nbsp;</td>";
		strxml += "<td align='right'>&nbsp;</td>";
		//	strxml += "<td align='right'>&nbsp;</td>";
		//	strxml += "<td align='right'>&nbsp;</td>";
		strxml += "<td align='right'>&nbsp;</td></tr></table></td></tr>";

		strxml += "<tr><td><table width='100%' valign='top' ><tr><td  colspan='2'>&nbsp;</td></tr></table>";
		strxml += "<tr><td><table width='100%' valign='top' ><tr><td  colspan='2'>&nbsp;</td></tr></table>";
		strxml += "<tr><td><table width='100%' valign='top' ><tr><td  colspan='2'>&nbsp;</td></tr></table>";
		//strxml += "<tr><td><table width='100%' valign='top' ><tr><td  colspan='2'>&nbsp;</td></tr></table>";

		strxml += "</td></tr>";
		strxml += "<tr><td><table width='100%' >";
		strxml += "<tr><td style='width:41px;font-size: 12px;' colspan='5'>";
		strxml += "<p align='right'>Date/Time:"+Timez+"</p>"; 
		strxml += "</td></tr>";
		strxml += "</table>";
		strxml += "</td></tr>";



		strxml += "<tr><td><table width='100%' valign='top' >";
		strxml += "<tr><td  colspan='5'>";

		strxml +="<table width='100%'>";
		strxml +="<tr style=\"font-weight:bold;background-color:gray;color:white;\">";

		strxml += "<td width='15%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
		strxml += "Bin Location";
		strxml += "</td>";

		strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
		strxml += "Location Group";
		strxml += "</td>";

		strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
		strxml += "Location Type";
		strxml += "</td>";

		strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
		strxml += "Cube";
		strxml += "</td>";

		strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
		strxml += "Remaining Cube";
		strxml += "</td>";

		strxml += "<td width='80%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
		strxml += "Height (in mts)";
		strxml += "</td>";

		strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
		strxml += "Width (in mts)";
		strxml += "</td>";

		strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
		strxml += "Depth (in mts)";
		strxml += "</td>";

		strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
		strxml += "Weight (in mts)";
		strxml += "</td></tr>";



		//for(var i=rowcount;i<result.length;i++)
		//{
		/*var EmptyBinLocation="";
		var LocGrpid="";
		var RemaniningCube="";*/


		var orderListArray=new Array();

		for(k=0;k<result.length;k++)
		{
			//nlapiLogExecution('ERROR', 'orderList[k]', result[k]); 
			var ordsearchresult = result[k];

			if(ordsearchresult!=null)
			{
				nlapiLogExecution('ERROR', 'ordsearchresult.length ', ordsearchresult.length); 
				for(var j=0;j<ordsearchresult.length;j++)
				{
					orderListArray[orderListArray.length]=ordsearchresult[j];				
				}
			}
		}
		for(var j = 0; j < orderListArray.length; j++){		
			//nlapiLogExecution('ERROR', 'orderListArray ', orderListArray[j]); 
			var searchresults = orderListArray[j];

			EmptyBinLocation=searchresults.getValue('name');
			LocGrpid=searchresults.getText('custrecord_inboundlocgroupid');
			LocType=searchresults.getText('custrecord_ebizlocationtype');
			Cube=searchresults.getValue('custrecord_cube');
			RemaniningCube=searchresults.getValue('custrecord_remainingcube');

			Height=searchresults.getValue('custrecord_height');
			Width=searchresults.getValue('custrecord_width');
			Depth=searchresults.getValue('custrecord_depth');
			Weight=searchresults.getValue('custrecordweight');

			//strxml += "<table width='100%' height='100%'>";
			strxml += "<tr>";
			strxml += "<td width='15%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += EmptyBinLocation;
			strxml += "</td>";

			strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += LocGrpid;
			strxml += "</td>";

			strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += LocType;
			strxml += "</td>";

			strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += Cube;
			strxml += "</td>";

			strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += RemaniningCube;
			strxml += "</td>";

			strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += Height;
			strxml += "</td>";

			strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += Width;
			strxml += "</td>";

			strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += Depth;
			strxml += "</td>";

			strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += Weight;
			strxml += "</td>";
			strxml += "</tr>";


		}


		strxml += "</table>";

		strxml += "</td></tr>";
		strxml += "</table>";

		strxml += "</td></tr>";

		strxml += "</table>"; 


		strxml += "\n</body>\n</pdf>";
		xml=xml+strxml;
		var exportXML = strxml;
		response.setContentType('PLAINTEXT', 'ExcelResults.xls', 'attachment');
		response.write(exportXML);	

	}
}

var tempEmptyBinLocResultsArray=new Array();
var tempInvtResultsArray=new Array();

function getEmptyBinLocSearchResults(maxno,invtsearchresults,vBinlocGroup)
{
	nlapiLogExecution('Error', 'Into getEmptyBinLocSearchResults');
	nlapiLogExecution('Error', 'maxno',maxno);
	nlapiLogExecution('Error', 'invtsearchresults',invtsearchresults);
	nlapiLogExecution('Error', 'vBinlocGroup',vBinlocGroup);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
//	filters.push(new nlobjSearchFilter('internalid','custrecord_ebiz_inv_binloc','anyof','@NONE@'));
//	code added from Boonbah account on 25Feb13 by santosh
	filters.push(new nlobjSearchFilter('internalid',null,'noneof',invtsearchresults));
	filters.push(new nlobjSearchFilter('custrecord_ebizlocationtype',null,'noneof',8));
	if(vBinlocGroup!=null && vBinlocGroup!='')
		filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid',null,'anyof',vBinlocGroup));
	//up to here
	if(maxno!=-1)
	{
		filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));
	}


	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_inboundlocgroupid');
	column[1] = new nlobjSearchColumn('id');
	column[1].setSort();   
	column[2] = new nlobjSearchColumn('name');
	column[3] = new nlobjSearchColumn('custrecord_ebizlocationtype');
	column[4] = new nlobjSearchColumn('custrecord_cube');
	column[5] = new nlobjSearchColumn('custrecord_remainingcube');
	column[6] = new nlobjSearchColumn('custrecord_height');
	column[7] = new nlobjSearchColumn('custrecord_width');
	column[8] = new nlobjSearchColumn('custrecord_depth');
	column[9] = new nlobjSearchColumn('custrecordweight');
	column[2].setSort();  

	//code added from Boonbah account on 25Feb13 by santosh
	var Binsearchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, column);
	if(Binsearchresults!=null)
	{
		if(Binsearchresults.length>=1000)
		{
			var maxno1=Binsearchresults[Binsearchresults.length-1].getValue(column[1]);
			nlapiLogExecution('Error', 'maxno1',maxno1);
			tempEmptyBinLocResultsArray.push(Binsearchresults);
			getEmptyBinLocSearchResults(maxno1,invtsearchresults,vBinlocGroup);
		}
		else
		{
			tempEmptyBinLocResultsArray.push(Binsearchresults);
		}
	}

	nlapiLogExecution('Error', 'Out of getEmptyBinLocSearchResults');
	return tempEmptyBinLocResultsArray;
	//upto here
}

function getInventorySearchResults(maxno,vBinlocGroup)
{
	nlapiLogExecution('Error', 'Into getInventorySearchResults');
	nlapiLogExecution('Error', 'maxno',maxno);
	nlapiLogExecution('Error', 'vBinlocGroup',vBinlocGroup);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [18,19]));
	if(vBinlocGroup!=null && vBinlocGroup!='')
		filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'anyof', vBinlocGroup));
	if(maxno!=-1)
	{
		filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));
	}

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	column[1] = new nlobjSearchColumn('internalid');
	column[1].setSort(false);
	var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, column);
	if(invtsearchresults!=null)
	{
		if(invtsearchresults.length>=1000)
		{
			//code added from Boonbah account on 25Feb13 by santosh
			nlapiLogExecution('Error', 'invtsearchresults',invtsearchresults.length);
			var maxno1=invtsearchresults[invtsearchresults.length-1].getId();
			for(var j=0;j<invtsearchresults.length;j++)
			{
				if(tempInvtResultsArray.indexOf(invtsearchresults[j].getValue('custrecord_ebiz_inv_binloc'))==-1)
					tempInvtResultsArray.push(invtsearchresults[j].getValue('custrecord_ebiz_inv_binloc'));
			}
			getInventorySearchResults(maxno1,vBinlocGroup);
		}
		else
		{
			for(var j=0;j<invtsearchresults.length;j++)
			{
				if(tempInvtResultsArray.indexOf(invtsearchresults[j].getValue('custrecord_ebiz_inv_binloc'))==-1)
					tempInvtResultsArray.push(invtsearchresults[j].getValue('custrecord_ebiz_inv_binloc'));
			}
		}
	}

	nlapiLogExecution('Error', 'Out of getInventorySearchResults');
	return tempInvtResultsArray;
	//up to here
}

function calcTime(offset) {

	//var vtime=new Date();

	//nlapiLogExecution('ERROR', 'TimeOffset', vtime.getTimezoneOffset());

	// create Date object for current location
	d = new Date();

	// convert to msec
	// add local time zone offset 
	// get UTC time in msec
	utc = d.getTime() + (d.getTimezoneOffset() * 60000);

	// create new Date object for different city
	// using supplied offset
	nd = new Date(utc + (3600000*offset));

	// return time as a string
	//return "The local time in " + city + " is " + nd.toLocaleString();

	var stringDt=((parseFloat(nd.getMonth()) + 1) + '/' + (parseFloat(nd.getDate())) + '/' + nd.getFullYear());

	var timestamp;
	var a_p = "";

	//Getting time in hh:mm tt format.
	var curr_hour = nd.getHours();
	var curr_min = nd.getMinutes();

	// determining the am/pm indicator
	if (curr_hour < 12)
		a_p = "am";
	else
		a_p = "pm";

	// finalizing hours depending on 24hr clock
	if (curr_hour == 0)
		curr_hour = 12;
	else if(curr_hour > 12)
		curr_hour -= 12;

	if (curr_min.length == 1)
		curr_min = "0" + curr_min;

	//Adding fields to update time zones.
	timestamp = curr_hour + ":" + curr_min + " " + a_p;

	return stringDt + " " + timestamp;

}

