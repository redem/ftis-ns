/***************************************************************************
??????????????????????????????eBizNET
??????????????????????? eBizNET Solutions Inc
****************************************************************************
*
*? $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Suitelet/ebiz_SkuSummaryReport_SL.js,v $
*? $Revision: 1.3.4.1.4.1.4.2 $
*? $Date: 2013/05/14 14:49:18 $
*? $Author: grao $
*? $Name: t_NSWMS_2013_1_3_23 $
*
* DESCRIPTION
*? Functionality
*
* REVISION HISTORY
*? $Log: ebiz_SkuSummaryReport_SL.js,v $
*? Revision 1.3.4.1.4.1.4.2  2013/05/14 14:49:18  grao
*? CASE201112/CR201113/LOG201121
*? Standard bundle issues fixes
*?
*? Revision 1.3.4.1.4.1.4.1  2013/05/08 15:11:30  grao
*? CASE201112/CR201113/LOG201121
*? Standard bundle issues fixes
*?
*? Revision 1.3.4.1.4.1  2012/11/01 14:55:15  schepuri
*? CASE201112/CR201113/LOG201121
*? Decimal Qty Conversions
*?
*? Revision 1.3.4.1  2012/05/14 14:53:22  spendyala
*? CASE201112/CR201113/LOG201121
*? Issue related to display sku summary.
*?
*? Revision 1.3  2011/10/31 09:23:10  spendyala
*? CASE201112/CR201113/LOG201121
*? got the inventory by location wise from inventory mater .
*?
*
****************************************************************************/



/**
 * function used to get the summary of the sku.
 */
function SkuSummary(request, response)
{
	if (request.getMethod() == 'GET') 
	{
		//create form
		var form = nlapiCreateForm('Item Summary');

		form.setScript('customscript_inventoryclientvalidations');
		
		//adding From date 
		form.addField('custpage_fromdate', 'date', 'From Date').setMandatory( true );

		//adding a select field of type item
		form.addField('custpage_items', 'multiselect', 'Item','item').setMandatory( true );

		//adding To date
		form.addField('custpage_todate', 'date', 'To Date').setMandatory( true );

		//adding button
		form.addSubmitButton('Display');

		response.writePage(form);
	}
	else
	{
		var sku = request.getParameter('custpage_items');
		var fromdate = request.getParameter('custpage_fromdate');
		var todate = request.getParameter('custpage_todate');


		nlapiLogExecution('ERROR','fromdate',fromdate);
		nlapiLogExecution('ERROR','todate',todate);
		var skuArray=new Array();
		skuArray=sku.split('');
		nlapiLogExecution('ERROR','skuArray',skuArray);

		//create form
		var form = nlapiCreateForm('Item Summary');
		form.setScript('customscript_inventoryclientvalidations');

		//adding From date 
		form.addField('custpage_fromdate', 'date', 'From Date').setMandatory( true );

		//adding a select field of type item
		form.addField('custpage_items', 'multiselect', 'Item','item').setMandatory( true );
//		form.addField('custpage_items', 'select', 'Item','item');

		//adding To date
		form.addField('custpage_todate', 'date', 'To Date').setMandatory( true );


		//adding button
		form.addSubmitButton('Display');

		//create a sublist to display values.
		var sublist = form.addSubList("custpage_results", "list", "Item List");

		sublist.addField("custpage_item", "select", "Item",'item').setDisplayType('inline');
		sublist.addField("custpage_invtadj", "text", "Invt Adjustment");
		sublist.addField("custpage_chkn", "text", "CHKN");
		sublist.addField("custpage_putw", "text", "PUTW");
		sublist.addField("custpage_xferin", "text", "XFER IN");
		sublist.addField("custpage_totalinbound", "text", "Total Inbound");
		sublist.addField("custpage_focreation", "text", "FO Creation");
		sublist.addField("custpage_pick", "text", "PICK");
		sublist.addField("custpage_ship", "text", "SHIP");
		sublist.addField("custpage_xferout", "text", "XFER OUT");
		sublist.addField("custpage_totaloutbound", "text", "Total Outbound");
		sublist.addField("custpage_move", "text", "MOVE");
		sublist.addField("custpage_replenishment", "text", "Replenishment");
		sublist.addField("custpage_invtransanction", "text", "Inventory According To Transaction");
		sublist.addField("custpage_sysinvt", "text", "System Inventory");
		sublist.addField("custpage_diff", "text", "Difference");
		sublist.addField("custpage_matching", "text", "Matching");

		//dynamic creating the sublist field whose location has make ware house site as true.
		var locationfilter=new Array();
		locationfilter.push(new nlobjSearchFilter('custrecord_ebizwhsite',null,'is','T'));

		var locationcolumn=new Array();
		locationcolumn[0]=new nlobjSearchColumn('name').setSort('true');

		var locationsearchrecord=nlapiSearchRecord('location',null,locationfilter,locationcolumn);

		var AllLocation=new Array();
		var replaceChar =/[^a-zA-Z 0-9]|\s+/g;
		for ( var locCount = 0; locCount < locationsearchrecord.length; locCount++)
		{
			var str=locationsearchrecord[locCount].getValue('name');
			//var name=str.replace(/\s/g, "");
			var name=str.replace(replaceChar, "");
			nlapiLogExecution('ERROR','name',name.toLowerCase());
			var id="custpage_"+name.toLowerCase();
			sublist.addField(id, "text", str);
			AllLocation[locCount]=new Array();
			AllLocation[locCount][0]=str;
			AllLocation[locCount][1]=name.toLowerCase();
		}

		for ( var skucount = 0; skucount < skuArray.length; skucount++)
		{
			sku=skuArray[skucount];
			nlapiLogExecution('ERROR','sku'+skucount,sku);

			//getting results from invt adjt
			var invtadjtfilter = new Array();

			if(sku != null && sku != "")
			{
				invtadjtfilter.push(new nlobjSearchFilter('custrecord_ebizskuno', null,'anyof', sku));
			}
			if((fromdate != null && fromdate != "")&&(todate != null && todate != ""))
			{
				invtadjtfilter.push(new nlobjSearchFilter('custrecord_ebiz_recorddate', null, 'within', fromdate, todate));
			}

			var InvtAdjcolumns = new Array();
			//InvtAdjcolumns[0]=new nlobjSearchColumn('custrecord_ebiz_tasktype',null,'group');
			InvtAdjcolumns[0] = new nlobjSearchColumn('custrecord_ebiz_adjustqty',null,'sum');

			var InvtAdjtsearchresults = nlapiSearchRecord('customrecord_ebiznet_invadj', null, invtadjtfilter, InvtAdjcolumns);

			var InvtAdjQty="";
			InvtAdjQty=InvtAdjtsearchresults[0].getValue('custrecord_ebiz_adjustqty',null,'sum');

			if(InvtAdjQty==null||InvtAdjQty=="")
				InvtAdjQty=0;
			nlapiLogExecution('ERROR','InvtAdjQty:',InvtAdjQty);

			//getting results from opentask
			var opentaskfilter = new Array();

			if(sku != null && sku != "")
			{
				opentaskfilter.push(new nlobjSearchFilter('custrecord_ebiz_sku_no', null,'is', sku));
			}
			if((fromdate != null && fromdate != "")&&(todate != null && todate != ""))
			{
				opentaskfilter.push(new nlobjSearchFilter('custrecordact_begin_date', null, 'within', fromdate, todate));
			}

			var opentaskcolumns = new Array();
			opentaskcolumns[0]=new nlobjSearchColumn('custrecord_tasktype',null,'group');
			opentaskcolumns[1] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');

			var opentasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, opentaskfilter, opentaskcolumns);

			var OpentaskChknQty,OpentaskPutwQty,OpentaskPickQty,OpentaskShipQty,OpentaskXferQty,OpentaskMoveQty,OpentaskRplnQty="";
			if(opentasksearchresults!=null)
			{
				nlapiLogExecution('ERROR','opentasksearchresults',opentasksearchresults.length);
				for ( var count = 0; count < opentasksearchresults.length; count++) 
				{
					var temp=opentasksearchresults[count].getText('custrecord_tasktype',null,'group');
					nlapiLogExecution('ERROR','temp',temp);
					if(temp=='CHKN')
						OpentaskChknQty=opentasksearchresults[count].getValue('custrecord_expe_qty',null,'sum');
					else if(temp=='PUTW')
						OpentaskPutwQty=opentasksearchresults[count].getValue('custrecord_expe_qty',null,'sum');
					else if(temp=='PICK')
						OpentaskPickQty=opentasksearchresults[count].getValue('custrecord_expe_qty',null,'sum');
					else if(temp=='SHIP')
						OpentaskShipQty=opentasksearchresults[count].getValue('custrecord_expe_qty',null,'sum');
					else if(temp == 'XFER')
						OpentaskXferQty=opentasksearchresults[count].getValue('custrecord_expe_qty',null,'sum');
					else if(temp == 'MOVE')
						OpentaskMoveQty=opentasksearchresults[count].getValue('custrecord_expe_qty',null,'sum');
					else if(temp == 'RPLN')
						OpentaskRplnQty=opentasksearchresults[count].getValue('custrecord_expe_qty',null,'sum');
				}
			}
			nlapiLogExecution('ERROR','OpentaskChknQty:',OpentaskChknQty);
			nlapiLogExecution('ERROR','OpentaskPutwQty:',OpentaskPutwQty);
			nlapiLogExecution('ERROR','OpentaskPickQty:',OpentaskPickQty);
			nlapiLogExecution('ERROR','OpentaskShipQty:',OpentaskShipQty);
			nlapiLogExecution('ERROR','OpentaskXferQty:',OpentaskXferQty);
			nlapiLogExecution('ERROR','OpentaskMoveQty:',OpentaskMoveQty);
			nlapiLogExecution('ERROR','OpentaskRplnQty:',OpentaskRplnQty);


			//getting results from closedtask
			var closedtaskfilter = new Array();

			if(sku != null && sku != "")
			{
				closedtaskfilter.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_sku_no', null,'is', sku));
			}
			if((fromdate != null && fromdate != "")&&(todate != null && todate != ""))
			{
				closedtaskfilter.push(new nlobjSearchFilter('custrecord_ebiztask_act_begin_date', null, 'within', fromdate, todate));
			}

			var Closedtaskcolumns = new Array();
			Closedtaskcolumns[0]=new nlobjSearchColumn('custrecord_ebiztask_tasktype',null,'group');
			Closedtaskcolumns[1] = new nlobjSearchColumn('custrecord_ebiztask_act_qty',null,'sum');

			var Closedtasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, closedtaskfilter, Closedtaskcolumns);

			var ClosetaskChknQty,ClosetaskPutwQty,ClosetaskPickQty,ClosetaskXferQty,ClosetaskShipQty,ClosetaskMoveQty,ClosetaskRplnQty="";
			if(Closedtasksearchresults!=null)
			{
				nlapiLogExecution('ERROR','Closedtasksearchresults',Closedtasksearchresults.length);
				for ( var count1 = 0; count1 < Closedtasksearchresults.length; count1++) 
				{
					var temp=Closedtasksearchresults[count1].getText('custrecord_ebiztask_tasktype',null,'group');
					nlapiLogExecution('ERROR','Closedtemp',temp);
					if(temp=='CHKN')
						ClosetaskChknQty=Closedtasksearchresults[count1].getValue('custrecord_ebiztask_act_qty',null,'sum');
					else if(temp=='PUTW')
						ClosetaskPutwQty=Closedtasksearchresults[count1].getValue('custrecord_ebiztask_act_qty',null,'sum');
					else if(temp=='PICK')
						ClosetaskPickQty=Closedtasksearchresults[count1].getValue('custrecord_ebiztask_act_qty',null,'sum');
					else if(temp == 'SHIP')
						ClosetaskShipQty=Closedtasksearchresults[count1].getValue('custrecord_ebiztask_act_qty',null,'sum');
					else if(temp == 'XFER')
						ClosetaskXferQty=Closedtasksearchresults[count1].getValue('custrecord_ebiztask_act_qty',null,'sum');
					else if(temp == 'MOVE')
						ClosetaskMoveQty=Closedtasksearchresults[count1].getValue('custrecord_ebiztask_act_qty',null,'sum');
					else if(temp == 'RPLN')
						ClosetaskRplnQty=Closedtasksearchresults[count1].getValue('custrecord_ebiztask_act_qty',null,'sum');

				}
			}
			nlapiLogExecution('ERROR','ClosetaskChknQty:',ClosetaskChknQty);
			nlapiLogExecution('ERROR','ClosetaskPutwQty:',ClosetaskPutwQty);
			nlapiLogExecution('ERROR','ClosetaskPickQty:',ClosetaskPickQty);
			nlapiLogExecution('ERROR','ClosetaskShipQty:',ClosetaskShipQty);
			nlapiLogExecution('ERROR','ClosetaskXferQty:',ClosetaskXferQty);
			nlapiLogExecution('ERROR','ClosetaskMoveQty:',ClosetaskMoveQty);
			nlapiLogExecution('ERROR','ClosetaskRplnQty:',ClosetaskRplnQty);

			if(OpentaskChknQty== null||OpentaskChknQty=="")
				OpentaskChknQty=0;
			if(OpentaskPutwQty== null||OpentaskPutwQty=="")
				OpentaskPutwQty=0;
			if(OpentaskPickQty== null||OpentaskPickQty=="")
				OpentaskPickQty=0;
			if(OpentaskShipQty== null||OpentaskShipQty=="")
				OpentaskShipQty=0;
			if(OpentaskXferQty== null||OpentaskXferQty=="")
				OpentaskXferQty=0;
			if(OpentaskMoveQty== null||OpentaskMoveQty=="")
				OpentaskMoveQty=0;
			if(OpentaskRplnQty== null||OpentaskRplnQty=="")
				OpentaskRplnQty=0;

			if(ClosetaskChknQty== null||ClosetaskChknQty=="")
				ClosetaskChknQty=0;
			if(ClosetaskPutwQty== null||ClosetaskPutwQty=="")
				ClosetaskPutwQty=0;
			if(ClosetaskPickQty== null||ClosetaskPickQty=="")
				ClosetaskPickQty=0;
			if(ClosetaskShipQty== null||ClosetaskShipQty=="")
				ClosetaskShipQty=0;
			if(ClosetaskXferQty== null||ClosetaskXferQty=="")
				ClosetaskXferQty=0;
			if(ClosetaskMoveQty== null||ClosetaskMoveQty=="")
				ClosetaskMoveQty=0;
			if(ClosetaskRplnQty== null||ClosetaskRplnQty=="")
				ClosetaskRplnQty=0;

			var ChknQty=parseFloat(OpentaskChknQty)+parseFloat(ClosetaskChknQty);
			var PutwQty=parseFloat(OpentaskPutwQty)+parseFloat(ClosetaskPutwQty);
			var PickQty=parseFloat(OpentaskPickQty)+parseFloat(ClosetaskPickQty);
			var ShipQty=parseFloat(OpentaskShipQty)+parseFloat(ClosetaskShipQty);
			var XferQty=parseFloat(OpentaskXferQty)+parseFloat(ClosetaskXferQty);
			var MoveQty=parseFloat(OpentaskMoveQty)+parseFloat(ClosetaskMoveQty);
			var RplnQty=parseFloat(OpentaskRplnQty)+parseFloat(ClosetaskRplnQty);
			var TotalInBoundQty=parseFloat(InvtAdjQty)+PutwQty;
			var TotalOutBoundQty=PickQty;
			var InvtTransactionQty=TotalInBoundQty-TotalOutBoundQty;

			nlapiLogExecution('ERROR','ChknQty:',ChknQty);
			nlapiLogExecution('ERROR','PutwQty:',PutwQty);
			nlapiLogExecution('ERROR','PickQty:',PickQty);
			nlapiLogExecution('ERROR','ShipQty:',ShipQty);
			nlapiLogExecution('ERROR','XferQty:',XferQty);
			nlapiLogExecution('ERROR','MoveQty:',MoveQty);
			nlapiLogExecution('ERROR','RplnQty:',RplnQty);
			nlapiLogExecution('ERROR','TotalInBoundQty:',TotalInBoundQty);
			nlapiLogExecution('ERROR','TotalOutBoundQty:',TotalOutBoundQty);
			nlapiLogExecution('ERROR','InvtTransactionQty:',InvtTransactionQty);


			//getting records from fullfillment order
			var fullfillment_filter=new Array();
			if(sku != null && sku != "")
			{
				fullfillment_filter.push(new nlobjSearchFilter('custrecord_ebiz_linesku', null,'anyof', sku));
			}
			if((fromdate != null && fromdate != "")&&(todate != null && todate != ""))
			{
				fullfillment_filter.push(new nlobjSearchFilter('custrecord_record_linedate', null, 'within', fromdate, todate));
			}

			var fullfillmentcolumns = new Array();
			fullfillmentcolumns[0] = new nlobjSearchColumn('custrecord_ord_qty',null,'sum');

			var fullfillmentsearchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, fullfillment_filter, fullfillmentcolumns);

			var fullfillmentQty = "";
			fullfillmentQty=fullfillmentsearchresults[0].getValue('custrecord_ord_qty',null,'sum');

			if(fullfillmentQty=="")
			{
				fullfillmentQty=0;
			}
			nlapiLogExecution('ERROR','fullfillmentQty:',fullfillmentQty);

			//getting results from create invt
			var createinvtfilter = new Array();

			if(sku != null && sku != "")
			{
				createinvtfilter.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null,'anyof', sku));
			}
			createinvtfilter.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null,'anyof', ['3','19']));//3=Putaway complete,19=storage 

			var CreateInvtcolumns = new Array();
//			CreateInvtcolumns[0]=new nlobjSearchColumn('custrecord_invttasktype',null,'group');
			CreateInvtcolumns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh',null,'sum');

			var CreateInvtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, createinvtfilter, CreateInvtcolumns);

			var CreateInvtQty="";
			CreateInvtQty=CreateInvtsearchresults[0].getValue('custrecord_ebiz_qoh',null,'sum');

			if(CreateInvtQty==null||CreateInvtQty=="")
				CreateInvtQty=0;
			nlapiLogExecution('ERROR','CreateInvtQty:',CreateInvtQty);
			var differenceInQty=parseFloat(CreateInvtQty)-InvtTransactionQty;

			var MatchingStr="";
			if(differenceInQty==0)
				MatchingStr='Matching';
			else
				MatchingStr='NotMatching';
			nlapiLogExecution('ERROR','differenceInQty:',differenceInQty);
			nlapiLogExecution('ERROR','MatchingStr:',MatchingStr);


			//getting qty depending upon location from item master

			var itemmaster= nlapiLoadRecord('inventoryitem',sku);			
			var count=itemmaster.getLineItemCount('locations');
			var items=new Array();
			for(var x=0;x<count;x++)
			{
				items[x]=new Array();
				items[x][0]=itemmaster.getLineItemValue('locations','location_display',x+1);
				items[x][1]=itemmaster.getLineItemValue('locations','quantityonhand',x+1);
			}

			var countline=skucount+1;
			form.getSubList('custpage_results').setLineItemValue('custpage_item', countline, sku );
			form.getSubList('custpage_results').setLineItemValue('custpage_invtadj', countline, InvtAdjQty.toString());
			form.getSubList('custpage_results').setLineItemValue('custpage_chkn', countline, ChknQty.toString());
			form.getSubList('custpage_results').setLineItemValue('custpage_putw', countline, PutwQty.toString());
			form.getSubList('custpage_results').setLineItemValue('custpage_xferin', countline, XferQty.toString());
			form.getSubList('custpage_results').setLineItemValue('custpage_totalinbound', countline, TotalInBoundQty.toString());
			form.getSubList('custpage_results').setLineItemValue('custpage_focreation', countline, fullfillmentQty.toString());
			form.getSubList('custpage_results').setLineItemValue('custpage_pick', countline, PickQty.toString());
			form.getSubList('custpage_results').setLineItemValue('custpage_ship', countline, ShipQty.toString());
			form.getSubList('custpage_results').setLineItemValue('custpage_xferout', countline, XferQty.toString());
			form.getSubList('custpage_results').setLineItemValue('custpage_totaloutbound', countline, TotalOutBoundQty.toString());
			form.getSubList('custpage_results').setLineItemValue('custpage_move', countline, MoveQty.toString());
			form.getSubList('custpage_results').setLineItemValue('custpage_replenishment', countline, RplnQty.toString());
			form.getSubList('custpage_results').setLineItemValue('custpage_invtransanction', countline, InvtTransactionQty.toString());
			form.getSubList('custpage_results').setLineItemValue('custpage_sysinvt', countline, CreateInvtQty.toString());
			form.getSubList('custpage_results').setLineItemValue('custpage_diff', countline, differenceInQty.toString());
			form.getSubList('custpage_results').setLineItemValue('custpage_matching', countline, MatchingStr.toString());
			for ( var locCounts = 0; locCounts < AllLocation.length; locCounts++) 
			{
				for ( var itemCounts = 0; itemCounts < items.length; itemCounts++) 
				{
					if(AllLocation[locCounts][0]==items[itemCounts][0])
						form.getSubList('custpage_results').setLineItemValue('custpage_'+AllLocation[locCounts][1], countline, items[itemCounts][1]);
				}
			}
		}
		response.writePage(form);
	}
}


