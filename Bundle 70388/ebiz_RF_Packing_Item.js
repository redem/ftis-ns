/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/ebiz_RF_Packing_Item.js,v $
 *     	   $Revision: 1.2.4.8.4.2.4.5.2.1 $
 *     	   $Date: 2015/11/24 11:29:34 $
 *     	   $Author: aanchal $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Packing_Item.js,v $
 * Revision 1.2.4.8.4.2.4.5.2.1  2015/11/24 11:29:34  aanchal
 * 2015.2 issue fix
 * 201415757
 *
 * Revision 1.2.4.8.4.2.4.5  2014/09/19 16:27:30  sponnaganti
 * Case# 201410412
 * DCD SB Issue fix
 *
 * Revision 1.2.4.8.4.2.4.4  2014/06/13 12:51:50  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.2.4.8.4.2.4.3  2014/05/30 00:41:03  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.2.4.8.4.2.4.2  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.2.4.8.4.2.4.1  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.2.4.8.4.2  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.2.4.8.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.2.4.8  2012/08/01 07:16:42  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Picking Routing based on Loc group seq
 *
 * Revision 1.2.4.7  2012/06/15 10:08:49  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Packing changes
 *
 * Revision 1.2.4.6  2012/06/02 09:29:00  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to packing resolved.
 *
 * Revision 1.2.4.5  2012/05/17 22:52:39  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Packing
 *
 * Revision 1.2.4.4  2012/04/20 07:35:04  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Packing
 *
 * Revision 1.2.4.3  2012/04/17 10:52:40  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Packing
 *
 * Revision 1.2.4.1  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.2  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.1  2011/04/19 14:19:06  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * RF Packing Functionality
 *
 *
 *****************************************************************************/
function EnterItem(request, response)
{
	if (request.getMethod() == 'GET') 
	{   
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7,st8,st9,st10;

		if( getLanguage == 'es_ES')
		{
			st1 = "TEMA:";
			st2 = "ENTER / ESCANEADO DE &#205;TEMS";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";
			st5 = "PEDIDO # ";
			st6 = "CAJA #: ";
			st8 = "ELEMENTO DESCRIPCI&#211;N:";
			st9 = "CANTIDAD A GRANEL";


		}
		else
		{
			st1 = "ITEM: ";
			st2 = "ENTER/SCAN ITEM ";
			st3 = "SEND";
			st4 = "PREV";
			st5 = "ORDER#:";
			st6 = "CARTON#: ";
			st8 = "ITEM DESCRIPTION: ";
			st9 = "BULK QTY:";

		}

		var getOrderno = request.getParameter('custparam_orderno');
		var getContlpno=request.getParameter('custparam_contlpno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocationId = request.getParameter('custparam_beginLocation');
		var getItem = request.getParameter('custparam_item');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');        
		var getEnteredLocation = request.getParameter('custparam_endlocation');
		var getBeginBinLocation = request.getParameter('custparam_beginlocationname');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var vclusterNo = request.getParameter('custparam_clusterno');		
		var name=request.getParameter('custparam_name');
		var vBatchno = request.getParameter('custparam_batchno');
		var vcontlp = request.getParameter('custparam_contlp');
		var vcontsize = request.getParameter('custparam_contsize');
		var vlineCount = request.getParameter('custparam_linecount');
		var vloopCount = request.getParameter('custparam_loopcount');
		var resultsCount =request.getParameter('custparam_count');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var wmslocation=request.getParameter('custparam_wmslocation');
		var MultipleItemScaned =request.getParameter('custpage_multipleitemscan');
		var	vClusterno="";
		//	var NextItemId=request.getParameter('custparam_nextiteminternalid');
		nlapiLogExecution('DEBUG', 'getItemInternalId', getItemInternalId);	
		nlapiLogExecution('DEBUG', 'NextItemInternalId', NextItemInternalId);	
		if(NextItemInternalId!==null && NextItemInternalId!=='' )
		{
			getItemInternalId=NextItemInternalId;
		}
		var Itype = nlapiLookupField('item', getItemInternalId, 'recordType');
		var ItemRec = nlapiLoadRecord(Itype, getItemInternalId);
		var Itemdescription='';
		nlapiLogExecution('DEBUG', 'getItem', getItem);	
		nlapiLogExecution('DEBUG', 'getItemDescription', getItemDescription);
		var getItemName = ItemRec.getFieldValue('itemid');

		if(ItemRec.getFieldValue('description') != null && ItemRec.getFieldValue('description') != "")
		{	
			Itemdescription = ItemRec.getFieldValue('description');
		}
		else if(ItemRec.getFieldValue('salesdescription') != null && ItemRec.getFieldValue('salesdescription') != "")
		{	
			Itemdescription = ItemRec.getFieldValue('salesdescription');
		}	

		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enteritem').focus();";        
		html = html + "</script>";
		html = html + "</head><body>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st5 + "<label>" + name + "</label>";	
		html = html + "				</td>";
		html = html + "			</tr>";  
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st6 + "<label>" + getContlpno + "</label>";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + "<label>" + getItemName + "</label>";
		html = html + "			</tr>";
		html = html + "			<tr>";        
		html = html + "				<td align = 'left'>" + st2 + "";		
		html = html + "				</td>";
		html = html + "			</tr>";

		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enteritem' id='enteritem' type='text'/>";
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getOrderno + ">";
		html = html + "				<input type='hidden' name='hdnItemName' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + getBeginBinLocation + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vClusterno + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value=" + vBatchno + ">";
		html = html + "				<input type='hidden' name='hdnlinecount' value=" + vlineCount + ">";
		html = html + "				<input type='hidden' name='hdnloopcount' value=" + vloopCount + ">";
		html = html + "				<input type='hidden' name='hdnnextitem' value=" + NextItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdncontlpno' value=" + getContlpno + ">";
		html = html + "				<input type='hidden' name='hdnresultscount' value=" + resultsCount + ">";
		html = html + "				<input type='hidden' name='hdnwmslocation' value=" + wmslocation + ">";
		html = html + "				<input type='hidden' name='hdnname' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdngetitemname' value='" + getItemName + "'>";
		html = html + "				<input type='hidden' name='hdnMultipleItemScaned' value=" + MultipleItemScaned + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		

		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st9 + " <input name='cmdBulkqty' type='submit' value='F8' />" + st4 + " <input name='cmdPrevious' type='submit' value='F7'/>";        
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enteritem').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var getLanguage =  request.getParameter('hdngetLanguage');

		var getEnteredItem = request.getParameter('enteritem');
		nlapiLogExecution('DEBUG', 'Entered Order No', getEnteredItem);      

		var getWaveNo = request.getParameter('hdnWaveNo');
		var getRecordInternalId = request.getParameter('hdnRecordInternalId');
		var getContainerLpNo = request.getParameter('hdnContainerLpNo');
		var getExpectedQuantity = request.getParameter('hdnExpectedQuantity');
		var getBeginLocation = request.getParameter('hdnBeginLocation');
		var getItem = request.getParameter('hdnItem');
		var getItemDescription = request.getParameter('hdnItemDescription');
		var getItemInternalId = request.getParameter('hdnItemInternalId');
		var getDOLineId = request.getParameter('hdnDOLineId');
		var getInvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var getOrderLineNo = request.getParameter('hdnOrderLineNo');
		var getItem = request.getParameter('hdnItemName');
		var vClusterNo = request.getParameter('hdnClusterNo');
		var vBatchNo = request.getParameter('hdnbatchno');		
		var getOrderNo = request.getParameter('hdnWaveNo');
		var getCortonNo = request.getParameter('hdncontlpno');
		var getresultscount= request.getParameter('hdnresultscount');
		var wmslocation= request.getParameter('hdnwmslocation');
		var name=request.getParameter('hdnname');
		var getItemName=request.getParameter('hdngetitemname');

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');   
		var checkqty = request.getParameter('cmdBulkqty');  
		//var checkqty = request.getParameter('bulkqty');   
		//nlapiLogExecution('DEBUG', 'checkqty', checkqty);

		var SOarray = new Array();
		var getLanguage =  request.getParameter('hdngetLanguage');

		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);


		var st7,st10;

		if( getLanguage == 'es_ES')
		{

			st7 = "ART&#205;CULO INV&#193;LIDO ";
			st10 = "NO TEMAS DE LINEA EMPACAR";


		}
		else
		{	
			st7 = "INVALID ITEM";
			st10 = "NO LINE ITEMS TO PACK";					

		}

		SOarray["custparam_language"] = getLanguage;

		SOarray["custparam_error"] = st7;//'INVALID ITEM';
		SOarray["custparam_screenno"] = '26';

		SOarray["custparam_orderno"] = getWaveNo;
		SOarray["custparam_recordinternalid"] = getRecordInternalId;
		SOarray["custparam_containerlpno"] = getContainerLpNo;
		SOarray["custparam_expectedquantity"] = getExpectedQuantity;
		SOarray["custparam_beginLocation"] = getBeginLocation;
		SOarray["custparam_item"] = getItem;
		SOarray["custparam_itemdescription"] = getItemDescription;
		SOarray["custparam_iteminternalid"] = getItemInternalId;
		SOarray["custparam_dolineid"] = getDOLineId;
		SOarray["custparam_invoicerefno"] = getInvoiceRefNo;
		SOarray["custparam_orderlineno"] = getOrderLineNo;
		SOarray["custparam_clusterno"] = vClusterNo;
		SOarray["custparam_batchno"] = vBatchNo;
		SOarray["custparam_linecount"] = request.getParameter('hdnlinecount');
		SOarray["custparam_loopcount"] = request.getParameter('hdnloopcount');
		SOarray["custparam_nextiteminternalid"] = request.getParameter('hdnnextitem');
		SOarray['custparam_count']=getresultscount;
		SOarray['custparam_wmslocation']=wmslocation;
		SOarray['custparam_name']=name;
		SOarray['custpage_multipleitemscan']=request.getParameter('hdnMultipleItemScaned');
		SOarray["custparam_contlpno"]=getCortonNo;
		var checkBulkqty='N';
		if(checkqty!=null && checkqty!='')
		{
			checkBulkqty='Y';
		}
		SOarray['custparam_bulkqty']=checkqty;


		nlapiLogExecution('DEBUG', 'Order LineNo is', getOrderLineNo);

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept SO #.
		if (optedEvent == 'F7') {
			//response.sendRedirect('SUITELET', 'customscript_rf_picking_container_no', 'customdeploy_rf_picking_container_no_di', false, SOarray);
			response.sendRedirect('SUITELET', 'customscript_rf_packing_orderno', 'customdeploy_rf_packing_orderno_di', false, SOarray);
		}
		else if(checkqty=='F8')
		{

			nlapiLogExecution('DEBUG', 'getOrderNo', getOrderNo);
			nlapiLogExecution('DEBUG', 'getCortonNo', getCortonNo);
			nlapiLogExecution('DEBUG', 'Into If', 'hi1');
			nlapiLogExecution('DEBUG', 'getEnteredItem', getEnteredItem);
			nlapiLogExecution('DEBUG', 'wmslocation', wmslocation);

			if(getEnteredItem!=null && getEnteredItem!='')
				var currItem = validateSKU(getEnteredItem, wmslocation, null) ;
			else
				SOarray["custparam_error"] = "ENTER ITEM";//'No Line Items To Pack';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);

			nlapiLogExecution('DEBUG', 'currItem ', currItem );
			//case# 201410412
			if(currItem!=null && currItem!='')
			{
				var SOFilters = new Array();
				if(getOrderNo!=null && getOrderNo!='')
					SOFilters.push(new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', getOrderNo));

				if(getCortonNo!=null && getCortonNo!='')
					SOFilters.push(new nlobjSearchFilter( 'custrecord_container_lp_no',null, 'is', getCortonNo));

				SOFilters.push(new nlobjSearchFilter( 'custrecord_pack_confirmed_date',null, 'isempty', null));

				SOFilters.push(new nlobjSearchFilter('custrecord_tasktype',null, 'anyof', [3]));	
				SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag',null, 'anyof', [8]));	

				var SOColumns = new Array();
				SOColumns[0] = new nlobjSearchColumn('custrecord_lpno');
				SOColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
				SOColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
				SOColumns[3] = new nlobjSearchColumn('custrecord_sku');
				SOColumns[4] = new nlobjSearchColumn('custrecord_skudesc');
				SOColumns[5] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
				SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
				SOColumns[7] = new nlobjSearchColumn('custrecord_invref_no');
				SOColumns[8] = new nlobjSearchColumn('custrecord_line_no');
				SOColumns[9] = new nlobjSearchColumn('custrecord_actbeginloc');
				SOColumns[10] = new nlobjSearchColumn('custrecord_batch_no');	
				SOColumns[11] = new nlobjSearchColumn('custrecord_ebiz_order_no');
				SOColumns[12] = new nlobjSearchColumn('custrecord_container_lp_no');
				SOColumns[13] = new nlobjSearchColumn('custrecord_wms_location');
				SOColumns[14] = new nlobjSearchColumn('custrecord_act_qty');

				var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);  
				if(SOSearchResults != null && SOSearchResults.length > 0) 
				{
					if(currItem!=null&&currItem!="")
					{
						var flag='F';
						var SOSearchResult;
						nlapiLogExecution('DEBUG', 'Test3', 'Test3');
						nlapiLogExecution('DEBUG', 'SearchResults of given Order', SOSearchResults.length);
						nlapiLogExecution('DEBUG', 'SearchResults of given Order', getOrderNo);
						nlapiLogExecution('DEBUG', 'currItem', currItem);
						for ( var count = 0; count < SOSearchResults.length; count++) {
							nlapiLogExecution('DEBUG', 'SearchResults of given Order', SOSearchResults[count].getText('custrecord_sku'));
							if(SOSearchResults[count].getText('custrecord_sku')==currItem)
							{
								flag='T';
								SOSearchResult = SOSearchResults[count];
								break;
							}
						}


						var actualQty=SOSearchResult.getValue('custrecord_act_qty');
						if(actualQty==null || actualQty=='' ||actualQty=='null')
						{
							actualQty=0;
						}
						var expectedQty=SOSearchResult.getValue('custrecord_expe_qty');
						if(expectedQty==null || expectedQty=='' ||expectedQty=='null')
						{
							expectedQty=0;
						}
						nlapiLogExecution('DEBUG', 'actualQty', actualQty);
						nlapiLogExecution('DEBUG', 'expectedQty', expectedQty);
						if(parseFloat(actualQty)==parseFloat(expectedQty))
						{
							nlapiLogExecution('DEBUG', 'inside', 'if');
							SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
							SOarray["custparam_actualquantity"] = SOSearchResult.getValue('custrecord_act_qty');


						}
						else
						{
							nlapiLogExecution('DEBUG', 'inside', 'else');	
							SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_act_qty');
							SOarray["custparam_actualquantity"] = SOSearchResult.getValue('custrecord_act_qty');
						}




						if(flag=='T')
						{
//							var SOSearchResult = SOSearchResults[0];
							if(SOSearchResults.length > 1)
							{
								var SOSearchnextResult = SOSearchResults[1];
								SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
								SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
								nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
								nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
							}
							// SOarray["custparam_waveno"] = getWaveNo;
							nlapiLogExecution('DEBUG', 'SearchResults of given Order', SOSearchResult.getValue('custrecord_lpno'));
							nlapiLogExecution('DEBUG', 'SearchResults of given Order', SOSearchResult.getValue('custrecord_expe_qty'));
							nlapiLogExecution('DEBUG', 'SearchResults of given Order', getOrderNo);

							//SOarray["custparam_linecount"] = SOSearchResults.length;
							SOarray["custparam_orderno"] = getOrderNo;
							SOarray["custparam_contlpno"] = getCortonNo;
							SOarray["custparam_recordinternalid"] = SOSearchResult.getId();			       
							//	SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');			       
							SOarray["custparam_item"] = SOSearchResult.getText('custrecord_sku');
							SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');	
							SOarray['custparam_loopcount'] = SOSearchResults.length;
						}
					}
				}
				else 
				{
					SOarray["custparam_error"] = st10;//'No Line Items To Pack';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					nlapiLogExecution('DEBUG', 'Error: ', 'No Line Items To Pack');
				}
				//if (getEnteredLocation != '' && getEnteredLocation == getBeginBinLocation) {
				nlapiLogExecution('DEBUG', 'getEnteredItem', getEnteredItem);
				nlapiLogExecution('DEBUG', 'getItemName', getItemName);

//				var currItem = validateSKU(getEnteredItem, wmslocation, null);
				if(getEnteredItem != '' && getEnteredItem == getItemName)
				{
					if (currItem!=null && currItem!='') 
					{		
						response.sendRedirect('SUITELET', 'customscript_rf_packing_qty', 'customdeploy_rf_packing_qty_di', false, SOarray);
						nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');

					}
					else 
					{
//						SOarray["custparam_error"] = 'Invalid Item';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						nlapiLogExecution('DEBUG', 'Error: ', 'Invalid Item');
					}
				}
				else if (currItem!=null && currItem!='' && SOSearchResults!=null && SOSearchResults!='') 
				{		
					for ( var count = 0; count < SOSearchResults.length; count++) 
					{
						if(SOSearchResults[count].getText('custrecord_sku')==currItem)
						{
							SOSearchResult=SOSearchResults[count];
							SOarray["custparam_contlpno"] = getCortonNo;
							SOarray["custparam_recordinternalid"] = SOSearchResult.getId();			       
							SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');			       
							SOarray["custparam_item"] = SOSearchResult.getText('custrecord_sku');
							SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');	
							SOarray['custparam_loopcount'] = SOSearchResults.length;
							SOarray["custparam_iteminternalid"]=SOSearchResult.getValue('custrecord_sku');
							break;
						}
//						SOarray["custparam_item"] = SOSearchResults[count].getText('custrecord_sku');
					}
					response.sendRedirect('SUITELET', 'customscript_rf_packing_qty', 'customdeploy_rf_packing_qty_di', false, SOarray);
					nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');

				}
				else
				{
//					SOarray["custparam_error"] = 'Invalid Item';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;
					nlapiLogExecution('DEBUG', 'Error: ', 'Invalid Item');
				}
			}
			else
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				nlapiLogExecution('DEBUG', 'Error: ', 'Invalid Item');
			}

		}
		else 
		{
			nlapiLogExecution('DEBUG', 'getOrderNo', getOrderNo);
			nlapiLogExecution('DEBUG', 'getCortonNo', getCortonNo);
			nlapiLogExecution('DEBUG', 'Into If', 'hi1');
			var currItem = '';
			if(getEnteredItem != null && getEnteredItem != 'null' && getEnteredItem!='')
				//	if ((getEnteredItem != ''&& getEnteredItem!=null)&& getEnteredItem != getItemName)
			{
				//currItem = validateSKU(getEnteredItem, wmslocation, null) ;

				var actItemidArray=validateSKUId(getEnteredItem,wmslocation,'');
				nlapiLogExecution('ERROR', 'After validateSKU1',actItemidArray);
				var itemRecordArr = eBiz_RF_GetItemForItemIdWithArrNew(getEnteredItem);

				var ordertype = 'salesorder';
				var ebizOrdNo=GetSOInternalId(getOrderNo,ordertype);

				var trantype = nlapiLookupField('transaction', ebizOrdNo, 'recordType');

				nlapiLogExecution('DEBUG', 'trantype',trantype);

				var rec='';
				if(trantype=='transferorder')
					rec= nlapiLoadRecord('transferorder', ebizOrdNo);
				else
					rec= nlapiLoadRecord('salesorder', ebizOrdNo);
				// Case# 20148649 ends
				var OrdName=rec.getFieldValue('tranid');

				nlapiLogExecution('DEBUG', 'itemRecordArr',itemRecordArr);
				var soLineDetails ='';
				//	var actItemid='';
				if(itemRecordArr !=null && itemRecordArr !='')
				{
					soLineDetails = eBiz_RF_GetSOLineDetailsForItemArr(OrdName, itemRecordArr,trantype);


					//case 20140720 start
					if(soLineDetails != null && soLineDetails != "")
					{
						currItem=soLineDetails[0].getValue('item');
					}
					//case 20140720 end
				}
			}
			else
			{
				SOarray["custparam_error"] = 'Invalid Item';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				nlapiLogExecution('DEBUG', 'Error: ', 'Invalid Item');
			}
			nlapiLogExecution('DEBUG', 'currItem ', currItem );
			//case# 201410412
			if(currItem!=null && currItem!='')
			{	
				var SOFilters = new Array();
				if(getOrderNo!=null && getOrderNo!='')
					SOFilters.push(new nlobjSearchFilter('tranid', 'custrecord_ebiz_order_no', 'is', getOrderNo));

				if(getCortonNo!=null && getCortonNo!='')
					SOFilters.push(new nlobjSearchFilter( 'custrecord_container_lp_no',null, 'is', getCortonNo));

				//SOFilters.push(new nlobjSearchFilter( 'custrecord_pack_confirmed_date',null, 'isempty', null));

				SOFilters.push(new nlobjSearchFilter('custrecord_tasktype',null, 'anyof', [3]));	
				SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag',null, 'anyof', [8]));	
				var SOColumns = new Array();
				SOColumns[0] = new nlobjSearchColumn('custrecord_lpno');
				SOColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
				SOColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
				SOColumns[3] = new nlobjSearchColumn('custrecord_sku');
				SOColumns[4] = new nlobjSearchColumn('custrecord_skudesc');
				SOColumns[5] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
				SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
				SOColumns[7] = new nlobjSearchColumn('custrecord_invref_no');
				SOColumns[8] = new nlobjSearchColumn('custrecord_line_no');
				SOColumns[9] = new nlobjSearchColumn('custrecord_actbeginloc');
				SOColumns[10] = new nlobjSearchColumn('custrecord_batch_no');	
				SOColumns[11] = new nlobjSearchColumn('custrecord_ebiz_order_no');
				SOColumns[12] = new nlobjSearchColumn('custrecord_container_lp_no');
				SOColumns[13] = new nlobjSearchColumn('custrecord_wms_location');
				SOColumns[14] = new nlobjSearchColumn('custrecord_act_qty');


				var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);  
				if (SOSearchResults != null && SOSearchResults.length > 0) 
				{

					if(currItem!=null&&currItem!="")
					{
						var flag='F';
						var SOSearchResult;
						nlapiLogExecution('DEBUG', 'Test3', 'Test3');
						nlapiLogExecution('DEBUG', 'SearchResults of given Order', SOSearchResults.length);
						nlapiLogExecution('DEBUG', 'SearchResults of given Order', getOrderNo);
						for ( var count = 0; count < SOSearchResults.length; count++) {
							//if(SOSearchResults[count].getText('custrecord_sku')==currItem)
							if(SOSearchResults[count].getValue('custrecord_sku')==currItem)
							{
								flag='T';
								SOSearchResult = SOSearchResults[count];
								break;
							}
						}

						var actualQty=SOSearchResult.getValue('custrecord_act_qty');
						if(actualQty==null || actualQty=='' ||actualQty=='null')
						{
							actualQty=0;
						}
						var expectedQty=SOSearchResult.getValue('custrecord_expe_qty');
						if(expectedQty==null || expectedQty=='' ||expectedQty=='null')
						{
							expectedQty=0;
						}
						nlapiLogExecution('DEBUG', 'actualQty', actualQty);
						nlapiLogExecution('DEBUG', 'expectedQty', expectedQty);
						if(parseFloat(actualQty)==parseFloat(expectedQty))
						{
							nlapiLogExecution('DEBUG', 'inside', 'if');
							SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
							SOarray["custparam_actualquantity"] = SOSearchResult.getValue('custrecord_act_qty');


						}
						else
						{
							nlapiLogExecution('DEBUG', 'inside', 'else');	
							SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_act_qty');
							SOarray["custparam_actualquantity"] = SOSearchResult.getValue('custrecord_act_qty');
						}



						if(flag=='T')
						{
							if(SOSearchResults.length > 1)
							{
								var SOSearchnextResult = SOSearchResults[1];
								SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
								SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
								nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
								nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
							}
							// SOarray["custparam_waveno"] = getWaveNo;
							nlapiLogExecution('DEBUG', 'SearchResults of given Order', SOSearchResult.getValue('custrecord_lpno'));
							nlapiLogExecution('DEBUG', 'SearchResults of given Order', SOSearchResult.getValue('custrecord_expe_qty'));
							nlapiLogExecution('DEBUG', 'SearchResults of given Order', getOrderNo);

							//SOarray["custparam_linecount"] = SOSearchResults.length;

							SOarray["custparam_orderno"] = getOrderNo;
							SOarray["custparam_contlpno"] = getCortonNo;
							SOarray["custparam_recordinternalid"] = SOSearchResult.getId();			       
							//SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');			       
							SOarray["custparam_item"] = SOSearchResult.getText('custrecord_sku');
							SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');	
							SOarray['custparam_loopcount'] = SOSearchResults.length;
						}
					}
				}
				else 
				{
					SOarray["custparam_error"] = st10;//'No Line Items To Pack';				
					nlapiLogExecution('DEBUG', 'Error: ', 'No Line Items To Pack');
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;
				}
				//if (getEnteredLocation != '' && getEnteredLocation == getBeginBinLocation) {
				nlapiLogExecution('DEBUG', 'getEnteredItem', getEnteredItem);
				nlapiLogExecution('DEBUG', 'getItemName', getItemName);

				//var currItem = validateSKU(getEnteredItem, wmslocation, null);
				if(getEnteredItem != '' && getEnteredItem == getItemName)
				{
					if (currItem!=null && currItem!='') 
					{		
						response.sendRedirect('SUITELET', 'customscript_rf_packing_qty', 'customdeploy_rf_packing_qty_di', false, SOarray);
						nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');

					}
					else 
					{
						SOarray["custparam_error"] = 'Invalid Item';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						nlapiLogExecution('DEBUG', 'Error: ', 'Invalid Item');
					}
				}
				else if (currItem!=null && currItem!='') 
				{		
					for ( var count = 0; count < SOSearchResults.length; count++) 
					{
						if(SOSearchResults[count].getText('custrecord_sku')==currItem)
						{
							SOSearchResult=SOSearchResults[count];
							SOarray["custparam_contlpno"] = getCortonNo;
							SOarray["custparam_recordinternalid"] = SOSearchResult.getId();			       
							SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');			       
							SOarray["custparam_item"] = SOSearchResult.getText('custrecord_sku');
							SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');	
							SOarray['custparam_loopcount'] = SOSearchResults.length;
							SOarray["custparam_iteminternalid"]=SOSearchResult.getValue('custrecord_sku');
							break;
						}
//						SOarray["custparam_item"] = SOSearchResults[count].getText('custrecord_sku');
					}
					response.sendRedirect('SUITELET', 'customscript_rf_packing_qty', 'customdeploy_rf_packing_qty_di', false, SOarray);
					nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');

				}
				else
				{
//					SOarray["custparam_error"] = 'Invalid Item';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					nlapiLogExecution('DEBUG', 'Error: ', 'Invalid Item');
				}
			}
			else
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				nlapiLogExecution('DEBUG', 'Error: ', 'Invalid Item');
			}
		}
	}
}

/**
 * 
 * @param itemNo
 * @param location
 * @param company
 * @returns
 */
function validateSKU(itemNo, location, company){
	nlapiLogExecution('DEBUG', 'validateSKU', 'Start');

	var inputParams = 'Item = ' + itemNo + '<br>';
	inputParams = inputParams + 'Location = ' + location + '<br>';
	inputParams = inputParams + 'Company = ' + company;
	nlapiLogExecution('DEBUG', 'Input Parameters', inputParams);

	var currItem = eBiz_RF_GetItemForItemNo(itemNo);

	if(currItem == ""){
		currItem = eBiz_RF_GetItemBasedOnUPCCode(itemNo);
	}

	if(currItem == ""){
		currItem = eBiz_RF_GetItemFromSKUDims(itemNo, location);
	}

	var logMsg = "";
	if(currItem == ""){
		logMsg = 'Unable to retrieve item';
		currItem = null;
	} else {
		logMsg = 'Item = ' + currItem;
	}

	nlapiLogExecution('DEBUG', 'Item Retrieved', logMsg);
	nlapiLogExecution('DEBUG', 'validateSKU', 'End');
	return currItem;
}

function GetSOInternalId(SOText,ordertype)
{

	ordertype = 'salesorder';
	nlapiLogExecution('ERROR','Into GetSOInternalId (Input)',SOText);
	nlapiLogExecution('ERROR','Into GetSOInternalId (ordertype)',ordertype);
	var ActualSoID='-1';

	var filter=new Array();
	filter.push(new nlobjSearchFilter('tranid',null,'is',SOText.toString()));
	filter.push(new nlobjSearchFilter('mainline',null,'is','F'));

	var columns=new Array();
	columns.push(new nlobjSearchColumn('shipmethod'));

	var searchrec=nlapiSearchRecord(ordertype,null,filter,columns);
	if(searchrec==null)
	{
		searchrec=nlapiSearchRecord('transferorder',null,filter,columns);
	}
	if(searchrec!=null && searchrec!='' && searchrec.length>0)
	{
		ActualSoID=searchrec[0].getId();
	}

	nlapiLogExecution('ERROR','Out of GetSOInternalId (Output)',ActualSoID);

	return ActualSoID;
}
function eBiz_RF_GetSOLineDetailsForItemArr(poID, itemIDArr,trantype){
	nlapiLogExecution('ERROR', 'eBiz_RF_GetPOLineDetailsForItemArr', 'Start');
	var logMsg = 'PO ID = ' + poID + '<br>';
	logMsg = logMsg + 'Item ID = ' + itemIDArr;
	logMsg = logMsg + 'trantype = ' + trantype;
	nlapiLogExecution('ERROR', 'Input Parameters', logMsg);


	//alert('Order Id ' + poID);
	//alert('itemIDArr ' + itemIDArr);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('tranid', null, 'is', poID));
	filters.push(new nlobjSearchFilter('item', null, 'anyof', itemIDArr));
	if(trantype=='salesorder')
		filters.push(new nlobjSearchFilter('custrecord_ebizwhsite', 'location', 'is', 'T'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('item');
	columns[1] = new nlobjSearchColumn('line');
	columns[2] = new nlobjSearchColumn('custcol_ebiz_po_cube');
	columns[3] = new nlobjSearchColumn('location');
	columns[4] = new nlobjSearchColumn('quantity');
	columns[5] = new nlobjSearchColumn('quantityshiprecv');

	var SOLineSearchResults = nlapiSearchRecord(trantype, null, filters, columns);

	if(SOLineSearchResults != null && SOLineSearchResults.length > 0)
		nlapiLogExecution('ERROR', 'No. of PO Lines Retrieved', SOLineSearchResults.length);

	nlapiLogExecution('ERROR', 'eBiz_RF_GetPOLineDetailsForItemArr', 'End');

	return SOLineSearchResults;
}
