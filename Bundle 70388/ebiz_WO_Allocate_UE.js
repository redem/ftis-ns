/***************************************************************************
  	eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/UserEvents/ebiz_WO_Allocate_UE.js,v $
 *     	   $Revision: 1.7.2.2.4.1.4.7.2.1 $
 *     	   $Date: 2015/09/21 14:02:08 $
 *     	   $Author: deepshikha $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_8 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_WO_Allocate_UE.js,v $
 * Revision 1.7.2.2.4.1.4.7.2.1  2015/09/21 14:02:08  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.7.2.2.4.1.4.7  2014/08/20 14:47:59  grao
 * Case#: 20149990 �Standard bundle  issue fixes
 *
 * Revision 1.7.2.2.4.1.4.6  2014/07/15 08:06:47  nneelam
 * case#  20149463
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.7.2.2.4.1.4.5  2014/07/07 15:43:05  skreddy
 * case # 20149320
 * PCT Prod issue fix
 *
 * Revision 1.7.2.2.4.1.4.4  2014/05/01 15:36:29  skavuri
 * Case# 20148202 issue fixed
 *
 * Revision 1.7.2.2.4.1.4.3  2013/03/08 14:44:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.7.2.2.4.1.4.2  2013/03/01 14:35:04  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.7.2.2.4.1.4.1  2013/02/27 13:22:58  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from Monobind
 *
 * Revision 1.7.2.2.4.1  2012/11/01 14:54:57  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.7.2.2  2012/02/10 09:31:54  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge
 *
 * Revision 1.9  2012/02/09 14:43:42  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Physical and Virtual Location changes
 *
 * Revision 1.8  2012/01/31 12:42:46  gkalla
 * CASE201112/CR201113/LOG201121
 * Work order Bin location issues fixed
 *
 * Revision 1.6  2011/12/30 20:36:31  gkalla
 * CASE201112/CR201113/LOG201121
 * For WO allocation
 *
 * Revision 1.5  2011/12/30 16:57:29  gkalla
 * CASE201112/CR201113/LOG201121
 * For WO allocation
 *
 * Revision 1.4  2011/12/28 07:53:04  gkalla
 * CASE201112/CR201113/LOG201121
 * For PO Overage
 *
 * Revision 1.3  2011/12/14 23:35:10  gkalla
 * CASE201112/CR201113/LOG201121
 * Changed Deployment ids
 *
 * Revision 1.2  2011/11/22 15:42:20  gkalla
 * CASE201112/CR201113/LOG201121
 * Inventory allocation in WO
 *
 * Revision 1.1  2011/09/23 16:22:25  gkalla
 * CASE201112/CR201113/LOG201121
 *
 *****************************************************************************/
/**
 * @author LN
 *@version
 *@date
 *@Description: This is an User Event Script to allocate inventory in create inventory against to Work Order in Work order screen.
 level LP validation
 */

/*var eventtype; 
function  myPageInit(type){

	eventtype=type;
	alert("type" + eventtype);
}*/ 
//function AllocateQuantity(type, form, request)
function AllocateQuantity(type)
{


	nlapiLogExecution('ERROR', 'type', type);
	//alert("eventtype" + type);
	//if(type=='edit' || type=='create')
	if(type=='create')
	{
		//AllocateInv();

	}
	/*else if(type=='delete')
	{

		DeallocateInv();
	}*/
	/*else if(type=='edit')
	{

		//DeallocateInv();
		AllocateInv();
	}*/
}

function AllocateInv()
{
	nlapiLogExecution('ERROR', 'Into Allocation');
	var itemcount=nlapiGetLineItemCount('item');
	var assemItem= nlapiGetFieldValue('assemblyitem');
	var vWOId= nlapiGetFieldValue('tranid');
	var vCarrier= nlapiGetFieldValue('shipmethod');
	var vId= nlapiGetFieldValue('id');
	var qty1=nlapiGetFieldValue('quantity');
	var vLocation= nlapiGetFieldValue('location');
	var qty=0;
	if(qty1!= null && qty1!="" && parseFloat(qty1)>0)
		qty=qty1;

	nlapiLogExecution('ERROR', 'vId', vId);
	nlapiLogExecution('ERROR', 'vWOId', vWOId);
	var searchresults = nlapiLoadRecord('workorder', vId);
	if(searchresults !=null && searchresults != "" && searchresults.length>0)
		vWOId=searchresults.getFieldValue('tranid');
	nlapiLogExecution('ERROR', 'vWOId', vWOId);

	nlapiLogExecution('ERROR', 'ItemCount', itemcount);
	nlapiLogExecution('ERROR', 'assemItem', assemItem);

	if(itemcount>0)
	{ 
		// Create a Record in Opentask
		var now = new Date();
		//a Date object to be used for a random value
		var now = new Date();
		//now= now.getHours();
		//Getting time in hh:mm tt format.
		var a_p = "";
		var d = new Date();
		var curr_hour = now.getHours();
		if (curr_hour < 12) {
			a_p = "am";
		}
		else {
			a_p = "pm";
		}
		if (curr_hour == 0) {
			curr_hour = 12;
		}
		if (curr_hour > 12) {
			curr_hour = curr_hour - 12;
		}

		var curr_min = now.getMinutes();

		curr_min = curr_min + "";

		if (curr_min.length == 1) {
			curr_min = "0" + curr_min;
		}
		//var eBizWaveNo = GetMaxTransactionNo('WAVE');
		for(var s=1;s<=itemcount;s++)
		{
			//alert("SkuNo" + SkuNo);
			strItem = nlapiGetLineItemValue('item', 'item', s);
			//alert("strItem" + strItem);
			strQty = nlapiGetLineItemValue('item', 'quantity', s);
			//alert("strQty" + strQty);
			//alert("varAssemblyQty" + varAssemblyQty);
			var fields = ['custitem_item_family', 'custitem_item_group'];
			var ComponentItemType = nlapiLookupField('item', strItem, 'recordType');
			if (ComponentItemType != null && ComponentItemType !='')
			{
				var columns = nlapiLookupField(ComponentItemType, strItem, fields);


				var itemfamily = columns.custitem_item_family;
				var itemgroup = columns.custitem_item_group;
			} 
			var vitemqty=0;
			var vrecid="" ;
			var vlp="",vlocation="",vqty="",vlotnoWithQty="",vremainqty="",location;

			/*var vInvValue=vArr[s-1];
				var varsku = nlapiGetLineItemValue('item', 'item', s);
				var varskuText = nlapiGetLineItemText('item', 'item', s);
				var VRec = vInvValue[2];
				var varQty = vInvValue[5];
				var LineLocation=vInvValue[1];	
				var vBatch =vInvValue[3];	*/


			var varsku = nlapiGetLineItemValue('item', 'item', s);
			var varskuText = nlapiGetLineItemText('item', 'item', s);
			var VRec = nlapiGetLineItemValue('item', 'custcol_ebizwoinventoryref', s);
			var varQty = nlapiGetLineItemValue('item', 'custcol_ebizwoavalqty', s);
			//var LineLocation=nlapiGetLineItemValue('item','custcol_ebizwobinloc',s);
			var LineLocation=nlapiGetLineItemValue('item','custcol_locationsitemreceipt',s);	
			var vBatch =nlapiGetLineItemValue('item','serialnumbers',s);	
			nlapiLogExecution('ERROR', 'varsku', varsku);
			nlapiLogExecution('ERROR', 'VRec', VRec);

			var vRecArr=new Array();
			if(VRec!= null && VRec != "" && VRec.indexOf(',') != -1)
				vRecArr=VRec.split(',');
			else
				vRecArr.push(VRec);
			var vQtyArr=new Array();
			if(varQty!= null && varQty != "" && varQty.indexOf(',') != -1)
				vQtyArr=varQty.split(',');
			else
				vQtyArr.push(varQty);
			var vRecArrBin=new Array();
			if(LineLocation!= null && LineLocation != ""  && LineLocation.indexOf(',') != -1)
				vRecArrBin=LineLocation.split(',');
			else
				vRecArrBin.push(LineLocation);
			for(var k=0;k<vRecArr.length;k++)
			{



				var vLineQty=vQtyArr[k];
				var vLineRec=vRecArr[k];
				var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', vLineRec);
				var qty = transaction.getFieldValue('custrecord_ebiz_qoh');
				var allocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
				var vLP=transaction.getFieldValue('custrecord_ebiz_inv_lp');
				var vPackcode=transaction.getFieldValue('custrecord_ebiz_inv_packcode');
				var vSKUStatus=transaction.getFieldValue('custrecord_ebiz_inv_sku_status');

				nlapiLogExecution('ERROR', 'qty', qty);
				nlapiLogExecution('ERROR', 'allocqty', allocqty);
				nlapiLogExecution('ERROR', 'varqty', vLineQty);
				transaction.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(allocqty)+ parseFloat(vLineQty)).toFixed(5));
				nlapiSubmitRecord(transaction, false, true);

				var rcptrecordid3 = nlapiLoadRecord('workorder', vId);
				var woidWO= rcptrecordid3.getFieldValue('tranid');
				nlapiLogExecution('ERROR','woidWO  ',woidWO);
				vWOId=woidWO;



				var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
				customrecord.setFieldValue('custrecord_tasktype', 3); // 5 for KTS,3 for PICK, 4 for SHIP 
				// code added from Monobind account on 27Feb13 by santosh
				// to fetch date from Datestamp
				//customrecord.setFieldValue('custrecordact_begin_date', (parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
				customrecord.setFieldValue('custrecordact_begin_date', DateStamp());
				//upto here
				customrecord.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));

				//customrecord.setFieldValue('custrecord_act_end_date', (parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
				//customrecord.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
				//customrecord.setFieldValue('custrecord_expe_qty', varqty); 
				nlapiLogExecution('ERROR','expe qty ',vLineQty);
				nlapiLogExecution('ERROR','custrecord_actbeginloc ',vRecArrBin[k]);
				customrecord.setFieldValue('custrecord_expe_qty', parseFloat(vLineQty).toFixed(5));  //Commented and added to resovle decimal issue
				customrecord.setFieldValue('custrecord_act_qty', parseFloat(vLineQty).toFixed(5));
				customrecord.setFieldValue('custrecord_wms_status_flag', 9);	// 14 stands for outbound process


				customrecord.setFieldValue('custrecord_actbeginloc', vRecArrBin[k]);	
				//customrecord.setFieldValue('custrecord_actendloc', vbinloc);						
				//customrecord.setFieldValue('custrecord_lpno', vlp); 
				customrecord.setFieldValue('custrecord_upd_date', now);
				//customrecord.setFieldValue('custrecord_sku', vskuText);
				customrecord.setFieldValue('custrecord_ebiz_sku_no', varsku);
				customrecord.setFieldValue('name', vWOId);		
				var currentContext = nlapiGetContext();  
				var currentUserID = currentContext.getUser();
				customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
				customrecord.setFieldValue('custrecord_ebizuser', currentUserID);
				//customrecord.setFieldValue('custrecord_ebiz_wave_no', eBizWaveNo);
				customrecord.setFieldValue('custrecord_ebiz_order_no', vId);
				customrecord.setFieldValue('custrecord_batch_no', vBatch);
				customrecord.setFieldValue('custrecord_ebiz_cntrl_no', vId);
				//customrecord.setFieldValue('custrecord_ebiz_cntrl_no', rcptrecordid);

				customrecord.setFieldValue('custrecord_sku', varsku);
				customrecord.setFieldValue('custrecord_line_no', parseFloat(s));
				customrecord.setFieldValue('custrecord_lpno', vLP);
				customrecord.setFieldValue('custrecord_packcode', vPackcode);					
				customrecord.setFieldValue('custrecord_sku_status', vSKUStatus);
				customrecord.setFieldValue('custrecord_wms_location', vLocation);


				//nlapiLogExecution('ERROR','eBizWaveNo ',eBizWaveNo);


				nlapiSubmitRecord(customrecord);			

				nlapiLogExecution('ERROR', 'Success');





				/*var rcptrecordid2 = nlapiLoadRecord('customrecord_ebiznet_ordline', rcptrecordid);
				var woidFulfill= rcptrecordid2.getFieldText('custrecord_ns_ord');
				nlapiLogExecution('ERROR','woidFulfill  ',woidFulfill);*/




			}
		}

		return false;
	}

}

function CheckAvailability()
{
	nlapiLogExecution('ERROR', 'Into Check Availablity ');
	var itemcount=nlapiGetLineItemCount('item');
	var assemItem= nlapiGetFieldValue('assemblyitem');
	var vWOId= nlapiGetFieldValue('tranid');
	var vCarrier= nlapiGetFieldValue('shipmethod');
	var vId= nlapiGetFieldValue('id');
	var qty1=nlapiGetFieldValue('quantity');
	var vLocation= nlapiGetFieldValue('location');
	var qty=0;
	if(qty1!= null && qty1!="" && parseFloat(qty1)>0)
		qty=qty1;

	nlapiLogExecution('ERROR', 'vId', vId);
	nlapiLogExecution('ERROR', 'vWOId', vWOId);
	/*var searchresults = nlapiLoadRecord('workorder', vId);
	if(searchresults !=null && searchresults != "" && searchresults.length>0)
		vWOId=searchresults.getFieldValue('tranid');
	nlapiLogExecution('ERROR', 'vWOId', vWOId);*/

	nlapiLogExecution('ERROR', 'ItemCount', itemcount);
	nlapiLogExecution('ERROR', 'assemItem', assemItem);

	if(itemcount>0)
	{
		var vMessage= fnGetDetails(assemItem,qty,itemcount);

		if(vMessage== 'SUCCESS') 
		{ 
			return true;
		} 
		else
		{
			if(vMessage!= null && vMessage != "")
			{
				var vArrMsg=vMessage.split('~');
				if(vArrMsg.length>1)
				{
					nlapiLogExecution('ERROR','Allocation Failed ',vArrMsg[1]);
					var ErrorMsg = nlapiCreateError('CannotAllocate',vArrMsg[1], true);
					throw ErrorMsg; //throw this error object, do not catch it
				}
				else
				{
					nlapiLogExecution('ERROR','eBiz Allocation Failed ');
					var ErrorMsg = nlapiCreateError('CannotAllocate','Allocation Failed', true);
					throw ErrorMsg; //throw this error object, do not catch it
				}	
			}
			return false;
		}

	}
}

function DeallocateInv()
{
	nlapiLogExecution('ERROR', 'Deallocation ');
	var itemcount=nlapiGetLineItemCount('item');
	var assemItem= nlapiGetFieldValue('assemblyitem');
	var vWOId= nlapiGetFieldValue('tranid');
	var vCarrier= nlapiGetFieldValue('shipmethod');
	var vId= nlapiGetFieldValue('id');
	var vstatus= nlapiGetFieldValue('status');
	nlapiLogExecution('ERROR', 'vstatus', vstatus);

	
	var vserchresluts  = getRecordDetails(vId);
	if(vserchresluts != null && vserchresluts!='' && vserchresluts.length>0)
	{
		var ErrorMsg = nlapiCreateError('CannotAllocate','You are not allowed to Delete the record after genereated bin location', true);
		throw ErrorMsg;
	}	
	
	if(vstatus == 'Built')
	{
		var ErrorMsg = nlapiCreateError('CannotAllocate','You are not allowed to Delete the record after genereated bin location', true);
		throw ErrorMsg;
	}
	else
	{
		var vLocation= nlapiGetFieldValue('location');

		nlapiLogExecution('ERROR', 'vId', vId);
		nlapiLogExecution('ERROR', 'vWOId', vWOId);


		nlapiLogExecution('ERROR', 'ItemCount', itemcount);
		nlapiLogExecution('ERROR', 'assemItem', assemItem);

		if(itemcount>0)
		{

			//var eBizWaveNo = GetMaxTransactionNo('WAVE');
			for(var s=1;s<=itemcount;s++)
			{

				var varsku = nlapiGetLineItemValue('item', 'item', s);
				var varskuText = nlapiGetLineItemText('item', 'item', s);
				var VRec = nlapiGetLineItemValue('item', 'custcol_ebizwoinventoryref', s);
				var varQty = nlapiGetLineItemValue('item', 'custcol_ebizwoavalqty', s);
				//var LineLocation=nlapiGetLineItemValue('item','custcol_ebizwobinloc',s);
				var LineLocation=nlapiGetLineItemValue('item','custcol_locationsitemreceipt',s);	
				var vBatch =nlapiGetLineItemValue('item','serialnumbers',s);	
				nlapiLogExecution('ERROR', 'varsku', varsku);
				nlapiLogExecution('ERROR', 'VRec', VRec);

				var vRecArr=new Array();
				if(VRec!= null && VRec != "")
					vRecArr=VRec.split(',');
				var vQtyArr=new Array();
				if(varQty!= null && varQty != "")
					vQtyArr=varQty.split(',');
				var vRecArrBin=new Array();
				if(LineLocation!= null && LineLocation != "")
					vRecArrBin=LineLocation.split(',');
				for(var k=0;k<vRecArr.length;k++)
				{



					var vLineQty=vQtyArr[k];
					var vLineRec=vRecArr[k];
					var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', vLineRec);
					var qty = transaction.getFieldValue('custrecord_ebiz_qoh');
					var allocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
					var vLP=transaction.getFieldValue('custrecord_ebiz_inv_lp');
					var vPackcode=transaction.getFieldValue('custrecord_ebiz_inv_packcode');
					var vSKUStatus=transaction.getFieldValue('custrecord_ebiz_inv_sku_status');

					nlapiLogExecution('ERROR', 'qty', qty);
					nlapiLogExecution('ERROR', 'allocqty', allocqty);
					nlapiLogExecution('ERROR', 'varqty', vLineQty);
					transaction.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(allocqty)- parseFloat(vLineQty)).toFixed(5));
					nlapiSubmitRecord(transaction, false, true);

					var rcptrecordid3 = nlapiLoadRecord('workorder', vId);
					var woidWO= rcptrecordid3.getFieldValue('tranid');
					nlapiLogExecution('ERROR','woidWO  ',woidWO);
					vWOId=woidWO;

					var OTFilter = new Array();
					OTFilter.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vId));

					var OTSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, OTFilter, null);

					if(OTSearchResults != null && OTSearchResults != "")
					{
						for(var p=0;p<OTSearchResults.length;p++)
						{
							var id = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', OTSearchResults[p].getId());
							nlapiLogExecution('ERROR', 'Deleted Rec from OpenTask : ', id);
						}
					} 		

					nlapiLogExecution('ERROR', 'Deletion Success'); 

				}

				//nlapiSetLineItemValue('item', 'item', s,"");			 
				nlapiSetLineItemValue('item', 'custcol_ebizwoinventoryref', s,"");
				nlapiSetLineItemValue('item', 'custcol_ebizwoavalqty', s,""); 
				nlapiSetLineItemValue('item','custcol_locationsitemreceipt',s,"");	
				nlapiSetLineItemValue('item','serialnumbers',s,"");
				nlapiSetFieldValue('custbody_ebiz_update_confirm','F');
				nlapiLogExecution('ERROR', 'Custom columns data cleared'); 

			}
		}
	} 
	return false;
}


function getRecordDetails(WOId){
	var filter= new Array();
	nlapiLogExecution('ERROR','WOId',WOId);
	filter.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', WOId));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_line_no');

	var searchRecords = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filter, columns);
	if(searchRecords!=null && searchRecords!='')
		nlapiLogExecution('ERROR','searchRecords',searchRecords);
	return searchRecords;
}

function fnGetDetails(vitem,varAssemblyQty,itemcount)
{
	nlapiLogExecution('ERROR','Into Get Details');
	var vInvReturn=new Array();
	//alert("3");
	var strItem="",strQty=0,vcomponent;
	var ItemType = nlapiLookupField('item', vitem, 'recordType');
	nlapiLogExecution('ERROR','ItemType',ItemType);
	var searchresultsitem;
	if(ItemType!=null && ItemType !='')

	{
		nlapiLogExecution('ERROR','Item Type',ItemType);
		nlapiLogExecution('ERROR','Item Type is NULL ',ItemType);

		searchresultsitem = nlapiLoadRecord(ItemType, vitem); //1020
	}

	else
	{
		nlapiLogExecution('ERROR','Item Type is NULL ',ItemType);
	}
	var SkuNo=searchresultsitem.getFieldValue('itemid');
	var recCount=0; 
	//Written by Ganesh
	//alert("ItemCount : "+ itemcount);
	var vAlert='';
	nlapiLogExecution('ERROR','itemcount ',itemcount);
	for(var m=1; m<=itemcount;m++) 
	{
		//alert("SkuNo" + SkuNo);
		strItem = nlapiGetLineItemValue('item', 'item', m);
		//alert("strItem" + strItem);
		strQty = nlapiGetLineItemValue('item', 'quantity', m);
		//alert("strQty" + strQty);
		//alert("varAssemblyQty" + varAssemblyQty);
		nlapiLogExecution('ERROR','Hi1 ');
		var fields = ['custitem_item_family', 'custitem_item_group'];
		var ComponentItemType = nlapiLookupField('item', strItem, 'recordType');
		if (ComponentItemType != null && ComponentItemType !='')
		{
			nlapiLogExecution('ERROR','Hi2 ');
			var columns = nlapiLookupField(ComponentItemType, strItem, fields);


			var itemfamily = columns.custitem_item_family;
			var itemgroup = columns.custitem_item_group;
		}
		//avlqty= (parseFloat(strQty)*parseFloat(varAssemblyQty));
		//alert("avlqty " + avlqty);
		//var arryinvt=PickStrategieKittoStock(strItem, itemfamily, itemgroup, avlqty);
		nlapiLogExecution('ERROR','Hi3 ');
		var arryinvt=PickStrategieKittoStock(strItem, itemfamily, itemgroup, strQty);
		var vitemqty=0;
		var vrecid="" ;
		var vlp="",vlocation="",vqty="",vlotnoWithQty="",vremainqty="",location;
		vitemText=nlapiGetLineItemText('item', 'item', m);
		if(arryinvt.length>0)
		{
			var vLot="";
			var vPrevLot="";
			var vLotQty=0;
			var vTempLot="";
			var vBoolFound=false;

			//alert("vitemText" + vitemText);
			for (j=0; j < arryinvt.length; j++) 
			{			
				var invtarray= arryinvt[j];  	

				//vitem =searchresults[i].getValue('memberitem');		
				//vitemText =searchresults[i].getText('memberitem');


				if(vlp == "" || vlp == null)
					vlp = invtarray[2];
				else
					vlp = vlp+","+ invtarray[2];
				if(vTempLot == "" || vTempLot == null)
					vTempLot = invtarray[4];
				else
					vTempLot = vTempLot+","+ invtarray[4];
				if(vPrevLot==null || vPrevLot== "")
				{
					vPrevLot=invtarray[4];
					vLotQty=parseFloat(invtarray[0]);
					vLot=invtarray[4];
					vBoolFound=true;
				}
				else if(vPrevLot==invtarray[4])
				{
					vLotQty=vLotQty+ parseFloat(invtarray[0]);						
				}
				else if(vPrevLot!=invtarray[4])
				{
					if(vlotnoWithQty == "" || vlotnoWithQty == null)
						vlotnoWithQty = vPrevLot+"(" + vLotQty +")";//invtarray[7] is lotno id
					else
						vlotnoWithQty = vlotnoWithQty+ String.fromCharCode(5) + vPrevLot +"(" + vLotQty +")";

					if(vLot == "" || vLot == null)
						vLot = invtarray[4];
					else
						vLot = vLot+","+ invtarray[4];

					vPrevLot=invtarray[4];
					vLotQty=parseFloat(invtarray[0]);

				}
				if(vqty == "" || vqty == null)
					vqty = invtarray[0];
				else
					vqty = vqty+","+ invtarray[0];

				var qty1 = invtarray[0];

				if(vlocation == "" || vlocation == null)
					vlocation = invtarray[6];
				else
					vlocation = vlocation+","+ invtarray[6];




				if(vrecid == "" || vrecid == null)
					vrecid = invtarray[3];
				else
					vrecid = vrecid+","+ invtarray[3];




				if(vremainqty == "" || vremainqty == null)
					vremainqty = invtarray[5];
				else
					vremainqty = vremainqty+","+ invtarray[5];
				location = invtarray[1];
				vitemqty=parseFloat(vitemqty)+parseFloat(qty1);
			}
			if(vBoolFound==true)
			{
				/*if(vlotnoWithQty == "" || vlotnoWithQty == null)
					vlotnoWithQty = invtarray[4]+"(" + vLotQty +")";//invtarray[7] is lotno id
				else
					vlotnoWithQty = vlotnoWithQty+","+ invtarray[4]+"(" + vLotQty +")";*/

				if(vlotnoWithQty == "" || vlotnoWithQty == null)
					vlotnoWithQty = invtarray[4]+"(" + vLotQty +")";
				else
					vlotnoWithQty = vlotnoWithQty+ String.fromCharCode(5) + invtarray[4]+"(" + vLotQty +")";


				//alert("vTempLot" + vTempLot);
				//alert("vLot.split(',').length" + vLot.split(',').length);
				// alert("vLotQty " + vlotnoWithQty);				

				if(vLot.split(',').length>1)
					vLot=vlotnoWithQty;
			}
			//if(avlqty > vitemqty )
			if(strQty > vitemqty )			
			{ 
				vAlert=vAlert+vitemText+ ": You have only " + vitemqty +" available out of " +strQty + "    \n";

			}
			else 
			{
				/*alert("vlp" + vlp);
				alert("location" + location);
				alert("vrecid" + vrecid);
				alert("vLot" + vLot);
				alert("vTempLot" + vTempLot);
				alert("vqty" + vqty);
				alert("vlocation" + vlocation);*/

				nlapiSelectLineItem('item',m);
				try {

					//var InvValues = [vlp, location, vrecid, vLot,vTempLot,vqty,vlocation,vitemText,strItem,strQty];
					//vInvReturn.push(InvValues);
					nlapiSetCurrentLineItemValue('item','custcol_ebiz_lp',vlp);
					nlapiSetCurrentLineItemValue('item','custcol_locationsitemreceipt', location);//previously it is vlocation
					nlapiSetCurrentLineItemValue('item','custcol_ebizwoinventoryref', vrecid);
					//alert("vLot "+ vLot);
					//nlapiSetCurrentLineItemValue('item','serialnumbers', vLot);
					// For default Lot
					vLot=vitemText.replace(/ /g,"-");
					//alert("vLot "+ vLot);
					nlapiSetCurrentLineItemValue('item','serialnumbers', vLot);
					//nlapiSetCurrentLineItemValue('item','serialnumbers_display', vLot);					
					/*var newLot = vLot.split(','); 
					newLot = newLot.join(String.fromCharCode(5)); 
					nlapiSetCurrentLineItemValue('item','serialnumbers', newLot);*/

					var newLot = vLot.split(','); 
					newLot = newLot.join(String.fromCharCode(5)); 
					nlapiLogExecution('ERROR', 'Before Set Lot no', newLot);
					nlapiSetCurrentLineItemValue('item','serialnumbers', newLot);
					nlapiLogExecution('ERROR', 'After Set Lot no', newLot);
					if(vTempLot != null && vTempLot != "")
						nlapiSetCurrentLineItemValue('item','custcol_ebizwolotinfo', vTempLot);
					nlapiSetCurrentLineItemValue('item','custcol_ebizwoavalqty', vqty);
					nlapiSetCurrentLineItemValue('item','custcol_ebizwobinloc', vlocation);
					nlapiSetCurrentLineItemValue('item','custcol_ebizwolotlineno', m);
					nlapiCommitLineItem('item');
					nlapiLogExecution('ERROR', 'Committed ', 'Success');
				}
				catch (e) {

					if (e instanceof nlobjError) 
					{
						nlapiLogExecution('ERROR', 'system error', e.getCode()+ '\n' + e.getDetails());

					}

					else 
					{ 
						nlapiLogExecution('ERROR', 'unexpected error', e.toString());
						//alert('unexpected error' + '\n' + 'Error: ' + e.toString() );
					}
//					nlapiCancelLineItem('item');
					break;

				}
			}
		}
		else
		{
			vAlert=vAlert+vitemText+ ": You have only " + vitemqty +" available out of " +strQty + "    \n";
		}
	}
	if(vAlert!=null && vAlert!='')
	{ 
		/*var vErrorRet=new Array();
		vErrorRet.push('FAIL');
		vErrorRet.push(vAlert);
		return vErrorRet;*/
		return 'FAIL~' + vAlert;
	}
	else
		return 'SUCCESS';
}

function DeAllocateQuantity(type)
{
	nlapiLogExecution('ERROR', 'Into Before Sumit');
	var locationid=nlapiGetFieldValue('location');
	if(type=='edit')
	{	
		/*if(locationid == null || locationid == "")
		{
			nlapiLogExecution('ERROR','Please Select Location  ');
			var ErrorMsg = nlapiCreateError('CannotAllocate','Please Select Location ', true);
			throw ErrorMsg; //throw this error object, do not catch it

			//return false;
		}
		nlapiLogExecution('ERROR', 'type', type);
		DeallocateInv();
		//CheckAvailability();
		 */	
		nlapiLogExecution('ERROR', 'Into Edit Blocking');

		/* Commented edit blocking code*/
		var UpdateConfirm=nlapiGetFieldValue('custbody_ebiz_update_confirm');
		/*** The below code is merged from Boombah production account on 07thMar13 by Santosh as part of Standard bundle***/
		//added QcStatus Flag
		var QCStatusflag = nlapiGetFieldValue('custbody_ebiz_wo_qc_status');
		nlapiLogExecution('ERROR', 'QCStatusflag',QCStatusflag);

		nlapiLogExecution('ERROR', 'UpdateConfirm',UpdateConfirm);
		if(UpdateConfirm != 'T' && QCStatusflag!='2')
		{
			/*** up to here ***/
			var filters = new Array();
			var vId= nlapiGetFieldValue('id');
			nlapiLogExecution('ERROR', 'into Block vId', vId);
			if (vId != "") {
				filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', parseFloat(vId)));
			} 

			var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, null);

			if(searchresults != null && searchresults != "" && searchresults.length>0)
			{ 	  
				nlapiLogExecution('ERROR', 'searchresults.length', searchresults.length);
				nlapiLogExecution('ERROR','Data already Allocated not allow to edit ');
				var ErrorMsg = nlapiCreateError('CannotAllocate','You are not allowed to edit the record after genereated bin location', true);
				throw ErrorMsg; //throw this error object, do not catch it

			}
			else
			{
				return true;
			}	
		}
		else
		{	
			nlapiSetFieldValue('custbody_ebiz_update_confirm','F');

			nlapiLogExecution('ERROR', 'Else Block');
			return true;
		}	

	} 
	else if(type=='delete')
	{
		nlapiLogExecution('ERROR', 'type', type);
		DeallocateInv();
	}
	else if(type=='create')
	{
		if(locationid == null || locationid == "")
		{
			nlapiLogExecution('ERROR','Please Select Location  ');
			var ErrorMsg = nlapiCreateError('CannotAllocate','Please Select Location ', true);
			throw ErrorMsg; //throw this error object, do not catch it

			//return false;
		}
		else
		{	
			//CheckAvailability();

			nlapiLogExecution('ERROR', 'Into clear Custom fields before WO Create');
			var itemcount=nlapiGetLineItemCount('item'); 
			if(itemcount>0)
			{

				//var eBizWaveNo = GetMaxTransactionNo('WAVE');
				for(var s=1;s<=itemcount;s++)
				{
					//nlapiSetLineItemValue('item', 'item', s,"");
					nlapiSetFieldValue('custbody_ebiz_update_confirm','F');
					nlapiSetLineItemValue('item', 'custcol_ebizwoinventoryref', s,"");
					nlapiSetLineItemValue('item', 'custcol_ebizwoavalqty', s,""); 
					nlapiSetLineItemValue('item','custcol_locationsitemreceipt',s,"");	
					nlapiSetLineItemValue('item','serialnumbers',s,"");
					nlapiSetLineItemValue('item','custcol_ebiz_lp',s,"");
					nlapiSetLineItemValue('item','custcol_ebizwolotinfo',s,"");				 
					nlapiSetLineItemValue('item','custcol_ebizwobinloc',s,"");
					nlapiSetLineItemValue('item','custcol_ebizwolotlineno',s,"");
					nlapiLogExecution('ERROR', 'Custom columns data cleared'); 				 
				}
			}
		}

	}
}

function PickStrategieKittoStock(item,itemfamily,itemgroup,avlqty){
	//alert("5");
	var actallocqty = 0;
	var Resultarray = new Array();
	nlapiLogExecution('ERROR','item',item);
	nlapiLogExecution('ERROR','itemgroup',itemgroup);
	nlapiLogExecution('ERROR','itemfamily',itemfamily);

	var filters = new Array();	
	filters[0] = new nlobjSearchFilter('custrecord_ebizskupickrul', null, 'anyof', ['@NONE@',item]);
	var k=1;
	if (itemgroup != "" && itemgroup != null) {
		filters[k] = new nlobjSearchFilter('custrecord_ebizskugrouppickrul',null,  'anyof', ['@NONE@',itemgroup]);
		k=k+1;
	}
	if (itemfamily!= "" && itemfamily!= null) {
		filters[k] = new nlobjSearchFilter('custrecord_ebizskufamilypickrul', null, 'anyof', ['@NONE@',itemfamily]);
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_ebizpickzonerul');
	columns[2] = new nlobjSearchColumn('custrecord_ebizpickmethod');
	columns[3] = new nlobjSearchColumn('custrecord_ebizsequencenopickrul');

	//Fetching pick rule
	nlapiLogExecution('ERROR','Pick rule Fectching');
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters, columns);

	for (var i = 0; searchresults != null && i < searchresults.length; i++) {

		var searchresultpick = searchresults[i];				
		var vpickzone=searchresultpick.getValue('custrecord_ebizpickzonerul');			

		nlapiLogExecution('ERROR', "LP: " + LP);
		nlapiLogExecution('ERROR','PickZone',vpickzone);			
		var filterszone = new Array();	
		filterszone[0] = new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'is', vpickzone,null);
		//filterszone.push(new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', ['@NONE@', poLocn]));

		var columnzone = new Array();
		columnzone[0] = new nlobjSearchColumn('custrecord_locgroup_no');

		var searchresultszone = nlapiSearchRecord('customrecord_zone_locgroup', null, filterszone, columnzone);
		nlapiLogExecution('ERROR','Loc Group Fetching');

		for (var j = 0; searchresultszone != null && j < searchresultszone.length; j++) {

			var searchresultzone = searchresultszone[j];
			var vlocgroupno = searchresultzone.getValue('custrecord_locgroup_no');

			nlapiLogExecution('ERROR', 'LocGroup'+ searchresultzone.getValue('custrecord_locgroup_no'), searchresultzone.getText('custrecord_locgroup_no'));

			var filtersinvt = new Array();

			filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', item));
			filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['3','19']));
			filtersinvt.push(new nlobjSearchFilter('custrecord_outboundlocgroupid','custrecord_ebiz_inv_binloc', 'is', vlocgroupno));

			nlapiLogExecution('ERROR', "custrecord_ebiz_inv_sku: " + item);

			var columnsinvt = new Array();
			columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
			columnsinvt[1] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
			columnsinvt[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
			columnsinvt[3] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
			columnsinvt[4] = new nlobjSearchColumn('custrecord_ebiz_qoh');
			columnsinvt[5] = new nlobjSearchColumn('custrecord_pickseqno');
			columnsinvt[6] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');
			columnsinvt[5].setSort();
			columnsinvt[4].setSort(false);

			var searchresultsinvt = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);
			for (var l = 0; searchresultsinvt != null && l < searchresultsinvt.length; l++) {

				var searchresult = searchresultsinvt[l];
				var actqty = searchresult.getValue('custrecord_ebiz_qoh');
				var allocqty = searchresult.getValue('custrecord_ebiz_alloc_qty');
				var LP = searchresult.getValue('custrecord_ebiz_inv_lp');
				var vactLocation = searchresult.getValue('custrecord_ebiz_inv_binloc');
				var vactLocationtext = searchresult.getText('custrecord_ebiz_inv_binloc');
				var vlotno = searchresult.getValue('custrecord_ebiz_inv_lot');
				var vlotText = searchresult.getText('custrecord_ebiz_inv_lot');
				var Recid = searchresult.getId(); 


				//alert(" 664 actqty:" +actqty);
				//alert(" 664 allocqty:" +allocqty);
				//alert(" 664 LP:" +LP);
				//alert(" 664 vactLocation:" +vactLocation);
				//alert(" 664 vactLocationtext:" +vactLocationtext);
				//alert(" 664 vlotno:" +vlotno);
				//alert(" 664 vlotText:" +vlotText);
				//alert(" 664 Recid:" +Recid);


				nlapiLogExecution('ERROR', 'Recid1', Recid);
				if (isNaN(allocqty)) {
					allocqty = 0;
				}
				if (allocqty == "") {
					allocqty = 0;
				}
				if( parseFloat(actqty)<0)
				{
					actqty=0;
				}
				nlapiLogExecution('ERROR', "allocqty: " + allocqty);
				nlapiLogExecution('ERROR', "LP: " + LP);
				var remainqty=0;

				//Added by Ganesh not to allow negative or zero Qtys 
				if(parseFloat(actqty) >=0 && parseFloat(allocqty)>=0)
					remainqty = parseFloat(actqty) - parseFloat(allocqty);


				var cnfmqty;

////				alert("remainqty"+remainqty );
////				alert("avlqty"+avlqty );
				if (parseFloat(remainqty) > 0) {

					////alert("parseFloat(avlqty) "+parseFloat(avlqty));

					if ((parseFloat(avlqty) - parseFloat(actallocqty)) <= parseFloat(remainqty)) {
						cnfmqty = parseFloat(avlqty) - parseFloat(actallocqty);
						actallocqty = avlqty;
						////alert('cnfmqty in if '+cnfmqty);
					}
					else {
						cnfmqty = remainqty;
						////alert('cnfmqty2 ' +cnfmqty);
						actallocqty = parseFloat(actallocqty) + parseFloat(remainqty);
					}

					//alert(" 708 cnfmqty:" +cnfmqty);
					//alert(" 709 actallocqty:" +actallocqty);

					////alert('BeforeIf ' +cnfmqty);
					if (parseFloat(cnfmqty) > 0) {
						////alert('AfterIf ' +cnfmqty);
						//Resultarray
						var invtarray = new Array();
						////alert('cnfmqty3 ' +cnfmqty);
						invtarray[0]= cnfmqty;
						////alert('cnfmqty4 ' +cnfmqty);
						invtarray[1]= vactLocation;
						invtarray[2] = LP;
						invtarray[3] = Recid;
						invtarray[4] = vlotText;
						invtarray[5] =remainqty;
						invtarray[6] =vactLocationtext;
						invtarray[7] =vlotno;
						//alert("cnfmqty "+cnfmqty);
						//alert("cnfmqty " + cnfmqty);
						//alert(vlotno);
						//alert(vlotText);
						nlapiLogExecution('ERROR', 'RecId:',Recid);
						//invtarray[3] = vpackcode;
						//invtarray[4] = vskustatus;
						//invtarray[5] = vuomid;
						Resultarray.push(invtarray);								
					}


				}

				if ((avlqty - actallocqty) == 0) {

					return Resultarray;

				}

			} 
		}
	}

	return Resultarray;  
}
//Case# 20148202 starts
function Onload(type, form, request)
{
	var WOId=nlapiGetFieldValue('id');
	nlapiLogExecution('DEBUG','WOId ',WOId);
	nlapiLogExecution('DEBUG','type ',type);
	if(WOId!=null&&WOId!="")
	{
		var filter = new Array();
		filter[0]= new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', WOId);
		var column = new Array();
		column[0] = new nlobjSearchColumn('custrecord_wms_status_flag'); 
		var searchRec = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filter, column);
		if(searchRec != null && searchRec != "")
		{
			var closebutton = form.getButton('closeremaining');  
			if(closebutton!=null)
				closebutton.setDisabled(true);
			if(type=='edit')
			{
				var voidbutton = form.getButton('void');
				if(voidbutton!=null)
					voidbutton.setDisabled(true);
			}
		}
	}
}