/***************************************************************************
 eBizNET Solutions
 ****************************************************************************
 *
 *  $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Suitelet/Attic/ebiz_Inbound_ReversalQb_SL.js,v $
 *  $Revision: 1.1.2.1.12.2 $
 *  $Date: 2015/11/05 16:01:45 $
 *  $Author: deepshikha $
 *  $Name: t_WMS_2015_2_StdBundle_1_78 $
 *
 * DESCRIPTION
 *  Functionality
 *
 * REVISION HISTORY
 *  $Log: ebiz_Inbound_ReversalQb_SL.js,v $
 *  Revision 1.1.2.1.12.2  2015/11/05 16:01:45  deepshikha
 *  2015.2 issue fixes
 *  201415043
 * 
 *  Revision 1.1.2.1.12.1  2015/10/21 09:20:06  schepuri
 *  case# 201415013,201415043
 * 
 *  Revision 1.1.2.1.8.2  2014/02/05 14:51:56  sponnaganti
 *  case# 20127058
 *  Scriptid and deploy ids are changed
 * 
 *  Revision 1.1.2.1.8.1  2014/02/04 16:15:09  rrpulicherla
 *  CASE201112/CR201113/LOG201121
 * 
 *  Inbound reversal
 * 
 *  Revision 1.1.2.1  2013/05/16 11:25:32  spendyala
 *  CASE201112/CR201113/LOG201121
 *  New scripts for inbound reversal taken from TSG
 * 
 *  Revision 1.1.2.2.4.3.4.3  2013/03/21 14:18:14  snimmakayala
 *  CASE201112/CR201113/LOG2012392  
 *
 ****************************************************************************/

function InboundReversalQbSuitelet(request, response){
	var ctx = nlapiGetContext();
	if (request.getMethod() == 'GET') 
	{
		var context = nlapiGetContext();
		nlapiLogExecution('ERROR','Remaining usage at the start of GET',context.getRemainingUsage());

		var form = nlapiCreateForm('Inbound Reversal');
		var TranTypeField = form.addField('custpage_trantype', 'select', 'Tran Type').setLayoutType('startrow', 'none');
		TranTypeField.addSelectOption('purchaseorder', 'Purchase Order');
		TranTypeField.addSelectOption('transferorder', 'Transfer Order');
		TranTypeField.addSelectOption('returnauthorization', 'RMA');
		TranTypeField.setDefaultValue('purchaseorder'); 
		var orderField = form.addField('custpage_orderlist', 'text', 'Order #'); 
		var lpField = form.addField('custpage_lp', 'text', 'LP#');		  
		var button = form.addSubmitButton('Display');

		response.writePage(form);
	}
	else
	{
		var form = nlapiCreateForm('Inbound Reversal');
		var ordId = "";
		var vTrantype="";
		var vLP="";
		var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
		msg.setLayoutType('outside','startcol');
		if(request.getParameter('custpage_trantype') != null && request.getParameter('custpage_trantype') != '')
		{	
			vTrantype=request.getParameter('custpage_trantype');
		}
		if(request.getParameter('custpage_orderlist') != null && request.getParameter('custpage_orderlist') != '')
		{	
			ordId=GetOrdInternalId(request.getParameter('custpage_orderlist'),vTrantype);
		}
		nlapiLogExecution('ERROR','request.getParameter(custpage_lp) new',request.getParameter('custpage_lp'));
		if(request.getParameter('custpage_lp') != null && request.getParameter('custpage_lp') != '')
		{	
			nlapiLogExecution('ERROR','request.getParameter(custpage_lp)',request.getParameter('custpage_lp'));
			//vLP=request.getParameter('custpage_lp');
			var ordernumber = getordernameforLP(request.getParameter('custpage_lp'));
		
			if(ordernumber != null && ordernumber != '')
			{
				nlapiLogExecution('ERROR','ordernumber new',ordernumber);
				vLP = GetOrdInternalId(ordernumber,vTrantype);
			}
			
		}
		nlapiLogExecution('ERROR','ordId new',ordId);
		nlapiLogExecution('ERROR','vLP new',vLP);

		if(ordId != null && ordId != '' && ordId != '-1')
		{
			var ordArray = new Array();		
			ordArray["custpage_orderlist"] = ordId;
			ordArray["custpage_trantype"] = vTrantype;
			ordArray["custpage_lp"] = request.getParameter('custpage_lp');
			//case# 20127058 starts (script id and deploy id changed)
			response.sendRedirect('SUITELET', 'customscript_inboundreversal','customdeploy_inboundreversal', false, ordArray);
			//case# 20127058 end
		}
		//else if(request.getParameter('custpage_lp')!='' && request.getParameter('custpage_lp')!='')
		else if(vLP!='' && vLP!='' && vLP != '-1')
		{
			var ordArray = new Array();		
			ordArray["custpage_orderlist"] = ordId;
			ordArray["custpage_trantype"] = vTrantype;
			ordArray["custpage_lp"] = request.getParameter('custpage_lp');

			response.sendRedirect('SUITELET', 'customscript_inboundreversal','customdeploy_inboundreversal', false, ordArray);
		}
		else
		{
			msg.setDefaultValue("<div id='div__alert' align='center'><script>showAlertBox('div__alert', 'Error', 'NO Records available for given Order#.', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
		}
		response.writePage(form);
	}
}
function getordernameforLP(lp)
{
	var ordernumber="";
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_lpno', null, 'is', lp));

	var columns = new Array();
	columns.push(new nlobjSearchColumn('name'));

	var opentasks = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	
	if(opentasks!=null && opentasks!='' && opentasks.length>0)
	{
		ordernumber=opentasks[0].getValue('name');
	}

	nlapiLogExecution('ERROR','Out of getordernameforLP (Output)',ordernumber);
	if(ordernumber == null || ordernumber == '')
	{
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_ebiztask_lpno', null, 'is', lp));

		var columns = new Array();
		//columns.push(new nlobjSearchColumn('custrecord_ebiztask_ebiz_order_no'));// from this field we are getting prefix as purchase order
		columns.push(new nlobjSearchColumn('name'));

		var closetasks = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filters, columns);	
		if(closetasks!=null && closetasks!='' && closetasks.length>0)
		{
			ordernumber=closetasks[0].getValue('name');
		}
	}

	return ordernumber;

}

function GetOrdInternalId(OrdText,TranType)
{
	nlapiLogExecution('ERROR','Into GetOrdInternalId (Input)',OrdText);

	var ActualOrdID='-1';

	var filter=new Array();
	filter.push(new nlobjSearchFilter('tranid',null,'is',OrdText.toString()));
	filter.push(new nlobjSearchFilter('mainline',null,'is','T'));

	var searchrec=nlapiSearchRecord(TranType,null,filter,null);	 
	if(searchrec!=null && searchrec!='' && searchrec.length>0)
	{
		ActualOrdID=searchrec[0].getId();
	}

	nlapiLogExecution('ERROR','Out of GetOrdInternalId (Output)',ActualOrdID);

	return ActualOrdID;
}
