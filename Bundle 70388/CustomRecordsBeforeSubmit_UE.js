/***************************************************************************
	  		   eBizNET Solutions Inc               
 ****************************************************************************/
/**
 * Function to be used as BeforeSubmit event for custom records to prevent the user
 * from deleting the custom records
 * 
 * @param type
 */
function CustomRecordsBeforeSubmit(type){
	var ctx = nlapiGetContext();
	var exeCtx = ctx.getExecutionContext();
	var customForm = nlapiGetFieldValue('customform');
	var role = nlapiGetRole();
	
	var message = 'type = ' + type + '<br>';
	message = message + 'execution context = ' + exeCtx + '<br>';
	message = message + 'custom form = ' + customForm + '<br>';
	message = message + 'role = ' + role;
	nlapiLogExecution('DEBUG', 'output', message);

	// Trap only the 'delete' event
	if(type == 'delete'){
		// Further check to ensure that only requests from the user interface and userevent
		// are caught
		if(customForm != null || exeCtx == 'userinterface' || exeCtx == 'userevent'){
			nlapiLogExecution('DEBUG', 'Creating Error Object', 'Creating Error Object');
			var cannotDelError = nlapiCreateError('CannotDelete',
					'This record is restricted for manual deletion from the user interface.', true);
			throw cannotDelError;  // Netsuite Easter Egg: throw this error object, do not (never) catch it
		}
	}
}
