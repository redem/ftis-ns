/***************************************************************************
    eBizNET Solutions
****************************************************************************
*
*  $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_bolreportPDF_Standard.js,v $
*  $Revision: 1.1.2.1.4.1.4.2 $
*  $Date: 2013/07/17 06:33:29 $
*  $Author: mbpragada $
*  $Name: t_NSWMS_2013_1_7_43 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_bolreportPDF_Standard.js,v $
*  Revision 1.1.2.1.4.1.4.2  2013/07/17 06:33:29  mbpragada
*  Case# 20123471
*  BOL after trailer depart
*
*  Revision 1.1.2.1.4.1.4.1  2013/03/08 14:45:50  skreddy
*  CASE201112/CR201113/LOG201121
*  Code merged from Endochoice as part of Standard bundle
*
*  Revision 1.1.2.1.4.1  2012/11/01 14:55:02  schepuri
*  CASE201112/CR201113/LOG201121
*  Decimal Qty Conversions
*
*  Revision 1.1.2.1  2012/08/26 14:18:07  spendyala
*  CASE201112/CR201113/LOG201121
*  New Script File for BOL PDF.
*
*
****************************************************************************/


function getallpalletcount(distinctSOno)
{
	var totalnoofpallets=0,searchresultspallet='';
	var palletfilters = new Array();				
	palletfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));				
	palletfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', distinctSOno));
	palletfilters.push(new nlobjSearchFilter('custrecord_ebizfootprint', null, 'isnotempty'));	

	var palletcolumns = new Array();
	palletcolumns[0] = new nlobjSearchColumn('custrecord_ebizfootprint');	
	palletcolumns[1] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	palletcolumns[2] = new nlobjSearchColumn('custrecord_ebiz_trailer_no');
	searchresultspallet = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, palletfilters, palletcolumns);
//	for ( var i = 0 ; searchresultspallet !=null && i < searchresultspallet.length ; i++){	

//	totalnoofpallets=totalnoofpallets + searchresultspallet[i].getValue('custrecord_ebizfootprint');
//	}
	if(searchresultspallet == null || searchresultspallet =='')
	{
		var palletfilters = new Array();				
		palletfilters.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', ['3']));				
		palletfilters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no', null, 'anyof', distinctSOno));
		palletfilters.push(new nlobjSearchFilter('custrecord_ebiztask_ebizfootprint', null, 'isnotempty'));	

		var palletcolumns = new Array();
		palletcolumns[0] = new nlobjSearchColumn('custrecord_ebiztask_ebizfootprint');	
		palletcolumns[1] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_order_no');
		palletcolumns[2] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_trailer_no');
		searchresultspallet = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, palletfilters, palletcolumns);
	}
	return searchresultspallet;
}
function getallBOL(distinctSOno)
{
	nlapiLogExecution('ERROR','getallBOL');
	var bolfilters = new Array();
	var bolValues = new Array();
	bolfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['14']));
	if(distinctSOno !=null && distinctSOno !=""){
		bolfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', distinctSOno));	}	

	var bolcolumns = new Array();
	bolcolumns[0] = new nlobjSearchColumn('custrecord_bol');	 
	bolcolumns[1] = new nlobjSearchColumn('custrecord_pro');
	bolcolumns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	bolcolumns[3] = new nlobjSearchColumn('custrecord_sub_bol');
	bolcolumns[4] = new nlobjSearchColumn('custrecord_ebiz_trailer_no');
	searchresultsbol = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, bolfilters, bolcolumns);
//	if( searchresultsbol != null &&  searchresultsbol != "")
//	{
//	var bollable=searchresultsbol[0].getValue('custrecord_sub_bol');
//	var proNum=searchresultsbol[0].getValue('custrecord_pro');
//	bolValues.push(bollable);
//	bolValues.push(proNum);
//	}
	return searchresultsbol;
}
function GetIndividiualItemEachQuantity(getAllEachQuantityForItemRes,Item,packcode){

	var individiualItemQuantityListT = new Array();
	var individiualItemQuantityList = new Array();	
	try{

		if (getAllEachQuantityForItemRes != null && getAllEachQuantityForItemRes.length >0 && Item != null && Item.length>0){
			for ( var i = 0 ; i < getAllEachQuantityForItemRes.length ; i++){				
				if (getAllEachQuantityForItemRes[i].getValue('custrecord_ebizitemdims')  == Item )
				{
					individiualItemQuantityList = new Array();
					individiualItemQuantityList[0] = getAllEachQuantityForItemRes[i].getValue('custrecord_ebizqty');
					individiualItemQuantityList[1] = getAllEachQuantityForItemRes[i].getValue('custrecord_ebizweight');
					individiualItemQuantityList[2] = getAllEachQuantityForItemRes[i].getValue('custrecord_ebizpackcodeskudim');
					individiualItemQuantityList[3] = getAllEachQuantityForItemRes[i].getValue('custrecord_ebizitemdims');

					individiualItemQuantityListT.push(individiualItemQuantityList);
				}
			}
		}
	}
	catch(exception){
		nlapiLogExecution('ERROR', 'individiualItemQuantityListT', exception);
	}	

	return individiualItemQuantityListT;
}
function getIndividualPalletQty(getallpalletcountRes,distinctSOno){
	var totalpalletqty=0;
	var individiualPalletQty = new Array();
	var individiualPalletQtyT = new Array();	
	try{

		if (getallpalletcountRes != null && getallpalletcountRes.length >0 && distinctSOno != null && distinctSOno.length>0){
			for ( var i = 0 ; i < getallpalletcountRes.length ; i++){				
				if (getallpalletcountRes[i].getValue('custrecord_ebiz_order_no')  == distinctSOno )
				{
					individiualPalletQty = new Array();
					totalpalletqty=parseFloat(totalpalletqty) + parseFloat(getallpalletcountRes[i].getValue('custrecord_ebizfootprint'));
					individiualPalletQty[0] = totalpalletqty;	

					individiualPalletQtyT.push(individiualPalletQty);
				}
			}
		}
	}
	catch(exception){
		nlapiLogExecution('ERROR', 'getIndividualPalletQty', exception);
	}	

	return individiualPalletQtyT;
}
function GetIndividiualBOL(getallBOLRes,distinctSOno){

	nlapiLogExecution('ERROR','Into GetIndividiualBOL',getallBOLRes);
	nlapiLogExecution('ERROR','distinctSOno',distinctSOno);
	var individiualBOL = new Array();
	var individiualBOLT = new Array();	
	try{

		if (getallBOLRes != null && getallBOLRes.length >0 && distinctSOno != null && distinctSOno.length>0){
			for ( var i = 0 ; i < getallBOLRes.length ; i++){				
				if (getallBOLRes[i].getValue('custrecord_ebiz_order_no')  == distinctSOno )
				{
					individiualBOL = new Array();					
					individiualBOL[0] = getallBOLRes[i].getValue('custrecord_bol');
					individiualBOL[1] = getallBOLRes[i].getValue('custrecord_pro');
					individiualBOL[2] = getallBOLRes[i].getValue('custrecord_sub_bol');
					individiualBOL[3] = getallBOLRes[i].getValue('custrecord_ebiz_trailer_no');

					individiualBOLT.push(individiualBOL);
				}
			}
		}
	}
	catch(exception){
		nlapiLogExecution('ERROR', 'GetIndividiualBOL', exception);
	}	
	return individiualBOLT;
}
function GetIndividiualSOdetails(getallSOAddressRes,distinctSOno){

	var individiualSO = new Array();
	var individiualSOT = new Array();	
	try{

		if (getallSOAddressRes != null && getallSOAddressRes.length >0 && distinctSOno != null && distinctSOno.length>0){
			for ( var i = 0 ; i < getallSOAddressRes.length ; i++){				
				if (getallSOAddressRes[i].getValue('internalid')  == distinctSOno )
				{
					individiualSO = new Array();					
					individiualSO[0] = getallSOAddressRes[i].getValue('tranid');
					individiualSO[1] = getallSOAddressRes[i].getValue('shipaddressee');					
					individiualSO[2] = getallSOAddressRes[i].getValue('custbody_nswms_company');
					individiualSO[3] = getallSOAddressRes[i].getValue('custbody_shipment_no');
					individiualSO[4] = getallSOAddressRes[i].getText('shipmethod');
					individiualSO[5] = getallSOAddressRes[i].getText('custbody_nswmsfreightterms');
					individiualSO[6] = getallSOAddressRes[i].getText('custbody_salesorder_carrier');
					individiualSO[7] = getallSOAddressRes[i].getValue('location');					
					individiualSO[8] = getallSOAddressRes[i].getValue('shipaddress1');
					individiualSO[9] = getallSOAddressRes[i].getValue('shipaddress2');
					individiualSO[10] = getallSOAddressRes[i].getValue('shipcity');					
					individiualSO[11] = getallSOAddressRes[i].getValue('shipstate');
					individiualSO[12] = getallSOAddressRes[i].getValue('shipcountry');
					individiualSO[13] = getallSOAddressRes[i].getValue('shipzip');					
					individiualSO[14] = getallSOAddressRes[i].getValue('custbody_customer_phone');
					individiualSO[15] = getallSOAddressRes[i].getValue('otherrefnum');
					individiualSO[16] = getallSOAddressRes[i].getValue('entity');
					individiualSO[17] = getallSOAddressRes[i].getValue('tranid');
					//individiualSO[18] = getallSOAddressRes[i].getValue('custbody_edi_dtm063');
					
					//added on 250812 by suman
					individiualSO[19] = getallSOAddressRes[i].getText('location');
					individiualSO[20] = getallSOAddressRes[i].getText('entity');
					individiualSO[21] = getallSOAddressRes[i].getValue('message');
					individiualSO[22] = getallSOAddressRes[i].getValue('shipdate');
					
					//end of code as of 250812
					individiualSOT.push(individiualSO);
				}
			}
		}
	}
	catch(exception){
		nlapiLogExecution('ERROR', 'GetindividiualSO', exception);
	}	
	return individiualSOT;
}
function getallSOAddress(distinctSOno){
	nlapiLogExecution('ERROR', 'GetSalesOrderDetails', 'Start');	
	try
	{	
		var filters = new Array();
		var columns = new Array();
		filters.push(new nlobjSearchFilter('internalid', null, 'anyof', distinctSOno));
		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

		columns[0] = new nlobjSearchColumn('tranid');
		columns[1] = new nlobjSearchColumn('tranid');	
		columns[2] = new nlobjSearchColumn('tranid');
		columns[3] = new nlobjSearchColumn('shipmethod');
		columns[4] = new nlobjSearchColumn('shipmethod');
		columns[5] = new nlobjSearchColumn('shipaddressee');
		columns[6] = new nlobjSearchColumn('shipaddress1');
		columns[7] = new nlobjSearchColumn('shipaddress2');
		columns[8] = new nlobjSearchColumn('shipcity');
		columns[9] = new nlobjSearchColumn('shipstate');
		columns[10] = new nlobjSearchColumn('shipcountry');
		columns[11] = new nlobjSearchColumn('shipzip');
		columns[12] = new nlobjSearchColumn('custbody_nswmspoexpshipdate');
		columns[13] = new nlobjSearchColumn('custbody_nswmsactualarrivaldate');
		columns[14] = new nlobjSearchColumn('custbody_nswmssodestination');
		columns[15] = new nlobjSearchColumn('custbody_nswmssoroute');
		columns[16] = new nlobjSearchColumn('class');
		columns[17] = new nlobjSearchColumn('entity');
		columns[18] = new nlobjSearchColumn('billaddressee');
		columns[19] = new nlobjSearchColumn('billaddress1');
		columns[20] = new nlobjSearchColumn('billaddress2');
		columns[21] = new nlobjSearchColumn('billaddress3');
		columns[22] = new nlobjSearchColumn('billcity');
		columns[23] = new nlobjSearchColumn('billstate');
		columns[24] = new nlobjSearchColumn('billzip');
		columns[25] = new nlobjSearchColumn('billcountry');
		columns[26] = new nlobjSearchColumn('billphone');
		columns[27] = new nlobjSearchColumn('location');
		columns[28] = new nlobjSearchColumn('custbody_nswms_company');
		columns[29] = new nlobjSearchColumn('department');
		columns[30] = new nlobjSearchColumn('terms');
		columns[31] = new nlobjSearchColumn('custbody_nswmssoordertype');		                                   
		columns[32] = new nlobjSearchColumn('custbody_nswmspriority');
		columns[33] = new nlobjSearchColumn('otherrefnum');
		columns[34] = new nlobjSearchColumn('shipphone');
		columns[35] = new nlobjSearchColumn('shipphone');
		columns[36] = new nlobjSearchColumn('shipphone');
		columns[37] = new nlobjSearchColumn('custbody_nswmsarrivaldate');
		columns[38] = new nlobjSearchColumn('internalid');		
		columns[39] = new nlobjSearchColumn('class');
		columns[40] = new nlobjSearchColumn('custbody_shipment_no');
		columns[41] = new nlobjSearchColumn('custbody_total_weight');
		columns[42] = new nlobjSearchColumn('class');	
		columns[43] = new nlobjSearchColumn('custbody_nswmsfreightterms');	
		columns[44] = new nlobjSearchColumn('custbody_salesorder_carrier');	
//		columns[45] = new nlobjSearchColumn('shipaddr1');	
//		columns[46] = new nlobjSearchColumn('shipaddr2');
		columns[45] = new nlobjSearchColumn('custbody_customer_phone');
		columns[46] = new nlobjSearchColumn('message');
		columns[47] = new nlobjSearchColumn('shipdate');
	//	columns[46] = new nlobjSearchColumn('custbody_edi_dtm063');

		var ResSSODetails = nlapiSearchRecord('salesorder', null, filters,columns);

	}

	catch(exception) 
	{
		nlapiLogExecution('ERROR', 'GetSalesOrderDetails', exception);
	}
	nlapiLogExecution('ERROR', 'ResSSODetails', ResSSODetails);
	if(ResSSODetails !=null)
		nlapiLogExecution('ERROR', 'ResSSODetails length', ResSSODetails.length);
	return ResSSODetails;

}


function bolReportPDFSuitelet(request, response){
	if (request.getMethod() == 'GET') {	

		var context = nlapiGetContext();
		nlapiLogExecution('ERROR','Remaining usage at Page statr',context.getRemainingUsage());

		var form = nlapiCreateForm('BOL Report PDF');
		var vQbBol ="",vQbcarriertype="",companyname='';
		var vfulfillmentordno ="",vQbcarrier='',shipmentno="";
		var vWaveno ="";
		var ordno ="",trailerName,masterShipperNo,trailerName='',vCarrierQb='',vCarrierQbId='';


		if(request.getParameter('custpage_carriertype')!=null && request.getParameter('custpage_carriertype')!="")
		{
			vQbcarriertype = request.getParameter('custpage_carriertype');
		} 
		if(request.getParameter('custparam_ebiz_bolno')!=null && request.getParameter('custparam_ebiz_bolno')!="")
		{
			vQbBol = request.getParameter('custparam_ebiz_bolno');
		} 
		if(request.getParameter('custparam_fulfillmentordno')!=null && request.getParameter('custparam_fulfillmentordno')!="")
		{
			vfulfillmentordno = request.getParameter('custparam_fulfillmentordno');
		} 
		if(request.getParameter('custparam_waveno')!=null && request.getParameter('custparam_waveno')!="")
		{
			vWaveno = request.getParameter('custparam_waveno');
		} 
		if(request.getParameter('custparam_ordno')!=null && request.getParameter('custparam_ordno')!="")
		{
			ordno = request.getParameter('custparam_ordno');
		} 
		if(request.getParameter('custparam_shipment')!=null && request.getParameter('custparam_shipment')!="")
		{
			shipmentno = request.getParameter('custparam_shipment');
		}
		if(request.getParameter('custparam_shipment')!=null && request.getParameter('custparam_shipment')!="")
		{
			shipmentno = request.getParameter('custparam_shipment');
		}
		if(request.getParameter('custpage_trailername')!=null && request.getParameter('custpage_trailername')!="")
		{
			trailerName = request.getParameter('custpage_trailername');
		}
		if(request.getParameter('custpage_vcarrierqb')!=null && request.getParameter('custpage_vcarrierqb')!="")
		{
			vCarrierQb = request.getParameter('custpage_vcarrierqb');
		}
		if(request.getParameter('custpage_vcarrierqbId')!=null && request.getParameter('custpage_vcarrierqbId')!="")
		{
			vCarrierQbId = request.getParameter('custpage_vcarrierqbId');
		}

		var str = 'vQbBol=' + vQbBol;
		str = str + '<br>' + 'vfulfillmentordno=' + vfulfillmentordno;
		str = str + '<br>' + 'vWaveno=' + vWaveno;
		str = str + '<br>' + 'ordno=' + ordno;
		str = str + '<br>' + 'shipmentno=' + shipmentno;

		nlapiLogExecution('ERROR', 'Request Parameters', str);

		//to get  sealnumber
		var sealno='';
		if(trailerName !=null && trailerName !=""){
			var filtertrailer = new nlobjSearchFilter('name', null,	'is', trailerName);}
		var columntrailer = new Array();
		columntrailer[0] = new nlobjSearchColumn('custrecord_ebizpro');
		columntrailer[1] = new nlobjSearchColumn('custrecord_ebizseal');
		columntrailer[2] = new nlobjSearchColumn('custrecord_ebizcompanytrailer');
		nlapiLogExecution('ERROR','before getting pro,sealno',context.getRemainingUsage());
		var searchrecordtrailer = nlapiSearchRecord('customrecord_ebiznet_trailer', null,filtertrailer, columntrailer);
		if(searchrecordtrailer !=null && searchrecordtrailer !=""){
			//var proNum = searchrecordtrailer[0].getValue('custrecord_ebizpro'); 
			//sealno = searchrecordtrailer[0].getValue('custrecord_ebizseal');
			companyname = searchrecordtrailer[0].getText('custrecord_ebizcompanytrailer');
		}

		var companyid;//customerpo;
		var locationlist;
		var shiptoAddress1="",childbollable="",shiptoAddress2="",shiptocity="",shiptostate="",shiptocountry="",shiptocompany="",customer="",shiptozipcode="",shiptophone="",shiptocompany1="",shiptoso="",shiptocarrier="";
		var shipfromcity="",loadnumber="",freightterms="",carrier="",shipfromcountry="",shipfromzipcode="",shipfromaddress1="",shipfromaddress2="",shipfromphone="",shipfromstate="",shipfromcompname="",locationid="",locationidname="";
		var shipadd="";
		var shiptoTotAdd="";

		nlapiLogExecution('ERROR','before getting shipto',context.getRemainingUsage());

		var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body  font-size=\"8\"  size=\"A4\" padding-top=\" 13mm\" padding-right=\"3mm\"  padding-left=\"3mm\" font-family='Helvetica'>\n";


		var  totalitemeachQuantity1=0,totalitemeachQuantity=0,totalglobalitemeachQuantity=0,itemeachQuantity1=0,itemeachQuantity=0,searchresultsWaveNo='',CustomerPO='',ordernumber='',CustomerPO1='',ordernumber1='',itemId ,totalordqty=0,totalnoofpallets=0,totalprodwght=0,totalpalletwght=0,totalheaderqty=0,packCode ,pallets,mustdelshipdate ,orderqty ,ordernumber ,itempalletQuantity,itemcaseQuantity,noofpallets=0,productweight,palletweight=0,totalpalletweight=0,totalweight;
		var itemId1 ,footprint1,footprint,totalordqty1=0,totalnoofpallets1=0,totalprodwght1=0,totalpalletwght1=0,totalheaderqty1=0,packCode1 ,pallets1,mustdelshipdate1 ,orderqty1 ,soid1 ,ordernumber1 ,itempalletQuantity1,itemcaseQuantity1,noofpallets1,productweight1,palletweight1,totalweight1;
		var sysdate=DateStamp();
		var waveno = new Array();
		var waveno1 = new Array();
		var soid=new Array();
		var packcode=new Array();
		var itemarray = new Array();
		var vdate = sysdate.toString();
		var globalOrdqty=0,globalPalletqty=0,globalEachqty=0,productweightfre=0;
		var globalOrdqty1=0,globalPalletqty1=0,globalEachqty1=0;
		var globalproductweight=0,totalglobalproductweight=0,totalglobalpalletweight=0,totalitemcaseQuantity=0;
		var globalproductweight1=0,totalglobalproductweight1=0,totalitemcaseQuantity1=0;var HeaderDetails = new Array();

		var globalproductweight2=0,globalitempalletQuantity2=0,totalglobalproductweightfre2=0,totproductweightfre2=0,totalglobalproductweight2=0,totalglobalpalletweight2=0,totalitemcaseQuantity2=0;
		var globalproductweight2=0,totalglobalproductweight2=0,totalitemcaseQuantity2=0;
		var  totalitemeachQuantity2=0,totalitemeachQuantity2=0,totalglobalitemeachQuantity2=0,itemeachQuantity2=0,itemeachQuantity2=0,itemId2 ,totalordqty2=0,totalnoofpallets2=0,totalprodwght2=0,totalpalletwght2=0,totalheaderqty2=0,packCode2 ,pallets2,mustdelshipdate2 ,orderqty2 ,ordernumber2 ,itempalletQuantity2,itemcaseQuantity2=0,noofpallets2=0,productweight2=0,palletweight2=0,totalpalletweight2=0,totalweight2=0;
		var itemId2 ,footprint2,totalordqty2=0,totalnoofpallets2=0,totalprodwght2=0,totalpalletwght2=0;
		var totalheaderqty2=0,packCode2 ,pallets2,orderqty2 ,itempalletQuantity2;
		var itemcaseQuantity2,noofpallets2,productweight2,palletweight2,totalweight2;	
		var searchresultspallet='',prevso2='';globalitempalletQuantity=0;

		//modified
//		The below code is commneted by Satish.N on 07/20/2012 as part of performance tuning.
//		var wavefilters = new Array();

//		if(trailerName !=null && trailerName !=""){
//		wavefilters.push(new nlobjSearchFilter('custrecord_ebiz_trailer_no', null, 'is', trailerName));}

//		wavefilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3','4']));

//		if(vfulfillmentordno !=null && vfulfillmentordno !=""){
//		wavefilters.push(new nlobjSearchFilter('name', null, 'is', vfulfillmentordno));	}	

//		if(vWaveno !=null && vWaveno !=""){
//		wavefilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', vWaveno));	}	

//		var wavecolumns = new Array();
//		wavecolumns[0] = new nlobjSearchColumn('custrecord_ebiz_wave_no');		
//		wavecolumns[1] = new nlobjSearchColumn('custrecord_ebizfootprint');
//		wavecolumns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
//		wavecolumns[3] = new nlobjSearchColumn('custrecord_tasktype');	 
//		searchresultsWaveNo = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, wavefilters, wavecolumns);


//		for (var i = 0; searchresultsWaveNo != null && i < searchresultsWaveNo.length; i++) 
//		{
//		//nlapiLogExecution('ERROR', 'searchresultsWaveNo.length',searchresultsWaveNo.length);
//		waveno[i] = searchresultsWaveNo[i].getValue('custrecord_ebiz_wave_no');
//		}
//		var distinctaveno=removeDuplicateElement(waveno);
//		for (var i = 0; distinctaveno != null && i < distinctaveno.length; i++) 
//		{ 				
		nlapiLogExecution('ERROR','vWaveno',vWaveno);
		var noofordfilters = new Array();
//		if(distinctaveno[i] !=null && distinctaveno[i] !=""){
//		noofordfilters.push(new nlobjSearchFilter('custrecord_ebiz_wave', null,'equalTo', distinctaveno[i])); }

		if(vWaveno !=null && vWaveno !=""){
			noofordfilters.push(new nlobjSearchFilter('custrecord_ebiz_wave', null,'equalTo', vWaveno)); }
		noofordfilters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null,'anyof', ['8','10','11','14'])); 
		if(shipmentno !=null && shipmentno !=""){
			noofordfilters.push(new nlobjSearchFilter('custrecord_shipment_no', null, 'is', shipmentno));	}

		var noofordcolumns = new Array();
		noofordcolumns.push(new nlobjSearchColumn('custrecord_pickqty'));			 
		noofordcolumns.push(new nlobjSearchColumn('custrecord_ebiz_linesku'));				 
		noofordcolumns.push(new nlobjSearchColumn('custrecord_linepackcode'));
		noofordcolumns.push(new nlobjSearchColumn('custrecord_ebiz_shipdate'));
		noofordcolumns.push(new nlobjSearchColumn('name'));			 
		noofordcolumns.push(new nlobjSearchColumn('custrecord_ordline').setSort());
		noofordcolumns.push(new nlobjSearchColumn('custrecord_ns_ord').setSort());

		var searchresultsordline = nlapiSearchRecord('customrecord_ebiznet_ordline', null, noofordfilters, noofordcolumns);		
		for (var n = 0; searchresultsordline != null && n < searchresultsordline.length; n++) 
		{
			soid[n] = searchresultsordline[n].getValue('name');
			packcode[n] = searchresultsordline[n].getValue('custrecord_linepackcode');
			itemarray[n] = searchresultsordline[n].getValue('custrecord_ebiz_linesku');
		}

		var distinctSOno=removeDuplicateElement(soid);
		nlapiLogExecution('ERROR','distinctSOno',distinctSOno);
		var distinctItems = removeDuplicateElement(itemarray);

		var allitemseachQuantity =  getItemQuantityFromItemDimsAll(distinctItems);

		var getallpalletcountRes=getallpalletcount(distinctSOno);
		var getallBOLRes=getallBOL(distinctSOno);

		var getallSOAddressRes=getallSOAddress(distinctSOno);

		var NoOfOrdersPerPage = 5;
		var masterbolsplit='N';
		var headertbcont = parseFloat(distinctSOno.length)/NoOfOrdersPerPage;

		if(headertbcont== null || headertbcont == "")
			headertbcont=0;

		nlapiLogExecution('ERROR','headertbcont',Math.ceil(headertbcont));
		var strxml ="";
		// open task query by wave# 

		for (var i = 0; distinctSOno != null && i < distinctSOno.length; i++) 
		{		
			totalitemeachQuantity=0;
//			var palletfilters = new Array();				
//			palletfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));				
//			palletfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', distinctSOno[i]));
//			palletfilters.push(new nlobjSearchFilter('custrecord_ebizfootprint', null, 'isnotempty'));	

//			var palletcolumns = new Array();
//			palletcolumns[0] = new nlobjSearchColumn('custrecord_ebizfootprint',null,'group');
//			var searchresultspallet = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, palletfilters, palletcolumns);
//			if(searchresultspallet !="" && searchresultspallet!=null){
//			noofpallets=searchresultspallet[0].getValue('custrecord_ebizfootprint',null,'group');
//			}
//			if(noofpallets ==null || noofpallets =="")
//			noofpallets=0;
			var Individiualnoofpallets = getIndividualPalletQty(getallpalletcountRes,distinctSOno[i]);
			if(Individiualnoofpallets !=null && Individiualnoofpallets.length > 0){
				noofpallets=Individiualnoofpallets[0][0];
			}
			if(noofpallets ==null || noofpallets =="")
				noofpallets=0;
			totproductweightfre=0;globalproductweight=0;
			for (var n = 0; searchresultsordline != null && n < searchresultsordline.length; n++ ) 
			{
				if(searchresultsordline[n].getValue('name') == distinctSOno[i])
				{
					itemId = searchresultsordline[n].getValue('custrecord_ebiz_linesku');
					packCode = searchresultsordline[n].getText('custrecord_linepackcode');						 					 
					orderqty =searchresultsordline[n].getValue('custrecord_pickqty');

					itempalletQuantity =0;// getPalletQuantityFromItemDims(itemId, packCode);				 

					//itemeachQuantity = getItemQuantityFromItemDims(itemId, packCode);

					itemeachQuantity = getItemEachQuantityFromItemDims(allitemseachQuantity,itemId, packCode,1);

					if(packCode== "" ||packCode ==null || packCode ==0)
						packCode=1;		

					totalitemeachQuantity=Math.round(parseFloat(totalitemeachQuantity)+parseFloat(orderqty/packCode));

					if(vQbcarriertype == '2'){						 
						productweightfre=parseFloat(orderqty)*parseFloat(itemeachQuantity[1]);
						totproductweightfre=parseFloat(totproductweightfre) + parseFloat(productweightfre);						 
						if(prevso != distinctSOno[i]){
							tempproductweight=parseFloat(totproductweightfre)+ parseFloat(noofpallets)* 50;
						}
						prevso=distinctSOno[i];						 
						palletweight=parseFloat(noofpallets)* 50;						 
						globalproductweight=parseFloat(globalproductweight)+parseFloat(tempproductweight);
						globalitempalletQuantity=parseFloat(globalitempalletQuantity) + parseFloat(itempalletQuantity);								 
					}
					else
					{		
						nlapiLogExecution('ERROR','productweight 45',productweight);
						nlapiLogExecution('ERROR','globalproductweight 45',globalproductweight);
						productweight=parseFloat(orderqty)*parseFloat(itemeachQuantity[1]);		
						globalproductweight=parseFloat(globalproductweight)+ parseFloat(productweight);
						globalitempalletQuantity=parseFloat(globalitempalletQuantity) + parseFloat(itempalletQuantity);

					}
				}
			}

			totalglobalproductweight2 = parseFloat(totalglobalproductweight2) + parseFloat(globalproductweight);
			totalnoofpallets2 = parseFloat(totalnoofpallets2) + parseFloat(noofpallets);
			totalglobalproductweightfre2 = parseFloat(totalglobalproductweightfre2) + parseFloat(totproductweightfre);
			totalglobalpalletweight2 = parseFloat(totalglobalpalletweight2) + parseFloat(globalitempalletQuantity);
			totalpalletweight2 = parseFloat(totalpalletweight2) + parseFloat(palletweight);
			totalglobalitemeachQuantity2 = parseFloat(totalglobalitemeachQuantity2) + parseFloat(totalitemeachQuantity);
		}	
		if(distinctSOno[0] !=null && distinctSOno[0] !=""){
			var salesorderlist = nlapiLoadRecord('salesorder', distinctSOno[0]); 				
			var IndividiualSOdetails=GetIndividiualSOdetails(getallSOAddressRes,distinctSOno[0]);
			shiptoso = IndividiualSOdetails[0][0];//salesorderlist.getFieldValue('tranid');					
			shiptocompany=IndividiualSOdetails[0][1];//salesorderlist.getFieldValue('shipaddressee');

			if(shiptocompany == null)
				shiptocompany = "";

			companyid=IndividiualSOdetails[0][2];//salesorderlist.getFieldValue('custbody_nswms_company');
			if(companyid == null)
				companyid = "";

			loadnumber=IndividiualSOdetails[0][3];//salesorderlist.getFieldValue('custbody_shipment_no');
			if(loadnumber == null)
				loadnumber = "";

			if(vCarrierQb!=null&&vCarrierQb!="")
				carrier=vCarrierQb;
			else
				carrier=IndividiualSOdetails[0][4];//salesorderlist.getFieldText('shipmethod');
			if(carrier == null)
				carrier = "";		

			freightterms=IndividiualSOdetails[0][5];//salesorderlist.getFieldText('custbody_nswmsfreightterms');
			if(freightterms == "COLLECT")
				freightterms = "X";
			else
				//freightterms="__________";
				freightterms = "X";


			//companyname=salesorderlist.getFieldText('custbody_nswms_company');
			//if(companyname == null)
			//companyname = "";

			shiptocarrier=IndividiualSOdetails[0][6];//salesorderlist.getFieldText('custbody_salesorder_carrier');
			if(shiptocarrier == null)
				shiptocarrier = "";

			locationid=IndividiualSOdetails[0][7];//salesorderlist.getFieldValue('location');	
			if(locationid == null)
				locationid = "";

			nlapiLogExecution('ERROR','locationinternalid ',locationid); 


			shiptoAddress1 = IndividiualSOdetails[0][8];//salesorderlist.getFieldValue('shipaddr1');
			shiptoAddress2 = IndividiualSOdetails[0][9];//salesorderlist.getFieldValue('shipaddr2');
			if(shiptoAddress1 == null)
			{
				shiptoAddress1 ="";
			}
			if(shiptoAddress2 == null)
			{
				shiptoAddress2 ="";
			}

			shiptoTotAdd = shiptoAddress1 +"  "+ shiptoAddress2;

			shiptocity = IndividiualSOdetails[0][10];//salesorderlist.getFieldValue('shipcity');
			if(shiptocity == null)
				shiptocity = "";

			shiptostate = IndividiualSOdetails[0][11];//salesorderlist.getFieldValue('shipstate');
			if(shiptostate == null)
				shiptostate = "";

			shiptocountry = IndividiualSOdetails[0][12];//salesorderlist.getFieldValue('shipcountry');
			if(shiptocountry == null)
				shiptocountry = "";

			shiptozipcode = IndividiualSOdetails[0][13];//salesorderlist.getFieldValue('shipzip');
			if(shiptozipcode == null)
				shiptozipcode = "";

			shiptophone = IndividiualSOdetails[0][14];//salesorderlist.getFieldValue('custbody_customer_phone');
			if(shiptophone == null)
				shiptophone = "";
		}
		nlapiLogExecution('ERROR','after getting shipto',context.getRemainingUsage());
		if(locationid != null && locationid != ""){
			nlapiLogExecution('ERROR','before getting shipfrom',context.getRemainingUsage());
			var companylist = nlapiLoadRecord('location', locationid); 
			shipfromcompname = companylist.getFieldValue('name');
			if(shipfromcompname == null)
				shipfromcompname = "";

			addressee = companylist.getFieldValue('addressee');
			if(addressee == null)
				addressee = "";


			shipfromcity=companylist.getFieldValue('city');
			if(shipfromcity == null)
				shipfromcity = "";


			shipfromcountry=companylist.getFieldText('country');
			if(shipfromcountry == null)
				shipfromcountry = "";

			shipfromstate=companylist.getFieldValue('state');
			if(shipfromstate == null)
				shipfromstate = "";

			shipfromaddress1=companylist.getFieldValue('addr1');
			shipfromaddress2=companylist.getFieldValue('addr2');

			if(shipfromaddress1 == null)
			{
				shipfromaddress1 ="";
			}
			if(shipfromaddress2 == null)
			{
				shipfromaddress2 ="";
			}

			shipadd = shipfromaddress1 +"  "+ shipfromaddress2;

			shipfromphone=companylist.getFieldValue('addrphone');
			if(shipfromphone == null)
				shipfromphone = "";

			shipfromzipcode=companylist.getFieldValue('zip');
			if(shipfromzipcode == null)
				shipfromzipcode = "";
		}
		nlapiLogExecution('ERROR', 'shipmentno', shipmentno);
		nlapiLogExecution('ERROR', 'vCarrierQbId', vCarrierQbId);
		//scac code
		if(shipmentno == "" || shipmentno ==null){
			if(vCarrierQbId !=null && vCarrierQbId !=""){
				var shipCarrierOptions = salesorderlist.getField('shipmethod').getSelectOptions(null, null);			
				//var shipCarrierOptions = IndividiualSOdetails[0][4].getSelectOptions(null, null);			
				//nlapiLogExecution('ERROR', 'shipoptionlen', shipCarrierOptions.length);
				if(vCarrierQbId!=null&&vCarrierQbId!="")
					var shipactval=vCarrierQbId;
				else
					var shipactval = salesorderlist.getFieldValue('shipmethod'); 
				//var shipactval = IndividiualSOdetails[0][4];
				for(var i=0; i<shipCarrierOptions.length; i++)
				{
					var shipItemInternalId = shipCarrierOptions[i].getId();
					var shipItemText = shipCarrierOptions[i].getText();
					var shipItemCode;
					nlapiLogExecution('ERROR','shipItemInternalId == shipactval',shipItemInternalId+'/'+shipactval);
					if ( shipItemInternalId == shipactval)
					{
						var res = nlapiSearchGlobal(shipItemText);
						nlapiLogExecution('ERROR', 'shipItemText', shipItemText);
						if(res != null && res !='')
							nlapiLogExecution('ERROR', 'res[0].getValue(type)', res[0].getValue('type'));
						if(res != null &&  res[0].getValue('type') == 'Shipping Cost Item')
						{
							nlapiLogExecution('ERROR', 'result len', res.length);
							shipItemCode = res[0].getValue('info1');
							i=shipCarrierOptions.length;
						}
					}
				}
			}
			else
			{
				try
				{
					//shipItemCode=IndividiualSOdetails[0][4];
					var shipItemText=IndividiualSOdetails[0][4];
					nlapiLogExecution('ERROR', 'shipItemText', shipItemText);
					var res = nlapiSearchGlobal(shipItemText);
					nlapiLogExecution('ERROR', 'shipItemText', shipItemText);
					if(res != null && res !='')
					{
						nlapiLogExecution('ERROR', 'result len', res.length);
						for ( var x = 0; x < res.length; x++)
						{
							nlapiLogExecution('ERROR', 'res[x].getValue(type)', res[x].getValue('type'));
							if(res[x].getValue('type')=='Shipping Cost Item')
							{
								shipItemCode = res[x].getValue('info1');
							}
						}
					}
				}
				catch(exp)
				{
					nlapiLogExecution('ERROR','excpception in ship carrier',exp);
				}

			}
			//shipItemCode=IndividiualSOdetails[0][4];
			if(shipItemCode==""||shipItemCode==null)
				shipItemCode="";
		}
		else
		{
			shipItemCode=shipmentno;
		}

		nlapiLogExecution('ERROR','after getting scac code',context.getRemainingUsage());	

		//var carriernameload = nlapiLoadRecord('shipmethod', vCarrierQbId);
//		var carriernameload=nlapiSearchGlobal(carrier);
//		nlapiLogExecution('ERROR','carriernameload',carriernameload);
//		if(carriernameload!=null && carriernameload!='')
//		{nlapiLogExecution('ERROR','into carriernameload','done');
//		for(i=0;i<carriernameload.length;i++)
//		{
//		if(carriernameload[i].getValue('type') == 'Shipping Cost Item')
//		{			
//		shipItemCode = carriernameload[i].getValue('info1');
//		break;
//		}
//		}
//		}



//		var bolfilters = new Array();		
//		bolfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['14']));
//		if(distinctSOno[0] !=null && distinctSOno[0] !=""){
//		bolfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', distinctSOno[0]));	}	

//		var bolcolumns = new Array();
//		bolcolumns[0] = new nlobjSearchColumn('custrecord_bol');	 
//		bolcolumns[1] = new nlobjSearchColumn('custrecord_pro');	 
//		searchresultsbol = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, bolfilters, bolcolumns);
//		if( searchresultsbol != null &&  searchresultsbol != "")
//		{
//		var bollable=searchresultsbol[0].getValue('custrecord_bol');
//		var proNum=searchresultsbol[0].getValue('custrecord_pro');
//		}

		var IndividiualbolRes=GetIndividiualBOL(getallBOLRes,distinctSOno[0]);
		if(IndividiualbolRes !=null && IndividiualbolRes.length > 0){
			var bollable=IndividiualbolRes[0][0];
			var proNum=IndividiualbolRes[0][1];
//			var vtrailerno=IndividiualbolRes[0][3];
		}
		//code added on 26th aug by suman
		var trailerRecRes=getTrailerName(vWaveno);
		var trailerID=trailerRecRes[0];
		var vtrailerno=trailerRecRes[1];
		//end of code as of 26th aug.
		if(bollable == null || bollable == "")
			bollable="";
		if(proNum == null || proNum == "")
			proNum="";
		nlapiLogExecution('ERROR', 'bollable',bollable);
		nlapiLogExecution('ERROR', 'proNum',proNum);

		var temp=0;
		for (var j = 0; j < Math.ceil(headertbcont); j++) 
		{
			temp++;
			strxml +="<table  align='center' border='1' cellpadding='0' width='100%'><tr><td width='100%' > ";
			strxml +="<table align='center' id='Table14' border='1'  cellpadding='0' width='100%' >		                           ";
			strxml +="<tr>													   ";
			strxml +="<td width='100%'>											   ";
			strxml +="<table  width='100%' cellpadding='0' border='1' style='border: thin solid #000000;border-color: #000000;'>		   ";
			strxml +="<tr height='15'>													   ";
			strxml +="<td style='border: thin solid #000000;border-color: #000000;' colspan='4'>							   ";
			strxml +="<table style='border-style: none; border-color: #000000;' id='Table6' ";
			strxml +="width='100%' cellpadding='0' border='1'>								   ";
			strxml +="<tr>													   ";
			strxml +="<td align='center' style='font-weight:bold;'>											   ";
			strxml +="Date: &nbsp;&nbsp;&nbsp;&nbsp;"+sysdate;
			strxml +="</td>													   ";
			strxml +="<td align='center' style='font-weight:bold;'>											   ";
			strxml +="MASTER BILL OF LADING											   ";
			strxml +="</td>													   ";
			strxml +="<td align='center' style='font-weight:bold;'>	";
			if(Math.ceil(headertbcont) >= 2)
				strxml +="PAGE "+ parseFloat(j+1) +" OF " + Math.ceil(headertbcont);
			else
				strxml +="PAGE 1 OF 1";
			strxml +="</td>	</tr>	</table></td></tr>												   ";	
			strxml +="<tr>													   ";
			strxml +="<td style='border-color: #000000;' width='100%' colspan='2'>							   ";
			strxml +="<table style='border-style: none; border-color: #000000;' width='100%'>				   ";
			strxml +="<tr>													   ";
			strxml +="<td style='border-style: none; border-color: #000000;'>						   ";
			strxml +="<table style='border-style: none; border-color: #000000;' id='Table3' width='100%' ";
			strxml +=" cellpadding='0' border='1'>								   ";
			strxml +="<tr  style=\"background-color:black;color:white\">													   ";
			strxml +="<td align='center' style='font-weight:bold;border-color: #000000; border-right-style: solid; border-left-style: solid;	   ";
			strxml +="border-right-width: 1px; border-left-width: 1px; color: #FFFFFF;' >			   ";
			strxml +="SHIP FROM												   ";
			strxml +="</td>													   ";
			strxml +="</tr>													   ";
			strxml +="<tr height='50'>													   ";
			strxml +="<td>													   ";
			strxml +="<table style='border-style: none; border-color: #000000;' id='Table5' width='80%'	";
			strxml +=" cellpadding='0' border='1'>								   ";
			strxml +="<tr>													   ";
			strxml +="<td align='left'>											   ";
			strxml +="<b>Name :</b>													   ";
			strxml +="</td>													   ";
			strxml +="<td align='left' colspan='2'>											   ";
			//strxml +=shipfromcompname											   ;
			strxml +=addressee  ;
			strxml +="</td>													   ";
			strxml +="</tr>													   ";
			strxml +="<tr>													   ";
			strxml +="<td align='left'>											   ";
			strxml +="<b>Address :</b>												   ";
			strxml +="</td>													   ";
			strxml +="<td align='left' colspan='2'>											   ";
			strxml +=shipfromaddress1+ " "+ shipfromaddress2;
			strxml +="</td>													   ";
			strxml +="</tr>													   ";
			strxml +="<tr>													   ";
			strxml +="<td align='left' width='20%'>											   ";
			strxml +="<b>City / State / ZIP :</b>											   ";
			strxml +="</td>													   ";
			strxml +="<td align='left' width='30%' colspan='2'>											   ";
			strxml +=shipfromcity + " "+ shipfromstate + " "+ shipfromzipcode;
			strxml +="</td>													   ";
			strxml +="</tr>													   ";
			strxml +="<tr>													   ";
			strxml +="<td align='left'>											   ";
			strxml +="<b>SID # :	</b></td><td>												   ";
			strxml +=IndividiualSOdetails[0][19];
			strxml +="</td>													   ";
			strxml +="<td align='right'>											   ";
			strxml +="<b>FOB ( )</b>";
			strxml +="</td>													   ";
			strxml +="</tr>													   ";
			strxml +="</table>												   ";
			strxml +="</td>													   ";
			strxml +="</tr>													   ";
			strxml +="<tr  style=\"background-color:black;color:white\">													   ";
			strxml +="<td align='center' style='font-weight:bold;border-color: #000000; border-right-style: solid; border-left-style: solid;	   ";
			strxml +="border-right-width: 1px; border-left-width: 1px; color: #FFFFFF;' >			   ";
			strxml +="SHIP TO												   ";
			strxml +="</td>													   ";
			strxml +="</tr>													   ";
			strxml +="<tr height='50'>													   ";
			strxml +="<td>													   ";
			strxml +="<table style='border-style: none; border-color: #000000;' id='Table8' width='80%' ";
			strxml +=" cellpadding='0' border='1'>								   ";
			strxml +="<tr>													   ";
			strxml +="<td align='left'>											   ";
			strxml +="<b>Name :</b>													   ";
			strxml +="</td>													   ";
			strxml +="<td align='left' colspan='2'>											   ";
			strxml +=shiptocompany;
			strxml +="</td>													   ";
			strxml +="</tr>													   ";
			strxml +="<tr>													   ";
			strxml +="<td align='left'>											   ";
			strxml +="<b>Address :</b>												   ";
			strxml +="</td>													   ";
			strxml +="<td align='left' colspan='2'>											   ";
			strxml +=shiptoTotAdd;
			strxml +="</td>													   ";
			strxml +="</tr>													   ";
			strxml +="<tr>													   ";
			strxml +="<td align='left' width='20%'>											   ";
			strxml +="<b>City / State / ZIP :</b>											   ";
			strxml +="</td>													   ";
			strxml +="<td align='left' width='30%' colspan='2'>											   ";
			strxml +=shiptocity + " "+ shiptostate + " "+ shiptozipcode          ;
			strxml +="</td>													   ";
			strxml +="</tr>													   ";
			strxml +="<tr>													   ";
			strxml +="<td align='left'>											   ";
			strxml +="<b>CID # :	</b></td><td>												   ";
			strxml +=IndividiualSOdetails[0][20];
			strxml +="</td>													   ";
			strxml +="<td align='right'>											   ";
			strxml +="<b>FOB ( )</b>											   ";
			strxml +="</td>	</tr>	</table></td></tr>													   ";	
			strxml +="<tr  style=\"background-color:black;color:white\">													   ";
			strxml +="<td align='center' style='border-color: #000000; border-right-style: solid; border-left-style: solid;	   ";
			strxml +="border-right-width: 1px; border-left-width: 1px; color: #FFFFFF;'>			   ";
			strxml +="THIRD PARTY FREIGHT CHARGE BILL TO									   ";
			strxml +="</td>													   ";
			strxml +="</tr>													   ";
			strxml +="<tr height='35'>											   ";
			strxml +="<td align='left'>											   ";
			strxml +="<table style='border-style: none; border-color: #000000;' id='Table9' width='100%' ";
			strxml +=" cellpadding='0' border='1'>								   ";
			strxml +="<tr><td>&nbsp;</td></tr>	</table></td>	</tr>											   ";	

			strxml +="</table></td>	</tr></table></td>";
			strxml +="<td style='border-style: none; border-color: #000000;' valign='top' width='100%' colspan='2'>			   ";
			strxml +="<table width='100%' style='border-style: none; border-color: #000000;'>				   ";
			strxml +="<tr>													   ";
			strxml +="<td style='border-style: none; border-color: #000000;' width='100%'>						   ";
			strxml +="<table style='border-style: none; border-color: #000000;' id='Table1' width='100%' ";
			strxml +=" cellpadding='0' border='1'>								   ";				
			strxml +="<tr height='45'>											   ";
			strxml +="<td width='100%' align='left' valign='top' style='border: thin solid #000000;border-color: #000000;' >						   ";
			strxml +="<table style='border-style: none; border-color: #000000;' id='Table4' width='100%' ";
			strxml +=" cellpadding='0' border='1'>								   ";
			strxml +="<tr>													   ";
			strxml +="<td align='center' width='100%' style='font-weight:bold;'>											   ";
			strxml +="Bill Of Lading Number : " + bollable ;
			strxml +="</td>	</tr>";
			strxml +="<tr>        <td align='center'>";
			if(bollable != null && bollable != "")
			{
				strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
				strxml += bollable;
				strxml += "\"/>";
			}
			strxml +="</td>    </tr>";
			strxml +="</table></td>	</tr>													   ";		
			strxml +="<tr height='45'>													   ";
			strxml +="<td align='left' valign='middle' style='border: thin solid #000000;border-color: #000000;'>							   ";
			strxml +="<table style='border-style: none; border-color: #000000;' id='Table7' width='100%' ";
			strxml +=" cellpadding='0' border='1'>								   ";
			strxml +="<tr>													   ";
			strxml +="<td align='left'>											   ";
			strxml +="<b>Carrier Name :		</b>										   ";
			strxml +="</td>													   ";
			strxml +="<td align='left'>											   ";
			strxml +=shipItemCode;//carrier;
			strxml +="</td>													   ";
			strxml +="</tr>													   ";
			strxml +="<tr>													   ";
			strxml +="<td align='left'>											   ";
			strxml +="<b>Trailer Number :</b>											   ";
			strxml +="</td>													   ";
			strxml +="<td align='left'>											   ";
			strxml +=vtrailerno;
			strxml +="</td>													   ";
			strxml +="</tr>													   ";
			strxml +="<tr>													   ";
			strxml +="<td align='left'>											   ";
			strxml +="<b>Seal Number(s) :</b>											   ";
			strxml +="</td>													   ";
			strxml +="<td align='left'>											   ";
			strxml +=sealno;
			strxml +="</td>	</tr></table></td>	</tr>												   ";	
			strxml +="<tr height='45'>													   ";
			strxml +="<td align='left' valign='middle' style='border: thin solid #000000;border-color: #000000;'>							   ";
			strxml +="<table style='border-style: none; border-color: #000000;' id='Table11' width='100%' ";
			strxml +=" cellpadding='0' border='1'>								   ";
			strxml +="<tr>													   ";
			strxml +="<td align='left'>											   ";
			strxml +="<b>SCAC :</b>													   ";
			strxml +="</td>													   ";
			strxml +="<td align='left'>											   ";
			strxml +=carrier;//shipItemCode;
			strxml +="</td>													   ";
			strxml +="</tr>													   ";
			strxml +="<tr>													   ";
			strxml +="<td align='left'>											   ";
			strxml +="<b>Pro Number :</b>												   ";
			strxml +="</td>													   ";
			strxml +="<td align='left'>											   ";
			strxml +="";//proNum;
			strxml +="</td>													   ";
			strxml +="</tr>													   ";
			strxml +="<tr>													   ";
			strxml +="<td align='left'>											   ";
			strxml +="<b>Load Number :</b>												   ";
			strxml +="</td>													   ";
			strxml +="<td align='left'>											   ";
			strxml +=trailerID												   ;
			strxml +="</td>	</tr></table></td>	</tr>												   ";	
			strxml +="<tr height='35'>											   ";
			strxml +="<td align='left' style='border: thin solid #000000;border-color: #000000;' valign='top'>						   ";
			strxml +="<table style='border-style: none; border-color: #000000;' id='Table12' width='100%' ";
			strxml +=" cellpadding='0' border='1'>								   ";
			strxml +="<tr>													   ";
			strxml +="<td colspan='3'>											   ";
			strxml +="<b>Freight Charge Terms : (Freight Charges are prepaid unless marked otherwise)</b>			   ";
			strxml +="</td>	</tr>													   ";	
			strxml +="<tr>	<td>&nbsp;</td></tr>												   ";	
			strxml +="<tr>													   ";
			strxml +="<td style='padding-right: 60px; border-color: #000000; padding-bottom: 5px'>				   ";
			strxml +="<b>Prepaid :__________	</b>										   ";
			strxml +="</td>													   ";
			strxml +="<td style='padding-right: 60px;  padding-bottom: 5px'>				   ";
//			strxml +="<b>Collect:</b> <u> "+freightterms+"</u>											   ";
			strxml +="<b>Collect:</b> 											   ";
			strxml +="</td>													   ";
			strxml +="<td style='padding-right: 60px; border-color: #000000; padding-bottom: 5px'>				   ";
			strxml +="<b>3rd Party:__________</b>											   ";
			strxml +="</td>	</tr></table></td>	</tr>													   ";	
			strxml +="</table>	</td>	</tr></table>	</td>	</tr>  ";
			strxml +="<tr  style=\"background-color:black;color:white\" height='15'>													   ";
			strxml +="<td colspan='4' valign='middle' align='center' style='font-weight:bold;border-color: #000000; border-right-style: solid;		   ";
			strxml +="border-left-style: solid; border-right-width: 1px; border-left-width: 1px; color: #FFFFFF;'>		   ";
			//strxml +="bgcolor='Black'>											   ";
			strxml +="CUSTOMER ORDER INFORMATION												   ";
			strxml +="</td>													   ";
			strxml +="</tr>													   ";
			strxml +="<tr>													   ";
			strxml +="<td align='center' colspan='4' style='border-style: none; border-width: 0px; border-color: #000000'>	   ";

			strxml +="<table style='border-style: none; border-color: #000000;' id='Table19' ";
			strxml +="width='100%' cellpadding='0' border='1'>								   ";
			strxml +="<tr>													   ";
			strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center'>							   ";
			strxml +="CUSTOMER <br />ORDER NUMBER											   ";
			strxml +="</td>													   ";
			strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center'>							   ";
			strxml +="#PALLETS												   ";
			strxml +="</td>													   ";
			strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center' >						   ";
			strxml +="PRODUCT<br />WEIGHT											   ";
			strxml +="</td>													   ";
			strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center' >						   ";
			strxml +="PALLET <br />WEIGHT												   ";
			strxml +="</td>													   ";
			strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center'>							   ";
			strxml +="TOTAL <br />WEIGHT												   ";
			strxml +="</td>													   ";
			strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center' >						   ";
			strxml +="PALLET/SLIP												   ";
			strxml +="</td>													   ";
			strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center'>							   ";
			strxml +="MUST DELIVER<br /> BY DATE											   ";
			strxml +="</td>													   ";
			strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center'>							   ";
			strxml +="ORDER NUMBER												   ";
			strxml +="</td>	</tr>																   ";		 


			//
			//var distinctpackcodeno=removeDuplicateElement(packcode);
			//nlapiLogExecution('ERROR', "distinctpackcodeno" , distinctpackcodeno.length);

			//new code added for global BOL results




			//ending of new code


			var TableCnt=0;
			var count=0;
			totalnoofpallets=0;var prevso='';totalpalletweight=0;
			totalglobalproductweight=0;var totalglobalproductweightfre=0;var totproductweightfre=0;
			for (var i = parseFloat(j)*parseFloat(NoOfOrdersPerPage); distinctSOno != null && i < parseFloat(j+1)*parseFloat(NoOfOrdersPerPage) && i < distinctSOno.length ; i++) 
			{

				totalitemeachQuantity=0;

				if(distinctSOno[i] !=null && distinctSOno[i] !=""){
//					var salesorderRec = nlapiLoadRecord('salesorder', distinctSOno[i]);
//					var CustomerPO = salesorderRec.getFieldValue('otherrefnum');
//					customer = salesorderRec.getFieldValue('entity');
//					nlapiLogExecution('ERROR','customer',customer);
//					var ordernumber = salesorderRec.getFieldValue('tranid');
//					var mustdelshipdate = salesorderRec.getFieldValue('custbody_edi_dtm063');
//					if(mustdelshipdate ==null || mustdelshipdate =="")
//					mustdelshipdate="";
					var IndividiualSOdetails=GetIndividiualSOdetails(getallSOAddressRes,distinctSOno[i]);
					var CustomerPO = IndividiualSOdetails[0][15];
					customer = IndividiualSOdetails[0][16];
					var ordernumber = IndividiualSOdetails[0][17];
					var mustdelshipdate = IndividiualSOdetails[0][22];
					if(mustdelshipdate ==null || mustdelshipdate =="")
						mustdelshipdate="";

				}
				if(CustomerPO ==null || CustomerPO =="")
					CustomerPO="";
				count++;
//				pallet count filter
				globalproductweight=0;
				globalitempalletQuantity=0;var tempproductweight=0;
//				var palletfilters = new Array();				
//				palletfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));				
//				palletfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', distinctSOno[i]));
//				palletfilters.push(new nlobjSearchFilter('custrecord_ebizfootprint', null, 'isnotempty'));	

//				var palletcolumns = new Array();
//				palletcolumns[0] = new nlobjSearchColumn('custrecord_ebizfootprint',null,'group');
//				var searchresultspallet = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, palletfilters, palletcolumns);
//				if(searchresultspallet !="" && searchresultspallet!=null){
//				noofpallets=searchresultspallet[0].getValue('custrecord_ebizfootprint',null,'group');
//				}
//				if(noofpallets ==null || noofpallets =="")
//				noofpallets=0;

				var Individiualnoofpallets = getIndividualPalletQty(getallpalletcountRes,distinctSOno[i]);
				if(Individiualnoofpallets !=null && Individiualnoofpallets.length > 0){
					noofpallets=Individiualnoofpallets[0][0];
				}
				if(noofpallets ==null || noofpallets =="")
					noofpallets=0;
				nlapiLogExecution('ERROR','noofpallets',noofpallets);
				totproductweightfre=0;
				for (var n = 0; searchresultsordline != null && n < searchresultsordline.length; n++ ) 
				{

					if(searchresultsordline[n].getValue('name') == distinctSOno[i])
					{
						itemId = searchresultsordline[n].getValue('custrecord_ebiz_linesku');
						packCode = searchresultsordline[n].getText('custrecord_linepackcode');						 					 
						orderqty =searchresultsordline[n].getValue('custrecord_pickqty');
						nlapiLogExecution('ERROR','packCode',packCode);

						itempalletQuantity = getItemEachQuantityFromItemDims(allitemseachQuantity,itemId, packCode,3);
						itemeachQuantity = getItemEachQuantityFromItemDims(allitemseachQuantity,itemId, packCode,1);

						//itempalletQuantity = getPalletQuantityFromItemDims(itemId, packCode);				 
						//itemeachQuantity =getItemQuantityFromItemDims(itemId, packCode);

						if(packCode== "" ||packCode ==null || packCode ==0)
							packCode=1;

						nlapiLogExecution('ERROR','itemeachQuantity[0]',itemeachQuantity[0]);
						nlapiLogExecution('ERROR','itemeachweight[1]',itemeachQuantity[1]);
						nlapiLogExecution('ERROR','orderqty',orderqty);
						nlapiLogExecution('ERROR','packCode',packCode);


						//totalitemeachQuantity=parseFloat((parseFloat(totalitemeachQuantity)+parseFloat(itemeachQuantity[0]))/distinctpackcodeno.length);
						//totalitemeachQuantity=parseFloat((parseFloat(totalitemeachQuantity)+parseFloat(itemeachQuantity[0]))/packCode);
						//totalitemeachQuantity=parseFloat((parseFloat(totalitemeachQuantity)+parseFloat(itemeachQuantity[0])));
						//totalweight=parseFloat(productweight);	
						totalitemeachQuantity=Math.round(parseFloat(totalitemeachQuantity)+parseFloat(orderqty/packCode));
						nlapiLogExecution('ERROR','totalitemeachQuantity',totalitemeachQuantity);
						nlapiLogExecution('ERROR','vQbcarriertype',vQbcarriertype);
						if(vQbcarriertype == '2'){

							nlapiLogExecution('ERROR','into Carrier Type','LTL');
							nlapiLogExecution('ERROR','orderqty',orderqty);
							nlapiLogExecution('ERROR','itemeachQuantity[1]',itemeachQuantity[1]);
							productweightfre=parseFloat(orderqty)*parseFloat(itemeachQuantity[1]);
							totproductweightfre=parseFloat(totproductweightfre) + parseFloat(productweightfre);
							nlapiLogExecution('ERROR','productweightfre',productweightfre);
							nlapiLogExecution('ERROR','totproductweightfre',totproductweightfre);
							//tempproductweight=parseFloat(productweightfre)+ parseFloat(noofpallets)*parseFloat(itempalletQuantity);
							nlapiLogExecution('ERROR','n',n);
							nlapiLogExecution('ERROR','searchresultsordline.length',searchresultsordline.length);
							if(prevso != distinctSOno[i]){
								tempproductweight=parseFloat(totproductweightfre)+ parseFloat(noofpallets)* 50;
							}
							prevso=distinctSOno[i];
							nlapiLogExecution('ERROR','tempproductweight',tempproductweight);
							palletweight=parseFloat(noofpallets)* 50;
							nlapiLogExecution('ERROR','palletweight',palletweight);
							nlapiLogExecution('ERROR','globalproductweight',globalproductweight);
							globalproductweight=parseFloat(globalproductweight)+parseFloat(tempproductweight);
							nlapiLogExecution('ERROR','globalproductweight',globalproductweight);

							globalitempalletQuantity=parseFloat(globalitempalletQuantity) + parseFloat(itempalletQuantity);	
							nlapiLogExecution('ERROR','globalitempalletQuantity',globalitempalletQuantity);
						}
						else
						{
							nlapiLogExecution('ERROR','into Carrier Type','LT');								
							productweight=parseFloat(orderqty)*parseFloat(itemeachQuantity[1]);		
							globalproductweight=parseFloat(globalproductweight)+ parseFloat(productweight);
							globalitempalletQuantity=parseFloat(globalitempalletQuantity) + parseFloat(itempalletQuantity);

						}
					}
				}

				totalglobalproductweight=parseFloat(totalglobalproductweight)+ parseFloat(globalproductweight);
				totalnoofpallets=parseFloat(totalnoofpallets)+ parseFloat(noofpallets);
				totalglobalproductweightfre=parseFloat(totalglobalproductweightfre)+ parseFloat(totproductweightfre);
				totalglobalpalletweight=parseFloat(totalglobalpalletweight)+ parseFloat(globalitempalletQuantity);
				totalpalletweight=parseFloat(totalpalletweight)+ parseFloat(palletweight);
				totalglobalitemeachQuantity=parseFloat(totalglobalitemeachQuantity)+ parseFloat(totalitemeachQuantity);
				nlapiLogExecution('ERROR','noofpallets 1',noofpallets);
				strxml +="<tr height='15'>	  ";
				strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>   ";
				strxml +=CustomerPO  ;
				strxml +="</td>	   ";
				strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>   ";
				strxml +=noofpallets;
				strxml +="</td>  ";
				strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>	   ";
				if(vQbcarriertype == '2')
					strxml +=totproductweightfre.toFixed(2);

				else
					strxml +=globalproductweight.toFixed(2);
				strxml +="</td>	  ";
				strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>  ";
				if(vQbcarriertype == '2')
					strxml +=palletweight.toFixed(2);
				else
					strxml +="0";
				strxml +="</td>	   ";
				strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>	  ";
				var vTotWeightNew=0;
				if(vQbcarriertype == '2')
				{
					var vTotWeightNewTemp=totproductweightfre+palletweight;
					vTotWeightNew=vTotWeightNewTemp.toFixed(2);
				}
				else
				{
					var vTotWeightNewTemp=globalproductweight+0;
					vTotWeightNew=vTotWeightNewTemp.toFixed(2);
				}
				//strxml +=globalproductweight.toFixed(2);
				strxml +=vTotWeightNew;
				strxml +="</td>	  ";
				strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center' >  ";
				strxml +="Y ";
				strxml +="</td>	  ";
				strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>  ";
				strxml +=mustdelshipdate;
				strxml +="</td>	  ";
				strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>   ";
				strxml +=ordernumber;
				strxml +="</td>	  ";
				strxml +="</tr>  ";
				var currentRow = [i, CustomerPO, noofpallets, globalproductweight, 
				                  mustdelshipdate, ordernumber, noofpallets, globalproductweight,totalitemeachQuantity,globalitempalletQuantity,palletweight,totalpalletweight,productweightfre,totproductweightfre];

				HeaderDetails.push(currentRow);
			}			

			strxml +="<tr height='16'>	  ";
			strxml +="<td align='center' style='font-weight:bold;border: thin solid #000000;border-color: #000000;'>   ";
			//Math.ceil(headertbcont
			if(Math.ceil(headertbcont) > temp){
				strxml +="Page Total"  ;}
			else {
				strxml +="Grand Total"  ;}
			strxml +="</td>	   ";
			strxml +="<td align='center' style='border: thin solid #000000;border-color: #000000;'>   ";
			if(Math.ceil(headertbcont) > temp)
				strxml +=totalnoofpallets;
			else
				strxml +=totalnoofpallets2;
			strxml +="</td>  ";
			strxml +="<td align='center' style='border: thin solid #000000;border-color: #000000;'>	   ";
			//strxml +=totalglobalproductweight;

			if(Math.ceil(headertbcont) > temp ){				
				if(vQbcarriertype == '2')
					strxml +=totalglobalproductweightfre.toFixed(2);					
				else
					strxml +=totalglobalproductweight.toFixed(2);
			}
			else
			{
				if(vQbcarriertype == '2')
					strxml +=totalglobalproductweightfre2.toFixed(2);					
				else
					strxml +=totalglobalproductweight2.toFixed(2);
			}

			strxml +="</td>	  ";
			strxml +="<td align='center' style='border: thin solid #000000;border-color: #000000;'>  ";
			if(Math.ceil(headertbcont) > temp ){		
				if(vQbcarriertype == '2')
					strxml +=totalpalletweight.toFixed(2);
				else
					strxml +="0";
			}
			else
			{					
				if(vQbcarriertype == '2')
					strxml +=totalpalletweight2.toFixed(2);
				else
					strxml +="0";					
			}
			strxml +="</td>	   ";
			strxml +="<td align='center' style='border: thin solid #000000;border-color: #000000;'>	  ";
			var vGrndTotWeightNew=0,vGrndTotWeightNew2=0;
			if(Math.ceil(headertbcont) > temp){
				if(vQbcarriertype == '2')
					vGrndTotWeightNew=totalglobalproductweightfre+totalpalletweight;
				else
					vGrndTotWeightNew=totalglobalproductweight+0;
			}
			else {
				if(vQbcarriertype == '2')
					vGrndTotWeightNew2=totalglobalproductweightfre2+totalpalletweight2;
				else
					vGrndTotWeightNew2=totalglobalproductweight2+0;					
			}
			//strxml +=globalproductweight.toFixed(2);	
			if(Math.ceil(headertbcont) > temp )
				strxml +=vGrndTotWeightNew.toFixed(2);	
			else
				strxml +=vGrndTotWeightNew2.toFixed(2);
			//strxml +=totalglobalproductweight.toFixed(2);
			strxml +="</td>	  ";
			strxml +="<td style='border: thin solid #000000;border-color: #000000;'>  ";
			strxml +="&nbsp;";
			strxml +="</td>	  ";
			strxml +="<td style='border: thin solid #000000;border-color: #000000;'>  ";
			strxml +="&nbsp;";
			strxml +="</td>	  ";
			strxml +="<td style='border: thin solid #000000;border-color: #000000;'>   ";
			strxml +="&nbsp;";
			strxml +="</td>	  ";
			strxml +="</tr>  ";				
			strxml +="</table>";
			strxml +="</td>	</tr></table></td>	</tr>	   ";

			strxml +="<tr>													   ";
			strxml +="<td>													   ";
			strxml +="<table  align='center' cellpadding='0' style='border-color: #000000; ";
			strxml +="border-width: 1;' width='100%' border='1'>								   ";
			strxml +="<tr>	  ";
			strxml +="<td colspan='3' width='100%' style='border-style: none;'>						   ";
			strxml +="<table  cellpadding='0' style='border-color: #000000; border-style: none; ";
			strxml +="border-width: 1;' width='100%' border='1'> ";


			strxml +="<tr  style=\"background-color:black;color:white\" height='15'>													   ";
			strxml +="<td colspan='9' valign='middle' align='center' style='font-weight:bold;border-color: #000000; border-right-style: solid;		   ";
			strxml +="border-left-style: solid; border-right-width: 1px; border-left-width: 1px; color: #FFFFFF;'	>	   ";
			//strxml +="bgcolor='Black'>											   ";
			strxml +="CARRIER INFORMATION											   ";
			strxml +="</td>	</tr>												   ";		
			strxml +="<tr>													   ";
			strxml +="<td colspan='2' align='center' style='font-weight:bold;border: thin solid #000000;border-color: #000000;'>						   ";
			strxml +="HANDLING<br />UNIT												   ";
			strxml +="</td>													   ";
			strxml +="<td colspan='2' align='center' style='font-weight:bold;border: thin solid #000000;border-color: #000000;'>						   ";
			strxml +="PACKAGE												   ";
			strxml +="</td>													   ";
			strxml +="<td rowspan='2' width='5%' align='center' style='font-weight:bold;border: thin solid #000000;border-color: #000000;'>						   ";
			strxml +="WEIGHT												   ";
			strxml +="</td>";
			strxml +="<td rowspan='2' width='10%' align='center' style='font-weight:bold;border: thin solid #000000;border-color: #000000;'>";
			strxml +="H.M.(X)";
			strxml +="</td>";
			strxml +="<td rowspan='2' width='50%' align='center' style='border: thin solid #000000;border-color: #000000;'>";
			strxml +="<b>COMMODITY DESCRIPTION</b><br />";
			strxml +="<p align='left'><span style='font-size:6pt'>Commodities Requiring special or additional care or attention in handling or slowing ";
			strxml +="must be so";
			strxml +="marked as to ensure safe transportation with ordinary care";
			strxml +="see section 2 (e) of NMFC item 360</span></p>";
			strxml +="</td>";
			strxml +="<td colspan='2' align='center' style='font-weight:bold;border: thin solid #000000;border-color: #000000;'>";
			strxml +="LTL ONLY	 ";
			strxml +="</td>	";
			strxml +="</tr>	";
			strxml +="<tr>	";
			strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center'>";
			strxml +="QTY  ";
			strxml +="</td>";
			strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center'>";
			strxml +="TYPE";
			strxml +="</td>";
			strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center'>";
			strxml +="QTY";
			strxml +="</td>";
			strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center'>";
			strxml +="TYPE";
			strxml +="</td>";
			strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center'>";
			strxml +="NMFC";
			strxml +="</td>";
			strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center'>";
			strxml +="CLASS";
			strxml +="</td>";
			strxml +="</tr>";
			strxml +="<tr height='20'>";
			//container loop starts here
			strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>	 ";
			strxml +=totalnoofpallets2;//total pallets
			strxml +="</td>";
			strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>";
			strxml +="PALL";
			strxml +="</td>";
			strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>";
			strxml +=totalglobalitemeachQuantity2;
			strxml +="</td>";
			strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>";
			strxml +="EA";
			strxml +="</td>";
			strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>";			
			if(vQbcarriertype == '2')
			{
				var tempwt= totalglobalproductweightfre2+totalpalletweight2;
				strxml +=tempwt.toFixed(2);
			}
			else
				strxml +=totalglobalproductweight2.toFixed(2);	
			strxml +="</td>";
			strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>";
			strxml +="&nbsp;";
			strxml +="</td>";
			strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='left'>";
			strxml +="BICYCLE";
			strxml +="</td>";
			strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>";
			strxml +="&nbsp;";
			strxml +="</td>";
			strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>";
			strxml +="125";
			strxml +="</td>	</tr>";		
			strxml +="</table></td>	</tr>";

			strxml +="<tr >";		
			strxml +="<td colspan='3' style='border: thin solid #000000; border-color: #000000;'>";
			strxml +="<table width='100%' cellpadding='0' style='border: thin solid #000000; border-color: #000000;'>";
			nlapiLogExecution('ERROR', 'count',count);
			if(count >= NoOfOrdersPerPage ){
				strxml +="<tr height='16%'>";
				strxml +="<td valign='top' width='50%' align='center' style='border: thin solid #000000; border-color: #000000;font-size: 11pt; font-weight: bold;'>";
				strxml +="Special Instructions</br>";
				strxml +=IndividiualSOdetails[0][21];
				strxml +="</td>";
				strxml +="<td width='50%' align='center' valign='middle' style='font-size: 11pt; font-weight: bold;'>";
				strxml +="<table cellpadding='0' width='100%' border='0'>";
				strxml +="<tr>";
				strxml +="<td align='right' valign='middle' style='font-size: 10pt; font-weight: bold;'>";
				strxml +="Driver agrees with appointment, <br />arrival, ";
				strxml +="start and finish times listed, <br /> unless, ";
				strxml +="otherwise noted.";
				strxml +="</td>";
				strxml +="<td align='right' valign='middle' style='font-size: 9pt; font-weight: bold;'>";
				strxml +="Date: ______________<br /><br />";
				strxml +="Appt: ______________<br /><br />";
				strxml +="Arrival: ______________<br /><br />";
				strxml +="Start: ______________<br /><br />";
				strxml +="Finish: ______________";
				strxml +="</td>";
				strxml +="</tr>";
				strxml +="<tr><td align='center' colspan='2'><br/><br/><br/><br/>___________________________________________</td></tr>";
				strxml +="</table>";
				strxml +="</td>";
				strxml +="</tr>";
				masterbolsplit='Y';
			}				
			else if(count < NoOfOrdersPerPage && masterbolsplit == 'N')
			{
				nlapiLogExecution('ERROR', 'masterbolsplit',masterbolsplit);
				nlapiLogExecution('ERROR', 'count',count);
				nlapiLogExecution('ERROR', 'NoOfOrdersPerPage',NoOfOrdersPerPage);

				strxml +="<tr height='23%'>";
				strxml +="<td valign='top' width='50%' align='center' style='border: thin solid #000000; border-color: #000000;font-size: 11pt; font-weight: bold;'>";
				strxml +="Special Instructions<br/>";
				strxml +=IndividiualSOdetails[0][21];
				strxml +="</td>";
				strxml +="<td width='50%' align='center' valign='middle' style='font-size: 11pt; font-weight: bold;'>";
				strxml +="<table cellpadding='0' width='100%' border='0'>";
				strxml +="<tr>";
				strxml +="<td align='right' valign='middle' style='font-size: 10pt; font-weight: bold;'>";
				strxml +="Driver agrees with appointment, <br />arrival, ";
				strxml +="start and finish times listed, <br /> unless, ";
				strxml +="otherwise noted.";
				strxml +="</td>";
				strxml +="<td align='right' valign='middle' style='font-size: 9pt; font-weight: bold;'>";
				strxml +="Date: ______________<br /><br />";
				strxml +="Appt: ______________<br /><br />";
				strxml +="Arrival: ______________<br /><br />";
				strxml +="Start: ______________<br /><br />";
				strxml +="Finish: ______________";
				strxml +="</td>";
				strxml +="</tr>";
				strxml +="<tr><td align='center' colspan='2'><br/><br/><br/><br/>___________________________________________</td></tr>";
				strxml +="</table>";
				strxml +="</td>";
				strxml +="</tr>";
			}
			strxml +="</table>";
			strxml +="</td>";
			strxml +="</tr>";
//			else
//			strxml +="<tr height='23%'>";



			strxml +="<tr><td colspan='2' style='border: thin solid #000000;border-color: #000000;' valign='top'>				   ";

			strxml +="<b>&nbsp;&nbsp;NOTE:Liability Limitation for loss or damage in this shipment may be applicable.<br/>			   ";
			strxml +="&nbsp;&nbsp;See 49 U.S.C. 14706(c)(1)(A) and (B)	</b>								   ";
			strxml +="</td>													   ";
			strxml +="<td style='border: thin solid #000000;border-color: #000000;' valign='top'>							   ";
			strxml +="<b>&nbsp;&nbsp;COD Amount : $</b> _____________________<br />								   ";
			strxml +="<br />												   ";
			strxml +="&nbsp;&nbsp;Fee Terms: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Collect ( ) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Prepaid  ";
			strxml +="( )<br />												   ";
			strxml +="<br />												   ";
			strxml +="&nbsp;&nbsp;Customer Check Acceptable : ( )									   ";
			strxml +="</td>													   ";
			strxml +="</tr>													   ";

			strxml +="<tr height='45'>													   ";
			strxml +="<td colspan='2'  valign='middle' style='font-size:7pt;border: thin solid #000000;border-color: #000000;'>							   ";

			strxml +="<table  align='center' cellpadding='5' width='100%'>								   ";
			strxml +="<tr><td>	  ";

			strxml +="RECEIVED, subject to individually determined rates or contracts that have been agreed			   ";
			strxml +="upon in writing between the carrier and								   ";
			//strxml +="<br />												   ";
			strxml +="shipper, if applicable, other wise to the rates, classifications and rules that have			   ";
			strxml +="been established by the carrier and are available to						   ";
			strxml +="the shipper on request and all the terms and conditions of the NMFC Uniform Straight			   ";
			strxml +="Bill of Lading.											   ";
			strxml +="</td></tr></table></td>													   ";
			strxml +="<td style='border: thin solid #000000;border-color: #000000;'>									   ";
			strxml +="<table  align='center' cellpadding='5' width='100%'>								   ";
			strxml +="<tr><td>	  ";

			strxml +="<p><span style='font-size:6pt'>The carrier shall not take delivery of the shipment without payment of freight and all			   ";
			strxml +="other lawful charges.<br /></span></p>										   ";
			strxml +="<br />												   ";
			strxml +="________________________________ Shipper<br />							   ";
			strxml +="Signature:												   ";
			strxml +="</td>	</tr></table></td>												   ";
			strxml +="</tr>													   ";
			strxml +="<tr style='border-style: none; border-width: 0px; border-color: #000000' height='90'>				   ";
			strxml +="<td style='border: thin solid #000000;border-color: #000000;' width='55%' valign='top'>						   ";
			strxml +="<table  align='center' cellpadding='5' width='100%'>								   ";
			strxml +="<tr><td>	  ";

			strxml +="<b>SHIPPER SIGNATURE / DATE:</b><br />									   ";
			strxml +="<p align='left'><span style='font-size:7pt'>This is to certify that the above named materials are properly classified, packaged, ";
			strxml +="marked and labeled and &#47; or in proper condition for transportation, according to the";
			strxml +=" applicable regulations of the DOT.</span></p>		<br/>						   ";
			strxml +="__________________________Shipper<br />							   ";
			strxml +="Signature:												   ";
			strxml +="</td>	</tr></table></td>													   ";
			strxml +="<td width='60%' style='border: thin solid #000000;border-color: #000000;' valign='top'>						   ";

			strxml +="<table width='100%'><tr><td style='font-weight:bold;'>";
			strxml +="Trailer Loaded:</td><td style='font-weight:bold;'>";
			strxml +="Freight Counted:</td></tr><tr><td>";
			strxml +="( )By shipper</td><td>";
			strxml +="( )By shipper</td></tr><tr><td>";
			strxml +="( )By Driver</td><td>";
			strxml +="( )By Driver/Pallets said to contain</td></tr><tr><td>";
			strxml +="&nbsp;</td><td>";
			strxml +="( )By Driver/Pieces</td></tr></table>	";		
			strxml +="</td>													   ";
			strxml +="<td width='58%' align='left' style='border: thin solid #000000;border-color: #000000;' valign='top'>						   ";

			strxml +="<table  align='center' cellpadding='5' width='100%'>								   ";
			strxml +="<tr><td>	  ";

			strxml +="<b>CARRIER SIGNATURE / PICKUP DATE</b><br />									   ";
			strxml +="<p align='left'><span style='font-size:7pt'>Carrier acknowledges receipt of packages and required placards. Carrier certifies			   ";
			strxml +="emergency response information was made available and for carrier has the DOT emergency		   ";
			strxml +="response guide book or equivalent documentation in the vehicle.						   ";
			strxml +="<br /><br />	<br />											   ";
			strxml +="Property described above is received in good order, except as noted.</span></p>					   ";
			strxml +="</td></tr></table></td>	</tr></table></td>	</tr></table>	</td></tr>	</table>											   ";
		}
		//}

		//starting of child pages
		if(distinctSOno != null && distinctSOno != "")
		{	
			if(distinctSOno.length > 1)
			{
				var salesorderlist = nlapiLoadRecord('salesorder', distinctSOno[0]);
				for(var p=0;p<distinctSOno.length;p++)
				{	
					nlapiLogExecution('ERROR', 'distinctSOno.length',distinctSOno.length);
					totalordqty1=0, totalprodwght1=0, totalheaderqty1=0,productweight1=0,totalweight1=0,itemId1='',packCode1='',globalOrdqty1=0,itempalletQuantity1=0,itemcaseQuantity1=0,itemcaseQuantity1=0,globalEachqty1=0;				 
					globalOrdqty1=0,globalPalletqty1=0,globalEachqty1=0;				 
					globalproductweight1=0,totalglobalproductweight1=0,totalitemcaseQuantity1=0;

					//var childbollable=GenerateLable(companyname);			 
					var count=1+p;
					//page break
					//strxml +="<p style='page-break-after:always'></p>";


					if(distinctSOno[p] !=null && distinctSOno[p] !=""){
						//var salesorderlist = nlapiLoadRecord('salesorder', distinctSOno[p]);
						var IndividiualSOdetails=GetIndividiualSOdetails(getallSOAddressRes,distinctSOno[p]);

						shiptoso = IndividiualSOdetails[0][0];//salesorderlist.getFieldValue('tranid');			
						shiptocompany=IndividiualSOdetails[0][1];//salesorderlist.getFieldValue('shipaddressee');
						if(shiptocompany == null)
							shiptocompany = "";

						companyid=IndividiualSOdetails[0][2];//salesorderlist.getFieldValue('custbody_nswms_company');
						if(companyid == null)
							companyid = "";

						loadnumber=IndividiualSOdetails[0][3];//salesorderlist.getFieldValue('custbody_shipment_no');
						if(loadnumber == null)
							loadnumber = "";

						if(vCarrierQb!=null&&vCarrierQb!="")
							carrier=vCarrierQb;
						else
							carrier=IndividiualSOdetails[0][4];//salesorderlist.getFieldText('shipmethod');

						if(carrier == null)
							carrier = "";		

						freightterms=IndividiualSOdetails[0][5];//salesorderlist.getFieldText('custbody_nswmsfreightterms');
						if(freightterms == "COLLECT")
							freightterms = "X";
						else
							//freightterms="__________";
							freightterms = "X";

						shiptocarrier=IndividiualSOdetails[0][6];//salesorderlist.getFieldText('custbody_salesorder_carrier');
						if(shiptocarrier == null)
							shiptocarrier = "";

						locationid=IndividiualSOdetails[0][7];//salesorderlist.getFieldValue('location');	
						if(locationid == null)
							locationid = "";

						nlapiLogExecution('ERROR','locationinternalid ',locationid); 


						shiptoAddress1 = IndividiualSOdetails[0][8];//salesorderlist.getFieldValue('shipaddr1');
						shiptoAddress2 = IndividiualSOdetails[0][9];//salesorderlist.getFieldValue('shipaddr2');
						if(shiptoAddress1 == null)
						{
							shiptoAddress1 ="";
						}
						if(shiptoAddress2 == null)
						{
							shiptoAddress2 ="";
						}

						shiptoTotAdd = shiptoAddress1 +"  "+ shiptoAddress2;

						shiptocity = IndividiualSOdetails[0][10];//salesorderlist.getFieldValue('shipcity');
						if(shiptocity == null)
							shiptocity = "";

						shiptostate = IndividiualSOdetails[0][11];//salesorderlist.getFieldValue('shipstate');
						if(shiptostate == null)
							shiptostate = "";

						shiptocountry = IndividiualSOdetails[0][12];//salesorderlist.getFieldValue('shipcountry');
						if(shiptocountry == null)
							shiptocountry = "";

						shiptozipcode = IndividiualSOdetails[0][13];//salesorderlist.getFieldValue('shipzip');
						if(shiptozipcode == null)
							shiptozipcode = "";

						shiptophone = IndividiualSOdetails[0][14];//salesorderlist.getFieldValue('custbody_customer_phone');
						if(shiptophone == null)
							shiptophone = "";
					}
					nlapiLogExecution('ERROR','after getting shipto',context.getRemainingUsage());
//					if(locationid != null && locationid != ""){
//					nlapiLogExecution('ERROR','before getting shipfrom',context.getRemainingUsage());
//					var companylist = nlapiLoadRecord('location', locationid); 
//					shipfromcompname = companylist.getFieldValue('name');
//					if(shipfromcompname == null)
//					shipfromcompname = "";

//					addressee = companylist.getFieldValue('addressee');
//					if(addressee == null)
//					addressee = "";


//					shipfromcity=companylist.getFieldValue('city');
//					if(shipfromcity == null)
//					shipfromcity = "";


//					shipfromcountry=companylist.getFieldText('country');
//					if(shipfromcountry == null)
//					shipfromcountry = "";

//					shipfromstate=companylist.getFieldValue('state');
//					if(shipfromstate == null)
//					shipfromstate = "";

//					shipfromaddress1=companylist.getFieldValue('addr1');
//					shipfromaddress2=companylist.getFieldValue('addr2');

//					if(shipfromaddress1 == null)
//					{
//					shipfromaddress1 ="";
//					}
//					if(shipfromaddress2 == null)
//					{
//					shipfromaddress2 ="";
//					}

//					shipadd = shipfromaddress1 +"  "+ shipfromaddress2;

//					shipfromphone=companylist.getFieldValue('addrphone');
//					if(shipfromphone == null)
//					shipfromphone = "";

//					shipfromzipcode=companylist.getFieldValue('zip');
//					if(shipfromzipcode == null)
//					shipfromzipcode = "";
//					}

					//scac code
					if(vCarrierQbId !=null && vCarrierQbId !=""){

						var shipCarrierOptions = salesorderlist.getField('shipmethod').getSelectOptions(null, null);
						//nlapiLogExecution('ERROR', 'shipoptionlen', shipCarrierOptions.length);
						if(vCarrierQbId!=null&&vCarrierQbId!="")
							var shipactval=vCarrierQbId;
						else
							//var shipactval = IndividiualSOdetails[0][4];//salesorderlist.getFieldValue('shipmethod'); 
							var shipactval = salesorderlist.getFieldValue('shipmethod');

						for(var i=0; i<shipCarrierOptions.length; i++)
						{
							var shipItemInternalId = shipCarrierOptions[i].getId();
							var shipItemText = shipCarrierOptions[i].getText();
							var shipItemCode;

							if ( shipItemInternalId == shipactval)
							{
								var res = nlapiSearchGlobal(shipItemText);
								nlapiLogExecution('ERROR', 'shipItemText', shipItemText);
								if(res !=null && res !='')
									nlapiLogExecution('ERROR', 'res[0].getValue(type)', res[0].getValue('type'));
								if(res != null &&  res[0].getValue('type') == 'Shipping Cost Item')
								{
									nlapiLogExecution('ERROR', 'result len', res.length);
									shipItemCode = res[0].getValue('info1');
									i=shipCarrierOptions.length;
								}
							}
						}
					}
					else
					{
//						shipItemCode=IndividiualSOdetails[0][4];
						try
						{
							var shipItemText=IndividiualSOdetails[0][4];
							nlapiLogExecution('ERROR', 'shipItemText', shipItemText);
							var res = nlapiSearchGlobal(shipItemText);
							nlapiLogExecution('ERROR', 'shipItemText', shipItemText);
							if(res != null && res !='')
							{
								nlapiLogExecution('ERROR', 'result len', res.length);
								for ( var x = 0; x < res.length; x++)
								{
									nlapiLogExecution('ERROR', 'res[x].getValue(type)', res[x].getValue('type'));
									if(res[x].getValue('type')=='Shipping Cost Item')
									{
										shipItemCode = res[x].getValue('info1');
									}
								}
							}
						}
						catch(exp)
						{
							nlapiLogExecution('ERROR','excpception in ship carrier',exp);
						}
					}

					//shipItemCode=IndividiualSOdetails[0][4];
					if(shipItemCode==""||shipItemCode==null)
						shipItemCode="";
					nlapiLogExecution('ERROR','after getting scac code',context.getRemainingUsage());		

//					var bolfilters = new Array();		
//					bolfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['14']));
//					if(distinctSOno[p] !=null && distinctSOno[p] !=""){
//					bolfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', distinctSOno[p]));	}	

//					var bolcolumns = new Array();
//					bolcolumns[0] = new nlobjSearchColumn('custrecord_sub_bol');	 
//					bolcolumns[1] = new nlobjSearchColumn('custrecord_pro');	 
//					searchresultsbol = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, bolfilters, bolcolumns);
//					if( searchresultsbol != null &&  searchresultsbol != "")
//					{
//					childbollable=searchresultsbol[0].getValue('custrecord_sub_bol');
//					var proNum=searchresultsbol[0].getValue('custrecord_pro');
//					}
					var IndividiualbolRes=GetIndividiualBOL(getallBOLRes,distinctSOno[p]);
					if(IndividiualbolRes !=null && IndividiualbolRes.length > 0){
						childbollable=IndividiualbolRes[0][0];
						var proNum=IndividiualbolRes[0][1];
					}
					if(childbollable == null || childbollable == "")
						childbollable="";
					if(proNum == null || proNum == "")
						proNum="";
					nlapiLogExecution('ERROR', 'bollable',bollable);
					nlapiLogExecution('ERROR', 'proNum',proNum);

					strxml += "<table align='center' border='1' cellpadding='0' width='100%'><tr><td width='100%' >";
					strxml +="<table align='center' id='Table14' border='1'  cellpadding='0' width='100%' >		                           ";
					strxml +="<tr>													   ";
					strxml +="<td width='100%'>											   ";
					strxml +="<table  width='100%' cellpadding='0' border='1' style='border: thin solid #000000;border-color: #000000;'>		   ";
					strxml +="<tr>													   ";
					strxml +="<td style='border: thin solid #000000;border-color: #000000;' colspan='4'>							   ";
					strxml +="<table style='border-style: none; border-color: #000000;' id='Table6' ";
					strxml +="width='100%' cellpadding='0' border='1'>								   ";
					strxml +="<tr height='20'>													   ";
					strxml +="<td align='center' style='font-weight:bold;'>											   ";
					strxml +="Date: &nbsp;&nbsp;&nbsp;&nbsp;"+sysdate;
					strxml +="</td>													   ";
					strxml +="<td align='center' style='font-weight:bold;'>											   ";
					strxml +="BILL OF LADING											   ";
					strxml +="</td>													   ";
					strxml +="<td align='center' style='font-weight:bold;'>											   ";
					strxml +="PAGE 1 OF 1";//"+count;
					strxml +="</td>	</tr>	</table>												   ";	
					strxml +="</td></tr>";
					strxml +="<tr>													   ";
					strxml +="<td style='border-color: #000000;' width='100%' colspan='2'>							   ";
					strxml +="<table style='border-style: none; border-color: #000000;' width='100%'>				   ";
					strxml +="<tr>													   ";
					strxml +="<td style='border-style: none; border-color: #000000;'>						   ";
					strxml +="<table style='border-style: none; border-color: #000000;' id='Table3' width='100%' ";
					strxml +=" cellpadding='0' border='1'>								   ";
					strxml +="<tr  style=\"background-color:black;color:white\">													   ";
					strxml +="<td valign='middle' align='center' style='font-weight:bold;font-weight:bold;border-color: #000000; border-right-style: solid; border-left-style: solid;	   ";
					strxml +="border-right-width: 1px; border-left-width: 1px; color: #FFFFFF;' >			   ";
					strxml +="SHIP FROM												   ";
					strxml +="</td>													   ";
					strxml +="</tr>													   ";
					strxml +="<tr height='55'>													   ";
					strxml +="<td valign='middle'>													   ";
					strxml +="<table style='border-style: none; border-color: #000000;' id='Table5' width='80%'	";
					strxml +=" cellpadding='0' border='1'>								   ";
					strxml +="<tr>													   ";
					strxml +="<td align='left'>											   ";
					strxml +="<b>Name :</b>													   ";
					strxml +="</td>													   ";
					strxml +="<td align='left' colspan='2'>											   ";
					//strxml +=shipfromcompname											   ;
					strxml +=addressee  ;
					strxml +="</td>													   ";
					strxml +="</tr>													   ";
					strxml +="<tr>													   ";
					strxml +="<td align='left'>											   ";
					strxml +="<b>Address :</b>												   ";
					strxml +="</td>													   ";
					strxml +="<td align='left' colspan='2'>											   ";
					strxml +=shipfromaddress1+ " "+ shipfromaddress2;
					strxml +="</td>													   ";
					strxml +="</tr>													   ";
					strxml +="<tr>													   ";
					strxml +="<td align='left' width='20%'>											   ";
					strxml +="<b>City / State / ZIP :</b>											   ";
					strxml +="</td>													   ";
					strxml +="<td align='left' width='30%' colspan='2'>											   ";
					strxml +=shipfromcity + " "+ shipfromstate + " "+ shipfromzipcode;
					strxml +="</td>													   ";
					strxml +="</tr>													   ";
					strxml +="<tr>													   ";
					strxml +="<td align='left'>											   ";
					strxml +="<b>SID # :	</b></td><td>												   ";
					strxml +=IndividiualSOdetails[0][19];
					strxml +="</td>													   ";
					strxml +="<td align='right'>											   ";
					strxml +="<b>FOB ( )</b>												   ";
					strxml +="</td>													   ";
					strxml +="</tr>													   ";
					strxml +="</table>												   ";
					strxml +="</td>													   ";
					strxml +="</tr>													   ";
					strxml +="<tr  style=\"background-color:black;color:white\">													   ";
					strxml +="<td align='center' style='font-weight:bold;border-color: #000000; border-right-style: solid; border-left-style: solid;	   ";
					strxml +="border-right-width: 1px; border-left-width: 1px; color: #FFFFFF;' >			   ";
					strxml +="SHIP TO												   ";
					strxml +="</td>													   ";
					strxml +="</tr>													   ";
					strxml +="<tr height='55'>													   ";
					strxml +="<td valign='middle'>													   ";
					strxml +="<table style='border-style: none; border-color: #000000;' id='Table8' width='80%' ";
					strxml +=" cellpadding='0' border='1'>								   ";
					strxml +="<tr>													   ";
					strxml +="<td align='left'>											   ";
					strxml +="<b>Name :	</b>												   ";
					strxml +="</td>													   ";
					strxml +="<td align='left' colspan='2'>											   ";
					strxml +=shiptocompany;
					strxml +="</td>													   ";
					strxml +="</tr>													   ";
					strxml +="<tr>													   ";
					strxml +="<td align='left'>											   ";
					strxml +="<b>Address :</b>												   ";
					strxml +="</td>													   ";
					strxml +="<td align='left' colspan='2'>											   ";
					strxml +=shiptoTotAdd;
					strxml +="</td>													   ";
					strxml +="</tr>													   ";
					strxml +="<tr>													   ";
					strxml +="<td align='left' width='20%'>											   ";
					strxml +="<b>City /State / ZIP :</b>											   ";
					strxml +="</td>													   ";
					strxml +="<td align='left' width='30%' colspan='2'>											   ";
					strxml +=shiptocity + " "+ shiptostate+ " "+ shiptozipcode          ;
					strxml +="</td>													   ";
					strxml +="</tr>													   ";
					strxml +="<tr>													   ";
					strxml +="<td align='left'>											   ";
					strxml +="<b>CID # :</b></td><td>													   ";
					strxml +=IndividiualSOdetails[0][20];
					strxml +="</td>													   ";
					strxml +="<td align='right'>											   ";
					strxml +="<b>FOB ( )</b>											   ";
					strxml +="</td>	</tr>	</table></td></tr>													   ";	
					strxml +="<tr  style=\"background-color:black;color:white\">													   ";
					strxml +="<td align='center' style='border-color: #000000; border-right-style: solid; border-left-style: solid;	   ";
					strxml +="border-right-width: 1px; border-left-width: 1px; color: #FFFFFF;'>			   ";
					strxml +="THIRD PARTY FREIGHT CHARGE BILL TO									   ";
					strxml +="</td>													   ";
					strxml +="</tr>													   ";
					strxml +="<tr height='55'>											   ";
					strxml +="<td align='left'>											   ";
					strxml +="<table style='border-style: none; border-color: #000000;' id='Table9' width='100%' ";
					strxml +=" cellpadding='0' border='1'>								   ";
					strxml +="<tr><td>&nbsp;</td></tr>	</table></td>	</tr>											   ";	
					strxml +="<tr>													   ";
					strxml +="<td style='border: thin solid #000000;border-color: #000000;'>									   ";
					strxml +="</td>													   ";
					strxml +="</tr>													   ";
					strxml +="<tr height='55'>											   ";
					strxml +="<td align='left' style='border-style: none;'>								   ";
					strxml +="<table style='border-style: none; border-color: #000000;' id='Table10' width='100%' ";
					strxml +=" cellpadding='0' >								   ";
					strxml +="<tr>													   ";
					strxml +="<td>									   ";
					strxml +="<b>Special Instructions</b>	<br/>										   ";
					strxml +=IndividiualSOdetails[0][21];
					strxml +="</td>	</tr></table></td>	</tr></table></td>	</tr></table></td>												   ";
					strxml +="<td style='border-style: none; border-color: #000000;' width='100%' valign='top' colspan='2'>			   ";
					strxml +="<table width='100%' style='border-style: none; border-color: #000000;'>				   ";
					strxml +="<tr>													   ";
					strxml +="<td style='border-style: none; border-color: #000000;' width='100%'>						   ";
					strxml +="<table style='border-style: none; border-color: #000000;' id='Table1' width='100%' ";
					strxml +=" cellpadding='0' border='1'>								   ";
//					strxml +="<tr>													   ";
//					strxml +="<td style='border: thin solid #000000;border-color: #000000;'>									   ";
//					strxml +="</td>													   ";
//					strxml +="</tr>													   ";
					strxml +="<tr height='80'>											   ";
					strxml +="<td align='left' valign='top' style='border: thin solid #000000;border-color: #000000;' >						   ";
					strxml +="<table style='border-style: none; border-color: #000000;' id='Table4' width='100%' ";
					strxml +=" cellpadding='0' border='1'>								   ";
					strxml +="<tr>													   ";
					strxml +="<td align='center' width='100%' style='font-weight:bold;'>											   ";
					strxml +="Bill Of Lading Number : " + childbollable ;
					strxml +="</td>	</tr>";
					strxml +="<tr>        <td align='center'>";
					if(childbollable != null && childbollable != "")
					{
						strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
						strxml += childbollable;
						strxml += "\"/>";
					}
					strxml +="</td>    </tr>";
					strxml +="</table></td>	</tr>													   ";		
					strxml +="<tr height='40'>													   ";
					strxml +="<td valign='middle' align='left' style='border: thin solid #000000;border-color: #000000;'>							   ";
					strxml +="<table style='border-style: none; border-color: #000000;' id='Table7' width='100%' ";
					strxml +=" cellpadding='0' border='1'>								   ";
					strxml +="<tr>													   ";
					strxml +="<td align='left'>											   ";
					strxml +="<b>Carrier Name :	</b>											   ";
					strxml +="</td>													   ";
					strxml +="<td align='left'>											   ";
					strxml +=shipItemCode;//carrier;
					strxml +="</td>													   ";
					strxml +="</tr>													   ";
					strxml +="<tr>													   ";
					strxml +="<td align='left'>											   ";
					strxml +="<b>Trailer Number :</b>											   ";
					strxml +="</td>													   ";
					strxml +="<td align='left'>											   ";
//					strxml +=trailerName;
					strxml +=vtrailerno;
					strxml +="</td>													   ";
					strxml +="</tr>													   ";
					strxml +="<tr>													   ";
					strxml +="<td align='left'>											   ";
					strxml +="<b>Seal Number(s) :</b>											   ";
					strxml +="</td>													   ";
					strxml +="<td align='left'>											   ";
					strxml +=sealno;
					strxml +="</td>	</tr></table></td>	</tr>												   ";	
					strxml +="<tr height='40'>													   ";
					strxml +="<td valign='middle' align='left' style='border: thin solid #000000;border-color: #000000;'>							   ";
					strxml +="<table style='border-style: none; border-color: #000000;' id='Table11' width='100%' ";
					strxml +=" cellpadding='0' border='1'>								   ";
					strxml +="<tr>													   ";
					strxml +="<td align='left'>											   ";
					strxml +="<b>SCAC :	</b>												   ";
					strxml +="</td>													   ";
					strxml +="<td align='left'>											   ";
					strxml +=carrier;//shipItemCode;
					strxml +="</td>													   ";
					strxml +="</tr>													   ";
					strxml +="<tr>													   ";
					strxml +="<td align='left'>											   ";
					strxml +="<b>Pro Number :</b>												   ";
					strxml +="</td>													   ";
					strxml +="<td align='left'>											   ";
					strxml +="";//proNum;
					strxml +="</td>													   ";
					strxml +="</tr>													   ";
					strxml +="<tr>													   ";
					strxml +="<td align='left'>											   ";
					strxml +="<b>Load Number :	</b>											   ";
					strxml +="</td>													   ";
					strxml +="<td align='left'>											   ";
//					strxml +=loadnumber;
					strxml +=trailerID												   ;
					strxml +="</td>	</tr></table></td>	</tr>												   ";	
					strxml +="<tr height='50'>											   ";
					strxml +="<td align='left' style='border: thin solid #000000;border-color: #000000;' valign='top'>						   ";
					strxml +="<table style='border-style: none; border-color: #000000;' id='Table12' width='100%' ";
					strxml +=" cellpadding='0' border='1'>								   ";
					strxml +="<tr>													   ";
					strxml +="<td colspan='3'>											   ";
					strxml +="<b>Freight Charge Terms : (Freight Charges are prepaid unless marked<br/> otherwise)	</b>		   ";
					strxml +="</td>	</tr>													   ";	
					strxml +="<tr>	<td>&nbsp;</td></tr>												   ";	
					strxml +="<tr>													   ";
					strxml +="<td style='padding-right: 60px; border-color: #000000; padding-bottom: 5px'>				   ";
					strxml +="<b>Prepaid :__________	</b>										   ";
					strxml +="</td>													   ";
					strxml +="<td style='padding-right: 60px; border-color: #000000; padding-bottom: 5px'>				   ";
					strxml +="<b>Collect :</b> <u> "+freightterms+"</u>											   ";
					strxml +="</td>													   ";
					strxml +="<td style='padding-right: 60px; border-color: #000000; padding-bottom: 5px'>				   ";
					strxml +="<b>3rd Party :__________</b>											   ";
					strxml +="</td>	</tr></table></td>	</tr>														   ";		
					strxml +="<tr height='20'>											   ";
					strxml +="<td align='left' style='border: thin solid #000000;border-color: #000000;'>							   ";
					strxml +="<table style='border-style: none; border-color: #000000;' id='Table13' width='100%'			   ";
					strxml +=" cellpadding='0' border='1'>								   ";
					strxml +="<tr  height='40'>													   ";
					strxml +="<td style='padding-left: 30px; border-style: none;border-color: #000000;'>				   ";
					strxml +="( ) Master Bill of Lading : With attached Underlying Bills of Lading					   ";
					strxml +="</td>	</tr></table></td>	</tr></table>	</td>	</tr></table>	</td>	</tr>  ";
					strxml +="<tr height='20' style=\"background-color:black;color:white\">													   ";
					strxml +="<td colspan='4' valign='middle' align='center' style='border-color: #000000; border-right-style: solid;		   ";
					strxml +="border-left-style: solid; border-right-width: 1px; border-left-width: 1px; color: #FFFFFF;'>		   ";
					//strxml +="bgcolor='Black'>											   ";
					strxml +="CUSTOMER ORDER INFORMATION												   ";
					strxml +="</td>													   ";
					strxml +="</tr>													   ";
					strxml +="<tr>													   ";
					strxml +="<td align='center' colspan='4' style='border-style: none; border-width: 0px; border-color: #000000'>	   ";
					strxml +="<table style='border-style: none; border-color: #000000;' id='Table19' ";
					strxml +="width='100%' cellpadding='0' border='1'>								   ";
					strxml +="<tr height='30'>													   ";
					strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>							   ";
					strxml +="CUSTOMER <br />ORDER NUMBER											   ";
					strxml +="</td>													   ";
					strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center'>							   ";
					strxml +="#PALLETS												   ";
					strxml +="</td>													   ";
					strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center' >						   ";
					strxml +="PRODUCT<br />WEIGHT											   ";
					strxml +="</td>													   ";
					strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center' >						   ";
					strxml +="PALLET <br />WEIGHT												   ";
					strxml +="</td>													   ";
					strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center'>							   ";
					strxml +="TOTAL <br />WEIGHT												   ";
					strxml +="</td>													   ";
					strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center' >						   ";
					strxml +="PALLET/SLIP												   ";
					strxml +="</td>													   ";
					strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center'>							   ";
					strxml +="MUST DELIVER<br /> BY DATE											   ";
					strxml +="</td>													   ";
					strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center'>							   ";
					strxml +="ORDER NUMBER												   ";
					strxml +="</td>	</tr>										   ";

					var CustomerPO1 = HeaderDetails[p][1];
					var noofpallets1 = HeaderDetails[p][2];
					var globalproductweight1 = HeaderDetails[p][3];
					var mustdelshipdate1 = HeaderDetails[p][4];
					var ordernumber1 = HeaderDetails[p][5];
					var totalnoofpallets1 = HeaderDetails[p][6];
					var totalglobalproductweight1 = HeaderDetails[p][7];
					var totalitemeachQuantity1 = HeaderDetails[p][8];
					var globalitempalletQuantity1 = HeaderDetails[p][9];
					var palletweight1=HeaderDetails[p][10];
					var totalpalletweight1=HeaderDetails[p][11];
					var productweightfre1=HeaderDetails[p][12];
					var totproductweightfre1=HeaderDetails[p][13];
					var grandtotal;
					//var totalitemeachQuantity1 = 1;

					nlapiLogExecution('ERROR', 'CustomerPO1',CustomerPO1);

					strxml +="<tr height='20'>	  ";
					strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>";
					strxml +=CustomerPO1  ;
					strxml +="</td>	   ";
					strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>   ";
					strxml +=noofpallets1;
					strxml +="</td>  ";
					strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>	   ";
					//strxml +=globalproductweight1;
					if(vQbcarriertype == '2')
						strxml +=totproductweightfre1.toFixed(2);					
					else
						strxml +=globalproductweight1.toFixed(2);
					strxml +="</td>	  ";
					strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>  ";
//					if(vQbcarriertype == '202')
//					strxml +=globalitempalletQuantity1;
//					else
//					strxml +="0";
					if(vQbcarriertype == '2')
						strxml +=palletweight1.toFixed(2);
					else
						strxml +="0";
					strxml +="</td>	   ";
					strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>	  ";
					if(vQbcarriertype == '2')
					{
						grandtotal=parseFloat(totproductweightfre1)+parseFloat(palletweight1);
						strxml +=grandtotal.toFixed(2);	
					}
					else
						strxml +=globalproductweight1.toFixed(2);
					strxml +="</td>	  ";
					strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>  ";
					strxml +="Y ";
					strxml +="</td>	  ";
					strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>  ";
					strxml +=mustdelshipdate1;
					strxml +="</td>	  ";
					strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>   ";
					strxml +=ordernumber1;
					strxml +="</td>	  ";
					strxml +="</tr>  ";

					//}			
					//}
					strxml +="<tr  height='20'>	  ";
					strxml +="<td align='center' style='border: thin solid #000000;border-color: #000000;'>   ";
					strxml +="Grand Total"  ;
					strxml +="</td>	   ";
					strxml +="<td align='center' style='border: thin solid #000000;border-color: #000000;'>   ";
					strxml +=noofpallets1;
					strxml +="</td>  ";
					strxml +="<td align='center' style='border: thin solid #000000;border-color: #000000;'>	   ";
					//strxml +=globalproductweight1;
					if(vQbcarriertype == '2')
						strxml +=totproductweightfre1.toFixed(2);
					else					
						strxml +=globalproductweight1.toFixed(2);
					strxml +="</td>	  ";
					strxml +="<td align='center' style='border: thin solid #000000;border-color: #000000;'>  ";
//					if(vQbcarriertype == '202')
//					strxml +=globalitempalletQuantity1;
//					else
//					strxml +="0";
					if(vQbcarriertype == '2')
						strxml +=palletweight1.toFixed(2);
					else
						strxml +="0";
					strxml +="</td>	   ";
					strxml +="<td align='center' style='border: thin solid #000000;border-color: #000000;'>	  ";
					if(vQbcarriertype == '2')
					{
						grandtotal=parseFloat(totproductweightfre1)+parseFloat(palletweight1);
						strxml +=grandtotal.toFixed(2);	
					}
					else
						strxml +=globalproductweight1.toFixed(2);
					strxml +="</td>	  ";
					strxml +="<td style='border: thin solid #000000;border-color: #000000;'>  ";
					strxml +="&nbsp;";
					strxml +="</td>	  ";
					strxml +="<td style='border: thin solid #000000;border-color: #000000;'>  ";
					strxml +="&nbsp;";
					strxml +="</td>	  ";
					strxml +="<td style='border: thin solid #000000;border-color: #000000;'>   ";
					strxml +="&nbsp;";
					strxml +="</td>	  ";
					strxml +="</tr>  ";

					//ending of child header
					//}
					strxml +="</table></td>	</tr></table></td>	</tr>	   ";		
					strxml +="<tr>													   ";
					strxml +="<td>													   ";
					strxml +="<table  align='center' cellpadding='0' style='border-color: #000000; ";
					strxml +="border-width: 1;' width='100%' border='1'>								   ";
					strxml +="<tr>													   ";
					strxml +="<td colspan='3' width='100%' style='border-style: none;'>						   ";
					strxml +="<table  cellpadding='0' style='border-color: #000000; border-style: none; ";
					strxml +="border-width: 1;' width='100%' border='1'>								   ";
					strxml +="<tr height='20' style=\"background-color:black;color:white\">													   ";
					strxml +="<td colspan='9' valign='middle' align='center' style='font-weight:bold;border-color: #000000; border-right-style: solid;		   ";
					strxml +="border-left-style: solid; border-right-width: 1px; border-left-width: 1px; color: #FFFFFF;'	>	   ";
					//strxml +="bgcolor='Black'>											   ";
					strxml +="CARRIER INFORMATION											   ";
					strxml +="</td>													   ";
					strxml +="</tr>													   ";
					strxml +="<tr height='40'>													   ";
					strxml +="<td colspan='2' align='center' style='border: thin solid #000000;border-color: #000000;'>						   ";
					strxml +="HANDLING UNIT												   ";
					strxml +="</td>													   ";
					strxml +="<td colspan='2' align='center' style='font-weight:bold;border: thin solid #000000;border-color: #000000;'>						   ";
					strxml +="PACKAGE												   ";
					strxml +="</td>													   ";
					strxml +="<td rowspan='2' align='center' style='font-weight:bold;border: thin solid #000000;border-color: #000000;'>						   ";
					strxml +="WEIGHT												   ";
					strxml +="</td>													   ";
					strxml +="<td rowspan='2' align='center' style='font-weight:bold;border: thin solid #000000;border-color: #000000;'>						   ";
					strxml +="H.M.(X)												   ";
					strxml +="</td>													   ";
					strxml +="<td rowspan='2' width='50%' align='center' style='border: thin solid #000000;border-color: #000000;'>";
					strxml +="<b>COMMODITY DESCRIPTION</b><br />";
					strxml +="<p align='left'><span style='font-size:6pt'>Commodities Requiring special or additional care or attention in handling or slowing ";
					strxml +="must be so";
					strxml +="marked as to ensure safe transportation with ordinary care";
					strxml +="see section 2 (e) of NMFC item 360</span></p>";
					strxml +="</td>";						
					strxml +="<td colspan='2' align='center' style='font-weight:bold;border: thin solid #000000;border-color: #000000;'>						   ";
					strxml +="LTL ONLY												   ";
					strxml +="</td>													   ";
					strxml +="</tr>													   ";
					strxml +="<tr height='20'>													   ";
					strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center'>							   ";
					strxml +="QTY													   ";
					strxml +="</td>													   ";
					strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center'>							   ";
					strxml +="TYPE													   ";
					strxml +="</td>													   ";
					strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center'>							   ";
					strxml +="QTY													   ";
					strxml +="</td>													   ";
					strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center'>							   ";
					strxml +="TYPE													   ";
					strxml +="</td>													   ";
					strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center'>							   ";
					strxml +="NMFC													   ";
					strxml +="</td>													   ";
					strxml +="<td style='font-weight:bold;border: thin solid #000000;border-color: #000000;' align='center'>							   ";
					strxml +="CLASS													   ";
					strxml +="</td>													   ";
					strxml +="</tr>													   ";
					strxml +="<tr height='20'>											   ";
					//container loop starts here
					strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>	 ";
					strxml +=noofpallets1;
					strxml +="</td>													   ";
					strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>							   ";
					strxml +="PALL													   ";
					strxml +="</td>													   ";
					strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>							   ";
					strxml +=totalitemeachQuantity1.toFixed(2);
					strxml +="</td>													   ";
					strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>							   ";
					strxml +="EA													   ";
					strxml +="</td>													   ";
					strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>							   ";

					if(vQbcarriertype == '2')
					{
						grandtotal=parseFloat(totproductweightfre1)+parseFloat(palletweight1);
						strxml +=grandtotal.toFixed(2);	
					}
					else
						strxml +=totalglobalproductweight1.toFixed(2);
					strxml +="</td>													   ";
					strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>							   ";
					strxml +="&nbsp;												   ";
					strxml +="</td>													   ";
					strxml +="<td style='border-color: #000000; padding-left: 30px;' align='left'>					   ";
					strxml +="BICYCLE												   ";
					strxml +="</td>													   ";
					strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>							   ";
					strxml +="&nbsp;												   ";
					strxml +="</td>													   ";
					strxml +="<td style='border: thin solid #000000;border-color: #000000;' align='center'>							   ";
					strxml +="125													   ";
					strxml +="</td>	</tr>												   ";		
					strxml +="</table></td>	</tr>														   ";

					strxml +="<tr height='75'><td valign='middle' colspan='2' style='border: thin solid #000000;border-color: #000000;' width='65%' >				   ";
					strxml +="<table  align='center' cellpadding='5' width='100%'>								   ";
					strxml +="<tr><td>	  ";

					strxml +="Where the rate is dependent on value, shippers are required to state specifically			   ";
					strxml +="in writing the agreed or									   ";
					strxml +="declared value of the property.<br />									   ";
					strxml +="The agreed or declared value of the property is hereby specifically stated by the			   ";
					strxml +="shipper to be not exceeding<br />									   ";
					strxml +="__________________ per __________________								   ";
					strxml +="<br />												   ";
					strxml +="&nbsp;												   ";
					strxml +="</td>	</tr></table></td>												   ";
					strxml +="<td valign='middle' style='border: thin solid #000000;border-color: #000000;' >							   ";
					strxml +="<table  align='center' cellpadding='5' width='100%'>								   ";
					strxml +="<tr><td>	  ";

					strxml +="<b>COD Amount : $</b> _____________________<br />								   ";
					strxml +="<br />												   ";
					strxml +="Fee Terms: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Collect ( ) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Prepaid  ";
					strxml +="( )<br />												   ";
					strxml +="<br />												   ";
					strxml +="Customer Check Acceptable : ( )									   ";
					strxml +="</td>	</tr></table></td>												   ";
					strxml +="</tr>													   ";
					strxml +="<tr >													   ";
					strxml +="<td valign='middle' height='30' colspan='3' style='font-weight:bold;border: thin solid #000000;border-color: #000000;'>							   ";
					strxml +="&nbsp;&nbsp;NOTE:Liability Limitation for loss or damage in this shipment may be applicable.			   ";
					strxml +="See 49 U.S.C. 14706(c)(1)(A) and (B)									   ";
					strxml +="</td>													   ";
					strxml +="</tr>													   ";
					strxml +="<tr height='75'>													   ";
					strxml +="<td colspan='2' valign='middle' style='font-size:7pt;border: thin solid #000000;border-color: #000000;'>							   ";
					strxml +="<table  align='center' cellpadding='5' width='100%'>								   ";
					strxml +="<tr><td>	  ";

					strxml +="RECEIVED, subject to individually determined rates or contracts that have been agreed			   ";
					strxml +="upon in writing between the carrier and								   ";
					//strxml +="<br />												   ";
					strxml +="shipper, if applicable, other wise to the rates, classifications and rules that have			   ";
					strxml +="been established by the carrier and are available to						   ";
					strxml +="the shipper on request and all the terms and conditions of the NMFC Uniform Straight			   ";
					strxml +="Bill of Lading.											   ";
					strxml +="</td>	</tr></table></td>												   ";
					strxml +="<td style='border: thin solid #000000;border-color: #000000;'>									   ";

					strxml +="<table  align='center' cellpadding='5' width='100%'>								   ";
					strxml +="<tr><td>	  ";
					strxml +="<p><span style='font-size:6pt'>The carrier shall not take delivery of the shipment without payment of freight and all			   ";
					strxml +="other lawful charges.<br /></span></p>										   ";
					strxml +="<br />												   ";
					strxml +="________________________________ Shipper<br />							   ";
					strxml +="Signature:												   ";
					strxml +="</td>	</tr></table></td>												   ";
					strxml +="</tr>													   ";
					strxml +="<tr style='border-style: none; border-width: 0px; border-color: #000000' >				   ";
					strxml +="<td style='border: thin solid #000000;border-color: #000000;'  valign='top'>						   ";
					strxml +="<table  align='center' cellpadding='5' width='100%'>								   ";
					strxml +="<tr><td>	  ";

					strxml +="<b>SHIPPER SIGNATURE / DATE:</b><br />									   ";
					strxml +="<p align='left'><span style='font-size:7pt'>This is to certify that the above named materials are properly classified, packaged, ";
					strxml +="marked and labeled and &#47; or in proper condition for transportation, according to the";
					strxml +=" applicable regulations of the DOT.</span></p>		<br/>						   ";
					strxml +="__________________________Shipper<br />							   ";
					strxml +="Signature:												   ";
					strxml +="</td>	</tr></table></td>													   ";
					strxml +="<td width='60%' style='border: thin solid #000000;border-color: #000000;' valign='top'>						   ";

					strxml +="<table width='100%'><tr><td style='font-weight:bold;'>";
					strxml +="Trailer Loaded:</td><td style='font-weight:bold;'>";
					strxml +="Freight Counted:</td></tr><tr><td>";
					strxml +="(X)By shipper</td><td>";
					strxml +="(X)By shipper</td></tr><tr><td>";
					strxml +="( )By Driver</td><td>";
					strxml +="( )By Driver/Pallets said to contain</td></tr><tr><td>";
					strxml +="&nbsp;</td><td>";
					strxml +="( )By Driver/Pieces</td></tr></table>	";		
					strxml +="</td>													   ";
					strxml +="<td width='58%' align='left' style='border: thin solid #000000;border-color: #000000;' valign='top'>						   ";

					strxml +="<table  align='center' cellpadding='5' width='100%'>								   ";
					strxml +="<tr><td>	  ";

					strxml +="<b>CARRIER SIGNATURE / PICKUP DATE</b><br />									   ";
					strxml +="<p align='left'><span style='font-size:7pt'>Carrier acknowledge receipt of packages and required placards. Carrier certifies			   ";
					strxml +="emergency response information was made available and for carrier has the DOT emergency		   ";
					strxml +="response guide book or equivalent documentation in the vehicle.						   ";
					strxml +="<br /><br />	<br />											   ";
					strxml +="Property described above is received in good order, except as noted.</span></p>					   ";
					strxml +="</td></tr></table></td>	</tr></table></td>	</tr></table>";


					strxml += "</td></tr></table>";
					nlapiLogExecution('ERROR', 'distinctSOno.length-1', distinctSOno.length-1);
					nlapiLogExecution('ERROR', 'p', p);
					if((distinctSOno.length-1) != p)
					{
						nlapiLogExecution('ERROR', 'into brk', 'done');
						//strxml +="<p style='page-break-after:always'></p>";
					}

				}
			}
		}		
		strxml =strxml+ "\n</body>\n</pdf>";		
		//nlapiLogExecution('ERROR', 'strxml', strxml);
		xml=xml +strxml;
		//nlapiLogExecution('ERROR','XML',xml);

		var file = nlapiXMLToPDF(xml);	
		nlapiLogExecution('ERROR','Remaining usage at the End',context.getRemainingUsage());
		response.setContentType('PDF','BOLReport.pdf');
		response.write( file.getValue() );
	}
	else //this is the POST block
	{

	}
}

function getPalletQuantityFromItemDims(itemId, packCode){
	nlapiLogExecution('ERROR', 'getPalletQuantityFromItemDims','done');
	var itemQuantity = 0;
	var filters = new Array();          
	if(itemId !=null && itemId !=""){
		filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is',itemId)); 		
	}
	filters.push(new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'anyof', [3]));	 
	if(packCode !=null && packCode !=""){
		//filters.push(new nlobjSearchFilter('custrecord_ebizpackcodeskudim', null, 'is',packCode));		 
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizweight');

	var searchItemResults = nlapiSearchRecord('customrecord_ebiznet_skudims',null, filters, columns);

	if(searchItemResults!=null && searchItemResults !=""){    		
		itemQuantity = searchItemResults[0].getValue('custrecord_ebizweight');  
	}    	

	return itemQuantity;	
}
function getItemQuantityFromItemDims(itemId, packCode){
	nlapiLogExecution('ERROR', 'getItemQuantityFromItemDims','done');
	var itemQuantity = 0,itemQuantitywght=0;
	var filters = new Array();    
	var Currentrow = new Array();    
	if(itemId !=null && itemId !=""){
		filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is',itemId)); 		
	}
	filters.push(new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'anyof', [1]));	 
	if(packCode !=null && packCode !=""){
		//filters.push(new nlobjSearchFilter('custrecord_ebizpackcodeskudim', null, 'is',packCode));		 
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizqty');
	columns[1] = new nlobjSearchColumn('custrecord_ebizweight');

	var searchItemResults = nlapiSearchRecord('customrecord_ebiznet_skudims',null, filters, columns);

	if(searchItemResults!=null && searchItemResults !=""){    		
		itemQuantity = searchItemResults[0].getValue('custrecord_ebizqty'); 
		itemQuantitywght = searchItemResults[0].getValue('custrecord_ebizweight'); 
	}    	
	Currentrow.push(itemQuantity);
	Currentrow.push(itemQuantitywght);
	return Currentrow;	
}
function DateStamp(){
	var now = new Date();
	return ((parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
}


function GenerateLable(company){	 
	var finaltext="";
	var duns="";
	var ResultText="";
	//added by mahesh

	nlapiLogExecution('ERROR', 'company', company);
	try 
	{	
		var lpMaxValue=GetMaxLPNo('1', '6','');
		nlapiLogExecution('ERROR', 'lpMaxValue', lpMaxValue);

		var dunsfilters = new Array();
		dunsfilters[0] = new nlobjSearchFilter('custrecord_company', null, 'is', company);
		var dunscolumns = new Array();
		dunscolumns[0] = new nlobjSearchColumn('custrecord_compduns');			 

		var dunssearchresults = nlapiSearchRecord('customrecord_ebiznet_company', null, dunsfilters, dunscolumns);
		if (dunssearchresults !=null && dunssearchresults !="") 
		{
			duns = dunssearchresults[0].getValue('custrecord_compduns');
			nlapiLogExecution('ERROR', 'duns', duns);
		}

		var dunssubstring = duns.substring(1, duns.length);
		finaltext=dunssubstring+"00000"+lpMaxValue;

		//to get chk digit

		var str1=finaltext;
		var str2='1313131313131313';
		var res1=str1.split("");
		var res2=str2.split("");
		var total=0;
		for(var i=0;i < res1.length;i++)
		{
			var rchkdigit=parseFloat(res1[i])+parseFloat(res2[i]);
			total=parseFloat(total)+parseFloat(rchkdigit);
		}
//		var rem=Math.round(total/10);
//		var multiple=rem*10;
		var multiple=parseFloat(Math.ceil(total / 10.0))*10;	
		var CheckDigitValue=(multiple-total).toString();		
		ResultText=finaltext+CheckDigitValue;

	} 
	catch (err) 
	{

	}
	nlapiLogExecution('ERROR', 'out of GenerateLable', 'done');
	return ResultText;

}
function GetMaxLPNo(lpGenerationType, lpType,whsite){
	var maxLP = 1;
	var maxLPPrefix = "";

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpmax');
	columns[1] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix');

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'is', lpGenerationType));
	filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lptype', null, 'anyof', [lpType]));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(whsite!=null && whsite!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_site', null, 'anyof', ['@NONE@', whsite]));

	var results = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, filters, columns);

	if(results != null){
		if(results.length > 1){
			alert('More records returned than expected');
			nlapiLogExecution('ERROR', 'GetMaxLPNo:LP Max Query returned more than 1 row');
		}else{
			// Incrementing the max LP number only if the search returns 1 row
			for (var t = 0; t < results.length; t++){
				if (results[t].getValue('custrecord_ebiznet_lprange_lpmax') != null) {
					maxLP = results[t].getValue('custrecord_ebiznet_lprange_lpmax');
					maxLPPrefix = results[t].getValue('custrecord_ebiznet_lprange_lpprefix'); 


				}

				if(maxLP==null||maxLP==''||isNaN(maxLP))
					maxLP=0;

				maxLP = parseFloat(maxLP) + 1;
				// update the new max LP in the custom record
				nlapiSubmitField('customrecord_ebiznet_lp_range', results[t].getId(),
						'custrecord_ebiznet_lprange_lpmax', parseFloat(maxLP));
			}
		}
	}

	// convert maxLP to string
	maxLP = maxLP.toString();
	nlapiLogExecution('DEBUG', 'GetMaxLPNo:New MaxLP', maxLP);
	return maxLPPrefix + maxLP;
}

function getTrailerName(waveno)
{
	nlapiLogExecution('ERROR','Into getTrailerName',waveno);
	var TotalTrailerRes=new Array();
	var trailername='';
	var id='';
	var vfilter = new Array();				
	vfilter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', 10));				
	vfilter.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', waveno));

	var vcolumn = new Array();
	vcolumn[0] = new nlobjSearchColumn('custrecord_ebiz_trailer_no');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, vfilter, vcolumn);
	if(searchresults!=null&&searchresults!="")
		trailername=searchresults[0].getValue('custrecord_ebiz_trailer_no');
	
	if(trailername!=null&&trailername!="")
	{
		var filter=new Array();
		filter[0]=new nlobjSearchFilter('name',null,'is',trailername);
		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizid');

		var rec=nlapiSearchRecord('customrecord_ebiznet_trailer',null,filter,column);
		if(rec!=null&&rec!="")
			id=rec[0].getValue('custrecord_ebizid');
		nlapiLogExecution('ERROR','Outof getTrailerName',id);
	}
	TotalTrailerRes.push(id);
	TotalTrailerRes.push(trailername);
	return TotalTrailerRes;
}
function removeDuplicateElement(arrayName)
{
	var newArray=new Array();
	label:for(var i=0; i<arrayName.length;i++ )
	{  
		for(var j=0; j<newArray.length;j++ )
		{
			if(newArray[j]==arrayName[i]) 
				continue label;
		}
		newArray[newArray.length] = arrayName[i];
	}
	return newArray;
} 


//The below functions are written by Satish.N on 07/2012.

function getItemQuantityFromItemDimsAll(itemarray){
	nlapiLogExecution('ERROR', 'Into getItemQuantityFromItemDimsAll');
	var searchItemResults = new Array();
	var filters = new Array();    
	var Currentrow = new Array();   

	if(itemarray !=null && itemarray !=""){
		filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof',itemarray)); 		
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizqty');
	columns[1] = new nlobjSearchColumn('custrecord_ebizweight');
	columns[2] = new nlobjSearchColumn('custrecord_ebizpackcodeskudim');
	columns[3] = new nlobjSearchColumn('custrecord_ebizitemdims');
	columns[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim');

	searchItemResults = nlapiSearchRecord('customrecord_ebiznet_skudims',null, filters, columns);

	nlapiLogExecution('ERROR', 'Out of getItemQuantityFromItemDimsAll');

	return searchItemResults;
}

function getItemEachQuantityFromItemDims(allitemseachQuantity,itemId, packCode,uomlevel){
	nlapiLogExecution('ERROR', 'Into getItemEachQuantityFromItemDims');

	var itemQuantity = 0,itemQuantitywght=0;	
	var Currentrow = new Array();    

	if(allitemseachQuantity!=null && allitemseachQuantity !="")
	{  


		for(var j=0; j<allitemseachQuantity.length;j++ )
		{	
			var dimsitem=allitemseachQuantity[j].getValue('custrecord_ebizitemdims');
			var dimsuomlevel=allitemseachQuantity[j].getValue('custrecord_ebizuomlevelskudim');

			var str = 'ItemDims Length. = ' + allitemseachQuantity.length + '<br>';
			str = str + 'itemId. = ' + itemId + '<br>';
			str = str + 'uomlevel. = ' + uomlevel + '<br>';	
			str = str + 'dimsitem. = ' + dimsitem + '<br>';	
			str = str + 'dimsuomlevel. = ' + dimsuomlevel + '<br>';

//			nlapiLogExecution('ERROR', 'Log in dims iteration', str);

			if(itemId == allitemseachQuantity[j].getValue('custrecord_ebizitemdims') 
					&& uomlevel==allitemseachQuantity[j].getValue('custrecord_ebizuomlevelskudim') )
			{
				itemQuantity = allitemseachQuantity[j].getValue('custrecord_ebizqty'); 
				itemQuantitywght = allitemseachQuantity[j].getValue('custrecord_ebizweight'); 
			}
		}
	}    	
	Currentrow.push(itemQuantity);
	Currentrow.push(itemQuantitywght);

	nlapiLogExecution('ERROR', 'Out of getItemEachQuantityFromItemDims');

	return Currentrow;	
}



//Upto here.