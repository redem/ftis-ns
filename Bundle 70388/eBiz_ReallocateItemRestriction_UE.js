/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/UserEvents/eBiz_ReallocateItemRestriction_UE.js,v $
 *     	   $Revision: 1.1.14.3 $
 *     	   $Date: 2015/02/16 07:32:38 $
 *     	   $Author: snimmakayala $
 *     	   $Name: t_eBN_2014_2_StdBundle_0_222 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 * *
 * DESCRIPTION
 *  	
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: eBiz_ReallocateItemRestriction_UE.js,v $
 * Revision 1.1.14.3  2015/02/16 07:32:38  snimmakayala
 * Case#: 201411516
 *
 * Revision 1.1.14.2  2015/02/12 07:05:20  spendyala
 * 201411516
 *
 * Revision 1.1.14.1  2013/07/15 14:26:38  rrpulicherla
 * case# 20123425
 * GFT Issue fixes
 *
 * Revision 1.1  2011/12/12 22:01:39  gkalla
 * CASE201112/CR201113/LOG201121
 * New Userevent for deallocation
 * 
 *
 *****************************************************************************/
function AutoAdjustFulfillmentOrders(type, form, request)
{
	//var create_fulfillment_order = nlapiGetFieldValue('custbody_create_fulfillment_order');
	//var soid = nlapiGetRecordId();
	var vCount = nlapiGetLineItemCount('order');
	var vFOtoDeleteCount=0;
	var vArrayFOinternalId=new Array();
	nlapiLogExecution('ERROR','vCount',vCount);
	var vCountSales=0;
	for(var p=1;p<=vCount;p++)
	{
		var commitFlag = nlapiGetLineItemValue('order', 'commit', p);
		if(commitFlag=='T')
		{
			var vMessage='';
			vCountSales = vCountSales +1; 
			//nlapiLogExecution('ERROR','commitFlag',commitFlag);
			var vItem = nlapiGetFieldValue('item');
			nlapiLogExecution('ERROR','vItem',vItem);
			var soid = nlapiGetLineItemValue('order', 'orderid', p);
			var sonum = nlapiGetLineItemValue('order', 'ordernumber', p);
			nlapiLogExecution('ERROR','soid',soid);
			var trantype= nlapiLookupField('transaction',soid,'recordType');
			//nlapiLogExecution('ERROR','trantype',trantype);
			if(trantype=='salesorder')
			{
				nlapiLogExecution('ERROR','into IF');
				var QtyRemaining = nlapiGetLineItemValue('order', 'quantityremaining', p);
				var QtyCommit = nlapiGetLineItemValue('order', 'quantitycommitted', p);
				var orderline = nlapiGetLineItemValue('order', 'orderline', p);

				nlapiLogExecution('ERROR','QtyCommit',QtyRemaining);
				nlapiLogExecution('ERROR','QtyCommit',QtyCommit);
				nlapiLogExecution('ERROR','orderline',orderline);
				//if(parseFloat(QtyCommit) < parseFloat(QtyRemaining))
				//{
				var salessearchfilters = new Array(); 
				salessearchfilters.push(new nlobjSearchFilter('custrecord_ns_ord', null, 'anyof', soid));
				salessearchfilters.push(new nlobjSearchFilter('custrecord_ebiz_linesku', null, 'anyof', vItem));
				salessearchfilters.push(new nlobjSearchFilter('custrecord_ordline', null, 'equalto', parseInt(orderline)));

				var columns=new Array();
				columns.push(new nlobjSearchColumn('custrecord_lineord'));
				columns.push(new nlobjSearchColumn('custrecord_ordline'));
				columns.push(new nlobjSearchColumn('custrecord_linestatus_flag'));	
				columns.push(new nlobjSearchColumn('custrecord_ebiz_linesku'));
				columns.push(new nlobjSearchColumn('custrecord_ord_qty'));						
				var salessearchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, salessearchfilters, columns);
				if(salessearchresults != null && salessearchresults != "")
				{
					nlapiLogExecution('ERROR','salessearchresults.length',salessearchresults.length);
					for(var k=0;k<salessearchresults.length;k++)
					{						
						var vWMSStatus=salessearchresults[k].getValue('custrecord_linestatus_flag');
						var vComQtyFulfillment = salessearchresults[k].getValue('custrecord_ord_qty');
						var vlinesku = salessearchresults[k].getValue('custrecord_ebiz_linesku');

						nlapiLogExecution('ERROR','status',vWMSStatus);
						nlapiLogExecution('ERROR','line sku',vlinesku);
						nlapiLogExecution('ERROR','Ord Qty',vComQtyFulfillment);
						nlapiLogExecution('ERROR','QtyCommit',QtyCommit);

						if(parseFloat(QtyCommit) <= parseFloat(vComQtyFulfillment) && vWMSStatus=='25')
						{
							vFOtoDeleteCount = vFOtoDeleteCount + 1;
							vArrayFOinternalId.push(salessearchresults[k].getId()); 
						}
						else if( vWMSStatus!='25' && parseFloat(QtyCommit)!=parseFloat(vComQtyFulfillment))
						{
							var ctx = nlapiGetContext();
							var exeCtx = ctx.getExecutionContext();
							//if(type == 'delete')
							{
								if(vMessage!= null && vMessage != "")
								{
									vMessage = vMessage + "," + sonum;
								}
								else
									vMessage = sonum;
							}
						} 
					}

					nlapiLogExecution('ERROR','vMessage',vMessage);
					if(vMessage != null && vMessage!='')
					{ 
						var output = 'type = ' + type + '<br>';
						output = output + 'execution context = ' + exeCtx + '<br>';
						output = output + 'custom form = ' + nlapiGetFieldValue('customform') + '<br>';
						output = output + 'role = ' + nlapiGetRole();
						nlapiLogExecution('DEBUG', 'output', output);

						if(nlapiGetFieldValue('customform') != null || exeCtx == 'userinterface' || exeCtx == 'userevent')
						{
							nlapiLogExecution('DEBUG', 'creating error object', 'creating error object');
							var cannotDelError = nlapiCreateError('CannotDelete', vMessage + ' order(s) cannot be reallocate because order(s) already picked.', true);
							throw cannotDelError; //throw this error object, do not catch it
						}
					}
					else if(vFOtoDeleteCount>0)
					{
						nlapiLogExecution('ERROR','Deleting FO Count  ',vArrayFOinternalId.length);
						for(var s=0;s<vArrayFOinternalId.length;s++)
						{
							nlapiLogExecution('ERROR','Deleted FO ',vArrayFOinternalId[s]);
							var id = nlapiDeleteRecord('customrecord_ebiznet_ordline', vArrayFOinternalId[s]);
						}
					}

				}
				//}


			}
		}
	} 

	//return false;
}	