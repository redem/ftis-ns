/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_LoadTrailerQb_SL.js,v $
 *     	   $Revision: 1.6.4.1.4.2.4.2.6.2 $
 *     	   $Date: 2015/03/24 14:45:37 $
 *     	   $Author: grao $
 *     	   $Name: t_eBN_2015_1_StdBundle_1_23 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_LoadTrailerQb_SL.js,v $
 * Revision 1.6.4.1.4.2.4.2.6.2  2015/03/24 14:45:37  grao
 * LP SB issue fixes  201412176
 *
 * Revision 1.6.4.1.4.2.4.2.6.1  2014/11/05 15:43:33  schepuri
 * 201410439 issue fix
 *
 * Revision 1.6.4.1.4.2.4.2  2013/04/09 13:21:16  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related to display no records
 *
 * Revision 1.6.4.1.4.2.4.1  2013/04/03 01:48:10  kavitha
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.6.4.1.4.2  2012/11/07 06:31:38  mbpragada
 * 20120490
 *
 * Revision 1.6.4.1.4.1  2012/11/01 14:55:02  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.6.4.1  2012/08/10 14:47:08  spendyala
 * CASE201112/CR201113/LOG201121
 * Added functionailty to fetch more than 1000 records
 * in wave and order query block
 *
 * Revision 1.6  2011/09/26 08:47:27  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.5  2011/08/31 10:36:13  mbpragada
 * CASE201112/CR201113/LOG201121
 * Completed Load/Unload Trailer Logic.
 *
 * Revision 1.4  2011/07/21 05:02:54  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 * Revision 1.3  2011/07/21 05:02:24  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/

/**
 * This function is to fill the wave numbers in the dropdown of the query block.
 * @param form
 * @param WaveField
 */
function fillWaveNumbers(form, WaveField,maxno){
	var waveFilters = new Array();
	waveFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '4')); //Ship task
	waveFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['7','28'])); //status 'B'
	
	if(maxno!=-1)
	{
		waveFilters.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno)));
	}
	var waveSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, waveFilters, new nlobjSearchColumn('custrecord_ebiz_wave_no'));

	for (var i = 0; waveSearchResults != null && i < waveSearchResults.length; i++) {
		var res = form.getField('custpage_wave').getSelectOptions(waveSearchResults[i].getValue('custrecord_ebiz_wave_no'), 'is');
		if (res != null) {
			if (res.length > 0) {
				continue;
			}
		}
		WaveField.addSelectOption(waveSearchResults[i].getValue('custrecord_ebiz_wave_no'), waveSearchResults[i].getValue('custrecord_ebiz_wave_no'));
	}
	if(waveSearchResults!=null && waveSearchResults.length>=1000)
	{
		var maxno=waveSearchResults[waveSearchResults.length-1].getValue('id');		
		fillWaveNumbers(form, WaveField,maxno);	
	}
}

/**
 * This function is to fill the fulfillment order numbers in the dropdown of the query block.
 * @param form
 * @param fulfillmentField
 */
var fulfillmentSearchResults=new Array();
function fillFulfillmentOrders(form, fulfillmentField,maxno){
	var fulfillmentOrderFilters = new Array();
	fulfillmentOrderFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3')); //Ship task
	fulfillmentOrderFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['7','28'])); //status 'B'
	if(maxno!=-1)
	{
		fulfillmentOrderFilters.push(new nlobjSearchFilter('id', null, 'lessthan', parseInt(maxno)));
	}
	var columns=new Array();
	columns[0]=new nlobjSearchColumn('name');
	columns[1]=new nlobjSearchColumn('id');
	columns[1].setSort(true);
		var SearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, fulfillmentOrderFilters, columns);
		var maxno=SearchResults[SearchResults.length-1].getId();
	var fulfillmentOrderSearchResults=unique(SearchResults,fulfillmentSearchResults);
	
	if(SearchResults!=null && SearchResults.length>=1000)
	{
			
		fillFulfillmentOrders(form, fulfillmentField,maxno);	
	}
	else
		{
		for (var i = 0; fulfillmentOrderSearchResults != null && i < fulfillmentOrderSearchResults.length; i++) {
			var res = form.getField('custpage_fulfillmentorder').getSelectOptions(fulfillmentOrderSearchResults[i].getValue('name'), 'is');
			if (res != null) {
				if (res.length > 0) {
					continue;
				}
			}
			fulfillmentField.addSelectOption(fulfillmentOrderSearchResults[i].getValue('name'), fulfillmentOrderSearchResults[i].getValue('name'));
		}
		}
}

/**
 * This function is to fill the ship lp numbers in the dropdown of the query block.
 * @param form
 * @param shipLpField
 */
function fillShipLP(form, shipLpField){
	var shipLPFilers = new Array();
	shipLPFilers.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lptype', null, 'is', '3')); //Ship task
	shipLPFilers.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_wmsstatusflag', null, 'anyof', ['7','28'])); //Status 'B'
	shipLpField.addSelectOption("", "");
	
	var shipLPSearchResults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, shipLPFilers, new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp'));

	for (var i = 0; shipLPSearchResults != null && i < shipLPSearchResults.length; i++) {
		var resdo = form.getField('custpage_shiplp').getSelectOptions(shipLPSearchResults[i].getValue('custrecord_ebiz_lpmaster_lp'), 'is');
		if (resdo != null) {
			if (resdo.length > 0) {
				continue;
			}
		}
		shipLpField.addSelectOption(shipLPSearchResults[i].getValue('custrecord_ebiz_lpmaster_lp'), shipLPSearchResults[i].getValue('custrecord_ebiz_lpmaster_lp'));
	}	 
}
/**
 * This function is to remove the duplicate fullfillment orders from the  searchresult and return the unique array.
 * 	The parameters are the fullfillmentordersearchresult and the unique fullfillmentordersearchresult.
 */
function unique(arr,outarray) {
    //var a = [];
    var l = arr.length;
    for(var i=0; i<l; i++) {
        for(var j=i+1; j<l; j++) {
            // If a[i] is found later in the array
            if (arr[i].getValue('name') === arr[j].getValue('name'))
              j = ++i;
        }
        outarray[outarray.length]=(arr[i]);
    }
    return outarray;
};
/**
 * This function is the main function which call the fill functions.
 * 	The parameters are the request and the response.
 * @param request
 * @param response
 */
function LoadTrailerQb(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Load Trailer');

		//wave number	
		var WaveField = form.addField('custpage_wave', 'select', 'Wave');
		WaveField.setLayoutType('startrow', 'none');
		WaveField.addSelectOption("", "");
		fillWaveNumbers(form, WaveField,-1);
		
		
		//fulfillment order
		var fulfillmentField = form.addField('custpage_fulfillmentorder', 'select', 'Fulfillment Order');
		fulfillmentField.setLayoutType('startrow', 'none');
		fulfillmentField.addSelectOption("", "");
		fillFulfillmentOrders(form, fulfillmentField,-1);
		
		//	ship lp
		var shipLpField = form.addField('custpage_shiplp', 'select', 'Ship LP #');
		shipLpField.setLayoutType('startrow', 'none');
		fillShipLP(form, shipLpField);
		
			//var carrier = form.addField('custpage_carrier', 'select', 'Carrier','customrecord_ebiznet_carrier');
		var shippingcarrier = form.addField('custpage_carrier', 'select', 'Carrier');
		shippingcarrier.setLayoutType('startrow', 'none');

		var record = nlapiCreateRecord('salesorder', {recordmode: 'dynamic'});
		var subs = nlapiGetContext().getFeature('subsidiaries');
		if(subs==true)
		{
			var loc='';
			var vSubsidiaryVal=getSubsidiaryNew(loc);
			nlapiLogExecution('ERROR','vSubsidiaryVal',vSubsidiaryVal);
			if(vSubsidiaryVal != null && vSubsidiaryVal != '')
			//record.setFieldValue('subsidiary', 1);
				record.setFieldValue('subsidiary', vSubsidiaryVal);
		}
		var methodsField = record.getField('shipmethod');
		var methodOptions = methodsField.getSelectOptions(null, null);
		nlapiLogExecution('ERROR','methodOptions',methodOptions);
		nlapiLogExecution('ERROR','methodOptions.length',methodOptions.length);

		shippingcarrier.addSelectOption("",""); 

		for (var i = 0; methodOptions != null && i < methodOptions.length; i++) {
			nlapiLogExecution('ERROR', methodOptions[i].getId() + ',' + methodOptions[i].getText() );
			shippingcarrier.addSelectOption(methodOptions[i].getId(),methodOptions[i].getText());
		}
		
		
		
		/*var Destination = form.addField('custpage_dest', 'select', 'Destination','customlist_nswmsdestinationlov');
		Destination.setLayoutType('startrow', 'none');
		var Route = form.addField('custpage_route', 'select', 'Route# / Shipment#');
		*/
		
		
		
		form.addSubmitButton('Display');
		response.writePage(form);
	}
	else{
		var vwave = request.getParameter('custpage_wave');
		var vfulfillment = request.getParameter('custpage_fulfillmentorder');
		var vshiplp = request.getParameter('custpage_shiplp');
		var vcarrier = request.getParameter('custpage_carrier');
		var vdest = request.getParameter('custpage_dest');
		var vroute = request.getParameter('custpage_route');
		
		var loadTrailerArray = new Array();
		loadTrailerArray["custpage_wave"] = vwave;
		loadTrailerArray["custpage_fulfillmentorder"] = vfulfillment;
		loadTrailerArray["custpage_shiplp"] = vshiplp;		 
		loadTrailerArray["custpage_carrier"] = vcarrier;	
		loadTrailerArray["custpage_dest"] = vdest;	
		loadTrailerArray["custpage_route"] = vroute;	
		response.sendRedirect('SUITELET', 'customscript_loadunloadtrailer', 'customdeploy_loadunloadtrailer', false, loadTrailerArray);
	}
}