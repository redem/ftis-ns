/***************************************************************************
  eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/ebiz_KitToStock_SL.js,v $
 *     	   $Revision: 1.6.4.8.4.1.4.4 $
 *     	   $Date: 2015/09/02 15:31:40 $
 *     	   $Author: grao $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_KitToStock_SL.js,v $
 * Revision 1.6.4.8.4.1.4.4  2015/09/02 15:31:40  grao
 * 2015.2   issue fixes 201414213
 *
 * Revision 1.6.4.8.4.1.4.3  2014/08/04 09:10:57  nneelam
 * case#  20149371
 * Stanadard Bundle Partial WO Issue Fix.
 *
 * Revision 1.6.4.8.4.1.4.2  2014/06/13 15:33:38  sponnaganti
 * case# 20148583
 * Stnd Bundle Issue Fix
 *
 * Revision 1.6.4.8.4.1.4.1  2013/03/08 14:43:37  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.6.4.8.4.1  2012/10/26 14:00:51  skreddy
 * CASE201112/CR201113/LOG201121
 * Changed the custom id of Qc stautus
 *
 * Revision 1.6.4.8  2012/08/15 16:26:05  mbpragada
 * 20120490
 * Issue fix as a part of 339 bundle
 *
 * Revision 1.6.4.7  2012/06/07 15:55:04  gkalla
 * CASE201112/CR201113/LOG201121
 * Added inbound functionality to main item using put strategies
 *
 * Revision 1.6.4.6  2012/04/30 11:38:54  spendyala
 * CASE201112/CR201113/LOG201121
 * While Searching of Item in ItemMaster,
 * 'name' filter is changed to 'nameinternal' and also checking weather the item is inactive state or not.
 *
 * Revision 1.6.4.5  2012/04/12 17:18:00  gkalla
 * CASE201112/CR201113/LOG201121
 * Added Lot# validation
 *
 * Revision 1.6.4.4  2012/04/10 22:41:15  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue at vertical
 *
 * Revision 1.11  2012/04/10 22:33:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.10  2012/02/10 07:31:58  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * kitting
 *
 * Revision 1.9  2012/01/31 12:42:28  gkalla
 * CASE201112/CR201113/LOG201121
 * Work order Bin location issues fixed
 *
 * Revision 1.8  2012/01/20 12:08:10  gkalla
 * CASE201112/CR201113/LOG201121
 * Allocation
 *
 * Revision 1.7  2012/01/20 11:37:16  gkalla
 * CASE201112/CR201113/LOG201121
 * Allocation
 *
 * Revision 1.6  2011/10/31 14:44:38  gkalla
 * CASE201112/CR201113/LOG201121
 * To get only Main Item details in WO dropdown
 *
 * Revision 1.5  2011/10/11 18:19:24  mbpragada
 * CASE201112/CR201113/LOG201121
 * inserting sku status and packcode into create inventory and
 * generated lp automatically
 *
 * Revision 1.4  2011/09/30 08:54:41  sallampati
 * CASE2011263/CR2011260/LOG2011188
 * The version code 1.1.1.1
 *  is merged with 1.3. This will be the latest with kit to order.
 *
 * Revision 1.3  2011/07/21 05:25:54  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/
/*
 * Processing HTTP GET Request
 */
function addAllWorkOrdersToField(workorder, workorderList,form){
	nlapiLogExecution('ERROR', 'Status ', 'Hi');
	if(workorderList != null && workorderList.length > 0){
		for (var i = 0; i < workorderList.length ; i++) {	
			nlapiLogExecution('ERROR', 'Status ', workorderList[i].getValue('status'));
			var res=  form.getField('custpage_workorder').getSelectOptions(workorderList[i].getValue('tranid'), 'is');
			if (res != null) {
				if (res.length > 0) {
					continue;
				}
			}
			workorder.addSelectOption(workorderList[i].getValue('internalid'), workorderList[i].getValue('tranid'));
//			for(var i = 0; i < workorderList.length; i++){
//			workorder.addSelectOption(workorderList[i].getValue('internalid'), workorderList[i].getValue('tranid'));
//			}
		}
	}

}

function getAllWorkOrders(){

	var filters = new Array();
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('status', null, 'anyof', ['WorkOrd:B','WorkOrd:D']));
	//case# 20148583 starts
	var vRoleLocation = getRoledBasedLocationNew();
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		filters.push(new nlobjSearchFilter('location', null, 'anyof', vRoleLocation));
	}
	//case# 20148583 ends
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('tranid');
	columns[1] = new nlobjSearchColumn('internalid');
	//columns[2] = new nlobjSearchColumn('status');
	columns[0].setSort(true);	
	var searchResults = nlapiSearchRecord('workorder', null, filters, columns);
	//nlapiLogExecution('ERROR', 'Status', 'Hi3');
	return searchResults;
}
function kitToOrderGETRequest(request, response){
	nlapiLogExecution('ERROR', 'kitToOrderGETRequest Start', 'kitToOrderGETRequest Start');
	//create the Suitelet form
	var form = nlapiCreateForm('Confirm Assembly Build');
	var vSOId = "";
	var vSOName = "";
	var vSOLocation = "";
	var vSOLineItem = "";
	var vSOLineItemName = "";
	var vSOLineNum = "";
	var vSOLineQty = "";
	var vSOCommitedLineQty = "";
	var vSOFulfilledLineQty = "";

	vSOId = request.getParameter('custparam_soid');
	vSOName = request.getParameter('custparam_soname');
	vSOLocation = request.getParameter('custparam_soloc');
	vSOLineItem = request.getParameter('custparam_lneitem');
	vSOLineItemName = request.getParameter('custparam_lneitxt');
	vSOLineNum = request.getParameter('custparam_lneno');
	vSOLineQty = request.getParameter('custparam_lneqty');


	var WorkOrder  = form.addField('custpage_workorder', 'select', 'Work Order').setLayoutType('startrow', 'none').setMandatory(true);
	WorkOrder.addSelectOption("","");

	// Retrieve all sales orders
	nlapiLogExecution('ERROR', 'Status', 'Hi4');
	var WorkOrderList = getAllWorkOrders();
	nlapiLogExecution('ERROR', 'Status', 'Hi2');
	// Add all sales orders to SO Field
	addAllWorkOrdersToField(WorkOrder, WorkOrderList,form);

	var soDate = form.addField('custpage_sodate', 'date', 'Date');
	soDate.setLayoutType('normal', 'startrow');
	soDate.setDefaultValue(nlapiDateToString(new Date()));

	var item = form.addField('cusppage_item', 'select', 'Kit Item', 'custom');

	/*var itemData = nlapiSearchRecord('item', null, new nlobjSearchFilter('type', null, 'is', 'Assembly'),
			[new nlobjSearchColumn('internalid'), new nlobjSearchColumn('itemid')]);*/

	// Changed On 30/4/12 by Suman

	var filteritem=new Array();
	filteritem.push(new nlobjSearchFilter('type', null, 'is', 'Assembly'));
	filteritem.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
	var columnsitem = new Array();
	columnsitem[0] = new nlobjSearchColumn('internalid');
	columnsitem[1] = new nlobjSearchColumn('itemid');
	var itemData = nlapiSearchRecord('item', null,filteritem,columnsitem);

	// End of Changes as On 30/4/12


	item.addSelectOption('select','Select');
	for(i=0; itemData!=null && i<itemData.length; i++){
		item.addSelectOption(itemData[i].getValue("internalid"), itemData[i].getValue("itemid"));
	}

	nlapiLogExecution('ERROR','vSolineitem '+ vSOLineItem);
	item.setDefaultValue(vSOLineItem);
	item.setLayoutType('normal', 'startrow');

	var kitQty = form.addField('custpage_kitqty', 'text', 'Qty');
	if(vSOLineQty != null && vSOLineQty != "")
		kitQty.setDefaultValue(vSOLineQty);


	kitQty.setLayoutType('normal', 'startrow');



	var doField = form.addField('custpage_refno', 'text', 'Ref#');        
	doField.setLayoutType('normal', 'startrow');
	doField.setDefaultValue(vSOName);


	var site = form.addField('cusppage_site', 'select', 'Location', 'location');
	site.setLayoutType('normal', 'startrow');
	site.setDefaultValue(vSOLocation);

	var soInternalNum = form.addField('custpage_sointernno', 'text', 'sono');
	soInternalNum.setDisplayType('hidden');
	soInternalNum.setDefaultValue(vSOId); 

	var chknCompany = form.addField('custpage_company', 'select', 'Company', 'customrecord_ebiznet_company');
	form.addSubmitButton('Get Assembly Build Details');  
	response.writePage(form);
}

/*
 * Processing HTTP POST Request
 */
function kitToOrderPOSTRequest(request, response){
	nlapiLogExecution('DEBUG', 'kitToOrderPostRequest Start', 'kitToOrderPostRequest Start');
	var form = nlapiCreateForm('Confirm Assembly Build');
	var item = request.getParameter('cusppage_item');		
	var location =request.getParameter('cusppage_site');
	var qty = request.getParameter('custpage_kitqty');
	var vDate = request.getParameter('custpage_sodate');
	var vRefNo = request.getParameter('custpage_refno');
	var vSOInternalNo = request.getParameter('custpage_sointernno');
	var vRefCompany = request.getParameter('custpage_company');	
	var vWorkOrder = request.getParameter('custpage_workorder');

	nlapiLogExecution('ERROR','vWorkOrder ',vWorkOrder);
	var kitArray = new Array();
	kitArray["custparam_item"] = item;
	kitArray["custparam_locaiton"] = location;
	if(qty!= null && qty !="")
		kitArray["custpage_kitqty"] = parseFloat(qty);
	kitArray["custpage_sodate"] = vDate;
	kitArray["custpage_company"] = vRefCompany;		
	kitArray["custpage_sointernalid"] = vSOInternalNo;	
	kitArray["custpage_workorder"] = vWorkOrder;

	nlapiLogExecution('ERROR', 'woidno',vWorkOrder);
	var rcptrecordid3 = '';
	var vWOTex = '';
	var validLocation='';
	if(vWorkOrder!=null && vWorkOrder!='')
	{
		rcptrecordid3 = nlapiLoadRecord('workorder', vWorkOrder);
		vWOText= rcptrecordid3.getFieldValue('tranid');
		validLocation = rcptrecordid3.getFieldValue('location');
		
	}
	nlapiLogExecution('ERROR', 'vWOText',vWOText);
	nlapiLogExecution('ERROR', 'validLocation',validLocation);
	nlapiLogExecution('ERROR', 'location',location);
	
	
	if (validLocation!=null&&validLocation!=''&&(validLocation!=location)&&(location!=null&&location!='')){// case# 201416971
		nlapiLogExecution('ERROR','invalid location');
		var ErrorMsg = 'INVALID LOCATION';
		showInlineMessage(form, 'Error', '' , ErrorMsg);
		response.writePage(form);
		return false;
	}
	
	
	
	
	var filter=new Array();


	filter.push(new nlobjSearchFilter('custrecord_ebiz_order_no','null','is',vWorkOrder));

	var searchrecord=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filter,null);
	if(searchrecord == null || searchrecord == "")
	{
		nlapiLogExecution('ERROR','Bin locations are not yet generated for this Work order# '+ vWOText + '. Please generate bin locations first then perform Assembly Build');
		var ErrorMsg = 'Bin locations are not yet generated for this Work order# '+ vWOText + '. Please generate bin locations first then perform Assembly Build';

		showInlineMessage(form, 'Error', '' , ErrorMsg);
		response.writePage(form);
		return false;

		//throw ErrorMsg; //throw this error object, do not catch it		
	}	
	else
	{
		response.sendRedirect('SUITELET', 'customscript_kittostockdtls_suitelet',
				'customdeploy_kittostockdtls_suitelet_di', false, kitArray);
	}

}

function kitToStockSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		kitToOrderGETRequest(request, response);
	} else {
		//code added by santosh on 24Aug12
		var form = nlapiCreateForm('Confirm Assembly Build');
		var woidno= request.getParameter('custpage_workorder');
		var searchresults = '';
		var vWOId = '';
		var ApproveFlag = '';
		if(woidno!=null && woidno!='')
		{
			searchresults = nlapiLoadRecord('workorder', woidno);
			vWOId=searchresults.getFieldValue('tranid');
			ApproveFlag=searchresults.getFieldValue('custbody_ebiz_wo_qc_status');
		}
		nlapiLogExecution('ERROR','Approve flag ',ApproveFlag);
		if(ApproveFlag=='2')//if Approved
		{
			kitToOrderPOSTRequest(request, response);
		}
		else//Pending
		{
			nlapiLogExecution('ERROR','Approve flag ',ApproveFlag);
			//var ErrorMsg = nlapiCreateError('CannotAllocate','Please Approve this Work Order:' + vWOId, true);
			//throw ErrorMsg; //throw this error object, do not catch it
			showInlineMessage(form, 'Error','Please Approve this Work order ' + vWOId + '', '');
			response.writePage(form);
		}
		//end of the code on 24Aug12
	}    
}





