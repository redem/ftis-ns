/***************************************************************************
	  		   eBizNET Solutions Inc               
****************************************************************************/
/* 
*     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Client/Attic/ebiz_ContainerExceptions_CL.js,v $
*     	   $Revision: 1.1.2.1 $
*     	   $Date: 2012/08/08 15:19:10 $
*     	   $Author: rrpulicherla $
*
*   eBizNET version and checksum stamp.  Do not remove.
*   $eBiznet_VER: .............. $eBizNET_SUM: .....
* 
* PRAMETERS
*
* DESCRIPTION
* 
* REVISION HISTORY
* $Log: ebiz_ContainerExceptions_CL.js,v $
* Revision 1.1.2.1  2012/08/08 15:19:10  rrpulicherla
* CASE201112/CR201113/LOG201121
* Kit to order
*
* Revision 1.4.2.1  2012/02/01 09:15:47  spendyala
* CASE201112/CR201113/LOG201121
* Passed one more parameter i.e., loc id to the function ebiznet_LPRange_CL.
*
* Revision 1.4  2011/12/12 13:04:13  snimmakayala
* CASE201112/CR201113/LOG201121
* spelling corrections in success message
*
* Revision 1.3  2011/08/25 14:16:50  mbpragada
* CASE201112/CR201113/LOG201121
* Completed Container Management Logic.
* Modularized the Container Management Logic.
*
* Revision 1.2  2011/08/24 15:04:25  mbpragada
* CASE201112/CR201113/LOG201121
* Completed Container Management Logic.
*
*****************************************************************************/

/**
 *  * This is the main function it checks whether the given LP is valid or not.

 */

function containerExceptions_CL()
{
	try
	{
		var LineCount= nlapiGetLineItemCount('custpage_items');
		var newContainer="";
		var newContainerSize="";
		var newQty= "";
		var internalId=""; 
		var flag="";

		for(var s=1;s <=LineCount;s++){
			flag= nlapiGetLineItemValue('custpage_items', 'custpage_select', s);
			if(flag == "T"){
				binlocation = nlapiGetLineItemValue('custpage_items', 'custpage_binlocation', s);
				lp= nlapiGetLineItemValue('custpage_items', 'custpage_paltlp', s);
				if(binlocation==null || binlocation=='')
				{
					alert('Please Select Binlocation');
					return false;
				}
				if(lp==null || lp=='')
				{
					alert('Please Enter LP');
					return false;
				}
				var LPValidate = ebiznet_LPRange_CL(lp, '2','');//ebiznet_LPRange_CL(lpno, type, loc)
				
				if(containerLPValidate){

					return true;
				}
				else{
					alert('INVALID LP');
					return false;
				}
			}
		}
	}
	catch(exp)
	{
		alert('Exception'+exp);
	}
	
	//return true;
}