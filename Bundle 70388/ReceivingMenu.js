/***************************************************************************
 eBizNET Solutions Inc                
 ***************************************************************************
 **     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Inbound/Suitelet/ReceivingMenu.js,v $
 *     	   $Revision: 1.4.4.9.4.5.4.18.2.2 $
 *     	   $Date: 2014/09/10 15:49:57 $
 *     	   $Author: sponnaganti $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ReceivingMenu.js,v $
 * Revision 1.4.4.9.4.5.4.18.2.2  2014/09/10 15:49:57  sponnaganti
 * Case# 201410277
 * Stnd Bundle issue fixed
 *
 * Revision 1.4.4.9.4.5.4.18.2.1  2014/08/06 07:45:46  rmukkera
 * Case # 20149847
 *
 * Revision 1.4.4.9.4.5.4.18  2014/06/13 05:50:05  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.4.4.9.4.5.4.17  2014/06/12 15:24:47  skavuri
 * Case # 20148880 (LinkButton functionality added to options in RF Screens)
 *
 * Revision 1.4.4.9.4.5.4.16  2014/05/30 00:26:51  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.4.4.9.4.5.4.15  2014/04/28 09:24:01  skavuri
 * Case# 20148187 issue fixed
 *
 * Revision 1.4.4.9.4.5.4.14  2014/04/25 13:41:02  nneelam
 * case#  20148169
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.4.4.9.4.5.4.13  2014/04/24 16:01:57  nneelam
 * case#  20148169
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.4.4.9.4.5.4.12  2014/04/23 09:35:22  skavuri
 * Case# 20148136 issue fixed
 *
 * Revision 1.4.4.9.4.5.4.11  2014/04/22 14:56:16  grao
 *  LL Sb RMA CARTChekin enhancement case#:20148087
 *
 * Revision 1.4.4.9.4.5.4.10  2014/04/22 14:54:49  grao
 *  LL Sb RMA CARTChekin enhancement case#:20148087
 *
 * Revision 1.4.4.9.4.5.4.9  2014/02/25 23:58:00  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20127387
 *
 * Revision 1.4.4.9.4.5.4.8  2014/02/13 15:15:17  sponnaganti
 * case# 20127056
 * For Keyboard Enter button to work
 *
 * Revision 1.4.4.9.4.5.4.7  2013/12/02 08:56:07  schepuri
 * 20125938
 *
 * Revision 1.4.4.9.4.5.4.6  2013/07/25 08:55:24  rmukkera
 * landed cost cr changes
 *
 * Revision 1.4.4.9.4.5.4.5  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.4.4.9.4.5.4.4  2013/05/09 15:22:40  skreddy
 * CASE201112/CR201113/LOG201121
 * changed the Build Carton name to Build Cart
 *
 * Revision 1.4.4.9.4.5.4.3  2013/04/17 16:04:01  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.4.4.9.4.5.4.2  2013/03/15 09:28:43  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.4.4.9.4.5.4.1  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.4.4.9.4.5  2012/09/27 13:13:26  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.4.4.9.4.4  2012/09/27 10:55:14  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.4.4.9.4.3  2012/09/26 12:48:52  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.4.4.9.4.2  2012/09/25 11:52:00  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.4.4.9.4.1  2012/09/21 15:09:07  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.4.4.9  2012/09/14 09:20:36  grao
 * no message
 *
 * Revision 1.4.4.4  2012/04/24 14:51:49  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Menu name changes
 *
 * Revision 1.4.4.3  2012/04/17 10:59:57  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Checkin TO
 *
 * Revision 1.4.4.2  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.4.4.1  2012/02/22 12:44:36  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.7  2012/02/17 13:38:30  schepuri
 * CASE201112/CR201113/LOG201121
 * Added CVS Header
 *
 * Revision 1.3  2012/02/17 13:32:31  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 */
function ReceivingMenu(request, response){
	if (request.getMethod() == 'GET') {
		/*                var html = "<html><head><title>RECEIVING MENU</title> ";				
		 html = html + " <style>";
         html = html + ".img";
         html = html + "{";
             html = html + "width: 100%;";
             html = html + "position: absolute;";
             html = html + "top: 0;";
             html = html + "left: 0;";             
			 html = html + " background-image: url('https://system.netsuite.com/core/media/media.nl?id=1598&c=TSTDRV770123&h=f0861194ff8ec54c2ac3');";
             html = html + "background-repeat: no-repeat;";
         html = html + "}";
         html = html + ".style1";
         html = html + "{";
             html = html + "height: 25px;";
             html = html + "width: 319px;";
         html = html + "}";
         html = html + ".style2";
        html = html + " {";
             html = html + "height: 29px;";
             html = html + "width: 319px;";
         html = html + "}";
         html = html + ".style3";
         html = html + "{";
             html = html + "height: 24px;";
             html = html + "width: 319px;";
         html = html + "}";
         html = html + ".style4";
         html = html + "{";
             html = html + "height: 19px;";
             html = html + "width: 319px;";
         html = html + "}";
         html = html + ".style6";
         html = html + "{";
             html = html + "height: 439px;";
             html = html + "width: 319px;";
         html = html + "}";
         html = html + ".style7";
         html = html + "{";
             html = html + "height: 15px;";
             html = html + "width: 319px;";
         html = html + "}";
         html = html + ".style8";
         html = html + "{";
             html = html + "height: 67px;";
             html = html + "width: 319px;";
         html = html + "}";
         html = html + ".style9";
         html = html + "{";
             html = html + "width: 83px;";
         html = html + "} ";
     html = html + "</style> </head>";
        html = html + "<body>";
        html = html + "	<form name='_rfreceivingmenu' method='POST'>";

	  html = html + "		 <table id='tbl' width='100%' style='border-right: gray thin inset; border-top: gray thin inset; ";
         html = html + "		border-left: gray thin inset; width: 11.5cm; border-bottom: gray thin inset; ";
         html = html + "		height: 22.85cm' class='img'> ";
         html = html + "		<tr> ";
             html = html + "		<td> ";
                 html = html + "		<table> ";
                     html = html + "		<tr> ";
                         html = html + "		<td class='style9'> ";
                         html = html + "		</td> ";
                         html = html + "		<td align='left' class='style1'> ";
                             html = html + "		1. CHECK-IN BY PALLET ";
                         html = html + "		</td> ";
                     html = html + "		</tr> ";
                     html = html + "		<tr> ";
                         html = html + "		<td class='style9'> ";
                         html = html + "		</td> ";
                         html = html + "		<td align='left' class='style2'> ";
                             html = html + "		2. PUTAWAY BY PALLET ";
                         html = html + "		</td> ";
                     html = html + "		</tr> ";
                     html = html + "		<tr> ";
                         html = html + "		<td class='style9'> ";
                         html = html + "		</td> ";
                         html = html + "		<td align='left' class='style3'> ";
                             html = html + "		3. CHECK-IN BY CART ";
                         html = html + "		</td> ";
                     html = html + "		</tr> ";
                     html = html + "		<tr> ";
                         html = html + "		<td class='style9'> ";
                         html = html + "		</td> ";
                         html = html + "		<td align='left' class='style4'> ";
                             html = html + "		4. PUTAWAY BY CART ";
                         html = html + "		</td> ";
                     html = html + "		</tr> ";
                     html = html + "		<tr> ";
                         html = html + "		<td class='style9'> ";
                         html = html + "		</td> ";
                         html = html + "		<td align='left' class='style4'> ";
                             html = html + "		ENTER SELECTION ";
                         html = html + "		</td> ";
                     html = html + "		</tr> ";
                     html = html + "		<tr> ";
                         html = html + "		<td class='style9'> ";
                         html = html + "		</td> ";
                         html = html + "		<td align='left' class='style7'> ";
                             html = html + "		&nbsp;<input name='selectoption' type='text' /> ";
                         html = html + "		</td> ";
                     html = html + "		</tr> ";
                     html = html + "		<tr> ";
                         html = html + "		<td class='style9'> ";
                         html = html + "		</td> ";
                         html = html + "		<td align='left' class='style8'> ";
                             html = html + "		SEND ";
                             html = html + "		<input id='cmdSend' type='submit' value='ENT' /> ";
                             html = html + "		PREV";
                             html = html + "		<input name='cmdPrevious' type='submit' value='F7' /> ";
                         html = html + "		</td> ";
                     html = html + "		</tr> ";
                     html = html + "		<tr> ";
                         html = html + "		<td class='style9'> ";
                         html = html + "		</td> ";
                         html = html + "		<td align='left' class='style6'> ";
                             html = html + "		&nbsp; ";
                         html = html + "		</td> ";
                     html = html + "		</tr> ";
                 html = html + "		</table> ";
             html = html + "		</td> ";
         html = html + "		</tr> ";
         html = html + "		</tr> ";
     html = html + "		</table> "; 
		 */		


		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);


		var st0,st1,st2,st3,st4,st5,st6,st7,st8,st9,st10,st11,st12,st13,st14,st15;

		if( getLanguage == 'es_ES' || getLanguage == 'es_AR')
		{
			st0 = "RECEPCI&#211;N DE MEN&#218;";
			st1 = "RECEPCI&#211;N POR TARIMA ";
			st2 = "GUARDAR POR TARIMA";
			st3 = "RMA LLEGADA POR PALLET";
			st4 = "REGISTRO DE PEDIDO";
			st5 = "GUARDE POR CARRO";
			st6 = "T.O LLEGADA POR PALLET";
			st7 = "T.O LLEGADA POR CARRO";
			st8 = "CHECK-IN EN BOLSA";
			st9 = "INGRESAR SELECCI&#211;N";
			st13 = "RECIBO DEL ART&#205;CULO POR PO";
			st10 = "ENVIAR";
			st11 = "ANTERIOR";	
			st14 = "RECIBO DEL ART&#205;CULO POR ORDER";
			st15 = "RMA LLEGADA POR CARRO"; //case# 201410277
		}
		else
		{
			st0 = "RECEIVING MENU";
			st1 = "CHECK-IN BY PALLET";
			st2 = "PUTAWAY BY PALLET";
			st3 = "RMA CHECK-IN BY PALLET";
			st4 = "CHECK-IN BY CART";
			st5 = "PUTAWAY BY CART";
			st6 = "T .O CHECK-IN BY PALLET";
			st7 = "T .O CHECK-IN BY CART";
			st8 = "BUILD CART";
			st9 = "ENTER SELECTION";
			st10 = "SEND";
			st11 = "PREV";
			st12 = "CHECK-IN BY TOTE";

			st13 = "ITEM RECEIPT BY PO";
			//20125938
			st14 = "ITEM RECEIPT BY TO";
			st15 = "RMA CHECK-IN BY CART";

			//	st13 = "ITEM RECEIPT BY ORDER";
			/*//20125938
			st14 = "ITEM RECEIPT BY TO";*/


		}
		//Case# 20148880 (LinkButton Functionality)
		var checkInURL_1 = nlapiResolveURL('SUITELET', 'customscript_rf_checkin_po', 'customdeploy_rf_checkin_po_di');
		var linkURL_1 = checkInURL_1; 
		var checkInURL_2 = nlapiResolveURL('SUITELET', 'customscript_rf_putaway_lp', 'customdeploy_rf_putaway_lp_di');
		var linkURL_2 = checkInURL_2; 
		var checkInURL_3 = nlapiResolveURL('SUITELET', 'customscript_rf_rma_checkin', 'customdeploy_rf_rma_checkin_di');
		var linkURL_3 = checkInURL_3; 
		var checkInURL_4 = nlapiResolveURL('SUITELET', 'customscript_ebiz_rf_cartcheckin_rma', 'customdeploy_ebiz_rf_cartcheckin_rma_di');
		var linkURL_4 = checkInURL_4; 
		var checkInURL_5 = nlapiResolveURL('SUITELET', 'customscript_ebiz_rf_cartcheckin_po', 'customdeploy_ebiz_rf_cartcheckin_po_di');
		var linkURL_5 = checkInURL_5; 
		var checkInURL_6 = nlapiResolveURL('SUITELET', 'customscript_rf_putaway_cartlp', 'customdeploy_rf_putaway_cartlp_di');
		var linkURL_6 = checkInURL_6; 
		var checkInURL_7 = nlapiResolveURL('SUITELET', 'customscript_rf_to_checkin', 'customdeploy_rf_to_checkin_di');
		var linkURL_7 = checkInURL_7; 
		var checkInURL_8 = nlapiResolveURL('SUITELET', 'customscript_ebiz_rf_cartcheckin_to', 'customdeploy_ebiz_rf_cartcheckin_to_di');
		var linkURL_8 = checkInURL_8; 
		var checkInURL_9 = nlapiResolveURL('SUITELET', 'customscript_rf_buildcarton_lp', 'customdeploy_rf_buildcarton_lp_di');
		var linkURL_9 = checkInURL_9; 
		var checkInURL_10 = nlapiResolveURL('SUITELET', 'customscript_ebiz_rf_postitemreceipt', 'customdeploy_ebiz_rf_postitemreceipt_di');
		var linkURL_10 = checkInURL_10; 

		var functionkeyHtml=getFunctionkeyScript('_rfreceivingmenu'); 
		var html = "<html><head><title>RECEIVING MENU</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//html = html + " document.getElementById('selectoption').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rfreceivingmenu' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 1. " + st1;
		html = html + "				<td align = 'left'> 1. <a href='" + linkURL_1 + "' style='text-decoration: none'>" + st1 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 2. " + st2;
		html = html + "				<td align = 'left'> 2. <a href='" + linkURL_2 + "' style='text-decoration: none'>" + st2 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> ";
		html = html + "			<tr>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 3. " + st3;
		html = html + "				<td align = 'left'> 3. <a href='" + linkURL_3 + "' style='text-decoration: none'>" + st3 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 4. " + st15;
		html = html + "				<td align = 'left'> 4. <a href='" + linkURL_4 + "' style='text-decoration: none'>" + st15 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> ";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 5. " + st4;
		html = html + "				<td align = 'left'> 5. <a href='" + linkURL_5 + "' style='text-decoration: none'>" + st4 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> ";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 6. " + st5;
		html = html + "				<td align = 'left'> 6. <a href='" + linkURL_6 + "' style='text-decoration: none'>" + st5 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> ";    
		html = html + "			<tr>";
	//	html = html + "				<td align = 'left'> 7. " + st6;
		html = html + "				<td align = 'left'> 7. <a href='" + linkURL_7 + "' style='text-decoration: none'>" + st6 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> "; 
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 8. "+ st7;
		html = html + "				<td align = 'left'> 8. <a href='" + linkURL_8 + "' style='text-decoration: none'>" + st7 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> "; 
		html = html + "			<tr>";
	//	html = html + "				<td align = 'left'> 9. " + st8;
		html = html + "				<td align = 'left'> 9. <a href='" + linkURL_9 + "' style='text-decoration: none'>" + st8 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr>";

		html = html + "			<tr>";
	//	html = html + "				<td align = 'left'>10. " + st13;
		html = html + "				<td align = 'left'> 10. <a href='" + linkURL_10 + "' style='text-decoration: none'>" + st13 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr>";

		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st9;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='selectoption' id='selectoption' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>SEND <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		   html = html + "<script type='text/javascript'>document.getElementById('selectoption').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		// This variable is to get the option selected. If option selected is 1, it navigates to
		// the check-in screen. If the option selected is 2, it navigates to the putaway screen.
		var optedField = request.getParameter('selectoption');

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		var POarray = new Array();  

		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;

		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);

		var st12;
		if( getLanguage == 'es_ES')
		{
			st12 = "OPCI&#211;N V&#193;LIDA";	
		}
		else
		{
			st12 = "INVALID OPTION";
		}





		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to RF Main Menu.
		if (optedEvent == 'F7') {
			nlapiLogExecution('DEBUG', 'optedEvent if', optedEvent);
			response.sendRedirect('SUITELET', 'customscript_rf_main_menu', 'customdeploy_rf_main_menu_di', false, POarray);
		}
		else 
			nlapiLogExecution('DEBUG', 'optedEvent else', optedEvent);
		//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
		//            if (optedEvent != '' && optedEvent != null) {

		if(optedEvent != 'F7')
		{
			if (optedField != null) 
			{

				POarray["custparam_option"] = optedField;
				nlapiLogExecution('DEBUG', 'optedField', optedField);
			}

			if (optedField == '1') {
				response.sendRedirect('SUITELET', 'customscript_rf_checkin_po', 'customdeploy_rf_checkin_po_di', false, POarray);
			}
			else 
				if (optedField == '2') {
					response.sendRedirect('SUITELET', 'customscript_rf_putaway_lp', 'customdeploy_rf_putaway_lp_di', false, POarray);
				}
				else 
					if (optedField == '3') {
						response.sendRedirect('SUITELET', 'customscript_rf_rma_checkin', 'customdeploy_rf_rma_checkin_di', false, POarray);
					}
					else 
						if (optedField == '4') {
							//response.sendRedirect('SUITELET', 'customscript_rf_rma_checkin', 'customdeploy_rf_rma_checkin_di', false, POarray);
							POarray["custparam_rmacartcheckin"] = 'RMACART';
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cartcheckin_rma', 'customdeploy_ebiz_rf_cartcheckin_rma_di', false, POarray);
						}
						else 
							if (optedField == '5') {
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cartcheckin_po', 'customdeploy_ebiz_rf_cartcheckin_po_di', false, POarray);
							}
							else 
								if (optedField == '6') {
									response.sendRedirect('SUITELET', 'customscript_rf_putaway_cartlp', 'customdeploy_rf_putaway_cartlp_di', false, POarray);
								}
								else
									if (optedField == '7') {
										response.sendRedirect('SUITELET', 'customscript_rf_to_checkin', 'customdeploy_rf_to_checkin_di', false, POarray);
									}
									else
										if (optedField == '8') {
											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cartcheckin_to', 'customdeploy_ebiz_rf_cartcheckin_to_di', false, POarray);
										}
										else
											if (optedField == '9') {
												response.sendRedirect('SUITELET', 'customscript_rf_buildcarton_lp', 'customdeploy_rf_buildcarton_lp_di', false, POarray);
											}
											else

												if (optedField == '10') {

													//	if (optedField == '9') {

													response.sendRedirect('SUITELET', 'customscript_ebiz_rf_postitemreceipt', 'customdeploy_ebiz_rf_postitemreceipt_di', false, POarray);
										//	}
											}//Case# 20148187
												else if((optedField!='1' && optedField!='2' && optedField!='3' && optedField!='4' && optedField!='5') || (optedField==''))
												{
													POarray["custparam_screenno"]='CRT16';
													POarray["custparam_error"] = st12;
													response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
												}
		}

			//            }
			//			else if (optedEvent == '' && optedEvent == null)
			//			{
			//	if the 'Send' button is clicked without any option value entered,
			//  it has to remain in the same screen ie., Receiving Menu.
			//				response.sendRedirect('SUITELET', 'customscript_rf_receiving_menu', 'customdeploy_rf_receiving_menu_di', false, optedEvent);
			//			}
			nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
		}
	}
//}//case# 20148136
