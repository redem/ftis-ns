/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/UserEvents/ebiz_AfterCreateinventory_UE.js,v $
 *     	   $Revision: 1.15.4.4.4.5.2.38.2.4 $
 *     	   $Date: 2015/12/01 15:14:48 $
 *     	   $Author: skreddy $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_AfterCreateinventory_UE.js,v $
 * Revision 1.15.4.4.4.5.2.38.2.4  2015/12/01 15:14:48  skreddy
 * case# 201415883 & 201415951
 * 2015.2 issue fix
 *
 * Revision 1.15.4.4.4.5.2.38.2.3  2015/11/13 16:45:51  sponnaganti
 * case# 201415038
 * 2015.2 issue fix
 *
 * Revision 1.15.4.4.4.5.2.38.2.2  2015/11/10 17:08:46  sponnaganti
 * case# 201415038
 * 2015.2 issue fix
 *
 * Revision 1.15.4.4.4.5.2.38.2.1  2015/09/21 14:02:04  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.15.4.4.4.5.2.38  2015/07/30 15:50:58  nneelam
 * case# 201413465
 *
 * Revision 1.15.4.4.4.5.2.37  2015/07/02 19:46:00  snimmakayala
 * Case#: 201413192
 *
 * Revision 1.15.4.4.4.5.2.36  2014/11/04 15:11:58  sponnaganti
 * Case# 201410621
 * CT SB Issue fixed
 *
 * Revision 1.15.4.4.4.5.2.35  2014/08/23 01:07:22  gkalla
 * case#201410075
 * Pick Seq not updating
 *
 * Revision 1.15.4.4.4.5.2.34  2014/07/31 15:50:21  sponnaganti
 * Case# 20149759
 * Stnd Bundle Issue fix
 *
 * Revision 1.15.4.4.4.5.2.33  2014/07/21 13:11:57  snimmakayala
 * Case: 20149246
 * Record adjustment record for create inventory
 *
 * Revision 1.15.4.4.4.5.2.32  2014/06/30 13:19:56  snimmakayala
 * no message
 *
 * Revision 1.15.4.4.4.5.2.31  2014/06/25 14:37:55  rmukkera
 * Case # 20149077
 *
 * Revision 1.15.4.4.4.5.2.30  2014/05/21 23:10:50  snimmakayala
 * Case #: 20148475
 *
 * Revision 1.15.4.4.4.5.2.29  2014/04/22 15:13:23  grao
 * Case#: 20148081  Sonic Sb issue fixes
 *
 * Revision 1.15.4.4.4.5.2.28  2014/04/21 15:50:10  nneelam
 * case#  20148072
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.15.4.4.4.5.2.27  2014/04/21 14:01:54  skreddy
 * case # 20148051
 * Sonic SB  issue fix
 *
 * Revision 1.15.4.4.4.5.2.26  2014/04/01 16:40:09  skavuri
 * Case # 20127878 issue fixed
 *
 * Revision 1.15.4.4.4.5.2.25  2014/03/24 15:32:23  skavuri
 * Case # 20127801 issue fixed
 *
 * Revision 1.15.4.4.4.5.2.24  2014/03/20 15:44:01  nneelam
 * case#  20127755
 * Standard Bundle Issue Fix.
 *
 * Revision 1.15.4.4.4.5.2.23  2014/02/21 15:56:23  nneelam
 * case#  20127162
 * Standard Bundle Issue Fix.
 *
 * Revision 1.15.4.4.4.5.2.22  2014/02/19 13:35:24  sponnaganti
 * case# 20127151
 * (tasktype=='11' added to if for updating remcube when tasktype is ADJT)
 *
 * Revision 1.15.4.4.4.5.2.21  2014/02/18 15:33:25  sponnaganti
 * case# 20127162
 *  (tasktype=='7' added to if for updating remcube when tasktype is CYCC)
 *
 * Revision 1.15.4.4.4.5.2.20  2014/02/13 15:36:59  skavuri
 * case # 20127153 (now lp# s are not duplicate correct working
 *
 * Revision 1.15.4.4.4.5.2.19  2014/01/29 14:03:51  schepuri
 * 20126925
 * Standard Bundle Issue
 *
 * Revision 1.15.4.4.4.5.2.18  2013/12/26 21:46:05  grao
 * Case# 20126148 related issue fixes in Sb issue fixes
 *
 * Revision 1.15.4.4.4.5.2.17  2013/12/18 15:24:25  nneelam
 * case# 20126288
 * Netsuite Inventory updation
 *
 * Revision 1.15.4.4.4.5.2.16  2013/11/28 15:19:52  grao
 * Case# 20126021  related issue fixes in SB 2014.1
 *
 * Revision 1.15.4.4.4.5.2.15  2013/11/23 02:51:27  snimmakayala
 * Standard Bundle Fix.
 * Case# : 201217582
 *
 * Revision 1.15.4.4.4.5.2.14  2013/11/06 15:24:47  rmukkera
 * Case # 20125557
 *
 * Revision 1.15.4.4.4.5.2.13  2013/11/05 15:24:18  rmukkera
 * Case# 20125557
 *
 * Revision 1.15.4.4.4.5.2.12  2013/08/05 15:04:15  rrpulicherla
 * case# 20123755
 * BB Cycle count issues
 *
 * Revision 1.15.4.4.4.5.2.11  2013/07/09 10:06:27  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA :: Issue Fixes
 * Case# : 20123239
 *
 * Revision 1.15.4.4.4.5.2.10  2013/06/24 15:43:09  gkalla
 * CASE201112/CR201113/LOG201121
 * fix of PMME case#20123158
 *
 * Revision 1.15.4.4.4.5.2.9  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.15.4.4.4.5.2.8  2013/04/22 15:40:01  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.15.4.4.4.5.2.7  2013/04/05 01:19:39  kavitha
 * CASE201112/CR201113/LOG2012392
 * TSG Issue fixes
 *
 * Revision 1.15.4.4.4.5.2.6  2013/04/01 20:59:31  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.15.4.4.4.5.2.5  2013/03/26 13:27:42  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.15.4.4.4.5.2.4  2013/03/22 11:44:32  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.15.4.4.4.5.2.3  2013/03/19 11:53:20  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.15.4.4.4.5.2.2  2013/03/15 17:04:16  skreddy
 * CASE201112/CR201113/LOG201121
 * added new feild for capturing time with sec
 *
 * Revision 1.15.4.4.4.5.2.1  2013/03/01 14:35:04  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.15.4.4.4.5  2013/02/22 06:43:07  skreddy
 * CASE201112/CR201113/LOG201121
 * changed to driven by system rule
 *
 * Revision 1.15.4.4.4.4  2013/02/18 06:30:49  skreddy
 * CASE201112/CR201113/LOG201121
 * Create Inventory Aduit
 *
 * Revision 1.15.4.4.4.3  2013/01/02 15:43:22  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Inventory Sync issue fixes
 * .
 *
 * Revision 1.15.4.4.4.2  2012/12/16 01:39:08  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA after go live Fixes.
 *
 * Revision 1.15.4.4.4.1  2012/11/01 14:54:57  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.15.4.4  2012/05/07 10:50:36  mbpragada
 * CASE201112/CR201113/LOG201121
 * Remaining Cube update for bin location through  UI Pick
 * Confirmation for Batch item.
 *
 * Revision 1.15.4.3  2012/04/18 13:40:37  schepuri
 * CASE201112/CR201113/LOG201121
 * calculate remaing cube of bin loc after putaway confirm
 *
 * Revision 1.15.4.2  2012/04/02 06:31:53  spendyala
 * CASE201112/CR201113/LOG201121
 * issue related to batch was fixed.
 *
 * Revision 1.15.4.1  2012/02/09 10:42:19  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge to brach
 *
 * Revision 1.15  2011/11/15 16:07:16  snimmakayala
 * CASE201112/CR201113/LOG201121
 * set display flag to 'N' when updating inventory
 *
 * Revision 1.14  2011/09/30 11:22:53  sallampati
 * CASE2011263/CR2011260/LOG2011188
 * Lot# has been added as extra parameter for inventory posting function.
 * this is enhanced for TNT
 *
 * Revision 1.13  2011/09/21 18:58:16  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Inventory Move Fixes
 *
 * Revision 1.12  2011/09/16 15:08:04  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.11  2011/08/20 07:22:41  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.10  2011/07/29 11:13:39  rmukkera
 * CASE201112/CR201113/LOG201121
 * Modified the update loccube functionality
 *
 * Revision 1.9  2011/07/21 07:04:32  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/
var oldqty;
var qty;
var oldqoh;
var oldbinloc;
function CreateInventorySubmitUE(type){
	nlapiLogExecution('DEBUG', 'type', type);

	var AduitRequired = GetSystemRuleForaudit();

	if(type=='delete')
	{

		//To Update remaining cube while deleting the record
		nlapiLogExecution('DEBUG', 'type',type);
		var oldRecord = nlapiGetOldRecord();
		nlapiLogExecution('DEBUG', 'Create Invt Id',oldRecord.getId());
		var itemvalue = oldRecord.getFieldValue('custrecord_ebiz_inv_sku');
		var binloc = oldRecord.getFieldValue('custrecord_ebiz_inv_binloc');
		var qohqty = oldRecord.getFieldValue('custrecord_ebiz_qoh');
		nlapiLogExecution('ERROR', 'binloc', binloc);
		var OldLocRemCube;
		var fields = ['custrecord_remainingcube'];
		if(binloc!=null && binloc!=''){
			var columns= nlapiLookupField('customrecord_ebiznet_location',binloc,fields);
			OldLocRemCube = columns.custrecord_remainingcube;
		}
		var itemCube = 0;
		if(itemvalue!=null && itemvalue!=''){
			var filters = new Array();

			filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemvalue);
			filters[1] = new nlobjSearchFilter('custrecord_ebizuomskudim', null, 'is', 1);

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_ebizcube'); 
			var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

			if(skuDimsSearchResults != null && skuDimsSearchResults != "" ){
				nlapiLogExecution('ERROR', 'skuDimsSearchResults', skuDimsSearchResults.length);
				for (var i = 0; i < skuDimsSearchResults.length; i++) {
					var skuDim = skuDimsSearchResults[i];
					itemCube  =  skuDim.getValue('custrecord_ebizcube');		    	
				}
			}
		}
		nlapiLogExecution('DEBUG', 'OldLocRemCube', OldLocRemCube);
		nlapiLogExecution('DEBUG', 'qty', qty);
		nlapiLogExecution('DEBUG', 'itemCube', itemCube);	

		var newLocCube = parseFloat(OldLocRemCube)  + (qohqty * parseFloat(itemCube));

		nlapiLogExecution('DEBUG', 'newLocCube', newLocCube);

		if (binloc != null && binloc != "" && newLocCube >= 0){
			UpdateLocCube(binloc, newLocCube);
		}

		if(AduitRequired=='Y')
		{
			CreateInvtAudit(type);//for create inventory auditing
		}
	}

	if (type == 'create' || type == 'edit' || type == 'xedit') 
	{     
		try
		{
			var oldrecord;
			if(type=='edit' || type == 'xedit')
			{
				oldrecord=nlapiGetOldRecord();
				oldqty = oldrecord.getFieldValue('custrecord_ebiz_inv_qty');
				oldqoh = oldrecord.getFieldValue('custrecord_ebiz_qoh');
				oldbinloc = oldrecord.getFieldValue('custrecord_ebiz_inv_binloc');
				var oldbinloctxt = oldrecord.getFieldText('custrecord_ebiz_inv_binloc');

				var str = 'oldqty.' + oldqty + '<br>';
				str = str + 'oldqoh.' + oldqoh + '<br>';	
				str = str + 'oldbinloc. ' + oldbinloc + '<br>';	
				str = str + 'oldbinloctxt. ' + oldbinloctxt + '<br>';	

				nlapiLogExecution('DEBUG', 'Old Record Values', str);
			}

			var newRecord = nlapiGetNewRecord();
			var newinvrecid=newRecord.getId();
			var itemvalue = newRecord.getFieldValue('custrecord_ebiz_inv_sku');
			qty = newRecord.getFieldValue('custrecord_ebiz_inv_qty');
			var qohqty = newRecord.getFieldValue('custrecord_ebiz_qoh');
			var newlp = newRecord.getFieldValue('custrecord_ebiz_inv_lp');
			var memo = newRecord.getFieldValue('custrecord_ebiz_inv_note1');
			var accountno = newRecord.getFieldValue('custrecord_ebiz_inv_account_no');
			var loc = newRecord.getFieldValue('custrecord_ebiz_inv_loc');
			var binloc = newRecord.getFieldValue('custrecord_ebiz_inv_binloc');
			var disp = newRecord.getFieldValue('custrecord_ebiz_displayfield');
			var callinv = newRecord.getFieldValue('custrecord_ebiz_callinv');
			var dummychk= newRecord.getFieldValue('custrecord_wms_inv_status_flag');
			var itemstatus = newRecord.getFieldValue('custrecord_ebiz_inv_sku_status');
			var tasktype = newRecord.getFieldValue('custrecord_invttasktype');
			var adjusttype = newRecord.getFieldValue('custrecord_ebiz_inv_adjusttype');
			var newbinloctxt = newRecord.getFieldText('custrecord_ebiz_inv_binloc');
			var vfifodate = newRecord.getFieldValue('custrecord_ebiz_inv_fifo');
			nlapiLogExecution('DEBUG', 'vfifodate', vfifodate);
			//Added by Sudheer to post the lotno inventory items.

			//var lot = newRecord.getFieldValue('custrecord_ebiz_inv_lot');
			var lot = newRecord.getFieldText('custrecord_ebiz_inv_lot');

			nlapiLogExecution('DEBUG', 'newinvrecid', newinvrecid);

			if(type =='xedit')
			{
				var createinvrec= nlapiLoadRecord('customrecord_ebiznet_createinv', newinvrecid);
				if(createinvrec!=null && createinvrec!='')
				{
					itemvalue = createinvrec.getFieldValue('custrecord_ebiz_inv_sku');
					qty = createinvrec.getFieldValue('custrecord_ebiz_inv_qty');
					qohqty = createinvrec.getFieldValue('custrecord_ebiz_qoh');
					newlp = createinvrec.getFieldValue('custrecord_ebiz_inv_lp');
					memo = createinvrec.getFieldValue('custrecord_ebiz_inv_note1');
					accountno = createinvrec.getFieldValue('custrecord_ebiz_inv_account_no');
					loc = createinvrec.getFieldValue('custrecord_ebiz_inv_loc');
					binloc = createinvrec.getFieldValue('custrecord_ebiz_inv_binloc');
					disp = createinvrec.getFieldValue('custrecord_ebiz_displayfield');
					callinv = createinvrec.getFieldValue('custrecord_ebiz_callinv');
					dummychk= createinvrec.getFieldValue('custrecord_wms_inv_status_flag');
					itemstatus = createinvrec.getFieldValue('custrecord_ebiz_inv_sku_status');
					tasktype = createinvrec.getFieldValue('custrecord_invttasktype');
					adjusttype = createinvrec.getFieldValue('custrecord_ebiz_inv_adjusttype');
					newbinloctxt = createinvrec.getFieldText('custrecord_ebiz_inv_binloc');
					lot = createinvrec.getFieldText('custrecord_ebiz_inv_lot');
					vfifodate = createinvrec.getFieldValue('custrecord_ebiz_inv_fifo');
				}

			}

			var str = 'lotno.' + lot + '<br>';
			str = str + 'disp.' + disp + '<br>';	
			str = str + 'callinv. ' + callinv + '<br>';	
			str = str + 'tasktype. ' + tasktype + '<br>';	
			str = str + 'adjusttype. ' + adjusttype + '<br>';	
			str = str + 'binloc. ' + binloc + '<br>';	
			str = str + 'oldqoh. ' + oldqoh + '<br>';	
			str = str + 'qohqty. ' + qohqty + '<br>';	
			str = str + 'tasktype. ' + qohqty + '<br>';	
			str = str + 'dummychk. ' + dummychk + '<br>';	
			str = str + 'newbinloctxt. ' + newbinloctxt + '<br>';	
			str = str + 'loc. ' + loc + '<br>';	
			str = str + 'AduitRequired. ' + AduitRequired + '<br>';	
			str = str + 'itemvalue. ' + itemvalue + '<br>';
			str = str + 'vfifodate. ' + vfifodate + '<br>';	

			nlapiLogExecution('DEBUG', 'Field Values', str);

			/*
			 * If the QOH is no changed then no need to perform cube updates for bin location.
			 */
			//if(oldqoh!=qohqty)
			//{
			if(AduitRequired=='Y')
			{
				CreateInvtAudit(type);//for create inventory auditing
			}

			var AvalCubeCheck;
			//task type 2 means Putaway,9 Move,8 Replean,7 CYCC,18 XFER,11 ADjustment,5 KTS
			if(tasktype!='2' && tasktype!='9' && tasktype!='8' && tasktype!='7'&& tasktype!='18' && tasktype!='11' && tasktype!='3' && tasktype!='5' 
				&& (oldqoh!=qohqty))
			{
				if (type=='create')
				{
					nlapiLogExecution('DEBUG', 'Bin loc in create', binloc);
					AvalCubeCheck=	UpdateCube(qty, parseFloat(itemvalue), binloc,type,oldqty,qty);
				}
				if(type=='edit')
				{
					nlapiLogExecution('DEBUG', 'Bin loc in edit', binloc);

					//case # 20127235,20127162 comment below code.

					/*	if(parseFloat(oldqty)!=parseFloat(qty)){

							if(parseFloat(oldqty)>parseFloat(qty)){
								var val=parseFloat(oldqty)-parseFloat(qty);
								nlapiLogExecution('DEBUG', 'val', val);
								AvalCubeCheck = UpdateCube(val, parseFloat(itemvalue), binloc,type,oldqty,qty);
							}
							else if(parseFloat(oldqty)<parseFloat(qty))
							{
								var val=parseFloat(qty)-parseFloat(oldqty);
								nlapiLogExecution('DEBUG', 'val1', val);
								AvalCubeCheck = UpdateCube(val, parseFloat(itemvalue), binloc,type,oldqty,qty);
							}
						}*/

					//End

					if(parseFloat(oldqoh)!=parseFloat(qohqty)){

						if(parseFloat(oldqoh)>parseFloat(qohqty)){
							var val=parseFloat(oldqoh)-parseFloat(qohqty);
							nlapiLogExecution('DEBUG', 'val', val);
							AvalCubeCheck = UpdateCube(val, parseFloat(itemvalue), binloc,type,oldqoh,qohqty);
						}
						else if(parseFloat(oldqoh)<parseFloat(qohqty))
						{
							var val=parseFloat(qohqty)-parseFloat(oldqoh);
							nlapiLogExecution('DEBUG', 'val1', val);
							AvalCubeCheck = UpdateCube(val, parseFloat(itemvalue), binloc,type,oldqoh,qohqty);
						}
					}
				}
			}
			//case# 20127162 (tasktype=='7' added to if for updating remcube when tasktype is CYCC)
			//case# 20127151 (tasktype=='11' added to if for updating remcube when tasktype is ADJT)
			//case# 20147959 (tasktype=='5' added to if for updating remcube when tasktype is KTS)
			else if((tasktype=='2' || tasktype=='7' || tasktype=='11' || tasktype=='9' || tasktype=='8' || tasktype=='18' || tasktype=='3' || tasktype=='5') && 
					(type=='edit' || type=='create') && (oldqoh!=qohqty))
			{
				//case # 20127755 added create if condition.
//				case # 20148072
				if (type=='create'&& dummychk !='18' && (tasktype!='2'))
				{
					nlapiLogExecution('DEBUG', 'Bin loc in create', binloc);
					AvalCubeCheck=	UpdateCube(qty, parseFloat(itemvalue), binloc,type,oldqty,qty);
				}

				/*if(parseFloat(oldqty)!=parseFloat(qty)){

						if(parseFloat(oldqty)>parseFloat(qty)){
							var val=parseFloat(oldqty)-parseFloat(qty);
							nlapiLogExecution('DEBUG', 'val', val);
							AvalCubeCheck = UpdateCube(val, parseFloat(itemvalue), binloc,type,oldqty,qty);
						}
						else if(parseFloat(oldqty)<parseFloat(qty))
						{
							var val=parseFloat(qty)-parseFloat(oldqty);
							nlapiLogExecution('DEBUG', 'val1', val);
							AvalCubeCheck = UpdateCube(val, parseFloat(itemvalue), binloc,type,oldqty,qty);
						}
					}*/
				//if(type=='edit')
				if(type=='edit' && tasktype!='2')
				{
					nlapiLogExecution('DEBUG', 'Bin loc in edit', binloc);
					if(parseFloat(oldqoh)!=parseFloat(qohqty)){

						if(parseFloat(oldqoh)>parseFloat(qohqty)){
							var val=parseFloat(oldqoh)-parseFloat(qohqty);
							nlapiLogExecution('DEBUG', 'val', val);
							AvalCubeCheck = UpdateCube(val, parseFloat(itemvalue), binloc,type,oldqoh,qohqty);
						}
						else if(parseFloat(oldqoh)<parseFloat(qohqty))
						{
							var val=parseFloat(qohqty)-parseFloat(oldqoh);
							nlapiLogExecution('DEBUG', 'val1', val);
							AvalCubeCheck = UpdateCube(val, parseFloat(itemvalue), binloc,type,oldqoh,qohqty);
						}
					}
				}
			}
			//case# 20127162 end
			//case# 20127151 end
			var Recid = newRecord.getId();
			// Case# 20127801 starts
			nlapiLogExecution('DEBUG', 'itemvalue is', itemvalue);
			if(itemvalue!=null && itemvalue!='')
			{
				var itemTypesku = nlapiLookupField('item', itemvalue, 'salesdescription');
				var itemdesc=itemTypesku;
				nlapiLogExecution('ERROR', 'itemdesc is', itemdesc);
				nlapiSubmitField('customrecord_ebiznet_createinv', Recid, 'custrecord_ebiz_itemdesc', itemdesc);
			}
			// Case# 20127801 end
			var filters=new Array();
			filters.push(new nlobjSearchFilter('internalid', null, 'is', binloc));
			if(loc!=null && loc!='')
				filters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof', loc));			
			filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			filters.push(new nlobjSearchFilter('isinactive', 'custrecord_inboundlocgroupid', 'is', 'F'));
			filters.push(new nlobjSearchFilter('isinactive', 'custrecord_outboundlocgroupid', 'is', 'F'));
			var columns=new Array();
			columns.push(new nlobjSearchColumn('custrecord_inboundlocgroupid'));
			columns.push(new nlobjSearchColumn('custrecord_outboundlocgroupid'));
			// Case # 20127878 starts
			columns.push(new nlobjSearchColumn('custrecord_aisle_row'));
			columns.push(new nlobjSearchColumn('custrecord_level'));
			// Case # 20127878 ends
			var searchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);

			if(searchresults != null && searchresults != '' && searchresults.length>0)
			{	
				nlapiLogExecution('DEBUG', 'Into Update location groups');
				var inblocgroupid = searchresults[0].getValue('custrecord_inboundlocgroupid');
				var outlocgroupid = searchresults[0].getValue('custrecord_outboundlocgroupid');
				// Case # 20127878 starts
				var aisle = searchresults[0].getValue('custrecord_aisle_row');
				var level = searchresults[0].getValue('custrecord_level');
				//Case # 20127878 ends
				var invPickSeq = searchresults[0].getValue('custrecord_startingpickseqno');
				var invPutSeq = searchresults[0].getValue('custrecord_startingputseqno');
				var fieldNames = new Array(); 
				fieldNames.push('custrecord_inboundinvlocgroupid');
				fieldNames.push('custrecord_outboundinvlocgroupid');
				//  Case # 20127878 starts
				fieldNames.push('custrecord_ebiz_inv_aisle');
				fieldNames.push('custrecord_ebiz_inv_level');
				// Case # 20127878 ends
				fieldNames.push('custrecord_pickseqno');
				fieldNames.push('custrecord_putseqno');
				var newValues = new Array(); 
				newValues.push(inblocgroupid);
				newValues.push(outlocgroupid);
				// Case # 20127878 starts
				newValues.push(aisle);
				newValues.push(level);
				//Case # 20127878 ends
				newValues.push(invPickSeq);
				newValues.push(invPutSeq);
				nlapiSubmitField('customrecord_ebiznet_createinv', Recid, fieldNames, newValues);
			}
			nlapiLogExecution('DEBUG', 'AvalCubeCheck', AvalCubeCheck);
			if (disp != 'N') {
				var Recid = newRecord.getId();
				var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', Recid);
				transaction.setFieldValue('custrecord_ebiz_qoh', parseFloat(qty).toFixed(5));
				if (dummychk != "1" ||dummychk!="18" ) {
					transaction.setFieldValue('custrecord_wms_inv_status_flag', 19);
					transaction.setFieldValue('custrecord_invttasktype', 10);
				}
				transaction.setFieldValue('custrecord_updated_user_no', nlapiGetContext().getUser());
				transaction.setFieldValue('custrecord_ebiz_callinv', "N");
				if(binloc==null || binloc=='')
					binloc = transaction.getFieldValue('custrecord_ebiz_inv_binloc');
				var filters = new Array();
				//Case start 20126021
				filters.push(new nlobjSearchFilter('internalid', null, 'is', binloc));
				//end
				if(loc!=null && loc!='')
					filters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof', loc));	
				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_inboundlocgroupid');
				columns[1] = new nlobjSearchColumn('custrecord_outboundlocgroupid');
				columns[2] = new nlobjSearchColumn('custrecord_startingpickseqno');
				columns[3] = new nlobjSearchColumn('custrecord_startingputseqno');
				columns[4] = new nlobjSearchColumn('custrecord_aisle_row');
				columns[5] = new nlobjSearchColumn('custrecord_level');
				columns[6] = new nlobjSearchColumn('custrecord_palletposition');
				var searchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);
				nlapiLogExecution('DEBUG', 'Location', binloc);

				if (AvalCubeCheck==true)
				{
					for (var i = 0; searchresults != null && i < searchresults.length; i++) {
						nlapiLogExecution('DEBUG', 'FOund');
						transaction.setFieldValue('custrecord_inboundinvlocgroupid', searchresults[0].getValue('custrecord_inboundlocgroupid'));
						transaction.setFieldValue('custrecord_outboundinvlocgroupid', searchresults[0].getValue('custrecord_outboundlocgroupid'));
						transaction.setFieldValue('custrecord_pickseqno', searchresults[0].getValue('custrecord_startingpickseqno'));
						transaction.setFieldValue('custrecord_putseqno', searchresults[0].getValue('custrecord_startingputseqno'));
						transaction.setFieldValue('custrecord_ebiz_inv_aisle', searchresults[0].getValue('custrecord_aisle_row'));
						transaction.setFieldValue('custrecord_ebiz_inv_level', searchresults[0].getValue('custrecord_level'));
						transaction.setFieldValue('custrecord_ebiz_inv_palletposition', searchresults[0].getValue('custrecord_palletposition'));
						transaction.setFieldValue('custrecord_ebiz_callinv','N');
						//Case # 20125557 Start
						var columns = nlapiLookupField('item', itemvalue, ['recordType','custitem_item_family','custitem_item_group']);
						var itemSubtype = columns.recordType;
						var itemfamId = columns.custitem_item_family;
						var itemgrpId = columns.custitem_item_group;
						// case no start 20126925
						var lotno=newRecord.getFieldValue('custrecord_ebiz_inv_lot');
						var site=newRecord.getFieldValue('custrecord_ebiz_inv_loc');
						if ((itemSubtype.recordType == "lotnumberedinventoryitem" || itemSubtype.recordType =="lotnumberedassemblyitem") && lot!=null && lot!='') 
						{
							nlapiLogExecution('DEBUG', 'lotno', lotno);
							var filters =new Array();
							filters[0] = new nlobjSearchFilter('internalid', null,'is',lotno);
							if(itemvalue!=null && itemvalue!='')
							{
								filters[1] = new nlobjSearchFilter('custrecord_ebizsku', null,'anyof',itemvalue);	
							}
							var columns = new Array();
							columns[0] = new nlobjSearchColumn('custrecord_ebizfifodate'); //FIFO Date
							columns[1] = new nlobjSearchColumn('custrecord_ebizexpirydate');
							var searchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filters, columns);
							if(searchresults !=null && searchresults!='')
							{
								transaction.setFieldValue('custrecord_ebiz_inv_fifo',searchresults[0].getValue('custrecord_ebizfifodate'));
								transaction.setFieldValue('custrecord_ebiz_expdate',searchresults[0].getValue('custrecord_ebizexpirydate'));
							}
						}
						else
						{
							//var vfifodate = newRecord.getFieldValue('custrecord_ebiz_inv_fifo');
							nlapiLogExecution('DEBUG','vfifodate.',vfifodate);
							if(vfifodate == null || vfifodate =='')
							{
								fifovalue = FifovalueCheck(itemSubtype,itemvalue,itemfamId,itemgrpId,null,null,null);
								nlapiLogExecution('DEBUG','FIFO Value Check.',fifovalue);
								if(fifovalue!=null && fifovalue!="" && (fifovalue != 'NaN/NaN/NaN'))
									transaction.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
								else
									transaction.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());
								
							}
						}
						//Case # 20125557 END
					}
					nlapiSubmitRecord(transaction, false, true);
				}

			}
			else
			{
				//Case # 20125557 Start
				var Recid = newRecord.getId();
				var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', Recid);
				var columns = nlapiLookupField('item', itemvalue, ['recordType','custitem_item_family','custitem_item_group']);
				var itemSubtype = columns.recordType;
				var itemfamId = columns.custitem_item_family;
				var itemgrpId = columns.custitem_item_group;
				// case no start 20126925
				var lotno=newRecord.getFieldValue('custrecord_ebiz_inv_lot');
				var site=newRecord.getFieldValue('custrecord_ebiz_inv_loc');
				if ((itemSubtype.recordType == "lotnumberedinventoryitem" || itemSubtype.recordType =="lotnumberedassemblyitem") && lot!=null && lot!='') 
				{
					nlapiLogExecution('DEBUG', 'lotvalue', lotno);
					var filters =new Array();
					filters[0] = new nlobjSearchFilter('internalid', null,'is',lotno);
					if(itemvalue!=null && itemvalue!='')
					{
						filters[1] = new nlobjSearchFilter('custrecord_ebizsku', null,'anyof',itemvalue);	
					}
					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_ebizfifodate'); //FIFO Date
					columns[1] = new nlobjSearchColumn('custrecord_ebizexpirydate');
					var searchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filters, columns);
					if(searchresults !=null && searchresults!='')
					{
						transaction.setFieldValue('custrecord_ebiz_inv_fifo',searchresults[0].getValue('custrecord_ebizfifodate'));
						transaction.setFieldValue('custrecord_ebiz_expdate',searchresults[0].getValue('custrecord_ebizexpirydate'));
					}
				}
				else
				{
					//var vfifodate = newRecord.getFieldValue('custrecord_ebiz_inv_fifo');
					nlapiLogExecution('DEBUG','vfifodate.',vfifodate);
					if(vfifodate == null || vfifodate =='')
					{
						fifovalue = FifovalueCheck(itemSubtype,itemvalue,itemfamId,itemgrpId,null,null,null);
						nlapiLogExecution('DEBUG','FIFO Value Check.',fifovalue);
						if(fifovalue!=null && fifovalue!="" && (fifovalue != 'NaN/NaN/NaN'))
							transaction.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
						else
							transaction.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());
						
					}
				}

				nlapiSubmitRecord(transaction, false, true);
				//Case # 20125557 END
			}

			var postinvtAdjId;
			//if (callinv == 'Y' &&  type != 'xedit') {
			if (callinv == 'Y') {

				//var tasktype=10;// 10 is the internalid value of tasktype "INVT";
				//var adjusttype="";
				//Added extra parameter by sudheer by passing lot# 093011
				nlapiLogExecution('DEBUG', 'itemvalue', itemvalue);
				var itemSubtype = nlapiLookupField('item', itemvalue, ['recordType']);
				nlapiLogExecution('DEBUG', 'itemSubtype.recordType =', itemSubtype.recordType);
					if (itemSubtype.recordType == 'serializedinventoryitem'||itemSubtype.recordType =='serializedassemblyitem') 
				{
					var serialsspaces = "";
					var filters= new Array();			
					filters[0] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', itemvalue);
					filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', newlp);
					filters[2] = new nlobjSearchFilter('custrecord_serial_location', null, 'anyof', loc);
					var column =new Array(); 

					column[0] = new nlobjSearchColumn('custrecord_serialnumber');							

					var searchresultser = nlapiSearchRecord('customrecord_ebiznetserialentry',null,filters,column);
					if(searchresultser!=null && searchresultser!='')
					{
						for ( var b = 0; b < searchresultser.length; b++ ) {
							if(serialsspaces==null || serialsspaces=='')
							{
								serialsspaces=searchresultser[b].getValue('custrecord_serialnumber');
							}
							else
							{
								serialsspaces += " " + searchresultser[b].getValue('custrecord_serialnumber');
							}
						}
					}
					lot = serialsspaces;
				}

				try 
				{
					postinvtAdjId=InvokeNSInventoryAdjustment(itemvalue,itemstatus,loc,qty,memo,tasktype,adjusttype,lot);
				} 
				catch (exp) 
				{
					nlapiLogExecution('DEBUG', 'Failed to Post Inventory Adjustment',exp);
					InsertExceptionLog('After Create Inventory',2, 'After Create Inventory', exp, null, null,null,null,null, nlapiGetUser());
				}
				if(postinvtAdjId != null && postinvtAdjId != '')
				{
					var fieldNames = new Array(); 
					fieldNames.push('custrecord_ebiz_callinv');
					fieldNames.push('custrecord_ebiz_displayfield');
					fieldNames.push('custrecord_ebiz_tranid');

					var newValues = new Array(); 
					newValues.push('N');
					newValues.push('N');
					newValues.push(postinvtAdjId);
					nlapiSubmitField('customrecord_ebiznet_createinv', Recid, fieldNames, newValues);
				}	
			}

			//1-CHKN	2-PUTW	3-PICK	7-CYCC	8-RPLN	9-MOVE	11-ADJT	18-XFER 5-KTS
			// memo != Created by Pick Reversal Process, this condition is included Since when no invt record found while doing outbound reversal to update, the system is creating 
			//a new record with task type INVT-10 and as a result in this script the system is creating the inventory adjustment record.
			if((type == 'create') && (tasktype!='1' && tasktype!='2' && tasktype!='9' && tasktype!='8' && tasktype!='7'&& 
					tasktype!='18' && tasktype!='11' && tasktype!='3' && tasktype!='5') && (memo!='Created by Pick Reversal Process'))
			{
				createInvtAdjtRecord(binloc,itemvalue,qohqty,newlp,adjusttype,loc,qohqty,null,postinvtAdjId,tasktype)
			}

			var LocName, LocId, LocRemCube, InbLocId, OubLocId, PickSeq, PutSeq, RemCube;		

			try 
			{
				if(AvalCubeCheck==true)
				{
					nlapiLogExecution('DEBUG', 'INTO MASTER LP INSERTION');
					var filtersmlp = new Array();
					filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', newlp);

					var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

					if (SrchRecord != null && SrchRecord.length > 0) 
					{
						nlapiLogExecution('DEBUG', 'LP FOUND');			
					}
					else 
					{
						nlapiLogExecution('DEBUG', 'LP NOT FOUND');

						var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
						customrecord.setFieldValue('name', newlp);
						customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', newlp);
						//case # 20127153 starts
						customrecord.setFieldValue('custrecord_ebiz_lpmaster_site', loc);
						//case # 20127153 ends
						var rec = nlapiSubmitRecord(customrecord, false, true);

					}
				} 
			}
			catch (exp) 
			{
				nlapiLogExecution('DEBUG', 'Failed to Update/Insert into Master LP Record',exp);
				InsertExceptionLog('After Create Inventory',2, 'After Create Inventory', exp, null, null,null,null,null, nlapiGetUser());
			}


			//for update fixed lp of pickface location
			var priorityPutawayLP='';
			var MaximumQuantity =0;
			if(itemvalue!=null && itemvalue!='')
			{

				var filters = new Array();
				filters.push(new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', [itemvalue]));
				//filters.push(new nlobjSearchFilter('custrecord_priorityputaway', null, 'is', 'T'));
				filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
				if(loc!=null && loc!='')
					filters.push(new nlobjSearchFilter('custrecord_pickface_location', null, 'anyof', [loc,'@NONE@']));
				if(binloc!=null && binloc!='')
					filters.push(new nlobjSearchFilter('custrecord_pickbinloc', null, 'anyof', [binloc]));
				if(itemstatus!=null && itemstatus!='')
					filters.push(new nlobjSearchFilter('custrecord_pickface_itemstatus', null, 'anyof', [itemstatus,'@NONE@']));

				//custrecord_pickbinloc
				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_pickface_ebiz_lpno');
				columns[1] = new nlobjSearchColumn('custrecord_maxqty');
				var pickFaceResults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, filters, columns);
				if (pickFaceResults){
					nlapiLogExecution('ERROR','Inside pickFaceRes','Notnull');
					priorityPutawayLP = pickFaceResults[0].getText('custrecord_pickface_ebiz_lpno');
					MaximumQuantity =pickFaceResults[0].getValue('custrecord_maxqty');
				}
			}

			var pickfaceRecommendedQuantity =0;
			if(priorityPutawayLP!=null && priorityPutawayLP!='')
			{	

				/*					var inventoryQuantity = 0;

							var inventoryFilters = new Array();
							inventoryFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', itemvalue));
							inventoryFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', binloc));
							inventoryFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', priorityPutawayLP));
							inventoryFilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3,19]));
							if(loc!=null && loc!='')
								inventoryFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [loc]));



							var inventoryColumns = new Array();
							inventoryColumns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');

							var inventorySearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, inventoryFilters, inventoryColumns);

							if (inventorySearchResults != null)
							{
								for(var i = 0; i < inventorySearchResults.length; i++)
								{
									inventoryQuantity = parseFloat(inventoryQuantity) + parseFloat(inventorySearchResults[i].getValue('custrecord_ebiz_qoh'));
								}
							}
							nlapiLogExecution('DEBUG', 'inventoryQuantity', inventoryQuantity);
							var pendingReplens	= getOpenQty(itemvalue,binloc);

							var	openreplensqty=0;
							var	pickqty=0;
							var	putawatqty=0;
							if(pendingReplens !=null && pendingReplens!='')
							{
								if(pendingReplens[0][2]!=null && pendingReplens[0][2]!='' && pendingReplens[0][2]!='null' && pendingReplens[0][2]!='unefined')
								{
									openreplensqty = pendingReplens[0][2];
								}
								if(pendingReplens[0][0]!=null && pendingReplens[0][0]!='' && pendingReplens[0][0]!='null' && pendingReplens[0][0]!='unefined')
								{
									putawatqty = pendingReplens[0][0];
								}
								if(pendingReplens[0][2]!=null && pendingReplens[0][1]!='' && pendingReplens[0][1]!='null' && pendingReplens[0][1]!='unefined')
								{
									pickqty = pendingReplens[0][1];
								}
							}


							pickfaceRecommendedQuantity	=(parseFloat(MaximumQuantity) - (parseFloat(openreplensqty) + parseFloat(inventoryQuantity)  - parseFloat(pickqty) + parseFloat(putawatqty)));
							//pickfaceRecommendedQuantity = parseFloat(MaximumQuantity) - (parseFloat(inventoryQuantity)+ parseFloat(opentaskInventoryQuantity));


						}
						nlapiLogExecution('DEBUG', 'pickfaceRecommendedQuantity', pickfaceRecommendedQuantity);

						if(parseFloat(pickfaceRecommendedQuantity) >= parseFloat(qty))
						{*/
				/*var fieldNames = new Array(); 
				fieldNames.push('custrecord_ebiz_inv_lp');
				fieldNames.push('name');

				var newValues = new Array(); 
				newValues.push(priorityPutawayLP);
				newValues.push(priorityPutawayLP);

				nlapiSubmitField('customrecord_ebiznet_createinv', Recid, fieldNames, newValues);*/
				if(dummychk != "18")// case# 201414173
				{
					var fieldNames = new Array(); 
					fieldNames.push('custrecord_ebiz_inv_lp');
					fieldNames.push('name');

					var newValues = new Array(); 
					newValues.push(priorityPutawayLP);
					newValues.push(priorityPutawayLP);
					nlapiLogExecution('ERROR','recid',Recid);
					nlapiLogExecution('ERROR','type',type);
					nlapiLogExecution('ERROR','wmsstatusflag',dummychk);
					nlapiLogExecution('ERROR','priorityPutawayLP',priorityPutawayLP);
					nlapiLogExecution('DEBUG', 'becuase we have priority putaway lp this code is changing lp# from picklp to priority putaway lp for recid',Recid);
					nlapiSubmitField('customrecord_ebiznet_createinv', Recid, fieldNames, newValues);
				}
				
			}



			//}
			//else
			//{
			// The below code is to update remaininig cube when changing the bin location.
			if(type=='edit' && oldbinloc!=binloc)
			{
				var cube;
				var BaseUOMQty;

				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemvalue);		    
				filters[1] = new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T');

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
				columns[1] = new nlobjSearchColumn('custrecord_ebizqty');
				var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);
				if(skuDimsSearchResults != null){
					for (var i = 0; i < skuDimsSearchResults.length; i++) {
						var skuDim = skuDimsSearchResults[i];
						cube = skuDim.getValue('custrecord_ebizcube');
						BaseUOMQty = skuDim.getValue('custrecord_ebizqty');
					}
				}

				var itemCube = 0;
				if ((!isNaN(cube))){						
					var uomqty = ((parseFloat(qohqty))/(parseFloat(BaseUOMQty)));			
					itemCube = (parseFloat(uomqty) * parseFloat(cube));
				} else {
					itemCube = 0;
				}

				UpdateLocationCube(oldbinloc,parseFloat(itemvalue),-parseFloat(itemCube));
				UpdateLocationCube(binloc,parseFloat(itemvalue),parseFloat(itemCube));
			}

			var Recid = newRecord.getId();

			var filters=new Array();
			filters.push(new nlobjSearchFilter('internalid', null, 'is', binloc));
			filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			filters.push(new nlobjSearchFilter('isinactive', 'custrecord_inboundlocgroupid', 'is', 'F'));
			filters.push(new nlobjSearchFilter('isinactive', 'custrecord_outboundlocgroupid', 'is', 'F'));
			var columns=new Array();
			columns.push(new nlobjSearchColumn('custrecord_inboundlocgroupid'));
			columns.push(new nlobjSearchColumn('custrecord_outboundlocgroupid'));
			var searchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);

			if(searchresults != null && searchresults != '' && searchresults.length>0)
			{	
				nlapiLogExecution('DEBUG', 'Into Update location groups');
				var inblocgroupid = searchresults[0].getValue('custrecord_inboundlocgroupid');
				var outlocgroupid = searchresults[0].getValue('custrecord_outboundlocgroupid');

				var fieldNames = new Array(); 
				fieldNames.push('custrecord_inboundinvlocgroupid');
				fieldNames.push('custrecord_outboundinvlocgroupid');

				var newValues = new Array(); 
				newValues.push(inblocgroupid);
				newValues.push(outlocgroupid);

				nlapiSubmitField('customrecord_ebiznet_createinv', Recid, fieldNames, newValues);
			}
			//}
		}
		catch(e)
		{
			nlapiLogExecution('DEBUG', 'Failed to Insert into CreateInvt Audit',e);
		}
	}
}


function createInvtAdjtRecord(binloc,skuno,expqty,explp,adjusttype,whsite,adjustqty,serialnumbers,NSAdjustid,tasktype)
{
	nlapiLogExecution('DEBUG', 'Into createInvtAdjtRecord');
	
	var str = 'binloc.' + binloc + '<br>';
	str = str + 'skuno.' + skuno + '<br>';	
	str = str + 'expqty. ' + expqty + '<br>';	
	str = str + 'explp. ' + explp + '<br>';	
	str = str + 'adjusttype. ' + adjusttype + '<br>';	
	str = str + 'whsite' + whsite + '<br>';	
	str = str + 'adjustqty' + adjustqty + '<br>';	
	str = str + 'serialnumbers. ' + serialnumbers + '<br>';	
	str = str + 'NSAdjustid. ' + NSAdjustid + '<br>';	
	str = str + 'tasktype. ' + tasktype + '<br>';	

	nlapiLogExecution('DEBUG', 'Old Record Values', str);
	
	if(skuno!=null && skuno!='')
	{

		var invAdjustCustRecord = nlapiCreateRecord('customrecord_ebiznet_invadj'); 
		invAdjustCustRecord.setFieldValue('custrecord_ebiz_binloc', binloc);
		invAdjustCustRecord.setFieldValue('custrecord_ebizskuno', skuno);
		invAdjustCustRecord.setFieldValue('custrecord_ebiz_expeqty', parseFloat(expqty).toFixed(5));
		invAdjustCustRecord.setFieldValue('custrecord_ebiz_invjlpno', explp);

		if(adjusttype!=null && adjusttype!='')
		{
			invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjtype', adjusttype);
		}

		invAdjustCustRecord.setFieldValue('custrecord_ebiz_siteid', whsite);
		invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjustqty', parseFloat(adjustqty).toFixed(5));
		invAdjustCustRecord.setFieldValue('custrecord_ebiz_tasktype', tasktype);
		invAdjustCustRecord.setFieldValue('custrecord_ebiz_recorddate', DateStamp());
		invAdjustCustRecord.setFieldValue('custrecord_ebiz_rectime', TimeStamp());
		invAdjustCustRecord.setFieldValue('custrecord_ebiz_updateduserno', nlapiGetContext().getUser());
		if(NSAdjustid != null && NSAdjustid != '')
			invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjustno', NSAdjustid);//NS asdjustment id
		if(serialnumbers != null && serialnumbers != '')
		{
			invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjserialnumbers', serialnumbers);
		}

		var id = nlapiSubmitRecord(invAdjustCustRecord, false, true);
	}
	
	nlapiLogExecution('DEBUG', 'Out of createInvtAdjtRecord');
}


function UpdateLocationCube(binloc,itemvalue,itemCube)
{
	nlapiLogExecution('DEBUG', 'Into UpdateLocationCube', itemCube);

	var RemCube;

	if(binloc!=null && binloc!='')
	{
		var LocRemCube = GeteLocCube(binloc);

		RemCube = parseFloat(LocRemCube) - parseFloat(itemCube);

		var retValue = UpdateLocCube(binloc, RemCube);
	}

	nlapiLogExecution('DEBUG', 'Out of UpdateLocationCube', RemCube);
}

function UpdateCube(quantity, itemvalue ,binloc,type,oldqty,newqty){
	var cube;
	var BaseUOMQty;
	var RemCube;

	var resultqty = parseFloat(quantity);
	nlapiLogExecution('DEBUG', 'resultqty', resultqty);

	if(parseFloat(resultqty) != 0 && (type == 'create' || type == 'edit'))
	{
		var vCube = 0;
		var vweight = 0;
		// Item dimensions
		nlapiLogExecution('DEBUG', 'Create Inventory:item ID', itemvalue);
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemvalue);		    
		filters[1] = new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T');

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
		columns[1] = new nlobjSearchColumn('custrecord_ebizqty');
		var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);
		if(skuDimsSearchResults != null){
			for (var i = 0; i < skuDimsSearchResults.length; i++) {
				var skuDim = skuDimsSearchResults[i];
				cube = skuDim.getValue('custrecord_ebizcube');
				BaseUOMQty = skuDim.getValue('custrecord_ebizqty');
			}
		}

		var itemCube = 0;
		if ((!isNaN(cube))){
			nlapiLogExecution('DEBUG', 'parseFloat(resultqty)', parseFloat(resultqty));
			var uomqty = ((parseFloat(resultqty))/(parseFloat(BaseUOMQty)));			
			itemCube = (parseFloat(uomqty) * parseFloat(cube));
			nlapiLogExecution('DEBUG', 'Create Inventory:itemCube', itemCube);
		} else {
			itemCube = 0;
		}
		nlapiLogExecution('DEBUG', 'itemCube', itemCube);
		var LocRemCube = GeteLocCube(binloc);
		nlapiLogExecution('DEBUG', 'Create Inventory:LocRemCube', LocRemCube);
		if(type=='edit')
		{
			if(parseFloat(oldqty)>parseFloat(newqty)){
				RemCube = parseFloat(LocRemCube) + parseFloat(itemCube);

			}
			else if(parseFloat(oldqty)< parseFloat(newqty))
			{

				RemCube = parseFloat(LocRemCube) - parseFloat(itemCube);
			}
		}
		else
		{
			RemCube = parseFloat(LocRemCube) - parseFloat(itemCube);
		}

		nlapiLogExecution('DEBUG', 'LocId in RemCube', RemCube);
		nlapiLogExecution('DEBUG', 'LocId in UpdateLocCube', binloc);
		if (binloc != null && binloc != "" && RemCube >= 0){
			nlapiLogExecution('DEBUG', 'RemCube', RemCube);
			var retValue = UpdateLocCube(binloc, RemCube);
			nlapiLogExecution('DEBUG', 'LocId in UpdateLocCube', retValue);
			return  true;
		}
		else
		{
			return  false;
		}
	}	
}



function CreateInvtAudit(type){



	nlapiLogExecution('DEBUG', 'type',type);
	var newRecord = nlapiGetNewRecord();
	var itemvalue = newRecord.getFieldValue('custrecord_ebiz_inv_sku');
	var qty = newRecord.getFieldValue('custrecord_ebiz_inv_qty');
	var qohqty = newRecord.getFieldValue('custrecord_ebiz_qoh');
	var newlp = newRecord.getFieldValue('custrecord_ebiz_inv_lp');
	var accountno = newRecord.getFieldValue('custrecord_ebiz_inv_account_no');
	var loc = newRecord.getFieldValue('custrecord_ebiz_inv_loc');
	var binloc = newRecord.getFieldValue('custrecord_ebiz_inv_binloc');
	var disp = newRecord.getFieldValue('custrecord_ebiz_displayfield');
	var callinv = newRecord.getFieldValue('custrecord_ebiz_callinv');
	var dummychk= newRecord.getFieldValue('custrecord_wms_inv_status_flag');
	var itemstatus = newRecord.getFieldValue('custrecord_ebiz_inv_sku_status');
	var tasktype = newRecord.getFieldValue('custrecord_invttasktype');
	var adjusttype = newRecord.getFieldValue('custrecord_ebiz_inv_adjusttype');
	var newbinloctxt = newRecord.getFieldText('custrecord_ebiz_inv_binloc');
	nlapiLogExecution('DEBUG', 'newrecord binloc', newbinloctxt);
	var lot = newRecord.getFieldText('custrecord_ebiz_inv_lot');

	nlapiLogExecution('DEBUG', 'lotno', lot);
	nlapiLogExecution('DEBUG', 'disp', disp);
	nlapiLogExecution('DEBUG', 'callinv', callinv);

	nlapiLogExecution('DEBUG', 'tasktype', tasktype);
	nlapiLogExecution('DEBUG', 'adjusttype', adjusttype);
	nlapiLogExecution('DEBUG', 'type', type);
	nlapiLogExecution('DEBUG', 'binloc', binloc);
	nlapiLogExecution('DEBUG', 'oldqoh', oldqoh);
	nlapiLogExecution('DEBUG', 'qohqty', qohqty);
	nlapiLogExecution('DEBUG', 'Status Flag', dummychk);
	nlapiLogExecution('DEBUG', 'RecordId', newRecord.getId());

	var newMasterlp = newRecord.getFieldText('custrecord_ebiz_inv_masterlp');
	var memo = newRecord.getFieldValue('custrecord_ebiz_inv_note1');
	var newMemo2 = newRecord.getFieldText('custrecord_ebiz_inv_note2');
	var newbinlocId = newRecord.getFieldValue('custrecord_ebiz_inv_binloc');
	var newName = newRecord.getFieldValue('name');
	var newPackcode = newRecord.getFieldValue('custrecord_ebiz_inv_packcode');
	var newLot = newRecord.getFieldValue('custrecord_ebiz_inv_lot');
	var newAllqty = newRecord.getFieldValue('custrecord_ebiz_alloc_qty');
	var newCyntFlag = newRecord.getFieldValue('custrecord_ebiz_cycl_count_hldflag');
	var newExpdate = newRecord.getFieldValue('custrecord_ebiz_expdate');
	var newOutboundGrpId = newRecord.getFieldValue('custrecord_outboundinvlocgroupid');
	var newPickseqno = newRecord.getFieldValue('custrecord_pickseqno');
	var newInboundGrpId = newRecord.getFieldValue('custrecord_inboundinvlocgroupid');
	var Updateduser = newRecord.getFieldValue('custrecord_updated_user_no');
	var NewTranno = newRecord.getFieldValue('custrecord_ebiz_transaction_no');
	var newSerial = newRecord.getFieldValue('custrecord_ebiz_serialnumbers');
	var newPutseqno = newRecord.getFieldValue('custrecord_putseqno');

	nlapiLogExecution('DEBUG', 'newMasterlp', newMasterlp);
	nlapiLogExecution('DEBUG', 'newLot', newLot);
	nlapiLogExecution('DEBUG', 'newbinlocId', newbinlocId);
	nlapiLogExecution('DEBUG', 'Updateduser', Updateduser);
	nlapiLogExecution('DEBUG', 'newPutseqno', newPutseqno);

	var Updateduser =nlapiGetContext().getUser();
	nlapiLogExecution('DEBUG', 'Updateduser', Updateduser);

	var datetimestamp=TimeStampinSec();
	nlapiLogExecution('DEBUG', 'datetimestamp', datetimestamp);

	if(type == 'create')
	{
		nlapiLogExecution('DEBUG', 'create', 'create');
		try
		{
			var ActionType ='Inserted';
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_createinv_audit');
			customrecord.setFieldValue('name', newName);
			customrecord.setFieldValue('custrecord_ebiz_inv_binloc_old', newbinlocId);
			customrecord.setFieldValue('custrecord_ebiz_inv_masterlp_old', newMasterlp);
			customrecord.setFieldValue('custrecord_ebiz_inv_lp_old', newlp);
			customrecord.setFieldValue('custrecord_ebiz_inv_sku_old', itemvalue);		
			customrecord.setFieldValue('custrecord_ebiz_inv_sku_status_old', itemstatus);		
			customrecord.setFieldValue('custrecord_ebiz_inv_packcode_old', newPackcode);
			customrecord.setFieldValue('custrecord_ebiz_inv_qty_old', qty);
			customrecord.setFieldValue('custrecord_ebiz_inv_lot_old', newLot);
			customrecord.setFieldValue('custrecord_ebiz_inv_loc_old', loc);
			customrecord.setFieldValue('custrecord_ebiz_inv_note1_old', memo);
			customrecord.setFieldValue('custrecord_ebiz_inv_note2_old', newMemo2);
			customrecord.setFieldValue('custrecord_ebiz_alloc_qty_old', newAllqty);
			customrecord.setFieldValue('custrecord_ebiz_cycl_count_hldflag_old', newCyntFlag);
			customrecord.setFieldValue('custrecord_ebiz_qoh_old', qohqty);		
			customrecord.setFieldValue('custrecord_ebiz_expdate_old', newExpdate);		
			customrecord.setFieldValue('custrecord_ebiz_wms_inv_status_flag_old', dummychk);
			customrecord.setFieldValue('custrecord_ebiz_invttasktype_old', tasktype);
			customrecord.setFieldValue('custrecord_ebiz_outboundinvlocgroup_old', newOutboundGrpId);
			customrecord.setFieldValue('custrecord_ebiz_pickseqno_old', newPickseqno);		
			customrecord.setFieldValue('custrecord_ebiz_inboundinvlocgroup_old', newInboundGrpId);
			customrecord.setFieldValue('custrecord_ebiz_callinv_old', callinv);
			customrecord.setFieldValue('custrecord_ebiz_updated_user_no_old', Updateduser);
			customrecord.setFieldValue('custrecord_ebiz_transaction_no_old', NewTranno);
			customrecord.setFieldValue('custrecord_ebiz_serialnumbers_old', newSerial);
			customrecord.setFieldValue('custrecord_ebiz_putseqno_old', newPutseqno);
			customrecord.setFieldValue('custrecord_ebiz_inv_updateddate_old', DateStamp());
			customrecord.setFieldValue('custrecord_ebiz_inv_updatedtime_old', TimeStamp());
			customrecord.setFieldValue('custrecord_ebiz_inv_internalid', newRecord.getId());
			customrecord.setFieldValue('custrecord_ebiz_inv_action_type', ActionType);


			var rec = nlapiSubmitRecord(customrecord, false, true);
			nlapiLogExecution('DEBUG', 'rec', rec);
		}
		catch(e)
		{
			nlapiLogExecution('DEBUG', 'Failed to Insert into CreateInvt Audit',e);
		}

	}
	else if(type == 'edit')
	{

		nlapiLogExecution('DEBUG', 'edit', 'edit');

		try
		{
			var oldrecord=nlapiGetOldRecord();
			var olditemvalue = oldrecord.getFieldValue('custrecord_ebiz_inv_sku');
			var oldqty = oldrecord.getFieldValue('custrecord_ebiz_inv_qty');
			var oldqohqty = oldrecord.getFieldValue('custrecord_ebiz_qoh');
			var oldlp = oldrecord.getFieldValue('custrecord_ebiz_inv_lp');
			var oldmemo = oldrecord.getFieldValue('custrecord_ebiz_inv_note1');
			var oldaccountno = oldrecord.getFieldValue('custrecord_ebiz_inv_account_no');
			var oldloc = oldrecord.getFieldValue('custrecord_ebiz_inv_loc');
			var oldbinloc = oldrecord.getFieldValue('custrecord_ebiz_inv_binloc');
			var olddisp = oldrecord.getFieldValue('custrecord_ebiz_displayfield');
			var oldcallinv = oldrecord.getFieldValue('custrecord_ebiz_callinv');
			var olddummychk= oldrecord.getFieldValue('custrecord_wms_inv_status_flag');
			var olditemstatus = oldrecord.getFieldValue('custrecord_ebiz_inv_sku_status');
			var oldtasktype = oldrecord.getFieldValue('custrecord_invttasktype');
			var oldadjusttype = oldrecord.getFieldValue('custrecord_ebiz_inv_adjusttype');
			var oldbinloctxt = oldrecord.getFieldText('custrecord_ebiz_inv_binloc');

			var oldMasterlp = oldrecord.getFieldText('custrecord_ebiz_inv_masterlp');
			var oldMemo2 = oldrecord.getFieldText('custrecord_ebiz_inv_note2');
			var oldbinlocId = oldrecord.getFieldValue('custrecord_ebiz_inv_binloc');
			var oldName = oldrecord.getFieldValue('name');
			var oldPackcode = oldrecord.getFieldValue('custrecord_ebiz_inv_packcode');
			var oldLot = oldrecord.getFieldValue('custrecord_ebiz_inv_lot');
			var oldAllqty = oldrecord.getFieldValue('custrecord_ebiz_alloc_qty');
			var oldCyntFlag = oldrecord.getFieldValue('custrecord_ebiz_cycl_count_hldflag');
			var oldExpdate = oldrecord.getFieldValue('custrecord_ebiz_expdate');
			var oldOutboundGrpId = oldrecord.getFieldValue('custrecord_outboundinvlocgroupid');
			var oldPickseqno = oldrecord.getFieldValue('custrecord_pickseqno');
			var oldInboundGrpId = oldrecord.getFieldValue('custrecord_inboundinvlocgroupid');
			var oldUpdateduser = oldrecord.getFieldValue('custrecord_updated_user_no');
			var oldTranno = oldrecord.getFieldValue('custrecord_ebiz_transaction_no');
			var oldSerial = oldrecord.getFieldValue('custrecord_ebiz_serialnumbers');
			var oldPutseqno = oldrecord.getFieldValue('custrecord_putseqno');
			var oldUpdateDate = oldrecord.getFieldValue('custrecord_ebiz_inv_updateddate');
			var oldUpdateTime = oldrecord.getFieldValue('custrecord_ebiz_inv_updatedtime');

			nlapiLogExecution('DEBUG', 'oldUpdateDate', oldUpdateDate);
			nlapiLogExecution('DEBUG', 'oldUpdateTime', oldUpdateTime);
			nlapiLogExecution('DEBUG', 'oldqty', oldqty);
			nlapiLogExecution('DEBUG', 'oldUpdateduser', oldUpdateduser);


			var ActionType ='Updated';
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_createinv_audit');
			customrecord.setFieldValue('name', newName);
			customrecord.setFieldValue('custrecord_ebiz_inv_binloc_new', newbinlocId);
			customrecord.setFieldValue('custrecord_ebiz_inv_masterlp_new', newMasterlp);
			customrecord.setFieldValue('custrecord_ebiz_inv_lp_new', newlp);
			customrecord.setFieldValue('custrecord_ebiz_inv_sku_new', itemvalue);		
			customrecord.setFieldValue('custrecord_ebiz_inv_sku_status_new', itemstatus);		
			customrecord.setFieldValue('custrecord_ebiz_inv_packcode_new', newPackcode);
			customrecord.setFieldValue('custrecord_ebiz_inv_qty_new', qty);
			customrecord.setFieldValue('custrecord_ebiz_inv_lot_new', newLot);
			customrecord.setFieldValue('custrecord_ebiz_inv_loc_new', loc);
			customrecord.setFieldValue('custrecord_ebiz_inv_note1_new', memo);
			customrecord.setFieldValue('custrecord_ebiz_inv_note2_new', newMemo2);
			customrecord.setFieldValue('custrecord_ebiz_alloc_qty_new', newAllqty);
			customrecord.setFieldValue('custrecord_ebiz_cycl_count_hldflag_new', newCyntFlag);
			customrecord.setFieldValue('custrecord_ebiz_qoh_new', qohqty);		
			customrecord.setFieldValue('custrecord_ebiz_expdate_new', newExpdate);		
			customrecord.setFieldValue('custrecord_ebiz_wms_inv_status_flag_new', dummychk);
			customrecord.setFieldValue('custrecord_ebiz_invttasktype_new', tasktype);
			customrecord.setFieldValue('custrecord_ebiz_outboundinvlocgroup_new', newOutboundGrpId);
			customrecord.setFieldValue('custrecord_ebiz_pickseqno_new', newPickseqno);		
			customrecord.setFieldValue('custrecord_ebiz_inboundinvlocgroup_new', newInboundGrpId);
			customrecord.setFieldValue('custrecord_ebiz_callinv_new', callinv);
			customrecord.setFieldValue('custrecord_ebiz_updated_user_no_new', Updateduser);
			customrecord.setFieldValue('custrecord_ebiz_transaction_no_new', NewTranno);
			customrecord.setFieldValue('custrecord_ebiz_serialnumbers_new', newSerial);
			customrecord.setFieldValue('custrecord_ebiz_putseqno_new', newPutseqno);
			customrecord.setFieldValue('custrecord_ebiz_inv_updateddate_new', DateStamp());
			customrecord.setFieldValue('custrecord_ebiz_inv_updatedtime_new', TimeStamp());
			customrecord.setFieldValue('custrecord_ebiz_inv_internalid', newRecord.getId());
			customrecord.setFieldValue('custrecord_ebiz_inv_updateddatetime_new', datetimestamp);

			customrecord.setFieldValue('custrecord_ebiz_inv_binloc_old', oldbinlocId);
			customrecord.setFieldValue('custrecord_ebiz_inv_masterlp_old', oldMasterlp);
			customrecord.setFieldValue('custrecord_ebiz_inv_lp_old', oldlp);
			customrecord.setFieldValue('custrecord_ebiz_inv_sku_old', olditemvalue);		
			customrecord.setFieldValue('custrecord_ebiz_inv_sku_status_old', olditemstatus);		
			customrecord.setFieldValue('custrecord_ebiz_inv_packcode_old', oldPackcode);
			customrecord.setFieldValue('custrecord_ebiz_inv_qty_old', oldqty);
			customrecord.setFieldValue('custrecord_ebiz_inv_lot_old', oldLot);
			customrecord.setFieldValue('custrecord_ebiz_inv_loc_old', oldloc);
			customrecord.setFieldValue('custrecord_ebiz_inv_note1_old', oldmemo);
			customrecord.setFieldValue('custrecord_ebiz_inv_note2_old', oldMemo2);
			customrecord.setFieldValue('custrecord_ebiz_alloc_qty_old', oldAllqty);
			customrecord.setFieldValue('custrecord_ebiz_cycl_count_hldflag_old', oldCyntFlag);
			customrecord.setFieldValue('custrecord_ebiz_qoh_old', oldqohqty);		
			customrecord.setFieldValue('custrecord_ebiz_expdate_old', oldExpdate);		
			customrecord.setFieldValue('custrecord_ebiz_wms_inv_status_flag_old', olddummychk);
			customrecord.setFieldValue('custrecord_ebiz_invttasktype_old', oldtasktype);
			customrecord.setFieldValue('custrecord_ebiz_outboundinvlocgroup_old', oldOutboundGrpId);
			customrecord.setFieldValue('custrecord_ebiz_pickseqno_old', oldPickseqno);		
			customrecord.setFieldValue('custrecord_ebiz_inboundinvlocgroup_old', oldInboundGrpId);
			customrecord.setFieldValue('custrecord_ebiz_callinv_old', oldcallinv);
			customrecord.setFieldValue('custrecord_ebiz_updated_user_no_old', oldUpdateduser);
			customrecord.setFieldValue('custrecord_ebiz_transaction_no_old', oldTranno);
			customrecord.setFieldValue('custrecord_ebiz_serialnumbers_old', oldSerial);
			customrecord.setFieldValue('custrecord_ebiz_putseqno_old', oldPutseqno);
			customrecord.setFieldValue('custrecord_ebiz_inv_updateddate_old', oldUpdateDate);
			customrecord.setFieldValue('custrecord_ebiz_inv_updatedtime_old', oldUpdateTime);
			customrecord.setFieldValue('custrecord_ebiz_inv_action_type', ActionType);

			var Id = nlapiSubmitRecord(customrecord, false, true);
			nlapiLogExecution('DEBUG', 'Id', Id);
		}
		catch(e)
		{
			nlapiLogExecution('DEBUG', 'Failed to Insert into CreateInvt Audit',e);
		}

	}
	else if(type == 'delete')
	{
		try
		{		
			nlapiLogExecution('DEBUG', 'delete', 'delete');
			var ActionType ='Deleted';
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_createinv_audit');
			customrecord.setFieldValue('name', newName);
			customrecord.setFieldValue('custrecord_ebiz_inv_binloc_old', newbinlocId);
			customrecord.setFieldValue('custrecord_ebiz_inv_masterlp_old', newMasterlp);
			customrecord.setFieldValue('custrecord_ebiz_inv_lp_old', newlp);
			customrecord.setFieldValue('custrecord_ebiz_inv_sku_old', itemvalue);		
			customrecord.setFieldValue('custrecord_ebiz_inv_sku_status_old', itemstatus);		
			customrecord.setFieldValue('custrecord_ebiz_inv_packcode_old', newPackcode);
			customrecord.setFieldValue('custrecord_ebiz_inv_qty_old', qty);
			customrecord.setFieldValue('custrecord_ebiz_inv_lot_old', newLot);
			customrecord.setFieldValue('custrecord_ebiz_inv_loc_old', loc);
			customrecord.setFieldValue('custrecord_ebiz_inv_note1_old', memo);
			customrecord.setFieldValue('custrecord_ebiz_inv_note2_old', newMemo2);
			customrecord.setFieldValue('custrecord_ebiz_alloc_qty_old', newAllqty);
			customrecord.setFieldValue('custrecord_ebiz_cycl_count_hldflag_old', newCyntFlag);
			customrecord.setFieldValue('custrecord_ebiz_qoh_old', qohqty);		
			customrecord.setFieldValue('custrecord_ebiz_expdate_old', newExpdate);		
			customrecord.setFieldValue('custrecord_ebiz_wms_inv_status_flag_old', dummychk);
			customrecord.setFieldValue('custrecord_ebiz_invttasktype_old', tasktype);
			customrecord.setFieldValue('custrecord_ebiz_outboundinvlocgroup_old', newOutboundGrpId);
			customrecord.setFieldValue('custrecord_ebiz_pickseqno_old', newPickseqno);		
			customrecord.setFieldValue('custrecord_ebiz_inboundinvlocgroup_old', newInboundGrpId);
			customrecord.setFieldValue('custrecord_ebiz_callinv_old', callinv);
			customrecord.setFieldValue('custrecord_ebiz_updated_user_no_old', Updateduser);
			customrecord.setFieldValue('custrecord_ebiz_transaction_no_old', NewTranno);
			customrecord.setFieldValue('custrecord_ebiz_serialnumbers_old', newSerial);
			customrecord.setFieldValue('custrecord_ebiz_putseqno_old', newPutseqno);
			customrecord.setFieldValue('custrecord_ebiz_inv_updateddate_old', DateStamp());
			customrecord.setFieldValue('custrecord_ebiz_inv_updatedtime_old',TimeStamp());
			customrecord.setFieldValue('custrecord_ebiz_inv_internalid', newRecord.getId());
			customrecord.setFieldValue('custrecord_ebiz_inv_action_type', ActionType);
			customrecord.setFieldValue('custrecord_ebiz_inv_updateddatetime_new', datetimestamp);
			try
			{
				var rec = nlapiSubmitRecord(customrecord, false, true);
				nlapiLogExecution('DEBUG', 'rec', rec);
			}
			catch(e)
			{
				nlapiLogExecution('DEBUG', 'Failed to Insert into CreateInvt Audit',e);
			}

		}
		catch(e)
		{
			nlapiLogExecution('DEBUG', 'Failed to Insert into CreateInvt Audit while delete the record');
		}

	}
}



function GetSystemRuleForaudit()
{
	try
	{
		var rulevalue='N';
		var filter=new Array();
		filter.push(new nlobjSearchFilter('name',null,'is','Enable Audit logs on Inventory'));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizrulevalue');

		var searchresult=nlapiSearchRecord('customrecord_ebiznet_sysrules',null,filter,column);

		if(searchresult!=null&&searchresult!="")
			rulevalue=searchresult[0].getValue('custrecord_ebizrulevalue');
		nlapiLogExecution('DEBUG','rulevalue',rulevalue);
		return rulevalue;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in GetSystemRuleaduit',exp);
	}
}


function TimeStampinSec(){
	var timestamp;
	var now = new Date();
	var a_p = "";

	//Getting time in hh:mm tt format.
	var curr_hour = now.getHours();
	var curr_min = now.getMinutes();
	var curr_sec = now.getSeconds();

	// determining the am/pm indicator
	if (curr_hour < 12)
		a_p = "am";
	else
		a_p = "pm";

	// finalizing hours depending on 24hr clock
	if (curr_hour == 0)
		curr_hour = 12;
	else if(curr_hour > 12)
		curr_hour -= 12;

	if (curr_min.length == 1)
		curr_min = "0" + curr_min;


	if (curr_sec.length == 1)
		curr_sec = "0" + curr_sec;

	//Adding fields to update time zones.
	timestamp = curr_hour + ":" + curr_min + ":" + curr_sec + " " + a_p;
	nlapiLogExecution('DEBUG','timestamp',timestamp);

	//Adding fields to update time zones.
	//timestamp = curr_hour + ":" + curr_min + " " + a_p;

	var datetime = DateStamp()+" "+timestamp;
	nlapiLogExecution('DEBUG','datetime',datetime);
	return datetime;
}

function  InvokeNSInventoryAdjustment(item,itemstatus,loc,qty,notes,tasktype,adjusttype,lot)
{
	/*
		var vItemMapLocation =  getItemStatusMapLoc(itemstatus);
		var vAccountNo = getAccountNo(loc,vItemMapLocation);

		var outAdj = nlapiCreateRecord('inventoryadjustment');			
		outAdj.setFieldValue('account', vAccountNo);
		outAdj.setFieldValue('memo', notes);
		outAdj.insertLineItem('inventory', 1);
		outAdj.setLineItemValue('inventory', 'item', 1, item);
		outAdj.setLineItemValue('inventory', 'location', 1, vItemMapLocation);
		outAdj.setLineItemValue('inventory', 'adjustqtyby', 1, parseFloat(qty));
		nlapiSubmitRecord(outAdj, false, true);
		nlapiLogExecution('Debug', 'type argument', 'type is create');
	 */

	nlapiLogExecution('Debug', 'Location info ::', loc);
	nlapiLogExecution('Debug', 'Task Type ::', tasktype);
	nlapiLogExecution('Debug', 'Adjustment Type ::', adjusttype);
	nlapiLogExecution('Debug', 'Qty ::', qty);
	nlapiLogExecution('Debug', 'Item ::', item);

	var confirmLotToNS='Y';
	confirmLotToNS=GetConfirmLotToNS(loc);

	var vAccountNo;	
	//Code for fetching account no from stockadjustment custom record	
	vAccountNo = getStockAdjustmentAccountNo(loc,tasktype,adjusttype);			
	nlapiLogExecution('Debug', 'Fetcht Account from stockadjustment', vAccountNo);		
	//upto to here stockadjustment

	var vItemMapLocation =  getItemStatusMapLoc(itemstatus);	
	nlapiLogExecution('Debug', 'vItemMapLocation', vItemMapLocation);	

	if(vAccountNo==null || vAccountNo=="")
	{		
		vAccountNo = getAccountNo(loc,vItemMapLocation);
		nlapiLogExecution('Debug', 'Fetcht Account from Inventory AccountNo', vAccountNo);
	}

	var vCost;
	var vAvgCost;
	var vItemname;
	var avgcostlot;

	var filters = new Array();          
	filters.push(new nlobjSearchFilter('internalid', null, 'is',item));
	// Changed On 30/4/12 by Suman
	filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
	// End of Changes as On 30/4/12

	//Case # 20149077 Start
	if(loc !=null && loc!='' && loc!='null' && loc!='undefined' && loc > 0)
	{
		filters.push(new nlobjSearchFilter('inventorylocation',null, 'anyof',loc));
	}
	//Case # 20149077 end
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('cost');
	//columns[1] = new nlobjSearchColumn('averagecost');

	//As per email from NS on  02-May-2012  we have changed the code to use 'locationaveragecost' as item unit cost
	//Email from Thad Johnson to Sid
	columns[1] = new nlobjSearchColumn('locationaveragecost');

	columns[2] = new nlobjSearchColumn('itemid');

	//As per recommendation from NS PM Jeff hoffmiester This field 'custitem16' is used as avg cost for Default lot updation in Dec 2011 for TNT
	//columns[3] = new nlobjSearchColumn('custitem16');

	var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
	if (itemdetails !=null) 
	{
		vItemname=itemdetails[0].getValue('itemid');
		vCost = itemdetails[0].getValue('cost');
		nlapiLogExecution('Debug', 'vCost', vCost);
		//vAvgCost = itemdetails[0].getValue('averagecost');
		vAvgCost = itemdetails[0].getValue('locationaveragecost');
		//As per recommendation from NS PM Jeff hoffmiester This field custitem16 is used as avg cost for Default lot updation in Dec 2011 for TNT
		//avgcostlot=itemdetails[0].getValue('custitem16');
		//nlapiLogExecution('Debug', 'avgcostlot', avgcostlot);	
		nlapiLogExecution('Debug', 'Average Cost', vAvgCost);	         
	}		

	var outAdj = nlapiCreateRecord('inventoryadjustment');		
	if(vAccountNo!=null&&vAccountNo!=""&&vAccountNo!="null")
		outAdj.setFieldValue('account', vAccountNo);
	outAdj.setFieldValue('memo', notes);
	outAdj.selectNewLineItem('inventory');
	outAdj.setCurrentLineItemValue('inventory', 'item', item);
	outAdj.setCurrentLineItemValue('inventory', 'location',vItemMapLocation);	
	outAdj.setCurrentLineItemValue('inventory', 'adjustqtyby',parseFloat(qty));
	if(vAvgCost != null &&  vAvgCost != "")	
	{
		nlapiLogExecution('Debug', 'into if cond vAvgCost', vAvgCost);
		outAdj.setCurrentLineItemValue('inventory', 'unitcost',vAvgCost);
	}
	else
	{
		nlapiLogExecution('Debug', 'into else cond.unit cost', vCost);
		outAdj.setCurrentLineItemValue('inventory', 'unitcost', vCost);
	}
	//outAdj.setLineItemValue('inventory', 'unitcost', 1, avgcostlot);//avg cost send to inventory
	//outAdj.setFieldValue('subsidiary', 3);

	//This following line will return 'true' for ONE WORLD Account, 'false' for non-ONE WORLD Account.
	var subs = nlapiGetContext().getFeature('subsidiaries');
	nlapiLogExecution('Debug', 'subs', subs);
	if(subs==true)
	{
		var vSubsidiaryVal=getSubsidiaryNew(loc);
		nlapiLogExecution('Debug','vSubsidiaryVal',vSubsidiaryVal);
		if(vSubsidiaryVal != null && vSubsidiaryVal != '')
			outAdj.setFieldValue('subsidiary', vSubsidiaryVal);
	}


	//Added by Sudheer on 093011 to send lot# for inventory posting
	nlapiLogExecution('Debug', 'lot', lot);
	if(lot!=null && lot!='')
	{
		//vItemname=vItemname.replace(" ","-");
		vItemname=vItemname.replace(/ /g,"-");


		var fields = ['recordType', 'custitem_ebizserialin'];
		var columns = nlapiLookupField('item', item, fields);
		var vItemType = columns.recordType;
		nlapiLogExecution('Debug','vItemType',vItemType);

		var serialInflg="F";		
		serialInflg = columns.custitem_ebizserialin;
		nlapiLogExecution('Debug','serialInflg',serialInflg);

		nlapiLogExecution('Debug', 'lot', lot);
		nlapiLogExecution('Debug', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");
//		For advanced Bin serial Lot management check
		var vAdvBinManagement=false;
		var ctx = nlapiGetContext();
		if(ctx != null && ctx != '')
		{
			if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
				vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
		}  
		nlapiLogExecution('Debug', 'vAdvBinManagement', vAdvBinManagement);

		if (vItemType == "serializedinventoryitem" || vItemType == "serializedassemblyitem" || serialInflg == "T") {
			nlapiLogExecution('Debug','lot',lot);
			if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
			{
				//case 20126071 start : posting serial numbers for AdvanceBinmanagement
				if(lot !=null && lot !='')
				{
					//case # 20126232? start
					var tempQty;

					if(parseFloat(qty)<0)
					{
						tempQty=-1;
					}
					else
					{
						tempQty=1;
					}
					//case # 20126232? end
					var SerialArray = lot.split(' ');
					if(SerialArray !=null && SerialArray !='' && SerialArray.length>0)
					{
						nlapiLogExecution('Debug','SerialArray.length',SerialArray.length);
						var compSubRecord = outAdj.createCurrentLineItemSubrecord('inventory','inventorydetail');
						for (var x = 0; x < SerialArray.length; x++)
						{
							nlapiLogExecution('Debug','SerialArray',SerialArray[x]);
							/*if(x==0)
							var compSubRecord = outAdj.createCurrentLineItemSubrecord('inventory','inventorydetail');*/
							compSubRecord.selectNewLineItem('inventoryassignment');
							//case # 20126232? start
							compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', tempQty);
							//case # 20126232? end
							compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', SerialArray[x]);

							compSubRecord.commitLineItem('inventoryassignment');

						}
						compSubRecord.commit();
					}
				}
				//case 20126071 end
			}
			else
			{
				if(parseFloat(qty)<0)
					qty=parseFloat(qty)*(-1);
				nlapiLogExecution('Debug', 'LotNowithQty', lot + "(" + parseInt(qty) + ")");
				//outAdj.setLineItemValue('inventory', 'serialnumbers', 1, lot);
				outAdj.setCurrentLineItemValue('inventory', 'serialnumbers',lot);
			}
		}
		else if (vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem")
		{
			if(confirmLotToNS=='N')
				lot=vItemname;
			if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
			{
				var compSubRecord = outAdj.createCurrentLineItemSubrecord('inventory','inventorydetail');
				compSubRecord.selectNewLineItem('inventoryassignment');
				compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', qty);
				compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lot);

				compSubRecord.commitLineItem('inventoryassignment');
				compSubRecord.commit();
			}
			else
			{
				if(parseFloat(qty)<0)
					qty=parseFloat(qty)*(-1);
				nlapiLogExecution('Debug', 'LotNowithQty', lot + "(" + parseInt(qty) + ")");
				//outAdj.setLineItemValue('inventory', 'serialnumbers', 1, lot + "(" + parseFloat(qty) + ")");
				outAdj.setCurrentLineItemValue('inventory', 'serialnumbers',lot + "(" + parseFloat(qty) + ")");
			}
		}
	}
	outAdj.commitLineItem('inventory');
	var id=nlapiSubmitRecord(outAdj, false, true);
	nlapiLogExecution('Debug', 'type argument', 'type is create');
	nlapiLogExecution('Debug', 'type argument id', id);
	return id;
}

function UpdateCube(quantity, itemvalue ,binloc,type,oldqty,newqty){
	var cube;
	var BaseUOMQty;
	var RemCube;

	var resultqty = parseFloat(quantity);
	nlapiLogExecution('DEBUG', 'resultqty', resultqty);

	if(parseFloat(resultqty) != 0 && (type == 'create' || type == 'edit'))
	{
		var vCube = 0;
		var vweight = 0;
		// Item dimensions
		nlapiLogExecution('DEBUG', 'Create Inventory:item ID', itemvalue);
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemvalue);		    
		filters[1] = new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T');

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
		columns[1] = new nlobjSearchColumn('custrecord_ebizqty');
		var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);
		if(skuDimsSearchResults != null){
			for (var i = 0; i < skuDimsSearchResults.length; i++) {
				var skuDim = skuDimsSearchResults[i];
				cube = skuDim.getValue('custrecord_ebizcube');
				BaseUOMQty = skuDim.getValue('custrecord_ebizqty');
			}
		}

		var itemCube = 0;
		if ((!isNaN(cube))){
			nlapiLogExecution('DEBUG', 'parseFloat(resultqty)', parseFloat(resultqty));
			var uomqty = ((parseFloat(resultqty))/(parseFloat(BaseUOMQty)));			
			itemCube = (parseFloat(uomqty) * parseFloat(cube));
			nlapiLogExecution('DEBUG', 'Create Inventory:itemCube', itemCube);
		} else {
			itemCube = 0;
		}
		nlapiLogExecution('DEBUG', 'itemCube', itemCube);
		var LocRemCube = GeteLocCube(binloc);
		nlapiLogExecution('DEBUG', 'Create Inventory:LocRemCube', LocRemCube);
		if(type=='edit')
		{
			if(parseFloat(oldqty)>parseFloat(newqty)){
				RemCube = parseFloat(LocRemCube) + parseFloat(itemCube);

			}
			else if(parseFloat(oldqty)< parseFloat(newqty))
			{

				RemCube = parseFloat(LocRemCube) - parseFloat(itemCube);
			}
		}
		else
		{
			RemCube = parseFloat(LocRemCube) - parseFloat(itemCube);
		}

		nlapiLogExecution('DEBUG', 'LocId in RemCube', RemCube);
		nlapiLogExecution('DEBUG', 'LocId in UpdateLocCube', binloc);
		if (binloc != null && binloc != "" && RemCube >= 0){
			nlapiLogExecution('DEBUG', 'RemCube', RemCube);
			var retValue = UpdateLocCube(binloc, RemCube);
			nlapiLogExecution('DEBUG', 'LocId in UpdateLocCube', retValue);
			return  true;
		}
		else
		{
			return  false;
		}
	}	
}
