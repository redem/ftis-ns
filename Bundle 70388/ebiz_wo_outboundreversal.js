/***************************************************************************
���������������������eBizNET Solutions Inc
 ****************************************************************************
 *
 *� $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/Attic/ebiz_wo_outboundreversal.js,v $
 *� $Revision: 1.1.2.1.8.1 $
 *� $Date: 2015/09/21 14:02:27 $
 *� $Author: deepshikha $
 *� $Name: t_WMS_2015_2_StdBundle_1_8 $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_wo_outboundreversal.js,v $
 *� Revision 1.1.2.1.8.1  2015/09/21 14:02:27  deepshikha
 *� 2015.2 issueFix
 *� 201414466
 *�
 *� Revision 1.1.2.1  2013/03/08 14:43:37  skreddy
 *� CASE201112/CR201113/LOG201121
 *� Code merged from Endochoice as part of Standard bundle
 *�
 *� Revision 1.1.2.4.4.3  2012/12/03 16:41:00  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Additional text fields order field ,serial no# and carton no#

 *
 ****************************************************************************/

function wooutboundReversal(request, response){
	if(request.getMethod() == 'GET'){
		nlapiLogExecution('ERROR', 'outboundReversal - GET', 'Start');
		var form = nlapiCreateForm('WO Outbound Reversal');

		var woid=request.getParameter('custpage_workorder');
		var wointid = form.addField('custpage_hdnwono', 'text', 'wono');
		wointid.setDisplayType('hidden');
		wointid.setDefaultValue(woid); 
		form.addField("custpage_confirm","checkbox", "Cancel pick generation of components");
		createOrderSublist(woid,form, request, response);

		form.addSubmitButton('Submit');

		response.writePage(form);

		nlapiLogExecution('ERROR', 'outboundReversal - GET', 'End');
	} else if (request.getMethod() == 'POST'){

		nlapiLogExecution('ERROR', 'outboundReversal - POST', 'Start');
		try
		{
			var form = nlapiCreateForm('Outbound Reversal');

			var woid=request.getParameter('custpage_hdnwono');

			//form.setScript('customscript_ebiz_outboundreversal_cl');
			if(woid!=null && woid!='')
			{
				nlapiLogExecution('ERROR','INTOwoid1',woid);
				//To delete assembly build
				var vDelAssBuild=DeleteAssemblyBuild(woid);
				//For pick reversal
				var CancelAlloc=request.getParameter('custpage_confirm');
				nlapiLogExecution('ERROR', 'CancelAlloc', CancelAlloc);
				var CancelFlag = false;
				if(CancelAlloc=='T')
					CancelFlag = true;	

				WOInvReversal(woid,CancelFlag);
				//To delete putaway pf kit item
				deletePutaway(woid);
				var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
				msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'WO outbound reversed successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
				//var searchresults = nlapiLoadRecord('workorder', woid); 
			}

		}
		catch(exp) {
			nlapiLogExecution('ERROR', 'Exception in Outbound Reversal ', exp);	
			showInlineMessage(form, 'Error', 'Outbound Reversal Failed', "");
			response.writePage(form);
		}
		response.writePage(form);
	}

}
 
 
  
function createOrderSublist(woid,form, request, response){

	if(woid!=null && woid!='')
	{
		var ItemSubList = form.addSubList("custpage_woreport", "list", "Kit Item");
		//ItemSubList.addMarkAllButtons();
		//ItemSubList.addField("custpage_woreport_confirm", "checkbox", "Confirm").setDefaultValue('F');
		ItemSubList.addField("custpage_woreport_item", "text","Item");
		ItemSubList.addField("custpage_woreport_binloc", "text", "Bin Location");
		ItemSubList.addField("custpage_woreport_lp", "text", "LP");
		ItemSubList.addField("custpage_woreport_lotno", "text", "Lot#");  //added by santosh
		ItemSubList.addField("custpage_woreport_itemstatus", "text", "ItemStatus"); //added by santosh 
		ItemSubList.addField("custpage_woreport_qty", "text", "Quantity");   

		nlapiLogExecution('ERROR','INTOwoid1',woid);
		var WOFilers = new Array();
		WOFilers.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', woid));
		WOFilers.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', 5));//kts
		WOFilers.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));//STATUS.INBOUND.PUTAWAY_COMPLETE�


		var columns=new Array();
		columns[0]=new nlobjSearchColumn('custrecord_sku');
		//columns[0].setSort(true);
		columns[1]=new nlobjSearchColumn('custrecord_actbeginloc');//bin loc
		columns[2]=new nlobjSearchColumn('custrecord_actendloc');//bin loc
		columns[3]=new nlobjSearchColumn('custrecord_lpno');
		columns[4]=new nlobjSearchColumn('custrecord_act_qty');
		columns[5]=new nlobjSearchColumn('custrecord_batch_no');//added by santosh
		columns[6]=new nlobjSearchColumn('custrecord_sku_status');//added by santosh

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, WOFilers,columns);



		if(searchresults!=null && searchresults!='') 
		{ 	    	
			for (var i = 0; i < searchresults.length; i++) {

				taskid = searchresults[i].getId();
				var vitem = searchresults[i].getText('custrecord_sku');
				var vactbeginloc = searchresults[i].getText('custrecord_actbeginloc');
				var vactendloc = searchresults[i].getText('custrecord_actendloc');
				var vlpno = searchresults[i].getValue('custrecord_lpno');
				var vactqty = searchresults[i].getValue('custrecord_act_qty');

				var vlotno = searchresults[i].getValue('custrecord_batch_no');//added by santosh
				var Itemstatus = searchresults[i].getText('custrecord_sku_status');//added by santosh
				nlapiLogExecution('ERROR','vlotno',vlotno);

				form.getSubList('custpage_woreport').setLineItemValue('custpage_woreport_item', i+1, vitem);
				form.getSubList('custpage_woreport').setLineItemValue('custpage_woreport_binloc', i+1, vactendloc);
				form.getSubList('custpage_woreport').setLineItemValue('custpage_woreport_lp', i+1, vlpno);
				form.getSubList('custpage_woreport').setLineItemValue('custpage_woreport_qty', i+1, vactqty);  
				//code added by santosh on 08Oct12
				form.getSubList('custpage_woreport').setLineItemValue('custpage_woreport_lotno', i+1, vlotno);
				form.getSubList('custpage_woreport').setLineItemValue('custpage_woreport_itemstatus', i+1, Itemstatus);
				//end of the code

			}
		}

	}	 
}
function DeleteAssemblyBuild(woid)
{
	try
	{
		var filter=new Array();
		filter.push(new nlobjSearchFilter('createdfrom','null','is',woid));

		filter.push(new nlobjSearchFilter('mainline','null','is','T'));
		var vDeleted=false;
		var searchrecord=nlapiSearchRecord('assemblybuild',null,filter,null);
		if(searchrecord != null && searchrecord != '')
		{
			for(var s=0;s<searchrecord.length;s++)
			{	
				var id = nlapiDeleteRecord('assemblybuild', searchrecord[s].getId());
				nlapiLogExecution('ERROR', 'Deleted Rec s OpenTask : ', id);
				//vDeleted=true;
			}
		}
		/*else
	{
		vDeleted=true;
	}*/
	}
	catch (e) {

		if (e instanceof nlobjError) 
		{
			nlapiLogExecution('ERROR', 'system error', e.getCode()+ '\n' + e.getDetails());

		}

		else 
		{ 
			nlapiLogExecution('ERROR', 'unexpected error', e.toString());
			//alert('unexpected error' + '\n' + 'Error: ' + e.toString() );
		}
//		nlapiCancelLineItem('item');

		nlapiLogExecution('ERROR','Error ', e.toString());
		var ErrorMsg = nlapiCreateError('CannotAllocate',e.toString(), true);
		throw ErrorMsg;

		

	}

}

function deletePutaway(woid)
{
	var filters=new Array();
	if (woid != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', woid));
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'is', '3'));
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '5'));
	} 
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_lpno');
	columns[1] = new nlobjSearchColumn('custrecord_act_qty');
	columns[2] = new nlobjSearchColumn('custrecord_invref_no');
	
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(searchresults != null && searchresults != '')
	{
		for(var p=0;p<searchresults.length;p++)
		{
			var vInvRefNoOpen=searchresults[p].getValue('custrecord_invref_no');
			var vActQty=searchresults[p].getValue('custrecord_act_qty');
			if(vInvRefNoOpen != null && vInvRefNoOpen != '' && vActQty!= null && vActQty != '' )
			{	
				var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', vInvRefNoOpen);
				var qty = transaction.getFieldValue('custrecord_ebiz_qoh');
				var allocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
				var vLP=transaction.getFieldValue('custrecord_ebiz_inv_lp');
				var vPackcode=transaction.getFieldValue('custrecord_ebiz_inv_packcode');
				var vSKUStatus=transaction.getFieldValue('custrecord_ebiz_inv_sku_status');

				nlapiLogExecution('ERROR', 'qty', qty);
				nlapiLogExecution('ERROR', 'allocqty', allocqty);
				nlapiLogExecution('ERROR', 'vActQty', vActQty);
				nlapiLogExecution('ERROR', 'vInvRefNoOpen', vInvRefNoOpen);
				 
				transaction.setFieldValue('custrecord_ebiz_qoh',(parseFloat(qty)- parseFloat(vActQty)).toFixed(5));
				nlapiSubmitRecord(transaction, false, true);
				if((parseFloat(qty)- parseFloat(vActQty)).toFixed(5)<=0)
					nlapiDeleteRecord('customrecord_ebiznet_createinv',vInvRefNoOpen);
					//nlapiDeleteRecord('customrecord_ebiznet_createinv',serchInvtRec[j].getId());
				nlapiLogExecution('ERROR', 'main item inv deleted', vActQty);
				
				//Update putaway record status
				var fieldNames = new Array(); 
				fieldNames.push('custrecord_wms_status_flag');                 
				fieldNames.push('custrecord_notes');

				var newValues = new Array(); 

				newValues.push('29');                     
				newValues.push('Updated by Pick Reversal process');
				nlapiSubmitField('customrecord_ebiznet_trn_opentask', searchresults[p].getId(), fieldNames, newValues);
			}
		}	
	}
	/*//Remove stage location inventory from create inventory
	var Ifilters = new Array();
	Ifilters[0] = new nlobjSearchFilter('custrecord_ebiz_transaction_no', null, 'anyof', woid);
	Ifilters[1] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [18]);
	  
	var serchInvtRec= nlapiSearchRecord('customrecord_ebiznet_createinv', null, Ifilters, null);
	if(serchInvtRec != null && serchInvtRec != '')
	{
		for(var j=0;j<serchInvtRec.length;j++)
		{
			nlapiDeleteRecord('customrecord_ebiznet_createinv',serchInvtRec[j].getId());
		}	
	}*/
}
/*function WOInvReversal(woid,CancelAlloc)
{ 
	nlapiLogExecution('ERROR', 'vWOId', woid);
	var vResults=new Array();
	var filters = new Array(); 
	nlapiLogExecution('ERROR', 'into Block vWOId', woid);
	if (woid != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', woid));
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', '8'));
	} 
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_invref_no');
	columns[1] = new nlobjSearchColumn('custrecord_act_qty');



	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterscolumnsll);

	if(searchresults != null && searchresults != "" && searchresults.length>0)
	{ 

		nlapiLogExecution('ERROR', 'ItemCount', searchresults.length);

		for(var p=0;p<searchresults.length;p++)
		{
			var vInvRefNoOpen=searchresults[p].getValue('custrecord_invref_no');
			var vActQty=searchresults[p].getValue('custrecord_act_qty');

			if(vInvRefNoOpen != null && vInvRefNoOpen != '' && vActQty!= null && vActQty != '' )
			{	
				var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', vInvRefNoOpen);
				var qty = transaction.getFieldValue('custrecord_ebiz_qoh');
				var allocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
				var vLP=transaction.getFieldValue('custrecord_ebiz_inv_lp');
				var vPackcode=transaction.getFieldValue('custrecord_ebiz_inv_packcode');
				var vSKUStatus=transaction.getFieldValue('custrecord_ebiz_inv_sku_status');

				nlapiLogExecution('ERROR', 'qty', qty);
				nlapiLogExecution('ERROR', 'allocqty', allocqty);
				nlapiLogExecution('ERROR', 'varqty', vLineQty);
				if(CancelAlloc==false)
					transaction.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(allocqty)+ parseFloat(vActQty)).toFixed(5));
				transaction.setFieldValue('custrecord_ebiz_qoh',(parseFloat(qty)+ parseFloat(vActQty)).toFixed(5));
				nlapiSubmitRecord(transaction, false, true);

				//Need to update status in open task
				nlapiLogExecution('ERROR', 'In to updateStatusInOpenTask function');
				if(CancelAlloc==true)
				{	
					var fieldNames = new Array(); 
					fieldNames.push('custrecord_wms_status_flag');				 
					fieldNames.push('custrecord_notes');

					var newValues = new Array(); 
					newValues.push('29');				 
					newValues.push('Updated by Pick Reversal process');
				}
				else
				{
					var fieldNames = new Array(); 
					fieldNames.push('custrecord_wms_status_flag');  
					fieldNames.push('custrecord_act_end_date');
					fieldNames.push('custrecord_actendloc');
					fieldNames.push('custrecord_act_qty');		
					fieldNames.push('custrecord_notes');

					var newValues = new Array(); 
					newValues.push('9');
					newValues.push('');
					newValues.push('');
					newValues.push('');
					newValues.push('Updated by Pick Reversal process');					
				}
				nlapiSubmitField('customrecord_ebiznet_trn_opentask', searchresults[p].getId(), fieldNames, newValues);
			}
		}
		//Remove stage location inventory from create inventory
		var Ifilters = new Array();
		Ifilters[0] = new nlobjSearchFilter('custrecord_ebiz_transaction_no', null, 'anyof', woid);
		Ifilters[1] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [18]);
		  
		var serchInvtRec= nlapiSearchRecord('customrecord_ebiznet_createinv', null, Ifilters, null);
		if(serchInvtRec != null && serchInvtRec != '')
		{
			for(var j=0;j<serchInvtRec.length;j++)
			{
				nlapiDeleteRecord('customrecord_ebiznet_createinv',serchInvtRec[j].getId());
			}	
		}	
		if(CancelAlloc==true)
		{	
			var wosearchresultsitem = nlapiLoadRecord('workorder', woid);
			var itemcount= wosearchresultsitem.getLineItemCount('item');
			if(itemcount>0)
			{

				//var eBizWaveNo = GetMaxTransactionNo('WAVE');
				for(var s=1;s<=itemcount;s++)
				{  
					nlapiLogExecution('ERROR', 'into Clear', s);

					wosearchresultsitem.setLineItemValue('item', 'custcol_ebizwoinventoryref', s,'');
					wosearchresultsitem.setLineItemValue('item', 'custcol_ebizwoavalqty', s,''); 
					wosearchresultsitem.setLineItemValue('item','custcol_locationsitemreceipt',s,'');	
					wosearchresultsitem.setLineItemValue('item','serialnumbers',s,''); 
					wosearchresultsitem.setLineItemValue('item','custcol_ebizwolotinfo',s,'');
					wosearchresultsitem.setLineItemValue('item','custcol_ebizwobinloc',s,'');
					wosearchresultsitem.setLineItemValue('item','custcol_ebizwobinlocval_2',s,'');
					wosearchresultsitem.setLineItemValue('item','custcol_ebizwolotlineno',s,'');
					wosearchresultsitem.setLineItemValue('item','custcol_ebiz_lp',s,'');
					wosearchresultsitem.setFieldValue('custbody_ebiz_update_confirm','T');
					nlapiLogExecution('ERROR', 'Custom columns data cleared'); 

				}
				var CommittedId = nlapiSubmitRecord(wosearchresultsitem, true);

				nlapiLogExecution('ERROR', 'CommittedId ', CommittedId);
				
				var ErrorMsg = nlapiCreateError('CannotAllocate','De-allocated Successfully', true);
			throw ErrorMsg;
				//showInlineMessage(form, 'Confirmation', 'De-allocated Successfully', '');						 
				//response.writePage(form);
			}
		}
		vResults[0]='S';
		vResults[1] = 'De-allocated Successfully';
	}
	else
	{	
		vResults[0]='E';
		vResults[1] = 'Bin locations are not allocated to this work order';
		//showInlineMessage(form, 'Error', 'Bin locations are not allocated to this work order', '');
		//response.writePage(form);

	}
	return vResults;
} */