/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inbound/Client/ebiz_Confirm_putaway_CL.js,v $
 *     	   $Revision: 1.4 $
 *     	   $Date: 2011/07/21 09:04:33 $
 *     	   $Author: jrnarsupalli $
 *     	   $Name:  $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_Confirm_putaway_CL.js,v $
 * Revision 1.4  2011/07/21 09:04:33  jrnarsupalli
 * CASE201112/CR201113/LOG201121
 * Added function onDelete
 *
 * Revision 1.3  2011/07/21 06:42:08  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/


function poReceiptEntry()
{	
	var receiptno = nlapiGetFieldValue('custpage_receipt');
	var trailerno = nlapiGetFieldValue('custpage_trailer');

	var filterspoReceipt = new Array();
	if (receiptno != "" && receiptno != null) 
		filterspoReceipt[0] = new nlobjSearchFilter('custrecord_ebiz_poreceipt_receiptno', null, 'is', receiptno);
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_poreceipt_receiptno');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_poreceipt_wmsstatusflag');


	var searchresultsPoReceipt = nlapiSearchRecord('customrecord_ebiznet_trn_poreceipt', null, filterspoReceipt, columns);

	if(searchresultsPoReceipt != "" && searchresultsPoReceipt != null)
	{
		alert("Receipt No should be Unique");
		return false;
	}

	var lineCnt = nlapiGetLineItemCount('custpage_receipt_items');
	var i=0;
	var chkcount = 0;
	//if(trailerno!=null && trailerno!=''){
	for (var s = 1; s <= lineCnt; s++)
	{
		var linecheck = nlapiGetLineItemValue('custpage_receipt_items', 'custpage_receipt_chksel', s);
		var lineReceiptQty= nlapiGetLineItemValue('custpage_receipt_items', 'custpage_receipt_receiptqty', s);
		var lineOrdQty= nlapiGetLineItemValue('custpage_receipt_items', 'custpage_receipt_poqty', s);
		var lineRemaningQty= nlapiGetLineItemValue('custpage_receipt_items', 'custpage_receipt_remaningqty', s);
//		var intnum = lineReceiptQty.toFixed();
		if(linecheck == "T") 
		{
			chkcount = 1;
			if(parseInt(lineReceiptQty) > parseInt(lineOrdQty))
			{
				alert("Receipt Qty should not be greater than PO Qty");
				return false;
			}
			if(parseInt(lineReceiptQty) > parseInt(lineRemaningQty))
			{
				alert("Receipt Qty should not be greater than Remaning Qty");
				return false;
			}
			if(lineReceiptQty == "")
			{
				alert('Please Enter Receipt Qty');
				return false;
			}
			else if(isNaN(lineReceiptQty) == true)
			{
				alert('Please enter Receipt Quantity as a number');		
				return false;
			}
			else if(lineReceiptQty < 0)
			{
				alert('Receipt Quantity cannot be -ve');		
				return false;
			}
			else if(lineReceiptQty == 0 && lineRemaningQty != 0)
			{
				alert('Receipt Quantity cannot be 0');		
				return false;
			}
		}
		if(chkcount == 0)
		{
			alert("Please Select Atleast One Line");            
			return false;
		}

	}
	/*}
	else
		{
		
			alert('Please select Trailer No');
		
		}*/
	return true;

}