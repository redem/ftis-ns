

function fnCheckItemAlias(type)
{
	//alert('start'); // Case# 201410375
	var vItemAliasName =  nlapiGetFieldValue('name'); 
	//alert('start11');
	var vSite	= nlapiGetFieldValue('custrecord_ebiz_location');
//	alert('start12');
	var vCompany	= nlapiGetFieldValue('custrecord_ebiz_company');
//	alert('start13');
	var vItem = nlapiGetFieldValue('custrecord_ebiz_item');
//	alert('start14');
	var vUom = nlapiGetFieldValue('custrecord_ebiz_uom');
//	alert('start1');
	if(vItemAliasName != "" && vItemAliasName != null && vItem != "" && vItem != null && vSite != "" && vSite != null && vCompany != ""  && vCompany != null && vUom != "" && vUom != null)
	{
		//alert('start2');
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('name', null, 'is',vItemAliasName);
		filters[1] = new nlobjSearchFilter('custrecord_ebiz_location', null, 'anyof',['@NONE@',vSite]);
		filters[2] = new nlobjSearchFilter('custrecord_ebiz_company', null, 'anyof',['@NONE@',vCompany]);
		filters[3] = new nlobjSearchFilter('custrecord_ebiz_item',null, 'anyof',[vItem]);
		filters[4] = new nlobjSearchFilter('isinactive', null, 'is','F');
		filters[5] = new nlobjSearchFilter('custrecord_ebiz_uom', null, 'is',vUom);
	
		//alert('start3');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, filters);
		//alert('start4');
		if(searchresults)
		{
			alert("The item alias already exists.");
			return false;
		}
		else
		{
			return true;
		} 
	}
	else	
	{
		return true;
	}
}