/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_PickFaceException.js,v $
 *     	   $Revision: 1.2.4.6.4.1.4.4.2.2 $
 *     	   $Date: 2015/04/24 13:18:52 $
 *     	   $Author: schepuri $
 *     	   $Name: t_eBN_2015_1_StdBundle_1_62 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_PickFaceException.js,v $
 * Revision 1.2.4.6.4.1.4.4.2.2  2015/04/24 13:18:52  schepuri
 * case# 201412486
 *
 * Revision 1.2.4.6.4.1.4.4.2.1  2014/08/07 14:16:09  rmukkera
 * Case # 20149871
 *
 * Revision 1.2.4.6.4.1.4.4  2014/06/13 09:43:53  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.2.4.6.4.1.4.3  2014/06/06 07:28:25  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.2.4.6.4.1.4.2  2014/05/30 00:34:22  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.2.4.6.4.1.4.1  2013/04/17 16:02:35  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.2.4.6.4.1  2012/09/21 15:01:30  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.2.4.6  2012/06/18 07:34:38  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT and Prod issue fixes
 *
 * Revision 1.2.4.5  2012/06/11 13:43:23  spendyala
 * CASE201112/CR201113/LOG201121
 * passing parameters to query string was missing.
 *
 * Revision 1.2.4.4  2012/05/15 23:11:59  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Record type issue fixed in Pallet checkin
 *
 * Revision 1.2.4.3  2012/04/11 12:47:01  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF PO location Exception
 *
 * Revision 1.2.4.2  2012/03/16 14:08:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.2.4.1  2012/02/22 12:45:18  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.2  2011/09/24 19:47:31  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.1  2011/06/14 08:15:01  pattili
 * CASE201112/CR201113/LOG201121
 * 1. RF changes in Replenishment. New files added as part of replenishment.
 *
 * Revision 1.5  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.4  2011/04/21 07:03:02  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * RF Packing Functionality
 *
 * Revision 1.3  2011/04/20 13:34:26  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * RF Packing Functionality
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/

function PickFaceException(request, response){
	if (request.getMethod() == 'GET') {

		var poNo = request.getParameter('custparam_poid');
		var poItem = request.getParameter('custparam_poitem');
		var poLineNo = request.getParameter('custparam_lineno');
		var fetchedItemId = request.getParameter('custparam_fetcheditemid');
		var poInternalId = request.getParameter('custparam_pointernalid');
		var itemCube = request.getParameter('custparam_itemcube');
		var getBaseUomQty = request.getParameter('custparam_baseuomqty');
		var whLocation = request.getParameter('custparam_whlocation');
		var actualBeginDate = request.getParameter('custparam_actualbegindate');
		var actualBeginTime = request.getParameter('custparam_actualbegintime'); 
		var actualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var poQtyEntered = request.getParameter('custparam_poqtyentered');
		var poItemRemainingQty = request.getParameter('custparam_poitemremainingqty');
		var poLinePackcode = request.getParameter('custparam_polinepackcode');
		var poLineQuantity = request.getParameter('custparam_polinequantity');
		var poLineQuantityReceived = request.getParameter('custparam_polinequantityreceived');
		var poLineItemStatus = request.getParameter('custparam_polineitemstatus');
		var itemDescription = request.getParameter('custparam_itemdescription');
		var getPOLineItemStatusValue = request.getParameter('custparam_polineitemstatusValue');
		var trantype= request.getParameter('custparam_trantype');

		pickfaceRecommendedQuantity = request.getParameter('custparam_pickfaceexception');
		nlapiLogExecution('ERROR', 'Pickface Recommended Quantity', pickfaceRecommendedQuantity);

		var poQuantityEntered = request.getParameter('custparam_poqtyentered');
		nlapiLogExecution('ERROR', 'PO Quantity Entered', poQuantityEntered);

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4,st5,st6,st7;
		if( getLanguage == 'es_ES')
		{

			st0 = "";
			st1 = "RECOGER LA CARA CANTIDAD ";
			st2 = "CANTIDAD IMPUTADA";
			st3 = "&#191;QUIERES RECIBIR";
			st4 = "TO BULK / PALLET LOCATION (Y/N)?";
			st5 = "Entre la opci&#243;n";
			st6 = "ENVIAR";
			st7 = "ANTERIOR";

		}
		else
		{
			st0 = "";
			st1 = "PICKFACE QUANTITY ";
			st2 = "ENTERED QUANTITY";
			st3 = "DO YOU WANT TO RECEIVE";
			st4 = "TO BULK / PALLET LOCATION (Y/N)?";
			st5 = "ENTER OPTION";
			st6 = "SEND";
			st7 = "PREV";

		}		


		var functionkeyHtml=getFunctionkeyScript('_rf_putaway_lp'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('cmdSend').focus();";        
		html = html + "</script>";		
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_pickface_exception' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + " :<label>" + pickfaceRecommendedQuantity + "</label>";
		html = html + "				<input type='hidden' name='hdnpickfaceRecommendedQuantity' value=" + pickfaceRecommendedQuantity + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + " :<label>" + poQuantityEntered + "</label>";
		html = html + "				<input type='hidden' name='hdnpoQuantityEntered' value=" + poQuantityEntered + ">";
		html = html + "				<input type='hidden' name='hdnItemPackCode' value=" + poLinePackcode + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + poLineQuantity + ">";
		html = html + "				<input type='hidden' name='hdnQuantityReceived' value=" + poLineQuantityReceived + ">";
		html = html + "				<input type='hidden' name='hdnItemStatus' value=" + poLineItemStatus + "></td>";
		html = html + "				<input type='hidden' name='hdnItemCube' value=" + itemCube + "></td>";
		html = html + "				<input type='hidden' name='hdnbaseuomqty' value=" + getBaseUomQty + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + actualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + actualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + actualBeginTimeAMPM + ">";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + itemDescription + ">";
		html = html + "				<input type='hidden' name='hdnPONo' value=" + poNo + ">";
		html = html + "				<input type='hidden' name='hdnPOItem' value=" + poItem + ">";
		html = html + "				<input type='hidden' name='hdnPOLineNo' value=" + poLineNo + ">";
		html = html + "				<input type='hidden' name='hdnFetchedItemId' value=" + fetchedItemId + ">";
		html = html + "				<input type='hidden' name='hdnPOInternalId' value=" + poInternalId + ">";
		html = html + "				<input type='hidden' name='hdnItemStatusValue' value=" + getPOLineItemStatusValue + ">";
		html = html + "				<input type='hidden' name='hdntrantype' value=" + trantype + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + "</td></tr>";
		html = html + "				<tr><td align = 'left'>" + st4;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> " + st5;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enteroption' type='text' value='Y'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st6 + "<input name='cmdSend' id='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "				" + st7 + "<input name='cmdPrevious' type='submit' value='F7'/></td>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('cmdSend').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('ERROR', 'Into Response', 'Into Response');

		var optedEvent = request.getParameter('cmdPrevious');
		var pickfaceRecommendedQuantity = request.getParameter('hdnpickfaceRecommendedQuantity');
		var poQuantityEntered = request.getParameter('hdnpoQuantityEntered');
		var enteredOption = request.getParameter('enteroption').toUpperCase();

		var POarray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR', 'getLanguage', POarray["custparam_language"]);


		var st8;
		if( getLanguage == 'es_ES')
		{

			st8 = "OPCI&#211;N V&#193;LIDA";
		}
		else
		{

			st8 = "INVALID OPTION";

		}

		// From Check-In process
		POarray["custparam_poid"] = request.getParameter('hdnPONo');
		POarray["custparam_poitem"] = request.getParameter('hdnPOItem');
		POarray["custparam_lineno"] = request.getParameter('hdnPOLineNo');
		POarray["custparam_fetcheditemid"] = request.getParameter('hdnFetchedItemId');
		POarray["custparam_pointernalid"] = request.getParameter('hdnPOInternalId');
		POarray["custparam_poqtyentered"] = request.getParameter('hdnpoQuantityEntered');
//		POarray["custparam_poitemremainingqty"] = request.getParameter('custparam_poitemremainingqty');
		POarray["custparam_polinepackcode"] = request.getParameter('hdnItemPackCode');
		POarray["custparam_polinequantity"] = request.getParameter('hdnQuantity');
		POarray["custparam_polinequantityreceived"] = request.getParameter('hdnQuantityReceived');
		POarray["custparam_polineitemstatus"] = request.getParameter('hdnItemStatus');
		POarray["custparam_itemdescription"] = request.getParameter('hdnItemDescription');
		POarray["custparam_itemcube"] = request.getParameter('hdnItemCube');
		POarray["custparam_baseuomqty"] = request.getParameter('hdnbaseuomqty');
		POarray["custparam_actualbegindate"] = request.getParameter('hdnActualBeginDate');
		POarray["custparam_actualbegintime"] = request.getParameter('hdnActualBeginTime');
		POarray["custparam_actualbegintimeampm"] = request.getParameter('hdnActualBeginTimeAMPM');
		POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
		POarray["custparam_polineitemstatusValue"] = request.getParameter('hdnItemStatusValue');
		POarray["custparam_trantype"] = request.getParameter('hdntrantype');
		//	if the previous button 'F8' is clicked, it has to go to the previous screen 
		//  ie., it has to go to RF Check-In PO screen.

		POarray["custparam_screenno"] = '3A';
		POarray["custparam_error"] = st8;

		nlapiLogExecution('ERROR', 'Entered Option', enteredOption);
		nlapiLogExecution('ERROR', 'Warehouse Location', POarray["custparam_whlocation"]);
		var trantype=null;

		var tranid = POarray["custparam_pointernalid"];

		if(tranid !=null && tranid!='' && tranid!='null')
		{
			trantype = nlapiLookupField('transaction', tranid, 'recordType');
		}


		if (optedEvent == 'F7'){
			if(trantype !=null && trantype!='' && trantype!='undefined' && trantype=='returnauthorization')
			{
				response.sendRedirect('SUITELET', 'customscript_rf_rma_checkinqty', 'customdeploy_rf_rmacheckin_qty_di', false, POarray);	
			}
			else
			{
				//response.sendRedirect('SUITELET', 'customscript_rf_checkin_qty', 'customdeploy_rf_checkin_qty_di', false, POarray);
				response.sendRedirect('SUITELET', 'customscript_rf_to_checkinqty', 'customdeploy_rf_tocheckin_qty_di', false, POarray);
			}
		}
		else{
			POarray["custparam_enteredOption"] = enteredOption;

			nlapiLogExecution('ERROR', 'trantype', trantype);
			if (enteredOption == 'N'){

				if(trantype !=null && trantype!='' && trantype!='undefined' && trantype=='returnauthorization')
				{
					response.sendRedirect('SUITELET', 'customscript_rf_rma_checkinqty', 'customdeploy_rf_rmacheckin_qty_di', false, POarray);	
				}
				else
				{
//					case# 201412486
					//response.sendRedirect('SUITELET', 'customscript_rf_checkin_qty', 'customdeploy_rf_checkin_qty_di', false, POarray);
					response.sendRedirect('SUITELET', 'customscript_rf_to_checkinqty', 'customdeploy_rf_tocheckin_qty_di', false, POarray);
				}
				nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
			}
			else if(enteredOption == 'Y')
			{
				if(trantype !=null && trantype!='' && trantype!='undefined' && trantype=='returnauthorization')
				{
					response.sendRedirect('SUITELET', 'customscript_rf_rma_checkin_item_status', 'customdeploy_rf_rma_checkin_item_status', false, POarray);	
				}
				else
				{
					//response.sendRedirect('SUITELET', 'customscript_rf_checkin_item_status', 'customdeploy_rf_checkin_item_status_di', false, POarray);
					response.sendRedirect('SUITELET', 'customscript_rf_to_checkin_item_status', 'customdeploy_rf_to_checkin_item_status_d', false, POarray);
				}
			}
			else if (enteredOption == "" || enteredOption != 'N' || enteredOption != 'Y'){
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('ERROR', 'Entered Option', POarray["custparam_enteredOption"]);
			}
		}
	}
}
