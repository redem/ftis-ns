/***************************************************************************
���������������������
����������������eBizNET Solutions Inc
****************************************************************************
*
*� $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_PickConfirmSerial_SL.js,v $
<<<<<<< ebiz_PickConfirmSerial_SL.js
*� $Revision: 1.2.14.11.2.1 $
*� $Date: 2015/11/06 22:19:16 $
*� $Author: sponnaganti $
*� $Name: t_WMS_2015_2_StdBundle_1_92 $
=======
*� $Revision: 1.2.14.11.2.1 $
*� $Date: 2015/11/06 22:19:16 $
*� $Author: sponnaganti $
*� $Name: t_WMS_2015_2_StdBundle_1_92 $
>>>>>>> 1.2.14.6
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_PickConfirmSerial_SL.js,v $
*� Revision 1.2.14.11.2.1  2015/11/06 22:19:16  sponnaganti
*� case# 201415443
*� 2015.2 issue fix
*�
*� Revision 1.2.14.11  2014/11/20 15:42:01  skavuri
*� Case# 201410844 Std bundle issue fixed
*�
*� Revision 1.2.14.10  2014/09/03 15:47:47  sponnaganti
*� case# 20147957
*� stnd bundle issue fix
*�
*� Revision 1.2.14.9  2014/07/10 06:56:36  skavuri
*� Case# 20149315 Compatibility Issue Fixed
*�
*� Revision 1.2.14.8  2014/07/07 07:09:23  spendyala
*� CASE201112/CR201113/LOG201121
*� Issue fixed related to case#20149307
*�
*� Revision 1.2.14.7  2014/01/07 15:33:31  rmukkera
*� Case # 20126486
*�
*� Revision 1.2.14.6  2013/12/09 14:03:54  schepuri
*� 20126285
*�
*� Revision 1.2.14.5  2013/11/28 15:18:48  grao
*� Case# 20126018  related issue fixes in SB 2014.1
*�
*� Revision 1.2.14.4  2013/09/30 16:01:13  rmukkera
*� Case#  20124376
*�
*� Revision 1.3.14.1  2013/09/11 15:23:51  rmukkera
*� Case# 20124376
*�
*� Revision 1.3  2012/01/06 06:51:20  spendyala
*� CASE201112/CR201113/LOG201121
*� commented  one of the search filter criteria for fetching records from binlocation group to validate name.
*�
*
****************************************************************************/
function SerialNoSelectSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Serial Number Selection');
		var vQbSKU="";
		var vQbLPNo="";
		var vQbQty="";	
		var vopentask="";
		var vcontlp="";
		var vdointernid="";
		var vdoLineno="";
		var vbinloc="";	
		var vserialout = "";
		var vQbSKUname =""; // Case# 201410844
		if(request.getParameter('custparam_serialskuid')!=null && request.getParameter('custparam_serialskuid')!="")
		{
			vQbSKU = request.getParameter('custparam_serialskuid');	
			vQbSKUname = nlapiLookupField('item', vQbSKU, 'itemid');

		}        

		if(request.getParameter('custparam_serialskulp')!=null && request.getParameter('custparam_serialskulp')!="")
		{
			vQbLPNo=request.getParameter('custparam_serialskulp');
			var fromLP = form.addField('custpage_fromlp', 'text', 'From LP');
			fromLP.setDefaultValue(vQbLPNo);
			fromLP.setDisplayType('hidden');
			nlapiLogExecution('ERROR','fromLP ',fromLP);
		}
		if(request.getParameter('custparam_serialskuchknqty')!=null && request.getParameter('custparam_serialskuchknqty')!="")
		{
			vQbQty=request.getParameter('custparam_serialskuchknqty');
		}
		if(request.getParameter('custparam_opentaskid')!=null && request.getParameter('custparam_opentaskid')!="")
		{
			vopentask=request.getParameter('custparam_opentaskid');
		}
		if(request.getParameter('custparam_contLpno')!=null && request.getParameter('custparam_contLpno')!="")
		{
			vcontlp=request.getParameter('custparam_contLpno');
		}
		if(request.getParameter('custparam_dointernno')!=null && request.getParameter('custparam_dointernno')!="")
		{
			vdointernid=request.getParameter('custparam_dointernno');
		}
		if(request.getParameter('custparam_dolineno')!=null && request.getParameter('custparam_dolineno')!="")
		{
			vdoLineno=request.getParameter('custparam_dolineno');
		}
//		case 20124196,20124197 start
		if(request.getParameter('custparam_actbeginloc')!=null && request.getParameter('custparam_actbeginloc')!="")
		{
			vbinloc=request.getParameter('custparam_actbeginloc');
		}
		if(request.getParameter('custparam_serialout')!=null && request.getParameter('custparam_serialout')!="")
		{
			vserialout=request.getParameter('custparam_serialout');
		}
		if(request.getParameter('custparam_saleorderid')!=null && request.getParameter('custparam_saleorderid')!="")
		{
			vsalesorderid=request.getParameter('custparam_saleorderid');
		}

		if(request.getParameter('custparam_serialskuname')!=null && request.getParameter('custparam_serialskuname')!="")
		{
			vQbSKUname=request.getParameter('custparam_serialskuname');
		}
		nlapiLogExecution('ERROR','vQbSKUname ',vQbSKUname);
		if(request.getParameter('custparam_oldLP')!=null && request.getParameter('custparam_oldLP')!="")
		{
			voldLP=request.getParameter('custparam_oldLP');
			var oldLP = form.addField('custpage_oldlp', 'text', 'old LP');
			oldLP.setDefaultValue(voldLP);
			oldLP.setDisplayType('hidden');
			nlapiLogExecution('ERROR','oldLP ',voldLP);
		}
		if(request.getParameter('custparam_serials')!=null && request.getParameter('custparam_serials')!="")
		{
			voldSerials=request.getParameter('custparam_serials');

			var oldSerials = form.addField('custpage_oldserials', 'text', 'old serials');
			oldSerials.setDefaultValue(voldSerials);
			oldSerials.setDisplayType('hidden');
			nlapiLogExecution('ERROR','oldSerials ',oldSerials);
		}
		if(request.getParameter('custparam_invref')!=null && request.getParameter('custparam_invref')!="")
		{
			vinvref=request.getParameter('custparam_invref');

			var invRef = form.addField('custpage_invref', 'text', 'invrefno');
			invRef.setDefaultValue(vinvref);
			invRef.setDisplayType('hidden');
			nlapiLogExecution('ERROR','invRef ',vinvref);
		}
		var tempfield = form.addField('custpage_vtempvalue', 'text', 'TempValue');
		tempfield.setDefaultValue("F");
		tempfield.setDisplayType('hidden');

		var skuidfield = form.addField('custpage_skuid', 'text', 'SKU ID');
		skuidfield.setDefaultValue(vQbSKU);
		skuidfield.setDisplayType('hidden');

		var skunamefield = form.addField('custpage_skuname', 'text', 'SKU Name');
		skunamefield.setDefaultValue(vQbSKUname);
		skunamefield.setDisplayType('hidden');


		var opentaskField = form.addField('custpage_opentask', 'text', 'Opentask');
		opentaskField.setDefaultValue(vopentask);
		opentaskField.setDisplayType('hidden'); 

		var contLpField = form.addField('custpage_contlp', 'text', 'ContLp');
		contLpField.setDefaultValue(vcontlp);
		contLpField.setDisplayType('hidden'); 

		var doField = form.addField('custpage_doid', 'text', 'do');
		doField.setDefaultValue(vdointernid);
		doField.setDisplayType('hidden');

		var dolineField = form.addField('custpage_dolineno', 'text', 'do line');
		dolineField.setDefaultValue(vdoLineno);
		dolineField.setDisplayType('hidden');

		var binloc = form.addField('custpage_actbeginloc', 'text', 'Bin Location');
		//case start 20126018�
		binloc.setDefaultValue(vbinloc);
		binloc.setDisplayType('hidden');

		nlapiLogExecution('ERROR','vbinloc ',vbinloc);
		//case 20126018�end

		var ActualQtyField = form.addField('custpage_actualqty', 'text', 'actual qty');
		ActualQtyField.setDefaultValue(vQbQty);
		ActualQtyField.setDisplayType('hidden');
		//case 20124196,20124197 start
		var vActbinlocation = form.addField('custpage_actbeginloc1', 'text', 'Actual Binlocation');
		vActbinlocation.setDefaultValue(vbinloc);
		vActbinlocation.setDisplayType('hidden');
//		end
		var serialoutField = form.addField('custpage_serialout', 'text', 'SerialOut');
		serialoutField.setDefaultValue(vserialout);
		serialoutField.setDisplayType('hidden');	
		

		
		var SOidfield = form.addField('custpage_salesorderid', 'text', 'Salesorder ID');
		SOidfield.setDefaultValue(vsalesorderid);
		SOidfield.setDisplayType('hidden');

		nlapiLogExecution("ERROR","vserialout",vserialout);
		if(vserialout=="F")
		{
			form.setScript('customscript_pickcnfmserialsave_cl') ;


			var sublist = form.addSubList("custpage_items", "list", "Item List"); //Case# 20149315
			sublist.addField("custpage_select", "checkbox", "Confirm").setDefaultValue('F'); 
			sublist.addField("custpage_serialno", "text", "Serial #");
			sublist.addField("custpage_serialinternid", "text", "Serial #").setDisplayType('hidden');			

			var filters = new Array();				
			//filters[0] = new nlobjSearchFilter('custrecord_serialitem', null, 'equalto', vQbSKU); 	

			filters[0] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', vQbLPNo);
			filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [3,19]);


			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_serialnumber');

			var searchresults = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);

			var vSerialNo;
			for (var i = 0; searchresults != null && i < searchresults.length; i++) {
				var searchresult = searchresults[i];

				vSerialNo = searchresult.getValue('custrecord_serialnumber');           
				form.getSubList('custpage_items').setLineItemValue('custpage_serialno', i + 1, vSerialNo);           
				form.getSubList('custpage_items').setLineItemValue('custpage_serialinternid', i + 1, searchresult.getId());
			}
		}
		else
		{
			form.setScript('customscript_pickcnfmserialsave_cl') ;
			var sublist = form.addSubList("custpage_items", "inlineeditor", "ItemList");
			sublist.addField("custpage_select", "checkbox", "Confirm").setDefaultValue('T');
			sublist.addField("custpage_vsalesorderfield", "text", "SalesOrder").setDisabled(true).setDefaultValue(vdointernid.split(".")[0]);
			sublist.addField("custpage_item", "text", "Item").setDisabled(true).setDefaultValue(vQbSKUname);
			sublist.addField("custpage_serialno", "text", "Serial #");
			
			for (var i = 0; vQbQty != null && i < parseInt(vQbQty)-1; i++) {
						        
				form.getSubList('custpage_items').setLineItemValue('custpage_serialno', i + 1, '');           
				//form.getSubList('custpage_items').setLineItemValue('custpage_serialinternid', i + 1, searchresult.getId());
			}
			
			
			
			
		}
		form.addSubmitButton('Confirm');

		response.writePage(form);
	}
	else //this is the POST block
	{
		nlapiLogExecution("ERROR","Into ELSE");
		var form = nlapiCreateForm('Serial Number Selection');
		form.setScript('customscript_pickcnfmserialsave_cl') ;
		var tempfield = form.addField('custpage_vtempvalue', 'text', 'TempValue');
		tempfield.setDefaultValue("T");
		tempfield.setDisplayType('hidden');
		
		var SerialLP = request.getParameter('custpage_oldlp');
		nlapiLogExecution("ERROR","SerialLP",SerialLP);
		
		var vskuid = request.getParameter('custpage_skuid');
		nlapiLogExecution("ERROR","vskuid",vskuid);

		var vopentask = request.getParameter('custpage_opentask');
		nlapiLogExecution("ERROR","vopentask",vopentask);

		var vcontlp = request.getParameter('custpage_contlp');
		nlapiLogExecution("ERROR","vcontlp",vcontlp);

		var vFOid = request.getParameter('custpage_doid');
		nlapiLogExecution("ERROR","vFOid",vFOid);

		var vFolineno = request.getParameter('custpage_dolineno');
		nlapiLogExecution("ERROR","vFolineno",vFolineno);

		var vactbinloc = request.getParameter('custpage_actbeginloc');
		nlapiLogExecution("ERROR","vactbinloc",vactbinloc);

		var vactqty = request.getParameter('custpage_actualqty');
		nlapiLogExecution("ERROR","vactqty",vactqty);

		var vserialout = request.getParameter('custpage_serialout');
		nlapiLogExecution("ERROR","vserialout",vserialout);

		var vsalesorderid = request.getParameter('custpage_salesorderid');
		nlapiLogExecution("ERROR","vsalesorderid",vsalesorderid);


		if(vserialout=="T")
		{
			var vbatchno="";
			var lineCnt = request.getLineItemCount('custpage_items');
			nlapiLogExecution("ERROR","lineCnt",lineCnt);
			for (var j = 1; j <= lineCnt; j++) 
			{
				var chkVal = request.getLineItemValue('custpage_items','custpage_select', j);
				var vSerialNoEntered = request.getLineItemValue('custpage_items','custpage_serialno', j);
				if (chkVal == 'T')
				{
					var customrecord = nlapiCreateRecord('customrecord_ebiznetserialentry'); 
					customrecord.setFieldValue('custrecord_serialnumber', vSerialNoEntered);					
					customrecord.setFieldValue('custrecord_serialwmsstatus', '8');//with status STATUS.OUTBOUND.PICK_CONFIRMED
					customrecord.setFieldValue('custrecord_serialebizsono', vsalesorderid);
					customrecord.setFieldValue('custrecord_serialsono', vFOid);
					customrecord.setFieldValue('custrecord_serialsolineno', vFolineno); 
					customrecord.setFieldValue('custrecord_serialparentid', vcontlp);
					customrecord.setFieldValue('custrecord_serialitem', vskuid);
					customrecord.setFieldValue('name', vSerialNoEntered);
					var recid = nlapiSubmitRecord(customrecord, true);
				}
				if (vbatchno == "") {
					vbatchno = vSerialNoEntered;
				}
				else {
					vbatchno = vbatchno + "," + vSerialNoEntered;
				}
			}
			var opentask = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', vopentask);
			opentask.setFieldValue('custrecord_serial_no', vbatchno);
			//opentask.setFieldValue('custrecord_actbeginloc', vactbinloc);
			//case# 20147957 (updating New Lp field in open task for getting invrefno in pick confirmation when doing location exception)
			opentask.setFieldValue('custrecord_ebiz_new_lp', SerialLP);
			//case# 20147957 ends
			var opentskretval  =nlapiSubmitRecord(opentask, true);
			showInlineMessage(form, 'Confirmation', "Serial# are Confirmed Succefully");
			form.addSubmitButton('Back To PickConfirmation');
			response.writePage(form);
		}
	}
}
	
	function pickcnfmSerialRecSave()
	{

		//try {
		var cnt = nlapiGetLineItemCount('custpage_items');
		//alert("Count : " + cnt);
		var opentaskId = nlapiGetFieldValue('custpage_opentask');
		var contLpno = nlapiGetFieldValue('custpage_contlp');
		var doNo = nlapiGetFieldValue('custpage_doid');
		var dolineno = nlapiGetFieldValue('custpage_dolineno');
		var binloc = nlapiGetFieldValue('custpage_actbeginloc');
		var vbatchno="";
		var SerialLP = nlapiGetFieldValue('custpage_oldlp');
		
		
		for (var p = 1; p <= cnt; p++) {
			var chkselect = nlapiGetLineItemValue('custpage_items', 'custpage_select', p);
			var lineinternno = nlapiGetLineItemValue('custpage_items', 'custpage_serialinternid', p);
			var vserialno = nlapiGetLineItemValue('custpage_items', 'custpage_serialno', p);			
			if (chkselect == 'T') {

				/*var fields = new Array();
				var values = new Array();
				fields[0] = 'custrecord_serialcontlpno';
				values[0] = contLpno;
				fields[1] = 'custrecord_serialsono';
				values[1] = doNo;
				fields[2] = 'custrecord_serialsolineno';
				values[2] = dolineno;
				var updatefields = nlapiSubmitField('customrecord_ebiznetserialentry', lineinternno, fields, values);
				alert(updatefields);
				 */				 
				var trnserial = nlapiLoadRecord('customrecord_ebiznetserialentry', lineinternno);
				trnserial.setFieldValue('custrecord_serialcontlpno', contLpno);
				trnserial.setFieldValue('custrecord_serialsono', doNo);
				trnserial.setFieldValue('custrecord_serialsolineno', dolineno);
				// alert('Updation');
				var retval  =nlapiSubmitRecord(trnserial, true);

				//alert(retval);				  
				if (vbatchno == "") {
					vbatchno = vserialno;
				}
				else {
					vbatchno = vbatchno + "," + vserialno;
				}
			}
		}

		var opentask = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', opentaskId);
		opentask.setFieldValue('custrecord_serial_no', vbatchno);
		//case# 20147957 (updating New Lp field in open task for getting invrefno in pick confirmation when doing location exception)
		opentask.setFieldValue('custrecord_ebiz_new_lp', SerialLP);
		//case# 20147957 ends
		var opentskretval  =nlapiSubmitRecord(opentask, true);
		alert('Updated Successfully');
		//window.opener.document.location.reload(true);
		window.opener.location.reload();
		window.close(); 


		/*alert('vbatchno  ' + vbatchno);
var fieldsinvt = new Array();
var valuesinvt = new Array();
fieldsinvt[0]='custrecord_batch_no';
valuesinvt[0] = vbatchno;
var updatefieldsinvt = nlapiSubmitField('customrecord_ebiznetserialentry', lineinternno, fieldsinvt, valuesinvt);
alert(updatefieldsinvt);
		 */
		/*}

	catch(exp)
	{
		alert('Into Exception. '+exp);
		return false;
	}*/
		return true;
}
