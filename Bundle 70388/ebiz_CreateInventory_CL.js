/***************************************************************************
����������������������������eBizNET
���������������������eBizNET SOLUTIONS LTD
****************************************************************************
*
*� $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Masters/Client/ebiz_CreateInventory_CL.js,v $
*� $Revision: 1.8.2.4.4.1.4.1 $
*� $Date: 2014/05/22 06:52:40 $
*� $Author: spendyala $
*� $Name: t_eBN_2014_1_StdBundle_3_33 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_CreateInventory_CL.js,v $
*� Revision 1.8.2.4.4.1.4.1  2014/05/22 06:52:40  spendyala
*� CASE201112/CR201113/LOG201121
*� Issue fixed related to case#20148312
*�
*� Revision 1.8.2.4.4.1  2012/11/01 14:55:22  schepuri
*� CASE201112/CR201113/LOG201121
*� Decimal Qty Conversions
*�
*� Revision 1.8.2.4  2012/07/18 06:35:16  spendyala
*� CASE201112/CR201113/LOG201121
*� Exp date and FIFO date will be set default depending upon Lot# selected.
*�
*� Revision 1.8.2.3  2012/04/20 11:57:00  schepuri
*� CASE201112/CR201113/LOG201121
*� changing the Label of Batch #  field to Lot#
*�
*� Revision 1.8.2.2  2012/02/02 13:05:03  spendyala
*� CASE201112/CR201113/LOG201121
*� Added validation for company field while creating inventory record.
*�
*� Revision 1.8.2.1  2012/01/20 13:26:17  spendyala
*� CASE201112/CR201113/LOG201121
*�  Added Search Column in getPalletQuantityFromItemDims function to fetch the record whose uom level is highest.
*�
*� Revision 1.8  2011/12/05 13:09:57  spendyala
*� CASE201112/CR201113/LOG201121
*� removed unwanted alert statements
*�
*� Revision 1.7  2011/12/02 13:40:53  spendyala
*� CASE201112/CR201113/LOG201121
*� added new function validateLOT to validate the LOT/batch# field
*�
*
****************************************************************************/



/**
 * @author LN
 *@version
 *@date
 *@Description: This is a Client Script acting as a Library function to Check-In Screen line
 level LP validation
 */

var eventtype;
var QtyChangeCheck=false;
var QtyOldValue="";
var enteredQuantity;
function  myPageInit(type){
	QtyOldValue = nlapiGetFieldValue('custrecord_ebiz_inv_qty');
	eventtype=type;
	}
function  CheckQtyChange(type,name,linenum){
	if (name=='custrecord_ebiz_inv_qty'){	
		QtyChangeCheck=true;
	}
	if(name=='custrecord_ebiz_inv_lot')
		{
		var batchid=nlapiGetFieldValue('custrecord_ebiz_inv_lot');
		var field=new Array();
		field.push('custrecord_ebizexpirydate');
		field.push('custrecord_ebizfifodate');
		
		var rec=nlapiLookupField('customrecord_ebiznet_batch_entry',batchid,field);
		var expdate=rec.custrecord_ebizexpirydate;
		var fifodate=rec.custrecord_ebizfifodate;
		nlapiSetFieldValue('custrecord_ebiz_expdate',expdate);
		nlapiSetFieldValue('custrecord_ebiz_inv_fifo',fifodate);
		
		
		
		}
	}
function validateQuantity()
{
	nlapiLogExecution('ERROR', 'type', eventtype);
	//var inventoryQuantity = nlapiGetFieldValue('custrecord_ebiz_inv_qty');
	var allocationQuantity = nlapiGetFieldValue('custrecord_ebiz_alloc_qty');
	var quantityOnHand = nlapiGetFieldValue('custrecord_ebiz_qoh');
	var availableQuantity = nlapiGetFieldValue('custrecord_ebiz_avl_qty');		
	enteredQuantity = nlapiGetFieldValue('custrecord_ebiz_inv_qty');
	var packCode = nlapiGetFieldValue('custrecord_ebiz_inv_packcode'); 
	var lpNo = nlapiGetFieldValue('custrecord_ebiz_inv_lp');
	var itemId = nlapiGetFieldValue('custrecord_ebiz_inv_sku');
	var binLocationId = nlapiGetFieldValue('custrecord_ebiz_inv_binloc');
	var vId = nlapiGetFieldValue('id');
	var company=nlapiGetFieldValue('custrecord_ebiz_inv_company');
	var site=nlapiGetFieldValue('custrecord_ebiz_inv_loc');

	var locationCubeValidate;

	var type = "PALT";
	var getLPNo = lpNo;
	var result = "";
	var uomLevel = 3;
	var UOM = "PALLET";

	if(parseFloat(enteredQuantity) <= 0){
		alert('Qty should be greater than 0');
		return false;
	}
	else if(parseFloat(allocationQuantity)<0){
		alert('Allocated Qty should be greater than 0');
		return false;
	}
	else if(parseFloat(quantityOnHand)<=0){
		alert('Qty On Hand should be greater than 0');
		return false;
	}
	else if(parseFloat(availableQuantity)<=0){
		alert('Qty Available should be greater than 0');
		return false;
	}

	var itemQuantity = getPalletQuantityFromItemDims(itemId, packCode);

	if(parseFloat(enteredQuantity) > parseFloat(itemQuantity)){
		alert("Qty is greater than Pallet Qty");
		return false;    		
	}




	if(eventtype=='edit' && QtyChangeCheck==true)
	{
		if(parseFloat(QtyOldValue)!=parseFloat(enteredQuantity)){

			if(parseFloat(QtyOldValue)>parseFloat(enteredQuantity)){
				var val=parseFloat(QtyOldValue)-parseFloat(enteredQuantity);
				locationCubeValidate = validateLocationCube(val, itemId, binLocationId);
			}
			else if(parseFloat(QtyOldValue)<parseFloat(enteredQuantity))
			{
				var val=parseFloat(enteredQuantity)-parseFloat(QtyOldValue);
				locationCubeValidate = validateLocationCube(val, itemId, binLocationId);
			}
		}
	}
	else if(eventtype=='edit' && QtyChangeCheck==false)
	{    	
		var str=ValidateLot(itemId);
		return str;
	}
	else if(eventtype=='create' || eventtype=='copy')
	{

		locationCubeValidate = validateLocationCube(enteredQuantity, itemId, binLocationId);
	}

	nlapiLogExecution('ERROR','locationCubeValidate', locationCubeValidate.toString());
	//alert('locationCubeValidate' +locationCubeValidate);
	if(company != null && company != "")
	{
		if (locationCubeValidate != false){
			if (getLPNo != "" && getLPNo != null){
				if(eventtype!='edit')
				{
					result = validateLPRange(getLPNo);
					if (result == "Y") {
						var validateLPNo = validateLPMaster(getLPNo,company,site);
						if(validateLPNo != null){
							alert('LP Already Exist');
							return false;
						}
						else{
							var str=ValidateLot(itemId);
							return str;
						}
					}

					else{
						alert('Invalid LP Range!');
						return false;
					}	
				}
				else
				{
					var str=ValidateLot(itemId);
					return str;
				}
			}
			else{
				alert('Invalid LP No');
				return false;
			}
		}
		else{

			alert('Quantity exceeds location cube');		
			return false;
		}
	}
	else
	{
		alert("Please enter value for: Company");
		return false;
	}
}

/**
 * This function is to filter and get the pallet quantity from the item dimensions for
 * 	the item, uom and pack code
 * 	This returns the itemQuantity for the specific item
 * @param itemId
 * @param uomLevel
 * @param packCode
 * @returns {Number}
 */
function getPalletQuantityFromItemDims(itemId, packCode){
	var itemQuantity = 0;
	var filters = new Array();          
	filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is',itemId);
//	filters[1] = new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'anyof', [3]);		  
	filters[1] = new nlobjSearchFilter('custrecord_ebizpackcodeskudim', null, 'is',packCode);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizqty');
	columns[1] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim').setSort(true);//it fetches the records with highest uomlevel first.
	
	var searchItemResults = nlapiSearchRecord('customrecord_ebiznet_skudims',null, filters, columns);
	if(searchItemResults!=null){    		
		itemQuantity = searchItemResults[0].getValue('custrecord_ebizqty');  
	}    	
	
	return itemQuantity;	
}

/**
 * This function is to validate the LP number if it exists within the LP Range
 * 	The parameters that this function checks are:
 * 		LP Prefix, LP Begin number and LP End number
 */
function validateLPRange(getLPNo){
	var result="";
	
	var columnLPRange=new Array();
	columnLPRange[0]=new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix');
	var lpRangeSearchResults = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, null, columnLPRange);
	
	if (lpRangeSearchResults) {
		for (var i = 0; i < lpRangeSearchResults.length; i++) {
			try {
				var getLPPrefix = lpRangeSearchResults[i].getValue('custrecord_ebiznet_lprange_lpprefix');
				var recordId = lpRangeSearchResults[i].getId();

				var transaction = nlapiLoadRecord('customrecord_ebiznet_lp_range', recordId);
				var beginLPRange = transaction.getFieldValue('custrecord_ebiznet_lprange_begin');
				var endRange = transaction.getFieldValue('custrecord_ebiznet_lprange_end');
				var lpGenerationType = transaction.getFieldValue('custrecord_ebiznet_lprange_lpgentype');
				var lpType = transaction.getFieldValue('custrecord_ebiznet_lprange_lptype');

				if (lpType == '1') {
					var lpPrefix = transaction.getFieldValue('custrecord_ebiznet_lprange_lpprefix');
					var lpPrefixLen = getLPPrefix.length;
					var vLPLen = getLPNo.substring(0, lpPrefixLen).toUpperCase();
					if (vLPLen == getLPPrefix) {
						var varnum = getLPNo.substring(lpPrefixLen, getLPNo.length);
						if (varnum.length > endRange.length) {
							result = "N";
							break;
						}
						if ((parseFloat(varnum,10) < parseFloat(beginLPRange)) || (parseFloat(varnum,10) > parseFloat(endRange))) {
							result = "N";
							break;
						}
						else{
							result = "Y";
							break;
						}

					}
					else {
						result = "N";
					}
				}
			} 
			catch (err) {
			}
		}
	}
	return result;
}

/**
 * This function is to validate the LP # if it exists im the LP master
 */

function validateLPMaster(lpNo,company,site){
	var lpMasterFilters = new Array();				
	lpMasterFilters[0] = new nlobjSearchFilter('name', null, 'is', lpNo);
	lpMasterFilters[1] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_site', null, 'anyof',['@NONE@',site]);
	lpMasterFilters[2] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_company', null, 'anyof', ['@NONE@',company]);
	var lpMasterColumns = new Array();
	lpMasterColumns[0] = new nlobjSearchColumn('name');
	var lpMasterSearchResults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, lpMasterFilters,lpMasterColumns);		
 return lpMasterSearchResults;
}


/**
 * This function is to validate the location cube for the item, bin location and the quantity passed
 */
function validateLocationCube(quantity, itemValue, binLocation){
	
	var cube = 0.0;
	var baseUOMQty = 0.0;
	var remCube = 0.0;
	var itemUomCube = 0.0;
	var anotherItemCube = 0.0;

	var resultQty = quantity;

//	This filters are to fetch the item dimensions for the item and the bin location	
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemValue);		    
	filters[1] = new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
	columns[1] = new nlobjSearchColumn('custrecord_ebizqty');

	var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

	if(skuDimsSearchResults != null && skuDimsSearchResults.length > 0){
		for (var i = 0; i < skuDimsSearchResults.length; i++){
			var skuDim = skuDimsSearchResults[i];
			cube = skuDim.getValue('custrecord_ebizcube');
			baseUOMQty = skuDim.getValue('custrecord_ebizqty');
		}
	}

	if ((!isNaN(cube))){
		var uomqty = parseFloat(quantity)/parseFloat(baseUOMQty);
		itemUomCube = parseFloat(uomqty) * parseFloat(cube);
		var itemCube = itemUomCube.toString();

	} else {
		itemUomCube = 0;
		itemCube=0;
	}

	var LocRemCube = GetLocCube(binLocation);
	//alert('LocRemCube '+LocRemCube);
	var locRem = LocRemCube.toString();
	
	//alert('Loc remaincube '+locRem);

	if(eventtype=='edit')
	{
		if(parseFloat(QtyOldValue)>parseFloat(enteredQuantity)){
			remCube = parseFloat(locRem) + parseFloat(itemCube);
			//alert('remCube1 '+remCube);

		}
		else if(parseFloat(QtyOldValue)< parseFloat(enteredQuantity))
		{

			remCube = parseFloat(locRem) - parseFloat(itemCube);
			//alert('remCube2 '+remCube);
		}
	}
	else
	{
		//alert('parseFloat(locRem) '+parseFloat(locRem));
		//alert('parseFloat(itemCube) '+parseFloat(itemCube));
		remCube = parseFloat(locRem) - parseFloat(itemCube);
		//alert('remCube3 '+remCube);
	}

	
	var remainingCube = remCube.toString();
	//alert('remainingCube '+remainingCube);

	if (binLocation != null && binLocation != "" && remainingCube >= 0){
		return true;
	}
	else{
		return false;
	}
}

/**
 * @param LocID
 * @returns {Number}
 */
function GetLocCube(LocID){
	var remainingCube = 0;
	try {
		var fields = ['custrecord_remainingcube'];
		var columns= nlapiLookupField('customrecord_ebiznet_location',LocID,fields);
		remainingCube = columns.custrecord_remainingcube;
	}catch(exception) {
		nlapiLogExecution('DEBUG', 'GetLocCube:Lookup for remainingCube failed', exception);
	}
	nlapiLogExecution('DEBUG', 'GetLocCube:Remaining Cube for Location', remainingCube);
	return remainingCube;
}

 
/**
 * this function is to validate the batch field if the item is of batch type
 * and check whether the batch# is belongs to same batch item or not.
 * created by suman.
 * @param itemid
 * @returns {Boolean}
 */
function ValidateLot(itemid)
{
	var ItemType = nlapiLookupField('item', itemid, 'recordType');
	try{
		if(ItemType=='lotnumberedinventoryitem'||ItemType=='lotnumberedassemblyitem'||ItemType=='serializedinventoryitem')
		{
			var LOT=nlapiGetFieldValue('custrecord_ebiz_inv_lot');
			if(LOT==null||LOT=="")
			{
				alert('LOT# cant be left empty for LOT Item');
				return false;
			}
			else
			{
				var filterBatchEntry=new Array();
				filterBatchEntry.push(new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',itemid));

				var columnBatchEntry=new Array();
				columnBatchEntry[0]=new nlobjSearchColumn('custrecord_ebizlotbatch');
				columnBatchEntry[1]=new nlobjSearchColumn('internalid');
				var searchrecordBatchEntry=nlapiSearchRecord('customrecord_ebiznet_batch_entry',null,filterBatchEntry,columnBatchEntry);

				if(searchrecordBatchEntry!=null&&searchrecordBatchEntry!="")
				{
					var checkflag=0;
					var BatchTxt=nlapiGetFieldText('custrecord_ebiz_inv_lot');
					var BatchId=nlapiGetFieldValue('custrecord_ebiz_inv_lot');
					for ( var count = 0; count < searchrecordBatchEntry.length; count++) {
						nlapiLogExecution('ERROR','ONcomparing',BatchId+'='+searchrecordBatchEntry[count].getValue('custrecord_ebizlotbatch'));
						if((BatchTxt==searchrecordBatchEntry[count].getValue('custrecord_ebizlotbatch'))&&(BatchId==searchrecordBatchEntry[count].getValue('internalid')))
						{
							checkflag=1;
						}
					}
					if(checkflag!=1)
					{
						alert('LOT# doesnt matches with the Item Specified');
						return false;
					}
					else
						return true;
				}
			}
		}
		else
			return true;
	}
	catch(exe)
	{
		nlapiLogExecution('ERROR','error in the ValidateLot fn',exe); 
	}
}