/***************************************************************************
����������� eBizNET Solutions Inc
 ****************************************************************************
 *
 *� $Source: /cvs/products/eBizNET4.5/EoN/RF/Inventory/Suitelet/ebiz_RF_ReplenishmentMenu.js,v $
 *� $Revision: 1.2.4.4.4.2.4.4 $
 *� $Date: 2014/06/13 09:31:49 $
 *� $Author: skavuri $
 *� $Name: t_NSWMS_2014_1_3_125 $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_RF_ReplenishmentMenu.js,v $
 *� Revision 1.2.4.4.4.2.4.4  2014/06/13 09:31:49  skavuri
 *� Case# 20148882 (added Focus Functionality for Textbox)
 *�
 *� Revision 1.2.4.4.4.2.4.3  2014/05/30 00:34:23  nneelam
 *� case#  20148622
 *� Stanadard Bundle Issue Fix.
 *�
 *� Revision 1.2.4.4.4.2.4.2  2013/04/17 16:02:35  skreddy
 *� CASE201112/CR201113/LOG201121
 *� added meta tag
 *�
 *� Revision 1.2.4.4.4.2.4.1  2013/03/05 14:46:44  skreddy
 *� CASE201112/CR201113/LOG201121
 *� Code merged from Lexjet production as part of Standard bundle
 *�
 *� Revision 1.2.4.4.4.2  2012/09/26 12:37:08  grao
 *� CASE201112/CR201113/LOG201121
 *� Converting Multi language without small characters
 *�
 *� Revision 1.2.4.4.4.1  2012/09/21 15:01:30  grao
 *� CASE201112/CR201113/LOG201121
 *� Converting Multilanguage
 *�
 *� Revision 1.2.4.4  2012/08/09 07:05:31  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Issue related to Round turn in scanning other Items.
 *�
 *� Revision 1.2.4.3  2012/03/16 14:08:54  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Disable-button functionality is been added.
 *�
 *� Revision 1.2.4.2  2012/02/24 21:14:31  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *�
 *� putaway confirmation
 *�
 *� Revision 1.2.4.1  2012/02/21 13:26:31  schepuri
 *� CASE201112/CR201113/LOG201121
 *� function Key Script code merged
 *�
 *� Revision 1.3  2012/02/21 11:48:45  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Added FunctionkeyScript
 *�
 */
function ReplenyMenu(request, response){
	if (request.getMethod() == 'GET') {

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4,st5;
		if( getLanguage == 'es_ES')
		{

			st0 = "REPOSICI&#211;N DE MEN&#218;";
			st1 = "REPOSICI&#211;N";			
			st2 = "ENVIAR";
			st3 = "ANTERIOR";

		}
		else
		{
			st0 = "REPLENISHMENT MENU";
			st1 = "REPLENISHMENT";			
			st2 = "SEND";
			st3 = "PREV";


		}		

		var functionkeyHtml=getFunctionkeyScript('_rfinventorymenu'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('selectoption').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rfinventorymenu' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> 1." + st1;
		html = html + "				</td>";
		html = html + "			</tr>";
		/* html = html + "			<tr>";
        html = html + "				<td align = 'left'> 2. CLUSTER REPLENISHMENT";
        html = html + "				</td>";
        html = html + "			</tr> ";*/
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='selectoption' id='selectoption' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st3 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "			<td>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "			</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('selectoption').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		// This variable is to get the option selected. If option selected is 1, it navigates to
		// the check-in screen. If the option selected is 2, it navigates to the putaway screen.
		var optedField = request.getParameter('selectoption');

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');
		var RPLNarray = new Array();  

		var getLanguage = request.getParameter('hdngetLanguage');
		RPLNarray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR', 'getLanguage', RPLNarray["custparam_language"]);


		var st4,st5;
		if( getLanguage == 'es_ES')
		{
			st4 = "RPLN MEN&#218;";
			st5 = "OPCI&#211;N V&#193;LIDA";
		}
		else
		{
			st4 = "RPLNMENU";
			st5 = "INVALID OPTION";


		}

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to RF Main Menu.
		if (optedEvent == 'F7') {
			response.sendRedirect('SUITELET', 'customscript_rf_main_menu', 'customdeploy_rf_main_menu_di', false, RPLNarray);
		}
		else 
			//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
			//            if (optedEvent != '' && optedEvent != null) {
			if (optedField == '1') {
				response.sendRedirect('SUITELET', 'customscript_rf_replenishment_no', 'customdeploy_rf_replenishment_no', false, RPLNarray);
			}
			else 
				if (optedField == '2') {
					response.sendRedirect('SUITELET', 'customscript_replen_clusteno', 'customdeploy_replen_clusteno', false, RPLNarray);
				}
				else if((optedField!='1' && optedField!='2') || (optedField==''))
				{
					RPLNarray["custparam_screenno"]=st4;
					RPLNarray["custparam_error"] = st5;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, RPLNarray);
				}
		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}
}
