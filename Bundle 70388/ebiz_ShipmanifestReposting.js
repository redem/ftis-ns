/***************************************************************************
       eBizNET 
	   eBizNET Solutions Inc
****************************************************************************
*
*  $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_ShipmanifestReposting.js,v $
*  $Revision: 1.1.2.3.4.1 $
*  $Date: 2015/11/12 16:46:45 $
*  $Author: rmalraj $
*  $Name: t_WMS_2015_2_StdBundle_1_122 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_ShipmanifestReposting.js,v $
*  Revision 1.1.2.3.4.1  2015/11/12 16:46:45  rmalraj
*  201413120 In Ship manifesto Reposting when we are creating ship manifesto record for already created order, system is showing 2 Back buttons
*
*  Revision 1.1.2.3  2014/05/15 15:50:49  sponnaganti
*  Case# 20148368
*  Standard Bundle Issue fix
*
*  Revision 1.1.2.2  2013/05/14 17:37:50  svanama
*  CASE201112/CR201113/LOG201121
*  shipmanifest reposting
*
*
****************************************************************************/

 function createShipmanifestCustomRecord(request, response)
{
	if (request.getMethod() == 'GET') 
	{
		nlapiLogExecution('ERROR', "Get", 'Get');
		var form = nlapiCreateForm('ShipManifest Record Creation');
		var foOrder =form.addField('custpage_fonumber','text','FulFillment Order#');
		var cartonnumber =form.addField('custpage_carton','text','Carton #');
		form.addField('custpage_hidback','text','backbuttonvalue').setDisplayType('hidden').setDefaultValue('true');
		var button = form.addSubmitButton('Generate');
		form.addButton('custpage_displaybtn','Back','Display()');
		form.setScript('customscript_labelclientscript_cl');
		response.writePage(form);
	}
	else
	{
		var form = nlapiCreateForm('ShipManifest Record Creation');
		var msg = form.addField('custpage_message','inlinehtml', null, null, null).setLayoutType('endrow');

		var vhdnBackValue = request.getParameter('custpage_hidback');
		var foOrder = request.getParameter('custpage_fonumber');
		nlapiLogExecution('ERROR', "foOrderPOST", foOrder);
		var cartonnumber = request.getParameter('custpage_carton');
		form.setScript('customscript_labelclientscript_cl');
		nlapiLogExecution('ERROR', "cartonnumber", cartonnumber);
		nlapiLogExecution('ERROR', "vhdnBackValue", vhdnBackValue);
		var button = form.addSubmitButton('Back');
		
		
		//form.addButton('custpage_displaybtn','Back','Display()');
		if((vhdnBackValue=='true'))
		{
			if(((foOrder!=null)&&(foOrder!=""))||((cartonnumber!=null)&&(cartonnumber!="")))
			{

				nlapiLogExecution('ERROR', "postcartonnumber",cartonnumber);
				var qtyfilters = new Array();
				if(foOrder!="")
				{
					qtyfilters.push(new nlobjSearchFilter('name', null, 'is',foOrder));
				}
				if(cartonnumber!="")
				{
					qtyfilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is',cartonnumber));
				}
				form.addField('custpage_hidback','text','backbuttonvalue').setDisplayType('hidden').setDefaultValue('true');

				qtyfilters.push( new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof',['28'])); 
				qtyfilters.push( new nlobjSearchFilter('custrecord_tasktype', null, 'anyof',[3]));
				qtyfilters.push( new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isnotempty'));//201411445 ,modified by dinesh
				var qtycolumns = new Array();
				qtycolumns[0] = new nlobjSearchColumn('custrecord_container_lp_no', null, 'group');
				qtycolumns[1] = new nlobjSearchColumn('custrecord_ebiz_order_no', null, 'MAX');
				var qtysearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, qtyfilters, qtycolumns);
				if(qtysearchresults !=null)
				{
					var packcount=0;
					for(var g=0;g<qtysearchresults.length;g++)
					{
						nlapiLogExecution('ERROR', "qtysearchresults", qtysearchresults.length);
						packcount=parseInt(packcount)+1;
						var splitordno=''; 
						var OrderNumber;
						var vtotalpackcount=qtysearchresults.length;
						var vContainerLpNo=qtysearchresults[g].getValue('custrecord_container_lp_no', null, 'group');
						var vSalesOrder=qtysearchresults[g].getValue('custrecord_ebiz_order_no',null,'MAX');
						nlapiLogExecution('ERROR','vSalesOrder',vSalesOrder);
						if(vSalesOrder!=null)
						{
							OrderNumber=vSalesOrder.split('#');
						}
						splitordno=OrderNumber[1];
						var ordertype=OrderNumber[0];
						ordertype=ordertype.replace(/\s/g,'');
						//case# 20148368 starts 
						if(ordertype=="SalesOrder" || ordertype=="Order")			
						{//case# 20148368 ends
							ordertype="salesorder";
						}
						else if(ordertype=='TransferOrder')
						{
							ordertype='transferorder';
						}

						nlapiLogExecution('ERROR','ordertype',ordertype);
						var vsalesOrderId;
						if ((splitordno != "")&&(splitordno!=null))
						{
							var filtersso = new Array();
							filtersso[0] = new nlobjSearchFilter('mainline', null, 'is', 'T');
							filtersso[1] = new nlobjSearchFilter('tranid', null, 'is',splitordno); 
							var columnso = new Array();
							columnso.push(new nlobjSearchColumn('Internalid'));
							var searchSalesOrderRecord = nlapiSearchRecord(ordertype, null, filtersso,columnso);
							vsalesOrderId=searchSalesOrderRecord[0].getValue('Internalid');
						}
						 
nlapiLogExecution('ERROR','vsalesOrderId',vsalesOrderId);

						var CarrieerName=getCarrierservice(vsalesOrderId);
						//CreateShippingManifestRecord(vsalesOrderId,vContainerLpNo,CarrieerName,null,null,packcount,vtotalpackcount);
						//nlapiLogExecution('ERROR', "vContainerLpNo", vContainerLpNo);
						//msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'ShipManifest Records Created Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '75%', null, null, null);</script></div>");
						if((cartonnumber!=""))
						{
							if(CheckShipmanifestCustomRecord(vContainerLpNo)!='T')
							{
								CreateShippingManifestRecord(vsalesOrderId,vContainerLpNo,CarrieerName,null,null,packcount,vtotalpackcount);
								nlapiLogExecution('ERROR', "vContainerLpNo", vContainerLpNo);
								msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'ShipManifest Records Created Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '75%', null, null, null);</script></div>");
							}
							else
							{
								nlapiLogExecution('ERROR', "vContainerLpNo", vContainerLpNo);
								msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'ERROR', 'Already Containerlp Exits in Shipmanifest CustomRecord', NLAlertDialog.TYPE_HIGH_PRIORITY,  '75%', null, null, null);</script></div>");
							}
						}
						else
						{
							nlapiLogExecution('ERROR', "IfcartonnumberNull", "IfcartonnumberNull");
							CreateShippingManifestRecord(vsalesOrderId,vContainerLpNo,CarrieerName,null,null,packcount,vtotalpackcount);

							msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'ShipManifest Records Created Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '75%', null, null, null);</script></div>");

						}
					}						
					response.writePage(form);

				}
				else
				{
					nlapiLogExecution('ERROR', "Backbutton clicked after ship manifest record creation part");
					msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'ERROR', 'There is no PackTask Records in OpenTask', NLAlertDialog.TYPE_HIGH_PRIORITY,  '75%', null, null, null);</script></div>");
					response.writePage(form);
				}
			}
			else
			{
				nlapiLogExecution('ERROR', "null values",vhdnBackValue);
				if(vhdnBackValue==false)
				{
					nlapiLogExecution('ERROR', "Backbutton clicked else part");
					response.sendRedirect('SUITELET','customscript_shipmanifestrecordcreation', 'customdeploy_shipmanifestrecordcreation',false,null);	
					form.addField('custpage_hidback','text','backbuttonvalue').setDisplayType('hidden').setDefaultValue('true');
				}
				else
				{
					msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'ERROR', 'There is no PackTask Records in OpenTask', NLAlertDialog.TYPE_HIGH_PRIORITY,  '75%', null, null, null);</script></div>");
					response.sendRedirect('SUITELET','customscript_shipmanifestrecordcreation', 'customdeploy_shipmanifestrecordcreation',false,null);
					response.writePage(form);
				}
			}
		}
		else
		{
			nlapiLogExecution('ERROR', "AfterElse",'AfterElse');
			nlapiLogExecution('ERROR', "AcvhdnBackValueinsideElse",vhdnBackValue);
			response.sendRedirect('SUITELET','customscript_shipmanifestrecordcreation', 'customdeploy_shipmanifestrecordcreation',false,null);	
			form.addField('custpage_hidback','text','backbuttonvalue').setDisplayType('hidden').setDefaultValue('true');
		}
	}
}
function CheckShipmanifestCustomRecord(vContLpNo)
{
	nlapiLogExecution('ERROR', 'Into CheckShipmanifestCustomRecord',vContLpNo);	
	var IsContLpExist='F';
	var vIndSpace=vContLpNo.indexOf(' ');
	nlapiLogExecution('ERROR', 'CheckShipmanifestCustomRecord',vIndSpace);
	try
	{
		var filter = new Array();
		filter.push(new nlobjSearchFilter('custrecord_ship_contlp',null,'is',vContLpNo));
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ship_orderno');
		var manifestList= nlapiSearchRecord('customrecord_ship_manifest',null,filter,columns);
		if(manifestList!=null && manifestList.length>0)
			IsContLpExist='T';		
	}
	catch(e)
	{
		nlapiLogExecution('ERROR', 'unexpected error in CheckShipmanifestCustomRecord');	
	}
	nlapiLogExecution('ERROR', 'Out of CheckShipmanifestCustomRecord',IsContLpExist);	
	return IsContLpExist;
}

function getCarrierservice(vebizOrdNo)
{
	var columns = new Array();
	var filters = new Array();
	var searchresultscarr;
	var vStgCarrierName="";
	nlapiLogExecution('ERROR', 'vebizOrdNo', vebizOrdNo);
	filters.push(new nlobjSearchFilter('custrecord_ns_ord', null, 'is',vebizOrdNo));

	columns[0] = new nlobjSearchColumn('custrecord_do_wmscarrier');	
	columns[1] = new nlobjSearchColumn('custrecord_do_carrier');				
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_wave');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_ordline_customizetype');
	columns[4] = new nlobjSearchColumn('custrecord_do_order_type');

	var searchresultsord = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
	if(searchresultsord!=null && searchresultsord!='' && searchresultsord.length>0)
	{
		var Carrier = searchresultsord[0].getValue('custrecord_do_carrier');
		var customizetype = searchresultsord[0].getValue('custrecord_ebiz_ordline_customizetype');
		var ordertype = searchresultsord[0].getValue('custrecord_do_order_type');
		var Carriertext = searchresultsord[0].getText('custrecord_do_carrier');
		nlapiLogExecution('ERROR', 'CarrierId',Carrier); 
		nlapiLogExecution('ERROR', 'Carriertext',Carriertext);
		nlapiLogExecution('ERROR', 'customizetype',customizetype); 
		nlapiLogExecution('ERROR', 'ordertype',ordertype); 
		if(Carrier!=null && Carrier!='')
		{
			var filterscarr = new Array();
			var columnscarr = new Array();

			filterscarr.push(new nlobjSearchFilter('custrecord_carrier_nsmethod', null, 'anyof',Carrier));
			filterscarr.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

			columnscarr[0] = new nlobjSearchColumn('custrecord_carrier_type');	
			columnscarr[1] = new nlobjSearchColumn('custrecord_carrier_service_level');	
			columnscarr[2] = new nlobjSearchColumn('id');
			columnscarr[3] = new nlobjSearchColumn('custrecord_carrier_name');
			searchresultscarr = nlapiSearchRecord('customrecord_ebiznet_carrier', null, filterscarr, columnscarr);
		}
		if(searchresultscarr!=null)
		{
			vStgCarrierName=searchresultscarr[0].getValue('custrecord_carrier_type');
		}
	}

	return vStgCarrierName;
}