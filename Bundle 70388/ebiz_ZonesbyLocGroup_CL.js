/***************************************************************************
????????????????????????????eBizNET
?????????????????????eBizNET Solutions Inc
 ****************************************************************************
 *
 *? $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Masters/Client/ebiz_ZonesbyLocGroup_CL.js,v $
 *? $Revision: 1.3.4.1 $
 *? $Date: 2012/02/09 16:10:57 $
 *? $Author: snimmakayala $
 *? $Name: t_NSWMS_LOG201121_26 $
 *
 * DESCRIPTION
 *? Functionality
 *
 * REVISION HISTORY
 *? $Log: ebiz_ZonesbyLocGroup_CL.js,v $
 *? Revision 1.3.4.1  2012/02/09 16:10:57  snimmakayala
 *? CASE201112/CR201113/LOG201121
 *? Physical and Virtual Location changes
 *?
 *? Revision 1.4  2012/02/09 14:43:42  snimmakayala
 *? CASE201112/CR201113/LOG201121
 *? Physical and Virtual Location changes
 *?
 *? Revision 1.3  2012/01/10 07:19:33  spendyala
 *? CASE201112/CR201113/LOG201121
 *?  added code to remove space.
 *?
 *
 ****************************************************************************/

function fnCheckLocgrpZone(type)
{
	var varZone =  nlapiGetFieldValue('custrecordcustrecord_putzoneid');  
	var vLocgroup	= nlapiGetFieldValue('custrecord_locgroup_no');  	 	 
	var vSeqNo	= nlapiGetFieldValue('custrecord_zone_seq');  	 	 
	var vId = nlapiGetFieldValue('id');  

	if(varZone != "" && varZone != null && vLocgroup != "" && vLocgroup != null && vSeqNo != "" && vSeqNo != null && vId != ""  && vId != null)
	{
		var filters = new Array();

		filters[0] = new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'anyof',[varZone]);	 
		filters[1] = new nlobjSearchFilter('custrecord_locgroup_no', null, 'anyof',[vLocgroup]);

		if(vId!=null && vId!="")
		{
			filters[2] = new nlobjSearchFilter('internalid', null, 'noneof',vId);	
		}

		var searchresults = nlapiSearchRecord('customrecord_zone_locgroup', null, filters);


		if (searchresults != null && searchresults.length > 0) 
		{
			alert("This Zone and Location Group Combination already exists.");
			return false;
		}

		else
		{
			var filters1 = new Array();

			filters1[0] = new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'anyof',[varZone]);	 
			filters1[1] = new nlobjSearchFilter('custrecord_zone_seq', null, 'equalTo',[vSeqNo]);

			if(vId!=null && vId!="")
			{
				filters1[2] = new nlobjSearchFilter('internalid', null, 'noneof',vId);	
			}


			var searchresults1 = nlapiSearchRecord('customrecord_zone_locgroup', null, filters1);

			if (searchresults1 != null && searchresults1.length > 0) 
			{
				alert("This Zone and Seq Combination already exists.");
				return false;
			}
			else
			{
				return true;
			}
		} 
	}
	else
	{
		return true;
	}


}