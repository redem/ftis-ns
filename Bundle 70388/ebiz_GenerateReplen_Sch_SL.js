/***************************************************************************
 eBizNET
 7HILLS BUSINESS SOLUTION LTD
 ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/Attic/ebiz_GenerateReplenQb_New_SL.js,v $
 *     	   $Revision: 1.1.2.3 $
 *     	   $Date: 2015/08/04 15:30:06 $
 *     	   $Author: skreddy $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 *$Log: ebiz_GenerateReplenQb_New_SL.js,v $
 *Revision 1.1.2.3  2015/08/04 15:30:06  skreddy
 *Case# 201413769
 *2015.2 compatibility issue fix
 *
 *Revision 1.1.2.2  2015/07/09 16:45:41  grao
 *LP  issue fixes  201413259
 *
 *Revision 1.1.2.1  2015/07/07 20:17:49  grao
 *2015.2  issue fixes  201413259
 *
 *Revision 1.1.2.3  2012/09/14 10:31:16  rrpulicherla
 *CASE201112/CR201113/LOG201121
 *Replen changes
 *
 *Revision 1.1.2.2  2012/09/04 22:35:20  rrpulicherla
 *CASE201112/CR201113/LOG201121
 *Replen issues
 *
 *Revision 1.1.2.1  2012/03/22 08:11:10  rrpulicherla
 *CASE201112/CR201113/LOG201121
 *
 * Replenishment
 *
 *Revision 1.5  2011/08/20 07:22:41  vrgurujala
 *CASE201112/CR201113/LOG201121
 *
 *Revision 1.4  2011/06/15 08:01:04  skota
 *CASE201112/CR201113/LOG201121
 *code changes to check 'Active' status while populating company drop down and removed the condition 'Math.min(500)' while populating item and location drop downs
 *
 *****************************************************************************/
function GenerateReplenSch(type){


	nlapiLogExecution('ERROR','Scheduler Started');

	nlapiLogExecution('ERROR','type', type);

	var context = nlapiGetContext(); 

	var item="";
	var itemgroup="";
	var itemfamily="";
	var whlocation="";

	try{

		item = context.getSetting('SCRIPT', 'custscript_item');
		itemgroup = context.getSetting('SCRIPT', 'custscript_itemgrp');
		itemfamily = context.getSetting('SCRIPT', 'custscript_itemfamily');
		whlocation=context.getSetting('SCRIPT', 'custscript_loc');


		nlapiLogExecution('ERROR','item', item);
		nlapiLogExecution('ERROR','itemgroup', itemgroup);
		nlapiLogExecution('ERROR','itemfamily', itemfamily);
		nlapiLogExecution('ERROR','whlocation', whlocation);


		var vItemArr=new Array();
		if(item != null && item != "")
		{
			vItemArr = item.split('');
		}

		nlapiLogExecution('ERROR','vItemArr', vItemArr);

		nlapiLogExecution('ERROR','Into CreateDynamicReplenishmentNew','');
		var context = nlapiGetContext();
		nlapiLogExecution('ERROR','Remaining Usage 1',context.getRemainingUsage());
		//if ( type != 'scheduled') return; 

		var pfBinLocationId = '';
		var pfItem = '';
		var pfMaximumQuantity = 0;
		var pfMaximumPickQuantity = 0;
		var pffixedlp = '';
		var pfstatus = '';
		var replenrule = '';
		var pfRoundQty = 0;
		var pfWHLoc = '';
		var pfpickzoneid = '';
		var replnArray = new Array();

		var replenRulesList = getListForAllReplenZones();
		nlapiLogExecution('ERROR','Remaining Usage 1',context.getRemainingUsage());
		var vintrid=0;
		var vinternalid=0;


		var filters = new Array();	
		filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null,'anyof', ['25']));
		if(vItemArr!=null && vItemArr!='')
			filters.push(new nlobjSearchFilter('custrecord_ebiz_linesku', null,'anyof', vItemArr));
		if(whlocation!=null && whlocation!='')
			filters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null,'anyof', [whlocation]));
		if(itemgroup!=null && itemgroup!='')
			filters.push(new nlobjSearchFilter('custitem_item_group', 'custrecord_ebiz_linesku','anyof', itemgroup));
		if(itemfamily!=null && itemfamily!='')
			filters.push(new nlobjSearchFilter('custitem_item_family', 'custrecord_ebiz_linesku','anyof', itemfamily));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ord_qty',null,'sum');
		columns[1] = new nlobjSearchColumn('custrecord_linesku_status',null, 'group');
		columns[2] = new nlobjSearchColumn('custrecord_ebiz_linesku',null,'group');

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);

		if(searchresults != null && searchresults != '')
			nlapiLogExecution('ERROR','Open Pick Tasks',searchresults.length);

		if(searchresults != null && searchresults != ''&& searchresults.length>0)
		{
			nlapiLogExecution('ERROR','Open Pick Tasks',searchresults.length);

			for (var s = 0; s < searchresults.length; s++) 
			{
				vinternalid = searchresults[s].getId();

				if(context.getRemainingUsage()<500)
				{
					nlapiLogExecution('ERROR','Remaining Usage',context.getRemainingUsage());
					break;
				}

				var vqty = searchresults[s].getValue('custrecord_ord_qty',null,'sum');
				var item = searchresults[s].getValue('custrecord_ebiz_linesku',null,'group');
				var itemStatus = searchresults[s].getValue('custrecord_linesku_status',null, 'group');

				nlapiLogExecution('ERROR','vqty',vqty);
				nlapiLogExecution('ERROR','item',item);
				nlapiLogExecution('ERROR','itemStatus',itemStatus);

				var replendone = IsReplenDone(replnArray,itemStatus,item);

				nlapiLogExecution('ERROR','replendone',replendone);

				if(replendone == false)
				{
					var itemStatuscarr = new Array();
					var itemarr = new Array();

					itemStatuscarr.push(itemStatus);
					itemarr.push(item);				

					var taskfilters = new Array();

					taskfilters.push(new nlobjSearchFilter('custrecord_pickfacesku', null,'anyof', itemarr)); 
					taskfilters.push(new nlobjSearchFilter('custrecord_pickface_itemstatus', null,'anyof', itemStatuscarr)); 
					taskfilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

					var taskcolumns = new Array();

					taskcolumns[0] = new nlobjSearchColumn('custrecord_pickfacesku');
					taskcolumns[1] = new nlobjSearchColumn('custrecord_replenqty');
					taskcolumns[2] = new nlobjSearchColumn('custrecord_maxqty');
					taskcolumns[3] = new nlobjSearchColumn('custrecord_pickbinloc');
					taskcolumns[4] = new nlobjSearchColumn('custrecord_pickruleid');
					taskcolumns[5] = new nlobjSearchColumn('custrecord_minqty');    
					taskcolumns[6] = new nlobjSearchColumn('custrecord_pickface_location');
					taskcolumns[7] = new nlobjSearchColumn('custrecord_pickface_ebiz_lpno');
					taskcolumns[8] = new nlobjSearchColumn('custrecord_maxpickqty');
					taskcolumns[9] = new nlobjSearchColumn('custrecord_pickface_itemstatus');
					taskcolumns[10] = new nlobjSearchColumn('custrecord_pickface_location');
					taskcolumns[11] = new nlobjSearchColumn('custrecord_roundqty');
					taskcolumns[12] = new nlobjSearchColumn('custrecord_pickface_location');	
					taskcolumns[13] = new nlobjSearchColumn('custrecord_inboundlocgroupid', 'custrecord_pickbinloc');
					taskcolumns[14] = new nlobjSearchColumn('custrecord_pickzone');
					taskcolumns[15] = new nlobjSearchColumn('custrecord_autoreplen');

					var pfLocationResults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, taskfilters, taskcolumns); // Closed Task
					if(pfLocationResults!= null && pfLocationResults!= '' && pfLocationResults.length>0)
					{
						nlapiLogExecution('ERROR','Pick Face Locations',pfLocationResults.length);

						for (var z = 0; z < pfLocationResults.length; z++) 
						{
							pfBinLocationId = pfLocationResults[z].getValue('custrecord_pickbinloc');
							pfItem = pfLocationResults[z].getValue('custrecord_pickfacesku');
							pfMaximumQuantity = pfLocationResults[z].getValue('custrecord_maxqty');
							pfMaximumPickQuantity = pfLocationResults[z].getValue('custrecord_maxpickqty');
							pffixedlp = pfLocationResults[z].getValue('custrecord_pickface_ebiz_lpno');
							pfstatus=pfLocationResults[z].getValue('custrecord_pickface_itemstatus');
							replenrule=pfLocationResults[z].getValue('custrecord_pickruleid');
							pfRoundQty=pfLocationResults[z].getValue('custrecord_roundqty');
							pfWHLoc=pfLocationResults[z].getValue('custrecord_pickface_location');
							pfpickzoneid = pfLocationResults[z].getValue('custrecord_pickzone');					
							replenQty=pfLocationResults[z].getValue('custrecord_replenqty');
						}

						var str = 'pfBinLocationId. = ' + pfBinLocationId + '<br>';
						str = str + 'pfItem. = ' + pfItem + '<br>';	
						str = str + 'pfMaximumQuantity. = ' + pfMaximumQuantity + '<br>';
						str = str + 'pfMaximumPickQuantity. = ' + pfMaximumPickQuantity + '<br>';
						str = str + 'pffixedlp. = ' + pffixedlp + '<br>';
						str = str + 'pfstatus. = ' + pfstatus + '<br>';
						str = str + 'replenrule. = ' + replenrule + '<br>';
						str = str + 'pfRoundQty. = ' + pfRoundQty + '<br>';
						str = str + 'pfWHLoc. = ' + pfWHLoc + '<br>';
						str = str + 'pfpickzoneid. = ' + pfpickzoneid + '<br>';
						str = str + 'replenQty. = ' + replenQty + '<br>';

						nlapiLogExecution('ERROR', 'Pickface Details1', str);

						var replenItemArray =  new Array();
						var currentRow = new Array();
						var j = 0;
						var idx = 0;
						var putawatqty=0;
						var pickfaceQty=0;
						var openreplensqty=0;

						var pickqty=0;
						var searchpickyqty=getOpenPickqty(pfItem,pfBinLocationId);
						if(searchpickyqty!=null && searchpickyqty!='')
						{
							pickqty = searchpickyqty[0].getValue('custrecord_expe_qty', null, 'sum');
						}

						var serchreplensqty=getopenreplens(pfItem,pfBinLocationId,pfWHLoc);
						if(serchreplensqty!=null && serchreplensqty!='')
						{
							openreplensqty = serchreplensqty[0].getValue('custrecord_expe_qty', null, 'sum');
						}

						var searchputawyqty=getOpenputawayqty(pfItem,pfBinLocationId);
						if(searchputawyqty!=null && searchputawyqty!='')
						{
							putawatqty = searchputawyqty[0].getValue('custrecord_expe_qty', null, 'sum');
						}

						var searchpickfaceQty = getQOHForAllSKUsinPFLocation(pfBinLocationId,pfItem,pfWHLoc);
						if(searchpickfaceQty!=null && searchpickfaceQty!='')
						{
							pickfaceQty=searchpickfaceQty[0].getValue('custrecord_ebiz_qoh',null,'sum');
						}


						if(pfRoundQty == null || pfRoundQty == '' || isNaN(pfRoundQty))
							pfRoundQty=1;

						if(openreplensqty == null || openreplensqty == '')
							openreplensqty=0;

						if(pickqty == null || pickqty == '')
							pickqty=0;

						if(putawatqty == null || putawatqty == '')
							putawatqty=0;

						if(pickfaceQty == null || pickfaceQty == '')
							pickfaceQty=0;

						if(parseInt(pickfaceQty)<0)
							pickfaceQty=0;

						var str = 'putawatqty. = ' + putawatqty + '<br>';
						str = str + 'pickqty. = ' + pickqty + '<br>';	
						str = str + 'pickfaceQty. = ' + pickfaceQty + '<br>';
						str = str + 'replenQty. = ' + replenQty + '<br>';
						str = str + 'openreplensqty. = ' + openreplensqty + '<br>';
						str = str + 'pfRoundQty. = ' + pfRoundQty + '<br>';

						nlapiLogExecution('ERROR', 'Pickface Details2', str);

						var tempQty = parseInt(pfMaximumQuantity) -  (parseInt(openreplensqty) + parseInt(putawatqty) + parseInt(pickfaceQty) - parseInt(pickqty));

						nlapiLogExecution('ERROR','tempQty1',tempQty);

						var tempwithRoundQty = Math.floor(parseFloat(tempQty)/parseFloat(pfRoundQty));

						if(tempwithRoundQty == null || tempwithRoundQty == '' || isNaN(tempwithRoundQty))
							tempwithRoundQty=0;

						nlapiLogExecution('ERROR','tempwithRoundQty',tempwithRoundQty);

						tempQty = parseInt(pfRoundQty)*parseInt(tempwithRoundQty);

						nlapiLogExecution('ERROR','tempQty2',tempQty);

						if(parseInt(tempQty)>0)
						{
							var replenTaskCount = Math.ceil(parseInt(tempQty) / parseInt(replenQty));

							nlapiLogExecution('ERROR','replenTaskCount',replenTaskCount);

							currentRow = [pfItem, replenQty, replenrule, pfMaximumQuantity, pfMaximumPickQuantity, pickfaceQty, pfWHLoc, 
							              pfBinLocationId, tempQty, replenTaskCount, pffixedlp,pfstatus,itemStatus,pfRoundQty];
							if(idx == 0)
								idx = currentRow.length;

							replenItemArray[j++] = currentRow;
							var taskCount;
							var zonesArray = new Array();

							var replenZoneList = "";
							var replenZoneTextList = "";
							var replenzonestatuslist="";
							var replenzonestatustextlist="";
							var Reportno = "";

							if (replenRulesList != null){
								for (var w = 0; w < replenRulesList.length; w++){

									if (w == 0){
										replenZoneList += replenRulesList[w].getValue('custrecord_replen_strategy_zone');
										replenZoneTextList += replenRulesList[w].getText('custrecord_replen_strategy_zone');
										replenzonestatuslist +=replenRulesList[w].getValue('custrecord_replen_strategy_itemstatus');
										replenzonestatustextlist +=replenRulesList[w].getText('custrecord_replen_strategy_itemstatus');
									} else {
										replenZoneList += ',';
										replenZoneList += replenRulesList[w].getValue('custrecord_replen_strategy_zone');

										replenZoneTextList += ',';
										replenZoneTextList += replenRulesList[w].getText('custrecord_replen_strategy_zone');

										replenzonestatuslist += ',';
										replenzonestatuslist += replenRulesList[w].getValue('custrecord_replen_strategy_itemstatus');

										replenzonestatustextlist += ',';
										replenzonestatustextlist += replenRulesList[w].getText('custrecord_replen_strategy_itemstatus');
									}
								}
							} 

							nlapiLogExecution('ERROR','replenZoneList',replenZoneList);

							var itemList = buildItemListForReplenProcessing(replenItemArray, j);

							nlapiLogExecution('ERROR','replen Item List',itemList);

							var locnGroupList = getZoneLocnGroupsList(replenZoneList,pfWHLoc);	
							var ZoneAndLocationGrp =getZoneLocnGroupsListforRepln(replenZoneList,pfWHLoc);
							nlapiLogExecution('DEBUG','ZoneAndLocationGrp',ZoneAndLocationGrp);

							nlapiLogExecution('ERROR','pfstatus',pfstatus);
							nlapiLogExecution('ERROR','Location Group List',locnGroupList);

							var inventorySearchResultsReplen = getItemDetailsforRepln(locnGroupList, itemList,replenzonestatuslist,pfWHLoc,null,pfstatus);
							nlapiLogExecution('ERROR','inventorySearchResultsReplen',inventorySearchResultsReplen);
							if ((inventorySearchResultsReplen != null && inventorySearchResultsReplen != "")){





								//Get the Stage Location in case of two step replenishment process

								var stageLocation='';

								nlapiLogExecution('ERROR','Reportno',Reportno);

								if(tempQty>0 && tempQty!='NAN')
								{
									// Get the report number
									if (Reportno == null || Reportno == "") {
										Reportno = GetMaxTransactionNo('REPORT');
										Reportno = Reportno.toString();
									}

									var vreplnpriority = 5;
									var result=	replenRuleZoneLocation(inventorySearchResultsReplen, replenItemArray, Reportno, stageLocation, pfWHLoc,'','',vreplnpriority,ZoneAndLocationGrp);
									nlapiLogExecution('ERROR','result',result);
									if(result[0]==true)
									{
										var curRow = [itemStatuscarr,item];
										replnArray.push(curRow);
										
										if(Reportno!=null && Reportno!='')
										var vSendmaiReplenReport = getfuncSendmailReplenReport(Reportno);
										
									}
									/*else
									{
										createFailedReplenTask(pfBinLocationId,pfItem,pfWHLoc,null,pfstatus);
									}*/
								}
							}
						}

						if(context.getRemainingUsage()<500)
						{
							nlapiLogExecution('ERROR','Remaining Usage',context.getRemainingUsage());
							break;
						}
					}
				}		
			}
		}

		nlapiLogExecution('ERROR','Remaining Usage at the end ',context.getRemainingUsage());	

		nlapiLogExecution('ERROR','Out of  CreateDynamicReplenishmentNew','');

	}
	catch(exp1) 
	{
		nlapiLogExecution('Debug', 'Exception in generatereplensch : ', exp1);	
	}
}

function createFailedReplenTask(pfLocid,pfItem,whLocation,WaveNo,pfstatus)
{
	nlapiLogExecution('ERROR', 'Into createFailedReplenTask');

	try
	{
		var customRecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		customRecord.setFieldValue('custrecord_tasktype', 8);
		customRecord.setFieldValue('custrecordact_begin_date', DateStamp());
		customRecord.setFieldValue('custrecord_actualbegintime', TimeStamp());
		customRecord.setFieldValue('custrecord_actbeginloc', pfLocid);                        
		customRecord.setFieldValue('custrecord_wms_status_flag', 26); 
		customRecord.setFieldValue('custrecord_upd_date', DateStamp());
		customRecord.setFieldValue('custrecord_ebiz_sku_no', pfItem);
		customRecord.setFieldValue('custrecord_sku', pfItem);                        
		customRecord.setFieldValue('name', pfItem);
		customRecord.setFieldValue('custrecord_notes','Insufficient Inventory');

		if(whLocation!=null && whLocation!='')
			customRecord.setFieldValue('custrecord_wms_location',whLocation);			

		if(WaveNo!=null && WaveNo!='')
			customRecord.setFieldValue('custrecord_ebiz_wave_no', WaveNo);

		if(pfstatus!=null && pfstatus!='')
			customRecord.setFieldValue('custrecord_sku_status', pfstatus);

		var openTaskRecordId = nlapiSubmitRecord(customRecord);
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR', 'Exception in createFailedReplenTask',exp);
	}

	nlapiLogExecution('ERROR', 'Out of createFailedReplenTask');
}

function IsReplenDone(replnArray,pitemstatus,item)
{
	nlapiLogExecution('ERROR', 'Into IsReplenDone');

	var replendone=false;

	if(replnArray!=null && replnArray!='' && replnArray.length>0)
	{
		for (var t = 0; t < replnArray.length; t++) {

			var replnitemstatus = replnArray[t][0];
			var replnitem = replnArray[t][1];

			if(pitemstatus == replnitemstatus && item == replnitem)
			{
				replendone = true;
				return replendone;
			}
		}
	}

	nlapiLogExecution('ERROR', 'Out of IsReplenDone');

	return replendone;
}
function getopenreplens(fulfilmentItem,pfLocationId,pfWHLoc)
{
	//nlapiLogExecution('ERROR','controlno',controlno);
	nlapiLogExecution('ERROR','fulfilmentItem',fulfilmentItem);
	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8])); // Replen task
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
	filters.push(new nlobjSearchFilter('custrecord_sku', null,'anyof' ,fulfilmentItem));
	filters.push(new nlobjSearchFilter('custrecord_actendloc', null,'anyof' ,pfLocationId));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return searchresults;
}
function getOpenputawayqty(item,actEndLocation)
{
	nlapiLogExecution('ERROR','actEndLocation',actEndLocation);
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', actEndLocation);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);				
	filters[2] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');
	filters[3] = new nlobjSearchFilter('custrecord_sku', null, 'anyof',item);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return searchresults;
}
function getQOHForAllSKUsinPFLocation(actEndLocation,ItemId,WHLoc){

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', actEndLocation));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_avl_qty', null, 'greaterthan', 0));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', ItemId));
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19','3']));

	var columns = new Array();	
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh',null,'sum');
	var pfSKUInvList = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

	return pfSKUInvList;
}
function getOpenPickqty(item,actEndLocation)
{
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', actEndLocation);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]);				
	filters[2] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');
	filters[3] = new nlobjSearchFilter('custrecord_sku', null, 'anyof',item);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	return searchresults;
}
function buildItemListForReplenProcessing(replenItemArray, itemArrayLength){
	var replenItemList = new Array();

	for (var p = 0; p < itemArrayLength; p++){
		replenItemList.push(replenItemArray[p][0]);
	}

	return replenItemList;
}

function getfuncSendmailReplenReport(repid){
	if(repid!=null && repid!='')
	{
		nlapiLogExecution('ERROR','INTOrepid1',repid);

		var wonumber;

		var filters = new Array();
		filters[0] = new nlobjSearchFilter('name', null, 'is', repid);
		filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_line_no');
		columns[1] = new nlobjSearchColumn('custrecord_lpno');
		columns[2] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[3] = new nlobjSearchColumn('custrecord_sku_status');
		columns[4] = new nlobjSearchColumn('custrecord_batch_no');
		columns[5] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[6] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[7] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');    	
		columns[8] = new nlobjSearchColumn('custrecord_sku'); //Retreive SKU Details
		columns[9] = new nlobjSearchColumn('custrecord_skudesc'); //Retreive SKU desc Details
		columns[10] = new nlobjSearchColumn('custrecord_packcode');
		columns[11] = new nlobjSearchColumn('custrecord_lotnowithquantity');
		columns[12] = new nlobjSearchColumn('custrecord_comp_id');
		columns[13] = new nlobjSearchColumn('custrecord_actendloc');
		columns[14] = new nlobjSearchColumn('custrecord_tasktype');
		columns[15] = new nlobjSearchColumn('custrecord_from_lp_no');

		columns[7].setSort(); // case# 201413592
        columns[8].setSort();

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);


		var url;
		/*var ctx = nlapiGetContext();
		nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
		if (ctx.getEnvironment() == 'PRODUCTION') 
		{
			url = 'https://system.netsuite.com';			
		}
		else if (ctx.getEnvironment() == 'SANDBOX') 
		{
			url = 'https://system.sandbox.netsuite.com';				
		}*/
		nlapiLogExecution('ERROR', 'PDF URL',url);	
		var filefound = nlapiLoadFile('Images/LOGOCOMP.jpg'); 
		if (filefound) 
		{ 
			nlapiLogExecution('ERROR', 'Event', 'file;'+filefound.getId()); 
			var imageurl = filefound.getURL();
			nlapiLogExecution('ERROR','imageurl',imageurl);
			//var finalimageurl = url + imageurl;//+';';
			var finalimageurl = imageurl;//+';';
			//finalimageurl=finalimageurl+ '&expurl=T;';
			nlapiLogExecution('ERROR','imageurl',finalimageurl);
			finalimageurl=finalimageurl.replace(/&/g,"&amp;");

		} 
		else 
		{
			nlapiLogExecution('ERROR', 'Event', 'No file;');
		}

		//date
		var sysdate=DateStamp();
		var systime=TimeStamp();
		var Timez=calcTime('-5.00');
		nlapiLogExecution('ERROR','TimeZone',Timez);
		var datetime= new Date();
		datetime=datetime.toLocaleTimeString() ;

		var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<head><macrolist><macro id='myfooter'><p align='center'>Page <pagenumber/> of <totalpages/></p></macro></macrolist></head><body  font-size=\"7\"  size=\"A4-landscape\"    padding-top=\" 0mm\"   footer='myfooter' footer-height='20mm'>\n";
		//var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<head></head><body  font-size=\"7\"  size=\"A4-landscape\"    padding-top=\" 0mm\"   footer='myfooter' footer-height='20mm'>\n";
//		nlapiLogExecution('ERROR','xml0 ',"<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body font-size=\"7\">\n");

		var replaceChar =/\$|,|@|#|~|`|\%|\*|\^|\&|\+|\=|\-|\_|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|\./g;
		var strxml= "";


		if(searchresults!=null && searchresults!="")
		{
			var rowcount=0;//added by santosh
			var totalcount=searchresults.length;
			while(0<totalcount)//added by santosh
			{ 
				var count=0;
				nlapiLogExecution('ERROR','totalcount',totalcount);
				nlapiLogExecution('ERROR','count',count);

			strxml += "<table width='100%'>";
			strxml += "<tr><td><table width='100%' >";
			strxml += "<tr ><td valign='middle' align='left'><img src='" + finalimageurl + "'></img></td><td valign='middle' align='left'  style='font-size:xx-large;'>";
			strxml += "Replenishment Report ";
			strxml += "</td><td align='right'>&nbsp;</td></tr></table></td></tr>";
			strxml += "<tr><td><table width='100%' >";
			strxml += "<tr><td style='width:41px;font-size: 12px;'>";
			strxml += "<p align='right'>Date/Time:"+Timez+"</p>"; 
			strxml += "</td></tr>";
			strxml += "</table>";

			strxml += "</td></tr>";
			strxml += "<tr><td><table width='100%' >";
			strxml += "<tr><td>";
			strxml +="<table align='left'>";

			strxml +="<tr><td align='left' style='width:20px; font-size: 16px;'>Replenishment# :</td>"; 
			strxml +="<td align='right' style='width:41px; font-size: 14px;'>";
			strxml += repid;
			strxml +="</td>";
			strxml +="</tr>";

			strxml +="</table>";
			strxml += "</td></tr>";
			strxml += "</table>";

			strxml += "</td></tr>";

			strxml += "<tr><td><table width='1000px' >";
			strxml += "<tr><td>";

			strxml +="<table  width='980px'>";
			strxml +="<tr style=\"font-weight:bold;background-color:gray;color:white;\">";

			strxml += "<td width='7%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += "Report#";
			strxml += "</td>";

			strxml += "<td width='7%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += "From LP#";
			strxml += "</td>";

			strxml += "<td width='7%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += "TO LP#";
			strxml += "</td>";

			strxml += "<td width='7%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += "Item";
			strxml += "</td>";

			strxml += "<td width='7%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += "LOT#";
			strxml += "</td>";

			strxml += "<td width='7%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += "Quantity";
			strxml += "</td>";

			strxml += "<td width='7%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += "Act Begin Location";
			strxml += "</td>";

			strxml += "<td width='7%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += "Act End Location";
			strxml += "</td>";

			strxml += "<td width='16%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px; '>";
			strxml += "Task Type";
			strxml += "</td>";

			strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += "Serial #";
			strxml += "</td>";

			strxml =strxml+  "</tr>";

			for(var m=rowcount; m< searchresults.length; m++)
			{	
				count++;
				rowcount++;
				searchresult=searchresults[m];
				var line = repid;
				if(line== null || line =="")
					line="";

				var  lp = searchresult.getValue('custrecord_lpno');
				if(lp== null || lp =="")	
					lp="&nbsp;";

				var  itemname = searchresult.getText('custrecord_sku');
				if(itemname== null || itemname =="")
					itemname="&nbsp;";

				var lotbatch = searchresult.getValue('custrecord_batch_no');
				if(lotbatch== null || lotbatch =="")
					lotbatch="&nbsp;";

				var qty = searchresult.getValue('custrecord_expe_qty');
				if(qty== null || qty =="")
					qty="";

				var  loc = searchresult.getText('custrecord_actbeginloc');
				if(loc== null || loc =="")	
					loc="&nbsp;";

				var endloc =  searchresult.getText('custrecord_actendloc');
				if(endloc== null || endloc =="")
					endloc="&nbsp;";

				var tasktype =  searchresult.getText('custrecord_tasktype');
				if(tasktype== null || tasktype =="")
					tasktype="&nbsp;";

				var FromLp =  searchresult.getValue('custrecord_from_lp_no');
				if(FromLp== null || FromLp =="")
					FromLp="&nbsp;";


				strxml =strxml+  "<tr>";
				strxml += "<td width='7%' align='center' style='border-width: 1px; border-color: #000000;font-size: 10px;'>";
				strxml += line;
				strxml += "</td>";

				strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 10px;'>";
				strxml += FromLp;
				strxml += "</td>";

				strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 10px;'>";
				strxml += lp;
				strxml += "</td>";

				strxml += "<td width='6%' align='center' style='border-width: 1px; border-color: #000000;font-size: 10px;'>";
				//strxml += itemname;
				if(itemname != null && itemname!= "")
				{
					strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
					strxml += itemname;
					strxml += "\"/>";
				}

				strxml += "</td>";


				strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 10px;'>";
				strxml += lotbatch;
				strxml += "</td>";

				strxml += "<td width='7%' align='center' style='border-width: 1px; border-color: #000000;font-size: 10px;'>";
				strxml += qty;
				strxml += "</td>";

				strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 10px;'>";
				strxml += loc;
				strxml += "</td>";

				strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 10px;'>";
				strxml += endloc;
				strxml += "</td>";

				strxml += "<td width='7%' align='center' style='border-width: 1px; border-color: #000000;font-size: 10px;'>";
				strxml += tasktype;
				strxml += "</td>";

				strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 10px;'>";
				strxml += '';
				strxml += "</td></tr>";
                //case 201411009
				if(count==8)//added by santosh
				{
					break;
				}
			}
			strxml =strxml+"</table>";

			strxml += "</td></tr>";
			strxml += "</table>";

			strxml += "</td></tr>";

			strxml += "</table>"; 

			totalcount=parseFloat(totalcount)-parseFloat(count);
			if(rowcount==searchresult.length)
			{
				strxml +="<p style=\" page-break-after:avoid\"></p>";
			}
			else
			{
				strxml +="<p style=\" page-break-after:always\"></p>";
			}
			}
			strxml =strxml+ "\n</body>\n</pdf>";		
			xml=xml +strxml;
			//nlapiLogExecution('ERROR','xml',xml);
			var file = nlapiXMLToPDF(xml);	
			
			nlapiLogExecution('ERROR','file',file);

			var pdffilename = repid+'_ReplenReport.pdf';

			nlapiLogExecution('ERROR','pdffilename',pdffilename);

			var filevalue=file.getValue();
			var newAttachment = nlapiCreateFile(pdffilename,file.type,filevalue);

			var context = nlapiGetContext();
			
			var userId = context.getUser();
			nlapiLogExecution('ERROR','userId',userId);
			var userAccountId = context.getCompany();
			var username='';
			try
			{
				var transaction = nlapiLoadRecord('Employee', userId);
				var variable=transaction.getFieldValue('email');
				username=variable.split('@')[0];
			}
			catch(exp)
			{
				nlapiLogExecution('ERROR','exp',exp);
			}

			var strContent="";

			var strSubject="Replen Report printed for Report# "+repid;//+" and Fulfillment Order# "+vQbfullfillmentNo;// +"  by user "+username+" at  "+Timez; 
			strContent +="Report#:"+repid;
			strContent +="<br/>";
			strContent +="AccountId#:"+userAccountId;
			strContent +="<br/>";
			strContent +=strSubject;

			var filters = new Array();
			filters.push(new nlobjSearchFilter('custrecord_email_alert_type', null, 'anyof', ['2']));
			filters.push(new nlobjSearchFilter('custrecord_email_trantype', null, 'anyof', ['2']));

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_user_email');
			columns[1] = new nlobjSearchColumn('custrecord_email_option');
			var searchresults = nlapiSearchRecord('customrecord_email_config', null, filters, columns);		 
			var email= "";
			var emailbcc="";
			var emailcc="";
			var emailappend="";
			var emailbccappend= new Array();
			var emailccappend=new Array();
			var count=0;
			var bcccount=0;
			for(var g=0;searchresults!=null && g<searchresults.length;g++)
			{
				var emailtext=searchresults[g].getText('custrecord_email_option');
				nlapiLogExecution('ERROR','emailtext',emailtext);
				if(emailtext=="BCC")
				{
					emailbccappend[bcccount]=searchresults[g].getValue('custrecord_user_email');
					bcccount++;		 
				}
				else if(emailtext=="CC")
				{
					emailccappend[count]=searchresults[g].getValue('custrecord_user_email');
					count++;		 
				}
				else
				{
					email =searchresults[g].getValue('custrecord_user_email');
					emailappend +=email+";";
				}
			} 
			var substirngemail= emailappend .substring(0, emailappend .length-1);
			nlapiLogExecution('ERROR','variable ',variable );
			if(variable != null && variable != '')
			{
				substirngemail = substirngemail + ';' + variable;
			}
			nlapiLogExecution('ERROR','substirngemail',substirngemail);

			nlapiSendEmail(userId,substirngemail,strSubject,strContent,emailccappend,emailbccappend,null,newAttachment);
			


		}
	}
	
}
function calcTime(offset) {

	//var vtime=new Date();

	//nlapiLogExecution('ERROR', 'TimeOffset', vtime.getTimezoneOffset());

	// create Date object for current location
	d = new Date();

	// convert to msec
	// add local time zone offset 
	// get UTC time in msec
	utc = d.getTime() + (d.getTimezoneOffset() * 60000);

	// create new Date object for different city
	// using supplied offset
	nd = new Date(utc + (3600000*offset));

	// return time as a string
	//return "The local time in " + city + " is " + nd.toLocaleString();

	var stringDt=((parseFloat(nd.getMonth()) + 1) + '/' + (parseFloat(nd.getDate())) + '/' + nd.getFullYear());

	var timestamp;
	var a_p = "";

	//Getting time in hh:mm tt format.
	var curr_hour = nd.getHours();
	var curr_min = nd.getMinutes();

	// determining the am/pm indicator
	if (curr_hour < 12)
		a_p = "am";
	else
		a_p = "pm";

	// finalizing hours depending on 24hr clock
	if (curr_hour == 0)
		curr_hour = 12;
	else if(curr_hour > 12)
		curr_hour -= 12;

	if (curr_min.length == 1)
		curr_min = "0" + curr_min;

	//Adding fields to update time zones.
	timestamp = curr_hour + ":" + curr_min + " " + a_p;

	return stringDt + " " + timestamp;


}