/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************
 *
 * $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_PrintAutoPacking.js,v $
 * $Revision: 1.1.2.6.2.1 $
 * $Date: 2015/11/05 17:42:22 $
 * $Author: grao $
 * $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 * DESCRIPTION
 * Functionality
 *
 * REVISION HISTORY
 * $Log: ebiz_PrintAutoPacking.js,v $
 * Revision 1.1.2.6.2.1  2015/11/05 17:42:22  grao
 * 2015.2 Issue Fixes 201413012
 *
 * Revision 1.1.2.6  2015/07/27 11:31:19  skreddy
 * Case# 201413012
 *
 * Create the Standard packlist
 *
 * Revision 1.1.2.5  2015/05/13 13:20:43  schepuri
 * case# 201412741
 * For Dynamic Packslip printing
 *
 * Revision 1.1.2.4  2015/05/07 14:38:02  skreddy
 * Case# 201412597
 * Packlist Printing after Packing completed
 *
 * Revision 1.1.2.3  2013/10/31 14:33:41  svanama
 * Case# 201217331
 * Auto packing name changed in packlist flie
 *
 * Revision 1.1.2.2  2013/03/19 11:45:09  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.1.2.1  2013/03/06 09:54:14  gkalla
 * CASE201112/CR201113/LOG201121
 * Lexjet Files merging for Standard bundle
 *
 * Revision 1.1.2.5  2012/07/30 13:43:00  svanama
 * CASE201112/CR201113/LOG201
 * CodeChanges in printPackList
 *
 * Revision 1.1.2.4  2012/07/19 14:16:29  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to PackageCount was resolved.
 *
 * Revision 1.1.2.3  2012/07/17 15:34:12  spendyala
 * CASE201112/CR201113/LOG201121
 * While creating shipmanifest record updating Packageno and totalpackage count.
 *
 * Revision 1.1.2.2  2012/07/11 08:26:41  spendyala
 * CASE201112/CR201113/LOG201121
 * While creating shipmanifest record updating Packageno and totalpackage count.
 *
 * Revision 1.1.2.1  2012/05/21 13:05:48  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Auto Packing
 *
 * Revision 1.2  2012/05/08 16:18:42  svanama
 * CASE201112/CR201113/LOG201121
 * packcode changes or added
 *
 * Revision 1.1  2012/04/24 13:45:19  svanama
 * CASE201112/CR201113/LOG201121
 * new pack code file added
 *
 *
 ****************************************************************************/

function PrintAutoPacking(request, response)
{
	if (request.getMethod() == 'GET')
	{

		var html = "<html><head><title></title>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		html = html + " document.getElementById('cmdYes').focus();";        
		html = html + "</script>";		
		html = html + "</head><body>";
		html = html + "	<form name='_rfprintpacklist' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>DO YOU WANT TO COMPLETE THE PACKING?";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> <input name='cmdYes' type='submit' value='YES'/>";
		html = html + "					 <input name='cmdNo' type='submit' value='NO'/>";
		html = html + "				</td>";
		html = html + "			</tr>";

		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";
		response.write(html);
	}
	else
	{
		var optedEvent = request.getParameter('cmdYes');
		nlapiLogExecution('ERROR', 'optedEvent', optedEvent);
		if(optedEvent=='YES')
		{
			var POarray=new Array();

			var getCartonLPNo = request.getParameter('custparam_cartonno');
			nlapiLogExecution('ERROR', 'getCartLPNo', getCartonLPNo);
			var getFONO= request.getParameter('custparam_fono');
			nlapiLogExecution('ERROR', 'getFONO', getFONO);
			var printername=request.getParameter('custparam_printername');
			var openTaskFilters1 = new Array();
			var openTaskColumns = new Array();
			openTaskColumns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');
			openTaskColumns[1] = new nlobjSearchColumn('name');//
			openTaskColumns[2] = new nlobjSearchColumn('custrecord_wms_location');
			openTaskColumns[3] = new nlobjSearchColumn('custrecord_wms_status_flag');

			POarray["custparam_cartonno"] = getCartonLPNo;
			POarray["custparam_fono"] = getFONO;
			POarray["custparam_printername"] = printername;
			POarray["custparam_screenno"] = 'PL1';

			if(printername == null || printername =='')
				POarray["custparam_printername"] = '';

			if(getCartonLPNo!=null && getCartonLPNo!='')
			{
				nlapiLogExecution('ERROR', 'getCartLPNo', getCartonLPNo);
				openTaskFilters1.push( new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getCartonLPNo));
			}
			if(getFONO!=null && getFONO!='')
			{
				openTaskFilters1.push( new nlobjSearchFilter('name', null, 'is', getFONO));
			}
			openTaskFilters1.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); // PICK and PACK
			//openTaskFilters1.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['8']));
			var openTaskSearchResults1 = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, openTaskFilters1, openTaskColumns);
			nlapiLogExecution('ERROR', 'openSearchResults1', openTaskSearchResults1);
			if(openTaskSearchResults1!=null && openTaskSearchResults1!='' && openTaskSearchResults1.length>0)   
			{
				nlapiLogExecution('ERROR', 'openSearchResults1', openTaskSearchResults1.length);

				var vPickConfCount=0;
				var vPackCompCount=0;
				var vebizOrdNo=openTaskSearchResults1[0].getValue('custrecord_ebiz_order_no');
				var fulfillmentnumber=openTaskSearchResults1[0].getValue('name');
				var whlocation=openTaskSearchResults1[0].getValue('custrecord_wms_location');

				for(var s=0;s<openTaskSearchResults1.length;s++)
				{
					var vebizOrdStatus=openTaskSearchResults1[s].getValue('custrecord_wms_status_flag');
					if(vebizOrdStatus=='8')
						vPickConfCount=parseInt(vPickConfCount)+1;
					else if(vebizOrdStatus=='28')
						vPackCompCount=parseInt(vPackCompCount)+1;

				}


				nlapiLogExecution('ERROR', 'vebizOrdNo', vebizOrdNo);
				nlapiLogExecution('ERROR', 'fulfillmentnumber', fulfillmentnumber);
				nlapiLogExecution('ERROR', 'whlocation', whlocation);
				if(parseInt(vPackCompCount)==0 && parseInt(vPickConfCount)==0 )
				{
					POarray["custparam_error"] =  "No pending picks with pick confirmed status.";
					nlapiLogExecution('ERROR', 'There is no Pick confirm and pack complete status tasks');
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				}
				else if(parseInt(vPackCompCount)>0 && parseInt(vPickConfCount)<=0 )
				{

					nlapiLogExecution('ERROR', 'All are Packcomplete');

					createpacklisthtml(vebizOrdNo,fulfillmentnumber,printername);
				}
				else
				{
					nlapiLogExecution('ERROR', 'Have Pick Confirmed tasks');					 
					if(vebizOrdNo!=null && vebizOrdNo!='' && fulfillmentnumber!=null && fulfillmentnumber !='')
					{
						//Case Start # 201217331 
						//Name changed to PacklistAutoPacking 
						
						PacklistAutoPacking(vebizOrdNo,whlocation); 
						//Case End # 201217331
						createpacklisthtml(vebizOrdNo,fulfillmentnumber,printername);

					}

				}

			}
			else
			{
				POarray["custparam_error"] =  "NO PENDING PICKS WITH PICK CONFIRMED STATUS.";
				nlapiLogExecution('ERROR', 'No records to print in open task');
				nlapiLogExecution('ERROR', 'There is no Pick confirm and pack complete status tasks');
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			}	
		}
		else
		{
			POarray["custparam_error"] =  "NO PENDING PICKS";
			response.sendRedirect('SUITELET', 'customscript_rf_main_menu', 'customdeploy_rf_main_menu_di');
		}

	}

}
function DataRec(vCompId, vSiteId, vEbizOrdNo,vOrdNo,vContainer,vPackStation,vEbizControlNo,vItem) 
{
	if(vCompId != null && vCompId != '')
		this.DataRecCompId = vCompId;
	else
		this.DataRecCompId=null;

	if(vSiteId != null && vSiteId != '')
		this.DataRecSiteId = vSiteId;
	else
		this.DataRecSiteId=null;
	this.DataRecEbizOrdNo = vEbizOrdNo;
	this.DataRecOrdNo = vOrdNo;
	this.DataRecContainer = vContainer;
	this.DataRecPackStation = vPackStation;
	this.DataRecEbizControlNo = vEbizControlNo;
	this.DataRecItem = vItem;	
}	

function createpacklist(vebizOrdNo,fulfillmentnumber,printername,trantype,salesorderdetails,locationRecord)
{
	var filters=new Array();
	var columns=new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebizruletype', null, 'is',"Dynamic Packlist"));
	filters.push(new nlobjSearchFilter('custrecord_ebizrulevalue', null, 'is',"Y"));

	var searchresultssystemrules = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters,columns);

	nlapiLogExecution('Debug', 'searchresultssystemrules', searchresultssystemrules);

	if(searchresultssystemrules!=null && searchresultssystemrules!='')
	{
		createDynamicPacklistHtml(vebizOrdNo,fulfillmentnumber,printername,trantype,salesorderdetails,locationRecord)
	}
	else
	{
		createpacklisthtml(vebizOrdNo,fulfillmentnumber,printername,trantype,salesorderdetails,locationRecord);
	}

}

function createpacklisthtml(openarray,vebizOrdNo,fulfillmentnumber,printername,trantype,salesorderdetails,locationRecord,wavenumber)
{

	try
	{	
		
	nlapiLogExecution('ERROR', 'into CreatePackApacklisthtml ', 'into CreatePackApacklisthtml');
	nlapiLogExecution('ERROR', 'createpackcodehtmlvebizOrdNo ', vebizOrdNo);
	nlapiLogExecution('ERROR', 'fulfillmentnumber ', fulfillmentnumber);

	var billtoaddress=salesorderdetails.getFieldValue('billaddress'); 
	var shipaddress=salesorderdetails.getFieldValue('shipaddress');	
	var orderdate=salesorderdetails.getFieldValue('trandate');
	var ordernumber=salesorderdetails.getFieldValue('tranid');	
	var customerpo=salesorderdetails.getFieldValue('otherrefnum');
	var entity=salesorderdetails.getFieldText('entity');
	var locationId =salesorderdetails.getFieldValue('location');
	var shipmethod=salesorderdetails.getFieldText('shipmethod');
	var shipDate=salesorderdetails.getFieldValue('shipdate');
	if((customerpo==null)||(customerpo==''))
	{
		customerpo="";
	}
	if((shipDate==null)||(shipDate==''))
	{
		shipDate="";
	}
	if((orderdate==null)||(orderdate==''))
	{
		orderdate="";
	}

	shipmethod=shipmethod.replace(/\s/g, "");
	if((shipmethod==null)||(shipmethod==''))
	{
		shipmethod="";
	}
	var FOB='';

		nlapiLogExecution('ERROR', 'location ', locationId);
		var shipaddressee="";var shipaddr1="";var shipaddr2="";var shipcity="";var shipcountry="";var shipstate="";var shipzip="";var shipstateandcountry="";
		shipaddressee=salesorderdetails.getFieldValue('shipaddressee'); 
		shipaddr1=salesorderdetails.getFieldValue('shipaddr1'); 
		shipaddr2=salesorderdetails.getFieldValue('shipaddr2');
		shipcity=salesorderdetails.getFieldValue('shipcity'); 
		shipcountry=salesorderdetails.getFieldValue('shipcountry'); 
		shipstate=salesorderdetails.getFieldValue('shipstate'); 
		shipzip=salesorderdetails.getFieldValue('shipzip'); 
		if((shipaddressee==null)||(shipaddressee==''))
		{
			shipaddressee="";
		}
		if((shipaddr1==null)||(shipaddr1==''))
		{
			shipaddr1="";
		}
		if((shipaddr2==null)||(shipaddr2==''))
		{
			shipaddr2="";
		}
		if((shipcity==null)||(shipcity==''))
		{
			shipcity="";
		}
		if((shipcountry==null)||(shipcountry==''))
		{
			shipcountry="";
		}
		if((shipstate==null)||(shipstate==''))
		{
			shipstate="";
		}
		if((shipzip==null)||(shipzip==''))
		{
			shipzip="";
		}
		shipaddr1=shipaddr1+", "+shipaddr2;
		shipstateandcountry=shipcity+" "+shipstate+" "+shipzip;
		var locationadress =nlapiLoadRecord('Location',locationId);

		var addr1="";var city="";var state="";var zip=""; var stateandzip=""; var returnadresse="";
		addr1=locationadress.getFieldValue('addr1');
		city=locationadress.getFieldValue('city');
		state=locationadress.getFieldValue('state');
		zip=locationadress.getFieldValue('zip');
		returnadresse=locationadress.getFieldValue('addressee');
		if((addr1==null)||(addr1==''))
		{
			addr1="";
		}
		if((city==null)||(city==''))
		{
			city="";
		}
		if((state==null)||(state==''))
		{
			state="";
		}
		if((zip==null)||(zip==''))
		{
			zip="";
		}
		if((returnadresse==null)||(returnadresse==''))
		{
			returnadresse="";
		}
		stateandzip=city+" "+state+" "+zip;



		var groupopentaskfilterarray = new Array();
		//Please uncheck  the fillementnumber code 
		groupopentaskfilterarray.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof',vebizOrdNo));
		groupopentaskfilterarray.push(new nlobjSearchFilter('name', null, 'is',fulfillmentnumber.toString()));
		groupopentaskfilterarray.push( new nlobjSearchFilter('custrecord_tasktype', null, 'anyof',[3])); 
		groupopentaskfilterarray.push( new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof',[28]));
		if((wavenumber!=null)&&(wavenumber!=''))
		{
			nlapiLogExecution('ERROR', 'openwavenumber ', wavenumber);
			groupopentaskfilterarray.push( new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is',wavenumber)); 
		}
		var groupopentaskcolumnarray = new Array();
		
		groupopentaskcolumnarray[0] = new nlobjSearchColumn('custrecord_sku', null, 'group');
		groupopentaskcolumnarray[1] = new nlobjSearchColumn('custrecord_line_no', null, 'group');
		groupopentaskcolumnarray[2] = new nlobjSearchColumn('custrecord_act_end_date', null, 'group');
		


		groupopentaskcolumnarray[3] = new nlobjSearchColumn('formulanumeric', null, 'sum');	
		groupopentaskcolumnarray[3].setFormula("TO_NUMBER({custrecord_act_qty})");
		groupopentaskcolumnarray[4] = new nlobjSearchColumn('custrecord_parent_sku_no', null, 'group').setSort(true);
		groupopentaskcolumnarray[5] = new nlobjSearchColumn('custrecord_container_lp_no', null, 'group');
		var groupopentasksearchresults  = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, groupopentaskfilterarray, groupopentaskcolumnarray);
		var appendcontlp="";
		var actualenddate="";
		var strVar="";
		var noofCartons=0;

		if(groupopentasksearchresults!=null)
		{
			nlapiLogExecution('ERROR', 'groupopentasksearchresults.length ', groupopentasksearchresults.length);
			var totalamount='';
			var groupcount=groupopentasksearchresults.length;
			var grouplength=0;
			var invoicetasklength=groupopentasksearchresults.length;
			var linenumber=1;
			var pagecount=1;

			var totalinvoice=0;

			var totalcount=groupopentasksearchresults.length;
			var totalshipqty=0;
			var totalcube=0;
			var totalweight=0;
			var strVar="";
			while(0<totalcount)
			{


				//for(var h=0; h<totalcount;h++)
				//{
				var count=0;
				var kititemcount=0;
				strVar +="<html>";

				strVar += " <body>";
				strVar += "    <table style=\"width: 100%;\">";
				strVar += "    <tr>";
				strVar += "    <td >";
				strVar += "    <table>";
				strVar += " <td align=\"left\" style=\"width: 65%;\">";
				strVar += "        <table style=\"width: 25%;\" align=\"left\">";
				strVar += "            <tr>";
				strVar += "                <td>";
				strVar += "                    <img src=\"headerimage\" width=\"320\" height=\"65\" />";
				strVar += "                </td>";
				strVar += "            </tr>";
				strVar += "            <tr>";
				strVar += "                <td style=\"font-size: 12px; font-family:Arial;\">";
				strVar += "<br \/>" +returnadresse+" <br \/>" +addr1+" <br \/>" +stateandzip+"" ;			
				strVar += "                </td>";
				strVar += "            </tr>";			
				strVar += "        </table>";
				strVar += "        </td>";
				strVar += " <td></td>";
				strVar += "    <td></td>";
				strVar += "<td style=\"width: 35%; font-family:Arial;\" valign=\"top\">";
				strVar += "        <b>";
				strVar += "            <h2 align=\"right\">";
				strVar += "                Packing Slip</h2>";
				strVar += "        </b>";
				strVar += "        <table style=\"width: 150px;\" frame=\"box\" rules=\"all\" align=\"right\" border=\"0\" cellpadding=\"0.5\"";
				strVar += "            cellspacing=\"0\">";
				strVar += "            <tr>";
				strVar += "                <td style=\"font-size: 14px; text-align: center; border-top: 1px solid black; border-right: 1px solid black;";
				strVar += "                    border-left: 1px solid black; border-bottom: 1px solid black;\">";
				strVar += "                    Order Date";			
				strVar += "                </td>";
				strVar += "            </tr>";
				strVar += "            <tr>";
				strVar += "                <td style=\"font-size: 12px; text-align: center; border-right: 1px solid black; border-left: 1px solid black;";
				strVar += "                    border-bottom: 1px solid black; height: 18px\">";
				strVar += "					"+orderdate+"";
				strVar += "                </td>";
				strVar += "            </tr>";
				strVar += "        </table>";
				strVar += "        </td>";


				strVar += "</table>";
				strVar += "    </td>";
				strVar += "   </tr>";
				strVar += "   <tr>";
				strVar += "<td align=\"left\" style=\"width: 100%;\">";			
				strVar += "        <table style=\"width: 100%\">";
				strVar += "            <tr>";
				strVar += "                <td>";
				strVar += "                    <table style=\"width: 55%;\" rules=\"all\" align=\"left\" border=\"0\" frame=\"box\">";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 15px; text-align: left; border-top: 1px solid black; border-right: 1px solid black;";
				strVar += "                                border-left: 1px solid black; border-bottom: 1px solid black; height:24px; font-family:Arial;\">";
				strVar += "                               &nbsp Ship To";
				strVar += "                            </td>";
				strVar += "                        </tr>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 14px; text-align: left; border-right: 1px solid black; border-left: 1px solid black;";
				strVar += "                                border-bottom: 1px solid black; height: 80px;\" valign=\"top\">";
				strVar += "                                <table>";
				strVar += "                                    <tr>";
				strVar += "                                        <td style=\"font-size: 12px;\">";
				strVar += "											"+shipaddressee+"";
				strVar += "                                        </td>";
				strVar += "                                    </tr>";
				strVar += "                                    <tr>";
				strVar += "                                        <td style=\"font-size: 12px;\">";
				strVar += "											"+shipaddr1+"";
				strVar += "                                        </td>";
				strVar += "                                    </tr>";
				strVar += "                                    <tr>";
				strVar += "                                        <td style=\"font-size: 12px;\">";
				strVar += "											"+shipstateandcountry+"";
				strVar += "                                        </td>";
				strVar += "                                    </tr>";
				strVar += "                                </table>";
				strVar += "                            </td>";
				strVar += "                        </tr>";
				strVar += "                    </table>";
				strVar += "                </td>";
				strVar += "            </tr>";
				strVar += "            <br />";
				strVar += "            <tr>";
				strVar += "                <td>";
				strVar += "                    <table style=\"width: 100%;\" rules=\"all\" border=\"0\" frame=\"box\" cellpadding=\"0.5\"";
				strVar += "                        cellspacing=\"0\">";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 15px; text-align: left; border-top: 1px solid black; border-right: 1px solid black;";
				strVar += "                                border-left: 1px solid black; border-bottom: 1px solid black; height:24px; font-family:Arial;\">";
				strVar += "                                &nbsp Ship Date";
				strVar += "                            </td>";
				strVar += "                            <td style=\"font-size: 15px; text-align: left; border-top: 1px solid black; border-right: 1px solid black;";
				strVar += "                                border-bottom: 1px solid black; height:24px; font-family:Arial;\">";
				strVar += "                                 &nbsp Ship Via";
				strVar += "                            </td>";
				strVar += "                            <td style=\"font-size: 15px; text-align: left; border-top: 1px solid black; border-right: 1px solid black;";
				strVar += "                                border-bottom: 1px solid black; height:24px; font-family:Arial;\">";
				strVar += "                               &nbsp PO #";
				strVar += "                            </td>";
				strVar += "                            <td style=\"font-size: 15px; text-align: left; border-top: 1px solid black; border-right: 1px solid black;";
				strVar += "                                border-bottom: 1px solid black; height:24px; font-family:Arial;\">";
				strVar += "                               &nbsp FOB";
				strVar += "                            </td>";
				strVar += "                        </tr>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 14px; text-align: left; border-right: 1px solid black; border-left: 1px solid black;";
				strVar += "                                border-bottom: 1px solid black; height: 22px;\">";
				strVar += "								 &nbsp"+shipDate+"";
				strVar += "                            </td>";
				strVar += "                            <td style=\"font-size: 14px; text-align: left; border-right: 1px solid black; border-bottom: 1px solid black;";
				strVar += "                                height: 22px;\">";
				strVar += "								&nbsp"+shipmethod+"";
				strVar += "                            </td>";
				strVar += "                            <td style=\"font-size: 14px; text-align: left; border-right: 1px solid black; border-bottom: 1px solid black;";
				strVar += "                                height: 22px;\">";
				strVar += "								&nbsp"+customerpo+"";
				strVar += "                            </td>";
				strVar += "                            <td style=\"font-size: 14px; text-align: left; border-right: 1px solid black; border-bottom: 1px solid black;";
				strVar += "                                height: 22px;\">";
				strVar += "								&nbsp"+FOB+"";
				strVar += "                            </td>";
				strVar += "                        </tr>";
				strVar += "                    </table>";


				strVar += "                    <table style=\"width: 100%;\" rules=\"all\" border=\"0\" frame=\"box\" cellpadding=\"0.5\"";
				strVar += "                        cellspacing=\"0\">";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 15px; text-align: left; border-right: 1px solid black; border-left: 1px solid black;";
				strVar += "                                border-bottom: 1px solid black; height:24px; font-family:Arial;\">";
				strVar += "                               &nbsp Shipping Notes";
				strVar += "                            </td>";
				strVar += "                        </tr>";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 14px; text-align: left; border-right: 1px solid black; border-left: 1px solid black;";
				strVar += "                                border-bottom: 1px solid black; height: 22px;\">";

				strVar += "                            </td>";
				strVar += "                        </tr>";
				strVar += "                    </table>";


				strVar += "                    <table style=\"width: 100%;\" rules=\"all\" border=\"0\" frame=\"box\" cellpadding=\"0.5\"";
				strVar += "                        cellspacing=\"0\">";
				strVar += "                        <tr>";
				strVar += "                            <td style=\"font-size: 15px; text-align: center; border-right: 1px solid black; border-left: 1px solid black;";
				strVar += "                                border-bottom: 1px solid black; height:24px; font-family:Arial;\">";
				strVar += "                                Item #";
				strVar += "                            </td>";
				strVar += "                            <td style=\"font-size: 15px; text-align: left; border-right: 1px solid black; border-bottom: 1px solid black; height:24px; font-family:Arial;\">";
				strVar += "                               &nbsp Description";
				strVar += "                            </td>";
				strVar += "                            <td style=\"font-size: 15px; text-align: center; border-right: 1px solid black; border-bottom: 1px solid black; height:24px; font-family:Arial;\">";
				strVar += "                                Ordered";
				strVar += "                            </td>";
				strVar += "                            <td style=\"font-size: 15px; text-align: center; border-right: 1px solid black; border-bottom: 1px solid black; height:24px; font-family:Arial;\">";
				strVar += "                                Units";
				strVar += "                            </td>";
				strVar += "                            <td style=\"font-size: 15px; text-align: center; border-right: 1px solid black; border-bottom: 1px solid black; height:24px; font-family:Arial;\">";
				strVar += "                                Back Order";
				strVar += "                            </td>";
				strVar += "                            <td style=\"font-size: 15px; text-align: center; border-right: 1px solid black; border-bottom: 1px solid black; height:24px; font-family:Arial;\">";
				strVar += "                                Shipped";
				strVar += "                            </td>";
				strVar += "                        </tr>";
				//loop starts
				var repeatpartentsku;
				for(var g=grouplength; g<groupopentasksearchresults.length;g++)
				{
				count++;
				grouplength++;
				var vContainerLpNo = groupopentasksearchresults [g].getValue('custrecord_container_lp_no', null, 'group');
				var itemText = groupopentasksearchresults [g].getText('custrecord_sku', null, 'group');
				var ItemId= groupopentasksearchresults [g].getValue('custrecord_sku', null, 'group');
				var lineno = groupopentasksearchresults [g].getValue('custrecord_line_no', null, 'group');
				var totalqty = groupopentasksearchresults [g].getValue('formulanumeric', null, 'sum');
				nlapiLogExecution('ERROR', 'totalqty ', totalqty);
				var unitvalue,decscription,suggestedprice;
				var backordervalue = '0';
				var parentskuitemid = groupopentasksearchresults [g].getValue('custrecord_parent_sku_no', null, 'group');
				var parentsku = groupopentasksearchresults [g].getText('custrecord_parent_sku_no', null, 'group');
				nlapiLogExecution('ERROR', 'groupopentasklineno ', lineno);
				nlapiLogExecution('ERROR', 'parentskuitemid ', parentskuitemid);
				var parentitemtype='';
				var parentitemSubtype='';
				if(parentskuitemid!=null && parentskuitemid!='')
				{
					parentitemSubtype = nlapiLookupField('item', parentskuitemid, ['recordType']);
					parentitemtype=parentitemSubtype.recordType;
					if(parentitemtype!="kititem")
					{
						if(trantype!='transferorder')
						{
							/* Code commented on OCT23 by Siva Krishna.Vanama
					//unitvalue=salesorderdetails.getLineItemValue('item','rate',lineno);
					//suggestedprice=salesorderdetails.getLineItemValue('item','price_display',lineno);
					backordervalue=salesorderdetails.getLineItemValue('item','quantitybackordered',lineno);
					decscription=salesorderdetails.getLineItemValue('item','description',lineno); 
							 */
							var lineitemcount=salesorderdetails.getLineItemCount('item');
							for(var p=1;p<=lineitemcount;p++)
							{
								var iteminternalid=salesorderdetails.getLineItemValue('item','item',p);
								if(iteminternalid==ItemId)
								{
									//unitvalue=salesorderdetails.getLineItemValue('item','rate',p);
									//suggestedprice=salesorderdetails.getLineItemValue('item','price_display',p);
									backordervalue=salesorderdetails.getLineItemValue('item','quantitybackordered',p);
									decscription=salesorderdetails.getLineItemValue('item','description',p);

									break;


								}


							}
						}
						else
						{
							var lineitemcount=salesorderdetails.getLineItemCount('item');
							for(var p=1;p<=lineitemcount;p++)
							{
								var iteminternalid=salesorderdetails.getLineItemValue('item','item',p);
								if(iteminternalid==ItemId)
								{
									//unitvalue=salesorderdetails.getLineItemValue('item','rate',p);
									//suggestedprice=salesorderdetails.getLineItemValue('item','price_display',p);
									backordervalue=salesorderdetails.getLineItemValue('item','quantitybackordered',p);
									decscription=salesorderdetails.getLineItemValue('item','description',p);

									break;


								}


							}
						}
					}
				}
				else
				{
					backordervalue="0";
					var itemdescription = nlapiLookupField('item', ItemId, ['displayname']);
					decscription=itemdescription.displayname;
				}
				
				if(decscription == null || decscription == '' || decscription == 'undefined')
				{
					decscription = '';
				}
				
				if(backordervalue == null || backordervalue == '' || backordervalue == 'undefined')
				{
					backordervalue = '0';
				}
				
				if(parentitemtype=="kititem")
				{
					if(parentsku!=repeatpartentsku)
					{
						var parentskudesc =nlapiLookupField('item', parentskuitemid, ['displayname']);
						var parentdescription=parentskudesc.displayname;
						kititemcount++;
						strVar += "<tr>";
						strVar += "<td style=\"font-size: 14px;font-family:Times New Roman; text-align: center; border-right: 1px solid black; border-left: 1px solid black;";
						strVar += " border-bottom: none; height:22px;\">";
						strVar += "								"+parentsku+"";
						strVar += "</td>";
						strVar += "                            <td style=\"font-size: 12px; text-align: left; border-right: 1px solid black; border-bottom: none; height:22px;\">";
						strVar +=""+parentdescription+"";
						strVar += "                            </td>";
						strVar += "                            <td style=\"font-size: 12px; text-align: center; border-right: 1px solid black; border-bottom: none; height:22px;\">";
						strVar += "                                           &nbsp";
						strVar += "                            </td>";
						strVar += "                            <td style=\"font-size: 12px; text-align: center; border-right: 1px solid black; border-bottom: none; height:22px;\">";
						strVar += "                                           &nbsp ";
						strVar += "                            </td>";
						strVar += "                            <td style=\"font-size: 12px; text-align: center; border-right: 1px solid black; border-bottom: none; height:22px;\">";
						strVar +=" &nbsp";
						strVar += "                            </td>";
						strVar += "                            <td style=\"font-size: 12px; text-align: center; border-right: 1px solid black; border-bottom: none; height:22px;\">";
						strVar += "   &nbsp";
						strVar += "                            </td>";
						strVar += "                        </tr>";
					}
				}
				strVar += "                        <tr>";
				if(parentskuitemid==ItemId)
				{
					strVar += "                            <td style=\"font-size: 14px; text-align: center; border-right: 1px solid black; border-left: 1px solid black;";
					strVar += "                                border-bottom: none; height:22px;\">";
					strVar += "								"+itemText+"";
					strVar += "                            </td>";
				}
				else
				{
					strVar += "                            <td style=\"font-size: 14px; text-align: center; border-right: 1px solid black; border-left: 1px solid black;";
					strVar += "                                border-bottom: none; height:22px;\">";
					strVar += "							&nbsp&nbsp&nbsp"+itemText+"";
					strVar += "                            </td>";
				}
				if(parentskuitemid==ItemId)
				{
					strVar += "                            <td style=\"font-size: 12px; text-align: left; border-right: 1px solid black; border-bottom: none; height:22px;\">";
					strVar +=""+decscription+"";
					strVar += "                            </td>";
				}
				else
				{
					strVar += "                            <td style=\"font-size: 12px; text-align: left; border-right: 1px solid black; border-bottom: none; height:22px;\">";
					strVar +="&nbsp&nbsp&nbsp"+decscription+"";
					strVar += "                            </td>";
				}
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-right: 1px solid black; border-bottom: none; height:22px;\">";
				strVar += "                                            "+totalqty+" &nbsp";
				strVar += "                            </td>";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-right: 1px solid black; border-bottom: none; height:22px;\">";
				strVar += "                                           &nbsp EA";
				strVar += "                            </td>";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-right: 1px solid black; border-bottom: none; height:22px;\">";
				strVar +=" &nbsp"+backordervalue+"";
				strVar += "                            </td>";
				strVar += "                            <td style=\"font-size: 12px; text-align: center; border-right: 1px solid black; border-bottom: none; height:22px;\">";
				strVar += "                                            "+totalqty+" &nbsp";
				strVar += "                            </td>";
				strVar += "                        </tr>";
				var pagebreakcount=parseInt(count)+parseInt(kititemcount);
				if(pagebreakcount==10)
				{
					break;
				}
				//Loop Ends
				repeatpartentsku=parentsku;
			}

			// start of for Not Having lines

			var Height='';
			if(pagecount==1)
			{
				Height='230px';
			}
			if(pagecount>1)
			{
				Height='420px';
			}
			var recordCount=pagebreakcount;
			Height=parseInt(Height)-parseInt((recordCount*20));
			nlapiLogExecution('ERROR', 'height ', Height);

			strVar += "                                    <tr>";
			strVar += "                                        <td style=\"font-size: 12px; text-align: left; border-right: 1px solid black; border-left: 1px solid black;";
			strVar += "                                            border-bottom: 1px solid black; height: "+Height+";\">";
			strVar += "                                        </td>";
			strVar += "                                        <td style=\"font-size: 12px; text-align: left; border-right: 1px solid black; border-bottom: 1px solid black;";
			strVar += "                                            height: 4px;\">";
			strVar += "                                        </td>";
			strVar += "                                        <td style=\"font-size: 12px; text-align: left; border-right: 1px solid black; border-bottom: 1px solid black;";
			strVar += "                                            height: 4px;\">";
			strVar += "                                        </td>";
			strVar += "                                        <td style=\"font-size: 12px; text-align: left; border-right: 1px solid black; border-bottom: 1px solid black;";
			strVar += "                                            height: 4px;\">";
			strVar += "                                        </td>";
			strVar += "                                        <td style=\"font-size: 12px; text-align: left; border-right: 1px solid black; border-bottom: 1px solid black;";
			strVar += "                                            height: 4px;\">";
			strVar += "                                        </td>";
			strVar += "                                        <td style=\"font-size: 12px; text-align: left; border-right: 1px solid black; border-bottom: 1px solid black;";
			strVar += "                                            height: 4px;\">";
			strVar += "                                        </td>";		
			strVar += "                                    </tr>";

			// End of for Not Having lines

			strVar += "  </table>";
			strVar += "                </td>";
			strVar += "            </tr>";
			strVar += "        </table>";
			strVar += " </td>";
			strVar += "    </tr>";			

			strVar += " <tr>";
			strVar += "    <td>";
			strVar += "    <br \/>";
			strVar += "    ----------------------------------------------------------------------------------------------------------------------------";
			strVar += "    <br \/>";
			strVar += "    <br \/>    ";
			strVar += "    </td>";
			strVar += "    </tr>";

//			strVar += "  <tr>";
//			strVar += "    <td  style=\"width: 55%;\"align=\"right\">";
//			strVar += "    <b>Customer Return From <\/b>";
//			strVar += "    </td>";
//			strVar += "    </tr>";


			strVar += "  <tr>";
			strVar += "   <td>";


			strVar += "        <table style=\"width: 45%;\" align=\"left\">";
			strVar += "<tr>";
			strVar += "            <td style=\"font-size: 15px;\">";
			strVar += "               <br \/>";
			strVar += "            </td>";
			strVar += "        </tr>";
			strVar += " <tr>";
			strVar += "            <td style=\"font-size: 13px; font-family:Arial;\">";
			strVar += "                <b>Ship Returns To<\/b>";
			strVar += "            </td>";
			strVar += "        </tr>";

			strVar += "            <tr>";
			strVar += "                <td style=\"font-size: 12px; \">";
			strVar +=""+returnadresse+" <br /> "+addr1+" <br /> "+stateandzip+" ";
			strVar += "                </td>";
			strVar += "            </tr>";

			strVar += "        </table>";


			strVar += "        <table style=\"width: 55%;\" align=\"right\">";			
			strVar += "            <tr>";
			strVar += "                <td style=\"font-size: 13px; font-family:Arial;\"><b>Customer Return From </b>";
			strVar += "                </td>";
			strVar += "                <td >";			
			strVar += "                </td>";
			strVar += "            </tr>";
			strVar += "            <tr>";
			strVar += "                <td style=\"font-size: 13px; font-family:Arial;\">";
			strVar += "                    <b>Customer </b>";
			strVar += "                </td>";
			strVar += "                <td style=\"font-size: 12px;\">";
			strVar += "											"+shipaddressee+"";
			strVar += "                </td>";
			strVar += "            </tr>";
			strVar += "            <tr>";
			strVar += "                <td style=\"font-size: 13px; font-family:Arial;\">";
			strVar += "                    <b>Order  </b>";
			strVar += "                </td>";
			strVar += "                <td style=\"font-size: 12px;\">";
			strVar +=""+ordernumber+"";
			strVar += "                </td>";
			strVar += "            </tr>";
			strVar += "            <tr>";
			strVar += "                <td style=\"font-size: 13px; font-family:Arial;\">";
			strVar += "                    <b>R.A. # </b>";
			strVar += "                </td>";
			strVar += "                <td>";
			strVar += "                </td>";
			strVar += "            </tr>";
			strVar += "        </table>";
			strVar += "        <br />";
			strVar += "        <br />";
			strVar += "        <br />";
			strVar += "        <br />";			
			strVar += " </td>";
			strVar += "     </tr>";

			strVar += " <tr>";
			strVar += "     <td>";
			strVar += "        <table style=\"width: 100%;\" frame=\"box\" rules=\"all\" align=\"right\" border=\"0\" cellpadding=\"0.5\"";
			strVar += "            cellspacing=\"0\">";
			strVar += "            <tr style=\"background-color: Gray;\">";
			strVar += "                <td style=\"font-size: 15px; text-align: left; color: white; border-top: 1px solid black;";
			strVar += "                    border-right: 1px solid black; border-left: 1px solid black; border-bottom: 1px solid black; font-family:Arial;\">";
			strVar += "                    &nbsp Item";
			strVar += "                </td>";
			strVar += "                <td style=\"font-size: 15px; text-align: left; color: white; border-top: 1px solid black;";
			strVar += "                    border-right: 1px solid black; border-bottom: 1px solid black; font-family:Arial;\">";
			strVar += "                    &nbsp Quantity";
			strVar += "                </td>";
			strVar += "                <td style=\"font-size: 15px; text-align: left; color: white; border-top: 1px solid black;";
			strVar += "                    border-right: 1px solid black; border-bottom: 1px solid black; font-family:Arial;\">";
			strVar += "                   &nbsp Reason For Returning";
			strVar += "                </td>";
			strVar += "            </tr>";
			strVar += "            <tr>";
			strVar += "                <td style=\"font-size: 16px; text-align: left; border-right: 1px solid black; border-left: 1px solid black;";
			strVar += "                    border-bottom: 1px solid black;\" height=\"55px\">";
			strVar += "                </td>";
			strVar += "                <td style=\"font-size: 16px; text-align: left; border-right: 1px solid black; border-bottom: 1px solid black;\"";
			strVar += "                    height=\"55px\">";
			strVar += "                </td>";
			strVar += "                <td style=\"font-size: 16px; text-align: left; border-right: 1px solid black; border-bottom: 1px solid black;\"";
			strVar += "                    valign=\"Top\">";
			strVar += "                </td>";
			strVar += "            </tr>";
			strVar += "        </table>";
			strVar += "</td>";
			strVar += "    </tr>";

			if(grouplength==groupopentasksearchresults.length)
			{
				strVar +="<p style=\" page-break-after:avoid\"></p>";
			}
			else
			{
				strVar +="<p style=\" page-break-after:always\"></p>";
			}
			strVar += "        </table>";
			strVar += " </body>";
			strVar += "        </html>";
			nlapiLogExecution('ERROR', 'totalcount', totalcount);
			totalcount=parseInt(totalcount)-parseInt(count);
			nlapiLogExecution('ERROR', 'totalcountafter', totalcount);

		}
		var tasktype='14';
		var labeltype='PackList';
		var print='F';
		var reprint='F';
		var company='';
		var location='';
		var formattype='html';
		var labelrecord = nlapiCreateRecord('customrecord_labelprinting'); 
		labelrecord.setFieldValue('name', fulfillmentnumber); 
		labelrecord.setFieldValue('custrecord_labeldata',strVar);  
		labelrecord.setFieldValue('custrecord_label_refno',fulfillmentnumber);     
		labelrecord.setFieldValue('custrecord_label_tasktype',tasktype);//chkn task   
		labelrecord.setFieldValue('custrecord_labeltype',labeltype);    
		labelrecord.setFieldValue('custrecord_label_lp',vContainerLpNo);

		labelrecord.setFieldValue('custrecord_label_print', print);
		labelrecord.setFieldValue('custrecord_label_reprint', reprint);
		labelrecord.setFieldValue('custrecord_label_company', company);
		labelrecord.setFieldValue('custrecord_label_location', location);
		labelrecord.setFieldValue('custrecord_label_printername',printername);

		//nlapiLogExecution('ERROR', 'htmlstring ', strVar);

		var tranid = nlapiSubmitRecord(labelrecord);
		nlapiLogExecution('ERROR', 'tranid ', tranid);
		//PackListMessage(tranid,printername,request);
		}
	else
	{
		var getCartonLPNo = request.getParameter('custparam_cartonno');
		nlapiLogExecution('ERROR', 'getCartLPNo', getCartonLPNo);
		var getFONO= request.getParameter('custparam_fono');
		nlapiLogExecution('ERROR', 'getFONO', getFONO);

		var msgvar='';
		if(getCartonLPNo!=null && getCartonLPNo!='')
		{
			msgvar="CartonLP# "+getCartonLPNo;				 

		}
		if(getFONO!=null && getFONO!='')
		{
			msgvar="FO# "+getFONO;
		}
		if(getFONO!=null && getFONO!='' && getCartonLPNo!=null && getCartonLPNo!='')
		{
			msgvar="FO# "+getFONO+ "and  CartonLP# "+getCartonLPNo;
		}
		var POarray=new Array();
		POarray["custparam_screenno"] = 'PL2';
		POarray["custparam_printername"] = printername;
		POarray["custparam_error"] =  "There is no records to create PackList for this  "+msgvar+"";
		response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);

		nlapiLogExecution('ERROR', 'Recordsnotfound', 'Recordsnotfound');
	}
		var Endtime=PacklistTimeStamp();
		nlapiLogExecution('ERROR', 'EndPacklistTime ', Endtime);
}
	catch(exp)
	{
		nlapiLogExecution('ERROR', 'exceptionhtmlpackcode ', exp);

	}
}

function PacklistTimeStamp()
{
	var timestamp;
	var now = new Date();
	var a_p = "";

	//Getting time in hh:mm tt format.
	var curr_hour = now.getHours();
	var curr_min = now.getMinutes();

	// determining the am/pm indicator
	if (curr_hour < 12)
		a_p = "am";
	else
		a_p = "pm";

	// finalizing hours depending on 24hr clock
	if (curr_hour == 0)
		curr_hour = 12;
	else if(curr_hour > 12)
		curr_hour -= 12;

	if (curr_min.length == 1)
		curr_min = "0" + curr_min;

	//Adding fields to update time zones.
	timestamp = curr_hour + ":" + curr_min + " " + a_p;

	return timestamp;
}
//Case Start # 201217331
//Function Name changed 
function PacklistAutoPacking(vebizOrdNo,whlocation){	
	nlapiLogExecution('ERROR', 'into AutoPacking (SO Internal Id)', vebizOrdNo);
	nlapiLogExecution('ERROR', 'into AutoPacking (WH Location)', whlocation);
	if(vebizOrdNo!=null && vebizOrdNo!='')
	{
		nlapiLogExecution('ERROR', 'into AutoPacking vebizOrdNo is not null', vebizOrdNo);
		var vOrdAutoPackFlag="";
		var vStgAutoPackFlag="";
		var vStgCarrierType="";
		var vCarrierType="";
		var vStgCarrierName="";
		var vorderType='';
		/*var vOrderAutoPackFlag = getAutoPackFlagforOrdType(vebizOrdNo);
		if(vOrderAutoPackFlag!=null&& vOrderAutoPackFlag.length>0){
			vOrdAutoPackFlag = vOrderAutoPackFlag[0].getValue('custrecord_autopackflag','custbody_nswmssoordertype');
			vCarrierType = vOrderAutoPackFlag[0].getText('custrecord_carrier_type','custbody_salesorder_carrier');
			vorderType = vOrderAutoPackFlag[0].getValue('custbody_nswmssoordertype');
			nlapiLogExecution('ERROR', 'Order Type',vOrdAutoPackFlag);
			nlapiLogExecution('ERROR', 'AutoPackFlag @ Order Type',vOrdAutoPackFlag);
			nlapiLogExecution('ERROR', 'Carrier @ Order Type',vCarrierType);
		}*/

		//if(vOrdAutoPackFlag!='T' || (vCarrierType=="" || vCarrierType==null)){
		var vStageAutoPackFlag = getAutoPackFlagforStage(whlocation,vorderType);
		if(vStageAutoPackFlag!=null && vStageAutoPackFlag.length>0){
			vStgCarrierType = vStageAutoPackFlag[0][0];
			vStgAutoPackFlag = vStageAutoPackFlag[0][1];
			vStgCarrierName = vStageAutoPackFlag[0][2];
			nlapiLogExecution('ERROR', 'AutoPackFlag(Stage)',vStgAutoPackFlag);
			nlapiLogExecution('ERROR', 'Carrier(Stage)',vStgCarrierType);
		}
		//}

		//if(vCarrierType=="" && vStgCarrierType!="")
		vCarrierType=vStgCarrierType;

		nlapiLogExecution('ERROR', 'Carrier', vCarrierType);

		//if(vOrdAutoPackFlag=='T' || vStgAutoPackFlag=='T'){
		var context = nlapiGetContext();
		nlapiLogExecution('ERROR','Remaining usage at the start of UpdatesInOpenTask ',context.getRemainingUsage());
		UpdatesInOpenTask(vebizOrdNo,vCarrierType,vStgCarrierName);
		//}
	}
	nlapiLogExecution('ERROR', 'out of AutoPacking');
}
function getAutoPackFlagforOrdType(vebizOrdNo){
	nlapiLogExecution('ERROR', 'into getAutoPackFlagforOrdType eBizOrdNo',vebizOrdNo);
	var searchresults = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter('internalid', null, 'is', vebizOrdNo));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_carrier_type','custbody_salesorder_carrier');
	columns[1] = new nlobjSearchColumn('custrecord_autopackflag','custbody_nswmssoordertype');
	columns[2] = new nlobjSearchColumn('custbody_nswmssoordertype');
	searchresults = nlapiSearchRecord('salesorder', null, filters, columns);
//	var	searchresults = nlapiLoadRecord('salesorder', vebizOrdNo);
	nlapiLogExecution('ERROR', 'out of getAutoPackFlagforOrdType', searchresults);
	return searchresults;

}
function getAutoPackFlagforStage(whlocation,vorderType){
	nlapiLogExecution('ERROR', 'into getAutoPackFlagforStage');
	nlapiLogExecution('ERROR', 'whlocation',whlocation);
	nlapiLogExecution('ERROR', 'vorderType',vorderType);
	var vStgRule = new Array();
	//getStageRule(Item, vCarrier, vSite, vCompany, stgDirection,ordertype)
	vStgRule = getStageRule('', '', whlocation, '', 'OUB',vorderType);
	nlapiLogExecution('ERROR', 'Stage Rule', vStgRule);

	nlapiLogExecution('ERROR', 'out of getAutoPackFlagforStage');
	return vStgRule;
}
function UpdatesInOpenTask(vebizOrdNo,vCarrierType,CarrieerName){
	nlapiLogExecution('ERROR', 'into UpdatesInOpenTask function', vebizOrdNo);
	nlapiLogExecution('ERROR', 'CarrierType', vCarrierType);
	var packstation = getPackStation();
	var DataRecArr=new Array();
	var containerlpArray=new Array();
	var ebizorderno;
	var filters = new Array();
	nlapiLogExecution('ERROR','orderno in getSalesOrderlist',vebizOrdNo);

	if(vebizOrdNo!=""){
		filters.push( new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', vebizOrdNo));
	}

	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//Status Flag - C (Picks Confirmed)
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK
	//filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vContLp));//Task Type - PICK

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[4] = new nlobjSearchColumn('custrecord_comp_id');
	columns[5] = new nlobjSearchColumn('custrecord_site_id');
	columns[6] = new nlobjSearchColumn('custrecord_container');
	columns[7] = new nlobjSearchColumn('custrecord_total_weight');
	columns[8] = new nlobjSearchColumn('custrecord_sku');

	//columns[0].setSort();
	columns[1].setSort();
	var salesOrderList = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	
	if(salesOrderList != null && salesOrderList.length > 0){
		var orderno, containerno,ebizcntrlno,siteid,compid,containersize,totweight,internalid,item;	
		for (var i = 0; i < salesOrderList.length; i++){
			var context = nlapiGetContext();
			nlapiLogExecution('ERROR','Remaining usage in the for loop starting ',context.getRemainingUsage());
			orderno = salesOrderList[i].getValue('name');
			containerno = salesOrderList[i].getValue('custrecord_container_lp_no');
			ebizorderno = salesOrderList[i].getValue('custrecord_ebiz_order_no');
			ebizcntrlno = salesOrderList[i].getValue('custrecord_ebiz_cntrl_no');
			compid = salesOrderList[i].getValue('custrecord_comp_id');
			siteid = salesOrderList[i].getValue('custrecord_site_id');
			containersize = salesOrderList[i].getValue('custrecord_container');
			totweight = salesOrderList[i].getValue('custrecord_total_weight');
			item = salesOrderList[i].getValue('custrecord_sku');//modified on dec 12

			internalid = salesOrderList[i].getId();
			containerlpArray[i]=containerno;
			DataRecArr.push(new DataRec(compid,siteid,ebizorderno,orderno,containerno,packstation,ebizcntrlno,item));
			var context = nlapiGetContext();
			nlapiLogExecution('ERROR','Remaining usage Before updating status to pack complete to open task ',context.getRemainingUsage());
			updateStatusInOpenTask(internalid,28);//28 - Status Flag - Outbound Pack Complete
			var context = nlapiGetContext();
			nlapiLogExecution('ERROR','Remaining usage After update status to opentask ',context.getRemainingUsage());
			//updateStatusInLPMaster(containerno,28);//28 - Status Flag - Outbound Pack Complete Moved to below function
			//CreatePACKTask(compid,siteid,ebizorderno,orderno,containerno,14,packstation,packstation,28,ebizcntrlno,item);//14 - Task Type - PACK

//			if(vCarrierType=="PC")
//			CreateShippingManifestRecord(ebizorderno,containerno,CarrieerName);
		} 


		if(DataRecArr.length>0)
		{
			var vtotalpackcount=removeDuplicateElement(containerlpArray);
			nlapiLogExecution('ERROR', 'DataRecArr.length', DataRecArr.length);
			var oldcontainer="";
			var packcount=0;
			for (var k = 0; k < DataRecArr.length; k++)
			{	
				var DataRecord=DataRecArr[k];
				var containerlpno =  DataRecord.DataRecContainer;
				nlapiLogExecution('ERROR', 'Old Container', oldcontainer);
				nlapiLogExecution('ERROR', 'New Container', containerlpno);
				if(oldcontainer!=containerlpno)
				{
					packcount=parseInt(packcount)+1;
					updateStatusInLPMaster(containerno,28);//28 - Status Flag - Outbound Pack Complete
					var context = nlapiGetContext();
					nlapiLogExecution('ERROR','Remaining usage after update status in lp master ',context.getRemainingUsage());
					CreatePACKTask(DataRecord.compid,DataRecord.DataRecSiteId,DataRecord.DataRecEbizOrdNo,DataRecord.DataRecOrdNo,DataRecord.DataRecContainer,14,DataRecord.DataRecPackStation,DataRecord.DataRecPackStation,28,DataRecord.DataRecEbizControlNo,DataRecord.DataRecItem);//14 - Task Type - PACK
					var context = nlapiGetContext();
					nlapiLogExecution('ERROR','Remaining usage after pack task creation ',context.getRemainingUsage()); 
					if(vCarrierType=="PC")
					{	
						CreateShippingManifestRecord(ebizorderno,containerlpno,CarrieerName,null,null,packcount,vtotalpackcount);
					}
					var context = nlapiGetContext();
					nlapiLogExecution('ERROR','Remaining usage after Ship manifest ',context.getRemainingUsage());
					oldcontainer = containerlpno;

				}
			}
		}
	}

	nlapiLogExecution('ERROR', 'out of UpdatesInOpenTask function', vebizOrdNo);
}
function pickreportOpentaskrecords(opentaskarray,vOrdeNo)
{
	var vQbWave=opentaskarray["vQbWave"];
	var locationId=opentaskarray["locationId"];
	var filters = new Array();
	//filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9']));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(vQbWave)));
	if(locationId!=null)
	{
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof',locationId));
	}
	filters.push(new nlobjSearchFilter('name', null, 'is', vOrdeNo));

	var columns = new Array();

	columns.push(new nlobjSearchColumn('custrecord_line_no', null, 'group'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no', null, 'group'));
	columns.push(new nlobjSearchColumn('custrecord_expe_qty', null, 'sum'));
	columns.push(new nlobjSearchColumn('custrecord_lpno', null, 'group'));
	columns.push(new nlobjSearchColumn('custrecord_actbeginloc', null,'group'));
	columns.push(new nlobjSearchColumn('custrecord_sku', null, 'group'));
	columns.push(new nlobjSearchColumn('custrecord_sku_status', null, 'group'));
	columns.push(new nlobjSearchColumn('custrecord_packcode', null, 'group'));
	columns.push(new nlobjSearchColumn('name', null, 'group'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_clus_no', null, 'group'));
	columns.push(new nlobjSearchColumn('custrecord_container', null, 'group'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_order_no', null, 'max'));
	columns.push(new nlobjSearchColumn('custrecord_batch_no', null, 'group'));
	columns.push(new nlobjSearchColumn('custrecord_total_weight', null, 'group'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid', null, 'group'));

	columns[4].setSort();
	columns[12].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return searchresults;
}
function removeDuplicateSoArray(arrayName)
{

	var newArray=new Array();
	label:for(var i=0; i<arrayName.length;i++ )
	{  
		for(var j=0; j<newArray.length;j++ )
		{
			if(newArray[j]==arrayName[i]) 
				continue label;
		}
		newArray[newArray.length] = arrayName[i];
	}
	return newArray;

}
function createPickReportHtml(opentaskarray,salesorder,locationRecord,customerentityrecord)
{
	var vQbWave=opentaskarray["vQbWave"];
	var VOrderNo=opentaskarray["VOrderNo"];

	var vSalesInternalId=opentaskarray["vSalesInternalId"];
	nlapiLogExecution('ERROR', 'createPickReportHtml','createPickReportHtml');	
	var searchresults =pickreportOpentaskrecords(opentaskarray,VOrderNo);
	nlapiLogExecution('ERROR', 'searchresults',searchresults[1]);
	if(searchresults!=null&&searchresults!=''&&searchresults.length>0)
	{
		var totalwt=0;
		//calculate total wt for particular so#
		for (var x = 0; x < searchresults.length; x++)
		{
			var searchresult = searchresults[x];



			if( searchresults[x].getValue('custrecord_total_weight', null, 'group')!=null && searchresults[x].getValue('custrecord_total_weight', null, 'group')!="")
				var packweight=searchresults[x].getValue('custrecord_total_weight', null, 'group');
			var packqty= searchresult.getValue('custrecord_expe_qty', null, 'sum');
			var totalpackageweight=parseFloat(packweight)*parseFloat(packqty);	
			totalwt=parseFloat(totalwt)+parseFloat(totalpackageweight);
			totalwt=totalwt.toFixed(2)+' Lbs';
			if(totalwt=="NaN")
			{
				totalwt="0.00 Lbs";
			}

		}

		var replaceChar =/\$|,|@|&|#|~|`|\%|\*|\^|\&|\+|\=|\-|\_|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|\./g;
		var trantype = nlapiLookupField('transaction', vSalesInternalId, 'recordType');
		//var salesorder = nlapiLoadRecord(trantype, vSalesInternalId);
		var	carrier = salesorder.getFieldText('shipmethod');
		var SalesorderNo= salesorder.getFieldValue('tranid');
		var locationId=opentaskarray["locationId"];
		var thirdpartyaccountno=salesorder.getFieldValue('custbody_lj_tran_thirdpartyacct');
		if((thirdpartyaccountno==null)||(thirdpartyaccountno==''))
		{
			thirdpartyaccountno="";
		}

		//var locationId =salesorder.getFieldValue('location');
		nlapiLogExecution('ERROR', 'PickreportLocaton',locationId);	
		var shipaddressee="";var shipaddr1="";var shipaddr2="";var shipcity="";var shipcountry="";var shipstate="";var shipzip="";var shipstateandcountry="";
		var shipattention="";
		shipattention=salesorder.getFieldValue('shipattention'); 
		shipaddressee=salesorder.getFieldValue('shipaddressee'); 
		shipaddr1=salesorder.getFieldValue('shipaddr1'); 
		shipaddr2=salesorder.getFieldValue('shipaddr2');
		shipcity=salesorder.getFieldValue('shipcity'); 
		shipcountry=salesorder.getFieldValue('shipcountry'); 
		shipstate=salesorder.getFieldValue('shipstate'); 
		shipzip=salesorder.getFieldValue('shipzip'); 
		var subsidiaryId=salesorder.getFieldValue('subsidiary');
		nlapiLogExecution('ERROR', 'subsidiaryId',subsidiaryId);
		var customerlogoId="";
		if((subsidiaryId!=null)&&(subsidiaryId!=''))
		{
			var subsidarysearch=nlapiLoadRecord('subsidiary', subsidiaryId);

			if((subsidarysearch!=null)&&(subsidarysearch!=''))
			{
				customerlogoId=subsidarysearch.getFieldValue('logo'); 
			}
		}
		nlapiLogExecution('ERROR', 'customerlogoId',customerlogoId);
		var ismultilineship=opentaskarray["ismultilineship"];
		var shipaddress=salesorder.getFieldValue('shipaddress'); 
		if((shipaddress==null)||(shipaddress==''))
		{
			shipaddress="";
		}
		if(ismultilineship=='T')
		{
			if((customerentityrecord!="")&&(customerentityrecord!=""))
			{
				shipattention=customerentityrecord.getFieldValue('shipattention'); 
				shipaddr1 = customerentityrecord.getFieldValue('shipaddr1');
				shipaddr2 = customerentityrecord.getFieldValue('shipaddr2');
				shipaddressee = customerentityrecord.getFieldValue('shipaddressee');
				shipcity = customerentityrecord.getFieldValue('shipcity');
				shipstate = customerentityrecord.getFieldValue('shipstate');
				shipzip = customerentityrecord.getFieldValue('shipzip');
				shipcountry= customerentityrecord.getFieldValue('shipcountry');

			}

		}

		if((shipattention==null)||(shipattention==''))
		{
			shipattention="";
		}
		if((shipaddressee==null)||(shipaddressee==''))
		{
			shipaddressee="";
		}
		else
		{
			shipaddressee=shipaddressee.replace(replaceChar,'');
		}
		if((shipaddr1==null)||(shipaddr1==''))
		{
			shipaddr1="";
		}
		else
		{
			shipaddr1=shipaddr1.replace(replaceChar,'');
		}
		if((shipaddr2==null)||(shipaddr2==''))
		{
			shipaddr2="";
		}
		else
		{
			shipaddr2=shipaddr2.replace(replaceChar,'');
		}
		if((shipcity==null)||(shipcity==''))
		{
			shipcity="";
		}
		else
		{
			shipcity=shipcity.replace(replaceChar,'');
		}
		if((shipcountry==null)||(shipcountry==''))
		{
			shipcountry="";
		}
		else
		{
			shipcountry=shipcountry.replace(replaceChar,'');
		}
		if((shipstate==null)||(shipstate==''))
		{
			shipstate="";
		}
		else
		{
			shipstate=shipstate.replace(replaceChar,'');
		}
		if((shipzip==null)||(shipzip==''))
		{
			shipzip="";
		}
		else
		{
			shipzip=shipzip.replace(replaceChar,'');
		}
		if((shipaddr2==null)||(shipaddr2==''))
		{
			shipaddr1=shipaddr1;
		}
		else
		{
			shipaddr1=shipaddr1+","+shipaddr2;
			shipaddr1=shipaddr1.replace(replaceChar,'');
		}
		shipstateandcountry=shipcity+", "+shipstate+" "+shipzip;
		var addr1="";var city="";var state="";var zip=""; var stateandzip=""; var returnadresse="";
		if(locationId!=null)
		{
			//var locationRecord =nlapiLoadRecord('Location',locationId);
			addr1=locationRecord.getFieldValue('addr1');
			if((addr1==null)||(addr1==''))
			{
				addr1="";
			}
			else
			{
				addr1=addr1.replace(replaceChar,'');
			}
			city=locationRecord.getFieldValue('city');
			if((city==null)||(city==''))
			{
				city="";
			}
			else
			{
				city=city.replace(replaceChar,'');
			}
			state=locationRecord.getFieldValue('state');
			if((state==null)||(state==''))
			{
				state="";
			}
			else
			{
				state=state.replace(replaceChar,'');
			}

			zip=locationRecord.getFieldValue('zip');
			if((zip==null)||(zip==''))
			{
				zip="";
			}
			else
			{
				zip=zip.replace(replaceChar,'');
			}

			returnadresse=locationRecord.getFieldValue('addressee');
			if((returnadresse==null)||(returnadresse==''))
			{
				returnadresse="";
			}
			else
			{
				returnadresse=returnadresse.replace(replaceChar,'');
			}
			stateandzip=city+", "+state+" "+zip;
		}
		var  virtualDriveName="C";
		var file_name="LOGOCOMP";
		var  headerimage='"'+virtualDriveName+'://EBIZNET//'+file_name+'.JPG"';
		var strVar="";
		strVar +="<html>";
		strVar +="<body style=\"padding-right:0px; padding-left:0px;padding-top:0px;padding-bottom:0px;\">";
		strVar += "<table style=\"width:100%;font-family: Calibri;\">";
		strVar += "    <tr style=\"height:\"><td align=\"left\" style=\"width: 12%; font-size: 12px; vertical-align: top;\">";
		if((customerlogoId!=null)&&(customerlogoId!=""))
		{
			strVar +="<img src=\"headerimage\"/> ";
		}
		else
		{
			strVar +="<img src="+headerimage+"/>";
		}
		strVar +="<\/td>";
		strVar += "    <td align=\"top\" style=\" font-size:12px;vertical-align:text-top; text-align:center\">*** PICK REPORT ***<\/td>";
		strVar += "    <td><\/td><\/tr>";
		strVar += "    <tr><td><\/td><\/tr>";
		strVar += "    <tr>";
		strVar += "    <td style=\" text-align:left\" valign=\"top\">";
		strVar += "    <table border=\"0\" width=\"310px\" frame=\"box\" rules=\"all\" cellpadding=\"2\" cellspacing=\"0\" style=\"vertical-align: top;\">";


		strVar += "    <tr>";
		strVar += "    <td style=\"font-size: 12px; text-align: center; border-left: 1px solid black; border-top: 1px solid black;\">";
		strVar += "					                              <b> Ship Method </b>";
		strVar += "					                           <\/td>";
		strVar += "					                           ";
		strVar += "    <td style=\"font-size: 12px; text-align: center;border-bottom: none; border-left: 1px solid black;  border-top: 1px solid black; border-right: 1px solid black;\">";
		strVar += "					                             "+carrier+"";
		strVar += "					                           <\/td>";

		strVar += "    <\/tr>";
		strVar += "    <tr >";
		strVar += "					                           <td style=\"font-size: 12px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black;";
		strVar += "					                               border-right: none;\">";
		strVar += "					                              <b> Weight </b>";
		strVar += "					                           <\/td>";
		strVar += "					                           ";
		strVar += "					                           <td style=\"font-size: 12px;border-bottom: 1px solid black; height:30px; text-align: center; border-left: 1px solid black; border-top: 1px solid black;";
		strVar += "					                               border-right: 1px solid black;\">";
		strVar += "					                              "+totalwt+" ";
		strVar += "					                           <\/td>";
		strVar += "    <\/tr>";
		if(thirdpartyaccountno!=null && thirdpartyaccountno!=''){			

			strVar += "<tr>";
			strVar += "                                <td style=\"font-size: 12px; text-align: center; border-bottom: 1px solid black; border-left: 1px solid black;";
			strVar += "                                    height: 26px; border-top: none;\">";
			strVar += "                                    <b> 3rd Party Account # </b>";
			strVar += "                                <\/td>";
			strVar += "                                <td style=\"font-size: 12px; border-bottom: 1px solid black; height: 26px; text-align: center;";
			strVar += "                                    border-left: 1px solid black; border-top: none; border-right: 1px solid black;\">";
			strVar += "                                    "+thirdpartyaccountno+"";
			strVar += "                                <\/td>";
			strVar += "                            <\/tr>";
		}
		strVar += "    <\/table>";
		strVar += "    <\/td>";
		strVar += "    <td style=\"width:250px\"><\/td>";


		strVar += "    <td>";
		strVar += "    <table border=\"0\"  width=\"100%\" frame=\"box\" rules=\"all\" cellpadding=\"2\" cellspacing=\"0\" style=\"vertical-align:middle; text-align:right\">";


		strVar += "    <tr>";

		strVar += "    <td style=\"font-size: 12px; text-align: center; border-left: 1px solid black; border-top: 1px solid black;\">";
		strVar += "					                             <b>  Wave # </b>";
		strVar += "					                           <\/td>";
		strVar += "					                           ";


		strVar += "					                           <td style=\"font-size: 12px; text-align: center; border-left: 1px solid black; border-top: 1px solid black;";
		strVar += "					                               border-right: none;\">";
		strVar += "					                              <b> W/H Order # </b>";
		strVar += "					                           <\/td>";
		strVar += "<td style=\"font-size: 12px; text-align: center; border-left: 1px solid black; border-top: 1px solid black;";
		strVar += "                                    border-right: 1px solid black;\">";
		strVar += "                                    <b> Sales Order # </b>";
		strVar += "                                <\/td>";
		strVar += "    <\/tr>";

		strVar += "    <tr  valign=\"middle\">";
		vQbWave=parseInt(vQbWave).toString();
		strVar += "    <td style=\"font-size: 12px; height:30px; text-align: center; border-bottom: none; border-left: 1px solid black; border-top: 1px solid black;\">";
		strVar += "					                             "+vQbWave+" ";
		strVar += "					                           <\/td>";
		strVar += "					                           ";	

		strVar += "					                           <td style=\"font-size: 12px; border-bottom: none; height:30px; text-align: center; border-left: 1px solid black; border-top: 1px solid black;";
		strVar += "					                               border-right: none;\">";
		strVar += "					                            "+VOrderNo+"";
		strVar += "					                           <\/td>";
		strVar += "<td style=\"font-size: 12px; border-bottom: none; height: 30px; text-align: center;";
		strVar += "                                    border-left: 1px solid black; border-top: 1px solid black; border-right: 1px solid black;\">";
		strVar += "                                    "+SalesorderNo+"";
		strVar += "                                <\/td>";
		strVar += "    <\/tr>";

		strVar += "<tr valign=\"middle\">";
		strVar += "                                <td style=\"font-size: 12px; height: 30px; text-align: center; border-bottom: 1px solid black;";
		strVar += "                                    border-left: 1px solid black; border-top: none;\">";
		strVar += "                                    <font size=\"4\" face=\"MRV Code39extS\">*"+vQbWave+"*<\/font>";
		strVar += "                                <\/td>";
		strVar += "                                <td style=\"font-size: 12px; border-bottom: 1px solid black; height: 30px; text-align: center;";
		strVar += "                                    border-left: 1px solid black; border-top: none; border-right: none;\">";

		strVar += "                                <\/td>";
		strVar += "                                <td style=\"font-size: 12px; border-bottom: 1px solid black; height: 30px; text-align: center;";
		strVar += "                                    border-left: 1px solid black; border-top: none; border-right: 1px solid black;\">";
		strVar += "                                    <font size=\"4\" face=\"MRV Code39extS\">*"+SalesorderNo+"*<\/font>";
		strVar += "                                <\/td>";
		strVar += "                            <\/tr>";

		strVar += "    <\/table>";

		strVar += "    <\/td>";

		strVar += "    <\/tr>";

		var shipinstructions=salesorder.getFieldValue('custbody_nswmspoinstructions');
		if((shipinstructions==null)||(shipinstructions==''))
		{
			shipinstructions='';
		}
		if((shipinstructions!=null)&&(shipinstructions!=''))
		{
			strVar += "<tr>";

			strVar += "<td colspan=\"5\"  width=\"100%\">";
			strVar += "<table cellspacing=\"0\";width=\"100%\" height=\"30px\">";
			strVar += "<tr>";
			strVar += "<td   width=\"4%\" style=\"font-size: 20px; font-family:Calibri; white-space: nowrap; vertical-align: bottom;text-align:center;\">";
			strVar += "<b>Shipping Instructions  :<\/b>";
			strVar += "<\/td>";
			strVar += "<td  style=\"font-size: 20px; font-weight: bold; font-family:Calibri;vertical-align: bottom;text-align:Left;\">";

			strVar += " &nbsp;"+shipinstructions+"";
			strVar += "<\/td>";
			strVar += "<\/tr>";
			strVar += "<\/table>";
			strVar += " <\/td>";
			strVar += " <\/tr> ";
		}
		else
		{

			strVar += "<tr>";
			strVar += "<td>";
			strVar += "<\/td>";
			strVar += "<\/tr>";
		}
		strVar += "    <tr style=\"vertical-align:top\">";
		strVar += "<td>";
		strVar += "<table>";
		strVar += "<tr>";
		strVar += " <td style=\"font-size: 12px; white-space: nowrap; vertical-align: top;\">";
		strVar += " &nbsp;&nbsp;&nbsp;<b>Ship To: </b>";
		strVar += "<\/td>";
		strVar += "<td style=\"font-size: 12px;\">";
		strVar += "<table>";


		var intercostatus=salesorder.getFieldText('intercostatus');

		if(intercostatus!="Paired")
		{
			if((shipattention!=null)&&(shipattention!=""))
			{
				strVar += " <tr>";
				strVar += "<td style=\"font-size: 12px;\">";
				strVar += " &nbsp;"+shipattention+"";
				strVar += " <\/td>";
				strVar += "<\/tr>";
			}
			strVar += " <tr>";
			strVar += "<td style=\"font-size: 12px;\">";
			strVar += " &nbsp;"+shipaddressee+"";
			strVar += " <\/td>";
			strVar += "<\/tr>";

			strVar += "<tr>";
			strVar += " <td style=\"font-size: 12px;\">";
			strVar += " "+shipaddr1+"";
			strVar += "<\/td>";
			strVar += "<\/tr>";

			strVar += "<tr>";
			strVar += " <td style=\"font-size: 12px;\">";
			strVar += " "+shipstateandcountry+"";
			strVar += "<\/td>";
			strVar += "<\/tr>";
		}
		else
		{
			nlapiLogExecution('ERROR', 'intercostatus',intercostatus);	
			strVar +="<tr style=\"font-size: 12px;text-align: Left;\">";
			strVar += "<td style=\"font-size: 12px;width:150px;text-align:Left;\">";
			strVar += "&nbsp;"+shipaddress+"";
			strVar +="<\/td>";
			strVar +="<\/tr>";
		}
		/*
		strVar += "<tr>";
		strVar += " <td style=\"font-size: 12px;\">";
		strVar += " ";
		strVar += "<\/td>";
		strVar += "<\/tr>";
		 */
		strVar += "<\/table>";
		strVar += " <\/td>";
		strVar += "<\/tr>";
		strVar += "<\/table>";
		strVar += "<\/td>";
		strVar +="<td><\/td>";


		strVar += "<td>";
		strVar += "<table style=\"font-family: Calibri;\">";
		strVar += "<tr>";
		strVar += " <td style=\"font-size: 12px; white-space: nowrap; vertical-align: top;\">";
		strVar += " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b> Location: </b>";
		strVar += "<\/td>";
		strVar += "<td style=\"font-size: 12px;\">";
		strVar += "<table>";
		strVar += " <tr>";
		strVar += "<td style=\"font-size: 12px;\">";
		strVar += " &nbsp;"+returnadresse+"";
		strVar += "<\/tr>";

		strVar += "<tr>";
		strVar += " <td style=\"font-size: 12px;\">";
		strVar += " "+addr1+"";
		strVar += "<\/td>";
		strVar += "<\/tr>";

		strVar += "<tr>";
		strVar += " <td style=\"font-size: 12px;\">";
		strVar += " "+stateandzip+"";
		strVar += "<\/td>";
		strVar += "<\/tr>";
		/*
		strVar += "<tr>";
		strVar += " <td style=\"font-size: 12px;\">";
		strVar += " ";
		strVar += "<\/td>";
		strVar += "<\/tr>";
		 */
		strVar += "<\/table>";
		strVar += " <\/td>";
		strVar += "<\/tr>";
		strVar += "<\/table>";
		strVar += "<\/td>";
		strVar += "<\/tr>";
		strVar += "<tr><td colspan=\"10\">";
		strVar += " <table  cellspacing=\"0\"  style=\"width: 100%; font-family:Calibri; border-right: 1px solid black;border-left: 1px solid black; border-top: 1px solid black;border-bottom: 1px solid black;\" rules=\"all\" align=\"left\" border=\"0\" frame=\"box\">";

		strVar += "<tr style=\"height:50px\">";
		strVar += " <td style=\"font-size: 12px; border-right: 1px solid black; width:4%; text-align:center;\"><b>Line # </b><\/td>";
		strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;width:10%;text-align:center;\"> <b> Bin Location </b><\/td>";
		strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;width:15%;text-align:center;\"> <b>Item # </b><\/td>";
		strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;width:4%;text-align:center;\"> <b>Qty </b><\/td>";
		strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;width:13%;text-align:center;\"> <b>Lot # </b><\/td>";
		strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;width:13%;text-align:center;\"> <b>LP # </b><\/td>";
		strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;width:4%;text-align:center;\"> <b> Pack Code </b><\/td>";
		strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;width:18%;text-align:center;\"> <b>Item Description </b><\/td>";
		strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;width:13%;text-align:center;\"> <b>UPC Code </b><\/td>";		
		strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;width:8%;text-align:center;\"><b> Status </b><\/td>";




		//strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;width:12%;text-align:center;\">Container LP #<\/td>";
		strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;width:12%;text-align:center;\"> <b>Container Size </b><\/td>";
		strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;width:12%;text-align:center;\"> <b>Cluster </b><\/td>";
		strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;width:12%;text-align:center;\"> <b>Weight </b><\/td>";
		strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;width:12%;text-align:center;\"> <b>Zone </b><\/td>";
		strVar += " <\/tr>";
		if (searchresults != null) {
			for (var i = 0; i < searchresults.length; i++) 
			{
				var searchresult = searchresults[i];
				/*
				vline = searchresult.getValue('custrecord_line_no');
				vlotbatch = searchresult.getValue('custrecord_batch_no');
				nlapiLogExecution('ERROR', 'vlotbatch',vlotbatch);
				vitem = searchresult.getValue('custrecord_ebiz_sku_no');
				vqty = searchresult.getValue('custrecord_expe_qty');
				vTaskType = searchresult.getText('custrecord_tasktype');
				vLpno = searchresult.getValue('custrecord_lpno');
				vlocationid = searchresult.getValue('custrecord_actbeginloc');
				vlocation = searchresult.getText('custrecord_actbeginloc');
				vSKUID=searchresult.getValue('custrecord_sku');
				var Itype = nlapiLookupField('item', vSKUID, 'recordType');
				var ItemRec = nlapiLoadRecord(Itype, vSKUID);
				vSKU = ItemRec.getFieldValue('itemid');
				vskustatus = searchresult.getText('custrecord_sku_status');
				vpackcode = searchresult.getValue('custrecord_packcode');
				vdono = searchresult.getValue('name');
				vcontlp=searchresult.getValue('custrecord_container_lp_no');
				vcontsize=searchresult.getText('custrecord_container');
				vclusno=searchresult.getValue('custrecord_ebiz_clus_no');
				upccode=searchresult.getValue('upccode','custrecord_sku');
				itemdesc=searchresult.getValue('description','custrecord_sku');
				vweight=searchresult.getValue('custrecord_total_weight');
				var vWMSCarrier=searchresult.getText('custrecord_ebizwmscarrier');
				var vzone=searchresult.getText('custrecord_ebiz_zoneid');
				 */


				var vSKUID=searchresult.getValue('custrecord_sku', null, 'group');
				var vdono = searchresult.getValue('name', null, 'group');

				var poItemFields = ['itemid','upccode','salesdescription'];
				var rec = nlapiLookupField('item', vSKUID, poItemFields);

				//rec=nlapiLoadRecord('item',itemID);
				var vSKU=rec.itemid;
				var itemdesc=rec.salesdescription;
				var upccode=rec.upccode;

				//var ItemRec = nlapiLoadRecord(Itype, vSKUID);


				var vskustatus = searchresult.getText('custrecord_sku_status', null, 'group');

				//var vSKU = ItemRec.getFieldValue('itemid');
				//var upccode = ItemRec.getFieldValue('upccode');
				//var itemdesc = ItemRec.getFieldValue('salesdescription');

				strVar += " <tr style=\"height:40px\">";

				var vline = searchresult.getValue('custrecord_line_no', null, 'group');
				strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;border-top: 1px solid black; width:4%; text-align:center;\">"+vline+"<\/td>";
				//var vSKU = ItemRec.getFieldValue('itemid');
				//var vSKU=rec.itemid;
				var vlocation = searchresult.getText('custrecord_actbeginloc', null, 'group');
				if(vlocation=="- None -")
				{
					vlocation="";
				}
				strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;border-top: 1px solid black;width:11%;text-align:center;\">"+vlocation+"<\/td>";

				strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;border-top: 1px solid black;width:15%;text-align:center;\">"+vSKU+"<\/td>";

				var vqty = searchresult.getValue('custrecord_expe_qty', null, 'sum');
				strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;border-top: 1px solid black;width:4%;text-align:center;\">"+vqty+"<\/td>";

				var vlotbatch = searchresult.getValue('custrecord_batch_no', null, 'group');
				if(vlotbatch=="- None -")
				{
					vlotbatch="";
				}
				strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;border-top: 1px solid black;width:13%;text-align:center;\">"+vlotbatch+"<\/td>";

				var vLpno = searchresult.getValue('custrecord_lpno', null, 'group');
				if(vLpno=="- None -")
				{
					vLpno="";
				}
				strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;border-top: 1px solid black;width:13%;text-align:center;\">"+vLpno+"<\/td>";

				var vpackcode = searchresult.getValue('custrecord_packcode', null, 'group');
				if(vpackcode=="- None -")
				{
					vpackcode="";
				}
				strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;border-top: 1px solid black;width:4%;text-align:center;\">"+vpackcode+"<\/td>";

				strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;border-top: 1px solid black;width:18%;text-align:center;\">"+itemdesc+"<\/td>";

				strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;border-top: 1px solid black;width:13%;text-align:center;\">"+upccode+"<\/td>";


				var vskustatus = searchresult.getText('custrecord_sku_status', null, 'group');
				if(vskustatus=="- None -")
				{
					vskustatus="";
				}
				strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;border-top: 1px solid black;width:8%;text-align:center;\">"+vskustatus+"<\/td>";







				//strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;border-top: 1px solid black;width:12%;text-align:center;\">"+vcontlp+"<\/td>";
				var vcontsize=searchresult.getText('custrecord_container', null, 'group');
				if(vcontsize=="- None -")
				{
					vcontsize="";
				}
				strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;border-top: 1px solid black;width:12%;text-align:center;\">"+vcontsize+"<\/td>";

				var vclusno=searchresult.getValue('custrecord_ebiz_clus_no', null, 'group');
				if(vclusno=="- None -")
				{
					vclusno="";
				}
				strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;border-top: 1px solid black;width:12%;text-align:center;\">"+vclusno+"<\/td>";

				var vweight=searchresult.getValue('custrecord_total_weight', null, 'group');
				if(vweight=="- None -")
				{
					vweight="";
				}
				strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;border-top: 1px solid black;width:12%;text-align:center;\">"+vweight+"<\/td>";

				var vzone=searchresult.getText('custrecord_ebiz_zoneid', null, 'group');
				if(vzone=="- None -")
				{
					vzone="";
				}
				strVar += " <td style=\"font-size: 12px; border-right: 1px solid black;border-top: 1px solid black;width:12%;text-align:center;\">"+vzone+"<\/td>";

				strVar += " <\/tr>";
			}
		}
		strVar += " <\/table>";
		strVar += "<\/td><\/tr>";

		strVar += "    <\/table>";
		strVar += "    <\/body>";
		strVar += "    <\/html>";
		var tasktype='3';
		var labeltype='PickReport';
		var print='F';
		var reprint='F';
		var company='';

		var formattype='html';
		var labelrecord = nlapiCreateRecord('customrecord_labelprinting'); 
		labelrecord.setFieldValue('name', vQbWave); 
		labelrecord.setFieldValue('custrecord_labeldata',strVar);  
		labelrecord.setFieldValue('custrecord_label_refno',vQbWave);     
		labelrecord.setFieldValue('custrecord_label_tasktype',tasktype);   
		labelrecord.setFieldValue('custrecord_labeltype',labeltype);    
		labelrecord.setFieldValue('custrecord_label_lp',VOrderNo);
		labelrecord.setFieldValue('custrecord_label_location',locationId);
		labelrecord.setFieldValue('custrecord_label_print', print);
		labelrecord.setFieldValue('custrecord_label_reprint', reprint);
		labelrecord.setFieldValue('custrecord_label_company', company);
		labelrecord.setFieldValue('custrecord_label_refno1', customerlogoId);
		//labelrecord.setFieldValue('custrecord_label_location', location);
		var tranid = nlapiSubmitRecord(labelrecord);
	}
}
function calcTime(offset) {

	//var vtime=new Date();

	//nlapiLogExecution('ERROR', 'TimeOffset', vtime.getTimezoneOffset());

	// create Date object for current location
	d = new Date();

	// convert to msec
	// add local time zone offset 
	// get UTC time in msec
	utc = d.getTime() + (d.getTimezoneOffset() * 60000);

	// create new Date object for different city
	// using supplied offset
	nd = new Date(utc + (3600000*offset));

	// return time as a string
	//return "The local time in " + city + " is " + nd.toLocaleString();

	var stringDt=((parseInt(nd.getMonth()) + 1) + '/' + (parseInt(nd.getDate())) + '/' + nd.getFullYear());

	var timestamp;
	var a_p = "";

	//Getting time in hh:mm tt format.
	var curr_hour = nd.getHours();
	var curr_min = nd.getMinutes();

	// determining the am/pm indicator
	if (curr_hour < 12)
		a_p = "am";
	else
		a_p = "pm";

	// finalizing hours depending on 24hr clock
	if (curr_hour == 0)
		curr_hour = 12;
	else if(curr_hour > 12)
		curr_hour -= 12;

	if (curr_min.length == 1)
		curr_min = "0" + curr_min;

	//Adding fields to update time zones.
	timestamp = curr_hour + ":" + curr_min + " " + a_p;

	return stringDt + " " + timestamp;


}
function removeDuplicateElement(arrayName)
{
	var newArray=new Array();
	label:for(var i=0; i<arrayName.length;i++ )
	{  
		for(var j=0; j<newArray.length;j++ )
		{
			if(newArray[j]==arrayName[i]) 
				continue label;
		}
		newArray[newArray.length] = arrayName[i];
	}
	return newArray.length;
}