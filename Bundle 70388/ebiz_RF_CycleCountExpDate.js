
/***************************************************************************
�������������������������
���������������������eBizNET Solutions
****************************************************************************
*
*� $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_CycleCountExpDate.js,v $
*� $Revision: 1.1.2.1.4.5.2.4.4.1 $
*� $Date: 2015/11/16 15:35:27 $
*� $Author: rmukkera $
*� $Name: t_WMS_2015_2_StdBundle_1_152 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_RF_CycleCountExpDate.js,v $
*� Revision 1.1.2.1.4.5.2.4.4.1  2015/11/16 15:35:27  rmukkera
*� case # 201415115
*�
*� Revision 1.1.2.1.4.5.2.4  2014/05/30 00:34:19  nneelam
*� case#  20148622
*� Stanadard Bundle Issue Fix.
*�
*� Revision 1.1.2.1.4.5.2.3  2014/04/16 07:57:30  spendyala
*� CASE201112/CR201113/LOG201121
*� Issue fixed related to case#20148006
*�
*� Revision 1.1.2.1.4.5.2.2  2013/04/17 16:02:37  skreddy
*� CASE201112/CR201113/LOG201121
*� added meta tag
*�
*� Revision 1.1.2.1.4.5.2.1  2013/03/05 13:35:38  rmukkera
*� Merging of lexjet Bundle files to Standard bundle
*�
*� Revision 1.1.2.1.4.5  2013/02/18 06:29:04  skreddy
*� CASE201112/CR201113/LOG201121
*�  RF Lot auto generating FIFO enhancement
*�
*� Revision 1.1.2.1.4.4  2012/11/23 06:35:04  schepuri
*� CASE201112/CR201113/LOG201121
*� issue related to redirecting Previous button
*�
*� Revision 1.1.2.1.4.3  2012/10/05 06:34:12  grao
*� CASE201112/CR201113/LOG201121
*�
*� Converting Multiple language into given suggestions
*�
*� Revision 1.1.2.1.4.2  2012/09/26 12:33:26  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multi language without small characters
*�
*� Revision 1.1.2.1.4.1  2012/09/21 15:01:30  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multilanguage
*�
*� Revision 1.1.2.1  2012/03/21 15:09:47  spendyala
*� CASE201112/CR201113/LOG201121
*� New Screen added to create BatchNo.
*�
*
****************************************************************************/

/**
 * @param request
 * @param response
 */
function CycleCountExpDate(request, response){
	if (request.getMethod() == 'GET') {
		try{
			var getPlanNo = request.getParameter('custparam_planno');
			nlapiLogExecution('ERROR', 'getPlanNo', getPlanNo);

			var getRecordId = request.getParameter('custparam_recordid');
			nlapiLogExecution('ERROR','getRecprdId',getRecordId);
			var getBeginLocationId = request.getParameter('custparam_begin_location_id');
			var getBeginLocationName =  request.getParameter('custparam_begin_location_name');
			var getPackCode =  request.getParameter('custparam_packcode');
			var getExpectedQuantity =  request.getParameter('custparam_expqty');
			var getExpLPNo = request.getParameter('custparam_lpno');
			var getActLPNo = request.getParameter('custparam_actlp');
			var getExpItem = request.getParameter('custparam_expitem');
			var getExpItemDescription = request.getParameter('custparam_expitemdescription');

			var getActualItem = request.getParameter('custparam_actitem');
			var itemInternalid = request.getParameter('custparam_expiteminternalid');
			var RecordCount=request.getParameter('custparam_noofrecords');
			var NextLocation=request.getParameter('custparam_nextlocation');
			var getActualPackCode = request.getParameter('custparam_actpackcode');	
			var getBatchNo = request.getParameter('custparam_actbatch');
	                var ItemShelfLife = request.getParameter('custparam_shelflife');
			var CaptureExpiryDate = request.getParameter('custparam_captureexpirydate');
			var CaptureFifoDate = request.getParameter('custparam_capturefifodate');
			
			var planitem=request.getParameter('custparam_planitem');
			
			var getLanguage = request.getParameter('custparam_language');
		    nlapiLogExecution('ERROR', 'getLanguage', getLanguage);
		    
			var st0,st1,st2,st3,st4,st5,st6;
			if( getLanguage == 'es_ES')
			{
				
				st0 = "CHECKIN FECHA DE CADUCIDAD";
				st1 = "FECHA DE MANUFACTURA";
				st2 = "FECHA DE EXPIRACI&#211;N:";
				st3 = "FECHA DE CADUCIDAD";
				st4 = "FORMATO: MMDDAA";
				st5 = "ENVIAR";
				st6 = "ANTERIOR";
								
			}
			else
			{
				st0 = "CHECKIN EXPIRY DATE";
				st1 = "MFG DATE";
				st2 = "EXP DATE:";
				st3 = "BEST BEFORE DATE";
				st4 = "FORMAT : MMDDYY";
				st5 = "SEND";
				st6 = "PREV";

			}
			

			var functionkeyHtml=getFunctionkeyScript('_rf_cyclecount_exp_date'); 
			var html = "<html><head><title>" + st0 + "</title>";
			html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
			html = html +functionkeyHtml;
			html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
			html = html + "	<form name='_rf_cyclecount_exp_date' method='POST'>";
			html = html + "		<table>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st1 + ":</td>";
			html = html + "				<td><input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + "></td>";
			html = html + "				<input type='hidden' name='hdnCycleCountPlanNo' value=" + getPlanNo + ">";
			html = html + "				<input type='hidden' name='hdnBeginLocationName' value=" + getBeginLocationName + ">";
			html = html + "				<input type='hidden' name='hdnRecordId' value=" + getRecordId + ">";
			html = html + "				<input type='hidden' name='hdnPackCode' value=" + getPackCode + ">";
			html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";		
			html = html + "				<input type='hidden' name='hdnExpLPNo' value=" + request.getParameter('custparam_lpno') + "></td>";
			html = html + "				<input type='hidden' name='hdnActLPNo' value=" + request.getParameter('custparam_actlpno') + "></td>";
			html = html + "				<input type='hidden' name='hdnExpectedItemDescription' value=" + getExpItemDescription + ">";	
			html = html + "				<input type='hidden' name='hdniteminternalid' value=" + itemInternalid + ">";
			html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
			html = html + "				<input type='hidden' name='hdnnext' value=" + NextLocation + ">";
			html = html + "				<input type='hidden' name='hdnBatchNo' value='" + getBatchNo + "'>";
			html = html + "			    	<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
	          	html = html + "				<input type='hidden' name='hdnItemShelfLife' value=" + ItemShelfLife + ">";
			html = html + "				<input type='hidden' name='hdnCaptureExpiryDate' value=" + CaptureExpiryDate + ">";
			html = html + "				<input type='hidden' name='hdnCaptureFifoDate' value=" + CaptureFifoDate + ">";
			html = html + "				<input type='hidden' name='hdnplanitem' value='" + planitem + "'>";		
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'><input name='entermfgdate' type='text'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st2 +  ":</td>";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'><input name='enterexpdate' type='text'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st3 +  ": </td>";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'><input name='enterbbdate' type='text'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'><label>" + st4;
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st5 +  " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
			html = html + "					" + st6 +  " <input name='cmdPrevious' type='submit' value='F7'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "		 </table>";
			html = html + "	</form>";
			html = html + "</body>";
			html = html + "</html>";

			response.write(html);
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR','Exception in ExpDate Request part',exp);
		}
	}
	else {
		try {

			var optedEvent = request.getParameter('cmdPrevious');

			var getMfgDate = request.getParameter('entermfgdate');
			var getExpDate = request.getParameter('enterexpdate');
			var getBestBeforeDate = request.getParameter('enterbbdate');

			nlapiLogExecution('DEBUG', 'optedEvent', optedEvent);
			var CCarray = new Array();
			var getLanguage = request.getParameter('hdngetLanguage');
			CCarray["custparam_language"] = getLanguage;
			nlapiLogExecution('ERROR', 'getLanguage', CCarray["custparam_language"]);
	    	
			
			var st7,st8,st9;
			if( getLanguage == 'es_ES')
			{
				st7 = "ERROR EN LA PANTALLA DE FECHA EXP";
				st8 = "MFG FECHA NO DEBE SER MAYOR O IGUAL A LA FECHA EXP";
				st9 = "FECHAS NO V&#193;LIDAS ";
			}
			else
			{
				st7 = "ERROR IN EXP DATE SCREEN";
				st8 = "MFGDATE SHLD NOT BE GREATER THAN OR EQUAL TO EXPDATE";
				st9 = "INVALID DATES";
			}
			
			
			CCarray["custparam_error"] = st7;
			CCarray["custparam_screenno"] = '35A';
			CCarray["custparam_recordid"] = request.getParameter('custparam_recordid');
			CCarray["custparam_planno"] = request.getParameter('custparam_planno');
			CCarray["custparam_begin_location_id"] = request.getParameter('custparam_begin_location_id');
			CCarray["custparam_begin_location_name"] = request.getParameter('custparam_begin_location_name');
			CCarray["custparam_lpno"] = request.getParameter('custparam_lpno');
			CCarray["custparam_packcode"] = request.getParameter('custparam_packcode');
			CCarray["custparam_skustatus"] = request.getParameter('custparam_skustatus');
			CCarray["custparam_expqty"] = request.getParameter('custparam_expqty');
			CCarray["custparam_actlp"] = request.getParameter('custparam_actlp');
			CCarray["custparam_expitem"] = request.getParameter('custparam_expitem');
			CCarray["custparam_expitemdescription"] = request.getParameter('custparam_expitemdescription');

			CCarray["custparam_actitem"] = request.getParameter('custparam_actitem');
			CCarray["custparam_actpackcode"] = request.getParameter('custparam_actpackcode');
			CCarray["custparam_expiteminternalid"] = request.getParameter('hdniteminternalid');
			CCarray["custparam_noofrecords"] = request.getParameter('hdnRecCount');
			CCarray["custparam_nextlocation"] = request.getParameter('hdnnext');
			CCarray["custparam_skustatus"] = request.getParameter('custparam_skustatus');
			CCarray["custparam_actbatch"] = request.getParameter('hdnBatchNo');
			CCarray["custparam_shelflife"] = request.getParameter('hdnItemShelfLife');
			CCarray["custparam_captureexpirydate"] = request.getParameter('hdnCaptureExpiryDate');
			CCarray["custparam_capturefifodate"] = request.getParameter('hdnCaptureFifoDate');
			CCarray["custparam_planitem"] = request.getParameter('hdnplanitem');
			var itemShelflife = request.getParameter('hdnItemShelfLife');
			var CaptureExpirydate = request.getParameter('hdnCaptureExpiryDate');
			var CaptureFifodate = request.getParameter('hdnCaptureFifoDate');
			nlapiLogExecution('ERROR', 'itemShelflife', itemShelflife);
			nlapiLogExecution('ERROR', 'CaptureExpirydate', CaptureExpirydate);
			nlapiLogExecution('ERROR', 'CaptureFifodate', CaptureFifodate);
						

			var error='';
			var errorflag='F';

			if (optedEvent == 'F7') {
				nlapiLogExecution('DEBUG', 'CHECK IN EXP DATE F7 Pressed');
				response.sendRedirect('SUITELET', 'customscript_rf_cyclecountbatch', 'customdeploy_rf_cyclecountbatch_di', false, CCarray);
			}
			else {
				var ValueMfgDate="";
				var ValueExpDate="";
				if (getMfgDate != '' || getExpDate != '' || getBestBeforeDate != '') 
				{
					var getMfgln=getMfgDate.length;
					var getExpln=getExpDate.length;
					var getBestBeforeDateln=getBestBeforeDate.length;
					nlapiLogExecution('ERROR', 'getBestBeforeDateln', getMfgln+"/"+getExpln+"/"+getBestBeforeDateln);
					if(getMfgln>6||getExpln>6||getBestBeforeDateln>6)
						errorflag="T";
				}
				nlapiLogExecution('ERROR', 'errorflag', errorflag);
				if (((getMfgDate != '' && getExpDate == '') || (getMfgDate == '' && getExpDate != '') || (getMfgDate != '' && getExpDate != '' && getBestBeforeDate != ''))&&errorflag=="F") {
					if (getMfgDate != '' && getExpDate == '')
					{
						var Mfgdate= RFDateFormat(getMfgDate);
						if(Mfgdate[0]=='true')
						{
							nlapiLogExecution('ERROR','Mfgdate',Mfgdate[1]);
							CCarray["custparam_mfgdate"]=Mfgdate[1];
							ValueMfgDate=Mfgdate[1];
						}
						else {
							errorflag='T';
							error='MfgDate:'+Mfgdate[1];
						}
					}
					if (getMfgDate == '' && getExpDate != '')
					{
						var Expdate= RFDateFormat(getExpDate);
						if(Expdate[0]=='true')
						{
							nlapiLogExecution('ERROR','Expdate',Expdate[1]);
							CCarray["custparam_expdate"]=Expdate[1];
							ValueExpDate=Expdate[1];
						}
						else {
							errorflag='T';
							error=error+'Expdate:'+Expdate[1];
						}

					}
					if (getMfgDate != '' && getExpDate != '' && getBestBeforeDate != '') {

						var Mfgdate= RFDateFormat(getMfgDate);
						if(Mfgdate[0]=='true')
						{
							nlapiLogExecution('ERROR','Mfgdate',Mfgdate[1]);
							CCarray["custparam_mfgdate"]=Mfgdate[1];
							ValueMfgDate=Mfgdate[1];
						}
						else {
							errorflag='T';
							error='MfgDate:'+Mfgdate[1];
						}

						var Expdate= RFDateFormat(getExpDate);
						if(Expdate[0]=='true')
						{
							nlapiLogExecution('ERROR','Expdate',Expdate[1]);
							CCarray["custparam_expdate"]=Expdate[1];
							ValueExpDate=Expdate[1];
						}
						else {
							errorflag='T';
							error=error+'Expdate:'+Expdate[1];
						}

						var BestBeforedate= RFDateFormat(getBestBeforeDate);
						if(BestBeforedate[0]=='true')
						{
							nlapiLogExecution('ERROR','BestBeforedate',BestBeforedate[1]);
							CCarray["custparam_bestbeforedate"]=BestBeforedate[1];
						}
						else {
							errorflag='T';
							error=error+'BestBeforeDate:'+BestBeforedate[1];
						}

					}
					nlapiLogExecution('ERROR','error',error);
					if(errorflag=='F')
					{
						//Checking Condn that MfgDate shld not be greater than or equal to ExpDate. 
						nlapiLogExecution('ERROR','ValueMfgDate',ValueMfgDate);
						nlapiLogExecution('ERROR','ValueExpDate',ValueExpDate);
						
						var d = new Date();
						var vExpiryDate='';
						if(ValueExpDate ==null || ValueExpDate =='') //if expiryDate is null 
						{
							var vExpiryDate='';
							if(itemShelflife !=null && itemShelflife!='')
							{
								d.setDate(d.getDate()+parseInt(itemShelflife));
								vExpiryDate=((d.getMonth()+1)+"/"+(d.getDate())+"/"+(d.getFullYear()));
							}
							else
							{
								vExpiryDate='01/01/2099';										     
							}
							CCarray["custparam_expdate"]=vExpiryDate;
							nlapiLogExecution('ERROR','vExpiryDate',vExpiryDate);
							ValueExpDate=vExpiryDate;
						}
						nlapiLogExecution('ERROR','ValueExpDate',ValueExpDate);
						
						if(ValueMfgDate == null || ValueMfgDate=='')
							CCarray["custparam_mfgdate"]='';
						if(CCarray["custparam_bestbeforedate"]==null || CCarray["custparam_bestbeforedate"]=='')
							CCarray["custparam_bestbeforedate"]='';

												
						if (Date.parse(ValueMfgDate) >= Date.parse(ValueExpDate)) 
						{
							CCarray["custparam_error"] = st8;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
						}
						else					
						{
							if(CaptureFifodate =='F')
							{
								//If ItemCapatureFifoDate Unchecked

								CCarray["custparam_fifodate"]=DateStamp();
								CCarray["custparam_lastdate"]='';
								CCarray["custparam_fifocode"]='';

								var rec=CreateBatchEntryRec(CCarray);																
								response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_quantity', 'customdeploy_rf_cyclecount_quantity_di', false, CCarray);

							}
							else
							{
								response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_fifo_date', 'customdeploy_rf_cyclecount_fifo_date_di', false, CCarray);
							}

						}
							//response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_fifo_date', 'customdeploy_rf_cyclecount_fifo_date_di', false, CCarray);
					}
					else
					{
						CCarray["custparam_error"] = error;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
					}
				}
				else {
					CCarray["custparam_error"] = st9;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
				}
			}
		} 
		catch (e) {
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
			nlapiLogExecution('DEBUG', 'Catch: Location not found');

		}
	}
}







/**
 * @param CCarray
 */
function CreateBatchEntryRec(CCarray)
{
	try {
		var site="";
		nlapiLogExecution('ERROR', 'in');

		nlapiLogExecution('ERROR', 'BATCH NOT FOUND');
		var customrecord = nlapiCreateRecord('customrecord_ebiznet_batch_entry');

		customrecord.setFieldValue('name', CCarray["custparam_actbatch"]);
		customrecord.setFieldValue('custrecord_ebizlotbatch', CCarray["custparam_actbatch"]);
		customrecord.setFieldValue('custrecord_ebizmfgdate', CCarray["custparam_mfgdate"]);
		customrecord.setFieldValue('custrecord_ebizbestbeforedate', CCarray["custparam_bestbeforedate"]);
		customrecord.setFieldValue('custrecord_ebizexpirydate', CCarray["custparam_expdate"]);
		customrecord.setFieldValue('custrecord_ebizfifodate', CCarray["custparam_fifodate"]);
		customrecord.setFieldValue('custrecord_ebizfifocode', CCarray["custparam_fifocode"]);
		customrecord.setFieldValue('custrecord_ebizlastavldate', CCarray["custparam_lastdate"]);
		nlapiLogExecution('ERROR', '1');
		
		//customrecord.setFieldText('custrecord_ebizsku', getPOItem);
		customrecord.setFieldValue('custrecord_ebizsku', CCarray["custparam_actitem"]);
		customrecord.setFieldValue('custrecord_ebizpackcode',CCarray["custparam_packcode"]);


		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto', CCarray["custparam_planno"]);

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_cyclesite_id');

		nlapiLogExecution('ERROR', 'Before Search Results Id', 'getSearchResultsId');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, column);
		if(searchresults)
			site=searchresults[0].getValue('custrecord_cyclesite_id');
		if(site!=null&&site!="")
			//customrecord.setFieldValue('custrecord_ebizsitebatch',site);
		var rec = nlapiSubmitRecord(customrecord, false, true);
		nlapiLogExecution('ERROR', 'rec',rec);

	} 
	catch (e) {
		nlapiLogExecution('ERROR', 'Failed to Update/Insert into BATCH ENTRY Record',e);
	}
}
