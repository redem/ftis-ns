/**
 * @param type
 * @returns {Boolean}
 */
function fnCheckReceiptType(type)
{
	var varReceiptID =  nlapiGetFieldValue('name');  
	var vComapny	= nlapiGetFieldValue('custrecord_ebizcompanyrcpt');  	 
	var vSite	= nlapiGetFieldValue('custrecord_ebizsiterecpt');  
	var vId = nlapiGetFieldValue('id');  

	if(varReceiptID != "" && varReceiptID != null && vComapny != "" && vComapny != null && vSite != "" && vSite != null)
	{
		var filters = new Array();
		
		filters[0] = new nlobjSearchFilter('name', null, 'is',varReceiptID.replace(/\s+$/, ""));//Added by suman on 06/01/12 to trim spaces at right.
//		filters[1] = new nlobjSearchFilter('custrecord_ebizcompanyrcpt', null, 'anyof',[vComapny]);	 
		filters[1] = new nlobjSearchFilter('custrecord_ebizsiterecpt', null, 'anyof',['@NONE@',vSite]);
	
		if(vId!=null && vId!="")
		{
			filters[2] = new nlobjSearchFilter('internalid', null, 'noneof',vId);	
		}
	
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_receipt_type', null, filters);
	
	
		if (searchresults != null && searchresults.length > 0) 
		{
			alert("Receipt type already exists.");
			return false;
		}
		else
		{
			return true;
		} 
	}
	else	
	{
		return true;
	}
}