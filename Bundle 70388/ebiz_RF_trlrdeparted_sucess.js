/***************************************************************************
	  		   eBizNET Solutions Inc               
 ****************************************************************************/
/* Prologue - 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Outbound/Suitelet/ebiz_RF_trlrdeparted_sucess.js,v $
 *     	   $Revision: 1.2.4.2.4.1.4.3 $
 *     	   $Date: 2014/05/30 00:41:06 $
 *     	   $Author: nneelam $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_trlrdeparted_sucess.js,v $
 * Revision 1.2.4.2.4.1.4.3  2014/05/30 00:41:06  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.2.4.2.4.1.4.2  2013/06/11 14:30:20  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.2.4.2.4.1.4.1  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.2.4.2.4.1  2012/09/25 07:13:53  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Language
 *
 * Revision 1.2.4.2  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.2.4.1  2012/02/22 13:05:46  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.2  2011/09/28 13:23:13  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 *
 *****************************************************************************/

function TrailerDepartSuccessMenu(request, response)
{
	if (request.getMethod() == 'GET') 
	{	
		
		var getLanguage = request.getParameter('custparam_language');	
	    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
	    
		var st0,st1,st2;
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "TRAILER SALI&#211; CON &#201;XITO";
			st2 = "MEN&#218;";			
	  	}
		else
		{
			st0 = "";
			st1 = "TRAILER DEPARTED SUCESSFULLY";
			st2 = "MENU";
		}    	
		var functionkeyHtml=getFunctionkeyScript('_rfload'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rfload' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> " + st1;
		html = html + "				</td>";
		html = html + "			</tr>"; 
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + " <input name='cmdMenu' type='submit' value='F8'/>";	 
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "			    <td>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else
	{
		var optedEvent = request.getParameter('cmdMenu');
		nlapiLogExecution('DEBUG', 'Into Response','Into Response');     
		var getLanguage = request.getParameter('hdngetLanguage');
		if (request.getParameter('cmdMenu') == 'F8') {            
			response.sendRedirect('SUITELET', 'customscript_rf_main_menu', 'customdeploy_rf_main_menu_di', false, optedEvent);
		}

		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}
}