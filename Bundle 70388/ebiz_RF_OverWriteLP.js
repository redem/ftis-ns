/***************************************************************************
  eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_OverWriteLP.js,v $
 *     	   $Revision: 1.3.4.5.4.5.4.8.4.1 $
 *     	   $Date: 2015/11/09 16:07:12 $
 *     	   $Author: aanchal $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_99 $
 *This Suitelet is meant to scan or enter the licence plate # for which the item is created.
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
  * REVISION HISTORY
 * $Log: ebiz_RF_OverWriteLP.js,v $
 * Revision 1.3.4.5.4.5.4.8.4.1  2015/11/09 16:07:12  aanchal
 * 2015.2 Issue fix
 * 201415276
 *
 * Revision 1.3.4.5.4.5.4.8  2014/06/23 07:30:53  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox) Issue Fixed.
 *
 * Revision 1.3.4.5.4.5.4.7  2014/06/03 15:53:14  gkalla
 * case#20148720
 * Nautilus Create inventory issue with overwrite LP
 *
 * Revision 1.3.4.5.4.5.4.6  2014/05/30 00:34:22  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.3.4.5.4.5.4.5  2014/01/16 14:32:07  nneelam
 * case#  20126819
 * Added if condition to a filter to check null vlaue
 *
 * Revision 1.3.4.5.4.5.4.4  2013/06/05 22:11:22  spendyala
 * CASE201112/CR201113/LOG201121
 * FIFO field updating with data stamp is removed.
 *
 * Revision 1.3.4.5.4.5.4.3  2013/05/02 14:24:47  schepuri
 * updating remaning cube issue fix
 *
 * Revision 1.3.4.5.4.5.4.2  2013/04/17 16:02:35  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.3.4.5.4.5.4.1  2013/03/01 14:34:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.3.4.5.4.5  2012/11/27 17:50:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Serial Capturing file.
 *
 * Revision 1.3.4.5.4.4  2012/11/09 14:14:58  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form 2012.2 branch.
 *
 * Revision 1.3.4.5.4.3  2012/11/01 14:55:42  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.3.4.5.4.2  2012/09/26 12:36:25  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.3.4.5.4.1  2012/09/21 15:01:30  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.3.4.5  2012/08/16 12:40:00  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fix as a part of 339 bundle i.e.,
 * merging the inventory with existing LP# in that location even though
 * we select option not to merging.
 *
 * Revision 1.3.4.4  2012/03/16 14:08:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.3.4.3  2012/02/21 13:25:08  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.3.4.2  2012/02/06 14:19:56  spendyala
 * CASE201112/CR201113/LOG201121
 * Added ActualEndTime and ActualEndDate.
 *
 * Revision 1.3.4.1  2012/02/01 08:08:46  spendyala
 * CASE201112/CR201113/LOG201121
 * while creating inventory record a/c ref invalid issue is been resolved by passing empty value to the field.
 *
 * Revision 1.3  2011/10/05 15:21:33  spendyala
 * CASE201112/CR201113/LOG201121
 * added  Merge Lp flag field
 *
 * Revision 1.2  2011/09/16 07:22:56  schepuri
 * CASE201112/CR201113/LOG201121
 * duplicate lps
 *
 * 
 */

function OverWriteLp(request,response){
	if(request.getMethod()=='GET'){

		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime')+ ' ' + request.getParameter('custparam_actualbegintimeampm');

		var getBinLocationId = request.getParameter('custparam_binlocationid');
		var getBinLocation = request.getParameter('custparam_binlocationname');
		var getItemId = request.getParameter('custparam_itemid');
		var getItem = request.getParameter('custparam_item');
		var getQuantity = request.getParameter('custparam_quantity');
		var getItemStatus = request.getParameter('custparam_itemstatus');
		var getItemStatusId = request.getParameter('custparam_itemstatusid');
		var getPackCode = request.getParameter('custparam_packcode');
		var getPackCodeId = request.getParameter('custparam_packcodeid');
		var getAdjustmentType = request.getParameter('custparam_adjustmenttype');
		var getAdjustmentTypeId = request.getParameter('custparam_adjustmenttypeid');
		var sitelocation = request.getParameter('custparam_sitelocation');		
		var locationId = request.getParameter('custparam_locationId');
		var getLp = request.getParameter('custparam_itemlp');
		nlapiLogExecution('ERROR', 'getLp tet',getLp);

		var getBatchId = request.getParameter('custparam_BatchId');
		var getBatchNo = request.getParameter('custparam_BatchNo');
		
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('ERROR', 'getLanguage', getLanguage);
	    
		var st0,st1,st2;
		if( getLanguage == 'es_ES')
		{
			
			st0 = "UBICACI&#211;N";
			st1 = "LP ";
			st2 = "&#191;QUIERES OVERWRITE LP";
			
		}
		else
		{
			st0 = "LOCATION";
			st1 = "LP ";
			st2 = "DO YOU WANT TO OVERWRITE LP";
			
		}		
		
		
		var functionkeyHtml=getFunctionkeyScript('_rf_sitecomp'); 
		var html = "<html><head><title>" + st0 + "</title>"; 
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_sitecomp' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" +  st1+ ": <label>" + getLp + "</label>";
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnBinLocationId' value=" + getBinLocationId + ">";
		html = html + "				<input type='hidden' name='hdnBinLocation' value=" + getBinLocation + ">";
		html = html + "				<input type='hidden' name='hdnItemId' value=" + getItemId + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getQuantity + ">";
		html = html + "				<input type='hidden' name='hdnPackCode' value=" + getPackCode + ">";
		html = html + "				<input type='hidden' name='hdnPackCodeId' value=" + getPackCodeId + ">";
		html = html + "				<input type='hidden' name='hdnItemStatus' value=" + getItemStatus + ">";
		html = html + "				<input type='hidden' name='hdnItemStatusId' value=" + getItemStatusId + ">";
		html = html + "				<input type='hidden' name='hdnAdjustmentType' value=" + getAdjustmentType + ">";
		html = html + "				<input type='hidden' name='hdnAdjustmentTypeId' value=" + getAdjustmentTypeId + ">";
		html = html + "				<input type='hidden' name='hdnsiteLocation' value=" + sitelocation + ">";
		html = html + "				<input type='hidden' name='hdnLocationInternalid' value=" + locationId+ ">";
		html = html + "				<input type='hidden' name='hdnlp' value=" + getLp + ">";
		
		html = html + "				<input type='hidden' name='hdnBatchId' value=" + getBatchId + ">";
		html = html + "				<input type='hidden' name='hdnBatchNo' value=" + getBatchNo + ">";
		html = html + "				    <input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				</td>";
		html = html + "			</tr>";           
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2;
		html = html + "				</td>";      
		html = html + "			</tr>";       
		html = html + "				<td align = 'left'><input name='cmdSend' type='submit' id='cmdSend' value='YES' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					 <input name='cmdPrevious' type='submit' value='NO'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('cmdSend').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else
	{

		var optedEvent = request.getParameter('cmdPrevious');				 
		var CIarray = new Array();
		
		var getLanguage = request.getParameter('hdngetLanguage');
		CIarray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR', 'getLanguage', CIarray["custparam_language"]);
    	
		
		var st4;
		if( getLanguage == 'es_ES')
		{
			
			st4 = "LP EST&#193; ASIGNADO AL ELEMENTO DIFERENTE";
		}
		else
		{
			
			st4 = "LP IS ALREADY ASSIGNED TO DIFFERENT ITEM";
			
		}
		
		//	Get the Inveentory data from the previous screen, which is passed as a parameter		
		var getBinLocationId = request.getParameter('hdnBinLocationId');
		var getBinLocation = request.getParameter('hdnBinLocation');
		var getItemId = request.getParameter('hdnItemId');
		var getItem = request.getParameter('hdnItem');
		var getQuantity = request.getParameter('hdnQuantity');
		var getPackCode = request.getParameter('hdnPackCode');
		var getPackCodeId = request.getParameter('hdnPackCodeId');
		var getItemStatus = request.getParameter('hdnItemStatus');
		var getItemStatusId = request.getParameter('hdnItemStatusId');
		var getAdjustmentType = request.getParameter('hdnAdjustmentType');
		var getAdjustmentTypeId = request.getParameter('hdnAdjustmentTypeId');
		var SiteLocation = request.getParameter('hdnsiteLocation');
		var locationId = request.getParameter('hdnLocationInternalid');
		var getItemLp = request.getParameter('hdnlp');

		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime')+ ' ' + request.getParameter('custparam_actualbegintimeampm');
		var getBatchId = request.getParameter('hdnBatchId');
		var getBatchNo = request.getParameter('hdnBatchNo');
		
		//Added on 06/02/12 by suman
		var getActualEndDate = DateStamp();
		var getActualEndTime = TimeStamp();
		//end of code

		var getBaseUOM = 'EACH';
		var RecordExists="N";


		CIarray["custparam_binlocationid"] = getBinLocationId;
		CIarray["custparam_binlocationname"] = getBinLocation;
		CIarray["custparam_itemid"] = getItemId;
		CIarray["custparam_item"] = getItem;
		CIarray["custparam_quantity"] = getQuantity;
		CIarray["custparam_itemstatus"] = getItemStatus;
		CIarray["custparam_itemstatusid"] = getItemStatusId;
		CIarray["custparam_packcode"] = getPackCode;
		CIarray["custparam_packcodeid"] = getPackCodeId;
		CIarray["custparam_adjustmenttype"] = getAdjustmentType;
		CIarray["custparam_adjustmenttypeid"] = getAdjustmentTypeId;

		CIarray["custparam_actualbegindate"] = request.getParameter('custparam_actualbegindate');
		CIarray["custparam_locationId"] = locationId;
		var TimeArray = new Array();
		TimeArray = getActualBeginTime.split(' ');
		CIarray["custparam_actualbegintime"] = TimeArray[0];
		CIarray["custparam_actualbegintimeampm"] = TimeArray[1];
		CIarray["custparam_itemlp"] = getItemLp.toString();
		CIarray["custparam_sitelocation"] =SiteLocation;
		CIarray["custparam_screenno"] = '17A';
		CIarray["custparam_BatchId"] =getBatchId;
		CIarray["custparam_BatchNo"]=getBatchNo;
		if(optedEvent=='NO')
		{nlapiLogExecution('ERROR', 'CIarray["custparam_itemlp"]::',CIarray["custparam_itemlp"]);
		response.sendRedirect('SUITELET', 'customscript_rf_create_invt_lp', 'customdeploy_rf_create_invt_lp_di', false, CIarray);
		}
		else
		{
			/*
			  //	Get the Inveentory data from the previous screen, which is passed as a parameter		
			        var getBinLocationId = request.getParameter('hdnBinLocationId');
			        var getBinLocation = request.getParameter('hdnBinLocation');
			        var getItemId = request.getParameter('hdnItemId');
			        var getItem = request.getParameter('hdnItem');
			        var getQuantity = request.getParameter('hdnQuantity');
			        var getPackCode = request.getParameter('hdnPackCode');
					var getPackCodeId = request.getParameter('hdnPackCodeId');
			        var getItemStatus = request.getParameter('hdnItemStatus');
					var getItemStatusId = request.getParameter('hdnItemStatusId');
			        var getAdjustmentType = request.getParameter('hdnAdjustmentType');
			        var getAdjustmentTypeId = request.getParameter('hdnAdjustmentTypeId');
			        var SiteLocation = request.getParameter('hdnsiteLocation');
			        var locationId = request.getParameter('hdnLocationInternalid');

			        var getActualBeginDate = request.getParameter('custparam_actualbegindate');
			        var getActualBeginTime = request.getParameter('custparam_actualbegintime')+ ' ' + request.getParameter('custparam_actualbegintimeampm');

			        var getActualEndDate = DateStamp();
					var getActualEndTime = TimeStamp();

			        var getBaseUOM = 'EACH';
			 */



			var filtersinv = new Array();

			filtersinv.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc',null, 'anyof',locationId));
			//filtersinv.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc',null, 'is',locationId));
			filtersinv.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc',null, 'anyof',getBinLocationId));	
			filtersinv.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku',null, 'anyof',getItemId));
			//case # 20126819 starts
			if(getItemStatusId!=null && getItemStatusId!="" &&getItemStatusId!='null')
			filtersinv.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status',null, 'anyof',getItemStatusId));
			//end
			filtersinv.push(new nlobjSearchFilter('custrecord_ebiz_inv_packcode',null, 'anyof',getPackCode));										
			filtersinv.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp',null, 'is',getItemLp));
			filtersinv.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag',null, 'anyof',[3,19]));
			if(getBatchId!=null && getBatchId!="" &&getBatchId!='null')
			filtersinv.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot',null,'anyof',getBatchId));
			
			nlapiLogExecution('ERROR', 'locationId::',locationId);
			nlapiLogExecution('ERROR', 'getBinLocationId::',getBinLocationId);
			nlapiLogExecution('ERROR', 'getItemId::',getItemId);
			nlapiLogExecution('ERROR', 'getItemStatusId::',getItemStatusId);
			nlapiLogExecution('ERROR', 'getPackCodeId::',getPackCode);
			nlapiLogExecution('ERROR', 'getLPDetails::',getItemLp);

			var SrchINVRecord = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinv);


			if (SrchINVRecord != null && SrchINVRecord.length > 0) 
			{											
				RecordExists="Y";
				//check for merge or over write
				var id=SrchINVRecord[0].getId();

				nlapiLogExecution('ERROR', 'Inventory id in OverWriteLP is ::'+ id);

				CIarray["custparam_inventoryId"] = id;
				CIarray["custparam_flag"] = 0;
				nlapiLogExecution('ERROR', 'Record Exists in Inventory::');

				response.sendRedirect('SUITELET', 'customscript_rf_inventory_merge_lp', 'customdeploy_rf_inventory_merge_lp_di', false, CIarray);
			}
			else
			{						
				nlapiLogExecution('ERROR', 'Else part of the search inventotry');						
				var filtersMergeinv = new Array();
				filtersMergeinv.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp',null, 'is',getItemLp));
				var SrchMergeINVRecord = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersMergeinv);
				if(SrchMergeINVRecord != null && SrchMergeINVRecord.length > 0)
				{							
					CIarray["custparam_error"] = st4;
					nlapiLogExecution('ERROR', 'CIarray["custparam_itemlp"]::1',CIarray["custparam_itemlp"]);
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
					nlapiLogExecution('ERROR', 'Entered Item LP', getItemLp);
				}
				else
				{
					nlapiLogExecution('ERROR', 'into create inventory of Different LP');
					CreateInventory(getItemId, getItem, getItemStatusId, getItemStatus, getPackCodeId, getPackCode, 
							getAdjustmentTypeId, getAdjustmentType, getItemLp, getBinLocation, getBinLocationId, 
							getQuantity, getActualEndDate, getActualEndTime, getActualBeginDate, getActualBeginTime, 
							SiteLocation,locationId,getBatchId);				                				                
					nlapiLogExecution('ERROR', 'Created', 'Successfully');

					var itemSubtype = nlapiLookupField('item', getItemId, ['recordType','custitem_ebizserialin']);
					CIarray["custparam_itemtype"] = itemSubtype.recordType;
					nlapiLogExecution('ERROR', 'itemSubtype.recordType =', itemSubtype);
					if (itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.custitem_ebizserialin == 'T') 
					{
						response.sendRedirect('SUITELET', 'customscript_rf_create_invt_serialscan', 'customdeploy_rf_create_invt_serialscan_d', false, CIarray);
						return;
					}
					else
						response.sendRedirect('SUITELET', 'customscript_rf_create_invt_complete', 'customdeploy_rf_create_invt_complete_di', false, CIarray);
				}
			}
		}
	}

}

function CreateInventory(ItemId, Item, ItemStatusId,ItemStatus, PackCodeId, PackCode, AdjustmentTypeId, AdjustmentType, ItemLP, BinLocation, BinLocationId, Quantity, ActualEndDate, ActualEndTime, ActualBeginDate, ActualBeginTime, SiteLocation,locationId,lot)
{
	    //create a inventory record
        var CreateInventoryRecord = nlapiCreateRecord('customrecord_ebiznet_createinv');
        nlapiLogExecution('ERROR', 'Create Inventory Record', 'Inventory Record');
		
	    CreateInventoryRecord.setFieldValue('custrecordact_begin_date', ActualBeginDate);
        CreateInventoryRecord.setFieldValue('custrecord_act_end_date', ActualEndDate);
        //Adding fields to update time zones.
        CreateInventoryRecord.setFieldValue('custrecord_actualbegintime', ActualBeginTime);
        CreateInventoryRecord.setFieldValue('custrecord_actualendtime', ActualEndTime);
        CreateInventoryRecord.setFieldValue('custrecord_recordtime', TimeStamp());
        CreateInventoryRecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
        CreateInventoryRecord.setFieldValue('custrecord_current_date', DateStamp());
        CreateInventoryRecord.setFieldValue('custrecord_upd_date', DateStamp());

        //Status flag .
        //        customrecord.setFieldValue('custrecord_status_flag', 'S');
        CreateInventoryRecord.setFieldValue('custrecord_status_flag', 3);
        CreateInventoryRecord.setFieldValue('custrecord_wms_inv_status_flag', 19);
    	CreateInventoryRecord.setFieldValue('custrecord_ebiz_qoh', parseFloat(Quantity).toFixed(4));
    	CreateInventoryRecord.setFieldValue('custrecord_invttasktype', 10);//INVT);
        //Added for Item name and desc
        CreateInventoryRecord.setFieldValue('custrecord_sku', Item);
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_sku', ItemId);
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(Quantity).toFixed(4));
//        CreateInventoryRecord.setFieldValue('custrecord_skudesc', ItemDesc);
        CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_sku_status', ItemStatusId);
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_packcode', 1);//PackCodeId);
		
		
		/*
		var filtersAccNo = new Array();
        filtersAccNo[0] = new nlobjSearchFilter('custrecord_location', null, 'is', SiteLocation);
        var colsAcc = new Array();
        colsAcc[0]=new nlobjSearchColumn('custrecord_accountno');                
        var Accsearchresults = nlapiSearchRecord('customrecord_inventoryaccountno', null, filtersAccNo, colsAcc);              
        var varAccountNo = Accsearchresults[0].getValue('custrecord_accountno');                                
        nlapiLogExecution('ERROR', 'Account ',varAccountNo);
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_account_no', varAccountNo);//Account #);
		*/
		
		//code added on 01/02/12 by suman.
		//without passing this value system throughing an error stating that a/c no ref is invalid .
		//So to overcome that bug v are passing empty value.
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_account_no', '');
		//end of code added on 01/02/12
		
		//Added on 20Feb by suman
		//checking the condition whether lot is null or not,If it is not null then allow to pass the value to the field.
		if(lot!=null&&lot!="")
		{
			CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_lot', lot);	
			var rec=nlapiLookupField('customrecord_ebiznet_batch_entry',lot,['custrecord_ebizfifodate','custrecord_ebizexpirydate']);
			var fifodate=rec.custrecord_ebizfifodate;
			var expdate=rec.custrecord_ebizexpirydate;
			if(fifodate!=null && fifodate!='')
				CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_fifo',fifodate);
			/*else
				CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_fifo',DateStamp());*/
			CreateInventoryRecord.setFieldValue('custrecord_ebiz_expdate', expdate);
		}
		/*else
		{
			CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_fifo',DateStamp());
		}*/
		//

		
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_callinv', 'Y'); //Added by ramana for invoking NSInventory;
		
		nlapiLogExecution('ERROR', 'Location id fetched are', locationId);
		
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_loc',locationId);
				
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_lp', ItemLP);//Item LP #);
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_binloc', BinLocationId);//Bin Location);
		
		//Added by suman as on 090812
		//updating Adjustment type and Company while creating invt record.
		nlapiLogExecution('ERROR','AdjustmentTypeId',AdjustmentTypeId+'/'+AdjustmentType);
		if(AdjustmentTypeId!=null&&AdjustmentTypeId!="")
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_adjusttype', AdjustmentTypeId);
		
		var compId=getCompanyId();
		if(compId!=null&&compId!="")
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_company', compId);
		
		//end of code as of 090812.
		
		

		//code added on 130812 by suman.
		//Code to update user id and recorded time.
		var currentContext = nlapiGetContext();
		var currentUserID = currentContext.getUser();
		CreateInventoryRecord.setFieldValue('custrecord_updated_user_no',currentUserID);
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_updatedtime',TimeStamp());
		//end of code as of 130812.
		
        nlapiLogExecution('ERROR', 'Submitting Create Inventory Record', 'Inventory Record');
        
        //commit the record to NetSuite
        var CreateInventoryRecordId = nlapiSubmitRecord(CreateInventoryRecord, false, true);
		
        nlapiLogExecution('ERROR', 'Inventory Creation', 'Success');
        
        try 
		{
			nlapiLogExecution('ERROR', 'INTO MASTER LP INSERTION');
			var filtersmlp = new Array();
			filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', ItemLP);

			var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

			if (SrchRecord != null && SrchRecord.length > 0) 
			{
				nlapiLogExecution('ERROR', 'LP FOUND');			
			}
			else 
			{
				nlapiLogExecution('ERROR', 'LP NOT FOUND');
				var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
				customrecord.setFieldValue('name', ItemLP);
				customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', ItemLP);
				var rec = nlapiSubmitRecord(customrecord, false, true);
				nlapiLogExecution('ERROR', 'Master LP Insertion', 'Success');
			}
		} 
		catch (e) 
		{
			nlapiLogExecution('ERROR', 'Failed to Update/Insert into Master LP Record');
		}
		
		/*var arrDims = getSKUCubeAndWeight(ItemId, 1);
		var itemCube = 0;
		if (arrDims["BaseUOMItemCube"] != "" && (!isNaN(arrDims["BaseUOMItemCube"]))) 
		{
			var uomqty = ((parseFloat(Quantity))/(parseFloat(arrDims["BaseUOMQty"])));			
			itemCube = (parseFloat(uomqty) * parseFloat(arrDims["BaseUOMItemCube"]));
			nlapiLogExecution('ERROR', 'itemCube', itemCube);
		} 
		
		var vOldRemainingCube = GeteLocCube(BinLocationId);
		nlapiLogExecution('ERROR', 'vOldRemainingCube', vOldRemainingCube);
		
		var vTotalCubeValue = parseFloat(itemCube) - parseFloat(vOldRemainingCube);
		nlapiLogExecution('ERROR', 'vOldLocationRemainingCube', vTotalCubeValue);
		
		var retValue =  UpdateLocCube(BinLocationId,vTotalCubeValue);
		nlapiLogExecution('ERROR', 'Loc Cube Updation', 'Success');*/
}


function getCompanyId()
{
	try{
	var CompName;	
	var CompanyFilters = new Array();
	//CompanyFilters.push(new nlobjSearchFilter('internalid',null,'is',1));
	CompanyFilters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
	var CompanyColumn = new Array();	
	CompanyColumn.push(new nlobjSearchColumn('custrecord_company'));		
	var CompanyResults = nlapiSearchRecord('customrecord_ebiznet_company', null, CompanyFilters,CompanyColumn);	    
	if(CompanyResults!=null)
	{
		CompName=CompanyResults[0].getId();
	}
	return CompName;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in getCompanyId',exp);
		
	}
}