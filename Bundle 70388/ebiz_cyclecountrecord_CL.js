/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inventory/Client/Attic/ebiz_cyclecountrecord_CL.js,v $
 *     	   $Revision: 1.1.2.3.4.1 $
 *     	   $Date: 2012/11/01 14:54:56 $
 *     	   $Author: schepuri $
 *     	   $Name: t_NSWMS_2013_1_2_7 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_cyclecountrecord_CL.js,v $
 * Revision 1.1.2.3.4.1  2012/11/01 14:54:56  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.1.2.3  2012/07/02 06:37:28  schepuri
 * no message
 *
 * Revision 1.1.2.2  2012/07/02 06:35:32  schepuri
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.1.2.1  2012/07/02 06:13:06  schepuri
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.12.2.6  2012/04/25 06:54:57  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Blind Cycle Count
 *
 *
 *****************************************************************************/

function ebiz_cyclecountrecord_CL(fld)
{
	//alert('hello');
	var vactQty =  nlapiGetCurrentLineItemValue('custpage_items','custpage_actqty');
	var PlanNo =  nlapiGetCurrentLineItemValue('custpage_items','custpage_planno');
	//alert('hello'+PlanNo);
	var invrecid='';
	var filters = new Array();
	
	filters.push(new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto',PlanNo));
	filters.push(new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [20]));
	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_invtid');     
	var List = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, columns);
	//alert('hello1');
	if( List!=null && List!='')
	{ 
		invrecid = List.getValue('custrecord_invtid');
		//alert(invrecid);
	}
	var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv',invrecid);
	
	var allocQty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
	
	if(parseFloat(allocQty)>parseFloat(vactQty))
	{
		alert("AllocQty is greater than ActualQty");
		return false;    		
	}  
	
}
function Onchange(type,name ,linenum)
{
	//if(name=='custpage_actqty')
	//{
		var vactQty =  nlapiGetCurrentLineItemValue('custpage_items','custpage_actqty');
		var PlanNo =  nlapiGetCurrentLineItemValue('custpage_items','custpage_planno');
			
		
		var invrecid='';
		var filters = new Array();
		
		filters.push(new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto',PlanNo));
		filters.push(new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [20]));
		
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_invtid');     
		var List = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, columns);
		
		if( List!=null && List!='')
		{ 
			invrecid = List[0].getValue('custrecord_invtid');
			//alert(invrecid);
		}
				
		var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv',invrecid);
		
		var allocQty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
		
		if(parseFloat(allocQty)>parseFloat(vactQty))
		{
			alert("AllocQty is greater than ActualQty");
			return false;    		
		}  
//	}
	

}


