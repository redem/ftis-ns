/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_BulkPicking_ContainerNo.js,v $
 *     	   $Revision: 1.1.2.19 $
 *     	   $Date: 2015/06/09 10:07:21 $
 *     	   $Author: snimmakayala $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * 
 * PRAMETERS
 *
 * DESCRIPTION
 * 
 * REVISION HISTORY
 * $Log: ebiz_RF_BulkPicking_ContainerNo.js,v $
 * Revision 1.1.2.19  2015/06/09 10:07:21  snimmakayala
 * 201412975
 *
 * Revision 1.1.2.18  2015/05/20 14:27:53  schepuri
 * case# 201412830
 *
 * Revision 1.1.2.17  2015/04/17 15:25:04  grao
 * SB issue fixes  201412075
 *
 * Revision 1.1.2.16  2015/04/13 09:25:59  rrpulicherla
 * Case#201412277
 *
 * Revision 1.1.2.15  2015/01/27 13:49:22  schepuri
 * issue # 201411355
 *
 * Revision 1.1.2.14  2014/08/11 07:20:48  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20149801
 *
 * Revision 1.1.2.13  2014/08/11 07:05:59  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20149829
 *
 * Revision 1.1.2.12  2014/07/21 15:39:10  skavuri
 * Case # 20148688 SB Issue Fixed
 *
 * Revision 1.1.2.11  2014/06/18 13:53:31  grao
 * Case#: 20148929 New GUI account issue fixes
 *
 * Revision 1.1.2.10  2014/06/13 13:31:56  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.9  2014/06/06 07:16:35  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.1.2.8  2014/05/30 00:41:00  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.7  2014/05/19 15:17:19  sponnaganti
 * case# 20148440
 * Standard Bundle Issue Fix
 *
 * Revision 1.1.2.6  2014/05/01 14:11:37  sponnaganti
 * case # 20148200
 * (stage record creation)
 *
 * Revision 1.1.2.5  2014/04/17 15:45:54  nneelam
 * case#  20148057
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.4  2014/04/16 15:33:41  nneelam
 * case#  20146553
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.3  2014/03/10 16:24:02  skavuri
 * Case# 20127528 issue fixed
 *
 * Revision 1.1.2.2  2013/08/23 06:11:38  snimmakayala
 * Case# 20124031
 * NLS - UAT ISSUES
 * Weight is not getting updated properly in LP master for SHIPASIS cartons.
 *
 * Revision 1.1.2.1  2013/07/19 08:18:22  gkalla
 * Case# 20123527
 * Bulk picking CR for Nautilus
 *
 * Revision 1.23.2.35.4.19.2.21  2013/06/26 15:03:30  snimmakayala
 * Case# : 20123197
 * PMM :: Issue Fixes
 * 
 *****************************************************************************/


function PickingContainerNo(request, response){
	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();
	if (request.getMethod() == 'GET') {
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('Debug', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7;

		if( getLanguage == 'es_ES')
		{
			//st1 = "CART&#211;N #:";
			st1 = "N&#218;MERO DE EMPAQUE: ";
			st2 = "TAMA&#209;O DEL EMPAQUE:";
			//st3 = "ENTER / SCAN CART&#211;N #";
			st3 = "INGRESAR / ESCANEAR DEL N&#218;MERO DE EMPAQUE";
			st4 = "INTRODUZCA EL TAMA&#209;O";
			st5 = "CONTINUAR";
			st6 = "ANTERIOR";
			st7 = "ETAPA Y CONTINUAR";
		}
		else
		{
			st1 = "CARTON NO : ";
			st2 = "CARTON SIZE : ";
			st3 = "ENTER/SCAN CARTON NO ";
			st4 = "ENTER SIZE ";
			st5 = "CONTINUE";
			st6 = "PREV";
			st7 = "STAGE AND CONTINUE";


		}
		var getWaveno = request.getParameter('custparam_waveno');

		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocationId = request.getParameter('custparam_beginLocation');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getItemName = request.getParameter('custparam_itemname');

		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');        
		var getEnteredLocation = request.getParameter('custparam_endlocation');
		var getBeginBinLocation = request.getParameter('custparam_beginlocationname');

		var getSerialNo = request.getParameter('custparam_serialno');

		var whLocation = request.getParameter('custparam_whlocation');
		var getZoneNo = request.getParameter('custparam_zoneno');
		//var getZoneId = request.getParameter('custparam_zoneid');
		var getZoneId=request.getParameter('custparam_ebizzoneno');
		nlapiLogExecution('Debug', 'getZoneId', getZoneId);
		nlapiLogExecution('Debug', 'getZoneNo', getZoneNo);
		var getItem = '';

		nlapiLogExecution('Debug', 'getItemName', getItemName);
		if(getItemName==null || getItemName=='')
		{
			var Itype = nlapiLookupField('item', getItemInternalId, 'recordType');
			var ItemRec = nlapiLoadRecord(Itype, getItemInternalId);
			getItem = ItemRec.getFieldValue('itemid');
		}
		else
		{
			getItem=getItemName;
		}

		var itemType=request.getParameter('custparam_itemType');
		var ActBatchno = request.getParameter('custparam_Actbatchno');
		var ExpBatchno=request.getParameter('custparam_Expbatchno');

		var EntLoc = request.getParameter('custparam_EntLoc');
		var EntLocRec=request.getParameter('custparam_EntLocRec');
		var EntLocID=request.getParameter('custparam_EntLocId');
		var ExceptionFlag = request.getParameter('custparam_Exc');

		nlapiLogExecution('Debug', 'EntLoc', EntLoc);
		nlapiLogExecution('Debug', 'EntLocRec', EntLocRec);
		nlapiLogExecution('Debug', 'EntLocID', EntLocID);
		nlapiLogExecution('Debug', 'ExceptionFlag', ExceptionFlag);

		/***upto here***/
		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');
		//end of code as of 15Feb.

		var getPickQty=0;
		getPickQty=request.getParameter('custparam_enteredqty');
		if(getPickQty == "" || getPickQty == null)
		{
			getPickQty=request.getParameter('custparam_expectedquantity');
		}


		var getLPContainerSize= '';

		var str = 'EntLoc. = ' + EntLoc + '<br>';
		str = str + 'EntLocRec. = ' + EntLocRec + '<br>';	
		str = str + 'EntLocID. = ' + EntLocID + '<br>';	
		str = str + 'ExceptionFlag. = ' + ExceptionFlag + '<br>';	

		str = str + 'getItem. = ' + getItem + '<br>';	

		str = str + 'getPickQty. = ' + getPickQty + '<br>';	
		str = str + 'getExpectedQuantity. = ' + getExpectedQuantity + '<br>';	

		str = str + 'getEnteredLocation. = ' + getEnteredLocation + '<br>';	

		nlapiLogExecution('Debug', 'Parameter Values', str);


		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no');
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;alert('please donot refresh the page, as system is in processing..');return false;}}"; 
		html = html + "else {if (keycode == 116){alert('please donot refresh the page, as system is in processing..');return false;}}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		/*html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";*/
		//html = html + " document.getElementById('entercontainerno').focus();";   

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 

		//html = html + "	  alert(evt.keyCode);";
		//html = html + "	  alert(node.type);";

		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";

		//html = html + "	  alert(document.getElementById('cmdSend').disabled);";

		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>WAVE# :<label>" + getWaveno + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";	
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>ZONE# :<label>" + getZoneNo + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";

		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationId' value='" + getBeginLocationId + "'>";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemName' value='" + getItemName + "'>";
		html = html + "				<input type='hidden' name='hdnItemDescription' value='" + getItemDescription + "'>";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";


		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEnteredLocation' value='" + getEnteredLocation + "'>";
		html = html + "				<input type='hidden' name='hdnBeginBinLocation' value='" + getBeginBinLocation + "'>";

		html = html + "				<input type='hidden' name='hdnserialno' value=" + getSerialNo + ">";

		html = html + "				<input type='hidden' name='hdnbatchno' value=" + vBatchno + ">";		
		html = html + "				<input type='hidden' name='hdnActQty' value=" + getPickQty + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";

		//code added on 15Feb 2012
		html = html + "				<input type='hidden' name='hdnEntLoc' value=" + EntLoc + ">";
		html = html + "				<input type='hidden' name='hdnEntLocRec' value=" + EntLocRec + ">";
		html = html + "				<input type='hidden' name='hdnEntLocID' value=" + EntLocID + ">";
		html = html + "				<input type='hidden' name='hdnExceptionFlag' value=" + ExceptionFlag + ">";
		html = html + "				<input type='hidden' name='hdnitemtype' value=" + itemType + ">";
		html = html + "				<input type='hidden' name='hdnactlot' value=" + ActBatchno + ">";
		html = html + "				<input type='hidden' name='hdnexplot' value=" + ExpBatchno + ">";

		//End of code.
		html = html + "				<input type='hidden' name='hdnebizzoneno' value=" + getZoneId + ">";
		html = html + "				<input type='hidden' name='hdnzoneno' value='" + getZoneNo + "'>";// Case# 20127528 getZoneNo must be pass as string for url


		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";		 
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st3;//ENTER/SCAN CARTON NO ;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entercontainerno' id='entercontainerno' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st4;//ENTER SIZE 
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entersize' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		/*html = html + "				<td align = 'left'>"+ st5 +" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					"+ st6 +" <input name='cmdPrevious' type='submit' value='F7'/>";*/
		html = html + "				<td align = 'left'>"+ st5 +" <input name='cmdCont' type='submit' value='F8'/><br>";
		html = html + "				"+ st7 +" <input name='cmdStage' type='submit' value='F9' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/><br>";
		html = html + "					"+ st6 +" <input name='cmdPrevious' type='submit' value='F7'/>";
//		html = html + "					SUSPEND <input name='cmdSuspend' type='submit' value='F10'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('entercontainerno').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('Debug', 'Into Response', 'Into Response');

		var getLanguage = request.getParameter('hdngetLanguage');

		var st7;

		if( getLanguage == 'es_ES')
		{
			st7 = "CAJA NO V&#193;LIDO #";
			st8 = "TAMA&#209;O V&#193;LIDO #";
		}
		else
		{
			st7 = "INVALID CARTON #";
			st8 = "INVALID SIZE";

		}
		var vCont= request.getParameter('cmdCont');
		var vStage= request.getParameter('cmdStage');

		nlapiLogExecution('Debug', 'vCont', vCont);
		nlapiLogExecution('Debug', 'vStage', vStage);

		nlapiLogExecution('Debug', 'Time Stamp at the start of response',TimeStampinSec());
		var vRemaningqty=0,vDisplayedQty=0,vActqty=0;
		var vZoneId=request.getParameter('hdnebizzoneno');
		vActqty = request.getParameter('hdnActQty');

		var TotalWeight=0;
		var getEnteredContainerNo = request.getParameter('entercontainerno');


		getContainerSize = request.getParameter('entersize');

		var getWaveNo = request.getParameter('hdnWaveNo');

		var BeginLocation = request.getParameter('hdnBeginLocationId');
		var vbatchno = request.getParameter('hdnbatchno');		
		var ItemNo = request.getParameter('hdnItemInternalId');
		var ItemDesc = request.getParameter('hdnItemDescription');
		var Item = request.getParameter('hdnItem');
		var ItemName = request.getParameter('hdnItemName');
		var PickQty = request.getParameter('hdnExpectedQuantity');

		var EndLocation = request.getParameter('hdnEndLocInternalId');

		var BeginLocationName = request.getParameter('hdnBeginBinLocation');
		var EndLocInternalId = request.getParameter('hdnEndLocInternalId');

		var SerialNo = request.getParameter('hdnserialno');

		var vBatchno = request.getParameter('hdnbatchno');	


		var EntLoc = request.getParameter('hdnEntLoc');
		var EntLocRec = request.getParameter('hdnEntLocRec');
		var EntLocID = request.getParameter('hdnEntLocID');
		var ExceptionFlag = request.getParameter('hdnExceptionFlag');
		if(ExceptionFlag=='L')
			if(EntLocID!=null&&EntLocID!="")
				EndLocation=EntLocID;
		//End of code.

		var vCarrier;
		var vSite;
		var vCompany;
		var stgDirection="OUB";

		vSite = request.getParameter('hdnwhlocation');

		var str = 'vActqty. = ' + request.getParameter('hdnActQty') + '<br>';	
		str = str + 'getEnteredContainerNo. = ' + request.getParameter('entercontainerno') + '<br>';	

		str = str + 'getContainerSize. = ' + getContainerSize + '<br>';	

		str = str + 'ItemNo. = ' + ItemNo + '<br>';	

		str = str + 'getWaveNo. = ' + getWaveNo + '<br>';	

		str = str + 'ItemName. = ' + ItemName + '<br>';	
		str = str + 'EndLocation. = ' + EndLocation + '<br>';	
		str = str + 'BeginLocationName. = ' + BeginLocationName + '<br>';	

		nlapiLogExecution('Debug', 'Parameter Values in Response', str);

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		// This variable is to hold the SO# entered.
		var SOarray = new Array();
		SOarray["custparam_language"] = getLanguage;
		SOarray["custparam_error"] = st7;//'INVALID CARTON #';
		SOarray["custparam_screenno"] = 'BULK05';

		SOarray["custparam_whlocation"] = request.getParameter('hdnwhlocation');

		var vSkipId=request.getParameter('hdnskipid');
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');
		vSkipId=0;
		vSite = request.getParameter('hdnwhlocation');
		nlapiLogExecution('Debug', 'vSite', vSite);

		SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');

		SOarray["custparam_expectedquantity"] = request.getParameter('hdnExpectedQuantity');
		SOarray["custparam_beginLocation"] = request.getParameter('hdnBeginLocationId');
		SOarray["custparam_item"] = request.getParameter('hdnItem');
		SOarray["custparam_itemname"] = request.getParameter('hdnItemName');
		SOarray["custparam_itemdescription"] = request.getParameter('hdnItemDescription');
		SOarray["custparam_iteminternalid"] = request.getParameter('hdnItemInternalId');

		SOarray["custparam_beginlocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');

		SOarray["custparam_serialno"] = request.getParameter('hdnserialno');

		SOarray["custparam_batchno"] = vbatchno;


		SOarray["custparam_newcontainerlp"] = getEnteredContainerNo;

		var ItemType=request.getParameter('hdnitemtype');
		SOarray["custparam_itemType"] = ItemType;
		SOarray["custparam_Actbatchno"]=request.getParameter('hdnactlot');
		SOarray["custparam_Expbatchno"] = request.getParameter('hdnexplot');


		var itemSubtype = nlapiLookupField('item', ItemNo, ['recordType', 'custitem_ebiz_captureattribute']);
		var itemattribute = itemSubtype.custitem_ebiz_captureattribute;

		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  request.getParameter('hdnebizzoneno');
			SOarray["custparam_zoneno"] =  request.getParameter('hdnzoneno');

		}
		else
		{
			SOarray["custparam_ebizzoneno"] = '';
			SOarray["custparam_zoneno"] ='';
		}
		nlapiLogExecution('Debug', 'NextLocation', NextShowLocation);
		if (sessionobj!=context.getUser()) 
		{
			try
			{
				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}

				//nlapiLogExecution('Debug','Order Line Number',SORec.getFieldValue('custrecord_line_no'));

				//	if the previous button 'F7' is clicked, it has to go to the previous screen 
				//  ie., it has to go to accept SO #.
				if (optedEvent == 'F7') 
				{
					//response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, SOarray);
					response.sendRedirect('SUITELET', 'customscript_rf_bulkpick_confirm', 'customdeploy_rf_bulkpick_confirm_di', false, SOarray);
				}
				else 
				{
					if (getEnteredContainerNo != '' && getEnteredContainerNo != '')
					{
						SOarray["custparam_newcontainerlp"] = getEnteredContainerNo;

						nlapiLogExecution('Debug', 'getEnteredContainerNo', 'userdefined');
						nlapiLogExecution('Debug', 'Time Stamp at the start of fnValidateEnteredContLP',TimeStampinSec());
						var vOpenTaskRecs=fnValidateEnteredContLP(getEnteredContainerNo,getWaveNo,vZoneId);
						nlapiLogExecution('Debug', 'Time Stamp at the end of fnValidateEnteredContLP',TimeStampinSec());
						if(vOpenTaskRecs != null && vOpenTaskRecs != '')
						{
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							nlapiLogExecution('Debug', 'Error: ', 'Container# is invalid');
						}
						else
						{	
							//	case# 201411355
//							case# 201412830
							if(getContainerSize!="" && getContainerSize!=null && getContainerSize!="-")
							{
								var validContainerSize = getAllContainers(getContainerSize,vSite);
								if(validContainerSize == null || validContainerSize == '')
								{
									SOarray["custparam_error"] = st8;
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
									nlapiLogExecution('Debug', 'Error: ', 'Containersize is invalid');
									return;
								}
							}
							var getEnteredContainerNoPrefix = getEnteredContainerNo.substring(0, 3).toUpperCase();
							var isExistfnValidate=isExitfnValidateEnteredContLP(getEnteredContainerNo,getWaveNo,vZoneId);

							var LPReturnValue;

							if(isExistfnValidate != null && isExistfnValidate != '')
							{
								LPReturnValue=true;
							}
							else
								LPReturnValue = ebiznet_LPRange_CL_withLPType(getEnteredContainerNo, '2','2',vSite);//'2'UserDefiend,'2'PICK
							//var LPReturnValue = ebiznet_LPRange_CL_withLPType(getEnteredContainerNo.replace(/\s+$/,""), '2','2',vSite);//'2'UserDefiend,'2'PICK

							nlapiLogExecution('Debug', 'Time Stamp at the end of ebiznet_LPRange_CL_withLPType',TimeStampinSec());
							nlapiLogExecution('Debug', 'LPReturnValue',LPReturnValue);
							if(LPReturnValue == true){
								nlapiLogExecution('Debug', 'getItem', ItemNo);
								var arrDims = getSKUCubeAndWeightforconfirm(ItemNo, 1);
								nlapiLogExecution('Debug', 'Time Stamp at the end of getSKUCubeAndWeightforconfirm',TimeStampinSec());
								var itemCube = 0;
								var itemWeight=0;						
								if (arrDims[0] != "" && (!isNaN(arrDims[1]))){
									itemCube = (parseFloat(PickQty) * parseFloat(arrDims[0]));
									itemWeight = (parseFloat(PickQty) * parseFloat(arrDims[1]));//									
								} 

								var ContainerCube;					
								var containerInternalId;
								var ContainerSize;
								if(getContainerSize=="" || getContainerSize==null || getContainerSize=='SHIPASIS')
								{
									getContainerSize=ContainerSize;
									nlapiLogExecution('Debug', 'ContainerSize', ContainerSize);	
									ContainerCube = itemCube;
									TotalWeight=itemWeight;
								}	
								else
								{
									var arrContainerDetails = getContainerCubeAndTarWeight("",getContainerSize);
									nlapiLogExecution('Debug', 'Time Stamp at the end of getContainerCubeAndTarWeight',TimeStampinSec());
									if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) {

										ContainerCube =  parseFloat(arrContainerDetails[0]);						
										containerInternalId = arrContainerDetails[3];
										ContainerSize=arrContainerDetails[4];
										TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));// added on 28 nov
									} 
								}
								nlapiLogExecution('Debug', 'ContainerSizeInternalId', containerInternalId);				
								//Insert LP Record
								var ResultText='';
								CreateRFMasterLPRecord(getEnteredContainerNo,containerInternalId,ContainerCube,TotalWeight,getContainerSize,ResultText);
								nlapiLogExecution('Debug', 'Time Stamp at the end of CreateRFMasterLPRecord',TimeStampinSec());
								//added by suman as on 05/08/12.
								//update all the open picks with the new container LP#.
								//UpdateAllContainerLp(getEnteredContainerNo,getFetchedContainerNo,SalesOrderInternalId,OrdName,getWaveNo,vZoneId,vPickType);
								//end of the code as of 05/08/12.
								//nlapiLogExecution('Debug', 'Time Stamp at the end of UpdateAllContainerLp',TimeStampinSec());
								//UpdateRFFulfillOrdLine(vdono,vActqty);
								//nlapiLogExecution('Debug', 'Time Stamp at the end of UpdateRFFulfillOrdLine',TimeStampinSec());
								//UpdateOpenTask
								/*UpdateRFOpenTask(vActqty, containerInternalId,getEnteredContainerNo,itemCube,itemWeight,EndLocation,
								PickQty,ExceptionFlag,EntLocRec,ItemNo,SerialNo,getWaveNo,vZoneId);*/

								nlapiLogExecution('Debug', 'Time Stamp at the end of UpdateRFOpenTask',TimeStampinSec());
								//Below code is merged from Lexjet production and its lexjet specific, so commented theis code by Ganesh on 5th Mar 2013***/
								//UpdateNewContainelpinShipmanifest(getEnteredContainerNo,getFetchedContainerNo,OrdName);
								/***upto here***/						
								nlapiLogExecution('Debug', 'Time Stamp at the end of updatelastpicktaskforoldcarton',TimeStampinSec());


								var opentaskcount=0;

								//opentaskcount=getOpenTasksCount(getWaveNo,getFetchedContainerNo,vPickType,SalesOrderInternalId,vdono,vZoneId);
								nlapiLogExecution('Debug', 'Time Stamp at the start of getOpenTasksCount',TimeStampinSec());

								//opentaskcount=getOpenTasksCount(getWaveNo,getEnteredContainerNo);

								nlapiLogExecution('Debug', 'opentaskcount', opentaskcount);
								//For fine tuning pick confirmation and doing autopacking in user event after last item
								var IsitLastPick='F';
								if(opentaskcount > parseFloat(vSkipId) + 1)
									IsitLastPick='F';
								else
									IsitLastPick='T';

								//For qty exception
								//nlapiLogExecution('Debug', 'RcId', RcId);
								var InvQOH=0;

								if(request.getParameter('entercontainerno')==''||request.getParameter('entercontainerno')==null)
								{
									var cartonrequiredflag = getSystemRule('RF Picking Carton # Scan Required?');
									nlapiLogExecution('Debug', 'cartonrequiredflag', cartonrequiredflag);

									if(cartonrequiredflag=='Y')
									{
										SOarray["custparam_error"] = 'PLEASE ENTER/SCAN CARTON#';
										response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
										return;
									}
								}


								SOarray["custparam_newcontainerlp"] = getEnteredContainerNo;

								nlapiLogExecution('Debug', 'getEnteredContainerNo = getFetchedContainerNo');

								nlapiLogExecution('Debug', 'Time Stamp at the start of getSKUCubeAndWeightforconfirm',TimeStampinSec());
								var arrDims = getSKUCubeAndWeightforconfirm(ItemNo, 1);
								nlapiLogExecution('Debug', 'Time Stamp at the end of getSKUCubeAndWeightforconfirm',TimeStampinSec());
								var itemCube = 0;
								var itemWeight=0;						

								if (arrDims[0] != "" && (!isNaN(arrDims[1]))){
									//	these are commented because here  PickQty is excepted qty but we have to cal based on act qty
									itemCube = (parseFloat(PickQty) * parseFloat(arrDims[0]));
									itemWeight = (parseFloat(PickQty) * parseFloat(arrDims[1]));//	
								}

								var ContainerCube;					
								var containerInternalId;

								nlapiLogExecution('Debug', 'getContainerSize', getContainerSize);	

								nlapiLogExecution('Debug', 'Time Stamp at the start of getContainerCubeAndTarWeight',TimeStampinSec());
								var arrContainerDetails = getContainerCubeAndTarWeight("",getContainerSize);	
								nlapiLogExecution('Debug', 'Time Stamp at the end of getContainerCubeAndTarWeight',TimeStampinSec());
								if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) {

									ContainerCube =  parseFloat(arrContainerDetails[0]);						
									containerInternalId = arrContainerDetails[3];
									ContainerSize=arrContainerDetails[4];
									TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));//now added
								} 
								nlapiLogExecution('Debug', 'ContainerSizeInternalId', containerInternalId);	
								nlapiLogExecution('Debug', 'ContainerSize', ContainerSize);	

								//UpdateOpenTask,fulfillmentorder

								nlapiLogExecution('Debug', 'Time Stamp at the start of UpdateRFFulfillOrdLine',TimeStampinSec());

								UpdateRFOpenTask(vActqty, containerInternalId,getEnteredContainerNo,itemCube,itemWeight,EndLocation,
										PickQty,ExceptionFlag,EntLocRec,ItemNo,SerialNo,getWaveNo,vZoneId,BeginLocation,request);
								//UpdateRFFulfillOrdLine(vdono,vActqty);
								nlapiLogExecution('Debug', 'Time Stamp at the end of UpdateRFFulfillOrdLine',TimeStampinSec());

								//updatelastpicktaskforoldcarton(getEnteredContainerNo,getWaveNo,vZoneId);

								//create new record in opentask,fullfillordline when we have qty exception

								/*vRemaningqty = PickQty-vActqty;
				var recordcount=1;//it is iterator in GUI
				nlapiLogExecution('Debug', 'vDisplayedQty', PickQty);	
				nlapiLogExecution('Debug', 'vActqty', vActqty);	
				if(PickQty != vActqty)
				{
					nlapiLogExecution('Debug', 'inexception condition1', 'inexception condition');	
					CreateNewShortPick(RcId,PickQty,vActqty,vdono,recordcount,getEnteredContainerNo,TotalWeight,vbatchno,vRemaningqty);//have to check for wt and cube calculation
				}*/

								nlapiLogExecution('Debug', 'opentaskcount', opentaskcount);
								nlapiLogExecution('Debug', 'BeginLocation', BeginLocation);

								nlapiLogExecution('Debug', 'ItemNo', ItemNo);
								if(vCont=='F8')
								{
									var vOpenTaskRecs=GetOrdDetails(getWaveNo,vZoneId);

									if(vOpenTaskRecs != null && vOpenTaskRecs != '' &&  vOpenTaskRecs.length >  0)
									{
										var NextShowLocation=vOpenTaskRecs[0].getValue('custrecord_actbeginloc',null,'group');
										var NextShowItem=vOpenTaskRecs[0].getValue('custrecord_sku',null,'group');
										var SOSearchResult=vOpenTaskRecs[0];
										SOarray["custparam_waveno"] = SOSearchResult.getValue('custrecord_ebiz_wave_no',null,'group');
										SOarray["custparam_zoneid"] = vZoneId;
										//	SOarray["custparam_zoneno"] = getZoneNo;
										//	SOarray["custparam_ebizzoneno"] = vZoneId;

										SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty',null,'sum');
										SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc',null,'group');
										SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc',null,'group');
										SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku',null,'group');
										SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku',null,'group');
										SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc',null,'group');
										SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no',null,'group');

										SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location',null,'group');
										SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id',null,'group');


										SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container',null,'group');
										//SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no',null,'group');

										SOarray["custparam_venterwave"]=getWaveNo;
										SOarray["custparam_venterzone"]=getZoneNo;
										if(BeginLocation != NextShowLocation)
										{  
											nlapiLogExecution('Debug', 'Navigating To1', 'Picking Location');
											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_bulkpick_location', 'customdeploy_ebiz_rf_bulkpick_location', false, SOarray);

										}
										else if(ItemNo != NextShowItem)
										{  
											nlapiLogExecution('Debug', 'Navigating To1', 'Picking Item');

											response.sendRedirect('SUITELET', 'customscript_rf_bulkpick_item', 'customdeploy_rf_bulkpick_item', false, SOarray);								

										} 

									}
									else{
										//nlapiLogExecution('Debug', 'Before AutoPacking 1 : Sales Order InternalId', SalesOrderInternalId);
										//For fine tuning pick confirmation and doing autopacking in user event after last item				
										nlapiLogExecution('Debug', 'Navigating To1', 'Stage Location');

										if(itemattribute == 'T')
										{
											SOarray["custparam_containerlpno"] = getEnteredContainerNo;
											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_item_attribute', 'customdeploy_ebiz_rf_item_attribute_di', false, SOarray);
										}
										else
										{
											response.sendRedirect('SUITELET', 'customscript_rf_bulkpick_stage_loc', 'customdeploy_rf_bulkpick_stage_loc_di', false, SOarray);
										}
									}
								}
								else
								{
									nlapiLogExecution('Debug', 'Navigating To2', 'Stage Location');
									response.sendRedirect('SUITELET', 'customscript_rf_bulkpick_stage_loc', 'customdeploy_rf_bulkpick_stage_loc_di', false, SOarray);
								}

							}
							else  
							{
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								nlapiLogExecution('Debug', 'Error: ', 'Container# is invalid');
							}
						}
					}
					else
					{
						//case# 20148440 starts
						SOarray["custparam_error"]="PLEASE ENTER CARTON NO";
						//case# 20148440 ends
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						nlapiLogExecution('Debug', 'Error: ', 'Wave # not found');
					}
				}
			}
			catch (e) 
			{
				nlapiLogExecution('Debug', 'Exception: ', e);
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			} 
			finally 
			{					
				context.setSessionObject('session', null);
			}
		}
		else
		{
			SOarray["custparam_screenno"] = '12';
			SOarray["custparam_error"] = 'CARTON ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
		}
	}	
}


/*
 function getOpenTasksCount(waveno,containerlpno,vPickType,SalesOrderInternalId,OrdName,vZoneId,vSite)
{
	nlapiLogExecution('Debug', 'Into getOpenTasksCount...');

	var str = 'waveno. = ' + waveno + '<br>';
	str = str + 'containerlpno. = ' + containerlpno + '<br>';	
	str = str + 'vPickType. = ' + vPickType + '<br>';	
	str = str + 'SalesOrderInternalId. = ' + SalesOrderInternalId + '<br>';
	str = str + 'OrdName. = ' + OrdName + '<br>';
	str = str + 'vZoneId. = ' + vZoneId + '<br>';
	str = str + 'vSite. = ' + vSite + '<br>';

	nlapiLogExecution('Debug', 'Function Parameters', str);



	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_ebizsitepickrule', null, 'anyof', vSite));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizpickmethod');
	columns[1] = new nlobjSearchColumn('custrecord_cartonization_method','custrecord_ebizpickmethod');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_stagedetermination','custrecord_ebizpickmethod');

	var pickRuleSearchResult = new nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters, columns);

	var pickMethodId ;
	var Stagedet;
	if(pickRuleSearchResult != null){
		for (var i=0; i<pickRuleSearchResult.length; i++){			
			pickMethodId = pickRuleSearchResult[0].getValue('custrecord_ebizpickmethod');

			Stagedet = pickRuleSearchResult[i].getValue('custrecord_ebiz_stagedetermination','custrecord_ebizpickmethod');
		}
	}
	nlapiLogExecution('ERROR', 'pickMethodId', pickMethodId);


	var fields=new Array();	
	fields[0]='custrecord_ebiz_stagedetermination';
	var pickmethodcolumns = nlapiLookupField('customrecord_ebiznet_pick_method', pickMethodId, fields);	
	if(pickmethodcolumns!=null)

	var Stagedet = pickmethodcolumns.custrecord_ebiz_stagedetermination;

	nlapiLogExecution('ERROR', 'Stagedet', Stagedet);

	var openreccount=0;
	var SOFilters = new Array();
	var SOColumns = new Array();

	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK

	if(Stagedet == 3)//Carton Level
	{
		if(containerlpno !=null && containerlpno !='' && containerlpno !='null')
			SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', containerlpno));
	}	

	if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && waveno != null && waveno != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(waveno)));
	}	 
	if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
	{
		nlapiLogExecution('Debug', 'OrdNo inside If', OrdName);
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
	}
	if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && SalesOrderInternalId!=null && SalesOrderInternalId!="" && SalesOrderInternalId!= "null")
	{
		nlapiLogExecution('Debug', 'SO Id inside If', SalesOrderInternalId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId));
	}
	if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
	{
		nlapiLogExecution('Debug', 'Zone # inside If', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
	}

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	if(SOSearchResults!=null && SOSearchResults!=''&& SOSearchResults.length>0)
		openreccount=SOSearchResults.length;	

	nlapiLogExecution('Debug', 'Out of getOpenTasksCount...',openreccount);

	return openreccount;

}*/


function updatelastpicktaskforoldcarton(getEnteredContainerNo,getWaveNo,vZoneId)
{
	nlapiLogExecution('Debug', 'Into updatelastpicktaskforoldcarton', getEnteredContainerNo);
	nlapiLogExecution('Debug', 'getWaveNo', getWaveNo);
	nlapiLogExecution('Debug', 'vZoneId', vZoneId);
	var openreccount=0;

	var SOFilters = new Array();
	var SOColumns = new Array();
	var updatetask='T';

	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK	

	if(getWaveNo != null && getWaveNo != '')
	{	 
		nlapiLogExecution('DEBUG', 'getWaveNo inside if', getWaveNo); 
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(getWaveNo)));
	}
	if(vZoneId != null && vZoneId != '')
	{	 
		nlapiLogExecution('DEBUG', 'vZoneId inside if', vZoneId); 
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));
	}
	if(getEnteredContainerNo!=null && getEnteredContainerNo!='' && getEnteredContainerNo!='null')
		SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getEnteredContainerNo));	
	else
		SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'isempty'));	


	SOColumns[0] = new nlobjSearchColumn('custrecord_wms_status_flag');
	SOColumns[1] = new nlobjSearchColumn('custrecord_device_upload_flag');

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	if(SOSearchResults!=null && SOSearchResults!=''&& SOSearchResults.length>0)
	{
		for (var i = 0; i < SOSearchResults.length; i++){

			var status  = SOSearchResults[i].getValue('custrecord_wms_status_flag');
			var deviceuploadflag  = SOSearchResults[i].getValue('custrecord_device_upload_flag');

			nlapiLogExecution('Debug', 'status', status);
			nlapiLogExecution('Debug', 'deviceuploadflag', deviceuploadflag);

			if(status=='9')//Pick Generated
			{
				updatetask='F';
			}
		}
		nlapiLogExecution('Debug', 'updatetask', updatetask);

		if(updatetask=='T')
		{
			nlapiSubmitField('customrecord_ebiznet_trn_opentask', SOSearchResults[0].getId(),'custrecord_device_upload_flag', 'T');
		}
	}

	nlapiLogExecution('Debug', 'Out of updatelastpicktaskforoldcarton', updatetask);
}

function getAllContainers(containersize,whsite)
{
	nlapiLogExecution('Debug', 'into getAllContainers', '');
	var containerslist = new Array();

	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_containertype', null, 'anyof', ['1','4']));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_containername', null, 'is', containersize));				
	if(whsite!=null && whsite!='')
		filters.push(new nlobjSearchFilter('custrecord_ebizsitecontainer', null, 'anyof', ['@NONE@',whsite]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('Internalid');

	containerslist = nlapiSearchRecord('customrecord_ebiznet_container', null, filters, columns);

	nlapiLogExecution('Debug', 'out of getAllContainers', '');

	return containerslist;
}


function UpdateRFOpenTask(vActqty, containerInternalId,getEnteredContainerNo,itemCube,itemWeight,EndLocation,
		PickQty,ExceptionFlag,EntLocRec,ItemNo,SerialNo,getWaveNo,vZoneId,beginlocid,request)
{
	var str = 'vActqty. = ' + vActqty + '<br>';
	str = str + 'containerInternalId. = ' + containerInternalId + '<br>';	
	str = str + 'getEnteredContainerNo. = ' + getEnteredContainerNo + '<br>';
	str = str + 'itemCube. = ' + itemCube + '<br>';
	str = str + 'itemWeight. = ' + itemWeight + '<br>';
	str = str + 'EndLocation. = ' + EndLocation + '<br>';
	str = str + 'PickQty. = ' + PickQty + '<br>';
	str = str + 'ExceptionFlag. = ' + ExceptionFlag + '<br>';
	str = str + 'EntLocRec. = ' + EntLocRec + '<br>';
	str = str + 'ItemNo. = ' + ItemNo + '<br>';
	str = str + 'SerialNo. = ' + SerialNo + '<br>';
	str = str + 'getWaveNo. = ' + getWaveNo + '<br>';
	str = str + 'vZoneId. = ' + vZoneId + '<br>';
	str = str + 'beginlocid. = ' + beginlocid + '<br>';
	str = str + 'ItemNo. = ' + ItemNo + '<br>';
	nlapiLogExecution('Debug', 'Parameter Values in Response', str);

	try
	{
		var SOFilters = new Array();
		if(getWaveNo != null && getWaveNo != '')
		{	 
			nlapiLogExecution('DEBUG', 'getWaveNo inside if', getWaveNo); 
			SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(getWaveNo)));
		}

		if(vZoneId != null && vZoneId != '')
		{	 

			nlapiLogExecution('DEBUG', 'vZoneId inside if', vZoneId);
			SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));

		}

		SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));//pick gen
		SOFilters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', beginlocid));
		SOFilters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', ItemNo));

		var SOColumns = new Array();

		//SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no',null,'group'));
		SOColumns.push(new nlobjSearchColumn('name'));
		SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
		SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
		SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
		SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
		SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
		SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
		SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no')); 
		SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no')); 
		SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid')); 
		SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_wave_no'));
		SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
		SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
		SOColumns.push(new nlobjSearchColumn('custrecord_container'));
		SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
		SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
		SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));


		SOColumns[0].setSort();
		SOColumns[1].setSort();
		SOColumns[2].setSort();
		SOColumns[3].setSort();
		SOColumns[4].setSort();
		SOColumns[5].setSort();
		SOColumns[6].setSort();

		var SOSearchResults1 = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
		nlapiLogExecution('DEBUG', 'UpdateRFOpenTaskSOSearchResults1', SOSearchResults1.length);
		if(SOSearchResults1 != null && SOSearchResults1 != '')
		{
			var OldFONo;
			for(var v=0;v<SOSearchResults1.length;v++)
			{
				var IsItLastPick='F';
				if((v!=0 && OldFONo != SOSearchResults1[v].getValue('name')) || (v==0 && SOSearchResults1.length>1 && SOSearchResults1[v].getValue('name') != SOSearchResults1[v+1].getValue('name')) || (v==SOSearchResults1.length-1))				{
					IsItLastPick='T';
					vLastPickTask=SOSearchResults1[v].getValue('name');
				}	
				var RecId=SOSearchResults1[v].getId();
				var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecId);
				var vinvrefno=transaction.getFieldValue('custrecord_invref_no');
				var vwmsstatus = transaction.getFieldValue('custrecord_wms_status_flag');
				var vActQty = transaction.getFieldValue('custrecord_expe_qty');
				var ControlNo = transaction.getFieldValue('custrecord_ebiz_cntrl_no');
				var ActPickQty=transaction.getFieldValue('custrecord_expe_qty');
				var expPickQty=transaction.getFieldValue('custrecord_expe_qty');
				var SalesOrderInternalId=transaction.getFieldValue('custrecord_ebiz_order_no');

				var PrevContainerLP=transaction.getFieldValue('custrecord_container_lp_no');

				nlapiLogExecution('ERROR', 'vwmsstatus: ', vwmsstatus);
				nlapiLogExecution('DEBUG', 'UpdateRFOpenTaskPrevContainerLP', PrevContainerLP);


				//start

				if(PrevContainerLP!=null && PrevContainerLP!='')
				{

					var filter=new Array();
					filter.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp',null,'is',PrevContainerLP));

					var lpsearchresult=nlapiSearchRecord('customrecord_ebiznet_master_lp',null,filter);
					if(lpsearchresult !=null && lpsearchresult !='' && lpsearchresult!= 'null') //Case# 20148688
					{
						nlapiLogExecution('DEBUG', 'UpdateRFOpenTasklpsearchresult', lpsearchresult.length);

						for(n=0;n<lpsearchresult.length;n++){
							var Id=lpsearchresult[n].getId();
							/*	var lptransaction = nlapiLoadRecord('customrecord_ebiznet_master_lp', Id);
					lptransaction.setFieldValue('custrecord_ebiz_lpmaster_masterlp',getEnteredContainerNo);
					lptransaction.setFieldValue('custrecord_ebiz_lpmaster_wmsstatusflag','8');
					nlapiSubmitRecord(lptransaction,false,true);
							 */

							var fields=new Array();
							var values=new Array();
							nlapiLogExecution('DEBUG', 'SalesOrderInternalId-Id', SalesOrderInternalId);
							fields[0] ='custrecord_ebiz_lpmaster_masterlp';
							fields[1] ='custrecord_ebiz_lpmaster_wmsstatusflag';
							fields[2] ='custrecord_ebiz_lpmaster_controlno';

							values[0] = getEnteredContainerNo;
							values[1] = '8';
							values[2] = SalesOrderInternalId;

							var updatefields = nlapiSubmitField('customrecord_ebiznet_master_lp',Id,fields,values);

						}
					}
				}



				//end


				/*	var filter=new Array();
				filter[0]=new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', PrevContainerLP);*/



				if(vwmsstatus!='8')
				{

					transaction.setFieldValue('custrecord_wms_status_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')
					if(ActPickQty!= null && ActPickQty !="")
						transaction.setFieldValue('custrecord_act_qty', ActPickQty);
					if(containerInternalId!= null && containerInternalId !="")
						transaction.setFieldValue('custrecord_container', containerInternalId);
					if(EndLocation!=null && EndLocation!="")
						transaction.setFieldValue('custrecord_actendloc', EndLocation);	
					if(itemWeight!=null && itemWeight!="")
						transaction.setFieldValue('custrecord_total_weight', parseFloat(itemWeight).toFixed(4));
					if(itemCube!=null && itemCube!="")
						transaction.setFieldValue('custrecord_totalcube', parseFloat(itemCube).toFixed(4));	
					if(getEnteredContainerNo!=null && getEnteredContainerNo!="")
						transaction.setFieldValue('custrecord_container_lp_no', getEnteredContainerNo);
					transaction.setFieldValue('custrecord_ebiz_old_containerlp', PrevContainerLP);
					transaction.setFieldValue('custrecord_bulk_pick_flag', 'T');
					transaction.setFieldValue('custrecord_act_end_date', DateStamp());
					transaction.setFieldValue('custrecord_actualendtime',TimeStamp());
					/*if(vReason!=null && vReason!="")
						transaction.setFieldValue('custrecord_notes', vReason);	*/
					if(SerialNo!=null && SerialNo!="")
						transaction.setFieldValue('custrecord_serial_no', SerialNo);
					var currentContext = nlapiGetContext();  
					var currentUserID = currentContext.getUser();
					transaction.setFieldValue('custrecord_upd_ebiz_user_no',currentUserID);

					nlapiLogExecution('Debug', 'IsItLastPick: ', IsItLastPick);
					transaction.setFieldValue('custrecord_device_upload_flag',IsItLastPick);
					var vemployee = request.getParameter('custpage_employee');

					nlapiLogExecution('Debug', 'Updating RF Open Task Record Id: ', RecId);

					if(ActPickQty != expPickQty)
					{
						var vkititem = '';
						var vkititemtext = '';

						var filters = new Array(); 			 
						filters[0] = new nlobjSearchFilter('custrecord_sku', null, 'is', ItemNo);	
						filters[1] = new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', SalesOrderInternalId);	

						var columns1 = new Array(); 
						columns1[0] = new nlobjSearchColumn('custrecord_parent_sku_no' ); 

						var searchresultskititem = nlapiSearchRecord( 'customrecord_ebiznet_trn_opentask', null, filters, columns1 ); 
						if(searchresultskititem!=null && searchresultskititem!='')
						{
							vkititem = searchresultskititem[0].getValue('custrecord_parent_sku_no');
							vkititemtext = searchresultskititem[0].getText('custrecord_parent_sku_no');
						}

						var kititemTypesku ='';

						if(vkititem!=null && vkititem!='')
						{
							kititemTypesku = nlapiLookupField('item', vkititem, 'recordType');
						}

						if(kititemTypesku=='kititem')
						{
							nlapiLogExecution('Debug', 'vsalesordInternid', SalesOrderInternalId);
							nlapiLogExecution('Debug', 'kititem', vkititem);
							var kitfilters = new Array(); 			 
							kitfilters[0] = new nlobjSearchFilter('custrecord_parent_sku_no', null, 'anyof', vkititem);	
							kitfilters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]);	
							kitfilters[2] = new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', SalesOrderInternalId);	

							var kitcolumns1 = new Array(); 
							kitcolumns1[0] = new nlobjSearchColumn( 'custrecord_expe_qty' ); 			
							kitcolumns1[1] = new nlobjSearchColumn( 'custrecord_act_qty' );

							var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, kitfilters, kitcolumns1 ); 
							for(var z=0; z<searchresults.length;z++) 
							{										
								var kitrcid=searchresults[z].getId();
								var Expqty=searchresults[z].getValue('custrecord_expe_qty');
								var Actqty=searchresults[z].getValue('custrecord_act_qty');

								nlapiLogExecution('Debug', 'Actqty', Actqty);
								nlapiLogExecution('Debug', 'Expqty', Expqty);
								if(Actqty==Expqty || Actqty=='')
								{
									nlapiSubmitField('customrecord_ebiznet_trn_opentask', kitrcid, 'custrecord_kit_exception_flag', 'T');
								}
							}

						}

					}

					if(ExceptionFlag=='L')
					{
						var rec=nlapiLoadRecord('customrecord_ebiznet_createinv',NewRecId);
						var LotNO=rec.getFieldText('custrecord_ebiz_inv_lot');
						nlapiLogExecution('Debug','LotNO',LotNO);
						transaction.setFieldValue('custrecord_batch_no',LotNO);
					}

					nlapiSubmitRecord(transaction, false, true);
					nlapiLogExecution('Debug', 'Time Stamp after updating open task',TimeStampinSec());
					nlapiLogExecution('Debug', 'Updated RF Open Task successfully');
					nlapiLogExecution('Debug', 'ExceptionFlag',ExceptionFlag);
					if((SerialNo!=null && SerialNo!='') && SerialNo!='null' )
						UpdateSerialentry(RecId,SerialNo);
					//code added on 15Feb by suman.
					if(ExceptionFlag!='L')
					{
						try
						{
							nlapiLogExecution('Debug', 'Time Stamp at the start of deleteAllocations',TimeStampinSec());
							deleteAllocations(vinvrefno,ActPickQty,getEnteredContainerNo,expPickQty,SalesOrderInternalId);
							nlapiLogExecution('Debug', 'Time Stamp at the end of deleteAllocations',TimeStampinSec());
						}
						catch(exp)
						{
							nlapiLogExecution('Debug', 'Exception in deleteAllocations',exp);
						}
					}
					else
					{
						nlapiLogExecution('Debug', 'vinvrefno',vinvrefno);
						nlapiLogExecution('Debug', 'NewRecId',NewRecId);

						try
						{
							if(vinvrefno!=null && vinvrefno!='')
								deleteOldAllocations(vinvrefno,ActPickQty,getEnteredContainerNo,expPickQty,SalesOrderInternalId);
							deleteNewAllocations(NewRecId,ActPickQty,getEnteredContainerNo,expPickQty,SalesOrderInternalId);
						}
						catch(exp)
						{
							nlapiLogExecution('Debug', 'Exception in deleteOldNewAllocations',exp);
						}
					}
				}
				if(ControlNo != null && ControlNo != '')
				{
					UpdateRFFulfillOrdLine(ControlNo,vActQty);	
				}	
			}	
		}	


		//End of code as of 15Feb.
	}
	catch(exp1)
	{
		nlapiLogExecution('Debug', 'Exception in UpdateRFOpenTask',exp1);
	}
}

/**
 * This function clear the allocation qty from inventory.
 * @param invrefno
 * @param PickQty
 */
function deleteAllocations(invrefno,ActPickQty,contlpno,expPickQty,SalesOrderInternalId)
{
	nlapiLogExecution('Debug', 'Into deleteAllocations (Inv Ref NO)',invrefno);
	var str = 'Inv Ref No. = ' + invrefno + '<br>';
	str = str + 'CARTON LP. = ' + contlpno + '<br>';	
	str = str + 'ActPickQty. = ' + ActPickQty + '<br>';	
	str = str + 'expPickQty. = ' + expPickQty + '<br>';	

	nlapiLogExecution('Debug', 'Parameter Values', str);

	try
	{

		var scount=1;
		var invtrecid;
		var newqoh = 0;
		LABL1: for(var i=0;i<scount;i++)
		{	

			nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);
			try
			{
				var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
				var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
				var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');
				var vNewAllocQty=0;
				if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {
					vNewAllocQty=parseFloat(Invallocqty)- parseFloat(expPickQty);
					if(parseFloat(vNewAllocQty)<0)
						vNewAllocQty=0;

					Invttran.setFieldValue('custrecord_ebiz_alloc_qty',parseFloat(vNewAllocQty));
				}
				else
					Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);


				if (InvQOH != null && InvQOH != "" && InvQOH>0) {
					newqoh=parseFloat(InvQOH)- parseFloat(expPickQty);
					if(parseFloat(newqoh)<0)
						newqoh=0;

					//case # 20146553 
					Invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(newqoh));

					//	Invttran.setFieldValue('custrecord_ebiz_alloc_qty',parseFloat(newqoh));
				}
				else
					Invttran.setFieldValue('custrecord_ebiz_qoh',0);

				nlapiLogExecution('Debug', 'vNewAllocQty',vNewAllocQty);



				//Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(InvQOH) - (parseInt(expPickQty)-parseInt(remQty))); 
				Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
				Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

				invtrecid = nlapiSubmitRecord(Invttran, false, true);
				nlapiLogExecution('Debug', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);



//				var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
//				var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
//				var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');
//				var vNewAllocQty=0;
//				if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {
//				vNewAllocQty=parseInt(Invallocqty)- parseInt(expPickQty);
//				if(parseFloat(vNewAllocQty)<0)
//				vNewAllocQty=0;

//				Invttran.setFieldValue('custrecord_ebiz_alloc_qty',parseInt(vNewAllocQty));
//				}
//				else
//				Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);

//				nlapiLogExecution('Debug', 'vNewAllocQty',vNewAllocQty);

//				var remainqty=0;


//				if(remQty==null || remQty=='null'  || remQty=='' || isNaN(remQty))
//				{
//				newqoh = parseInt(InvQOH) - parseInt(expPickQty);
//				Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(newqoh)); 
//				}
//				else
//				{
//				newqoh = parseInt(vNewAllocQty) + parseInt(remQty);
//				Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(newqoh)); 
//				}

//				//Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(InvQOH) - (parseInt(expPickQty)-parseInt(remQty))); 
//				Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
//				Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

//				invtrecid = nlapiSubmitRecord(Invttran, false, true);
//				nlapiLogExecution('Debug', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);


			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 
				/*var exceptionname='RF Picking';
				var functionality='RFPick';
				var trantype=3;
				var vInvRecId=invrefno;
				var vcontainerLp=contlpno;
				nlapiLogExecution('Debug', 'DetailsError', functionality);	
				nlapiLogExecution('Debug', 'vcontainerLp', vcontainerLp);
				nlapiLogExecution('Debug', 'vInvRecId', invrefno);
				var reference3=ActPickQty;
				var reference4 ="";
				var reference5 ="";
				var userId = nlapiGetUser();
				InsertExceptionLog(exceptionname,trantype, functionality, wmsE, invrefno, vcontainerLp,reference3,reference4,reference5, userId);*/
				nlapiLogExecution('Debug', 'Exception in RF Inv move : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}


//		case # 20148057
		//case # 20148200 starts (stage record creation)
		if(invtrecid!=null && invtrecid!='' && parseFloat(ActPickQty)>0)
			CreateSTGInvtRecord(invtrecid, contlpno,ActPickQty,SalesOrderInternalId);
		//case # 20148200 end
		if((parseFloat(newqoh)) <= 0)
		{		
			nlapiLogExecution('Debug', 'Deleting record from inventory if QOH becomes zero', invrefno);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
		}
	}
	catch(exp)
	{
		nlapiLogExecution('Debug', 'Exception in deleteAllocations',exp);
	}		
}

/**
 * This function creats outbound staging inventory
 * @param invtrecid
 * @param vContLp
 * @param vqty
 */

function CreateSTGInvtRecord(invtrecid, vContLp,vqty,SalesOrderInternalId) 
{
	nlapiLogExecution('Debug', 'Into CreateSTGInvtRecord');
	var str = 'Inv Ref No. = ' + invtrecid + '<br>';
	str = str + 'CARTON LP. = ' + vContLp + '<br>';	
	str = str + 'Qty. = ' + vqty + '<br>';	
	str = str + 'SalesOrderInternalId. = ' + SalesOrderInternalId + '<br>';	
	nlapiLogExecution('Debug', 'STG INVT Details', str);

	try
	{
		var stgmInvtRec = nlapiCopyRecord('customrecord_ebiznet_createinv',invtrecid);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_lp', vContLp);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(vqty).toFixed(4));
		stgmInvtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(vqty).toFixed(4));
		stgmInvtRec.setFieldValue('custrecord_ebiz_alloc_qty', '0');
		stgmInvtRec.setFieldValue('custrecord_wms_inv_status_flag', '18');	//FLAG.INVENTORY.OUTBOUND('O') 		
		stgmInvtRec.setFieldValue('custrecord_invttasktype', '3'); //Task Type - PICK
		stgmInvtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
		stgmInvtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');	
		if(SalesOrderInternalId!=null && SalesOrderInternalId!='')
			stgmInvtRec.setFieldValue('custrecord_ebiz_transaction_no', SalesOrderInternalId);		
		nlapiSubmitRecord(stgmInvtRec, false, true);
	}
	catch(exp)
	{
		nlapiLogExecution('Debug', 'Exception in CreateSTGInvtRecord',exp);
	}

	nlapiLogExecution('Debug', 'Out of CreateSTGInvtRecord');
}

/**
 * 
 * @param lp
 * @returns
 */
function containerRFLPExists(lp){
	var filters = new Array();
	nlapiLogExecution('Debug', 'lp:',lp); 
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lp));	 
	nlapiLogExecution('Debug', 'Master LP Feaching','Success');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
	return searchresults;
}

function OpenTaskRFLPExists(lp){
	var filters = new Array();
	nlapiLogExecution('Debug', 'lp:',lp); 
	filters.push( new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', lp));	 

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');

	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	return searchresults;
}

function getLPWeight(lp){
	var filters = new Array();
	nlapiLogExecution('Debug', 'lp:',lp); 
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lp));	 
	nlapiLogExecution('Debug', 'Master LP Feaching','Success');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
	var lpmastArr=new Array();
	nlapiLogExecution('Debug', 'Before:',lp); 
	if(searchresults != null && searchresults!= "" && searchresults.length>0)
	{
		lpmastArr.push(searchresults[0].getValue('custrecord_ebiz_lpmaster_lp'));
		lpmastArr.push(searchresults[0].getValue('custrecord_ebiz_lpmaster_totwght'));
		nlapiLogExecution('Debug', 'lp:',searchresults[0].getValue('custrecord_ebiz_lpmaster_lp')); 
		nlapiLogExecution('Debug', 'lpweight:',searchresults[0].getValue('custrecord_ebiz_lpmaster_totwght')); 
	}
	return lpmastArr;
}

function containerSizeExists(lp,SizeId){
	var filters = new Array();
	nlapiLogExecution('Debug', 'lp:',lp); 
	nlapiLogExecution('Debug', 'SizeId:',SizeId); 
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lp));
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_sizeid', null, 'anyof', SizeId));	
	nlapiLogExecution('Debug', 'Size Feaching','Success');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sizeid');
	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);        	       	
	return searchresults;
}

/**
 * 
 * @param vContLpNo
 * @param vContainerSize
 * @param TotalWeight
 * @param ContainerCube
 */
function CreateRFMasterLPRecord(vContLpNo,containerInternalId,ContainerCube,ItemWeight,getContainerSize,ResultText)
{																							
	var contLPExists = containerRFLPExists(vContLpNo);

	var TotalWeight=ItemWeight;


	if(contLPExists==null){		

		var MastLP=nlapiCreateRecord('customrecord_ebiznet_master_lp');
		MastLP.setFieldValue('name', vContLpNo);
		MastLP.setFieldValue('custrecord_ebiz_lpmaster_lp', vContLpNo);
		if(containerInternalId!="" && containerInternalId!=null)
		{
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_sizeid', containerInternalId);
		}	
		nlapiLogExecution('Debug', 'TotalWeight',TotalWeight);
		nlapiLogExecution('Debug', 'ContainerCube',ContainerCube);
		if(TotalWeight!="" && TotalWeight!=null)
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_totwght', parseFloat(TotalWeight).toFixed(4));
		if(ContainerCube!="" && ContainerCube!=null)
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(ContainerCube).toFixed(4));
		if(ResultText != null && ResultText != '')
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_sscc', ResultText);
		var currentContext = nlapiGetContext();		        		
		var retktoval = nlapiSubmitRecord(MastLP);
		nlapiLogExecution('Debug', 'Master LP Insertion','Success');

	}
	else
	{
		var recordId="";

		if(contLPExists!=null){


			for (var i = 0; i < contLPExists.length; i++){
				recordId = contLPExists[i].getId();
				LPWeight = contLPExists[i].getValue('custrecord_ebiz_lpmaster_totwght');
				nlapiLogExecution('Debug', 'recordId',recordId);		

				var MastLPUpdate = nlapiLoadRecord('customrecord_ebiznet_master_lp', recordId);	
				TotWeight=parseFloat(ItemWeight)+parseFloat(LPWeight);
				nlapiLogExecution('Debug', 'itemWeight',ItemWeight);
				nlapiLogExecution('Debug', 'LPWeight',LPWeight);
				nlapiLogExecution('Debug', 'TotWeight',TotWeight);

//				if(TotWeight!="" && TotWeight!=null)
//				MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_totwght', parseFloat(TotWeight).toFixed(4));
//				if(ContainerCube!="" && ContainerCube!=null)
//				MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(ContainerCube).toFixed(4));
				if(ResultText != null && ResultText != '')
					MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_sscc', ResultText);
				var currentContext = nlapiGetContext();	        				
				nlapiSubmitRecord(MastLPUpdate, false, true);        				 
				nlapiLogExecution('Debug', 'Master LP Updation','Success');

			}

		}
	}
}

function UpdateRFFulfillOrdLine(vdono,vActqty)
{
	var pickqtyfinal =0;
	nlapiLogExecution('Debug', 'into UpdateRFFulfillOrdLine : ', vdono);

	try
	{

		var doline = nlapiLoadRecord('customrecord_ebiznet_ordline',vdono);	

		var oldpickQty = doline.getFieldValue('custrecord_pickqty');
		var ordQty = doline.getFieldValue('custrecord_ord_qty');
		var pickgenqty = doline.getFieldValue('custrecord_pickgen_qty');
		var vorditem = doline.getFieldValue('custrecord_ebiz_linesku');
		
		var vitemtype = nlapiLookupField('item', vorditem, 'recordType');
		
		if(isNaN(oldpickQty) || oldpickQty==null || oldpickQty=='')
			oldpickQty=0;

		if(isNaN(ordQty) || ordQty==null || ordQty=='')
			ordQty=0;

		if(isNaN(vActqty) || vActqty==null || vActqty=='')
			vActqty=0;

		if(isNaN(pickgenqty) || pickgenqty==null || pickgenqty=='')
			pickgenqty=0;

		pickqtyfinal=parseFloat(oldpickQty)+parseFloat(vActqty);

		var str = 'pickqtyfinal. = ' + pickqtyfinal + '<br>';
		str = str + 'oldpickQty. = ' + oldpickQty + '<br>';	
		str = str + 'ordQty. = ' + ordQty + '<br>';	
		str = str + 'vActqty. = ' + vActqty + '<br>';	
		str = str + 'pickgenqty. = ' + pickgenqty + '<br>';	
		str = str + 'vitemtype. = ' + vitemtype + '<br>';	

		nlapiLogExecution('Debug', 'Qty Details', str);

		if((parseFloat(pickgenqty)>=parseFloat(pickqtyfinal)) && vitemtype!='kititem')
		{
			doline.setFieldValue('custrecord_upddate', DateStamp());
			doline.setFieldValue('custrecord_pickqty', parseFloat(pickqtyfinal));

			if(parseFloat(pickqtyfinal)<parseFloat(ordQty))
				doline.setFieldValue('custrecord_linestatus_flag', '11'); //STATUS.OUTBOUND.PARTIALLY_PROCESSED
			else
				doline.setFieldValue('custrecord_linestatus_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')

			nlapiSubmitRecord(doline, false, true);
		}
		else
		{
			doline.setFieldValue('custrecord_upddate', DateStamp());
			doline.setFieldValue('custrecord_pickqty', parseFloat(pickgenqty));
			doline.setFieldValue('custrecord_linestatus_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')

			nlapiSubmitRecord(doline, false, true);
		}
	}
	catch(exp)
	{
		nlapiLogExecution('Debug', 'Exception in UpdateRFFulfillOrdLine',exp);
	}

	nlapiLogExecution('Debug', 'Out of UpdateRFFulfillOrdLine ');
}

/**
 * This function clear the allocation qty from inventory.
 * @param invrefno
 * @param PickQty
 */
function deleteOldAllocations(invrefno,actPickQty,contlpno,expPickQty,SalesOrderInternalId)
{
	nlapiLogExecution('Debug', 'Into deleteOldAllocations (Inv Ref NO)',invrefno);

	try
	{
		var scount=1;
		var invtrecid;

		LABL1: for(var i=0;i<scount;i++)
		{	

			nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);
			try
			{
				var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
				var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
				var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

				if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {

					if(parseFloat(Invallocqty)- parseFloat(expPickQty)>=0)
						Invttran.setFieldValue('custrecord_ebiz_alloc_qty',parseFloat(Invallocqty)- parseFloat(expPickQty));
					else
						Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);

				}
				else
				{
					Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);
				}

				//Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(InvQOH) - parseInt(ActPickQty)); 
				Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
				Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

				invtrecid = nlapiSubmitRecord(Invttran, false, true);
				nlapiLogExecution('Debug', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);

//				var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
//				var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
//				var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

//				if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {

//				if(parseFloat(Invallocqty)- parseFloat(expPickQty)>=0)
//				Invttran.setFieldValue('custrecord_ebiz_alloc_qty',parseFloat(Invallocqty)- parseFloat(expPickQty));
//				else
//				Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);

//				}
//				else
//				{
//				Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);
//				}

//				//Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(InvQOH) - parseInt(ActPickQty)); 
//				Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
//				Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

//				var invtrecid = nlapiSubmitRecord(Invttran, false, true);
//				nlapiLogExecution('Debug', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);


			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 
				/*var exceptionname='RF Picking';
				var functionality='RFPick';
				var trantype=3;
				var vInvRecId=invrefno;
				var vcontainerLp=contlpno;
				nlapiLogExecution('Debug', 'DetailsError', functionality);	
				nlapiLogExecution('Debug', 'vcontainerLp', vcontainerLp);
				nlapiLogExecution('Debug', 'vInvRecId', invrefno);
				var reference3=ActPickQty;
				var reference4 ="";
				var reference5 ="";
				var userId = nlapiGetUser();
				InsertExceptionLog(exceptionname,trantype, functionality, wmsE, invrefno, vcontainerLp,reference3,reference4,reference5, userId);*/
				nlapiLogExecution('Debug', 'Exception in RF Inv move : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}

		/*if(invtrecid!=null && invtrecid!='')
			CreateSTGInvtRecord(invtrecid, contlpno,pickqty);
		 */
		/*if((parseFloat(InvQOH) - parseFloat(ActPickQty)) == 0)
		{		
			nlapiLogExecution('Debug', 'Deleting record from inventory if QOH becomes zero', invrefno);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
		}*/
	}
	catch(exp)
	{
		nlapiLogExecution('Debug', 'Exception in deleteAllocations',exp);
	}

	nlapiLogExecution('Debug', 'Out of deleteOldAllocations (Inv Ref NO)',invrefno);
}

/**
 * This function clear the allocation qty from inventory.
 * @param invrefno
 * @param PickQty
 */
function deleteNewAllocations(invrefno,actPickQty,contlpno,expPickQty,SalesOrderInternalId)
{
	nlapiLogExecution('Debug', 'Into deleteNewAllocations (Inv Ref NO)',invrefno);
	nlapiLogExecution('Debug', 'actPickQty',actPickQty);
	nlapiLogExecution('Debug', 'expPickQty',expPickQty);
	//nlapiLogExecution('Debug', 'vRemQty',vRemQty);

	try
	{

		var scount=1;
		var invtrecid;
		var InvQOH =0;
		LABL1: for(var i=0;i<scount;i++)
		{	

			nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);
			try
			{
				var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
				var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
				InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

				if (InvQOH != null && InvQOH != "" && InvQOH>0) {

					if(parseFloat(InvQOH)- parseFloat(expPickQty)>=0)
						Invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(InvQOH)- parseFloat(expPickQty));
					else
						Invttran.setFieldValue('custrecord_ebiz_qoh',0);

				}
				else
				{
					Invttran.setFieldValue('custrecord_ebiz_qoh',0);
				}

				//Invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(InvQOH) - parseFloat(expPickQty)); 
				Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
				Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

				invtrecid = nlapiSubmitRecord(Invttran, false, true);
				nlapiLogExecution('Debug', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);



//				var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
//				var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
//				InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

//				//Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(InvQOH) - (parseInt(expPickQty)-parseInt(vRemQty))); 
//				Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(InvQOH) - parseInt(expPickQty)); 
//				Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
//				Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

//				var invtrecid = nlapiSubmitRecord(Invttran, false, true);
//				nlapiLogExecution('Debug', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);


			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 
				/*var exceptionname='RF Picking';
				var functionality='RFPick';
				var trantype=3;
				var vInvRecId=invrefno;
				var vcontainerLp=contlpno;
				nlapiLogExecution('Debug', 'DetailsError', functionality);	
				nlapiLogExecution('Debug', 'vcontainerLp', vcontainerLp);
				nlapiLogExecution('Debug', 'vInvRecId', invrefno);
				var reference3=ActPickQty;
				var reference4 ="";
				var reference5 ="";
				var userId = nlapiGetUser();
				InsertExceptionLog(exceptionname,trantype, functionality, wmsE, invrefno, vcontainerLp,reference3,reference4,reference5, userId);*/
				nlapiLogExecution('Debug', 'Exception in RF Inv move : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}




		if(invtrecid!=null && invtrecid!='' && parseFloat(actPickQty)>0)
			CreateSTGInvtRecord(invtrecid, contlpno,actPickQty,SalesOrderInternalId);

		if((parseFloat(InvQOH) - parseFloat(expPickQty)) <= 0)
		{		
			nlapiLogExecution('Debug', 'Deleting record from inventory if QOH becomes zero', invrefno);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
		}
	}
	catch(exp)
	{
		nlapiLogExecution('Debug', 'Exception in deleteAllocations',exp);
	}	

	nlapiLogExecution('Debug', 'Out of deleteNewAllocations (Inv Ref NO)',invrefno);
}



/**
 * @param getEnteredContainerNo
 * @param getFetchedContainerNo
 * @param SOOrder
 * @param OrdName
 */
function UpdateAllContainerLp(getEnteredContainerNo,getFetchedContainerNo,SOOrder,OrdName,waveno,vZoneId,vPickType)
{
	nlapiLogExecution('Debug','Into UpdateAllContainerLp');

	try
	{

		var str = 'waveno. = ' + waveno + '<br>';
		str = str + 'SOOrder. = ' + SOOrder + '<br>';	
		str = str + 'OrdName. = ' + OrdName + '<br>';
		str = str + 'vZoneId. = ' + vZoneId + '<br>';
		str = str + 'getFetchedContainerNo. = ' + getFetchedContainerNo + '<br>';
		str = str + 'getEnteredContainerNo. = ' + getEnteredContainerNo + '<br>';
		str = str + 'vPickType. = ' + vPickType + '<br>';

		nlapiLogExecution('Debug', 'Function Parameters', str);

		var context = nlapiGetContext();
		var orderno, containerno,ebizcntrlno,siteid,compid,containersize,totweight,internalid,item,waveno,
		beginloc,endloc,lot,actqty,expqty,uomlevel,empid,begindate,begintime,packcode,lp,lineno,Zoneno,ebizLP,
		taskassignedto;

		var otparent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent');
		var parentid = nlapiSubmitRecord(otparent); 
		var newParent = nlapiLoadRecord('customrecord_ebiz_throwaway_parent', parentid);

		var filteropentask=new Array();
		filteropentask.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
		filteropentask.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK	

		if(getFetchedContainerNo!=null && getFetchedContainerNo!='' && getFetchedContainerNo!='null')
			filteropentask.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getFetchedContainerNo));	
		else
			filteropentask.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'isempty'));	

		filteropentask.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));

		if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && waveno != null && waveno != "")
		{
			filteropentask.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(waveno)));
		}	 
		if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
		{
			filteropentask.push(new nlobjSearchFilter('name', null, 'is', OrdName));
		}
		if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && SOOrder!=null && SOOrder!="" && SOOrder!= "null")
		{
			filteropentask.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SOOrder));
		}
		if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
		{	
			filteropentask.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
		}

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('name');
		columns[1] = new nlobjSearchColumn('custrecord_container_lp_no');
		columns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		columns[4] = new nlobjSearchColumn('custrecord_comp_id');
		columns[5] = new nlobjSearchColumn('custrecord_wms_location');
		columns[6] = new nlobjSearchColumn('custrecord_container');
		columns[7] = new nlobjSearchColumn('custrecord_total_weight');
		columns[8] = new nlobjSearchColumn('custrecord_sku');
		columns[9] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[10] = new nlobjSearchColumn('custrecord_actendloc');
		columns[11] = new nlobjSearchColumn('custrecord_batch_no');
		columns[12] = new nlobjSearchColumn('custrecord_act_qty');
		columns[13] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[14] = new nlobjSearchColumn('custrecord_uom_level');
		columns[15] = new nlobjSearchColumn('custrecord_ebizuser');
		columns[16] = new nlobjSearchColumn('custrecordact_begin_date');
		columns[17] = new nlobjSearchColumn('custrecord_actualbegintime');
		columns[18] = new nlobjSearchColumn('custrecord_packcode');
		columns[19] = new nlobjSearchColumn('custrecord_lpno');
		columns[20] = new nlobjSearchColumn('custrecord_line_no');
		columns[21] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
		columns[22] = new nlobjSearchColumn('custrecord_ebiz_zoneid');
		columns[23] = new nlobjSearchColumn('custrecord_ebiz_lpno');
		columns[24] = new nlobjSearchColumn('custrecord_ebizzone_no');
		columns[25] = new nlobjSearchColumn('custrecord_taskassignedto');

		var SearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filteropentask, columns);
		if(SearchResults!=null&&SearchResults!="")
		{
			for ( var i = 0; i < SearchResults.length; i++) 
			{

//				var recid=SearchResults[count].getId();
//				var UpdateRec=nlapiSubmitField('customrecord_ebiznet_trn_opentask',recid,'custrecord_container_lp_no',getEnteredContainerNo);
//				nlapiLogExecution('Debug','UpdateRec',UpdateRec);

				orderno = SearchResults[i].getValue('name');
				containerno = SearchResults[i].getValue('custrecord_container_lp_no');
				ebizorderno = SearchResults[i].getValue('custrecord_ebiz_order_no');
				ebizcntrlno = SearchResults[i].getValue('custrecord_ebiz_cntrl_no');
				compid = SearchResults[i].getValue('custrecord_comp_id');
				siteid = SearchResults[i].getValue('custrecord_wms_location');
				containersize = SearchResults[i].getValue('custrecord_container');
				totweight = SearchResults[i].getValue('custrecord_total_weight');
				item = SearchResults[i].getValue('custrecord_sku');
				beginloc = SearchResults[i].getValue('custrecord_actbeginloc');
				endloc = SearchResults[i].getValue('custrecord_actendloc');
				lot = SearchResults[i].getValue('custrecord_batch_no');
				actqty = SearchResults[i].getValue('custrecord_act_qty');
				expqty = SearchResults[i].getValue('custrecord_expe_qty');
				uomlevel = SearchResults[i].getValue('custrecord_uom_level');
				empid = SearchResults[i].getValue('custrecord_taskassignedto');
				internalid = SearchResults[i].getId();
				begindate=SearchResults[i].getValue('custrecordact_begin_date');
				begintime=SearchResults[i].getValue('custrecord_actualbegintime');
				packcode=SearchResults[i].getValue('custrecord_packcode');
				lp=SearchResults[i].getValue('custrecord_lpno');
				lineno=SearchResults[i].getValue('custrecord_line_no');
				waveno = SearchResults[i].getValue('custrecord_ebiz_wave_no');
				Zoneno = SearchResults[i].getValue('custrecord_ebiz_zoneid');
				ebizLP = SearchResults[i].getValue('custrecord_ebiz_lpno');
				var vZoneno=SearchResults[i].getValue('custrecord_ebizzone_no');
				taskassignedto = SearchResults[i].getValue('custrecord_taskassignedto');

				newParent.selectNewLineItem('recmachcustrecord_ebiz_ot_parent');

				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'id', internalid);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','name', orderno);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_wave_no', waveno);
				//newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualendtime', TimeStamp());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_recordupdatetime', TimeStamp());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_upd_date', DateStamp());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actendloc', endloc);
				//newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_end_date', DateStamp());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_batch_no', lot);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_status_flag', 9);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_location', siteid);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_uom_level', uomlevel);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_qty', parseFloat(actqty).toFixed(4));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_upd_ebiz_user_no', context.getUser());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actbeginloc', beginloc);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecordact_begin_date',begindate);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualbegintime', begintime);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_taskassignedto', empid);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_cntrl_no', ebizcntrlno);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_tasktype', 3);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_expe_qty', parseFloat(expqty).toFixed(4));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_sku', item);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_sku_no', item);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_packcode', packcode);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_lpno', lp);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_order_no', ebizorderno);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_line_no', lineno);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_comp_id', compid);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_container_lp_no', getEnteredContainerNo);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_container', containersize);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_total_weight', parseFloat(totweight).toFixed(4));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_zoneid', Zoneno);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebizzone_no', vZoneno);

				if(ebizLP==null||ebizLP=="")
					newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_lpno', ebizLP);

				if(taskassignedto!=null&&taskassignedto!="")
					newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_taskassignedto', taskassignedto);

				newParent.commitLineItem('recmachcustrecord_ebiz_ot_parent');


			}
			nlapiSubmitRecord(newParent);
		}
	}
	catch(exp)
	{
		nlapiLogExecution('Debug','Exception in UpdateAllContianerLp',exp);	
	}

	nlapiLogExecution('Debug','Out of UpdateAllContianerLp');	
}



function fnValidateEnteredContLP(EnteredContLP,getWaveNo,vZoneId)
{
	nlapiLogExecution('Debug', 'Into fnValidateEnteredContLP',EnteredContLP);	
	//nlapiLogExecution('Debug', 'SalesOrderInternalId',SalesOrderInternalId);	
	var filters=new Array();
	filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', EnteredContLP));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', '3'));
	if(getWaveNo != null && getWaveNo != '')
	{	 
		nlapiLogExecution('DEBUG', 'getWaveNo inside if', getWaveNo); 
		filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'isnot', parseInt(getWaveNo)));
	}  
	/*if(vsite!=null && vsite!='')
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vsite)); */ 
	if(vZoneId!=null && vZoneId!='')
	{
		nlapiLogExecution('DEBUG', 'vZoneId inside if', vZoneId);	
		filters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'isnot', vZoneId));
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');



	var salesOrderList = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return salesOrderList;
}
function isExitfnValidateEnteredContLP(EnteredContLP,getWaveNo,vZoneId)
{
	nlapiLogExecution('Debug', 'Into fnValidateEnteredContLP',EnteredContLP);	
	//nlapiLogExecution('Debug', 'SalesOrderInternalId',SalesOrderInternalId);	
	var filters=new Array();
	filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', EnteredContLP));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', '3'));
	if(getWaveNo != null && getWaveNo != '')
	{	 
		nlapiLogExecution('DEBUG', 'getWaveNo inside if', getWaveNo); 
		filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(getWaveNo)));
	}  
	/*if(vsite!=null && vsite!='')
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vsite)); */ 
	if(vZoneId!=null && vZoneId!='')
	{
		nlapiLogExecution('DEBUG', 'vZoneId inside if', vZoneId);	
		filters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');



	var salesOrderList = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return salesOrderList;
}


function GenerateLable(uompackflag){

	nlapiLogExecution('Debug', 'CreateMasterLPRecord uompackflag', uompackflag);
	var finaltext="";
	var duns="";
	var label="",uom="",ResultText="";
	//added by mahesh


	try 
	{	
		var lpMaxValue=GetMaxLPNo('2', '5','');
		nlapiLogExecution('Debug', 'lpMaxValue', lpMaxValue);

		var prefixlength=lpMaxValue.length;
		nlapiLogExecution('Debug', 'prefixlength', prefixlength);

		if(prefixlength==0)
			label="000000000";
		else if(prefixlength==1)
			label="00000000"+lpMaxValue;
		else if(prefixlength==2)
			label="0000000"+lpMaxValue;
		else if(prefixlength==3)
			label="000000"+lpMaxValue;
		else if(prefixlength==4)
			label="00000"+lpMaxValue;
		else if(prefixlength==5)
			label="0000"+lpMaxValue;
		else if(prefixlength==6)
			label="000"+lpMaxValue;
		else if(prefixlength==7)
			label="00"+lpMaxValue;
		else if(prefixlength==8)
			label="0"+lpMaxValue;
		else if(prefixlength==9)
			label=lpMaxValue;

		//to get company id

		duns=GetCompanyDUNSnumber('');

		if(uompackflag == "1" || uompackflag == "EACH" ) 
			uom="0"; 
		else if(uompackflag == "3" || uompackflag == "PALLET") 
			uom="2";
		else
			uom="0";
		nlapiLogExecution('Debug', 'uom', uom);
		nlapiLogExecution('Debug', 'CreateMasterLPRecord finaltext', finaltext);
		finaltext=uom+duns+label;
		nlapiLogExecution('Debug', 'CreateMasterLPRecord finaltext', finaltext);



		//to get chk digit
		var checkStr=finaltext;
		var ARL=0;
		var BRL=0;
		var CheckDigitValue="";
		for (i = checkStr.length-1;  i > 0;  i--)
		{
			ARL = ARL+parseInt(checkStr.charAt(i));
			i--;
		}		
		ARL=ARL*3;
		for (i = checkStr.length-2;  i > 0;  i--)
		{
			BRL = BRL+parseInt(checkStr.charAt(i));
			i--;
		}		
		var sumOfARLBRL=ARL+BRL;
		var CheckDigit=0;

		while(CheckDigit<10)
		{
			if(sumOfARLBRL%10==0)
			{ 
				CheckDigitValue=CheckDigit; 
				break; 
			} 

			sumOfARLBRL++;
			CheckDigit++;
		}
		nlapiLogExecution('Debug', 'CreateMasterLPRecord CheckDigit', CheckDigitValue.toString());
		ResultText="00"+finaltext+CheckDigitValue.toString();
		nlapiLogExecution('Debug', 'CreateMasterLPRecord ResultText', ResultText);
	} 
	catch (err) 
	{

	}
	return ResultText;
}

function GetCompanyDUNSnumber(company)
{
	var dunsfilters = new Array();
	var duns='';

	if(company!=null && company!='')
		dunsfilters.push(new nlobjSearchFilter('internalid', null, 'is', company));

	dunsfilters.push(new nlobjSearchFilter('custrecord_compduns', null, 'isnotempty'));
	dunsfilters.push(new nlobjSearchFilter('isinactive', null,'is', 'F'));

	var dunscolumns = new Array();
	dunscolumns[0] = new nlobjSearchColumn('custrecord_compduns');			 

	var dunssearchresults = nlapiSearchRecord('customrecord_ebiznet_company', null, dunsfilters, dunscolumns);
	if (dunssearchresults !=null && dunssearchresults !="") 
	{
		duns = dunssearchresults[0].getValue('custrecord_compduns');
		nlapiLogExecution('Debug', 'duns', duns);
	}
	return duns;
}

function updateWtInLPMaster(containerlp){
	nlapiLogExecution('Debug', 'In to updateWtInLPMaster...',containerlp);
	var recordId="";

	var filtersSO = new Array();
	filtersSO.push(new nlobjSearchFilter('name', null, 'is', containerlp));
	filtersSO.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_sscc', null, 'isempty'));

	var colsSO = new Array();
	colsSO[0] = new nlobjSearchColumn('name');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersSO, colsSO);

	if(searchresults){
		var ResultText=GenerateLable('1');
		for (var i = 0; i < searchresults.length; i++){

			recordId = searchresults[i].getId();
			nlapiLogExecution('Debug', 'recordId',recordId);

			nlapiSubmitField('customrecord_ebiznet_master_lp', recordId,'custrecord_ebiz_lpmaster_sscc', ResultText);

			nlapiLogExecution('Debug', 'Updated LP Master with SSCC',ResultText);
		}
	}

	nlapiLogExecution('Debug', 'Out of updateWtInLPMaster');
}



//Updating Serial numbers
function UpdateSerialentry(RcId,SerialNo)
{
	try
	{
		nlapiLogExecution('Debug', 'RcId', RcId);
		nlapiLogExecution('Debug', 'SerialNo', SerialNo);
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RcId);
		var  fromLpno=transaction.getFieldValue('custrecord_from_lp_no');
		var  getOrdName=transaction.getFieldValue('name');
		var  getOrdLineNo=transaction.getFieldValue('custrecord_line_no');
		var item=transaction.getFieldValue('custrecord_sku');
		var getEbizOrd=transaction.getFieldValue('custrecord_ebiz_order_no');
		var getContainerLP=transaction.getFieldValue('custrecord_container_lp_no');


		var getSerialNoarr=SerialNo.split(',');
		if(getSerialNoarr.length>0)
		{
			for(var z=0; z< getSerialNoarr.length ;z++)
			{
				var filtersser = new Array();
				filtersser[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getSerialNoarr[z]);
				if(item !=null && item!='')
					filtersser[1] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', item);


				var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser);
				for ( var i = 0; i < SrchRecord.length; i++) {
					var searchresultserial = SrchRecord[i];
					var vserialid = searchresultserial.getId();
					//nlapiLogExecution('Debug', 'vserialid', vserialid);
				}

				var fields = new Array();
				var values = new Array();
				fields[0] = 'custrecord_serialwmsstatus';
				fields[1] = 'custrecord_serialebizsono';
				fields[2] = 'custrecord_serialsono';
				fields[3] = 'custrecord_serialsolineno';
				fields[4] = 'custrecord_serialparentid';
				fields[5] = 'custrecord_serialitem';
				values[0] = '8';//STATUS.OUTBOUND.PICK_CONFIRMED
				values[1] = getEbizOrd;
				values[2] = getOrdName;
				values[3] = getOrdLineNo;
				values[4] = getContainerLP;
				values[5] = item;

				var updatefields = nlapiSubmitField('customrecord_ebiznetserialentry',vserialid, fields, values);

			}
		}
	}
	catch (e) 
	{
		nlapiLogExecution('Debug', 'Exception in Updating Serial',e);
	}
}

function GetOrdDetails(getWaveNo,vZoneId)
{
	var SOFilters = new Array();
	if(getWaveNo != null && getWaveNo != '')
	{	 
		nlapiLogExecution('DEBUG', 'getWaveNo inside if', getWaveNo); 
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(getWaveNo)));
	}

	if(vZoneId != null && vZoneId != '')
	{	
		nlapiLogExecution('DEBUG', 'vZoneId inside if', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));
	}
	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));//pick gen
	var SOColumns = new Array();

	//SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no',null,'group'));			 
	SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq',null,'group'));
	SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group'));
	SOColumns.push(new nlobjSearchColumn('custrecord_sku',null,'group'));
	SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty',null,'sum'));
	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc',null,'group'));		
	SOColumns.push(new nlobjSearchColumn('custrecord_skudesc',null,'group'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group')); 
	SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no',null,'group')); 
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid',null,'group')); 
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_wave_no',null,'group'));
	SOColumns.push(new nlobjSearchColumn('custrecord_wms_location',null,'group'));
	SOColumns.push(new nlobjSearchColumn('custrecord_comp_id',null,'group'));
	SOColumns.push(new nlobjSearchColumn('custrecord_container',null,'group'));

	SOColumns[0].setSort();
	SOColumns[1].setSort();
	SOColumns[2].setSort();
	SOColumns[3].setSort(3);
	SOColumns[4].setSort();
	SOColumns[5].setSort();

	var SOSearchResults1 = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	nlapiLogExecution('DEBUG', 'SOSearchResults1', SOSearchResults1);
	return SOSearchResults1;
}
