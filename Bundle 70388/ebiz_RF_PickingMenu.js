/***************************************************************************
  		   eBizNET Solutions Inc                
 ***************************************************************************
 **     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/ebiz_RF_PickingMenu.js,v $
 *     	   $Revision: 1.2.4.11.4.8.4.13 $
 *     	   $Date: 2015/07/09 13:01:31 $
 *     	   $Author: snimmakayala $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_PickingMenu.js,v $
 * Revision 1.2.4.11.4.8.4.13  2015/07/09 13:01:31  snimmakayala
 * Case#: 201413420
 * Wave List and Order List options.
 *
 * Revision 1.2.4.11.4.8.4.9.2.4  2015/07/09 12:57:38  snimmakayala
 * Case#: 201413420
 * Wave List and Order List options.
 *
 * Revision 1.2.4.11.4.8.4.9.2.3  2014/09/16 14:46:26  skreddy
 * case # 201410363
 *  TPP SB issue fix
 *
 * Revision 1.2.4.11.4.8.4.9.2.2  2014/08/13 10:50:44  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Fast picking
 *
 * Revision 1.2.4.11.4.8.4.9.2.1  2014/08/12 16:33:37  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Fast picking
 *
 * Revision 1.2.4.11.4.8.4.9  2014/06/13 12:26:18  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.2.4.11.4.8.4.8  2014/06/12 15:25:51  skavuri
 * Case # 20148880 (LinkButton functionality added to options in RF Screens)
 *
 * Revision 1.2.4.11.4.8.4.7  2014/05/30 00:41:05  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.2.4.11.4.8.4.6  2014/01/30 14:03:45  schepuri
 * 20127002
 * Standard Bundle Issue
 *
 * Revision 1.2.4.11.4.8.4.5  2013/08/15 01:43:06  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201215000
 * RF Fast Picking
 *
 * Revision 1.2.4.11.4.8.4.4  2013/07/19 08:18:22  gkalla
 * Case# 20123527
 * Bulk picking CR for Nautilus
 *
 * Revision 1.2.4.11.4.8.4.3  2013/06/11 14:30:20  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.2.4.11.4.8.4.2  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.2.4.11.4.8.4.1  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.2.4.11.4.8  2012/10/04 10:27:54  grao
 * CASE201112/CR201113/LOG201121
 * Converting multilanguage with given suggestions
 *
 * Revision 1.2.4.11.4.7  2012/10/04 06:29:26  grao
 * no message
 *
 * Revision 1.2.4.11.4.6  2012/10/04 06:20:43  grao
 * no message
 *
 * Revision 1.2.4.11.4.5  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.2.4.11.4.4  2012/09/25 13:39:15  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.2.4.11.4.3  2012/09/25 13:34:51  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.2.4.11.4.2  2012/09/25 07:13:53  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Language
 *
 * Revision 1.2.4.11.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.2.4.11  2012/09/14 20:12:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production Issue Fixes for FISK and BOOMBAH.
 *
 * Revision 1.2.4.6  2012/06/25 14:54:17  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 * Revision 1.2.4.5  2012/06/21 10:15:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 * Revision 1.2.4.4  2012/05/24 15:28:56  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to invalid option entry.
 *
 * Revision 1.2.4.3  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.2.4.2  2012/02/23 18:24:09  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.4  2012/02/23 13:08:07  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.3  2012/02/17 13:32:31  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 */

function PickingMenu(request, response)
{
	if (request.getMethod() == 'GET') 
	{	
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4,st5,st6,st7;
		if( getLanguage == 'es_ES')
		{
			st0 = "PICKING MEN&#218;";
			st1 = "RECOLECCI&#211;N DE ORDENES";  
			st2 = "RECOLECCI&#211;N EN BLOQUE";
			st3 = "INVERSI&#211;N DE ALEJAMIENTO";
			st4 = "INGRESAR SELECCI&#211;N";			
			st5 = "ENVIAR";
			st6 = "ANTERIOR";
			st7 = 'PICKING GRANEL';
			//st8 = "FAST PICKING";
			st9 = "ORDER LIST";
			st10 = "WAVE LIST";

		}
		else
		{
			st0 = "PICKING MENU";
			st1 = "ORDER PICKING";
			st2 = "CLUSTER PICKING";
			st3 = "OUTBOUND REVERSAL";
			st4 = "ENTER SELECTION";
			st5 = "SEND"; 
			st6 = "PREV";
			st7 = "BULK PICKING";
			//st8 = "FAST PICKING";
			st9 = "ORDER LIST";
			st10 = "WAVE LIST";
		}
		var FastPicking = 'Y';
		//Case# 20148880 (LinkButton Functionality)
		var checkInURL_1 = nlapiResolveURL('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di')+"&custparam_language=" + getLanguage + "&custparam_fastpick=" + FastPicking;
		var linkURL_1 = checkInURL_1; 

		var checkInURL_2 = nlapiResolveURL('SUITELET', 'customscript_rf_picking_cluster_no', 'customdeploy_rf_picking_cluster_no_di');
		var linkURL_2 = checkInURL_2; 
		var checkInURL_3 = nlapiResolveURL('SUITELET', 'customscript_rf_outboundreversalmenu', 'customdeploy_rf_outboundreversalmenu');
		var linkURL_3 = checkInURL_3; 
		var checkInURL_4 = nlapiResolveURL('SUITELET', 'customscript_rf_bulkpick_container', 'customdeploy_rf_bulkpick_container_di');
		var linkURL_4 = checkInURL_4; 
		var checkInURL_5 = nlapiResolveURL('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di');
		var linkURL_5 = checkInURL_5; 

		var checkInURL_6 = nlapiResolveURL('SUITELET', 'customscript_rf_picking_orderlist', 'customdeploy_rf_picking_orderlist');
		var linkURL_6 = checkInURL_6; 

		var checkInURL_7 = nlapiResolveURL('SUITELET', 'customscript_rf_picking_wavelist', 'customdeploy_rf_picking_wavelist');
		var linkURL_7 = checkInURL_7; 

		var functionkeyHtml=getFunctionkeyScript('_rfpickingmenu'); 
		var html = "<html><head><title>" + st0 +  "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//html = html + " document.getElementById('selectoption').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rfpickingmenu' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 1. " + st1;
		html = html + "				<td align = 'left'> 1. <a href='" + linkURL_1 + "' style='text-decoration: none'>" + st1 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 2. " + st2;
		html = html + "				<td align = 'left'> 2. <a href='" + linkURL_2 + "' style='text-decoration: none'>" + st2 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> ";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 3. " + st3;
		html = html + "				<td align = 'left'> 3. <a href='" + linkURL_3 + "' style='text-decoration: none'>" + st3 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> ";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 4. " + st7;
		html = html + "				<td align = 'left'> 4. <a href='" + linkURL_4 + "' style='text-decoration: none'>" + st7 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> ";
		//html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 5. " + st8;
		//html = html + "				<td align = 'left'> 5. <a href='" + linkURL_5 + "' style='text-decoration: none'>" + st8 + "</a>";
		//html = html + "				</td>";
		//html = html + "			</tr> ";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 6. " + st9;
		html = html + "				<td align = 'left'> 5. <a href='" + linkURL_6 + "' style='text-decoration: none'>" + st9 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 7. " + st10;
		html = html + "				<td align = 'left'> 6. <a href='" + linkURL_7 + "' style='text-decoration: none'>" + st10 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st4;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='selectoption' id='selectoption' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st5 + "<input id='cmdSend' type='submit' value='ENT'/>";
		html = html + "				"	+ st6 + "<input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('selectoption').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else
	{
		nlapiLogExecution('DEBUG', 'Into Response','Into Response');

		var optedField = request.getParameter('selectoption');
		var optedEvent = request.getParameter('cmdPrevious');

		var SOarray=new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		SOarray["custparam_language"] = getLanguage;

		nlapiLogExecution('DEBUG', 'getLanguage', SOarray["custparam_language"]);

		var st7,st8;
		if( getLanguage == 'es_ES')
		{
			st7 = "OPCI&#211;N V&#193;LIDA ";
			st8 = "SELECCI&#211;N MEN&#220;";
		}
		else
		{
			st7 = "INVALID OPTION";
			st8 = "PICK MENU";
		}


		if (request.getParameter('cmdPrevious') == 'F7') {
			nlapiLogExecution('DEBUG', 'Clicked on Previous', request.getParameter('cmdPrevious'));
			// caae no start 20127002 
			response.sendRedirect('SUITELET', 'customscript_rf_main_menu', 'customdeploy_rf_main_menu_di', false, SOarray);
			return;
			// caae no end 20127002 
		}
		else 
			if (optedField == '1') {
				//response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, SOarray);
				SOarray["custparam_fastpick"] = 'Y';
				response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, SOarray);
			}
			else 
				if (optedField == '2') {
					response.sendRedirect('SUITELET', 'customscript_rf_picking_cluster_no', 'customdeploy_rf_picking_cluster_no_di', false, SOarray);
				}
				else 
					if (optedField == '3') {
						response.sendRedirect('SUITELET', 'customscript_rf_outboundreversalmenu', 'customdeploy_rf_outboundreversalmenu', false, SOarray);
					}
					else 
						if (optedField == '4') {
							response.sendRedirect('SUITELET', 'customscript_rf_bulkpick_container', 'customdeploy_rf_bulkpick_container_di', false, SOarray);
						}
		//else 
		//if (optedField == '5') {
		//SOarray["custparam_fastpick"] = 'Y';
		//response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, SOarray);
		//}
						else 
							if (optedField == '5') {
								response.sendRedirect('SUITELET', 'customscript_rf_picking_orderlist', 'customdeploy_rf_picking_orderlist', false, SOarray);
							}
							else 
								if (optedField == '6') {
									response.sendRedirect('SUITELET', 'customscript_rf_picking_wavelist', 'customdeploy_rf_picking_wavelist', false, SOarray);
								}
								else
								{

									SOarray["custparam_screenno"] = st8;
									SOarray["custparam_error"] = st7;
									SOarray["custparam_language"] = getLanguage;
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								}
		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}
}
