/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_CycleCountConfirmation.js,v $
 *     	   $Revision: 1.7.4.4.4.8.2.25.2.2 $
 *     	   $Date: 2015/11/16 15:35:13 $
 *     	   $Author: rmukkera $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_152 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_CycleCountConfirmation.js,v $
 * Revision 1.7.4.4.4.8.2.25.2.2  2015/11/16 15:35:13  rmukkera
 * case # 201415115
 *
 * Revision 1.7.4.4.4.8.2.25.2.1  2015/11/14 15:19:22  rrpulicherla
 * 2015.2 issues
 *
 * Revision 1.7.4.4.4.8.2.25  2015/07/17 15:27:36  skreddy
 * Case# 201413407
 *  SW SB issue fix
 *
 * Revision 1.7.4.4.4.8.2.24  2014/06/13 10:14:28  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.7.4.4.4.8.2.23  2014/06/06 12:18:01  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Sonic changes
 *
 * Revision 1.7.4.4.4.8.2.22  2014/06/06 07:39:06  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.7.4.4.4.8.2.21  2014/05/30 00:34:19  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.7.4.4.4.8.2.20  2014/01/09 14:52:32  rmukkera
 * Case # 20126718
 *
 * Revision 1.7.4.4.4.8.2.19  2014/01/06 13:16:03  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.7.4.4.4.8.2.18  2013/12/19 15:25:31  nneelam
 * case# 20126283
 * Serial no Issue in Resolve Fix.
 *
 * Revision 1.7.4.4.4.8.2.17  2013/11/01 12:52:28  rmukkera
 * Case# 20125424
 *
 * Revision 1.7.4.4.4.8.2.16  2013/10/29 15:18:58  grao
 * Issue fix related to the 20125354
 *
 * Revision 1.7.4.4.4.8.2.15  2013/10/18 15:47:00  skreddy
 * Case# 20124594
 * Affosa SB  issue fix
 *
 * Revision 1.7.4.4.4.8.2.14  2013/09/23 07:12:55  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related case#201216858 is resolved.
 *
 * Revision 1.7.4.4.4.8.2.13  2013/09/16 15:37:36  rmukkera
 * Case# 20124313
 *
 * Revision 1.7.4.4.4.8.2.12  2013/08/30 16:41:08  skreddy
 * Case# 20124147
 * standard bundle issue fix
 *
 * Revision 1.7.4.4.4.8.2.11  2013/08/21 16:02:12  rmukkera
 * Issue Fix related to 20123999�
 *
 * Revision 1.7.4.4.4.8.2.10  2013/08/05 15:04:15  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * BB Cycle count issues
 * case#20123755
 *
 * Revision 1.7.4.4.4.8.2.9  2013/08/03 21:16:08  snimmakayala
 * Case# 201214994
 * Cycle Count Issue Fixes
 *
 * Revision 1.7.4.4.4.8.2.8  2013/08/02 15:35:20  rmukkera
 * add new item related changes
 *
 * Revision 1.7.4.4.4.8.2.7  2013/06/05 12:05:10  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.7.4.4.4.8.2.6  2013/05/08 15:06:16  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.7.4.4.4.8.2.5  2013/05/01 01:15:24  kavitha
 * CASE201112/CR201113/LOG201121
 * TSG Issue fixes
 *
 * Revision 1.7.4.4.4.8.2.4  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.7.4.4.4.8.2.3  2013/04/03 01:57:27  kavitha
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.7.4.4.4.8.2.2  2013/03/22 11:45:05  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.7.4.4.4.8.2.1  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.7.4.4.4.8  2013/02/20 22:30:57  kavitha
 * CASE201112/CR201113/LOG201121
 * Cycle count process - Issue fixes
 *
 * Revision 1.7.4.4.4.7  2013/02/14 01:35:04  kavitha
 * CASE201112/CR201113/LOG201121
 * Cycle count process - Issue fixes
 *
 * Revision 1.7.4.4.4.6  2013/02/01 07:58:15  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Cyclecountlatestfixed
 *
 * Revision 1.7.4.4.2.1  2012/10/23 23:12:13  spendyala
 * CASE201112/CR201113/LOG201121
 * Record Does not exist issue fixed.
 *
 * Revision 1.7.4.4  2012/04/02 08:24:32  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to cycle count batch item was fixed.
 *
 * Revision 1.7.4.3  2012/03/20 15:10:05  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to disabling the button is resolved.
 *
 * Revision 1.7.4.2  2012/03/16 14:08:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.7.4.1  2012/02/21 13:22:38  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.8  2012/02/21 11:46:49  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 */
function CycleCountConfirmation(request, response){
	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);
	if (request.getMethod() == 'GET') {

		var getPlanNo = request.getParameter('custparam_planno');
		var getRecordId = request.getParameter('custparam_recordid');
		var getBeginLocationId = request.getParameter('custparam_begin_location_id');
		var getBeginLocationName = request.getParameter('custparam_begin_location_name');
		var getPackCode = request.getParameter('custparam_packcode');
		var getExpectedQuantity = request.getParameter('custparam_expqty');
		var getExpLPNo = request.getParameter('custparam_lpno');
		var getActLPNo = request.getParameter('custparam_actlp');
		var getExpItem = request.getParameter('custparam_expitem');
		var getExpItemDescription = request.getParameter('custparam_expitemdescription');
		var getActualItem = request.getParameter('custparam_actitem');
		var getActualPackCode = request.getParameter('custparam_actitem');
		var getActualQuantity = request.getParameter('custparam_actualqty');
		var getActualBatch=request.getParameter('custparam_actbatch');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');		
		nlapiLogExecution('DEBUG', 'getBeginLocation', NextLocation);
		nlapiLogExecution('DEBUG', 'RecordCount', RecordCount);
		var Confirmationflag=request.getParameter('custparam_confmsg');	
		nlapiLogExecution('ERROR', 'Confirmationflag', Confirmationflag);
		var getSerialNo=request.getParameter('custparam_serialno');
		var planitem=request.getParameter('custparam_planitem');
		var getActualItemInternamId = request.getParameter('custparam_expiteminternalid');
		var vfetchexpserialno = nlapiLookupField('customrecord_ebiznet_cyclecountexe', getRecordId, 'custrecord_cycle_expserialno');
		var fetchexpserialno='';
		if(vfetchexpserialno!=null && vfetchexpserialno!= '')
		{
			nlapiLogExecution('ERROR','fetchexpserialno in get',fetchexpserialno);
			fetchexpserialno = vfetchexpserialno.custrecord_cycle_expserialno;
			nlapiLogExecution('ERROR','fetchexpserialno in get',fetchexpserialno);
		}
		
		//Case # 20124594 : start : spanish language conversion
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
		
		var st0,st1,st2,st3,st4,st5,st6,st7,st8,st9;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st0 = "RECUENTO DE CICLO";
			st1 = "CONFIRMACI?N";
			st2 = "ACTUALMENTE NO EXISTE INVENTARIO";
			st3 = "Realizar un inventario se recoge o se mueve";
			st4 = "A ALGUNOS OTROS BIN UBICACI?N";
            st5 = "CICLO DE CONTAR DESDE PLAN DE PRENSA";
            st6 = "CONFIRMAR n?mero de procesos";
            st7 = "CONFIRMAR";
            st8 = "ANTERIOR";
            st9 = "Confirmar y Agregar nuevo elemento";

		}
		else
		{
			st0 = "CYCLE COUNT";
			st1 = "CONFIRMATION";
			st2 = "CURRENTLY INVENTORY DOES NOT EXIST";
			st3 = "EITHER INVENTORY IS PICKED OR MOVED";
			st4 = "TO SOME OTHER BIN LOCATION";
			st5 = "SINCE CYCLE COUNT PLAN RELEASE";
            st6 = "CONFIRM COUNT PROCESS";
            st7 = "CONFIRM";
            st8 = "PREV";
            st9 = "CONFIRM & ADD NEW ITEM";
		}
		
		//case # 20124594 end
		


		var functionkeyHtml=getFunctionkeyScript('_rf_cyclecountconfirmation'); 
		var html = "<html><head><title>CYCLE COUNT</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";     
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		
		//html = html + " document.getElementById('cmdSend').focus();";  
		
		
		
		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		//html = html + "	  alert(document.getElementById('cmdSend').disabled);";

		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cyclecountconfirmation' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		if(Confirmationflag=='true')
		{
			html = html + "				<td align = 'left'>" + st0  +   " </td></tr>";
			html = html + "				<tr><td align = 'left'>" +  st1   +   "<label>";
		}
		else
		{
			html = html + "				<td align = 'left'>" +  st2 +    "</td></tr>";
			html = html + "				<tr><td align = 'left'>" +   st3    +   "</td></tr>";
			html = html + "				<tr><td align = 'left'>" + st4  +  "</td></tr>";
			html = html + "				<tr><td align = 'left'>" + st5 + "</td></tr>";
		}
		html = html + "				<tr><td><input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
		html = html + "				<input type='hidden' name='hdnCycleCountPlanNo' value=" + getPlanNo + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationName' value=" + getBeginLocationName + ">";
		html = html + "				<input type='hidden' name='hdnRecordId' value=" + getRecordId + ">";
		html = html + "				<input type='hidden' name='hdnPackCode' value=" + getPackCode + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnExpLPNo' value=" + request.getParameter('custparam_lpno') + "></td>";
		html = html + "				<input type='hidden' name='hdnActLPNo' value=" + request.getParameter('custparam_actlpno') + "></td>";
		html = html + "				<input type='hidden' name='hdnExpectedItemDescription' value=" + getExpItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnActualItem' value=" + getActualItem + ">";
		html = html + "				<input type='hidden' name='hdnActualPackCode' value=" + getActualPackCode + ">";
		html = html + "				<input type='hidden' name='hdnActualQuantity' value=" + getActualQuantity + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnnext' value=" + NextLocation + ">";
		html = html + "				<input type='hidden' name='hdngetSerialNo' value=" + getSerialNo + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				<input type='hidden' name='hdnflag'>";
		html = html + "				<input type='hidden' name='hdniteminternalid' value=" + getActualItemInternamId + ">";
		html = html + "				<input type='hidden' name='hdnplanitem' value='" + planitem + "'>";		
		html = html + "				</td>";
		html = html + "			</tr>";
		if(Confirmationflag=='true')
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>F8 - " + st6 +  "";
			html = html + "				</td>";
			html = html + "			</tr>";
		}
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'>CONFIRM <input name='cmdSend' type='submit' value='F8' onclick='this.form.hdnflag.value=this.value;this.form.submit();this.disabled=true; return false'/>";
		//Case Start 20125354� Label name changed
		html = html + "				<td align = 'left'>" + st7 + " <input name='cmdSend' id='cmdSend' type='submit' value='F8' onclick='this.form.hdnflag.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		//end
		//html = html + "				<td align = 'left'>CONFIRM <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					" + st8 + " <input name='cmdPrevious' type='submit' value='F7'/>";


		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				" + st9 + " <input name='cmdaddnewitem' type='submit' value='F9'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('cmdSend').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		var CCarray = new Array();
		CCarray["custparam_screenno"] = '34';
		var optedEvent = request.getParameter('cmdPrevious');
		var optedEventSend = request.getParameter('hdnflag');
		var optedEventAddNewItem = request.getParameter('cmdaddnewitem');

		var getRecordId = request.getParameter('custparam_recordid');
		var getPlanNo = request.getParameter('custparam_planno');
		var getBeginLocationId = request.getParameter('custparam_begin_location_id');
		var getBeginLocationName = request.getParameter('custparam_begin_location_name');
		var getLPNo = request.getParameter('custparam_lpno');
		var getPackCode = request.getParameter('custparam_packcode');
		var getExpQty = request.getParameter('custparam_expqty');
		var getActLP = request.getParameter('custparam_actlp');
		var getExpItem = request.getParameter('custparam_expitem');
		var getExpItemInternalid = request.getParameter('custparam_expiteminternalid');
		var getExpItemDescription = request.getParameter('custparam_expitemdescription');
		var getActualItem = request.getParameter('custparam_actitem');
		var getActualPackCode = request.getParameter('custparam_actpackcode');
		var getActualQty = request.getParameter('custparam_actualqty');
		var getActualBatch = request.getParameter('custparam_actbatch');
		var RecCount = request.getParameter('hdnRecCount');
		var NextShowLocation=request.getParameter('hdnNextLocation');
		nlapiLogExecution('ERROR', 'itemstatus', request.getParameter('custparam_skustatus'));
		var getItemstatus =  request.getParameter('custparam_skustatus');
		nlapiLogExecution('ERROR', 'getActualItemstatus', request.getParameter('custparam_actitemstatus'));
		var getActualItemstatus =  request.getParameter('custparam_actitemstatus');

		var getExceptedQty = request.getParameter('hdnExpectedQuantity');
		var Confirmationflag = request.getParameter('custparam_confmsg');
		nlapiLogExecution('ERROR','Confirmationflag',Confirmationflag);

		var serialno = request.getParameter('hdngetSerialNo');//cpmmented because serail no are fetched below
		//var serialno = '';
		var currentContext = nlapiGetContext();
		var getPlanRecordedby = currentContext.getUser();		
		var getPlanRecordedate = DateStamp();
		var getPlanRecordedtime = TimeStamp();		


		var wmsflag=31;//Record
		if(Confirmationflag=='false')
			wmsflag=22;//Resolve

		nlapiLogExecution('ERROR','getExceptedQty',getExceptedQty);
		nlapiLogExecution('ERROR','serialno in eariler post',serialno);
		var getLanguage = request.getParameter('hdngetLanguage');
		CCarray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR','getLanguage',getLanguage);

		CCarray["custparam_recordid"] = getRecordId;
		CCarray["custparam_planno"] = getPlanNo;
		CCarray["custparam_begin_location_id"] = getBeginLocationId;
		CCarray["custparam_begin_location_name"] = getBeginLocationName;
		CCarray["custparam_lpno"] = getLPNo;
		CCarray["custparam_packcode"] = getPackCode;
		CCarray["custparam_expqty"] = getExpQty;
		CCarray["custparam_actlp"] = getActLP;
		CCarray["custparam_expitem"] = getExpItem;
		CCarray["custparam_expitemdescription"] = getExpItemDescription;
		CCarray["custparam_actitem"] = getActualItem;
		CCarray["custparam_actpackcode"] = getActualPackCode;
		CCarray["custparam_actualqty"] = getActualQty;
		CCarray["custparam_nextlocation"] = NextShowLocation;
		CCarray["custparam_skustatus"] = getItemstatus;
		CCarray["custparam_actitemstatus"] = getActualItemstatus;
		CCarray["custparam_actbatch"] = getActualBatch;
		CCarray["custparam_expiteminternalid"] = request.getParameter('hdniteminternalid');
		CCarray["custparam_recordedby"] = getPlanRecordedby;
		CCarray["custparam_recordedate"] = getPlanRecordedate;
		CCarray["custparam_recordetime"] = getPlanRecordedtime;
		CCarray["custparam_confmsg"] = Confirmationflag;
		CCarray["custparam_planitem"] = request.getParameter('hdnplanitem');
//		case # 20124147  start and end       
		var result=false;

		if (sessionobj!=context.getUser()) {
			try
			{

				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}
				if (optedEvent == 'F7') {					
					nlapiLogExecution('DEBUG', 'Cycle Count Confirmation - F7');
					response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_itemstatus', 'customdeploy_rf_cyclecount_itemstatus_di', false, CCarray);
				}
				else {

			if (optedEventSend == 'F8' || optedEventAddNewItem=='F9') {
				nlapiLogExecution('ERROR', 'into rulevalue', ruleValue);
				var getActualBeginDate = DateStamp();
				var getActualBeginTime = TimeStamp();
				nlapiLogExecution('ERROR', 'getRecordId', getRecordId);
				var ruleValue=GetSystemRuleForLPRequired();
				CCarray["custparam_ruleValue"] = ruleValue;
				nlapiLogExecution('ERROR', 'rulevalue', ruleValue);
				nlapiLogExecution('ERROR', 'getActualItem', getActualItem);
				nlapiLogExecution('ERROR', 'getActLP', getActLP);
				nlapiLogExecution('ERROR', 'getRecordId', getRecordId);

				var serialno = '';
				var filtersser = new Array();
				filtersser[0] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', getActualItem);
				filtersser[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', getActLP);
				filtersser[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'C');

				var columnser = new Array();
				columnser[0] = new nlobjSearchColumn('custrecord_serialnumber');

				var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser, columnser);
				if(SrchRecord != null && SrchRecord != '' && SrchRecord.length > 0)
				{
					nlapiLogExecution('ERROR', 'SrchRecord.length', SrchRecord.length);
					for (var a = 0; a < SrchRecord.length; a++) {

						nlapiLogExecution('ERROR', 'SrchRecord[a].getId()', SrchRecord[a].getId());
						nlapiLogExecution('ERROR', 'custrecord_serialnumber', SrchRecord[a].getValue('custrecord_serialnumber'));

						var customrecord= nlapiLoadRecord('customrecord_ebiznetserialentry', SrchRecord[a].getId());
						var fields = new Array();
						var values = new Array();
						fields[0] = 'custrecord_serialstatus';
						//case #  20126283 Updating with I status.
						values[0] = 'I';
						//End
						var updatefields = nlapiSubmitField('customrecord_ebiznetserialentry',SrchRecord[a].getId(), fields, values);

						if(serialno == "" || serialno == null)
						{
							serialno = SrchRecord[a].getValue('custrecord_serialnumber');
						}
						else
						{
							serialno = serialno +","+ SrchRecord[a].getValue('custrecord_serialnumber');
						}
					}
				}
				nlapiLogExecution('ERROR', 'final serialno', serialno);
				var isfromAddnewItemScreen='F';

				if(getRecordId!=null && getRecordId!='')
				{
					var record=nlapiLoadRecord('customrecord_ebiznet_cyclecountexe',getRecordId);
					nlapiLogExecution('ERROR', 'record', record);
					if(record!=null && record!='')
					{
						var notes=record.getFieldValue('custrecord_cycle_notes');
						var expectedItem=record.getFieldValue('custrecord_cyclesku');
						if(expectedItem==getActualItem)
						{
							getItemstatus=record.getFieldValue('custrecord_expcyclesku_status');
						}
						nlapiLogExecution('ERROR', 'notes', notes);
						if(notes=='fromaddnewitem')
						{
							isfromAddnewItemScreen='T';	
						}
					}
				}
				nlapiLogExecution('ERROR', 'isfromAddnewItemScreen', isfromAddnewItemScreen);
				if(ruleValue=='Y' || isfromAddnewItemScreen=='T')
				{
					result = CycleCountRecord(getRecordId, getPlanNo, getActualBeginDate, getActualBeginTime, getBeginLocationId, 
							getBeginLocationName, getActualQty, getActLP, getActualPackCode, getActualItem,getActualItemstatus,
							getActualBatch,getExpQty,wmsflag,serialno,getPlanRecordedate,getPlanRecordedtime,getPlanRecordedby,isfromAddnewItemScreen);
				}
				else
				{
					nlapiLogExecution('ERROR', 'getExpItem', getExpItem);

					var batchNo="";
					var getExpSKU='';
					if(CCRec!=null)
					{
						if(CCRec.getFieldValue('custrecord_expcycleabatch_no') != null && CCRec.getFieldValue('custrecord_expcycleabatch_no') != "")
						{
							batchNo=CCRec.getFieldValue('custrecord_expcycleabatch_no');
							 getExpSKU = CCRec.getFieldValue('custrecord_cyclesku');
							nlapiLogExecution('ERROR','batchNo',batchNo);
						}
					}
					if(getExpItem != null && getExpItem != "")
					{
					//	var getExpSKU = CCRec.getFieldValue('custrecord_cyclesku');
						nlapiLogExecution("ERROR","getExpItemInternalid",getExpItemInternalid);
						var getExpSKU=getExpItemInternalid;
						var filters = new Array();
						filters[0] = new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto', getPlanNo);
						filters[1] = new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [20]);
						filters[2]=new nlobjSearchFilter('custrecord_cycact_beg_loc', null, 'anyof', getBeginLocationId);
						if(getExpSKU!=null && getExpSKU!='')
							filters[3]=new nlobjSearchFilter('custrecord_cyclesku', null, 'anyof',getExpSKU);
						if(batchNo != null && batchNo != "")
						{
							filters[4]=new nlobjSearchFilter('custrecord_expcycleabatch_no', null, 'is', batchNo);
						}
						var skucols=new Array();
						skucols[0]=new nlobjSearchColumn('custrecord_cycleexp_qty');
						skucols[1]=new nlobjSearchColumn('custrecord_cyclelpno');
						skucols[2]=new nlobjSearchColumn('custrecord_expcyclepc');
						skucols[3]=new nlobjSearchColumn('custrecord_expcycleabatch_no');
						skucols[4]=new nlobjSearchColumn('custrecord_cyc_skip_task').setSort(true);
						skucols[5]=new nlobjSearchColumn('custrecord_cyclesku');
						skucols[6]=new nlobjSearchColumn('custrecord_expcyclesku_status');
						var SkuSearchResults=nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, skucols);
						if(SkuSearchResults!=null && SkuSearchResults!='' && SkuSearchResults.length>0)
						{
							var qty=0;
							for(var j=0;j<SkuSearchResults.length;j++)
							{
								getRecordId=SkuSearchResults[j].getId();
								qty=SkuSearchResults[j].getValue('custrecord_cycleexp_qty');
								if(parseFloat(getActualQty)>0)
								{
									var tempqty=parseFloat(SkuSearchResults[j].getValue('custrecord_cycleexp_qty'));
									if((j!=SkuSearchResults.length-1) && (tempqty<=parseFloat(getActualQty)))
									{

										qty=SkuSearchResults[j].getValue('custrecord_cycleexp_qty');

									}
									else
									{
										qty=parseFloat(getActualQty);

											}
										}
										else
										{
											qty=0;
										}
										var getActLP=SkuSearchResults[j].getValue('custrecord_cyclelpno');
										var getActualPackCode=SkuSearchResults[j].getValue('custrecord_expcyclepc');
										//var getExpQty=SkuSearchResults[j].getValue('custrecord_cycleexp_qty');
										var expectedItem=SkuSearchResults[j].getValue('custrecord_cyclesku');
										if(expectedItem==getActualItem)
										{
											getItemstatus=record.getFieldValue('custrecord_expcyclesku_status');
										}

								result = CycleCountRecord(getRecordId, getPlanNo, getActualBeginDate, getActualBeginTime, getBeginLocationId, 
										getBeginLocationName, qty, getActLP, getActualPackCode, getActualItem,getActualItemstatus,
										getActualBatch,getExpQty,wmsflag,serialno,getPlanRecordedate,getPlanRecordedtime,getPlanRecordedby,
										isfromAddnewItemScreen);

								if(getActualQty>0)
								{
									getActualQty=parseFloat(getActualQty)-qty;
								}
							}
						}
					}
					else
					{
						result = CycleCountRecord(getRecordId, getPlanNo, getActualBeginDate, getActualBeginTime, getBeginLocationId, 
								getBeginLocationName, getActualQty, getActLP, getActualPackCode, getActualItem,getActualItemstatus,
								getActualBatch,getExpQty,wmsflag,serialno,getPlanRecordedate,getPlanRecordedtime,getPlanRecordedby,
								isfromAddnewItemScreen);
					}

				}
						nlapiLogExecution('ERROR', 'result',result);
				if (result == true) {

					if(ruleValue=='N' || optedEventAddNewItem=='F9')
					{
						if(optedEventAddNewItem=='F9')
						{
							CCarray["custparam_planno"] = request.getParameter('custparam_planno');
							CCarray["custparam_begin_location_name"] = getBeginLocationName;
							nlapiLogExecution('ERROR', 'Cycle Count AddnewItem Pressed');
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cyc_addnewitem', 'customdeploy_ebiz_rf_cyc_addnewitem_di', false, CCarray);
						}
						else
						{
							var getRecordId='';
							var filters = new Array();
							filters[0] = new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto', getPlanNo);
							filters[1] = new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [20]);
							filters[2]=new nlobjSearchFilter('custrecord_cycact_beg_loc', null, 'anyof', getBeginLocationId);
							if(getExpSKU!=null && getExpSKU!='')
								filters[3]=new nlobjSearchFilter('custrecord_cyclesku', null, 'anyof', getExpSKU);
							var skucols=new Array();
							skucols[0]=new nlobjSearchColumn('custrecord_cycleexp_qty');
							skucols[1]=new nlobjSearchColumn('custrecord_cyclelpno');
							skucols[2]=new nlobjSearchColumn('custrecord_cyc_skip_task').setSort(true);
							var SkuSearchResults=nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, skucols);


							if(SkuSearchResults!=null && SkuSearchResults!='' && SkuSearchResults.length>0)
							{
								getRecordId=SkuSearchResults[0].getId();						

								var CCRec = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe', getRecordId);
								CCarray["custparam_recordid"] = getRecordId;
								CCarray["custparam_planno"] = request.getParameter('custparam_planno');
								CCarray["custparam_begin_location_id"] =getBeginLocationId; //searchresults.getValue('custrecord_cycact_beg_loc');
								CCarray["custparam_begin_location_name"] = getBeginLocationName;//searchresults.getText('custrecord_cycact_beg_loc');
								CCarray["custparam_lpno"] = CCRec.getFieldValue('custrecord_cyclelpno');
								CCarray["custparam_packcode"] = CCRec.getFieldValue('custrecord_expcyclepc');
								CCarray["custparam_expqty"] = CCRec.getFieldValue('custrecord_cycleexp_qty');
								CCarray["custparam_expitem"] = CCRec.getFieldText('custrecord_cyclesku');
								CCarray["custparam_expitemId"] = CCRec.getFieldValue('custrecord_cyclesku');
								CCarray["custparam_expiteminternalid"] = CCRec.getFieldValue('custparam_expiteminternalid');
								CCarray["custparam_expitemdescription"] = CCRec.getFieldValue('custrecord_cycle_skudesc');
								CCarray["custparam_expitemno"] = CCRec.getFieldValue('custrecord_cyclecount_exp_ebiz_sku_no');
								CCarray["custparam_noofrecords"] = SkuSearchResults.length;	
								response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_lp', 'customdeploy_rf_cyclecount_lp_di', false, CCarray);
							}
							else
							{
								var getRecordId='';
								var filters = new Array();
								filters[0] = new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto', getPlanNo);
								filters[1] = new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [20]);
								filters[2]=new nlobjSearchFilter('custrecord_cycact_beg_loc', null, 'anyof', getBeginLocationId);

								var skucols=new Array();
								skucols[0]=new nlobjSearchColumn('custrecord_cycleexp_qty');
								skucols[1]=new nlobjSearchColumn('custrecord_cyclelpno');
								skucols[2]=new nlobjSearchColumn('custrecord_cyc_skip_task').setSort(true);
								var SkuSearchResults=nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, skucols);

								if(SkuSearchResults!=null && SkuSearchResults!='' && SkuSearchResults.length>0)
								{
									getRecordId=SkuSearchResults[0].getId();						

									var CCRec = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe', getRecordId);
									CCarray["custparam_recordid"] = getRecordId;
									CCarray["custparam_planno"] = request.getParameter('custparam_planno');
									CCarray["custparam_begin_location_id"] =getBeginLocationId; //searchresults.getValue('custrecord_cycact_beg_loc');
									CCarray["custparam_begin_location_name"] = getBeginLocationName;//searchresults.getText('custrecord_cycact_beg_loc');
									CCarray["custparam_lpno"] = CCRec.getFieldValue('custrecord_cyclelpno');
									CCarray["custparam_packcode"] = CCRec.getFieldValue('custrecord_expcyclepc');
									CCarray["custparam_expqty"] = CCRec.getFieldValue('custrecord_cycleexp_qty');
									CCarray["custparam_expitem"] = CCRec.getFieldText('custrecord_cyclesku');
									CCarray["custparam_expitemId"] = CCRec.getFieldValue('custrecord_cyclesku');
									CCarray["custparam_expiteminternalid"] = CCRec.getFieldValue('custparam_expiteminternalid');
									CCarray["custparam_expitemdescription"] = CCRec.getFieldValue('custrecord_cycle_skudesc');
									CCarray["custparam_expitemno"] = CCRec.getFieldValue('custrecord_cyclecount_exp_ebiz_sku_no');
									CCarray["custparam_noofrecords"] = SkuSearchResults.length;	
									response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_sku', 'customdeploy_rf_cyclecount_sku_di', false, CCarray);
								}
								else
								{

									var filters = new Array();
									filters[0] = new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto', getPlanNo);
									filters[1] = new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [20]);


									var skucols=new Array();
									skucols[0]=new nlobjSearchColumn('custrecord_cycleexp_qty');
									skucols[1]=new nlobjSearchColumn('custrecord_cyclelpno');
									skucols[2]=new nlobjSearchColumn('custrecord_cyc_skip_task').setSort(true);
									var SkuSearchResults=nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, skucols);

									if(SkuSearchResults!=null && SkuSearchResults!='' && SkuSearchResults.length>0)
									{
										nlapiLogExecution('ERROR', 'test6', 'test6');
										response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_location', 'customdeploy_rf_cyclecount_location_di', false, CCarray); 
									}
									else
									{
										response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_plan_no', 'customdeploy_rf_cyclecount_plan_no_di', false, CCarray);
									}
								}


							}
						}
					}
					else
					{


						var getRecordId='';
						var filters = new Array();
						filters[0] = new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto', getPlanNo);
						filters[1] = new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [20]);
						filters[2]=new nlobjSearchFilter('custrecord_cycact_beg_loc', null, 'anyof', getBeginLocationId);

						var skucols=new Array();
						skucols[0]=new nlobjSearchColumn('custrecord_cycleexp_qty');
						skucols[1]=new nlobjSearchColumn('custrecord_cyclelpno');
						skucols[2]=new nlobjSearchColumn('custrecord_cyc_skip_task').setSort(true);
						var SkuSearchResults=nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, skucols);

						if(SkuSearchResults!=null && SkuSearchResults!='' && SkuSearchResults.length>0)
						{
							getRecordId=SkuSearchResults[0].getId();						

							var CCRec = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe', getRecordId);
							CCarray["custparam_recordid"] = getRecordId;
							CCarray["custparam_planno"] = request.getParameter('custparam_planno');
							CCarray["custparam_begin_location_id"] =getBeginLocationId; //searchresults.getValue('custrecord_cycact_beg_loc');
							CCarray["custparam_begin_location_name"] = getBeginLocationName;//searchresults.getText('custrecord_cycact_beg_loc');
							CCarray["custparam_lpno"] = CCRec.getFieldValue('custrecord_cyclelpno');
							CCarray["custparam_packcode"] = CCRec.getFieldValue('custrecord_expcyclepc');
							CCarray["custparam_expqty"] = CCRec.getFieldValue('custrecord_cycleexp_qty');
							CCarray["custparam_expitem"] = CCRec.getFieldText('custrecord_cyclesku');
							CCarray["custparam_expitemId"] = CCRec.getFieldValue('custrecord_cyclesku');
							CCarray["custparam_expiteminternalid"] = CCRec.getFieldValue('custparam_expiteminternalid');
							CCarray["custparam_expitemdescription"] = CCRec.getFieldValue('custrecord_cycle_skudesc');
							CCarray["custparam_expitemno"] = CCRec.getFieldValue('custrecord_cyclecount_exp_ebiz_sku_no');
							CCarray["custparam_noofrecords"] = SkuSearchResults.length;	
							response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_lp', 'customdeploy_rf_cyclecount_lp_di', false, CCarray);
						}
						else
						{

							var filters = new Array();
							filters[0] = new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto', getPlanNo);
							filters[1] = new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [20]);


							var skucols=new Array();
							skucols[0]=new nlobjSearchColumn('custrecord_cycleexp_qty');
							skucols[1]=new nlobjSearchColumn('custrecord_cyclelpno');
							skucols[2]=new nlobjSearchColumn('custrecord_cyc_skip_task').setSort(true);
							var SkuSearchResults=nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, skucols);

									if(SkuSearchResults!=null && SkuSearchResults!='' && SkuSearchResults.length>0)
									{
										response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_location', 'customdeploy_rf_cyclecount_location_di', false, CCarray); 
									}
									else
									{
										nlapiLogExecution('DEBUG', 'Cycle Count Confirmation - F8');
										response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_plan_no', 'customdeploy_rf_cyclecount_plan_no_di', false);//, CCarray);
									}
								}
							}
						}
						else
						{
							response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_plan_no', 'customdeploy_rf_cyclecount_plan_no_di', false, CCarray);
							nlapiLogExecution('DEBUG', 'cyclecount plan');
						}
					}
					else {
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
						nlapiLogExecution('DEBUG', 'Cycle Count Not Confirmed');
					}
				}
			}
			catch (e)  {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
				nlapiLogExecution('DEBUG', 'exception',e);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');

			}

		}
		else
		{
			CCarray["custparam_screenno"] = '28';
			CCarray["custparam_error"] = 'ITEM ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
		}
	}
}


function CycleCountRecord(RecordId, PlanNo, ActualBeginDate, ActualBeginTime, 
		BeginLocationId, BeginLocationName, ActualQty, ActLP, ActualPackCode, 
		ActualItem,ActualStatus,ActualBatch,expqty,wmsflag,serialno,getPlanRecordedate,getPlanRecordedtime,getPlanRecordedby,
		isfromAddnewItemScreen)
{
	try 
	{
		nlapiLogExecution('ERROR', 'RecordId', RecordId);
		nlapiLogExecution('ERROR', 'PlanNo', PlanNo);
		nlapiLogExecution('ERROR', 'ActualQty', ActualQty);
		nlapiLogExecution('ERROR', 'expqty', expqty);

		if(expqty == null || expqty == "")
		{
			expqty = 0;
		}

		var CCFields = new Array();
		var CCValues = new Array();

		CCFields[0] = 'custrecord_act_beg_date';
		CCValues[0] = ActualBeginDate;

		CCFields[1] = 'custrecord_cycact_beg_time';
		CCValues[1] = ActualBeginTime;

		CCFields[2] = 'custrecord_cycleact_end_date';
		CCValues[2] = DateStamp();

		CCFields[3] = 'custrecord_cycact_end_time';
		CCValues[3] = TimeStamp();

		CCFields[4] = 'custrecord_cyclerec_upd_date';
		CCValues[4] = DateStamp();

		CCFields[5] = 'custrecord_cyclerec_upd_time';
		CCValues[5] = TimeStamp();

		CCFields[6] = 'custrecord_cycleact_sku';
		CCValues[6] = ActualItem;

		CCFields[7] = 'custrecord_cycle_act_qty';
		CCValues[7] = ActualQty.toString();

		CCFields[8] = 'custrecord_cycact_lpno';
		CCValues[8] = ActLP;

		CCFields[9] = 'custrecord_cyclestatus_flag';
		CCValues[9] = 31;

		CCFields[10] = 'custrecord_cyclepc';
		CCValues[10] = ActualPackCode;

		CCFields[11] = 'custrecord_cyclesku_status';
		CCValues[11] = ActualStatus;

		CCFields[12] = 'custrecord_cycleabatch_no';
		CCValues[12] = ActualBatch;

		CCFields[13] = 'custrecord_cyclecount_act_ebiz_sku_no';
		CCValues[13] = ActualItem;

	//	CCFields[14] = 'custrecord_cycleexp_qty';
		//CCValues[14] = expqty;

		CCFields[14] = 'custrecord_cyclestatus_flag';
		CCValues[14] = wmsflag;

		nlapiLogExecution('ERROR', 'serialno', serialno);
		CCFields[15] = 'custrecord_cycle_serialno';
		CCValues[15] = serialno;

		CCFields[16] = 'custrecord_cyccplan_recorddate';
		CCValues[16] = getPlanRecordedate;

		CCFields[17] = 'custrecord_cyccplan_recordtime';
		CCValues[17] = getPlanRecordedtime;

		CCFields[18] = 'custrecord_cyccplan_recordedby';
		CCValues[18] = getPlanRecordedby;

		if(isfromAddnewItemScreen =='T')
		{
			CCFields[19] = 'custrecord_expcyclesku_status';
			CCValues[19] = ActualStatus;
		}

		try 
		{
			nlapiSubmitField('customrecord_ebiznet_cyclecountexe', RecordId, CCFields, CCValues);
			nlapiLogExecution('ERROR', 'After submit', RecordId);
		} 
		catch (error) 
		{
			nlapiLogExecution('ERROR', 'Into Catch Blok', error);
		}

		//*************** The below code is added by Satish.N on 22/07/2013*******************//

		var cyccrecord = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe', RecordId);

		var vinvtid = cyccrecord.getFieldValue('custrecord_invtid');

		if(vinvtid!=null && vinvtid!='')
		{
			var fieldNames = new Array(); 
			fieldNames.push('custrecord_ebiz_invholdflg');  
			fieldNames.push('custrecord_ebiz_cycl_count_hldflag');
			fieldNames.push('custrecord_ebiz_callinv');

			var newValues = new Array(); 
			newValues.push('T');
			newValues.push('T');
			newValues.push('N');

			nlapiSubmitField('customrecord_ebiznet_createinv', vinvtid, fieldNames, newValues);		
		}

		//******************************************Upto here *******************************//

		nlapiLogExecution('ERROR', 'Confirmed', RecordId + '|' + PlanNo + '|' + ActualBeginDate + '|' + ActualBeginTime + 
				'|' + BeginLocationId + '|' + BeginLocationName + '|' + ActualQty + '|' + ActLP + '|' + ActualPackCode + 
				'|' + ActualItem+'|' + ActualBatch+'|' + ActualStatus + '|' + expqty + '|' + serialno);

		return true;
	} 
	catch (e) 
	{
		return false;
	}
}

function GetSystemRuleForLPRequired()
{
	try
	{
		var rulevalue='Y';
		var filter=new Array();
		filter.push(new nlobjSearchFilter('name',null,'is','Is LP Scan required for CycleCount?'));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizrulevalue');

		var searchresult=nlapiSearchRecord('customrecord_ebiznet_sysrules',null,filter,column);

		if(searchresult!=null&&searchresult!="")
			rulevalue=searchresult[0].getValue('custrecord_ebizrulevalue');
		nlapiLogExecution('ERROR','rulevalue',rulevalue);
		return rulevalue;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetSystemRuleForLPRequired',exp);
	}
}
