
function ebiznet_ReceivingReportPDF(request, response){
	if (request.getMethod() == 'GET') {

		var form = nlapiCreateForm('Receiving Report');
		var vQbWave,vQbfullfillmentNo ="";
		var getOrderno=request.getParameter('custparam_orderno');
		var getOrdername=request.getParameter('custparam_ordername');

		nlapiLogExecution('ERROR', 'getOrderno', getOrderno);
		nlapiLogExecution('ERROR', 'getOrdername', getOrdername);

		var trantype = nlapiLookupField('transaction', getOrderno, 'recordType');
		nlapiLogExecution('ERROR', 'trantype', trantype);

		var polinefilters = new Array();
		polinefilters[0] = new nlobjSearchFilter('internalid', null, 'is', getOrderno);			
		polinefilters[1] = new nlobjSearchFilter('billable', null, 'is', 'false');		
		polinefilters[2] = new nlobjSearchFilter('item', null, 'noneof','@NONE@');

		var polinecolumns = new Array();
		polinecolumns[0] = new nlobjSearchColumn('line');
		polinecolumns[1] = new nlobjSearchColumn('item');//,
		polinecolumns[2] = new nlobjSearchColumn('quantity');
		polinecolumns[3] = new nlobjSearchColumn('custcol_ebiznet_item_status');
		polinecolumns[4] = new nlobjSearchColumn('formulanumeric');
		polinecolumns[4].setFormula('ABS({quantity})');
		polinecolumns[5] = new nlobjSearchColumn('description','item');
		polinecolumns[6] = new nlobjSearchColumn('upccode','item');
		polinecolumns[7] = new nlobjSearchColumn('entity');
		polinecolumns[8] = new nlobjSearchColumn('vendorname','item');
		polinecolumns[9] = new nlobjSearchColumn('billaddressee');

		var posearchresults;
		if(trantype=='purchaseorder')
		{
			nlapiLogExecution('ERROR', 'purchaseorder', 'purchaseorder');
			// To filter null value records from the search results
			posearchresults=nlapiSearchRecord('purchaseorder', null, polinefilters, polinecolumns);
		}

		if(trantype=='transferorder')
		{
			nlapiLogExecution('ERROR', 'transferorder', 'transferorder');
			polinefilters[3] = new nlobjSearchFilter('transactionlinetype', null, 'anyof', 'ITEM');
			posearchresults=nlapiSearchRecord('transferorder',null,polinefilters,polinecolumns);
		}

		if(trantype=='returnauthorization')
		{
			nlapiLogExecution('ERROR', 'returnauthorization', 'returnauthorization');
			polinefilters[3] = new nlobjSearchFilter('taxline', null, 'is', 'F');
			posearchresults=nlapiSearchRecord('returnauthorization',null,polinefilters,polinecolumns);
		}

		var replaceChar =/\$|,|@|&|#|~|`|\%|\*|\^|\&|\+|\=|\-|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|\./g;
			var poqty,poitemdesc,poitemupc,poitemname,poitemstatus,poline,povendor,poitemid;
//		case# 201412500
		var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<head><macrolist><macro id='myfooter'><p align='center'>Page <pagenumber/> of <totalpages/></p></macro></macrolist></head><body  font-size=\"7\"  size=\"A4-landscape\"    padding-top=\" 0mm\"   footer='myfooter' footer-height='20mm'>\n";
		if (posearchresults != null)
		{     
			var strxml ="";
			for (var i = 0; i < posearchresults.length; i++)  // Even though PO is having one line, search results are giving two records, assumtion is first record might be expense record 
			{     		
				nlapiLogExecution('ERROR', 'posearchresults.length', posearchresults.length);
				poline = posearchresults[i].getValue('line');
				poitemid = posearchresults[i].getValue('item');
				poqty = posearchresults[i].getValue('formulanumeric');					
				nlapiLogExecution('ERROR', 'poqty', poqty);
				poitemstatus = posearchresults[i].getValue('custcol_ebiznet_item_status');
				poitemdesc = posearchresults[i].getValue('description','item');
				poitemupc = posearchresults[i].getValue('upccode','item');
				povendor = posearchresults[i].getValue('billaddressee');

				/*nlapiLogExecution('ERROR', 'SKU Status', itemstatus);
				nlapiLogExecution('ERROR', 'vorderid', vorderid);*/
				nlapiLogExecution('ERROR', 'line', poline);



				var filters = new Array();
				filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', getOrderno));		
				filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_putgen_qty', null, 'greaterthan', 0));
				filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', poline));

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_orderline_no');
				columns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_item');					
				columns[2] = new nlobjSearchColumn('custrecord_orderlinedetails_order_qty');
				columns[3] = new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty');
				columns[4] = new nlobjSearchColumn('custrecord_orderlinedetails_putgen_qty');
				columns[5] = new nlobjSearchColumn('custrecord_orderlinedetails_putconf_qty');
				columns[6] = new nlobjSearchColumn('custrecord_orderlinedetails_putexcep_qty');
				columns[7] = new nlobjSearchColumn('description','custrecord_orderlinedetails_item');
				columns[8] = new nlobjSearchColumn('upccode','custrecord_orderlinedetails_item');	
				columns.push(new nlobjSearchColumn('vendorname','custrecord_orderlinedetails_item'));
				var searchresults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, filters, columns);
				var vendor="";var ordqty="";var putgenqty = "";var pconfqty = "";var premqty = "";var itemname = "";var upccode = "";var recvqty = "";var itemdesc = "";
				

				//date
				var sysdate=DateStamp();
				var systime=TimeStamp();
				var Timez=calcTime('-5.00');
				var datetime= new Date();
				datetime=datetime.toLocaleTimeString() ;
				var finalimageurl = '';

				var url;

				var filefound = nlapiLoadFile('Images/LOGOCOMP.jpg'); 
				if (filefound) 
				{ 
					nlapiLogExecution('ERROR', 'Event', 'file;'+filefound.getId()); 
					var imageurl = filefound.getURL();
					finalimageurl = imageurl;//+';';
					finalimageurl=finalimageurl.replace(/&/g,"&amp;");

				} 
				else 
				{
					nlapiLogExecution('ERROR', 'Event', 'No file;');
				}


				var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<head><macrolist><macro id='myfooter'><p align='center'>Page <pagenumber/> of <totalpages/></p></macro></macrolist></head><body  font-size=\"7\"  size=\"A4-landscape\"    padding-top=\" 0mm\"   footer='myfooter' footer-height='20mm'>\n";


				//	var strxml=strxml+"";
				//	strxml += "<table width='100%' >";
				//var strxml = "<table width='100%' >";// case# 201412500

				var pageno=0;
				if(pageno==0)
				{
					pageno=parseFloat(pageno+1);
				}
// case# 201416317


			/*	strxml += "<tr ><td valign='middle' align='left'><img src='" + finalimageurl + "'></img></td><td valign='middle' align='left'  style='font-size:xx-large;'>";
				strxml += "Receiving Report ";
				strxml += "</td><td align='right'>&nbsp;</td></tr></table>";
				strxml += "<p align='right'>Date/Time:"+Timez+"</p>";

				strxml +="<table align='left' style='width:35%;' border='1'>";
				strxml +="<tr><td align='left' style='width:31px'>PO / TO / RMA # :</td>";

				strxml +="<td>";
				if(getOrdername != null && getOrdername!= "")
				{
					strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
					strxml += getOrdername;
					strxml += "\"/>";
				}
				strxml += "</td></tr>";

				strxml +="<tr><td align='left' style='width:51px'>Vendor Name :</td>";

				strxml +="<td>";
				if(povendor != null && povendor != "")
				{
					//strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
					strxml += povendor;
					//strxml += "\"/>";
				}
				strxml += "</td></tr>";



				strxml +="</table>";

				strxml +="<table  width='100%'>";
				strxml +="<tr style=\"font-weight:bold;background-color:gray;color:white;\">";

				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000'>";
				strxml += " Item Name";
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

				strxml += "<td width='15%' style='border-width: 1px; border-color: #000000'>";
				strxml += "Item Description";
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

				strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
				strxml += "UPC Code";
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";



				strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
				strxml += "Order Qty";
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

				strxml += "<td width='8%' style='border-width: 1px; border-color: #000000'>";//uncommented
				strxml += "Qty Received";
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";	

				strxml += "<td width='3%' style='border-width: 1px; border-color: #000000'>";			
				strxml += "Qty Remaining to be Received";
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";	*/			

			
				if(i==0)
				{
					strxml = "<table width='100%' >";
					var pageno=0;




					strxml += "<tr ><td valign='middle' align='left'><img src='" + finalimageurl + "'></img></td><td valign='middle' align='left'  style='font-size:xx-large;'>";
					strxml += "Receiving Report ";
					strxml += "</td><td align='right'>&nbsp;</td></tr></table>";
					strxml += "<p align='right'>Date/Time:"+Timez+"</p>";

					strxml +="<table align='left' style='width:35%;' border='1'>";
					strxml +="<tr><td align='left' style='width:31px'>PO / TO / RMA # :</td>";

					strxml +="<td>";
					if(getOrdername != null && getOrdername!= "")
					{
						strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
						strxml += getOrdername;
						strxml += "\"/>";
					}
					strxml += "</td></tr>";

					strxml +="<tr><td align='left' style='width:51px'>Vendor Name :</td>";

					strxml +="<td>";
					if(povendor != null && povendor != "")
					{
						//strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
						//strxml += povendor;
						strxml += povendor.replace(/&/g,"&amp;");
						//strxml += "\"/>";
					}
					strxml += "</td></tr>";



					strxml +="</table>";

					strxml +="<table  width='100%'>";
					strxml +="<tr style=\"font-weight:bold;background-color:gray;color:white;\">";

					strxml += "<td width='16%' style='border-width: 1px; border-color: #000000'>";
					strxml += " Item Name";
					strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

					strxml += "<td width='15%' style='border-width: 1px; border-color: #000000'>";
					strxml += "Item Description";
					strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

					strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
					strxml += "UPC Code";
					strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";



					strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
					strxml += "Order Qty";
					strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

					strxml += "<td width='8%' style='border-width: 1px; border-color: #000000'>";//uncommented
					strxml += "Qty Received";
					strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";	

					strxml += "<td width='3%' style='border-width: 1px; border-color: #000000'>";			
					strxml += "Qty Remaining to be Received";
					strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";				

					/*var tempLineNo;var pagetotalno=0;
				if (searchresults != null) {*/
				}

				if(searchresults!=null&&searchresults!=''&&searchresults.length>0)
				{
					nlapiLogExecution('ERROR', 'searchresults.length', searchresults.length);
					var pagetotalno=0;
					for (var i1 = 0; i1 < searchresults.length; i1++) {
						var searchresult = searchresults[i1];
						vendor = searchresult.getValue('vendorname','custrecord_orderlinedetails_item');
						ordqty = searchresult.getValue('custrecord_orderlinedetails_order_qty');
						putgenqty = searchresult.getValue('custrecord_orderlinedetails_putgen_qty');
						pconfqty = searchresult.getValue('custrecord_orderlinedetails_putconf_qty');
						premqty = parseInt(ordqty) - parseInt(putgenqty);
						itemname = searchresult.getValue('custrecord_orderlinedetails_item');
						itemdesc = searchresult.getValue('description','custrecord_orderlinedetails_item');
						upccode = searchresult.getValue('upccode','custrecord_orderlinedetails_item');
						recvqty = searchresult.getValue('custrecord_orderlinedetails_checkin_qty');

					}
				}
				/*nlapiLogExecution('ERROR', 'ordqty', ordqty);
						nlapiLogExecution('ERROR', 'pconfqty', pconfqty);
						nlapiLogExecution('ERROR', 'premqty', premqty);
						nlapiLogExecution('ERROR', 'itemname', itemname);
						nlapiLogExecution('ERROR', 'itemdesc', itemdesc);
						nlapiLogExecution('ERROR', 'upccode', upccode);
						nlapiLogExecution('ERROR', 'vendor', vendor);
						//}
						//}

						nlapiLogExecution('ERROR', 'poqty', poqty);
						nlapiLogExecution('ERROR', 'poitemname', poitemname);
						nlapiLogExecution('ERROR', 'poitemdesc', poitemdesc);
						nlapiLogExecution('ERROR', 'poitemupc', poitemupc);
						nlapiLogExecution('ERROR', 'povendor', povendor);*/

				nlapiLogExecution('ERROR', 'recvqty', recvqty);
				if(recvqty==null || recvqty=='')
					recvqty = 0;

				nlapiLogExecution('ERROR', 'ordqty', ordqty);
				if(ordqty==null || ordqty =='')
					ordqty = poqty;

				premqty = parseInt(ordqty) - parseInt(recvqty);
				nlapiLogExecution('ERROR', 'premqty', premqty);

				nlapiLogExecution('ERROR', 'itemname', itemname);
				if(itemname==null || itemname=='')						
					itemname = poitemid;

				var filters2 = new Array();
				var columns2 = new Array();
				columns2[0] = new nlobjSearchColumn('name');
				filters2.push(new nlobjSearchFilter('internalid', null, 'is', itemname));
				var ItemInfoResults = nlapiSearchRecord('item', null, filters2, columns2);
				if(ItemInfoResults != null && ItemInfoResults != "")
				{
					poitemname = ItemInfoResults[0].getValue('name');
				}

				//if(poitemname != null && poitemname !="")
				//poitemname=poitemname.replace(replaceChar,'');
				nlapiLogExecution('ERROR', 'poitemname', poitemname);



				itemname = poitemname;
				nlapiLogExecution('ERROR', 'itemname here', itemname);
				nlapiLogExecution('ERROR', 'itemdesc', itemdesc);
				if(itemdesc==null || itemdesc=='')						
					itemdesc = poitemdesc;
				nlapiLogExecution('ERROR', 'itemdesc here', itemdesc);

				nlapiLogExecution('ERROR', 'upccode', upccode);
				if(upccode==null || upccode=='')						
					upccode = poitemupc;

				nlapiLogExecution('ERROR', 'upccode here', upccode);
				/*nlapiLogExecution('ERROR', 'vendor', vendor);
				if(vendor==null || vendor=='')						
					vendor = povendor;*/



				var vline, vitem, vqty, vTaskType,vlotbatch, vmainline, vrecid, vlocation, vLpno, vSKU, vlocationid, vinvrefno, vskustatus, vpackcode, vdono,vcontlp,vcontsize,vclusno,vweight,vcaseqty,vnoofcases;




				strxml =strxml+  "<tr>";

				strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
				if(itemname != null && itemname != "")
				{
					itemname = itemname.replace(/"/g,"&#34;");
					strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
					strxml += itemname;
					strxml += "\"/>";
				}
				strxml += "</td>";
				nlapiLogExecution('ERROR', 'itemdesc here1', itemdesc);
				if(itemdesc != null && itemdesc !="")
					itemdesc=itemdesc.replace(replaceChar,'');
				strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
				strxml += itemdesc;
				strxml += "</td>";

				strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
				nlapiLogExecution('ERROR', 'upccode here1', upccode);
				if(upccode != null && upccode != "")
				{
					nlapiLogExecution('ERROR', 'upccode here2', upccode);
					strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
					strxml += upccode;
					strxml += "\"/>";
				}
				strxml += "</td>";

				strxml += "<td width='8%' style='border-width: 1px; border-color: #000000'>";
				strxml += ordqty;
				strxml += "</td>";


				strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
				strxml += recvqty;
				strxml += "</td>";

				strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";//uncommented for value
				strxml += premqty;
				strxml += "</td></tr>";									  

				pagetotalno=parseFloat(pagetotalno)+1;

				//strxml =strxml+"</table>";
				if(i == posearchresults.length-1)
				{			
					nlapiLogExecution('ERROR', 'terminated table', i);
					strxml =strxml+"</table>";
				}

				//}




			}  //Case # 20149589



			strxml =strxml+ "\n</body>\n</pdf>";
			xml=xml +strxml;
			var file = nlapiXMLToPDF(xml);	
			response.setContentType('PDF','Receiving Report.pdf');
			response.write( file.getValue() );

			/*strxml =strxml+"</table>";
				if((distinctSoIds.length-count)>1)
				{
					pageno=parseFloat(pageno)+1;
					strxml=strxml+ "<p style='page-break-after:always'></p>";
				}
				else
				{
					//pageno=parseFloat(pageno)+1;
					//strxml=strxml+ "<p style='vertical-align:bottom;align:right;font-size:9'>Page No: "+pageno+" </p>";
				}*/



			//} //Case # 20149589 
		}
	}
	else //this is the POST block
	{

	}
}



function calcTime(offset) {

	//var vtime=new Date();

	//nlapiLogExecution('ERROR', 'TimeOffset', vtime.getTimezoneOffset());

	// create Date object for current location
	d = new Date();

	// convert to msec
	// add local time zone offset 
	// get UTC time in msec
	utc = d.getTime() + (d.getTimezoneOffset() * 60000);

	// create new Date object for different city
	// using supplied offset
	nd = new Date(utc + (3600000*offset));

	// return time as a string
	//return "The local time in " + city + " is " + nd.toLocaleString();

	var stringDt=((parseFloat(nd.getMonth()) + 1) + '/' + (parseFloat(nd.getDate())) + '/' + nd.getFullYear());

	var timestamp;
	var a_p = "";

	//Getting time in hh:mm tt format.
	var curr_hour = nd.getHours();
	var curr_min = nd.getMinutes();

	// determining the am/pm indicator
	if (curr_hour < 12)
		a_p = "am";
	else
		a_p = "pm";

	// finalizing hours depending on 24hr clock
	if (curr_hour == 0)
		curr_hour = 12;
	else if(curr_hour > 12)
		curr_hour -= 12;

	if (curr_min.length == 1)
		curr_min = "0" + curr_min;

	//Adding fields to update time zones.
	timestamp = curr_hour + ":" + curr_min + " " + a_p;

	return stringDt + " " + timestamp;


}


function vremoveDuplicateElement(arrayName)
{
	var newArray=new Array();
	label:for(var i=0; i<arrayName.length;i++ )
	{  
		for(var j=0; j<newArray.length;j++ )
		{
			if(newArray[j]==arrayName[i]) 
				continue label;
		}
		newArray[newArray.length] = arrayName[i];
	}
	return newArray;
}

