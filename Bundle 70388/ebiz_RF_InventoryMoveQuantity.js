/***************************************************************************
	  		   eBizNET Solutions Inc               
 ****************************************************************************/
/* Prologue - 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_InventoryMoveQuantity.js,v $
 *     	   $Revision: 1.11.2.4.4.3.4.12.2.4 $
 *     	   $Date: 2015/12/02 15:09:14 $
 *     	   $Author: grao $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_InventoryMoveQuantity.js,v $
 * Revision 1.11.2.4.4.3.4.12.2.4  2015/12/02 15:09:14  grao
 * 2015.2 Issue Fixes 201415553
 *
 * Revision 1.11.2.4.4.3.4.12.2.3  2015/11/25 15:36:19  aanchal
 * 2015.2 Issue fix
 * 201415553
 *
 * Revision 1.11.2.4.4.3.4.12.2.2  2015/11/16 07:40:28  gkalla
 * case# 201415553
 * System is allowing to scan decimals for serialized items.
 *
 * Revision 1.11.2.4.4.3.4.12.2.1  2015/11/10 16:40:51  aanchal
 * 2015.2 Issue fix
 * 201415549
 *
 * Revision 1.11.2.4.4.3.4.12  2015/08/21 11:00:44  nneelam
 * case# 201414102
 *
 * Revision 1.11.2.4.4.3.4.11  2015/07/03 13:11:03  schepuri
 * case# 201413339
 *
 * Revision 1.11.2.4.4.3.4.10  2014/12/23 06:41:18  spendyala
 * case # 201411303
 *
 * Revision 1.11.2.4.4.3.4.9  2014/11/05 10:34:08  sponnaganti
 * Case# 201410748
 * True Fab SB Issue fixed
 *
 * Revision 1.11.2.4.4.3.4.8  2014/06/13 09:50:35  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.11.2.4.4.3.4.7  2014/06/06 07:32:40  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.11.2.4.4.3.4.6  2014/05/30 00:34:21  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.11.2.4.4.3.4.5  2014/02/06 14:41:46  skreddy
 * case 20127088
 * Ryonet Prod issue fix
 *
 * Revision 1.11.2.4.4.3.4.4  2013/12/06 15:12:16  skreddy
 * Case# 20126219
 * 2014.1 stnd bundle issue fix
 *
 * Revision 1.11.2.4.4.3.4.3  2013/06/11 16:01:25  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Stdbundle issue fixes
 *
 * Revision 1.11.2.4.4.3.4.2  2013/05/31 15:15:48  grao
 * CASE201112/CR201113/LOG201121
 * PMM Issues fixes
 *
 * Revision 1.11.2.4.4.3.4.1  2013/04/17 16:02:35  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.11.2.4.4.3  2012/11/01 14:55:42  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.11.2.4.4.2  2012/09/26 12:36:25  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.11.2.4.4.1  2012/09/21 15:01:30  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.11.2.4  2012/09/13 12:46:35  spendyala
 * CASE201112/CR201113/LOG201121
 * Correcting the spelling while populating to user.
 *
 * Revision 1.11.2.3  2012/03/16 14:08:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.11.2.2  2012/02/23 00:27:34  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * supress lp in Invtmove and lp merge based on fifodate
 *
 * Revision 1.11.2.1  2012/02/07 12:52:23  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Inventory Move Changes
 *
 * Revision 1.14  2012/01/20 01:10:19  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Inventory Move Changes
 *
 * Revision 1.13  2012/01/19 14:48:32  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Invt Move
 *
 * Revision 1.12  2012/01/13 23:58:56  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Invt Move
 *
 * Revision 1.11  2011/12/26 22:40:12  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Inventory Move Changes
 *
 * Revision 1.9  2011/09/28 13:19:58  mbpragada
 * CASE201112/CR201113/LOG201121
 *

 *****************************************************************************/
function InventoryMoveQuantity(request, response)
{
	var getBinLocationId = request.getParameter('custparam_imbinlocationid');
	var getBinLocation = request.getParameter('custparam_imbinlocationname');
	var getItemId = request.getParameter('custparam_imitemid');
	var getItem = request.getParameter('custparam_imitem');
	var getTotQuantity = request.getParameter('custparam_totquantity');
	var getAvailQuantity = request.getParameter('custparam_availquantity');    
	var getUOMId = request.getParameter('custparam_uomid');
	var getUOM = request.getParameter('custparam_uom');
	var getStatus = request.getParameter('custparam_status');
	var getStatusId = request.getParameter('custparam_statusid');
	var getLOTId = request.getParameter('custparam_lotid');
	var getLOTNo = request.getParameter('custparam_lot');  
	var getLPId = request.getParameter('custparam_lpid');
	var getLP = request.getParameter('custparam_lp');  
	var getItemType = request.getParameter('custparam_itemtype');
	var getActualBeginDate = request.getParameter('custparam_actualbegindate');
	var getActualBeginTime = request.getParameter('custparam_actualbegintime');
	var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
	var getInvtRecID=request.getParameter('custparam_invtrecid');
	var locationId=request.getParameter('custparam_locationId');//
	var ItemDesc = request.getParameter('custparam_itemdesc');
	var compId=request.getParameter('custparam_compid');//
	var fifodate=request.getParameter('custparam_fifodate');//
	nlapiLogExecution('ERROR', 'locationId ', request.getParameter('custparam_locationId'));
	nlapiLogExecution('ERROR', 'getItemType', getItemType);
	nlapiLogExecution('ERROR', 'getInvtRecID ',getInvtRecID);
	nlapiLogExecution('ERROR', 'fifodate ',fifodate);
	
	var	tempBinLocationId=request.getParameter('custparam_hdntmplocation');
	var tempItemId=request.getParameter('custparam_hdntempitem');
	var tempLP=request.getParameter('custparam_hdntemplp');

	if (request.getMethod() == 'GET') 
	{	
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('ERROR', 'getLanguage', getLanguage);
	    
		var st0,st1,st2,st3,st4;
		if( getLanguage == 'es_ES')
		{
			
			st0 = "INVENTARIO DE MUDANZA";
			st1 = "CANTIDAD TOTAL ";
			st2 = "Ingresar Mover CANT :";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";
		}
		else
		{
			st0 = "INVENTORY MOVE";
			st1 = "TOTAL QUANTITY ";
			st2 = "ENTER MOVE QTY :";
			st3 = "SEND";
			st4 = "PREV";
		}		
		
		var caseQty = fetchCaseQuantity(getItemId,locationId,null);
		
		var functionkeyHtml=getFunctionkeyScript('_rf_inventorymove_quantity');
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterquantity').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head>";
		html = html + "<body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_inventorymove_quantity' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + ":<label>" + getAvailQuantity + " Each</label>";
		var pikQtyBreakup = getQuantityBreakdown(caseQty,getAvailQuantity);
		html = html + "	("+caseQty+"/Case:"+pikQtyBreakup+")";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterquantity' id='enterquantity' type='text'/>";
		html = html + "					<input type='hidden' name='hdnBinLocationId' value=" + getBinLocationId + ">";
		html = html + "					<input type='hidden' name='hdnBinLocation' value='" + getBinLocation + "'>";
		html = html + "					<input type='hidden' name='hdnItemId' value=" + getItemId + ">";
		html = html + "					<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "					<input type='hidden' name='hdnLP' value=" + getLP + ">";
		html = html + "					<input type='hidden' name='hdnLPId' value=" + getLPId + ">";        
		html = html + "					<input type='hidden' name='hdnLOTNo' value=" + getLOTNo + ">";
		html = html + "					<input type='hidden' name='hdnLOTNoId' value=" + getLOTId + ">";
		html = html + "					<input type='hidden' name='hdnStatus' value='" + getStatus + "'>";
		html = html + "					<input type='hidden' name='hdnStatusId' value=" + getStatusId + ">";
		html = html + "					<input type='hidden' name='hdnUOM' value=" + getUOM + ">";
		html = html + "					<input type='hidden' name='hdnUOMId' value=" + getUOMId + ">";
		html = html + "					<input type='hidden' name='hdnTotQty' value=" + getTotQuantity + ">";
		html = html + "					<input type='hidden' name='hdnAvailQty' value=" + getAvailQuantity + ">";       
		html = html + "					<input type='hidden' name='hdnItemType' value=" + getItemType + ">";       
		html = html + "					<input type='hidden' name='hdnInvtRecID' value=" + getInvtRecID + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				    <input type='hidden' name='hdnlocationId' value=" + locationId + ">";
		html = html + "				    <input type='hidden' name='hdncompId' value=" + compId + ">";
		html = html + "				    <input type='hidden' name='hdnItemdesc' value='" + ItemDesc + "'>";
		html = html + "				    <input type='hidden' name='hdnfifodate' value='" + fifodate + "'>";
		html = html + "				    <input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		//html = html + "				    <input type='hidden' name='hdntotalidresults' value=" + totalidres + ">";
		html = html + "				    <input type='hidden' name='hdntmplocation' value=" + tempBinLocationId + ">";
		html = html + "				    <input type='hidden' name='hdntemplp' value='" + tempLP + "'>";
		html = html + "				    <input type='hidden' name='hdntempitem' value='" + tempItemId + "'>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSearch' type='submit' value='F8' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "				" + st4 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterquantity').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{

		var getMoveQuantity = request.getParameter('enterquantity');
		nlapiLogExecution('ERROR', 'New Move Qty', getMoveQuantity);

		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');

		var IMarray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		IMarray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR', 'getLanguage', IMarray["custparam_language"]);
    	
		
		var st5,st6,st7,st8;
		if( getLanguage == 'es_ES')
		{
			
			st5 = "MOVER CANTIDAD NO DEBE SER MAYOR QUE LA CANTIDAD DISPONIBLE";
			st6 = "CANTIDAD NEGATIVA NO SE MUEVE";
			st7 = "INGRESAR MOVER CANTIDAD";
			st8 = "CANTIDAD MOVIMIENTO NO DEBE SER SUPERIOR A LA CANTIDAD DE PALETA";
			st9 = "Decimal qty is not allowed for serial item";
		}
		else
		{
			
			st5 = "MOVE QUANTITY SHOULD NOT BE GREATER THAN THE AVAILABLE QUANTITY";// case# 201417058
			st6 = "NEGATIVE  QUANTITY NOT MOVED";
			st7 = "MOVE QUANTITY SHOULD NOT BE 0";
			st8 = "MOVE QUANTITY SHOULD NOT BE GREATER THAN THE PALLET QUANTITY";
			st9 = "Decimal qty is not allowed for serial item";
			
		}
		
		nlapiLogExecution('ERROR', 'getAvailQuantity ',getAvailQuantity);
		nlapiLogExecution('ERROR', 'getMoveQuantity ',getMoveQuantity);
//		if(getAvailQuantity == getMoveQuantity)
		var allocatedqty=0;
		if(request.getParameter('hdnInvtRecID')!=null&&request.getParameter('hdnInvtRecID')!="")
			allocatedqty=nlapiLookupField("customrecord_ebiznet_createinv",request.getParameter('hdnInvtRecID'),"custrecord_ebiz_alloc_qty");
		nlapiLogExecution('ERROR', 'allocatedqty ',allocatedqty);
		if((getAvailQuantity == getMoveQuantity)&&(allocatedqty==0))
		{
			IMarray["custparam_totqtymoveed"] = 'Y';
		}
		else
		{
			IMarray["custparam_totqtymoveed"] = 'N';
		}
		IMarray["custparam_imbinlocationid"] = request.getParameter('hdnBinLocationId');
		IMarray["custparam_imbinlocationname"] = request.getParameter('hdnBinLocation');
		IMarray["custparam_imitemid"] = request.getParameter('hdnItemId');
		IMarray["custparam_imitem"] = request.getParameter('hdnItem');
		IMarray["custparam_totquantity"] = request.getParameter('hdnTotQty');
		IMarray["custparam_availquantity"] = request.getParameter('hdnAvailQty');  		
		IMarray["custparam_status"] = request.getParameter('hdnStatus');
		IMarray["custparam_statusid"] = request.getParameter('hdnStatusId');
		IMarray["custparam_uom"] = request.getParameter('hdnUOM');
		IMarray["custparam_uomid"] = request.getParameter('hdnUOMId');		
		IMarray["custparam_lot"] = request.getParameter('hdnLOTNo');
		IMarray["custparam_lotid"] = request.getParameter('hdnLOTNoId');
		IMarray["custparam_lp"] = request.getParameter('hdnLP');
		IMarray["custparam_lpid"] = request.getParameter('hdnLPId');
		IMarray["custparam_itemtype"]=request.getParameter('hdnitemType');
		IMarray["custparam_invtrecid"] = request.getParameter('hdnInvtRecID');
		IMarray["custparam_actualbegindate"] = getActualBeginDate;
		IMarray["custparam_locationId"] = request.getParameter('hdnlocationId');
		IMarray["custparam_itemdesc"] = request.getParameter('hdnItemdesc');
		IMarray["custparam_compid"] = request.getParameter('hdncompId');
		IMarray["custparam_fifodate"] = request.getParameter('hdnfifodate');
		nlapiLogExecution('ERROR', 'Compid', request.getParameter('hdncompId'));
		///IMarray["custparam_totalidresults"] = request.getParameter('hdntotalidresults');
		IMarray["custparam_hdntmplocation"] = request.getParameter('hdntmplocation');
		IMarray["custparam_hdntemplp"] = request.getParameter('hdntemplp');
		IMarray["custparam_hdntempitem"] = request.getParameter('hdntempitem');
		nlapiLogExecution('ERROR', 'IMarray["custparam_locationId"] ', IMarray["custparam_locationId"]);

		IMarray["custparam_screenno"] = '21';


		var TimeArray = new Array();
		IMarray["custparam_actualbegintime"] = getActualBeginTime;
		IMarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM;

		var optedEvent = request.getParameter('cmdPrevious');
		nlapiLogExecution('ERROR', 'Itemtype', IMarray["custparam_itemtype"]);
		if (optedEvent == 'F7') 
		{   
			if (IMarray["custparam_itemtype"] == 'lotnumberedinventoryitem' || IMarray["custparam_itemtype"] == 'lotnumberedassemblyitem' || IMarray["custparam_itemtype"] == 'assemblyitem') 
			{
				response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_batchconf', 'customdeploy_rf_inventory_move_batch_di', false, IMarray);
			}
			else
			{
				response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_cont', 'customdeploy_rf_inventory_move_cont_di', false, IMarray);
			}
		}
		else 
		{
			try 
			{
				nlapiLogExecution('ERROR', 'Move Quantity', getMoveQuantity);
				nlapiLogExecution('ERROR', 'Avail Quantity', request.getParameter('hdnAvailQty'));
				//case20127088 allowing  quantity greater than zero  ex 0.6
				
				
				var fields = ['recordType', 'custitem_ebizserialin'];
				var columns = nlapiLookupField('item', IMarray["custparam_imitemid"], fields);
				var vItemType = columns.recordType;
				
				var serialin = columns.custitem_ebizserialin;
				nlapiLogExecution('Debug','vItemType',vItemType);
				
				
				
				
				
				
				if (getMoveQuantity != null && getMoveQuantity != '' && getMoveQuantity >0) 
				{
					if (parseFloat(getMoveQuantity) <= parseFloat(request.getParameter('hdnAvailQty'))) 
					{            			
						IMarray["custparam_moveqty"] = getMoveQuantity;
						nlapiLogExecution('ERROR', 'Quantity', getMoveQuantity);	      				
					}
					else
					{
						IMarray["custparam_error"] = st5;      					

						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
						nlapiLogExecution('ERROR', 'Move quantity sholud be less than the available quantity', getMoveQuantity);
						return;
					}
					/*nlapiLogExecution('ERROR', 'getItemType', getItemType);	
					if(getItemType == 'serializedinventoryitem' || getItemType == 'serializedassemblyitem')
					{
						if(parseFloat(getMoveQuantity) % 1 != 0)
						{
							IMarray["custparam_error"] = st9; 
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
							nlapiLogExecution('ERROR', 'Decimal qty is not allowed for serial item', getMoveQuantity);
							return;
						}
					}	*/
					
					
					if(vItemType == 'serializedinventoryitem' || vItemType == 'serializedassemblyitem' || serialin == 'T')
					{
						nlapiLogExecution('Debug','getMoveQuantity',getMoveQuantity);
						if(parseInt(getMoveQuantity) != getMoveQuantity)
						{
							IMarray["custparam_error"] = "Decimal qty is not allowed for serial item"; 
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
							nlapiLogExecution('ERROR', 'Decimal qty is not allowed for serial item', getMoveQuantity);
							return;
							}
					}	
				}
				else 
				{
					if(getMoveQuantity < 0)
					{
						IMarray["custparam_error"] = st6;
					}
					else
					{
						IMarray["custparam_error"] = st7;
					}
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
					nlapiLogExecution('ERROR', 'Quantity is not entered', getMoveQuantity);
					return;
				}                

				//case 20126219 start : to restrict not move more than pallet qty
				var palletQuantity = fetchPalletQuantity(IMarray["custparam_imitemid"],IMarray["custparam_locationId"],null);
				nlapiLogExecution('ERROR', 'palletQuantity', palletQuantity);
				
				if(parseFloat(getMoveQuantity) > parseFloat(palletQuantity))
				{
					IMarray["custparam_error"] = st8;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
					nlapiLogExecution('ERROR', 'Quantity is greater than palet qty ', palletQuantity);
					return;
				}
				//case 20126219 end
				
				// response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_lp', 'customdeploy_rf_inventory_move_lp_di', false, IMarray);
				//response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_endloc', 'customdeploy_rf_inventory_move_endloc_di', false, IMarray);
				response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_status', 'customdeploy_rf_inventory_move_status_di', false, IMarray);
				nlapiLogExecution('ERROR', 'Navigating to Status', 'Success');

			} 
			catch (e) 
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
				nlapiLogExecution('ERROR', 'Catching the Search Results', 'Length is null');
			}
			nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
		}
	}
}
//case 20126219 start 

function fetchPalletQuantity(itemId, location, company)
{
	nlapiLogExecution('ERROR','itemId',itemId);
	nlapiLogExecution('ERROR','location in fetchPalletQuantity',location);
	var itemPalletQuantity = 0;

	var itemFilters = new Array();
	itemFilters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemId));
	itemFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(location!=null&&location!='')
		itemFilters.push(new nlobjSearchFilter('custrecord_ebizsiteskudim',null,'anyof',['@NONE@',location]));

	var itemColumns = new Array();
	itemColumns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	itemColumns[1] = new nlobjSearchColumn('custrecord_ebizsiteskudim');
	itemColumns[2] = new nlobjSearchColumn('custrecord_ebizcompanyskudimension');
	itemColumns[3] = new nlobjSearchColumn('custrecord_ebizqty'); 	
	itemColumns[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim').setSort(true); 	

	var itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, itemFilters, itemColumns);

	if (itemSearchResults != null && itemSearchResults != '')
	{
		itemPalletQuantity = itemSearchResults[0].getValue(itemColumns[3]);
		nlapiLogExecution('ERROR','Item Pallet Quantity',itemPalletQuantity);
	}
	return itemPalletQuantity;
}

//case 20126219 end
