/***************************************************************************
 eBizNET Solutions Inc  
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_Confirm_AddnewBins.js,v $
 *     	   $Revision: 1.1.2.1 $
 *     	   $Date: 2015/04/14 09:57:08 $
 *     	   $Author: snimmakayala $
 *     	   $Name: t_eBN_2015_1_StdBundle_1_45 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 *
 *****************************************************************************/

function CYCC_AddNewBins(request, response){
	if (request.getMethod() == 'GET') {
		var getCartLP;

		var getPlanNo = request.getParameter('custparam_planno');
		nlapiLogExecution('ERROR', 'getPlanNo', getPlanNo);

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4,st5;
		// 20126048

		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st0 = "";
			st1 = "LP Carro";
			st2 = "ENTRAR / ESCANEAR N&#218;MERO DE CARRO";	
			st3 = "N&#250;mero de placa";
			st4 = "ENVIAR";
			st5 = "ANTERIOR";

		}
		else
		{
			st0 = "";
			//st1 = "No Bins to Record fro this Plan. Do you want to Add more Bins.";	
			st1 = "NO BINS TO RECORD FOR THIS PLAN, DO YOU WANT TO ADD MORE BINS.";	
			st4 = "YES";
			st5 = "NO";

		}



		var functionkeyHtml=getFunctionkeyScript('_rf_putaway_lp'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('entercartlp').focus();";    

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>"; 
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_cycc_addnewbins' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> ";
		html = html + "				<input type='hidden' name='hdncyccplanno' value=" + getPlanNo + ">";
		html = html + "				<input type='hidden' name='hdnlanguage' value=" + getLanguage + ">";
		html = html + "				</td>";
		html = html + "			</tr>";

		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + "</td></tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				</td>";
		html = html + "			</tr>";

		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				</td>";
		html = html + "			</tr>";

		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st4  +" <input name='cmdYes' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					" + st5 + " <input name='cmdNo' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		//html = html + "<script type='text/javascript'>document.getElementById('entercartlp').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {

		try
		{
			nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is not null');

			var getCycleCountPlanNo = request.getParameter('custparam_planno');
			nlapiLogExecution('ERROR', 'getPlanNo', getCycleCountPlanNo);

			var getLanguage = request.getParameter('hdngetLanguage');

			var CCarray = new Array();
			CCarray["custparam_language"] = getLanguage;



			var st6;
			if( getLanguage == 'es_ES')
			{

				st6= "ubicaci�n no v�lida";	

			}
			else
			{

				st6="INVALID PLAN NO";
			}

			CCarray["custparam_error"] = st6;
			CCarray["custparam_screenno"] = 'CYCCAddmoreItems';
			CCarray["custparam_planno"] = getCycleCountPlanNo;

			var optedEvent = request.getParameter('cmdNo');
			nlapiLogExecution('ERROR', 'optedEvent', optedEvent);
			//	if the YES button 'ENT' is clicked, it has to go to the Add new Item screen 
			//	if the NO button 'F7' is clicked, it has to go to the CYCC Plan no screen 
			if (optedEvent == 'F7') {

				nlapiLogExecution('ERROR', 'optedEvent', 'NO');				
				response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_plan_no', 'customdeploy_rf_cyclecount_plan_no_di', false, CCarray);
				return;
			}
			else {

				nlapiLogExecution('ERROR', 'optedEvent', 'YES');
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cyc_addnewitem', 'customdeploy_ebiz_rf_cyc_addnewitem_di', false, CCarray);
				return;

			}
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR', 'exception in CYCC', exp);
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
			return;
		}
	}
}
