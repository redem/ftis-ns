/***************************************************************************
����������������������������
���������������������eBizNET Solutions Inc 
 ****************************************************************************
 *
 *� $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Masters/Client/ebiz_CheckItemFamily_CL.js,v $
 *� $Revision: 1.3.4.1.8.1 $
 *� $Date: 2013/09/11 15:23:51 $
 *� $Author: rmukkera $
 *� $Name: t_NSWMS_2014_1_1_174 $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_CheckItemFamily_CL.js,v $
 *� Revision 1.3.4.1.8.1  2013/09/11 15:23:51  rmukkera
 *� Case# 20124376
 *�
 *� Revision 1.3.4.1  2012/02/09 16:10:57  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Physical and Virtual Location changes
 *�
 *� Revision 1.4  2012/02/09 14:43:42  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Physical and Virtual Location changes
 *�
 *� Revision 1.3  2012/01/10 07:19:33  spendyala
 *� CASE201112/CR201113/LOG201121
 *�  added code to remove space.
 *�
 *
 ****************************************************************************/

function fnCheckItemFamily(type)
{
	var vItemFamilyName =  nlapiGetFieldValue('name');
	var vItemDesc =  nlapiGetFieldValue('custrecord_skufamdescription');
	var vSite	= nlapiGetFieldValue('custrecord_ebizsiteskuf');  
	var vCompany	= nlapiGetFieldValue('custrecord_ebizcompanyskuf');	

	/*
	 * Added on 1/4/2011 by Phani
	 * The below variable will fetch the internal id of the record selected.
	 */    

	var vId = nlapiGetFieldValue('id');
	if(vItemFamilyName != "" && vItemFamilyName != null && vItemDesc != "" && vItemDesc != null && vSite != "" && vSite != null && vCompany != ""  && vCompany != null)
	{
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('name', null, 'is',vItemFamilyName);
		filters[1] = new nlobjSearchFilter('custrecord_ebizsiteskuf', null, 'anyof',['@NONE@',vSite]);
		filters[2] = new nlobjSearchFilter('custrecord_ebizcompanyskuf', null, 'anyof',['@NONE@',vCompany]);
		filters[3] = new nlobjSearchFilter('isinactive', null, 'is','F');

		/*
		 * Added on 1/4/2011 by Phani
		 * The below variable will filter based on the internal id of the record selected.
		 */
		if (vId != null && vId != "") {
			filters[4] = new nlobjSearchFilter('internalid', null, 'noneof', vId);
		}

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sku_family', null, filters);

		if(searchresults)
		{
			alert("The item family already exists.");
			return false;
		}
		else
		{
			return true;
		} 
	}
	else	
	{
		return true;
	}
}