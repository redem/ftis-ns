function backfunction()
{
	 var ctx = nlapiGetContext();
	 var url="https://system.netsuite.com";
	 var linkURL =url+ nlapiResolveURL('SUITELET', 'customscript_editfullfilmentorderdetails', 'customdeploy_editfullfilmentorderdetails');
	 window.location=linkURL;
}
function WaveSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Order Line Details');
		createQb(form,'normal');	
		var  button=form.addSubmitButton('Display');	
		
		response.writePage(form);
	}
	else //this is the POST block
	{
		// Retrieve the number of items for which wave needs to be created
		var lineCount = request.getLineItemCount('custpage_items');

		var form = nlapiCreateForm('Order Line Details');
		
//		if(lineCount==-1)
//	  {
		
		createQb(form,'hidden');
		
		 form.addSubmitButton('Submit');
		 form.setScript('customscript_editfullfilmentorderdetails');
		 form.addButton('custpage_print','back','backfunction()');
		createWaveGenSublist(form);
		var orderList = getOrdersForWaveGen(request);
		var msg;
		if(orderList != null && orderList.length > 0){
			 msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
			 msg.setDisplaySize(100,30);
		 	var shippingcarrier = form.addField('custpage_shipmethod', 'select', 'Shipping Method');
			var record = nlapiCreateRecord('salesorder', {recordmode: 'dynamic'});
			var methodsField = record.getField('shipmethod');
			var methodOptions = methodsField.getSelectOptions(null, null);
			//nlapiLogExecution('ERROR','methodOptions',methodOptions);		
			 shippingcarrier.addSelectOption("",""); 			 
			  for (var i = 0; methodOptions != null && i < methodOptions.length; i++) {			
	         shippingcarrier.addSelectOption(methodOptions[i].getId(),methodOptions[i].getText());
	     }	
	    	shippingcarrier.setDisplaySize(100,30);
	    	 if(request.getParameter('custpage_shipmethod')!=null)
	 		{
	    		 shippingcarrier.setDefaultValue(request.getParameter('custpage_shipmethod'));
	 		}
			var sofrightterms = form.addField('custpage_frieghtterms', 'select', 'Freight Terms','customlist_nswmsfreighttermslov');
			sofrightterms.setDisplaySize(100,30);
			 if(request.getParameter('custpage_frieghtterms')!=null)
		 		{
				 sofrightterms.setDefaultValue(request.getParameter('custpage_frieghtterms'));
		 		}
			var carrier = form.addField('custpage_carrierso', 'select', 'Carrier','customrecord_ebiznet_carrier');
			carrier.setDisplaySize(100,30);
			 if(request.getParameter('custpage_carrierso')!=null)
		 		{
				 carrier.setDefaultValue(request.getParameter('custpage_carrierso'));
		 		}
			var OrdPriority = form.addField('custpage_orderpriority', 'select', 'Ord Priority','customlist_ebiznet_order_priority');
			OrdPriority.setDisplaySize(100,30);
			 if(request.getParameter('custpage_orderpriority')!=null)
		 		{
				 OrdPriority.setDefaultValue(request.getParameter('custpage_orderpriority'));
		 		}
			for(var i = 0; i < orderList.length; i++){
				var currentOrder = orderList[i];

				// Call function to add one fulfillment order to the sublist
				addFulfilmentOrderToSublist(form, currentOrder, i);
			}
		}
//	  }

			var fulfillOrderList = getSelectedFulfillmentOrderInfo(request);	
			
			if(fulfillOrderList!=null && fulfillOrderList.length>0)
				{
				
				var ship_method=request.getParameter('custpage_shipmethod');
				var order_priority=request.getParameter('custpage_orderpriority');
				var freight_terms=request.getParameter('custpage_frieghtterms');
				var carrier=request.getParameter('custpage_carrierso');
				var fields=new Array();
				var values=new Array();
				var i=0;
				if(ship_method!=null && ship_method!='')
					{
					fields[i]="custrecord_do_carrier";
					values[i]=ship_method;
					i++;
					}
				if(order_priority!=null && order_priority!='')
				{
				fields[i]="custrecord_do_order_priority";
				values[i]=order_priority;
				i++;
				}
				if(freight_terms!=null && freight_terms!='')
				{
				fields[i]="custrecord_ebiz_freightterms";
				values[i]=freight_terms;
				i++;
				}
				if(carrier!= null && carrier!='')
				{
				fields[i]="custrecord_do_wmscarrier";
				values[i]=carrier;
				
				}
				for(var j=0;j<fulfillOrderList.length;j++){
					var internalid=fulfillOrderList[j][20];
					var id=parseInt(internalid);
					nlapiSubmitField('customrecord_ebiznet_ordline',id,fields,values) ;
				}
			var orderList = getOrdersForWaveGen(request);

				if(orderList != null && orderList.length > 0){	
					for(var i = 0; i < orderList.length; i++){
						var currentOrder = orderList[i];

						// Call function to add one fulfillment order to the sublist
						addFulfilmentOrderToSublist(form, currentOrder, i);
					}
					
					
	              msg.setDefaultValue("<div id='div__alert' align='center' style='width:20px;'></div><script>showAlertBox('div__alert', 'Confirmation', 'orders updated Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '30%', null, null, null);</script></div>");
	              msg.setLayoutType('startrow', 'startcol');  
				}
	             
	              //response.sendRedirect('SUITELET', 'customscript_editfullfilmentorderdetails', 'customdeploy_editfullfilmentorderdetails');
				}
		//	}
		
		
			
			response.writePage(form);
	}
}
function getSelectedFulfillmentOrderInfo(request){
	//nlapiLogExecution('ERROR','into getSelectedFulfillmentOrderInfo', '');
	var fulfillOrderInfoArray = new Array();
	var orderInfoCount = 0;

	// Retrieve the number of items for which wave needs to be created
	var lineCount = request.getLineItemCount('custpage_items');

	for(var k = 1; k <= lineCount; k++){
		var selectValue = request.getLineItemValue('custpage_items', 'custpage_so', k);
		//nlapiLogExecution('ERROR','selectValue', selectValue);

		if(request.getLineItemValue('custpage_items', 'custpage_so', k)=='T'){
			var itemName = request.getLineItemValue('custpage_items', 'custpage_itemname', k);
			var itemNo = request.getLineItemValue('custpage_items', 'custpage_itemno', k);
			var availQty = request.getLineItemValue('custpage_items', 'custpage_avlqty', k);
			var orderQty = request.getLineItemValue('custpage_items', 'custpage_ordqty', k);
			var doNo = request.getLineItemValue('custpage_items', 'custpage_doinernno', k);
			var lineNo = request.getLineItemValue('custpage_items', 'custpage_lineno', k);
			var doName = request.getLineItemValue('custpage_items', 'custpage_sono', k);
			var soInternalID = request.getLineItemValue('custpage_items', 'custpage_soinernno', k);
			var itemStatus = request.getLineItemValue('custpage_items', 'custpage_skustatus', k);
			var packCode = request.getLineItemValue('custpage_items', 'custpage_packcode', k);
			var uom = request.getLineItemValue('custpage_items', 'custpage_uomid', k);
			var lotBatchNo = request.getLineItemValue('custpage_items', 'custpage_lotbatch', k);
			var location = request.getLineItemValue('custpage_items', 'custpage_location', k);
			var company = request.getLineItemValue('custpage_items', 'custpage_company', k);
			var itemInfo1 = request.getLineItemValue('custpage_items', 'custpage_iteminfo1', k);
			var itemInfo2 = request.getLineItemValue('custpage_items', 'custpage_iteminfo2', k);
			var itemInfo3 = request.getLineItemValue('custpage_items', 'custpage_iteminfo2', k);
			var orderType = request.getLineItemValue('custpage_items', 'custpage_ordtype', k);
			var orderPriority = request.getLineItemValue('custpage_items', 'custpage_ordpriority', k);
			var internalid = request.getLineItemValue('custpage_items', 'custpage_internalid', k);
			var currentRow = [k, soInternalID, lineNo, doNo, doName, itemName, itemNo, itemStatus, 
			                  availQty, orderQty, packCode, uom, lotBatchNo, company, 
			                  itemInfo1, itemInfo2, itemInfo3, orderType, orderPriority,location,internalid];

			fulfillOrderInfoArray[orderInfoCount++] = currentRow;
		}
	}
	//nlapiLogExecution('ERROR','fulfillOrderInfoArray', fulfillOrderInfoArray.length);

	//nlapiLogExecution('ERROR','out of getSelectedFulfillmentOrderInfo', '');

	return fulfillOrderInfoArray;

}

function createQb(form,displaytype){
	//var group = form.addFieldGroup('mygroup','_');
	 var soField = form.addField('custpage_qbso', 'select', 'Fulfillment Ord#');
     soField.setLayoutType('startrow', 'none');  		
     soField.setDisplayType(displaytype);
		var filtersso = new Array();		
        filtersso[0] = new nlobjSearchFilter( 'custrecord_ord_qty', null, 'greaterthan', '0' );
		filtersso[1] = new nlobjSearchFilter( 'custrecord_linestatus_flag', null, 'anyof', [25] );
		 soField.addSelectOption("","");
		 var columnsinvt = new Array();
		columnsinvt[0] = new nlobjSearchColumn('custrecord_lineord');
		columnsinvt[0].setSort();
		columnsinvt[1] = new nlobjSearchColumn('custrecord_shipment_no');
		
		
     var searchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filtersso,columnsinvt);
		 
     for (var i = 0; searchresults != null && i < searchresults.length; i++) {
			//Searching for Duplicates		
			var res=  form.getField('custpage_qbso').getSelectOptions(searchresults[i].getValue('custrecord_lineord'), 'is');
			if (res != null) {
				//nlapiLogExecution('ERROR', 'res.length', res.length +searchresults[i].getValue('custrecord_lineord') );
				if (res.length > 0) {
					continue;
				}
			}		
         soField.addSelectOption(searchresults[i].getValue('custrecord_lineord'), searchresults[i].getValue('custrecord_lineord'));
     }	
     if(request.getParameter('custpage_qbso')!=null)
		{
		soField.setDefaultValue(request.getParameter('custpage_qbso'));
		}
//Shipment #
     var shipmentField = form.addField('custpage_qbshipmentno', 'select', 'Shipment #');
     shipmentField.setLayoutType('startrow', 'none');
     shipmentField.setDisplayType(displaytype);
     shipmentField.addSelectOption("","");
     for (var i = 0; searchresults != null && i < searchresults.length; i++) {
			
     	if(searchresults[i].getValue('custrecord_shipment_no')!=null && searchresults[i].getValue('custrecord_shipment_no')!="")
     		{
     	//Searching for Duplicates		
			var res=  form.getField('custpage_qbshipmentno').getSelectOptions(searchresults[i].getValue('custrecord_shipment_no'), 'is');
			if (res != null) {
				//nlapiLogExecution('ERROR', 'res.length', res.length +searchresults[i].getValue('custrecord_shipment_no') );
				if (res.length > 0) {
					continue;
				}
			}		
			shipmentField.addSelectOption(searchresults[i].getValue('custrecord_shipment_no'), searchresults[i].getValue('custrecord_shipment_no'));
     }  
     }
     if(request.getParameter('custpage_qbshipmentno')!=null)
		{
    	 shipmentField.setDefaultValue(request.getParameter('custpage_qbshipmentno'));
		}
     
   //Route #
     var RouteField = form.addField('custpage_qbrouteno', 'select', 'Route','customlist_ebiznetroutelov');
     RouteField.setLayoutType('startrow', 'none');
     RouteField.setDisplayType(displaytype);
     if(request.getParameter('custpage_qbrouteno')!=null)
		{
    	 RouteField.setDefaultValue(request.getParameter('custpage_qbrouteno'));
		}
     
		var soOrdPriority = form.addField('custpage_ordpriority', 'select', 'Ord Priority','customlist_ebiznet_order_priority');
     soOrdPriority.setLayoutType('startrow', 'none'); 		
     soOrdPriority.setDisplayType(displaytype);
     if(request.getParameter('custpage_ordpriority')!=null)
		{
    	 soOrdPriority.setDefaultValue(request.getParameter('custpage_ordpriority'));
		}
		 var soOrdType = form.addField('custpage_ordtype', 'select', 'Order Type','customrecord_ebiznet_order_type');
		 soOrdType.setLayoutType('startrow', 'none');
		 soOrdType.setDisplayType(displaytype);
		  if(request.getParameter('custpage_ordtype')!=null)
			{
			  soOrdType.setDefaultValue(request.getParameter('custpage_ordtype'));
			}
		 
		 
		 var Consignee = form.addField('custpage_consignee', 'select', 'Consignee','customer');
		 Consignee.setLayoutType('startrow', 'none');
		 Consignee.setDisplayType(displaytype);
		  if(request.getParameter('custpage_consignee')!=null)
			{
			  Consignee.setDefaultValue(request.getParameter('custpage_consignee'));
			}
		 
		  var itemfamily = form.addField('custpage_itemfamily', 'select', 'Item Family','customrecord_ebiznet_sku_family');
		  itemfamily.setDisplayType(displaytype);
		  if(request.getParameter('custpage_itemfamily')!=null)
			{
			  itemfamily.setDefaultValue(request.getParameter('custpage_itemfamily'));
			}
		  var itemgroup = form.addField('custpage_itemgroup', 'select', 'Item Group','customrecord_ebiznet_sku_group');
		  itemgroup.setDisplayType(displaytype);
		  if(request.getParameter('custpage_itemgroup')!=null)
			{
			  itemgroup.setDefaultValue(request.getParameter('custpage_itemgroup'));
			}
		  
		  var item = form.addField('custpage_item', 'select', 'Item','inventoryitem');
		  item.setDisplayType(displaytype);
		  if(request.getParameter('custpage_item')!=null)
			{
			  item.setDefaultValue(request.getParameter('custpage_item'));
			}
		  
		  var iteminfo1= form.addField('custpage_iteminfoone', 'select', 'Item Info1');
		  iteminfo1.addSelectOption("","");
		  iteminfo1.setDisplayType(displaytype);
		  var columns = new Array();
			columns[0] = new nlobjSearchColumn('custitem_item_info_1');			 
			var filters= new Array();
			filters.push(new nlobjSearchFilter('custitem_item_info_1', null, 'isnotempty'));			 
		//Searchng Duplicates
			var result= nlapiSearchRecord('item',null,filters,columns);
			 for (var i = 0; result != null && i < result.length; i++) {
				 
				 var res=  form.getField('custpage_iteminfoone').getSelectOptions(result[i].getValue('custitem_item_info_1'), 'is');
					if (res != null) {						
						if (res.length > 0) {
							continue;
						}
					}
						
				 iteminfo1.addSelectOption(result[i].getValue('custitem_item_info_1'),result[i].getValue('custitem_item_info_1'));
			 }
		  
			  if(request.getParameter('custpage_iteminfoone')!=null)
				{
				  iteminfo1.setDefaultValue(request.getParameter('custpage_iteminfoone'));
				}
		  var iteminfo2= form.addField('custpage_iteminfotwo', 'select', 'Item Info2');
		  iteminfo2.addSelectOption("","");
		  var columns = new Array();
			columns[0] = new nlobjSearchColumn('custitem_item_info_2');			 
			var filters= new Array();
			filters.push(new nlobjSearchFilter('custitem_item_info_2', null, 'isnotempty'));			 
			var result= nlapiSearchRecord('item',null,filters,columns);
			 for (var i = 0; result != null && i < result.length; i++) {
				 
				 var res=  form.getField('custpage_iteminfotwo').getSelectOptions(result[i].getValue('custitem_item_info_2'), 'is');
					if (res != null) {						
						if (res.length > 0) {
							continue;
						}
					}
				 
				 iteminfo2.addSelectOption(result[i].getValue('custitem_item_info_2'),result[i].getValue('custitem_item_info_2'));
			 }
		  
			 iteminfo2.setDisplayType(displaytype);
			  if(request.getParameter('custpage_iteminfotwo')!=null)
				{
				  iteminfo2.setDefaultValue(request.getParameter('custpage_iteminfotwo'));
				}
		  var iteminfo3= form.addField('custpage_iteminfothree', 'select', 'Item Info3');
		  iteminfo3.addSelectOption("","");
		  iteminfo3.setDisplayType(displaytype);
		  var columns = new Array();
			columns[0] = new nlobjSearchColumn('custitem_item_info_3');			 
			var filters= new Array();
			filters.push(new nlobjSearchFilter('custitem_item_info_3', null, 'isnotempty'));			 
			var result= nlapiSearchRecord('item',null,filters,columns);
			 for (var i = 0; result != null && i < result.length; i++) {
					 var res=  form.getField('custpage_iteminfothree').getSelectOptions(result[i].getValue('custitem_item_info_3'), 'is');
						if (res != null) {						
							if (res.length > 0) {
								continue;
							}
						}
				 iteminfo3.addSelectOption(result[i].getValue('custitem_item_info_3'),result[i].getValue('custitem_item_info_3'));
			 }
			 

			  if(request.getParameter('custitem_item_info_3')!=null)
				{
				  iteminfo3.setDefaultValue(request.getParameter('custitem_item_info_3'));
				}
		 var packcode = form.addField('custpage_packcode', 'select', 'Pack Code','customlist73');	 
		 packcode.setDisplayType(displaytype);
		// packcode.addSelectOption("","");
		  if(request.getParameter('custitem_item_info_3')!=null)
			{
			  packcode.setDefaultValue(request.getParameter('custpage_packcode'));
			}
		 var uom = form.addField('custpage_uom', 'select', 'UOM','customlist75');
		 uom.setDisplayType(displaytype);
		 if(request.getParameter('custpage_uom')!=null)
			{
			 uom.setDefaultValue(request.getParameter('custpage_uom'));
			}
		 
		 var Company = form.addField('custpage_company', 'select', 'Company','customrecord_ebiznet_company');
		 Company.setDisplayType(displaytype);
		// uom.addSelectOption("","");	
		 if(request.getParameter('custpage_company')!=null)
			{
			 Company.setDefaultValue(request.getParameter('custpage_company'));
			}
		var Destination = form.addField('custpage_dest', 'select', 'Destination','customlist_nswmsdestinationlov');
		Destination.setLayoutType('startrow', 'none');	
		Destination.setDisplayType(displaytype);
		if(request.getParameter('custpage_dest')!=null)
		{
			Destination.setDefaultValue(request.getParameter('custpage_dest'));
		}
		var shippingcarrier = form.addField('custpage_shippingcarrier', 'select', 'Shipping Method',null);
		shippingcarrier.setLayoutType('startrow', 'none');
		shippingcarrier.setDisplayType(displaytype);
		var record = nlapiCreateRecord('salesorder', {recordmode: 'dynamic'});
		var methodsField = record.getField('shipmethod');
		var methodOptions = methodsField.getSelectOptions(null, null);
		//nlapiLogExecution('ERROR','methodOptions',methodOptions);
		//nlapiLogExecution('ERROR', methodOptions[0].getId() + ',' + methodOptions[0].getText() );
		 shippingcarrier.addSelectOption("",""); 
		 
		  for (var i = 0; methodOptions != null && i < methodOptions.length; i++) {
		  //	nlapiLogExecution('ERROR', methodOptions[i].getId() + ',' + methodOptions[i].getText() );
         shippingcarrier.addSelectOption(methodOptions[i].getId(),methodOptions[i].getText());
     }	
		  if(request.getParameter('custpage_shippingcarrier')!=null)
			{
				shippingcarrier.setDefaultValue(request.getParameter('custpage_shippingcarrier'));
			}
		//added by shylaja on 091011 to filter the statuses which are not having allow pick at sku status.
		var itemstatus = form.addField('custpage_itemstatus', 'select', 'Item Status');
		var ItemstatusFilter = new Array();
		var Itemstatuscolumns = new Array();
		itemstatus.setDisplayType(displaytype);
		ItemstatusFilter[0] = new nlobjSearchFilter('custrecord_allowpickskustatus', null, 'is', 'T');
		Itemstatuscolumns[0] = new nlobjSearchColumn('custrecord_statusdescriptionskustatus');

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, ItemstatusFilter, Itemstatuscolumns);



		 
		 
		 
		 
		 
		  itemstatus.addSelectOption("","");
		 if (searchresults != null) {	
				for (var i = 0; i < Math.min(500, searchresults.length); i++) {

					var res = form.getField('custpage_itemstatus').getSelectOptions(searchresults[i].getValue('custrecord_statusdescriptionskustatus'), 'is');
					if (res != null) {
						if (res.length > 0) {
							continue;
						}
					}
					//nlapiLogExecution('ERROR','res',res);
					//nlapiLogExecution('ERROR','searchresults[i].getId()',searchresults[i].getId());
					//itemstatus.addSelectOption(searchresults[i].getId(), searchresults[i].getValue('custrecord_statusdescriptionskustatus'));
					//nlapiLogExecution('ERROR','itemstatus',itemstatus);
				}
			}
		// nlapiLogExecution('ERROR','itemstatusnew',itemstatus);
		 if(request.getParameter('custpage_itemstatus')!=null)
			{
			 itemstatus.setDefaultValue(request.getParameter('custpage_itemstatus'));
			}
		 
		 
		 var country = form.addField('custpage_country', 'select', 'Country');
		 country.setDisplayType(displaytype);
		 var record = nlapiCreateRecord('location', {recordmode: 'dynamic'});		
		var countryField = record.getField('country');
		var countryOptions = countryField.getSelectOptions(null, null);
		//nlapiLogExecution('ERROR','countryOptions',countryOptions);
		//nlapiLogExecution('ERROR', countryOptions[0].getId() + ',' + countryOptions[0].getText() );
		 country.addSelectOption("","");
		  for (var i = 0; countryOptions != null && i < countryOptions.length; i++) {
		  	//nlapiLogExecution('ERROR', countryOptions[i].getId() + ',' + countryOptions[i].getText() );
         country.addSelectOption(countryOptions[i].getId(),countryOptions[i].getText());
     }
		  if(request.getParameter('custpage_country')!=null)
			{
			 country.setDefaultValue(request.getParameter('custpage_country'));
			}
		var state = form.addField('custpage_state', 'select', 'Ship State');
		state.setDisplayType(displaytype);
		var addr1 = form.addField('custpage_addr1', 'select', 'Ship Address');
		addr1.setDisplayType(displaytype);
		var City = form.addField('custpage_city', 'select', 'Ship City');
		City.setDisplayType(displaytype);
		 state.addSelectOption("","");
		  addr1.addSelectOption("","");
		   City.addSelectOption("","");
		var columns = new Array();
		columns.push(new nlobjSearchColumn('shipstate'));
		columns.push(new nlobjSearchColumn('shipcity'));
		columns.push(new nlobjSearchColumn('shipaddress'));
				
		var filters= new Array();
		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
		filters.push(new nlobjSearchFilter('status', null, 'anyof', ['SalesOrd:B','SalesOrd:D','SalesOrd:E']));
		
		var result= nlapiSearchRecord('salesorder',null,filters,columns);
		
		  for (var i = 0; result != null && i < result.length; i++) {
		  	var soresult=result[i];
			var vshipstate=soresult.getValue('shipstate');			
			var res=  form.getField('custpage_state').getSelectOptions(vshipstate, 'is');
			if (res != null) {				
				if (res.length > 0) {
					continue;
				}
			}		
         state.addSelectOption(vshipstate, vshipstate);
		  }
		  if(request.getParameter('custpage_state')!=null)
			{
			 state.setDefaultValue(request.getParameter('custpage_state'));
			}
		  for (var i = 0; result != null && i < result.length; i++) {
		  	var soresult=result[i];
			var vshipstate=soresult.getValue('shipstate');
			var vshipcity=soresult.getValue('shipcity');
			var vshipaddr=soresult.getValue('shipaddress');
			
		  var res1=  form.getField('custpage_addr1').getSelectOptions(vshipaddr, 'is');
			if (res1 != null) {				
				if (res1.length > 0) {
					continue;
				}
			}		
         addr1.addSelectOption(vshipaddr, vshipaddr);
		  }
		  if(request.getParameter('custpage_addr1')!=null)
			{
				addr1.setDefaultValue(request.getParameter('custpage_addr1'));
			}
		  for (var i = 0; result != null && i < result.length; i++) {
		  	var soresult=result[i];			
			var vshipcity=soresult.getValue('shipcity');
			
		  var res1=  form.getField('custpage_city').getSelectOptions(vshipcity, 'is');
			if (res1 != null) {				
				if (res1.length > 0) {
					continue;
				}
			}		
         City .addSelectOption(vshipcity, vshipcity);
		  }
		  if(request.getParameter('custpage_city')!=null)
			{
			  City.setDefaultValue(request.getParameter('custpage_city'));
			}
		//added carrier field By suman
		var carrier = form.addField('custpage_carrier', 'select', 'Carrier','customrecord_ebiznet_carrier');
     //end of carrier field
		carrier.setDisplayType(displaytype);
		 if(request.getParameter('custpage_carrier')!=null)
			{
			 carrier.setDefaultValue(request.getParameter('custpage_carrier'));
			}
		var soShipdate = form.addField('custpage_soshipdate', 'date', 'Ship Date');
		soShipdate.setDisplayType(displaytype);
		 if(request.getParameter('custpage_soshipdate')!=null)
			{
			 soShipdate.setDefaultValue(request.getParameter('custpage_soshipdate'));
			}
		var sofrightterms = form.addField('custpage_sofrieghtterms', 'select', 'Freight Terms','customlist_nswmsfreighttermslov');
		sofrightterms.setDisplayType(displaytype);
		if(request.getParameter('custpage_sofrieghtterms')!=null)
			{
			 sofrightterms.setDefaultValue(request.getParameter('custpage_sofrieghtterms'));
			}
		var soOrderdate = form.addField('custpage_soorderdate', 'date', 'Order Date');
		soOrderdate.setDisplayType(displaytype);
		if(request.getParameter('custpage_soorderdate')!=null)
		{
			soOrderdate.setDefaultValue(request.getParameter('custpage_soorderdate'));
		}
		
}



function createWaveGenSublist(form){
	var sublist = form.addSubList("custpage_items", "list", "ItemList");
	sublist.addMarkAllButtons();
	
	sublist.addField("custpage_so", "checkbox", "Confirm").setDefaultValue('F');
	sublist.addField("custpage_sono", "text", "Fulfillment Ord #");
	sublist.addField("custpage_lineno", "text", "Line #");
	sublist.addField("custpage_customer", "text", "Customer");
	sublist.addField("custpage_carrier", "text", "WmsCarrier");
	sublist.addField("custpage_shipmethods", "text", "Ship method");
	sublist.addField("custpage_ordtype", "text", "Order Type");
	sublist.addField("custpage_ordpriority", "text", "Order Priority");
	sublist.addField("custpage_itemname", "text", "Item");
	sublist.addField("custpage_skustatus", "text", "Item Status").setDisplayType('hidden');
	sublist.addField("custpage_skustatustext", "text", "Item Status");
	sublist.addField("custpage_packcode", "text", "Packcode");
	sublist.addField("custpage_lotbatch", "text", "Lot /Batch #");
	sublist.addField("custpage_ordqty", "integer", "Fulfilment Order Qty");
	sublist.addField("custpage_shipdate", "text", "Ship Date").setDisplayType('disabled');
	sublist.addField("custpage_freightterms", "text", "Freight Terms").setDisplayType('disabled');
	sublist.addField("custpage_orderdate", "text", "Order Date").setDisplayType('disabled');
	sublist.addField("custpage_itemno", "text", "Item No").setDisplayType('hidden');
	sublist.addField("custpage_doinernno", "text", "DONo").setDisplayType('hidden');
	sublist.addField("custpage_soinernno", "text", "SONo").setDisplayType('hidden');
	sublist.addField("custpage_uomid", "text", "uomid").setDisplayType('hidden');
	sublist.addField("custpage_location", "text", "location").setDisplayType('hidden');
	sublist.addField("custpage_company", "text", "Company").setDisplayType('hidden');
	sublist.addField("custpage_iteminfo1", "text", "Item Info1").setDisplayType('hidden');
	sublist.addField("custpage_iteminfo2", "text", "Item Info2").setDisplayType('hidden');
	sublist.addField("custpage_iteminfo3", "text", "Item Info3").setDisplayType('hidden');
	sublist.addField("custpage_sointernid", "text", "Item Info3").setDisplayType('hidden');
	sublist.addField("custpage_internalid", "text", "Fulfillment Ord internalid").setDisplayType('hidden');
}


function addFulfilmentOrderToSublist(form, currentOrder, i){
	form.getSubList('custpage_items').setLineItemValue('custpage_lineno', i + 1,
			currentOrder.getValue('custrecord_ordline'));
	form.getSubList('custpage_items').setLineItemValue('custpage_customer', i + 1,
			currentOrder.getText('custrecord_do_customer'));
	form.getSubList('custpage_items').setLineItemValue('custpage_carrier', i + 1,
			currentOrder.getText('custrecord_do_wmscarrier'));
	form.getSubList('custpage_items').setLineItemValue('custpage_ordtype', i + 1,
			currentOrder.getText('custrecord_do_order_type'));
	form.getSubList('custpage_items').setLineItemValue('custpage_ordpriority', i + 1, 
			currentOrder.getText('custrecord_do_order_priority'));
	form.getSubList('custpage_items').setLineItemValue('custpage_itemno', i + 1, 
			currentOrder.getValue('custrecord_ebiz_linesku'));
	form.getSubList('custpage_items').setLineItemValue('custpage_itemname', i + 1, 
			currentOrder.getText('custrecord_ebiz_linesku'));
	form.getSubList('custpage_items').setLineItemValue('custpage_ordqty', i + 1, 
			currentOrder.getValue('custrecord_ord_qty'));
	form.getSubList('custpage_items').setLineItemValue('custpage_sono', i + 1, 
			currentOrder.getValue('custrecord_lineord'));
	form.getSubList('custpage_items').setLineItemValue('custpage_doinernno', i + 1, currentOrder.getId());
	form.getSubList('custpage_items').setLineItemValue('custpage_soinernno', i + 1, 
			currentOrder.getValue('name'));
	form.getSubList('custpage_items').setLineItemValue('custpage_skustatustext', i + 1, 
			currentOrder.getText('custrecord_linesku_status'));			
	form.getSubList('custpage_items').setLineItemValue('custpage_skustatus', i + 1, 
			currentOrder.getValue('custrecord_linesku_status'));
	form.getSubList('custpage_items').setLineItemValue('custpage_packcode', i + 1, 
			currentOrder.getText('custrecord_linepackcode'));
	form.getSubList('custpage_items').setLineItemValue('custpage_uomid', i + 1, 
			currentOrder.getText('custrecord_lineuom_id'));
	form.getSubList('custpage_items').setLineItemValue('custpage_lotbatch', i + 1, 
			currentOrder.getValue('custrecord_batch'));
	form.getSubList('custpage_items').setLineItemValue('custpage_location', i + 1, 
			currentOrder.getValue('custrecord_ordline_wms_location'));
	form.getSubList('custpage_items').setLineItemValue('custpage_iteminfo1', i + 1, 
			currentOrder.getText('custrecord_fulfilmentiteminfo1'));
	form.getSubList('custpage_items').setLineItemValue('custpage_iteminfo2', i + 1, 
			currentOrder.getText('custrecord_fulfilmentiteminfo2'));
	form.getSubList('custpage_items').setLineItemValue('custpage_iteminfo3', i + 1, 
			currentOrder.getText('custrecord_fulfilmentiteminfo3'));		
	form.getSubList('custpage_items').setLineItemValue('custpage_company', i + 1, 
			currentOrder.getValue('custrecord_ordline_company'));
	form.getSubList('custpage_items').setLineItemValue('custpage_shipdate', i + 1, 
			currentOrder.getValue('custrecord_ebiz_shipdate'));
	form.getSubList('custpage_items').setLineItemValue('custpage_freightterms', i + 1, 
			currentOrder.getText('custrecord_ebiz_freightterms'));
	form.getSubList('custpage_items').setLineItemValue('custpage_orderdate', i + 1, 
			currentOrder.getValue('custrecord_ebiz_orderdate'));
	form.getSubList('custpage_items').setLineItemValue('custpage_internalid', i + 1, 
			currentOrder.getId());
	form.getSubList('custpage_items').setLineItemValue('custpage_shipmethods', i + 1, 
			currentOrder.getText('custrecord_do_carrier'));
}


function getOrdersForWaveGen(request){
	var orderList = new Array();

	// Validating all the request parameters and pushing to a local array
	var localVarArray = validateRequestParams(request);

	// Get all the search filters for wave generation
	var filters = new Array();
	filters = specifyWaveFilters(localVarArray);
	
	// Get all the columns that the search should return
	var columns = new Array();
	columns = getWaveColumns();

	orderList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);

	return orderList;
}
function validateRequestParams(request){
	var soNo = "";
	var companyName = "";
	var itemName= "";
	var customerName = "";
	var orderPriority = "";
	var orderType = "";
	var itemGroup = "";
	var itemFamily = "";
	var packCode = "";
	var UOM = "";
	var itemStatus = "";
	var itemInfo1 = "";
	var itemInfo2 = "";
	var itemInfo3 = "";
	var shippingCarrier = "";
	var shipCountry = ""; 
	var shipState = "";
	var shipCity = "";
	var shipAddr1 = "";
	var shipmentNo = "";
	var routeNo = "";
	var carrier = "";
	var Shipdate= "";
	  var FreightTerms= "";
	  var OrderDate= "";
	  

	var localVarArray = new Array();

	if (request.getParameter('custpage_qbso') != null && request.getParameter('custpage_qbso') != "") {
		soNo = request.getParameter('custpage_qbso');
	}

	if (request.getParameter('custpage_company') != null && request.getParameter('custpage_company') != "") {
		companyName = request.getParameter('custpage_company');
	}

	if (request.getParameter('custpage_item') != null && request.getParameter('custpage_item') != "") {
		itemName = request.getParameter('custpage_item');
	}

	if (request.getParameter('custpage_consignee') != null && request.getParameter('custpage_consignee') != "") {
		customerName = request.getParameter('custpage_consignee');
	}

	if (request.getParameter('custpage_ordpriority') != null && request.getParameter('custpage_ordpriority') != "") {
		orderPriority = request.getParameter('custpage_ordpriority');
	}

	if (request.getParameter('custpage_ordtype') != null && request.getParameter('custpage_ordtype') != "") {
		orderType = request.getParameter('custpage_ordtype');
	}

	if (request.getParameter('custpage_itemgroup') != null && request.getParameter('custpage_itemgroup') != "") {
		itemGroup = request.getParameter('custpage_itemgroup');
	}

	if (request.getParameter('custpage_itemfamily') != null && request.getParameter('custpage_itemfamily') != "") {
		itemFamily = request.getParameter('custpage_itemfamily');
	}

	if (request.getParameter('custpage_packcode') != null && request.getParameter('custpage_packcode') != "") {
		packCode = request.getParameter('custpage_packcode');//custrecord_linepackcode
	}

	if (request.getParameter('custpage_uom') != null && request.getParameter('custpage_uom') != "") {
		UOM = request.getParameter('custpage_uom');//custrecord_lineuom_id
	}

	if (request.getParameter('custpage_itemstatus') != null && request.getParameter('custpage_itemstatus') != "") {
		itemStatus = request.getParameter('custpage_itemstatus'); //custrecord_linesku_status
	}

	if (request.getParameter('custpage_siteminfo1')!=null && request.getParameter('custpage_siteminfo1')!="" ){
		itemInfo1=request.getParameter('custpage_siteminfo1'); 
	}

	if (request.getParameter('custpage_siteminfo2')!=null && request.getParameter('custpage_siteminfo2')!="" ){
		itemInfo2=request.getParameter('custpage_siteminfo2'); 
	}

	if (request.getParameter('custpage_siteminfo3')!=null && request.getParameter('custpage_siteminfo3')!="" ){
		itemInfo3=request.getParameter('custpage_siteminfo3'); 
	}

	if (request.getParameter('custpage_shippingcarrier')!=null && request.getParameter('custpage_shippingcarrier')!="" ){
		shippingCarrier=request.getParameter('custpage_shippingcarrier'); 
	}

	if (request.getParameter('custpage_country')!=null && request.getParameter('custpage_country')!="" ){
		shipCountry=request.getParameter('custpage_country'); 
	}

	if (request.getParameter('custpage_state')!=null && request.getParameter('custpage_state')!="" ){
		shipState=request.getParameter('custpage_country'); 
	}

	if (request.getParameter('custpage_city')!=null && request.getParameter('custpage_city')!="" ){
		shipCity=request.getParameter('custpage_city'); 
	}

	if (request.getParameter('custpage_addr1')!=null && request.getParameter('custpage_addr1')!="" ){
		shipAddr1=request.getParameter('custpage_addr1'); 
	}

	if (request.getParameter('custpage_shipmentno')!=null && request.getParameter('custpage_shipmentno')!="" ){
		shipmentNo=request.getParameter('custpage_shipmentno'); 
	}

	if (request.getParameter('custpage_routeno')!=null && request.getParameter('custpage_routeno')!="" ){
		routeNo=request.getParameter('custpage_routeno'); 
	}
	//added carrier By Suman
	if (request.getParameter('custpage_carrier')!=null && request.getParameter('custpage_carrier')!="" ){
		carrier=request.getParameter('custpage_carrier'); 
	}
	
	if (request.getParameter('custpage_soshipdate')!=null && request.getParameter('custpage_soshipdate')!="" ){
		Shipdate=request.getParameter('custpage_soshipdate'); 
	}
	
	if (request.getParameter('custpage_sofrieghtterms')!=null && request.getParameter('custpage_sofrieghtterms')!="" ){
		FreightTerms=request.getParameter('custpage_sofrieghtterms'); 		
	}
	
	if (request.getParameter('custpage_soorderdate')!=null && request.getParameter('custpage_soorderdate')!="" ){
		OrderDate=request.getParameter('custpage_soorderdate'); 
	}

	var currentRow = [soNo, companyName, itemName, customerName, orderPriority, orderType, itemGroup, itemFamily,
	                  packCode, UOM, itemStatus, itemInfo1, itemInfo2, itemInfo3, shippingCarrier, shipCountry, 
	                  shipState, shipCity, shipAddr1, shipmentNo, routeNo, carrier,Shipdate,FreightTerms,OrderDate];
	
	//nlapiLogExecution('ERROR', 'currentRow', currentRow);
	localVarArray.push(currentRow);
	return localVarArray;
}
function specifyWaveFilters(localVarArray){
	var filters = new Array();

	// Sales Order No
	if(localVarArray[0][0] != "")
		filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', localVarArray[0][0]));

	// Company
	if(localVarArray[0][1] != "")
		filters.push(new nlobjSearchFilter('custrecord_ordline_company', null, 'is', localVarArray[0][1]));

	// Item Name
	if(localVarArray[0][2] != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_linesku', null, 'is', localVarArray[0][2]));

	// Customer Name
	if(localVarArray[0][3] != "")
		filters.push(new nlobjSearchFilter('custrecord_do_customer', null, 'is', localVarArray[0][3]));

	// Order Priority
	if(localVarArray[0][4] != "")
		filters.push(new nlobjSearchFilter('custrecord_do_order_priority', null, 'is', localVarArray[0][4]));

	// Order Type
	if(localVarArray[0][5] != "")
		filters.push(new nlobjSearchFilter('custrecord_do_order_type', null, 'is', localVarArray[0][5]));

	// Item Group
	if(localVarArray[0][6] != "")
		filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', localVarArray[0][6]));

	// Item Family
	if(localVarArray[0][7] != "")
		filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', localVarArray[0][7]));

	// Pack Code
	if(localVarArray[0][8] != "")
		filters.push(new nlobjSearchFilter('custrecord_linepackcode', null, 'is', localVarArray[0][8]));

	// UOM
	if(localVarArray[0][9] != "")
		filters.push(new nlobjSearchFilter('custrecord_lineuom_id', null, 'is', localVarArray[0][9]));

	// Item Status
	if(localVarArray[0][10] != "")
		filters.push(new nlobjSearchFilter('custrecord_linesku_status', null, 'is', localVarArray[0][10]));

	// Item Info 1
	if(localVarArray[0][11] != "")
		filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo1', null, 'is', localVarArray[0][11]));

	// Item Info 2
	if(localVarArray[0][12] != "")
		filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo2', null, 'is', localVarArray[0][12]));

	// Item Info 3
	if(localVarArray[0][13] != "")
		filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo3', null, 'is', localVarArray[0][13]));

	// Shipping Carrier
	if(localVarArray[0][14] != "")
		filters.push(new nlobjSearchFilter('shipmethod', 'custrecord_ns_ord', 'anyof', [localVarArray[0][14]]));

	// Ship Country
	if(localVarArray[0][15] != "")
		filters.push(new nlobjSearchFilter('shipcountry', 'custrecord_ns_ord', 'anyof', [localVarArray[0][15]]));

	// Ship State
	if(localVarArray[0][16] != "")
		filters.push(new nlobjSearchFilter('shipstate', 'custrecord_ns_ord', 'is', localVarArray[0][16]));

	// Ship City
	if(localVarArray[0][17] != "")
		filters.push(new nlobjSearchFilter('shipcity', 'custrecord_ns_ord', 'is', localVarArray[0][17]));

	// Masquerading the ship address with ship city
	if(localVarArray[0][18] != "")
		filters.push(new nlobjSearchFilter('shipaddress', 'custrecord_ns_ord', 'is', localVarArray[0][17]));

	// Shipment No.
	if(localVarArray[0][19] != "")
		filters.push(new nlobjSearchFilter('custrecord_shipment_no', null, 'is', localVarArray[0][19]));

	// Route No.
	if(localVarArray[0][20] != "")
		filters.push(new nlobjSearchFilter('custrecord_route_no', null, 'anyof', [localVarArray[0][20]]));

	// WMS Status Flag = 'E' (i.e. sales order is ready to be in a wave)
	filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'is', '25'));

	// Indicates the Sales Order header
	filters.push(new nlobjSearchFilter('mainline', 'custrecord_ns_ord', 'is', 'T'));			
	// carrier Added By Suman
	if(localVarArray[0][21] != "")
		filters.push(new nlobjSearchFilter('custrecord_do_wmscarrier', null, 'anyof', [localVarArray[0][21]]));
	
	if(localVarArray[0][22] != "")
		{
		var myDate = nlapiStringToDate(localVarArray[0][22]);
		filters.push(new nlobjSearchFilter('custrecord_ebiz_shipdate', null, 'on', myDate));}
	
	if(localVarArray[0][23] != "")
		{
		//nlapiLogExecution('ERROR', 'freightterms', localVarArray[0][23]);
		var freightterms=(localVarArray[0][23]);
		filters.push(new nlobjSearchFilter('custrecord_ebiz_freightterms', null, 'is', freightterms));}
	
	if(localVarArray[0][24] != "")
		{
		filters.push(new nlobjSearchFilter('custrecord_ebiz_orderdate', null, 'on', localVarArray[0][24]));}
	return filters;
}
function getWaveColumns(){
	var columns = new Array();

	columns[0] = new nlobjSearchColumn('custrecord_linesku_status');
	columns[1] = new nlobjSearchColumn('custrecord_do_customer');
	columns[2] = new nlobjSearchColumn('custrecord_do_carrier');
	columns[3] = new nlobjSearchColumn('custrecord_do_order_type');
	columns[4] = new nlobjSearchColumn('custrecord_do_order_priority');
	columns[5] = new nlobjSearchColumn('custrecord_lineord');
	columns[6] = new nlobjSearchColumn('custrecord_ebiz_linesku');
	columns[7] = new nlobjSearchColumn('custrecord_ord_qty');
	columns[8] = new nlobjSearchColumn('name');
	columns[8].setSort();
	columns[9] = new nlobjSearchColumn('custrecord_lineord');
	columns[9].setSort();		
	columns[10] = new nlobjSearchColumn('custrecord_ordline');
	columns[10].setSort();
	columns[11] = new nlobjSearchColumn('custrecord_linepackcode');
	columns[12] = new nlobjSearchColumn('custrecord_lineuom_id');
	columns[13] = new nlobjSearchColumn('custrecord_batch');
	columns[14] = new nlobjSearchColumn('custrecord_ordline_wms_location');
	columns[15] = new nlobjSearchColumn('custrecord_ordline_company');
	columns[16] = new nlobjSearchColumn('custrecord_fulfilmentiteminfo1');
	columns[17] = new nlobjSearchColumn('custrecord_fulfilmentiteminfo2');
	columns[18] = new nlobjSearchColumn('custrecord_fulfilmentiteminfo3');	
	columns[19] = new nlobjSearchColumn('custrecord_ebiz_shipdate');	
	columns[20] = new nlobjSearchColumn('custrecord_ebiz_freightterms');	
	columns[21] = new nlobjSearchColumn('custrecord_ebiz_orderdate');	
	columns[22]=new nlobjSearchColumn('custrecord_do_wmscarrier');
	return columns;
}
