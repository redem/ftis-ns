/***************************************************************************
	  		   eBizNET Solutions Inc               
 ****************************************************************************
/* Prologue - 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Masters/Client/ebiz_CheckItemDimensions_CL.js,v $
 *     	   $Revision: 1.4.10.2.4.5.2.2 $
 *     	   $Date: 2014/08/14 10:16:42 $
 *     	   $Author: sponnaganti $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_CheckItemDimensions_CL.js,v $
 * Revision 1.4.10.2.4.5.2.2  2014/08/14 10:16:42  sponnaganti
 * case# 20149924
 * stnd bundle issue fix
 *
 * Revision 1.4.10.2.4.5.2.1  2014/08/01 14:06:10  rmukkera
 * Case # 20148908
 *
 * Revision 1.4.10.2.4.5  2014/06/11 15:32:07  skavuri
 * Case# 20148859 SB Issue Fixed
 *
 * Revision 1.4.10.2.4.4  2014/05/26 15:00:50  skreddy
 * case # 20148503/20148525
 * LL SB and Standard  issue fix
 *
 * Revision 1.4.10.2.4.3  2013/10/25 16:11:39  skreddy
 * Case# 20125320
 * standard bundle  issue fix
 *
 * Revision 1.4.10.2.4.2  2013/09/11 15:23:51  rmukkera
 * Case# 20124376
 *
 * Revision 1.4.10.2.4.1  2013/07/25 06:57:50  skreddy
 * Case# 201214768
 * Item dimensions for pallet
 *
 * Revision 1.4.10.2  2012/11/01 14:55:22  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.4.10.1  2012/09/26 22:43:33  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue Fixes for FISK,BOOMBAH and TDG.
 *
 * Revision 1.4  2012/01/04 21:10:32  skota
 * CASE201112/CR201113/LOG201121
 * Added null filter to site field and base uom filter
 *
 * Revision 1.3  2011/12/23 12:48:21  skota
 * CASE201112/CR201113/LOG201121
 * Added null filter to company field and base uom filter
 *
 * Revision 1.2  2011/09/20 12:29:53  mbpragada
 * CASE201112/CR201113/LOG201121
 * Added Validations for Item Dimensions
 *
  
 *****************************************************************************/
/**
 * This function is used to check below mentioned 2 conditions.
 * 1. To check whether the item dimensions already exists or not for given item.
 * 2. To check whether the base UOM already exists or not for given item.
 */
function fnCheckItemDimensions(type){

	var vItemDimensionName = nlapiGetFieldValue('name');
	var vSite = nlapiGetFieldValue('custrecord_ebizsiteskudim');
	var vCompany = nlapiGetFieldValue('custrecord_ebizcompanyskudimension');
	var vItem = nlapiGetFieldValue('custrecord_ebizitemdims');
	var vPackCode = nlapiGetFieldValue('custrecord_ebizpackcodeskudim');
	var vUOM = nlapiGetFieldValue('custrecord_ebizuomskudim');
	var baseUOMflag = nlapiGetFieldValue('custrecord_ebizbaseuom');
	var id = nlapiGetFieldValue('id');

	nlapiLogExecution('ERROR', 'vItemDimensionName ', vItemDimensionName);
	nlapiLogExecution('ERROR', 'vSite ', vSite);
	nlapiLogExecution('ERROR', 'vCompany ', vCompany);
	nlapiLogExecution('ERROR', 'vItem ', vItem);
	nlapiLogExecution('ERROR', 'vPackCode ', vPackCode);
	nlapiLogExecution('ERROR', 'vUOM ', vUOM);
	nlapiLogExecution('ERROR', 'baseUOMflag ', baseUOMflag);	 
	nlapiLogExecution('ERROR', 'id ', id);

	//case 20125320 start : added if condition if ID is not null
	/*if(id !=null && id !='')
	{*/
	//case 20125320 end

		var filters = new Array();

		if (vSite != null && vSite != "") 
		{	
			filters.push( new nlobjSearchFilter('custrecord_ebizsiteskudim', null, 'anyof', ['@NONE@',parseFloat(vSite)]));
		}
		filters.push( new nlobjSearchFilter('custrecord_ebizcompanyskudimension', null, 'anyof', ['@NONE@',vCompany]));
		filters.push( new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', [vItem]));
		filters.push( new nlobjSearchFilter('custrecord_ebizpackcodeskudim', null, 'is', vPackCode));
		filters.push( new nlobjSearchFilter('custrecord_ebizuomskudim', null, 'anyof', [vUOM]));
		filters.push( new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		if (id != null && id != "") 
		{	
			filters.push( new nlobjSearchFilter('internalid', null, 'noneof', parseFloat(id)));
		}		

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizuomskudim');	

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);	

		if (searchresults != null && searchresults != '' && searchresults.length > 0) 
		{  		
			alert("The item dimensions already exists.");
			return false;

		}
		else {
			if(baseUOMflag == 'T'){
				var filters1 = new Array();		
				nlapiLogExecution('ERROR', 'into else filters: ', 'sucess');
				filters1.push( new nlobjSearchFilter('custrecord_ebizsiteskudim', null, 'anyof', ['@NONE@',vSite]));
				filters1.push(new nlobjSearchFilter('custrecord_ebizcompanyskudimension', null, 'anyof', ['@NONE@',vCompany]));
				filters1.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', [vItem]));
				filters1.push(new nlobjSearchFilter('custrecord_ebizpackcodeskudim', null, 'is', vPackCode));	
				filters1.push(new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T'));			
				filters1.push( new nlobjSearchFilter('isinactive', null, 'is', 'F'));			
				if (id != null && id != "") 
				{
					filters1.push(new nlobjSearchFilter('internalid', null, 'noneof', id));
				}		 
				var columns1 = new Array();			 
				columns1[0] = new nlobjSearchColumn('custrecord_ebizbaseuom');

				var searchresults1 = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters1,columns1);	
				if(searchresults1 != null && searchresults1 !="")
				{
					alert("The base UOM already exists.");
					return false;				

			}

		}
		/*else
			{*/
		var vLength = nlapiGetFieldValue('custrecord_ebizlength');
		var vHeight = nlapiGetFieldValue('custrecord_ebizheight');
		var vWidth = nlapiGetFieldValue('custrecord_ebizwidth');

		if (vLength == "") {
			alert("Enter Length of the item");
			return false;
		}
		else 
			if (vHeight == "") {
				alert("Enter Height of the item");
				return false;
			}
			else 
				if (vWidth == "") {
					alert("Enter Width of the item");
					return false;
				}
				else 
					if (vLength == "" && vHeight == "" && vWidth == "") {
						alert("Enter the item dimensions");
						return false;
					}
					else
					{
						var vCube = parseFloat(vLength * vHeight * vWidth);
						if(parseFloat(vCube)>0)
						{
							nlapiSetFieldValue('custrecord_ebizcube', parseFloat(vCube).toFixed(4));
						}
						else
						{
							nlapiSetFieldValue('custrecord_ebizcube', parseFloat(vCube));
							alert('Item cube should not be zero ');
							return false;
						}
					}



		//}
//		case201214768 start
		//var vUOM = nlapiGetFieldValue('custrecord_ebizuomskudim');
		//alert(vUOM);
		//case# 20149924 (below code is commented because it is not applicable to stnd bundle)
		/*if(vUOM =='3')
		{
			//alert('hi');
			nlapiSetFieldValue('custrecord_ebizdims_packflag', 3);
		}
		else
		{
			nlapiSetFieldValue('custrecord_ebizdims_packflag', '');
		}*/

//		end
	}
	//}
	return true;
}

//case201214768 start

function UOMchange(type, fld)
{
	try{
		//alert('fld'+fld);
		//case# 20149924 (below code is commented because it is not applicable to stnd bundle)
		/*if (fld == "custrecord_ebizuomskudim") 
		{

			var vUOM = nlapiGetFieldValue('custrecord_ebizuomskudim');
			//alert(vUOM);

			if(vUOM =='3')
			{
				//alert('hi');
				nlapiSetFieldValue('custrecord_ebizdims_packflag', 3);
			}
			else
			{
				nlapiSetFieldValue('custrecord_ebizdims_packflag', '');
			}
		}*/
	
	}
	
	catch(e)
	{
		
	}

}

//end
