/***************************************************************************
�eBizNET Solutions Inc
 ****************************************************************************
 *
 *� $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_Outbound_ReversalQb_SL.js,v $
 *� $Revision: 1.1.2.2.4.3.4.6.2.1 $
 *� $Date: 2015/11/09 16:08:21 $
 *� $Author: aanchal $
 *� $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_Outbound_ReversalQb_SL.js,v $
 *� Revision 1.1.2.2.4.3.4.6.2.1  2015/11/09 16:08:21  aanchal
 *� 2015.2 Issue fix
 *� 201415283
 *�
 *� Revision 1.1.2.2.4.3.4.6  2014/07/11 15:39:11  sponnaganti
 *� Case# 20149417
 *� Compatibility Issue fix
 *�
 *� Revision 1.1.2.2.4.3.4.5  2014/07/08 15:50:47  sponnaganti
 *� case# 20149322
 *� Compatibility issue fix
 *�
 *� Revision 1.1.2.2.4.3.4.4  2014/05/09 16:01:38  skavuri
 *� Case # 20148342 SB Issue Fixed
 *�
 *� Revision 1.1.2.2.4.3.4.3  2013/03/21 14:18:14  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Prod and UAT issue fixes.
 *�
 *� Revision 1.1.2.2.4.3.4.2  2013/02/27 13:59:09  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Merged from Monobind.
 *�
 *� Revision 1.1.2.2.4.3.4.1  2013/02/27 13:06:28  rmukkera
 *� monobid production bundle changes were merged to cvs with tag
 *� t_eBN_2013_1_StdBundle_2
 *�
 *� Revision 1.1.2.2.4.3  2012/12/03 16:41:00  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Additional text fields order field ,serial no# and carton no#
 *�
 *� Revision 1.1.2.2.4.2  2012/11/01 14:55:02  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Decimal Qty Conversions
 *�
 *� Revision 1.1.2.2.4.1  2012/10/01 05:18:35  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Merged code from 2012.2.
 *�
 *� Revision 1.1.2.2  2012/07/23 06:32:09  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Issue fixes
 *�
 *� Revision 1.1.2.1  2012/06/22 12:30:23  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Outbound Reversal
 *�

 *
 ****************************************************************************/

function OutboundReversalQbSuitelet(request, response){
	var ctx = nlapiGetContext();
	if (request.getMethod() == 'GET') 
	{
		var context = nlapiGetContext();
		nlapiLogExecution('ERROR','Remaining usage at the start of GET',context.getRemainingUsage());

		var form = nlapiCreateForm('Outbound Reversal');

		//var orderField = form.addField('custpage_orderlist', 'select', 'Sales Order #').setLayoutType('startrow', 'none');
		//case# 20149322 starts
		//var orderField = form.addField('custpage_orderlist', 'text', 'Sales Order # / TRANSFER ORDER #');// Case# 20148342
		var orderField = form.addField('custpage_orderlist', 'text', 'S0# /T0# / VRA#');//case# 20149417
		//case# 20149322 ends
		
		//orderField.addSelectOption("", "");
		var cartonField = form.addField('custpage_cartonlist', 'select', 'Carton #').setLayoutType('startrow', 'none');
		cartonField.addSelectOption("", "");

		//var salesOrderList = getAllSalesOrders(-1);
		//addAllSalesOrdersToField(form,orderField, salesOrderList);

		var cartonList = getAllcartons(-1);
		addAllCartonsToField(form,cartonField, cartonList);


		//fillOrderField(form, orderField,cartonField,-1);

		var serialField = form.addField('custpage_seriallist', 'text', 'Serial #').setLayoutType('startrow', 'none');

		nlapiLogExecution('ERROR','Remaining usage at the end of GET',context.getRemainingUsage());

		var button = form.addSubmitButton('Display');

		response.writePage(form);
	}
	else
	{
		var SoId = "";
		if(request.getParameter('custpage_orderlist') != null && request.getParameter('custpage_orderlist') != '')
			SoId=GetSOInternalId(request.getParameter('custpage_orderlist'));
		var ordArray = new Array();		
		ordArray["custpage_orderlist"] = SoId;
		ordArray["custpage_cartonlist"] = request.getParameter('custpage_cartonlist');
		ordArray["custpage_seriallist"] = request.getParameter('custpage_seriallist');

		response.sendRedirect('SUITELET', 'customscript_outboundreversal','customdeploy_outboundreversal', false, ordArray);

	}
}

function addAllSalesOrdersToField(form,OrderField, salesOrderList){
	if(salesOrderList != null && salesOrderList.length > 0){
		for (var i = 0; i < salesOrderList.length; i++) 
		{
			var res=  form.getField('custpage_orderlist').getSelectOptions(salesOrderList[i].getText('custrecord_ebiz_order_no'), 'is');
			if (res != null) {
				if (res.length > 0) {
					continue;
				}
			}
			OrderField.addSelectOption(salesOrderList[i].getValue('custrecord_ebiz_order_no'), salesOrderList[i].getText('custrecord_ebiz_order_no'));
		}
	}
}

function addAllCartonsToField(form,cartonField, cartonList){
	if(cartonList != null && cartonList.length > 0){
		for (var i = 0; i < cartonList.length; i++) 
		{
			var res=  form.getField('custpage_cartonlist').getSelectOptions(cartonList[i].getValue('custrecord_container_lp_no'), 'is');
			if (res != null) {
				if (res.length > 0) {
					continue;
				}
			}
			cartonField.addSelectOption(cartonList[i].getValue('custrecord_container_lp_no'), cartonList[i].getValue('custrecord_container_lp_no'));
		}
	}
}



var vOrdArr=new Array();
function getAllSalesOrders(maxno){
	var filtersso = new Array();		

	//9-Picks Generated, 14-Shipped, 15-Selected Into Wave, 25-Edit, 26-Picks Failed, 29-Closed, 30-Short Pick
	filtersso.push(new nlobjSearchFilter( 'custrecord_wms_status_flag', null, 'noneof', [9,14,15,25,26,29,30]));
	//3 - PICK
	filtersso.push(new nlobjSearchFilter( 'custrecord_tasktype', null, 'anyof', [3]));
	/* This filter condition is merged from Monobind production bundle on 27th feb 2013 by Radhika */                     
	filtersso.push(new nlobjSearchFilter( 'type', 'custrecord_ebiz_order_no', 'anyof', ['SalesOrd','TrnfrOrd']));

	if(maxno!=-1)
	{
		filtersso.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno)));
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('id');	 
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_order_no');


	//columns[0].setSort(true);
	columns[1].setSort(true);

	var customerSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso,columns);

	for (var i = 0; customerSearchResults != null && i < customerSearchResults.length; i++) {


		vOrdArr.push(customerSearchResults[i]);
	}

	if(customerSearchResults!=null && customerSearchResults.length>=1000)
	{
		var maxno=customerSearchResults[customerSearchResults.length-1].getValue('id');		
		getAllSalesOrders(maxno);	
	}
	return vOrdArr;
}

var vContArr=new Array();
function getAllcartons(maxno){
	var filtersso = new Array();		

	//9-Picks Generated, 14-Shipped, 15-Selected Into Wave, 25-Edit, 26-Picks Failed, 29-Closed, 30-Short Pick
	filtersso.push(new nlobjSearchFilter( 'custrecord_wms_status_flag', null, 'noneof', [9,14,15,25,26,29,30]));
	//3 - PICK
	filtersso.push(new nlobjSearchFilter( 'custrecord_tasktype', null, 'anyof', [3]));
	filtersso.push(new nlobjSearchFilter( 'custrecord_container_lp_no', null, 'isnotempty'));
	if(maxno!=-1)
	{
		filtersso.push(new nlobjSearchFilter('id', null, 'lessthan', parseInt(maxno)));
	}



	var columns = new Array();
	columns[0] = new nlobjSearchColumn('id');	 
	columns[1] = new nlobjSearchColumn('custrecord_container_lp_no');

	//columns[0].setSort(true);
	//columns[1].setSort();
	columns[1].setSort(true);

	//


	var customerSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso,columns);

	for (var i = 0; customerSearchResults != null && i < customerSearchResults.length; i++) {


		vContArr.push(customerSearchResults[i]);
	}

	if(customerSearchResults!=null && customerSearchResults.length>=1000)
	{
		var maxno=customerSearchResults[customerSearchResults.length-1].getValue('id');		
		getAllcartons(maxno);	
	}
	return vContArr;
}

function removeDuplicateOrders(arrayName)
{
	var newArray=new Array();
	label:for(var i=0; arrayName!=null && i<arrayName.length;i++ )
	{  
		for(var j=0; j<newArray.length;j++ )
		{
			if(newArray[j].getValue('custrecord_ebiz_order_no')==arrayName[i].getValue('custrecord_ebiz_order_no')) 
				continue label;
		}
		newArray[newArray.length] = arrayName[i];
	}
	return newArray;
}

function GetSOInternalId(SOText)
{
	nlapiLogExecution('ERROR','Into GetSOInternalId (Input)',SOText);

	var ActualSoID='-1';

	var filter=new Array();
	filter.push(new nlobjSearchFilter('tranid',null,'is',SOText.toString()));
	filter.push(new nlobjSearchFilter('mainline',null,'is','F'));

	var searchrec=nlapiSearchRecord('salesorder',null,filter,null);
	if(searchrec==null)
	{
		searchrec=nlapiSearchRecord('transferorder',null,filter,null);
	}
	if(searchrec==null)
	{
		nlapiLogExecution('ERROR','vendorreturnauthorization');
		searchrec=nlapiSearchRecord('vendorreturnauthorization',null,filter,null);
	}
	
	if(searchrec!=null && searchrec!='' && searchrec.length>0)
	{
		ActualSoID=searchrec[0].getId();
	}
	nlapiLogExecution('ERROR','searchrec',searchrec);
	nlapiLogExecution('ERROR','ActualSoID',ActualSoID);
	
	
	nlapiLogExecution('ERROR','Out of GetSOInternalId (Output)',ActualSoID);

	return ActualSoID;
}