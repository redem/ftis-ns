/***************************************************************************
  eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_WOPutawayConfirmLoc.js,v $
 *     	   $Revision: 1.1.2.2.4.24.2.4 $
 *     	   $Date: 2015/11/20 14:52:05 $
 *     	   $Author: skreddy $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_WOPutawayConfirmLoc.js,v $
 * Revision 1.1.2.2.4.24.2.4  2015/11/20 14:52:05  skreddy
 * case 201415773
 * 2015.2 issue fix
 *
 * Revision 1.1.2.2.4.24.2.3  2015/11/05 14:18:11  schepuri
 * case# 201415362
 *
 * Revision 1.1.2.2.4.24.2.2  2015/10/20 08:09:30  nneelam
 * case# 201415121
 *
 * Revision 1.1.2.2.4.24.2.1  2015/10/08 15:23:08  aanchal
 * 2015.2 issues
 * 201414923
 *
 * Revision 1.1.2.2.4.24  2015/08/21 15:34:15  nneelam
 * case# 201414092
 *
 * Revision 1.1.2.2.4.23  2015/08/03 15:45:15  skreddy
 * Case# 201413770
 * 2015.2 compatibility issue fix
 *
 * Revision 1.1.2.2.4.22  2015/04/13 09:25:59  rrpulicherla
 * Case#201412277
 *
 * Revision 1.1.2.2.4.21  2015/01/21 13:40:39  schepuri
 * issue # 201411366
 *
 * Revision 1.1.2.2.4.20  2014/11/18 15:40:17  skavuri
 * Case# 201410445 Std bundle issue fixed
 *
 * Revision 1.1.2.2.4.19  2014/08/18 13:55:00  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * optimistic locking changes
 *
 * Revision 1.1.2.2.4.18  2014/08/08 15:09:54  sponnaganti
 * Case# 20147959
 * Stnd Bundle Issue fix
 *
 * Revision 1.1.2.2.4.17  2014/08/06 15:26:32  skavuri
 * Case# 20149843 SB Issue Fixed
 *
 * Revision 1.1.2.2.4.16  2014/08/04 09:10:20  nneelam
 * case#  20149371
 * Stanadard Bundle Partial WO Issue Fix.
 *
 * Revision 1.1.2.2.4.15  2014/07/10 07:04:31  skavuri
 * Case# 20149080 Compatibility Issue Fixed
 *
 * Revision 1.1.2.2.4.14  2014/06/17 15:07:39  rmukkera
 * Case # 20148420�
 *
 * Revision 1.1.2.2.4.13  2014/06/13 08:43:11  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.2.4.12  2014/06/06 07:41:03  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.1.2.2.4.11  2014/05/30 00:34:25  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.2.4.10  2014/05/14 14:04:25  gkalla
 * case#20148019
 * Stage records not clearing
 *
 * Revision 1.1.2.2.4.9  2014/03/24 08:14:53  nneelam
 * case#  20124024
 * Roynet Issue Fix.
 *
 * Revision 1.1.2.2.4.8  2014/03/20 14:14:04  skreddy
 * case 20127784
 * New Demo account issue fix
 *
 * Revision 1.1.2.2.4.7  2014/01/09 14:08:58  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.1.2.2.4.6  2014/01/06 15:40:13  nneelam
 * case# 20126662,20126660
 * Std Bundle Issue Fix.
 *
 * Revision 1.1.2.2.4.5  2014/01/06 13:16:03  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.1.2.2.4.4  2013/08/21 14:21:24  skreddy
 * Case# 20123541
 * issue rellated to expiry date for item in NS
 *
 * Revision 1.1.2.2.4.3  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.2.4.2  2013/03/19 11:48:12  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.1.2.2.4.1  2013/03/08 14:38:41  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.1.2.2  2012/12/03 15:43:22  rmukkera
 * CASE201112/CR201113/LOG2012392
 * Issue fix related to cannot read length from null
 *
 * Revision 1.1.2.1  2012/11/23 09:40:27  skreddy
 * CASE201112/CR201113/LOG201121
 * RF version of WO Assembly building process
 *
 * Revision 1.23.2.41.4.3  2012/10/03 11:33:31  schepuri
 * CASE201112/CR201113/LOG201121
 * NSUOM Convertion
 *
 *
 *
 *****************************************************************************/

function WOPutawayLocation(request, response){
	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);
	if (request.getMethod() == 'GET') {
		var getRecordCount = request.getParameter('custparam_recordcount');
		nlapiLogExecution('ERROR', 'getFetchedLocationId ', getFetchedLocation);

		var getconfirmedLPCount = request.getParameter('custparam_confirmedLpCount');
		var getlpCount = request.getParameter('custparam_lpCount');
		var TempLPNoArray = request.getParameter('custparam_lpNumbersArray');
		nlapiLogExecution('ERROR', 'confirmedLPCount ', getconfirmedLPCount);
		nlapiLogExecution('ERROR', 'lpCount ', getlpCount);
		nlapiLogExecution('ERROR', 'TempLPNoArray ', TempLPNoArray);
		//	 Get the LP#, Quantity, Location 
		//  from the previous screen, which is passed as a parameter	
		var getLPNo = request.getParameter('custparam_lpno');
		var getQuantity = request.getParameter('custparam_quantity');
		var getFetchedLocation = request.getParameter('custparam_beginlocation');
		var getFetchedItem = request.getParameter('custparam_item');
		var getFetchedItemText = request.getParameter('custparam_itemtext');
		var getFetchedItemDescription = request.getParameter('custparam_itemDescription');
		var getFetchedLocationId = request.getParameter('custparam_location');
		var getWoInternalId = request.getParameter('custparam_ebizordno');
		var getLineNo = request.getParameter('custparam_lineno');
		var EnteredLpNo = request.getParameter('custparam_enteredLPNo');
		var exceptionqtyflag = request.getParameter('custparam_exceptionQuantityflag');
		var exceptionqty = request.getParameter('custparam_exceptionquantity');
		var getActQuantity = request.getParameter('custparam_quantity');
		var getRecordId = request.getParameter('custparam_recordid');
		var getWHLocation = request.getParameter('custparam_whlocation');
		var getWONo = request.getParameter('custparam_woname');
		var getBatchNo = request.getParameter('custparam_batchno');



		var getCartLPNo = request.getParameter('custparam_cartno');
		nlapiLogExecution('ERROR', 'getCartLPNo ', getCartLPNo);
		nlapiLogExecution('ERROR', 'getFetchedItem ', getFetchedItem);

		var getOptedField = request.getParameter('custparam_option');



		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);


		var st0,st1,st2,st3,st4,st5,st6,st7,st8,st9;

		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "IR A PUNTO DE"; 
			st2 = "INGRESAR / ESCANEAR UBICACI&#211;N";			
			st3 = "ART&#205;CULO";
			st4 = "PLACA";
			st5 = "CONT";
			st6 = "ANTERIOR";	
			st7 = "";
			st8 = "";

		}
		else
		{
			st0 = "WO#";
			st1 = "CONFIRM LOCATION"; 
			st2 = "ENTER/SCAN LOCATION";			
			st3 = "ASSEMBLY ITEM";
			st4 = "LP";
			st5 = "CONF";
			st6 = "PREV";
			st7 = "BUILD QTY";
			st8	= "LOT#";
			st9 = "LOC EXCEPTION";

		}




		var functionkeyHtml=getFunctionkeyScript('_rf_putaway_lp'); 
		var html = "<html><head><title>ASSEMBLY BUILD</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		//html = html + " document.getElementById('enterlocation').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_putaway_lp' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st0 + " : <label>" + getWONo + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " : <label>" + getFetchedItemText + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";

		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st7 + " : <label>" + getQuantity + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";	

		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st8 + " : <label>" + getBatchNo + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";	



		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st4 + " : <label>" + getLPNo + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";		
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + ": <label>" + getFetchedLocation + "</label>";
		html = html + "				<input type='hidden' name='hdnLPNo' value=" + getLPNo + "></td>";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getQuantity + "></td>";
		html = html + "				<input type='hidden' name='hdnFetchedLocation' value=" + getFetchedLocation + "></td>";
		html = html + "				<input type='hidden' name='hdnFetchedItem' value=" + getFetchedItem + "></td>";
		html = html + "				<input type='hidden' name='hdnFetchedItemDescription' value=" + getFetchedItemDescription + "></td>";
		html = html + "				<input type='hidden' name='hdnFetchedItemText' value='" + getFetchedItemText + "'></td>";
		html = html + "				<input type='hidden' name='hdnFetchedLocationId' value=" + getFetchedLocationId + "></td>";
		html = html + "				<input type='hidden' name='hdnRecordCount' value=" + getRecordCount + "></td>";
		html = html + "				<input type='hidden' name='hdnconfirmedLPCount' value=" + getconfirmedLPCount + "></td>";
		html = html + "				<input type='hidden' name='hdnlpCount' value=" + getlpCount + "></td>";
		html = html + "				<input type='hidden' name='hdnlpNumbersArray' value=" + TempLPNoArray + "></td>";
		html = html + "				<input type='hidden' name='hdnRecordId' value=" + getRecordId + "></td>";
		html = html + "				<input type='hidden' name='hdnCartLPNo' value=" + getCartLPNo + ">";
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		html = html + "				<input type='hidden' name='hdnLineNo' value=" + getLineNo + ">";
		html = html + "				<input type='hidden' name='hdnWONo' value=" + getWoInternalId + ">";
		html = html + "				<input type='hidden' name='hdngetActQuantity' value=" + getActQuantity + ">";
		html = html + "				<input type='hidden' name='hdnexceptionqty' value=" + exceptionqty + ">";
		html = html + "				<input type='hidden' name='hdnexceptionqtyflag' value=" + exceptionqtyflag + ">";
		html = html + "				<input type='hidden' name='hdnenteredlp' value=" + EnteredLpNo + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + getWHLocation + ">";
		html = html + "				<input type='hidden' name='hdnBatchNo' value='" + getBatchNo + "'>";
		html = html + "				<input type='hidden' name='hdnWOName' value=" + getWONo + ">";	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> " + st2;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlocation' id='enterlocation' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st5 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdLocException.disabled=true;this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					" + st9 + " <input name='cmdLocException' type='submit' value='F8'/>";
		html = html + "					" + st6 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlocation').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('ERROR', 'INTO Response');

		var getFetchedLocation = request.getParameter('hdnFetchedLocation');
		nlapiLogExecution('ERROR', 'getFetchedLocation', getFetchedLocation);
		// This variable is to hold the LP entered.
		var getLocation = request.getParameter('enterlocation');
		nlapiLogExecution('ERROR', 'getLocation', getLocation.toUpperCase());

		var getLPNo = request.getParameter('hdnLPNo');
		nlapiLogExecution('ERROR', 'getLPNo', getLPNo);

		/*var getActQuantity = request.getParameter('custparam_quantity');
		nlapiLogExecution('ERROR', 'getActQuantity', getActQuantity);

		var exceptionqty=request.getParameter('custparam_exceptionquantity');
		nlapiLogExecution('ERROR', 'exceptionqty', exceptionqty);

		var exceptionqtyflag=request.getParameter('custparam_exceptionQuantityflag');
		nlapiLogExecution('ERROR', 'exceptionqtyflag', exceptionqtyflag);*/
//		var getActQuantity = request.getParameter('custparam_quantity');
		var getActQuantity = request.getParameter('hdngetActQuantity');
		nlapiLogExecution('ERROR', 'getActQuantity', getActQuantity);
		//var exceptionqty=request.getParameter('custparam_exceptionquantity');
		var exceptionqty=request.getParameter('hdnexceptionqty');
		nlapiLogExecution('ERROR', 'exceptionqty', exceptionqty);
		//var exceptionqtyflag=request.getParameter('custparam_exceptionQuantityflag');
		var exceptionqtyflag=request.getParameter('hdnexceptionqtyflag');
		nlapiLogExecution('ERROR', 'exceptionqtyflag', exceptionqtyflag);

		getRecordCount = request.getParameter('hdnRecordCount');
		nlapiLogExecution('ERROR', 'getRecordCount', getRecordCount);

		getRecordId = request.getParameter('hdnRecordId');
		nlapiLogExecution('ERROR', 'getRecordId old', getRecordId);

		var POarray = new Array();


		var getLanguage = request.getParameter('hdngetLanguage');
		var getWHLocation = request.getParameter('hdnWhLocation');
		var getFetchedItemId = request.getParameter('hdnFetchedItem');
		POarray["custparam_whlocation"] = getWHLocation;

		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR', 'getLanguage', POarray["custparam_language"]);
		nlapiLogExecution('ERROR', 'getWHLocation', getWHLocation);


		var st7;
		if( getLanguage == 'es_ES')
		{
			st7 = "UBICACI&#211;N NO V&#193;LIDA ";
		}
		else
		{
			st7 = "Please Enter Valid Location";
		}



		var getOptedField = request.getParameter('hdnOptedField');
		nlapiLogExecution('ERROR', 'getOptedField ', getOptedField);
		POarray["custparam_option"] = getOptedField;

		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]));
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '5'));
		//filters.push(new nlobjSearchFilter('custrecord_ebiz_lpno', null, 'is', getLPNo));

		if(getRecordId!=null && getRecordId!='')
			filters.push(new nlobjSearchFilter('internalid', null, 'is', getRecordId));

		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_lpno'));
		columns.push(new nlobjSearchColumn('custrecord_expe_qty'));
		columns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
		columns.push(new nlobjSearchColumn('custrecord_sku'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
		columns.push(new nlobjSearchColumn('custrecord_skudesc'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
		columns.push(new nlobjSearchColumn('custrecord_line_no'));
		columns.push(new nlobjSearchColumn('custrecord_sku_status'));
		columns.push(new nlobjSearchColumn('custrecord_packcode'));
		columns.push(new nlobjSearchColumn('custrecord_batch_no'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_trailer_no'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_ot_receipt_no'));
		columns.push(new nlobjSearchColumn('custrecord_wms_location'));
		columns.push(new nlobjSearchColumn('custrecord_ebizmethod_no'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));


		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

		POarray["custparam_error"] = st7;
		POarray["custparam_screenno"] = 'WOPutConfirm';
		nlapiLogExecution('ERROR', 'Screen #', POarray["custparam_screenno"]);
		//Added on 12Mar by suman.
		POarray["custparam_recordid"] = getRecordId;
		nlapiLogExecution('ERROR', 'getRecordId new', getRecordId);
		POarray["custparam_quantity"] = getActQuantity;
		POarray["custparam_exceptionquantity"] = exceptionqty;
		POarray["custparam_exceptionQuantityflag"] = exceptionqtyflag;
		POarray["custparam_cartno"] = request.getParameter('hdnCartLPNo');
		getCartLPNo = request.getParameter('hdnCartLPNo');
		nlapiLogExecution('ERROR', 'getCartLPNo', getCartLPNo);

		POarray["custparam_beginlocation"] = request.getParameter('custparam_beginlocation');
		POarray["custparam_itemtext"] = request.getParameter('hdnFetchedItemText');
		POarray["custparam_lpno"] = getLPNo;
		POarray["custparam_lpNumbersArray"] = request.getParameter('hdnlpNumbersArray');
		POarray["custparam_pono"] = request.getParameter('hdnWONo');
		POarray["custparam_confirmedLPCount"] = request.getParameter('hdnconfirmedLPCount');
		POarray["custparam_lpCount"] = request.getParameter('hdnlpCount');
		POarray["custparam_enteredLPNo"] = request.getParameter('hdnenteredlp');
		if(searchresults!=null)
		{
			POarray["custparam_recordcount"] = parseInt(searchresults.length);
		}

		POarray["custparam_batchno"] = request.getParameter('hdnBatchNo');
		POarray["custparam_woname"] = request.getParameter('hdnWOName');
		POarray["custparam_item"] = request.getParameter('hdnFetchedItem');

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');


		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept PO #.


		if (sessionobj!=context.getUser()) {
			try
			{

				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}
				if (optedEvent == 'F7') {
//					response.sendRedirect('SUITELET', 'customscript_rf_putaway_lp', 'customdeploy_rf_putaway_lp_di', false, POarray);
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_put_lp', 'customdeploy_ebiz_rf_wo_put_lp_di', false, POarray);
					return;
				}
				else {

					if (request.getParameter('cmdLocException') == 'F8') {

						nlapiLogExecution('ERROR', 'location exception', 'Location Exception');
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_put_loc_excep', 'customdeploy_ebiz_rf_wo_put_loc_excep_di', false, POarray);
						return;

					}

					else{
						// try {
						if (getLocation.toUpperCase() != null) 
						{
							if(getFetchedLocation.toUpperCase() != getLocation.toUpperCase())
							{
								nlapiLogExecution('ERROR', 'plz enter correct location', getLocation.toUpperCase());
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								return;
							}
							else
							{
								nlapiLogExecution('ERROR', 'entered location is correct', getLocation.toUpperCase());
								if (searchresults != null && searchresults.length > 0) {
									nlapiLogExecution('ERROR', 'SearchResults of given LP', 'LP is found');


									var getLPId = searchresults[0].getId();
									nlapiLogExecution('ERROR', 'get LP Internal Id', getLPId);

									POarray["custparam_lpno"] = searchresults[0].getValue('custrecord_lpno');
									POarray["custparam_quantity"] = searchresults[0].getValue('custrecord_expe_qty');
									POarray["custparam_location"] = searchresults[0].getValue('custrecord_actbeginloc');
									POarray["custparam_item"] = searchresults[0].getValue('custrecord_sku');
									POarray["custparam_itemid"] = searchresults[0].getValue('custrecord_ebiz_sku_no');
									POarray["custparam_itemDescription"] = searchresults[0].getValue('custrecord_skudesc');
									POarray["custparam_pointernalid"] = searchresults[0].getValue('custrecord_ebiz_order_no');
									POarray["custparam_polinenumber"] = searchresults[0].getValue('custrecord_line_no');
									POarray["custparam_poitemstatus"] = searchresults[0].getValue('custrecord_sku_status');
									POarray["custparam_popackcode"] = searchresults[0].getValue('custrecord_packcode');
									POarray["custparam_polotbatchno"] = searchresults[0].getValue('custrecord_batch_no');

									var vqty = searchresults[0].getValue('custrecord_expe_qty');
									var vitem = searchresults[0].getValue('custrecord_sku');
									var vloc = searchresults[0].getValue('custrecord_wms_location');
									var varLotNo = searchresults[0].getValue('custrecord_batch_no');
									var assemblyrecid = searchresults[0].getValue('custrecord_ebiz_order_no');
									var methodid=searchresults[0].getValue('custrecord_ebizmethod_no');


									//POarray["custparam_itemtext"] = request.getParameter('hdnFetchedItemText');
									var whloc=searchresults[0].getValue('custrecord_wms_location');
									var methodid=searchresults[0].getValue('custrecord_ebizmethod_no');
									var vEbizTrailerNo='';
									if(searchresults[0].getValue('custrecord_ebiz_trailer_no') != null && searchresults[0].getValue('custrecord_ebiz_trailer_no') != '')
									{
										vEbizTrailerNo=searchresults[0].getValue('custrecord_ebiz_trailer_no');
									}
									var vEbizReceiptNo='';
									if(searchresults[0].getValue('custrecord_ebiz_ot_receipt_no') != null && searchresults[0].getValue('custrecord_ebiz_ot_receipt_no') != '')
									{
										vEbizReceiptNo=searchresults[0].getValue('custrecord_ebiz_ot_receipt_no');
									}
									POarray["custparam_ebiztrailer"] = vEbizTrailerNo;
									POarray["custparam_ebizreceipt"] = vEbizReceiptNo;

									nlapiLogExecution('ERROR', 'geteBizControlNo', POarray["custparam_pointernalid"]);
									nlapiLogExecution('ERROR', 'geteBizSKUNo', POarray["custparam_itemid"]);
									nlapiLogExecution('ERROR', 'itemStatus', POarray["custparam_poitemstatus"]);
									nlapiLogExecution('ERROR', 'custparam_confirmedLPCount', POarray["custparam_confirmedLPCount"]);
									nlapiLogExecution('ERROR', 'custparam_lpCount', POarray["custparam_lpCount"]);
									nlapiLogExecution('ERROR', 'custparam_optedEvent', POarray["custparam_optedEvent"]);
									//nlapiLogExecution('ERROR', 'custparam_ebiztrailer', POarray["custparam_ebiztrailer"]);
									//nlapiLogExecution('ERROR', 'custparam_ebizreceipt', POarray["custparam_ebizreceipt"]);

									if (getOptedField == 4) {
										POarray["custparam_location"] = getLocation.toUpperCase();
									}
									nlapiLogExecution('ERROR', 'getLocation', getLocation.toUpperCase());
									//Case# 20149843 starts
									//var BinLocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null, new nlobjSearchFilter('name', null, 'is', getLocation.toUpperCase()));
									var filters = new Array();
									filters[0] = new nlobjSearchFilter('name', null, 'is', getLocation.toUpperCase());
									var columns = new Array();
									columns[0] = new nlobjSearchColumn('name');
									var BinLocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);
									//Case# 20149843 ends
									if (BinLocationSearch != null)
									{
										POarray["custparam_beginlocation"] = BinLocationSearch[0].getValue('name');
										nlapiLogExecution('ERROR', 'Location Name is', POarray["custparam_beginlocation"]);
										POarray["custparam_beginlocationinternalid"] = BinLocationSearch[0].getId();
										nlapiLogExecution('ERROR', 'Begin Location Internal Id', POarray["custparam_beginlocationinternalid"]);

										var getBeginLocationInternalId = POarray["custparam_beginlocationinternalid"];

										if (getOptedField != 4) {
											if (getLocation.toUpperCase() != POarray["custparam_beginlocation"]) {
												response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
												nlapiLogExecution('ERROR', 'Entered Bin Location', 'Does not match with the actual bin location');
											}
										}

										var LocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null, new nlobjSearchFilter('name', null, 'is', getLocation.toUpperCase()));

										if (LocationSearch!=null && LocationSearch.length != 0) {
											nlapiLogExecution('ERROR', 'Length of Location Search', LocationSearch.length);

											for (var s = 0; s < LocationSearch.length; s++) {
												var EndLocationId = LocationSearch[s].getId();
												nlapiLogExecution('ERROR', 'End Location Id', EndLocationId);
											}
										}
										var trantype = nlapiLookupField('transaction', POarray["custparam_pointernalid"], 'recordType');

										var qty;
										if(exceptionqtyflag=='true')
										{
											qty=exceptionqty;;
										}
										else
											qty=POarray["custparam_quantity"];

										var vputwrecid=-1;

										vputwrecid=UpdateOpenTask(POarray["custparam_pointernalid"], POarray["custparam_polinenumber"], POarray["custparam_itemid"], 
												POarray["custparam_itemDescription"], POarray["custparam_poitemstatus"], POarray["custparam_popackcode"], 
												qty, getLocation.toUpperCase(), getBeginLocationInternalId, POarray["custparam_lpno"], EndLocationId,exceptionqty, 
												getRecordId,trantype,POarray["custparam_polotbatchno"],vloc,methodid);
										/*vputwrecid = rf_confirmputaway(POarray["custparam_pointernalid"], POarray["custparam_polinenumber"], POarray["custparam_itemid"], 
									POarray["custparam_itemDescription"], POarray["custparam_poitemstatus"], POarray["custparam_popackcode"], 
									qty, getLocation.toUpperCase(), getBeginLocationInternalId, POarray["custparam_lpno"], EndLocationId,exceptionqty, 
									getRecordId,trantype,POarray["custparam_polotbatchno"],whloc,methodid);
							//rf_confirmputaway(PORec.getFieldValue('custrecord_ebiz_cntrl_no'), POarray["custparam_polinenumber"], PORec.getFieldValue('custrecord_ebiz_sku_no'), PORec.getFieldValue('custrecord_skudesc'), PORec.getFieldValue('custrecord_sku_status'), PORec.getFieldValue('custrecord_packcode'), PORec.getFieldValue('custrecord_expe_qty'), getLocation, getBeginLocationInternalId, POarray["custparam_lpno"], EndLocationId, getActQuantity, getRecordId);*/

										nlapiLogExecution('ERROR', 'vputwrecid', vputwrecid);

										if(vputwrecid!=-1)
										{
											/*if(vEbizTrailerNo != null && vEbizTrailerNo != '')
									{
										if(vEbizReceiptNo != null && vEbizReceiptNo != '')
										{
											var poReceiptRec = nlapiLoadRecord('customrecord_ebiznet_trn_poreceipt', vEbizReceiptNo);
											if(poReceiptRec != null && poReceiptRec != '')
											{
												var previousrcvqty=poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_rcvqty');
												var newrcvQty= parseFloat(previousrcvqty)+parseFloat(qty);
												//var id=nlapiSubmitField('customrecord_ebiznet_trn_poreceipt', vEbizReceiptNo, 'custrecord_ebiz_poreceipt_rcvqty',newrcvQty.toString());
												//nlapiLogExecution('ERROR','id',id);
											}	
										}	
									}	*/
											/*TrnLineUpdation(trantype, 'PUTW', POarray["custparam_pono"], 
										POarray["custparam_pointernalid"], POarray["custparam_polinenumber"], 
										POarray["custparam_itemid"], null, qty,"",
										POarray["custparam_poitemstatus"]);

								nlapiLogExecution('ERROR', 'getCartLPNo after confirming', getCartLPNo);*/
											var Remqty=0;
											if(exceptionqtyflag=='true')
											{
												itemDimensions = getSKUCubeAndWeight(POarray["custparam_itemid"],"");
												var itemCube = itemDimensions[0];
												Remqty=parseFloat(POarray["custparam_quantity"])-parseFloat(qty);
												var TotalItemCube = parseFloat(itemCube) * parseFloat(Remqty);
												nlapiLogExecution('ERROR', 'Total Item Cube ', TotalItemCube );						

												var binLocationRemainingCube = GeteLocCube(getBeginLocationInternalId);
												nlapiLogExecution('ERROR', 'binLocationRemainingCube ', binLocationRemainingCube );
												var remainingCube = parseFloat(binLocationRemainingCube) + parseFloat(TotalItemCube);
												UpdateLocCube(getBeginLocationInternalId, parseFloat(remainingCube));

												nlapiLogExecution('ERROR', 'remainingCube ', remainingCube );
											}
										}
										else
										{
											nlapiLogExecution('ERROR', 'Putaway Failed ');
											POarray["custparam_error"] = 'Putaway Failed';
											response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
										}
										if (getRecordCount > 1) {

											//transformAssemblyItem(assemblyrecid,vqty,vloc,varLotNo,vitem)
											POarray["custparam_lpno"] = searchresults[0].getValue('custrecord_lpno');
											POarray["custparam_recordcount"] = parseInt(getRecordCount)-1;
											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_put_lp', 'customdeploy_ebiz_rf_wo_put_lp_di', false, POarray);


										}						
										else {
											//case 20123541 start
											if(varLotNo!="" && varLotNo!=null)
											{

												var expdate='';
												var filterspor = new Array();
												filterspor[0] = new nlobjSearchFilter('name', null, 'is', varLotNo);
												/*if(vloc!=null && vloc!="")
											filterspor[1] = new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', vloc);*/
												if(vitem!=null && vitem!="")
													filterspor[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', vitem);

												var column=new Array();
												column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');

												var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
												if(receiptsearchresults!=null && receiptsearchresults!='')
												{

													expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');

												}
												nlapiLogExecution('ERROR', 'expdate', expdate);
											}//end
											//Case# 201410445 starts
											//transformAssemblyItem(assemblyrecid,vqty,vloc,varLotNo,vitem,expdate);
											var assmid=transformAssemblyItem(assemblyrecid,vqty,vloc,varLotNo,vitem,expdate);
											nlapiLogExecution('ERROR', 'assmid', assmid);
											if(assmid != null && assmid != "")
											{
												updateOpenTaskwithns(assemblyrecid,assmid);
											}
											//Case# 201410445 ends
											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_menu', 'customdeploy_ebiz_rf_wo_menu_di', false, POarray);
										}

										/*
										 * 	This is to delete the check-in transactions from the inventory record while putaway confirmation. 
										 */			

										DeleteInvtStageRecdAssemblyItem(POarray["custparam_pointernalid"],POarray["custparam_lpno"],getWHLocation,getFetchedItemId);
									}
									else
									{
										response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
										nlapiLogExecution('ERROR', 'Entered Bin Location', 'Scanned bin location is wrong');
									}
								}
								else {
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
									nlapiLogExecution('ERROR', 'Here the Search Results2 ', 'Length is null');
								}
							}
						}
						else {
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							nlapiLogExecution('ERROR', 'Location is not entered / scanned', getLocation.toUpperCase());
						}
					}
					nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
				}
			}
			catch (e)  {
				nlapiLogExecution('DEBUG', 'ctach before finally',e);
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');

			}
		}
		else
		{
			POarray["custparam_screenno"] = 'WOPutScanLp';
			POarray["custparam_error"] = 'LP ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
		}
	}
}

function isMergeLP(poloc,methodid)
{
	var varMergeLP = 'F';
	var filtersputmethod = new Array();
	filtersputmethod.push(new nlobjSearchFilter('custrecord_ebizsiteputaway', null, 'anyof', ['@NONE@',poloc]));
	if(methodid!=null && methodid!='')
		filtersputmethod.push(new nlobjSearchFilter('custrecord_methodid', null, 'is', methodid));
	filtersputmethod.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var colsputmethod = new Array();
	colsputmethod[0] = new nlobjSearchColumn('custrecord_mergelp');            

	var putwmethodsearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_method', null, filtersputmethod, colsputmethod);

	if(putwmethodsearchresults != null &&putwmethodsearchresults != ''&& putwmethodsearchresults.length > 0)
	{

		varMergeLP = putwmethodsearchresults[0].getValue('custrecord_mergelp');
	}

	return varMergeLP;
}
function getMaxUOMQty(putItemId,putItemPC)
{
	var varPaltQty=0;
	var searchresults = new Array();

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', putItemId));
	if(putItemPC!=null && putItemPC!='')
		filters.push(new nlobjSearchFilter('custrecord_ebizpackcodeskudim', null, 'is', putItemPC));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizqty');
	columns[1] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim'); 	
	columns[1].setSort(true);

	searchresults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);
	if(searchresults != null && searchresults != '' && searchresults.length > 0)
	{
		varPaltQty = searchresults[0].getValue('custrecord_ebizqty');
	}

	return varPaltQty;
}
function DeleteInvtStageRecdAssemblyItem(WoIntid,lp,WHLocation,getFetchedItemId)
{
	nlapiLogExecution('ERROR','Inside DeleteInvtRecCreatedforCHKNTask ','Funciton');

	nlapiLogExecution('ERROR','WoIntid ',WoIntid);
	nlapiLogExecution('ERROR','WHLocation',WHLocation);
	nlapiLogExecution('ERROR','lp',lp);
	var vstageLoc ='';
	var vCarrier ='';  
	var vSite;
	var vCompany;
	var stgDirection="OUB";
	// case# 201411366
	var tranordertype = nlapiLookupField('workorder', WoIntid, 'custbody_nswmssoordertype');
	if(vstageLoc==null || vstageLoc=='')
		vstageLoc = GetPickStageLocation(getFetchedItemId, vCarrier, WHLocation, vCompany,stgDirection,null,null,null,tranordertype);
	nlapiLogExecution('ERROR', 'vstagLoc', vstageLoc);
	if (vstageLoc != null && vstageLoc != "" && vstageLoc != "-1") 
	{

		var Ifilters = new Array();
		Ifilters.push(new nlobjSearchFilter('custrecord_ebiz_transaction_no', null, 'anyof', WoIntid));
		Ifilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', lp));
		//Ifilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['18']));
		Ifilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['36']));
		/*if(vstageLoc != null && vstageLoc != '')
		{
			Ifilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', vstageLoc));	
		}*/

		Ifilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', WHLocation));


		var invtId="";
		var invtType="";
		var serchInvtRec= nlapiSearchRecord('customrecord_ebiznet_createinv', null, Ifilters, null);
		if (serchInvtRec) 
		{
			for (var s = 0; s < serchInvtRec.length; s++) {
				nlapiLogExecution('ERROR','Inside loop','loop');
				var searchresult = serchInvtRec[ s ];
				nlapiLogExecution('ERROR','need to print this','print');
				invtId = serchInvtRec[s].getId();
				invtType= serchInvtRec[s].getRecordType();
				nlapiLogExecution('ERROR','CHKN INVT Id',invtId);
				nlapiLogExecution('ERROR','CHKN invtType ',invtType);
				nlapiDeleteRecord(serchInvtRec[s].getRecordType(),serchInvtRec[s].getId());
				nlapiLogExecution('ERROR','Invt Deleted record Id',invtId);

			}
		}
	}
}

function geteBizItemDimensions(itemid)
{
	var searchRec = new Array();
	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemid));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim') ;
	column[1] = new nlobjSearchColumn('custrecord_ebizqty') ;
	column[2] = new nlobjSearchColumn('custrecord_ebizbaseuom') ;
	column[3] = new nlobjSearchColumn('custrecord_ebiznsuom') ;
	column[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim') ;
	searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);

	return searchRec;

}



function UpdateOpenTask(pointid, linenum, itemid, itemdesc, itemstatus, itempackcode, quantity, binlocationid, 
		getBeginLocationInternalId, invtlp, EndLocationId, ActQuantity, RecordId,trantype,batchno,whloc,methodid)
{

	// Create a Record in Opentask
	var now = new Date();
	//a Date object to be used for a random value
	var now = new Date();
	//now= now.getHours();
	//Getting time in hh:mm tt format.
	var a_p = "";
	var d = new Date();
	var curr_hour = now.getHours();
	if (curr_hour < 12) {
		a_p = "am";
	}
	else {
		a_p = "pm";
	}
	if (curr_hour == 0) {
		curr_hour = 12;
	}
	if (curr_hour > 12) {
		curr_hour = curr_hour - 12;
	}

	var curr_min = now.getMinutes();

	curr_min = curr_min + "";

	if (curr_min.length == 1) {
		curr_min = "0" + curr_min;
	}

	var vEbizTrailerNo='';			
	var vEbizReceiptNo='';
	var getlotnoid="";
	var batchno = '';
	var invtrecid='';// case# 201415362
	nlapiLogExecution('ERROR', 'pointid', pointid);

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', pointid);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'is', '2');
	filters[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2, 6]);
	filters[3] = new nlobjSearchFilter('internalid', null, 'is', RecordId);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[1] = new nlobjSearchColumn('custrecord_actendloc');
	columns[2] = new nlobjSearchColumn('custrecordact_begin_date');
	columns[3] = new nlobjSearchColumn('custrecord_act_end_date');
	columns[4] = new nlobjSearchColumn('custrecord_wms_status_flag');
	columns[5] = new nlobjSearchColumn('custrecord_actualbegintime');
	columns[6] = new nlobjSearchColumn('custrecord_actualendtime');
	columns[7] = new nlobjSearchColumn('custrecord_upd_date');
	columns[8] = new nlobjSearchColumn('custrecord_recordupdatetime');
	columns[9] = new nlobjSearchColumn('custrecord_batch_no');
	columns[10] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[11] = new nlobjSearchColumn('custrecord_sku_status');
	columns[12] = new nlobjSearchColumn('custrecord_packcode');
	columns[13] = new nlobjSearchColumn('custrecord_ebiz_lpno');
	columns[14] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[15] = new nlobjSearchColumn('custrecord_ebiz_trailer_no');
	columns[16] = new nlobjSearchColumn('custrecord_ebiz_ot_receipt_no');



	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);







	var putItemId,putItemStatus,putItemPC,putBinLoc,putLP,opentaskintid,putQty,putoldBinLoc,expdate,vfifoDate;//20149080
	//Update Open task 
	var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordId);

	putItemId=transaction.getFieldValue('custrecord_ebiz_sku_no');
	putItemStatus=transaction.getFieldValue('custrecord_sku_status');
	putItemPC=transaction.getFieldValue('custrecord_packcode');
	putBinLoc=transaction.getFieldValue('custrecord_actbeginloc');
	putLP=transaction.getFieldValue('custrecord_ebiz_lpno');
//	putQty = searchresult.getValue('custrecord_expe_qty');
	putQty = quantity;
	batchno = transaction.getFieldValue('custrecord_batch_no');
	//putQty = ActQuantity;//here inventory should be with eaches
	putoldBinLoc= transaction.getFieldValue('custrecord_actbeginloc');
	if(transaction.getFieldValue('custrecord_ebiz_trailer_no') != null && transaction.getFieldValue('custrecord_ebiz_trailer_no') != '')
	{
		vEbizTrailerNo=transaction.getFieldValue('custrecord_ebiz_trailer_no');
	}
	if(transaction.getFieldValue('custrecord_ebiz_ot_receipt_no') != null && transaction.getFieldValue('custrecord_ebiz_ot_receipt_no') != '')
	{
		vEbizReceiptNo=transaction.getFieldValue('custrecord_ebiz_ot_receipt_no');
	}




	transaction.setFieldValue('custrecord_wms_status_flag', '3'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')

	transaction.setFieldValue('custrecord_act_qty', quantity);

	transaction.setFieldValue('custrecord_actendloc', EndLocationId);
	//transaction.setFieldValue('custrecord_container', vcontainersize);
	//transaction.setFieldValue('custrecord_total_weight', itemWeight);
	//transaction.setFieldValue('custrecord_totalcube', itemCube);
	//transaction.setFieldValue('custrecord_batch_no', vBatchno);

	//transaction.setFieldValue('custrecord_container_lp_no', vContlpNo);
	//transaction.setFieldValue('custrecord_act_end_date', (parseInt(d.getMonth()) + 1) + '/' + (parseInt(d.getDate())) + '/' + d.getFullYear());
	transaction.setFieldValue('custrecord_act_end_date', DateStamp());
	transaction.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));

	var currentContext = nlapiGetContext();
	var currentUserID = currentContext.getUser();
	transaction.setFieldValue('custrecord_upd_ebiz_user_no',currentUserID);
	var vemployee = request.getParameter('custpage_employee');
	nlapiLogExecution('ERROR', 'vemployee :', vemployee);
	if (vemployee != null && vemployee != "") 
	{
		transaction.setFieldValue('custrecord_taskassignedto',vemployee);
	} 
	else 
	{
		transaction.setFieldValue('custrecord_taskassignedto',currentUserID);
	}
	//Case# 20149080 starts
	var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group','custitem_ebizserialin'];
	var columns = nlapiLookupField('item', putItemId, fields);
	var ItemType = columns.recordType;					
	var batchflg = columns.custitem_ebizbatchlot;
	if(ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflg == "T")
	{
		nlapiLogExecution('ERROR', 'batchno', batchno);
		if(batchno!="" && batchno!=null)
		{
			var filterspor = new Array();
			if(batchno!=null && batchno!="")
				filterspor.push(new nlobjSearchFilter('name', null, 'is', batchno));
			/*if(whLocation!=null && whLocation!="")
				filterspor[1] = new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', whLocation);*/
			if(putItemId!=null && putItemId!="")
				filterspor.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', putItemId));
			var column=new Array();
			column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
			column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');
			var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
			if(receiptsearchresults!=null)
			{
				getlotnoid= receiptsearchresults[0].getId();
				nlapiLogExecution('ERROR', 'getlotnoid', getlotnoid);
				expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');
				vfifoDate = receiptsearchresults[0].getValue('custrecord_ebizfifodate');
			}
			nlapiLogExecution('ERROR', 'expdate', expdate);
			nlapiLogExecution('ERROR', 'vfifoDate', vfifoDate);
		}
	}
	if (expdate != null && expdate != "") 
		transaction.setFieldValue('custrecord_expirydate',expdate);
	if (vfifoDate != null && vfifoDate != "") 
		transaction.setFieldValue('custrecord_fifodate',vfifoDate);
	nlapiSubmitRecord(transaction, false, true);
	//Case# 20149080 ends

	//Create record in CreateInvt
	var varMergeLP = 'F';

	if(methodid!=null && methodid!='')
		varMergeLP = isMergeLP(whloc,methodid);


	var priorityPutawayLP = priorityPutawayfixedLP(itemid,whloc);

	if(varMergeLP=="T" || (priorityPutawayLP!==null && priorityPutawayLP!=='')){
		//invtlp=priorityPutawayLP;
		// case# 201417035
		if(priorityPutawayLP != null && priorityPutawayLP != '')
			invtlp=priorityPutawayLP;

		var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group','custitem_ebizserialin'];
		var columns = nlapiLookupField('item', putItemId, fields);
		var ItemType = columns.recordType;					
		var batchflg = columns.custitem_ebizbatchlot;
		var itemfamId= columns.custitem_item_family;
		var itemgrpId= columns.custitem_item_group;
		var serialInflg = columns.custitem_ebizserialin;

		if(ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflg == "T")
		{
			nlapiLogExecution('ERROR', 'batchno', batchno);
			if(batchno!="" && batchno!=null)
			{

				var filterspor = new Array();
				filterspor.push(new nlobjSearchFilter('name', null, 'is', batchno));
				/*if(whLocation!=null && whLocation!="")
					filterspor[1] = new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', whLocation);*/
				if(itemid!=null && itemid!="")
					filterspor.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', putItemId));

				var column=new Array();
				column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
				column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');

				var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
				if(receiptsearchresults!=null)
				{
					getlotnoid= receiptsearchresults[0].getId();
					nlapiLogExecution('ERROR', 'getlotnoid', getlotnoid);
					//expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');
					//vfifoDate = receiptsearchresults[0].getValue('custrecord_ebizfifodate');
				}
				//nlapiLogExecution('ERROR', 'expdate', expdate);
				//nlapiLogExecution('ERROR', 'vfifoDate', vfifoDate);
			}
		}

		var fifodate;
		var filtersfifo = new Array();
		filtersfifo.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', putLP));
		var columnsfifo = new Array();
		columnsfifo[0] = new nlobjSearchColumn('custrecord_ebiz_inv_fifo');    
		var invttransactionforfifodate = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersfifo, columnsfifo);
		if(invttransactionforfifodate!=null && invttransactionforfifodate!='')
		{

			fifodate=invttransactionforfifodate[0].getValue('custrecord_ebiz_inv_fifo');
			nlapiLogExecution('ERROR', 'fifodate inside', fifodate);
		}
		nlapiLogExecution('ERROR', 'putItemId', putItemId);
		nlapiLogExecution('ERROR', 'putItemStatus', putItemStatus);
		nlapiLogExecution('ERROR', 'putItemPC', putItemPC);
		nlapiLogExecution('ERROR', 'putBinLoc', putBinLoc);
		nlapiLogExecution('ERROR', 'putLP', putLP);
		nlapiLogExecution('ERROR', 'fifodate', fifodate);
		nlapiLogExecution('ERROR', 'getlotnoid from new search', getlotnoid);
		var varPaltQty = getMaxUOMQty(putItemId,putItemPC);

		nlapiLogExecution('ERROR', 'varPaltQty', varPaltQty);


		nlapiLogExecution('ERROR', 'Checking for merge the item');
		var filtersinvt = new Array();

		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', putItemId));
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', putItemStatus));
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', putBinLoc));
		filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', '19'));
		if(fifodate!=null && fifodate!='')
			filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_fifo', null, 'on', fifodate));

		if(putItemPC!=null && putItemPC!='')
			filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_packcode', null, 'anyof', putItemPC));							

		if(getlotnoid!=null && getlotnoid!='')
		{
			nlapiLogExecution('ERROR', 'getlotnoid inside', getlotnoid);
			filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'is', getlotnoid));
		}

		if(varPaltQty!=null && varPaltQty!='')
			filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'lessthan', varPaltQty));							

		var columnsinvt = new Array();
		columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');    
		columnsinvt[0].setSort();

		var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);
		nlapiLogExecution('ERROR', 'invtsearchresults', invtsearchresults);
		if(invtsearchresults != null && invtsearchresults != ''  && invtsearchresults.length > 0)
		{
			nlapiLogExecution('ERROR', 'invtsearchresults.length', invtsearchresults.length);
			var newputqty=putQty;
			var BoolInvMerged=false;
			for (var i = 0; i < invtsearchresults.length; i++) 
			{
				var qoh=invtsearchresults[i].getValue('custrecord_ebiz_qoh');

				nlapiLogExecution('ERROR', 'exiting qoh', qoh);
				nlapiLogExecution('ERROR', 'putQty', putQty);
				nlapiLogExecution('ERROR', 'varPaltQty', varPaltQty);

				if(newputqty>0 && (parseFloat(putQty)+parseFloat(qoh))<=parseFloat(varPaltQty))
				{
					nlapiLogExecution('ERROR', 'Inventory Record ID', invtsearchresults[i].getId());
					BoolInvMerged=true;
					nlapiLogExecution('ERROR', 'BoolInvMerged', BoolInvMerged);
					var scount=1;
					LABL1: for(var z=0;z<scount;z++)
					{
						try
						{
							var invttransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invtsearchresults[i].getId());											

							var varExistQOHQty = invttransaction.getFieldValue('custrecord_ebiz_qoh');
							var varExistInvQty = invttransaction.getFieldValue('custrecord_ebiz_inv_qty');
							var varExistLP=invttransaction.getFieldValue('custrecord_ebiz_inv_lp');

							nlapiLogExecution('ERROR', 'varExistQOHQty', varExistQOHQty);
							nlapiLogExecution('ERROR', 'varExistInvQty', varExistInvQty);

							var varupdatedQOHqty = parseFloat(varExistQOHQty) + parseFloat(putQty);
							var varupdatedInvqty = parseFloat(varExistInvQty) + parseFloat(putQty);

							invttransaction.setFieldValue('custrecord_ebiz_qoh', parseFloat(varupdatedQOHqty).toFixed(4));
							invttransaction.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(varupdatedInvqty).toFixed(4));
							invttransaction.setFieldValue('custrecord_ebiz_callinv', 'N');    

							nlapiSubmitRecord(invttransaction,false, true);

							nlapiLogExecution('ERROR', 'nlapiSubmitRecord', 'Record Submitted');
							nlapiLogExecution('ERROR', 'Inventory is merged to the LP '+varExistLP);
							newputqty=0;

						}
						catch(ex)
						{
							nlapiLogExecution('ERROR', 'Exception in deleteAllocations',ex);

							var exCode='CUSTOM_RECORD_COLLISION'; 
							var wmsE='Inventory record being updated by another user. Please try again...';
							if (ex instanceof nlobjError) 
							{	
								wmsE=ex.getCode() + '\n' + ex.getDetails();
								exCode=ex.getCode();
							}
							else
							{
								wmsE=ex.toString();
								exCode=ex.toString();
							}  

							nlapiLogExecution('ERROR', 'Exception in RF Inv move : ', ex);

							if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
							{ 
								scount=scount+1;
								continue LABL1;
							}
							else break LABL1;
						}
					}
				}
			}
			if(BoolInvMerged==false)
			{
				var accountNumber = "";

				//Creating Inventory Record.
				var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');

				nlapiLogExecution('ERROR', 'Creating INVT  Record if BoolInvMerged is false in merge lp true', 'INVT');

				invtRec.setFieldValue('name', itemid);
				invtRec.setFieldValue('custrecord_ebiz_inv_binloc', EndLocationId);
				invtRec.setFieldValue('custrecord_ebiz_inv_lp', invtlp);
				invtRec.setFieldValue('custrecord_ebiz_inv_sku', itemid);
				invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
				if(itempackcode!=null&&itempackcode!="")
					invtRec.setFieldValue('custrecord_ebiz_inv_packcode', itempackcode);
				//invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(ActQuantity).toFixed(4));
				invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(quantity).toFixed(4));
				invtRec.setFieldValue('custrecord_ebiz_inv_lot', batchno);
				//invtRec.setFieldValue('custrecord_ebiz_inv_recorddate',(parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
				//invtRec.setFieldValue('custrecord_ebiz_inv_rectime',((curr_hour) + ":" + (curr_min) + " " + a_p));
				invtRec.setFieldValue('custrecord_inv_ebizsku_no', itemid);
				//invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(ActQuantity).toFixed(4));
				invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(quantity).toFixed(4));
				invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemdesc);
				invtRec.setFieldValue('custrecord_ebiz_inv_loc', whloc);
				invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');//WH Location.
				invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);
				invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
				invtRec.setFieldValue('custrecord_wms_inv_status_flag','19');//19=FLAG.INVENTORY.STORAGE
				//invtRec.setFieldValue('custrecord_invttasktype', '2');
				invtRec.setFieldValue('custrecord_invttasktype', '5');//case# 20147959 (5-KTS)
				invtRec.setFieldValue('custrecord_ebiz_transaction_no', pointid);
				if(getuomlevel!=null && getuomlevel!='')
					invtRec.setFieldText('custrecord_ebiz_uomlvl', getuomlevel);


				var expdate='';
				nlapiLogExecution('ERROR', 'ItemType', ItemType);
				nlapiLogExecution('ERROR', 'batchflg', batchflg);
				nlapiLogExecution('ERROR', 'batchno', batchno);

				if(getlotnoid!=null && getlotnoid!='')
				{
					var fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
					invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
				}
				if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem"|| batchflg == "T")
//					if (ItemType == "lotnumberedinventoryitem" || batchflg == "T")
				{ 										
					try
					{
						//Checking FIFO Policy.					
						if (getlotnoid != "") {
							if(vfifoDate == null || vfifoDate =='')
							{
								fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
								nlapiLogExecution('ERROR','FIFO Value Check.',fifovalue);
							}
							else
							{
								fifovalue=vfifoDate;
							}
							invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
							invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
							invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
						}
					}
					catch(exp)
					{
						nlapiLogExecution('ERROR','Exception in FIFO Value Check.',exp);
					}
				}
				else
					invtRec.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());


				nlapiLogExecution('ERROR', 'Before Submitting invtrecid', 'INVTRECORDS');

				invtrecid = nlapiSubmitRecord(invtRec, false, true);

				nlapiLogExecution('ERROR', 'After Submitting invtrecid', invtrecid);
				if (invtrecid != null) {
					nlapiLogExecution('ERROR', 'Cust INVT Rec Creation Succes with ID ', invtrecid);
				}
				else {
					nlapiLogExecution('ERROR', 'Cust INVT Rec Creation Fail ', 'Fail');
				}

				//Added for updation of Cube information by ramana on 10th june 2011

				nlapiLogExecution('ERROR', 'OLD BIN LOCATION DETAILS ', putoldBinLoc);
				nlapiLogExecution('ERROR', 'NEW BIN LOCATION DETAILS ', putBinLoc);
				nlapiLogExecution('ERROR', 'ITEMID', putItemId);
				nlapiLogExecution('ERROR', 'LP', putLP);

				if(putBinLoc!=putoldBinLoc)
				{   
					/*
							//For the OLD Location Updation Purpose.    											 											
					 */

					var arrDims = getSKUCubeAndWeight(putItemId, 1);
					var itemCube = 0;
					if (arrDims["BaseUOMItemCube"] != "" && (!isNaN(arrDims["BaseUOMItemCube"]))) 
					{
						var uomqty = ((parseFloat(putQty))/(parseFloat(arrDims["BaseUOMQty"])));			
						itemCube = (parseFloat(uomqty) * parseFloat(arrDims["BaseUOMItemCube"]));
						nlapiLogExecution('ERROR', 'checkInPOSTRequest:itemCube', itemCube);
					} 
					if(putoldBinLoc != "" && putoldBinLoc != null)
					{
						var vOldRemainingCube = GeteLocCube(putoldBinLoc);
					}
					nlapiLogExecution('ERROR', 'vOldRemainingCube', vOldRemainingCube);

					var vTotalCubeValue = parseFloat(itemCube)+ parseFloat(vOldRemainingCube);
					nlapiLogExecution('ERROR', 'vOldLocationRemainingCube', vTotalCubeValue);
					if(putoldBinLoc != "" && putoldBinLoc != null)
					{
						var retValue =  UpdateLocCube(putoldBinLoc,vTotalCubeValue);
					}
					//upto to here old Location Updation purpose

					//For the New Location updation

					var vNewLocationRemainingCube = GeteLocCube(putBinLoc);   

					nlapiLogExecution('ERROR', 'vNewLocationRemainingCube', vNewLocationRemainingCube);

					//var vTotalNewLocation =  parseFloat(vartotalCube)- parseFloat(vNewLocationRemainingCube);
					var vTotalNewLocationCube =  parseFloat(vNewLocationRemainingCube)- parseFloat(itemCube);

					nlapiLogExecution('ERROR', 'vTotalNewLocation', vTotalNewLocationCube);

					if(vTotalNewLocationCube<0)
					{
						nlapiLogExecution('ERROR', 'inside PO Overage Validation', 'inside loop');					  
						var form = nlapiCreateForm('Confirm Putaway');
						nlapiLogExecution('ERROR', 'Form Called', 'form');					  
						var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);		  
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'Qty exceeds location capacity', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");		    		   
						nlapiLogExecution('ERROR', 'message', msg);					  
						response.writePage(form);                  						
						return;			
					}
					else
					{
						var retValue1 = UpdateLocCube(putBinLoc,vTotalNewLocationCube);
					}
					//upto to here new location

				}						 
				var currentUserID = getCurrentUser();
				nlapiLogExecution('ERROR', 'currentUserID', currentUserID);
//				for (var i = 0; i < searchresults.length; i++) {
//				vbatchno=searchresults[i].getValue('custrecord_batch_no');
//				var searchresult = searchresults[i];
//				var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', searchresult.getId());

//				nlapiLogExecution('ERROR', 'STATUS before', searchresult.getValue('custrecord_wms_status_flag'));

//				if (searchresult.getValue('custrecord_wms_status_flag') == 6) {
//				transaction.setFieldValue('custrecord_actbeginloc', getBeginLocationInternalId);// Give location Id.
//				transaction.setFieldValue('custrecordact_begin_date', DateStamp());
//				//transaction.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
//				}

//				transaction.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
//				transaction.setFieldValue('custrecord_recordupdatetime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
//				transaction.setFieldValue('custrecord_upd_date', DateStamp());
//				transaction.setFieldValue('custrecord_actendloc', EndLocationId);// Give location Id.
//				transaction.setFieldValue('custrecord_act_end_date', DateStamp());
//				transaction.setFieldValue('custrecord_wms_status_flag', 3);
//				transaction.setFieldValue('custrecord_act_qty', parseFloat(ActQuantity).toFixed(4));
//				transaction.setFieldValue('custrecord_taskassignedto', currentUserID);
//				var	recid=nlapiSubmitRecord(transaction, false, true);
//				//	vputwrecid[1]='';
//				//MoveTaskRecord(vputwrecid[0]);
//				} 

				//MoveTaskRecord(opentaskintid);
				/*	if(vEbizTrailerNo != null && vEbizTrailerNo != '')
				{
					if(vEbizReceiptNo != null && vEbizReceiptNo != '')
					{
						nlapiLogExecution('ERROR', 'vEbizReceiptNo', vEbizReceiptNo);
						nlapiLogExecution('ERROR', 'vEbizTrailerNo', vEbizTrailerNo);
						var poReceiptRec = nlapiLoadRecord('customrecord_ebiznet_trn_poreceipt', vEbizReceiptNo);

						if(poReceiptRec != null && poReceiptRec != '')
						{
							var previousrcvqty=0;
							if(poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_rcvqty') != null && poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_rcvqty') != '')
							{
								previousrcvqty=poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_rcvqty');
							}	
							var newrcvQty= parseFloat(previousrcvqty)+parseFloat(putQty);

							nlapiLogExecution('ERROR', 'previousrcvqty', previousrcvqty);
							nlapiLogExecution('ERROR', 'qty', qty);
							nlapiLogExecution('ERROR', 'newrcvQty', newrcvQty);

							var id=nlapiSubmitField('customrecord_ebiznet_trn_poreceipt', vEbizReceiptNo, 'custrecord_ebiz_poreceipt_rcvqty',newrcvQty.toString());
							nlapiLogExecution('ERROR','id',id);
						}	
					}	
				}*/

			}
		}
		else
		{
			var accountNumber = "";

			//Creating Inventory Record.
			var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');

			nlapiLogExecution('ERROR', 'Creating INVT  Record in mergelp if invserach result is null', 'INVT');

			invtRec.setFieldValue('name', itemid);
			invtRec.setFieldValue('custrecord_ebiz_inv_binloc', EndLocationId);
			invtRec.setFieldValue('custrecord_ebiz_inv_lp', invtlp);
			invtRec.setFieldValue('custrecord_ebiz_inv_sku', itemid);
			invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
			if(itempackcode!=null&&itempackcode!="")
				invtRec.setFieldValue('custrecord_ebiz_inv_packcode', itempackcode);
			//invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(ActQuantity).toFixed(4));
			invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(quantity).toFixed(4));
			invtRec.setFieldValue('custrecord_ebiz_inv_lot', batchno);
			//invtRec.setFieldValue('custrecord_ebiz_inv_recorddate',(parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
			//invtRec.setFieldValue('custrecord_ebiz_inv_rectime',((curr_hour) + ":" + (curr_min) + " " + a_p));
			invtRec.setFieldValue('custrecord_inv_ebizsku_no', itemid);
			nlapiLogExecution('ERROR','parseFloat(ActQuantity).toFixed(4)',parseFloat(ActQuantity).toFixed(4));
			//invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(ActQuantity).toFixed(4));
			invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(quantity).toFixed(4));
			invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemdesc);
			invtRec.setFieldValue('custrecord_ebiz_inv_loc', whloc);
			invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');//WH Location.
			invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);
			invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
			invtRec.setFieldValue('custrecord_wms_inv_status_flag','19');//19=FLAG.INVENTORY.STORAGE
			//invtRec.setFieldValue('custrecord_invttasktype', '2');
			invtRec.setFieldValue('custrecord_invttasktype', '5');//case# 20147959 (5-KTS)
			invtRec.setFieldValue('custrecord_ebiz_transaction_no', pointid);
			if(getuomlevel!=null && getuomlevel!='')
				invtRec.setFieldText('custrecord_ebiz_uomlvl', getuomlevel);

			var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group','custitem_ebizserialin'];
			var columns = nlapiLookupField('item', itemid, fields);
			var ItemType = columns.recordType;					
			var batchflg = columns.custitem_ebizbatchlot;
			var itemfamId= columns.custitem_item_family;
			var itemgrpId= columns.custitem_item_group;
			var serialInflg = columns.custitem_ebizserialin;
			var getlotnoid="";
			var expdate='';
			var vfifoDate='';
			nlapiLogExecution('ERROR', 'ItemType', ItemType);
			nlapiLogExecution('ERROR', 'batchflg', batchflg);
			nlapiLogExecution('ERROR', 'batchno', batchno);
			if(ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflg == "T")
			{
				nlapiLogExecution('ERROR', 'batchno', batchno);
				if(batchno!="" && batchno!=null)
				{

					var filterspor = new Array();
					filterspor.push(new nlobjSearchFilter('name', null, 'is', batchno));
					/*if(whLocation!=null && whLocation!="")
						filterspor.psuh(new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', whLocation));*/
					if(itemid!=null && itemid!="")
						filterspor.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemid));

					var column=new Array();
					column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
					column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');

					var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
					if(receiptsearchresults!=null)
					{
						getlotnoid= receiptsearchresults[0].getId();
						nlapiLogExecution('ERROR', 'getlotnoid', getlotnoid);
						expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');
						vfifoDate=receiptsearchresults[0].getValue('custrecord_ebizfifodate');
					}
					nlapiLogExecution('ERROR', 'expdate', expdate);
					nlapiLogExecution('ERROR', 'vfifoDate', vfifoDate);
				}
			}
			if(getlotnoid!=null && getlotnoid!='')
			{
				var fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
				invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
			}
			if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem"|| batchflg == "T")
//				if (ItemType == "lotnumberedinventoryitem" || batchflg == "T")
			{ 										
				try
				{
					//Checking FIFO Policy.					
					if (getlotnoid != "") {
						if(vfifoDate ==null ||vfifoDate=='')
						{
							fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
							nlapiLogExecution('ERROR','FIFO Value Check.',fifovalue);
						}
						else
						{
							fifovalue = vfifoDate;
						}
						nlapiLogExecution('ERROR','fifovalue.',fifovalue);
						invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
						invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
						invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
					}
				}
				catch(exp)
				{
					nlapiLogExecution('ERROR','Exception in FIFO Value Check.',exp);
				}
			}
			else
				invtRec.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());


			nlapiLogExecution('ERROR', 'Before Submitting invtrecid', 'INVTRECORDS');

			invtrecid = nlapiSubmitRecord(invtRec, false, true);

			nlapiLogExecution('ERROR', 'After Submitting invtrecid', invtrecid);
			if (invtrecid != null) {
				nlapiLogExecution('ERROR', 'Cust INVT Rec Creation in merge lp true and inv search rwsult is null Succes with ID ', invtrecid);
			}
			else {
				nlapiLogExecution('ERROR', 'Cust INVT Rec Creation Fail ', 'Fail');
			}

			//Added for updation of Cube information by ramana on 10th june 2011

			nlapiLogExecution('ERROR', 'OLD BIN LOCATION DETAILS ', putoldBinLoc);
			nlapiLogExecution('ERROR', 'NEW BIN LOCATION DETAILS ', putBinLoc);
			nlapiLogExecution('ERROR', 'ITEMID', putItemId);
			nlapiLogExecution('ERROR', 'LP', putLP);

			if(putBinLoc!=putoldBinLoc)
			{   
				/*
						//For the OLD Location Updation Purpose.    											 											
				 */

				var arrDims = getSKUCubeAndWeight(putItemId, 1);
				var itemCube = 0;
				if (arrDims["BaseUOMItemCube"] != "" && (!isNaN(arrDims["BaseUOMItemCube"]))) 
				{
					var uomqty = ((parseFloat(putQty))/(parseFloat(arrDims["BaseUOMQty"])));			
					itemCube = (parseFloat(uomqty) * parseFloat(arrDims["BaseUOMItemCube"]));
					nlapiLogExecution('ERROR', 'checkInPOSTRequest:itemCube', itemCube);
				} 
				if(putoldBinLoc != "" && putoldBinLoc != null)
				{
					var vOldRemainingCube = GeteLocCube(putoldBinLoc);
				}
				nlapiLogExecution('ERROR', 'vOldRemainingCube', vOldRemainingCube);

				var vTotalCubeValue = parseFloat(itemCube)+ parseFloat(vOldRemainingCube);
				nlapiLogExecution('ERROR', 'vOldLocationRemainingCube', vTotalCubeValue);
				if(putoldBinLoc != "" && putoldBinLoc != null)
				{
					var retValue =  UpdateLocCube(putoldBinLoc,vTotalCubeValue);
				}
				//upto to here old Location Updation purpose

				//For the New Location updation

				var vNewLocationRemainingCube = GeteLocCube(putBinLoc);   

				nlapiLogExecution('ERROR', 'vNewLocationRemainingCube', vNewLocationRemainingCube);

				//var vTotalNewLocation =  parseFloat(vartotalCube)- parseFloat(vNewLocationRemainingCube);
				var vTotalNewLocationCube =  parseFloat(vNewLocationRemainingCube)- parseFloat(itemCube);

				nlapiLogExecution('ERROR', 'vTotalNewLocation', vTotalNewLocationCube);

				if(vTotalNewLocationCube<0)
				{
					nlapiLogExecution('ERROR', 'inside PO Overage Validation', 'inside loop');					  
					var form = nlapiCreateForm('Confirm Putaway');
					nlapiLogExecution('ERROR', 'Form Called', 'form');					  
					var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);		  
					msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'Qty exceeds location capacity', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");		    		   
					nlapiLogExecution('ERROR', 'message', msg);					  
					response.writePage(form);                  						
					return;			
				}
				else
				{
					var retValue1 = UpdateLocCube(putBinLoc,vTotalNewLocationCube);
				}
				//upto to here new location

			}                     
			//upto to here on 10th june 2011

//			for (var i = 0; i < searchresults.length; i++) {
//			vbatchno=searchresults[i].getValue('custrecord_batch_no');
//			var searchresult = searchresults[i];
//			var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', searchresult.getId());

//			nlapiLogExecution('ERROR', 'STATUS before', searchresult.getValue('custrecord_wms_status_flag'));

//			if (searchresult.getValue('custrecord_wms_status_flag') == 6) {
//			transaction.setFieldValue('custrecord_actbeginloc', getBeginLocationInternalId);// Give location Id.
//			transaction.setFieldValue('custrecordact_begin_date', DateStamp());
//			//transaction.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
//			}

//			transaction.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
//			transaction.setFieldValue('custrecord_recordupdatetime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
//			transaction.setFieldValue('custrecord_upd_date', DateStamp());
//			transaction.setFieldValue('custrecord_actendloc', EndLocationId);// Give location Id.
//			transaction.setFieldValue('custrecord_act_end_date', DateStamp());
//			transaction.setFieldValue('custrecord_wms_status_flag', 3);
//			transaction.setFieldValue('custrecord_act_qty', parseFloat(ActQuantity).toFixed(4));
//			//vputwrecid=nlapiSubmitRecord(transaction, false, true);
//			//MoveTaskRecord(vputwrecid);
//			var recid=nlapiSubmitRecord(transaction, false, true);
//			//vputwrecid[1]='';
//			//MoveTaskRecord(vputwrecid[0]);
//			} 

			//MoveTaskRecord(opentaskintid);
			/*	if(vEbizTrailerNo != null && vEbizTrailerNo != '')
			{
				if(vEbizReceiptNo != null && vEbizReceiptNo != '')
				{
					nlapiLogExecution('ERROR', 'vEbizReceiptNo', vEbizReceiptNo);
					nlapiLogExecution('ERROR', 'vEbizTrailerNo', vEbizTrailerNo);
					var poReceiptRec = nlapiLoadRecord('customrecord_ebiznet_trn_poreceipt', vEbizReceiptNo);

					if(poReceiptRec != null && poReceiptRec != '')
					{
						var previousrcvqty=0;
						if(poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_rcvqty') != null && poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_rcvqty') != '')
						{
							previousrcvqty=poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_rcvqty');
						}	
						var newrcvQty= parseFloat(previousrcvqty)+parseFloat(putQty);

						nlapiLogExecution('ERROR', 'previousrcvqty', previousrcvqty);
						nlapiLogExecution('ERROR', 'qty', qty);
						nlapiLogExecution('ERROR', 'newrcvQty', newrcvQty);

						var id=nlapiSubmitField('customrecord_ebiznet_trn_poreceipt', vEbizReceiptNo, 'custrecord_ebiz_poreceipt_rcvqty',newrcvQty.toString());
						nlapiLogExecution('ERROR','id',id);
					}	
				}	
			}*/
		}
		//serial Entry status updation to S
		/*	if (ItemType == "serializedinventoryitem"|| ItemType == "serializedassemblyitem" || serialInflg == "T") {
			try {
				if (tempserialId != null) {
					var temSeriIdArr = new Array();
					temSeriIdArr = tempserialId.split(',');
					nlapiLogExecution('ERROR', 'updating Serial num Records to', 'Storage Status');
					for (var p = 0; p < temSeriIdArr.length; p++) {
						var fields = new Array();
						var values = new Array();
						fields[0] = 'custrecord_serialwmsstatus';
						values[0] = '3';
						var updateStatus = nlapiSubmitField('customrecord_ebiznetserialentry', temSeriIdArr[p], fields, values);
						nlapiLogExecution('ERROR', 'Inside loop serialId ', temSeriIdArr[p]);

						nlapiLogExecution('ERROR', 'Serial num Records to', 'Storage Status Success');
					}
				}
			} 
			catch (myexp) {
				nlapiLogExecution('ERROR', 'Into Serial num exeption', myexp);
			}
		}*/
	}


	else{
		var accountNumber='';
		var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');

		nlapiLogExecution('ERROR', 'Creating INVT  Record', 'INVT');

		invtRec.setFieldValue('name', itemid);
		invtRec.setFieldValue('custrecord_ebiz_inv_binloc', EndLocationId);
		invtRec.setFieldValue('custrecord_ebiz_inv_lp', invtlp);
		invtRec.setFieldValue('custrecord_ebiz_inv_sku', itemid);
		invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
		if(itempackcode!=null&&itempackcode!="")
			invtRec.setFieldValue('custrecord_ebiz_inv_packcode', itempackcode);
		invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(quantity).toFixed(4));
		//invtRec.setFieldValue('custrecord_ebiz_inv_lot', batchno);
		//invtRec.setFieldValue('custrecord_ebiz_inv_recorddate',(parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
		//invtRec.setFieldValue('custrecord_ebiz_inv_rectime',((curr_hour) + ":" + (curr_min) + " " + a_p));
		invtRec.setFieldValue('custrecord_inv_ebizsku_no', itemid);
		invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(quantity).toFixed(4));
		invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemdesc);
		invtRec.setFieldValue('custrecord_ebiz_inv_loc', whloc);
		invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');//WH Location.
		invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);
		invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
		invtRec.setFieldValue('custrecord_wms_inv_status_flag','19');//19=FLAG.INVENTORY.STORAGE
		invtRec.setFieldValue('custrecord_invttasktype', '5');
		invtRec.setFieldValue('custrecord_ebiz_transaction_no', pointid);
		var getuomlevel='';
		var eBizItemDims=geteBizItemDimensions(itemid);
		if(eBizItemDims!=null&&eBizItemDims.length>0)
		{
			for(z=0; z < eBizItemDims.length; z++)
			{
				if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
				{
					getBaseUOM = eBizItemDims[z].getText('custrecord_ebizuomskudim');
					getuomlevel = eBizItemDims[z].getText('custrecord_ebizuomlevelskudim');
				}
			}
		}
		if(getuomlevel!=null && getuomlevel!='')
			invtRec.setFieldText('custrecord_ebiz_uomlvl', getuomlevel);

		var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group'];
		var columns = nlapiLookupField('item', itemid, fields);
		var ItemType = columns.recordType;					
		var batchflg = columns.custitem_ebizbatchlot;
		var itemfamId= columns.custitem_item_family;
		var itemgrpId= columns.custitem_item_group;
		var getlotnoid="";
		var expdate='';
		nlapiLogExecution('ERROR', 'ItemType', ItemType);
		nlapiLogExecution('ERROR', 'batchflg', batchflg);
		nlapiLogExecution('ERROR', 'batchno', batchno);
		if(ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflg == "T")
		{
			nlapiLogExecution('ERROR', 'batchno', batchno);
			if(batchno!="" && batchno!=null)
			{

				var filterspor = new Array();
				filterspor[0] = new nlobjSearchFilter('name', null, 'is', batchno);
				if(whloc!=null && whloc!="")
					filterspor[1] = new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', whloc);
				if(itemid!=null && itemid!="")
					filterspor[2] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemid);

				var column=new Array();
				column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
				column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');

				var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
				if(receiptsearchresults!=null)
				{
					getlotnoid= receiptsearchresults[0].getId();
					nlapiLogExecution('ERROR', 'getlotnoid', getlotnoid);
					expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');
					vfifodate=receiptsearchresults[0].getValue('custrecord_ebizfifodate');
					invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
					invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
					invtRec.setFieldValue('custrecord_ebiz_inv_fifo', vfifodate);
				}
				nlapiLogExecution('ERROR', 'expdate', expdate);
			}
		}
		if(getlotnoid!=null && getlotnoid!='')
		{
			var fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
			//invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
		}
		if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem"|| batchflg == "T")
//			if (ItemType == "lotnumberedinventoryitem" || batchflg == "T")
		{ 										
			try
			{
				//Checking FIFO Policy.					
				if (getlotnoid != "") {
					/*fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
				nlapiLogExecution('ERROR','FIFO Value Check.',fifovalue);
				invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
				invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
				invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);*/
				}
			}
			catch(exp)
			{
				nlapiLogExecution('ERROR','Exception in FIFO Value Check.',exp);
			}
		}
		else
			invtRec.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());



		nlapiLogExecution('ERROR', 'Before Submitting invtrecid', 'INVTRECORDS');

		invtrecid = nlapiSubmitRecord(invtRec, false, true);

		nlapiLogExecution('ERROR', 'After Submitting invtrecid', invtrecid);
		if (invtrecid != null) {
			nlapiLogExecution('ERROR', 'Cust INVT Rec Creation Succes with ID ', invtrecid);
		}
		else {
			nlapiLogExecution('ERROR', 'Cust INVT Rec Creation Fail ', 'Fail');
		}


	}
	
	if(invtrecid != null && invtrecid != '' ){
		var transactionrec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordId);
		transactionrec.setFieldValue('custrecord_invref_no', invtrecid);
		nlapiSubmitRecord(transactionrec, false, true);
		
		}

	return invtrecid;
}



var vExpDateGlobArr=new Array();

function transformAssemblyItem(assemblyrecid,vqty,vloc,varLotNo,vitem,vExpDate)
{
	try
	{

		nlapiLogExecution('ERROR', 'transformAssemblyItem',assemblyrecid);
		nlapiLogExecution('ERROR', 'transformAssembly_qty',vqty);
		nlapiLogExecution('ERROR', 'vloc',vloc);
		nlapiLogExecution('ERROR', 'varLotNo',varLotNo);
		nlapiLogExecution('ERROR', 'vitem',vitem);
		nlapiLogExecution('ERROR', 'vExpDate',vExpDate);
		var fromRecord = 'workorder'; 
		var toRecord = 'assemblybuild';

		var vAdvBinManagement=false;
		var context = nlapiGetContext();
		if(context != null && context != '')
		{
			if(context.getFeature('advbinseriallotmgmt').toString() != null && context.getFeature('advbinseriallotmgmt').toString() != '')
				vAdvBinManagement=context.getFeature('advbinseriallotmgmt');
			nlapiLogExecution('ERROR', 'vAdvBinManagement_if', vAdvBinManagement);
		}
		var record = nlapiTransformRecord(fromRecord, assemblyrecid, toRecord);

		record.setFieldValue('quantity', vqty);
		record.setFieldValue('location', vloc);	
		nlapiLogExecution('ERROR', 'Location for main component',vloc);
		nlapiLogExecution('ERROR', 'Quantity for main component',vqty);

		var confirmLotToNS='Y';
		confirmLotToNS=GetConfirmLotToNS(vloc);


		nlapiLogExecution('ERROR', 'varLotNo1', varLotNo + "(" + vqty + ")");


		// To calculate the no of components for the main assembly.
		nlapiLogExecution('ERROR', 'vitem',vitem);
		var ComponentItemType = nlapiLookupField('item', vitem, 'recordType');
		var fields = ['recordType','custitem_ebizbatchlot'];
		//nlapiLogExecution('ERROR','ItemType',ItemType);
		var searchresultsitem;
		var batchflag="F";
		var ItemType="";
		if(ComponentItemType!=null && ComponentItemType !='')
		{
			nlapiLogExecution('ERROR','Item Type',ComponentItemType);
			var columns = nlapiLookupField(ComponentItemType, vitem, fields); 

			ItemType = columns.recordType;
			batchflag= columns.custitem_ebizbatchlot; 
			searchresultsitem = nlapiLoadRecord(ItemType, vitem); //1020
		}

		else
		{
			nlapiLogExecution('ERROR','Item Type is NULL ',ItemType);
		}

		var SkuNo=searchresultsitem.getFieldValue('itemid'); 

		if(confirmLotToNS=='N')
			varLotNo=SkuNo.replace(/ /g,"-");
		nlapiLogExecution('ERROR','varLotNo Main',varLotNo);

		//if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflag=="T" )
		if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem"){
			record.setFieldValue('serialnumbers',  varLotNo + "(" + vqty + ")") ;
			if(vExpDate != null && vExpDate != '')
			{	
				record.setFieldValue('expirationdate',  vExpDate) ;
				var vLocExpDate=new Array();
				vLocExpDate[0]=vitem;
				vLocExpDate[1]=varLotNo;
				vLocExpDate[2]=vExpDate;
				vExpDateGlobArr.push(vLocExpDate);
			}
			if(!vAdvBinManagement)
			{	
				record.setFieldValue('serialnumbers',  varLotNo + "(" + vqty + ")") ;
			}
			else
			{
				var compSubRecord1 = record.createSubrecord('inventorydetail');
				compSubRecord1.selectNewLineItem('inventoryassignment');

				//nlapiLogExecution('ERROR', 'expdate1', expdate1);
				//if(expdate1 !=null && expdate1 !='')
				//compSubRecord.setCurrentLineItemValue('inventoryassignment', 'expirationdate', expdate1);
				if(confirmLotToNS == 'Y')
					compSubRecord1.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', varLotNo);
				else
					compSubRecord1.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vitem);
				compSubRecord1.setCurrentLineItemValue('inventoryassignment', 'quantity', vqty);
				compSubRecord1.commitLineItem('inventoryassignment');
				compSubRecord1.commit();

			}
		}
		var recCount=0; 
		var filters = new Array(); 
		//filters[0] = new nlobjSearchFilter('itemid', null, 'is', request.getParameter("custparam_item"));//kit_id is the parameter name 
		filters[0] = new nlobjSearchFilter('itemid', null, 'is', SkuNo);//kit_id is the parameter name 
		var columns1 = new Array(); 


		columns1[0] = new nlobjSearchColumn( 'memberitem' ); 


		//  columns1[0] = new nlobjSearchColumn( 'internalid' ); 
		columns1[1] = new nlobjSearchColumn( 'memberquantity' );

		var j=0,k=1;
		var searchresults = nlapiSearchRecord( 'item', null, filters, columns1 ); 
		var vAlert='';
		var CompItemCount=0;
		if(record !=null && record!='')
		{
			CompItemCount=record.getLineItemCount('component');
		}
		nlapiLogExecution('ERROR','Member items count is ',searchresults.length);
		var j=1;	
//		for(var i=1; i<=searchresults.length;i++) // This loop contains the member items of the assembly 
//		{ 
		var previtem,prevbatch, prevQty , strbatchtoNS,prevLine,prevItemText; // These variables are to track the item and batch and consolidate the data and pass inputs to assembly build
		var boolfirstitem= true;



		var lineCnt='';
		var filterOpentask = new Array();
		filterOpentask[0] = new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', assemblyrecid);
		filterOpentask[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['8']);
		//filterOpentask[2] = new nlobjSearchFilter('custrecord_notes', null, 'isnotempty');
		filterOpentask[2] = new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty');
		filterOpentask[3] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', 3);
		
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_line_no');
		columns[1] = new nlobjSearchColumn('custrecord_sku');
		columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[3] = new nlobjSearchColumn('custrecord_lpno');
		columns[4] = new nlobjSearchColumn('custrecord_batch_no');
		columns[5] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[6] = new nlobjSearchColumn('custrecord_act_qty');                                    
		columns[0].setSort();

		var Opentaskresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterOpentask,columns);	

		if(Opentaskresults!=null && Opentaskresults!=''){
			lineCnt = Opentaskresults.length;

		}

		nlapiLogExecution('ERROR', 'lineCnt',lineCnt);

		for (var k = 0; k < lineCnt; k++) 
		{				

			//var LotNo = request.getLineItemValue('custpage_deliveryord_items', 'custpage_hiddenlotnotext', k);
			var LotNo="";
			var varqty = Opentaskresults[k].getValue('custrecord_expe_qty');
			//var varsku =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_sku', k);
			var varsku = Opentaskresults[k].getValue('custrecord_sku');
			nlapiLogExecution('ERROR', 'varsku',varsku);
			//var varskuText =  request.getLineItemText('custpage_deliveryord_items', 'custpage_deliveryord_sku', k);
			var varLineno =  Opentaskresults[k].getValue('custrecord_line_no'); 
			LotNo = Opentaskresults[k].getValue('custrecord_batch_no');

			var varskuText;
			var vnItemType="";
			var ItemTypeComp = nlapiLookupField('item', varsku, ['recordType','itemid']);
		/*	var searchresultsitemComp = nlapiLoadRecord(ItemTypeComp, varsku); //1020
			if(searchresultsitemComp != null && searchresultsitemComp != "")
				varskuText= searchresultsitemComp.getFieldValue('itemid');*/
			if(ItemTypeComp != null && ItemTypeComp != '')
			{
				varskuText=ItemTypeComp.itemid;
				vnItemType = ItemTypeComp.recordType;
			}
			nlapiLogExecution('ERROR','vnItemType',vnItemType);
			nlapiLogExecution('ERROR', 'varskuText',varskuText);

			nlapiLogExecution('ERROR', 'LotNo',LotNo);
			nlapiLogExecution('ERROR', 'k',k);
			nlapiLogExecution('ERROR', 'Lot number sending for component',LotNo + "(" + varqty + ")");

			if (boolfirstitem == true) // If first time , store the values to previous variables.
			{

				nlapiLogExecution('ERROR','First time cond');
				previtem = varsku;
				prevItemText=varskuText;
				prevbatch = LotNo;
				prevQty = varqty;
				prevLine=varLineno;
				nlapiLogExecution('ERROR', 'previtem',previtem);
				nlapiLogExecution('ERROR', 'prevbatch',prevbatch);
				nlapiLogExecution('ERROR', 'prevQty',prevQty);
				
				if(CompItemCount !=null && CompItemCount!='' && CompItemCount!=0)
				{
					if(parseInt(searchresults.length) == 1)
					{

						var itemIndex=1;
						var prevTempbatch ='';
						var prevTempQty ='';
						var strbatchtoNS='';
						for (var s = 0; s < lineCnt; s++) 
						{
							/*var varTempqty =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_qtyexp', s);
						var varTempsku =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_hiddenitemid', s);

						var varTempLineno=  request.getLineItemValue('custpage_deliveryord_items', 'custpage_lineno', s);
						var varTempLotNo = request.getLineItemValue('custpage_deliveryord_items', 'custpage_hiddenlotnotext', s);*/

							var varTempqty =  Opentaskresults[s].getValue('custrecord_expe_qty');;
							var varTempsku =  Opentaskresults[s].getValue('custrecord_sku');

							var varTempLineno=  Opentaskresults[s].getValue('custrecord_line_no'); ;
							var varTempLotNo = Opentaskresults[s].getValue('custrecord_batch_no');
							if(s == 0)
							{
								strbatchtoNS = varTempLotNo + "(" + varTempqty + ")";
								nlapiLogExecution('Debug', "s=1", strbatchtoNS);
							}
							else
							{
								if (prevTempbatch == varTempLotNo) 
								{
									prevTempQty = parseFloat(prevTempQty) + parseFloat(varTempqty); 
									strbatchtoNS = prevTempbatch + "(" + prevTempQty + ")";		
									nlapiLogExecution('Debug', "strbatchtoNS_if", strbatchtoNS);
								}
								else
								{
									nlapiLogExecution('Debug', "strbatchtoNS_else", strbatchtoNS);
									if(strbatchtoNS != null && strbatchtoNS != "")
										strbatchtoNS = strbatchtoNS + ',' + varTempLotNo + "(" + varTempqty + ")"; 
									else
										strbatchtoNS = varTempLotNo + "(" + varTempqty + ")";
								}
							}
							prevTempbatch = varTempLotNo;
							prevTempQty = varTempqty;
						}

						nlapiLogExecution('Debug', "itemIndex", itemIndex);
						//nlapiLogExecution('ERROR', 'CompItemCount',CompItemCount);
						//var strbatchtoNS = prevbatch + "(" + prevQty + ")";
						nlapiLogExecution('ERROR', 'strbatchtoNS11',strbatchtoNS);
						record.selectLineItem('component', 1);
						//record.setLineItemValue('component', 'item', prevLine,varsku) ; 
						record.setLineItemValue('component', 'item', itemIndex,varsku) ; 
						record.setLineItemValue('component', 'componentnumbers', prevLine,strbatchtoNS) ;
						record.setCurrentLineItemValue('component', 'quantity', prevQty);

						var TempLotArray =new Array();
						if(strbatchtoNS !=null && strbatchtoNS !='' && strbatchtoNS !='null')
						{
							TempLotArray=strbatchtoNS.split(',');
							nlapiLogExecution('ERROR','TempLotArray ',TempLotArray.length);
						}

						if(!vAdvBinManagement)
						{	
							if(vnItemType == "lotnumberedinventoryitem" || vnItemType=="lotnumberedassemblyitem")
								record.setLineItemValue('component', 'componentnumbers', itemIndex,strbatchtoNS) ;
						}
						else
						{
							if(vnItemType == "lotnumberedinventoryitem" || vnItemType=="lotnumberedassemblyitem")
							{
								var vcompSubRecord = record.editCurrentLineItemSubrecord('component','componentinventorydetail');
								for(var s1=0;s1<TempLotArray.length;s1++)
								{
									var TempLotNumber=TempLotArray[s1];
									nlapiLogExecution('ERROR', 'TempLotNumber',TempLotNumber);
									var TempPos = TempLotNumber.indexOf("(")+1;
									nlapiLogExecution('ERROR', 'TempPos ', TempPos);
									var TempvQty=TempLotNumber.slice(TempPos, -1);
									nlapiLogExecution('ERROR', 'TempvQty', TempvQty);
									var vTempLotno=TempLotNumber.slice(0, TempPos-1);
									nlapiLogExecution('ERROR', 'vTempLotno', vTempLotno);
									if(vcompSubRecord !=null && vcompSubRecord!='')
									{
										nlapiLogExecution('ERROR', 'vcompSubRecord', vcompSubRecord);
										//vcompSubRecord.selectLineItem('inventoryassignment', 1);	
										var subrecordcount=vcompSubRecord.getLineItemCount('inventoryassignment')
										nlapiLogExecution('ERROR','subrecordcount1',subrecordcount);
										if(subrecordcount>1)
											vcompSubRecord.selectLineItem('inventoryassignment', 1);
										else
											vcompSubRecord.selectNewLineItem('inventoryassignment');	

									}
									else								
									{
										nlapiLogExecution('ERROR','else ','done');
										vcompSubRecord = record.createCurrentLineItemSubrecord('component', 'componentinventorydetail');
										vcompSubRecord.selectNewLineItem('inventoryassignment');
									}
									//vcompSubRecord.selectNewLineItem('inventoryassignment');
									vcompSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vTempLotno);
									vcompSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', TempvQty);
									vcompSubRecord.commitLineItem('inventoryassignment');
									//vcompSubRecord.commit();
								}
								vcompSubRecord.commit();
								record.commitLineItem('component');

								var vcompSubRecord = record.editCurrentLineItemSubrecord('component','componentinventorydetail');

								nlapiLogExecution('ERROR', 'vcompSubRecord123',vcompSubRecord);

								if(vcompSubRecord !=null && vcompSubRecord!='')
								{
									vcompSubRecord.selectLineItem('inventoryassignment', 1);
								}
								else								
								{
									nlapiLogExecution('ERROR', 'create',varsku);																		 
									vcompSubRecord = record.createCurrentLineItemSubrecord('component', 'componentinventorydetail');
									vcompSubRecord.selectNewLineItem('inventoryassignment');
								}
								//vcompSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', strbatchtoNS);
								vcompSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', prevbatch);
								vcompSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', prevQty);
								//vcompSubRecord.setCurrentLineItemValue('inventoryassignment', 'binnumber', 3);
								vcompSubRecord.commitLineItem('inventoryassignment');
								vcompSubRecord.commit();
								record.commitLineItem('component');

							}
						}
					}
				}
			}

			else // for boolfirstitem ==true
			{
				var WOLength=0;
				var itemIndex=0;
				if(record!=null && record !='')
				{
					WOLength = record.getLineItemCount('component');  

					for (var cnt1 = 1; cnt1 <= WOLength; cnt1++) {

						var itemLineNo = record.getLineItemValue('component', 'orderline', cnt1);

						nlapiLogExecution('Debug', "itemLineNo", itemLineNo); 
						nlapiLogExecution('Debug', "prevLine here", prevLine);
						if (itemLineNo == prevLine) {
							itemIndex=cnt1;    
							break;
							//nlapiLogExecution('Debug', "itemIndex", itemIndex);
						}
					}
				}
				nlapiLogExecution('Debug', "itemIndex", itemIndex);
				if ( previtem == varsku  && prevLine==varLineno)

				{
					if (prevbatch == LotNo) // If the batch no is same , then qty is getting added.

					{nlapiLogExecution('ERROR','Batch no same_strbatchtoNS',strbatchtoNS);
					nlapiLogExecution('ERROR','Batch no same',lineCnt);
					prevQty = parseFloat(prevQty) + parseFloat(varqty); 
					nlapiLogExecution('ERROR','Batch no same - Qty',prevQty);

					if ( k == lineCnt) // If this is last line in the grid, we have to submitt details now
					{
						//nlapiLogExecution('ERROR','k=linecnt cond. this is lastline','');
						if(strbatchtoNS != null && strbatchtoNS != "")
							strbatchtoNS = strbatchtoNS + ',' + prevbatch + "(" + prevQty + ")"; // Batch string for item is prepared
						else
							strbatchtoNS = prevbatch + "(" + prevQty + ")";

						//nlapiLogExecution('ERROR', 'Lot number sending for component',LotNo + "(" + varqty + ")");
						//Assigning the details for first item to NS build assembly

					//	nlapiLogExecution('ERROR','NS Item1',record.getLineItemValue('component', 'item', j));
						nlapiLogExecution('ERROR','NS Item1',record.getLineItemValue('component', 'item', itemIndex));
					//	record.setLineItemValue('component', 'item', j,varsku) ;   // Index i is the index for member items of assembly
						record.selectLineItem('component', itemIndex);
						if(confirmLotToNS=='N')
							strbatchtoNS=varskuText.replace(/ /g,"-");

						//strbatchtoNS=varskuText.replace(/ /g,"-");
						nlapiLogExecution('ERROR','NS strbatchtoNS1 ',strbatchtoNS);
						nlapiLogExecution('ERROR','NS strbatchtoNSFinal1 ',strbatchtoNS);
						//record.setLineItemValue('component', 'componentnumbers', j,strbatchtoNS) ; // Assigning batch string for the first item
						if(!vAdvBinManagement)
						{	

							record.setLineItemValue('component', 'componentnumbers', itemIndex,strbatchtoNS) ;
						}
						else
						{
							compSubRecord = record.editCurrentLineItemSubrecord('component','componentinventorydetail');

							nlapiLogExecution('ERROR','Lastlinecustrecord',compSubRecord);
							if(compSubRecord !=null && compSubRecord!='' )
							{
								var subrecordcount=compSubRecord.getLineItemCount('inventoryassignment')
								nlapiLogExecution('ERROR','subrecordcount1',subrecordcount);
								if(subrecordcount>1)
									compSubRecord.selectLineItem('inventoryassignment', 1);
								else
									compSubRecord.selectNewLineItem('inventoryassignment');	
								//compSubRecord.selectLineItem('inventoryassignment', 1);
							}
							else								
							{
								nlapiLogExecution('ERROR','subrecordcountelse','Else');
								compSubRecord = record.createCurrentLineItemSubrecord('component', 'componentinventorydetail');
								compSubRecord.selectNewLineItem('inventoryassignment');
							}


							/*if(compSubRecord !=null && compSubRecord!='' )
							{
								//compSubRecord = record.createCurrentLineItemSubrecord('component', 'componentinventorydetail');
								//compSubRecord.selectNewLineItem('inventoryassignment');
//								var subrecordcount=compSubRecord.getLineItemCount('inventoryassignment')
//								if(subrecordcount>1)
//								{
//									nlapiLogExecution('ERROR','testrecrd','testrecrd');
//									compSubRecord = record.createCurrentLineItemSubrecord('component', 'componentinventorydetail');
//									compSubRecord.selectNewLineItem('inventoryassignment');
//								}											
//								else

									compSubRecord.selectNewLineItem('inventoryassignment');	
							}
							else								
							{
								compSubRecord = record.createCurrentLineItemSubrecord('component', 'componentinventorydetail');
								compSubRecord.selectNewLineItem('inventoryassignment');
							}*/
							nlapiLogExecution('ERROR','prevbatch',prevbatch);
							nlapiLogExecution('ERROR','prevQty',prevQty);
							compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', prevbatch);
							compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', prevQty);
							compSubRecord.commitLineItem('inventoryassignment');
							compSubRecord.commit();
							record.commitLineItem('component');



						}
						nlapiLogExecution('ERROR','sending inputs for assembly. last item sku',varsku);
						nlapiLogExecution('ERROR','sending inputs for assembly. last item lotnos',strbatchtoNS);
						j=j+1;
						strbatchtoNS='';

					} // end if for k == linecnt cond
					} // end if for prevbatch = lotno
					else // else for prevbatch = lotno // If the batch no changes , then preparing the batch string for first batch and storing second batch details

					{
						nlapiLogExecution('ERROR','Batch no Change-SKU same');
						nlapiLogExecution('ERROR','Batch no Change-Batchstring');
						if(strbatchtoNS != null && strbatchtoNS != "")
							strbatchtoNS = strbatchtoNS + ',' + prevbatch + "(" + prevQty + ")"; // Batch string for item is prepared
						else
							strbatchtoNS = prevbatch + "(" + prevQty + ")";
						nlapiLogExecution('ERROR','Batch no Change-Batchstring',strbatchtoNS);
						prevbatch = LotNo;
						prevQty = parseFloat(varqty);

						if ( k+1 == lineCnt) // If this is last line in the grid, we have to submitt details now
						{
							nlapiLogExecution('ERROR','This is lastline');
							if(strbatchtoNS != null && strbatchtoNS != "")
								strbatchtoNS = strbatchtoNS + ',' + prevbatch + "(" + prevQty + ")"; // Batch string for item is prepared
							else
								strbatchtoNS = prevbatch + "(" + prevQty + ")";

							//Assigning the details for first item to NS build assembly

						//	nlapiLogExecution('ERROR','NS Item2',record.getLineItemValue('component', 'item', j));
						//	record.setLineItemValue('component', 'item', j,varsku) ;   // Index i is the index for member items of assembly
							nlapiLogExecution('ERROR','NS Item2',record.getLineItemValue('component', 'item', itemIndex));
							record.selectLineItem('component', itemIndex);
							record.setLineItemValue('component', 'item', itemIndex,varsku) ;

							if(confirmLotToNS=='N')
								strbatchtoNS=varskuText.replace(/ /g,"-");
							//strbatchtoNS=varskuText.replace(/ /g,"-");
							nlapiLogExecution('ERROR','NS strbatchtoNS2 ',strbatchtoNS);

							//strbatchtoNS=varskuText.replace(/ /g,"-");
							nlapiLogExecution('ERROR','NS strbatchtoNS1 ',strbatchtoNS);
							nlapiLogExecution('ERROR','NS strbatchtoNSFinal3 ',strbatchtoNS);
							nlapiLogExecution('ERROR','NS line ',j);
							var LotArray =new Array();
							if(strbatchtoNS !=null && strbatchtoNS !='' && strbatchtoNS !='null')
							{
								LotArray=strbatchtoNS.split(',');
								nlapiLogExecution('ERROR','LotArray ',LotArray.length);
							}
						//	record.setLineItemValue('component', 'componentnumbers', j,strbatchtoNS) ; // Assigning batch string for the first item
							if(!vAdvBinManagement)
							{	

								record.setLineItemValue('component', 'componentnumbers', itemIndex,strbatchtoNS) ;
							}
							else
							{

								var compSubRecord = record.editCurrentLineItemSubrecord('component','componentinventorydetail');
								for(var d=0;d<LotArray.length;d++)
								{
									var LotNumber=LotArray[d];
									nlapiLogExecution('ERROR', 'LotNumber',LotNumber);
									var pos = LotNumber.indexOf("(")+1;
									nlapiLogExecution('ERROR', 'pos ', pos);
									var vQty=LotNumber.slice(pos, -1);
									nlapiLogExecution('ERROR', 'vQty', vQty);
									var vLotno=LotNumber.slice(0, pos-1);
									nlapiLogExecution('ERROR', 'vLotno', vLotno);
									if(compSubRecord !=null && compSubRecord!='')
									{
										nlapiLogExecution('ERROR', 'compSubRecord', compSubRecord);
										/*var subrecordcount=compSubRecord.getLineItemCount('inventoryassignment');
										nlapiLogExecution('ERROR','subrecordcount ',subrecordcount);
										if(subrecordcount>1)
											compSubRecord.selectLineItem('inventoryassignment', 1);
										else*/
										compSubRecord.selectNewLineItem('inventoryassignment');	

									}
									else								
									{
										nlapiLogExecution('ERROR','else ','done');
										compSubRecord = record.createCurrentLineItemSubrecord('component', 'componentinventorydetail');
										compSubRecord.selectNewLineItem('inventoryassignment');
									}
									//compSubRecord.selectNewLineItem('inventoryassignment');
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vLotno);
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vQty);
									compSubRecord.commitLineItem('inventoryassignment');
									//compSubRecord.commit();
								}
								compSubRecord.commit();
								record.commitLineItem('component');



							}
							nlapiLogExecution('ERROR','sending inputs for assembly. last item sku',varsku);
							nlapiLogExecution('ERROR','sending inputs for assembly. last item lotnos',strbatchtoNS);
							j=j+1;
							strbatchtoNS='';

						} // end if for k==linecnt cond


					}
				} // end if for previtem==varsku.
				else // for previtem==varsku cond else for previtem == varsku cond.  If the prev and current items are not equal , assign prev details to NS and store new items details
				{
					nlapiLogExecution('ERROR','SKU Changed');

					if (prevbatch != null && prevbatch !='' ) // When the item changes completing the preparation of batch string of first item
					{
						nlapiLogExecution('ERROR','SKU Changed1');
						if(strbatchtoNS!= null && strbatchtoNS != "")
							strbatchtoNS = strbatchtoNS + ',' + prevbatch + "(" + prevQty + ")"; // Batch string for item is prepared
						else
							strbatchtoNS = prevbatch + "(" + prevQty + ")"; // Batch string for item is prepared
						nlapiLogExecution('ERROR','varsku',varsku);
						nlapiLogExecution('ERROR','strbatchtoNS',strbatchtoNS);
						//Assigning the details for first item to NS build assembly

						nlapiLogExecution('ERROR','j',j);
					//	nlapiLogExecution('ERROR','NS Item3',record.getLineItemValue('component', 'item', j));
					//	record.setLineItemValue('component', 'item', j,previtem);   // Index i is the index for member items of assembly
						record.selectLineItem('component', itemIndex);
						record.setLineItemValue('component', 'item', itemIndex,previtem);

						if(confirmLotToNS=='N')
							strbatchtoNS=prevItemText.replace(/ /g,"-");
						//strbatchtoNS=prevItemText.replace(/ /g,"-");
						nlapiLogExecution('ERROR','NS strbatchtoNS1 ',strbatchtoNS);
						nlapiLogExecution('ERROR','NS strbatchtoNSFinal4 ',strbatchtoNS);
						nlapiLogExecution('ERROR','NS line ',j);
						var LotArray2 =new Array();
						if(strbatchtoNS !=null && strbatchtoNS !='' && strbatchtoNS !='null')
						{
							LotArray2=strbatchtoNS.split(',');
							nlapiLogExecution('ERROR','LotArray2 ',LotArray2.length);
						}
						
						//record.setLineItemValue('component', 'componentnumbers', j,strbatchtoNS) ; // Assigning batch string for the first item
						if(!vAdvBinManagement)								
							record.setLineItemValue('component', 'componentnumbers', itemIndex,strbatchtoNS) ;
						else
						{
							compSubRecord = record.editCurrentLineItemSubrecord('component','componentinventorydetail');

							for(var d1=0;d1<LotArray2.length;d1++)
							{
								var LotNumber2=LotArray2[d1];
								nlapiLogExecution('ERROR', 'LotNumber2',LotNumber2);
								var pos2 = LotNumber2.indexOf("(")+1;
								nlapiLogExecution('ERROR', 'pos2 ', pos2);
								var vQty2=LotNumber2.slice(pos2, -1);
								nlapiLogExecution('ERROR', 'vQty2', vQty2);
								var vLotno2=LotNumber2.slice(0, pos2-1);
								nlapiLogExecution('ERROR', 'vLotno2', vLotno2);

								if(compSubRecord !=null && compSubRecord!='')
								{
									var subrecordcount=compSubRecord.getLineItemCount('inventoryassignment')
									nlapiLogExecution('ERROR','subrecordcount',subrecordcount);
									if(subrecordcount>1)
										compSubRecord.selectLineItem('inventoryassignment', 1);
									else
										compSubRecord.selectNewLineItem('inventoryassignment');	
									//compSubRecord.selectLineItem('inventoryassignment', 1);
								}
								else								
								{
									nlapiLogExecution('ERROR','subrecordcountelse','Else');
									compSubRecord = record.createCurrentLineItemSubrecord('component', 'componentinventorydetail');
									compSubRecord.selectNewLineItem('inventoryassignment');
								}
								nlapiLogExecution('ERROR','Inuts to Assembly build -item',prevbatch);
								nlapiLogExecution('ERROR','Inuts to Assembly build -item',prevQty);
								compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vLotno2);
								compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vQty2);
								compSubRecord.commitLineItem('inventoryassignment');
							}
							compSubRecord.commit();
							record.commitLineItem('component');

						}
						j=j+1;
						nlapiLogExecution('ERROR','SKU Changed2');
						strbatchtoNS ='';

						nlapiLogExecution('ERROR','Inuts to Assembly build -item',varsku);
						nlapiLogExecution('ERROR','Inuts to Assembly build -lotno',strbatchtoNS);

						// Start assigning the data for the new item

						previtem = varsku;
						prevItemText=varskuText;
						prevbatch = LotNo;
						prevQty = varqty;
						prevLine=varLineno;
						if (k+1 == lineCnt) // If this is last line in the grid, we have to submitt details now
						{
							var WOLength=0;
							var itemIndex=0;
							if(record!=null && record !='')
							{
								WOLength = record.getLineItemCount('component');  

								for (var cnt2 = 1; cnt2 <= WOLength; cnt2++) {

									var itemLineNo = record.getLineItemValue('component', 'orderline', cnt2);

									nlapiLogExecution('Debug', "itemLineNo", itemLineNo); 
									nlapiLogExecution('Debug', "prevLine here", prevLine);
									if (itemLineNo == prevLine) {
										itemIndex=cnt2;    
										break;
										//nlapiLogExecution('Debug', "itemIndex", itemIndex);
									}
								}
							}
							nlapiLogExecution('ERROR','itemIndex',itemIndex);
							record.selectLineItem('component', itemIndex);
							
							nlapiLogExecution('ERROR','This is lastline');

							if(strbatchtoNS!= null && strbatchtoNS != "")
								strbatchtoNS = strbatchtoNS + ',' + prevbatch + "(" + prevQty + ")"; // Batch string for item is prepared
							else
								strbatchtoNS = prevbatch + "(" + prevQty + ")"; // Batch string for item is prepared


							//Assigning the details for first item to NS build assembly
							nlapiLogExecution('ERROR','NS Item',record.getLineItemValue('component', 'item', j));
							nlapiLogExecution('ERROR','varsku',varsku);
							nlapiLogExecution('ERROR','j',j);

							if(confirmLotToNS=='N')
								strbatchtoNS=varskuText.replace(/ /g,"-");

							//strbatchtoNS=varskuText.replace(/ /g,"-");
							nlapiLogExecution('ERROR','NS strbatchtoNS1 ',strbatchtoNS);
							record.setLineItemValue('component', 'item', itemIndex,varsku);

							//record.setLineItemValue('component', 'item', j,varsku) ;   // Index i is the index for member items of assembly
							nlapiLogExecution('ERROR','NS strbatchtoNSFinal2 ',strbatchtoNS);
						//	record.setLineItemValue('component', 'componentnumbers', j,strbatchtoNS) ; // Assigning batch string for the first item
							var LotArray1 =new Array();
							if(strbatchtoNS !=null && strbatchtoNS !='' && strbatchtoNS !='null')
							{
								LotArray1=strbatchtoNS.split(',');
								nlapiLogExecution('ERROR','LotArray1 ',LotArray1.length);
							}
							if(!vAdvBinManagement)
							{	

								record.setLineItemValue('component', 'componentnumbers', itemIndex,strbatchtoNS) ;
							}
							else
							{
								compSubRecord = record.editCurrentLineItemSubrecord('component','componentinventorydetail');

								for(var d2=0;d2<LotArray1.length;d2++)
								{
									var LotNumber1=LotArray1[d2];
									nlapiLogExecution('ERROR', 'LotNumber1',LotNumber1);
									var pos1 = LotNumber1.indexOf("(")+1;
									nlapiLogExecution('ERROR', 'pos1 ', pos1);
									var vQty1=LotNumber1.slice(pos1, -1);
									nlapiLogExecution('ERROR', 'vQty1', vQty1);
									var vLotno1=LotNumber1.slice(0, pos1-1);
									nlapiLogExecution('ERROR', 'vLotno1', vLotno1);
									if(compSubRecord !=null && compSubRecord!='')
									{
										var subrecordcount=compSubRecord.getLineItemCount('inventoryassignment')
										nlapiLogExecution('ERROR','subrecordcount',subrecordcount);
										if(subrecordcount>1)
											compSubRecord.selectLineItem('inventoryassignment', 1);
										else
											compSubRecord.selectNewLineItem('inventoryassignment');	
										//compSubRecord.selectLineItem('inventoryassignment', 1);
									}
									else								
									{
										nlapiLogExecution('ERROR','subrecordcountelse','Else');
										compSubRecord = record.createCurrentLineItemSubrecord('component', 'componentinventorydetail');
										compSubRecord.selectNewLineItem('inventoryassignment');
									}

									/*if(compSubRecord !=null && compSubRecord!='')
								{
									compSubRecord.selectLineItem('inventoryassignment', 1);
								}
								else								
								{
									compSubRecord = record.createCurrentLineItemSubrecord('component', 'componentinventorydetail');
									compSubRecord.selectNewLineItem('inventoryassignment');
								}*/
									//compSubRecord.selectNewLineItem('inventoryassignment');
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vLotno1);
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vQty1);
									compSubRecord.commitLineItem('inventoryassignment');
								}
								compSubRecord.commit();
								record.commitLineItem('component');



							}
							nlapiLogExecution('ERROR','sending inputs for assembly. last item sku',varsku);
							nlapiLogExecution('ERROR','sending inputs for assembly. last item lotnos',strbatchtoNS);
							j=j+1;
							strbatchtoNS='';

						} // end if for k==linecnt condf

					}// end if if (prevbatch != null && prevbatch !='' ) 


				}	// end if previtem==varsku



			}// end if for boolfirstitem ==true

			boolfirstitem= false;  // For second item onwards it should compare for current item and prev item

		} // End for loop (var k = 1; k <= lineCnt; k++) 



//		} // Ending for loop for Searchresults			

		nlapiLogExecution('ERROR','Before submitting assemblybuild');

		var id = nlapiSubmitRecord(record, false);

		nlapiLogExecution('ERROR','After submitting assemblybuild and id is ',id);
		//case 20123541 start
		if(id != null && id != '')
		{
			var updateNSinOpenTask = UpdateNsconfInOpenTask(assemblyrecid,id);

			if(vExpDate != null && vExpDate != '')
			{

				var filters = new Array();
				if(vitem!= null && vitem!= '')
					filters.push(new nlobjSearchFilter( 'item', null, 'is', vitem));
				if(varLotNo!= null && varLotNo!= '')
					filters.push(new nlobjSearchFilter( 'inventorynumber', null, 'is', varLotNo ));



				// Define search columns
				var columns = new Array();
				columns[0] = new nlobjSearchColumn('item');
				columns[1] = new nlobjSearchColumn('inventorynumber');
				// Create the saved search
				var searchresults=nlapiSearchRecord( 'inventorynumber',null, filters, columns );

				for (var i in searchresults) {

					var searchResult = searchresults[i].getId(); // Gets the internal id
					var assemblyRecord = nlapiLoadRecord('inventorynumber',searchResult);

					assemblyRecord.setFieldValue('expirationdate',vExpDate);
					nlapiSubmitRecord(assemblyRecord,false);
				}
			}
		}	//end
		return id;

	} catch (e) {
		if (e instanceof nlobjError)
			nlapiLogExecution('ERROR', 'system error', e.getCode() + '\n'
					+ e.getDetails());
		else
		{
			nlapiLogExecution('ERROR', 'unexpected error', e.toString());
			Err=e.toString();
			return 'ERROR,' + e.toString();
		}
	}
} 



function GetPickStageLocation(Item, vCarrier, vSite, vCompany, stgDirection,vCarrierType,customizetype,StageBinInternalId,ordertype){

	nlapiLogExecution('Error', 'into GetPickStageLocation', Item);
	nlapiLogExecution('Error', 'Item', Item);
	nlapiLogExecution('Error', 'vSite', vSite);
	nlapiLogExecution('Error', 'vCompany', vCompany);
	nlapiLogExecution('Error', 'stgDirection', stgDirection);
	nlapiLogExecution('Error', 'vCarrier', vCarrier);
	nlapiLogExecution('Error', 'vCarrierType', vCarrierType);
	nlapiLogExecution('Error', 'ordertype', ordertype);
	nlapiLogExecution('Error', 'customizetype', customizetype);


	//var ordertype='5'; //Order Type ="WORK ORDER";
	var ItemFamily, ItemGroup, Location;
	var location_found = false;
	var LocArry = new Array();

	var fields = ['custitem_item_family', 'custitem_item_group', 'custitem_ebizdefskustatus', 'custitem_item_info_1', 'custitem_item_info_2', 'custitem_item_info_3'];
	var ItemFamily = "";
	var ItemGroup = "";
	var ItemStatus = "";
	var ItemInfo1 = "";
	var ItemInfo2 = "";
	var ItemInfo3 = "";
	var columns = nlapiLookupField('item', Item, fields);
	if(columns!=null){
		ItemFamily = columns.custitem_item_family;
		ItemGroup = columns.custitem_item_group;
		ItemStatus = columns.custitem_ebizdefskustatus;
		ItemInfo1 = columns.custitem_item_info_1;
		ItemInfo2 = columns.custitem_item_info_2;
		ItemInfo3 = columns.custitem_item_info_3;
	}

	nlapiLogExecution('Error', 'ItemFamily', ItemFamily);
	nlapiLogExecution('Error', 'ItemGroup', ItemGroup);
	nlapiLogExecution('Error', 'ItemStatus', ItemStatus);
	nlapiLogExecution('Error', 'ItemInfo1', ItemInfo1);
	nlapiLogExecution('Error', 'ItemInfo2', ItemInfo2);
	nlapiLogExecution('Error', 'ItemInfo3', ItemInfo3);

	nlapiLogExecution('Error', 'Before Variable declaration');

	var locgroupid = 0;
	var zoneid = 0;
	var direction = "";
	if (stgDirection == 'INB') {
		direction = "1";
	}
	else 
		if (stgDirection == 'OUB') {
			direction = "2";
		}
		else
		{
			direction = "3";
		}

	var columns = new Array();
	var filters = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_sequencenostgrule');
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationstgrule');
	columns[2] = new nlobjSearchColumn('custrecord_inboundlocationgroupstg');
	columns[3] = new nlobjSearchColumn('custrecord_outboundlocationgroup');
	columns[4] = new nlobjSearchColumn('custrecord_nooffootprints', 'custrecord_locationstgrule');
	columns[5] = new nlobjSearchColumn('custrecord_stagerule_ordtype');

	if (ItemFamily != null && ItemFamily != "") {
		filters.push(new nlobjSearchFilter('custrecord_skufamilystgrule', null, 'anyof', ['@NONE@', ItemFamily]));
	}
	if (ItemGroup != null && ItemGroup != "") {
		filters.push(new nlobjSearchFilter('custrecord_skugroupstg', null, 'anyof', ['@NONE@', ItemGroup]));
	}
	if (Item != null && Item != "") {
		filters.push(new nlobjSearchFilter('custrecord_skustgrule', null, 'anyof', ['@NONE@', Item]));
	}
	if (vCarrier != null && vCarrier != "") {
		filters.push(new nlobjSearchFilter('custrecord_carrierstgrule', null, 'anyof', ['@NONE@', vCarrier]));

	}
	if (vCarrierType != null && vCarrierType != "") {
		filters.push(new nlobjSearchFilter('custrecord_carriertypestgrule', null, 'anyof', ['@NONE@', vCarrierType]));

	}
	if (vSite != null && vSite != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizsitestage', null, 'anyof', ['@NONE@', vSite]));

	}
	if (vCompany != null && vCompany != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizcompanystage', null, 'anyof', ['@NONE@', vCompany]));

	}
	if (ordertype != null && ordertype != "") {
		filters.push(new nlobjSearchFilter('custrecord_stagerule_ordtype', null, 'anyof', ['@NONE@', ordertype]));

	}
	else
		filters.push(new nlobjSearchFilter('custrecord_stagerule_ordtype', null, 'anyof', ['@NONE@']));
	if (customizetype != null && customizetype != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebiz_stg_customizetype', null, 'anyof', ['@NONE@', customizetype]));

	}
	filters.push(new nlobjSearchFilter('custrecord_directionstgrule', null, 'anyof', [direction]));
	filters.push(new nlobjSearchFilter('custrecord_stg_tasktype', null, 'anyof', ['@NONE@','3']));

	/*if (StageBinInternalId != null && StageBinInternalId != "")
		//filters.push(new nlobjSearchFilter('custrecord_locationstgrule', null, 'anyof', StageBinInternalId));
	filters.push(new nlobjSearchFilter('custrecord_outboundlocgroupid','custrecord_outboundlocationgroup', 'anyof', StageBinInternalId));*/
	nlapiLogExecution('Error', 'Before Searching of Stageing Rules');

	var vstgLocation,vstgLocationText;
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters, columns);
	if (searchresults != null) {
		for (var i = 0; i < Math.min(10, searchresults.length); i++) {
			//GetPickStagLpCount                
			//fectch stage location for Ordertype = WORK ORDER		  
			var ordertype = searchresults[i].getText('custrecord_stagerule_ordtype');
			nlapiLogExecution('Error', 'ordertype::', ordertype);
			//if(ordertype == 'WORK ORDER'){
			vstgLocation = searchresults[i].getValue('custrecord_locationstgrule');
			vstgLocationText = searchresults[i].getText('custrecord_locationstgrule');
			//}

			nlapiLogExecution('Error', 'Stageing Location Value::', vstgLocation);
			nlapiLogExecution('Error', 'Stageing Location Text::', vstgLocationText);


			var vnoofFootPrints = searchresults[i].getValue('custrecord_nooffootprints');

			nlapiLogExecution('Error', 'vnoofFootPrints::', vnoofFootPrints);


			if (vstgLocation != null && vstgLocation != "") {
				var vLpCnt = GetPickStagLpCount(vstgLocation, vSite, vCompany);
				if (vnoofFootPrints != null && vnoofFootPrints != "") {
					if (parseFloat(vLpCnt) < parseFloat(vnoofFootPrints)) {
						return vstgLocation;
					}
				}
				else {
					return vstgLocation;
				}
			}


			nlapiLogExecution('Error', 'final stagelocation::', vstgLocation);

			var LocGroup = "";
			if (stgDirection == 'INB') {
				LocGroup = searchresults[i].getValue('custrecord_inboundlocationgroupstg');
			}
			else {
				LocGroup = searchresults[i].getValue('custrecord_outboundlocationgroup');
			}

			nlapiLogExecution('Error', 'final stagelocation::', vstgLocation);
			nlapiLogExecution('Error', 'LocGroup::', LocGroup);

			if (LocGroup != null && LocGroup != "") {

				var collocGroup = new Array();
				var filtersLocGroup = new Array();
				collocGroup[0] = new nlobjSearchColumn('custrecord_startingpickseqno');
				collocGroup[0].setSort();
				collocGroup[1] = new nlobjSearchColumn('name');
				collocGroup[2] = new nlobjSearchColumn('custrecord_nooffootprints');

				if (stgDirection = 'INB') {
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [LocGroup]));
				}
				else { 
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_outboundlocgroupid', null, 'anyof', [LocGroup]));
				}

				//This code is commented on 9th September 2011 by ramana
				/*
				var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
				for (var k = 0; searchresults != null && k < searchresults.length; k++) {
					var vLocid=searchresults[k].getId();
						var vLpCnt = GetPickStagLpCount(vLocid, vSite, vCompany);
						var vnoofFootPrints = searchresults[k].getValue('custrecord_nooffootprints');

						//--
						if (vnoofFootPrints != null && vnoofFootPrints != "") {
								if (vLpCnt < vnoofFootPrints) {
									return vLocid;
								}
						}
					else {
						return vLocid;
						}
				}
				 */

				var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
				nlapiLogExecution('Error', 'searchresultsloc::', searchresultsloc);
				for (var k = 0; searchresultsloc != null && k < searchresultsloc.length; k++) {
					var vLocid=searchresultsloc[k].getId();
//					var vLocid=searchresultsloc[0].getId();

					nlapiLogExecution('Error', 'vLocid::', vLocid);

					var vLpCnt = GetPickStagLpCount(vLocid, vSite, vCompany);
					var vnoofFootPrints = searchresultsloc[k].getValue('custrecord_nooffootprints');

					//--
					if (vnoofFootPrints != null && vnoofFootPrints != "") {
						if (parseFloat(vLpCnt) < parseFloat(vnoofFootPrints)) {
							return vLocid;
						}
					}
					else {
						return vLocid;
					}
				}
			}
		}
	}
	return -1;
}



function GetPickStagLpCount(location, site, company){
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_lp',null,'count');

	var outLPCount = 0;

	var filters= new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', [location]));

	if(site != null && site != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [site]));

	if(company != null && company != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_company', null, 'anyof', [company]));

	var lpCount = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
	if(lpCount != null)
		outLPCount = lpCount[0].getValue('custrecord_ebiz_inv_lp', null, 'count');

	return outLPCount;
}

function UpdateNsconfInOpenTask(woid,nsrefid){
	nlapiLogExecution('Error', 'woid', woid);
	nlapiLogExecution('Error', 'nsrefid', nsrefid);
	var filterOpentask = new Array();
	filterOpentask[0] = new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', woid);
	filterOpentask[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['8']);
	filterOpentask[2] = new nlobjSearchFilter('custrecord_notes', null, 'isnotempty');
	filterOpentask[3] = new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_line_no');
	columns[1] = new nlobjSearchColumn('custrecord_sku');
	columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[3] = new nlobjSearchColumn('custrecord_lpno');
	columns[4] = new nlobjSearchColumn('custrecord_batch_no');
	columns[5] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[6] = new nlobjSearchColumn('custrecord_act_qty');                                    
	columns[0].setSort();

	var Opentaskresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterOpentask,columns);	
	if(Opentaskresults!=null && Opentaskresults!='')
	{
		for(var i= 0; i<Opentaskresults.length; i++){
			var id = Opentaskresults[i].getId();

			nlapiLogExecution('Error', 'id', id);

			var rec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask',id);
			rec.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', nsrefid);
			nlapiSubmitRecord(rec,false, true);

		}
	}


}

//Case# 201410445 starts
function updateOpenTaskwithns(woid,id){

	var ItemStatusFilter = new Array();
	ItemStatusFilter.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', woid));
	ItemStatusFilter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', 3));
	ItemStatusFilter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', 8));
	ItemStatusFilter.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty'));

	nlapiLogExecution('ERROR', 'Into updateOpenTaskwithns');
	nlapiLogExecution('ERROR', 'woid',woid);
	nlapiLogExecution('ERROR', 'id',id);
	var column = new Array();
	column[0] = new nlobjSearchColumn('name');


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, ItemStatusFilter, column);
	if(searchresults!=null && searchresults!=''){
		for(var i=0; i<searchresults.length; i++){

			var opid = searchresults[i].getId();

			var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask',opid);

			transaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no',id);

			var id2 = nlapiSubmitRecord(transaction,false,true);

		}
	}
}
//Case# 201410445 ends
