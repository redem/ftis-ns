/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************
 *
 *  $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Suitelet/Attic/ebiz_Inbound_Reversal_SL.js,v $
 *  $Revision: 1.1.2.1.4.9.2.2 $
 *  $Date: 2015/11/02 23:30:18 $
 *  $Author: sponnaganti $
 *  $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 * DESCRIPTION
 *  Functionality
 *
 * REVISION HISTORY
 *  $Log: ebiz_Inbound_Reversal_SL.js,v $
 *  Revision 1.1.2.1.4.9.2.2  2015/11/02 23:30:18  sponnaganti
 *  case# 201415242
 *  Issue is while pushing values to array we are taking values from
 *  open/closed task. But here for closed task actual qty column is not added to columns.
 * 
 *  Revision 1.1.2.1.4.9.2.1  2015/10/19 16:14:15  deepshikha
 *  2015.2 issue fixes
 *  201414962
 * 
 *  Revision 1.1.2.1.4.9  2015/02/27 15:45:45  sponnaganti
 *  Case# 201411709
 *  True Fab Sb Issue fix
 * 
 *  Revision 1.1.2.1.4.8  2014/11/27 13:58:47  sponnaganti
 *  case# 201411104
 *  CT Prod issue fix
 * 
 *  Revision 1.1.2.1.4.7  2014/08/05 15:25:37  sponnaganti
 *  Case# 20149809
 *  Stnd Bundle Issue fix
 * 
 *  Revision 1.1.2.1.4.6  2014/07/22 16:00:15  sponnaganti
 *  Case# 20149600
 *  Compatibility Issue fix
 * 
 *  Revision 1.1.2.1.4.5  2014/07/17 15:22:58  sponnaganti
 *  Case# 20149518
 *  Stnd Bundle Issue fix
 * 
 *  Revision 1.1.2.1.4.4  2014/06/20 15:25:38  sponnaganti
 *  case# 20148346
 *  Stnd Bundle issue fix
 * 
 *  Revision 1.1.2.1.4.3  2014/04/16 15:08:51  skreddy
 *  case # 20147986
 *  MHP sb  issue fix
 * 
 *  Revision 1.1.2.1.4.2  2014/04/09 06:04:50  skreddy
 *  case # 20140023
 *  LL sb  issue fix
 * 
 *  Revision 1.1.2.1.4.1  2014/03/06 15:29:36  sponnaganti
 *  case# 20127538
 *  (PO Status PwtQty issue)
 * 
 *  Revision 1.1.2.1.8.1  2014/02/04 16:15:09  rrpulicherla
 *  CASE201112/CR201113/LOG201121
 * 
 *  Inbound reversal
 * 
 *  Revision 1.1.2.1.4.1  2014/01/06 07:26:04  schepuri
 *  20126620
 * 
 *  Revision 1.1.2.1  2013/05/16 11:25:32  spendyala
 *  CASE201112/CR201113/LOG201121
 *  New scripts for inbound reversal taken from TSG
 * 
 *  Revision 1.1.2.4.4.3.4.2  2013/04/04 15:07:34  rmukkera 
 *  CASE201112/CR201113/LOG2012392
 *  Production and UAT issue fixes. 
 *
 ****************************************************************************/

function inboundReversal(request, response){
	if(request.getMethod() == 'GET'){
		nlapiLogExecution('ERROR', 'inboundReversal - GET', 'Start');
		var form = nlapiCreateForm('Inbound Reversal');
		form.setScript('customscript_ebiz_inboundreversal_cl');
		var hiddenQueryParams=form.addField('custpage_qeryparams', 'text', 'queryparams').setDisplayType('hidden');
		var hiddenField_selectpage = form.addField('custpage_hiddenfieldselectpage', 'text', '').setDisplayType('hidden');
		hiddenField_selectpage.setDefaultValue('F');

		createOrderSublist(form, request, response);

		var taskList = getTasksForReversal(request,form);

		if(taskList != null && taskList.length > 0){
			setPagingForSublist(taskList,form);
		}

		form.addSubmitButton('Submit');

		response.writePage(form);

		nlapiLogExecution('ERROR', 'outboundReversal - GET', 'End');
	} else if (request.getMethod() == 'POST'){

		nlapiLogExecution('ERROR', 'inboundReversal - POST', 'Start');
		try
		{
			var form = nlapiCreateForm('Inbound Reversal');
			//form.setScript('customscript_ebiz_inboundreversal_cl');
			var hiddenQueryParams=form.addField('custpage_qeryparams', 'text', 'queryparams').setDisplayType('hidden');
			var hiddenField_selectpage = form.addField('custpage_hiddenfieldselectpage', 'text', '').setDisplayType('hidden');
			hiddenField_selectpage.setDefaultValue('F');
			nlapiLogExecution('ERROR','custpage_hiddenfieldselectpage',request.getParameter('custpage_hiddenfieldselectpage'));

			if(request.getParameter('custpage_qeryparams')!=null)
			{
				hiddenQueryParams.setDefaultValue(request.getParameter('custpage_qeryparams'));
			}

			if(request.getParameter('custpage_hiddenfieldselectpage')=='F')
			{

				// Retrieve the number of items for which wave needs to be created
				var lineCount = request.getLineItemCount('custpage_items');
				var contlparray = new Array();
				var vSuccessMsg='N';
				for(var k = 1; k <= lineCount; k++){
					var selectValue = request.getLineItemValue('custpage_items', 'custpage_so', k);
					if(selectValue=='T')
					{
						var lpno=request.getLineItemValue('custpage_items', 'custpage_lpno', k);
						var ordid=request.getLineItemValue('custpage_items', 'custpage_soinernno', k);

						var taskid=request.getLineItemValue('custpage_items', 'custpage_taskid', k);
						var taskqty=request.getLineItemValue('custpage_items', 'custpage_qty', k);
						var ordlineno=request.getLineItemValue('custpage_items', 'custpage_lineno', k);
						var taskactloc=request.getLineItemValue('custpage_items', 'custpage_actloc', k);
						var itemid=request.getLineItemValue('custpage_items', 'custpage_itemintrid', k);
						var tasktype=request.getLineItemValue('custpage_items', 'custpage_tasktype', k);
						var trantype=request.getLineItemValue('custpage_items', 'custpage_trantype', k);
						var status=request.getLineItemValue('custpage_items', 'custpage_statusvalue', k);
						var nsrefno=request.getLineItemValue('custpage_items', 'custpage_nsrefno', k);
						//contlparray.push(contlp);


						performTaskReversal(lpno,ordid,ordlineno,taskid,taskqty,'',itemid,tasktype,taskactloc,status,trantype,nsrefno);

						vSuccessMsg='T';

						//ClearOutBoundInventory(contlp,taskqty,taskweight,itemid);
						/*if(itemid!=null && itemid!='' && itemid!='null')
						{
							var itemTypesku = nlapiLookupField('item', itemid, 'recordType');
							nlapiLogExecution('ERROR', 'itemTypesku', itemTypesku);
							if (itemTypesku == "serializedinventoryitem")
							{
								updateSerialEntry(contlp,itemid,taskqty,ordlineno,ordid,fromlp);
							}
						}*/


					}
				}
				if(vSuccessMsg=='T')
				{
					//var vDistinctContainerLp = removeDuplicateElement(contlparray);				
					//nlapiLogExecution('ERROR', 'vDistinctContainerLp ', vDistinctContainerLp);	
					//RemovePackTask(vDistinctContainerLp);
					showInlineMessage(form, 'Confirmation', 'Inbound Reversal Completed Successfully ');
				}
				//case# 20149809 starts (For displaying alert message when we are not selecting atleast one checkbox in sublist)
				else
				{
					showInlineMessage(form, 'ERROR', 'Please select atleast one line ');
				}
				//case# 20149809 ends
			}
			else
			{
				createOrderSublist(form, request, response);

				var taskList = getTasksForReversal(request,form);

				if(taskList != null && taskList.length > 0){
					setPagingForSublist(taskList,form);
				}

				form.addSubmitButton('Submit');
			}
		}
		catch(exp) {
			nlapiLogExecution('ERROR', 'Exception in Inbound Reversal ', exp);	
			showInlineMessage(form, 'Error', 'Inbound Reversal Failed', "");
			response.writePage(form);
		}
		response.writePage(form);
	}

}

function performTaskReversal(lpno,ordid,ordlineno,taskid,taskqty,taskuomlevel,itemid,tasktype,taskactloc,wmsstatus,trantype,nsrefno)
{

	var str = str + 'ordid. = ' + ordid + '<br>';	

	str = str + 'ordlineno. = ' + ordlineno + '<br>';
	str = str + 'taskid. = ' + taskid + '<br>';
	str = str + 'taskqty. = ' + taskqty + '<br>';	 
	str = str + 'itemid. = ' + itemid + '<br>';
	str = str + 'trantype. = ' + trantype + '<br>';
	str = str + 'nsrefno. = ' + nsrefno + '<br>';
	nlapiLogExecution('ERROR', 'performTaskReversal Parameters', str);
	if(wmsstatus == '3')
		updateItemReceipt(ordid,ordlineno,taskqty,itemid,nsrefno); //22
	updateIBOpentaskandInventory(lpno,taskqty,taskid,taskqty,taskuomlevel,itemid,ordid,taskactloc,ordlineno,tasktype,wmsstatus,trantype);


}
function updateSerialEntry(contlp,itemid,taskqty,ordlineno,ordid,fromlp,trantype)
{
	var temptaskqty=taskqty;
	var filters = new Array();
	nlapiLogExecution('ERROR', 'trantype', trantype);
	filters.push(new nlobjSearchFilter('custrecord_serialparentid', null, 'is', contlp));
	filters.push(new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', itemid));
	if(trantype=='purchaseorder')
	{	
	filters.push(new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', ordid));
	filters.push(new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', ordlineno));
	}
	else
	{	
		filters.push(new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', ordid));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', ordlineno));
		}
	
	nlapiLogExecution('ERROR', 'contlp', contlp);
	nlapiLogExecution('ERROR', 'itemid', itemid);
	nlapiLogExecution('ERROR', 'ordid', ordid);
	nlapiLogExecution('ERROR', 'ordlineno', ordlineno);
	filters.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['1','3']));
	//var columns = new Array();
	//columns[0] = new nlobjSearchColumn('name');
	//var serialEntryResults = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
	
	var serialEntryResults = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, null);	
	if(serialEntryResults!=null && serialEntryResults!='' && serialEntryResults!='null')
	{
		nlapiLogExecution('ERROR', 'serialEntryResults.length', serialEntryResults.length);
		for(var k1=0;k1<serialEntryResults.length;k1++)
		{
			var  count=parseInt(k1)+1;
			if(parseInt(count)<=parseInt(temptaskqty))
			{
				nlapiDeleteRecord('customrecord_ebiznetserialentry',serialEntryResults[k1].getId());
				//Need to think about deletion of serial entry of transfer order serial numbers
			}
		}
	}
	
	
	// case# 
	var tempfilters = new Array();
	nlapiLogExecution('ERROR', 'trantype', trantype);
	tempfilters.push(new nlobjSearchFilter('custrecord_tempserialparentid', null, 'is', contlp));
	tempfilters.push(new nlobjSearchFilter('custrecord_tempserialitem', null, 'anyof', itemid));
	/*if(trantype=='purchaseorder')
	{*/	
		tempfilters.push(new nlobjSearchFilter('custrecord_tempserialorderno', null, 'is', ordid));
		tempfilters.push(new nlobjSearchFilter('custrecord_tempserialpolineno', null, 'is', ordlineno));
	/*}
	else
	{	
		tempfilters.push(new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', ordid));
		tempfilters.push(new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', ordlineno));
		}*/
	
	nlapiLogExecution('ERROR', 'contlp tempfilters', contlp);
	nlapiLogExecution('ERROR', 'itemid tempfilters', itemid);
	nlapiLogExecution('ERROR', 'ordid tempfilters', ordid);
	nlapiLogExecution('ERROR', 'ordlineno tempfilters', ordlineno);
	//tempfilters.push(new nlobjSearchFilter('custrecord_tempserialstatus', null, 'anyof', ['1','3']));
	
	var tempserialEntryResults = nlapiSearchRecord('customrecord_ebiznettempserialentry', null, tempfilters, null);	
	if(tempserialEntryResults!=null && tempserialEntryResults!='' && tempserialEntryResults!='null')
	{
		nlapiLogExecution('ERROR', 'tempserialEntryResults.length', tempserialEntryResults.length);
		for(var k1=0;k1<tempserialEntryResults.length;k1++)
		{
			var  count=parseInt(k1)+1;
			if(parseInt(count)<=parseInt(temptaskqty))
			{
				nlapiDeleteRecord('customrecord_ebiznettempserialentry',tempserialEntryResults[k1].getId());
				//Need to think about deletion of serial entry of transfer order serial numbers
			}
		}
	}
	

}
/*function RemovePackTask(vDistinctContainerLp)
{
	nlapiLogExecution('ERROR', 'Into RemovePackTask', vDistinctContainerLp);

	try
	{

		for ( var vcount = 0; vDistinctContainerLp!=null && vcount < vDistinctContainerLp.length; vcount++) 
		{
			nlapiLogExecution('ERROR', 'Container LP', vDistinctContainerLp[vcount]);

			if(vDistinctContainerLp[vcount]!= "" && vDistinctContainerLp[vcount]!= null)
			{
				var filters = new Array();
				filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vDistinctContainerLp[vcount]));

				//26-Picks Failed, 29-Closed, 30-Short Pick
				filters.push(new nlobjSearchFilter( 'custrecord_wms_status_flag', null, 'noneof', [26,29,30]));

				//3 - PICK
				filters.push(new nlobjSearchFilter( 'custrecord_tasktype', null, 'anyof', [3]));

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
				columns[0].setSort();

				var opentasks = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	

				nlapiLogExecution('ERROR', 'opentasks', opentasks);
				if(opentasks==null || opentasks=='')
				{				
					var packfilters = new Array();

					packfilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vDistinctContainerLp[vcount]));

					//28-Pack Complete
					packfilters.push(new nlobjSearchFilter( 'custrecord_wms_status_flag', null, 'anyof', [28]));

					//14 - PACK
					packfilters.push(new nlobjSearchFilter( 'custrecord_tasktype', null, 'anyof', [14]));

					var packcolumns = new Array();
					packcolumns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
					packcolumns[0].setSort();

					var packtasks = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, packfilters, packcolumns);	
					nlapiLogExecution('ERROR', 'packtasks', packtasks);
					if(packtasks!=null && packtasks!='' && packtasks.length>0)
					{
						for(var j=0;j < packtasks.length; j++)
						{
							nlapiLogExecution('ERROR', 'Deleting Pack Task...');
							nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', packtasks[j].getId());
						}
					}

					// Deleting ShipManifest Record
					var smfilters = new Array();

					smfilters.push(new nlobjSearchFilter('custrecord_ship_ref5', null, 'is', vDistinctContainerLp[vcount]));

					var smcolumns = new Array();
					smcolumns[0] = new nlobjSearchColumn('custrecord_ship_ref5');
					smcolumns[0].setSort();

					var shipmanifesttasks = nlapiSearchRecord('customrecord_ship_manifest', null, smfilters, smcolumns);	
					nlapiLogExecution('ERROR', 'shipmanifesttasks', shipmanifesttasks);
					if(shipmanifesttasks!=null && shipmanifesttasks!='' && shipmanifesttasks.length>0)
					{
						for(var k=0;k < shipmanifesttasks.length; k++)
						{
							nlapiLogExecution('ERROR', 'Deleting Ship Manifest Record...');
							nlapiDeleteRecord('customrecord_ship_manifest', shipmanifesttasks[k].getId());
						}
					}
				}
			}
		}
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR', 'Exception in RemovePackTask', exp);
	}

	nlapiLogExecution('ERROR', 'Out of RemovePackTask', vDistinctContainerLp);
}*/

function ClearOutBoundInventory(contlp,taskqty,taskweight,itemid)
{
	nlapiLogExecution('ERROR', 'Into ClearOutBoundInventory');

	var str = 'contlp. = ' + contlp + '<br>';
	str = str + 'taskqty. = ' + taskqty + '<br>';	
	str = str + 'taskweight. = ' + taskweight + '<br>';	
	str = str + 'itemid. = ' + itemid + '<br>';	

	nlapiLogExecution('ERROR', 'ClearOutBoundInventory Parameters', str);

	try
	{
		if(contlp!=null && contlp!='')
		{
			var filters = new Array();
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', contlp));
			//26-Outbound
			filters.push(new nlobjSearchFilter( 'custrecord_wms_inv_status_flag', null, 'anyof', [18]));
			filters.push(new nlobjSearchFilter( 'custrecord_ebiz_inv_sku', null, 'anyof', itemid));

			var columns=new Array();
			columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');

			var inventorysearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv',null,filters,columns);
			if(inventorysearchresults!=null && inventorysearchresults!='' && inventorysearchresults.length>0)
			{
				for(var k=0;k < inventorysearchresults.length; k++)
				{
					var obqoh = inventorysearchresults[k].getValue('custrecord_ebiz_qoh');
					if(isNaN(obqoh))
						obqoh=0;

					var newqty = parseFloat(obqoh)-parseFloat(taskqty);

					if(isNaN(newqty))
						newqty=0;

					nlapiLogExecution('ERROR', 'newqty', newqty);

					if(newqty<=0)
						nlapiDeleteRecord('customrecord_ebiznet_createinv', inventorysearchresults[k].getId());
					else
					{
						nlapiSubmitField('customrecord_ebiznet_createinv', inventorysearchresults[k].getId(), 'custrecord_ebiz_qoh', parseFloat(newqty));
					}
				}
			}

			//Shipmanifest Update
			var smfilters = new Array();

			smfilters.push(new nlobjSearchFilter('custrecord_ship_ref5', null, 'is', contlp));

			var smcolumns = new Array();
			smcolumns[0] = new nlobjSearchColumn('custrecord_ship_pkgwght');
			smcolumns[0].setSort();

			var shipmanifesttasks = nlapiSearchRecord('customrecord_ship_manifest', null, smfilters, smcolumns);	
			if(shipmanifesttasks!=null && shipmanifesttasks!='' && shipmanifesttasks.length>0)
			{
				for(var l=0;l < shipmanifesttasks.length; l++)
				{
					var actweight = shipmanifesttasks[l].getValue('custrecord_ship_pkgwght');

					if(isNaN(actweight))
						actweight=0;

					var newweight = parseFloat(actweight)-parseFloat(taskweight);

					nlapiLogExecution('ERROR', 'newweight', newweight);

					if(parseFloat(newweight)>0)
						nlapiSubmitField('customrecord_ship_manifest', shipmanifesttasks[l].getId(), 'custrecord_ship_pkgwght', parseFloat(newweight));
				}				
			}
		}
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR', 'Exception in ClearOutBoundInventory', exp);
	}

	nlapiLogExecution('ERROR', 'Out of of ClearOutBoundInventory');
}

function createOrderSublist(form, request, response){
	var sublist = form.addSubList("custpage_items", "list", "ItemList");
	sublist.addMarkAllButtons();
	sublist.addField("custpage_so", "checkbox", "Confirm").setDefaultValue('F');
	sublist.addField("custpage_lpno", "text", "LP #");
	sublist.addField("custpage_sono", "text", "Order #");
	sublist.addField("custpage_lineno", "text", "Line #");
	sublist.addField("custpage_item", "text", "Item");
	sublist.addField("custpage_qty", "text", "Qty");
	sublist.addField("custpage_itemstatus", "text", "Item Status");
	sublist.addField("custpage_packcode", "text", "Packcode");
	sublist.addField("custpage_lotbatch", "text", "Lot #");
	sublist.addField("custpage_binloc", "text", "Bin Location");	  
	sublist.addField("custpage_serailno", "text", "Serial #");
	sublist.addField("custpage_status", "text", "Status");
	sublist.addField("custpage_soinernno", "text", "SONo").setDisplayType('hidden');	
	sublist.addField("custpage_location", "text", "location").setDisplayType('hidden');
	sublist.addField("custpage_company", "text", "Company").setDisplayType('hidden');
	sublist.addField("custpage_itemintrid", "text", "ITEM ID").setDisplayType('hidden');	 
	sublist.addField("custpage_taskid", "text", "Task Id").setDisplayType('hidden');
	sublist.addField("custpage_tasktype", "text", "Task Type").setDisplayType('hidden');
	sublist.addField("custpage_actloc", "text", "ActualLoc").setDisplayType('hidden');
	sublist.addField("custpage_statusvalue", "text", "Status").setDisplayType('hidden');
	sublist.addField("custpage_trantype", "text", "Tran Type");
	sublist.addField("custpage_nsrefno", "text", "Ns Ref").setDisplayType('hidden');
	/*sublist.addField("custpage_uomlevel", "text", "UOM Level").setDisplayType('hidden');
	sublist.addField("custpage_taskweight", "text", "TASK WEIGHT").setDisplayType('hidden');
	sublist.addField("custpage_taskcube", "text", "TASK CUBE").setDisplayType('hidden');

	sublist.addField("custpage_fromlp", "text", "ITEM ID").setDisplayType('hidden');*/
}

function addTaskListToSublist(form, currentTask, i,vTranType){

	form.getSubList('custpage_items').setLineItemValue('custpage_lpno', i + 1,
			currentTask[8]);
	form.getSubList('custpage_items').setLineItemValue('custpage_sono', i + 1,
			currentTask[15]);
	form.getSubList('custpage_items').setLineItemValue('custpage_lineno', i + 1,
			currentTask[1]);
	form.getSubList('custpage_items').setLineItemValue('custpage_item', i + 1,
			currentTask[16]);
	if(currentTask[23]!=currentTask[3])
		{
		nlapiLogExecution('Error', 'currentTask[23]',currentTask[23]);

		form.getSubList('custpage_items').setLineItemValue('custpage_qty', i + 1, 
				currentTask[23]);
		}
	else
		{
		form.getSubList('custpage_items').setLineItemValue('custpage_qty', i + 1, 
				currentTask[3]);
		}
	if(currentTask[9]=='2')
		{
		form.getSubList('custpage_items').setLineItemValue('custpage_qty', i + 1, 
				currentTask[3]);
		}
		
			// case no 20126616
	//form.getSubList('custpage_items').setLineItemValue('custpage_itemstatus', i + 1, 
	//		currentTask[5]);
	form.getSubList('custpage_items').setLineItemValue('custpage_itemstatus', i + 1, 
			currentTask[17]);//item status text
	form.getSubList('custpage_items').setLineItemValue('custpage_packcode', i + 1, 
			currentTask[6]);
	form.getSubList('custpage_items').setLineItemValue('custpage_lotbatch', i + 1, 
			currentTask[4]);
	if(currentTask[9]=='2'){


		if(currentTask[18] == null || currentTask[18] == '' || currentTask[18] == 'null')
		{
			form.getSubList('custpage_items').setLineItemValue('custpage_binloc', i + 1, 
					currentTask[24]);
		}
		else
		{
			form.getSubList('custpage_items').setLineItemValue('custpage_binloc', i + 1, 
					currentTask[18]);
		}

	}
	else{


		form.getSubList('custpage_items').setLineItemValue('custpage_binloc', i + 1, 
				currentTask[18]);
	}

	form.getSubList('custpage_items').setLineItemValue('custpage_status', i + 1, 
			currentTask[19]);
	form.getSubList('custpage_items').setLineItemValue('custpage_statusvalue', i + 1, 
			currentTask[9]);
	
	form.getSubList('custpage_items').setLineItemValue('custpage_soinernno', i + 1, 
			currentTask[0]);
	form.getSubList('custpage_items').setLineItemValue('custpage_location', i + 1, 
			currentTask[11]);
	//form.getSubList('custpage_items').setLineItemValue('custpage_company', i + 1,currentTask[0]);
	form.getSubList('custpage_items').setLineItemValue('custpage_itemintrid', i + 1, 
			currentTask[2]);
	form.getSubList('custpage_items').setLineItemValue('custpage_serailno', i + 1, 
			currentTask[12]);
	form.getSubList('custpage_items').setLineItemValue('custpage_taskid', i + 1, 
			currentTask[20]);
	form.getSubList('custpage_items').setLineItemValue('custpage_tasktype', i + 1, 
			currentTask[21]);
	form.getSubList('custpage_items').setLineItemValue('custpage_actloc', i + 1, 
			currentTask[7]);
	form.getSubList('custpage_items').setLineItemValue('custpage_trantype', i + 1, 
			vTranType);
	nlapiLogExecution('Error', 'currentTask[21]', currentTask[21]);
	//form.getSubList('custpage_items').setLineItemValue('custpage_taskid', i + 1,currentTask.getId());
	nlapiLogExecution('Error', 'currentTask[22]', currentTask[22]);
	form.getSubList('custpage_items').setLineItemValue('custpage_nsrefno', i + 1, 
			currentTask[22]);
	/*temparr.getValue('custrecord_ebiztask_ebiz_order_no')0
	,temparr.getValue('custrecord_ebiztask_line_no'),1
	temparr.getValue('custrecord_ebiztask_sku'),2
	temparr.getValue('custrecord_ebiztask_expe_qty'),3
	temparr.getValue('custrecord_ebiztask_batch_no'),4
	temparr.getValue('custrecord_ebiztask_sku_status'),5
	temparr.getValue('custrecord_ebiztask_packcode'),6
	temparr.getValue('custrecord_ebiztask_actendloc'),7
	temparr.getValue('custrecord_ebiztask_lpno'),8
	temparr.getValue('custrecord_ebiztask_wms_status_flag'),9
	temparr.getValue('name'),10
	temparr.getValue('custrecord_ebiztask_site_id'),11
	temparr.getValue('custrecord_ebiztask_serial_no'),12
	temparr.getValue('custrecord_ebiztask_from_lp_no'),13
	temparr.getValue('custrecord_ebiztask_tasktype'),14
	temparr.getText('custrecord_ebiztask_ebiz_order_no'),15
	temparr.getText('custrecord_ebiztask_sku'),16
	temparr.getText('custrecord_ebiztask_sku_status'),17
	temparr.getText('custrecord_ebiztask_actendloc'),18
	temparr.getText('custrecord_ebiztask_wms_status_flag')19];*/
	/*
	form.getSubList('custpage_items').setLineItemValue('custpage_uomlevel', i + 1, 
			currentTask.getValue('custrecord_uom_level'));
	form.getSubList('custpage_items').setLineItemValue('custpage_taskweight', i + 1, 
			currentTask.getValue('custrecord_total_weight'));
	form.getSubList('custpage_items').setLineItemValue('custpage_taskcube', i + 1, 
			currentTask.getValue('custrecord_totalcube'));

	form.getSubList('custpage_items').setLineItemValue('custpage_fromlp', i + 1, 
			currentTask.getValue('custrecord_from_lp_no'));
	var vnsrefno = currentTask.getValue('custrecord_ebiz_nsconfirm_ref_no');

	nlapiLogExecution('Error', 'vnsrefno', vnsrefno);*/
}

var searchResultArray=new Array();

function getTasksForReversal(request,form){

	// Validating all the request parameters and pushing to a local array
	var localVarArray;
	//this is to maintain the querystring parameters while refreshing the paging drop down
	if (request.getMethod() == 'POST') {
		var queryparams=request.getParameter('custpage_qeryparams');
		nlapiLogExecution('Error', 'queryparams', queryparams);
		var tempArray=new Array();
		tempArray.push(queryparams.split(','));
		localVarArray=tempArray;
		nlapiLogExecution('Error', 'tempArray', tempArray.length);
	}
	else
	{
		localVarArray = validateRequestParams(request,form);
		nlapiLogExecution('Error', 'localVarArray', localVarArray);
	}

	// Get all the search filters from open task
	var filters1 = new Array();   
	filters1 = specifyTaskFiltersOpen(localVarArray);

	// Get all the columns that the search should return
	var columns1 = new Array();
	columns1 = getTaskColumnsOpen();

	var opentaskList = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters1, columns1);

	searchResultArray.push(opentaskList);

	// Get all the search filters from open task
	var filters2 = new Array();   
	filters2 = specifyTaskFiltersClose(localVarArray);

	// Get all the columns that the search should return
	var columns2 = new Array();
	columns2 = getTaskColumnsClose();

	var ClosedtaskList = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filters2, columns2);

	searchResultArray.push(ClosedtaskList);
	searchResultArray.push(localVarArray[0][1]);
	return searchResultArray;
}

function validateRequestParams(request,form){
	var ordNo = "";
	var trantype = "";
	var lpNO = "";

	var localVarArray = new Array();

	if (request.getParameter('custpage_orderlist') != null && request.getParameter('custpage_orderlist') != "") {
		ordNo = request.getParameter('custpage_orderlist');
	}


	if (request.getParameter('custpage_trantype') != null && request.getParameter('custpage_trantype') != "") {
		trantype = request.getParameter('custpage_trantype');
	}

	if (request.getParameter('custpage_lp') != null && request.getParameter('custpage_lp') != "") {
		lpNO = request.getParameter('custpage_lp');
	}

	var currentRow = [ordNo,trantype,lpNO];
	localVarArray.push(currentRow);
	if (request.getParameter('custpage_qeryparams')==null )
	{
		var hiddenQueryParams=form.getField('custpage_qeryparams');
		hiddenQueryParams.setDefaultValue(localVarArray.toString());
	}
	return localVarArray;
}

function specifyTaskFiltersOpen(localVarArray){
	var filters = new Array();

	// Sales Order No
	if(localVarArray[0][0] != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', localVarArray[0][0]));

	if(localVarArray[0][2] != "")
		filters.push(new nlobjSearchFilter('custrecord_lpno', null, 'is', localVarArray[0][2]));

	//2-Locations assigned, 3-Putaway completed
	filters.push(new nlobjSearchFilter( 'custrecord_wms_status_flag', null, 'anyof', [2,3]));
	//filters.push(new nlobjSearchFilter( 'custrecord_wms_status_flag', null, 'anyof', [3]));
	//1 - Checkin, 2- putaway
	//filters.push(new nlobjSearchFilter( 'custrecord_tasktype', null, 'anyof', [1,2]));
	filters.push(new nlobjSearchFilter( 'custrecord_tasktype', null, 'anyof', [2]));
	return filters;
}

/**
 * 
 * @returns {Array}
 */
function getTaskColumnsOpen(){
	var columns = new Array();

	columns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
	columns.push(new nlobjSearchColumn('custrecord_line_no'));
	columns.push(new nlobjSearchColumn('custrecord_sku'));
	columns.push(new nlobjSearchColumn('custrecord_expe_qty'));
	columns.push(new nlobjSearchColumn('custrecord_act_qty'));

	columns.push(new nlobjSearchColumn('custrecord_batch_no'));
	columns.push(new nlobjSearchColumn('custrecord_sku_status'));
	columns.push(new nlobjSearchColumn('custrecord_packcode'));
	columns.push(new nlobjSearchColumn('custrecord_actendloc'));
	columns.push(new nlobjSearchColumn('custrecord_lpno'));	 	
	columns.push(new nlobjSearchColumn('custrecord_wms_status_flag'));	 	
	columns.push(new nlobjSearchColumn('name'));		 
	columns.push(new nlobjSearchColumn('custrecord_site_id'));	 
	columns.push(new nlobjSearchColumn('custrecord_serial_no'));
	columns.push(new nlobjSearchColumn('custrecord_from_lp_no'));
	columns.push(new nlobjSearchColumn('custrecord_tasktype'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_nsconfirm_ref_no'));
	columns.push(new nlobjSearchColumn('custrecord_actbeginloc'));


	return columns;
}

function specifyTaskFiltersClose(localVarArray){
	var filters = new Array();

	// Sales Order No
	if(localVarArray[0][0] != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no', null, 'is', localVarArray[0][0]));

	if(localVarArray[0][2] != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiztask_lpno', null, 'is', localVarArray[0][2]));

	//2-Locations assigned, 3-Putaway completed
	filters.push(new nlobjSearchFilter( 'custrecord_ebiztask_wms_status_flag', null, 'anyof', [2,3]));
	//filters.push(new nlobjSearchFilter( 'custrecord_ebiztask_wms_status_flag', null, 'anyof', [3]));

	//1 - Checkin, 2- putaway
	//filters.push(new nlobjSearchFilter( 'custrecord_ebiztask_tasktype', null, 'anyof', [1,2]));
	filters.push(new nlobjSearchFilter( 'custrecord_ebiztask_tasktype', null, 'anyof', [1,2]));
	nlapiLogExecution('ERROR','specifyTaskFiltersClose');
	return filters;
}

/**
 * 
 * @returns {Array}
 */
function getTaskColumnsClose(){
	var columns = new Array();

	columns.push(new nlobjSearchColumn('custrecord_ebiztask_ebiz_order_no'));
	columns.push(new nlobjSearchColumn('custrecord_ebiztask_line_no'));
	columns.push(new nlobjSearchColumn('custrecord_ebiztask_sku'));
	columns.push(new nlobjSearchColumn('custrecord_ebiztask_expe_qty'));
	columns.push(new nlobjSearchColumn('custrecord_ebiztask_batch_no'));
	columns.push(new nlobjSearchColumn('custrecord_ebiztask_sku_status'));
	columns.push(new nlobjSearchColumn('custrecord_ebiztask_packcode'));
	columns.push(new nlobjSearchColumn('custrecord_ebiztask_actendloc'));
	columns.push(new nlobjSearchColumn('custrecord_ebiztask_lpno'));		 	
	columns.push(new nlobjSearchColumn('custrecord_ebiztask_wms_status_flag'));	 	
	columns.push(new nlobjSearchColumn('name'));	 
	columns.push(new nlobjSearchColumn('custrecord_ebiztask_site_id'));	  
	columns.push(new nlobjSearchColumn('custrecord_ebiztask_serial_no'));
	columns.push(new nlobjSearchColumn('custrecord_ebiztask_from_lp_no'));
	columns.push(new nlobjSearchColumn('custrecord_ebiztask_tasktype'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_nsconf_refno_ebiztask'));
	columns.push(new nlobjSearchColumn('custrecord_ebiztask_act_qty'));

	return columns;
}

function setPagingForSublist(taskList,form)
{
	if(taskList != null && taskList.length > 0){

		nlapiLogExecution('Error', 'taskList length ', taskList.length);
		var taskListArray=new Array();		
var vTranType=taskList[2];

		for(k=0;k<2;k++)
		{
			var tasksearchresult = taskList[k];

			if(tasksearchresult!=null)
			{		
				if(k==0)
				{	
					for(var j=0;j<tasksearchresult.length;j++)
					{	
						var actqty=tasksearchresult[j].getValue('custrecord_expe_qty');

						if(actqty==null || actqty=='' || isNaN(actqty))
							actqty=0;

						if(parseFloat(actqty)>0)
						{	
							var temparr=tasksearchresult[j];
							taskListArray[taskListArray.length]=[temparr.getValue('custrecord_ebiz_order_no'),temparr.getValue('custrecord_line_no'),temparr.getValue('custrecord_sku'),temparr.getValue('custrecord_expe_qty'),temparr.getValue('custrecord_batch_no'),temparr.getValue('custrecord_sku_status'),temparr.getValue('custrecord_packcode'),temparr.getValue('custrecord_actendloc'),temparr.getValue('custrecord_lpno'),temparr.getValue('custrecord_wms_status_flag'),temparr.getValue('name'),temparr.getValue('custrecord_site_id'),temparr.getValue('custrecord_serial_no'),temparr.getValue('custrecord_from_lp_no'),temparr.getValue('custrecord_tasktype'),temparr.getText('custrecord_ebiz_order_no'),temparr.getText('custrecord_sku'),temparr.getText('custrecord_sku_status'),temparr.getText('custrecord_actendloc'),temparr.getText('custrecord_wms_status_flag'),temparr.getId(),'Open',temparr.getValue('custrecord_ebiz_nsconfirm_ref_no'),temparr.getValue('custrecord_act_qty'),temparr.getText('custrecord_actbeginloc'),temparr.getValue('custrecord_actbeginloc')];

						}
					}
				}
				if(k==1)
				{	
					for(var j=0;j<tasksearchresult.length;j++)
					{	
						var actqty=tasksearchresult[j].getValue('custrecord_ebiztask_expe_qty');

						if(actqty==null || actqty=='' || isNaN(actqty))
							actqty=0;

						if(parseFloat(actqty)>0)
						{	
							var temparr=tasksearchresult[j];
							taskListArray[taskListArray.length]=[temparr.getValue('custrecord_ebiztask_ebiz_order_no'),temparr.getValue('custrecord_ebiztask_line_no'),temparr.getValue('custrecord_ebiztask_sku'),temparr.getValue('custrecord_ebiztask_expe_qty'),temparr.getValue('custrecord_ebiztask_batch_no'),temparr.getValue('custrecord_ebiztask_sku_status'),temparr.getValue('custrecord_ebiztask_packcode'),temparr.getValue('custrecord_ebiztask_actendloc'),temparr.getValue('custrecord_ebiztask_lpno'),temparr.getValue('custrecord_ebiztask_wms_status_flag'),temparr.getValue('name'),temparr.getValue('custrecord_ebiztask_site_id'),temparr.getValue('custrecord_ebiztask_serial_no'),temparr.getValue('custrecord_ebiztask_from_lp_no'),temparr.getValue('custrecord_ebiztask_tasktype'),temparr.getText('custrecord_ebiztask_ebiz_order_no'),temparr.getText('custrecord_ebiztask_sku'),temparr.getText('custrecord_ebiztask_sku_status'),temparr.getText('custrecord_ebiztask_actendloc'),temparr.getText('custrecord_ebiztask_wms_status_flag'),temparr.getId(),'Closed',temparr.getValue('custrecord_ebiz_nsconf_refno_ebiztask'),temparr.getValue('custrecord_ebiztask_act_qty')];							 
						}
					}
				}
			}
		}

		var test='';

		if(taskListArray.length>0)
		{
			if(taskListArray.length>50)
			{
				var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('hidden');
				pagesize.setDisplaySize(10,10);
				pagesize.setLayoutType('outsidebelow', 'startrow');
				var select= form.addField('custpage_selectpage','select', 'Select Records');	
				select.setLayoutType('outsidebelow', 'startrow');			
				select.setDisplaySize(200,30);
				if (request.getMethod() == 'GET'){
					pagesize.setDefaultValue("50");
					pagesizevalue=50;
				}
				else
				{
					if(request.getParameter('custpage_pagesize')!=null)
					{
						pagesizevalue= request.getParameter('custpage_pagesize');
					}
					else
					{
						pagesizevalue= 50;
						pagesize.setDefaultValue("50");
					}
				}

				//this is to add the pageno's to the dropdown.
				var len=taskListArray.length/parseFloat(pagesizevalue);
				for(var k=1;k<=Math.ceil(len);k++)
				{
					var from;var to;

					to=parseFloat(k)*parseFloat(pagesizevalue);
					from=(parseFloat(to)-parseFloat(pagesizevalue))+1;

					if(parseFloat(to)>taskListArray.length)
					{
						to=taskListArray.length;
						test=from.toString()+","+to.toString(); 
					}

					var temp=from.toString()+" to "+to.toString();
					var tempto=from.toString()+","+to.toString();
					select.addSelectOption(tempto,temp);

				} 

				if (request.getMethod() == 'POST'){

					if(request.getParameter('custpage_selectpage')!=null ){

						select.setDefaultValue(request.getParameter('custpage_selectpage'));	

					}
					if(request.getParameter('custpage_pagesize')!=null ){

						pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	

					}
				}
			}
			else
			{
				pagesizevalue=parseFloat(taskListArray.length);
			}

			var minval=0;var maxval=parseFloat(pagesizevalue);
			if(parseFloat(pagesizevalue)>taskListArray.length)
			{
				maxval=orderListArray.length;
			}

			var selectno=request.getParameter('custpage_selectpage');
			if(selectno!=null )
			{
				var selectedPage= request.getParameter('custpage_selectpage');
				var selectedPageArray=selectedPage.split(',');			
				var diff=parseFloat(selectedPageArray[1])-(parseFloat(selectedPageArray[0])-1);
				nlapiLogExecution('ERROR', 'diff',diff);

				var pagevalue=request.getParameter('custpage_pagesize');
				nlapiLogExecution('ERROR', 'pagevalue',pagevalue);
				if(pagevalue!=null)
				{
					if(parseFloat(diff)==parseFloat(pagevalue)|| test==selectno)
					{

						var selectedPageArray=selectno.split(',');	
						//nlapiLogExecution('ERROR', 'selectedPageArray.length ', selectedPageArray.length);  
						minval=parseFloat(selectedPageArray[0])-1;
						//nlapiLogExecution('ERROR', 'selectedPageArray[0] ', selectedPageArray[0]);  
						maxval=parseFloat(selectedPageArray[1]);
						//nlapiLogExecution('ERROR', 'selectedPageArray[1] ', selectedPageArray[1]);  
					}
				}
			}


			var c=0;
			var minvalue;
			minvalue=minval;

			for(var j = minvalue; j < maxval; j++){		

				var currentTask = taskListArray[j];
				addTaskListToSublist(form, currentTask, c,vTranType);
				c=c+1;
			}
		}
	}
}

function updateItemReceipt(ordid,ordlineno,taskqty,itemid,nsrefno)
{
	nlapiLogExecution('ERROR', 'Into updateItemReceipt');
	nlapiLogExecution('ERROR', 'ordid',ordid);
	nlapiLogExecution('ERROR', 'ordlineno',ordlineno);
	nlapiLogExecution('ERROR', 'taskqty',taskqty);
	nlapiLogExecution('ERROR', 'itemid',itemid);
	nlapiLogExecution('ERROR', 'nsrefno',nsrefno);
	if(ordid!=null && ordid!='')
	{
		try
		{
			var trantype = nlapiLookupField('transaction', ordid, 'recordType');
			nlapiLogExecution('ERROR','trantype',trantype);
			
			var itemIndex=0;
			var totalqty=taskqty;
			var vLotQty=0;
			var boolfound=true;

			var filters = new Array();
			filters.push(new nlobjSearchFilter('createdfrom', null, 'is', ordid));
			filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));

			filters.push(new nlobjSearchFilter('item', null, 'anyof', itemid));

			var columns = new Array();    
			columns.push(new nlobjSearchColumn('quantity')); 
			columns.push(new nlobjSearchColumn('line'));
			columns.push(new nlobjSearchColumn('location'));

			var itemReceiptResults = nlapiSearchRecord('itemreceipt', null, filters, columns);
			if(itemReceiptResults != null && itemReceiptResults !='')
			{	
				/*var nsrefno;
				for(var z=0;z<itemReceiptResults.length;z++)
				{
					//if(itemReceiptResults[z].getValue('line')==ordlineno)
						nsrefno=itemReceiptResults[0].getId();
				}*/	

				var TransformRec = nlapiLoadRecord('itemreceipt', nsrefno);

				// To get the no of lines from item fulfillment record
				var IFLength = TransformRec.getLineItemCount('item');  

				for (var j = 1; IFLength!=null && j <= IFLength; j++) {

					var itemLineNo = TransformRec.getLineItemValue('item', 'orderline', j);

					if(trantype=="transferorder")
						itemLineNo=parseInt(itemLineNo)-2;
					
					if (itemLineNo == ordlineno) {
						itemIndex=j;    				
					}

				}

				nlapiLogExecution('ERROR', "itemIndex", itemIndex);

				if(itemIndex!=0)
				{
					var item_id = TransformRec.getLineItemValue('item', 'item', itemIndex);
					var itemname = TransformRec.getLineItemText('item', 'item', itemIndex);
					if(itemname!=null && itemname!='')
						itemname=itemname.replace(/ /g,"-");
					var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', itemIndex);
					var itemloc2 = TransformRec.getLineItemValue('item', 'location', itemIndex);
					var NSOrdUOM = TransformRec.getLineItemValue('item', 'units', itemIndex);


					nlapiLogExecution('ERROR', 'item_id', item_id);
					nlapiLogExecution('ERROR', 'itemname', itemname);
					nlapiLogExecution('ERROR', 'NSOrdUOM', NSOrdUOM);

					var vItemDimsArr = geteBizItemDimensionsArray(item_id);

					if(NSOrdUOM!=null && NSOrdUOM!="")
					{

						var veBizUOMQty=0;
						var vBaseUOMQty=0;

						if(vItemDimsArr!=null && vItemDimsArr.length>0)
						{
							for(z=0; z < vItemDimsArr.length; z++)
							{
								if(vItemDimsArr[z].getValue('custrecord_ebizitemdims')==item_id)
								{	
									if(vItemDimsArr[z].getValue('custrecord_ebizbaseuom') == 'T')
									{
										vBaseUOMQty = vItemDimsArr[z].getValue('custrecord_ebizqty');
										nlapiLogExecution('ERROR', 'vBaseUOMQty', vBaseUOMQty);
									}
									nlapiLogExecution('ERROR', 'Dim UOM', vItemDimsArr[z].getValue('custrecord_ebiznsuom'));
									nlapiLogExecution('ERROR', 'NS UOM', NSOrdUOM);
									if(NSOrdUOM == vItemDimsArr[z].getValue('custrecord_ebiznsuom'))
									{
										nlapiLogExecution('ERROR', 'NS UOM', vItemDimsArr[z].getText('custrecord_ebiznsuom'));
										veBizUOMQty = vItemDimsArr[z].getValue('custrecord_ebizqty');	
										nlapiLogExecution('ERROR', 'veBizUOMQty', veBizUOMQty);
									}
								}
							}	
							if(veBizUOMQty==null || veBizUOMQty=='')
							{
								veBizUOMQty=vBaseUOMQty;
							}
							if(veBizUOMQty!=0)
								totalqty = Math.floor((parseFloat(totalqty)*parseFloat(vBaseUOMQty))/parseFloat(veBizUOMQty));
							nlapiLogExecution('ERROR', 'totalqty', totalqty);

							vLotQty =totalqty;
						}
					}

					if(boolfound)
					{
						if(totalqty>0)
						{
							TransformRec.selectLineItem('item', itemIndex);
							var oldputqty = TransformRec.getCurrentLineItemValue('item', 'quantity');
							var newputqty = parseFloat(oldputqty)-parseFloat(totalqty);
							if(parseFloat(newputqty)>0)
							{
								TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'T');
								TransformRec.setCurrentLineItemValue('item', 'quantity', newputqty);					
								TransformRec.setCurrentLineItemValue('item', 'location', itemloc2);
								TransformRec.commitLineItem('item');
							}
							else
							{
								nlapiLogExecution('ERROR', 'putaway Qty is Zero', newputqty);
								nlapiLogExecution('ERROR', 'IFLength', IFLength);

								if(IFLength==1)
								{
									nlapiDeleteRecord('itemreceipt', nsrefno);
									return;
								}
								else
									TransformRec.setLineItemValue('item','itemreceive',itemIndex,'F');

							}
						}
						else
						{
							nlapiLogExecution('ERROR', 'Picked Qty is Zero', totalqty);
							nlapiLogExecution('ERROR', 'IFLength', IFLength);

							if(IFLength==1)
							{
								nlapiDeleteRecord('itemreceipt', nsrefno);
								return;
							}
							else
								TransformRec.setLineItemValue('item','itemreceive',itemIndex,'F');
						}
					}
				}

				nlapiLogExecution('ERROR', 'Before Submit', '');

				if(TransformRec!=null)
				{
					var TransformRecId = nlapiSubmitRecord(TransformRec, true);
					nlapiLogExecution('ERROR', 'After Submit', '');
				}
			}
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR', 'Exception in updateItemReceipt',exp);
		}
	}

	nlapiLogExecution('ERROR', 'Out of updateItemReceipt');
}

function updateIBOpentaskandInventory(lpno,reversalqty,taskid,taskactqty,taskuomlevel,itemid,ordintrid,taskactloc,lineno,tasktype,wmsstatus,trantype)
{
	nlapiLogExecution('ERROR', 'Into updateIBOpentaskandInventory');


	var str = 'reversalqty. = ' + reversalqty + '<br>';	
	str = str + 'lpno. = ' + lpno + '<br>';	
	str = str + 'taskactqty. = ' + taskactqty + '<br>';	
	str = str + 'opentaskid. = ' + taskid + '<br>';
	str = str + 'tasktype. = ' + tasktype + '<br>';	 
	str = str + 'taskuomlevel. = ' + taskuomlevel + '<br>';
	str = str + 'itemid. = ' + itemid + '<br>';
	//str = str + 'lprecid. = ' + lprecid + '<br>';
	str = str + 'taskactloc. = ' + taskactloc + '<br>';
	str = str + 'ordintrid. = ' + ordintrid + '<br>';
	str = str + 'lineno. = ' + lineno + '<br>';
	str = str + 'wmsstatus. = ' + wmsstatus + '<br>';
	nlapiLogExecution('ERROR', 'Function Parameters', str);
	var lpnoNew=lpno;
	
	var InvtREFId=updateOpenTask(taskid,reversalqty,taskactqty,tasktype,lpnoNew,lineno,itemid,ordintrid);
	updateTransactionOrdLine(reversalqty,itemid,ordintrid,lineno,wmsstatus);
	
	if(wmsstatus=='3')
	{
		var filters = new Array();
		if(taskactloc != null && taskactloc != '')
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', taskactloc));
		filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', 19));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', itemid));

		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', lpno));
		var columns = new Array();    
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
		columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
		columns[3] = new nlobjSearchColumn('custrecord_ebiz_avl_qty');
		var invResults1 = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
		var vInvRefNo;
		
		if(invResults1 !=  null && invResults1 != '')
		{
			vInvRefNo=invResults1[0].getId();
			nlapiLogExecution('ERROR', 'Into Update Inventory mainLP',vInvRefNo);
			var inventoryTransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', vInvRefNo);
			if(inventoryTransaction!=null && inventoryTransaction!='' && inventoryTransaction!='-1')
			{


				var qoh = inventoryTransaction.getFieldValue('custrecord_ebiz_qoh');

				if (isNaN(qoh))
				{
					qoh = 0;
				}

				var totQOH=parseFloat(qoh)-parseFloat(reversalqty);

				inventoryTransaction.setFieldValue('custrecord_ebiz_qoh', parseFloat(totQOH));
				inventoryTransaction.setFieldValue('custrecord_ebiz_callinv', 'N');
				inventoryTransaction.setFieldValue('custrecord_ebiz_displayfield', 'N');
				inventoryTransaction.setFieldValue('custrecord_ebiz_inv_note1', 'Updated by Inbound Reversal Process');  

				nlapiSubmitRecord(inventoryTransaction,false,true);
				nlapiLogExecution('ERROR', 'totQOH', totQOH);
				
				try
				{
					if((parseFloat(totQOH)) <= 0)
					{		
						nlapiLogExecution('ERROR', 'Deleting record from inventory if QOH becomes zero', vInvRefNo);
						var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', vInvRefNo);				
					}
				}
				catch(e)
				{
					nlapiLogExecution('ERROR', 'exception in Delete allocation',e);
				}

				nlapiLogExecution('ERROR', 'Out of Update Inventory');
			}
		}
		else
		{
			if(InvtREFId!=null&&InvtREFId!=""&&InvtREFId!="null")
			{
				nlapiLogExecution('ERROR', 'Into Update Inventory upon InvtREFId',InvtREFId);
				var inventoryTransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', InvtREFId);
				if(inventoryTransaction!=null && inventoryTransaction!='' && inventoryTransaction!='-1')
				{


					var qoh = inventoryTransaction.getFieldValue('custrecord_ebiz_qoh');

					if (isNaN(qoh))
					{
						qoh = 0;
					}

					var totQOH=parseFloat(qoh)-parseFloat(reversalqty);

					inventoryTransaction.setFieldValue('custrecord_ebiz_qoh', parseFloat(totQOH));
					inventoryTransaction.setFieldValue('custrecord_ebiz_callinv', 'N');
					inventoryTransaction.setFieldValue('custrecord_ebiz_displayfield', 'N');
					inventoryTransaction.setFieldValue('custrecord_ebiz_inv_note1', 'Updated by Inbound Reversal Process');  

					nlapiSubmitRecord(inventoryTransaction,false,true);
					try
					{
						if((parseFloat(totQOH)) <= 0)
						{		
							nlapiLogExecution('ERROR', 'Deleting record from inventory if QOH becomes zero', InvtREFId);
							var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', InvtREFId);				
						}
					}
					catch(e1)
					{
						nlapiLogExecution('ERROR', 'exception in Delete allocation',e1);
					}

					nlapiLogExecution('ERROR', 'Out of Update Inventory');
				}
			}
			else{
				nlapiLogExecution('ERROR', 'Into Update Inventory 2');
				var filters = new Array();
				if(taskactloc != null && taskactloc != '')
					filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', taskactloc));
				filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', 19));
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', itemid));

				//filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'anyof', lpno));
				var columns = new Array();    
				columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');
				columns[1] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
				columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
				columns[3] = new nlobjSearchColumn('custrecord_ebiz_avl_qty');
				var invResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
				if(invResults !=  null && invResults != '')
				{

					for(var s=0;s<invResults.length;s++)
					{
						if(invResults[s].getValue('custrecord_ebiz_avl_qty')!= null && invResults[s].getValue('custrecord_ebiz_avl_qty') != '' && parseInt(invResults[s].getValue('custrecord_ebiz_avl_qty'))>taskactqty)
						{
							vInvRefNo=invResults1[s].getId();
							var inventoryTransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
							if(inventoryTransaction!=null && inventoryTransaction!='' && inventoryTransaction!='-1')
							{
								nlapiLogExecution('ERROR', 'Into Update Inventory',vInvRefNo);

								var qoh = inventoryTransaction.getFieldValue('custrecord_ebiz_qoh');
								lpnoNew=inventoryTransaction.getFieldValue('custrecord_ebiz_inv_lp');
								if (isNaN(qoh))
								{
									qoh = 0;
								}

								var totQOH=parseFloat(qoh)-parseFloat(reversalqty);

								inventoryTransaction.setFieldValue('custrecord_ebiz_qoh', parseFloat(totQOH));
								inventoryTransaction.setFieldValue('custrecord_ebiz_callinv', 'N');
								inventoryTransaction.setFieldValue('custrecord_ebiz_displayfield', 'N');
								inventoryTransaction.setFieldValue('custrecord_ebiz_inv_note1', 'Updated by Inbound Reversal Process');  

								nlapiSubmitRecord(inventoryTransaction,false,true);
								try
								{
									if((parseFloat(totQOH)) <= 0)
									{		
										nlapiLogExecution('ERROR', 'Deleting record from inventory if QOH becomes zero', invrefno);
										var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
									}
								}
								catch(e2)
								{
									nlapiLogExecution('ERROR', 'exception in Delete allocation',e2);
								}

								nlapiLogExecution('ERROR', 'Out of Update Inventory');
								break;
							} 
						}
					}	
				}
			}
		}
	}
	else
	{
		//Need to delete inbound inventory
		DeleteInvtRecCreatedforCHKNTask(ordintrid, lpno);
		//case# 20148346 starts
		var fields = ['custrecord_actbeginloc'];
		var columns= nlapiLookupField('customrecord_ebiznet_trn_opentask',taskid,fields);
		var actbinloc = columns.custrecord_actbeginloc;
		nlapiLogExecution('ERROR', 'actbinloc', actbinloc);
		var vqty=parseFloat(reversalqty);
		var RemCube = GeteLocCube(actbinloc);
		nlapiLogExecution('ERROR', 'RemCube', RemCube);
		var itemDimensions=getSKUCubeAndWeight(itemid,1);
		var itemCube=itemDimensions[0];
		nlapiLogExecution('ERROR', 'itemCube', itemCube);
		var TotalItemCube = parseFloat(itemCube) * parseFloat(vqty);
		nlapiLogExecution('ERROR', 'TotalItemCube', TotalItemCube);
		var remainingCube = parseFloat(RemCube) + parseFloat(TotalItemCube);
		nlapiLogExecution('ERROR', 'remainingCube', remainingCube);
		UpdateLocCube(actbinloc,remainingCube);
		//case# 20148346 ends
		
	}	
	if(lpnoNew!= null && lpnoNew != '')
	{
		if(itemid!=null && itemid!='' && itemid!='null')
		{
			var fields = ['recordType', 'custitem_ebizserialin'];
			var columns = nlapiLookupField('item', itemid, fields);
			var ItemType = columns.recordType;
			var serialInflg = columns.custitem_ebizserialin;


			nlapiLogExecution('ERROR', 'ItemType', ItemType);
			nlapiLogExecution('ERROR', 'serialInflg', serialInflg);
			if (ItemType == "serializedinventoryitem" || serialInflg == "T")
			{
				updateSerialEntry(lpnoNew,itemid,reversalqty,lineno,ordintrid,'',trantype);

			}
		}

		//UpdateSerialEntry(lpnoNew,itemid,ordintrid);

	}	 

	nlapiLogExecution('ERROR', 'Out of updateOpentaskandInventory');
}
function updateTransactionOrdLine(reversalqty,itemid,ordintrid,lineno,wmsstatus)
{
	nlapiLogExecution('ERROR', 'Into updateTransaction Ord line');

	var str = 'reversalqty. = ' + reversalqty + '<br>';
	str = str + 'itemid. = ' + itemid + '<br>';
	str = str + 'ordintrid. = ' + ordintrid + '<br>';	
	str = str + 'lineno. = ' + lineno + '<br>';	

	nlapiLogExecution('ERROR', 'Function Parameters', str);

	//var taskremqty=parseFloat(taskactqty)-parseFloat(reversalqty);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', ordintrid));
	filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', lineno));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty');
	columns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_putgen_qty');
	columns[2] = new nlobjSearchColumn('custrecord_orderlinedetails_putconf_qty');


	var searchResults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, filters, columns);
	if(searchResults != null && searchResults != '')
	{
		var chkQty=searchResults[0].getValue('custrecord_orderlinedetails_checkin_qty');
		var PwtQty=searchResults[0].getValue('custrecord_orderlinedetails_putgen_qty');
		var PwtConfQty=searchResults[0].getValue('custrecord_orderlinedetails_putconf_qty');

		RecId=searchResults[0].getId();
		if(chkQty == null || chkQty == '')
			chkQty=0;
		if(PwtQty == null || PwtQty == '')
			PwtQty=0;
		if(PwtConfQty == null || PwtConfQty == '')
			PwtConfQty=0;
		nlapiLogExecution('ERROR','chkQty',chkQty);
		nlapiLogExecution('ERROR','PwtQty',PwtQty);
		nlapiLogExecution('ERROR','PwtConfQty',PwtConfQty);
		chkQty=parseInt(chkQty)-parseInt(reversalqty);
		//case# 20127538 starts (PO Status PwtQty issue)
		PwtQty=parseInt(PwtQty)-parseInt(reversalqty);
		if(wmsstatus=='3')
		{
		PwtConfQty=parseInt(PwtConfQty)-parseInt(reversalqty);
		}
		//case# 20127538 end
		nlapiLogExecution('ERROR','chkQty AFTER',chkQty);
		nlapiLogExecution('ERROR','PwtQty AFTER',PwtQty);
		nlapiLogExecution('ERROR','PwtConfQty AFTER',PwtConfQty);
		//case# 20148660 starts(exception qty not updating)
		if(PwtConfQty==null || PwtConfQty=='')
			PwtConfQty=0;
		var excqty=parseInt(PwtQty)-parseInt(PwtConfQty);
		nlapiLogExecution('ERROR','excqty ',excqty);
		var Fields=new Array();
		Fields[0]='custrecord_orderlinedetails_checkin_qty';
		Fields[1]='custrecord_orderlinedetails_putgen_qty';
		Fields[2]='custrecord_orderlinedetails_putconf_qty';
		Fields[3]='custrecord_orderlinedetails_putexcep_qty';
		
		var Values=new Array();
		Values[0]=chkQty;
		Values[1]=PwtQty;
		Values[2]=PwtConfQty;
		Values[3]=excqty;

		var ID=nlapiSubmitField('customrecord_ebiznet_order_line_details', RecId, Fields, Values);	
	}	




	nlapiLogExecution('ERROR', 'Out of update transaction ord line details');
}

function updateOpenTask(taskid,reversalqty,taskactqty,tasktype,lpnoNew,lineno,itemid,ordintrid)
{
	nlapiLogExecution('ERROR', 'Into updateOpenTask');

	var str = 'taskid. = ' + taskid + '<br>';
	str = str + 'reversalqty. = ' + reversalqty + '<br>';
	str = str + 'taskactqty. = ' + taskactqty + '<br>';
	str = str + 'tasktype. = ' + tasktype + '<br>';
	str = str + 'lpnoNew. = ' + lpnoNew + '<br>';
	str = str + 'lineno. = ' + lineno + '<br>';
	str = str + 'itemid. = ' + itemid + '<br>';
	str = str + 'ordintrid. = ' + ordintrid + '<br>';

	var InvtRefID="";
	var taskremqty=parseFloat(taskactqty)-parseFloat(reversalqty);

	str = str + 'taskremqty. = ' + taskremqty + '<br>';

	nlapiLogExecution('ERROR', 'Function Parameters', str);
	var cartlpArray=new Array();
	if(tasktype=='Open')
	{
		var filters=new Array();
		if(ordintrid != null && ordintrid != '')
			filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ordintrid));

		if(lpnoNew != null && lpnoNew !="")
			filters.push(new nlobjSearchFilter('custrecord_lpno', null, 'is', lpnoNew));

		//2-Locations assigned, 3-Putaway completed
		filters.push(new nlobjSearchFilter( 'custrecord_wms_status_flag', null, 'anyof', [1,2,3]));
		//filters.push(new nlobjSearchFilter( 'custrecord_wms_status_flag', null, 'anyof', [3]));
		//1 - Checkin, 2- putaway
		filters.push(new nlobjSearchFilter( 'custrecord_tasktype', null, 'anyof', [1,2]));
		//filters.push(new nlobjSearchFilter( 'custrecord_tasktype', null, 'anyof', [2]));
		
		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_invref_no');
		column[1]=new nlobjSearchColumn('custrecord_tasktype');
		column[2]=new nlobjSearchColumn('custrecord_transport_lp');
		column[1].setSort();
		var opentaskList = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, column);
		if(opentaskList != null && opentaskList != '')
		{
			for(var i=0;i<opentaskList.length;i++)
			{
				var fields = new Array();
				var values = new Array();

				fields.push('custrecord_notes');
				fields.push('custrecord_reversalqty');
				fields.push('custrecord_expe_qty');


				values.push('Updated by Putaway Reversal process');
				values.push(parseFloat(reversalqty));
				values.push(parseFloat(taskremqty));


				if(parseFloat(taskremqty)<1)
				{
					fields.push('custrecord_wms_status_flag');
					values.push('32');
					//case# 20149518 starts(updating end date for doing checkin(not putaway) and reversal)
					fields.push('custrecord_act_end_date');
					values.push(DateStamp());
					//case# 20149518 ends
					var cartlp=opentaskList[i].getValue('custrecord_transport_lp');
					cartlpArray.push(cartlp);
					nlapiLogExecution('ERROR', 'cartlp ',cartlp);
					nlapiLogExecution('ERROR', 'cartlpArray ',cartlpArray);
				}
				nlapiLogExecution('ERROR', 'open task id ',opentaskList[i].getId());
				nlapiSubmitField('customrecord_ebiznet_trn_opentask', opentaskList[i].getId(), fields, values);
				if(parseInt(i)== parseInt(opentaskList.length-1))
				{
					InvtRefID=opentaskList[i].getValue("custrecord_invref_no");
					//return InvtRefID;
				}
			}
		}
	}
	else
	{
		/*var filters=new Array();
		if(ordintrid != null && ordintrid != '')
			filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ordintrid));

		if(lpnoNew != null && lpnoNew !="")
			filters.push(new nlobjSearchFilter('custrecord_lpno', null, 'is', lpnoNew));

		//2-Locations assigned, 3-Putaway completed
		filters.push(new nlobjSearchFilter( 'custrecord_wms_status_flag', null, 'anyof', [1,2,3]));
		//filters.push(new nlobjSearchFilter( 'custrecord_wms_status_flag', null, 'anyof', [3]));
		//1 - Checkin, 2- putaway
		filters.push(new nlobjSearchFilter( 'custrecord_tasktype', null, 'anyof', [1,2]));
		//filters.push(new nlobjSearchFilter( 'custrecord_tasktype', null, 'anyof', [2]));

		
		var opentaskList = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, column);
		if(opentaskList != null && opentaskList != '')
		{
			for(var i=0;i<opentaskList.length;i++)
			{
				var fields = new Array();
				var values = new Array();

				fields.push('custrecord_notes');
				fields.push('custrecord_reversalqty');
				fields.push('custrecord_expe_qty');


				values.push('Updated by Putaway Reversal process');
				values.push(parseFloat(reversalqty));
				values.push(parseFloat(taskremqty));


				if(parseFloat(taskremqty)<1)
				{
					fields.push('custrecord_wms_status_flag');
					values.push('32');

				}
				nlapiLogExecution('ERROR', 'open task id ',opentaskList[i].getId());
				nlapiSubmitField('customrecord_ebiznet_trn_opentask', opentaskList[i].getId(), fields, values);
				
			}
		}*/
		var filters=new Array();
		if(ordintrid != null && ordintrid != '')
			filters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no', null, 'is', ordintrid));

		if(lpnoNew != null && lpnoNew != "")
			filters.push(new nlobjSearchFilter('custrecord_ebiztask_lpno', null, 'is', lpnoNew));

		//2-Locations assigned, 3-Putaway completed
		filters.push(new nlobjSearchFilter( 'custrecord_ebiztask_wms_status_flag', null, 'anyof', [1,2,3]));
		//filters.push(new nlobjSearchFilter( 'custrecord_ebiztask_wms_status_flag', null, 'anyof', [3]));

		//1 - Checkin, 2- putaway
		filters.push(new nlobjSearchFilter( 'custrecord_ebiztask_tasktype', null, 'anyof', [1,2]));
		//filters.push(new nlobjSearchFilter( 'custrecord_ebiztask_tasktype', null, 'anyof', [2]));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebiztask_invref_no');
		column[1]=new nlobjSearchColumn('custrecord_ebiztask_tasktype');
		column[2]=new nlobjSearchColumn('custrecord_ebiztask_transport_lp');
		column[1].setSort();
		
		var closedtaskList = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filters, column);
		if(closedtaskList != null && closedtaskList != '')
		{
			for(var i=0;i<closedtaskList.length;i++)
			{
				var fields = new Array();
				var values = new Array();

				fields.push('custrecord_ebiztask_notes');
				//fields.push('custrecord_reversalqty');
				fields.push('custrecord_ebiztask_expe_qty');


				values.push('Updated by Putaway Reversal process');
				//values.push(parseFloat(reversalqty));
				values.push(parseFloat(taskremqty));


				if(parseFloat(taskremqty)<1)
				{
					fields.push('custrecord_ebiztask_wms_status_flag');
					values.push('32');
					var cartlp=closedtaskList[i].getValue('custrecord_ebiztask_transport_lp');
					cartlpArray.push(cartlp);
					nlapiLogExecution('ERROR', 'cartlp ',cartlp);
					nlapiLogExecution('ERROR', 'cartlpArray ',cartlpArray);
				}
				nlapiLogExecution('ERROR', 'Closed task id ',closedtaskList[i].getId());
				nlapiSubmitField('customrecord_ebiznet_trn_ebiztask', closedtaskList[i].getId(), fields, values);
				if(parseInt(i)== parseInt(closedtaskList.length-1))
				{
					nlapiLogExecution('ERROR', 'i value',i);
					InvtRefID=closedtaskList[i].getValue("custrecord_ebiztask_invref_no");
					//return InvtRefID;
				}
			}
		} 
	}
	var newcartlpArray=new Array();
	newcartlpArray=removeDuplicateElement(cartlpArray);
	nlapiLogExecution('ERROR', 'newcartlpArray',newcartlpArray);
	if(newcartlpArray!=null && newcartlpArray!="")
	{
		for(var z=0;z<newcartlpArray.length;z++)
		{
			nlapiLogExecution('ERROR', 'newcartlpArray[z]',newcartlpArray[z]);
			var filtersmlp = new Array(); 
			filtersmlp[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', newcartlpArray[z]);

			var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

			if (SrchRecord != null && SrchRecord.length > 0) {
				var rec=nlapiLoadRecord('customrecord_ebiznet_master_lp',SrchRecord[0].getId());
				rec.setFieldValue('custrecord_ebiz_cart_closeflag', 'F');
				var recid = nlapiSubmitRecord(rec, false, true);
			}
		}
	}
	nlapiLogExecution('ERROR', 'Out of updateOpenTask');
	return InvtRefID;
}
function geteBizItemDimensionsArray(itemidArray)
{
	var searchRec = new Array();
	if(itemidArray!=null && itemidArray!='' && itemidArray.length>0)
	{
		var filter = new Array();
		filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', itemidArray));
		filter.push(new nlobjSearchFilter('custrecord_ebiznsuom', null, 'noneof', '@NONE@'));
		filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	

		var column = new Array();
		column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim') ;
		column[1] = new nlobjSearchColumn('custrecord_ebizqty') ;
		column[2] = new nlobjSearchColumn('custrecord_ebizbaseuom') ;
		column[3] = new nlobjSearchColumn('custrecord_ebiznsuom') ;
		column[4] = new nlobjSearchColumn('custrecord_ebizitemdims') ;
		searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);
	}

	return searchRec;

}
function DeleteInvtRecCreatedforCHKNTask(pointid, lp)
{
	nlapiLogExecution('ERROR','Inside DeleteInvtRecCreatedforCHKNTask lp ',lp);
	var Ifilters = new Array();
	Ifilters[0] = new nlobjSearchFilter('name', null, 'is', pointid);
	Ifilters[1] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [17]);
	Ifilters[2] = new nlobjSearchFilter('custrecord_invttasktype', null, 'anyof', [1]);
	Ifilters[3] = new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', lp);

	var invtId="";
	var invtType="";
	var serchInvtRec= nlapiSearchRecord('customrecord_ebiznet_createinv', null, Ifilters, null);
	if (serchInvtRec) 
	{
		for (var s = 0; s < serchInvtRec.length; s++) {
			nlapiLogExecution('ERROR','Inside loop','loop');
			var searchresult = serchInvtRec[ s ];
			nlapiLogExecution('ERROR','need to print this','print');
			invtId = serchInvtRec[s].getId();
			invtType= serchInvtRec[s].getRecordType();
			nlapiLogExecution('ERROR','CHKN INVT Id',invtId);
			nlapiLogExecution('ERROR','CHKN invtType ',invtType);
			nlapiDeleteRecord(serchInvtRec[s].getRecordType(),serchInvtRec[s].getId());
			nlapiLogExecution('ERROR','Invt Deleted record Id',invtId);

		}
	}
}