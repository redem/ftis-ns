/***************************************************************************
 eBizNET Solutions                
 ****************************************************************************/
/* Prologue - 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_InventoryMove_Complete.js,v $
 *     	   $Revision: 1.6.2.14.4.10.2.37.2.1 $
 *     	   $Date: 2015/11/27 22:12:10 $
 *     	   $Author: skreddy $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_InventoryMove_Complete.js,v $
 * Revision 1.6.2.14.4.10.2.37.2.1  2015/11/27 22:12:10  skreddy
 * case# 201415860
 * KRM prod issue fix
 *
 * Revision 1.6.2.14.4.10.2.37  2015/07/28 16:31:50  grao
 * 2015.2   issue fixes  201413725
 *
 * Revision 1.6.2.14.4.10.2.36  2015/04/28 15:59:52  nneelam
 * case# 201412525
 *
 * Revision 1.6.2.14.4.10.2.35  2015/04/13 09:25:58  rrpulicherla
 * Case#201412277
 *
 * Revision 1.6.2.14.4.10.2.34  2015/01/06 06:35:56  schepuri
 * issue#   201411332
 *
 * Revision 1.6.2.14.4.10.2.33  2014/12/26 14:00:13  schepuri
 * issue# 201411318
 *
 * Revision 1.6.2.14.4.10.2.32  2014/11/10 15:24:27  schepuri
 * 201411003  issue fix
 *
 * Revision 1.6.2.14.4.10.2.31  2014/10/29 15:37:16  skavuri
 * Case# 201410719 std byndle issue fixed
 *
 * Revision 1.6.2.14.4.10.2.30  2014/09/15 15:58:10  skavuri
 * Case# 201410342 Std Bundle issue fixed
 *
 * Revision 1.6.2.14.4.10.2.29  2014/09/01 16:23:51  gkalla
 * case#201220816
 * ACE Inv move issue
 *
 * Revision 1.6.2.14.4.10.2.28  2014/08/08 14:54:43  skreddy
 * case # 20149881
 * True Fabrications SB issue fix
 *
 * Revision 1.6.2.14.4.10.2.27  2014/07/08 15:41:11  sponnaganti
 * case# 20148325
 * Compatibility issue fix
 *
 * Revision 1.6.2.14.4.10.2.26  2014/06/19 08:06:21  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148948
 *
 * Revision 1.6.2.14.4.10.2.25  2014/06/18 14:54:08  rmukkera
 * Case # 20145096
 *
 * Revision 1.6.2.14.4.10.2.24  2014/06/13 09:59:51  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.6.2.14.4.10.2.23  2014/05/30 00:34:21  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.6.2.14.4.10.2.22  2014/04/09 16:13:31  gkalla
 * case#201218724
 * Ryonet Fixed LP issue fix
 *
 * Revision 1.6.2.14.4.10.2.21  2014/04/01 16:03:59  skreddy
 * case 20127701
 * standard bundle  issue fix
 *
 * Revision 1.6.2.14.4.10.2.20  2014/03/14 14:25:21  sponnaganti
 * case# 20127701
 * (create inventory issue)
 *
 * Revision 1.6.2.14.4.10.2.19  2014/03/04 13:58:55  gkalla
 * case#20127482
 * MHP Inventory is not merging while inventory move
 *
 * Revision 1.6.2.14.4.10.2.18  2014/02/18 09:27:48  snimmakayala
 * Case# : 20127160
 * Inventory Move Merge LP issue for MHP
 *
 * Revision 1.6.2.14.4.10.2.17  2014/01/06 13:16:03  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.6.2.14.4.10.2.16  2013/12/27 13:52:32  schepuri
 * 20126516
 *
 * Revision 1.6.2.14.4.10.2.15  2013/11/18 13:50:06  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Surftech Issue fixes
 *
 * Revision 1.6.2.14.4.10.2.14  2013/10/25 20:02:30  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 20125294
 *
 * Revision 1.6.2.14.4.10.2.13  2013/10/22 15:50:46  gkalla
 * Case# 20125141
 * Dynacraft negative allocation issue fix
 *
 * Revision 1.6.2.14.4.10.2.12  2013/10/22 15:05:13  skreddy
 * Case# 20124748
 * standard bundle issue fix
 *
 * Revision 1.6.2.14.4.10.2.11  2013/07/09 15:45:09  rrpulicherla
 * Case# 20123344
 * Inventory Move issues
 *
 * Revision 1.6.2.14.4.10.2.10  2013/07/02 15:49:10  rmukkera
 * Issue Fix For
 *
 * Issue: 20123262 RF invt move : System throwing script error after LP screen
 *
 * Revision 1.6.2.14.4.10.2.9  2013/06/28 16:25:22  skreddy
 * Case# 20123228
 * Monobind SB issue fix
 *
 * Revision 1.6.2.14.4.10.2.8  2013/06/26 16:35:02  gkalla
 * Case# 20123185
 * Standard bundle Issue Fix
 *
 * Revision 1.6.2.14.4.10.2.7  2013/06/19 22:52:44  gkalla
 * CASE201112/CR201113/LOG201121
 * As part of optimistic locking
 *
 * Revision 1.6.2.14.4.10.2.6  2013/06/05 22:11:22  spendyala
 * CASE201112/CR201113/LOG201121
 * FIFO field updating with data stamp is removed.
 *
 * Revision 1.6.2.14.4.10.2.5  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.6.2.14.4.10.2.4  2013/03/26 13:27:42  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.6.2.14.4.10.2.3  2013/03/13 13:57:18  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Time Stamp related changes.
 *
 * Revision 1.6.2.14.4.10.2.2  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.6.2.14.4.10.2.1  2013/03/01 14:34:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.6.2.14.4.10  2013/02/06 01:25:08  kavitha
 * CASE201112/CR201113/LOG201121
 * Serial # functionality - Inventory process
 *
 * Revision 1.6.2.14.4.9  2012/12/16 01:39:08  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA after go live Fixes.
 *
 * Revision 1.6.2.14.4.8  2012/12/12 07:43:51  spendyala
 * CASE201112/CR201113/LOG201121
 * moved from 2012.2 branch
 *
 * Revision 1.6.2.14.4.7  2012/11/26 16:50:02  gkalla
 * CASE201112/CR201113/LOG201121
 * Case 20120953 for RF inventory move
 *
 * Revision 1.6.2.14.4.6  2012/11/12 15:50:00  gkalla
 * CASE201112/CR201113/LOG201121
 * Locking concept in RF Inventory move
 *
 * Revision 1.6.2.14.4.5  2012/11/01 14:55:42  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.6.2.14.4.4  2012/09/26 22:43:33  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue Fixes for FISK,BOOMBAH and TDG.
 *
 * Revision 1.6.2.14.4.3  2012/09/26 12:40:44  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.6.2.14.4.2  2012/09/24 22:45:21  spendyala
 * CASE201112/CR201113/LOG201121
 * Issues related to Expiry date is resolved.
 *
 * Revision 1.6.2.14.4.1  2012/09/21 15:01:30  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.6.2.14  2012/09/13 12:43:33  spendyala
 * CASE201112/CR201113/LOG201121
 * issue related to FIFO Date populating in create inventory.
 *
 * Revision 1.6.2.13  2012/09/07 03:47:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.6.2.12  2012/09/03 13:49:11  schepuri
 * CASE201112/CR201113/LOG201121
 * added date stamp
 *
 * Revision 1.6.2.11  2012/08/07 09:16:49  gkalla
 * CASE201112/CR201113/LOG201121
 * To update Rem cube of From location
 *
 * Revision 1.6.2.10  2012/07/24 16:47:13  gkalla
 * CASE201112/CR201113/LOG201121
 * Inventory move physical location issue
 *
 * Revision 1.6.2.9  2012/06/26 23:47:27  gkalla
 * CASE201112/CR201113/LOG201121
 * Inv move issue fixed
 *
 * Revision 1.6.2.8  2012/05/07 12:01:35  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Replenishment
 *
 * Revision 1.6.2.7  2012/04/30 10:02:37  spendyala
 * CASE201112/CR201113/LOG201121
 * While Searching of Item in ItemMaster,
 * 'name' filter is changed to 'nameinternal' and also checking weather the item is inactive state or not.
 *
 * Revision 1.6.2.6  2012/04/11 14:04:40  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Invtentory Move
 *
 * Revision 1.6.2.5  2012/04/11 12:16:47  gkalla
 * CASE201112/CR201113/LOG201121
 * True lot functionality
 *
 * Revision 1.6.2.4  2012/02/23 00:27:34  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * supress lp in Invtmove and lp merge based on fifodate
 *
 * Revision 1.6.2.3  2012/02/21 13:24:22  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.6.2.2  2012/02/16 14:59:50  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Invtmove
 *
 * Revision 1.6.2.1  2012/02/07 12:52:23  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Inventory Move Changes
 *
 * Revision 1.10  2012/01/27 07:03:13  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Inventory Move Changes
 *
 * Revision 1.9  2012/01/20 01:10:19  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Inventory Move Changes
 *
 * Revision 1.8  2012/01/19 14:48:32  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Invt Move
 *
 * Revision 1.7  2012/01/13 15:21:13  gkalla
 * CASE201112/CR201113/LOG201121
 * For Inventory move with diff location
 *
 * Revision 1.6  2011/12/30 22:36:57  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Inventory Move Changes
 *
 * Revision 1.5  2011/12/29 15:00:10  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Inventory Move Changes
 *
 * Revision 1.13  2011/12/28 23:11:17  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Inventory Move Changes
 *

 *****************************************************************************/

function CompleteInventoryMove(request, response)
{	
	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);

	if (request.getMethod() == 'GET') 
	{
		/*if (sessionobj!=context.getUser())
		{
			if(sessionobj==null || sessionobj=='')
			{
				sessionobj=context.getUser();
				context.setSessionObject('session', sessionobj); 
			}*/
		try
		{
			if (sessionobj!=context.getUser())
			{

			if(sessionobj==null || sessionobj=='')
			{
				sessionobj=context.getUser();
				context.setSessionObject('session', sessionobj); 
			}
			var totqtymoved=request.getParameter('custparam_totqtymoveed');
			var getMoveQuantity = request.getParameter('custparam_moveqty');
			var getUOMId = request.getParameter('custparam_uomid');
			var getUOM = request.getParameter('custparam_uom');
			var getStatus = request.getParameter('custparam_status');
			var getStatusId = request.getParameter('custparam_statusid');
			var getLOTId = request.getParameter('custparam_lotid');
			var getLOTNo = request.getParameter('custparam_lot'); 
			var getItemType = request.getParameter('custparam_itemtype');
			var getBatchNo = request.getParameter('custparam_batch'); 	 
			var getActualBeginDate = request.getParameter('custparam_actualbegindate');
			var getActualBeginTime = request.getParameter('custparam_actualbegintime');
			var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
			var locationId=request.getParameter('custparam_locationId');//
			nlapiLogExecution('ERROR', 'locationId ', request.getParameter('custparam_locationId')); 
			var getexistingLP=request.getParameter('custparam_existingLP');//
			var getBinLocationId = request.getParameter('custparam_imbinlocationid');
			var getBinLocation = request.getParameter('custparam_imbinlocationname');
			var getItemId = request.getParameter('custparam_imitemid');
			var getItem = request.getParameter('custparam_imitem');
			var getTotQuantity = request.getParameter('custparam_totquantity');
			var getAvailQuantity = request.getParameter('custparam_availquantity');	 
			var getLP = request.getParameter('custparam_lp');
			var getLPId = request.getParameter('custparam_lpid');
			var getMoveQty = request.getParameter('custparam_moveqty');
			var getInvtRecID=request.getParameter('custparam_invtrecid');
			nlapiLogExecution('ERROR', 'getInvtRecID ',getInvtRecID);
			var locationid=request.getParameter('custparam_locationId');
			var getBatchId = request.getParameter('custparam_batchid');
			var getActualBeginDate = request.getParameter('custparam_actualbegindate');
			var getActualBeginTime = request.getParameter('custparam_actualbegintime');
			var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
			var putmethodID=request.getParameter('custparam_NewPutMethodID');//
			var getNewBinLocId= request.getParameter('custparam_newbinlocationid');
			var getNewBinLocation= request.getParameter('custparam_newbinlocationname');
			var getNewStatus=request.getParameter('custparam_skustatus');
			var adjustmenttype=request.getParameter('custparam_adjusttype');
			var getNewLP=request.getParameter('custparam_newLP');
			var ItemDesc = request.getParameter('custparam_itemdesc');
			var compId=request.getParameter('custparam_compid');//
			var fifodate=request.getParameter('custparam_fifodate');//
			nlapiLogExecution('ERROR', 'compId ',compId);
			nlapiLogExecution('ERROR', 'putmethodID ',putmethodID);
			var oldstatusid='';

		var itemstatuslist = getItemStatusList(getStatus);

		if(itemstatuslist!=null && itemstatuslist!='')
		{
			oldstatusid=itemstatuslist[0].getId();
		}

		nlapiLogExecution('ERROR', 'oldstatusid ', oldstatusid);
		nlapiLogExecution('ERROR', 'getStatusId ', getStatusId);
		//case 20124748� start : Checking Itemstatus
		if((oldstatusid!=null && oldstatusid!='') && (parseInt(oldstatusid)== parseInt(getStatusId)))
			getStatusId = oldstatusid;
		else
			getStatusId = getStatusId;
		////case 20124748 end

		nlapiLogExecution('ERROR', 'getStatusId ', getStatusId);
		nlapiLogExecution('ERROR', 'getNewStatus ', getNewStatus);
		nlapiLogExecution('ERROR', 'totqtymoved ', totqtymoved);
		nlapiLogExecution('ERROR', 'locationid ', request.getParameter('custparam_locationId'));
		var getPackCode='1';
		var getPackCodeId='1'; 
		var vbinlocation='';
		var vLP='';

		var IMarray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		IMarray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR', 'getLanguage', IMarray["custparam_language"]);


		var st5;
		if( getLanguage == 'es_ES')
		{

			st5 = "LP NO V&#193;LIDO";
			st6 = "LP V&#193;LIDA / BIN UBICACI�N";

		}
		else
		{

			st5 = "INVALID LP";
			st6 = "INVALID LP/BIN LOCATION";

		}
		var vErrorLog;
		var vTaskType=10;
		var getNewLPArray,getNewLPNo;
		var getActualEndDate = DateStamp();
		var getActualEndTime = TimeStamp();
		IMarray["custparam_newbinlocationid"] = getNewBinLocId;
		IMarray["custparam_newbinlocationname"] = getNewBinLocation;

				nlapiLogExecution('ERROR', 'A_getMoveQuantity22', getMoveQuantity);
				nlapiLogExecution('ERROR', 'A_getAvailQuantity22', getAvailQuantity);

				if(getInvtRecID != null && getInvtRecID != '')
				{	
					var filterstmp = new Array();
					filterstmp.push(new nlobjSearchFilter('internalid', null, 'is', getInvtRecID));

					var searchresultstmp = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filterstmp, null);
					if(searchresultstmp == null || searchresultstmp == '' || searchresultstmp.length<=0)
					{
						IMarray["custparam_error"] = 'Move is already performed for this LP/Location';
						IMarray["custparam_screenno"] = '18';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
						nlapiLogExecution('ERROR', 'Move is already performed for this LP/Location');
						nlapiLogExecution('ERROR', 'Record Doesnot exist', getInvtRecID);
						return;
					}	
				}


				if(parseFloat(getMoveQuantity) < parseFloat(getAvailQuantity) && (getNewLP==null || getNewLP==''))
				{
					nlapiLogExecution('ERROR', 'getNewLP before22', 'getNewLP');
					getNewLPArray = GetMultipleLP(1,1,1,locationid);
					nlapiLogExecution('ERROR', 'getNewLPArray after', getNewLPArray);
					getNewLPNo = getNewLPArray.split(',');
					getNewLP =getNewLPNo[0]+getNewLPNo[1];
					nlapiLogExecution('ERROR', 'getNewLP after', getNewLP);
				}

		if ((getNewLP == '' || getNewLP == null) && (getexistingLP==null && getexistingLP=='')) 
		{
			//	if the 'Send' button is clicked without any option value entered,
			//  it has to show an error message. The next screen to which it has to navigate is to the error screen.
			///IMarray["custparam_error"] = 'Enter New LP';
			IMarray["custparam_error"] = st5;
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
			nlapiLogExecution('ERROR', 'Entered New Location', getNewBinLocation);
			return;
		}
		else 
		{
			try
			{
				if(getInvtRecID != null && getInvtRecID != '')
				{	
					nlapiLogExecution('ERROR','soname before Locking',getInvtRecID);
					var fulfillRec= nlapiCreateRecord('customrecord_ebiznet_lockrecs');
					fulfillRec.setFieldValue('name', getInvtRecID);					
					fulfillRec.setFieldValue('custrecord_ebiznet_trantype', 'rfinvmove');
					fulfillRec.setFieldValue('externalid', getInvtRecID);
					fulfillRec.setFieldValue('custrecord_ebiznet_notes', 'Locked by RF Inv move');

					nlapiSubmitRecord(fulfillRec,false,true );
					nlapiLogExecution('ERROR','Locked successfully');
				}
			}
			catch(exp1)
			{

				nlapiLogExecution('ERROR', 'exp1', exp1);
				IMarray["custparam_screenno"] = '18';
				var wmsE='Another thread is currently processing inventory move to the same location, which prevents your request from executed.';
				IMarray["custparam_error"] = wmsE;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
				nlapiLogExecution('ERROR', ' External Locking',wmsE);
				return;
			}
			var invtrecid=-1;

			nlapiLogExecution('ERROR', 'A_getMoveQuantity1', getMoveQuantity);
			nlapiLogExecution('ERROR', 'A_getAvailQuantity1', getAvailQuantity);
			if(parseFloat(getMoveQuantity) <= parseFloat(getAvailQuantity))
			{	
				if(getNewLP==null || getNewLP=='')
					getNewLP=getLP;
				
				//if(getBinLocationId == getNewBinLocId && getLP == getNewLP && parseFloat(getMoveQuantity) < parseFloat(getAvailQuantity))
				if(getLP == getNewLP && parseFloat(getMoveQuantity) < parseFloat(getAvailQuantity))
				{
					IMarray["custparam_error"] = st6;
					IMarray["custparam_screenno"] = '18';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
					nlapiLogExecution('ERROR', 'Entered New Location/LP', 'Entered New Location/LP');
					return;
				}
				
				invtrecid=createOpenTaskAndInventoryRecord(getItemId, getItem, getStatusId, getStatus,
						getUOMId, getUOM, getLOTNo, getLOTId, getLP, getNewLP, getBinLocation, 
						getBinLocationId, getNewBinLocation, getNewBinLocId, getPackCode, getPackCodeId, 
						getTotQuantity, getAvailQuantity, getMoveQty, getActualEndDate, getActualEndTime,
						getActualBeginDate, getActualBeginTime, getInvtRecID,locationid,getNewStatus,
						adjustmenttype,ItemDesc,compId,fifodate,getActualBeginTimeAMPM,putmethodID,totqtymoved);

			}
			nlapiLogExecution('ERROR', 'invtrecid', invtrecid);
			if(invtrecid=='ERROR')
			{	
				//var wmsE = nlapiCreateError('Another thread is currently processing inventory move to the same location, which prevents your request from executed.', '', true);
				vErrorLog='ERROR';
				IMarray["custparam_screenno"] = '18';
				var wmsE='Another thread is currently processing inventory move to the same location, which prevents your request from executed.';
				IMarray["custparam_error"] = wmsE;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
				nlapiLogExecution('ERROR', 'Locked',wmsE);
				return;

				//throw wmsE;
			}
			else if(invtrecid=='-1')
			{

				IMarray["custparam_screenno"] = '18';
				var wmsE='Inventory Move is failed.';
				IMarray["custparam_error"] = wmsE;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);				
				return;
			}
			else
			{	
				if(getInvtRecID!=null && getInvtRecID!='')
				{
					try
					{

						//start


						if(parseFloat(getMoveQuantity) == parseFloat(getAvailQuantity))
						{
							//if(getNewLP==null || getNewLP=='')
							//getNewLP=getLP;

							var NewLPFilters = new Array();
							NewLPFilters[0] = new nlobjSearchFilter('name', null, 'is', getNewLP);

							var NewLPSearchResults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, NewLPFilters, null);				
							if (NewLPSearchResults != null && NewLPSearchResults.length > 0) 
							{
								nlapiLogExecution('ERROR', 'LPSearchResults', NewLPSearchResults.length);
								var getNewLPId = NewLPSearchResults[0].getId();
								nlapiLogExecution('ERROR', 'LP Id', getNewLPId );
								IMarray["custparam_newLP"] = getNewLP;
								IMarray["custparam_newLPId"] = getNewLPId;
							}
							nlapiLogExecution('ERROR', 'else getLP', getLP);
							nlapiLogExecution('ERROR', 'else getNewLP', getNewLP);

							var scount=1;
							LABL1: for(var i=0;i<scount;i++)
							{	
								nlapiLogExecution('ERROR', 'CUSTOM_RECORD_COLLISION', i);
								try
								{

									var recLoad = nlapiLoadRecord('customrecord_ebiznet_createinv', getInvtRecID);
									var vNewAvailQty=recLoad.getFieldValue('custrecord_ebiz_qoh');
									if(vNewAvailQty == null || vNewAvailQty == '')
										vNewAvailQty='0';




									/*invtrecid=createOpenTaskAndInventoryRecord(getItemId, getItem, getStatusId, getStatus, 
									getUOMId, getUOM, getLOTNo, getLOTId, getLP, getNewLP, getBinLocation, 
									getBinLocationId, getNewBinLocation, getNewBinLocId, getPackCode, getPackCodeId, 
									getTotQuantity, getAvailQuantity, getMoveQty, getActualEndDate, getActualEndTime, 
									getActualBeginDate, getActualBeginTime, getInvtRecID,locationid,getNewStatus,
									adjustmenttype,ItemDesc,compId,fifodate);*/

									//CreateInventory(getItemId, getItem, getStatusId, getStatus, getUOMId, getUOM, getLOTNo, getLOTId, getLP, getNewLP, getBinLocation, getBinLocationId, getNewBinLocation, getNewBinLocId, getPackCode, getPackCodeId, getTotQuantity, getAvailQuantity, getMoveQty, getActualEndDate, getActualEndTime, getActualBeginDate, getActualBeginTime, getInvtRecID,locationid,getNewStatus,adjustmenttype,vTaskType);
									nlapiLogExecution('ERROR', 'getTotQuantity', getTotQuantity);
									nlapiLogExecution('ERROR', 'getTotQuantity', getMoveQty);
									//var openmovetaskrecord=getMovetaskrecord(getLP,getItemId,getItem);
									//if((getInvtRecID!=null && getInvtRecID!='') && (openmovetaskrecord==null || openmovetaskrecord==''))
									if(getInvtRecID!=null && getInvtRecID!='')
									{ 	 
										//recLoad.setFieldValue('custrecord_ebiz_inv_qty', (parseFloat(getTotQuantity) - parseFloat(getMoveQty)));
										recLoad.setFieldValue('custrecord_ebiz_inv_qty', (parseFloat(vNewAvailQty) - parseFloat(getMoveQty)));

										//recLoad.setFieldValue('custrecord_ebiz_qoh', (parseFloat(getTotQuantity) - parseFloat(getMoveQty)));
										recLoad.setFieldValue('custrecord_ebiz_qoh', (parseFloat(vNewAvailQty) - parseFloat(getMoveQty)));
										recLoad.setFieldValue('custrecord_updated_user_no', nlapiGetContext().getUser());
										recLoad.setFieldValue('custrecord_ebiz_displayfield','N'); 
										recLoad.setFieldValue('custrecord_ebiz_callinv', 'N');
										nlapiSubmitRecord(recLoad, false, true);

										//if((parseFloat(getTotQuantity) - parseFloat(getMoveQty)) == 0)
										if((parseFloat(vNewAvailQty) - parseFloat(getMoveQty)) == 0)
										{					
											var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', getInvtRecID);	
											nlapiLogExecution('ERROR', 'Inventory Record Deletion', 'Successfully');
										}

										nlapiLogExecution('ERROR', 'Inventory Record Updated', 'Successfully');

									}



//									var recLoad = nlapiLoadRecord('customrecord_ebiznet_createinv', getInvtRecID);
//									var vOldInvQty= recLoad.getFieldValue('custrecord_ebiz_inv_qty');
//									var vOldInvQOH= recLoad.getFieldValue('custrecord_ebiz_qoh');
//									if(vOldInvQty == null || vOldInvQty == '')
//									vOldInvQty=0;
//									if(vOldInvQOH == null || vOldInvQOH == '')
//									vOldInvQOH=0;
//									recLoad.setFieldValue('custrecord_ebiz_inv_qty', (parseInt(vOldInvQty) - parseInt(getMoveQty)));
//									recLoad.setFieldValue('custrecord_ebiz_qoh', (parseInt(vOldInvQOH) - parseInt(getMoveQty)));
//									recLoad.setFieldValue('custrecord_updated_user_no', nlapiGetContext().getUser());
//									recLoad.setFieldValue('custrecord_ebiz_displayfield','N'); 
//									recLoad.setFieldValue('custrecord_ebiz_callinv', 'N');
//									nlapiSubmitRecord(recLoad, false, true);

								}
								catch(ex)
								{
									var exCode='CUSTOM_RECORD_COLLISION'; 
									var wmsE='Inventory record being updated by another user. Please try again...';
									if (ex instanceof nlobjError) 
									{	
										wmsE=ex.getCode() + '\n' + ex.getDetails();
										exCode=ex.getCode();
									}
									else
									{
										wmsE=ex.toString();
										exCode=ex.toString();
									}  
									/*var exceptionname='RF Inventory Move from Source';
									var functionality='InvMove';
									var trantype=3;
									var vInvRecId=getInvtRecID;
									var vcontainerLp=getLP;
									nlapiLogExecution('ERROR', 'DetailsError', functionality);	
									nlapiLogExecution('ERROR', 'vcontainerLp', vcontainerLp);
									nlapiLogExecution('ERROR', 'vInvRecId', vInvRecId);
									var reference3=getMoveQty;
									var reference4 ="";
									var reference5 ="";
									var userId = nlapiGetUser();
									InsertExceptionLog(exceptionname,trantype, functionality, wmsE, vInvRecId, vcontainerLp,reference3,reference4,reference5, userId);*/
									nlapiLogExecution('ERROR', 'Exception in RF Inv move : ', wmsE);

									//response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);



									if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
									{ 
										scount=scount+1;
										continue LABL1;
									}
									else break LABL1;
								}
							} 








							//if(getNewLP==null || getNewLP=='')
							//getNewLP=getLP;


						}
						else if(parseFloat(getMoveQuantity) < parseFloat(getAvailQuantity))
						{
							var scount=1;
							LABL1: for(var i=0;i<scount;i++)
							{	
								nlapiLogExecution('ERROR', 'CUSTOM_RECORD_COLLISION', i);
								try
								{
									if(getInvtRecID!=null && getInvtRecID!='')
									{
										var recLoad = nlapiLoadRecord('customrecord_ebiznet_createinv', getInvtRecID);
										var vNewAvailQty=recLoad.getFieldValue('custrecord_ebiz_qoh');
										if(vNewAvailQty == null || vNewAvailQty == '')
											vNewAvailQty='0';

										nlapiLogExecution('ERROR', 'if getLP', getLP);
										nlapiLogExecution('ERROR', 'if getNewLP', getNewLP);
										/*invtrecid=createOpenTaskAndInventoryRecord(getItemId, getItem, getStatusId, getStatus, getUOMId, getUOM, getLOTNo, getLOTId, 
								getLP, getNewLP, getBinLocation, getBinLocationId, getNewBinLocation, getNewBinLocId, getPackCode, getPackCodeId, 
								getTotQuantity, getAvailQuantity, getMoveQty, getActualEndDate, getActualEndTime, getActualBeginDate, 
									getActualBeginTime, getInvtRecID,locationid,getNewStatus,adjustmenttype,ItemDesc,compId,fifodate);*/
										nlapiLogExecution('ERROR', 'OpenTask Record Creation', 'Successfully');	
										nlapiLogExecution('ERROR', 'Inventory Record Id', getInvtRecID);	




										//recLoad.setFieldValue('custrecord_ebiz_inv_qty', (parseFloat(getTotQuantity) - parseFloat(getMoveQty)));
										//recLoad.setFieldValue('custrecord_ebiz_qoh', (parseFloat(getTotQuantity) - parseFloat(getMoveQty)));
										recLoad.setFieldValue('custrecord_ebiz_qoh', (parseFloat(vNewAvailQty) - parseFloat(getMoveQty)));
										recLoad.setFieldValue('custrecord_updated_user_no', nlapiGetContext().getUser());
										recLoad.setFieldValue('custrecord_ebiz_displayfield','N');						 	
										recLoad.setFieldValue('custrecord_ebiz_callinv', 'N');
										nlapiSubmitRecord(recLoad, false, true);

										//if((parseFloat(getTotQuantity) - parseFloat(getMoveQty)) == 0)
										if((parseFloat(vNewAvailQty) - parseFloat(getMoveQty)) == 0)
										{					
											var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', getInvtRecID);				
										}

									}
								}
								catch(ex)
								{
									var exCode='CUSTOM_RECORD_COLLISION'; 
									var wmsE='Inventory record being updated by another user. Please try again...';
									if (ex instanceof nlobjError) 
									{	
										wmsE=ex.getCode() + '\n' + ex.getDetails();
										exCode=ex.getCode();
									}
									else
									{
										wmsE=ex.toString();
										exCode=ex.toString();
									}  
									/*var exceptionname='RF Inventory Move from Source';
									var functionality='InvMove';
									var trantype=3;
									var vInvRecId=getInvtRecID;
									var vcontainerLp=getLP;
									nlapiLogExecution('ERROR', 'DetailsError', functionality);	
									nlapiLogExecution('ERROR', 'vcontainerLp', vcontainerLp);
									nlapiLogExecution('ERROR', 'vInvRecId', vInvRecId);
									var reference3=getMoveQty;
									var reference4 ="";
									var reference5 ="";
									var userId = nlapiGetUser();
									InsertExceptionLog(exceptionname,trantype, functionality, wmsE, vInvRecId, vcontainerLp,reference3,reference4,reference5, userId);*/
									nlapiLogExecution('ERROR', 'Exception in RF Inv move : ', wmsE);

									//response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);



									if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
									{ 
										scount=scount+1;
										continue LABL1;
									}
									else break LABL1;
								}
							}

							nlapiLogExecution('ERROR', 'getexistingLP', getexistingLP);
							nlapiLogExecution('ERROR', 'getNewLP', getNewLP);
							if(getexistingLP == getNewLP)
							{
								var filtersinv = new Array();
								if(getNewLP!=null && getNewLP!='')
									filtersinv.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp',null, 'is',getNewLP));	
								var SrchINVRecord = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinv, null);
								if(SrchINVRecord!=null && SrchINVRecord!='')
									var getNewLPId = SrchINVRecord[0].getId();
								IMarray["custparam_newLP"] = getNewLP;
								IMarray["custparam_newLPId"] = getNewLPId;
							}
							else
							{
								//code to be written when the user gives diff Lp
							}



//							var recLoad = nlapiLoadRecord('customrecord_ebiznet_createinv', getInvtRecID);
//							var vOldInvQty= recLoad.getFieldValue('custrecord_ebiz_inv_qty');
//							var vOldInvQOH= recLoad.getFieldValue('custrecord_ebiz_qoh');
//							if(vOldInvQty == null || vOldInvQty == '')
//							vOldInvQty=0;
//							if(vOldInvQOH == null || vOldInvQOH == '')
//							vOldInvQOH=0;
//							recLoad.setFieldValue('custrecord_ebiz_inv_qty', (parseInt(vOldInvQty) - parseInt(getMoveQty)));
//							recLoad.setFieldValue('custrecord_ebiz_qoh', (parseInt(vOldInvQOH) - parseInt(getMoveQty)));
//							recLoad.setFieldValue('custrecord_updated_user_no', nlapiGetContext().getUser());
//							recLoad.setFieldValue('custrecord_ebiz_displayfield','N'); 
//							recLoad.setFieldValue('custrecord_ebiz_callinv', 'N');
//							nlapiSubmitRecord(recLoad, false, true);



						}


					}
					catch(exp)
					{
						nlapiLogExecution('ERROR', 'Exception in Inventory Record updation', exp);
					}
				}
				//Load the inevtory record to fetch LP and Location.
				nlapiLogExecution('ERROR', 'Inventory Record Internal Id', invtrecid);

				if(invtrecid!=-1 && invtrecid!='' && invtrecid!=null)
				{
					try
					{
						var newinvtrecid = nlapiLoadRecord('customrecord_ebiznet_createinv',invtrecid);
						if(newinvtrecid!=-1 && newinvtrecid!=''&& newinvtrecid!=null)
						{
							vbinlocation = newinvtrecid.getFieldText('custrecord_ebiz_inv_binloc'); 
							vLP = newinvtrecid.getFieldValue('custrecord_ebiz_inv_lp');
						}
					}
					catch(exp)
					{
						nlapiLogExecution('ERROR', 'Exception in loading invt record', exp);
					}
				}
			}

		}
		nlapiLogExecution('ERROR', 'vbinlocation', vbinlocation);
		nlapiLogExecution('ERROR', 'vLP', vLP);

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4;
		if( getLanguage == 'es_ES')
		{

			st0 = "INVENTARIO DE MUDANZA";
			st1 = "INVENTARIO DE MUDANZA";
			st2 = "BIN UBICACI&#211;N:";
			st3 = "LP REAL NO";
			st4 = "CONTINUAR";

		}
		else
		{
			st0 = "INVENTORY MOVE";
			st1 = "INVENTORY MOVE SUCCESSFUL";
			st2 = "BIN LOCATION :";
			st3 = "ACTUAL LP NO";
			st4 = "CONTINUE";

		}




		var functionkeyHtml=getFunctionkeyScript('_rf_putaway_lp'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		//html = html + " document.getElementById('cmdSend').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_putaway_lp' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + "</td></tr>";
		if(vbinlocation!=null && vbinlocation!=''){
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st2 + " " + vbinlocation + "</td></tr>";   
		}
		if(vLP!=null && vLP!=''){
			html = html + "			<tr>";
			html = html + "				<td align = 'left'> " + st3 + " " + vLP + "</td></tr>";   
		}
		html = html + "			<tr>";
		html = html + "				<td><input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st4 + " <input name='cmdSend' id='cmdSend' type='submit' value='F8'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('cmdSend').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

				response.write(html);
			}
			else
			{
				var IMarray = new Array();
				IMarray["custparam_screenno"] = '18';
				IMarray["custparam_error"] = 'LP ALREADY IN PROCESS';
				context.setSessionObject('session', null);
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
			}
		}
		catch (e)  {
			IMarray["custparam_screenno"] = '18';
			IMarray["custparam_error"] = 'LP ALREADY IN PROCESS';
			context.setSessionObject('session', null);
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
		} finally {					
			//context.setSessionObject('session', null);
			nlapiLogExecution('DEBUG', 'finally','block');

			//Unlocking Sales order
			nlapiLogExecution('ERROR','Inventory internal id before unlocking in finally',getInvtRecID);


			if(getInvtRecID != null && getInvtRecID != '')
			{	
				var ExternalIdFilters = new Array();
				ExternalIdFilters.push(new nlobjSearchFilter('name', null, 'is', getInvtRecID));
				ExternalIdFilters.push(new nlobjSearchFilter('custrecord_ebiznet_trantype', null, 'is', 'rfinvmove'));
				var searchresultsExt = nlapiSearchRecord('customrecord_ebiznet_lockrecs', null, ExternalIdFilters, null);
				if(searchresultsExt != null && searchresultsExt != '')
				{
					nlapiLogExecution('ERROR','Recordid before unlocking',searchresultsExt[0].getId());
					nlapiDeleteRecord('customrecord_ebiznet_lockrecs',searchresultsExt[0].getId());
					nlapiLogExecution('ERROR','Unlocked successfully');
				}	
			}
		}
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is not null');

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.

		var IMarray = new Array();

		var optedEvent = request.getParameter('cmdSend');
		/*if (sessionobj!=context.getUser())
		{*/
		try
		{

			if(sessionobj==null || sessionobj=='')
			{
				sessionobj=context.getUser();
				context.setSessionObject('session', sessionobj); 
			}
			//	if the previous button 'F7' is clicked, it has to go to the previous screen 
			//  ie., it has to go to create inventory screen.
			if (optedEvent == 'F8') 
			{
				response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_main', 'customdeploy_rf_inventory_move_main_di', false, optedEvent);
			}
			nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
		}
		catch (e)  {
			IMarray["custparam_screenno"] = '18';
			IMarray["custparam_error"] = 'LP ALREADY IN PROCESS';
			context.setSessionObject('session', null);
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
		} finally {					
			context.setSessionObject('session', null);
			nlapiLogExecution('DEBUG', 'finally','block');

		}
		/*}
		else
		{
			IMarray["custparam_screenno"] = '18';
			IMarray["custparam_error"] = 'LP ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
		}*/
	}
}

function CreateInventory(ItemId, Item, ItemStatusId,ItemStatus, UOMId, UOM, LOTNo, LOTNoId, ItemLP, NewItemLP, BinLocation, BinLocationId, 
		NewBinLocation, NewBinLocationId, getPackCode, getPackCodeId, TotQuantity, AvailQty, MoveQty, ActualEndDate, ActualEndTime, 
		ActualBeginDate, ActualBeginTime, getInvtRecID,locationid,getNewStatus,adjustmenttype,vTaskType,CallFlag,ItemDesc,compId,fifodate,opentaskid,putmethodID,totqtymoved)
{
	nlapiLogExecution('ERROR', 'Into CreateInventory', '');
	nlapiLogExecution('ERROR', 'ItemStatusId', ItemStatusId);
	nlapiLogExecution('ERROR', 'New ItemStatusId', getNewStatus);
	nlapiLogExecution('ERROR', 'BinLocationId', BinLocationId);
	nlapiLogExecution('ERROR', 'New BinLocationId', NewBinLocationId);
	nlapiLogExecution('ERROR', 'MoveQty', MoveQty);
	nlapiLogExecution('ERROR', 'compId in CreateInventory', compId);
	nlapiLogExecution('ERROR', 'putmethodID in CreateInventory', putmethodID);
	var expdate="";
	//if((getInvtRecID!=null && getInvtRecID!='') && compId==null || compId=='')
	if(getInvtRecID!=null && getInvtRecID!='')
	{
		try
		{
			var recLoad = nlapiLoadRecord('customrecord_ebiznet_createinv', getInvtRecID);
			if(compId==null || compId=='')
				compId=recLoad.getFieldValue('custrecord_ebiz_inv_company');
			expdate=recLoad.getFieldValue('custrecord_ebiz_expdate');
			var invOldExistAvailQty = recLoad.getFieldValue('custrecord_ebiz_avl_qty');
			nlapiLogExecution('ERROR', 'invOldExistAvailQty', invOldExistAvailQty);
			if(invOldExistAvailQty == null || invOldExistAvailQty == '')
				invOldExistAvailQty=0;
			if(parseInt(invOldExistAvailQty) < parseInt(MoveQty))
			{
				return 'ERROR';

			}
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR', 'Exception in CreateInventory', exp);
		}
	}
	nlapiLogExecution('ERROR', 'expdate', expdate);
	nlapiLogExecution('ERROR', 'compId', compId);
//	var vNewSite;
//	if(NewBinLocationId!=null && NewBinLocationId!='' && )
//	{
//	try
//	{
//	var recLoadBinLoc = nlapiLoadRecord('customrecord_ebiznet_location', NewBinLocationId);
//	vNewSite=recLoadBinLoc.getFieldValue('custrecord_ebizsitelocf');
//	}
//	catch(exp)
//	{
//	nlapiLogExecution('ERROR', 'Exception in Get location from bin loc in CreateInventory', exp);
//	}
//	}

//	nlapiLogExecution('ERROR', 'vNewSite', vNewSite);
//	nlapiLogExecution('ERROR', 'locationid', locationid);
//	if(vNewSite != null && vNewSite != '')
//	locationid=vNewSite;
	if(locationid==null || locationid=='')
		locationid=getRoledBasedLocation();

	nlapiLogExecution('ERROR', 'locationid', locationid);

	var invtrecid = -1;
	if(getNewStatus!=null && getNewStatus!=''&& ItemStatusId!=getNewStatus)
		ItemStatusId=getNewStatus;

	var FifoDateInLot = '';

	nlapiLogExecution('ERROR', 'LOTNoId', LOTNoId);

	//if(LOTNoId!=null && LOTNoId!='')
	//FifoDateInLot = nlapiLookupField('customrecord_ebiznet_batch_entry', LOTNoId, 'custrecord_ebizfifodate');

	//Case# 20123225 start : to insert expiry date
	if(expdate == null || expdate =='')
	{
		if(LOTNoId!=null && LOTNoId!='')
			expdate = nlapiLookupField('customrecord_ebiznet_batch_entry', LOTNoId, 'custrecord_ebizexpirydate');

	}
	//Case# 20123225 end
	var varMergeLP = 'T';
	nlapiLogExecution('DEBUG', 'putmethodID', putmethodID);
	if(putmethodID!=null && putmethodID!='')
		varMergeLP = isMergeLP(putmethodID);
	nlapiLogExecution('DEBUG', 'varMergeLP', varMergeLP);
	nlapiLogExecution('ERROR', 'FifoDateInLot', FifoDateInLot);
	nlapiLogExecution('ERROR', 'ItemLP', ItemLP);
	nlapiLogExecution('ERROR', 'NewItemLP', NewItemLP);
	if((BinLocationId!=NewBinLocationId || ItemLP != NewItemLP)&& varMergeLP=="T")
	{
		var invExistQty=0;
		var filters = new Array();
		var i = 0;

		nlapiLogExecution('ERROR', 'ItemId',ItemId);
		if (ItemId != null && ItemId != '') {		
			filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', ItemId);
			i++;
		}

		nlapiLogExecution('ERROR', 'ItemStatus',ItemStatus);
		nlapiLogExecution('ERROR', 'ItemStatusId',ItemStatusId);

		if(ItemStatus!=null && ItemStatus!='')
		{
			filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', ItemStatusId);
			i++;
		}

		nlapiLogExecution('ERROR', 'packcode',getPackCode);
		if(getPackCode !=null && getPackCode!="")
		{
			filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_packcode', null, 'is', getPackCode);
			i++;
		}

		nlapiLogExecution('ERROR', 'NewBinLocationId',NewBinLocationId);
		if(NewBinLocationId!=null&&NewBinLocationId!="")
		{
			filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', NewBinLocationId);
			i++;
		}

		nlapiLogExecution('ERROR', 'LOTNoId',LOTNoId);
		if(LOTNoId!=null&&LOTNoId!="")
		{
			filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', [LOTNoId]);
			i++;
		}
		nlapiLogExecution('ERROR', 'fifodate',fifodate);
		if(fifodate!=null&&fifodate!="")
		{
			filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_fifo', null, 'on', fifodate);
			i++;
		}
		if(locationid!=null&&locationid!="")
		{
			filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', locationid);
			i++;
		}
		
		
		nlapiLogExecution('ERROR', 'ItemLP',ItemLP);
		nlapiLogExecution('ERROR', 'NewItemLP',NewItemLP);
		if(ItemLP!=null && ItemLP!='' && ItemLP != NewItemLP)
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'isnot', ItemLP));

		//Case# 20145096�(System is not merging the moved qty with the existing lot LP) start
		/*if(NewItemLP!=null && NewItemLP!='' )
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', NewItemLP));*/
		//Case# 20145096 end
		filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [19,3]));
		//Case# 201410342 Starts
		var varPaltQty = getMaxUOMQty(ItemId,getPackCode);
		if(varPaltQty!=null && varPaltQty!='')
			filters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'lessthan', varPaltQty));	// Case# 201410719		
	//Case# 201410342 ends
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh'); 
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_lp'); 
		var inventorysearch = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

		var vid=0;
		var invExistLp='';
		if(inventorysearch != null && inventorysearch != '' && inventorysearch.length>0)
		{
			nlapiLogExecution('ERROR', 'Inventory found in same location for same item',inventorysearch.length);

			for(var k=0; k<inventorysearch.length;k++)
			{

				invExistLp = inventorysearch[k].getValue('custrecord_ebiz_inv_lp');	
				nlapiLogExecution('ERROR', 'invExistLp',invExistLp);
				nlapiLogExecution('ERROR', 'NewItemLP',NewItemLP);
				//case# 20127701 starts (create inventory issue)
				//if(invExistLp == NewItemLP)
				//{
					vid = inventorysearch[k].getId();
					invExistQty  =  inventorysearch[k].getValue('custrecord_ebiz_qoh');
				//}
				//case# 20127701 end
			}
		}

		nlapiLogExecution('ERROR', 'invExistQty',invExistQty);
		nlapiLogExecution('ERROR', 'invExistLp',invExistLp);
		nlapiLogExecution('ERROR', 'vid',vid);

		if(parseFloat(invExistQty) > 0){  
			nlapiLogExecution('ERROR', 'If invExistQty > 0',invExistQty);

		/*	try
			{
				nlapiLogExecution('ERROR','soname before Locking',vid);
				var fulfillRec= nlapiCreateRecord('customrecord_ebiznet_lockrecs');
				fulfillRec.setFieldValue('name', vid);					
				fulfillRec.setFieldValue('custrecord_ebiznet_trantype', 'rfinvmove');
				fulfillRec.setFieldValue('externalid', vid);
				fulfillRec.setFieldValue('custrecord_ebiznet_notes', 'Locked by RF Inv move');

				nlapiSubmitRecord(fulfillRec,false,true );
				nlapiLogExecution('ERROR','Locked successfully');
			}
			catch(exp)
			{
				nlapiLogExecution('ERROR', 'error', exp.getCode());
				nlapiSubmitField('customrecord_ebiznet_trn_opentask', opentaskid, 'custrecord_notes', exp);
				InsertExceptionLog('Inventory Move',2, 'Inventory Move', exp, null, null,null,null,null, nlapiGetUser());				
				return 'ERROR';
				//var wmsE = nlapiCreateError('Another thread is currently processing inventory move to the same location, which prevents your request from executed.', '', true);
				//throw wmsE;
				//return false;
			}*/
			try
			{
				var scount=1;
				LABL1: for(var i=0;i<scount;i++)
				{	
					nlapiLogExecution('ERROR', 'CUSTOM_RECORD_COLLISION', i);
					try
					{

						var invLoad = nlapiLoadRecord('customrecord_ebiznet_createinv', vid);
						invExistQty= invLoad.getFieldValue('custrecord_ebiz_qoh');
						var updatedInventory = parseFloat(invExistQty) + parseFloat(MoveQty);
						invLoad.setFieldValue('custrecord_ebiz_qoh', parseFloat(updatedInventory).toFixed(4));		
						invLoad.setFieldValue('custrecord_updated_user_no', nlapiGetContext().getUser());
						invLoad.setFieldValue('custrecord_ebiz_displayfield','N');	
						invLoad.setFieldValue('custrecord_ebiz_callinv', 'N');

						invtrecid = nlapiSubmitRecord(invLoad, false, true);
						nlapiLogExecution('ERROR', 'Inventory Merged to the existing LP',invExistLp);



//						var invLoad = nlapiLoadRecord('customrecord_ebiznet_createinv', vid);
//						invExistQty = invLoad.getFieldValue('custrecord_ebiz_qoh');
//						var updatedInventory = parseInt(invExistQty) + parseInt(MoveQty);
//						invLoad.setFieldValue('custrecord_ebiz_qoh', updatedInventory);		
//						invLoad.setFieldValue('custrecord_updated_user_no', nlapiGetContext().getUser());
//						invLoad.setFieldValue('custrecord_ebiz_displayfield','N');	
//						invLoad.setFieldValue('custrecord_ebiz_callinv', 'N');

//						invtrecid = nlapiSubmitRecord(invLoad, false, true);

					}
					catch(ex)
					{
						var exCode='CUSTOM_RECORD_COLLISION'; 
						var wmsE='Inventory record being updated by another user. Please try again...';
						if (ex instanceof nlobjError) 
						{	
							wmsE=ex.getCode() + '\n' + ex.getDetails();
							exCode=ex.getCode();
						}
						else
						{
							wmsE=ex.toString();
							exCode=ex.toString();
						}  
						/*var exceptionname='RF Inventory Move';
						var functionality='InvMove';
						var trantype=3;
						var vInvRecId=vid;
						var vcontainerLp=NewItemLP;
						nlapiLogExecution('ERROR', 'DetailsError', functionality);	
						nlapiLogExecution('ERROR', 'vcontainerLp', vcontainerLp);
						nlapiLogExecution('ERROR', 'vInvRecId', vid);
						var reference3=MoveQty;
						var reference4 =ItemId;
						var reference5 ="";
						var userId = nlapiGetUser();
						InsertExceptionLog(exceptionname,trantype, functionality, wmsE, vid, vcontainerLp,reference3,reference4,reference5, userId);*/
						nlapiLogExecution('ERROR', 'Exception in RF Inv move : ', wmsE);

						//response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);



						if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
						{
							scount=scount+1;
							continue LABL1;
						}
						else break LABL1;
					}
				}







				var arrDims = getSKUCubeAndWeight(ItemId);           

				var itemCube = 0;
				nlapiLogExecution('ERROR', 'arrDims[0]', arrDims[0]);
				nlapiLogExecution('ERROR', 'arrDims[1]', arrDims[1]);
				//arrDims["BaseUOMItemCube"] =arrDims[0];
				//arrDims["BaseUOMQty"]=arrDims[1];
				if (arrDims[0] != "" && (!isNaN(arrDims[0]))) 
				{
					nlapiLogExecution('ERROR', 'EnterItemcube', NewBinLocationId);
					/*
					 * Item Cube Formula : ItemCube = ((TotalQty/BaseUOMQty)*BaseUOMCube)
					 */
					var uomqty = ((parseFloat(MoveQty))/(parseFloat(arrDims[1])));	
					nlapiLogExecution('ERROR', 'uomqty', uomqty);
					itemCube = (parseFloat(uomqty) * parseFloat(arrDims[0]));
					nlapiLogExecution('ERROR', 'itemCube1', itemCube);
				} else 
				{
					itemCube = 0;
				} 
				nlapiLogExecution('ERROR', 'BinLocationId', BinLocationId);

				//	var oldLocRemCube = GeteLocCube(BinLocationId);           

				//	var oldremcube = (parseFloat(oldLocRemCube) + parseFloat(itemCube));			
				//	oldLocRemCube = parseFloat(oldremcube);

				//	if (BinLocationId != null && BinLocationId != "") 
				//	{            		        		
				//		var retValue = UpdateLocCube(BinLocationId, oldLocRemCube);				        	 
				//	}
				nlapiLogExecution('ERROR', 'New Bin Loc ID', NewBinLocationId);
				//below code is commented because  binlocation remaining cube is updating through userevent
			/*	var newLocRemCube = GeteLocCube(NewBinLocationId);
				nlapiLogExecution('ERROR', 'newLocRemCube', newLocRemCube);
				nlapiLogExecution('ERROR', 'itemCube', itemCube);

				if (parseFloat(newLocRemCube) > parseFloat(itemCube)) 
				{
					var newremcube = parseFloat(newLocRemCube) - parseFloat(itemCube);
					newLocRemCube = parseFloat(newremcube);

					if (NewBinLocationId != null && NewBinLocationId != "") 
					{
						var retValue = UpdateLocCube(NewBinLocationId, newLocRemCube);				
					}
				}*/
			}
			catch(exp)
			{
				nlapiLogExecution('ERROR','exception in RF Inventory move',exp.message);
				nlapiSubmitField('customrecord_ebiznet_trn_opentask', opentaskid, 'custrecord_notes', exp);
				InsertExceptionLog('Inventory Move',2, 'Inventory Move', exp, null, null,null,null,null, nlapiGetUser());
			}
			/*finally
			{
				//Unlocking Sales order
				nlapiLogExecution('ERROR','Inventory internal id before unlocking in finally',vid);


				if(vid != null && vid != '')
				{	
					var ExternalIdFilters = new Array();
					ExternalIdFilters.push(new nlobjSearchFilter('name', null, 'is', vid));
					ExternalIdFilters.push(new nlobjSearchFilter('custrecord_ebiznet_trantype', null, 'is', 'rfinvmove'));
					var searchresultsExt = nlapiSearchRecord('customrecord_ebiznet_lockrecs', null, ExternalIdFilters, null);
					if(searchresultsExt != null && searchresultsExt != '')
					{
						nlapiLogExecution('ERROR','Recordid before unlocking',searchresultsExt[0].getId());
						nlapiDeleteRecord('customrecord_ebiznet_lockrecs',searchresultsExt[0].getId());
						nlapiLogExecution('ERROR','Unlocked successfully');
					}	
				}
			}*/

       // case # 20127701 parameter itemLP
			//case# 20148325 starts
			if(totqtymoved=="Y")
			{
				UpdateSerialNumLpForInvtMerge(ItemId,ItemLP,invExistLp);
			}
			//case# 20148325 ends
		}
		else
		{
			var retLprec =	InsertLP(NewItemLP);
			var CreateInventoryRecord = nlapiCreateRecord('customrecord_ebiznet_createinv');
			nlapiLogExecution('ERROR', 'Create Inventory Move Record', 'Inventory Record');

			CreateInventoryRecord.setFieldValue('name', 'Inventory Move');
			CreateInventoryRecord.setFieldValue('custrecord_wms_inv_status_flag',19); //Status - STORAGE

			nlapiLogExecution('ERROR', 'IN  Create Inventory Record: ItemId', ItemId);
			nlapiLogExecution('ERROR', 'IN  Create Inventory Record: Item', Item);
			var MoveQtyTemp='1';
			nlapiLogExecution('ERROR', 'IN  Create Inventory Record: MoveQtyTemp', MoveQtyTemp);
			//Added for Item name and desc
			CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_sku', ItemId);
			CreateInventoryRecord.setFieldValue('custrecord_inv_ebizsku_no', ItemId);
			CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(MoveQty).toFixed(4));
			//var vNewMovQty=MoveQty;
			nlapiLogExecution('ERROR', 'IN  Create Inventory Record: custrecord_ebiz_avl_qty', parseFloat(MoveQty).toFixed(4));
			CreateInventoryRecord.setFieldValue('custrecord_ebiz_avl_qty', parseFloat(MoveQty).toFixed(4));
			nlapiLogExecution('ERROR', 'IN  Create Inventory Record: custrecord_ebiz_avl_qty', parseFloat(MoveQty).toFixed(4));

			CreateInventoryRecord.setFieldValue('custrecord_ebiz_qoh', parseFloat(MoveQty).toFixed(4));
			nlapiLogExecution('ERROR', 'IN  Create Inventory Record: custrecord_ebiz_qoh', parseFloat(MoveQty).toFixed(4));
			CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_packcode', 1);
			CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_sku_status', ItemStatusId);
			CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_lot', LOTNoId);
			nlapiLogExecution('ERROR', 'fifodate', fifodate);
			if(fifodate!=null && fifodate!='')
				CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_fifo', fifodate);
			/*else
			{
				CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());
			}*/
			nlapiLogExecution('ERROR', 'locationid', locationid);
			CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_loc', locationid);//Inventory Location);
			CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_lp', NewItemLP);//Item LP #);
			if(NewItemLP==null)
			{
				CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_lp', ItemLP);
			}

			nlapiLogExecution('ERROR', 'NewBinLocationId', NewBinLocationId);
			if(NewBinLocationId!=null && NewBinLocationId!='')
			{
				nlapiLogExecution('ERROR', 'NewBinLocationId', NewBinLocationId);
				CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_binloc', NewBinLocationId);//Bin Location);
			}
			CreateInventoryRecord.setFieldValue('custrecord_ebiz_callinv', 'N');//Bin Location);
			CreateInventoryRecord.setFieldValue('custrecord_ebiz_displayfield', 'N');//Bin Location);
			CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_packcode', getPackCode);
			CreateInventoryRecord.setFieldValue('custrecord_invttasktype', vTaskType);
			CreateInventoryRecord.setFieldValue('custrecord_ebiz_itemdesc', ItemDesc);
			nlapiLogExecution('ERROR', 'compId', compId);
			if(compId!=null && compId!='')
			{
				CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_company', compId);
			}
			if(expdate!=null && expdate!='')
			{
				CreateInventoryRecord.setFieldValue('custrecord_ebiz_expdate', expdate);
			}
			nlapiLogExecution('ERROR', 'custrecord_status_flag', '19');
			nlapiLogExecution('ERROR', 'Submitting Create Inventory Record', CreateInventoryRecord);

			//commit the record to NetSuite
			invtrecid = nlapiSubmitRecord(CreateInventoryRecord);
			nlapiLogExecution('ERROR', 'CreateInventoryRecordId', 'test');
			nlapiLogExecution('ERROR', 'CreateInventoryRecordId', invtrecid);

			nlapiLogExecution('ERROR', 'Inventory Move Creation', 'Success');

			var arrDims = getSKUCubeAndWeight(ItemId);           

			var itemCube = 0;
			nlapiLogExecution('ERROR', 'arrDims[0]', arrDims[0]);
			nlapiLogExecution('ERROR', 'arrDims[1]', arrDims[1]);
			//arrDims["BaseUOMItemCube"] =arrDims[0];
			//arrDims["BaseUOMQty"]=arrDims[1];
			if (arrDims[0] != "" && (!isNaN(arrDims[0]))) 
			{
				/*
				 * Item Cube Formula : ItemCube = ((TotalQty/BaseUOMQty)*BaseUOMCube)
				 */
				var uomqty = ((parseFloat(MoveQty))/(parseFloat(arrDims[1])));			
				itemCube = (parseFloat(uomqty) * parseFloat(arrDims[0]));
			} else 
			{
				itemCube = 0;
			}           

			//var oldLocRemCube = GeteLocCube(BinLocationId);           

			//var oldremcube = (parseFloat(oldLocRemCube) + parseFloat(itemCube));			
			//oldLocRemCube = parseFloat(oldremcube);

			//if (BinLocationId != null && BinLocationId != "") 
			//{            		        		
			//	var retValue = UpdateLocCube(BinLocationId, oldLocRemCube);				        	 
			//}
			nlapiLogExecution('ERROR', 'New Bin Loc ID', NewBinLocationId);
			//below code is commented because  binlocation remaining cube is updating through userevent
			/*var newLocRemCube = GeteLocCube(NewBinLocationId);
			nlapiLogExecution('ERROR', 'newLocRemCube', newLocRemCube);
			nlapiLogExecution('ERROR', 'itemCube', itemCube);

			if (parseFloat(newLocRemCube) > parseFloat(itemCube)) 
			{
				var newremcube = parseFloat(newLocRemCube) - parseFloat(itemCube);
				newLocRemCube = parseFloat(newremcube);

				if (NewBinLocationId != null && NewBinLocationId != "") 
				{
					var retValue = UpdateLocCube(NewBinLocationId, newLocRemCube);				
				}
			}*/
			nlapiLogExecution('ERROR', 'Out of CreateInventory', '');
		}
	}
	else
	{
		var retLprec =	InsertLP(NewItemLP);
		var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');
		nlapiLogExecution('ERROR', 'Creating Inventory Record', 'INVT');
		invtRec.setFieldValue('name', 'Inventory Move');
		invtRec.setFieldValue('custrecord_ebiz_inv_binloc', NewBinLocationId);
		invtRec.setFieldValue('custrecord_ebiz_inv_lp', NewItemLP);//Item LP #);
		if(NewItemLP==null)
		{
			invtRec.setFieldValue('custrecord_ebiz_inv_lp', ItemLP);
		}
		invtRec.setFieldValue('custrecord_ebiz_inv_sku', ItemId);
		invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(MoveQty).toFixed(4));
		invtRec.setFieldValue('custrecord_inv_ebizsku_no', ItemId);
		invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(MoveQty).toFixed(4));
		invtRec.setFieldValue('custrecord_ebiz_inv_loc', locationid);
		invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', ItemStatusId);
		invtRec.setFieldValue('custrecord_ebiz_inv_packcode', getPackCode);
		invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
		invtRec.setFieldValue('custrecord_ebiz_displayfield','N');				
		invtRec.setFieldValue('custrecord_wms_inv_status_flag',19);	//Storage
		nlapiLogExecution('ERROR', 'Before Submitting inventory LotBatch', LOTNoId);
		invtRec.setFieldValue('custrecord_ebiz_inv_lot', LOTNoId);
		if(fifodate!=null && fifodate!='')
			invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifodate);
		invtRec.setFieldValue('custrecord_invttasktype', vTaskType);
		invtRec.setFieldValue('custrecord_ebiz_inv_adjusttype', adjustmenttype);
		invtRec.setFieldValue('custrecord_ebiz_itemdesc', ItemDesc);
		nlapiLogExecution('ERROR', 'compId', compId);
		if(compId!=null && compId!='')
		{
			invtRec.setFieldValue('custrecord_ebiz_inv_company', compId);
		}
		if(expdate!=null && expdate!='')
		{
			invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
		}

		nlapiLogExecution('ERROR', 'Before Submitting inventory recid', 'Inventory Records');
		invtrecid = nlapiSubmitRecord(invtRec, false, true);
	}

	return invtrecid;
}

function createOpenTaskAndInventoryRecord(ItemId, Item, ItemStatusId,ItemStatus, UOMId, UOM, LOTNo, LOTNoId, ItemLP, NewItemLP, 
		BinLocation, BinLocationId, NewBinLocation, NewBinLocationId, getPackCode, getPackCodeId, TotQuantity, AvailQty, MoveQty, 
		ActualEndDate, ActualEndTime, ActualBeginDate, ActualBeginTime, getInvtRecID,locationid,getNewStatus,adjustmenttype,
		ItemDesc,compId,fifodate,getActualBeginTimeAMPM,putmethodID)
{
	nlapiLogExecution('ERROR', 'Into createOpenTaskAndInventoryRecord', putmethodID);

	try{
		//a Date object to be used for a random value
		var now = new Date();
		//now= now.getHours();
		//Getting time in hh:mm tt format.
		var a_p = "";
		var d = new Date();
		var curr_hour = now.getHours();
		if (curr_hour < 12) {
			a_p = "am";
		}
		else {
			a_p = "pm";
		}
		if (curr_hour == 0) {
			curr_hour = 12;
		}
		if (curr_hour > 12) {
			curr_hour = curr_hour - 12;
		}

		var curr_min = now.getMinutes();

		curr_min = curr_min + "";

		if (curr_min.length == 1) {
			curr_min = "0" + curr_min;
		}
		nlapiLogExecution("ERROR", "Time of Day ", (curr_hour + " : " + curr_min + " " + a_p));


		var invtrecid=-1;
		var CallFlag='N';
		var OldMakeWHFlag='Y';
		var NewMakeWHFlag='Y';
		var vNewWHlocatin;
		var vOldWHlocatin;
		var vNSFlag;
		var UpdateFlag;
		var vNSCallFlag='N';
		nlapiLogExecution('ERROR', 'ItemStatusId',ItemStatusId);
		nlapiLogExecution('ERROR', 'getNewStatus',getNewStatus);
		if(ItemStatusId==getNewStatus)
			CallFlag='N';
		else
		{
			vNSFlag=GetNSCallFlag(ItemStatusId,getNewStatus);
			nlapiLogExecution('ERROR', 'vNSFlag',vNSFlag);

			if(vNSFlag!= null && vNSFlag!= "" && vNSFlag.length>0)
			{
				CallFlag=vNSFlag[0];
				nlapiLogExecution('ERROR', 'vNSFlag[0]',vNSFlag[0]);
			}
			if(vNSFlag[0]=='N')
				CallFlag='N';
			else if(vNSFlag[0]=='Y') 
			{ 
				OldMakeWHFlag= GetMWFlag(vNSFlag[1]);
				NewMakeWHFlag= GetMWFlag(vNSFlag[2]);
				vNewWHlocatin=vNSFlag[2]; 
				vOldWHlocatin=vNSFlag[1]; 
			}


		}
		var vTaskType='9';
		if(CallFlag=='N')
			vTaskType='9';
		else
			vTaskType='18';
		nlapiLogExecution('ERROR', 'CallFlag',CallFlag);

		nlapiLogExecution('ERROR', 'Done MOVE Record Insertion In Create Inventory:', getInvtRecID);

		if(NewMakeWHFlag=='Y')
		{
			/*if(vNewWHlocatin!=null && vNewWHlocatin!='')
			locationid=vNewWHlocatin;*/
		if((locationid == null || locationid == '') && vNewWHlocatin!=null && vNewWHlocatin!='')
			locationid=vNewWHlocatin;
			invtrecid = CreateInventory(ItemId, Item, ItemStatusId,ItemStatus, UOMId, UOM, LOTNo, LOTNoId, ItemLP, NewItemLP, BinLocation, 
					BinLocationId, NewBinLocation, NewBinLocationId, getPackCode, getPackCodeId, TotQuantity, AvailQty, MoveQty, 
					ActualEndDate, ActualEndTime, ActualBeginDate, ActualBeginTime, getInvtRecID,locationid,getNewStatus,adjustmenttype,
					vTaskType,CallFlag,ItemDesc,compId,fifodate,null,putmethodID);
			if(invtrecid=='ERROR' || invtrecid==-1)
				return 'ERROR';

		}
		else // case no 201411003
		{
			invtrecid = 1;

		}
		nlapiLogExecution('ERROR', 'Done MOVE Record Insertion In Create Inventory:', getInvtRecID);

		nlapiLogExecution('ERROR', 'vTaskType',vTaskType);

		//added code on 060912 by suman
		//This will get the max movereport sequence no from WAVE record table.
		var reportno = GetMaxTransactionNo('MOVEREPORT');
		nlapiLogExecution('ERROR', 'reportno', reportno);


		var opentaskrec = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');

		opentaskrec.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		opentaskrec.setFieldValue('custrecord_act_qty', parseFloat(MoveQty).toFixed(4));//Assigning new quantity
		opentaskrec.setFieldValue('custrecord_lpno', NewItemLP);
		opentaskrec.setFieldValue('custrecord_ebiz_lpno', NewItemLP);
		if(vTaskType!=null && vTaskType!='')
			opentaskrec.setFieldValue('custrecord_tasktype', vTaskType);
		opentaskrec.setFieldValue('custrecord_actbeginloc', BinLocationId);
		opentaskrec.setFieldValue('custrecord_actendloc', NewBinLocationId);	
		opentaskrec.setFieldValue('custrecord_sku', ItemId);
		opentaskrec.setFieldValue('custrecord_skudesc', Item);
		opentaskrec.setFieldValue('custrecordact_begin_date', ActualBeginDate);
		opentaskrec.setFieldValue('custrecord_act_end_date', ActualEndDate);
		opentaskrec.setFieldValue('custrecord_packcode', getPackCode);
		opentaskrec.setFieldValue('custrecord_wms_status_flag', 19);
		opentaskrec.setFieldValue('custrecord_batch_no', LOTNo);
		opentaskrec.setFieldValue('custrecord_upd_ebiz_user_no', nlapiGetContext().getUser());
		opentaskrec.setFieldValue('custrecord_taskassignedto', nlapiGetContext().getUser());
		//	opentaskrec.setFieldValue('custrecord_ebiz_report_no', reportno.toString());
		//added on 111212 by suman
		//Adding fields to update time zones.
		opentaskrec.setFieldValue('custrecord_actualbegintime', TimeStamp());
		opentaskrec.setFieldValue('custrecord_actualendtime', TimeStamp());
		opentaskrec.setFieldValue('custrecord_recordtime', TimeStamp());
		opentaskrec.setFieldValue('custrecord_recordupdatetime', TimeStamp());
		opentaskrec.setFieldValue('custrecord_current_date', ActualEndDate);
		opentaskrec.setFieldValue('custrecord_upd_date', ActualEndDate);
		opentaskrec.setFieldValue('custrecord_ebiz_new_lp', NewItemLP);
		opentaskrec.setFieldValue('custrecord_from_lp_no', ItemLP);
		if(locationid!=null&&locationid!="")
			opentaskrec.setFieldValue('custrecord_wms_location', locationid);
		opentaskrec.setFieldValue('custrecord_ebiz_report_no', reportno.toString());
		//end of code as of 111212.
		opentaskrec.setFieldValue('custrecord_expe_qty', AvailQty);
		opentaskrec.setFieldValue('custrecord_sku_status', ItemStatusId);
		opentaskrec.setFieldValue('custrecord_ebiz_new_sku_status', getNewStatus);
		opentaskrec.setFieldValue('custrecord_skudesc', ItemDesc);
		opentaskrec.setFieldValue('custrecord_ebiz_new_lp', NewItemLP);
		opentaskrec.setFieldValue('custrecord_from_lp_no', ItemLP);	
		opentaskrec.setFieldValue('custrecord_wms_location', locationid);
		nlapiLogExecution('ERROR', 'AvailQty', 'AvailQty');
		nlapiLogExecution('ERROR', 'ItemStatusId', 'ItemStatusId');
		nlapiLogExecution('ERROR', 'getNewStatus', 'getNewStatus');
		nlapiLogExecution('ERROR', 'ItemDesc', 'ItemDesc');
		nlapiLogExecution('ERROR', 'NewItemLP', 'NewItemLP');
		nlapiLogExecution('ERROR', 'ItemLP', 'ItemLP');
		nlapiLogExecution('ERROR', 'locationid', 'locationid');

		nlapiLogExecution('DEBUG', 'Submitting MOVE record', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(opentaskrec, false, true);
		nlapiLogExecution('ERROR', 'Done MOVE Record Insertion in OpenTask:', 'Success');


		nlapiLogExecution('ERROR', 'CallFlag:', CallFlag);

		if(CallFlag=='Y')//To trigger NS automatically
		{
			var invmergeLP = '';
			var itemSubtype = nlapiLookupField('item', ItemId, ['recordType']);
			nlapiLogExecution('ERROR', 'itemSubtype.recordType =', itemSubtype.recordType);
			if (itemSubtype.recordType == 'serializedinventoryitem'|| itemSubtype.recordType == "serializedassemblyitem") 
			{
				var invLoad = nlapiLoadRecord('customrecord_ebiznet_createinv', invtrecid);
				invmergeLP = invLoad.getFieldValue('custrecord_ebiz_inv_lp');

				if(NewItemLP == "" || NewItemLP == null)
				{
					NewItemLP = ItemLP;
				}

				nlapiLogExecution('ERROR', 'NewItemLP new:', NewItemLP);
				nlapiLogExecution('ERROR', 'invmergeLP new:', invmergeLP);

				var serialsspaces = "";
				var filters= new Array();			
				filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', ItemId);
				//filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', NewItemLP);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invmergeLP);
				filters[2] = new nlobjSearchFilter('custrecord_serialbinlocation', null, 'is', NewBinLocationId);
				filters[3] = new nlobjSearchFilter('custrecord_serial_lpno', null, 'is', NewItemLP);

				var column =new Array(); 

				column[0] = new nlobjSearchColumn('custrecord_serialnumber');							

				var searchresultser = nlapiSearchRecord('customrecord_ebiznetserialentry',null,filters,column);
				if(searchresultser!=null && searchresultser!='')
				{
					for ( var b = 0; b < searchresultser.length; b++ ) {
						if(serialsspaces==null || serialsspaces=='')
						{
							serialsspaces=searchresultser[b].getValue('custrecord_serialnumber');
						}
						else
						{
							serialsspaces += " " + searchresultser[b].getValue('custrecord_serialnumber');
						}
					}
				}
				LOTNo = serialsspaces;
			}
			else if (itemSubtype.recordType != "lotnumberedinventoryitem" && itemSubtype.recordType != "lotnumberedassemblyitem" ) 
			{	//no need to send lot# to NS,if item is not Lotnumbered item in NS
			
				LOTNo='';
			}
			InvokeNSInventoryTransfer(ItemId,ItemStatus,vOldWHlocatin,vNewWHlocatin,MoveQty,LOTNo);

			//Code for fetching account no from stockadjustment custom record	
			/*	

			 * This part of code is commented as per inputs from NS team
			 * For all vitual location inventory move NS team suggested us to us Inventory Transfer object instead of inventory adjustment object
			 * Averate cost/price of the item will not change if we use Inventory Transfer object
			var AccountNo = getStockAdjustmentAccountNoNew(adjustmenttype);

			if(AccountNo !=null && AccountNo != "" && AccountNo.length>0)
			{
				var FromAccNo=AccountNo[0];
				var ToAccNo=AccountNo[1];
				nlapiLogExecution('ERROR', 'vOldWHlocatin',vOldWHlocatin);
				nlapiLogExecution('ERROR', 'vNewWHlocatin',vNewWHlocatin);

				nlapiLogExecution('ERROR', 'FromAccNo',FromAccNo);
				nlapiLogExecution('ERROR', 'ToAccNo',ToAccNo);
				InvokeNSInventoryAdjustmentNew(ItemId,ItemStatus,vOldWHlocatin,-(parseFloat(MoveQty)),"",vTaskType,adjustmenttype,LOTNo,FromAccNo);
				InvokeNSInventoryAdjustmentNew(ItemId,getNewStatus,vNewWHlocatin,MoveQty,"",vTaskType,adjustmenttype,LOTNo,ToAccNo);							 
			} */
		}
	}
	catch (exp) {
		if (exp instanceof nlobjError) 
			nlapiLogExecution('ERROR', 'system error', exp.getCode() + '\n' + exp.getDetails());
		else 
			nlapiLogExecution('ERROR', 'unexpected error', exp.toString());

		/*if(opentaskrecid!=null && opentaskrecid!='')
			nlapiSubmitField('customrecord_ebiznet_trn_opentask', opentaskrecid, 'custrecord_notes', exp);*/

		InsertExceptionLog('Inventory Move',2, 'Inventory Move', exp, null, null,null,null,null, nlapiGetUser());
	}

	nlapiLogExecution('ERROR', 'Out of createOpenTaskAndInventoryRecord', '');

	return invtrecid;
}

function getStockAdjustmentAccountNoNew(adjusttype)
{	
	var StAdjustmentAccountNo=new Array();
	var filterStAccNo = new Array();
	var colsStAccNo = new Array();

	if(adjusttype!=null && adjusttype!="")
		filterStAccNo.push(new nlobjSearchFilter('internalid',null,'is',adjusttype));

	filterStAccNo.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	colsStAccNo[0] = new nlobjSearchColumn('custrecord_ebiz_mapto_ns_glaccount');
	colsStAccNo[1] = new nlobjSearchColumn('custrecord_ebiz_to_ns_glaccount');

	var StockAdjustAccResults = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, filterStAccNo, colsStAccNo);
	if(StockAdjustAccResults!=null && StockAdjustAccResults != "" && StockAdjustAccResults.length>0)
	{
		StAdjustmentAccountNo.push(StockAdjustAccResults[0].getValue('custrecord_ebiz_mapto_ns_glaccount'));
		StAdjustmentAccountNo.push(StockAdjustAccResults[0].getValue('custrecord_ebiz_to_ns_glaccount'));
	}

	return StAdjustmentAccountNo;	
}

/**
 * Passing item,item status, qty and location to NS
 * 
 * @param item
 * @param itemstatus
 * @param loc
 * @param qty
 * @param notes
 */
function  InvokeNSInventoryAdjustmentNew(item,itemstatus,loc,qty,notes,tasktype,adjusttype,lot,AccNo)
{
	try{
		nlapiLogExecution('ERROR', 'Location info::', loc);
		nlapiLogExecution('ERROR', 'Task Type::', tasktype);
		nlapiLogExecution('ERROR', 'Adjustment Type::', adjusttype);
		nlapiLogExecution('ERROR', 'qty::', qty);
		nlapiLogExecution('ERROR', 'AccNo::', AccNo);

		var confirmLotToNS='N';
		confirmLotToNS=GetConfirmLotToNS(loc);
		var vItemname;

		var filters = new Array();          
		filters.push(new nlobjSearchFilter('internalid', null, 'is',item));
		//Changed on 30/4/12 by suman
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		//End of changes as on 30/4/12
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('cost');
		columns[1] = new nlobjSearchColumn('averagecost');
		columns[2] = new nlobjSearchColumn('itemid');

		var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
		if (itemdetails !=null) 
		{
			vItemname=itemdetails[0].getValue('itemid');
			vCost = itemdetails[0].getValue('cost');
			nlapiLogExecution('ERROR', 'vCost', vCost);
			vAvgCost = itemdetails[0].getValue('averagecost');
			nlapiLogExecution('ERROR', 'Average Cost', vAvgCost);	         
		}		

		var outAdj = nlapiCreateRecord('inventoryadjustment');			
		outAdj.setFieldValue('account', AccNo);
		outAdj.setFieldValue('memo', notes);
		outAdj.setFieldValue('adjlocation', loc);
		outAdj.insertLineItem('inventory', 1);
		outAdj.setLineItemValue('inventory', 'item', 1, item);
		outAdj.setLineItemValue('inventory', 'location', 1, loc);	
		outAdj.setLineItemValue('inventory', 'adjustqtyby', 1, parseFloat(qty));
		if(vAvgCost != null &&  vAvgCost != "")	
		{
			nlapiLogExecution('ERROR', 'into if cond vAvgCost', vAvgCost);
			outAdj.setLineItemValue('inventory', 'unitcost', 1, vAvgCost);
		}
		else
		{
			nlapiLogExecution('ERROR', 'into else cond.unit cost', vCost);
			outAdj.setLineItemValue('inventory', 'unitcost', 1, vCost);
		}
		//outAdj.setFieldValue('subsidiary', 4);

		//Added by Sudheer on 093011 to send lot# for inventory posting

		if(lot!=null && lot!='')
		{
			if(parseFloat(qty)<0)
				qty=parseFloat(qty)*(-1);

			if(confirmLotToNS=='N')
				lot=vItemname;	

			nlapiLogExecution('ERROR', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");
			outAdj.setLineItemValue('inventory', 'serialnumbers', 1, lot + "(" + parseFloat(qty) + ")");
		}

		nlapiSubmitRecord(outAdj, true, true);
		nlapiLogExecution('ERROR', 'type argument', 'type is create');
	}


	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in InvokeNSInventoryAdjustmentNew ', exp);	

	}
}

function  InvokeNSInventoryTransfer(item,itemstatus,loc,toloc,qty,lot)
{
	try{
		nlapiLogExecution('ERROR', 'Into InvokeNSInventoryTransfer');		
		nlapiLogExecution('ERROR', 'item', item);		
		nlapiLogExecution('ERROR', 'itemstatus', itemstatus);
		nlapiLogExecution('ERROR', 'loc', loc);
		nlapiLogExecution('ERROR', 'toloc', toloc);
		nlapiLogExecution('ERROR', 'qty', qty);
		nlapiLogExecution('ERROR', 'lot', lot);

		var ItemType='';

		if(item!= null && item != "")
		{
			var columns = nlapiLookupField('item', item, [ 'recordType' ]);
			ItemType = columns.recordType;
		}

		nlapiLogExecution('ERROR', 'ItemType', ItemType);
		var confirmLotToNS='N';
		confirmLotToNS=GetConfirmLotToNS(loc);
		var vItemname;	

		var filters = new Array();          
		filters.push(new nlobjSearchFilter('internalid', null, 'is',item));
		//Changed on 30/4/12 by suman
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		//End of changes as on 30/4/12
		var columns = new Array();		
		columns[0] = new nlobjSearchColumn('itemid');
		var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
		if (itemdetails !=null) 
		{
			vItemname=itemdetails[0].getValue('itemid');
		}

		var invttransfer = nlapiCreateRecord('inventorytransfer');			

		invttransfer.setFieldValue('location', loc);//from Location
		invttransfer.setFieldValue('transferlocation', toloc);
		invttransfer.selectNewLineItem('inventory');
		invttransfer.setCurrentLineItemValue('inventory', 'item', item);		
		invttransfer.setCurrentLineItemValue('inventory', 'adjustqtyby', parseFloat(qty));

		var subs = nlapiGetContext().getFeature('subsidiaries');
		nlapiLogExecution('ERROR', 'subs', subs);
		if(subs==true)
		{
			var vSubsidiaryVal=getSubsidiaryNew(loc);
			nlapiLogExecution('ERROR', 'vSubsidiaryVal', vSubsidiaryVal);
			if(vSubsidiaryVal != null && vSubsidiaryVal != '')
				invttransfer.setFieldValue('subsidiary', vSubsidiaryVal);			
		}


		if(lot!=null && lot!='')
		{
			//vItemname=vItemname.replace(" ","-");
			vItemname=vItemname.replace(/ /g,"-");
			if(parseFloat(qty)<0)
				qty=parseFloat(qty)*(-1);
			if(confirmLotToNS=='N')
				lot=vItemname;	

			//For advanced Bin serial Lot management check
			var vAdvBinManagement=false;
			var ctx = nlapiGetContext();
			if(ctx != null && ctx != '')
			{
				if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
					vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
			}  
			nlapiLogExecution('ERROR', 'vAdvBinManagement', vAdvBinManagement);

			if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
			{
				// case no 20126516
				/*var compSubRecord = invttransfer.createCurrentLineItemSubrecord('inventory','inventorydetail');
				nlapiLogExecution('ERROR', 'test1', 'test1');
				compSubRecord.selectNewLineItem('inventoryassignment');
				compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', qty);
				//compSubRecord.setCurrentLineItemValue('inventoryassignment', 'binnumber', vSubsidiaryVal);
				compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lot);

				compSubRecord.commitLineItem('inventoryassignment');
				compSubRecord.commit();*/

				// case no 20126516
				var templot = lot.split(' ');

				nlapiLogExecution('ERROR', 'templot', templot.length);

				var compSubRecord = invttransfer.createCurrentLineItemSubrecord('inventory','inventorydetail');
				if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem") 
				{
					for(var t=0; t< templot.length; t++)
					{
						nlapiLogExecution('ERROR', 'test11', templot[t]);
						compSubRecord.selectNewLineItem('inventoryassignment');
						compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
						//compSubRecord.setCurrentLineItemValue('inventoryassignment', 'binnumber', vSubsidiaryVal);
						compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', templot[t]);

						compSubRecord.commitLineItem('inventoryassignment');
					}
				}
				else
				{
					nlapiLogExecution('ERROR', 'test1', 'test1');
					compSubRecord.selectNewLineItem('inventoryassignment');
					compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', qty);
					//compSubRecord.setCurrentLineItemValue('inventoryassignment', 'binnumber', vSubsidiaryVal);
					compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lot);

					compSubRecord.commitLineItem('inventoryassignment');
				}

				compSubRecord.commit();
			}
			else
			{
				if(parseFloat(qty)<0)
					qty=parseFloat(qty)*(-1);

				if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem") 
				{
					nlapiLogExecution('ERROR', 'LotNowithQty', lot);
					invttransfer.setCurrentLineItemValue('inventory', 'serialnumbers',  lot);
				}	
				else
				{
					nlapiLogExecution('ERROR', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");
					invttransfer.setCurrentLineItemValue('inventory', 'serialnumbers', lot + "(" + parseFloat(qty) + ")");
				}
			}
		}

		invttransfer.commitLineItem('inventory'); 
		nlapiSubmitRecord(invttransfer, true, true);
		nlapiLogExecution('ERROR', 'type argument', 'type is create');
	}


	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in InvokeNSInventoryTransferNew ', exp);	

	}
}

function InsertLP(ItemNewLP)
{
	try {
		nlapiLogExecution('ERROR', 'INTO MASTER LP INSERTION');
		var filtersmlp = new Array();
		filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', ItemNewLP);

		var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

		if (SrchRecord != null && SrchRecord.length > 0) 
		{
			nlapiLogExecution('ERROR', 'LP FOUND');

			lpExists = 'Y';
		}
		else 
		{
			nlapiLogExecution('ERROR', 'LP NOT FOUND');
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
			customrecord.setFieldValue('name', ItemNewLP);
			customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', ItemNewLP);
			var rec = nlapiSubmitRecord(customrecord, false, true);
		}
	} 
	catch (e) 
	{
		nlapiLogExecution('ERROR', 'Failed to Update/Insert into Master LP Record');
	}
}

function GetNSCallFlag(Status,NewStatus)
{
	nlapiLogExecution('ERROR', 'Into GetNSCallFlag', '');
	nlapiLogExecution('ERROR', 'Status', Status);
	nlapiLogExecution('ERROR', 'New Status', NewStatus);
	var result=new Array();
	var retFlag='N';
	var OldMapLocation;
	var NewMapLocation;
	if(Status!=null && Status!="")
	{
		var OldIStatus = nlapiLoadRecord('customrecord_ebiznet_sku_status', Status);
		if(OldIStatus!= null && OldIStatus != "")
		{
			OldMapLocation=OldIStatus.getFieldValue('custrecord_item_status_location_map');
			OldIStatus=null;
		} 
	}
	if(NewStatus!=null && NewStatus!="")
	{
		var NewIStatus = nlapiLoadRecord('customrecord_ebiznet_sku_status', NewStatus);
		if(NewIStatus!= null && NewIStatus != "")
		{
			NewMapLocation=NewIStatus.getFieldValue('custrecord_item_status_location_map');
		} 
	}
	if(OldMapLocation != null && OldMapLocation !="" && NewMapLocation != null && NewMapLocation != "")
	{
		if(OldMapLocation==NewMapLocation)
		{
			retFlag='N';
			result.push('N');
		}
		else
		{
			result.push('Y');
		}
		result.push(OldMapLocation);
		result.push(NewMapLocation);
	}
	nlapiLogExecution('ERROR', 'retFlag', retFlag);
	nlapiLogExecution('ERROR', 'OldMapLocation', OldMapLocation);
	nlapiLogExecution('ERROR', 'NewMapLocation', NewMapLocation);
	nlapiLogExecution('ERROR', 'Out of GetNSCallFlag', '');
	return result;
}

function GetMWFlag(WHLocation)
{
	nlapiLogExecution('ERROR', 'Into GetMWFlag : WHLocation', WHLocation);
	var retFlag='Y';
	var MWHSiteFlag;

	if(WHLocation!=null && WHLocation!="")
	{
		var filters = new Array();
		filters.push(new nlobjSearchFilter('internalid', null, 'is', WHLocation));
		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_ebizwhsite')); 

		var WHOld = nlapiSearchRecord('location', null, filters, columns);

		if(WHOld!= null && WHOld != "" && WHOld.length>0)
		{
			MWHSiteFlag=WHOld[0].getValue('custrecord_ebizwhsite');
			nlapiLogExecution('ERROR', 'MWHSiteFlag', MWHSiteFlag);
			WHOld=null;
		} 
	} 
	if(MWHSiteFlag != null && MWHSiteFlag !="" )
	{
		if(MWHSiteFlag=='F')
			retFlag='N';
		else
			retFlag='Y';

	}
	nlapiLogExecution('ERROR', 'retFlag', retFlag);
	nlapiLogExecution('ERROR', 'Out of GetMWFlag ', '');
	return retFlag;
}

function TimeStamp(){
	var timestamp;
	var now = new Date();
	var a_p = "";

	//Getting time in hh:mm tt format.
	var curr_hour = now.getHours();
	var curr_min = now.getMinutes();

	// determining the am/pm indicator
	if (curr_hour < 12)
		a_p = "am";
	else
		a_p = "pm";

	// finalizing hours depending on 24hr clock
	if (curr_hour == 0)
		curr_hour = 12;
	else if(curr_hour > 12)
		curr_hour -= 12;

	if (curr_min.length == 1)
		curr_min = "0" + curr_min;

	//Adding fields to update time zones.
	timestamp = curr_hour + ":" + curr_min + " " + a_p;

	return timestamp;
}

/**
 * Function to return the current date in mm/dd/yyyy format
 * @author Phani
 * @returns Current system date
 */
/*function DateStamp(){
	var now = new Date();
	return ((parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
}*/
function fnUpdateCubeOldLoc(ItemId,OldBinLocationId,MoveQty)
{
	var arrDims = getSKUCubeAndWeight(ItemId);           

	var itemCube = 0;
	if (arrDims["BaseUOMItemCube"] != "" && (!isNaN(arrDims["BaseUOMItemCube"]))) 
	{
		nlapiLogExecution('ERROR', 'EnterItemcube', OldBinLocationId);
		/*
		 * Item Cube Formula : ItemCube = ((TotalQty/BaseUOMQty)*BaseUOMCube)
		 */
		var uomqty = ((parseFloat(MoveQty))/(parseFloat(arrDims["BaseUOMQty"])));	
		nlapiLogExecution('ERROR', 'uomqty', uomqty);
		itemCube = (parseFloat(uomqty) * parseFloat(arrDims["BaseUOMItemCube"]));
		nlapiLogExecution('ERROR', 'itemCube1', itemCube);
	} else 
	{
		itemCube = 0;
	} 
	nlapiLogExecution('ERROR', 'OldBinLocationId', OldBinLocationId);

	//	var oldLocRemCube = GeteLocCube(BinLocationId);           

	//	var oldremcube = (parseFloat(oldLocRemCube) + parseFloat(itemCube));			
	//	oldLocRemCube = parseFloat(oldremcube);

	//	if (BinLocationId != null && BinLocationId != "") 
	//	{            		        		
	//		var retValue = UpdateLocCube(BinLocationId, oldLocRemCube);				        	 
	//	}
	nlapiLogExecution('ERROR', 'Old Bin Loc ID', OldBinLocationId);
	var newLocRemCube = GeteLocCube(OldBinLocationId);
	nlapiLogExecution('ERROR', 'newLocRemCube', newLocRemCube);
	nlapiLogExecution('ERROR', 'itemCube', itemCube);

	if (parseFloat(newLocRemCube) > parseFloat(itemCube)) 
	{
		var newremcube = parseFloat(newLocRemCube) - parseFloat(itemCube);
		newLocRemCube = parseFloat(newremcube);

		if (OldBinLocationId != null && OldBinLocationId != "") 
		{
			var retValue = UpdateLocCube(OldBinLocationId, newLocRemCube);				
		}
	}	
}

function getItemStatusList(status)
{
	var itemStatusSearchResult = new Array();
	var itemStatusColumns = new Array();
	var itemStatusFilters = new Array();

	itemStatusFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	itemStatusFilters.push(new nlobjSearchFilter('name', null, 'is', status));

	itemStatusColumns[0] = new nlobjSearchColumn('name');
	itemStatusColumns[1] = new nlobjSearchColumn('internalid');
	itemStatusColumns[1].setSort(false);

	var itemStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);

	return itemStatusSearchResult;

}

function getSubsidiaryNew(location)
{
	if(location != null && location !='')
	{	
		var vSubsidiary;
		var locRec= nlapiLoadRecord('location', location);
		if(locRec != null && locRec != '')
			vSubsidiary=locRec.getFieldValue('subsidiary');
		if(vSubsidiary != null && vSubsidiary != '')
			return vSubsidiary;
		else 
			return null;
	}
	else
		return null;

}


function UpdateSerialNumLpForInvtMerge(ItemId,actlp,InvtLP)
{
	nlapiLogExecution('ERROR', 'into UpdateSerialNumLpForInvtMerge', 'done');

	nlapiLogExecution('ERROR', 'actlp', actlp);
	nlapiLogExecution('ERROR', 'InvtLP', InvtLP);
	nlapiLogExecution('ERROR', 'ItemId', ItemId);

	try {						

		var tempserialId='';
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', ItemId);

		//filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', actlp);
		filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', actlp);
		filters[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'is', ['19']);


		var columns = new Array();

		columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
		var serialnumcsv = "";
		var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
		var tempserialId=new Array();
		if (serchRec !=null && serchRec !='' && serchRec.length>0) {
			for (var n1 = 0; n1 < serchRec.length; n1++) {
				//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
				if (tempserialId == "") {
					tempserialId = serchRec[n1].getId();
					tempserial=serchRec[n1].getValue('custrecord_serialnumber');
				}
				else {
					tempserialId = tempserialId + "," + serchRec[n1].getId();
					tempserial=tempserial+","+serchRec[n1].getValue('custrecord_serialnumber');
				}
			}
		}//end
		nlapiLogExecution('ERROR', 'tempserialId', tempserialId);
		if (tempserialId != null && tempserialId !='') {
			var temSeriIdArr = new Array();
			temSeriIdArr = tempserialId.split(',');
			nlapiLogExecution('ERROR', 'updating Serial num Records to', 'Storage Status');
			for (var p = 0; p < temSeriIdArr.length; p++) {
				nlapiLogExecution('ERROR', 'temSeriIdArr.length ', temSeriIdArr.length);
				var fields = new Array();
				var values = new Array();
				fields[0] = 'custrecord_serialparentid';
				fields[1] = 'custrecord_serialwmsstatus';
				fields[2] = 'custrecord_serial_lpno';
				values[0] = InvtLP;
				values[1] = '3';
				values[2] = actlp;

				nlapiLogExecution('ERROR', 'Inside loop serialId ', temSeriIdArr[p]);
				var updateStatus = nlapiSubmitField('customrecord_ebiznetserialentry', temSeriIdArr[p], fields, values);
				nlapiLogExecution('ERROR', 'Inside loop serialId ', temSeriIdArr[p]);

				nlapiLogExecution('ERROR', 'UpdateSerialNumLpForInvtMerge', 'LP Merge Status Success');
			}
		}
	} 
	catch (exp) {
		nlapiLogExecution('ERROR', 'UpdateSerialNumLpForInvtMerge exeption', exp);
	}

}





function isMergeLP(methodid)
{
	var varMergeLP = 'F';
	var filtersputmethod = new Array();
	if(methodid!=null && methodid!='')
		filtersputmethod.push(new nlobjSearchFilter('internalid', null, 'anyof', methodid));
	filtersputmethod.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var colsputmethod = new Array();
	colsputmethod[0] = new nlobjSearchColumn('custrecord_mergelp');            

	var putwmethodsearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_method', null, filtersputmethod, colsputmethod);

	if(putwmethodsearchresults != null &&putwmethodsearchresults != ''&& putwmethodsearchresults.length > 0)
	{

		varMergeLP = putwmethodsearchresults[0].getValue('custrecord_mergelp');
	}

	return varMergeLP;
}
function getMaxUOMQty(putItemId,putItemPC)
{
	var varPaltQty=0;
	var searchresults = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', putItemId));
	if(putItemPC!=null && putItemPC!='')
		filters.push(new nlobjSearchFilter('custrecord_ebizpackcodeskudim', null, 'is', putItemPC));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizqty');
	columns[1] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim'); 	
	columns[1].setSort(true);
	searchresults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);
	if(searchresults != null && searchresults != '' && searchresults.length > 0)
	{
		varPaltQty = searchresults[0].getValue('custrecord_ebizqty');
	}
	return varPaltQty;
}