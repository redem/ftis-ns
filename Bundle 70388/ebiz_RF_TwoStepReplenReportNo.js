/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_TwoStepReplenReportNo.js,v $
 *   $Revision: 1.1.2.2.2.1 $
 *   $Date: 2015/11/05 22:29:54 $
 *   $Author: rrpulicherla $
 *   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 *   PARAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_TwoStepReplenReportNo.js,v $
 * Revision 1.1.2.2.2.1  2015/11/05 22:29:54  rrpulicherla
 * 2015.2 issues
 *
 * Revision 1.1.2.2  2015/05/05 15:27:24  grao
 * SB issue fixes  201412643  and 201412645
 *
 * Revision 1.1.2.1  2015/01/02 15:05:41  skreddy
 * Case# 201411349
 * Two step replen process
 *
 *****************************************************************************/

function TwoStepReplenReportNo(request, response){
	if (request.getMethod() == 'GET') {

		var html=getScanReportNo(request);        
		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		// This variable is to hold the REPLEN NO# entered.
		var Reparray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		Reparray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR', 'getLanguage', Reparray["custparam_language"]);


		var st8,st9,st10,st11;
		if( getLanguage == 'es_ES')
		{
			st8 = "NO V&#193;LIDA REPOSICI&#211;N";
			st9 = "NO REPLENS DE ESTA ONDA NO";
			st10 = "NO REPLENS para esta selecciï¿½n";
		}
		else
		{
			st8 = "INVALID REPLENISHMENT NO";
			st9 = "NO REPLENS FOR THIS WAVE NO";
			st10= "NO OPEN REPLENS FOR THIS SELECTION";
			st11 = "INVALID ZONE ";
		}

		Reparray["custparam_repno"] = request.getParameter('enterReplenNo');
		Reparray["custparam_zoneno"] = request.getParameter('enterZoneNo');
		Reparray["custparam_error"] = st8;
		Reparray["custparam_waveno"]='';
		Reparray["custparam_entersku"] = '';
		Reparray["custparam_enterpickloc"] = '';
		Reparray["custparam_entertaskpriority"] = '';
		Reparray["custparam_screenno"] = '2stepRptNo';
		
		var reportNo=Reparray["custparam_repno"];
		var waveno=Reparray["custparam_waveno"];
		var getItem = Reparray["custparam_entersku"];
		var getPrimaryloc = Reparray["custparam_enterpickloc"];
		var getPriority = Reparray["custparam_entertaskpriority"];
		var getItemId='';

		var getNumber = request.getParameter('hdngetnumber');
		nlapiLogExecution('Error', 'getNumber', getNumber);
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');
		var optedNext = request.getParameter('cmdNEXT');

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to RF Main Menu.
		if (optedEvent == 'F7') {
			response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenmenu', 'customdeploy_ebiz_twostepreplenmenu', false, Reparray);
		}
		else {
			var waveno;
			var reportNo= request.getParameter('enterReplenNo');
			var zoneNo= request.getParameter('enterZoneNo');
			nlapiLogExecution('Error', 'reportNo', reportNo);
			nlapiLogExecution('Error', 'zoneNo', zoneNo);
			
			if((reportNo == '' || reportNo == null) && (zoneNo == '' || zoneNo == null)){
				Reparray["custparam_error"] = "ENTER REPORT NO AND/OR ZONE";

				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
				return;
				
			}

			if (reportNo != "" || zoneNo!="" ){
				Reparray["custparam_enterrepno"] = reportNo;
				Reparray["custparam_waveno"] = waveno;
				Reparray["custparam_entersku"] = getItem;
				Reparray["custparam_enterskuid"] = getItemId;
				Reparray["custparam_enterpickloc"] = getPrimaryloc;
				Reparray["custparam_entertaskpriority"] = getPriority;
				Reparray["custparam_zoneno"] = zoneNo;
				
				var vOrdFormat='';
				if(reportNo!=null && reportNo!='')
				{
					vOrdFormat='R';
				}

				if(zoneNo!=null && zoneNo!='')
				{
					vOrdFormat=vOrdFormat + 'Z';
				}

				if(reportNo != null && reportNo != "" && zoneNo!=null && zoneNo!="")
				{
					vOrdFormat='ALL';
				}

				var vZoneId='';
				
				if(zoneNo != null && zoneNo != '')
				{
					nlapiLogExecution('DEBUG', 'Into Zone Validate', 'Into Zone Validate');
					var ZoneFilters = new Array();
					var ZoneColumns = new Array();

					ZoneFilters.push(new nlobjSearchFilter('custrecord_putzoneid', null, 'is', zoneNo));
					ZoneFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
					var ZoneSearchResults = nlapiSearchRecord('customrecord_ebiznet_putpick_zone', null, ZoneFilters, ZoneColumns);
					if(ZoneSearchResults != null && ZoneSearchResults != '' && ZoneSearchResults.length>0)
					{
						vZoneId=ZoneSearchResults[0].getId();
					}
					else
					{
						nlapiLogExecution('DEBUG', 'Into Zone Validate Else', 'Into Zone Validate Else');
						var ZoneFilters1 = new Array();
						var ZoneColumns1 = new Array();
						ZoneFilters1.push(new nlobjSearchFilter('name', null, 'is', zoneNo));
						ZoneFilters1.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
						var ZoneSearchResults1 = nlapiSearchRecord('customrecord_ebiznet_putpick_zone', null, ZoneFilters1, ZoneColumns1);
						if(ZoneSearchResults1 != null && ZoneSearchResults1 != '' && ZoneSearchResults1.length>0)
						{
							vZoneId=ZoneSearchResults1[0].getId();
						}
						else
						{
							Reparray["custparam_error"] = st11;//'INVALID ZONE';
							Reparray["custparam_screenno"] = '2stepRptNo';
							nlapiLogExecution('DEBUG', 'Error: ', 'Zone not found');
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
							return;
						}
					}
				}
				
				nlapiLogExecution('DEBUG', 'vZoneId: ', vZoneId);
				
				Reparray["custparam_zoneno"] = vZoneId;			
				
				
				//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
				var searchresults =getReplenTasks(reportNo,vZoneId);
				
				if (searchresults != null && searchresults.length > 0) {					

						Reparray["custparam_replentype"] = vOrdFormat;
						Reparray["custparam_repbegloc"] = searchresults[0].getText('custrecord_actbeginloc',null,'group');
						Reparray["custparam_repbeglocId"] = searchresults[0].getValue('custrecord_actbeginloc',null,'group');
						Reparray["custparam_repsku"] = searchresults[0].getText('custrecord_sku',null,'group');
						Reparray["custparam_repexpqty"] = searchresults[0].getValue('custrecord_expe_qty',null,'sum');
						Reparray["custparam_repskuno"] = searchresults[0].getValue('custrecord_ebiz_sku_no',null,'group');					
						Reparray["custparam_whlocation"] = searchresults[0].getValue('custrecord_wms_location',null,'group');
						Reparray["custparam_reppicklocno"] = searchresults[0].getValue('custrecord_actendloc',null,'group');
						Reparray["custparam_reppickloc"] = searchresults[0].getText('custrecord_actendloc',null,'group');
						if(Reparray["custparam_repno"] == '' || Reparray["custparam_repno"] == '' || Reparray["custparam_repno"] =='null')
						Reparray["custparam_repno"] =searchresults[0].getValue('name',null,'group');
						nlapiLogExecution('ERROR','Report#', Reparray["custparam_repno"]);
						Reparray["custparam_noofrecords"] = searchresults.length;	
						nlapiLogExecution('ERROR','custparam_noofrecords', Reparray["custparam_noofrecords"]);

						nlapiLogExecution('ERROR','beginLocationId', Reparray["custparam_repbeglocId"]);

						nlapiLogExecution('ERROR','getReplenTasks: whLocation', searchresults[0].getValue('custrecord_wms_location'));

						response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplencartscan', 'customdeploy_ebiz_twostepreplencartscan', false, Reparray);
					
				}
				else{
					Reparray["custparam_error"] = st8;
					
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
					return;
				}
			}
			else{

				var searchresults =getReplenTasks(reportNo,vZoneId);

				if (searchresults != null && searchresults.length > 0) {					

					Reparray["custparam_replentype"] = vOrdFormat;
					Reparray["custparam_repbegloc"] = searchresults[0].getText('custrecord_actbeginloc',null,'group');
					Reparray["custparam_repbeglocId"] = searchresults[0].getValue('custrecord_actbeginloc',null,'group');
					Reparray["custparam_repsku"] = searchresults[0].getText('custrecord_sku',null,'group');
					Reparray["custparam_repexpqty"] = searchresults[0].getValue('custrecord_expe_qty',null,'sum');
					Reparray["custparam_repskuno"] = searchresults[0].getValue('custrecord_ebiz_sku_no',null,'group');					
					Reparray["custparam_whlocation"] = searchresults[0].getValue('custrecord_wms_location',null,'group');
					Reparray["custparam_reppicklocno"] = searchresults[0].getValue('custrecord_actendloc',null,'group');
					Reparray["custparam_reppickloc"] = searchresults[0].getText('custrecord_actendloc',null,'group');

					Reparray["custparam_noofrecords"] = searchresults.length;	
					nlapiLogExecution('ERROR','custparam_noofrecords', Reparray["custparam_noofrecords"]);

					nlapiLogExecution('ERROR','beginLocationId', Reparray["custparam_repbeglocId"]);

					nlapiLogExecution('ERROR','getReplenTasks: whLocation', searchresults[0].getValue('custrecord_wms_location'));

					response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplencartscan', 'customdeploy_ebiz_twostepreplencartscan', false, Reparray);

				}
				else{
					Reparray["custparam_error"] = st10;

					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
					return;
				}

							
			}
		}
	} //end of else loop.
}

//Get HTML code for scaning Report no
function getScanReportNo(request)
{
	var getLanguage = request.getParameter('custparam_language');
	nlapiLogExecution('ERROR', 'getLanguage', getLanguage);

	var st0,st1,st2,st3,st4,st5,st6,st7;
	if( getLanguage == 'es_ES')
	{
		st0 = "REPOSICI&#211;N DE NO";
		st1 = "ENTRAR/SCAN INFORME NO";
		st2 = "Y / O ENTRAR / SCAN ONDA NO";
		st3 = "ENVIAR";
		st4 = "ANTERIOR";	
	}
	else
	{
		st0 = "REPLENISHMENT NO";
		st1 = "ENTER/SCAN REPORT NO";
		st2 = "AND/OR ENTER/SCAN ZONE NO";
		st3 = "SEND";
		st4 = "PREV";
	}		


	var functionkeyHtml=getFunctionkeyScript('_rf_replenishment_po'); 
	var html = "<html><head><title>" + st0 + "</title>";
	html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
	html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";
	html = html + "var version = navigator.appVersion;";
	html = html + "document.onkeydown = function (e) {";
	html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
	html = html + "if ((version.indexOf('MSIE') != -1)) { ";
	html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
	html = html + "else {if (keycode == 116)return false;}";
	html = html + "};";
	html = html + "function stopRKey(evt) { ";
	html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
	html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
	html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
	html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
	html = html + "	  alert('System Processing, Please wait...');";
	html = html + "	  return false;}} ";
	html = html + "	} ";
	html = html + "	document.onkeypress = stopRKey; ";
	html = html + "</script>";
	html = html +functionkeyHtml;
	html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
	html = html + "	<form name='_rf_replenishment_po' method='POST'>";
	html = html + "		<table>";
	html = html + "			<tr>";	
	html = html + "				<td align = 'left'>";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'>" + st1;
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'><input name='enterReplenNo' id='enterReplenNo' type='text'/>";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'>" + st2;
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'><input name='enterZoneNo' id='enterZoneNo' type='text'/>";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
	html = html + "					"+ st4 + " <input name='cmdPrevious' type='submit' value='F7'/>";
	html = html + "				</td>";
	html = html + "			</tr>";
	html = html + "			<tr>";
	html = html + "			<td>";
	html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
	html = html + "			</td>";
	html = html + "			</tr>";
	html = html + "		 </table>";
	html = html + "	</form>";
	html = html + "<script type='text/javascript'>document.getElementById('enterReplenNo').focus();</script>";
	html = html + "</body>";
	html = html + "</html>";
	return html;
}

function getReplenTasks(reportNo,zoneNo)
{	
	var filters = new Array();
	var vOrdFormat='R';
	var vOrdFormat;
	
	
	if(reportNo!=null && reportNo!='')
	{
		nlapiLogExecution('ERROR','replenreportNo',reportNo);
		filters.push(new nlobjSearchFilter('name', null, 'is', reportNo));
		vOrdFormat='R';
	}
	if(zoneNo!=null && zoneNo!='')
	{
		nlapiLogExecution('ERROR','zoneNo',zoneNo);
		filters.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', zoneNo));
		vOrdFormat=vOrdFormat + 'I';
	}

	if(reportNo != null && reportNo != "" && zoneNo!=null && zoneNo!="")
	{
		vOrdFormat='ALL';
	}

	var RoleLocation=getRoledBasedLocationNew();
	if((RoleLocation !=null && RoleLocation !='' && RoleLocation !=0))
	{
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', RoleLocation));
	}
	
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20])); 
	
	try
	{
	
		var columns = new Array();
		
			columns[0] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
			columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
			columns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
			columns[3] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
			columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
			columns[5] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
			columns[6] = new nlobjSearchColumn('custrecord_actendloc',null,'group');
			columns[7] = new nlobjSearchColumn('name',null,'group');
			
		
		
		columns[1].setSort(false);
		columns[2].setSort(false);

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
		if(searchresults != null && searchresults != '')
			nlapiLogExecution('ERROR','searchresults tssssst',searchresults.length);
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','searchresults tssssst',exp);
	}
	nlapiLogExecution('DEBUG','searchresults',searchresults);
	return searchresults;
}

function getPickFaceLoc(skuNo)
{
	var filter = new Array();
	filter[0] = new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', skuNo);

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_pickbinloc');

	var searchresult = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, filter, column);
	return searchresult;
}
