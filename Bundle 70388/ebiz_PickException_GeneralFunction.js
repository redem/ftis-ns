/***************************************************************************
�eBizNET Solutions
 ****************************************************************************
 *
 *� $Source: /cvs/products/NSWMS/EoN/WMS_UI/Common/Suitelet/ebiz_PickException_GeneralFunction.js,v $
 *� $Revision: 1.2.2.16.4.8.4.28.2.2 $
 *� $Date: 2015/11/30 23:09:09 $
 *� $Author: nneelam $
 *� $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_PickException_GeneralFunction.js,v $
 *� Revision 1.2.2.16.4.8.4.28.2.2  2015/11/30 23:09:09  nneelam
 *� case# 201415880
 *�
 *� Revision 1.2.2.16.4.8.4.28.2.1  2015/11/04 23:27:46  sponnaganti
 *� case# 201413978
 *� 2015.2 issue fix
 *�
 *� Revision 1.2.2.16.4.8.4.28  2015/06/11 13:34:31  schepuri
 *� case# 201412518
 *�
 *� Revision 1.2.2.16.4.8.4.27  2015/04/21 13:43:33  schepuri
 *� case# 201412443,201412370
 *�
 *� Revision 1.2.2.16.4.8.4.26  2015/03/10 16:53:53  snimmakayala
 *� no message
 *�
 *� Revision 1.2.2.16.4.8.4.25  2015/01/02 15:03:54  skreddy
 *� Case# 201411349
 *� Two step replen process
 *�
 *� Revision 1.2.2.16.4.8.4.24  2014/12/24 12:15:53  snimmakayala
 *� Case#: 201411312
 *�
 *� Revision 1.2.2.16.4.8.4.17.2.5  2014/12/23 12:33:10  snimmakayala
 *� Case#: 201411312
 *�
 *� Revision 1.2.2.16.4.8.4.17.2.4  2014/10/15 10:35:26  skavuri
 *� Case# 201410650 Std bundle issue fixed
 *�
 *� Revision 1.2.2.16.4.8.4.17.2.3  2014/09/16 16:11:31  sponnaganti
 *� Case# 201410374
 *� Stnd Bundle Issue fix
 *�
 *� Revision 1.2.2.16.4.8.4.17.2.2  2014/07/21 13:29:46  snimmakayala
 *� Case: 20149536
 *� Replen generation issue
 *�
 *� Revision 1.2.2.16.4.8.4.17.2.1  2014/06/27 11:36:04  sponnaganti
 *� case# 20149070
 *� Compatability Test Acc issue fix
 *�
 *� Revision 1.2.2.16.4.8.4.17  2014/06/12 14:39:13  grao
 *� Case#: 20148833  New GUI account issue fixes
 *�
 *� Revision 1.2.2.16.4.8.4.16  2014/04/23 15:37:55  grao
 *� SB iSSUES FIXES cASE#;20148120
 *�
 *� Revision 1.2.2.16.4.8.4.15  2014/04/23 15:08:10  rmukkera
 *� Case # 20127804
 *�
 *� Revision 1.2.2.16.4.8.4.14  2014/04/03 15:56:33  skavuri
 *� Case # 20140002 issue fixed
 *�
 *� Revision 1.2.2.16.4.8.4.13  2014/02/14 15:00:59  rmukkera
 *� Case # 20127171
 *�
 *� Revision 1.2.2.16.4.8.4.12  2014/01/09 14:06:01  grao
 *� Case# 20126638 related issue fixes in Sb issue fixes
 *�
 *� Revision 1.2.2.16.4.8.4.11  2014/01/08 15:01:50  grao
 *� Case# 20126638  related issue fixes in Sb issue fixes
 *�
 *� Revision 1.2.2.16.4.8.4.10  2013/11/28 15:19:16  grao
 *� Case# 20125886 related issue fixes in SB 2014.1
 *�
 *� Revision 1.2.2.16.4.8.4.9  2013/11/20 23:28:13  skreddy
 *� Case# 20125850
 *� Lost and Found CR for MHP
 *� issue fix
 *�
 *� Revision 1.2.2.16.4.8.4.8  2013/11/04 15:21:12  rmukkera
 *� Case#  20125458
 *�
 *� Revision 1.2.2.16.4.8.4.7  2013/11/01 13:42:03  rmukkera
 *� Case# 20125458
 *�
 *� Revision 1.2.2.16.4.8.4.6  2013/10/29 15:20:39  grao
 *� Issue fix related to the 20125394
 *�
 *� Revision 1.2.2.16.4.8.4.5  2013/09/16 09:40:01  snimmakayala
 *� GSUSA PROD ISSUE
 *� Case# : 201215000
 *� RF Fast Picking
 *�
 *� Revision 1.2.2.16.4.8.4.4  2013/05/14 14:15:44  schepuri
 *� GSUSA SB Task Priority issue
 *�
 *� Revision 1.2.2.16.4.8.4.3  2013/04/10 15:50:20  skreddy
 *� CASE201112/CR201113/LOG201121
 *� issue related to qty exception in picking
 *�
 *� Revision 1.2.2.16.4.8.4.2  2013/03/19 13:50:22  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Production and UAT issue fixes.
 *�
 *� Revision 1.2.2.16.4.8.4.1  2013/03/01 15:01:12  rmukkera
 *� code is merged from FactoryMation production as part of Standard bundle
 *�
 *� Revision 1.2.2.16.4.8  2013/01/11 13:25:08  skreddy
 *� CASE201112/CR201113/LOG201121
 *� issue related Quantity reallocation
 *�
 *� Revision 1.2.2.16.4.7  2013/01/09 15:18:44  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Round Qty functionality in replenishment.
 *� .
 *�
 *� Revision 1.2.2.16.4.6  2012/12/31 05:53:10  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Inventory Sync issue fixes
 *� .
 *�
 *� Revision 1.2.2.16.4.5  2012/12/16 01:39:08  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� GSUSA after go live Fixes.
 *�
 *� Revision 1.2.2.16.4.4  2012/12/06 07:15:51  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� GSUSA UAT Fixes.Creating Failed Task for Replens.
 *�
 *� Revision 1.2.2.16.4.3  2012/11/01 14:55:22  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Decimal Qty Conversions
 *�
 *� Revision 1.2.2.16.4.2  2012/10/29 20:43:34  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� GUUSA UAT ISSUE FIXES
 *�
 *� Revision 1.2.2.16.4.1  2012/10/26 13:59:47  skreddy
 *� CASE201112/CR201113/LOG201121
 *� Issue related to Loc exception for pickface location
 *�
 *� Revision 1.2.2.16  2012/09/05 14:22:51  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Can not print packing list is resolved
 *�
 *� Revision 1.2.2.15  2012/08/30 15:29:58  skreddy
 *� CASE201112/CR201113/LOG201121
 *�  issue fixed
 *�
 *� Revision 1.2.2.14  2012/08/29 18:18:33  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *� replens generation based on Auto replen flag
 *�
 *� Revision 1.2.2.13  2012/08/28 16:03:15  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Issue related to Cannot read property "length" from null.
 *�
 *� Revision 1.2.2.12  2012/08/27 07:04:33  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *� clusterpickingReplen
 *�
 *� Revision 1.2.2.11  2012/08/24 12:49:03  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Qty Exception issue for Boombah.Positive Adjustments are posting to NS if we perform qty exception.
 *�
 *� Revision 1.2.2.10  2012/07/19 05:22:24  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Performance Tuning
 *�
 *� Revision 1.2.2.9  2012/06/26 23:01:55  gkalla
 *� CASE201112/CR201113/LOG201121
 *� Subsidiary issue fixed
 *�
 *� Revision 1.2.2.8  2012/05/22 16:03:26  gkalla
 *� CASE201112/CR201113/LOG201121
 *� To fix replenishment issue for RF Pick Qty exception
 *�
 *� Revision 1.2.2.7  2012/05/04 22:52:08  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Production issue fixes
 *�
 *� Revision 1.2.2.6  2012/04/30 10:33:42  spendyala
 *� CASE201112/CR201113/LOG201121
 *� While Searching of Item in ItemMaster,
 *� 'name' filter is changed to 'nameinternal' and also checking weather the item is inactive state or not.
 *�
 *
 ****************************************************************************/


function CreateNewShortPick(RcId,vDisplayedQty,pickqty,vdono,recordcount,getEnteredContainerNo,TotalWeight,newLot,remainqty,Systemrules,request)
{
	try
	{
		nlapiLogExecution('DEBUG', 'into CreateNewShortPickOpenTask : ', 'CreateNewShortPickOpenTask');

		var vRemaningqty=0,vname,vCompany1,vcontainer,vcontrolno,vcontainerLPno,vItemno,vEbizItemNo,vEbizWaveNo1,vLineno1,vlpno1,
		vpackcode1,vSkuStatus1,vTaskType1,vUOMid1;
		var vEbizZone1,vUOMLevel1,vEbizReceiptNo1,vInvRefNo1,vEbizUser1,vFromLP1,vEbizOrdNo1=0,vWMSLocation1,vBeginLoc1,vLot;
		var vname2,vsku2,vlineord2,vlineord2,vordlineno2,vOrdQty2,vPackCode2,vPickGenFlag2,vPickGenFlag2,vPickGenQty2,vPickQty2,
		vlineskustatus2,vlinestatusflag2;
		var vUOMid2,vBackOrdFlag2,vParentOrd2,vOrdType2,vCustomer2,vDoCarrirer2,vWMSLocation2,vLineCompany2,vWMSCarrier2,vOrderPriority2;
		var vAdjustType1;
		var pickqtyfinal=0;
		var ordqtyfinal=0;
		var currentContext = nlapiGetContext();
		var currentUserID = currentContext.getUser();

		var Opentasktransaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RcId);

		vname = Opentasktransaction.getFieldValue('name');
		vCompany1 = Opentasktransaction.getFieldValue('custrecord_comp_id');
		vContainer = Opentasktransaction.getFieldValue('custrecord_container');
		vControlno = Opentasktransaction.getFieldValue('custrecord_ebiz_cntrl_no');
		vContainerLPno = Opentasktransaction.getFieldValue('custrecord_container_lp_no');
		vItemno = Opentasktransaction.getFieldValue('custrecord_sku');
		vEbizItemNo = Opentasktransaction.getFieldValue('custrecord_ebiz_sku_no');
		vEbizWaveNo1 = Opentasktransaction.getFieldValue('custrecord_ebiz_wave_no');
		vLineno1 = Opentasktransaction.getFieldValue('custrecord_line_no');
		vlpno1 = Opentasktransaction.getFieldValue('custrecord_lpno');
		vpackcode1 = Opentasktransaction.getFieldValue('custrecord_packcode');
		vSkuStatus1 = Opentasktransaction.getFieldValue('custrecord_sku_status');
		vTaskType1 = Opentasktransaction.getFieldValue('custrecord_tasktype');
		vBeginLoc1 = Opentasktransaction.getFieldValue('custrecord_actbeginloc');
		vUOMLevel1 = Opentasktransaction.getFieldValue('custrecord_uom_level');
		vEbizReceiptNo1 = Opentasktransaction.getFieldValue('custrecord_ebiz_receipt_no');
		vInvRefNo1 = Opentasktransaction.getFieldValue('custrecord_invref_no');
		vFromLP1 = Opentasktransaction.getFieldValue('custrecord_from_lp_no'); 
		vEbizOrdNo1 = Opentasktransaction.getFieldValue('custrecord_ebiz_order_no');
		vWMSLocation1 = Opentasktransaction.getFieldValue('custrecord_wms_location');
		vEbizRule1 = Opentasktransaction.getFieldValue('custrecord_ebizrule_no');
		vEbizMethod1 = Opentasktransaction.getFieldValue('custrecord_ebizmethod_no');
		vEbizZone1 = Opentasktransaction.getFieldValue('custrecord_ebizzone_no');
		vEbizUser1 = Opentasktransaction.getFieldValue('custrecord_ebizuser');
		vLot = Opentasktransaction.getFieldValue('custrecord_batch_no');

		nlapiLogExecution('ERROR', 'vBeginLoc1', vBeginLoc1);
		nlapiLogExecution('ERROR', 'vWMSLocation1', vWMSLocation1);
		nlapiLogExecution('ERROR', 'vItemno', vItemno);
		nlapiLogExecution('DEBUG', 'pickqty', pickqty);
		nlapiLogExecution('DEBUG', 'vDisplayedQty', vDisplayedQty);
		nlapiLogExecution('DEBUG', 'remainqty', remainqty);
		if(remainqty==null || remainqty=='' || remainqty=='null' || isNaN(remainqty))
			remainqty=0;
		vRemaningqty = (parseFloat(pickqty)+parseFloat(remainqty))-parseFloat(vDisplayedQty);
		nlapiLogExecution('ERROR', 'vRemaningqty before', vRemaningqty);

		if( Systemrules!=null && Systemrules!=''& Systemrules.length>0 && Systemrules[0]=='Y' && (Systemrules[1] =="STND" ||Systemrules[1] =="NOADJUST" ))
		{
			nlapiLogExecution('ERROR', 'Into STND', vInvRefNo1);
			vRemaningqty = pickqty-vDisplayedQty;
		}
		else
		{
			//for Adjust Binlocation Inventory

			//nlapiLogExecution('ERROR', 'vInvRefNo1', vInvRefNo1);	
			//Case # 20127804�  Start
			/*var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', vInvRefNo1);
		var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
		var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

		if(Invallocqty==null || Invallocqty=='')
			Invallocqty=0;

		nlapiLogExecution('ERROR', 'Invallocqty', Invallocqty);
		nlapiLogExecution('ERROR', 'InvQOH', InvQOH);

//		if(remainqty ==0)
//		vRemaningqty = parseFloat(InvQOH)- parseFloat(vActqty);
//		else
//		vRemaningqty = parseFloat(InvQOH-vActqty)+parseFloat(remainqty);

		vRemaningqty = parseFloat(Invallocqty)-parseFloat(InvQOH);

		var vNewQOH =  Invallocqty;

		if(vNewQOH<=0)
		{
			nlapiLogExecution('Debug', 'Deleting record from inventory if QOH becomes zero', vInvRefNo1);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', vInvRefNo1);			
		}
		else
		{
			Invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(vNewQOH));  
			Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
			Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

			var invtrecid = nlapiSubmitRecord(Invttran, false, true);
			nlapiLogExecution('Debug', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);
		}*/
			nlapiLogExecution('ERROR', 'else clearBinlevelInvt', "Calling");
			vRemaningqty=clearBinlevelInvt(vItemno,vBeginLoc1,vWMSLocation1,vInvRefNo1);
			//Case # 20127804�  End
		}

		nlapiLogExecution('ERROR', 'vRemaningqty after', vRemaningqty);

		var vRemPickQty = parseFloat(vDisplayedQty)- parseFloat(pickqty);

		var PickExpOpenTaskRec=nlapiCreateRecord('customrecord_ebiznet_trn_opentask');

		PickExpOpenTaskRec.setFieldValue('name', vname);
		PickExpOpenTaskRec.setFieldValue('custrecordact_begin_date', DateStamp());
		PickExpOpenTaskRec.setFieldValue('custrecord_comp_id', vCompany1);
		PickExpOpenTaskRec.setFieldValue('custrecord_container', vContainer);
		PickExpOpenTaskRec.setFieldValue('custrecord_current_date', DateStamp());
		PickExpOpenTaskRec.setFieldValue('custrecord_ebiz_cntrl_no', vControlno);
		PickExpOpenTaskRec.setFieldValue('custrecord_container_lp_no', vContainerLPno);
		PickExpOpenTaskRec.setFieldValue('custrecord_sku', vItemno);
		PickExpOpenTaskRec.setFieldValue('custrecord_ebiz_sku_no', vEbizItemNo);
		PickExpOpenTaskRec.setFieldValue('custrecord_ebiz_wave_no', vEbizWaveNo1);
		PickExpOpenTaskRec.setFieldValue('custrecord_expe_qty', parseFloat(vRemPickQty).toFixed(4));
		PickExpOpenTaskRec.setFieldValue('custrecord_line_no', vLineno1);
		PickExpOpenTaskRec.setFieldValue('custrecord_lpno', vlpno1);
		PickExpOpenTaskRec.setFieldValue('custrecord_packcode', vpackcode1);
		PickExpOpenTaskRec.setFieldValue('custrecord_sku_status', vSkuStatus1);
		PickExpOpenTaskRec.setFieldValue('custrecord_wms_status_flag', '30'); //Status Flag - Short Pick 
		PickExpOpenTaskRec.setFieldValue('custrecord_tasktype', vTaskType1);
		PickExpOpenTaskRec.setFieldValue('custrecord_totalcube', '');
		PickExpOpenTaskRec.setFieldValue('custrecord_total_weight', '');
		PickExpOpenTaskRec.setFieldValue('custrecord_ebizzone_no', vEbizZone1);
		PickExpOpenTaskRec.setFieldValue('custrecord_uom_level', vUOMLevel1);
		PickExpOpenTaskRec.setFieldValue('custrecord_ebiz_receipt_no', vEbizReceiptNo1);
		PickExpOpenTaskRec.setFieldValue('custrecord_actualbegintime', TimeStamp());
		PickExpOpenTaskRec.setFieldValue('custrecord_invref_no', vInvRefNo1);
		PickExpOpenTaskRec.setFieldValue('custrecord_from_lp_no', vFromLP1);
		PickExpOpenTaskRec.setFieldValue('custrecord_ebiz_order_no', vEbizOrdNo1);
		PickExpOpenTaskRec.setFieldValue('custrecord_wms_location', vWMSLocation1);
		PickExpOpenTaskRec.setFieldValue('custrecord_ebizrule_no', vEbizRule1);
		PickExpOpenTaskRec.setFieldValue('custrecord_ebizmethod_no', vEbizMethod1);
		PickExpOpenTaskRec.setFieldValue('custrecord_ebizuser', currentUserID);					
		PickExpOpenTaskRec.setFieldValue('custrecord_upd_ebiz_user_no',currentUserID);
		var vemployee = request.getParameter('custpage_employee');

		if (vemployee != null && vemployee != "") 
		{
			PickExpOpenTaskRec.setFieldValue('custrecord_taskassignedto',vemployee);
		} 
		else 
		{
			PickExpOpenTaskRec.setFieldValue('custrecord_taskassignedto',currentUserID);
		}
		nlapiLogExecution('DEBUG', 'before CreateNewShortPickOpenTask : ', 'before CreateNewShortPickOpenTask');
		if(vdono!= null && vdono !='' && vdono !='null')
		{

			var FulfillOrdLinetransaction = nlapiLoadRecord('customrecord_ebiznet_ordline',vdono);

			vname2 = FulfillOrdLinetransaction.getFieldValue('name');
			vsku2 = FulfillOrdLinetransaction.getFieldValue('custrecord_ebiz_linesku');
			vlineord2 = FulfillOrdLinetransaction.getFieldValue('custrecord_lineord');

			var newfulfillordnoarray = vlineord2.split('.');
			var ordername = newfulfillordnoarray[0];

			//generate new fullfillment order no.
			var fullFillmentorderno=fulfillmentOrderDetails(vEbizOrdNo1,ordername);

			vordlineno2 = FulfillOrdLinetransaction.getFieldValue('custrecord_ordline');
			vOrdQty2 = FulfillOrdLinetransaction.getFieldValue('custrecord_ord_qty');
			vPackCode2 = FulfillOrdLinetransaction.getFieldValue('custrecord_linepackcode');
			vPickGenFlag2 = FulfillOrdLinetransaction.getFieldValue('custrecord_pickgen_flag');
			vPickGenQty2 = FulfillOrdLinetransaction.getFieldValue('custrecord_pickgen_qty');
			vlineskustatus2 = FulfillOrdLinetransaction.getFieldValue('custrecord_linesku_status');
			vUOMid2 = FulfillOrdLinetransaction.getFieldValue('custrecord_lineuom_id');
			vBackOrdFlag2 = FulfillOrdLinetransaction.getFieldValue('custrecord_back_order_flag');
			vParentOrd2 = FulfillOrdLinetransaction.getFieldValue('custrecord_ns_ord');
			vOrdType2 = FulfillOrdLinetransaction.getFieldValue('custrecord_do_order_type');
			vCustomer2 = FulfillOrdLinetransaction.getFieldValue('custrecord_do_customer');
			vDoCarrirer2 = FulfillOrdLinetransaction.getFieldValue('custrecord_do_carrier'); 
			vWMSLocation2 = FulfillOrdLinetransaction.getFieldValue('custrecord_ordline_wms_location');
			vLineCompany2 = FulfillOrdLinetransaction.getFieldValue('custrecord_ordline_company');
			vWMSCarrier2 = FulfillOrdLinetransaction.getFieldValue('custrecord_do_wmscarrier');
			vOrderPriority2 = FulfillOrdLinetransaction.getFieldValue('custrecord_do_order_priority');
			var NSRefNo = FulfillOrdLinetransaction.getFieldValue('custrecord_nsconfirm_ref_no');


			var oldpickQty=FulfillOrdLinetransaction.getFieldValue('custrecord_pickqty');

			ordqtyfinal = parseFloat(vOrdQty2)-parseFloat(vRemPickQty);
			var pickGenqtyfinal = parseFloat(vPickGenQty2)-parseFloat(vRemPickQty);

			if(parseFloat(ordqtyfinal)<0)
				ordqtyfinal=0;
			if(parseFloat(pickGenqtyfinal)<0)
				pickGenqtyfinal=0;
			FulfillOrdLinetransaction.setFieldValue('custrecord_ord_qty', parseFloat(ordqtyfinal));
			FulfillOrdLinetransaction.setFieldValue('custrecord_pickgen_qty', parseFloat(pickGenqtyfinal));

			if(parseFloat(oldpickQty)<parseFloat(ordqtyfinal))
				FulfillOrdLinetransaction.setFieldValue('custrecord_linestatus_flag', '11'); //STATUS.OUTBOUND.PARTIALLY_PROCESSED
			else
				FulfillOrdLinetransaction.setFieldValue('custrecord_linestatus_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')
			if(isNaN(oldpickQty))
				oldpickQty=0;

			pickqtyfinal=parseFloat(oldpickQty)+parseFloat(pickqty);
			vRemaningqtyfinal = parseFloat(vRemaningqty);

			nlapiLogExecution('DEBUG', 'vRemaningqtyfinal', vRemaningqtyfinal);
			nlapiLogExecution('DEBUG', 'vRemaningqty', vRemaningqty);
			nlapiLogExecution('DEBUG', 'vWMSLocation1', vWMSLocation1);
		}

//		var PickExpFulfillOrdLineRec=nlapiCreateRecord('customrecord_ebiznet_ordline');

//		PickExpFulfillOrdLineRec.setFieldValue('name', vname2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_ebiz_linesku', vsku2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_lineord', fullFillmentorderno);//new fullfillord
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_ordline', vordlineno2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_ord_qty',parseFloat(vRemPickQty));//vOrdQty2
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_linepackcode', vPackCode2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_pickgen_flag', vPickGenFlag2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_linesku_status', vlineskustatus2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_linestatus_flag', '25');//status.outbound.edit
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_lineuom_id', vUOMid2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_back_order_flag', vBackOrdFlag2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_ns_ord', vParentOrd2);//have check the values in parameter
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_do_order_type', vOrdType2);//have check the values in parameter
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_do_customer', vCustomer2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_do_carrier', vDoCarrirer2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_ordline_wms_location', vWMSLocation2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_ordline_company', vLineCompany2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_do_wmscarrier', vWMSCarrier2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_do_order_priority', vOrderPriority2);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_nsconfirm_ref_no', NSRefNo);
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_linenotes2', 'Created by Confirm picks qty Exception');
//		PickExpFulfillOrdLineRec.setFieldValue('custrecord_linenotes1', vOrdQty2);
//		nlapiLogExecution('DEBUG', 'after PickExpFulfillOrdLineRec : ', 'after PickExpFulfillOrdLineRec');

		updateWtInLPMaster(getEnteredContainerNo,TotalWeight);

		vAdjustType1=getAdjustmentType(vWMSLocation1);

		var vsonamearr = vname.split('.');
		var vsoname = vsonamearr[0];


		var vnotes1="This is from Confirm picks qty Exception. SO#: "+vsoname;

		nlapiSubmitRecord(PickExpOpenTaskRec, false, true);

//		var getfulfillrecid = nlapiSubmitRecord(PickExpFulfillOrdLineRec, false, true);

		if(vdono!= null && vdono !='' && vdono !='null')
		{
			FulfillOrdLinetransaction.setFieldValue('custrecord_linenotes1', vnotes1);
			var getOldfulfillrecid = nlapiSubmitRecord(FulfillOrdLinetransaction, false, true);
		}
		if(parseFloat(vRemaningqty)!=0)
		{
			/*var invAdjustCustRecord = nlapiCreateRecord('customrecord_ebiznet_invadj');
		createInvAdjRecord(invAdjustCustRecord, vBeginLoc1, vItemno, vSkuStatus1,newLot , vDisplayedQty, vlpno1,
				vAdjustType1, vnotes1, vWMSLocation1, parseFloat(vRemaningqty), vpackcode1, recordcount);
		nlapiLogExecution('ERROR', 'adjustType in adjust main ', vAdjustType1);	*/    
			var tasktype=11; //11 is the internalid value of tasktype "ADJT"; 

			nlapiLogExecution('DEBUG', 'pickqty ', pickqty);	
			nlapiLogExecution('DEBUG', 'parseFloat(pickqty) ', parseFloat(pickqty));	
			nlapiLogExecution('DEBUG', 'vDisplayedQty ', vDisplayedQty);	
			nlapiLogExecution('DEBUG', 'parseFloat(vDisplayedQty) ', parseFloat(vDisplayedQty));	
			nlapiLogExecution('DEBUG', 'vRemaningqty ', vRemaningqty);	
			nlapiLogExecution('DEBUG', 'parseFloat(vRemaningqty) ', parseFloat(vRemaningqty));	
			if(Systemrules!=null && Systemrules!='')
			{
				nlapiLogExecution('ERROR', 'Systemrules[0] ', Systemrules[0]);
				nlapiLogExecution('ERROR', 'Systemrules[1]', Systemrules[1]);
				nlapiLogExecution('ERROR', 'Systemrules[1]', Systemrules[1]);
				nlapiLogExecution('ERROR', 'vWMSLocation1', vWMSLocation1);
			}


			// case# 201412518
			//if(PostAdjustmenttoNS(vWMSLocation1)=='Y')
			var PostAdjustmenttoNS = getSystemRule('Post Adjustment to NS during Pick Exception?');
			if(PostAdjustmenttoNS=='Y')
			{
				var fields = ['recordType', 'custitem_ebizserialin'];
				var columns = nlapiLookupField('item', vItemno, fields);
				var vItemType = columns.recordType;
				nlapiLogExecution('ERROR','vItemType',vItemType);
				var serialInflg="F";		
				serialInflg = columns.custitem_ebizserialin;
				nlapiLogExecution('ERROR','serialInflg',serialInflg);
				if (vItemType == "serializedinventoryitem" || vItemType == "serializedassemblyitem" || serialInflg == "T") {
					var filtersser = new Array();


					if(vItemno !=null && vItemno!='')
						filtersser.push(new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', vItemno));

					filtersser.push( new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['3','19']));

					if(vlpno1 !=null && vlpno1!='')
					{
						//filtersser[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', getContainerLP);
						filtersser.push(new nlobjSearchFilter('custrecord_serialparentid', null, 'is', vlpno1));

					}
					var cols= new Array();
					cols[0]=new nlobjSearchColumn('custrecord_serialnumber');
					var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser,cols);
					nlapiLogExecution('DEBUG', 'SrchRecord', SrchRecord);
					var tempqty=parseFloat(vDisplayedQty)-parseFloat(pickqty);
					nlapiLogExecution('DEBUG', 'tempqty', tempqty);
					var serialNumbers= new Array();

					if(SrchRecord !=null && SrchRecord!='')
					{
						for(var k=0;k<SrchRecord.length && k<parseFloat(tempqty);k++)
						{
							serialNumbers[serialNumbers.length]=SrchRecord[k].getValue('custrecord_serialnumber');
							var SerialRecord= nlapiLoadRecord('customrecord_ebiznetserialentry',SrchRecord[k].getId() );
							SerialRecord.setFieldValue('custrecord_serialstatus','D');
							nlapiSubmitRecord(SerialRecord);
						}
					}


					newLot=serialNumbers.toString();
					nlapiLogExecution('ERROR', 'newLot ', newLot);	
				}//Case # 20127171�Start
				else if (vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem")
				{
					newLot=vLot;
				}//Case # 20127171�End
				/*var netsuiteadjustId = getIdInvokeNSInventoryAdjustment(vItemno,vSkuStatus1,vWMSLocation1,parseFloat(vRemaningqty),vnotes1,tasktype,vAdjustType1,newLot);
			nlapiLogExecution('DEBUG', 'netsuiteadjustId main11', netsuiteadjustId);	
			invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjustno',netsuiteadjustId);*/

				if(Systemrules!=null && Systemrules!='' && Systemrules.length>0 && Systemrules[0]=='Y' && Systemrules[1] !="NOADJUST")
				{


					//to fetch Stock adjustment Details from Stock Adjustments 
					var adjtasktype=22; //Tasktype : LOST
					var GetStockAdjustResults = getStockAdjustmentDetails(vWMSLocation1,adjtasktype,null);
					if(GetStockAdjustResults !=null && GetStockAdjustResults !='' && GetStockAdjustResults.length >0)
					{
						nlapiLogExecution('ERROR', 'GetStockAdjustResults.length ', GetStockAdjustResults.length);	
						var transactiontype =  GetStockAdjustResults[0].getValue('custrecord_ebiz_adjtype_trantype');
						var MapNSlocation =  GetStockAdjustResults[0].getValue('custrecord_ebiz_adjtype_mapnslocation');
						var MoveWmsBinlocation =  GetStockAdjustResults[0].getValue('custrecord_ebiz_adtype_movewmsbinloc');

						nlapiLogExecution('ERROR', 'transactiontype ', transactiontype);	
						nlapiLogExecution('ERROR', 'MapNSlocation', MapNSlocation);	
						nlapiLogExecution('ERROR', 'MoveWmsBinlocation', MoveWmsBinlocation);

						//tranaction Type =" Inventory Transfer", perform Inventory Transfer
						if(transactiontype == "2")
						{

							var transferQty = -(vRemaningqty);
							nlapiLogExecution('ERROR', 'transferQty', transferQty);

							var InvtranfId = InvokeNSInventoryTransfer(vItemno,vSkuStatus1,vWMSLocation1,MapNSlocation,parseFloat(transferQty),newLot);
							var Invtnotes1 = "Created from Invt Transfers";
							//create inventory mapped location
							createInvtwithStockadjBinloc(MoveWmsBinlocation, vItemno, vSkuStatus1,newLot , vDisplayedQty, vlpno1,
									vAdjustType1, Invtnotes1, vWMSLocation1, parseFloat(transferQty), vpackcode1, recordcount);

							CreateOpentaskRecord(vBeginLoc1, vItemno, vSkuStatus1,newLot, vDisplayedQty, vlpno1,vAdjustType1, Invtnotes1, vWMSLocation1, parseFloat(transferQty), vpackcode1, recordcount,MoveWmsBinlocation);


						}
						else
						{
							// Inventory Adjustment

							var invAdjustCustRecord = nlapiCreateRecord('customrecord_ebiznet_invadj');
							createInvAdjRecord(invAdjustCustRecord, vBeginLoc1, vItemno, vSkuStatus1,newLot , vDisplayedQty, vlpno1,
									vAdjustType1, vnotes1, vWMSLocation1, parseFloat(vRemaningqty), vpackcode1, recordcount);
							nlapiLogExecution('ERROR', 'adjustType in adjust main ', vAdjustType1);	
							var netsuiteadjustId='';
							//if(pickqty!=0)
							netsuiteadjustId = InvokeNSInventoryAdjustment(vItemno,vSkuStatus1,vWMSLocation1,parseFloat(vRemaningqty)*-1,vnotes1,tasktype,vAdjustType1,newLot);
							//	var netsuiteadjustId = getIdInvokeNSInventoryAdjustment(vItemno,vSkuStatus1,vWMSLocation1,parseFloat(vRemaningqty),vnotes1,tasktype,vAdjustType1,newLot);
							nlapiLogExecution('ERROR', 'netsuiteadjustId main11', netsuiteadjustId);
							var adjustnote ="Not posted to GL Account";
							if(netsuiteadjustId == null || netsuiteadjustId =='')
								invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjnotes1',adjustnote);
							if(netsuiteadjustId != null && netsuiteadjustId !='')
								invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjustno',netsuiteadjustId);
							invAdjustCustRecord.setFieldValue('custrecord_ebiz_cntrlno',vEbizOrdNo1);

							// Update Inventory Adjustment custom record
							var invAdjRecId = nlapiSubmitRecord(invAdjustCustRecord, false, true);
							nlapiLogExecution('ERROR', 'Inventory Adjust Record Insertion / Update for invAdjustRecId',
									invAdjRecId + ' is Success');

						}



					}
					else
					{

						nlapiLogExecution('ERROR', 'into else ','sucess');

						var invAdjustCustRecord = nlapiCreateRecord('customrecord_ebiznet_invadj');
						createInvAdjRecord(invAdjustCustRecord, vBeginLoc1, vItemno, vSkuStatus1,newLot , vDisplayedQty, vlpno1,
								vAdjustType1, vnotes1, vWMSLocation1, parseFloat(vRemaningqty), vpackcode1, recordcount);
						nlapiLogExecution('ERROR', 'adjustType in adjust main ', vAdjustType1);	
						var netsuiteadjustId='';
						//if(pickqty!=0)
						netsuiteadjustId = InvokeNSInventoryAdjustment(vItemno,vSkuStatus1,vWMSLocation1,parseFloat(vRemaningqty),vnotes1,tasktype,vAdjustType1,newLot);
						//	var netsuiteadjustId = getIdInvokeNSInventoryAdjustment(vItemno,vSkuStatus1,vWMSLocation1,parseFloat(vRemaningqty),vnotes1,tasktype,vAdjustType1,newLot);
						nlapiLogExecution('ERROR', 'netsuiteadjustId main11', netsuiteadjustId);
						var adjustnote ="Not posted to GL Account";
						if(netsuiteadjustId == null || netsuiteadjustId =='')
							invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjnotes1',adjustnote);
						if(netsuiteadjustId != null && netsuiteadjustId !='')
							invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjustno',netsuiteadjustId);
						invAdjustCustRecord.setFieldValue('custrecord_ebiz_cntrlno',vEbizOrdNo1);

						// Update Inventory Adjustment custom record
						var invAdjRecId = nlapiSubmitRecord(invAdjustCustRecord, false, true);
						nlapiLogExecution('ERROR', 'Inventory Adjust Record Insertion / Update for invAdjustRecId',
								invAdjRecId + ' is Success');


					}


//					var netsuiteadjustId = getIdInvokeNSInventoryAdjustment(vItemno,vSkuStatus1,vWMSLocation1,parseFloat(vRemaningqty),vnotes1,tasktype,vAdjustType1,newLot);
//					nlapiLogExecution('ERROR', 'netsuiteadjustId main11', netsuiteadjustId);	
//					invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjustno',netsuiteadjustId);
				}


			}

			/*invAdjustCustRecord.setFieldValue('custrecord_ebiz_cntrlno',vEbizOrdNo1);

		// Update Inventory Adjustment custom record
		var invAdjRecId = nlapiSubmitRecord(invAdjustCustRecord, false, true);
		nlapiLogExecution('DEBUG', 'Inventory Adjust Record Insertion / Update for invAdjustRecId',
				invAdjRecId + ' is Success');*/
		}
	}
	catch(e)
	{
		nlapiLogExecution('ERROR', 'Exception: ', e);
	}
}
//Case # 20127804�  Start
function clearBinlevelInvt(Item,binLocation,WHLoc,pInvRefNo1)
{
	var totalClearedInvt=0;
	nlapiLogExecution('ERROR', 'into  clearBinlevelInvt', 'clearBinlevelInvt');	
	nlapiLogExecution('ERROR', 'Item', Item);	
	nlapiLogExecution('ERROR', 'binLocation', binLocation);	
	nlapiLogExecution('ERROR', 'WHLoc', WHLoc);	
	nlapiLogExecution('ERROR', 'pInvRefNo1', pInvRefNo1);	
	try
	{
		var filters = new Array();
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', binLocation));		
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', Item));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_avl_qty', null, 'greaterthan', 0));
		filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19','3']));
		if(WHLoc != null && WHLoc !='')
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', WHLoc));
		if(pInvRefNo1 != null && pInvRefNo1 !='')
			filters.push(new nlobjSearchFilter('internalid', null, 'anyof', [pInvRefNo1]));

		var columns = new Array();	
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');


		var BinInvList = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
		nlapiLogExecution('ERROR', 'BinInvList', BinInvList);	
		if(BinInvList !=null && BinInvList!='' && BinInvList.length > 0)
		{
			nlapiLogExecution('ERROR', 'BinInvList', BinInvList.length);	

			for(var k1=0;k1<BinInvList.length;k1++)
			{
				var scount=1;
				LABL1: for(var p=0;p<scount;p++)
				{	
					nlapiLogExecution('ERROR', 'invrefno', BinInvList[k1].getId());
					var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', BinInvList[k1].getId());

					var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
					var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');
					nlapiLogExecution('ERROR', 'Invallocqty', Invallocqty);
					nlapiLogExecution('ERROR', 'InvQOH', InvQOH);

					if(Invallocqty!=null && Invallocqty!='' && Invallocqty!='null' && Invallocqty!='undefined')
					{
						var tempQty=parseFloat(InvQOH)-parseFloat(Invallocqty);
						totalClearedInvt=parseFloat(totalClearedInvt)+parseFloat(tempQty);
						Invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(Invallocqty)); 
						Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
						Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

						var invtrecid = nlapiSubmitRecord(Invttran, false, true);
						if(tempQty <= 0)
						{
							nlapiLogExecution('Debug', 'Deleting record from inventory if QOH becomes zero', invtrecid);
							var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invtrecid);
						}
					}
					else
					{
						totalClearedInvt=parseFloat(totalClearedInvt)+parseFloat(InvQOH);
						Invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(0)); 
						var invtrecid = nlapiSubmitRecord(Invttran, false, true);
						nlapiLogExecution('Debug', 'Deleting record from inventory if QOH becomes zero', invtrecid);
						var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invtrecid);
					}


				}
			}

		}
	}
	catch (e) {
		if (e instanceof nlobjError)
			nlapiLogExecution('ERROR', 'system error', e.getCode()
					+ '\n' + e.getDetails());
		else
			nlapiLogExecution('ERROR', 'unexpected error', e
					.toString());
	}	
	nlapiLogExecution('ERROR', 'totalClearedInvt', totalClearedInvt);	
	return totalClearedInvt;
}
//Case # 20127804� End
/**
 * Generate the fulfillment order number based on the existing fulfillment orders
 *  	by concatenating the sales order number with the maximum value of the fulfillment order number.
 * @param salesOrderId
 * @returns {String}
 */
function fulfillmentOrderDetails(salesOrderId,salesordername)
{
	var salesOrderArray = new Array();
	var searchFulfillment =  getFullFillment(salesOrderId);
	//nlapiLogExecution('DEBUG','searchFulfillment :', searchFulfillment);

	//If searchfulfillment is null it indicates Its a New SO. 
	if(searchFulfillment == null){
		var fulfillmentNumber=salesordername +".1";
	}
	else{
		for(var i=0; i< searchFulfillment.length;i++)
		{
			var fullfillmentlineord = searchFulfillment[i].getValue('custrecord_lineord');
			var lineordsplit = fullfillmentlineord.split('.');
			salesOrderArray[i] = lineordsplit[1];
		}
		//MaxValue returns the max fullfillment order number
		var maximumValue = MaxValue(salesOrderArray);

		//adding 1 to the max fullfillment order number to get the new fullfillment order no.
		var fulfillmentValue = parseFloat(maximumValue) + parseFloat(1);
		nlapiLogExecution('DEBUG','fulfillment Value :', fulfillmentValue);

		//concatinating the fulfillmentvalue to salesorderid to get new fullfillment no.
		var fulfillmentNumber =  salesordername + "." + fulfillmentValue;
		//nlapiLogExecution('DEBUG','fulfillmentNumber :', fulfillmentNumber);
	}
	return fulfillmentNumber;
}

/**
 * searching for records in fullfillment order line
 * @param soId
 * @returns ordlinesearchresults
 */
function getFullFillment(soId)
{
	var ordlinefilters = new Array();
	ordlinefilters[0] = new nlobjSearchFilter('name', null, 'is', soId);
	var ordlinecolumns = new Array();  
	ordlinecolumns[0] = new nlobjSearchColumn('custrecord_lineord').setSort();        
	ordlinecolumns[1] = new nlobjSearchColumn('custrecord_ordline');
	ordlinecolumns[2] = new nlobjSearchColumn('custrecord_ord_qty'); // fulfillment ordered quantity
	var ordlinesearchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, ordlinefilters, ordlinecolumns);
	return ordlinesearchresults;
}

/**
 * Getting the maximum value for the fulfillment order.
 * @param array
 * @returns maximumNumber
 */
function MaxValue(array)
{
	var maximumNumber = array[0];
	for (var i = 0; i < array.length; i++) 
	{
		if (array[i] > maximumNumber) 
		{
			maximumNumber = array[i];
		}
	}
	return maximumNumber;
}

function getAdjustmentType(vWMSLocation1)
{
	var vAdjustType1='';
	var filters = new Array();		
	if(vWMSLocation1!=null && vWMSLocation1!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_adjtype_location', null, 'anyof',[vWMSLocation1]));		
	filters.push(new nlobjSearchFilter('custrecord_adjusttasktype', null,'is','11'));//ADJT
	filters.push(new nlobjSearchFilter('custrecord_adjustdefaultflag', null,'is','T'));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');	

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, filters,columns);		

	if(searchresults != null && searchresults !="")
	{
		if(searchresults.length > 0){
			vAdjustType1 =searchresults[0].getId();
		}
	}

	return vAdjustType1;
}

function createInvAdjRecord(invAdjustCustRecord, locn, sku, skuStatus, lot, qty, lp, adjustType,
		notes, siteLocnValue, resultQty, packCode, iter){
	invAdjustCustRecord.setFieldValue('name', sku + iter);
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_binloc', locn);
	invAdjustCustRecord.setFieldValue('custrecord_ebizskuno', sku);
	invAdjustCustRecord.setFieldValue('custrecord_skustatus', skuStatus);
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_batchno', lot);
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_expeqty', parseFloat(qty).toFixed(4));
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_invjlpno', lp);
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjtype', adjustType);
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjnotes1', notes);
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_siteid', siteLocnValue);
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjustqty', parseFloat(resultQty).toFixed(4));
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_tasktype', 11);
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_recorddate', DateStamp());
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_rectime', TimeStamp());
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_packcode', packCode);
	if(nlapiGetContext().getUser()>0)
		invAdjustCustRecord.setFieldValue('custrecord_ebiz_updateduserno', nlapiGetContext().getUser());
}

function updateWtInLPMaster(containerlp, TotalWeight){
	nlapiLogExecution('DEBUG', 'In to updateStatusInOpenTask function');
	nlapiLogExecution('DEBUG', 'containerlp in updateStatusInLPMaster',containerlp);
	nlapiLogExecution('DEBUG', 'TotalWeight in updateStatusInLPMaster',TotalWeight);
	//nlapiLogExecution('DEBUG', 'totCube in updateStatusInLPMaster',totCube);
	if(containerlp!=null && containerlp!='')
	{
		var recordId="";
		var filtersSO = new Array();
		filtersSO.push(new nlobjSearchFilter('name', null, 'is', containerlp));

		var colsSO = new Array();
		colsSO[0] = new nlobjSearchColumn('name');

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersSO, colsSO);
		nlapiLogExecution('DEBUG', 'searchresults',searchresults);
		if(searchresults){
			for (var i = 0; i < searchresults.length; i++){
				recordId = searchresults[i].getId();
				nlapiLogExecution('DEBUG', 'recordId',recordId);

				var transaction = nlapiLoadRecord('customrecord_ebiznet_master_lp', recordId);
				transaction.setFieldValue('custrecord_ebiz_lpmaster_totwght', parseFloat(TotalWeight).toFixed(4));


				nlapiSubmitRecord(transaction, false, true);
				nlapiLogExecution('DEBUG', 'Pack Complete - Cube and Wt Updated in LP master',recordId);
			}
		}
	}

	nlapiLogExecution('DEBUG', 'Out of updateWtInLPMaster function');
}

function  getIdInvokeNSInventoryAdjustment(item,itemstatus,loc,qty,notes,tasktype,adjusttype,lot)
{
	/*
		var vItemMapLocation =  getItemStatusMapLoc(itemstatus);
		var vAccountNo = getAccountNo(loc,vItemMapLocation);

		var outAdj = nlapiCreateRecord('inventoryadjustment');			
		outAdj.setFieldValue('account', vAccountNo);
		outAdj.setFieldValue('memo', notes);
		outAdj.insertLineItem('inventory', 1);
		outAdj.setLineItemValue('inventory', 'item', 1, item);
		outAdj.setLineItemValue('inventory', 'location', 1, vItemMapLocation);
		outAdj.setLineItemValue('inventory', 'adjustqtyby', 1, parseFloat(qty));
		nlapiSubmitRecord(outAdj, false, true);
		nlapiLogExecution('DEBUG', 'type argument', 'type is create');
	 */

	nlapiLogExecution('ERROR', 'Location info::', loc);
	nlapiLogExecution('ERROR', 'Task Type::', tasktype);
	nlapiLogExecution('ERROR', 'Adjustment Type::', adjusttype);
	nlapiLogExecution('ERROR', 'qty::', qty);

	/*//Case#:20125394  start: Qty exception for serial item when actual qty is 0 related issue fixed 
	if(qty==-1 || qty == null || qty == ''){
		qty = 0;
	}
	//end
	 */	var confirmLotToNS='Y';
	 confirmLotToNS=GetConfirmLotToNS(loc);

	 var vAccountNo;	
	 //Code for fetching account no from stockadjustment custom record	
	 vAccountNo = getStockAdjustmentAccountNo(loc,tasktype,adjusttype);			
	 nlapiLogExecution('DEBUG', 'Fetcht Account from stockadjustment', vAccountNo);		
	 //upto to here stockadjustment

	 var vItemMapLocation =  getItemStatusMapLoc(itemstatus);	
	 nlapiLogExecution('DEBUG', 'vItemMapLocation', vItemMapLocation);	

	 if(vAccountNo==null || vAccountNo=="")
	 {		
		 vAccountNo = getAccountNo(loc,vItemMapLocation);
		 nlapiLogExecution('DEBUG', 'Fetcht Account from Inventory AccountNo', vAccountNo);
	 }

	 var vCost;
	 var vAvgCost;

	 var filters = new Array();          
	 filters.push(new nlobjSearchFilter('internalid', null, 'is',item));
	 //Changed on 30/4/12by suman
	 filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
	 //End of changes as on 30/4/12.
	 var columns = new Array();
	 columns[0] = new nlobjSearchColumn('cost');
	 columns[1] = new nlobjSearchColumn('averagecost');
	 columns[2] = new nlobjSearchColumn('itemid');

	 var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
	 if (itemdetails !=null) 
	 {
		 vItemname=itemdetails[0].getValue('itemid');
		 vCost = itemdetails[0].getValue('cost');
		 nlapiLogExecution('DEBUG', 'vCost', vCost);
		 vAvgCost = itemdetails[0].getValue('averagecost');
		 nlapiLogExecution('DEBUG', 'Average Cost', vAvgCost);	         
	 }		

	 var outAdj = nlapiCreateRecord('inventoryadjustment');			
	 if(vAccountNo!=null&&vAccountNo!=""&&vAccountNo!="null")
		 outAdj.setFieldValue('account', vAccountNo);
	 outAdj.setFieldValue('memo', notes);
	 outAdj.insertLineItem('inventory', 1);
	 outAdj.setLineItemValue('inventory', 'item', 1, item);
	 outAdj.setLineItemValue('inventory', 'location', 1, vItemMapLocation);	
	 outAdj.setLineItemValue('inventory', 'adjustqtyby', 1, parseFloat(qty));
	 if(vAvgCost != null &&  vAvgCost != "")	
	 {
		 nlapiLogExecution('DEBUG', 'into if cond vAvgCost', vAvgCost);
		 outAdj.setLineItemValue('inventory', 'unitcost', 1, vAvgCost);
	 }
	 else
	 {
		 nlapiLogExecution('DEBUG', 'into else cond.unit cost', vCost);
		 outAdj.setLineItemValue('inventory', 'unitcost', 1, vCost);
	 }
	 //outAdj.setFieldValue('subsidiary', 3);
	 //This following line will return 'true' for ONE WORLD Account, 'false' for non-ONE WORLD Account.
	 var subs = nlapiGetContext().getFeature('subsidiaries');
	 nlapiLogExecution('DEBUG', 'subs', subs);
	 if(subs==true)
	 {
		 var vSubsidiaryVal=getSubsidiaryNew(loc);
		 if(vSubsidiaryVal != null && vSubsidiaryVal != '')
			 outAdj.setFieldValue('subsidiary', vSubsidiaryVal);
		 nlapiLogExecution('DEBUG', 'vSubsidiaryVal', vSubsidiaryVal);
	 }
	 //Added by Sudheer on 093011 to send lot# for inventory posting

	 if(lot!=null && lot!='')
	 {
		 nlapiLogExecution('DEBUG', 'lot', lot);

		 vItemname=vItemname.replace(/ /g,"-");
		 var fields = ['recordType', 'custitem_ebizserialin'];
		 var columns = nlapiLookupField('item', item, fields);
		 var vItemType = columns.recordType;
		 nlapiLogExecution('DEBUG','vItemType',vItemType);

		 var serialInflg="F";		
		 serialInflg = columns.custitem_ebizserialin;
		 nlapiLogExecution('DEBUG','serialInflg',serialInflg);

		 nlapiLogExecution('DEBUG', 'lot', lot);
		 nlapiLogExecution('DEBUG', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");

		 //For advanced Bin serial Lot management check
		 var vAdvBinManagement=false;
		 var ctx = nlapiGetContext();
		 if(ctx != null && ctx != '')
		 {
			 if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
				 vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
		 }  
		 nlapiLogExecution('DEBUG', 'vAdvBinManagement', vAdvBinManagement);

		 if (vItemType == "serializedinventoryitem" || vItemType == "serializedassemblyitem" || serialInflg == "T") {
			 nlapiLogExecution('DEBUG','lot',lot);
			 if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
			 {
				 var compSubRecord = outAdj.createCurrentLineItemSubrecord('inventory','inventorydetail');
				 compSubRecord.selectNewLineItem('inventoryassignment');
				 compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', qty);
				 compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lot);

				 compSubRecord.commitLineItem('inventoryassignment');
				 compSubRecord.commit();
			 }
			 else
			 {
				 /*if(parseFloat(qty)<0)
					qty=parseFloat(qty)*(-1);*/
				 outAdj.setLineItemValue('inventory', 'serialnumbers', 1, lot);
				 //outAdj.setCurrentLineItemValue('inventory', 'serialnumbers',lot);
			 }
		 }
		 else if(vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem")
		 {		

			 if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
			 {
				 var compSubRecord = outAdj.createCurrentLineItemSubrecord('inventory','inventorydetail');
				 nlapiLogExecution('DEBUG', 'test1', 'test1');
				 compSubRecord.selectNewLineItem('inventoryassignment');
				 compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', qty);
				 //compSubRecord.setCurrentLineItemValue('inventoryassignment', 'binnumber', vSubsidiaryVal);
				 compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lot);

				 compSubRecord.commitLineItem('inventoryassignment');
				 compSubRecord.commit();
			 }
			 else
			 {
				 if(parseInt(qty)<0)
					 qty=parseInt(qty)*(-1);
				 nlapiLogExecution('DEBUG', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");
				 outAdj.setLineItemValue('inventory', 'serialnumbers', 1, lot + "(" + parseFloat(qty) + ")");
				 //outAdj.setCurrentLineItemValue('inventory', 'serialnumbers', lot + "(" + parseFloat(qty) + ")");
				 nlapiLogExecution('DEBUG', 'LotNowithQty', 'done');
			 }
		 }
	 }

	 //outAdj.commitLineItem('inventory');
	 var internalid = nlapiSubmitRecord(outAdj, true, true);
	 nlapiLogExecution('DEBUG', 'type argument', 'type is create');
	 return internalid;
}


function  InvokeNSInventoryAdjustment(item,itemstatus,loc,qty,notes,tasktype,adjusttype,lot)
{

	nlapiLogExecution('Debug', 'Location info ::', loc);
	nlapiLogExecution('Debug', 'Task Type ::', tasktype);
	nlapiLogExecution('Debug', 'Adjustment Type ::', adjusttype);
	nlapiLogExecution('Debug', 'Qty ::', qty);
	nlapiLogExecution('Debug', 'Item ::', item);

	var confirmLotToNS='Y';
	confirmLotToNS=GetConfirmLotToNS(loc);

	var vAccountNo;	
	//Code for fetching account no from stockadjustment custom record	
	vAccountNo = getStockAdjustmentAccountNo(loc,tasktype,adjusttype);			
	nlapiLogExecution('Debug', 'Fetcht Account from stockadjustment', vAccountNo);		
	//upto to here stockadjustment

	var vItemMapLocation =  getItemStatusMapLoc(itemstatus);	
	nlapiLogExecution('Debug', 'vItemMapLocation', vItemMapLocation);	

	if(vAccountNo==null || vAccountNo=="")
	{		
		vAccountNo = getAccountNo(loc,vItemMapLocation);
		nlapiLogExecution('Debug', 'Fetcht Account from Inventory AccountNo', vAccountNo);
	}

	var vCost;
	var vAvgCost;
	var vItemname;
	var avgcostlot;

	var filters = new Array();          
	filters.push(new nlobjSearchFilter('internalid', null, 'is',item));
	// Changed On 30/4/12 by Suman
	filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
	// End of Changes as On 30/4/12

	//Case # 20149077 Start
	if(loc !=null && loc!='' && loc!='null' && loc!='undefined' && loc > 0)
	{
		filters.push(new nlobjSearchFilter('inventorylocation',null, 'anyof',loc));
	}
	//Case # 20149077 end

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('cost');
	//columns[1] = new nlobjSearchColumn('averagecost');

	//As per email from NS on  02-May-2012  we have changed the code to use 'locationaveragecost' as item unit cost
	//Email from Thad Johnson to Sid
	columns[1] = new nlobjSearchColumn('locationaveragecost');

	columns[2] = new nlobjSearchColumn('itemid');

	//As per recommendation from NS PM Jeff hoffmiester This field 'custitem16' is used as avg cost for Default lot updation in Dec 2011 for TNT
	//columns[3] = new nlobjSearchColumn('custitem16');

	var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
	if (itemdetails !=null) 
	{
		vItemname=itemdetails[0].getValue('itemid');
		vCost = itemdetails[0].getValue('cost');
		nlapiLogExecution('Debug', 'vCost', vCost);
		//vAvgCost = itemdetails[0].getValue('averagecost');
		vAvgCost = itemdetails[0].getValue('locationaveragecost');
		//As per recommendation from NS PM Jeff hoffmiester This field custitem16 is used as avg cost for Default lot updation in Dec 2011 for TNT
		//avgcostlot=itemdetails[0].getValue('custitem16');
		//nlapiLogExecution('Debug', 'avgcostlot', avgcostlot);	
		nlapiLogExecution('Debug', 'Average Cost', vAvgCost);	         
	}		

	var outAdj = nlapiCreateRecord('inventoryadjustment');		
	if(vAccountNo!=null&&vAccountNo!=""&&vAccountNo!="null")
		outAdj.setFieldValue('account', vAccountNo);
	outAdj.setFieldValue('memo', notes);
	outAdj.selectNewLineItem('inventory');
	outAdj.setCurrentLineItemValue('inventory', 'item', item);
	outAdj.setCurrentLineItemValue('inventory', 'location',vItemMapLocation);	
	outAdj.setCurrentLineItemValue('inventory', 'adjustqtyby',parseFloat(qty));
	if(vAvgCost != null &&  vAvgCost != "")	
	{
		nlapiLogExecution('Debug', 'into if cond vAvgCost', vAvgCost);
		outAdj.setCurrentLineItemValue('inventory', 'unitcost',vAvgCost);
	}
	else
	{
		nlapiLogExecution('Debug', 'into else cond.unit cost', vCost);
		outAdj.setCurrentLineItemValue('inventory', 'unitcost', vCost);
	}
	//outAdj.setLineItemValue('inventory', 'unitcost', 1, avgcostlot);//avg cost send to inventory
	//outAdj.setFieldValue('subsidiary', 3);

	//This following line will return 'true' for ONE WORLD Account, 'false' for non-ONE WORLD Account.
	var subs = nlapiGetContext().getFeature('subsidiaries');
	nlapiLogExecution('Debug', 'subs', subs);
	if(subs==true)
	{
		var vSubsidiaryVal=getSubsidiaryNew(loc);
		nlapiLogExecution('Debug','vSubsidiaryVal',vSubsidiaryVal);
		if(vSubsidiaryVal != null && vSubsidiaryVal != '')
			outAdj.setFieldValue('subsidiary', vSubsidiaryVal);
	}


	//Added by Sudheer on 093011 to send lot# for inventory posting
	nlapiLogExecution('Debug', 'lot', lot);
	if(lot!=null && lot!='')
	{
		//vItemname=vItemname.replace(" ","-");
		vItemname=vItemname.replace(/ /g,"-");


		var fields = ['recordType', 'custitem_ebizserialin'];
		var columns = nlapiLookupField('item', item, fields);
		var vItemType = columns.recordType;
		nlapiLogExecution('Debug','vItemType',vItemType);

		var serialInflg="F";		
		serialInflg = columns.custitem_ebizserialin;
		nlapiLogExecution('Debug','serialInflg',serialInflg);

		nlapiLogExecution('Debug', 'lot', lot);
		nlapiLogExecution('Debug', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");
//		For advanced Bin serial Lot management check
		var vAdvBinManagement=false;
		var ctx = nlapiGetContext();
		if(ctx != null && ctx != '')
		{
			if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
				vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
		}  
		nlapiLogExecution('Debug', 'vAdvBinManagement', vAdvBinManagement);

		if (vItemType == "serializedinventoryitem" || vItemType == "serializedassemblyitem" || serialInflg == "T") {
			nlapiLogExecution('Debug','lot',lot);
			if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
			{
				//case 20126071 start : posting serial numbers for AdvanceBinmanagement
				if(lot !=null && lot !='')
				{
					//case # 20126232� start
					var tempQty;

					if(parseFloat(qty)<0)
					{
						tempQty=-1;
					}
					else
					{
						tempQty=1;
					}
					//case # 20126232� end
					var SerialArray = lot.split(',');
					if(SerialArray !=null && SerialArray !='' && SerialArray.length>0)
					{
						nlapiLogExecution('Debug','SerialArray.length',SerialArray.length);
						var compSubRecord = outAdj.createCurrentLineItemSubrecord('inventory','inventorydetail');
						for (var x = 0; x < SerialArray.length; x++)
						{
							nlapiLogExecution('Debug','SerialArray',SerialArray[x]);
							/*if(x==0)
							var compSubRecord = outAdj.createCurrentLineItemSubrecord('inventory','inventorydetail');*/
							compSubRecord.selectNewLineItem('inventoryassignment');
							//case # 20126232� start
							compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', tempQty);
							//case # 20126232� end
							compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', SerialArray[x]);

							compSubRecord.commitLineItem('inventoryassignment');

						}
						compSubRecord.commit();
					}
				}
				//case 20126071 end
			}
			else
			{
				if(parseFloat(qty)<0)
					qty=parseFloat(qty)*(-1);
				nlapiLogExecution('Debug', 'LotNowithQty', lot + "(" + parseInt(qty) + ")");
				//outAdj.setLineItemValue('inventory', 'serialnumbers', 1, lot);
				outAdj.setCurrentLineItemValue('inventory', 'serialnumbers',lot);
			}
		}
		else if (vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem")
		{
			if(confirmLotToNS=='N')
				lot=vItemname;
			if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
			{
				var compSubRecord = outAdj.createCurrentLineItemSubrecord('inventory','inventorydetail');
				compSubRecord.selectNewLineItem('inventoryassignment');
				compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', qty);
				compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lot);

				compSubRecord.commitLineItem('inventoryassignment');
				compSubRecord.commit();
			}
			else
			{
				if(parseFloat(qty)<0)
					qty=parseFloat(qty)*(-1);
				nlapiLogExecution('Debug', 'LotNowithQty', lot + "(" + parseInt(qty) + ")");
				//outAdj.setLineItemValue('inventory', 'serialnumbers', 1, lot + "(" + parseFloat(qty) + ")");
				outAdj.setCurrentLineItemValue('inventory', 'serialnumbers',lot + "(" + parseFloat(qty) + ")");
			}
		}
	}
	outAdj.commitLineItem('inventory');
	var id=nlapiSubmitRecord(outAdj, false, true);
	nlapiLogExecution('Debug', 'type argument', 'type is create');
	nlapiLogExecution('Debug', 'type argument id', id);
	return id;
}




function isPickFaceLocation(item,location)
{
	nlapiLogExecution('DEBUG', 'Into isPickFaceLocation');
	var isPickFace='F';
	var PickFaceFilters = new Array();
	var PickFaceColumns = new Array();

	PickFaceFilters.push(new nlobjSearchFilter('custrecord_pickbinloc', null, 'anyof', location)); 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', item)); 
	PickFaceFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	
	PickFaceColumns[0] = new nlobjSearchColumn('custrecord_autoreplen');
	var PickFaceSearchResults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, PickFaceFilters, PickFaceColumns);
	if(PickFaceSearchResults!=null && PickFaceSearchResults!=''&& PickFaceSearchResults.length>0)
		isPickFace=PickFaceSearchResults[0].getValue('custrecord_autoreplen');

	nlapiLogExecution('DEBUG', 'Return Value',isPickFace);
	nlapiLogExecution('DEBUG', 'Out of isPickFaceLocation');

	return isPickFace;
}

function getOpenReplns(item,location)
{
	nlapiLogExecution('DEBUG', 'Into getOpenReplns');
	var PickFaceSearchResults= new Array();
	var PickFaceFilters = new Array();
	var PickFaceColumns = new Array();

	PickFaceFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8])); // Task Type - RPLN	 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', item)); 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'is', location));	
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));

	PickFaceColumns[0] = new nlobjSearchColumn('custrecord_expe_qty');
	PickFaceColumns[1] = new nlobjSearchColumn('custrecord_taskpriority');

	PickFaceSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, PickFaceFilters, PickFaceColumns);
	if(PickFaceSearchResults!=null&&PickFaceSearchResults!="")
		//nlapiLogExecution('DEBUG', 'Out of getOpenReplns',PickFaceSearchResults.length);

		return PickFaceSearchResults;

}

function getOpenputawayqty(actEndLocation)
{
	nlapiLogExecution('DEBUG','actEndLocation',actEndLocation);
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', actEndLocation);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);				
	filters[2] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return searchresults;
}

function getopenreplens(fulfilmentItem,pfLocationId)
{	
	nlapiLogExecution('DEBUG','fulfilmentItem',fulfilmentItem);

	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8])); // Replen task
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
	filters.push(new nlobjSearchFilter('custrecord_sku', null,'anyof' ,fulfilmentItem));
	filters.push(new nlobjSearchFilter('custrecord_actendloc', null,'anyof' ,pfLocationId));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return searchresults;
}

function generateReplenishment(pfLocid,pfItem,whLocation,WaveNo,taskpriority)
{
	nlapiLogExecution('DEBUG', 'Into generateReplenishment');
	var result = false;
	var resultArray=""; 
	var pendingReplnPutwQty=0;
	var pendingPickqty=0;
	var pickfaceQty=0;
	var replenQty=0;
	var pfMaximumQuantity=0;
	var pfMaximumPickQuantity=0;
	var pendingPutwQty=0;
	var replenrule='';
	var pffixedlp='';
	var pfstatus='';
	var pfRoundQty=0;
	var replenItemArray =  new Array();
	var currentRow = new Array();
	var j = 0;
	var idx = 0;
	var pickfaceQty=0;

	nlapiLogExecution('DEBUG', 'pfLocid',pfLocid);
	nlapiLogExecution('DEBUG', 'pfItem',pfItem);
	nlapiLogExecution('DEBUG', 'whLocation',whLocation);

	var searchputawyqty=getOpenputawayqty(pfLocid);
	if(searchputawyqty!=null && searchputawyqty!='')
	{
		pendingPutwQty = searchputawyqty[0].getValue('custrecord_expe_qty', null, 'sum');
	}

	var searchreplnqty=getopenreplens(pfItem,pfLocid);
	if(searchreplnqty!=null && searchreplnqty!='')
	{
		pendingReplnPutwQty = searchreplnqty[0].getValue('custrecord_expe_qty', null, 'sum');
	}

	var searchpickyqty=getOpenPickqty(pfItem,pfLocid);
	if(searchpickyqty!=null && searchpickyqty!='')
	{
		pendingPickqty = searchpickyqty[0].getValue('custrecord_expe_qty',null,'sum');
	}

	var searchpickfaceQty = getQOHinPFLocation(pfItem,pfLocid);
	if(searchpickfaceQty!=null && searchpickfaceQty!='')
	{
		pickfaceQty=searchpickfaceQty[0].getValue('custrecord_ebiz_qoh',null,'sum');
	}

	var pfSearchResults=getSKUQtyInPickfaceLocns(whLocation,pfItem);
	if(pfSearchResults!=null && pfSearchResults!='')
	{
		for (var n = 0; n < pfSearchResults.length; n++)
		{
			var itemid = pfSearchResults[n].getValue('custrecord_pickfacesku');
			var locationid = pfSearchResults[n].getValue('custrecord_pickbinloc');

			if(pfItem==itemid && pfLocid==locationid)
			{
				replenQty = pfSearchResults[n].getValue('custrecord_replenqty');
				pfMaximumQuantity = pfSearchResults[n].getValue('custrecord_maxqty');
				replenrule = pfSearchResults[n].getValue('custrecord_pickruleid');
				pfMaximumPickQuantity = pfSearchResults[n].getValue('custrecord_maxpickqty');
				pffixedlp = pfSearchResults[n].getValue('custrecord_pickface_ebiz_lpno');
				pfstatus = pfSearchResults[n].getValue('custrecord_pickface_itemstatus');
				pfRoundQty = pfSearchResults[n].getValue('custrecord_roundqty');
			}
		}

		var searchpickfaceQty = getQOHForAllSKUsinPFLocation(pfLocid,pfItem,whLocation);//Case# 201410650
		if(searchpickfaceQty!=null && searchpickfaceQty!='')
		{
			pickfaceQty=searchpickfaceQty[0].getValue('custrecord_ebiz_qoh',null,'sum');
		}

		if(pickfaceQty == null || pickfaceQty == '' || parseInt(pickfaceQty)<0)
			pickfaceQty=0;

		if(pfRoundQty == null || pfRoundQty == '')
			pfRoundQty=1;

		if(pendingPickqty == null || pendingPickqty == '')
			pendingPickqty=0;

		if(pendingReplnPutwQty == null || pendingReplnPutwQty == '')
			pendingReplnPutwQty=0;

		if(replenQty == null || replenQty == '')
			replenQty=0;

		if(pendingPutwQty == null || pendingPutwQty == '')
			pendingPutwQty=0;

		nlapiLogExecution('DEBUG', 'pfMaximumQuantity',pfMaximumQuantity);		 
		nlapiLogExecution('DEBUG', 'pendingReplnPutwQty',pendingReplnPutwQty);
		nlapiLogExecution('DEBUG', 'pendingPickqty',pendingPickqty);
		nlapiLogExecution('DEBUG', 'replenQty',replenQty);
		nlapiLogExecution('DEBUG','pickfaceQty',pickfaceQty);
		nlapiLogExecution('DEBUG','pendingPutwQty',pendingPutwQty);
		nlapiLogExecution('DEBUG','pfRoundQty',pfRoundQty);


		var tempQty = parseFloat(pfMaximumQuantity) -  (parseFloat(pendingReplnPutwQty) - parseFloat(pendingPickqty) + parseFloat(pendingPutwQty)+parseFloat(pickfaceQty));
		//var tempQty = (Math.ceil((parseFloat(pfMaximumQuantity) - (parseFloat(openreplensqty) + parseFloat(pickfaceQty) - parseFloat(pickqty) + parseFloat(putawatqty)))/ parseFloat(pfRoundQty)))*parseFloat(pfRoundQty);
		nlapiLogExecution('DEBUG','tempQty',tempQty);

		var tempwithRoundQty = Math.floor(parseFloat(tempQty)/parseFloat(pfRoundQty));
		if(tempwithRoundQty == null || tempwithRoundQty == '' || isNaN(tempwithRoundQty))
			tempwithRoundQty=0;
		nlapiLogExecution('DEBUG','tempwithRoundQty',tempwithRoundQty);
		tempQty = parseFloat(pfRoundQty)*parseFloat(tempwithRoundQty);
		nlapiLogExecution('DEBUG','tempQty2',tempQty);
		var replenTaskCount = Math.ceil(parseFloat(tempQty) / parseFloat(replenQty));
		nlapiLogExecution('DEBUG','tempQty',tempQty);

		//var tempQty = parseFloat(pfMaximumQuantity) -  parseFloat(pendingReplnPutwQty) - parseFloat(pendingPickqty) + parseFloat(putawatqty)+parseFloat(pickfaceQty);

		nlapiLogExecution('DEBUG','tempQty',tempQty);
//		var replenTaskCount = Math.ceil(parseFloat(tempQty) / parseFloat(replenQty));
//		nlapiLogExecution('DEBUG','replenTaskCount',replenTaskCount);
		currentRow = [pfItem, replenQty, replenrule, pfMaximumQuantity, pfMaximumPickQuantity, pickfaceQty, whLocation, 
		              pfLocid, tempQty, replenTaskCount, pffixedlp,pfstatus,'',pfRoundQty];

		if(idx == 0)
			idx = currentRow.length;

		replenItemArray[j++] = currentRow;

		var taskCount;
		var zonesArray = new Array();

		var replenRulesList = getListForAllReplenZones();

		var replenZoneList = "";
		var replenZoneTextList = "";
		var replenzonestatuslist="";
		var replenzonestatustextlist="";
		var stageLocation='';
		var Reportno = "";

		if (replenRulesList != null){
			for (var w = 0; w < replenRulesList.length; w++){

				if (w == 0){
					replenZoneList += replenRulesList[w].getValue('custrecord_replen_strategy_zone');
					replenZoneTextList += replenRulesList[w].getText('custrecord_replen_strategy_zone');
					replenzonestatuslist +=replenRulesList[w].getValue('custrecord_replen_strategy_itemstatus');
					replenzonestatustextlist +=replenRulesList[w].getText('custrecord_replen_strategy_itemstatus');
				} else {
					replenZoneList += ',';
					replenZoneList += replenRulesList[w].getValue('custrecord_replen_strategy_zone');

					replenZoneTextList += ',';
					replenZoneTextList += replenRulesList[w].getText('custrecord_replen_strategy_zone');

					replenzonestatuslist += ',';
					replenzonestatuslist += replenRulesList[w].getValue('custrecord_replen_strategy_itemstatus');

					replenzonestatustextlist += ',';
					replenzonestatustextlist += replenRulesList[w].getText('custrecord_replen_strategy_itemstatus');
				}
			}
		} 

		var itemList = buildItemListForReplenProcessing(replenItemArray, j);

		var locnGroupList = getZoneLocnGroupsList(replenZoneList);

		var inventorySearchResults = getItemDetailsforRepln(locnGroupList, itemList,replenzonestatuslist);
		nlapiLogExecution('DEBUG', 'inventorySearchResults',inventorySearchResults);
		if (inventorySearchResults != null && inventorySearchResults != ""){
			if (Reportno == null || Reportno == "") {
				Reportno = GetMaxTransactionNo('REPORT');
				Reportno = Reportno.toString();
			}
//			case# 201417183
			//result=replenRuleZoneLocation(inventorySearchResults, replenItemArray, Reportno, stageLocation, whLocation,'',WaveNo,taskpriority);
			resultArray=replenRuleZoneLocation(inventorySearchResults, replenItemArray, Reportno, stageLocation, whLocation,'',WaveNo,taskpriority);
			result=resultArray[0];

		}

	}

	if(result!=true)
	{
		createFailedReplenTask(pfLocid,pfItem,whLocation,WaveNo,pfstatus);
	}

	nlapiLogExecution('DEBUG', 'Return Value',result);
	nlapiLogExecution('DEBUG', 'Out of generateReplenishment');
	return result;
}

function createFailedReplenTask(pfLocid,pfItem,whLocation,WaveNo,pfstatus)
{
	nlapiLogExecution('DEBUG', 'Into createFailedReplenTask');

	try
	{
		var customRecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		customRecord.setFieldValue('custrecord_tasktype', 8);
		customRecord.setFieldValue('custrecordact_begin_date', DateStamp());
		customRecord.setFieldValue('custrecord_actualbegintime', TimeStamp());
		customRecord.setFieldValue('custrecord_actbeginloc', pfLocid);                        
		customRecord.setFieldValue('custrecord_wms_status_flag', 26); 
		customRecord.setFieldValue('custrecord_upd_date', DateStamp());
		customRecord.setFieldValue('custrecord_ebiz_sku_no', pfItem);
		customRecord.setFieldValue('custrecord_sku', pfItem);                        
		customRecord.setFieldValue('name', pfItem);
		customRecord.setFieldValue('custrecord_notes','Insufficient Inventory');

		if(whLocation!=null && whLocation!='')
			customRecord.setFieldValue('custrecord_wms_location',whLocation);			

		if(WaveNo!=null && WaveNo!='')
			customRecord.setFieldValue('custrecord_ebiz_wave_no', WaveNo);

		if(pfstatus!=null && pfstatus!='')
			customRecord.setFieldValue('custrecord_sku_status', pfstatus);

		var openTaskRecordId = nlapiSubmitRecord(customRecord);
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in createFailedReplenTask',exp);
	}

	nlapiLogExecution('DEBUG', 'Out of createFailedReplenTask');
}

function buildItemListForReplenProcessing(replenItemArray, itemArrayLength){
	var replenItemList = new Array();

	for (var p = 0; p < itemArrayLength; p++){
		replenItemList.push(replenItemArray[p][0]);
	}

	return replenItemList;
}

function getSubsidiaryNew(location)
{
	if(location != null && location !='')
	{	
		var vSubsidiary;
		var locRec= nlapiLoadRecord('location', location);
		if(locRec != null && locRec != '')
			vSubsidiary=locRec.getFieldValue('subsidiary');
		if(vSubsidiary != null && vSubsidiary != '')
			return vSubsidiary;
		else 
			return null;
	}
	else
		return null;

}

function getQOHForAllSKUsinPFLocation(actEndLocation,ItemId){

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', actEndLocation));
	//filters.push(new nlobjSearchFilter('custrecord_ebizlocationtype','custrecord_ebiz_inv_binloc', 'anyof', [6,7]));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0)); 
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', ItemId));
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19','3']));
	//if(WHLoc != null && WHLoc !='')
	//filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', WHLoc));
	var columns = new Array();	
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh',null,'sum');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_lp',null,'group');


	var pfSKUInvList = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

	return pfSKUInvList;
}

//The below function is used to determine the rule value for posting the adjustment to netsuite during picking qty exception.
function PostAdjustmenttoNS(Site)
{

	try{
		var filters = new Array();

		//Post Adjustments to NS in Pick Exception?

		filters[0] = new nlobjSearchFilter('custrecord_ebizruletype', null, 'is', 'PICK101');
		filters[1] = new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', ['@NONE@', Site]);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
		columns[1] = new nlobjSearchColumn('custrecord_ebizsite');

		columns[1].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters, columns);	
		if(searchresults != null && searchresults != '')
		{
			if(searchresults[0].getValue('custrecord_ebizrulevalue') != null && searchresults[0].getValue('custrecord_ebizrulevalue') != '')
			{
				return searchresults[0].getValue('custrecord_ebizrulevalue');
			}
			else
				return 'Y';
		}
		else
			return 'Y';
	}
	catch (exp) 
	{
		nlapiLogExecution('DEBUG', 'Exception in GetConfirmLotToNS and returning Y: ', exp);
		return 'Y';
	}	
}
//Inventory transfer

function InvokeNSInventoryTransfer(item,itemstatus,loc,toloc,qty,lot)
{
	try{
		nlapiLogExecution('ERROR', 'Into InvokeNSInventoryTransfer');		
		nlapiLogExecution('ERROR', 'item', item);		
		nlapiLogExecution('ERROR', 'itemstatus', itemstatus);
		nlapiLogExecution('ERROR', 'loc', loc);
		nlapiLogExecution('ERROR', 'toloc', toloc);
		nlapiLogExecution('ERROR', 'qty', qty);
		nlapiLogExecution('ERROR', 'lot', lot);

		var ItemType='';
		var serialOutflg="F";	
		var InvtranfId='';

		if(item!= null && item != "")
		{
			var columns = nlapiLookupField('item', item, [ 'recordType','custitem_ebizserialout' ]);
			ItemType = columns.recordType;
			serialOutflg = columns.custitem_ebizserialout;
		}

		nlapiLogExecution('ERROR', 'ItemType', ItemType);
		var confirmLotToNS='Y';
		confirmLotToNS=GetConfirmLotToNS(loc);
		var vItemname;	

		var filters = new Array();          
		filters.push(new nlobjSearchFilter('internalid', null, 'is',item));
		// Changed On 30/4/12 by Suman
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		// End of Changes as On 30/4/12
		var columns = new Array();		
		columns[0] = new nlobjSearchColumn('itemid');
		var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
		if (itemdetails !=null) 
		{
			vItemname=itemdetails[0].getValue('itemid');
		}
		/* The below code is merged from Lexjet production account on 03-02-2013 by Radhika as part of Standard bundle*/
		var invttransfer = nlapiCreateRecord('inventorytransfer');	

		var subs = nlapiGetContext().getFeature('subsidiaries');		
		if(subs==true)
		{
			var vSubsidiaryVal=getSubsidiaryNew(loc);
			if(vSubsidiaryVal != null && vSubsidiaryVal != '')
				invttransfer.setFieldValue('subsidiary', vSubsidiaryVal);

		}		

		/* Up to here */ 
		invttransfer.setFieldValue('location', loc);//from Location
		invttransfer.setFieldValue('transferlocation', toloc);
		invttransfer.selectNewLineItem('inventory');
		invttransfer.setCurrentLineItemValue('inventory', 'item', item);		
		invttransfer.setCurrentLineItemValue('inventory', 'adjustqtyby', parseFloat(qty));


		//if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem") 
		if(lot!=null && lot!='')
		{
			//vItemname=vItemname.replace(" ","-");
			vItemname=vItemname.replace(/ /g,"-");

			if(confirmLotToNS=='N')
				lot=vItemname;	





			//For advanced Bin serial Lot management check
			var vAdvBinManagement=false;
			var ctx = nlapiGetContext();
			if(ctx != null && ctx != '')
			{
				if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
					vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
			}  
			nlapiLogExecution('ERROR', 'vAdvBinManagement', vAdvBinManagement);

			if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
			{
				var compSubRecord = invttransfer.createCurrentLineItemSubrecord('inventory','inventorydetail');
				nlapiLogExecution('ERROR', 'test1', 'test1');
				compSubRecord.selectNewLineItem('inventoryassignment');
				compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', qty);
				//compSubRecord.setCurrentLineItemValue('inventoryassignment', 'binnumber', vSubsidiaryVal);
				compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lot);

				compSubRecord.commitLineItem('inventoryassignment');
				compSubRecord.commit();
			}
			else
			{
				if(parseInt(qty)<0)
					qty=parseInt(qty)*(-1);

				if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem") 
				{
					nlapiLogExecution('ERROR', 'LotNowithQty', lot);
					invttransfer.setCurrentLineItemValue('inventory', 'serialnumbers',  lot);
				}	
				else
				{
					nlapiLogExecution('ERROR', 'LotNowithQty', lot + "(" + parseInt(qty) + ")");
					invttransfer.setCurrentLineItemValue('inventory', 'serialnumbers', lot + "(" + parseInt(qty) + ")");
				}
			}



		}


		invttransfer.commitLineItem('inventory'); 
		InvtranfId = nlapiSubmitRecord(invttransfer, true, true);
		nlapiLogExecution('ERROR', 'Out of InvokeNSInventoryTransfer',InvtranfId);
		return InvtranfId;
	}


	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in InvokeNSInventoryTransferNew ', exp);	

	}
}
//create inventory with Stock adjustment specified binlocation

function createInvtwithStockadjBinloc(locn, sku, skuStatus, lot, qty, lp, adjustType,
		notes, siteLocnValue, resultQty, packCode, iter){

	nlapiLogExecution('ERROR', 'In to Creat Invt', 'sucess');	
	nlapiLogExecution('ERROR', 'locn ', locn);	
	nlapiLogExecution('ERROR', 'lot ', lot);	

	if(parseFloat(resultQty)<0)
		resultQty=parseFloat(resultQty)*(-1);

	var invCustRecord = nlapiCreateRecord('customrecord_ebiznet_createinv');
	invCustRecord.setFieldValue('name', sku + iter);
	invCustRecord.setFieldValue('custrecord_ebiz_inv_binloc', locn);
	invCustRecord.setFieldValue('custrecord_ebiz_inv_sku', sku);
	invCustRecord.setFieldValue('custrecord_ebiz_inv_sku_status', skuStatus);


	if(lot !=null && lot !='')
	{
		nlapiLogExecution('ERROR', 'lot ', lot);	
		nlapiLogExecution('ERROR', 'sku ', sku);	
		var filterbatch = new Array();
		filterbatch[0] = new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', lot);
		filterbatch[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', sku);

		var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterbatch);
		if(receiptsearchresults!=null)
		{
			var lotid= receiptsearchresults[0].getId();
			invCustRecord.setFieldValue('custrecord_ebiz_inv_lot', lotid);
			nlapiLogExecution('ERROR', 'lotid ', lotid);	
		}
	}

	//invCustRecord.setFieldValue('custrecord_ebiz_inv_lot', lot);
	invCustRecord.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(resultQty).toFixed(4));
	invCustRecord.setFieldValue('custrecord_ebiz_qoh', parseFloat(resultQty).toFixed(4));
	invCustRecord.setFieldValue('custrecord_ebiz_inv_lp', lp);
	invCustRecord.setFieldValue('custrecord_ebiz_adjtype', adjustType);
	invCustRecord.setFieldValue('custrecord_ebiz_inv_note1',notes);
	invCustRecord.setFieldValue('custrecord_ebiz_inv_loc', siteLocnValue);
	invCustRecord.setFieldValue('custrecord_invttasktype', 11);
	invCustRecord.setFieldValue('custrecord_ebiz_inv_recorddate', DateStamp());
	invCustRecord.setFieldValue('custrecord_ebiz_inv_rectime', TimeStamp());
	invCustRecord.setFieldValue('custrecord_ebiz_inv_packcode', packCode);
	invCustRecord.setFieldValue('custrecord_wms_inv_status_flag', 19);

	var invCustId = nlapiSubmitRecord(invCustRecord, false, true);
	nlapiLogExecution('ERROR', 'invCustId ', invCustId);	

}
//Create Open task Record (MOVE task)

function CreateOpentaskRecord(locn, sku, skuStatus, lot, qty, lp, adjustType,
		notes, siteLocnValue, resultQty, packCode, iter,MoveWmsBinlocation)
{

	nlapiLogExecution('ERROR', 'locn ', locn);	
	nlapiLogExecution('ERROR', 'MoveWmsBinlocation ', MoveWmsBinlocation);	


	var vTaskType ="9";
	var opentaskrec = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');

	opentaskrec.setFieldValue('custrecord_ebiz_sku_no', sku);
	opentaskrec.setFieldValue('custrecord_act_qty', parseFloat(resultQty).toFixed(4));//Assigning new quantity
	opentaskrec.setFieldValue('custrecord_lpno', lp);
	opentaskrec.setFieldValue('custrecord_ebiz_lpno', lp);
	if(vTaskType!=null && vTaskType!='')
		opentaskrec.setFieldValue('custrecord_tasktype', vTaskType);
	opentaskrec.setFieldValue('custrecord_actbeginloc', locn);
	opentaskrec.setFieldValue('custrecord_actendloc', MoveWmsBinlocation);	
	opentaskrec.setFieldValue('custrecord_sku', sku);
	opentaskrec.setFieldValue('custrecordact_begin_date', DateStamp());
	opentaskrec.setFieldValue('custrecord_act_end_date', DateStamp());
	if(packCode !=null && packCode!='')
		opentaskrec.setFieldValue('custrecord_packcode', packCode);
	opentaskrec.setFieldValue('custrecord_wms_status_flag', 19);
	opentaskrec.setFieldValue('custrecord_batch_no', lot);
	opentaskrec.setFieldValue('custrecord_upd_ebiz_user_no', nlapiGetContext().getUser());
	opentaskrec.setFieldValue('custrecord_taskassignedto', nlapiGetContext().getUser());
	//	opentaskrec.setFieldValue('custrecord_ebiz_report_no', reportno.toString());
	//added on 111212 by suman
	//Adding fields to update time zones.
	opentaskrec.setFieldValue('custrecord_actualbegintime', TimeStamp());
	opentaskrec.setFieldValue('custrecord_actualendtime', TimeStamp());
	opentaskrec.setFieldValue('custrecord_recordtime', TimeStamp());
	opentaskrec.setFieldValue('custrecord_recordupdatetime', TimeStamp());
	//opentaskrec.setFieldValue('custrecord_current_date', ActualEndDate);
	//opentaskrec.setFieldValue('custrecord_upd_date', ActualEndDate);
	if(siteLocnValue!=null&&siteLocnValue!="")
		opentaskrec.setFieldValue('custrecord_wms_location', siteLocnValue);
	//end of code as of 111212.
	opentaskrec.setFieldValue('custrecord_expe_qty', resultQty);
	opentaskrec.setFieldValue('custrecord_sku_status', skuStatus);
	opentaskrec.setFieldValue('custrecord_ebiz_new_sku_status', skuStatus);
	opentaskrec.setFieldValue('custrecord_ebiz_new_lp', lp);
	opentaskrec.setFieldValue('custrecord_from_lp_no', lp);	

	nlapiLogExecution('ERROR', 'Submitting MOVE record', 'TRN_OPENTASK');

	//commit the record to NetSuite
	var recid = nlapiSubmitRecord(opentaskrec, false, true);


}
//case# 201412443
function CreateNewShortPickForWorkOrder(RcId,vDisplayedQty,pickqty,vdono,recordcount,getEnteredContainerNo,TotalWeight,newLot,remainqty,Systemrules,request)
{
	nlapiLogExecution('DEBUG', 'into CreateNewShortPickOpenTask : ', 'CreateNewShortPickOpenTask');

	var vRemaningqty=0,vname,vCompany1,vcontainer,vcontrolno,vcontainerLPno,vItemno,vEbizItemNo,vEbizWaveNo1,vLineno1,vlpno1,
	vpackcode1,vSkuStatus1,vTaskType1,vUOMid1;
	var vEbizZone1,vUOMLevel1,vEbizReceiptNo1,vInvRefNo1,vEbizUser1,vFromLP1,vEbizOrdNo1=0,vWMSLocation1,vBeginLoc1,vLot;
	var vname2,vsku2,vlineord2,vlineord2,vordlineno2,vOrdQty2,vPackCode2,vPickGenFlag2,vPickGenFlag2,vPickGenQty2,vPickQty2,
	vlineskustatus2,vlinestatusflag2;
	var vUOMid2,vBackOrdFlag2,vParentOrd2,vOrdType2,vCustomer2,vDoCarrirer2,vWMSLocation2,vLineCompany2,vWMSCarrier2,vOrderPriority2;
	var vAdjustType1;
	var pickqtyfinal=0;
	var ordqtyfinal=0;
	var currentContext = nlapiGetContext();
	var currentUserID = currentContext.getUser();

	var Opentasktransaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RcId);

	vname = Opentasktransaction.getFieldValue('name');
	vCompany1 = Opentasktransaction.getFieldValue('custrecord_comp_id');
	vContainer = Opentasktransaction.getFieldValue('custrecord_container');
	vControlno = Opentasktransaction.getFieldValue('custrecord_ebiz_cntrl_no');
	vContainerLPno = Opentasktransaction.getFieldValue('custrecord_container_lp_no');
	vItemno = Opentasktransaction.getFieldValue('custrecord_sku');
	vEbizItemNo = Opentasktransaction.getFieldValue('custrecord_ebiz_sku_no');
	vEbizWaveNo1 = Opentasktransaction.getFieldValue('custrecord_ebiz_wave_no');
	vLineno1 = Opentasktransaction.getFieldValue('custrecord_line_no');
	vlpno1 = Opentasktransaction.getFieldValue('custrecord_lpno');
	vpackcode1 = Opentasktransaction.getFieldValue('custrecord_packcode');
	vSkuStatus1 = Opentasktransaction.getFieldValue('custrecord_sku_status');
	vTaskType1 = Opentasktransaction.getFieldValue('custrecord_tasktype');
	vBeginLoc1 = Opentasktransaction.getFieldValue('custrecord_actbeginloc');
	vUOMLevel1 = Opentasktransaction.getFieldValue('custrecord_uom_level');
	vEbizReceiptNo1 = Opentasktransaction.getFieldValue('custrecord_ebiz_receipt_no');
	vInvRefNo1 = Opentasktransaction.getFieldValue('custrecord_invref_no');
	vFromLP1 = Opentasktransaction.getFieldValue('custrecord_from_lp_no'); 
	vEbizOrdNo1 = Opentasktransaction.getFieldValue('custrecord_ebiz_order_no');
	vWMSLocation1 = Opentasktransaction.getFieldValue('custrecord_wms_location');
	vEbizRule1 = Opentasktransaction.getFieldValue('custrecord_ebizrule_no');
	vEbizMethod1 = Opentasktransaction.getFieldValue('custrecord_ebizmethod_no');
	vEbizZone1 = Opentasktransaction.getFieldValue('custrecord_ebizzone_no');
	vEbizUser1 = Opentasktransaction.getFieldValue('custrecord_ebizuser');
	vLot = Opentasktransaction.getFieldValue('custrecord_batch_no');

	nlapiLogExecution('DEBUG', 'pickqty', pickqty);
	nlapiLogExecution('DEBUG', 'vDisplayedQty', vDisplayedQty);
	nlapiLogExecution('DEBUG', 'remainqty', remainqty);
	if(remainqty==null || remainqty=='' || remainqty=='null' || isNaN(remainqty))
		remainqty=0;
	vRemaningqty = (parseFloat(pickqty)+parseFloat(remainqty))-parseFloat(vDisplayedQty);
	nlapiLogExecution('ERROR', 'vRemaningqty before', vRemaningqty);

	if( Systemrules!=null && Systemrules!=''& Systemrules.length>0 && Systemrules[0]=='Y' && (Systemrules[1] =="STND" ||Systemrules[1] =="NOADJUST" ))
	{
		nlapiLogExecution('ERROR', 'Into STND', vInvRefNo1);
		vRemaningqty = pickqty-vDisplayedQty;
	}
	else
	{
		//for Adjust Binlocation Inventory

		//nlapiLogExecution('ERROR', 'vInvRefNo1', vInvRefNo1);	
		//Case # 20127804�  Start
		/*var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', vInvRefNo1);
		var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
		var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

		if(Invallocqty==null || Invallocqty=='')
			Invallocqty=0;

		nlapiLogExecution('ERROR', 'Invallocqty', Invallocqty);
		nlapiLogExecution('ERROR', 'InvQOH', InvQOH);

//		if(remainqty ==0)
//		vRemaningqty = parseFloat(InvQOH)- parseFloat(vActqty);
//		else
//		vRemaningqty = parseFloat(InvQOH-vActqty)+parseFloat(remainqty);

		vRemaningqty = parseFloat(Invallocqty)-parseFloat(InvQOH);

		var vNewQOH =  Invallocqty;

		if(vNewQOH<=0)
		{
			nlapiLogExecution('Debug', 'Deleting record from inventory if QOH becomes zero', vInvRefNo1);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', vInvRefNo1);			
		}
		else
		{
			Invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(vNewQOH));  
			Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
			Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

			var invtrecid = nlapiSubmitRecord(Invttran, false, true);
			nlapiLogExecution('Debug', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);
		}*/

		vRemaningqty=clearBinlevelInvt(vItemno,vBeginLoc1,vWMSLocation1);
		//Case # 20127804�  End
	}

	nlapiLogExecution('ERROR', 'vRemaningqty after', vRemaningqty);

	var vRemPickQty = parseFloat(vDisplayedQty)- parseFloat(pickqty);

	var PickExpOpenTaskRec=nlapiCreateRecord('customrecord_ebiznet_trn_opentask');

	PickExpOpenTaskRec.setFieldValue('name', vname);
	PickExpOpenTaskRec.setFieldValue('custrecordact_begin_date', DateStamp());
	PickExpOpenTaskRec.setFieldValue('custrecord_comp_id', vCompany1);
	PickExpOpenTaskRec.setFieldValue('custrecord_container', vContainer);
	PickExpOpenTaskRec.setFieldValue('custrecord_current_date', DateStamp());
	PickExpOpenTaskRec.setFieldValue('custrecord_ebiz_cntrl_no', vControlno);
	PickExpOpenTaskRec.setFieldValue('custrecord_container_lp_no', vContainerLPno);
	PickExpOpenTaskRec.setFieldValue('custrecord_sku', vItemno);
	PickExpOpenTaskRec.setFieldValue('custrecord_ebiz_sku_no', vEbizItemNo);
	PickExpOpenTaskRec.setFieldValue('custrecord_ebiz_wave_no', vEbizWaveNo1);
	PickExpOpenTaskRec.setFieldValue('custrecord_expe_qty', parseFloat(vRemPickQty).toFixed(4));
	PickExpOpenTaskRec.setFieldValue('custrecord_line_no', vLineno1);
	PickExpOpenTaskRec.setFieldValue('custrecord_lpno', vlpno1);
	PickExpOpenTaskRec.setFieldValue('custrecord_packcode', vpackcode1);
	PickExpOpenTaskRec.setFieldValue('custrecord_sku_status', vSkuStatus1);
	PickExpOpenTaskRec.setFieldValue('custrecord_wms_status_flag', '30'); //Status Flag - Short Pick 
	PickExpOpenTaskRec.setFieldValue('custrecord_tasktype', vTaskType1);
	PickExpOpenTaskRec.setFieldValue('custrecord_totalcube', '');
	PickExpOpenTaskRec.setFieldValue('custrecord_total_weight', '');
	PickExpOpenTaskRec.setFieldValue('custrecord_ebizzone_no', vEbizZone1);
	PickExpOpenTaskRec.setFieldValue('custrecord_uom_level', vUOMLevel1);
	PickExpOpenTaskRec.setFieldValue('custrecord_ebiz_receipt_no', vEbizReceiptNo1);
	PickExpOpenTaskRec.setFieldValue('custrecord_actualbegintime', TimeStamp());
	PickExpOpenTaskRec.setFieldValue('custrecord_invref_no', vInvRefNo1);
	PickExpOpenTaskRec.setFieldValue('custrecord_from_lp_no', vFromLP1);
	PickExpOpenTaskRec.setFieldValue('custrecord_ebiz_order_no', vEbizOrdNo1);
	PickExpOpenTaskRec.setFieldValue('custrecord_wms_location', vWMSLocation1);
	PickExpOpenTaskRec.setFieldValue('custrecord_ebizrule_no', vEbizRule1);
	PickExpOpenTaskRec.setFieldValue('custrecord_ebizmethod_no', vEbizMethod1);
	PickExpOpenTaskRec.setFieldValue('custrecord_ebizuser', currentUserID);					
	PickExpOpenTaskRec.setFieldValue('custrecord_upd_ebiz_user_no',currentUserID);
	var vemployee = request.getParameter('custpage_employee');

	if (vemployee != null && vemployee != "") 
	{
		PickExpOpenTaskRec.setFieldValue('custrecord_taskassignedto',vemployee);
	} 
	else 
	{
		PickExpOpenTaskRec.setFieldValue('custrecord_taskassignedto',currentUserID);
	}
	nlapiLogExecution('DEBUG', 'before CreateNewShortPickOpenTask : ', 'before CreateNewShortPickOpenTask');


	//nlapiLogExecution('DEBUG', 'vRemaningqtyfinal', vRemaningqtyfinal);
	nlapiLogExecution('DEBUG', 'vRemaningqty', vRemaningqty);
	nlapiLogExecution('DEBUG', 'vWMSLocation1', vWMSLocation1);


	updateWtInLPMaster(getEnteredContainerNo,TotalWeight);

	vAdjustType1=getAdjustmentType(vWMSLocation1);

	var vnotes1="This is from Confirm picks qty Exception";

	nlapiSubmitRecord(PickExpOpenTaskRec, false, true);

//	var getfulfillrecid = nlapiSubmitRecord(PickExpFulfillOrdLineRec, false, true);

	//FulfillOrdLinetransaction.setFieldValue('custrecord_linenotes1', vnotes1);
	//var getOldfulfillrecid = nlapiSubmitRecord(FulfillOrdLinetransaction, false, true);
	if(parseFloat(vRemaningqty)!=0)
	{
		/*var invAdjustCustRecord = nlapiCreateRecord('customrecord_ebiznet_invadj');
		createInvAdjRecord(invAdjustCustRecord, vBeginLoc1, vItemno, vSkuStatus1,newLot , vDisplayedQty, vlpno1,
				vAdjustType1, vnotes1, vWMSLocation1, parseFloat(vRemaningqty), vpackcode1, recordcount);
		nlapiLogExecution('ERROR', 'adjustType in adjust main ', vAdjustType1);	*/    
		var tasktype=11; //11 is the internalid value of tasktype "ADJT"; 

		nlapiLogExecution('DEBUG', 'pickqty ', pickqty);	
		nlapiLogExecution('DEBUG', 'parseFloat(pickqty) ', parseFloat(pickqty));	
		nlapiLogExecution('DEBUG', 'vDisplayedQty ', vDisplayedQty);	
		nlapiLogExecution('DEBUG', 'parseFloat(vDisplayedQty) ', parseFloat(vDisplayedQty));	
		nlapiLogExecution('DEBUG', 'vRemaningqty ', vRemaningqty);	
		nlapiLogExecution('DEBUG', 'parseFloat(vRemaningqty) ', parseFloat(vRemaningqty));	
		if(Systemrules!=null && Systemrules!='')
		{
			nlapiLogExecution('ERROR', 'Systemrules[0] ', Systemrules[0]);
			nlapiLogExecution('ERROR', 'Systemrules[1]', Systemrules[1]);
			nlapiLogExecution('ERROR', 'Systemrules[1]', Systemrules[1]);
			nlapiLogExecution('ERROR', 'vWMSLocation1', vWMSLocation1);
		}


		// case# 201412518
		//if(PostAdjustmenttoNS(vWMSLocation1)=='Y')
		var PostAdjustmenttoNS = getSystemRule('Post Adjustment to NS during Pick Exception?');
		if(PostAdjustmenttoNS=='Y')
		{
			var fields = ['recordType', 'custitem_ebizserialin'];
			var columns = nlapiLookupField('item', vItemno, fields);
			var vItemType = columns.recordType;
			nlapiLogExecution('ERROR','vItemType',vItemType);
			var serialInflg="F";		
			serialInflg = columns.custitem_ebizserialin;
			nlapiLogExecution('ERROR','serialInflg',serialInflg);
			if (vItemType == "serializedinventoryitem" || vItemType == "serializedassemblyitem" || serialInflg == "T") {
				var filtersser = new Array();


				if(vItemno !=null && vItemno!='')
					filtersser.push(new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', vItemno));

				filtersser.push( new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['3','19']));

				if(vlpno1 !=null && vlpno1!='')
				{
					//filtersser[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', getContainerLP);
					filtersser.push(new nlobjSearchFilter('custrecord_serialparentid', null, 'is', vlpno1));

				}
				var cols= new Array();
				cols[0]=new nlobjSearchColumn('custrecord_serialnumber');
				var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser,cols);
				nlapiLogExecution('DEBUG', 'SrchRecord', SrchRecord);
				var tempqty=parseFloat(vDisplayedQty)-parseFloat(pickqty);
				nlapiLogExecution('DEBUG', 'tempqty', tempqty);
				var serialNumbers= new Array();

				if(SrchRecord !=null && SrchRecord!='')
				{
					for(var k=0;k<SrchRecord.length && k<parseFloat(tempqty);k++)
					{
						serialNumbers[serialNumbers.length]=SrchRecord[k].getValue('custrecord_serialnumber');
						var SerialRecord= nlapiLoadRecord('customrecord_ebiznetserialentry',SrchRecord[k].getId() );
						SerialRecord.setFieldValue('custrecord_serialstatus','D');
						nlapiSubmitRecord(SerialRecord);
					}
				}


				newLot=serialNumbers.toString();
				nlapiLogExecution('ERROR', 'newLot ', newLot);	
			}//Case # 20127171�Start
			else if (vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem")
			{
				newLot=vLot;
			}//Case # 20127171�End
			/*var netsuiteadjustId = getIdInvokeNSInventoryAdjustment(vItemno,vSkuStatus1,vWMSLocation1,parseFloat(vRemaningqty),vnotes1,tasktype,vAdjustType1,newLot);
			nlapiLogExecution('DEBUG', 'netsuiteadjustId main11', netsuiteadjustId);	
			invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjustno',netsuiteadjustId);*/

			if(Systemrules!=null && Systemrules!='' && Systemrules.length>0 && Systemrules[0]=='Y' && Systemrules[1] !="NOADJUST")
			{


				//to fetch Stock adjustment Details from Stock Adjustments 
				var adjtasktype=22; //Tasktype : LOST
				var GetStockAdjustResults = getStockAdjustmentDetails(vWMSLocation1,adjtasktype,null);
				if(GetStockAdjustResults !=null && GetStockAdjustResults !='' && GetStockAdjustResults.length >0)
				{
					nlapiLogExecution('ERROR', 'GetStockAdjustResults.length ', GetStockAdjustResults.length);	
					var transactiontype =  GetStockAdjustResults[0].getValue('custrecord_ebiz_adjtype_trantype');
					var MapNSlocation =  GetStockAdjustResults[0].getValue('custrecord_ebiz_adjtype_mapnslocation');
					var MoveWmsBinlocation =  GetStockAdjustResults[0].getValue('custrecord_ebiz_adtype_movewmsbinloc');

					nlapiLogExecution('ERROR', 'transactiontype ', transactiontype);	
					nlapiLogExecution('ERROR', 'MapNSlocation', MapNSlocation);	
					nlapiLogExecution('ERROR', 'MoveWmsBinlocation', MoveWmsBinlocation);

					//tranaction Type =" Inventory Transfer", perform Inventory Transfer
					if(transactiontype == "2")
					{

						var transferQty = -(vRemaningqty);
						nlapiLogExecution('ERROR', 'transferQty', transferQty);

						var InvtranfId = InvokeNSInventoryTransfer(vItemno,vSkuStatus1,vWMSLocation1,MapNSlocation,parseFloat(transferQty),newLot);
						var Invtnotes1 = "Created from Invt Transfers";
						//create inventory mapped location
						createInvtwithStockadjBinloc(MoveWmsBinlocation, vItemno, vSkuStatus1,newLot , vDisplayedQty, vlpno1,
								vAdjustType1, Invtnotes1, vWMSLocation1, parseFloat(transferQty), vpackcode1, recordcount);

						CreateOpentaskRecord(vBeginLoc1, vItemno, vSkuStatus1,newLot, vDisplayedQty, vlpno1,vAdjustType1, Invtnotes1, vWMSLocation1, parseFloat(transferQty), vpackcode1, recordcount,MoveWmsBinlocation);


					}
					else
					{
						// Inventory Adjustment

						var invAdjustCustRecord = nlapiCreateRecord('customrecord_ebiznet_invadj');
						createInvAdjRecord(invAdjustCustRecord, vBeginLoc1, vItemno, vSkuStatus1,newLot , vDisplayedQty, vlpno1,
								vAdjustType1, vnotes1, vWMSLocation1, parseFloat(vRemaningqty), vpackcode1, recordcount);
						nlapiLogExecution('ERROR', 'adjustType in adjust main ', vAdjustType1);	
						var netsuiteadjustId='';
						if(pickqty!=0)
							netsuiteadjustId = InvokeNSInventoryAdjustment(vItemno,vSkuStatus1,vWMSLocation1,parseFloat(vRemaningqty),vnotes1,tasktype,vAdjustType1,newLot);
						//	var netsuiteadjustId = getIdInvokeNSInventoryAdjustment(vItemno,vSkuStatus1,vWMSLocation1,parseFloat(vRemaningqty),vnotes1,tasktype,vAdjustType1,newLot);
						nlapiLogExecution('ERROR', 'netsuiteadjustId main11', netsuiteadjustId);
						var adjustnote ="Not posted to GL Account";
						if(netsuiteadjustId == null || netsuiteadjustId =='')
							invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjnotes1',adjustnote);
						if(netsuiteadjustId != null && netsuiteadjustId !='')
							invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjustno',netsuiteadjustId);
						invAdjustCustRecord.setFieldValue('custrecord_ebiz_cntrlno',vEbizOrdNo1);

						// Update Inventory Adjustment custom record
						var invAdjRecId = nlapiSubmitRecord(invAdjustCustRecord, false, true);
						nlapiLogExecution('ERROR', 'Inventory Adjust Record Insertion / Update for invAdjustRecId',
								invAdjRecId + ' is Success');

					}



				}
				else
				{

					nlapiLogExecution('ERROR', 'into else ','sucess');

					var invAdjustCustRecord = nlapiCreateRecord('customrecord_ebiznet_invadj');
					createInvAdjRecord(invAdjustCustRecord, vBeginLoc1, vItemno, vSkuStatus1,newLot , vDisplayedQty, vlpno1,
							vAdjustType1, vnotes1, vWMSLocation1, parseFloat(vRemaningqty), vpackcode1, recordcount);
					nlapiLogExecution('ERROR', 'adjustType in adjust main ', vAdjustType1);	
					var netsuiteadjustId='';
					if(pickqty!=0)
						netsuiteadjustId = InvokeNSInventoryAdjustment(vItemno,vSkuStatus1,vWMSLocation1,parseFloat(vRemaningqty),vnotes1,tasktype,vAdjustType1,newLot);
					//	var netsuiteadjustId = getIdInvokeNSInventoryAdjustment(vItemno,vSkuStatus1,vWMSLocation1,parseFloat(vRemaningqty),vnotes1,tasktype,vAdjustType1,newLot);
					nlapiLogExecution('ERROR', 'netsuiteadjustId main11', netsuiteadjustId);
					var adjustnote ="Not posted to GL Account";
					if(netsuiteadjustId == null || netsuiteadjustId =='')
						invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjnotes1',adjustnote);
					if(netsuiteadjustId != null && netsuiteadjustId !='')
						invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjustno',netsuiteadjustId);
					invAdjustCustRecord.setFieldValue('custrecord_ebiz_cntrlno',vEbizOrdNo1);

					// Update Inventory Adjustment custom record
					var invAdjRecId = nlapiSubmitRecord(invAdjustCustRecord, false, true);
					nlapiLogExecution('ERROR', 'Inventory Adjust Record Insertion / Update for invAdjustRecId',
							invAdjRecId + ' is Success');


				}


//				var netsuiteadjustId = getIdInvokeNSInventoryAdjustment(vItemno,vSkuStatus1,vWMSLocation1,parseFloat(vRemaningqty),vnotes1,tasktype,vAdjustType1,newLot);
//				nlapiLogExecution('ERROR', 'netsuiteadjustId main11', netsuiteadjustId);	
//				invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjustno',netsuiteadjustId);
			}


		}

		/*invAdjustCustRecord.setFieldValue('custrecord_ebiz_cntrlno',vEbizOrdNo1);

		// Update Inventory Adjustment custom record
		var invAdjRecId = nlapiSubmitRecord(invAdjustCustRecord, false, true);
		nlapiLogExecution('DEBUG', 'Inventory Adjust Record Insertion / Update for invAdjustRecId',
				invAdjRecId + ' is Success');*/
	}
}
