
/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_WO_PickingItem.js,v $ 
 *     	   $Revision: 1.1.2.3.4.6 $
 *     	   $Date: 2014/08/15 15:36:35 $
 *     	   $Author: snimmakayala $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_WO_PickingItem.js,v $
 * Revision 1.1.2.3.4.6  2014/08/15 15:36:35  snimmakayala
 * Case: 20149972 & 20149973
 * JAWBONE WO PICKING FIXES
 *
 * Revision 1.1.2.3.4.5  2014/06/13 08:53:38  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.3.4.4  2014/05/30 00:34:24  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.3.4.3  2014/02/05 15:09:07  sponnaganti
 * case# 20127056
 * For Keyboard Enter button to work
 *
 * Revision 1.1.2.3.4.2  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.3.4.1  2013/03/08 14:38:41  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.1.2.3  2012/12/07 14:29:05  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related to UPC code for the Item
 *
 * Revision 1.1.2.2  2012/12/03 15:42:18  rmukkera
 * CASE201112/CR201113/LOG2012392
 * Issue fix Reference error getwaveno not defined
 *
 * Revision 1.1.2.1  2012/11/23 09:10:51  skreddy
 * CASE201112/CR201113/LOG201121
 * RF version of WO Assembly building process
 *
 * Revision 1.13.2.21.4.9  2012/11/01 14:55:23  schepuri
 *
 *
 *****************************************************************************/
function PickingItem(request, response){
	if (request.getMethod() == 'GET') {
		var getFetchedBeginLocation = request.getParameter('custparam_beginLocation');


		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('Error', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7,st8;

		if( getLanguage == 'es_ES')
		{
			st1 = "ART&#205;CULO:";
			st2 = "INGRESAR / ESCANEO DEL ART&#205;CULO";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";
			st5 = "OVERRIDE";
			st6 = "SKIP";
			st8 ="DESCRIPCI&#211;N DEL ART&#205;CULO";
			st9 = "CONTENEDOR LP:";
		}
		else
		{
			st1 = "ITEM: ";
			st2 = "ENTER/SCAN ITEM ";
			st3 = "SEND";
			st4 = "PREV";
			st5 = "OVERRIDE";
			st6 = "SKIP";
			st8 = "ITEM DESCRIPTION:";
			st9 = " LP #:";
		}

		var getWOid = request.getParameter('custparam_woid');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocation = request.getParameter('custparam_beginLocation');
		var getItem = request.getParameter('custparam_item');
		var getItemName = request.getParameter('custparam_itemname');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var getFetchedLocation = request.getParameter('custparam_beginLocationname');
		var vClusterno = request.getParameter('custparam_clusterno');
		var vBatchno = request.getParameter('custparam_batchno');
		//var vBatchno = request.getParameter('custparam_batchno');
		var whLocation = request.getParameter('custparam_whlocation');
		var whCompany = request.getParameter('custparam_whcompany');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var NextItemId=request.getParameter('custparam_nextiteminternalid');
		var pickType=request.getParameter('custparam_picktype');

		var name=request.getParameter('name');
		var ContainerSize=request.getParameter('custparam_containersize');
		var getOrderNo=request.getParameter('custparam_ebizordno');
		var getZoneNo=request.getParameter('custparam_ebizzoneno');
		var getnextExpectedQuantity = request.getParameter('custparam_nextexpectedquantity');
		var ItemStatus = request.getParameter('custparam_itemstatus');

		nlapiLogExecution('ERROR', 'getnextExpectedQuantity', getnextExpectedQuantity);
		nlapiLogExecution('ERROR', 'getWOid', getWOid);
		nlapiLogExecution('ERROR', 'getItemInternalId', getItemInternalId);
		nlapiLogExecution('DEBUG', 'getBeginLocation', NextLocation);
		nlapiLogExecution('DEBUG', 'NextItemId', NextItemId);
		nlapiLogExecution('ERROR', 'ItemStatus', ItemStatus);
		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');
		if(getFetchedLocation==null)
		{
			getFetchedLocation=NextLocation;
		}
		if(getItemInternalId==null)
		{
			getItemInternalId=NextItemId;
		}

		var Itype = nlapiLookupField('item', getItemInternalId, 'recordType');
		var ItemRec = nlapiLoadRecord(Itype, getItemInternalId);
		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');        
		var getEnteredLocation = request.getParameter('custparam_endlocation');
		var Itemdescription='';
		nlapiLogExecution('ERROR', 'getItem', getItem);	
		nlapiLogExecution('ERROR', 'getItemDescription', getItemDescription);
		var getItemName = ItemRec.getFieldValue('itemid');
		//var getItemName = ItemRec.getFieldValue('externalid');

		if(ItemRec.getFieldValue('description') != null && ItemRec.getFieldValue('description') != "")
		{	
			Itemdescription = ItemRec.getFieldValue('description');
		}
		else if(ItemRec.getFieldValue('salesdescription') != null && ItemRec.getFieldValue('salesdescription') != "")
		{	
			Itemdescription = ItemRec.getFieldValue('salesdescription');
		}	
		Itemdescription = Itemdescription.substring(0, 20);

		var itemdescArray = new Array();
		var itemdesc='';
		itemdescArray = Itemdescription.split(' ');
		for(j=0;j<itemdescArray.length-1;j++)
		{
			//if(itemdesc == '')
			//	itemdesc = itemdescArray[j];
			//else
			itemdesc = itemdesc + " " + itemdescArray[j];
		}

		nlapiLogExecution('ERROR', 'getItem', getItemName);
		nlapiLogExecution('ERROR', 'getItemdescription', Itemdescription);
		nlapiLogExecution('ERROR', 'Next Location', NextLocation);

		// var BinLocationRec = nlapiLoadRecord('customrecord_ebiznet_location', getFetchedBeginLocation);

		//  var getFetchedLocation = BinLocationRec.getFieldValue('custrecord_ebizlocname');
		//   nlapiLogExecution('DEBUG', 'Location Name is', getFetchedLocation);
		//case# 20127056 starts (Now form name is passed correctly in function to work keyboard enter button)
		var functionkeyHtml=getFunctionkeyScript('_ebiz_rf_wo_picking_item'); 
		//case# 20127056 end
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";   
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		//html = html + " document.getElementById('enteritem').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_ebiz_rf_wo_picking_item' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";		
		html = html + "				<td align = 'left'>"+ st1 +"<label>" + getItemName + "</label><br>"+ st8 +"<label>" + Itemdescription + "</label>";//<br>REMAINING QTY: <label>" + remqty + "</label>";
		nlapiLogExecution('ERROR', 'getContainerLpNo', getContainerLpNo);
		if(getContainerLpNo != null && getContainerLpNo != '')
			html = html + "				<br>"+ st9 +"<label>" + getContainerLpNo + "</label>";
		html = html + "				<input type='hidden' name='hdnWOid' value=" + getWOid + ">";
		html = html + "				<input type='hidden' name='hdnItemName' value='" + getItemName + "'>";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value='" + getFetchedLocation + "'>";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vClusterno + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value=" + vBatchno + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnwhCompany' value=" + whCompany + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEnteredLocation' value=" + getEnteredLocation + ">";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationid' value=" + getBeginLocation + ">";
		html = html + "				<input type='hidden' name='hdnebizzoneno' value=" + getZoneNo + ">";
		html = html + "				<input type='hidden' name='hdnextExpectedQuantity' value=" + getnextExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnitemstatus' value=" + ItemStatus + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st2;//ENTER/SCAN ITEM 
		html = html + "				<input type='hidden' name='hdnnext' value=" + NextLocation + ">";
		html = html + "				<input type='hidden' name='hdnnextitem' value=" + NextItemId + ">";	
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";	
		html = html + "				<input type='hidden' name='hdnName' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + ContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnebizOrdNo' value=" + getOrderNo + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enteritem' id='enteritem' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 +" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.focus();this.disabled=true; return false'/>";
		html = html + "					"+ st4 +" <input name='cmdPrevious' type='submit' value='F7'/>";
		//	html = html + "					"+ st5 +" <input name='cmdOverride' type='submit' value='F11'/>";
		html = html + "					"+ st6 +" <input name='cmdSKIP' type='submit' value='F12'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enteritem').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('ERROR', 'Into Response', TimeStampinSec());

		var getLanguage = request.getParameter('hdngetLanguage');

		var st7,st8;

		if( getLanguage == 'es_ES')
		{
			st7 = "ART&#205;CULO INV&#193;LIDO";
			st8 = "DESCRIPCI&#211;N DEL ART&#205;CULO:";

		}
		else
		{
			st7 = "INVALID ITEM";
			st8 = "ITEM DESCRIPTION: ";
		}

		var getEnteredItem = request.getParameter('enteritem');
		var getWOid = request.getParameter('hdnWOid');
		var getRecordInternalId = request.getParameter('hdnRecordInternalId');
		var getContainerLpNo = request.getParameter('hdnContainerLpNo');
		var getExpectedQuantity = request.getParameter('hdnExpectedQuantity');
		var getBeginLocation = request.getParameter('hdnBeginLocation');
		var getBeginLocationid = request.getParameter('hdnBeginLocationid');
		var getItem = request.getParameter('hdnItem');
		var getItemName = request.getParameter('hdnItemName');
		var getItemDescription = request.getParameter('hdnItemDescription');
		var getItemInternalId = request.getParameter('hdnItemInternalId');
		var getDOLineId = request.getParameter('hdnDOLineId');
		var getInvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var getOrderLineNo = request.getParameter('hdnOrderLineNo');
		var vClusterNo = request.getParameter('hdnClusterNo');
		var vBatchNo = request.getParameter('hdnbatchno');	
		var whLocation = request.getParameter('hdnwhlocation');
		var vZoneId=request.getParameter('hdnebizzoneno');
		var nextexpqty=request.getParameter('hdnextExpectedQuantity');
		var whCompany = request.getParameter('hdnwhCompany');
		var RecCount=request.getParameter('hdnRecCount');
		var getNext=request.getParameter('hdnnext');
		var getNextItemId=request.getParameter('hdnnextitem');
		var OrdName=request.getParameter('hdnName');
		var ContainerSize=request.getParameter('hdnContainerSize');
		var ebizOrdNo=request.getParameter('hdnebizOrdNo');

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');
		var optskipEvent = request.getParameter('cmdSKIP');

		// This variable is to hold the SO# entered.
		var SOarray = new Array();
		SOarray["custparam_language"] = getLanguage;
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		SOarray["custparam_error"] = st7;//'INVALID ITEM';
		SOarray["custparam_screenno"] = 'WOItem';
		SOarray["custparam_whlocation"] = whLocation;
		SOarray["custparam_woid"] = getWOid;
		SOarray["custparam_recordinternalid"] = getRecordInternalId;
		SOarray["custparam_containerlpno"] = getContainerLpNo;
		SOarray["custparam_expectedquantity"] = getExpectedQuantity;
		SOarray["custparam_beginLocation"] = getBeginLocation;
		SOarray["custparam_beginLocationname"] = getBeginLocation;
		SOarray["custparam_item"] = getItem;
		SOarray["custparam_itemname"] = getItemName;
		SOarray["custparam_itemdescription"] = getItemDescription;
		SOarray["custparam_iteminternalid"] = getItemInternalId;
		SOarray["custparam_dolineid"] = getDOLineId;
		SOarray["custparam_invoicerefno"] = getInvoiceRefNo;
		SOarray["custparam_orderlineno"] = getOrderLineNo;
		SOarray["custparam_clusterno"] = vClusterNo;
		SOarray["custparam_batchno"] = vBatchNo;
		SOarray["custparam_noofrecords"] = RecCount;
		SOarray["custparam_nextlocation"] = getNext;
		SOarray["custparam_nextiteminternalid"] = getNextItemId;
		SOarray["name"] = OrdName;
		SOarray["custparam_containersize"] = ContainerSize;
		SOarray["custparam_ebizordno"] = ebizOrdNo;
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
		SOarray["custparam_nextexpectedquantity"] = nextexpqty;
		SOarray["custparam_Actbatchno"] = "";
		SOarray["custparam_Expbatchno"] = "";
		SOarray["custparam_itemstatus"] = request.getParameter('hdnitemstatus');
		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  request.getParameter('hdnebizzoneno');
		}
		else
			SOarray["custparam_ebizzoneno"] = '';
		var vSkipId=request.getParameter('hdnskipid');
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept SO #.
		if (optedEvent == 'F7') {
			//response.sendRedirect('SUITELET', 'customscript_rf_picking_container_no', 'customdeploy_rf_picking_container_no_di', false, SOarray);
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_location', 'customdeploy_ebiz_rf_wo_picking_loc_di', false, SOarray);
		}
		else if(request.getParameter('cmdSKIP') == 'F12')
		{
			var vPickType=request.getParameter('hdnpicktype');
			nlapiLogExecution('ERROR', 'vPickType',vPickType);
			var RecCount;
			var filterOpentask = new Array();
			filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', getWOid));
			filterOpentask.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
			filterOpentask.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));
			if(vZoneId!=null && vZoneId!="" && vZoneId!="null")
			{
				filterOpentask.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));
			}
			/***  The below code is merged from Endochoice account on 07thMar13  by Santosh  as part of Standard bundle ***/

			var WOcolumns = new Array();
			WOcolumns.push(new nlobjSearchColumn('custrecord_line_no'));
			WOcolumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
			WOcolumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
			WOcolumns.push(new nlobjSearchColumn('custrecord_sku'));
			WOcolumns.push(new nlobjSearchColumn('custrecord_expe_qty'));				
			WOcolumns.push(new nlobjSearchColumn('custrecord_lpno'));
			WOcolumns.push(new nlobjSearchColumn('custrecord_batch_no'));
			WOcolumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
			WOcolumns.push(new nlobjSearchColumn('custrecord_act_qty'));   
			WOcolumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
			WOcolumns.push(new nlobjSearchColumn('custrecord_invref_no'));
			WOcolumns.push(new nlobjSearchColumn('custrecord_wms_location'));
			WOcolumns.push(new nlobjSearchColumn('custrecord_comp_id'));
			WOcolumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));				
			WOcolumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));	
			WOcolumns.push(new nlobjSearchColumn('custrecord_sku_status'));	


			WOcolumns[1].setSort();
			WOcolumns[2].setSort();
			WOcolumns[3].setSort();
			WOcolumns[4].setSort(true);
			/*** up to here ***/
			var WOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterOpentask,WOcolumns);	
			if (WOSearchResults != null && WOSearchResults.length > 0) {
				nlapiLogExecution('ERROR', 'WOSearchResults.length', WOSearchResults.length);

				if(WOSearchResults.length <= parseInt(vSkipId))
				{
					vSkipId=0;
				}
				var SearchResult = WOSearchResults[parseInt(vSkipId)];
				if(WOSearchResults.length > parseInt(vSkipId) + 1)
				{
					var WOSearchnextResult = WOSearchResults[parseInt(vSkipId) + 1];
					SOarray["custparam_nextlocation"] = WOSearchnextResult.getText('custrecord_actbeginloc');
					SOarray["custparam_nextiteminternalid"] = WOSearchnextResult.getValue('custrecord_ebiz_sku_no');
					SOarray["custparam_nextitem"] = WOSearchnextResult.getText('custrecord_sku');
					SOarray["custparam_nextserialno"] = WOSearchnextResult.getValue('custrecord_batch_no');

				}
				else
				{
					var WOSearchnextResult = WOSearchResults[0];
					SOarray["custparam_nextlocation"] = WOSearchnextResult.getText('custrecord_actbeginloc');
					SOarray["custparam_nextiteminternalid"] = WOSearchnextResult.getValue('custrecord_ebiz_sku_no');
					SOarray["custparam_nextitem"] = WOSearchnextResult.getText('custrecord_sku');
					SOarray["custparam_nextserialno"] = WOSearchnextResult.getValue('custrecord_batch_no');
				}
				SOarray["custparam_woid"] =getWOid; 
				SOarray["custparam_iteminternalid"] = SearchResult.getValue('custrecord_ebiz_sku_no');
				SOarray["custparam_item"] = SearchResult.getText('custrecord_sku');
				SOarray["custparam_recordinternalid"] = SearchResult.getId();
				SOarray["custparam_expectedquantity"] = SearchResult.getValue('custrecord_expe_qty');
				SOarray["custparam_beginLocation"] = SearchResult.getValue('custrecord_actbeginloc');
				SOarray["custparam_itemdescription"] = SearchResult.getValue('custrecord_skudesc');
				SOarray["custparam_dolineid"] = SearchResult.getValue('custrecord_ebiz_cntrl_no');
				SOarray["custparam_invoicerefno"] = SearchResult.getValue('custrecord_invref_no');
				SOarray["custparam_orderlineno"] = SearchResult.getValue('custrecord_line_no');
				SOarray["custparam_beginlocationname"] = SearchResult.getText('custrecord_actbeginloc');
				SOarray["custparam_batchno"] = SearchResult.getValue('custrecord_batch_no');
				//SOarray["custparam_containerlpno"] = SearchResult.getValue('custrecord_container_lp_no');
				SOarray["custparam_containerlpno"] = SearchResult.getValue('custrecord_lpno');// case# 201417077

				SOarray["custparam_whlocation"] = SearchResult.getValue('custrecord_wms_location');
				SOarray["custparam_whcompany"] = SearchResult.getValue('custrecord_comp_id');
				SOarray["custparam_noofrecords"] = SearchResult.length;
				SOarray["custparam_ebizordno"] =  SearchResult.getValue('custrecord_ebiz_order_no');
				SOarray["custparam_itemstatus"] =  SearchResult.getValue('custrecord_sku_status');
			}
			vSkipId= parseFloat(vSkipId) + 1;
			SOarray["custparam_skipid"] = vSkipId;		
//			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
//			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');

			if(getBeginLocation != SOarray["custparam_nextlocation"])
			{  
				SOarray["custparam_beginLocationname"]=null;
				SOarray["custparam_iteminternalid"]=null;
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_location', 'customdeploy_ebiz_rf_wo_picking_loc_di', false, SOarray);								
				nlapiLogExecution('ERROR', 'Navigating To1', 'Picking Location');
			}
			else if(getItemInternalId != SOarray["custparam_nextiteminternalid"])
			{ 
				SOarray["custparam_iteminternalid"] = SOarray["custparam_nextiteminternalid"];
				SOarray["custparam_beginLocationname"]=null;
				//SOarray["custparam_iteminternalid"]=null;
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_item', 'customdeploy_ebiz_rf_wo_picking_item_di', false, SOarray);								
				nlapiLogExecution('ERROR', 'Navigating To1', 'Picking Item');
			}
			else
			{
				SOarray["custparam_iteminternalid"] = SOarray["custparam_nextiteminternalid"];
				SOarray["custparam_beginLocationname"]=null;
				//SOarray["custparam_iteminternalid"]=null;
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_item', 'customdeploy_ebiz_rf_wo_picking_item_di', false, SOarray);								
				nlapiLogExecution('ERROR', 'Navigating To1', 'Picking Item');
			}
		}
		else {
			if (getEnteredItem != '' && getEnteredItem != getItemName) {

				var actItemidArray=validateSKUId(getEnteredItem,whLocation,'');
				nlapiLogExecution('ERROR', 'After validateSKU1',actItemidArray);
				var actItemid = actItemidArray[1];
				nlapiLogExecution('ERROR', 'After validateSKU11',actItemid);
				if(actItemid!=null && actItemid!="")
				{
					nlapiLogExecution('Error', 'actItemid ', actItemid);
					getEnteredItem=actItemid;
				}
				else
				{
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					nlapiLogExecution('DEBUG', 'Error: ', 'Did not scan the location');
				}
			}

			else if(getEnteredItem == getItemName)
			{
				getEnteredItem = getItemInternalId;
			}	

			var str = 'getEnteredItem.' + getEnteredItem + '<br>';
			str = str + 'getItemName.' + getItemName + '<br>';
			str = str + 'getItemInternalId.' + getItemInternalId + '<br>';
			str = str + 'getItem.' + getItem + '<br>';
			str = str + 'getBeginLocationid.' + getBeginLocationid + '<br>';

			nlapiLogExecution('ERROR', 'Item Attributes', str);

			if (getEnteredItem != '' && getEnteredItem == getItemInternalId) {
				var ItemTypeRec = nlapiLookupField('item', getItemInternalId, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);				
				var batchflag = ItemTypeRec.custitem_ebizbatchlot;
				var ItemType = ItemTypeRec.recordType;

				nlapiLogExecution('ERROR', 'AFter Item Lookup', TimeStampinSec());

				var WOFilters = new Array();
				WOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));
				WOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));

				if(getWOid != null && getWOid != "")
				{
					WOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', getWOid));
				}

				if(getItemInternalId != null && getItemInternalId != "")
				{
					WOFilters.push(new nlobjSearchFilter('custrecord_ebiz_sku_no', null, 'is', getItemInternalId));
				}

				if(vZoneId!=null && vZoneId!="" && vZoneId!="null")
				{
					WOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));
				}

				if(getBeginLocationid!=null && getBeginLocationid!='')
					WOFilters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', getBeginLocationid));

				var WOcolumns = new Array();
				WOcolumns.push(new nlobjSearchColumn('custrecord_line_no'));
				WOcolumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
				//WOcolumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
				WOcolumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
				WOcolumns.push(new nlobjSearchColumn('custrecord_sku'));
				WOcolumns.push(new nlobjSearchColumn('custrecord_expe_qty'));				
				WOcolumns.push(new nlobjSearchColumn('custrecord_lpno'));
				WOcolumns.push(new nlobjSearchColumn('custrecord_batch_no'));
				WOcolumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
				WOcolumns.push(new nlobjSearchColumn('custrecord_act_qty'));   
				WOcolumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
				WOcolumns.push(new nlobjSearchColumn('custrecord_invref_no'));
				WOcolumns.push(new nlobjSearchColumn('custrecord_wms_location'));
				WOcolumns.push(new nlobjSearchColumn('custrecord_comp_id'));
				WOcolumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));				
				WOcolumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));	
				WOcolumns.push(new nlobjSearchColumn('custrecord_sku_status'));	

				WOcolumns[1].setSort();
				WOcolumns[2].setSort();
				WOcolumns[3].setSort();
				WOcolumns[4].setSort(true);
				/*** up to here ***/

				nlapiLogExecution('ERROR', 'Before Search Record', TimeStampinSec());

				var WOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, WOFilters,WOcolumns);	

				nlapiLogExecution('ERROR', 'After Search Record', TimeStampinSec());

				if (WOSearchResults != null && WOSearchResults.length > 0) {
					nlapiLogExecution('ERROR', 'WOSearchResults.length', WOSearchResults.length);

					SOarray["custparam_recordinternalid"] = WOSearchResults[0].getId();
					SOarray["custparam_expectedquantity"] = WOSearchResults[0].getValue('custrecord_expe_qty');
					SOarray["custparam_invoicerefno"] = WOSearchResults[0].getValue('custrecord_invref_no');
					SOarray["custparam_batchno"] = WOSearchResults[0].getValue('custrecord_batch_no');
					SOarray["custparam_ebizordno"] =  WOSearchResults[0].getValue('custrecord_ebiz_order_no');
					SOarray["custparam_itemstatus"] =  WOSearchResults[0].getValue('custrecord_sku_status');

					if(WOSearchResults.length >1)
					{
						var WOSearchnextResult = WOSearchResults[1];
						SOarray["custparam_nextlocation"] = WOSearchnextResult.getText('custrecord_actbeginloc');
						SOarray["custparam_nextiteminternalid"] = WOSearchnextResult.getValue('custrecord_ebiz_sku_no');
						SOarray["custparam_nextexpectedquantity"] = WOSearchnextResult.getValue('custrecord_expe_qty');
						SOarray["custparam_nextexpectedquantity"] = WOSearchnextResult.getValue('custrecord_expe_qty');

//						nlapiLogExecution('ERROR', 'Next Location', WOSearchnextResult.getText('custrecord_actbeginloc'));
//						nlapiLogExecution('ERROR', 'Next Item Intr Id', WOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
					}

				}

				nlapiLogExecution('ERROR', 'ItemType', ItemType);
				//If Lotnumbered item the navigate to batch # scan
				if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem" ||batchflag=='T')
				{
					SOarray["custparam_itemType"] ='lotnumberedinventoryitem';
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_batch', 'customdeploy_ebiz_rf_wo_picking_batch_di', false, SOarray);					  
				}
				else {

					nlapiLogExecution('ERROR', 'Before Redirecting', TimeStampinSec());
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_confirm', 'customdeploy_ebiz_rf_wo_pick_confirm_di', false, SOarray);
					nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
				}
			}
			else 
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				nlapiLogExecution('DEBUG', 'Error: ', 'Did not scan the location');
			}
		}
	}
}
