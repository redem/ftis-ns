
/***************************************************************************
eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Reports/Client/ebiz_Inventoryreport_CL.js,v $
 *     	   $Revision: 1.1.4.5.4.1.4.5.2.2 $
 *     	   $Date: 2015/11/25 22:21:39 $
 *     	   $Author: sponnaganti $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_Inventoryreport_CL.js,v $
 * Revision 1.1.4.5.4.1.4.5.2.2  2015/11/25 22:21:39  sponnaganti
 * Case# 201415851
 * LP Prod Issue fix
 *
 * Revision 1.1.4.5.4.1.4.5.2.1  2015/10/28 12:55:30  schepuri
 * case# 201413568
 *
 * Revision 1.1.4.5.4.1.4.5  2015/07/21 13:23:41  schepuri
 * case# 201413568
 *
 * Revision 1.1.4.5.4.1.4.4  2014/08/14 10:05:00  sponnaganti
 * case# 20149951
 * stnd bundle issue fix
 *
 * Revision 1.1.4.5.4.1.4.3  2013/11/25 06:54:29  vmandala
 * Case# 20125912
 * Fixed subnmittin page in Quckship  on changing dropdown values
 *
 * Revision 1.1.4.5.4.1.4.2  2013/05/08 15:09:20  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.1.4.5.4.1.4.1  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.1.4.5.4.1  2012/12/21 09:05:15  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA after go live Fixes.
 *
 * Revision 1.1.4.5  2012/06/25 22:24:37  gkalla
 * CASE201112/CR201113/LOG201121
 * Fine tuned using child-parent relation
 *
 * Revision 1.1.4.4  2012/03/20 20:42:32  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Moved from Trunk to Branch
 *
 * Revision 1.6  2012/03/20 18:05:54  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Wave Creation by sales order header
 *
 * Revision 1.5  2012/03/19 05:37:24  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.4  2012/03/16 09:59:37  schepuri
 * CASE201112/CR201113/LOG201121
 * code merge from 1.1.4.2
 *
 * Revision 1.3  2012/03/15 07:27:20  rmukkera
 * CASE201112/CR201113/LOG201121
 *  condition added for wave cancelation
 *
 * Revision 1.2  2012/03/09 07:25:15  rmukkera
 * CASE201112/CR201113/LOG201121
 * Printreport2 new  meihod added for wavecreation
 *
 * Revision 1.1  2011/11/02 12:34:45  rmukkera
 * CASE201112/CR201113/LOG201121
 *
 * new file
 *  */

function changePageValues(type,name)
{	
	if(name=='custpage_selectpage'||name=='custpage_pagesize')
	{
		var sublist=nlapiGetLineItemCount('custpage_wavecancellist');		
		if(sublist!=null)
		{	        	  
			nlapiSetFieldValue('custpage_hiddenfield','F');  
		}
		var transfersublist  = nlapiGetLineItemCount('custpage_replen_items');	 
		if(transfersublist!=null)
		{
			nlapiSetFieldValue('custpage_hiddenfield','T');  
		}
		var putawaygen_locs  = nlapiGetLineItemCount('custpage_items');	 
		if(putawaygen_locs!=null)
		{
			//case 20125912 start
			nlapiSetFieldValue('custpage_hiddenfield','T');  
			//case end
		}
		NLDoMainFormButtonAction("submitter",true);	
	}
	else
	{
		return true;
	}
}

function Printreport2(ebizwaveno){
	var fullfillment=null;
	var WavePDFURL = nlapiResolveURL('SUITELET', 'customscript_pickreportpdf', 'customdeploy_pickreportpdf');
	/*var ctx = nlapiGetContext();
	if (ctx.getEnvironment() == 'PRODUCTION') {
		WavePDFURL = 'https://system.netsuite.com' + WavePDFURL;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			WavePDFURL = 'https://system.sandbox.netsuite.com' + WavePDFURL;				
		}*/
	//var linkURL = "https://system.netsuite.com/app/site/hosting/scriptlet.nl?script=141&deploy=1";	

	WavePDFURL = WavePDFURL + '&custparam_ebiz_wave_no='+ ebizwaveno+'&custparam_ebiz_fullfilmentno=';
	window.open(WavePDFURL);
}


function backtosearch()
{
	var WaveQBURL = nlapiResolveURL('SUITELET', 'customscript_wavecreationqb', 'customdeploy_wavecreationqb_di');
	/*var ctx = nlapiGetContext();
	if (ctx.getEnvironment() == 'PRODUCTION') {
		WaveQBURL = 'https://system.netsuite.com' + WaveQBURL;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			WaveQBURL = 'https://system.sandbox.netsuite.com' + WaveQBURL;				
		}*/
	//window.open(WavePDFURL);
	window.location.href = WaveQBURL;
}


function backtogeneratesearch()
{
	var queryparams=nlapiGetFieldValue('custpage_qeryparams');

	var querystrings='';

	if(queryparams!=null && queryparams!='')
	{
		var temparray=queryparams.split(',');

		if(temparray[0]!='')
		{
			var param=temparray[0];
			querystrings=querystrings+"&custpage_qbso="+param+"";
		}
		if(temparray[1]!='')
		{
			querystrings=querystrings+"&custpage_company="+temparray[1]+"";
		}
		if(temparray[2]!='')
		{
			querystrings=querystrings+"&custpage_item="+temparray[2]+"";
		}
		if(temparray[3]!='')
		{
			querystrings=querystrings+"&custpage_consignee="+temparray[3]+"";
		}
		if(temparray[4]!='')
		{
			querystrings=querystrings+"&custpage_ordpriority="+temparray[4]+"";
		}
		if(temparray[5]!='')
		{
			querystrings=querystrings+"&custpage_ordtype="+temparray[5]+"";
		}
		if(temparray[6]!='')
		{
			querystrings=querystrings+"&custpage_itemgroup="+temparray[6]+"";
		}
		if(temparray[7]!='')
		{
			querystrings=querystrings+"&custpage_itemfamily="+temparray[7]+"";
		}
		if(temparray[8]!='')
		{
			querystrings=querystrings+"&custpage_packcode="+temparray[8]+"";
		}
		if(temparray[9]!='')
		{
			querystrings=querystrings+"&custpage_uom="+temparray[9]+"";
		}
		if(temparray[10]!='')
		{
			querystrings=querystrings+"&custpage_itemstatus="+temparray[10]+"";
		}
		if(temparray[11]!='')
		{
			querystrings=querystrings+"&custpage_siteminfo1="+temparray[11]+"";
		}
		if(temparray[12]!='')
		{
			querystrings=querystrings+"&custpage_siteminfo2="+temparray[12]+"";
		}
		if(temparray[13]!='')
		{
			querystrings=querystrings+"&custpage_siteminfo3="+temparray[13]+"";
		}
		if(temparray[14]!='')
		{
			querystrings=querystrings+"&custpage_shippingcarrier="+temparray[14]+"";
		}
		if(temparray[15]!='')
		{
			querystrings=querystrings+"&custpage_country="+temparray[15]+"";
		}
		if(temparray[16]!='')
		{
			querystrings=querystrings+"&custpage_state="+temparray[16]+"";
		}
		if(temparray[17]!='')
		{
			querystrings=querystrings+"&custpage_city="+temparray[17]+"";
		}
		if(temparray[18]!='')
		{
			querystrings=querystrings+"&custpage_addr1="+temparray[18]+"";
		}
		if(temparray[19]!='')
		{
			querystrings=querystrings+"&custpage_shipmentno="+temparray[19]+"";
		}
		if(temparray[20]!='')
		{
			querystrings=querystrings+"&custpage_routeno="+temparray[20]+"";
		}
		if(temparray[21]!='')
		{
			querystrings=querystrings+"&custpage_carrier="+temparray[21]+"";
		}
		if(temparray[22]!='')
		{
			querystrings=querystrings+"&custpage_soshipdate="+temparray[22]+"";
		}
		if(temparray[23]!='')
		{
			var param=temparray[23];
			querystrings=querystrings+"&custpage_wmsstatusflag="+param+"";
		}
	}
	var WaveQBURL = nlapiResolveURL('SUITELET', 'customscript_wavecreation', 'customdeploy_wavecreationdi');
	/*var ctx = nlapiGetContext();
	if (ctx.getEnvironment() == 'PRODUCTION') {
		WaveQBURL = 'https://system.netsuite.com' + WaveQBURL+querystrings;		
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			WaveQBURL = 'https://system.sandbox.netsuite.com' + WaveQBURL+querystrings;				
		}*/
	//window.open(WavePDFURL);

	window.location.href = WaveQBURL;
}


function datecomparision()
{
	var fromdate=nlapiGetFieldValue("custpage_fromdate");
	var todate=nlapiGetFieldValue("custpage_todate");
	/*alert("fromdate" + fromdate);
	alert("todate" + todate);*/
	//case# 20149951 starts(from date and to date cannot be greter than current date)
	if(new Date(fromdate)>new Date(todate))
	{
		alert('From date cannot be greater than To date');
		return false;
	}
	if(new Date(fromdate)>new Date(DateStamp()))
	{
		alert('From date cannot be greater than Current date');
		return false;
	}
	if(new Date(todate)>new Date(DateStamp()))
	{
		alert('To date cannot be greater than Current date');
		return false;

	}
	try // case# 201413568
	{
		
		return vcheckAtleast('custpage_replen_items', 'custpage_replen_sel');
	}
	catch(exp)
	{
		alert('Expception'+exp);
	}


	return true;

}

function Display()
{
	//alert('Hi');
	nlapiSetFieldValue('custpage_tempflag','Confirm');
	NLDoMainFormButtonAction("submitter",true);
}

function vcheckAtleast(subListID, checkBoxName){
	var AtleastonelinetoSelected="T";// case# 201413568
	var lineCnt = nlapiGetLineItemCount(subListID);
	var hiddenField = nlapiGetFieldValue("custpage_hiddenfield");
	//alert(hiddenField);
	for (var s = 1; s <= lineCnt; s++) 
	{
		var linecheck= nlapiGetLineItemValue(subListID,checkBoxName,s);


		if (linecheck == "T") {	
			AtleastonelinetoSelected="T";
			break;

		}
		else
		{
			AtleastonelinetoSelected="F";
		}
	}
	if(AtleastonelinetoSelected=="F" && hiddenField =='F')
	{
		alert("Please Select atleast one Line(s)");
		return false;
	}

	return true;
}

