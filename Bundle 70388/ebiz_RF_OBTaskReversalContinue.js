/***************************************************************************
 eBizNET SOLUTIONS LTD
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_OBTaskReversalContinue.js,v $
<<<<<<< ebiz_RF_OBTaskReversalContinue.js
 *     	   $Revision: 1.1.2.2.4.5.4.20 $
 *     	   $Date: 2014/06/13 13:03:39 $
 *     	   $Author: skavuri $
 *     	   $Name: t_NSWMS_2014_1_3_125 $
=======
 *     	   $Revision: 1.1.2.2.4.5.4.20 $
 *     	   $Date: 2014/06/13 13:03:39 $
 *     	   $Author: skavuri $
 *     	   $Name: t_NSWMS_2014_1_3_125 $
>>>>>>> 1.1.2.2.4.5.4.8
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 * 
 * $Log: ebiz_RF_OBTaskReversalContinue.js,v $
 * Revision 1.1.2.2.4.5.4.20  2014/06/13 13:03:39  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.2.4.5.4.19  2014/06/06 07:05:54  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.1.2.2.4.5.4.18  2014/05/30 00:41:02  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.2.4.5.4.17  2014/04/22 15:39:54  nneelam
 * case#  20148079
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.2.4.5.4.16  2014/02/04 06:53:43  nneelam
 * case#  20127030
 * std bundle issue fix
 *
 * Revision 1.1.2.2.4.5.4.15  2014/01/24 14:49:01  rmukkera
 * Case # 20126895
 *
 * Revision 1.1.2.2.4.5.4.14  2014/01/09 14:08:21  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.1.2.2.4.5.4.13  2014/01/06 13:19:17  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.1.2.2.4.5.4.12  2013/12/27 15:39:24  rmukkera
 * Case # 20126573
 *
 * Revision 1.1.2.2.4.5.4.11  2013/12/24 15:12:45  rmukkera
 * Case # 20126520
 *
 * Revision 1.1.2.2.4.5.4.10  2013/12/12 15:35:51  gkalla
 * case#20126306
 * MHP issue in pick reversal
 *
 * Revision 1.1.2.2.4.5.4.9  2013/11/28 06:06:31  skreddy
 * Case# 20125929
 * Afosa SB issue fix
 *
 * Revision 1.1.2.2.4.5.4.8  2013/11/27 14:09:43  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * prodissue fixes
 *
 * Revision 1.1.2.2.4.5.4.7  2013/11/19 07:22:49  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed w.r.t case#20125807
 * i.e.. line# for transfer order differ by 1.
 *
 * Revision 1.1.2.2.4.5.4.6  2013/09/17 15:11:27  rmukkera
 * Case# 20124319�
 *
 * Revision 1.1.2.2.4.5.4.5  2013/09/11 14:24:33  skreddy
 * Case# 20124319
 * standard bundle  issue fix
 *
 * Revision 1.1.2.2.4.5.4.4  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.2.2.4.5.4.3  2013/05/16 14:40:26  schepuri
 * serial no status updating in serial entry - surftech sb
 *
 * Revision 1.1.2.2.4.5.4.2  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.2.4.5.4.1  2013/04/04 15:07:55  rmukkera
 *
 * Issue Fix related to Outbound Pick reversal serial no updation issue GUI(serial no was not updating back to storage)
 *
 * Revision 1.1.2.2.4.5  2012/12/17 23:00:13  kavitha
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal Process Issue fixes
 *
 *
 *****************************************************************************/
function OBTaskReversalContinue(request, response)
{
	
	
	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);

	var getEbizOrdNo = request.getParameter('custparam_uccebizordno');
	var getEbizItemNo = request.getParameter('custparam_uccebizitem');
	var getCartonNo = request.getParameter('custparam_ucccontlpno');
	var getReversalBy = request.getParameter('custparam_uccreversalby');
	var getReversalType = request.getParameter('custparam_uccreversaltype');
	var getNumber=0;

	if(request.getParameter('custparam_recnumber') != null && request.getParameter('custparam_recnumber') != "")
	{
		getNumber=parseFloat(request.getParameter('custparam_recnumber'));
		nlapiLogExecution('DEBUG', 'getNumber', getNumber);
	}
	else
	{
		getNumber=0; 	
	}

	var total=0;

	nlapiLogExecution('DEBUG', 'getEbizOrdNo', getEbizOrdNo);
	nlapiLogExecution('DEBUG', 'getEbizItemNo', getEbizItemNo);
	nlapiLogExecution('DEBUG', 'getCartonNo', getCartonNo);
	nlapiLogExecution('DEBUG', 'getReversalBy', getReversalBy);
	nlapiLogExecution('DEBUG', 'getNumber', getNumber);
	nlapiLogExecution('DEBUG', 'getReversalType', getReversalType);

	var getItemName,getItemIntrId,getPackcode,getActQty,getOrdNo,getOrdIntrId,getOrdLine,getWMSStatus,getShipLpNo,getLocation,getCompany,
	getNSRefNo,getInvRefNo,getFOIntrId,getTaskIntrId,getFoName,getTaskWeight,getTaskCube,getUOMLevel,getEndLocation,getCarton,salesordno;

	var getLanguage = request.getParameter('custparam_language');
	nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);

	var UCCarray=new Array();
	UCCarray["custparam_screenno"] = 'UCCTASK2';
	UCCarray["custparam_language"] = getLanguage;

	var st2,st3,st4,st5,st6,st7st8,st9,st10,st12,st13;
	var st1,st11;
	//case 20125929 start : for spanish conversion
	if( getLanguage == 'es_ES' || getLanguage =='es_AR')
	{
		//case 20125929 end
		st2 = "INVERSI&#211;N DE TRABAJO OB CONTINUAR";
		//st3 = "CAJA DE CART&#211;N #";
		st3 = "CAJA #";
		st4 = "TEMA";
		st5 = "UBICACI&#211;N:";
		st6 = "TAREA CANT :";
		st7 = "ENTER CANT INVERSI&#211;N";
		st8 = "NEXT";
		st9 = "CONTINUAR";
		st10 = "ANTERIOR";
		st12 = "TAREA SELECCIONADA : ";
		st13 = "VENTAS DE PEDIDO # ";
		st1 = "VENTAS DE PEDIDO NO V&#193;LIDO # / CAJA # / TEMA ";
		st11 = "REGISTROS NO M&#193;S";
	}
	else
	{

		st2 = "OB TASK REVERSAL CONTINUE";
		st3 = "CARTON # :";
		st4 = "ITEM :";
		st5 = "LOCATION :";
		st6 = "TASK QTY :";
		st7 = "ENTER REVERSAL QTY";
		st8 = "NEXT";
		st9 = "CONTINUE";
		st10 = "PREV";
		st12 = "SELECTED TASK : ";
		st13 = "SALES ORDER # ";
		st1 = "INVALID SALES ORDER #/CARTON #/ITEM";
		st11 = "NO MORE RECORDS";

	}

	// Fetch the Location for the item entered
	if(getEbizOrdNo != null || getEbizItemNo != null || getCartonNo != null)
	{
		var taskFilters = new Array();
		var taskColumns = new Array();

		if(getReversalType.indexOf('C') != -1 && getCartonNo!=null && getCartonNo!="")
			taskFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getCartonNo));
		if(getReversalType.indexOf('O') != -1 && getEbizOrdNo!=null && getEbizOrdNo!="")
			taskFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', getEbizOrdNo));
		if(getReversalType.indexOf('I') != -1 && getEbizItemNo!=null && getEbizItemNo!="")
			taskFilters.push(new nlobjSearchFilter('custrecord_sku', null, 'is', getEbizItemNo));
		// 3 - PICK
		taskFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); 
		// 7 - BUILD SHIP UNIT 	8 - PICK CONFIRMED	28 - PACK COMPLETE
		taskFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [7,8,28]));

		taskColumns[0] = new nlobjSearchColumn('custrecord_sku');
		taskColumns[1] = new nlobjSearchColumn('custrecord_packcode');
		taskColumns[2] = new nlobjSearchColumn('custrecord_act_qty');
		taskColumns[3] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		taskColumns[4] = new nlobjSearchColumn('custrecord_line_no');
		taskColumns[5] = new nlobjSearchColumn('custrecord_wms_status_flag');
		taskColumns[6] = new nlobjSearchColumn('custrecord_ebiz_ship_lp_no');
		taskColumns[7] = new nlobjSearchColumn('custrecord_wms_location');
		taskColumns[8] = new nlobjSearchColumn('custrecord_comp_id');
		taskColumns[9] = new nlobjSearchColumn('custrecord_ebiz_nsconfirm_ref_no');
		taskColumns[10] = new nlobjSearchColumn('custrecord_invref_no');
		taskColumns[11] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		taskColumns[12] = new nlobjSearchColumn('name');
		taskColumns[13] = new nlobjSearchColumn('custrecord_total_weight');
		taskColumns[14] = new nlobjSearchColumn('custrecord_totalcube');
		taskColumns[15] = new nlobjSearchColumn('custrecord_uom_level');
		taskColumns[16] = new nlobjSearchColumn('custrecord_actendloc');
		taskColumns[17] = new nlobjSearchColumn('custrecord_container_lp_no');

		var taskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, taskFilters, taskColumns);
		if(taskSearchResults != null && taskSearchResults != '')
		{
			nlapiLogExecution('DEBUG', 'Task Search Results Length', taskSearchResults.length);  
			nlapiLogExecution('DEBUG', 'getNumber in get', getNumber);  

			total = taskSearchResults.length;
			nlapiLogExecution('DEBUG', 'total in get', total);  
			if(parseFloat(getNumber)<parseFloat(total))
			{
				var recNo=0;
				if(getNumber!=0)
					recNo=parseFloat(getNumber);

				getItemName = taskSearchResults[recNo].getText('custrecord_sku');
				getItemIntrId = taskSearchResults[recNo].getValue('custrecord_sku');
				getPackcode = taskSearchResults[recNo].getValue('custrecord_packcode');
				getActQty = taskSearchResults[recNo].getValue('custrecord_act_qty');
				getOrdNo = taskSearchResults[recNo].getText('custrecord_ebiz_order_no');
				getOrdIntrId = taskSearchResults[recNo].getValue('custrecord_ebiz_order_no');
				getOrdLine = taskSearchResults[recNo].getValue('custrecord_line_no');
				getWMSStatus = taskSearchResults[recNo].getValue('custrecord_wms_status_flag');
				getShipLpNo = taskSearchResults[recNo].getValue('custrecord_ebiz_ship_lp_no');
				getLocation = taskSearchResults[recNo].getValue('custrecord_wms_location');  
				getCompany =taskSearchResults[recNo].getValue('custrecord_comp_id');
				getNSRefNo = taskSearchResults[recNo].getValue('custrecord_ebiz_nsconfirm_ref_no');
				getInvRefNo = taskSearchResults[recNo].getValue('custrecord_invref_no');
				getFOIntrId = taskSearchResults[recNo].getValue('custrecord_ebiz_cntrl_no');
				getTaskIntrId=taskSearchResults[recNo].getId();
				getFoName=taskSearchResults[recNo].getValue('name');
				getTaskWeight=taskSearchResults[recNo].getValue('custrecord_total_weight');
				getTaskCube=taskSearchResults[recNo].getValue('custrecord_totalcube');
				getUOMLevel=taskSearchResults[recNo].getValue('custrecord_uom_level');
				getEndLocation = taskSearchResults[recNo].getText('custrecord_actendloc');
				getCarton = taskSearchResults[recNo].getValue('custrecord_container_lp_no');

				salesordno='';
				if(getFoName!=null && getFoName!='')
					salesordno = getFoName.split('.')[0];
				else
					salesordno=getOrdNo;

				nlapiLogExecution('DEBUG', 'salesordno',salesordno);

			}
			else
			{				
				UCCarray["custparam_error"] = st11;//'No More Records';				
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, UCCarray);
			}
		}
		else
		{
			//Case # 20126573 Start
			UCCarray["custparam_screenno"] = 'UCCTASK1';
			//Case # 20126573 End
			UCCarray["custparam_error"] = st1;//"Invalid Sales Order #/Carton #/Item";
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, UCCarray);			
		}

	}


	if (request.getMethod() == 'GET') 
	{
		var functionkeyHtml=getFunctionkeyScript('_rf_inventorymove_continue');		
		var html = "<html><head><title>"+ st2 +"</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";      
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		
		//html = html + " document.getElementById('cmdSearch').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head>";
		html = html + "<body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_inventorymove_continue' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st12 +"";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + (parseFloat(getNumber) + 1) + " OF <label>" + parseFloat(total) + "</label>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st13 +" :<label>" + salesordno + "</label>";
		html = html + "				<input type='hidden' name='hdnOrdNo' value=" + getOrdNo + ">";
		html = html + "				<input type='hidden' name='hdnItemName' value=" + getItemName + ">";
		html = html + "				<input type='hidden' name='hdnPackcode' value=" + getPackcode + ">";
		html = html + "				<input type='hidden' name='hdnItemIntrId' value=" + getItemIntrId + ">";
		html = html + "				<input type='hidden' name='hdnActQty' value=" + getActQty + ">";
		html = html + "				<input type='hidden' name='hdnOrdIntrId' value=" + getOrdIntrId + ">";
		html = html + "				<input type='hidden' name='hdnOrdLine' value=" + getOrdLine + ">";
		html = html + "				<input type='hidden' name='hdnWMSStatus' value=" + getWMSStatus + ">";
		html = html + "				<input type='hidden' name='hdnShipLpNo' value=" + getShipLpNo + ">";
		html = html + "				<input type='hidden' name='hdnLocation' value=" + getLocation + ">";
		html = html + "				<input type='hidden' name='hdnCompany' value=" + getCompany + ">";
		html = html + "				<input type='hidden' name='hdnNSRefNo' value=" + getNSRefNo + ">";
		html = html + "				<input type='hidden' name='hdnInvRefNo' value=" + getInvRefNo + ">";
		html = html + "				<input type='hidden' name='hdnFOIntrId' value=" + getFOIntrId + ">";
		html = html + "				<input type='hidden' name='hdnTaskIntrId' value=" + getTaskIntrId + ">";
		html = html + "				<input type='hidden' name='hdnFoName' value=" + getFoName + ">";
		html = html + "				<input type='hidden' name='hdnTaskWeight' value=" + getTaskWeight + ">";
		html = html + "				<input type='hidden' name='hdnTaskCube' value=" + getTaskCube + ">";
		html = html + "				<input type='hidden' name='hdnUOMLevel' value=" + getUOMLevel + ">";
		html = html + "				<input type='hidden' name='hdnendlocation' value=" + getEndLocation + ">";
		html = html + "				<input type='hidden' name='hdncartonno' value=" + getCarton + ">";
		html = html + "				<input type='hidden' name='hdnReversalby' value=" + getReversalBy + ">";
		html = html + "				<input type='hidden' name='hdnReversalType' value=" + getReversalType + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 +"<label>" + getCarton + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st4 +"<label>" + getItemName + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st5 +"<label>" + getEndLocation + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st6 +"<label>" + getActQty + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st7 +"";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterreversalqty' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr><td align = 'left'>";
		if((parseFloat(getNumber) + 1)<parseFloat(total))//case # 20148079
		{
			html = html + "				"+ st8 +" <input name='cmdnext' type='submit' value='F7'/>";
		}
		html = html + "				"+ st9 +" <input name='cmdSearch' id='cmdSearch' type='submit' value='F8' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdnext.disabled=true; return false'/>";
		html = html + "				"+ st10 +"  <input name='cmdPrevious' type='submit' value='F9'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('cmdSearch').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response','Into Response');

		var getLanguage =  request.getParameter('hdngetLanguage');
		var st1,st11;

		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st1 = "VENTAS DE PEDIDO NO V&#193;LIDO # / CAJA # / TEMA ";
			st11 = "REGISTROS NO M&#193;S";


		}
		else
		{
			st1 = "INVALID SALES ORDER #/CARTON #/ITEM";
			st11 = "NO MORE RECORDS";


		}
		var optedEvent = request.getParameter('cmdPrevious');
		var optnext=request.getParameter('cmdnext');
		var reversalqty = request.getParameter('enterreversalqty');

		nlapiLogExecution('DEBUG', 'optedEvent',optedEvent);
		nlapiLogExecution('DEBUG', 'optnext',optnext);

		var UCCarray=new Array();
		UCCarray["custparam_language"] = getLanguage;
		UCCarray["custparam_screenno"] = 'UCCTASK2';
		UCCarray["custparam_uccitem"] = request.getParameter('hdnItemName');
		UCCarray["custparam_uccebizitem"] = request.getParameter('hdnItemIntrId');
		UCCarray["custparam_uccpackcode"] = request.getParameter('hdnPackcode');
		UCCarray["custparam_uccactqty"] = request.getParameter('hdnActQty');
		UCCarray["custparam_uccordno"] = request.getParameter('hdnOrdNo');
		UCCarray["custparam_uccebizordno"] = request.getParameter('hdnOrdIntrId');
		UCCarray["custparam_uccordlineno"] = request.getParameter('hdnOrdLine');
		UCCarray["custparam_uccwmsstatus"] = request.getParameter('hdnWMSStatus');
		UCCarray["custparam_uccshiplpno"] = request.getParameter('hdnShipLpNo');
		UCCarray["custparam_ucclocation"] = request.getParameter('hdnLocation');
		UCCarray["custparam_ucccompany"] = request.getParameter('hdnCompany');
		UCCarray["custparam_uccnsrefno"] = request.getParameter('hdnNSRefNo');
		UCCarray["custparam_uccinvrefno"] = request.getParameter('hdnInvRefNo');
		UCCarray["custparam_uccfointrid"] = request.getParameter('hdnFOIntrId');
		UCCarray["custparam_ucctaskintrid"] = request.getParameter('hdnTaskIntrId');
		UCCarray["custparam_uccname"] = request.getParameter('hdnFoName');
		UCCarray["custparam_ucctaskweight"] = request.getParameter('hdnTaskWeight');
		UCCarray["custparam_ucctaskcube"] = request.getParameter('hdnTaskCube');
		UCCarray["custparam_uccuomlevel"] = request.getParameter('hdnUOMLevel');
		UCCarray["custparam_ucclprecid"] = request.getParameter('hdnlprecid');
		UCCarray["custparam_uccendlocation"] = request.getParameter('hdnendlocation');
		UCCarray["custparam_ucccontlpno"] = request.getParameter('hdncartonno');
		UCCarray["custparam_uccreversalby"] = request.getParameter('hdnReversalby');
		UCCarray["custparam_uccreversaltype"] = request.getParameter('hdnReversalType');

		nlapiLogExecution('DEBUG', 'getNumber',getNumber);
		
		if (sessionobj!=context.getUser()) {
			try
			{
				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}
		if(optnext=='F7')
		{
			getNumber=getNumber+1;
			UCCarray["custparam_recnumber"]= getNumber;
			response.sendRedirect('SUITELET', 'customscript_rf_outboundreversalcontinue', 'customdeploy_rf_outboundreversalcontinue', false, UCCarray);
		}
		else if (optedEvent == 'F9') 
		{
			response.sendRedirect('SUITELET', 'customscript_rf_outboundreversalbytask', 'customdeploy_rf_outboundreversalbytask', false, UCCarray);
		}
		else 
		{			
			//Outbound Reversal
			var nsrefno = request.getParameter('hdnNSRefNo');
			var ordlineno = request.getParameter('hdnOrdLine');
			var reversalqty = request.getParameter('enterreversalqty');
			//Case # 20126520 Start
			if( (reversalqty=='') || (reversalqty==null) || (reversalqty=='null'))
				{
				//reversalqty=taskactqty;
				UCCarray["custparam_error"] = "Pls Enter Reversal Qty";
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, UCCarray);
				nlapiLogExecution('Debug', 'Error: ', 'Reversal Qty Should not be less than zero');
				return false;
				}
			if(isNaN(reversalqty))
				{
				UCCarray["custparam_error"] = "Pls Enter Valid Reversal Qty";
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, UCCarray);
				nlapiLogExecution('Debug', 'Error: ', 'Reversal Qty Should not be less than zero');
				return false;
				}
			//Case # 20126520 end
			var invrefno = request.getParameter('hdnInvRefNo');
			var taskid = request.getParameter('hdnTaskIntrId');
			var foid = request.getParameter('hdnFOIntrId');
			var taskactqty = request.getParameter('hdnActQty');
			var taskweight = request.getParameter('hdnTaskWeight');
			var taskcube = request.getParameter('hdnTaskCube');
			var taskuomlevel = request.getParameter('hdnUOMLevel');
			var itemid = request.getParameter('hdnItemIntrId');
			var cartonno = request.getParameter('hdncartonno');
			var lprecid = null;
			var ordintrid = request.getParameter('hdnOrdIntrId');	
			//Code changed on 18th Nov 2013 by suman.
			//If ordertype is of transfer order system will differ line# by 1.
			var trantype = nlapiLookupField('transaction', ordintrid, 'recordType');
			var ItemlineNO;
			//case # 20126895�start
			/*if(trantype=="transferorder")
				ItemlineNO=parseFloat(ordlineno)+1;
			else*///case # 20126895�end
				ItemlineNO=ordlineno;
			//End of code as of 18th Nov.
			nlapiLogExecution('DEBUG', 'reversalqty before',reversalqty);
			nlapiLogExecution('DEBUG', 'taskactqty before',taskactqty);
			if(isNaN(reversalqty)  || reversalqty == '' || reversalqty == null)
				reversalqty=taskactqty;
			//Case # 20126184� Start
			//case # 20127030�start, condition changed from less than zero to less than 1
			if(parseFloat(reversalqty)<1)
			{
				UCCarray["custparam_error"] = "Reversal Qty Should be atleast one quantity";
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, UCCarray);
				nlapiLogExecution('Debug', 'Error: ', 'Reversal Qty Should not be less than zero');
				return false;
			}
			else if(parseFloat(reversalqty)>parseFloat(taskactqty))
				{
				UCCarray["custparam_error"] = "Reversal Qty Should not be greater than TaskQty";
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, UCCarray);
				nlapiLogExecution('Debug', 'Error: ', 'Reversal Qty Should not be greater than TaskQty');
				return false;
			}
			else
				{
				
				}
			//Case # 20126184� End
			nlapiLogExecution('DEBUG', 'reversalqty',reversalqty);
			nlapiLogExecution('DEBUG', 'taskactqty',taskactqty);

			var vSuccessMsg='F';

			var contlparray = new Array();

			contlparray.push(cartonno);

			updateItemFulfillment(nsrefno,ItemlineNO,reversalqty);
			updateOpentaskandInventory(invrefno,reversalqty,taskid,taskactqty,taskweight,taskcube,taskuomlevel,itemid,lprecid,ordintrid);
			updateFulfillmentOrder(foid,ordlineno,reversalqty);	
			vSuccessMsg='T';
			var itemTypesku = '';
			var serialInflg ='F';
			var serialOutflg ='F';
			ClearOutBoundInventory(cartonno,reversalqty,taskweight,itemid);
			if(itemid!=null && itemid!='' && itemid!='null')
			{
				var fields = ['recordType','custitem_ebizserialin','custitem_ebizserialout'];
				var columns = nlapiLookupField('item', itemid, fields);

				if(columns != null && columns != '')
				{
					itemTypesku = columns.recordType;	
					//	var serialInflg="F";		
					serialInflg = columns.custitem_ebizserialin;
					serialOutflg = columns.custitem_ebizserialout;

					nlapiLogExecution('DEBUG', 'itemTypesku', itemTypesku);
					nlapiLogExecution('DEBUG', 'serialInflg', serialInflg);
					nlapiLogExecution('DEBUG', 'serialOutflg', serialOutflg);
				}

				nlapiLogExecution('DEBUG', 'itemTypesku', itemTypesku);
				if (itemTypesku == "serializedinventoryitem" || itemTypesku == "serializedassemblyitem" || serialInflg == "T" || serialOutflg == "T") 
				{
					updateSerialEntry(itemid,reversalqty,ordlineno,ordintrid,taskid);
				}
			}


			if(vSuccessMsg=='T')
			{
				var vDistinctContainerLp = removeDuplicateElement(contlparray);				
				nlapiLogExecution('DEBUG', 'vDistinctContainerLp ', vDistinctContainerLp);	
				RemovePackTask(vDistinctContainerLp);
			}

			response.sendRedirect('SUITELET', 'customscript_rf_completereversal', 'customdeploy_rf_completereversal', false, UCCarray);
		}
			}
			catch (e)  {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, UCCarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');

			}
		}
		else
		{
			UCCarray["custparam_screenno"] = 'UCCTASK1';
			UCCarray["custparam_error"] = 'TASK ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, UCCarray);
		}
	}
}
function updateSerialEntry(itemid,taskqty,ordlineno,ordid,optaskid)
{
	var contlp='';
	var fromlp='';
	var ordno ='';
	if(optaskid!=null && optaskid!='' && optaskid!='null')
	{
		var opentasrec=nlapiLoadRecord('customrecord_ebiznet_trn_opentask',optaskid);
		contlp=opentasrec.getFieldValue('custrecord_container_lp_no');
		fromlp=opentasrec.getFieldValue('custrecord_from_lp_no');
		ordno =opentasrec.getFieldValue('name');
		
	}
	nlapiLogExecution('DEBUG', 'contlp',contlp);
	nlapiLogExecution('DEBUG', 'fromlp',fromlp);
	nlapiLogExecution('DEBUG', 'ordid',ordid);
	nlapiLogExecution('DEBUG', 'ordlineno',ordlineno);
	nlapiLogExecution('DEBUG', 'itemid',itemid);
	nlapiLogExecution('DEBUG', 'ordno',ordno);

	var temptaskqty=taskqty;
	var filters = new Array();
//Case # 20124319� Start
	if(contlp!='')
		filters.push(new nlobjSearchFilter('custrecord_serialparentid', null, 'is', contlp));
	/*if(fromlp!='')
		filters.push(new nlobjSearchFilter('custrecord_serialparentid', null, 'is', fromlp));*/
//Case # 20124319� end
	filters.push(new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', itemid));
	//filters.push(new nlobjSearchFilter('custrecord_serialebizsono', null, 'is', ordid));
	filters.push(new nlobjSearchFilter('custrecord_serialsono', null, 'is', ordno));
	filters.push(new nlobjSearchFilter('custrecord_serialsolineno', null, 'is', ordlineno));
	filters.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['8']));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	var serialEntryResults = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
	if(serialEntryResults!=null && serialEntryResults!='' && serialEntryResults!='null')
		{
		nlapiLogExecution('DEBUG', 'serialEntryResults',serialEntryResults);
		for(var k1=0;k1<serialEntryResults.length;k1++)
			{
			var  count=parseInt(k1)+1;
			if(parseInt(count)<=parseInt(temptaskqty))
				{
			var SerialEntryRec=nlapiLoadRecord('customrecord_ebiznetserialentry',serialEntryResults[k1].getId());
			   if(SerialEntryRec!=null && SerialEntryRec!='' && SerialEntryRec!='null')
				   {
				   nlapiLogExecution('DEBUG', 'SerialEntryRec',SerialEntryRec);
				   SerialEntryRec.setFieldValue('custrecord_serialwmsstatus',['19']);
				   SerialEntryRec.setFieldValue('custrecord_serialparentid',fromlp);
				   nlapiSubmitRecord(SerialEntryRec);
				   }
				}
			}
		}

}

function ClearOutBoundInventory(contlp,taskqty,taskweight,itemid)
{
	nlapiLogExecution('DEBUG', 'Into ClearOutBoundInventory');

	var str = 'contlp. = ' + contlp + '<br>';
	str = str + 'taskqty. = ' + taskqty + '<br>';	
	str = str + 'taskweight. = ' + taskweight + '<br>';	
	str = str + 'itemid. = ' + itemid + '<br>';	

	nlapiLogExecution('DEBUG', 'ClearOutBoundInventory Parameters', str);

	try
	{
		if(contlp!=null && contlp!='')
		{
			var filters = new Array();
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', contlp));
			//26-Outbound
			filters.push(new nlobjSearchFilter( 'custrecord_wms_inv_status_flag', null, 'anyof', [18]));
			filters.push(new nlobjSearchFilter( 'custrecord_ebiz_inv_sku', null, 'anyof', itemid));

			var columns=new Array();
			columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');

			var inventorysearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv',null,filters,columns);
			if(inventorysearchresults!=null && inventorysearchresults!='' && inventorysearchresults.length>0)
			{
				var remtaskqty=taskqty;
				for(var k=0;k < inventorysearchresults.length; k++)
				{
					var obqoh = inventorysearchresults[k].getValue('custrecord_ebiz_qoh');

					if(obqoh>=taskqty && remtaskqty>0)
					{
						if(isNaN(obqoh))
							obqoh=0;

					var newqty = parseInt(obqoh)-parseInt(taskqty);

					if(isNaN(newqty))
						newqty=0;

					nlapiLogExecution('DEBUG', 'newqty', newqty);

					nlapiLogExecution('DEBUG', 'id', inventorysearchresults[k].getId());

						if(newqty<=0)
						{
							nlapiDeleteRecord('customrecord_ebiznet_createinv', inventorysearchresults[k].getId());
							nlapiLogExecution('DEBUG', 'Outbound Inventory Deleted');
						}
						else
						{
							nlapiSubmitField('customrecord_ebiznet_createinv', inventorysearchresults[k].getId(), 'custrecord_ebiz_qoh', newqty);
						}
						remtaskqty=parseInt(remtaskqty)-parseInt(taskqty);
					}
				}
			}

			//Shipmanifest Update
			var smfilters = new Array();

			smfilters.push(new nlobjSearchFilter('custrecord_ship_ref5', null, 'is', contlp));

			var smcolumns = new Array();
			smcolumns[0] = new nlobjSearchColumn('custrecord_ship_pkgwght');
			smcolumns[0].setSort();

			var shipmanifesttasks = nlapiSearchRecord('customrecord_ship_manifest', null, smfilters, smcolumns);	
			if(shipmanifesttasks!=null && shipmanifesttasks!='' && shipmanifesttasks.length>0)
			{
				for(var l=0;l < shipmanifesttasks.length; l++)
				{
					var actweight = shipmanifesttasks[l].getValue('custrecord_ship_pkgwght');

					if(isNaN(actweight))
						actweight=0;

					var newweight = parseFloat(actweight)-parseFloat(taskweight);

					nlapiLogExecution('DEBUG', 'newweight', newweight);

					if(parseFloat(newweight)>0)
						nlapiSubmitField('customrecord_ship_manifest', shipmanifesttasks[l].getId(), 'custrecord_ship_pkgwght', parseFloat(newweight));
				}				
			}
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in ClearOutBoundInventory', exp);
	}

	nlapiLogExecution('DEBUG', 'Out of of ClearOutBoundInventory');
}

function RemovePackTask(vDistinctContainerLp)
{
	nlapiLogExecution('DEBUG', 'Into RemovePackTask', vDistinctContainerLp);

	try
	{

		for ( var vcount = 0; vDistinctContainerLp!=null && vcount < vDistinctContainerLp.length; vcount++) 
		{
			nlapiLogExecution('DEBUG', 'Container LP', vDistinctContainerLp[vcount]);

			if(vDistinctContainerLp[vcount]!= "" && vDistinctContainerLp[vcount]!= null)
			{
				var filters = new Array();
				filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vDistinctContainerLp[vcount]));

				//26-Picks Failed, 29-Closed, 30-Short Pick
				filters.push(new nlobjSearchFilter( 'custrecord_wms_status_flag', null, 'noneof', [26,29,30]));

				//3 - PICK
				filters.push(new nlobjSearchFilter( 'custrecord_tasktype', null, 'anyof', [3]));

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
				columns[0].setSort();

				var opentasks = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	

				nlapiLogExecution('DEBUG', 'opentasks', opentasks);
				if(opentasks==null || opentasks=='')
				{				
					var packfilters = new Array();

					packfilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vDistinctContainerLp[vcount]));

					//28-Pack Complete
					packfilters.push(new nlobjSearchFilter( 'custrecord_wms_status_flag', null, 'anyof', [28]));

					//14 - PACK
					packfilters.push(new nlobjSearchFilter( 'custrecord_tasktype', null, 'anyof', [14]));

					var packcolumns = new Array();
					packcolumns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
					packcolumns[0].setSort();

					var packtasks = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, packfilters, packcolumns);	
					nlapiLogExecution('DEBUG', 'packtasks', packtasks);
					if(packtasks!=null && packtasks!='' && packtasks.length>0)
					{
						for(var j=0;j < packtasks.length; j++)
						{
							nlapiLogExecution('DEBUG', 'Deleting Pack Task...');
							nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', packtasks[j].getId());
						}
					}

					// Deleting ShipManifest Record
					var smfilters = new Array();

					smfilters.push(new nlobjSearchFilter('custrecord_ship_ref5', null, 'is', vDistinctContainerLp[vcount]));

					var smcolumns = new Array();
					smcolumns[0] = new nlobjSearchColumn('custrecord_ship_ref5');
					smcolumns[0].setSort();

					var shipmanifesttasks = nlapiSearchRecord('customrecord_ship_manifest', null, smfilters, smcolumns);	
					nlapiLogExecution('DEBUG', 'shipmanifesttasks', shipmanifesttasks);
					if(shipmanifesttasks!=null && shipmanifesttasks!='' && shipmanifesttasks.length>0)
					{
						for(var k=0;k < shipmanifesttasks.length; k++)
						{
							nlapiLogExecution('DEBUG', 'Deleting Ship Manifest Record...');
							nlapiDeleteRecord('customrecord_ship_manifest', shipmanifesttasks[k].getId());
						}
					}
				}
			}
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in RemovePackTask', exp);
	}

	nlapiLogExecution('DEBUG', 'Out of RemovePackTask', vDistinctContainerLp);
}
