/***************************************************************************
??????????????????????????????? ? ????????????????????????????? ??eBizNET
???????????????????????                           eBizNET Solutions Inc
****************************************************************************
*
*? $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Masters/Client/ebiz_systemRules_CL.js,v $
*? $Revision: 1.1.2.1 $
*? $Date: 2012/04/16 15:05:00 $
*? $Author: spendyala $
*? $Name: t_NSWMS_2012_1_1_2 $
*
* DESCRIPTION
*? Functionality
*
* REVISION HISTORY
*? $Log: ebiz_systemRules_CL.js,v $
*? Revision 1.1.2.1  2012/04/16 15:05:00  spendyala
*? CASE201112/CR201113/LOG201121
*? Issue related to displaying error message is resolved.
*?
*? Revision 1.1  2011/12/28 07:31:53  spendyala
*?  CASE201112/CR201113/LOG201121
*? system rule client script
*?
*
****************************************************************************/

function onchange(type, name,linenum)
{
	if(name=='custpage_defaultid')
	{
		nlapiRemoveLineItemOption('custpage_rules', 'custpage_rulevalue', null);

		var defaultId = nlapiGetCurrentLineItemText('custpage_rules','custpage_defaultid');
		var filter=new Array();
		filter.push(new nlobjSearchFilter('name',null,'is',defaultId));
		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizdescription');
		column[1]=new nlobjSearchColumn('custrecord_ebizruletype');
		column[2]=new nlobjSearchColumn('name');
		column[3]=new nlobjSearchColumn('custrecord_ebizruleval_refid');
		var searchrecord=nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filter, column);
		if(searchrecord!=null&&searchrecord!="")
		{
			nlapiSetCurrentLineItemValue('custpage_rules','custpage_description',
					searchrecord[0].getValue('custrecord_ebizdescription'),false);
			nlapiSetCurrentLineItemValue('custpage_rules','custpage_ruletype',
					searchrecord[0].getValue('custrecord_ebizruletype').toString(),false);
			nlapiSetCurrentLineItemValue('custpage_rules','custpage_internalid',
					searchrecord[0].getValue('name').toString(), false);
		}
		//search for rule values 

		var filterRuleValues=new Array();
		filterRuleValues[0]=new nlobjSearchFilter('name', null, 'is', searchrecord[0].getText('custrecord_ebizruleval_refid'));
		var columnRuleValues=new Array();
		columnRuleValues[0]=new nlobjSearchColumn('custrecord_ebiznet_sysrule_refvalue').setSort();

		var searchresult=nlapiSearchRecord('customrecord_ebiznet_sysrulevalues', null, filterRuleValues, columnRuleValues);

		nlapiInsertLineItemOption('custpage_rules', 'custpage_rulevalue', '' ,'', false);
		if(searchresult!=null&&searchresult!="")
		{
			for ( var count = 0; count < searchresult.length; count++)
			{
				nlapiInsertLineItemOption('custpage_rules', 'custpage_rulevalue', searchresult[count].getId() , 
						searchresult[count].getValue('custrecord_ebiznet_sysrule_refvalue'), false);
			}
		}

	}
	if(name=='custpage_rulevalue')
	{
		var ruletext=nlapiGetCurrentLineItemText('custpage_rules','custpage_rulevalue');
		nlapiSetCurrentLineItemValue('custpage_rules','custpage_rulevaluetext',ruletext);
	}
}
function onremoveline(type)
{
	var location = nlapiGetCurrentLineItemValue('custpage_rules','custpage_location');
	var company = nlapiGetCurrentLineItemValue('custpage_rules','custpage_company');
	if(location ==""&& company=="")
	{
		alert('User Unauthorised to Edit this record');
		return false;
	}
	return true;
//	var del=nlapiGetFieldValue('custpage_deletedlines');
//	alert(del);
//	if(del==null)
//	nlapiSetFieldValue('deleted_lines', defaultId+',','F', true);
//	else
//	nlapiSetFieldValue('deleted_lines', del+','+defaultId,'F', true);
//	return true;
}