/***************************************************************************
			 eBizNET
    eBizNET SOLUTIONS LTD
 ****************************************************************************
 *
 * $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_ASNCConfirmation_SCH.js,v $
 * $Revision: 1.1.2.3.4.3.4.11.2.3 $
 * $Date: 2015/11/26 09:00:58 $
 * $Author: mpragada $
 * $Name: t_WMS_2015_2_StdBundle_1_187 $
 *
 * DESCRIPTION
 * Functionality
 *
 * REVISION HISTORY
 * $Log: ebiz_ASNCConfirmation_SCH.js,v $
 * Revision 1.1.2.3.4.3.4.11.2.3  2015/11/26 09:00:58  mpragada
 * case# 201415862
 * EDI Configurable screen changes
 *
 * Revision 1.1.2.3.4.3.4.13  2015/11/24 13:49:19  mpragada
 * case# 201414406
 * EDI Configurable screen changes
 *
 * Revision 1.1.2.3.4.3.4.12  2015/11/10 08:53:45  mpragada
 * case# 201415369
 *
 * Revision 1.1.2.3.4.3.4.11  2015/06/09 11:04:41  mpragada
 * Case# 201412758
 * EDI Issue Fixes
 *

 *
 ****************************************************************************/
/**
 * Function will Get all the customer details. This list will be used to get the ASN Required? information.
 * @param skulist
 * @returns {Array}
 */
function GetAllCustomers()
{	
	nlapiLogExecution('DEBUG', 'getAllCustomers', 'Start');	

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custentity_ebiz_asn_required', null, 'is', 'T'));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('entityid');
	columns[1] = new nlobjSearchColumn('custentity_ebiz_asn_required');	
	columns[2] = new nlobjSearchColumn('internalid');	
	columns[3] = new nlobjSearchColumn('internalid');	
	var results = nlapiSearchRecord('customer' , null,	filters, columns);	
	nlapiLogExecution('DEBUG', 'getAllCustomers results', results.length);	
	return results;
}

function GetAllresultsfromASNTiming()
{	
	nlapiLogExecution('DEBUG', 'GetAllresultsfromASNTiming', 'Start'); 	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_asntiming_entity');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_asntiming_dcstore');	

	var results = nlapiSearchRecord('customrecord_ebiznet_asn_timing' , null,	null, columns);	
	if(results !=null && results !='')
		nlapiLogExecution('DEBUG', 'GetAllresultsfromASNTiming', results.length);	
	return results;
}
/**
 * Function Gets all the sales order inernal id  , customer internal id 
 * @param OrderList - All distinct orders departed for the trailer.
 * @returns {Array}
 */

function getallSOLinedetails(orderList)
{
	nlapiLogExecution('DEBUG', 'getallSOLinedetails', orderList);	
	var salesorderDetailList = new Array();
	var filters = new Array();
	var columns = new Array();
	filters.push(new nlobjSearchFilter('internalid', null, 'anyof', orderList));
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));

	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('rate');
	columns[2] = new nlobjSearchColumn('amount');
	columns[3] = new nlobjSearchColumn('quantity');
	columns[4] = new nlobjSearchColumn('line');


	var Res = nlapiSearchRecord('salesorder', null, filters,columns);
	nlapiLogExecution('DEBUG', 'getallSOLinedetails Res', Res);	
	return Res;
}

function GetIndividualSOLinedetails(SOLIst,line,so)
{
	var buyerpart = "";var individiualSOLineT = new Array();
	var individiualSOLineList = new Array();
	try
	{
		if (SOLIst != null && SOLIst.length>0) {
			nlapiLogExecution('DEBUG', 'SOLIst.length', SOLIst.length);
			for ( var i=0; i < SOLIst.length ; i++){

				if (SOLIst[i].getValue('line') == line && SOLIst[i].getId() == so){

					individiualSOLineList = new Array();
					individiualSOLineList[0] = SOLIst[i].getValue('rate');
					individiualSOLineList[1] = SOLIst[i].getValue('amount');
					individiualSOLineList[2] = SOLIst[i].getValue('quantity');	

					i = SOLIst.length;
					individiualSOLineT.push(individiualSOLineList);
				}

			}
		}
	}
	catch(exception) {
		nlapiLogExecution('DEBUG', 'GetIndividiualBatchID', exception);
	}
	return individiualSOLineT;	
}

//function GetSalesorderDetailsforASNTrigger(orderList)
//{
//nlapiLogExecution('DEBUG', 'GetSalesorderDetailsforASNTrigger', 'Start');	
//var salesorderDetailList = new Array();
//var filters = new Array();
//var columns = new Array();
//filters.push(new nlobjSearchFilter('internalid', null, 'anyof', orderList));
//filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

//columns[0] = new nlobjSearchColumn('internalid');
//columns[1] = new nlobjSearchColumn('entity');
////columns[2] = new nlobjSearchColumn('companyid');	
//var Res = nlapiSearchRecord('salesorder', null, filters,columns);
//nlapiLogExecution('DEBUG', 'GetSalesorderDetailsforASNTrigger Res', Res);	
//return Res;
//}
var ResultSalesorderDetailsforASN;
function GetSalesorderDetailsforASNTrigger(orderList,maxno)
{
	if(orderList != null && orderList !='')
	{
		try
		{
			nlapiLogExecution('DEBUG', 'GetSalesorderDetailsforASNTrigger', orderList);	
			var salesorderDetailList = new Array();
			var filters = new Array();
			var columns = new Array();
			filters.push(new nlobjSearchFilter('internalid', null, 'anyof', orderList));
			filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
			if(maxno!=null && maxno!='' && maxno!='-1')
				filters.push(new nlobjSearchFilter('internalidnumber', null, 'greaterthan', maxno));
			else
				ResultSalesorderDetailsforASN = new Array();

			columns[0] = new nlobjSearchColumn('internalid').setSort();
			columns[1] = new nlobjSearchColumn('entity');
			//columns[2] = new nlobjSearchColumn('internalid');
			//columns[2] = new nlobjSearchColumn('internalid').setSort();
			//columns[2] = new nlobjSearchColumn('companyid');	
			var searchresults = nlapiSearchRecord('salesorder', null, filters,columns);

			if( searchresults!=null && searchresults.length>=1000)
			{ 
				for(var s=0;s<searchresults.length;s++)
				{	
					ResultSalesorderDetailsforASN.push(searchresults[s]);
				}
				nlapiLogExecution('DEBUG', 'searchresults[searchresults.length-1]', searchresults[searchresults.length-1]);	
				var maxno=searchresults[searchresults.length-1].getId();
				nlapiLogExecution('DEBUG', 'GetSalesorderDetailsforASNTrigger maxno', maxno);	
				GetSalesorderDetailsforASNTrigger(orderList,maxno);	
			}
			else
			{
				for(var s=0;searchresults!=null && s<searchresults.length;s++)
				{	
					ResultSalesorderDetailsforASN.push(searchresults[s]);
				} 
			}

			if(ResultSalesorderDetailsforASN !=null && ResultSalesorderDetailsforASN !='')
				nlapiLogExecution('DEBUG', 'ResultSalesorderDetailsforASN length', ResultSalesorderDetailsforASN.length);

		}
		catch(exception) {
			nlapiLogExecution('DEBUG', 'GetSalesorderDetailsforASNTrigger exception', exception);
		}
		//nlapiLogExecution('DEBUG', 'GetSalesorderDetailsforASNTrigger Res', Res);	
		return ResultSalesorderDetailsforASN;
	}
}
function GetIndividualabuyerpart(allalternateids,skuid)
{
	var buyerpart = "";
	try
	{
		if (allalternateids != null && allalternateids.length>0) {
			nlapiLogExecution('DEBUG', 'allalternateids.length', allalternateids.length);
			nlapiLogExecution('DEBUG', 'allalternateids testtt', '123');
			for ( var i=0; i < allalternateids.length ; i++){

				//nlapiLogExecution('DEBUG', 'so', so);
				if (allalternateids[i].getId() == skuid){
					nlapiLogExecution('DEBUG', 'allalternateids matched', '123');
					buyerpart = allalternateids[i].getValue('upccode');
					nlapiLogExecution('DEBUG', 'buyerpart matched', buyerpart);
					i = allalternateids.length;
				}

			}
		}
	}
	catch(exception) {
		nlapiLogExecution('DEBUG', 'GetIndividiualBatchID', exception);
	}
	return buyerpart;	
}
function getallbuyerpartno(skulist)
{
	nlapiLogExecution('DEBUG', 'getallbuyerpartno', 'Start');	
	var salesorderDetailList = new Array();
	var filters = new Array();
	var columns = new Array();
	filters.push(new nlobjSearchFilter('internalid', null, 'anyof', skulist));	

	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('upccode');
	//code added by suman as on 29th July
	columns[2] = new nlobjSearchColumn('upccode');

	var Res = nlapiSearchRecord('item', null, filters,columns);
	nlapiLogExecution('DEBUG', 'getallbuyerpartno Res', Res);	
	return Res;
}

function getRealtimeOrders(OrdersEligibleForASNGeneration)
{
	nlapiLogExecution('DEBUG', 'getRealtimeOrders', 'Start');	
	var salesorderDetailList = new Array();
	var filters = new Array();
	var columns = new Array();
	filters.push(new nlobjSearchFilter('internalid', null, 'anyof', OrdersEligibleForASNGeneration));
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('entity');
	columns[2] = new nlobjSearchColumn('entity');
	//columns[3] = new nlobjSearchColumn('custbody_dc_store');


	var Res = nlapiSearchRecord('salesorder', null, filters,columns);
	nlapiLogExecution('DEBUG', 'getRealtimeOrders Res', Res);	
	return Res;
}

function getDistinctOrders(shiplpList){
	nlapiLogExecution('DEBUG', 'getDistinctOrders', 'Start');
	var shiplpIDList = new Array();
	try{

		if(shiplpList != null && shiplpList.length > 0)
		{
			for(var i = 0; i < shiplpList.length; i++)
			{
				var shiplpID = shiplpList[i].getValue('custrecord_ebiz_order_no');
				if(!isDistinctOrder(shiplpIDList, shiplpID))
					shiplpIDList.push(shiplpID);
			}
		}
		nlapiLogExecution('DEBUG', 'getDistinctOrders', 'End');
	}

	catch(exception) 
	{
		nlapiLogExecution('DEBUG', 'getDistinctOrders', exception);
	}
	nlapiLogExecution('DEBUG', 'getDistinctOrders', 'End');
	return shiplpIDList;

}
function isDistinctOrder(shiplpIDList, shiplpID){
	var matchFound = false;
	if(shiplpIDList != null && shiplpIDList.length > 0){
		for(var i = 0; i < shiplpIDList.length; i++){
			if(shiplpIDList[i] == shiplpID)
				matchFound = true;
		}
	}

	return matchFound;
}

//get distict SHIPlp's

function getDistinctshiplpsList(shiplpList){
	nlapiLogExecution('DEBUG', 'getDistinctshiplpsList', 'Start');
	var shiplpIDList = new Array();
	try{

		if(shiplpList != null && shiplpList.length > 0)
		{
			for(var i = 0; i < shiplpList.length; i++)
			{
				var shiplpID = shiplpList[i].getValue('custrecord_ship_lp_no');
				if(!isDistinctShipLP(shiplpIDList, shiplpID))
					shiplpIDList.push(shiplpID);
			}
		}
		nlapiLogExecution('DEBUG', 'getDistinctshiplpsList', 'End');
	}

	catch(exception) 
	{
		nlapiLogExecution('DEBUG', 'getDistinctshiplpsList', exception);
	}
	nlapiLogExecution('DEBUG', 'getDistinctshiplpsList', 'End');
	return shiplpIDList;

}
function getDistinctOrdersList(shiplpList){
	nlapiLogExecution('DEBUG', 'getDistinctOrdersList', shiplpList);
	var shiplpIDList = new Array();	
	try{

		if(shiplpList != null && shiplpList.length > 0)
		{
			for(var i = 0; i < shiplpList.length; i++)
			{
				var shiplpID = shiplpList[i].getValue('custrecord_ebiz_order_no');
				if(!isDistinctShipLP(shiplpIDList, shiplpID))
					shiplpIDList.push(shiplpID);
			}
		}
		nlapiLogExecution('DEBUG', 'getDistinctOrdersList', 'End');
	}

	catch(exception) 
	{
		nlapiLogExecution('DEBUG', 'getDistinctOrdersList', exception);
	}
	nlapiLogExecution('DEBUG', 'getDistinctOrdersList', shiplpIDList);
	return shiplpIDList;

}
function isDistinctShipLP(shiplpIDList, shiplpID){
	var matchFound = false;
	if(shiplpIDList != null && shiplpIDList.length > 0){
		for(var i = 0; i < shiplpIDList.length; i++){
			if(shiplpIDList[i] == shiplpID)
				matchFound = true;
		}
	}

	return matchFound;
}


//function getShippingDetailsForEligibleOrders(orderlist)
//{
//nlapiLogExecution('DEBUG', 'getShippingDetailsForEligibleOrders', 'Start');
//var noofshiplpfilter = new Array();
//var noofshiplpcol = new Array();		

//noofshiplpfilter.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', orderlist));
//noofshiplpfilter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [4])); 	//SHIP Task
//noofshiplpfilter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [14])); // SHIPPED
//noofshiplpfilter.push(new nlobjSearchFilter('custrecord_hostid',  null, 'isempty')); // LOADED

//noofshiplpcol[0] = new nlobjSearchColumn('custrecord_ship_lp_no').setSort(); 
//noofshiplpcol[1] = new nlobjSearchColumn('custrecord_ebiz_trailer_no'); 
//noofshiplpcol[2] = new nlobjSearchColumn('custrecord_wms_status_flag'); 
//noofshiplpcol[3] = new nlobjSearchColumn('custrecord_tasktype'); 
//noofshiplpcol[4] = new nlobjSearchColumn('custrecord_container'); 
//noofshiplpcol[5] = new nlobjSearchColumn('custrecord_comp_id'); 
//noofshiplpcol[6] = new nlobjSearchColumn('custrecord_site_id');
//noofshiplpcol[7] = new nlobjSearchColumn('custrecord_ebiz_order_no');

//shiplpsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, noofshiplpfilter, noofshiplpcol);
//nlapiLogExecution('DEBUG', 'getShippingDetailsForEligibleOrders shiplpsearchresults', shiplpsearchresults);
//return shiplpsearchresults;
//}

function getShippingDetailsForEligibleOrders(orderlist,multishiplp)
{
	nlapiLogExecution('DEBUG', 'getShippingDetailsForEligibleOrders orderlist', orderlist);
	nlapiLogExecution('DEBUG', 'getShippingDetailsForEligibleOrders multishiplp', multishiplp);
	var noofshiplpfilter = new Array();
	var noofshiplpcol = new Array();		

	noofshiplpfilter.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no', null, 'anyof', orderlist));
	noofshiplpfilter.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', [3])); 	//Pick Task
	noofshiplpfilter.push(new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag', null, 'anyof', [14])); // SHIPPED
	noofshiplpfilter.push(new nlobjSearchFilter('custrecord_ebiztask_hostid',  null, 'isempty')); // LOADED
	if(multishiplp != null && multishiplp != '')
		noofshiplpfilter.push(new nlobjSearchFilter('custrecord_ebiztask_ship_lp_no', null, 'is', multishiplp)); // LOADED

	noofshiplpcol[0] = new nlobjSearchColumn('custrecord_ebiztask_ship_lp_no',null,'group').setSort(); 
	noofshiplpcol[1] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_trailer_no',null,'group'); 
	noofshiplpcol[2] = new nlobjSearchColumn('custrecord_ebiztask_wms_status_flag',null,'group'); 
	noofshiplpcol[3] = new nlobjSearchColumn('custrecord_ebiztask_tasktype',null,'group'); 	
	noofshiplpcol[4] = new nlobjSearchColumn('custrecord_ebiztask_comp_id',null,'group'); 
	noofshiplpcol[5] = new nlobjSearchColumn('custrecord_ebiztask_site_id',null,'group');
	noofshiplpcol[6] = new nlobjSearchColumn('internalid','custrecord_ebiztask_ebiz_order_no','group');
	//noofshiplpcol[7] = new nlobjSearchColumn('custrecord_ebiztask_container_size_id',null,'group'); 

	shiplpsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, noofshiplpfilter, noofshiplpcol);
	nlapiLogExecution('DEBUG', 'getShippingDetailsForEligibleOrders shiplpsearchresults', shiplpsearchresults);
	return shiplpsearchresults;
}

function GetIndividiualClosedTaskDetails(searchrecordClosedTask,ebizorderno){

	var individiualClosedTaskDetailsListT = new Array();
	var individiualClosedTaskDetailsList = new Array();
	nlapiLogExecution('DEBUG', 'GetIndividiualClosedTaskDetails', 'Start');
	nlapiLogExecution('DEBUG', 'searchrecordClosedTask', searchrecordClosedTask);
	nlapiLogExecution('DEBUG', 'ebizorderno', ebizorderno);
	try{

		if (searchrecordClosedTask != null && searchrecordClosedTask.length >0 && ebizorderno != null && ebizorderno.length>0){
			for ( var i = 0 ; i < searchrecordClosedTask.length ; i++){
				//nlapiLogExecution('DEBUG', 'searchrecordClosedTask custrecord_ebiztask_ebiz_order_no',searchrecordClosedTask[i].getValue('custrecord_ebiztask_ebiz_order_no'));
				if (searchrecordClosedTask[i].getValue('custrecord_ebiztask_ebiz_order_no')  == ebizorderno )
				{
					//nlapiLogExecution('DEBUG', 'OrderNo matches and order internalid', searchrecordClosedTask[i]['custrecord_ebiztask_ebiz_order_no']);

					individiualClosedTaskDetailsList = new Array();
					individiualClosedTaskDetailsList[0] = searchrecordClosedTask[i].getValue('custrecord_ebiztask_line_no');
					individiualClosedTaskDetailsList[1] = searchrecordClosedTask[i].getValue('custrecord_ebiztask_ebiz_sku_no');
					individiualClosedTaskDetailsList[2] = searchrecordClosedTask[i].getText('custrecord_ebiztask_ebiz_sku_no');					
					individiualClosedTaskDetailsList[3] = searchrecordClosedTask[i].getValue('custrecord_ebiztask_act_qty');
					individiualClosedTaskDetailsList[4] = searchrecordClosedTask[i].getValue('custrecord_ebiztask_ebiz_order_no');
					individiualClosedTaskDetailsList[5] = searchrecordClosedTask[i].getValue('custrecord_ebiztask_uom_level');					
					individiualClosedTaskDetailsList[6] = searchrecordClosedTask[i].getValue('custrecord_ebiztask_ebiz_contlp_no');
					individiualClosedTaskDetailsList[7] = searchrecordClosedTask[i].getText('custrecord_ebiztask_ebiz_contlp_no');
					individiualClosedTaskDetailsList[8] = searchrecordClosedTask[i].getText('custrecord_ebiztask_serial_no');
					individiualClosedTaskDetailsList[9] = searchrecordClosedTask[i].getValue('custrecord_ebiztask_act_end_date');
					individiualClosedTaskDetailsList[10] = searchrecordClosedTask[i].getId();
					individiualClosedTaskDetailsList[11] = searchrecordClosedTask[i].getValue('custrecord_ebiztask_batch_no');
					individiualClosedTaskDetailsList[12] = searchrecordClosedTask[i].getValue('custrecord_ebiztask_packcode');				
					individiualClosedTaskDetailsList[13] = searchrecordClosedTask[i].getValue('tranid','custrecord_ebiztask_ebiz_order_no');
					individiualClosedTaskDetailsList[14] = searchrecordClosedTask[i].getValue('custrecord_ebiztask_container_size_id');
					individiualClosedTaskDetailsListT.push(individiualClosedTaskDetailsList);
				}
			}
		}
	}
	catch(exception){
		nlapiLogExecution('DEBUG', 'individiualClosedTaskDetailsListT', exception);
	}

	nlapiLogExecution('DEBUG', 'GetIndividualOrderInfo', 'end');
	nlapiLogExecution('DEBUG', 'Individual order info-individiualClosedTaskDetailsListT', individiualClosedTaskDetailsListT);
	return individiualClosedTaskDetailsListT;
}
/**
 * Function to retrieve the SKU details for a list of SKUs
 * @param skulist
 * @returns {Array}
 */
function GetAllcustomfieldDetails(closedTaskList,skulist){
	nlapiLogExecution('DEBUG', 'GetAllcustomfieldDetails', 'Start');

	var UPCDetailsListT = new Array();

	var UPCDetailsList = new Array();
	var filters = new Array();
	var columns = new Array();
	try
	{			
		filters.push(new nlobjSearchFilter('custrecord16', null, 'is', 'E'));
		filters.push(new nlobjSearchFilter('custrecord17', null, 'anyof', skulist));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord14');
		columns[1] = new nlobjSearchColumn('custrecord17');		

		var ResupcCodes = nlapiSearchRecord('customrecord265', null, filters, columns);


		if(ResupcCodes != null && ResupcCodes.length > 0){
			for(var i = 0; i < ResupcCodes.length; i++ ){
				UPCDetailsList = new Array();
				UPCDetailsList[0] = ResupcCodes[i].getValue('custrecord17');
				UPCDetailsList[1] = ResupcCodes[i].getValue('custrecord14');				
				UPCDetailsListT.push (UPCDetailsList);
			}
		}
	}
	catch(exception){
		nlapiLogExecution('DEBUG', 'GetAllcustomfieldDetails', exception);
	}		
	return UPCDetailsListT;
}
/**
 * Function to retrieve the SKU details for a list of SKUs
 * @param skulist
 * @returns {Array}
 */
function GetAllUPCDetails(closedTaskList,skulist){
	nlapiLogExecution('DEBUG', 'GetAllUPCDetails', 'Start');

	var UPCDetailsListT = new Array();

	var UPCDetailsList = new Array();
	var filters = new Array();
	var columns = new Array();
	try
	{			
		filters.push(new nlobjSearchFilter('custrecord16', null, 'is', 'U'));
		filters.push(new nlobjSearchFilter('custrecord17', null, 'anyof', skulist));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord14');
		columns[1] = new nlobjSearchColumn('custrecord17');				

		var ResupcCodes = nlapiSearchRecord('customrecord265', null, filters, columns);

		if(ResupcCodes != null && ResupcCodes.length > 0){
			for(var i = 0; i < ResupcCodes.length; i++ ){
				UPCDetailsList = new Array();
				UPCDetailsList[0] = ResupcCodes[i].getValue('custrecord17');
				UPCDetailsList[1] = ResupcCodes[i].getValue('custrecord14');
				UPCDetailsListT.push (UPCDetailsList);
			}
		}
	}
	catch(exception){
		nlapiLogExecution('DEBUG', 'GetAllUPCDetails', exception);
	}			
	return UPCDetailsListT;
}
function getDistinctTrailers(shiplpList){
	nlapiLogExecution('DEBUG', 'getDistinctTrailers', 'Start');
	var trailerList = new Array();
	try{

		if(shiplpList != null && shiplpList.length > 0)
		{
			for(var i = 0; i < shiplpList.length; i++)
			{
				var trailerID = shiplpList[i].getValue('custrecord_ebiz_trailer_no');
				if(!isDistinctTrailer(trailerList, trailerID))
					trailerList.push(trailerID);
			}
		}
		//nlapiLogExecution('DEBUG', 'getDistinctTrailers', 'End');
	}

	catch(exception) 
	{
		nlapiLogExecution('DEBUG', 'getDistinctTrailers', exception);
	}
	//nlapiLogExecution('DEBUG', 'getDistinctTrailers', 'End');
	return trailerList;

}
function isDistinctTrailer(trailerList, trailerID){
	var matchFound = false;
	if(trailerList != null && trailerList.length > 0){
		for(var i = 0; i < trailerList.length; i++){
			if(trailerList[i] == trailerID)
				matchFound = true;
		}
	}

	return matchFound;
}
function TrailerEligibiltyCheck(EligibaleTrailersfor856,trailer)
{

	var matchFound = false;
	nlapiLogExecution('DEBUG', 'TrailerEligibiltyCheck EligibaleTrailersfor856', EligibaleTrailersfor856);
	nlapiLogExecution('DEBUG', 'trailer', trailer);

	if(trailer == '- None -')
		matchFound = true;

	if(EligibaleTrailersfor856 != null && EligibaleTrailersfor856.length > 0){
		for(var i = 0; i < EligibaleTrailersfor856.length; i++){
			if(EligibaleTrailersfor856[i] == trailer)
				matchFound = true;
		}
	}

	return matchFound;
}
/**
 * Landing function for ASN Confirmation - 856 EDI for SPS Commerce
 * 
 * @param type: Edit Mode Flag
 */
function AsncConfirmationUE(type) {
	nlapiLogExecution('DEBUG', 'AsncConfirmationUE', 'Start');
	nlapiLogExecution('DEBUG', 'Event Type=', type);

	var trailername="",departDate="";	var ASNCRuleVal = "",Dummylps="";	var SKUList = new Array();	var SKUDetailsList = new Array();	var newRecIdsArray = new Array();
	var containerLengthTotal = 0;	var containerWidthTotal = 0;	var containerHeightTotal = 0;	var containerWeightTotal = 0;var ShipUnitCharges = 0;
	var ShipunitlevelQty = 0;var shipLP="", trailer="",wmsStatusflag="",tasktype="",ContainerSizeId="",compid="",siteid="",SRRNO='';
	var SalesorderDetails="", MasterBOL="", salesOrderList="", batchDetails="", uomDetails="", skuDetails="", searchrecordClosedTask="", orderList="",shiplpsList='',trailerList='';
	var OrderwithCustomerInfo="",CustomerMasterInfo="",ShippingDetailsforOrder="";var OrdersEligibleForASNGeneration=new Array();  // Added by Mahesh on 072712 to trigger the ASN based on customer master ASN Required?
	var skulist="",prevcompid="",prevsiteid="",UPCDetails="",CustomFieldDetails="";
	var EligibaleTrailersfor856=new Array();var EligibleTrailer='';var EligibaleOrdersfor856=new Array();
	var ebizpro="",ebizseal="",ebizmastershipper="",ebizappointmenttrailer="",ebizexparrivaldate="",scaccodetrlr="";
	var Phone='',Address1='',city='',state='',zip='',boscovsshippper='',Addressee='',QSBOL='';
	try {
		if ( type == 'scheduled' || type =='userinterface') { // Process ASN Confirmation only if in EDIT
			// mode

//			var shippingfromres = nlapiLoadRecord('location', 8);	
//			Addressee=	 shippingfromres.getFieldValue('addressee');
//			Phone= shippingfromres.getFieldValue('addrphone');
//			Address1= shippingfromres.getFieldValue('addr1');
//			city= shippingfromres.getFieldValue('city');
//			state= shippingfromres.getFieldValue('state');
//			zip= shippingfromres.getFieldValue('zip');
//			boscovsshippper= shippingfromres.getFieldValue('custrecord_boscovs_shippper_num');
			//get number of shiplps' from opentask

			var noofshiplpfilter = new Array();
			var noofshiplpcol = new Array();		


			noofshiplpfilter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [4])); 	//SHIP Task
			noofshiplpfilter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [14])); // SHIPPED
			noofshiplpfilter.push(new nlobjSearchFilter('custrecord_hostid',  null, 'isempty')); // LOADED

			noofshiplpcol[0] = new nlobjSearchColumn('custrecord_ship_lp_no'); 
			noofshiplpcol[1] = new nlobjSearchColumn('custrecord_ebiz_trailer_no'); 
			noofshiplpcol[2] = new nlobjSearchColumn('custrecord_wms_status_flag'); 
			noofshiplpcol[3] = new nlobjSearchColumn('custrecord_tasktype'); 
			noofshiplpcol[4] = new nlobjSearchColumn('custrecord_container'); 
			noofshiplpcol[5] = new nlobjSearchColumn('custrecord_comp_id'); 
			noofshiplpcol[6] = new nlobjSearchColumn('custrecord_site_id');
			noofshiplpcol[7] = new nlobjSearchColumn('custrecord_ebiz_order_no');

			shiplpsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, noofshiplpfilter, noofshiplpcol);
			nlapiLogExecution('DEBUG', 'shiplpsearchresults', shiplpsearchresults);
			if(shiplpsearchresults !=null && shiplpsearchresults !="")
			{
				nlapiLogExecution('DEBUG', 'into depart triler process', 'done');
				shiplpsList = getDistinctshiplpsList(shiplpsearchresults);
				trailerList = getDistinctTrailers(shiplpsearchresults);	
				//orderList = getDistinctOrders(shiplpsearchresults);	
				//nlapiLogExecution('DEBUG', 'distinct orderlist count', orderList);				

				//get distict orders from closed task				
				if(shiplpsList != null)
					nlapiLogExecution('DEBUG', 'shiplpsList',shiplpsList);
				for (var y = 0; shiplpsList!= null && y < shiplpsList.length; y++) 
				{
					var shiplpsListfilter = new Array();
					var shiplpsListcol = new Array();
					shiplpsListfilter.push(new nlobjSearchFilter('custrecord_ebiztask_ship_lp_no', null, 'is', shiplpsList[y]));
					shiplpsListfilter.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', [3])); 	//PICK Task	
					shiplpsListfilter.push(new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag', null, 'anyof', [14])); 	//PICK Task	
					shiplpsListfilter.push(new nlobjSearchFilter('custrecord_ebiztask_hostid',  null, 'isempty')); // LOADED
					shiplpsListcol[0] = new nlobjSearchColumn('internalid','custrecord_ebiztask_ebiz_order_no','group').setSort();
					shiplpsListsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, shiplpsListfilter, shiplpsListcol);
					for (var d = 0; shiplpsListsearchresults!= null && d < shiplpsListsearchresults.length; d++){
						EligibaleOrdersfor856.push(shiplpsListsearchresults[d].getValue('internalid','custrecord_ebiztask_ebiz_order_no','group'));
					}
				}
				nlapiLogExecution('DEBUG', 'EligibaleOrdersfor856', EligibaleOrdersfor856);	
				CustomerMasterInfo=GetAllCustomers();
				OrderwithCustomerInfo=GetSalesorderDetailsforASNTrigger(EligibaleOrdersfor856,'-1');					
				//to get req so's with flay req=Y					
				var matchFound = false;

				/* The below for loop is used to filter the order numbers which are eligible for ASN Generation */  
				for ( var m = 0; OrderwithCustomerInfo !=null && m < OrderwithCustomerInfo.length; m++) 
				{
					for ( var n = 0; CustomerMasterInfo !=null && n < CustomerMasterInfo.length; n++) 
					{

						if (CustomerMasterInfo[n].getValue('internalid') == OrderwithCustomerInfo[m].getValue('entity') && CustomerMasterInfo[n].getValue('custentity_ebiz_asn_required') == 'T')
						{
							OrdersEligibleForASNGeneration.push(OrderwithCustomerInfo[m].getValue('internalid'));
							matchFound = true;
							//nlapiLogExecution('DEBUG', 'matchFound', matchFound);
						}
					}
				}			

				nlapiLogExecution('DEBUG', 'OrdersEligibleForASNGeneration', OrdersEligibleForASNGeneration);
				if(OrdersEligibleForASNGeneration !=null && OrdersEligibleForASNGeneration !="")
				{
					searchrecordClosedTask = GeteBizTaskRecs(siteid, compid,OrdersEligibleForASNGeneration,'-1');
					if(searchrecordClosedTask != null && searchrecordClosedTask !=""){
						nlapiLogExecution('DEBUG', 'After closed taskrecs - aval Usage', nlapiGetContext().getRemainingUsage());

						if (searchrecordClosedTask != null	&& searchrecordClosedTask != "") 
						{
							logMsg = logMsg + 'Closed Task Count='	+ searchrecordClosedTask.length + '<br>';
						} else {
							logMsg = logMsg + 'Closed Task Count=' + 'Zero' + '<br>';
						}

						// Get the details for the distinct list of SKUs
						skulist = getDistinctSKUsFromTaskList(searchrecordClosedTask);
						skuDetails = GetSKUDetails(searchrecordClosedTask,skulist);
						//UPCDetails = GetAllUPCDetails(searchrecordClosedTask,skulist);
						//CustomFieldDetails = GetAllcustomfieldDetails(searchrecordClosedTask,skulist);

						nlapiLogExecution('DEBUG', 'After SKU details - aval Usage', nlapiGetContext().getRemainingUsage());

						if (skuDetails != null && skuDetails != "") {
							logMsg = logMsg + 'SKU Details Count=' + skuDetails.length	+ '<br>';
						} else {
							logMsg = logMsg + 'SKU Details Count=' + 'Zero' + '<br>';
						}
						// Get the UOM Details for the distinct SKUS

						uomDetails = GetUOMDetails(searchrecordClosedTask,skulist);
						nlapiLogExecution('DEBUG', 'After UOM details - aval Usage', nlapiGetContext().getRemainingUsage());
						nlapiLogExecution('DEBUG', 'Before uom details length', uomDetails);
						if (uomDetails != null && uomDetails != "") {
							logMsg = logMsg + 'UOM Details Count=' + uomDetails.length+ '<br>';
						} else {
							logMsg = logMsg + 'UOM Details Count=' + 'Zero' + '<br>';
						}
						// Get the Batch Details for the distinct skus

						var batchDetails = GetBatchDetails(searchrecordClosedTask,skulist);
						nlapiLogExecution('DEBUG', 'After Batch details - aval Usage', nlapiGetContext().getRemainingUsage());
						if (batchDetails != null && batchDetails != "") {
							logMsg = logMsg + 'Batch Details Count='+ batchDetails.length + '<br>';
						} else {
							logMsg = logMsg + 'Batch Details Count=' + 'Zero' + '<br>';
						}			

						//  to get distinct SO internal Id's
						salesOrderList = getDistinctOrderIDsFromTaskList(searchrecordClosedTask);				
						//to get BOL#

//						MasterBOL=GetMasterBOL(salesOrderList);
//						nlapiLogExecution('DEBUG', 'MasterBOL 0',MasterBOL[0]);
//						nlapiLogExecution('DEBUG', 'MasterBOL 1',MasterBOL[1]);

						// Get the details for the distinct list of orders.
						SalesorderDetails = GetSalesOrderDetails(searchrecordClosedTask,salesOrderList);
						nlapiLogExecution('DEBUG', 'After SO details - aval Usage', nlapiGetContext().getRemainingUsage());
						if (SalesorderDetails != null && SalesorderDetails != "") {
							logMsg = logMsg + 'Salesorder Details Count='+ SalesorderDetails.length + '<br>';				} 
						else {
							logMsg = logMsg + ' Salesorder Details Count=' + 'Zero'+ '<br>';
						}
						//}
						var multishiplp='';
						var Listfilter = new Array();
						var Listcol = new Array();
						Listfilter.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', OrdersEligibleForASNGeneration));
						Listfilter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [4])); 	//PICK Task			
						Listfilter.push(new nlobjSearchFilter('custrecord_hostid',  null, 'isempty')); // LOADED
						Listfilter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [14])); 
						Listcol[0] = new nlobjSearchColumn('custrecord_ship_lp_no');
						Listsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, Listfilter, Listcol);
						if(Listsearchresults != null && Listsearchresults !=""){
							multishiplp=Listsearchresults[0].getValue('custrecord_ship_lp_no');
						}

						ShippingDetailsforOrder=getShippingDetailsForEligibleOrders(OrdersEligibleForASNGeneration,multishiplp);
						//var buyerpartids=getallbuyerpartnoNLS(OrdersEligibleForASNGeneration);
						var allalternateids=getallbuyerpartno(skulist);
						var allsolineresults=getallSOLinedetails(OrdersEligibleForASNGeneration);	
						if(trailerList != null)
							nlapiLogExecution('DEBUG', 'trailerList length',trailerList.length);
						//case# 201415369  starts
						for (var z = 0; trailerList!= null && z < trailerList.length; z++) 
						{
							var trailerListfilter = new Array();
							var trailerListcol = new Array();
							trailerListfilter.push(new nlobjSearchFilter('custrecord_ebiz_trailer_no', null, 'is', trailerList[z]));
							trailerListfilter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); 	//PICK Task			
							trailerListfilter.push(new nlobjSearchFilter('custrecord_hostid',  null, 'isempty')); // LOADED
							trailerListfilter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [14])); 
							trailerListfilter.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', OrdersEligibleForASNGeneration)); 
							trailerListcol[0] = new nlobjSearchColumn('custrecord_ebiz_trailer_no');
							trailerListsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, trailerListfilter, trailerListcol);
							if(trailerListsearchresults == null || trailerListsearchresults ==""){
								EligibaleTrailersfor856.push(trailerList[z]);
							}
						}
						//case# 201415369  end
						nlapiLogExecution('DEBUG', 'EligibaleTrailersfor856 length',EligibaleTrailersfor856);
						if(ShippingDetailsforOrder != null)
						{
							nlapiLogExecution('DEBUG', 'ShippingDetailsforOrder', ShippingDetailsforOrder.length);
						}
						var parent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); //create new parent record
						for (var q = 0; ShippingDetailsforOrder!= null && q < ShippingDetailsforOrder.length; q++) 
						{						
							trailer = ShippingDetailsforOrder[q].getValue('custrecord_ebiztask_ebiz_trailer_no',null,'group');

							if(TrailerEligibiltyCheck(EligibaleTrailersfor856,trailer)){
								shipLP = ShippingDetailsforOrder[q].getValue('custrecord_ebiztask_ship_lp_no',null,'group');						
								wmsStatusflag = ShippingDetailsforOrder[q].getValue('custrecord_ebiztask_wms_status_flag',null,'group');
								//tasktype = ShippingDetailsforOrder[q].getValue('custrecord_ebiztask_tasktype',null,'group');								
								compid = ShippingDetailsforOrder[q].getValue('custrecord_ebiztask_comp_id',null,'group');
								siteid = ShippingDetailsforOrder[q].getValue('custrecord_ebiztask_site_id',null,'group');		
								ebizorderno = ShippingDetailsforOrder[q].getValue('internalid','custrecord_ebiztask_ebiz_order_no','group');

								MasterBOL=GetMasterBOL(ebizorderno);
								nlapiLogExecution('DEBUG', 'MasterBOL 0',MasterBOL[0]);
								nlapiLogExecution('DEBUG', 'MasterBOL 1',MasterBOL[1]);

								//get container sizeid
								var contsizefilter = new Array();
								var contsizecol = new Array();
								contsizefilter.push(new nlobjSearchFilter('custrecord_ship_lp_no', null, 'is', shipLP));			
								//contsizefilter.push(new nlobjSearchFilter('custrecord_hostid',  null, 'isempty'));
								contsizecol[0] = new nlobjSearchColumn('custrecord_container');
								contsizeresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, contsizefilter, contsizecol);
								if(contsizeresults != null){
									ContainerSizeId = contsizeresults[0].getValue('custrecord_container');
								}

								//get SRR 
//								var srrfilter = new Array();
//								var srrcol = new Array();
//								srrfilter.push(new nlobjSearchFilter('custrecord_ship_order', null, 'anyof', ebizorderno));
//								srrcol[0] = new nlobjSearchColumn('custrecord_ebiz_ship_routing_reqno');
//								srrresults = nlapiSearchRecord('customrecord_ship_manifest', null, srrfilter, srrcol);
//								if(srrresults != null){
//								SRRNO = srrresults[0].getValue('custrecord_ebiz_ship_routing_reqno');
//								}
								nlapiLogExecution('DEBUG', 'Current Available Usage', nlapiGetContext().getRemainingUsage());
								// Get system rule for SPS ASN Confirmation
								if(prevcompid == compid && prevsiteid == siteid)
								{//nothing doing

								}
								else
								{
									ASNCRuleVal = SystemRuleforASNC(siteid, compid);
								}
								prevcompid = compid;
								prevsiteid=siteid;
								// Get all dummy lp's againast to Master Lp					
								nlapiLogExecution('DEBUG', 'After system rule - aval Usage', nlapiGetContext().getRemainingUsage());
								nlapiLogExecution('DEBUG', 'ASNCRuleValue', ASNCRuleVal);
								// Hardcoding for Dyna. It should be removed after testing.

								ASNCRuleVal = 'Y';

								var logMsg = 'shipLP=' + shipLP + '<br>';
								logMsg = logMsg + 'Trailer=' + trailer + '<br>';
								logMsg = logMsg + 'WMS Status Flag=' + wmsStatusflag + '<br>';
								logMsg = logMsg + 'Task Type=' + tasktype + '<br>';
								logMsg = logMsg + 'Container SizeID=' + ContainerSizeId + '<br>';
								logMsg = logMsg + 'Comp id=' + compid + '<br>';
								logMsg = logMsg + 'Site ID=' + siteid + '<br>';
								logMsg = logMsg + '856SystemRule=' + ASNCRuleVal;

								nlapiLogExecution('DEBUG', 'Current Available Values', logMsg);								
								// SHIP
								if (wmsStatusflag == 14 &&  ASNCRuleVal == 'Y') {

									nlapiLogExecution('DEBUG', 'All Record sets Lengths', logMsg);

									if (searchrecordClosedTask != null 	&& searchrecordClosedTask.length > 0) {

										// Varaible Declaration
										var lineno ="", skuid ="", skuidText ="", Actqty = "",SOid = "",uomlevel ="", containerLP ="", containerLPtext ="", SerilalId = "",IssueDate = "",IntId = "";
										var BatchIdText = "",packCode="",BatchId = "",uomIdText = "",upcCode="",customField="",IndividualSKUDetailsList ="",SKUDesc ="",SKUFamily ="",SKUGroup = "",SKUtype = "";
										var HazmatCode ="", TempCntrl ="", vendor ="",IndividualOrderInfoList ="",SalesOrdNo = "",ebizOrderNo="",EDIREF = "",ShipmentVendor ="", Shipmethod = "",ShipmethodID ="", SCACCode = "";
										var PrevShipmethod ="",FOB ="",ShipAddressee ="",ShipAddress1 = "",ShipAddress2 = "",Shipcity = "",ShipState = "",ShipCountry = "",Shipzip = "",ShiptoPhone ="";
										var ExpectedShipdate = "",ActualArrivalDate = "",PlannedArrivalDate = "",Destination = "",Route = "",Class = "",ConsigneeId = "",Billaddressee = "",Billaddr1 = "";
										var Billaddr2 = "",Billaddr3 = "",Billcity = "",BillState = "",Billzip = "",Billcountry = "",Billphone = "",location = "",Company = "",Department = "";
										var varTerms = "",CustomerPO = "",Shipphone='',ShiptoPhone = "",Billphone = "",ShipAddrID = "",individualitemdimsList ="",UOM1 = "",UOMQty1 = "",UOMLength1 = "",UOMWidth1 = "";
										var UOMHeight1 = "",UOMWeight1 = "",UOMCube1 = "",Packcode1 = "",UOMlevel1 = "",UOM2 = "",UOMQty2 = "",UOMLength2 = "",UOMWidth2 = "",UOMHeight2 = "",UOMWeight2 = "";
										var UOMCube2 = "",Packcode2 = "",UOMlevel2 = "",UOM3 = "",UOMQty3 = "",UOMLength3 = "",UOMWidth3 = "",UOMHeight3 = "",UOMWeight3 = "",UOMCube3 = "",Packcode3 = "";
										var UOMlevel3 = "",UOM4 = "",UOMQty4 = "",UOMLength4 = "",UOMWidth4 = "",UOMHeight4 = "",UOMWeight4 = "",UOMCube4 = "",Packcode4 = "",UOMlevel4 = "",UOM5 = "",UOMQty5 = "";
										var UOMLength5 = "",UOMWidth5 = "",UOMHeight5 = "",UOMWeight5 = "",UOMCube5 = "",Packcode5 = "",UOMlevel5 = "",CUCC = "",Conainercube = "",ContainerWeight = "",IndividualCUCCDetailsList = "";
										var EUCC = "",IndividualEUCCDetailsList = "",SUCC = "",ShipunitCube = "",ShipunitWeight = "",IndividualSUCCDetailsList = "",ContainerLength = "",ContainerWidth = "",ContainerHeight = "";
										var ContainerMaxWeight = "",ContainerTareWeight = "",ContainerCube = "",IndividualContainerDimslist ="",TrackingNumbers = "",ShippingCharges = "",IndividualShipmanfDetList ="",Pronum = "";
										var SealNo = "",MasterShipper = "",TrailerAppointNo = "",IndividualTrailerDetails = "",prevskuid="",prevcontainer  ="",prevshipLP ="",prevcontainerLP="", previtemId="", newcontlp="",prevtrailer="",PrevCarrier='',Prevsoshipmethod='';
										var PrevOrdNo = "",prevlineno = "",TaskInternalID="",CarrierOptions="",ShiptoEmail ="",ShiptoFax="", ShiptoEmail="",tranid="";
										var ConsigneeId="",salesordWeight="",salesordCube="",Billcity="",BillState="",Billzip="",Billcountry="",Billphone="",location="",Company="",prevlocation='';
										var Department="",Terms="",OrderType="",OrderPriority="",CustomerPO="",ShipAddrID="",CUCC="",CUCC = "",Conainercube = "",ContainerWeight = "",EUCC="";
										var TrackingNumbers="",ShippingCharges="",SUCC = "",ShipunitCube = "",ShipunitWeight = "",ContainerLength = "",ContainerWidth = "",ContainerHeight = "",ContainerMaxWeight = "",ContainerTareWeight = "",ContainerCube = "";
										var Pronum="",TrailerCarrier='',carrierIdnew='',carrierNamenew='',carrierScacnew='',MasterShipper="",TrailerAppointNo="",SOid="",SOidText='',prevSOid="",soalternateidlist="",alternateid="", ItemID="";					
										var IndividualupcCodeDetailsList="",TrailerDetails='',IndividualcustomFieldDetailsList="",locationvalue='',ConsigneeValue='';
										var sysdate=DateStamp();
										nlapiLogExecution('DEBUG', 'Before for loop - Available Usage', nlapiGetContext().getRemainingUsage());
										var stsupdatecnt =0;
										var Department_gs ="";	var srr_gs = "";
										var route_gs = "";	var store_gs = "";	var MarkforStoreID  ="",SCAC='';

										var IndividiualOrdersearchrecordClosedTask=GetIndividiualClosedTaskDetails(searchrecordClosedTask,ebizorderno);
										nlapiLogExecution('DEBUG', 'IndividiualOrdersearchrecordClosedTask', IndividiualOrdersearchrecordClosedTask);
										if(IndividiualOrdersearchrecordClosedTask == null || IndividiualOrdersearchrecordClosedTask =='')
											updateStatusflagForOrder(ebizorderno);
										for ( var count = 0; count < IndividiualOrdersearchrecordClosedTask.length; count++) {


											lineno=IndividiualOrdersearchrecordClosedTask[count][0];
											skuid=IndividiualOrdersearchrecordClosedTask[count][1];
											skuidText=IndividiualOrdersearchrecordClosedTask[count][2];					
											Actqty=IndividiualOrdersearchrecordClosedTask[count][3] ;
											SOid=IndividiualOrdersearchrecordClosedTask[count][4] ;
											uomlevel=IndividiualOrdersearchrecordClosedTask[count][5] ;					
											containerLP=IndividiualOrdersearchrecordClosedTask[count][6];
											containerLPtext=IndividiualOrdersearchrecordClosedTask[count][7] ;
											SerilalId=IndividiualOrdersearchrecordClosedTask[count][8] ;
											IssueDate=IndividiualOrdersearchrecordClosedTask[count][9] ;
											TaskInternalID=IndividiualOrdersearchrecordClosedTask[count][10];
											BatchIdText=IndividiualOrdersearchrecordClosedTask[count][11] ;
											packCode=IndividiualOrdersearchrecordClosedTask[count][12] ;
											SOidText=IndividiualOrdersearchrecordClosedTask[count][13] ;


											logMsg = 'LineNo=' + lineno + '<br>';
											logMsg = logMsg + 'SKU ID=' + skuid + '<br>';
											logMsg = logMsg + 'skuidText=' + skuidText + '<br>';
											logMsg = logMsg + 'Act Qty=' + Actqty + '<br>';
											logMsg = logMsg + 'SO ID=' + SOid + '<br>';
											logMsg = logMsg + 'SOid Text=' + SOidText + '<br>';
											logMsg = logMsg + 'UOM Level=' + uomlevel + '<br>';
											logMsg = logMsg + 'Container LP=' + containerLP	+ '<br>';
											logMsg = logMsg + 'Batch ID Text=' + BatchIdText;
											nlapiLogExecution('DEBUG', 'Current Record Values',	logMsg);
											logMsg = "";

											// Get Batch ID
											if (BatchIdText != null && BatchIdText != "" &&  BatchIdText !='-') {
												BatchId = GetIndividualBatchID(batchDetails, skuid);
											}
											// Get UPC Code
											if ( skuid == prevskuid )
											{	//No need to call upc API. You can use the existing one.
												nlapiLogExecution('DEBUG', 'skuid == prevskuid cond',	skuid);
											}
											else
											{ //Get the upc code1
												nlapiLogExecution('DEBUG', 'skuid != prevskuid cond',	skuid);
												//upcCode = GetupcCode(skuid);
//												IndividualupcCodeDetailsList = GetIndividualUPCInfo(UPCDetails, skuid);
//												if(IndividualupcCodeDetailsList !="")
//												{
//												upcCode=IndividualupcCodeDetailsList[0][0];
//												}
												// get the details for itemmoreinfo custom field value 1
												//customField = Getcustomfield(skuid);
												//IndividualcustomFieldDetailsList = GetIndividualcustomFieldInfo(CustomFieldDetails, skuid);
												//if(IndividualcustomFieldDetailsList !=""){
												//customField=IndividualcustomFieldDetailsList[0][0];
												//}
												nlapiLogExecution('DEBUG', 'upc code',	upcCode);
												nlapiLogExecution('DEBUG', 'customField',customField);

												// The below API will take skudetails , skuid as
												// parameter and
												// returns all the details of the skuid
												nlapiLogExecution('DEBUG', 'skudetails list sending to ind-skufun', skuDetails + '  ' + skuid 	);

												IndividualSKUDetailsList = GetIndividualSKUInfo(skuDetails, skuid);
												nlapiLogExecution('DEBUG', 'IndividualSKUDetailsList',	IndividualSKUDetailsList.length);
												ItemID= IndividualSKUDetailsList[0][0]; //SKU  	
												SKUDesc = IndividualSKUDetailsList[0][2];  		//SalesDesc
												SKUFamily = IndividualSKUDetailsList[0][3]; 	// itemFamily
												SKUGroup = IndividualSKUDetailsList[0][4];      //itemGroup
												SKUtype = IndividualSKUDetailsList[0][1];       //skutype
												HazmatCode = IndividualSKUDetailsList[0][5];    //HazmatCls];
												TempCntrl = IndividualSKUDetailsList[0][6];     //TempCntrl;
												vendor = IndividualSKUDetailsList[0][7];		// VendorName
												upcCode=IndividualSKUDetailsList[0][9];
												cost=IndividualSKUDetailsList[0][10];

												// Get SKU dimensions for the sku.
												individualitemdimsList = AssignUOMDetails(uomDetails, skuid);

												UOM1 = "";UOMQty1 = "";UOMLength1 = "";UOMWidth1 = "";
												UOMHeight1 = "";UOMWeight1 = "";UOMCube1 = "";Packcode1 = "";UOMlevel1 = "";UOM2 = "";UOMQty2 = "";UOMLength2 = "";	UOMWidth2 = "";	UOMHeight2 = "";UOMWeight2 = "";
												UOMCube2 = "";	Packcode2 = "";	UOMlevel2 = "";	UOM3 = "";
												UOMQty3 = "";UOMLength3 = "";UOMWidth3 = "";UOMHeight3 = "";UOMWeight3 = "";UOMCube3 = "";
												Packcode3 = "";	UOMlevel3 = "";	UOM4 = "";UOMQty4 = "";	UOMLength4 = "";
												UOMWidth4 = "";UOMHeight4 = "";UOMWeight4 = "";UOMCube4 = "";Packcode4 = "";UOMlevel4 = "";UOM5 = "";UOMQty5 = "";UOMLength5 = "";UOMWidth5 = "";UOMHeight5 = "";UOMWeight5 = "";UOMCube5 = "";Packcode5 = "";UOMlevel5 = "";
												nlapiLogExecution('DEBUG', 'SKU Dim details',individualitemdimsList);

												if (individualitemdimsList != null&& individualitemdimsList.length > 0) {
													nlapiLogExecution('DEBUG', 'individualitemdimsList.length',individualitemdimsList.length);
													if (individualitemdimsList.length == 1) {
														UOM1 = individualitemdimsList[0][0];//['UOMID'];
														UOMQty1 = individualitemdimsList[0][1];//['UOMQty'];
														UOMLength1 = individualitemdimsList[0][2];//['UOMLength'];
														UOMWidth1 = individualitemdimsList[0][3];//['UOMWidth'];
														UOMHeight1 = individualitemdimsList[0][4];//['UOMHeight'];
														UOMWeight1 = individualitemdimsList[0][5];//['UOMWeight'];
														UOMCube1 = individualitemdimsList[0][6];//['UOMCube'];
														Packcode1 = individualitemdimsList[0][9];//['UOMlevel'];
														UOMlevel1 = individualitemdimsList[0][8];//['Packcode'];
													} else if (individualitemdimsList.length == 2) {

														nlapiLogExecution('DEBUG', 'individualitemdimsList[0][0]',individualitemdimsList[0][0]);
														UOM1 = individualitemdimsList[0][0];//['UOMID'];
														nlapiLogExecution('DEBUG', 'UOM1',UOM1);
														UOMQty1 = individualitemdimsList[0][1];//['UOMQty'];
														UOMLength1 = individualitemdimsList[0][2];//['UOMLength'];
														UOMWidth1 = individualitemdimsList[0][3];//['UOMWidth'];
														UOMHeight1 = individualitemdimsList[0][4];//['UOMHeight'];
														UOMWeight1 = individualitemdimsList[0][5];//['UOMWeight'];
														UOMCube1 = individualitemdimsList[0][6];//['UOMCube'];
														Packcode1 = individualitemdimsList[0][9];//['UOMlevel'];
														UOMlevel1 = individualitemdimsList[0][8];//['Packcode'];

														UOM2 = individualitemdimsList[1][0];//['UOMID'];
														UOMQty2 = individualitemdimsList[1][1];//['UOMQty'];
														UOMLength2 = individualitemdimsList[1][2];//['UOMLength'];
														UOMWidth2 = individualitemdimsList[1][3];//['UOMWidth'];
														UOMHeight2 = individualitemdimsList[1][4];//['UOMHeight'];
														UOMWeight2 = individualitemdimsList[1][5];//['UOMWeight'];
														UOMCube2 = individualitemdimsList[1][6];//['UOMCube'];
														Packcode2 = individualitemdimsList[1][9];//['UOMlevel'];
														UOMlevel2 = individualitemdimsList[1][8];//['Packcode'];

													} else if (individualitemdimsList.length == 3) {

														UOM1 = individualitemdimsList[0][0];//['UOMID'];
														UOMQty1 = individualitemdimsList[0][1];//['UOMQty'];
														UOMLength1 = individualitemdimsList[0][2];//['UOMLength'];
														UOMWidth1 = individualitemdimsList[0][3];//['UOMWidth'];
														UOMHeight1 = individualitemdimsList[0][4];//['UOMHeight'];
														UOMWeight1 = individualitemdimsList[0][5];//['UOMWeight'];
														UOMCube1 = individualitemdimsList[0][6];//['UOMCube'];
														Packcode1 = individualitemdimsList[0][9];//['UOMlevel'];
														UOMlevel1 = individualitemdimsList[0][8];//['Packcode'];

														UOM2 = individualitemdimsList[1][0];//['UOMID'];
														UOMQty2 = individualitemdimsList[1][1];//['UOMQty'];
														UOMLength2 = individualitemdimsList[1][2];//['UOMLength'];
														UOMWidth2 = individualitemdimsList[1][3];//['UOMWidth'];
														UOMHeight2 = individualitemdimsList[1][4];//['UOMHeight'];
														UOMWeight2 = individualitemdimsList[1][5];//['UOMWeight'];
														UOMCube2 = individualitemdimsList[1][6];//['UOMCube'];
														Packcode2 = individualitemdimsList[1][9];//['UOMlevel'];
														UOMlevel2 = individualitemdimsList[1][8];//['Packcode'];


														UOM3 = individualitemdimsList[2][0];//['UOMID'];
														UOMQty3 = individualitemdimsList[2][1];//['UOMQty'];
														UOMLength3 = individualitemdimsList[2][2];//['UOMLength'];
														UOMWidth3 = individualitemdimsList[2][3];//['UOMWidth'];
														UOMHeight3 = individualitemdimsList[2][4];//['UOMHeight'];
														UOMWeight3 = individualitemdimsList[2][5];//['UOMWeight'];
														UOMCube3 = individualitemdimsList[2][6];//['UOMCube'];
														Packcode3 = individualitemdimsList[2][9];//['UOMlevel'];
														UOMlevel3 = individualitemdimsList[2][8];//['Packcode'];

													} else if (individualitemdimsList.length == 4) {

														UOM1 = individualitemdimsList[0][0];//['UOMID'];
														UOMQty1 = individualitemdimsList[0][1];//['UOMQty'];
														UOMLength1 = individualitemdimsList[0][2];//['UOMLength'];
														UOMWidth1 = individualitemdimsList[0][3];//['UOMWidth'];
														UOMHeight1 = individualitemdimsList[0][4];//['UOMHeight'];
														UOMWeight1 = individualitemdimsList[0][5];//['UOMWeight'];
														UOMCube1 = individualitemdimsList[0][6];//['UOMCube'];
														Packcode1 = individualitemdimsList[0][9];//['UOMlevel'];
														UOMlevel1 = individualitemdimsList[0][8];//['Packcode'];

														UOM2 = individualitemdimsList[0][0];//['UOMID'];
														UOMQty2 = individualitemdimsList[0][1];//['UOMQty'];
														UOMLength2 = individualitemdimsList[0][2];//['UOMLength'];
														UOMWidth2 = individualitemdimsList[0][3];//['UOMWidth'];
														UOMHeight2 = individualitemdimsList[0][4];//['UOMHeight'];
														UOMWeight2 = individualitemdimsList[0][5];//['UOMWeight'];
														UOMCube2 = individualitemdimsList[0][6];//['UOMCube'];
														Packcode2 = individualitemdimsList[0][9];//['UOMlevel'];
														UOMlevel2 = individualitemdimsList[0][8];//['Packcode'];


														UOM3 = individualitemdimsList[0][0];//['UOMID'];
														UOMQty3 = individualitemdimsList[0][1];//['UOMQty'];
														UOMLength3 = individualitemdimsList[0][2];//['UOMLength'];
														UOMWidth3 = individualitemdimsList[0][3];//['UOMWidth'];
														UOMHeight3 = individualitemdimsList[0][4];//['UOMHeight'];
														UOMWeight3 = individualitemdimsList[0][5];//['UOMWeight'];
														UOMCube3 = individualitemdimsList[0][6];//['UOMCube'];
														Packcode3 = individualitemdimsList[0][9];//['UOMlevel'];
														UOMlevel3 = individualitemdimsList[0][8];//['Packcode'];

														UOM4 = individualitemdimsList[0][0];//['UOMID'];
														UOMQty4 = individualitemdimsList[0][1];//['UOMQty'];
														UOMLength4 = individualitemdimsList[0][2];//['UOMLength'];
														UOMWidth4 = individualitemdimsList[0][3];//['UOMWidth'];
														UOMHeight4 = individualitemdimsList[0][4];//['UOMHeight'];
														UOMWeight4 = individualitemdimsList[0][5];//['UOMWeight'];
														UOMCube4 = individualitemdimsList[0][6];//['UOMCube'];
														Packcode4 = individualitemdimsList[0][9];//['UOMlevel'];
														UOMlevel4 = individualitemdimsList[0][8];//['Packcode'];
													} else if (individualitemdimsList.length == 5) {

														UOM1 = individualitemdimsList[0][0];//['UOMID'];
														UOMQty1 = individualitemdimsList[0][1];//['UOMQty'];
														UOMLength1 = individualitemdimsList[0][2];//['UOMLength'];
														UOMWidth1 = individualitemdimsList[0][3];//['UOMWidth'];
														UOMHeight1 = individualitemdimsList[0][4];//['UOMHeight'];
														UOMWeight1 = individualitemdimsList[0][5];//['UOMWeight'];
														UOMCube1 = individualitemdimsList[0][6];//['UOMCube'];
														Packcode1 = individualitemdimsList[0][9];//['UOMlevel'];
														UOMlevel1 = individualitemdimsList[0][8];//['Packcode'];

														UOM2 = individualitemdimsList[0][0];//['UOMID'];
														UOMQty2 = individualitemdimsList[0][1];//['UOMQty'];
														UOMLength2 = individualitemdimsList[0][2];//['UOMLength'];
														UOMWidth2 = individualitemdimsList[0][3];//['UOMWidth'];
														UOMHeight2 = individualitemdimsList[0][4];//['UOMHeight'];
														UOMWeight2 = individualitemdimsList[0][5];//['UOMWeight'];
														UOMCube2 = individualitemdimsList[0][6];//['UOMCube'];
														Packcode2 = individualitemdimsList[0][9];//['UOMlevel'];
														UOMlevel2 = individualitemdimsList[0][8];//['Packcode'];


														UOM3 = individualitemdimsList[0][0];//['UOMID'];
														UOMQty3 = individualitemdimsList[0][1];//['UOMQty'];
														UOMLength3 = individualitemdimsList[0][2];//['UOMLength'];
														UOMWidth3 = individualitemdimsList[0][3];//['UOMWidth'];
														UOMHeight3 = individualitemdimsList[0][4];//['UOMHeight'];
														UOMWeight3 = individualitemdimsList[0][5];//['UOMWeight'];
														UOMCube3 = individualitemdimsList[0][6];//['UOMCube'];
														Packcode3 = individualitemdimsList[0][9];//['UOMlevel'];
														UOMlevel3 = individualitemdimsList[0][8];//['Packcode'];

														UOM4 = individualitemdimsList[0][0];//['UOMID'];
														UOMQty4 = individualitemdimsList[0][1];//['UOMQty'];
														UOMLength4 = individualitemdimsList[0][2];//['UOMLength'];
														UOMWidth4 = individualitemdimsList[0][3];//['UOMWidth'];
														UOMHeight4 = individualitemdimsList[0][4];//['UOMHeight'];
														UOMWeight4 = individualitemdimsList[0][5];//['UOMWeight'];
														UOMCube4 = individualitemdimsList[0][6];//['UOMCube'];
														Packcode4 = individualitemdimsList[0][9];//['UOMlevel'];
														UOMlevel4 = individualitemdimsList[0][8];//['Packcode'];

														UOM5 = individualitemdimsList[0][0];//['UOMID'];
														UOMQty5 = individualitemdimsList[0][1];//['UOMQty'];
														UOMLength5 = individualitemdimsList[0][2];//['UOMLength'];
														UOMWidth5 = individualitemdimsList[0][3];//['UOMWidth'];
														UOMHeight5 = individualitemdimsList[0][4];//['UOMHeight'];
														UOMWeight5 = individualitemdimsList[0][5];//['UOMWeight'];
														UOMCube5 = individualitemdimsList[0][6];//['UOMCube'];
														Packcode5 = individualitemdimsList[0][9];//['UOMlevel'];
														UOMlevel5 = individualitemdimsList[0][8];//['Packcode'];
													}
												}
											}
											prevskuid = skuid;					

											logMsg = 'SKU=' + skuid + '<br>';
											logMsg = logMsg + 'SKU Desc=' + SKUDesc + '<br>';
											logMsg = logMsg + 'SKU Family=' + SKUFamily + '<br>';
											logMsg = logMsg + 'SKU Group=' + SKUGroup + '<br>';
											logMsg = logMsg + 'SKU Type=' + SKUtype + '<br>';
											logMsg = logMsg + 'HazmatCode=' + HazmatCode + '<br>';
											logMsg = logMsg + 'Temp Cntrl=' + TempCntrl + '<br>';
											logMsg = logMsg + 'Vendor=' + vendor + '<br>';
											logMsg = logMsg + 'UPC Code=' + upcCode + '<br>';
											logMsg = logMsg + 'Batch ID=' + BatchId + '<br>';

											nlapiLogExecution('DEBUG', 'Current SKU Record Values',logMsg);
											if(SOid == prevSOid){
												//No need to call upc API. You can use the existing one.
												nlapiLogExecution('DEBUG', 'SOid == prevSOid cond',	SOid);
											}
											else
											{
												IndividualOrderInfoList = GetIndividualOrderInfo(SalesorderDetails, SOid);
												//nlapiLogExecution('DEBUG', 'Order info list-IndividualOrderInfoList',IndividualOrderInfoList);
												if(IndividualOrderInfoList !=null && IndividualOrderInfoList !=''){
													ebizOrderNo=SOid;
													SalesOrdNo = IndividualOrderInfoList[0][0]; //tranID
													EDIREF = IndividualOrderInfoList[0][1];   //EDIRefFld;
													ShipmentVendor = IndividualOrderInfoList[0][2]; // ShipmentVendor;
													Shipmethod = IndividualOrderInfoList[0][3];   // Shipmethod
													ShipmethodID = IndividualOrderInfoList[0][4]; // ShipmethodID'
													SCACCode=Shipmethod;
													operatingUnit= IndividualOrderInfoList[0][45];//['operating unit'];
													nlapiLogExecution('DEBUG', 'Shipment vendor',ShipmentVendor);
													nlapiLogExecution('DEBUG', 'operatingUnit',operatingUnit);

													salesordWeight= IndividualOrderInfoList[0][46];//['operating unit'];
													salesordCube= IndividualOrderInfoList[0][47];//['operating unit'];						
													nlapiLogExecution('DEBUG', 'salesordWeight',salesordWeight);
													nlapiLogExecution('DEBUG', 'salesordCube',salesordCube);

													tranid = IndividualOrderInfoList[0][0];
													FOB = IndividualOrderInfoList[0][39];  // FOB
													ShipAddressee = IndividualOrderInfoList[0][5]; //ShipAddressee
													ShipAddress1 = IndividualOrderInfoList[0][6];// ShipAddr1
													ShipAddress2 = IndividualOrderInfoList[0][7];// [ShipAddr2]
													Shipcity = IndividualOrderInfoList[0][8];// ['ShipCity'];
													ShipState = IndividualOrderInfoList[0][9];//['ShipState'];
													ShipCountry = IndividualOrderInfoList[0][10];//['Shipcountry'];
													Shipzip = IndividualOrderInfoList[0][11]; //['Shipzip'];
													ShiptoPhone = IndividualOrderInfoList[0][42]; //['ShiptoPhone'];
													ExpectedShipdate = IndividualOrderInfoList[0][12];//['ExpectedShipDate'];
													ActualArrivalDate = IndividualOrderInfoList[0][13];//['ActualArrivalDate'];

													PlannedArrivalDate = IndividualOrderInfoList[0][43];//'PlannedArrivaDateTime'];
													nlapiLogExecution('DEBUG', 'Planeed Arrival date and time',PlannedArrivalDate);

													Destination = IndividualOrderInfoList[0][14];//['Destination'];
													Route = IndividualOrderInfoList[0][50];//['Route'];
													nlapiLogExecution('DEBUG', '1',Route);
													Class = IndividualOrderInfoList[0][17];//['class'];
													ConsigneeId = IndividualOrderInfoList[0][18]; //['ConsigneeId'];
													Billaddressee = IndividualOrderInfoList[0][19];//['BillAddressee'];
													Billaddr1 = IndividualOrderInfoList[0][20]; //['Billaddr1'];
													Billaddr2 = IndividualOrderInfoList[0][21];//['Billaddr2'];
													Billaddr3 = IndividualOrderInfoList[0][22];//['Billaddr3'];
													Billcity = IndividualOrderInfoList[0][23];//['BillCity'];
													BillState = IndividualOrderInfoList[0][24];//['BillState'];
													Billzip = IndividualOrderInfoList[0][25];//['Billzip'];
													Billcountry = IndividualOrderInfoList[0][26];//['BillCountry'];
													Billphone = IndividualOrderInfoList[0][27];//['BillPhone'];
													location = IndividualOrderInfoList[0][28];//['Location'];
													Company = IndividualOrderInfoList[0][29]; //['Company'];
													Department = IndividualOrderInfoList[0][30];// ['Department'];
													Terms = IndividualOrderInfoList[0][31]; //['Terms'];
													OrderType = IndividualOrderInfoList[0][32]; //['OrderType'];
													OrderPriority = IndividualOrderInfoList[0][33]; //['OrderPriority'];
													CustomerPO = IndividualOrderInfoList[0][34];//['CustomerPO'];
													Shipphone = IndividualOrderInfoList[0][35];//['ShipPhone'];
													//Billphone = IndividualOrderInfoList[0][37];//['BillPhone'];
													ShipAddrID = IndividualOrderInfoList[0][40];//['ShipAddrID'];

													Department_gs = IndividualOrderInfoList[0][48]; 
													srr_gs = IndividualOrderInfoList[0][49]; 
													route_gs = IndividualOrderInfoList[0][50]; 
													store_gs = IndividualOrderInfoList[0][51]; 
													MarkforStoreID  = IndividualOrderInfoList[0][52]; 
													SCAC  = IndividualOrderInfoList[0][53]; 
													locationvalue= IndividualOrderInfoList[0][54]; 
													ConsigneeValue = IndividualOrderInfoList[0][55]; 
												}	
											}
											prevSOid=SOid;

											if(locationvalue == prevlocation)
											{//No need to get the details, use the existing details
											}
											else
											{
												if( locationvalue != null && locationvalue !='' )
												{
													nlapiLogExecution('DEBUG', 'locationvalue',locationvalue);
													var shippingfromres = nlapiLoadRecord('location', locationvalue);	
													Addressee=	 shippingfromres.getFieldValue('addressee');
													Phone= shippingfromres.getFieldValue('addrphone');
													Address1= shippingfromres.getFieldValue('addr1');
													city= shippingfromres.getFieldValue('city');
													state= shippingfromres.getFieldValue('state');
													zip= shippingfromres.getFieldValue('zip');
													//boscovsshippper= shippingfromres.getFieldValue('custrecord_boscovs_shippper_num');
												}
											}
											prevlocation = locationvalue;
											// Getting CUCC label data.
											if ( containerLP == prevcontainerLP){
												//No need to get the details, use the existing details
											}
											else{
												// Getting CUCC 
												IndividualCUCCDetailsList = GetContainerCUCCDetails(searchrecordClosedTask, containerLP);
												nlapiLogExecution('DEBUG', 'Getting IndividualCUCCDetailsList',IndividualCUCCDetailsList);
												if (IndividualCUCCDetailsList != null && IndividualCUCCDetailsList.length > 0) {
													CUCC = IndividualCUCCDetailsList[0][1]; //['CUCC'];
													Conainercube = IndividualCUCCDetailsList[0][2];//['ContainerCube'];
													ContainerWeight = IndividualCUCCDetailsList[0][3]; //['ContainerWeight'];
												}
												//Getting Shipmanifest Details
												//GetIndividualShipmanifestDetails
												IndividualShipmanfDetList = GetShipmanifestDetails("", containerLP);
												if (IndividualShipmanfDetList != null && IndividualShipmanfDetList.length > 0) {
													TrackingNumbers = IndividualShipmanfDetList[0][1];//['TrackingNumbers'];
													ShippingCharges = IndividualShipmanfDetList[0][2];//['ShippingCharges'];

													if (ShippingCharges == null	 || ShippingCharges == "" || ShippingCharges == "0.0")
														ShippingCharges = 0;
												}

											} // end if -contlp
											prevcontainerLP = containerLP;
											// Getting SUCC Details for shipLP
											if ( shipLP == prevshipLP)
											{	//No need of calling API and use existing values.
											}
											else
											{
												IndividualSUCCDetailsList = GetContainerSUCCDetails(shipLP);

												if (IndividualSUCCDetailsList != null
														&& IndividualSUCCDetailsList.length > 0) {

													SUCC = IndividualSUCCDetailsList[0][1]; //['SUCC'];
													ShipunitCube = IndividualSUCCDetailsList[0][2]; //['Shipunitcube'];
													ShipunitWeight = IndividualSUCCDetailsList[0][3]; //['ShipunitWeight'];
												}
											}
											prevshipLP=shipLP;
											// Getting Container Dims
											nlapiLogExecution('DEBUG', 'ContainerSizeId' , ContainerSizeId);
											nlapiLogExecution('DEBUG', 'prevcontainer' , prevcontainer);
											if(ContainerSizeId == '110' || ContainerSizeId == '7')
											{
												IndividualContainerDimslist=GetSHIPASISContainerDims(uomlevel,skuid)
												if (IndividualContainerDimslist != null && IndividualContainerDimslist.length > 0) {
													ContainerLength = IndividualContainerDimslist[0][0];//['ContainerLength'];
													ContainerWidth = IndividualContainerDimslist[0][1];//['ContainerWidth'];
													ContainerHeight = IndividualContainerDimslist[0][2];//['ContainerHeight'];
													ContainerMaxWeight = IndividualContainerDimslist[0][3];//['ContainerMaxWeight'];
													ContainerCube = IndividualContainerDimslist[0][4];//['Containercube'];

												}
											}
											else if ( ContainerSizeId == prevcontainer)
											{// no need to call API.
											}
											else
											{
												IndividualContainerDimslist=  GetContainerDims(ContainerSizeId);
												nlapiLogExecution('DEBUG', 'Getindividualcondims-IndividualContainerDimslist',IndividualContainerDimslist);
												if (IndividualContainerDimslist != null && IndividualContainerDimslist.length > 0) {
													ContainerLength = IndividualContainerDimslist[0][0];//['ContainerLength'];
													ContainerWidth = IndividualContainerDimslist[0][1];//['ContainerWidth'];
													ContainerHeight = IndividualContainerDimslist[0][2];//['ContainerHeight'];
													ContainerMaxWeight = IndividualContainerDimslist[0][3];//['ContainerMaxWeight'];
													ContainerTareWeight = IndividualContainerDimslist[0][4];//['ContainerTareWeight'];
													ContainerCube = IndividualContainerDimslist[0][5];//['ContainerCube'];
												}
											}
											prevcontainer = ContainerSizeId;
											// Getting Trailer Details
											if(prevtrailer == trailer)
											{// no need to call API.
											}
											else
											{
												TrailerDetails = GetTrailerDetails(trailer);

												if (TrailerDetails != null	&& TrailerDetails.length > 0) {

													Pronum = TrailerDetails[0][0];//['ProNo'];
													SealNo = TrailerDetails[0][1];//['Seal'];
													MasterShipper = TrailerDetails[0][2];//['MasterShipper'];
													TrailerAppointNo =TrailerDetails[0][3];//['TrailerAppointNo'];
													TrailerCarrier =TrailerDetails[0][5];//['TrailerAppointNo'];


													if ( PlannedArrivalDate == null || PlannedArrivalDate == "")
													{
														PlannedArrivalDate = TrailerDetails[0][4]; // If sales order planned arrival date is null get the value from trailer exp arrival date
													}	
												}
											}
											prevtrailer = trailer;
											if(TrailerCarrier !=null &&  TrailerCarrier !='')
											{
												if(PrevCarrier == TrailerCarrier )
												{
													// no need to call API.
												}
												else
												{
													CarServiceDetails = GetCarServiceDetails(TrailerCarrier);

													if (CarServiceDetails != null	&& CarServiceDetails.length > 0) {
														carrierIdnew = CarServiceDetails[0][0];
														carrierNamenew = CarServiceDetails[0][1];
														carrierScacnew = CarServiceDetails[0][2];
													}
												}
												PrevCarrier = TrailerCarrier;
											}
											else
											{
												nlapiLogExecution('DEBUG', 'into ShipmethodID', ShipmethodID);
												if(ShipmethodID !=null &&  ShipmethodID !='')
												{
													if(Prevsoshipmethod == ShipmethodID )
													{
														// no need to call API.
													}
													else
													{
														CarServiceDetails = GetCarServiceDetails(ShipmethodID);

														if (CarServiceDetails != null	&& CarServiceDetails.length > 0) {
															carrierIdnew = CarServiceDetails[0][0];
															carrierNamenew = CarServiceDetails[0][1];
															carrierScacnew = CarServiceDetails[0][2];
														}
													}
													Prevsoshipmethod = ShipmethodID;
												}
											}
											IndividualSOLinedetails=GetIndividualSOLinedetails(allsolineresults,lineno,SOid);
											if (IndividualSOLinedetails != null && IndividualSOLinedetails.length > 0) {											 
												deliverylineprice = IndividualSOLinedetails[0][0]; 
												deliverylinecost= IndividualSOLinedetails[0][1]; 
												deliverylineqty = IndividualSOLinedetails[0][2];											
//												if(newcontlp == containerLP && ItemID == previtemId)
//												{
//												nlapiLogExecution('DEBUG', 'into if', 'done');
//												nlapiSubmitField('customrecord_ebiznet_trn_ebiztask',TaskInternalID, 'custrecord_ebiztask_hostid', 'SPS');
//												}
//												else {
//												var Resdummylpdetails = getdummylpdetails(containerLP,ItemID,'-1');

//												if(  Resdummylpdetails !=null){
//												nlapiLogExecution('DEBUG', 'Resdummylpdetails.length', Resdummylpdetails.length);}
//												if(OrderType == 'Standard' && operatingUnit == 'Finished Goods' ){

//												for ( var i = 0 ; Resdummylpdetails !=null && i < Resdummylpdetails.length; i++ )
//												{
//												UCC = Resdummylpdetails[i].getValue('custrecord_ebiz_lpmaster_sscc');							
//												LP = Resdummylpdetails[i].getValue('custrecord_ebiz_lpmaster_lp');

												//var createRec = nlapiCreateRecord('customrecord_ebiznetinterfaceasnc');

												parent.selectNewLineItem('recmachcustrecord_ebiz_stagetable_parent');	

												//Case Start 201414406 	

												var EDICustRequired = IsEDICustRequired();
												nlapiLogExecution('ERROR', 'EDICustRequired LTL', EDICustRequired);
												if(EDICustRequired == 'Y')
												{
													var ressoDetails = GetEDIconfig(SOid,lineno);
													for ( var w = 0 ; ressoDetails !=null && w < ressoDetails.length; w++ )
													{
														var columns= ressoDetails[w][0].getAllColumns();
														var columnLen = columns.length;
														for (c = 0; c < columnLen; c++)
														{

															var value = ressoDetails[w][0].getValue(columns[c]);
															//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent',ressoDetails[w][1], value);
															if(ressoDetails[w][2] == 'LTL')
															{

																if( (ressoDetails[w][3] != null && ressoDetails[w][3] != '') && (ressoDetails[w][3] == ConsigneeId) )
																{	
																	nlapiLogExecution('ERROR', 'into customer specific', 'done');
																	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent',ressoDetails[w][1], value);
																}
																else
																{
																	nlapiLogExecution('ERROR', 'into not customer specific', ressoDetails[w][1]);
																	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent',ressoDetails[w][1], value);
																	//parent.commitLineItem('recmachcustrecord_ebiz_stagetable_parent');
																}
															}
														}

													}
													//line level
													var ressolineDetails = GetEDIconfigLinelevel(SOid,lineno);
													for ( var w = 0 ; ressolineDetails !=null && w < ressolineDetails.length; w++ )
													{

														var resval = ressolineDetails[w][0];
														if(ressolineDetails[w][2] == 'LTL')
														{
															if( (ressoDetails[w][3] != null && ressoDetails[w][3] != '') && (ressoDetails[w][3] == ConsigneeId) )
															{	
																parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent',ressolineDetails[w][1], resval);
															}
															else
																parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent',ressolineDetails[w][1], resval);
														}
													}

													//defaultvalues
													var resDefaultvalues = GetEDIconfigDefaultvalues(SOid,lineno);
													for ( var w = 0 ; resDefaultvalues !=null && w < resDefaultvalues.length; w++ )
													{
														var resdefaulval = resDefaultvalues[w][0];
														nlapiLogExecution('ERROR', 'resdefaulval', resdefaulval);
														nlapiLogExecution('ERROR', 'resDefaultvalues[w][1]', resDefaultvalues[w][1]);

														parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent',resDefaultvalues[w][1], resdefaulval);
													}

												}

												//Case End 201414406 	

												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_ebizordno', ebizOrderNo);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_delivery_item_custfield0', customField);
												//parent.setCurrentLineItemValue('custrecord_ebiz_asnc_packcode', packCode);

												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_delivery_custfield_000',EDIREF);

												//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_scac_code',SCACCode);
												//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_scac_code',scaccodetrlr);															
												//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_scac_code',SCAC);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_scac_code',carrierScacnew);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shipunitdimsuom', 'Inches');
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_carriermode','LTL');
												//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_carrierservicelevel',Shipmethod);
												//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_carrierservicelevel',TrailerCarrier);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_carrierservicelevel',carrierScacnew);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_fobpaycode',FOB);
												//nlapiLogExecution('DEBUG', ' before consignee Route', Route);
												//nlapiLogExecution('DEBUG', ' ConsigneeId', ConsigneeId);

												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_pickauth_no',Route);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_containerdimsuom', 'Inches');
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_skudimsuom','Inches');
												if (vendor == null || vendor == "") {
													vendor = ItemID;
												}
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_skuvendor',vendor);
												//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_ship_vendor',ShipmentVendor);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_ship_vendor',boscovsshippper);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_delivery_uom','EACH');
												// Added by Sudheer to populate all sku dims cubes
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_cube',UOMCube1);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_cube2',UOMCube2);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_cube3',UOMCube3);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_cube4',UOMCube4);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_cube5',UOMCube5);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_cont_cube',ContainerCube);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sealno', SealNo);
												//parent.setCurrentLineItemValue('custrecord_ebiz_bol', MasterBOL[0]);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_bol', MasterShipper);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_waybill_ref',MasterShipper);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_temp_cntrl',TempCntrl);
												if (ShipAddressee == "" || ShipAddressee == null)
													ShipAddressee = ConsigneeId;
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shipto_contact',ShipAddressee);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_markfor', 'N');
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','name', tranid);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_contwght_lbs', ContainerWeight);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shipunitwght_lbs',	ShipunitWeight);
												//commented by mahesh as per sravan sir request
												//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shipunitwght_lbs',	salesordWeight);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiztrailerno',trailer);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebizdepartment',Department);
												//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebizdepartment',Department_gs);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_class', Class);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebizshipunitid',shipLP);
//												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_carrierid',Shipmethod);
//												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_carrier_name',Shipmethod);
												//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_carrierid',TrailerCarrier);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_carrierid',carrierIdnew);
												//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_carrier_name',TrailerCarrier);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_carrier_name',carrierNamenew);
												if (Route == "" || Route == null)
													Route = Destination;
												// custbody_locationaddressid
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_route', Route);
												//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_route', srr_gs);
												//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_route', SRRNO);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_dest',Destination);
												if(Pronum == null || Pronum == "")
													Pronum=MasterBOL[1];
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_pronum',Pronum);
												//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiploc_id',ShipAddrID);
												//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiploc_id',route_gs);
												//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiploc_id',store_gs);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiploc_id',MarkforStoreID );
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptoadd1',ShipAddress1);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptoadd2',ShipAddress2);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptocity',Shipcity);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptostate',ShipState);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptozip',Shipzip);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptocountry', ShipCountry);
												//parent.setCurrentLineItemValue('custrecord_ebiz_shiptophone',ShiptoPhone);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptophone',Shipphone);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptoemail',ShiptoEmail); // *********
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptofax',ShiptoFax); 							
												if(ActualArrivalDate =="" || ActualArrivalDate == null) // Added by Mahesh
													ActualArrivalDate=sysdate;
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_expected_arrivaldate',ActualArrivalDate);

												if(PlannedArrivalDate =="" || PlannedArrivalDate == null) // Added by Sudheer
													PlannedArrivalDate=sysdate;
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_planned_arrival_datetime',PlannedArrivalDate);
												//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_containerlp',LP);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_containerlp',containerLP);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_containerlp_charges',ShippingCharges);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_container_trackingno',TrackingNumbers);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_issue_period',IssueDate);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_payment_terms', Terms);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_container_length',ContainerLength);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_container_width',ContainerWidth);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_container_height',ContainerHeight);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_container_weight',ContainerMaxWeight);
												//nlapiLogExecution('DEBUG', 'tranid before insert',tranid);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_deliveryid',tranid);
												//nlapiLogExecution('DEBUG', 'Order type is  ', OrderType);		
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_order_type',OrderType);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_order_priority',OrderPriority);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_consigneeid',ConsigneeId);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billinginfo_locationid',Billaddr3);
												if (Billaddressee == "" || Billaddressee == null)
													Billaddressee = ConsigneeId;
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billto_contactname',Billaddressee);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billto_addr1',Billaddr1);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billto_addr2',Billaddr2);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billtocity',Billcity);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billtostate',BillState);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billtozip',Billzip);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billtocountry', Billcountry);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billtophone',Billphone);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billtoemail',ShiptoEmail); // ******************
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billtofax',ShiptoFax);// *******************
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_customer_pono', CustomerPO);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_host_ordno',SOid);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_deliveryline_no', lineno);
												//nlapiLogExecution('DEBUG', 'skuid internal ', skuid);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_item_internalid', skuid);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_asnc_sku', ItemID);
												//nlapiLogExecution('DEBUG', 'skuid internal ', ItemID);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_asnc_sku_desc', SKUDesc);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_asnc_sku_family', SKUFamily);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_asnc_sku_group', SKUGroup);
												if(SKUtype == 'undefined' || SKUtype== "" || SKUtype == null)
													SKUtype="";
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_asnc_sku_type', SKUtype);

												alternateid=GetIndividualabuyerpart(allalternateids,skuid);										

												prevlineno = lineno;
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_buyer_partnumber',	alternateid); 
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_upc_code',upcCode);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_hazmat_code',HazmatCode);
												if (BatchId != null && BatchId != "")
												{
													parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_batch_id',BatchId);
													parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_batch_delivery_qty', Actqty);
												}
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_serial_id',SerilalId);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_quantity',Actqty);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_id', UOM1);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_qty',UOMQty1);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_length',UOMLength1);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_width',UOMWidth1);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_weight',UOMWeight1);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_height',UOMHeight1);

												if ( packCode == null || packCode =="")  // If packcode is null , defaulting to 1.
													packCode="1";
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_asnc_packcode', packCode);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_id2', UOM2);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_qty2',UOMQty2);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_length2',UOMLength2);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_width2',UOMWidth2);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_wght2',UOMWeight2);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_hght2',UOMHeight2);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_id3', UOM3);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_qty3',UOMQty3);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_length3',UOMLength3);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_width3',UOMWidth3);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_weight3',UOMWeight3);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_height3',UOMHeight3);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_id4', UOM4);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_qty4',UOMQty4);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_length4',UOMLength4);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_width4',UOMWidth4);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_weight4',UOMWeight4);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_height4',UOMHeight4);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_id5', UOM5);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_qty5',UOMQty5);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_length5',UOMLength5);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_width5',UOMWidth5);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_weight5',UOMWeight5);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_height5',UOMHeight5);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_wh_location',location);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_asnc_company',Company);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_container_levelqty', Actqty);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_s_ucc128',SUCC);

												if (EUCC != null && EUCC != "")
													EUCC = EUCC.substring(0, EUCC.length - 1);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_c_ucc128',CUCC);								
												//parent.setCurrentLineItemValue('custrecord_ebiz_eucc128', EUCC);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_eucc128', EUCC);

												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiznetshipunitlgth',ContainerLength);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shipunit_width',ContainerWidth);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shipunit_height',ContainerHeight);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shipunit_weight',ShipunitWeight);
												//parent.setCurrentLineItemValue('custrecord_ebiz_ship_cube',ShipunitCube);
												//commented by mahesh as per sravan sir request
												//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_ship_cube',salesordCube);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_ship_cube',ShipunitCube);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipunitcharges',ShipUnitCharges);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipunit_levelqty',Actqty);

												//newly added

												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_itemunitprice',cost);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_itemunitcost',cost);

												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_casepack',UOMQty2);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_deliverylineqty',Actqty);//deliverylineqty
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_deliverylineprice',deliverylineprice);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_deliverylinecost',deliverylinecost);

												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipfromlocationid',location);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipfromcontactname',Addressee);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipfromaddr1',Address1);
//												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipfromaddr2',getallVendornoRes[0][2]);
//												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipfromaddr3',getallVendornoRes[0][3]);															
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipfromcity',city);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipfromstate',state);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipfromzip',zip);
												//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipfromcountry',getallVendornoRes[0][0]);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipfromphone',Phone);
												//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipfromemail',getallVendornoRes[0][0]);
												//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipfromfax',getallVendornoRes[0][0]);									
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_status_flag',"E"); 

												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebizvendor',vendor);

												parent.commitLineItem('recmachcustrecord_ebiz_stagetable_parent');
												//} 
												nlapiSubmitField('customrecord_ebiznet_trn_ebiztask',TaskInternalID, 'custrecord_ebiztask_hostid', 'SPS');


											}
										}
										newcontlp = containerLP;
										previtemId=ItemID;
										//} // End of closed task for loop
										//nlapiLogExecution('DEBUG', 'contsizeresults[0]',contsizeresults);
//										if(IndividiualOrdersearchrecordClosedTask != null && IndividiualOrdersearchrecordClosedTask != '')
//										nlapiSubmitField('customrecord_ebiznet_trn_opentask',contsizeresults[0].getId(), 'custrecord_hostid', 'SPS');
										var shiptaskfilter = new Array();

										shiptaskfilter.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SOid));	
										shiptaskfilter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [14])); 		
										noofshiplpfilter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [4])); 									

										shiptaskresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, shiptaskfilter, null);										
										for (var bb = 0; shiptaskresults!= null && bb < shiptaskresults.length; bb++) 
										{
											nlapiSubmitField('customrecord_ebiznet_trn_opentask',shiptaskresults[bb].getId(), 'custrecord_hostid', 'SPS');
										}
									}
								}
								if(SOid !=null && SOid != '')
								{
									parent.selectNewLineItem('recmachcustrecord_ebiz_asn_parent');
									parent.setCurrentLineItemValue('recmachcustrecord_ebiz_asn_parent','custrecord_parent_orderno', SOid);
									parent.setCurrentLineItemValue('recmachcustrecord_ebiz_asn_parent','custrecord_parent_deliveryid', SOidText);
									parent.setCurrentLineItemValue('recmachcustrecord_ebiz_asn_parent','custrecord_parent_customer', ConsigneeId);
									parent.commitLineItem('recmachcustrecord_ebiz_asn_parent');
								}
							}
							else
							{
								nlapiLogExecution('DEBUG', 'Records','Some more Pick tasks available for this trailer in open task');
							}
						}
						nlapiLogExecution('DEBUG', 'Before inserting into stage table - Current Available Usage', nlapiGetContext().getRemainingUsage());
						nlapiLogExecution('DEBUG', 'Before inserting into stage table','done');
						nlapiSubmitRecord(parent); 
						nlapiLogExecution('DEBUG', 'After updating ebiz status flag = E  and Out of Forloop - Current Available Usage', nlapiGetContext().getRemainingUsage());
					}
				}

			}

		}
		else {
			nlapiLogExecution('DEBUG', 'Records','New Rec doesnot exist to Create SPS');
		}
	} 
	catch (exp) {
		nlapiLogExecution('DEBUG', 'Exeception', exp);
	}

	nlapiLogExecution('DEBUG', 'AsncConfirmationUE', 'End');
}
function GetIndividualcustomFieldInfo(skulist, sku ){

	var IndividualSKUInfoList = new Array();

	var IndividualSKUInfoListT = new Array();
	nlapiLogExecution('DEBUG', 'GetIndividualcustomFieldInfo', 'Start');
	nlapiLogExecution('DEBUG', 'skulist', skulist);
	nlapiLogExecution('DEBUG', 'sku', sku);
	try{

		if (skulist != null && skulist.length >0 && sku != null && sku.length>0){
			for ( var i = 0 ; i < skulist.length ; i++){

				if (skulist[i][0]  == sku )
				{
					IndividualSKUInfoList = new Array();
					//nlapiLogExecution('DEBUG', 'sku matches..internal id is', skulist[i][0]);
					IndividualSKUInfoList[0] = skulist[i][1]; 					
					IndividualSKUInfoListT.push(IndividualSKUInfoList);
					i = skulist.length; 	
				}
			}
		}
	}
	catch(exception){
		nlapiLogExecution('DEBUG', 'GetIndividualcustomFieldInfo', exception);

	}

	nlapiLogExecution('DEBUG', 'GetIndividualcustomFieldInfo', 'end');
	nlapiLogExecution('DEBUG', 'IndividualSKUInfoListT', IndividualSKUInfoListT);
	return IndividualSKUInfoListT;
}
function GetIndividualUPCInfo(skulist, sku ){

	var IndividualSKUInfoList = new Array();

	var IndividualSKUInfoListT = new Array();
	nlapiLogExecution('DEBUG', 'GetIndividualUPCInfo', 'Start');
	nlapiLogExecution('DEBUG', 'skulist', skulist);
	nlapiLogExecution('DEBUG', 'sku', sku);
	try{

		if (skulist != null && skulist.length >0 && sku != null && sku.length>0){
			for ( var i = 0 ; i < skulist.length ; i++){

				if (skulist[i][0]  == sku )
				{
					IndividualSKUInfoList = new Array();
					//nlapiLogExecution('DEBUG', 'sku matches..internal id is', skulist[i][0]);
					IndividualSKUInfoList[0] = skulist[i][1]; 					
					IndividualSKUInfoListT.push(IndividualSKUInfoList);
					i = skulist.length; 	
				}
			}
		}
	}
	catch(exception){
		nlapiLogExecution('DEBUG', 'GetIndividualUPCInfo', exception);

	}

	nlapiLogExecution('DEBUG', 'GetIndividualUPCInfo', 'end');
	nlapiLogExecution('DEBUG', 'IndividualSKUInfoListT', IndividualSKUInfoListT);
	return IndividualSKUInfoListT;
}
function GetAllRecentRecords(OrdersEligibleForASNGeneration,trailername){
	nlapiLogExecution('DEBUG', 'into GetAllRecentRecords', OrdersEligibleForASNGeneration);
	nlapiLogExecution('DEBUG', 'into GetAllRecentRecords trailername', trailername);
	var filters = new Array();
	var columns = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_ebizordno', null, 'anyof', OrdersEligibleForASNGeneration));
	filters.push(new nlobjSearchFilter('custrecord_ebiztrailerno', null, 'is', trailername));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_status_flag', null, 'is', 'I'));

	columns[0] = new nlobjSearchColumn('custrecord_ebiz_ebizordno');
	columns[1] = new nlobjSearchColumn('custrecord_ebiztrailerno');
	columns[2] = new nlobjSearchColumn('internalid');
	columns[3] = new nlobjSearchColumn('name');

	var ResDetails = nlapiSearchRecord('customrecord_ebiznetinterfaceasnc', null, filters,columns);
	nlapiLogExecution('DEBUG', 'ResDetails', ResDetails);
	return ResDetails;
}

/* This function returns sales order details */
function GetSalesOrderDetails(closedTaskList,salesOrderList){
	nlapiLogExecution('DEBUG', 'GetSalesOrderDetails', 'Start');



	var salesorderDetailListT = new Array();
	try
	{
		//12344444
		var salesorderDetailList = new Array();
		//commented by mahesh
		//var salesOrderList = getDistinctOrderIDsFromTaskList(closedTaskList);

		nlapiLogExecution('DEBUG', 'SOList', salesOrderList);

		//salesOrderList = ProvideInputs(salesOrderList,'List');

		//nlapiLogExecution('DEBUG', 'SOList with Provideinputs', salesOrderList);

		var filters = new Array();
		var columns = new Array();


		filters.push(new nlobjSearchFilter('internalid', null, 'anyof', salesOrderList));
		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

		// Getting sku type is pending


		columns[0] = new nlobjSearchColumn('tranid');
		columns[1] = new nlobjSearchColumn('tranid');
		//columns[1] = new nlobjSearchColumn('custbody_edi_ref_mr');
		////var alternateid=salesorderRec.getLineItemValue('item','custcol13',lineno);
		/// carrieroptions
		///Shipping method
		columns[2] = new nlobjSearchColumn('shipmethod');
		columns[3] = new nlobjSearchColumn('shipmethod');
		columns[4] = new nlobjSearchColumn('shipaddressee');
		columns[5] = new nlobjSearchColumn('shipaddressee');
		columns[6] = new nlobjSearchColumn('shipaddress1');
		columns[7] = new nlobjSearchColumn('shipaddress2');
		columns[8] = new nlobjSearchColumn('shipcity');
		columns[9] = new nlobjSearchColumn('shipstate');
		columns[10] = new nlobjSearchColumn('shipcountry');
		columns[11] = new nlobjSearchColumn('shipzip');
		columns[12] = new nlobjSearchColumn('custbody_nswmspoexpshipdate');
		columns[13] = new nlobjSearchColumn('custbody_nswmsactualarrivaldate');
		columns[14] = new nlobjSearchColumn('custbody_nswmssodestination');
		columns[15] = new nlobjSearchColumn('custbody_nswmssoroute');
		columns[16] = new nlobjSearchColumn('class');
		columns[17] = new nlobjSearchColumn('entity');
		columns[18] = new nlobjSearchColumn('billaddressee');
		columns[19] = new nlobjSearchColumn('billaddress1');
		columns[20] = new nlobjSearchColumn('billaddress2');
		columns[21] = new nlobjSearchColumn('billaddress3');
		columns[22] = new nlobjSearchColumn('billcity');
		columns[23] = new nlobjSearchColumn('billstate');
		columns[24] = new nlobjSearchColumn('billzip');
		columns[25] = new nlobjSearchColumn('billcountry');
		columns[26] = new nlobjSearchColumn('billphone');
		columns[27] = new nlobjSearchColumn('location');
		columns[28] = new nlobjSearchColumn('custbody_nswms_company');
		columns[29] = new nlobjSearchColumn('otherrefnum');
		columns[30] = new nlobjSearchColumn('terms');
		columns[31] = new nlobjSearchColumn('custbody_nswmssoordertype');		                                   
		columns[32] = new nlobjSearchColumn('custbody_nswmspriority');
		columns[33] = new nlobjSearchColumn('otherrefnum');
		columns[34] = new nlobjSearchColumn('shipphone');
		columns[35] = new nlobjSearchColumn('shipphone');
		columns[36] = new nlobjSearchColumn('shipphone');
		columns[37] = new nlobjSearchColumn('custbody_nswmsarrivaldate');
		columns[38] = new nlobjSearchColumn('internalid');		
		columns[39] = new nlobjSearchColumn('class');
		columns[40] = new nlobjSearchColumn('custbody_shipment_no');

		columns[41] = new nlobjSearchColumn('custbody_total_weight');
		columns[42] = new nlobjSearchColumn('shipphone');	
		columns[43] = new nlobjSearchColumn('department');	
		columns[44] = new nlobjSearchColumn('shipphone');	
		columns[45] = new nlobjSearchColumn('custbody_nswmssoroute');	
		columns[46] = new nlobjSearchColumn('custbody_nswmssodestination');
		columns[47] = new nlobjSearchColumn('department');
		columns[48] = new nlobjSearchColumn('department');

		//235235,235233,235234

		var ResSSODetails = nlapiSearchRecord('salesorder', null, filters,columns);
		nlapiLogExecution('DEBUG', 'ResSSODetails Length', ResSSODetails.length);

		if ( ResSSODetails !=null && ResSSODetails.length > 0)
		{
			for ( var i = 0 ; i < ResSSODetails.length; i++ ){
				salesorderDetailList = new Array();

				salesorderDetailList[0] = ResSSODetails[i].getValue('tranid');
				salesorderDetailList[1] = ResSSODetails[i].getValue('custbody_shipment_no');
				salesorderDetailList[2] = ResSSODetails[i].getValue('custbody_shipment_no');
				salesorderDetailList[3] = ResSSODetails[i].getText('Shipmethod');
				salesorderDetailList[4] = ResSSODetails[i].getValue('Shipmethod');
				salesorderDetailList[5]=  ResSSODetails[i].getValue('custbody_shipment_no');
				salesorderDetailList[6] = ResSSODetails[i].getValue('shipaddressee');
				salesorderDetailList[7] = ResSSODetails[i].getValue('shipaddress1');
				nlapiLogExecution('DEBUG', 'shipaddr1', ResSSODetails[i].getValue('shipaddr1'));
				nlapiLogExecution('DEBUG', 'shipaddr2', ResSSODetails[i].getValue('shipaddr2'));
				salesorderDetailList[8] = ResSSODetails[i].getValue('shipaddress2');
				salesorderDetailList[9] = ResSSODetails[i].getValue('shipcity');
				salesorderDetailList[10] = ResSSODetails[i].getValue('shipstate');
				salesorderDetailList[11] = ResSSODetails[i].getValue('shipcountry');
				salesorderDetailList[12] = ResSSODetails[i].getValue('shipzip');
				salesorderDetailList[13] = ResSSODetails[i].getValue('custbody_nswmspoexpshipdate');
				salesorderDetailList[14] = ResSSODetails[i].getValue('custbody_nswmsactualarrivaldate');
				salesorderDetailList[15] = ResSSODetails[i].getValue('custbody_nswmssodestination');
				//salesorderDetailList[16] = ResSSODetails[i].getText('custbody_nswmssoroute');
				salesorderDetailList[16] = ResSSODetails[i].getValue('custbody_shipment_no');
				nlapiLogExecution('DEBUG', 'salesorderDetailList[16]',salesorderDetailList[16]);
				nlapiLogExecution('DEBUG', 'ResSSODetails[i].getText',ResSSODetails[i].getText('custbody_shipment_no'));
				salesorderDetailList[17] = ResSSODetails[i].getValue('shipzip');
				salesorderDetailList[18] = ResSSODetails[i].getText('class');
				salesorderDetailList[19] = ResSSODetails[i].getText('entity');
				salesorderDetailList[20] = ResSSODetails[i].getValue('billaddressee');
				salesorderDetailList[21] = ResSSODetails[i].getValue('billaddress1');
				salesorderDetailList[22] = ResSSODetails[i].getValue('billaddress2');
				salesorderDetailList[23] = ResSSODetails[i].getValue('billaddress3');
				salesorderDetailList[24] = ResSSODetails[i].getValue('billcity');
				salesorderDetailList[25] = ResSSODetails[i].getValue('billstate');
				salesorderDetailList[26] = ResSSODetails[i].getValue('billzip');
				salesorderDetailList[27] = ResSSODetails[i].getValue('billcountry');
				salesorderDetailList[28] = ResSSODetails[i].getValue('billphone');
				salesorderDetailList[29] = ResSSODetails[i].getText('location');
				salesorderDetailList[30] = ResSSODetails[i].getText('custbody_nswms_company');
				salesorderDetailList[31] = ResSSODetails[i].getText('department');
				salesorderDetailList[32]=  ResSSODetails[i].getText('terms');
				//salesorderDetailList[33]=  ResSSODetails[i].getText('custbody_nswmsordertype');
				salesorderDetailList[33]=  ResSSODetails[i].getText('custbody_nswmssoordertype');
				salesorderDetailList[34] = ResSSODetails[i].getText('custbody_nswmspriority');
				salesorderDetailList[35] = ResSSODetails[i].getValue('otherrefnum');
				salesorderDetailList[36] = ResSSODetails[i].getValue('shipphone');
				salesorderDetailList[37] = ResSSODetails[i].getValue('billphone');
				salesorderDetailList[38] = ResSSODetails[i].getText('custbody_nswmspriority');
				salesorderDetailList[39] = ResSSODetails[i].getValue('otherrefnum');
				//salesorderDetailList[40]=  ResSSODetails[i].getValue('shipmethod').getSelectOptions(null, null);
				salesorderDetailList[41] = ResSSODetails[i].getValue('custbody_locationaddressid');
				//salesorderDetailList[42] = ResSSODetails[i].getValue('ShiptoPhone');
				salesorderDetailList[43] = ResSSODetails[i].getValue('custbody_nswmsarrivaldate');
				salesorderDetailList[44] = ResSSODetails[i].getValue('internalid');
				salesorderDetailList[45] = ResSSODetails[i].getText('class');

				salesorderDetailList[46] = ResSSODetails[i].getValue('custbody_total_weight');
				salesorderDetailList[47] = ResSSODetails[i].getValue('custbody_total_vol');	
				salesorderDetailList[48] = ResSSODetails[i].getValue('department');
				salesorderDetailList[49] = ResSSODetails[i].getValue('shipphone');
				salesorderDetailList[50] = ResSSODetails[i].getText('custbody_nswmssoroute');
				salesorderDetailList[51] = ResSSODetails[i].getValue('custbody_nswmssodestination');
				salesorderDetailList[52] = ResSSODetails[i].getValue('department');
				salesorderDetailList[53] = ResSSODetails[i].getValue('department');
				salesorderDetailList[54] = ResSSODetails[i].getValue('location');
				salesorderDetailList[55] = ResSSODetails[i].getValue('entity');
				salesorderDetailListT.push(salesorderDetailList);
			}
		}
		// Get details from item master against the given item

	}

	catch(exception) 
	{
		nlapiLogExecution('DEBUG', 'Exception GetSalesOrderDetails', exception);
	}

	nlapiLogExecution('DEBUG', 'GetSalesOrderDetails', 'end');
	nlapiLogExecution('DEBUG', 'salesorderDetailList', salesorderDetailList);

	return salesorderDetailListT;

}

function GetMasterBOL(salesOrderList)
{
	var masterbol="",opentaskpronumber="";
	var arrbolpronum = new Array();
	var bolfilters = new Array();		
	bolfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['14']));
	if(salesOrderList !=null && salesOrderList !=""){
		bolfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', salesOrderList));	}	

	var bolcolumns = new Array();
	bolcolumns[0] = new nlobjSearchColumn('custrecord_bol');  
	bolcolumns[1] = new nlobjSearchColumn('custrecord_pro');  
	searchresultsbol = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, bolfilters, bolcolumns);
	if( searchresultsbol != null &&  searchresultsbol != "")
	{
		masterbol=searchresultsbol[0].getValue('custrecord_bol');
		opentaskpronumber=searchresultsbol[0].getValue('custrecord_pro');
		arrbolpronum.push(masterbol);
		arrbolpronum.push(opentaskpronumber);
	}

	return arrbolpronum;
}

/* This function returns shipmanifest details for the contlp */
function GetShipmanifestDetails(closedTaskList, contlp){
	nlapiLogExecution('DEBUG', 'GetShipmanifestDetails', 'Start');


	var shipmanifestDetailsListT = new Array();
	try
	{
		//var salesOrderList = getDistinctOrderIDsFromTaskList(closedTaskList);

		var filters = new Array();
		var columns = new Array();
		filters.push(new nlobjSearchFilter('custrecord_ship_contlp', null, 'is', contlp));
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ship_charges');
		columns[1] =  new nlobjSearchColumn('custrecord_ship_trackno');
		columns[2] = new nlobjSearchColumn('custrecord_ship_contlp');
		columns[3] = new nlobjSearchColumn('custrecord_ship_orderno');


		var ResShipmanfDetails = nlapiSearchRecord('customrecord_ship_manifest', null, filters,columns);

		if ( ResShipmanfDetails !=null && ResShipmanfDetails.length > 0){
			for ( var i = 0 ; i < ResShipmanfDetails.length; i++ ){
				var shipmanifestDetailsList = new Array();
				shipmanifestDetailsList[0] = ResShipmanfDetails[i].getValue('custrecord_ship_contlp');
				shipmanifestDetailsList[1] = ResShipmanfDetails[i].getValue('custrecord_ship_trackno');
				shipmanifestDetailsList[2] = ResShipmanfDetails[i].getValue('custrecord_ship_charges');
				shipmanifestDetailsList[3] = ResShipmanfDetails[i].getValue('custrecord_ship_orderno');
				shipmanifestDetailsListT.push(shipmanifestDetailsList);
			}
		}
	}

	catch(exception) 
	{
		nlapiLogExecution('DEBUG', 'Get GetShipmanifestDetails', exception);
	}

	nlapiLogExecution('DEBUG', 'GetShipmanifestDetails', 'end');
	nlapiLogExecution('DEBUG', 'shipmanifestDetailsListT', shipmanifestDetailsListT);
	return shipmanifestDetailsListT;

}

/* This function returns shipmanifest details for all the distinct orders */
function GetIndividualShipmanifestDetails(shipmanifestDetails, contlp) {
	nlapiLogExecution('DEBUG', 'GetIndividualShipmanifestDetails', 'Start');
	var IndividualShipmanifestDetailsT = new Array();
	try {

		if (shipmanifestDetails != null && shipmanifestDetails.length > 0) {
			for ( var i = 0; i < shipmanifestDetails.length; i++) {
				if (IndividualShipmanifestDetails[i]['ContLP'] == contlp) {
					var IndividualShipmanifestDetails = new Array();
					IndividualShipmanifestDetails[0] = shipmanifestDetails[i]
					.getValue('TrackingNumbers');
					IndividualShipmanifestDetails[1] = shipmanifestDetails[i]
					.getValue('ShippingCharges');
					IndividualShipmanifestDetailsT
					.push(IndividualShipmanifestDetails);
				}
			}
		}
	}

	catch (exception) {
		nlapiLogExecution('DEBUG', ' GetIndividualShipmanifestDetails',
				exception);
	}

	nlapiLogExecution('DEBUG', 'GetIndividualShipmanifestDetails', 'end');
	return IndividualShipmanifestDetails;

}


/* This function returns SCAC code of the carrier */
function ReturnSCACCode(ShipCarrierOptions,  ShipMethodActValue)
{
	nlapiLogExecution('DEBUG', 'ReturnSCACCode', 'Start');
	nlapiLogExecution('DEBUG', 'ShipmethodActvalue', ShipMethodActValue);
	nlapiLogExecution('DEBUG', 'ShipCarrierOptions', ShipCarrierOptions);
	var shipItemCode="";
	try {

		var ShipItemID ="";
		var shipItemText="";
		for(var i=0; i<ShipCarrierOptions.length; i++){
			ShipItemID = ShipCarrierOptions[i].getId();
			shipItemText = ShipCarrierOptions[i].getText();

			if ( ShipItemID == ShipMethodActValue)
			{
				var res = nlapiSearchGlobal(shipItemText);
				nlapiLogExecution('DEBUG', 'shipItemText', shipItemText);

				if(res != null && res.length >0 &&  res[0].getValue('type') == 'Shipping Cost Item')
				{
					shipItemCode = res[0].getValue('info1');
					i=ShipCarrierOptions.length;
				}
			}
		}
	}

	catch(exception) 
	{
		nlapiLogExecution('DEBUG', 'Getting SCAC code ', exception);
	}
	nlapiLogExecution('DEBUG', 'Getting SCAC code -shipitemcode ', shipItemCode);
	return shipItemCode;

	nlapiLogExecution('DEBUG', 'ReturnSCACCode', 'End');
}

function isDistinctSKU(skuList, sku) {
	var matchFound = false;
	//nlapiLogExecution('DEBUG', 'isDistinctSKU', 'start');
	try {

		if (skuList != null && skuList.length > 0) {
			for ( var i = 0; i < skuList.length; i++) {
				if (skuList[i] == sku)
					matchFound = true;
			}
		}
	} catch (exception) {
		nlapiLogExecution('DEBUG', 'isDistinctSKU', exception);
	}
	//nlapiLogExecution('DEBUG', 'isDistinctSKU', 'end');
	return matchFound;
}

function isDistinctContainerSizeID(ContainerSizIDLIst, Containersizeid) {
	//nlapiLogExecution('DEBUG', 'isDistinctContainerSizeID', 'start');
	var matchFound = false;
	try {
		if (ContainerSizIDLIst != null && ContainerSizIDLIst.length > 0) {
			for ( var i = 0; i < ContainerSizIDLIst.length; i++) {
				if (ContainerSizIDLIst[i] == Containersizeid)
					matchFound = true;
			}
		}
	} catch (exception) {
		nlapiLogExecution('DEBUG', 'isDistinctContainerSizeID', exception);
	}
	//nlapiLogExecution('DEBUG', 'isDistinctContainerSizeID', 'end');
	return matchFound;
}



function isDistinctOrderID(OrderIDList, OrderID){
	var matchFound = false;
	if(OrderIDList != null && OrderIDList.length > 0){
		for(var i = 0; i < OrderIDList.length; i++){
			if(OrderIDList[i] == OrderID)
				matchFound = true;
		}
	}

	return matchFound;
}

function isDistinctContainer(ContainerIDList, ContLP){
	var matchFound = false;
	if(ContainerIDList != null && ContainerIDList.length > 0){
		for(var i = 0; i < ContainerIDList.length; i++){
			if(ContainerIDList[i] == ContLP)
				matchFound = true;
		}
	}

	return matchFound;
}

function getDistinctContLPsFromTaskList(closedTaskList){
	nlapiLogExecution('DEBUG', 'getDistinctContLPsFromTaskList', 'Start');
	var ContLPList = new Array();

	if(closedTaskList != null && closedTaskList.length > 0){
		for(var i = 0; i < closedTaskList.length; i++){
			var ContLP = closedTaskList[i].getValue('custrecord_ebiztask_ebiz_contlp_no');
			if(!isDistinctContainer(ContLPList, ContLP))
				ContLPList.push(ContLP);
		}
	}

	nlapiLogExecution('DEBUG', 'getDistinctContLPsFromTaskList', 'End');
	nlapiLogExecution('DEBUG', 'ContLPList', ContLPList);
	return ContLPList;
}
/* This function returns disict container sizeids for the shipLP */
function getDistinctContainerSizeIDsFromTaskList(closedTaskList){
	nlapiLogExecution('DEBUG', 'getDistinctContainerSizeIDsFromTaskList', 'Start');
	var ContainerSizeIDList = new Array();
	try{

		if(closedTaskList != null && closedTaskList.length > 0)
		{
			for(var i = 0; i < closedTaskList.length; i++)
			{
				var ContainerSizeID = closedTaskList[i].getValue('custrecord_container');
				if(!isDistinctContainerSizeID(ContainerSizeIDList, ContainerSizeID))
					ContainerSizeIDList.push(ContainerSizeID);
			}
		}

		nlapiLogExecution('DEBUG', 'getDistinctContainerSizeIDsFromTaskList', 'End');
	}

	catch(exception) 
	{
		nlapiLogExecution('DEBUG', 'getDistinctContainerSizeIDsFromTaskList', exception);
	}
	nlapiLogExecution('DEBUG', 'getDistinctContainerSizeIDsFromTaskList', 'End');
	return ContainerSizeIDList;

}

/**
 * Function will take container size id as input and return the dims.
 * @param skulist
 * @returns {Array}
 */
function GetContainerDims(ContainerSizeIDs){
	nlapiLogExecution('DEBUG', 'GetContainerDims', 'Start');
	var ContainerDims = new Array();
	var ContainerDimsT = new Array();
	try{
		// Get the list of distinct SKUs
		//var ContainerSizeIDs = getDistinctContainerSizeIDsFromTaskList(closedTaskList);
		//nlapiLogExecution('DEBUG', 'GetContainerDims-Containersizeids', ContainerSizeIDs);
		var filters = new Array();
		var columns = new Array();
		filters.push(new nlobjSearchFilter('internalid' , null, 'is',ContainerSizeIDs));
		// Getting sku type is pending

		columns[0] = new nlobjSearchColumn('custrecord_length');
		columns[1] = new nlobjSearchColumn('custrecord_widthcontainer');
		columns[2] = new nlobjSearchColumn('custrecord_heightcontainer');
		columns[3] = new nlobjSearchColumn('custrecord_maxweight');
		columns[4] = new nlobjSearchColumn('custrecord_tareweight');
		columns[5] = new nlobjSearchColumn('custrecord_cubecontainer');
		columns[6] = new nlobjSearchColumn('internalid');

		var ResContainerDims = nlapiSearchRecord('customrecord_ebiznet_container', null,filters, columns);

		if(ResContainerDims != null && ResContainerDims.length > 0){
			for(var i = 0; i < ResContainerDims.length; i++ ){
				ContainerDims = new Array();
				ContainerDims[0] = ResContainerDims[i].getValue('custrecord_length');
				ContainerDims[1] = ResContainerDims[i].getValue('custrecord_widthcontainer');
				ContainerDims[2] = ResContainerDims[i].getValue('custrecord_heightcontainer');
				ContainerDims[3] = ResContainerDims[i].getValue('custrecord_maxweight');
				ContainerDims[4] = ResContainerDims[i].getValue('custrecord_tareweight');
				ContainerDims[5] = ResContainerDims[i].getValue('custrecord_cubecontainer');
				ContainerDims[6] = ResContainerDims[i].getValue('internalid');
				ContainerDimsT.push(ContainerDims);
			}

		}
	}catch(exception){
		nlapiLogExecution('DEBUG', 'GetContainerDims', exception);
	}

	nlapiLogExecution('DEBUG', 'GetContainerDims', 'end');
	nlapiLogExecution('DEBUG', 'ContainerDimsT', ContainerDimsT);
	return ContainerDimsT;
}
function GetSHIPASISContainerDims(uomlevel,skuid)
{
	nlapiLogExecution('DEBUG', 'GetSHIPASISContainerDims', 'Start');
	var ContainerDims = new Array();
	var ContainerDimsT = new Array();
	try{		
		nlapiLogExecution('DEBUG', 'GetSHIPASISContainerDims skuid', skuid);
		nlapiLogExecution('DEBUG', 'GetSHIPASISContainerDims uomlevel', uomlevel);
		var filters = new Array();          
		if(skuid !=null && skuid !=""){
			filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof',skuid)); 		
		}
		filters.push(new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'anyof', uomlevel));		

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizlength');
		columns[1] = new nlobjSearchColumn('custrecord_ebizwidth');
		columns[2] = new nlobjSearchColumn('custrecord_ebizheight');
		columns[3] = new nlobjSearchColumn('custrecord_ebizweight');
		columns[4] = new nlobjSearchColumn('custrecord_ebizcube');

		var ResContainerDims = nlapiSearchRecord('customrecord_ebiznet_skudims',null, filters, columns);

		if(ResContainerDims != null && ResContainerDims.length > 0){
			for(var i = 0; i < ResContainerDims.length; i++ ){
				ContainerDims = new Array();
				ContainerDims[0] = ResContainerDims[i].getValue('custrecord_ebizlength');
				ContainerDims[1] = ResContainerDims[i].getValue('custrecord_ebizwidth');
				ContainerDims[2] = ResContainerDims[i].getValue('custrecord_ebizheight');
				ContainerDims[3] = ResContainerDims[i].getValue('custrecord_ebizweight');
				ContainerDims[4] = ResContainerDims[i].getValue('custrecord_ebizcube');				
				ContainerDimsT.push(ContainerDims);
			}

		}
	}catch(exception){
		nlapiLogExecution('DEBUG', 'GetSHIPASISContainerDims', exception);
	}

	nlapiLogExecution('DEBUG', 'GetSHIPASISContainerDims', 'end');
	nlapiLogExecution('DEBUG', 'GetSHIPASISContainerDims', ContainerDimsT);
	return ContainerDimsT;
}
/**
 * Function to retrieve the Container details for a list of ContainerSizeIDS
 * @param skulist
 * @returns {Array}
 */
function GetIndividualContainerDims(containerdims , containersizeid ){
	nlapiLogExecution('DEBUG', 'GetIndividualContainerDims', 'Start');
	nlapiLogExecution('DEBUG', 'containerdims', containerdims);
	nlapiLogExecution('DEBUG', 'containersizeid', containersizeid);
	var IndividualContainerDimslist = new Array();
	var IndividualContainerDimslistT = new Array();
	try{
		if(containerdims != null && containerdims.length > 0){
			for(var i = 0; i < containerdims.length; i++ ){

				if (ContainerDims[i][6] == containersizeid){

					//nlapiLogExecution('DEBUG', 'containersizeid is matches ', ContainerDims[i][6]);

					IndividualContainerDimslist = new Array();
					IndividualContainerDimslist[0] = containerdims[i].getValue('ContainerLength');
					IndividualContainerDimslist[1] = containerdims[i].getValue('ContainerWidth');
					IndividualContainerDimslist[2] = containerdims[i].getValue('ContainerHeight');
					IndividualContainerDimslist[3] = containerdims[i].getValue('ContainerMaxWeight');	
					IndividualContainerDimslist[4] = containerdims[i].getValue('ContainerHeight');
					IndividualContainerDimslist[5] = containerdims[i].getValue('ContainerCube');
					IndividualContainerDimslistT.push(IndividualContainerDimslist);
				}
			}
		}
	}catch(exception){
		nlapiLogExecution('DEBUG', 'GetIndividualContainerDims', exception);
	}

	nlapiLogExecution('DEBUG', 'GetIndividualContainerDims', 'end');
	return IndividualContainerDimslist;
}


/*
 * This function returns SUCC, shipunit total cube , shipunit total weight
 * details
 */
function GetContainerSUCCDetails(shipLP){
	nlapiLogExecution('DEBUG', 'GetContainerSUCCDetails', 'Start');


	var ShipunitDetailsListT = new Array();
	try
	{
		var filters = new Array();
		var columns = new Array();


		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp',  null, 'is',shipLP));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sscc');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totcube');
		columns[2] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');

		var ResShipunitDetails = nlapiSearchRecord('customrecord_ebiznet_master_lp', null,	filters, columns);
		if ( ResShipunitDetails !=null && ResShipunitDetails.length > 0)
		{
			for ( var i = 0 ; i < ResShipunitDetails.length; i++ ){
				var ShipunitDetailsList = new Array();
				ShipunitDetailsList[0] = ResShipunitDetails[i].getValue('SHILP');
				ShipunitDetailsList[1] = ResShipunitDetails[i].getValue('custrecord_ebiz_lpmaster_sscc');
				ShipunitDetailsList[2] = ResShipunitDetails[i].getValue('custrecord_ebiz_lpmaster_totcube');
				ShipunitDetailsList[3] = ResShipunitDetails[i].getValue('custrecord_ebiz_lpmaster_totwght');
				ShipunitDetailsListT.push(ShipunitDetailsList);
			}
		}
	}

	catch(exception) 
	{
		nlapiLogExecution('DEBUG', ' GetContainerSUCCDetails', exception);
	}

	nlapiLogExecution('DEBUG', 'GetContainerSUCCDetails', 'end');
	nlapiLogExecution('DEBUG', 'ShipunitDetailsListT',ShipunitDetailsListT);
	return ShipunitDetailsListT;

}
/*
 * This function returns SUCC, shipunit total cube , shipunit total weight
 * details
 */
function GetTrailerDetails(trailer){
	nlapiLogExecution('DEBUG', 'GetTrailerDetails', 'Start');


	var TrailerDetailsListT = new Array();
	try
	{

		//var filters = new Array();
		var columns = new Array();
		var filters = new Array();
		filters.push( new nlobjSearchFilter('name', null,'is', trailer));
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizpro');
		columns[1] = new nlobjSearchColumn('custrecord_ebizseal');
		columns[2] = new nlobjSearchColumn('custrecord_ebizmastershipper');
		columns[3] = new nlobjSearchColumn('custrecord_ebizappointmenttrailer');
		columns[4] = new nlobjSearchColumn('custrecord_ebizexparrivaldate');
		columns[5] = new nlobjSearchColumn('custrecord_ebizcarrierid');
		var ResTrailerDet = nlapiSearchRecord('customrecord_ebiznet_trailer', null,filters, columns);
		if ( ResTrailerDet !=null && ResTrailerDet.length > 0){
			for ( var i = 0 ; i < ResTrailerDet.length; i++ ){
				var TrailerDetailsList = new Array();
				TrailerDetailsList[0] = ResTrailerDet[i].getValue('custrecord_ebizpro');
				TrailerDetailsList[1] = ResTrailerDet[i].getValue('custrecord_ebizseal');
				TrailerDetailsList[2] = ResTrailerDet[i].getValue('custrecord_ebizmastershipper');
				TrailerDetailsList[3] = ResTrailerDet[i].getValue('custrecord_ebizappointmenttrailer');
				TrailerDetailsList[4] = ResTrailerDet[i].getValue('custrecord_ebizexparrivaldate');
				TrailerDetailsList[5] = ResTrailerDet[i].getText('custrecord_ebizcarrierid');

				TrailerDetailsListT.push(TrailerDetailsList);
			}
		}
	}

	catch(exception) 
	{
		nlapiLogExecution('DEBUG', ' GetTrailerDetails', exception);
	}

	nlapiLogExecution('DEBUG', 'GetTrailerDetails', 'end');
	nlapiLogExecution('DEBUG', 'TrailerDetailsListT', TrailerDetailsListT);
	return TrailerDetailsListT;

}

//CASE # 201412758  Start
/* This function returns Container CUCC , container total cube , container total weight details  for the contlp */
function GetContainerCUCCDetails(closedTaskList, contlp){
	nlapiLogExecution('DEBUG', 'GetContainerCUCCDetails', 'Start');

	var ContainerDetailsList = new Array();
	var ContainerDetailsListT = new Array();

	try
	{
		//var ContLPDetails = getDistinctContLPsFromTaskList(closedTaskList);
		//nlapiLogExecution('DEBUG', 'ContLPDetails', ContLPDetails);
		var filters = new Array();
		var columns = new Array();

		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is',contlp));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_sscc', null, 'isnotempty'));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_wmsstatusflag', null, 'anyof', [28]));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sscc');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totcube');
		columns[2] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
		columns[3] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');

		var ResContainerDetails = nlapiSearchRecord('customrecord_ebiznet_master_lp', null,	filters, columns);		
		if ( ResContainerDetails !=null && ResContainerDetails.length > 0)
		{
			for ( var i = 0 ; i < ResContainerDetails.length; i++ ){
				ContainerDetailsList = new Array();
				ContainerDetailsList[0] = ResContainerDetails[i].getValue('custrecord_ebiz_lpmaster_lp');
				ContainerDetailsList[1] = ResContainerDetails[i].getValue('custrecord_ebiz_lpmaster_sscc');
				ContainerDetailsList[2] = ResContainerDetails[i].getValue('custrecord_ebiz_lpmaster_totcube');
				ContainerDetailsList[3] = ResContainerDetails[i].getValue('custrecord_ebiz_lpmaster_totwght');
				ContainerDetailsListT.push(ContainerDetailsList);
			}
		}
		// Get details from item master against the given item

	}


	catch(exception) 
	{
		nlapiLogExecution('DEBUG', ' GetContainerCUCCDetails', exception);
	}

	nlapiLogExecution('DEBUG', 'GetContainerCUCCDetails', 'end');
	nlapiLogExecution('DEBUG', ' ContainerDetailsListT', ContainerDetailsListT);
	return ContainerDetailsListT;

}
//CASE # 201412758  End
/*This fuction returns EUCC numbers for the container */

function GetContainerEUCCDetails(closedTaskList, contlp,ItemID){
	nlapiLogExecution('DEBUG', 'GetContainerEUCCDetails', 'Start');

	var ContainerEUCCDetailsT = new Array();

	try
	{

		var EUCC="";

		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_masterlp',  null, 'is',contlp));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_item',  null, 'is',ItemID));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sscc');

		var ResContainerEUCCDetails = nlapiSearchRecord('customrecord_ebiznet_master_lp' , null,	filters, columns);		
		if ( ResContainerEUCCDetails !=null && ResContainerEUCCDetails.length > 0)
		{
			for ( var i = 0 ; i < ResContainerEUCCDetails.length; i++ ){

				if ( EUCC != null  && EUCC !="" )
				{
					EUCC = EUCC +  "," + ResContainerEUCCDetails[i].getValue('custrecord_ebiz_lpmaster_sscc');
				}
				else
				{
					EUCC = ResContainerEUCCDetails[i].getValue('custrecord_ebiz_lpmaster_sscc');
				}

			}

		}
		// Get details from item master against the given item

	}

	catch(exception) 
	{
		nlapiLogExecution('DEBUG', ' GetContainerEUCCDetails', exception);
	}

	nlapiLogExecution('DEBUG', 'GetContainerEUCCDetails', 'end');
	nlapiLogExecution('DEBUG', 'EUCC', EUCC);
	return EUCC;

}




/*This fuction returns EUCC numbers for the container */

function GetIndividiualEUCCDetails(EUCCdetailslist, contlp){
	nlapiLogExecution('DEBUG', 'GetIndividiualEUCCDetails', 'Start');
	nlapiLogExecution('DEBUG', 'EUCCdetailslist', EUCCdetailslist);
	var IndividualEUCCDetailsList = new Array();
	var IndividualEUCCDetailsListT = new Array();
	try
	{
		if ( EUCCdetailslist !=null && EUCCdetailslist.length > 0)
		{
			for ( var i = 0 ; i < EUCCdetailslist.length; i++ ){

				if (EUCCdetailslist[i]['ContLP'] = contlp) {

					IndividualEUCCDetailsList[0]  = IndividualEUCCDetailsList[0]['EUCC'] + ",";


				} 


			}
			IndividualEUCCDetailsListT.push(IndividualEUCCDetailsList);
		}
		// Get details from item master against the given item

	}

	catch(exception) 
	{
		nlapiLogExecution('DEBUG', ' GetIndividiualEUCCDetails', exception);
	}

	nlapiLogExecution('DEBUG', 'GetIndividiualEUCCDetails', 'end');
	return IndividualEUCCDetailsListT;

}

/*  This function returns distinct internal ids of skus */
function getDistinctSKUsFromTaskList(closedTaskList){
	nlapiLogExecution('DEBUG', 'getDistinctSKUsFromTaskList', 'Start');
	var skusList = new Array();
	try{	
		if(closedTaskList != null && closedTaskList.length > 0){
			for(var i = 0; i < closedTaskList.length; i++){
				var skuID = closedTaskList[i].getValue('custrecord_ebiztask_ebiz_sku_no');
				//nlapiLogExecution('DEBUG', 'skuID', skuID);
				if(!isDistinctSKU(skusList, skuID))
					skusList.push(skuID);

			}
		}
	}
	catch(exception) 
	{
		nlapiLogExecution('DEBUG', 'getDistinctSKUsFromTaskList', exception);
	}

	nlapiLogExecution('DEBUG', 'getDistinctSKUsFromTaskList', 'End');
	nlapiLogExecution('DEBUG', 'skulist', skusList);
	return skusList;
}

/*  This function returns distinct internal ids of skus with square brackets which can be used for search filters */
function getDistinctSKUsFromTaskListLR(closedTaskList){
	nlapiLogExecution('DEBUG', 'getDistinctSKUsFromTaskListLR', 'Start');
	var skuListLR = new Array();
	try{	
		if(closedTaskList != null && closedTaskList.length > 0){
			for(var i = 0; i < closedTaskList.length; i++){
				var skuID = closedTaskList[i].getValue('custrecord_ebiztask_ebiz_sku_no');
				skuID= '[' + skuID + ']';
				nlapiLogExecution('DEBUG', 'skuID', skuID);
				if(!isDistinctSKU(skuListLR, skuID))
					skuListLR.push(skuID);

			}
		}
	}
	catch(exception) 
	{
		nlapiLogExecution('DEBUG', 'getDistinctSKUsFromTaskListLR', exception);
	}

	nlapiLogExecution('DEBUG', 'getDistinctSKUsFromTaskListLR', 'End');
	nlapiLogExecution('DEBUG', 'skulistLR', skuListLR);
	return skuListLR;
}

/**
 * 
 * @param closedTaskList
 * @returns {Array}
 */
function getDistinctOrderIDsFromTaskList(closedTaskList){
	nlapiLogExecution('DEBUG', 'getDistinctOrderIDsFromTaskList', 'Start');
	var OrderIDList = new Array();
	try{

		if(closedTaskList != null && closedTaskList.length > 0)
		{
			for(var i = 0; i < closedTaskList.length; i++)
			{
				var OrderID = closedTaskList[i].getValue('custrecord_ebiztask_ebiz_order_no');
				if(!isDistinctOrderID(OrderIDList, OrderID))
					OrderIDList.push(OrderID);
			}
		}

		nlapiLogExecution('DEBUG', 'getDistinctOrderIDsFromTaskList', 'End');
	}

	catch(exception) 
	{
		nlapiLogExecution('DEBUG', 'getDistinctOrderIDsFromTaskList', exception);
	}
	nlapiLogExecution('DEBUG', 'getDistinctOrderIDsFromTaskList', 'End');
	return OrderIDList;

}
/**
 * Function to retrieve the UPC details for the list of skus
 * 
 * @param skulist
 * @returns {Array}
 */
function GetUPCDetails(closedTaskList){
	nlapiLogExecution('DEBUG', 'GetUPCDetails', 'Start');

	var upcDetailsListT = new Array();

	try{
		// Get the list of distinct SKUs
		var skulist = getDistinctSKUsFromTaskList(closedTaskList);

		var filters = new Array();
		var columns = new Array();


		filters.push(new nlobjSearchFilter('custrecord17', null, 'anyof',skulist)); // item
		filters.push(new nlobjSearchFilter('custrecord16', null, 'is', 'U'));
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord17'); // item
		columns[1] = new nlobjSearchColumn('custrecord14'); // UPC

		nlapiLogExecution('DEBUG', 'Beforesearch', 'Start');

		var resUPCDetails = nlapiSearchRecord('customrecord265', null,filters, columns);

		if(resUPCDetails != null && resUPCDetails.length > 0){
			for(var i = 0; i < resUPCDetails.length; i++ ){
				var upcDetailsList = new Array();
				upcDetailsList[0] = resUPCDetails[i].getValue('custrecord17');
				upcDetailsList[1] = resUPCDetails[i].getValue('custrecord14');
				upcDetailsListT.push(upcDetailsList);
			}
		}
	}catch(exception){
		nlapiLogExecution('DEBUG', 'GetUPCDetails', exception);
	}

	nlapiLogExecution('DEBUG', 'GetUPCDetails', 'end');
	return upcDetailsListT;
}



/**
 * Function to retrieve the batch details for the list of SKUS
 * 
 * @param skulist
 * @returns {Array}
 */
function GetBatchDetails(closedTaskList,skulist){
	nlapiLogExecution('DEBUG', 'GetBatchDetails', 'Start');

	var BatchDetailsListT = new Array();

	try{
		// Get the list of distinct SKUs
		//var skulist = getDistinctSKUsFromTaskList(closedTaskList);

		var filters = new Array();
		var columns = new Array();
		nlapiLogExecution('DEBUG', 'GetBatchDetails skulist', skulist);

		filters.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof',skulist));
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizlotbatch');
		columns[1] = new nlobjSearchColumn('custrecord_ebizsku');

		var ResBatchDetails = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null,filters, columns);

		if(ResBatchDetails != null && ResBatchDetails.length > 0){
			for(var i = 0; i < ResBatchDetails.length; i++ ){
				var BatchDetailsList = new Array();
				BatchDetailsList[0] = ResBatchDetails[i].getValue('custrecord_ebizsku');
				BatchDetailsList[1] = ResBatchDetails[i].getValue('custrecord_ebizlotbatch');
				BatchDetailsListT.push(BatchDetailsList);
			}
		}
	}catch(exception){
		nlapiLogExecution('DEBUG', 'GetBatchDetails', exception);
	}

	nlapiLogExecution('DEBUG', 'GetBatchDetails', 'end');
	return BatchDetailsList;
}


/**
 * Function to retrieve the SKU details for a list of SKUs
 * @param skulist
 * @returns {Array}
 */
function GetUOMDetails(closedTaskList,skulist){
	nlapiLogExecution('DEBUG', 'GetUOMDetails', 'Start');

	var uomDetailsListT = new Array();
	//var skulist = new Array();
	var skustring ="";


	try{
		// Get the list of distinct SKUs
		//skulist = getDistinctSKUsFromTaskList(closedTaskList);
		nlapiLogExecution('DEBUG', 'Before provide inputs SKUList', skulist);
		// This API returns skulist as [1,2,3];
		// skustring = ProvideInputs(skulist,'List' );
		var filters = new Array();
		var columns = new Array();

		nlapiLogExecution('DEBUG', 'After provide inputs -SKUList', skulist);
		filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof',skulist));


		// Getting sku type is pending
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizuomskudim');
		columns[1] = new nlobjSearchColumn('custrecord_ebizqty');
		columns[2] = new nlobjSearchColumn('custrecord_ebizlength');
		columns[3] = new nlobjSearchColumn('custrecord_ebizwidth');
		columns[4] = new nlobjSearchColumn('custrecord_ebizheight');
		columns[5] = new nlobjSearchColumn('custrecord_ebizweight');
		columns[6] = new nlobjSearchColumn('custrecord_ebizcube');
		columns[7] = new nlobjSearchColumn('custrecord_ebizitemdims');
		columns[8] = new nlobjSearchColumn('custrecord_ebizpackcodeskudim');
		columns[9] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim');

		var ResItemDims = nlapiSearchRecord('customrecord_ebiznet_skudims', null,filters, columns);

		if(ResItemDims != null && ResItemDims.length > 0){
			for(var i = 0; i < ResItemDims.length; i++ ){

				var uomDetailsList = new Array();
				uomDetailsList[0] = ResItemDims[i].getText('custrecord_ebizuomskudim');
				uomDetailsList[1] = ResItemDims[i].getValue('custrecord_ebizqty');
				uomDetailsList[2] = ResItemDims[i].getValue('custrecord_ebizlength');
				uomDetailsList[3] = ResItemDims[i].getValue('custrecord_ebizwidth');
				uomDetailsList[4] = ResItemDims[i].getValue('custrecord_ebizheight');
				uomDetailsList[5] = ResItemDims[i].getValue('custrecord_ebizweight');
				uomDetailsList[6] = ResItemDims[i].getValue('custrecord_ebizcube');
				uomDetailsList[7] = ResItemDims[i].getValue('custrecord_ebizitemdims');
				uomDetailsList[8] = ResItemDims[i].getValue('custrecord_ebizpackcodeskudim');
				uomDetailsList[9] = ResItemDims[i].getValue('custrecord_ebizuomlevelskudim');
				nlapiLogExecution('DEBUG', 'Get UOM Details - Array pushing to 2D Array',uomDetailsList );
				uomDetailsListT.push(uomDetailsList);
			}
		}
	}catch(exception){
		nlapiLogExecution('DEBUG', 'Get UOM Details', exception);
	}

	nlapiLogExecution('DEBUG', 'GetUOMDetails', 'end');
	nlapiLogExecution('DEBUG', 'GetUOMDetails',uomDetailsListT.length );
	return uomDetailsListT;

}

/* This function accepts a list and based on the object it will return the data */
function ProvideInputs(recordset, mode) {
	nlapiLogExecution('DEBUG', 'ProvideInputs', 'Start');
	nlapiLogExecution('DEBUG', 'recordset', recordset);
	nlapiLogExecution('DEBUG', 'mode', mode);

	var listOutput = "";
	try {

		if (recordset != null && recordset.length > 0) {
			if (mode == "List") {
				listOutput = "[";
				for ( var i = 0; i < recordset.length; i++) {
					listOutput = listOutput + "'" + recordset[i] + "'" + ",";
				}
				listOutput = listOutput.substring(0,
						listOutput.length - 1);

				listOutput = listOutput + "]";
			}

		}

	} catch (exception) {
		nlapiLogExecution('DEBUG', 'ProvideInputs', exception);

	}
	nlapiLogExecution('DEBUG', 'listOutput', listOutput);
	nlapiLogExecution('DEBUG', 'ProvideInputs', 'End');
	return listOutput;

}

/**
 * Function to retrieve the SKU details for a list of SKUs
 * @param skulist
 * @returns {Array}
 */
function GetSKUDetails(closedTaskList,skulist){
	nlapiLogExecution('DEBUG', 'GetSKUDetails', 'Start');

	var skuDetailsListT = new Array();
	try{
		// Get the list of distinct SKUs
		var skuDetailsList = new Array();
		//var skulist = getDistinctSKUsFromTaskList(closedTaskList);
		nlapiLogExecution('DEBUG', 'skulist', skulist);
		var filters = new Array();
		var columns = new Array();


		filters.push(new nlobjSearchFilter('internalid', null, 'anyof', skulist));
		// Changed On 30/4/12 by Suman
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		// End of Changes as On 30/4/12

		// Getting sku type is pending
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('salesdescription');
		columns[1] = new nlobjSearchColumn('custitem_item_family');
		columns[2] = new nlobjSearchColumn('custitem_item_group');
		columns[3] = new nlobjSearchColumn('custitem_ebizhazmatclass');
		columns[4] = new nlobjSearchColumn('custitem_ebiztempcntrl');
		columns[5] = new nlobjSearchColumn('itemid');
		columns[6] = new nlobjSearchColumn('vendorname');
		columns[7] = new nlobjSearchColumn('internalid');
		columns[8] = new nlobjSearchColumn('upccode');
		columns[9] = new nlobjSearchColumn('cost');

		//columns[7] = new nlobjSearchColumn('recordtype')

		var ResSKUDetails = nlapiSearchRecord('item', null, filters, columns);

		if(ResSKUDetails != null && ResSKUDetails.length > 0){
			for(var i = 0; i < ResSKUDetails.length; i++ ){
				skuDetailsList = new Array();
				skuDetailsList[0] = ResSKUDetails[i].getValue('itemid');
				//skuDetailsList[i]['recordtype'] = ResSKUDetails[i].getValue('recordtype');
				skuDetailsList[1] = ResSKUDetails[i].getValue('salesdescription');
				skuDetailsList[2] = ResSKUDetails[i].getText('custitem_item_family');
				skuDetailsList[3]= ResSKUDetails[i].getText('custitem_item_group');
				skuDetailsList[4] = ResSKUDetails[i].getText('custitem_ebizhazmatclass');
				skuDetailsList[5] = ResSKUDetails[i].getValue('custitem_ebiztempcntrl');
				skuDetailsList[6] = ResSKUDetails[i].getText('vendorname');
				skuDetailsList[7] = ResSKUDetails[i].getValue('internalid');
				skuDetailsList[8] = ResSKUDetails[i].getValue('upccode');
				skuDetailsList[9] = ResSKUDetails[i].getValue('cost');
				skuDetailsListT.push (skuDetailsList);
			}
		}
	}catch(exception){
		nlapiLogExecution('DEBUG', 'Get SKU Details', exception);
	}

	nlapiLogExecution('DEBUG', 'GetSKUDetails', 'end');
	nlapiLogExecution('DEBUG', 'skuDetailsListT', skuDetailsListT);
	return skuDetailsListT;
}
/*
 * This functon accepts skulist and sku as two parameters and returns all the
 * details of the sku
 */
function GetIndividualSKUInfo(skulist, sku ){

	var IndividualSKUInfoList = new Array();

	var IndividualSKUInfoListT = new Array();
	nlapiLogExecution('DEBUG', 'GetIndividualSKUInfo', 'Start');
	nlapiLogExecution('DEBUG', 'skulist', skulist);
	nlapiLogExecution('DEBUG', 'sku', sku);
	try{

		if (skulist != null && skulist.length >0 && sku != null && sku.length>0){
			for ( var i = 0 ; i < skulist.length ; i++){

				if (skulist[i][7]  == sku )
				{
					IndividualSKUInfoList = new Array();
					//nlapiLogExecution('DEBUG', 'sku matches..internal id is', skulist[i][7]);
					IndividualSKUInfoList[0] = skulist[i][0]; //['itemid'];
					//IndividualSKUInfoList[1] = skulist[i]['recordtype'];
					IndividualSKUInfoList[2] = skulist[i][1];//['salesdescription'];
					IndividualSKUInfoList[3] = skulist[i][2];//['custitem_item_family'];
					IndividualSKUInfoList[4] = skulist[i][3];//['custitem_item_group'];
					IndividualSKUInfoList[5] = skulist[i][4];//['custitem_ebizhazmatclass'];
					IndividualSKUInfoList[6] = skulist[i][5];//['custitem_ebiztempcntrl'];
					IndividualSKUInfoList[7] = skulist[i][6];//['vendorname'];
					IndividualSKUInfoList[8] = skulist[i][7];//['vendorname'];
					IndividualSKUInfoList[9] = skulist[i][8];//['vendorname'];
					IndividualSKUInfoList[10] = skulist[i][9];//['vendorname'];
					IndividualSKUInfoListT.push(IndividualSKUInfoList);
					i = skulist.length; 	
				}

			}
		}


	}
	catch(exception){
		nlapiLogExecution('DEBUG', 'GetIndividualSKUInfo', exception);

	}

	nlapiLogExecution('DEBUG', 'GetIndividualSKUInfo', 'end');
	nlapiLogExecution('DEBUG', 'IndividualSKUInfoListT', IndividualSKUInfoListT);
	return IndividualSKUInfoListT;
}

/*
 * This function accepts containerlist , contlp as parameters and returns CUCC
 * no, weight and cube
 */
function GetIndividualCUCCData(containerlist , ContLP ){


	var IndividualCUCCDetailsListT = new Array();
	nlapiLogExecution('DEBUG', 'GetIndividualCUCCData', 'Start');
	try{

		if (containerlist != null && containerlist.length >0 && ContLP != null && ContLP.length>0){
			for ( var i = 0 ; i < containerlist.length ; i++){

				if (containerlist[i]['ContLP']  == ContLP )
				{
					var IndividualCUCCDetailsList = new Array();
					IndividualCUCCDetailsList[0] = containerlist[i]['ContLP'];
					IndividualCUCCDetailsList[1]= containerlist[i]['CUCC'];
					IndividualCUCCDetailsList[2] = containerlist[i]['ContainerCube'];
					IndividualCUCCDetailsList[3] = containerlist[i]['ContainerWeight'];
					IndividualCUCCDetailsListT.push(IndividualCUCCDetailsList);
					i = containerlist.length; 	
				}

			}
		}


	}
	catch(exception){
		nlapiLogExecution('DEBUG', 'GetIndividualCUCCData', exception);
	}

	nlapiLogExecution('DEBUG', 'GetIndividualCUCCData', 'end');
	return IndividualCUCCDetailsList;
}


/* This function accepts salesorderdetaillist ,salesorder as parameter and returns salesorder details.*/

function GetIndividualOrderInfo(OrderDetailList, OrderNo ){


	var IndividualOrderInfoListT = new Array();
	var IndividualOrderInfoList = new Array();
	nlapiLogExecution('DEBUG', 'GetIndividualOrderInfo', 'Start');
	nlapiLogExecution('DEBUG', 'OrderDetailList', OrderDetailList);
	nlapiLogExecution('DEBUG', 'OrderNo', OrderNo);
	try{

		if (OrderDetailList != null && OrderDetailList.length >0 && OrderNo != null && OrderNo.length>0){
			for ( var i = 0 ; i < OrderDetailList.length ; i++){

				if (OrderDetailList[i][44]  == OrderNo )
				{
					nlapiLogExecution('DEBUG', 'OrderNo matches and order internalid', OrderDetailList[i][44]);				
					IndividualOrderInfoList = new Array();
					IndividualOrderInfoList[0] = OrderDetailList[i][0];//.getValue('tranid');
					IndividualOrderInfoList[1] = OrderDetailList[i][1];//.getValue('custbody_edi_ref_mr');
					IndividualOrderInfoList[2] = OrderDetailList[i][2];//.getValue('custbody_dynacraftvendornumber');
					IndividualOrderInfoList[3] = OrderDetailList[i][3];//.getValue('Shipmethod');
					IndividualOrderInfoList[4] = OrderDetailList[i][4];//.shipmethodi //getValue('custbody_edi_fob');
					IndividualOrderInfoList[5] = OrderDetailList[i][6];//.getValue('shipaddressee');
					IndividualOrderInfoList[6] = OrderDetailList[i][7];//.getValue('shipaddr1');
					IndividualOrderInfoList[7] = OrderDetailList[i][8];//.getValue('shipaddr2');
					IndividualOrderInfoList[8] = OrderDetailList[i][9];//.getValue('shipcity');
					IndividualOrderInfoList[9] = OrderDetailList[i][10];//.getValue('shipstate');
					IndividualOrderInfoList[10] = OrderDetailList[i][11];//.getValue('shipcountry');
					IndividualOrderInfoList[11] = OrderDetailList[i][12];//.getValue('shipzip');
					IndividualOrderInfoList[12] = OrderDetailList[i][13];//.getValue('custbody_nswmspoexpshipdate');
					IndividualOrderInfoList[13] = OrderDetailList[i][14];//.getValue('custbody_nswmsactualarrivaldate');
					IndividualOrderInfoList[14] = OrderDetailList[i][15];//.getValue('custbody_nswmssodestination');
					IndividualOrderInfoList[15] = OrderDetailList[i][16];//.getValue('custbody_nswmssoroute');
					nlapiLogExecution('DEBUG', 'OrderDetailList[i][16]',OrderDetailList[i][16]);
					//IndividualOrderInfoList[16] = OrderDetailList[i].getValue('shipzip');
					IndividualOrderInfoList[17] = OrderDetailList[i][18];//.getValue('class');
					IndividualOrderInfoList[18] = OrderDetailList[i][19];//getValue('entity');
					IndividualOrderInfoList[19] = OrderDetailList[i][20];//.getValue('billaddressee');
					IndividualOrderInfoList[20] = OrderDetailList[i][21];//.getValue('billaddr1');
					IndividualOrderInfoList[21] = OrderDetailList[i][22];//.getValue('billaddr2');
					IndividualOrderInfoList[22] = OrderDetailList[i][23];//.getValue('billaddr3');
					IndividualOrderInfoList[23] = OrderDetailList[i][24];//.getValue('billcity');
					IndividualOrderInfoList[24] = OrderDetailList[i][25];//.getValue('billstate');
					IndividualOrderInfoList[25] = OrderDetailList[i][26];//.getValue('billzip');
					IndividualOrderInfoList[26] = OrderDetailList[i][27];//.getValue('billcountry');
					IndividualOrderInfoList[27] = OrderDetailList[i][28];//.getValue('billphone');
					IndividualOrderInfoList[28] = OrderDetailList[i][29];//.getValue('Location');
					IndividualOrderInfoList[29] = OrderDetailList[i][30];//.getValue('custbody_nswms_company');
					IndividualOrderInfoList[30] = OrderDetailList[i][31];//.getValue('department');
					IndividualOrderInfoList[31] = OrderDetailList[i][32];//.getValue('terms');
					IndividualOrderInfoList[32] = OrderDetailList[i][33];//.getValue('custbody_nswmsordertype');
					IndividualOrderInfoList[33] = OrderDetailList[i][34];//getValue('custbody_nswmspriority');
					IndividualOrderInfoList[34] = OrderDetailList[i][35];//.getValue('otherrefnum');
					IndividualOrderInfoList[35] = OrderDetailList[i][36];//.getValue('shipphone');
					IndividualOrderInfoList[36] = OrderDetailList[i][37];//.getValue('billphone');
					IndividualOrderInfoList[37] = OrderDetailList[i][38];//.getValue('custbody_nswmspriority');
					IndividualOrderInfoList[38] = OrderDetailList[i][39];//.getValue('otherrefnum');
					//IndividualOrderInfoList[39] = OrderDetailList[i][40];//['CarrierOptions'];
					IndividualOrderInfoList[39] = OrderDetailList[i][5];// //getValue('custbody_edi_fob');
					IndividualOrderInfoList[40] = OrderDetailList[i][41];//custbody_locationaddressid
					//IndividualOrderInfoList[41] = OrderDetailList[i][3];//['ShipMethodID'];
					IndividualOrderInfoList[42] = OrderDetailList[i][42];//['ShipPhone'];
					IndividualOrderInfoList[43]= OrderDetailList[i][43];//['PlannedArrivaDateTime'];
					//IndividualOrderInfoList[44]= OrderDetailList[i][44];//['PlannedArrivaDateTime'];
					IndividualOrderInfoList[45]= OrderDetailList[i][45];//['operating unit'];

					IndividualOrderInfoList[46]= OrderDetailList[i][46];
					IndividualOrderInfoList[47]= OrderDetailList[i][47];

					IndividualOrderInfoList[48]= OrderDetailList[i][48];
					IndividualOrderInfoList[49]= OrderDetailList[i][49];
					IndividualOrderInfoList[50]= OrderDetailList[i][50];
					IndividualOrderInfoList[51]= OrderDetailList[i][51];
					IndividualOrderInfoList[52]= OrderDetailList[i][52];
					IndividualOrderInfoList[53]= OrderDetailList[i][53];
					IndividualOrderInfoList[54]= OrderDetailList[i][54];
					IndividualOrderInfoList[55]= OrderDetailList[i][55];
					IndividualOrderInfoListT.push(IndividualOrderInfoList);
					//salesorderDetailList[44] = ResSSODetails[i].getValue('internalid');

				}

			}
		}


	}
	catch(exception){
		nlapiLogExecution('DEBUG', 'GetIndividualOrderInfo', exception);
	}

	nlapiLogExecution('DEBUG', 'GetIndividualOrderInfo', 'end');
	nlapiLogExecution('DEBUG', 'Individual order info-IndividualOrderInfoListT', IndividualOrderInfoListT);
	return IndividualOrderInfoListT;
}

/*
 * This function accepts salesorderdetaillist ,salesorder as parameter and
 * returns salesorder details.
 */

/* This function will accept sales order internal id as parameter and returns all the shipmethod master */

function GetCarrierOptions( SOid ){
	var ShipCarrierOptions="";
	var salesorderRec ="";
	try
	{
		nlapiLogExecution('DEBUG', 'GetCarrierOptions', 'Start');
		nlapiLogExecution('DEBUG', 'GetCarrierOptions-SOid',SOid);
		salesorderRec = nlapiLoadRecord('salesorder', SOid);					
		ShipCarrierOptions = salesorderRec.getField('shipmethod').getSelectOptions(null, null);
		nlapiLogExecution('DEBUG', 'shipoptionlen', ShipCarrierOptions.length);

	}
	catch(exception){
		nlapiLogExecution('DEBUG', 'GetCarrierOptions', exception);

	}

	nlapiLogExecution('DEBUG', 'GetCarrierOptions', 'End');
	//nlapiLogExecution('DEBUG', 'GetCarrierOptions-ShipCarrierOptions', ShipCarrierOptions);
	return ShipCarrierOptions;
}

/*
 * This function accepts salesorderdetaillist ,salesorder as parameter and
 * returns salesorder details.
 */

function GetAlternateIDSforOrder(OrdNo){

	var OrderAlternateIDsList = new Array();
	var OrderAlternateIDsListT = new Array();
	var filters= new Array();
	var columns = new Array();
	nlapiLogExecution('DEBUG', 'GetAlternateIDSforOrder', 'Start');
	try{

		if ( OrdNo != null &&  OrdNo !=""){

			filters.push(new nlobjSearchFilter('internalid', null, 'is', OrdNo));


			columns[0] = new nlobjSearchColumn('line');
			columns[1] = new nlobjSearchColumn('alternateid');

			var ResOrderAlternateDetails = nlapiSearchRecord('salesorder', null, filters,columns);

			if (ResOrderAlternateDetails != null &&  ResOrderAlternateDetails.length > 0 )
			{
				for(var i = 0; i < ResOrderAlternateDetails.length; i++ ){
					OrderAlternateIDsList = new Array();
					OrderAlternateIDsList[0] = ResOrderAlternateDetails[i].getValue('line');
					OrderAlternateIDsList[1] = ResOrderAlternateDetails[i].getValue('alternateid');
					OrderAlternateIDsListT.push(OrderAlternateIDsList);
				}



			}




		}
	}
	catch(exception){
		nlapiLogExecution('DEBUG', 'GetAlternateIDSforOrder', exception);
	}
	nlapiLogExecution('DEBUG', '', 'OrderAlternateIDsListT',OrderAlternateIDsListT);
	nlapiLogExecution('DEBUG', 'GetAlternateIDSforOrder', 'end');

	return OrderAlternateIDsListT;
}



/* This function is to return AlternateId of the salesorder items. Parameters are orderno , line no , item.
 * 
 * 
 * 
 */

/*
 * This function accepts salesorder alternateid item list and lineno returns
 * salesorder details.
 */

function GetAlternateIDforOrderLine(orderinternalid, lineno ){

	var alternateid="";
	nlapiLogExecution('DEBUG', 'GetAlternateIDforOrderLine', 'Start');
	nlapiLogExecution('DEBUG', 'Internal id and lineno are', orderinternalid + ' ' + lineno );

	try{

		var salesorderRec = nlapiLoadRecord('salesorder', orderinternalid);
		alternateid=salesorderRec.getLineItemValue('item','custcol13',lineno);
	}
	catch(exception){
		nlapiLogExecution('DEBUG', 'GetAlternateIDforOrderLine', exception);
	}

	nlapiLogExecution('DEBUG', 'Alternateid', alternateid);
	nlapiLogExecution('DEBUG', 'GetAlternateIDforOrderLine', 'end');

	return alternateid;
}


/*This API will assign all the UOM details for the item 
 * Type : Skulist , sku*/

function AssignUOMDetails ( uomDetailsList , item)
{
	nlapiLogExecution('DEBUG', 'uomDetailsList', uomDetailsList);
	nlapiLogExecution('DEBUG', 'item', item);
	var uomIndividualdetailsT  = new Array();
	var uomCnt = 0;
	try {

		if ( uomDetailsList != null && uomDetailsList.length > 0 ){

			for ( var i = 0 ; i < uomDetailsList.length ; i++ ) {


				var uomDetails = uomDetailsList[i];
				//if  (uomDetailsList[i]['Item'] == item){
				//nlapiLogExecution('DEBUG', 'uomDetails[7]', uomDetails[7]);
				if  (uomDetails[7] == item){
					var uomIndividualdetails  = new Array();

					nlapiLogExecution('DEBUG', 'uomDetails', uomDetails);					

					uomIndividualdetails[0]= uomDetails[0];
					uomIndividualdetails[1]= uomDetails[1];
					uomIndividualdetails[2]= uomDetails[2];
					uomIndividualdetails[3]= uomDetails[3];
					uomIndividualdetails[4]= uomDetails[4];
					uomIndividualdetails[5]= uomDetails[5];
					uomIndividualdetails[6]= uomDetails[6];
					uomIndividualdetails[7]= uomDetails[1];
					uomIndividualdetails[8]= uomDetails[8];

					/*uomDetailsList[0] = ResItemDims[i].getValue('custrecord_ebizuomskudim');
					uomDetailsList[1] = ResItemDims[i].getValue('custrecord_ebizqty');
					uomDetailsList[2] = ResItemDims[i].getValue('custrecord_ebizlength');
					uomDetailsList[3] = ResItemDims[i].getValue('custrecord_ebizwidth');
					uomDetailsList[4] = ResItemDims[i].getValue('custrecord_ebizheight');
					uomDetailsList[5] = ResItemDims[i].getValue('custrecord_ebizweight');
					uomDetailsList[6] = ResItemDims[i].getValue('custrecord_ebizcube');
					uomDetailsList[7] = ResItemDims[i].getValue('custrecord_ebizitemdims');
					uomDetailsList[8] = ResItemDims[i].getValue('custrecord_ebizpackcodeskudim');
					uomDetailsList[9] = ResItemDims[i].getValue('custrecord_ebizuomlevelskudim');*/

					uomIndividualdetailsT.push(uomIndividualdetails);
					nlapiLogExecution('DEBUG', 'uomIndividualdetailsT', uomIndividualdetailsT);
				} 



			} // End for 

		} // If resultset is not null					

	} // end for try		

	catch (exception){
		nalapiLogExecution('DEBUG','AssignUOMDetails', exception);
	}

	nlapiLogExecution ('DEBUG','AssignUOMDetails','End');
	nlapiLogExecution('DEBUG', 'uomIndividualdetailsT Final', uomIndividualdetailsT);
	return uomIndividualdetailsT;
}
/**
 * 
 */
/* This function will return Y , in case system rule defined for 856 Generation*/
function SystemRuleforASNC(siteid, compid)
{
	var ASNCRule = 'N';
	try {
		var filters = new Array();
		if (compid != null && compid != ""){
			filters.push(new nlobjSearchFilter('custrecord_ebizcomp', null, 'anyof', compid));
		}

		if (siteid != null && siteid != ""){
			filters.push(new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', siteid));
		}

		filters.push(new nlobjSearchFilter('name', null, 'is','ASNC_CNF'));
		filters.push(new nlobjSearchFilter('custrecord_ebizdescription', null, 'is','ASN Confirmation Required?'));
		filters.push(new nlobjSearchFilter('custrecord_ebizruleval_refid', null, 'anyof', '1004'));
		filters.push(new nlobjSearchFilter('custrecord_ebizrulevalue', null, 'is', 'Yes'));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizruleval_refid');
		columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');

		var ResASNCRule = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters,columns);

		if (ResASNCRule!= null  && ResASNCRule.length > 0){
			ASNCRule = 'Y'; 
		}
	}	
	catch(exception) {
		nlapiLogExecution('DEBUG', 'Systemrule Fetching-856', exception);
	}

	return ASNCRule;
}
/*
 * This function will take shipLP as parameter and return the result set of all shipped task records.
 *  */
var ResultTaskRecords ;
function GeteBizTaskRecs(siteid, compid,orderno,maxno)
{
	//var ResultTaskRecords = "";
	try {
		nlapiLogExecution ('DEBUG','orderno',orderno);
		var filters = new Array();
		if(maxno!=null && maxno!='' && maxno!='-1')
			filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));
		else
			ResultTaskRecords = new Array();
		//filters.push(new nlobjSearchFilter('custrecord_ebiztask_ship_lp_no', null, 'is', shipLP));
		filters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no', null, 'anyof', orderno));
		filters.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', '3'));// 3=pick
		//filters.push(new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag', null, 'anyof','7'));// 7=STATUS.OUTBOUND.SHIP_UNIT_BUILT
		filters.push(new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag', null, 'anyof','14'));// ship
		filters.push(new nlobjSearchFilter('custrecord_ebiztask_hostid', null, 'isempty'));
		filters.push(new nlobjSearchFilter('mainline','custrecord_ebiztask_ebiz_order_no','is','T'));
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiztask_line_no');
		columns[1] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_sku_no');
		columns[2] = new nlobjSearchColumn('custrecord_ebiztask_sku');
		columns[3] = new nlobjSearchColumn('custrecord_ebiztask_act_qty');
		columns[4] = new nlobjSearchColumn('tranid','custrecord_ebiztask_ebiz_order_no');
		columns[5] = new nlobjSearchColumn('custrecord_ebiztask_uom_level');
		columns[6] = new nlobjSearchColumn('custrecord_ebiztask_uom_id');
		columns[7] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_contlp_no');
		columns[8] = new nlobjSearchColumn('custrecord_ebiztask_serial_no');
		columns[9] = new nlobjSearchColumn('custrecord_ebiztask_act_end_date');
		columns[10] = new nlobjSearchColumn('custrecord_ebiztask_batch_no');
		columns[11] = new nlobjSearchColumn('custrecord_ebiztask_packcode');
		columns[12] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_order_no');
		columns[13] = new nlobjSearchColumn('custrecord_ebiztask_container_size_id');
		columns[14] = new nlobjSearchColumn('id');
		columns[2].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null,filters, columns);

		if( searchresults!=null && searchresults.length>=1000)
		{ 
			for(var s=0;s<searchresults.length;s++)
			{	
				ResultTaskRecords.push(searchresults[s]);
			}
			var maxno=searchresults[searchresults.length-1].getValue('id');
			GeteBizTaskRecs(siteid, compid,orderno,maxno)	
		}
		else
		{
			for(var s=0;searchresults!=null && s<searchresults.length;s++)
			{	
				ResultTaskRecords.push(searchresults[s]);
			} 
		}

		if(ResultTaskRecords !=null && ResultTaskRecords !='')
			nlapiLogExecution('DEBUG', 'ResultTaskRecords length', ResultTaskRecords.length);

	}

	catch(exception) {
		nlapiLogExecution('DEBUG', 'GeteBizTaskRecs', exception);
	}

	return ResultTaskRecords;
}
/*
 * This function will take batchdetailsarry and item as inputs and return batch
 * id as output
 */
function GetIndividualBatchID( BatchDetailsList,item)
{
	var BatchID = "";
	try
	{
		if (BatchDetailsList != null && BatchDetailsList.length>0) {
			for ( var i=0; i < BatchDetailsList.length ; i++){
				if (BatchDetailsList[i]['custrecord_ebizsku'] == item){

					BatchID = BatchDetailsList[i].getValue('custrecord_ebizlotbatch');
					i = BatchDetailsList.length;
				}

			}
		}
	}
	catch(exception) {
		nlapiLogExecution('DEBUG', 'GetIndividiualBatchID', exception);
	}
	return BatchID;	
}

/*
 * This function will take upcdetailslist and item as inputs and return upcCode
 * id as output
 */
function GetIndividiualupcCode ( upcDetailsList,item)
{
	var upcCode = "";
	try
	{
		if (upcDetailsList != null && upcDetailsList.length>0) {
			for ( var i=0; i < upcDetailsList.length ; i++){
				if (upcDetailsList[i]['Item'] == item){

					upcCode = upcDetailsList[i].getValue('BatchID');
					i = upcDetailsList.length;
				}

			}
		}
	}
	catch(exception) {
		nlapiLogExecution('DEBUG', 'GetIndividiualupcCode', exception);
	}
	return upcCode;	
}
/**
 * Method to retrieve the UPC Code for a given SKU
 * 
 * @param siteid
 * @param compid
 * @param batchtxt
 * @returns {String}
 */
function GetupcCode ( skuid){
	var upcCode = "";
	try
	{
		nlapiLogExecution('DEBUG', 'GetupcCode', 'start');
		var filters = new Array();
		//skuid = '[' + skuid + ']';
		nlapiLogExecution('DEBUG', 'skuid', skuid);
		filters.push(new nlobjSearchFilter('custrecord16', null, 'is', 'U'));
		filters.push(new nlobjSearchFilter('custrecord17', null, 'anyof', skuid));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord14');

		var ResupcCodes = nlapiSearchRecord('customrecord265', null, filters, columns);
		if (ResupcCodes != null && ResupcCodes.length > 0) {
			upcCode = ResupcCodes[0].getValue('custrecord14');
		}
	}
	catch(exception) {
		nlapiLogExecution('DEBUG', 'GetupcCode', exception);
	}
	nlapiLogExecution('DEBUG', 'GetupcCode', 'stop');
	return upcCode;	
}
function Getcustomfield (skuid){
	var upcCode = "";
	try
	{
		nlapiLogExecution('DEBUG', 'GetupcCode', 'start');
		var filters = new Array();
		//skuid = '[' + skuid + ']';
		nlapiLogExecution('DEBUG', 'skuid', skuid);
		filters.push(new nlobjSearchFilter('custrecord16', null, 'is', 'E'));
		filters.push(new nlobjSearchFilter('custrecord17', null, 'anyof', skuid));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord14');

		var ResupcCodes = nlapiSearchRecord('customrecord265', null, filters, columns);
		if (ResupcCodes != null && ResupcCodes.length > 0) {
			upcCode = ResupcCodes[0].getValue('custrecord14');
		}
	}
	catch(exception) {
		nlapiLogExecution('DEBUG', 'GetupcCode', exception);
	}
	nlapiLogExecution('DEBUG', 'GetupcCode', 'stop');
	return upcCode;	
}

//Ending
function DateStamp(){
	var now = new Date();
	return ((parseInt(now.getMonth()) + 1) + '/' + (parseInt(now.getDate())) + '/' + now.getFullYear());
}



function getdummylpdetails(containerLP,ItemID,maxno)
{
	var Resdummylpdetails = new Array();
	nlapiLogExecution('DEBUG', 'into getdummylpdetails', 'done');
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_masterlp', null, 'is',containerLP));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_item', null, 'is',ItemID));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	if(maxno!=null && maxno!='' && maxno!='-1')
		filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_masterlp');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_item');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sscc');
	columns[4] = new nlobjSearchColumn('id').setSort();

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp' , null,	filters, columns);	
	if( searchresults!=null && searchresults.length>=1000)
	{ 
		for(var s=0;s<searchresults.length;s++)
		{	
			Resdummylpdetails.push(searchresults[s]);
		}
		var maxno=searchresults[searchresults.length-1].getValue('id');
		getdummylpdetails(containerLP,ItemID,maxno);	
	}
	else
	{
		for(var s=0;searchresults!=null && s<searchresults.length;s++)
		{	
			Resdummylpdetails.push(searchresults[s]);
		} 
	}
	nlapiLogExecution('DEBUG', 'out getdummylpdetails', Resdummylpdetails);
	return Resdummylpdetails;


}
function GetCarServiceDetails(carrier)
{
	try
	{
		nlapiLogExecution('DEBUG', 'Into GetCarServiceDetails ', carrier);	
		var filter=new Array();
		var columns=new Array();
		var CarServiceDetailsListT = new Array();

		//filter.push(new nlobjSearchFilter('custrecord_carrier_id',null,'is',carrier));
		filter.push(new nlobjSearchFilter('custrecord_carrier_nsmethod',null,'anyof',carrier));

		columns.push(new nlobjSearchColumn('custrecord_carrier_id'));
		columns.push(new nlobjSearchColumn('custrecord_carrier_name'));
		columns.push(new nlobjSearchColumn('custrecord_carrier_service_level'));

		var carrierrec=nlapiSearchRecord('customrecord_ebiznet_carrier',null,filter,columns);
		if ( carrierrec !=null && carrierrec.length > 0){
			for ( var i = 0 ; i < carrierrec.length; i++ ){
				var CarServiceDetailsList = new Array();
				CarServiceDetailsList[0] = carrierrec[i].getValue('custrecord_carrier_id');
				CarServiceDetailsList[1] = carrierrec[i].getValue('custrecord_carrier_name');
				CarServiceDetailsList[2] = carrierrec[i].getValue('custrecord_carrier_service_level');				

				CarServiceDetailsListT.push(CarServiceDetailsList);
			}
		}
	}
	catch(exception) {
		nlapiLogExecution('DEBUG', 'Exception in GetCarServiceDetails', exception);
	}
	nlapiLogExecution('DEBUG', 'GetCarServiceDetails', 'end');
	return CarServiceDetailsListT;
}
function AsncConfirmationDropship(type) 
{
	try {
		if ( type == 'scheduled' || type =='userinterface') {

			nlapiLogExecution('DEBUG', 'into quick ship', 'done');
			var trailername="",departDate="";	var ASNCRuleVal = "",Dummylps="";	var SKUList = new Array();	var SKUDetailsList = new Array();	var newRecIdsArray = new Array();
			var containerLengthTotal = 0;	var containerWidthTotal = 0;	var containerHeightTotal = 0;	var containerWeightTotal = 0;var ShipUnitCharges = 0;
			var ShipunitlevelQty = 0;var shipLP="", trailer="",wmsStatusflag="",tasktype="",ContainerSizeId="",uomLevel='',compid="",siteid="",SRRNO='';
			var SalesorderDetails="", MasterBOL="", salesOrderList="", batchDetails="", uomDetails="", skuDetails="", searchrecordClosedTask="", orderList="",shiplpsList='',trailerList='';
			var OrderwithCustomerInfo="",CustomerMasterInfo="",ShippingDetailsforOrder="";var OrdersEligibleForASNGeneration=new Array();  // Added by Mahesh on 072712 to trigger the ASN based on customer master ASN Required?
			var skulist="",prevcompid="",prevsiteid="",UPCDetails="",CustomFieldDetails="";
			var EligibaleTrailersfor856=new Array();var EligibleTrailer='';var EligibaleOrdersfor856=new Array();
			var ebizpro="",ebizseal="",ebizmastershipper="",ebizappointmenttrailer="",ebizexparrivaldate="",scaccodetrlr="";
			var Phone='',Address1='',city='',state='',zip='',boscovsshippper='',Addressee='',QSBOL='';

			//START:
			nlapiLogExecution('DEBUG', 'into quick ship process', 'done');

			var EligibalePacktaskorderList=new Array();
			var PacktaskorderList=new Array();

			shiplpsearchresults=GetOpenTaskRecs('-1');

			nlapiLogExecution('DEBUG', 'shiplpsearchresults', shiplpsearchresults);
			if(shiplpsearchresults !=null && shiplpsearchresults !="")
			{
				nlapiLogExecution('DEBUG', 'shiplpsearchresults length', shiplpsearchresults.length);
				shiplpsList = getDistinctOrdersList(shiplpsearchresults);
				//trailerList = getDistinctTrailers(shiplpsearchresults);	
				//orderList = getDistinctOrders(shiplpsearchresults);	
				//nlapiLogExecution('DEBUG', 'distinct orderlist count', orderList);
				nlapiLogExecution('DEBUG', 'shiplpsList', shiplpsList);
				for (var z = 0; shiplpsList!= null && z < shiplpsList.length; z++) 
				{
					if(shiplpsList[z] != null && shiplpsList[z] !='')
					{
						var PacktaskorderLististfilter = new Array();			

						PacktaskorderLististfilter.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', shiplpsList[z]));			
						//PacktaskorderLististfilter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); 	
						//PacktaskorderLististfilter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'noneof', [26,29]));
						PacktaskorderLististfilter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [14]));
						PacktaskorderLististsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, PacktaskorderLististfilter , null);
						if(PacktaskorderLististsearchresults== null || PacktaskorderLististsearchresults == ""){       

							PacktaskorderList.push(shiplpsList[z]);
						}
					}		
				}		
				nlapiLogExecution('DEBUG', 'PacktaskorderList',PacktaskorderList);
				var lenthVariable='';
//				if(PacktaskorderList.length > 20 )
//				lenthVariable=20;
//				else
				lenthVariable=PacktaskorderList.length;
				//splitting orderno's
				for( lv=0; lv < lenthVariable; lv++)
				{

					if(PacktaskorderList[lv] != null && PacktaskorderList[lv] != '')
						EligibalePacktaskorderList.push(PacktaskorderList[lv])
				}

				//updatePACKStatusInOpenTask(EligibalePacktaskorderList);

				//get distict orders from closed task				
				if(EligibalePacktaskorderList != null)
					nlapiLogExecution('DEBUG', 'EligibalePacktaskorderList',EligibalePacktaskorderList);
				for (var y = 0; EligibalePacktaskorderList!= null && y < EligibalePacktaskorderList.length; y++) 
				{
					var shiplpsListfilter = new Array();
					var shiplpsListcol = new Array();
					shiplpsListfilter.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no', null, 'anyof', EligibalePacktaskorderList[y]));
					shiplpsListfilter.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', [3])); 	//PICK Task	
					shiplpsListfilter.push(new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag', null, 'anyof', [14])); 	//PICK Task	
					shiplpsListfilter.push(new nlobjSearchFilter('custrecord_ebiztask_hostid',  null, 'isempty')); // LOADED						

					shiplpsListcol[0] = new nlobjSearchColumn('internalid','custrecord_ebiztask_ebiz_order_no','group').setSort();

					shiplpsListsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, shiplpsListfilter, shiplpsListcol);

					for (var d = 0; shiplpsListsearchresults!= null && d < shiplpsListsearchresults.length; d++){
						EligibaleOrdersfor856.push(shiplpsListsearchresults[d].getValue('internalid','custrecord_ebiztask_ebiz_order_no','group'));
					}
				}
				nlapiLogExecution('DEBUG', 'EligibaleOrdersfor856', EligibaleOrdersfor856);	
				//EligibaleOrdersfor856.push(3132043);
				if(EligibaleOrdersfor856 != null && EligibaleOrdersfor856 != '')
				{
					CustomerMasterInfo=GetAllCustomers();
					OrderwithCustomerInfo=GetSalesorderDetailsforASNTrigger(EligibaleOrdersfor856,'-1');					
					//to get req so's with flay req=Y					
					var matchFound = false;

					/* The below for loop is used to filter the order numbers which are eligible for ASN Generation */  
					for ( var m = 0; OrderwithCustomerInfo !=null && m < OrderwithCustomerInfo.length; m++) 
					{
						if(OrdersEligibleForASNGeneration.length <= 50)
						{
							for ( var n = 0; CustomerMasterInfo !=null && n < CustomerMasterInfo.length; n++) 
							{

								if (CustomerMasterInfo[n].getValue('internalid') == OrderwithCustomerInfo[m].getValue('entity') && CustomerMasterInfo[n].getValue('custentity_ebiz_asn_required') == 'T')
								{
									OrdersEligibleForASNGeneration.push(OrderwithCustomerInfo[m].getValue('internalid'));
									matchFound = true;
									//nlapiLogExecution('DEBUG', 'matchFound', matchFound);
								}
							}
						}
						else
						{
							break;
						}
					}			
				}
				nlapiLogExecution('DEBUG', 'OrdersEligibleForASNGeneration', OrdersEligibleForASNGeneration);
				if(OrdersEligibleForASNGeneration != null && OrdersEligibleForASNGeneration != '')
					nlapiLogExecution('DEBUG', 'OrdersEligibleForASNGeneration length', OrdersEligibleForASNGeneration.length);
				if(OrdersEligibleForASNGeneration !=null && OrdersEligibleForASNGeneration !="")
				{
					searchrecordClosedTask = GeteBizTaskRecs(siteid, compid,OrdersEligibleForASNGeneration,'-1');

					if(searchrecordClosedTask == null || searchrecordClosedTask == '')
						updateStatusflagForDropshipOrder(OrdersEligibleForASNGeneration);

					if(searchrecordClosedTask != null && searchrecordClosedTask !=""){
						nlapiLogExecution('DEBUG', 'After closed taskrecs - aval Usage', nlapiGetContext().getRemainingUsage());

						if (searchrecordClosedTask != null	&& searchrecordClosedTask != "") 
						{
							logMsg = logMsg + 'Closed Task Count='	+ searchrecordClosedTask.length + '<br>';
						} else {
							logMsg = logMsg + 'Closed Task Count=' + 'Zero' + '<br>';
						}

						// Get the details for the distinct list of SKUs
						skulist = getDistinctSKUsFromTaskList(searchrecordClosedTask);
						skuDetails = GetSKUDetails(searchrecordClosedTask,skulist);
						//UPCDetails = GetAllUPCDetails(searchrecordClosedTask,skulist);
						//CustomFieldDetails = GetAllcustomfieldDetails(searchrecordClosedTask,skulist);

						nlapiLogExecution('DEBUG', 'After SKU details - aval Usage', nlapiGetContext().getRemainingUsage());

						if (skuDetails != null && skuDetails != "") {
							logMsg = logMsg + 'SKU Details Count=' + skuDetails.length	+ '<br>';
						} else {
							logMsg = logMsg + 'SKU Details Count=' + 'Zero' + '<br>';
						}
						// Get the UOM Details for the distinct SKUS

						uomDetails = GetUOMDetails(searchrecordClosedTask,skulist);
						nlapiLogExecution('DEBUG', 'After UOM details - aval Usage', nlapiGetContext().getRemainingUsage());
						nlapiLogExecution('DEBUG', 'Before uom details length', uomDetails);
						if (uomDetails != null && uomDetails != "") {
							logMsg = logMsg + 'UOM Details Count=' + uomDetails.length+ '<br>';
						} else {
							logMsg = logMsg + 'UOM Details Count=' + 'Zero' + '<br>';
						}
						// Get the Batch Details for the distinct skus

						var batchDetails = GetBatchDetails(searchrecordClosedTask,skulist);
						nlapiLogExecution('DEBUG', 'After Batch details - aval Usage', nlapiGetContext().getRemainingUsage());
						if (batchDetails != null && batchDetails != "") {
							logMsg = logMsg + 'Batch Details Count='+ batchDetails.length + '<br>';
						} else {
							logMsg = logMsg + 'Batch Details Count=' + 'Zero' + '<br>';
						}			

						//  to get distinct SO internal Id's
						salesOrderList = getDistinctOrderIDsFromTaskList(searchrecordClosedTask);				
						//to get BOL#

//						MasterBOL=GetMasterBOL(salesOrderList);
//						nlapiLogExecution('DEBUG', 'MasterBOL 0',MasterBOL[0]);
//						nlapiLogExecution('DEBUG', 'MasterBOL 1',MasterBOL[1]);

						// Get the details for the distinct list of orders.
						SalesorderDetails = GetSalesOrderDetails(searchrecordClosedTask,salesOrderList);
						nlapiLogExecution('DEBUG', 'After SO details - aval Usage', nlapiGetContext().getRemainingUsage());
						if (SalesorderDetails != null && SalesorderDetails != "") {
							logMsg = logMsg + 'Salesorder Details Count='+ SalesorderDetails.length + '<br>';				} 
						else {
							logMsg = logMsg + ' Salesorder Details Count=' + 'Zero'+ '<br>';
						}
						//}
						ShippingDetailsforOrder=getShippingDetailsForEligibleOrders(OrdersEligibleForASNGeneration,'');

						//var allalternateids=getallbuyerpartno(OrdersEligibleForASNGeneration);
						var allalternateids=getallbuyerpartno(skulist);
						var allsolineresults=getallSOLinedetails(OrdersEligibleForASNGeneration);	

//						if(trailerList != null)
//						nlapiLogExecution('DEBUG', 'trailerList length',trailerList.length);
//						for (var z = 0; trailerList!= null && z < trailerList.length; z++) 
//						{
//						var trailerListfilter = new Array();
//						var trailerListcol = new Array();
//						trailerListfilter.push(new nlobjSearchFilter('custrecord_ebiz_trailer_no', null, 'is', trailerList[z]));
//						trailerListfilter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); 	//PICK Task			
//						trailerListfilter.push(new nlobjSearchFilter('custrecord_hostid',  null, 'isempty')); // LOADED
//						trailerListcol[0] = new nlobjSearchColumn('custrecord_ebiz_trailer_no');
//						trailerListsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, trailerListfilter, trailerListcol);
//						if(trailerListsearchresults == null || trailerListsearchresults ==""){
//						EligibaleTrailersfor856.push(trailerList[z]);
//						}
//						}
						nlapiLogExecution('DEBUG', 'EligibaleTrailersfor856 length',EligibaleTrailersfor856);
						if(ShippingDetailsforOrder != null)
						{
							nlapiLogExecution('DEBUG', 'ShippingDetailsforOrder', ShippingDetailsforOrder.length);
						}
						var parent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); //create new parent record
						for (var q = 0; ShippingDetailsforOrder!= null && q < ShippingDetailsforOrder.length; q++) 
						{						
							trailer = ShippingDetailsforOrder[q].getValue('custrecord_ebiztask_ebiz_trailer_no',null,'group');

							//if(TrailerEligibiltyCheck(EligibaleTrailersfor856,trailer)){
							shipLP = ShippingDetailsforOrder[q].getValue('custrecord_ebiztask_ship_lp_no',null,'group');						
							wmsStatusflag = ShippingDetailsforOrder[q].getValue('custrecord_ebiztask_wms_status_flag',null,'group');
							//tasktype = ShippingDetailsforOrder[q].getValue('custrecord_ebiztask_tasktype',null,'group');								
							compid = ShippingDetailsforOrder[q].getValue('custrecord_ebiztask_comp_id',null,'group');
							siteid = ShippingDetailsforOrder[q].getValue('custrecord_ebiztask_site_id',null,'group');		
							ebizorderno = ShippingDetailsforOrder[q].getValue('internalid','custrecord_ebiztask_ebiz_order_no','group');
							//ContainerSizeId = ShippingDetailsforOrder[q].getValue('custrecord_ebiztask_container_size_id',null,'group');

							MasterBOL=GetMasterBOL(ebizorderno);
							nlapiLogExecution('DEBUG', 'MasterBOL 0',MasterBOL[0]);
							nlapiLogExecution('DEBUG', 'MasterBOL 1',MasterBOL[1]);					

							if( (trailer == null || trailer =='' || trailer == '- None -'))
							{									
//								var time = new Date();														
//								trailer="TR_"+ time.getHours() + "" + time.getMinutes() + "" + time.getSeconds();
								trailer="TR"+ ebizorderno;
							}
							if( (shipLP == null || shipLP =='' || shipLP == '- None -'))
							{
								//shipLP="SP"+ time.getHours() + "" + time.getMinutes() + "" + time.getSeconds();
								shipLP="SP"+ ebizorderno;
							}

							try {
								//get internal id for task type "PACK" from open task.
								var filter1 = new Array();
								var col1 = new Array();
								col1[0] = new nlobjSearchColumn('custrecord_bol');
								filter1.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [14])); 	
								filter1.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [28])); 
								filter1.push(new nlobjSearchFilter('custrecord_hostid',  null, 'isempty')); 
								filter1.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', ebizorderno));								
								var results1 = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filter1, col1);
								if(results1!= null)
									QSBOL=results1[0].getValue('custrecord_bol');
							}

							catch(exception) 
							{
								nlapiLogExecution('DEBUG', 'exception in get internal id for task type "PACK" from open task', exception);
							}

							//get container sizeid
//							var contsizefilter = new Array();
//							var contsizecol = new Array();
//							contsizefilter.push(new nlobjSearchFilter('custrecord_ship_lp_no', null, 'is', shipLP));			
//							//contsizefilter.push(new nlobjSearchFilter('custrecord_hostid',  null, 'isempty'));
//							contsizecol[0] = new nlobjSearchColumn('custrecord_container');
//							contsizeresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, contsizefilter, contsizecol);
//							if(contsizeresults != null){
//							ContainerSizeId = contsizeresults[0].getValue('custrecord_container');
//							}

							//get SRR 
//							var srrfilter = new Array();
//							var srrcol = new Array();
//							srrfilter.push(new nlobjSearchFilter('custrecord_ship_order', null, 'anyof', ebizorderno));
//							srrcol[0] = new nlobjSearchColumn('custrecord_ebiz_ship_routing_reqno');
//							srrresults = nlapiSearchRecord('customrecord_ship_manifest', null, srrfilter, srrcol);
//							if(srrresults != null){
//							SRRNO = srrresults[0].getValue('custrecord_ebiz_ship_routing_reqno');
//							}
							nlapiLogExecution('DEBUG', 'Current Available Usage', nlapiGetContext().getRemainingUsage());
							// Get system rule for SPS ASN Confirmation
							if(prevcompid == compid && prevsiteid == siteid)
							{//nothing doing

							}
							else
							{
								ASNCRuleVal = SystemRuleforASNC(siteid, compid);
							}
							prevcompid = compid;
							prevsiteid=siteid;
							// Get all dummy lp's againast to Master Lp					
							nlapiLogExecution('DEBUG', 'After system rule - aval Usage', nlapiGetContext().getRemainingUsage());
							nlapiLogExecution('DEBUG', 'ASNCRuleValue', ASNCRuleVal);
							// Hardcoding for Dyna. It should be removed after testing.

							ASNCRuleVal = 'Y';

							var logMsg = 'shipLP=' + shipLP + '<br>';
							logMsg = logMsg + 'Trailer=' + trailer + '<br>';
							logMsg = logMsg + 'WMS Status Flag=' + wmsStatusflag + '<br>';
							logMsg = logMsg + 'Task Type=' + tasktype + '<br>';
							logMsg = logMsg + 'Container SizeID=' + ContainerSizeId + '<br>';
							logMsg = logMsg + 'Comp id=' + compid + '<br>';
							logMsg = logMsg + 'Site ID=' + siteid + '<br>';
							logMsg = logMsg + '856SystemRule=' + ASNCRuleVal;

							nlapiLogExecution('DEBUG', 'Current Available Values', logMsg);								
							// SHIP
							if (wmsStatusflag == 14 &&  ASNCRuleVal == 'Y') {

								nlapiLogExecution('DEBUG', 'All Record sets Lengths', logMsg);

								if (searchrecordClosedTask != null 	&& searchrecordClosedTask.length > 0) {

									// Varaible Declaration
									var lineno ="", skuid ="", skuidText ="", Actqty = "",SOid = "",uomlevel ="", containerLP ="", containerLPtext ="", SerilalId = "",IssueDate = "",IntId = "";
									var BatchIdText = "",packCode="",BatchId = "",uomIdText = "",upcCode="",customField="",IndividualSKUDetailsList ="",SKUDesc ="",SKUFamily ="",SKUGroup = "",SKUtype = "";
									var HazmatCode ="", TempCntrl ="", vendor ="",IndividualOrderInfoList ="",SalesOrdNo = "",ebizOrderNo="",EDIREF = "",ShipmentVendor ="", Shipmethod = "",ShipmethodID ="", SCACCode = "";
									var PrevShipmethod ="",FOB ="",ShipAddressee ="",ShipAddress1 = "",ShipAddress2 = "",Shipcity = "",ShipState = "",ShipCountry = "",Shipzip = "",ShiptoPhone ="";
									var ExpectedShipdate = "",ActualArrivalDate = "",PlannedArrivalDate = "",Destination = "",Route = "",Class = "",ConsigneeId = "",Billaddressee = "",Billaddr1 = "";
									var Billaddr2 = "",Billaddr3 = "",Billcity = "",BillState = "",Billzip = "",Billcountry = "",Billphone = "",location = "",Company = "",Department = "";
									var varTerms = "",CustomerPO = "",Shipphone='',ShiptoPhone = "",Billphone = "",ShipAddrID = "",individualitemdimsList ="",UOM1 = "",UOMQty1 = "",UOMLength1 = "",UOMWidth1 = "";
									var UOMHeight1 = "",UOMWeight1 = "",UOMCube1 = "",Packcode1 = "",UOMlevel1 = "",UOM2 = "",UOMQty2 = "",UOMLength2 = "",UOMWidth2 = "",UOMHeight2 = "",UOMWeight2 = "";
									var UOMCube2 = "",Packcode2 = "",UOMlevel2 = "",UOM3 = "",UOMQty3 = "",UOMLength3 = "",UOMWidth3 = "",UOMHeight3 = "",UOMWeight3 = "",UOMCube3 = "",Packcode3 = "";
									var UOMlevel3 = "",UOM4 = "",UOMQty4 = "",UOMLength4 = "",UOMWidth4 = "",UOMHeight4 = "",UOMWeight4 = "",UOMCube4 = "",Packcode4 = "",UOMlevel4 = "",UOM5 = "",UOMQty5 = "";
									var UOMLength5 = "",UOMWidth5 = "",UOMHeight5 = "",UOMWeight5 = "",UOMCube5 = "",Packcode5 = "",UOMlevel5 = "",CUCC = "",Conainercube = "",ContainerWeight = "",IndividualCUCCDetailsList = "";
									var EUCC = "",IndividualEUCCDetailsList = "",SUCC = "",ShipunitCube = "",ShipunitWeight = "",IndividualSUCCDetailsList = "",ContainerLength = "",ContainerWidth = "",ContainerHeight = "";
									var ContainerMaxWeight = "",ContainerTareWeight = "",ContainerCube = "",IndividualContainerDimslist ="",TrackingNumbers = "",ShippingCharges = "",IndividualShipmanfDetList ="",Pronum = "";
									var SealNo = "",MasterShipper = "",TrailerAppointNo = "",IndividualTrailerDetails = "",prevskuid="",prevcontainer  ="",prevshipLP ="",prevcontainerLP="", previtemId="", newcontlp="",prevtrailer="",PrevCarrier='',Prevsoshipmethod='';
									var PrevOrdNo = "",prevlineno = "",TaskInternalID="",CarrierOptions="",ShiptoEmail ="",ShiptoFax="", ShiptoEmail="",tranid="";
									var ConsigneeId="",salesordWeight="",salesordCube="",Billcity="",BillState="",Billzip="",Billcountry="",Billphone="",location="",locationvalue='',Company="",prevlocation='';
									var Department="",Terms="",OrderType="",OrderPriority="",CustomerPO="",ShipAddrID="",CUCC="",CUCC = "",Conainercube = "",ContainerWeight = "",EUCC="";
									var TrackingNumbers="",ShippingCharges="",SUCC = "",ShipunitCube = "",ShipunitWeight = "",ContainerLength = "",ContainerWidth = "",ContainerHeight = "",ContainerMaxWeight = "",ContainerTareWeight = "",ContainerCube = "";
									var Pronum="",TrailerCarrier='',carrierIdnew='',carrierNamenew='',carrierScacnew='',MasterShipper="",TrailerAppointNo="",SOid="",SOidText='',prevSOid="",soalternateidlist="",alternateid="", ItemID="";					
									var IndividualupcCodeDetailsList="",TrailerDetails='',IndividualcustomFieldDetailsList="",ConsigneeValue='';
									var sysdate=DateStamp();
									nlapiLogExecution('DEBUG', 'Before for loop - Available Usage', nlapiGetContext().getRemainingUsage());
									var stsupdatecnt =0;
									var Department_gs ="";	var srr_gs = "";
									var route_gs = "";	var store_gs = "";	var MarkforStoreID  ="",SCAC='';

									var IndividiualOrdersearchrecordClosedTask=GetIndividiualClosedTaskDetails(searchrecordClosedTask,ebizorderno);
									nlapiLogExecution('DEBUG', 'IndividiualOrdersearchrecordClosedTask', IndividiualOrdersearchrecordClosedTask);
									if(IndividiualOrdersearchrecordClosedTask == null || IndividiualOrdersearchrecordClosedTask =='')
										updateStatusflagForOrder(ebizorderno);
									for ( var count = 0; count < IndividiualOrdersearchrecordClosedTask.length; count++) {


										lineno=IndividiualOrdersearchrecordClosedTask[count][0];
										skuid=IndividiualOrdersearchrecordClosedTask[count][1];
										skuidText=IndividiualOrdersearchrecordClosedTask[count][2];					
										Actqty=IndividiualOrdersearchrecordClosedTask[count][3] ;
										SOid=IndividiualOrdersearchrecordClosedTask[count][4] ;
										uomlevel=IndividiualOrdersearchrecordClosedTask[count][5] ;					
										containerLP=IndividiualOrdersearchrecordClosedTask[count][6];
										containerLPtext=IndividiualOrdersearchrecordClosedTask[count][7] ;
										SerilalId=IndividiualOrdersearchrecordClosedTask[count][8] ;
										IssueDate=IndividiualOrdersearchrecordClosedTask[count][9] ;
										TaskInternalID=IndividiualOrdersearchrecordClosedTask[count][10];
										BatchIdText=IndividiualOrdersearchrecordClosedTask[count][11] ;
										packCode=IndividiualOrdersearchrecordClosedTask[count][12] ;
										SOidText=IndividiualOrdersearchrecordClosedTask[count][13] ;
										ContainerSizeId = IndividiualOrdersearchrecordClosedTask[count][14] ;

										logMsg = 'LineNo=' + lineno + '<br>';
										logMsg = logMsg + 'SKU ID=' + skuid + '<br>';
										logMsg = logMsg + 'skuidText=' + skuidText + '<br>';
										logMsg = logMsg + 'Act Qty=' + Actqty + '<br>';
										logMsg = logMsg + 'SO ID=' + SOid + '<br>';
										logMsg = logMsg + 'SOid Text=' + SOidText + '<br>';
										logMsg = logMsg + 'UOM Level=' + uomlevel + '<br>';
										logMsg = logMsg + 'Container LP=' + containerLP	+ '<br>';
										logMsg = logMsg + 'Batch ID Text=' + BatchIdText;
										nlapiLogExecution('DEBUG', 'Current Record Values',	logMsg);
										logMsg = "";

										// Get Batch ID
										if (BatchIdText != null && BatchIdText != "" &&  BatchIdText !='-' ) {
											BatchId = GetIndividualBatchID(batchDetails, skuid);
										}
										// Get UPC Code
										if ( skuid == prevskuid )
										{	//No need to call upc API. You can use the existing one.
											nlapiLogExecution('DEBUG', 'skuid == prevskuid cond',	skuid);
										}
										else
										{ //Get the upc code1
											nlapiLogExecution('DEBUG', 'skuid != prevskuid cond',	skuid);
											//upcCode = GetupcCode(skuid);
//											IndividualupcCodeDetailsList = GetIndividualUPCInfo(UPCDetails, skuid);
//											if(IndividualupcCodeDetailsList !="")
//											{
//											upcCode=IndividualupcCodeDetailsList[0][0];
//											}
											// get the details for itemmoreinfo custom field value 1
											//customField = Getcustomfield(skuid);
											//IndividualcustomFieldDetailsList = GetIndividualcustomFieldInfo(CustomFieldDetails, skuid);
											//if(IndividualcustomFieldDetailsList !=""){
											//customField=IndividualcustomFieldDetailsList[0][0];
											//}
											nlapiLogExecution('DEBUG', 'upc code',	upcCode);
											nlapiLogExecution('DEBUG', 'customField',customField);

											// The below API will take skudetails , skuid as
											// parameter and
											// returns all the details of the skuid
											nlapiLogExecution('DEBUG', 'skudetails list sending to ind-skufun', skuDetails + '  ' + skuid 	);

											IndividualSKUDetailsList = GetIndividualSKUInfo(skuDetails, skuid);

											if(IndividualSKUDetailsList !=null && IndividualSKUDetailsList != '')
											{
												nlapiLogExecution('DEBUG', 'IndividualSKUDetailsList',	IndividualSKUDetailsList.length);
												ItemID= IndividualSKUDetailsList[0][0]; //SKU  	
												SKUDesc = IndividualSKUDetailsList[0][2];  		//SalesDesc
												SKUFamily = IndividualSKUDetailsList[0][3]; 	// itemFamily
												SKUGroup = IndividualSKUDetailsList[0][4];      //itemGroup
												SKUtype = IndividualSKUDetailsList[0][1];       //skutype
												HazmatCode = IndividualSKUDetailsList[0][5];    //HazmatCls];
												TempCntrl = IndividualSKUDetailsList[0][6];     //TempCntrl;
												vendor = IndividualSKUDetailsList[0][7];		// VendorName
												upcCode=IndividualSKUDetailsList[0][9];
												cost=IndividualSKUDetailsList[0][10];
											}

											// Get SKU dimensions for the sku.
											individualitemdimsList = AssignUOMDetails(uomDetails, skuid);

											UOM1 = "";UOMQty1 = "";UOMLength1 = "";UOMWidth1 = "";
											UOMHeight1 = "";UOMWeight1 = "";UOMCube1 = "";Packcode1 = "";UOMlevel1 = "";UOM2 = "";UOMQty2 = "";UOMLength2 = "";	UOMWidth2 = "";	UOMHeight2 = "";UOMWeight2 = "";
											UOMCube2 = "";	Packcode2 = "";	UOMlevel2 = "";	UOM3 = "";
											UOMQty3 = "";UOMLength3 = "";UOMWidth3 = "";UOMHeight3 = "";UOMWeight3 = "";UOMCube3 = "";
											Packcode3 = "";	UOMlevel3 = "";	UOM4 = "";UOMQty4 = "";	UOMLength4 = "";
											UOMWidth4 = "";UOMHeight4 = "";UOMWeight4 = "";UOMCube4 = "";Packcode4 = "";UOMlevel4 = "";UOM5 = "";UOMQty5 = "";UOMLength5 = "";UOMWidth5 = "";UOMHeight5 = "";UOMWeight5 = "";UOMCube5 = "";Packcode5 = "";UOMlevel5 = "";
											nlapiLogExecution('DEBUG', 'SKU Dim details',individualitemdimsList);

											if (individualitemdimsList != null&& individualitemdimsList.length > 0) {
												nlapiLogExecution('DEBUG', 'individualitemdimsList.length',individualitemdimsList.length);
												if (individualitemdimsList.length == 1) {
													UOM1 = individualitemdimsList[0][0];//['UOMID'];
													UOMQty1 = individualitemdimsList[0][1];//['UOMQty'];
													UOMLength1 = individualitemdimsList[0][2];//['UOMLength'];
													UOMWidth1 = individualitemdimsList[0][3];//['UOMWidth'];
													UOMHeight1 = individualitemdimsList[0][4];//['UOMHeight'];
													UOMWeight1 = individualitemdimsList[0][5];//['UOMWeight'];
													UOMCube1 = individualitemdimsList[0][6];//['UOMCube'];
													Packcode1 = individualitemdimsList[0][9];//['UOMlevel'];
													UOMlevel1 = individualitemdimsList[0][8];//['Packcode'];
												} else if (individualitemdimsList.length == 2) {

													nlapiLogExecution('DEBUG', 'individualitemdimsList[0][0]',individualitemdimsList[0][0]);
													UOM1 = individualitemdimsList[0][0];//['UOMID'];
													nlapiLogExecution('DEBUG', 'UOM1',UOM1);
													UOMQty1 = individualitemdimsList[0][1];//['UOMQty'];
													UOMLength1 = individualitemdimsList[0][2];//['UOMLength'];
													UOMWidth1 = individualitemdimsList[0][3];//['UOMWidth'];
													UOMHeight1 = individualitemdimsList[0][4];//['UOMHeight'];
													UOMWeight1 = individualitemdimsList[0][5];//['UOMWeight'];
													UOMCube1 = individualitemdimsList[0][6];//['UOMCube'];
													Packcode1 = individualitemdimsList[0][9];//['UOMlevel'];
													UOMlevel1 = individualitemdimsList[0][8];//['Packcode'];

													UOM2 = individualitemdimsList[1][0];//['UOMID'];
													UOMQty2 = individualitemdimsList[1][1];//['UOMQty'];
													UOMLength2 = individualitemdimsList[1][2];//['UOMLength'];
													UOMWidth2 = individualitemdimsList[1][3];//['UOMWidth'];
													UOMHeight2 = individualitemdimsList[1][4];//['UOMHeight'];
													UOMWeight2 = individualitemdimsList[1][5];//['UOMWeight'];
													UOMCube2 = individualitemdimsList[1][6];//['UOMCube'];
													Packcode2 = individualitemdimsList[1][9];//['UOMlevel'];
													UOMlevel2 = individualitemdimsList[1][8];//['Packcode'];

												} else if (individualitemdimsList.length == 3) {

													UOM1 = individualitemdimsList[0][0];//['UOMID'];
													UOMQty1 = individualitemdimsList[0][1];//['UOMQty'];
													UOMLength1 = individualitemdimsList[0][2];//['UOMLength'];
													UOMWidth1 = individualitemdimsList[0][3];//['UOMWidth'];
													UOMHeight1 = individualitemdimsList[0][4];//['UOMHeight'];
													UOMWeight1 = individualitemdimsList[0][5];//['UOMWeight'];
													UOMCube1 = individualitemdimsList[0][6];//['UOMCube'];
													Packcode1 = individualitemdimsList[0][9];//['UOMlevel'];
													UOMlevel1 = individualitemdimsList[0][8];//['Packcode'];

													UOM2 = individualitemdimsList[1][0];//['UOMID'];
													UOMQty2 = individualitemdimsList[1][1];//['UOMQty'];
													UOMLength2 = individualitemdimsList[1][2];//['UOMLength'];
													UOMWidth2 = individualitemdimsList[1][3];//['UOMWidth'];
													UOMHeight2 = individualitemdimsList[1][4];//['UOMHeight'];
													UOMWeight2 = individualitemdimsList[1][5];//['UOMWeight'];
													UOMCube2 = individualitemdimsList[1][6];//['UOMCube'];
													Packcode2 = individualitemdimsList[1][9];//['UOMlevel'];
													UOMlevel2 = individualitemdimsList[1][8];//['Packcode'];


													UOM3 = individualitemdimsList[2][0];//['UOMID'];
													UOMQty3 = individualitemdimsList[2][1];//['UOMQty'];
													UOMLength3 = individualitemdimsList[2][2];//['UOMLength'];
													UOMWidth3 = individualitemdimsList[2][3];//['UOMWidth'];
													UOMHeight3 = individualitemdimsList[2][4];//['UOMHeight'];
													UOMWeight3 = individualitemdimsList[2][5];//['UOMWeight'];
													UOMCube3 = individualitemdimsList[2][6];//['UOMCube'];
													Packcode3 = individualitemdimsList[2][9];//['UOMlevel'];
													UOMlevel3 = individualitemdimsList[2][8];//['Packcode'];

												} else if (individualitemdimsList.length == 4) {

													UOM1 = individualitemdimsList[0][0];//['UOMID'];
													UOMQty1 = individualitemdimsList[0][1];//['UOMQty'];
													UOMLength1 = individualitemdimsList[0][2];//['UOMLength'];
													UOMWidth1 = individualitemdimsList[0][3];//['UOMWidth'];
													UOMHeight1 = individualitemdimsList[0][4];//['UOMHeight'];
													UOMWeight1 = individualitemdimsList[0][5];//['UOMWeight'];
													UOMCube1 = individualitemdimsList[0][6];//['UOMCube'];
													Packcode1 = individualitemdimsList[0][9];//['UOMlevel'];
													UOMlevel1 = individualitemdimsList[0][8];//['Packcode'];

													UOM2 = individualitemdimsList[0][0];//['UOMID'];
													UOMQty2 = individualitemdimsList[0][1];//['UOMQty'];
													UOMLength2 = individualitemdimsList[0][2];//['UOMLength'];
													UOMWidth2 = individualitemdimsList[0][3];//['UOMWidth'];
													UOMHeight2 = individualitemdimsList[0][4];//['UOMHeight'];
													UOMWeight2 = individualitemdimsList[0][5];//['UOMWeight'];
													UOMCube2 = individualitemdimsList[0][6];//['UOMCube'];
													Packcode2 = individualitemdimsList[0][9];//['UOMlevel'];
													UOMlevel2 = individualitemdimsList[0][8];//['Packcode'];


													UOM3 = individualitemdimsList[0][0];//['UOMID'];
													UOMQty3 = individualitemdimsList[0][1];//['UOMQty'];
													UOMLength3 = individualitemdimsList[0][2];//['UOMLength'];
													UOMWidth3 = individualitemdimsList[0][3];//['UOMWidth'];
													UOMHeight3 = individualitemdimsList[0][4];//['UOMHeight'];
													UOMWeight3 = individualitemdimsList[0][5];//['UOMWeight'];
													UOMCube3 = individualitemdimsList[0][6];//['UOMCube'];
													Packcode3 = individualitemdimsList[0][9];//['UOMlevel'];
													UOMlevel3 = individualitemdimsList[0][8];//['Packcode'];

													UOM4 = individualitemdimsList[0][0];//['UOMID'];
													UOMQty4 = individualitemdimsList[0][1];//['UOMQty'];
													UOMLength4 = individualitemdimsList[0][2];//['UOMLength'];
													UOMWidth4 = individualitemdimsList[0][3];//['UOMWidth'];
													UOMHeight4 = individualitemdimsList[0][4];//['UOMHeight'];
													UOMWeight4 = individualitemdimsList[0][5];//['UOMWeight'];
													UOMCube4 = individualitemdimsList[0][6];//['UOMCube'];
													Packcode4 = individualitemdimsList[0][9];//['UOMlevel'];
													UOMlevel4 = individualitemdimsList[0][8];//['Packcode'];
												} else if (individualitemdimsList.length == 5) {

													UOM1 = individualitemdimsList[0][0];//['UOMID'];
													UOMQty1 = individualitemdimsList[0][1];//['UOMQty'];
													UOMLength1 = individualitemdimsList[0][2];//['UOMLength'];
													UOMWidth1 = individualitemdimsList[0][3];//['UOMWidth'];
													UOMHeight1 = individualitemdimsList[0][4];//['UOMHeight'];
													UOMWeight1 = individualitemdimsList[0][5];//['UOMWeight'];
													UOMCube1 = individualitemdimsList[0][6];//['UOMCube'];
													Packcode1 = individualitemdimsList[0][9];//['UOMlevel'];
													UOMlevel1 = individualitemdimsList[0][8];//['Packcode'];

													UOM2 = individualitemdimsList[0][0];//['UOMID'];
													UOMQty2 = individualitemdimsList[0][1];//['UOMQty'];
													UOMLength2 = individualitemdimsList[0][2];//['UOMLength'];
													UOMWidth2 = individualitemdimsList[0][3];//['UOMWidth'];
													UOMHeight2 = individualitemdimsList[0][4];//['UOMHeight'];
													UOMWeight2 = individualitemdimsList[0][5];//['UOMWeight'];
													UOMCube2 = individualitemdimsList[0][6];//['UOMCube'];
													Packcode2 = individualitemdimsList[0][9];//['UOMlevel'];
													UOMlevel2 = individualitemdimsList[0][8];//['Packcode'];


													UOM3 = individualitemdimsList[0][0];//['UOMID'];
													UOMQty3 = individualitemdimsList[0][1];//['UOMQty'];
													UOMLength3 = individualitemdimsList[0][2];//['UOMLength'];
													UOMWidth3 = individualitemdimsList[0][3];//['UOMWidth'];
													UOMHeight3 = individualitemdimsList[0][4];//['UOMHeight'];
													UOMWeight3 = individualitemdimsList[0][5];//['UOMWeight'];
													UOMCube3 = individualitemdimsList[0][6];//['UOMCube'];
													Packcode3 = individualitemdimsList[0][9];//['UOMlevel'];
													UOMlevel3 = individualitemdimsList[0][8];//['Packcode'];

													UOM4 = individualitemdimsList[0][0];//['UOMID'];
													UOMQty4 = individualitemdimsList[0][1];//['UOMQty'];
													UOMLength4 = individualitemdimsList[0][2];//['UOMLength'];
													UOMWidth4 = individualitemdimsList[0][3];//['UOMWidth'];
													UOMHeight4 = individualitemdimsList[0][4];//['UOMHeight'];
													UOMWeight4 = individualitemdimsList[0][5];//['UOMWeight'];
													UOMCube4 = individualitemdimsList[0][6];//['UOMCube'];
													Packcode4 = individualitemdimsList[0][9];//['UOMlevel'];
													UOMlevel4 = individualitemdimsList[0][8];//['Packcode'];

													UOM5 = individualitemdimsList[0][0];//['UOMID'];
													UOMQty5 = individualitemdimsList[0][1];//['UOMQty'];
													UOMLength5 = individualitemdimsList[0][2];//['UOMLength'];
													UOMWidth5 = individualitemdimsList[0][3];//['UOMWidth'];
													UOMHeight5 = individualitemdimsList[0][4];//['UOMHeight'];
													UOMWeight5 = individualitemdimsList[0][5];//['UOMWeight'];
													UOMCube5 = individualitemdimsList[0][6];//['UOMCube'];
													Packcode5 = individualitemdimsList[0][9];//['UOMlevel'];
													UOMlevel5 = individualitemdimsList[0][8];//['Packcode'];
												}
											}
										}
										prevskuid = skuid;					

										logMsg = 'SKU=' + skuid + '<br>';
										logMsg = logMsg + 'SKU Desc=' + SKUDesc + '<br>';
										logMsg = logMsg + 'SKU Family=' + SKUFamily + '<br>';
										logMsg = logMsg + 'SKU Group=' + SKUGroup + '<br>';
										logMsg = logMsg + 'SKU Type=' + SKUtype + '<br>';
										logMsg = logMsg + 'HazmatCode=' + HazmatCode + '<br>';
										logMsg = logMsg + 'Temp Cntrl=' + TempCntrl + '<br>';
										logMsg = logMsg + 'Vendor=' + vendor + '<br>';
										logMsg = logMsg + 'UPC Code=' + upcCode + '<br>';
										logMsg = logMsg + 'Batch ID=' + BatchId + '<br>';

										nlapiLogExecution('DEBUG', 'Current SKU Record Values',logMsg);
										if(SOid == prevSOid){
											//No need to call upc API. You can use the existing one.
											nlapiLogExecution('DEBUG', 'SOid == prevSOid cond',	SOid);
										}
										else
										{
											IndividualOrderInfoList = GetIndividualOrderInfo(SalesorderDetails, SOid);
											//nlapiLogExecution('DEBUG', 'Order info list-IndividualOrderInfoList',IndividualOrderInfoList);
											if(IndividualOrderInfoList !=null && IndividualOrderInfoList !=''){
												ebizOrderNo=SOid;
												SalesOrdNo = IndividualOrderInfoList[0][0]; //tranID
												EDIREF = IndividualOrderInfoList[0][1];   //EDIRefFld;
												ShipmentVendor = IndividualOrderInfoList[0][2]; // ShipmentVendor;
												Shipmethod = IndividualOrderInfoList[0][3];   // Shipmethod
												ShipmethodID = IndividualOrderInfoList[0][4]; // ShipmethodID'
												SCACCode=Shipmethod;
												operatingUnit= IndividualOrderInfoList[0][45];//['operating unit'];
												nlapiLogExecution('DEBUG', 'Shipment vendor',ShipmentVendor);
												nlapiLogExecution('DEBUG', 'operatingUnit',operatingUnit);

												salesordWeight= IndividualOrderInfoList[0][46];//['operating unit'];
												salesordCube= IndividualOrderInfoList[0][47];//['operating unit'];						
												nlapiLogExecution('DEBUG', 'salesordWeight',salesordWeight);
												nlapiLogExecution('DEBUG', 'salesordCube',salesordCube);

												tranid = IndividualOrderInfoList[0][0];
												FOB = IndividualOrderInfoList[0][39];  // FOB
												ShipAddressee = IndividualOrderInfoList[0][5]; //ShipAddressee
												ShipAddress1 = IndividualOrderInfoList[0][6];// ShipAddr1
												ShipAddress2 = IndividualOrderInfoList[0][7];// [ShipAddr2]
												Shipcity = IndividualOrderInfoList[0][8];// ['ShipCity'];
												ShipState = IndividualOrderInfoList[0][9];//['ShipState'];
												ShipCountry = IndividualOrderInfoList[0][10];//['Shipcountry'];
												Shipzip = IndividualOrderInfoList[0][11]; //['Shipzip'];
												ShiptoPhone = IndividualOrderInfoList[0][42]; //['ShiptoPhone'];
												ExpectedShipdate = IndividualOrderInfoList[0][12];//['ExpectedShipDate'];
												ActualArrivalDate = IndividualOrderInfoList[0][13];//['ActualArrivalDate'];

												PlannedArrivalDate = IndividualOrderInfoList[0][43];//'PlannedArrivaDateTime'];
												nlapiLogExecution('DEBUG', 'Planeed Arrival date and time',PlannedArrivalDate);

												Destination = IndividualOrderInfoList[0][14];//['Destination'];
												Route = IndividualOrderInfoList[0][50];//['Route'];
												nlapiLogExecution('DEBUG', '1',Route);
												Class = IndividualOrderInfoList[0][17];//['class'];
												ConsigneeId = IndividualOrderInfoList[0][18]; //['ConsigneeId'];
												Billaddressee = IndividualOrderInfoList[0][19];//['BillAddressee'];
												Billaddr1 = IndividualOrderInfoList[0][20]; //['Billaddr1'];
												Billaddr2 = IndividualOrderInfoList[0][21];//['Billaddr2'];
												Billaddr3 = IndividualOrderInfoList[0][22];//['Billaddr3'];
												Billcity = IndividualOrderInfoList[0][23];//['BillCity'];
												BillState = IndividualOrderInfoList[0][24];//['BillState'];
												Billzip = IndividualOrderInfoList[0][25];//['Billzip'];
												Billcountry = IndividualOrderInfoList[0][26];//['BillCountry'];
												Billphone = IndividualOrderInfoList[0][27];//['BillPhone'];
												location = IndividualOrderInfoList[0][28];//['Location'];
												Company = IndividualOrderInfoList[0][29]; //['Company'];
												Department = IndividualOrderInfoList[0][30];// ['Department'];
												Terms = IndividualOrderInfoList[0][31]; //['Terms'];
												OrderType = IndividualOrderInfoList[0][32]; //['OrderType'];
												OrderPriority = IndividualOrderInfoList[0][33]; //['OrderPriority'];
												CustomerPO = IndividualOrderInfoList[0][34];//['CustomerPO'];
												Shipphone = IndividualOrderInfoList[0][35];//['ShipPhone'];
												//Billphone = IndividualOrderInfoList[0][37];//['BillPhone'];
												ShipAddrID = IndividualOrderInfoList[0][40];//['ShipAddrID'];

												Department_gs = IndividualOrderInfoList[0][48]; 
												srr_gs = IndividualOrderInfoList[0][49]; 
												route_gs = IndividualOrderInfoList[0][50]; 
												store_gs = IndividualOrderInfoList[0][51]; 
												MarkforStoreID  = IndividualOrderInfoList[0][52]; 
												SCAC  = IndividualOrderInfoList[0][53]; 
												locationvalue= IndividualOrderInfoList[0][54]; 
												ConsigneeValue = IndividualOrderInfoList[0][55]; 
											}	
										}
										prevSOid=SOid;

										if(locationvalue == prevlocation)
										{//No need to get the details, use the existing details
										}
										else
										{
											if( locationvalue != null && locationvalue !='' )
											{
												nlapiLogExecution('DEBUG', 'locationvalue',locationvalue);
												var shippingfromres = nlapiLoadRecord('location', locationvalue);	
												Addressee=	 shippingfromres.getFieldValue('addressee');
												Phone= shippingfromres.getFieldValue('addrphone');
												Address1= shippingfromres.getFieldValue('addr1');
												city= shippingfromres.getFieldValue('city');
												state= shippingfromres.getFieldValue('state');
												zip= shippingfromres.getFieldValue('zip');
												//boscovsshippper= shippingfromres.getFieldValue('custrecord_boscovs_shippper_num');
											}
										}
										prevlocation = locationvalue;
										// Getting CUCC label data.
										if ( containerLP == prevcontainerLP){
											//No need to get the details, use the existing details
										}
										else{
											// Getting CUCC 
											IndividualCUCCDetailsList = GetContainerCUCCDetails(searchrecordClosedTask, containerLP);
											nlapiLogExecution('DEBUG', 'Getting IndividualCUCCDetailsList',IndividualCUCCDetailsList);
											if (IndividualCUCCDetailsList != null && IndividualCUCCDetailsList.length > 0) {
												CUCC = IndividualCUCCDetailsList[0][1]; //['CUCC'];
												Conainercube = IndividualCUCCDetailsList[0][2];//['ContainerCube'];
												ContainerWeight = IndividualCUCCDetailsList[0][3]; //['ContainerWeight'];
											}
											//Getting Shipmanifest Details
											//GetIndividualShipmanifestDetails
											IndividualShipmanfDetList = GetShipmanifestDetails("", containerLP);
											if (IndividualShipmanfDetList != null && IndividualShipmanfDetList.length > 0) {
												TrackingNumbers = IndividualShipmanfDetList[0][1];//['TrackingNumbers'];
												ShippingCharges = IndividualShipmanfDetList[0][2];//['ShippingCharges'];

												if (ShippingCharges == null	 || ShippingCharges == "" || ShippingCharges == "0.0")
													ShippingCharges = 0;
											}

										} // end if -contlp
										prevcontainerLP = containerLP;
										// Getting SUCC Details for shipLP
										if ( shipLP == prevshipLP)
										{	//No need of calling API and use existing values.
										}
										else
										{
											IndividualSUCCDetailsList = GetContainerSUCCDetails(shipLP);

											if (IndividualSUCCDetailsList != null
													&& IndividualSUCCDetailsList.length > 0) {

												SUCC = IndividualSUCCDetailsList[0][1]; //['SUCC'];
												ShipunitCube = IndividualSUCCDetailsList[0][2]; //['Shipunitcube'];
												ShipunitWeight = IndividualSUCCDetailsList[0][3]; //['ShipunitWeight'];
											}
										}
										prevshipLP=shipLP;
										// Getting Container Dims
										nlapiLogExecution('DEBUG', 'ContainerSizeId' , ContainerSizeId);
										nlapiLogExecution('DEBUG', 'prevcontainer' , prevcontainer);
										if(ContainerSizeId == '110' || ContainerSizeId == '7')
										{
											IndividualContainerDimslist=GetSHIPASISContainerDims(uomlevel,skuid)
											if (IndividualContainerDimslist != null && IndividualContainerDimslist.length > 0) {
												ContainerLength = IndividualContainerDimslist[0][0];//['ContainerLength'];
												ContainerWidth = IndividualContainerDimslist[0][1];//['ContainerWidth'];
												ContainerHeight = IndividualContainerDimslist[0][2];//['ContainerHeight'];
												ContainerMaxWeight = IndividualContainerDimslist[0][3];//['ContainerMaxWeight'];
												ContainerCube = IndividualContainerDimslist[0][4];//['Containercube'];

											}
										}
										else if ( ContainerSizeId == prevcontainer)
										{// no need to call API.
										}
										else
										{
											if(ContainerSizeId != null && ContainerSizeId !=''){
												IndividualContainerDimslist=  GetContainerDims(ContainerSizeId);
												nlapiLogExecution('DEBUG', 'Getindividualcondims-IndividualContainerDimslist',IndividualContainerDimslist);
												if (IndividualContainerDimslist != null && IndividualContainerDimslist.length > 0) {
													ContainerLength = IndividualContainerDimslist[0][0];//['ContainerLength'];
													ContainerWidth = IndividualContainerDimslist[0][1];//['ContainerWidth'];
													ContainerHeight = IndividualContainerDimslist[0][2];//['ContainerHeight'];
													ContainerMaxWeight = IndividualContainerDimslist[0][3];//['ContainerMaxWeight'];
													ContainerTareWeight = IndividualContainerDimslist[0][4];//['ContainerTareWeight'];
													ContainerCube = IndividualContainerDimslist[0][5];//['ContainerCube'];
												}
											}
										}
										prevcontainer = ContainerSizeId;

										// Getting Trailer Details
										if(prevtrailer == trailer)
										{// no need to call API.
										}
										else
										{
											TrailerDetails = GetTrailerDetails(trailer);

											if (TrailerDetails != null	&& TrailerDetails.length > 0) {

												Pronum = TrailerDetails[0][0];//['ProNo'];
												SealNo = TrailerDetails[0][1];//['Seal'];
												MasterShipper = TrailerDetails[0][2];//['MasterShipper'];
												TrailerAppointNo =TrailerDetails[0][3];//['TrailerAppointNo'];
												TrailerCarrier =TrailerDetails[0][5];//['TrailerAppointNo'];


												if ( PlannedArrivalDate == null || PlannedArrivalDate == "")
												{
													PlannedArrivalDate = TrailerDetails[0][4]; // If sales order planned arrival date is null get the value from trailer exp arrival date
												}	
											}
										}
										prevtrailer = trailer;
										if(TrailerCarrier !=null &&  TrailerCarrier !='')
										{
											if(PrevCarrier == TrailerCarrier )
											{
												// no need to call API.
											}
											else
											{
												CarServiceDetails = GetCarServiceDetails(TrailerCarrier);

												if (CarServiceDetails != null	&& CarServiceDetails.length > 0) {
													carrierIdnew = CarServiceDetails[0][0];
													carrierNamenew = CarServiceDetails[0][1];
													carrierScacnew = CarServiceDetails[0][2];
												}
											}
											PrevCarrier = TrailerCarrier;
										}
										else
										{
											nlapiLogExecution('DEBUG', 'into ShipmethodID', ShipmethodID);
											if(ShipmethodID !=null &&  ShipmethodID !='')
											{
												if(Prevsoshipmethod == ShipmethodID )
												{
													// no need to call API.
												}
												else
												{
													CarServiceDetails = GetCarServiceDetails(ShipmethodID);

													if (CarServiceDetails != null	&& CarServiceDetails.length > 0) {
														carrierIdnew = CarServiceDetails[0][0];
														carrierNamenew = CarServiceDetails[0][1];
														carrierScacnew = CarServiceDetails[0][2];
													}
												}
												Prevsoshipmethod = ShipmethodID;
											}
										}								
										IndividualSOLinedetails=GetIndividualSOLinedetails(allsolineresults,lineno,SOid);
										if (IndividualSOLinedetails != null && IndividualSOLinedetails.length > 0) {											 
											deliverylineprice = IndividualSOLinedetails[0][0]; 
											deliverylinecost= IndividualSOLinedetails[0][1]; 
											deliverylineqty = IndividualSOLinedetails[0][2];											
//											if(newcontlp == containerLP && ItemID == previtemId)
//											{
//											nlapiLogExecution('DEBUG', 'into if', 'done');
//											nlapiSubmitField('customrecord_ebiznet_trn_ebiztask',TaskInternalID, 'custrecord_ebiztask_hostid', 'SPS');
//											}
//											else {
//											var Resdummylpdetails = getdummylpdetails(containerLP,ItemID,'-1');

//											if(  Resdummylpdetails !=null){
//											nlapiLogExecution('DEBUG', 'Resdummylpdetails.length', Resdummylpdetails.length);}
//											if(OrderType == 'Standard' && operatingUnit == 'Finished Goods' ){

//											for ( var i = 0 ; Resdummylpdetails !=null && i < Resdummylpdetails.length; i++ )
//											{
//											UCC = Resdummylpdetails[i].getValue('custrecord_ebiz_lpmaster_sscc');							
//											LP = Resdummylpdetails[i].getValue('custrecord_ebiz_lpmaster_lp');

											//var createRec = nlapiCreateRecord('customrecord_ebiznetinterfaceasnc');

											parent.selectNewLineItem('recmachcustrecord_ebiz_stagetable_parent');	

											//Case Start 201414406 

											var EDICustRequired = IsEDICustRequired();
											nlapiLogExecution('ERROR', 'EDICustRequired', EDICustRequired);
											if(EDICustRequired == 'Y')
											{
												var ressoDetails = GetEDIconfig(SOid,lineno);
												for ( var w = 0 ; ressoDetails !=null && w < ressoDetails.length; w++ )
												{
													var columns= ressoDetails[w][0].getAllColumns();
													var columnLen = columns.length;
													for (c = 0; c < columnLen; c++)
													{

														var value = ressoDetails[w][0].getValue(columns[c]);
														nlapiLogExecution('ERROR', 'value PC', value);
														nlapiLogExecution('ERROR', 'ressoDetails[w][2]', ressoDetails[w][2]);
														if(ressoDetails[w][2] == 'PC')
														{
															//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent',ressoDetails[w][1], value);
															if( (ressoDetails[w][3] != null && ressoDetails[w][3] != '') && (ressoDetails[w][3] == ConsigneeId) )
															{	
																nlapiLogExecution('ERROR', 'into customer specific', 'done');
																parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent',ressoDetails[w][1], value);
															}
															else
															{
																nlapiLogExecution('ERROR', 'into not customer specific', ressoDetails[w][1]);
																parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent',ressoDetails[w][1], value);
																//parent.commitLineItem('recmachcustrecord_ebiz_stagetable_parent');
															}
														}
														//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent',ressoDetails[w][1], value);
													}

												}
												//line level
												var ressolineDetails = GetEDIconfigLinelevel(SOid,lineno);
												for ( var w = 0 ; ressolineDetails !=null && w < ressolineDetails.length; w++ )
												{
													var resval = ressolineDetails[w][0];
													if(ressolineDetails[w][2] == 'PC')
													{
														if( (ressoDetails[w][3] != null && ressoDetails[w][3] != '') && (ressoDetails[w][3] == ConsigneeId) )
														{	
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent',ressolineDetails[w][1], resval);
														}
														else
															parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent',ressolineDetails[w][1], resval);
													}
												}

												var resDefaultvalues = GetEDIconfigDefaultvalues(SOid,lineno);
												for ( var w = 0 ; resDefaultvalues !=null && w < resDefaultvalues.length; w++ )
												{
													var resdefaulval = resDefaultvalues[w][0];

													parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent',resDefaultvalues[w][1], resdefaulval);
												}
											}

											//Case End 201414406 

											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_ebizordno', ebizOrderNo);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_delivery_item_custfield0', customField);
											//parent.setCurrentLineItemValue('custrecord_ebiz_asnc_packcode', packCode);

											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_delivery_custfield_000',EDIREF);

											//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_scac_code',SCACCode);
											//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_scac_code',scaccodetrlr);															
											//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_scac_code',SCAC);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_scac_code',carrierScacnew);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shipunitdimsuom', 'Inches');
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_carriermode','LTL');
											//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_carrierservicelevel',Shipmethod);
											//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_carrierservicelevel',TrailerCarrier);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_carrierservicelevel',carrierScacnew);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_fobpaycode',FOB);
											//nlapiLogExecution('DEBUG', ' before consignee Route', Route);
											//nlapiLogExecution('DEBUG', ' ConsigneeId', ConsigneeId);

											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_pickauth_no',Route);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_containerdimsuom', 'Inches');
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_skudimsuom','Inches');
											if (vendor == null || vendor == "") {
												vendor = ItemID;
											}
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_skuvendor',vendor);
											//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_ship_vendor',ShipmentVendor);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_ship_vendor',boscovsshippper);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_delivery_uom','EACH');
											// Added by Sudheer to populate all sku dims cubes
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_cube',UOMCube1);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_cube2',UOMCube2);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_cube3',UOMCube3);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_cube4',UOMCube4);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_cube5',UOMCube5);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_cont_cube',ContainerCube);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sealno', SealNo);
											//parent.setCurrentLineItemValue('custrecord_ebiz_bol', MasterBOL[0]);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_bol', QSBOL);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_waybill_ref',QSBOL);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_temp_cntrl',TempCntrl);
											if (ShipAddressee == "" || ShipAddressee == null)
												ShipAddressee = ConsigneeId;
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shipto_contact',ShipAddressee);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_markfor', 'N');
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','name', tranid);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_contwght_lbs', ContainerWeight);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shipunitwght_lbs',	ShipunitWeight);
											//commented by mahesh as per sravan sir request
											//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shipunitwght_lbs',	salesordWeight);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiztrailerno',trailer);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebizdepartment',Department);
											//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebizdepartment',Department_gs);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_class', Class);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebizshipunitid',shipLP);
//											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_carrierid',Shipmethod);
//											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_carrier_name',Shipmethod);
											//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_carrierid',TrailerCarrier);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_carrierid',carrierIdnew);
											//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_carrier_name',TrailerCarrier);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_carrier_name',carrierNamenew);
											if (Route == "" || Route == null)
												Route = Destination;
											// custbody_locationaddressid
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_route', Route);
											//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_route', srr_gs);
											//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_route', SRRNO);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_dest',Destination);
											if(Pronum == null || Pronum == "")
												Pronum=MasterBOL[1];
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_pronum',Pronum);
											//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiploc_id',ShipAddrID);
											//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiploc_id',route_gs);
											//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiploc_id',store_gs);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiploc_id',MarkforStoreID );
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptoadd1',ShipAddress1);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptoadd2',ShipAddress2);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptocity',Shipcity);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptostate',ShipState);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptozip',Shipzip);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptocountry', ShipCountry);
											//parent.setCurrentLineItemValue('custrecord_ebiz_shiptophone',ShiptoPhone);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptophone',Shipphone);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptoemail',ShiptoEmail); // *********
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shiptofax',ShiptoFax); 							
											if(ActualArrivalDate =="" || ActualArrivalDate == null) // Added by Mahesh
												ActualArrivalDate=sysdate;
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_expected_arrivaldate',ActualArrivalDate);

											if(PlannedArrivalDate =="" || PlannedArrivalDate == null) // Added by Sudheer
												PlannedArrivalDate=sysdate;
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_planned_arrival_datetime',PlannedArrivalDate);
											//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_containerlp',LP);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_containerlp',containerLP);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_containerlp_charges',ShippingCharges);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_container_trackingno',TrackingNumbers);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_issue_period',IssueDate);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_payment_terms', Terms);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_container_length',ContainerLength);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_container_width',ContainerWidth);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_container_height',ContainerHeight);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_container_weight',ContainerMaxWeight);
											//nlapiLogExecution('DEBUG', 'tranid before insert',tranid);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_deliveryid',tranid);
											//nlapiLogExecution('DEBUG', 'Order type is  ', OrderType);		
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_order_type',OrderType);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_order_priority',OrderPriority);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_consigneeid',ConsigneeId);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billinginfo_locationid',Billaddr3);
											if (Billaddressee == "" || Billaddressee == null)
												Billaddressee = ConsigneeId;
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billto_contactname',Billaddressee);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billto_addr1',Billaddr1);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billto_addr2',Billaddr2);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billtocity',Billcity);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billtostate',BillState);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billtozip',Billzip);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billtocountry', Billcountry);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billtophone',Billphone);
											nlapiLogExecution('DEBUG', 'ShiptoEmail',ShiptoEmail);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billtoemail',ShiptoEmail); // ******************
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_billtofax',ShiptoFax);// *******************
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_customer_pono', CustomerPO);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_host_ordno',SOid);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_deliveryline_no', lineno);
											//nlapiLogExecution('DEBUG', 'skuid internal ', skuid);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_item_internalid', skuid);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_asnc_sku', ItemID);
											//nlapiLogExecution('DEBUG', 'skuid internal ', ItemID);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_asnc_sku_desc', SKUDesc);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_asnc_sku_family', SKUFamily);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_asnc_sku_group', SKUGroup);
											if(SKUtype == 'undefined' || SKUtype== "" || SKUtype == null)
												SKUtype="";
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_asnc_sku_type', SKUtype);

											alternateid=GetIndividualabuyerpart(allalternateids,skuid);										
											nlapiLogExecution('DEBUG', 'alternateid after','done');
											prevlineno = lineno;
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_buyer_partnumber',	alternateid); 
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_upc_code',upcCode);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_hazmat_code',HazmatCode);
											if (BatchId != null && BatchId != "")
											{
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_batch_id',BatchId);
												parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_batch_delivery_qty', Actqty);
											}
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_serial_id',SerilalId);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_quantity',Actqty);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_id', UOM1);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_qty',UOMQty1);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_length',UOMLength1);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_width',UOMWidth1);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_weight',UOMWeight1);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_height',UOMHeight1);

											if ( packCode == null || packCode =="")  // If packcode is null , defaulting to 1.
												packCode="1";
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_asnc_packcode', packCode);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_id2', UOM2);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_qty2',UOMQty2);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_length2',UOMLength2);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_width2',UOMWidth2);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_wght2',UOMWeight2);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_hght2',UOMHeight2);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_id3', UOM3);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_qty3',UOMQty3);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_length3',UOMLength3);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_width3',UOMWidth3);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_weight3',UOMWeight3);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_height3',UOMHeight3);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_id4', UOM4);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_qty4',UOMQty4);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_length4',UOMLength4);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_width4',UOMWidth4);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_weight4',UOMWeight4);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_height4',UOMHeight4);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_id5', UOM5);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_uom_qty5',UOMQty5);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_length5',UOMLength5);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_width5',UOMWidth5);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_weight5',UOMWeight5);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_sku_height5',UOMHeight5);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_wh_location',location);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_asnc_company',Company);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_container_levelqty', Actqty);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_s_ucc128',SUCC);

											if (EUCC != null && EUCC != "")
												EUCC = EUCC.substring(0, EUCC.length - 1);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_c_ucc128',CUCC);								
											//parent.setCurrentLineItemValue('custrecord_ebiz_eucc128', EUCC);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_eucc128', EUCC);

											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiznetshipunitlgth',ContainerLength);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shipunit_width',ContainerWidth);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shipunit_height',ContainerHeight);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_shipunit_weight',ShipunitWeight);
											//parent.setCurrentLineItemValue('custrecord_ebiz_ship_cube',ShipunitCube);
											//commented by mahesh as per sravan sir request
											//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_ship_cube',salesordCube);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_ship_cube',ShipunitCube);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipunitcharges',ShipUnitCharges);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipunit_levelqty',Actqty);

											//newly added

											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_itemunitprice',cost);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_itemunitcost',cost);

											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_casepack',UOMQty2);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_deliverylineqty',Actqty);//deliverylineqty
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_deliverylineprice',deliverylineprice);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_deliverylinecost',deliverylinecost);

											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipfromlocationid',location);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipfromcontactname',Addressee);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipfromaddr1',Address1);
//											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipfromaddr2',getallVendornoRes[0][2]);
//											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipfromaddr3',getallVendornoRes[0][3]);															
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipfromcity',city);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipfromstate',state);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipfromzip',zip);
											//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipfromcountry',getallVendornoRes[0][0]);
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipfromphone',Phone);
											//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipfromemail',getallVendornoRes[0][0]);
											//parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_shipfromfax',getallVendornoRes[0][0]);									
											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebiz_status_flag',"E"); 

											parent.setCurrentLineItemValue('recmachcustrecord_ebiz_stagetable_parent','custrecord_ebizvendor',vendor);

											parent.commitLineItem('recmachcustrecord_ebiz_stagetable_parent');
											//} 
											nlapiSubmitField('customrecord_ebiznet_trn_ebiztask',TaskInternalID, 'custrecord_ebiztask_hostid', 'SPS');


										}
									}
									newcontlp = containerLP;
									previtemId=ItemID;
									//} // End of closed task for loop
									//nlapiLogExecution('DEBUG', 'shiplpsearchresults[q].getId()',shiplpsearchresults[q].getId());
									if(IndividiualOrdersearchrecordClosedTask != null && IndividiualOrdersearchrecordClosedTask != '')
									{
										//for (var b = 0; results1!= null && b < results1.length; b++) 
										//{
										if(results1!= null && results1!= '')
											nlapiSubmitField('customrecord_ebiznet_trn_opentask',results1[0].getId(), 'custrecord_hostid', 'SPS');
										//}
									}
								}
							}
							if(SOid !=null && SOid != '')
							{
								parent.selectNewLineItem('recmachcustrecord_ebiz_asn_parent');
								parent.setCurrentLineItemValue('recmachcustrecord_ebiz_asn_parent','custrecord_parent_orderno', SOid);
								parent.setCurrentLineItemValue('recmachcustrecord_ebiz_asn_parent','custrecord_parent_deliveryid', SOidText);
								parent.setCurrentLineItemValue('recmachcustrecord_ebiz_asn_parent','custrecord_parent_customer', ConsigneeId);
								nlapiLogExecution('DEBUG', 'test','test');
								parent.commitLineItem('recmachcustrecord_ebiz_asn_parent');
							}
//							}
//							else
//							{
//							nlapiLogExecution('DEBUG', 'Records','Some more Pick tasks available for this trailer in open task');
//							}
						}
						nlapiLogExecution('DEBUG', 'Before inserting into stage table - Current Available Usage', nlapiGetContext().getRemainingUsage());
						nlapiLogExecution('DEBUG', 'Before inserting into stage table','done');
						nlapiSubmitRecord(parent); 
						nlapiLogExecution('DEBUG', 'After updating ebiz status flag = E  and Out of Forloop - Current Available Usage', nlapiGetContext().getRemainingUsage());
					}
				}
			}
		}
	}//Case Start 201415862
	catch (exp) {
		nlapiLogExecution('ERROR', 'Exception in packtaskconfirm', exp);
	}
	nlapiLogExecution('ERROR', 'packtaskconfirm ending', 'End');
	//Case End 201415862
}
function updatePACKStatusInOpenTask(orderList)
{
	if(orderList !=null && orderList !=""){
		nlapiLogExecution('DEBUG', 'updatePACKStatusInOpenTask', orderList);
		var filters = new Array();
		var columns = new Array();

		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', orderList));
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [14])); 	
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [28])); 		
		columns[0] = new nlobjSearchColumn('internalid');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask' , null,	filters, columns);	

		for(var cn=0; searchresults != null && cn<searchresults.length; cn++)
		{
			try
			{
				nlapiSubmitField('customrecord_ebiznet_trn_opentask',searchresults[cn].getId(), 'custrecord_hostid', 'P');
			}
			catch (exp) {
				nlapiLogExecution('DEBUG', 'Exeception updatePACKStatusInOpenTask', exp);
			}
		}
	}
}
function updateStatusflagForOrder(ebizorderno)
{
	nlapiLogExecution('DEBUG', 'updateStatusflagForOrder', ebizorderno);
	var filters = new Array();
	var columns = new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ebizorderno));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [14])); 	//SHIP Task
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [28])); // SHIPPED
	filters.push(new nlobjSearchFilter('custrecord_hostid',  null, 'is','P')); 	
	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask' , null,	filters, columns);	
	if(searchresults !=null)
		nlapiLogExecution('DEBUG', 'number of orders for that trailer', searchresults.length);
	for(var cn=0; searchresults !=null && cn<searchresults.length; cn++)
	{
		nlapiLogExecution('DEBUG', 'UpdateOrderStausfortrailer orders', searchresults[cn].getText('custrecord_ebiz_order_no'));
		try
		{
			nlapiSubmitField('customrecord_ebiznet_trn_opentask',searchresults[cn].getId(), 'custrecord_hostid', '');
		}
		catch (exp) {
			nlapiLogExecution('DEBUG', 'Exeception in updateStatusflagForOrder', exp);
		}
	}
}
var ResultOpenTaskRecords ;
function GetOpenTaskRecs(maxno)
{
	try {
		nlapiLogExecution ('DEBUG','GetOpenTaskRecs',maxno);

		var noofshiplpfilter = new Array();

		if(maxno!=null && maxno!='' && maxno!='-1')
			noofshiplpfilter.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));
		else
			ResultOpenTaskRecords = new Array();

		noofshiplpfilter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [14])); 	
		noofshiplpfilter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [28])); 
		noofshiplpfilter.push(new nlobjSearchFilter('custrecord_hostid',  null, 'isempty')); 

		var noofshiplpcol = new Array();
		noofshiplpcol[0] = new nlobjSearchColumn('custrecord_ship_lp_no'); 
		noofshiplpcol[1] = new nlobjSearchColumn('custrecord_ebiz_trailer_no'); 
		noofshiplpcol[2] = new nlobjSearchColumn('custrecord_wms_status_flag'); 
		noofshiplpcol[3] = new nlobjSearchColumn('custrecord_tasktype'); 
		noofshiplpcol[4] = new nlobjSearchColumn('custrecord_container'); 
		noofshiplpcol[5] = new nlobjSearchColumn('custrecord_comp_id'); 
		noofshiplpcol[6] = new nlobjSearchColumn('custrecord_site_id');
		noofshiplpcol[7] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		noofshiplpcol[8] = new nlobjSearchColumn('internalid');
		noofshiplpcol[9] = new nlobjSearchColumn('id').setSort();

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null,noofshiplpfilter, noofshiplpcol);

		if( searchresults!=null && searchresults.length>=1000)
		{ 
			for(var s=0;s<searchresults.length;s++)
			{	
				ResultOpenTaskRecords.push(searchresults[s]);
			}
			var maxno=searchresults[searchresults.length-1].getValue('id');
			GetOpenTaskRecs(maxno);	
		}
		else
		{
			for(var s=0;searchresults!=null && s<searchresults.length;s++)
			{	
				ResultOpenTaskRecords.push(searchresults[s]);
			} 
		}

		if(ResultOpenTaskRecords !=null && ResultOpenTaskRecords !='')
			nlapiLogExecution('DEBUG', 'ResultOpenTaskRecords length', ResultOpenTaskRecords.length);

	}

	catch(exception) {
		nlapiLogExecution('DEBUG', 'GetOpenTaskRecs', exception);
	}

	return ResultOpenTaskRecords;
}
function updateStatusflagForDropshipOrder(ebizorderno)
{
	nlapiLogExecution('DEBUG', 'updateStatusflagForDropshipOrder', ebizorderno);
	var filters = new Array();
	var columns = new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ebizorderno));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [14])); 	//SHIP Task
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [28])); // SHIPPED
	filters.push(new nlobjSearchFilter('custrecord_hostid',  null, 'is','P')); 	
	columns[0] = new nlobjSearchColumn('internalid');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask' , null,	filters, columns);	
	if(searchresults !=null)
		nlapiLogExecution('DEBUG', 'number of orders for that trailer', searchresults.length);
	for(var cn=0; searchresults !=null && cn<searchresults.length; cn++)
	{
		nlapiLogExecution('DEBUG', 'UpdateOrderStausfortrailer orders', searchresults[cn].getText('custrecord_ebiz_order_no'));
		try
		{
			nlapiSubmitField('customrecord_ebiznet_trn_opentask',searchresults[cn].getId(), 'custrecord_hostid', '');
		}
		catch (exp) {
			nlapiLogExecution('DEBUG', 'Exeception in updateStatusflagForDropshipOrder', exp);
		}
	}
}