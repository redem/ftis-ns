//profilerInitialize("DepartTrailer");
/*
eBizNET SOLUTIONS LTD
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_DepartTrailer_sch_SL.js,v $
 *     	   $Revision: 1.1.2.1.4.1 $
 *     	   $Date: 2015/11/03 19:21:06 $
 *     	   $Author: sponnaganti $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_68 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_DepartTrailer_sch_SL.js,v $
 * Revision 1.1.2.1.4.1  2015/11/03 19:21:06  sponnaganti
 * case# 201415184
 * Dynacraft Prod Issue fix
 *
 * Revision 1.1.2.1  2014/05/22 22:13:48  snimmakayala
 * Case #: 20148508
 *
 * Revision 1.2.2.9  2012/06/12 13:22:29  mbpragada
 * CASE201112/CR201113/LOG201121
 * Dynacraft INB/OUB issue fix
 *
 * Revision 1.2.2.8  2012/06/11 22:25:10  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT and Prod issue fixes
 *
 * Revision 1.2.2.7  2012/06/07 13:23:11  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Update shipqty in fulfillment
 *
 * Revision 1.2.2.5  2012/04/20 14:01:18  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.2.2.4  2012/04/12 21:46:16  spendyala
 * CASE201112/CR201113/LOG201121
 * Showing meaningful data instead of showing incomplete data.
 *
 * Revision 1.2.2.3  2012/04/11 11:10:55  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing
 *
 * Revision 1.2.2.2  2012/04/06 14:03:27  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Performance Tunning
 *
 * Revision 1.2.2.1  2012/02/08 14:24:13  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing  related to updateopentask with status flag shipped and actualenddate
 *
 * Revision 1.2  2011/12/12 08:41:22  rgore
 * CASE201112/CR201113/LOG201121
 * Sanitized error message for spelling and grammar.
 * - Ratnakar
 * 12 Dec 2011
 *
 * Revision 1.1  2011/11/23 11:15:41  schepuri
 * CASE201112/CR201113/LOG201121
 * splitting load and depart trailer functionality
 *
 *
 *****************************************************************************/
/**
 * This is the main function which performs the complete Trailer Depart process.
 * @param request
 * @param response
 */
function DepartTrailerSuitelet(request, response){
	if (request.getMethod() == 'GET'){

		nlapiLogExecution('ERROR', 'Time Stamp at the start of GET',TimeStampinSec());
		var form = nlapiCreateForm('Depart Trailer');
		form.setScript('customscript_ebiz_departtrailer_cl');
		var trailerno = "";

		var trailer = form.addField('custpage_trailer', 'select', 'Trailer','customrecord_ebiznet_trailer');
		trailer.setLayoutType('startrow', 'none');

		if (request.getParameter('custpage_trailer') != null && request.getParameter('custpage_trailer') != "") {
			trailerno = request.getParameter('custpage_trailer');
			trailer.setDefaultValue(request.getParameter('custpage_trailer'));
		}

		var pronoresearch = nlapiLoadRecord('customrecord_ebiznet_trailer', trailerno); 

		var trailerName=pronoresearch.getFieldValue('name');
		var currentRecord;
		var resultsetShipLP,resultsetSoNo;
		createLoadTrailerSubList(form);
		nlapiLogExecution('ERROR','trailerName',trailerName);

		var filters = specifyLoadTrailerFilters(trailerName);
		var columns = specifyLoadTrailerColumns();

		var SearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
		if(SearchResults != "" && SearchResults != null){
			nlapiLogExecution('ERROR', "SearchResults is not empty", SearchResults.length);
			for (var i = 0; i < SearchResults.length; i++) {
				currentRecord = SearchResults[i];
				addLoadTrailerDetailsToSubList(form, currentRecord, parseInt(i));
			}

		}

		nlapiLogExecution('ERROR', 'Time Stamp at the end of GET',TimeStampinSec());

		trailer.setMandatory(true);		
		form.addSubmitButton('Depart');
		response.writePage(form);
	}
	else
	{
		nlapiLogExecution('ERROR', 'Time Stamp at the start of POST',TimeStampinSec());
		nlapiLogExecution('ERROR', 'Remaining Usage at the start of POST', nlapiGetContext().getRemainingUsage());


		var form = nlapiCreateForm('Depart Trailer');
		//form.addSubmitButton('Depart');
		var lineCnt = request.getLineItemCount('custpage_departtrailer');
		form.addButton('custpage_backtosearch','Back','backtoqbscreen_new()');
		form.setScript('customscript_ebiz_departtrailer_cl');
		if(lineCnt!=null && lineCnt>0) {

			var trailerno = request.getParameter('custpage_trailer');
			nlapiLogExecution('ERROR', "trailerno", trailerno);
			var pronoresearch = nlapiLoadRecord('customrecord_ebiznet_trailer', trailerno); 
			var trailerName=pronoresearch.getFieldValue('name');
			var ShipLpArray = new Array();
			for (var f = 1; f <= lineCnt; f++) {
				var shiplp = request.getLineItemValue('custpage_departtrailer','custpage_shiplp',f);
				if(ShipLpArray.indexOf(shiplp) == -1)
				{
					ShipLpArray.push(shiplp);
				}
			}
			nlapiLogExecution('ERROR', 'ShipLpArray', ShipLpArray);
			nlapiLogExecution('ERROR', "trailerName", trailerName);
			if(ShipLpArray != null && ShipLpArray != '')
			{
				for (var x = 0; x < ShipLpArray.length; x++) 
				{
					var vShipLp=ShipLpArray[x];
					var filters = new Array();
					filters.push( new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [7]));
					filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
					if(vShipLp != "" && vShipLp!=null)
						filters.push(new nlobjSearchFilter('custrecord_ship_lp_no', null, 'is', vShipLp));	

					var SearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, null);

					if(SearchResults != null && SearchResults !='')
					{
						nlapiLogExecution('ERROR', 'SearchResults.length', SearchResults.length);
						//var msg = form.addField('custpage_messages', 'inlinehtml', null, null, null);
						//msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', '"+trailerName+" is not fully loaded for the ship lp# "+ vShipLp+"', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
						showInlineMessage(form, 'Error', trailerName+ ' is not fully loaded for the ship lp# '+ vShipLp, null);
						response.writePage(form);
						return;
					}

				}
			}
			if(trailerno!=null && trailerno!='')
			{
				//invoke a sched script  
				var param = new Array();
				param['custscript_trailer'] = trailerno;
				nlapiScheduleScript('customscript_departtrailerscheduler', null,param);
				showInlineMessage(form, 'Confirmation', 'Depart Trailer Process has been initiated', null);

			}
			else
			{
				//showInlineMessage(form, 'Error', 'please select the trailer', null);
				var msg = form.addField('custpage_messages', 'inlinehtml', null, null, null);
				msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error','Please select the trailer', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
			}
		}


		response.writePage(form);
	}
}


function updateShipTask(shiplp)
{
	nlapiLogExecution('ERROR', "Into updateShipTask:shiplp", shiplp);

	var shipTaskFilers = new Array();
	var shipTaskColumns = new Array();

	if(shiplp!=null && shiplp!='')
	{
		shipTaskFilers.push(new nlobjSearchFilter('custrecord_ship_lp_no', null, 'is', shiplp));
		shipTaskFilers.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [4])); 	//SHIP Task
		shipTaskFilers.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [10])); // LOADED

		var openTaskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, shipTaskFilers, shipTaskColumns);
		for (var i = 0; openTaskSearchResults!=null && i < openTaskSearchResults.length; i++) {
			var shipTaskId = openTaskSearchResults[i].getId();

			nlapiLogExecution('ERROR', "updateOpenTask:TaskId", shipTaskId);

			nlapiSubmitField('customrecord_ebiznet_trn_opentask', shipTaskId, 'custrecord_wms_status_flag', '14');
		}
	}

	nlapiLogExecution('ERROR', "Out of updateShipTask");
}

/*
 * To update open task with Ship status  
 * @param TaskId // TaskId is internal id of opentask
 */
//function updateOpenTask(openTaskDetails,newParent)
//{	
//nlapiLogExecution('ERROR', "updateOpenTask:TaskId", TaskId);

//if(TaskId!=null && TaskId!='')
//{
//var fieldNames = new Array(); 
//fieldNames.push('custrecord_wms_status_flag');  	
//fieldNames.push('custrecord_act_end_date'); 

//var newValues = new Array(); 
//newValues.push('14');
//var vDate=DateStamp();
//newValues.push(vDate);	

//nlapiSubmitField('customrecord_ebiznet_trn_opentask', TaskId, fieldNames, newValues);

////MoveTaskRecord(TaskId)
//}

//nlapiLogExecution('ERROR', "Out of updateOpenTask:TaskId", TaskId);
//}
function updateOpenTask(openTaskDetails,newParent)
{	
	nlapiLogExecution('ERROR', "into updateOpenTask:TaskId", openTaskDetails);
	if(openTaskDetails!=null && openTaskDetails!='' && openTaskDetails.length>0)
	{
		nlapiLogExecution('ERROR', "openTaskDetails[32]", openTaskDetails[32]);

		newParent.selectNewLineItem('recmachcustrecord_ebiz_ot_parent');

		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'id', openTaskDetails[0]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_status_flag', '14');			
		//newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_end_date',DateStamp());
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_tasktype',openTaskDetails[21]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_sku',openTaskDetails[9]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecordact_begin_date',openTaskDetails[14]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualbegintime',openTaskDetails[15]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_total_weight',openTaskDetails[16]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_totalcube',openTaskDetails[17]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_container_lp_no',openTaskDetails[13]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ship_lp_no',openTaskDetails[5]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_zoneid',openTaskDetails[18]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_sku_no',openTaskDetails[19]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_wave_no',openTaskDetails[4]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_upd_ebiz_user_no',openTaskDetails[20]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','name',openTaskDetails[3]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_order_no',openTaskDetails[6]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebizzone_no',openTaskDetails[22]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_location',openTaskDetails[23]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_packcode',openTaskDetails[24]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_line_no',openTaskDetails[25]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_expe_qty',openTaskDetails[26]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_cntrl_no',openTaskDetails[27]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_qty',openTaskDetails[28]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecordact_begin_date',openTaskDetails[29]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actbeginloc',openTaskDetails[30]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualbegintime',openTaskDetails[31]);
		nlapiLogExecution('ERROR', "openTaskDetails[32]", openTaskDetails[32]);
		if(openTaskDetails[32] != null && openTaskDetails[32] != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_end_date',openTaskDetails[32]);
		else
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_end_date',DateStamp());
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actendloc',openTaskDetails[33]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualendtime',openTaskDetails[34]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_nsconfirm_ref_no',openTaskDetails[35]);

		newParent.commitLineItem('recmachcustrecord_ebiz_ot_parent');		
	}
}
/*
 *  This function is to specify the Load Trailer filters.
 * 	This function returns an array of filter criteria specified. 
 * @param waveno
 * @param fulfillmentorder
 * @param shiplp
 * @returns {Array}
 */
function specifyLoadTrailerFilters(trailername){

	nlapiLogExecution('ERROR','Into specifyLoadTrailerFilters','');
	var filters = new Array();


	if(trailername != "" && trailername!=null){
		nlapiLogExecution('ERROR','Inside trailername',trailername);
		filters.push(new nlobjSearchFilter('custrecord_ebiz_trailer_no', null, 'is', trailername));			
	}
	// Add filter criteria for WMS Status Flag;
	// Search for 'TRAILER LOADED' ;
	filters.push( new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [10]));

	// Add filter criteria for Task type;
	// Search for 'SHIP'/4 ;
	// Search for 'PICK'/3 ;
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
	nlapiLogExecution('ERROR','Out of specifyLoadTrailerFilters','');
	return filters;
}

/**
 *  This function is to specify the Load Trailer columns.
 * 	This returns an array of columns to be fetched from the search record. 
 * @returns {Array}
 */
function specifyLoadTrailerColumns(){
	var columns = new Array();
	columns.push(new nlobjSearchColumn('name'));
	columns.push(new nlobjSearchColumn('custrecord_ship_lp_no'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_wave_no'));
	columns.push(new nlobjSearchColumn('custrecord_sku'));
	columns.push(new nlobjSearchColumn('custrecord_act_qty'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
	columns.push(new nlobjSearchColumn('custrecord_container_lp_no'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_nsconfirm_ref_no'));
	columns.push(new nlobjSearchColumn('custrecord_total_weight'));
	columns.push(new nlobjSearchColumn('custrecordact_begin_date'));
	columns.push(new nlobjSearchColumn('custrecord_actualbegintime'));
	columns.push(new nlobjSearchColumn('custrecord_totalcube'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
	//columns.push(new nlobjSearchColumn('custrecord_ebiz_nsconfirm_ref_no'));
	columns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
	columns.push(new nlobjSearchColumn('custrecord_wms_location'));
	columns.push(new nlobjSearchColumn('custrecord_tasktype'));
	columns.push(new nlobjSearchColumn('custrecord_packcode'));
	columns.push(new nlobjSearchColumn('custrecord_line_no'));
	columns.push(new nlobjSearchColumn('custrecord_expe_qty'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
	columns.push(new nlobjSearchColumn('custrecord_act_qty'));
	columns.push(new nlobjSearchColumn('custrecordact_begin_date'));
	columns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
	columns.push(new nlobjSearchColumn('custrecord_actualbegintime'));
	columns.push(new nlobjSearchColumn('custrecord_act_end_date'));
	columns.push(new nlobjSearchColumn('custrecord_actendloc'));
	columns.push(new nlobjSearchColumn('custrecord_actualendtime'));	 
	//columns.push(new nlobjSearchColumn('tranid','custrecord_ebiz_order_no'));
	//columns.push(new nlobjSearchColumn('custbody_salesorder_carrier','custrecord_ebiz_order_no'));


	columns[0].setSort();

	return columns;
}
/**
 * This function is to create Load Trailer Sublist.
 *  This requires form to be sent as a parameter to build the sublist.
 * @param form
 */
function createLoadTrailerSubList(form){

	var sublist = form.addSubList("custpage_departtrailer", "list", "Load Trailer");
	sublist.addField("custpage_serialno", "text", "SL #");
	sublist.addField("custpage_ordernumber", "text", "Order #");
	sublist.addField("custpage_wavenumber", "text", "Wave #");	 
	sublist.addField("custpage_shiplp", "text", "Ship LP");
	sublist.addField("custpage_internalid", "text", "Internal Id").setDisplayType('hidden');
	sublist.addField("custpage_sono", "text", "sono").setDisplayType('hidden');
	sublist.addField("custpage_itemname", "text", "Item");
	sublist.addField("custpage_ordqty", "text", "Qty");
	sublist.addField("custpage_ebizskuno", "text", "eizskuno").setDisplayType('hidden');
	sublist.addField("custpage_ebizdono", "text", "eizdono").setDisplayType('hidden');
	sublist.addField("custpage_nsrefno", "text", "nsrefno").setDisplayType('hidden');
	sublist.addField("custpage_weight", "text", "Weight").setDisplayType('hidden');
	sublist.addField("custpage_container", "text", "Container").setDisplayType('hidden');	
	sublist.addField("custpage_begindate", "text", "begin date").setDisplayType('hidden');
	sublist.addField("custpage_actbegintime", "text", "actual begin time").setDisplayType('hidden');
	sublist.addField("custpage_ebiz_zoneid", "text", "Zone Id").setDisplayType('hidden');
	sublist.addField("custpage_ebiz_sku_no", "text", "EbizSKU").setDisplayType('hidden');

	sublist.addField("custpage_ebizzone_no", "text", "ebizzone").setDisplayType('hidden');
	sublist.addField("custpage_wms_location", "text", "wmslocation").setDisplayType('hidden');
	sublist.addField("custpage_packcode", "text", "packcode").setDisplayType('hidden');
	sublist.addField("custpage_line_no", "text", "LineNo").setDisplayType('hidden');
	sublist.addField("custpage_expe_qty", "text", "ExpQty").setDisplayType('hidden');
	sublist.addField("custpage_ebiz_cntrl_no", "text", "ebizContrl").setDisplayType('hidden');
	sublist.addField("custpage_act_qty", "text", "ActQty").setDisplayType('hidden');
	sublist.addField("custpage_begin_date", "text", "BeginDate").setDisplayType('hidden');
	sublist.addField("custpage_actbeginloc", "text", "BegLoc").setDisplayType('hidden');
	sublist.addField("custpage_actualbegintime", "text", "BegTime").setDisplayType('hidden');
	sublist.addField("custpage_act_end_date", "text", "EndDate").setDisplayType('hidden');
	sublist.addField("custpage_actendloc", "text", "EndLoc").setDisplayType('hidden');
	sublist.addField("custpage_actualendtime", "text", "EndTime").setDisplayType('hidden');
	//sublist.addField("custpage_ebiz_sku_no", "text", "EbizSKU").setDisplayType('hidden');
	sublist.addField("custpage_tasktype", "text", "Tasktpe").setDisplayType('hidden');
	sublist.addField("custpage_tranid", "text", "TranId").setDisplayType('hidden');
	sublist.addField("custpage_socarrier", "text", "SOCarrier").setDisplayType('hidden');
	//sublist.addField("custpage_weight", "text", "weight").setDisplayType('hidden');
	sublist.addField("custpage_cube", "text", "cube").setDisplayType('hidden');

}

/**
 * This function is to add the Load Trailer details to the sublist.
 * The Load Trailer details to the container management screen include order, wave number,
 * consignee,ship LP, load trailer internal id 
 * @param form
 * @param currentRecord
 * @param i
 */
function addLoadTrailerDetailsToSubList(form, currentRecord, i){

	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_serialno', i + 1, 
			parseInt(i) + 1);

	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_ordernumber', i + 1, 
			currentRecord.getValue('name'));

	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_wavenumber', i + 1, 
			currentRecord.getValue('custrecord_ebiz_wave_no'));	

	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_shiplp', i + 1, 
			currentRecord.getValue('custrecord_ship_lp_no'));

	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_internalid', i + 1, 
			currentRecord.getId());

	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_sono', i + 1, 
			currentRecord.getValue('custrecord_ebiz_order_no'));

	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_itemname', i + 1, 
			currentRecord.getText('custrecord_sku'));

	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_ordqty', i + 1, 
			currentRecord.getValue('custrecord_act_qty'));

	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_ebizskuno', i + 1, 
			currentRecord.getValue('custrecord_sku'));

	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_ebizdono', i + 1, 
			currentRecord.getValue('custrecord_ebiz_cntrl_no'));

	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_nsrefno', i + 1, 
			currentRecord.getValue('custrecord_ebiz_nsconfirm_ref_no'));

	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_weight', i + 1, 
			currentRecord.getValue('custrecord_total_weight'));

	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_cube', i + 1, 
			currentRecord.getValue('custrecord_totalcube'));
	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_tasktype', i + 1, 
			currentRecord.getValue('custrecord_tasktype'));

	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_container', i + 1, 
			currentRecord.getValue('custrecord_container_lp_no'));
	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_ebiz_zoneid', i + 1, 
			currentRecord.getValue('custrecord_ebiz_zoneid'));
	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_ebiz_sku_no', i + 1, 
			currentRecord.getValue('custrecord_ebiz_sku_no'));
	//form.getSubList('custpage_departtrailer').setLineItemValue('custpage_ebiz_nsconfirm_ref_no', i + 1, 
	//		currentRecord.getValue('custrecord_ebiz_nsconfirm_ref_no'));
	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_ebizzone_no', i + 1, 
			currentRecord.getValue('custrecord_ebizzone_no'));
	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_wms_location', i + 1, 
			currentRecord.getValue('custrecord_wms_location'));
	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_packcode', i + 1, 
			currentRecord.getValue('custrecord_packcode'));
	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_line_no', i + 1, 
			currentRecord.getValue('custrecord_line_no'));
	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_expe_qty', i + 1, 
			currentRecord.getValue('custrecord_expe_qty'));
	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_ebiz_cntrl_no', i + 1, 
			currentRecord.getValue('custrecord_ebiz_cntrl_no'));
	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_act_qty', i + 1, 
			currentRecord.getValue('custrecord_act_qty'));
	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_begin_date', i + 1, 
			currentRecord.getValue('custrecordact_begin_date'));
	//form.getSubList('custpage_departtrailer').setLineItemValue('custpage_act_qty', i + 1, 
	//		currentRecord.getValue('custrecord_act_qty'));
	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_actbeginloc', i + 1, 
			currentRecord.getValue('custrecord_actbeginloc'));
	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_actualbegintime', i + 1, 
			currentRecord.getValue('custrecord_actualbegintime'));
	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_act_end_date', i + 1, 
			currentRecord.getValue('custrecord_act_end_date'));
	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_actendloc', i + 1, 
			currentRecord.getValue('custrecord_actendloc'));
	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_actualendtime', i + 1, 
			currentRecord.getValue('custrecord_actualendtime'));
	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_socarrier', i + 1, 
	'');
	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_tranid', i + 1, 
	'');

//	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_socarrier', i + 1, 
//	currentRecord.getText('custbody_salesorder_carrier','custrecord_ebiz_order_no'));
//	form.getSubList('custpage_departtrailer').setLineItemValue('custpage_tranid', i + 1, 
//	currentRecord.getValue('tranid','custrecord_ebiz_order_no'));
	//form.getSubList('custpage_departtrailer').setLineItemValue('custpage_act_end_date', i + 1, 
	//		currentRecord.getValue('custrecord_act_end_date'));


}

function createShiManifestRecord(PronId,Tripno,Actdept,sealno,trailerName,soname,carrier,weight,parent,sonum)
{	
	var string="";
	if(carrier !=null && carrier !=""){
		string = carrier+"-" ;
	}
	if(Tripno !=null && Tripno !=""){
		string += Tripno+"-" ;
	}
	if(trailerName !=null && trailerName !=""){
		string += trailerName+"-" ;
	}
	if(Actdept !=null && Actdept !=""){
		string += Actdept+"-" ;
	}
	if(sealno !=null && sealno !=""){
		string += sealno ;
	}
	var lp=string;

	parent.selectNewLineItem('recmachcustrecord_ebiz_sm_parent');	 

	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','name', soname);
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_orderno', soname);
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_trackno', PronId);
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_contlp', lp);
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_carrier', 'LTL');
	if(weight==0)
		weight=0.1;
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_actwght', weight);
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_sm_parent','custrecord_ship_charges', 0);	

	parent.commitLineItem('recmachcustrecord_ebiz_sm_parent');
	nlapiLogExecution('ERROR', 'Shipmanifest Created', soname);


}

function createShiManifestRecordOld(PronId,Tripno,Actdept,sealno,trailerName,sono,carrier,weight,soname,vCarrier) {
	nlapiLogExecution('ERROR', "into createShiManifestRecord", 'success');

	nlapiLogExecution('ERROR', "PronId", PronId);
	nlapiLogExecution('ERROR', "Tripno", Tripno);
	nlapiLogExecution('ERROR', "Actdept", Actdept);
	nlapiLogExecution('ERROR', "sealno", sealno);
	nlapiLogExecution('ERROR', "trailerName", trailerName);
	nlapiLogExecution('ERROR', "soname", soname);
	nlapiLogExecution('ERROR', "sono", sono);
	nlapiLogExecution('ERROR', "carrier", carrier);
	nlapiLogExecution('ERROR', "weight", weight);
	nlapiLogExecution('ERROR', "vCarrier", vCarrier);
	var string="";
	if(carrier !=null && carrier !=""){
		string = carrier+"-" ;
	}
	if(Tripno !=null && Tripno !=""){
		string += Tripno+"-" ;
	}
	if(trailerName !=null && trailerName !=""){
		string += trailerName+"-" ;
	}
	if(Actdept !=null && Actdept !=""){
		string += Actdept+"-" ;
	}
	if(sealno !=null && sealno !=""){
		string += sealno ;
	}
	var lp=string;

	var ShiManifestRecord = nlapiCreateRecord('customrecord_ship_manifest');

	ShiManifestRecord.setFieldValue('name', sono);
	ShiManifestRecord.setFieldValue('custrecord_ship_orderno', soname);
	ShiManifestRecord.setFieldValue('custrecord_ship_trackno', PronId);
	ShiManifestRecord.setFieldValue('custrecord_ship_contlp', lp);
	ShiManifestRecord.setFieldValue('custrecord_ship_carrier', 'LTL');
	//ShiManifestRecord.setFieldValue('custrecord_ship_pkgwght', weight);
	if(weight==0)
		weight=0.1;
	ShiManifestRecord.setFieldValue('custrecord_ship_actwght', weight);
	ShiManifestRecord.setFieldValue('custrecord_ship_charges', 0);
	ShiManifestRecord.setFieldValue('custrecord_ship_order', sono);
	ShiManifestRecord.setFieldValue('custrecord_ship_act_carrier', vCarrier);
	nlapiLogExecution('ERROR', "Before createShiManifestRecord", '');
	var recid = nlapiSubmitRecord(ShiManifestRecord);
	nlapiLogExecution('ERROR', "After createShiManifestRecord", 'success');

}
/**
 * This function is used to update the Status in closed task record.
 *  
 * @param trailerName
 */
function updateClosedTask(trailerName)
{
	nlapiLogExecution('ERROR', "updateClosedTask:trailerName", trailerName);
	var closedTaskId;
	var closedTaskFilers = new Array();
	var closeTaskDetails = new Array();

	// Add filter criteria for Task type ;
	// Search for 'PICK';
	var vDistinctOrders=new Array();
	var vPrevOrd;
	closedTaskFilers.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));
	closedTaskFilers.push(new nlobjSearchFilter('custrecord_ebiz_trailer_no', null, 'is', trailerName)); 	 

	var closedTaskColumns = new Array();
	closedTaskColumns[0] = new nlobjSearchColumn('name');
	closedTaskColumns[1] = new nlobjSearchColumn('custrecord_ebiz_lpno');
	closedTaskColumns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	closedTaskColumns[2].setSort();
	var closedTaskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, closedTaskFilers, closedTaskColumns);
	if(closedTaskSearchResults!=null && closedTaskSearchResults!='')
	{
		for (var i = 0; i < closedTaskSearchResults.length; i++) {
			closedTaskId = closedTaskSearchResults[i].getId();
			var vcontainerlp = closedTaskSearchResults[i].getValue('custrecord_ebiz_lpno');

			var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', closedTaskId);

			var quantityPicked = transaction.getFieldValue('custrecord_act_qty');
			var fulfillmentOrderInternalId = transaction.getFieldValue('custrecord_ebiz_cntrl_no');
			var fulfillmentOrder = transaction.getFieldValue('name');
			var waveNumber = transaction.getFieldValue('custrecord_ebiz_wave_no');
			var itemId = transaction.getFieldValue('custrecord_sku');
			var closeTaskRecordId = closedTaskSearchResults[i].getId();
			var itemFulfillmentInternalId = transaction.getFieldValue('custrecord_ebiz_nsconfirm_ref_no');//3369
			//var totalweight = transaction.getFieldValue('custrecord_ebiztask_total_weight');//3369
			var ItemOrd = transaction.getFieldValue('custrecord_ebiz_order_no');//Order#
			transaction.setFieldValue('custrecord_wms_status_flag', 14);//added by shylaja on nov 21
			nlapiSubmitRecord(transaction, true);

			var currentRow = [i, closeTaskRecordId, quantityPicked, fulfillmentOrder, 
			                  fulfillmentOrderInternalId, itemId, waveNumber, itemFulfillmentInternalId];

			closeTaskDetails.push(currentRow);

			clearOutboundInventory(vcontainerlp);

			if(vPrevOrd != ItemOrd)
			{	
				vDistinctOrders.push(ItemOrd);
				vPrevOrd == ItemOrd;
			} 	
			else
			{

			}

		}
		// To update Ship status in fulfillment ord line
		if(closeTaskDetails != null && closeTaskDetails != '' && closeTaskDetails.length>0)
		{
			updateFulfillment(closeTaskDetails);
		}	
	}


	return vDistinctOrders;
}

function clearOutboundInventory(vebizcontainerlp)
{
	nlapiLogExecution('ERROR', "Into  clearOutboundInventory" , vebizcontainerlp);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null,'is',vebizcontainerlp));
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['18']));

	var columns= new Array();

	var inventorysearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv',null,filters ,columns);
	if(inventorysearchresults!=null)
	{
		for (var i = 0; inventorysearchresults != null && i < inventorysearchresults.length; i++) 
		{
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', inventorysearchresults[i].getId());
			nlapiLogExecution('ERROR', 'Deleted Outbound Inventory Record : ', id);	
		}
	}

	nlapiLogExecution('ERROR', "Out of  clearOutboundInventory" , vebizcontainerlp);	
}

/**
 * This function is used to update the status flag to SHIPPED and updating ship quantity in Fulfillment 
 * 	Order Line.
 * The close task details array consists of 
 * 	0 = i, 1 = closeTaskRecordId, 2 = quantityPicked, 3 = fulfillmentOrder,	4 = fulfillmentOrderInternalId, 
 * 	5= itemId, 6 = waveNumber, 7 = item fulfillment Id
 * @param fulfillmetIds
 * @param trailerName
 */
function updateFulfillment(closeTaskDetails){
	for (var i = 0; i < closeTaskDetails.length; i++) {
		var fulfillmentOrderInternalId = closeTaskDetails[i][4];
		var shipQuantity = closeTaskDetails[i][2];
		if(fulfillmentOrderInternalId!=null && fulfillmentOrderInternalId!="")
		{
			//set the status flag as STATUS.OUTBOUND.SHIPPED

			/*var transaction = nlapiLoadRecord('customrecord_ebiznet_ordline', fulfillmentOrderInternalId);			
			transaction.setFieldValue('custrecord_linestatus_flag', 14);
			transaction.setFieldValue('custrecord_ship_qty', shipQuantity);
			nlapiSubmitRecord(transaction, true);*/

			var fieldNames = new Array(); 
			fieldNames.push('custrecord_linestatus_flag');  	
			fieldNames.push('custrecord_ship_qty'); 
			nlapiLogExecution('ERROR', "shipQuantity" , shipQuantity);
			var newValues = new Array(); 
			newValues.push('14');	
			newValues.push(shipQuantity);		

			nlapiSubmitField('customrecord_ebiznet_ordline', fulfillmentOrderInternalId, fieldNames, newValues);
		}
	}	
}

/*
 *
 * To process shipmanifest records
 * @param ClosedTaskDetails
 * @param PronId
 * @param Tripno
 * @param Actdept
 * @param sealno
 * @param trailerName
 * @returns {Number}
 */
function ProcessShipmanifest(distinctorders,ClosedTaskDetails,PronId,Tripno,Actdept,sealno,trailerName,TranIdArr,SOCarrierArr,vCarrierFrTrailer)
{	
	nlapiLogExecution('ERROR', "Into ProcessShipmanifest");	

	var count=0;
	var ordarrray = new Array();
	var distinctordFilers = new Array(); 
	var salesOrderNumber='';


	if(distinctorders != null && distinctorders!=""){
		nlapiLogExecution('ERROR', "distinctordSearchResults", distinctorders.length);	

		for (var i = 0; i < distinctorders.length; i++){

			var Salesord=distinctorders[i];
			nlapiLogExecution('ERROR', "Salesordp else", Salesord);
			count +=1;

			var trantypeso = nlapiLookupField('transaction', distinctorders[i], 'recordType');
			//Script added on 15th Feb 2013 by suman
			//To over come time execution process LoadRecord is replaced with search criteria.

			var searchfilter=new Array();
			searchfilter[0]=new nlobjSearchFilter("internalid",null,"anyof",distinctorders[i]);
			searchfilter[1]=new nlobjSearchFilter("mainline",null,"is","T");

			var searchColumn=new Array();
			searchColumn[0]=new nlobjSearchColumn("tranid");
			searchColumn[1]=new nlobjSearchColumn("custbody_salesorder_carrier");

			var salesorderheadresearch=nlapiSearchRecord(trantypeso,null,searchfilter,searchColumn);

			var soname = salesorderheadresearch[0].getValue("tranid");
			var carrier=salesorderheadresearch[0].getText("custbody_salesorder_carrier");
			/*var salesorderheadresearch = nlapiLoadRecord(trantypeso, distinctorders[i]); 
			var soname = salesorderheadresearch.getFieldValue('tranid');
			var carrier=salesorderheadresearch.getFieldText('custbody_salesorder_carrier');*/
			//end of code as of 15th Feb 2013
			nlapiLogExecution('ERROR', "after salesorder", Salesord);
			var weight=getweight(Salesord,ClosedTaskDetails);
			var createShiManifestRecordResult = createShiManifestRecordOld(PronId,Tripno,Actdept,sealno,trailerName,Salesord,carrier,weight,soname,vCarrierFrTrailer);


		}
	}

	nlapiLogExecution('ERROR', "record count", count);
	return count;
}

//var count=0;
//var ordarrray = new Array();
//var distinctordFilers = new Array(); 
//var salesOrderNumber='';


//if(distinctorders != null && distinctorders!=""){
//nlapiLogExecution('ERROR', "distinctordSearchResults", distinctorders.length);	

////var parent1 = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); 

//for (var i = 0; i < distinctorders.length; i++){

//var Salesord=distinctorders[i];
//nlapiLogExecution('ERROR', "Salesordp else", Salesord);
//count +=1;

////var trantypeso = nlapiLookupField('transaction', distinctorders[i], 'recordType');
////var salesorderheadresearch = nlapiLoadRecord(trantypeso, distinctorders[i]); 
////var soname = salesorderheadresearch.getFieldValue('tranid');
//var soname = TranIdArr[i];
////var carrier=salesorderheadresearch.getFieldText('custbody_salesorder_carrier');
//var carrier=SOCarrierArr[i];
//nlapiLogExecution('ERROR', "after salesorder", Salesord);
//nlapiLogExecution('ERROR', "soname", soname);
//nlapiLogExecution('ERROR', "carrier", carrier);
//var weight=getweight(Salesord,ClosedTaskDetails);
////nlapiLogExecution('ERROR', "parent1", parent1);
////var createShiManifestRecordResult = createShiManifestRecord(PronId,Tripno,Actdept,sealno,trailerName,Salesord,carrier,weight,parent1,Salesord);
//var createShiManifestRecordResult = createShiManifestRecordOld(PronId,Tripno,Actdept,sealno,trailerName,Salesord,carrier,weight);
////createShiManifestRecord(PronId,Tripno,Actdept,sealno,trailerName,Salesord,carrier,weight,parent1,Salesord);

//nlapiLogExecution('ERROR', 'Shipmanifest Created', i);
//}
////nlapiSubmitRecord(parent1); 
//}

//nlapiLogExecution('ERROR', "record count", count);
//return count;
//}

function getweight(SalesordId,ClosedTaskDetails)
{
	nlapiLogExecution('ERROR', "Into getweight", SalesordId);
	var totweight=0;

	if(ClosedTaskDetails!=null && ClosedTaskDetails!='')
	{
		for (var i = 0; i < ClosedTaskDetails.length; i++) {
			var ordno = ClosedTaskDetails[i][0];
			var weight = ClosedTaskDetails[i][1];
			if(weight==null || weight=='' || isNaN(weight))
				weight=0;
			if(ordno==SalesordId)
				totweight+=parseFloat(weight);
		}
	}

	nlapiLogExecution('ERROR', "Out of getweight", totweight);
	return totweight;
}

function getweightold(SalesordId)
{
	nlapiLogExecution('ERROR', "getweight", SalesordId);
	var weight;
	var closedTaskFilers = new Array(); 

	closedTaskFilers.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no', null, 'is', SalesordId)); 

	var closedTaskColumns = new Array();
	closedTaskColumns[0] = new nlobjSearchColumn('custrecord_ebiztask_total_weight');

	var closedTaskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, closedTaskFilers, closedTaskColumns);
	if(closedTaskSearchResults!=null && closedTaskSearchResults!='')
	{
		for (var i = 0; i < closedTaskSearchResults.length; i++) {
			weight = closedTaskSearchResults[0].getValue('custrecord_ebiztask_total_weight');
		}
	}
	return weight;
}
