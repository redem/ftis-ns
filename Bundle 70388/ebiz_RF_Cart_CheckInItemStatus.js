/***************************************************************************
eBizNET Solutions 
 ****************************************************************************
 *
 *� $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_Cart_CheckInItemStatus.js,v $
 *� $Revision: 1.3.2.24.4.8.2.42.2.2 $
 *� $Date: 2015/11/14 12:46:49 $
 *� $Author: aanchal $
 *� $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_RF_Cart_CheckInItemStatus.js,v $
 *� Revision 1.3.2.24.4.8.2.42.2.2  2015/11/14 12:46:49  aanchal
 *� 2015.2 Issue fix
 *� 201415633
 *�
 *� Revision 1.3.2.24.4.8.2.42.2.1  2015/10/16 12:59:18  schepuri
 *� case# 201414931
 *�
 *� Revision 1.3.2.24.4.8.2.42  2015/08/10 18:51:07  mvarre
 *� case# 201413930
 *� Phoenix Prod issue
 *� While displaying item statuses we are sorting with "Display sequence" but after scanning item status we
 *� are not sorting with "Display sequence". That's the reason system populated another item status in Open
 *� task.
 *�
 *� Revision 1.3.2.24.4.8.2.41  2015/07/31 13:26:55  rrpulicherla
 *� Case#201413343
 *�
 *� Revision 1.3.2.24.4.8.2.40  2015/07/30 21:16:57  grao
 *� 2015.2   issue fixes  201412876
 *�
 *� Revision 1.3.2.24.4.8.2.39  2015/07/23 13:37:41  schepuri
 *� case#201413593
 *�
 *� Revision 1.3.2.24.4.8.2.38  2015/07/22 15:42:42  grao
 *� 2015.2   issue fixes  201413426
 *�
 *� Revision 1.3.2.24.4.8.2.37  2015/07/15 15:16:45  grao
 *� 2015.2 ssue fixes  201412876
 *�
 *� Revision 1.3.2.24.4.8.2.36  2015/04/27 13:28:16  schepuri
 *� case# 201412498
 *�
 *� Revision 1.3.2.24.4.8.2.35  2015/02/03 13:50:33  schepuri
 *� issue # 201411370
 *�
 *� Revision 1.3.2.24.4.8.2.34  2014/10/17 13:08:06  skavuri
 *� Case# 201410581 Std bundle Issue fixed
 *�
 *� Revision 1.3.2.24.4.8.2.33  2014/09/24 15:49:42  skavuri
 *� Case# 201410490 Std Bundle issue fixed
 *�
 *� Revision 1.3.2.24.4.8.2.32  2014/09/16 14:53:17  skreddy
 *� case # 201410256
 *� TPP SB issue fix
 *�
 *� Revision 1.3.2.24.4.8.2.31  2014/08/07 06:10:09  skreddy
 *� case # 20149835
 *� One Industries SB issue fix
 *�
 *� Revision 1.3.2.24.4.8.2.30  2014/07/29 15:57:15  skavuri
 *� Case# 20149685 SB Issue Fixed
 *�
 *� Revision 1.3.2.24.4.8.2.29  2014/07/24 12:53:47  snimmakayala
 *� no message
 *�
 *� Revision 1.3.2.24.4.8.2.28  2014/07/09 12:33:55  rmukkera
 *� Case # 20149229
 *� jawbone issues applicable to standard also
 *�
 *� Revision 1.3.2.24.4.8.2.27  2014/06/25 14:43:17  rmukkera
 *� Case # 20149055
 *�
 *� Revision 1.3.2.24.4.8.2.26  2014/06/13 09:50:30  grao
 *� Case#: 20148838  New GUI account issue fixes
 *�
 *� Revision 1.3.2.24.4.8.2.25  2014/06/13 08:17:41  skavuri
 *� Case# 20148882 (added Focus Functionality for Textbox)
 *�
 *� Revision 1.3.2.24.4.8.2.24  2014/06/13 08:13:35  skavuri
 *� Case# 20148882 (added Focus Functionality for Textbox)
 *�
 *� Revision 1.3.2.24.4.8.2.23  2014/06/12 14:43:18  grao
 *� Case#: 20148838  New GUI account issue fixes
 *�
 *� Revision 1.3.2.24.4.8.2.22  2014/05/30 00:26:47  nneelam
 *� case#  20148622
 *� Stanadard Bundle Issue Fix.
 *�
 *� Revision 1.3.2.24.4.8.2.21  2014/03/12 06:22:25  nneelam
 *� case#  20127642
 *� Standard Bundle Issue Fix.
 *�
 *� Revision 1.3.2.24.4.8.2.20  2014/03/07 10:49:05  rmukkera
 *� no message
 *�
 *� Revision 1.3.2.24.4.8.2.19  2014/02/04 16:35:38  skavuri
 *� case # 20126898 ((for displaying particular item status in TO Checkin UI ))
 *�
 *� Revision 1.3.2.24.4.8.2.18  2013/12/03 15:32:22  skreddy
 *� Case# 20125952
 *� 2014.1 stnd bundle issue fix
 *�
 *� Revision 1.3.2.24.4.8.2.17  2013/11/27 14:09:43  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *�
 *� prodissue fixes
 *�
 *� Revision 1.3.2.24.4.8.2.16  2013/11/23 02:50:26  snimmakayala
 *� Standard Bundle Fix.
 *� Case# : 201217576
 *�
 *� Revision 1.3.2.24.4.8.2.15  2013/11/22 14:17:15  schepuri
 *� 20125763
 *�
 *� Revision 1.3.2.24.4.8.2.14  2013/10/30 13:14:52  snimmakayala
 *� Standard Bundle Fix.
 *� Case# : 20124515
 *� Empty location check during putaway location generation.
 *�
 *� Revision 1.3.2.24.4.8.2.13  2013/09/17 06:09:01  snimmakayala
 *� GSUSA PROD ISSUE
 *� Case# : 201215000
 *� RF Fast Picking
 *�
 *� Revision 1.3.2.24.4.8.2.12  2013/09/16 15:41:24  rmukkera
 *� Case# 20124313
 *�
 *� Revision 1.3.2.24.4.8.2.11  2013/08/02 15:42:38  rmukkera
 *� Issue fix for
 *� RF Cart Putaway :: When we enter the Cart LP screen is displaying invalid Cart LP message.
 *�
 *� Revision 1.3.2.24.4.8.2.10  2013/06/11 14:30:40  schepuri
 *� Error Code Change ERROR to DEBUG
 *�
 *� Revision 1.3.2.24.4.8.2.9  2013/05/15 14:52:45  grao
 *� CASE201112/CR201113/LOG201121
 *� Standard bundle issues fixes
 *�
 *� Revision 1.3.2.24.4.8.2.8  2013/05/02 14:24:47  schepuri
 *� updating remaning cube issue fix
 *�
 *� Revision 1.3.2.24.4.8.2.7  2013/04/17 16:04:01  skreddy
 *� CASE201112/CR201113/LOG201121
 *� added meta tag
 *�
 *� Revision 1.3.2.24.4.8.2.6  2013/03/15 15:00:13  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Production and UAT issue fixes.
 *�
 *� Revision 1.3.2.24.4.8.2.5  2013/03/13 13:57:16  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Time Stamp related changes.
 *�
 *� Revision 1.3.2.24.4.8.2.4  2013/03/12 06:44:57  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Merged from Monobind.
 *�
 *� Revision 1.3.2.24.4.8.2.3  2013/03/01 15:01:12  rmukkera
 *� code is merged from FactoryMation production as part of Standard bundle
 *�
 *� Revision 1.3.2.24.4.8.2.2  2013/02/26 14:06:50  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Merged from Boombah.
 *�
 *� Revision 1.3.2.24.4.8  2013/02/19 15:38:30  grao
 *� no message
 *�
 *� Revision 1.3.2.24.4.7  2013/02/07 15:10:34  schepuri
 *� CASE201112/CR201113/LOG201121
 *� disabling ENTER Button func added
 *�
 *� Revision 1.3.2.24.4.6  2012/12/05 08:36:59  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� GSUSA UAT Fixes.
 *�
 *� Revision 1.3.2.24.4.5  2012/11/01 14:55:34  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Decimal Qty Conversions
 *�
 *� Revision 1.3.2.24.4.4  2012/09/27 10:53:53  grao
 *� CASE201112/CR201113/LOG201121
 *�
 *� Converting multiple language with given Spanish terms
 *�
 *� Revision 1.3.2.24.4.3  2012/09/26 12:51:32  grao
 *� CASE201112/CR201113/LOG201121
 *� Converting Multi language without small characters
 *�
 *� Revision 1.3.2.24.4.2  2012/09/24 22:45:21  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Issues related to Expiry date is resolved.
 *�
 *� Revision 1.3.2.24.4.1  2012/09/21 14:57:16  grao
 *� CASE201112/CR201113/LOG201121
 *� Converting Multilanguage
 *�
 *� Revision 1.3.2.24  2012/09/11 00:45:23  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Production Issue Fixes for FISK and BOOMBAH.
 *�
 *� Revision 1.3.2.23  2012/09/03 13:45:29  schepuri
 *� CASE201112/CR201113/LOG201121
 *� added date stamp
 *�
 *� Revision 1.3.2.22  2012/07/31 06:52:10  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Issue related to Item Status was resolved.
 *�
 *� Revision 1.3.2.21  2012/07/23 22:49:26  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Checking for the condition weather Item Status is configured on not.
 *�
 *� Revision 1.3.2.20  2012/06/28 14:33:46  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Missing parameters while passing to query string.
 *�
 *� Revision 1.3.2.19  2012/06/22 10:13:11  gkalla
 *� CASE201112/CR201113/LOG201121
 *� Double click event issue
 *�
 *� Revision 1.3.2.18  2012/06/19 16:13:20  gkalla
 *� CASE201112/CR201113/LOG201121
 *� Fixed Cart checkin issues
 *�
 *� Revision 1.3.2.17  2012/06/19 07:07:15  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Issue related to creating same batch# for multiple Sku's.
 *�
 *� Revision 1.3.2.16  2012/05/21 15:01:21  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Searching item status for the item with in the specified site.
 *�
 *� Revision 1.3.2.15  2012/05/17 13:34:22  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Moving checkin task even location assigned failed issue resolved.
 *�
 *� Revision 1.3.2.14  2012/05/14 14:35:41  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Issue related to Cube Calculation is resolved.
 *�
 *� Revision 1.3.2.13  2012/04/27 15:02:38  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Passing Warehouse site as a parameter to fetch the record.
 *�
 *� Revision 1.3.2.12  2012/04/25 21:19:27  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *�
 *� RF cart Changes
 *�
 *� Revision 1.3.2.11  2012/04/17 10:38:39  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *�
 *� RF Cart To Checkin
 *�
 *� Revision 1.3.2.10  2012/04/11 12:36:47  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *�
 *� show item status based on PO location
 *�
 *� Revision 1.3.2.9  2012/04/06 15:16:39  gkalla
 *� CASE201112/CR201113/LOG201121
 *� Disable back button
 *�
 *� Revision 1.3.2.8  2012/03/29 06:32:54  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Cart Checkin issues for TPP
 *�
 *� Revision 1.10  2012/03/29 06:13:39  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Cart Checkin issues for TPP
 *�
 *� Revision 1.9  2012/03/20 16:15:43  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Open Task to Closed Task movement
 *�
 *� Revision 1.8  2012/03/14 09:32:07  gkalla
 *� CASE201112/CR201113/LOG201121
 *� Added disable button functionality
 *�
 *� Revision 1.7  2012/03/02 14:54:28  gkalla
 *� CASE201112/CR201113/LOG201121
 *� Changed Redirecting scripts deploy id
 *�
 *� Revision 1.6  2012/02/23 12:57:46  rmukkera
 *� CASE201112/CR201113/LOG201121
 *� bug fix
 *�
 *� Revision 1.5  2012/02/23 10:27:51  gkalla
 *� CASE201112/CR201113/LOG201121
 *� For cart checkin LP issue
 *�
 *� Revision 1.4  2012/02/23 08:53:34  gkalla
 *� CASE201112/CR201113/LOG201121
 *� For cart checkin LP issue
 *�
 *� Revision 1.3  2012/02/16 10:30:13  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Added FunctionkeyScript
 *�
 *� Revision 1.2  2012/02/15 13:26:38  schepuri
 *� CASE201112/CR201113/LOG201121
 *� fixed invalid LP  issue at getmaxlp no
 *�
 *� Revision 1.1  2012/02/02 08:56:27  rmukkera
 *� CASE201112/CR201113/LOG201121
 *�   cartlp new file
 *�
 *
 ****************************************************************************/

function CheckInItemStatus(request, response){



	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');

	var user=context.getUser();

//	if(sessionobj==null || sessionobj=='')
//	{
//	sessionobj=context.getUser();
//	context.setSessionObject('session', 'Contact'); 
//	}

	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);
	if (request.getMethod() == 'GET') {
		var ItemStatus;
		var itemStatusId;
		var itemStatusLoopCount = 0;
		var nextClicked;

		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);

		//	Get the PO# from the previous screen, which is passed as a parameter	
		
		var cartprocess = request.getParameter('custparam_cartprocess');
		var getPONo = request.getParameter('custparam_poid');
		var getPOItem = request.getParameter('custparam_poitem');
		var getPOLineNo = request.getParameter('custparam_lineno');
		var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		var getPOInternalId = request.getParameter('custparam_pointernalid');
		var getPOQtyEntered = request.getParameter('custparam_poqtyentered');
		var getPOItemRemainingQty = request.getParameter('custparam_poitemremainingqty');
		var getPOLinePackCode = request.getParameter('custparam_polinepackcode');
		var getPOLineItemStatusValue = request.getParameter('custparam_polineitemstatusValue');
		nlapiLogExecution('DEBUG','getPOLineItemStatusValue',getPOLineItemStatusValue);
		var trantype= request.getParameter('custparam_trantype');
		var getItemCube = request.getParameter('custparam_itemcube');
//		var getItemQuantity = request.getParameter('hdnQuantity');
//		var getItemQuantityReceived = request.getParameter('hdnQuantityReceived');
		var getItemQuantity = request.getParameter('custparam_polinequantity');
		var getItemQuantityReceived = request.getParameter('custparam_polinequantityreceived');

		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');

		var getWHLocation = request.getParameter('custparam_whlocation');
		nlapiLogExecution('DEBUG','getWHLocation',getWHLocation);

		var pickfaceEnteredOption = request.getParameter('custparam_enteredOption');

		var itemStatusFilters = new Array();
		itemStatusFilters[0] = new nlobjSearchFilter('isinactive',null, 'is','F');
		itemStatusFilters[1] = new nlobjSearchFilter('custrecord_allowrcvskustatus',null, 'is','T');
		if(getWHLocation!=null && getWHLocation!='')
			itemStatusFilters[2] = new nlobjSearchFilter('custrecord_ebizsiteskus', null, 'anyof', [getWHLocation]);

		var itemStatusColumns = new Array();
		/*itemStatusColumns[0] = new nlobjSearchColumn('custrecord_display_sequence');
		itemStatusColumns[1] = new nlobjSearchColumn('name');
		itemStatusColumns[0].setSort();
		itemStatusColumns[1].setSort();*/


		itemStatusColumns[0] = new nlobjSearchColumn('custrecord_display_sequence');
		itemStatusColumns[1] = new nlobjSearchColumn('name');
		itemStatusColumns[2] = new nlobjSearchColumn('internalid');
		itemStatusColumns[3] = new nlobjSearchColumn('custrecord_defaultskustaus');

		// Case # 20126898?end
		itemStatusColumns[0].setSort();
		itemStatusColumns[1].setSort();
		itemStatusColumns[2].setSort();

		var itemStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);

		var itemStatusCount=0;
		if(itemStatusSearchResult!=null&&itemStatusSearchResult!="")
			itemStatusCount = itemStatusSearchResult.length;
		var poloneitemStatus;
		var polineitemstatusvalue;
		var displayseq;
		for(var k =0; k<itemStatusCount ;k++)
		{
			polineitemstatusvalue =  itemStatusSearchResult[k].getId();
			if(polineitemstatusvalue == getPOLineItemStatusValue)
			{
				poloneitemStatus = itemStatusSearchResult[k].getValue('name');
				displayseq = itemStatusSearchResult[k].getValue('custrecord_display_sequence');
			}
		}
		if (request.getParameter('custparam_count') != null)
		{
			var itemStatusRetrieved = request.getParameter('custparam_count');
			nlapiLogExecution('DEBUG', 'itemStatusRetrieved', itemStatusRetrieved);

			nextClicked = request.getParameter('custparam_nextclicked');
			nlapiLogExecution('DEBUG', 'nextClicked', nextClicked);

			itemStatusCount = itemStatusCount - itemStatusRetrieved;
			nlapiLogExecution('DEBUG', 'itemStatusCount', itemStatusCount);
		}
		else
		{
			var itemStatusRetrieved = 0;
			nlapiLogExecution('DEBUG', 'itemStatusRetrieved', itemStatusRetrieved);
		}

		/*if (itemStatusCount > 4)
		{
			nlapiLogExecution('DEBUG', 'itemStatusCount greater than 4', itemStatusCount);
			itemStatusLoopCount = 4;
		}
		else
		{
			nlapiLogExecution('DEBUG', 'itemStatusCount less than 4', itemStatusCount);
			itemStatusLoopCount = itemStatusCount;
		}*/
		itemStatusLoopCount = itemStatusCount;


		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);


		var st0,st1,st2,st3;

		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "INGRESAR SELECCI&#211;N";			
			st2 = "ENVIAR";
			st3 = "ANTERIOR";

		}
		else
		{
			st0 = "";
			st1 = "ENTER ITEM STATUS"; 
			st2 = "SEND";
			st3 = "PREV";

		}


		var functionkeyHtml=getFunctionkeyScript('_rf_checkin_itemstatus'); 
		var html = "<html><head><title>" + st0 + "</title>"; 
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		//html = html + " document.getElementById('enterstatus').focus();";        


		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL()'>";
		html = html + "	<form name='_rf_checkin_itemstatus' method='POST'>";
		html = html + "		<table>";

		nlapiLogExecution('DEBUG', 'itemStatusLoopCount', itemStatusLoopCount);
		var StatusNo=1;
		for (var i = itemStatusRetrieved; i < (parseFloat(itemStatusRetrieved) + parseFloat(itemStatusLoopCount)); i++) {
			var itemStatusSearchResults = itemStatusSearchResult[i];

			if (itemStatusSearchResults != null)
			{
				itemStatus = itemStatusSearchResults.getValue('name');
				itemStatusId = itemStatusSearchResults.getId();				
			}
			var value = parseFloat(i) + 1;
			//html = html + "			<tr><td align = 'left'>"+ value +". <label>" + itemStatus + "</label></td></tr>";   
			/*if(poloneitemStatus==itemStatus)
			{
				StatusNo=value; 
			}*/
			if(trantype=='transferorder')// case# 201411370
			{
				if (itemStatusSearchResult[i].getValue('custrecord_defaultskustaus') == 'T') 
				{
					StatusNo=value;
				}
			}
			else
			{
				if(poloneitemStatus==itemStatus)
				{
					StatusNo=value; 
				}
			}

			nlapiLogExecution('DEBUG', 'StatusNo', StatusNo);
		}
		html = html + "			<tr>";
//		html = html + "				<td align = 'left'>ENTER ITEM STATUS : <label>" + getPOLineItemStatus + "</label>";
		html = html + "			<tr><td align = 'left'>" + st1 + "</td></tr> ";
		html = html + "			<tr>";
		// case# 20126898 starts (for not displaying default item status no)
		//Case # 20149685 starts
		if(trantype!='transferorder')
		{
			html = html + "				<td align = 'left'><input name='enterstatus' id='enterstatus' type='text' value='"+ StatusNo +"'/>";
		}
		else
		{
			//html = html + "				<td align = 'left'><input name='enterstatus' id='enterstatus' type='text' value=''/>"; // case# 20126898 ends
			html = html + "				<td align = 'left'><input name='enterstatus' id='enterstatus' type='text' value='"+ StatusNo +"'/>";
		}
		//html = html + "				<td align = 'left'><input name='enterstatus' id='enterstatus' type='text' value='"+ StatusNo +"'/>";
		//Case # 20149685 ends
		//html = html + "				<input type='hidden' name='hdndisplayseq' value=" + displayseq + "></td>";
		html = html + "				<input type='hidden' name='hdnItemStatus' value=" + poloneitemStatus + "></td>";
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				<input type='hidden' name='hdnItemPackCode' value=" + getPOLinePackCode + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getItemQuantity + ">";
		html = html + "				<input type='hidden' name='hdnQuantityReceived' value=" + getItemQuantityReceived + ">";
		html = html + "				<input type='hidden' name='hdnItemCube' value=" + getItemCube + ">";
		html = html + "				<input type='hidden' name='hdnItemRemaininingQuantity' value=" + parseFloat(getPOItemRemainingQty) + ">";
		html = html + "				<input type='hidden' name='hdnPOQuantityEntered' value=" + parseFloat(getPOQtyEntered) + ">";
		html = html + "				<input type='hidden' name='hdnPOInternalId' value=" + getPOInternalId + ">";
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		html = html + "				<input type='hidden' name='hdnNextClicked' value=" + nextClicked + ">";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + getWHLocation + ">";
		html = html + "				<input type='hidden' name='hdnPickfaceEnteredOption' value=" + pickfaceEnteredOption + ">";
		html = html + "				<input type='hidden' name='hdntrantype' value=" + trantype + ">";
		html = html + "				<input type='hidden' name='hdnItemStatusNo' value=" + getPOLineItemStatusValue + "></td>";
		html = html + "				<input type='hidden' name='hdnflag'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdncartpro' value=" + cartprocess + ">";
		html = html + "				<input type='hidden' name='hdnCount' value=" + i + "></td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'>" + st2 +"<input name='cmdSend' type='submit' value='ENT' onclick='document.getElementById(\"cmdPrevious\").focus();this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "				<td align = 'left'>" + st2 +"<input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					" + st3 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";




		for (var i = itemStatusRetrieved; i < (parseFloat(itemStatusRetrieved) + parseFloat(itemStatusLoopCount)); i++) {
			var itemStatusSearchResults = itemStatusSearchResult[i];

			if (itemStatusSearchResults != null)
			{
				itemStatus = itemStatusSearchResults.getValue('name');
				itemStatusId = itemStatusSearchResults.getId();				
			}
			var value = parseFloat(i) + 1;
			html = html + "			<tr><td align = 'left'>"+ value +". <label>" + itemStatus + "</label></td></tr>";   
			if(poloneitemStatus==itemStatus)
			{
				StatusNo=value; 
			}

			nlapiLogExecution('DEBUG', 'StatusNo', StatusNo);
		}


		/*if (itemStatusLoopCount > 4)
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>NEXT <input name='cmdNext' type='submit' value='F8'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
		}*/
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterstatus').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {

		nlapiLogExecution('DEBUG', 'Time Stamp at the start of POST',TimeStampinSec());

		var getReturnedItemStatus = request.getParameter('hdnItemStatus');
//		Added by Phani 04-08-2011
		var getItemStatusOption = request.getParameter('enterstatus');
		var getActualBeginTime = request.getParameter('hdnActualBeginTime');
		var getActualBeginTimeAMPM = request.getParameter('hdnActualBeginTimeAMPM');
		var nextClicked = request.getParameter('hdnNextClicked');
		var getWHLocation = request.getParameter('hdnWhLocation');
		var trantype=request.getParameter('hdntrantype');	
		var getOptedField = request.getParameter('hdnOptedField');

//		nlapiLogExecution('DEBUG', 'getWHLocation', getWHLocation);
//		nlapiLogExecution('DEBUG', 'nextClicked', nextClicked);

		// This variable is to hold the Quantity entered.
		var POarray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;

		//nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);

		var st4,st5;
		if( getLanguage == 'es_ES')
		{
			st4 = "ESTADO ELEMENTO NO V&#193;LIDO";				
			st5 = "NO SE PUEDE RECIBIR CON ESTE ESTADO";
		}
		else
		{
			st4 = "INVALID ITEM STATUS";
			st5 = "YOU CANNOT RECEIVE WITH THIS STATUS";
		}

		POarray["custparam_poid"] = request.getParameter('custparam_poid');
		POarray["custparam_StatusNo"] = request.getParameter('hdnItemStatusNo');
		POarray["custparam_poitem"] = request.getParameter('custparam_poitem');
		POarray["custparam_lineno"] = request.getParameter('custparam_lineno');
		POarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
		POarray["custparam_pointernalid"] = request.getParameter('hdnPOInternalId'); 
		POarray["custparam_poqtyentered"] = request.getParameter('hdnPOQuantityEntered'); 
		POarray["custparam_poitemremainingqty"] = request.getParameter('hdnItemRemaininingQuantity'); 
		POarray["custparam_polinepackcode"] = request.getParameter('hdnItemPackCode'); 
		POarray["custparam_polinequantity"] = request.getParameter('hdnQuantity');
		POarray["custparam_polinequantityreceived"] = request.getParameter('hdnQuantityReceived'); 
		POarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
		POarray["custparam_enteredOption"] = request.getParameter('hdnPickfaceEnteredOption');
		POarray["custparam_cartlpno"] = request.getParameter('custparam_cartlpno');
		POarray["custparam_itemcube"] = request.getParameter('custparam_itemcube');
		POarray["custparam_trantype"] = request.getParameter('hdntrantype');		
		POarray["custparam_polineitemstatusValue"] = request.getParameter('hdnItemStatusNo');
		var vitemstatusno = request.getParameter('hdnItemStatusNo');

		POarray["custparam_error"] = st4;
		POarray["custparam_screenno"] = 'CRT5';

		POarray["custparam_actualbegindate"] = request.getParameter('hdnActualBeginDate');
		POarray["custparam_actualbegintime"] = getActualBeginTime; 
		POarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM; 
		// Added by Phani on 03-25-2011
		POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
		/* The below code is merged from FactoryMation production account on 02-28-2013 by Radhika as part of Standard bundle*/	
		//nlapiLogExecution('DEBUG', 'WH Location', POarray["custparam_whlocation"]);

		var getOptedField = request.getParameter('hdnOptedField');
		/* Up to here */ 
		POarray["custparam_option"] = getOptedField;
		 var cartprocess = request.getParameter('hdncartpro');
		 
		 if(cartprocess == null || cartprocess == '' || cartprocess == 'null' )
		 {
			 cartprocess ='CARTSCREEN';

		 }
		POarray["custparam_cartprocess"] = cartprocess;
		
		nlapiLogExecution('DEBUG', 'Cart Option', POarray["custparam_cartprocess"]);

//		nlapiLogExecution('DEBUG', 'WH Location', POarray["custparam_whlocation"]);
//		nlapiLogExecution('DEBUG', 'custparam_poqtyentered', POarray["custparam_poqtyentered"]);
//		nlapiLogExecution('DEBUG', 'optedField', POarray["custparam_option"]);

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		if (sessionobj!=context.getUser()) {

			try
			{

				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}
				var optedEvent; 
				optedEvent = request.getParameter('hdnflag');
				//nlapiLogExecution('DEBUG', 'optedEvent', optedEvent);
				//	if the previous button 'F7' is clicked, it has to go to the previous screen 
				if (request.getParameter('cmdPrevious') == 'F7') {
					if (nextClicked != 'Y')
					{
						POarray["custparam_number"]="";
						POarray["custparam_poqtyentered"]="";
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_checkinqty', 'customdeploy_ebiz_rf_cart_checkinqty_di', false, POarray);
					}
					else
					{
						POarray["custparam_count"] = 0;
//						nlapiLogExecution('DEBUG', 'nextClicked', nextClicked);
//						nlapiLogExecution('DEBUG', 'Next in F7', POarray["custparam_count"]);
						response.sendRedirect('SUITELET', 'customscript_rf_itemstatus', 'customdeploy_rf_itemstatus_di', false, POarray);
					}
				}
				//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
				else if (request.getParameter('cmdNext') == 'F8') {
					POarray["custparam_count"] = request.getParameter('hdnCount');
					POarray["custparam_nextclicked"] = "Y";

					//nlapiLogExecution('DEBUG', 'Next', POarray["custparam_count"]);
					response.sendRedirect('SUITELET', 'customscript_rf_checkin_item_status', 'customdeploy_rf_checkin_item_status_di', false, POarray);
				}
				else
				{
					var fields = ['recordType', 'custitem_ebizserialin','custitem_ebizbatchlot','custitem_item_family','custitem_item_group'];
					var columns = nlapiLookupField('item', POarray["custparam_fetcheditemid"], fields);
					var itemSubtype = columns.recordType;	
					var serialInflg="F";		
					serialInflg = columns.custitem_ebizserialin;
					var batchflag="F";
					batchflag= columns.custitem_ebizbatchlot;
					var itemfamilyID=columns.custitem_item_family;
					var itemgroupID=columns.custitem_item_group;
					//nlapiLogExecution('DEBUG', 'citem subtype is =', itemSubtype);

//					Added by Phani on 04-08-2011            
					if (getItemStatusOption == "")
					{
						//	if the 'Send' button is clicked without any option value entered,
						//  it has to show an error message. The next screen to which it has to navigate is to the error screen.
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('DEBUG', 'Entered Item Status', 'No data');
					}
					else {
						/*
						 * Added by Phani on 04-08-2011.
						 * This part of the code is to fetch the Item Status from the option selected from the Item Status menu.
						 */
						nlapiLogExecution('DEBUG', 'Item Status Option', getItemStatusOption);

						var itemStatusFilters = new Array();
						itemStatusFilters[0] = new nlobjSearchFilter('isinactive',null, 'is','F');
						itemStatusFilters[1] = new nlobjSearchFilter('custrecord_allowrcvskustatus',null, 'is','T');
						if(getWHLocation!=null && getWHLocation!='')
							itemStatusFilters[2] = new nlobjSearchFilter('custrecord_ebizsiteskus', null, 'anyof', [getWHLocation]);

						var itemStatusColumns = new Array();

						/*itemStatusColumns[0] = new nlobjSearchColumn('custrecord_display_sequence');
						itemStatusColumns[1] = new nlobjSearchColumn('name');
						itemStatusColumns[2] = new nlobjSearchColumn('custrecord_allowrcvskustatus');*/

						// case# 201412498
						itemStatusColumns[0] = new nlobjSearchColumn('custrecord_display_sequence');
						itemStatusColumns[1] = new nlobjSearchColumn('name');
						itemStatusColumns[2] = new nlobjSearchColumn('internalid');
						itemStatusColumns[3] = new nlobjSearchColumn('custrecord_defaultskustaus');
						itemStatusColumns[4] = new nlobjSearchColumn('custrecord_allowrcvskustatus');

						itemStatusColumns[0].setSort();
						itemStatusColumns[1].setSort();
						itemStatusColumns[2].setSort();
						var itemStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);
						var itemStatusCount = 0;
						if(itemStatusSearchResult!=null && itemStatusSearchResult!="")
							itemStatusCount = itemStatusSearchResult.length;

						var count = request.getParameter('hdnCount');
						//nlapiLogExecution('DEBUG', 'count', count);

						if (parseFloat(count) >= parseFloat(getItemStatusOption))
						{
							if (itemStatusSearchResult != null)
							{
								var ItemStatusRcvFlag;
								for (var i = 0; i <= getItemStatusOption; i++) {
									var itemStatusSearchResults = itemStatusSearchResult[i];
									if (parseFloat(getItemStatusOption) == parseFloat(i+1))
									{
										//nlapiLogExecution('DEBUG', 'here');
										var itemStatus = itemStatusSearchResults.getValue('name');
										itemStatusId = itemStatusSearchResults.getId();
										//nlapiLogExecution('DEBUG', 'Latest Item Status', itemStatus);
										ItemStatusRcvFlag=itemStatusSearchResults.getValue('custrecord_allowrcvskustatus');
									}
									else
									{
										POarray["custparam_error"] = 'INVALID ITEM STATUS';
										nlapiLogExecution('DEBUG', 'Item Status entered is not a valid status');
										response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);								
									}
								}
								//nlapiLogExecution('DEBUG', 'ItemStatusRcvFlag', ItemStatusRcvFlag);
								if(ItemStatusRcvFlag=='T')
								{
									if (itemStatus != null)
									{
										POarray["custparam_polineitemstatustext"] = itemStatus;//Case# 201410581
										if (itemSubtype == "lotnumberedinventoryitem" || itemSubtype=="lotnumberedassemblyitem" || batchflag=="T" )
										{
											POarray["custparam_polineitemstatus"] = itemStatusId;
											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_chkbatchno', 'customdeploy_ebiz_rf_cart_chkbatchno_di', false, POarray);
											nlapiLogExecution('DEBUG', 'Routing towards LOT/BATCH screens');
										}
										else {
											POarray["custparam_polineitemstatus"] = itemStatusId;

											CheckinLp(request.getParameter('custparam_fetcheditemid'), request.getParameter('custparam_poid'), request.getParameter('custparam_poitem'), 
													request.getParameter('custparam_lineno'), request.getParameter('hdnPOInternalId'), 
													request.getParameter('hdnPOQuantityEntered'), request.getParameter('hdnItemRemaininingQuantity'), 
													request.getParameter('hdnItemPackCode'), itemStatusId, request.getParameter('hdnQuantity'), 
													request.getParameter('hdnQuantityReceived'), request.getParameter('custparam_itemdescription'), 
													request.getParameter('custparam_itemcube'), request.getParameter('hdnActualBeginDate'), 
													getActualBeginTime, getActualBeginTimeAMPM,trantype,getWHLocation,itemSubtype,serialInflg,batchflag,itemfamilyID,itemgroupID,request,response,cartprocess,vitemstatusno);
										}
									}
									else {
										POarray["custparam_error"] = st4;
										response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
										nlapiLogExecution('DEBUG', 'Item Status Not Found as Valid in ITEMSTATUS RECORD');
									}
								}
								else
								{
									POarray["custparam_error"] = st5;
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
									nlapiLogExecution('DEBUG', 'RCV Status flag F');

								}
							}
						}
						else {
							POarray["custparam_error"] = st4;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							nlapiLogExecution('DEBUG', 'Item Status entered is not a valid status');
						}
						nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
					}
				}

				nlapiLogExecution('DEBUG', 'Time Stamp at the end of POST',TimeStampinSec());
			}
			catch (e)  {
				nlapiLogExecution('DEBUG', 'e',e);
				POarray["custparam_error"] = 'INVALID ITEM STATUS';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');
				//case 20125952 start :
				//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_criteriamenu', 'customdeploy_ebiz_rf_cart_criteriamenu', false, POarray);
				//case 20125952 end
			}
		}
		else
		{
			POarray["custparam_screenno"] = 'CRT5P';
			POarray["custparam_error"] = 'ITEM ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
		}
	}


}

function CheckinLp(ItemId,poid,poitem,lineno,pointernalid,poqtyentered,poitemremainingqty,polinepackcode,polineitemstatus,
		polinequantity,polinequantityreceived,itemdescription,itemcube,ActualBeginDate,ActualBeginTime,ActualBeginTimeAMPM,
		trantype,getWHLocation,itemSubtype,serialInflg,batchflag,itemfamilyID,itemgroupID,request,response,cartprocess,vitemstatusno)
{	

	nlapiLogExecution('DEBUG', 'Into CheckinLp');

	nlapiLogExecution('DEBUG', 'Time Stamp at the start of CheckinLp',TimeStampinSec());

	var getItemLP = "", getItemCartLP = "",getsysItemLP="";
	getItemCartLP = request.getParameter('custparam_cartlpno');
	var getFetchedItemId = ItemId;
	var stagelocid='';
	var docklocid='';

	nlapiLogExecution('DEBUG', 'trantype', trantype);
	nlapiLogExecution('DEBUG', 'getItemCartLP', getItemCartLP);
	nlapiLogExecution('DEBUG', 'getWHLocation', getWHLocation);

	try
	{
		stagelocid = GetInboundStageLocation(getWHLocation, null, 'INB');
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in  GetInboundStageLocation', exp);
	}	

	if(stagelocid==null || stagelocid=='')
		stagelocid=getStagingLocation(getWHLocation);

	//docklocid = getDockLocation(getWHLocation);

	docklocid = stagelocid;

	var POarray = new Array();
	// Added by Phani on 03-25-2011
	POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
	var poLoc = request.getParameter('hdnWhLocation');
	//nlapiLogExecution('DEBUG', 'WH Location', POarray["custparam_whlocation"]);	

	nlapiLogExecution('DEBUG', 'Time Stamp at the start of GetMaxLPNo',TimeStampinSec());

	getsysItemLP = GetMaxLPNo(1, 1,poLoc);

	nlapiLogExecution('DEBUG', 'Time Stamp at the end of GetMaxLPNo',TimeStampinSec());

	nlapiLogExecution('DEBUG', 'getsysItemLP', getsysItemLP);

	// This variable is to hold the Quantity entered.

	var EndLocationArray = new Array();


	var getPONo = poid;
	var getPOItem = poitem;
	var getPOLineNo = lineno;
	var getPOInternalId = pointernalid;
	var getPOQtyEntered = poqtyentered;
	var getPOItemRemainingQty = poitemremainingqty;
	var getPOLinePackCode = polinepackcode;
	var getPOLineItemStatus = polineitemstatus;
	var getPOLineQuantity = polinequantity;
	var getPOLineQuantityReceived = polinequantityreceived;
	var getItemDescription = itemdescription;
	var getItemCube = itemcube;
	var getActualBeginDate = ActualBeginDate;
	var getActualBeginTime = ActualBeginTime;
	var getActualBeginTimeAMPM = ActualBeginTimeAMPM;

	var getLineCount = 1;
	var getBaseUOM = 'EACH';
	var getBinLocation = "";

	var getLanguage = request.getParameter('hdngetLanguage');
	POarray["custparam_language"] = getLanguage;

	nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);

	var st6;
	if( getLanguage == 'es_ES')
	{
		st6 = "ESTADO ELEMENTO NO V&#193;LIDO";				

	}
	else
	{
		st6 = "INVALID LP";

	}

	POarray["custparam_error"] = st6;
	POarray["custparam_cartprocess"] = cartprocess;
	POarray["custparam_poid"] = poid;
	POarray["custparam_poitem"] = poitem;
	POarray["custparam_lineno"] = lineno;
	POarray["custparam_fetcheditemid"] =ItemId;
	POarray["custparam_pointernalid"] = pointernalid;
	POarray["custparam_poqtyentered"] = poqtyentered;
	POarray["custparam_poitemremainingqty"] = poitemremainingqty;
	POarray["custparam_polinepackcode"] = polinepackcode;
	POarray["custparam_polinequantity"] = polinequantity;
	POarray["custparam_polinequantityreceived"] = polinequantityreceived;
	POarray["custparam_polineitemstatus"] = polineitemstatus;
	POarray["custparam_itemdescription"] = itemdescription;
	POarray["custparam_itemquantity"] = polinequantity;
	POarray["custparam_cartlpno"] = request.getParameter('custparam_cartlpno');
	POarray["custparam_enteredOption"] = request.getParameter('hdnPickfaceEnteredOption');

	nlapiLogExecution('DEBUG', 'custparam_poqtyentered', POarray["custparam_poqtyentered"]);

	POarray["custparam_itemcube"] = itemcube;

	POarray["custparam_actualbegindate"] =ActualBeginDate;
	POarray["custparam_actualbegintime"] = getActualBeginTime; 
	POarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM; 
	nlapiLogExecution('DEBUG', 'custparam_actualbegintime', POarray["custparam_actualbegintime"]);
	nlapiLogExecution('DEBUG', 'custparam_actualbegintimeampm', POarray["custparam_actualbegintimeampm"]);

	POarray["custparam_screenno"] = 'CRT5';
	POarray["custparam_batchno"] = request.getParameter('hdnBatchNo');
	POarray["custparam_mfgdate"] = request.getParameter('hdnMfgDate');
	POarray["custparam_expdate"] = request.getParameter('hdnExpDate');
	POarray["custparam_bestbeforedate"] = request.getParameter('hdnBestBeforeDate');
	POarray["custparam_lastdate"] = request.getParameter('hdnLastAvlDate');
	POarray["custparam_fifodate"] = request.getParameter('hdnFifoDate');
	POarray["custparam_fifocode"] = request.getParameter('hdnFifoCode');
	POarray["custparam_cartlp"] = request.getParameter('custparam_cartlp');
	POarray["custparam_polineitemstatusValue"] = vitemstatusno;
	POarray["custparam_trantype"] = trantype;


	var POfilters = new Array();
	nlapiLogExecution('DEBUG', 'getPOInternalId', getPOInternalId);
	POfilters[0] = new nlobjSearchFilter('name', null, 'is', getPOInternalId);

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_poreceipt', null, POfilters, null);

	var getPOReceiptNo = '';

	if (searchresults != null && searchresults.length > 0) {
		for (var i = 0; searchresults != null && i < searchresults.length; i++) {
			getPOReceiptNo = searchresults[i].getValue('custrecord_ebiz_poreceipt_receiptno');
		}

		nlapiLogExecution('DEBUG', 'PO Receipt No', getPOReceiptNo);
	}

	var LPReturnValue = "";
	nlapiLogExecution('DEBUG', 'getsysItemLP', getsysItemLP);

	LPReturnValue = true;
	nlapiLogExecution('DEBUG', 'LP Return Value new', LPReturnValue);

	var lpExists = 'N';
	//LP Checking in masterlp record starts
	if (LPReturnValue == true) {
		try {
//			nlapiLogExecution('DEBUG', 'INTO MASTER LP INSERTION');
//			var filtersmlp = new Array();
//			filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', getsysItemLP);
//			if(getWHLocation != null && getWHLocation != '')
//			filtersmlp.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_site', null, 'is', getWHLocation));
//			var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

//			if (SrchRecord != null && SrchRecord.length > 0) {
//			nlapiLogExecution('DEBUG', 'LP FOUND');

//			lpExists = 'Y';
//			}
//			else {
			nlapiLogExecution('DEBUG', 'LP NOT FOUND');
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
			customrecord.setFieldValue('name', getsysItemLP);
			customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', getsysItemLP);
			customrecord.setFieldValue('custrecord_ebiz_lpmaster_site', poLoc);
			customrecord.setFieldValue('custrecord_ebiz_lpmaster_lptype', 4);//LP type=CART
			var rec = nlapiSubmitRecord(customrecord, false, true);
//			}
		} 
		catch (e) {
			nlapiLogExecution('DEBUG', 'Failed to Update/Insert into Master LP Record');
		}
		//LP Checking in masterlp record ends
		nlapiLogExecution('DEBUG', 'POarray.length1', POarray.length);
		nlapiLogExecution('DEBUG', 'lp Exists111', lpExists);
		var remainingCube = '';
		var vPickfaceLocId='';
		if(lpExists != 'Y')
		{
			POarray["custparam_polineitemlp"] = getsysItemLP;
			POarray["custparam_poreceiptno"] = getPOReceiptNo;
			var TotalItemCube = parseFloat(getItemCube) * parseFloat(getPOQtyEntered);
			nlapiLogExecution('DEBUG', 'Total Item Cube ', TotalItemCube );
			/*var itemSubtype = nlapiLookupField('item', POarray["custparam_fetcheditemid"], ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
			POarray["custparam_recordtype"] = itemSubtype.recordType;
			var batchflag=itemSubtype.custitem_ebizbatchlot;
			nlapiLogExecution('DEBUG', 'itemSubtype.recordType =', itemSubtype.recordType);
			nlapiLogExecution('DEBUG', 'itemSubtype.custitem_ebizserialin =', itemSubtype.custitem_ebizserialin);
			nlapiLogExecution('DEBUG', 'itemSubtype.custitem_ebizbatchlot =', itemSubtype.custitem_ebizbatchlot);*/

			//case # 20127642�added item status parameter to priorityputaway.

			var priorityPutawayLocnArr = priorityPutaway(getFetchedItemId, getPOQtyEntered,getWHLocation,getPOLineItemStatus);
			var priorityRemainingQty = priorityPutawayLocnArr[0];
			var priorityQty = priorityPutawayLocnArr[1];
			var priorityLocnID = priorityPutawayLocnArr[2];

			nlapiLogExecution('DEBUG', 'checkInPOSTRequest:priorityRemainingQty', priorityRemainingQty);
			nlapiLogExecution('DEBUG', 'checkInPOSTRequest:priorityQty', priorityQty);
			nlapiLogExecution('DEBUG', 'checkInPOSTRequest:POQtyEntered', getPOQtyEntered);
			nlapiLogExecution('DEBUG', 'checkInPOSTRequest:priorityLocnID', priorityLocnID);

			var pickfaceEnteredOption = request.getParameter('hdnPickfaceEnteredOption');
			var putmethod,putrule;
			var qcRuleCheck=getQCRule(getFetchedItemId);
			nlapiLogExecution('DEBUG', 'qcRuleCheck', qcRuleCheck);

			var context = nlapiGetContext();
			nlapiLogExecution('DEBUG','Remaining usage 1 ',context.getRemainingUsage());
			nlapiLogExecution('DEBUG', 'pickfaceEnteredOption', pickfaceEnteredOption);
			if(pickfaceEnteredOption !=null && pickfaceEnteredOption !='' && pickfaceEnteredOption=='Y')
				vPickfaceLocId=priorityLocnID;
			nlapiLogExecution('DEBUG', 'vPickfaceLocId', vPickfaceLocId);
			if(priorityQty <= getPOQtyEntered){		
				var getBeginLocation=null;

				if(!qcRuleCheck)
				{
				var filters2 = new Array();

				var columns2 = new Array();
				columns2[0] = new nlobjSearchColumn('custitem_item_family');
				columns2[1] = new nlobjSearchColumn('custitem_item_group');
				columns2[2] = new nlobjSearchColumn('custitem_ebizdefskustatus');
				columns2[3] = new nlobjSearchColumn('custitem_item_info_1');
				columns2[4] = new nlobjSearchColumn('custitem_item_info_2');
				columns2[5] = new nlobjSearchColumn('custitem_item_info_3');
				columns2[6] = new nlobjSearchColumn('custitem_ebizabcvelitem');
				filters2.push(new nlobjSearchFilter('internalid', null, 'is', getFetchedItemId));
				var ItemInfoResults = nlapiSearchRecord('item', null, filters2, columns2);

				var filters3=new Array();
				filters3.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
				var columns3=new Array();
				columns3[0] = new nlobjSearchColumn('custrecord_skufamilypickrule');
				columns3[1] = new nlobjSearchColumn('custrecord_skugrouppickrule');
				columns3[2] = new nlobjSearchColumn('custrecord_skustatuspickrule');
				columns3[3] = new nlobjSearchColumn('custrecord_skuinfo1pickrule');
				columns3[4] = new nlobjSearchColumn('custrecord_skuinfo2pickrule');
				columns3[5] = new nlobjSearchColumn('custrecord_skuinfo3pickrule');
				columns3[6] = new nlobjSearchColumn('custrecord_skupickrule');
				columns3[7] = new nlobjSearchColumn('custrecord_abcvelpickrule');
				columns3[8] = new nlobjSearchColumn('custrecord_ebizsitepickput');
				columns3[9] = new nlobjSearchColumn('custrecord_ruleidpickrule');
				columns3[10] = new nlobjSearchColumn('custrecord_putawaymethod');
				columns3[11] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
				columns3[12] = new nlobjSearchColumn('custrecord_manual_location_generation');
				columns3[13] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
				columns3[14] = new nlobjSearchColumn('formulanumeric');
				columns3[14].setFormula("TO_NUMBER({custrecord_sequencenumberpickrule})");
				columns3[15] = new nlobjSearchColumn('custrecord_locationpickrule');
				//The below column is added by Satish.N on 29OCT2013 - Case # 20124515
				columns3[16] = new nlobjSearchColumn('custrecord_emptylocation','custrecord_putawaymethod');
				// Upto here on 29OCT2013 - Case # 20124515
				columns3[14].setSort();

				var putrulesearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters3, columns3);

				//getBeginLocation = generatePutawayLocation(getFetchedItemId, getPOLinePackCode, getPOLineItemStatus, getBaseUOM, parseFloat(TotalItemCube), itemSubtype.recordType,poLoc);
				nlapiLogExecution('DEBUG', 'Time Stamp at the start of GetPutawayLocationNew',TimeStampinSec());

				getBeginLocation = GetPutawayLocationNew(getFetchedItemId, getPOLinePackCode, getPOLineItemStatus, getBaseUOM, 
						parseFloat(TotalItemCube),itemSubtype,poLoc,ItemInfoResults,putrulesearchresults,vPickfaceLocId);

				nlapiLogExecution('DEBUG', 'Time Stamp at the end of GetPutawayLocationNew',TimeStampinSec());

				var context = nlapiGetContext();
				nlapiLogExecution('DEBUG','Remaining usage 2 ',context.getRemainingUsage());
			}
				nlapiLogExecution('DEBUG', 'Begin Location', getBeginLocation);

				var vLocationname="";
				if (getBeginLocation != null && getBeginLocation != '') {
					var getBeginLocationId = getBeginLocation[2];
					vLocationname= getBeginLocation[0];
					nlapiLogExecution('DEBUG', 'Begin Location Id', getBeginLocationId);
				}
				else {
					var getBeginLocationId = "";
					nlapiLogExecution('DEBUG', 'Begin Location Id is null', getBeginLocationId);
				}

				if(getBeginLocation!=null && getBeginLocation != '')
					remainingCube = getBeginLocation[1];

				nlapiLogExecution('DEBUG', 'remainingCube', remainingCube);
				if (getBeginLocation != null && getBeginLocation != '') {
					putmethod = getBeginLocation[9];
					putrule = getBeginLocation[10];
				}
				else
				{
					putmethod = "";
					putrule = "";
				}
			}

			var getActualEndDate = DateStamp();
			nlapiLogExecution('DEBUG', 'getActualEndDate', getActualEndDate);
			var getActualEndTime = TimeStamp();
			nlapiLogExecution('DEBUG', 'getActualEndTime', getActualEndTime);
			//Case# 201410490 starts
			//if (itemSubtype == 'serializedinventoryitem' || serialInflg == 'T') {
			if (itemSubtype == 'serializedinventoryitem' || serialInflg == 'T' || itemSubtype == 'serializedassemblyitem') {
				//Case# 201410490 ends
				nlapiLogExecution('DEBUG', 'serializedinventoryitem', '');
				var dimfilters = new Array();
				dimfilters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', POarray["custparam_fetcheditemid"]);
				dimfilters[1] = new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T');
				var dimcolumn = new Array();
				dimcolumn[0] = new nlobjSearchColumn('custrecord_ebizuomskudim');
				dimcolumn[1] = new nlobjSearchColumn('custrecord_ebizqty');
				var dimsearchresult = nlapiSearchRecord('customrecord_ebiznet_skudims', null, dimfilters, dimcolumn);

				if (dimsearchresult != null && dimsearchresult.length > 0) {
					POarray["custparam_uomid"] = dimsearchresult[0].getValue('custrecord_ebizuomskudim');
					POarray["custparam_uomidtext"] = dimsearchresult[0].getText('custrecord_ebizuomskudim');
					POarray["custparam_uomqty"] = dimsearchresult[0].getText('custrecord_ebizqty');
					nlapiLogExecution('DEBUG', 'POarray["custparam_uomid"]', POarray["custparam_uomid"]);
				}
				POarray["custparam_qccheck"] = qcRuleCheck;							

				POarray["custparam_locid"] = getBeginLocationId;
				POarray["custparam_remainingCube"] = remainingCube;

				nlapiLogExecution('DEBUG', 'POarray["custparam_remainingCube"]', POarray["custparam_remainingCube"]);

				// POarray["custparam_location"] = getBeginLocation;
				POarray["custparam_location"] = vLocationname;
				POarray["custparam_number"] = 0;
				nlapiLogExecution('DEBUG', 'getBeginLocationId', getBeginLocationId);
				nlapiLogExecution('DEBUG', 'POarray["custparam_number"]', POarray["custparam_number"]);
				nlapiLogExecution('DEBUG', 'POarray.length', POarray.length);
				// case no 20125763
				POarray["custparam_putmethod"] = putmethod;
				POarray["custparam_putrule"] = putrule;

				//response.sendRedirect('SUITELET', 'customscript_serial_no', 'customdeploy_serial_no', false, POarray);
				response.sendRedirect('SUITELET', 'customscript_serial_no', 'customdeploy_serial_no_di', false, POarray);

				nlapiLogExecution('DEBUG', 'Towards Serial No. Screen');
				return;
				nlapiLogExecution('DEBUG', 'This is after return statement');
			}
			nlapiLogExecution('DEBUG', 'This is before executing PUTW record creation');

			/*
			 * This is to insert a record in Transaction Line Details custom record
			 * The purpose of this is to identify the quantity checked-in, location generated for and putaway confirmation
			 * */ 
			var context = nlapiGetContext();
			nlapiLogExecution('DEBUG','Remaining usage 3 ',context.getRemainingUsage());
			var trantype= nlapiLookupField('transaction',POarray["custparam_pointernalid"],'recordType');

			TrnLineUpdation(trantype, 'CHKN', POarray["custparam_poid"], POarray["custparam_pointernalid"], 
					POarray["custparam_lineno"], POarray["custparam_fetcheditemid"], 
					POarray["custparam_polinequantity"], POarray["custparam_poqtyentered"],"",getPOLineItemStatus);

			/*
			 * If the received quantity has to be putaway to a pickface location based on priority putaway flag, 
			 * 	create PUTW task in TRN_OPENTASK 
			 * 		set taskType = 2(PUTW) and wmsStatusFlag = 2(INBOUND/LOCATIONS ASSIGNED)
			 * 		beginLocation = priorityLocnID and endLocn = ""
			 */	

			if (priorityQty >= getPOQtyEntered){			//(priorityQty != 0){
				taskType = "2"; // PUTW
				wmsStatusFlag = "2"; // INBOUND/LOCATIONS ASSIGNED
				getBeginLocationId = priorityLocnID;
				var LocRemCube = GeteLocCube(getBeginLocationId);

				nlapiLogExecution('DEBUG', 'LocRemCube', LocRemCube);
				if (parseFloat(LocRemCube) > parseFloat(TotalItemCube)) 
					remainingCube = parseFloat(LocRemCube) - parseFloat(TotalItemCube);
			}

			nlapiLogExecution('DEBUG', 'Before custChknPutwRecCreation calling (getBeginLocationId)', getBeginLocationId);
			nlapiLogExecution('DEBUG', 'getPOInternalId', getPOInternalId);
			nlapiLogExecution('DEBUG', 'getPONo', getPONo);
			nlapiLogExecution('DEBUG', 'getPOQtyEntered', getPOQtyEntered);
			nlapiLogExecution('DEBUG', 'getPOLineNo', getPOLineNo);
			nlapiLogExecution('DEBUG', 'getPOItemRemainingQty', getPOItemRemainingQty);
			nlapiLogExecution('DEBUG', 'getLineCount', getLineCount);
			nlapiLogExecution('DEBUG', 'getFetchedItemId', getFetchedItemId);
			nlapiLogExecution('DEBUG', 'getPOItem', getPOItem);
			nlapiLogExecution('DEBUG', 'getItemDescription', getItemDescription);
			nlapiLogExecution('DEBUG', 'getPOLineItemStatus', getPOLineItemStatus);
			nlapiLogExecution('DEBUG', 'getPOLinePackCode', getPOLinePackCode);
			nlapiLogExecution('DEBUG', 'getBaseUOM', getBaseUOM);
			nlapiLogExecution('DEBUG', 'getBinLocation', getBinLocation);
			nlapiLogExecution('DEBUG', 'getPOLineQuantityReceived', getPOLineQuantityReceived);
			nlapiLogExecution('DEBUG', 'getActualEndDate', getActualEndDate);
			nlapiLogExecution('DEBUG', 'getActualEndTime', getActualEndTime);
			nlapiLogExecution('DEBUG', 'getPOReceiptNo', getPOReceiptNo);
			nlapiLogExecution('DEBUG', 'getActualBeginDate', getActualBeginDate);
			nlapiLogExecution('DEBUG', 'getActualBeginTime', getActualBeginTime);
			nlapiLogExecution('DEBUG', 'getActualBeginTimeAMPM', getActualBeginTimeAMPM);
			nlapiLogExecution('DEBUG', 'getBeginLocationId', getBeginLocationId);
			nlapiLogExecution('DEBUG', 'POarray["custparam_batchno"]', POarray["custparam_batchno"]);
			nlapiLogExecution('DEBUG', 'itemSubtype.recordType', itemSubtype);
			nlapiLogExecution('DEBUG', 'itemSubtype.custitem_ebizbatchlot', batchflag);
			nlapiLogExecution('DEBUG', 'batchflag', batchflag);
			nlapiLogExecution('DEBUG', 'getItemCartLP', getItemCartLP);
			nlapiLogExecution('DEBUG', 'POarray["custparam_whlocation"]', POarray["custparam_whlocation"]);
			nlapiLogExecution('DEBUG', 'batchflag', batchflag);
			nlapiLogExecution('DEBUG', 'putmethod', putmethod);
			nlapiLogExecution('DEBUG', 'putrule', putrule);
			var context = nlapiGetContext();
			nlapiLogExecution('DEBUG','Remaining usage 4 ',context.getRemainingUsage());
			var vValid=custChknPutwRecCreation(getPOInternalId, getPONo, getPOQtyEntered, getPOLineNo, getPOItemRemainingQty,  
					getLineCount, getFetchedItemId, getPOItem, getItemDescription, getPOLineItemStatus, getPOLinePackCode, getBaseUOM, getsysItemLP, getBinLocation, 
					getPOLineQuantityReceived, getActualEndDate, getActualEndTime, getPOReceiptNo, "", getActualBeginDate, 
					getActualBeginTime, getActualBeginTimeAMPM, getBeginLocationId, POarray["custparam_batchno"], 
					POarray["custparam_mfgdate"], POarray["custparam_expdate"], POarray["custparam_bestbeforedate"], 
					POarray["custparam_lastdate"], POarray["custparam_fifodate"], POarray["custparam_fifocode"],itemSubtype, 
					getItemCartLP, POarray["custparam_whlocation"],batchflag,putmethod,putrule,stagelocid,docklocid,serialInflg,itemfamilyID,itemgroupID,qcRuleCheck);

			var context = nlapiGetContext();
			nlapiLogExecution('DEBUG','Remaining usage 5 ',context.getRemainingUsage());
			nlapiLogExecution('DEBUG', 'vValid', vValid);
			if(vValid==false)
			{
				response.sendRedirect('SUITELET', 'customscript_rf_checkin_cnf', 'customdeploy_rf_checkin_cnf', false, POarray);
			}
			else
			{
				/*
				 * This is to insert a record in inventory custom record.
				 * The data that is to be inserted is PO Internal Id, Item, Item Status, Pack Code, Quantity, LP#
				 * The parameters list is: 
				 * pointid, itemid, itemdesc, itemstatus, itempackcode, quantity, invtlp, warehouse location
				 * 
				 */  

				TrnLineUpdation(trantype, 'ASPW', POarray["custparam_poid"], POarray["custparam_pointernalid"], 
						POarray["custparam_lineno"], POarray["custparam_fetcheditemid"], 
						POarray["custparam_polinequantity"], POarray["custparam_poqtyentered"],"");
				if(getBeginLocationId!=null && getBeginLocationId!='')
				{
					UpdateLocCube(getBeginLocationId, remainingCube);
				}

				if (itemSubtype == 'lotnumberedinventoryitem' || itemSubtype == "lotnumberedassemblyitem" 
					|| itemSubtype == "assemblyitem" || batchflag == 'T') {
					//checks for Batch/Lot exists in Batch Entry, Update if exists if not insert
					try {
						nlapiLogExecution('DEBUG', 'in');
						var filtersbat = new Array();
						filtersbat[0] = new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', POarray["custparam_batchno"]);
						if(getFetchedItemId!=null&&getFetchedItemId!="")
							filtersbat[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', getFetchedItemId);

						var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filtersbat);
						nlapiLogExecution('DEBUG', 'SrchRecord', SrchRecord);
						if (SrchRecord) {

							nlapiLogExecution('DEBUG', ' BATCH FOUND');
							var transaction = nlapiLoadRecord('customrecord_ebiznet_batch_entry', SrchRecord[0].getId());
							transaction.setFieldValue('custrecord_ebizmfgdate', POarray["custparam_mfgdate"]);
							transaction.setFieldValue('custrecord_ebizbestbeforedate', POarray["custparam_bestbeforedate"]);
							transaction.setFieldValue('custrecord_ebizexpirydate', POarray["custparam_expdate"]);
							transaction.setFieldValue('custrecord_ebizfifodate', POarray["custparam_fifodate"]);
							transaction.setFieldValue('custrecord_ebizfifocode', POarray["custparam_fifocode"]);
							transaction.setFieldValue('custrecord_ebizlastavldate', POarray["custparam_lastdate"]);
							var rec = nlapiSubmitRecord(transaction, false, true);

						}
						else {
							nlapiLogExecution('DEBUG', 'BATCH NOT FOUND');

							var customrecord = nlapiCreateRecord('customrecord_ebiznet_batch_entry');
							customrecord.setFieldValue('name', POarray["custparam_batchno"]);
							customrecord.setFieldValue('custrecord_ebizlotbatch', POarray["custparam_batchno"]);
							customrecord.setFieldValue('custrecord_ebizmfgdate', POarray["custparam_mfgdate"]);
							customrecord.setFieldValue('custrecord_ebizbestbeforedate', POarray["custparam_bestbeforedate"]);
							customrecord.setFieldValue('custrecord_ebizexpirydate', POarray["custparam_expdate"]);
							customrecord.setFieldValue('custrecord_ebizfifodate', POarray["custparam_fifodate"]);
							customrecord.setFieldValue('custrecord_ebizfifocode', POarray["custparam_fifocode"]);
							customrecord.setFieldValue('custrecord_ebizlastavldate', POarray["custparam_lastdate"]);
							nlapiLogExecution('DEBUG', 'Item Id',getFetchedItemId);
							customrecord.setFieldValue('custrecord_ebizsku', getFetchedItemId);
							customrecord.setFieldValue('custrecord_ebizpackcode',getPOLinePackCode);
							customrecord.setFieldValue('custrecord_ebizsitebatch',POarray["custparam_whlocation"]);
							var rec = nlapiSubmitRecord(customrecord, false, true);
						}
					} 
					catch (e) {
						nlapiLogExecution('DEBUG', 'Failed to Update/Insert into BATCH ENTRY Record');
					}
				}

				rf_checkin(getPOInternalId, getFetchedItemId, getItemDescription, getPOLineItemStatus, 
						getPOLinePackCode, getPOQtyEntered, getsysItemLP, POarray["custparam_whlocation"],
						POarray["custparam_batchno"], POarray["custparam_mfgdate"], 
						POarray["custparam_expdate"], POarray["custparam_bestbeforedate"], 
						POarray["custparam_lastdate"], POarray["custparam_fifodate"], 
						POarray["custparam_fifocode"],trantype,stagelocid,docklocid);

				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_criteriamenu', 'customdeploy_ebiz_rf_cart_criteriamenu', false, POarray);
			}
		}
		else {
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			nlapiLogExecution('DEBUG', 'Entered Item LP', getsysItemLP);
		}
	}
	else {
		response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
		nlapiLogExecution('DEBUG', 'Entered Item LP', getsysItemLP);
	}

	nlapiLogExecution('DEBUG', 'Time Stamp at the end of CheckinLp',TimeStampinSec());
	nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');

}

function custChknPutwRecCreation(po, poValue, quan, lineno, remQty, lineCnt, ItemId, ItemName, 
		ItemDesc, ItemStatus, PackCode, BaseUOM, LP, BinLoc, RcvQty, ActualEndDate, ActualEndTime, 
		poreceiptno, BeginLocation, ActualBeginDate, ActualBeginTime, ActualBeginTimeAMPM, BeginLocationId, 
		BatchNo, MfgDate, ExpDate, BestBeforeDate, LastDate, FifoDate, FifoCode, itemrectype, cartlp, 
		WHLocation,batchflag,putmethod,putrule,stagelocid,docklocid,serialInflg,itemfamilyID,itemgroupID,qcRuleCheck)
{
	nlapiLogExecution('DEBUG', 'Into custChknPutwRecCreation');
	nlapiLogExecution('DEBUG', 'Time Stamp at the start of custChknPutwRecCreation',TimeStampinSec());

	/*var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group'];
	var columns = nlapiLookupField('item', ItemId, fields);*/
	var ItemType = itemrectype;					
	var batchflg = batchflag;
	var itemfamId= itemfamilyID;
	var itemgrpId= itemgroupID;
	var getlotnoid="";

	var vValid=true;

	var now = new Date();
	/* The below code is merged from FactoryMation production account on 02-28-2013 by Radhika as part of Standard bundle*/

	var stagelocid='';
	var docklocid='';
	/* Up to here */ 
	//stagelocid = getStagingLocation();
	nlapiLogExecution('DEBUG', 'Stage Location', stagelocid);

	//docklocid = getDockLocation();
	nlapiLogExecution('DEBUG', 'Dock Location', docklocid);

	if (BeginLocationId != null && BeginLocationId != '' ) //Creating custom record with CHKN and PUTW task,
	{
		//create the openTask record With CHKN Task Type
		var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating CHKN Record', 'TRN_OPENTASK');

		//populating the fields
		customrecord.setFieldValue('name', poValue);
		customrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		customrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		customrecord.setFieldValue('custrecord_act_qty', quan);
		customrecord.setFieldValue('custrecord_expe_qty', quan);
		customrecord.setFieldValue('custrecord_lpno', LP);
		customrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		customrecord.setFieldValue('custrecord_line_no', lineno);
		customrecord.setFieldValue('custrecord_packcode', PackCode);
		customrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		customrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		customrecord.setFieldValue('custrecord_tasktype', 1); //For Check-In (CHKN)
		customrecord.setFieldValue('custrecord_actbeginloc', docklocid);
		customrecord.setFieldValue('custrecord_actendloc', stagelocid);
		customrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		//Added for Item name and desc
		customrecord.setFieldValue('custrecord_sku', ItemId);
		customrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//For eBiznet Receipt Number .
		customrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		customrecord.setFieldValue('custrecordact_begin_date', ActualBeginDate);
		customrecord.setFieldValue('custrecord_act_end_date', ActualEndDate);

		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		//	customrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		nlapiLogExecution('DEBUG', 'getActualBeginTime', getActualBeginTime);
		customrecord.setFieldValue('custrecord_actualendtime', TimeStamp());
		customrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		customrecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
		customrecord.setFieldValue('custrecord_current_date', DateStamp());
		customrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		customrecord.setFieldValue('custrecord_wms_status_flag', 1);

		if(po != null && po != '')
			customrecord.setFieldValue('custrecord_ebiz_order_no', po);

		// Added by Phani on 03-25-2011
		// Insert the value in the WMS Location
		customrecord.setFieldValue('custrecord_wms_location', WHLocation);//

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" || itemrectype == "assemblyitem"  ||batchflag=='T')
		{
			customrecord.setFieldValue('custrecord_batch_no', BatchNo);
			customrecord.setFieldValue('custrecord_expirydate', ExpDate);
			customrecord.setFieldValue('custrecord_fifodate', FifoDate);
			if(BatchNo !=null && BatchNo!='' && BatchNo!='null' && BatchNo!='undefined')
			{
				customrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");
			}
		}
		//cartLP
		customrecord.setFieldValue('custrecord_transport_lp', cartlp);
		nlapiLogExecution('DEBUG', 'Submitting CHKN record', 'TRN_OPENTASK');

		var currentUserID = getCurrentUser();

		customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		customrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		customrecord.setFieldValue('custrecord_taskassignedto', currentUserID);

		//commit the record to NetSuite
		var reccheckid = nlapiSubmitRecord(customrecord);
		nlapiLogExecution('DEBUG', 'Done CHKN Record Insertion :', 'Success');
		nlapiLogExecution('DEBUG', 'BeginLocationId', BeginLocationId);

		//create the openTask record With PUTW Task Type
		/*if(BeginLocationId==null || BeginLocationId=="")
			return false;*/
		if(!qcRuleCheck && (BeginLocationId==null || BeginLocationId==""))
			return false;
		var putwrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating PUTW record', 'TRN_OPENTASK');

		//populating the fields
		putwrecord.setFieldValue('name', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		putwrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		putwrecord.setFieldValue('custrecord_lpno', LP);
		putwrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		putwrecord.setFieldValue('custrecord_line_no', lineno);
		/* The below code is merged from FactoryMation production account on 02-28-2013 by Radhika as part of Standard bundle*/		
		if(PackCode==null || PackCode=='')
			PackCode=1;
		/* Up to here */ 
		putwrecord.setFieldValue('custrecord_packcode', PackCode);
		putwrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		putwrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		putwrecord.setFieldValue('custrecord_tasktype', 2); //For Putaway (PUTW)

		nlapiLogExecution('DEBUG', 'Fetched Begin Location', BeginLocation);

		putwrecord.setFieldValue('custrecord_actbeginloc', BeginLocationId);
		putwrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		//For eBiznet Receipt Number .
		putwrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		putwrecord.setFieldValue('custrecordact_begin_date', DateStamp());

		//Adding fields to update time zones.
		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		//putwrecord.setFieldValue('custrecord_actualbegintime', TimeStamp());
		putwrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		putwrecord.setFieldValue('custrecord_current_date', DateStamp());
		putwrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		/*if (BeginLocationId != "") {
			putwrecord.setFieldValue('custrecord_wms_status_flag', 2);
		}
		else {
			putwrecord.setFieldValue('custrecord_wms_status_flag', 6);
		}*/
		nlapiLogExecution('DEBUG', 'qcRuleCheck', qcRuleCheck);

		if(qcRuleCheck)
		{
			putwrecord.setFieldValue('custrecord_wms_status_flag', 23);
		}
		else
		{ 
			if (BeginLocationId != "") {
				putwrecord.setFieldValue('custrecord_wms_status_flag', 2);
			}
			else {
				putwrecord.setFieldValue('custrecord_wms_status_flag', 6);
			}
			//putwrecord.setFieldValue('custrecord_wms_status_flag', 6);
			//putwrecord.setFieldValue('custrecord_actbeginloc', BeginLocationId);


		}
		//Added for Item name and desc
		putwrecord.setFieldValue('custrecord_sku', ItemId);
		putwrecord.setFieldValue('custrecord_skudesc', ItemDesc.substring(0, 200));
		putwrecord.setFieldValue('custrecord_wms_location', WHLocation);//

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" || itemrectype == "assemblyitem" ||batchflag=='T')
		{
			putwrecord.setFieldValue('custrecord_batch_no', BatchNo);
			putwrecord.setFieldValue('custrecord_expirydate', ExpDate);
			putwrecord.setFieldValue('custrecord_fifodate', FifoDate);
			if(BatchNo !=null && BatchNo!='' && BatchNo!='null' && BatchNo!='undefined')
			{
				putwrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");
			}
		}
		//cartLP
		putwrecord.setFieldValue('custrecord_transport_lp', cartlp);

		var currentUserID = getCurrentUser();

		putwrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		putwrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		putwrecord.setFieldValue('custrecord_taskassignedto', currentUserID);
		if(putmethod!=null && putmethod!='')
			putwrecord.setFieldValue('custrecord_ebizmethod_no', putmethod);
		if(putrule!=null && putrule!='')
			putwrecord.setFieldValue('custrecord_ebizrule_no', putrule);

		if(po != null && po != '')
			putwrecord.setFieldValue('custrecord_ebiz_order_no', po);

		//commit the record to NetSuite
		var putwrecordid = nlapiSubmitRecord(putwrecord);
		try
		{
			MoveTaskRecord(reccheckid);
		}
		catch(exp)
		{
			nlapiLogExecution('DEBUG', 'Exception in  MoveTaskRecord');
		}
		nlapiLogExecution('DEBUG', 'Done PUTW Record Insertion ', 'TRN_OPENTASK');
		nlapiLogExecution('DEBUG', 'Time Stamp at the end of custChknPutwRecCreation',TimeStampinSec());
		return true;
	}//end if binloc is not null
	else {
		//create the openTask record With CHKN Task Type
		var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating customrecord', 'TRN_OPENTASK');

		//populating the fields
		customrecord.setFieldValue('name', poValue);
		customrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		customrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);

		nlapiLogExecution('DEBUG', 'custrecord_ebiz_sku_no tesing', 'ItemId');

		customrecord.setFieldValue('custrecord_act_qty', quan);
		customrecord.setFieldValue('custrecord_expe_qty', quan);
		customrecord.setFieldValue('custrecord_lpno', LP);
		customrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		customrecord.setFieldValue('custrecord_line_no', lineno);
		customrecord.setFieldValue('custrecord_packcode', PackCode);
		customrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		customrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		customrecord.setFieldValue('custrecord_tasktype', 1); //For Check-In (CHKN)
		customrecord.setFieldValue('custrecord_actbeginloc', docklocid);
		customrecord.setFieldValue('custrecord_actendloc', stagelocid);
		customrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);
		customrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);
		customrecord.setFieldValue('custrecordact_begin_date', ActualBeginDate);
		customrecord.setFieldValue('custrecord_act_end_date', ActualEndDate);
		//Adding fields to update time zones.
		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		//customrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		customrecord.setFieldValue('custrecord_actualendtime', TimeStamp());
		customrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		customrecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
		customrecord.setFieldValue('custrecord_current_date', DateStamp());
		customrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		customrecord.setFieldValue('custrecord_wms_status_flag', 1);

		if(po != null && po != '')
			customrecord.setFieldValue('custrecord_ebiz_order_no', po);


		// Added by Phani on 03-25-2011
		// Insert the value in the WMS Location
		customrecord.setFieldValue('custrecord_wms_location', WHLocation);//

		//Added for Item name and desc
		customrecord.setFieldValue('custrecord_sku', ItemId);
		customrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" || itemrectype == "assemblyitem" ||batchflag=='T')
		{

			customrecord.setFieldValue('custrecord_batch_no', BatchNo);
			customrecord.setFieldValue('custrecord_expirydate', ExpDate);
			customrecord.setFieldValue('custrecord_fifodate', FifoDate);
			if(BatchNo !=null && BatchNo!='' && BatchNo!='null' && BatchNo!='undefined')
			{
				customrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");
			}
		}
		//cartLP
		customrecord.setFieldValue('custrecord_transport_lp', cartlp);

		var currentUserID = getCurrentUser();

		customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		customrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		customrecord.setFieldValue('custrecord_taskassignedto', currentUserID);

		nlapiLogExecution('DEBUG', 'Submitting customrecord', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var reccheckid = nlapiSubmitRecord(customrecord);
		nlapiLogExecution('DEBUG', 'Done CHKN Record Insertion :', 'Success');

		//create the openTask record With PUTW Task Type
		var putwrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating customrecord', 'TRN_OPENTASK');

		//populating the fields
		putwrecord.setFieldValue('name', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		putwrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		putwrecord.setFieldValue('custrecord_lpno', LP);
		putwrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		putwrecord.setFieldValue('custrecord_line_no', lineno);
		/* The below code is merged from FactoryMation production account on 02-28-2013 by Radhika as part of Standard bundle*/		
		if(PackCode==null || PackCode=='')
			PackCode=1;
		/* Up to here */ 
		putwrecord.setFieldValue('custrecord_packcode', PackCode);
		putwrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		putwrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		putwrecord.setFieldValue('custrecord_tasktype', 2); //For Putaway (PUTW)
		putwrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);
		putwrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);
		putwrecord.setFieldValue('custrecord_current_date', DateStamp());
		putwrecord.setFieldValue('custrecord_upd_date', DateStamp());
		//putwrecord.setFieldValue('custrecord_wms_status_flag', 6);
		putwrecord.setFieldValue('custrecord_wms_location', WHLocation);//
		putwrecord.setFieldValue('custrecord_sku', ItemId);
		putwrecord.setFieldValue('custrecord_skudesc', ItemDesc.substring(0, 200));
		nlapiLogExecution('DEBUG', 'qcRuleCheck', qcRuleCheck);
		//Status flag .
		if(qcRuleCheck)
		{
			putwrecord.setFieldValue('custrecord_wms_status_flag', 23);
		}
		else
		{ 
			putwrecord.setFieldValue('custrecord_wms_status_flag', 6);

		}
		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" ||itemrectype == "assemblyitem"  ||batchflag=='T')
		{
			putwrecord.setFieldValue('custrecord_batch_no', BatchNo);
			putwrecord.setFieldValue('custrecord_expirydate', ExpDate);
			putwrecord.setFieldValue('custrecord_fifodate', FifoDate);
			if(BatchNo !=null && BatchNo!='' && BatchNo!='null' && BatchNo!='undefined')
			{
				putwrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");
			}

		}
		//cartLP
		putwrecord.setFieldValue('custrecord_transport_lp', cartlp);

		var currentUserID = getCurrentUser();

		putwrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		putwrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		putwrecord.setFieldValue('custrecord_taskassignedto', currentUserID);
		if(putmethod!=null && putmethod!='')
			putwrecord.setFieldValue('custrecord_ebizmethod_no', putmethod);
		if(putrule!=null && putrule!='')
			putwrecord.setFieldValue('custrecord_ebizrule_no', putrule);

		if(po != null && po != '')
			putwrecord.setFieldValue('custrecord_ebiz_order_no', po);

		nlapiLogExecution('DEBUG', 'Submitting customrecord', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(putwrecord);
		/*try
		{
			MoveTaskRecord(reccheckid);
		}
		catch(exp)
		{
			nlapiLogExecution('DEBUG', 'Exception in  MoveTaskRecord');
		}*/
		nlapiLogExecution('DEBUG', 'Done CHKN Record Insertion :', 'Success');
		nlapiLogExecution('DEBUG', 'Time Stamp at the end of custChknPutwRecCreation',TimeStampinSec());
		return false;
	}
}

function getDockLocation(site){
	var retDockLocation = "";

	/*
	 * To fetch dock location,
	 *   Search customrecord_ebiznet_location for all location whose
	 *   location group id has location type as 'DOCK' and location group type as 'INBOUND' or 'BOTH'
	 */
	var dockFilters = new Array();
	// LOCATION TYPE = DOCK
	dockFilters.push(new nlobjSearchFilter('custrecord_grplocationtype', 'custrecord_inboundlocgroupid', 'is', '3'));

	// LOCATION GROUP TYPE = INBOUND or BOTH
	dockFilters.push(new nlobjSearchFilter('custrecord_locgrouptype', 'custrecord_inboundlocgroupid', 'anyof', ['1', '2']));

	if(site!=null && site!="")
		dockFilters.push(new nlobjSearchFilter('custrecord_ebizsiteloc', 'custrecord_inboundlocgroupid', 'anyof', ['@NONE@',site]));

	// Results for inbound staging location
	var dockLocationResults = nlapiSearchRecord('customrecord_ebiznet_location', null, dockFilters);

	// This search could return more than one inbound staging location.  We are using the first one.
	if(dockLocationResults != null)
		retDockLocation = dockLocationResults[0].getId();

	return retDockLocation;
}

function rf_checkin(pointid, itemid, itemdesc, itemstatus, itempackcode, quantity, invtlp, whLocation,
		batchno, mfgdate, expdate, bestbeforedate, lastdate, fifodate, fifocode,trantype,stagelocid){
	nlapiLogExecution('DEBUG', 'inside rf_checkin', 'Success');

	nlapiLogExecution('DEBUG', 'whLocation', whLocation);
	nlapiLogExecution('DEBUG', 'PO Internal ID', pointid);
	nlapiLogExecution('DEBUG', 'trantype', trantype);

	if(whLocation==null || whLocation=="")
	{

		if(trantype=='transferorder')
		{
			whLocation = nlapiLookupField(trantype, pointid, 'transferlocation');
		}
		else
		{
			whLocation = nlapiLookupField(trantype, pointid, 'location');
		}

		nlapiLogExecution('DEBUG', 'whLocation (from lookup)', whLocation);
	}

	/*
	 * Get the stage location. This will be the bin location to which the item will be 
	 * placed at the time of check-in. The same data is to be inserted in inventory record after it is
	 * done in opentask record.
	 */
	//var stagelocid;

	//stagelocid = getStagingLocation();
	nlapiLogExecution('DEBUG', 'Stage Location', stagelocid);

	/*
	 * To create inventory record.
	 */
	var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');

	nlapiLogExecution('DEBUG', 'Creating Inventory Record', 'INVT');
	/* The below code is merged from FactoryMation production account on 02-28-2013 by Radhika as part of Standard bundle*/
	var filtersAccount = new Array();
	if(whLocation!=null && whLocation!=""){
		filtersAccount.push(new nlobjSearchFilter('custrecord_location', null, 'anyof', whLocation));
	}

	/* Up to here */ 
	var accountNumber = "";

	invtRec.setFieldValue('name', pointid);
	invtRec.setFieldValue('custrecord_ebiz_inv_binloc', stagelocid);
	invtRec.setFieldValue('custrecord_ebiz_inv_lp', invtlp);
	invtRec.setFieldValue('custrecord_ebiz_inv_sku', itemid);
	invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
	/* The below code is merged from FactoryMation production account on 02-28-2013 by Radhika as part of Standard bundle*/	
	if(itempackcode==null || itempackcode=='')
		itempackcode=1;
	/* Up to here */ 
	invtRec.setFieldValue('custrecord_ebiz_inv_packcode', itempackcode);
	invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(quantity).toFixed(4));
	invtRec.setFieldValue('custrecord_ebiz_inv_loc', whLocation);
	invtRec.setFieldValue('custrecord_inv_ebizsku_no', itemid);
	invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
	invtRec.setFieldValue('custrecord_wms_inv_status_flag','17');
	invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
	invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(quantity).toFixed(4));
	invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemdesc);
	invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);
	invtRec.setFieldValue('custrecord_invttasktype', 1);
	if(pointid!=null && pointid!="")
		invtRec.setFieldValue('custrecord_ebiz_transaction_no', pointid);

	nlapiLogExecution('DEBUG', 'batchno', batchno);

	if(batchno!=null && batchno!='')
	{
		nlapiLogExecution('DEBUG', 'batchno into if', batchno);
		try
		{
			//Added on 18/06/12 by suman.
			//since custrecord_ebiz_in_lot is a list need to pass InternalId.
			var filterBatch=new Array();
			filterBatch[0]=new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',batchno);
			if(itemid!=null&&itemid!="")
				filterBatch[1]=new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',itemid);
			var rec=nlapiSearchRecord('customrecord_ebiznet_batch_entry',null,filterBatch,null);
			var BatchID=rec[0].getId();
			//end of code additon as on 18/06/12.
			invtRec.setFieldValue('custrecord_ebiz_inv_lot', BatchID);
			invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
			invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifodate);
		}
		catch(exp)
		{
			nlapiLogExecution('DEBUG', 'Exception', exp);
		}
	}

	var invtRecordId = nlapiSubmitRecord(invtRec, false, true);
//	}
//	else
//	{
//	response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, "Invalid Account #");
//	//nlapiLogExecution('DEBUG', 'Entered Item LP', getItemLP);
//	}
	nlapiLogExecution('DEBUG', 'After Submitting invtrecid', invtRecordId);
	if (invtRecordId != null) {
		nlapiLogExecution('DEBUG', 'Inventory record creation is successful', invtRecordId);
	}
	else {
		nlapiLogExecution('DEBUG', 'Inventory record creation is failure', 'Fail');
	}
} 

function getStagingLocation(site){
	var retInboundStagingLocn = "";

	/*
	 * To fetch inbound staging location,
	 *   Search customrecord_ebiznet_location for all location whose
	 *   location group id has location type as 'STAGE' and location group type as 'INBOUND' or 'BOTH'
	 */
	var stageFilters = new Array();
	// LOCATION TYPE = STAGE
	stageFilters.push(new nlobjSearchFilter('custrecord_grplocationtype', 'custrecord_inboundlocgroupid', 'is', '8'));
	stageFilters.push(new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'is', '8'));

	// LOCATION GROUP TYPE = INBOUND or BOTH
	stageFilters.push(new nlobjSearchFilter('custrecord_locgrouptype', 'custrecord_inboundlocgroupid', 'anyof', ['1', '2']));

	//code added on 5Mar By suman
	//Searching for the record with the site id and lowest seqNo.

	//SITE
	if(site!=null && site!="")
		stageFilters.push(new nlobjSearchFilter('custrecord_ebizsiteloc', 'custrecord_inboundlocgroupid', 'anyof', ['@NONE@',site]));

	stageFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	//SEQUENCE NO.
	var stageColumn=new Array();
	stageColumn[0]=new nlobjSearchColumn('custrecord_sequenceno','custrecord_inboundlocgroupid');
	stageColumn[0].setSort();

	// Results for inbound staging location
	var stageLocnResults = nlapiSearchRecord('customrecord_ebiznet_location', null, stageFilters, stageColumn);

	//end of modifing code as of 5Mar.

	// This search could return more than one inbound staging location.  We are using the first one.
	if(stageLocnResults != null)
		retInboundStagingLocn = stageLocnResults[0].getId();

	return retInboundStagingLocn;
}

function getQCRule(Item)
{
	var fields = ['custitem_item_family','custitem_item_group','custitem_ebizdefskustatus','custitem_item_info_1','custitem_item_info_2','custitem_item_info_3'];

	var columns= nlapiLookupField('item',Item,fields);
	var ItemFamily = columns.custitem_item_family;
	var ItemGroup = columns.custitem_item_group;
	//nlapiLogExecution('DEBUG', 'ItemStatus', ItemStatus);

	var ItemStatus = columns.custitem_ebizdefskustatus;
	var ItemInfo1 = columns.custitem_item_info_1;
	var ItemInfo2 = columns.custitem_item_info_2;
	var ItemInfo3 = columns.custitem_item_info_3;



	var i=0;
	var filters = new Array();

	var ItemFamArr = new Array();
	ItemFamArr[0] = ItemFamily;
	ItemFamArr[1] = '@NONE@';

	if(ItemFamily!=null && ItemFamily != "")
	{

		nlapiLogExecution('DEBUG','Into ItemFamily',ItemFamily);


		filters.push(new nlobjSearchFilter('custrecord_qcrules_item_family', null, 'anyof', ['@NONE@', ItemFamily]));

	}
	if(ItemGroup!=null && ItemGroup != "")
	{

		nlapiLogExecution('DEBUG','Into ItemGroup',ItemGroup);

		filters.push(new nlobjSearchFilter('custrecord_qcrules_item_group', null, 'anyof', ['@NONE@', ItemGroup]));

	}
	if(ItemStatus!=null && ItemStatus != "")
	{
		nlapiLogExecution('DEBUG','Into ItemStatus',ItemStatus);

		filters.push(new nlobjSearchFilter('custrecord_qcrules_item_status', null, 'anyof', ['@NONE@', ItemStatus]));

	}	 
	if(Item != null && Item != "")
	{
		nlapiLogExecution('DEBUG','Into Item',Item);

		filters.push(new nlobjSearchFilter('custrecord_qcrules_item', null, 'anyof', ['@NONE@', Item]));

	}

	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));



	var t1 = new Date();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_qc_rules', null, filters, null);
	if(searchresults !=null && searchresults != '' && searchresults.length>0)
	{
		//	return false;
		return true; // return False beacuase QCRules checking is not required
	}	
	else
		return false;
}
