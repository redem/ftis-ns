/***************************************************************************
			eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Suitelet/Attic/ebiz_PutawayConfirmSerial_SL.js,v $
 *     	   $Revision: 1.1.2.3.6.1 $
 *     	   $Date: 2015/12/02 15:06:59 $
 *     	   $Author: skreddy $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_PutawayConfirmSerial_SL.js,v $
 * Revision 1.1.2.3.6.1  2015/12/02 15:06:59  skreddy
 * case# 201415885
 * 2015.2 issue fix
 *
 * Revision 1.1.2.3  2013/10/25 16:12:43  skreddy
 * Case# 20125113
 * standard bundle  issue fix
 *
 * Revision 1.1.2.2  2013/09/24 15:41:34  rmukkera
 * Case# 20124543�
 *
 * Revision 1.1.2.1  2013/05/08 15:21:07  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle Issue Fixes
 *
 * Revision 1.17.2.9.4.3.2.4  2013/04/24 15:20:31  grao
 * CASE201112/CR201113/LOG201121
/**********************************************
 */
function PutawayConfirmSerialNoSelect(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Serial Number Selection');
		var vQbSKU="";
		var vQbLPNo="";
		var vQbQty="";	
		var vopentask="";
		var vcontlp="";
		var vdointernid="";
		var vdoLineno="";	
		var binlocation=''

		if(request.getParameter('custparam_serialskuid')!=null && request.getParameter('custparam_serialskuid')!="")
		{
			vQbSKU = request.getParameter('custparam_serialskuid');			

		}        

		if(request.getParameter('custparam_serialskulp')!=null && request.getParameter('custparam_serialskulp')!="")
		{
			vQbLPNo=request.getParameter('custparam_serialskulp');
		}
		if(request.getParameter('custparam_serialskuchknqty')!=null && request.getParameter('custparam_serialskuchknqty')!="")
		{
			vQbQty=request.getParameter('custparam_serialskuchknqty');
		}
		if(request.getParameter('custparam_opentaskid')!=null && request.getParameter('custparam_opentaskid')!="")
		{
			vopentask=request.getParameter('custparam_opentaskid');
		}
		if(request.getParameter('custparam_contLpno')!=null && request.getParameter('custparam_contLpno')!="")
		{
			vcontlp=request.getParameter('custparam_contLpno');
		}
		if(request.getParameter('custparam_dointernno')!=null && request.getParameter('custparam_dointernno')!="")
		{
			vdointernid=request.getParameter('custparam_dointernno');
		}
		if(request.getParameter('custparam_dolineno')!=null && request.getParameter('custparam_dolineno')!="")
		{
			vdoLineno=request.getParameter('custparam_dolineno');
		}
		
		//case 20125113 start :added binlocation
		if(request.getParameter('custparam_binlocation')!=null && request.getParameter('custparam_binlocation')!="")
		{
			binlocation=request.getParameter('custparam_binlocation');
		}
		//case 20125113 end :
		form.setScript('customscript_putwaycnfmserialsave_cl') ;	
		var opentaskField = form.addField('custpage_opentask', 'text', 'Opentask');
		opentaskField.setDefaultValue(vopentask);
		opentaskField.setDisplayType('hidden'); 
		var contLpField = form.addField('custpage_contlp', 'text', 'ContLp');
		contLpField.setDefaultValue(vcontlp);
		contLpField.setDisplayType('hidden'); 

		var doField = form.addField('custpage_doid', 'text', 'do');
		doField.setDefaultValue(vdointernid);
		doField.setDisplayType('hidden');

		var dolineField = form.addField('custpage_dolineno', 'text', 'do line');
		dolineField.setDefaultValue(vdoLineno);
		dolineField.setDisplayType('hidden');

		var ActualQtyField = form.addField('custpage_actualqty', 'text', 'actual qty');
		ActualQtyField.setDefaultValue(vQbQty);
		ActualQtyField.setDisplayType('hidden');
		//case 20125113 start :added binlocation
		var BinlocationField = form.addField('custpage_binlocation', 'text', 'binlocation');
		BinlocationField.setDefaultValue(binlocation);
		BinlocationField.setDisplayType('hidden');

		//case 20125113 end :
		var sublist = form.addSubList("custpage_items", "list", "ItemList");
		sublist.addField("custpage_select", "checkbox", "Confirm").setDefaultValue('F'); 
		sublist.addField("custpage_serialno", "text", "Serial #");
		sublist.addField("custpage_serialinternid", "text", "Serial #").setDisplayType('hidden');			

		var filters = new Array();				
		//filters[0] = new nlobjSearchFilter('custrecord_serialitem', null, 'equalto', vQbSKU); 	

		filters[0] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', vQbLPNo);
		filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);


		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_serialnumber');

		var searchresults = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);

		var vSerialNo;
		for (var i = 0; searchresults != null && i < searchresults.length; i++) {
			var searchresult = searchresults[i];

			vSerialNo = searchresult.getValue('custrecord_serialnumber');           
			form.getSubList('custpage_items').setLineItemValue('custpage_serialno', i + 1, vSerialNo);           
			form.getSubList('custpage_items').setLineItemValue('custpage_serialinternid', i + 1, searchresult.getId());

		}
		form.addSubmitButton('Confirm');

		response.writePage(form);
	}
	else //this is the POST block
	{

	}
}
	
function putawaycnfmSerialRecSave()
{

	//try {
	var cnt = nlapiGetLineItemCount('custpage_items');
	//alert("Count : " + cnt);
	var opentaskId = nlapiGetFieldValue('custpage_opentask');
	var contLpno = nlapiGetFieldValue('custpage_contlp');
	var doNo = nlapiGetFieldValue('custpage_doid');
	var dolineno = nlapiGetFieldValue('custpage_dolineno');
	var Newbinlocation = nlapiGetFieldValue('custpage_binlocation');
	var vbatchno="";
	//Case # 20124543�Start (system is accepting the serial numbers as less  qty  while doing qty exception through UI for RMA.) 

	var qty=nlapiGetFieldValue('custpage_actualqty');
	var selectedSerialsCount=0;
	for (var p1 = 1; p1 <= cnt; p1++) {
		var chkselect = nlapiGetLineItemValue('custpage_items', 'custpage_select', p1);

		if (chkselect == 'T') {
			selectedSerialsCount=parseInt(selectedSerialsCount)+parseInt(1);
		}
	}
	if(selectedSerialsCount ==0)
	{
		alert("Please select Serial Numbers");
		return false;
	}
	if(selectedSerialsCount!=0 && qty!=null && qty!='' && parseInt(qty)!=parseInt(selectedSerialsCount))
	{
		alert("Please select "+qty +" Serails");
		return false;
	}
	//Case # 20124543 End.
	for (var p = 1; p <= cnt; p++) {
		var chkselect = nlapiGetLineItemValue('custpage_items', 'custpage_select', p);
		var lineinternno = nlapiGetLineItemValue('custpage_items', 'custpage_serialinternid', p);
		var vserialno = nlapiGetLineItemValue('custpage_items', 'custpage_serialno', p);			
		if (chkselect == 'T') {

			/*var fields = new Array();
				var values = new Array();
				fields[0] = 'custrecord_serialcontlpno';
				values[0] = contLpno;
				fields[1] = 'custrecord_serialsono';
				values[1] = doNo;
				fields[2] = 'custrecord_serialsolineno';
				values[2] = dolineno;
				var updatefields = nlapiSubmitField('customrecord_ebiznetserialentry', lineinternno, fields, values);
				alert(updatefields);
				*/				 
				  var trnserial = nlapiLoadRecord('customrecord_ebiznetserialentry', lineinternno);
				  //trnserial.setFieldValue('custrecord_serialcontlpno', contLpno);
				  trnserial.setFieldValue('custrecord_serialebizpono', doNo);
				  trnserial.setFieldValue('custrecord_serialstatus', 'S');	
				//  trnserial.setFieldValue('custrecord_serialstatus', 'P');	
				  trnserial.setFieldValue('custrecord_serialpolineno', dolineno);
				 // alert('Updation');
				 var retval  =nlapiSubmitRecord(trnserial, true);
				 
				   //alert(retval);				  
				if (vbatchno == "") {
					vbatchno = vserialno;
				}
				else {
					vbatchno = vbatchno + "," + vserialno;
				}
			}
			else
				{
				 var trnserial = nlapiLoadRecord('customrecord_ebiznetserialentry', lineinternno);
				  //trnserial.setFieldValue('custrecord_serialcontlpno', contLpno);
				  trnserial.setFieldValue('custrecord_serialebizpono', doNo);
				 // trnserial.setFieldValue('custrecord_serialstatus', 'D');		
				  
				  trnserial.setFieldValue('custrecord_serialstatus', 'P');	
				  
				 trnserial.setFieldValue('custrecord_serialsolineno', dolineno);
				 // alert('Updation');
				 var retval  =nlapiSubmitRecord(trnserial, true);
				
				}
		}
		
		 var opentask = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', opentaskId);
	var oldbinlocation = opentask.getFieldValue('custrecord_actbeginloc');
		 opentask.setFieldValue('custrecord_serial_no', vbatchno);
	//case 20125113 start :for qty exception and location exception
	if(Newbinlocation !=null && Newbinlocation !='')
	{
		if(parseInt(oldbinlocation) != parseInt(Newbinlocation))
			opentask.setFieldValue('custrecord_actbeginloc', Newbinlocation);
	}
	//case 20125113 end
		// alert(opentaskId);
		// alert(vbatchno);
		  var opentskretval  =nlapiSubmitRecord(opentask, true);
		   alert('Updated Successfully');
		  //window.opener.document.location.reload(true);
		  window.opener.location.reload();
		  window.close(); 
		  

/*alert('vbatchno  ' + vbatchno);
var fieldsinvt = new Array();
var valuesinvt = new Array();
fieldsinvt[0]='custrecord_batch_no';
valuesinvt[0] = vbatchno;
var updatefieldsinvt = nlapiSubmitField('customrecord_ebiznetserialentry', lineinternno, fieldsinvt, valuesinvt);
alert(updatefieldsinvt);
*/
	/*}
	
	catch(exp)
	{
		alert('Into Exception. '+exp);
		return false;
	}*/
	return true;
}
