
/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/UserEvents/ebiz_AfterShipmanifestTrackingnoUpdate_UE.js,v $
 *     	   $Revision: 1.22.2.23.4.6.2.26.2.4 $
 *     	   $Date: 2015/11/27 11:48:15 $
 *     	   $Author: svanama $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_192 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_AfterShipmanifestTrackingnoUpdate_UE.js,v $
 * Revision 1.22.2.23.4.6.2.26.2.4  2015/11/27 11:48:15  svanama
 * Case # 201414979 Printing Pack label from Bartender
 *
 * Revision 1.22.2.23.4.6.2.26.2.3  2015/11/16 22:58:50  sponnaganti
 * 201415701 201415702
 * 2015.2 issue fix
 *
 * Revision 1.22.2.23.4.6.2.26.2.2  2015/11/04 23:37:57  sponnaganti
 * case# 201415375
 * 2015.2 issue fix
 *
 * Revision 1.22.2.23.4.6.2.26.2.1  2015/09/23 14:57:36  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.22.2.23.4.6.2.26  2015/07/17 13:26:30  schepuri
 * case# 201413549
 *
 * Revision 1.22.2.23.4.6.2.25  2015/06/30 16:36:37  mvarre
 * Case# 201413187
 * MHP Kit item pick qty and ship qty issue
 *
 * Revision 1.22.2.23.4.6.2.24  2015/05/12 07:45:49  skreddy
 * Case# 201412599
 * Invoice generation
 *
 * Revision 1.22.2.23.4.6.2.23  2015/04/14 13:52:28  schepuri
 * case# 201412199
 *
 * Revision 1.22.2.23.4.6.2.22  2015/04/13 10:02:42  snimmakayala
 * no message
 *
 * Revision 1.22.2.23.4.6.2.21  2015/03/23 14:35:09  snimmakayala
 * no message
 *
 * Revision 1.22.2.23.4.6.2.20  2015/01/29 07:27:12  snimmakayala
 * Case#: 201411485
 *
 * Revision 1.22.2.23.4.6.2.19  2014/11/26 14:34:52  schepuri
 *  201411093�  issue fix
 *
 * Revision 1.22.2.23.4.6.2.18  2014/09/26 15:11:11  sponnaganti
 * Case# 201410526
 * Stnd Bundle Issue fix
 *
 * Revision 1.22.2.23.4.6.2.17  2014/07/29 16:01:11  sponnaganti
 * Case# 20149571
 * Stnd Bundle Issue fix
 *
 * Revision 1.22.2.23.4.6.2.16  2014/06/10 13:15:40  rmukkera
 * Case # 20148832
 * VRA functionality added
 *
 * Revision 1.22.2.23.4.6.2.15  2014/05/27 14:59:07  grao
 * Case#: 20148539  & 20148537  Standard  issue fixes
 *
 * Revision 1.22.2.23.4.6.2.14  2014/03/28 15:33:22  skavuri
 * Case # 20127782 issue fixed
 *
 * Revision 1.22.2.23.4.6.2.13  2014/01/20 15:48:14  gkalla
 * case#20126871 �
 * PMM outbound stage not deleting
 *
 * Revision 1.22.2.23.4.6.2.12  2014/01/10 10:36:23  grao
 * Case# 20126724� related issue fixes in Sb issue fixes
 *
 * Revision 1.22.2.23.4.6.2.11  2013/12/05 11:50:16  snimmakayala
 * no message
 *
 * Revision 1.22.2.23.4.6.2.10  2013/09/20 08:23:57  svanama
 * Case# 20124513
 * Itemfullfillment updation until last carton shipped in shipmanifest customrecord
 *
 * Revision 1.22.2.23.4.6.2.8  2013/08/19 16:38:02  grao
 * Standard bundle issues fixes :20123880 System should update the SO line with  Fulfilled qty
 *
 * Revision 1.22.2.23.4.6.2.7  2013/06/18 15:06:58  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Departtrailerissuefix
 *
 * Revision 1.22.2.23.4.6.2.6  2013/06/18 14:12:21  mbpragada
 * 20120490
 *
 * Revision 1.22.2.23.4.6.2.5  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.22.2.23.4.6.2.4  2013/04/29 15:01:06  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.22.2.23.4.6.2.3  2013/04/04 20:20:53  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixes
 *
 * Revision 1.22.2.23.4.6.2.2  2013/03/19 11:45:09  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.22.2.23.4.6.2.1  2013/03/06 09:48:26  gkalla
 * CASE201112/CR201113/LOG201121
 * Lexjet Files merging for Standard bundle
 *
 * Revision 1.22.2.23.4.6  2013/02/21 14:32:53  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.22.2.23.4.5  2013/01/21 15:51:53  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA-BuildShip Unit Chnages.
 *
 * Revision 1.22.2.23.4.4  2013/01/17 06:00:28  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Stage Inventory deletion for LTL orders.
 * .
 *
 * Revision 1.22.2.23.4.3  2012/12/12 17:46:03  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * aftershipmanifest issue fix
 *
 * Revision 1.22.2.23.4.2  2012/12/04 16:33:45  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA UAT Fixes.
 *
 * Revision 1.22.2.23.4.1  2012/11/01 14:54:56  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.22.2.23  2012/09/03 19:09:37  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Fisk Production Issue Fixes
 *
 * Revision 1.22.2.22  2012/08/27 07:35:46  svanama
 * CASE201112/CR201113/LOG201
 * issues fixed for trackingnumber and actuweight updatation under packages tab
 *
 * Revision 1.22.2.21  2012/08/25 17:28:58  svanama
 * CASE201112/CR201113/LOG201
 * Code changes for popCode changes in update shipcost to ItemFulfillment
 *
 * Revision 1.22.2.20  2012/07/30 16:48:11  gkalla
 * CASE201112/CR201113/LOG201121
 * Changes of email alerts
 *
 * Revision 1.22.2.19  2012/07/23 08:02:50  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * After shipmanifest shipping issue fix
 *
 * Revision 1.22.2.18  2012/07/10 23:10:48  gkalla
 * CASE201112/CR201113/LOG201121
 * Alert Mails
 *
 * Revision 1.22.2.17  2012/07/10 06:50:05  spendyala
 * CASE201112/CR201113/LOG201121
 * NSconfirmation field will be updated when posted to NS.
 *
 * Revision 1.22.2.16  2012/07/04 18:47:49  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Need to consider the tasks for which item fulfillment is successfully sent.
 *
 * Revision 1.22.2.15  2012/06/21 10:15:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 * Revision 1.22.2.14  2012/06/11 22:27:02  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT and Prod issue fixes
 *
 * Revision 1.22.2.13  2012/05/25 22:25:26  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.22.2.12  2012/05/22 15:40:15  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.22.2.11  2012/05/10 23:34:45  gkalla
 * CASE201112/CR201113/LOG201121
 * Shipmanifest with multiple item fulfillments
 *
 * Revision 1.22.2.10  2012/05/09 07:52:37  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Partial Status Handling
 *
 * Revision 1.22.2.9  2012/04/17 08:40:17  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pool of records
 *
 * Revision 1.22.2.8  2012/03/20 20:42:32  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Moved from Trunk to Branch
 *
 * Revision 1.32  2012/03/20 18:49:45  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Open Task to Closed Task movement
 *
 * Revision 1.31  2012/03/16 10:06:13  schepuri
 * CASE201112/CR201113/LOG201121
 * code merge from 1.22.2.7
 *
 * Revision 1.30  2012/03/13 15:56:48  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TPP Shipping Cost Changes
 *
 * Revision 1.29  2012/02/24 15:04:42  rmukkera
 * CASE201112/CR201113/LOG201121
 *   code added for quickship in the edit mode
 *
 * Revision 1.28  2012/02/16 00:52:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.27  2012/02/07 14:32:04  snimmakayala
 * CASE201112/CR201113/LOG201121
 * quickship issue fixes
 *
 * Revision 1.26  2012/02/06 14:20:30  rmukkera
 * CASE201112/CR201113/LOG201121
 *   QuickShip code modified
 *
 * Revision 1.25  2012/02/02 14:11:32  rmukkera
 * CASE201112/CR201113/LOG201121
 *   new code added in the else part of create mode for quick ship purpose.
 *
 * Revision 1.24  2012/01/30 07:05:05  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.23  2012/01/27 10:33:16  snimmakayala
 * CASE201112/CR201113/LOG201121
 * outbound inventory deletion.
 *
 * Revision 1.22  2011/12/22 18:42:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Shipping
 *
 * Revision 1.21  2011/12/05 14:58:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.20  2011/11/28 14:03:37  svanama
 * CASE201112/CR201113/LOG201121
 * Modifications are changed in updateshipqty
 *
 * Revision 1.19  2011/11/28 13:25:30  svanama
 * CASE201112/CR201113/LOG201121
 * two functions are added in the  DeleteItemfulfillmentLine and CreateShipmanifestRecord added i
 *
 * Revision 1.18  2011/11/28 09:36:30  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Ship Date update in fulfillment order line
 *
 * Revision 1.17  2011/11/11 17:20:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Inventory Deletion
 *
 * Revision 1.16  2011/11/04 11:03:25  svanama
 * CASE201112/CR201113/LOG201121
 * InsertExceptionLog  is added in try catch block ..
 *
 * Revision 1.15  2011/11/02 15:34:16  snimmakayala
 * CASE201112/CR201113/LOG201121
 * update ship qty against the fulfillment order line
 *
 * Revision 1.14  2011/11/02 10:22:42  snimmakayala
 * CASE201112/CR201113/LOG201121
 * update ship qty against the fulfillment order line
 *
 * Revision 1.13  2011/10/25 17:03:22  gkalla
 * CASE201112/CR201113/LOG201121
 * for shipmanifest LTL carrier type
 *
 * Revision 1.12  2011/10/24 22:18:36  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Inventory deletion for LTL
 *
 * Revision 1.11  2011/10/19 14:07:27  rmukkera
 * CASE201112/CR201113/LOG201121
 * changed code for ltl carrier
 *
 * Revision 1.10  2011/10/18 20:17:09  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Clear Outbound staging Inventory
 *
 * Revision 1.9  2011/10/11 08:09:43  skdokka
 * CASE201112/CR201113/LOG201121
 * Merged TNT changes
 *
 * Revision 1.8  2011/10/05 12:58:30  rmukkera
 * CASE201112/CR201113/LOG201121
 * added new functionality for LTL carrier
 *
 * Revision 1.7  2011/09/26 13:48:25  rmukkera
 * CASE201112/CR201113/LOG201121
 * bug fixed
 *
 * Revision 1.6  2011/09/26 11:28:49  rmukkera
 * CASE201112/CR201113/LOG201121
 * trackingno condition added in if statement
 *
 * Revision 1.5  2011/09/26 08:45:19  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.4  2011/09/22 07:07:54  rmukkera
 * CASE201112/CR201113/LOG201121
 * code  modification in calling item fullfillment
 *
 * Revision 1.3  2011/09/21 14:44:43  rmukkera
 * CASE201112/CR201113/LOG201121
 * code  modification in calling transform record
 *
 * Revision 1.2  2011/09/16 07:27:07  rmukkera
 * CASE201112/CR201113/LOG201121
 * source code changes included both open and close tasks
 *
 *
 * added cvs header
 *

 *****************************************************************************/




var dataNotFoundMsg="";	var vSiteId="";var vCompId="";var VQbwaveno="";
function AfterShipmanifestTrackingnoUpdateUE(type)
{

	var userId;
	userId = nlapiGetUser();
	var ruleid = 'Creation of Ship Manifest for LTL';

	var vShippingRule=getSystemRuleValue(ruleid);

	var FULFILLMENTATORDERLEVEL=getSystemRuleValue('IF001');

	nlapiLogExecution('DEBUG', 'FULFILLMENTATORDERLEVEL',FULFILLMENTATORDERLEVEL);

	if(type=='create' && vShippingRule!='BuildShip')
	{
		nlapiLogExecution('DEBUG', 'Into Create');
		var vorderno="";
		var vebizcontainerlp="";
		var vcarrier ='';

		var newRecord = nlapiGetNewRecord();
		vcarrier =nlapiGetFieldValue('custrecord_ship_carrier');
		vorderno =nlapiGetFieldValue('name');

		nlapiLogExecution('DEBUG', 'vorderno ', vorderno);
		nlapiLogExecution('DEBUG', 'vcarrier ', vcarrier);
		if(vcarrier=="LTL")
		{
			nlapiLogExecution('DEBUG', 'Into LTL');

			try{
				//case# 20149751 starts(Changing vsalesOrderId to vorderno)
				//var trantype = nlapiLookupField('transaction', vsalesOrderId, 'recordType');
				var trantype = nlapiLookupField('transaction', vorderno, 'recordType');
				//case# 20149751 ends
				nlapiLogExecution('Debug', 'trantype', trantype);

				if(trantype != "vendorreturnauthorization")
				{


//					var vorderno =nlapiGetFieldValue('custrecord_ship_orderno');

					vebizcontainerlp =nlapiGetFieldValue('custrecord_ship_contlp');
					nlapiLogExecution('DEBUG', 'vordernoLTL', vorderno);
					nlapiLogExecution('DEBUG', 'vebizcontainerlpLTL', vebizcontainerlp);
					if(vorderno!="" && vorderno!=null)
					{
						var trackingno;
						var shipcharges;
						var pakageweight;

						trackingno = nlapiGetFieldValue('custrecord_ship_trackno');
						shipcharges =nlapiGetFieldValue('custrecord_ship_charges');
						pakageweight=nlapiGetFieldValue('custrecord_ship_actwght');

						var filters = new Array();
						filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'is',vorderno));
						filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));
						filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isnotempty'));
						filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [14]));//depart // case# 201412199
						var columns= new Array();
						columns[0] = new nlobjSearchColumn('custrecord_ebiz_nsconfirm_ref_no');
						columns[1] = new nlobjSearchColumn('name');
						columns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
						columns[3] = new nlobjSearchColumn('custrecord_ship_lp_no');
						columns[4] = new nlobjSearchColumn('custrecord_line_no');
						columns[5] = new nlobjSearchColumn('custrecord_act_qty');
						var transactionid;
						var opentasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters ,columns);


						nlapiLogExecution('DEBUG', 'trackingno2', trackingno);
						nlapiLogExecution('DEBUG', 'pakageweight2', pakageweight);
						nlapiLogExecution('DEBUG', 'shipcharges2 ', shipcharges);

						if(opentasksearchresults !=null)
						{
							for (var j = 0; j < opentasksearchresults.length; j++)
							{

								nlapiLogExecution('DEBUG', 'opentasksearchresults.length', opentasksearchresults.length);
								//var wmslocation=opentasksearchresults[0].getValue('custrecord_wms_location');
								transactionid=opentasksearchresults[j].getValue('custrecord_ebiz_nsconfirm_ref_no');
								//var vebizcontainerlp=opentasksearchresults[0].getValue('custrecord_ebiztask_ship_lp_no');
								nlapiLogExecution('DEBUG', 'vebizcontainerlp ', vebizcontainerlp);
								nlapiLogExecution('DEBUG', 'transactionid ', transactionid);
								//nlapiLogExecution('DEBUG', 'NS Confirmation #2 ', transactionid);
								if(transactionid!=null && transactionid!="")
								{
									var OpenTaskTransaction = nlapiLoadRecord('itemfulfillment', transactionid);
									OpenTaskTransaction.setFieldValue('shipstatus','C'); //Statusflag='C'
									var lastcharges =OpenTaskTransaction.getFieldValue('shippingcost');
									nlapiLogExecution('DEBUG', 'shipcharges ', lastcharges);
									var totalshipcharges=parseFloat(lastcharges)+parseFloat(shipcharges);
									OpenTaskTransaction.setFieldValue('generateintegratedshipperlabel','F');
									//OpenTaskTransaction.setFieldValue('shippingcost',totalshipcharges);
									OpenTaskTransaction.setFieldValue('custbody_fulfill_shipcost',totalshipcharges);
									OpenTaskTransaction.selectNewLineItem('package');
									OpenTaskTransaction.setCurrentLineItemValue('package', 'packagedescr', vebizcontainerlp);
									OpenTaskTransaction.setCurrentLineItemValue('package', 'packagetrackingnumber', trackingno);
									OpenTaskTransaction.setCurrentLineItemValue('package', 'packageweight',pakageweight);
									//OpenTaskTransaction.setCurrentLineItemValue('item', 'location',wmslocation);
									OpenTaskTransaction.commitLineItem('package');
									//OpenTaskTransaction.commitLineItem('item');
									var id= nlapiSubmitRecord(OpenTaskTransaction ,true);
									nlapiLogExecution('DEBUG', 'After open task submit record ', id);
									var  SalesOrderInternalId= opentasksearchresults[0].getValue('custrecord_ebiz_order_no');	
									nlapiLogExecution('DEBUG', 'SalesOrderInternalId 2', SalesOrderInternalId);


									var trantype = nlapiLookupField('transaction', SalesOrderInternalId, 'recordType');
									nlapiLogExecution('DEBUG', 'trantype', trantype);
									var ffOrdNo = opentasksearchresults[j].getValue('name');
									var ffOrdLineNo = opentasksearchresults[j].getValue('custrecord_line_no');
									var shipqty = opentasksearchresults[j].getValue('custrecord_act_qty');

									updateShipQty(ffOrdNo,ffOrdLineNo,shipqty);
									MoveTaskRecord(opentasksearchresults[j].getId());//added now
									try{
										var shippedcartons='',Pro='',BOL='',TrailerCarrier='',SCAC='';

										var containerfilters = new Array();
										containerfilters.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof',[3]));
										containerfilters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no', null, 'anyof',[vorderno]));	
										var containercolumns = new Array();
										containercolumns.push(new nlobjSearchColumn('custrecord_ebiztask_ebiz_contlp_no',null,'group' ));
										containercolumns.push(new nlobjSearchColumn('custrecord_ebiztask_ebiz_trailer_no',null,'group'));
										var contsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask' , null,containerfilters, containercolumns);
										nlapiLogExecution('ERROR', 'contsearchresults 1', contsearchresults);
										if(contsearchresults !=null && contsearchresults !='')
										{
											shippedcartons=contsearchresults.length;
											nlapiLogExecution('ERROR', 'contsearchresults 2', contsearchresults.length);
											nlapiLogExecution('ERROR', 'contsearchresults 3', contsearchresults[0].getValue('custrecord_ebiztask_ebiz_trailer_no',null,'group'));

											var trailer=contsearchresults[0].getValue('custrecord_ebiztask_ebiz_trailer_no',null,'group');
											nlapiLogExecution('ERROR', 'trailer', trailer);

											var trailerfilters = new Array();
											trailerfilters.push(new nlobjSearchFilter('name', null,'is',trailer));

											var trailercolumns = new Array();
											trailercolumns.push(new nlobjSearchColumn('custrecord_ebizpro'));
											trailercolumns.push(new nlobjSearchColumn('custrecord_ebizmastershipper'));
											trailercolumns.push(new nlobjSearchColumn('custrecord_ebizcarrierid'));
											var trlrsearchresults = nlapiSearchRecord('customrecord_ebiznet_trailer' , null,trailerfilters , trailercolumns );
											nlapiLogExecution('ERROR', 'trlrsearchresults', trlrsearchresults);
											if(trlrsearchresults!=null && trlrsearchresults!='')
											{	
												Pro = trlrsearchresults[0].getValue('custrecord_ebizpro');
												BOL = trlrsearchresults[0].getValue('custrecord_ebizmastershipper');
												TrailerCarrier= trlrsearchresults[0].getText('custrecord_ebizcarrierid');

												nlapiLogExecution('ERROR', 'Pro', Pro);
												nlapiLogExecution('ERROR', 'BOL', BOL);
												nlapiLogExecution('ERROR', 'TrailerCarrier', TrailerCarrier);
											}
											if(TrailerCarrier  != null && TrailerCarrier != ''){
												var CarServiceDetails = GetCarServiceDetails(TrailerCarrier);
												if (CarServiceDetails != null	&& CarServiceDetails.length > 0) {					
													SCAC = CarServiceDetails[0][2];
													nlapiLogExecution('ERROR', 'SCAC', SCAC);
												}
											}

											var fields = new Array();
											var values = new Array();
											var type='';

											fields.push('custbody_ebiz_so_scac');
											fields.push('custbody_ebiz_so_pro');
											fields.push('custbody_ebiz_so_bol');
											fields.push('custbody_ebiz_so_carton_cnt');
											fields.push('custbody_ebiz_so_track_type');

											if(BOL!=null && BOL!= '')
											{
												type='BM';
												Pro='';
											}
											else if(Pro!=null && Pro!= '')
											{
												type='CN';
												BOL='';
											}
											else
												type='';


											values.push(SCAC);
											values.push(Pro);
											values.push(BOL);
											values.push(shippedcartons);
											values.push(type);
											var trantypeso = nlapiLookupField('transaction', vorderno, 'recordType');
											nlapiSubmitField(trantypeso, vorderno, fields, values);

										}

									}
									catch(exp) {
										nlapiLogExecution('ERROR', 'exception while updating sale order',exp);
									}
								}
							}
						}

						var packfilters = new Array();
						packfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'is',vorderno));
						packfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['14']));

						var packcolumns= new Array();
						packcolumns[0] = new nlobjSearchColumn('custrecord_container_lp_no');

						var packtasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,packfilters ,packcolumns);
						if(packtasksearchresults !=null && packtasksearchresults !='')
						{
							nlapiLogExecution('DEBUG', 'PACK Task Results Length', packtasksearchresults.length);

							for (var i = 0; i < packtasksearchresults.length; i++) 
							{
								var vcontainerlp = packtasksearchresults[i].getValue('custrecord_container_lp_no');
								nlapiLogExecution('DEBUG', 'Calling clearOutboundInventory (LTL)', vcontainerlp);
								clearOutboundInventory(vcontainerlp);
							}
						}
					}
				}
				else
				{


					var neworderno=nlapiGetFieldValue('custrecord_ship_order');
					var fulfillmentNo=nlapiGetFieldValue('custrecord_ship_ref3');
					var newRecord = nlapiGetNewRecord();

					nlapiLogExecution('Debug', 'neworderno', neworderno);
					nlapiLogExecution('Debug', 'fulfillmentNo', fulfillmentNo);
					nlapiLogExecution('Debug', 'vebizcontainerlp', vebizcontainerlp);

					if(neworderno != null && neworderno != '')
					{	
						var ismultilineship='F';
						var trantypeso = nlapiLookupField('transaction', neworderno, 'recordType');
						nlapiLogExecution('Debug', 'trantypeso', trantypeso);
						var salesorderrec= nlapiLoadRecord(trantypeso, neworderno);
						if(salesorderrec!=null && salesorderrec!='')
							ismultilineship=salesorderrec.getFieldValue('ismultishipto');

						nlapiLogExecution('Debug', 'Item Line Shipping', ismultilineship);
						if(ismultilineship!='T')
						{
							var vNSConfirmNo=createItemFulfillment(neworderno,fulfillmentNo,'QuickShip',"","",vebizcontainerlp,"","","","",salesorderrec);
							nlapiLogExecution('DEBUG', 'vNSConfirmNo', vNSConfirmNo);

							var internalidrecord=newRecord.getId();

							if((vNSConfirmNo!=null)&&(vNSConfirmNo!=''))
							{
								nlapiLogExecution('DEBUG', 'UpdateShipManifestRecord# Foe Ship', 'UpdateShipManifestRecord# ');

								nlapiLogExecution('DEBUG', 'internalidrecord# ', internalidrecord);
								UpdateShipManifestRecord(vNSConfirmNo,internalidrecord);
								UpdateAllShipManifestRecord(vNSConfirmNo,internalidrecord,vMastLPNo,fulfillmentNo,'PC');
								nlapiLogExecution('DEBUG', 'clearOutboundInventory for PC');
								clearOutboundInventory(vebizcontainerlp);
							}
						}
						else
						{
							var vNSConfirmNo= createItemFulfillmentMultiLineShipping(neworderno,fulfillmentNo,vebizcontainerlp,shipcharges,trackingno,pakageweight,'QuickShip',vMastLPNo);
							nlapiLogExecution('DEBUG', 'vNSConfirmNo', vNSConfirmNo);

							var internalidrecord=newRecord.getId();

							if((vNSConfirmNo!=null)&&(vNSConfirmNo!=''))
							{
								nlapiLogExecution('DEBUG', 'UpdateShipManifestRecord# Foe Ship', 'UpdateShipManifestRecord# ');

								nlapiLogExecution('DEBUG', 'internalidrecord# ', internalidrecord);
								UpdateShipManifestRecord(vNSConfirmNo,internalidrecord);
								UpdateAllShipManifestRecord(vNSConfirmNo,internalidrecord,vMastLPNo,fulfillmentNo,'PC');
								nlapiLogExecution('DEBUG', 'clearOutboundInventory for PC');
								clearOutboundInventory(vebizcontainerlp);
							}
						}
					}



				}
			}
			catch(exp) {

				var exceptionname='SalesOrder';
				var functionality='ShipManifest Update';
				var trantype=2;
				nlapiLogExecution('DEBUG', 'DetailsError', functionality);	
				nlapiLogExecution('DEBUG', 'vebizcontainerlp', vebizcontainerlp);
				nlapiLogExecution('DEBUG', 'vorderno', vorderno);
				var reference2=vebizcontainerlp;
				var reference3="";
				var reference4 ="";
				var reference5 ="";

				//InsertExceptionLog(exceptionname,trantype, functionality, exp, vorderno, vebizcontainerlp,reference3,reference4,reference5, userId);
				var alertType=1;//1 for exception and 2 for report
				errSendMailByProcess(trantype,exp,vorderno,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);

				nlapiLogExecution('DEBUG', 'Exception in update Tracking Number for LTL Orders : ', exp);		
			}
		}
		else
		{
			nlapiLogExecution('DEBUG', 'Into QuickShip Orders');

			//case# 201410526 For Other tahn LTL carrier we tacking 
			//	vorderno =nlapiGetFieldValue('custrecord_ship_orderno');
			vorderno =nlapiGetFieldValue('custrecord_ship_order');// case# 201411093
			var trantypeso = nlapiLookupField('transaction', vorderno, 'recordType');
			nlapiLogExecution('Debug', 'trantypeso', trantypeso);




			try{
				if(trantypeso != "vendorreturnauthorization")
				{

//					var vorderno =nlapiGetFieldValue('custrecord_ship_orderno');
					//var newRecord = nlapiGetNewRecord();
					//vorderno =nlapiGetFieldValue('name');
					nlapiLogExecution('DEBUG', 'vorderno QuickShip', vorderno);
					var shipcontainerlp=nlapiGetFieldValue('custrecord_ship_contlp');
					nlapiLogExecution('DEBUG', 'shipcontainerlp', shipcontainerlp);
					nlapiLogExecution('DEBUG', 'intoelseofcreate', vorderno);
					vebizcontainerlp =nlapiGetFieldValue('custrecord_ship_contlp');

					nlapiLogExecution('DEBUG', 'vebizcontainerlp QuickShip', vebizcontainerlp);

					var trackingno;
					var shipcharges;
					var pakageweight;

					trackingno = nlapiGetFieldValue('custrecord_ship_trackno');
					shipcharges =nlapiGetFieldValue('custrecord_ship_charges');
					pakageweight=nlapiGetFieldValue('custrecord_ship_actwght');

					nlapiLogExecution('DEBUG', 'trackingno', trackingno);
					nlapiLogExecution('DEBUG', 'pakageweight', pakageweight);
					nlapiLogExecution('DEBUG', 'shipcharges ', shipcharges);

					if((vorderno!="" && vorderno!=null) && (trackingno!=null && trackingno!=''))
					{					

						var filters = new Array();
						filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'is',vorderno));
						filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'is',shipcontainerlp));
						filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));
						filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null,'isnotempty'));

						var columns= new Array();
						columns[0] = new nlobjSearchColumn('custrecord_ebiz_nsconfirm_ref_no');
						columns[1] = new nlobjSearchColumn('name');
						columns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
						columns[3] = new nlobjSearchColumn('custrecord_ship_lp_no');
						columns[4] = new nlobjSearchColumn('custrecord_line_no');
						columns[5] = new nlobjSearchColumn('custrecord_act_qty');
						columns[6] = new nlobjSearchColumn('custrecord_container_lp_no');
						var transactionid;
						var opentasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters ,columns);

						nlapiLogExecution('DEBUG', 'trackingno2', trackingno);
						nlapiLogExecution('DEBUG', 'pakageweight2', pakageweight);
						nlapiLogExecution('DEBUG', 'shipcharges2 ', shipcharges);

						if(opentasksearchresults !=null)
						{
							var vNSConfirmArr=new Array();
							var wmslocation=opentasksearchresults[0].getValue('custrecord_wms_location');
							for(var p=0;p<opentasksearchresults.length;p++)
							{
								transactionid=opentasksearchresults[p].getValue('custrecord_ebiz_nsconfirm_ref_no');
								if(vNSConfirmArr== null || vNSConfirmArr == '' || vNSConfirmArr.length==0)
								{
									vNSConfirmArr.push(transactionid);
								}
								else
								{
									// To avoid duplicate
									var vDuplicate=false;
									for(var s=0;s<vNSConfirmArr.length;s++)
									{
										if(vNSConfirmArr[s] == transactionid)
											vDuplicate=true;
									}
									if(vDuplicate == false)
									{
										vNSConfirmArr.push(transactionid);
									}	
								}	
							}

							nlapiLogExecution('DEBUG', 'NS Confirmation # ', transactionid);
							nlapiLogExecution('DEBUG', 'vNSConfirmArr # ', vNSConfirmArr);
							//var wmslocation=opentasksearchresults[0].getValue('custrecord_wms_location');
							transactionid=opentasksearchresults[0].getValue('custrecord_ebiz_nsconfirm_ref_no');
							//var vebizcontainerlp=opentasksearchresults[0].getValue('custrecord_ebiztask_ship_lp_no');
							nlapiLogExecution('DEBUG', 'vebizcontainerlp ', vebizcontainerlp);
							nlapiLogExecution('DEBUG', 'NS Confirmation #2 ', transactionid);
							if(transactionid!=null && transactionid!="")
							{
								for(k=0;k<vNSConfirmArr.length;k++)
								{	
									nlapiLogExecution('DEBUG', 'NS Confirmation # ', vNSConfirmArr[k]);
									if(vNSConfirmArr[k]!='0')
									{
										var OpenTaskTransaction = nlapiLoadRecord('itemfulfillment', vNSConfirmArr[k]);
										var totalLine=OpenTaskTransaction.getLineItemCount('package');
										nlapiLogExecution('DEBUG', 'lineNum ', totalLine);
										for(var i=1; i<= parseFloat(totalLine); i++)
										{

											var packagetrack=OpenTaskTransaction.getLineItemValue('package','packagetrackingnumber',i);
											var packageweight=OpenTaskTransaction.getLineItemValue('package','packageweight',i);
											nlapiLogExecution('DEBUG', 'packageweight', packageweight);
											nlapiLogExecution('DEBUG', 'packagetrack ', packagetrack);
											if((packagetrack==null)&&(packageweight!=null))
											{
												OpenTaskTransaction.removeLineItem('package',i);
											}
										}	

										OpenTaskTransaction.setFieldValue('shipstatus','C'); //Statusflag='C'
										var lastcharges =OpenTaskTransaction.getFieldValue('shippingcost');
										nlapiLogExecution('DEBUG', 'shipcharges ', lastcharges);
										var totalshipcharges=parseFloat(lastcharges)+parseFloat(shipcharges);
										if(pakageweight=='0.0' || pakageweight=="0.0000" || pakageweight=='undefined' || pakageweight == 0 || pakageweight =='' || pakageweight =='null' || pakageweight == null || isNaN(pakageweight))
										{
											pakageweight='0.11';
										}
										//OpenTaskTransaction.setFieldValue('shippingcost',totalshipcharges);
										OpenTaskTransaction.setFieldValue('generateintegratedshipperlabel','F');
										OpenTaskTransaction.setFieldValue('custbody_fulfill_shipcost',totalshipcharges);
										OpenTaskTransaction.selectNewLineItem('package');
										OpenTaskTransaction.setCurrentLineItemValue('package', 'packagedescr', vebizcontainerlp);
										OpenTaskTransaction.setCurrentLineItemValue('package', 'packagetrackingnumber', trackingno);
										OpenTaskTransaction.setCurrentLineItemValue('package', 'packageweight',pakageweight);
										//OpenTaskTransaction.setCurrentLineItemValue('item', 'location',wmslocation);
										OpenTaskTransaction.commitLineItem('package');
										//OpenTaskTransaction.commitLineItem('item');
										var id= nlapiSubmitRecord(OpenTaskTransaction ,true);
										nlapiLogExecution('ERROR', 'After open task submit record ', id);
										//Case # 20127782 starts code for updating nsconfirmation number
										if(id!=null && id!='')
										{	

											var internalidrecord=newRecord.getId();


											nlapiLogExecution('ERROR', 'UpdateShipManifestRecord# ', 'UpdateShipManifestRecord# ');

											nlapiLogExecution('ERROR', 'internalidrecord# ', internalidrecord);
											UpdateShipManifestRecord(id,internalidrecord);

										}
										//Case # 20127782 end of code for updating nsconfiramtion number


									}
								}

								var  SalesOrderInternalId= opentasksearchresults[0].getValue('custrecord_ebiz_order_no');	
								nlapiLogExecution('DEBUG', 'SalesOrderInternalId 2', SalesOrderInternalId);
								var trantype = nlapiLookupField('transaction', SalesOrderInternalId, 'recordType');

								nlapiLogExecution('DEBUG', 'trantype 2', trantype);

								for (var j = 0; j < opentasksearchresults.length; j++)
								{

									var ffOrdNo = opentasksearchresults[j].getValue('name');
									var ffOrdLineNo = opentasksearchresults[j].getValue('custrecord_line_no');
									var shipqty = opentasksearchresults[j].getValue('custrecord_act_qty');

									updateShipQty(ffOrdNo,ffOrdLineNo,shipqty);

									var opentaskcontainerlpno=opentasksearchresults[j].getValue('custrecord_container_lp_no');
									if(shipcontainerlp==opentaskcontainerlpno)
									{
										nlapiLogExecution('DEBUG', 'opentasksearchresults[j].getId() ', opentasksearchresults[j].getId());
										updateopentaskstatus(opentasksearchresults[j].getId());
									}

									MoveTaskRecord(opentasksearchresults[j].getId());//added now
								}
							}
						}

						var packfilters = new Array();
						packfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'is',vorderno));
						packfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['14']));

						var packcolumns= new Array();
						packcolumns[0] = new nlobjSearchColumn('custrecord_container_lp_no');

						var packtasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,packfilters ,packcolumns);
						if(packtasksearchresults !=null && packtasksearchresults !='')
						{
							nlapiLogExecution('DEBUG', 'packtasksearchresults length2',packtasksearchresults.length);
							for (var i = 0; i < packtasksearchresults.length; i++) 
							{
								var vebizcontainerlp = packtasksearchresults[i].getValue('custrecord_container_lp_no');
								nlapiLogExecution('DEBUG', 'clearOutboundInventory for LTL');
								clearOutboundInventory(vebizcontainerlp);
							}
						}
					}
				}
				else
				{

					nlapiLogExecution('DEBUG', 'Into vendor  Orders');
					var neworderno=nlapiGetFieldValue('custrecord_ship_order');
					var fulfillmentNo=nlapiGetFieldValue('custrecord_ship_ref3');
					var newRecord = nlapiGetNewRecord();
					var vebizcontainerlp =nlapiGetFieldValue('custrecord_ship_contlp');
					nlapiLogExecution('Debug', 'neworderno', neworderno);
					nlapiLogExecution('Debug', 'fulfillmentNo', fulfillmentNo);
					nlapiLogExecution('Debug', 'vebizcontainerlp', vebizcontainerlp);
					var vMastLPNo =nlapiGetFieldValue('custrecord_ship_ref5');
					if(neworderno != null && neworderno != '')
					{	
						var ismultilineship='F';
						var trantypeso = nlapiLookupField('transaction', neworderno, 'recordType');
						nlapiLogExecution('Debug', 'trantypeso', trantypeso);
						var salesorderrec= nlapiLoadRecord(trantypeso, neworderno);
						if(salesorderrec!=null && salesorderrec!='')
							ismultilineship=salesorderrec.getFieldValue('ismultishipto');

						nlapiLogExecution('Debug', 'Item Line Shipping', ismultilineship);
						if(ismultilineship!='T')
						{


							var vNSConfirmNo=createItemFulfillment(neworderno,fulfillmentNo,'QuickShip',"","",vebizcontainerlp,"","","","",salesorderrec);
							nlapiLogExecution('DEBUG', 'vNSConfirmNo', vNSConfirmNo);
							var internalidrecord=newRecord.getId();

							if((vNSConfirmNo!=null)&&(vNSConfirmNo!=''))
							{
								nlapiLogExecution('DEBUG', 'UpdateShipManifestRecord# Foe Ship', 'UpdateShipManifestRecord# ');

								nlapiLogExecution('DEBUG', 'internalidrecord# ', internalidrecord);
								UpdateShipManifestRecord(vNSConfirmNo,internalidrecord);
								UpdateAllShipManifestRecord(vNSConfirmNo,internalidrecord,vMastLPNo,fulfillmentNo,'PC');
								nlapiLogExecution('DEBUG', 'clearOutboundInventory for PC');
								//clearOutboundInventory(vebizcontainerlp);






//								var vorderno =nlapiGetFieldValue('custrecord_ship_orderno');
								//var newRecord = nlapiGetNewRecord();
								vorderno =nlapiGetFieldValue('name');
								nlapiLogExecution('DEBUG', 'vorderno QuickShip', vorderno);
								var shipcontainerlp=nlapiGetFieldValue('custrecord_ship_contlp');
								nlapiLogExecution('DEBUG', 'shipcontainerlp', shipcontainerlp);
								nlapiLogExecution('DEBUG', 'intoelseofcreate', vorderno);
								vebizcontainerlp =nlapiGetFieldValue('custrecord_ship_contlp');

								nlapiLogExecution('DEBUG', 'vebizcontainerlp QuickShip', vebizcontainerlp);

								var trackingno;
								var shipcharges;
								var pakageweight;

								trackingno = nlapiGetFieldValue('custrecord_ship_trackno');
								shipcharges =nlapiGetFieldValue('custrecord_ship_charges');
								pakageweight=nlapiGetFieldValue('custrecord_ship_actwght');

								nlapiLogExecution('DEBUG', 'trackingno', trackingno);
								nlapiLogExecution('DEBUG', 'pakageweight', pakageweight);
								nlapiLogExecution('DEBUG', 'shipcharges ', shipcharges);

								if((vorderno!="" && vorderno!=null) && (trackingno!=null && trackingno!=''))
								{					

									var filters = new Array();
									filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'is',vorderno));
									filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'is',shipcontainerlp));
									filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));
									filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null,'isnotempty'));

									var columns= new Array();
									columns[0] = new nlobjSearchColumn('custrecord_ebiz_nsconfirm_ref_no');
									columns[1] = new nlobjSearchColumn('name');
									columns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
									columns[3] = new nlobjSearchColumn('custrecord_ship_lp_no');
									columns[4] = new nlobjSearchColumn('custrecord_line_no');
									columns[5] = new nlobjSearchColumn('custrecord_act_qty');
									columns[6] = new nlobjSearchColumn('custrecord_container_lp_no');
									var transactionid;
									var opentasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters ,columns);

									nlapiLogExecution('DEBUG', 'trackingno2', trackingno);
									nlapiLogExecution('DEBUG', 'pakageweight2', pakageweight);
									nlapiLogExecution('DEBUG', 'shipcharges2 ', shipcharges);

									if(opentasksearchresults !=null)
									{
										var vNSConfirmArr=new Array();
										var wmslocation=opentasksearchresults[0].getValue('custrecord_wms_location');
										for(var p=0;p<opentasksearchresults.length;p++)
										{
											transactionid=opentasksearchresults[p].getValue('custrecord_ebiz_nsconfirm_ref_no');
											if(vNSConfirmArr== null || vNSConfirmArr == '' || vNSConfirmArr.length==0)
											{
												vNSConfirmArr.push(transactionid);
											}
											else
											{
												// To avoid duplicate
												var vDuplicate=false;
												for(var s=0;s<vNSConfirmArr.length;s++)
												{
													if(vNSConfirmArr[s] == transactionid)
														vDuplicate=true;
												}
												if(vDuplicate == false)
												{
													vNSConfirmArr.push(transactionid);
												}	
											}	
										}

										nlapiLogExecution('DEBUG', 'NS Confirmation # ', transactionid);
										nlapiLogExecution('DEBUG', 'vNSConfirmArr # ', vNSConfirmArr);
										//var wmslocation=opentasksearchresults[0].getValue('custrecord_wms_location');
										transactionid=opentasksearchresults[0].getValue('custrecord_ebiz_nsconfirm_ref_no');
										//var vebizcontainerlp=opentasksearchresults[0].getValue('custrecord_ebiztask_ship_lp_no');
										nlapiLogExecution('DEBUG', 'vebizcontainerlp ', vebizcontainerlp);
										nlapiLogExecution('DEBUG', 'NS Confirmation #2 ', transactionid);
										if(transactionid!=null && transactionid!="")
										{
											for(k=0;k<vNSConfirmArr.length;k++)
											{	
												nlapiLogExecution('DEBUG', 'NS Confirmation # ', vNSConfirmArr[k]);
												if(vNSConfirmArr[k]!='0')
												{
													var OpenTaskTransaction = nlapiLoadRecord('itemfulfillment', vNSConfirmArr[k]);
													var totalLine=OpenTaskTransaction.getLineItemCount('package');
													nlapiLogExecution('DEBUG', 'lineNum ', totalLine);
													for(var i=1; i<= parseFloat(totalLine); i++)
													{

														var packagetrack=OpenTaskTransaction.getLineItemValue('package','packagetrackingnumber',i);
														var packageweight=OpenTaskTransaction.getLineItemValue('package','packageweight',i);
														nlapiLogExecution('DEBUG', 'packageweight', packageweight);
														nlapiLogExecution('DEBUG', 'packagetrack ', packagetrack);
														if((packagetrack==null)&&(packageweight!=null))
														{
															OpenTaskTransaction.removeLineItem('package',i);
														}
													}	

													OpenTaskTransaction.setFieldValue('shipstatus','C'); //Statusflag='C'
													var lastcharges =OpenTaskTransaction.getFieldValue('shippingcost');
													nlapiLogExecution('DEBUG', 'shipcharges ', lastcharges);
													var totalshipcharges=parseFloat(lastcharges)+parseFloat(shipcharges);
													if(pakageweight=='0.0' || pakageweight=="0.0000" || pakageweight=='undefined' || pakageweight == 0 || pakageweight =='' || pakageweight =='null' || pakageweight == null || isNaN(pakageweight))
													{
														pakageweight='0.11';
													}
													//OpenTaskTransaction.setFieldValue('shippingcost',totalshipcharges);
													OpenTaskTransaction.setFieldValue('generateintegratedshipperlabel','F');
													OpenTaskTransaction.setFieldValue('custbody_fulfill_shipcost',totalshipcharges);
													OpenTaskTransaction.selectNewLineItem('package');
													OpenTaskTransaction.setCurrentLineItemValue('package', 'packagedescr', vebizcontainerlp);
													OpenTaskTransaction.setCurrentLineItemValue('package', 'packagetrackingnumber', trackingno);
													OpenTaskTransaction.setCurrentLineItemValue('package', 'packageweight',pakageweight);
													//OpenTaskTransaction.setCurrentLineItemValue('item', 'location',wmslocation);
													OpenTaskTransaction.commitLineItem('package');
													//OpenTaskTransaction.commitLineItem('item');
													var id= nlapiSubmitRecord(OpenTaskTransaction ,true);
													nlapiLogExecution('DEBUG', 'After open task submit record ', id);
												}
											}

											var  SalesOrderInternalId= opentasksearchresults[0].getValue('custrecord_ebiz_order_no');	
											nlapiLogExecution('DEBUG', 'SalesOrderInternalId 2', SalesOrderInternalId);

											for (var j = 0; j < opentasksearchresults.length; j++)
											{

												var ffOrdNo = opentasksearchresults[j].getValue('name');
												var ffOrdLineNo = opentasksearchresults[j].getValue('custrecord_line_no');
												var shipqty = opentasksearchresults[j].getValue('custrecord_act_qty');

												updateShipQty(ffOrdNo,ffOrdLineNo,shipqty);

												var opentaskcontainerlpno=opentasksearchresults[j].getValue('custrecord_container_lp_no');
												if(shipcontainerlp==opentaskcontainerlpno)
												{
													nlapiLogExecution('DEBUG', 'opentasksearchresults[j].getId() ', opentasksearchresults[j].getId());
													updateopentaskstatus(opentasksearchresults[j].getId());
												}

												MoveTaskRecord(opentasksearchresults[j].getId());//added now
												var salesorderrec= nlapiLoadRecord("vendorreturnauthorization", SalesOrderInternalId);

												salesorderrec.setLineItemValue('item','custcol_transactionstatusflag',ffOrdLineNo,14);
												nlapiSubmitRecord(salesorderrec, true);	
											}
										}
									}

									var packfilters = new Array();
									packfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'is',vorderno));
									packfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['14']));

									var packcolumns= new Array();
									packcolumns[0] = new nlobjSearchColumn('custrecord_container_lp_no');

									var packtasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,packfilters ,packcolumns);
									if(packtasksearchresults !=null && packtasksearchresults !='')
									{
										nlapiLogExecution('DEBUG', 'packtasksearchresults length2',packtasksearchresults.length);
										for (var i = 0; i < packtasksearchresults.length; i++) 
										{
											var vebizcontainerlp = packtasksearchresults[i].getValue('custrecord_container_lp_no');
											nlapiLogExecution('DEBUG', 'clearOutboundInventory for LTL');
											clearOutboundInventory(vebizcontainerlp);
										}
									}
								}










							}
						}
						else
						{
							var vNSConfirmNo= createItemFulfillmentMultiLineShipping(neworderno,fulfillmentNo,vebizcontainerlp,shipcharges,trackingno,pakageweight,'QuickShip',vMastLPNo);
							nlapiLogExecution('DEBUG', 'vNSConfirmNo', vNSConfirmNo);

							var internalidrecord=newRecord.getId();

							if((vNSConfirmNo!=null)&&(vNSConfirmNo!=''))
							{
								nlapiLogExecution('DEBUG', 'UpdateShipManifestRecord# Foe Ship', 'UpdateShipManifestRecord# ');

								nlapiLogExecution('DEBUG', 'internalidrecord# ', internalidrecord);
								UpdateShipManifestRecord(vNSConfirmNo,internalidrecord);
								UpdateAllShipManifestRecord(vNSConfirmNo,internalidrecord,vMastLPNo,fulfillmentNo,'PC');
								nlapiLogExecution('DEBUG', 'clearOutboundInventory for PC');
								clearOutboundInventory(vebizcontainerlp);
							}
						}
					}



				}
			}

			catch(exp) {

				var exceptionname='SalesOrder';
				var functionality='ShipManifest Update';
				var trantype=2;
				nlapiLogExecution('DEBUG', 'DetailsError', functionality);	
				nlapiLogExecution('DEBUG', 'vebizcontainerlp', vebizcontainerlp);
				nlapiLogExecution('DEBUG', 'vorderno', vorderno);
				var reference2=vebizcontainerlp;
				var reference3="";
				var reference4 ="";
				var reference5 ="";
				var alertType=1;//1 for exception and 2 for report
				errSendMailByProcess(trantype,exp,vorderno,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);

				//InsertExceptionLog(exceptionname,trantype, functionality, exp, vorderno, vebizcontainerlp,reference3,reference4,reference5, userId);
				nlapiLogExecution('DEBUG', 'Exception in update Tracking Number for Quick Ship Orders : ', exp);		
			}
		}
	}

	if(type=='edit' || type=='xedit')
	{
		nlapiLogExecution('DEBUG', 'Into Edit');
		var vebizcontainerlp;
		var VsalesOrder;
		var vtrackingnonew;
		var chargesnew;
		var weightnew;
		var oldcontainerlp;
		var oldtrackingnumber;
		var oldcharges;
		var oldweight;
		var vOrdNo='';
		var pckgtype='';
		var returnsucess;
		try
		{
			var newRecord = nlapiGetNewRecord();

			vebizcontainerlp =newRecord.getFieldValue('custrecord_ship_contlp');
			VsalesOrder = newRecord.getFieldValue('custrecord_ship_orderno');
			vOrdNo = newRecord.getFieldValue('custrecord_ship_orderno');
			vtrackingnonew =newRecord.getFieldValue('custrecord_ship_trackno');
			chargesnew=newRecord.getFieldValue('custrecord_ship_charges');
			weightnew=newRecord.getFieldValue('custrecord_ship_actwght');
			pckgtype = newRecord.getFieldValue('custrecord_ship_pkgtype');
			var fulfillmentNo=newRecord.getFieldValue('custrecord_ship_ref3');
			var newshipmentid=newRecord.getId();			
			var oldRecord = nlapiGetOldRecord();
			oldtrackingnumber=oldRecord.getFieldValue('custrecord_ship_trackno');
			oldcharges=oldRecord.getFieldValue('custrecord_ship_charges');
			oldweight=oldRecord.getFieldValue('custrecord_ship_actwght');
			oldcontainerlp=oldRecord.getFieldValue('custrecord_ship_contlp');
			var vcarrier =oldRecord.getFieldValue('custrecord_ship_carrier');
			var voidstatus=newRecord.getFieldValue('custrecord_ship_void');
			var oldvoidstatus=oldRecord.getFieldValue('custrecord_ship_void');
			var oldorderno=oldRecord.getFieldValue('custrecord_ship_order');

			nlapiLogExecution('DEBUG', 'voidstatus ', voidstatus);

			var str = 'newshipmentid. = ' + newshipmentid + '<br>';
			str = str + 'fulfillmentNo. = ' + fulfillmentNo + '<br>';	
			str = str + 'vebizcontainerlp. = ' + vebizcontainerlp + '<br>';
			str = str + 'VsalesOrder. = ' + VsalesOrder + '<br>';
			str = str + 'voidstatus. = ' + voidstatus + '<br>';
			str = str + 'oldvoidstatus. = ' + oldvoidstatus + '<br>';
			str = str + 'oldorderno. = ' + oldorderno + '<br>';
			str = str + 'pckgtype. = ' + pckgtype + '<br>';
			str = str + 'vcarrier. = ' + vcarrier + '<br>';

			nlapiLogExecution('DEBUG', 'Log 1 ', str);

			//Label printing canceled then voidstatus y
			if(vcarrier=="LTL")
				oldorderno=oldRecord.getFieldValue('name');
			nlapiLogExecution('DEBUG', 'oldorderno in edit', oldorderno);
			var trantypeso = nlapiLookupField('transaction', oldorderno, 'recordType');
			nlapiLogExecution('Debug', 'trantypeso', trantypeso);			
			if(trantypeso != "vendorreturnauthorization")
			{
				if(voidstatus !="Y")
				{
					var trackingno;
					var shipcharges;
					var pakageweight;
					var paymethodtype;

					//If voidstatus 'N' it is not all in to this condition
					if(vebizcontainerlp!=""&& (vtrackingnonew!=null||vtrackingnonew!="") && (vcarrier!="LTL") && (voidstatus !="N"))
					{
						try
						{
							var generatebartenderpacklabel=getSystemRuleValue('Bartender-PackingSlip');
							if(generatebartenderpacklabel=="Y")
							{
								GenerateBartenderPackingSlip(newRecord);
							}
						}
						catch(exp)
						{
							nlapiLogExecution('ERROR', 'Exp-GenerateBartenderPackingSlip',exp);
						}
						var custom5=newRecord.getFieldValue('custrecord_ship_custom5');
						trackingno = newRecord.getFieldValue('custrecord_ship_trackno');
						pakageweight=newRecord.getFieldValue('custrecord_ship_actwght');
						paymethodtype = newRecord.getFieldValue('custrecord_ship_paymethod');
						nlapiLogExecution('DEBUG', 'freefrieght', paymethodtype);
						if(paymethodtype =='FREE_FRIEGHT')
						{
							shipcharges="0.00";
							nlapiLogExecution('DEBUG', 'freefrieght', shipcharges);  
						}
						else if(paymethodtype =="COLLECT")
						{
							shipcharges="0.00";
							nlapiLogExecution('DEBUG', 'collectcharges', shipcharges);  

						}
						else
						{
							shipcharges =newRecord.getFieldValue('custrecord_ship_charges');
						}

						nlapiLogExecution('DEBUG', 'trackingno', trackingno);
						nlapiLogExecution('DEBUG', 'pakageweight', pakageweight);
						nlapiLogExecution('DEBUG', 'shipcharges ', shipcharges);
						if(pakageweight=='0.0' || pakageweight=="0.0000" || pakageweight=='undefined' || pakageweight == 0 || pakageweight =='' || pakageweight =='null' || pakageweight == null || isNaN(pakageweight))
						{
							pakageweight='0.11';
						}
						var opentasksearchresults= getopentaskdetails(vebizcontainerlp);
						if(opentasksearchresults !=null && opentasksearchresults != '')
						{
							var vNSConfirmArr=new Array();
							var wmslocation=opentasksearchresults[0].getValue('custrecord_wms_location');

							for(var p=0;p<opentasksearchresults.length;p++)
							{
								transactionid=opentasksearchresults[p].getValue('custrecord_ebiz_nsconfirm_ref_no');
								if(vNSConfirmArr== null || vNSConfirmArr == '' || vNSConfirmArr.length==0)
								{
									vNSConfirmArr.push(transactionid);
								}
								else
								{
									// To avoid duplicate
									var vDuplicate=false;
									for(var s=0;s<vNSConfirmArr.length;s++)
									{
										if(vNSConfirmArr[s] == transactionid)
											vDuplicate=true;
									}
									if(vDuplicate == false)
									{
										vNSConfirmArr.push(transactionid);
									}
								}	
							}

							nlapiLogExecution('DEBUG', 'NS Confirmation # ', transactionid);
							nlapiLogExecution('DEBUG', 'vNSConfirmArr # ', vNSConfirmArr);
							if(transactionid!=null && transactionid!="")
							{
								nlapiLogExecution('DEBUG', 'voidstatus # ', voidstatus);
								nlapiLogExecution('DEBUG', 'oldvoidstatus # ', oldvoidstatus);

								//if((voidstatus =="U")&&(oldvoidstatus =="N"))
								if((voidstatus =="U"))
								{
									var shipcontainerlp=oldRecord.getFieldValue('custrecord_ship_contlp');
									var Waveno=oldRecord.getFieldValue('custrecord_ship_custom4');

									//if New Void Status 'U' and oldvoidstatus 'N' it creates updateitemfullment
									nlapiLogExecution('DEBUG', 'NewRecord updated : ', 'NEWTRACKING NUMBER UPDATED');
									var itemfulfillmentransactionid='';
									for(k=0;k<vNSConfirmArr.length;k++)
									{	
										//UpdateItemFulfillment(transactionid,shipcharges,vebizcontainerlp,trackingno,pakageweight);
										itemfulfillmentransactionid=UpdateItemFulfillment(vNSConfirmArr[k],shipcharges,vebizcontainerlp,trackingno,
												pakageweight,fulfillmentNo,Waveno,FULFILLMENTATORDERLEVEL,oldorderno);

										var internalidrecord=newRecord.getId();

										if((itemfulfillmentransactionid!=null)&&(itemfulfillmentransactionid!=''))
										{
											nlapiLogExecution('DEBUG', 'UpdateShipManifestRecord# ', 'UpdateShipManifestRecord# ');
											nlapiLogExecution('DEBUG', 'itemfulfillmentransactionid # ', itemfulfillmentransactionid);
											nlapiLogExecution('DEBUG', 'internalidrecord# ', internalidrecord);

											UpdateShipManifestRecord(itemfulfillmentransactionid,internalidrecord,Waveno,fulfillmentNo,
													FULFILLMENTATORDERLEVEL);
											//Specfic for DCD invioce generation added by integration team

											nlapiLogExecution('DEBUG', 'Before Into URl Calling', 'Into URl Calling');

											var filters=new Array();
											var columns=new Array();

											filters.push(new nlobjSearchFilter('custrecord_ebizruletype', null, 'is',"Auto Invoice"));
											filters.push(new nlobjSearchFilter('custrecord_ebizrulevalue', null, 'is',"Y"));

											var searchresultscarr = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters,columns);

											nlapiLogExecution('Debug', 'Before create invoice', searchresultscarr);

											if(searchresultscarr!=null && searchresultscarr!='' && searchresultscarr.length>0)
											{
												nlapiLogExecution('Debug', 'into create invoice', 'into create invoice');
												//commented because of customized code
												var printername="";
												nlapiLogExecution('Debug', 'fulfillmentNo is:', fulfillmentNo);
												nlapiLogExecution('Debug', 'vebizcontainerlp is:', vebizcontainerlp);
												var searchshipmanifestshippedrecords=GetShipManifestCustomRecordsforInvoice(fulfillmentNo,vebizcontainerlp);
												var searchopentaskpicklocationrecords=Getopentaskpicklocationrecords(fulfillmentNo);
												nlapiLogExecution('DEBUG', 'searchshipmanifestshippedrecords', searchshipmanifestshippedrecords);
												nlapiLogExecution('DEBUG', 'searchopentaskpicklocationrecords', searchopentaskpicklocationrecords);
												if(searchshipmanifestshippedrecords==null  && searchopentaskpicklocationrecords==null)
												{
													createinvoice(itemfulfillmentransactionid,printername);	
												}

											}

											nlapiLogExecution('DEBUG', 'After Into URl Calling', 'After Into URl Calling');
										}
									}


									if((itemfulfillmentransactionid!=null)&&(itemfulfillmentransactionid!=''))
									{
										nlapiLogExecution('DEBUG', 'clearOutboundInventory for PC');
										clearOutboundInventory(vebizcontainerlp);
										if((custom5=="S") &&(custom5 !="D"))
										{
											//custom5 status not allow duplicate record for updateshipqty and clearOutBoundInventory
											nlapiLogExecution('DEBUG', 'custom5', custom5);
											if(FULFILLMENTATORDERLEVEL!='Y')
											{
												for (var j = 0; j < opentasksearchresults.length; j++)
												{
													nlapiLogExecution('DEBUG', 'custom5', 'updateShipQty');
													var ffOrdNo = opentasksearchresults[j].getValue('name');
													var ffOrdLineNo = opentasksearchresults[j].getValue('custrecord_line_no');
													var shipqty = opentasksearchresults[j].getValue('custrecord_act_qty');
													updateShipQty(ffOrdNo,ffOrdLineNo,shipqty);		

													var opentaskcontainerlpno=opentasksearchresults[j].getValue('custrecord_container_lp_no');
													nlapiLogExecution('DEBUG', 'opentaskcontainerlpno ', opentaskcontainerlpno);
													nlapiLogExecution('DEBUG', 'shipcontainerlp ', shipcontainerlp);

													if(shipcontainerlp==opentaskcontainerlpno)
													{
														nlapiLogExecution('DEBUG', 'opentasksearchresults[j].getId() ', opentasksearchresults[j].getId());
														updateopentaskstatus(opentasksearchresults[j].getId());
														nlapiLogExecution('DEBUG', 'tsting movetaskrecord for pc');
														MoveTaskRecord(opentasksearchresults[j].getId());//added now
													}

													//AutoShipMent(SalesOrderInternalId);
												}

												//clearOutboundInventory(votebizcontainerlp);
											}
											else
											{
												try
												{
													updatefulfillmentorderstatus(fulfillmentNo,itemfulfillmentransactionid,vebizcontainerlp);
													nlapiLogExecution('DEBUG', 'clearOutboundInventory for PC',vebizcontainerlp);
													clearOutboundInventory(vebizcontainerlp);
												}
												catch(exp)
												{
													nlapiLogExecution('DEBUG','Exception in updatefulfillmentorderstatus',exp);
													nlapiLogExecution('DEBUG', 'Exception Details', exp.getDetails());
												}
											}
										}
									}

									if(pckgtype!=null && pckgtype=='SHIP')
									{
										var Mastercartonfilters = new Array();
										Mastercartonfilters.push(new nlobjSearchFilter('custrecord_ship_mastercarton', null,'is',shipcontainerlp));

										var Mastercartoncolumns= new Array();
										Mastercartoncolumns[0] = new nlobjSearchColumn('custrecord_ship_contlp').setSort();
										Mastercartoncolumns[1] = new nlobjSearchColumn('custrecord_ship_order');

										Mastercartonsearchresults = nlapiSearchRecord('customrecord_ship_manifest',null,Mastercartonfilters ,Mastercartoncolumns);

										nlapiLogExecution('DEBUG', 'Mastercartonsearchresults', Mastercartonsearchresults);

										if(Mastercartonsearchresults!=null && Mastercartonsearchresults!='')
										{
											var prevtempordno='-1';

											for (var j1 = 0;j1 < Mastercartonsearchresults.length; j1++)
											{
												var tempshipcontainerlp = Mastercartonsearchresults[j1].getValue('custrecord_ship_contlp');
												var temporderno = Mastercartonsearchresults[j1].getValue('custrecord_ship_order');

												nlapiLogExecution('EMERGENCY', 'tempshipcontainerlp ', tempshipcontainerlp);

												for (var j2 = 0; j2 < opentasksearchresults.length; j2++)
												{
													var ffOrdNo = opentasksearchresults[j2].getValue('name');
													var ffOrdLineNo = opentasksearchresults[j2].getValue('custrecord_line_no');
													var shipqty = opentasksearchresults[j2].getValue('custrecord_act_qty');
													var FOIntrId = opentasksearchresults[j2].getValue('custrecord_ebiz_cntrl_no');

													//updateShipQty(ffOrdNo,ffOrdLineNo,shipqty);		

													updateFOShipQty(FOIntrId,ffOrdNo,ffOrdLineNo,shipqty);		

													var opentaskcontainerlpno = opentasksearchresults[j2].getValue('custrecord_container_lp_no');
													nlapiLogExecution('DEBUG', 'opentaskcontainerlpno ', opentaskcontainerlpno);
													nlapiLogExecution('DEBUG', 'shipcontainerlp ', shipcontainerlp);
													nlapiLogExecution('DEBUG', 'tempshipcontainerlp ', tempshipcontainerlp);

													if((tempshipcontainerlp!=null && tempshipcontainerlp!='') 
															&& (tempshipcontainerlp == opentaskcontainerlpno))
													{
														clearOutboundInventory(opentaskcontainerlpno);
														nlapiLogExecution('EMERGENCY', 'opentasksearchresults[j2].getId() ', opentasksearchresults[j2].getId());
														updateopentaskstatus(opentasksearchresults[j2].getId());
														if(prevtempordno!=temporderno)
														{
															updatescaccode(temporderno,ActualCarrierName,servicelevel);
															prevtempordno = temporderno;
														}
														nlapiLogExecution('EMERGENCY', 'tsting movetaskrecord for pc');
														MoveTaskRecord(opentasksearchresults[j2].getId());//added now
													}
												}
											}
										}
									}
								}
								/*else
							{
								//if voidstatus 'U' and oldvoidstatus 'U' are equal the deleteitemfulfillment line creates anotherline
								nlapiLogExecution('DEBUG', 'oldrecord updated : ', 'OLDRECORD TRACKING NUMBER UPDATED');
								if((oldcontainerlp==vebizcontainerlp)&& oldcontainerlp!=""&& (oldtrackingnumber!=null||oldtrackingnumber!="") && vcarrier!="LTL")
								{

									nlapiLogExecution('DEBUG', 'oldTrackingNoafterelse: ', oldtrackingnumber);
									nlapiLogExecution('DEBUG', 'oldActualweightafterelse ', oldweight);
									nlapiLogExecution('DEBUG', 'oldshipchargesafterelse : ', oldcharges);
									for(k=0;k<vNSConfirmArr.length;k++)
									{	
										nlapiLogExecution('DEBUG', 'vNSConfirmArr[k] ', vNSConfirmArr[k]);
										if(vNSConfirmArr[k]!='0')
										{
//											DeleteItemfulfillmentLine(transactionid,oldcharges,vebizcontainerlp,oldtrackingnumber);
  									       UpdateItemFulfillment(transactionid,shipcharges,vebizcontainerlp,trackingno,pakageweight);
											DeleteItemfulfillmentLine(vNSConfirmArr[k],oldcharges,vebizcontainerlp,oldtrackingnumber);
											//var itemfulfillmentransactionid=UpdateItemFulfillment(vNSConfirmArr[k],shipcharges,vebizcontainerlp,trackingno,pakageweight);
											var internalidrecord=newRecord.getId();
											if((itemfulfillmentransactionid!=null)&&(itemfulfillmentransactionid!=''))
											{
												UpdateShipManifestRecord(itemfulfillmentransactionid,internalidrecord);
											}
										}
									}
									clearOutboundInventory(vebizcontainerlp);
									for (var j = 0; j < opentasksearchresults.length; j++)
									{
										var ffOrdNo = opentasksearchresults[j].getValue('name');
										var ffOrdLineNo = opentasksearchresults[j].getValue('custrecord_line_no');
										var shipqty = opentasksearchresults[j].getValue('custrecord_act_qty');
										updateShipQty(ffOrdNo,ffOrdLineNo,shipqty);	
										nlapiLogExecution('DEBUG', 'tsting movetaskrecord for pc');
										MoveTaskRecord(opentasksearchresults[j].getId());//added now
									}
								}
							}*/
							}
						}
					}
				}
				else
				{
					var internalidold=oldRecord.getId();
					nlapiLogExecution('DEBUG', 'oldTrackingNotnull', oldtrackingnumber);
					nlapiLogExecution('DEBUG', 'AfterVoidY', vtrackingnonew);
					if((vtrackingnonew==oldtrackingnumber) && (vtrackingnonew!=null||vtrackingnonew!="") && (oldtrackingnumber!=null||oldtrackingnumber!=""))
					{
						nlapiLogExecution('DEBUG', 'oldTrackingNotnull', 'oldTrackingNo');
						var opentasksearchresults= getopentaskdetails(vebizcontainerlp);
						if(opentasksearchresults !=null)
						{
							var	transactionid=opentasksearchresults[0].getValue('custrecord_ebiz_nsconfirm_ref_no');
							nlapiLogExecution('DEBUG', 'NS Confirmation afterelse # ', transactionid);
							if(transactionid!=null && transactionid!="")
							{
								nlapiLogExecution('DEBUG', 'oldTrackingNoafterelse: ', oldtrackingnumber);
								nlapiLogExecution('DEBUG', 'oldActualweightafterelse ', oldweight);
								nlapiLogExecution('DEBUG', 'oldshipchargesafterelse : ', oldcharges);
								DeleteItemfulfillmentLine(transactionid,oldcharges,vebizcontainerlp,oldtrackingnumber);

							}
						}
						nlapiLogExecution('DEBUG', 'vebizcontainerlp', vebizcontainerlp);
						nlapiLogExecution('DEBUG', 'internalidold', internalidold);
						CreateShipmanifestRecord(internalidold,vebizcontainerlp,oldorderno);
						var shipmentgetrecord= nlapiLoadRecord('customrecord_ship_manifest', internalidold,vebizcontainerlp);
						var setvoidflag="R";
						shipmentgetrecord.setFieldValue('custrecord_ship_void',setvoidflag);
						var setcustomflag="D";
						shipmentgetrecord.setFieldValue('custrecord_ship_custom5',setcustomflag);
						var id = nlapiSubmitRecord(shipmentgetrecord, true);
					}
				}
			}
			else
			{


				if((voidstatus =="U"))
				{
					var neworderno=newRecord.getFieldValue('custrecord_ship_order');
					nlapiLogExecution('Debug', 'neworderno', neworderno);
					if(oldorderno != null && oldorderno != '')
					{	
						var vMastLPNo =nlapiGetFieldValue('custrecord_ship_ref5');
						var ismultilineship='F';

						var trantypeso = nlapiLookupField('transaction', oldorderno, 'recordType');
						nlapiLogExecution('Debug', 'trantypeso', trantypeso);
						var salesorderrec= nlapiLoadRecord(trantypeso, oldorderno);
						if(salesorderrec!=null && salesorderrec!='')
							ismultilineship=salesorderrec.getFieldValue('ismultishipto');

						nlapiLogExecution('Debug', 'Item Line Shipping', ismultilineship);
						if(ismultilineship!='T')
						{

							var vNSConfirmNo=createItemFulfillment(neworderno,fulfillmentNo,'QuickShip',"","",vebizcontainerlp,"","","","",salesorderrec);
							nlapiLogExecution('DEBUG', 'vNSConfirmNo', vNSConfirmNo);

							var internalidrecord=newRecord.getId();

							if((vNSConfirmNo!=null)&&(vNSConfirmNo!=''))
							{
								nlapiLogExecution('DEBUG', 'UpdateShipManifestRecord# Foe Ship', 'UpdateShipManifestRecord# ');

								nlapiLogExecution('DEBUG', 'internalidrecord# ', internalidrecord);
								UpdateShipManifestRecord(vNSConfirmNo,internalidrecord);
								UpdateAllShipManifestRecord(vNSConfirmNo,internalidrecord,vMastLPNo,fulfillmentNo,'PC');
								nlapiLogExecution('DEBUG', 'clearOutboundInventory for PC');
								//clearOutboundInventory(vebizcontainerlp);




								//case# 201410526 For Other tahn LTL carrier we tacking 
								vorderno =neworderno;


//								var vorderno =nlapiGetFieldValue('custrecord_ship_orderno');
								//var newRecord = nlapiGetNewRecord();
								//vorderno =nlapiGetFieldValue('name');
								nlapiLogExecution('DEBUG', 'vorderno QuickShip', vorderno);
								var shipcontainerlp=nlapiGetFieldValue('custrecord_ship_contlp');
								nlapiLogExecution('DEBUG', 'shipcontainerlp', shipcontainerlp);
								nlapiLogExecution('DEBUG', 'intoelseofcreate', vorderno);
								vebizcontainerlp =nlapiGetFieldValue('custrecord_ship_contlp');

								nlapiLogExecution('DEBUG', 'vebizcontainerlp QuickShip', vebizcontainerlp);

								var trackingno;
								var shipcharges;
								var pakageweight;

								trackingno = nlapiGetFieldValue('custrecord_ship_trackno');
								shipcharges =nlapiGetFieldValue('custrecord_ship_charges');
								pakageweight=nlapiGetFieldValue('custrecord_ship_actwght');

								nlapiLogExecution('DEBUG', 'trackingno', trackingno);
								nlapiLogExecution('DEBUG', 'pakageweight', pakageweight);
								nlapiLogExecution('DEBUG', 'shipcharges ', shipcharges);

								if((vorderno!="" && vorderno!=null) && (trackingno!=null && trackingno!=''))
								{					

									var filters = new Array();
									filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'is',vorderno));
									filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'is',shipcontainerlp));
									filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));
									filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null,'isnotempty'));

									var columns= new Array();
									columns[0] = new nlobjSearchColumn('custrecord_ebiz_nsconfirm_ref_no');
									columns[1] = new nlobjSearchColumn('name');
									columns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
									columns[3] = new nlobjSearchColumn('custrecord_ship_lp_no');
									columns[4] = new nlobjSearchColumn('custrecord_line_no');
									columns[5] = new nlobjSearchColumn('custrecord_act_qty');
									columns[6] = new nlobjSearchColumn('custrecord_container_lp_no');
									var transactionid;
									var opentasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters ,columns);

									nlapiLogExecution('DEBUG', 'trackingno2', trackingno);
									nlapiLogExecution('DEBUG', 'pakageweight2', pakageweight);
									nlapiLogExecution('DEBUG', 'shipcharges2 ', shipcharges);

									if(opentasksearchresults !=null)
									{
										var vNSConfirmArr=new Array();
										var wmslocation=opentasksearchresults[0].getValue('custrecord_wms_location');
										for(var p=0;p<opentasksearchresults.length;p++)
										{
											transactionid=opentasksearchresults[p].getValue('custrecord_ebiz_nsconfirm_ref_no');
											if(vNSConfirmArr== null || vNSConfirmArr == '' || vNSConfirmArr.length==0)
											{
												vNSConfirmArr.push(transactionid);
											}
											else
											{
												// To avoid duplicate
												var vDuplicate=false;
												for(var s=0;s<vNSConfirmArr.length;s++)
												{
													if(vNSConfirmArr[s] == transactionid)
														vDuplicate=true;
												}
												if(vDuplicate == false)
												{
													vNSConfirmArr.push(transactionid);
												}	
											}	
										}

										nlapiLogExecution('DEBUG', 'NS Confirmation # ', transactionid);
										nlapiLogExecution('DEBUG', 'vNSConfirmArr # ', vNSConfirmArr);
										//var wmslocation=opentasksearchresults[0].getValue('custrecord_wms_location');
										transactionid=opentasksearchresults[0].getValue('custrecord_ebiz_nsconfirm_ref_no');
										//var vebizcontainerlp=opentasksearchresults[0].getValue('custrecord_ebiztask_ship_lp_no');
										nlapiLogExecution('DEBUG', 'vebizcontainerlp ', vebizcontainerlp);
										nlapiLogExecution('DEBUG', 'NS Confirmation #2 ', transactionid);
										if(transactionid!=null && transactionid!="")
										{
											for(k=0;k<vNSConfirmArr.length;k++)
											{	
												nlapiLogExecution('DEBUG', 'NS Confirmation # ', vNSConfirmArr[k]);
												if(vNSConfirmArr[k]!='0')
												{
													var OpenTaskTransaction = nlapiLoadRecord('itemfulfillment', vNSConfirmArr[k]);
													var totalLine=OpenTaskTransaction.getLineItemCount('package');
													nlapiLogExecution('DEBUG', 'lineNum ', totalLine);
													for(var i=1; i<= parseFloat(totalLine); i++)
													{

														var packagetrack=OpenTaskTransaction.getLineItemValue('package','packagetrackingnumber',i);
														var packageweight=OpenTaskTransaction.getLineItemValue('package','packageweight',i);
														nlapiLogExecution('DEBUG', 'packageweight', packageweight);
														nlapiLogExecution('DEBUG', 'packagetrack ', packagetrack);
														if((packagetrack==null)&&(packageweight!=null))
														{
															OpenTaskTransaction.removeLineItem('package',i);
														}
													}	

													OpenTaskTransaction.setFieldValue('shipstatus','C'); //Statusflag='C'
													var lastcharges =OpenTaskTransaction.getFieldValue('shippingcost');
													nlapiLogExecution('DEBUG', 'shipcharges ', lastcharges);
													var totalshipcharges=parseFloat(lastcharges)+parseFloat(shipcharges);
													if(pakageweight=='0.0' || pakageweight=="0.0000" || pakageweight=='undefined' || pakageweight == 0 || pakageweight =='' || pakageweight =='null' || pakageweight == null || isNaN(pakageweight))
													{
														pakageweight='0.11';
													}
													//OpenTaskTransaction.setFieldValue('shippingcost',totalshipcharges);
													OpenTaskTransaction.setFieldValue('generateintegratedshipperlabel','F');
													OpenTaskTransaction.setFieldValue('custbody_fulfill_shipcost',totalshipcharges);
													OpenTaskTransaction.selectNewLineItem('package');
													OpenTaskTransaction.setCurrentLineItemValue('package', 'packagedescr', vebizcontainerlp);
													OpenTaskTransaction.setCurrentLineItemValue('package', 'packagetrackingnumber', trackingno);
													OpenTaskTransaction.setCurrentLineItemValue('package', 'packageweight',pakageweight);
													//OpenTaskTransaction.setCurrentLineItemValue('item', 'location',wmslocation);
													OpenTaskTransaction.commitLineItem('package');
													//OpenTaskTransaction.commitLineItem('item');
													var id= nlapiSubmitRecord(OpenTaskTransaction ,true);
													nlapiLogExecution('DEBUG', 'After open task submit record ', id);
												}
											}

											var  SalesOrderInternalId= opentasksearchresults[0].getValue('custrecord_ebiz_order_no');	
											nlapiLogExecution('DEBUG', 'SalesOrderInternalId 2', SalesOrderInternalId);

											for (var j = 0; j < opentasksearchresults.length; j++)
											{

												var ffOrdNo = opentasksearchresults[j].getValue('name');
												var ffOrdLineNo = opentasksearchresults[j].getValue('custrecord_line_no');
												var shipqty = opentasksearchresults[j].getValue('custrecord_act_qty');

												updateShipQty(ffOrdNo,ffOrdLineNo,shipqty);

												var opentaskcontainerlpno=opentasksearchresults[j].getValue('custrecord_container_lp_no');
												if(shipcontainerlp==opentaskcontainerlpno)
												{
													nlapiLogExecution('DEBUG', 'opentasksearchresults[j].getId() ', opentasksearchresults[j].getId());
													updateopentaskstatus(opentasksearchresults[j].getId());
												}

												MoveTaskRecord(opentasksearchresults[j].getId());//added now


												var salesorderrec= nlapiLoadRecord("vendorreturnauthorization", SalesOrderInternalId);

												salesorderrec.setLineItemValue('item','custcol_transactionstatusflag',ffOrdLineNo,14);
												nlapiSubmitRecord(salesorderrec, true);	

											}
										}
									}

									var packfilters = new Array();
									packfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'is',vorderno));
									packfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['14']));

									var packcolumns= new Array();
									packcolumns[0] = new nlobjSearchColumn('custrecord_container_lp_no');

									var packtasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,packfilters ,packcolumns);
									if(packtasksearchresults !=null && packtasksearchresults !='')
									{
										nlapiLogExecution('DEBUG', 'packtasksearchresults length2',packtasksearchresults.length);
										for (var i = 0; i < packtasksearchresults.length; i++) 
										{
											var vebizcontainerlp = packtasksearchresults[i].getValue('custrecord_container_lp_no');
											nlapiLogExecution('DEBUG', 'clearOutboundInventory for LTL');
											clearOutboundInventory(vebizcontainerlp);
										}
									}
								}								
							}
						}
						else
						{
							var vNSConfirmNo= createItemFulfillmentMultiLineShipping(oldorderno,fulfillmentNo,vebizcontainerlp,shipcharges,trackingno,pakageweight,'QuickShip',vMastLPNo);
							nlapiLogExecution('DEBUG', 'vNSConfirmNo', vNSConfirmNo);

							var internalidrecord=newRecord.getId();

							if((vNSConfirmNo!=null)&&(vNSConfirmNo!=''))
							{
								nlapiLogExecution('DEBUG', 'UpdateShipManifestRecord# Foe Ship', 'UpdateShipManifestRecord# ');

								nlapiLogExecution('DEBUG', 'internalidrecord# ', internalidrecord);
								UpdateShipManifestRecord(vNSConfirmNo,internalidrecord);
								UpdateAllShipManifestRecord(vNSConfirmNo,internalidrecord,vMastLPNo,fulfillmentNo,'PC');
								nlapiLogExecution('DEBUG', 'clearOutboundInventory for PC');
								clearOutboundInventory(vebizcontainerlp);
							}
						}
					}
				}


			}
		}
		catch(exp) {
			var exceptionname='SalesOrder';
			var functionality='ShipManifest Update';
			var trantype=2;
			nlapiLogExecution('DEBUG', 'DetailsError', functionality);	
			nlapiLogExecution('DEBUG', 'vebizcontainerlp', vebizcontainerlp);
			nlapiLogExecution('DEBUG', 'vOrdNo', vOrdNo);
			var reference2=vebizcontainerlp;
			var reference3="";
			var reference4 ="";
			var reference5 ="";
			var alertType=1;//1 for exception and 2 for report
			errSendMailByProcess(trantype,exp,vOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
			//InsertExceptionLog(exceptionname,trantype, functionality, exp, VsalesOrder, vebizcontainerlp,reference3,reference4,reference5, userId);
			nlapiLogExecution('DEBUG', 'Exception in update Tracking Number for Carrier PC : ', exp);		
		}
	}	
}
function UpdateAllShipManifestRecord(nsConfirmnumber,shipmentinternalid,vMastLPNo,fulfillmentNo,carrierType)
{
	nlapiLogExecution('DEBUG', 'Into UpdateAllShipManifestRecord', nsConfirmnumber);	

	try
	{
		var shipmentfilter = new Array();
		shipmentfilter.push(new nlobjSearchFilter('custrecord_ship_nsconf_no', null, 'isempty'));
		if(fulfillmentNo!=null && fulfillmentNo!='')
			shipmentfilter.push(new nlobjSearchFilter('custrecord_ship_ref3', null, 'is', fulfillmentNo));
		if(vMastLPNo!=null && vMastLPNo!='')
			shipmentfilter.push(new nlobjSearchFilter('custrecord_ship_ref4', null, 'is',vMastLPNo));
		var shipmentcolumns = new Array();
		shipmentcolumns[0] = new nlobjSearchColumn('custrecord_ship_ref5');
		var searchshipmanifestshippedrecords= nlapiSearchRecord('customrecord_ship_manifest', null, shipmentfilter, shipmentcolumns);
		if(searchshipmanifestshippedrecords!=null && searchshipmanifestshippedrecords!='')
		{
			for (var j = 0; j < searchshipmanifestshippedrecords.length; j++)
			{
				shipmentinternalid = searchshipmanifestshippedrecords[j].getId();

				nlapiLogExecution('DEBUG', 'shipmentinternalid', shipmentinternalid);	

				var fields = new Array();
				var values = new Array();
				fields[1] ='custrecord_ship_nsconf_no';
				values[1] = nsConfirmnumber;

				var updatefields = nlapiSubmitField('customrecord_ship_manifest',shipmentinternalid,fields,values);
			}
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in UpdateAllShipManifestRecord', exp);	
	}

	nlapiLogExecution('DEBUG', 'Out of UpdateAllShipManifestRecord', nsConfirmnumber);	
}
function createItemFulfillment(SalesOrderInternalId,vebizOrdNo,vWMSCarrier,confirmLotToNS,nsrefno,vcontainerLp,wavenumber,
		itemloc,fulfillmenttask,packtaskid,LoadSalesOrderRec)
{	
	nlapiLogExecution('Debug', 'Into createItemFulfillment',TimeStampinSec());

	var str4 = 'SalesOrderInternalId. = ' + SalesOrderInternalId + '<br>';
	str4 = str4 + 'vebizOrdNo. = ' + vebizOrdNo + '<br>';
	str4 = str4 + 'vWMSCarrier. = ' + vWMSCarrier + '<br>';
	str4 = str4 + 'confirmLotToNS. = ' + confirmLotToNS + '<br>';	
	str4 = str4 + 'nsrefno. = ' + nsrefno + '<br>';
	str4 = str4 + 'vcontainerLp. = ' + vcontainerLp + '<br>';
	str4 = str4 + 'wavenumber. = ' + wavenumber + '<br>';
	str4 = str4 + 'itemloc. = ' + itemloc + '<br>';
	str4 = str4 + 'fulfillmenttask. = ' + fulfillmenttask + '<br>';
	str4 = str4 + 'packtaskid. = ' + packtaskid + '<br>';

	nlapiLogExecution('DEBUG', 'Function Parameters', str4);


	var userId = nlapiGetUser();
	//Post Itemfulfillment at Order level? (Y or N)
	var FULFILLMENTATORDERLEVEL = getSystemRuleValue('IF001');
	nlapiLogExecution('DEBUG', 'FULFILLMENTATORDERLEVEL', FULFILLMENTATORDERLEVEL);
	var lineArray = new Array();
	var filterLineArray = new Array();
	var filterItemArray =new Array(); // For distinct item ids
	//var AddTotalArray = new Array();
	var itemListArray= new Array();
	var ItemArray= new Array();
	var filter= new Array();
	var TransformRecId = null;

	if(SalesOrderInternalId != null && SalesOrderInternalId != '')
		filter.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId)); 

	filter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); //PICK

	if(fulfillmenttask=='PICK')
		filter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['9']));//9 //Picks generated
	else
		filter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['8','9']));//9 //Picks generated or Picks confirmed

	/*if(vWMSCarrier != null && vWMSCarrier != '')
		filter.push(new nlobjSearchFilter('custrecord_ebizwmscarrier', null, 'anyof', vWMSCarrier));*/ //For WMS Carrier

	if(FULFILLMENTATORDERLEVEL!='Y')
	{
		filter.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'is', vcontainerLp));
	}
	else
	{
		if(vebizOrdNo !=null && vebizOrdNo!='')
			filter.push(new nlobjSearchFilter('name', null,'is', vebizOrdNo));
	}

	filter.push(new nlobjSearchFilter('custrecord_act_qty',  null,'notequalto','0')); 
	filter.push(new nlobjSearchFilter('custrecord_act_qty',  null,'notequalto','0.0'));
	filter.push(new nlobjSearchFilter('isinactive',null,'is','F'));

	var columns= new Array(); 

	columns[0] = new nlobjSearchColumn('custrecord_act_qty');
	columns[1] = new nlobjSearchColumn('custrecord_line_no');
	columns[2]= new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[3]= new nlobjSearchColumn('custrecord_act_end_date');

	var opentaskordersearchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filter,columns);

	nlapiLogExecution('Debug', 'opentaskordersearchresult',opentaskordersearchresult);

	if(opentaskordersearchresult==null || opentaskordersearchresult=='') // To make sure there is not records exists with Picks generated stage for the order
	{
		nlapiLogExecution('Debug', 'No Open Records. All are Pack Confirmaed');
		var QuilfiedItemArray=new Array();
		var QuilfiedItemLineArray=new Array();
		var QuilfiedItemLineKitArray=new Array();
		var filters= new Array();					

		if(SalesOrderInternalId != null && SalesOrderInternalId != '')
			filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId)); 

		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); // PICK
		if(fulfillmenttask=='PICK')
			filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['8']));//8 //Picks confirmed
		else
			filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['28']));//28//Pack Complete
		filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',  null,'isempty')); // NS REF no is empty
		/*** Below change merged from Factory mation production account on 1st March 2013 by Ganesh ***/
		filters.push(new nlobjSearchFilter('custrecord_act_qty',  null,'notequalto','0')); 

		filters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
		/***Upto here***/
		/*if(vWMSCarrier != null && vWMSCarrier != '')
			filters.push(new nlobjSearchFilter('custrecord_ebizwmscarrier', null, 'anyof', vWMSCarrier));*/ //For WMS Carrier

		if(FULFILLMENTATORDERLEVEL!='Y')
		{
			filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'is', vcontainerLp));
		}
		else
		{
			if(vebizOrdNo !=null && vebizOrdNo!='')
				filters.push(new nlobjSearchFilter('name', null,'is', vebizOrdNo));
		}

		var column =new Array(); 

		column[0] = new nlobjSearchColumn('custrecord_act_qty');
		column[1] = new nlobjSearchColumn('custrecord_line_no');
		column[2]= new nlobjSearchColumn('custrecord_ebiz_sku_no');
		column[3]= new nlobjSearchColumn('custrecord_act_end_date');
		column[4]= new nlobjSearchColumn('internalid');
		column[5]= new nlobjSearchColumn('name');
		column[6]= new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		column[7]= new nlobjSearchColumn('custrecord_ebiz_order_no');
		column[8]= new nlobjSearchColumn('custrecord_serial_no');
		column[9]= new nlobjSearchColumn('custrecord_batch_no');					
		column[10]= new nlobjSearchColumn('custrecord_total_weight');
		column[11]= new nlobjSearchColumn('custrecord_totalcube');
		column[12]= new nlobjSearchColumn('custrecord_parent_sku_no');
		column[13]= new nlobjSearchColumn('custrecord_sku');
		column[1].setSort();
		column[9].setSort();

		var searchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,column);

		var DoLineId="";
		var totalWeight=0.0;var totalcube=0.0;
		if(searchresult !=null && searchresult !='') // SearchResult contains all the open task records of task type PICK and status flag PACK COMPLETE
		{
			nlapiLogExecution('Debug', 'searchresult length',searchresult.length);
			var sum =0;
			var actqty="";
			var linenumber="";
			var skuno="";
			var vBatchno="";
			var vserialno="";

			DoLineId=searchresult[0].getValue('custrecord_ebiz_cntrl_no');

			nlapiLogExecution('Debug', 'searchresult.length',searchresult.length);

			for (l = 0; l < searchresult.length; l++) 
			{  

				if(SalesOrderInternalId==null || SalesOrderInternalId=='')
					SalesOrderInternalId=searchresult[l].getValue('custrecord_ebiz_order_no');	

				itemListArray[l] = new Array();

				actqty=searchresult[l].getValue('custrecord_act_qty');
				linenumber=searchresult[l].getValue('custrecord_line_no');
				skuno=searchresult[l].getValue('custrecord_ebiz_sku_no');
				/***Below code has been merged from Factory mation prodcution by Ganesh on 1st March 2013***/
				/*skuno=searchresult[l].getValue('custrecord_ebiz_sku_no');
				var skuname=searchresult[l].getText('custrecord_ebiz_sku_no');*/
				var parentskuno=searchresult[l].getValue('custrecord_parent_sku_no');
				var skuname=searchresult[l].getText('custrecord_parent_sku_no');
				var skuvalue=searchresult[l].getValue('custrecord_ebiz_sku_no');
				var skutext=searchresult[l].getText('custrecord_sku');
				/***Upto here***/
				vBatchno = searchresult[l].getValue('custrecord_batch_no');
				vserialno = searchresult[l].getValue('custrecord_serial_no');
				if(searchresult[l].getValue('custrecord_total_weight')!=null && searchresult[l].getValue('custrecord_total_weight')!='')
				{
					totalWeight=totalWeight+parseFloat(searchresult[l].getValue('custrecord_total_weight'));
				}
				if(searchresult[l].getValue('custrecord_totalcube')!=null && searchresult[l].getValue('custrecord_totalcube')!='')
				{
					totalcube=totalcube+parseFloat(searchresult[l].getValue('custrecord_totalcube'));
				}
				itemListArray[l][0]=actqty;
				itemListArray[l][1]=skuno;
				itemListArray[l][2]=linenumber;
				itemListArray[l][3]=vBatchno;
				itemListArray[l][4]=vserialno;
				/***Below code has been merged from factory mation production by Ganesh on 1st March 2013***/
				itemListArray[l][5]=skuname;
				itemListArray[l][6]=parentskuno;
				itemListArray[l][7]=skuvalue;
				itemListArray[l][8]=skutext;
				/***Upto here ***/
				ItemArray.push(skuno);
				lineArray.push(linenumber);
			}



			// The below logic is to get all the distinct line nos to filterLineArray . This contails distinct lines nos for the order.
			label:for(var i=0; i<lineArray.length;i++ )
			{  
				for(var j=0; j<filterLineArray.length;j++ )
				{
					if(filterLineArray [j]==lineArray [i]) 
						continue label;
				}
				filterLineArray [filterLineArray .length] = lineArray [i];
			}

			filterLineArray.sort(function(a,b){return a - b;});

			// The below logic is to get all the distinct Item ids filterItemArray . This contains distinct Item ids for the order.
			label2:for(var m=0; m<ItemArray.length;m++ )
			{  
				for(var p=0; p<filterItemArray.length;p++ )
				{
					if(filterItemArray[p]==ItemArray[m]) 
						continue label2;
				}
				filterItemArray[filterItemArray.length] = ItemArray[m];
			}

			// To fetch Item dims from Item Array
			var vItemDimsArr = geteBizItemDimensionsArray(filterItemArray);
			if(vItemDimsArr !=null && vItemDimsArr != "")
			{
				nlapiLogExecution('Debug', 'vItemDimsArr.length', vItemDimsArr.length);
			}	

			var TransformRec;

			var filters= new Array();

			filters.push(new nlobjSearchFilter('internalid', null,'is', SalesOrderInternalId));
			filters.push(new nlobjSearchFilter('mainline',null,'is','T'));

			var column =new Array(); 

			column[0] = new nlobjSearchColumn('custbody_nswmssoordertype');
			column[1] = new nlobjSearchColumn('custbody_total_weight');

			var trantype = nlapiLookupField('transaction', SalesOrderInternalId, 'recordType');
			nlapiLogExecution('Debug', 'trantype', trantype);
			var vAdvBinManagement=false;

			var ctx = nlapiGetContext();
			if(ctx != null && ctx != '')
			{
				if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
					vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
			}  
			nlapiLogExecution('Debug', 'vAdvBinManagement', vAdvBinManagement);
			if(trantype=="salesorder")
			{
				var orderTypeSearchResult = nlapiSearchRecord('salesorder',null,filters,column);
				if(orderTypeSearchResult!=null && orderTypeSearchResult.length>0)
				{
					try
					{
						if(orderTypeSearchResult[0].getValue('custbody_total_weight')!=null && orderTypeSearchResult[0].getValue('custbody_total_weight')!='') 					
						{
							totalWeight=totalWeight+parseFloat(orderTypeSearchResult[0].getValue('custbody_total_weight'));
						}
						nlapiLogExecution('Debug', 'from so', 'from so');
						nlapiLogExecution('Debug', 'Time Stamp at the start of TransformRecord',TimeStampinSec());
						TransformRec = nlapiTransformRecord('salesorder', SalesOrderInternalId, 'itemfulfillment');
						nlapiLogExecution('Debug', 'Time Stamp at the end of TransformRecord',TimeStampinSec());
					}
					catch(e) {


						var exceptionname='SalesOrder';
						var functionality='Creating Item Fulfillment';
						var trantype=2;
						var reference2="";
						var reference3="";
						var reference4 ="";
						var reference5 ="";
						var alertType=1;//1 for exception and 2 for report
						var exceptiondetails=e;
						if ( e instanceof nlobjError )
						{
							nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
							exceptiondetails=e.getCode() + '\n' + e.getDetails();
						}
						else
						{
							exceptiondetails=e.toString();
							nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
						}
						errSendMailByProcess(trantype,e,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
						UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,exceptiondetails);
						InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,SalesOrderInternalId);
						nlapiLogExecution('Debug', 'Exception in TransformRec (salesorder) : ', e);		


					}

				}
			}
			else if(trantype=="vendorreturnauthorization")
			{
				try
				{
					nlapiLogExecution('Debug', 'from to', 'from to');
					TransformRec = nlapiTransformRecord('vendorreturnauthorization', SalesOrderInternalId, 'itemfulfillment');
				}

				catch(e) {

					var exceptionname='vendorreturnauthorization';
					var functionality='Creating Item Fulfillment';
					var trantype=2;
					var reference2="";
					var reference3="";
					var reference4 ="";
					var reference5 ="";
					var alertType=1;//1 for exception and 2 for report
					var exceptiondetails=e;
					if ( e instanceof nlobjError )
					{
						nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
						exceptiondetails=e.getCode() + '\n' + e.getDetails();
					}
					else
					{
						exceptiondetails=e.toString();
						nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
					}
					errSendMailByProcess(trantype,e,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
					InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,SalesOrderInternalId);
					UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,exceptiondetails);
					nlapiLogExecution('Debug', 'Exception in TransformRec (salesorder) : ', e);		



				}
			}
			else
			{
				try
				{
					nlapiLogExecution('Debug', 'from to', 'from to');
					TransformRec = nlapiTransformRecord('transferorder', SalesOrderInternalId, 'itemfulfillment');
				}

				catch(e) {

					var exceptionname='TransferOrder';
					var functionality='Creating Item Fulfillment';
					var trantype=2;
					var reference2="";
					var reference3="";
					var reference4 ="";
					var reference5 ="";
					var alertType=1;//1 for exception and 2 for report
					var exceptiondetails=e;
					if ( e instanceof nlobjError )
					{
						nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
						exceptiondetails=e.getCode() + '\n' + e.getDetails();
					}
					else
					{
						exceptiondetails=e.toString();
						nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
					}
					errSendMailByProcess(trantype,e,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
					InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,SalesOrderInternalId);
					UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,exceptiondetails);
					nlapiLogExecution('Debug', 'Exception in TransformRec (salesorder) : ', e);		



				}
			}
			/*var itemnoarr = new Array();
			for(var q=0; q<filterLineArray.length;q++ ) // This array contains all disctinct lines no for the order
			{
				if(TransformRec!=null)
				{				
					var vitemno = TransformRec.getLineItemValue('item', 'item', q+1);
					itemnoarr.push(vitemno);
				}
			}*/
			var itemnoarr = new Array();
			for(var q=0; q<filterLineArray.length;q++ ) // This array contains all disctinct lines no for the order
			{
				if(TransformRec!=null)
				{
					var SOItemLength = TransformRec.getLineItemCount('item');
					for(var vcount=1;vcount<=SOItemLength;vcount++)
					{
						var itemLinevalue = TransformRec.getLineItemValue('item', 'line', vcount);
						if(itemLinevalue==filterLineArray[q])
						{		
							var vitemno = TransformRec.getLineItemValue('item', 'item', vcount);
							itemnoarr.push(vitemno);
							break;
						}
					}
				}
			}

			var memberitemsarr = new Array();
			if(itemnoarr!=null && itemnoarr!='')
			{
				memberitemsarr = getMemberItemsDetails(itemnoarr);
			}
			var IsBoolean=new Array();
			var vTobeConfItemCount=0;

			for(var i=0; i<filterLineArray.length;i++ ) // This array contains all disctinct lines no for the order
			{ 
				var context = nlapiGetContext();
				nlapiLogExecution('Debug','Remaining usage at the start of Item fulfillment line# ' + i,context.getRemainingUsage());

				var actuvalqty="";
				var skunumber="";
				var linenumber="";
				var totalqty=0;	
				var ItemType="";
				var batchno="";
				var serialno="";var serialout="";
				var filterlinenumber=filterLineArray[i];
				//By Ganesh for batch# entry
				var vPrevbatch;
				var vPrevLine;
				var vLotNo;
				var VLotNo1;
				var vLotQty=0;
				var vLotTotQty=0;
				var boolfound = false;
				var serialsspaces = "";
				var itemname ='';
				var itemvalue;
				var vLotonly;
				var IsLineClosed="F";
				if(TransformRec!=null)
				{
					//Case # 20126922Â Start
					if(LoadSalesOrderRec !=null && LoadSalesOrderRec!='')
					{
						IsLineClosed = LoadSalesOrderRec.getLineItemValue('item', 'isclosed', i+1);
					}
					itemname = TransformRec.getLineItemText('item', 'item', i+1);
					itemvalue = TransformRec.getLineItemValue('item', 'item', i+1);
					/*** Below code has been merged from factory mation production on 1st March 2013 by Ganesh as part of standard bundle***/
					/*itemname = TransformRec.getLineItemText('item', 'item', filterlinenumber);
				itemvalue = TransformRec.getLineItemValue('item', 'item', filterlinenumber);*/

					for(var y=0; y<itemListArray.length;y++) // This array contails all the data for the order
					{ 
						var vlinenumber =itemListArray[y][2];						
						if(filterlinenumber==vlinenumber)
						{
							itemvalue =itemListArray[y][6];
							itemname =itemListArray[y][5];
							if(itemvalue==null || itemvalue=='')
								itemvalue=itemListArray[y][7];
							itemname=itemListArray[y][8];
						}
					}	
				}
				nlapiLogExecution('Debug', 'itemvalue', itemvalue);
				nlapiLogExecution('Debug', 'itemname', itemname);
				nlapiLogExecution('Debug', 'IsLineClosed', IsLineClosed);
				/***upto here ***/			
				if(itemname!=null && itemname!=''&&IsLineClosed!="T")
				{
					itemname=itemname.replace(/ /g,"-");

					var itemTypesku = nlapiLookupField('item', itemvalue, 'recordType');

					if(itemTypesku=='kititem')
					{
						//Code added on 5th Sep 2013 by Suman.
						//case# 20127164 starts (FULFILLMENTATORDERLEVEL=='F' is changed to FULFILLMENTATORDERLEVEL!='Y')
						if(FULFILLMENTATORDERLEVEL!='Y')
							var KitIsPicked=ISKitItemcompletlyPicked(itemvalue,vebizOrdNo,vWMSCarrier);
						nlapiLogExecution("ERROR","KitIsPicked",KitIsPicked);

						if(KitIsPicked=="true"||FULFILLMENTATORDERLEVEL=='Y')
						{
							//case# 20127164 end
							//End of code as of 5th Sep.
							/*** Below code has been merged from factory mation production on 1st March 2013 by Ganesh as part of standard bundle***/
							var vKitItemArr=new Array();
							var vKitItemQtyArr=new Array();
							var vKitMemberItemQtyArr=new Array();
							/***upto here ***/
							for(var n=0; n<itemListArray.length;n++) // This array contails all the data for the order
							{ 
								linenumber =itemListArray[n][2];


								if(memberitemsarr!=null && memberitemsarr!='')
								{
									nlapiLogExecution('Debug', 'memberitemsarr length', memberitemsarr.length);

									for(var w=0; w<memberitemsarr.length;w++) 
									{
										fulfilmentItem = memberitemsarr[w].getValue('memberitem');
										memberitemqty = memberitemsarr[w].getValue('memberquantity');

										nlapiLogExecution('Debug', 'fulfilmentItem', fulfilmentItem);
										nlapiLogExecution('Debug', 'memberitemqty', memberitemqty);
										nlapiLogExecution('Debug', 'itemListArray[n][1]', itemListArray[n][1]);

										if((filterlinenumber==linenumber && itemListArray[n][1]==fulfilmentItem))
										{
											QuilfiedItemArray.push(itemvalue);
											nlapiLogExecution('Debug', 'QuilfiedItemArray KIT', QuilfiedItemArray);

											QuilfiedItemLineArray.push(linenumber);
											nlapiLogExecution('Debug', 'QuilfiedItemLineArray KIT', QuilfiedItemLineArray);

											QuilfiedItemLineKitArray.push(linenumber);
											nlapiLogExecution('ERROR', 'QuilfiedItemLineKitArray KIT', QuilfiedItemLineKitArray);

											nlapiLogExecution('Debug', 'actuvalqty', actuvalqty);
											nlapiLogExecution('Debug', 'totalqty', totalqty);
											nlapiLogExecution('Debug', 'opentask qty', itemListArray[n][0]);
											nlapiLogExecution('Debug', 'batch', itemListArray[n][3]);
											skunumber=itemListArray[n][1];

											var columns;
											batchno =itemListArray[n][3];

											if(serialno=="")
											{
												serialno =itemListArray[n][4];}
											else
											{ 
												serialno =serialno+","+itemListArray[n][4];
											}

											if(confirmLotToNS=='N' && (itemListArray[n][3]!=null && itemListArray[n][3]!=''))
											{

												vPrevbatch=itemname;
												itemListArray[n][3] = itemname;
												vLotTotQty= parseFloat(vLotTotQty) + parseFloat(itemListArray[n][0]);
												nlapiLogExecution('Debug', 'vLotTotQty', vLotTotQty);
											}

											if(vAdvBinManagement==true)
											{
												if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem")
												{
													nlapiLogExecution('Debug', 'Using vAdvBinManagement2',vAdvBinManagement);
													var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
													compSubRecord.selectNewLineItem('inventoryassignment');
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', itemListArray[n][0]);
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', itemListArray[n][4]);

													compSubRecord.commitLineItem('inventoryassignment');
													compSubRecord.commit();
												}
											}
											/*** Below code has been merged from factory mation production on 1st March 2013 by Ganesh as part of standard bundle***/
											if (boolfound) {
												if(vKitItemArr.indexOf(fulfilmentItem)!= -1)
												{
													vKitItemQtyArr[vKitItemArr.indexOf(fulfilmentItem)]= parseInt(vKitItemQtyArr[vKitItemArr.indexOf(fulfilmentItem)]) + parseInt(itemListArray[n][0]);

												}
												else
												{	
													vKitItemArr.push(fulfilmentItem);
													vKitItemQtyArr.push(itemListArray[n][0]);
													vKitMemberItemQtyArr.push(memberitemqty);
												}
												totalqty = parseFloat(totalqty) + parseFloat(itemListArray[n][0]);
												/***Upto here***/

												vBatchno += " " + itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
												serialsspaces += " " + itemListArray[n][4];


												if(vPrevLine==linenumber && vPrevbatch==itemListArray[n][3])
												{
													if(itemListArray[n][0] != null && itemListArray[n][0] != "")
														vLotQty= parseFloat(vLotQty) + parseFloat(itemListArray[n][0]);
												}
												else
												{ 	
													nlapiLogExecution('Debug', 'itemname2', itemname);										

													if(vLotNo!=null && vLotNo != "")
														vLotNo += "," + vPrevbatch +"(" + vLotQty + ")";
													else
														vLotNo = vPrevbatch +"(" + vLotQty + ")";

													if(vAdvBinManagement)
													{
														if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
														{
															nlapiLogExecution('Debug', 'Using vAdvBinManagement3',vAdvBinManagement);
															var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
															compSubRecord.selectNewLineItem('inventoryassignment');
															compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vLotQty);
															compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vPrevbatch);

															compSubRecord.commitLineItem('inventoryassignment');
															compSubRecord.commit();
															//New code end

														}
													}

													if(itemListArray[n][0] != null && itemListArray[n][0] != "")
														vLotQty=parseFloat(itemListArray[n][0]);
													vPrevbatch=itemListArray[n][3];
													vPrevLine=linenumber;
												}

											} else {
												if(skunumber!= null && skunumber != "")
												{
													nlapiLogExecution('Debug', 'skunumber', skunumber);
													columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
													ItemType = columns.recordType;
													nlapiLogExecution('Debug', 'ItemType in else', ItemType);
												}
												/***Below code is merged from Factory mation production account on 2nd Mar 2013 by Ganesh***/
												vKitItemArr.push(fulfilmentItem);
												vKitItemQtyArr.push(itemListArray[n][0]);
												vKitMemberItemQtyArr.push(memberitemqty);

												totalqty = parseFloat(itemListArray[n][0]);
												nlapiLogExecution('Debug', 'totalqty10', totalqty);
												//totalqty = Math.floor(parseFloat(itemListArray[n][0])/memberitemqty);
												/***Upto here***/
												vBatchno = itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
												serialsspaces = itemListArray[n][4];

												if(itemListArray[n][0] != null && itemListArray[n][0] != "")
													vLotQty=parseFloat(itemListArray[n][0]);
												vPrevbatch=itemListArray[n][3];
												vPrevLine=linenumber;
											}
											boolfound = true;
											IsBoolean.push("T");
										}
									}
									/***Below code is merged from Factory mation production on 2nd Mar 2013 by Ganesh  as part of standard bundle***/
								}
							}
						}
						if(boolfound==true)
						{
							if(vKitItemQtyArr != null && vKitItemQtyArr != '' && vKitItemQtyArr.length>0)
							{
								totalqty=Math.floor(parseFloat(vKitItemQtyArr[0])/parseFloat(vKitMemberItemQtyArr[0]));
								nlapiLogExecution('Debug', 'totalqty11', totalqty);
							}	
						}/***Upto here***/
					}
					else
					{
						for(var n=0; n<itemListArray.length;n++) // This array contails all the data for the order
						{ 
							linenumber =itemListArray[n][2];

							nlapiLogExecution('Debug', 'linenumber', linenumber);
							nlapiLogExecution('Debug', 'filterlinenumber', filterlinenumber);

							if((filterlinenumber==linenumber))
							{
								QuilfiedItemArray.push(itemvalue);
								nlapiLogExecution('ERROR', 'QuilfiedItemArray Inventory', QuilfiedItemArray);

								QuilfiedItemLineArray.push(linenumber);
								nlapiLogExecution('ERROR', 'QuilfiedItemLineArray Inventory', QuilfiedItemLineArray);

								nlapiLogExecution('Debug', 'actuvalqty', actuvalqty);
								nlapiLogExecution('Debug', 'totalqty', totalqty);
								skunumber=itemListArray[n][1];

								var columns;
								batchno =itemListArray[n][3];

								if(serialno=="")
								{
									serialno =itemListArray[n][4];}
								else
								{ 
									serialno =serialno+","+itemListArray[n][4];
								}

								if(confirmLotToNS=='N' && (itemListArray[n][3]!=null && itemListArray[n][3]!=''))
								{

									vPrevbatch=itemname;
									itemListArray[n][3] = itemname;
									vLotTotQty= parseFloat(vLotTotQty) + parseFloat(itemListArray[n][0]);
									nlapiLogExecution('Debug', 'vLotTotQty', vLotTotQty);
								}
								columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
								ItemType = columns.recordType;

								if (boolfound) {
									totalqty = parseFloat(totalqty) + parseFloat(itemListArray[n][0]);

									vBatchno += " " + itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
									serialsspaces += " " + itemListArray[n][4];


									if(vPrevLine==linenumber && vPrevbatch==itemListArray[n][3])
									{
										if(itemListArray[n][0] != null && itemListArray[n][0] != "")
											vLotQty= parseFloat(vLotQty) + parseFloat(itemListArray[n][0]);
									}
									else
									{ 	
										nlapiLogExecution('Debug', 'itemname2', itemname);										

										if(vLotNo!=null && vLotNo != "")
											vLotNo += "," + vPrevbatch +"(" + vLotQty + ")";
										else
											vLotNo = vPrevbatch +"(" + vLotQty + ")";
										/***Below code is merged form Lexjet production by Ganesh  on 5th Mar 2013***/
//										if(vAdvBinManagement)
//										{
//										if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
//										{

//										nlapiLogExecution('Debug', 'SalesOrderInternalId',SalesOrderInternalId);
//										TransformRec = nlapiTransformRecord('transferorder', SalesOrderInternalId, 'itemfulfillment');
//										nlapiLogExecution('Debug', 'Using vAdvBinManagement5',vAdvBinManagement);
//										nlapiLogExecution('Debug', 'test1','test1');
//										nlapiLogExecution('Debug', 'TransformRec',TransformRec);

//										var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
//										nlapiLogExecution('Debug', 'test2','test2')
//										compSubRecord.selectNewLineItem('inventoryassignment');
//										nlapiLogExecution('Debug', 'Using vLotQty',vLotQty);
//										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vLotQty);
//										nlapiLogExecution('Debug', 'vPrevbatch',vPrevbatch);
//										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vPrevbatch);

//										compSubRecord.commitLineItem('inventoryassignment');
//										compSubRecord.commit();
//										nlapiLogExecution('Debug', 'End','END');
//										//New code end

//										}
//										}
										/***Upto here***/
										if(itemListArray[n][0] != null && itemListArray[n][0] != "")
											vLotQty=parseFloat(itemListArray[n][0]);
										vPrevbatch=itemListArray[n][3];
										vPrevLine=linenumber;
									}

								} else {
									if(skunumber!= null && skunumber != "")
									{
										columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
										ItemType = columns.recordType;
										nlapiLogExecution('Debug', 'ItemType in else', ItemType);
									}
									totalqty = parseFloat(itemListArray[n][0]);
									vBatchno = itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
									serialsspaces = itemListArray[n][4];

									if(itemListArray[n][0] != null && itemListArray[n][0] != "")
										vLotQty=parseFloat(itemListArray[n][0]);
									vPrevbatch=itemListArray[n][3];
									vPrevLine=linenumber;
								}
								boolfound = true;
								IsBoolean.push("T");
							}
						}
					}

					nlapiLogExecution('Debug', 'skuvalue', skunumber);
					nlapiLogExecution('Debug', 'filterlinenumber', filterlinenumber);
					nlapiLogExecution('Debug', 'totalqty', totalqty);
					nlapiLogExecution('Debug', 'batchno', batchno);
					nlapiLogExecution('Debug', 'serialsspaces', serialsspaces);

					var SOLength = TransformRec.getLineItemCount('item');  // To get the no of lines from item fulfillment record
					nlapiLogExecution('Debug', "SO Length", SOLength);    
					var itemIndex=0;
					var SOWMSCarrier = TransformRec.getFieldValue('custbody_salesorder_carrier');
					for (var j = 1; j <= SOLength; j++) {

						var item_id = TransformRec.getLineItemValue('item', 'item', j);
						// To get Item Type
						//Below code commented for governance issue and this is code is not using
						//var columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
						var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', j);
						var itemLineNo = TransformRec.getLineItemValue('item', 'line', j);
						if(trantype=="transferorder"){
							itemLineNo=parseFloat(itemLineNo)-1;
						}
						var NSUOM = TransformRec.getLineItemValue('item', 'units', j);  
						nlapiLogExecution('Debug', "itemIndex", itemIndex); 
						nlapiLogExecution('Debug', "itemLineNo", itemLineNo);
						if (itemLineNo == filterlinenumber) {
							itemIndex=j;    
							nlapiLogExecution('Debug', "itemIndex", itemIndex);
						}
					}
					if(itemIndex!=0 && totalqty!=null && totalqty!='')
					{
						var item_id = TransformRec.getLineItemValue('item', 'item', itemIndex);
						var itemname = TransformRec.getLineItemText('item', 'item', itemIndex);
						var itemLineNo = TransformRec.getLineItemValue('item', 'line', itemIndex);
						//itemname=itemname.replace(" ","-");
						if(itemname!=null && itemname!='')
							itemname=itemname.replace(/ /g,"-");
						var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', itemIndex);
						var itemloc2 = TransformRec.getLineItemValue('item', 'location', itemIndex);
						nlapiLogExecution('Debug', 'itemloc2', itemloc2);

						var NSOrdUOM = TransformRec.getLineItemValue('item', 'units', itemIndex);
						nlapiLogExecution('Debug', 'itemIndex', itemIndex);
						nlapiLogExecution('Debug', 'itemloc2', itemloc2);
						nlapiLogExecution('Debug', 'NSOrdUOM', NSOrdUOM);

						if(NSOrdUOM!=null && NSOrdUOM!="")
						{

							var veBizUOMQty=0;
							var vBaseUOMQty=0;

							if(vItemDimsArr!=null && vItemDimsArr.length>0)
							{
								for(z=0; z < vItemDimsArr.length; z++)
								{
									if(vItemDimsArr[z].getValue('custrecord_ebizitemdims')==item_id)
									{	
										if(vItemDimsArr[z].getValue('custrecord_ebizbaseuom') == 'T')
										{
											vBaseUOMQty = vItemDimsArr[z].getValue('custrecord_ebizqty');
											nlapiLogExecution('Debug', 'vBaseUOMQty', vBaseUOMQty);
										}

										if(NSOrdUOM == vItemDimsArr[z].getValue('custrecord_ebiznsuom'))
										{
											nlapiLogExecution('Debug', 'NS UOM', vItemDimsArr[z].getText('custrecord_ebiznsuom'));
											veBizUOMQty = vItemDimsArr[z].getValue('custrecord_ebizqty');	
											nlapiLogExecution('Debug', 'veBizUOMQty', veBizUOMQty);
										}
									}
								}
								if(veBizUOMQty==null || veBizUOMQty=='')
								{
									veBizUOMQty=vBaseUOMQty;
								}
								if(veBizUOMQty!=0)
								{ //Case# 20123228 start: to accept decimal qty
									//totalqty = Math.floor((parseFloat(totalqty)*parseFloat(vBaseUOMQty))/parseFloat(veBizUOMQty));
									totalqty = (parseFloat(totalqty)*parseFloat(vBaseUOMQty))/parseFloat(veBizUOMQty);
									//Case# 20123228 end:
								}
								nlapiLogExecution('Debug', 'totalqty', totalqty);

								vLotQty =totalqty;
							}
						}

						if (boolfound) {
							nlapiLogExecution('Debug', 'confirmLotToNS',confirmLotToNS);
							if(confirmLotToNS=='N')
								vPrevbatch=itemname;				
							nlapiLogExecution('Debug', 'vPrevbatch',vPrevbatch);


							if(vLotNo!=null && vLotNo != "")
								vLotNo += "," + vPrevbatch +"(" + vLotQty + ")";
							else
								vLotNo = vPrevbatch +"(" + vLotQty + ")";
							//Case # 20126405   start
							TransformRec.selectLineItem('item', itemIndex);
							//Case # 20126405   End
							if(vAdvBinManagement)
							{
								if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
								{
									/***Below code is merged from Lexjet production on 5th march by Ganesh ***/
									if(itemListArray!=null && itemListArray!='')
									{
										var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
										for(var i1=0;i1<itemListArray.length;i1++)
										{										

											var opentaskitemid = itemListArray[i1][1];
											nlapiLogExecution('Debug', 'opentaskitemid', opentaskitemid);
											nlapiLogExecution('Debug', 'soitemid', item_id);
											nlapiLogExecution('Debug', 'filterlinenumber', filterlinenumber);
											nlapiLogExecution('Debug', 'Locallinenumber', Locallinenumber);
											var Locallinenumber =itemListArray[i1][2];
											if(opentaskitemid==item_id && filterlinenumber == Locallinenumber)
											{

												if(itemListArray[i1][0] != null && itemListArray[i1][0] != "")
													vLotQty=parseInt(itemListArray[i1][0]);
												vPrevbatch=itemListArray[i1][3];


//												if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
//												{
												nlapiLogExecution('Debug', 'Using vAdvBinManagement1',vLotQty);
												nlapiLogExecution('Debug', 'Using vPrevbatch',vPrevbatch);


												compSubRecord.selectNewLineItem('inventoryassignment');
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vLotQty);
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vPrevbatch);

												compSubRecord.commitLineItem('inventoryassignment');
											}

										}
										compSubRecord.commit();
									}
									/***upto here***/
									//New code end

								}
							}

							nlapiLogExecution('Debug', 'vLotNo',vLotNo);
							nlapiLogExecution('Debug', 'vLotQty',vLotQty);
							nlapiLogExecution('Debug', 'vLotonly',vLotonly);


							if(vAdvBinManagement)
							{
								if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem")
								{

									var filters= new Array();				
									nlapiLogExecution('Debug', 'vebizOrdNo', vebizOrdNo);
									filters.push(new nlobjSearchFilter('custrecord_serialsono', null,'is', vebizOrdNo));
									filters.push(new nlobjSearchFilter('custrecord_serialsolineno', null,'is', filterlinenumber));
									filters.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null,'anyof', ['8']));
									var column =new Array(); 

									column[0] = new nlobjSearchColumn('custrecord_serialnumber');							

									var searchresultser = nlapiSearchRecord('customrecord_ebiznetserialentry',null,filters,column);
									if(searchresultser!=null && searchresultser!='')
									{
										var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
										//	for ( var b = 0; b < searchresultser.length; b++ ) {//case # 20127174Â  
										for ( var b = 0; b < totalqty; b++ ) {
											serialsspaces=searchresultser[b].getValue('custrecord_serialnumber');

											nlapiLogExecution('Debug', 'Using vAdvBinManagement4',vAdvBinManagement);
											nlapiLogExecution('Debug', 'Using totalqty',totalqty);
											nlapiLogExecution('Debug', 'Using serialsspaces',serialsspaces);

											compSubRecord.selectNewLineItem('inventoryassignment');
											//Case # 20126405 Start
											compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
											//Case # 20126405 End
											compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serialsspaces);

											compSubRecord.commitLineItem('inventoryassignment');


										}
										compSubRecord.commit();
									}	
								}
							}
							TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'T');
							nlapiLogExecution('Debug', 'totalqty', totalqty);
							TransformRec.setCurrentLineItemValue('item', 'quantity', parseFloat(totalqty).toFixed(5));
							nlapiLogExecution('Debug', 'itemloc2', itemloc2);
							TransformRec.setCurrentLineItemValue('item', 'location', itemloc2);

							nlapiLogExecution('Debug', 'ItemType', ItemType);
							if(!vAdvBinManagement)
							{	
								if (ItemType == "lotnumberedinventoryitem") {
									nlapiLogExecution('Debug', 'vLotNo', vLotNo);									
									TransformRec.setCurrentLineItemValue('item','serialnumbers', vLotNo);
									nlapiLogExecution('Debug', 'vBatchno', vBatchno);
								}
								else if(ItemType == "lotnumberedassemblyitem") 
								{
									TransformRec.setCurrentLineItemValue('item','serialnumbers', vLotNo);
									nlapiLogExecution('Debug', 'vLotNo', vLotNo);
								}
								else if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem") {
									var serialsspaces = "";
									if(serialsspaces==null || serialsspaces=='')
									{
										var filters= new Array();				
										nlapiLogExecution('Debug', 'vebizOrdNo', vebizOrdNo);
										filters.push(new nlobjSearchFilter('custrecord_serialsono', null,'is', vebizOrdNo));
										filters.push(new nlobjSearchFilter('custrecord_serialsolineno', null,'is', filterlinenumber));
										filters.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null,'anyof', ['8']));
										var column =new Array(); 

										column[0] = new nlobjSearchColumn('custrecord_serialnumber');							

										var searchresultser = nlapiSearchRecord('customrecord_ebiznetserialentry',null,filters,column);
										if(searchresultser!=null && searchresultser!='')
										{
											for ( var b = 0; b < searchresultser.length; b++ ) {
												if(serialsspaces==null || serialsspaces=='')
												{
													serialsspaces=searchresultser[b].getValue('custrecord_serialnumber');
												}
												else
												{
													serialsspaces += " " + searchresultser[b].getValue('custrecord_serialnumber');
												}
											}
										}									
										nlapiLogExecution('Debug', 'serialsspaces', serialsspaces);
									}
									nlapiLogExecution('Debug', 'before serialsspaces', serialsspaces);
									TransformRec.setCurrentLineItemValue('item','serialnumbers', serialsspaces);
								}
							}

							vLotNo="";
						}
						TransformRec.commitLineItem('item');
					}
					else
					{
						nlapiLogExecution('Debug', 'itemIndex in else', itemIndex);


						if(itemIndex != 0)
						{
							TransformRec.selectLineItem('item', itemIndex);


							TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'F');


							TransformRec.commitLineItem('item');
						}
					}
				}
			}
			//Case# 20124622 Start//Issue Fix for the so having lines with different site then itemfulfillment was failing if we generate the wave for single site
			var SOLengthnew;
			if(TransformRec!=null && TransformRec!='')
				SOLengthnew = TransformRec.getLineItemCount('item');

			if(parseInt(filterLineArray.length)!=parseInt(SOLengthnew))
			{
				for(var k1=1;k1<=SOLengthnew;k1++)
				{
					var isLinenoComitted=false;
					var linenum= TransformRec.getLineItemValue('item', 'line', k1);
					for(var k2=0;k2<filterLineArray.length;k2++)
					{
						var filterlineno=filterLineArray[k2];
						if(parseInt(linenum)==parseInt(filterlineno))
						{
							isLinenoComitted=true;
							break;
						}

					}
					if(isLinenoComitted==false)
					{
						nlapiLogExecution('Debug', 'itemIndex in', k1);
						TransformRec.selectLineItem('item', k1);

						TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'F');

						TransformRec.commitLineItem('item');
					}
				}
			}
			//Case# 20124622 End

			//nlapiLogExecution('Debug', 'itemloc', itemloc);
			nlapiLogExecution('ERROR', 'Before Submit', IsBoolean);
			nlapiLogExecution('ERROR', 'ISBoolean.indexOf(T)', IsBoolean.indexOf("T"));
			if(parseInt(IsBoolean.indexOf("T"))!=parseInt(-1))
			{
				nlapiLogExecution('ERROR', "INTOIF");
				boolfound=true;
			}
			nlapiLogExecution('ERROR', 'boolfound', boolfound);
			nlapiLogExecution('ERROR', 'TransformRec', TransformRec);

			if(TransformRec!=null && boolfound==true)
			{
				nlapiLogExecution('Debug', 'Time Stamp at the start of nlapiSubmitRecord',TimeStampinSec());
				try
				{
					TransformRecId = nlapiSubmitRecord(TransformRec, true);
					nlapiLogExecution('Debug', 'Time Stamp at the end of nlapiSubmitRecord',TimeStampinSec());
				}
				catch(e)
				{
					var exceptionname='SalesOrder';
					var functionality='Creating Item Fulfillment';
					var trantype=2;
					var reference2="";
					var reference3="";
					var reference4 ="";
					var reference5 ="";
					var alertType=1;//1 for exception and 2 for report
					var exceptiondetails=e;
					if ( e instanceof nlobjError )
					{
						nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
						exceptiondetails=e.getCode() + '\n' + e.getDetails();
					}
					else
					{
						exceptiondetails=e.toString();
						nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
					}
					InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,SalesOrderInternalId);

					var ErrorCode=e.getCode();
					var vErrorMsg=e.message;
					UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,exceptiondetails);
				}
				nlapiLogExecution('ERROR', 'After Submit', TransformRecId);

				/*for ( var i = 0; i < searchresult.length; i++ ) {
					//nlapiLogExecution('ERROR', 'searchresult[i].getId()', searchresult[i].getId());
					//nlapiLogExecution('ERROR', 'TransformRecId', TransformRecId);
					nlapiSubmitField(searchresult[i].getRecordType(), searchresult[i].getId(), 'custrecord_ebiz_nsconfirm_ref_no', TransformRecId);
				}*/
				//Code added on 5th Sep 2013 by suman.
				nlapiLogExecution('ERROR', 'QuilfiedItemArray',QuilfiedItemArray);
				nlapiLogExecution('ERROR', 'QuilfiedItemLineArray',QuilfiedItemLineArray);
				if(QuilfiedItemArray!=null&&QuilfiedItemArray!=""&&QuilfiedItemArray.length!=0)
				{
					var QuilfiedItemArray=removeDuplicateElementOld(QuilfiedItemArray);
					var QuilfiedItemLineArray=removeDuplicateElementOld(QuilfiedItemLineArray);
					var QuilfiedItemLineKitArray=removeDuplicateElementOld(QuilfiedItemLineKitArray);
					UpdateAllOpenTaskRecordsWithNSID(QuilfiedItemArray,vebizOrdNo,vWMSCarrier,TransformRecId,QuilfiedItemLineArray,vcontainerLp,QuilfiedItemLineKitArray,FULFILLMENTATORDERLEVEL);
				}
				//End of code as of 5th Sep 2013.

				if(packtaskid!=null && packtaskid!='')
					nlapiSubmitField('customrecord_ebiznet_trn_opentask', packtaskid, 'custrecord_ebiz_nsconfirm_ref_no', TransformRecId);

				/*if(TransformRecId !=null && TransformRecId !='')
					AutoPackingAfterfulfillment(SalesOrderInternalId,itemloc,vebizOrdNo);*/

				if(trantype=="salesorder" && TransformRecId !=null && TransformRecId !='' && totalWeight != null && totalWeight != '' && parseFloat(totalWeight) != 0 && totalWeight!=0.0 )
				{
					totalcube = roundNumber(totalcube, 2);

					var fields = new Array();
					var values = new Array();

					fields.push('custbody_total_weight');
					fields.push('custbody_total_vol');

					values.push(parseInt(totalWeight));
					values.push(parseFloat(totalcube));

					nlapiSubmitField('salesorder', SalesOrderInternalId, fields, values);	

					/***Below code is merged from Lexjet production on 5th march by Ganesh and commented ***/
					//UpdateShipmanifestCustomrecord(vebizOrdNo,wavenumber,TransformRecId);
					/***upto here***/

				}
			}
		}
	}	
	return TransformRecId;
	nlapiLogExecution('Debug', 'Time Stamp at the end of createItemFulfillment',TimeStampinSec());
}
function getMemberItemsDetails(itemnoarr)
{
	nlapiLogExecution('Debug', 'Into getMemberItemsDetails', itemnoarr);

	var filters = new Array(); 			 
	filters[0] = new nlobjSearchFilter('internalid', null, 'anyof', itemnoarr);	

	var columns1 = new Array(); 
	columns1[0] = new nlobjSearchColumn( 'memberitem' ); 			
	columns1[1] = new nlobjSearchColumn( 'memberquantity' );
	columns1[2] = new nlobjSearchColumn( 'itemid' ).setSort();

	var searchresults = nlapiSearchRecord( 'item', null, filters, columns1 ); 

	nlapiLogExecution('Debug', 'Out of getMemberItemsDetails', searchresults);

	return searchresults;

}
function UpdateAllOpenTaskRecordsWithNSID(QuilfiedItemArray,vebizOrdNo,vWMSCarrier,TransformRecId,QuilfiedItemLineArray,vcontainerLp,QuilfiedItemLineKitArray,FULFILLMENTATORDERLEVEL)
{
	try
	{
		nlapiLogExecution("Debug","QuilfiedItemArray",QuilfiedItemArray);
		nlapiLogExecution("Debug","QuilfiedItemLineArray",QuilfiedItemLineArray);
		nlapiLogExecution("Debug","QuilfiedItemLineKitArray",QuilfiedItemLineKitArray);
		nlapiLogExecution("Debug","FULFILLMENTATORDERLEVEL",FULFILLMENTATORDERLEVEL);
		nlapiLogExecution("Debug","vcontainerLp",vcontainerLp);
		nlapiLogExecution("Debug","vebizOrdNo",vebizOrdNo);
		nlapiLogExecution("Debug","TransformRecId",TransformRecId);
		var filters= new Array();					

		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); // PICK
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['28']));// PACK COMPLETE
		filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',  null,'isempty')); // NS REF no is empty
		filters.push(new nlobjSearchFilter('custrecord_act_qty',  null,'isnot','0')); 
		filters.push(new nlobjSearchFilter('custrecord_act_qty',  null,'isnot','0.0'));
		if(vWMSCarrier != null && vWMSCarrier != '')
			filters.push(new nlobjSearchFilter('custrecord_ebizwmscarrier', null, 'anyof', vWMSCarrier)); //For WMS Carrier
		if(QuilfiedItemArray != null && QuilfiedItemArray != '')
			filters.push(new nlobjSearchFilter('custrecord_parent_sku_no', null, 'anyof', QuilfiedItemArray));
		if(FULFILLMENTATORDERLEVEL!='Y')
		{
			if(vcontainerLp!=null&&vcontainerLp!="")
				filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'is', vcontainerLp));
		}
		else
			filters.push(new nlobjSearchFilter('name', null,'is', vebizOrdNo));
		var column=new Array();
		column[0]=new nlobjSearchColumn("custrecord_line_no");

		var searchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,column);

		if(searchresult!=null&&searchresult!="")
		{
			for(vcount=0;vcount<searchresult.length;vcount++)
			{
				var actlineno=searchresult[vcount].getValue("custrecord_line_no");
//				nlapiLogExecution("ERROR","actlineno",actlineno);
				for(vlinecount=0;vlinecount<QuilfiedItemLineArray.length;vlinecount++)
				{
//					nlapiLogExecution("ERROR","QuilfiedItemLineArray[vlinecount]",QuilfiedItemLineArray[vlinecount]);
					if(actlineno==QuilfiedItemLineArray[vlinecount])
					{
						var recid=searchresult[vcount].getId();
						nlapiSubmitField(searchresult[vcount].getRecordType(), recid, 'custrecord_ebiz_nsconfirm_ref_no', TransformRecId);
						nlapiLogExecution("Debug","recid",recid);
					}
				}
			}
			if(QuilfiedItemLineKitArray!=null&&QuilfiedItemLineKitArray!="")
				UpdateAllOpenTaskRecordsWithNSID_KitItem(QuilfiedItemArray,vebizOrdNo,vWMSCarrier,TransformRecId,QuilfiedItemLineKitArray,vcontainerLp);
		}
		nlapiLogExecution("ERROR","UpdateAllOpenTaskRecordsWithNSID",QuilfiedItemArray+"/"+vebizOrdNo+"/"+vWMSCarrier+"/"+TransformRecId);
	}
	catch(exp)
	{
		nlapiLogExecution("ERROR","exception in UpdateAllOpenTaskRecordsWithNSID",exp);
	}
}
function UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,ErrorCode)
{
	try
	{
		nlapiLogExecution('Debug',"SalesOrderInternalId",SalesOrderInternalId);
		nlapiLogExecution('Debug',"vebizOrdNo",vebizOrdNo);
		nlapiLogExecution('Debug',"vcontainerLp",vcontainerLp);
		nlapiLogExecution('Debug',"ErrorCode",ErrorCode);

		var filters= new Array();

		if(SalesOrderInternalId != null && SalesOrderInternalId != '')
			filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId));

		filters.push(new nlobjSearchFilter('name', null,'is', vebizOrdNo));
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['28']));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',  null,'isempty'));
		filters.push(new nlobjSearchFilter('custrecord_act_qty',  null,'isnot','0'));
		filters.push(new nlobjSearchFilter('custrecord_act_qty',  null,'isnot','0.0'));

		var searchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,null);
		if(searchresult!=null&&searchresult!="")
		{
			for ( var count = 0; count < searchresult.length; count++)
			{
				var recid=searchresult[count].getId();
				nlapiLogExecution('Debug',"recid",recid);
				nlapiSubmitField("customrecord_ebiznet_trn_opentask",recid,"custrecord_ebiz_error_log",ErrorCode);
			}
		}
	}
	catch(e)
	{
		nlapiLogExecution('Debug',"Exception in UpdateOpenTaskWithExceptionValue",e);
	}
}
/*function createItemFulfillment(SalesOrderInternalId,vebizOrdNo,vcontainerLp,shipcharges,trackingno,pakageweight,
		vNotes,vMastLPNo)
{	
	var confirmLotToNS='Y';
	confirmLotToNS=GetConfirmLotToNS("");
	nlapiLogExecution('Debug', 'confirmLotToNS', confirmLotToNS);

	nlapiLogExecution('Debug', 'Into createItemFulfillment');
	nlapiLogExecution('Debug', 'Time Stamp at the start of createItemFulfillment',TimeStampinSec());

	nlapiLogExecution('Debug', 'vcontainerLp',vcontainerLp);
	nlapiLogExecution('Debug', 'vebizOrdNo',vebizOrdNo);
	nlapiLogExecution('Debug', 'SalesOrderInternalId',SalesOrderInternalId);
	nlapiLogExecution('Debug', 'vMastLPNo',vMastLPNo);

	var filterShip = new Array();
	filterShip.push(new nlobjSearchFilter('custrecord_ship_ref5',null,'is',vMastLPNo));
	//filterShip.push(new nlobjSearchFilter('custrecord_ship_ref3',null,'is',vebizOrdNo));
	filterShip.push(new nlobjSearchFilter('custrecord_ship_trackno',null,'isempty'));

	var manifestList= nlapiSearchRecord('customrecord_ship_manifest',null,filterShip,null);
	nlapiLogExecution('Debug', 'manifestList',manifestList);
	if(manifestList== null || manifestList =='')
	{

		var userId = nlapiGetUser();
		//Post Itemfulfillment at Order level? (Y or N)
		var FULFILLMENTATORDERLEVEL = "";//getSystemRuleValue('IF001');

		var lineArray = new Array();
		var filterLineArray = new Array();
		var filterItemArray =new Array(); // For distinct item ids
		//var AddTotalArray = new Array();
		var itemListArray= new Array();
		var ItemArray= new Array();
		var filter= new Array();
		var vNSConfirmNOLog;
		if(SalesOrderInternalId != null && SalesOrderInternalId != '')
			filter.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId)); 


		filter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); //PICK

		filter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['8','9']));//9 //Picks generated or Picks confirmed

		if(vNotes != 'LTL')
		{
			if(vebizOrdNo !=null && vebizOrdNo!='' && vebizOrdNo!='null' && vebizOrdNo!='undefined')
				{
				nlapiLogExecution('Debug', 'vebizOrdNo1',vebizOrdNo);
			filter.push(new nlobjSearchFilter('name', null,'is', vebizOrdNo));
				}
			//filter.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'is', vcontainerLp));
		}

		if(vMastLPNo != null && vMastLPNo != '')
			filter.push(new nlobjSearchFilter('custrecord_mast_ebizlp_no', null, 'is', vMastLPNo));

		filter.push(new nlobjSearchFilter('custrecord_act_qty',  null,'notequalto','0')); 
		filter.push(new nlobjSearchFilter('custrecord_act_qty',  null,'notequalto','0.0'));
		filter.push(new nlobjSearchFilter('isinactive',null,'is','F'));
		var columns= new Array(); 

		columns[0] = new nlobjSearchColumn('custrecord_act_qty');
		columns[1] = new nlobjSearchColumn('custrecord_line_no');
		columns[2]= new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[3]= new nlobjSearchColumn('custrecord_act_end_date');

		var opentaskordersearchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filter,columns);

		nlapiLogExecution('Debug', 'opentaskordersearchresult',opentaskordersearchresult);

		if(opentaskordersearchresult==null || opentaskordersearchresult=='') // To make sure there is not records exists with Picks generated stage for the order
		{
			nlapiLogExecution('Debug', 'No Open Records. All are Pack Confirmaed');
			var QuilfiedItemArray=new Array();
			var QuilfiedItemLineArray=new Array();
			var filters= new Array();					

			var vAllDistLocs= GetAllLocationsFromOT(SalesOrderInternalId,vebizOrdNo,vcontainerLp,vNotes,vMastLPNo,null);
			var vTotNoofLocs=1;//For null values;
			if(vAllDistLocs != null && vAllDistLocs != '')
				vTotNoofLocs = vAllDistLocs.length;
			nlapiLogExecution('Debug', 'vTotNoofLocs',vTotNoofLocs);
			for(var q=0;q<vTotNoofLocs;q++)
			{	
				var filters = new Array();

				var vActLoc;
				if(vAllDistLocs != null && vAllDistLocs != '')
					vActLoc=vAllDistLocs[q].getValue('custrecord_wms_location',null,'group');

				if(SalesOrderInternalId != null && SalesOrderInternalId != '')
					filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId)); 


				filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); // PICK
				filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['28','14','7','10']));//28//Pack Complete
				filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',  null,'isempty')); // NS REF no is empty
 *//*** Below change merged from Factory mation production account on 1st March 2013 by Ganesh ***//*
				filters.push(new nlobjSearchFilter('custrecord_act_qty',  null,'notequalto','0')); 

				filters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
  *//***Upto here***//*
				if(vNotes != 'LTL')
				{
					//filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'is', vcontainerLp));
					if(vebizOrdNo !=null && vebizOrdNo!='' && vebizOrdNo!='null' && vebizOrdNo!='undefined')
					{
					nlapiLogExecution('Debug', 'vebizOrdNo1',vebizOrdNo);
				filter.push(new nlobjSearchFilter('name', null,'is', vebizOrdNo));
					}
				}
				if(vActLoc != null && vActLoc != '')
					{
					nlapiLogExecution('Debug', 'vActLoc',vActLoc);
					filters.push(new nlobjSearchFilter('custrecord_wms_location', null,'anyof', vActLoc));
					}

				//if(vMastLPNo != null && vMastLPNo != '')
					//filters.push(new nlobjSearchFilter('custrecord_mast_ebizlp_no', null, 'is', vMastLPNo));

				var column =new Array(); 

				//column[0] = new nlobjSearchColumn('custrecord_ebiz_act_solocation');		
				column[0] = new nlobjSearchColumn('custrecord_act_qty');
				column[1] = new nlobjSearchColumn('custrecord_line_no');
				column[2]= new nlobjSearchColumn('custrecord_ebiz_sku_no');
				column[3]= new nlobjSearchColumn('custrecord_act_end_date');
				column[4]= new nlobjSearchColumn('internalid');
				column[5]= new nlobjSearchColumn('name');
				column[6]= new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
				column[7]= new nlobjSearchColumn('custrecord_ebiz_order_no');
				column[8]= new nlobjSearchColumn('custrecord_serial_no');
				column[9]= new nlobjSearchColumn('custrecord_batch_no');					
				column[10]= new nlobjSearchColumn('custrecord_total_weight');
				column[11]= new nlobjSearchColumn('custrecord_totalcube');
				column[12]= new nlobjSearchColumn('custrecord_parent_sku_no');
				column[13]= new nlobjSearchColumn('custrecord_sku');

				column[1].setSort();
				column[9].setSort();

				var searchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,column);

				var DoLineId="";
				var totalWeight=0.0;var totalcube=0.0;
				if(searchresult !=null && searchresult !='') // SearchResult contains all the open task records of task type PICK and status flag PACK COMPLETE
				{
					nlapiLogExecution('Debug', 'searchresult length',searchresult.length);
					var sum =0;
					var actqty="";
					var linenumber="";
					var skuno="";
					var vBatchno="";
					var vserialno="";

					DoLineId=searchresult[0].getValue('custrecord_ebiz_cntrl_no');

					nlapiLogExecution('Debug', 'searchresult.length',searchresult.length);
					nlapiLogExecution('Debug', 'SalesOrderInternalId',SalesOrderInternalId);

					for (l = 0; l < searchresult.length; l++) 
					{  

						if(SalesOrderInternalId==null || SalesOrderInternalId=='')
							SalesOrderInternalId=searchresult[l].getValue('custrecord_ebiz_order_no');	

						itemListArray[l] = new Array();

						actqty=searchresult[l].getValue('custrecord_act_qty');
						linenumber=searchresult[l].getValue('custrecord_line_no');
						skuno=searchresult[l].getValue('custrecord_ebiz_sku_no');
   *//***Below code has been merged from Factory mation prodcution by Ganesh on 1st March 2013***//*
						skuno=searchresult[l].getValue('custrecord_ebiz_sku_no');
				var skuname=searchresult[l].getText('custrecord_ebiz_sku_no');
						var parentskuno=searchresult[l].getValue('custrecord_parent_sku_no');
						var skuname=searchresult[l].getText('custrecord_parent_sku_no');
						var skuvalue=searchresult[l].getValue('custrecord_ebiz_sku_no');
						var skutext=searchresult[l].getText('custrecord_sku');

    *//***Upto here***//*
						vBatchno = searchresult[l].getValue('custrecord_batch_no');
						vserialno = searchresult[l].getValue('custrecord_serial_no');
						if(searchresult[l].getValue('custrecord_total_weight')!=null && searchresult[l].getValue('custrecord_total_weight')!='')
						{
							totalWeight=totalWeight+parseFloat(searchresult[l].getValue('custrecord_total_weight'));
						}
						if(searchresult[l].getValue('custrecord_totalcube')!=null && searchresult[l].getValue('custrecord_totalcube')!='')
						{
							totalcube=totalcube+parseFloat(searchresult[l].getValue('custrecord_totalcube'));
						}
						itemListArray[l][0]=actqty;
						itemListArray[l][1]=skuno;
						itemListArray[l][2]=linenumber;
						itemListArray[l][3]=vBatchno;
						itemListArray[l][4]=vserialno;
     *//***Below code has been merged from factory mation production by Ganesh on 1st March 2013***//*
						itemListArray[l][5]=skuname;
						itemListArray[l][6]=parentskuno;
						itemListArray[l][7]=skuvalue;
						itemListArray[l][8]=skutext;
      *//***Upto here ***//*
						ItemArray.push(skuno);
						lineArray.push(linenumber);
					}



					// The below logic is to get all the distinct line nos to filterLineArray . This contails distinct lines nos for the order.
					label:for(var i=0; i<lineArray.length;i++ )
					{  
						for(var j=0; j<filterLineArray.length;j++ )
						{
							if(filterLineArray [j]==lineArray [i]) 
								continue label;
						}
						filterLineArray [filterLineArray .length] = lineArray [i];
					}

					filterLineArray.sort(function(a,b){return a - b;});

					// The below logic is to get all the distinct Item ids filterItemArray . This contains distinct Item ids for the order.
					label2:for(var m=0; m<ItemArray.length;m++ )
					{  
						for(var p=0; p<filterItemArray.length;p++ )
						{
							if(filterItemArray[p]==ItemArray[m]) 
								continue label2;
						}
						filterItemArray[filterItemArray.length] = ItemArray[m];
					}

					// To fetch Item dims from Item Array
					var vItemDimsArr = geteBizItemDimensionsArray(filterItemArray);
					if(vItemDimsArr !=null && vItemDimsArr != "")
					{
						nlapiLogExecution('Debug', 'vItemDimsArr.length', vItemDimsArr.length);
					}	

					var TransformRec;

					nlapiLogExecution('Debug', 'SalesOrderInternalId', SalesOrderInternalId);

					var filters= new Array();

					filters.push(new nlobjSearchFilter('internalid', null,'is', SalesOrderInternalId));
					filters.push(new nlobjSearchFilter('mainline',null,'is','T'));

					var column =new Array(); 

					column[0] = new nlobjSearchColumn('custbody_nswmssoordertype');
					column[1] = new nlobjSearchColumn('custbody_total_weight');

					var trantype = nlapiLookupField('transaction', SalesOrderInternalId, 'recordType');
					nlapiLogExecution('Debug', 'trantype', trantype);
					var vAdvBinManagement=false;

					var ctx = nlapiGetContext();
					if(ctx != null && ctx != '')
					{
						if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
							vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
					}  
					nlapiLogExecution('Debug', 'vAdvBinManagement', vAdvBinManagement);
					if(trantype=="salesorder")
					{
						var orderTypeSearchResult = nlapiSearchRecord('salesorder',null,filters,column);
						if(orderTypeSearchResult!=null && orderTypeSearchResult.length>0)
						{
							try
							{
								if(orderTypeSearchResult[0].getValue('custbody_total_weight')!=null && orderTypeSearchResult[0].getValue('custbody_total_weight')!='') 					
								{
									totalWeight=totalWeight+parseFloat(orderTypeSearchResult[0].getValue('custbody_total_weight'));
								}
								nlapiLogExecution('Debug', 'from so', 'from so');
								nlapiLogExecution('Debug', 'Time Stamp at the start of TransformRecord',TimeStampinSec());
								TransformRec = nlapiTransformRecord('salesorder', SalesOrderInternalId, 'itemfulfillment');
								nlapiLogExecution('Debug', 'Time Stamp at the end of TransformRecord',TimeStampinSec());
							}
							catch(e) {


								var exceptionname='SalesOrder';
								var functionality='Creating Item Fulfillment';
								var trantype=2;
								nlapiLogExecution('Debug', 'DetailsError', functionality);	
								nlapiLogExecution('Debug', 'vcontainerLp', vcontainerLp);
								nlapiLogExecution('Debug', 'vebizOrdNo352', vebizOrdNo);
								var reference2="";
								var reference3="";
								var reference4 ="";
								var reference5 ="";
								var alertType=1;//1 for exception and 2 for report
								var exceptiondetails=e;
								if ( e instanceof nlobjError )
								{
									nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
									exceptiondetails=e.getCode() + '\n' + e.getDetails();
								}
								else
								{
									exceptiondetails=e.toString();
									nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
								}
								errSendMailByProcess(trantype,e,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
								//UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,exceptiondetails);
								InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,SalesOrderInternalId);
								nlapiLogExecution('Debug', 'Exception in TransformRec (salesorder) : ', e);		


							}

						}
					}
					else
					{
						try
						{
							nlapiLogExecution('Debug', 'from to', 'from to');
							TransformRec = nlapiTransformRecord('transferorder', SalesOrderInternalId, 'itemfulfillment');
						}

						catch(e) {

							var exceptionname='TransferOrder';
							var functionality='Creating Item Fulfillment';
							var trantype=2;
							nlapiLogExecution('Debug', 'DetailsError', functionality);	
							nlapiLogExecution('Debug', 'vcontainerLp', vcontainerLp);
							nlapiLogExecution('Debug', 'vebizOrdNo353', vebizOrdNo);
							var reference2="";
							var reference3="";
							var reference4 ="";
							var reference5 ="";
							var alertType=1;//1 for exception and 2 for report
							var exceptiondetails=e;
							if ( e instanceof nlobjError )
							{
								nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
								exceptiondetails=e.getCode() + '\n' + e.getDetails();
							}
							else
							{
								exceptiondetails=e.toString();
								nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
							}
							errSendMailByProcess(trantype,e,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
							InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,SalesOrderInternalId);
							//UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,exceptiondetails);
							nlapiLogExecution('Debug', 'Exception in TransformRec (salesorder) : ', e);		



						}
					}
					var itemnoarr = new Array();
			for(var q=0; q<filterLineArray.length;q++ ) // This array contains all disctinct lines no for the order
			{
				if(TransformRec!=null)
				{				
					var vitemno = TransformRec.getLineItemValue('item', 'item', q+1);
					itemnoarr.push(vitemno);
				}
			}
					var itemnoarr = new Array();
					for(var q1=0; q1<filterLineArray.length;q1++ ) // This array contains all disctinct lines no for the order
					{
						if(TransformRec!=null)
						{
							var SOItemLength = TransformRec.getLineItemCount('item');
							for(var vcount=1;vcount<=SOItemLength;vcount++)
							{
								var itemLinevalue = TransformRec.getLineItemValue('item', 'line', vcount);
								if(itemLinevalue==filterLineArray[q1])
								{
									nlapiLogExecution("ERROR","filterLineArray[q1]",filterLineArray[q1]);			
									var vitemno = TransformRec.getLineItemValue('item', 'item', vcount);
									nlapiLogExecution("ERROR","vitemno",vitemno);	
									itemnoarr.push(vitemno);
									break;
								}
							}
						}
					}

					var memberitemsarr = new Array();
					if(itemnoarr!=null && itemnoarr!='')
					{
						memberitemsarr = getMemberItemsDetails(itemnoarr);
					}
					var IsBoolean=new Array();
					var vTobeConfItemCount=0;

					for(var i=0; i<filterLineArray.length;i++ ) // This array contains all disctinct lines no for the order
					{ 
						var context = nlapiGetContext();
						nlapiLogExecution('Debug','Remaining usage at the start of Item fulfillment line# ' + i,context.getRemainingUsage());

						var actuvalqty="";
						var skunumber="";
						var linenumber="";
						var totalqty=0;	
						var ItemType="";
						var batchno="";
						var serialno="";var serialout="";
						var filterlinenumber=filterLineArray[i];
						//By Ganesh for batch# entry
						var vPrevbatch;
						var vPrevLine;
						var vLotNo;
						var VLotNo1;
						var vLotQty=0;
						var vLotTotQty=0;
						var boolfound = false;
						var serialsspaces = "";
						var itemname ='';
						var itemvalue;
						var vLotonly;
						if(TransformRec!=null)
						{
							itemname = TransformRec.getLineItemText('item', 'item', i+1);
							itemvalue = TransformRec.getLineItemValue('item', 'item', i+1);
       *//*** Below code has been merged from factory mation production on 1st March 2013 by Ganesh as part of standard bundle***//*
							itemname = TransformRec.getLineItemText('item', 'item', filterlinenumber);
				itemvalue = TransformRec.getLineItemValue('item', 'item', filterlinenumber);
							nlapiLogExecution('Debug', 'itemvalue', itemvalue);
							for(var y=0; y<itemListArray.length;y++) // This array contails all the data for the order
							{ 
								var vlinenumber =itemListArray[y][2];
								nlapiLogExecution('Debug', 'filterlinenumber', filterlinenumber);
								nlapiLogExecution('Debug', 'vlinenumber', vlinenumber);
								if(filterlinenumber==vlinenumber)
								{
									itemvalue =itemListArray[y][6];
									itemname =itemListArray[y][5];
									if(itemvalue==null || itemvalue=='')
										itemvalue=itemListArray[y][7];
									itemname=itemListArray[y][8];
								}
							}	


						}
						nlapiLogExecution('Debug', 'itemvalue', itemvalue);
						nlapiLogExecution('Debug', 'itemname', itemname);

        *//***upto here ***//*			
						if(itemname!=null && itemname!='')
						{
							itemname=itemname.replace(/ /g,"-");

							var itemTypesku = nlapiLookupField('item', itemvalue, 'recordType');

							if(itemTypesku=='kititem')
							{
								//Code added on 5th Sep 2013 by Suman.
								if(FULFILLMENTATORDERLEVEL!='Y')
									var KitIsPicked=ISKitItemcompletlyPicked(itemvalue,vebizOrdNo);
								nlapiLogExecution("ERROR","KitIsPicked",KitIsPicked);

								if(KitIsPicked=="true"||FULFILLMENTATORDERLEVEL=='Y')
								{
									//End of code as of 5th Sep.
         *//*** Below code has been merged from factory mation production on 1st March 2013 by Ganesh as part of standard bundle***//*
									var vKitItemArr=new Array();
									var vKitItemQtyArr=new Array();
									var vKitMemberItemQtyArr=new Array();
          *//***upto here ***//*
									for(var n=0; n<itemListArray.length;n++) // This array contails all the data for the order
									{ 
										linenumber =itemListArray[n][2];

										nlapiLogExecution('Debug', 'linenumber', linenumber);
										nlapiLogExecution('Debug', 'filterlinenumber', filterlinenumber);

										if(memberitemsarr!=null && memberitemsarr!='')
										{
											nlapiLogExecution('Debug', 'memberitemsarr length', memberitemsarr.length);

											for(var w=0; w<memberitemsarr.length;w++) 
											{
												fulfilmentItem = memberitemsarr[w].getValue('memberitem');
												memberitemqty = memberitemsarr[w].getValue('memberquantity');

												nlapiLogExecution('Debug', 'fulfilmentItem', fulfilmentItem);
												nlapiLogExecution('Debug', 'memberitemqty', memberitemqty);
												nlapiLogExecution('Debug', 'itemListArray[n][1]', itemListArray[n][1]);

												if((filterlinenumber==linenumber && itemListArray[n][1]==fulfilmentItem))
												{
													QuilfiedItemArray.push(itemvalue);
													nlapiLogExecution('Debug', 'QuilfiedItemArray KIT', QuilfiedItemArray);

													QuilfiedItemLineArray.push(linenumber);
													nlapiLogExecution('Debug', 'QuilfiedItemLineArray KIT', QuilfiedItemLineArray);
													nlapiLogExecution('Debug', 'actuvalqty', actuvalqty);
													nlapiLogExecution('Debug', 'totalqty', totalqty);
													nlapiLogExecution('Debug', 'opentask qty', itemListArray[n][0]);
													nlapiLogExecution('Debug', 'batch', itemListArray[n][3]);
													skunumber=itemListArray[n][1];

													var columns;
													batchno =itemListArray[n][3];

													if(serialno=="")
													{
														serialno =itemListArray[n][4];}
													else
													{ 
														serialno =serialno+","+itemListArray[n][4];
													}

													if(confirmLotToNS=='N' && (itemListArray[n][3]!=null && itemListArray[n][3]!=''))
													{

														vPrevbatch=itemname;
														itemListArray[n][3] = itemname;
														vLotTotQty= parseFloat(vLotTotQty) + parseFloat(itemListArray[n][0]);
														nlapiLogExecution('Debug', 'vLotTotQty', vLotTotQty);
													}

													if(vAdvBinManagement==true)
													{
														if (ItemType == "serializedinventoryitem")
														{
															nlapiLogExecution('Debug', 'Using vAdvBinManagement2',vAdvBinManagement);
															var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
															compSubRecord.selectNewLineItem('inventoryassignment');
															compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', itemListArray[n][0]);
															compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', itemListArray[n][4]);

															compSubRecord.commitLineItem('inventoryassignment');
															compSubRecord.commit();
														}
													}
           *//*** Below code has been merged from factory mation production on 1st March 2013 by Ganesh as part of standard bundle***//*
													if (boolfound) {
														if(vKitItemArr.indexOf(fulfilmentItem)!= -1)
														{
															vKitItemQtyArr[vKitItemArr.indexOf(fulfilmentItem)]= parseInt(vKitItemQtyArr[vKitItemArr.indexOf(fulfilmentItem)]) + parseInt(itemListArray[n][0]);

														}
														else
														{	
															vKitItemArr.push(fulfilmentItem);
															vKitItemQtyArr.push(itemListArray[n][0]);
															vKitMemberItemQtyArr.push(memberitemqty);
														}
														totalqty = parseFloat(totalqty) + parseFloat(itemListArray[n][0]);
            *//***Upto here***//*

														vBatchno += " " + itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
														serialsspaces += " " + itemListArray[n][4];


														if(vPrevLine==linenumber && vPrevbatch==itemListArray[n][3])
														{
															if(itemListArray[n][0] != null && itemListArray[n][0] != "")
																vLotQty= parseFloat(vLotQty) + parseFloat(itemListArray[n][0]);
														}
														else
														{ 	
															nlapiLogExecution('Debug', 'itemname2', itemname);										

															if(vLotNo!=null && vLotNo != "")
																vLotNo += "," + vPrevbatch +"(" + vLotQty + ")";
															else
																vLotNo = vPrevbatch +"(" + vLotQty + ")";

															if(vAdvBinManagement)
															{
																if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
																{
																	nlapiLogExecution('Debug', 'Using vAdvBinManagement3',vAdvBinManagement);
																	var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
																	compSubRecord.selectNewLineItem('inventoryassignment');
																	compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vLotQty);
																	compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vPrevbatch);

																	compSubRecord.commitLineItem('inventoryassignment');
																	compSubRecord.commit();
																	//New code end

																}
															}

															if(itemListArray[n][0] != null && itemListArray[n][0] != "")
																vLotQty=parseFloat(itemListArray[n][0]);
															vPrevbatch=itemListArray[n][3];
															vPrevLine=linenumber;
														}

													} else {
														if(skunumber!= null && skunumber != "")
														{
															nlapiLogExecution('Debug', 'skunumber', skunumber);
															columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
															ItemType = columns.recordType;
															nlapiLogExecution('Debug', 'ItemType in else', ItemType);
														}
             *//***Below code is merged from Factory mation production account on 2nd Mar 2013 by Ganesh***//*
														vKitItemArr.push(fulfilmentItem);
														vKitItemQtyArr.push(itemListArray[n][0]);
														vKitMemberItemQtyArr.push(memberitemqty);

														totalqty = parseFloat(itemListArray[n][0]);
														nlapiLogExecution('Debug', 'totalqty10', totalqty);
														//totalqty = Math.floor(parseFloat(itemListArray[n][0])/memberitemqty);
              *//***Upto here***//*
														vBatchno = itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
														serialsspaces = itemListArray[n][4];

														if(itemListArray[n][0] != null && itemListArray[n][0] != "")
															vLotQty=parseFloat(itemListArray[n][0]);
														vPrevbatch=itemListArray[n][3];
														vPrevLine=linenumber;
													}
													boolfound = true;
													IsBoolean.push("T");
												}
											}
               *//***Below code is merged from Factory mation production on 2nd Mar 2013 by Ganesh  as part of standard bundle***//*
										}
									}
								}
								if(boolfound==true)
								{
									if(vKitItemQtyArr != null && vKitItemQtyArr != '' && vKitItemQtyArr.length>0)
									{
										totalqty=Math.floor(parseFloat(vKitItemQtyArr[0])/parseFloat(vKitMemberItemQtyArr[0]));
										nlapiLogExecution('Debug', 'totalqty11', totalqty);
									}	
								}*//***Upto here***//*
							}
							else
							{
								for(var n=0; n<itemListArray.length;n++) // This array contails all the data for the order
								{ 
									linenumber =itemListArray[n][2];

									nlapiLogExecution('Debug', 'linenumber', linenumber);
									nlapiLogExecution('Debug', 'filterlinenumber', filterlinenumber);

									if((filterlinenumber==linenumber))
									{
										QuilfiedItemArray.push(itemvalue);
										nlapiLogExecution('ERROR', 'QuilfiedItemArray Inventory', QuilfiedItemArray);

										QuilfiedItemLineArray.push(linenumber);
										nlapiLogExecution('ERROR', 'QuilfiedItemLineArray Inventory', QuilfiedItemLineArray);

										nlapiLogExecution('Debug', 'actuvalqty', actuvalqty);
										nlapiLogExecution('Debug', 'totalqty', totalqty);
										skunumber=itemListArray[n][1];

										var columns;
										batchno =itemListArray[n][3];

										if(serialno=="")
										{
											serialno =itemListArray[n][4];}
										else
										{ 
											serialno =serialno+","+itemListArray[n][4];
										}

										if(confirmLotToNS=='N' && (itemListArray[n][3]!=null && itemListArray[n][3]!=''))
										{

											vPrevbatch=itemname;
											itemListArray[n][3] = itemname;
											vLotTotQty= parseFloat(vLotTotQty) + parseFloat(itemListArray[n][0]);
											nlapiLogExecution('Debug', 'vLotTotQty', vLotTotQty);
										}
										columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
										ItemType = columns.recordType;

										if (boolfound) {
											totalqty = parseFloat(totalqty) + parseFloat(itemListArray[n][0]);

											vBatchno += " " + itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
											serialsspaces += " " + itemListArray[n][4];


											if(vPrevLine==linenumber && vPrevbatch==itemListArray[n][3])
											{
												if(itemListArray[n][0] != null && itemListArray[n][0] != "")
													vLotQty= parseFloat(vLotQty) + parseFloat(itemListArray[n][0]);
											}
											else
											{ 	
												nlapiLogExecution('Debug', 'itemname2', itemname);										

												if(vLotNo!=null && vLotNo != "")
													vLotNo += "," + vPrevbatch +"(" + vLotQty + ")";
												else
													vLotNo = vPrevbatch +"(" + vLotQty + ")";

												if(itemListArray[n][0] != null && itemListArray[n][0] != "")
													vLotQty=parseFloat(itemListArray[n][0]);
												vPrevbatch=itemListArray[n][3];
												vPrevLine=linenumber;
											}

										} else {
											if(skunumber!= null && skunumber != "")
											{
												columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
												ItemType = columns.recordType;
												nlapiLogExecution('Debug', 'ItemType in else', ItemType);
											}
											totalqty = parseFloat(itemListArray[n][0]);
											vBatchno = itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
											serialsspaces = itemListArray[n][4];

											if(itemListArray[n][0] != null && itemListArray[n][0] != "")
												vLotQty=parseFloat(itemListArray[n][0]);
											vPrevbatch=itemListArray[n][3];
											vPrevLine=linenumber;
										}
										boolfound = true;
										IsBoolean.push("T");
									}
								}
							}

							nlapiLogExecution('Debug', 'skuvalue', skunumber);
							nlapiLogExecution('Debug', 'filterlinenumber', filterlinenumber);
							nlapiLogExecution('Debug', 'totalqty', totalqty);
							nlapiLogExecution('Debug', 'batchno', batchno);
							nlapiLogExecution('Debug', 'serialsspaces', serialsspaces);

							var SOLength = TransformRec.getLineItemCount('item');  // To get the no of lines from item fulfillment record
							nlapiLogExecution('Debug', "SO Length", SOLength);    
							var itemIndex=0;
							var SOWMSCarrier = TransformRec.getFieldValue('custbody_salesorder_carrier');
							for (var j = 1; j <= SOLength; j++) {

								var item_id = TransformRec.getLineItemValue('item', 'item', j);
								// To get Item Type
								//Below code commented for governance issue and this is code is not using
								//var columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
								var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', j);
								var itemLineNo = TransformRec.getLineItemValue('item', 'line', j);
								if(trantype=="transferorder"){
									itemLineNo=parseFloat(itemLineNo)-1;
								}
								var NSUOM = TransformRec.getLineItemValue('item', 'units', j);  
								nlapiLogExecution('Debug', "itemIndex", itemIndex); 
								nlapiLogExecution('Debug', "itemLineNo", itemLineNo);
								if (itemLineNo == filterlinenumber) {
									itemIndex=j;    
									nlapiLogExecution('Debug', "itemIndex", itemIndex);
								}
							}
							if(itemIndex!=0 && totalqty!=null && totalqty!='')
							{
								var item_id = TransformRec.getLineItemValue('item', 'item', itemIndex);
								var itemname = TransformRec.getLineItemText('item', 'item', itemIndex);
								var itemLineNo = TransformRec.getLineItemValue('item', 'line', itemIndex);
								//itemname=itemname.replace(" ","-");
								if(itemname!=null && itemname!='')
									itemname=itemname.replace(/ /g,"-");
								var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', itemIndex);
								var itemloc2 = TransformRec.getLineItemValue('item', 'location', itemIndex);
								nlapiLogExecution('Debug', 'itemloc2', itemloc2);

								var NSOrdUOM = TransformRec.getLineItemValue('item', 'units', itemIndex);
								nlapiLogExecution('Debug', 'itemIndex', itemIndex);
								nlapiLogExecution('Debug', 'itemloc2', itemloc2);
								nlapiLogExecution('Debug', 'NSOrdUOM', NSOrdUOM);

								if(NSOrdUOM!=null && NSOrdUOM!="")
								{

									var veBizUOMQty=0;
									var vBaseUOMQty=0;

									if(vItemDimsArr!=null && vItemDimsArr.length>0)
									{
										for(z=0; z < vItemDimsArr.length; z++)
										{
											if(vItemDimsArr[z].getValue('custrecord_ebizitemdims')==item_id)
											{	
												if(vItemDimsArr[z].getValue('custrecord_ebizbaseuom') == 'T')
												{
													vBaseUOMQty = vItemDimsArr[z].getValue('custrecord_ebizqty');
													nlapiLogExecution('Debug', 'vBaseUOMQty', vBaseUOMQty);
												}

												if(NSOrdUOM == vItemDimsArr[z].getValue('custrecord_ebiznsuom'))
												{
													nlapiLogExecution('Debug', 'NS UOM', vItemDimsArr[z].getText('custrecord_ebiznsuom'));
													veBizUOMQty = vItemDimsArr[z].getValue('custrecord_ebizqty');	
													nlapiLogExecution('Debug', 'veBizUOMQty', veBizUOMQty);
												}
											}
										}
										if(veBizUOMQty==null || veBizUOMQty=='')
										{
											veBizUOMQty=vBaseUOMQty;
										}
										if(veBizUOMQty!=0)
										{ //Case# 20123228 start: to accept decimal qty
											//totalqty = Math.floor((parseFloat(totalqty)*parseFloat(vBaseUOMQty))/parseFloat(veBizUOMQty));
											totalqty = (parseFloat(totalqty)*parseFloat(vBaseUOMQty))/parseFloat(veBizUOMQty);
											//Case# 20123228 end:
										}
										nlapiLogExecution('Debug', 'totalqty', totalqty);

										vLotQty =totalqty;
									}
								}

								if (boolfound) {
									nlapiLogExecution('Debug', 'confirmLotToNS',confirmLotToNS);
									if(confirmLotToNS=='N')
										vPrevbatch=itemname;				
									nlapiLogExecution('Debug', 'vPrevbatch',vPrevbatch);

									nlapiLogExecution('Debug', 'vLotNo',vLotNo);
									nlapiLogExecution('Debug', 'vLotQty',vLotQty);
									if(vLotNo!=null && vLotNo != "")
										vLotNo += "," + vPrevbatch +"(" + vLotQty + ")";
									else
										vLotNo = vPrevbatch +"(" + vLotQty + ")";
									//Case # 20126405   start
									TransformRec.selectLineItem('item', itemIndex);
									//Case # 20126405   End
									if(vAdvBinManagement)
									{
										if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
										{
											nlapiLogExecution('Debug', 'itemListArray',itemListArray);
								 *//***Below code is merged from Lexjet production on 5th march by Ganesh ***//*
											if(itemListArray!=null && itemListArray!='')
											{

												var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');

												for(var i1=0;i1<itemListArray.length;i1++)
												{										

													var opentaskitemid = itemListArray[i1][1];
													nlapiLogExecution('Debug', 'opentaskitemid', opentaskitemid);
													nlapiLogExecution('Debug', 'soitemid', item_id);
													nlapiLogExecution('Debug', 'filterlinenumber', filterlinenumber);
													nlapiLogExecution('Debug', 'Locallinenumber', Locallinenumber);
													var Locallinenumber =itemListArray[i1][2];
													if(opentaskitemid==item_id && filterlinenumber == Locallinenumber)
													{

														if(itemListArray[i1][0] != null && itemListArray[i1][0] != "")
															vLotQty=parseInt(itemListArray[i1][0]);
														vPrevbatch=itemListArray[i1][3];


//														if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
//														{
														nlapiLogExecution('Debug', 'Using vAdvBinManagement1',vLotQty);
														nlapiLogExecution('Debug', 'Using vPrevbatch',vPrevbatch);


														compSubRecord.selectNewLineItem('inventoryassignment');
														compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vLotQty);
														compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vPrevbatch);

														compSubRecord.commitLineItem('inventoryassignment');
													}

												}
												compSubRecord.commit();
											}
								  *//***upto here***//*
											//New code end

										}
									}

									nlapiLogExecution('Debug', 'vLotNo',vLotNo);
									nlapiLogExecution('Debug', 'vLotQty',vLotQty);
									nlapiLogExecution('Debug', 'vLotonly',vLotonly);


									if(vAdvBinManagement)
									{
										if (ItemType == "serializedinventoryitem")
										{

											var filters= new Array();				
											nlapiLogExecution('Debug', 'vebizOrdNo', vebizOrdNo);
											filters.push(new nlobjSearchFilter('custrecord_serialsono', null,'is', vebizOrdNo));
											filters.push(new nlobjSearchFilter('custrecord_serialsolineno', null,'is', filterlinenumber));
											filters.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null,'anyof', ['8']));
											var column =new Array(); 

											column[0] = new nlobjSearchColumn('custrecord_serialnumber');							

											var searchresultser = nlapiSearchRecord('customrecord_ebiznetserialentry',null,filters,column);
											if(searchresultser!=null && searchresultser!='')
											{
												var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
												for ( var b = 0; b < searchresultser.length; b++ ) {

													serialsspaces=searchresultser[b].getValue('custrecord_serialnumber');

													nlapiLogExecution('Debug', 'Using vAdvBinManagement4',vAdvBinManagement);
													nlapiLogExecution('Debug', 'Using totalqty',totalqty);
													nlapiLogExecution('Debug', 'Using serialsspaces',serialsspaces);

													compSubRecord.selectNewLineItem('inventoryassignment');
													//Case # 20126405 Start
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
													//Case # 20126405 End
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serialsspaces);

													compSubRecord.commitLineItem('inventoryassignment');


												}
												compSubRecord.commit();
											}	
										}
									}
									TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'T');
									nlapiLogExecution('Debug', 'totalqty', totalqty);
									TransformRec.setCurrentLineItemValue('item', 'quantity', parseFloat(totalqty).toFixed(5));
									nlapiLogExecution('Debug', 'itemloc2', itemloc2);
									TransformRec.setCurrentLineItemValue('item', 'location', itemloc2);

									nlapiLogExecution('Debug', 'ItemType', ItemType);



									if(!vAdvBinManagement)
									{	
										if (ItemType == "lotnumberedinventoryitem") {
											nlapiLogExecution('Debug', 'vLotNo', vLotNo);									
											TransformRec.setCurrentLineItemValue('item','serialnumbers', vLotNo);
											nlapiLogExecution('Debug', 'vBatchno', vBatchno);
										}
										else if(ItemType == "lotnumberedassemblyitem") 
										{
											TransformRec.setCurrentLineItemValue('item','serialnumbers', vLotNo);
											nlapiLogExecution('Debug', 'vLotNo', vLotNo);
										}
										else if (ItemType == "serializedinventoryitem") {
											var serialsspaces = "";
											if(serialsspaces==null || serialsspaces=='')
											{
												var filters= new Array();				
												nlapiLogExecution('Debug', 'vebizOrdNo', vebizOrdNo);
												filters.push(new nlobjSearchFilter('custrecord_serialsono', null,'is', vebizOrdNo));
												filters.push(new nlobjSearchFilter('custrecord_serialsolineno', null,'is', filterlinenumber));
												filters.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null,'anyof', ['8']));
												var column =new Array(); 

												column[0] = new nlobjSearchColumn('custrecord_serialnumber');							

												var searchresultser = nlapiSearchRecord('customrecord_ebiznetserialentry',null,filters,column);
												if(searchresultser!=null && searchresultser!='')
												{
													for ( var b = 0; b < searchresultser.length; b++ ) {
														if(serialsspaces==null || serialsspaces=='')
														{
															serialsspaces=searchresultser[b].getValue('custrecord_serialnumber');
														}
														else
														{
															serialsspaces += " " + searchresultser[b].getValue('custrecord_serialnumber');
														}
													}
												}									
												nlapiLogExecution('Debug', 'serialsspaces', serialsspaces);
											}
											nlapiLogExecution('Debug', 'before serialsspaces', serialsspaces);
											TransformRec.setCurrentLineItemValue('item','serialnumbers', serialsspaces);
										}
									}

									vLotNo="";
								}
								TransformRec.commitLineItem('item');
							}
							else
							{
								nlapiLogExecution('Debug', 'itemIndex in else', itemIndex);
								TransformRec.selectLineItem('item', itemIndex);

								TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'F');

								TransformRec.commitLineItem('item');
							}
						}
					}
					//Case# 20124622 Start//Issue Fix for the so having lines with different site then itemfulfillment was failing if we generate the wave for single site
					var SOLengthnew = TransformRec.getLineItemCount('item');
					if(parseInt(filterLineArray.length)!=parseInt(SOLengthnew))
					{
						for(var k1=1;k1<=SOLengthnew;k1++)
						{
							var isLinenoComitted=false;
							var linenum= TransformRec.getLineItemValue('item', 'line', k1);
							for(var k2=0;k2<filterLineArray.length;k2++)
							{
								var filterlineno=filterLineArray[k2];
								if(parseInt(linenum)==parseInt(filterlineno))
								{
									isLinenoComitted=true;
									break;
								}

							}
							if(isLinenoComitted==false)
							{
								nlapiLogExecution('Debug', 'itemIndex in', k1);
								TransformRec.selectLineItem('item', k1);

								TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'F');

								TransformRec.commitLineItem('item');
							}
						}
					}
					//Case# 20124622 End

					//nlapiLogExecution('Debug', 'itemloc', itemloc);
					nlapiLogExecution('ERROR', 'Before Submit', IsBoolean);
					nlapiLogExecution('ERROR', 'ISBoolean.indexOf(T)', IsBoolean.indexOf("T"));
					if(parseInt(IsBoolean.indexOf("T"))!=parseInt(-1))
					{
						nlapiLogExecution('ERROR', "INTOIF");
						boolfound=true;
					}
					nlapiLogExecution('ERROR', 'boolfound', boolfound);
					nlapiLogExecution('ERROR', 'TransformRec', TransformRec);


					if(TransformRec!=null && boolfound==true)
					{
						var TransformRecId;
						nlapiLogExecution('Debug', 'Time Stamp at the start of nlapiSubmitRecord',TimeStampinSec());
						try
						{
							TransformRec = UpdateItemFulfillment(TransformRec,shipcharges,vcontainerLp,trackingno,pakageweight,vebizOrdNo)
							TransformRecId = nlapiSubmitRecord(TransformRec, true);
							nlapiLogExecution('Debug', 'Time Stamp at the end of nlapiSubmitRecord',TimeStampinSec());
							nlapiLogExecution('Debug', 'TransformRecId', TransformRecId);
						}
						catch(e)
						{
							var exceptionname='SalesOrder';
							var functionality='Creating Item Fulfillment';
							var trantype=2;
							nlapiLogExecution('Debug', 'DetailsError', functionality);	
							nlapiLogExecution('Debug', 'vcontainerLp', vcontainerLp);
							nlapiLogExecution('Debug', 'vebizOrdNo354', vebizOrdNo);
							var reference2="";
							var reference3="";
							var reference4 ="";
							var reference5 ="";
							var alertType=1;//1 for exception and 2 for report
							var exceptiondetails=e;
							if ( e instanceof nlobjError )
							{
								nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
								exceptiondetails=e.getCode() + '\n' + e.getDetails();
							}
							else
							{
								exceptiondetails=e.toString();
								nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
							}
							InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,SalesOrderInternalId);

							var ErrorCode=e.getCode();
							var vErrorMsg=e.message;
							//UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,exceptiondetails);
							nlapiLogExecution('Debug', 'Debug', e.getCode());
							nlapiLogExecution('Debug', 'Debug', e.message);
							nlapiLogExecution('Debug', 'Debug', e);
						}
						nlapiLogExecution('ERROR', 'After Submit', TransformRecId);



						nlapiLogExecution('ERROR', 'QuilfiedItemArray',QuilfiedItemArray);
						nlapiLogExecution('ERROR', 'QuilfiedItemLineArray',QuilfiedItemLineArray);
						if(QuilfiedItemArray!=null&&QuilfiedItemArray!=""&&QuilfiedItemArray.length!=0)
						{
							var QuilfiedItemArray=removeDuplicateElementOld(QuilfiedItemArray);
							var QuilfiedItemLineArray=removeDuplicateElementOld(QuilfiedItemLineArray);

							if(vNSConfirmNOLog != null && vNSConfirmNOLog != '')
								vNSConfirmNOLog = vNSConfirmNOLog + ',' + TransformRecId; 
							else
								vNSConfirmNOLog = TransformRecId;

							nlapiLogExecution('ERROR', 'vNSConfirmNOLog',vNSConfirmNOLog);

							if(vNSConfirmNOLog!=null && vNSConfirmNOLog!='')
							{

								UpdateAllOpenTaskRecordsWithNSID(QuilfiedItemArray,vebizOrdNo,"",TransformRecId,QuilfiedItemLineArray,
										vcontainerLp,vNotes,vMastLPNo,SalesOrderInternalId);
							}
						}
						//End of code as of 5th Sep 2013.

						if(packtaskid!=null && packtaskid!='')
						nlapiSubmitField('customrecord_ebiznet_trn_opentask', packtaskid, 'custrecord_ebiz_nsconfirm_ref_no', TransformRecId);
					} 


				}
			}
		}	
		nlapiLogExecution('Debug', 'Time Stamp at the end of createItemFulfillment',TimeStampinSec());
		nlapiLogExecution('Debug', 'vNSConfirmNOLog',vNSConfirmNOLog);
		return vNSConfirmNOLog;
	//}
}*/
function geteBizItemDimensionsArray(itemidArray)
{
	var searchRec = new Array();
	if(itemidArray!=null && itemidArray!='' && itemidArray.length>0)
	{
		var filter = new Array();
		filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', itemidArray));
		filter.push(new nlobjSearchFilter('custrecord_ebiznsuom', null, 'noneof', '@NONE@'));
		filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	

		var column = new Array();
		column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim') ;
		column[1] = new nlobjSearchColumn('custrecord_ebizqty') ;
		column[2] = new nlobjSearchColumn('custrecord_ebizbaseuom') ;
		column[3] = new nlobjSearchColumn('custrecord_ebiznsuom') ;
		column[4] = new nlobjSearchColumn('custrecord_ebizitemdims') ;
		searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);
	}

	return searchRec;

}
function createItemFulfillmentLTLMultiLine(SalesOrderInternalId,vebizOrdNo,vcontainerLp,shipcharges,trackingno,pakageweight,
		vNotes,vMastLPNo,vtrailer)
{	

	nlapiLogExecution('Debug', 'Into createItemFulfillmentLTLMultiLine');
	nlapiLogExecution('Debug', 'Time Stamp at the start of createItemFulfillmentLTLMultiLine',TimeStampinSec());

	var confirmLotToNS='Y';
	confirmLotToNS=GetConfirmLotToNS("");
	nlapiLogExecution('Debug', 'confirmLotToNS', confirmLotToNS);
	nlapiLogExecution('Debug', 'vcontainerLp',vcontainerLp);
	nlapiLogExecution('Debug', 'vebizOrdNo',vebizOrdNo);
	nlapiLogExecution('Debug', 'SalesOrderInternalId',SalesOrderInternalId);
	nlapiLogExecution('Debug', 'vMastLPNo',vMastLPNo);

	var shipgroupslist=new Array();

	var userId = nlapiGetUser();
	//Post Itemfulfillment at Order level? (Y or N)
	var FULFILLMENTATORDERLEVEL = "";//getSystemRuleValue('IF001');

	var socolumns = new Array();
	var sofilters = new Array();
	var vNSConfirmNOLog;

	sofilters.push(new nlobjSearchFilter('internalid', null, 'is', SalesOrderInternalId));
	sofilters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));	
	// Pending Fulfillment / Partially Fulfilled / Pending Billing_Partially Fulfilled
	sofilters.push(new nlobjSearchFilter('status', null, 'anyof', ['SalesOrd:B','SalesOrd:D','SalesOrd:E']));	
	sofilters.push(new nlobjSearchFilter('shipping', null, 'is', 'F'));//To avoid shipping lines.
	sofilters.push(new nlobjSearchFilter('taxline', null, 'is', 'F'));//To avoid tax lines.

	socolumns[0] = new nlobjSearchColumn('shipgroup');
	socolumns[1] = new nlobjSearchColumn('item');
	socolumns[2] = new nlobjSearchColumn('line').setSort();

	var sosearchresults = nlapiSearchRecord('salesorder', null, sofilters, socolumns);
	if(sosearchresults!=null && sosearchresults!='')
	{
		// The below logic is to get all the distinct ship groups to shipgroupslist. 
		label:for(var u=0; u<sosearchresults.length;u++ )
		{  
			var vshipgroup=sosearchresults[u].getValue('shipgroup');

			if(vshipgroup==null || vshipgroup==''||vshipgroup==0)
				vshipgroup=1;	

			for(var v=0; v<shipgroupslist.length;v++ )
			{
				if(shipgroupslist[v]==vshipgroup) 
					continue label;
			}

			nlapiLogExecution('Debug', 'vshipgroup',vshipgroup);
			shipgroupslist[shipgroupslist.length] = vshipgroup;
		}

	for(var y=0; y<shipgroupslist.length;y++ )
	{
		var shipgroup=shipgroupslist[y];

		var glbitemarray=new Array();

		nlapiLogExecution('Debug', 'shipgroup',shipgroup);

		for(var x=0; x<sosearchresults.length;x++ )
		{
			var vshipgroup2=sosearchresults[x].getValue('shipgroup');

			if(vshipgroup2==null || vshipgroup2==''||vshipgroup2==0)
				vshipgroup2=1;

			if(shipgroup == vshipgroup2)
			{
				nlapiLogExecution('Debug', 'item',sosearchresults[x].getValue('item'));
				glbitemarray.push(sosearchresults[x].getValue('item'));
			}
		}

		nlapiLogExecution('Debug', 'glbitemarray',glbitemarray);

		var lineArray = new Array();
		var filterLineArray = new Array();
		var filterItemArray =new Array(); // For distinct item ids
		//var AddTotalArray = new Array();
		var itemListArray= new Array();
		var ItemArray= new Array();

		var vNSConfirmNOLog;

		nlapiLogExecution('Debug', 'No Open Records. All are Pack Confirmaed');
		var QuilfiedItemArray=new Array();
		var QuilfiedItemLineArray=new Array();
		var filters= new Array();					

		var vAllDistLocs= GetAllLocationsFromOT(SalesOrderInternalId,vebizOrdNo,vcontainerLp,vNotes,vMastLPNo,glbitemarray);
		var vTotNoofLocs=1;//For null values;
		if(vAllDistLocs != null && vAllDistLocs != '')
			vTotNoofLocs = vAllDistLocs.length;
		nlapiLogExecution('Debug', 'vTotNoofLocs',vTotNoofLocs);
		for(var q=0;q<vTotNoofLocs;q++)
		{	
			var vActLoc;
			if(vAllDistLocs != null && vAllDistLocs != '')
				vActLoc=vAllDistLocs[q].getValue('custrecord_wms_location',null,'group');

			nlapiLogExecution('Debug', 'vActLoc',vActLoc);
			nlapiLogExecution('Debug', 'SalesOrderInternalId',SalesOrderInternalId);

			var filters = new Array();

			if(SalesOrderInternalId != null && SalesOrderInternalId != '')
				filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId)); 

			filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); // PICK
			//filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['28','14','7','10']));//28//Pack Complete
			filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['14','10']));//28//Pack Complete
			filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',  null,'isempty')); // NS REF no is empty
			/*** Below change merged from Factory mation production account on 1st March 2013 by Ganesh ***/
			filters.push(new nlobjSearchFilter('custrecord_act_qty',  null,'notequalto','0')); 

			filters.push(new nlobjSearchFilter('isinactive',null,'is','F'));

			if(vActLoc != null && vActLoc != '')
				filters.push(new nlobjSearchFilter('custrecord_ebiz_act_solocation', null,'anyof', vActLoc));

			if(glbitemarray != null && glbitemarray != '')
				filters.push(new nlobjSearchFilter('custrecord_parent_sku_no', null,'anyof', glbitemarray));

			var column =new Array(); 

			//column[0] = new nlobjSearchColumn('custrecord_ebiz_act_solocation');		
			column[0] = new nlobjSearchColumn('custrecord_act_qty');
			column[1] = new nlobjSearchColumn('custrecord_line_no');
			column[2]= new nlobjSearchColumn('custrecord_ebiz_sku_no');
			column[3]= new nlobjSearchColumn('custrecord_act_end_date');
			column[4]= new nlobjSearchColumn('internalid');
			column[5]= new nlobjSearchColumn('name');
			column[6]= new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
			column[7]= new nlobjSearchColumn('custrecord_ebiz_order_no');
			column[8]= new nlobjSearchColumn('custrecord_serial_no');
			column[9]= new nlobjSearchColumn('custrecord_batch_no');					
			column[10]= new nlobjSearchColumn('custrecord_total_weight');
			column[11]= new nlobjSearchColumn('custrecord_totalcube');
			column[12]= new nlobjSearchColumn('custrecord_parent_sku_no');
			column[13]= new nlobjSearchColumn('custrecord_sku');

			column[1].setSort();
			column[9].setSort();

			var searchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,column);

			nlapiLogExecution('Debug', 'searchresult',searchresult);

			var DoLineId="";
			var totalWeight=0.0;var totalcube=0.0;
			if(searchresult !=null && searchresult !='') // SearchResult contains all the open task records of task type PICK and status flag PACK COMPLETE
			{
				nlapiLogExecution('Debug', 'searchresult length',searchresult.length);
				var sum =0;
				var actqty="";
				var linenumber="";
				var skuno="";
				var vBatchno="";
				var vserialno="";

				DoLineId=searchresult[0].getValue('custrecord_ebiz_cntrl_no');

				nlapiLogExecution('Debug', 'searchresult.length',searchresult.length);
				nlapiLogExecution('Debug', 'SalesOrderInternalId',SalesOrderInternalId);

				for (l = 0; l < searchresult.length; l++) 
				{  

					if(SalesOrderInternalId==null || SalesOrderInternalId=='')
						SalesOrderInternalId=searchresult[l].getValue('custrecord_ebiz_order_no');	

					itemListArray[l] = new Array();

					actqty=searchresult[l].getValue('custrecord_act_qty');
					linenumber=searchresult[l].getValue('custrecord_line_no');
					skuno=searchresult[l].getValue('custrecord_ebiz_sku_no');
					/***Below code has been merged from Factory mation prodcution by Ganesh on 1st March 2013***/
					/*skuno=searchresult[l].getValue('custrecord_ebiz_sku_no');
				var skuname=searchresult[l].getText('custrecord_ebiz_sku_no');*/
					var parentskuno=searchresult[l].getValue('custrecord_parent_sku_no');
					var skuname=searchresult[l].getText('custrecord_parent_sku_no');
					var skuvalue=searchresult[l].getValue('custrecord_ebiz_sku_no');
					var skutext=searchresult[l].getText('custrecord_sku');

					/***Upto here***/
					vBatchno = searchresult[l].getValue('custrecord_batch_no');
					vserialno = searchresult[l].getValue('custrecord_serial_no');
					if(searchresult[l].getValue('custrecord_total_weight')!=null && searchresult[l].getValue('custrecord_total_weight')!='')
					{
						totalWeight=totalWeight+parseFloat(searchresult[l].getValue('custrecord_total_weight'));
					}
					if(searchresult[l].getValue('custrecord_totalcube')!=null && searchresult[l].getValue('custrecord_totalcube')!='')
					{
						totalcube=totalcube+parseFloat(searchresult[l].getValue('custrecord_totalcube'));
					}
					itemListArray[l][0]=actqty;
					itemListArray[l][1]=skuno;
					itemListArray[l][2]=linenumber;
					itemListArray[l][3]=vBatchno;
					itemListArray[l][4]=vserialno;
					/***Below code has been merged from factory mation production by Ganesh on 1st March 2013***/
					itemListArray[l][5]=skuname;
					itemListArray[l][6]=parentskuno;
					itemListArray[l][7]=skuvalue;
					itemListArray[l][8]=skutext;
					/***Upto here ***/
					ItemArray.push(skuno);
					lineArray.push(linenumber);
				}

				// The below logic is to get all the distinct line nos to filterLineArray . This contails distinct lines nos for the order.
				label:for(var i=0; i<lineArray.length;i++ )
				{  
					for(var j=0; j<filterLineArray.length;j++ )
					{
						if(filterLineArray [j]==lineArray [i]) 
							continue label;
					}
					filterLineArray [filterLineArray .length] = lineArray [i];
				}

				filterLineArray.sort(function(a,b){return a - b;});

				// The below logic is to get all the distinct Item ids filterItemArray . This contains distinct Item ids for the order.
				label2:for(var m=0; m<ItemArray.length;m++ )
				{  
					for(var p=0; p<filterItemArray.length;p++ )
					{
						if(filterItemArray[p]==ItemArray[m]) 
							continue label2;
					}
					filterItemArray[filterItemArray.length] = ItemArray[m];
				}

				// To fetch Item dims from Item Array
				var vItemDimsArr = geteBizItemDimensionsArray(filterItemArray);
				if(vItemDimsArr !=null && vItemDimsArr != "")
				{
					nlapiLogExecution('Debug', 'vItemDimsArr.length', vItemDimsArr.length);
				}	

				var TransformRec;

				nlapiLogExecution('Debug', 'SalesOrderInternalId', SalesOrderInternalId);

				var filters= new Array();

				filters.push(new nlobjSearchFilter('internalid', null,'is', SalesOrderInternalId));
				filters.push(new nlobjSearchFilter('mainline',null,'is','T'));

				var column =new Array(); 

				column[0] = new nlobjSearchColumn('custbody_nswmssoordertype');
				column[1] = new nlobjSearchColumn('custbody_total_weight');

				var trantype = nlapiLookupField('transaction', SalesOrderInternalId, 'recordType');
				nlapiLogExecution('Debug', 'trantype', trantype);
				var vAdvBinManagement=false;

				var ctx = nlapiGetContext();
				if(ctx != null && ctx != '')
				{
					if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
						vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
				}  
				nlapiLogExecution('Debug', 'vAdvBinManagement', vAdvBinManagement);
				if(trantype=="salesorder")
				{
					var orderTypeSearchResult = nlapiSearchRecord('salesorder',null,filters,column);
					if(orderTypeSearchResult!=null && orderTypeSearchResult.length>0)
					{
						try
						{
							if(orderTypeSearchResult[0].getValue('custbody_total_weight')!=null && orderTypeSearchResult[0].getValue('custbody_total_weight')!='') 					
							{
								totalWeight=totalWeight+parseFloat(orderTypeSearchResult[0].getValue('custbody_total_weight'));
							}
							nlapiLogExecution('Debug', 'from so', 'from so');
							nlapiLogExecution('Debug', 'Time Stamp at the start of TransformRecord',TimeStampinSec());
							//TransformRec = nlapiTransformRecord('salesorder', SalesOrderInternalId, 'itemfulfillment');
							TransformRec = nlapiTransformRecord('salesorder', SalesOrderInternalId, 'itemfulfillment', { 'shipgroup' : shipgroupslist[y]});
							nlapiLogExecution('Debug', 'Time Stamp at the end of TransformRecord',TimeStampinSec());
						}
						catch(e) {


							var exceptionname='SalesOrder';
							var functionality='Creating Item Fulfillment';
							var trantype=2;
							nlapiLogExecution('Debug', 'DetailsError', functionality);	
							nlapiLogExecution('Debug', 'vcontainerLp', vcontainerLp);
							nlapiLogExecution('Debug', 'vebizOrdNo352', vebizOrdNo);
							var reference2="";
							var reference3="";
							var reference4 ="";
							var reference5 ="";
							var alertType=1;//1 for exception and 2 for report
							var exceptiondetails=e;
							if ( e instanceof nlobjError )
							{
								nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
								exceptiondetails=e.getCode() + '\n' + e.getDetails();
							}
							else
							{
								exceptiondetails=e.toString();
								nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
							}
							errSendMailByProcess(trantype,e,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
							//UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,exceptiondetails);
							InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,SalesOrderInternalId);
							nlapiLogExecution('Debug', 'Exception in TransformRec (salesorder) : ', e);		


						}

					}
				}
				else
				{
					try
					{
						nlapiLogExecution('Debug', 'from to', 'from to');
						//TransformRec = nlapiTransformRecord('transferorder', SalesOrderInternalId, 'itemfulfillment');
						TransformRec = nlapiTransformRecord('transferorder', SalesOrderInternalId, 'itemfulfillment', { 'shipgroup' : shipgroupslist[y]});
					}

					catch(e) {

						var exceptionname='TransferOrder';
						var functionality='Creating Item Fulfillment';
						var trantype=2;
						nlapiLogExecution('Debug', 'DetailsError', functionality);	
						nlapiLogExecution('Debug', 'vcontainerLp', vcontainerLp);
						nlapiLogExecution('Debug', 'vebizOrdNo353', vebizOrdNo);
						var reference2="";
						var reference3="";
						var reference4 ="";
						var reference5 ="";
						var alertType=1;//1 for exception and 2 for report
						var exceptiondetails=e;
						if ( e instanceof nlobjError )
						{
							nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
							exceptiondetails=e.getCode() + '\n' + e.getDetails();
						}
						else
						{
							exceptiondetails=e.toString();
							nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
						}
						errSendMailByProcess(trantype,e,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
						InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,SalesOrderInternalId);
						//UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,exceptiondetails);
						nlapiLogExecution('Debug', 'Exception in TransformRec (salesorder) : ', e);		



					}
				}
				var itemnoarr = new Array();
				for(var q1=0; q1<filterLineArray.length;q1++ ) // This array contains all disctinct lines no for the order
				{
					if(TransformRec!=null)
					{
						var SOItemLength = TransformRec.getLineItemCount('item');
						for(var vcount=1;vcount<=SOItemLength;vcount++)
						{
							var itemLinevalue = TransformRec.getLineItemValue('item', 'line', vcount);
							if(itemLinevalue==filterLineArray[q1])
							{
								nlapiLogExecution("ERROR","filterLineArray[q1]",filterLineArray[q1]);			
								var vitemno = TransformRec.getLineItemValue('item', 'item', vcount);
								nlapiLogExecution("ERROR","vitemno",vitemno);	
								itemnoarr.push(vitemno);
								break;
							}
						}
					}
				}

				var memberitemsarr = new Array();
				if(itemnoarr!=null && itemnoarr!='')
				{
					memberitemsarr = getMemberItemsDetails(itemnoarr);
				}
				var IsBoolean=new Array();
				var vTobeConfItemCount=0;

				for(var i=0; i<filterLineArray.length;i++ ) // This array contains all disctinct lines no for the order
				{ 
					var context = nlapiGetContext();
					nlapiLogExecution('Debug','Remaining usage at the start of Item fulfillment line# ' + i,context.getRemainingUsage());

					var actuvalqty="";
					var skunumber="";
					var linenumber="";
					var totalqty=0;	
					var ItemType="";
					var batchno="";
					var serialno="";var serialout="";
					var filterlinenumber=filterLineArray[i];
					//By Ganesh for batch# entry
					var vPrevbatch;
					var vPrevLine;
					var vLotNo;
					var VLotNo1;
					var vLotQty=0;
					var vLotTotQty=0;
					var boolfound = false;
					var serialsspaces = "";
					var itemname ='';
					var itemvalue;
					var vLotonly;
					if(TransformRec!=null)
					{
						itemname = TransformRec.getLineItemText('item', 'item', i+1);
						itemvalue = TransformRec.getLineItemValue('item', 'item', i+1);
						nlapiLogExecution('Debug', 'itemvalue', itemvalue);
						for(var y2=0; y2<itemListArray.length;y2++) // This array contails all the data for the order
						{ 
							var vlinenumber =itemListArray[y2][2];
							if(filterlinenumber==vlinenumber)
							{
								itemvalue =itemListArray[y2][6];
								itemname =itemListArray[y2][5];
								if(itemvalue==null || itemvalue=='')
									itemvalue=itemListArray[y2][7];
								itemname=itemListArray[y2][8];
							}
						}
					}

					/***upto here ***/			
					if(itemname!=null && itemname!='')
					{
						itemname=itemname.replace(/ /g,"-");

						var itemTypesku = nlapiLookupField('item', itemvalue, 'recordType');

						if(itemTypesku=='kititem')
						{
							//Code added on 5th Sep 2013 by Suman.
							if(FULFILLMENTATORDERLEVEL!='Y')
								var KitIsPicked=ISKitItemcompletlyPicked(itemvalue,vebizOrdNo);
							nlapiLogExecution("ERROR","KitIsPicked",KitIsPicked);

							if(KitIsPicked=="true"||FULFILLMENTATORDERLEVEL=='Y')
							{
								//End of code as of 5th Sep.
								/*** Below code has been merged from factory mation production on 1st March 2013 by Ganesh as part of standard bundle***/
								var vKitItemArr=new Array();
								var vKitItemQtyArr=new Array();
								var vKitMemberItemQtyArr=new Array();
								/***upto here ***/
								for(var n=0; n<itemListArray.length;n++) // This array contails all the data for the order
								{ 
									linenumber =itemListArray[n][2];

									if(memberitemsarr!=null && memberitemsarr!='')
									{
										nlapiLogExecution('Debug', 'memberitemsarr length', memberitemsarr.length);

										for(var w=0; w<memberitemsarr.length;w++) 
										{
											fulfilmentItem = memberitemsarr[w].getValue('memberitem');
											memberitemqty = memberitemsarr[w].getValue('memberquantity');

											if((filterlinenumber==linenumber && itemListArray[n][1]==fulfilmentItem))
											{
												QuilfiedItemArray.push(itemvalue);
												nlapiLogExecution('Debug', 'QuilfiedItemArray KIT', QuilfiedItemArray);

												QuilfiedItemLineArray.push(linenumber);

												skunumber=itemListArray[n][1];

												var columns;
												batchno =itemListArray[n][3];

												if(serialno=="")
												{
													serialno =itemListArray[n][4];}
												else
												{ 
													serialno =serialno+","+itemListArray[n][4];
												}

												if(confirmLotToNS=='N' && (itemListArray[n][3]!=null && itemListArray[n][3]!=''))
												{

													vPrevbatch=itemname;
													itemListArray[n][3] = itemname;
													vLotTotQty= parseFloat(vLotTotQty) + parseFloat(itemListArray[n][0]);
													nlapiLogExecution('Debug', 'vLotTotQty', vLotTotQty);
												}

												if(vAdvBinManagement==true)
												{
													if (ItemType == "serializedinventoryitem")
													{
														nlapiLogExecution('Debug', 'Using vAdvBinManagement2',vAdvBinManagement);
														var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
														compSubRecord.selectNewLineItem('inventoryassignment');
														compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', itemListArray[n][0]);
														compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', itemListArray[n][4]);

														compSubRecord.commitLineItem('inventoryassignment');
														compSubRecord.commit();
													}
												}
												/*** Below code has been merged from factory mation production on 1st March 2013 by Ganesh as part of standard bundle***/
												if (boolfound) {
													if(vKitItemArr.indexOf(fulfilmentItem)!= -1)
													{
														vKitItemQtyArr[vKitItemArr.indexOf(fulfilmentItem)]= parseInt(vKitItemQtyArr[vKitItemArr.indexOf(fulfilmentItem)]) + parseInt(itemListArray[n][0]);

													}
													else
													{	
														vKitItemArr.push(fulfilmentItem);
														vKitItemQtyArr.push(itemListArray[n][0]);
														vKitMemberItemQtyArr.push(memberitemqty);
													}
													totalqty = parseFloat(totalqty) + parseFloat(itemListArray[n][0]);
													/***Upto here***/

													vBatchno += " " + itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
													serialsspaces += " " + itemListArray[n][4];


													if(vPrevLine==linenumber && vPrevbatch==itemListArray[n][3])
													{
														if(itemListArray[n][0] != null && itemListArray[n][0] != "")
															vLotQty= parseFloat(vLotQty) + parseFloat(itemListArray[n][0]);
													}
													else
													{ 	
														nlapiLogExecution('Debug', 'itemname2', itemname);										

														if(vLotNo!=null && vLotNo != "")
															vLotNo += "," + vPrevbatch +"(" + vLotQty + ")";
														else
															vLotNo = vPrevbatch +"(" + vLotQty + ")";

														if(vAdvBinManagement)
														{
															if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
															{
																nlapiLogExecution('Debug', 'Using vAdvBinManagement3',vAdvBinManagement);
																var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
																compSubRecord.selectNewLineItem('inventoryassignment');
																compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vLotQty);
																compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vPrevbatch);

																compSubRecord.commitLineItem('inventoryassignment');
																compSubRecord.commit();
																//New code end

															}
														}

														if(itemListArray[n][0] != null && itemListArray[n][0] != "")
															vLotQty=parseFloat(itemListArray[n][0]);
														vPrevbatch=itemListArray[n][3];
														vPrevLine=linenumber;
													}

												} else {
													if(skunumber!= null && skunumber != "")
													{
														nlapiLogExecution('Debug', 'skunumber', skunumber);
														columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
														ItemType = columns.recordType;
														nlapiLogExecution('Debug', 'ItemType in else', ItemType);
													}
													/***Below code is merged from Factory mation production account on 2nd Mar 2013 by Ganesh***/
													vKitItemArr.push(fulfilmentItem);
													vKitItemQtyArr.push(itemListArray[n][0]);
													vKitMemberItemQtyArr.push(memberitemqty);

													totalqty = parseFloat(itemListArray[n][0]);
													nlapiLogExecution('Debug', 'totalqty10', totalqty);
													//totalqty = Math.floor(parseFloat(itemListArray[n][0])/memberitemqty);
													/***Upto here***/
													vBatchno = itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
													serialsspaces = itemListArray[n][4];

													if(itemListArray[n][0] != null && itemListArray[n][0] != "")
														vLotQty=parseFloat(itemListArray[n][0]);
													vPrevbatch=itemListArray[n][3];
													vPrevLine=linenumber;
												}
												boolfound = true;
												IsBoolean.push("T");
											}
										}
										/***Below code is merged from Factory mation production on 2nd Mar 2013 by Ganesh  as part of standard bundle***/
									}
								}
							}
							if(boolfound==true)
							{
								if(vKitItemQtyArr != null && vKitItemQtyArr != '' && vKitItemQtyArr.length>0)
								{
									totalqty=Math.floor(parseFloat(vKitItemQtyArr[0])/parseFloat(vKitMemberItemQtyArr[0]));
									nlapiLogExecution('Debug', 'totalqty11', totalqty);
								}	
							}/***Upto here***/
						}
						else
						{
							for(var n=0; n<itemListArray.length;n++) // This array contails all the data for the order
							{ 
								linenumber =itemListArray[n][2];

								if((filterlinenumber==linenumber))
								{
									QuilfiedItemArray.push(itemvalue);
									QuilfiedItemLineArray.push(linenumber);

									skunumber=itemListArray[n][1];

									var columns;
									batchno =itemListArray[n][3];

									if(serialno=="")
									{
										serialno =itemListArray[n][4];}
									else
									{ 
										serialno =serialno+","+itemListArray[n][4];
									}

									if(confirmLotToNS=='N' && (itemListArray[n][3]!=null && itemListArray[n][3]!=''))
									{

										vPrevbatch=itemname;
										itemListArray[n][3] = itemname;
										vLotTotQty= parseFloat(vLotTotQty) + parseFloat(itemListArray[n][0]);
										nlapiLogExecution('Debug', 'vLotTotQty', vLotTotQty);
									}
									columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
									ItemType = columns.recordType;

									if (boolfound) {
										totalqty = parseFloat(totalqty) + parseFloat(itemListArray[n][0]);

										vBatchno += " " + itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
										serialsspaces += " " + itemListArray[n][4];


										if(vPrevLine==linenumber && vPrevbatch==itemListArray[n][3])
										{
											if(itemListArray[n][0] != null && itemListArray[n][0] != "")
												vLotQty= parseFloat(vLotQty) + parseFloat(itemListArray[n][0]);
										}
										else
										{ 	
											nlapiLogExecution('Debug', 'itemname2', itemname);										

											if(vLotNo!=null && vLotNo != "")
												vLotNo += "," + vPrevbatch +"(" + vLotQty + ")";
											else
												vLotNo = vPrevbatch +"(" + vLotQty + ")";

											if(itemListArray[n][0] != null && itemListArray[n][0] != "")
												vLotQty=parseFloat(itemListArray[n][0]);
											vPrevbatch=itemListArray[n][3];
											vPrevLine=linenumber;
										}

									} else {
										if(skunumber!= null && skunumber != "")
										{
											columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
											ItemType = columns.recordType;
											nlapiLogExecution('Debug', 'ItemType in else', ItemType);
										}
										totalqty = parseFloat(itemListArray[n][0]);
										vBatchno = itemListArray[n][3]+ "("+ parseFloat(itemListArray[n][0]) +")";
										serialsspaces = itemListArray[n][4];

										if(itemListArray[n][0] != null && itemListArray[n][0] != "")
											vLotQty=parseFloat(itemListArray[n][0]);
										vPrevbatch=itemListArray[n][3];
										vPrevLine=linenumber;
									}
									boolfound = true;
									IsBoolean.push("T");
								}
							}
						}

						var SOLength = TransformRec.getLineItemCount('item');  // To get the no of lines from item fulfillment record
						nlapiLogExecution('Debug', "SO Length", SOLength);    
						var itemIndex=0;
						var SOWMSCarrier = TransformRec.getFieldValue('custbody_salesorder_carrier');
						for (var j = 1; j <= SOLength; j++) {

							var item_id = TransformRec.getLineItemValue('item', 'item', j);
							// To get Item Type
							//Below code commented for governance issue and this is code is not using
							//var columns = nlapiLookupField('item', skunumber, [ 'recordType','custitem_ebizserialout' ]);
							var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', j);
							var itemLineNo = TransformRec.getLineItemValue('item', 'line', j);
							if(trantype=="transferorder"){
								itemLineNo=parseFloat(itemLineNo)-1;
							}
							var NSUOM = TransformRec.getLineItemValue('item', 'units', j);  

							if (itemLineNo == filterlinenumber) {
								itemIndex=j;    
								nlapiLogExecution('Debug', "itemIndex", itemIndex);
							}
						}
						if(itemIndex!=0 && totalqty!=null && totalqty!='')
						{
							var item_id = TransformRec.getLineItemValue('item', 'item', itemIndex);
							var itemname = TransformRec.getLineItemText('item', 'item', itemIndex);
							var itemLineNo = TransformRec.getLineItemValue('item', 'line', itemIndex);
							//itemname=itemname.replace(" ","-");
							if(itemname!=null && itemname!='')
								itemname=itemname.replace(/ /g,"-");
							var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', itemIndex);
							var itemloc2 = TransformRec.getLineItemValue('item', 'location', itemIndex);
							nlapiLogExecution('Debug', 'itemloc2', itemloc2);

							var NSOrdUOM = TransformRec.getLineItemValue('item', 'units', itemIndex);

							if(NSOrdUOM!=null && NSOrdUOM!="")
							{
								var veBizUOMQty=0;
								var vBaseUOMQty=0;

								if(vItemDimsArr!=null && vItemDimsArr.length>0)
								{
									for(z=0; z < vItemDimsArr.length; z++)
									{
										if(vItemDimsArr[z].getValue('custrecord_ebizitemdims')==item_id)
										{	
											if(vItemDimsArr[z].getValue('custrecord_ebizbaseuom') == 'T')
											{
												vBaseUOMQty = vItemDimsArr[z].getValue('custrecord_ebizqty');
												nlapiLogExecution('Debug', 'vBaseUOMQty', vBaseUOMQty);
											}

											if(NSOrdUOM == vItemDimsArr[z].getValue('custrecord_ebiznsuom'))
											{
												nlapiLogExecution('Debug', 'NS UOM', vItemDimsArr[z].getText('custrecord_ebiznsuom'));
												veBizUOMQty = vItemDimsArr[z].getValue('custrecord_ebizqty');	
												nlapiLogExecution('Debug', 'veBizUOMQty', veBizUOMQty);
											}
										}
									}
									if(veBizUOMQty==null || veBizUOMQty=='')
									{
										veBizUOMQty=vBaseUOMQty;
									}
									if(veBizUOMQty!=0)
									{ //Case# 20123228 start: to accept decimal qty
										//totalqty = Math.floor((parseFloat(totalqty)*parseFloat(vBaseUOMQty))/parseFloat(veBizUOMQty));
										totalqty = (parseFloat(totalqty)*parseFloat(vBaseUOMQty))/parseFloat(veBizUOMQty);
										//Case# 20123228 end:
									}
									nlapiLogExecution('Debug', 'totalqty', totalqty);

									vLotQty =totalqty;
								}
							}

							if (boolfound) {
								nlapiLogExecution('Debug', 'confirmLotToNS',confirmLotToNS);
								if(confirmLotToNS=='N')
									vPrevbatch=itemname;				

								if(vLotNo!=null && vLotNo != "")
									vLotNo += "," + vPrevbatch +"(" + vLotQty + ")";
								else
									vLotNo = vPrevbatch +"(" + vLotQty + ")";
								//Case # 20126405   start
								TransformRec.selectLineItem('item', itemIndex);
								//Case # 20126405   End
								if(vAdvBinManagement)
								{
									if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
									{
										nlapiLogExecution('Debug', 'itemListArray',itemListArray);
										/***Below code is merged from Lexjet production on 5th march by Ganesh ***/
										if(itemListArray!=null && itemListArray!='')
										{

											var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');

											for(var i1=0;i1<itemListArray.length;i1++)
											{										

												var opentaskitemid = itemListArray[i1][1];
												var Locallinenumber =itemListArray[i1][2];
												if(opentaskitemid==item_id && filterlinenumber == Locallinenumber)
												{

													if(itemListArray[i1][0] != null && itemListArray[i1][0] != "")
														vLotQty=parseInt(itemListArray[i1][0]);
													vPrevbatch=itemListArray[i1][3];


//													if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem")
//													{
													nlapiLogExecution('Debug', 'Using vAdvBinManagement1',vLotQty);
													nlapiLogExecution('Debug', 'Using vPrevbatch',vPrevbatch);


													compSubRecord.selectNewLineItem('inventoryassignment');
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', vLotQty);
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vPrevbatch);

													compSubRecord.commitLineItem('inventoryassignment');
												}

											}
											compSubRecord.commit();
										}
										/***upto here***/
										//New code end

									}
								}

								if(vAdvBinManagement)
								{
									if (ItemType == "serializedinventoryitem")
									{

										var filters= new Array();				
										nlapiLogExecution('Debug', 'vebizOrdNo', vebizOrdNo);
										filters.push(new nlobjSearchFilter('custrecord_serialsono', null,'is', vebizOrdNo));
										filters.push(new nlobjSearchFilter('custrecord_serialsolineno', null,'is', filterlinenumber));
										filters.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null,'anyof', ['8']));
										var column =new Array(); 

										column[0] = new nlobjSearchColumn('custrecord_serialnumber');							

										var searchresultser = nlapiSearchRecord('customrecord_ebiznetserialentry',null,filters,column);
										if(searchresultser!=null && searchresultser!='')
										{
											var compSubRecord = TransformRec.createCurrentLineItemSubrecord('item','inventorydetail');
											for ( var b = 0; b < searchresultser.length; b++ ) {

												serialsspaces=searchresultser[b].getValue('custrecord_serialnumber');

												nlapiLogExecution('Debug', 'Using vAdvBinManagement4',vAdvBinManagement);
												nlapiLogExecution('Debug', 'Using totalqty',totalqty);
												nlapiLogExecution('Debug', 'Using serialsspaces',serialsspaces);

												compSubRecord.selectNewLineItem('inventoryassignment');
												//Case # 20126405 Start
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
												//Case # 20126405 End
												compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serialsspaces);

												compSubRecord.commitLineItem('inventoryassignment');


											}
											compSubRecord.commit();
										}	
									}
								}
								TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'T');
								nlapiLogExecution('Debug', 'totalqty', totalqty);
								TransformRec.setCurrentLineItemValue('item', 'quantity', parseFloat(totalqty).toFixed(5));
								nlapiLogExecution('Debug', 'itemloc2', itemloc2);
								TransformRec.setCurrentLineItemValue('item', 'location', itemloc2);

								nlapiLogExecution('Debug', 'ItemType', ItemType);

								if(!vAdvBinManagement)
								{	
									if (ItemType == "lotnumberedinventoryitem") {
										nlapiLogExecution('Debug', 'vLotNo', vLotNo);									
										TransformRec.setCurrentLineItemValue('item','serialnumbers', vLotNo);
										nlapiLogExecution('Debug', 'vBatchno', vBatchno);
									}
									else if(ItemType == "lotnumberedassemblyitem") 
									{
										TransformRec.setCurrentLineItemValue('item','serialnumbers', vLotNo);
										nlapiLogExecution('Debug', 'vLotNo', vLotNo);
									}
									else if (ItemType == "serializedinventoryitem") {
										var serialsspaces = "";
										if(serialsspaces==null || serialsspaces=='')
										{
											var filters= new Array();				
											nlapiLogExecution('Debug', 'vebizOrdNo', vebizOrdNo);
											filters.push(new nlobjSearchFilter('custrecord_serialsono', null,'is', vebizOrdNo));
											filters.push(new nlobjSearchFilter('custrecord_serialsolineno', null,'is', filterlinenumber));
											filters.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null,'anyof', ['8']));
											var column =new Array(); 

											column[0] = new nlobjSearchColumn('custrecord_serialnumber');							

											var searchresultser = nlapiSearchRecord('customrecord_ebiznetserialentry',null,filters,column);
											if(searchresultser!=null && searchresultser!='')
											{
												for ( var b = 0; b < searchresultser.length; b++ ) {
													if(serialsspaces==null || serialsspaces=='')
													{
														serialsspaces=searchresultser[b].getValue('custrecord_serialnumber');
													}
													else
													{
														serialsspaces += " " + searchresultser[b].getValue('custrecord_serialnumber');
													}
												}
											}									
											nlapiLogExecution('Debug', 'serialsspaces', serialsspaces);
										}
										nlapiLogExecution('Debug', 'before serialsspaces', serialsspaces);
										TransformRec.setCurrentLineItemValue('item','serialnumbers', serialsspaces);
									}
								}

								vLotNo="";
							}
							TransformRec.commitLineItem('item');
						}
						else
						{
							nlapiLogExecution('Debug', 'itemIndex in else', itemIndex);
							TransformRec.selectLineItem('item', itemIndex);

							TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'F');

							TransformRec.commitLineItem('item');
						}
					}
				}
				//Case# 20124622 Start//Issue Fix for the so having lines with different site then itemfulfillment was failing if we generate the wave for single site
				var SOLengthnew = TransformRec.getLineItemCount('item');
				if(parseInt(filterLineArray.length)!=parseInt(SOLengthnew))
				{
					for(var k1=1;k1<=SOLengthnew;k1++)
					{
						var isLinenoComitted=false;
						var linenum= TransformRec.getLineItemValue('item', 'line', k1);
						for(var k2=0;k2<filterLineArray.length;k2++)
						{
							var filterlineno=filterLineArray[k2];
							if(parseInt(linenum)==parseInt(filterlineno))
							{
								isLinenoComitted=true;
								break;
							}

						}
						if(isLinenoComitted==false)
						{
							nlapiLogExecution('Debug', 'itemIndex in', k1);
							TransformRec.selectLineItem('item', k1);

							TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'F');

							TransformRec.commitLineItem('item');
						}
					}
				}
				//Case# 20124622 End

				//nlapiLogExecution('Debug', 'itemloc', itemloc);
				nlapiLogExecution('ERROR', 'Before Submit', IsBoolean);
				nlapiLogExecution('ERROR', 'ISBoolean.indexOf(T)', IsBoolean.indexOf("T"));
				if(parseInt(IsBoolean.indexOf("T"))!=parseInt(-1))
				{
					nlapiLogExecution('ERROR', "INTOIF");
					boolfound=true;
				}


				if(TransformRec!=null && boolfound==true)
				{
					var TransformRecId;
					nlapiLogExecution('Debug', 'Time Stamp at the start of nlapiSubmitRecord',TimeStampinSec());
					try
					{
						TransformRec = UpdateItemFulfillment(TransformRec,shipcharges,vcontainerLp,trackingno,pakageweight,vebizOrdNo)
						TransformRecId = nlapiSubmitRecord(TransformRec, true);
						nlapiLogExecution('Debug', 'Time Stamp at the end of nlapiSubmitRecord',TimeStampinSec());
						nlapiLogExecution('Debug', 'TransformRecId', TransformRecId);
					}
					catch(e)
					{
						var exceptionname='SalesOrder';
						var functionality='Creating Item Fulfillment';
						var trantype=2;
						nlapiLogExecution('Debug', 'DetailsError', functionality);	
						nlapiLogExecution('Debug', 'vcontainerLp', vcontainerLp);
						nlapiLogExecution('Debug', 'vebizOrdNo354', vebizOrdNo);
						var reference2="";
						var reference3="";
						var reference4 ="";
						var reference5 ="";
						var alertType=1;//1 for exception and 2 for report
						var exceptiondetails=e;
						if ( e instanceof nlobjError )
						{
							nlapiLogExecution( 'Debug', 'system error', e.getCode() + '\n' + e.getDetails() );
							exceptiondetails=e.getCode() + '\n' + e.getDetails();
						}
						else
						{
							exceptiondetails=e.toString();
							nlapiLogExecution( 'Debug', 'unexpected error', e.toString() );
						}
						InsertExceptionLog(exceptionname,trantype, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId,SalesOrderInternalId);

						var ErrorCode=e.getCode();
						var vErrorMsg=e.message;
						//UpdateOpenTaskWithExceptionValue(SalesOrderInternalId,vebizOrdNo,vcontainerLp,exceptiondetails);
						nlapiLogExecution('Debug', 'Debug', e.getCode());
						nlapiLogExecution('Debug', 'Debug', e.message);
						nlapiLogExecution('Debug', 'Debug', e);
					}
					nlapiLogExecution('ERROR', 'After Submit', TransformRecId);
					nlapiLogExecution('ERROR', 'QuilfiedItemArray',QuilfiedItemArray);
					nlapiLogExecution('ERROR', 'QuilfiedItemLineArray',QuilfiedItemLineArray);

					if(QuilfiedItemArray!=null&&QuilfiedItemArray!=""&&QuilfiedItemArray.length!=0)
					{
						var QuilfiedItemArray=removeDuplicateElementOld(QuilfiedItemArray);
						var QuilfiedItemLineArray=removeDuplicateElementOld(QuilfiedItemLineArray);

						if(vNSConfirmNOLog != null && vNSConfirmNOLog != '')
							vNSConfirmNOLog = vNSConfirmNOLog + ',' + TransformRecId; 
						else
							vNSConfirmNOLog = TransformRecId;

						nlapiLogExecution('ERROR', 'vNSConfirmNOLog',vNSConfirmNOLog);

						if(vNSConfirmNOLog!=null && vNSConfirmNOLog!='')
						{

							UpdateAllOpenTaskRecordsWithNSID(QuilfiedItemArray,'',"",TransformRecId,QuilfiedItemLineArray,
									vcontainerLp,vNotes,'',SalesOrderInternalId,'LTL');
						}
					}
					//End of code as of 5th Sep 2013.

					/*if(packtaskid!=null && packtaskid!='')
						nlapiSubmitField('customrecord_ebiznet_trn_opentask', packtaskid, 'custrecord_ebiz_nsconfirm_ref_no', TransformRecId);*/
				} 


			}
		}
	}
	}
	nlapiLogExecution('Debug', 'Time Stamp at the end of createItemFulfillmentLTLMultiLine',TimeStampinSec());
	nlapiLogExecution('Debug', 'vNSConfirmNOLog',vNSConfirmNOLog);
	return vNSConfirmNOLog;

}

function GetAllLocationsFromOT(SalesOrderInternalId,vebizOrdNo,vcontainerLp,vNotes,vMastLPNo,glbitemarray)
{
	var filters=new Array();
	if(SalesOrderInternalId != null && SalesOrderInternalId != '')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId)); 


	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); // PICK
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['28','7','14','10']));//28//Pack Complete
	filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',  null,'isempty')); // NS REF no is empty
	/*** Below change merged from Factory mation production account on 1st March 2013 by Ganesh ***/
	filters.push(new nlobjSearchFilter('custrecord_act_qty',  null,'notequalto','0')); 

	filters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
	/***Upto here***/

	if(vNotes != 'LTL')
	{	
		//filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'is', vcontainerLp));
		if(vebizOrdNo !=null && vebizOrdNo!='')
			filters.push(new nlobjSearchFilter('name', null,'is', vebizOrdNo));
		if(vMastLPNo != null && vMastLPNo != '')
			filters.push(new nlobjSearchFilter('custrecord_mast_ebizlp_no', null, 'is', vMastLPNo));
	}

	if(glbitemarray != null && glbitemarray != '')
		filters.push(new nlobjSearchFilter('custrecord_parent_sku_no', null,'anyof', glbitemarray));

	var column =new Array(); 

	column[0] = new nlobjSearchColumn('custrecord_wms_location',null,'group');		

	var searchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,column);	
	return searchresult;
}

function UpdateShipManifestRecord(nsConfirmnumber,shipmentinternalid)
{
	nlapiLogExecution('DEBUG', 'UpdateShipManifestRecord', nsConfirmnumber);	
	var fields = new Array();
	var values = new Array();
	fields[1] ='custrecord_ship_nsconf_no';
	values[1] = nsConfirmnumber;
	fields[2] ='custrecord_ship_itemfulfill_no';
	values[2] = nsConfirmnumber;

	var updatefields = nlapiSubmitField('customrecord_ship_manifest',shipmentinternalid,fields,values);
	nlapiLogExecution('DEBUG', 'UpdateShipManifestRecord', nsConfirmnumber);	
}

function DeleteItemfulfillmentLine(transactionid,oldcharges,vebizcontainerlp,oldtrackingnumber)
{
	var OpenTaskTransaction = nlapiLoadRecord('itemfulfillment', transactionid);
	var totalLine=OpenTaskTransaction.getLineItemCount('package');
	nlapiLogExecution('DEBUG', 'lineNum ', totalLine);
	var isupdate='N';
	var  remaingcharges ="";
	for(var i=1; i<= parseFloat(totalLine); i++)
	{
		var container2= OpenTaskTransaction.getLineItemValue('package','packagedescr',i);
		var packagetrack=OpenTaskTransaction.getLineItemValue('package','packagetrackingnumber',i);
		var packageweight=OpenTaskTransaction.getLineItemValue('package','packageweight',i);
		nlapiLogExecution('DEBUG', 'container2afterelse', container2);
		if((vebizcontainerlp==container2)&&(oldtrackingnumber==packagetrack))
		{
			OpenTaskTransaction.removeLineItem('package',i);
			var lastcharges =OpenTaskTransaction.getFieldValue('shippingcost');
			nlapiLogExecution('DEBUG', 'shipchargeslastcharges ', lastcharges);
			remaingcharges=lastcharges-oldcharges;
			nlapiLogExecution('DEBUG', 'remaingcharges', remaingcharges);
			//OpenTaskTransaction.setFieldValue('shippingcost',remaingcharges);
			OpenTaskTransaction.setFieldValue('custbody_fulfill_shipcost',remaingcharges);
			isupdate='Y';
		}
	}
	var totalLine=OpenTaskTransaction.getLineItemCount('packagefedex');
	nlapiLogExecution('DEBUG', 'fedexlineNum ', totalLine);
	var  remaingcharges ="";
	for(var i=1; i<= parseFloat(totalLine); i++)
	{
		var container2= OpenTaskTransaction.getLineItemValue('packagefedex','packagedescrfedex',i);
		var packagetrack=OpenTaskTransaction.getLineItemValue('packagefedex','packagetrackingnumberfedex',i);
		var packageweight=OpenTaskTransaction.getLineItemValue('packagefedex','packageweightfedex',i);
		nlapiLogExecution('DEBUG', 'container2afterelse', container2);
		if((vebizcontainerlp==container2)&&(oldtrackingnumber==packagetrack))
		{
			OpenTaskTransaction.removeLineItem('packagefedex',i);
			var lastcharges =OpenTaskTransaction.getFieldValue('shippingcost');
			nlapiLogExecution('DEBUG', 'shipchargeslastcharges ', lastcharges);
			remaingcharges=lastcharges-oldcharges;
			nlapiLogExecution('DEBUG', 'remaingcharges', remaingcharges);
			//OpenTaskTransaction.setFieldValue('shippingcost',remaingcharges);
			OpenTaskTransaction.setFieldValue('custbody_fulfill_shipcost',remaingcharges);
			isupdate='Y';
		}
	}
	var totalLine=OpenTaskTransaction.getLineItemCount('packageups');
	nlapiLogExecution('DEBUG', 'fedexlineNum ', totalLine);
	var  remaingcharges ="";
	for(var i=1; i<= parseFloat(totalLine); i++)
	{
		var container2= OpenTaskTransaction.getLineItemValue('packageups','packagedescrups',i);
		var packagetrack=OpenTaskTransaction.getLineItemValue('packageups','packagetrackingnumberups',i);
		var packageweight=OpenTaskTransaction.getLineItemValue('packageups','packageweightups',i);
		nlapiLogExecution('DEBUG', 'container2afterelse', container2);
		if((vebizcontainerlp==container2)&&(oldtrackingnumber==packagetrack))
		{
			OpenTaskTransaction.removeLineItem('packageups',i);
			var lastcharges =OpenTaskTransaction.getFieldValue('shippingcost');
			nlapiLogExecution('DEBUG', 'shipchargeslastcharges ', lastcharges);
			remaingcharges=lastcharges-oldcharges;
			nlapiLogExecution('DEBUG', 'remaingcharges', remaingcharges);
			//OpenTaskTransaction.setFieldValue('shippingcost',remaingcharges);
			OpenTaskTransaction.setFieldValue('custbody_fulfill_shipcost',remaingcharges);
			isupdate='Y';
		}
	}
	if(isupdate=='Y')
	{
		var id =nlapiSubmitRecord(OpenTaskTransaction,true);
	}

}

function updateopentaskstatus(opentaskid)
{
	nlapiLogExecution('DEBUG', 'Into updateopentaskstatus - Id', opentaskid);

	var fieldNames = new Array(); 
	fieldNames.push('custrecord_wms_status_flag');  	
	fieldNames.push('custrecord_notes'); 

	var newValues = new Array(); 
	newValues.push(14);
	newValues.push('QuickShip');	

	nlapiSubmitField('customrecord_ebiznet_trn_opentask', opentaskid, fieldNames, newValues);

//	var opentaskrec=nlapiLoadRecord('customrecord_ebiznet_trn_opentask',id);
//	opentaskrec.setFieldValue('custrecord_notes','QuickShip');
//	opentaskrec.setFieldValue('custrecord_wms_status_flag',14);//status.outbound.shipped 
//	var id = nlapiSubmitRecord(opentaskrec, true);
//	nlapiLogExecution('DEBUG', 'id', id);

	nlapiLogExecution('DEBUG', 'Out of updateopentaskstatus - Id', opentaskid);
}

function GetShipManifestCustomRecordsforInvoice(fulfillmentOrder,vebizcontainerlp)
{
	var shipmentfilter = new Array();
	shipmentfilter.push(new nlobjSearchFilter('custrecord_ship_ref3', null, 'is', fulfillmentOrder));
	shipmentfilter.push(new nlobjSearchFilter('custrecord_ship_trackno', null, 'isempty'));
	shipmentfilter.push(new nlobjSearchFilter('custrecord_ship_ref5', null, 'isnot',vebizcontainerlp));
	var shipmentcolumns = new Array();
	shipmentcolumns[0] = new nlobjSearchColumn('custrecord_ship_ref5');
	var searchshipmanifestshippedrecords= nlapiSearchRecord('customrecord_ship_manifest', null, shipmentfilter, shipmentcolumns);
	return searchshipmanifestshippedrecords;
}

function Getopentaskpicklocationrecords(fulfillmentOrder)
{
	var opentaskfilter = new Array();
	opentaskfilter.push(new nlobjSearchFilter('name', null, 'is', fulfillmentOrder));
	opentaskfilter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','8']));	
	opentaskfilter.push(new nlobjSearchFilter('custrecord_act_qty', null, 'greaterthan', 0));	
	opentaskfilter.push(new nlobjSearchFilter('custrecord_act_qty', null, 'isnotempty'));	

	var opentasktcolumns = new Array();
	opentasktcolumns[0] = new nlobjSearchColumn('name');
	var searchopentaskpicklocationrecords= nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, opentaskfilter, opentasktcolumns);
	return searchopentaskpicklocationrecords;
}
function updateFOShipQty(FOIntrId,ffOrdNo,ffOrdLineNo,shipqty)
{
	nlapiLogExecution('DEBUG', 'Into updateFOShipQty - FOIntrId', FOIntrId);
	nlapiLogExecution('DEBUG', 'shipqty', shipqty);

	try
	{
		var forec = nlapiLoadRecord('customrecord_ebiznet_ordline',FOIntrId);

		var oldshipqty = forec.getFieldValue('custrecord_ship_qty');
		var ordQty = forec.getFieldValue('custrecord_ord_qty');

		if(oldshipqty==null || oldshipqty=='' || isNaN(oldshipqty))
			oldshipqty=0;

		if(ordQty==null || ordQty=='' || isNaN(ordQty))
			ordQty=0;

		if(shipqty==null || shipqty=='' || isNaN(shipqty))
			shipqty=0;

		shipqty = parseInt(shipqty)+parseInt(oldshipqty);

		forec.setFieldValue('custrecord_ship_qty',parseInt(shipqty));
		if(parseInt(shipqty)<parseInt(ordQty))
			forec.setFieldValue('custrecord_linestatus_flag', '13'); //STATUS.OUTBOUND.PARTIALLY_SHIPPED
		else
			forec.setFieldValue('custrecord_linestatus_flag', '14'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')

		forec.setFieldValue('custrecord_ebiz_shipdate', DateStamp());

		nlapiSubmitRecord(forec, true);
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in updateFOShipQty',exp);
	}

	nlapiLogExecution('DEBUG', 'Out of updateFOShipQty');
}

function updateShipQty(ffOrdNo,ffOrdLineNo,shipqty)
{
	nlapiLogExecution('DEBUG', 'Into updateShipQty - Qty', shipqty);
	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', ffOrdNo));
	filter.push(new nlobjSearchFilter('custrecord_ordline', null, 'equalto', ffOrdLineNo));
	var column = new Array();
	column[0] = new nlobjSearchColumn('internalid') ;
	column[1] = new nlobjSearchColumn('name') ;
	column[2] = new nlobjSearchColumn('custrecord_ship_qty') ;
	column[3] = new nlobjSearchColumn('custrecord_ord_qty') ;
	column[4] = new nlobjSearchColumn('custrecord_ebiz_linesku') ;// case# 201413549
	column[5] = new nlobjSearchColumn('custrecord_pickqty') ;
	var searchRec= nlapiSearchRecord('customrecord_ebiznet_ordline', null, filter, column);
	if (searchRec!=null && searchRec!='')
	{
		for (var s = 0; s < searchRec.length; s++)
		{
			var ffEbizOrdNo = searchRec[s].getValue('internalid');
			var salesorderintrid = searchRec[s].getValue('name');
			var ordQty = searchRec[s].getValue('custrecord_ord_qty');
			var oldshipqty = searchRec[s].getValue('custrecord_ship_qty');
			var vorditem = searchRec[s].getValue('custrecord_ebiz_linesku');
			var vpickqty = searchRec[s].getValue('custrecord_pickqty');

			var vitemtype = nlapiLookupField('item', vorditem, 'recordType');

			if(ordQty==null || ordQty=='' || isNaN(ordQty))
				ordQty=0;

			if(oldshipqty==null || oldshipqty=='' || isNaN(oldshipqty))
				oldshipqty=0;

			shipqty = parseFloat(shipqty)+parseFloat(oldshipqty);

			var fieldNames = new Array(); 
			fieldNames.push('custrecord_linestatus_flag');  	
			fieldNames.push('custrecord_ship_qty'); 
			fieldNames.push('custrecord_ebiz_shipdate'); 

			var newValues = new Array(); 
			if(parseFloat(shipqty)<parseFloat(ordQty))
				newValues.push('13'); //STATUS.OUTBOUND.PARTIALLY_SHIPPED
			else
				newValues.push('14'); //STATUS.OUTBOUND.SHIPPED
			//newValues.push(parseFloat(shipqty));
			if((parseFloat(shipqty)>=parseFloat(vpickqty)) && vitemtype=='kititem')
			{
				newValues.push(parseFloat(vpickqty));
			}
			else
			{
				newValues.push(parseFloat(shipqty));
			}
			var vDate=DateStamp();
			newValues.push(vDate);	

			nlapiSubmitField('customrecord_ebiznet_ordline', ffEbizOrdNo, fieldNames, newValues);


//			var transaction = nlapiLoadRecord('customrecord_ebiznet_ordline', ffEbizOrdNo); 
//			var oldshipqty = transaction.getFieldValue('custrecord_ship_qty');
//			var ordQty = transaction.getFieldValue('custrecord_ord_qty');

//			if(ordQty==null || ordQty=='' || isNaN(ordQty))
//			ordQty=0;

//			if(oldshipqty==null || oldshipqty=='' || isNaN(oldshipqty))
//			oldshipqty=0;

//			shipqty = parseFloat(shipqty)+parseFloat(oldshipqty);

//			transaction.setFieldValue('custrecord_ship_qty',parseFloat(shipqty));
//			if(parseInt(shipqty)<parseInt(ordQty))
//			transaction.setFieldValue('custrecord_linestatus_flag', '13'); //STATUS.OUTBOUND.PARTIALLY_SHIPPED
//			else
//			transaction.setFieldValue('custrecord_linestatus_flag', '14'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')

//			transaction.setFieldValue('custrecord_ebiz_shipdate', DateStamp());
//			nlapiSubmitRecord(transaction, true);
			//updatesalesorderline(salesorderintrid,ffOrdLineNo,shipqty,'S');
		}
	}
	nlapiLogExecution('DEBUG', 'Out of updateShipQty', shipqty);
}

function getShipManifestRecords(fulfillmentOrder,nsconfirmationno)
{
	var shipmanifestarray = new Array();
	shipmanifestarray.push(new nlobjSearchFilter('custrecord_ship_ref3',null,'is',fulfillmentOrder));
	shipmanifestarray.push(new nlobjSearchFilter('custrecord_ship_nsconf_no',null,'is',nsconfirmationno));
	var shipmanifestrecordsearch= nlapiSearchRecord('customrecord_ship_manifest', null, shipmanifestarray,null);
	return shipmanifestrecordsearch;
}

function GetTransferOrderDetails(TransferOrder)
{
	try
	{
		var Id;
		var TransferOrderFilter=new Array();
		TransferOrderFilter.push(new nlobjSearchFilter('tranid',null,'is',TransferOrder));
		TransferOrderFilter.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

		var TransferOrderColumn=new Array();
		TransferOrderColumn[0]=new nlobjSearchColumn('shippingcost');
		var searchTransferOrder=nlapiSearchRecord('transferorder',null,TransferOrderFilter,TransferOrderColumn);
		if(SearchRec!=null && SearchRec!="")
		{
			Id=SearchRec[0].getId();
		}
		result[0]=Id;
		return result;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception IN TransferOrder',exp);
	}
}

function GetSalesOrderDetails(salesorder)
{
	try{
		var category='';
		var override='F';
		var shipCost=0;
		var Id;
		var result=new Array();
		var salesOrderFilter=new Array();
		salesOrderFilter.push(new nlobjSearchFilter('tranid',null,'is',salesorder));
		salesOrderFilter.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

		var salesOrderColumn=new Array();
		salesOrderColumn[0]=new nlobjSearchColumn('custbody_customer_type');
		salesOrderColumn[1]=new nlobjSearchColumn('custbody_override_shipping');
		salesOrderColumn[2]=new nlobjSearchColumn('custbody_ebizso_actcost');


		var SearchRec=nlapiSearchRecord('salesorder',null,salesOrderFilter,salesOrderColumn);
		if(SearchRec!=null && SearchRec!="")
		{
			//In production datatype freeformText and in sandbox datatype listOrRecord
			category=SearchRec[0].getText('custbody_customer_type');
			//category=SearchRec[0].getValue('custbody_customer_type');
			override=SearchRec[0].getValue('custbody_override_shipping');
			shipCost=SearchRec[0].getValue('custbody_ebizso_actcost');
			Id=SearchRec[0].getId();
		}
		result[0]=category;
		result[1]=override;
		result[2]=shipCost;
		result[3]=Id;
		nlapiLogExecution('DEBUG','result',result);
		return result;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception IN GetSalesOrderDetails',exp);
	}
}

function UpdateTransforOrderItemFulfillment(transactionid,shipcharges,vebizcontainerlp,trackingno,pakageweight,ActualCarrier,SalesOrder,PublishedRate,customerShipCost,fulfillmentOrder,newshipmanifestid,transferorderinternalid)
{
	try
	{
		nlapiLogExecution('DEBUG', 'UpdateTransforOrderitemfullfilment ', 'UpdateTransforOrderitemfullfilment');
		nlapiLogExecution('DEBUG', 'vSalesOrder', SalesOrder);
		var splitfulfillmentNo=fulfillmentOrder.split('.');
		ShipmentNO=splitfulfillmentNo[1];
		nlapiLogExecution('DEBUG', 'ShipmentNO', ShipmentNO);
		var searchshipmanifestrecord=getShipManifestRecords(fulfillmentOrder,transactionid);

		//var FetchedToDetails=GetTransferOrderDetails(SalesOrder);

		//var TOInternalID=FetchedToDetails[0];
		var TOInternalID=transferorderinternalid;
		nlapiLogExecution('DEBUG', 'SOInternalID', TOInternalID);
		var totalshipcharges;
		var OpenTaskTransaction = nlapiLoadRecord('itemfulfillment', transactionid);
		OpenTaskTransaction.setFieldValue('shipstatus','C'); //Statusflag='C'
		OpenTaskTransaction.setFieldValue('generateintegratedshipperlabel','F');
		nlapiLogExecution('DEBUG', 'ActualCarrier', ActualCarrier);
		if(ActualCarrier!=null && ActualCarrier!='')
		{
			//OpenTaskTransaction.setFieldValue('custbody_fulfill_shipcarrier',ActualCarrier);
			//OpenTaskTransaction.setFieldValue('custbody_ebiz_actual_carrier',ActualCarrier);
		}
		var lastcharges=null;
		if((searchshipmanifestrecord==null)||(searchshipmanifestrecord==''))
		{

			totalshipcharges=customerShipCost;
		}
		else
		{
			nlapiLogExecution('DEBUG', 'IF4','Outside');
			totalshipcharges=customerShipCost;
			lastcharges=OpenTaskTransaction.getFieldValue('shippingcost');

			nlapiLogExecution('DEBUG', 'totalshipchargesbeforadd ',totalshipcharges);
			totalshipcharges=parseFloat(lastcharges)+parseFloat(totalshipcharges);
			nlapiLogExecution('DEBUG', 'totalshipchargesafteradd ', totalshipcharges);
		}



		if(totalshipcharges!="")
		{

			OpenTaskTransaction.setFieldValue('shippingcost',parseFloat(totalshipcharges));

		}
		var lastgsusashipcharges=OpenTaskTransaction.getFieldValue('custbody_gsusa_shipping_cost');
		nlapiLogExecution('DEBUG', 'fullfillshipchargesbeforadd ',lastgsusashipcharges);
		//Updating Cummalative Charges

		var fulfillmentshipcharges;
		if((lastgsusashipcharges==null)||(lastgsusashipcharges==''))
		{
			fulfillmentshipcharges=shipcharges;
		}
		else
		{
			fulfillmentshipcharges=parseFloat(lastgsusashipcharges)+parseFloat(shipcharges);

		}

		nlapiLogExecution('DEBUG', 'fullfillshipchargesafteradd ',fulfillmentshipcharges);
		if(fulfillmentshipcharges!=null && fulfillmentshipcharges!='')
		{
			OpenTaskTransaction.setFieldValue('custbody_gsusa_shipping_cost',parseFloat(fulfillmentshipcharges));
			OpenTaskTransaction.setFieldValue('custbody_fulfill_custshipcost',parseFloat(fulfillmentshipcharges));
		}

		//Updating Publish Charges
		var lastpublishrate=OpenTaskTransaction.getFieldValue('custbody_fulfill_pubrate');
		nlapiLogExecution('DEBUG', 'PublishedRate ',PublishedRate);
		nlapiLogExecution('DEBUG', 'publishratebeforadd ',lastpublishrate);
		var totalpublishrate;
		if(PublishedRate==null || PublishedRate=='')
			PublishedRate=0;
		if(lastpublishrate==null || lastpublishrate=='')
			lastpublishrate=0;

		if((lastpublishrate==null)||(lastpublishrate==''))
		{
			totalpublishrate=PublishedRate;
		}
		else
		{
			totalpublishrate=parseFloat(lastpublishrate)+parseFloat(PublishedRate);
		}
		nlapiLogExecution('DEBUG', 'publishrateafteradd', totalpublishrate);

		if(totalpublishrate!=null && totalpublishrate!='')
			OpenTaskTransaction.setFieldValue('custbody_fulfill_pubrate',parseFloat(totalpublishrate));

		OpenTaskTransaction.selectNewLineItem('package');
		OpenTaskTransaction.setCurrentLineItemValue('package', 'packagedescr', vebizcontainerlp);
		OpenTaskTransaction.setCurrentLineItemValue('package', 'packagetrackingnumber', trackingno);
		OpenTaskTransaction.setCurrentLineItemValue('package', 'packageweight',pakageweight);
//		OpenTaskTransaction.setCurrentLineItemValue('item', 'location',wmslocation);
		OpenTaskTransaction.commitLineItem('package');
		OpenTaskTransaction.selectNewLineItem('packagefedex');
		OpenTaskTransaction.setCurrentLineItemValue('packagefedex', 'packagedescrfedex', vebizcontainerlp);
		OpenTaskTransaction.setCurrentLineItemValue('packagefedex', 'packagetrackingnumberfedex', trackingno);
		OpenTaskTransaction.setCurrentLineItemValue('packagefedex', 'packageweightfedex',pakageweight);
//		OpenTaskTransaction.setCurrentLineItemValue('item', 'location',wmslocation);
		OpenTaskTransaction.commitLineItem('packagefedex');
//		OpenTaskTransaction.commitLineItem('item');
		var itemfullfilmentid= nlapiSubmitRecord(OpenTaskTransaction ,true);
		nlapiLogExecution('DEBUG', 'After open task submit record ', itemfullfilmentid);

		//UpdateSalesOrder(fulfillmentshipcharges,totalshipcharges,totalpublishrate,ShipmentNO,TOInternalID,SOcustomerCategory,SOOverRideChkd);
		if(parseInt(ShipmentNO)==1)
		{
			UpdateTranferOrder(fulfillmentshipcharges,totalshipcharges,totalpublishrate,TOInternalID,ShipmentNO);
		}
		else
		{
			UpdateTranferOrder(shipcharges,customerShipCost,PublishedRate,TOInternalID,ShipmentNO);

		}
		UpdateShipManifestRecord(newshipmanifestid,itemfullfilmentid,null,null,null);

	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in UpdateTransferOrderItemFulfillment function',exp);

	}
}
function UpdateTranferOrder(GSUSAShipcost,CustShipCost,PublishedRate,TOInternalID,ShipmentNO)
{
	nlapiLogExecution('DEBUG','UpdateTranferOrder','UpdateTranferOrder');
	nlapiLogExecution('DEBUG','ShipmentNO:',ShipmentNO);
	var TransferOrder=nlapiLoadRecord('transferorder',TOInternalID);
	if(parseInt(ShipmentNO)==1)
	{
		TransferOrder.setFieldValue('custbody_ebizso_actcost',parseFloat(GSUSAShipcost));
		TransferOrder.setFieldValue('custbody_ebizso_custshipcost',parseFloat(CustShipCost));
		TransferOrder.setFieldValue('shippingcost',parseFloat(CustShipCost));
		TransferOrder.setFieldValue('custbody_ebizso_pubrate',parseFloat(PublishedRate));
		var id = nlapiSubmitRecord(TransferOrder, true);
		nlapiLogExecution('DEBUG','Id',id);
	}
	else
	{


		var PreviousGSUSAShipcost=TransferOrder.getFieldValue('custbody_ebizso_actcost');
		//var PreviousCustShipCost=SOrec.getFieldValue('custbody_ebizso_custshipcost');
		var PreviousCustShipCost=TransferOrder.getFieldValue('shippingcost');
		var PreviousPublishedRate=TransferOrder.getFieldValue('custbody_ebizso_pubrate');
		nlapiLogExecution('DEBUG','GSUSAShipcostOld',PreviousGSUSAShipcost);
		nlapiLogExecution('DEBUG','CustShipCostOld',PreviousCustShipCost);
		nlapiLogExecution('DEBUG','PublishedRateOld',PreviousPublishedRate);
		var NewGSUSAShipcost=parseFloat(PreviousGSUSAShipcost)+parseFloat(GSUSAShipcost);
		var NewCustShipCost=parseFloat(PreviousCustShipCost)+parseFloat(CustShipCost);
		var NewPublishedRate=parseFloat(PreviousPublishedRate)+parseFloat(PublishedRate);
		nlapiLogExecution('DEBUG','GSUSAShipcostNew',NewGSUSAShipcost);
		nlapiLogExecution('DEBUG','PublishedRateNew',NewPublishedRate);
		TransferOrder.setFieldValue('custbody_ebizso_actcost',parseFloat(NewGSUSAShipcost));
		TransferOrder.setFieldValue('custbody_ebizso_custshipcost',parseFloat(NewCustShipCost));
		TransferOrder.setFieldValue('shippingcost',parseFloat(NewCustShipCost));
		TransferOrder.setFieldValue('custbody_ebizso_pubrate',parseFloat(NewPublishedRate));
		var id = nlapiSubmitRecord(TransferOrder, true);
		nlapiLogExecution('DEBUG','Id',id);
	}

}
/*
function UpdateItemFulfillment(transactionid,shipcharges,vebizcontainerlp,trackingno,pakageweight,ActualCarrier,SalesOrder,
		PublishedRate,customerShipCost,fulfillmentOrder,newshipmanifestid,ActualCarrierName,ActualCarrierId)
{
	try
	{
		nlapiLogExecution('DEBUG', 'updateitemfullfilment ', 'updateitemfullfilment');
		nlapiLogExecution('DEBUG', 'vSalesOrder', SalesOrder);
		nlapiLogExecution('DEBUG', 'fulfillmentOrder', fulfillmentOrder);
		var ShipmentNO;
		if(fulfillmentOrder!=null && fulfillmentOrder!='')
		{
			var splitfulfillmentNo = fulfillmentOrder.split('.');
			ShipmentNO = splitfulfillmentNo[1];
		}
		nlapiLogExecution('DEBUG', 'ShipmentNO', ShipmentNO);
		var returnflag = -1;
		var salesordrid;

		var searchshipmanifestrecord=getShipManifestRecords(fulfillmentOrder,transactionid);
		var totalshipcharges;
		var OpenTaskTransaction = nlapiLoadRecord('itemfulfillment', transactionid);
		OpenTaskTransaction.setFieldValue('shipstatus','C'); //Statusflag='C'
		OpenTaskTransaction.setFieldValue('generateintegratedshipperlabel','F');
		nlapiLogExecution('DEBUG', 'ActualCarrier', ActualCarrier);

		if(ActualCarrier!=null && ActualCarrier!='')
		{
			OpenTaskTransaction.setFieldValue('custbody_fulfill_shipcarrier',ActualCarrier); // Shipping Method			
		}

		if(ActualCarrierId!=null && ActualCarrierId!='')
		{
			OpenTaskTransaction.setFieldValue('custbody_ebiz_actual_carrier',ActualCarrierId); // Shipping Method			
		}

		var FetchedSODetails=GetSalesOrderDetails(SalesOrder);
		var SOcustomerCategory=FetchedSODetails[0];
		var SOOverRideChkd=FetchedSODetails[1];
		var SOActShipCost=FetchedSODetails[2];
		var SOInternalID=FetchedSODetails[3];
		nlapiLogExecution('DEBUG', 'SOInternalID', SOInternalID);
		var lastcharges=0;
		nlapiLogExecution('DEBUG', 'LastShipcharges ', lastcharges);

		if(SOcustomerCategory=="Retail"&&SOOverRideChkd=='F'&&parseInt(ShipmentNO)==1)
		{
			totalshipcharges=0;
			lastcharges=OpenTaskTransaction.getFieldValue('shippingcost');
			nlapiLogExecution('DEBUG', 'LastShipcharges ', lastcharges);
			nlapiLogExecution('DEBUG', 'IF 1');
		}
		else if(SOcustomerCategory=="Retail"&&SOOverRideChkd=='F'&&parseInt(ShipmentNO)>1)
		{
			totalshipcharges=0;
			nlapiLogExecution('DEBUG', 'IF2');
		}
		else if(SOcustomerCategory=="Retail"&&SOOverRideChkd=='T'&&parseInt(ShipmentNO)>1)
		{
			totalshipcharges=0;
			nlapiLogExecution('DEBUG', 'IF3');
		}
		else if(SOcustomerCategory=="Retail"&&SOOverRideChkd=='T'&&parseInt(ShipmentNO)==1)
		{
			if((searchshipmanifestrecord==null)||(searchshipmanifestrecord==''))
			{
				totalshipcharges=customerShipCost;
				nlapiLogExecution('DEBUG', 'IF4','Inside');
			}
			else
			{
				totalshipcharges=customerShipCost;
				lastcharges=OpenTaskTransaction.getFieldValue('shippingcost');
				nlapiLogExecution('DEBUG', 'IF4','Outside');
			}
		}
		else if(SOcustomerCategory!="Retail"&&SOOverRideChkd=='F'&&parseInt(ShipmentNO)==1)
		{
			if((searchshipmanifestrecord==null)||(searchshipmanifestrecord==''))
			{
				totalshipcharges=customerShipCost;
				nlapiLogExecution('DEBUG', 'IF6','Inside');
			}
			else
			{
				totalshipcharges=customerShipCost;
				lastcharges=OpenTaskTransaction.getFieldValue('shippingcost');
				nlapiLogExecution('DEBUG', 'IF6','Outside');
			}
		}
		else if(SOcustomerCategory!="Retail"&&SOOverRideChkd=='F'&&parseInt(ShipmentNO)>1)
		{
			if((searchshipmanifestrecord==null)||(searchshipmanifestrecord==''))
			{
				totalshipcharges=customerShipCost;
				nlapiLogExecution('DEBUG', 'IF9','Inside');
			}
			else
			{
				totalshipcharges=customerShipCost;
				lastcharges=OpenTaskTransaction.getFieldValue('shippingcost');
				nlapiLogExecution('DEBUG', 'IF9','Outside');
			}
		}
		else if(SOcustomerCategory!="Retail"&&SOOverRideChkd=='T'&&parseInt(ShipmentNO)==1)
		{
			if((searchshipmanifestrecord==null)||(searchshipmanifestrecord==''))
			{
				totalshipcharges=customerShipCost;
				nlapiLogExecution('DEBUG', 'IF7','Inside');
			}
			else
			{
				totalshipcharges=customerShipCost;
				lastcharges=OpenTaskTransaction.getFieldValue('shippingcost');
				nlapiLogExecution('DEBUG', 'IF7','Outside');

			}
		}
		else if(SOcustomerCategory!="Retail"&&SOOverRideChkd=='T'&&parseInt(ShipmentNO)>1)
		{
			totalshipcharges=0;
			nlapiLogExecution('DEBUG', 'IF8');
		}

		nlapiLogExecution('DEBUG', 'lastcharges ', lastcharges);
		if(lastcharges==null || lastcharges=='')
			lastcharges=0;
		nlapiLogExecution('DEBUG', 'totalshipchargesbeforadd ', totalshipcharges);
		if(totalshipcharges==null || totalshipcharges=='')
			totalshipcharges=0;
		totalshipcharges=parseFloat(lastcharges)+parseFloat(totalshipcharges);
		nlapiLogExecution('DEBUG', 'totalshipchargesafteradd ', totalshipcharges);
		if((SOcustomerCategory!="Retail")&&(SOOverRideChkd=='F'))
		{
			if(totalshipcharges!="" && totalshipcharges!=null)
			{
				nlapiLogExecution('DEBUG', 'Scenario3Testing ', totalshipcharges);
				OpenTaskTransaction.setFieldValue('shippingcost',parseFloat(totalshipcharges));

			}
		}

		var lastgsusashipcharges=OpenTaskTransaction.getFieldValue('custbody_gsusa_shipping_cost');
		nlapiLogExecution('DEBUG', 'fullfillshipchargesbeforadd ',lastgsusashipcharges);
		var fulfillmentshipcharges;
		if((lastgsusashipcharges==null)||(lastgsusashipcharges==''))
		{
			fulfillmentshipcharges=shipcharges;
		}
		else
		{
			fulfillmentshipcharges=parseFloat(lastgsusashipcharges)+parseFloat(shipcharges);

		}		

		//nlapiLogExecution('DEBUG', 'fullfillshipchargesafteradd ',fulfillmentshipcharges);
		if(fulfillmentshipcharges==null || fulfillmentshipcharges=='' || isNaN(fulfillmentshipcharges))
		{
			fulfillmentshipcharges=0;
		}
		nlapiLogExecution('DEBUG', 'fullfillshipchargesafteradd ',fulfillmentshipcharges);
		if(fulfillmentshipcharges!=null && fulfillmentshipcharges!='')
		{
			OpenTaskTransaction.setFieldValue('custbody_gsusa_shipping_cost',parseFloat(fulfillmentshipcharges));
			OpenTaskTransaction.setFieldValue('custbody_fulfill_custshipcost',parseFloat(fulfillmentshipcharges));
		}

		var lastpublishrate=OpenTaskTransaction.getFieldValue('custbody_fulfill_pubrate');
		nlapiLogExecution('DEBUG', 'PublishedRate ',PublishedRate);
		nlapiLogExecution('DEBUG', 'publishratebeforadd ',lastpublishrate);
		var totalpublishrate;
		if(PublishedRate==null || PublishedRate=='')
			PublishedRate=0;
		if(lastpublishrate==null || lastpublishrate=='')
			lastpublishrate=0;

		if((lastpublishrate==null)||(lastpublishrate==''))
		{
			totalpublishrate=PublishedRate;
		}
		else
		{
			totalpublishrate=parseFloat(lastpublishrate)+parseFloat(PublishedRate);
		}
		nlapiLogExecution('DEBUG', 'publishrateafteradd', totalpublishrate);
		if(totalpublishrate!=null && totalpublishrate!='')
			OpenTaskTransaction.setFieldValue('custbody_fulfill_pubrate',parseFloat(totalpublishrate));

		nlapiLogExecution('DEBUG', 'vebizcontainerlp', vebizcontainerlp);
		nlapiLogExecution('DEBUG', 'trackingno', trackingno);
		nlapiLogExecution('DEBUG', 'pakageweight', pakageweight);
		nlapiLogExecution('DEBUG', 'transactionid', transactionid);

		OpenTaskTransaction.selectNewLineItem('package');
		OpenTaskTransaction.setCurrentLineItemValue('package', 'packagedescr', vebizcontainerlp);
		OpenTaskTransaction.setCurrentLineItemValue('package', 'packagetrackingnumber', trackingno);
		OpenTaskTransaction.setCurrentLineItemValue('package', 'packageweight',parseFloat(pakageweight));
		OpenTaskTransaction.commitLineItem('package');
		OpenTaskTransaction.selectNewLineItem('packagefedex');
		OpenTaskTransaction.setCurrentLineItemValue('packagefedex', 'packagedescrfedex', vebizcontainerlp);
		OpenTaskTransaction.setCurrentLineItemValue('packagefedex', 'packagetrackingnumberfedex', trackingno);
		OpenTaskTransaction.setCurrentLineItemValue('packagefedex', 'packageweightfedex',parseFloat(pakageweight));
		OpenTaskTransaction.commitLineItem('packagefedex');
		var itemfullfilmentid= nlapiSubmitRecord(OpenTaskTransaction ,true);
		nlapiLogExecution('DEBUG', 'After open task submit record ', itemfullfilmentid);
		nlapiLogExecution('DEBUG', 'clearOutboundInventory for PC');
		if(itemfullfilmentid != null && itemfullfilmentid != '' )			
			clearOutboundInventory(vebizcontainerlp);

		if(parseInt(ShipmentNO)==1)
		{
			salesordrid = UpdateSalesOrder(fulfillmentshipcharges,totalshipcharges,totalpublishrate,ShipmentNO,
					SOInternalID,SOcustomerCategory,SOOverRideChkd,ActualCarrierName);
		}
		else
		{
			salesordrid = UpdateSalesOrder(shipcharges,customerShipCost,PublishedRate,ShipmentNO,SOInternalID,
					SOcustomerCategory,SOOverRideChkd,ActualCarrierName);
		}

		UpdateShipManifestRecord(newshipmanifestid,itemfullfilmentid);
		nlapiLogExecution('DEBUG', 'After open task submit record returnflag', returnflag);
		nlapiLogExecution('DEBUG', 'After open task submit record itemfullfilmentid', itemfullfilmentid);
		nlapiLogExecution('DEBUG', 'After open task submit record salesordrid', salesordrid);



		if(itemfullfilmentid != null && itemfullfilmentid != '' && salesordrid != null && salesordrid != '')
			returnflag = 1;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in UpdateItemFullFillment function',exp);
		nlapiLogExecution('DEBUG','Exception in UpdateItemFullFillment function',exp.getDetails());
	}
	return returnflag;
}
 */
function updatescaccode(temporderno,ActualCarrierName,servicelevel)
{
	nlapiLogExecution('DEBUG','Into updatescaccode');
	nlapiLogExecution('DEBUG','ActualCarrierName',ActualCarrierName);
	nlapiLogExecution('DEBUG','servicelevel',servicelevel);
	nlapiLogExecution('DEBUG','temporderno',temporderno);

	try
	{
		var scaccode='';

		if(ActualCarrierName!=null && ActualCarrierName!='')
			scaccode=ActualCarrierName;
		else
			scaccode=servicelevel;


		var fields = new Array();
		var values = new Array();

		fields.push('custbody_ebiz_scac_code');


		values.push(scaccode);


		nlapiSubmitField('salesorder', temporderno, fields, values);
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in updatescaccode',exp);
	}

	nlapiLogExecution('DEBUG','Out of updatescaccode');
}

function UpdateSalesOrder(GSUSAShipcost,CustShipCost,PublishedRate,ShipmentNO,SOInternalID,
		SOcustomerCategory,SOOverRideChkd,ActualCarrierName)
{
	nlapiLogExecution('DEBUG','Into UpdateSalesOrder');

	try
	{	
		nlapiLogExecution('DEBUG','GSUSAShipcost',GSUSAShipcost);
		nlapiLogExecution('DEBUG','CustShipCost',CustShipCost);
		nlapiLogExecution('DEBUG','PublishedRate',PublishedRate);
		nlapiLogExecution('DEBUG','ShipmentNO',ShipmentNO);
		nlapiLogExecution('DEBUG','SOInternalID',SOInternalID);
		nlapiLogExecution('DEBUG','SOcustomerCategory',SOcustomerCategory);
		nlapiLogExecution('DEBUG','SOOverRideChkd',SOOverRideChkd);		
		var id;

		if(parseInt(ShipmentNO)==1)
		{
			nlapiLogExecution('DEBUG','Into IF ShipmentNO:',ShipmentNO);
//			SOrec.setFieldValue('custbody_ebizso_actcost',parseFloat(GSUSAShipcost));
//			if((SOcustomerCategory!="Retail")&&(SOOverRideChkd=='F'))
//			{
//			nlapiLogExecution('DEBUG','Scenario3if',CustShipCost);
//			SOrec.setFieldValue('custbody_ebizso_custshipcost',parseFloat(CustShipCost));
//			SOrec.setFieldValue('shippingcost',parseFloat(CustShipCost));
//			}
//			SOrec.setFieldValue('custbody_ebizso_pubrate',parseFloat(PublishedRate));
//			SOrec.setFieldValue('custbody_ebiz_scac_code',ActualCarrierName);
//			id = nlapiSubmitRecord(SOrec, true);
//			nlapiLogExecution('DEBUG','Id',id);

			var fields = new Array();
			var values = new Array();

			fields.push('custbody_ebizso_actcost');
			values.push(parseFloat(GSUSAShipcost));

			fields.push('custbody_ebiz_scac_code');
			values.push(ActualCarrierName);

			fields.push('custbody_ebizso_pubrate');
			values.push(parseFloat(PublishedRate));

			if((SOcustomerCategory!="Retail")&&(SOOverRideChkd=='F'))
			{
				nlapiLogExecution('DEBUG','Scenario3if',CustShipCost);

				fields.push('custbody_ebizso_custshipcost');
				values.push(parseFloat(CustShipCost));

				fields.push('shippingcost');
				values.push(parseFloat(CustShipCost));
			}


			nlapiSubmitField('salesorder', SOInternalID, fields, values);

			id = SOInternalID;
		}
		else
		{
			nlapiLogExecution('DEBUG','Into Else ShipmentNO:',ShipmentNO);
			var SOrec=nlapiLoadRecord('salesorder',SOInternalID);
			var PreviousGSUSAShipcost=SOrec.getFieldValue('custbody_ebizso_actcost');
			var PreviousCustShipCost=SOrec.getFieldValue('shippingcost');
			var PreviousPublishedRate=SOrec.getFieldValue('custbody_ebizso_pubrate');
			nlapiLogExecution('DEBUG','GSUSAShipcostOld',PreviousGSUSAShipcost);
			nlapiLogExecution('DEBUG','CustShipCostOld',PreviousCustShipCost);
			nlapiLogExecution('DEBUG','PublishedRateOld',PreviousPublishedRate);
			var NewGSUSAShipcost;
			var NewCustShipCost;
			var NewPublishedRate;
			if((PreviousGSUSAShipcost==null)||(PreviousGSUSAShipcost==''))
			{
				NewGSUSAShipcost=GSUSAShipcost;
			}
			else
			{
				NewGSUSAShipcost=parseFloat(PreviousGSUSAShipcost)+parseFloat(GSUSAShipcost);
			}
			if((PreviousCustShipCost==null)||(PreviousCustShipCost==''))
			{
				NewCustShipCost=CustShipCost;
			}
			else
			{
				NewCustShipCost=parseFloat(PreviousCustShipCost)+parseFloat(CustShipCost);
			}
			if((PreviousPublishedRate==null)||(PreviousPublishedRate==''))
			{
				NewPublishedRate=PublishedRate;
			}
			else
			{
				NewPublishedRate=parseFloat(PreviousPublishedRate)+parseFloat(PublishedRate);
			}

			nlapiLogExecution('DEBUG','GSUSAShipcostNew',NewGSUSAShipcost);
			nlapiLogExecution('DEBUG','CustShipCostNew',NewCustShipCost);
			nlapiLogExecution('DEBUG','PublishedRateNew',NewPublishedRate);

			SOrec.setFieldValue('custbody_ebizso_actcost',parseFloat(NewGSUSAShipcost));
			if((SOcustomerCategory!="Retail")&&(SOOverRideChkd=='F'))
			{
				nlapiLogExecution('DEBUG','Scenario3else',NewCustShipCost);
				SOrec.setFieldValue('shippingcost',parseFloat(NewCustShipCost));
				SOrec.setFieldValue('custbody_ebizso_custshipcost',parseFloat(NewCustShipCost));
			}
			SOrec.setFieldValue('custbody_ebizso_pubrate',parseFloat(NewPublishedRate));
			SOrec.setFieldValue('custbody_ebiz_scac_code',ActualCarrierName);

			id = nlapiSubmitRecord(SOrec, true);
			nlapiLogExecution('DEBUG','Id1',id);
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in UpdateSalesOrder function',exp);
		nlapiLogExecution('DEBUG', 'unexpected error', exp.getDetails());
	}

	nlapiLogExecution('DEBUG','Out of UpdateSalesOrder',id);

	return id;
}

/*function UpdateShipManifestRecord(oldshipmentid,itemfullfilmentid)
{

	var fields = new Array();
	var values = new Array();

	fields[1] = 'custrecord_ship_nsconf_no';
	values[1] = itemfullfilmentid;
	fields[2] ='custrecord_ship_itemfulfill_no';
	values[2] = itemfullfilmentid;
	var updatefields = nlapiSubmitField('customrecord_ship_manifest',oldshipmentid,fields,values);

}*/
function GetShipManifestCustomRecords(fulfillmentOrder,vebizcontainerlp,Waveno,vsalesorderid)
{
	var str = 'fulfillmentOrder = ' + fulfillmentOrder + '<br>';
	str = str + 'vebizcontainerlp = ' + vebizcontainerlp+ '<br>';
	str = str + 'Waveno = ' + Waveno+ '<br>';
	str = str + 'vsalesorderid = ' + vsalesorderid+ '<br>';

	nlapiLogExecution('Debug', 'Into  GetShipManifestCustomRecords', str);

	var shipmentfilter = new Array();
	//shipmentfilter.push(new nlobjSearchFilter('custrecord_ship_ref3', null, 'is', fulfillmentOrder));
	if(vsalesorderid!=null && vsalesorderid!='')
		shipmentfilter.push(new nlobjSearchFilter('custrecord_ship_order', null, 'is', vsalesorderid));
	shipmentfilter.push(new nlobjSearchFilter('custrecord_ship_trackno', null, 'isempty'));
	shipmentfilter.push(new nlobjSearchFilter('custrecord_ship_ref5', null, 'isnot',vebizcontainerlp));
	if(Waveno!=null && Waveno!='' && Waveno!='null')
	{
		shipmentfilter.push(new nlobjSearchFilter('custrecord_ship_custom4', null, 'is',Waveno));
	}
	var shipmentcolumns = new Array();
	shipmentcolumns[0] = new nlobjSearchColumn('custrecord_ship_ref5');
	var searchshipmanifestshippedrecords= nlapiSearchRecord('customrecord_ship_manifest', null, shipmentfilter, shipmentcolumns);
	return searchshipmanifestshippedrecords;
}

function UpdateItemFulfillmentOld(transactionid,shipcharges,vebizcontainerlp,trackingno,pakageweight,fulfillmentOrder)
{
	nlapiLogExecution('DEBUG', 'updateitemfullfilment ', 'updateitemfullfilment');

	var OpenTaskTransaction = nlapiLoadRecord('itemfulfillment', transactionid);

	//Start Case :20124513
	var searchshipmanifestshippedrecords=GetShipManifestCustomRecords(fulfillmentOrder,vebizcontainerlp);
	nlapiLogExecution('ERROR', 'searchshipmanifestshippedrecords', searchshipmanifestshippedrecords);
	var FULFILLMENTATORDERLEVEL='T';
	if(FULFILLMENTATORDERLEVEL=='T')
	{
		if(searchshipmanifestshippedrecords==null)
		{
			OpenTaskTransaction.setFieldValue('shipstatus','C'); //Statusflag='C'
		}
		else
		{
			OpenTaskTransaction.setFieldValue('shipstatus','B'); //Statusflag='B'
		}
	}
	else
	{
		OpenTaskTransaction.setFieldValue('shipstatus','C');
	}
	//End case 20124513
	//Statusflag='C'
	//var lastcharges =OpenTaskTransaction.getFieldValue('shippingcost');
	var lastcharges =OpenTaskTransaction.getFieldValue('custbody_fulfill_shipcost');
	nlapiLogExecution('DEBUG', 'shipchargeslast ', lastcharges);
	//Code commented by Sravan for duplicate shipping cost issue for epicuren
	//var totalshipcharges=parseFloat(shipcharges);
	var totalshipcharges="";
	if((lastcharges=='')||(lastcharges==null))
	{
		totalshipcharges=parseFloat(shipcharges);
	}
	else
	{
		totalshipcharges=parseFloat(lastcharges)+parseFloat(shipcharges);
	}
	nlapiLogExecution('DEBUG', 'totalshipcharges', totalshipcharges);
	//OpenTaskTransaction.setFieldValue('shippingcost',totalshipcharges);
	OpenTaskTransaction.setFieldValue('custbody_fulfill_shipcost',totalshipcharges);
	OpenTaskTransaction.setFieldValue('generateintegratedshipperlabel','F');
	if(pakageweight=='0.0' || pakageweight=="0.0000" || pakageweight=='undefined' || pakageweight == 0 || pakageweight =='' || pakageweight =='null' || pakageweight == null || isNaN(pakageweight))
	{
		pakageweight='0.11';
	}
	nlapiLogExecution('DEBUG', 'updateitemfullfilment pakageweight', pakageweight);
	var trackingnumberarray = new Array();
	trackingnumberarray=trackingno.split(',');
	for(var t=0;t<trackingnumberarray.length;t++)
	{
		trackingno=trackingnumberarray[t];
		OpenTaskTransaction.selectNewLineItem('package');
		OpenTaskTransaction.setCurrentLineItemValue('package', 'packagedescr', vebizcontainerlp);
		OpenTaskTransaction.setCurrentLineItemValue('package', 'packagetrackingnumber', trackingno);
		OpenTaskTransaction.setCurrentLineItemValue('package', 'packageweight',pakageweight);
//		OpenTaskTransaction.setCurrentLineItemValue('item', 'location',wmslocation);
		OpenTaskTransaction.commitLineItem('package');
		OpenTaskTransaction.selectNewLineItem('packagefedex');
		OpenTaskTransaction.setCurrentLineItemValue('packagefedex', 'packagedescrfedex', vebizcontainerlp);
		OpenTaskTransaction.setCurrentLineItemValue('packagefedex', 'packagetrackingnumberfedex', trackingno);
		OpenTaskTransaction.setCurrentLineItemValue('packagefedex', 'packageweightfedex',pakageweight);
//		OpenTaskTransaction.setCurrentLineItemValue('item', 'location',wmslocation);
		OpenTaskTransaction.commitLineItem('packagefedex');
		OpenTaskTransaction.selectNewLineItem('packageups');
		OpenTaskTransaction.setCurrentLineItemValue('packageups', 'packagedescrups', vebizcontainerlp);
		OpenTaskTransaction.setCurrentLineItemValue('packageups', 'packagetrackingnumberups', trackingno);
		OpenTaskTransaction.setCurrentLineItemValue('packageups', 'packageweightups',pakageweight);
//		OpenTaskTransaction.setCurrentLineItemValue('item', 'location',wmslocation);
		OpenTaskTransaction.commitLineItem('packageups');
	}
//	OpenTaskTransaction.commitLineItem('item');
	var id= nlapiSubmitRecord(OpenTaskTransaction ,true);
	nlapiLogExecution('DEBUG', 'After open task submit record ', id);
	return id;
}
function UpdateItemFulfillment(transactionid,shipcharges,vebizcontainerlp,trackingno,pakageweight,fulfillmentOrder,Waveno,
		FULFILLMENTATORDERLEVEL,vsalesorderid)
{
	nlapiLogExecution('DEBUG', 'Into UpdateItemFulfillment');

	var str = 'transactionid. = ' + transactionid + '<br>';
	str = str + 'shipcharges. = ' + shipcharges + '<br>';	
	str = str + 'vebizcontainerlp. = ' + vebizcontainerlp + '<br>';
	str = str + 'trackingno. = ' + trackingno + '<br>';
	str = str + 'pakageweight. = ' + pakageweight + '<br>';
	str = str + 'fulfillmentOrder. = ' + fulfillmentOrder + '<br>';
	str = str + 'Waveno. = ' + Waveno + '<br>';
	str = str + 'vsalesorderid. = ' + vsalesorderid + '<br>';
	str = str + 'FULFILLMENTATORDERLEVEL. = ' + FULFILLMENTATORDERLEVEL + '<br>';

	nlapiLogExecution('Debug', 'Parameter Details', str);

	var OpenTaskTransaction = nlapiLoadRecord('itemfulfillment', transactionid);

	var vpickcartonsarr = getpickcartons(transactionid);

	//Start Case :20124513
	//var searchshipmanifestshippedrecords=GetShipManifestCustomRecords(fulfillmentOrder,vebizcontainerlp,Waveno,vsalesorderid);

	var searchshipmanifestshippedrecords = CheckAlltheCartonsShipped(fulfillmentOrder,vebizcontainerlp,Waveno,vsalesorderid,vpickcartonsarr);

	nlapiLogExecution('ERROR', 'CheckAlltheCartonsShipped', searchshipmanifestshippedrecords);
	var OpenTaskTransaction = nlapiLoadRecord('itemfulfillment', transactionid);

	//Statusflag='C'
	//var lastcharges =OpenTaskTransaction.getFieldValue('shippingcost');
	var lastcharges =OpenTaskTransaction.getFieldValue('custbody_fulfill_shipcost');
	nlapiLogExecution('DEBUG', 'shipchargeslast ', lastcharges);
	//Code commented by Sravan for duplicate shipping cost issue for epicuren
	//var totalshipcharges=parseFloat(shipcharges);
	var totalshipcharges="";
	if((lastcharges=='')||(lastcharges==null))
	{
		totalshipcharges=parseFloat(shipcharges);
	}
	else
	{
		totalshipcharges=parseFloat(lastcharges)+parseFloat(shipcharges);
	}
	nlapiLogExecution('DEBUG', 'totalshipcharges', totalshipcharges);
	//OpenTaskTransaction.setFieldValue('shippingcost',totalshipcharges);
	OpenTaskTransaction.setFieldValue('custbody_fulfill_shipcost',totalshipcharges);
	OpenTaskTransaction.setFieldValue('generateintegratedshipperlabel','F');
	//if(pakageweight=='0.0' || pakageweight=='0.0000' || pakageweight=='undefined'||pakageweight=='')
	if(pakageweight=='0.0' || pakageweight=="0.0000" || pakageweight=='undefined' || pakageweight == 0 || pakageweight =='' || pakageweight =='null' || pakageweight == null || isNaN(pakageweight))
	{
		pakageweight='0.11';
	}
	nlapiLogExecution('DEBUG', 'updateitemfullfilment pakageweight', pakageweight);
	var trackingnumberarray = new Array();
	trackingnumberarray=trackingno.split(',');
	for(var t=0;t<trackingnumberarray.length;t++)
	{
		trackingno=trackingnumberarray[t];
		OpenTaskTransaction.selectNewLineItem('package');
		OpenTaskTransaction.setCurrentLineItemValue('package', 'packagedescr', vebizcontainerlp);
		OpenTaskTransaction.setCurrentLineItemValue('package', 'packagetrackingnumber', trackingno);
		OpenTaskTransaction.setCurrentLineItemValue('package', 'packageweight',pakageweight);
//		OpenTaskTransaction.setCurrentLineItemValue('item', 'location',wmslocation);
		OpenTaskTransaction.commitLineItem('package');
		OpenTaskTransaction.selectNewLineItem('packagefedex');
		OpenTaskTransaction.setCurrentLineItemValue('packagefedex', 'packagedescrfedex', vebizcontainerlp);
		OpenTaskTransaction.setCurrentLineItemValue('packagefedex', 'packagetrackingnumberfedex', trackingno);
		OpenTaskTransaction.setCurrentLineItemValue('packagefedex', 'packageweightfedex',pakageweight);
//		OpenTaskTransaction.setCurrentLineItemValue('item', 'location',wmslocation);
		OpenTaskTransaction.commitLineItem('packagefedex');
		OpenTaskTransaction.selectNewLineItem('packageups');
		OpenTaskTransaction.setCurrentLineItemValue('packageups', 'packagedescrups', vebizcontainerlp);
		OpenTaskTransaction.setCurrentLineItemValue('packageups', 'packagetrackingnumberups', trackingno);
		OpenTaskTransaction.setCurrentLineItemValue('packageups', 'packageweightups',pakageweight);
//		OpenTaskTransaction.setCurrentLineItemValue('item', 'location',wmslocation);
		OpenTaskTransaction.commitLineItem('packageups');

		OpenTaskTransaction.selectNewLineItem('packageusps');
		OpenTaskTransaction.setCurrentLineItemValue('packageusps', 'packagedescrusps', vebizcontainerlp);
		OpenTaskTransaction.setCurrentLineItemValue('packageusps', 'packagetrackingnumberusps', trackingno);
		OpenTaskTransaction.setCurrentLineItemValue('packageusps', 'packageweightusps',pakageweight);
		OpenTaskTransaction.commitLineItem('packageusps');

	}
//	OpenTaskTransaction.commitLineItem('item');
	//case Starts 20148537 
	//The below code is commented by Satish.N on 02/24 as this value will be coming as a parameter to this function
	//var FULFILLMENTATORDERLEVEL=getSystemRuleValue('IF001');

	//get the itemfulfillment status from If record
	var itemfulfillmentstatus =OpenTaskTransaction.getFieldValue('shipstatus');
	var id="";
	if(FULFILLMENTATORDERLEVEL=='Y')//end case 20148537 
	{
		if(searchshipmanifestshippedrecords==null || searchshipmanifestshippedrecords=='')
		{
			OpenTaskTransaction.setFieldValue('shipstatus','C'); //Statusflag='C'
			id= nlapiSubmitRecord(OpenTaskTransaction ,true);
			nlapiLogExecution('DEBUG', 'Updated as C');
			updateOldItemFulfillment(fulfillmentOrder,vebizcontainerlp,Waveno);
		}
		else
		{
			if(searchshipmanifestshippedrecords.length==0)
			{
				OpenTaskTransaction.setFieldValue('shipstatus','C'); //Statusflag='C'
				id= nlapiSubmitRecord(OpenTaskTransaction ,true);
				nlapiLogExecution('DEBUG', 'Updated as C - 2');
				updateOldItemFulfillment(fulfillmentOrder,vebizcontainerlp,Waveno);
			}
			else if(itemfulfillmentstatus!="C")
			{
				OpenTaskTransaction.setFieldValue('shipstatus','B');
				id= nlapiSubmitRecord(OpenTaskTransaction ,true);
				nlapiLogExecution('DEBUG', 'Updated as B');
			}

		}
	}
	else
	{
		OpenTaskTransaction.setFieldValue('shipstatus','C');
		id= nlapiSubmitRecord(OpenTaskTransaction ,true);
		nlapiLogExecution('DEBUG', 'Updated as C - 3');
	}
	nlapiLogExecution('DEBUG', 'After open task submit record ', id);
	return id;
}

function updateOldItemFulfillment(vfulfillordno,vebizcontlp,vWaveno)
{
	var str = 'fulfillmentOrder = ' + vfulfillordno + '<br>';
	str = str + 'vebizcontainerlp = ' + vebizcontlp+ '<br>';
	str = str + 'vWaveno = ' + vWaveno+ '<br>';

	nlapiLogExecution('Debug', 'Into  updateOldItemFulfillment', str);

	var shipmentfilter = new Array();
	shipmentfilter.push(new nlobjSearchFilter('custrecord_ship_ref3', null, 'is', vfulfillordno));
	shipmentfilter.push(new nlobjSearchFilter('custrecord_ship_trackno', null, 'isnotempty'));
	shipmentfilter.push(new nlobjSearchFilter('custrecord_ship_ref5', null, 'isnot',vebizcontlp));
	if(vWaveno!=null && vWaveno!='')
	{
		shipmentfilter.push(new nlobjSearchFilter('custrecord_ship_custom4', null, 'is',vWaveno));
	}
	var shipmentcolumns = new Array();
	shipmentcolumns[0] = new nlobjSearchColumn('custrecord_ship_ref5');
	shipmentcolumns[1] = new nlobjSearchColumn('custrecord_ship_itemfulfill_no');
	var searchshipmanifestshippedrecords= nlapiSearchRecord('customrecord_ship_manifest', null, shipmentfilter, shipmentcolumns);
	if(searchshipmanifestshippedrecords!=null && searchshipmanifestshippedrecords!='')
	{
		for (var j = 0;j < searchshipmanifestshippedrecords.length; j++) 
		{
			var vitemfulfillno = searchshipmanifestshippedrecords[j].getValue('custrecord_ship_itemfulfill_no');

			if(vitemfulfillno!=null && vitemfulfillno!='')
			{
				var OpenTaskTransaction = nlapiLoadRecord('itemfulfillment', vitemfulfillno);

				var ifstatus = OpenTaskTransaction.getFieldValue('shipstatus');
				if(ifstatus=='B')
				{
					OpenTaskTransaction.setFieldValue('shipstatus','C');
					id= nlapiSubmitRecord(OpenTaskTransaction ,true);
				}
			}
		}
	}

	nlapiLogExecution('Debug', 'Out of  updateOldItemFulfillment');
}

function clearOutboundInventory(vebizcontainerlp)
{
	nlapiLogExecution('DEBUG', "Into  clearOutboundInventory" , vebizcontainerlp);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null,'is',vebizcontainerlp));
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['18']));

	var columns= new Array();

	var inventorysearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv',null,filters ,columns);
	if(inventorysearchresults!=null && inventorysearchresults!='' )
	{
		for (var i = 0; inventorysearchresults != null && i < inventorysearchresults.length; i++) 
		{
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', inventorysearchresults[i].getId());
			nlapiLogExecution('DEBUG', 'Deleted Outbound Inventory Record : ', id);	
		}
	}

	nlapiLogExecution('DEBUG', "Out of  clearOutboundInventory" , vebizcontainerlp);	
}

function getopentaskdetails(vebizcontainerlp,pckgtype)
{
	nlapiLogExecution('DEBUG', 'Into getopentaskdetails');	
	nlapiLogExecution('DEBUG', 'vebizcontainerlp', vebizcontainerlp);	

	var opentasksearchresults = new Array();

	if(vebizcontainerlp!=null && vebizcontainerlp!='')
	{
		var filters = new Array();

		if(pckgtype!='SHIP')
			filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'is',vebizcontainerlp));		
		else
			filters.push(new nlobjSearchFilter('custrecord_ship_lp_no', null,'is',vebizcontainerlp));

		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['28','7','14']));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isnotempty'));

		var columns= new Array();

		columns[0] = new nlobjSearchColumn('custrecord_ebiz_nsconfirm_ref_no');
		columns[1] = new nlobjSearchColumn('name');
		columns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		columns[3] = new nlobjSearchColumn('custrecord_wms_location');
		columns[4] = new nlobjSearchColumn('custrecord_act_qty');
		columns[5] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		columns[6] = new nlobjSearchColumn('custrecord_line_no');
		columns[7] = new nlobjSearchColumn('custrecord_container_lp_no');

		opentasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters ,columns);		
	}

	nlapiLogExecution('DEBUG', 'Out of getopentaskdetails');

	return opentasksearchresults;
}

function AutoShipMent(VOrdNo)
{
	var searchresultsTemp=new Array();
	// To get the Wave details based on selection criteria

	var searchresults=GetWaveOrdDetails(null,VOrdNo,null,null,null);
	if(searchresults!=null)
	{
		nlapiLogExecution('DEBUG', "dataNotFoundMsg " , dataNotFoundMsg);
		if(dataNotFoundMsg=="")
		{
			nlapiLogExecution('DEBUG', "searchresultsTemp " , searchresultsTemp.length);
			var vline, vitem, vqty, vTaskType, vmainline, vrecid, vlocation, vLpno, vSKU, vlocationid, vinvrefno,vskustatus,vpackcode,vdono,vSOno,vuomid;
			var vcontLP,vLP,vSizeId,vTotWt,vTotCube;
			var now = new Date();		
			var lineCnt = searchresults.length;	
			var matchFound = true;
			var actualDate = DateStamp();
			var actualTime = TimeStamp();
			var sizeId;
			var TotWeight=0,TotVolume=0,inventoryInout;
			var vTareWeight=0,vTareVol=0;
			var NewShipLPNo;

			//To calulate auto Buildship# from LP Range
			var AutoShipLPNo=getAutoShipLP();
			NewShipLPNo=AutoShipLPNo;

			if(matchFound){
				nlapiLogExecution('DEBUG',"if: matchFound ", matchFound );
				nlapiLogExecution('DEBUG',"AutoShipLPNo ", NewShipLPNo );
				var picktaskid;
				var LpMasterId;

				var vBoolAnythingChecked=false;
				for (var k = 0; k < lineCnt; k++) {

					var searchresult = searchresults[k];	
					if(searchresult!=null)
					{
						vline = searchresult.custrecord_line_no; 
						vitem = searchresult.custrecord_Ebiz_sku_no;
						if(searchresult.custrecord_Act_qty==null || searchresult.custrecord_Act_qty== "")
							vqty=0;
						else
							vqty= parseFloat(searchresult.custrecord_Act_qty);

						nlapiLogExecution("ERROR", "vqty ", vqty);
						vTaskType = searchresult.custrecord_Tasktype;
						vLpno = searchresult.custrecord_ebiz_sku_no;
						vlocationid = searchresult.custrecord_Actbeginloc;
						vlocation = searchresult.custrecord_Actbeginloc;
						vskustatusValue= searchresult.custrecord_Sku_status;
						vskustatus= searchresult.custrecord_Sku_status;
						vpackcode= searchresult.custrecord_Packcode;
						vdono=searchresult.Name;
						if(vdono != null && vdono != "")
							vSOno=vdono.split('.')[0];
						vSKUNo = searchresult.custrecord_Sku;
						vSKU = searchresult.custrecord_Sku; 
						vcontLP = searchresult.custrecord_Container_lp_no;
						vLP = searchresult.custrecord_Ebiz_lpmaster_lp;
						vSizeId = searchresult.custrecord_Ebiz_lpmaster_sizeid;
						vTotWt = searchresult.custrecord_Ebiz_lpmaster_totwght;
						vTotCube = searchresult.custrecord_Ebiz_lpmaster_totcube; 
						vuomid=searchresult.custrecord_Uom_id; 	
						picktaskid = searchresult.gId; 
						LpMasterId = searchresult.gidLp;
						var vCartWeight = vTotWt; 
						var vCartVol = vTotCube;


						//To calculate Total weight and volume

						if(vCartWeight!=null && vCartWeight !="")
							TotWeight+=parseFloat(vCartWeight);
						if(vCartVol!=null && vCartVol !="")
							TotVolume+=parseFloat(vCartVol);


						nlapiLogExecution('DEBUG', "Line Count " , k);
						nlapiLogExecution('DEBUG', "picktaskid" , picktaskid);
						nlapiLogExecution('DEBUG', "LpMasterId" , LpMasterId);

						// To update ShipLP# in opentask
						var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', picktaskid);
						transaction.setFieldValue('custrecord_wms_status_flag', "7"); // for status B(Built onto Ship Unit)
						transaction.setFieldValue('custrecord_ship_lp_no', NewShipLPNo);
						nlapiSubmitRecord(transaction, true);
						nlapiLogExecution('DEBUG', 'opentask updated ', "");

						// To update ShipLP# in mast LP

						var transaction = nlapiLoadRecord('customrecord_ebiznet_master_lp', LpMasterId); 			 
						transaction.setFieldValue('custrecord_ebiz_lpmaster_masterlp', NewShipLPNo);
						nlapiSubmitRecord(transaction, true);

						nlapiLogExecution('DEBUG', 'LP Master updated ', ""); 
						MoveTaskRecord(picktaskid);

						//Moving Pick Task to EbizTask
						nlapiLogExecution('DEBUG', 'eBiztask updated ', "");

						var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
						//populating the fields
						customrecord.setFieldValue('custrecord_tasktype', '4'); //Ship Task
						//customrecord.setFieldValue('custrecord_container', sizeId); //Ship Task 
						customrecord.setFieldValue('custrecord_ebiz_wave_no', VQbwaveno);
						customrecord.setFieldValue('custrecord_totalcube', parseFloat(TotVolume).toFixed(5));
						customrecord.setFieldValue('custrecord_total_weight', parseFloat(TotWeight).toFixed(5));
						customrecord.setFieldValue('custrecord_ship_lp_no', NewShipLPNo);
						customrecord.setFieldValue('name', NewShipLPNo);
						customrecord.setFieldValue('custrecordact_begin_date', actualDate);
						customrecord.setFieldValue('custrecord_wms_status_flag', '7');// for status B(Built onto Ship Unit)
						customrecord.setFieldValue('custrecord_actualbegintime', actualTime);
						customrecord.setFieldValue('custrecord_ebiz_cntrl_no', searchresult.custrecord_ebiz_cntrl_no);
						customrecord.setFieldValue('custrecord_site_id', vSiteId);//For SiteId 
						nlapiLogExecution('DEBUG', 'Submitting SHIP record', 'TRN_OPENTASK');
						//commit the record to NetSuite
						var opentaskrecid = nlapiSubmitRecord(customrecord, true);
						nlapiLogExecution('DEBUG', 'Submitted SHIP record', 'TRN_OPENTASK');


						// To Create record in LP Master
						var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp'); 
						customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', NewShipLPNo);
						//customrecord.setFieldValue('custrecord_ebiz_lpmaster_sizeid', sizeId);
						customrecord.setFieldValue('custrecord_ebiz_lpmaster_totwght', parseFloat(TotWeight).toFixed(5));
						customrecord.setFieldValue('custrecord_ebiz_lpmaster_lptype', '3');//SHIP
						customrecord.setFieldValue('name', NewShipLPNo);
						customrecord.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(TotVolume).toFixed(5));
						customrecord.setFieldValue('custrecord_ebiz_lpmaster_wmsstatusflag', '28');// For Closed(STATUS.OUTBOUND.PACK_COMPLETE)
						//commit the record to NetSuite
						var masterlprecid = nlapiSubmitRecord(customrecord, true);


						//Load Depart trailer  code.


						var filters = new Array();		

						filters[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['4']); //ship task
						var j = 1;
						//nlapiLogExecution('DEBUG', "VQbwave " + VQbwave);
						if (VQbwaveno != "") {
							filters[j] = new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', VQbwaveno);
							j = j + 1;
						}
						if (NewShipLPNo != "") {
							filters[j] = new nlobjSearchFilter('custrecord_ebiz_ship_lp_no', null, 'starts with', NewShipLPNo);
							j = j + 1;
						}
						filters[j] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['7']);


						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_line_no');
						columns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
						columns[2] = new nlobjSearchColumn('custrecord_act_qty');
						columns[3] = new nlobjSearchColumn('custrecord_tasktype');
						columns[4] = new nlobjSearchColumn('custrecord_ebiz_ship_lp_no');
						columns[5] = new nlobjSearchColumn('custrecord_actbeginloc');
						columns[6] = new nlobjSearchColumn('custrecord_sku');
						columns[7] = new nlobjSearchColumn('name');			
						columns[8] = new nlobjSearchColumn('custrecord_container_lp_no'); 
						columns[9] = new nlobjSearchColumn('custrecord_sku_status');
						columns[10] = new nlobjSearchColumn('custrecord_packcode');
						columns[11] = new nlobjSearchColumn('custrecord_uom_id');
						columns[12] = new nlobjSearchColumn('custrecord_total_weight');
						columns[13] = new nlobjSearchColumn('custrecord_totalcube');
						columns[14] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');


						var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);


						var vline, vitem, vqty, vTaskType, vmainline, vrecid, vlocation, vLpno, vSKU, vlocationid, vinvrefno, vitemstatus, vpackcode;
						var vtotcube = 0;
						var vtotwght=0;
						for (var i = 0; searchresults != null && i < searchresults.length; i++) {

							nlapiLogExecution('DEBUG', "searchresults" + searchresults.length);
							var searchresult = searchresults[i];

							vline = searchresult.getValue('custrecord_line_no');
							vitem = searchresult.getValue('custrecord_ebiz_sku_no');

							vqty = searchresult.getValue('custrecord_act_qty');
							vTaskType = searchresult.getValue('custrecord_tasktype');
							vLpno = searchresult.getValue('custrecord_ebiz_ship_lp_no');
							vlocationid = searchresult.getValue('custrecord_actbeginloc');

							vlocation = searchresult.getText('custrecord_actbeginloc');
							vitemstatus = searchresult.getValue('custrecord_sku_status');
							vpackcode = searchresult.getValue('custrecord_packcode');

							nlapiLogExecution('DEBUG', "vlocationid" + vlocationid);
							nlapiLogExecution('DEBUG', "vlocation" + vlocation);


							vSKU = searchresult.getText('custrecord_sku');

							var vlinecube = searchresult.getValue('custrecord_totalcube');
							var vlinewght = searchresult.getValue('custrecord_total_weight');
							if (!isNaN(vlinecube)) {
								vtotcube = parseFloat(vtotcube) + parseFloat(vlinecube);
							}

							if (!isNaN(vlinewght)) {
								vtotwght = parseFloat(vtotwght) + parseFloat(vlinewght);
							}
							var now = new Date();					
							var itemname= vSKU;
							var itemNo= "";
							var avlqty=vqty; 	 
							var ebizDono=searchresult.getValue('custrecord_ebiz_cntrl_no'); 
							var Lineno=vline; 
							var vDoname= searchresult.getValue('name');
							var ebizWaveNo = VQbwaveno;
							var vactLocation = vlocation;
							var picktaskid= searchresult.getId();					

							var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', picktaskid);
							transaction.setFieldValue('custrecord_wms_status_flag', "14"); //14-S(shipped)				
							nlapiSubmitRecord(transaction, true);
							nlapiLogExecution('DEBUG', "ebizDono" + ebizDono);

							var docols = nlapiLookupField('customrecord_ebiznet_ordline',ebizDono,['name','custrecord_ns_ord']);
							var salsordInternid=docols.name;
							var vsono=docols.custrecord_ns_ord;
							nlapiLogExecution('DEBUG', "salsordInternid" + salsordInternid);
							var type = "salesorder";					
							var putgenqty = 0;
							var putconfqty = 0;
							var ttype = "SHIP";
							var confqty = avlqty;				
							var tranValue = TrnLineUpdation(type, ttype, salsordInternid, vsono, Lineno, itemNo, avlqty, confqty);

							MoveTaskRecord(picktaskid);
						}
					}
				}
			}
		}
	}
}

function GetWaveOrdDetails(VQbwave,VOrdNo,VShipLP,VConsignee,searchresultsTemp)
{

	nlapiLogExecution('DEBUG',"Into GetWaveOrdDetails (VOrdNo) ", VOrdNo );

	var filters = new Array();
	var searchresults =new Array();

	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));  //Pick Task
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['8','28'])); //statusflag='C'and STATUS.OUTBOUND.PACK_COMPLETE

	filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', [VOrdNo])); //for OrdNo'

	nlapiLogExecution('DEBUG',"OrdNo ", VOrdNo );

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_line_no');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[2] = new nlobjSearchColumn('custrecord_act_qty');
	columns[3] = new nlobjSearchColumn('custrecord_tasktype');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_ship_lp_no');
	columns[5] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[6] = new nlobjSearchColumn('custrecord_sku');
	columns[7] = new nlobjSearchColumn('name');    	
	columns[8] = new nlobjSearchColumn('custrecord_container_lp_no');    	 
	columns[9] = new nlobjSearchColumn('custrecord_sku_status');
	columns[10] = new nlobjSearchColumn('custrecord_packcode');
	columns[11] = new nlobjSearchColumn('custrecord_uom_id');
	columns[12] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[13] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');		
	columns[14] = new nlobjSearchColumn('custrecord_totalcube');
	columns[15] = new nlobjSearchColumn('custrecord_total_weight');
	columns[16] = new nlobjSearchColumn('custrecord_comp_id');
	columns[17] = new nlobjSearchColumn('custrecord_site_id');
	columns[18] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
	// To get the data from Opentask based on selection criteria
	searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(searchresults !=null && searchresults != "" )
	{
		nlapiLogExecution('DEBUG',"Open Task Records Found ", searchresults );
		searchresultsTemp=searchresults;	var searchData=new Array();
		if(searchresults.length>0)
		{
			VQbwaveno=searchresults[0].getValue('custrecord_ebiz_wave_no') ;
			nlapiLogExecution('DEBUG',"VQbwaveno ", VQbwaveno );
			for(var i=0;i<searchresults.length;i++)
			{		
				columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
				columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sizeid');
				columns[2] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
				columns[3] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totcube');

				filters = new Array();
				filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', searchresults[i].getValue('custrecord_container_lp_no'))); //for OrdNo'

				// To get the Container Details from master lp
				var searchresultsLP = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
				//nlapiLogExecution('DEBUG',"searchresults MasterLP ",searchresultsLP.length );

				// TO map container lp# of opentask with lp# of mast lp
				nlapiLogExecution('DEBUG',"searchresultsLP", searchresultsLP );
				if(searchresultsLP != null && searchresultsLP != "" )
				{
					if(searchresultsLP.length>0)
					{
						vCompId=searchresults[i].getValue('custrecord_comp_id');
						vSiteId=searchresults[i].getValue('custrecord_site_id');

						nlapiLogExecution('DEBUG',"searchresults value ", searchresults[i].getValue('custrecord_container_lp_no') );

						searchData.push(new openTaskRec(searchresults[i].getValue('custrecord_line_no'),searchresults[i].getValue('custrecord_ebiz_sku_no'),
								searchresults[i].getValue('custrecord_act_qty'),searchresults[i].getValue('custrecord_tasktype'),searchresults[i].getValue('custrecord_ebiz_ship_lp_no')
								,searchresults[i].getText('custrecord_actbeginloc'),searchresults[i].getText('custrecord_sku'),searchresults[i].getValue('name')
								,searchresults[i].getValue('custrecord_container_lp_no'),searchresults[i].getText('custrecord_sku_status'),searchresults[i].getValue('custrecord_packcode')
								,searchresults[i].getValue('custrecord_uom_id'),searchresults[i].getValue('custrecord_ebiz_sku_no'),searchresults[i].getValue('custrecord_ebiz_cntrl_no')
								,searchresultsLP[0].getValue('custrecord_ebiz_lpmaster_lp'),searchresultsLP[0].getValue('custrecord_ebiz_lpmaster_sizeid')
								,searchresultsLP[0].getValue('custrecord_ebiz_lpmaster_totwght'),searchresultsLP[0].getValue('custrecord_ebiz_lpmaster_totcube'),searchresults[i].getValue('custrecord_total_weight'),searchresults[i].getValue('custrecord_totalcube'), searchresults[i].getId(), searchresultsLP[0].getId()));

						nlapiLogExecution('DEBUG',"searchresults Data ",searchresults.length );
					}
				}

				else
				{
					dataNotFoundMsg="No records found for the given search criteria";
				}
				if(searchresults == null || searchresults == "")
				{
					dataNotFoundMsg="No records found for the given search criteria";
				}
			}
			searchresults=searchData;
		}
	}
	else
	{
		dataNotFoundMsg="No records found for the given search criteria";
	}
	return searchresults;
}

//here create duplicate record in shipmanifest
function CreateShipmanifestRecord(shipinternalid,containerlp,VsalesOrder)
{

	var shipmentgetrecord= nlapiLoadRecord('customrecord_ship_manifest', shipinternalid,containerlp);
	var createrecord = nlapiCreateRecord('customrecord_ship_manifest');
	createrecord.setFieldValue('custrecord_ship_order',VsalesOrder);
	createrecord.setFieldValue('custrecord_ship_location',shipmentgetrecord.getFieldValue('custrecord_ship_location'));
	createrecord.setFieldValue('custrecord_ship_company',shipmentgetrecord.getFieldValue('custrecord_ship_company'));
	createrecord.setFieldValue('custrecord_ship_orderno',shipmentgetrecord.getFieldValue('custrecord_ship_orderno'));
	createrecord.setFieldValue('custrecord_ship_contlp',shipmentgetrecord.getFieldValue('custrecord_ship_contlp'));
	createrecord.setFieldValue('custrecord_ship_carrier',shipmentgetrecord.getFieldValue('custrecord_ship_carrier'));
	createrecord.setFieldValue('custrecord_ship_servicelevel',shipmentgetrecord.getFieldValue('custrecord_ship_servicelevel'));
	createrecord.setFieldValue('custrecord_ship_account',shipmentgetrecord.getFieldValue('custrecord_ship_account'));
	createrecord.setFieldValue('custrecord_ship_sendmail',shipmentgetrecord.getFieldValue('custrecord_ship_sendmail'));
	createrecord.setFieldValue('custrecord_ship_email',shipmentgetrecord.getFieldValue('custrecord_ship_email'));
	createrecord.setFieldValue('custrecord_ship_custid',shipmentgetrecord.getFieldValue('custrecord_ship_custid'));
	createrecord.setFieldValue('custrecord_ship_contactname',shipmentgetrecord.getFieldValue('custrecord_ship_contactname'));
	createrecord.setFieldValue('custrecord_ship_addr1',shipmentgetrecord.getFieldValue('custrecord_ship_addr1'));
	createrecord.setFieldValue('custrecord_ship_addr2',shipmentgetrecord.getFieldValue('custrecord_ship_addr2'));
	createrecord.setFieldValue('custrecord_ship_addr3',shipmentgetrecord.getFieldValue('custrecord_ship_addr3'));
	createrecord.setFieldValue('custrecord_ship_city',shipmentgetrecord.getFieldValue('custrecord_ship_city'));
	createrecord.setFieldValue('custrecord_ship_state',shipmentgetrecord.getFieldValue('custrecord_ship_state'));
	createrecord.setFieldValue('custrecord_ship_country',shipmentgetrecord.getFieldValue('custrecord_ship_country'));
	createrecord.setFieldValue('custrecord_ship_zip',shipmentgetrecord.getFieldValue('custrecord_ship_zip'));
	createrecord.setFieldValue('custrecord_ship_phone',shipmentgetrecord.getFieldValue('custrecord_ship_phone'));
	createrecord.setFieldValue('custrecord_ship_pkgtype',shipmentgetrecord.getFieldValue('custrecord_ship_pkgtype'));
	createrecord.setFieldValue('custrecord_ship_pkgwght',shipmentgetrecord.getFieldValue('custrecord_ship_pkgwght'));
	var actuvalweight="";
	createrecord.setFieldValue('custrecord_ship_actwght',actuvalweight);
	createrecord.setFieldValue('custrecord_ship_pkgno',shipmentgetrecord.getFieldValue('custrecord_ship_pkgno'));
	createrecord.setFieldValue('custrecord_ship_pkgcount',shipmentgetrecord.getFieldValue('custrecord_ship_pkgcount'));
	createrecord.setFieldValue('custrecord_ship_ref1',shipmentgetrecord.getFieldValue('custrecord_ship_ref1'));
	createrecord.setFieldValue('custrecord_ship_ref2',shipmentgetrecord.getFieldValue('custrecord_ship_ref2'));
	createrecord.setFieldValue('custrecord_ship_ref3',shipmentgetrecord.getFieldValue('custrecord_ship_ref3'));
	createrecord.setFieldValue('custrecord_ship_ref4',shipmentgetrecord.getFieldValue('custrecord_ship_ref4'));
	createrecord.setFieldValue('custrecord_ship_ref5',shipmentgetrecord.getFieldValue('custrecord_ship_ref5'));
	createrecord.setFieldValue('custrecordship_dvflag',shipmentgetrecord.getFieldValue('custrecordship_dvflag'));
	createrecord.setFieldValue('custrecord_ship_value',shipmentgetrecord.getFieldValue('custrecord_ship_value'));
	createrecord.setFieldValue('custrecord_ship_codflag',shipmentgetrecord.getFieldValue('custrecord_ship_codflag'));
	createrecord.setFieldValue('custrecord_ship_satflag',shipmentgetrecord.getFieldValue('custrecord_ship_satflag'));
	createrecord.setFieldValue('custrecord_ship_length',parseFloat(shipmentgetrecord.getFieldValue('custrecord_ship_length')).toFixed(5));
	createrecord.setFieldValue('custrecord_ship_width',parseFloat(shipmentgetrecord.getFieldValue('custrecord_ship_width')).toFixed(5));
	createrecord.setFieldValue('custrecord_ship_height',parseFloat(shipmentgetrecord.getFieldValue('custrecord_ship_height')).toFixed(5));
	var trackingnumber="";
	createrecord.setFieldValue('custrecord_ship_trackno',trackingnumber);
	createrecord.setFieldValue('custrecord_ship_masttrackno',trackingnumber);
	createrecord.setFieldValue('custrecord_ship_consignee',shipmentgetrecord.getFieldValue('custrecord_ship_consignee'));
	createrecord.setFieldValue('custrecord_ship_ordertype',shipmentgetrecord.getFieldValue('custrecord_ship_ordertype'));
	createrecord.setFieldValue('custrecord_ship_paymethod',shipmentgetrecord.getFieldValue('custrecord_ship_paymethod'));
	createrecord.setFieldValue('custrecord_ship_custom1',shipmentgetrecord.getFieldValue('custrecord_ship_custom1'));
	createrecord.setFieldValue('custrecord_ship_custom2',shipmentgetrecord.getFieldValue('custrecord_ship_custom2'));
	createrecord.setFieldValue('custrecord_ship_custom3',shipmentgetrecord.getFieldValue('custrecord_ship_custom3'));
	createrecord.setFieldValue('custrecord_ship_custom4',shipmentgetrecord.getFieldValue('custrecord_ship_custom4'));
	var custom5="D";
	createrecord.setFieldValue('custrecord_ship_custom5',custom5);
	var shipcharges="";
	createrecord.setFieldValue('custrecord_ship_charges',shipcharges);
	createrecord.setFieldValue('custrecord_ship_codamount',shipmentgetrecord.getFieldValue('custrecord_ship_codamount'));
	var setvoidflag="N";
	createrecord.setFieldValue('custrecord_ship_void',setvoidflag);
	createrecord.setFieldValue('custrecord_ship_system',shipmentgetrecord.getFieldValue('custrecord_ship_system'));
	createrecord.setFieldValue('custrecord_ship_user',shipmentgetrecord.getFieldValue('custrecord_ship_user'));
	createrecord.setFieldValue('custrecord_ship_nsconf_no',shipmentgetrecord.getFieldValue('custrecord_ship_nsconf_no'));
	createrecord.setFieldValue('custrecord_ship_host_system',shipmentgetrecord.getFieldValue('custrecord_ship_host_system'));
	createrecord.setFieldValue('name',shipinternalid);
	var id = nlapiSubmitRecord(createrecord, true);
}

function getAutoShipLP()
{
	var AutoShipLPNo="1";	 
	AutoShipLPNo=GetMaxLPNo('1', '3');// for Auto generated with SHIP LPType
	return AutoShipLPNo;
}

function openTaskRec(custrecord_line_no, custrecord_ebiz_sku_no, custrecord_act_qty,custrecord_tasktype
		,custrecord_ebiz_ship_lp_no,custrecord_actbeginloc,custrecord_sku,name
		,custrecord_container_lp_no,custrecord_sku_status,custrecord_packcode,custrecord_uom_id
		,custrecord_ebiz_sku_no,custrecord_ebiz_cntrl_no,custrecord_ebiz_lpmaster_lp,custrecord_ebiz_lpmaster_sizeid
		,custrecord_ebiz_lpmaster_totwght,custrecord_ebiz_lpmaster_totcube,custrecord_total_weight,custrecord_totalcube,gid,gidlp) {
	this.custrecord_Line_no = custrecord_line_no;
	this.custrecord_Ebiz_sku_no = custrecord_ebiz_sku_no;
	this.custrecord_Act_qty = custrecord_act_qty;
	this.custrecord_Tasktype = custrecord_tasktype;
	this.custrecord_Ebiz_ship_lp_no = custrecord_ebiz_ship_lp_no;
	this.custrecord_Actbeginloc = custrecord_actbeginloc;
	this.custrecord_Sku = custrecord_sku;
	this.Name = name;
	this.custrecord_Container_lp_no = custrecord_container_lp_no;
	this.custrecord_Sku_status = custrecord_sku_status;
	this.custrecord_Packcode = custrecord_packcode;
	this.custrecord_Uom_id = custrecord_uom_id;
	this.custrecord_Ebiz_sku_no = custrecord_ebiz_sku_no;
	this.custrecord_Ebiz_cntrl_no = custrecord_ebiz_cntrl_no;
	this.custrecord_Ebiz_lpmaster_lp = custrecord_ebiz_lpmaster_lp;
	this.custrecord_Ebiz_lpmaster_sizeid = custrecord_ebiz_lpmaster_sizeid;
	this.custrecord_Ebiz_lpmaster_totwght = custrecord_ebiz_lpmaster_totwght;
	this.custrecord_Ebiz_lpmaster_totcube = custrecord_ebiz_lpmaster_totcube;
	this.custrecord_Total_weight = custrecord_total_weight;
	this.custrecord_Totalcube = custrecord_totalcube;
	this.gId = gid;
	this.gidLp = gidlp;
}
function GetCarServiceDetails(carrier)
{
	try
	{
		nlapiLogExecution('ERROR', 'Into GetCarServiceDetails ', carrier);	
		var filter=new Array();
		var columns=new Array();
		var CarServiceDetailsListT = new Array();
		filter.push(new nlobjSearchFilter('custrecord_carrier_id',null,'is',carrier));	

		columns.push(new nlobjSearchColumn('custrecord_carrier_id'));
		columns.push(new nlobjSearchColumn('custrecord_carrier_name'));
		columns.push(new nlobjSearchColumn('custrecord_carrier_service_level'));

		var carrierrec=nlapiSearchRecord('customrecord_ebiznet_carrier',null,filter,columns);
		if ( carrierrec !=null && carrierrec.length > 0){
			for ( var i = 0 ; i < carrierrec.length; i++ ){
				var CarServiceDetailsList = new Array();
				CarServiceDetailsList[0] = carrierrec[i].getValue('custrecord_carrier_id');
				CarServiceDetailsList[1] = carrierrec[i].getValue('custrecord_carrier_name');
				CarServiceDetailsList[2] = carrierrec[i].getValue('custrecord_carrier_service_level');				

				CarServiceDetailsListT.push(CarServiceDetailsList);
			}
		}
	}
	catch(exception) {
		nlapiLogExecution('ERROR', 'Exception in GetCarServiceDetails', exception);
	}
	nlapiLogExecution('ERROR', 'GetCarServiceDetails', 'end');
	return CarServiceDetailsListT;
}

function IsShipmanifestContLpExist(vContLpNo,site)
{
	nlapiLogExecution('Debug', 'Into IsContLpExist',vContLpNo);	
	var IsContLpExist='F';

	try
	{
		var filter = new Array();
		filter.push(new nlobjSearchFilter('custrecord_ship_contlp',null,'is',vContLpNo));
		if(site!=null)
		{
			filter.push(new nlobjSearchFilter('custrecord_ship_location',null,'is',site));	
		}
		filter.push(new nlobjSearchFilter('custrecord_ship_void',null,'is','N'));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ship_orderno');
		var manifestList= nlapiSearchRecord('customrecord_ship_manifest',null,filter,columns);
		if(manifestList!=null && manifestList!='')
			IsContLpExist='T';		
	}
	catch(e)
	{
		nlapiLogExecution('Debug', 'unexpected error in IsContLpExist');	
	}
	nlapiLogExecution('Debug', 'Out of IsContLpExist',IsContLpExist);	
	return IsContLpExist;
}

function updatefulfillmentorderstatus(vfono,vitemfulfillmentno,vebizcontainerlp)
{

	var str = 'vfono = ' + vfono + '<br>';
	str = str + 'vitemfulfillmentno = ' + vitemfulfillmentno+ '<br>';
	str = str + 'vebizcontainerlp = ' + vebizcontainerlp+ '<br>';

	nlapiLogExecution('Debug', 'Into  updatefulfillmentorderstatus', str);

	var filters = new Array();

	//filters.push(new nlobjSearchFilter('name', null,'is',vfono));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['28']));
	//filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'is',vitemfulfillmentno));
	filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vebizcontainerlp));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isnotempty'));

	var columns= new Array();

	columns[0] = new nlobjSearchColumn('custrecord_ebiz_nsconfirm_ref_no');
	columns[1] = new nlobjSearchColumn('name');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[3] = new nlobjSearchColumn('custrecord_wms_location');
	columns[4] = new nlobjSearchColumn('custrecord_act_qty');
	columns[5] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[6] = new nlobjSearchColumn('custrecord_line_no');
	columns[7] = new nlobjSearchColumn('custrecord_container_lp_no');

	var otsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters ,columns);
	if(otsearchresults!=null && otsearchresults!='')
	{
		nlapiLogExecution('Debug', 'Open Tasks Count',otsearchresults.length);

		for ( var s = 0 ; s < otsearchresults.length; s++ )
		{
			var ffOrdNo = otsearchresults[s].getValue('name');
			var ffOrdLineNo = otsearchresults[s].getValue('custrecord_line_no');
			var shipqty = otsearchresults[s].getValue('custrecord_act_qty');
			updateShipQty(ffOrdNo,ffOrdLineNo,shipqty);	

			MoveTaskRecord(otsearchresults[s].getId());
		}
	}

	nlapiLogExecution('Debug', 'Out of updatefulfillmentorderstatus');
}

function getpickcartons(vitemfulfillrefno)
{
	nlapiLogExecution('DEBUG', 'Into getpickcartons');	
	nlapiLogExecution('DEBUG', 'vitemfulfillrefno', vitemfulfillrefno);	

	var opentasksearchresults = new Array();

	if(vitemfulfillrefno!=null && vitemfulfillrefno!='')
	{
		var filters = new Array();

		filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null,'is',vitemfulfillrefno));
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['28','7','14']));

		var columns= new Array();

		columns[0] = new nlobjSearchColumn('custrecord_container_lp_no',null,'group');

		opentasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters ,columns);		
	}

	nlapiLogExecution('DEBUG', 'Out of getpickcartons');

	return opentasksearchresults;
} 

function CheckAlltheCartonsShipped(fulfillmentOrder,vebizcontainerlp,Waveno,vsalesorderid,vpickcartonsarr)
{
	var str = 'fulfillmentOrder = ' + fulfillmentOrder + '<br>';
	str = str + 'vebizcontainerlp = ' + vebizcontainerlp+ '<br>';
	str = str + 'Waveno = ' + Waveno+ '<br>';
	str = str + 'vsalesorderid = ' + vsalesorderid+ '<br>';
	str = str + 'vpickcartonsarr = ' + vpickcartonsarr+ '<br>';

	nlapiLogExecution('Debug', 'Into CheckAlltheCartonsShipped', str);

	var searchshipmanifestshippedrecords = new Array();

	if(vpickcartonsarr!=null && vpickcartonsarr!='')
	{
		for ( var s = 0 ; s < vpickcartonsarr.length; s++ )
		{
			var vcontainerlp = vpickcartonsarr[s].getValue('custrecord_container_lp_no',null,'group');

			nlapiLogExecution('Debug', 'vcontainerlp', vcontainerlp);

			var shipmentfilter = new Array();
			if(vsalesorderid!=null && vsalesorderid!='')
				shipmentfilter.push(new nlobjSearchFilter('custrecord_ship_order', null, 'is', vsalesorderid));
			shipmentfilter.push(new nlobjSearchFilter('custrecord_ship_trackno', null, 'isempty'));
			shipmentfilter.push(new nlobjSearchFilter('custrecord_ship_ref5', null, 'is',vcontainerlp));
			var shipmentcolumns = new Array();
			shipmentcolumns[0] = new nlobjSearchColumn('custrecord_ship_ref5');
			searchshipmanifestshippedrecords= nlapiSearchRecord('customrecord_ship_manifest', null, shipmentfilter, shipmentcolumns);
			nlapiLogExecution('Debug', 'searchshipmanifestshippedrecords', searchshipmanifestshippedrecords);
			if(searchshipmanifestshippedrecords!=null && searchshipmanifestshippedrecords!='')
				return searchshipmanifestshippedrecords;
		}
	}
	nlapiLogExecution('Debug', 'Out of CheckAlltheCartonsShipped');
	return searchshipmanifestshippedrecords;

}