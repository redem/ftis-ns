/***************************************************************************
 eBizNET Solutions 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_Cart_PutawayConfirmation.js,v $
 *     	   $Revision: 1.4.2.8.4.6.2.14 $
 *     	   $Date: 2014/07/21 16:02:10 $
 *     	   $Author: sponnaganti $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Cart_PutawayConfirmation.js,v $
 * Revision 1.4.2.8.4.6.2.14  2014/07/21 16:02:10  sponnaganti
 * Case# 20148231
 * Stnd Bundle Issue fix
 *
 * Revision 1.4.2.8.4.6.2.13  2014/06/19 08:13:36  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148942
 *
 * Revision 1.4.2.8.4.6.2.12  2014/06/13 07:46:51  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.4.2.8.4.6.2.11  2014/06/06 06:24:33  skavuri
 * Case# 20148749 (Refresh Functionality ) SB Issue Fixed
 *
 * Revision 1.4.2.8.4.6.2.10  2014/05/30 00:26:48  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.4.2.8.4.6.2.9  2013/12/02 08:59:59  schepuri
 * 20126048
 *
 * Revision 1.4.2.8.4.6.2.8  2013/11/18 10:22:49  grao
 * Issue  getting LPswith locations assigned from open task 20125782 fixes
 *
 * Revision 1.4.2.8.4.6.2.7  2013/11/18 10:21:58  grao
 * Issue  getting LPswith locations assigned from open task 20125782 fixes
 *
 * Revision 1.4.2.8.4.6.2.6  2013/06/25 14:52:04  mbpragada
 * Case# 20123178
 * Cart put away: System is accepting other sites bin location
 * while doing location exception
 *
 * Revision 1.4.2.8.4.6.2.5  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.4.2.8.4.6.2.4  2013/05/10 00:33:08  kavitha
 * CASE201112/CR201113/LOG201121
 * Issue fixes
 *
 * Revision 1.4.2.8.4.6.2.3  2013/04/17 16:04:02  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.4.2.8.4.6.2.2  2013/04/03 15:32:02  kavitha
 * CASE201112/CR201113/LOG2012392
 * GFT changes - Added Item Description
 *
 * Revision 1.4.2.8.4.6.2.1  2013/03/08 14:35:02  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.4.2.8.4.6  2013/02/07 15:10:34  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.4.2.8.4.5  2012/11/11 03:40:30  snimmakayala
 * CASE201112/CR201113/LOG201121
 * GSUSA UAT issue fixes.
 *
 * Revision 1.4.2.8.4.4  2012/11/01 14:55:35  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.4.2.8.4.3  2012/09/27 10:54:36  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.4.2.8.4.2  2012/09/26 12:45:35  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.4.2.8.4.1  2012/09/21 14:57:16  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.4.2.8  2012/04/27 15:05:56  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue realted to  exception qty against the Lp# resolved.
 *
 * Revision 1.4.2.7  2012/04/19 15:09:18  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to sequential execution process is resolved.
 *
 * Revision 1.4.2.6  2012/04/17 10:38:39  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Cart To Checkin
 *
 * Revision 1.4.2.5  2012/03/19 10:08:29  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.4.2.4  2012/02/24 17:45:52  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * putaway confirmation
 *
 * Revision 1.4.2.3  2012/02/23 13:48:08  schepuri
 * CASE201112/CR201113/LOG201121
 * fixed TPP issues
 *
 * Revision 1.5  2012/02/23 13:36:20  gkalla
 * CASE201112/CR201113/LOG201121
 * For cart checkin LP issue
 *
 * Revision 1.4  2012/02/16 10:34:33  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 * Revision 1.3  2012/02/09 14:32:22  gkalla
 * CASE201112/CR201113/LOG201121
 * issue fixed for cart LP scanning for TPP
 *
 * Revision 1.5  2011/12/28 06:57:00  spendyala
 * CASE201112/CR201113/LOG201121
 * made changes for qty exception
 *
 * Revision 1.4  2011/08/24 12:42:30  schepuri
 * CASE201112/CR201113/LOG201121
 * RF PutAway confirmation based on putseq no
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/
//to sort an array
 function arr(seq, lp) {
	 nlapiLogExecution('DEBUG', 'arr seq', seq);
	 nlapiLogExecution('DEBUG', 'arr lp', lp);
       	this.Seq= parseFloat(seq);
       	this.Lp = lp;
       	 }
//to sort an array
  function sortBySeq(a, b) {
       	var x = a.Seq;
       	var y = b.Seq;
       	return ((x < y) ? -1 : ((x > y) ? 1 : 0));
       }
       
 function PutawayConfirmation(request, response){
	 if (request.getMethod() == 'GET') {
		 //	Get the LP#, Quantity, Location 
		 //  from the previous screen, which is passed as a parameter
//		 var TempLPNoArray = request.getParameter('custparam_lpNumbersArray');
//		 nlapiLogExecution('DEBUG', 'lpNumbersFromArray', TempLPNoArray);
		 var getCartNo = request.getParameter('custparam_cartno');
		 var fetchLPNofrmqtyException=request.getParameter('custparam_hdnlpno_qtyexception');
			 var confirmedLPCount = request.getParameter('custparam_confirmedLpCount');
		 nlapiLogExecution('DEBUG', 'before starting confirmedLPCount', confirmedLPCount);

		 if (confirmedLPCount == null){
			 confirmedLPCount = 0;
		 }

		 nlapiLogExecution('DEBUG', 'starting confirmedLPCount', confirmedLPCount);
		nlapiLogExecution('DEBUG', 'getCartNo', getCartNo);
		 var filters = new Array(); 
		 var newarray = new Array();
//		 var TempLPArray = TempLPNoArray .split(',');
		  var TempLPNoArray; var lpCount=0;
//		 for(i=0;i<TempLPArray.length;i++)
		 if(getCartNo!=null && getCartNo!='')
	 {                       
			// filters[0] = new nlobjSearchFilter('custrecord_lpno', null, 'is',TempLPArray[i]);
			 filters[0] = new nlobjSearchFilter('custrecord_transport_lp', null, 'is',getCartNo);
			 filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'is','2'); // PUTW task 
			 filters[2] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');
			 
			 //case Start 20125782
			 filters[3] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
			 //end
			 if(fetchLPNofrmqtyException!=""&&fetchLPNofrmqtyException!=null)
			 filters[4] = new nlobjSearchFilter('custrecord_lpno', null, 'is', fetchLPNofrmqtyException);
			 
			 var columns = new Array(); 
			 columns[0] = new nlobjSearchColumn('custrecord_startingputseqno','custrecord_actbeginloc'); 
			 columns[1] = new nlobjSearchColumn('custrecord_lpno'); 
			  columns[0].setSort();
			 var searchresults1 = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null, filters, columns);
			 var vLP='';
			 if(searchresults1!=null)
			 {
				 lpCount = searchresults1.length;
				 for(var j=0;j<searchresults1.length;j++)
				 {
					 if(j!=searchresults1.length-1)
						 {
						 TempLPNoArray=searchresults1[j].getValue('custrecord_lpno')+",";
						 }
					 else
						 {
						 TempLPNoArray=searchresults1[j].getValue('custrecord_lpno');
						 }
					 vLP=searchresults1[j].getValue('custrecord_lpno');
					 newarray.push(new  arr(searchresults1[j].getValue('custrecord_startingputseqno','custrecord_actbeginloc'),searchresults1[j].getValue('custrecord_lpno')));
				 }
			 }
			
			 //var f = newarray[i];
			 //nlapiLogExecution('DEBUG', 'newarray.sort(sortBySeq)   f.seq', f.Seq);
			 //nlapiLogExecution('DEBUG', 'newarray.sort(sortBySeq)   f.lp', f.Lp);
		 }

		nlapiLogExecution('DEBUG', 'newarray', newarray);


		var TArray = new Array();
		//TArray  = newarray.sort(sortBySeq);
		//nlapiLogExecution('DEBUG', 'TArray', TArray);
		 

		var getLPNo=vLP;
		//for(confirmedLPCount=0; confirmedLPCount<lpCount; confirmedLPCount++)
		nlapiLogExecution('DEBUG', 'getLPNo', getLPNo);
		nlapiLogExecution('DEBUG', 'confirmedLPCount', confirmedLPCount);
		nlapiLogExecution('DEBUG', 'lpCount', lpCount);
		/*if(confirmedLPCount<lpCount)
		{
			nlapiLogExecution('DEBUG', 'inif confirmedLPCount as index', confirmedLPCount);
			//var b = TArray[confirmedLPCount];
			var b = newarray[0];
			if(b!=null && b!='')
			{
				var sequnceNo = b.Seq;
				var sortedLP = b.Lp;
				nlapiLogExecution('DEBUG', 'newarray.sort(sortBySeq)   b.seq', b.Seq);
				nlapiLogExecution('DEBUG', 'newarray.sort(sortBySeq)   b.lp', b.Lp);
				nlapiLogExecution('DEBUG', 'newarray.sort(sortBySeq)   sequnceNo', sequnceNo);
				nlapiLogExecution('DEBUG', 'newarray.sort(sortBySeq)   sortedLP', sortedLP);



				getLPNo = sortedLP;
			}
			//break;
		}*/
		if(newarray != null && newarray != "")
			getLPNo=newarray[0].Lp;
		nlapiLogExecution('DEBUG', 'getLPNo', getLPNo);
		//var getLPNo = request.getParameter('custparam_lpno');
		var getCartLPNo = request.getParameter('custparam_cartno');
		var getRecordCount=0;
		 if (getCartLPNo != null && getCartNo!='' && getLPNo != null) {// case# 201417032
		var filters = new Array(); 
		filters[0] = new nlobjSearchFilter('custrecord_transport_lp', null, 'is',getCartLPNo);
		filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'is','2'); // PUTW task 
		filters[2] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');
		filters[3] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2, 6]);

		var searchresults1 = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null, filters, null);
		if(searchresults1!=null)
		{
			getRecordCount = searchresults1.length;
		}

		 nlapiLogExecution('DEBUG', 'getLPNo', getLPNo);
		 nlapiLogExecution('DEBUG', 'getCartLPNo', getCartLPNo);

		 var getOptedField = request.getParameter('custparam_option');
		 nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);
		// if (getCartLPNo != null && getLPNo != null) {
		 var filters = new Array();
		 //        filters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
		 //        filters[1] = new nlobjSearchFilter('custrecord_transport_lp', null, 'is', getCartLPNo);
		 //		filters[2] = new nlobjSearchFilter('custrecord_lpno', null, 'is', getLPNo);

//		 if (getOptedField == "4") {
			 filters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
			 filters[1] = new nlobjSearchFilter('custrecord_transport_lp', null, 'is', getCartLPNo);
			 filters[2] = new nlobjSearchFilter('custrecord_lpno', null, 'is', getLPNo);
//		 }
//		 else {
//			 filters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
//			 filters[1] = new nlobjSearchFilter('custrecord_lpno', null, 'is', getLPNo);
//		 }

		 var columns = new Array();
		 columns[0] = new nlobjSearchColumn('custrecord_transport_lp');
		 columns[1] = new nlobjSearchColumn('custrecord_lpno');
		 columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
		 columns[3] = new nlobjSearchColumn('custrecord_actbeginloc');
		 columns[4] = new nlobjSearchColumn('custrecord_sku');
		 columns[5] = new nlobjSearchColumn('custrecord_skudesc');
		 columns[6] = new nlobjSearchColumn('custrecord_wms_location');

		 var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

		
			 if (searchresults != null && searchresults.length > 0) {
				 nlapiLogExecution('DEBUG', 'SearchResults of given LP', 'LP is found');

				 var getLPNo = searchresults[0].getValue('custrecord_lpno');

				 var getQuantity = searchresults[0].getValue('custrecord_expe_qty');
				 var getFetchedItem = searchresults[0].getValue('custrecord_sku');
				 var getFetchedItemText = searchresults[0].getText('custrecord_sku');
				 var getFetchedItemDescription = searchresults[0].getValue('custrecord_skudesc');
				 var getFetchedLocationId = searchresults[0].getValue('custrecord_actbeginloc');
				 var getFetchedLocation = searchresults[0].getText('custrecord_actbeginloc');
				 nlapiLogExecution('DEBUG', 'SearchResults of given LP getFetchedLocation', getFetchedLocation);
				 var getWHLocation = searchresults[0].getValue('custrecord_wms_location');

				 nlapiLogExecution('DEBUG', 'SearchResults of given LP getWHLocation', getWHLocation);
				 //getRecordCount = request.getParameter('custparam_recordcount');
				 nlapiLogExecution('DEBUG', 'SearchResults of given LP getRecordCount', getRecordCount);
			 }
		 }
//		 if((getFetchedLocationId!=null && getFetchedLocationId!='')||(getFetchedLocation!=null && getFetchedLocation!=''))
//		 {
//			 if((request.getParameter('custparam_beginlocationinternalid')!=null && request.getParameter('custparam_beginlocationinternalid')!='')||(request.getParameter('custparam_location')!=null && request.getParameter('custparam_location')!=''))
//			 {
//
//				 if((request.getParameter('custparam_beginlocationinternalid')!=getFetchedLocationId )||(request.getParameter('custparam_location')!=getFetchedLocation ))
//				 {
//					 getFetchedLocationId=request.getParameter('custparam_beginlocationinternalid');
//					 getFetchedLocation=request.getParameter('custparam_beginlocation');
//				 }
//			 }
//		 }
		 var qtyexceptionFlag=request.getParameter('custparam_qtyexceptionflag');
		 nlapiLogExecution('DEBUG', 'qtyexceptionFlag',qtyexceptionFlag);
		 //case# 20148231 starts
		 var vremqty='';
		 if(qtyexceptionFlag=='true')
		 {
			 getQuantity=request.getParameter('custparam_exceptionqty');
			 vremqty=request.getParameter('custparam_remainingqty');
			 nlapiLogExecution('DEBUG', 'getQuantity',getQuantity);
			 nlapiLogExecution('DEBUG', 'vremqty',vremqty);
		 }//case# 20148231 ends
		 else
			 qtyexceptionFlag='false';
		 
		 var itemPalletQuantity = request.getParameter('custparam_itempalletquantity');

		 nlapiLogExecution('DEBUG', 'getLPNo', getLPNo);
		 
		 
		 var getLanguage = request.getParameter('custparam_language');
		    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
		    
			var st0,st1,st2,st3,st4,st5,st6,st7,st8,st9;
			// 20126048
			if( getLanguage == 'es_ES' || getLanguage =='es_AR')
			{
				st0 = "";
				st1 = "PLACA";
				st2 = "ART&#205;CULO";
				st3 = "CONFIRMAR LA CANTIDAD";
				st4 = "CONFIRMAR UBICACI&#211;N";
				st5 = "EXCEPCI&#211;N DE UBICACI&#211;N";
				st6 = "EXCEPCI&#211;N DE CANTIDAD";
				st7 = "CONF";
				st8 = "ANTERIOR";
				st9 = "DESCRIPCI&#211;N";
				
			}
			else
			{
				st0 = "";
				st1 = "LP";
				st2 = "ITEM";
				st3 = "CONFIRM QUANTITY";
				st4 = "CONFIRM LOCATION";
				st5 = "LOC EXCEPTION";
				st6 = "QTY EXCEPTION";
				st7 = "CONF";
				st8 = "PREV";
				st9 = "DESCRIPTION";
			}
		 

		 var functionkeyHtml=getFunctionkeyScript('_rf_putaway_lp'); 
		 var html = "<html><head><title>" + st0 + "</title>";
		 html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		 html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
			html = html + "var version = navigator.appVersion;";
			html = html + "document.onkeydown = function (e) {";
			html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
			html = html + "if ((version.indexOf('MSIE') != -1)) { ";
			html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
			html = html + "else {if (keycode == 116)return false;}";
			html = html + "};";
			//Case# 20148749 Refresh Functionality ends
		 html = html + "nextPage = new String(history.forward());";          
			html = html + "if (nextPage == 'undefined')";     
			html = html + "{}";     
			html = html + "else";     
			html = html + "{  location.href = window.history.forward();"; 
			html = html + "} ";
		 //html = html + " document.getElementById('cmdConfirm').focus();";        
		 html = html + "function stopRKey(evt) { ";
			//html = html + "	  alert('evt');";
			html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
			html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
			html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
			html = html + "	  if(document.getElementById('cmdConfirm').disabled==true){";
			html = html + "	  alert('System Processing, Please wait...');";
			html = html + "	  return false;}} ";
			html = html + "	} ";

			html = html + "	document.onkeypress = stopRKey; ";
		 html = html + "</script>";
		 html = html +functionkeyHtml;
		 html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		 html = html + "	<form name='_rf_putaway_lp' method='POST'>";
		 html = html + "		<table>";
		 html = html + "			<tr>";
		 html = html + "				<td align = 'left'>" + st1 + " : <label>" + getLPNo + "</label>";
		 html = html + "				<input type='hidden' name='hdnLPNo' value=" + getLPNo + "></td>";
		 html = html + "				<input type='hidden' name='hdnQuantity' value=" + getQuantity + "></td>";
		 html = html + "				<input type='hidden' name='hdnFetchedLocation' value=" + getFetchedLocation + "></td>";
		 html = html + "				<input type='hidden' name='hdnFetchedItem' value=" + getFetchedItem + "></td>";
		 html = html + "				<input type='hidden' name='hdnFetchedItemDescription' value=" + getFetchedItemDescription + "></td>";
		 html = html + "				<input type='hidden' name='hdnFetchedLocationId' value=" + getFetchedLocationId + "></td>";
		 html = html + "				<input type='hidden' name='hdnRecordCount' value=" + getRecordCount + "></td>";
		 html = html + "				<input type='hidden' name='hdnConfirmedLPCount' value=" + confirmedLPCount + "></td>";
		 html = html + "				<input type='hidden' name='hdnlpCount' value=" + lpCount + "></td>";
		 html = html + "				<input type='hidden' name='hdnlpNumbersArray' value=" + TempLPNoArray + "></td>";
		 html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		 html = html + "				<input type='hidden' name='hdnCartLPNo' value='" + getCartLPNo + "'>";
		 html = html + "				<input type='hidden' name='hdnWhLocation' value=" + getWHLocation + ">";
		 html = html + "				<input type='hidden' name='hdnItemPalletQuantity' value=" + itemPalletQuantity + ">";
		 html = html + "				<input type='hidden' name='hdnexceptionQuantityflag' value=" + qtyexceptionFlag + ">";
		 html = html + "				<input type='hidden' name='hdnRemQuantity' value=" + vremqty + "></td>";//case# 20148231
		 html = html + "	            <input type='hidden' name='hdnflag'>";
		 html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		 html = html + "				</td>";
		 html = html + "			</tr>";
		 html = html + "			<tr>";
		 html = html + "				<td align = 'left'>" + st2 + ": <label>" + getFetchedItemText + "</label>";
		 html = html + "				</td>";
		 html = html + "			</tr>";
		 html = html + "			<tr>";
		 html = html + "				<td align = 'left'>" + st9 + ": <label>" + getFetchedItemDescription + "</label>";
		 html = html + "				</td>";
		 html = html + "			</tr>";
		 html = html + "			<tr>";
		 html = html + "				<td align = 'left'>" + st3 + ": <label>" + getQuantity + "</label>";
		 html = html + "				</td>";
		 html = html + "			</tr>";
		 html = html + "			<tr>";
		 html = html + "				<td align = 'left'>" + st4 + ": </td></tr>";
		 html = html + "				<tr><td align = 'left'><label>" + getFetchedLocation + "</label></td>";
		 html = html + "			</tr>";
		 html = html + "			<tr>";
		 html = html + "				<td align = 'left'>" + st5 + " <input name='cmdLocException' type='submit' value='F10'/></td></tr>";
		 html = html + "				 <tr><td align = 'left'>" + st6 +" <input name='cmdQtyException' type='submit' value='F2'/>";
		 html = html + "				</td>";
		 html = html + "			</tr>";
		 html = html + "			<tr>";
		 html = html + "				<td align = 'left'>" + st7 + " <input name='cmdConfirm' id='cmdConfirm' type='submit' value='F8' onclick='this.form.hdnflag.value=this.value;this.form.submit();this.disabled=true;this.form.cmdLocException.disabled=true;this.form.cmdQtyException.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		 html = html + "				" + st8 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		 html = html + "				</td>";
		 html = html + "			</tr>";
		 html = html + "		 </table>";
		 html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		 html = html + "<script type='text/javascript'>document.getElementById('cmdConfirm').focus();</script>";
		 html = html + "</body>";
		 html = html + "</html>";

		 response.write(html);


	 }
    else {
        nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is not null');
        
        // This variable is to hold the LP entered.
        var getLPNo = request.getParameter('hdnLPNo');	//request.getParameter('custparam_lpno');
        nlapiLogExecution('DEBUG', 'getLPNo1', getLPNo);
        var getWHLocation = request.getParameter('hdnWhLocation');
        nlapiLogExecution('DEBUG', 'getWHLocation', getWHLocation);
        
        var getFetchedLocation = request.getParameter('hdnFetchedLocation');
        var getFetchedItem = request.getParameter('hdnFetchedItem');
        var getFetchedItemDescription = request.getParameter('hdnFetchedItemDescription');
        var getFetchedLocationId = request.getParameter('hdnFetchedLocationId');
        
        nlapiLogExecution('DEBUG', 'getFetchedLocation', getFetchedLocation);
       // nlapiLogExecution('DEBUG', 'getQuantity', getQuantity);
        nlapiLogExecution('DEBUG', 'getFetchedItem', getFetchedItem);
        nlapiLogExecution('DEBUG', 'getFetchedItemDescription', getFetchedItemDescription);
        
        var getOptedField = request.getParameter('hdnOptedField');
        nlapiLogExecution('DEBUG', 'hdnOptedField', getOptedField);
        
        var getCartLPNo = request.getParameter('hdnCartLPNo');
	nlapiLogExecution('DEBUG', 'getCartLPNo', getCartLPNo);
        var filters = new Array();
        
//        if (getOptedField == "4") 
//        {
            filters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
            filters[1] = new nlobjSearchFilter('custrecord_transport_lp', null, 'is', getCartLPNo);
            filters[2] = new nlobjSearchFilter('custrecord_lpno', null, 'is', getLPNo);
//        }
//        else
//        {
//            filters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
//            filters[1] = new nlobjSearchFilter('custrecord_lpno', null, 'is', getLPNo);
//        }
        
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('custrecord_transport_lp');
        columns[1] = new nlobjSearchColumn('custrecord_lpno');
        columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
        columns[3] = new nlobjSearchColumn('custrecord_actbeginloc');
        columns[4] = new nlobjSearchColumn('custrecord_sku');
        columns[5] = new nlobjSearchColumn('custrecord_skudesc');
        columns[6] = new nlobjSearchColumn('custrecord_line_no');
        columns[7] = new nlobjSearchColumn('name');
        
        // Added by Phani on 03-25-2011
        columns[8] = new nlobjSearchColumn('custrecord_wms_location');
        
        var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
        
        var POarray = new Array();
        
        var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);
        
        
        var getQuantity = request.getParameter('hdnQuantity');
        nlapiLogExecution('DEBUG', 'getQuantity', getQuantity);
        POarray["custparam_exceptionquantity"] = getQuantity;
        POarray["custparam_whlocation"] = getWHLocation;
        POarray["custparam_exceptionQuantityflag"]=request.getParameter('hdnexceptionQuantityflag');
    	var getRemQuantity = request.getParameter('hdnRemQuantity');
    	 nlapiLogExecution('DEBUG', 'getRemQuantity', getRemQuantity);
    	POarray["custparam_remainingqty"] = getRemQuantity;
    	POarray["custparam_qtyexceptionflag"]=request.getParameter('hdnexceptionQuantityflag');
        var getOptedField = request.getParameter('hdnOptedField');
        POarray["custparam_option"] = getOptedField;
        
        POarray["custparam_screenno"] = 'CRT12';
        
        var getRecordCount = request.getParameter('hdnRecordCount');
        nlapiLogExecution('DEBUG', 'getRecordCount else', getRecordCount);
        
        POarray["custparam_cartno"] = request.getParameter('hdnCartLPNo');
        POarray["custparam_itempalletquantity"] = request.getParameter('hdnItemPalletQuantity');

        if (searchresults != null && searchresults.length > 0) {
            nlapiLogExecution('DEBUG', 'SearchResults of given LP', 'LP is found');
            
            var getLPId = searchresults[0].getId();
            nlapiLogExecution('DEBUG', 'get LP Internal Id', getLPId);
            
            POarray["custparam_lpno"] = searchresults[0].getValue('custrecord_lpno');
            POarray["custparam_quantity"] = searchresults[0].getValue('custrecord_expe_qty');
            POarray["custparam_item"] = searchresults[0].getValue('custrecord_sku');
            POarray["custparam_itemtext"] = searchresults[0].getText('custrecord_sku');
            POarray["custparam_itemDescription"] = searchresults[0].getValue('custrecord_skudesc');
            POarray["custparam_location"] = searchresults[0].getValue('custrecord_actbeginloc');
            POarray["custparam_beginlocation"] = searchresults[0].getText('custrecord_actbeginloc');
            POarray["custparam_recordcount"] = getRecordCount;
            POarray["custparam_recordid"] = searchresults[0].getId();
            POarray["custparam_lineno"] = searchresults[0].getValue('custrecord_line_no');
            POarray["custparam_pono"] = searchresults[0].getValue('name');
            
            POarray["custparam_lpNumbersArray"] = request.getParameter('hdnlpNumbersArray');
            POarray["custparam_confirmedLpCount"] = parseFloat(request.getParameter('hdnConfirmedLPCount')) + 1;
            POarray["custparam_lpCount"] = parseFloat(request.getParameter('hdnlpCount'));
        }
        //	if the previous button 'F7' is clicked, it has to go to the previous screen 
        //  ie., it has to go to accept PO #.
        if (request.getParameter('cmdPrevious') == 'F7') {
            nlapiLogExecution('DEBUG', 'Clicked on Previous', request.getParameter('cmdPrevious'));
//            response.sendRedirect('SUITELET', 'customscript_rf_putaway_lp', 'customdeploy_rf_putaway_lp_di', false, POarray);
            response.sendRedirect('SUITELET', 'customscript_rf_putaway_cartlp', 'customdeploy_rf_putaway_cartlp_di', false, POarray);
        }
        else 
            if (request.getParameter('hdnflag') == 'F8') {
                nlapiLogExecution('DEBUG', 'Clicked on Confirm', request.getParameter('cmdConfirm'));
                //response.sendRedirect('SUITELET', 'customscript_rf_putaway_location', 'customdeploy_rf_putaway_location_di', false, POarray);
                response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawayloc', 'customdeploy_ebiz_rf_cart_putawayloc_di', false, POarray);
            }
            else 
                if (request.getParameter('cmdLocException') == 'F10') {
                    nlapiLogExecution('DEBUG', 'Clicked on Location Exception', request.getParameter('cmdLocException'));
                    response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawaylocexcp', 'customdeploy_ebiz_rf_cart_putawaylocexcp', false, POarray);
                }
                else 
                    if (request.getParameter('cmdQtyException') == 'F2') {
                        nlapiLogExecution('DEBUG', 'Clicked on Quantity Exception', request.getParameter('cmdQtyException'));
                        response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawayqtyexp', 'customdeploy_ebiz_rf_cart_putawayqtyexp', false, POarray);
                    }
        nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
    }
}
