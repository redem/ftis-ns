function QuantityDiscrepancy(request, response){
	if (request.getMethod() == 'GET') {
		var getnextlocation = request.getParameter('custparam_nextlocation');
		var getactpackcode = request.getParameter('custparam_actpackcode');
		var getexpitemdescription = request.getParameter('custparam_expitemdescription');
		var getlpno = request.getParameter('custparam_lpno');
		var getnoofrecords = request.getParameter('custparam_noofrecords');
		    var geterror = request.getParameter('custparam_error');
		var getitemtype = request.getParameter('custparam_itemtype');
		var getconfmsg = request.getParameter('custparam_confmsg');
		var getexpqty = request.getParameter('custparam_expqty');
		var getexpiteminternalid = request.getParameter('custparam_expiteminternalid');
		var getbeginlocationname = request.getParameter('custparam_begin_location_name');
		var getpackcode = request.getParameter('custparam_packcode');
		var getscreenno = request.getParameter('custparam_screenno');
		var getexpitem = request.getParameter('custparam_expitem');
		var getactualqty = request.getParameter('custparam_actualqty');
		var getactitem = request.getParameter('custparam_actitem');
		var getplanno = request.getParameter('custparam_planno');
		var getactbatch = request.getParameter('custparam_actbatch');
		var getrecordid = request.getParameter('custparam_recordid');
		var getactlp = request.getParameter('custparam_actlp');
		var getbeginlocationid = request.getParameter('custparam_begin_location_id');
		var getskustatus = request.getParameter('custparam_skustatus');
		var planitem=request.getParameter('custparam_planitem');
		//sri Case# 20127045 starts
		var getRuleValue = request.getParameter('custparam_Rulevalue');
		var getBlindRuleValue = request.getParameter('custparam_blindrulevalue');
		nlapiLogExecution('ERROR', 'getRuleValue is', getRuleValue);
		nlapiLogExecution('ERROR', 'getBlindRuleValue', getBlindRuleValue);
		//sri Case# 20127045 end
		//Case # 20125186  start
		if(getrecordid!=null && getrecordid!='')
		{
			var record=nlapiLoadRecord('customrecord_ebiznet_cyclecountexe',getrecordid);
			nlapiLogExecution('ERROR', 'record', record);
			if(record!=null && record!='')
			{
				var notes=record.getFieldValue('custrecord_cycle_notes');
				//case# 20127238 starts (getting item name from cycle count for displaying at Item)
				var itemname=record.getFieldText('custrecord_cycleact_sku');
				var expitemname=record.getFieldText('custrecord_cyclesku');//Case# 20149385
				nlapiLogExecution('ERROR', 'itemname', itemname);
				nlapiLogExecution('ERROR', 'notes', notes);
				if(notes=='fromaddnewitem')
				{
					getexpitem=	itemname;
				}
				//Case# 20149385 starts
				else
					{
					//if bind rule for Item is 'Y' we need to display scanned item
					if(getexpitem==expitemname)
					{
					getexpitem=expitemname;
					}
					//getexpitem=expitemname;
					}
				//	Case# 20149385 ends
				//case# 20127238 end
			}
		}
		//Case # 20125186 End.
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
		
		var functionkeyHtml=getFunctionkeyScript('_rf_cyclecountquantity');
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
//Case # 20124594 : start : spanish language conversion
		
		var st0,st1,st2,st3,st4,st5,st6,st7,st8,st9;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st0 = "ALERTA";
			st1 = "Bin Ubicaci&#243;n";
			st2 = "art&#237;culo";
			st3 = " Cantidad Contado";
			st4 = " Cantidad esperada ";
            st5 = " Por favor revise";
            st6 = " CONT";
            st7 = "ANTERIOR";
          

		}
		else
		{
			st0 = "Alert";
			st1 = "Binlocation";
			st2 = "Item";
			st3 = "Qty Counted";
			st4 = "Expected Qty";
			st5 = "Please Double Check";
            st6 = "CONT";
            st7 = "PREV";
           
		}
		
		
//case # 20124594 end
		var html = "<html><head><title>CYCLE COUNT QTY Discrepancy</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		html = html + " document.getElementById('enterqty').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head>";
		html = html + "<body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cyclecountquantity' method='POST'>";
		html = html + "		<table>"; 
		html = html + "				<tr><td>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdnplanitem' value='" + planitem + "'>";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st0+" </td></tr>";
		
		html = html + "				<tr><td align = 'left'>" + st1  + " :<label>" + getbeginlocationname+ "</label></td></tr>";
		html = html + "				<tr><td align = 'left'>" + st2  + " :<label>" + getexpitem+ "</label></td></tr>";
		html = html + "				<tr><td align = 'left'></td></tr>";
		html = html + "				<tr><td align = 'left'>" + st3  + " :<label>" + getactualqty+ "</label> is not equal to the</td></tr>";
		
		//case# 20127045,201413294 starts
		if(getBlindRuleValue=='N')
		html = html + "				<tr><td align = 'left'>" +st4  + " :<label>" + getexpqty+ " </label></td></tr>";
		else
			
		html = html + "				<tr><td align = 'left'>" +st4  + " :<label>"+''+" </label></td></tr>";
		//case# 20127045 end

		
		//html = html + "				<tr><td align = 'left'>" +st4  + " :<label>" + getexpqty+ " </label></td></tr>";//Case# 201411113
		
		html = html + "				<tr><td align = 'left'></td></tr>";
		html = html + "				<tr><td align = 'left'>" + st5  + "</td></tr>";

		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st6 + " <input name='cmdCont' type='submit' value='F8' style='width:40;height:40;'/>";
		html = html + "					" + st7 + " <input name='cmdprevious' type='submit' value='F10' style='width:40;height:40;'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		
		response.write(html);
		
	}
	else{
		
		var optedEvent = request.getParameter('cmdCont');
		var optedEvent1 = request.getParameter('cmdprevious');
		
		var CCarray= new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		CCarray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR','getLanguage',getLanguage);
		CCarray["custparam_nextlocation"] = request.getParameter('custparam_nextlocation');
		CCarray["custparam_actpackcode"] = request.getParameter('custparam_actpackcode');
		CCarray["custparam_expitemdescription"] = request.getParameter('custparam_expitemdescription');
		CCarray["custparam_lpno"] = request.getParameter('custparam_lpno');
		CCarray["custparam_noofrecords"] = request.getParameter('custparam_noofrecords');
		CCarray["custparam_error"] = request.getParameter('custparam_error');
		CCarray["custparam_itemtype"] = request.getParameter('custparam_itemtype');
		CCarray["custparam_confmsg"] = request.getParameter('custparam_confmsg');
		CCarray["custparam_expqty"] = request.getParameter('custparam_expqty');
		CCarray["custparam_expiteminternalid"] = request.getParameter('custparam_expiteminternalid');
		CCarray["custparam_begin_location_name"] = request.getParameter('custparam_begin_location_name');
		CCarray["custparam_packcode"] = request.getParameter('custparam_packcode');
		CCarray["custparam_screenno"] = request.getParameter('custparam_screenno');
		CCarray["custparam_expitem"] = request.getParameter('custparam_expitem');
		CCarray["custparam_actualqty"] = request.getParameter('custparam_actualqty');
		CCarray["custparam_actitem"] = request.getParameter('custparam_actitem');
		CCarray["custparam_planno"] = request.getParameter('custparam_planno');
		CCarray["custparam_actbatch"] = request.getParameter('custparam_actbatch');
		CCarray["custparam_recordid"] = request.getParameter('custparam_recordid');
		CCarray["custparam_actlp"] = request.getParameter('custparam_actlp');
		CCarray["custparam_begin_location_id"] = request.getParameter('custparam_begin_location_id');
		CCarray["custparam_skustatus"] = request.getParameter('custparam_skustatus');
		CCarray["custparam_planitem"] = request.getParameter('hdnplanitem');
		
		if (optedEvent == 'F8'){
			
			response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_itemstatus', 'customdeploy_rf_cyclecount_itemstatus_di', false, CCarray);	
			
		}
		else{
			response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_quantity', 'customdeploy_rf_cyclecount_quantity_di', false, CCarray);
		}
		
		
		
		
		
		
		
	}
}
