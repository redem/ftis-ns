/***************************************************************************
			 eBizNET
    eBizNET SOLUTIONS LTD
 ****************************************************************************
 *
 * $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_EDI_Config_SL.js,v $
 * $Revision: 1.1.4.3 $
 * $Date: 2015/11/25 08:52:05 $
 * $Author: mpragada $
 * $Name: t_WMS_2015_2_StdBundle_1_190 $
 *
 * DESCRIPTION
 * Functionality
 *
 * REVISION HISTORY
 * $Log: ebiz_EDI_Config_SL.js,v $
 * Revision 1.1.4.3  2015/11/25 08:52:05  mpragada
 * case# 201414406
 * EDI Configurable screen changes
 *
 * Revision 1.1.2.3.4.3.4.12  2015/11/10 08:53:45  mpragada
 * case# 201415369
 *
 * Revision 1.1.2.3.4.3.4.11  2015/06/09 11:04:41  mpragada
 * Case# 201412758
 * EDI Issue Fixes
 *

 *
 ****************************************************************************/
/**
 * Function will Get all the customer details. This list will be used to get the ASN Required? information.
 * @param skulist
 * @returns {Array}
 */
//Case Start 201414406 
function IsEDICustRequired()
{

	try{
		var filters = new Array();

		filters.push(new nlobjSearchFilter('custrecord_ebizruletype', null, 'is', 'EDI_CUST_REQ'));		
		filters.push(new nlobjSearchFilter('custrecord_ebizactive', null, 'is', 'T'));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
		columns[1] = new nlobjSearchColumn('custrecord_ebizsite');

		columns[1].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters, columns);	
		if(searchresults != null && searchresults != '')
		{
			if(searchresults[0].getValue('custrecord_ebizrulevalue') != null && searchresults[0].getValue('custrecord_ebizrulevalue') != '')
			{
				return searchresults[0].getValue('custrecord_ebizrulevalue');
			}
			else
				return 'Y';
		}
		else
			return 'Y';
	}
	catch (exp) 
	{
		nlapiLogExecution('ERROR', 'Exception in GetConfirmLotToNS and returning Y: ', exp);
		return 'Y';
	}	
}
function GetEDIconfig(soid,lineno){

	var test= new Array();

	var FrmCustRecdName='',FrmCustRecdID='',FrmCustRecdFieldID='',toCustRecdName='',toCustRecdID='',soheaderlevel='',solinelevel='',carriertype='',customer='' ;
	var defaultvalue='';
	var filters = new Array();	
	var columns = new Array();

	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	columns.push(new nlobjSearchColumn('custrecord_frm_custrecord_name'));	
	columns.push(new nlobjSearchColumn('custrecord_frm_cstmrecord_id'));	
	columns.push(new nlobjSearchColumn('custrecord_to_custrecrd_name'));	
	columns.push(new nlobjSearchColumn('custrecord_to_cstmrecord_fld_name'));	
	columns.push(new nlobjSearchColumn('custrecord_soheader_level'));	
	columns.push(new nlobjSearchColumn('custrecord_soline_level'));	
	columns.push(new nlobjSearchColumn('custrecord_carriertype_ediconfig'));	
	columns.push(new nlobjSearchColumn('custrecord_consignee_name'));
	columns.push(new nlobjSearchColumn('custrecord_default_val'));

	var searchresults = nlapiSearchRecord('customrecord_wms_edi_config', null, filters, columns);

	if(searchresults!=null && searchresults!='')
	{
		for(var m=0; m < searchresults.length; m++)
		{
			FrmCustRecdName = (searchresults[m].getText('custrecord_frm_custrecord_name')).split(':')[0];
			FrmCustRecdID = (searchresults[m].getText('custrecord_frm_custrecord_name')).split(':')[1];
			FrmCustRecdFieldID = searchresults[m].getValue('custrecord_frm_cstmrecord_id');
			toCustRecdName = searchresults[m].getValue('custrecord_to_custrecrd_name');
			toCustRecdID = searchresults[m].getValue('custrecord_to_cstmrecord_fld_name');
			soheaderlevel = searchresults[m].getValue('custrecord_soheader_level');
			solinelevel = searchresults[m].getValue('custrecord_soline_level');
			carriertype = searchresults[m].getText('custrecord_carriertype_ediconfig');
			customer = searchresults[m].getText('custrecord_consignee_name');
			defaultvalue = searchresults[m].getValue('custrecord_default_val');

			if(FrmCustRecdID == 'salesorder' && soheaderlevel == 'T')
			{
				var res=GetSODetails(FrmCustRecdID,FrmCustRecdFieldID,toCustRecdName,toCustRecdID,soid);
				for(var s=0;s<res.length;s++)
				{	
					var newcurrentRow = [res[s],toCustRecdID,carriertype,customer,defaultvalue]
					test.push(newcurrentRow);
				}

			}
		}

	}
	return test;
}

function GetSODetails(FrmCustRecdID,FrmCustRecdFieldID,toCustRecdName,toCustRecdID,soid)
{

	// FrmCustRecdName= "'"+FrmCustRecdName+"'";
	// FrmCustRecdID= "'"+FrmCustRecdID+"'";

	var filters = new Array();	
	var columns = new Array();

	filters.push(new nlobjSearchFilter('internalid', null, 'anyof', soid));
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

	columns.push(new nlobjSearchColumn(FrmCustRecdFieldID));		
	var sosearchresults = nlapiSearchRecord(FrmCustRecdID, null, filters, columns);	

	nlapiLogExecution('DEBUG', 'sosearchresults ', sosearchresults.length);
	return sosearchresults ;

}

function GetEDIconfigLinelevel(soid,lineno){

	var test1= new Array();

	var FrmCustRecdName='',FrmCustRecdID='',FrmCustRecdFieldID='',toCustRecdName='',toCustRecdID='',soheaderlevel='',solinelevel='',carriertype='' ;
	var defaultvalue='';
	var filters = new Array();	
	var columns = new Array();

	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	columns.push(new nlobjSearchColumn('custrecord_frm_custrecord_name'));	
	columns.push(new nlobjSearchColumn('custrecord_frm_cstmrecord_id'));	
	columns.push(new nlobjSearchColumn('custrecord_to_custrecrd_name'));	
	columns.push(new nlobjSearchColumn('custrecord_to_cstmrecord_fld_name'));
	columns.push(new nlobjSearchColumn('custrecord_soheader_level'));	
	columns.push(new nlobjSearchColumn('custrecord_soline_level'));	
	columns.push(new nlobjSearchColumn('custrecord_carriertype_ediconfig'));
	columns.push(new nlobjSearchColumn('custrecord_consignee_name'));
	columns.push(new nlobjSearchColumn('custrecord_default_val'));

	var searchresults = nlapiSearchRecord('customrecord_wms_edi_config', null, filters, columns);

	if(searchresults!=null && searchresults!='')
	{
		for(var m=0; m < searchresults.length; m++)
		{
			FrmCustRecdName = (searchresults[m].getText('custrecord_frm_custrecord_name')).split(':')[0];
			FrmCustRecdID = (searchresults[m].getText('custrecord_frm_custrecord_name')).split(':')[1];
			FrmCustRecdFieldID = searchresults[m].getValue('custrecord_frm_cstmrecord_id');
			toCustRecdName = searchresults[m].getValue('custrecord_to_custrecrd_name');
			toCustRecdID = searchresults[m].getValue('custrecord_to_cstmrecord_fld_name');
			soheaderlevel = searchresults[m].getValue('custrecord_soheader_level');
			solinelevel = searchresults[m].getValue('custrecord_soline_level');
			carriertype = searchresults[m].getText('custrecord_carriertype_ediconfig');
			customer = searchresults[m].getText('custrecord_consignee_name');
			defaultvalue = searchresults[m].getValue('custrecord_default_val');

			if(FrmCustRecdID == 'salesorder' && solinelevel == 'T')
			{
				var res1=GetSoLinedetails(FrmCustRecdID,FrmCustRecdFieldID,toCustRecdName,toCustRecdID,soid,lineno);

				var newlineRow = [res1,toCustRecdID,carriertype,customer,defaultvalue]
				test1.push(newlineRow);		

			}
		}

	}
	return test1;
}

function GetSoLinedetails(FrmCustRecdID,FrmCustRecdFieldID,toCustRecdName,toCustRecdID,soid,lineno ){

	var lineval="";	

	try{

		var salesorderRec = nlapiLoadRecord('salesorder', soid);
		lineval=salesorderRec.getLineItemValue('item',FrmCustRecdFieldID,lineno);
	}
	catch(exception){
		nlapiLogExecution('DEBUG', 'GetSoLinedetails', exception);
	}


	return lineval;
}
function GetEDIconfigDefaultvalues(soid,lineno){
	nlapiLogExecution('DEBUG', 'GetEDIconfigDefaultvalues', GetEDIconfigDefaultvalues);

	var defaultval= new Array();

	var FrmCustRecdName='',FrmCustRecdID='',FrmCustRecdFieldID='',toCustRecdName='',toCustRecdID='',soheaderlevel='',solinelevel='',carriertype='',customer='' ;
	var defaultvalue='',isdefault='';
	var filters = new Array();	
	var columns = new Array();

	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	columns.push(new nlobjSearchColumn('custrecord_frm_custrecord_name'));	
	columns.push(new nlobjSearchColumn('custrecord_frm_cstmrecord_id'));	
	columns.push(new nlobjSearchColumn('custrecord_to_custrecrd_name'));	
	columns.push(new nlobjSearchColumn('custrecord_to_cstmrecord_fld_name'));	
	columns.push(new nlobjSearchColumn('custrecord_soheader_level'));	
	columns.push(new nlobjSearchColumn('custrecord_soline_level'));	
	columns.push(new nlobjSearchColumn('custrecord_carriertype_ediconfig'));	
	columns.push(new nlobjSearchColumn('custrecord_consignee_name'));
	columns.push(new nlobjSearchColumn('custrecord_default_val'));
	columns.push(new nlobjSearchColumn('custrecord_isdefault'));

	var searchresults = nlapiSearchRecord('customrecord_wms_edi_config', null, filters, columns);

	if(searchresults!=null && searchresults!='')
	{
		for(var m=0; m < searchresults.length; m++)
		{
			FrmCustRecdName = (searchresults[m].getText('custrecord_frm_custrecord_name')).split(':')[0];
			FrmCustRecdID = (searchresults[m].getText('custrecord_frm_custrecord_name')).split(':')[1];
			FrmCustRecdFieldID = searchresults[m].getValue('custrecord_frm_cstmrecord_id');
			toCustRecdName = searchresults[m].getValue('custrecord_to_custrecrd_name');
			toCustRecdID = searchresults[m].getValue('custrecord_to_cstmrecord_fld_name');
			soheaderlevel = searchresults[m].getValue('custrecord_soheader_level');
			solinelevel = searchresults[m].getValue('custrecord_soline_level');
			carriertype = searchresults[m].getValue('custrecord_carriertype_ediconfig');
			customer = searchresults[m].getText('custrecord_consignee_name');
			defaultvalue = searchresults[m].getValue('custrecord_default_val');
			isdefault = searchresults[m].getValue('custrecord_isdefault');

			nlapiLogExecution('DEBUG', 'isdefault', isdefault);
			if(isdefault == 'T')
			{
				var newcurrentRow = [defaultvalue,toCustRecdID,carriertype,customer,defaultvalue]
				defaultval.push(newcurrentRow);

			}
		}

	}
	return defaultval;
}
//Case End 201414406 