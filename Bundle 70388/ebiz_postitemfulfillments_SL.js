
function fnupdateshipmanifest()
{
	var filters = new Array();
	var columns = new Array();

	filters.push(new nlobjSearchFilter('custrecord_ship_trackno', null, 'isnotempty'));
	filters.push(new nlobjSearchFilter('custrecord_ship_itemfulfill_no', null, 'anyof', ['@NONE@']));
	filters.push(new nlobjSearchFilter('custrecord_ship_void', null, 'is', 'U'));
	//filters.push(new nlobjSearchFilter('custrecord_ship_act_shipdate', null, 'is', '3/29/2013'));

	columns.push(new nlobjSearchColumn('custrecord_ship_act_shipdate'));

	var SOSearchResults = nlapiSearchRecord('customrecord_ship_manifest', null, filters , columns );
	if(SOSearchResults!=null)
	{
		nlapiLogExecution('ERROR', 'SOSearchResults',SOSearchResults.length);

		for(var p=0;p<SOSearchResults.length;p++)
		{

			nlapiSubmitField('customrecord_ship_manifest', SOSearchResults[p].getId(), 'custrecord_ship_void', 'N');

			var OpenTaskTransaction = nlapiLoadRecord('customrecord_ship_manifest', SOSearchResults[p].getId());
			OpenTaskTransaction.setFieldValue('custrecord_ship_void', 'U');
			nlapiSubmitRecord(OpenTaskTransaction, true);


		}
	}

}

function fnResendItemfulfillment()
{
	var context = nlapiGetContext();
	
	var filters = new Array();
	var columns = new Array();

	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof',[14]));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty'));

	columns[0] = new nlobjSearchColumn('custrecord_pack_confirmed_date');
	columns[0].setSort(true);

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters , columns );
	if(SOSearchResults!=null)
	{
		nlapiLogExecution('ERROR', 'SOSearchResults',SOSearchResults.length);

		for(var p=0;p<SOSearchResults.length;p++)
		{
			var OpenTaskTransaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', SOSearchResults[p].getId());
			OpenTaskTransaction.setFieldValue('custrecord_device_upload_flag', 'T');
			nlapiSubmitRecord(OpenTaskTransaction, true);
			
			if(context.getRemainingUsage()<100)
			{
				nlapiLogExecution('ERROR','Remaining Usage',context.getRemainingUsage());
				break;
			}
		}
	}
}

