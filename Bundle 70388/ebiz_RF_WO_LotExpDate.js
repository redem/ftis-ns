/***************************************************************************
��������� eBizNET Solutions Inc
 ****************************************************************************
 *
 *� $Source: /cvs/products/eBizNET4.5/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_WO_LotExpDate.js,v $
 *� $Revision: 1.1.2.1.4.4.2.1 $
 *� $Date: 2014/07/10 07:07:29 $
 *� $Author: skavuri $
 *� $Name: t_eBN_2014_1_StdBundle_3_157 $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_WO_LotExpDate.js,v $
 * Revision 1.1.2.1.4.4.2.1  2014/07/10 07:07:29  skavuri
 * Case# 20149080 Compatibility Issue Fixed
 *
 * Revision 1.1.2.1.4.4  2014/05/30 00:34:23  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.1.4.3  2013/06/17 14:05:31  skreddy
 * CASE201112/CR201113/LOG201121
 * issue rellated date format
 *
 * Revision 1.1.2.1.4.2  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.1.4.1  2013/03/08 14:38:41  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.1.2.2  2012/12/03 15:43:22  rmukkera
 *� �
 *
 ****************************************************************************/


/**
 * @param request
 * @param response
 */
function WOLotExpDate(request, response){
	if (request.getMethod() == 'GET') {
		//	Get the PO# from the previous screen, which is passed as a parameter		
		var getPONo = request.getParameter('custparam_woid');
		var getWONo = request.getParameter('custparam_woname');		
		var getWOItem = request.getParameter('custparam_item');
		var getPOLineNo = request.getParameter('custparam_lineno');
		var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		var getWOInternalId = request.getParameter('custparam_woid');
		var getWOQtyEntered = request.getParameter('custparam_expectedquantity');
		var getWOItemRemainingQty = request.getParameter('custparam_poitemremainingqty');
		var getWOLinePackCode = request.getParameter('custparam_packcode');
		var getPOLineItemStatus = request.getParameter('custparam_polineitemstatus');
		var getItemCube = request.getParameter('custparam_itemcube');
		var getnoofrecords =request.getParameter('custparam_noofrecords');
		/*		var getItemQuantity = request.getParameter('hdnQuantity');
		var getItemQuantityReceived = request.getParameter('hdnQuantityReceived');*/

		var getItemQuantity = request.getParameter('custparam_expectedquantity');
		var getItemQuantityReceived = request.getParameter('custparam_polinequantityreceived');

		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var getBatchNo = request.getParameter('custparam_batchno');
		var getWHLocation = request.getParameter('custparam_whlocation');
		var getItemType = request.getParameter('custparam_itemtype');
		var getwoStatus =request.getParameter('custparam_wostatus');
		nlapiLogExecution('ERROR','getWHLocation',getWHLocation);
		nlapiLogExecution('ERROR','getBatchNo',getBatchNo);

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);
		/***  The below code is merged from Endochoice account on 07thMar13  by Santosh  as part of Standard bundle ***/
		var getLotExpirydate = request.getParameter('custparam_batchexpirydate');
		/*** up to here ***/
		var st0,st1,st2,st3,st4,st5,st6;
		if( getLanguage == 'es_ES')
		{
			st0 = "LA HORA DE LLEGADA FECHA DE CADUCIDAD";
			st1 = "FECHA DE MANUFACTURA";
			st2 = "FECHA DE EXPIRACI&#211;N";
			st3 = "FECHA DE CADUCIDAD";
			st4 = "FORMATO: MMDDAA";			
			st5 = "ENVIAR";
			st6 = "ANTERIOR";			
		}
		else
		{
			st0 = "CHECKIN EXPIRY DATE";
			st1 = "MFG DATE";
			st2 = "EXP DATE";
			st3 = "BEST BEFORE DATE";
			st4 = "FORMAT : MMDDYY";
			st5 = "SEND";
			st6 = "PREV";

		}




		var functionkeyHtml=getFunctionkeyScript('_rf_cyclecountsku'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cyclecountsku' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + ":</td>";
		html = html + "				<input type='hidden' name='hdnWOStatus' value=" + getwoStatus + "></td>";
		html = html + "				<input type='hidden' name='hdnItemStatus' value=" + getPOLineItemStatus + "></td>";
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				<input type='hidden' name='hdnItemPackCode' value=" + getWOLinePackCode + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getItemQuantity + ">";
		html = html + "				<input type='hidden' name='hdnQuantityReceived' value=" + getItemQuantityReceived + ">";
		html = html + "				<input type='hidden' name='hdnItemCube' value=" + getItemCube + ">";
		html = html + "				<input type='hidden' name='hdnItemRemaininingQuantity' value=" + parseFloat(getWOItemRemainingQty) + ">";
		html = html + "				<input type='hidden' name='hdnPOQuantityEntered' value=" + parseFloat(getWOQtyEntered) + ">";
		html = html + "				<input type='hidden' name='hdnPOInternalId' value=" + getWOInternalId + ">";
		html = html + "				<input type='hidden' name='hdnBatchNo' value='" + getBatchNo + "'>";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + getWHLocation + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				<input type='hidden' name='hdnWOName' value=" + getWONo + ">";
		html = html + "				<input type='hidden' name='hdnnoofrecords' value=" + getnoofrecords + ">";
		html = html + "				<input type='hidden' name='hdnItemType' value=" + getItemType + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entermfgdate' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>EXP DATE:</td>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		/***  The below code is merged from Endochoice account on 07thMar13  by Santosh  as part of Standard bundle ***/
		if(getLotExpirydate!=null && getLotExpirydate!='')
		{
			html = html + "				<td align = 'left'><input name='enterexpdate' type='text'  value="+getLotExpirydate+" disabled/>";
			html = html + "				<input type='hidden' name='hdnlotexpirydate' value=" + getLotExpirydate + ">";
			html = html + "				</td>";
		}
		else
		{
			html = html + "				<td align = 'left'><input name='enterexpdate' type='text'/>";
			html = html + "				</td>";
		}
		/*** up to here ***/
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + ": </td>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterbbdate' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><label>" + st4;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st5 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st6 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);

	}
	else {
		try {

			var optedEvent = request.getParameter('cmdPrevious');

			var getMfgDate = request.getParameter('entermfgdate');
			var getExpDate = request.getParameter('enterexpdate');
			/***  The below code is merged from Endochoice account on 07thMar13  by Santosh  as part of Standard bundle ***/
			if(request.getParameter('hdnlotexpirydate')!=null && request.getParameter('hdnlotexpirydate')!='' && getExpDate=='')
			{
				getExpDate = request.getParameter('hdnlotexpirydate');
			}
			/*** up to here ***/
			var getBestBeforeDate = request.getParameter('enterbbdate');

			nlapiLogExecution('ERROR', 'optedEvent', optedEvent);

			var POarray = new Array();
			var getLanguage = request.getParameter('hdngetLanguage');
			POarray["custparam_language"] = getLanguage;
			nlapiLogExecution('ERROR', 'getLanguage', POarray["custparam_language"]);			
			var st7,st8;
			if( getLanguage == 'es_ES')
			{
				st7 = "ERROR EN LA PANTALLA DE FECHA EXP";
				st8 = "FECHAS NO V&#193;LIDAS";
			}
			else
			{
				st7 = "ERROR IN EXP DATE SCREEN";
				st8 = "INVALID DATES";
				st11 = "MFG DATE format should be MMDDYY";
				st12 = "EXP DATE format should be MMDDYY";
				st13 = "BEST BEFORE DATE format should be MMDDYY";
			}			

			POarray["custparam_noofrecords"] = request.getParameter('hdnnoofrecords');
			POarray["custparam_woname"] = request.getParameter('hdnWOName');

			POarray["custparam_cartlpno"] = request.getParameter('custparam_cartlpno');
			POarray["custparam_polineitemstatus"] = request.getParameter('hdnItemStatus');
			POarray["custparam_poitem"] = request.getParameter('custparam_poitem');
			POarray["custparam_lineno"] = request.getParameter('custparam_lineno');
			POarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
			POarray["custparam_pointernalid"] = request.getParameter('hdnPOInternalId'); //request.getParameter('custparam_pointernalid');
			POarray["custparam_poqtyentered"] = request.getParameter('hdnPOQuantityEntered'); //request.getParameter('custparam_poqtyentered');
			POarray["custparam_poitemremainingqty"] = request.getParameter('hdnItemRemaininingQuantity'); //request.getParameter('custparam_poitemremainingqty');
			POarray["custparam_polinepackcode"] = request.getParameter('hdnItemPackCode'); //request.getParameter('custparam_polinepackcode');
			POarray["custparam_polinequantity"] = request.getParameter('hdnQuantity'); //request.getParameter('custparam_polinequantity');
			POarray["custparam_polinequantityreceived"] = request.getParameter('hdnQuantityReceived'); //request.getParameter('custparam_polinequantityreceived');
			POarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
			POarray["custparam_itemcube"] = request.getParameter('custparam_itemcube');
			POarray["custparam_actualbegindate"] = request.getParameter('hdnActualBeginDate');
			POarray["custparam_actualbegintime"] = request.getParameter('hdnActualBeginTime');
			POarray["custparam_actualbegintimeampm"] = request.getParameter('hdnActualBeginTimeAMPM');
			POarray["custparam_actualbegintime"] = request.getParameter('hdnActualBeginTime');
			POarray["custparam_actualbegintimeampm"] = request.getParameter('hdnActualBeginTimeAMPM');
			POarray["custparam_error"] = st7;
			POarray["custparam_screenno"] = 'WOLotExpDt';
			POarray["custparam_batchno"] = request.getParameter('hdnBatchNo');
			POarray["custparam_expectedquantity"] = request.getParameter('hdnQuantity');
			nlapiLogExecution('ERROR','hdnBatchNo',request.getParameter('hdnBatchNo'));
			POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
			POarray["custparam_wostatus"] = request.getParameter('hdnWOStatus');
			POarray["custparam_packcode"] = request.getParameter('hdnItemPackCode');
			POarray["custparam_itemtype"] = request.getParameter('hdnItemType');
			/***  The below code is merged from Endochoice account on 07thMar13  by Santosh  as part of Standard bundle ***/
			POarray["custparam_batchexpirydate"] = getExpDate;
			/*** up to here ***/
			POarray["custparam_woid"] = request.getParameter('custparam_woid');
			POarray["custparam_item"] = request.getParameter('custparam_item');
			nlapiLogExecution('ERROR', 'WH Location', POarray["custparam_whlocation"]);
			var error='';
			var errorflag='F';

			if (optedEvent == 'F7') {
				nlapiLogExecution('DEBUG', 'CHECK IN EXP DATE F7 Pressed');
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_batchno', 'customdeploy_ebiz_rf_wo_putaway_batch_di', false, POarray);
			}
			else {
				
				if(getMfgDate != '' && getMfgDate.length > 6)
				{
					POarray["custparam_error"] = st11;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					return;
				}
				else if(getExpDate != '' && getExpDate.length > 6)
				{
					POarray["custparam_error"] = st12;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					return;
				}
				else if(getBestBeforeDate != '' && getBestBeforeDate.length > 6)
				{
					POarray["custparam_error"] = st13;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					return;
				}
					
				
				if ((getMfgDate != '' && getExpDate == '') || (getMfgDate == '' && getExpDate != '') || (getMfgDate != '' && getExpDate != '' && getBestBeforeDate != '')) {
					if (getMfgDate != '' && getExpDate == '')
					{
						var Mfgdate= RFDateFormat(getMfgDate);
						if(Mfgdate[0]=='true')
						{
							nlapiLogExecution('ERROR','Mfgdate',Mfgdate[1]);
							POarray["custparam_mfgdate"]=Mfgdate[1];
						}
						else {
							errorflag='T';
							error='MfgDate:'+Mfgdate[1];
						}
					}
					if (getMfgDate == '' && getExpDate != '')
					{
						/***  The below code is merged from Endochoice account on 07thMar13  by Santosh  as part of Standard bundle ***/
						//checking expdate is not equal to null/empty
						//nlapiLogExecution('ERROR',"request.getParameter('hdnlotexpirydate')",request.getParameter('hdnlotexpirydate'));
						//if(request.getParameter('hdnlotexpirydate')==null && request.getParameter('hdnlotexpirydate')=='') //Case# 20149080
						if(request.getParameter('hdnlotexpirydate')==null || request.getParameter('hdnlotexpirydate')=='')
						{/*** up to here ***/
							var Expdate= RFDateFormat(getExpDate);
							if(Expdate[0]=='true')
							{
								nlapiLogExecution('ERROR','Expdate',Expdate[1]);
								POarray["custparam_expdate"]=Expdate[1];
							}
							else {
								errorflag='T';
								error=error+'Expdate:'+Expdate[1];
							}
							/***  The below code is merged from Endochoice account on 07thMar13  by Santosh  as part of Standard bundle ***/
						}
						else
						{
							POarray["custparam_expdate"]=request.getParameter('hdnlotexpirydate');
						}
						/*** up to here ***/

					}
					if (getMfgDate != '' && getExpDate != '' && getBestBeforeDate != '') {

						var Mfgdate= RFDateFormat(getMfgDate);
						if(Mfgdate[0]=='true')
						{
							nlapiLogExecution('ERROR','Mfgdate',Mfgdate[1]);
							POarray["custparam_mfgdate"]=Mfgdate[1];
						}
						else {
							errorflag='T';
							error='MfgDate:'+Mfgdate[1];
						}
						/***  The below code is merged from Endochoice account on 07thMar13  by Santosh  as part of Standard bundle ***/
						if(request.getParameter('hdnlotexpirydate')==null && request.getParameter('hdnlotexpirydate')=='')
						{ /*** up to here ***/
							var Expdate= RFDateFormat(getExpDate);
							if(Expdate[0]=='true')
							{
								nlapiLogExecution('ERROR','Expdate',Expdate[1]);
								POarray["custparam_expdate"]=Expdate[1];
							}
							else {
								errorflag='T';
								error=error+'Expdate:'+Expdate[1];
							}
							/***  The below code is merged from Endochoice account on 07thMar13  by Santosh  as part of Standard bundle ***/
						}
						else
						{
							POarray["custparam_expdate"]=request.getParameter('hdnlotexpirydate');
						}
						/*** up to here ***/

						var BestBeforedate= RFDateFormat(getBestBeforeDate);
						if(BestBeforedate[0]=='true')
						{
							nlapiLogExecution('ERROR','BestBeforedate',BestBeforedate[1]);
							POarray["custparam_bestbeforedate"]=BestBeforedate[1];
						}
						else {
							errorflag='T';
							error=error+'BestBeforeDate:'+BestBeforedate[1];
						}

						//POarray["custparam_mfgdate"] = RFDateFormat(getMfgDate);
						//POarray["custparam_expdate"] = RFDateFormat(getExpDate);
						//POarray["custparam_bestbeforedate"] = RFDateFormat(getBestBeforeDate);
					}
					nlapiLogExecution('ERROR','error',error);
					if(errorflag=='F')
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_lotfifodate', 'customdeploy_ebiz_rf_wo_lotfifodate_di', false, POarray);
					else
					{
						POarray["custparam_error"] = error;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					}
				}
				else {
					POarray["custparam_error"] = st8;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				}
			}
		} 
		catch (e) {
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			nlapiLogExecution('DEBUG', 'Catch: Location not found');

		}
	}
}
