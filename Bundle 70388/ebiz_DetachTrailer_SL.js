/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_DetachTrailer_SL.js,v $
 *     	   $Revision: 1.1.2.1 $
 *     	   $Date: 2014/04/18 16:04:04 $
 *     	   $Author: gkalla $
 *     	   $Name: t_eBN_2014_1_StdBundle_2_30 $
 *
 *   
 *
 * 
 *****************************************************************************/


function detachtrailer(request,responce) {
	if (request.getMethod() == 'GET') 
	{
		var form = nlapiCreateForm('Detach Trailer');
		var trailer = form.addField('custpage_trailer', 'select', 'Trailer','customrecord_ebiznet_trailer');
		trailer.setLayoutType('startrow', 'none');

		trailer.setMandatory(true);		
		form.addSubmitButton('Detach');
		response.writePage(form);
	}
	else
	{
		var form = nlapiCreateForm('Detach Trailer');
		var trailerno=request.getParameter('custpage_trailer');
		nlapiLogExecution('ERROR','else trailerno',trailerno);
		var pronoresearch = nlapiLoadRecord('customrecord_ebiznet_trailer', trailerno);
		var trailerName=pronoresearch.getFieldValue('name');
		nlapiLogExecution('ERROR','trailerName',trailerName);

		var shiplparray=new Array();
		var opentaskids=new Array();
		var wavenos=new Array();
		var openFilers = new Array();
		if(trailerName!=null && trailerName!='')
			openFilers.push(new nlobjSearchFilter('custrecord_ebiz_trailer_no', null, 'is', trailerName));

		openFilers.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['10']));
		//openFilers.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty'));

		var openColumns=new Array();
		openColumns.push(new nlobjSearchColumn('custrecord_ship_lp_no'));
		openColumns.push(new nlobjSearchColumn('custrecord_ebiz_trailer_no'));
		openColumns.push(new nlobjSearchColumn('custrecord_wms_status_flag'));
		openColumns.push(new nlobjSearchColumn('custrecord_ebiz_wave_no'));

		var openSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, openFilers, openColumns);
		nlapiLogExecution('ERROR','openSearchResults',openSearchResults);
		if(openSearchResults!=null && openSearchResults!='')
		{
			for(var i=0;i<openSearchResults.length;i++)
			{
				shiplparray.push(openSearchResults[i].getValue('custrecord_ship_lp_no'));
				opentaskids.push(openSearchResults[i].getId());
				wavenos.push(openSearchResults[i].getValue('custrecord_ebiz_wave_no'))

			}

			var resultopentask=updateOpentask(opentaskids,trailerName);
			var shiplpinternalids=new Array();
			shiplpinternalids=fillShipLP(shiplparray,trailerName);
			nlapiLogExecution('ERROR', "shiplpinternalids", shiplpinternalids);
			var resultshiplp=updateLPMaster(shiplpinternalids,trailerName);
			updateClosedTask(wavenos,trailerName);
			nlapiLogExecution('ERROR', "resultopentask", resultopentask);
			nlapiLogExecution('ERROR', "resultshiplp", resultshiplp);
			if(resultopentask!=-1 && resultshiplp!=-1)
			{
				form.setScript('customscript_detachtrailer');
				var msg = form.addField('custpage_messages', 'inlinehtml', null, null, null);
				msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Trailer Detached successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
				nlapiLogExecution('ERROR','after completed','after completed');
				response.writePage(form);

			}
			else
			{
				var lpErrorMessage1 = 'Trailer Failed to Detach';
				showInlineMessage(form, 'Error', lpErrorMessage1, null);
				response.writePage(form);
			}
		}
		else
		{
			var lpErrorMessage1 = 'No Records to Detach on Selected Trailer';
			showInlineMessage(form, 'Error', lpErrorMessage1, null);
			response.writePage(form);

		}

	}

}

/*function Printreport()
{	
response.sendRedirect('SUITELET', 'customscript_loadunloadtrailer', 'customdeploy_loadunloadtrailer', false, null);
}*/

function updateOpentask(recordId, trailerName){
	var opentaskRecordId =-1;
	nlapiLogExecution('ERROR', "updateOpentask: trailerName", trailerName);
	nlapiLogExecution('ERROR', "updateOpentask: recordId", recordId);
	recordId=removeDuplicateElement(recordId);
	if(recordId!=null && recordId!="")
	{
		for(var i=0;i<recordId.length;i++){
			var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recordId[i]);
			var vTaskType=transaction.getFieldValue('custrecord_tasktype');
			if(vTaskType == '4')
			{
				nlapiLogExecution('ERROR', "Into Ship task");
				transaction.setFieldValue('custrecord_wms_status_flag', "28");
			}
			else
				transaction.setFieldValue('custrecord_wms_status_flag', "7");
			transaction.setFieldValue('custrecord_ebiz_trailer_no', '');
			transaction.setFieldValue('custrecord_upd_date',DateStamp()); 
			transaction.setFieldValue('custrecord_upd_ebiz_user_no',nlapiGetContext().getUser()); 
			opentaskRecordId = nlapiSubmitRecord(transaction, true);
			nlapiLogExecution('ERROR', "updateOpentask: opentaskRecordId", opentaskRecordId);
		}

	}


	return opentaskRecordId;
}



function fillShipLP(shipLP,trailerName){
	var shipLPId=new Array();


	shipLP=removeDuplicateElement(shipLP);
	for(var n=0;n<shipLP.length;n++)
	{
		var shipLPFilers = new Array();
		shipLPFilers.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_masterlp', null, 'is',trailerName));

		shipLPFilers.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_wmsstatusflag', null, 'anyof',['10']));
		shipLPFilers.push(new nlobjSearchFilter('name', null, 'is', shipLP[n]));

		var shipLPColumns = new Array();
		shipLPColumns[0] = new nlobjSearchColumn('name');

		var shipLPSearchResults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, shipLPFilers, shipLPColumns);
		nlapiLogExecution('ERROR', "shipLPSearchResults", shipLPSearchResults);
		if(shipLPSearchResults!=null && shipLPSearchResults!="")
		{
			for (var i = 0; i < shipLPSearchResults.length; i++) {
				shipLPId.push(shipLPSearchResults[i].getId());
			}
		}
	}
	return shipLPId;
}


function updateLPMaster(shipLPId, trailerName){
	var lpRecordId = -1;
	nlapiLogExecution('ERROR', "updateLPMaster: trailerName", trailerName);
	nlapiLogExecution('ERROR', "updateLPMaster: shipLPId", shipLPId);
	shipLPId=removeDuplicateElement(shipLPId);
	if(shipLPId!=null && shipLPId!="")
	{
		for(var n=0;n<shipLPId.length;n++)
		{
			var transaction = nlapiLoadRecord('customrecord_ebiznet_master_lp', shipLPId[n]);

			transaction.setFieldValue('custrecord_ebiz_lpmaster_wmsstatusflag', "7");
			transaction.setFieldValue('custrecord_ebiz_lpmaster_masterlp', '');
			lpRecordId = nlapiSubmitRecord(transaction, true);
		}
	}

	nlapiLogExecution('ERROR', "updateLPMaster: lpRecordId", lpRecordId);

	return lpRecordId;
}



function updateClosedTask(waveNumber,trailerName)
{
	nlapiLogExecution('ERROR', "updateClosedTask:waveNumber", waveNumber);
	var closedTaskId;
	waveNumber=removeDuplicateElement(waveNumber);
	for(var h=0;h<waveNumber.length;h++)
	{
		var closedTaskFilers = new Array();
		var closeTaskDetails = new Array();

		closedTaskFilers.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_trailer_no', null, 'is',trailerName));
		closedTaskFilers.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_wave_no', null, 'is', waveNumber[h]));	 

		var closedTaskColumns = new Array();
		closedTaskColumns[0] = new nlobjSearchColumn('name');
		closedTaskColumns[1] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_lpno');

		var closedTaskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, closedTaskFilers, closedTaskColumns);
		if(closedTaskSearchResults!=null && closedTaskSearchResults!='')
		{nlapiLogExecution('ERROR', "closedTaskSearchResults", closedTaskSearchResults.length);
		for (var i = 0; i < closedTaskSearchResults.length; i++) {
			closedTaskId = closedTaskSearchResults[i].getId();
			var vcontainerlp = closedTaskSearchResults[i].getValue('custrecord_ebiztask_ebiz_lpno');

			var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_ebiztask', closedTaskId);

			var quantityPicked = transaction.getFieldValue('custrecord_ebiztask_act_qty');
			var fulfillmentOrderInternalId = transaction.getFieldValue('custrecord_ebiztask_ebiz_cntrl_no');
			var fulfillmentOrder = transaction.getFieldValue('name');
			var waveNumber = transaction.getFieldValue('custrecord_ebiztask_ebiz_wave_no');

			var itemId = transaction.getFieldValue('custrecord_ebiztask_sku');
			nlapiLogExecution('ERROR', "itemId", itemId);
			var closeTaskRecordId = closedTaskSearchResults[i].getId();
			var itemFulfillmentInternalId = transaction.getFieldValue('custrecord_ebiz_nsconf_refno_ebiztask');//3369

			transaction.setFieldValue('custrecord_ebiztask_ebiz_trailer_no', '');
			transaction.setFieldValue('custrecord_ebiztask_wms_status_flag',"7");
			nlapiSubmitRecord(transaction, true);
			nlapiLogExecution('ERROR', "submitted in closed task", 'submitted');

		}
		}
	}


	return closeTaskDetails;
}
