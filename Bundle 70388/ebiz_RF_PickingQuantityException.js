/***************************************************************************
 eBizNET SOLUTIONS 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/ebiz_RF_PickingQuantityException.js,v $
<<<<<<< ebiz_RF_PickingQuantityException.js
<<<<<<< ebiz_RF_PickingQuantityException.js
 *     	   $Revision: 1.2.2.22.4.14.2.50.2.1 $
 *     	   $Date: 2015/11/05 15:37:39 $
 *     	   $Author: aanchal $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $ 
=======
 *     	   $Revision: 1.2.2.22.4.14.2.50.2.1 $
 *     	   $Date: 2015/11/05 15:37:39 $
 *     	   $Author: aanchal $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $ 
>>>>>>> 1.2.2.22.4.14.2.34
=======
<<<<<<< ebiz_RF_PickingQuantityException.js
 *     	   $Revision: 1.2.2.22.4.14.2.50.2.1 $
 *     	   $Date: 2015/11/05 15:37:39 $
 *     	   $Author: aanchal $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $ 
=======
 *     	   $Revision: 1.2.2.22.4.14.2.50.2.1 $
 *     	   $Date: 2015/11/05 15:37:39 $
 *     	   $Author: aanchal $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $ 
>>>>>>> 1.2.2.22.4.14.2.34
>>>>>>> 1.2.2.22.4.14.2.29
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_PickingQuantityException.js,v $
 * Revision 1.2.2.22.4.14.2.50.2.1  2015/11/05 15:37:39  aanchal
 * 2015.2 Issue Fix
 * 201415252
 *
 * Revision 1.2.2.22.4.14.2.50  2015/08/04 14:07:38  grao
 * 2015.2   issue fixes  20143630
 *
 * Revision 1.2.2.22.4.14.2.49  2015/07/02 15:45:06  skreddy
 * Case# 201412952
 * Signwarehouse SB issue fix
 *
 * Revision 1.2.2.22.4.14.2.48  2015/06/22 15:40:30  grao
 * SB issue fixes  201412952
 *
 * Revision 1.2.2.22.4.14.2.47  2015/04/13 09:25:59  rrpulicherla
 * Case#201412277
 *
 * Revision 1.2.2.22.4.14.2.46  2015/01/29 07:27:58  snimmakayala
 * Case#: 201411485
 *
 * Revision 1.2.2.22.4.14.2.45  2014/10/17 13:58:35  skavuri
 * Case# 201410637 Std bundle Issue fixed
 *
 * Revision 1.2.2.22.4.14.2.44  2014/09/25 17:57:17  skreddy
 * case # 201410534
 * Alpha comm CR (''pick by carton LP")
 *
 * Revision 1.2.2.22.4.14.2.43  2014/08/19 13:04:08  skavuri
 * Case# 201410014 Std Bundle issue fixed
 *
 * Revision 1.2.2.22.4.14.2.42  2014/07/14 15:32:40  skavuri
 * Case# 20148322 SB Issue Fixed
 *
 * Revision 1.2.2.22.4.14.2.41  2014/06/13 12:24:51  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.2.2.22.4.14.2.40  2014/06/06 15:14:55  sponnaganti
 * case# 20148756
 * stnd Bundle Issue Fix
 *
 * Revision 1.2.2.22.4.14.2.39  2014/05/30 00:41:05  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.2.2.22.4.14.2.38  2014/05/26 07:08:44  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148320
 *
 * Revision 1.2.2.22.4.14.2.37  2014/05/20 15:46:24  skavuri
 * Case # 20148439 SB Issue Fixed
 *
 * Revision 1.2.2.22.4.14.2.36  2014/04/18 14:34:41  rmukkera
 * Case # 20147952
 *
 * Revision 1.2.2.22.4.14.2.35  2014/03/24 15:09:45  grao
 * Case# 20127804 related issue fixes in Sonic  electrox issue fixes
 *
 * Revision 1.2.2.22.4.14.2.34  2014/02/19 13:39:42  sponnaganti
 * case# 20127170
 * (single quotes added to getItem,getItemName)
 *
 * Revision 1.2.2.22.4.14.2.33  2014/02/06 15:33:28  nneelam
 * case#  20126849
 * std bundle issue fix
 *
 * Revision 1.2.2.22.4.14.2.32  2014/01/29 07:17:15  sponnaganti
 * case# 20126988
 * Removing the remaining qty field
 *
 * Revision 1.2.2.22.4.14.2.31  2014/01/06 13:19:17  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.2.2.22.4.14.2.30  2013/12/26 21:47:01  grao
 * Case# 20126289 related issue fixes in Sb issue fixes
 *
 * Revision 1.2.2.22.4.14.2.29  2013/12/23 15:09:01  rmukkera
 * Case # 20126289
 *
 * Revision 1.2.2.22.4.14.2.28  2013/12/12 15:00:42  rmukkera
 * Case# 20126289
 *
 * Revision 1.2.2.22.4.14.2.27  2013/12/09 14:04:22  schepuri
 * 20126289
 *
 * Revision 1.2.2.22.4.14.2.26  2013/12/05 11:56:23  snimmakayala
 * no message
 *
 * Revision 1.2.2.22.4.14.2.25  2013/11/25 10:22:07  skreddy
 * Case# 20125850
 * Lost and Found CR for MHP
 * issue fix
 *
 * Revision 1.2.2.22.4.14.2.24  2013/11/22 14:29:59  rmukkera
 * Case# 20125591
 *
 * Revision 1.2.2.22.4.14.2.23  2013/11/22 08:40:38  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#201216680
 *
 * Revision 1.2.2.22.4.14.2.22  2013/11/20 23:28:13  skreddy
 * Case# 20125850
 * Lost and Found CR for MHP
 * issue fix
 *
 * Revision 1.2.2.22.4.14.2.21  2013/11/14 06:32:04  skreddy
 * Case# 201215000
 * Fast Picking issue fix
 *
 * Revision 1.2.2.22.4.14.2.20  2013/10/22 14:01:29  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 20124795
 * Replen Priorities
 *
 * Revision 1.2.2.22.4.14.2.19  2013/10/09 16:22:13  skreddy
 * Case# 20124964
 * tpp SB issue fixs
 *
 * Revision 1.2.2.22.4.14.2.18  2013/09/16 09:40:01  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201215000
 * RF Fast Picking
 *
 * Revision 1.2.2.22.4.14.2.17  2013/08/15 01:43:06  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201215000
 * RF Fast Picking
 *
 * Revision 1.2.2.22.4.14.2.16  2013/08/02 00:07:00  skreddy
 * Case# 20123709
 * issue rellated to qyy exception
 *
 * Revision 1.2.2.22.4.14.2.15  2013/06/19 22:56:07  gkalla
 * CASE201112/CR201113/LOG201121
 * As part of Demo issue
 *
 * Revision 1.2.2.22.4.14.2.14  2013/06/14 13:01:47  grao
 * CASE201112/CR201113/LOG201121
 * PMM Issues fixes
 *
 * Issue fixed for Displaying item  name instead of item id in Qty exception screen
 *
 * Revision 1.2.2.22.4.14.2.13  2013/06/11 14:30:20  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.2.2.22.4.14.2.12  2013/06/05 12:02:07  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.2.2.22.4.14.2.11  2013/06/03 07:51:31  gkalla
 * CASE201112/CR201113/LOG201121
 * Redirecting to RF Pick menu
 *
 * Revision 1.2.2.22.4.14.2.10  2013/05/14 14:15:44  schepuri
 * GSUSA SB Task Priority issue
 *
 * Revision 1.2.2.22.4.14.2.9  2013/04/29 14:41:04  schepuri
 * Total weight undefined issue fix
 *
 * Revision 1.2.2.22.4.14.2.8  2013/04/26 15:49:51  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fixes
 *
 * Revision 1.2.2.22.4.14.2.7  2013/04/24 15:54:26  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fixes
 *
 * Revision 1.2.2.22.4.14.2.6  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.2.2.22.4.14.2.5  2013/04/08 08:47:03  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.2.2.22.4.14.2.4  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.2.2.22.4.14.2.3  2013/03/01 15:01:12  rmukkera
 * code is merged from FactoryMation production as part of Standard bundle
 *
 * Revision 1.2.2.22.4.14.2.2  2013/02/28 06:55:57  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Merged from Monobind.
 *
 * Revision 1.2.2.22.4.14.2.1  2013/02/27 13:22:58  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from Monobind
 *
 * Revision 1.2.2.22.4.14  2013/02/06 14:38:20  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.2.2.22.4.13  2012/12/31 05:53:10  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Inventory Sync issue fixes
 * .
 *
 * Revision 1.2.2.22.4.12  2012/12/16 01:39:08  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA after go live Fixes.
 *
 * Revision 1.2.2.22.4.11  2012/12/12 17:46:39  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * aftershipmanifest issue fix
 *
 * Revision 1.2.2.22.4.10  2012/11/30 23:56:48  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA UAT Fixes.
 *
 * Revision 1.2.2.22.4.9  2012/11/12 15:44:48  gkalla
 * CASE201112/CR201113/LOG201121
 * RF Qty exception
 *
 * Revision 1.2.2.22.4.8  2012/11/01 14:55:23  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.2.2.22.4.7  2012/10/26 08:06:42  gkalla
 * CASE201112/CR201113/LOG201121
 * Cluster# filter in query
 *
 * Revision 1.2.2.22.4.6  2012/10/23 17:08:39  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form branch.
 *
 * Revision 1.2.2.22.4.5  2012/10/11 10:27:04  gkalla
 * CASE201112/CR201113/LOG201121
 * Pick confirmation and other inventory issues
 *
 * Revision 1.2.2.22.4.4  2012/10/07 23:06:03  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue Fixes for FISK,BOOMBAH and TDG.
 *
 * Revision 1.2.2.22.4.3  2012/10/04 10:27:54  grao
 * CASE201112/CR201113/LOG201121
 * Converting multilanguage with given suggestions
 *
 * Revision 1.2.2.22.4.2  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.2.2.22.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.2.2.22  2012/09/04 20:45:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Incorporate Remaining Qty option in Cluster Picking Qty exception.
 *
 * Revision 1.2.2.21  2012/08/30 01:59:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Fisk Production Issue Fixes
 *
 * Revision 1.2.2.19  2012/08/08 15:14:59  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Kit to order
 *
 * Revision 1.2.2.18  2012/08/07 21:18:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.2.2.17  2012/08/03 21:34:22  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.2.2.16  2012/07/30 16:58:16  gkalla
 * CASE201112/CR201113/LOG201121
 * replenishment merging
 *
 * Revision 1.2.2.15  2012/07/19 05:24:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Performance Tuning
 *
 * Revision 1.2.2.14  2012/06/06 16:50:48  gkalla
 * CASE201112/CR201113/LOG201121
 * To give the message if we don't have open replens and not able to generate replens.
 *
 * Revision 1.2.2.13  2012/06/02 09:15:46  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to picking resolved.
 *
 * Revision 1.2.2.12  2012/05/22 16:05:08  gkalla
 * CASE201112/CR201113/LOG201121
 * To fix replenishment issue for RF Pick Qty exception
 *
 * Revision 1.2.2.11  2012/05/09 07:52:37  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Partial Status Handling
 *
 * Revision 1.2.2.10  2012/04/24 16:11:22  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Picking Qty Exception (with REPLNS)
 *
 * Revision 1.2.2.9  2012/04/24 13:49:35  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Picking Qty Exception (with REPLNS)
 *
 * Revision 1.2.2.8  2012/04/24 07:09:40  mbpragada
 * CASE201112/CR201113/LOG201121
 * fixed the issues in skip location functionality in RF Picking
 *
 * Revision 1.2.2.7  2012/04/19 15:00:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Zone Picking
 *
 * Revision 1.2.2.6  2012/04/18 14:27:31  mbpragada
 * CASE201112/CR201113/LOG201121
 * added Skip Location functionality
 *
 * Revision 1.2.2.5  2012/04/18 13:35:27  schepuri
 * CASE201112/CR201113/LOG201121
 * validating so qty with pick qty
 *
 * Revision 1.2.2.4  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.2.2.3  2012/02/22 13:08:24  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.2.2.2  2012/02/08 15:27:30  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * functional keys
 *
 * Revision 1.2.2.1  2012/02/08 12:35:51  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge to brach
 *
 * Revision 1.4  2012/01/27 07:12:42  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RFpick qty exception changes
 *
 * Revision 1.2  2011/12/13 10:13:03  schepuri
 * CASE201112/CR201113/LOG201121
 * adding suitlet for RF PICK Exception
 *
 * Revision 1.1  2011/12/12 13:01:24  schepuri
 * CASE201112/CR201113/LOG201121
 * adding suitlet for RF PICK Exception
 *
 *
 *****************************************************************************/

function PickingQuantityException(request, response)
{

	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);

	if (request.getMethod() == 'GET') 
	{
		var fastpick = request.getParameter('custparam_fastpick');
		nlapiLogExecution('DEBUG', 'fastpick', fastpick);

		//var getItemName="",getItemNo="",getQuantity="";
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7,st8,st9,st10;

		if( getLanguage == 'es_ES')
		{
			st1 = "CONFIRME LA REPLENS ABIERTAS";
			st2 = "INVENTARIO INSUFICIENTE EN LUGAR A GRANEL";
			st3 = "CANT ESPERADO:";
			st4 = "ART&#205;CULO:";
			st5 = "N&#218;MERO DE EMPAQUE:";
			st6 = "INGRESAR LA CANTIDAD ACTUAL";
			st7 = "RAZ&#211;N:";
			//case# 20126988 start
			//st8 = "INGRESAR LA CANTIDAD RESTANTE";
			//case# 20126988 end
			st9 = "ENVIAR";
			st10 = "ANTERIOR";
		}
		else
		{
			st1 = "CONFIRM THE OPEN REPLENS"; 
			st2 = "INSUFFICIENT INVENTORY IN BULK LOCATION";
			st3 = "EXPECTED QTY : ";
			st4 = "ITEM: ";
			st5 = "CARTON NO : ";
			st6 = "ENTER ACTUAL QUANTITY ";
			st7 = "REASON : ";
			//case# 20126988 start
			//st8 = "ENTER REMAINING QUANTITY ";
			//case# 20126988 end
			st9 = "SEND ";
			st10 = "PREV";
		}


		var getWaveNo = request.getParameter('custparam_waveno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocation = request.getParameter('custparam_beginLocation');
		var getItem = request.getParameter('custparam_item');
		var getItemName = request.getParameter('custparam_itemname');
		var getItemDesc = request.getParameter('custparam_itemdescription');
		var getItemNo = request.getParameter('custparam_iteminternalid');
		var getdoLoineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getBeginLocationName = request.getParameter('custparam_beginlocationname');
		if(getBeginLocationName == null || getBeginLocationName == '' || getBeginLocationName =='null' || getBeginLocationName =='undefined')
			getBeginLocationName = request.getParameter('custparam_beginLocationname');
		var getEndLocationInternalId = request.getParameter('custparam_endlocinternalid');
		var getEndLocation = request.getParameter('custparam_endlocation');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var getClusterNo = request.getParameter('custparam_clusterno');
		var getBatchNo = request.getParameter('custparam_batchno');
		var getNoofRecords = request.getParameter('custparam_noofrecords');
		var getNextLocation = request.getParameter('custparam_nextlocation');
		var name = request.getParameter('name');
		var getRecCount = request.getParameter('custparam_RecCount');
		var getContainerSize = request.getParameter('custparam_containersize');
		var getEbizOrdNo = request.getParameter('custparam_ebizordno');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var whLocation = request.getParameter('custparam_whlocation');
		//parameters erquired if serializedinventoryitem
		var getnumber = request.getParameter('custparam_number');
		var getRecType = request.getParameter('custparam_RecType');
		var getSerOut = request.getParameter('custparam_SerOut');
		var getSerIn = request.getParameter('custparam_SerIn');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var getNextExpectedQuantity = request.getParameter('custparam_nextexpectedquantity');
		var NextRecordId=request.getParameter('custparam_nextrecordid');
		var Exceptionflag=request.getParameter('custparam_Exceptionflag');
		var itemType=request.getParameter('custparam_itemType');
		var ActBatchno = request.getParameter('custparam_Actbatchno');
		var ExpBatchno=request.getParameter('custparam_Expbatchno');
		nlapiLogExecution('DEBUG', 'NextItemInternalId', NextItemInternalId);
		var pickType=request.getParameter('custparam_picktype');
		var ItemInstructions=request.getParameter('custparam_itemInstructions');
		var serialscanflag=request.getParameter('custparam_serialscanflag');
		

		//case # 20126849........added below parameter
		var EntLoc = request.getParameter('custparam_EntLoc');
		var EntLocRec=request.getParameter('custparam_EntLocRec');
		var EntLocID=request.getParameter('custparam_EntLocId');
		var ExceptionFlag = request.getParameter('custparam_Exc');

		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');

		var getZoneNo=request.getParameter('custparam_ebizzoneno');
		
		nlapiLogExecution('DEBUG', 'serialscanflag in get', serialscanflag);
		nlapiLogExecution('DEBUG', 'getWaveNo', getWaveNo);
		nlapiLogExecution('DEBUG', 'getRecordInternalId', getRecordInternalId);
		nlapiLogExecution('DEBUG', 'getContainerLpNo', getContainerLpNo);
		nlapiLogExecution('DEBUG', 'getQuantity', getQuantity);
		nlapiLogExecution('DEBUG', 'getBeginLocation', getBeginLocation);
		nlapiLogExecution('DEBUG', 'getItem', getItem);
		nlapiLogExecution('DEBUG', 'getItemName', getItemName);
		nlapiLogExecution('DEBUG', 'getItemDesc', getItemDesc);
		nlapiLogExecution('DEBUG', 'getItemNo', getItemNo);
		nlapiLogExecution('DEBUG', 'getdoLoineId', getdoLoineId);
		nlapiLogExecution('DEBUG', 'getInvoiceRefNo', getInvoiceRefNo);
		nlapiLogExecution('DEBUG', 'getBeginLocationName', getBeginLocationName);
		nlapiLogExecution('DEBUG', 'getEndLocationInternalId', getEndLocationInternalId);
		nlapiLogExecution('DEBUG', 'getEndLocation', getEndLocation);
		nlapiLogExecution('DEBUG', 'getOrderLineNo', getOrderLineNo);
		nlapiLogExecution('DEBUG', 'getClusterNo', getClusterNo);
		nlapiLogExecution('DEBUG', 'getBatchNo', getBatchNo);
		nlapiLogExecution('DEBUG', 'getNoofRecords', getNoofRecords);
		nlapiLogExecution('DEBUG', 'getNextLocation', getNextLocation);
		nlapiLogExecution('DEBUG', 'name', name);
		nlapiLogExecution('DEBUG', 'getRecCount', getRecCount);
		nlapiLogExecution('DEBUG', 'getContainerSize', getContainerSize);
		nlapiLogExecution('DEBUG', 'getEbizOrdNo', getEbizOrdNo);
		nlapiLogExecution('DEBUG', 'getnumber', getnumber);
		nlapiLogExecution('DEBUG', 'getRecType', getRecType);
		nlapiLogExecution('DEBUG', 'getSerOut', getSerOut);
		nlapiLogExecution('DEBUG', 'getSerIn', getSerIn);
		nlapiLogExecution('DEBUG', 'getNextExpectedQuantity', getNextExpectedQuantity);
		nlapiLogExecution('DEBUG', 'getBeginLocation', getBeginLocation);
		nlapiLogExecution('DEBUG', 'NextItemInternalId', NextItemInternalId);

		var SOarray=new Array();
		SOarray["custparam_language"] = getLanguage;
		if(fastpick!='Y')
		{
			SOarray["custparam_screenno"] = '11EXP';
		}
		else
		{
			SOarray["custparam_screenno"] = 'pickingLocItem';//to redirect to new screen
		}

		SOarray["custparam_error"]=st1;//'CONFIRM 

		SOarray["custparam_waveno"] = getWaveNo;
		SOarray["custparam_recordinternalid"] = getRecordInternalId;
		SOarray["custparam_containerlpno"] = getContainerLpNo;
		SOarray["custparam_expectedquantity"] = getQuantity;
		SOarray["custparam_beginLocation"] = getBeginLocation;
		SOarray["custparam_item"] = getItem;
		SOarray["custparam_itemname"] = getItemName;
		SOarray["custparam_itemdescription"] = getItemDesc;
		SOarray["custparam_iteminternalid"] = getItemNo;
		SOarray["custparam_dolineid"] = getdoLoineId;
		SOarray["custparam_invoicerefno"] = getInvoiceRefNo;
		SOarray["custparam_beginlocationname"] = getBeginLocationName;
		SOarray["custparam_beginLocationname"] = getBeginLocationName;
		SOarray["custparam_endlocinternalid"] = getEndLocationInternalId;
		SOarray["custparam_endlocation"] = getEndLocation;
		SOarray["custparam_orderlineno"] = getOrderLineNo;
		SOarray["custparam_clusterno"] = getClusterNo;
		SOarray["custparam_batchno"] = getBatchNo;
		SOarray["custparam_noofrecords"] = getNoofRecords;
		SOarray["custparam_nextlocation"] = getNextLocation;
		SOarray["name"] = name;
		SOarray["custparam_containersize"] = getContainerSize;
		SOarray["custparam_ebizordno"] = getEbizOrdNo;
		SOarray["custparam_number"] = getnumber;
		SOarray["custparam_RecType"] = getRecType;
		SOarray["custparam_SerOut"] = getSerOut;
		SOarray["custparam_SerIn"] = getSerIn;
		SOarray["custparam_nextiteminternalid"] = NextItemInternalId;
		SOarray["custparam_nextexpectedquantity"]=getNextExpectedQuantity;
		SOarray["custparam_nextrecordid"]=NextRecordId;
		SOarray["custparam_itemType"] = itemType;
		SOarray["custparam_Actbatchno"]=request.getParameter('custparam_Actbatchno');
		SOarray["custparam_Expbatchno"] = request.getParameter('custparam_Expbatchno');
		SOarray["custparam_fastpick"] = fastpick;
		SOarray["custparam_itemInstructions"] = ItemInstructions;

		if(getZoneNo!=null && getZoneNo!="")
		{
			SOarray["custparam_ebizzoneno"] =  getZoneNo;
		}
		else
			SOarray["custparam_ebizzoneno"] = '';

		nlapiLogExecution('DEBUG', 'FO Internalid', getdoLoineId);

		if(getdoLoineId!=null && getdoLoineId!='')
		{
			var folinerec = nlapiLoadRecord('customrecord_ebiznet_ordline', getdoLoineId);
			if(folinerec!=null && folinerec!='')
			{
				var shipcompleteflag = folinerec.getFieldValue('custrecord_shipcomplete');
				nlapiLogExecution('DEBUG', 'shipcompleteflag', shipcompleteflag);
				if(shipcompleteflag=='T')
				{
					var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',getRecordInternalId,'custrecord_skiptask');
					if(skiptask==null || skiptask=='')
					{
						skiptask=1;
					}
					else
					{
						skiptask=parseInt(skiptask)+1;
					}

					nlapiLogExecution('DEBUG', 'skiptask',skiptask);

					nlapiSubmitField('customrecord_ebiznet_trn_opentask', getRecordInternalId, 'custrecord_skiptask', skiptask);

					var SOarray1=new Array();
					nlapiLogExecution('DEBUG', 'pickType',pickType);
					SOarray1=buildTaskArray(getWaveNo,getClusterNo,name,getEbizOrdNo,getZoneNo,pickType,getContainerLpNo);					
					if(fastpick!='Y')
					{
						SOarray1["custparam_screenno"] = '11EXP';
					}
					else
					{
						SOarray1["custparam_screenno"] = 'pickingLocItem';
					}
					SOarray["custparam_beginLocationname"] = getNextLocation; //Case# 201410014 
					SOarray1["custparam_error"]='THIS ORDER HAS SHIP COMPLETE.CONTACT SUPERVISIOR.';
					SOarray["custparam_fastpick"] = fastpick;
					SOarray1["custparam_fastpick"] = fastpick; //Case# 201410637 
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray1);
					return;
				}
			}
		}

		var IsPickFaceLoc = 'F';

		IsPickFaceLoc = isPickFaceLocation(getItemNo,getEndLocationInternalId);

		if(IsPickFaceLoc=='T' && Exceptionflag!='Y')
		{			
			var openreplns = new Array();
			openreplns=getOpenReplns(getItemNo,getEndLocationInternalId);
			var searchpickfaceQty = getQOHinPFLocation(getItemNo,getEndLocationInternalId);
			var pickfaceQty;
			if(searchpickfaceQty!=null && searchpickfaceQty!='')
			{
				pickfaceQty=searchpickfaceQty[0].getValue('custrecord_ebiz_qoh',null,'sum');
			}
			var vtaskpriority = '0';
			nlapiLogExecution('DEBUG', 'pickfaceQty', pickfaceQty);
			if(openreplns!=null && openreplns!='' && openreplns.length>0)
			{
				var totalreplnqty=0;
				for(var i=0; i< openreplns.length;i++)
				{
					var expqty=openreplns[i].getValue('custrecord_expe_qty');
					if(expqty == null || expqty == '')
					{
						expqty=0;
					}

					totalreplnqty = parseInt(totalreplnqty)+parseInt(expqty);

					nlapiSubmitField('customrecord_ebiznet_trn_opentask', openreplns[i].getId(), 'custrecord_taskpriority', '0');
				}

				nlapiLogExecution('DEBUG', 'totalreplnqty',totalreplnqty);
				nlapiLogExecution('DEBUG', 'getQuantity',getQuantity);

				if(parseInt(totalreplnqty)>=parseInt(getQuantity))
				{					
					nlapiLogExecution('DEBUG', 'getRecordInternalId',getRecordInternalId);

					var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',getRecordInternalId,'custrecord_skiptask');
					if(skiptask==null || skiptask=='')
					{
						skiptask=1;
					}
					else
					{
						skiptask=parseInt(skiptask)+1;
					}
					nlapiLogExecution('DEBUG', 'skiptask',skiptask);

					nlapiSubmitField('customrecord_ebiznet_trn_opentask', getRecordInternalId, 'custrecord_skiptask', skiptask);
				}
				else
				{
					nlapiLogExecution('DEBUG', 'Into Else');

					var replengen = generateReplenishment(getEndLocationInternalId,getItemNo,whLocation,getWaveNo,'0');
					if(replengen==true)
					{
						var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',getRecordInternalId,'custrecord_skiptask');
						if(skiptask==null || skiptask=='')
						{
							skiptask=1;
						}
						else
						{
							skiptask=parseInt(skiptask)+1;
						}

						nlapiLogExecution('DEBUG', 'skiptask',skiptask);

						nlapiSubmitField('customrecord_ebiznet_trn_opentask', getRecordInternalId, 'custrecord_skiptask', skiptask);

					}
					else
					{
						nlapiLogExecution('DEBUG', 'Into Else');

						var replengen = generateReplenishment(getEndLocationInternalId,getItemNo,whLocation,getWaveNo,vtaskpriority);
						if(replengen==true)
						{
							var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',getRecordInternalId,'custrecord_skiptask');
							if(skiptask==null || skiptask=='')
							{
								skiptask=1;
							}
							else
							{
								skiptask=parseInt(skiptask)+1;
							}

							nlapiLogExecution('DEBUG', 'skiptask',skiptask);

							nlapiSubmitField('customrecord_ebiznet_trn_opentask', getRecordInternalId, 'custrecord_skiptask', skiptask);

							//nlapiSubmitField('customrecord_ebiznet_trn_opentask', getRecordInternalId, 'custrecord_skiptask', 'Y');
							var SOarray=new Array();
							nlapiLogExecution('DEBUG', 'pickType',pickType);
							SOarray=buildTaskArray(getWaveNo,getClusterNo,name,getEbizOrdNo,getZoneNo,pickType);

							SOarray["custparam_skipid"] = vSkipId;
							if(fastpick!='Y')
							{
								SOarray["custparam_screenno"] = '11EXP';
							}
							else
							{
								SOarray["custparam_screenno"] = 'pickingLocItem';
							}
							SOarray["custparam_error"]=st1;//'CONFIRM THE OPEN REPLENS';
							SOarray["custparam_fastpick"] = fastpick;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							return;
						}
					}	
				}
				var SOarray=new Array();
				nlapiLogExecution('DEBUG', 'pickType',pickType);
				SOarray=buildTaskArray(getWaveNo,getClusterNo,name,getEbizOrdNo,getZoneNo,pickType,fastpick);

				SOarray["custparam_skipid"] = vSkipId;
				if(fastpick!='Y')
				{
					SOarray["custparam_screenno"] = '11EXP';
				}
				else
				{
					SOarray["custparam_screenno"] = 'pickingLocItem';
				}	
				SOarray["custparam_error"]='CONFIRM THE OPEN REPLENS';
				SOarray["custparam_fastpick"] = fastpick;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				return;
			}
			else
			{
				nlapiLogExecution('DEBUG','taskpriority from pick excep in else', vtaskpriority);
				var replengen = generateReplenishment(getEndLocationInternalId,getItemNo,whLocation,getWaveNo,'0');
				if(replengen==true)
				{
					var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',getRecordInternalId,'custrecord_skiptask');
					if(skiptask==null || skiptask=='')
					{
						skiptask=1;
					}
					else
					{
						skiptask=parseInt(skiptask)+1;
					}

					nlapiLogExecution('DEBUG', 'skiptask',skiptask);

					nlapiSubmitField('customrecord_ebiznet_trn_opentask', getRecordInternalId, 'custrecord_skiptask', skiptask);

					var SOarray=new Array();
					nlapiLogExecution('DEBUG', 'pickType',pickType);
					SOarray=buildTaskArray(getWaveNo,getClusterNo,name,getEbizOrdNo,getZoneNo,pickType,fastpick);

					SOarray["custparam_skipid"] = vSkipId;
					if(fastpick!='Y')
					{
						SOarray["custparam_screenno"] = '11EXP';
					}
					else
					{
						SOarray["custparam_screenno"] = 'pickingLocItem';
					}	
					SOarray["custparam_error"]=st1;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;
				}
				else if(pickfaceQty==null && pickfaceQty=='')
				{
					var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',getRecordInternalId,'custrecord_skiptask');
					if(skiptask==null || skiptask=='')
					{
						skiptask=1;
					}
					else
					{
						skiptask=parseInt(skiptask)+1;
					}

					nlapiLogExecution('DEBUG', 'skiptask',skiptask);

					nlapiSubmitField('customrecord_ebiznet_trn_opentask', getRecordInternalId, 'custrecord_skiptask', skiptask);

					var SOarray=new Array();
					nlapiLogExecution('DEBUG', 'pickType',pickType);
					SOarray=buildTaskArray(getWaveNo,getClusterNo,name,getEbizOrdNo,getZoneNo,pickType,fastpick);
					SOarray["custparam_skipid"] = vSkipId;
					if(fastpick!='Y')
					{
						SOarray["custparam_screenno"] = '11EXP';
					}
					else
					{
						SOarray["custparam_screenno"] = 'pickingLocItem';
					}	
					SOarray["custparam_error"]=st2;
					Exceptionflag='Y';
					SOarray["custparam_Exceptionflag"]=Exceptionflag;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;
				}
			}
		}

		if(getBeginLocation==null)
		{
			getBeginLocation=getNextLocation;
		}
		if(getItemNo==null)
		{
			getItemNo=NextItemInternalId;
		}

		//general func is added in script library
		var functionkeyHtml=getFunctionkeyScript('_rf_pickingquantityexception'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";   
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";

		//html = html + " document.getElementById('enterquantity').focus();";    

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		//html = html + "	  alert(node.type);";
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		//	html = html + "	  alert(document.getElementById('cmdConfirm').disabled);";

		html = html + "	  if(document.getElementById('cmdConfirm').disabled==true){";

		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";

		html = html +functionkeyHtml;
		html = html + "</head>";
		html = html + "<body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_pickingquantityexception' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><label>" + name + "</label> ## WAVE# :<label>" + getWaveNo + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		// Case# 20148439 starts
		//html = html + "				<td align = 'left'>"+ st4 +"<label>" + getItemName + "</label>";
		//html = html + "				<td align = 'left'>"+ st4 +"<label>" + getItem + "</label>";
		// Case# 20148439 ends
		//case# 20148756 starts
		html = html + "				<td align = 'left'>"+ st4 +"<label>" + getItemName + "</label>";
		//case# 20148756 ends
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveNo + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + getBeginLocation + ">";
		//case# 20127170 starts (single quotes added to getItem,getItemName)
		html = html + "				<input type='hidden' name='hdnItem' value='" + getItem + "'>";
		html = html + "				<input type='hidden' name='hdnItemName' value='" + getItemName + "'>";
		//case# 20127170 end
		html = html + "				<input type='hidden' name='hdnItemDesc' value=" + getItemDesc + ">";
		html = html + "				<input type='hidden' name='hdnItemNo' value=" + getItemNo + ">";
		html = html + "				<input type='hidden' name='hdndoLoineId' value=" + getdoLoineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationName' value='" + getBeginLocationName + "'>";
		html = html + "				<input type='hidden' name='hdnEndLocationInternalId' value=" + getEndLocationInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEndLocation' value=" + getEndLocation + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + getClusterNo + ">";
		html = html + "				<input type='hidden' name='hdnBatchNo' value=" + getBatchNo + ">";
		html = html + "				<input type='hidden' name='hdnNoofRecords' value=" + getNoofRecords + ">";
		html = html + "				<input type='hidden' name='hdnNextLocation' value='" + getNextLocation + "'>";
		html = html + "				<input type='hidden' name='hdnname' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + getRecCount + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + getContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnEbizOrdNo' value=" + getEbizOrdNo + ">";
		html = html + "				<input type='hidden' name='hdnNextItemId' value=" + NextItemInternalId + ">";	
		html = html + "				<input type='hidden' name='hdnnumber' value=" + getnumber + ">";
		html = html + "				<input type='hidden' name='hdnRecType' value=" + getRecType + ">";
		html = html + "				<input type='hidden' name='hdnSerOut' value=" + getSerOut + ">";
		html = html + "				<input type='hidden' name='hdnSerIn' value=" + getSerIn + ">";
		html = html + "				<input type='hidden' name='hdnNextItemId' value=" + NextItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnebizzoneno' value=" + getZoneNo + ">";
		html = html + "				<input type='hidden' name='hdnflag'>";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdnNextexpectedqty' value=" + getNextExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnNextrecordid' value=" + NextRecordId + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				<input type='hidden' name='hdnitemtype' value=" + itemType + ">";
		html = html + "				<input type='hidden' name='hdnactlot' value=" + ActBatchno + ">";
		html = html + "				<input type='hidden' name='hdnexplot' value=" + ExpBatchno + ">";
		html = html + "				<input type='hidden' name='hdnfastpick' value=" + fastpick + ">";	
		html = html + "				<input type='hidden' name='hdnItemInstructions' value='" + ItemInstructions + "'>";
		html = html + "				<input type='hidden' name='hdnEntLoc' value=" + EntLoc + ">";
		html = html + "				<input type='hidden' name='hdnEntLocRec' value=" + EntLocRec + ">";
		html = html + "				<input type='hidden' name='hdnEntLocID' value=" + EntLocID + ">";
		html = html + "				<input type='hidden' name='hdnExceptionFlag' value=" + ExceptionFlag + ">";
		html = html + "				<input type='hidden' name='hdnserialscanflag' value=" + serialscanflag + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st6 +" ";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterquantity' id='enterquantity' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st7 +"";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterreason' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		//case# 20126988 start (enterremainqty commented)
		//html = html + "			<tr>";
		//html = html + "				<td align = 'left'>"+ st8 +"";
		//html = html + "				</td>";
		//html = html + "			</tr>";
		//html = html + "			<tr>";
		//html = html + "				<td align = 'left'><input name='enterremainqty' type='text'/>";
		//html = html + "				</td>";
		//html = html + "			</tr>";
		// case# 20126988 end 
		html = html + "			<tr>";
		//	html = html + "				<td align = 'left'>"+ st9 +"<input name='cmdConfirm' type='submit' value='F8' onclick='this.form.hdnflag.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "				<td align = 'left'>"+ st9 +"<input name='cmdConfirm' type='submit' value='ENT' onclick='this.form.hdnflag.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					"+ st10 +"<input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterquantity').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is not null');

		var fastpick = request.getParameter('hdnfastpick');

		nlapiLogExecution('DEBUG', 'fastpick', fastpick);

		var getLanguage = request.getParameter('hdngetLanguage');

		var st11,st12,st13;

		if( getLanguage == 'es_ES')
		{
			st11 = "CAJA NO V&#193;LIDO #";
			st12 = "RECOGIDA EN M&#218;LTIPLOS DE 2, ESTA SELECCI&#211;N ES PARTE DE LA PARTIDA KIT PACKAGE";
			st13 = "CANTIDADES RESTANTES Y REAL NO DEBE SER MAYOR DE LO ESPERADO CANTIDAD";
			st14 = "CANTIDAD REAL NO DEBE ESTAR EN BLANCO";
			st15 = "ELIGE CANTIDAD NO DEBE SER MAYOR QUE CANTIDAD PEDIDO";

		}
		else
		{
			st11 = "INVALID CARTON #";
			//st12 = "PICK IN MULTIPLES OF 2 AS THIS PICK IS PART OF KIT PACKAGE ITEM";
			st12 = "QUANTITY EXCEPTION IS NOT ALLOWED FOR KIT ITEMS";
			st13 = "REMAINING AND ACTUAL QUANTITIES SHOULD NOT BE GREATER THAN EXPECTED QTY";
			st14 = "ACTUAL QTY SHOULD NOT BE BLANK";
			st15 = "PICK QTY SHOULD NOT BE GREATER THAN ORDERED QTY";
			st16= "INVALID QUANTITY ";
		}


		// This variable is to hold the LP entered.
		var getEnteredQuantity = request.getParameter('enterquantity');
		nlapiLogExecution('DEBUG', 'getEnteredQuantity', getEnteredQuantity);

		var getEnteredReason = request.getParameter('enterreason');
		var vZoneId=request.getParameter('hdnebizzoneno');
		nlapiLogExecution('DEBUG', 'getEnteredReason', getEnteredReason);

		//var optedEvent = request.getParameter('cmdPrevious');

		var pickQtyExceptionarray = new Array();
		pickQtyExceptionarray["custparam_language"] = getLanguage;
		var WaveNo = request.getParameter('hdnWaveNo');
		var RecordInternalId = request.getParameter('hdnRecordInternalId');
		var ContainerLpNo = request.getParameter('hdnContainerLpNo');
		var FetchedQuantity = request.getParameter('hdnQuantity');
		var BeginLocation = request.getParameter('hdnBeginLocation');
		var Item = request.getParameter('hdnItem');
		var ItemName = request.getParameter('hdnItemName');
		var ItemDesc = request.getParameter('hdnItemDesc');
		var ItemNo = request.getParameter('hdnItemNo');
		var doLoineId = request.getParameter('hdndoLoineId');
		var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var BeginLocationName = request.getParameter('hdnBeginLocationName');
		var EndLocationInternalId = request.getParameter('hdnEndLocationInternalId');
		var EndLocation = request.getParameter('hdnEndLocation');
		var OrderLineNo = request.getParameter('hdnOrderLineNo');
		var ClusterNo = request.getParameter('hdnClusterNo');
		var BatchNo = request.getParameter('hdnBatchNo');
		var NoofRecords = request.getParameter('hdnNoofRecords');
		var NextLocation = request.getParameter('hdnNextLocation');
		var name = request.getParameter('hdnname');
		var RecCount = request.getParameter('hdnRecCount');
		var ContainerSize = request.getParameter('hdnContainerSize');
		var EbizOrdNo = request.getParameter('hdnEbizOrdNo');
		var NextItemInternalId=request.getParameter('hdnNextItemId');
		var NextrecordID=request.getParameter('hdnNextrecordid');
		var ItemType=request.getParameter('hdnitemtype');
		var serailscanflag=request.getParameter('hdnserialscanflag');
		

		var number = request.getParameter('hdnnumber');
		var RecType = request.getParameter('hdnRecType');
		var SerOut = request.getParameter('hdnSerOut');
		var SerIn = request.getParameter('hdnSerIn');
		var remainqty = request.getParameter('enterremainqty');
		if(remainqty==null || remainqty=='')
			remainqty=null;
		var vSite = request.getParameter('hdnwhlocation');
		pickQtyExceptionarray["custparam_serialscanflag"] = request.getParameter('hdnserialscanflag');
		pickQtyExceptionarray["custparam_screenno"] = '11A';
		pickQtyExceptionarray["custparam_nextiteminternalid"] = request.getParameter('hdnNextItemId');
		pickQtyExceptionarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		pickQtyExceptionarray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
		pickQtyExceptionarray["custparam_fastpick"] = fastpick;

		nlapiLogExecution('DEBUG', 'pickQtyExceptionarray[custparam_serialscanflag]', pickQtyExceptionarray["custparam_serialscanflag"]);
		nlapiLogExecution('DEBUG', 'getEnteredQuantity', getEnteredQuantity);
		nlapiLogExecution('DEBUG', 'getFetchedQuantity', FetchedQuantity);
		nlapiLogExecution('DEBUG', 'WaveNo', WaveNo);
		nlapiLogExecution('DEBUG', 'RecordInternalId', RecordInternalId);
		nlapiLogExecution('DEBUG', 'ContainerLpNo', ContainerLpNo);
		nlapiLogExecution('DEBUG', 'FetchedQuantity', FetchedQuantity);
		nlapiLogExecution('DEBUG', 'BeginLocation', BeginLocation);
		nlapiLogExecution('DEBUG', 'Item', Item);
		nlapiLogExecution('DEBUG', 'ItemName', ItemName);
		nlapiLogExecution('DEBUG', 'ItemDesc', ItemDesc);
		nlapiLogExecution('DEBUG', 'ItemNo', ItemNo);
		nlapiLogExecution('DEBUG', 'doLoineId', doLoineId);
		nlapiLogExecution('DEBUG', 'InvoiceRefNo', InvoiceRefNo);
		nlapiLogExecution('DEBUG', 'BeginLocationName', BeginLocationName);
		nlapiLogExecution('DEBUG', 'EndLocationInternalId', EndLocationInternalId);
		nlapiLogExecution('DEBUG', 'EndLocation', EndLocation);
		nlapiLogExecution('DEBUG', 'OrderLineNo', OrderLineNo);
		nlapiLogExecution('DEBUG', 'ClusterNo', ClusterNo);
		nlapiLogExecution('DEBUG', 'BatchNo', BatchNo);
		nlapiLogExecution('DEBUG', 'NoofRecords', NoofRecords);
		nlapiLogExecution('DEBUG', 'NextLocation', NextLocation);
		nlapiLogExecution('DEBUG', 'name', name);
		nlapiLogExecution('DEBUG', 'ContainerSize', ContainerSize);
		nlapiLogExecution('DEBUG', 'EbizOrdNo', EbizOrdNo);
		nlapiLogExecution('DEBUG', 'number', number);
		nlapiLogExecution('DEBUG', 'RecType', RecType);
		nlapiLogExecution('DEBUG', 'SerOut', SerOut);
		nlapiLogExecution('DEBUG', 'SerIn', SerIn);


		//pickQtyExceptionarray["custparam_enteredqty"] = getEnteredQuantity;
		pickQtyExceptionarray["custparam_enteredReason"] = getEnteredReason;
		// case #:20123709 start
		//pickQtyExceptionarray["custparam_enteredQty"] = getEnteredQuantity;
		// end

		//pickQtyExceptionarray["custparam_fetchedQty"] = FetchedQuantity;

		pickQtyExceptionarray["custparam_waveno"] = WaveNo;
		pickQtyExceptionarray["custparam_recordinternalid"] = RecordInternalId;
		pickQtyExceptionarray["custparam_containerlpno"] = ContainerLpNo;
		pickQtyExceptionarray["custparam_expectedquantity"] = FetchedQuantity;
		pickQtyExceptionarray["custparam_beginLocation"] = BeginLocation;
		pickQtyExceptionarray["custparam_item"] = Item;
		pickQtyExceptionarray["custparam_itemname"] = ItemName;
		pickQtyExceptionarray["custparam_itemdescription"] = ItemDesc;
		pickQtyExceptionarray["custparam_iteminternalid"] = ItemNo;
		pickQtyExceptionarray["custparam_dolineid"] = doLoineId;
		pickQtyExceptionarray["custparam_invoicerefno"] = InvoiceRefNo;
		pickQtyExceptionarray["custparam_beginlocationname"] = BeginLocationName;
		pickQtyExceptionarray["custparam_beginLocationname"] = BeginLocationName;
		pickQtyExceptionarray["custparam_endlocinternalid"] = EndLocationInternalId;
		pickQtyExceptionarray["custparam_endlocation"] = EndLocation;
		pickQtyExceptionarray["custparam_orderlineno"] = OrderLineNo;
		pickQtyExceptionarray["custparam_clusterno"] = ClusterNo;
		pickQtyExceptionarray["custparam_batchno"] = BatchNo;
		pickQtyExceptionarray["custparam_noofrecords"] = NoofRecords;
		pickQtyExceptionarray["custparam_nextlocation"] = NextLocation;
		pickQtyExceptionarray["name"] = name;
		pickQtyExceptionarray["custparam_containersize"] = ContainerSize;
		pickQtyExceptionarray["custparam_ebizordno"] = EbizOrdNo;
		pickQtyExceptionarray["custparam_number"] = number;
		pickQtyExceptionarray["custparam_RecType"] = RecType;
		pickQtyExceptionarray["custparam_SerOut"] = SerOut;
		pickQtyExceptionarray["custparam_SerIn"] = SerIn;
		pickQtyExceptionarray["custparam_nextiteminternalid"] = request.getParameter('hdnNextItemId');
		pickQtyExceptionarray["custparam_nextexpectedquantity"]=request.getParameter('hdnNextexpectedqty');
		pickQtyExceptionarray["custparam_nextrecordid"]=request.getParameter('hdnNextrecordid');
		pickQtyExceptionarray["custparam_itemType"] = ItemType;
		pickQtyExceptionarray["custparam_Actbatchno"]=request.getParameter('hdnactlot');
		pickQtyExceptionarray["custparam_Expbatchno"] = request.getParameter('hdnexplot');

		pickQtyExceptionarray["custparam_EntLoc"]=request.getParameter('hdnEntLoc');
		pickQtyExceptionarray["custparam_EntLocRec"] = request.getParameter('hdnEntLocRec');
		pickQtyExceptionarray["custparam_EntLocId"]=request.getParameter('hdnEntLocID');
		pickQtyExceptionarray["custparam_Exc"] = request.getParameter('hdnExceptionFlag');

		var nextexpqty = request.getParameter('hdnNextexpectedqty');

		if(vZoneId!=null && vZoneId!="")
		{
			pickQtyExceptionarray["custparam_ebizzoneno"] =  request.getParameter('hdnebizzoneno');
		}
		else
			pickQtyExceptionarray["custparam_ebizzoneno"] = '';

		var vSkipId=request.getParameter('hdnskipid');
		pickQtyExceptionarray["custparam_skipid"] = request.getParameter('hdnskipid');		
		pickQtyExceptionarray["custparam_remqty"] = remainqty;


		nlapiLogExecution('DEBUG', 'hdnNextItemId', request.getParameter('hdnNextItemId'));

		var vkititem = '';
		var vkititemtext = '';

		if((EbizOrdNo !=null && EbizOrdNo!='') ||(ContainerLpNo!=null && ContainerLpNo !=''))
		{
			var filters = new Array(); 			 
			filters.push(new nlobjSearchFilter('custrecord_sku', null, 'is', ItemNo));	
			if(EbizOrdNo !=null && EbizOrdNo!='')
				filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', EbizOrdNo));	
			if(ContainerLpNo!=null && ContainerLpNo !='')
				filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', ContainerLpNo));

			var columns1 = new Array(); 
			columns1[0] = new nlobjSearchColumn('custrecord_parent_sku_no' ); 

			var searchresultskititem = nlapiSearchRecord( 'customrecord_ebiznet_trn_opentask', null, filters, columns1 ); 
			if(searchresultskititem!=null && searchresultskititem!='')
			{
				vkititem = searchresultskititem[0].getValue('custrecord_parent_sku_no');
				vkititemtext = searchresultskititem[0].getText('custrecord_parent_sku_no');
			}
		}
		var kititemTypesku = '';

		if(vkititem!=null && vkititem!='')
		{
			kititemTypesku = nlapiLookupField('item', vkititem, 'recordType');
		}

		nlapiLogExecution('DEBUG', 'kititemTypesku', kititemTypesku);

		if(kititemTypesku=='kititem')
		{
			//removed as on 051113 by suman to increase the performance.
			/*var searchresultsitem = nlapiLoadRecord(kititemTypesku, vkititem);
			var SkuNo=searchresultsitem.getFieldValue('itemid');

			var kitfilters = new Array(); 			 
			kitfilters[0] = new nlobjSearchFilter('itemid', null, 'is', SkuNo);	*/

			var kitfilters = new Array(); 			 
			kitfilters[0] = new nlobjSearchFilter("internalid",null,"anyof",vkititem);
			//end of code as on 051113
			var kitcolumns1 = new Array(); 
			kitcolumns1[0] = new nlobjSearchColumn( 'memberitem' ); 			
			kitcolumns1[1] = new nlobjSearchColumn( 'memberquantity' );

			var searchresults = nlapiSearchRecord( 'item', null, kitfilters, kitcolumns1 );
			if(searchresults!=null && searchresults!='')
			{

				for(var w=0; w<searchresults.length;w++) 
				{
					fulfilmentItem = searchresults[w].getValue('memberitem');
					memberitemqty = searchresults[w].getValue('memberquantity');

					if(fulfilmentItem==ItemNo)
					{
						var kitqty=Math.floor(getEnteredQuantity/memberitemqty)*memberitemqty;

						//if((getEnteredQuantity!=kitqty) && (parseFloat(getEnteredQuantity) < parseFloat(FetchedQuantity)))
						if(parseFloat(getEnteredQuantity) < parseFloat(FetchedQuantity))
						{	
							
							nlapiLogExecution('DEBUG', 'inside exception', kitqty);
							//pickQtyExceptionarray["custparam_screenno"] = '11A';
							pickQtyExceptionarray["custparam_enteredQty"] = FetchedQuantity;
							pickQtyExceptionarray["custparam_screenno"] = '13NEW';
							pickQtyExceptionarray["custparam_error"] = st12;//'Pick in multiples of 2 as this pick is part of Kit Package item';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, pickQtyExceptionarray);
							nlapiLogExecution('DEBUG', 'Error: ', 'Pick in multiples of 2 as this pick is part of Kit Package item');
							return;

						}

					}
				}
			}	

		}
		if (sessionobj!=context.getUser()) {
			try
			{

				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}

//				if the previous button 'F7' is clicked, it has to go to the previous screen 
				if (request.getParameter('cmdPrevious') == 'F7') {
					nlapiLogExecution('DEBUG', 'Clicked on Previous', request.getParameter('cmdPrevious'));
					if(fastpick!='Y')
					{
						response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, pickQtyExceptionarray);
					}
					else
					{
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pickingcarton', 'customdeploy_ebiz_rf_pickingcarton', false, pickQtyExceptionarray);
					}
					return;
				}
				else 
				{
					nlapiLogExecution('DEBUG', 'Clicked on Confirm', request.getParameter('cmdConfirm'));
					pickQtyExceptionarray["custparam_enteredqty"] = getEnteredQuantity;
					pickQtyExceptionarray["custparam_enteredQty"] = getEnteredQuantity;
					
					//if (request.getParameter('hdnflag') == 'F8') {
					if(getEnteredQuantity==null || getEnteredQuantity=='')
					{
						pickQtyExceptionarray["custparam_error"] = st14;//'Actual Qty should not be blank';
						pickQtyExceptionarray["custparam_fastpick"] = fastpick;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, pickQtyExceptionarray);
						nlapiLogExecution('DEBUG', 'Error: ', 'Actual Qty should not be blank');
						return;
					}
					else if(isNaN(getEnteredQuantity))
					{
					pickQtyExceptionarray["custparam_error"] = st16;//'INVALID QUANTITY';
					pickQtyExceptionarray["custparam_fastpick"] = fastpick;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, pickQtyExceptionarray);
					nlapiLogExecution('DEBUG', 'Error: ', 'Actual Qty should not be blank');
					return;
					}
					if(getEnteredQuantity!=null && getEnteredQuantity!='' && getEnteredQuantity<0)
					{
						pickQtyExceptionarray["custparam_error"] = 'Actual Qty Should Be Greater Than Or Equal To Zero';
						pickQtyExceptionarray["custparam_fastpick"] = fastpick;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, pickQtyExceptionarray);
						nlapiLogExecution('DEBUG', 'Error: ', 'Actual Qty should not be blank');
						return;
					}
					if(getEnteredQuantity==null || getEnteredQuantity=='' || getEnteredQuantity==' ')
						getEnteredQuantity=0;

					nlapiLogExecution('DEBUG', 'getEnteredQuantity after validate', getEnteredQuantity);

					//The following variable should be 'Y' for Dynacraft.
					var vAutoContainerFlag='N';
					pickQtyExceptionarray["custparam_autocont"] = vAutoContainerFlag;
					nlapiLogExecution('DEBUG', 'vAutoContainerFlag', vAutoContainerFlag);
					if(vAutoContainerFlag=='Y' || parseFloat(getEnteredQuantity)==0)
					{
						if(getEnteredQuantity==null || getEnteredQuantity=='')
						{
							pickQtyExceptionarray["custparam_error"] = st14;//'Actual Qty should not be blank';
							pickQtyExceptionarray["custparam_fastpick"] = fastpick;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, pickQtyExceptionarray);
							nlapiLogExecution('DEBUG', 'Error: ', 'Actual Qty should not be blank');
							return;
						}
						
						

						//case # 20126423 start

						if(isNaN(remainqty))
							remainqty=0;

						if(parseFloat(getEnteredQuantity) + parseFloat(remainqty) > parseFloat(FetchedQuantity))
						{	
							pickQtyExceptionarray["custparam_error"] = "SUM OF ENT QTY AND REM QTY EXCEEDS PICK QTY";//'Pick Qty should not be greater than Ordered Qty';
							pickQtyExceptionarray["custparam_fastpick"] = fastpick;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, pickQtyExceptionarray);
							nlapiLogExecution('DEBUG', 'Error: ', 'Pick Qty should not be greater than Ordered Qty');
							return;
						}

						//End

						if(parseFloat(getEnteredQuantity) > parseFloat(FetchedQuantity))
						{				

							pickQtyExceptionarray["custparam_error"] = st15;//'Pick Qty should not be greater than Ordered Qty';
							pickQtyExceptionarray["custparam_fastpick"] = fastpick;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, pickQtyExceptionarray);
							nlapiLogExecution('DEBUG', 'Error: ', 'Pick Qty should not be greater than Ordered Qty');
							return;
						}
						var RcId=RecordInternalId;
						var EndLocation = getEndLocationInternalId;
						var TotalWeight=0;
						var getReason=request.getParameter('custparam_enteredReason');
						var SORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordInternalId);
						var getContainerLPNo= SORec.getFieldValue('custrecord_container_lp_no');
						getLPContainerSize= SORec.getFieldText('custrecord_container');
						var pickMethod=SORec.getFieldValue('custrecord_ebizmethod_no');
						if(pickMethod!=null && pickMethod!='' && pickMethod!='null')
						{
							pickMethod=parseInt(pickMethod);
						}
						pickQtyExceptionarray["custparam_newcontainerlp"] = getContainerLPNo;
						var vPickType = request.getParameter('hdnpicktype');
						var vBatchno = request.getParameter('hdnbatchno');
						var PickQty = FetchedQuantity;
						var vActqty = getEnteredQuantity;
						var SalesOrderInternalId = EbizOrdNo;
						var getContainerSize = getLPContainerSize;
						var  vdono = doLoineId;
						nlapiLogExecution('DEBUG', 'WaveNo', WaveNo);
						nlapiLogExecution('DEBUG', 'getContainerLPNo', getContainerLPNo);
						var opentaskcount=0;

						nlapiLogExecution('DEBUG', 'opentaskcount', WaveNo);
						nlapiLogExecution('DEBUG', 'opentaskcount', getContainerLPNo);
						nlapiLogExecution('DEBUG', 'opentaskcount', vPickType);
						nlapiLogExecution('DEBUG', 'opentaskcount', SalesOrderInternalId);
						nlapiLogExecution('DEBUG', 'opentaskcount', vdono);
						nlapiLogExecution('DEBUG', 'ordName', name);
						nlapiLogExecution('DEBUG', 'vZoneId', vZoneId);
						opentaskcount=getOpenTasksCount(WaveNo,getContainerLPNo,vPickType,SalesOrderInternalId,name,vZoneId);
						//20126286

						var opentaskcountNav=getOpenTasksCountForNavigation(WaveNo,getContainerLPNo,vPickType,SalesOrderInternalId,name,vZoneId,vSite,pickMethod)
						nlapiLogExecution('DEBUG', 'opentaskcount', opentaskcount);
						nlapiLogExecution('DEBUG', 'opentaskcountNav', opentaskcountNav);
						//For fine tuning pick confirmation and doing autopacking in user event after last item
						var IsitLastPick='F';
						if(opentaskcount > parseFloat(vSkipId) + 1)
							IsitLastPick='F';
						else
							IsitLastPick='T';

						nlapiLogExecution('DEBUG', 'getItem', ItemNo);
						nlapiLogExecution('DEBUG', 'vSite', vSite);
						var Systemrules = SystemRuleForStockAdjustment(vSite);
						nlapiLogExecution('ERROR', 'Systemrules', Systemrules);
						if(Systemrules == null || Systemrules =='' || Systemrules.length==0)
							Systemrules=null;

						ConfirmPickTask(ItemNo,getContainerSize,vdono,RcId,vActqty,getContainerLPNo,EndLocation,getReason,PickQty,vBatchno,
								IsitLastPick,remainqty,kititemTypesku,SalesOrderInternalId,vkititem,Systemrules);
						//if(RecCount > 1)
						//if(opentaskcount > parseFloat(vSkipId) + 1)
						if(opentaskcountNav >  1)
						{
							nlapiLogExecution('DEBUG', 'vPickType', vPickType);
							var SOFilters = new Array();
							SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

							if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && WaveNo != null && WaveNo != "")
							{
								nlapiLogExecution('DEBUG', 'WaveNo inside If', WaveNo);
								SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
							}

							if(ClusterNo!= null && ClusterNo!="" && ClusterNo!= "null")
							{
								nlapiLogExecution('DEBUG', 'ClusNo inside If', ClusterNo);

								SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusterNo));
							}

							if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && name!=null && name!="" && name!= "null")
							{
								nlapiLogExecution('DEBUG', 'OrdNo inside If', name);

								SOFilters.push(new nlobjSearchFilter('name', null, 'is', name));
							}

							if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && SalesOrderInternalId!=null && SalesOrderInternalId!="" && SalesOrderInternalId!= "null")
							{
								nlapiLogExecution('DEBUG', 'SO Id inside If', SalesOrderInternalId);
								SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId));
							}

							if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!="null")
							{
								nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
								SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));
								//SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', vZoneId));
							}
							var SOColumns = new Array();
							SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
//							SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
							//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
							SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
							//Code end as on 290414
							SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
							SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
							SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
							SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
							SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
							SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
							SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
							SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
							SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
							SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
							SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
							SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
							SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
							SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
							SOColumns.push(new nlobjSearchColumn('name'));
							SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
							SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
							SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
							SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
							SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));

							SOColumns[0].setSort();
							SOColumns[1].setSort();
							SOColumns[2].setSort();
							SOColumns[3].setSort();
							SOColumns[4].setSort();
							SOColumns[5].setSort(true);

							var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
							nlapiLogExecution('DEBUG', 'Results', SOSearchResults);
							if (SOSearchResults != null && SOSearchResults.length > 0) 
							{
								vSkipId=0;

								if(SOSearchResults.length <= parseFloat(vSkipId))
								{
									vSkipId=0;
								}
								var SOSearchResult = SOSearchResults[vSkipId];
								if(SOSearchResults.length > parseFloat(vSkipId) + 1)
								{
									var SOSearchnextResult = SOSearchResults[parseFloat(vSkipId) + 1];
									pickQtyExceptionarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
									NextLocation = SOSearchnextResult.getText('custrecord_actbeginloc');
									pickQtyExceptionarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
									nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
								}
								pickQtyExceptionarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
								pickQtyExceptionarray["custparam_recordinternalid"] = SOSearchResult.getId();
								//pickQtyExceptionarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
								pickQtyExceptionarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
								pickQtyExceptionarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
								pickQtyExceptionarray["custparam_beginLocation"] = SOSearchResult.getText('custrecord_actbeginloc');
								pickQtyExceptionarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
								pickQtyExceptionarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
								pickQtyExceptionarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
								pickQtyExceptionarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
								pickQtyExceptionarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
								pickQtyExceptionarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
								pickQtyExceptionarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
								pickQtyExceptionarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
								pickQtyExceptionarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
								pickQtyExceptionarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
								pickQtyExceptionarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
								pickQtyExceptionarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
								pickQtyExceptionarray["custparam_noofrecords"] = SOSearchResults.length;		
								pickQtyExceptionarray["name"] =  SOSearchResult.getValue('name');
								pickQtyExceptionarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
								pickQtyExceptionarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
								pickQtyExceptionarray["custparam_fastpick"] = fastpick;
								if(vZoneId!=null && vZoneId!="")
								{
									pickQtyExceptionarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
								}
								else
									pickQtyExceptionarray["custparam_ebizzoneno"] = '';

							}
							if(BeginLocation != NextLocation)
							{ 
								pickQtyExceptionarray["custparam_beginLocationname"]=null;
								pickQtyExceptionarray["custparam_iteminternalid"]=null;
								pickQtyExceptionarray["custparam_expectedquantity"]=null;
								pickQtyExceptionarray["custparam_remqty"]=null;

								nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Location');
								if(fastpick!='Y')
								{
									response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, pickQtyExceptionarray);
								}
								else
								{						
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, pickQtyExceptionarray);
								}
								return;					
							}
							else if(ItemNo != NextItemInternalId)
							{ 
								pickQtyExceptionarray["custparam_iteminternalid"] = NextItemInternalId;
								pickQtyExceptionarray["custparam_beginLocationname"]=null;
								pickQtyExceptionarray["custparam_expectedquantity"]=null;
								pickQtyExceptionarray["custparam_remqty"]=null;
								nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Item');

								if(fastpick!='Y')
								{
									response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, pickQtyExceptionarray);
								}
								else
								{						
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, pickQtyExceptionarray);
								}
								return;
							}
							else if(ItemNo == NextItemInternalId)
							{ 
								pickQtyExceptionarray["custparam_iteminternalid"] = NextItemInternalId;
								pickQtyExceptionarray["custparam_beginLocationname"]=null;
								pickQtyExceptionarray["custparam_expectedquantity"]=nextexpqty;
								pickQtyExceptionarray["custparam_remqty"]=null;

								nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Qty Confirm');

								if(fastpick!='Y')
								{
									response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, pickQtyExceptionarray);
								}
								else
								{						
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, pickQtyExceptionarray);
								}
							}
						}
						else{

							nlapiLogExecution('DEBUG', 'Before AutoPacking 1 : Sales Order InternalId', SalesOrderInternalId);
							pickQtyExceptionarray["custparam_fastpick"] = fastpick;
							//For fine tuning pick confirmation and doing autopacking in user event after last item
							//AutoPacking(SalesOrderInternalId);
							/*nlapiLogExecution('DEBUG', 'Navigating To1', 'Foot print');
						response.sendRedirect('SUITELET', 'customscript_rf_picking_footprint', 'customdeploy_rf_picking_footprint_di', false, pickQtyExceptionarray);*/

							//nlapiLogExecution('DEBUG', 'Before AutoPacking 1 : Sales Order InternalId', SalesOrderInternalId);
							//For fine tuning pick confirmation and doing autopacking in user event after last item
							//AutoPacking(SalesOrderInternalId);
							nlapiLogExecution('DEBUG', 'Navigating To1', 'Stage Location');
							response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, pickQtyExceptionarray);					
						}

					}
					else
					{
						if(isNaN(getEnteredQuantity))
							getEnteredQuantity=0;
						//case # 20126423 start
						if(isNaN(remainqty))
							remainqty=0;

						if(parseFloat(getEnteredQuantity) + parseFloat(remainqty) > parseFloat(FetchedQuantity))
						{	
							pickQtyExceptionarray["custparam_error"] = "SUM OF ENT QTY AND REM QTY EXCEEDS PICK QTY";//'Pick Qty should not be greater than Ordered Qty';
							pickQtyExceptionarray["custparam_fastpick"] = fastpick;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, pickQtyExceptionarray);
							nlapiLogExecution('DEBUG', 'Error: ', 'Pick Qty should not be greater than Ordered Qty');
							return;
						}

						//End

						if(parseFloat(getEnteredQuantity) > parseFloat(FetchedQuantity))
						{				
							pickQtyExceptionarray["custparam_fastpick"] = fastpick;
							pickQtyExceptionarray["custparam_error"] = st15;//'Pick Qty should not be greater than Ordered Qty';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, pickQtyExceptionarray);
							nlapiLogExecution('DEBUG', 'Error: ', 'Pick Qty should not be greater than Ordered Qty');
							return;
						}

						nlapiLogExecution('DEBUG', 'Error: ', ItemNo);
						if(ItemNo!=null && ItemNo !=''){	

							var itemSubtype = nlapiLookupField('item', ItemNo, ['recordType', 'custitem_ebizserialin','custitem_ebizserialout', 'custitem_ebizbatchlot']);

							if (itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.custitem_ebizserialout == 'T') {
								pickQtyExceptionarray["custparam_number"] = 0;
								pickQtyExceptionarray["custparam_RecType"] = itemSubtype.recordType;
								pickQtyExceptionarray["custparam_SerOut"] = itemSubtype.custitem_ebizserialout;
								pickQtyExceptionarray["custparam_SerIn"] = itemSubtype.custitem_ebizserialin;
								//pickQtyExceptionarray["custparam_expectedquantity"] = parseInt(getEnteredQuantity);
								//pickQtyExceptionarray["custparam_enteredQty"] = parseInt(FetchedQuantity);
								if(fastpick!='Y')
								{
									response.sendRedirect('SUITELET', 'customscript_rf_pick_serialscan', 'customdeploy_rf_pick_serialscan_di', false, pickQtyExceptionarray);
								}
								else{
									response.sendRedirect('SUITELET', 'customscript_rf_fastpick_serialscan', 'customdeploy_rf_fastpick_serialscan_di', false, pickQtyExceptionarray);
								}
								return;
							}
							else
							{
								response.sendRedirect('SUITELET', 'customscript_rf_picking_container_no', 'customdeploy_rf_picking_container_no_di', false, pickQtyExceptionarray);
							}
						}
						pickQtyExceptionarray["custparam_fastpick"] = fastpick;
						nlapiLogExecution('DEBUG', 'Recordinternalid', pickQtyExceptionarray["custparam_recordinternalid"]);
						if(fastpick!='Y')
						{
							response.sendRedirect('SUITELET', 'customscript_rf_picking_container_no', 'customdeploy_rf_picking_container_no_di', false, pickQtyExceptionarray);
						}
						else
						{
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pickingcarton', 'customdeploy_ebiz_rf_pickingcarton', false, pickQtyExceptionarray);
						}
					}
				}
			}
			catch (e)  {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, pickQtyExceptionarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');

			}
		}
		else
		{
			SOarray["custparam_screenno"] = '12';
			SOarray["custparam_error"] = 'CONTAINER# ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, pickQtyExceptionarray);
		}
	}
}

function getOpenTasksCount(waveno,containerlpno,vPickType,SalesOrderInternalId,OrdName,vZoneId)
{
	nlapiLogExecution('DEBUG', 'Into getOpenTasksCount...');

	var str = 'waveno. = ' + waveno + '<br>';
	str = str + 'containerlpno. = ' + containerlpno + '<br>';	
	str = str + 'vPickType. = ' + vPickType + '<br>';	
	str = str + 'SalesOrderInternalId. = ' + SalesOrderInternalId + '<br>';
	str = str + 'OrdName. = ' + OrdName + '<br>';
	str = str + 'vZoneId. = ' + vZoneId + '<br>';

	nlapiLogExecution('DEBUG', 'Function Parameters', str);
	var openreccount=0;
	var SOFilters = new Array();
	var SOColumns = new Array();

	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK	 
	if(containerlpno !=null && containerlpno !='' && containerlpno !='null')
		SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', containerlpno));	

	nlapiLogExecution('DEBUG', 'vPickType', vPickType);
	if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && waveno != null && waveno != "")
	{
		nlapiLogExecution('DEBUG', 'WaveNo inside If', waveno);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(waveno)));
	}

	if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
	{
		nlapiLogExecution('DEBUG', 'OrdNo inside If', OrdName);

		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
	}

	if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && SalesOrderInternalId!=null && SalesOrderInternalId!="" && SalesOrderInternalId!= "null")
	{
		nlapiLogExecution('DEBUG', 'SO Id inside If', SalesOrderInternalId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId));
	}

	if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!="null")
	{
		nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));
		//SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', vZoneId));
	}
	if((vPickType.indexOf('C') != -1 || vPickType=='ALL') && containerlpno!=null && containerlpno!="" && containerlpno!= "null")
	{
		nlapiLogExecution('DEBUG', 'Carton # inside If', containerlpno);
		SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', containerlpno));			
	}

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	if(SOSearchResults!=null && SOSearchResults!=''&& SOSearchResults.length>0)
		openreccount=SOSearchResults.length;	

	nlapiLogExecution('DEBUG', 'Out of getOpenTasksCount...',openreccount);

	return openreccount;

}

function ConfirmPickTask(ItemInternalId,getContainerSize,vdono,RcId,vActqty,getContainerLPNo,EndLocation,getReason,PickQty,
		vBatchno,IsitLastPick,remainqty,kititemTypesku,SalesOrderInternalId,vkititem,Systemrules)
{
	var arrDims = getSKUCubeAndWeightforconfirm(ItemInternalId, 1);
	var itemCube = 0;
	var itemWeight=0;	
	var TotalWeight=0;
	if (arrDims[0] != "" && (!isNaN(arrDims[1]))){
		//	these are commented because here  PickQty is excepted qty but we have to cal based on act qty
		itemCube = (parseFloat(PickQty) * parseFloat(arrDims[0]));
		itemWeight = (parseFloat(PickQty) * parseFloat(arrDims[1]));//	


	} 
	nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemCube', itemCube);
	nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemweight', itemWeight);
	nlapiLogExecution('DEBUG', 'ContainerSize:ContainerSize', getContainerSize);		


	var ContainerCube;					
	var containerInternalId;
	var ContainerSize;
	var vRemaningqty=0;
	nlapiLogExecution('DEBUG', 'getContainerSize', getContainerSize);	

	if(getContainerSize=="" || getContainerSize==null)
	{
		getContainerSize=ContainerSize;
		nlapiLogExecution('DEBUG', 'ContainerSize', ContainerSize);	
	}
	nlapiLogExecution('DEBUG', 'getContainerSize', getContainerSize);	
	var arrContainerDetails = getContainerCubeAndTarWeight("",getContainerSize);					
	if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) {

		ContainerCube =  parseFloat(arrContainerDetails[0]);						
		containerInternalId = arrContainerDetails[3];
		ContainerSize=arrContainerDetails[4];
		TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));//now added
	} 
	nlapiLogExecution('DEBUG', 'ContainerSizeInternalId', containerInternalId);	
	nlapiLogExecution('DEBUG', 'ContainerSize', ContainerSize);	

	//UpdateOpenTask,fulfillmentorder
	nlapiLogExecution('DEBUG', 'vdono', vdono);	

	UpdateRFOpenTask(RcId,vActqty, containerInternalId,getContainerLPNo,itemCube,itemWeight,EndLocation,
			getReason,PickQty,IsitLastPick,remainqty,SalesOrderInternalId,Systemrules);

	var SORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RcId);
	vdono= SORec.getFieldValue('custrecord_ebiz_cntrl_no');

	UpdateRFFulfillOrdLine(vdono,vActqty);

	//create new record in opentask,fullfillordline when we have qty exception
	//vRemaningqty = vDisplayedQty-vActqty;
	vRemaningqty = PickQty-vActqty;
	var recordcount=1;//it is iterator in GUI
	nlapiLogExecution('DEBUG', 'vDisplayedQty', PickQty);	
	nlapiLogExecution('DEBUG', 'vActqty', vActqty);	
	if(isNaN(PickQty))
		PickQty=0;

	if(isNaN(vActqty))
		vActqty=0;

	if(isNaN(remainqty))
		remainqty=0;

	if(PickQty != vActqty)
	{
		if(kititemTypesku=='kititem')
		{
			nlapiLogExecution('DEBUG', 'vsalesordInternid', SalesOrderInternalId);

			var filters = new Array(); 			 
			filters[0] = new nlobjSearchFilter('custrecord_parent_sku_no', null, 'anyof', vkititem);	
			filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]);	
			filters[2] = new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', SalesOrderInternalId);	

			var columns1 = new Array(); 
			columns1[0] = new nlobjSearchColumn( 'custrecord_expe_qty' ); 			
			columns1[1] = new nlobjSearchColumn( 'custrecord_act_qty' );

			var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, null ); 
			for(var z=0; z<searchresults.length;z++) 
			{										
				var kitrcid=searchresults[z].getId();
				var Expqty=searchresults[z].getValue('custrecord_expe_qty');
				var Actqty=searchresults[z].getValue('custrecord_act_qty');


				if(parseFloat(Expqty) != parseFloat(Actqty))
				{
					var kitopentask = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', Recid);
					kitopentask.setFieldValue('custrecord_kit_exception_flag', 'T');
					nlapiSubmitRecord(kitopentask, false, true);
				}
			}

		}
		nlapiLogExecution('DEBUG', 'inexception condition1', 'inexception condition');	
		//Case# 20148322 starts (request parameter passing)
		//CreateNewShortPick(RcId,PickQty,vActqty,vdono,recordcount,getContainerLPNo,TotalWeight,vBatchno,remainqty,Systemrules);//have to check for wt and cube calculation 
		CreateNewShortPick(RcId,PickQty,vActqty,vdono,recordcount,getContainerLPNo,TotalWeight,vBatchno,remainqty,Systemrules,request);//have to check for wt and cube calculation 
		//Case# 20148322 ends
	}
}

/**
 * 
 * @param RcId
 * @param PickQty
 * @param containerInternalId
 * @param getEnteredContainerNo
 * @param itemCube
 * @param itemWeight
 * @param EndLocation
 */
function UpdateRFOpenTask(RcId, actPickQty, containerInternalId, getEnteredContainerNo, itemCube,
		itemWeight, EndLocation,vReason,expPickQty,IsItLastPick,remainqty,SalesOrderInternalId,Systemrules)
{
	nlapiLogExecution('DEBUG', 'into UpdateRFOpenTask containerlp: ', getEnteredContainerNo);
	var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RcId);
	var vinvrefno=transaction.getFieldValue('custrecord_invref_no');
	//Case # 20126289 Start
	var itemno =transaction.getFieldValue('custrecord_sku');
	//Case # 20126289 End.
	transaction.setFieldValue('custrecord_wms_status_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')
	if(actPickQty!= null && actPickQty !="")
		transaction.setFieldValue('custrecord_act_qty', parseFloat(actPickQty).toFixed(4));
	//transaction.setFieldValue('custrecord_expe_qty', PickQty);
	if(containerInternalId!= null && containerInternalId !="")
		transaction.setFieldValue('custrecord_container', containerInternalId);
	if(EndLocation!=null && EndLocation!="")
		transaction.setFieldValue('custrecord_actendloc', EndLocation);	
	if(itemWeight!=null && itemWeight!="")
		transaction.setFieldValue('custrecord_total_weight', parseFloat(itemWeight).toFixed(4));
	if(itemCube!=null && itemCube!="")
		transaction.setFieldValue('custrecord_totalcube', parseFloat(itemCube).toFixed(4));	
	if(getEnteredContainerNo!=null && getEnteredContainerNo!="")
		transaction.setFieldValue('custrecord_container_lp_no', getEnteredContainerNo);
	transaction.setFieldValue('custrecord_act_end_date', DateStamp());
	transaction.setFieldValue('custrecord_actualendtime',TimeStamp());
	if(vReason!=null && vReason!="")
		transaction.setFieldValue('custrecord_notes', vReason);	
	var currentContext = nlapiGetContext();  
	var currentUserID = currentContext.getUser();
	transaction.setFieldValue('custrecord_upd_ebiz_user_no',currentUserID);
	nlapiLogExecution('DEBUG', 'IsItLastPick: ', IsItLastPick);
	transaction.setFieldValue('custrecord_device_upload_flag',IsItLastPick);
	var vemployee = request.getParameter('custpage_employee');
	nlapiLogExecution('DEBUG', 'vemployee :', vemployee);
	nlapiLogExecution('DEBUG', 'currentUserID :', currentUserID);
//	if (vemployee != null && vemployee != "") 
//	{
//	transaction.setFieldValue('custrecord_taskassignedto',vemployee);
//	} 
//	else if (currentUserID != null && currentUserID != "") 
//	{
//	transaction.setFieldValue('custrecord_taskassignedto',currentUserID);
//	}

	nlapiLogExecution('DEBUG', 'Updating RF Open Task Record Id: ', RcId);
	nlapiSubmitRecord(transaction, false, true);
	nlapiLogExecution('DEBUG', 'Updated RF Open Task successfully');

	deleteAllocations(vinvrefno,actPickQty,getEnteredContainerNo,expPickQty,remainqty,SalesOrderInternalId,itemno,EndLocation,Systemrules);

}

/**
 * This function clear the allocation qty from inventory.
 * @param invrefno
 * @param PickQty
 */
function deleteAllocations(invrefno,actPickqty,contlpno,expPickQty,remainqty,SalesOrderInternalId,itemno,binlocation,Systemrules)
{
	nlapiLogExecution('DEBUG', 'Into deleteAllocations (Inv Ref NO)',invrefno);

	try
	{
		var invtfilters = new Array(); 			 
		invtfilters[0] = new nlobjSearchFilter('internalidnumber', null, 'equalto', invrefno);

		var invtcolumns1 = new Array(); 
		invtcolumns1[0] = new nlobjSearchColumn( 'custrecord_ebiz_inv_sku' ); 			
		invtcolumns1[1] = new nlobjSearchColumn( 'custrecord_ebiz_inv_binloc' );

		var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, invtfilters, invtcolumns1); 

		nlapiLogExecution('Debug', 'invtsearchresults', invtsearchresults);

		if(invtsearchresults!=null && invtsearchresults!='' && invtsearchresults.length>0)
		{
			nlapiLogExecution('Debug', 'invtsearchresults length', invtsearchresults.length);

			var scount=1;
			var invtrecid;
			var newqoh = 0;

			LABL1: for(var i=0;i<scount;i++)
			{	

				nlapiLogExecution('ERROR', 'CUSTOM_RECORD_COLLISION', i);
				try
				{
					var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
					var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
					var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');
					var vNewAllocQty=0;
					if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {
						vNewAllocQty=parseFloat(Invallocqty)- parseFloat(expPickQty);
						if(parseFloat(vNewAllocQty)<0)
							vNewAllocQty=0;

						Invttran.setFieldValue('custrecord_ebiz_alloc_qty',parseFloat(vNewAllocQty));
					}
					else
						Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);

					nlapiLogExecution('ERROR', 'vNewAllocQty',vNewAllocQty);

					var remainqty=0;

					//Case # 20126289 Start
					if(parseInt(expPickQty) != parseInt(actPickqty))
					{

						nlapiLogExecution('ERROR', 'Systemrules.length', Systemrules.length);
						nlapiLogExecution('ERROR', 'Systemrules[0]', Systemrules[0]);
						nlapiLogExecution('ERROR', 'Systemrules[1]', Systemrules[1]);

						if(Systemrules.length>0 && Systemrules[0]=='Y' && (Systemrules[1] =="STND" ||Systemrules[1] =="NOADJUST" ))
						{

//							if(remQty==null || remQty=='null'  || remQty=='' || isNaN(remQty))
//							{
//							newqoh = parseFloat(InvQOH) - parseFloat(expPickQty);
//							}
//							else
//							{
//							newqoh = parseFloat(InvQOH) - parseFloat(actPickqty);
//							}

							if(Systemrules[1] =="STND")
								newqoh = parseFloat(InvQOH) - parseFloat(expPickQty);
							else if (Systemrules[1] =="NOADJUST")
								newqoh = parseFloat(InvQOH) - parseFloat(actPickqty);
						}
						else if(Systemrules.length>0 && Systemrules[0]=='Y' && Systemrules[1] =="ADJUSTBINLOC")
						{
							// consider binlocation quantity

//							if(remQty==null || remQty=='null'  || remQty=='' || isNaN(remQty))
//							{
//							newqoh = parseInt(InvQOH) - parseInt(expPickQty);
//							}
//							else
//							{
//							newqoh = parseInt(vNewAllocQty) + parseInt(remQty);
//							}

							newqoh = parseFloat(InvQOH) - parseFloat(actPickqty);

						}
						else
						{
							newqoh = parseFloat(InvQOH) - parseFloat(actPickqty);
						}

					}
					else
					{
						newqoh = parseInt(InvQOH) - parseInt(actPickqty);
					}
					//Case # 20126289 End
					nlapiLogExecution('ERROR', 'newqoh',newqoh);
					Invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(newqoh));  
					Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
					Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

					invtrecid = nlapiSubmitRecord(Invttran, false, true);
					nlapiLogExecution('ERROR', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);



//					var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
//					var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
//					var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');
//					var vNewAllocQty=0;
//					if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {
//					vNewAllocQty=parseInt(Invallocqty)- parseInt(expPickQty);
//					if(parseFloat(vNewAllocQty)<0)
//					vNewAllocQty=0;

//					Invttran.setFieldValue('custrecord_ebiz_alloc_qty',parseInt(vNewAllocQty));
//					}
//					else
//					Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);

//					nlapiLogExecution('ERROR', 'vNewAllocQty',vNewAllocQty);

//					var remainqty=0;


//					if(remQty==null || remQty=='null'  || remQty=='' || isNaN(remQty))
//					{
//					newqoh = parseInt(InvQOH) - parseInt(expPickQty);
//					Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(newqoh)); 
//					}
//					else
//					{
//					newqoh = parseInt(vNewAllocQty) + parseInt(remQty);
//					Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(newqoh)); 
//					}

//					//Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(InvQOH) - (parseInt(expPickQty)-parseInt(remQty))); 
//					Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
//					Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

//					invtrecid = nlapiSubmitRecord(Invttran, false, true);
//					nlapiLogExecution('ERROR', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);


				}
				catch(ex)
				{
					var exCode='CUSTOM_RECORD_COLLISION'; 
					var wmsE='Inventory record being updated by another user. Please try again...';
					if (ex instanceof nlobjError) 
					{	
						wmsE=ex.getCode() + '\n' + ex.getDetails();
						exCode=ex.getCode();
					}
					else
					{
						wmsE=ex.toString();
						exCode=ex.toString();
					} 

					nlapiLogExecution('ERROR', 'Exception in RF Inv move : ', wmsE); 
					if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
					{ 
						scount=scount+1;
						continue LABL1;
					}
					else break LABL1;
				}
			}



			//}
			/*else
			{
				clearItemAllocations(actPickqty,contlpno,expPickQty,remainqty,SalesOrderInternalId,itemno,binlocation);
			}*/



		}
		//}
		else
		{
			clearItemAllocations(actPickqty,contlpno,expPickQty,remainqty,SalesOrderInternalId,itemno,binlocation);

		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in deleteAllocations',exp);
	}		
}

function UpdateRFFulfillOrdLine(vdono,vActqty)
{
	var pickqtyfinal =0;
	nlapiLogExecution('DEBUG', 'into UpdateRFFulfillOrdLine : ', vdono);

	var doline = nlapiLoadRecord('customrecord_ebiznet_ordline',vdono);	

	var oldpickQty=doline.getFieldValue('custrecord_pickqty');
	var ordQty=doline.getFieldValue('custrecord_ord_qty');

	if(isNaN(oldpickQty) || oldpickQty==null || oldpickQty=='')
		oldpickQty=0;

	if(isNaN(ordQty) || ordQty==null || ordQty=='')
		ordQty=0;

	if(isNaN(vActqty) || vActqty==null || vActqty=='')
		vActqty=0;

	pickqtyfinal=parseFloat(oldpickQty)+parseFloat(vActqty);

	var str = 'pickqtyfinal. = ' + pickqtyfinal + '<br>';
	str = str + 'oldpickQty. = ' + oldpickQty + '<br>';	
	str = str + 'ordQty. = ' + ordQty + '<br>';	
	str = str + 'vActqty. = ' + vActqty + '<br>';	

	nlapiLogExecution('DEBUG', 'Qty Details', str);
	/* The below condition is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle*/
	if(parseInt(ordQty)>=parseInt(pickqtyfinal))
	{

		doline.setFieldValue('custrecord_upddate', DateStamp());
		doline.setFieldValue('custrecord_pickqty', parseFloat(pickqtyfinal));
		//doline.setFieldValue('custrecord_pickgen_qty', parseFloat(pickqtyfinal));

		if(parseFloat(pickqtyfinal)<parseFloat(ordQty))
			doline.setFieldValue('custrecord_linestatus_flag', '11'); //STATUS.OUTBOUND.PARTIALLY_PROCESSED
		else
			doline.setFieldValue('custrecord_linestatus_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')

		nlapiSubmitRecord(doline, false, true);

	}
	nlapiLogExecution('DEBUG', 'Out of UpdateRFFulfillOrdLine ');
}

/**
 * This function creats outbound staging inventory
 * @param invtrecid
 * @param vContLp
 * @param vqty
 */

function CreateSTGInvtRecord(invtrecid, vContLp,vqty,SalesOrderInternalId) 
{
	nlapiLogExecution('DEBUG', 'Into CreateSTGInvtRecord (Container LP)',vContLp);

	try
	{
		var stgmInvtRec = nlapiCopyRecord('customrecord_ebiznet_createinv',invtrecid);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_lp', vContLp);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(vqty).toFixed(4));
		stgmInvtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(vqty).toFixed(4));
		stgmInvtRec.setFieldValue('custrecord_ebiz_alloc_qty', '0');
		stgmInvtRec.setFieldValue('custrecord_wms_inv_status_flag', '18');	//FLAG.INVENTORY.OUTBOUND('O') 		
		stgmInvtRec.setFieldValue('custrecord_invttasktype', '3'); //Task Type - PICK
		stgmInvtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
		stgmInvtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');		
		// Temporarily hard coded by Satish.N
		//stgmInvtRec.setFieldValue('custrecord_ebiz_inv_binloc', 7218);	
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_note1', '');	
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_note2', '');	
		if(SalesOrderInternalId!=null && SalesOrderInternalId!='')
			stgmInvtRec.setFieldValue('custrecord_ebiz_transaction_no', SalesOrderInternalId);
		nlapiSubmitRecord(stgmInvtRec, false, true);
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in CreateSTGInvtRecord',exp);
	}

	nlapiLogExecution('DEBUG', 'Out of CreateSTGInvtRecord');
}

function buildTaskArray(WaveNo,ClusNo,OrdName,ebizOrdNo,vZoneId,vPickType,getContainerLpNo,fastpick)
{
	var SOarray = new Array();
	var SOFilters = new Array();
	var SOColumns=new Array();
	nlapiLogExecution('DEBUG', 'getContainerLpNo inside If', getContainerLpNo);

	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

	if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && WaveNo != null && WaveNo != "")
	{
		//code added from Monobind on 27Feb13 by santosh
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
		//upto here
	}
	if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
	{
		nlapiLogExecution('DEBUG', 'ClusNo inside If', ClusNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
	}
	if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
	{
		nlapiLogExecution('DEBUG', 'OrdNo inside If', OrdName);
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
	}
	if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && ebizOrdNo!=null && ebizOrdNo!="" && ebizOrdNo!= "null")
	{
		nlapiLogExecution('DEBUG', 'SO Id inside If', ebizOrdNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ebizOrdNo));
	}
	if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
	{
		nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
	}
	if((vPickType.indexOf('C') != -1 || vPickType=='ALL') && getContainerLpNo!=null && getContainerLpNo!="" && getContainerLpNo!= "null")
	{
		nlapiLogExecution('DEBUG', 'Container LP inside If', getContainerLpNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getContainerLpNo));
	}


	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
//	SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
	//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
	SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
	//Code end as on 290414
	SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
	SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
	SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
	SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
	SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
	SOColumns.push(new nlobjSearchColumn('name'));
	SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
	SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
	SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));

	SOColumns[0].setSort();
	SOColumns[1].setSort();
	SOColumns[2].setSort();
	SOColumns[3].setSort();
	SOColumns[4].setSort();
	SOColumns[5].setSort(true);

	SOarray["custparam_ebizordno"] = ebizOrdNo;

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

	if (SOSearchResults != null && SOSearchResults.length > 0) {

		var SOSearchResult = SOSearchResults[0];
		if(SOSearchResults.length > 1)
		{
			var SOSearchnextResult = SOSearchResults[1];
			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
			nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
		}
		else
		{
			var SOSearchnextResult = SOSearchResults[0];
			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
			nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
			nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
		}

		SOarray["custparam_waveno"] = WaveNo;
		SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
		//SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
		SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
		SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
		SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
		SOarray["custparam_endlocinternalid"] = SOSearchResult.getValue('custrecord_actbeginloc');		
		SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
		SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
		SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
		SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
		SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
		SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
		SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
		SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
		SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
		SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
		SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
		SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
		SOarray["custparam_noofrecords"] = SOSearchResults.length;		
		SOarray["name"] =  SOSearchResult.getValue('name');
		SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
		SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
		SOarray["custparam_picktype"] =vPickType;
		SOarray["custparam_fastpick"] = fastpick;
		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
		}
		else
			SOarray["custparam_ebizzoneno"] = '';
	}

	return SOarray;
}
function getOpenTasksCountForNavigation(waveno,containerlpno,vPickType,SalesOrderInternalId,OrdName,vZoneId,vSite,pickMethodId)
{
	nlapiLogExecution('Debug', 'Into getOpenTasksCountNav...');

	var str = 'waveno. = ' + waveno + '<br>';
	str = str + 'containerlpno. = ' + containerlpno + '<br>';	
	str = str + 'vPickType. = ' + vPickType + '<br>';	
	str = str + 'SalesOrderInternalId. = ' + SalesOrderInternalId + '<br>';
	str = str + 'OrdName. = ' + OrdName + '<br>';
	str = str + 'vZoneId. = ' + vZoneId + '<br>';
	str = str + 'vSite. = ' + vSite + '<br>';
	str = str + 'pickMethodId. = ' + pickMethodId + '<br>';
	nlapiLogExecution('Debug', 'Function Parameters', str);



	/*var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_ebizsitepickrule', null, 'anyof', vSite));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizpickmethod');
	columns[1] = new nlobjSearchColumn('custrecord_cartonization_method','custrecord_ebizpickmethod');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_stagedetermination','custrecord_ebizpickmethod');

	var pickRuleSearchResult = new nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters, columns);

	var pickMethodId ;
	var Stagedet;
	if(pickRuleSearchResult != null){
		for (var i=0; i<pickRuleSearchResult.length; i++){			
			pickMethodId = pickRuleSearchResult[0].getValue('custrecord_ebizpickmethod');

			Stagedet = pickRuleSearchResult[i].getValue('custrecord_ebiz_stagedetermination','custrecord_ebizpickmethod');
		}
	}
	nlapiLogExecution('ERROR', 'pickMethodId', pickMethodId);*/
	var pickmethodSearchResult=null;
	if(pickMethodId!=null && pickMethodId!='')
	{

		var filters = new Array();
		filters.push(new nlobjSearchFilter('internalid', null, 'is', pickMethodId));


		var columns = new Array();

		columns.push(new nlobjSearchColumn('custrecord_ebiz_stagedetermination'));

		var pickmethodSearchResult = new nlapiSearchRecord('customrecord_ebiznet_pick_method', null, filters, columns);
	}

	var pickMethodId ;
	var Stagedet;
	if(pickmethodSearchResult != null){
		for (var i=0; i<pickmethodSearchResult.length; i++){			
			Stagedet = pickmethodSearchResult[i].getValue('custrecord_ebiz_stagedetermination');
		}
	}
	nlapiLogExecution('ERROR', 'pickMethodId', pickMethodId);


	//The below changes done by Satish.N
	//If stage determination is not defined,take carton level as default.

	if(Stagedet==null || Stagedet=='')
		Stagedet=3;

	//Upto here.


	/*var fields=new Array();	
	fields[0]='custrecord_ebiz_stagedetermination';
	var pickmethodcolumns = nlapiLookupField('customrecord_ebiznet_pick_method', pickMethodId, fields);	
	if(pickmethodcolumns!=null)

	var Stagedet = pickmethodcolumns.custrecord_ebiz_stagedetermination;*/

	nlapiLogExecution('ERROR', 'Stagedet', Stagedet);

	var openreccount=0;
	var SOFilters = new Array();
	var SOColumns = new Array();

	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK

	if(Stagedet == 3)//Carton Level
	{
		nlapiLogExecution('ERROR', 'containerlpno', containerlpno);
		if(containerlpno !=null && containerlpno !='' && containerlpno !='null')
			SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', containerlpno));
	}
	else if(Stagedet == 2)//Wave Level
	{
		nlapiLogExecution('ERROR', 'waveno', waveno);
		if(waveno !=null && waveno !='' && waveno !='null')
			SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(waveno)));
	}
	else if(Stagedet == 1)//Order Level
	{
		nlapiLogExecution('ERROR', 'OrdName', OrdName);
		if(OrdName !=null && OrdName !='' && OrdName !='null')
			SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
	}
	/*if(OrdName != null && OrdName != '')
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));*/

	if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && waveno != null && waveno != "")
	{
		nlapiLogExecution('ERROR', 'waveno', waveno);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(waveno)));
	}	 
	if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
	{
		nlapiLogExecution('Debug', 'OrdNo inside If', OrdName);
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
	}
	if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && SalesOrderInternalId!=null && SalesOrderInternalId!="" && SalesOrderInternalId!= "null")
	{
		nlapiLogExecution('Debug', 'SO Id inside If', SalesOrderInternalId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId));
	}
	if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
	{
		nlapiLogExecution('Debug', 'Zone # inside If', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
	}

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	if(SOSearchResults!=null && SOSearchResults!=''&& SOSearchResults.length>0)
		openreccount=SOSearchResults.length;	

	nlapiLogExecution('Debug', 'Out of getOpenTasksCountForNav...',openreccount);

	return openreccount;

}
