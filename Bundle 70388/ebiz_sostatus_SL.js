/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Reports/Suitelet/ebiz_sostatus_SL.js,v $
 *     	   $Revision: 1.6.4.4.4.3.4.4.4.1 $
 *     	   $Date: 2015/11/13 17:13:29 $
 *     	   $Author: snimmakayala $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_130 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_sostatus_SL.js,v $
 * Revision 1.6.4.4.4.3.4.4.4.1  2015/11/13 17:13:29  snimmakayala
 * 201415645
 *
 * Revision 1.6.4.4.4.3.4.4  2014/05/12 14:55:41  sponnaganti
 * case# 20148036
 * UOM field Updation
 *
 * Revision 1.6.4.4.4.3.4.3  2014/04/29 14:10:43  sponnaganti
 * case# 20148036
 * (UOM filed is commented)
 *
 * Revision 1.6.4.4.4.3.4.2  2013/05/31 15:09:02  grao
 * CASE201112/CR201113/LOG201121
 * PMM Issues fixes
 *
 * Revision 1.6.4.4.4.3.4.1  2013/03/15 09:28:43  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.6.4.4.4.3  2012/11/01 14:55:15  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.6.4.4.4.2  2012/10/02 22:45:03  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue Fixes for FISK,BOOMBAH and TDG.
 *
 * Revision 1.6.4.4  2012/08/02 15:00:56  gkalla
 * CASE201112/CR201113/LOG201121
 * To show consolidated qty
 *
 * Revision 1.6.4.3  2012/05/07 09:40:46  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * so status
 *
 * Revision 1.7  2012/02/20 15:33:59  gkalla
 * CASE201112/CR201113/LOG201121
 * Added code to fetch more than 1000 orders to drop down
 *
 * Revision 1.6  2011/10/25 23:07:55  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Status display
 *
 * Revision 1.5  2011/06/30 15:19:52  rgore
 * CASE201112/CR201113/LOG201121
 * Added necessary ';' to remove any code warnings
 * Ratnakar
 * 30 June 2011
 *
 * Revision 1.4  2011/05/27 07:58:36  skota
 * CASE201112/CR201113/LOG201121
 * SO's Order by changed to descending
 *
 * Revision 1.3  2011/05/27 07:14:46  skota
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.2  2011/05/27 06:58:57  skota
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.4  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.3  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/
function ebiznet_OrderStatus_SL(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Order Status');
		var salesorder = form.addField('custpage_order', 'text', 'Order#');

		salesorder.setMandatory(true);

		var button = form.addSubmitButton('Display');

		response.writePage(form);
	}
	else {

		var form = nlapiCreateForm('Order Status');

		var soidno = request.getParameter('custpage_order');
		var salesorder = form.addField('custpage_order', 'text', 'Order#');
		salesorder.setMandatory(true);
		salesorder.setDefaultValue(soidno);

		var soid = GetSOInternalId(soidno);

		if(soid != null && soid != "")
		{
			var button = form.addSubmitButton('Display');

			var ordertext = form.addField('custpage_textorder', 'text', 'Order #').setLayoutType('startrow');
			ordertext.setDisplayType('inline');
			var statustext = form.addField('custpage_textstatus', 'text', 'Status');
			statustext.setDisplayType('inline');
			var consigneetext = form.addField('custpage_textconsignee', 'text', 'Consignee');
			consigneetext.setDisplayType('inline');
			var carriertext = form.addField('custpage_textcarrier', 'text', 'Carrier');
			carriertext.setDisplayType('inline');

			var addresstext = form.addField('custpage_textaddress', 'textarea', 'Address');
			addresstext.setDisplayType('inline');

			var entdatetext = form.addField('custpage_textentdate', 'text', 'Entered Date');
			entdatetext.setDisplayType('inline');

			/*
			 * LN,Defining SubList lines
			 */
			var sublist = form.addSubList("custpage_items", "list", "ItemList");
			sublist.addField("custpage_slno", "text", "Sl.No");
			sublist.addField("custpage_fono", "text", "FO #");
			sublist.addField("custpage_lieno", "text", "Line #");
			sublist.addField("custpage_itemname", "text", "Item");
			//sublist.addField("custpage_desc", "text", "Description");	
			sublist.addField("custpage_quantity", "text", "Order Qty");
			//case# 20148036 starts (UOM filed is uncommented)
			sublist.addField("custpage_uom", "text", "UOM");	
			//case# 20148036 end
			sublist.addField("custpage_itemstatus", "text", "Item Status");
			sublist.addField("custpage_allocqty", "text", "Pick Gen Qty");
			sublist.addField("custpage_pickedqty", "text", "Picked Qty");
			sublist.addField("custpage_shippedqty", "text", "Ship Qty");
			sublist.addField("custpage_status", "text", "Status");;

			nlapiLogExecution('Error', 'SO', soid);

			var trantype = nlapiLookupField('transaction', soid, 'recordType');
			nlapiLogExecution('ERROR', 'trantype', trantype);

			var transaction;
			if(trantype!='salesorder')
			{
				transaction = nlapiLoadRecord('transferorder', soid);
			}
			else
			{
				transaction = nlapiLoadRecord('salesorder', soid);
			}
			ordertext.setDefaultValue(transaction.getFieldValue('tranid'));
			consigneetext.setDefaultValue(transaction.getFieldText('entity'));
			carriertext.setDefaultValue(transaction.getFieldText('shipmethod'));
			addresstext.setDefaultValue(transaction.getFieldValue('shipaddress'));
			entdatetext.setDefaultValue(transaction.getFieldValue('trandate'));
			statustext.setDefaultValue(transaction.getFieldValue('status'));

			nlapiLogExecution('Error', 'statustext', statustext);

			var filters = new Array();
			filters[0] = new nlobjSearchFilter('name', null, 'is', soid);

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_ebiz_linesku');
			columns[1] = new nlobjSearchColumn('custrecord_ordline').setSort();
			columns[2] = new nlobjSearchColumn('custrecord_lineuom_id');
			columns[3] = new nlobjSearchColumn('custrecord_ord_qty');
			columns[4] = new nlobjSearchColumn('custrecord_base_uom_id');
			columns[5] = new nlobjSearchColumn('custrecord_base_ord_qty');
			columns[6] = new nlobjSearchColumn('custrecord_pickgen_qty');
			columns[7] = new nlobjSearchColumn('custrecord_pickqty');
			columns[8] = new nlobjSearchColumn('custrecord_ship_qty');
			columns[9] = new nlobjSearchColumn('custrecord_linepackcode');
			columns[10] = new nlobjSearchColumn('custrecord_lineord');
			columns[11] = new nlobjSearchColumn('custrecord_linestatus_flag');
			columns[12] = new nlobjSearchColumn('custrecord_linesku_status');

			// LN, execute the search, passing null filter and return columns
			var searchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
			var i=0;
			for (var v = 0; searchresults != null && v < searchresults.length; v++) {

				var vPickqty = searchresults[v].getValue('custrecord_pickqty');
				if(vPickqty=='' || vPickqty==null || isNaN(vPickqty))
					vPickqty=0;

				var vPickgenqty = searchresults[v].getValue('custrecord_pickgen_qty');
				if(vPickgenqty=='' || vPickgenqty==null || isNaN(vPickgenqty))
					vPickgenqty=0;

				var vShipqty = searchresults[v].getValue('custrecord_ship_qty');
				if(vShipqty=='' || vShipqty==null || isNaN(vShipqty))
					vShipqty=0;


				form.getSubList('custpage_items').setLineItemValue('custpage_slno', i + 1, i + 1);
				form.getSubList('custpage_items').setLineItemValue('custpage_fono', i + 1, searchresults[v].getValue('custrecord_lineord'));
				form.getSubList('custpage_items').setLineItemValue('custpage_lieno', i + 1, searchresults[v].getValue('custrecord_ordline'));
				form.getSubList('custpage_items').setLineItemValue('custpage_itemname', i + 1, searchresults[v].getText('custrecord_ebiz_linesku'));


				//form.getSubList('custpage_items').setLineItemValue('custpage_desc', i + 1, arr_desc[i]);
				form.getSubList('custpage_items').setLineItemValue('custpage_quantity', i + 1, searchresults[v].getValue('custrecord_ord_qty'));
				//case# 20148036 starts
				var searchRec = new Array();
				var filter = new Array();
				filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', searchresults[v].getValue('custrecord_ebiz_linesku')));
				filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	

				var column = new Array();

				column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim') ;

				searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);

				form.getSubList('custpage_items').setLineItemValue('custpage_uom', i + 1, searchRec[0].getText('custrecord_ebizuomskudim'));
				//case# 20148036 end
				form.getSubList('custpage_items').setLineItemValue('custpage_itemstatus', i + 1, searchresults[v].getText('custrecord_linesku_status'));
				form.getSubList('custpage_items').setLineItemValue('custpage_allocqty', i + 1, parseFloat(vPickgenqty).toFixed(4));
				form.getSubList('custpage_items').setLineItemValue('custpage_pickedqty', i + 1, parseFloat(vPickqty).toFixed(4));
				form.getSubList('custpage_items').setLineItemValue('custpage_shippedqty', i + 1, parseFloat(vShipqty).toFixed(4));
				form.getSubList('custpage_items').setLineItemValue('custpage_status', i + 1, searchresults[v].getText('custrecord_linestatus_flag'));
				i++;

			}
		}
		else
		{
			var msg= form.addField('custpage_message', 'inlinehtml', null, null, null);
			var msgtext = "Invalid Order# : "+soidno;
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', '"+ msgtext + "', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
		}
		response.writePage(form);
	}

}

function fillsalesorderField(form, salesorderField,maxno){
	var salesorderFilers = new Array();
	salesorderFilers.push(new nlobjSearchFilter('mainline', null, 'is', 'T')); 
	if(maxno!=-1)
	{
		salesorderFilers.push(new nlobjSearchFilter('internalidnumber', null, 'lessthan', parseFloat(maxno)));
	}


	salesorderField.addSelectOption("", "");

	var columns=new Array();
	columns[0]=new nlobjSearchColumn('tranid');
	columns[0].setSort(true);
	columns[1]=new nlobjSearchColumn('internalid');
	columns[2]=new nlobjSearchColumn('type');


	var customerSearchResults = nlapiSearchRecord('salesorder', null, salesorderFilers,columns);


	for (var i = 0; customerSearchResults != null && i < customerSearchResults.length; i++) {

		//nlapiLogExecution('ERROR', 'tranid', customerSearchResults[i].getValue('tranid')); 
		if(customerSearchResults[i].getValue('tranid') != null && customerSearchResults[i].getValue('tranid') != "" && customerSearchResults[i].getValue('tranid') != " ")
		{
			var resdo = form.getField('custpage_order').getSelectOptions(customerSearchResults[i].getValue('tranid'), 'is');
			if (resdo != null) {
				if (resdo.length > 0) {
					continue;
				}
			}
		}
		salesorderField.addSelectOption(customerSearchResults[i].getValue('internalid'), customerSearchResults[i].getValue('tranid'));
	}
	if(customerSearchResults!=null && customerSearchResults.length>=1000)
	{
		var column=new Array();
		column[0]=new nlobjSearchColumn('tranid');		
		column[1]=new nlobjSearchColumn('internalid');
		column[1].setSort(true);

		var OrderSearchResults = nlapiSearchRecord('salesorder', null, salesorderFilers,column);

		var maxno=OrderSearchResults[OrderSearchResults.length-1].getValue(columns[1]);		
		fillsalesorderField(form, salesorderField,maxno);	

	}
}
/**
 * 
 * @param SOid // To get SO internal Id
 */
function GetSOInternalId(SOid)
{
	var salesorderFilers = new Array();
	salesorderFilers.push(new nlobjSearchFilter('mainline', null, 'is', 'T')); 
	salesorderFilers.push(new nlobjSearchFilter('tranid', null, 'is', SOid)); 
	//salesorderFilers.push(new nlobjSearchFilter('line', null, 'is', ordline)); 


	var customerSearchResults = nlapiSearchRecord('salesorder', null, salesorderFilers,null);	
	if(customerSearchResults==null)
	{
		customerSearchResults=nlapiSearchRecord('transferorder',null,salesorderFilers,null);
	}

	if(customerSearchResults != null && customerSearchResults != "")
	{
		nlapiLogExecution('ERROR', 'customerSearchResults.length ',customerSearchResults.length);
		return customerSearchResults[0].getId();
	}
	else
		return null;
}