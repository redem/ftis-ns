function PickPutRuleFieldChanged(type, name)
{
//  This particular code is 
// based on the Cycle count method flag , and disables/enables both standard and custom fields.

if (name == 'custrecord_ruletypepickorput')
{
var value = nlapiGetFieldText('custrecord_ruletypepickorput');
if (value=='PICK')
{
nlapiDisableField('custrecord_putawaymethod', true);
nlapiDisableField('custrecord_putawayzonepickrule', true);
nlapiDisableField('custrecord_pickmethodpickrule', false);
nlapiDisableField('custrecord_putzonepickrule', false);
}
else if (value=='PUT')
{
nlapiDisableField('custrecord_putawaymethod', false);
nlapiDisableField('custrecord_putawayzonepickrule', false);
nlapiDisableField('custrecord_pickmethodpickrule', true);
nlapiDisableField('custrecord_putzonepickrule', true);
}

}
}
