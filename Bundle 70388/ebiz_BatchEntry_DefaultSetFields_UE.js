/***************************************************************************
���������������������eBizNET
�����������������eBizNET Solutions Inc
****************************************************************************
*
*� $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inbound/UserEvents/ebiz_BatchEntry_DefaultSetFields_UE.js,v $
*� $Revision: 1.1.2.1.4.1.4.1.4.1 $
*� $Date: 2014/07/29 15:06:11 $
*� $Author: grao $
*� $Name: t_eBN_2014_2_StdBundle_0_6 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_BatchEntry_DefaultSetFields_UE.js,v $
*� Revision 1.1.2.1.4.1.4.1.4.1  2014/07/29 15:06:11  grao
*� Case#: 20149594 New 2014.2 Compatibilty issue fixes
*�
*� Revision 1.1.2.1.4.1.4.1  2013/11/22 07:05:40  spendyala
*� CASE201112/CR201113/LOG201121
*� Issue fixed related to case#20125672
*�
*� Revision 1.1.2.1.4.1  2012/11/01 14:55:22  schepuri
*� CASE201112/CR201113/LOG201121
*� Decimal Qty Conversions
*�
*� Revision 1.1.2.1  2012/03/21 15:01:19  spendyala
*� CASE201112/CR201113/LOG201121
*� Issue related to set the batch entry fields is resolved.
*�
*� Revision 1.1  2011/12/19 06:55:25  spendyala
*� CASE201112/CR201113/LOG201121
*� Added BEFORE load functionality to create  new batch entry record with default values.
*�
*
****************************************************************************/

/**
 * This event will fire before load of the record.
 * This function will set default values to create a new BatchEntry record.
 * @param type
 * @param form
 * @param request
 */
function BatchEntrySetDefaultFields(type, form, request)
{
	nlapiLogExecution('ERROR','BATCH');
	nlapiLogExecution('ERROR','TYPE',type);
	try
	{
		if(type=='create')
		{
			var skuid=request.getParameter('custparam_serialskuid');
			if(skuid!=null&&skuid!="")
			{
				var locationid=request.getParameter('custparam_locationid');
				var companyid=request.getParameter('custparam_companyid');
				var lot=request.getParameter('custparam_lot');
				var name=request.getParameter('custparam_name');
				var itemstatus=request.getParameter('custparam_itemstatus');
				var packcode=request.getParameter('custparam_packcode');
				var expdate=request.getParameter('custparam_expdate');
/*//				var expdate=DateStamp();
				nlapiLogExecution('ERROR','locationid',locationid);
				nlapiLogExecution('ERROR','companyid',companyid);
				nlapiLogExecution('ERROR','skuid',skuid);
				nlapiLogExecution('ERROR','lot',lot);
				nlapiLogExecution('ERROR','name',name);
				nlapiLogExecution('ERROR','itemstatus',itemstatus);
				nlapiLogExecution('ERROR','packcode',packcode);
//				nlapiLogExecution('ERROR','expdate',expdate);
*/				nlapiSetFieldValue('custrecord_ebizsitebatch',locationid);
				nlapiSetFieldValue('custrecord_ebizcompanybatch',companyid);
				nlapiSetFieldValue('custrecord_ebizsku',skuid);
				nlapiSetFieldValue('custrecord_ebizlotbatch',lot);
				nlapiSetFieldValue('name',name);
				nlapiSetFieldValue('custrecord_ebizskustatus',itemstatus);
				nlapiSetFieldValue('custrecord_ebizpackcode',packcode);
				nlapiSetFieldValue('custrecord_ebizexpirydate',expdate);
			}
		}
	}

	catch(exe)
	{
		nlapiLogExecution('ERROR','exe',exe);
	}
}
