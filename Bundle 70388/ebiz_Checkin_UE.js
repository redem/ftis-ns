/***************************************************************************
 eBizNET Solutions       
 ****************************************************************************/
/* 
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/UserEvents/ebiz_Checkin_UE.js,v $
 *     	   $Revision: 1.9.4.14.4.3.2.26.2.2 $
 *     	   $Date: 2015/10/27 18:22:11 $
 *     	   $Author: grao $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * 
 * PRAMETERS
 *
 * DESCRIPTION
 * 
 * REVISION HISTORY
 * $Log: ebiz_Checkin_UE.js,v $
 * Revision 1.9.4.14.4.3.2.26.2.2  2015/10/27 18:22:11  grao
 * 2015.2 Issue Fixes 201415210
 *
 * Revision 1.9.4.14.4.3.2.26.2.1  2015/10/20 10:49:32  grao
 *  CT issue fixes 201415135
 *
 * Revision 1.9.4.14.4.3.2.26  2015/04/07 13:59:33  schepuri
 * case# 201412256
 *
 * Revision 1.9.4.14.4.3.2.25  2014/06/06 14:50:39  grao
 * Case#: 20148768 Standard  issue fixes
 *
 * Revision 1.9.4.14.4.3.2.24  2014/04/18 15:43:45  skavuri
 * Case# 20148014 SB Issue fixed
 *
 * Revision 1.9.4.14.4.3.2.23  2014/02/14 10:40:06  grao
 * Case# 20127180 related issue fixes in Sonic  electrox issue fixes
 *
 * Revision 1.9.4.14.4.3.2.22  2014/02/06 14:52:18  rmukkera
 * Case # 20127076
 *
 * Revision 1.9.4.14.4.3.2.21  2013/12/23 14:09:59  schepuri
 * 20126456
 *
 * Revision 1.9.4.14.4.3.2.20  2013/11/22 14:40:36  grao
 * PO Creation related issue fixes in SB 2014.1
 *
 * Revision 1.9.4.14.4.3.2.19  2013/11/21 11:37:40  grao
 * 20125819  && 20125820
 *
 * Revision 1.9.4.14.4.3.2.18  2013/10/01 14:46:08  grao
 * issue fixes related to the 20124682
 *
 * Revision 1.9.4.14.4.3.2.17  2013/10/01 14:45:20  grao
 * issue fixes related to the 20124682
 *
 * Revision 1.9.4.14.4.3.2.16  2013/09/19 16:03:38  skreddy
 * Case# 20124466
 * Afosa SB  issue fix
 *
 * Revision 1.9.4.14.4.3.2.15  2013/09/10 07:03:17  spendyala
 * CASE201112/CR201113/LOG201121
 * Fine tuninng  of script.
 *
 * Revision 1.9.4.14.4.3.2.14  2013/08/30 16:40:33  skreddy
 * Case# 20124143
 * standard bundle issue fix
 *
 * Revision 1.9.4.14.4.3.2.13  2013/08/27 15:22:42  skreddy
 * Case#: 20124078
 * Issue rellated to restrict transctions if location mapped to user
 *
 * Revision 1.9.4.14.4.3.2.12  2013/08/13 15:17:22  rmukkera
 * Issue Fix related to 20123769ï¿½,20123770ï¿½
 *
 * Revision 1.9.4.14.4.3.2.11  2013/07/12 15:35:36  nneelam
 * Case# 20122210
 * Check in link in Standard Bundle  Issue Fix.
 *
 * Revision 1.9.4.14.4.3.2.10  2013/07/06 01:01:08  skreddy
 * Case# 20123235
 * UOM conversions, display UOM text
 *
 * Revision 1.9.4.14.4.3.2.9  2013/07/04 18:53:46  spendyala
 * case# 201215953, case# 201215487
 * If PO has non-invt or expenses only then we enable Receive option.
 *
 * Revision 1.9.4.14.4.3.2.8  2013/06/28 21:59:48  grao
 * Case# 20123235
 * Showing Units type text instead of Id
 *
 * Revision 1.9.4.14.4.3.2.7  2013/06/28 21:58:39  grao
 * PCT Sb issue Fixes
 * 20123235
 * Showing Units type text instead of Id
 *
 * Revision 1.9.4.14.4.3.2.6  2013/06/27 16:37:06  skreddy
 * Case# 20123183
 * POline level locations
 *
 * Revision 1.9.4.14.4.3.2.5  2013/05/15 13:37:11  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * pocheckin
 *
 * Revision 1.9.4.14.4.3.2.4  2013/03/22 11:44:32  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.9.4.14.4.3.2.3  2013/03/19 11:46:26  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.9.4.14.4.3.2.2  2013/02/26 13:23:18  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Marged from Boombah.
 *
 * Revision 1.9.4.14.4.3.2.1  2013/02/26 12:52:10  rmukkera
 * code merged from boombah production bundle on 26th feb 2013
 * t_eBN_2013_1_StdBundle_1
 *
 * Revision 1.9.4.14.4.3  2013/02/20 11:44:58  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * checkin link
 *
 * Revision 1.9.4.14.4.2  2012/12/03 15:39:42  rmukkera
 * CASE201112/CR201113/LOG2012392
 * UOM conversions code fix
 *
 * Revision 1.9.4.14.4.1  2012/11/01 14:55:22  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.9.4.14  2012/08/01 07:19:55  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * checkin link
 *
 * Revision 1.9.4.13  2012/07/27 13:01:19  schepuri
 * CASE201112/CR201113/LOG201121
 * issue There are no records of this type
 *
 * Revision 1.9.4.12  2012/07/19 14:06:41  spendyala
 * CASE201112/CR201113/LOG201121
 * Fine Tunning.
 *
 * Revision 1.9.4.11  2012/06/18 07:34:38  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT and Prod issue fixes
 *
 * Revision 1.9.4.10  2012/05/25 22:25:26  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.9.4.9  2012/04/16 15:02:16  spendyala
 * CASE201112/CR201113/LOG201121
 * Calculating POoverage is moved to general function.
 *
 * Revision 1.9.4.8  2012/04/13 22:23:28  spendyala
 * CASE201112/CR201113/LOG201121
 * Depending upon POoverage Value mentioned in the item master
 * value of poOverage will be calculated accordingly.
 *
 * Revision 1.9.4.7  2012/03/31 18:13:43  spendyala
 * CASE201112/CR201113/LOG201121
 * Added Code to disable "receive" button
 *
 * Revision 1.9.4.6  2012/03/31 17:09:50  spendyala
 * CASE201112/CR201113/LOG201121
 * Added Code to disable "receive" button
 *
 * Revision 1.9.4.5  2012/02/07 15:33:22  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UOM Conversion
 *
 * Revision 1.16  2012/02/07 15:29:11  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UOM Conversion
 *
 * Revision 1.15  2012/02/01 14:13:30  spendyala
 *  CASE201112/CR201113/LOG201121
 * Merged files from batch up to 1.9.4.4 revision.
 *
 * Revision 1.14  2012/01/30 07:43:23  spendyala
 * CASE201112/CR201113/LOG201121
 * POvalue in checkin link is uncommented.
 *
 * Revision 1.13  2012/01/27 08:40:30  spendyala
 * CASE201112/CR201113/LOG201121
 * Checking condition with createform and isdropshipitemfield for showing checkin link at line level.
 *
 * Revision 1.12  2012/01/27 06:46:38  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Multi UOM Processing
 *
 * Revision 1.11  2012/01/25 07:22:38  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Multi UOM Processing
 *
 * Revision 1.10  2012/01/24 06:51:18  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Multi UOM Processing
 *
 * Revision 1.9  2011/11/18 22:57:03  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Delete record from inventory if QOH becomes 0
 *
 * Revision 1.8  2011/08/23 08:46:53  skota
 * CASE201112/CR201113/LOG201121
 * Variable 'poOverage' initialization changed from 'N' to 0 (zero) as the field type changed from checkbox to freeform text
 *
 *    
 *
 *****************************************************************************/
/***************************************************
 * This is a User Event Script that generates a
 * link for each line item that points to the
 * Suitelet page above.  It is meant to be deployed
 * on the Purchase Order record.
 */
function poAddCheckInLinkUE(type, form, request){
	//obtain the context object
	var ctx = nlapiGetContext();
	var createdfrom=nlapiGetFieldValue('createdfrom');
	nlapiLogExecution('ERROR', 'createdfrom', createdfrom);
	var vLocation=nlapiGetFieldValue('location');
	var mwhsiteflag='F';
	if(createdfrom==""||createdfrom==null)
	{
		//only execute when the exec context is web browser and in view mode of the PO
		if (ctx.getExecutionContext() == 'userinterface' && type == 'view') {

			//obtain the PO internal ID
			var poID = nlapiGetRecordId();

			try
			{
				var ItemArrayList=new Array();
				for (var j = 0; j < form.getSubList('item').getLineItemCount(); j++) 
				{
					ItemArrayList.push(nlapiGetLineItemValue('item', 'item', j + 1));
				}
				nlapiLogExecution('Error', 'ItemArrayList', ItemArrayList);
				if(vLocation!=null && vLocation!='')
				{
					var fields = ['custrecord_ebizwhsite'];

					var locationcolumns = nlapiLookupField('Location', vLocation, fields);
					if(locationcolumns!=null)
						mwhsiteflag = locationcolumns.custrecord_ebizwhsite;
				}
				nlapiLogExecution('Error', 'mwhsiteflag', mwhsiteflag);
				/*var filter=new Array();
				filter[0]=new nlobjSearchFilter('internalid',null,'anyof',poID);
				filter[1]=new nlobjSearchFilter('mainline', null, 'is', 'T');
				var column=new Array();
				column[0]=new nlobjSearchColumn('customform');

				var rec=nlapiSearchRecord('purchaseorder',null,filter,column);
				if(rec!=null && rec!="")
				{
					var vcustomform=rec[0].getText('customform');
					nlapiLogExecution('ERROR', 'vcustomform', vcustomform);*/
					if(mwhsiteflag=='T')
					{
						// Case# 20148014 starts 
						var CheckPOinTransOrd = CheckPO(poID);
						nlapiLogExecution('Error', 'CheckPOinTransOrd', CheckPOinTransOrd);
						var closebtn= form.getButton('closeremaining');
						nlapiLogExecution('Error', 'closeremaining', closebtn);
						if(closebtn!=null && CheckPOinTransOrd ==true)
							closebtn.setDisabled(true);
						// Case# 20148014 ends
						if(ItemArrayList!=null&&ItemArrayList!="")
						{
							var tempconf="F";
							var filter=new Array();
							filter[0]=new nlobjSearchFilter("internalid",null,"anyof",ItemArrayList);

							var column=new Array();
							column[0]=new nlobjSearchColumn("type");

							var res=nlapiSearchRecord("item",null,filter,column);
							for(var count=0;count<res.length;count++)
							{
								var itemtype=res[count].getText("type");
								nlapiLogExecution('Error', 'itemtype', itemtype);
								if(itemtype=="Assembly/Bill of Materials"||itemtype=="Inventory Item"||itemtype=="Kit/Package")
								{
									tempconf="T";
									break;
								}
							}
							if(tempconf=="T")
							{
								var receivebtn= form.getButton('receive');  
								if(receivebtn!=null)
									receivebtn.setDisabled(true);
							}
						}
					}
//				}
			}
			catch (exp) 
			{
				nlapiLogExecution('ERROR', 'Exception in hiding receive button : ', exp);		
			}

			//Add the "ebiznet Check In" field
			form.getSubList('item').addField('custpage_checkin_link', 'text', 'Check-in', null);


			var poName = nlapiGetFieldValue('tranid');
			var location = nlapiGetFieldValue('location');
			var postatus = nlapiGetFieldValue('status');
			nlapiLogExecution('ERROR', 'postatus', postatus);
			nlapiLogExecution('ERROR', 'location', location);
			
			//case # : 20124078 start
			//checking for rolebased location 
			var vRoleLocation=getRoledBasedLocation();
			var filter1=new Array();
			filter1.push(new nlobjSearchFilter('internalid',null,'anyof',poID));
			filter1.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
								
			if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
			{
				filter1.push(new nlobjSearchFilter('location', null, 'anyof', vRoleLocation));
				nlapiLogExecution('ERROR', 'vRoleLocation', vRoleLocation);
			}
			var column1=new Array();
			column1[0]=new nlobjSearchColumn('customform');

			var rec=nlapiSearchRecord('purchaseorder',null,filter1,column1);
			
			//end		
			

			//resolve the generic relative URL for the Suitelet
			var checkInURL = nlapiResolveURL('SUITELET', 'customscript_ebiznetcheckin', 'customdeploy_checkinsuitelet');

			// Get the FQDN based on the context
			//checkInURL = getFQDNForHost(ctx.getEnvironment()) + checkInURL;
//			var poOverage = checkPOOverage(poID,location, null);
			var poOverage=0;
			//loop through the item sublist
			var vpoOverage_receipt=checkPOOverage_ReceiptType(poID);
			var ItemArray=new Array();
			var vPOoverageChecked='F';
			var vConfig=nlapiLoadConfiguration('accountingpreferences');
			if(vConfig != null && vConfig != '')
			{
				vPOoverageChecked=vConfig.getFieldValue('OVERRECEIPTS');
			}
			nlapiLogExecution('ERROR','vPOoverageChecked', vPOoverageChecked);

			for (var j = 0; j < form.getSubList('item').getLineItemCount(); j++) 
			{
				ItemArray.push(nlapiGetLineItemValue('item', 'item', j + 1));
			}

			var POOverageRec=vGetPOOverage(ItemArray);
			var vGetPoReceipt=nlapiGetFieldValue("custbody_nswmsporeceipttype");
			nlapiLogExecution("ERROR","vGetPoReceipt",vGetPoReceipt);
			var vpoOverage_receipt=checkPOOverage_ReceiptType(poID,vGetPoReceipt);
			
			for ( var i = 0; i < form.getSubList('item').getLineItemCount(); i++)
			{
				var Isdropshipitemfield=nlapiGetLineItemValue('item','isdropshipitemfield',i + 1);
				var IsClosed=nlapiGetLineItemValue('item','isclosed',i + 1);
				nlapiLogExecution('DEBUG', 'IsClosed', IsClosed);
				nlapiLogExecution('DEBUG', 'Isdropshipitemfield', Isdropshipitemfield);
				if(IsClosed!='T')
				{

					if(Isdropshipitemfield!='T')
					{
						if(vPOoverageChecked=='T')
						{
							poOverage=POOverageRec[i];
							if(poOverage==""||poOverage==null)
								poOverage=vpoOverage_receipt;
						}

						//case 20122210 start
						if(poOverage==''|| poOverage==null)
							poOverage=0;
						//case 20122210 end


						nlapiLogExecution('DEBUG','poOverage final', poOverage);

					var quantity = parseFloat(form.getSubList('item').getLineItemValue('quantity', i + 1));
					var lineno = form.getSubList('item').getLineItemValue('line', i + 1);
					var item = form.getSubList('item').getLineItemValue('item', i + 1);
					poOverage = (poOverage * quantity)/100;
					
					var qtywithPOOverage = parseFloat(quantity)+parseFloat(poOverage);
					var quantityRcv = parseFloat(form.getSubList('item').getLineItemValue('quantityreceived', i + 1));
					var RemQty = (parseFloat(quantity) - parseFloat(quantityRcv));
					var lineCnt=i+1;
					var checkinqty=GetPOcheckinqty(poName,lineno,item,poID);// case# 201412256
					if(checkinqty==null || checkinqty=='')
						checkinqty=0;
					nlapiLogExecution('ERROR', 'checkinqty', checkinqty);
					nlapiLogExecution('ERROR', 'RemQty', RemQty);
					nlapiLogExecution('ERROR', 'quantity info', 'Quantity: ' + quantity + '<br>' + 'Quantity Received: ' + quantityRcv);
					nlapiLogExecution('ERROR', 'PackCode', nlapiGetLineItemText('item', 'custcol_nswmspackcode', i + 1));
					nlapiLogExecution('ERROR', 'units', nlapiGetLineItemText('item', 'units', i + 1));
					var vitempackcode=nlapiGetLineItemText('item', 'custcol_nswmspackcode', i + 1);

					var vitemuomid=nlapiGetLineItemValue('item', 'units', i + 1);
					var vitemuom=nlapiGetLineItemText('item', 'units', i + 1);

					//var vitemuom=nlapiGetLineItemValue('item', 'units', i + 1);
                    var vPOlineLocation=nlapiGetLineItemValue('item', 'location', i + 1);
					nlapiLogExecution('ERROR', 'vPOlineLocation', vPOlineLocation);
					//Case # 20127076ï¿½Start
					if(vPOlineLocation==null || vPOlineLocation=="" || vPOlineLocation=='null')
						{
						vPOlineLocation=nlapiGetFieldValue('location');
						}
					//Case # 20127076ï¿½End
					//Case '20123235 Start ..if NsUOM are confiured, to get partial check in qty 
					 var poItemUOM=vitemuomid;
						if( poItemUOM!=null && poItemUOM!='')
						{
							var vbaseuomqty=0;
							var vuomqty=0;
							var eBizItemDims=geteBizItemDimensions(item);
							if(eBizItemDims!=null&&eBizItemDims.length>0)
							{
								nlapiLogExecution('ERROR', 'Dims Length', eBizItemDims.length);
								for(var z=0; z < eBizItemDims.length; z++)
								{
									if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
									{
										vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
									}
									nlapiLogExecution('ERROR', 'poItemUOM', poItemUOM);
									nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
									nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
									if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
									{
										vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
									}
								}
								if(vuomqty==null || vuomqty=='')
								{
									vuomqty=vbaseuomqty;
								}
								nlapiLogExecution('ERROR', 'vbaseuomqty', vbaseuomqty);
								nlapiLogExecution('ERROR', 'vuomqty', vuomqty);
								nlapiLogExecution('ERROR', 'checkinqty', checkinqty);

								if(checkinqty==null || checkinqty=='' || isNaN(checkinqty))
									checkinqty=0;
								else
									checkinqty = (parseFloat(checkinqty)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

							}
						}
						nlapiLogExecution('ERROR', 'checkinqty', checkinqty);					
						//Case '20123235 end ..
						var newOrdstatus = "Facturaci&#243;n pendiente/parcialmente recibido";

// case no 20126456ï¿½ 

					/* merged the last postatus condition from boombah acc on 25th feb 2013 by Radhika */ 
					if(postatus=='Pending Receipt'||postatus=='Partially Received' ||postatus=='Pending Billing/Partially Received'
						|| postatus=='Pending Refund/Partially Received' || postatus=='Approved by Supervisor/Pending Receipt' ||  postatus == "Recibo pendiente" || postatus == 'En espera de facturaci?n / Parcialmente Recibido' || postatus == 'Aprobado por el supervisor/Recibo pendiente' || postatus == newOrdstatus)
						//|| postatus=='Pending Refund/Partially Received' || postatus=='Approved by Supervisor/Pending Receipt' ||  postatus == "Recibo pendiente" || postatus == 'En espera de facturaciï¿½n / Parcialmente Recibido' || postatus == 'Aprobado por el supervisor/Recibo pendiente' || (postatus == 'Facturaciï¿½n pendiente/parcialmente recibido'))

					{
						//generate the hyperlink only when the line has not been fully received
						if ((quantityRcv < qtywithPOOverage) && (quantity> checkinqty)) {
							var linkURL;
							//Starts: Case#:20127180
							var isdropshipItem = 'F';
							var vdropshipfeature=false;
							var itemType;
							var ctx = nlapiGetContext();
							if(ctx != null && ctx != '')
							{
								if(ctx.getFeature('dropshipments') != null && ctx.getFeature('dropshipments') != '')
									vdropshipfeature=ctx.getFeature('dropshipments');
							}  
							
							nlapiLogExecution('ERROR', 'vdropshipfeature', vdropshipfeature);
							// check Item type is it noninventoty item or dropship Item
							if(vdropshipfeature)
							{
								nlapiLogExecution('ERROR', 'if Dropship feature is enabled', vdropshipfeature);
								var itemcolumns = nlapiLookupField('item', item , [ 'recordType','isdropshipitem']);
								itemType = itemcolumns.recordType;
								isdropshipItem = itemcolumns.isdropshipitem;
							}
							else
							{
								var itemcolumns = nlapiLookupField('item', item , ['recordType']);
								itemType = itemcolumns.recordType;
							}
							nlapiLogExecution('ERROR', 'itemType', itemType);	
							nlapiLogExecution('ERROR', 'isdropshipItem', isdropshipItem);
							//end of Case#; 20127180.
							
							
							//add the PO ID, item, and quantity to the URL
							/*linkURL = checkInURL + '&poID=' + poID;
							//linkURL = linkURL + '&poValue=' + poName;
							linkURL = linkURL + '&QtyChkn=' + checkinqty;
							linkURL = linkURL + '&QtyRem=' + RemQty;
							linkURL = linkURL + '&poLineItem=' + nlapiGetLineItemValue('item', 'item', i + 1);
							linkURL = linkURL + '&poLineQty=' + nlapiGetLineItemValue('item', 'quantity', i + 1);
							linkURL = linkURL + '&poLineNum=' + nlapiGetLineItemValue('item', 'line', i + 1);
							linkURL = linkURL + '&poSerialNos=' + nlapiGetLineItemValue('item', 'serialnumbers', i + 1);
							linkURL = linkURL + '&poItemStatus=' + nlapiGetLineItemValue('item', 'custcol_ebiznet_item_status', i + 1);
							linkURL = linkURL + '&poItemUOM=' + vitemuom;
							linkURL = linkURL + '&poItemPackcode=' + vitempackcode;         */
							nlapiLogExecution('ERROR', 'rec', rec);	
							if(rec !=null && rec!='')
							{//end

								if(itemType !="noninventoryitem" && isdropshipItem !="T" )
								{
									linkURL = checkInURL + '&poID=' + poID;
									//linkURL = linkURL + '&poValue=' + poName;
									linkURL = linkURL + '&QtyChk=' + checkinqty;
									linkURL = linkURL + '&QtyRem=' + RemQty;
									linkURL = linkURL + '&Item=' + nlapiGetLineItemValue('item', 'item', i + 1);
									linkURL = linkURL + '&Qty=' + nlapiGetLineItemValue('item', 'quantity', i + 1);
									linkURL = linkURL + '&LineNum=' + nlapiGetLineItemValue('item', 'line', i + 1);
									linkURL = linkURL + '&SerialNo=' + nlapiGetLineItemValue('item', 'serialnumbers', i + 1);
									linkURL = linkURL + '&skuStatus=' + nlapiGetLineItemValue('item', 'custcol_ebiznet_item_status', i + 1);
									linkURL = linkURL + '&UOM=' + vitemuomid;
									linkURL = linkURL + '&Packcode=' + vitempackcode;               
									linkURL = linkURL + '&POLineLocation=' + vPOlineLocation;
									linkURL = linkURL + '&UOMtext=' + vitemuom;

									//log the URL
									nlapiLogExecution('ERROR', 'check in url', linkURL);

									//populate the URL into the check in field as a hyperlink to the Suitelet
									form.getSubList('item').setLineItemValue('custpage_checkin_link', i + 1, '<a href="' + linkURL + '">Check In</a>');
								}
							}
						}

						}
					}
				}
			}
		}
	}
}


function checkPOOverage_ReceiptType(POId,receiptType){
	nlapiLogExecution('ERROR','PO Internal Id', POId);
	var poOverage = 0;

	/*var poFields = ['custbody_nswmsporeceipttype'];
	var poColumns = nlapiLookupField('transaction', POId, poFields);

	var receiptType = poColumns.custbody_nswmsporeceipttype;*/

	nlapiLogExecution('ERROR','here', receiptType);

	if (receiptType != "" && receiptType != null) 
	{
		var receiptFields = ['custrecord_po_overages'];
		var receiptColumns = nlapiLookupField('customrecord_ebiznet_receipt_type', receiptType, receiptFields);

		nlapiLogExecution('ERROR','here', receiptType);

		poOverage = receiptColumns.custrecord_po_overages;
	}
	nlapiLogExecution('ERROR','Out of checkPOOverage_ReceiptType', poOverage);
	return poOverage;
}

function GetPOcheckinqty(poName,lineno,item,poid)
{

	var POcheckinqty;

	var filter=new Array();
//case # 20124143 
	if(poName!=null && poName!='')
	{
		filter.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no',null,'equalto',poid));
		filter.push(new nlobjSearchFilter('custrecord_orderlinedetails_order_no',null,'is',poName));
		filter.push(new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no',null,'equalto',lineno));
		filter.push(new nlobjSearchFilter('custrecord_orderlinedetails_item',null,'anyof',item));
		nlapiLogExecution('ERROR','poName', poName);
		nlapiLogExecution('ERROR','lineno', lineno);
		nlapiLogExecution('ERROR','item', item);

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty');

		var searchrecord=nlapiSearchRecord('customrecord_ebiznet_order_line_details',null,filter,column);
		if(searchrecord!=null && searchrecord!='')
		{
			POcheckinqty=searchrecord[0].getValue('custrecord_orderlinedetails_checkin_qty');
		}
	}
// case #  20124143 end
	return POcheckinqty;
}


/**
 * @param ItemArray
 * @returns {Array}
 */
function vGetPOOverage(ItemArray)
{
	try
	{
		nlapiLogExecution('ERROR','Into vGetPOOverage ItemList',ItemArray);
		var POOverageArray=new Array();
		/*var poItemFields = ['custitem_ebizpo_overage'];
		var poItemColumns = nlapiLookupField('item', ItemId, poItemFields);

		vPOoverageQty   = poItemColumns.custitem_ebizpo_overage;*/

		var filter=new Array();
		filter.push(new nlobjSearchFilter('internalid',null,'anyof',ItemArray));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custitem_ebizpo_overage');

		var searchrecord=nlapiSearchRecord('item',null,filter,column);

		if(searchrecord!=null&&searchrecord!="")
		{
			for ( var i = 0; i < searchrecord.length; i++)
			{
				POOverageArray.push(searchrecord[i].getValue('custitem_ebizpo_overage'));
			}
		}
		nlapiLogExecution('ERROR','endof vGetPOOverage ItemList_Pooverage',POOverageArray);
		return POOverageArray;


	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in vGetPOOverage',exp);	
	}

}
function poBeforeSubmit()
{	
	var ctx = nlapiGetContext();
	nlapiLogExecution('ERROR','ctx.getExecutionContext()',ctx.getExecutionContext());
	nlapiLogExecution('ERROR','type',type);
	if (ctx.getExecutionContext() == 'userinterface' && (type == 'edit' || type == 'xedit' || type=='create'))
	{
		nlapiLogExecution('DEBUG','Into poBeforeSubmit function','');

		


		for(var i =0; i < nlapiGetLineItemCount('item'); i++)
		{
			var LineLocation = nlapiGetLineItemValue('item', 'location', i + 1);
			var LineItemStatus = nlapiGetLineItemValue('item', 'custcol_ebiznet_item_status', i + 1);
			var selecteditem = nlapiGetLineItemValue('item', 'item', i + 1);

			//ItemArrayList.push(nlapiGetLineItemValue('item', 'item', j + 1));
			nlapiLogExecution('ERROR','LineItemStatus',LineItemStatus);
			nlapiLogExecution('ERROR','selecteditem',selecteditem);
			nlapiLogExecution('ERROR','LineLocation',LineLocation);
			if(LineLocation==null || LineLocation=="" || LineLocation=='null')
			{
				LineLocation=nlapiGetFieldValue('location');
			}

			if(LineItemStatus == null || LineItemStatus == '')
			{

				if(selecteditem != null && selecteditem != '')
				{
					var searchresult = SetItemStatus("PurchaseOrder", selecteditem, LineLocation, null);

					if (searchresult != null && searchresult != '') 
					{
						nlapiSetLineItemValue('item', 'custcol_ebiznet_item_status', i+1,searchresult[0]);
						if(searchresult[1] != null && searchresult[1] != '')
							nlapiSetLineItemValue('item', 'custcol_nswmspackcode',i+1,searchresult[1]);
					}

				}
			}
		}




	}






}
// Case# 20148014 starts 
function CheckPO(Poid)
{
	nlapiLogExecution('ERROR','Poid',Poid);
	var vresult=false;
	var transactionFilters = new Array();
	transactionFilters[0] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', Poid);
//	transactionFilters[1] = new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', Lineno);
	var transactionColumns = new Array();
	transactionColumns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_ebiz_sku_no');
	transactionColumns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_orderline_no');
	var transactionSearchresults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, transactionFilters, transactionColumns);
	if(transactionSearchresults!=null && transactionSearchresults!=''&& transactionSearchresults.length>0)
	{
		vresult=true;
	}
	nlapiLogExecution('ERROR','vresult',vresult);
	return vresult;
}
// Case# 20148014 ends


function isEmpty(val) {
	return (val == null || val == '' || val == 'null');
}
function notEmpty(tmp) {
	return !isEmpty(tmp);
}
