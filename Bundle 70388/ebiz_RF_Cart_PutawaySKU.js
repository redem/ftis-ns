/****************************************************************
  eBizNET Solutions Inc
 ****************************************************************************
 *
 *� $Source: /cvs/products/eBizNET4.5/EoN/RF/Inbound/Suitelet/ebiz_RF_Cart_PutawaySKU.js,v $
 *� $Revision: 1.2.2.5.4.4.2.7 $
 *� $Date: 2014/06/20 14:59:48 $
 *� $Author: skavuri $
 *� $Name: t_NSWMS_2014_1_3_125 $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_RF_Cart_PutawaySKU.js,v $
 *� Revision 1.2.2.5.4.4.2.7  2014/06/20 14:59:48  skavuri
 *� Case # 20148882 SB Issue Fixed
 *�
 *� Revision 1.2.2.5.4.4.2.6  2014/06/13 07:30:31  skavuri
 *� Case# 20148882 (added Focus Functionality for Textbox)
 *�
 *� Revision 1.2.2.5.4.4.2.5  2014/05/30 00:26:48  nneelam
 *� case#  20148622
 *� Stanadard Bundle Issue Fix.
 *�
 *� Revision 1.2.2.5.4.4.2.4  2013/06/11 14:30:41  schepuri
 *� Error Code Change ERROR to DEBUG
 *�
 *� Revision 1.2.2.5.4.4.2.3  2013/04/17 16:04:01  skreddy
 *� CASE201112/CR201113/LOG201121
 *� added meta tag
 *�
 *� Revision 1.2.2.5.4.4.2.2  2013/03/13 13:57:16  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Time Stamp related changes.
 *�
 *� Revision 1.2.2.5.4.4.2.1  2013/03/08 14:35:02  skreddy
 *� CASE201112/CR201113/LOG201121
 *� Code merged from Endochoice as part of Standard bundle
 *�
 *� Revision 1.2.2.5.4.4  2013/02/07 15:10:34  schepuri
 *� CASE201112/CR201113/LOG201121
 *� disabling ENTER Button func added
 *�
 *� Revision 1.2.2.5.4.3  2012/09/27 10:54:36  grao
 *� CASE201112/CR201113/LOG201121
 *�
 *� Converting multiple language with given Spanish terms
 *�
 *� Revision 1.2.2.5.4.2  2012/09/26 12:46:45  grao
 *� CASE201112/CR201113/LOG201121
 *� Converting Multi language without small characters
 *�
 *� Revision 1.2.2.5.4.1  2012/09/21 14:57:16  grao
 *� CASE201112/CR201113/LOG201121
 *� Converting Multilanguage
 *�
 *� Revision 1.2.2.5  2012/05/03 21:17:46  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Production issue fixes
 *�
 *� Revision 1.2.2.4  2012/04/30 10:00:49  spendyala
 *� CASE201112/CR201113/LOG201121
 *� While Searching of Item in ItemMaster,
 *� 'name' filter is changed to 'nameinternal'
 *�
 *� Revision 1.2.2.3  2012/03/16 13:56:23  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Disable-button functionality is been added.
 *�
 *� Revision 1.2.2.2  2012/02/22 12:18:16  schepuri
 *� CASE201112/CR201113/LOG201121
 *� function Key Script code merged
 *�
 *� Revision 1.2  2012/02/16 10:36:36  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Added FunctionkeyScript
 *�
 *� Revision 1.1  2012/02/02 09:01:04  rmukkera
 *� CASE201112/CR201113/LOG201121
 *�   cartlp new file
 *�
 *
 ****************************************************************************/


function PutawaySKU(request, response){
	if (request.getMethod() == 'GET') 
	{
		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);

		//Variable Declaration
		var html = '';

		//	Get the LP # from the previous screen, which is passed as a parameter		
		var getCartLPNo = request.getParameter('custparam_cartno');
		nlapiLogExecution('DEBUG', 'Into Request', getCartLPNo);

		var getLPNo = request.getParameter('custparam_lpno');
		nlapiLogExecution('DEBUG', 'Into Request', getLPNo);

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4;
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "LICENCIA DE CARRO MATR&&#205;#205;CULA";
			st2 = "INGRESAR / ESCANEO DEL ART&#205;CULO";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";


		}
		else
		{
			st0 = "";
			st1 = "CART LP #";
			st2 = "ENTER/SCAN ITEM";
			st3 = "SEND";
			st4 = "PREV";

		}


		var functionkeyHtml=getFunctionkeyScript('_rf_putaway_sku'); 
		html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		//html = html + " document.getElementById('enteritem').focus();";   

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_putaway_sku' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + " : <label>" + getCartLPNo + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2;
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		html = html + "				<input type='hidden' name='hdnCartLPNo' value=" + getCartLPNo + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enteritem' id='enteritem' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>SEND <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enteritem').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');
		
		var vbegintime = TimeStamp();
		
		nlapiLogExecution('DEBUG', 'vbegintime', vbegintime);

		var getItem = request.getParameter('enteritem');

		var getCartLPNo = request.getParameter('hdnCartLPNo');		

		var CartLPfilters = new Array();
		CartLPfilters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [6,2]);
		CartLPfilters[1] = new nlobjSearchFilter('custrecord_transport_lp', null, 'is', getCartLPNo);

		var CartLPColumns = new Array();
		CartLPColumns[0] = new nlobjSearchColumn('custrecord_transport_lp');

		var CartLPSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, CartLPfilters, CartLPColumns);

		if (CartLPSearchResults != null && CartLPSearchResults.length > 0) 
		{
			nlapiLogExecution('DEBUG', 'CartLPSearchResults ', 'Length is not null');

			var getRecordCount = CartLPSearchResults.length;
		}

		var getLPNo = request.getParameter('custparam_lpno');
		nlapiLogExecution('DEBUG', 'Into Request', getLPNo);

		// This variable is to hold the SKU entered.
		var POarray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);


		var st5;
		if( getLanguage == 'es_ES')
		{
			st5 = "ART&#205;CULO INV&#193;LIDO";

		}
		else
		{
			st5 = "INVALID ITEM";

		}
		
		POarray["custparam_item"] = getItem;		//request.getParameter('enteritem');
		POarray["custparam_error"] = st5;
		POarray["custparam_cartno"] = getCartLPNo;
		POarray["custparam_screenno"] = 'CRT10';

		var getOptedField = request.getParameter('hdnOptedField');
		POarray["custparam_option"] = getOptedField;

		nlapiLogExecution('DEBUG', 'optedField', POarray["custparam_option"]);

		try {
			// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
			// to the previous screen.
			var optedEvent = request.getParameter('cmdPrevious');

			//	if the previous button 'F7' is clicked, it has to go to the previous screen 
			//  ie., it has to go to accept PO #.
			if (optedEvent != 'F7') {
				//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
				//            if (optedEvent != '' && optedEvent != null) {
				if (POarray["custparam_item"] != "") {
					//	var itemSearch = nlapiSearchRecord('item', null, new nlobjSearchFilter('itemid', null, 'haskeywords', POarray["custparam_poitem"]));

					//Added by Ramana for validating SKU Details
					var actItemid = validatePUTAWAYSKU(POarray["custparam_item"],  "", "");
					nlapiLogExecution('DEBUG', 'After validateSKU',actItemid);

					if((actItemid==null) || (actItemid==""))
					{
						nlapiLogExecution('DEBUG', 'After validateSKU return Value::',actItemid);
					}

					//  var itemSearch = nlapiSearchRecord('item', null, new nlobjSearchFilter('itemid', null, 'is', POarray["custparam_poitem"]));

//					var itemSearch = nlapiSearchRecord('item', null, new nlobjSearchFilter('itemid', null, 'is', actItemid));

					// Changed On 30/4/12 by Suman
					var filteritem=new Array();
					filteritem.push(new nlobjSearchFilter('nameinternal', null, 'is', actItemid));
					filteritem.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
					var itemSearch = nlapiSearchRecord('item', null,filteritem,null);

					nlapiLogExecution('DEBUG', 'Length of Item Search', itemSearch.length);

					POarray["custparam_item"] = actItemid;

					if (itemSearch.length == 0) 
					{
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
					}
					else 
					{
						var ItemSearchedId;
						for (var s = 0; s < itemSearch.length; s++) 
						{
							ItemSearchedId = itemSearch[s].getId();
							nlapiLogExecution('DEBUG', 'Fetched Item ID', ItemSearchedId);
						}

						var POfilters = new Array();

//						if (getOptedField == 4) {
						POfilters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [6,2]);
						POfilters[1] = new nlobjSearchFilter('custrecord_transport_lp', null, 'is', getCartLPNo);
						POfilters[2] = new nlobjSearchFilter('custrecord_sku', null, 'is', ItemSearchedId);
//						}
//						else
//						{
//						POfilters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [6,2]);
//						POfilters[1] = new nlobjSearchFilter('custrecord_lpno', null, 'is', getLPNo);
//						POfilters[2] = new nlobjSearchFilter('custrecord_sku', null, 'is', ItemSearchedId);
//						}
						var POColumns = new Array();
						POColumns[0] = new nlobjSearchColumn('custrecord_transport_lp');
						POColumns[1] = new nlobjSearchColumn('custrecord_lpno');
						POColumns[2] = new nlobjSearchColumn('custrecord_expe_qty');
						POColumns[3] = new nlobjSearchColumn('custrecord_actbeginloc');
						POColumns[4] = new nlobjSearchColumn('custrecord_sku');
						POColumns[5] = new nlobjSearchColumn('custrecord_skudesc');

						var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, POfilters, POColumns);

						if (searchresults != null && searchresults.length > 0) {
							nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is not null');

							POarray["custparam_lpno"] = searchresults[0].getValue('custrecord_lpno');
							POarray["custparam_quantity"] = searchresults[0].getValue('custrecord_expe_qty');
							POarray["custparam_itemDescription"] = searchresults[0].getValue('custrecord_skudesc');
							POarray["custparam_location"] = searchresults[0].getValue('custrecord_actbeginloc');
							POarray["custparam_beginlocation"] = searchresults[0].getText('custrecord_actbeginloc');
							POarray["custparam_recordcount"] = getRecordCount;
							nlapiLogExecution('DEBUG', 'get RecordCount', getRecordCount);							
							POarray["custparam_recordid"] = searchresults[0].getId();
							
							//*** The following code is added by Satish.N on 12-Mar-2013 to update task begin time and user name***//
							
							var currentContext = nlapiGetContext();  
							var currentUserID = currentContext.getUser();
							nlapiLogExecution('DEBUG', 'currentUserID', currentUserID);
							
							var RecID=searchresults[0].getId();
							
							var fields=new Array();
							fields[0]='custrecord_taskassignedto';		
							fields[1]='custrecord_actualbegintime';	
							
							var Values=new Array();
							Values[0]=currentUserID;	
							Values[1]=vbegintime;	
							
							var UpdatedRecID=nlapiSubmitField('customrecord_ebiznet_trn_opentask',RecID,fields,Values);
							
							//*** Upto here ***//

							nlapiLogExecution('DEBUG', 'Data is collected', 'Moving to Putaway Location');						
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawayloc', 'customdeploy_ebiz_rf_cart_putawayloc_di', false, POarray);
						}
						else {
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
						}
					}
				}
				else {
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
				}
			}
			else {
				response.sendRedirect('SUITELET', 'customscript_rf_putaway_cartlp', 'customdeploy_rf_putaway_cartlp_di', false, POarray);
			}
		} 
		catch (e) {
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			nlapiLogExecution('DEBUG', 'SearchResults in catch block', 'Length is null');
		}
	} //end of first if condition
} //end of function.


function validatePUTAWAYSKU(itemNo,location,company)
{
	nlapiLogExecution('DEBUG', 'validateSKU:Start',itemNo);

	var actItem="";

	var invitemfilters = new Array();
	invitemfilters[0] = new nlobjSearchFilter('name',null, 'is',itemNo);
	var invLocCol = new Array();
	invLocCol[0] = new nlobjSearchColumn('itemid');

	var invitemRes = nlapiSearchRecord('item',null,invitemfilters,invLocCol);
	if(invitemRes!=null)
	{
		actItem=invitemRes[0].getValue('itemid');
	}
	else
	{					
		var invLocfilters = new Array();
		invLocfilters[0] = new nlobjSearchFilter('upccode', null, 'is', itemNo);

		var invtRes = nlapiSearchRecord('item', null, invLocfilters, invLocCol);
		if(invtRes != null)
		{
			actItem=invtRes[0].getValue('itemid');
		}
		else
		{

			nlapiLogExecution('DEBUG', 'inSide SKUALIAS',itemNo);

			var skuAliasFilters = new Array();
			skuAliasFilters[0] = new nlobjSearchFilter('name', null, 'is', itemNo);

			var skuAliasCols = new Array();
			skuAliasCols[0] = new nlobjSearchColumn('custrecord_ebiz_item');

			var skuAliasResults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, skuAliasFilters, skuAliasCols);

			if(skuAliasResults != null)
				actItem=skuAliasResults[0].getText('custrecord_ebiz_item');

		}			
	}

	return actItem;
}


