/***************************************************************************
eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Suitelet/ebiz_RMAConfirmPutaway_SL.js,v $
 *     	   $Revision: 1.3.4.2.4.4.4.28.2.5 $
 *     	   $Date: 2015/11/13 15:56:35 $
 *     	   $Author: rmukkera $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 * DESCRIPTION
 * REVISION HISTORY
 * $Log: ebiz_RMAConfirmPutaway_SL.js,v $
 * Revision 1.3.4.2.4.4.4.28.2.5  2015/11/13 15:56:35  rmukkera
 * case # 201414368
 *
 * Revision 1.3.4.2.4.4.4.28.2.4  2015/11/09 16:05:52  aanchal
 * 2015.2 Issue fix
 * 201413034
 *
 * Revision 1.3.4.2.4.4.4.28.2.3  2015/11/07 12:25:28  grao
 * 2015.2 Issue Fixes 201414413
 *
 * Revision 1.3.4.2.4.4.4.28.2.2  2015/11/06 16:07:36  aanchal
 * 2015.2 Issue Fix
 * 201413034
 *
 * Revision 1.3.4.2.4.4.4.28.2.1  2015/09/21 11:35:20  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.3.4.2.4.4.4.28  2015/07/13 13:29:44  schepuri
 * case#  201413375
 *
 * Revision 1.3.4.2.4.4.4.27  2015/07/10 13:28:38  schepuri
 * case# 201413375
 *
 * Revision 1.3.4.2.4.4.4.26  2015/05/26 13:30:22  schepuri
 * case# 201412871
 *
 * Revision 1.3.4.2.4.4.4.25  2015/03/20 15:36:34  grao
 * LP SB issue fixes  201412117
 *
 * Revision 1.3.4.2.4.4.4.24  2015/03/02 13:54:16  schepuri
 * Issue fix # 201411654
 *
 * Revision 1.3.4.2.4.4.4.23  2014/09/18 13:14:58  skavuri
 * Case# 201410425 Std Bundle issue fixed
 *
 * Revision 1.3.4.2.4.4.4.22  2014/07/11 15:45:12  sponnaganti
 * Case# 20149408 20149409
 * Compatibility Issue fix
 *
 * Revision 1.3.4.2.4.4.4.21  2014/07/01 15:25:42  skavuri
 * Case# 20148038 Compatibility Issue fixed
 *
 * Revision 1.3.4.2.4.4.4.20  2014/06/27 11:43:34  sponnaganti
 * case# 20149067 20149068 20149069
 * Compatability Test Acc issue fix
 *
 * Revision 1.3.4.2.4.4.4.19  2014/06/17 15:42:05  skavuri
 * Case# 20148038 SB Issue Fixed.
 *
 * Revision 1.3.4.2.4.4.4.18  2014/06/09 15:26:32  sponnaganti
 * case# 20148770 20148800
 * Stnd Bundle Issue Fix
 *
 * Revision 1.3.4.2.4.4.4.17  2014/04/11 15:39:55  nneelam
 * case#  20145315
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.3.4.2.4.4.4.16  2014/04/04 14:09:30  skavuri
 * Case # 20127918/20127913 issue fixed
 *
 * Revision 1.3.4.2.4.4.4.15  2014/03/25 15:44:34  nneelam
 * case#  20127824�
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.3.4.2.4.4.4.14  2014/03/24 14:38:26  rmukkera
 * Case # 20127751�
 *
 * Revision 1.3.4.2.4.4.4.13  2014/03/20 15:43:34  nneelam
 * case#  20127765
 * Standard Bundle Issue Fix.
 *
 * Revision 1.3.4.2.4.4.4.12  2014/02/19 13:25:26  sponnaganti
 * case# 20126998
 * Standard Bundle Issue (Code added from RYONET SB Acc)
 *
 * Revision 1.3.4.2.4.4.4.11  2013/12/05 15:22:16  skreddy
 * Case# 20126191,20126073, 20125924 & 20126074
 * 2014.1 stnd bundle issue fix
 *
 * Revision 1.3.4.2.4.4.4.10  2013/11/25 15:16:52  grao
 * Case# 20125920  related issue fixes in SB 2014.1
 *
 * Revision 1.3.4.2.4.4.4.9  2013/10/28 15:42:27  grao
 * Issue fix related to the 20125353
 *
 * Revision 1.3.4.2.4.4.4.8  2013/09/24 15:41:50  rmukkera
 * Case# 20124542�
 *
 * Revision 1.3.4.2.4.4.4.7  2013/09/23 15:58:01  rmukkera
 * Case# 20124544
 *
 * Revision 1.3.4.2.4.4.4.6  2013/08/28 15:55:37  nneelam
 * Case#.  20124066,20124067
 * RMA PutAway Report
 * RMA Serial Item Exception Item Receipt Fail
 *
 * Revision 1.3.4.2.4.4.4.5  2013/06/17 15:03:00  nneelam
 * CASE201112/CR201113/LOG201121
 * Standard Bundle, BuildShip Units:Issue No. 20123030
 *
 * Revision 1.3.4.2.4.4.4.4  2013/06/14 15:51:57  nneelam
 * CASE201112/CR201113/LOG201121
 * RMA Related issue fixes
 *
 * Revision 1.3.4.2.4.4.4.3  2013/05/16 11:25:03  spendyala
 * CASE201112/CR201113/LOG201121
 * InvtRef# field of opentask record will be updated
 * to putaway task, once inbound process in completed
 *
 * Revision 1.3.4.2.4.4.4.2  2013/03/19 11:46:47  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.3.4.2.4.4.4.1  2013/03/01 14:34:59  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.3.4.2.4.4  2013/01/24 14:48:07  grao
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.3.4.2.4.3  2013/01/04 14:53:48  grao
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.3.4.2.4.2  2012/11/01 14:55:15  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.3.4.2.4.1  2012/10/23 17:08:38  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form branch.
 *
 * Revision 1.3.4.2  2012/04/20 13:02:12  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.3.4.1  2012/04/10 21:17:04  spendyala
 * CASE201112/CR201113/LOG201121
 * issue related toTrue Lot depending upon RuleValue is fixed.
 *
 * Revision 1.3  2011/11/13 00:16:15  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Inventory Deletion
 *
 * Revision 1.2  2011/10/18 16:54:38  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * TO Generate Locations
 *
 * Revision 1.1  2011/09/22 08:36:56  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RMA Functionality New Files
 *
 *
 *****************************************************************************/
function ebiznet_RMAConfirmPutaway_UE(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('RMA Confirm Putaway');

		//$LN, LOT/Batch # Validation...
		//form.setScript('customscript_chknlpvalidation');
		form.setScript('customscript_chknlpputwvalidation');		

		//Calling Client Script for validating Location
		// form.setScript('customscript_cpclientvalidations');
		//To get PO location and recordType.

		var fields = ['recordType', 'location','custbody_nswms_company'];
		nlapiLogExecution('ERROR', 'PO ID', request.getParameter('custparam_poid'));
		var columns = nlapiLookupField('transaction', request.getParameter('custparam_poid'), fields);
		var tranType = columns.recordType;
		var trnlocation = columns.location;
		var vcompany= columns.custbody_nswms_company;

		nlapiLogExecution('ERROR', 'Tran Type',tranType);
		nlapiLogExecution('ERROR', 'Location',trnlocation);
		nlapiLogExecution('ERROR', 'Company',vcompany);
		var pointid;
		if (request.getParameter('custparam_poid')) {
			pointid = request.getParameter('custparam_poid');
		}
		else { // pointid = form.getField('custpage_po').getSelectOptions().getId();
			nlapiLogExecution('ERROR', 'ELSE');
		}
		nlapiLogExecution('ERROR', 'pointid',pointid);

		/*
        //LN, add PO drop down
        var selectPO = form.addField('custpage_po', 'select', 'PO #');
        //LN,Get distinct po's and its IDs into dropdown

        var filterspo = new Array();
        filterspo[0] = new nlobjSearchFilter('mainline', null, 'is', 'T');
        var searchresults = nlapiSearchRecord('purchaseorder', null, filterspo, new nlobjSearchColumn('tranid'));
        for (var i = 0; i < Math.min(1000, searchresults.length); i++) {
            selectPO.addSelectOption(searchresults[i].getId(), searchresults[i].getValue('tranid'));
        }
        selectPO.setDefaultValue(pointid);
		 */

		var posearchresults = nlapiLoadRecord('returnauthorization', request.getParameter('custparam_poid')); //1020
		var vponum= posearchresults.getFieldValue('tranid');

		var VarPO = form.addField('custpage_po', 'text', 'RMA #');
		VarPO.setDefaultValue(vponum);
		//VarPO.setDisplayType('inline');


		//Added for Location select field
		var polocation = form.addField('custpage_loc_id', 'select', 'Location', 'location');
		polocation.setDefaultValue(trnlocation);
		polocation.setMandatory(true);

		try
		{
			var trantypefld = form.addField('custpage_trantype', 'text', 'Trantype');
			trantypefld.setDefaultValue(tranType);
			trantypefld.setDisplayType('hidden');
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR', 'Exception trantypefld',exp);
		}

		try
		{
			var chknCompany = form.addField('custpage_compid', 'select', 'Company', 'customrecord_ebiznet_company');
			chknCompany.setDefaultValue(vcompany);
			chknCompany.setDisplayType('inline');
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR', 'Exception chknCompany',exp);
		}

		var taskEmployee= form.addField('custpage_taskassignedempid','select','Task assigned to','employee');

		//nlapiLogExecution('ERROR', 'GetPARAM', request.getParameter('custparam_poid'));
		//nlapiLogExecution('ERROR', 'Select Field', pointid);
		/*
         LN,Defining SubList lines
		 */
		//loading LOT/Batch # from PO lines
		var loadedrec = nlapiLoadRecord(tranType, pointid);

		/*  var batchArr = new Array();
         for (var i = 1; i <= loadedrec.getLineItemCount('item'); i++) {
         batchArr[i] = loadedrec.getLineItemValue('item', 'serialnumbers', i);
         nlapiLogExecution('ERROR', 'batchArray', batchArr[i]);
         } */

		var sublist = form.addSubList("custpage_items", "inlineeditor", "Confirm Putaway");
		sublist.addField("custpage_polocgen", "checkbox", "Confirm").setDefaultValue('T');
		sublist.addField("custpage_line", "text", "Line#");
		sublist.addField("custpage_chkn_lp", "text", "LP#");
		sublist.addField("custpage_iteminternalid", "select", "Item", "item");//.setDisplayType('hidden');
		sublist.addField("custpage_itemstatus", "select", "Item Status", "customrecord_ebiznet_sku_status").setDefaultValue('1');
		sublist.addField("custpage_item_pc", "text", "Pack Code").setDefaultValue('1');
		//code added from FactoryMation on 28Feb13 by santosh ,  Lot#  label name changed 
		sublist.addField("custpage_lotbatch", "text", "Lot/Batch #");
		//upto here
		sublist.addField("custpage_quantity", "text", "Quantity");
	//	sublist.addField("custpage_location", "select", "Bin Location", "customrecord_ebiznet_location").setMandatory(true);
		sublist.addField("custpage_location", "select", "Bin Location", "customrecord_ebiznet_location");

		sublist.addField("custpage_pointid", "text", "poIntid").setDisplayType('hidden');
		sublist.addField("custpage_poskuid", "text", "skuIntid").setDisplayType('hidden');
		sublist.addField("custpage_poskudesc", "text", "skudesc").setDisplayType('hidden');
		sublist.addField("custpage_serialno", "text", "SerialNo").setDisplayType('hidden');
		sublist.addField("custpage_serialnos", "textarea", "Serial #");//.setDisplayType('entry');
		sublist.addField("custpage_batchwithqty", "textarea", "Batch with quantiy").setDisplayType('hidden');
		sublist.addField("custpage_line_item", "text", "LineItem").setDisplayType('hidden');
		sublist.addField("custpage_company", "text", "Company").setDisplayType('hidden');
		sublist.addField("custpage_oldlocation", "select", "old Bin Location", "customrecord_ebiznet_location").setDisplayType('hidden');
		sublist.addField("custpage_opentaskid", "text", "Open Task ID").setDisplayType('hidden');
		//case# 20148770 starts
		sublist.addField("custpage_oldqty", "text", "Old Qty").setDisplayType('hidden');
		sublist.addField("custpage_name", "text", "name").setDisplayType('hidden');
		sublist.addField("custpage_displayedquantity", "text", "Old Qty").setDisplayType('hidden');
		
		//case# 20148770 ends
		//var autobutton = form.addButton('custpage_confirmputaway', 'Save');

		var button = form.addSubmitButton('Confirm');

		//LN,Serial no's from serial Entry
		var arrayLP = new Array();
		var lpcolumns = new Array();
		lpcolumns[0] = new nlobjSearchColumn('custrecord_serialparentid');
		lpcolumns[1] = new nlobjSearchColumn('custrecord_serialnumber');
		
		
		//case# 20124066 start
		var lpfilters=new Array();
		if(pointid!=null && pointid!='')
		lpfilters = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
		//case# 20124066 end
		
		var lpsearchresults = nlapiSearchRecord('customrecord_ebiznetserialentry', null, lpfilters, lpcolumns);
		for (var i = 0; lpsearchresults != null && i < lpsearchresults.length; i++) {
			var SerialLP = new Array();
			SerialLP[0] = lpsearchresults[i].getValue('custrecord_serialparentid');
			SerialLP[1] = lpsearchresults[i].getValue('custrecord_serialnumber');
			arrayLP.push(SerialLP);

		}

		//LN, define search filters
		var filters = new Array();
		//var status = new Array("N", "L");
		if (pointid != "" && pointid != null) {
			filters[0] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', pointid);
		}
		else {
			filters[0] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', 1527);
		}

		filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);
		filters[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2, 6]);
		//filters[2] = new nlobjSearchFilter('custrecord_actendloc', null, 'is', isNaN);
		// filters[3] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_line_no');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpno');
		columns[2] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[3] = new nlobjSearchColumn('custrecord_sku_status');
		columns[4] = new nlobjSearchColumn('custrecord_batch_no');
		columns[5] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[6] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[7] = new nlobjSearchColumn('custrecord_sku'); //Retreive SKU Details
		columns[8] = new nlobjSearchColumn('custrecord_skudesc'); //Retreive SKU desc Details
		columns[9] = new nlobjSearchColumn('custrecord_packcode');
		//Batch with qty.
		columns[10] = new nlobjSearchColumn('custrecord_lotnowithquantity');
		columns[11] = new nlobjSearchColumn('custrecord_comp_id');
		columns[12]=new nlobjSearchColumn('custrecord_serial_no');
		//case# 20148770 starts
		columns[13]=new nlobjSearchColumn('name');
		//case# 20148770 ends
		columns[0].setSort();

		//LN, execute the  search, passing null filter and return columns
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
		var lp, line, itemname, itemstatus, lotbatch, qty, loc, itemid, itemdesc, itempackcode,batchwithqty,opentaskid,name;
		var rcptqty = new Array();

		/*
         LN, Searching records from custom record and dynamically adding to the sublist lines
		 */

		for (var i = 0; searchresults != null && i < searchresults.length; i++) {		
			var searchresult = searchresults[i];
			/*
             if (searchresult.getValue('custrecord_line_no') == i) {
             rcptqty[i] += searchresult.getValue('custrecord_expe_qty');
             }
			 */
			opentaskid = searchresult.getId();
			line = searchresult.getValue('custrecord_line_no');
			lp = searchresult.getValue('custrecord_ebiz_lpno');
			itemid = searchresult.getValue('custrecord_ebiz_sku_no');
			itemstatus = searchresult.getValue('custrecord_sku_status');
			lotbatch = searchresult.getValue('custrecord_batch_no');
			qty = searchresult.getValue('custrecord_expe_qty');
			loc = searchresult.getValue('custrecord_actbeginloc');
			itemname = searchresult.getValue('custrecord_sku');
			itemdesc = searchresult.getValue('custrecord_skudesc');
			itempackcode = searchresult.getValue('custrecord_packcode');
			batchwithqty =  searchresult.getValue('custrecord_lotnowithquantity');
			vserialno = searchresult.getValue('custrecord_serial_no');
			//case# 20148770 starts
			name = searchresult.getValue('name');
			//case# 20148770 ends
			retserialcsv = getSerialNoCSV(arrayLP, lp);
			//nlapiLogExecution('DEBUG','LOC',searchresult.getValue('custrecord_actbeginloc'));
			vCompany =  searchresult.getValue('custrecord_comp_id');
			form.getSubList('custpage_items').setLineItemValue('custpage_line', i + 1, line);
			form.getSubList('custpage_items').setLineItemValue('custpage_chkn_lp', i + 1, lp);
			form.getSubList('custpage_items').setLineItemValue('custpage_iteminternalid', i + 1, itemid);
			form.getSubList('custpage_items').setLineItemValue('custpage_itemstatus', i + 1, itemstatus);
			form.getSubList('custpage_items').setLineItemValue('custpage_item_pc', i + 1, itempackcode);
			form.getSubList('custpage_items').setLineItemValue('custpage_lotbatch', i + 1, lotbatch);
			form.getSubList('custpage_items').setLineItemValue('custpage_quantity', i + 1, qty);
			form.getSubList('custpage_items').setLineItemValue('custpage_location', i + 1, loc);
			form.getSubList('custpage_items').setLineItemValue('custpage_pointid', i + 1, pointid);
			form.getSubList('custpage_items').setLineItemValue('custpage_poskuid', i + 1, itemid);
			form.getSubList('custpage_items').setLineItemValue('custpage_poskudesc', i + 1, itemdesc);
			//form.getSubList('custpage_items').setLineItemValue('custpage_serialnos', i + 1, retserialcsv);
			var columns = nlapiLookupField('item', itemid, [ 'recordType','custitem_ebizserialout','custitem_ebizserialin' ]);
			var ItemType = columns.recordType;
			var serialout = columns.custitem_ebizserialout;
			var serialin = columns.custitem_ebizserialin;
			//if (ItemType == "serializedinventoryitem" || serialout == 'T') {
			if (ItemType == "serializedinventoryitem" || serialin == 'T') {
				if(vserialno !=null && vserialno !='')
				{
					var vserialQty = vserialno.split(',');
					if(vserialQty.length>0)
					{ 
						var vSerialCount="";
						vSerialCount=parseInt(vserialQty.length);
						nlapiLogExecution('ERROR', 'vSerialCount',vSerialCount);
						nlapiLogExecution('ERROR', 'vSerialCount',parseFloat(vSerialCount));
						//case # 20145315
						form.getSubList('custpage_items').setLineItemValue('custpage_serialnos', i + 1, vserialno + ",");
						form.getSubList('custpage_items').setLineItemValue('custpage_quantity', i + 1,vSerialCount.toString());
					}
				
				}
				else
				{
					form.getSubList('custpage_items').setLineItemValue('custpage_serialnos', i + 1, retserialcsv);
					form.getSubList('custpage_items').setLineItemValue('custpage_quantity', i + 1, qty);
				}
				
			}
			else
			{
				form.getSubList('custpage_items').setLineItemValue('custpage_serialnos', i + 1, retserialcsv);
				form.getSubList('custpage_items').setLineItemValue('custpage_quantity', i + 1, qty);
			}
			
			form.getSubList('custpage_items').setLineItemValue('custpage_batchwithqty', i + 1, batchwithqty);
			form.getSubList('custpage_items').setLineItemValue('custpage_line_item', i + 1, itemid);
			form.getSubList('custpage_items').setLineItemValue('custpage_company', i + 1, vCompany);	
			form.getSubList('custpage_items').setLineItemValue('custpage_oldlocation', i + 1, loc);
			form.getSubList('custpage_items').setLineItemValue('custpage_opentaskid', i + 1, opentaskid);
			//case# 20148770 starts 
			form.getSubList('custpage_items').setLineItemValue('custpage_oldqty', i + 1, qty);
			form.getSubList('custpage_items').setLineItemValue('custpage_displayedquantity', i + 1, qty);
			form.getSubList('custpage_items').setLineItemValue('custpage_name', i + 1, name);
			//case# 20148770 ends

		}
		nlapiSelectLineItem('custpage_items', 1);
		// form.addPageLink('crosslink', 'Back to PO Check-In', nlapiResolveURL('RECORD', 'purchaseorder', request.getParameter('custparam_poid')))
		form.addPageLink('crosslink', 'Back to PO', nlapiResolveURL('RECORD', tranType, request.getParameter('custparam_poid')));

		response.writePage(form);
	}
	else {
		// Code changes by Saritha.
		//Getting PO Internal Id.
		var pointid = request.getLineItemValue('custpage_items', 'custpage_pointid', 1);
		nlapiLogExecution('ERROR', 'PO Internal ID', pointid);

		//var tranType= nlapiLookupField('transaction',pointid,'recordType');

		var poloc = request.getParameter('custpage_loc_id');
		nlapiLogExecution('ERROR', 'poloc id is', poloc);
		var tranType = request.getParameter('custpage_trantype');
		var PutwCompany = request.getParameter('custpage_compid');

		nlapiLogExecution('ERROR', 'PutwCompany', PutwCompany);
		var poLocRec = nlapiLoadRecord(tranType, pointid);
		var poLocation = poLocRec.getFieldValue('location');
		nlapiLogExecution('ERROR', 'PO Location ID', poLocation);

		// Employee Id value
		var empId= request.getParameter('custpage_taskassignedempid');
		//New consolidated Quantity.
		var i = -1;
		var arrQty = new Array();
		var arrLine = new Array();
		var arrId = new Array();
		var arrBatchwithQty= new Array();
		//case# 20148770 starts
		var arrLpno = new Array();
		//case# 20148770 ends
		var confirmLotToNS='Y';
		confirmLotToNS=GetConfirmLotToNS(poloc);

		try {
			var lineCnt = request.getLineItemCount('custpage_items');
			nlapiLogExecution('ERROR', 'Consolidated lineCnt', lineCnt);
			var Lineno;
			var TempLineno;
			var ItemId;
			var TempItemId;
			var boolFlag = false;
			var Qty = 0;
			var stempLine = 0;
			var bwqty=""; //This variable is used for storing batch with qty field for each line making that space separated.
			var ItemStatusinfo;			
			var arrItemStatus = new Array();
			var arrItemQty = new Array();
			var tempItemStatus;
			var arrItemStat = new Array();	
			var itemstatusexistsflag='N';
			var arrBatch=new Array();
			var arrBatchNew=new Array();
			var vBatch;
			var prevBatch;
			var vlotQty=0;
			var lpno;
	var itemarray = new Array();
			nlapiLogExecution('ERROR', 'lineCnt ', lineCnt);
			var LotArrForAdvBin = new Array();//Newly added for advBinManagement
			var LotQtyArrForAdvBin = new Array();//Newly added for advBinManagement			
			var vTempBatchArr=new Array();//Newly added for advBinManagement
			var vTempBatchQtyArr=new Array();//Newly added for advBinManagement
			for (var s = 1; s <= lineCnt ; s++) 
			{
				var confirmFlag= request.getLineItemValue('custpage_items', 'custpage_polocgen', s);	
				//
				//case# 20148770 starts
				var voldqty = request.getLineItemValue('custpage_items', 'custpage_oldqty', s);
				lpno = request.getLineItemValue('custpage_items', 'custpage_chkn_lp', s);
				//case# 20148770 ends
				nlapiLogExecution('ERROR', 'Confirm Flag', confirmFlag);
				if(confirmFlag == 'T')
				{		
					Lineno = request.getLineItemValue('custpage_items', 'custpage_line', s);
					ItemId = request.getLineItemValue('custpage_items', 'custpage_iteminternalid', s);		
					ItemStatusinfo =  request.getLineItemValue('custpage_items','custpage_itemstatus',s); //Added by ramana
itemarray.push(ItemId);
					if (stempLine == 0) 
					{						
						stempLine = 1;
						TempLineno = Lineno;
						TempItemId = ItemId;
						tempItemStatus = ItemStatusinfo;
						prevBatch=request.getLineItemValue('custpage_items', 'custpage_lotbatch', s);
					}
					//if ((TempLineno == Lineno) && (tempItemStatus == ItemStatusinfo))
					if ( (TempItemId == ItemId ) && (TempLineno == Lineno) && (tempItemStatus == ItemStatusinfo))
					{
						Qty += parseFloat(request.getLineItemValue('custpage_items', 'custpage_quantity', s));
						nlapiLogExecution('ERROR', 'ConsQty1', Qty);
						if(bwqty != null && bwqty != "")
							bwqty+= "," + request.getLineItemValue('custpage_items', 'custpage_batchwithqty', s);
						else
							bwqty = request.getLineItemValue('custpage_items', 'custpage_batchwithqty', s);
						nlapiLogExecution('ERROR', 'batch with qty', bwqty);	
						ItemStatusinfo =  request.getLineItemValue('custpage_items','custpage_itemstatus',s); //Added by ramana
						if(prevBatch==request.getLineItemValue('custpage_items', 'custpage_lotbatch', s))
						{
							if(vlotQty != null && vlotQty != "" && vlotQty != 0)
								vlotQty=parseFloat(vlotQty)+parseFloat(request.getLineItemValue('custpage_items', 'custpage_quantity', s));
							else
								vlotQty=parseFloat(request.getLineItemValue('custpage_items', 'custpage_quantity', s));
						}
						else
						{
							if(vBatch != null && vBatch != "")
								vBatch= vBatch + " " + prevBatch + "(" + vlotQty + ")";
							else
								vBatch=prevBatch + "(" + vlotQty + ")";

							vTempBatchArr.push(prevBatch);
							vTempBatchQtyArr.push(vlotQty);
							prevBatch=request.getLineItemValue('custpage_items', 'custpage_lotbatch', s);
							vlotQty= parseFloat(request.getLineItemValue('custpage_items', 'custpage_quantity', s));
						}
					}
					else 
					{
						i = i + 1;
						nlapiLogExecution('ERROR', ' else part Value of i and Qty in else', Qty + 'vale of i ' + i + ' Item Status ' + tempItemStatus );
						if(vBatch != null && vBatch != "")
							vBatch= vBatch + " " + prevBatch + "(" + vlotQty + ")";
						else
							vBatch=prevBatch + "(" + vlotQty + ")";

						vTempBatchArr.push(prevBatch);
						vTempBatchQtyArr.push(vlotQty);
						
						LotArrForAdvBin[i]=vTempBatchArr;
						LotQtyArrForAdvBin[i]=vTempBatchQtyArr;
						vTempBatchArr=new Array();
						vTempBatchQtyArr=new Array();

						arrQty[i] = Qty;
						arrLine[i] = TempLineno;
						arrId[i] = TempItemId;
						arrBatchwithQty[i]= bwqty;
						arrItemStatus[i]=tempItemStatus;
						Qty = 0; //make it null for further loop.
						bwqty="";
						arrBatch[i]=vBatch;
						arrBatchNew[i]=prevBatch;
						vBatch="";
						prevBatch=request.getLineItemValue('custpage_items', 'custpage_lotbatch', s);
						vlotQty= parseFloat(request.getLineItemValue('custpage_items', 'custpage_quantity', s));

						//storing distinct item status like(Damage,Good,Repair etc) into Array(arrItemStat).
						//Which is required for fetching items map location from item status custom record.
						//and need to pass item maplocation into NS Location. 
						//this Array(arrItemStat) will be used while calling nlapiTransformRecord record.

						itemstatusexistsflag='N';	
						var arrLength;						
						arrLength=arrItemStat.length;
						nlapiLogExecution('ERROR', 'arrLength:: ', arrLength);

						if(arrLength>0)
						{						
							for (var k=0;k<arrLength; k++)
							{
								if(tempItemStatus==arrItemStat[k])
								{
									itemstatusexistsflag='Y';
								}							
							}
							if(itemstatusexistsflag=='N')
							{
								arrItemStat[arrLength]=tempItemStatus;
							}
						}						
						else
						{
							arrItemStat[arrLength]=tempItemStatus;											
						}


						//case# 20148770 starts
						lpno = request.getLineItemValue('custpage_items', 'custpage_chkn_lp', s);
						//case# 20148770 ends
						Qty = parseFloat(request.getLineItemValue('custpage_items', 'custpage_quantity', s));
						Lineno = request.getLineItemValue('custpage_items', 'custpage_line', s);
						ItemId = request.getLineItemValue('custpage_items', 'custpage_iteminternalid', s);
						bwqty= request.getLineItemValue('custpage_items', 'custpage_batchwithqty', s);
						nlapiLogExecution('ERROR', 'bwqty', bwqty);	
						ItemStatusinfo =  request.getLineItemValue('custpage_items','custpage_itemstatus',s); //Added by ramana
						TempLineno = Lineno;
						TempItemId = ItemId;
						tempItemStatus=ItemStatusinfo;
					}
					//case# 20148770 starts
					arrLpno[arrLpno.length] = lpno;
					//case# 20148770 ends
				}

			}


			i++;
			arrQty[i] = Qty;
			arrLine[i] = Lineno;
			arrId[i] = ItemId;
			arrBatchwithQty[i]= bwqty;
			arrItemStatus[i]=ItemStatusinfo;

			if(vBatch != null && vBatch != "")
				vBatch= vBatch + " " + prevBatch + "(" + vlotQty + ")";
			else
				vBatch=prevBatch + "(" + vlotQty + ")";

			arrBatch[i]=vBatch;
			arrBatchNew[i]=prevBatch;
			
			vTempBatchArr.push(prevBatch);
			vTempBatchQtyArr.push(vlotQty);
			LotArrForAdvBin[i]=vTempBatchArr;
			LotQtyArrForAdvBin[i]=vTempBatchQtyArr;
			vTempBatchArr=new Array();
			vTempBatchQtyArr=new Array();

			nlapiLogExecution('ERROR',"ARRAY BATCH",arrBatch);
			for ( var x = 0; x < arrBatch.length; x++) {
				nlapiLogExecution('ERROR',"ARRAY BATCH["+x+"]",arrBatch[x]);	
			}
			//storing distinct item status like(Damage,Good,Repair etc) into Array(arrItemStat).
			//Which is required for fetching items map location from item status custom record.
			//and need to pass item maplocation into NS Location. 
			//this Array(arrItemStat) will be used while calling nlapiTransformRecord record.
			itemstatusexistsflag1='N';	
			var arrLength1;			
			arrLength1=arrItemStat.length;
			if(arrLength1>0)
			{	
				for (var k=0;k<arrLength1; k++)
				{
					if(ItemStatusinfo==arrItemStat[k])
					{
						itemstatusexistsflag1='Y';
					}			
				}
				if(itemstatusexistsflag1=='N')
				{
					arrItemStat[arrLength1]=ItemStatusinfo;
				}
			}
			else
			{
				arrItemStat[arrLength1]=ItemStatusinfo;								
			}

			nlapiLogExecution('ERROR', 'arrItemStatus:: ', arrItemStatus.length);

			nlapiLogExecution('ERROR', 'After Loop Value of i and Qty in else', Qty + 'vale of i ' + i + ' Item Status ' + tempItemStatus );
			nlapiLogExecution('ERROR', 'Value of i and Qty if s is cnt', arrQty[i] + 'vale of i ' + i + 'Item Id' + arrId[i]+'Batch with Quantity'+bwqty);			

		} 
		catch (exp) {
			nlapiLogExecution('ERROR', 'Err in summing Qty', exp);
		}




		//  old code 
		/*
		try {
    		var lineCnt = request.getLineItemCount('custpage_items');
    		nlapiLogExecution('ERROR', 'Consolidated lineCnt', lineCnt);
    		var Lineno;
    		var TempLineno;
    		var ItemId;
    		var TempItemId;
    		var boolFlag = false;
    		var Qty = 0;
    		var stempLine = 0;
    		var bwqty=""; //This variable is used for storing batch with qty field for each line making that space separated.
    		var ItemStatusinfo;			
			var arrItemStatus = new Array();


            for (var s = 1; s <= lineCnt + 1; s++) {
            	var confirmFlag= request.getLineItemValue('custpage_items', 'custpage_polocgen', s);
            	nlapiLogExecution('ERROR', 'Confirm Flag', confirmFlag);
            	//if(confirmFlag == 'T')
            	//{
                if (s == lineCnt + 1) {
    					i++;
    					arrQty[i] = Qty;
    					arrLine[i] = Lineno;
    					arrId[i] = ItemId;
    					arrBatchwithQty[i]= bwqty;
    					arrItemStatus[i]=ItemStatusinfo;
    					nlapiLogExecution('ERROR', 'Value of i and Qty if s is cnt', arrQty[i] + 'vale of i ' + i + 'Item Id' + arrId[i]+'Batch with Quantity'+bwqty);
    					break;
    				}
    				Lineno = request.getLineItemValue('custpage_items', 'custpage_line', s);
    				ItemId = request.getLineItemValue('custpage_items', 'custpage_iteminternalid', s);
    				ItemStatusinfo =  request.getLineItemValue('custpage_items','custpage_itemstatus',s); //Added by ramana
    				nlapiLogExecution('ERROR', 'ItemId', ItemId);
    				if (s == 1) {
    					TempLineno = Lineno;
    					TempItemId = ItemId;
    				}
    				if (TempLineno == Lineno) {
    					if (boolFlag == false) {
    						boolFlag = true;
    						Qty = parseFloat(request.getLineItemValue('custpage_items', 'custpage_quantity', s));
    						nlapiLogExecution('ERROR', 'ConsQty1', Qty);
    						bwqty = request.getLineItemValue('custpage_items', 'custpage_batchwithqty', s);
    						nlapiLogExecution('ERROR', 'batch with qty', bwqty);

    					}
    					else {
    						Qty += parseFloat(request.getLineItemValue('custpage_items', 'custpage_quantity', s));
    						nlapiLogExecution('ERROR', 'ConsQty2', Qty);
    						bwqty = bwqty +" "+request.getLineItemValue('custpage_items', 'custpage_batchwithqty', s); 
    						nlapiLogExecution('ERROR', 'second bwqty', bwqty);

    					}

						//i = i + 1;
    					//nlapiLogExecution('ERROR', 'Value of i and Qty in else', Qty + 'vale of i ' + i);
    					//arrQty[i] = Qty;
    					//arrLine[i] = TempLineno;
    					//arrId[i] = TempItemId;
    					//arrBatchwithQty[i]= bwqty;
    					//arrItemStatus[i]=ItemStatusinfo;

    				}
    				else {
    					i = i + 1;
    					nlapiLogExecution('ERROR', 'Value of i and Qty in else', Qty + 'vale of i ' + i);
    					arrQty[i] = Qty;
    					arrLine[i] = TempLineno;
    					arrId[i] = TempItemId;
    					arrBatchwithQty[i]= bwqty;
    					arrItemStatus[i]=ItemStatusinfo;
    					Qty = 0; //make it null for further loop.
    					bwqty="";
    					boolFlag = true;
    					Qty = parseFloat(request.getLineItemValue('custpage_items', 'custpage_quantity', s));
    					Lineno = request.getLineItemValue('custpage_items', 'custpage_line', s);
    					ItemId = request.getLineItemValue('custpage_items', 'custpage_iteminternalid', s);
    					bwqty= request.getLineItemValue('custpage_items', 'custpage_batchwithqty', s); 
    					TempLineno = Lineno;
    					TempItemId = ItemId;
    				}
            //}
            }
        } 
        catch (exp) {
            nlapiLogExecution('ERROR', 'Err in summing Qty', exp);
        }
		 */


		var serialline = new Array();
		var serialnum = new Array();
		var serialId = new Array();
		var tempserialId = "";
		var Serialno="";

		//For log purpose:
		try {

			var t = 0;
			for (var k = 0; k < arrId.length; k++) {
				var tempSerial = "";
				nlapiLogExecution('ERROR', 'Item Ids individual', arrId[k]);
				// var ItemType = nlapiLookupField('item', arrId[k], 'recordType');
				var fields = ['recordType', 'custitem_ebizserialin'];
				var columns = nlapiLookupField('item', arrId[k], fields);
				var ItemType = columns.recordType;
				var serialInflg = columns.custitem_ebizserialin;
				if (ItemType == "serializedinventoryitem" || serialInflg == "T") {
					nlapiLogExecution('ERROR', 'Item Ids ItemType', 'serializedinventoryitem');
					var filters = new Array();
					
					//filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
 filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
					nlapiLogExecution('ERROR', 'beforeval', arrLine[k]);
					var val = arrLine[k];
					nlapiLogExecution('ERROR', 'afterval', val);
					//filters[1] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'equalto', val);
					filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_serialnumber');

					var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);

					if (serchRec) {
						//case# 20124067 start
						var SerialnoQty = arrQty[k];
						nlapiLogExecution('ERROR', 'SerialnoQty', SerialnoQty);
						if(serchRec.length>=SerialnoQty)
						{nlapiLogExecution('ERROR', 'serchRec', serchRec.length);
						//case # 20124067 end
						for (var n = 0; n < SerialnoQty; n++) {
							//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
							if (tempserialId == "") {
								tempserialId = serchRec[n].getId();
							}
							else {
								tempserialId = tempserialId + "," + serchRec[n].getId();
							}
							//This is for Serial num loopin with Space separated.
							if (tempSerial == "") {
								tempSerial = serchRec[n].getValue('custrecord_serialnumber');
							}
							else {
								tempSerial = tempSerial + "," + serchRec[n].getValue('custrecord_serialnumber');

							}
						}

						serialnum[t] = tempSerial;
						Serialno=tempSerial;
						tempSerial = "";
						serialline[t] = val;


						nlapiLogExecution('ERROR', 'serialnum[t]', serialnum[t]);
						nlapiLogExecution('ERROR', 'serialline[t]', serialline[t]);
						t++;

					}
				}
			}
			//Serial nums.
			serialId = tempserialId.split(',');
			//Makung it null
			tempserialId = "";
			nlapiLogExecution('ERROR', 'serialId ::', serialId[0] );
		} 
		}
		catch (exp) {
			nlapiLogExecution('ERROR', 'Exception in', exp);
		}



		/*
         // Start for consolidating quantities and  line array.
         var linearray = new Array();
         var qtyarray = new Array();
         var batarray = new Array();

         for (var i = 0; i < request.getLineItemCount('custpage_items'); i++) {
         if (request.getLineItemValue('custpage_items', 'custpage_polocgen', i + 1) == 'T') {
         linearray[i] = request.getLineItemValue('custpage_items', 'custpage_line', i + 1);
         qtyarray[i] = request.getLineItemValue('custpage_items', 'custpage_quantity', i + 1);
         batarray[i] = request.getLineItemValue('custpage_items', 'custpage_lotbatch', i + 1);
         }
         }

         var linetemp = 0, qtytemp = 0, battemp;
         for (var x = 0; x < linearray.length; x++) {
         for (var y = 0; y < (linearray.length - 1); y++) {
         if (linearray[y] > linearray[y + 1]) {
         linetemp = linearray[y + 1];
         linearray[y + 1] = linearray[y];
         linearray[y] = linetemp;

         qtytemp = qtyarray[y + 1];
         qtyarray[y + 1] = qtyarray[y];
         qtyarray[y] = qtytemp;

         battemp = batarray[y + 1];
         batarray[y + 1] = batarray[y];
         batarray[y] = battemp;
         }
         }
         }
         for (var j = 0; j < linearray.length; j++) {
         nlapiLogExecution('ERROR', 'Line Array||Qty Array||Batch Array after sorting', linearray[j] + '||' + qtyarray[j] + '||' + batarray[j])
         }
		 */
		/*
         //Checking for matched rows.
         var MatchedRows = new Array();
         var Result = new Array();
         var consolidatedqty = new Array();
         var Count = 0;
         var exists = false;

         for (i = 0; i < linearray.length; i++) {
         Count = 0;
         exists = false;

         for (j = i + 1; j < linearray.length; j++) {
         if (linearray[i] == linearray[j]) {

         Count = parseFloat(qtyarray[i]) + parseFloat(qtyarray[j]);
         MatchedRows[i] = linearray[i];
         }
         }

         for (k = 0; k < MatchedRows.length; k++) {
         if (linearray[i] == MatchedRows[k]) {
         exists = true;

         }
         }
         //alert(exists);
         if (exists) {
         Result[i] = Count;
         }
         else {
         Result[i] = qtyarray[i];
         }

         }
         var concount = 0;
         var consolidetedline = new Array();
         var consolidatedbat = new Array();
         for (var i = 0; i < Result.length; i++) {
         if (Result[i] != 0) {
         consolidatedqty[concount] = Result[i];
         consolidetedline[concount] = linearray[i];
         consolidatedbat[concount] = batarray[i];
         concount++;
         }
         }

         for (var i = 0; i < consolidatedqty.length; i++) {
         nlapiLogExecution('ERROR', 'CONSline||CONSqty ~ ', consolidetedline[i] + '||' + consolidatedqty[i]);
         }
		 */
var distinctitemarray = removeDuplicateElement(itemarray);
		var itemdimarray = getAlleBizItemDimensions(distinctitemarray);
		try {
			//Creating Item Receipt.
			
			//case # 20127765�added systemrule.
			
			var systemRule= GetSystemRuleForPostItemReceiptby(poloc);
			nlapiLogExecution('ERROR', 'systemRule', systemRule);
		var idl ='';
				if(systemRule=='PO')
				{
					idl='-1';
				}
			
			
			if(systemRule=='LP'){
			
			var fromrecord;
			var fromid;
			var torecord;
			var qty=0;

			// fromrecord = 'purchaseorder';
			fromrecord = tranType; //'purchaseorder';
			fromid = pointid; // Transform PO with ID = 26 ;
			torecord = 'itemreceipt'; // Transaform a record with given id to a different record type.
			// Get the object of the transformed record.  
			try {
				nlapiLogExecution('ERROR', 'fromid', fromid);
				nlapiLogExecution('ERROR', 'fromrecord', fromrecord);
				// var trecord = nlapiTransformRecord('purchaseorder', fromid, 'itemreceipt');
				//var qty=0;
				
				//case 20125924� start 
				var vAdvBinManagement=false;
				var ctx = nlapiGetContext();
				if(ctx != null && ctx != '')
				{
					if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
						vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
				}  
				nlapiLogExecution('ERROR', 'vAdvBinManagement', vAdvBinManagement);
				//case 20125924� end


				//Array(arrItemStat) containes distinct item status like(Good,Damage,Repair)
				//for each item status based on custom record(customrecord_ebiznet_sku_status) fetchs item_status_location_map
				//fetched item_status_location_map details will be passed to NetSuite as location field.

				for (var k=0; k<arrItemStat.length;k++) {

					nlapiLogExecution('ERROR', 'arrItemStat.length::', arrItemStat.length);
					nlapiLogExecution('ERROR', 'K value', k);
					nlapiLogExecution('ERROR', 'for SKU Status', arrItemStat[k]);

					var trecord = nlapiTransformRecord(fromrecord, fromid, 'itemreceipt');
					nlapiLogExecution('ERROR', 'Test', 'Test');
					var polinelength = trecord.getLineItemCount('item');

					var itemstatus = arrItemStat[k];																
					var varItemlocation;
					var filtersItemStatus = new Array(); 
					if(itemstatus!=null && itemstatus!='')
						filtersItemStatus[0] = new nlobjSearchFilter('internalid', null, 'is', itemstatus);
					var colsItemStatus = new Array();
					colsItemStatus[0]=new nlobjSearchColumn('custrecord_item_status_location_map');  
					var ItemLocStatusSrearchResults = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, filtersItemStatus, colsItemStatus);        						        						        						        						
					if(ItemLocStatusSrearchResults!=null)
						varItemlocation = ItemLocStatusSrearchResults[0].getValue('custrecord_item_status_location_map');

					nlapiLogExecution('ERROR', 'Item Status Location Map', varItemlocation);

					//polinelength contains the number of lines in PO. 
					//example if the po contains 2 lines then polinelength gives 2		

					for (var j = 1; j <= polinelength; j++) {
						var item_id = trecord.getLineItemValue('item', 'item', j);
						var item_name = trecord.getLineItemText('item', 'item', j);
						var itemrec = trecord.getLineItemValue('item', 'itemreceive', j);
						var itemLineNo = trecord.getLineItemValue('item', 'line', j);
						/* The below line is merged from Lexjet production account on 04-03-2013 by Radhika as part of Standard bundle*/		
						//var poItemUOM = trecord.getLineItemValue('item', 'unitsdisplay', 1);
						var poItemUOM = trecord.getLineItemValue('item', 'units', j);
						var commitflag = 'N';
						qty=0;

						//Array(arrLine) have the details in poline level parital qty. 
						//example polines no can appear multiple lines with partial qty information.

						for (var i = 0; i < arrLine.length; i++) {	

							var str = 'arrItemStat[k]. = ' + arrItemStat[k] + '<br>';
							str = str + 'arrItemStatus[i]. = ' + arrItemStatus[i] + '<br>';	
							str = str + 'itemLineNo. = ' + itemLineNo + '<br>';	
							str = str + 'arrLine[i]. = ' + arrLine[i] + '<br>';	
							//str = str + 'POInternalIds[count]. = ' + POInternalIds[count] + '<br>';	
							//str = str + 'POInternalIds[i]. = ' + POInternalIds[i] + '<br>';	

							nlapiLogExecution('ERROR', 'Item Receipt Details', str);

							if ((arrItemStat[k]== arrItemStatus[i]) && (itemLineNo ==arrLine[i])) 	
							{	
								var vrcvqty=arrQty[i];

								if(poItemUOM!=null && poItemUOM!='')
								{
									var vbaseuomqty=0;
									var vuomqty=0;

									if(itemdimarray!=null&&itemdimarray.length>0)
									{
										for(z=0; z < itemdimarray.length; z++)
										{
											var vdimsitemid=itemdimarray[z].getValue('custrecord_ebizitemdims');

											if(vdimsitemid == item_id)
											{
												if(itemdimarray[z].getValue('custrecord_ebizbaseuom') == 'T')
												{
													vbaseuomqty = itemdimarray[z].getValue('custrecord_ebizqty');					
												}
												if(poItemUOM.trim() == itemdimarray[z].getValue('custrecord_ebiznsuom').trim())
												{
													vuomqty = itemdimarray[z].getValue('custrecord_ebizqty');
												}
											}
										}


										if(vrcvqty==null || vrcvqty=='' || isNaN(vrcvqty))
											vrcvqty=0;
										else
											vrcvqty = (parseFloat(vrcvqty)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);


									}
								}

								commitflag = 'Y';
								var Itype = nlapiLookupField('item', arrId[i], 'recordType');

								trecord.selectLineItem('item', j);
								trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');

								trecord.setCurrentLineItemValue('item', 'quantity', parseFloat(vrcvqty));

								if (Itype == "serializedinventoryitem") {
									for (var x = 0; x < serialline.length; x++) {
										if (serialline[x] == arrLine[i]) {

											var stemp = serialnum[x];
											nlapiLogExecution('ERROR', 'Serial nums assigned', stemp);
											
											if(vAdvBinManagement==true)
											{ 	 
												nlapiLogExecution('ERROR', 'Using vAdvBinManagement2',vAdvBinManagement);
												
												//case 20125924� start : posting serial numbers
												var SerialArray = serialnum[x].split(',');
												if(SerialArray !=null && SerialArray !='' && SerialArray.length>0)
												{
													nlapiLogExecution('ERROR', 'SerialArray',SerialArray.length);
													var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
													for (var t = 0; t < SerialArray.length; t++)
													{
														compSubRecord.selectNewLineItem('inventoryassignment');
														compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
														compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', SerialArray[t]);

														compSubRecord.commitLineItem('inventoryassignment');
														//compSubRecord.commit();	
													}
													compSubRecord.commit();
													//case 20125924�end
												}
											}
											else
											{	
												trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnum[x]);//stemp);//consolidatedbat[i]);
											}
																						
											stemp = "";
										}
									}
								}
								else if (Itype == "lotnumberedinventoryitem" || Itype == "lotnumberedassemblyitem")
								{	
									item_name=item_name.replace(/ /g,"-");
									var newLotwithQty = item_name + "(" + parseFloat(vrcvqty) + ")";
									var confirmLotToNS='Y';
									confirmLotToNS=GetConfirmLotToNS(poloc);
									//case 20126074 start
									if(vAdvBinManagement==true)
									{ 	 
										nlapiLogExecution('ERROR', 'Using vAdvBinManagement2',vAdvBinManagement);
										if(LotArrForAdvBin!= null && LotArrForAdvBin!= '' && LotArrForAdvBin[i] != null && LotArrForAdvBin[i] != '' && LotArrForAdvBin[i].length>0)
										{
											nlapiLogExecution('ERROR', 'LotArrForAdvBin', LotArrForAdvBin);
											nlapiLogExecution('ERROR', 'LotQtyArrForAdvBin', LotQtyArrForAdvBin);
											nlapiLogExecution('ERROR', 'LotArrForAdvBin[i]', LotArrForAdvBin[i].length);
											nlapiLogExecution('ERROR', 'LotArrForAdvBin[i]', LotArrForAdvBin[i]);
											nlapiLogExecution('ERROR', 'LotQtyArrForAdvBin[i]', LotQtyArrForAdvBin[i]);
											var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
											for(var l=0;l<LotArrForAdvBin[i].length>0;l++)
											{	
												if(l!=0)													 
													var compSubRecord = trecord.editCurrentLineItemSubrecord('item','inventorydetail');

												compSubRecord.selectNewLineItem('inventoryassignment');
												nlapiLogExecution('ERROR', 'vrcvqty advanced', vrcvqty);
												nlapiLogExecution('ERROR', 'arrBatchNew advanced', arrBatchNew[i]);
												nlapiLogExecution('ERROR', 'confirmLotToNS', confirmLotToNS);


												//compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', LotQtyArrForAdvBin[i][l]);
												if(poItemUOM!=null && poItemUOM!='')
												{
													nlapiLogExecution('ERROR', 'vrcvqty advanced', vrcvqty);
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity',vrcvqty);
												}
												else
												{
													nlapiLogExecution('ERROR', 'intoelse', 'done');
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', LotQtyArrForAdvBin[i][l]);
												}
												if(confirmLotToNS=='N')
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', item_name);
												else
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', LotArrForAdvBin[i][l]);
												compSubRecord.commitLineItem('inventoryassignment');
												
											}
											compSubRecord.commit();
										}
									} //case 20126074 end
									else
									{								


										if(confirmLotToNS=='N')
											trecord.setCurrentLineItemValue('item', 'serialnumbers', newLotwithQty);
										/*else
									{
										trecord.setCurrentLineItemValue('item', 'serialnumbers', arrBatch[i]);
									}*/
										else
										{
											var newBatcharray = new Array();
											var finalarrBatch = new Array();
											var finalvBatch = '';
											if(arrBatch[i] != null  && arrBatch[i] != '' && arrBatch[i].length >0)
											{
												newBatcharray=arrBatch[i].split(' ');
												if(newBatcharray != null)
													for(var z=0;z<newBatcharray.length;z++)
													{ 
														var vNewLot=newBatcharray[z].substring(0,newBatcharray[z].indexOf('('));
														var vNewQty=newBatcharray[z].substring(newBatcharray[z].indexOf('(') + 1,newBatcharray[z].indexOf(')'));

														if(vNewQty==null || vNewQty=='' || isNaN(vNewQty))
															vNewQty=0;
														else
															vNewQty = (parseFloat(vNewQty)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);


														if(finalvBatch != null && finalvBatch != "")
															finalvBatch= finalvBatch + " " + vNewLot + "(" + vNewQty + ")";
														else
															finalvBatch=vNewLot + "(" + vNewQty + ")";
														//Case# 20123368 start
														nlapiLogExecution('ERROR', 'vNewLot', vNewLot);
														var expdate1=getLotExpdate(vNewLot,arrId);
														if(expdate1 !=null && expdate1 !='')
														{
															trecord.setCurrentLineItemValue('item', 'expirationdate', expdate1);
															trecord.setCurrentLineItemValue('item', 'custcol_exp_date', expdate1);
														}
														//Case# 20123368 end
													}
											}
											nlapiLogExecution('ERROR', 'finalvBatch value last', finalvBatch);
											nlapiLogExecution('ERROR', 'arrBatch[i] value last', arrBatch[i]);
											//trecord.setCurrentLineItemValue('item', 'serialnumbers', arrBatchwithQty[i]);
											trecord.setCurrentLineItemValue('item', 'serialnumbers', arrBatch[i]);

										}
									}

								}
								trecord.setCurrentLineItemValue('item', 'custcol_ebiznet_item_status', itemstatus);
								nlapiLogExecution('ERROR', 'arrLpno[i] value last', arrLpno[i]);
								nlapiLogExecution('ERROR', 'arrLpno[i] value last', arrLpno);
								trecord.setCurrentLineItemValue('item', 'custcol_lp', arrLpno[i]);
								trecord.commitLineItem('item');

								qty += parseFloat(vrcvqty);								
								//nlapiLogExecution('ERROR', 'qty value', qty);								
							}
						} //end of user entered arrLine

						trecord.setLineItemValue('item', 'location', j, varItemlocation); 
						trecord.setLineItemValue('item', 'quantity', j, parseFloat(qty));															
						if (commitflag == 'N') {
							trecord.selectLineItem('item', j);
							trecord.setCurrentLineItemValue('item', 'itemreceive', 'F');
							trecord.commitLineItem('item');
						}
					}
					 idl = nlapiSubmitRecord(trecord, true);

					nlapiLogExecution('ERROR', 'After Submitted Item Receipts:;', idl);
			} 
			
			}
			catch (e) {
				if (e instanceof nlobjError) 
					nlapiLogExecution('ERROR', 'system error', e.getCode() + '\n' + e.getDetails());
				else 
					nlapiLogExecution('ERROR', 'Unexpected err', e);
			}

		}
			if (idl != null) {

				///Getting Records to insert in to invenotory table for Confirm putaway list..
				nlapiLogExecution('ERROR', 'Cust INVT Rec Creation ', 'Record Creation');
				var putRecCnt = request.getLineItemCount('custpage_items');
				nlapiLogExecution('ERROR', 'Confirm Putaway REc count ', putRecCnt);
				for (var k = 1; k <= putRecCnt; k++) 
				{
					var confirmFlag= request.getLineItemValue('custpage_items', 'custpage_polocgen', k);
					//case# 20148770 starts
					var voldqty = request.getLineItemValue('custpage_items', 'custpage_oldqty', k);
					//case# 20148770 ends
					nlapiLogExecution('ERROR', 'Confirm Flag', confirmFlag);
					var InvtRefNo="";
					if(confirmFlag == 'T')
					{
						nlapiLogExecution('ERROR', 'Confirm InsideLine ', putRecCnt);
						var putLine = request.getLineItemValue('custpage_items', 'custpage_line', k);
						var putLP = request.getLineItemValue('custpage_items', 'custpage_chkn_lp', k);
						var putItemName = request.getLineItemValue('custpage_items', 'custpage_iteminternalid', k);
						var putItemId = request.getLineItemValue('custpage_items', 'custpage_iteminternalid', k);
						var putItemStatus = request.getLineItemValue('custpage_items', 'custpage_itemstatus', k);
						var putLotBatch = request.getLineItemValue('custpage_items', 'custpage_lotbatch', k);
						var putQty = request.getLineItemValue('custpage_items', 'custpage_quantity', k);
						var putBinLoc = request.getLineItemValue('custpage_items', 'custpage_location', k);
						var putPoId = request.getLineItemValue('custpage_items', 'custpage_pointid', k);
						var putItemId = request.getLineItemValue('custpage_items', 'custpage_poskuid', k);
						var putItemDesc = request.getLineItemValue('custpage_items', 'custpage_poskudesc', k);
						nlapiLogExecution('ERROR', 'Confirm InsideLineputItemDesc ', putItemDesc);
						var putItemPC = request.getLineItemValue('custpage_items', 'custpage_item_pc', k);
						//var putItemBatch = request.getLineItemValue('custpage_items', 'custpage_lotbatch', k);
						var putoldBinLoc = request.getLineItemValue('custpage_items', 'custpage_oldlocation', k);
						var opentaskintid = request.getLineItemValue('custpage_items', 'custpage_opentaskid', k);
						//case# 20148770 starts 
						var vname = request.getLineItemValue('custpage_items', 'custpage_name', k);
						//case# 20148770 ends
						nlapiLogExecution('ERROR', 'opentaskintid', opentaskintid);


						//*****************************************************************************************************//    							
						//Code Fetch the Item Map Location from ITEM Status Custom record based on item status
						//and Need to pass item Map Location and Location to inventory account no custom record to fetch Account No

						//Searching in item status     	


						nlapiLogExecution('ERROR', 'ItemStatus :: ', putItemStatus);
						var varItemStatusMapLocation;
						var filtersItemStatus = new Array();  
						if(putItemStatus!=null && putItemStatus!='')
							filtersItemStatus[0] = new nlobjSearchFilter('internalid', null, 'is', putItemStatus);
						var colsItemStatus = new Array();
						colsItemStatus[0]=new nlobjSearchColumn('custrecord_item_status_location_map');  
						var ItemStatusSrearchResults = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, filtersItemStatus, colsItemStatus);        						        						        						        						
						if(ItemStatusSrearchResults!=null)
							varItemStatusMapLocation = ItemStatusSrearchResults[0].getValue('custrecord_item_status_location_map');

						nlapiLogExecution('ERROR', 'varItemStatusMapLocation:: ', varItemStatusMapLocation);        					


						// Need to pass Account No Details into Create Inventory.

						nlapiLogExecution('ERROR', 'Location',poloc );
						var varAccountNo; 
						var filtersAccNo = new Array(); 
						filtersAccNo[0] = new nlobjSearchFilter('custrecord_location', null, 'is', poloc);
						filtersAccNo[1] = new nlobjSearchFilter('custrecord_inventorynslocation', null, 'is', varItemStatusMapLocation);        						
						var colsAcc = new Array();
						colsAcc[0]=new nlobjSearchColumn('custrecord_accountno');                
						var Accsearchresults = nlapiSearchRecord('customrecord_inventoryaccountno', null, filtersAccNo, colsAcc);
						if(Accsearchresults!=null)
							varAccountNo = Accsearchresults[0].getValue('custrecord_accountno');

						nlapiLogExecution('ERROR', 'Account ',varAccountNo);

						//upto to here for retrieving     							
						//*********************************************************************************************************//


						var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group'];
						var columns = nlapiLookupField('item', putItemId, fields);
						var ItemType = columns.recordType;					
						var batchflg="F"; 
						batchflg = columns.custitem_ebizbatchlot;
						var itemfamId= columns.custitem_item_family;
						var itemgrpId= columns.custitem_item_group;
						var getlotnoid="";
						if(ItemType == "lotnumberedinventoryitem" || batchflg == "T")
						{
							nlapiLogExecution('ERROR', 'putLotBatch', putLotBatch);
							if(putLotBatch!=""&&putLotBatch!=null)
							{
								var filterspor = new Array();
								filterspor[0] = new nlobjSearchFilter('name', null, 'is', putLotBatch);
								var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor);
								getlotnoid= receiptsearchresults[0].getId();
								nlapiLogExecution('ERROR', 'getlotnoid', getlotnoid);
							}
						}
						try {
							//Creating Inventory Record.
							nlapiLogExecution('ERROR', 'Site Location', poloc);
							var filtersputmethod = new Array();
							filtersputmethod[0] = new nlobjSearchFilter('custrecord_ebizsiteputaway', null, 'is', poloc);
							var colsputmethod = new Array();
							colsputmethod[0] = new nlobjSearchColumn('custrecord_mergelp');            
							var putwmethodsearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_method', null, filtersputmethod, colsputmethod);
							var varMergeLP = "";

							if(putwmethodsearchresults != null && putwmethodsearchresults.length > 0)
							{
								nlapiLogExecution('ERROR', 'putwmethodsearchresults.length', putwmethodsearchresults.length);
								var varMergeLP = putwmethodsearchresults[0].getValue('custrecord_mergelp');
							}

							nlapiLogExecution('ERROR', 'varMergeLP', varMergeLP);	
							//var priorityflag = priorityPutawayflag(putItemId,poloc);
							var priorityPutawayLP = priorityPutawayfixedLP(putItemId,poloc,putItemStatus,putBinLoc);
							nlapiLogExecution('ERROR', 'priorityPutawayLP', priorityPutawayLP);
							if(varMergeLP == "T" ||(priorityPutawayLP!==null && priorityPutawayLP!==''))
							{
								if(priorityPutawayLP!=null && priorityPutawayLP!='')
									putLP=priorityPutawayLP;
								nlapiLogExecution('ERROR', 'putItemId', putItemId);
								nlapiLogExecution('ERROR', 'putItemStatus', putItemStatus);
								nlapiLogExecution('ERROR', 'putItemPC', putItemPC);
								nlapiLogExecution('ERROR', 'putBinLoc', putBinLoc);
								var filtersinvt = new Array();
								filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', putItemId));
								filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', putItemStatus));
								if(putItemPC!=null && putItemPC!='')
									filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_packcode', null, 'anyof', putItemPC));
								filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', putBinLoc));
								if(getlotnoid!=null && getlotnoid!="")
								filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', getlotnoid));

								//filtersinvt[0] = new nlobjSearchFilter('custrecord_ebizsiteputaway', null, 'is', poloc);
								var columnsinvt = new Array();
								columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');            
								var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);

								if(invtsearchresults != null && invtsearchresults.length > 0)
								{
									nlapiLogExecution('ERROR', 'invtsearchresults.length', invtsearchresults.length);

									var invttransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invtsearchresults[0].getId());
									nlapiLogExecution('ERROR', 'Inventory Record ID', invtsearchresults[0].getId());
									var varExistQOHQty = invttransaction.getFieldValue('custrecord_ebiz_qoh');
									var varExistInvQty = invttransaction.getFieldValue('custrecord_ebiz_inv_qty');
									nlapiLogExecution('ERROR', 'varExistQOHQty', varExistQOHQty);
									nlapiLogExecution('ERROR', 'varExistInvQty', varExistInvQty);
									var varupdatedQOHqty = parseFloat(varExistQOHQty) + parseFloat(putQty);
									var varupdatedInvqty = parseFloat(varExistInvQty) + parseFloat(putQty);

									invttransaction.setFieldValue('custrecord_ebiz_qoh', parseFloat(varupdatedQOHqty).toFixed(5));
									invttransaction.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(varupdatedInvqty).toFixed(5));
									invttransaction.setFieldValue('custrecord_ebiz_callinv', 'N');    

									nlapiSubmitRecord(invttransaction, true);
									InvtRefNo=invtsearchresults[0].getId();
									nlapiLogExecution('ERROR', 'nlapiSubmitRecord', 'Record Submitted');

									//Added for updation of Cube information by ramana on 10th june 2011

									nlapiLogExecution('ERROR', 'OLD BIN LOCATION DETAILS ', putoldBinLoc);
									nlapiLogExecution('ERROR', 'NEW BIN LOCATION DETAILS ', putBinLoc);
									nlapiLogExecution('ERROR', 'ITEMID', putItemId);
									nlapiLogExecution('ERROR', 'LP', putLP);

									if(putBinLoc!=putoldBinLoc)
									{   
										/*
    											//For the OLD Location Updation Purpose.    											 											
										 */

										var arrDims = getSKUCubeAndWeight(putItemId, 1);
										var itemCube = 0;
										if (arrDims[0] != "" && (!isNaN(arrDims[0]))) 
										{
											var uomqty = ((parseFloat(putQty))/(parseFloat(arrDims[1])));			
											itemCube = (parseFloat(uomqty) * parseFloat(arrDims[0]));
											nlapiLogExecution('ERROR', 'checkInPOSTRequest:itemCube', itemCube);
										} 

										var vOldRemainingCube = GeteLocCube(putoldBinLoc);
										nlapiLogExecution('ERROR', 'vOldRemainingCube', vOldRemainingCube);

										var vTotalCubeValue = parseFloat(itemCube)+ parseFloat(vOldRemainingCube);
										nlapiLogExecution('ERROR', 'vOldLocationRemainingCube', vTotalCubeValue);

										var retValue =  UpdateLocCube(putoldBinLoc,vTotalCubeValue);
										//upto to here old Location Updation purpose

										//For the New Location updation

										var vNewLocationRemainingCube = GeteLocCube(putBinLoc);   

										nlapiLogExecution('ERROR', 'vNewLocationRemainingCube', vNewLocationRemainingCube);

										//var vTotalNewLocation =  parseFloat(vartotalCube)- parseFloat(vNewLocationRemainingCube);
										var vTotalNewLocationCube =  parseFloat(vNewLocationRemainingCube)- parseFloat(itemCube);

										nlapiLogExecution('ERROR', 'vTotalNewLocation', vTotalNewLocationCube);

										if(vTotalNewLocationCube<0)
										{
											nlapiLogExecution('ERROR', 'inside PO Overage Validation', 'inside loop');					  
											var form = nlapiCreateForm('Confirm Putaway');
											nlapiLogExecution('ERROR', 'Form Called', 'form');					  
											var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);		  
											msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'Qty exceeds location capacity', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");		    		   
											nlapiLogExecution('ERROR', 'message', msg);					  
											response.writePage(form);                  						
											return;			
										}
										else
										{
											var retValue1 = UpdateLocCube(putBinLoc,vTotalNewLocationCube);
										}
										//upto to here new location

									}						 

									var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', opentaskintid);

									//nlapiLogExecution('DEBUG', 'location', request.getLineItemValue('custpage_items', 'custpage_location', i + 1));

									//   nlapiLogExecution('ERROR', 'STATUS before', searchresult.getValue('custrecord_wms_status_flag'));

									if (transaction.getFieldValue('custrecord_wms_status_flag') == 6) 
									{
										nlapiLogExecution('ERROR', 'inside if condition', transaction.getFieldValue('custrecord_wms_status_flag'));
										transaction.setFieldValue('custrecord_actbeginloc', request.getLineItemValue('custpage_items', 'custpage_location', k));
										transaction.setFieldValue('custrecordact_begin_date', DateStamp());
										transaction.setFieldValue('custrecord_actualbegintime', TimeStamp());
									}
									//case 20125353 start Updation of Quantity through Qty exception
									transaction.setFieldValue('custrecord_act_qty', parseFloat(putQty).toFixed(5));
									//case end

									transaction.setFieldValue('custrecord_actualendtime', TimeStamp());
									//transaction.setFieldValue('custrecord_recordtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
									transaction.setFieldValue('custrecord_recordupdatetime', TimeStamp());//((curr_hour) + ":" + (curr_min) + " " + a_p));
									transaction.setFieldValue('custrecord_upd_date', DateStamp());//(parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
									//transaction.setFieldValue('custrecord_current_date', (parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());

									transaction.setFieldValue('custrecord_actendloc', request.getLineItemValue('custpage_items', 'custpage_location', k));
									transaction.setFieldValue('custrecord_act_end_date', DateStamp()); //(parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
									transaction.setFieldValue('custrecord_batch_no', request.getLineItemValue('custpage_items', 'custpage_lotbatch', k));
									// Case # 20127918/20127913 starts
									transaction.setFieldValue('custrecord_sku_status', request.getLineItemValue('custpage_items', 'custpage_itemstatus', k));
									transaction.setFieldValue('custrecord_serial_no', request.getLineItemValue('custpage_items', 'custpage_serialnos', k));
									// Case # 20127918/20127913 end
									transaction.setFieldValue('custrecord_wms_status_flag', 3);//Inbound S
									transaction.setFieldValue('custrecord_wms_location', poloc);
									var currentContext = nlapiGetContext();  
									var currentUserID = currentContext.getUser();
									transaction.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
									//   nlapiLogExecution('ERROR', 'STATUS after', searchresult.getValue('custrecord_wms_status_flag'));
									nlapiLogExecution("ERROR","InvtRefNo",InvtRefNo);
									transaction.setFieldValue('custrecord_invref_no', InvtRefNo);
									//case # 20127824  updating open task with nsconfno.
									nlapiLogExecution("ERROR","nsconfno",idl);
									//if(idl !=null && idl !='')
									if(idl !=null && idl !='' && idl!='-1')// Case # 20148038 
									{
										transaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', idl);
									}
									if (empId != null && empId != "") {
										transaction.setFieldValue('custrecord_taskassignedto', empId);
									}
									else
									{
										transaction.setFieldValue('custrecord_taskassignedto', currentUserID);
									}
									nlapiSubmitRecord(transaction, true);
									//Case# 20148038 starts
									//MoveTaskRecord(opentaskintid);
									if(systemRule=='LP')
									{
										try
										{
									MoveTaskRecord(opentaskintid);
										}
										catch(exp)
										{
											nlapiLogExecution('ERROR', 'Exception in  MoveTaskRecord');
										}
									}
									//Case# 20148038 ends
								}
								else
								{
									var fifovalue = '';
									var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');
									nlapiLogExecution('ERROR', 'CreATIN INVT  REc ', 'INVT');
									invtRec.setFieldValue('name', idl);
									invtRec.setFieldValue('custrecord_ebiz_inv_binloc', putBinLoc);
									invtRec.setFieldValue('custrecord_ebiz_inv_lp', putLP);
									invtRec.setFieldValue('custrecord_ebiz_inv_sku', putItemId);
									invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', putItemStatus);
									invtRec.setFieldValue('custrecord_ebiz_inv_packcode', putItemPC);
									invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(putQty).toFixed(5));
									//invtRec.setFieldValue('custrecord_ebiz_inv_recorddate',(parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
									//invtRec.setFieldValue('custrecord_ebiz_inv_rectime',((curr_hour) + ":" + (curr_min) + " " + a_p));
									invtRec.setFieldValue('custrecord_inv_ebizsku_no', putItemId);
									//invtRec.setFieldValue('custrecord_invt_ebizlp',putLP);
									//invtRec.setFieldValue('custrecord_ebiz_uomlvl','1');
									//invtRec.setFieldValue('custrecord_ebiz_tranid',idl);
									invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(putQty).toFixed(5));
									invtRec.setFieldValue('custrecord_ebiz_itemdesc', putItemDesc);
									//invtRec.setFieldValue('custrecord_ebiz_inv_loc', poLocation);

									invtRec.setFieldValue('custrecord_invttasktype', 2);
									invtRec.setFieldValue('custrecord_wms_inv_status_flag', 19); //Inventory S
									invtRec.setFieldValue('custrecord_ebiz_displayfield','N');

									// invtRec.setFieldValue('custrecord_ebiz_inv_account_no', 1);
									// invtRec.setFieldValue('custrecord_ebiz_inv_account_no', 1110);  //
									if(varAccountNo!= null && varAccountNo != "")
										invtRec.setFieldValue('custrecord_ebiz_inv_account_no', varAccountNo);
									//giving LOT NO id
									if (ItemType == "lotnumberedinventoryitem" || batchflg == "T")
									{ // invtRec.setFieldValue('custrecord_ebiz_inv_lot', putItemBatch);
										//Checking FIFO Policy.
										/*var fifovalue = FifovalueCheck(ItemType,putItemId,itemfamId,itemgrpId,putLine,putPoId,getlotnoid);
										if (getlotnoid != "") {
											nlapiLogExecution('ERROR','FIFO Value Check.',fifovalue);
											invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
											invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
										}*/
										
										if (getlotnoid != "") {
											//Checking FIFO Policy.
											var fifodate = nlapiLookupField('customrecord_ebiznet_batch_entry', getlotnoid, 'custrecord_ebizfifodate');
											if(fifodate == null ||fifodate=='' ){
												fifovalue = FifovalueCheck(ItemType,putItemId,itemfamId,itemgrpId,putLine,putPoId,getlotnoid);
												nlapiLogExecution('ERROR','FIFO DATE from FIFOvalue check fn',fifovalue);
											}
											else
												fifovalue=fifodate;
											expiryDateInLot = nlapiLookupField('customrecord_ebiznet_batch_entry', getlotnoid, 'custrecord_ebizexpirydate');
										}
										invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
										//invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
										if(expiryDateInLot!=null && expiryDateInLot!='')
										invtRec.setFieldValue('custrecord_ebiz_expdate', expiryDateInLot);
									}
									// case# 201413375

									if(fifovalue == null || fifovalue == '')
									{
										fifovalue = FifovalueCheck(ItemType,putItemId,itemfamId,itemgrpId,putLine,putPoId,getlotnoid);
									}

									if(fifovalue!=null && fifovalue!="" && (fifovalue != 'NaN/NaN/NaN'))
										invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
									else
										invtRec.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());
									//varItemStatusMapLocation:: This Location is passing inventory information when item status is damage or good     										
									invtRec.setFieldValue('custrecord_ebiz_inv_loc', poloc);    										
									//invtRec.setFieldValue('custrecord_ebiz_inv_loc', varItemStatusMapLocation);

									invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');                        
									invtRec.setFieldValue('custrecord_ebiz_inv_company', PutwCompany);

									nlapiLogExecution('ERROR', 'Before Submitting invtrecid', 'INVTRECORDS');
									var invtrecid = nlapiSubmitRecord(invtRec, false, true);
									InvtRefNo=invtrecid;
									nlapiLogExecution('ERROR', 'After Submitting invtrecid', invtrecid);
									if (invtrecid != null) {
										nlapiLogExecution('ERROR', 'Cust INVT Rec Creation Succes with ID ', invtrecid);
									}
									else {
										nlapiLogExecution('ERROR', 'Cust INVT Rec Creation Fail ', 'Fail');
									}

									//Added for updation of Cube information by ramana on 10th june 2011

									nlapiLogExecution('ERROR', 'OLD BIN LOCATION DETAILS ', putoldBinLoc);
									nlapiLogExecution('ERROR', 'NEW BIN LOCATION DETAILS ', putBinLoc);
									nlapiLogExecution('ERROR', 'ITEMID', putItemId);
									nlapiLogExecution('ERROR', 'LP', putLP);

									if(putBinLoc!=putoldBinLoc)
									{   
										/*
    											//For the OLD Location Updation Purpose.    											 											
										 */

										var arrDims = getSKUCubeAndWeight(putItemId, 1);
										var itemCube = 0;
										if (arrDims[0] != "" && (!isNaN(arrDims[0]))) 
										{
											var uomqty = ((parseFloat(putQty))/(parseFloat(arrDims[1])));			
											itemCube = (parseFloat(uomqty) * parseFloat(arrDims[0]));
											nlapiLogExecution('ERROR', 'checkInPOSTRequest:itemCube', itemCube);
										} 

										var vOldRemainingCube = GeteLocCube(putoldBinLoc);
										nlapiLogExecution('ERROR', 'vOldRemainingCube', vOldRemainingCube);

										var vTotalCubeValue = parseFloat(itemCube)+ parseFloat(vOldRemainingCube);
										nlapiLogExecution('ERROR', 'vOldLocationRemainingCube', vTotalCubeValue);

										var retValue =  UpdateLocCube(putoldBinLoc,vTotalCubeValue);
										//upto to here old Location Updation purpose

										//For the New Location updation

										var vNewLocationRemainingCube = GeteLocCube(putBinLoc);   

										nlapiLogExecution('ERROR', 'vNewLocationRemainingCube', vNewLocationRemainingCube);

										//var vTotalNewLocation =  parseFloat(vartotalCube)- parseFloat(vNewLocationRemainingCube);
										var vTotalNewLocationCube =  parseFloat(vNewLocationRemainingCube)- parseFloat(itemCube);

										nlapiLogExecution('ERROR', 'vTotalNewLocation', vTotalNewLocationCube);

										if(vTotalNewLocationCube<0)
										{
											nlapiLogExecution('ERROR', 'inside PO Overage Validation', 'inside loop');					  
											var form = nlapiCreateForm('Confirm Putaway');
											nlapiLogExecution('ERROR', 'Form Called', 'form');					  
											var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);		  
											msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'Qty exceeds location capacity', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");		    		   
											nlapiLogExecution('ERROR', 'message', msg);					  
											response.writePage(form);                  						
											return;			
										}
										else
										{
											var retValue1 = UpdateLocCube(putBinLoc,vTotalNewLocationCube);
										}
										//upto to here new location

									}                        
									//upto to here on 10th june 2011

									var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', opentaskintid);

									//nlapiLogExecution('DEBUG', 'location', request.getLineItemValue('custpage_items', 'custpage_location', i + 1));

									//   nlapiLogExecution('ERROR', 'STATUS before', searchresult.getValue('custrecord_wms_status_flag'));

									if (transaction.getFieldValue('custrecord_wms_status_flag') == 6) 
									{
										nlapiLogExecution('ERROR', 'inside if condition', transaction.getFieldValue('custrecord_wms_status_flag'));
										transaction.setFieldValue('custrecord_actbeginloc', request.getLineItemValue('custpage_items', 'custpage_location', k));
										transaction.setFieldValue('custrecordact_begin_date', DateStamp());
										transaction.setFieldValue('custrecord_actualbegintime', TimeStamp());
									}
     //case 20125353 start Updation of Quantity through Qty exception
									transaction.setFieldValue('custrecord_act_qty', parseFloat(putQty).toFixed(5));
									//case end
									transaction.setFieldValue('custrecord_actualendtime', TimeStamp());
									//transaction.setFieldValue('custrecord_recordtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
									transaction.setFieldValue('custrecord_recordupdatetime', TimeStamp());//((curr_hour) + ":" + (curr_min) + " " + a_p));
									transaction.setFieldValue('custrecord_upd_date', DateStamp());//(parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
									//transaction.setFieldValue('custrecord_current_date', (parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());

									transaction.setFieldValue('custrecord_actendloc', request.getLineItemValue('custpage_items', 'custpage_location', k));
									transaction.setFieldValue('custrecord_act_end_date', DateStamp()); //(parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
									transaction.setFieldValue('custrecord_batch_no', request.getLineItemValue('custpage_items', 'custpage_lotbatch', k));
									// Case # 20127918/20127913 starts
									transaction.setFieldValue('custrecord_sku_status', request.getLineItemValue('custpage_items', 'custpage_itemstatus', k));
									transaction.setFieldValue('custrecord_serial_no', request.getLineItemValue('custpage_items', 'custpage_serialnos', k));
									// Case # 20127918/20127913 end
									transaction.setFieldValue('custrecord_wms_status_flag', 3);//Inbound S
									transaction.setFieldValue('custrecord_wms_location', poloc);
									var currentContext = nlapiGetContext();  
									var currentUserID = currentContext.getUser();
									transaction.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
									//   nlapiLogExecution('ERROR', 'STATUS after', searchresult.getValue('custrecord_wms_status_flag'));
									nlapiLogExecution("ERROR","InvtRefNo",InvtRefNo);
									transaction.setFieldValue('custrecord_invref_no', InvtRefNo);
									//case 20126191,20126073 start
									//if(idl !=null && idl !='')
									if(idl !=null && idl !='' && idl !='-1') // Case# 20148038
									{
										transaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', idl);
									}
									//case 20126191�,20126073 end
									if (empId != null && empId != "") {
										transaction.setFieldValue('custrecord_taskassignedto', empId);
									}
									else
									{
										transaction.setFieldValue('custrecord_taskassignedto', currentUserID);
									}
									nlapiSubmitRecord(transaction, true);

									//Case# 20148038 starts
									//MoveTaskRecord(opentaskintid);
									if(systemRule=='LP')
									{
										try
										{
									MoveTaskRecord(opentaskintid);
										}
										catch(exp)
										{
											nlapiLogExecution('ERROR', 'Exception in  MoveTaskRecord');
										}
									}
									//Case# 20148038 ends
								}
							}
							//upto to here on 10th june 2011
							else
							{
								var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');
								nlapiLogExecution('ERROR', 'CreATIN INVT  REc2 ', 'INVT');
								invtRec.setFieldValue('name', idl);
								invtRec.setFieldValue('custrecord_ebiz_inv_binloc', putBinLoc);
								invtRec.setFieldValue('custrecord_ebiz_inv_lp', putLP);
								invtRec.setFieldValue('custrecord_ebiz_inv_sku', putItemId);
								invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', putItemStatus);
								invtRec.setFieldValue('custrecord_ebiz_inv_packcode', putItemPC);
								invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(putQty).toFixed(5));
								//invtRec.setFieldValue('custrecord_ebiz_inv_recorddate',(parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
								//invtRec.setFieldValue('custrecord_ebiz_inv_rectime',((curr_hour) + ":" + (curr_min) + " " + a_p));
								invtRec.setFieldValue('custrecord_inv_ebizsku_no', putItemId);
								//invtRec.setFieldValue('custrecord_invt_ebizlp',putLP);
								//invtRec.setFieldValue('custrecord_ebiz_uomlvl','1');
								//invtRec.setFieldValue('custrecord_ebiz_tranid',idl);
								invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(putQty).toFixed(5));
								invtRec.setFieldValue('custrecord_ebiz_itemdesc', putItemDesc);
								//invtRec.setFieldValue('custrecord_ebiz_inv_loc', poLocation);

								invtRec.setFieldValue('custrecord_invttasktype', 2);
								invtRec.setFieldValue('custrecord_wms_inv_status_flag', ['19']); //Inventory S
								invtRec.setFieldValue('custrecord_ebiz_displayfield','N');

								// invtRec.setFieldValue('custrecord_ebiz_inv_account_no', 1);
								// invtRec.setFieldValue('custrecord_ebiz_inv_account_no', 1110);  //
								//invtRec.setFieldValue('custrecord_ebiz_inv_account_no', varAccountNo);
								//giving LOT NO id
								if (ItemType == "lotnumberedinventoryitem" || batchflg == "T")
								{ // invtRec.setFieldValue('custrecord_ebiz_inv_lot', putItemBatch);
									//Checking FIFO Policy.
									/*var fifovalue = FifovalueCheck(ItemType,putItemId,itemfamId,itemgrpId,putLine,putPoId,getlotnoid);
									if (getlotnoid != "") {
										nlapiLogExecution('ERROR','FIFO Value Check.',fifovalue);
										invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
										invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
									}*/
									
									if (getlotnoid != "") {
										//Checking FIFO Policy.
										var fifodate = nlapiLookupField('customrecord_ebiznet_batch_entry', getlotnoid, 'custrecord_ebizfifodate');
										if(fifodate == null ||fifodate=='' ){
											fifovalue = FifovalueCheck(ItemType,putItemId,itemfamId,itemgrpId,putLine,putPoId,getlotnoid);
											nlapiLogExecution('ERROR','FIFO DATE from FIFOvalue check fn',fifovalue);
										}
										else
											fifovalue=fifodate;
										expiryDateInLot = nlapiLookupField('customrecord_ebiznet_batch_entry', getlotnoid, 'custrecord_ebizexpirydate');
									}
									invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
									invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
									if(expiryDateInLot!=null && expiryDateInLot!='')
									invtRec.setFieldValue('custrecord_ebiz_expdate', expiryDateInLot);
								}

								invtRec.setFieldValue('custrecord_ebiz_inv_loc', poloc);
								//invtRec.setFieldValue('custrecord_ebiz_inv_loc', varItemStatusMapLocation);
								invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');                        
								invtRec.setFieldValue('custrecord_ebiz_inv_company', PutwCompany);

								nlapiLogExecution('ERROR', 'Before Submitting invtrecid', 'INVTRECORDS');
								var invtrecid = nlapiSubmitRecord(invtRec, false, true);
								InvtRefNo=invtrecid;
								nlapiLogExecution('ERROR', 'After Submitting invtrecid', invtrecid);
								if (invtrecid != null) {
									nlapiLogExecution('ERROR', 'Cust INVT Rec Creation Succes with ID ', invtrecid);
								}
								else {
									nlapiLogExecution('ERROR', 'Cust INVT Rec Creation Fail ', 'Fail');
								}

								//Added for updation of Cube information by ramana on 10th june 2011

								nlapiLogExecution('ERROR', 'OLD BIN LOCATION DETAILS ', putoldBinLoc);
								nlapiLogExecution('ERROR', 'NEW BIN LOCATION DETAILS ', putBinLoc);
								nlapiLogExecution('ERROR', 'ITEMID', putItemId);
								nlapiLogExecution('ERROR', 'LP', putLP);

								if(putBinLoc!=putoldBinLoc)
								{   
									/*
											//For the OLD Location Updation Purpose.    											 											
									 */

									var arrDims = getSKUCubeAndWeight(putItemId, 1);
									var itemCube = 0;
									if (arrDims[0] != "" && (!isNaN(arrDims[0]))) 
									{
										var uomqty = ((parseFloat(putQty))/(parseFloat(arrDims[1])));			
										itemCube = (parseFloat(uomqty) * parseFloat(arrDims[0]));
										nlapiLogExecution('ERROR', 'checkInPOSTRequest:itemCube', itemCube);
									} 

									var vOldRemainingCube = GeteLocCube(putoldBinLoc);
									nlapiLogExecution('ERROR', 'vOldRemainingCube', vOldRemainingCube);

									var vTotalCubeValue = parseFloat(itemCube)+ parseFloat(vOldRemainingCube);
									nlapiLogExecution('ERROR', 'vOldLocationRemainingCube', vTotalCubeValue);

									var retValue =  UpdateLocCube(putoldBinLoc,vTotalCubeValue);
									//upto to here old Location Updation purpose

									//For the New Location updation

									var vNewLocationRemainingCube = GeteLocCube(putBinLoc);   

									nlapiLogExecution('ERROR', 'vNewLocationRemainingCube', vNewLocationRemainingCube);

									//var vTotalNewLocation =  parseFloat(vartotalCube)- parseFloat(vNewLocationRemainingCube);
									var vTotalNewLocationCube =  parseFloat(vNewLocationRemainingCube)- parseFloat(itemCube);

									nlapiLogExecution('ERROR', 'vTotalNewLocation', vTotalNewLocationCube);

									if(vTotalNewLocationCube<0)
									{
										nlapiLogExecution('ERROR', 'inside PO Overage Validation', 'inside loop');					  
										var form = nlapiCreateForm('Confirm Putaway');
										nlapiLogExecution('ERROR', 'Form Called', 'form');					  
										var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);		  
										msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'Qty exceeds location capacity', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");		    		   
										nlapiLogExecution('ERROR', 'message', msg);					  
										response.writePage(form);                  						
										return;			
									}
									else
									{
										var retValue1 = UpdateLocCube(putBinLoc,vTotalNewLocationCube);
									}
									//upto to here new location

								}                        
								//upto to here on 10th june 2011

								var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', opentaskintid);

								//nlapiLogExecution('DEBUG', 'location', request.getLineItemValue('custpage_items', 'custpage_location', i + 1));

								//   nlapiLogExecution('ERROR', 'STATUS before', searchresult.getValue('custrecord_wms_status_flag'));

								if (transaction.getFieldValue('custrecord_wms_status_flag') == 6) 
								{
									nlapiLogExecution('ERROR', 'inside if condition', transaction.getFieldValue('custrecord_wms_status_flag'));
									transaction.setFieldValue('custrecord_actbeginloc', request.getLineItemValue('custpage_items', 'custpage_location', k));
									transaction.setFieldValue('custrecordact_begin_date', DateStamp());
									transaction.setFieldValue('custrecord_actualbegintime', TimeStamp());
								}
																	//case 20125353 start Updation of Quantity through Qty exception
								transaction.setFieldValue('custrecord_act_qty', parseFloat(putQty).toFixed(5));
								//case end
								transaction.setFieldValue('custrecord_actualendtime', TimeStamp());
								//transaction.setFieldValue('custrecord_recordtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
								transaction.setFieldValue('custrecord_recordupdatetime', TimeStamp());//((curr_hour) + ":" + (curr_min) + " " + a_p));
								transaction.setFieldValue('custrecord_upd_date', DateStamp());//(parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
								//transaction.setFieldValue('custrecord_current_date', (parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
								//case 20126191�,20126073 start : changed i to k
								transaction.setFieldValue('custrecord_actendloc', request.getLineItemValue('custpage_items', 'custpage_location', k));
								//case 20126191�,20126073 end
								transaction.setFieldValue('custrecord_act_end_date', DateStamp()); //(parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
								transaction.setFieldValue('custrecord_batch_no', request.getLineItemValue('custpage_items', 'custpage_lotbatch', k));
								// Case # 20127918/20127913 starts
								transaction.setFieldValue('custrecord_sku_status', request.getLineItemValue('custpage_items', 'custpage_itemstatus', k));
								transaction.setFieldValue('custrecord_serial_no', request.getLineItemValue('custpage_items', 'custpage_serialnos', k));
								// Case # 20127918/20127913 end
								transaction.setFieldValue('custrecord_wms_status_flag', 3);//Inbound S
								transaction.setFieldValue('custrecord_wms_location', poloc);
								var currentContext = nlapiGetContext();  
								var currentUserID = currentContext.getUser();
								transaction.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
								//   nlapiLogExecution('ERROR', 'STATUS after', searchresult.getValue('custrecord_wms_status_flag'));
								
								nlapiLogExecution("ERROR","InvtRefNo",InvtRefNo);
								transaction.setFieldValue('custrecord_invref_no', InvtRefNo);
								//case 20126191,20126073 start
								//if(idl !=null && idl !='')
								if(idl !=null && idl !='' && idl !='-1')// Case# 20148038
								{
									transaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', idl);
								}
								//case 20126191�,20126073 end
								if (empId != null && empId != "") {
									transaction.setFieldValue('custrecord_taskassignedto', empId);
								}
								else
								{
									transaction.setFieldValue('custrecord_taskassignedto', currentUserID);
								}
								nlapiSubmitRecord(transaction, true);

								//Case# 20148038 starts
								//MoveTaskRecord(opentaskintid);
								if(systemRule=='LP')
								{
									try
									{
								MoveTaskRecord(opentaskintid);
									}
									catch(exp)
									{
										nlapiLogExecution('ERROR', 'Exception in  MoveTaskRecord');
									}
								}
								//Case# 20148038 ends

							}
						} 
						catch (error) {
							nlapiLogExecution('ERROR', 'Check for error msg in create invt', error);
						}
						//case# 20148770 starts
						
						try
						{
							DeleteInvtRecCreatedforCHKNTask(pointid);
						}
						catch(e)
						{
							nlapiLogExecution('ERROR', 'Delete Rec Error', e);
						}
						
						
						//Serial number Updation to "S".	
						try {
							if (serialId != null && serialId != "") {
								nlapiLogExecution('ERROR', 'updating Serial num Records to', 'Storage Status');
								nlapiLogExecution('ERROR', 'serialId.length', serialId.length);
								nlapiLogExecution('ERROR', 'serialId', serialId);
								for (var r = 0; r < serialId.length; r++) {
									var fields = new Array();
									var values = new Array();
//									fields[0] = 'custrecord_serialwmsstatus';
//									values[0] = '3';

									fields[0] = 'custrecord_serialwmsstatus';
									fields[1] = 'custrecord_serialbinlocation';
									values[0] = '3';
									values[1] = putBinLoc;
									nlapiLogExecution('ERROR','putBinLoc',putBinLoc);
									nlapiLogExecution('ERROR', 'serialId[r]', serialId[r]);
									var updateStatus = nlapiSubmitField('customrecord_ebiznetserialentry', serialId[r], fields, values);
									nlapiLogExecution('ERROR', 'Inside loop serialId ', serialId[r]);
								}
								nlapiLogExecution('ERROR', 'Serial num Records to', 'Storage Status Success');
							}
						} 
						catch (myexp) {
							nlapiLogExecution('ERROR', 'Into Serial num exeption', myexp);
						}
						
						try
						{
							nlapiLogExecution('ERROR', 'putQty', putQty);
							nlapiLogExecution('ERROR', 'voldqty', voldqty);
							if(parseFloat(putQty)<=parseFloat(voldqty))
							{
								nlapiLogExecution('ERROR', 'Qty Exception', null);
								var ItemInfo = eBiz_RF_GetItemCubeForItem(putItemId);

								var ItemCube = 0; 
								if(ItemInfo[0] != null && ItemInfo[0] != "")
									ItemCube = ItemInfo[0];
								nlapiLogExecution('ERROR', 'ItemCube', ItemCube);

								var ActualBeginDate = DateStamp();
								var ActualBeginTime = TimeStamp();
								var TimeArray = new Array();
								TimeArray = ActualBeginTime.split(' ');

								var getActualBeginTime = TimeArray[0];
								var getActualBeginTimeAMPM = TimeArray[1];
								var WmsLocation = "";
								if(putPoId!=null && putPoId!='')
								{
									var fields = ['location'];
									var columns = nlapiLookupField('transaction', putPoId, fields);
									WmsLocation = columns.location;
									nlapiLogExecution('ERROR', 'WmsLocation', WmsLocation);
								}
								var containerLP="";
								var remainqty =parseFloat(voldqty)-parseFloat(putQty);
								nlapiLogExecution('ERROR','remainqty',remainqty);
								nlapiLogExecution('ERROR', 'putPoId', putPoId);
								nlapiLogExecution('ERROR', 'putLine', putLine);
								nlapiLogExecution('ERROR', 'putQty', putQty);
								updateTrnOrderLine(putPoId,putLine,remainqty);
								nlapiLogExecution('ERROR', 'Out of updateTrnOrderLine', null);
								updateChknTaskQty(putLP,putQty);
								nlapiLogExecution('ERROR', 'After updateChknTaskQty', null);

								var itemremqty=0;
								if(parseFloat(remainqty)!=0)
								{
									CheckinLp(putItemId, vname,putItemId, putLine,putPoId, remainqty,itemremqty, 
											putItemPC,putItemStatus,
											voldqty, putQty,
											putItemDesc, ItemCube, 
											ActualBeginDate, getActualBeginTime, getActualBeginTimeAMPM,
											WmsLocation,containerLP,"",putLotBatch,putLP);
								}
							}
						}
						catch (error)
						{
							nlapiLogExecution('ERROR', 'Check for error msg in qty exec', error);
						}
						//case# 20148770 ends
					}
				}

				// Calling function to Delete Records.
				

				//Transaction Line Updation.
				var type = tranType;
				var putgenqty = 0;
				var putconfqty = 0;
				var ttype = "PUTW";				
				for (var p = 0; p < arrLine.length; p++) 
				{

					var confqty = arrQty[p]; //checkinLineQty;
					var lineno = arrLine[p];

					nlapiLogExecution('ERROR', 'Inside TRN Line Updation', confqty);

					var tranValue = TrnLineUpdation(type, ttype, pointid, pointid, lineno, null, null, confqty);

				}                
				
			} //if Item receipt is not null.
			else {
				nlapiLogExecution('ERROR', 'If Item Receipt Failed', 'Error in Item Receipt');
			}

		} 
		catch (e) {
			if (e instanceof nlobjError) 
				nlapiLogExecution('ERROR', 'system error', e.getCode() + '\n' + e.getDetails());
			else 
				nlapiLogExecution('ERROR', 'unexpected error', e);
		}
		//case# 20148770 starts
		if(parseFloat(voldqty) != parseFloat(putQty)){
			nlapiLogExecution('ERROR','into excepqty voldqty',voldqty);
			nlapiLogExecution('ERROR','into excepqty putQty',putQty);
			nlapiLogExecution('ERROR','into excepqty putQty',pointid);
			var excepqty=parseFloat(voldqty)-parseFloat(putQty);			
			nlapiLogExecution('ERROR','into excepqty excepqty',excepqty);
			var lineFilers = new Array;
			lineFilers.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', pointid));
			if(lineno!=null && lineno!='')
				lineFilers.push(new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', lineno));
			var lineSearchResults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, lineFilers, null);
			if(lineSearchResults!=null && lineSearchResults!='' && lineSearchResults.length>0)
			{
				var linetransaction = nlapiLoadRecord('customrecord_ebiznet_order_line_details', lineSearchResults[0].getId());
				linetransaction.setFieldValue('custrecord_orderlinedetails_putexcep_qty', parseFloat(excepqty).toFixed(5));					 
				nlapiSubmitRecord(linetransaction, false, true);
			}
		}
		//case# 20148770 ends
		nlapiLogExecution('ERROR', 'remaining usage', nlapiGetContext().getRemainingUsage());        
		// nlapiSetRedirectURL('RECORD', 'purchaseorder', pointid, false);
		nlapiSetRedirectURL('RECORD', tranType, pointid, false);

	} //end of response object.
} //End of function.
function getSerialNoCSV(arrayLP, lp){
	var csv = "";
	for (var i = 0; i < arrayLP.length; i++) {
		if (arrayLP[i][0] == lp) {
			csv += arrayLP[i][1] + " , ";
		}
	}
	return csv;
}

//Function used to delete records that are created in Create Inventory for CHKN task.
function DeleteInvtRecCreatedforCHKNTask(pointid)
{
	nlapiLogExecution('ERROR','Inside DeleteInvtRecCreatedforCHKNTask ','Funciton');
	var Ifilters = new Array();
	Ifilters.push(new nlobjSearchFilter('name', null, 'is', pointid));
	Ifilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [1,17]));
	Ifilters.push(new nlobjSearchFilter('custrecord_invttasktype', null, 'anyof', [1]));
	var invtId="";
	var invtType="";
	var serchInvtRec= nlapiSearchRecord('customrecord_ebiznet_createinv', null, Ifilters, null);
	if (serchInvtRec) 
	{
		for (var s = 0; s < serchInvtRec.length; s++) {
			nlapiLogExecution('ERROR','Inside loop','loop');
			var searchresult = serchInvtRec[ s ];
			nlapiLogExecution('ERROR','need to print this','print');
			invtId = serchInvtRec[s].getId();
			invtType= serchInvtRec[s].getRecordType();
			nlapiLogExecution('ERROR','CHKN INVT Id',invtId);
			nlapiLogExecution('ERROR','CHKN invtType ',invtType);
			nlapiDeleteRecord(serchInvtRec[s].getRecordType(),serchInvtRec[s].getId());
			nlapiLogExecution('ERROR','Invt Deleted record Id',invtId);

		}
	}

}
function GetSystemRuleForPostItemReceiptby(whLocation)
{
	
	try
	{
		var rulevalue='';	
		var filter=new Array();
		filter.push(new nlobjSearchFilter('name',null,'is','Post Item Receipt by'));
		//filter.push(new nlobjSearchFilter('custrecord_ebizsite',null,'anyof',['@NONE@',whLocation]));

		// case no start 20126968
		var vRoleLocation=getRoledBasedLocation();
		var resloc=new Array();
		resloc.push("@NONE@");

		nlapiLogExecution('ERROR','vRoleLocation',vRoleLocation);
		//case# 20149824
		if(whLocation!=null && whLocation!='')
		{
			filter.push(new nlobjSearchFilter('custrecord_ebizsite',null,'anyof',['@NONE@',whLocation]));
		}
		else if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
		{
			for(var count=0;count<vRoleLocation.length;count++)
				resloc.push(vRoleLocation[count]);
			filter.push(new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', resloc));
		}
		// Case# 20149154 starts
		/*else
			{
			filter.push(new nlobjSearchFilter('custrecord_ebizsite',null,'anyof',['@NONE@',whloc]));
			}*/
		// Case# 20149154 ends
		// case no end 20126968
		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizrulevalue');
		column[1]=new nlobjSearchColumn('custrecord_ebizsite');


		var searchresult=nlapiSearchRecord('customrecord_ebiznet_sysrules',null,filter,column);
		var siteval = '';
		if(searchresult!=null && searchresult!="")
		{
			for(var k = 0;k<searchresult.length;k++)
			{
				siteval = searchresult[k].getValue('custrecord_ebizsite');
				nlapiLogExecution('ERROR','whloc',whLocation);
				nlapiLogExecution('ERROR','siteval',siteval);

				if(whLocation!=null && whLocation!='')
				{
					if(whLocation == siteval)
					{
						rulevalue=searchresult[k].getValue('custrecord_ebizrulevalue');
						break;
					}
				}
			}

			if(rulevalue==null || rulevalue=='')
			{
				for(var z = 0;z<searchresult.length;z++)
				{
					siteval = searchresult[z].getValue('custrecord_ebizsite');

					if(siteval==null || siteval=='')
					{
						rulevalue=searchresult[z].getValue('custrecord_ebizrulevalue');
						break;
					}
				}

			}
		}

		nlapiLogExecution('ERROR','rulevalue',rulevalue);
		if(rulevalue!=null && rulevalue!='undefined' && rulevalue!='null' && rulevalue!='')
		{
			if(rulevalue.trim()!='LP' && rulevalue.trim()!='PO')
			{
				rulevalue='LP';
			}
		}
		else
		{
			rulevalue='LP';
		}
		nlapiLogExecution('ERROR','rulevalue',rulevalue);
		return rulevalue;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetSystemRuleForPostItemReceiptby',exp);
	}

	
	/*
	try
	{
		nlapiLogExecution('ERROR','vlocation',vlocation);
		var rulevalue='LP';	
		var filter=new Array();
		filter.push(new nlobjSearchFilter('name',null,'is','Post Item Receipt by'));
		//filter.push(new nlobjSearchFilter('custrecord_ebizsite',null,'anyof',['@NONE@',whLocation]));
		
		// case no start 20126968
		var vRoleLocation=getRoledBasedLocation();

		nlapiLogExecution('ERROR','vRoleLocation',vRoleLocation);

		if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
		{
			vRoleLocation.push('@NONE@');// case# 201412871
			filter.push(new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', vRoleLocation));
		}
		// Case# 20148038 starts
		else
			{
			filter.push(new nlobjSearchFilter('custrecord_ebizsite',null,'anyof',['@NONE@',vlocation]));
			}
		// Case# 20148038 ends
		// case no end 20126968
		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizrulevalue');
		column[1]=new nlobjSearchColumn('custrecord_ebizsite'); // CAse# 201410435

		var searchresult=nlapiSearchRecord('customrecord_ebiznet_sysrules',null,filter,column);

		if(searchresult!=null && searchresult!="")
		{
			if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
			{
				// if vRolelocation has multiple locations
				nlapiLogExecution('ERROR','into if',vRoleLocation);
				for(var t=0;t<searchresult.length;t++)
				{
					var vSiteLoc = searchresult[t].getValue('custrecord_ebizsite');
					if(vSiteLoc !=null && vSiteLoc !='')
					{
						if(parseInt(vSiteLoc)==parseInt(vlocation))
						{
							rulevalue=searchresult[t].getValue('custrecord_ebizrulevalue');
						}

					}
					else
					{
						rulevalue=searchresult[t].getValue('custrecord_ebizrulevalue');
					}
				}
			}
			else
			{

				rulevalue=searchresult[0].getValue('custrecord_ebizrulevalue');
			}
		}
			
		nlapiLogExecution('ERROR','rulevalue',rulevalue);
		if(rulevalue!=null && rulevalue!='undefined' && rulevalue!='null' && rulevalue!='')
		{
			if(rulevalue.trim()!='LP' && rulevalue.trim()!='PO')
			{
				rulevalue='LP';
			}
		}
		else
		{
			rulevalue='LP';
		}
		nlapiLogExecution('ERROR','rulevalue',rulevalue);
		return rulevalue;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetSystemRuleForPostItemReceiptby',exp);
	}
*/
	
}


//case# 20148770 starts
function CheckinLp(ItemId,POIdText,poitem,lineno,pointernalid,poqtyentered,poitemremainingqty,polinepackcode,
		polineitemstatus,polinequantity,polinequantityreceived,itemdescription,itemcube,
		ActualBeginDate,ActualBeginTime,ActualBeginTimeAMPM,WmsLocation,containerLP,NewLp,vBatchNo,putLP)
{
	var NewGenLP="";//added by santosh

	var POarray=new Array();
	nlapiLogExecution('ERROR','ItemId',ItemId);
	nlapiLogExecution('ERROR','POIdText',POIdText);
	nlapiLogExecution('ERROR','poitem',poitem);
	nlapiLogExecution('ERROR','pointernalid',pointernalid);
	nlapiLogExecution('ERROR','poqtyentered',poqtyentered);
	nlapiLogExecution('ERROR','poitemremainingqty',poitemremainingqty);
	nlapiLogExecution('ERROR','polinepackcode',polinepackcode);
	nlapiLogExecution('ERROR','polineitemstatus',polineitemstatus);
	nlapiLogExecution('ERROR','polinequantity',polinequantity);
	nlapiLogExecution('ERROR','polinequantityreceived',polinequantityreceived);
	nlapiLogExecution('ERROR','itemdescription',itemdescription);
	nlapiLogExecution('ERROR','itemcube',itemcube);
	nlapiLogExecution('ERROR','ActualBeginDate',ActualBeginDate);
	nlapiLogExecution('ERROR','ActualBeginTime',ActualBeginTime);
	nlapiLogExecution('ERROR','ActualBeginTimeAMPM',ActualBeginTimeAMPM);
	nlapiLogExecution('ERROR','WmsLocation',WmsLocation);
	nlapiLogExecution('ERROR','vBatchNo',vBatchNo);

	var getPONo = POIdText;
	var getPOItem = poitem;
	var getPOLineNo = lineno;

	var getPOInternalId = pointernalid;
	var getPOQtyEntered = poqtyentered;
	var getPOItemRemainingQty = poitemremainingqty;
	var getPOLinePackCode = polinepackcode;
	var getPOLineItemStatus = polineitemstatus;
	var getPOLineQuantity = polinequantity;
	var getPOLineQuantityReceived = polinequantityreceived;
	var getItemDescription = itemdescription;
	var getItemCube = itemcube;
	var getActualBeginDate = ActualBeginDate;
	var getFetchedItemId =ItemId;
	var getActualBeginTime = ActualBeginTime;
	var getActualBeginTimeAMPM = ActualBeginTimeAMPM;

	var getLineCount = 1;
	var getBaseUOM = 'EACH';
	var getBinLocation = "";

//	POarray["custparam_batchno"] = request.getParameter('hdnBatchNo');
	POarray["custparam_batchno"] =vBatchNo;
	POarray["custparam_mfgdate"] = request.getParameter('hdnMfgDate');
	POarray["custparam_expdate"] = request.getParameter('hdnExpDate');
	POarray["custparam_bestbeforedate"] = request.getParameter('hdnBestBeforeDate');
	POarray["custparam_lastdate"] = request.getParameter('hdnLastAvlDate');
	POarray["custparam_fifodate"] = request.getParameter('hdnFifoDate');
	POarray["custparam_fifocode"] = request.getParameter('hdnFifoCode');
	POarray["custparam_cartlp"] = request.getParameter('custparam_cartlp');



	var POfilters = new Array();
	nlapiLogExecution('ERROR', 'getPOInternalId', pointernalid);
	POfilters[0] = new nlobjSearchFilter('name', null, 'is', pointernalid);

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_poreceipt', null, POfilters, null);

	var getPOReceiptNo = '';

	if (searchresults != null && searchresults.length > 0) {
		for (var i = 0; searchresults != null && i < searchresults.length; i++) {
			getPOReceiptNo = searchresults[i].getValue('custrecord_ebiz_poreceipt_receiptno');
		}

		nlapiLogExecution('ERROR', 'PO Receipt No', getPOReceiptNo);
	}

	//code added by santosh on 07Aug12
	if(NewLp!=null && NewLp!="")
	{
		NewGenLP=NewLp;

	}
	else
	{
		var getsysItemLP = GetMaxLPNo(1, 1,WmsLocation);
		nlapiLogExecution('ERROR', 'getsysItemLP LP now added iis', getsysItemLP);	

		var LPReturnValue = "";
		nlapiLogExecution('ERROR', 'getsysItemLP', getsysItemLP);
		LPReturnValue = true;
		nlapiLogExecution('ERROR', 'LP Return Value new', LPReturnValue);
		var lpExists = 'N';
		//LP Checking in masterlp record starts
		/*if (LPReturnValue == true) {
		try {
			nlapiLogExecution('ERROR', 'INTO MASTER LP INSERTION');
			var filtersmlp = new Array();
			filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', getsysItemLP);

			var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

			if (SrchRecord != null && SrchRecord.length > 0) {
				nlapiLogExecution('ERROR', 'LP FOUND');

				lpExists = 'Y';
			}
			else {
				nlapiLogExecution('ERROR', 'LP NOT FOUND');
				var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
				customrecord.setFieldValue('name', getsysItemLP);
				customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', getsysItemLP);
				var rec = nlapiSubmitRecord(customrecord, false, true);
			}
		} 
		catch (e) {
			nlapiLogExecution('ERROR', 'Failed to Update/Insert into Master LP Record');
		}
	}*/

		getsysItemLP=CheckForLPExist(getsysItemLP,WmsLocation);
		nlapiLogExecution('ERROR', 'getsysItemLP', getsysItemLP);
		NewGenLP=getsysItemLP;
	}
	//end of the code by 07Aug12

	//case# 20148559 starts
	var stagelocid = getStagingLocation(WmsLocation);
	
	var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');
	nlapiLogExecution('DEBUG', 'Creating Inventory Record', 'INVT');
	var accountNumber = "";

	invtRec.setFieldValue('name', pointernalid);
	invtRec.setFieldValue('custrecord_ebiz_inv_binloc', stagelocid);
	invtRec.setFieldValue('custrecord_ebiz_inv_lp', NewGenLP);
	invtRec.setFieldValue('custrecord_ebiz_inv_sku', ItemId);
	invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', polineitemstatus);
	
	if(polinepackcode==null || polinepackcode=='')
		polinepackcode=1;

	invtRec.setFieldValue('custrecord_ebiz_inv_packcode', polinepackcode);
	invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(poqtyentered).toFixed(5));
	invtRec.setFieldValue('custrecord_ebiz_inv_loc', WmsLocation);
	invtRec.setFieldValue('custrecord_inv_ebizsku_no', ItemId);
	
	invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');

	invtRec.setFieldValue('custrecord_wms_inv_status_flag','17');//17=FLAG.INVENTORY.INBOUND
	invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
	invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(poqtyentered).toFixed(5));
	invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemdescription);
	invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);
	invtRec.setFieldValue('custrecord_invttasktype', 1);
//	if(uomlevel!=null && uomlevel!='')
	//	invtRec.setFieldText('custrecord_ebiz_uomlvl', uomlevel);



	if(vBatchNo!=null && vBatchNo!=''&& vBatchNo!='null')
	{
		nlapiLogExecution('DEBUG', 'batchno into if', vBatchNo);
		try
		{
		
			var filterBatch=new Array();
			filterBatch[0]=new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',vBatchNo);
			if(ItemId!=null&&ItemId!="")
				filterBatch[1]=new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',ItemId);
			var columnBatch=new Array();
			columnBatch[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
			columnBatch[1]=new nlobjSearchColumn('custrecord_ebizfifodate');
			var rec=nlapiSearchRecord('customrecord_ebiznet_batch_entry',null,filterBatch,columnBatch);
			var BatchID=rec[0].getId();
		
			invtRec.setFieldValue('custrecord_ebiz_inv_lot', BatchID);
			invtRec.setFieldValue('custrecord_ebiz_expdate', rec[0].getValue('custrecord_ebizexpirydate'));
			invtRec.setFieldValue('custrecord_ebiz_inv_fifo', rec[0].getValue('custrecord_ebizfifodate'));
		}
		catch(exp)
		{
			nlapiLogExecution('DEBUG', 'Exception', exp);
		}
	}

	
	if(pointernalid!=null && pointernalid!="")
		invtRec.setFieldValue('custrecord_ebiz_transaction_no', pointernalid);

	var invtRecordId = nlapiSubmitRecord(invtRec, false, true);
	nlapiLogExecution('ERROR','invtRecordId',invtRecordId);
	
	//case# 20148559 ends

	
	
	
	if(lpExists != 'Y')
	{
		var itemSubtype = nlapiLookupField('item', ItemId, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
		POarray["custparam_recordtype"] = itemSubtype.recordType;
		var batchflag=itemSubtype.custitem_ebizbatchlot;
		nlapiLogExecution('ERROR', 'itemSubtype.recordType =', itemSubtype.recordType);
		nlapiLogExecution('ERROR', 'itemSubtype.custitem_ebizserialin =', itemSubtype.custitem_ebizserialin);
		nlapiLogExecution('ERROR', 'itemSubtype.custitem_ebizbatchlot =', itemSubtype.custitem_ebizbatchlot);

		//POarray["custparam_polineitemlp"] = getsysItemLP;
		POarray["custparam_polineitemlp"] = NewGenLP;//added by santosh on 07Aug12
		POarray["custparam_poreceiptno"] = getPOReceiptNo;
		var TotalItemCube = parseFloat(itemcube) * parseFloat(poqtyentered);
		nlapiLogExecution('ERROR', 'Total Item Cube ', TotalItemCube );

//		var getBaseUOM = 'EACH';
		//case# 20149408 20149409 starts
		var filters2 = new Array();

		var columns2 = new Array();
		columns2[0] = new nlobjSearchColumn('custitem_item_family');
		columns2[1] = new nlobjSearchColumn('custitem_item_group');
		columns2[2] = new nlobjSearchColumn('custitem_ebizdefskustatus');
		columns2[3] = new nlobjSearchColumn('custitem_item_info_1');
		columns2[4] = new nlobjSearchColumn('custitem_item_info_2');
		columns2[5] = new nlobjSearchColumn('custitem_item_info_3');
		columns2[6] = new nlobjSearchColumn('custitem_ebizabcvelitem');
		filters2.push(new nlobjSearchFilter('internalid', null, 'is', ItemId));
		var ItemInfoResults = nlapiSearchRecord('item', null, filters2, columns2);

		var filters3=new Array();
		filters3.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		var columns3=new Array();
		columns3[0] = new nlobjSearchColumn('custrecord_skufamilypickrule');
		columns3[1] = new nlobjSearchColumn('custrecord_skugrouppickrule');
		columns3[2] = new nlobjSearchColumn('custrecord_skustatuspickrule');
		columns3[3] = new nlobjSearchColumn('custrecord_skuinfo1pickrule');
		columns3[4] = new nlobjSearchColumn('custrecord_skuinfo2pickrule');
		columns3[5] = new nlobjSearchColumn('custrecord_skuinfo3pickrule');
		columns3[6] = new nlobjSearchColumn('custrecord_skupickrule');
		columns3[7] = new nlobjSearchColumn('custrecord_abcvelpickrule');
		columns3[8] = new nlobjSearchColumn('custrecord_ebizsitepickput');
		columns3[9] = new nlobjSearchColumn('custrecord_ruleidpickrule');
		columns3[10] = new nlobjSearchColumn('custrecord_putawaymethod');
		columns3[11] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
		columns3[12] = new nlobjSearchColumn('custrecord_manual_location_generation');
		columns3[13] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
		columns3[14] = new nlobjSearchColumn('formulanumeric');
		columns3[14].setFormula("TO_NUMBER({custrecord_sequencenumberpickrule})");
		columns3[15] = new nlobjSearchColumn('custrecord_locationpickrule');
		//The below column is added by Satish.N on 29OCT2013 - Case # 20124515
		columns3[16] = new nlobjSearchColumn('custrecord_emptylocation','custrecord_putawaymethod');
		// Upto here on 29OCT2013 - Case # 20124515
		columns3[14].setSort();

		var putrulesearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters3, columns3);

		
		/*var getBeginLocation = generatePutawayLocation(ItemId, polinepackcode,
				polineitemstatus, getBaseUOM, parseFloat(TotalItemCube), itemSubtype.recordType,WmsLocation);*/
		var getBeginLocation = GetPutawayLocationNew(ItemId, polinepackcode,
				polineitemstatus, getBaseUOM, parseFloat(TotalItemCube), itemSubtype.recordType,WmsLocation,ItemInfoResults,putrulesearchresults);
		//case# 20149408 20149409 ends
		nlapiLogExecution('ERROR','getBeginLocation',getBeginLocation);


		var context = nlapiGetContext();
		nlapiLogExecution('ERROR','Remaining usage 2 ',context.getRemainingUsage());
		nlapiLogExecution('ERROR', 'Begin Location', getBeginLocation);

		var vLocationname="";
		if (getBeginLocation != null && getBeginLocation != '') {
			var getBeginLocationId = getBeginLocation[2];
			vLocationname= getBeginLocation[0];
			nlapiLogExecution('ERROR', 'Begin Location Id', getBeginLocationId);
		}
		else {
			var getBeginLocationId = "";
			nlapiLogExecution('ERROR', 'Begin Location Id is null', getBeginLocationId);
		}

		var remainingCube = 0;
		if(getBeginLocation!=null && getBeginLocation != '')
			remainingCube = getBeginLocation[1];

		nlapiLogExecution('ERROR', 'remainingCube', remainingCube);
		if (getBeginLocation != null && getBeginLocation != '') {
			putmethod = getBeginLocation[9];
			putrule = getBeginLocation[10];
		}
		else
		{
			putmethod = "";
			putrule = "";
		}
		var getActualEndDate = DateStamp();
		nlapiLogExecution('ERROR', 'getActualEndDate', getActualEndDate);
		var getActualEndTime = TimeStamp();
		nlapiLogExecution('ERROR', 'getActualEndTime', getActualEndTime);

		var context = nlapiGetContext();
		nlapiLogExecution('ERROR','Remaining usage 3 ',context.getRemainingUsage());
		var trantype= nlapiLookupField('transaction',pointernalid,'recordType');
		TrnLineUpdation(trantype, 'CHKN', POIdText,pointernalid,lineno, ItemId, 
				polinequantity, poqtyentered,"",polineitemstatus);

		var context = nlapiGetContext();
		nlapiLogExecution('ERROR','Remaining usage 4 ',context.getRemainingUsage());
		var vValid = custChknPutwRecCreation(getPOInternalId, getPONo, getPOQtyEntered, getPOLineNo, getPOItemRemainingQty, getLineCount, getFetchedItemId, 
				getPOItem, getItemDescription, getPOLineItemStatus, getPOLinePackCode, getBaseUOM, NewGenLP, getBinLocation, 
				getPOLineQuantityReceived, getActualEndDate, getActualEndTime, getPOReceiptNo, "", getActualBeginDate, 
				getActualBeginTime, getActualBeginTimeAMPM, getBeginLocationId, POarray["custparam_batchno"], 
				POarray["custparam_mfgdate"], POarray["custparam_expdate"], POarray["custparam_bestbeforedate"], 
				POarray["custparam_lastdate"], POarray["custparam_fifodate"], POarray["custparam_fifocode"], 
				itemSubtype.recordType, containerLP, WmsLocation,batchflag,putmethod,putrule,putLP);
		var context = nlapiGetContext();
		nlapiLogExecution('ERROR','Remaining usage 5 ',context.getRemainingUsage());
		nlapiLogExecution('ERROR', 'vValid', vValid);
		if(vValid==false)
		{
			nlapiLogExecution('ERROR','Exception in generating Locations');
		}
		else
		{


			/*	* This is to insert a record in inventory custom record.
			 * The data that is to be inserted is PO Internal Id, Item, Item Status, Pack Code, Quantity, LP#
			 * The parameters list is: 
			 * pointid, itemid, itemdesc, itemstatus, itempackcode, quantity, invtlp, warehouse location
			 * */

			TrnLineUpdation(trantype, 'ASPW', POIdText,pointernalid,lineno, ItemId, 
					polinequantity, poqtyentered,"",polineitemstatus);

			//updatePutQtyExc(pointernalid,lineno);
			if(getBeginLocationId!=null && getBeginLocationId!='')
			{
				
				UpdateLocCube(getBeginLocationId, remainingCube);
			}
		}
	}
	else
	{
		nlapiLogExecution('ERROR','exception :LP ALEADY Exist.so chkn and putaway task records are failed to create');
	}
}

function custChknPutwRecCreation(po, poValue, quan, lineno, remQty, lineCnt, ItemId, ItemName, 
		ItemDesc, ItemStatus, PackCode, BaseUOM, LP, BinLoc, RcvQty, ActualEndDate, ActualEndTime, 
		poreceiptno, BeginLocation, ActualBeginDate, ActualBeginTime, ActualBeginTimeAMPM, BeginLocationId, 
		BatchNo, MfgDate, ExpDate, BestBeforeDate, LastDate, FifoDate, FifoCode, itemrectype, cartlp, 
		WHLocation,batchflag,putmethod,putrule,putLP)
{
	var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group','custitem_ebizserialin'];
	
	
	var serialInflg="F";		
	
	var columns = nlapiLookupField('item', ItemId, fields);
	var ItemType = columns.recordType;					
	var batchflg = columns.custitem_ebizbatchlot;
	var itemfamId= columns.custitem_item_family;
	var itemgrpId= columns.custitem_item_group;
	serialInflg = columns.custitem_ebizserialin;
	var getlotnoid="";

//	if(ItemStatus==null || ItemStatus=='')
//	ItemStatus='12';

//	nlapiLogExecution('ERROR', 'putLotBatch', putLotBatch);
	
	nlapiLogExecution('ERROR', 'putLP', putLP);
	var filtersser = new Array();
	if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T")
	{

		filtersser[0] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', ItemId);
		filtersser[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', putLP);	
		filtersser[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'is', '1');

		var columnser = new Array();
		columnser[0] = new nlobjSearchColumn('custrecord_serialparentid');
		columnser[1] = new nlobjSearchColumn('custrecord_serialpolineno');

		var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser, columnser);

		if(SrchRecord!=null && SrchRecord!='')
		{
			for( var z=0; z<SrchRecord.length;z++)
			{
				var SerialId = SrchRecord[z].getId();

				if(SerialId!=null && SerialId!='')
				{
					var fields = new Array();
					var values = new Array();
					fields[0] = 'custrecord_serialparentid';
					fields[1] = 'custrecord_serialpolineno';
					//fields[2] = 'custrecord_serialebizrmano';
					
					
					values[0] = LP;
					values[1] = lineno;
					//values[2] = po;
					nlapiLogExecution('ERROR','SerialArray[z]',SerialId);
					nlapiSubmitField('customrecord_ebiznetserialentry', SerialId, fields, values);
				}

			}
		}

	}

	var vValid=true;
	nlapiLogExecution('ERROR', 'Into custChknPutwRecCreation', 'custChknPutwRecCreation');
	var now = new Date();
	var stagelocid, docklocid;
	//case# 20148559 starts
	//stagelocid = getStagingLocation();
	stagelocid = getStagingLocation(WHLocation);
	nlapiLogExecution('ERROR', 'Stage Location', stagelocid);

	docklocid = getDockLocation();
	
	nlapiLogExecution('ERROR', 'Dock Location', docklocid);

	if (BeginLocationId != null && BeginLocationId != '' ) //Creating custom record with CHKN and PUTW task,
	{
		//create the openTask record With CHKN Task Type
		var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('ERROR', 'Creating CHKN Record', 'TRN_OPENTASK');

		//populating the fields
		customrecord.setFieldValue('name', poValue);
		customrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		customrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		customrecord.setFieldValue('custrecord_act_qty', quan);
		customrecord.setFieldValue('custrecord_expe_qty', quan);
		customrecord.setFieldValue('custrecord_lpno', LP);
		customrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		customrecord.setFieldValue('custrecord_line_no', lineno);
		customrecord.setFieldValue('custrecord_packcode', PackCode);
		customrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		customrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		customrecord.setFieldValue('custrecord_tasktype', 1); //For Check-In (CHKN)
		customrecord.setFieldValue('custrecord_actbeginloc', docklocid);
		customrecord.setFieldValue('custrecord_actendloc', stagelocid);
		customrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);
		customrecord.setFieldValue('custrecord_ebiz_order_no', po);

		//Added for Item name and desc
		customrecord.setFieldValue('custrecord_sku', ItemId);
		customrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//For eBiznet Receipt Number .
		customrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		customrecord.setFieldValue('custrecordact_begin_date', ActualBeginDate);
		customrecord.setFieldValue('custrecord_act_end_date', ActualEndDate);

		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		//	customrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		nlapiLogExecution('ERROR', 'getActualBeginTime', getActualBeginTime);
		customrecord.setFieldValue('custrecord_actualendtime', ActualEndTime);
		customrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		customrecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
		customrecord.setFieldValue('custrecord_current_date', DateStamp());
		customrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		customrecord.setFieldValue('custrecord_wms_status_flag', 1);

		// Added by Phani on 03-25-2011
		// Insert the value in the WMS Location
		customrecord.setFieldValue('custrecord_wms_location', WHLocation);//

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" || itemrectype == "assemblyitem"  ||batchflag=='T')
		{
			customrecord.setFieldValue('custrecord_batch_no', BatchNo);
			customrecord.setFieldValue('custrecord_expirydate', ExpDate);
			customrecord.setFieldValue('custrecord_fifodate', FifoDate);
			customrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");
		}
		//cartLP
		customrecord.setFieldValue('custrecord_transport_lp', cartlp);
		nlapiLogExecution('ERROR', 'Submitting CHKN record', 'TRN_OPENTASK');

		var currentUserID = getCurrentUser();

		customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		customrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		customrecord.setFieldValue('custrecord_taskassignedto', currentUserID);

		//commit the record to NetSuite
		var reccheckid = nlapiSubmitRecord(customrecord);
		nlapiLogExecution('ERROR', 'Done CHKN Record Insertion :', 'Success');
		nlapiLogExecution('ERROR', 'BeginLocationId', BeginLocationId);

		//create the openTask record With PUTW Task Type
		if(BeginLocationId==null || BeginLocationId=="")
			return false;
		var putwrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('ERROR', 'Creating PUTW record', 'TRN_OPENTASK');

		//populating the fields
		putwrecord.setFieldValue('name', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		putwrecord.setFieldValue('custrecord_expe_qty', quan);
		putwrecord.setFieldValue('custrecord_lpno', LP);
		putwrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		putwrecord.setFieldValue('custrecord_line_no', lineno);
		putwrecord.setFieldValue('custrecord_packcode', PackCode);
		putwrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		putwrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		putwrecord.setFieldValue('custrecord_tasktype', 2); //For Putaway (PUTW)

		nlapiLogExecution('ERROR', 'Fetched Begin Location', BeginLocation);

		putwrecord.setFieldValue('custrecord_actbeginloc', BeginLocationId);
		putwrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);
		putwrecord.setFieldValue('custrecord_ebiz_order_no', po);

		//For eBiznet Receipt Number .
		putwrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		putwrecord.setFieldValue('custrecordact_begin_date', (parseInt(now.getMonth()) + 1) + '/' + (parseInt(now.getDate())) + '/' + now.getFullYear());

		//Adding fields to update time zones.
		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		//putwrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		putwrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		putwrecord.setFieldValue('custrecord_current_date', DateStamp());
		putwrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		if (BeginLocationId != "") {
			putwrecord.setFieldValue('custrecord_wms_status_flag', 2);
		}
		else {
			putwrecord.setFieldValue('custrecord_wms_status_flag', 6);
		}
		//Added for Item name and desc
		putwrecord.setFieldValue('custrecord_sku', ItemId);
		putwrecord.setFieldValue('custrecord_skudesc', ItemDesc);
		putwrecord.setFieldValue('custrecord_wms_location', WHLocation);//

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" || itemrectype == "assemblyitem" ||batchflag=='T')
		{
			putwrecord.setFieldValue('custrecord_batch_no', BatchNo);
			putwrecord.setFieldValue('custrecord_expirydate', ExpDate);
			putwrecord.setFieldValue('custrecord_fifodate', FifoDate);
			putwrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");
		}
		//cartLP
		putwrecord.setFieldValue('custrecord_transport_lp', cartlp);

		var currentUserID = getCurrentUser();

		putwrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		putwrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		putwrecord.setFieldValue('custrecord_taskassignedto', currentUserID);
		if(putmethod!=null && putmethod!='')
			putwrecord.setFieldValue('custrecord_ebizmethod_no', putmethod);
		if(putrule!=null && putrule!='')
			putwrecord.setFieldValue('custrecord_ebizrule_no', putrule);

		//commit the record to NetSuite
		var putwrecordid = nlapiSubmitRecord(putwrecord);
		var systemRule=GetSystemRuleForPostItemReceiptby(WHLocation);
		if(systemRule=='LP')
		{
			try
			{
				MoveTaskRecord(reccheckid);
			}
			catch(exp)
			{
				nlapiLogExecution('ERROR', 'Exception in  MoveTaskRecord');
			}
		}
		nlapiLogExecution('ERROR', 'Done PUTW Record Insertion ', 'TRN_OPENTASK');
		return true;
	}//end if binloc is not null
	else {
		//create the openTask record With CHKN Task Type
		var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('ERROR', 'Creating customrecord', 'TRN_OPENTASK');

		//populating the fields
		customrecord.setFieldValue('name', poValue);
		customrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		customrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);

		nlapiLogExecution('ERROR', 'custrecord_ebiz_sku_no tesing', 'ItemId');

		customrecord.setFieldValue('custrecord_act_qty', quan);
		customrecord.setFieldValue('custrecord_expe_qty', quan);
		customrecord.setFieldValue('custrecord_lpno', LP);
		customrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		customrecord.setFieldValue('custrecord_line_no', lineno);
		customrecord.setFieldValue('custrecord_packcode', PackCode);
		customrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		customrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		customrecord.setFieldValue('custrecord_tasktype', 1); //For Check-In (CHKN)
		customrecord.setFieldValue('custrecord_actbeginloc', docklocid);
		customrecord.setFieldValue('custrecord_actendloc', stagelocid);
		customrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);
		customrecord.setFieldValue('custrecord_ebiz_order_no', po);

		customrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		customrecord.setFieldValue('custrecordact_begin_date', ActualBeginDate);
		customrecord.setFieldValue('custrecord_act_end_date', ActualEndDate);
		//Adding fields to update time zones.
		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		//	customrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		customrecord.setFieldValue('custrecord_actualendtime', ActualEndTime);
		customrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		customrecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
		customrecord.setFieldValue('custrecord_current_date', DateStamp());
		customrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		customrecord.setFieldValue('custrecord_wms_status_flag', 1);

		// Added by Phani on 03-25-2011
		// Insert the value in the WMS Location
		customrecord.setFieldValue('custrecord_wms_location', WHLocation);//

		//Added for Item name and desc
		customrecord.setFieldValue('custrecord_sku', ItemId);
		customrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" || itemrectype == "assemblyitem" ||batchflag=='T')
		{

			customrecord.setFieldValue('custrecord_batch_no', BatchNo);
			customrecord.setFieldValue('custrecord_expirydate', ExpDate);
			customrecord.setFieldValue('custrecord_fifodate', FifoDate);
			customrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");
		}
		//cartLP
		customrecord.setFieldValue('custrecord_transport_lp', cartlp);

		var currentUserID = getCurrentUser();

		customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		customrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		customrecord.setFieldValue('custrecord_taskassignedto', currentUserID);

		nlapiLogExecution('ERROR', 'Submitting customrecord', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var reccheckid = nlapiSubmitRecord(customrecord);
		nlapiLogExecution('ERROR', 'Done CHKN Record Insertion :', 'Success');

		//create the openTask record With PUTW Task Type
		var putwrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('ERROR', 'Creating customrecord', 'TRN_OPENTASK');

		//populating the fields
		putwrecord.setFieldValue('name', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		putwrecord.setFieldValue('custrecord_expe_qty', quan);
		putwrecord.setFieldValue('custrecord_lpno', LP);
		putwrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		putwrecord.setFieldValue('custrecord_line_no', lineno);
		putwrecord.setFieldValue('custrecord_packcode', PackCode);
		putwrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		putwrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		putwrecord.setFieldValue('custrecord_tasktype', 2); //For Putaway (PUTW)
		putwrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);
		putwrecord.setFieldValue('custrecord_ebiz_order_no', po);
		putwrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);
		putwrecord.setFieldValue('custrecord_current_date', DateStamp());
		putwrecord.setFieldValue('custrecord_upd_date', DateStamp());
		putwrecord.setFieldValue('custrecord_wms_status_flag', 6);
		putwrecord.setFieldValue('custrecord_wms_location', WHLocation);//
		putwrecord.setFieldValue('custrecord_sku', ItemId);
		putwrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//Fields for Lot/Batch Sku's
		if (itemrectype == "lotnumberedinventoryitem" || itemrectype == "lotnumberedassemblyitem" ||itemrectype == "assemblyitem"  ||batchflag=='T')
		{
			putwrecord.setFieldValue('custrecord_batch_no', BatchNo);
			putwrecord.setFieldValue('custrecord_expirydate', ExpDate);
			putwrecord.setFieldValue('custrecord_fifodate', FifoDate);
			putwrecord.setFieldValue('custrecord_lotnowithquantity', BatchNo + "(" + quan + ")");

		}
		//cartLP
		putwrecord.setFieldValue('custrecord_transport_lp', cartlp);

		var currentUserID = getCurrentUser();

		putwrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		putwrecord.setFieldValue('custrecord_ebizuser', currentUserID);
		putwrecord.setFieldValue('custrecord_taskassignedto', currentUserID);
		if(putmethod!=null && putmethod!='')
			putwrecord.setFieldValue('custrecord_ebizmethod_no', putmethod);
		if(putrule!=null && putrule!='')
			putwrecord.setFieldValue('custrecord_ebizrule_no', putrule);

		nlapiLogExecution('ERROR', 'Submitting customrecord', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(putwrecord);
		/*try
		{
			MoveTaskRecord(reccheckid);
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR', 'Exception in  MoveTaskRecord');
		}*/
		nlapiLogExecution('ERROR', 'Done CHKN Record Insertion :', 'Success');
		return false;
	}
}
function getStagingLocation(WHLocation){
	var retInboundStagingLocn = "";

	/*
	 * To fetch inbound staging location,
	 *   Search customrecord_ebiznet_location for all location whose
	 *   location group id has location type as 'STAGE' and location group type as 'INBOUND' or 'BOTH'
	 */
	var stageFilters = new Array();
	// LOCATION TYPE = STAGE
	stageFilters.push(new nlobjSearchFilter('custrecord_grplocationtype', 'custrecord_inboundlocgroupid', 'is', '8'));

	// LOCATION GROUP TYPE = INBOUND or BOTH
	stageFilters.push(new nlobjSearchFilter('custrecord_locgrouptype', 'custrecord_inboundlocgroupid', 'anyof', ['1', '2']));
	var RoleLocation=getRoledBasedLocation();
	nlapiLogExecution('ERROR', 'RoleLocation', RoleLocation);
	if(RoleLocation != null && RoleLocation != '' && RoleLocation != 0)
	{
//		stageFilters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof', RoleLocation));
		if(WHLocation!=null && WHLocation!='')
		stageFilters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof', WHLocation));
	}
	else
	{
		if(WHLocation!=null && WHLocation!='')
		stageFilters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof', WHLocation));
	}
	// Results for inbound staging location
	var stageLocnResults = nlapiSearchRecord('customrecord_ebiznet_location', null, stageFilters);

	// This search could return more than one inbound staging location.  We are using the first one.
	if(stageLocnResults != null)
		retInboundStagingLocn = stageLocnResults[0].getId();

	return retInboundStagingLocn;
}
function getDockLocation(){
	var retDockLocation = "";

	/*
	 * To fetch dock location,
	 *   Search customrecord_ebiznet_location for all location whose
	 *   location group id has location type as 'DOCK' and location group type as 'INBOUND' or 'BOTH'
	 */
	var dockFilters = new Array();
	// LOCATION TYPE = DOCK
	dockFilters.push(new nlobjSearchFilter('custrecord_grplocationtype', 'custrecord_inboundlocgroupid', 'is', '3'));

	// LOCATION GROUP TYPE = INBOUND or BOTH
	dockFilters.push(new nlobjSearchFilter('custrecord_locgrouptype', 'custrecord_inboundlocgroupid', 'anyof', ['1', '2']));

	// Results for inbound staging location
	var dockLocationResults = nlapiSearchRecord('customrecord_ebiznet_location', null, dockFilters);

	// This search could return more than one inbound staging location.  We are using the first one.
	if(dockLocationResults != null)
		retDockLocation = dockLocationResults[0].getId();

	return retDockLocation;
}

function updateTrnOrderLine(poid,lineno,expQty)
{
	try{
		nlapiLogExecution('ERROR','poid',poid);
		nlapiLogExecution('ERROR','lineno',lineno);
		nlapiLogExecution('ERROR','expQty',expQty);
		var chkQty=0;
		var PwtQty=0;
		var RecId;	
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', poid));
		filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', lineno));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty');
		columns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_putgen_qty');

		var searchResults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, filters, columns);

		if(searchResults!=null&&searchResults!="")
		{
			chkQty=searchResults[0].getValue('custrecord_orderlinedetails_checkin_qty');
			PwtQty=searchResults[0].getValue('custrecord_orderlinedetails_putgen_qty');
			RecId=searchResults[0].getId();
			nlapiLogExecution('ERROR','chkQty',chkQty);
			nlapiLogExecution('ERROR','PwtQty',PwtQty);
			//Case  # 20125981?Start
			//case # 20148117
			chkQty=parseFloat(chkQty)-parseFloat(expQty);
			PwtQty=parseFloat(PwtQty)-parseFloat(expQty);
			nlapiLogExecution('ERROR','chkQty AFTER',chkQty);
			nlapiLogExecution('ERROR','PwtQty AFTER',PwtQty);
			var Fields=new Array();
			Fields[0]='custrecord_orderlinedetails_checkin_qty';
			Fields[1]='custrecord_orderlinedetails_putgen_qty';
			Fields[2]='custrecord_orderlinedetails_putexcep_qty';

			var Values=new Array();
			Values[0]=parseFloat(chkQty).toFixed(5);
			Values[1]=parseFloat(PwtQty).toFixed(5);
			Values[2]=parseFloat(expQty).toFixed(5);
			//Case  # 20125981�End
			var ID=nlapiSubmitField('customrecord_ebiznet_order_line_details', RecId, Fields, Values);
			nlapiLogExecution('ERROR','Trn Order Line RecID',ID);
		}
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in UpdateTrnOrderLine function',exp);
	}
}

function CheckForLPExist(getsysItemLP,WmsLocation)
{
	try {
		nlapiLogExecution('ERROR', 'INTO MASTER LP INSERTION');
		var filtersmlp = new Array();
		filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', getsysItemLP);

		var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

		if (SrchRecord != null && SrchRecord.length > 0) {
			nlapiLogExecution('ERROR', 'LP FOUND');

			getsysItemLP = GetMaxLPNo(1, 1,WmsLocation);
			nlapiLogExecution('ERROR', 'getsysItemLP LP now added iis', getsysItemLP);	
			CheckForLPExist(getsysItemLP,WmsLocation);
		}
		else {
			nlapiLogExecution('ERROR', 'LP NOT FOUND');
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
			customrecord.setFieldValue('name', getsysItemLP);
			customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', getsysItemLP);
			var rec = nlapiSubmitRecord(customrecord, false, true);
		}
	} 
	catch (e) {
		nlapiLogExecution('ERROR', 'Failed to Update/Insert into Master LP Record');
	}
	return getsysItemLP;
}

function eBiz_RF_GetItemCubeForItem(itemID){
	nlapiLogExecution('ERROR', 'eBiz_RF_GetItemCubeForItem', itemID);
	var ItemInfo=new Array();
	var itemCube = 0;
	var BaseUomQty=0;
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemID);
	filters[1] = new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
	columns[1] = new nlobjSearchColumn('custrecord_ebizqty');

	var itemDimSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

	if(itemDimSearchResults != null && itemDimSearchResults.length > 0){
		itemCube = itemDimSearchResults[0].getValue('custrecord_ebizcube');
		//code added on 13 feb 2012 by suman
		//To get the baseuom qty .
		BaseUomQty=itemDimSearchResults[0].getValue('custrecord_ebizqty');
		//end of code as of 13 feb 2012.
	}
	ItemInfo.push(itemCube);
	ItemInfo.push(BaseUomQty);
	nlapiLogExecution('ERROR', 'Retrieved Item Cube', 'Item Cube = ' + itemCube);
	nlapiLogExecution('ERROR', 'Retrieved BaseUomQty', 'BaseUomQty = ' + BaseUomQty);
	nlapiLogExecution('ERROR', 'eBiz_RF_GetItemCubeForItem', 'End');

	return ItemInfo;
}

function updateChknTaskQty(getLPNo,QuantityEntered)
{
	try
	{
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag', null, 'anyof', 1);//1 STATUS.INBOUND.CHECK_IN 
		if(getLPNo!=null&&getLPNo!='')
			filters[1] = new nlobjSearchFilter('custrecord_ebiztask_lpno', null, 'is', getLPNo);

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filters, null);
		if(searchresults!=null&&searchresults!="")
		{
			var RecId=searchresults[0].getId();
			var Id=nlapiSubmitField('customrecord_ebiznet_trn_ebiztask', RecId, 'custrecord_ebiztask_act_qty', QuantityEntered);
			nlapiLogExecution('ERROR','Chkn rec Id which is updated with new exp qty',Id);
		}
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in updateChknTaskQty',exp);	
	}

}

//case# 20148770 ends
// case # 201411654

function getAlleBizItemDimensions(itemarray)
{
	nlapiLogExecution('ERROR','Into  getAlleBizItemDimensions');

	var searchRec = new Array();

	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', itemarray));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim') ;
	column[1] = new nlobjSearchColumn('custrecord_ebizqty') ;
	column[2] = new nlobjSearchColumn('custrecord_ebizbaseuom') ;
	column[3] = new nlobjSearchColumn('custrecord_ebiznsuom') ;
	column[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim').setSort(true);
	column[5] = new nlobjSearchColumn('custrecord_ebizitemdims') ;
	column[6] = new nlobjSearchColumn('custrecord_ebizpackcodeskudim') ;

	searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);

	nlapiLogExecution('ERROR','Out of  getAlleBizItemDimensions');

	return searchRec;

}