/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_BuildshipUnitsQb_SL.js,v $
 *     	   $Revision: 1.8.2.3.4.1.4.4.4.1 $
 *     	   $Date: 2015/11/14 09:46:39 $
 *     	   $Author: sponnaganti $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_133 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_BuildshipUnitsQb_SL.js,v $
 * Revision 1.8.2.3.4.1.4.4.4.1  2015/11/14 09:46:39  sponnaganti
 * case# 201415644
 * 2015.2 issue fix
 *
 * Revision 1.8.2.3.4.1.4.4  2014/05/28 15:37:23  skreddy
 * case # 20148584
 * Standard Bundle issue fix
 *
 * Revision 1.8.2.3.4.1.4.3  2013/04/30 18:43:45  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.8.2.3.4.1.4.2  2013/04/09 13:21:36  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related to display results  with Container Lp
 *
 * Revision 1.8.2.3.4.1.4.1  2013/03/19 11:46:05  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.8.2.3.4.1  2012/11/01 14:55:02  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.8.2.3  2012/08/31 11:48:33  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Build ship units
 *
 * Revision 1.8.2.2  2012/08/10 17:47:52  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.8.2.1  2012/05/25 09:22:59  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.8  2012/01/17 23:41:22  snimmakayala
 * CASE201112/CR201113/LOG201121
 * BuildShipUnit Issue fixes
 *
 * Revision 1.7  2011/10/05 09:35:30  gkalla
 * CASE201112/CR201113/LOG201121
 * For Lot# entry updation
 *
 * Revision 1.6  2011/10/04 07:14:00  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.5  2011/09/23 13:31:43  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.4  2011/09/13 08:46:45  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code changes to display Pack Completed orders/waves also.
 *
 * Revision 1.3  2011/08/25 15:01:14  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.2  2011/07/21 06:59:59  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/

/**
 * This is the main function
 * 
 * @param request
 * @param response
 *  
 */

function BuildShipUnitsQbSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Build Ship Units');
		var vQbWave = "";
		var context = nlapiGetContext();
		var usageRemaining = context.getRemainingUsage();
		nlapiLogExecution('ERROR', 'usageRemaining',usageRemaining);
		var vRoleLocation = getRoledBasedLocationNew();
		nlapiLogExecution('ERROR', 'vRoleLocation',vRoleLocation);

		// To fill Wave no ddl
		var WaveField = form.addField('custpage_wave', 'select', 'Wave');
		WaveField.setLayoutType('startrow', 'none');
		WaveField.addSelectOption("", "");

		addWaveNo(form,WaveField,-1,vRoleLocation);		

		usageRemaining = context.getRemainingUsage();
		nlapiLogExecution('ERROR', 'usageRemaining',usageRemaining);

		// To fill Customer ddl
		var CustomerField = form.addField('custpage_customer', 'select', 'Customer', 'customer');
		CustomerField.setLayoutType('startrow', 'none');

		// To fill Orders in ddl
		var OrderField = form.addField('custpage_ordno', 'select', 'Order No');
		OrderField.setLayoutType('startrow', 'none');
		OrderField.addSelectOption("", "");
		addOrderNo(form,OrderField,-1,vRoleLocation);

		//To add Ship LP ddl
		//var ShipField = form.addField('custpage_shiplp', 'select', 'Ship LP','customrecord_ebiznet_master_lp');
		var ContainerField = form.addField('custpage_shiplp', 'select', 'Container LP#');
		ContainerField.setLayoutType('startrow', 'none');
		ContainerField.addSelectOption("", "");

		addContainerLP(form,ContainerField,-1);
		form.addSubmitButton('Display');

		response.writePage(form);
	}
	else //this is the POST block
	{
		var vWave = request.getParameter('custpage_wave');	 
		var vOrdNo = request.getParameter('custpage_ordno'); 
		var vConsignee = request.getParameter('custpage_consignee');
		var vShipNo = request.getParameter('custpage_shiplp');

		var SOarray = new Array();
		SOarray ["custpage_wave"] = vWave;
		SOarray ["custpage_ordno"] = vOrdNo;
		SOarray ["custpage_customer"] = vConsignee;
		SOarray ["custpage_shiplp"] = vShipNo;
		response.sendRedirect('SUITELET', 'customscript_buildshipunits', 'customdeploy_buildshipunits', false, SOarray );
	}
}

/**
 * Function to fill all Wave numbers in the dropdownlist and to add dropdownlist to Form
 * 
 * @param form 
 *
 */
function addWaveNo(form,WaveField,maxno,vRoleLocation){

	var filtersso = new Array();
	filtersso.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3')); //PICK Task
	filtersso.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['28'])); //status 'C'
	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'noneof','@NONE@'));		
	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isnotempty')); //PICK 
	if(parseFloat(maxno)!=-1)
	{
		nlapiLogExecution('ERROR', 'maxno', maxno);
		filtersso.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno)));
	}
	nlapiLogExecution('ERROR', 'vRoleLocation', vRoleLocation);
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		filtersso.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vRoleLocation)); 
	}
	var Columns = new Array();
	Columns[0] = new nlobjSearchColumn('custrecord_ebiz_wave_no');	 
	Columns[1] = new nlobjSearchColumn('id');	
	Columns[1].setSort(true);


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso, Columns);
	if (searchresults != null) {	
		for (var i = 0; i < searchresults.length; i++) {
			if(searchresults[i].getValue('custrecord_ebiz_wave_no') != null  && searchresults[i].getValue('custrecord_ebiz_wave_no') != "")
			{
				var res = form.getField('custpage_wave').getSelectOptions(searchresults[i].getValue('custrecord_ebiz_wave_no'), 'is');
				if (res != null) {
					if (res.length > 0) {
						continue;
					}
				}

				WaveField.addSelectOption(searchresults[i].getValue('custrecord_ebiz_wave_no'), searchresults[i].getValue('custrecord_ebiz_wave_no'));
			}
		}

		if(searchresults.length>=1000)
		{
			nlapiLogExecution('ERROR', 'searchresults for Wave', searchresults.length);
			var maxno=searchresults[searchresults.length-1].getValue('id');
			addWaveNo(form,WaveField,maxno,vRoleLocation);	
		}
	}
	//WaveField.setMandatory(true);
}
/**
 * Function to fill all Orders in the dropdownlist and to add dropdownlist to Form
 * 
 * @param form 
 *
 */
function addOrderNo(form,OrderField,maxno,vRoleLocation){
	

	var filtersso = new Array();
	filtersso.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3')); //PICK Task
	filtersso.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['28'])); //status 'C'
	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'noneof','@NONE@'));		
	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isnotempty')); //PICK 
	if(parseFloat(maxno)!=-1)
	{
		nlapiLogExecution('ERROR', 'maxno', maxno);
		filtersso.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno)));
	}
	nlapiLogExecution('ERROR', 'vRoleLocation', vRoleLocation);
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		filtersso[3] = new nlobjSearchFilter('custrecord_wms_location', null,  'anyof', vRoleLocation); 

	}
	var Columns = new Array();
	Columns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');	 
	Columns[1] = new nlobjSearchColumn('id');
	Columns[0].setSort(true);

	var searchresultsOrderNo = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso, Columns);
	if (searchresultsOrderNo != null) {	
		for (var i = 0; i < searchresultsOrderNo.length; i++) {
			if(searchresultsOrderNo[i].getValue('custrecord_ebiz_order_no')!= null && searchresultsOrderNo[i].getValue('custrecord_ebiz_order_no') != "")
			{
				var res = form.getField('custpage_ordno').getSelectOptions(searchresultsOrderNo[i].getText('custrecord_ebiz_order_no'), 'is');
				if (res != null) {
					if (res.length > 0) {
						continue;
					}
				}

				OrderField.addSelectOption(searchresultsOrderNo[i].getValue('custrecord_ebiz_order_no'), searchresultsOrderNo[i].getText('custrecord_ebiz_order_no'));
			}
		}
		if(searchresultsOrderNo.length>=1000)
		{
			nlapiLogExecution('ERROR', 'searchresults for Order', searchresultsOrderNo.length);
			var maxno=searchresultsOrderNo[searchresultsOrderNo.length-1].getValue('id');
			addOrderNo(form,OrderField,maxno,vRoleLocation);	
		}
	}
}



function addContainerLP(form,ContainerField,maxno){

	nlapiLogExecution('ERROR', 'addContainerLP', 'addContainerLP');
	var filtersso = new Array();
	filtersso.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3')); //PICK Task
	filtersso.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['28'])); //status 'C'
	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'noneof','@NONE@'));		
	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isnotempty')); //PICK 
	if(parseFloat(maxno)!=-1)
	{
		nlapiLogExecution('ERROR', 'maxno', maxno);
		filtersso.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno)));
	}
	var Columns = new Array();
	Columns[0] = new nlobjSearchColumn('custrecord_container_lp_no');	 
	Columns[1] = new nlobjSearchColumn('id');	
	Columns[1].setSort(true);


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso, Columns);
	if (searchresults != null) {	
		for (var i = 0; i < searchresults.length; i++) {
			if(searchresults[i].getValue('custrecord_container_lp_no') != null  && searchresults[i].getValue('custrecord_container_lp_no') != "")
			{
				var res = form.getField('custpage_shiplp').getSelectOptions(searchresults[i].getValue('custrecord_container_lp_no'), 'is');
				if (res != null) {
					if (res.length > 0) {
						continue;
					}
				}

				ContainerField.addSelectOption(searchresults[i].getValue('custrecord_container_lp_no'), searchresults[i].getValue('custrecord_container_lp_no'));
			}
		}

		if(searchresults.length>=1000)
		{
			nlapiLogExecution('ERROR', 'searchresults for Wave', searchresults.length);
			var maxno=searchresults[searchresults.length-1].getValue('id');
			addContainerLP(form,ContainerField,maxno);	
		}
	}
	//WaveField.setMandatory(true);
}