/***************************************************************************
eBizNET Solutions
 ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Suitelet/Attic/ebiz_po_receipt_report_pdf_mhp.js,v $
 *     	   $Revision: 1.1.2.4 $
 *     	   $Date: 2014/01/30 15:38:22 $
 *     	   $Author: skavuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 *$Log: ebiz_po_receipt_report_pdf_mhp.js,v $
 *Revision 1.1.2.4  2014/01/30 15:38:22  skavuri
 *case# 20126935 Carrier Name is displayed, po qty displayed
 *
 *Revision 1.1.2.3  2014/01/22 15:55:31  nneelam
 *case#  20126912
 *MHP PO Receipt Report
 *
 *Revision 1.1.2.2  2014/01/21 15:32:31  nneelam
 *case#  20126814
 *MHP PO Receipt Report
 *
 *Revision 1.1.2.1  2014/01/20 16:17:30  nneelam
 *case#  20126814
 *MHP PO Receipt Report
 *
 *Revision 1.1.2.1  2014/01/20 14:14:52  nneelam
 *case#  20126814
 *MHP PO Receipt Report
 *
 */
function POReportPDF(request, response) {

	if (request.getMethod() == 'GET') {

		var form = nlapiCreateForm('PO Receipt Report');
		
		var getpo = request.getParameter('custparam_ebiz_po_no');
		nlapiLogExecution('ERROR','getpo in pdf file',getpo);

		var filefound = nlapiLoadFile('Images/LOGOCOMP.jpg');
		if (filefound) {
			nlapiLogExecution('ERROR', 'Event', 'file;' + filefound.getId());
			var imageurl = filefound.getURL();
			// finalimageurl = url + imageurl;//+';';
			finalimageurl = imageurl;// +';';
			finalimageurl = finalimageurl.replace(/&/g, "&amp;");

		} else {
			nlapiLogExecution('ERROR', 'Event', 'No file;');
		}
		
var item,poline,poqty,potranid,podate;
		
		var form,lineno,sku,upccode,lotno,expdate,cases,pallets,unitspercase,recvdate,closedtask_qty=0,enterby,vendorname='';
		var index=0;
		//case# 20126935 starts
		var POorderqty=0;
		//case# 20126935 end
		var uom_type,uomqty;
		

// Search with po number starts
		var pointid="";
			if(getpo!=null&&getpo!="")
				pointid=GetPOInternalId(getpo);
		nlapiLogExecution('ERROR','pointid',getpo);
		
		
		// open task
		var otfilters=new Array();
		
		if(pointid !=null && pointid !='')
		otfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no',null,'is',pointid));
		otfilters.push(new nlobjSearchFilter('mainline',"custrecord_ebiz_order_no",'is',"T"));
		otfilters.push(new nlobjSearchFilter('custrecord_tasktype',null,'anyof',"2"));
		otfilters.push(new nlobjSearchFilter('custrecord_wms_status_flag',null,'anyof',"3"));
		
		var otcolumns=CreateColumnsot();
		otsearchrecords=new nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,otfilters,otcolumns);
		nlapiLogExecution('ERROR','Open task records length',otsearchrecords.length);
		// end of open task
		
		// closed task
		var filters=new Array();
		
		if(pointid !=null && pointid !='')
		filters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no',null,'is',pointid));
		filters.push(new nlobjSearchFilter('mainline',"custrecord_ebiztask_ebiz_order_no",'is',"T"));
		filters.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype',null,'anyof',"2"));
		filters.push(new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag',null,'anyof',"3"));
		
		var columns=CreateColumnsct();
		searchrecords=new nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask',null,filters,columns);
		nlapiLogExecution('ERROR','Closed task records length',searchrecords.length);
		// end of closed task
		

		
		var porecArray=new Array();
		if(otsearchrecords!=null&&otsearchrecords!='')
		{
			for(var i=0;i<otsearchrecords.length;i++)
			{
				porecArray.push(otsearchrecords[i].getValue("custrecord_ebiz_cntrl_no",null,"group"))
				
			}
		}

		if(searchrecords!=null&&searchrecords!='')
		{
			for(var i=0;i<searchrecords.length;i++)
			{
				porecArray.push(searchrecords[i].getValue("custrecord_ebiztask_ebiz_cntrl_no",null,"group"))
				
			}
		}
		var result="";
		if(porecArray!=null&&porecArray!='')
		result=GetPuracahseOrderValues(porecArray);
		nlapiLogExecution('ERROR','result',result.length);
		
		// for pdf report values
		var vendoraddres='',carrier='',fob='',podate='',expshipdate='',exparrdate='';
		if(result!=null && result!='')
			{
			for(var i=0;i<result.length;i++)
				{
				//carrier=result[i].getValue("custbody_poshippingitem");
				//case# 20126935 starts
				carrier=result[i].getText("custbody_poshippingitem");
				//case# 20126935 end
				nlapiLogExecution('ERROR','carrier',carrier);
				podate=result[i].getValue("trandate");
				expshipdate=result[i].getValue("custbody_nswmspoexpshipdate");
				exparrdate=result[i].getValue("custbody_nswmspoarrdate");
				 //vendoraddres=result[i].getValue('billaddress','entity');
				 //vendoraddres=result[i].getValue('billaddress');
				//vendoraddres=result[i].getText('entity');
				 //nlapiLogExecution('ERROR','vendoraddres',vendoraddres);
				 
				}
			
			}
		
		//for vendor address begin
		
		var polinefilters = new Array();
		polinefilters[0] = new nlobjSearchFilter('internalid', null, 'anyof', pointid);
		polinefilters[1] = new nlobjSearchFilter('mainline', null, 'is','T');			


		var polinecolumns = new Array();
		polinecolumns[0] = new nlobjSearchColumn('entity');
		
		var posearchresults = nlapiSearchRecord('purchaseorder', null, polinefilters, polinecolumns);
		nlapiLogExecution('ERROR','posearchresults is',posearchresults.length);
		if(posearchresults!=''&&posearchresults!='')
			var vendorname1=posearchresults[0].getValue('entity');
		
		nlapiLogExecution('ERROR','vendorname1 is',vendorname1);
		
		var entityrecord = nlapiLoadRecord('vendor', vendorname1);
		nlapiLogExecution('ERROR','entityrecord is',entityrecord.length);
		var custaddr1='',custaddr2='',custaddresee='',custcity='',custstate='';
		if(entityrecord!=''&&entityrecord!=='')
			{
		custaddr1 = entityrecord.getFieldValue('shipaddr1');
		nlapiLogExecution('ERROR','custaddr1',custaddr1);
		 custaddr2 = entityrecord.getFieldValue('shipaddr2');

		 custaddresee = entityrecord.getFieldValue('shipaddressee');

		custcity = entityrecord.getFieldValue('shipcity');

	     custstate = entityrecord.getFieldValue('shipstate');
			}
	    //for vendor address end
	      
		// for header pdf report
		
		
		var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<head><macrolist><macro id='myfooter'></macro></macrolist></head><body  font-size=\"7\"  size=\"A4-landscape\"    padding-top=\" 0mm\"   footer='myfooter' footer-height='20mm'>\n";

		
		var strxml = "<table width='100%' >";
	
	

	strxml += "<tr ><td valign='middle' align='left'><img src='"
			+ finalimageurl
			+ "'></img></td><td valign='middle' align='left'  style='font-size:xx-large;font-weight:bold;'>";
	strxml += "PO Receipt Report ";
	strxml += "</td><td align='right'>&nbsp;</td></tr>";
	
	strxml +="<tr><td> Maximum Human Performance</td><td></td><td></td></tr>";
	strxml +="<tr><td> 21 Dwight Place</td><td></td><td></td></tr>";
	strxml +="<tr><td> Fairfield NJ</td><td></td><td></td></tr>";
	strxml +="<tr><td> United States of America</td><td></td><td align='left' valign='left' style='font-size:small;font-weight:bold;'><p align='left'>PO # &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + getpo + "</p></td></tr>";
	strxml +="<tr><td></td></tr></table>";
	
	strxml +="-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------";
	
	
	//for vendor and remaing fields 
	strxml += "<table  width='100%'>";
	strxml +="<tr><td><table><tr><td style='font-size:medium;font-weight:bold;'>VENDOR :</td><td style='font-size:small;'>";
	strxml +=custaddr1;
	strxml +="</td></tr>";
	if(custaddr2!=''&&custaddr2!=null)
		{
	strxml +="<tr><td></td><td style='font-size:small;'>";
	strxml +=custaddr2;
	strxml +="</td></tr>";
		}
	if(custaddresee!=''&&custaddresee!=null)
	{
	strxml +="<tr><td></td><td style='font-size:small;'>";
	strxml +=custaddresee;
	strxml +="</td></tr>";
	}
	if(custcity!=''&&custcity!=null)
	{
	strxml +="<tr><td></td><td style='font-size:small;'>";
	strxml +=custcity;
	strxml +="</td></tr>";
	}
	strxml +="<tr><td></td><td style='font-size:small;'>";
	strxml +=custstate;
	strxml +="</td></tr></table></td><td></td><td align='left'><table><tr><td style='font-size:small;font-weight:bold;'>CARRIER:</td><td style='font-size:small;'>";
	strxml +=carrier;
	strxml +="</td></tr><tr><td style='font-size:small;font-weight:bold;'>FOB:</td><td style='font-size:small;'>";
	strxml +=fob;
	strxml +="</td></tr><tr><td style='font-size:small;font-weight:bold;'>PO DATE:</td><td style='font-size:small;'>";
	strxml +=podate;
	strxml +="</td></tr><tr><td style='font-size:small;font-weight:bold;'>EXP SHIP DATE:</td><td style='font-size:small;'>";
	strxml +=expshipdate;
	strxml +="</td></tr><tr><td style='font-size:small;font-weight:bold;'>EXP ARR DATE:</td><td style='font-size:small;'>";
	strxml +=exparrdate;
	strxml +="</td></tr></table></td></tr></table>";
	
	
	//for sublist headline
	strxml += "<table  width='100%'>";
	strxml += "<tr style=\"font-weight:bold;background-color:gray;color:white;\">";

	strxml += "<td width='1%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";
	strxml += "Line";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

	strxml += "<td width='25%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";
	strxml += "SKU";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

	strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";
	strxml += " MFG UPC";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

	strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";
	strxml += "LOT#";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

	strxml += "<td width='12%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";
	strxml += "EXP DATE";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

	strxml += "<td width='1%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";
	strxml += "#PALLETS";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

	strxml += "<td width='1%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";// uncommented
	strxml += "#CASES";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

	strxml += "<td width='12%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";
	strxml += "UNITS PER CASE";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

	strxml += "<td width='10%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";
	strxml += "RECEIPT DATE";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

	strxml += "<td width='12%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";
	strxml += "TOTAL RECEIVED";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

	strxml += "<td width='7%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";// uncommented
	strxml += "PO QTY";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

	strxml += "<td width='10%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";
	strxml += "ENTERED BY";
	strxml += "&nbsp;</td></tr>";
		
		
		// ending header pdf report
		
		// for item dimensions getting sku items into an array,for compare the
		// opentask,closetask iems
		
		var itemdimensionArray=new Array();
		if(otsearchrecords!=null&&otsearchrecords!='')
		{
			for(var i=0;i<otsearchrecords.length;i++)
			{
			
			itemdimensionArray.push(otsearchrecords[i].getValue("custrecord_sku",null,"group"))
			}
		}

		if(searchrecords!=null&&searchrecords!='')
		{
			for(var i=0;i<searchrecords.length;i++)
			{
			
			itemdimensionArray.push(searchrecords[i].getValue("custrecord_ebiztask_sku",null,"group"))
			}
		}
		
		var itemdimresult="";
		if(itemdimensionArray!=''&&itemdimensionArray!=null)
		var itemdimresult=GetItemdimensionvalues(itemdimensionArray); // to
																		// retive
																		// all
																		// dimension
																		// from
																		// itemdimension
																		// record
		nlapiLogExecution('ERROR','itemdimresult',itemdimresult.length);
		
		// opentask results start
		if(otsearchrecords!=null&&otsearchrecords!='')
		{
			for(var i=0;i<otsearchrecords.length;i++)
			{
				potranid=otsearchrecords[i].getValue("tranid","custrecord_ebiz_order_no","max");
				
				lineno=otsearchrecords[i].getValue("custrecord_line_no",null,"group");
				
				sku=otsearchrecords[i].getText("custrecord_sku",null,"group");
				
				var skuid=otsearchrecords[i].getValue("custrecord_sku",null,"group");
		
				upccode=otsearchrecords[i].getValue("upccode","custrecord_sku","max");
				
				lotno=otsearchrecords[i].getValue("custrecord_batch_no",null,"group");
				
				expdate=otsearchrecords[i].getValue('custrecord_expirydate',null,"max");
				
				recvdate=otsearchrecords[i].getValue('custrecord_act_end_date',null,"max");

				closedtask_qty=otsearchrecords[i].getValue('custrecord_act_qty',null,"sum");
				nlapiLogExecution('ERROR','Open task QTY ',closedtask_qty);

				enterby=otsearchrecords[i].getText('custrecord_taskassignedto',null,"group");
				
				vendorname=otsearchrecords[i].getValue("entity","custrecord_ebiz_order_no","max");//for vendor name
				// for pdf fields
				
				
				// pallets,cases,unitpercase
				nlapiLogExecution('ERROR','itemdimresult.length ',itemdimresult.length);
				var noofeaches,noofcases,noofpallets,veachuomqty,vcaseuomqty,vpalletuomqty,vremQty;
				noofpallets = 0;
				noofcases = 0;
				noofeaches = 0;
				vremQty = 0;
				for(var item=0;item<itemdimresult.length;item++)
					{
					var vitems=itemdimresult[item].getText('custrecord_ebizitemdims');
					nlapiLogExecution('ERROR','vitems',vitems);
					if(sku==vitems)
						{
						var vuom=itemdimresult[item].getValue('custrecord_ebizuomskudim');
						
						var vqty=itemdimresult[item].getValue('custrecord_ebizqty');
						nlapiLogExecution('ERROR','vqty',vqty);
						

						/*// var eBizItemDims = geteBizItemDimensions1(skuid);
				var eBizItemDims = GetItemdimensionvalues(sku);
				nlapiLogExecution('ERROR','eBizItemDims',eBizItemDims);
				if(eBizItemDims != null && eBizItemDims != '')
				{
					for(var i1= 0;i1< eBizItemDims.length;i1++)
					{
						var vUomlevel = eBizItemDims[i1].getValue('custrecord_ebizuomlevelskudim');
						if(vUomlevel=='3')
						{
							vpalletuomqty = eBizItemDims[i1].getValue('custrecord_ebizqty');
							//nlapiLogExecution('ERROR','vpalletuomqty',vpalletuomqty);
						}
						else
							if(vUomlevel=='2')
							{
								vcaseuomqty = eBizItemDims[i1].getValue('custrecord_ebizqty');
								//nlapiLogExecution('ERROR','vcaseuomqty',vcaseuomqty);
							}
							else
								if(vUomlevel=='1')
								{
									veachuomqty = eBizItemDims[i1].getValue('custrecord_ebizqty');
									//nlapiLogExecution('ERROR','veachuomqty',veachuomqty);

								}
					}
				}*/

						var vUomlevel = itemdimresult[item].getValue('custrecord_ebizuomlevelskudim');
						if(vUomlevel=='3')
						{
							vpalletuomqty = itemdimresult[item].getValue('custrecord_ebizqty'); 
							//nlapiLogExecution('ERROR','vpalletuomqty',vpalletuomqty);
						}
						else
							if(vUomlevel=='2')
							{
								vcaseuomqty = itemdimresult[item].getValue('custrecord_ebizqty'); 

								//nlapiLogExecution('ERROR','vcaseuomqty',vcaseuomqty);
							}
							else
								if(vUomlevel=='1')
								{
									veachuomqty = itemdimresult[item].getValue('custrecord_ebizqty');

									//nlapiLogExecution('ERROR','veachuomqty',veachuomqty);

								}
					}
				}
						vremQty=closedtask_qty;
						
						if(vremQty>0 && parseFloat(vpalletuomqty)>0){
							noofpallets = getSKUDimCount(vremQty, vpalletuomqty);
							vremQty = parseFloat(vremQty) - (noofpallets * vpalletuomqty);
							nlapiLogExecution('ERROR','vremQty pallet',vremQty);
						}

						if(vremQty>0 && parseFloat(vcaseuomqty)>0){
							noofcases = getSKUDimCount(vremQty, vcaseuomqty);
							vremQty = parseFloat(vremQty) - (noofcases * vcaseuomqty);									 
							nlapiLogExecution('ERROR','vremQty case',vremQty);
						}

						if(vremQty>0 && parseFloat(veachuomqty)>0){
							noofeaches = getSKUDimCount(vremQty, veachuomqty);
							vremQty = parseFloat(vremQty) - (noofeaches * veachuomqty);
							nlapiLogExecution('ERROR','vremQty each',vremQty);

						}

				for(var s=0;s<result.length;s++)
				{
					var vtranid=result[s].getValue("tranid");
					var vline=result[s].getValue("line");

					if((vtranid==potranid)&&(vline==lineno))
					POorderqty=result[s].getValue("quantity");
					nlapiLogExecution('ERROR','POorderqty',POorderqty);	
				}
				index=i+1;
				// OPEN TASK VALUES BINDING TO PDF
				
				strxml += "<tr><td width='1%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += lineno;
				strxml += "</td>";
				
				strxml += "<td width='25%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += sku;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += upccode;
				strxml += "</td>";
				
				
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += lotno;
				strxml += "</td>";
				
				strxml += "<td width='12%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += expdate;
				strxml += "</td>";
				
				strxml += "<td width='1%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += noofpallets;
				strxml += "</td>";
				
				strxml += "<td width='1%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += noofcases;
				strxml += "</td>";
				
				strxml += "<td width='12%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += noofeaches;
				strxml += "</td>";
				
				strxml += "<td width='10%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += recvdate;
				strxml += "</td>";
				
				strxml += "<td width='12%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += closedtask_qty;
				strxml += "</td>";
				
				
				
				strxml += "<td width='7%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += POorderqty;
				strxml += "</td>";
				
				strxml += "<td width='10%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += enterby;
				strxml += "</td></tr>";
				
				// BindSublistValues(form,lineno,sku,upccode,lotno,expdate,noofpallets,noofcases,noofeaches,recvdate,closedtask_qty,POorderqty,enterby,index);

			
		}
		}
		// closed task results start
		if(searchrecords!=null&&searchrecords!='')
		{
		
		for(var i=index;i<searchrecords.length;i++)
		{
			potranid=searchrecords[i].getValue("tranid","custrecord_ebiztask_ebiz_order_no","max");
			
			lineno=searchrecords[i].getValue("custrecord_ebiztask_line_no",null,"group");
			
			sku=searchrecords[i].getText("custrecord_ebiztask_sku",null,"group");
			
			var skuid=searchrecords[i].getValue("custrecord_sku",null,"group");
			
			upccode=searchrecords[i].getValue("upccode","custrecord_ebiztask_sku","max");
			
			lotno=searchrecords[i].getValue("custrecord_ebiztask_batch_no",null,"group");
			
			expdate=searchrecords[i].getValue('custrecord_ebiztask_expirydate',null,"max");
			
			recvdate=searchrecords[i].getValue('custrecord_ebiztask_act_end_date',null,"max");

			closedtask_qty=searchrecords[i].getValue('custrecord_ebiztask_act_qty',null,"sum");
			nlapiLogExecution('ERROR','Closed task QTY ',closedtask_qty);
			
			enterby=searchrecords[i].getText('custrecord_ebiztask_taskassgndid',null,"group");
			vendorname=searchrecords[i].getValue("entity","custrecord_ebiztask_ebiz_order_no","max");//for vendor name
			nlapiLogExecution('ERROR','vendorname ',vendorname);
			// pallets,cases,unitpercase
			nlapiLogExecution('ERROR','itemdimresult.length ',itemdimresult.length);
			var noofeaches,noofcases,noofpallets,veachuomqty,vcaseuomqty,vpalletuomqty,vremQty;
			noofpallets = 0;
			noofcases = 0;
			noofeaches = 0;
			vremQty = 0;
			for(var item=0;item<itemdimresult.length;item++)
				{
				var vitems=itemdimresult[item].getText('custrecord_ebizitemdims');
				nlapiLogExecution('ERROR','vitems inclosed',vitems);
				if(sku==vitems)
				{
					var vuom=itemdimresult[item].getValue('custrecord_ebizuomskudim');

					var vqty=itemdimresult[item].getValue('custrecord_ebizqty');
					nlapiLogExecution('ERROR','vqty',vqty);
					

					/*// var eBizItemDims = geteBizItemDimensions1(skuid);
			var eBizItemDims = GetItemdimensionvalues(sku);
			nlapiLogExecution('ERROR','eBizItemDims',eBizItemDims);
			if(eBizItemDims != null && eBizItemDims != '')
			{
				for(var i1= 0;i1< eBizItemDims.length;i1++)
				{
					var vUomlevel = eBizItemDims[i1].getValue('custrecord_ebizuomlevelskudim');
					if(vUomlevel=='3')
					{
						vpalletuomqty = eBizItemDims[i1].getValue('custrecord_ebizqty');
						//nlapiLogExecution('ERROR','vpalletuomqty',vpalletuomqty);
					}
					else
						if(vUomlevel=='2')
						{
							vcaseuomqty = eBizItemDims[i1].getValue('custrecord_ebizqty');
							//nlapiLogExecution('ERROR','vcaseuomqty',vcaseuomqty);
						}
						else
							if(vUomlevel=='1')
							{
								veachuomqty = eBizItemDims[i1].getValue('custrecord_ebizqty');
								//nlapiLogExecution('ERROR','veachuomqty',veachuomqty);

							}
				}
			}*/

					var vUomlevel = itemdimresult[item].getValue('custrecord_ebizuomlevelskudim');
					nlapiLogExecution('ERROR','vUomlevel',vUomlevel);
					if(vUomlevel=='3')
					{
						vpalletuomqty = itemdimresult[item].getValue('custrecord_ebizqty'); 
						nlapiLogExecution('ERROR','vpalletuomqty',vpalletuomqty);
					}
					else
						if(vUomlevel=='2')
						{
							vcaseuomqty = itemdimresult[item].getValue('custrecord_ebizqty'); 

							//nlapiLogExecution('ERROR','vcaseuomqty',vcaseuomqty);
						}
						else
							if(vUomlevel=='1')
							{
								veachuomqty = itemdimresult[item].getValue('custrecord_ebizqty');

								//nlapiLogExecution('ERROR','veachuomqty',veachuomqty);

							}
				}
			}
			nlapiLogExecution('ERROR','closedtask_qty in pdf',closedtask_qty);
					vremQty=closedtask_qty;

					if(vremQty>0 && parseFloat(vpalletuomqty)>0){
						noofpallets = getSKUDimCount(vremQty, vpalletuomqty);
						vremQty = parseFloat(vremQty) - (noofpallets * vpalletuomqty);
						nlapiLogExecution('ERROR','vremQty pallet',vremQty);
					}

				if(vremQty>0 && parseFloat(vcaseuomqty)>0){
					noofcases = getSKUDimCount(vremQty, vcaseuomqty);
					vremQty = parseFloat(vremQty) - (noofcases * vcaseuomqty);									 
					nlapiLogExecution('ERROR','vremQty case',vremQty);
				}

				if(vremQty>0 && parseFloat(veachuomqty)>0){
					noofeaches = getSKUDimCount(vremQty, veachuomqty);
					vremQty = parseFloat(vremQty) - (noofeaches * veachuomqty);
					nlapiLogExecution('ERROR','vremQty each',vremQty);

				}


				
			 
			for(var s=0;s<result.length;s++)
			{
				var vtranid=result[s].getValue("tranid");
				var vline=result[s].getValue("line");

				if((vtranid==potranid)&&(vline==lineno))
				POorderqty=result[s].getValue("quantity");
				nlapiLogExecution('ERROR','POorderqty',POorderqty);	
			}
			index=i+1;

			// CLOSED TASK VALUES BINDING TO PDF
			
			strxml += "<tr><td width='1%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
			strxml += lineno;
			strxml += "</td>";
			
			strxml += "<td width='25%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
			strxml += sku;
			strxml += "</td>";
			
			strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
			strxml += upccode;
			strxml += "</td>";
			
			
			
			strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
			strxml += lotno;
			strxml += "</td>";
			
			strxml += "<td width='12%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
			strxml += expdate;
			strxml += "</td>";
			
			strxml += "<td width='1%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
			strxml += noofpallets;
			strxml += "</td>";
			
			strxml += "<td width='1%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
			strxml += noofcases;
			strxml += "</td>";
			
			strxml += "<td width='12%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
			strxml += noofeaches;
			strxml += "</td>";
			
			strxml += "<td width='10%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
			strxml += recvdate;
			strxml += "</td>";
			
			strxml += "<td width='12%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
			strxml += closedtask_qty;
			strxml += "</td>";
			
			
			
			strxml += "<td width='7%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
			strxml += POorderqty;
			strxml += "</td>";
			
			strxml += "<td width='10%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
			strxml += enterby;
			strxml += "</td></tr>";
			
	// BindSublistValues(form,lineno,sku,upccode,lotno,expdate,noofpallets,noofcases,noofeaches,recvdate,closedtask_qty,POorderqty,enterby,index);

					
		}
		}
		strxml = strxml + "</table>";
		strxml = strxml + "\n</body>\n</pdf>";
		xml = xml + strxml;

		var file = nlapiXMLToPDF(xml);
		response.setContentType('PDF', 'POReceiptReport.pdf');
		response.write(file.getValue());
	
		}// first if ending
	else // this is the POST block
	{

	}
	
}// funcion ending





function CreateColumnsot()
{
	var columns=new Array();
	columns[0]=new nlobjSearchColumn("custrecord_ebiz_cntrl_no",null,"group");
	columns[1]=new nlobjSearchColumn('custrecord_line_no',null,"group");
	columns[2]=new nlobjSearchColumn('custrecord_sku',null,"group");
	columns[3]=new nlobjSearchColumn('upccode','custrecord_sku',"max");// mfgupc
	columns[4]=new nlobjSearchColumn('custrecord_batch_no',null,"group");
	columns[5]=new nlobjSearchColumn('custrecord_expirydate',null,"max"); // for
																			// expdate
	columns[6]=new nlobjSearchColumn('custrecord_act_end_date',null,"max"); // for
																			// receipt
																			// date
	columns[7]=new nlobjSearchColumn('custrecord_act_qty',null,"sum"); // for
																		// total
																		// recieced
																		// count
	columns[8]=new nlobjSearchColumn('custrecord_taskassignedto',null,"group"); 
	columns[9]=new nlobjSearchColumn("tranid","custrecord_ebiz_order_no","max"); 
	columns[10]=new nlobjSearchColumn("entity","custrecord_ebiz_order_no","max");
	
	return columns;
}


function CreateColumnsct()
{
	var columns=new Array();
	columns[0]=new nlobjSearchColumn("custrecord_ebiztask_ebiz_cntrl_no",null,"group");
	columns[1]=new nlobjSearchColumn('custrecord_ebiztask_line_no',null,"group");
	columns[2]=new nlobjSearchColumn('custrecord_ebiztask_sku',null,"group");
	columns[3]=new nlobjSearchColumn('upccode','custrecord_ebiztask_sku',"max");// mfgupc
	columns[4]=new nlobjSearchColumn('custrecord_ebiztask_batch_no',null,"group");
	columns[5]=new nlobjSearchColumn('custrecord_ebiztask_expirydate',null,"max"); // for
																					// expdate
	columns[6]=new nlobjSearchColumn('custrecord_ebiztask_act_end_date',null,"max"); // for
																						// receipt
																						// date
	columns[7]=new nlobjSearchColumn('custrecord_ebiztask_act_qty',null,"sum"); // for
																				// total
																				// recieced
																				// count
	columns[8]=new nlobjSearchColumn('custrecord_ebiztask_taskassgndid',null,"group"); 
	columns[9]=new nlobjSearchColumn("tranid","custrecord_ebiztask_ebiz_order_no","max"); 
	columns[10]=new nlobjSearchColumn("entity","custrecord_ebiztask_ebiz_order_no","max"); 
	return columns;
}

// for item dimensions
function GetItemdimensionvalues(itemvalue)
{
var itemdimensionfilter=new Array();
itemdimensionfilter.push(new nlobjSearchFilter('custrecord_ebizitemdims',null,'anyof',itemvalue));

var itemcolumns=new Array();

itemcolumns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');								// ITEM/SKU
itemcolumns[1] = new nlobjSearchColumn('custrecord_ebizuomskudim');								// UOM
itemcolumns[2] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim');						// UOM
																								// LEVEL
itemcolumns[3] = new nlobjSearchColumn('custrecord_ebizqty');									// QUANTITY
itemcolumns[4] = new nlobjSearchColumn('custrecord_ebizbaseuom');								// BASE
																								// UOM
																								// FLAG
itemcolumns[5] = new nlobjSearchColumn('custrecord_ebizdims_packflag');							// PACK
																								// FLAG
itemcolumns[6] = new nlobjSearchColumn('custrecord_ebizweight');								// WEIGHT
itemcolumns[7] = new nlobjSearchColumn('custrecord_ebizcube');	

var	itemdimension_searchrecords=new nlapiSearchRecord('customrecord_ebiznet_skudims',null,itemdimensionfilter,itemcolumns);
nlapiLogExecution('ERROR','itemdimension_searchrecords_length',itemdimension_searchrecords.length);
return itemdimension_searchrecords;
// end item
}
function GetPuracahseOrderValues(pointerid)
{
	/*
	 * var trantype = nlapiLookupField('transaction', pointerid, 'recordType');
	 * //nlapiLogExecution('ERROR', 'trantype', trantype);
	 * 
	 * var transaction; if(trantype =='purchaseorder') { transaction =
	 * nlapiLoadRecord('purchaseorder', pointerid); }
	 */
	
	var polinefilters = new Array();
	polinefilters[0] = new nlobjSearchFilter('internalid', null, 'anyof', pointerid);			
	polinefilters[1] = new nlobjSearchFilter('item', null, 'noneof','@NONE@');

	var polinecolumns = new Array();
	polinecolumns[0] = new nlobjSearchColumn('line');
	polinecolumns[1] = new nlobjSearchColumn('item');
	polinecolumns[2] = new nlobjSearchColumn('quantity');
	polinecolumns[3] = new nlobjSearchColumn('custcol_ebiznet_item_status');
	polinecolumns[4] = new nlobjSearchColumn('formulanumeric');
	polinecolumns[4].setFormula('ABS({quantity})');
	polinecolumns[5] = new nlobjSearchColumn('tranid'); // for poid
	polinecolumns[6] = new nlobjSearchColumn('entity');
	polinecolumns[7] = new nlobjSearchColumn('billaddressee');
	polinecolumns[8] = new nlobjSearchColumn('trandate'); // for pdf po date
	polinecolumns[9] = new nlobjSearchColumn('custbody_poshippingitem'); // for
																			// pdf
																			// carrier
	polinecolumns[10] = new nlobjSearchColumn('custbody_nswmspoexpshipdate'); // for
																				// exp
																				// shipdate
	polinecolumns[11] = new nlobjSearchColumn('custbody_nswmspoarrdate'); // for
																			// exp
																			// arrival
																			// date
	polinecolumns[12] = new nlobjSearchColumn('trandate');
	polinecolumns[13] = new nlobjSearchColumn('billaddress'); //for vendor address
	
	var posearchresults = nlapiSearchRecord('purchaseorder', null, polinefilters, polinecolumns);
	if(posearchresults !=null && posearchresults !='')
		{
		return posearchresults;
		}
	else
		{
		return null;
		}
}

function GetPOInternalId(POid)
{
	var purchaseorderFilers = new Array();
	purchaseorderFilers.push(new nlobjSearchFilter('mainline', null, 'is', 'T')); 
	purchaseorderFilers.push(new nlobjSearchFilter('tranid', null, 'is', POid)); 
	// salesorderFilers.push(new nlobjSearchFilter('line', null, 'is',
	// ordline));
	
	
	var customerSearchResults = nlapiSearchRecord('purchaseorder', null, purchaseorderFilers,null);	
	if(customerSearchResults==null)
	{
		// nlapiLogExecution('ERROR', 'transferorder','transferorder');
		customerSearchResults=nlapiSearchRecord('transferorder',null,purchaseorderFilers,null);
	}
	
	if(customerSearchResults != null && customerSearchResults != "")
	{
		// nlapiLogExecution('ERROR', 'customerSearchResults.length
		// ',customerSearchResults.length);
		return customerSearchResults[0].getId();
	}
	else
		return null;
}
// for item dimension count calculations
function getSKUDimCount(orderQty, dimQty){

	var str = 'order qty. = ' + orderQty + '<br>';
	str = str + 'dimensions qty. = ' + dimQty + '<br>';	
	nlapiLogExecution('DEBUG', 'into getSKUDimCount', str);

	var retSKUDimCount = 0;

	if(dimQty > 0 && (parseFloat(orderQty) >= parseFloat(dimQty)))
	{
		skuDimCount = parseFloat(orderQty)/parseFloat(dimQty);
		nlapiLogExecution('DEBUG', 'skuDimCount', skuDimCount);
		retSKUDimCount = Math.floor(skuDimCount);
		if(retSKUDimCount == 0)
			retSKUDimCount = 1;
	}
	else
		retSKUDimCount = 0; 

	nlapiLogExecution('DEBUG', 'out of getSKUDimCount', retSKUDimCount);

	return retSKUDimCount;
}

function geteBizItemDimensions1(itemid)
{
	var searchRec = new Array();
	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', itemid));
	// filter.push(new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null,
	// 'anyof', uomlevel));
	// filter.push(new nlobjSearchFilter('custrecord_ebizuomskudim', null,
	// 'anyof', uomlevel));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim') ;
	column[1] = new nlobjSearchColumn('custrecord_ebizqty') ;
	column[2] = new nlobjSearchColumn('custrecord_ebizbaseuom') ;
	column[3] = new nlobjSearchColumn('custrecord_ebiznsuom') ;
	column[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim') ;
	column[5] = new nlobjSearchColumn('custrecord_ebizcube');
	searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);

	return searchRec;

}