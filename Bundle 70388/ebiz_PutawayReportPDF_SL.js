/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Suitelet/ebiz_PutawayReportPDF_SL.js,v $
 *     	   $Revision: 1.3.4.1.8.8.2.3 $
 *     	   $Date: 2015/12/03 15:51:31 $
 *     	   $Author: aanchal $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_PutawayReportPDF_SL.js,v $
 * Revision 1.3.4.1.8.8.2.3  2015/12/03 15:51:31  aanchal
 * 2015.2 Issue Fix
 * 201415898
 *
 * Revision 1.3.4.1.8.8.2.2  2015/11/05 17:41:36  grao
 * 2015.2 Issue Fixes 201414055
 *
 * Revision 1.3.4.1.8.8.2.1  2015/10/15 14:27:21  schepuri
 * case# 201415028
 *
 * Revision 1.3.4.1.8.8  2015/08/21 15:29:13  grao
 * 2015.2   issue fixes 201414055
 *
 * Revision 1.3.4.1.8.7  2015/08/11 15:39:46  grao
 * 2015.2   issue fixes  201413879
 *
 * Revision 1.3.4.1.8.6  2015/07/22 13:23:32  schepuri
 * case# 201413596
 *
 * Revision 1.3.4.1.8.5  2015/05/04 14:07:18  schepuri
 * case# 201412590
 *
 * Revision 1.3.4.1.8.4  2014/12/23 13:58:33  schepuri
 * issue# 201410888
 *
 * Revision 1.3.4.1.8.3  2013/08/22 15:04:52  rmukkera
 * Case# 20123990
 *
 * Revision 1.3.4.1.8.2  2013/08/20 15:55:12  grao
 * case# 20123911
 * SB Issue fixes Added Container Lp in Put away report PDF
 *
 * Revision 1.3.4.1.8.1  2013/03/05 14:52:17  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Lexjet production as part of Standard bundle
 *
 * Revision 1.3.4.1  2012/04/20 12:57:03  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.3  2011/07/21 08:06:05  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/

function PutawayReportPDFSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Putaway Report');

		var getPOid = '';
		if (request.getParameter('custparam_po') != null && request.getParameter('custparam_po') != "" && request.getParameter('custparam_po')!='null') {
			getPOid = request.getParameter('custparam_po');
		}
		nlapiLogExecution('ERROR', 'getPOid',getPOid);
		var trantype;
		if(getPOid!=null&&getPOid!="")
			trantype = nlapiLookupField('transaction', getPOid, 'recordType');
		nlapiLogExecution('ERROR','trantype',trantype);

		var getPOValue = '';
		if (request.getParameter('custparam_povalue') != null && request.getParameter('custparam_povalue') != "" && request.getParameter('custparam_povalue') != "null") {
			getPOValue = request.getParameter('custparam_povalue');
		}
		//LN,Serial # were comma separated here...!
		var arrayLP = new Array();
		var lpcolumns = new Array();
		var lpfilters = new Array();
		//lpfilters.push(new  nlobjSearchFilter('custrecord_serialnumber', null, 'isnotempty'));
		
		/*if(getPOid!=null && getPOid!='')
		lpfilters = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', getPOid);*/
		// case# 201415898,		201416125
		if(getPOid!=null && getPOid!='')
		{
			// case# 201416125,201415898
			if(trantype == "returnauthorization")
				lpfilters = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', getPOid);
			else if (trantype == "purchaseorder")
				lpfilters = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', getPOid); 

			
		}
		lpcolumns[0] = new nlobjSearchColumn('custrecord_serialparentid');
		lpcolumns[1] = new nlobjSearchColumn('custrecord_serialnumber');
		var lpsearchresults = nlapiSearchRecord('customrecord_ebiznetserialentry', null, lpfilters, lpcolumns);
		for (var i = 0; lpsearchresults != null && i < lpsearchresults.length; i++) {
			var SerialLP = new Array();
			SerialLP[0] = lpsearchresults[i].getValue('custrecord_serialparentid');
			SerialLP[1] = lpsearchresults[i].getValue('custrecord_serialnumber');
			arrayLP.push(SerialLP);
		}
	//var tranType = nlapiLookupField('transaction', getPOid, 'recordType');
	//	nlapiLogExecution('DEBUG', 'tranType',tranType);
		// define search filters
		var filters = new Array();
		if(getPOid!=null && getPOid!='' && getPOid!='null')
		{
			filters.push(new  nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', getPOid));
		}
		filters.push(new  nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]));
		filters.push(new  nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2, 6]));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		columns[1] = new nlobjSearchColumn('custrecord_line_no');
		columns[2] = new nlobjSearchColumn('custrecord_sku');
		columns[3] = new nlobjSearchColumn('custrecord_sku_status');
		columns[4] = new nlobjSearchColumn('custrecord_tasktype');
		columns[5] = new nlobjSearchColumn('custrecord_uom_id');
		columns[6] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[7] = new nlobjSearchColumn('custrecord_ebiz_lpno');
		columns[8] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[9] = new nlobjSearchColumn('custrecord_batch_no');
		columns[10] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[11] = new nlobjSearchColumn('custrecord_transport_lp');
		columns[12] = new nlobjSearchColumn('custrecord_skudesc');// case# 201412590
		columns[1].setSort();

		// execute the  search, passing null filter and return columns
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
		var po_asn, line, itemname, itemstatus, taskstatus, uom, qty, lp, loc, seriallot, retserialcsv, itemintid,transportlp,itemdesc;
		retserialcsv="";

		//case # 201410888
//		nlapiLogExecution('ERROR', 'PDF URL',url);	
		var filefound = nlapiLoadFile('Images/LOGOCOMP.jpg'); 
		if (filefound) 
		{ 
			nlapiLogExecution('ERROR', 'Event', 'file;'+filefound.getId()); 
			var imageurl = filefound.getURL();
			nlapiLogExecution('ERROR','imageurl',imageurl);
			//var finalimageurl = url + imageurl;//+';';
			var finalimageurl = imageurl;//+';';
			//finalimageurl=finalimageurl+ '&expurl=T;';
			nlapiLogExecution('ERROR','imageurl',finalimageurl);
			finalimageurl=finalimageurl.replace(/&/g,"&amp;");

		} 
		else 
		{
			nlapiLogExecution('ERROR', 'Event', 'No file;');
		}


		var replaceChar =/\$|,|@|&|#|~|`|\%|\*|\^|\&|\+|\=|\-|\_|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|\./g;

			var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body  font-size=\"10\">\n";
		nlapiLogExecution('ERROR', 'xml0 ', "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body font-size=\"18\">\n");
		var strxml = "<table  > ";

		strxml += "<tr ><td valign='middle' align='left'><img src='" + finalimageurl + "'></img></td><td valign='middle' align='left'  style='font-size:xx-large;'></td></tr>";

		strxml += "<tr align='center'>";
		strxml += "<td>";
		if(trantype == 'transferorder')// case# 201415028
		{
			strxml += "PUTAWAY REPORT FOR TRANSFER ORDER# " + getPOValue;
		}		
		else if(trantype == 'returnauthorization')
		{
			strxml += "PUTAWAY REPORT FOR RETURN AUTHORIZATION ORDER # " + getPOValue;
		}
		else
		{
			strxml += "PUTAWAY REPORT FOR PURCHASE ORDER# " + getPOValue;
		}
		strxml += "</td></tr></table><table border='1'>";
		strxml = strxml + "<tr border='1' style=\"font-weight:bold\"><td  >";
		strxml += "PO / TO /RMA #";// case# 201413596
		strxml += "</td>";
		strxml += "<td>";
		strxml += "Line#";
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		strxml += "<td>";
		strxml += "Item";
		strxml += "</td>";
		strxml += "<td>";
		strxml += "Item Desc";
		strxml += "</td>";
		strxml += "<td>";
		strxml += "Item Status";
		strxml += "</td>";
		strxml += "<td>";
		strxml += "UOM";
		strxml += "</td>";
		strxml += "<td>";
		strxml += "Qty";
		strxml += "</td>";
		strxml += "<td>";
		strxml += "LP#";
		strxml += "</td>";
		strxml += "<td>";
		strxml += "Cart LP#";
		strxml += "</td>";
		strxml += "<td>";
		strxml += "Serial #";
		strxml += "</td>";
		strxml += "<td>";
		strxml += "LOT#";
		strxml += "</td>";
		strxml += "<td>";
		strxml += "Bin Location";
		strxml = strxml + "</td></tr><tr> <td colspan='8' ></td> </tr>";

		for (var i = 0; searchresults != null && i < searchresults.length; i++) {
			var searchresult = searchresults[i];
			po_asn = searchresult.getValue('custrecord_ebiz_cntrl_no');
			line = searchresult.getValue('custrecord_line_no');
			itemname = searchresult.getText('custrecord_sku');
			itemstatus = searchresult.getValue('custrecord_sku_status');
			nlapiLogExecution('ERROR','itemstatus ',itemstatus );

			if (itemstatus != null && itemstatus != "") {
				itemstatus = nlapiLookupField('customrecord_ebiznet_sku_status', itemstatus, 'name');
				nlapiLogExecution('ERROR', 'itemstatus ', itemstatus);
			}
			taskstatus = searchresult.getText('custrecord_tasktype');
			uom = searchresult.getValue('custrecord_uom_id');
			qty = searchresult.getValue('custrecord_expe_qty');
			lp = searchresult.getValue('custrecord_ebiz_lpno');
			loc = searchresult.getText('custrecord_actbeginloc');
			seriallot = searchresult.getValue('custrecord_batch_no');
			itemintid = searchresult.getValue('custrecord_ebiz_sku_no');
			transportlp = searchresult.getValue('custrecord_transport_lp');
			itemdesc = searchresult.getValue('custrecord_skudesc');
			var serialInflg = "F";
			var batchflg = "F";
			var fields = ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot'];
			var columns = nlapiLookupField('item', itemintid, fields);
			var ItemType = columns.recordType;
			serialInflg = columns.custitem_ebizserialin;
			batchflg = columns.custitem_ebizbatchlot;

			if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") {
				var res = getSerialNoCSV(arrayLP, lp);
				if (res == null || res == "") 
					retserialcsv = res;
				//form.getSubList('custpage_items').setLineItemValue('custpage_serialno', i + 1, retserialcsv);
			}

			if (ItemType == "lotnumberedinventoryitem" || batchflg == "T") {
				//form.getSubList('custpage_items').setLineItemValue('custpage_seriallot', i + 1, seriallot);
			}

			strxml = strxml + "<tr><td >";
			strxml += getPOValue;
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
			strxml += "<td>";
			strxml += line;
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
			strxml += "<td>";
			strxml += itemname;
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
			if(itemdesc != null && itemdesc !="")
				//itemdesc=itemdesc.replace(replaceChar,'');
			    itemdesc=itemdesc.replace(/_/g,"&#95;");

			strxml += "<td>";
			strxml += itemdesc;
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td>";
			strxml += itemstatus;
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
			strxml += "<td>";
			strxml += uom;
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
			strxml += "<td>";
			strxml += qty;
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
			strxml += "<td>";
			strxml += lp;
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;</td>";
			strxml += "<td>";
			strxml += transportlp;
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
			strxml += "<td>";
			if (res != null && res != "") 
			{
			strxml += res;
			}
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
			strxml += "<td>";
			strxml += seriallot;
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
			strxml += "<td>";
			strxml += loc;
			strxml = strxml + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
		}
		strxml = strxml + "</table>";
		strxml = strxml + "\n</body>\n</pdf>";
		nlapiLogExecution('ERROR', 'strxml', strxml);
		xml = xml + strxml;
		nlapiLogExecution('ERROR', 'XML', xml);
		var file = nlapiXMLToPDF(xml);
		response.setContentType('PDF', 'PutawayReport-' + getPOValue + '.pdf');
		response.write(file.getValue());
	}
	else //this is the POST block
	{

	}
}

function getSerialNoCSV(arrayLP, lp){
	var csv = "";
	nlapiLogExecution('ERROR', 'lp', lp);
	for (var i = 0; i < arrayLP.length; i++) {		//nlapiLogExecution('ERROR', 'arrayLP.length', arrayLP.length);
	
		if (arrayLP[i][0] == lp) {
			if(i==(arrayLP.length-1))
			{
			csv += arrayLP[i][1];
			break;
			}
			else
			csv += arrayLP[i][1] + " , ";
		}
	}
	return csv;
}
