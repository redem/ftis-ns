/***************************************************************************
 eBizNET Solutions Inc              
 ***************************************************************************
 **     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_PickingLocationAndItem.js,v $
 *     	   $Revision: 1.1.2.26 $
 *     	   $Date: 2015/07/07 15:50:06 $
 *     	   $Author: nneelam $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_PickingLocationAndItem.js,v $
 * Revision 1.1.2.26  2015/07/07 15:50:06  nneelam
 * case# 201413264
 *
 * Revision 1.1.2.25  2015/07/02 19:45:20  snimmakayala
 * Case#: 201413289
 *
 * Revision 1.1.2.24  2015/07/02 15:28:15  grao
 * 2015.2 issue fixes  201413118
 *
 * Revision 1.1.2.23  2015/04/22 15:30:12  skreddy
 * Case# 201412398
 * standard bundle issue fix
 *
 * Revision 1.1.2.22  2015/02/03 14:48:46  rrpulicherla
 * Case#201411317
 *
 * Revision 1.1.2.21  2015/01/27 16:40:37  rrpulicherla
 * Case#201411317
 *
 * Revision 1.1.2.20  2015/01/14 13:54:56  schepuri
 * issue # 201411398
 *
 * Revision 1.1.2.19  2014/11/25 15:48:20  skavuri
 * Case# 201410738 Std bundle issue fixed
 *
 * Revision 1.1.2.18  2014/10/16 13:43:54  vmandala
 * Case# 201410722 std bundle issue fixed
 *
 * Revision 1.1.2.17  2014/09/25 17:57:17  skreddy
 * case # 201410534
 * Alpha comm CR (''pick by carton LP")
 *
 * Revision 1.1.2.16  2014/09/25 15:44:54  skavuri
 * Case# 201410496 std bundle issue fixed
 *
 * Revision 1.1.2.15  2014/09/03 07:40:00  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#201410213
 *
 * Revision 1.1.2.14  2014/09/02 10:58:04  gkalla
 * case#20149227
 * ACE and JB cycle count issues
 *
 * Revision 1.1.2.13  2014/07/14 06:07:30  skreddy
 * case # 20149328
 * sonic sb issue fix
 *
 * Revision 1.1.2.12  2014/07/11 07:52:10  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20149309
 *
 * Revision 1.1.2.11  2014/07/08 13:23:52  snimmakayala
 * 20149092
 *
 * Revision 1.1.2.10  2014/07/08 13:06:10  snimmakayala
 * 20149092
 *
 * Revision 1.1.2.9  2014/07/01 23:43:52  skreddy
 * case # 20149114
 * sonic SB  issue fix
 *
 * Revision 1.1.2.8  2014/06/13 12:28:31  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.7  2014/06/06 08:08:56  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.1.2.6  2014/05/27 13:57:22  snimmakayala
 * Case#: 20148567
 * RF Packing issue fixes
 *
 * Revision 1.1.2.5  2014/05/26 07:08:44  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148320
 *
 * Revision 1.1.2.4  2014/05/09 14:32:48  rmukkera
 * Case# 20148334
 *
 * Revision 1.1.2.3  2013/11/14 06:32:04  skreddy
 * Case# 201215000
 * Fast Picking issue fix
 *
 * Revision 1.1.2.2  2013/09/16 09:40:01  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201215000
 * RF Fast Picking
 *
 * Revision 1.1.2.1  2013/08/15 01:43:06  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201215000
 * RF Fast Picking
 *

 *****************************************/

function PickingLocationAndItem(request, response){
	if (request.getMethod() == 'GET'){

		var fastpick = request.getParameter('custparam_fastpick');
		nlapiLogExecution('DEBUG', 'fastpick', 'fastpick');

		var getFetchedBeginLocation = request.getParameter('custparam_beginLocation');
		var getWaveno = request.getParameter('custparam_waveno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocation = request.getParameter('custparam_beginLocation');
		var getItem = request.getParameter('custparam_item');
		var getItemName = request.getParameter('custparam_itemname');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var getFetchedLocation = request.getParameter('custparam_beginLocationname');
		if(getFetchedLocation == null || getFetchedLocation == '' || getFetchedLocation =='null' || getFetchedLocation =='undefined')
			getFetchedLocation = request.getParameter('custparam_beginlocationname');
		var vClusterNo = request.getParameter('custparam_clusterno');
		var vBatchNo = request.getParameter('custparam_batchno');	
		var whLocation = request.getParameter('custparam_whlocation');
		var whCompany = request.getParameter('custparam_whcompany');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var pickType=request.getParameter('custparam_picktype');
		var OrdNo=request.getParameter('name');
		var ContainerSize=request.getParameter('custparam_containersize');
		var vSOId=request.getParameter('custparam_ebizordno');
		var getZoneNo=request.getParameter('custparam_ebizzoneno');
		var getnextExpectedQuantity = request.getParameter('custparam_nextexpectedquantity');
		var getEnteredLocation = request.getParameter('custparam_endlocation');
		var type1=request.getParameter('custparam_type');
		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid'); 
		var getNumber = request.getParameter('custparam_number');
		var getSerialNo = request.getParameter('custparam_serialno');

		//LL_CustomCode starts
		var trackingno_redirect = getSystemRuleValue('ShipAlone');
		var vSkipId1=request.getParameter('custparam_skipid');
		if(trackingno_redirect=='Y')
		{
			var SOarray = new Array();

			SOarray["custparam_picktype"] = pickType;
			SOarray["custparam_error"] = 'INVALID ITEM';
			SOarray["custparam_whlocation"] = whLocation;
			SOarray["custparam_waveno"] = getWaveno;
			SOarray["custparam_recordinternalid"] = getRecordInternalId;
			//nlapiLogExecution('ERROR', 'Load Rec Id', SOarray["custparam_recordinternalid"]);

			SOarray["custparam_containerlpno"] = getContainerLpNo;
			SOarray["custparam_expectedquantity"] = getExpectedQuantity;
			SOarray["custparam_beginLocation"] = getBeginLocation;
			SOarray["custparam_beginLocationname"] = getFetchedLocation;//sri
			SOarray["custparam_item"] = getItem;
			SOarray["custparam_itemname"] = getItemName;
			SOarray["custparam_itemdescription"] = getItemDescription;
			SOarray["custparam_iteminternalid"] = getItemInternalId;
			SOarray["custparam_dolineid"] = getDOLineId;
			SOarray["custparam_invoicerefno"] = getInvoiceRefNo;
			SOarray["custparam_orderlineno"] = getOrderLineNo;
			SOarray["custparam_clusterno"] = vClusterNo;
			SOarray["custparam_batchno"] = vBatchNo;
			SOarray["custparam_noofrecords"] = RecordCount;
			SOarray["custparam_nextlocation"] = NextLocation;
			SOarray["custparam_nextiteminternalid"] = NextItemInternalId;
			SOarray["name"] = OrdNo;
			SOarray["custparam_containersize"] = ContainerSize;
			SOarray["custparam_ebizordno"] = vSOId;
			SOarray["custparam_endlocinternalid"] =getEndLocInternalId;
			SOarray["custparam_endlocation"] = getEnteredLocation;
			SOarray["custparam_nextexpectedquantity"] = getnextExpectedQuantity;
			SOarray["custparam_Actbatchno"] = "";
			SOarray["custparam_Expbatchno"] = "";
			SOarray["custparam_skipid"] = vSkipId1;
			SOarray["custparam_fastpick"] = fastpick;
			SOarray["custparam_screenno"] = 'pickingLocItem';
			//check the script of ebiz_RF_pickinglocationAndItem_LL at custom bundle
			response.sendRedirect('SUITELET', 'customscript_ebiz_custom_sl_14', 'customdeploy_ebiz_custom_sl_14', false, SOarray);
		}
		//LL_CustomCode ends
		nlapiLogExecution('Error', 'getItemInternalId', getItemInternalId);
		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');

		nlapiLogExecution('Error', 'getItemName', getItemName);			
		nlapiLogExecution('Error', 'getItem', getItem);
		nlapiLogExecution('Error', 'getFetchedLocation', getFetchedLocation);
		nlapiLogExecution('Error', 'NextLocation', NextLocation);
		nlapiLogExecution('Error', 'NextItemInternalId', NextItemInternalId);

		if(getFetchedLocation==null || getFetchedLocation == "")
		{
			getFetchedLocation=NextLocation;
		}

		if(getItemInternalId==null || getItemInternalId == "")
		{
			getItemInternalId=NextItemInternalId;
		}
		//code added by santosh on 7Aug2012
		var Type='';
		if(request.getParameter('custparam_type') !=null &&  request.getParameter('custparam_type')!="")
		{
			Type=request.getParameter('custparam_type');
		}

		//end of code on 7aug2012

		var NextItemId=request.getParameter('custparam_nextiteminternalid');		
		var getOrderNo=request.getParameter('custparam_ebizordno');
		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid'); 
		var name=request.getParameter('name');
		var getEnteredLocation = request.getParameter('custparam_endlocation');



		//********************************************************************************************************************************//

		var RecCount;
		var SOFilters = new Array();
		SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));
		if((pickType.indexOf('W') != -1 || pickType=='ALL') && getWaveno != null && getWaveno != "")
		{
			SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(getWaveno)));
		}
		if((pickType.indexOf('O') != -1 || pickType=='ALL') && OrdNo!=null && OrdNo!="" && OrdNo!= "null")
		{
			nlapiLogExecution('ERROR', 'OrdNo inside If', OrdNo);
			SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdNo));
		}
		if((pickType.indexOf('S') != -1 || pickType=='ALL') && vSOId!=null && vSOId!="" && vSOId!= "null")
		{
			nlapiLogExecution('ERROR', 'SO Id inside If', vSOId);
			SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vSOId));
		}
		if((pickType.indexOf('Z') != -1 || pickType=='ALL') && getZoneNo!=null && getZoneNo!="" && getZoneNo!= "null")
		{
			nlapiLogExecution('ERROR', 'Zone # inside If', getZoneNo);
			SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', getZoneNo));			
		}
		if((pickType.indexOf('C') != -1 || pickType=='ALL') && getContainerLpNo!=null && getContainerLpNo!="" && getContainerLpNo!= "null") 
		{
			nlapiLogExecution('ERROR', 'Carton # inside If', getContainerLpNo);
			SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getContainerLpNo));			
		}
		var SOColumns = new Array();
		SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));	
		SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
		SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));	
		SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));		
		SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
		SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
		SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
		SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
		SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
		SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
		SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
		SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
		SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
		SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
		SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
		SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
		SOColumns.push(new nlobjSearchColumn('name'));
		SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
		SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
		SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
		SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));

		SOColumns[0].setSort();
		SOColumns[1].setSort();
		SOColumns[2].setSort();
		SOColumns[3].setSort();
		SOColumns[4].setSort();
		SOColumns[5].setSort(true);

		var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
		if (SOSearchResults != null && SOSearchResults != '' && SOSearchResults.length > 0) {
			nlapiLogExecution('ERROR', 'SOSearchResults.length', SOSearchResults.length);

			if(SOSearchResults.length > 0)
			{
				//var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];
				var SOSearchResult = SOSearchResults[0];
				OrdNo = SOSearchResult.getValue('name');
				getFetchedLocation = SOSearchResult.getText('custrecord_actbeginloc');
				getRecordInternalId = SOSearchResult.getId();
				getExpectedQuantity = SOSearchResult.getValue('custrecord_expe_qty');
				getItem = SOSearchResult.getValue('custrecord_sku');
				getItemName = SOSearchResult.getText('custrecord_sku');
				getItemInternalId = SOSearchResult.getValue('custrecord_sku');
				getDOLineId = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
				getInvoiceRefNo = SOSearchResult.getValue('custrecord_invref_no');
				getOrderLineNo = SOSearchResult.getValue('custrecord_line_no');
				vSOId = SOSearchResult.getValue('custrecord_ebiz_order_no');

				//if(SOSearchResults.length > 1)
				if(SOSearchResults.length >  parseInt(vSkipId) + 1)
				{

					//var SOSearchNextResult = SOSearchResults[1];
					var SOSearchNextResult = SOSearchResults[parseInt(vSkipId) + 1];// case# 201416947
					NextLocation = SOSearchNextResult.getText('custrecord_actbeginloc');
					NextItemId = SOSearchNextResult.getValue('custrecord_sku');
					NextItemInternalId = SOSearchNextResult.getValue('custrecord_sku');
				}
			}
		}
		else
		{
			var SOarray = new Array();
			SOarray["custparam_screenno"] = '12';
			SOarray["custparam_error"] = 'No Open Picks to Confirm.';
			nlapiLogExecution('DEBUG', 'Error: ', 'no open picks');
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			return;
		}

		nlapiLogExecution('Error', 'getItemName', getItemName);			
		nlapiLogExecution('Error', 'getItem', getItem);
		nlapiLogExecution('Error', 'getFetchedLocation', getFetchedLocation);
		nlapiLogExecution('Error', 'NextLocation', NextLocation);
		nlapiLogExecution('Error', 'NextItemInternalId', NextItemInternalId);
		nlapiLogExecution('Error', 'vSOId', vSOId);
		nlapiLogExecution('Error', 'getOrderNo', getOrderNo);


		//********************************************************************************************************************************//



		var Itemdescription='';

		nlapiLogExecution('ERROR', 'getItemName1', getItemName);	

		if(getItemInternalId!=null && getItemInternalId!='')
		{
			var Itemtype = nlapiLookupField('item', getItemInternalId, 'recordType');

			var filtersitem = new Array();
			var columnsitem = new Array();

			filtersitem.push(new nlobjSearchFilter('internalid', null, 'is',getItemInternalId));
			filtersitem.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

			columnsitem[0] = new nlobjSearchColumn('description');    
			columnsitem[1] = new nlobjSearchColumn('salesdescription');
			columnsitem[2] = new nlobjSearchColumn('itemid');

			var itemRecord = nlapiSearchRecord(Itemtype, null, filtersitem, columnsitem);
			if(itemRecord!=null && itemRecord!='' && itemRecord.length>0)
			{
				if(itemRecord[0].getValue('description') != null && itemRecord[0].getValue('description') != '')
				{
					Itemdescription = itemRecord[0].getValue('description');
				}
				else if(itemRecord[0].getValue('salesdescription') != null && itemRecord[0].getValue('salesdescription') != "")
				{	
					Itemdescription = itemRecord[0].getValue('salesdescription');
				}

				getItemName = itemRecord[0].getValue('itemid');
			}

			Itemdescription = Itemdescription.substring(0, 20);			
		}

		nlapiLogExecution('ERROR', 'getItemName2', getItemName);



		nlapiLogExecution('Error', 'getRecordInternalId', getRecordInternalId);	
		nlapiLogExecution('ERROR', 'getItem', getItem);	
		nlapiLogExecution('ERROR', 'getItemDescription', getItemDescription);
		nlapiLogExecution('ERROR', 'getItem', getItemName);
		nlapiLogExecution('ERROR', 'getItemdescription', Itemdescription);
		nlapiLogExecution('ERROR', 'Next Location', NextLocation);
		//Case# 201410496 starts
		var boolstageflag='F';
		var taskfilters = new Array();
		taskfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));//Task Type - PICK
		taskfilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['8','28']));// Pick Confirm and Pack Complete
		if(getWaveno!=null && getWaveno!='')
			taskfilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(getWaveno)));	
		var tasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, taskfilters, null);
		if(tasksearchresults!=null && tasksearchresults !='null' && tasksearchresults !='')
		{
			boolstageflag='T';
		}
		nlapiLogExecution('ERROR', 'boolstageflag', boolstageflag);
		//Case# 201410496 ends

		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";   
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		//html = html + " document.getElementById('enterlocation').focus();";  

//		html = html + "function validateItem(form) { ";
//		html = html + "	  if (form.enteritem.value==''){";
//		html = html + "   form.enteritem.focus();";  
//		html = html + "	  return false;} ";
//		html = html + "	} ";

		html = html + "function stopRKey(evt) { ";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('enteritem').value=='' || document.getElementById('enteritem').value==null){";
		html = html + "   document.getElementById('enteritem').focus();";
		html = html + "	  return false;}} ";
		html = html + "	} ";
		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>ORDER# :<label>" + OrdNo + "</label>,WAVE# :<label>" + getWaveno + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";

		html = html + "			<tr>";
		html = html + "				<td align = 'left'>LOCATION: <label>" + getFetchedLocation + "</label>";
		html = html + "				<input type='hidden' name='hdntype' value=" + Type + ">";
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + getFetchedLocation + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemName' value='" + getItemName + "'>";// case# 201411398
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vClusterNo  + ">";
		html = html + "				<input type='hidden' name='hdnBatchNo' value=" + vBatchNo  + ">";	
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnwhCompany' value=" + whCompany + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";	
		html = html + "				<input type='hidden' name='hdnOrdNo' value=" + OrdNo + ">";
		html = html + "				<input type='hidden' name='hdnsoid' value=" + vSOId + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + ContainerSize + ">";	
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		html = html + "				<input type='hidden' name='hdnebizzoneno' value=" + getZoneNo + ">";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdnfastpick' value=" + fastpick + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";		
		html = html + "				<td align = 'left'>ITEM: <label>" + getItemName + "</label><br> ITEM DESCRIPTION: <label>" + Itemdescription + "</label>";//<br>REMAINING QTY: <label>" + remqty + "</label>";
		html = html + "			</tr>";

		html = html + "			<tr>";
		html = html + "				<td align = 'left'>ENTER/SCAN LOCATION ";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlocation' id='enterlocation' type='text' tabindex='1'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>ENTER/SCAN ITEM ";//ENTER/SCAN ITEM 
		html = html + "				<input type='hidden' name='hdnnext' value=" + NextLocation + ">";
		html = html + "				<input type='hidden' name='hdnnextitem' value=" + NextItemId + ">";	
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";	
		html = html + "				<input type='hidden' name='hdnName' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + ContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnebizOrdNo' value=" + getOrderNo + ">";
		html = html + "				<input type='hidden' name='hdnSkipflag'>";
		html = html + "				<input type='hidden' name='hdnStageflag'>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enteritem' type='text' tabindex='2'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>SEND <input name='cmdSend' tabindex='3' type='submit' value='F8' onclick='this.form.submit();this.disabled=true;this.form.cmdSKIP.disabled=true;this.form.cmdOverride.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7' tabindex='4'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		//Case# 201410496 starts
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";//Case# 201410738
		if(boolstageflag=='T')
		{
			//html = html + "				<td align = 'left'>"; //Case# 201410738
			html = html + "					STAGE <input name='cmdOverride' type='submit' value='F11' tabindex='5' onclick='this.form.hdnStageflag.value=this.value;this.form.submit();this.focus();this.disabled=true;this.form.cmdSend.disabled=true;this.form.cmdSKIP.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
			//html = html + "				</td>"; // Case# 201410738
		}
		//Case# 201410496 ends
		//html = html + "				<td align = 'left'>"; // Case# 201410738
		html = html + "					SKIP <input name='cmdSKIP' type='submit' value='F12' tabindex='6' onclick='this.form.hdnSkipflag.value=this.value;this.form.submit();this.focus();this.disabled=true;this.form.cmdSend.disabled=true;this.form.cmdOverride.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlocation').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);



	}
	else
	{
		nlapiLogExecution('Error', 'Into Response', 'Into Response');

		var fastpick = request.getParameter('hdnfastpick');

		nlapiLogExecution('DEBUG', 'fastpick', 'fastpick');

		var getEnteredItem = request.getParameter('enteritem');
		var getBeginLocation='';
		var WaveNo=request.getParameter('hdnWaveNo');
		var OrdNo=request.getParameter('hdnOrdNo');
		var ClusNo = request.getParameter('hdnClusterNo');
		var NextShowLocation=request.getParameter('hdnBeginLocation');
		var NextShowItemId=request.getParameter('hdnItemInternalId');
		var vSOId=request.getParameter('hdnsoid');
		var vZoneId=request.getParameter('hdnebizzoneno');
		var vSkipId=request.getParameter('hdnskipid');
		var Type=request.getParameter('hdntype');
		var getWaveNo = request.getParameter('hdnWaveNo');
		var getRecordInternalId = request.getParameter('hdnRecordInternalId');
		var getContainerLpNo = request.getParameter('hdnContainerLpNo');
		var getExpectedQuantity = request.getParameter('hdnExpectedQuantity');
		var getBeginLocation = request.getParameter('hdnBeginLocation');
		var getBeginLocationid = request.getParameter('hdnBeginLocationid');
		var getItem = request.getParameter('hdnItem');
		var getItemName = request.getParameter('hdnItemName');
		var getItemDescription = request.getParameter('hdnItemDescription');
		var getItemInternalId = request.getParameter('hdnItemInternalId');
		var getDOLineId = request.getParameter('hdnDOLineId');
		var getInvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var getOrderLineNo = request.getParameter('hdnOrderLineNo');
		var vClusterNo = request.getParameter('hdnClusterNo');
		var vBatchNo = request.getParameter('hdnbatchno');	
		var whLocation = request.getParameter('hdnwhlocation');
		var vZoneId=request.getParameter('hdnebizzoneno');
		var nextexpqty=request.getParameter('hdnextExpectedQuantity');
		var whCompany = request.getParameter('hdnwhCompany');
		var RecCount=request.getParameter('hdnRecCount');
		var getNext=request.getParameter('hdnnext');
		var getNextItemId=request.getParameter('hdnnextitem');
		var OrdName=request.getParameter('hdnName');
		var ContainerSize=request.getParameter('hdnContainerSize');
		var ebizOrdNo=request.getParameter('hdnebizOrdNo');
		var getNumber = request.getParameter('custparam_number');
		var getSerialNo = request.getParameter('custparam_serialno');

		var SOarray = new Array();

		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		SOarray["custparam_error"] = 'INVALID ITEM';
		SOarray["custparam_whlocation"] = whLocation;
		SOarray["custparam_waveno"] = getWaveNo;
		SOarray["custparam_recordinternalid"] = getRecordInternalId;
		nlapiLogExecution('ERROR', 'Load Rec Id', SOarray["custparam_recordinternalid"]);
		SOarray["custparam_containerlpno"] = getContainerLpNo;
		SOarray["custparam_expectedquantity"] = getExpectedQuantity;
		SOarray["custparam_beginLocation"] = getBeginLocation;
		SOarray["custparam_beginLocationname"] = getBeginLocation;
		SOarray["custparam_item"] = getItem;
		SOarray["custparam_itemname"] = getItemName;
		SOarray["custparam_itemdescription"] = getItemDescription;
		SOarray["custparam_iteminternalid"] = getItemInternalId;
		SOarray["custparam_dolineid"] = getDOLineId;
		SOarray["custparam_invoicerefno"] = getInvoiceRefNo;
		SOarray["custparam_orderlineno"] = getOrderLineNo;
		SOarray["custparam_clusterno"] = vClusterNo;
		SOarray["custparam_batchno"] = vBatchNo;
		SOarray["custparam_noofrecords"] = RecCount;
		SOarray["custparam_nextlocation"] = getNext;
		SOarray["custparam_nextiteminternalid"] = getNextItemId;
		SOarray["name"] = OrdName;
		SOarray["custparam_containersize"] = ContainerSize;
		SOarray["custparam_ebizordno"] = ebizOrdNo;
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
		SOarray["custparam_nextexpectedquantity"] = nextexpqty;
		SOarray["custparam_Actbatchno"] = "";
		SOarray["custparam_Expbatchno"] = "";
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');
		SOarray["custparam_fastpick"] = fastpick;
		SOarray["custparam_number"] = getNumber;
		SOarray["custparam_serialno"] = getSerialNo;

		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  request.getParameter('hdnebizzoneno');
		}
		else
			SOarray["custparam_ebizzoneno"] = '';
		var vSkipId=request.getParameter('hdnskipid');
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');
		nlapiLogExecution('DEBUG', 'Order LineNo is', getOrderLineNo);

		nlapiLogExecution('Error', 'NextShowLocation', NextShowLocation);
		nlapiLogExecution('Error', 'getExpectedQuantity', getExpectedQuantity);
		nlapiLogExecution('Error', 'NextShowItemId', NextShowItemId);
		nlapiLogExecution('ERROR', 'Wave No', WaveNo);
		nlapiLogExecution('ERROR', 'OrdNo', OrdNo);
		nlapiLogExecution('ERROR', 'ClusNo', ClusNo);
		nlapiLogExecution('ERROR', 'vSkipId', vSkipId);
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		SOarray["custparam_error"] = 'INVALID LOCATION OR ITEM';
		SOarray["custparam_screenno"] = 'pickingLocItem';

		var getEnteredLocation = request.getParameter('enterlocation');		
		nlapiLogExecution('Error', 'Entered Location', getEnteredLocation);	
		nlapiLogExecution('Error', 'Location', getBeginLocation);	

		var vPickType=request.getParameter('hdnpicktype');
		nlapiLogExecution('ERROR', 'vPickType',vPickType);

		if(request.getParameter('hdnStageflag')!='F11')
		{
			var RecCount;					

			var SOFilters = new Array();
			SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));
			if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && getWaveNo != null && getWaveNo != "")
			{
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(getWaveNo)));
			}
			if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
			{
				nlapiLogExecution('ERROR', 'ClusNo inside If', vClusterNo);
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
			}
			if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdNo!=null && OrdNo!="" && OrdNo!= "null")
			{
				nlapiLogExecution('ERROR', 'OrdNo inside If', OrdNo);
				SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdNo));
			}
			if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && vSOId!=null && vSOId!="" && vSOId!= "null")
			{
				nlapiLogExecution('ERROR', 'SO Id inside If', vSOId);
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vSOId));
			}
			if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
			{
				nlapiLogExecution('ERROR', 'Zone # inside If', vZoneId);
				SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
			}

			if((vPickType.indexOf('C') != -1 || vPickType=='ALL') && getContainerLpNo!=null && getContainerLpNo!="" && getContainerLpNo!= "null") 
			{
				nlapiLogExecution('ERROR', 'Carton # inside If', getContainerLpNo);
				SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getContainerLpNo));			
			}

			SOarray["custparam_ebizordno"] =vSOId;
			var SOColumns = new Array();
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
//			SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
			//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
			SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
			//Code end as on 290414
			SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
			SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
			SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
			SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
			SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
			SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
			SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
			SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
			SOColumns.push(new nlobjSearchColumn('name'));
			SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
			SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
			SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));

			SOColumns[0].setSort();
			SOColumns[1].setSort();
			SOColumns[2].setSort();
			SOColumns[3].setSort();
			SOColumns[4].setSort();
			SOColumns[5].setSort(true);

			var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
			nlapiLogExecution('ERROR', 'Results', SOSearchResults);
			if (SOSearchResults != null && SOSearchResults.length > 0) {
				nlapiLogExecution('ERROR', 'SOSearchResults.length', SOSearchResults.length);
				nlapiLogExecution('ERROR', 'vSkipId', vSkipId);
				if(vSkipId == '' || vSkipId == null)
					vSkipId=0;
				if(SOSearchResults.length <= parseInt(vSkipId))
				{
					vSkipId=0;
				}
				nlapiLogExecution('ERROR', 'SearchResults of given Wave', WaveNo);
				var SOSearchResult = SOSearchResults[parseInt(vSkipId)];
				if(SOSearchResults.length > parseInt(vSkipId) + 1)
				{
					var SOSearchnextResult = SOSearchResults[parseInt(vSkipId) + 1];
					SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
					SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
					SOarray["custparam_nextexpectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
					nlapiLogExecution('ERROR', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
					nlapiLogExecution('ERROR', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
				}
				else
				{
					var SOSearchnextResult = SOSearchResults[0];
					SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
					SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
					SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
				}
				nlapiLogExecution('ERROR', 'SearchResults of given Wave', SOSearchResult.getValue('custrecord_lpno'));
				nlapiLogExecution('ERROR', 'SearchResults of given Wave', SOSearchResult.getValue('custrecord_expe_qty'));
				nlapiLogExecution('ERROR', 'SearchResults of given Wave', WaveNo);
				SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
				SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
				nlapiLogExecution('ERROR', 'Main Rec Id', SOarray["custparam_recordinternalid"]);
				SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
				SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
				SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
				SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
				SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
				SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
				SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
				SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
				SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
				SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
				SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
				SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
				SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
				SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
				SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
				SOarray["custparam_noofrecords"] = SOSearchResults.length;		
				SOarray["name"] =  SOSearchResult.getValue('name');
				SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
				SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
				SOarray["custparam_fastpick"] = fastpick;
				SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
				if(vZoneId!=null && vZoneId!="")
				{
					SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
				}
				else
					SOarray["custparam_ebizzoneno"] = '';
				getBeginLocation = SOSearchResult.getText('custrecord_actbeginloc');

			}


			nlapiLogExecution('Error', 'Name',  SOarray["name"]);	
			var vClusterNo = request.getParameter('hdnClusterNo');

			// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
			// to the previous screen.
			var optedEvent = request.getParameter('cmdPrevious');
			var optskipEvent = request.getParameter('hdnSkipflag');

			//	if the previous button 'F7' is clicked, it has to go to the previous screen 
			//  ie., it has to go to accept SO #.
			if (optedEvent == 'F7') {					
				SOarray["custparam_venterSO"]=vSOId;
				SOarray["custparam_venterFO"]=OrdNo;
				SOarray["custparam_venterwave"]=WaveNo;
				SOarray["custparam_venterzone"]=vZoneId;
				SOarray["custparam_fastpick"] = fastpick;			
				SOarray["custparam_ventercarton"] = getContainerLpNo;
				response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, SOarray);
			}
			else if(optskipEvent == 'F12')
			{
				var vPickType=request.getParameter('hdnpicktype');
				nlapiLogExecution('ERROR', 'vPickType',vPickType);
				nlapiLogExecution('ERROR', 'getWaveNo',getWaveNo);

				var RecCount;
				var SOFilters = new Array();
				SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));
				if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && getWaveNo != null && getWaveNo != "")
				{
					SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(getWaveNo)));
				}
				if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
				{
					nlapiLogExecution('ERROR', 'ClusNo inside If', vClusterNo);
					SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
				}
				if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
				{
					nlapiLogExecution('ERROR', 'OrdNo inside If', OrdName);
					SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
				}
				if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && vSOId!=null && vSOId!="" && vSOId!= "null")
				{
					nlapiLogExecution('ERROR', 'SO Id inside If', vSOId);
					SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vSOId));
				}
				if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
				{
					nlapiLogExecution('ERROR', 'Zone # inside If', vZoneId);
					SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
				}
				if((vPickType.indexOf('C') != -1 || vPickType=='ALL') && getContainerLpNo!=null && getContainerLpNo!="" && getContainerLpNo!= "null")
				{
					nlapiLogExecution('DEBUG', 'Carton # inside If', getContainerLpNo);
					SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getContainerLpNo));			
				}
				SOarray["custparam_ebizordno"] = vSOId;
				var SOColumns = new Array();
				SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
//				SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
				//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
				SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
				//Code end as on 290414
				SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
				SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
				SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
				SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
				SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
				SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
				SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
				SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
				SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
				SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
				SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
				SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
				SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
				SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
				SOColumns.push(new nlobjSearchColumn('name'));
				SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
				SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
				SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
				SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
				SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));

				SOColumns[0].setSort();
				SOColumns[1].setSort();
				SOColumns[2].setSort();
				SOColumns[3].setSort();
				SOColumns[4].setSort();
				SOColumns[5].setSort(true);

				var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
				nlapiLogExecution('ERROR', 'Results', SOSearchResults);
				if (SOSearchResults != null && SOSearchResults.length > 0) {
					nlapiLogExecution('ERROR', 'SOSearchResults.length', SOSearchResults.length);
					nlapiLogExecution('ERROR', 'vSkipId', vSkipId);

					vSkipId=0;

					if(SOSearchResults.length <= parseInt(vSkipId))
					{
						vSkipId=0;
					}
					nlapiLogExecution('ERROR', 'SearchResults of given Wave', getWaveno);
					var SOSearchResult = SOSearchResults[parseInt(vSkipId)];
					if(SOSearchResults.length > parseInt(vSkipId) + 1)
					{
						var SOSearchnextResult = SOSearchResults[parseInt(vSkipId) + 1];
						SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
						SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
						SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
						SOarray["custparam_recordinternalid"] = SOSearchnextResult.getId();
						SOarray["name"] =  SOSearchnextResult.getValue('name');
						SOarray["custparam_ebizordno"] =  SOSearchnextResult.getValue('custrecord_ebiz_order_no');
						nlapiLogExecution('ERROR', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
						nlapiLogExecution('ERROR', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
						nlapiLogExecution('ERROR', 'Next Item Qty', SOSearchnextResult.getValue('custrecord_expe_qty'));
					}
					else
					{
						var SOSearchnextResult = SOSearchResults[0];
						SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
						SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
						SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
						SOarray["custparam_recordinternalid"] = SOSearchnextResult.getId();
						SOarray["name"] =  SOSearchnextResult.getValue('name');
						SOarray["custparam_ebizordno"] =  SOSearchnextResult.getValue('custrecord_ebiz_order_no');
						nlapiLogExecution('ERROR', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
						nlapiLogExecution('ERROR', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
					}
					nlapiLogExecution('ERROR', 'SearchResults of given Wave', SOSearchResult.getValue('custrecord_lpno'));
					nlapiLogExecution('ERROR', 'Qty', SOSearchResult.getValue('custrecord_expe_qty'));
					nlapiLogExecution('ERROR', 'SearchResults of given Wave', getWaveno);
					SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
					nlapiLogExecution('ERROR', 'SearchResults Rec Id', SOarray["custparam_recordinternalid"]);
					getRecordInternalId = SOSearchResult.getId();
					SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
					SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');					
					SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
					SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
					SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
					SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
					SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
					SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
					SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
					SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
					SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
					SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
					SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
					SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
					SOarray["custparam_noofrecords"] = SOSearchResults.length;		
					//SOarray["name"] =  SOSearchResult.getValue('name');
					SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
					//SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
					//SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
					getBeginLocation = SOSearchResult.getText('custrecord_actbeginloc');
					SOarray["custparam_fastpick"] = fastpick;
					SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
					if(vZoneId!=null && vZoneId!="")
					{
						SOarray["custparam_ebizzoneno"] =  SOSearchResults[parseInt(vSkipId)].getValue('custrecord_ebizzone_no');
					}
					else
						SOarray["custparam_ebizzoneno"] = '';
				}

				var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',getRecordInternalId,'custrecord_skiptask');
				if(skiptask==null || skiptask=='')
				{
					skiptask=1;
				}
				else
				{
					skiptask=parseInt(skiptask)+1;
				}

				nlapiLogExecution('ERROR', 'skiptask',skiptask);

				nlapiSubmitField('customrecord_ebiznet_trn_opentask', getRecordInternalId, 'custrecord_skiptask', skiptask);

				vSkipId= parseInt(vSkipId) + 1;
				SOarray["custparam_skipid"] = vSkipId;	 
				nlapiLogExecution('ERROR', 'SOarray["custparam_nextiteminternalid"]', SOarray["custparam_nextiteminternalid"]);
				nlapiLogExecution('ERROR', 'SOarray["custparam_nextlocation"]', SOarray["custparam_nextlocation"]);
				nlapiLogExecution('ERROR', 'getBeginLocation', getBeginLocation);
				nlapiLogExecution('ERROR', 'getItemInternalId', getItemInternalId);

				SOarray["custparam_beginLocationname"]=null;
				SOarray["custparam_iteminternalid"] = SOarray["custparam_nextiteminternalid"];
				SOarray["custparam_expectedquantity"] = SOarray["custparam_nextexpectedquantity"];
				SOarray["custparam_fastpick"] = fastpick;
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, SOarray);

			}

			else {
				//To Remove case sensitive

				if ((getEnteredItem != ''&& getEnteredItem!=null)&& getEnteredItem != getItemName) {

					var actItemidArray=validateSKUId(getEnteredItem,whLocation,'');
					nlapiLogExecution('ERROR', 'After validateSKU1',actItemidArray);
					//var actItemid = actItemidArray[1];
					//nlapiLogExecution('ERROR', 'After validateSKU11',actItemid);
					var trantype = nlapiLookupField('transaction', vSOId, 'recordType');

					nlapiLogExecution('DEBUG', 'trantype',trantype);

					var itemRecordArr = eBiz_RF_GetItemForItemIdWithArr(actItemidArray[1]);
					var rec='';
					if(trantype=='transferorder')
						rec= nlapiLoadRecord('transferorder', vSOId);
					else
						rec= nlapiLoadRecord('salesorder', vSOId);
					// Case# 20148649 ends
					var OrdName=rec.getFieldValue('tranid');

					nlapiLogExecution('DEBUG', 'OrdName',OrdName);

					var soLineDetails ='';
					var actItemid='';
					if(itemRecordArr !=null && itemRecordArr !='')
					{
						soLineDetails = eBiz_RF_GetSOLineDetailsForItemArr(OrdName, itemRecordArr,trantype);


						//case 20140720 start
						if(soLineDetails != null && soLineDetails != "")
						{
							actItemid=soLineDetails[0].getValue('item');
						}
						//case 20140720 end
					}
					nlapiLogExecution('DEBUG', 'After validateSKU1',actItemid);
					if(actItemid!=null && actItemid!="")
					{
						nlapiLogExecution('Error', 'actItemid ', actItemid);
						getEnteredItem=actItemid;

						nlapiLogExecution('Error', 'getEnteredItem ', getEnteredItem);
						nlapiLogExecution('Error', 'getItem ', getItem);
						nlapiLogExecution('Error', 'getItemName ', getItemName);

						var SOFilters = new Array();
						SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));
						if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && getWaveNo != null && getWaveNo != "")
						{
							SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(getWaveNo)));
						}
						if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
						{
							nlapiLogExecution('ERROR', 'ClusNo inside If', vClusterNo);
							SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
						}
						if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdNo!=null && OrdNo!="" && OrdNo!= "null")
						{
							nlapiLogExecution('ERROR', 'OrdNo inside If', OrdNo);
							SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdNo));
						}
						if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && vSOId!=null && vSOId!="" && vSOId!= "null")
						{
							nlapiLogExecution('ERROR', 'SO Id inside If', vSOId);
							SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vSOId));
						}
						if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
						{
							nlapiLogExecution('ERROR', 'Zone # inside If', vZoneId);
							SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
						}

						SOarray["custparam_ebizordno"] =vSOId;
						var SOColumns = new Array();
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
//						SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
						//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
						SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
						//Code end as on 290414
						SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
						SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
						SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
						SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
						SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
						SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
						SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
						SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
						SOColumns.push(new nlobjSearchColumn('name'));
						SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
						SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
						SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));

						SOColumns[0].setSort();
						SOColumns[1].setSort();
						SOColumns[2].setSort();
						SOColumns[3].setSort();
						SOColumns[4].setSort();
						SOColumns[5].setSort(true);

						var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

						if (SOSearchResults != null && SOSearchResults.length > 0) {

							vSkipId = 0;

							if(SOSearchResults.length <= parseFloat(vSkipId))
							{
								vSkipId=0;
							}

							SOarray["custparam_recordinternalid"] = SOSearchResults[parseFloat(vSkipId)].getId();
							nlapiLogExecution('ERROR', 'vSkipId Rec Id', SOarray["custparam_recordinternalid"]);
							//SOarray["custparam_expectedquantity"] = SOSearchResults[parseFloat(vSkipId)].getValue('custrecord_expe_qty');
							SOarray["custparam_invoicerefno"] = SOSearchResults[parseFloat(vSkipId)].getValue('custrecord_invref_no');
							SOarray["custparam_batchno"] = SOSearchResults[parseFloat(vSkipId)].getValue('custrecord_batch_no');
							SOarray["custparam_ebizordno"] =  SOSearchResults[parseFloat(vSkipId)].getValue('custrecord_ebiz_order_no');
							if(vZoneId!=null && vZoneId!="")
							{
								SOarray["custparam_ebizzoneno"] =  SOSearchResults[parseFloat(vSkipId)].getValue('custrecord_ebizzone_no');
							}
							else
								SOarray["custparam_ebizzoneno"] = '';						
						}
					}
				}
				else if(getEnteredItem == getItemName)
				{
					nlapiLogExecution('ERROR', 'getEnteredItem', getEnteredItem);
					nlapiLogExecution('ERROR', 'getItemInternalId', getItemInternalId);
					getEnteredItem = getItemInternalId;
				}	
				nlapiLogExecution('Error', 'getEnteredItem ', getEnteredItem);
				nlapiLogExecution('Error', 'getItem ', getItem);
				nlapiLogExecution('Error', 'getItemName ', getItemName);

				if ((getEnteredItem != ''&& getEnteredItem!=null) && getEnteredItem == getItemInternalId) {
					var ItemTypeRec = nlapiLookupField('item', getItemInternalId, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);				
					var batchflag = ItemTypeRec.custitem_ebizbatchlot;
					var ItemType = ItemTypeRec.recordType;

					nlapiLogExecution('ERROR', 'getWaveNo', getWaveNo);
					nlapiLogExecution('ERROR', 'vClusterNo', vClusterNo);
					nlapiLogExecution('ERROR', 'vSOId', vSOId);
					nlapiLogExecution('ERROR', 'getItemInternalId', getItemInternalId);
					nlapiLogExecution('ERROR', 'vPickType', vPickType);

					var SOFilters = new Array();

					SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

					if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && getWaveNo != null && getWaveNo != "")
					{
						nlapiLogExecution('ERROR', 'WaveNo inside If', getWaveNo);
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(getWaveNo)));
					}

					if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
					{
						nlapiLogExecution('ERROR', 'ClusNo inside If', vClusterNo);
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
					}

					if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdNo!=null && OrdNo!="" && OrdNo!= "null")
					{
						nlapiLogExecution('ERROR', 'OrdNo inside If', OrdNo);
						SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdNo));
					}

					if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && vSOId!=null && vSOId!="" && vSOId!= "null")
					{
						nlapiLogExecution('ERROR', 'SO Id inside If', vSOId);
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vSOId));
					}

					if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!="null")
					{
						nlapiLogExecution('ERROR', 'Zone # inside If', vZoneId);
						SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));
					}

					if((vPickType.indexOf('C') != -1 || vPickType=='ALL') && getContainerLpNo!=null && getContainerLpNo!="" && getContainerLpNo!= "null") 
					{
						nlapiLogExecution('ERROR', 'Carton # inside If', getContainerLpNo);
						SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getContainerLpNo));			
					}


					var SOColumns = new Array();
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
//					SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
					//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
					SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
					//Code end as on 290414
					SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
					SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
					SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
					SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
					SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
					SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
					SOColumns.push(new nlobjSearchColumn('name'));
					SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
					SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
					SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));

					SOColumns[0].setSort();
					SOColumns[1].setSort();
					SOColumns[2].setSort();
					SOColumns[3].setSort();
					SOColumns[4].setSort();
					SOColumns[5].setSort(true);

					var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

					if (SOSearchResults != null && SOSearchResults.length > 0) {
						nlapiLogExecution('ERROR', 'SOSearchResults.length', SOSearchResults.length);
						nlapiLogExecution('ERROR', 'vSkipId', vSkipId);
						vSkipId=0;
						if(SOSearchResults.length <= parseFloat(vSkipId))
						{
							vSkipId=0;
						}
						nlapiLogExecution('ERROR', 'SOSearchResults count', SOSearchResults.length);
						SOarray["custparam_recordinternalid"] = SOSearchResults[parseFloat(vSkipId)].getId();
						//nlapiLogExecution('ERROR', 'SOSearchResults.get id', SOarray["custparam_recordinternalid"]);
						//SOarray["custparam_expectedquantity"] = SOSearchResults[parseFloat(vSkipId)].getValue('custrecord_expe_qty');
						//nlapiLogExecution('ERROR', 'Skip Next Qty', SOarray["custparam_expectedquantity"]);
						SOarray["custparam_invoicerefno"] = SOSearchResults[parseFloat(vSkipId)].getValue('custrecord_invref_no');
						SOarray["custparam_batchno"] = SOSearchResults[parseFloat(vSkipId)].getValue('custrecord_batch_no');
						SOarray["custparam_ebizordno"] =  SOSearchResults[parseFloat(vSkipId)].getValue('custrecord_ebiz_order_no');
						SOarray["custparam_itemstatus"] =  SOSearchResults[parseFloat(vSkipId)].getValue('custrecord_sku_status');
						if(vZoneId!=null && vZoneId!="")
						{
							SOarray["custparam_ebizzoneno"] =  SOSearchResults[parseFloat(vSkipId)].getValue('custrecord_ebizzone_no');
						}
						else
							SOarray["custparam_ebizzoneno"] = '';

						if(SOSearchResults.length > parseFloat(vSkipId) + 1)
						{
							var SOSearchnextResult = SOSearchResults[parseFloat(vSkipId) + 1];
							SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
							SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
							SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
							//SOarray["custparam_recordinternalid"] = SOSearchnextResult.getId();
							nlapiLogExecution('ERROR', 'SOSearchResults.get id', SOarray["custparam_recordinternalid"]);
							nlapiLogExecution('ERROR', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
							nlapiLogExecution('ERROR', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
							nlapiLogExecution('ERROR', 'Next Item Qty', SOSearchnextResult.getValue('custparam_nextexpectedquantity'));
						}

					}


					nlapiLogExecution('ERROR', 'ItemType', ItemType);
					//If Lotnumbered item the navigate to batch # scan
					if (getEnteredLocation != '' && getEnteredLocation.toUpperCase() == getBeginLocation.toUpperCase()) {


						var binFilters = new Array();
						var binColumns = new Array();

						binFilters.push(new nlobjSearchFilter('name', null, 'is', getEnteredLocation));
						binFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

						var LocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null, binFilters,binColumns);

						nlapiLogExecution('Error', 'Length of Location Search', LocationSearch.length);
						var getEndLocationInternalId;
						if (LocationSearch.length != 0) {
							for (var s = 0; s < LocationSearch.length; s++) {
								getEndLocationInternalId = LocationSearch[s].getId();
								nlapiLogExecution('DEBUG', 'End Location Id', getEndLocationInternalId);
							}

							SOarray["custparam_beginlocationname"] = getBeginLocation;
							SOarray["custparam_endlocinternalid"] = getEndLocationInternalId;
							SOarray["custparam_endlocation"] = getBeginLocation;
							SOarray["custparam_clusterno"] = vClusterNo;	
							nlapiLogExecution('Error', 'getBeginLocation',SOarray["custparam_beginLocation"]);
							var getBeginLocationid=	SOarray["custparam_beginLocation"];
							var wave=request.getParameter('hdnWaveNo');
							nlapiLogExecution('Error', 'wave', request.getParameter('hdnWaveNo'));

							SOarray["custparam_remqty"]=0;

							/*var invtholdflag = IsInvtonHold(getItemInternalId,getEndLocationInternalId,getInvoiceRefNo);
							if(invtholdflag=='T')
							{
								SOarray["custparam_screenno"] = '13';	
								SOarray["custparam_error"]='CYCLE COUNT IS IN PROGRESS FOR THIS LOCATION. PLEASE DO THE LOCATION EXCEPTION TO COMPLETE THIS PICK';
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								return;
							}*/


							/*	if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem" ||batchflag=='T')
							{
								SOarray["custparam_itemType"] ='lotnumberedinventoryitem';
								response.sendRedirect('SUITELET', 'customscript_rf_picking_batch', 'customdeploy_rf_picking_batch_di', false, SOarray);					  
							}
							else {*/
							nlapiLogExecution('Error', 'Navigating to Confirm carton screen', 'Success');
							nlapiLogExecution('Error', 'Recordinternalid', SOarray["custparam_recordinternalid"]);
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pickingcarton', 'customdeploy_ebiz_rf_pickingcarton', false, SOarray);
							nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
							//}					
							nlapiLogExecution('Error', 'Done customrecord', 'Success');
						}
						else {
							SOarray["custparam_beginlocationname"]=NextShowLocation;
							SOarray["custparam_nextlocation"]=NextShowLocation;	
							SOarray["custparam_nextiteminternalid"]=NextShowItemId;	
							nlapiLogExecution('Error', 'NextShowLocation : ', NextShowLocation);
							SOarray["custparam_fastpick"] = fastpick;
							nlapiLogExecution('Error', 'Error: ', 'Scanned a wrong location');
							nlapiLogExecution('Error', 'NextShowItemId : ', NextShowItemId);
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);

						}
					}
					else if (getEnteredLocation != '' && getEnteredLocation.toUpperCase() != getBeginLocation.toUpperCase()) {
						var vlocationfound='F';
						if (SOSearchResults != null && SOSearchResults.length > 0) {
							for (var l = 0; vlocationfound=='F' && l < SOSearchResults.length; l++) {

								var actBeginLocation = SOSearchResults[l].getText('custrecord_actbeginloc');
								//To Remove case sensitive
								nlapiLogExecution('Error', 'getEnteredLocation : ', getEnteredLocation.toUpperCase());
								nlapiLogExecution('Error', 'actBeginLocation : ', actBeginLocation.toUpperCase());

								if((getEnteredLocation.toUpperCase()==actBeginLocation.toUpperCase()) && (vlocationfound=='F')){

									vlocationfound='T';

									var pikgenresults=SOSearchResults[l];

									SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
									SOarray["custparam_recordinternalid"] = pikgenresults.getId();
									nlapiLogExecution('ERROR', 'pikgenresults Rec Id', SOarray["custparam_recordinternalid"]);
									SOarray["custparam_containerlpno"] = pikgenresults.getValue('custrecord_lpno');
									SOarray["custparam_expectedquantity"] = pikgenresults.getValue('custrecord_expe_qty');
									SOarray["custparam_beginLocation"] = pikgenresults.getValue('custrecord_actbeginloc');
									SOarray["custparam_item"] = pikgenresults.getValue('custrecord_sku');
									SOarray["custparam_itemname"] = pikgenresults.getText('custrecord_sku');
									SOarray["custparam_itemdescription"] = pikgenresults.getValue('custrecord_skudesc');
									SOarray["custparam_iteminternalid"] = pikgenresults.getValue('custrecord_ebiz_sku_no');
									SOarray["custparam_dolineid"] = pikgenresults.getValue('custrecord_ebiz_cntrl_no');
									SOarray["custparam_invoicerefno"] = pikgenresults.getValue('custrecord_invref_no');
									SOarray["custparam_orderlineno"] = pikgenresults.getValue('custrecord_line_no');
									SOarray["custparam_beginLocationname"] = pikgenresults.getText('custrecord_actbeginloc');
									SOarray["custparam_beginlocationname"] = pikgenresults.getText('custrecord_actbeginloc');
									SOarray["custparam_batchno"] = pikgenresults.getValue('custrecord_batch_no');
									SOarray["custparam_whlocation"] = pikgenresults.getValue('custrecord_wms_location');
									SOarray["custparam_whcompany"] = pikgenresults.getValue('custrecord_comp_id');
									SOarray["custparam_noofrecords"] = SOSearchResults.length;		
									SOarray["name"] =  pikgenresults.getValue('name');
									SOarray["custparam_containersize"] =  pikgenresults.getValue('custrecord_container');
									SOarray["custparam_ebizordno"] =  pikgenresults.getValue('custrecord_ebiz_order_no');
									SOarray["custparam_fastpick"] = fastpick;
									getBeginLocation = pikgenresults.getText('custrecord_actbeginloc');

									var vNextRecs=0;

									nlapiLogExecution('Error', 'Loop # : ', l+1);
									nlapiLogExecution('Error', 'SOSearchResults Length : ', SOSearchResults.length);

									if(l+1 < SOSearchResults.length)
										vNextRecs=l+1;
									else 
										vNextRecs=0;

									nlapiLogExecution('Error', 'vNextRecs : ', vNextRecs);

									if(SOSearchResults[vNextRecs].getText('custrecord_actbeginloc') != null && SOSearchResults[vNextRecs].getText('custrecord_actbeginloc') !='')
										SOarray["custparam_nextlocation"] = SOSearchResults[vNextRecs].getText('custrecord_actbeginloc');
									else
										SOarray["custparam_nextlocation"]='';

									if(SOSearchResults[vNextRecs].getValue('custrecord_ebiz_sku_no') != null && SOSearchResults[vNextRecs].getValue('custrecord_ebiz_sku_no') != '')	
										SOarray["custparam_nextiteminternalid"] = SOSearchResults[vNextRecs].getValue('custrecord_ebiz_sku_no');
									else
										SOarray["custparam_nextiteminternalid"]='';

									if(SOSearchResults[vNextRecs].getValue('custrecord_expe_qty') != null && SOSearchResults[vNextRecs].getValue('custrecord_expe_qty') != '')
										SOarray["custparam_nextexpectedquantity"] = SOSearchResults[vNextRecs].getValue('custrecord_expe_qty');
									else
										SOarray["custparam_nextexpectedquantity"]=0;

									nlapiLogExecution('ERROR', 'Next Location', SOarray["custparam_nextlocation"]);
									nlapiLogExecution('ERROR', 'Next Item Intr Id', SOarray["custparam_nextiteminternalid"]);

									var binFilters = new Array();
									var binColumns = new Array();

									binFilters.push(new nlobjSearchFilter('name', null, 'is', getEnteredLocation));
									binFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

									var LocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null, binFilters,binColumns);

									if (LocationSearch.length != 0) {
										for (var s = 0; s < LocationSearch.length; s++) {
											var getEndLocationInternalId = LocationSearch[s].getId();
											nlapiLogExecution('Error', 'End Location Id', getEndLocationInternalId);
										}

										SOarray["custparam_beginlocationname"] = getBeginLocation;
										SOarray["custparam_endlocinternalid"] = getEndLocationInternalId;					 
										SOarray["custparam_endlocation"] = actBeginLocation;
										SOarray["custparam_clusterno"] = vClusterNo;	

										for (var t = 0; t < SOSearchResults.length; t++) {
											var actBegLocation = SOSearchResults[t].getText('custrecord_actbeginloc');

											if((getEnteredLocation.toUpperCase()==actBegLocation.toUpperCase())){
												nlapiSubmitField('customrecord_ebiznet_trn_opentask', SOSearchResults[t].getId(), 'custrecord_skiptask', 'Y');
											}

										}
										/*	if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem" ||batchflag=='T')
										{
											SOarray["custparam_itemType"] ='lotnumberedinventoryitem';
											response.sendRedirect('SUITELET', 'customscript_rf_picking_batch', 'customdeploy_rf_picking_batch_di', false, SOarray);					  
										}
										else {*/
										nlapiLogExecution('Error', 'Navigating to Confirm carton screen', 'Success');
										nlapiLogExecution('Error', 'Recordinternalid', SOarray["custparam_recordinternalid"]);
										SOarray["custparam_fastpick"] = fastpick;
										response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pickingcarton', 'customdeploy_ebiz_rf_pickingcarton', false, SOarray);
										return;
										//}					
									}						
								}
							}
							if(vlocationfound=='F'){
								SOarray["custparam_beginlocationname"]=NextShowLocation;
								SOarray["custparam_nextlocation"]=NextShowLocation;	
								SOarray["custparam_nextiteminternalid"]=NextShowItemId;
								SOarray["custparam_fastpick"] = fastpick;

								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							}	
						}
					}
					else 
					{
						SOarray["custparam_beginlocationname"]=NextShowLocation;
						SOarray["custparam_nextlocation"]=NextShowLocation;	
						SOarray["custparam_nextiteminternalid"]=NextShowItemId;
						SOarray["custparam_fastpick"] = fastpick;
						nlapiLogExecution('Error', 'NextShowLocation : ', NextShowLocation);
						nlapiLogExecution('Error', 'Error: ', 'Did not scan the location');
						nlapiLogExecution('Error', 'NextShowItemId : ', NextShowItemId);
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);				
					}
				}
				else 
				{
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					nlapiLogExecution('DEBUG', 'Error: ', 'Did not scan the location');
				}
			}
		}
		else
		{

			nlapiLogExecution('ERROR', 'Into ON-DEMAND Stage');
			nlapiLogExecution('ERROR', 'OrdNo',OrdNo);
			nlapiLogExecution('ERROR', 'vZoneId',vZoneId);
			nlapiLogExecution('ERROR', 'vPickType',vPickType);

			SOarray["custparam_waveno"] = WaveNo;
			SOarray["custparam_recordinternalid"] = getRecordInternalId;
			nlapiLogExecution('ERROR', 'else Rec Id', SOarray["custparam_recordinternalid"]);
			SOarray["custparam_newcontainerlp"] = getContainerLpNo;
			SOarray["custparam_expectedquantity"] = getExpectedQuantity;
			SOarray["custparam_beginLocation"] = getBeginLocation;
			SOarray["custparam_item"] = getItem;
			SOarray["custparam_itemname"] = getItemName;
			SOarray["custparam_itemdescription"] = getItemDescription;
			SOarray["custparam_iteminternalid"] = getItemInternalId;
			SOarray["custparam_dolineid"] = getDOLineId;
			SOarray["custparam_invoicerefno"] = getInvoiceRefNo;
			SOarray["custparam_orderlineno"] = getOrderLineNo;
			SOarray["custparam_beginLocationname"] = getBeginLocation;
			SOarray["custparam_beginlocationname"] = getBeginLocation;
			SOarray["custparam_batchno"] = vBatchNo;
			SOarray["custparam_whlocation"] = whLocation;
			SOarray["custparam_whcompany"] = whCompany;
			SOarray["custparam_noofrecords"] = RecCount;		
			SOarray["name"] =  OrdNo;
			SOarray["custparam_containersize"] =  ContainerSize;
			SOarray["custparam_ebizordno"] =  vSOId;
			SOarray["custparam_ebizzoneno"] =  vZoneId;
			SOarray["custparam_clusterno"] =  ClusNo;
			SOarray["custparam_fastpick"] = fastpick;

			var SOFilters = new Array();

			SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

			if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && WaveNo != null && WaveNo != "")
			{
				nlapiLogExecution('ERROR', 'WaveNo inside If', ClusNo);
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(WaveNo)));
			}

			if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
			{
				nlapiLogExecution('ERROR', 'ClusNo inside If', ClusNo);
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
			}

			if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdNo!=null && OrdNo!="" && OrdNo!= "null")
			{
				nlapiLogExecution('ERROR', 'OrdNo inside If', OrdNo);
				SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdNo));
			}

			if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && vSOId!=null && vSOId!="" && vSOId!= "null")
			{
				nlapiLogExecution('ERROR', 'SO Id inside If', vSOId);
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vSOId));
			}

			if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!="null")
			{
				nlapiLogExecution('ERROR', 'Zone # inside If', vZoneId);
				SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));
			}
			if((vPickType.indexOf('C') != -1 || vPickType=='ALL') && getContainerLpNo!=null && getContainerLpNo!="" && getContainerLpNo!= "null")
			{
				nlapiLogExecution('DEBUG', 'Carton # inside If', getContainerLpNo);
				SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getContainerLpNo));			
			}

			var SOColumns = new Array();
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
//			SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
			//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
			SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
			//Code end as on 290414
			SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
			SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
			SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
			SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
			SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
			SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
			SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
			SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
			SOColumns.push(new nlobjSearchColumn('name'));
			SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
			SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));

			SOColumns[0].setSort();
			SOColumns[1].setSort();
			SOColumns[2].setSort();
			SOColumns[3].setSort();
			SOColumns[4].setSort();
			SOColumns[5].setSort(true);

			var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
			nlapiLogExecution('ERROR', 'Results', SOSearchResults);
			if (SOSearchResults != null && SOSearchResults.length > 0) {

				for (var s = 0; s < SOSearchResults.length; s++) {

					nlapiSubmitField('customrecord_ebiznet_trn_opentask', SOSearchResults[s].getId(),'custrecord_taskassignedto', '');					
				}
			}

			SOarray["custparam_ondemandstage"] = 'Y';
			nlapiLogExecution('ERROR', 'Navigating To', 'Stage Location');
			response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);

		}
	}
}
//Case # 20148334 start
function IsInvtonHold(ItemInternalId,EndLocInternalId,getInvoiceRefNo)
{
	nlapiLogExecution('ERROR', 'Into IsInvtonHold');
	nlapiLogExecution('ERROR', 'ItemInternalId', ItemInternalId);
	nlapiLogExecution('ERROR', 'EndLocInternalId', EndLocInternalId);

	var holdflag='F';


	var SOFilters = new Array();
	var SOColumns = new Array();
	if(getInvoiceRefNo == null || getInvoiceRefNo=="" || getInvoiceRefNo=="null" || getInvoiceRefNo=="undefined")
	{
		nlapiLogExecution('ERROR', 'into if',getInvoiceRefNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', ItemInternalId)); //Status Flag - Picks Generated
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', EndLocInternalId)); // Task Type - PICK	 
		SOFilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [19]));
	}
	else
	{
		nlapiLogExecution('ERROR', 'into else',getInvoiceRefNo);
		SOFilters.push(new nlobjSearchFilter('internalid', null, 'is', getInvoiceRefNo)); 
	}
	SOColumns[0] = new nlobjSearchColumn('custrecord_ebiz_cycl_count_hldflag' ); 
	SOColumns[1] = new nlobjSearchColumn('custrecord_ebiz_invholdflg' ); 

	var  searchresults = nlapiSearchRecord( 'customrecord_ebiznet_createinv', null, SOFilters, SOColumns );


	if(searchresults!=null && searchresults!='')
	{
		for(var z=0; z<searchresults.length;z++) 
		{										
			var cyclholdflag=searchresults[z].getValue('custrecord_ebiz_cycl_count_hldflag');
			var invtholdflag=searchresults[z].getValue('custrecord_ebiz_invholdflg');

			if(invtholdflag=='T')
			{
				holdflag='T';
				nlapiLogExecution('ERROR', 'Out of IsInvtonHold',holdflag);
				return holdflag;
			}
		}
	}

	nlapiLogExecution('ERROR', 'Out of IsInvtonHold',holdflag);

	return holdflag;	
}


function eBiz_RF_GetSOLineDetailsForItemArr(poID, itemIDArr,trantype){
	nlapiLogExecution('ERROR', 'eBiz_RF_GetPOLineDetailsForItemArr', 'Start');
	var logMsg = 'PO ID = ' + poID + '<br>';
	logMsg = logMsg + 'Item ID = ' + itemIDArr;
	logMsg = logMsg + 'trantype = ' + trantype;
	nlapiLogExecution('ERROR', 'Input Parameters', logMsg);


	//alert('Order Id ' + poID);
	//alert('itemIDArr ' + itemIDArr);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('tranid', null, 'is', poID));
	filters.push(new nlobjSearchFilter('item', null, 'anyof', itemIDArr));
	if(trantype=='salesorder')
		filters.push(new nlobjSearchFilter('custrecord_ebizwhsite', 'location', 'is', 'T'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('item');
	columns[1] = new nlobjSearchColumn('line');
	columns[2] = new nlobjSearchColumn('custcol_ebiz_po_cube');
	columns[3] = new nlobjSearchColumn('location');
	columns[4] = new nlobjSearchColumn('quantity');
	columns[5] = new nlobjSearchColumn('quantityshiprecv');

	var SOLineSearchResults = nlapiSearchRecord(trantype, null, filters, columns);

	if(SOLineSearchResults != null && SOLineSearchResults.length > 0)
		nlapiLogExecution('ERROR', 'No. of PO Lines Retrieved', SOLineSearchResults.length);

	nlapiLogExecution('ERROR', 'eBiz_RF_GetPOLineDetailsForItemArr', 'End');

	return SOLineSearchResults;
}