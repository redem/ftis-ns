/***************************************************************************
	  		   eBizNET Solutions Inc .
 ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_WaveQb_SL.js,v $
 *     	   $Revision: 1.12.2.4.4.1.4.1 $
 *     	   $Date: 2013/09/11 12:18:51 $
 *     	   $Author: rmukkera $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_WaveQb_SL.js,v $
 * Revision 1.12.2.4.4.1.4.1  2013/09/11 12:18:51  rmukkera
 * Case# 20124374
 *
 * Revision 1.12.2.4.4.1  2012/11/01 14:55:02  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.12.2.4  2012/02/20 15:24:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.12.2.3  2012/02/13 13:53:27  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge
 *
 * Revision 1.12.2.2  2012/02/08 12:35:51  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge to brach
 *
 * Revision 1.15  2012/02/01 05:57:28  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fix is merged from  1.12.2.1
 *
 * Revision 1.14  2012/01/13 16:50:36  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Generation User Event changes
 *
 * Revision 1.13  2012/01/12 13:22:37  spendyala
 * CASE201112/CR201113/LOG201121
 * added wmsStatus flag field to the form.
 *
 * Revision 1.12  2012/01/06 13:00:50  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Added Remarks field to pick report to show failure for picks.
 *
 * Revision 1.11  2012/01/05 14:01:49  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code tunning
 *
 * Revision 1.10  2011/12/30 00:11:07  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.9  2011/11/15 09:05:44  spendyala
 * CASE201112/CR201113/LOG201121
 * added ship date fields
 *
 * Revision 1.54  2011/11/02 11:09:36  snimmakayala
 * CASE201112/CR201113/LOG201121
 * 
 *****************************************************************************/
function fillfulfillorderField(form, FulfillOrderField,shipmentField,maxno){
	var filtersso = new Array();		
	//filtersso[0] = new nlobjSearchFilter( 'custrecord_ord_qty', null, 'greaterthan', '0' );
	//15-Selected Into Wave, 25-Edit, 26-Picks Failed
	filtersso.push(new nlobjSearchFilter( 'custrecord_linestatus_flag', null, 'anyof', [15,25,26]));
	filtersso.push(new nlobjSearchFilter( 'custrecord_lineord', null, 'isnotempty'));
	if(maxno!=-1)
	{
		//filtersso.push(new nlobjSearchFilter('internalid', null, 'lessthan', parseFloat(maxno)));
		filtersso.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno)));
	}
	 
	 var columnsinvt = new Array();
	columnsinvt[0] = new nlobjSearchColumn('id');	 
	columnsinvt[1] = new nlobjSearchColumn('custrecord_lineord');
	columnsinvt[0].setSort(true);
	columnsinvt[2] = new nlobjSearchColumn('custrecord_shipment_no');
	//columnsinvt[2] = new nlobjSearchColumn('custrecord_route_no');
	
    
	


	FulfillOrderField.addSelectOption("", "");
	shipmentField.addSelectOption("","");

	var customerSearchResults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filtersso,columnsinvt);
	 
	for (var i = 0; customerSearchResults != null && i < customerSearchResults.length; i++) {

		//nlapiLogExecution('ERROR', 'tranid', customerSearchResults[i].getValue('tranid')); 
		if(customerSearchResults[i].getValue('custrecord_lineord') != null && customerSearchResults[i].getValue('custrecord_lineord') != "" && customerSearchResults[i].getValue('custrecord_lineord') != " ")
		{
			var resdo = form.getField('custpage_qbso').getSelectOptions(customerSearchResults[i].getValue('custrecord_lineord'), 'is');
			if (resdo != null) {
				if (resdo.length > 0) {
					continue;
				}
			}
		}
		FulfillOrderField.addSelectOption(customerSearchResults[i].getValue('custrecord_lineord'), customerSearchResults[i].getValue('custrecord_lineord'));
	}
	for (var j = 0; customerSearchResults != null && j < customerSearchResults.length; j++) {

		//nlapiLogExecution('ERROR', 'tranid', customerSearchResults[i].getValue('tranid')); 
		if(customerSearchResults[j].getValue('custrecord_shipment_no') != null && customerSearchResults[j].getValue('custrecord_shipment_no') != "" && customerSearchResults[j].getValue('custrecord_shipment_no') != " ")
		{
			var resdo2 = form.getField('custpage_qbshipmentno').getSelectOptions(customerSearchResults[j].getValue('custrecord_shipment_no'), 'is');
			if (resdo2 != null) {
				if (resdo2.length > 0) {
					continue;
				}
			}
			shipmentField.addSelectOption(customerSearchResults[j].getValue('custrecord_shipment_no'), customerSearchResults[j].getValue('custrecord_shipment_no'));
		}
		
	}
	
	if(customerSearchResults!=null && customerSearchResults.length>=1000)
	{
//		var columnsinvt = new Array();
//		columnsinvt[0] = new nlobjSearchColumn('id');	 
//		columnsinvt[1] = new nlobjSearchColumn('custrecord_lineord');
//		columnsinvt[0].setSort(true);
//		columnsinvt[2] = new nlobjSearchColumn('custrecord_shipment_no');
//
//		var OrderSearchResults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filtersso,columnsinvt);

		var maxno=customerSearchResults[customerSearchResults.length-1].getValue('id');		
		fillfulfillorderField(form, FulfillOrderField,shipmentField,maxno);	

	}
}
function WaveQbSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Wave Generation');


		var soField = form.addField('custpage_qbso', 'select', 'Fulfillment Ord#');
		soField.setLayoutType('startrow', 'none');  		
		var shipmentField = form.addField('custpage_qbshipmentno', 'select', 'Shipment #');
		shipmentField .setLayoutType('startrow', 'none');
		fillfulfillorderField(form, soField,shipmentField,-1);
		/*var filtersso = new Array();		
		filtersso[0] = new nlobjSearchFilter( 'custrecord_ord_qty', null, 'greaterthan', '0' );
		//15-Selected Into Wave, 25-Edit, 26-Picks Failed
		filtersso[1] = new nlobjSearchFilter( 'custrecord_linestatus_flag', null, 'anyof', [15,25,26]);
		soField.addSelectOption("","");
		var columnsinvt = new Array();
		columnsinvt[0] = new nlobjSearchColumn('custrecord_lineord');
		columnsinvt[0].setSort();
		columnsinvt[1] = new nlobjSearchColumn('custrecord_shipment_no');
		//columnsinvt[2] = new nlobjSearchColumn('custrecord_route_no');

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filtersso,columnsinvt);

		for (var i = 0; searchresults != null && i < searchresults.length; i++) {
			//Searching for Duplicates		
			var res=  form.getField('custpage_qbso').getSelectOptions(searchresults[i].getValue('custrecord_lineord'), 'is');
			if (res != null) {
				nlapiLogExecution('ERROR', 'res.length', res.length +searchresults[i].getValue('custrecord_lineord') );
				if (res.length > 0) {
					continue;
				}
			}		
			soField.addSelectOption(searchresults[i].getValue('custrecord_lineord'), searchresults[i].getValue('custrecord_lineord'));
		}*/		
//		Shipment #
		
		/*shipmentField.addSelectOption("","");
		for (var i = 0; searchresults != null && i < searchresults.length; i++) {

			if(searchresults[i].getValue('custrecord_shipment_no')!=null && searchresults[i].getValue('custrecord_shipment_no')!="")
			{
				//Searching for Duplicates		
				var res=  form.getField('custpage_qbshipmentno').getSelectOptions(searchresults[i].getValue('custrecord_shipment_no'), 'is');
				if (res != null) {
					nlapiLogExecution('ERROR', 'res.length', res.length +searchresults[i].getValue('custrecord_shipment_no') );
					if (res.length > 0) {
						continue;
					}
				}		
				shipmentField.addSelectOption(searchresults[i].getValue('custrecord_shipment_no'), searchresults[i].getValue('custrecord_shipment_no'));
			}  
		}*/


		//Route #
		var RouteField = form.addField('custpage_qbrouteno', 'select', 'Route','customlist_ebiznetroutelov');
		RouteField.setLayoutType('startrow', 'none');


		var soOrdPriority = form.addField('custpage_ordpriority', 'select', 'Ord Priority','customlist_ebiznet_order_priority');
		soOrdPriority.setLayoutType('startrow', 'none'); 		

		var soOrdType = form.addField('custpage_ordtype', 'select', 'Order Type','customrecord_ebiznet_order_type');
		soOrdType.setLayoutType('startrow', 'none');

		var Consignee = form.addField('custpage_consignee', 'select', 'Consignee','customer');
		Consignee.setLayoutType('startrow', 'none');

		//var consigneeid=getFieldValue('custpage_consignee');
		//nlapiLogExecution('ERROR','CONSIGNEEID',consigneeid);


		var itemfamily = form.addField('custpage_itemfamily', 'select', 'Item Family','customrecord_ebiznet_sku_family');

		var itemgroup = form.addField('custpage_itemgroup', 'select', 'Item Group','customrecord_ebiznet_sku_group');

		var item = form.addField('custpage_item', 'select', 'Item','inventoryitem');	

		var iteminfo1= form.addField('custpage_iteminfoone', 'select', 'Item Info1');
		iteminfo1.addSelectOption("","");
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custitem_item_info_1');			 
		var filters= new Array();
		//filters.push(new nlobjSearchFilter('custitem_item_info_1', null, 'isnotempty'));	
		filters.push(new nlobjSearchFilter('custitem_item_info_1', null, 'noneof',['@NONE@']));	
		//Searchng Duplicates
		var result= nlapiSearchRecord('item',null,filters,columns);
		for (var i = 0; result != null && i < result.length; i++) {

			var res=  form.getField('custpage_iteminfoone').getSelectOptions(result[i].getValue('custitem_item_info_1'), 'is');
			if (res != null) {						
				if (res.length > 0) {
					continue;
				}
			}

			iteminfo1.addSelectOption(result[i].getValue('custitem_item_info_1'),result[i].getValue('custitem_item_info_1'));
		}

		var iteminfo2= form.addField('custpage_iteminfotwo', 'select', 'Item Info2');
		iteminfo2.addSelectOption("","");
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custitem_item_info_2');			 
		var filters= new Array();
		//filters.push(new nlobjSearchFilter('custitem_item_info_2', null, 'isnotempty'));	
		filters.push(new nlobjSearchFilter('custitem_item_info_2', null, 'noneof',['@NONE@']));	
		var result= nlapiSearchRecord('item',null,filters,columns);
		for (var i = 0; result != null && i < result.length; i++) {

			var res=  form.getField('custpage_iteminfotwo').getSelectOptions(result[i].getValue('custitem_item_info_2'), 'is');
			if (res != null) {						
				if (res.length > 0) {
					continue;
				}
			}

			iteminfo2.addSelectOption(result[i].getValue('custitem_item_info_2'),result[i].getValue('custitem_item_info_2'));
		}


		var iteminfo3= form.addField('custpage_iteminfothree', 'select', 'Item Info3');
		iteminfo3.addSelectOption("","");
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custitem_item_info_3');			 
		var filters= new Array();
		//filters.push(new nlobjSearchFilter('custitem_item_info_3', null, 'isnotempty'));	
		filters.push(new nlobjSearchFilter('custitem_item_info_3', null, 'noneof',['@NONE@']));	
		var result= nlapiSearchRecord('item',null,filters,columns);
		for (var i = 0; result != null && i < result.length; i++) {
			var res=  form.getField('custpage_iteminfothree').getSelectOptions(result[i].getValue('custitem_item_info_3'), 'is');
			if (res != null) {						
				if (res.length > 0) {
					continue;
				}
			}
			iteminfo3.addSelectOption(result[i].getValue('custitem_item_info_3'),result[i].getValue('custitem_item_info_3'));
		}


		//var packcode = form.addField('custpage_packcode', 'select', 'Pack Code','customlist73');	
		var packcode = form.addField('custpage_packcode', 'select', 'Pack Code','customlist_ebiznet_packcode');	
		//var packcode = form.addField('custpage_packcode', 'select', 'Pack Code','customlist_nswmsdestinationlov');	
		// packcode.addSelectOption("","");

		//var uom = form.addField('custpage_uom', 'select', 'UOM','customlist75');
		var uom = form.addField('custpage_uom', 'select', 'UOM','customlist_ebiznet_uom');
		var Company = form.addField('custpage_company', 'select', 'Company','customrecord_ebiznet_company');
		// uom.addSelectOption("","");	

		var Destination = form.addField('custpage_dest', 'select', 'Destination','customlist_nswmsdestinationlov');

		Destination.setLayoutType('startrow', 'none');		

		var shippingcarrier = form.addField('custpage_shippingcarrier', 'select', 'Shipping Medhod');
		shippingcarrier.setLayoutType('startrow', 'none');

		var record = nlapiCreateRecord('salesorder', {recordmode: 'dynamic'});

		var methodsField = record.getField('shipmethod');
		var methodOptions = methodsField.getSelectOptions(null, null);
		nlapiLogExecution('ERROR','methodOptions',methodOptions);
		//nlapiLogExecution('ERROR', methodOptions[0].getId() + ',' + methodOptions[0].getText() );
		shippingcarrier.addSelectOption("",""); 

		for (var i = 0; methodOptions != null && i < methodOptions.length; i++) {
			nlapiLogExecution('ERROR', methodOptions[i].getId() + ',' + methodOptions[i].getText() );
			shippingcarrier.addSelectOption(methodOptions[i].getId(),methodOptions[i].getText());
		}	
		//added by shylaja on 091011 to filter the statuses which are not having allow pick at sku status.
		var itemstatus = form.addField('custpage_itemstatus', 'select', 'Item Status');
		var ItemstatusFilter = new Array();
		var Itemstatuscolumns = new Array();

		ItemstatusFilter[0] = new nlobjSearchFilter('custrecord_allowpickskustatus', null, 'is', 'T');
		Itemstatuscolumns[0] = new nlobjSearchColumn('custrecord_statusdescriptionskustatus');

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, ItemstatusFilter, Itemstatuscolumns);


		//  var ItemStatusDesc = searchresults[0].getValue('custrecord_statusdescriptionskustatus');
		//	  nlapiLogExecution('ERROR','ItemStatusDesc',ItemStatusDesc);





		itemstatus.addSelectOption("","");
		if (searchresults != null) {	
			for (var i = 0; i < Math.min(500, searchresults.length); i++) {

				var res = form.getField('custpage_itemstatus').getSelectOptions(searchresults[i].getValue('custrecord_statusdescriptionskustatus'), 'is');
				if (res != null) {
					if (res.length > 0) {
						continue;
					}
				}
				nlapiLogExecution('ERROR','res',res);
				nlapiLogExecution('ERROR','searchresults[i].getId()',searchresults[i].getId());
				itemstatus.addSelectOption(searchresults[i].getId(), searchresults[i].getValue('custrecord_statusdescriptionskustatus'));
				nlapiLogExecution('ERROR','itemstatus',itemstatus);
			}
		}
		nlapiLogExecution('ERROR','itemstatusnew',itemstatus);



		var country = form.addField('custpage_country', 'select', 'Country');
		var record = nlapiCreateRecord('location', {recordmode: 'dynamic'});		
		var countryField = record.getField('country');
		var countryOptions = countryField.getSelectOptions(null, null);
		nlapiLogExecution('ERROR','countryOptions',countryOptions);
		nlapiLogExecution('ERROR', countryOptions[0].getId() + ',' + countryOptions[0].getText() );
		country.addSelectOption("","");
		for (var i = 0; countryOptions != null && i < countryOptions.length; i++) {
			nlapiLogExecution('ERROR', countryOptions[i].getId() + ',' + countryOptions[i].getText() );
			country.addSelectOption(countryOptions[i].getId(),countryOptions[i].getText());
		}

		var state = form.addField('custpage_state', 'select', 'Ship State');
		var addr1 = form.addField('custpage_addr1', 'select', 'Ship Address');
		var City = form.addField('custpage_city', 'select', 'Ship City');

		state.addSelectOption("","");
		addr1.addSelectOption("","");
		City.addSelectOption("","");
		var columns = new Array();
		columns.push(new nlobjSearchColumn('shipstate'));
		columns.push(new nlobjSearchColumn('shipcity'));
		columns.push(new nlobjSearchColumn('shipaddress'));

		var filters= new Array();
		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
		filters.push(new nlobjSearchFilter('status', null, 'anyof', ['SalesOrd:B','SalesOrd:D','SalesOrd:E']));

		var result= nlapiSearchRecord('salesorder',null,filters,columns);

		for (var i = 0; result != null && i < result.length; i++) {
			var soresult=result[i];
			var vshipstate=soresult.getValue('shipstate');			
			var res=  form.getField('custpage_state').getSelectOptions(vshipstate, 'is');
			if (res != null) {				
				if (res.length > 0) {
					continue;
				}
			}		
			state.addSelectOption(vshipstate, vshipstate);
		}

		for (var i = 0; result != null && i < result.length; i++) {
			var soresult=result[i];
			var vshipstate=soresult.getValue('shipstate');
			var vshipcity=soresult.getValue('shipcity');
			var vshipaddr=soresult.getValue('shipaddress');

			var res1=  form.getField('custpage_addr1').getSelectOptions(vshipaddr, 'is');
			if (res1 != null) {				
				if (res1.length > 0) {
					continue;
				}
			}		
			addr1.addSelectOption(vshipaddr, vshipaddr);
		}

		for (var i = 0; result != null && i < result.length; i++) {
			var soresult=result[i];			
			var vshipcity=soresult.getValue('shipcity');

			var res1=  form.getField('custpage_city').getSelectOptions(vshipcity, 'is');
			if (res1 != null) {				
				if (res1.length > 0) {
					continue;
				}
			}		

			City .addSelectOption(vshipcity, vshipcity);
		}


		//added carrier field By suman
		var carrier = form.addField('custpage_carrier', 'select', 'Carrier','customrecord_ebiznet_carrier');
		//end of carrier field

		var soShipdate = form.addField('custpage_soshipdate', 'date', 'Ship Date');

		var sofrightterms = form.addField('custpage_sofrieghtterms', 'select', 'Freight Terms','customlist_nswmsfreighttermslov');

		var soOrderdate = form.addField('custpage_soorderdate', 'date', 'Order Date');

		form.addField('custpage_wmsstatusflag','select','WMS Status Flag','customrecord_wms_status_flag').setDefaultValue(25);
		
		form.addSubmitButton('Display');




		response.writePage(form);
	}
	else //this is the POST block
	{
		var vSo = request.getParameter('custpage_qbso');
		var vitem = request.getParameter('custpage_item');
		var vcustomer = request.getParameter('custpage_consignee');
		var vordPriority = request.getParameter('custpage_ordpriority');
		var vordtype = request.getParameter('custpage_ordtype');
		var vitemgroup = request.getParameter('custpage_itemgroup');
		var vitemfamily = request.getParameter('custpage_itemfamily');
		var vpackcode = request.getParameter('custpage_packcode');
		var vuom = request.getParameter('custpage_uom');
		var vitemstatus = request.getParameter('custpage_itemstatus');
		var siteminfo1= request.getParameter('custpage_iteminfoone'); 
		var siteminfo2= request.getParameter('custpage_iteminfotwo');
		var siteminfo3= request.getParameter('custpage_iteminfothree');
		var vCompany= request.getParameter('custpage_company');
		var vShippingCarrier= request.getParameter('custpage_shippingcarrier');
		var vShipCountry= request.getParameter('custpage_country');
		var vShipState= request.getParameter('custpage_state');
		var vShipCity= request.getParameter('custpage_city');
		var vShipAddr1= request.getParameter('custpage_addr1');
		var vShipmentNo= request.getParameter('custpage_qbshipmentno');
		var vRouteno= request.getParameter('custpage_qbrouteno');
		//added By Suman
		var vCarrier= request.getParameter('custpage_carrier');

		var vShipdate= request.getParameter('custpage_soshipdate');
		var vFreightTerms= request.getParameter('custpage_sofrieghtterms');
		var vOrderDate= request.getParameter('custpage_soorderdate');
        var wmsstatusflag=request.getParameter('custpage_wmsstatusflag');
		nlapiLogExecution('ERROR','FreightTerms',vFreightTerms);
		nlapiLogExecution('ERROR','vShipdate',vShipdate);
		nlapiLogExecution('ERROR','vOrderDate',vOrderDate);
		nlapiLogExecution('ERROR','wmsstatusflag',wmsstatusflag);
		var WaveQbparams = new Array();		
		if (vSo!=null &&  vSo != "") {
			WaveQbparams["custpage_qbso"] = vSo;
		}

		if (vitem!=null &&  vitem != "") {
			WaveQbparams["custpage_item"] = vitem;
		}
		if (vcustomer!=null && vcustomer != "") {
			WaveQbparams ["custpage_consignee"] = vcustomer;
		}
		if (vordPriority!=null && vordPriority != "") {
			WaveQbparams ["custpage_ordpriority"] = vordPriority;
		}
		if (vordtype!=null && vordtype != "") {
			WaveQbparams ["custpage_ordtype"] = vordtype;
		}
		if (vitemgroup!=null && vitemgroup != "") {
			WaveQbparams ["custpage_itemgroup"] = vitemgroup;
		}
		if (vitemfamily!=null && vitemfamily != "") {
			WaveQbparams ["custpage_itemfamily"] = vitemfamily;
		}
		if (vpackcode!=null && vpackcode != "") {
			WaveQbparams ["custpage_packcode"] = vpackcode;
		}
		if (vuom!=null && vuom != "") {
			WaveQbparams ["custpage_uom"] = vuom;
		}
		if (vitemstatus!=null && vitemstatus != "") {
			WaveQbparams ["custpage_itemstatus"] = vitemstatus;
		}
		if (siteminfo1!=null && siteminfo1 != "") { 
			WaveQbparams ["custpage_siteminfo1"] = siteminfo1;
		}
		if (siteminfo2!=null && siteminfo2 != "") { 
			WaveQbparams ["custpage_siteminfo2"] = siteminfo2;
		}
		if (siteminfo3!=null && siteminfo3 != "") { 
			WaveQbparams ["custpage_siteminfo3"] = siteminfo3;
		}
		if (vCompany!=null && vCompany != "") {
			WaveQbparams ["custpage_company"] = vCompany;
		}
		if (vShippingCarrier!=null && vShippingCarrier != "") {
			WaveQbparams ["custpage_shippingcarrier"] = vShippingCarrier;
		}
		if (vShipCountry!=null && vShipCountry != "") {
			WaveQbparams ["custpage_country"] = vShipCountry;
		}
		if (vShipState!=null && vShipState != "") {
			WaveQbparams ["custpage_state"] = vShipState;
		}
		if (vShipCity!=null && vShipCity != "") {
			WaveQbparams ["custpage_city"] = vShipCity;
		}
		if (vShipAddr1!=null && vShipAddr1 != "") {
			WaveQbparams ["custpage_addr1"] = vShipAddr1;
		}  
		if (vShipmentNo!=null && vShipmentNo != "") {
			WaveQbparams ["custpage_shipmentno"] = vShipmentNo;
		}  
		if (vRouteno!=null && vRouteno != "") {
			WaveQbparams ["custpage_routeno"] = vRouteno;
		}  
		//added Carrier By Suman
		if (vCarrier!=null && vCarrier != "") {
			WaveQbparams ["custpage_carrier"] = vCarrier;
		}

		if (vShipdate!=null && vShipdate != "") {
			WaveQbparams ["custpage_soshipdate"] = vShipdate;
		}
		if (vFreightTerms!=null && vFreightTerms != "") {
			nlapiLogExecution('ERROR', 'freightterms',vFreightTerms) ;
			WaveQbparams ["custpage_sofrieghtterms"] = vFreightTerms;
		}
		if (vOrderDate!=null && vOrderDate != "") {
			WaveQbparams ["custpage_soorderdate"] = vOrderDate;
		}
		
		if (wmsstatusflag!=null && wmsstatusflag != "") {
			WaveQbparams ["custpage_wmsstatusflag"] = wmsstatusflag;
		}
		
		response.sendRedirect('SUITELET', 'customscript_waveconfirm', 'customdeploy_waveconfirm', false, WaveQbparams );
	}
}

function myGenerateLocationButton(){
	alert("Hi"); 
}