/***************************************************************************
���������������������eBizNET
������������������eBizNET Solutions Inc
 ****************************************************************************
 *
 *� $Source: /cvs/products/eBizNET4.5/EoN/RF/Inbound/Suitelet/Attic/ebiz_RF_BuildCarton_cnf.js,v $
 *� $Revision: 1.1.2.1.4.4.4.5 $
 *� $Date: 2014/06/13 08:31:19 $
 *� $Author: skavuri $
 *� $Name: t_NSWMS_2014_1_3_125 $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_RF_BuildCarton_cnf.js,v $
 *� Revision 1.1.2.1.4.4.4.5  2014/06/13 08:31:19  skavuri
 *� Case# 20148882 (added Focus Functionality for Textbox)
 *�
 *� Revision 1.1.2.1.4.4.4.4  2014/06/06 06:18:34  skavuri
 *� Case# 20148749 (Refresh Functionality ) SB Issue Fixed
 *�
 *� Revision 1.1.2.1.4.4.4.3  2014/05/30 00:26:46  nneelam
 *� case#  20148622
 *� Stanadard Bundle Issue Fix.
 *�
 *� Revision 1.1.2.1.4.4.4.2  2013/06/11 14:30:40  schepuri
 *� Error Code Change ERROR to DEBUG
 *�
 *� Revision 1.1.2.1.4.4.4.1  2013/04/17 16:04:01  skreddy
 *� CASE201112/CR201113/LOG201121
 *� added meta tag
 *�
 *� Revision 1.1.2.1.4.4  2012/09/27 13:13:26  grao
 *� CASE201112/CR201113/LOG201121
 *�
 *� Converting multiple language with given Spanish terms
 *�
 *� Revision 1.1.2.1.4.3  2012/09/27 10:53:53  grao
 *� CASE201112/CR201113/LOG201121
 *�
 *� Converting multiple language with given Spanish terms
 *�
 *� Revision 1.1.2.1.4.2  2012/09/26 12:42:29  grao
 *� CASE201112/CR201113/LOG201121
 *� Converting Multi language without small characters
 *�
 *� Revision 1.1.2.1.4.1  2012/09/24 10:07:04  grao
 *� CASE201112/CR201113/LOG201121
 *� Converting Multi Lnaguage
 *�
 *� Revision 1.1.2.1  2012/06/26 06:25:48  spendyala
 *� CASE201112/CR201113/LOG201121
 *� New Script for Build Carton.
 *�
 *
 ****************************************************************************/



function BuildCartonConfirm(request, response)
{
	if (request.getMethod() == 'GET') 
	{
		
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
	    
		var st0,st1,st2;
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "CONFIRMADO CON &#201;XITO" ;
			st2 = "PR&#211;XIMO";
			
		}
		else
		{
			st0 = "";
			st1 = "CONFIRMED SUCCESSFULLY";
			st2 = "NEXT";
			

		}
			
		
		
		var functionkeyHtml=getFunctionkeyScript('_rf_buildcarton_confirm'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('cmdSend').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_buildcarton_confirm' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + "</td></tr>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + " <input name='cmdSend' id='cmdSend' type='submit' value='F8' onclick='this.disabled=true; form.submit();return false'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('cmdSend').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		var getOptedField = request.getParameter('cmdSend');
		var getLanguage = request.getParameter('hdngetLanguage');
		var POArray = new Array();
		POarray["custparam_language"] = getLanguage;
		response.sendRedirect('SUITELET', 'customscript_rf_receiving_menu', 'customdeploy_rf_receiving_menu_di', false, POArray);
	}
}
