/***************************************************************************
 eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_PutawayLP.js,v $
 *     	   $Revision: 1.7.4.4.4.4.2.8.4.1 $
 *     	   $Date: 2015/09/22 13:31:47 $
 *     	   $Author: schepuri $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_PutawayLP.js,v $
 * Revision 1.7.4.4.4.4.2.8.4.1  2015/09/22 13:31:47  schepuri
 * case# 201414551
 *
 * Revision 1.7.4.4.4.4.2.8  2014/06/13 06:30:59  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.7.4.4.4.4.2.7  2014/06/06 06:53:09  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.7.4.4.4.4.2.6  2014/05/30 00:26:50  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.7.4.4.4.4.2.5  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.7.4.4.4.4.2.4  2013/05/22 11:39:03  spendyala
 * CASE201112/CR201113/LOG201121
 * Validating PO Status before user perform transaction.
 *
 * Revision 1.7.4.4.4.4.2.3  2013/04/17 16:04:01  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.7.4.4.4.4.2.2  2013/03/13 13:57:17  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Time Stamp related changes.
 *
 * Revision 1.7.4.4.4.4.2.1  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.7.4.4.4.4  2013/02/07 15:10:34  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.7.4.4.4.3  2012/11/01 14:55:35  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.7.4.4.4.2  2012/09/27 10:58:09  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.7.4.4.4.1  2012/09/21 14:57:16  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.7.4.4  2012/06/11 13:34:50  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to validating Lp.
 *
 * Revision 1.7.4.3  2012/04/12 21:38:19  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to LP scan.
 *
 * Revision 1.7.4.2  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.7.4.1  2012/02/22 12:39:05  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.7  2011/09/22 08:29:26  snimmakayala
 * CASE201112/CR201113/LOG201121
 * The spelling in License Plate is  " LICENSE"  not "LICENCE"
 *
 * Revision 1.6  2011/08/24 12:44:47  schepuri
 * CASE201112/CR201113/LOG201121
 * RF Putaway LP loop  based on RuleValue
 *
 * Revision 1.5  2011/06/14 07:45:49  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.4  2011/06/06 14:10:04  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 * In Check-in, Remaining cube calculation and putaway logic.
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/

function PutawayLP(request, response){
	if (request.getMethod() == 'GET') 
	{
		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);

		var getLP='';// case# 201414551
		var lpcount = request.getParameter('custparam_lpcount');
		if (lpcount == null)
			lpcount = 0;
		
		var vRoleLocation=getRoledBasedLocation();

		var filters = new Array();
		
		if(vRoleLocation != null && vRoleLocation != '')
			filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vRoleLocation));
		
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '2'));
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_lpno');

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

		for (var i = 0; searchresults != null && i < searchresults.length; i++) {
			var searchresult = searchresults[i];

			getLP = searchresult.getValue(columns[0]);
		}
		nlapiLogExecution('DEBUG', 'Retrieved LP', getLP);

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);


		var st0,st1,st2,st3,st4,st5;

		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "PLACA"; 
			st2 = "INGRESAR / ESCANEAR DEL N&#218;MERO DE MATR&#205;CULA";
			st3 = "";
			st4 = "ENVIAR";
			st5 = "ANTERIOR";


		}
		else
		{
			st0 = "";
			st1 = "LP"; 
			st2 = "ENTER/SCAN LICENSE";
			st3 = "PLATE NUMBER";
			st4 = "SEND";
			st5 = "PREV";

		}




		var functionkeyHtml=getFunctionkeyScript('_rf_putaway_lp'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterlp').focus();";        
		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";
		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_putaway_lp' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + " : <label>" + getLP + "</label>";
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		html = html + "				<input type='hidden' name='hdnlpcount' value=" + lpcount + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><label id='LPNo'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + " </td></tr>";
		html = html + "				<td align = 'left'>" + st3;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlp' id='enterlp' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st4 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					"+ st5 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlp').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is not null');

		// This variable is to hold the LP entered.
		var getLPNo = request.getParameter('enterlp');
		nlapiLogExecution('DEBUG', 'getLPNo', getLPNo);
		var getLPCount = request.getParameter('hdnlpcount');
		nlapiLogExecution('DEBUG', 'getLPCount', getLPCount);

		var filters = new Array();
		//		filters[0] = new nlobjSearchFilter('custrecord_status_flag', null, 'is', 'L');
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]));
		filters.push(new nlobjSearchFilter('custrecord_lpno', null, 'is', getLPNo));
		
		if(vRoleLocation != null && vRoleLocation != '')
			filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vRoleLocation));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_lpno');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		columns[2] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

		var POarray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;

		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);


		var st6;
		if( getLanguage == 'es_ES')
		{
			st6 = "LP NO V&#193;LIDO";
		}
		else
		{
			st6 = "INVALID LP";
		}



		var getOptedField = request.getParameter('hdnOptedField');
		POarray["custparam_option"] = getOptedField;

		POarray["custparam_error"] = st6;
		POarray["custparam_screenno"] = '6';
		var LPNoArray = new Array();
		var lpListString;

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept PO #.
		if (optedEvent == 'F7') {
			response.sendRedirect('SUITELET', 'customscript_rf_receiving_menu', 'customdeploy_rf_receiving_menu_di', false, POarray);
		}
		else {
			try { 
				var vhdnNumber = 0;
				var sysRulefilters = new Array();

				sysRulefilters[0] = new nlobjSearchFilter('custrecord_ebiztasktype', null, 'is','2');

				var sysRulecolumns = new Array();
				sysRulecolumns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');

				var sysRulesearchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, sysRulefilters, sysRulecolumns);

				if (sysRulesearchresults != null){
					var vhdnNumber = sysRulesearchresults[0].getValue('custrecord_ebizrulevalue');
					nlapiLogExecution('DEBUG', 'sysRulesearchresults vhdnNumber', vhdnNumber);
				}
				if (getLPNo != null && getLPNo != "") {
					if (searchresults != null){
						var poid=searchresults[0].getValue("custrecord_ebiz_order_no");
						var contrlno=searchresults[0].getValue("custrecord_ebiz_cntrl_no");
							if(poid==null&&poid=="")
								poid=contrlno;
						var trantype = nlapiLookupField('transaction', poid, 'recordType');
						var validate=CheckOrderStatus(poid,trantype);
						nlapiLogExecution("ERROR","validate",validate);
						if(validate=="F")
						{
							POarray["custparam_error"]="INVALID PO STATUS";
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							nlapiLogExecution('DEBUG', 'Invalid PO ', 'PO closed/cancelled');
							return;
						}
						else
						{
							if (request.getParameter('custparam_lpno') == null || request.getParameter('custparam_lpno') == ""){	
								POarray["custparam_lpno"] = getLPNo; 
							}
							else
							{
								POarray["custparam_lpno"] = request.getParameter('custparam_lpno') + ',' + getLPNo;
							}
							lpListString = POarray["custparam_lpno"];
							POarray["custparam_lpcount"] = parseFloat(request.getParameter('hdnlpcount')) + 1;
							nlapiLogExecution('DEBUG', 'lpcount', POarray["custparam_lpcount"]);

							LPNoArray.push(getLPNo);
							if ((parseFloat(getLPCount) + 1) < parseFloat(vhdnNumber)){
								nlapiLogExecution('DEBUG', 'Scanning LP No.');
								response.sendRedirect('SUITELET', 'customscript_rf_putaway_lp', 'customdeploy_rf_putaway_lp_di', false, POarray);
								return;
							}

							var getTaskId = searchresults[0].getId();
							nlapiLogExecution('DEBUG', 'getTaskId', getTaskId);

							//*** The following code is added by Satish.N on 12-Mar-2013 to update task begin time and user name***//

							var currentContext = nlapiGetContext();  
							var currentUserID = currentContext.getUser();
							nlapiLogExecution('DEBUG', 'currentUserID', currentUserID);

							var fields=new Array();
							fields[0]='custrecord_taskassignedto';		
							fields[1]='custrecord_actualbegintime';	

							var Values=new Array();
							Values[0]=currentUserID;	
							Values[1]=TimeStamp();	

							var UpdatedRecID=nlapiSubmitField('customrecord_ebiznet_trn_opentask',getTaskId,fields,Values);

							//*** Upto here ***//

							// Load a record into a variable from opentask table for the LP# entered
							var PORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', getTaskId);

							POarray["custparam_lpno"] = PORec.getFieldValue('custrecord_lpno');
							POarray["custparam_quantity"] = PORec.getFieldValue('custrecord_expe_qty');
							POarray["custparam_location"] = PORec.getFieldValue('custrecord_actbeginloc');
							nlapiLogExecution('DEBUG', 'Location', POarray["custparam_location"]);
							POarray["custparam_item"] = PORec.getFieldValue('custrecord_sku');
							POarray["custparam_itemtext"] = PORec.getFieldText('custrecord_sku');
							POarray["custparam_itemDescription"] = PORec.getFieldValue('custrecord_skudesc');
							POarray["custparam_itemstatus"] = PORec.getFieldValue('custrecord_sku_status');
							POarray["custparam_lpNumbersArray"] = lpListString;
							POarray["custparam_enteredLPNo"] = getLPNo;

							//*** The below code is commented by Satish.N on 03/12/2013 as the location name can be fetched from open task***//
//							var BinLocationRec = nlapiLoadRecord('customrecord_ebiznet_location', POarray["custparam_location"]);
//							POarray["custparam_beginlocation"] = BinLocationRec.getFieldValue('name');

							POarray["custparam_beginlocation"] = PORec.getFieldText('custrecord_actbeginloc');

							nlapiLogExecution('DEBUG', 'Location Name is', POarray["custparam_beginlocation"]);

							response.sendRedirect('SUITELET', 'customscript_rf_putaway_confirmation', 'customdeploy_rf_putaway_confirmation_di', false, POarray);
						}
					}
					else {
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
					}
				}
				else {
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					nlapiLogExecution('DEBUG', 'getLPNo is null ', getLPNo);
				}
			} 
			catch (e) {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'In error catch block ', 'Length is null');
			}
			nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
		}
	}
}
