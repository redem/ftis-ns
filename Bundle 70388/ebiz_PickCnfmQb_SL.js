/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_PickCnfmQb_SL.js,v $
 *     	   $Revision: 1.4.4.3.4.1.4.4 $
 *     	   $Date: 2015/08/25 10:17:03 $
 *     	   $Author: schepuri $
 *     	   $Name: t_eBN_2015_2_StdBundle_1_269 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 * *
 * DESCRIPTION
 *  	
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_PickCnfmQb_SL.js,v $
 * Revision 1.4.4.3.4.1.4.4  2015/08/25 10:17:03  schepuri
 * case# 201414074
 *
 * Revision 1.4.4.3.4.1.4.3  2013/07/25 21:22:45  grao
 * Case# 20123613
 * GFT Prod  issue fixes
 *
 * Revision 1.4.4.3.4.1.4.2  2013/07/04 18:56:38  spendyala
 * CASE201112/CR201113/LOG2012392
 * Issue fixed
 *
 * Revision 1.4.4.3.4.1.4.1  2013/03/05 13:35:46  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.4.4.3.4.1  2012/11/01 14:55:02  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.4.4.3  2012/08/10 14:47:08  spendyala
 * CASE201112/CR201113/LOG201121
 * Added functionailty to fetch more than 1000 records
 * in wave and order query block
 *
 * Revision 1.4.4.2  2012/05/15 23:00:27  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * pick confirm wave# drop down dispaly more than 1000 records
 *
 * Revision 1.4.4.1  2012/04/05 13:13:25  schepuri
 * CASE201112/CR201113/LOG201121
 * paging functionality is added
 *
 * Revision 1.4  2011/10/31 13:53:42  gkalla
 * CASE201112/CR201113/LOG201121
 * Added CVS header
 *
 * Revision 1.18  2011/10/31 11:39:48  gkalla
 * CASE201112/CR201113/LOG201121
 * To get the Wave#, fulfillment Order# and cluster#s in Sorting order
 *
 * Revision 1.17  2011/10/13 17:06:09  spendyala
 * CASE201112/CR201113/LOG201121
 * added printreport2 function for wavepickreport generation
 *
 * Revision 1.16  2011/10/12 20:00:15  spendyala
 * CASE201112/CR201113/LOG201121
 * uncommitted the display button
 *
 * Revision 1.15  2011/10/12 19:34:02  spendyala
 * CASE201112/CR201113/LOG201121
 * T&T issue is fixed
 *
 *
 *
 **********************************************************************************************************************/
function PickcnfmQbSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Pick Confirmation');
		/* The below code is merged from Lexjet production account on 03-02-2013 by Radhika as part of Standard bundle*/
		var vRoleLocation=getRoledBasedLocation();
		/* Up to here */ 
		var WaveField = form.addField('custpage_wave', 'select', 'Wave #');
		WaveField.setLayoutType('startrow', 'none');
		WaveField.addSelectOption("", "");
		/* The below code(Adding vRolelocation to the func)is merged from Lexjet production account on 03-02-2013 by Radhika as part of Standard bundle*/
		FillWave(form, WaveField,-1,vRoleLocation);
//		var filtersso = new Array();
//		filtersso[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'); //PICK
//		filtersso[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'is', '9');
//		var columnsinvt = new Array();
//		columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
//		columnsinvt[0].setSort('true');

//		WaveField.addSelectOption("", "");
//		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso, columnsinvt);
//		for (var i = 0; searchresults!=null && i < searchresults.length; i++) {
//		var res=  form.getField('custpage_wave').getSelectOptions(searchresults[i].getValue('custrecord_ebiz_wave_no'), 'is');
//		if (res != null) {
//		nlapiLogExecution('ERROR', 'res.length', res.length + searchresults[i].getValue('custrecord_ebiz_wave_no') );
//		if (res.length > 0) {
//		continue;
//		}
//		}
//		WaveField.addSelectOption(searchresults[i].getValue('custrecord_ebiz_wave_no'), searchresults[i].getValue('custrecord_ebiz_wave_no'));
//		}

		var DoField = form.addField('custpage_do', 'select', 'Fulfillment Order #');
		DoField.setLayoutType('startrow', 'none');
		DoField.addSelectOption("", "");
		/* The below code(Adding vRolelocation to the func)is merged from Lexjet production account on 03-02-2013 by Radhika as part of Standard bundle*/
		FillOrder(form, DoField,-1,vRoleLocation);

//		var filtersdo = new Array();
//		filtersdo[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'); //Pick
//		filtersdo[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'is',  '9');

//		var columnsdo = new Array();
//		columnsdo[0] = new nlobjSearchColumn('name');
//		columnsdo[0].setSort('true');

//		DoField.addSelectOption("", "");
//		var searchresultsdo = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersdo,columnsdo);

//		for (var i = 0; searchresultsdo!=null && i <  searchresultsdo.length; i++) {
//		var res=  form.getField('custpage_do').getSelectOptions(searchresultsdo[i].getValue('name'), 'is');
//		if (res != null) {
//		nlapiLogExecution('ERROR', 'res.length', res.length + searchresultsdo[i].getValue('name') );
//		if (res.length > 0) {
//		continue;
//		}
//		}

//		DoField.addSelectOption(searchresultsdo[i].getValue('name'), searchresultsdo[i].getValue('name'));
//		}	 
		form.addSubmitButton('Display');
		response.writePage(form);
	}
	else //this is the POST block
	{
		var vwave = request.getParameter('custpage_wave');
		var vQbdo = request.getParameter('custpage_do');
		var SOarray = new Array();
		if (vwave!=null &&  vwave != "") {
			SOarray ["ebiz_wave_no"] = vwave;
		}
		if (vQbdo!=null &&  vQbdo != "") {
			SOarray ["custpage_do"] = vQbdo;       
		}
		response.sendRedirect('SUITELET', 'customscript_confirmpicks', 'customdeploy_confirmpicks', false, SOarray );
	}
}
/* The below code(Adding vRolelocation to the func)is merged from Lexjet production account on 03-02-2013 by Radhika as part of Standard bundle*/
function FillWave(form, WaveField,maxno,vRoleLocation){

	var filtersso = new Array();
	filtersso.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));  // Pick task
	//Status Flag='G'(PICK GENERATED),'F' (OUTBOUND.FAILED)
	filtersso.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9']));
	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'isnotempty', null));
	/* The below code is merged from Lexjet production account on 03-02-2013 by Radhika as part of Standard bundle*/	
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		filtersso.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vRoleLocation));

	}
	/* Up to here */ 
	if(maxno!=-1)
	{
		filtersso.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno)));
	}

	var columnso = new Array();
	columnso.push(new nlobjSearchColumn('id').setSort(true));	
	columnso.push(new nlobjSearchColumn('custrecord_ebiz_wave_no').setSort(true));
	columnso.push(new nlobjSearchColumn('name'));

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso,columnso);

	for (var i = 0; searchresults != null && i < searchresults.length; i++) {

		if(searchresults[i].getValue('custrecord_ebiz_wave_no') != null && searchresults[i].getValue('custrecord_ebiz_wave_no') != "" && searchresults[i].getValue('custrecord_ebiz_wave_no') != " ")
		{
			var resdo = form.getField('custpage_wave').getSelectOptions(searchresults[i].getValue('custrecord_ebiz_wave_no'), 'is');
			if (resdo != null) {
				nlapiLogExecution('ERROR', 'res.length', resdo.length + searchresults[i].getValue('name') );
				if (resdo.length > 0) {
					continue;
				}
			}
		
		WaveField.addSelectOption(searchresults[i].getValue('custrecord_ebiz_wave_no'), searchresults[i].getValue('custrecord_ebiz_wave_no'));
		}
	}


	if(searchresults!=null && searchresults.length>=1000)
	{
		var maxno=searchresults[searchresults.length-1].getValue('id');		
		FillWave(form, WaveField,maxno);	
	}
}
/* The below code(Adding vRolelocation to the func)is merged from Lexjet production account on 03-02-2013 by Radhika as part of Standard bundle*/
function FillOrder(form, DoField,maxno,vRoleLocation)
{
	var filtersdo = new Array();
	filtersdo[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'); //Pick
	filtersdo[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'is',  '9');
	/* The below code is merged from Lexjet production account on 03-02-2013 by Radhika as part of Standard bundle*/	
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		filtersdo.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vRoleLocation));

	}
	/* Up to here */ 
	if(maxno!=-1)
	{
		filtersdo.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno)));
	}

	var columnsdo = new Array();
	columnsdo[0] = new nlobjSearchColumn('name').setSort(true);
	columnsdo.push(new nlobjSearchColumn('id').setSort(true));	

	var searchresultsdo = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersdo,columnsdo);

	for (var i = 0; searchresultsdo!=null && i <  searchresultsdo.length; i++) {
		if(searchresultsdo[i].getValue('name') != null && searchresultsdo[i].getValue('name') != "" && searchresultsdo[i].getValue('name') != " ")
		{
			var res=  form.getField('custpage_do').getSelectOptions(searchresultsdo[i].getValue('name'), 'is');
			if (res != null) {
				//nlapiLogExecution('ERROR', 'res.length', res.length + searchresultsdo[i].getValue('name') ); // case# 201414074
				if (res.length > 0) {
					continue;
				}
			}
		}
		DoField.addSelectOption(searchresultsdo[i].getValue('name'), searchresultsdo[i].getValue('name'));
	}

	if(searchresultsdo!=null && searchresultsdo.length>=1000)
	{
		var maxno=searchresultsdo[searchresultsdo.length-1].getValue('id');		
		FillOrder(form, DoField,maxno,vRoleLocation);	
	}
}
