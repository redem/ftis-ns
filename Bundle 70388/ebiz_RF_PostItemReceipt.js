/***************************************************************************
 eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/Attic/ebiz_RF_PostItemReceipt.js,v $
 *     	   $Revision: 1.1.2.52.2.8 $
 *     	   $Date: 2015/11/25 10:54:38 $
 *     	   $Author: deepshikha $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_PostItemReceipt.js,v $
 * Revision 1.1.2.52.2.8  2015/11/25 10:54:38  deepshikha
 * 2015.2 Issue Fix
 * 201415839
 *
 * Revision 1.1.2.52.2.7  2015/11/24 11:22:13  aanchal
 * 2015.2 issue fix
 * 201415680
 *
 * Revision 1.1.2.52.2.6  2015/11/20 10:52:36  grao
 * 2015.2 Issue Fixes 201415783
 *
 * Revision 1.1.2.52.2.5  2015/11/18 11:52:28  gkalla
 * case# 201415748
 * Added wms status flag filter with putaway completed, check in.
 *
 * Revision 1.1.2.52.2.4  2015/11/14 13:26:21  sponnaganti
 * case# 201415658
 * 2015.2 issue fix
 *
 * Revision 1.1.2.52.2.3  2015/11/02 23:39:24  sponnaganti
 * case# 201415289
 * Issue is with serial item we are not getting serial numbers to post item
 * receipt. Added "Receipt checkin" status to get RMA serials from serial entry
 *
 * Revision 1.1.2.52.2.2  2015/10/06 06:32:46  aanchal
 * 2015.2 issue fixes
 * 201414871
 *
 * Revision 1.1.2.52.2.1  2015/09/22 13:31:10  schepuri
 * case# 201414550
 *
 * Revision 1.1.2.52  2015/09/04 10:01:29  schepuri
 * case# 201414290
 *
 * Revision 1.1.2.51  2015/09/01 09:24:42  deepshikha
 * 2015.2 issue fixes
 * 201414146
 *
 * Revision 1.1.2.50  2015/08/17 10:56:27  skreddy
 * Case# 201414022
 * Bedgear SB issue fix
 *
 * Revision 1.1.2.49  2015/08/13 15:35:43  skreddy
 * Case# 201413992,201413993
 * Bedgear SB issue fix
 *
 * Revision 1.1.2.48  2015/08/06 15:35:55  skreddy
 * Case# 201413723,201413790
 * Bedgear SB issue fix
 *
 * Revision 1.1.2.47  2015/08/05 04:33:37  schepuri
 * case# 201413736
 *
 * Revision 1.1.2.46  2015/08/03 15:44:54  skreddy
 * Case# 201413777
 * 2015.2 compatibility issue fix
 *
 * Revision 1.1.2.45  2015/07/28 15:29:17  skreddy
 * Case# 201413628
 * Sighwarehouse SB issue fix
 *
 * Revision 1.1.2.44  2015/07/28 13:36:06  schepuri
 * case# 201413606
 *
 * Revision 1.1.2.43  2015/07/23 15:30:34  grao
 * 2015.2   issue fixes  201412877
 *
 * Revision 1.1.2.42  2015/07/16 15:33:59  skreddy
 * Case# 201413492,201413494,201413493
 * Briggs SB1 issue fix
 *
 * Revision 1.1.2.41  2015/07/03 13:11:37  schepuri
 * case# 201413302
 *
 * Revision 1.1.2.40  2015/06/16 12:50:48  schepuri
 * case# 201413116
 *
 * Revision 1.1.2.39  2015/06/01 15:41:30  grao
 * SB  2015.2 issue fixes  201412877
 *
 * Revision 1.1.2.38  2015/05/14 12:40:15  skreddy
 * Case# 201412778
 * signwarehouse SB issue fix
 *
 * Revision 1.1.2.37  2015/04/28 13:08:19  skreddy
 * Case# 201412490
 * Helimot  issue fix
 *
 * Revision 1.1.2.36  2015/04/22 15:28:40  skreddy
 * Case# 201412446
 * standard bundle issue fix
 *
 * Revision 1.1.2.35  2015/02/23 17:51:34  rrpulicherla
 * Case#201411317
 *
 * Revision 1.1.2.34  2015/02/18 14:26:02  rrpulicherla
 * Case#201411317
 *
 * Revision 1.1.2.33  2015/01/07 13:56:15  schepuri
 * issue#   201411371
 *
 * Revision 1.1.2.32  2014/11/03 16:46:29  gkalla
 * case # 201410837
 * CT Post item receipt and TO issue
 *
 * Revision 1.1.2.31  2014/09/19 11:25:57  skavuri
 * case # 201410336, 201410246
 *
 * Revision 1.1.2.30  2014/09/18 13:19:58  sponnaganti
 * Case# 201410421
 * Stnd Bundle Issue fix
 *
 * Revision 1.1.2.29  2014/08/25 06:38:55  skreddy
 * case # 20148852
 * post item receipt by scheduler
 *
 * Revision 1.1.2.28  2014/07/23 15:26:40  sponnaganti
 * Case# 20149597
 * Compatibility Issue fix
 *
 * Revision 1.1.2.27  2014/06/24 06:06:10  skreddy
 * case # 20148971
 * Sportshq prod  issue fix
 *
 * Revision 1.1.2.26  2014/06/18 15:44:11  skavuri
 * Case # 20141363 SB Issue Fixed
 *
 * Revision 1.1.2.25  2014/06/13 06:48:51  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.24  2014/06/12 13:42:01  skreddy
 * case # 20148827
 * MHP Prod issue fix
 *
 * Revision 1.1.2.23  2014/06/06 06:30:39  skavuri
 * Case# 20148749 (Refresh Functionality ) SB Issue Fixed
 *
 * Revision 1.1.2.22  2014/05/30 00:26:49  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.21  2014/05/28 15:18:44  skavuri
 * Case# 20148536 SB Issue Fixed
 *
 * Revision 1.1.2.20  2014/04/29 15:16:32  rmukkera
 * Case # 20141363
 *
 * Revision 1.1.2.19  2014/04/17 14:21:53  rmukkera
 * Case # 20141363
 *
 * Revision 1.1.2.18  2014/04/14 15:51:15  nneelam
 * case#  20141363
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.17  2014/04/09 15:45:17  nneelam
 * case#  20141363
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.16  2014/03/28 15:25:02  skavuri
 * Case # 20127871 issue fixed
 *
 * Revision 1.1.2.15  2014/03/03 07:19:02  snimmakayala
 * 20127286
 *
 * Revision 1.1.2.14  2014/02/25 23:58:00  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20127387
 *
 * Revision 1.1.2.13  2014/02/13 15:11:32  rmukkera
 * Case # 20127042,20127117
 *
 * Revision 1.1.2.12  2014/02/12 15:13:19  grao
 * Case# 20127042 related issue fixes in MHP SB issue fixes
 *
 * Revision 1.1.2.11  2014/02/07 15:47:22  nneelam
 * case#  20127042
 * std bundle issue fix
 *
 * Revision 1.1.2.10  2014/02/06 14:53:50  rmukkera
 * Case # 20127077
 *
 * Revision 1.1.2.9  2014/02/04 14:27:01  rmukkera
 * Case # 20126968
 *
 * Revision 1.1.2.8  2014/01/24 13:38:25  schepuri
 * 20126902
 * standard bundle issue fix
 *
 * Revision 1.1.2.7  2014/01/20 13:52:50  schepuri
 * 20126855
 * standard bundle issue fix
 *
 * Revision 1.1.2.6  2014/01/06 13:12:59  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.1.2.5  2013/12/02 08:54:57  schepuri
 * 20125938,20125998
 *
 * Revision 1.1.2.4  2013/11/14 15:42:42  skreddy
 * Case# 20125736
 * Afosa SB issue fix
 *
 * Revision 1.1.2.3  2013/11/12 06:39:39  skreddy
 * Case# 20125518 & 20125608
 * Afosa SB issue fix
 *
 * Revision 1.1.2.2  2013/11/08 14:34:53  schepuri
 * 20125554
 *
 * Revision 1.1.2.1  2013/07/25 08:59:39  rmukkera
 * landed cost cr new file
 *
 * Revision 1.23.2.41.4.12.2.25  2013/06/04 11:54:23  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Country of origin changes
 *
 * Revision 1.23.2.41.4.12.2.24  2013/06/04 11:05:58  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Country of origin changes
 *
 * Revision 1.23.2.41.4.12.2.23  2013/06/04 10:54:02  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Country of origin changes
 *
 * Revision 1.23.2.41.4.12.2.22  2013/05/31 15:17:36  grao
 * CASE201112/CR201113/LOG201121
 * PMM Issues fixes
 *
 * Revision 1.23.2.41.4.12.2.21  2013/05/16 11:25:03  spendyala
 * CASE201112/CR201113/LOG201121
 * InvtRef# field of opentask record will be updated
 * to putaway task, once inbound process in completed
 *
 * Revision 1.23.2.41.4.12.2.20  2013/05/15 07:36:54  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.23.2.41.4.12.2.19  2013/05/14 14:17:28  schepuri
 * Surftech issues of serial status update
 *
 * Revision 1.23.2.41.4.12.2.18  2013/05/10 11:02:30  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.23.2.41.4.12.2.17  2013/05/09 12:47:51  spendyala
 * CASE201112/CR201113/LOG2012392
 * Line level location is considered first if it is empty then header level location is taken.
 *
 * Revision 1.23.2.41.4.12.2.16  2013/05/03 15:37:50  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fixes
 *
 * Revision 1.23.2.41.4.12.2.15  2013/05/01 12:45:02  rmukkera
 * Changes done in the code to submit the more than 3800 chars to the net suite serial numbers.
 *
 * Revision 1.23.2.41.4.12.2.14  2013/04/29 14:41:15  schepuri
 * uom conversion issues
 *
 * Revision 1.23.2.41.4.12.2.13  2013/04/17 16:04:02  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.23.2.41.4.12.2.12  2013/04/16 15:05:06  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.23.2.41.4.12.2.11  2013/04/16 12:48:56  spendyala
 * CASE201112/CR201113/LOG2012392
 * Issue fixes.
 *
 * Revision 1.23.2.41.4.12.2.10  2013/04/10 15:48:31  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.23.2.41.4.12.2.9  2013/04/04 16:00:13  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related  to Putgenqty in PO status Report
 *
 * Revision 1.23.2.41.4.12.2.8  2013/04/04 15:08:27  rmukkera
 * Issue Fix related to Tsg while TO receiving for Serial item, system allows to scan the new serial#
 *
 * Revision 1.23.2.41.4.12.2.7  2013/04/03 06:45:58  gkalla
 * CASE201112/CR201113/LOG201121
 * As part of standard bundle
 *
 * Revision 1.23.2.41.4.12.2.6  2013/04/02 14:38:18  schepuri
 * tsg sb putaway qty exception
 *
 * Revision 1.23.2.41.4.12.2.5  2013/04/01 20:59:31  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.23.2.41.4.12.2.4  2013/03/15 09:28:43  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.23.2.41.4.12.2.3  2013/03/13 13:57:16  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Time Stamp related changes.
 *
 * Revision 1.23.2.41.4.12.2.2  2013/03/01 15:01:12  rmukkera
 * code is merged from FactoryMation production as part of Standard bundle
 *
 * Revision 1.23.2.41.4.12.2.1  2013/02/26 12:52:10  rmukkera
 * code merged from boombah production bundle on 26th feb 2013
 * t_eBN_2013_1_StdBundle_1
 *
 * Revision 1.23.2.41.4.12  2013/02/07 15:10:34  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.23.2.41.4.11  2013/02/07 08:40:30  skreddy
 * CASE201112/CR201113/LOG201121
 * RF Lot auto generating FIFO enhancement
 *
 * Revision 1.23.2.41.4.10  2013/01/08 15:41:28  gkalla
 * CASE201112/CR201113/LOG201121
 * Lexjet issue for batch entry irrespective of location
 *
 * Revision 1.23.2.41.4.9  2013/01/08 14:17:47  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form 2012.2 branch.
 *
 * Revision 1.23.2.41.4.8  2013/01/04 14:53:24  grao
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.23.2.41.4.7  2012/12/17 15:14:56  schepuri
 * CASE201112/CR201113/LOG201121
 * Reapting same lp after confirm putaway
 *
 * Revision 1.23.2.41.4.6  2012/12/12 16:02:32  gkalla
 * CASE201112/CR201113/LOG201121
 * Actual Bin management enhancement
 *
 * Revision 1.23.2.41.4.5  2012/12/11 16:24:45  grao
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.23.2.41.4.4  2012/11/01 14:55:35  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.23.2.41.4.3  2012/10/03 11:33:31  schepuri
 * CASE201112/CR201113/LOG201121
 * NSUOM Convertion
 *
 * Revision 1.23.2.41.4.2  2012/09/27 10:57:49  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.23.2.41.4.1  2012/09/21 14:57:16  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.23.2.41  2012/09/03 13:45:29  schepuri
 * CASE201112/CR201113/LOG201121
 * added date stamp
 *
 * Revision 1.23.2.40  2012/08/27 15:24:28  schepuri
 * CASE201112/CR201113/LOG201121
 * fetched location is considered with uppercase
 *
 * Revision 1.23.2.39  2012/08/24 18:13:58  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * NSUOMfield code changes
 *
 * Revision 1.23.2.38  2012/08/10 14:46:20  gkalla
 * CASE201112/CR201113/LOG201121
 * To create inventory if merged qty greater than pallet qty
 *
 * Revision 1.23.2.37  2012/08/10 14:45:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Navigation issue when F7 is pressed resolved.
 *
 * Revision 1.23.2.36  2012/08/06 06:49:03  spendyala
 * CASE201112/CR201113/LOG201121
 * Quantity exception issues is resolved.
 *
 * Revision 1.23.2.35  2012/07/31 06:54:44  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to Creating Inventory was resolved.
 *
 * Revision 1.23.2.34  2012/07/30 23:17:32  gkalla
 * CASE201112/CR201113/LOG201121
 * NSUOM issue
 *
 * Revision 1.23.2.33  2012/07/10 23:15:21  gkalla
 * CASE201112/CR201113/LOG201121
 * Alert Mails
 *
 * Revision 1.23.2.32  2012/06/15 07:13:11  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to Merge LP is fixed.
 *
 * Revision 1.23.2.31  2012/06/04 09:24:49  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.23.2.30  2012/05/31 12:49:33  mbpragada
 * CASE201112/CR201113/LOG201121
 * Dynacraft INB/OUB issue fix
 *
 * Revision 1.23.2.29  2012/05/23 14:16:17  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.23.2.28  2012/05/18 17:58:28  gkalla
 * CASE201112/CR201113/LOG201121
 * TO Item receipt issue fixed. Commented partial TO receipt check condition
 *
 * Revision 1.23.2.27  2012/05/16 13:22:17  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Multi UOM changes
 *
 * Revision 1.23.2.26  2012/05/11 14:50:02  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to item receipt is resolved.
 *
 * Revision 1.23.2.25  2012/05/10 15:00:34  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to clicking previous screen was resolved.
 *
 * Revision 1.23.2.24  2012/05/09 14:47:33  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to Fifo value for BatchItem while creating inv rec is fixed.
 *
 * Revision 1.23.2.23  2012/05/09 14:37:27  spendyala
 * CASE201112/CR201113/LOG201121
 * While creating Inv Record ExpireDate is not getting updated for a batch item,issue is resolved.
 *
 * Revision 1.23.2.22  2012/05/07 09:36:36  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * confirm putaway
 *
 * Revision 1.23.2.21  2012/05/04 13:54:05  gkalla
 * CASE201112/CR201113/LOG201121
 * Putaway qty exception for Batch item
 *
 * Revision 1.23.2.20  2012/04/30 15:09:07  gkalla
 * CASE201112/CR201113/LOG201121
 * To check lineno while doing Itemreceipt in transferorder
 *
 * Revision 1.23.2.19  2012/04/30 10:00:47  spendyala
 * CASE201112/CR201113/LOG201121
 * While Searching of Item in ItemMaster,
 * 'name' filter is changed to 'nameinternal'
 *
 * Revision 1.23.2.18  2012/04/13 23:12:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF CART PUTAWAY issues.
 *
 * Revision 1.23.2.17  2012/04/03 11:05:06  gkalla
 * CASE201112/CR201113/LOG201121
 * Added WMS status filter for Merge LP
 *
 * Revision 1.23.2.16  2012/04/02 06:16:19  spendyala
 * CASE201112/CR201113/LOG201121
 * set true lot in our custom records.
 *
 * Revision 1.23.2.15  2012/03/28 12:48:03  schepuri
 * CASE201112/CR201113/LOG201121
 * validating from entering another loc other than fetched location
 *
 * Revision 1.23.2.14  2012/03/21 11:09:44  schepuri
 * CASE201112/CR201113/LOG201121
 * Added movetask
 *
 * Revision 1.23.2.13  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.23.2.12  2012/03/12 13:51:22  spendyala
 * CASE201112/CR201113/LOG201121
 * Passing  Record id to the query string.
 *
 * Revision 1.23.2.11  2012/03/09 07:45:47  spendyala
 * CASE201112/CR201113/LOG201121
 * Code Merged from trunk.
 *
 * Revision 1.23.2.10  2012/03/02 01:25:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.23.2.9  2012/02/24 00:01:34  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * merge lp in cart putaway
 *
 * Revision 1.23.2.8  2012/02/23 23:09:35  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * merge lp
 *
 * Revision 1.23.2.7  2012/02/23 00:27:34  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * supress lp in Invtmove and lp merge based on fifodate
 *
 * Revision 1.23.2.6  2012/02/22 12:37:46  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.23.2.5  2012/02/16 14:08:11  spendyala
 * CASE201112/CR201113/LOG201121
 * While creating  inventory records, order# field is updating with purchase order#
 *
 * Revision 1.23.2.4  2012/02/13 23:21:11  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge
 *
 * Revision 1.28  2012/02/12 12:26:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Checkin MultiUOM issues
 *
 * Revision 1.27  2012/02/10 08:58:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge
 *
 * Revision 1.26  2012/01/20 19:02:57  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Confirm Putaway Issue
 *
 * Revision 1.25  2012/01/19 00:43:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Changes
 *
 * Revision 1.24  2012/01/09 13:16:10  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Replenishment
 *
 * Revision 1.23  2011/12/28 23:14:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Putaway changes
 *
 * Revision 1.22  2011/12/28 13:20:51  spendyala
 * CASE201112/CR201113/LOG201121
 * resolved issues related to qty exception while entering data into transaction order line table
 *
 * Revision 1.21  2011/12/28 06:57:22  spendyala
 * CASE201112/CR201113/LOG201121
 * made changes for qty exception
 *
 * Revision 1.20  2011/12/24 15:52:57  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.19  2011/12/23 23:36:41  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.18  2011/12/23 13:32:26  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.17  2011/12/19 14:59:34  snimmakayala
 * CASE201112/CR201113/LOG201121
 * FIFO in RF Receiving.
 *
 * Revision 1.16  2011/11/17 08:55:20  spendyala
 * CASE201112/CR201113/LOG201121
 * changed the wms flag for DeleteInvtRecCreatedforCHKNTask rec in crete inventory
 *
 * Revision 1.15  2011/10/24 21:36:20  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Wrong Status updated in Inventory.
 * It should be 19.
 *
 * Revision 1.14  2011/09/29 12:04:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.13  2011/09/28 16:45:31  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.12  2011/09/28 11:37:38  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RMA Functionality New Files
 *
 * Revision 1.11  2011/09/24 19:47:31  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.9  2011/08/24 12:43:03  schepuri
 * CASE201112/CR201113/LOG201121
 * RF Putaway Location based on putseq no
 *
 * Revision 1.8  2011/08/23 09:42:59  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.7  2011/07/12 06:39:08  pattili
 * CASE201112/CR201113/LOG201121
 * 1. Changes to RF Putaway SKU and Putaway Location.
 *
 * Revision 1.6  2011/06/30 13:06:55  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.5  2011/06/06 14:10:04  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 * In Check-in, Remaining cube calculation and putaway logic.
 *
 * Revision 1.4  2011/06/03 06:29:22  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/

function PostItemReceipt(request, response){

	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);

	if (request.getMethod() == 'GET') {

		var poid=request.getParameter('custparam_po');
		//20125938
		var optedfield=request.getParameter('custparam_option');
		if(poid==null || poid=='null')
		{
			poid='';
		}
		//case 20125518# Start : spanish language conversion
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
		nlapiLogExecution('DEBUG', 'optedfield', optedfield);


		var st0,st1,st2,st3,st4,st5,st6;

		if( getLanguage == 'es_ES' || getLanguage == 'es_AR' )
		{
			st0 = "";
			st1 = "ENTRAR ORDEN DE COMPRA / ORDEN DE TRANSFERENCIA"; 
			st2 = "RECIBO DEL ART&#205;CULO";
			st3 = "PUBLICAR";
			st4 = "ANTERIOR";			

		}
		else
		{
			st0 = "";
			st1 = "ENTER PO/TO"; 
			st2 = "ITEM RECEIPT";
			st3 = "POST";
			st4 = "PREV";			

		}

		//case 20125518# end
		var functionkeyHtml=getFunctionkeyScript('_rf_postitemreceipt'); 
		var html = "<html><head><title>Post Item Receipt</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		//html = html + " document.getElementById('enterlocation').focus();";        
		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		//html = html + "	<form name='_rf_putaway_lp' method='POST'>";_rf_postitemreceipt
		html = html + "	<form name='_rf_postitemreceipt' method='POST'>";// Case#  20148536
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> ENTER PO#";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input type='text' name='txtpo' id='txtpo' value="+poid+">";

		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> OR TO#";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input type='text' name='txtto' id='txtto' value="+poid+">";

		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> OR RMA#";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input type='text' name='txtrma' id='txtrma' value="+poid+">";

		html = html + "				</td>";
		html = html + "			</tr>";

		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";

		html = html + "				</td>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdnoptedfield' value=" + optedfield + ">";
		html = html + "			</tr>";


		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + "  <input name='cmdSend' type='submit' value='"+ st2 +"' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "  " + st4;
		html = html + "		<input name='cmdPrevious' type='submit' value='F7' /> ";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('txtpo').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('ERROR', 'into', 'response');
		var POarray = new Array();
		var optedEvent = request.getParameter('cmdPrevious');
		var optedEvent1 = request.getParameter('cmdPrevious1');

		var optedfield = request.getParameter('hdnoptedfield');

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
		POarray["custparam_language"]=getLanguage;

		nlapiLogExecution('DEBUG', 'optedfield in post', optedfield);

		var st0,st1,st2,st3,st4,st5,st6;

		if( getLanguage == 'es_ES' || getLanguage == 'es_AR' )
		{
			st0 = "POR FAVOR ENTRAR PO / A";
			st1 = "POR FAVOR ENTRAR V&#193;LIDO PO / A"; 
			st2 = "CONFIRMACI&#243;N: RECIBO DEL ART&#237;CULO PUBLICADO CON &#193XITO";
			st3 = "PUBLICAR";
			st4 = "ANTERIOR";			
			st6=  "Confirmation:Item Receipt posting has been initiated";

		}
		else
		{
			st0 = "PLEASE ENTER ORDER#";
			st1 = "PLEASE ENTER VALID ORDER#"; 
			//case no 201410336
			st2 = "CONFIRMATION:ITEM RECEIPT POSTED SUCCESSFULLY";
			st3 = "POST";
			st4 = "PREV";			
			st5 = "POST ITEM RECEIPT IS FAILED";
			st6=  "Confirmation:Item Receipt posting has been initiated successfully";

		}

		if (sessionobj!=context.getUser()) {
			try
			{

				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}

				if (optedEvent1 !=null && optedEvent1!='' && optedEvent1 == 'F9') {
					nlapiLogExecution('ERROR', 'optedEvent if', optedEvent);
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_postitemreceipt', 'customdeploy_ebiz_rf_postitemreceipt_di', false, POarray);
				}
				else {
					if (optedEvent == 'F7') {
						nlapiLogExecution('ERROR', 'optedEvent if', optedEvent);
						response.sendRedirect('SUITELET', 'customscript_rf_receiving_menu', 'customdeploy_rf_receiving_menu_di', false, POarray);
					}
					else {

						POarray["custparam_screenno"] = 'PR01';
						var getPOid="";
						var ordertype="";
						var vcount=0;
						var getPOTxt=request.getParameter('txtpo');
						var getTOTxt=request.getParameter('txtto');
						var getRMATxt=request.getParameter('txtrma');
						if(getPOTxt!=null&&getPOTxt!="")
						{
							getPOid=getPOTxt;
							ordertype="purchaseorder";

							var ordernomain=GetSOInternalId(getPOid,ordertype);
							nlapiLogExecution('ERROR','orderno',ordernomain);

							vcount++;
							if (ordernomain== ""||ordernomain==null||ordernomain==-1)
							{
								POarray["custparam_error"] = 'PLEASE ENTER VALID PURCHASE ORDER';
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								return;
							}

						}
						else if(getTOTxt!=null&&getTOTxt!="")
						{
							getPOid=getTOTxt;
							ordertype="transferorder";
							var ordernomain=GetSOInternalId(getPOid,ordertype);
							nlapiLogExecution('ERROR','orderno',ordernomain);
							vcount++;
							if (ordernomain== ""||ordernomain==null||ordernomain==-1)
							{
								POarray["custparam_error"] = 'PLEASE ENTER VALID TRANSFER ORDER';
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								return;
							}

						}
						else if(getRMATxt!=null&&getRMATxt!="")
						{
							getPOid=getRMATxt;
							ordertype="returnauthorization";
							var ordernomain=GetSOInternalId(getPOid,ordertype);
							nlapiLogExecution('ERROR','orderno',ordernomain);
							vcount++;
							if (ordernomain== ""||ordernomain==null||ordernomain==-1)
							{
								POarray["custparam_error"] = 'PLEASE ENTER VALID RMA';
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								return;
							}
						}
						nlapiLogExecution("ERROR","getpoid/ordertype",getPOid+'/'+ordertype+'/'+vcount);
						if(getPOid==null||getPOid==""||parseInt(vcount)>1)
						{
							if(parseInt(vcount)>1)
								POarray["custparam_error"] = 'MORE THAN ONE ORDER CANT BE PROCESSED';
							else
								POarray["custparam_error"] = 'PLEASE ENTER ORDER#';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							return;
						}
						else if(getPOid !=null && getPOid!='')
						{
							// case# 201413116
							var vRoleLocation=getRoledBasedLocation();
							nlapiLogExecution('ERROR','vRoleLocation new',vRoleLocation);
							if(vRoleLocation == null || vRoleLocation =='')
								vRoleLocation=0;
							var POtrantypefilters=new Array();
							POtrantypefilters.push(new nlobjSearchFilter('tranid',null,'is',getPOid));
							POtrantypefilters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
							if(ordertype == 'transferorder'){
								if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 'null' && vRoleLocation != 0)
								{
									POtrantypefilters.push(new nlobjSearchFilter('transferlocation', null, 'anyof', vRoleLocation));
								}

							}
							else{
								if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 'null' && vRoleLocation != 0)
								{
									// Case # 20127871 starts
									//case # 20141363ï¿½
									POtrantypefilters.push(new nlobjSearchFilter('location', null, 'anyof', vRoleLocation));
									//	POfilters.push(new nlobjSearchFilter('location', null, 'anyof', ['@NONE@',vRoleLocation]));
									//Case # 20127871 end
								}
							}
							var POtrantypecols=new Array();
							POtrantypecols[0]=new nlobjSearchColumn('internalid');

							var PORecinternalids=nlapiSearchRecord(ordertype,null,POtrantypefilters,POtrantypecols);
							var poid='';
							if(PORecinternalids!=null && PORecinternalids!='')
							{
								poid=PORecinternalids[0].getValue('internalid');
								//}
								if(poid!=null&&poid!="")
									trantype = nlapiLookupField('transaction', poid, 'recordType');
								nlapiLogExecution('ERROR','trantype',trantype);


								nlapiLogExecution('ERROR','trantype',trantype);
								POarray["custparam_trantype"] = trantype;
								var POfilters=new Array();
								POfilters.push(new nlobjSearchFilter('tranid',null,'is',getPOid));
								POfilters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
								POfilters.push(new nlobjSearchFilter('recordtype',null,'is',trantype));
								//POfilters.push(new nlobjSearchFilter('recordtype',null,'is','purchaseorder'));

								if(trantype == 'transferorder'){
									if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
									{
										POfilters.push(new nlobjSearchFilter('transferlocation', null, 'anyof', vRoleLocation));
									}

								}
								else{
									if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
									{
										// Case # 20127871 starts
										//case # 20141363�
										POfilters.push(new nlobjSearchFilter('location', null, 'anyof', vRoleLocation));
										//	POfilters.push(new nlobjSearchFilter('location', null, 'anyof', ['@NONE@',vRoleLocation]));
										//Case # 20127871 end
									}
								}


								var POcols=new Array();
								POcols[0]=new nlobjSearchColumn('status');
								POcols[1]=new nlobjSearchColumn('location');
//								POcols[2]= new nlobjSearchColumn('custbody_nswms_company');

								var PORec=nlapiSearchRecord('transaction',null,POfilters,POcols);
								if(PORec!=null&&PORec!='')
								{
									var poStatus=PORec[0].getValue('status');
									nlapiLogExecution('ERROR','poStatus',poStatus);
									var poToLocationID=PORec[0].getValue('location');
									nlapiLogExecution('ERROR','poToLocation',poToLocationID);

									var fields = ['custrecord_ebizwhsite'];
									var locationcolumns = nlapiLookupField('Location', poToLocationID, fields);
									var Tomwhsiteflag = locationcolumns.custrecord_ebizwhsite;
									nlapiLogExecution('ERROR','Tomwhsiteflag',Tomwhsiteflag);
									// Call method to retrieve the PO based on the PO ID
									//Case# 201410246
									//if(poStatus=='pendingReceipt'||poStatus=='partiallyReceived' ||poStatus=='pendingBillPartReceived')
									// case 201412490 
									if(poStatus=='pendingReceipt'||poStatus=='partiallyReceived' ||poStatus=='pendingBillPartReceived'||poStatus=='pendingReceiptPartFulfilled' ||poStatus=='Pending Refund/Partially Received'||poStatus=='pendingRefundPartReceived')
									{
										var filters = new Array();
										filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2,6]));
										filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
										filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty')); 
										filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'isempty'));
										filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', poid));

										var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,null);
										nlapiLogExecution('ERROR','opentaskSearchResults',opentaskSearchResults);
										if(opentaskSearchResults!=null && opentaskSearchResults!='' && opentaskSearchResults.length>0)
										{
											//Case # 20126968 Start
											var filters = new Array();
											filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
											filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty'));
											filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty')); 
											filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'isnotempty'));
											filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', poid));

											var columns=new Array();
											columns[0]=new nlobjSearchColumn('custrecord_sku',null,'group');
											columns[1]=new nlobjSearchColumn('custrecord_line_no',null,'group');
											columns[2]=new nlobjSearchColumn('formulanumeric',null,'SUM');
											columns[2].setFormula("TO_NUMBER({custrecord_act_qty})");
											columns[3]=new nlobjSearchColumn('custrecord_sku_status',null,'group');
											// case no 20125554
											// columns[4]=new nlobjSearchColumn('custrecord_ebiz_lpno',null,'group');

											var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,columns);
											nlapiLogExecution('ERROR','opentaskSearchResults',opentaskSearchResults);



											if(opentaskSearchResults!=null && opentaskSearchResults!='' && opentaskSearchResults.length >0)
											{
												//Case # 20126968 End

												POarray["custparam_error"] = 'There are open putaway records for this PO/TO,<br>Do u want to Continue';
												POarray["custparam_poid"] = poid;
												POarray["custparam_po"] = getPOid;
												POarray["custparam_locationId"] = poToLocationID;
												response.sendRedirect('SUITELET', 'customscript_ebiz_rf_postreceiptpartial', 'customdeploy_ebiz_rf_postrcptpartial_di', false, POarray);
												return;	
												//Case # 20126968 Start
											}
											else
											{
												POarray["custparam_error"] = 'There are no putaway records for this PO/TO';
												POarray["custparam_poid"] = poid;
												POarray["custparam_po"] = getPOid;
												POarray["custparam_locationId"] = poToLocationID;
												response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
												return;									  
											}
											//Case # 20126968 End

										}
										else
										{
											var opidarr = new Array();


											var filtersopid = new Array();
											filtersopid.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
											filtersopid.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty'));
											filtersopid.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty')); 
											filtersopid.push(new nlobjSearchFilter('custrecord_actendloc', null, 'isnotempty'));
											filtersopid.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', poid));


											var opentaskSearchResultsopid=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filtersopid,null);

											for(var n=0;n<opentaskSearchResultsopid.length;n++)
											{
												opidarr.push(opentaskSearchResultsopid[n].getId());
											}


											var filters = new Array();
											filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
											filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty'));
											filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty')); 
											filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'isnotempty'));
											filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', poid));
											filters.push(new nlobjSearchFilter('internalid', null, 'anyof', opidarr));

											var columns=new Array();
											columns[0]=new nlobjSearchColumn('custrecord_sku',null,'group');
											columns[1]=new nlobjSearchColumn('custrecord_line_no',null,'group');
											columns[2]=new nlobjSearchColumn('formulanumeric',null,'SUM');
											columns[2].setFormula("TO_NUMBER({custrecord_act_qty})");
											//columns[3]=new nlobjSearchColumn('custrecord_sku_status',null,'group');
											columns[3]=new nlobjSearchColumn('custrecord_wms_location',null,'group');
											columns[4]=new nlobjSearchColumn('custrecord_item_status_location_map','custrecord_sku_status','group');
											columns[5]=new nlobjSearchColumn('custrecord_sku_status',null,'group');
											// case no 20125554
											// columns[4]=new nlobjSearchColumn('custrecord_ebiz_lpno',null,'group');

											var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,columns);
											nlapiLogExecution('ERROR','opentaskSearchResults',opentaskSearchResults);

											if(opentaskSearchResults!=null && opentaskSearchResults!='' && opentaskSearchResults.length>0)
											{
												nlapiLogExecution('ERROR','opentaskSearchResults.length',opentaskSearchResults.length);
												if(parseInt(opentaskSearchResults.length)>=50)
												{	

													nlapiLogExecution('ERROR','invoke Scheduler','done');
													var param = new Array();
													param['custscript_poid'] = poid;	
													param['custscript_trantype'] = trantype;
													param['custscript_locationid']=poToLocationID
													nlapiScheduleScript('customscript_ebiz_postitem_receipt_sch', null,param);


													var functionkeyHtml=getFunctionkeyScript('_rf_postitemreceipt'); 
													var html = "<html><head><title>Post Item Receipt</title>";
													html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
													html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
													html = html + "nextPage = new String(history.forward());";          
													html = html + "if (nextPage == 'undefined')";     
													html = html + "{}";     
													html = html + "else";     
													html = html + "{  location.href = window.history.forward();"; 
													html = html + "} ";
													html = html + " document.getElementById('enterlocation').focus();";        
													html = html + "function stopRKey(evt) { ";
													//html = html + "	  alert('evt');";
													html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
													html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
													html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
													html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
													html = html + "	  alert('System Processing, Please wait...');";
													html = html + "	  return false;}} ";
													html = html + "	} ";

													html = html + "	document.onkeypress = stopRKey; ";
													html = html + "</script>";
													html = html +functionkeyHtml;
													html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
													html = html + "	<form name='_rf_putaway_lp' method='POST'>";
													html = html + "		<table>";

													html = html + "			<tr>";
													html = html + "				<td align = 'left'>"+st6+"";

													html = html + "				</td>";
													html = html + "			</tr>";
													html = html + "			<tr>";
													html = html + "				<td align = 'left'>";
													html = html + "		"+st4;
													html = html + "		<input name='cmdPrevious1' type='submit' value='F9' /> ";
													html = html + "				</td>";
													html = html + "			</tr>";

													html = html + "		 </table>";
													html = html + "	</form>";
													html = html + "</body>";
													html = html + "</html>";

													response.write(html);


												}
												else{
													//case # 20125608 start : for different Item status
													var ItemStatusArray =new Array();
													var ItemStatusMapLocArray =new Array();
													var itemReceiptArray =new Array();
													for(var k=0;k<opentaskSearchResults.length;k++)
													{
														var	itemstatus1=opentaskSearchResults[k].getValue('custrecord_sku_status',null,'group');
														var	MapLoc=opentaskSearchResults[k].getValue('custrecord_item_status_location_map','custrecord_sku_status','group');
														nlapiLogExecution('ERROR','itemstatus1',itemstatus1);

														if(ItemStatusArray.indexOf(itemstatus1) == -1)
														{
															ItemStatusArray.push(itemstatus1);
														}
														if(ItemStatusMapLocArray.indexOf(MapLoc) == -1)
														{
															ItemStatusMapLocArray.push(MapLoc);
														}
													}
													//case 20125608 end

													if(ItemStatusMapLocArray !=null && ItemStatusMapLocArray !='' && ItemStatusMapLocArray.length>0)
													{
														nlapiLogExecution('ERROR','ItemStatusMapLocArray',ItemStatusMapLocArray.length);
														nlapiLogExecution('ERROR','ItemStatusArray',ItemStatusArray.length);
													}
													for(j=0;j<ItemStatusMapLocArray.length;j++)
													{

														//var vItemStatus = ItemStatusArray[j];
														var WHLocation = ItemStatusMapLocArray[j];
														//nlapiLogExecution('ERROR','vItemStatus',vItemStatus);
														var trecord = nlapiTransformRecord(trantype, poid, 'itemreceipt');
														for(var i=0;i<opentaskSearchResults.length;i++)
														{
															nlapiLogExecution('ERROR','inside for loop',i);

															var ActQuantity=opentaskSearchResults[i].getValue('formulanumeric',null,'SUM');
															var linenum=opentaskSearchResults[i].getValue('custrecord_line_no',null,'group');
															var	itemid=opentaskSearchResults[i].getValue('custrecord_sku',null,'group');
															var	itemstatus;
															//var	itemstatus=opentaskSearchResults[i].getValue('custrecord_sku_status',null,'group');
															//case # 20141363ÃƒÆ’Ã‚Â¯Ãƒâ€šÃ‚Â¿Ãƒâ€šÃ‚Â½
															var	batchno=opentaskSearchResults[i].getValue('custrecord_batch_no',null,'group');
															var	itemstatus=opentaskSearchResults[i].getValue('custrecord_sku_status',null,'group');

															var	MapLoc=opentaskSearchResults[i].getValue('custrecord_item_status_location_map','custrecord_sku_status','group');
															nlapiLogExecution('ERROR','MapLoc',MapLoc);
															nlapiLogExecution('ERROR','WHLocation',WHLocation);
															//var invtlp = opentaskSearchResults[i].getValue('custrecord_ebiz_lpno',null,'group');
															if(WHLocation == MapLoc)
															{
																nlapiLogExecution('ERROR','invtlp inside loop',linenum);
																nlapiLogExecution('ERROR','id inside loop for opentask',opentaskSearchResults[i].getId());

																generateItemReceipt(trecord,trantype,poid,ActQuantity,linenum,itemstatus,itemid,trantype,poid,batchno,poToLocationID,itemstatus,MapLoc);
																// generateItemReceipt(trecord,trantype,poid,ActQuantity,linenum,itemstatus,itemid,trantype,poid,'',poToLocationID,invtlp)
																nlapiLogExecution('ERROR','invtlp inside loop1111',linenum);
															}
														}
														if(trecord != null && trecord != '')
														{
															idl = nlapiSubmitRecord(trecord);
															nlapiLogExecution('ERROR','idl',idl);
															var currentrow=[ItemStatusArray[j],idl];
															itemReceiptArray.push(currentrow);
															if(SerialArray !=null && SerialArray !='' && SerialArray.length>0)
															{
																UpdateSerialNumbersStatus(SerialArray);
																SerialArray.length=0; //clear the Serial# Array
															}
														}
													}
													nlapiLogExecution('ERROR','idl',idl);
													if(idl!=null && idl!='')
													{									 
														if(trantype=='transferorder')
															InvokeInventoryTransfer(poid);
														if(itemReceiptArray !=null && itemReceiptArray !='' && itemReceiptArray.length>0)
														{
															nlapiLogExecution('ERROR','itemReceiptArray',itemReceiptArray);
															for (var cnt=0;cnt<itemReceiptArray.length ;cnt++)
															{
																var vItemstatus=itemReceiptArray[cnt][0];
																var IRid=itemReceiptArray[cnt][1];
																//nlapiLogExecution('ERROR','IRid',IRid);
																var SeachResults=OpentaskrecordsforItemStatus(poid,vItemstatus);

																for(var M=0;M<SeachResults.length;M++)
																{
																	nlapiLogExecution('ERROR','SeachResults[M]',SeachResults[M]);
																	var updateStatus = nlapiSubmitField('customrecord_ebiznet_trn_opentask', SeachResults[M].getId(), 'custrecord_ebiz_nsconfirm_ref_no', IRid);
																}

															}

														}
														/*var filters2 = new Array();
														filters2.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
														filters2.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty'));
														//filters2.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty')); 
														filters2.push(new nlobjSearchFilter('custrecord_actendloc', null, 'isnotempty'));
														filters2.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', poid));
														var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters2,null);*/
														if(opidarr!=null && opidarr!='' && opidarr.length>0)
														{
															nlapiLogExecution('ERROR','opentaskSearchResults.length',opidarr.length);
															for(var j=0;j<opidarr.length;j++)
															{
																nlapiLogExecution('ERROR','opentaskSearchResultsID',opidarr[j]);
																// case no start 20126855
																MoveTaskRecord(opidarr[j],idl);

																nlapiLogExecution('ERROR','idlgd11',idl);
																// case no end 20126855
																nlapiLogExecution('ERROR','idl11',idl);
															}
														}
														nlapiLogExecution('ERROR','idl122221',idl);
														var functionkeyHtml=getFunctionkeyScript('_rf_postitemreceipt'); 
														var html = "<html><head><title>Post Item Receipt</title>";
														html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
														html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
														html = html + "nextPage = new String(history.forward());";          
														html = html + "if (nextPage == 'undefined')";     
														html = html + "{}";     
														html = html + "else";     
														html = html + "{  location.href = window.history.forward();"; 
														html = html + "} ";
														html = html + " document.getElementById('enterlocation').focus();";        
														html = html + "function stopRKey(evt) { ";
														//html = html + "	  alert('evt');";
														html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
														html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
														html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
														html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
														html = html + "	  alert('System Processing, Please wait...');";
														html = html + "	  return false;}} ";
														html = html + "	} ";

														html = html + "	document.onkeypress = stopRKey; ";
														html = html + "</script>";
														html = html +functionkeyHtml;
														html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
														//html = html + "	<form name='_rf_putaway_lp' method='POST'>";
														html = html + "	<form name='_rf_postitemreceipt' method='POST'>";// Case# 20148536
														html = html + "		<table>";

														html = html + "			<tr>";
														html = html + "				<td align = 'left'>"+st2+"";

														html = html + "				</td>";
														html = html + "			</tr>";
														html = html + "			<tr>";
														html = html + "				<td align = 'left'>";
														html = html + "		"+st4;
														html = html + "		<input name='cmdPrevious1' type='submit' value='F9' /> ";
														html = html + "				</td>";
														html = html + "			</tr>";

														html = html + "		 </table>";
														html = html + "	</form>";
														html = html + "</body>";
														html = html + "</html>";

														response.write(html);
													}
													nlapiLogExecution('ERROR','idl123441',idl);
												}
											}
											else{
												POarray["custparam_error"] = st1;//'Please Enter Valid PO/TO';
												response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
												return;									  
											}

										}				
									}
									else
									{
										POarray["custparam_error"] = st1;//'Please Enter Valid PO/TO';
										response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
										return;	
									}
								}
								else
								{
									POarray["custparam_error"] =st1; //'Please Enter Valid PO/TO';
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
									return;	
								}
							}
							else
							{
								POarray["custparam_error"] = st1;//'Please Enter Valid PO/TO';
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								return;	
							}

						}
						else
						{
							POarray["custparam_error"] = st0;//'Please Enter PO/TO';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							return;	
						}
					}
				}
			}
			catch (e)  {
				POarray["custparam_error"] = st5;
				nlapiLogExecution('ERROR', 'exception in postitemreceipt',e);
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');				
			}
		}
		else
		{
			POarray["custparam_screenno"] = 'PR01';
			POarray["custparam_error"] = 'ORDER ALREADY IN PROCESS';			
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);

		}

	}
}

function InvokeInventoryTransfer(poid)
{
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty')); 
	filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'isnotempty'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', poid));

	var columns=new Array();
	columns[0]=new nlobjSearchColumn('custrecord_sku',null,'group');
	columns[1]=new nlobjSearchColumn('custrecord_line_no',null,'group');
	columns[2]=new nlobjSearchColumn('formulanumeric',null,'SUM');
	columns[2].setFormula("TO_NUMBER({custrecord_act_qty})");
	columns[3]=new nlobjSearchColumn('custrecord_sku_status',null,'group');
	columns[4]=new nlobjSearchColumn('custrecord_wms_location',null,'group');
	columns[5]=new nlobjSearchColumn('custrecord_batch_no',null,'group');

	var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,columns);
	if(opentaskSearchResults!=null && opentaskSearchResults!='')
	{
		for(var k=0;k<opentaskSearchResults.length;k++)
		{
			var poLine = opentaskSearchResults[k].getValue('custrecord_line_no',null,'group');
			var poItem = opentaskSearchResults[k].getValue('custrecord_sku',null,'group');
			var poQty = opentaskSearchResults[k].getValue('formulanumeric',null,'SUM');
			var poItemStatus = opentaskSearchResults[k].getValue('custrecord_sku_status',null,'group');
			var poLocation = opentaskSearchResults[k].getValue('custrecord_wms_location',null,'group');
			var poBatch = opentaskSearchResults[k].getValue('custrecord_batch_no',null,'group');
			var vItemStatuslocation='';
			var Invtrymveloc='';

			if(poItemStatus!=null && poItemStatus!='')
				vItemStatuslocation = getItemStatusMapLoc(poItemStatus);

			if(poLocation!=vItemStatuslocation)
			{

				Invtrymveloc =  InvokeNSInventoryTransfer(poItem,poItemStatus,poLocation,
						vItemStatuslocation,poQty,poBatch,null);
			}
		}
	}
}

function  InvokeNSInventoryTransfer(item,itemstatus,loc,toloc,qty,lot,serialno)
{
	try{
		nlapiLogExecution('ERROR', 'Into InvokeNSInventoryTransfer');		
		nlapiLogExecution('ERROR', 'item', item);		
		nlapiLogExecution('ERROR', 'itemstatus', itemstatus);
		nlapiLogExecution('ERROR', 'loc', loc);
		nlapiLogExecution('ERROR', 'toloc', toloc);
		nlapiLogExecution('ERROR', 'qty', qty);
		nlapiLogExecution('ERROR', 'lot', lot);
		nlapiLogExecution('ERROR', 'test1', 'test1');

		var ItemType='';

		if(item!= null && item != "")
		{
			var columns = nlapiLookupField('item', item, [ 'recordType' ]);
			ItemType = columns.recordType;
		}

		nlapiLogExecution('ERROR', 'ItemType', ItemType);
		var confirmLotToNS='N';
		confirmLotToNS=GetConfirmLotToNS(loc);
		var vItemname;	

		var filters = new Array();          
		filters.push(new nlobjSearchFilter('internalid', null, 'is',item));
		//Changed on 30/4/12 by suman
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		//End of changes as on 30/4/12
		var columns = new Array();		
		columns[0] = new nlobjSearchColumn('itemid');
		var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
		if (itemdetails !=null) 
		{
			vItemname=itemdetails[0].getValue('itemid');
		}

		var invttransfer = nlapiCreateRecord('inventorytransfer');			

		invttransfer.setFieldValue('location', loc);//from Location
		invttransfer.setFieldValue('transferlocation', toloc);
		invttransfer.selectNewLineItem('inventory');
		invttransfer.setCurrentLineItemValue('inventory', 'item', item);		
		invttransfer.setCurrentLineItemValue('inventory', 'adjustqtyby', parseFloat(qty));
		invttransfer.setCurrentLineItemValue('inventory', 'unitconversionrate', '1');


		var subs = nlapiGetContext().getFeature('subsidiaries');
		nlapiLogExecution('ERROR', 'subs', subs);
		if(subs==true)
		{
			var vSubsidiaryVal=getSubsidiaryNew(loc);
			nlapiLogExecution('ERROR', 'vSubsidiaryVal', vSubsidiaryVal);
			if(vSubsidiaryVal != null && vSubsidiaryVal != '')
				invttransfer.setFieldValue('subsidiary', vSubsidiaryVal);			
		}


		if(lot!=null && lot!='')
		{
			//vItemname=vItemname.replace(" ","-");
			vItemname=vItemname.replace(/ /g,"-");
			if(parseFloat(qty)<0)
				qty=parseFloat(qty)*(-1);
			if(confirmLotToNS=='N')
				lot=vItemname;	

			//For advanced Bin serial Lot management check
			var vAdvBinManagement=false;
			var ctx = nlapiGetContext();
			if(ctx != null && ctx != '')
			{
				if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
					vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
			}  
			nlapiLogExecution('ERROR', 'vAdvBinManagement', vAdvBinManagement);

			if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
			{
				var compSubRecord = invttransfer.createCurrentLineItemSubrecord('inventory','inventorydetail');
				nlapiLogExecution('ERROR', 'test1', 'test1');
				compSubRecord.selectNewLineItem('inventoryassignment');
				compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', qty);
				//compSubRecord.setCurrentLineItemValue('inventoryassignment', 'binnumber', vSubsidiaryVal);
				compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lot);

				compSubRecord.commitLineItem('inventoryassignment');
				compSubRecord.commit();
			}
			else
			{
				if(parseFloat(qty)<0)
					qty=parseFloat(qty)*(-1);

				if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem") 
				{
					nlapiLogExecution('ERROR', 'serialnoNowithQty', serialno);
					invttransfer.setCurrentLineItemValue('inventory', 'serialnumbers',  serialno);
				}	
				else
				{
					nlapiLogExecution('ERROR', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");
					invttransfer.setCurrentLineItemValue('inventory', 'serialnumbers', lot + "(" + parseFloat(qty) + ")");
				}
			}
		}

		invttransfer.commitLineItem('inventory'); 
		nlapiSubmitRecord(invttransfer, true, true);

		nlapiLogExecution('ERROR', 'type argument', 'type is create');
	}


	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in InvokeNSInventoryTransferNew ', exp);	

	}
}
var SerialArray=new Array();
//function generateItemReceipt(trecord,fromrecord,fromid,ActQuantity,linenum,itemstatus,itemid,trantype,pointid,batchno,templocation,invtlp)
function generateItemReceipt(trecord,fromrecord,fromid,ActQuantity,linenum,itemstatus,itemid,trantype,pointid,batchno,templocation,vItemStatus,MapLoc)
{
	try {


		nlapiLogExecution('ERROR', "Into GenerateItemReceipt");
		nlapiLogExecution('ERROR', "vItemStatus", vItemStatus);
		var compSubRecord=null;
		var polinelength = trecord.getLineItemCount('item');
		nlapiLogExecution('ERROR', "polinelength", polinelength);
		var idl;
		var vAdvBinManagement=true;
		var tempserialId = "";
		var tempSerial = "";
		var ctx = nlapiGetContext();
		if(ctx != null && ctx != '')
		{
			if(ctx.getFeature('advbinseriallotmgmt').toString() != null && ctx.getFeature('advbinseriallotmgmt').toString() != '')
				vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
			nlapiLogExecution('ERROR', 'vAdvBinManagement_if', vAdvBinManagement);
		}
		//}  
		nlapiLogExecution('ERROR', 'vAdvBinManagement', vAdvBinManagement);
		for (var j = 1; j <= polinelength; j++) {
			nlapiLogExecution('ERROR', "Get Item Id", trecord.getLineItemValue('item', 'item', j));
			nlapiLogExecution('ERROR', "Get Item Text", trecord.getLineItemText('item', 'item', j));

			var item_id = trecord.getLineItemValue('item', 'item', j);
			var itemrec = trecord.getLineItemValue('item', 'itemreceive', j);
			var itemLineNo = trecord.getLineItemValue('item', 'line', j);
			//var poItemUOM = trecord.getLineItemValue('item', 'unitsdisplay', j);//text
			var poItemUOM = trecord.getLineItemValue('item', 'units', j);//value

			var commitflag = 'N';
			var quantity=0;
			quantity = ActQuantity;

			nlapiLogExecution('ERROR', 'quantity', quantity);

			var inventorytransfer="";
			nlapiLogExecution('ERROR', "itemLineNo(PO/TO)", itemLineNo);
			nlapiLogExecution('ERROR', "linenum(Task)", linenum);
			nlapiLogExecution('ERROR', "poItemUOM", poItemUOM);

			if(trantype!='transferorder')
			{
				//if (itemLineNo == linenum && vItemStatus == itemstatus) {
				if (itemLineNo == linenum) {

					//This part of the code will fetch the line level location.
					//If line level is empty then header level location is considered.
					//code added by suman on 080513.
					whLocation=trecord.getLineItemValue('item', 'location', j);//value
					if(whLocation==null||whLocation=="")
						whLocation=templocation;
					//End of code as of 080513.

					//var vrcvqty=arrQty[i];
					var vrcvqty=quantity;
					var vbaseuomqty=0;
					var vuomqty=0;
					var eBizItemDims=geteBizItemDimensions(item_id);
					if(poItemUOM!=null && poItemUOM!='')
					{
						/*var vbaseuomqty=0;
						var vuomqty=0;
						var eBizItemDims=geteBizItemDimensions(item_id);*/
						if(eBizItemDims!=null&&eBizItemDims.length>0)
						{
							nlapiLogExecution('ERROR', 'Dims Length', eBizItemDims.length);
							for(var z=0; z < eBizItemDims.length; z++)
							{
								if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
								{
									vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
								}
								nlapiLogExecution('ERROR', 'poItemUOM', poItemUOM);
								nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
								nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
								if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
								{
									vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
								}
							}
							if(vuomqty==null || vuomqty=='')
							{
								vuomqty=vbaseuomqty;
							}
							nlapiLogExecution('ERROR', 'vbaseuomqty', vbaseuomqty);
							nlapiLogExecution('ERROR', 'vuomqty', vuomqty);

							if(quantity==null || quantity=='' || isNaN(quantity))
								quantity=0;
							else
								quantity = (parseFloat(quantity)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

						}
					}
					//Code Added by Ramana
					var varItemlocation;
					nlapiLogExecution('ERROR', 'itemstatus', itemstatus);
					nlapiLogExecution('ERROR', 'MapLoc', MapLoc);
					nlapiLogExecution('ERROR', 'quantity', quantity);
					varItemlocation=MapLoc;
					//varItemlocation = getItemStatusMapLoc(itemstatus);
					//upto to here

					commitflag = 'Y';

					var fields = ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot'];
					var columns = nlapiLookupField('item', itemid, fields);
					if(columns != null && columns != '')
					{
						Itype = columns.recordType;	


						serialInflg = columns.custitem_ebizserialin;
					}

					nlapiLogExecution('ERROR', 'Value of J', j);
					if (Itype != "serializedinventoryitem" || serialInflg != "T")
					{
						trecord.selectLineItem('item', j);
						trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
						trecord.setCurrentLineItemValue('item', 'quantity', parseFloat(quantity).toFixed(5));
						trecord.setCurrentLineItemValue('item', 'location', varItemlocation); //Added by ramana
					}

					nlapiLogExecution('ERROR', 'Serialquantity', parseFloat(quantity).toFixed(5));
					nlapiLogExecution('ERROR', 'Into SerialNos');
					var totalconfirmedQty=0;
					var serialnumcsv = "";
					tempserialId = "";
					tempSerial = "";
					if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem" ) {

						var filters = new Array();
						filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
						filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty'));
						filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty')); 
						filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'isnotempty'));
						filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', pointid));
						//case# 20149597 starts(added filter lineno)
						filters.push(new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum));
						filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'greaterthan', 0));
						//case# 20149597 ends
						//case 20125736 start :
						var columns=new Array();
						/*columns[0]=new nlobjSearchColumn('custrecord_sku',null,'group');
						columns[1]=new nlobjSearchColumn('custrecord_line_no',null,'group');
						columns[2]=new nlobjSearchColumn('formulanumeric',null,'SUM');
						columns[2].setFormula("TO_NUMBER({custrecord_act_qty})");
						columns[3]=new nlobjSearchColumn('custrecord_sku_status',null,'group');
						columns[4]=new nlobjSearchColumn('custrecord_ebiz_lpno',null,'group');*/
						columns[0]=new nlobjSearchColumn('custrecord_invref_no');
						columns[1]=new nlobjSearchColumn('custrecord_line_no');
						columns[2]=new nlobjSearchColumn('custrecord_sku_status');
						columns[3]=new nlobjSearchColumn('custrecord_ebiz_lpno');
						columns[4]=new nlobjSearchColumn('custrecord_sku');
						columns[5]=new nlobjSearchColumn('custrecord_act_qty');

						var opentaskSearchResultsforlp=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,columns);

						if(opentaskSearchResultsforlp != null && opentaskSearchResultsforlp != '')
						{
							for(var k=0;k<opentaskSearchResultsforlp.length;k++)
							{

								var invtlp = opentaskSearchResultsforlp[k].getValue('custrecord_ebiz_lpno');
								var vsku = opentaskSearchResultsforlp[k].getValue('custrecord_sku');
								var	lineitemstatus = opentaskSearchResultsforlp[k].getValue('custrecord_sku_status');
								var linenumber  = opentaskSearchResultsforlp[k].getValue('custrecord_line_no');
								var invtrefno = opentaskSearchResultsforlp[k].getValue('custrecord_invref_no');
								var Itypevsku = '';
								var serialInflgvsku = '';
								var fields = ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot'];
								var columns = nlapiLookupField('item', vsku, fields);
								if(columns != null && columns != '')
								{
									Itypevsku = columns.recordType;	
									serialInflgvsku = columns.custitem_ebizserialin;
								}

								//if (itemLineNo == linenumber && vItemStatus == lineitemstatus)
								if (itemLineNo == linenumber)
								{
									if (Itypevsku == "serializedinventoryitem" || serialInflgvsku == "T" ||Itypevsku == "serializedassemblyitem") 
									{
										nlapiLogExecution('ERROR', 'Into SerialNos of Itypevsku',Itypevsku);
										nlapiLogExecution('ERROR', 'Into SerialNos of serialInflgvsku',serialInflgvsku);
										//nlapiLogExecution('ERROR', 'invtlp', invtlp);	
										nlapiLogExecution('ERROR', 'pointid', pointid);	
										nlapiLogExecution('ERROR', 'linenum', linenum);	
										nlapiLogExecution('ERROR', 'invtlp', invtlp);
										nlapiLogExecution('ERROR', 'invtrefno', invtrefno);
										var serialline = new Array();
										var serialId = new Array();


										var vMergeLP='';
										var vLP='';
										if(invtrefno !=null && invtrefno !='')
										{
											var invttransaction = nlapiLoadRecord('customrecord_ebiznet_createinv',invtrefno);											

											vMergeLP = invttransaction.getFieldValue('custrecord_ebiz_inv_lp');

										}
										nlapiLogExecution('ERROR', 'vMergeLP', vMergeLP);
										nlapiLogExecution('ERROR', 'Before_vLP', invtlp);

										if(vMergeLP !=null && vMergeLP!='')
										{
											if(vMergeLP !=invtlp)
											{
												vLP=vMergeLP;
											}
											else
											{
												vLP=invtlp;
											}

										}
										else
										{
											vLP=invtlp;
										}
										nlapiLogExecution('ERROR', 'After_vLP', vLP);


										/*var vfilters = new Array();
										vfilters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
										vfilters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [3]);
										vfilters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
										var vcolumns = new Array();
										vcolumns[0] = new nlobjSearchColumn('custrecord_serialparentid');
										var serialnumcsv = "";
										var serchRecres = nlapiSearchRecord('customrecord_ebiznetserialentry', null, vfilters, vcolumns);

										var newmergelp;
										if (serchRecres!=null && serchRecres!='') {
											newmergelp = serchRecres[0].getValue('custrecord_serialparentid');
											nlapiLogExecution('ERROR', 'newmergelp', newmergelp);	
										}*/

										/*if(invtlp!=newmergelp)
											invtlp = newmergelp;*/

										nlapiLogExecution('ERROR', 'invtlp if merged', invtlp);	
										//var tempSerial = "";
										var filters = new Array();


//										filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
//										filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
//										filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
//										filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);

										if(trantype=='returnauthorization')
										{
											filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
											filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['1','3','19']);
											filters[2] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);

										}
										else
										{
											filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
											filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['1','3','19']);
											filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
											//if(invtlp == newmergelp)
											//filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
											//if(vLP == invtlp)
											//	filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
											//filters[3] = new nlobjSearchFilter('custrecord_serial_lpno', null, 'is', invtlp);
											//filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', vMergeLP);

										}
										filters[3] = new nlobjSearchFilter('custrecord_serialstatus', null, 'doesnotstartwith', 'P');
										var columns = new Array();
										columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
										columns[0].setSort(false);
										var serialnumcsv = "";

										tempserialId = "";
										tempSerial = "";
										var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
										var serialnumArray=new Array();
										var tempQtyforinvDetail;

										if (serchRec) {

											/*if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
											{
												compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');

												tempQtyforinvDetail=1;

											}*/
											nlapiLogExecution('ERROR', 'invtlp', invtlp);		
											nlapiLogExecution('ERROR', 'serchRec.length', serchRec.length);		
											for (var n = 0; n < serchRec.length; n++) {
												//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
												nlapiLogExecution('ERROR', 'tempserialId if empty', tempserialId);
												if (tempserialId == "" || tempserialId== '' || tempserialId==null ) {
													tempserialId = serchRec[n].getId();
													nlapiLogExecution('ERROR', 'tempserialId', tempserialId);	
													SerialArray.push(serchRec[n].getId()); //capturing serialnumbers internal id's
												}
												else {
													tempserialId = tempserialId + "," + serchRec[n].getId();
													nlapiLogExecution('ERROR', 'tempserialId here', tempserialId);
													SerialArray.push(serchRec[n].getId()); //capturing serialnumbers internal id's
												}

												//This is for Serial num loopin with Space separated.
												if (tempSerial == "") {
													tempSerial = serchRec[n].getValue('custrecord_serialnumber');

													serialnumArray.push(serchRec[n].getValue('custrecord_serialnumber'));

													/*	if(vAdvBinManagement)
													{
														//var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
														compSubRecord.selectNewLineItem('inventoryassignment');
														compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', tempQtyforinvDetail);
														compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serchRec[n].getValue('custrecord_serialnumber'));

														compSubRecord.commitLineItem('inventoryassignment');
														//compSubRecord.commit();
													}
													 */
												}
												else {

													tempSerial = tempSerial + " " + serchRec[n].getValue('custrecord_serialnumber');
													/*if(vAdvBinManagement)
													{
														//var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
														compSubRecord.selectNewLineItem('inventoryassignment');
														compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', tempQtyforinvDetail);
														compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serchRec[n].getValue('custrecord_serialnumber'));

														compSubRecord.commitLineItem('inventoryassignment');
														//compSubRecord.commit();
													}*/
													if(tempSerial.length>3500)
													{
														tempSerial='';
														trecord.selectLineItem('item', j);
														trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
														var tempqty=serialnumArray.length;//parseInt(n-1)+parseInt(1);
														trecord.setCurrentLineItemValue('item', 'quantity', tempqty);

														trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
														trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumArray);
														nlapiLogExecution('ERROR', 'serialnumArray', serialnumArray);
														if(vAdvBinManagement)
														{
															compSubRecord.commit();
														}
														trecord.commitLineItem('item');
														idl = nlapiSubmitRecord(trecord);
														serialnumArray=new Array();
														totalconfirmedQty=parseInt(totalconfirmedQty)+parseInt(tempqty);
														if(n<serchRec.length)
														{
															var tQty=parseInt(quantity)-parseInt(totalconfirmedQty);
															nlapiLogExecution('ERROR', 'tQty1', tQty);
															var trecord = nlapiTransformRecord(fromrecord, fromid, 'itemreceipt');
															var polinelength = trecord.getLineItemCount('item');
															/*for (var j3 = 1; j3 <= polinelength; j3++) {*/
															var item_id = trecord.getLineItemValue('item', 'item', itemLineNo);
															var itemrec = trecord.getLineItemValue('item', 'itemreceive', itemLineNo);
															var itemLineNo = trecord.getLineItemValue('item', 'line', itemLineNo);
															var poItemUOM = trecord.getLineItemValue('item', 'unitsdisplay', itemLineNo);

															/*if (itemLineNo == linenum)
											{*/
															if(poItemUOM!=null && poItemUOM!='')
															{
																var vbaseuomqty=0;
																var vuomqty=0;
																var eBizItemDims=geteBizItemDimensions(item_id);
																if(eBizItemDims!=null&&eBizItemDims.length>0)
																{
																	nlapiLogExecution('ERROR', 'Dims Length', eBizItemDims.length);
																	for(var z=0; z < eBizItemDims.length; z++)
																	{
																		if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
																		{
																			vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
																		}
																		nlapiLogExecution('ERROR', 'poItemUOM', poItemUOM);
																		nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
																		nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
																		if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
																		{
																			vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
																		}
																	}
																	if(vuomqty==null || vuomqty=='')
																	{
																		vuomqty=vbaseuomqty;
																	}
																	nlapiLogExecution('ERROR', 'vbaseuomqty', vbaseuomqty);
																	nlapiLogExecution('ERROR', 'vuomqty', vuomqty);

																	if(tQty==null || tQty=='' || isNaN(tQty))
																		tQty=0;
																	else
																		tQty = (parseFloat(tQty)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

																}
															}
															nlapiLogExecution('ERROR', 'tQtylast', tQty);
															trecord.selectLineItem('item', itemLineNo);
															trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
															var tempqty=parseInt(n)+parseInt(1);
															trecord.setCurrentLineItemValue('item', 'quantity', tQty);

															trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
															//}

															//}
														}






													}
													serialnumArray.push(serchRec[n].getValue('custrecord_serialnumber'));

												}


												if(parseInt(n)== parseInt(quantity-1))
													break;




											}


											if(vAdvBinManagement)
											{
												nlapiLogExecution('ERROR', 'serialnumArray', serialnumArray.length);
												//var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');


												var compSubRecord = trecord.editCurrentLineItemSubrecord('item','inventorydetail');
												nlapiLogExecution('ERROR', 'compSubRecord',compSubRecord);
												if(compSubRecord !=null && compSubRecord!='')
												{
													compSubRecord.selectLineItem('inventoryassignment', 1);
												}
												else
												{
													nlapiLogExecution('ERROR', 'Create',compSubRecord);
													compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');

												}

												if(serialnumArray!=null && serialnumArray!='')
												{
													for(var k1 = 0; k1<serialnumArray.length;k1++)
													{
														nlapiLogExecution('ERROR', 'Serial@No', serialnumArray[k1]);
														compSubRecord.selectNewLineItem('inventoryassignment');
														compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
														compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serialnumArray[k1]);

														compSubRecord.commitLineItem('inventoryassignment');

													}
													compSubRecord.commit();	

												}

											}
											/*if(vAdvBinManagement)
											{
												compSubRecord.commit();	
											}*/
											/*nlapiLogExecution('ERROR', 'qtyexceptionFlag', qtyexceptionFlag);
							if(qtyexceptionFlag == "true")
							{
								serialnumcsv = getScannedSerialNoinQtyexception;
								nlapiLogExecution('ERROR', 'Serial nums assigned when qtyexceptionFlag is true', serialnumcsv);
							}
							else
							{*/
											serialnumcsv = tempSerial;
											nlapiLogExecution('ERROR', 'Serial nums assigned when qtyexceptionFlag is false', serialnumcsv);
											//}


										}
									}
								}
							}
						}
						tempSerial = "";
						nlapiLogExecution('ERROR', 'Serial nums assigned', serialnumcsv);
						nlapiLogExecution('ERROR', 'Serial nums tempserialId', tempserialId);
						if(!vAdvBinManagement)
						{
							if ((Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem")  && (trecord!=null && trecord!='')) {
								//trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumArray);
								//case 	201412778
								if(serialnumcsv!=null && serialnumcsv!='')
								{
									trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumcsv);
								}
								else	
								{
									trecord.setCurrentLineItemValue('item', 'serialnumbers', tempserialId);
								}
							}
						}


					}
					else 
						if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem")
						{
							//if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem"  || Itype == "assemblyitem") {
							nlapiLogExecution('ERROR', 'Into LOT/Batch');
							//nlapiLogExecution('ERROR', 'LP:', invtlp);
							var tempBatch = "";
							var batchcsv = "";
							var confirmLotToNS='Y';

							confirmLotToNS=GetConfirmLotToNS(varItemlocation);
							nlapiLogExecution('ERROR', 'confirmLotToNS:', confirmLotToNS);



							if(!vAdvBinManagement)
							{
								var vItemname;

								if(confirmLotToNS == 'Y')
								{
									var filters = new Array();
									filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
									filters.push(new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum));
									filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof',itemid));								 
									filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', pointid));
									if(itemstatus!=null && itemstatus!='')
										filters.push(new nlobjSearchFilter('custrecord_sku_status', null, 'anyof',itemstatus));
									var columns=new Array();
									columns[0]=new nlobjSearchColumn('custrecord_lotnowithquantity');							  

									var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,columns);
									nlapiLogExecution('ERROR','opentaskSearchResults',opentaskSearchResults);
									var tempBatch='';
									if(opentaskSearchResults!=null && opentaskSearchResults.length>0)
									{
										nlapiLogExecution('ERROR','opentaskSearchResults.length',opentaskSearchResults.length);
										for(var k1=0;k1<opentaskSearchResults.length;k1++)
										{
											if(tempBatch!='')
												tempBatch = tempBatch +','+opentaskSearchResults[k1].getValue('custrecord_lotnowithquantity');
											else
												tempBatch = opentaskSearchResults[k1].getValue('custrecord_lotnowithquantity');  
										}
									}
									nlapiLogExecution('ERROR','tempbatch',tempBatch);


								}
								else
								{
									nlapiLogExecution('ERROR', 'item_id:', item_id);
									var filters1 = new Array();          
									filters1[0] = new nlobjSearchFilter('internalid', null, 'is',item_id);
									var columns = new Array();

									columns[0] = new nlobjSearchColumn('itemid');

									var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
									if (itemdetails !=null) 
									{
										vItemname=itemdetails[0].getValue('itemid');
										nlapiLogExecution('ERROR', 'vItemname:', vItemname);
										//var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
										vItemname=vItemname.replace(/ /g,"-");
										tempBatch=vItemname + '('+ quantity + ')';


									}
								}
								trecord.setCurrentLineItemValue('item', 'serialnumbers', tempBatch);
							}
							else
							{
								var vItemname;
								nlapiLogExecution('ERROR', 'item_id1:', itemid);
								var filters1 = new Array();          
								filters1[0] = new nlobjSearchFilter('internalid', null, 'is',itemid);
								var columns = new Array();

								columns[0] = new nlobjSearchColumn('itemid');

								var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
								if (itemdetails !=null) 
								{
									vItemname=itemdetails[0].getValue('itemid');
									nlapiLogExecution('ERROR', 'vItemname1:', vItemname);
									//var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
									vItemname=vItemname.replace(/ /g,"-");
									tempBatch=vItemname + '('+ quantity + ')';


								}



								var filters = new Array();
								filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
								filters.push(new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum));
								filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof',itemid));								 
								filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', pointid));
								nlapiLogExecution('ERROR', 'itemstatus:', itemstatus);
								if(itemstatus!=null && itemstatus!='')
									filters.push(new nlobjSearchFilter('custrecord_sku_status', null, 'anyof',itemstatus));
								filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'greaterthan', 0));
								filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty'));
								//if(MapLoc!=null && MapLoc!='')
								//filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', MapLoc));


								var columns=new Array();
								columns[0]=new nlobjSearchColumn('custrecord_lotnowithquantity');							  
								columns[1]=new nlobjSearchColumn('custrecord_batch_no');
								columns[2]=new nlobjSearchColumn('custrecord_act_qty');
								var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,columns);
								nlapiLogExecution('ERROR','opentaskSearchResults123',opentaskSearchResults);
								var tempBatch='';
								if(opentaskSearchResults!=null && opentaskSearchResults.length>0)
								{
									var compSubRecord = trecord.editCurrentLineItemSubrecord('item','inventorydetail');
									var	editSubrecord='T';
									nlapiLogExecution('ERROR','compSubRecord ',compSubRecord);
									var subrecordcount=0;
									if(compSubRecord !=null && compSubRecord !='')
									{
										subrecordcount=compSubRecord.getLineItemCount('inventoryassignment')
										nlapiLogExecution('ERROR','subrecordcount ',subrecordcount);
									}
									if(compSubRecord ==null || compSubRecord =='')
									{
										nlapiLogExecution('ERROR','compSubRecord create',compSubRecord);
										editSubrecord='F';
										compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
									}
									nlapiLogExecution('ERROR','opentaskSearchResults.length',opentaskSearchResults.length);
									for(var k1=0;k1<opentaskSearchResults.length;k1++)
									{

										//case # 20141363ÃƒÂ¯Ã‚Â¿Ã‚Â½start
										var batch = opentaskSearchResults[k1].getValue('custrecord_batch_no');
										//var batch = opentaskSearchResults[0].getValue('custrecord_batch_no');
										var quantity =opentaskSearchResults[k1].getValue('custrecord_act_qty');
										nlapiLogExecution('ERROR','opentask batch',batch);
										nlapiLogExecution('ERROR','opentask quantity',quantity);
										nlapiLogExecution('ERROR', 'editSubrecord', editSubrecord);
										if(poItemUOM!=null && poItemUOM!='')
										{
											/*var vbaseuomqty=0;
											var vuomqty=0;
											var eBizItemDims=geteBizItemDimensions(item_id);*/
											if(eBizItemDims!=null&&eBizItemDims.length>0)
											{
												nlapiLogExecution('ERROR', 'Dims Length', eBizItemDims.length);
												for(var z=0; z < eBizItemDims.length; z++)
												{
													if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
													{
														vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
													}
													nlapiLogExecution('ERROR', 'poItemUOM', poItemUOM);
													nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
													nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
													if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
													{
														vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
													}
												}
												if(vuomqty==null || vuomqty=='')
												{
													vuomqty=vbaseuomqty;
												}
												nlapiLogExecution('ERROR', 'vbaseuomqty', vbaseuomqty);
												nlapiLogExecution('ERROR', 'vuomqty', vuomqty);

												if(quantity==null || quantity=='' || isNaN(quantity))
													quantity=0;
												else
													quantity = (parseFloat(quantity)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

											}
										}
										nlapiLogExecution('ERROR', 'after conversion', quantity);	
										if(editSubrecord=='F')
										{
											nlapiLogExecution('ERROR','editSubrecord',editSubrecord);
											compSubRecord.selectNewLineItem('inventoryassignment');

										}
										else
										{
											try
											{
												if(subrecordcount<=opentaskSearchResults.length)
												{

													compSubRecord.selectLineItem('inventoryassignment',(parseInt(k1)+1));
												}
												else
												{
													nlapiLogExecution('ERROR','Newline','Newline');
													compSubRecord.selectNewLineItem('inventoryassignment');
												}

											}
											catch(e)
											{												
												compSubRecord.selectNewLineItem('inventoryassignment');
											}

										}
										//compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', lotquantity);
										nlapiLogExecution('ERROR','batchno',batchno);
										nlapiLogExecution('ERROR','quantity',quantity);
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', parseFloat(quantity).toFixed(5));
										if(confirmLotToNS == 'Y')
											compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', batch);
										else
											compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
										compSubRecord.commitLineItem('inventoryassignment');

										//case # 20141363ÃƒÂ¯Ã‚Â¿Ã‚Â½end


									}
									//if(compSubRecord != null)
									compSubRecord.commit();
								}
								//nlapiLogExecution('ERROR','tempbatch',tempBatch);

							}

						}

					if(trecord!=null  && trecord!='')
					{
//						if(vAdvBinManagement)
//						{
//						if(compSubRecord!=null)
//						compSubRecord.commit();
//						}
						trecord.commitLineItem('item');
					}
				}
			}
			//}// case# 201414550
			else
			{ 
//				20125998
				nlapiLogExecution('ERROR', 'itemid (Task)in TO', itemid);	
				nlapiLogExecution('ERROR', 'item_id (Line) in TO', item_id);	
				//if(vTORestrictFlag =='F')
				//{	
				itemLineNo=parseFloat(itemLineNo)-2;
				if (itemid == item_id && itemLineNo == linenum) {

					commitflag = 'Y';

					var varItemlocation = MapLoc;				
					//varItemlocation = getItemStatusMapLoc(itemstatus);

					var fields = ['recordType', 'custitem_ebizserialin'];
					var columns = nlapiLookupField('item', itemid, fields);
					if(columns != null && columns != '')
					{
						Itype = columns.recordType;	

						//	var serialInflg="F";		
						serialInflg = columns.custitem_ebizserialin;
						nlapiLogExecution('ERROR', 'Itype', Itype);
						nlapiLogExecution('ERROR', 'columns', columns);
					}

					nlapiLogExecution('ERROR', 'Value of J', j);
					trecord.selectLineItem('item', j);
					trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
					trecord.setCurrentLineItemValue('item', 'quantity', parseFloat(quantity).toFixed(5));
					if(trantype!='transferorder')
						trecord.setCurrentLineItemValue('item', 'location', varItemlocation); //Added by ramana

					nlapiLogExecution('ERROR', 'Serialquantity', quantity);
					nlapiLogExecution('ERROR', 'Into SerialNos',pointid);

					var tempSerial = "";
					var serialnumcsv = "";

					var filters = new Array();
					filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
					filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty'));
					filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty')); 
					filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'isnotempty'));
					filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', pointid));
					filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'greaterthan', 0));

					var columns=new Array();
					/*columns[0]=new nlobjSearchColumn('custrecord_sku',null,'group');
					columns[1]=new nlobjSearchColumn('custrecord_line_no',null,'group');
					columns[2]=new nlobjSearchColumn('formulanumeric',null,'SUM');
					columns[2].setFormula("TO_NUMBER({custrecord_act_qty})");
					columns[3]=new nlobjSearchColumn('custrecord_sku_status',null,'group');
					columns[4]=new nlobjSearchColumn('custrecord_ebiz_lpno',null,'group');*/

					columns[0]=new nlobjSearchColumn('custrecord_invref_no');
					columns[1]=new nlobjSearchColumn('custrecord_line_no');
					columns[2]=new nlobjSearchColumn('custrecord_sku_status');
					columns[3]=new nlobjSearchColumn('custrecord_ebiz_lpno');
					columns[4]=new nlobjSearchColumn('custrecord_sku');
					columns[5]=new nlobjSearchColumn('custrecord_act_qty');

					var opentaskSearchResultsforlp=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,columns);

					if(opentaskSearchResultsforlp != null && opentaskSearchResultsforlp != '')
					{
						for(var k=0;k<opentaskSearchResultsforlp.length;k++)
						{
							//	if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem" || serialInflg == "T") 
							if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem" ) 
							{


								nlapiLogExecution('ERROR', 'Into SerialNos',trantype);
								//nlapiLogExecution('ERROR', 'invtlp', invtlp);					
								var serialline = new Array();
								var serialId = new Array();




								var filters = new Array();
								/*if(trantype=='returnauthorization' || trantype=='transferorder')
						{*/
								if(trantype=='returnauthorization')
								{
									filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
								}
								else if(trantype=='transferorder')
								{
									nlapiLogExecution('ERROR', 'Into SerialNos11',trantype);
									filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
									filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['1','3','19','8']);
									filters[2] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);
								}
								else
								{
									filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
									filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['1','3','19']);
									filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
								}
//								filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
//								filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);

								var columns = new Array();
								columns[0] = new nlobjSearchColumn('custrecord_serialnumber');

								var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
								var serialnumArray=new Array();var totalconfirmedQty=0;
								var tempQtyforinvDetail;

								if (serchRec) {

									if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
									{
										//Case # 20127042 Start
										/*compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');*/
										//Case # 20127042 End

										tempQtyforinvDetail=1;

									}
									nlapiLogExecution('ERROR', 'Into serchRec.length',serchRec.length);
									for (var n = 0; n < serchRec.length; n++) {
										var tempserialcount=parseInt(n)+1;
										if(parseInt(tempserialcount)<=parseInt(quantity)){
											//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
											if (tempserialId == "") {
												tempserialId = serchRec[n].getId();
											}
											else {
												tempserialId = tempserialId + "," + serchRec[n].getId();
											}

											//This is for Serial num loopin with Space separated.
											if (tempSerial == "") {
												tempSerial = serchRec[n].getValue('custrecord_serialnumber');

												serialnumArray.push(serchRec[n].getValue('custrecord_serialnumber'));
												//Case # 20127042 Start
												/*if(vAdvBinManagement)
												{
													//var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
													compSubRecord.selectNewLineItem('inventoryassignment');
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', tempQtyforinvDetail);
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serchRec[n].getValue('custrecord_serialnumber'));

													compSubRecord.commitLineItem('inventoryassignment');
													//compSubRecord.commit();
												}*/
												//Case # 20127042 End
											}
											else {

												tempSerial = tempSerial + " " + serchRec[n].getValue('custrecord_serialnumber');
												//Case # 20127042 Start
												/*if(vAdvBinManagement)
												{
													//var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
													compSubRecord.selectNewLineItem('inventoryassignment');
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', tempQtyforinvDetail);
													compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serchRec[n].getValue('custrecord_serialnumber'));

													compSubRecord.commitLineItem('inventoryassignment');
													//compSubRecord.commit();
												}*/
												//Case # 20127042 End
												if(tempSerial.length>3500)
												{
													tempSerial='';
													trecord.selectLineItem('item', j);
													trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
													var tempqty=serialnumArray.length;//parseInt(n-1)+parseInt(1);
													trecord.setCurrentLineItemValue('item', 'quantity', tempqty);

													trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
													trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumArray);
													nlapiLogExecution('ERROR', 'serialnumArray', serialnumArray);
													//Case # 20127042 Start
													/*if(vAdvBinManagement)
													{

														compSubRecord.commit();
													}*/
													//Case # 20127042 End
													trecord.commitLineItem('item');
													idl = nlapiSubmitRecord(trecord);
													serialnumArray=new Array();
													totalconfirmedQty=parseInt(totalconfirmedQty)+parseInt(tempqty);
													if(n<serchRec.length)
													{
														var tQty=parseInt(quantity)-parseInt(totalconfirmedQty);
														nlapiLogExecution('ERROR', 'tQty1', tQty);
														var trecord = nlapiTransformRecord(fromrecord, fromid, 'itemreceipt');
														var polinelength = trecord.getLineItemCount('item');
														/*for (var j3 = 1; j3 <= polinelength; j3++) {*/
														var item_id = trecord.getLineItemValue('item', 'item', itemLineNo);
														var itemrec = trecord.getLineItemValue('item', 'itemreceive', itemLineNo);
														//var itemLineNo = trecord.getLineItemValue('item', 'line', itemLineNo);
														var poItemUOM = trecord.getLineItemValue('item', 'unitsdisplay', itemLineNo);

														/*if (itemLineNo == linenum)
											{*/
														if(poItemUOM!=null && poItemUOM!='')
														{
															var vbaseuomqty=0;
															var vuomqty=0;
															var eBizItemDims=geteBizItemDimensions(item_id);
															if(eBizItemDims!=null&&eBizItemDims.length>0)
															{
																nlapiLogExecution('ERROR', 'Dims Length', eBizItemDims.length);
																for(var z=0; z < eBizItemDims.length; z++)
																{
																	if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
																	{
																		vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
																	}
																	nlapiLogExecution('ERROR', 'poItemUOM', poItemUOM);
																	nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
																	nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
																	if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
																	{
																		vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
																	}
																}
																if(vuomqty==null || vuomqty=='')
																{
																	vuomqty=vbaseuomqty;
																}
																nlapiLogExecution('ERROR', 'vbaseuomqty', vbaseuomqty);
																nlapiLogExecution('ERROR', 'vuomqty', vuomqty);

																if(tQty==null || tQty=='' || isNaN(tQty))
																	tQty=0;
																else
																	tQty = (parseFloat(tQty)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

															}
														}
														nlapiLogExecution('ERROR', 'tQtylast', tQty);
														trecord.selectLineItem('item', itemLineNo);
														trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
														var tempqty=parseInt(n)+parseInt(1);
														trecord.setCurrentLineItemValue('item', 'quantity', tQty);

														trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
														//}

														//}
													}






												}
												serialnumArray.push(serchRec[n].getValue('custrecord_serialnumber'));

											}

										}
									}

									serialnumcsv = tempSerial;
									tempSerial = "";
								}

								nlapiLogExecution('ERROR', 'Serial nums assigned', serialnumcsv);
								nlapiLogExecution('ERROR', 'Serial nums tempserialId', tempserialId);
								if(!vAdvBinManagement)
								{
									if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem") {
										trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumArray);
									}
								}
							}
							else 
								if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem"  || Itype == "assemblyitem") {
									nlapiLogExecution('ERROR', 'Into LOT/Batch');
									//nlapiLogExecution('ERROR', 'LP:', invtlp);
									var tempBatch = "";
									var batchcsv = "";

									var confirmLotToNS='Y';

									confirmLotToNS=GetConfirmLotToNS(varItemlocation);

									nlapiLogExecution('ERROR', 'confirmLotToNS:', confirmLotToNS);

									var vItemname;

									var batchnonew='';
									if(confirmLotToNS == 'Y')
									{
										var filters = new Array();
										filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
										filters.push(new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum));
										filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof',itemid));								 
										filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', pointid));
										filters.push(new nlobjSearchFilter('custrecord_act_qty', null, 'greaterthan', 0));

										var columns=new Array();
										columns[0]=new nlobjSearchColumn('custrecord_lotnowithquantity');							  
										columns[1]=new nlobjSearchColumn('custrecord_batch_no');	
										var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,columns);
										nlapiLogExecution('ERROR','opentaskSearchResults',opentaskSearchResults);
										var tempBatch='';
										if(opentaskSearchResults!=null && opentaskSearchResults.length>0)
										{
											nlapiLogExecution('ERROR','opentaskSearchResults.length',opentaskSearchResults.length);
											for(var k1=0;k1<opentaskSearchResults.length;k1++)
											{
												if(tempBatch!='')
													tempBatch = tempBatch +','+opentaskSearchResults[k1].getValue('custrecord_lotnowithquantity');
												else
												{
													batchnonew=opentaskSearchResults[k1].getValue('custrecord_batch_no');
													tempBatch = opentaskSearchResults[k1].getValue('custrecord_lotnowithquantity');
												}
											}
										}
										nlapiLogExecution('ERROR','tempbatch',tempBatch);


									}
									else
									{
										nlapiLogExecution('ERROR', 'item_id:', item_id);
										var filters1 = new Array();          
										filters1[0] = new nlobjSearchFilter('internalid', null, 'is',item_id);
										var columns = new Array();

										columns[0] = new nlobjSearchColumn('itemid');

										var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
										if (itemdetails !=null) 
										{
											vItemname=itemdetails[0].getValue('itemid');
											nlapiLogExecution('ERROR', 'vItemname:', vItemname);
											//var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
											vItemname=vItemname.replace(/ /g,"-");
											tempBatch=vItemname + '('+ quantity + ')';


										}
									}
									if(!vAdvBinManagement)
									{	
										batchcsv = tempBatch;
										nlapiLogExecution('ERROR', 'LOT/Batch nums assigned', batchcsv);
										trecord.setCurrentLineItemValue('item', 'serialnumbers', batchcsv);
									}
									else
									{
										//Case # 20127042 Start
										/*var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
										compSubRecord.selectNewLineItem('inventoryassignment');
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', quantity);
										if(confirmLotToNS == 'Y')
											compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', batchnonew);
										else
											compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
										compSubRecord.commitLineItem('inventoryassignment');
										compSubRecord.commit();*/
										//Case # 20127042 End
									}	
								}
						}
					}
					if(trecord!=null  && trecord!='')
					{
						/*if(vAdvBinManagement)
						{
							if(compSubRecord!=null)
								compSubRecord.commit();
						}*/
						trecord.commitLineItem('item');
					}

				}
				//Code Added by Ramana
				/*}
				else
				{
					trecord=null;
					idl=pointid;
				}*/	
			}
			if (commitflag == 'N' && trecord != null && trecord != '') {
				nlapiLogExecution('ERROR', 'commitflag is N', commitflag);
				trecord.selectLineItem('item', j);
				trecord.setCurrentLineItemValue('item', 'itemreceive', 'F');




				trecord.commitLineItem('item');
			}
		}
		nlapiLogExecution('ERROR', 'Before Item Receipt---', 'Before');

		/*if(trecord != null && trecord != '')
			idl = nlapiSubmitRecord(trecord);*/


		//nlapiLogExecution('ERROR', 'Item Receipt idl', idl);

		/*return idl;*/
	}
	catch(exp)
	{
		var exceptionname=trantype;
		var functionality='Creating ItemReceipt';
		var trantype2=1;//1 for Inbound and 2 for outbound
		var vebizOrdNo=pointid;
		var vcontainerLp='';
		var POarray = new Array();
		nlapiLogExecution('ERROR', 'DetailsError', functionality);	
		nlapiLogExecution('ERROR', 'vcontainerLp', vcontainerLp);
		nlapiLogExecution('ERROR', 'vebizOrdNo35', vebizOrdNo);
		var reference2="";
		var reference3="";
		var reference4 ="";
		var reference5 ="";
		var alertType=1;//1 for exception and 2 for report
		errSendMailByProcess(trantype2,exp,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
		//InsertExceptionLog(exceptionname,trantype, functionality, exp, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId);
		nlapiLogExecution('ERROR', 'Exception in TransformRec (PurchaseOrder) : ', exp);

		if (exp instanceof nlobjError) 
			nlapiLogExecution('ERROR', 'system error', exp.getCode() + '\n' + exp.getDetails());
		else 
			nlapiLogExecution('ERROR', 'unexpected error', exp.toString());

		/*nlapiLogExecution('ERROR', 'Updating PUTW task with end location - Record Id', RecordId);
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordId);
		transaction.setFieldValue('custrecord_actendloc', EndLocationId);
		nlapiSubmitRecord(transaction, false, true);
		nlapiLogExecution('ERROR', 'PUTW task updated with act end location', EndLocationId);*/

		nlapiLogExecution('ERROR', 'Putaway Failed ');
		POarray["custparam_error"] = exp.getCode() + '\n' + exp.getDetails();
		response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
		return;
	}	
}



//function for posting Item Receipt to NS Scheduler version
function ItemReceiptSchedular(type)
{
	nlapiLogExecution('Debug', 'Scheduler version', type);

	var poid;
	var trantype;
	var poToLocationID;
	try{
		var context = nlapiGetContext(); 
		poid = context.getSetting('SCRIPT', 'custscript_poid');
		trantype = context.getSetting('SCRIPT', 'custscript_trantype');
		poToLocationID = context.getSetting('SCRIPT', 'custscript_locationid');


		var str = 'type. = ' + type + '<br>';
		str = str + 'poid. = ' + poid + '<br>';	
		str = str + 'trantype. = ' + trantype + '<br>';	
		str = str + 'poToLocationID. = ' + poToLocationID + '<br>';	

		nlapiLogExecution('Debug', 'Parameter Details', str);

		if(type != 'aborted')
		{

			var filters = new Array();
			filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
			filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty'));
			filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty')); 
			filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'isnotempty'));
			filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', poid));

			var columns=new Array();
			columns[0]=new nlobjSearchColumn('custrecord_sku',null,'group');
			columns[1]=new nlobjSearchColumn('custrecord_line_no',null,'group');
			columns[2]=new nlobjSearchColumn('formulanumeric',null,'SUM');
			columns[2].setFormula("TO_NUMBER({custrecord_act_qty})");
			//columns[3]=new nlobjSearchColumn('custrecord_sku_status',null,'group');
			columns[3]=new nlobjSearchColumn('custrecord_wms_location',null,'group');
			columns[4]=new nlobjSearchColumn('custrecord_item_status_location_map','custrecord_sku_status','group');
			columns[5]=new nlobjSearchColumn('custrecord_sku_status',null,'group');
			// case no 20125554
			// columns[4]=new nlobjSearchColumn('custrecord_ebiz_lpno',null,'group');

			var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,columns);
			nlapiLogExecution('ERROR','opentaskSearchResults',opentaskSearchResults);

			if(opentaskSearchResults!=null && opentaskSearchResults!='' && opentaskSearchResults.length>0)
			{
				nlapiLogExecution('ERROR','opentaskSearchResults.length',opentaskSearchResults.length);


				//case # 20125608 start : for different Item status
				var ItemStatusArray =new Array();
				var ItemStatusMapLocArray =new Array();
				var itemReceiptArray =new Array();
				for(var k=0;k<opentaskSearchResults.length;k++)
				{
					var	itemstatus1=opentaskSearchResults[k].getValue('custrecord_sku_status',null,'group');
					nlapiLogExecution('ERROR','itemstatus1',itemstatus1);
					var	MapLoc=opentaskSearchResults[k].getValue('custrecord_item_status_location_map','custrecord_sku_status','group');
					if(ItemStatusArray.indexOf(itemstatus1) == -1)
					{
						ItemStatusArray.push(itemstatus1);
					}
					if(ItemStatusMapLocArray.indexOf(MapLoc) == -1)
					{
						ItemStatusMapLocArray.push(MapLoc);
					}
				}
				//case 20125608 end

				nlapiLogExecution('ERROR','ItemStatusArray',ItemStatusArray.length);

				//for(j=0;j<ItemStatusArray.length;j++)
				for(j=0;j<ItemStatusMapLocArray.length;j++)
				{

					//var vItemStatus = ItemStatusArray[j];
					var WHLocation = ItemStatusMapLocArray[j];
					//nlapiLogExecution('ERROR','vItemStatus',vItemStatus);
					var trecord = nlapiTransformRecord(trantype, poid, 'itemreceipt');
					for(var i=0;i<opentaskSearchResults.length;i++)
					{
						nlapiLogExecution('ERROR','inside for loop',i);

						var ActQuantity=opentaskSearchResults[i].getValue('formulanumeric',null,'SUM');
						var linenum=opentaskSearchResults[i].getValue('custrecord_line_no',null,'group');
						var	itemid=opentaskSearchResults[i].getValue('custrecord_sku',null,'group');
						var	itemstatus=opentaskSearchResults[i].getValue('custrecord_sku_status',null,'group');
						//var invtlp = opentaskSearchResults[i].getValue('custrecord_ebiz_lpno',null,'group');
						var	itemstatus;
						var	MapLoc=opentaskSearchResults[i].getValue('custrecord_item_status_location_map','custrecord_sku_status','group');
						if(WHLocation == MapLoc)
						{
							nlapiLogExecution('ERROR','invtlp inside loop',linenum);
							nlapiLogExecution('ERROR','id inside loop for opentask',opentaskSearchResults[i].getId());

							generateItemReceipt(trecord,trantype,poid,ActQuantity,linenum,itemstatus,itemid,trantype,poid,poToLocationID,null,itemstatus,MapLoc);
							// generateItemReceipt(trecord,trantype,poid,ActQuantity,linenum,itemstatus,itemid,trantype,poid,'',poToLocationID,invtlp)
							nlapiLogExecution('ERROR','invtlp inside loop1111',linenum);
						}
					}
					if(trecord != null && trecord != '')
					{
						idl = nlapiSubmitRecord(trecord);
						var currentrow=[ItemStatusArray[j],idl];
						itemReceiptArray.push(currentrow);
					}
				}
				nlapiLogExecution('ERROR','idl',idl);
				if(idl!=null && idl!='')
				{									 
					if(trantype=='transferorder')
						InvokeInventoryTransfer(poid);
//					case# 201414290
					var filters2 = new Array();
					filters2.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
					filters2.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty'));
					//filters2.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty')); 
					filters2.push(new nlobjSearchFilter('custrecord_actendloc', null, 'isnotempty'));
					filters2.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', poid));



					//	var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,null);
					var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters2,null);
					//var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,null);
					if(opentaskSearchResults!=null && opentaskSearchResults!='' && opentaskSearchResults.length>0)
					{
						//POarray["custparam_error"] = st2;
						nlapiLogExecution('ERROR','opentaskSearchResults.length',opentaskSearchResults.length);
						for(var j=0;j<opentaskSearchResults.length;j++)
						{
							nlapiLogExecution('ERROR','opentaskSearchResultsID',opentaskSearchResults[j].getId());
							// case no start 20126855
							MoveTaskRecord(opentaskSearchResults[j].getId(),idl);

							nlapiLogExecution('ERROR','idlgd11',idl);
							// case no end 20126855
							nlapiLogExecution('ERROR','idl11',idl);
						}

					}
				}
				nlapiLogExecution('ERROR','idl123441',idl);

			}
			else{
				nlapiLogExecution('ERROR','no open Putaway records','done');							  
			}



		}

	}
	catch(exp)
	{

		nlapiLogExecution('ERROR','exception in ItemReceiptSchedular',exp);
	}

}

function OpentaskrecordsforItemStatus(poid,vItemStatus)
{
	nlapiLogExecution('ERROR','in to OpentaskrecordsforItemStatus',poid);
	nlapiLogExecution('ERROR','vItemStatus',vItemStatus);
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isnotempty'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty')); 
	filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'isnotempty'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', poid));
	filters.push(new nlobjSearchFilter('custrecord_sku_status', 'null', 'anyof', vItemStatus));
	var opentaskSearchResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,null);
	nlapiLogExecution('ERROR','opentaskSearchResults',opentaskSearchResults);
	return opentaskSearchResults;
}



function GetSOInternalId(SOText,ordertype)
{
	nlapiLogExecution('ERROR','Into GetSOInternalId (Input)',SOText);

	var ActualSoID='';

	var vfilters=new Array();


	vfilters.push(new nlobjSearchFilter('tranid',null,'is',SOText.toString()));
	vfilters.push(new nlobjSearchFilter('mainline',null,'is','F'));


	var vsearchrecs=nlapiSearchRecord(ordertype,null,vfilters,null);


	nlapiLogExecution('ERROR','searchrec123',vsearchrecs);

	if(vsearchrecs!=null && vsearchrecs!='' && vsearchrecs.length>0)
	{
		ActualSoID=vsearchrecs[0].getId();
		nlapiLogExecution('ERROR','ActualSoID',ActualSoID);
	}


	nlapiLogExecution('ERROR','Out of GetSOInternalId (Output)',ActualSoID);

	return ActualSoID;
}