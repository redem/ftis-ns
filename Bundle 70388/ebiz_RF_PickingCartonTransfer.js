/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_PickingCartonTransfer.js,v $
 *     	   $Revision: 1.1.2.10.2.2 $
 *     	   $Date: 2015/12/02 15:33:05 $
 *     	   $Author: aanchal $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * 
 * PRAMETERS
 *
 * DESCRIPTION
 * 
 * REVISION HISTORY
 * $Log: ebiz_RF_PickingCartonTransfer.js,v $
 * Revision 1.1.2.10.2.2  2015/12/02 15:33:05  aanchal
 * 2015.2 Issue Fix
 * 201415719
 *
 * Revision 1.1.2.10.2.1  2015/11/16 15:38:17  rmukkera
 * case # 201415009
 *
 * Revision 1.1.2.10  2015/09/04 11:06:42  deepshikha
 * 2015.2 issue fixes
 * 201414193
 *
 * Revision 1.1.2.9  2015/04/13 09:25:59  rrpulicherla
 * Case#201412277
 *
 * Revision 1.1.2.8  2014/09/25 17:57:16  skreddy
 * case # 201410534
 * Alpha comm CR (''pick by carton LP")
 *
 * Revision 1.1.2.7  2014/06/13 12:39:19  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.6  2014/05/26 07:08:44  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148320
 *
 * Revision 1.1.2.5  2014/05/08 15:55:04  skavuri
 * Case# 20148323 SB Issue fixed
 *
 * Revision 1.1.2.4  2014/03/13 14:18:35  nneelam
 * case#  20127692
 * Dealmed Issue Fix.
 *
 * Revision 1.1.2.3  2013/11/14 06:32:04  skreddy
 * Case# 201215000
 * Fast Picking issue fix
 *
 * Revision 1.1.2.2  2013/09/16 09:40:01  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201215000
 * RF Fast Picking
 *
 * Revision 1.1.2.1  2013/08/15 01:43:06  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201215000
 * RF Fast Picking
 *
 *
 *
 *****************************************************************************/


function PickingCartonTransfer(request, response){
	if (request.getMethod() == 'GET') {

		var fastpick = request.getParameter('custparam_fastpick');

		nlapiLogExecution('ERROR', 'fastpick', fastpick);

		var getWaveno = request.getParameter('custparam_waveno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocationId = request.getParameter('custparam_beginLocation');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getItemName = request.getParameter('custparam_itemname');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');        
		var getEnteredLocation = request.getParameter('custparam_endlocation');
		var getBeginBinLocation = request.getParameter('custparam_beginlocationname');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var getSerialNo = request.getParameter('custparam_serialno');
		var vClusterNo = request.getParameter('custparam_clusterno');
		var vBatchno = request.getParameter('custparam_batchno');		
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var pickType=request.getParameter('custparam_picktype');
		var name=request.getParameter('name');
		var getOrderNo=request.getParameter('custparam_ebizordno');
		var whLocation = request.getParameter('custparam_whlocation');
		var getZoneNo=request.getParameter('custparam_ebizzoneno');
		var getRemQty = request.getParameter('custparam_remqty');
		var NextRecordId=request.getParameter('custparam_nextrecordid');
		var getItem = '';

		nlapiLogExecution('ERROR', 'getItemName', getItemName);
		if(getItemName==null || getItemName=='')
		{
			if(getItemInternalId!=null && getItemInternalId!='')
			{
				var Itemtype = nlapiLookupField('item', getItemInternalId, 'recordType');

				var filtersitem = new Array();
				var columnsitem = new Array();

				filtersitem.push(new nlobjSearchFilter('internalid', null, 'is',getItemInternalId));
				filtersitem.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

				columnsitem[0] = new nlobjSearchColumn('description');    
				columnsitem[1] = new nlobjSearchColumn('salesdescription');
				columnsitem[2] = new nlobjSearchColumn('itemid');

				var itemRecord = nlapiSearchRecord(Itemtype, null, filtersitem, columnsitem);
				if(itemRecord!=null && itemRecord!='' && itemRecord.length>0)
				{
					if(itemRecord[0].getValue('description') != null && itemRecord[0].getValue('description') != '')
					{
						getItemDescription = itemRecord[0].getValue('description');// Case# 20148323
					}
					else if(itemRecord[0].getValue('salesdescription') != null && itemRecord[0].getValue('salesdescription') != "")
					{	
						getItemDescription = itemRecord[0].getValue('salesdescription');// Case# 20148323
					}

					getItem = itemRecord[0].getValue('itemid');
				}

				//Itemdescription = Itemdescription.substring(0, 20);	//Case# 20148323
				getItemDescription = getItemDescription.substring(0, 20);
			}
		}
		else
		{
			getItem=getItemName;
		}

		//code added on 15Feb 2012 by suman
		var EntLoc = request.getParameter('custparam_EntLoc');
		var EntLocRec=request.getParameter('custparam_EntLocRec');
		var EntLocID=request.getParameter('custparam_EntLocId');
		var ExceptionFlag = request.getParameter('custparam_Exc');
		var getnextExpectedQuantity = request.getParameter('custparam_nextexpectedquantity');

		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');
		//end of code as of 15Feb.

		var getPickQty=0;
		getPickQty=request.getParameter('custparam_enteredQty');
		if(getPickQty == "" || getPickQty == null)
		{
			getPickQty=request.getParameter('custparam_expectedquantity');
		}
		var getReason=request.getParameter('custparam_enteredReason');

		var getLPContainerSize= '';

		var str = 'EntLoc. = ' + EntLoc + '<br>';
		str = str + 'EntLocRec. = ' + EntLocRec + '<br>';	
		str = str + 'EntLocID. = ' + EntLocID + '<br>';	
		str = str + 'ExceptionFlag. = ' + ExceptionFlag + '<br>';	
		str = str + 'getnextExpectedQuantity. = ' + getnextExpectedQuantity + '<br>';	
		str = str + 'getItem. = ' + getItem + '<br>';	
		str = str + 'getOrderNo. = ' + getOrderNo + '<br>';	
		str = str + 'getPickQty. = ' + getPickQty + '<br>';	
		str = str + 'getExpectedQuantity. = ' + getExpectedQuantity + '<br>';	
		str = str + 'getReason. = ' + getReason + '<br>';	
		str = str + 'getEnteredLocation. = ' + getEnteredLocation + '<br>';	
		str = str + 'getContainerLpNo. = ' + getContainerLpNo + '<br>';	
		str = str + 'getEndLocInternalId. = ' + getEndLocInternalId + '<br>';	
		str = str + 'getBeginBinLocation. = ' + getBeginBinLocation + '<br>';	

		nlapiLogExecution('ERROR', 'Parameter Values', str);

		try
		{
			nlapiLogExecution('ERROR', 'Lading Open Task...', getRecordInternalId);
			var SORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', getRecordInternalId);
			getContainerLpNo= SORec.getFieldValue('custrecord_container_lp_no');
			getLPContainerSize= SORec.getFieldText('custrecord_container');
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR', 'Exception in Loading Open Task', exp);
		}

		nlapiLogExecution('ERROR', 'getContainerLpNo', getContainerLpNo);

		var vdisplaylocation='';
		if(EntLoc!=null && EntLoc!='')
			vdisplaylocation=EntLoc;
		else
			vdisplaylocation=getBeginBinLocation;

		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no');
		var html = "<html><head>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";   
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('entercontainerno').focus();";   

		html = html + "function stopRKey(evt) { ";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('enterqty').value=='' || document.getElementById('enterqty').value==null){";
		html = html + "   document.getElementById('enterqty').focus();";
		html = html + "	  return false;}} ";
		html = html + "	} ";
		html = html + "	document.onkeypress = stopRKey; ";

//		html = html + "function validateQty(form) { ";
//		html = html + "	  if (form.enterqty.value==''){";
//		html = html + "   form.enterqty.focus();";  
//		html = html + "	  return false;} ";
//		html = html + "	} ";		

		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><label>" + name + "</label> ## WAVE# :<label>" + getWaveno + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";	
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>CARTON # : <label>" + getContainerLpNo + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";	
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>REMAINING QTY : <label>" + getPickQty + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";	
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";
		html = html + "				<input type='hidden' name='hdnFetchedContainerLPNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";		
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemName' value='" + getItemName + "'>";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEnteredLocation' value='" + getEnteredLocation + "'>";
		html = html + "				<input type='hidden' name='hdnBeginBinLocation' value='" + getBeginBinLocation + "'>";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnserialno' value=" + getSerialNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vClusterNo + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value=" + vBatchno + ">";		
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnNextLocation' value='" + NextLocation + "'>";
		html = html + "				<input type='hidden' name='hdnNextItem' value=" + NextItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnName' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + getLPContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnebizOrdNo' value=" + getOrderNo + ">";
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		html = html + "				<input type='hidden' name='hdnReason' value=" + getReason + ">";
		html = html + "				<input type='hidden' name='hdnActQty' value=" + getPickQty + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdnremqty' value=" + getRemQty + ">";
		html = html + "				<input type='hidden' name='hdnEntLoc' value=" + EntLoc + ">";
		html = html + "				<input type='hidden' name='hdnEntLocRec' value=" + EntLocRec + ">";
		html = html + "				<input type='hidden' name='hdnEntLocID' value=" + EntLocID + ">";
		html = html + "				<input type='hidden' name='hdnExceptionFlag' value=" + ExceptionFlag + ">";
		html = html + "				<input type='hidden' name='hdnebizzoneno' value=" + getZoneNo + ">";
		html = html + "				<input type='hidden' name='hdnextExpectedQuantity' value=" + getnextExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnNextrecordid' value=" + NextRecordId + ">";
		html = html + "				<input type='hidden' name='hdnCloseflag'>";
		html = html + "				<input type='hidden' name='hdnConfirmflag'>";
		html = html + "				<input type='hidden' name='hdnfastpick' value=" + fastpick + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>ENTER/SCAN NEW CARTON # ";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entercontainerno' id='entercontainerno' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>ENTER QTY ";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterqty' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";		
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>SEND <input name='cmdSend' type='submit' value='F8' onclick='this.form.hdnConfirmflag.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdClose.disabled=true;return false;'/>";
		html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "					CLOSE <input name='cmdClose' type='submit' value='F11' onclick='this.form.hdnCloseflag.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdSend.disabled=true;return false'/>";
		html = html + "				</td>";
		html = html + "			</tr>";		
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('entercontainerno').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('ERROR', 'Into Response', 'Into Response');
		nlapiLogExecution('ERROR', 'Time Stamp at the start of response',TimeStampinSec());
		nlapiLogExecution('ERROR', 'cmdSend',request.getParameter('cmdSend'));
		nlapiLogExecution('ERROR', 'cmdClose',request.getParameter('cmdClose'));
		nlapiLogExecution('ERROR', 'hdnCloseflag',request.getParameter('hdnCloseflag'));

		var fastpick = request.getParameter('hdnfastpick');

		nlapiLogExecution('ERROR', 'fastpick', 'fastpick');

		var vRemaningqty=0,vDisplayedQty=0,vActqty=0;
		var vZoneId=request.getParameter('hdnebizzoneno');
		vActqty = request.getParameter('hdnActQty');
		var vReason = request.getParameter('hdnReason');
		var TotalWeight=0;
		var getEnteredContainerNo = request.getParameter('entercontainerno');
		if(getEnteredContainerNo==null || getEnteredContainerNo=="")
		{
			getEnteredContainerNo=request.getParameter('hdnFetchedContainerLPNo');
		}
		var getFetchedContainerNo = request.getParameter('hdnFetchedContainerLPNo');
		var getRecordInternalId = request.getParameter('hdnRecordInternalId');
		var getContainerSize=request.getParameter('hdnContainerSize');

		if(getContainerSize==null || getContainerSize=="")
		{
			getContainerSize = request.getParameter('entersize');
		}			
		var getWaveNo = request.getParameter('hdnWaveNo');
		var RecordInternalId = request.getParameter('hdnRecordInternalId');
		var ContainerLPNo = request.getParameter('hdnContainerLpNo');
		var BeginLocation = request.getParameter('hdnBeginLocationId');
		var vbatchno = request.getParameter('hdnbatchno');		
		var ItemNo = request.getParameter('hdnItemInternalId');
		var ItemDesc = request.getParameter('hdnItemDescription');
		var Item = request.getParameter('hdnItem');
		var ItemName = request.getParameter('hdnItemName');
		var PickQty = request.getParameter('hdnExpectedQuantity');
		var RcId = request.getParameter('hdnRecordInternalId');
		var EndLocation = request.getParameter('hdnEndLocInternalId');
		var RecCount = request.getParameter('hdnRecCount');
		var NextShowLocation=request.getParameter('hdnNextLocation');
		var NextShowItem=request.getParameter('hdnNextItem');
		var OrdName=request.getParameter('hdnName');
		var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var BeginLocationName = request.getParameter('hdnBeginBinLocation');
		var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
		var OrderLineNo = request.getParameter('hdnOrderLineNo');
		var SerialNo = request.getParameter('hdnserialno');
		var vClusterNo = request.getParameter('hdnClusterNo');
		var vBatchno = request.getParameter('hdnbatchno');	
		var vRemQty = request.getParameter('hdnremqty');
		var NextrecordID=request.getParameter('hdnNextrecordid');
		var EntLoc = request.getParameter('hdnEntLoc');
		var EntLocRec = request.getParameter('hdnEntLocRec');
		var EntLocID = request.getParameter('hdnEntLocID');
		var ExceptionFlag = request.getParameter('hdnExceptionFlag');
		if(ExceptionFlag=='L')
			if(EntLocID!=null&&EntLocID!="")
				EndLocation=EntLocID;

		var newcarton = request.getParameter('entercontainerno');
		var newqty = request.getParameter('enterqty');

		var vCarrier;
		var vSite;
		var vCompany;
		var stgDirection="OUB";
		var vDoname=request.getParameter('hdnName');
		var vdono=request.getParameter('hdnDOLineId');
		var vPrevinvrefno=request.getParameter('hdnInvoiceRefNo');
		var SalesOrderInternalId=request.getParameter('hdnebizOrdNo');
		var vPickType = request.getParameter('hdnpicktype');
		vSite = request.getParameter('hdnwhlocation');
		nlapiLogExecution('Error', 'RcId', RcId);
		var str = 'Container. = ' + request.getParameter('hdnContainerSize') + '<br>';
		str = str + 'Expected Qty. = ' + PickQty + '<br>';
		str = str + 'newqty. = ' + newqty + '<br>';	
		str = str + 'getEnteredContainerNo. = ' + request.getParameter('entercontainerno') + '<br>';	
		str = str + 'OrdName. = ' + OrdName + '<br>';	
		str = str + 'NextShowLocation. = ' + NextShowLocation + '<br>';	
		str = str + 'getRecordInternalId. = ' + getRecordInternalId + '<br>';	
		str = str + 'getFetchedContainerNo. = ' + getFetchedContainerNo + '<br>';	
		str = str + 'NextShowItem. = ' + NextShowItem + '<br>';	
		str = str + 'ItemNo. = ' + ItemNo + '<br>';	
		str = str + 'SalesOrderInternalId. = ' + SalesOrderInternalId + '<br>';	
		str = str + 'vPickType. = ' + vPickType + '<br>';	
		str = str + 'vSite. = ' + vSite + '<br>';	
		str = str + 'getWaveNo. = ' + getWaveNo + '<br>';	
		str = str + 'InvoiceRefNo. = ' + InvoiceRefNo + '<br>';	
		str = str + 'EndLocation. = ' + EndLocation + '<br>';	
		str = str + 'BeginLocationName. = ' + BeginLocationName + '<br>';	


		nlapiLogExecution('ERROR', 'Parameter Values in Response', str);

		//ItemFulfillment(getWaveNo, RecordInternalId, ContainerLPNo, PickQty, BeginLocation, Item, ItemDesc, ItemNo, vdono, InvoiceRefNo, BeginLocationName, EndLocInternalId, EndLocation, OrderLineNo, SerialNo,vBatchno);
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		// This variable is to hold the SO# entered.
		var SOarray = new Array();

		SOarray["custparam_error"] = 'INVALID CARTON #';
		SOarray["custparam_screenno"] = '13NEW';
		SOarray["custparam_fastpick"] = fastpick;
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		SOarray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
		SOarray["custparam_ebizordno"] = request.getParameter('hdnebizOrdNo');
		var vSkipId=request.getParameter('hdnskipid');
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');
		vSkipId=0;

		SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');

		if(request.getParameter('hdnNextrecordid')!=null&&request.getParameter('hdnNextrecordid')!="")	
			SOarray["custparam_nextrecordid"] = request.getParameter('hdnNextrecordid');

		SOarray["custparam_recordinternalid"] = request.getParameter('hdnRecordInternalId');
		SOarray["custparam_containerlpno"] = request.getParameter('hdnContainerLpNo');
		SOarray["custparam_expectedquantity"] = request.getParameter('hdnExpectedQuantity');
		SOarray["custparam_beginLocation"] = request.getParameter('hdnBeginLocationId');
		SOarray["custparam_item"] = request.getParameter('hdnItem');
		SOarray["custparam_itemname"] = request.getParameter('hdnItemName');
		SOarray["custparam_itemdescription"] = request.getParameter('hdnItemDescription');
		SOarray["custparam_iteminternalid"] = request.getParameter('hdnItemInternalId');
		SOarray["custparam_dolineid"] = request.getParameter('hdnDOLineId');
		SOarray["custparam_invoicerefno"] = request.getParameter('hdnInvoiceRefNo');
		SOarray["custparam_beginlocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_beginLocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
		SOarray["custparam_orderlineno"] = request.getParameter('hdnOrderLineNo');
		SOarray["custparam_serialno"] = request.getParameter('hdnserialno');
		SOarray["custparam_clusterno"] = request.getParameter('hdnClusterNo');
		SOarray["custparam_batchno"] = vbatchno;
		SOarray["custparam_nextlocation"] = NextShowLocation;
		SOarray["custparam_nextiteminternalid"] = NextShowItem;
		SOarray["custparam_newcontainerlp"] = getEnteredContainerNo;
		SOarray["custparam_nextexpectedquantity"] = request.getParameter('hdnextExpectedQuantity');
		var nextexpqty= request.getParameter('hdnextExpectedQuantity');
		SOarray["name"] = OrdName;
		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  request.getParameter('hdnebizzoneno');
		}
		else
			SOarray["custparam_ebizzoneno"] = '';
		nlapiLogExecution('Error', 'NextLocation', NextShowLocation);

		nlapiLogExecution('Error', 'hdnConfirmflag', request.getParameter('hdnConfirmflag'));


		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept SO #.
		if (optedEvent == 'F7') 
		{
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pickingcarton', 'customdeploy_ebiz_rf_pickingcarton', false, SOarray);
		}
		else if (request.getParameter('hdnConfirmflag') == 'F8') 
		{
			if(request.getParameter('entercontainerno')=='' ||  request.getParameter('entercontainerno')==null)
			{
				SOarray["custparam_error"] = 'PLEASE ENTER/SCAN CARTON #';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				return;
			}

			if(request.getParameter('enterqty')=='' ||  request.getParameter('enterqty')==null)
			{
				SOarray["custparam_error"] = 'PLEASE ENTER QTY';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				return;
			}
			
			if( parseInt(vActqty) != parseInt(newqty))
			{
				SOarray["custparam_error"] = 'ENTER QUANTITY IS NOT MATCHED WITH REMAINING QUANTITY';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				nlapiLogExecution('ERROR', 'Error: ', 'quantity is not matched with remaining quantity');
				return;
			}

			var vOpenTaskRecs=fnValidateEnteredContLP(newcarton,SalesOrderInternalId);
			nlapiLogExecution('ERROR', 'Time Stamp at the end of fnValidateEnteredContLP',TimeStampinSec());
			if(vOpenTaskRecs != null && vOpenTaskRecs != '')
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				nlapiLogExecution('ERROR', 'Error: ', 'Container# is invalid');
			}
			else
			{	
				var result = fnValidateContainer(newcarton,getFetchedContainerNo, '2','2',vSite);
				if(result == false)
				{
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					nlapiLogExecution('ERROR', 'Error: ', 'Container# is invalid');
					return;
				}

				var getEnteredContainerNoPrefix = newcarton.substring(0, 3).toUpperCase();
				var LPReturnValue = ebiznet_LPRange_CL_withLPType(newcarton, '2','2',vSite);//'2'UserDefiend,'2'PICK
				nlapiLogExecution('ERROR', 'Time Stamp at the end of ebiznet_LPRange_CL_withLPType',TimeStampinSec());
				if(LPReturnValue == true){
					nlapiLogExecution('ERROR', 'getItem', ItemNo);
					var arrDims = getSKUCubeAndWeightforconfirm(ItemNo, 1);
					nlapiLogExecution('ERROR', 'Time Stamp at the end of getSKUCubeAndWeightforconfirm',TimeStampinSec());
					var itemCube = 0;
					var itemWeight=0;						
					if (arrDims[0] != "" && (!isNaN(arrDims[1]))){
						itemCube = (parseFloat(newqty) * parseFloat(arrDims[0]));
						itemWeight = (parseFloat(newqty) * parseFloat(arrDims[1]));//									
					} 

					var ContainerCube;					
					var containerInternalId;
					var ContainerSize;
					if(getContainerSize=="" || getContainerSize==null)
					{
						getContainerSize=ContainerSize;
						nlapiLogExecution('ERROR', 'ContainerSize', ContainerSize);	
						ContainerCube = itemCube;
						TotalWeight=itemWeight;
					}	
					else
					{
						var arrContainerDetails = getContainerCubeAndTarWeight("",getContainerSize);
						nlapiLogExecution('ERROR', 'Time Stamp at the end of getContainerCubeAndTarWeight',TimeStampinSec());
						if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) {

							ContainerCube =  parseFloat(arrContainerDetails[0]);						
							containerInternalId = arrContainerDetails[3];
							ContainerSize=arrContainerDetails[4];
							TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));// added on 28 nov
						} 
					}
					nlapiLogExecution('ERROR', 'ContainerSizeInternalId', containerInternalId);				
					//Insert LP Record				
					var ResultText='';
					//nlapiLogExecution('ERROR', 'Time Stamp at the end of GenerateLable',TimeStampinSec());
					CreateRFMasterLPRecord(newcarton,containerInternalId,ContainerCube,TotalWeight,getContainerSize,ResultText,vSite);
					nlapiLogExecution('ERROR', 'Time Stamp at the end of CreateRFMasterLPRecord',TimeStampinSec());

					nlapiLogExecution('ERROR', 'Time Stamp at the end of UpdateAllContainerLp',TimeStampinSec());

					nlapiLogExecution('ERROR', 'Time Stamp at the end of UpdateRFFulfillOrdLine',TimeStampinSec());

					nlapiLogExecution('ERROR', 'Before Process', RcId);	

					vRemaningqty = PickQty-newqty;

					var opentaskcount=getOpenTasksCount(getWaveNo,getEnteredContainerNo,vPickType,SalesOrderInternalId,OrdName,vZoneId);
					
					var IsitLastPick='F';
					if(opentaskcount >  1)
						IsitLastPick='F';
					else
						IsitLastPick='T';
					
					nlapiLogExecution('ERROR', 'IsitLastPick', IsitLastPick);	
					UpdateOldPickTask(RcId,vRemaningqty,newcarton,itemCube,itemWeight,newqty,EndLocation,InvoiceRefNo,IsitLastPick,vBatchno);

					if(parseInt(vRemaningqty)>0)
					{
						CreateandConfirmPickTaskwithNewCarton(RcId,newqty,newcarton,itemCube,itemWeight,EndLocation,OrdName,InvoiceRefNo,IsitLastPick,vBatchno);
					}

					UpdateRFFulfillOrdLine(vdono,newqty);

					//create new record in opentask,fullfillordline when we have qty exception

					var recordcount=1;//it is iterator in GUI

					if(parseInt(vRemaningqty)>0)
					{
						SOarray["custparam_expectedquantity"]=vRemaningqty;
						SOarray["custparam_fastpick"] = fastpick;	
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pickingcartonxfer', 'customdeploy_ebiz_rf_pickingcartonxfer', false, SOarray);
						return;
					}

					var opentaskcount=getOpenTasksCount(getWaveNo,getEnteredContainerNo,vPickType,SalesOrderInternalId,OrdName,vZoneId);

					if(opentaskcount > 0)
					{
						if(BeginLocation != NextShowLocation)
						{ 
							SOarray["custparam_beginLocationname"]=null;
							SOarray["custparam_iteminternalid"]=null;
							SOarray["custparam_expectedquantity"]=null;
							SOarray["custparam_fastpick"] = fastpick;	
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, SOarray);								
							nlapiLogExecution('ERROR', 'Navigating To1', 'Picking Location');
						}
						else if(ItemNo != NextShowItem)
						{ 
							SOarray["custparam_iteminternalid"] = NextShowItem;
							SOarray["custparam_beginLocationname"]=null;
							SOarray["custparam_expectedquantity"]=null;
							SOarray["custparam_iteminternalid"] = NextShowItem;
							SOarray["custparam_fastpick"] = fastpick;	
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, SOarray);								
							nlapiLogExecution('ERROR', 'Navigating To1', 'Picking Item');
						}
						else if(ItemNo == NextShowItem)
						{ 
							SOarray["custparam_iteminternalid"] = NextShowItem;
							SOarray["custparam_beginLocationname"]=null;
							SOarray["custparam_expectedquantity"]=nextexpqty;
							SOarray["custparam_fastpick"] = fastpick;	
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pickingcarton', 'customdeploy_ebiz_rf_pickingcarton', false, SOarray);								
							nlapiLogExecution('ERROR', 'Navigating To1', 'Picking Qty Confirm');
							nlapiLogExecution('ERROR', 'test2', 'done');
						}
					}
					else{
						nlapiLogExecution('ERROR', 'Before AutoPacking 2 : Sales Order InternalId', SalesOrderInternalId);
						nlapiLogExecution('ERROR', 'Navigating To2', 'Stage Location');
						SOarray["custparam_fastpick"] = fastpick;	
						response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);

					}
				}
				else  
				{
					SOarray["custparam_fastpick"] = fastpick;	
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					nlapiLogExecution('ERROR', 'Error: ', 'Container# is invalid');
				}
			}			
		}
		else if (request.getParameter('hdnCloseflag') == 'F11')
		{
			if(request.getParameter('entercontainerno')=='' ||  request.getParameter('entercontainerno')==null)
			{
				SOarray["custparam_fastpick"] = fastpick;	
				SOarray["custparam_error"] = 'PLEASE ENTER/SCAN CARTON #';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				return;
			}

			if(request.getParameter('enterqty')=='' ||  request.getParameter('enterqty')==null)
			{
				SOarray["custparam_error"] = 'PLEASE ENTER QTY';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				return;
			}

			if( parseInt(vActqty) != parseInt(newqty))
			{
				SOarray["custparam_error"] = 'ENTER QUANTITY IS NOT MATCHED WITH REMAINING QUANTITY';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				nlapiLogExecution('ERROR', 'Error: ', 'quantity is not matched with remaining quantity');
				return;
			}
			
			var vOpenTaskRecs=fnValidateEnteredContLP(newcarton,SalesOrderInternalId);
			nlapiLogExecution('ERROR', 'Time Stamp at the end of fnValidateEnteredContLP',TimeStampinSec());
			if(vOpenTaskRecs != null && vOpenTaskRecs != '')
			{
				SOarray["custparam_fastpick"] = fastpick;	
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				nlapiLogExecution('ERROR', 'Error: ', 'Container# is invalid');
			}
			else
			{	
				var result = fnValidateContainer(newcarton,getFetchedContainerNo, '2','2',vSite);
				if(result == false)
				{
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					nlapiLogExecution('ERROR', 'Error: ', 'Container# is invalid');
					return;
				}

				var getEnteredContainerNoPrefix = newcarton.substring(0, 3).toUpperCase();
				var LPReturnValue = ebiznet_LPRange_CL_withLPType(newcarton, '2','2',vSite);//'2'UserDefiend,'2'PICK
				nlapiLogExecution('ERROR', 'Time Stamp at the end of ebiznet_LPRange_CL_withLPType',TimeStampinSec());
				if(LPReturnValue == true){
					nlapiLogExecution('ERROR', 'getItem', ItemNo);
					var arrDims = getSKUCubeAndWeightforconfirm(ItemNo, 1);
					nlapiLogExecution('ERROR', 'Time Stamp at the end of getSKUCubeAndWeightforconfirm',TimeStampinSec());
					var itemCube = 0;
					var itemWeight=0;						
					if (arrDims[0] != "" && (!isNaN(arrDims[1]))){
						itemCube = (parseFloat(newqty) * parseFloat(arrDims[0]));
						itemWeight = (parseFloat(newqty) * parseFloat(arrDims[1]));//									
					} 

					var ContainerCube;					
					var containerInternalId;
					var ContainerSize;
					if(getContainerSize=="" || getContainerSize==null)
					{
						getContainerSize=ContainerSize;
						nlapiLogExecution('ERROR', 'ContainerSize', ContainerSize);	
						ContainerCube = itemCube;
						TotalWeight=itemWeight;
					}	
					else
					{
						var arrContainerDetails = getContainerCubeAndTarWeight("",getContainerSize);
						nlapiLogExecution('ERROR', 'Time Stamp at the end of getContainerCubeAndTarWeight',TimeStampinSec());
						if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) {

							ContainerCube =  parseFloat(arrContainerDetails[0]);						
							containerInternalId = arrContainerDetails[3];
							ContainerSize=arrContainerDetails[4];
							TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));// added on 28 nov
						} 
					}
					nlapiLogExecution('ERROR', 'ContainerSizeInternalId', containerInternalId);				
					//Insert LP Record				
					var ResultText='';
					//nlapiLogExecution('ERROR', 'Time Stamp at the end of GenerateLable',TimeStampinSec());
					CreateRFMasterLPRecord(newcarton,containerInternalId,ContainerCube,TotalWeight,getContainerSize,ResultText,vSite);
					nlapiLogExecution('ERROR', 'Time Stamp at the end of CreateRFMasterLPRecord',TimeStampinSec());

					nlapiLogExecution('ERROR', 'Time Stamp at the end of UpdateAllContainerLp',TimeStampinSec());

					nlapiLogExecution('ERROR', 'Time Stamp at the end of UpdateRFFulfillOrdLine',TimeStampinSec());

					nlapiLogExecution('ERROR', 'Before Process', RcId);	

					vRemaningqty = PickQty-newqty;
					
					var opentaskcount=getOpenTasksCount(getWaveNo,getEnteredContainerNo,vPickType,SalesOrderInternalId,OrdName,vZoneId);

					var IsitLastPick='F';
					if(opentaskcount >  1)
						IsitLastPick='F';
					else
						IsitLastPick='T';
					
					nlapiLogExecution('ERROR', 'IsitLastPick', IsitLastPick);	
					UpdateOldPickTask(RcId,vRemaningqty,newcarton,itemCube,itemWeight,newqty,EndLocation,InvoiceRefNo,IsitLastPick,vBatchno);

					if(parseInt(vRemaningqty)>0)
					{
						CreateandConfirmPickTaskwithNewCarton(RcId,newqty,newcarton,itemCube,itemWeight,EndLocation,OrdName,InvoiceRefNo,IsitLastPick,vBatchno);
					}

					UpdateRFFulfillOrdLine(vdono,newqty);

					var recordcount=1;//it is iterator in GUI

					if(parseInt(vRemaningqty)>0)
					{
						SOarray["custparam_expectedquantity"]=vRemaningqty;
						SOarray["custparam_fastpick"] = fastpick;	
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pickingcarton', 'customdeploy_ebiz_rf_pickingcarton', false, SOarray);
						return;
					}

					var opentaskcount=getOpenTasksCount(getWaveNo,getEnteredContainerNo,vPickType,SalesOrderInternalId,OrdName,vZoneId);

					if(opentaskcount > 0)
					{
						if(BeginLocation != NextShowLocation)
						{ 
							SOarray["custparam_beginLocationname"]=null;
							SOarray["custparam_iteminternalid"]=null;
							SOarray["custparam_expectedquantity"]=null;
							SOarray["custparam_fastpick"] = fastpick;	
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, SOarray);								
							nlapiLogExecution('ERROR', 'Navigating To1', 'Picking Location');
						}
						else if(ItemNo != NextShowItem)
						{ 
							SOarray["custparam_iteminternalid"] = NextShowItem;
							SOarray["custparam_beginLocationname"]=null;
							SOarray["custparam_expectedquantity"]=null;
							SOarray["custparam_iteminternalid"] = NextShowItem;
							SOarray["custparam_fastpick"] = fastpick;	
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, SOarray);								
							nlapiLogExecution('ERROR', 'Navigating To1', 'Picking Item');
						}
						else if(ItemNo == NextShowItem)
						{ 
							SOarray["custparam_iteminternalid"] = NextShowItem;
							SOarray["custparam_beginLocationname"]=null;
							SOarray["custparam_expectedquantity"]=nextexpqty;
							SOarray["custparam_fastpick"] = fastpick;	
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pickingcarton', 'customdeploy_ebiz_rf_pickingcarton', false, SOarray);								
							nlapiLogExecution('ERROR', 'Navigating To1', 'Picking Qty Confirm');
							nlapiLogExecution('ERROR', 'test2', 'done');
						}
					}
					else{
						nlapiLogExecution('ERROR', 'Before AutoPacking 2 : Sales Order InternalId', SalesOrderInternalId);
						nlapiLogExecution('ERROR', 'Navigating To2', 'Stage Location');
						SOarray["custparam_fastpick"] = fastpick;	
						response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);

					}
				}
				else  
				{
					SOarray["custparam_fastpick"] = fastpick;	
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					nlapiLogExecution('ERROR', 'Error: ', 'Container# is invalid');
				}
			}	
		}
	}
}

function UpdateOldPickTask(RcId,vRemaningqty,newcarton,itemCube,itemWeight,actqty,EndLocation,InvoiceRefNo,IsitLastPick,vBatchno)
{
	nlapiLogExecution('ERROR', 'Into UpdateOldPickTask...');

	var str = 'RcId. = ' + RcId + '<br>';
	str = str + 'vRemaningqty. = ' + vRemaningqty + '<br>';	
	str = str + 'newcarton. = ' + newcarton + '<br>';	
	str = str + 'itemCube. = ' + itemCube + '<br>';
	str = str + 'itemWeight. = ' + itemWeight + '<br>';
	str = str + 'actqty. = ' + actqty + '<br>';
	str = str + 'EndLocation. = ' + EndLocation + '<br>';
	str = str + 'IsitLastPick. = ' + IsitLastPick + '<br>';
	str = str + 'vBatchno. = ' + vBatchno + '<br>';

	nlapiLogExecution('ERROR', 'Function Parameters', str);

	var opentaskrec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RcId);	

	if(parseInt(vRemaningqty)>0)
	{
		opentaskrec.setFieldValue('custrecord_expe_qty',parseInt(vRemaningqty));	
	}
	else
	{
		opentaskrec.setFieldValue('custrecord_expe_qty',parseInt(actqty));	
		opentaskrec.setFieldValue('custrecord_act_qty',parseInt(actqty));	
		opentaskrec.setFieldValue('custrecord_act_end_date', DateStamp());
		opentaskrec.setFieldValue('custrecord_actualendtime', TimeStamp());
		opentaskrec.setFieldValue('custrecord_actendloc', EndLocation);
		opentaskrec.setFieldValue('custrecord_wms_status_flag', 8);
		opentaskrec.setFieldValue('custrecord_container_lp_no', newcarton);
		opentaskrec.setFieldValue('custrecord_total_weight', parseFloat(itemWeight).toFixed(4));
		opentaskrec.setFieldValue('custrecord_totalcube', parseFloat(itemCube).toFixed(4));
		opentaskrec.setFieldValue('custrecord_device_upload_flag', IsitLastPick);
		opentaskrec.setFieldValue('custrecord_batch_no',vBatchno);

		deleteAllocations(InvoiceRefNo,actqty,newcarton);
	}

	nlapiSubmitRecord(opentaskrec, false, true);

	nlapiLogExecution('ERROR', 'Out of UpdateOldPickTask...');
}

function CreateandConfirmPickTaskwithNewCarton(RcId,newqty,newcarton,itemCube,itemWeight,EndLocation,OrdName,InvoiceRefNo,IsitLastPick,vBatchno)
{
	nlapiLogExecution('ERROR', 'Into CreateandConfirmPickTaskwithNewCarton...');

	var str = 'RcId. = ' + RcId + '<br>';
	str = str + 'newqty. = ' + newqty + '<br>';	
	str = str + 'newcarton. = ' + newcarton + '<br>';	
	str = str + 'itemCube. = ' + itemCube + '<br>';
	str = str + 'itemWeight. = ' + itemWeight + '<br>';
	str = str + 'EndLocation. = ' + EndLocation + '<br>';
	str = str + 'OrdName. = ' + OrdName + '<br>';
	str = str + 'IsitLastPick. = ' + IsitLastPick + '<br>';
	str = str + 'vBatchno. = ' + vBatchno + '<br>';

	nlapiLogExecution('ERROR', 'Function Parameters', str);

	var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RcId);
	var vinvrefno = transaction.getFieldValue('custrecord_invref_no');
	var SalesOrderInternalId = transaction.getFieldValue('custrecord_ebiz_order_no');
	var ItemNo = transaction.getFieldValue('custrecord_sku');

	var Remainqtyrecord =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',RcId);	
	Remainqtyrecord.setFieldValue('custrecord_act_qty', parseInt(newqty)); 
	Remainqtyrecord.setFieldValue('custrecord_expe_qty', parseInt(newqty)); 
	Remainqtyrecord.setFieldValue('custrecord_wms_status_flag', 8);
	Remainqtyrecord.setFieldValue('custrecord_container_lp_no', newcarton);
	Remainqtyrecord.setFieldValue('custrecord_total_weight', parseFloat(itemWeight).toFixed(4));
	Remainqtyrecord.setFieldValue('custrecord_totalcube', parseFloat(itemCube).toFixed(4));
	Remainqtyrecord.setFieldValue('custrecord_act_end_date', DateStamp());
	Remainqtyrecord.setFieldValue('custrecord_actualendtime', TimeStamp());
	Remainqtyrecord.setFieldValue('custrecord_actendloc', EndLocation);
	Remainqtyrecord.setFieldValue('custrecord_batch_no', vBatchno);
	Remainqtyrecord.setFieldValue('name', OrdName);
	Remainqtyrecord.setFieldValue('custrecord_device_upload_flag', IsitLastPick);
	var rempick = nlapiSubmitRecord(Remainqtyrecord, false, true);

	deleteAllocations(vinvrefno,newqty,newcarton,newqty,null,SalesOrderInternalId,ItemNo,EndLocation);

	nlapiLogExecution('ERROR', 'Out of CreateandConfirmPickTaskwithNewCarton...');
}

function getOpenTasksCount(waveno,containerlpno,vPickType,SalesOrderInternalId,OrdName,vZoneId)
{
	nlapiLogExecution('ERROR', 'Into getOpenTasksCount...');

	var str = 'waveno. = ' + waveno + '<br>';
	str = str + 'containerlpno. = ' + containerlpno + '<br>';	
	str = str + 'vPickType. = ' + vPickType + '<br>';	
	str = str + 'SalesOrderInternalId. = ' + SalesOrderInternalId + '<br>';
	str = str + 'OrdName. = ' + OrdName + '<br>';
	str = str + 'vZoneId. = ' + vZoneId + '<br>';

	nlapiLogExecution('ERROR', 'Function Parameters', str);

	var openreccount=0;
	var SOFilters = new Array();
	var SOColumns = new Array();

	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK	
	
	//Case # 201415009  start
	/*if(containerlpno !=null && containerlpno !='' && containerlpno !='null')
		SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', containerlpno));	*/
	//Case # 201415009  end
	
	if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && waveno != null && waveno != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(waveno)));
	}	 
	if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
	{
		nlapiLogExecution('ERROR', 'OrdNo inside If', OrdName);
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
	}
	if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && SalesOrderInternalId!=null && SalesOrderInternalId!="" && SalesOrderInternalId!= "null")
	{
		nlapiLogExecution('ERROR', 'SO Id inside If', SalesOrderInternalId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId));
	}
	if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
	{
		nlapiLogExecution('ERROR', 'Zone # inside If', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
	}
	if((vPickType.indexOf('C') != -1 || vPickType=='ALL') && containerlpno!=null && containerlpno!="" && containerlpno!= "null")
	{
		nlapiLogExecution('DEBUG', 'Carton # inside If', containerlpno);
		SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', containerlpno));			
	}
	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	if(SOSearchResults!=null && SOSearchResults!=''&& SOSearchResults.length>0)
		openreccount=SOSearchResults.length;	

	nlapiLogExecution('ERROR', 'Out of getOpenTasksCount...',openreccount);

	return openreccount;

}


function updatelastpicktaskforoldcarton(containerlpno,SOOrder)
{
	nlapiLogExecution('ERROR', 'Into updatelastpicktaskforoldcarton', containerlpno);
	nlapiLogExecution('ERROR', 'SOOrder', SOOrder);

	var openreccount=0;

	var SOFilters = new Array();
	var SOColumns = new Array();
	var updatetask='T';

	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK	

	if(SOOrder!=null&&SOOrder!="")
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', parseInt(SOOrder)));

	if(containerlpno!=null && containerlpno!='' && containerlpno!='null')
		SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', containerlpno));	
	else
		SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'isempty'));	


	SOColumns[0] = new nlobjSearchColumn('custrecord_wms_status_flag');
	SOColumns[1] = new nlobjSearchColumn('custrecord_device_upload_flag');

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	if(SOSearchResults!=null && SOSearchResults!=''&& SOSearchResults.length>0)
	{
		for (var i = 0; i < SOSearchResults.length; i++){

			var status  = SOSearchResults[i].getValue('custrecord_wms_status_flag');
			var deviceuploadflag  = SOSearchResults[i].getValue('custrecord_device_upload_flag');

			nlapiLogExecution('ERROR', 'status', status);
			nlapiLogExecution('ERROR', 'deviceuploadflag', deviceuploadflag);

			if(status=='9')//Pick Generated
			{
				updatetask='F';
			}
		}
		nlapiLogExecution('ERROR', 'updatetask', updatetask);

		if(updatetask=='T')
		{
			nlapiSubmitField('customrecord_ebiznet_trn_opentask', SOSearchResults[0].getId(),'custrecord_device_upload_flag', 'T');
		}
	}

	nlapiLogExecution('ERROR', 'Out of updatelastpicktaskforoldcarton', updatetask);
}

function getAllContainers(containersize)
{
	nlapiLogExecution('ERROR', 'into getAllContainers', '');
	var containerslist = new Array();

	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_containertype', null, 'anyof', ['1','4']));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_containername', null, 'is', containersize));				

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('Internalid');

	containerslist = nlapiSearchRecord('customrecord_ebiznet_container', null, filters, columns);

	nlapiLogExecution('ERROR', 'out of getAllContainers', '');

	return containerslist;
}

function AutoPacking(vebizOrdNo){	
	nlapiLogExecution('ERROR', 'into AutoPacking', vebizOrdNo);
	if(vebizOrdNo!=null && vebizOrdNo!='')
	{
		nlapiLogExecution('ERROR', 'into AutoPacking vebizOrdNo is not null', vebizOrdNo);
		var vOrdAutoPackFlag="";
		var vStgAutoPackFlag="";
		var vStgCarrierType="";
		var vCarrierType="";
		var vStgCarrierName="";
		var vOrderAutoPackFlag = getAutoPackFlagforOrdType(vebizOrdNo);
		if(vOrderAutoPackFlag!=null&& vOrderAutoPackFlag.length>0){
			vOrdAutoPackFlag = vOrderAutoPackFlag[0].getValue('custrecord_autopackflag','custbody_nswmssoordertype');
			vCarrierType = vOrderAutoPackFlag[0].getText('custrecord_carrier_type','custbody_salesorder_carrier');
			nlapiLogExecution('ERROR', 'AutoPackFlag(Order Type)',vOrdAutoPackFlag);
			nlapiLogExecution('ERROR', 'Carrier(Order Type)',vCarrierType);
		}

		if(vOrdAutoPackFlag!='T' || (vCarrierType=="" ||vCarrierType==null)){
			var vStageAutoPackFlag = getAutoPackFlagforStage();
			if(vStageAutoPackFlag!=null && vStageAutoPackFlag.length>0){
				vStgCarrierType = vStageAutoPackFlag[0][0];
				vStgAutoPackFlag = vStageAutoPackFlag[0][1];
				vStgCarrierName = vStageAutoPackFlag[0][2];
				nlapiLogExecution('ERROR', 'AutoPackFlag(Stage)',vStgAutoPackFlag);
				nlapiLogExecution('ERROR', 'Carrier(Stage)',vStgCarrierType);
			}
		}

		if(vCarrierType=="" && vStgCarrierType!="")
			vCarrierType=vStgCarrierType;

		nlapiLogExecution('ERROR', 'Carrier', vCarrierType);


		if(vOrdAutoPackFlag=='T' || vStgAutoPackFlag=='T'){

			UpdatesInOpenTask(vebizOrdNo,vCarrierType,vStgCarrierName);
		}
	}
	nlapiLogExecution('ERROR', 'out of AutoPacking');
}

function getAutoPackFlagforStage(){
	nlapiLogExecution('ERROR', 'into getAutoPackFlagforStage');
	var vStgRule = new Array();
	vStgRule = getStageRule('', '', '', '', 'OUB');
	nlapiLogExecution('ERROR', 'Stage Rule', vStgRule);

	nlapiLogExecution('ERROR', 'out of getAutoPackFlagforStage');
	return vStgRule;
}

function getAutoPackFlagforOrdType(vebizOrdNo){
	nlapiLogExecution('ERROR', 'into getAutoPackFlagforOrdType eBizOrdNo',vebizOrdNo);
	var searchresults = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter('internalid', null, 'is', vebizOrdNo));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_carrier_type','custbody_salesorder_carrier');
	columns[1] = new nlobjSearchColumn('custrecord_autopackflag','custbody_nswmssoordertype');
	searchresults = nlapiSearchRecord('salesorder', null, filters, columns);
	nlapiLogExecution('ERROR', 'out of getAutoPackFlagforOrdType', searchresults);
	return searchresults;

}

function getOrderType(vebizOrdNo){
	nlapiLogExecution('ERROR', 'into getOrderType', vebizOrdNo);
	var searchresults = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter('tranid', null, 'is', vebizOrdNo));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custbody_salesorder_carrier');
	columns[1] = new nlobjSearchColumn('custrecord_autopackflag','custbody_nswmssoordertype');
	searchresults = nlapiSearchRecord('salesorder', null, filters, columns);
	nlapiLogExecution('ERROR', 'out of getOrderType', vOrderType);
	return searchresults;
}

function getAutoPackFlagforStageRule(vStgRule){
	nlapiLogExecution('ERROR', 'into getAutoPackFlagforStageRule', vStgRule);
	var vAutoPackFlag='F';
	var filters = new Array();
	filters.push(new nlobjSearchFilter('name', null, 'is', vStgRule));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_stgautopackflag');
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters, columns);
	if(searchresults){
		vAutoPackFlag = searchresults[0].getValue('custrecord_stgautopackflag');		
	}
	nlapiLogExecution('ERROR', 'out of getAutoPackFlagforStageRule', vAutoPackFlag);
	return vAutoPackFlag;
}

function UpdatesInOpenTask(vebizOrdNo,vCarrierType,CarrieerName){
	nlapiLogExecution('ERROR', 'into UpdatesInOpenTask function', vebizOrdNo);
	nlapiLogExecution('ERROR', 'CarrierType', vCarrierType);
	var packstation = getPackStation();
	var containerlpArray=new Array();var ebizorderno;
	var filters = new Array();
	nlapiLogExecution('ERROR','orderno in getSalesOrderlist',vebizOrdNo);

	if(vebizOrdNo!=""){
		filters.push( new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', vebizOrdNo));
	}

	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//Status Flag - C (Picks Confirmed)
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK
	//filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vContLp));//Task Type - PICK

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[4] = new nlobjSearchColumn('custrecord_comp_id');
	columns[5] = new nlobjSearchColumn('custrecord_site_id');
	columns[6] = new nlobjSearchColumn('custrecord_container');
	columns[7] = new nlobjSearchColumn('custrecord_total_weight');
	columns[8] = new nlobjSearchColumn('custrecord_sku');

	columns[0].setSort();
	columns[1].setSort();
	var salesOrderList = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	
	if(salesOrderList != null && salesOrderList.length > 0){
		var orderno, containerno,ebizcntrlno,siteid,compid,containersize,totweight,internalid,item;	
		for (var i = 0; i < salesOrderList.length; i++){
			orderno = salesOrderList[i].getValue('name');
			containerno = salesOrderList[i].getValue('custrecord_container_lp_no');
			ebizorderno = salesOrderList[i].getValue('custrecord_ebiz_order_no');
			ebizcntrlno = salesOrderList[i].getValue('custrecord_ebiz_cntrl_no');
			compid = salesOrderList[i].getValue('custrecord_comp_id');
			siteid = salesOrderList[i].getValue('custrecord_site_id');
			containersize = salesOrderList[i].getValue('custrecord_container');
			totweight = salesOrderList[i].getValue('custrecord_total_weight');
			item = salesOrderList[i].getValue('custrecord_sku');//modified on dec 12

			internalid = salesOrderList[i].getId();
			containerlpArray[i]=containerno;
			updateStatusInOpenTask(internalid,28);//28 - Status Flag - Outbound Pack Complete
			updateStatusInLPMaster(containerno,28);//28 - Status Flag - Outbound Pack Complete
			CreatePACKTask(compid,siteid,ebizorderno,orderno,containerno,14,packstation,packstation,28,ebizcntrlno,item);//14 - Task Type - PACK

//			if(vCarrierType=="PC")
//			CreateShippingManifestRecord(ebizorderno,containerno,CarrieerName);
		}
		if(containerlpArray.length>0&&vCarrierType=="PC")
		{
			var oldcontainer="";
			for (var k = 0; k < containerlpArray.length; k++)
			{				
				var containerlpno = containerlpArray[k];
				nlapiLogExecution('ERROR', 'Old Container', oldcontainer);
				nlapiLogExecution('ERROR', 'New Container', containerlpno);
				if(oldcontainer!=containerlpno)
				{
					CreateShippingManifestRecord(ebizorderno,containerlpArray[k],CarrieerName);
					oldcontainer = containerlpno;
				}
			}
		}
	}

	nlapiLogExecution('ERROR', 'out of UpdatesInOpenTask function', vebizOrdNo);
}


/**
 * This function clear the allocation qty from inventory.
 * @param invrefno
 * @param PickQty
 */
function deleteAllocations(invrefno,ActPickQty,contlpno,expPickQty,remQty,SalesOrderInternalId,itemno,binlocation)
{
	nlapiLogExecution('ERROR', 'Into deleteAllocations (Inv Ref NO)',invrefno);
	var str = 'Inv Ref No. = ' + invrefno + '<br>';
	str = str + 'CARTON LP. = ' + contlpno + '<br>';	
	str = str + 'ActPickQty. = ' + ActPickQty + '<br>';	
	str = str + 'SalesOrderInternalId. = ' + SalesOrderInternalId + '<br>';	
	str = str + 'itemno. = ' + itemno + '<br>';	
	str = str + 'binlocation. = ' + binlocation + '<br>';	
	nlapiLogExecution('ERROR', 'Parameter Values', str);

	try
	{
		var invtfilters = new Array(); 			 
		invtfilters[0] = new nlobjSearchFilter('internalidnumber', null, 'equalto', invrefno);

		var invtcolumns1 = new Array(); 
		invtcolumns1[0] = new nlobjSearchColumn( 'custrecord_ebiz_inv_sku' ); 			
		invtcolumns1[1] = new nlobjSearchColumn( 'custrecord_ebiz_inv_binloc' );

		var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, invtfilters, invtcolumns1); 

		nlapiLogExecution('Debug', 'invtsearchresults', invtsearchresults);

		if(invtsearchresults!=null && invtsearchresults!='' && invtsearchresults.length>0)
		{
			nlapiLogExecution('Debug', 'invtsearchresults length', invtsearchresults.length);

			var scount=1;
			var invtrecid;
			var newqoh = 0;
			LABL1: for(var i=0;i<scount;i++)
			{	

				nlapiLogExecution('ERROR', 'CUSTOM_RECORD_COLLISION', i);
				try
				{
					var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
					var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
					var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');
					var vNewAllocQty=0;
					if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {
						vNewAllocQty=parseInt(Invallocqty)- parseInt(ActPickQty);
						if(parseFloat(vNewAllocQty)<0)
							vNewAllocQty=0;

						Invttran.setFieldValue('custrecord_ebiz_alloc_qty',parseInt(vNewAllocQty));
					}
					else
						Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);

					nlapiLogExecution('ERROR', 'vNewAllocQty',vNewAllocQty);


					newqoh = parseInt(InvQOH) - parseInt(ActPickQty);
					Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(newqoh)); 

					Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
					Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

					invtrecid = nlapiSubmitRecord(Invttran, false, true);
					nlapiLogExecution('ERROR', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);


				}
				catch(ex)
				{
					var exCode='CUSTOM_RECORD_COLLISION'; 
					var wmsE='Inventory record being updated by another user. Please try again...';
					if (ex instanceof nlobjError) 
					{	
						wmsE=ex.getCode() + '\n' + ex.getDetails();
						exCode=ex.getCode();
					}
					else
					{
						wmsE=ex.toString();
						exCode=ex.toString();
					} 

					nlapiLogExecution('ERROR', 'Exception in RF Inv move : ', wmsE); 
					if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
					{ 
						scount=scount+1;
						continue LABL1;
					}
					else break LABL1;
				}
			}

			if(invtrecid!=null && invtrecid!='' && parseFloat(ActPickQty)>0)
				CreateSTGInvtRecord(invtrecid, contlpno,ActPickQty);

			if((parseInt(newqoh)) <= 0)
			{		
				nlapiLogExecution('Error', 'Deleting record from inventory if QOH becomes zero', invrefno);
				var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
			}
		}
		else
		{
			clearItemAllocations(ActPickQty,contlpno,expPickQty,remQty,SalesOrderInternalId,itemno,binlocation);
		}
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR', 'Exception in deleteAllocations',exp);
	}		
}

/**
 * This function creats outbound staging inventory
 * @param invtrecid
 * @param vContLp
 * @param vqty
 */

function CreateSTGInvtRecord(invtrecid, vContLp,vqty) 
{
	nlapiLogExecution('ERROR', 'Into CreateSTGInvtRecord');
	var str = 'Inv Ref No. = ' + invtrecid + '<br>';
	str = str + 'CARTON LP. = ' + vContLp + '<br>';	
	str = str + 'Qty. = ' + vqty + '<br>';	
	nlapiLogExecution('ERROR', 'STG INVT Details', str);

	try
	{
		var stgmInvtRec = nlapiCopyRecord('customrecord_ebiznet_createinv',invtrecid);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_lp', vContLp);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_qty', vqty);
		stgmInvtRec.setFieldValue('custrecord_ebiz_qoh', vqty);
		stgmInvtRec.setFieldValue('custrecord_ebiz_alloc_qty', '0');
		stgmInvtRec.setFieldValue('custrecord_wms_inv_status_flag', '18');	//FLAG.INVENTORY.OUTBOUND('O') 		
		stgmInvtRec.setFieldValue('custrecord_invttasktype', '3'); //Task Type - PICK
		stgmInvtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
		stgmInvtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');			
		nlapiSubmitRecord(stgmInvtRec, false, true);
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR', 'Exception in CreateSTGInvtRecord',exp);
	}

	nlapiLogExecution('ERROR', 'Out of CreateSTGInvtRecord');
}

/**
 * 
 * @param lp
 * @returns
 */
function containerRFLPExists(lp){
	var filters = new Array();
	nlapiLogExecution('ERROR', 'lp:',lp); 
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lp));	 
	nlapiLogExecution('ERROR', 'Master LP Feaching','Success');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
	return searchresults;
}

function OpenTaskRFLPExists(lp){
	var filters = new Array();
	nlapiLogExecution('ERROR', 'lp:',lp); 
	filters.push( new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', lp));	 

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');

	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	return searchresults;
}

function getLPWeight(lp){
	var filters = new Array();
	nlapiLogExecution('ERROR', 'lp:',lp); 
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lp));	 
	nlapiLogExecution('ERROR', 'Master LP Feaching','Success');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
	var lpmastArr=new Array();
	nlapiLogExecution('ERROR', 'Before:',lp); 
	if(searchresults != null && searchresults!= "" && searchresults.length>0)
	{
		lpmastArr.push(searchresults[0].getValue('custrecord_ebiz_lpmaster_lp'));
		lpmastArr.push(searchresults[0].getValue('custrecord_ebiz_lpmaster_totwght'));
		nlapiLogExecution('ERROR', 'lp:',searchresults[0].getValue('custrecord_ebiz_lpmaster_lp')); 
		nlapiLogExecution('ERROR', 'lpweight:',searchresults[0].getValue('custrecord_ebiz_lpmaster_totwght')); 
	}
	return lpmastArr;
}
function containerSizeExists(lp,SizeId){
	var filters = new Array();
	nlapiLogExecution('ERROR', 'lp:',lp); 
	nlapiLogExecution('ERROR', 'SizeId:',SizeId); 
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lp));
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_sizeid', null, 'anyof', SizeId));	
	nlapiLogExecution('ERROR', 'Size Feaching','Success');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sizeid');
	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);        	       	
	return searchresults;
}
/**
 * 
 * @param vContLpNo
 * @param vContainerSize
 * @param TotalWeight
 * @param ContainerCube
 */
function CreateRFMasterLPRecord(vContLpNo,containerInternalId,ContainerCube,ItemWeight,getContainerSize,ResultText,vSite)
{
	var contLPExists = containerRFLPExists(vContLpNo);
//	var LPWeightExits = getLPWeight(vContLpNo);
//	if(LPWeightExits!=null && LPWeightExits!="")
//	{		
//	var ContLP=LPWeightExits[0];
//	var LPWeight=LPWeightExits[1];
//	nlapiLogExecution('ERROR', 'LPWeight',LPWeight);
//	}
//	var TotalWeight;	

//	if(getContainerSize=="" || getContainerSize==null)
//	{
//	TotalWeight = ItemWeight;
//	}	
//	else
//	{
//	var arrContainerDetails = getContainerCubeAndTarWeight(containerInternalId,"");

//	if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) 
//	{		
//	TotalWeight = (parseFloat(ItemWeight) + parseFloat(arrContainerDetails[1]));
//	} 
//	}

	var TotalWeight=ItemWeight;


	if(contLPExists==null){		

		var MastLP=nlapiCreateRecord('customrecord_ebiznet_master_lp');
		MastLP.setFieldValue('name', vContLpNo);
		MastLP.setFieldValue('custrecord_ebiz_lpmaster_lp', vContLpNo);
		MastLP.setFieldValue('custrecord_ebiz_lpmaster_site', vSite);
		MastLP.setFieldValue('custrecord_ebiz_lpmaster_lptype', '2');


		if(containerInternalId!="" && containerInternalId!=null)
		{
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_sizeid', containerInternalId);
		}	
		nlapiLogExecution('ERROR', 'TotalWeight',TotalWeight);
		nlapiLogExecution('ERROR', 'ContainerCube',ContainerCube);
		nlapiLogExecution('ERROR', 'vSite',vSite);

		if(TotalWeight!="" && TotalWeight!=null)
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_totwght', TotalWeight);
		if(ContainerCube!="" && ContainerCube!=null)
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_totcube', ContainerCube);
		if(ResultText != null && ResultText != '')
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_sscc', ResultText);
		var currentContext = nlapiGetContext();		        		
		var retktoval = nlapiSubmitRecord(MastLP);
		nlapiLogExecution('ERROR', 'Master LP Insertion','Success');

	}
	else
	{
		var recordId="";

		if(contLPExists!=null){


			for (var i = 0; i < contLPExists.length; i++){
				recordId = contLPExists[i].getId();
				LPWeight = contLPExists[i].getValue('custrecord_ebiz_lpmaster_totwght');
				nlapiLogExecution('ERROR', 'recordId',recordId);		

				var MastLPUpdate = nlapiLoadRecord('customrecord_ebiznet_master_lp', recordId);	
				TotWeight=parseFloat(ItemWeight)+parseFloat(LPWeight);
				nlapiLogExecution('ERROR', 'itemWeight',ItemWeight);
				nlapiLogExecution('ERROR', 'LPWeight',LPWeight);
				nlapiLogExecution('ERROR', 'TotWeight',TotWeight);

				if(TotWeight!="" && TotWeight!=null)
					MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_totwght', TotWeight);
				if(ContainerCube!="" && ContainerCube!=null)
					MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_totcube', ContainerCube);
				if(ResultText != null && ResultText != '')
					MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_sscc', ResultText);
				var currentContext = nlapiGetContext();	        				
				nlapiSubmitRecord(MastLPUpdate, false, true);        				 
				nlapiLogExecution('ERROR', 'Master LP Updation','Success');

			}

		}
	}
}
/**
 * 
 * @param WaveNo
 * @param RecordInternalId
 * @param ContainerLPNo
 * @param ExpectedQuantity
 * @param BeginLocation
 * @param Item
 * @param ItemDescription
 * @param ItemInternalId
 * @param DoLineId
 * @param InvoiceRefNo
 * @param BeginLocationName
 * @param EndLocInternalId
 * @param EndLocation
 * @param OrderLineNo
 * @param SerialNo
 * @param vBatchno
 */
function ItemFulfillment(WaveNo, RecordInternalId, ContainerLPNo, ExpectedQuantity, BeginLocation, Item, ItemDescription, ItemInternalId, DoLineId, InvoiceRefNo, BeginLocationName, EndLocInternalId, EndLocation, OrderLineNo,SerialNo,vBatchno){
	var now = new Date();
	var lineCnt = request.getLineItemCount('custpage_items');

	nlapiLogExecution('ERROR', "Line Count" + lineCnt);
	var invtfalg = "";
	var ShortPick = "";
	var invnotfound = "";

	//getting time

	var a_p = "";
	var d = new Date();
	var curr_hour = now.getHours();
	if (curr_hour < 12) {
		a_p = "am";
	}
	else {
		a_p = "pm";
	}
	if (curr_hour == 0) {
		curr_hour = 12;
	}
	if (curr_hour > 12) {
		curr_hour = curr_hour - 12;
	}

	var curr_min = now.getMinutes();

	curr_min = curr_min + "";

	if (curr_min.length == 1) {
		curr_min = "0" + curr_min;
	}

	nlapiLogExecution('ERROR', "RecordInternalId", RecordInternalId);
	nlapiLogExecution('ERROR', "ExpectedQuantity", ExpectedQuantity);
	nlapiLogExecution('ERROR', "ExpectedQuantity", ExpectedQuantity);
	nlapiLogExecution('ERROR', "EndLocInternalId", EndLocInternalId);

	//updating allocqty in opentask
	var OpenTaskTransaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordInternalId);
	OpenTaskTransaction.setFieldValue('custrecord_wms_status_flag', '8');//Statusflag='C'
	OpenTaskTransaction.setFieldValue('custrecord_act_qty', ExpectedQuantity);
	OpenTaskTransaction.setFieldValue('custrecordact_end_date', DateStamp());
	OpenTaskTransaction.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
	OpenTaskTransaction.setFieldValue('custrecord_actendloc', EndLocInternalId);

	nlapiLogExecution('DEBUG', 'Done PICK Record Insertion :', 'Success');

	//Updating DO 

	var DOLine = nlapiLoadRecord('customrecord_ebiznet_ordline', DoLineId);
	DOLine.setFieldValue('custrecord_linestatus_flag', '1'); //Statusflag='C'
	DOLine.setFieldValue('custrecord_pickqty', parseInt(ExpectedQuantity));
	var SalesOrderInternalId = DOLine.getFieldValue('name');

	try {
		nlapiLogExecution('ERROR', 'Main Sales Order Internal Id', SalesOrderInternalId);
		var TransformRec = nlapiTransformRecord('salesorder', SalesOrderInternalId, 'itemfulfillment');

		var SOLength = TransformRec.getLineItemCount('item');
		nlapiLogExecution('ERROR', "SO Length", SOLength);

		/*
         for (var j = 0; j < SOLength; j++)
         {
         nlapiLogExecution('ERROR', "Get Line Value", TransformRec.getLineItemValue('item', 'line', j + 1));
         TransformRec.setLineItemValue('item', 'itemfulfillment', j + 1, 'F');
         }
         TransformRec.setLineItemValue('item', 'itemfulfillment', OrderLineNo, 'T');
         TransformRec.setLineItemValue('item', 'quantity', OrderLineNo, ExpectedQuantity);
         var TransformRecId = nlapiSubmitRecord(TransformRec, true);
		 */
		for (var j = 1; j <= SOLength; j++) {
			nlapiLogExecution('ERROR', "Get Line Value" + TransformRec.getLineItemValue('item', 'line', j ));

			var item_id = TransformRec.getLineItemValue('item', 'item', j);
			var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', j);
			var itemLineNo = TransformRec.getLineItemValue('item', 'line', j);

			if (itemLineNo == OrderLineNo) {
				TransformRec.selectLineItem('item', j);
				TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'T');
				TransformRec.setCurrentLineItemValue('item', 'quantity', ExpectedQuantity);
				//TransformRec.setCurrentLineItemValue('item', 'location', 1);
				var itemSubtype = nlapiLookupField('item', ItemInternalId, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
				var serialsspaces="";
				if (itemSubtype.recordType == 'serializedinventoryitem') {

					arrSerial= SerialNo.split(',');
					nlapiLogExecution('ERROR', "arrSerial", arrSerial);	
					for (var n = 0; n < arrSerial.length; n++) {
						if (n == 0) {
							serialsspaces = arrSerial[n];
						}
						else
						{
							serialsspaces += " " +arrSerial[n];
						}
					}					
					TransformRec.setCurrentLineItemValue('item', 'serialnumbers', serialsspaces);
				}
				//vBatchno
				if (itemSubtype.recordType == 'lotnumberedinventoryitem'){
					TransformRec.setCurrentLineItemValue('item', 'serialnumbers', vBatchno+'('+ExpectedQuantity+')');
				}


				TransformRec.commitLineItem('item');
			}
			else {
				TransformRec.selectLineItem('item', j);
				TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'F');
				TransformRec.commitLineItem('item');

			}
			//TransformRec.setLineItemValue('item', 'itemfulfillment', j + 1, 'F');
		}
		//			invtRec.setFieldValue('custrecord_ebiz_inv_loc', '1');//WH Location.

		//			TransformRec.setFieldValue('custbody_ebiz_fulfillmentord',vdono) 
		var TransformRecId = nlapiSubmitRecord(TransformRec, true);
	} 

	catch (e) {
		if (e instanceof nlobjError) 
			lapiLogExecution('ERROR', 'system error', e.getCode() + '\n' + e.getDetails());
		else 
			nlapiLogExecution('ERROR', 'unexpected error', e.toString());
	}

	///Updating qty,qoh in inventory
	nlapiLogExecution('ERROR', 'Invoice Ref No', InvoiceRefNo);
	var InventoryTranaction = nlapiLoadRecord('customrecord_ebiznet_createinv', InvoiceRefNo);

	var InvAllocQty = InventoryTranaction.getFieldValue('custrecord_ebiz_alloc_qty');
	var InvQOH = InventoryTranaction.getFieldValue('custrecord_ebiz_qoh');

	if (isNaN(InvAllocQty)) {
		InvAllocQty = 0;
	}
	InventoryTranaction.setFieldValue('custrecord_ebiz_qoh', parseInt(InvQOH) - parseInt(ExpectedQuantity));
	InventoryTranaction.setFieldValue('custrecord_ebiz_alloc_qty', parseInt(InvAllocQty) - parseInt(ExpectedQuantity));
	var InventoryTranactionId = nlapiSubmitRecord(InventoryTranaction, false, true);
	OpenTaskTransaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', TransformRecId);
	nlapiSubmitRecord(OpenTaskTransaction, true);
	nlapiSubmitRecord(DOLine, true);

	//    }

	//    var form = nlapiCreateForm('Pick  Confirmation');
	//    form.addField('custpage_label', 'label', '<h2>Pick Confirmation Successful</h2>');

	//    response.writePage(form);
}

function UpdateRFFulfillOrdLine(vdono,vActqty)
{
	var pickqtyfinal =0;
	nlapiLogExecution('ERROR', 'into UpdateRFFulfillOrdLine : ', vdono);

	try
	{

		var doline = nlapiLoadRecord('customrecord_ebiznet_ordline',vdono);	

		var oldpickQty=doline.getFieldValue('custrecord_pickqty');
		var ordQty=doline.getFieldValue('custrecord_ord_qty');

		if(isNaN(oldpickQty) || oldpickQty==null || oldpickQty=='')
			oldpickQty=0;

		if(isNaN(ordQty) || ordQty==null || ordQty=='')
			ordQty=0;

		if(isNaN(vActqty) || vActqty==null || vActqty=='')
			vActqty=0;

		pickqtyfinal=parseInt(oldpickQty)+parseInt(vActqty);

		var str = 'pickqtyfinal. = ' + pickqtyfinal + '<br>';
		str = str + 'oldpickQty. = ' + oldpickQty + '<br>';	
		str = str + 'ordQty. = ' + ordQty + '<br>';	
		str = str + 'vActqty. = ' + vActqty + '<br>';	

		nlapiLogExecution('ERROR', 'Qty Details', str);

//		if(parseInt(ordQty)>=parseInt(pickqtyfinal))
//		{
		doline.setFieldValue('custrecord_upddate', DateStamp());
		doline.setFieldValue('custrecord_pickqty', parseInt(pickqtyfinal));
		//doline.setFieldValue('custrecord_pickgen_qty', parseInt(pickqtyfinal));

		if(parseInt(pickqtyfinal)<parseInt(ordQty))
			doline.setFieldValue('custrecord_linestatus_flag', '11'); //STATUS.OUTBOUND.PARTIALLY_PROCESSED
		else
			doline.setFieldValue('custrecord_linestatus_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')

		nlapiSubmitRecord(doline, false, true);
//		}
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR', 'Exception in UpdateRFFulfillOrdLine',exp);
	}

	nlapiLogExecution('ERROR', 'Out of UpdateRFFulfillOrdLine ');
}


/**
 * This function clear the allocation qty from inventory.
 * @param invrefno
 * @param PickQty
 */
function deleteOldAllocations(invrefno,actPickQty,contlpno,expPickQty,vRemQty)
{
	nlapiLogExecution('ERROR', 'Into deleteOldAllocations (Inv Ref NO)',invrefno);

	try
	{
		var scount=1;
		var invtrecid;

		LABL1: for(var i=0;i<scount;i++)
		{	

			nlapiLogExecution('ERROR', 'CUSTOM_RECORD_COLLISION', i);
			try
			{
				var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
				var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
				var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

				if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {

					if(parseFloat(Invallocqty)- parseFloat(expPickQty)>=0)
						Invttran.setFieldValue('custrecord_ebiz_alloc_qty',parseFloat(Invallocqty)- parseFloat(expPickQty));
					else
						Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);

				}
				else
				{
					Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);
				}

				//Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(InvQOH) - parseInt(ActPickQty)); 
				Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
				Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

				var invtrecid = nlapiSubmitRecord(Invttran, false, true);
				nlapiLogExecution('ERROR', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);


			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 
				/*var exceptionname='RF Picking';
				var functionality='RFPick';
				var trantype=3;
				var vInvRecId=invrefno;
				var vcontainerLp=contlpno;
				nlapiLogExecution('ERROR', 'DetailsError', functionality);	
				nlapiLogExecution('ERROR', 'vcontainerLp', vcontainerLp);
				nlapiLogExecution('ERROR', 'vInvRecId', invrefno);
				var reference3=ActPickQty;
				var reference4 ="";
				var reference5 ="";
				var userId = nlapiGetUser();
				InsertExceptionLog(exceptionname,trantype, functionality, wmsE, invrefno, vcontainerLp,reference3,reference4,reference5, userId);*/
				nlapiLogExecution('ERROR', 'Exception in RF Inv move : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}

		/*if(invtrecid!=null && invtrecid!='')
			CreateSTGInvtRecord(invtrecid, contlpno,pickqty);
		 */
		/*if((parseInt(InvQOH) - parseInt(ActPickQty)) == 0)
		{		
			nlapiLogExecution('Error', 'Deleting record from inventory if QOH becomes zero', invrefno);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
		}*/
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR', 'Exception in deleteAllocations',exp);
	}

	nlapiLogExecution('ERROR', 'Out of deleteOldAllocations (Inv Ref NO)',invrefno);
}

/**
 * This function clear the allocation qty from inventory.
 * @param invrefno
 * @param PickQty
 */
function deleteNewAllocations(invrefno,actPickQty,contlpno,expPickQty,vRemQty)
{
	nlapiLogExecution('ERROR', 'Into deleteNewAllocations (Inv Ref NO)',invrefno);
	nlapiLogExecution('ERROR', 'actPickQty',actPickQty);
	nlapiLogExecution('ERROR', 'expPickQty',expPickQty);
	nlapiLogExecution('ERROR', 'vRemQty',vRemQty);

	try
	{
		var scount=1;
		var invtrecid;
		var InvQOH =0;
		LABL1: for(var i=0;i<scount;i++)
		{	

			nlapiLogExecution('ERROR', 'CUSTOM_RECORD_COLLISION', i);
			try
			{
				var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
				var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
				InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

				//Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(InvQOH) - (parseInt(expPickQty)-parseInt(vRemQty))); 
				Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(InvQOH) - parseInt(expPickQty)); 
				Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
				Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

				var invtrecid = nlapiSubmitRecord(Invttran, false, true);
				nlapiLogExecution('ERROR', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);


			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 
				/*var exceptionname='RF Picking';
				var functionality='RFPick';
				var trantype=3;
				var vInvRecId=invrefno;
				var vcontainerLp=contlpno;
				nlapiLogExecution('ERROR', 'DetailsError', functionality);	
				nlapiLogExecution('ERROR', 'vcontainerLp', vcontainerLp);
				nlapiLogExecution('ERROR', 'vInvRecId', invrefno);
				var reference3=ActPickQty;
				var reference4 ="";
				var reference5 ="";
				var userId = nlapiGetUser();
				InsertExceptionLog(exceptionname,trantype, functionality, wmsE, invrefno, vcontainerLp,reference3,reference4,reference5, userId);*/
				nlapiLogExecution('ERROR', 'Exception in RF Inv move : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}


		if(invtrecid!=null && invtrecid!='' && parseFloat(actPickQty)>0)
			CreateSTGInvtRecord(invtrecid, contlpno,actPickQty);

		if((parseInt(InvQOH) - parseInt(expPickQty)) <= 0)
		{		
			nlapiLogExecution('Error', 'Deleting record from inventory if QOH becomes zero', invrefno);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
		}
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR', 'Exception in deleteAllocations',exp);
	}	

	nlapiLogExecution('ERROR', 'Out of deleteNewAllocations (Inv Ref NO)',invrefno);
}



/**
 * @param getEnteredContainerNo
 * @param getFetchedContainerNo
 * @param SOOrder
 * @param OrdName
 */
function UpdateAllContainerLp(getEnteredContainerNo,getFetchedContainerNo,SOOrder,OrdName,waveno,vZoneId,vPickType)
{
	nlapiLogExecution('ERROR','Into UpdateAllContainerLp');

	try
	{

		var str = 'waveno. = ' + waveno + '<br>';
		str = str + 'SOOrder. = ' + SOOrder + '<br>';	
		str = str + 'OrdName. = ' + OrdName + '<br>';
		str = str + 'vZoneId. = ' + vZoneId + '<br>';
		str = str + 'getFetchedContainerNo. = ' + getFetchedContainerNo + '<br>';
		str = str + 'getEnteredContainerNo. = ' + getEnteredContainerNo + '<br>';
		str = str + 'vPickType. = ' + vPickType + '<br>';

		nlapiLogExecution('ERROR', 'Function Parameters', str);

		var context = nlapiGetContext();
		var orderno, containerno,ebizcntrlno,siteid,compid,containersize,totweight,internalid,item,waveno,
		beginloc,endloc,lot,actqty,expqty,uomlevel,empid,begindate,begintime,packcode,lp,lineno,Zoneno,ebizLP,
		taskassignedto;

		var otparent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent');
		var parentid = nlapiSubmitRecord(otparent); 
		var newParent = nlapiLoadRecord('customrecord_ebiz_throwaway_parent', parentid);

		var filteropentask=new Array();
		filteropentask.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
		filteropentask.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK	

		if(getFetchedContainerNo!=null && getFetchedContainerNo!='' && getFetchedContainerNo!='null')
			filteropentask.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getFetchedContainerNo));	
		else
			filteropentask.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'isempty'));	

		filteropentask.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));

		if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && waveno != null && waveno != "")
		{
			filteropentask.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(waveno)));
		}	 
		if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
		{
			filteropentask.push(new nlobjSearchFilter('name', null, 'is', OrdName));
		}
		if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && SOOrder!=null && SOOrder!="" && SOOrder!= "null")
		{
			filteropentask.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SOOrder));
		}
		if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
		{	
			filteropentask.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
		}

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('name');
		columns[1] = new nlobjSearchColumn('custrecord_container_lp_no');
		columns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		columns[4] = new nlobjSearchColumn('custrecord_comp_id');
		columns[5] = new nlobjSearchColumn('custrecord_wms_location');
		columns[6] = new nlobjSearchColumn('custrecord_container');
		columns[7] = new nlobjSearchColumn('custrecord_total_weight');
		columns[8] = new nlobjSearchColumn('custrecord_sku');
		columns[9] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[10] = new nlobjSearchColumn('custrecord_actendloc');
		columns[11] = new nlobjSearchColumn('custrecord_batch_no');
		columns[12] = new nlobjSearchColumn('custrecord_act_qty');
		columns[13] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[14] = new nlobjSearchColumn('custrecord_uom_level');
		columns[15] = new nlobjSearchColumn('custrecord_ebizuser');
		columns[16] = new nlobjSearchColumn('custrecordact_begin_date');
		columns[17] = new nlobjSearchColumn('custrecord_actualbegintime');
		columns[18] = new nlobjSearchColumn('custrecord_packcode');
		columns[19] = new nlobjSearchColumn('custrecord_lpno');
		columns[20] = new nlobjSearchColumn('custrecord_line_no');
		columns[21] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
		columns[22] = new nlobjSearchColumn('custrecord_ebiz_zoneid');
		columns[23] = new nlobjSearchColumn('custrecord_ebiz_lpno');
		columns[24] = new nlobjSearchColumn('custrecord_ebizzone_no');
		columns[25] = new nlobjSearchColumn('custrecord_taskassignedto');

		var SearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filteropentask, columns);
		if(SearchResults!=null&&SearchResults!="")
		{
			for ( var i = 0; i < SearchResults.length; i++) 
			{

//				var recid=SearchResults[count].getId();
//				var UpdateRec=nlapiSubmitField('customrecord_ebiznet_trn_opentask',recid,'custrecord_container_lp_no',getEnteredContainerNo);
//				nlapiLogExecution('ERROR','UpdateRec',UpdateRec);

				orderno = SearchResults[i].getValue('name');
				containerno = SearchResults[i].getValue('custrecord_container_lp_no');
				ebizorderno = SearchResults[i].getValue('custrecord_ebiz_order_no');
				ebizcntrlno = SearchResults[i].getValue('custrecord_ebiz_cntrl_no');
				compid = SearchResults[i].getValue('custrecord_comp_id');
				siteid = SearchResults[i].getValue('custrecord_wms_location');
				containersize = SearchResults[i].getValue('custrecord_container');
				totweight = SearchResults[i].getValue('custrecord_total_weight');
				item = SearchResults[i].getValue('custrecord_sku');
				beginloc = SearchResults[i].getValue('custrecord_actbeginloc');
				endloc = SearchResults[i].getValue('custrecord_actendloc');
				lot = SearchResults[i].getValue('custrecord_batch_no');
				actqty = SearchResults[i].getValue('custrecord_act_qty');
				expqty = SearchResults[i].getValue('custrecord_expe_qty');
				uomlevel = SearchResults[i].getValue('custrecord_uom_level');
				empid = SearchResults[i].getValue('custrecord_taskassignedto');
				internalid = SearchResults[i].getId();
				begindate=SearchResults[i].getValue('custrecordact_begin_date');
				begintime=SearchResults[i].getValue('custrecord_actualbegintime');
				packcode=SearchResults[i].getValue('custrecord_packcode');
				lp=SearchResults[i].getValue('custrecord_lpno');
				lineno=SearchResults[i].getValue('custrecord_line_no');
				waveno = SearchResults[i].getValue('custrecord_ebiz_wave_no');
				Zoneno = SearchResults[i].getValue('custrecord_ebiz_zoneid');
				ebizLP = SearchResults[i].getValue('custrecord_ebiz_lpno');
				var vZoneno=SearchResults[i].getValue('custrecord_ebizzone_no');
				taskassignedto = SearchResults[i].getValue('custrecord_taskassignedto');

				newParent.selectNewLineItem('recmachcustrecord_ebiz_ot_parent');

				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'id', internalid);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','name', orderno);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_wave_no', waveno);
				//newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualendtime', TimeStamp());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_recordupdatetime', TimeStamp());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_upd_date', DateStamp());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actendloc', endloc);
				//newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_end_date', DateStamp());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_batch_no', lot);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_status_flag', 9);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_location', siteid);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_uom_level', uomlevel);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_qty', actqty);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_upd_ebiz_user_no', context.getUser());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actbeginloc', beginloc);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecordact_begin_date',begindate);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualbegintime', begintime);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_taskassignedto', empid);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_cntrl_no', ebizcntrlno);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_tasktype', 3);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_expe_qty', expqty);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_sku', item);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_sku_no', item);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_packcode', packcode);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_lpno', lp);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_order_no', ebizorderno);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_line_no', lineno);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_comp_id', compid);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_container_lp_no', getEnteredContainerNo);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_container', containersize);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_total_weight', totweight);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_zoneid', Zoneno);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebizzone_no', vZoneno);

				if(ebizLP==null||ebizLP=="")
					newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_lpno', ebizLP);

				if(taskassignedto!=null&&taskassignedto!="")
					newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_taskassignedto', taskassignedto);

				newParent.commitLineItem('recmachcustrecord_ebiz_ot_parent');


			}
			nlapiSubmitRecord(newParent);
		}
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in UpdateAllContianerLp',exp);	
	}

	nlapiLogExecution('ERROR','Out of UpdateAllContianerLp');	
}



function fnValidateEnteredContLP(EnteredContLP,SalesOrderInternalId)
{
	nlapiLogExecution('ERROR', 'Into fnValidateEnteredContLP',EnteredContLP);	
	nlapiLogExecution('ERROR', 'SalesOrderInternalId',SalesOrderInternalId);	
	var filters=new Array();
	filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', EnteredContLP));
	if(SalesOrderInternalId!=null && SalesOrderInternalId!='' && SalesOrderInternalId!='null')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'noneof', SalesOrderInternalId));  
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');



	var salesOrderList = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return salesOrderList;
}

function GenerateLable(uompackflag){

	nlapiLogExecution('ERROR', 'CreateMasterLPRecord uompackflag', uompackflag);
	var finaltext="";
	var duns="";
	var label="",uom="",ResultText="";
	//added by mahesh


	try 
	{	
		var lpMaxValue=GetMaxLPNo('2', '5','');
		nlapiLogExecution('ERROR', 'lpMaxValue', lpMaxValue);

		var prefixlength=lpMaxValue.length;
		nlapiLogExecution('ERROR', 'prefixlength', prefixlength);

		if(prefixlength==0)
			label="000000000";
		else if(prefixlength==1)
			label="00000000"+lpMaxValue;
		else if(prefixlength==2)
			label="0000000"+lpMaxValue;
		else if(prefixlength==3)
			label="000000"+lpMaxValue;
		else if(prefixlength==4)
			label="00000"+lpMaxValue;
		else if(prefixlength==5)
			label="0000"+lpMaxValue;
		else if(prefixlength==6)
			label="000"+lpMaxValue;
		else if(prefixlength==7)
			label="00"+lpMaxValue;
		else if(prefixlength==8)
			label="0"+lpMaxValue;
		else if(prefixlength==9)
			label=lpMaxValue;

		//to get company id

		duns=GetCompanyDUNSnumber('');

		if(uompackflag == "1" || uompackflag == "EACH" ) 
			uom="0"; 
		else if(uompackflag == "3" || uompackflag == "PALLET") 
			uom="2";
		else
			uom="0";
		nlapiLogExecution('ERROR', 'uom', uom);
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord finaltext', finaltext);
		finaltext=uom+duns+label;
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord finaltext', finaltext);



		//to get chk digit
		var checkStr=finaltext;
		var ARL=0;
		var BRL=0;
		var CheckDigitValue="";
		for (i = checkStr.length-1;  i > 0;  i--)
		{
			ARL = ARL+parseInt(checkStr.charAt(i));
			i--;
		}		
		ARL=ARL*3;
		for (i = checkStr.length-2;  i > 0;  i--)
		{
			BRL = BRL+parseInt(checkStr.charAt(i));
			i--;
		}		
		var sumOfARLBRL=ARL+BRL;
		var CheckDigit=0;

		while(CheckDigit<10)
		{
			if(sumOfARLBRL%10==0)
			{ 
				CheckDigitValue=CheckDigit; 
				break; 
			} 

			sumOfARLBRL++;
			CheckDigit++;
		}
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord CheckDigit', CheckDigitValue.toString());
		ResultText="00"+finaltext+CheckDigitValue.toString();
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord ResultText', ResultText);
	} 
	catch (err) 
	{

	}
	return ResultText;
}

function GetCompanyDUNSnumber(company)
{
	var dunsfilters = new Array();
	var duns='';

	if(company!=null && company!='')
		dunsfilters.push(new nlobjSearchFilter('internalid', null, 'is', company));

	dunsfilters.push(new nlobjSearchFilter('custrecord_compduns', null, 'isnotempty'));
	dunsfilters.push(new nlobjSearchFilter('isinactive', null,'is', 'F'));

	var dunscolumns = new Array();
	dunscolumns[0] = new nlobjSearchColumn('custrecord_compduns');			 

	var dunssearchresults = nlapiSearchRecord('customrecord_ebiznet_company', null, dunsfilters, dunscolumns);
	if (dunssearchresults !=null && dunssearchresults !="") 
	{
		duns = dunssearchresults[0].getValue('custrecord_compduns');
		nlapiLogExecution('ERROR', 'duns', duns);
	}
	return duns;
}

function updateWtInLPMaster(containerlp){
	nlapiLogExecution('ERROR', 'In to updateWtInLPMaster...',containerlp);
	var recordId="";

	var filtersSO = new Array();
	filtersSO.push(new nlobjSearchFilter('name', null, 'is', containerlp));
	filtersSO.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_sscc', null, 'isempty'));

	var colsSO = new Array();
	colsSO[0] = new nlobjSearchColumn('name');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersSO, colsSO);

	if(searchresults){
		var ResultText=GenerateLable('1');
		for (var i = 0; i < searchresults.length; i++){

			recordId = searchresults[i].getId();
			nlapiLogExecution('ERROR', 'recordId',recordId);

			nlapiSubmitField('customrecord_ebiznet_master_lp', recordId,'custrecord_ebiz_lpmaster_sscc', ResultText);

			nlapiLogExecution('ERROR', 'Updated LP Master with SSCC',ResultText);
		}
	}

	nlapiLogExecution('ERROR', 'Out of updateWtInLPMaster');
}

function fnValidateContainer(EnteredContainerNo,FetchedContainerNo, type ,lptype,whloc)
{
	var vResult = "";
	var vargetlpno = EnteredContainerNo;

	nlapiLogExecution('ERROR', 'vargetlpno.length', vargetlpno.length);
	if (vargetlpno.length > 0) {
		var filters = new Array();

		filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'anyof', [type]));
		filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lptype', null, 'anyof', [lptype]));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		if(whloc!=null && whloc!="")
			filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_site', null, 'anyof', [whloc]));

		nlapiLogExecution('ERROR', 'userdefined type', type);

		var columns=new Array();
		columns.push(new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix'));
		columns.push(new nlobjSearchColumn('custrecord_ebiznet_lprange_begin'));
		columns.push(new nlobjSearchColumn('custrecord_ebiznet_lprange_end'));
		columns.push(new nlobjSearchColumn('custrecord_ebiznet_lprange_lpgentype'));
		columns.push(new nlobjSearchColumn('custrecord_ebiznet_lprange_lptype'));

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, filters,columns);
		if(searchresults!=null && searchresults.length>0)
		{
			for (var i = 0; i < searchresults.length; i++) {
				try {
					var getLPPrefix = searchresults[i].getValue('custrecord_ebiznet_lprange_lpprefix');

					var varBeginLPRange = searchresults[i].getValue('custrecord_ebiznet_lprange_begin');

					var varEndRange = searchresults[i].getValue('custrecord_ebiznet_lprange_end');

					var getLPGenerationTypeValue = searchresults[i].getValue('custrecord_ebiznet_lprange_lpgentype');
					var getLPTypeValue = searchresults[i].getValue('custrecord_ebiznet_lprange_lptype');

					if (getLPGenerationTypeValue == type) {
						var getLPrefix = searchresults[i].getValue('custrecord_ebiznet_lprange_lpprefix');
						var LPprefixlen = getLPrefix.length;
						var vLPLen = vargetlpno.substring(0, LPprefixlen);

						if (vLPLen == getLPrefix) {
							var varnum = vargetlpno.substring(LPprefixlen, vargetlpno.length);
							//nlapiLogExecution('ERROR', 'varnum', varnum.length);
							nlapiLogExecution('ERROR', 'varEndRange.length', varEndRange.length);
							
							if(varnum ==null || varnum =='')
							{
								vResult = "N";
								nlapiLogExecution('ERROR', 'varnum', varnum);
								break;
							}

							if (varnum.length > varEndRange.length) {
								vResult = "N";
								nlapiLogExecution('ERROR', 'length more than range', vResult);
								break;
							}								

							for(var j = 0; j < varnum.length; j++)
							{
								var a = varnum.charAt(j);

								if(a >= 'a' && a <= 'z')
								{
									vResult = "N";
									nlapiLogExecution('ERROR', 'contains lowercase a-z', vResult);
									break;
								}

								if(a >= 'A' && a <= 'Z')
								{
									vResult = "N";
									nlapiLogExecution('ERROR', 'contains uppercase A-Z', vResult);
									break;
								}
							}

							if(vResult == 'N')
							{
								break;
							}

							var EnteredContainer = "";
							for(var k = 0; k < varnum.length; k++)
							{
								var c = varnum.charAt(k);
								if(c != "0")
								{
									EnteredContainer = varnum.substring(k, varnum.length);
									break;
								}							   
							}
							nlapiLogExecution('ERROR', 'EnteredContainer', EnteredContainer);

							/*var FetchedContLPPrefix = FetchedContainerNo.substring(0, LPprefixlen);
							if(FetchedContLPPrefix == getLPrefix)
							{
								var FetchedContainerNo = FetchedContainerNo.substring(LPprefixlen, FetchedContainerNo.length);
								var FetchedContainer = "";
								for(var l = 0; l < FetchedContainerNo.length; l++)
								{
								   var d = FetchedContainerNo.charAt(l);
								   if(d != "0")
								   {
									   FetchedContainer = FetchedContainerNo.substring(l, FetchedContainerNo.length);
									   break;
								   }
								}

								nlapiLogExecution('ERROR', 'FetchedContainer', FetchedContainer);

								var diffContainerNo = 0;
								if(parseInt(EnteredContainer) > parseInt(FetchedContainer))
								{
									diffContainerNo = parseInt(EnteredContainer) - parseInt(FetchedContainer);
								}
								else
								{
									diffContainerNo = parseInt(FetchedContainer) - parseInt(EnteredContainer);
								}
								nlapiLogExecution('ERROR', 'diffContainerNo', diffContainerNo);
								if(parseInt(diffContainerNo) > 5000)
								{
									vResult = "N";
									nlapiLogExecution('ERROR', 'diff more than 5000', vResult);
									break;
								}
								else 
								{
									vResult = "Y";
									nlapiLogExecution('ERROR', 'diff less than 5000', vResult);
									break;
								}

								if(vResult == 'N')
								{
									break;
								}
							}*/						

							if ((parseFloat(varnum, 10) < parseFloat(varBeginLPRange)) || (parseFloat(varnum, 10) > parseFloat(varEndRange))) {
								vResult = "N";
								nlapiLogExecution('ERROR', 'not in range', vResult);
								break;
							}
							else {
								vResult = "Y";
								nlapiLogExecution('ERROR', 'in range', vResult);
								break;
							}						

						}
						else {
							nlapiLogExecution('ERROR', 'else', 'here');
							vResult = "N";
						}
					} //end of if statement
				} 

				catch (err) {
				}
			} //end of for loop
		}
		if (vResult == "Y") {
			nlapiLogExecution('ERROR', 'final if', 'true');
			return true;
		}
		else {
			nlapiLogExecution('ERROR', 'final else', 'false');
			return false;
		}
	}
	else {
		return false;
	}
}

function isPickFaceLocation(item,location)
{
	nlapiLogExecution('ERROR', 'Into isPickFaceLocation');

	var str = 'item. = ' + item + '<br>';
	str = str + 'location. = ' + location + '<br>';	

	nlapiLogExecution('ERROR', 'Function Parameters', str);

	var isPickFace='F';
	var PickFaceFilters = new Array();
	var PickFaceColumns = new Array();

	PickFaceFilters.push(new nlobjSearchFilter('custrecord_pickbinloc', null, 'anyof', location)); 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', item)); 
	PickFaceFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	
	PickFaceColumns[0] = new nlobjSearchColumn('custrecord_autoreplen');
	var PickFaceSearchResults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, PickFaceFilters, PickFaceColumns);
	if(PickFaceSearchResults!=null && PickFaceSearchResults!=''&& PickFaceSearchResults.length>0)
	{
		isPickFace='T';
	}

	nlapiLogExecution('ERROR', 'Out of isPickFaceLocation - Return Value',isPickFace);

	return isPickFace;
}

function getQOHForAllSKUsinPFLocation(location,item){

	nlapiLogExecution('ERROR','Into getQOHForAllSKUsinPFLocation');

	var str = 'item. = ' + item + '<br>';
	str = str + 'location. = ' + location + '<br>';	

	nlapiLogExecution('ERROR', 'Function Parameters', str);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', location));	
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', item));
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19','3']));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0));

	var columns = new Array();	
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');

	var pfSKUInvList = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

	nlapiLogExecution('ERROR','Out of getQOHForAllSKUsinPFLocation - Return Value',pfSKUInvList);

	return pfSKUInvList;
}

function buildTaskArray(WaveNo,ClusNo,OrdName,ebizOrdNo,vZoneId,vPickType)
{
	var SOarray = new Array();
	var SOFilters = new Array();
	var SOColumns = new Array();
	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

	if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && WaveNo != null && WaveNo != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(WaveNo)));
	}
	if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
	{
		nlapiLogExecution('ERROR', 'ClusNo inside If', ClusNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
	}
	if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
	{
		nlapiLogExecution('ERROR', 'OrdNo inside If', OrdName);
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
	}
	if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && ebizOrdNo!=null && ebizOrdNo!="" && ebizOrdNo!= "null")
	{
		nlapiLogExecution('ERROR', 'SO Id inside If', ebizOrdNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ebizOrdNo));
	}
	if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
	{
		nlapiLogExecution('ERROR', 'Zone # inside If', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
	}

	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
//	SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
	//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
	SOColumns.push(new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
	//Code end as on 290414
	SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
	SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
	SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
	SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
	SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
	SOColumns.push(new nlobjSearchColumn('name'));
	SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
	SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));

	SOColumns[0].setSort();
	SOColumns[1].setSort();
	SOColumns[2].setSort();
	SOColumns[3].setSort();
	SOColumns[4].setSort();
	SOColumns[5].setSort(true);

	SOarray["custparam_ebizordno"] = ebizOrdNo;

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

	if (SOSearchResults != null && SOSearchResults.length > 0) {

		var SOSearchResult = SOSearchResults[0];
		if(SOSearchResults.length > 1)
		{
			var SOSearchnextResult = SOSearchResults[1];
			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			nlapiLogExecution('ERROR', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
			nlapiLogExecution('ERROR', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
		}
		else
		{
			var SOSearchnextResult = SOSearchResults[0];
			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
			nlapiLogExecution('ERROR', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
			nlapiLogExecution('ERROR', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
		}

		SOarray["custparam_waveno"] = WaveNo;
		SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
		SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
		SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
		SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
		SOarray["custparam_endlocinternalid"] = SOSearchResult.getValue('custrecord_actbeginloc');		
		SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
		SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
		SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
		SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
		SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
		SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
		SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
		SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
		SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
		SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
		SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
		SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
		SOarray["custparam_noofrecords"] = SOSearchResults.length;		
		SOarray["name"] =  SOSearchResult.getValue('name');
		SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
		SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
		SOarray["custparam_picktype"] = vPickType;
		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
		}
		else
			SOarray["custparam_ebizzoneno"] = '';
	}

	return SOarray;
}

function getNextOpentaskInternalId(WaveNo,ClusNo,ebizOrdNo,vZoneId,vSkipId,SOarray,vPickType,OrdName)
{
	nlapiLogExecution('ERROR','getNextOpentaskInternalId',getNextOpentaskInternalId);
	nlapiLogExecution('ERROR','WaveNo',WaveNo);
	nlapiLogExecution('ERROR','ClusNo',ClusNo);
	nlapiLogExecution('ERROR','ebizOrdNo',ebizOrdNo);
	nlapiLogExecution('ERROR','vZoneId',vZoneId);
	nlapiLogExecution('ERROR','vSkipId',vSkipId);
	nlapiLogExecution('ERROR','vPickType',vPickType);
	nlapiLogExecution('ERROR','OrdName',OrdName);

	var recid='';
	var SOFilters = new Array();
	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

	if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && WaveNo != null && WaveNo != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(WaveNo)));
	}
	if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
	{
		nlapiLogExecution('ERROR', 'ClusNo inside If', ClusNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
	}
	if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
	{
		nlapiLogExecution('ERROR', 'OrdNo inside If', OrdName);
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
	}
	if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && ebizOrdNo!=null && ebizOrdNo!="" && ebizOrdNo!= "null")
	{
		nlapiLogExecution('ERROR', 'SO Id inside If', ebizOrdNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ebizOrdNo));
	}
	if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
	{
		nlapiLogExecution('ERROR', 'Zone # inside If', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
	}
	var SOColumns=new Array();

	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
	SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
	SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
	SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
	SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
	SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
	SOColumns.push(new nlobjSearchColumn('name'));
	SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
	SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));

	SOColumns[0].setSort();
	SOColumns[1].setSort();
	SOColumns[2].setSort();
	SOColumns[3].setSort();
	SOColumns[4].setSort();
	SOColumns[5].setSort(true);

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	nlapiLogExecution('ERROR', 'Results', SOSearchResults);
	nlapiLogExecution('ERROR', 'vSkipId', vSkipId);
	if (SOSearchResults != null && SOSearchResults.length > 0) 
	{nlapiLogExecution('ERROR', 'Results.length', SOSearchResults.length);
	//if(SOSearchResults.length > parseInt(vSkipId) + 1)//commented because last record values from open task are not passing to next screen
	//{

	vSkipId=0;

	if(SOSearchResults.length <= parseInt(vSkipId))
	{
		vSkipId=0;
	}
	var SOSearchResult = SOSearchResults[parseInt(vSkipId)];							
	SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
	SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
	SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
	SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
	SOarray["custparam_beginLocation"] = SOSearchResult.getText('custrecord_actbeginloc');
	SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
	SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
	SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
	SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
	SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
	SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
	SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
	SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
	SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
	SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
	SOarray["custparam_noofrecords"] = SOSearchResults.length;		
	SOarray["name"] =  SOSearchResult.getValue('name');
	SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
	SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
	if(vZoneId!=null && vZoneId!="")
	{
		SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
	}
	else
		SOarray["custparam_ebizzoneno"] = '';

	if(SOSearchResults.length > parseInt(vSkipId) + 1)
	{
		var SOSearchnextResult = SOSearchResults[parseInt(vSkipId) + 1];
		SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
		SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
		SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
		nlapiLogExecution('ERROR', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
		nlapiLogExecution('ERROR', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));		
		nlapiLogExecution('ERROR', 'Next Item Ex Qty', SOSearchnextResult.getValue('custrecord_expe_qty'));		
	}
	else
	{
		var SOSearchnextResult = SOSearchResults[0];
		SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
		SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
		nlapiLogExecution('ERROR', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
		nlapiLogExecution('ERROR', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));	
	}

	var SOSearchnextResult = SOSearchResults[parseInt(vSkipId) + 1];
	if(SOSearchnextResult!=null && SOSearchnextResult!='')
		recid = SOSearchnextResult.getId();
	//}
	}
	return recid;

}
