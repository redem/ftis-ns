/***************************************************************************
  	eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_ReplenBatch.js,v $
 *     	   $Revision: 1.2.2.4.4.2.4.9.2.1 $
 *     	   $Date: 2015/11/06 22:28:10 $
 *     	   $Author: sponnaganti $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_92 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_ReplenBatch.js,v $
 * Revision 1.2.2.4.4.2.4.9.2.1  2015/11/06 22:28:10  sponnaganti
 * case# 201414081
 * 2015.2 issue fix
 *
 * Revision 1.2.2.4.4.2.4.9  2015/08/06 15:35:31  skreddy
 * Case# 201413839
 * Bedgear SB issue fix
 *
 * Revision 1.2.2.4.4.2.4.8  2014/12/05 06:55:43  sponnaganti
 * case# 201411191
 * True Fab SB issue fix
 *
 * Revision 1.2.2.4.4.2.4.7  2014/06/23 07:27:52  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox) Issue Fixed.
 *
 * Revision 1.2.2.4.4.2.4.6  2014/05/30 00:34:22  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.2.2.4.4.2.4.5  2013/11/14 14:32:30  rmukkera
 * Case # 20125742
 *
 * Revision 1.2.2.4.4.2.4.4  2013/10/07 13:47:55  schepuri
 * 20124679
 *
 * Revision 1.2.2.4.4.2.4.3  2013/09/06 00:27:35  nneelam
 * Case#.20124289��
 * Wrong Error msg  Issue Fix..
 *
 * Revision 1.2.2.4.4.2.4.2  2013/04/17 16:02:35  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.2.2.4.4.2.4.1  2013/03/05 14:46:44  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Lexjet production as part of Standard bundle
 *
 * Revision 1.2.2.4.4.2  2012/09/26 12:37:08  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.2.2.4.4.1  2012/09/21 15:01:30  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.2.2.4  2012/04/20 11:17:46  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.2.2.3  2012/03/16 14:08:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.2.2.2  2012/02/21 13:25:18  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.5  2012/02/21 11:50:56  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 */
function ReplenBatch(request, response){
	if (request.getMethod() == 'GET') {
		var getFetchedBeginLocation = request.getParameter('custparam_beginLocation');

		var reportNo = request.getParameter('custparam_repno');
		var beginLoc = request.getParameter('custparam_repbegloc');

		var sku = request.getParameter('custparam_repsku');
		var skuNo= request.getParameter('custparam_repskuno');
		var expQty = request.getParameter('custparam_repexpqty');
		var repInternalId = request.getParameter('custparam_repid');
		var clusterNo = request.getParameter('custparam_clustno');
		var whlocation = request.getParameter('custparam_whlocation');
		var actEndLocationId = request.getParameter('custparam_reppicklocno');
		var actEndLocation = request.getParameter('custparam_reppickloc');
		var beginLocId = request.getParameter('custparam_repbeglocId');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var fromlpno=request.getParameter('custparam_fromlpno');
		var tolpno=request.getParameter('custparam_tolpno');
		var nextlocation=request.getParameter('custparam_nextlocation');
		var nextqty=request.getParameter('custparam_nextqty');
		var nextitem=request.getParameter('custparam_nextitem');
		var nextitemno=request.getParameter('custparam_nextitemno');
		var taskpriority = request.getParameter('custparam_entertaskpriority');
		var getNumber = request.getParameter('custparam_number');
		var getItemdescription = request.getParameter('custparam_Itemdescription');
		
		var getreportNo=request.getParameter('custparam_enterrepno');
		var getitem = request.getParameter('custparam_entersku');
		var getitemid = request.getParameter('custparam_enterskuid');
		var getloc = request.getParameter('custparam_enterpickloc');
		var getlocid = request.getParameter('custparam_enterpicklocid');
		var replenType = request.getParameter('custparam_replentype');
		var fromloc=request.getParameter('custparam_enterfromloc');
		var batchNo=request.getParameter('custparam_repbatchno');
		nlapiLogExecution('ERROR', 'entereditem', getitem);
		nlapiLogExecution('ERROR', 'entereditemid', getitemid);
		nlapiLogExecution('ERROR', 'enteredloc', getloc);
		nlapiLogExecution('ERROR', 'enteredlocid', getlocid);		
		nlapiLogExecution('ERROR', 'taskpriority', taskpriority);
		nlapiLogExecution('ERROR', 'entered report no', getreportNo);
		nlapiLogExecution('ERROR', 'fromloc', fromloc);
		nlapiLogExecution('ERROR', ' report no', reportNo);
		nlapiLogExecution('ERROR', ' batchNo', batchNo);


		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4,st5;
		if( getLanguage == 'es_ES')
		{

			st0 = "";
			st1 = "LOTE # ";
			st2 = "ENTRAR /SCAN LOTE #";			
			st3 = "ENVIAR";
			st4 = "ANTERIOR";
			st5 = "ANULAR";

		}
		else
		{
			st0 = "";
			st1 = "LOT# ";
			st2 = "ENTER/SCAN LOT#";			
			st3 = "SEND";
			st4 = "PREV";
			st5 = "OVERRIDE";

		}		

		if(batchNo ==null || batchNo =='')
		{
			if(repInternalId !=null && repInternalId !='' && repInternalId !='null')
			{
				batchNo= getBatchNo(repInternalId);
			}
		}
		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + ": <label>" + batchNo + "</label>";
		html = html + "				<input type='hidden' name='hdnbatch' value=" + batchNo + ">";
		html = html + "				<input type='hidden' name='hdnNo' value=" + reportNo + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + beginLoc + ">";
		html = html + "				<input type='hidden' name='hdnSku' value=" + sku + ">";
		html = html + "				<input type='hidden' name='hdnExpQty' value=" + expQty + ">";
		html = html + "				<input type='hidden' name='hdnSkuNo' value=" + skuNo + ">";
		html = html + "				<input type='hidden' name='hdnid' value=" + repInternalId + ">";
		html = html + "				<input type='hidden' name='hdnclustno' value=" + clusterNo + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whlocation + ">";
		html = html + "				<input type='hidden' name='hdnendlocationid' value=" + actEndLocationId + ">";
		html = html + "				<input type='hidden' name='hdnendlocation' value=" + actEndLocation + ">";
		html = html + "				<input type='hidden' name='hdnbeginLocId' value=" + beginLocId + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnfromLpno' value=" + fromlpno + ">";
		html = html + "				<input type='hidden' name='hdntoLpno' value=" + tolpno + ">";
		html = html + "				<input type='hidden' name='hdnnextloc' value=" + nextlocation + ">";
		html = html + "				<input type='hidden' name='hdnnextqty' value=" + nextqty + ">";
		html = html + "				<input type='hidden' name='hdnnitem' value=" + nextitem + ">";
		html = html + "				<input type='hidden' name='hdnnextitemno' value=" + nextitemno + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdntaskpriority' value=" + taskpriority + ">";
		html = html + "				<input type='hidden' name='hdngetNumber' value=" + getNumber + ">";
		
		html = html + "				<input type='hidden' name='hdngetitem' value=" + getitem + ">";
		html = html + "				<input type='hidden' name='hdngetitemid' value=" + getitemid + ">";
		html = html + "				<input type='hidden' name='hdngetloc' value=" + getloc + ">";
		html = html + "				<input type='hidden' name='hdngetlocid' value=" + getlocid + ">";
		html = html + "				<input type='hidden' name='hdngetreportno' value=" + getreportNo + ">";
		html = html + "				<input type='hidden' name='hdnReplenType' value=" + replenType + ">";
		html = html + "				<input type='hidden' name='hdnenterfromloc' value=" + fromloc + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> " + st2;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterbatch' id='enterbatch' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st4 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "					" + st5 + " <input name='cmdOverride' type='submit' value='F11'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterbatch').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		// This variable is to hold the SO# entered.
		var Reparray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		Reparray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR', 'getLanguage', Reparray["custparam_language"]);


		var st6;
		if( getLanguage == 'es_ES')
		{

			st6 = "UBICACI&#211;N NO V&#193;LIDA";
		}
		else
		{

			st6 = "INVALID LOCATION";

		}

		Reparray["custparam_repno"] = request.getParameter('hdnNo');
		Reparray["custparam_repbegloc"] = request.getParameter('hdnBeginLocation');
		Reparray["custparam_repsku"] = request.getParameter('hdnSku');
		Reparray["custparam_repexpqty"] = request.getParameter('hdnExpQty');
		Reparray["custparam_repid"] = request.getParameter('hdnid');
		Reparray["custparam_repskuno"] = request.getParameter('hdnSkuNo');
		//case 20124289�Start
		Reparray["custparam_error"] = 'INVALID BATCH';
		Reparray["custparam_screenno"] = 'R4';
		//case 20124289�End
		Reparray["custparam_clustno"] = clusterNo;		
		Reparray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
		Reparray["custparam_reppicklocno"] = request.getParameter('hdnendlocationid');
		Reparray["custparam_reppickloc"] = request.getParameter('hdnendlocation');
		Reparray["custparam_repbeglocId"] = request.getParameter('hdnbeginLocId');
		Reparray["custparam_noofrecords"] = request.getParameter('hdnRecCount');
		Reparray["custparam_fromlpno"] = request.getParameter('hdnfromLpno');
		Reparray["custparam_tolpno"] = request.getParameter('hdntoLpno');
		Reparray["custparam_nextlocation"] = request.getParameter('hdnnextloc');
		Reparray["custparam_nextqty"] = request.getParameter('hdnnextqty');
		Reparray["custparam_nextitem"] = request.getParameter('hdnnitem');
		Reparray["custparam_nextitemno"] = request.getParameter('nextitemno');
		nlapiLogExecution('DEBUG', 'nextitem', 'Reparray["custparam_nextitem"]');
		Reparray["custparam_entertaskpriority"] = request.getParameter('hdntaskpriority');
		Reparray["custparam_number"] = request.getParameter('hdngetNumber');
		Reparray["custparam_enterskuid"] = request.getParameter('hdngetitemid');
		Reparray["custparam_entersku"] = request.getParameter('hdngetitem');
		Reparray["custparam_enterpicklocid"] = request.getParameter('hdngetlocid');
		Reparray["custparam_enterpickloc"] = request.getParameter('hdngetloc');
		Reparray["custparam_replentype"] = request.getParameter('hdnReplenType');
		Reparray["custparam_enterrepno"] = request.getParameter('hdngetreportno');
		Reparray["custparam_enterfromloc"] = request.getParameter('hdnenterfromloc');
		Reparray["custparam_repbatchno"] = request.getParameter('hdnbatch');
		var getitemid = request.getParameter('hdngetitemid');
		var getlocid = request.getParameter('hdngetlocid');
		var taskpriority = request.getParameter('hdntaskpriority');
		var taskpriority = request.getParameter('hdntaskpriority');
		var getreportNo = request.getParameter('hdngetreportno');
		var replenType = request.getParameter('hdnReplenType');
		var reportNo = request.getParameter('hdnNo');

		var getEnteredbatch=request.getParameter('enterbatch');
		var vBatchNo=request.getParameter('hdnbatch');		

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept SO #.
		if (optedEvent == 'F7') {
			//response.sendRedirect('SUITELET', 'customscript_rf_picking_container_no', 'customdeploy_rf_picking_container_no_di', false, SOarray);
			/*** The below code is merged from Lexjet production account on 04thMar13 by santosh as part of Standard bundle ***/
			response.sendRedirect('SUITELET', 'customscript_rf_replenishment_item', 'customdeploy_rf_replenishment_item_di', false, Reparray);
			/***upto here***/
		}
		else {
			//if (getEnteredLocation != '' && getEnteredLocation == getBeginBinLocation) {
			if (getEnteredbatch != '' && getEnteredbatch == vBatchNo) {
				//	updateScanedLoc(request.getParameter('hdnid'));
			//	var searchresults =getRplenTasks(request.getParameter('hdnNo'));
				var searchresults =getReplenTasks(reportNo,getitemid,getlocid,taskpriority,getreportNo,replenType);
				var getNumber=0;
				if(request.getParameter('hdngetNumber')!= null && request.getParameter('hdngetNumber') != "")
					getNumber = request.getParameter('hdngetNumber');
				else
					getNumber=0;
				nlapiLogExecution('ERROR', 'getNumber in post', getNumber);
				var recNo=0;
				if (searchresults != null && searchresults.length > 0) 
				{
					if(searchresults.length==getNumber)
					{
						getNumber=0;
					}
				}
				if(getNumber!=0)
					recNo=parseFloat(getNumber);
				nlapiLogExecution('ERROR', 'recNo in post', recNo);
				
				if (searchresults != null && searchresults.length > 0) {                	
					nlapiLogExecution('ERROR', 'Into searchresults');
//					Reparray["custparam_repbegloc"] = searchresults[0].getText('custrecord_actbeginloc');
//					Reparray["custparam_repsku"] = searchresults[0].getValue('custrecord_sku');
//					Reparray["custparam_repexpqty"] = searchresults[0].getValue('custrecord_expe_qty');
//					Reparray["custparam_repskuno"] = searchresults[0].getValue('custrecord_ebiz_sku_no');
//					Reparray["custparam_repid"] = searchresults[0].getId();
					Reparray["custparam_batchno"] = getEnteredbatch;

					nlapiLogExecution('ERROR', 'Reparray["custparam_repid"]',Reparray["custparam_repid"]);
					nlapiLogExecution('ERROR', 'searchresults[0].getId()',searchresults[recNo].getId());
					var PickFaceSearchResult=   getPickFaceLoc(searchresults[recNo].getValue('custrecord_sku'));

					if (PickFaceSearchResult != null && PickFaceSearchResult.length > 0) {
						Reparray["custparam_reppicklocno"] = PickFaceSearchResult[0].getValue('custrecord_pickbinloc');
						Reparray["custparam_reppickloc"] = PickFaceSearchResult[0].getText('custrecord_pickbinloc');                     
					}    
					/*** The below code is merged from Lexjet production account on 04thMar13 by santosh as part of Standard bundle ***/                 
					response.sendRedirect('SUITELET', 'customscript_replen_lp', 'customdeploy_rf_replen_lp_di', false, Reparray);
					// response.sendRedirect('SUITELET', 'customscriptrf_replen_lp', 'customdeploy_rf_replen_lp_di', false, Reparray);
					/*** upto here***/
				}
				else
				{

					// response.sendRedirect('SUITELET', 'customscript_rf_replen_conf', 'customdeploy_rf_replen_conf_di', false, Reparray);

					nlapiLogExecution('ERROR', 'getIntransitRplenTasks');
					//var searchresults = getIntransitRplenTasks(request.getParameter('hdnNo'));
					var searchresults = getIntransitRplenTasks(getreportNo);
					nlapiLogExecution('ERROR', 'After getIntransitRplenTasks');

					if (searchresults != null && searchresults.length > 0) {
						nlapiLogExecution('ERROR', 'In getIntransitRplenTasks');	
//						Reparray["custparam_repbegloc"] = searchresults[0].getText('custrecord_actbeginloc');
//						Reparray["custparam_repsku"] = searchresults[0].getValue('custrecord_sku');
//						Reparray["custparam_repexpqty"] = searchresults[0].getValue('custrecord_expe_qty');
//						Reparray["custparam_repskuno"] = searchresults[0].getValue('custrecord_ebiz_sku_no');
//						Reparray["custparam_repid"] = searchresults[0].getId();
						Reparray["custparam_batchno"] = getEnteredbatch;
						var PickFaceSearchResult=   getPickFaceLoc(searchresults[recNo].getValue('custrecord_sku'));

						if (PickFaceSearchResult != null && PickFaceSearchResult.length > 0) {
							Reparray["custparam_reppicklocno"] = PickFaceSearchResult[0].getValue('custrecord_pickbinloc');
							Reparray["custparam_reppickloc"] = PickFaceSearchResult[0].getText('custrecord_pickbinloc');                     
						}
						//	if the 'Send' button is clicked without any option value entered,
						//  it has to remain in the same screen ie., Receiving Menu.
						response.sendRedirect('SUITELET', 'customscript_rf_replen_conf', 'customdeploy_rf_replen_conf_di', false, Reparray);
					}
					else
					{
						nlapiLogExecution('ERROR', 'redirecting to error screen');
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);                		 
					}
				}		
			}
			else 
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
				nlapiLogExecution('DEBUG', 'Error: ', 'Did not scan the location');
			}
		}
	}
}

function getBatchNo(recId)
{	
	var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recId);
	var batchNo=transaction.getFieldValue('custrecord_batch_no');
	return batchNo;
}
function getReplenTasks(reportNo,getitemid,getlocid,taskpriority,getreportNo,replenType)
{
	nlapiLogExecution('ERROR', 'reportNo',reportNo);

	nlapiLogExecution('ERROR', 'getitemid in replen task',getitemid);
	nlapiLogExecution('ERROR', 'getlocid in replen task',getlocid);
	nlapiLogExecution('ERROR', 'taskpriority in replen task',taskpriority);
	nlapiLogExecution('ERROR', 'getreportNo in replen task',getreportNo);
	nlapiLogExecution('ERROR', 'replenType in replen task',replenType);

	/*var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'is', reportNo);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]);
	filters[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]);
	filters[3] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');
	filters[4] = new nlobjSearchFilter('custrecord_act_qty',null, 'isempty');
	 */
	var filters = new Array();

	/*if(reportNo!=null && reportNo!='')
	{
		nlapiLogExecution('ERROR','replenreportNo',reportNo);
		filters.push(new nlobjSearchFilter('name', null, 'is', reportNo));
		vOrdFormat='R'
	}*/
	//if(getItemId!=null && getItemId!='')

	if(getreportNo != null && getreportNo != "" && (replenType.indexOf('R') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','name',getreportNo);
		filters.push(new nlobjSearchFilter('name', null, 'is', getreportNo));

	}

	if(getitemid != null && getitemid != "" && (replenType.indexOf('I') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenitem',getitemid);
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', getitemid));

	}

	//if(getBinLocationId!=null && getBinLocationId!='')
	if(getlocid!=null && getlocid!=''&& (replenType.indexOf('L') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenprimaryloc',getlocid);
		filters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', getlocid));

	}

	//if(getPriority!=null && getPriority!='')
	if(taskpriority!=null && taskpriority!='' && (replenType.indexOf('P') != -1 || replenType=='ALL'))
	{
		nlapiLogExecution('ERROR','Replenpriority',taskpriority);
		filters.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'is', taskpriority));

	}
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]));
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
	filters.push(new nlobjSearchFilter('custrecord_act_qty',null, 'isempty'));



	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
	columns[2] = new nlobjSearchColumn('custrecord_sku');
	columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[5] = new nlobjSearchColumn('custrecord_wms_location');
	columns[6] = new nlobjSearchColumn('custrecord_actendloc');
	columns[7] = new nlobjSearchColumn('custrecord_lpno');
	columns[8] = new nlobjSearchColumn('custrecord_from_lp_no');
	columns[9] = new nlobjSearchColumn('name');	
	//columns[10] = new nlobjSearchColumn('custrecord_batch_no');
	columns[10] = new nlobjSearchColumn('custrecord_taskpriority');	
	//columns[11] = new nlobjSearchColumn('id');	

	columns[1].setSort(false);
	columns[2].setSort(false);

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(searchresults != null && searchresults != '')
		nlapiLogExecution('ERROR', 'searchresults.length ingetreplen',searchresults.length);
	return searchresults;
}
function getRplenTasks(reportNo)
{


	var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'is', reportNo);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]);
	filters[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20]);
	//filters[3] = new nlobjSearchFilter('custrecord_act_qty', null, 'isempty');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[1] = new nlobjSearchColumn('custrecord_sku');
	columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[4] = new nlobjSearchColumn('custrecord_wms_location');
	columns[5] = new nlobjSearchColumn('custrecord_actendloc');
	columns[6] = new nlobjSearchColumn('custrecord_lpno');
	columns[7] = new nlobjSearchColumn('custrecord_from_lp_no');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return searchresults;
}
function updateScanedLoc(recId)
{
	try {
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recId);
		transaction.setFieldValue('custrecord_wms_status_flag', 24);
		var result = nlapiSubmitRecord(transaction);
	} 
	catch (e) {
		nlapiLogExecution('ERROR', 'Updating OPEN TASK Record failed', e);
	}
}
function getPickFaceLoc(skuNo)
{
	var filter = new Array();
	filter[0] = new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', skuNo);
	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_pickbinloc');

	var searchresult = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, filter, column);
	return searchresult;
}
function getIntransitRplenTasks(reportNo)
{
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'is', reportNo);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]);
	filters[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [24]);
	filters[3] = new nlobjSearchFilter('custrecord_act_qty', null, 'isempty');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[1] = new nlobjSearchColumn('custrecord_sku');
	columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[4] = new nlobjSearchColumn('custrecord_wms_location');
	columns[5] = new nlobjSearchColumn('custrecord_actendloc');
	columns[6] = new nlobjSearchColumn('custrecord_lpno');
	columns[7] = new nlobjSearchColumn('custrecord_from_lp_no');
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return searchresults;    

}
