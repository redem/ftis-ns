/***************************************************************************
 eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Inbound/Suitelet/Attic/ebiz_RF_ExpectedBinSize.js,v $
 *     	   $Revision: 1.1.2.1 $
 *     	   $Date: 2014/09/26 15:36:37 $
 *     	   $Author: sponnaganti $
 *     	   $Name: t_eBN_2014_2_StdBundle_0_131 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *****************************************************************************/
function CheckInExpectedBin(request, response){
	if (request.getMethod() == 'GET') {
		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);
		 var userAccountId = request.getParameter('custparam_userAccountId');
			nlapiLogExecution('DEBUG', 'userAccountId', userAccountId);
		//	Get the PO# from the previous screen, which is passed as a parameter		
		var getPONo = request.getParameter('custparam_poid');
		var getPOItem = request.getParameter('custparam_poitem');
		var getPOLineNo = request.getParameter('custparam_lineno');
		var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		var getPOInternalId = request.getParameter('custparam_pointernalid');
		var getPOQtyEntered = request.getParameter('custparam_poqtyentered');
		var getPOItemRemainingQty = request.getParameter('custparam_poitemremainingqty');
		var getPOLinePackCode = request.getParameter('custparam_polinepackcode');
		var trantype= request.getParameter('custparam_trantype');
		nlapiLogExecution('DEBUG','trantype',trantype);
		var getPOLineItemStatusValue="";
		if(trantype=="purchaseorder")
			getPOLineItemStatusValue = request.getParameter('custparam_polineitemstatusValue');
		else
			getPOLineItemStatusValue = request.getParameter('custparam_polineitemstatus');
		nlapiLogExecution('DEBUG','getPOLineItemStatusValue',getPOLineItemStatusValue);
		var getCountryOfOrigin=request.getParameter('custparam_countryoforigin');
		var getCountryid=request.getParameter('custparam_countryid');
		
		var getItemCube = request.getParameter('custparam_itemcube');
		var getBaseUomQty = request.getParameter('custparam_baseuomqty');
		var getItemQuantity = request.getParameter('custparam_polinequantity');
		var getItemQuantityReceived = request.getParameter('custparam_polinequantityreceived');

		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');

		var getWHLocation = request.getParameter('custparam_whlocation');
		nlapiLogExecution('DEBUG','getWHLocation',getWHLocation);

		var pickfaceEnteredOption = request.getParameter('custparam_enteredOption');
		
		
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
	    
		var st0,st1,st2,st3,st4,st5,st6;
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "ESPERADO BIN SIZE 1";
			st2 = "ESPERADO BIN SIZE 2";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";
			
		}
		else
		{
			st0 = "";
			st1 = "EXPECTED BIN SIZE 1";
			st2 = "EXPECTED BIN SIZE 2";
			st3 = "SEND";
			st4 = "PREV";

		}
			
		
				
		var functionkeyHtml=getFunctionkeyScript('_rf_checkin_expectedbinsize'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";    
		
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
				
		html = html + "function stopRKey(evt) { ";
		
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL()'>";
		html = html + "	<form name='_rf_checkin_expectedbinsize' method='POST'>";
		html = html + "		<table>";
		
		html = html + "			<tr>";
		html = html + "			<tr><td align = 'left'>" + st1 +"<font color='red'>*</font></td></tr> ";
		html = html + "			<tr>";
		html = html + "			<td align = 'left'><input name='enterbinsize1' id='enterbinsize1' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr><td align = 'left'>" + st2 +"</td></tr> ";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterbinsize2' id='enterbinsize2' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				<input type='hidden' name='hdnItemPackCode' value=" + getPOLinePackCode + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getItemQuantity + ">";
		html = html + "				<input type='hidden' name='hdnQuantityReceived' value=" + getItemQuantityReceived + ">";
		html = html + "				<input type='hidden' name='hdnItemCube' value=" + getItemCube + ">";
		html = html + "				<input type='hidden' name='hdntrantype' value=" + trantype + ">";
		
		html = html + "				<input type='hidden' name='hdnitemname' value='" + getPOItem + "'>";
		
		html = html + "				<input type='hidden' name='hdnBaseUomQty' value=" + getBaseUomQty + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				<input type='hidden' name='hdnItemRemaininingQuantity' value=" + parseFloat(getPOItemRemainingQty) + ">";
		html = html + "				<input type='hidden' name='hdnPOQuantityEntered' value=" + parseFloat(getPOQtyEntered) + ">";
		html = html + "				<input type='hidden' name='hdnPOInternalId' value=" + getPOInternalId + ">";
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		html = html + "				<input type='hidden' name='hdnuserAccountId' value=" + userAccountId + ">";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + getWHLocation + ">";
		html = html + "				<input type='hidden' name='hdncountryoforigin' value=" + getCountryOfOrigin + ">";
		html = html + "				<input type='hidden' name='hdncountryid' value=" + getCountryid + ">";
		html = html + "				<input type='hidden' name='hdnPickfaceEnteredOption' value=" + pickfaceEnteredOption + ">";
		html = html + "				<input type='hidden' name='hdnItemStatusNo' value=" + getPOLineItemStatusValue + "></td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					" + st4 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
	
		html = html + "<script type='text/javascript'>document.getElementById('enterbinsize1').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {

		var getExpectedBinSize1 = request.getParameter('enterbinsize1');
		var getExpectedBinSize2 = request.getParameter('enterbinsize2');
		
		
		var getActualBeginTime = request.getParameter('hdnActualBeginTime');
		var getActualBeginTimeAMPM = request.getParameter('hdnActualBeginTimeAMPM');

		
		var POarray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);
		var userAccountId = request.getParameter('hdnuserAccountId');
		nlapiLogExecution('DEBUG', 'userAccountId', userAccountId);
		POarray["custparam_userAccountId"] = userAccountId;
		
		var st4,st5;
		if( getLanguage == 'es_ES')
		{
			st4 = "NO V&Aacute;LIDO BIN SIZE 1";
			st5 = "NO V&Aacute;LIDO BIN SIZE 2";
			st6 = "ENTER BIN SIZE 1";
		}
		else
		{
			
			st4 = "INVALID BIN SIZE 1";
			st5 = "INVALID BIN SIZE 2";
			st6 = "ENTER BIN SIZE 1";
		}
		

		POarray["custparam_trantype"] = request.getParameter('hdntrantype');
		if(POarray["custparam_trantype"]=="purchaseorder")
			POarray["custparam_polineitemstatusValue"]=request.getParameter('hdnItemStatusNo');
		else
			POarray["custparam_polineitemstatus"]=request.getParameter('hdnItemStatusNo');
		
		POarray["custparam_poid"] = request.getParameter('custparam_poid');
		POarray["custparam_StatusNo"] = request.getParameter('hdnItemStatusNo');
		
		POarray["custparam_poitem"] = request.getParameter('hdnitemname');
		
		nlapiLogExecution('DEBUG', 'Item name', POarray["custparam_poitem"]);
		
		POarray["custparam_lineno"] = request.getParameter('custparam_lineno');
		POarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
		POarray["custparam_pointernalid"] = request.getParameter('hdnPOInternalId'); //request.getParameter('custparam_pointernalid');
		POarray["custparam_poqtyentered"] = request.getParameter('hdnPOQuantityEntered'); //request.getParameter('custparam_poqtyentered');
		POarray["custparam_poitemremainingqty"] = request.getParameter('hdnItemRemaininingQuantity'); //request.getParameter('custparam_poitemremainingqty');
		POarray["custparam_polinepackcode"] = request.getParameter('hdnItemPackCode'); //request.getParameter('custparam_polinepackcode');
		POarray["custparam_polinequantity"] = request.getParameter('hdnQuantity'); //request.getParameter('custparam_polinequantity');
		POarray["custparam_polinequantityreceived"] = request.getParameter('hdnQuantityReceived'); //request.getParameter('custparam_polinequantityreceived');
		POarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
		POarray["custparam_enteredOption"] = request.getParameter('hdnPickfaceEnteredOption');
		POarray["custparam_countryoforigin"] = request.getParameter('hdncountryoforigin');
		POarray["custparam_countryid"] = request.getParameter('hdncountryid');
		POarray["custparam_itemcube"] = request.getParameter('custparam_itemcube');
		//code added on 13 feb 2012 by suman
		//To get the baseuom qty .
		POarray["custparam_baseuomqty"] = request.getParameter('hdnBaseUomQty');

		nlapiLogExecution('DEBUG','ITEMINFO[0]',POarray["custparam_itemcube"]);
		nlapiLogExecution('DEBUG','ITEMINFO[1]',POarray["custparam_baseuomqty"]);
		//end of code as of 13 feb 2012.

		nlapiLogExecution('DEBUG', 'custparam_poqtyentered', POarray["custparam_poqtyentered"]);

		POarray["custparam_error"] = st4;
		POarray["custparam_screenno"] = 'EXBINSIZE';

		POarray["custparam_actualbegindate"] = request.getParameter('hdnActualBeginDate');
	
		POarray["custparam_actualbegintime"] = getActualBeginTime; //TimeArray[0];
		POarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM; //TimeArray[1];
		
		POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
		nlapiLogExecution('DEBUG', 'WH Location', POarray["custparam_whlocation"]);

		var getOptedField = request.getParameter('hdnOptedField');
		POarray["custparam_option"] = getOptedField;

		nlapiLogExecution('DEBUG', 'optedField', POarray["custparam_option"]);

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent; //= request.getParameter('cmdPrevious');

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
//		if (optedEvent == 'F7') {		
		if (request.getParameter('cmdPrevious') == 'F7') {
			if(POarray["custparam_trantype"]=="purchaseorder")
			{
				nlapiLogExecution('ERROR', 'navigating to PO qty screen');
				response.sendRedirect('SUITELET', 'customscript_rf_checkin_qty', 'customdeploy_rf_checkin_qty_di', false, POarray);
			}
			else
			{
				nlapiLogExecution('ERROR', 'navigating to RMA qty screen');
				response.sendRedirect('SUITELET', 'customscript_rf_rma_checkinqty', 'customdeploy_rf_rmacheckin_qty_di', false, POarray);
			}
		}				
		else// if (request.getParameter('cmdSend') == 'ENT')
		{
			if (getExpectedBinSize1 == "")
			{
				//	if the 'Send' button is clicked without any option value entered,
				//  it has to show an error message. The next screen to which it has to navigate is to the error screen.
				POarray["custparam_error"] = st6;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'Entered Bin Size 1', 'No data');
			}		
			else if(ValidateSplCharacter(getExpectedBinSize1))
			{
				if(getExpectedBinSize2!=null && getExpectedBinSize2!="")
				{
					if(!ValidateSplCharacter(getExpectedBinSize2))
					{
						POarray["custparam_error"] = st5;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						return;
						nlapiLogExecution('DEBUG', 'Invalid Bin Size 2', '');
					}
				}
				POarray["custparam_expectedbinsize1"]=getExpectedBinSize1;
				POarray["custparam_expectedbinsize2"]=getExpectedBinSize2;
				nlapiLogExecution('DEBUG', 'getExpectedBinSize1', getExpectedBinSize1);
				nlapiLogExecution('DEBUG', 'getExpectedBinSize2', getExpectedBinSize2);
				if(POarray["custparam_trantype"]=="purchaseorder")
				{
					nlapiLogExecution('ERROR', 'navigating to PO item status');
					response.sendRedirect('SUITELET', 'customscript_rf_checkin_item_status', 'customdeploy_rf_checkin_item_status_di', false, POarray);
					return;
				}
				else
				{
					nlapiLogExecution('ERROR', 'navigating to RMA item status');
					response.sendRedirect('SUITELET', 'customscript_rf_rma_checkin_item_status', 'customdeploy_rf_rma_checkin_item_status', false, POarray);
					return;
				}				
			}
			else
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'Invalid Bin Size 1', '');
			}
		}
	}
}
