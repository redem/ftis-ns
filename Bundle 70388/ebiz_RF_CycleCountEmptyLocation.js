/***************************************************************************
����������������������eBizNET
����������������eBizNET SOLUTIONS LTD
 ****************************************************************************
 *
 *� $Source: /cvs/products/eBizNET4.5/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_CycleCountEmptyLocation.js,v $
 *� $Revision: 1.1.2.6 $
 *� $Date: 2014/06/13 10:13:24 $
 *� $Author: skavuri $
 *� $Name: t_NSWMS_2014_1_3_125 $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_RF_CycleCountEmptyLocation.js,v $
 *� Revision 1.1.2.6  2014/06/13 10:13:24  skavuri
 *� Case# 20148882 (added Focus Functionality for Textbox)
 *�
 *� Revision 1.1.2.5  2014/05/30 00:34:19  nneelam
 *� case#  20148622
 *� Stanadard Bundle Issue Fix.
 *�
 *� Revision 1.1.2.4  2014/01/06 13:16:03  grao
 *� Case# 20126579 related issue fixes in Sb issue fixes
 *�
 *� Revision 1.1.2.3  2013/08/02 15:36:32  rmukkera
 *� Case# 201214994
 *� add new item related changes
 *�
 *� Revision 1.1.2.2  2013/06/05 12:05:10  grao
 *� CASE201112/CR201113/LOG201121
 *� Standard bundle issues fixes
 *�
 *� Revision 1.1.2.1  2013/05/01 01:13:04  kavitha
 *� CASE201112/CR201113/LOG201121
 *� TSG Issue fixes
 *�
 *
 ****************************************************************************/
/**
 * @author LN
 * @version
 * @date
 */
function CycleCountEmptyLocation(request, response){
	
	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);
	
	if (request.getMethod() == 'GET') {

		nlapiLogExecution('ERROR', 'Into Request', 'Success');

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);

		var getPlanNo = request.getParameter('custparam_planno');
		nlapiLogExecution('ERROR', 'getPlanNo', getPlanNo);

		var getBeginLocationId = request.getParameter('custparam_begin_location_id');
		nlapiLogExecution('ERROR', 'getBeginLocationId', getBeginLocationId);
		var getBeginLocationName = request.getParameter('custparam_begin_location_name');

		var st0,st1,st2;

		if( getLanguage == 'es_ES')
		{
			st0 = "ARE YOU SURE THAT THIS LOCATION IS EMPTY";			
		}
		else
		{
			st0 = "ARE YOU SURE THAT THIS LOCATION IS EMPTY";		
			//Narasimha
			st1="YES";
			st2="NO";
			//end
		}

		var functionkeyHtml=getFunctionkeyScript('_rf_cyclecountemptylocation'); 
		var html = "<html><head><title>"+st0+"</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  

		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";

		//html = html + " document.getElementById('enterlocation').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cyclecountemptylocation' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		html = html + "				<tr><td><input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
		html = html + "				<tr><td><input type='hidden' name='hdnBeginLocationName' value=" + getBeginLocationName + ">";
		html = html + "						<input type='hidden' name='hdnCycleCountPlanNo' value=" + getPlanNo + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st0+"";
		html = html + "				</td>";      
		html = html + "			</tr>";    
		//Narasimha
		html = html + "				<td align = 'left'>"+ st1 +"<input name='cmdSend' id='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "		"+		st2 +	 "<input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		//end
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('cmdSend').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);

	}
	else 
	{		
		var optedEvent = request.getParameter('cmdPrevious');

		var getCycleCountPlanNo = request.getParameter('custparam_planno');
		nlapiLogExecution('ERROR', 'getCycleCountPlanNo', getCycleCountPlanNo);

		var getBeginLocationId = request.getParameter('hdnBeginLocationId');
		nlapiLogExecution('ERROR', 'getBeginLocationId', getBeginLocationId);
		var getBeginLocationName = request.getParameter('hdnBeginLocationName');

		var CCarray = new Array();
		CCarray["custparam_planno"] = request.getParameter('custparam_planno');

		if (sessionobj!=context.getUser()) {
			try
			{

				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}
				//Narasimha
				if(optedEvent=='F7')
					//end
				{
					response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_location', 'customdeploy_rf_cyclecount_location_di', false, CCarray);
				}
				else
				{
					var result=false;
					var getActualBeginDate = DateStamp();
					var getActualBeginTime = TimeStamp();
					var wmsflag=31;
					var currentContext = nlapiGetContext();
					var getPlanRecordedby = currentContext.getUser();		
					var getPlanRecordedate = DateStamp();
					var getPlanRecordedtime = TimeStamp();
					var getRecordId = ''; var qty = 0; var getActLP = ''; var getActualPackCode = ''; var getActualItem = ''; 
					var getItemstatus = ''; var getActualBatch = ''; var getExpQty = '';
					var serialno = '';

			var filters = new Array();
			filters[0] = new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto', getCycleCountPlanNo);
			filters[1] = new nlobjSearchFilter('custrecord_cycle_act_qty', null, 'isempty');
			filters[2] = new nlobjSearchFilter('custrecord_cycact_beg_loc', null, 'anyof', getBeginLocationId);

			var searchresults = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, null);
			if(searchresults != null && searchresults != '')
			{
				for(var p=0;p<searchresults.length;p++)
				{
					var CyccExeRec = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe', searchresults[p].getId());
					getRecordId = searchresults[p].getId();
					qty = "0";
					getActLP = CyccExeRec.getFieldValue('custrecord_cyclelpno');
					getActualPackCode = CyccExeRec.getFieldValue('custrecord_expcyclepc');
					getActualItem = CyccExeRec.getFieldValue('custrecord_cyclesku');
					getItemstatus = CyccExeRec.getFieldValue('custrecord_expcyclesku_status');
					getActualBatch = CyccExeRec.getFieldValue('custrecord_expcycleabatch_no');
					getExpQty = CyccExeRec.getFieldValue('custrecord_cycleexp_qty');

							result = CycleCountRecord(getRecordId, getCycleCountPlanNo, getActualBeginDate, getActualBeginTime, getBeginLocationId, getBeginLocationName, qty, getActLP, getActualPackCode, getActualItem,getItemstatus,getActualBatch,getExpQty,wmsflag,serialno,getPlanRecordedate,getPlanRecordedtime,getPlanRecordedby);
						}	
					}
					var filters = new Array();
					filters[0] = new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto', getCycleCountPlanNo);
					filters[1] = new nlobjSearchFilter('custrecord_cycle_act_qty', null, 'isempty');
					var searchresults = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, null);
					if(searchresults != null && searchresults != '' && searchresults.length>0)
					{
						response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_location', 'customdeploy_rf_cyclecount_location_di', false, CCarray);
					}
					else
					{
						response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_plan_no', 'customdeploy_rf_cyclecount_plan_no_di', false, CCarray);
					}
				}
			}
			catch (e)  {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');

			}
		}
		else
		{
			CCarray["custparam_screenno"] = '28';
			CCarray["custparam_error"] = 'LOCATION ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
		}
	}
}


function CycleCountRecord(RecordId, PlanNo, ActualBeginDate, ActualBeginTime, 
		BeginLocationId, BeginLocationName, ActualQty, ActLP, ActualPackCode, 
		ActualItem,ActualStatus,ActualBatch,expqty,wmsflag,serialno,getPlanRecordedate,getPlanRecordedtime,getPlanRecordedby)
{
	try 
	{
		nlapiLogExecution('ERROR', 'RecordId', RecordId);
		nlapiLogExecution('ERROR', 'expqty', expqty);
		var CCFields = new Array();
		var CCValues = new Array();

		CCFields[0] = 'custrecord_act_beg_date';
		CCValues[0] = ActualBeginDate;

		CCFields[1] = 'custrecord_cycact_beg_time';
		CCValues[1] = ActualBeginTime;

		CCFields[2] = 'custrecord_cycleact_end_date';
		CCValues[2] = DateStamp();

		CCFields[3] = 'custrecord_cycact_end_time';
		CCValues[3] = TimeStamp();

		CCFields[4] = 'custrecord_cyclerec_upd_date';
		CCValues[4] = DateStamp();

		CCFields[5] = 'custrecord_cyclerec_upd_time';
		CCValues[5] = TimeStamp();

		CCFields[6] = 'custrecord_cycleact_sku';
		CCValues[6] = ActualItem;

		CCFields[7] = 'custrecord_cycle_act_qty';
		CCValues[7] = ActualQty.toString();

		CCFields[8] = 'custrecord_cycact_lpno';
		CCValues[8] = ActLP;

		CCFields[9] = 'custrecord_cyclestatus_flag';
		CCValues[9] = 31;

		CCFields[10] = 'custrecord_cyclepc';
		CCValues[10] = ActualPackCode;

		CCFields[11] = 'custrecord_cyclesku_status';
		CCValues[11] = ActualStatus;

		CCFields[12] = 'custrecord_cycleabatch_no';
		CCValues[12] = ActualBatch;

		CCFields[13] = 'custrecord_cyclecount_act_ebiz_sku_no';
		CCValues[13] = ActualItem;

		CCFields[14] = 'custrecord_cycleexp_qty';
		CCValues[14] = expqty;

		CCFields[15] = 'custrecord_cyclestatus_flag';
		CCValues[15] = wmsflag;

		CCFields[16] = 'custrecord_cycle_serialno';
		CCValues[16] = serialno;

		CCFields[17] = 'custrecord_cyccplan_recorddate';
		CCValues[17] = getPlanRecordedate;

		CCFields[18] = 'custrecord_cyccplan_recordtime';
		CCValues[18] = getPlanRecordedtime;

		CCFields[19] = 'custrecord_cyccplan_recordedby';
		CCValues[19] = getPlanRecordedby;

		try 
		{
			nlapiSubmitField('customrecord_ebiznet_cyclecountexe', RecordId, CCFields, CCValues);
			nlapiLogExecution('ERROR', 'After submit', RecordId);
		} 
		catch (error) 
		{
			nlapiLogExecution('ERROR', 'Into Catch Blok', error);
		}

		nlapiLogExecution('ERROR', 'Confirmed', RecordId + '|' + PlanNo + '|' + ActualBeginDate + '|' + ActualBeginTime + 
				'|' + BeginLocationId + '|' + BeginLocationName + '|' + ActualQty + '|' + ActLP + '|' + ActualPackCode + 
				'|' + ActualItem+'|' + ActualBatch+'|' + ActualStatus + '|' + expqty);

		return true;
	} 
	catch (e) 
	{
		return false;
	}
}