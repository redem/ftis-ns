/***************************************************************************
������������������������������� � ����������������������������� ��eBizNET
�����������������������                           eBizNET Solutions Inc
 ****************************************************************************
 *
 *� $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_Cart_CheckinPO.js,v $
 *� $Revision: 1.1.2.9.4.4.2.9 $
 *� $Date: 2014/10/16 14:50:59 $
 *� $Author: skavuri $
 *� $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_RF_Cart_CheckinPO.js,v $
 *� Revision 1.1.2.9.4.4.2.9  2014/10/16 14:50:59  skavuri
 *� Case# 201410724 Std Bundle issue fixed
 *�
 *� Revision 1.1.2.9.4.4.2.8  2014/08/07 06:09:16  skreddy
 *� case # 20149831
 *� One Industries SB issue fix
 *�
 *� Revision 1.1.2.9.4.4.2.7  2014/06/20 15:07:13  skavuri
 *� Case # 20148882 SB Issue Fixed
 *�
 *� Revision 1.1.2.9.4.4.2.6  2014/06/13 08:18:56  skavuri
 *� Case# 20148882 (added Focus Functionality for Textbox)
 *�
 *� Revision 1.1.2.9.4.4.2.5  2014/06/13 08:12:10  skavuri
 *� Case# 20148882 (added Focus Functionality for Textbox)
 *�
 *� Revision 1.1.2.9.4.4.2.4  2014/05/30 00:26:47  nneelam
 *� case#  20148622
 *� Stanadard Bundle Issue Fix.
 *�
 *� Revision 1.1.2.9.4.4.2.3  2013/06/11 14:30:40  schepuri
 *� Error Code Change ERROR to DEBUG
 *�
 *� Revision 1.1.2.9.4.4.2.2  2013/04/17 15:53:40  skreddy
 *� CASE201112/CR201113/LOG201121
 *� issue fixes
 *�
 *� Revision 1.1.2.9.4.4.2.1  2013/04/15 15:00:29  grao
 *� CASE201112/CR201113/LOG201121
 *� Standard bundle issues fixes
 *�
 *� Revision 1.1.2.9.4.4  2013/02/12 02:20:49  kavitha
 *� CASE201112/CR201113/LOG201121
 *� LexJet - Location level restriction
 *�
 *� Revision 1.1.2.9.4.3  2012/09/27 13:13:26  grao
 *� CASE201112/CR201113/LOG201121
 *�
 *� Converting multiple language with given Spanish terms
 *�
 *� Revision 1.1.2.9.4.2  2012/09/27 10:53:53  grao
 *� CASE201112/CR201113/LOG201121
 *�
 *� Converting multiple language with given Spanish terms
 *�
 *� Revision 1.1.2.9.4.1  2012/09/21 14:57:16  grao
 *� CASE201112/CR201113/LOG201121
 *� Converting Multilanguage
 *�
 *� Revision 1.1.2.9  2012/07/20 15:27:10  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Issue related to Invalid searchColumn was resolved.
 *�
 *� Revision 1.1.2.8  2012/06/04 09:24:49  mbpragada
 *� CASE201112/CR201113/LOG201121
 *�
 *� Revision 1.1.2.7  2012/04/19 09:30:06  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *�
 *� RF Cart Checkin PO
 *�
 *� Revision 1.1.2.6  2012/03/29 06:32:54  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Cart Checkin issues for TPP
 *�
 *� Revision 1.5  2012/03/29 06:13:39  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Cart Checkin issues for TPP
 *�
 *� Revision 1.4  2012/03/21 07:19:39  spendyala
 *� CASE201112/CR201113/LOG201121
 *� meged code from branch.
 *�
 *� Revision 1.3  2012/03/20 16:16:38  gkalla
 *� CASE201112/CR201113/LOG201121
 *� changed the code to restrict pending approval and closed status POs
 *�
 *� Revision 1.2  2012/02/28 01:24:15  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� RF Checkin issue fixes
 *�
 *� Revision 1.1  2012/02/02 09:10:22  rmukkera
 *� CASE201112/CR201113/LOG201121
 *�   cartlp new file
 *�
 *
 ****************************************************************************/


function CartCheckInPO(request, response)
{
	if (request.getMethod() == 'GET') 
	{
		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);
		//customscript_rf_general_script  
		
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
	    
		var st0,st1,st2,st3;
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "INGRESAR / ESCANEAR ORDEN DE COMPRA";
			st2 = "ENVIAR";
			st3 = "ANTERIOR";

			
		}
		else
		{
			st0 = "";
			st1 = "ENTER/SCAN PO#";
			st2 = "SEND";
			st3 = "PREV";

		}
		
		
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript'>";
		html = html + "function OnKeyDown_CL() ";
		html = html + " { ";     
		html = html + "         if (";
		html = html + " event.keyCode == 112 || event.keyCode == 113 || event.keyCode == 114 || event.keyCode == 115 || event.keyCode == 116 || event.keyCode == 117 ||";
		html = html + " event.keyCode == 118 || event.keyCode == 119 || event.keyCode == 120 || event.keyCode == 121 || event.keyCode == 122) {";
		html = html + " var arrElements = document.getElementsByTagName('input');";
		html = html + " var keyFound = false;";
		html = html + " for (i = 0; i < arrElements.length; i++) {";
		html = html + " if (arrElements[i].type == 'submit') {";
		html = html + " switch (event.keyCode) {";
		html = html + " case 112:";
		html = html + " if (arrElements[i].value == 'F1')";	//F7 Key														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 113:";
		html = html + " if (arrElements[i].value == 'F2')";		//F8 Key													
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 114:";
		html = html + " if (arrElements[i].value == 'F3')";	//F9 Key														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 115:";
		html = html + " if (arrElements[i].value == 'F4')";		//F10 Key													
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 116:";
		html = html + " if (arrElements[i].value == 'F5')";		//F11 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 117:";
		html = html + " if (arrElements[i].value == 'F6')";		//F11 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 118:";
		html = html + " if (arrElements[i].value == 'F7')";		//F7 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 119:";
		html = html + " if (arrElements[i].value == 'F8')";		//F8 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 120:";
		html = html + " if (arrElements[i].value == 'F9')";		//F9 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 121:";
		html = html + " if (arrElements[i].value == 'F10')";		//F10 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " case 122:";
		html = html + " if (arrElements[i].value == 'F11')";		//F11 Key";														
		html = html + " keyFound = true;";
		html = html + " break;";
		html = html + " }";
		html = html + " if (keyFound == true) {";        
		html = html + " eval('document._rf_checkin_po.' + arrElements[i].name + '.click();');";
		html = html + " return false;";
		html = html + " }";
		html = html + " }";
		html = html + " }";
		html = html + " }    ";        
		html = html + "    return true; ";
		html = html + "    }";        
		html = html + " </SCRIPT>";

		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterpo').focus();";        
		html = html + "</script>";
		html = html + " </head>";
		html = html + "<body onkeydown='return OnKeyDown_CL()'>";
		html = html + "	<form name='_rf_checkin_po' method='POST'>"; //onkeydown='return OnKeyDown_CL()' >";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1;
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterpo' id='enterpo' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st3 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterpo').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Processing RF', 'Validating PO');

		var ActualBeginDate = DateStamp();
		var ActualBeginTime = TimeStamp();
		var TimeArray = new Array();
		TimeArray = ActualBeginTime.split(' ');

		// Determine if 'F7' is clicked, in order to navigate to the previous screen
		var optedEvent = request.getParameter('cmdPrevious');

		// This variable is to hold the PO# entered.
		var POarray = new Array();
		
		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);
    	
		
		var st4,st5;
		if( getLanguage == 'es_ES')
		{
			st4 = "NO V&#193;LIDO PO";
			st5 = "ESTADO INV&#193;LIDO PO";
			
		}
		else
		{
			st4 = "INVALID PO";
			st5 = "INVALID PO STATUS";
			
		}

		POarray["custparam_poid"] = request.getParameter('enterpo');
		POarray["custparam_error"] = st4;
		POarray["custparam_screenno"] = 'CRT1';
		POarray["custparam_option"] = request.getParameter('hdnOptenField');
		POarray["custparam_actualbegindate"] = ActualBeginDate;
		POarray["custparam_actualbegintime"] = TimeArray[0];
		POarray["custparam_actualbegintimeampm"] = TimeArray[1];

		var message = 'PO ID = ' + POarray["custparam_poid"] + '<br>';
		message = message + 'OptedField = ' + POarray["custparam_option"] + '<br>';
		message = message + 'ActualBeginDate = ' + POarray["custparam_actualbegindate"] + '<br>';
		message = message + 'ActualBeginTime = ' + POarray["custparam_actualbegintime"] + '<br>';
		message = message + 'ActualBeginTimeAMPM = ' + POarray["custparam_actualbegintimeampm"] + '<br>';
		message = message + 'OptedEvent = ' + optedEvent;

		nlapiLogExecution('DEBUG', 'RF - PO SCREEN', message);

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to RF Main Menu.
		if (optedEvent == 'F7') 
		{
			response.sendRedirect('SUITELET', 'customscript_rf_receiving_menu', 'customdeploy_rf_receiving_menu_di', false, POarray);
		}
		else 
		{
			var getPOid=request.getParameter('enterpo');
			nlapiLogExecution('DEBUG','getPOid',getPOid);
			var POtrantypefilters=new Array();
			POtrantypefilters.push(new nlobjSearchFilter('tranid',null,'is',getPOid));
			POtrantypefilters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
			
			var POtrantypecols=new Array();
			POtrantypecols[0]=new nlobjSearchColumn('internalid');
			
			var PORecinternalids=nlapiSearchRecord('purchaseorder',null,POtrantypefilters,POtrantypecols);
			var poid='';
			if(PORecinternalids!=null && PORecinternalids!='')
			{
				poid=PORecinternalids[0].getValue('internalid');
				//}
			
			var trantype = nlapiLookupField('transaction', poid, 'recordType');
			nlapiLogExecution('DEBUG','trantype',trantype);
			POarray["custparam_trantype"] = trantype;
			var POfilters=new Array();
			POfilters.push(new nlobjSearchFilter('tranid',null,'is',getPOid));
			POfilters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
			POfilters.push(new nlobjSearchFilter('recordtype',null,'is',trantype));
			var vRoleLocation=getRoledBasedLocation();
			if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
			{
				POfilters.push(new nlobjSearchFilter('location', null, 'anyof', vRoleLocation));
			}

			var POcols=new Array();
			POcols[0]=new nlobjSearchColumn('status');
			POcols[1]=new nlobjSearchColumn('location');
			if(trantype!='purchaseorder')
			POcols[2]= new nlobjSearchColumn('transferlocation');

			var PORec=nlapiSearchRecord('transaction',null,POfilters,POcols);
			if(PORec!=null&&PORec!='')
			{
				var poStatus=PORec[0].getValue('status');
				nlapiLogExecution('DEBUG','poStatus',poStatus);
				var poToLocationID=PORec[0].getValue('location');
				var poTransferLocationID=PORec[0].getValue('transferlocation');
				nlapiLogExecution('DEBUG','poToLocation',poToLocationID);
				var Tomwhsiteflag='F';
				var fields = ['custrecord_ebizwhsite'];
				if((trantype=='purchaseorder') && (poToLocationID!=null && poToLocationID !=''))
				var locationcolumns = nlapiLookupField('Location', poToLocationID, fields);
				else if(poTransferLocationID !=null && poTransferLocationID!='')
				var locationcolumns = nlapiLookupField('Location', poTransferLocationID, fields);
					
				if(locationcolumns !=null && locationcolumns !='')
				 Tomwhsiteflag = locationcolumns.custrecord_ebizwhsite;
				nlapiLogExecution('DEBUG','Tomwhsiteflag',Tomwhsiteflag);
				// Call method to retrieve the PO based on the PO ID

				if(poStatus=='pendingReceipt'||poStatus=='partiallyReceived' ||poStatus=='pendingBillPartReceived')
				{
					if(Tomwhsiteflag=='T')
					{
						var poSearchResults = eBiz_RF_GetPOListForTranID(POarray["custparam_poid"],trantype);

						if(poSearchResults != null && poSearchResults.length > 0){
							if (POarray["custparam_poid"] != null) 
							{
								var whLocation= poSearchResults[0].getValue('location');
								var whCompany= poSearchResults[0].getValue('custbody_nswms_company');
								POarray["custparam_whlocation"] = whLocation;
								POarray["custparam_company"] =whCompany;

								message = 'whLocation = ' + whLocation + '<br>';
								message = message + 'whCompany = ' + whCompany;
								nlapiLogExecution('DEBUG', 'PO Details', message);
								//Case# 201410724 starts 
								var filters = new Array();
								filters.push(new nlobjSearchFilter ('tranid', null, 'is', POarray["custparam_poid"]));
								filters.push(new nlobjSearchFilter ('mainline', null, 'is', 'F'));
								var columns = new Array();
								columns.push(new nlobjSearchColumn('custcol_nswmspackcode'));
								columns.push(new nlobjSearchColumn('location'));
								var poPackcodeResults = nlapiSearchRecord(trantype, null, filters, columns);
								if(poPackcodeResults !='' && poPackcodeResults!=null && poPackcodeResults !='null')
								{
									var packcode= poPackcodeResults[0].getValue('custcol_nswmspackcode');
								    var whlocline= poPackcodeResults[0].getValue('location');
								    if(whLocation!=whlocline)
								    	{
										nlapiLogExecution('DEBUG', 'Whloc details', whlocline);

										POarray["custparam_whlocation"] = whlocline;

								    	}

									POarray["custparam_packcode"] =packcode;
									nlapiLogExecution('DEBUG', 'POarray["custparam_packcode"]', POarray["custparam_packcode"]);
								}
								//Case# 201410724 ends
								// Redirecting the control to the next screen - SCAN Cart
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cartscan', 'customdeploy_ebiz_rf_cartscan_di', false, POarray);
							}
							else 
							{
								//	if the 'Send' button is clicked without any option value entered,
								//  it has to remain in the same screen ie., Receiving Menu.
								nlapiLogExecution('DEBUG', 'PONo', POarray["custparam_poid"]);
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							}
						}
						else 
						{
							nlapiLogExecution('DEBUG', 'PONo', POarray["custparam_poid"]);
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						}
					}
					else
					{
						nlapiLogExecution('DEBUG', 'PONo', POarray["custparam_poid"]);
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);			
					}
				}
				else
				{
					POarray["custparam_error"] = st5;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
				}
			}
				else
				{
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
				}
		}
			else
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
			}
		}
	}  //end of else loop.
}

