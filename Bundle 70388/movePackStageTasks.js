/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Common/Suitelet/Attic/movePackStageTasks.js,v $
 *     	   $Revision: 1.1.2.3 $
 *     	   $Date: 2013/04/30 23:30:47 $
 *     	   $Author: snimmakayala $
 *     	   $Name:  $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: movePackStageTasks.js,v $
 * Revision 1.1.2.3  2013/04/30 23:30:47  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.1.2.2  2013/03/21 14:18:14  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.1.2.1  2013/03/19 14:37:58  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Schedulee to move PACK and STAGE tasks..
 * 
 *
 *****************************************************************************/

function deleteoubinventory(type)
{
	nlapiLogExecution('ERROR','Into  deleteoubinventory','');
	var context = nlapiGetContext();
	nlapiLogExecution('ERROR','Remaining Usage 1',context.getRemainingUsage());
	if ( type != 'scheduled') return; 

	var filters2 = new Array();
	filters2.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', ['14'])); //SHIP
	filters2.push(new nlobjSearchFilter('custrecord_linenotes1', null, 'is','Manual Update by scheduler'));

	var columns2 = new Array();
	columns2[0] = new nlobjSearchColumn('custrecord_ship_qty');
	columns2[1] = new nlobjSearchColumn('custrecord_pickqty');
	columns2[2] = new nlobjSearchColumn('custrecord_ns_ord').setSort(false);
	columns2[3] = new nlobjSearchColumn('custrecord_linenotes1');

	var fulfillorderlist = nlapiSearchRecord('customrecord_ebiznet_ordline',null,filters2,columns2 );
	for (var k = 0; fulfillorderlist != null && k < fulfillorderlist .length; k++) 
	{
		if(context.getRemainingUsage()<100)
		{
			break;
		}

		var ebizcntrlno = fulfillorderlist[k].getId();

		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', ebizcntrlno));
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//PICK TASK
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8,28]));//PICK TASK
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_container_lp_no');	
		columns[1] = new nlobjSearchColumn('custrecord_wms_status_flag');	
		columns[2] = new nlobjSearchColumn('internalid').setSort(true);

		var salesOrderLineDetails = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
		for (var i = 0; salesOrderLineDetails != null && i < salesOrderLineDetails.length; i++) 
		{
			var vcontlp=salesOrderLineDetails[i].getValue('custrecord_container_lp_no'); 

			var invtfilters = new Array();
			invtfilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null,'is',vcontlp));
			invtfilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['18']));//OUTBOUND
			var invtcolumns= new Array();

			var inventorysearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv',null,invtfilters,invtcolumns);
			if(inventorysearchresults!=null)
			{
				var d=10;
				for (var l = 0; inventorysearchresults != null && l < inventorysearchresults.length; l++) 
				{
					var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', inventorysearchresults[l].getId());             
				}
			}

			nlapiSubmitField('customrecord_ebiznet_trn_opentask', salesOrderLineDetails[i].getId() , 
					'custrecord_wms_status_flag', '14');

			try
			{
				MoveTaskRecord(salesOrderLineDetails[i].getId());
			}
			catch(exp)
			{
				nlapiLogExecution('ERROR', 'Exception in  MoveTaskRecord');
			}

			nlapiSubmitField('customrecord_ebiznet_ordline', fulfillorderlist[k].getId() , 
					'custrecord_linenotes1', 'Manually Shipped by scheduler');

			nlapiLogExecution('ERROR','Remaining Usage 2',context.getRemainingUsage());
			if(context.getRemainingUsage()<100)
			{
				break;
			}
		}
	}
	nlapiLogExecution('ERROR','Remaining Usage at the end ',context.getRemainingUsage());	

	nlapiLogExecution('ERROR','Out of  deleteoubinventory','');
}

function movePACKandSTGMTasks()
{
	var context = nlapiGetContext();

	var packfilters = new Array();
	packfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['14']));
	packfilters.push(new nlobjSearchFilter('mainline', 'custrecord_ebiz_order_no', 'is', 'T'));
	//Billed, Pending Billing
	//packfilters.push(new nlobjSearchFilter('status', 'custrecord_ebiz_order_no', 'anyof', ['SalesOrd:G','SalesOrd:F']));
	
	
	packfilters.push(new nlobjSearchFilter('status', 'custrecord_ebiz_order_no', 'anyof', ['SalesOrd:G','SalesOrd:F','TrnfrOrd:G','TrnfrOrd:F','TrnfrOrd:H']));
	packfilters.push(new nlobjSearchFilter('isinactive',"custrecord_sku","is","F"));
	packfilters.push(new nlobjSearchFilter('isinactive',"custrecord_actbeginloc","is","F"));
	packfilters.push(new nlobjSearchFilter('isinactive',"custrecord_actendloc","is","F"));
	packfilters.push(new nlobjSearchFilter("custrecord_sku",null,"noneof",["@NONE@"]));
	packfilters.push(new nlobjSearchFilter('isinactive',"custrecord_upd_ebiz_user_no","is","F"));
    packfilters.push(new nlobjSearchFilter('isinactive',"custrecord_parent_sku_no","is","F"));

	var packcolumns= new Array();
	packcolumns[0] = new nlobjSearchColumn('internalid','custrecord_ebiz_order_no','group').setSort();
	packcolumns[1] = new nlobjSearchColumn('custrecord_container_lp_no',null,'group');

	var packtasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,packfilters ,packcolumns);
	if(packtasksearchresults !=null && packtasksearchresults !='')
	{
		for (var i = 0; i < packtasksearchresults.length; i++) 
		{
			if(context.getRemainingUsage()<100)
			{
				break;
			}

			var vebizordno= packtasksearchresults[i].getValue('internalid','custrecord_ebiz_order_no','group');
			var vcontlp = packtasksearchresults[i].getValue('custrecord_container_lp_no',null,'group');

			nlapiLogExecution('ERROR','vebizordno',vebizordno);
			nlapiLogExecution('ERROR','vcontlp',vcontlp);

			var otfilters = new Array();
			otfilters .push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['14','13','3']));
			otfilters .push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', vebizordno));
			otfilters .push(new nlobjSearchFilter('isinactive',"custrecord_upd_ebiz_user_no","is","F"));
			otfilters .push(new nlobjSearchFilter('isinactive',"custrecord_ebizuser","is","F"));
			otfilters .push(new nlobjSearchFilter('isinactive',"custrecord_parent_sku_no","is","F"));
			otfilters .push(new nlobjSearchFilter('isinactive',"custrecord_sku","is","F"));
			
			var otcolumns= new Array();
			otcolumns[0] = new nlobjSearchColumn('internalid');

			var opentasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,otfilters ,otcolumns);

			for (var k = 0; opentasksearchresults != null && k < opentasksearchresults.length; k++) 
			{						
				try
				{
					MoveTaskRecord(opentasksearchresults[k].getId());
				}
				catch(exp)
				{
					nlapiLogExecution('ERROR', 'Exception in  MoveTaskRecord',exp);
				}

				if(context.getRemainingUsage()<100)
				{
					break;
				}
			}

			if(context.getRemainingUsage()<100)
			{
				break;
			}

		}	
	}
}





