/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/ebiz_RF_scanlp.js,v $
 *     	   $Revision: 1.7.4.6.4.3.4.10 $
 *     	   $Date: 2015/01/06 06:30:25 $
 *     	   $Author: schepuri $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_scanlp.js,v $
 * Revision 1.7.4.6.4.3.4.10  2015/01/06 06:30:25  schepuri
 * issue#   201411242
 *
 * Revision 1.7.4.6.4.3.4.9  2014/11/06 15:43:37  schepuri
 * 201410439  issue fix
 *
 * Revision 1.7.4.6.4.3.4.8  2014/06/13 12:12:28  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.7.4.6.4.3.4.7  2014/05/30 00:41:05  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.7.4.6.4.3.4.6  2014/05/22 15:40:32  sponnaganti
 * case# 20148495
 * Stnd Bundle Issue fix
 *
 * Revision 1.7.4.6.4.3.4.5  2013/07/02 15:55:47  rmukkera
 * Case# 20123274
 * 1.During RF trailer load , if we load multiple SHIP LPS, after departing the trailer  only one order ( of  the last SHIP LP) status is changing from pending fulfillment to pending billing. Rest of the SHIP LPS related order status is in pending fulfillment only.- 20123274
 *
 * Revision 1.7.4.6.4.3.4.4  2013/06/11 14:30:20  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.7.4.6.4.3.4.3  2013/05/08 15:08:11  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.7.4.6.4.3.4.2  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.7.4.6.4.3.4.1  2013/04/03 01:26:12  kavitha
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.7.4.6.4.3  2012/12/24 13:20:25  schepuri
 * CASE201112/CR201113/LOG201121
 * checking for nulls
 *
 * Revision 1.7.4.6.4.2  2012/11/01 14:55:23  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.7.4.6.4.1  2012/09/25 07:13:53  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Language
 *
 * Revision 1.7.4.6  2012/06/21 10:15:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 * Revision 1.7.4.5  2012/04/06 14:03:27  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Performance Tunning
 *
 * Revision 1.7.4.4  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.7.4.3  2012/02/22 12:59:35  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.7.4.2  2012/02/10 10:48:24  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing  related to updating opentask
 *
 * Revision 1.7.4.1  2012/02/06 14:01:43  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing (updating wms status flag to shipped in closed task)
 *
 * Revision 1.7  2011/09/27 13:22:34  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.6  2011/09/27 09:42:23  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.5  2011/09/26 20:11:39  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.4  2011/09/26 10:45:06  mbpragada
 * CASE201112/CR201113/LOG201121
 * Load/Unload Trailer Issue Fixes
 *
 * 
 *****************************************************************************/

function ScanLPMenu(request, response)
{
	if (request.getMethod() == 'GET') 
	{	
		var trailerno = request.getParameter('custparam_trlrno');

		var getLanguage = request.getParameter('custparam_language');	
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4,st5;
		if( getLanguage == 'es_ES')
		{
			st0 = "TRAILER DE CARGA";
			st1 = "SCAN / ENTER LP:";
			st2 = "ELECCI&#211;N";
			st3 = "N-NEXT D-LP HECHO";
			st4 = "ENVIAR";
			st5 = "ANTERIOR";

		}
		else
		{
			st0 = "TRAILER LOADING";
			st1 = "SCAN/ENTER LP :";
			st2 = "CHOICE";
			st3 = "N-NEXT LP D-DONE ";
			st4 = "SEND";
			st5 = "PREV";


		}    	

		var functionkeyHtml=getFunctionkeyScript('_rfload'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";    
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('selectLPno').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rfload' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> " + st1;
		html = html + "				<input type='hidden' name='hdntrlrNo' value='" + trailerno + "'>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='selectLPno' id='selectLPno' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>  " + st2;
		html = html + "				</td>";
		html = html + "			</tr> ";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='selectchoice' type='text'/>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> " + st3;
		html = html + "				</td>";
		html = html + "			</tr> ";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st4 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st5 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('selectLPno').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else
	{
		nlapiLogExecution('DEBUG', 'Into Response','Into Response');

		var optedField = request.getParameter('selectLPno');
		var optedChoice = request.getParameter('selectchoice');
		var optedEvent = request.getParameter('cmdPrevious');
		var trailerName = request.getParameter('hdntrlrNo');
		var shiplp = request.getParameter('selectLPno');

		var lparray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		lparray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', lparray["custparam_language"]);    	

		var st9;
		if( getLanguage == 'es_ES')
		{			
			st9 = "NO V&#193;LIDO LP SHIP #";
		}
		else
		{			
			st9 = "INVALID SHIP LP#";
		}

		lparray["custparam_trlrno"]=trailerName;
		if(request.getParameter('custparam_shilp')==null || request.getParameter('custparam_shilp')=='')
		{
			lparray["custparam_shilp"]=shiplp;
		}
		else
		{
			var tempShiplp=request.getParameter('custparam_shilp');
			var newShipLp=tempShiplp+","+shiplp;
			lparray["custparam_shilp"]=newShipLp;
		}
		lparray["custparam_error"] = st9;
		lparray["custparam_screenno"] = '37';

		if (request.getParameter('cmdPrevious') == 'F7') {
			nlapiLogExecution('DEBUG', 'Clicked on Previous', request.getParameter('cmdPrevious'));
			response.sendRedirect('SUITELET', 'customscript_rf_loadtrlr', 'customdeploy_rf_loadtrlr_di', false, optedEvent);
		}
		else 
			if (optedChoice == 'N') {
				try
				{
					var recordId=getrecordinternalidforshipLP(shiplp);	
					if(recordId != null && recordId != '')
					{
						nlapiLogExecution('DEBUG', 'internal Id', recordId[0]);
						nlapiLogExecution('DEBUG', 'wms location', recordId[3]);
						if(recordId[3] != null && recordId[3] != "")
						{
							var Filters = new Array();
							Filters.push(new nlobjSearchFilter('custrecord_ebizsitetrailer', null, 'anyof', recordId[3])); //Ship task
							//Filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_wmsstatusflag', null, 'anyof', ['7','28'])); //Status 'B'//custrecord_ebizappointmenttrailer
							//Filters.push(new nlobjSearchFilter('custrecord_ebizappointmenttrailer', null, 'is', trailerName));
							Filters.push(new nlobjSearchFilter('name', null, 'is', trailerName));


							var SearchResults = nlapiSearchRecord('customrecord_ebiznet_trailer', null, Filters, new nlobjSearchColumn('name'));
							if(SearchResults != null && SearchResults != "")
							{

								var opentaskRecordId = updateOpentask(recordId[0], trailerName);				
								var shipLPInternalId = fillShipLP(shiplp);			
								nlapiLogExecution('DEBUG', 'shipLPInternalId', shipLPInternalId);
								if(shipLPInternalId != null && shipLPInternalId != "")
								{
									var lpRecordId = updateLPMaster(shipLPInternalId, trailerName);			
//									wavenumber=recordId[1];
									var closeTaskDetails = updateClosedTask(recordId[1], trailerName);				
									//updateFulfillment(closeTaskDetails);				
									//var itemFulfillmentResult = updateItemFulfillment(closeTaskDetails);	
									var length=recordId[2];
									nlapiLogExecution('DEBUG', 'length', length);
									if(length>1)
										response.sendRedirect('SUITELET', 'customscript_rf_scanlp', 'customdeploy_rf_scanlp_di', false, lparray);
									else{
										response.sendRedirect('SUITELET', 'customscript_rf_sealnumber', 'customdeploy_rf_sealnumber_di', false, lparray);
									}
								}
								else
								{
									nlapiLogExecution('DEBUG', 'InValid shiplp1', 'Fail');
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, lparray);
								}
							}
							else
							{
								nlapiLogExecution('DEBUG', 'InValid shiplp', 'Fail');
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, lparray);
							}
						}
						else
						{
							nlapiLogExecution('DEBUG', 'InValid shiplp3', 'Fail');
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, lparray);
						}
					}
					else
					{
						nlapiLogExecution('DEBUG', 'InValid shiplp2', 'Fail');
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, lparray);
					}
				}
				catch(exp)
				{
					nlapiLogExecution('ERROR','Exception in Get',exp);
				}
			}
			else if (optedChoice == 'D')
			{
				var recordId=getrecordinternalidforshipLP(shiplp);
				if(recordId != null && recordId != '')
				{
					if(recordId[3] != null && recordId[3] != "")
					{
						nlapiLogExecution('DEBUG', 'internal Id', recordId[0]);
						nlapiLogExecution('DEBUG', 'wms location', recordId[3]);

						var Filters = new Array();
						Filters.push(new nlobjSearchFilter('custrecord_ebizsitetrailer', null, 'anyof', recordId[3])); //Ship task
						//Filters.push(new nlobjSearchFilter('custrecord_ebizappointmenttrailer', null, 'is', trailerName)); //Ship task
						//Filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_wmsstatusflag', null, 'anyof', ['7','28'])); //Status 'B'
						Filters.push(new nlobjSearchFilter('name', null, 'is', trailerName)); //case# 201416249


						var SearchResults = nlapiSearchRecord('customrecord_ebiznet_trailer', null, Filters, new nlobjSearchColumn('name'));
						if(SearchResults != null && SearchResults != "")
						{
							var opentaskRecordId = updateOpentask(recordId[0], trailerName);
							var shipLPInternalId = fillShipLP(shiplp);
							nlapiLogExecution('DEBUG', 'shipLPInternalId', shipLPInternalId);
							if(shipLPInternalId != null && shipLPInternalId != "")
							{
								var lpRecordId = updateLPMaster(shipLPInternalId, trailerName);
								var closeTaskDetails = updateClosedTask(recordId[1], trailerName);			
								//updateFulfillment(closeTaskDetails);			
								//var itemFulfillmentResult = updateItemFulfillment(closeTaskDetails);

								response.sendRedirect('SUITELET', 'customscript_rf_sealnumber', 'customdeploy_rf_sealnumber_di', false, lparray);
							}
							else
							{
								nlapiLogExecution('DEBUG', 'InValid shiplp D1', 'Fail');
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, lparray);
							}
						}
						else
						{
							nlapiLogExecution('DEBUG', 'InValid shiplp D2', 'Fail');
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, lparray);
						}

					}
					else
					{
						nlapiLogExecution('DEBUG', 'InValid shiplp D3', 'Fail');
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, lparray);
					}

				}
				else
				{
					nlapiLogExecution('DEBUG', 'InValid shiplp D4', 'Fail');
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, lparray);
				}
			}
			else
			{
				nlapiLogExecution('DEBUG', 'INVALID CHOICE', 'Fail');
				lparray["custparam_error"] = 'INVALID CHOICE';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, lparray);
			}

		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}
}
/**
 * This function is to get the ship LP id for the ship lp selected from Open task.
 * @param shipLP
 * @returns
 */
function getrecordinternalidforshipLP(shipLP){
	nlapiLogExecution('DEBUG', 'Into getrecordinternalidforshipLP', shipLP);
	var shipLPId=new Array(); 
	var shipLPFilers = new Array(); 

	shipLPFilers.push(new nlobjSearchFilter('custrecord_ship_lp_no', null, 'is', shipLP));
	//shipLPFilers.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', orderNo));
	shipLPFilers.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['4']));

	var shipLPColumns = new Array();
	shipLPColumns[0] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
	shipLPColumns[1] = new nlobjSearchColumn('custrecord_wms_location');

	var shipLPSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, shipLPFilers, shipLPColumns);
	if(shipLPSearchResults  != null && shipLPSearchResults  != ""){
		for (var i = 0; i < shipLPSearchResults.length; i++) {
			shipLPId[0] = shipLPSearchResults[0].getId();
			shipLPId[1] = shipLPSearchResults[0].getValue('custrecord_ebiz_wave_no');
			shipLPId[2] = shipLPSearchResults.length;
			shipLPId[3] = shipLPSearchResults[0].getValue('custrecord_wms_location');//case no 201410439
			nlapiLogExecution('DEBUG', 'ID', shipLPId[0]);
			nlapiLogExecution('DEBUG', 'ebiz_wave_no', shipLPId[1]);
			nlapiLogExecution('DEBUG', 'wms loc', shipLPId[3]);
		}
	}
	nlapiLogExecution('DEBUG', 'out of getrecordinternalidforshipLP', shipLPId);
	return shipLPId;
}
/**
 * This function is to update the SHIP task in open task record 
 * 	with the trailer number and the status to Loaded.
 * @param recordId
 */
function updateOpentask(recordId, trailerName){
	nlapiLogExecution('DEBUG', 'Into  updateOpentask',recordId);
	//var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recordId);	
//	transaction.setFieldValue('custrecord_wms_status_flag', "14");//modified status from 10 to 14
//	transaction.setFieldValue('custrecord_act_end_date', DateStamp());
//	transaction.setFieldValue('custrecord_ebiz_trailer_no', trailerName);
//	transaction.setFieldValue('custrecord_upd_date', DateStamp());
//	transaction.setFieldValue('custrecord_upd_ebiz_user_no', nlapiGetContext().getUser());
//	var opentaskRecordId = nlapiSubmitRecord(transaction, true);

	//set the status flag as STATUS.OUTBOUND.TRAILER_LOADED to shipped

	var opentaskRecordId =-1;

	var fieldNames = new Array(); 
	fieldNames.push('custrecord_wms_status_flag');  	
	fieldNames.push('custrecord_act_end_date'); 
	fieldNames.push('custrecord_ebiz_trailer_no'); 
	fieldNames.push('custrecord_upd_date'); 
	fieldNames.push('custrecord_upd_ebiz_user_no'); 

	var newValues = new Array(); 
	//newValues.push(14);
	newValues.push(10);
	newValues.push(DateStamp());	
	newValues.push(trailerName);
	newValues.push(DateStamp());
	newValues.push(parseInt(nlapiGetContext().getUser()));

	nlapiSubmitField('customrecord_ebiznet_trn_opentask', recordId, fieldNames, newValues);

	nlapiLogExecution('DEBUG', 'Opentask Succesfuly updated');
	return opentaskRecordId;
}
/**
 * This function is to get the ship LP id for the ship lp selected.
 * @param shipLP
 * @returns
 */
function fillShipLP(shipLP){
	nlapiLogExecution('DEBUG', 'shipLP',shipLP);
	var shipLPId;
	var shipLPFilers = new Array();
	// Add filter criteria for LP type;
	// Search for 'SHIP' ;
	shipLPFilers.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lptype', null, 'is', '3'));
	// Add filter criteria for WMS Status Flag;
	// Search for 'PACK_COMPLETE' ;
	shipLPFilers.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_wmsstatusflag', null, 'anyof', ['7','28']));
	shipLPFilers.push(new nlobjSearchFilter('name', null, 'is', shipLP));

	var shipLPColumns = new Array();
	shipLPColumns[0] = new nlobjSearchColumn('name');

	var shipLPSearchResults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, shipLPFilers, shipLPColumns);
	if(shipLPSearchResults  != null){
		for (var i = 0; i < shipLPSearchResults.length; i++) {
			shipLPId = shipLPSearchResults[0].getId();
		}
	}
	nlapiLogExecution('DEBUG', 'shipLPSearchResults',shipLPSearchResults);
	nlapiLogExecution('DEBUG', 'fillShipLP','Sucess');
	nlapiLogExecution('DEBUG', 'shipLPId',shipLPId);
	return shipLPId;
}
/**
 * This function is to update the SHIP task in LP Master record 
 * 	with the trailer number and the status to Loaded.
 * @param picktaskid
 */
function updateLPMaster(shipLPId, trailerName){
	var lpRecordId = -1;

	//set the status flag as STATUS.OUTBOUND.TRAILER_LOADED

	//	var transaction = nlapiLoadRecord('customrecord_ebiznet_master_lp', shipLPId);	
	//	transaction.setFieldValue('custrecord_ebiz_lpmaster_wmsstatusflag', "10");
	//	transaction.setFieldValue('custrecord_ebiz_lpmaster_masterlp', trailerName);
	//	lpRecordId = nlapiSubmitRecord(transaction, true);

	var fieldNames = new Array(); 
	fieldNames.push('custrecord_ebiz_lpmaster_wmsstatusflag');  	
	fieldNames.push('custrecord_ebiz_lpmaster_masterlp'); 

	var newValues = new Array(); 
	newValues.push('10');		
	newValues.push(trailerName);		

	nlapiSubmitField('customrecord_ebiznet_master_lp', shipLPId, fieldNames, newValues);

	nlapiLogExecution('DEBUG', 'LP Master Succesfuly updated','Sucess');

	return lpRecordId;
}
/**
 * This function is used to update the trailer number in closed task record.
 * @param waveNumber
 * @param trailerName
 */
function updateClosedTask(waveNumber,trailerName)
{
	nlapiLogExecution('DEBUG', "updateClosedTask:waveNumber", waveNumber);	
	nlapiLogExecution('DEBUG', "updateClosedTask:trailerName", trailerName);
	var closedTaskId;
	var closedTaskFilers = new Array();
	var closeTaskDetails = new Array();
	// Add filter criteria for Task type ;
	// Search for 'PICK';
	closedTaskFilers.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));
	closedTaskFilers.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', waveNumber));	 
	closedTaskFilers.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['7']));//Shipping Pallet Build Completed
	var closedTaskColumns = new Array();
	closedTaskColumns[0] = new nlobjSearchColumn('name');

	var closedTaskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, closedTaskFilers, closedTaskColumns);
	if(closedTaskSearchResults!=null && closedTaskSearchResults!="")
	{nlapiLogExecution('DEBUG', "openTaskSearchResults.length", closedTaskSearchResults.length);
	for (var i = 0; i < closedTaskSearchResults.length; i++) {
		closedTaskId = closedTaskSearchResults[i].getId();

		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', closedTaskId);
		var quantityPicked = transaction.getFieldValue('custrecord_act_qty');
		var fulfillmentOrderInternalId = transaction.getFieldValue('custrecord_ebiz_cntrl_no');
		var fulfillmentOrder = transaction.getFieldValue('name');
		var waveNumber = transaction.getFieldValue('custrecord_ebiz_wave_no');
		var itemId = transaction.getFieldValue('custrecord_sku');
		var closeTaskRecordId = closedTaskSearchResults[i].getId();
		var itemFulfillmentInternalId = transaction.getFieldValue('custrecord_ebiz_nsconfirm_ref_no');//3369		
		transaction.setFieldValue('custrecord_ebiz_trailer_no', trailerName);
		transaction.setFieldValue('custrecord_wms_status_flag', 14);//status.outbound.shipped
		nlapiSubmitRecord(transaction, true);


		nlapiLogExecution('DEBUG', "itemFulfillmentInternalId", itemFulfillmentInternalId);

		var currentRow = [i, closeTaskRecordId, quantityPicked, fulfillmentOrder, 
		                  fulfillmentOrderInternalId, itemId, waveNumber, itemFulfillmentInternalId];

		closeTaskDetails.push(currentRow);
		nlapiLogExecution('DEBUG', "closeTaskDetails", "success");
	}	
	}
	return closeTaskDetails;
}
/**
 * This function is used to update the status flag to SHIPPED and updating ship quantity in Fulfillment 
 * 	Order Line.
 * The close task details array consists of 
 * 	0 = i, 1 = closeTaskRecordId, 2 = quantityPicked, 3 = fulfillmentOrder,	4 = fulfillmentOrderInternalId, 
 * 	5= itemId, 6 = waveNumber, 7 = item fulfillment Id
 * @param fulfillmetIds
 * @param trailerName
 */
function updateFulfillment(closeTaskDetails){
	for (var i = 0; i < closeTaskDetails.length; i++) {
		var fulfillmentOrderInternalId = closeTaskDetails[i][4];
		var shipQuantity = closeTaskDetails[i][2];
		var itemFulfillmentResult = "";
		var itemFulfillmentId = closeTaskDetails[i][7];


		if(fulfillmentOrderInternalId!=null && fulfillmentOrderInternalId!="")
		{
//			var transaction = nlapiLoadRecord('customrecord_ebiznet_ordline', fulfillmentOrderInternalId);			
//			transaction.setFieldValue('custrecord_linestatus_flag', 14);
//			transaction.setFieldValue('custrecord_ship_qty', shipQuantity);
//			nlapiSubmitRecord(transaction, true);

			//set the status flag as STATUS.OUTBOUND.SHIPPED
			var fieldNames = new Array(); 
			fieldNames.push('custrecord_linestatus_flag');  	
			fieldNames.push('custrecord_ship_qty'); 

			var newValues = new Array(); 
			newValues.push('14');		
			newValues.push(parseFloat(shipQuantity).toFixed(4));		

			nlapiSubmitField('customrecord_ebiznet_ordline', fulfillmentOrderInternalId, fieldNames, newValues);

			nlapiLogExecution('DEBUG', "updated Fulfillment order line ", "success");


		}	
	}

}
/**
 * This function is used to send confirmation to netsuite.
 *  * The close task details array consists of 
 * 	0 = i, 1 = closeTaskRecordId, 2 = quantityPicked, 3 = fulfillmentOrder,	4 = fulfillmentOrderInternalId, 
 * 	5= itemId, 6 = waveNumber, 7 = item fulfillment Id
 * @param closeTaskDetails
 */
function updateItemFulfillment(closeTaskDetails){
	nlapiLogExecution('DEBUG', "closeTaskDetails ", closeTaskDetails);
	for (var i = 0; i < closeTaskDetails.length; i++) {
		var itemFulfillmentId = closeTaskDetails[i][7];
		nlapiLogExecution('DEBUG', "itemFulfillmentId ", itemFulfillmentId);
		var shipQuantity = closeTaskDetails[i][2];
		if(itemFulfillmentId!=null && itemFulfillmentId!="")
		{
//			var transaction = nlapiLoadRecord('itemfulfillment', itemFulfillmentId);
//			transaction.setFieldValue('shipstatus', 'C');
//			transaction.setFieldValue('quantity', shipQuantity);
//			var itemFulfillmentResult = nlapiSubmitRecord(transaction, true);

			var fieldNames = new Array(); 
			fieldNames.push('shipstatus');  	
			fieldNames.push('quantity'); 

			var newValues = new Array(); 
			newValues.push('C');		
			newValues.push(shipQuantity);		

			nlapiSubmitField('itemfulfillment', itemFulfillmentId, fieldNames, newValues);

			nlapiLogExecution('DEBUG', "updated NS Item fulfillment ", "success");
		}

	}
}

