/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_CreateInventoryBatchNo.js,v $
 *     	   $Revision: 1.1.2.6.4.4.2.6 $
 *     	   $Date: 2014/06/23 07:48:45 $
 *     	   $Author: skavuri $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 * 
 * REVISION HISTORY
 * $Log: ebiz_RF_CreateInventoryBatchNo.js,v $
 * Revision 1.1.2.6.4.4.2.6  2014/06/23 07:48:45  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox) Issue Fixed.
 *
 * Revision 1.1.2.6.4.4.2.5  2014/06/06 08:21:34  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.1.2.6.4.4.2.4  2014/05/30 00:34:18  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.6.4.4.2.3  2013/08/21 14:22:59  skreddy
 * Case# 20123285
 *  restict space charater in lot# feild
 *
 * Revision 1.1.2.6.4.4.2.2  2013/04/17 16:02:35  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.6.4.4.2.1  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.1.2.6.4.4  2013/02/07 08:48:19  skreddy
 * CASE201112/CR201113/LOG201121
 *  RF Lot auto generating FIFO enhancement
 *
 * Revision 1.1.2.6.4.3  2012/10/05 06:34:12  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting Multiple language into given suggestions
 *
 * Revision 1.1.2.6.4.2  2012/09/26 12:32:39  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.1.2.6.4.1  2012/09/21 15:01:30  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.1.2.6  2012/05/17 13:10:06  spendyala
 * CASE201112/CR201113/LOG201121
 * Driving user to Batch creation screen when user enter new Lot#
 *
 * Revision 1.1.2.5  2012/04/20 11:09:11  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.1.2.4  2012/03/16 14:08:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.1.2.3  2012/02/21 13:21:37  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.1.2.2  2012/02/14 07:43:58  schepuri
 * CASE201112/CR201113/LOG201121
 * CVS Keyword $Log is added
 *
 *****************************************************************************/
function CreateInventoryBatchNo(request, response)
{
	var getActualBeginDate = request.getParameter('custparam_actualbegindate');
	var getActualBeginTime = request.getParameter('custparam_actualbegintime');
	var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
	var getLocationId = request.getParameter('custparam_locationId');
	var getBinLocationId = request.getParameter('custparam_binlocationid');
	var getBinLocationName = request.getParameter('custparam_binlocationname');    
	var getItemId = request.getParameter('custparam_itemid');
	var getItem = request.getParameter('custparam_item');
	var getItem=getItem.replace(/ /g,"-");
	nlapiLogExecution('ERROR', 'after Item1 ', getItem);


	var getItemType = request.getParameter('custparam_ItemType');
	var getQuantity = request.getParameter('custparam_quantity');

	//var getLOTId = request.getParameter('custparam_lotid');
	//var getLOTNo = request.getParameter('custparam_lot');  

	nlapiLogExecution('ERROR', 'locationId ', request.getParameter('custparam_locationId'));
	nlapiLogExecution('ERROR', 'getItemType ', getItemType);
	nlapiLogExecution('ERROR', 'getItem ',getItem);

	if (request.getMethod() == 'GET') 
	{	

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);

		var st0,st1,st2,st3;
		if( getLanguage == 'es_ES')
		{

			st0 = "CREAR INVENTARIO";
			st1 = "INGRESAR LOTE :";
			st2 = "ENVIAR";
			st3 = "ANTERIOR";

		}
		else
		{
			st0 = "CREATE INVENTORY";
			st1 = "ENTER LOT# :";
			st2 = "SEND";
			st3 = "PREV";

		}



		var functionkeyHtml=getFunctionkeyScript('_rf_createinventory'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends      
		//html = html + " document.getElementById('enterbatchno').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_createinventory' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		/*html = html + "			<tr>";
		html = html + "				<td align = 'left'>BATCH # :<label>" + getLOTNo + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";*/
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterbatchno' id='enterbatchno' type='text'/>";
		html = html + "					<input type='hidden' name='hdnBinLocationId' value=" + getBinLocationId + ">";
		html = html + "					<input type='hidden' name='hdnBinLocation' value=" + getBinLocationName + ">";
		html = html + "					<input type='hidden' name='hdnLocationId' value=" + getLocationId + ">";
		html = html + "					<input type='hidden' name='hdnItemId' value=" + getItemId + ">";
		html = html + "					<input type='hidden' name='hdnItem' value=" + getItem+ ">";
		//html = html + "					<input type='hidden' name='hdnLOTNo' value=" + getLOTNo + ">";
		//	html = html + "					<input type='hidden' name='hdnLOTNoId' value=" + getLOTId + ">";
		html = html + "					<input type='hidden' name='hdnQty' value=" + getQuantity + ">";
		html = html + "					<input type='hidden' name='hdnItemType' value=" + getItemType + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				    <input type='hidden' name='hdnItemdesc' value='" + getItem + "'>";
		html = html + "			    	<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 +" <input name='cmdSearch' type='submit' value='F8' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "				" + st3 +" <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterbatchno').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		var getBatchNo = request.getParameter('enterbatchno');
		nlapiLogExecution('ERROR', 'New Batch No', getBatchNo); 
		var Itemtype= request.getParameter('hdnitemType');

		var Item= request.getParameter('hdnItem');
		var TempItem =  new Array();
		if(Item.length > 0)
		{
			var TempItem = Item.split("-");
			Item = TempItem[0] +" "+ TempItem[1];
			nlapiLogExecution('ERROR', 'Item tst ', Item);
		}


		var ItemId= request.getParameter('hdnItemId');
		var locationId = request.getParameter('hdnLocationId');
		nlapiLogExecution('ERROR', 'locationId tst ', locationId);
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');

		var CIarray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		CIarray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR', 'getLanguage', CIarray["custparam_language"]);


		var st4;
		if( getLanguage == 'es_ES')
		{
			st4 = "ENTER LOTE #";
		}
		else
		{
			st4 = "ENTER LOT#";
		}


		CIarray["custparam_binlocationid"] = request.getParameter('hdnBinLocationId');
		CIarray["custparam_binlocationname"] = request.getParameter('hdnBinLocation');
		CIarray["custparam_itemid"] = request.getParameter('hdnItemId');
		CIarray["custparam_item"] = request.getParameter('hdnItem');
		CIarray["custparam_item"] = Item;
		CIarray["custparam_quantity"] = request.getParameter('hdnQty');
		CIarray["custparam_itemtype"]=Itemtype;
		CIarray["custparam_actualbegindate"] = getActualBeginDate;
		CIarray["custparam_locationId"] = locationId;
		CIarray["custparam_actualbegintime"] = getActualBeginTime;
		CIarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM;
		CIarray["custparam_screenno"] = '15A';
		CIarray["custparam_BatchNo"] = getBatchNo;
		//IMarray["custparam_lot"] = request.getParameter('hdnLOTNo');
		//IMarray["custparam_lotid"] = request.getParameter('hdnLOTNoId');
		//IMarray["custparam_screenno"] = '20';

		nlapiLogExecution('ERROR', 'CIarray["custparam_locationId"] ', CIarray["custparam_locationId"]);
		nlapiLogExecution('ERROR', 'Itemtype', CIarray["custparam_itemtype"]);

		var optedEvent = request.getParameter('cmdPrevious');
		var batchid;
		if (optedEvent == 'F7') 
		{
			response.sendRedirect('SUITELET', 'customscript_rf_create_invt_binloc', 'customdeploy_rf_create_invt_binloc_di', false, CIarray);
		}
		else 
		{
			try 
			{            	

				if (getBatchNo != null && getBatchNo != '') 
				{
					try {
						nlapiLogExecution('ERROR', 'in');
						//case 20123285 start
						var result=ValidateSplCharacter(getBatchNo,'Lot #');
						nlapiLogExecution('ERROR','result',result);
						if(result == false)
						{
							CIarray["custparam_error"] = 'SPECIAL CHARS NOT ALLOWED IN LOT#';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
						}//end
						else					
						{
							var batchid = IsValidBatch(getBatchNo,ItemId);
							nlapiLogExecution('ERROR', 'batchid results', batchid);
							if(batchid == "" || batchid == null)
							{
								nlapiLogExecution('ERROR', 'if');
								if(ItemId!=null && ItemId!= '')
								{
									var poItemFields = ['custitem_ebiz_item_shelf_life','custitem_ebiz_item_cap_expiry_date','custitem_ebiz_item_cap_fifo_date'];
									var rec = nlapiLookupField('item', ItemId, poItemFields);
									var itemShelflife = rec.custitem_ebiz_item_shelf_life;
									var CaptureExpirydate= rec.custitem_ebiz_item_cap_expiry_date;
									var CaptureFifodate= rec.custitem_ebiz_item_cap_fifo_date;
									nlapiLogExecution('ERROR','CaptureExpirydate',CaptureExpirydate);
									nlapiLogExecution('ERROR','CaptureExpirydate',CaptureFifodate);
									nlapiLogExecution('ERROR','itemShelflife',itemShelflife);
									CIarray["custparam_batchno"] = getBatchNo;
									CIarray["custparam_shelflife"] = itemShelflife;
									CIarray["custparam_captureexpirydate"] = CaptureExpirydate;
									CIarray["custparam_capturefifodate"] = CaptureFifodate;

									var d = new Date();
									var vExpiryDate='';
									if(itemShelflife !=null && itemShelflife!='')
									{

										d.setDate(d.getDate()+parseInt(itemShelflife));
										vExpiryDate=((d.getMonth()+1)+"/"+(d.getDate())+"/"+(d.getFullYear()));
									}
									else
									{
										vExpiryDate='01/01/2099';										     
									}
									nlapiLogExecution('ERROR', 'vExpiryDate',vExpiryDate);

									if(CaptureExpirydate =='T' ||(CaptureExpirydate =='T' && CaptureFifodate =='T'))
									{				
										response.sendRedirect('SUITELET', 'customscript_rf_create_invt_expdate', 'customdeploy_rf_create_invt_expdate_di', false, CIarray);
									}
									else
										if(CaptureExpirydate !='T' && CaptureFifodate=='T' )
										{
											CIarray["custparam_fifodate"]='';
											CIarray["custparam_mfgdate"]='';
											CIarray["custparam_bestbeforedate"]='';
											CIarray["custparam_lastdate"]='';
											CIarray["custparam_fifocode"]='';																			
											CIarray["custparam_expdate"]= vExpiryDate;

											response.sendRedirect('SUITELET', 'customscript_rf_create_invt_fifodate', 'customdeploy_rf_create_invt_fifodate_di', false, CIarray);

										}
										else
											if(CaptureExpirydate =='F' && CaptureFifodate =='F' )
											{

												CIarray["custparam_fifodate"]=DateStamp();
												CIarray["custparam_mfgdate"]='';
												CIarray["custparam_bestbeforedate"]='';
												CIarray["custparam_lastdate"]='';
												CIarray["custparam_fifocode"]='';																					
												CIarray["custparam_expdate"]= vExpiryDate;
												/* The below code is merged from Lexjet production account on 03-02-2013 by Radhika as part of Standard bundle*/											
												var customrecord = nlapiCreateRecord('customrecord_ebiznet_batch_entry');
												customrecord.setFieldValue('name', CIarray["custparam_BatchNo"]);
												customrecord.setFieldValue('custrecord_ebizlotbatch', CIarray["custparam_BatchNo"]);
												customrecord.setFieldValue('custrecord_ebizmfgdate', CIarray["custparam_mfgdate"]);
												customrecord.setFieldValue('custrecord_ebizbestbeforedate', CIarray["custparam_bestbeforedate"]);
												customrecord.setFieldValue('custrecord_ebizexpirydate', CIarray["custparam_expdate"]);
												customrecord.setFieldValue('custrecord_ebizfifodate', CIarray["custparam_fifodate"]);
												customrecord.setFieldValue('custrecord_ebizfifocode', CIarray["custparam_fifocode"]);
												customrecord.setFieldValue('custrecord_ebizlastavldate', CIarray["custparam_lastdate"]);
												nlapiLogExecution('ERROR', '1');
												nlapiLogExecution('ERROR', 'Item Id',ItemId);
												customrecord.setFieldValue('custrecord_ebizsku', ItemId);
//												case# 201417174
												if(locationId != null && locationId != '')
													customrecord.setFieldValue('custrecord_ebizsitebatch',locationId);
												var rec = nlapiSubmitRecord(customrecord, false, true);
												nlapiLogExecution('ERROR', 'rec',rec);
												CIarray["custparam_BatchId"] = rec;
												/* Up to here */ 							
												response.sendRedirect('SUITELET', 'customscript_rf_create_invt_itemstat', 'customdeploy_rf_create_invt_itemstat_di', false, CIarray);
											}

								}




								//response.sendRedirect('SUITELET', 'customscript_rf_create_invt_expdate', 'customdeploy_rf_create_invt_expdate_di', false, CIarray);
								/*var customrecord = nlapiCreateRecord('customrecord_ebiznet_batch_entry');

						customrecord.setFieldValue('name', getBatchNo);
						customrecord.setFieldValue('custrecord_ebizlotbatch', getBatchNo);
						nlapiLogExecution('ERROR', '1');
						nlapiLogExecution('ERROR', 'Item Id',ItemId);
						//customrecord.setFieldText('custrecord_ebizsku', getPOItem);
						customrecord.setFieldValue('custrecord_ebizsku', ItemId);
						customrecord.setFieldValue('custrecord_ebizsitebatch',locationId);

						customrecord.setFieldValue('custrecord_ebizmfgdate', POarray["custparam_mfgdate"]);
							customrecord.setFieldValue('custrecord_ebizbestbeforedate', POarray["custparam_bestbeforedate"]);
							customrecord.setFieldValue('custrecord_ebizexpirydate', POarray["custparam_expdate"]);
							customrecord.setFieldValue('custrecord_ebizfifodate', POarray["custparam_fifodate"]);
							customrecord.setFieldValue('custrecord_ebizfifocode', POarray["custparam_fifocode"]);
							customrecord.setFieldValue('custrecord_ebizlastavldate', POarray["custparam_lastdate"]);

						var rec = nlapiSubmitRecord(customrecord, false, true);
						CIarray["custparam_BatchId"] = rec;
						CIarray["custparam_BatchNo"] = getBatchNo;
						response.sendRedirect('SUITELET', 'customscript_rf_create_invt_itemstat', 'customdeploy_rf_create_invt_itemstat_di', false, CIarray);
								 */
							}
							else
							{
								CIarray["custparam_BatchId"] = batchid;
								response.sendRedirect('SUITELET', 'customscript_rf_create_invt_itemstat', 'customdeploy_rf_create_invt_itemstat_di', false, CIarray);
							}
						}
					}
					catch (e) {
						nlapiLogExecution('ERROR', 'Failed to Update/Insert into BATCH ENTRY Record');
					}


				}
				else 
				{
					CIarray["custparam_error"] = st4;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
					nlapiLogExecution('ERROR', 'Batch is not entered', getBatchNo);
					return;
				}


			} 
			catch (e) 
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
				nlapiLogExecution('ERROR', 'Catching the Search Results', 'Length is null');
			}
			nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
		}
	}
}

function IsValidBatch(actbatch,actitem)
{
	nlapiLogExecution('ERROR', 'Into IsValidBatch');
	nlapiLogExecution('ERROR', 'Batch',actbatch);
	nlapiLogExecution('ERROR', 'Item',actitem);
	var IsValidBatch=true;
	var BatchId;
	var columns = new Array();
	var filters = new Array();

	columns[0] = new nlobjSearchColumn('custrecord_ebizlotbatch');

	filters.push(new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', actbatch));
	//filters.push(new nlobjSearchFilter('itemid','custrecord_ebizsku', 'is', actitem));
	filters.push(new nlobjSearchFilter('custrecord_ebizsku',null, 'is', actitem));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filters, columns);
	if(searchresults != null && searchresults != ''&& searchresults.length>0)
	{
		BatchId = searchresults[0].getId();
		//IsValidBatch=true;
	}
	//else
	//IsValidBatch=false;

	nlapiLogExecution('ERROR', 'existing batch no',BatchId);

	nlapiLogExecution('ERROR', 'Out of IsValidBatch');

	return BatchId;

}

//case 20123285 start

function ValidateSplCharacter(string,name)
{
	var iChars = "*|,\":<>[]{}`\';()@&$#% ";
	var length=string.length;
	var flag = 'N';
	for(var i=0;i<length;i++)
	{
		if(iChars.indexOf(string.charAt(i))!=-1)
		{
			flag='Y';
			break;
		}
	}
	if(flag == 'Y')
	{
		return false;
	}
	else
	{
		return true;
	}

}//end
