/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/*
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inventory/Suitelet/Attic/ebiz_WOReport_SL.js,v $
 *     	   $Revision: 1.1.2.1.4.2 $
 *     	   $Date: 2014/06/13 15:35:26 $
 *     	   $Author: sponnaganti $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 *
 *****************************************************************************/

function WOReportSuitelet(request, response){
	if (request.getMethod() == 'GET') {

		var form = nlapiCreateForm('Work Order Report');

		var woid=request.getParameter('custpage_workorder');
		nlapiLogExecution('ERROR','woid',woid);

		var WorkOrder  = form.addField('custpage_workorder', 'select', 'Work Order').setLayoutType('startrow', 'none');
		WorkOrder.addSelectOption("","");
		fillWOField(form, WorkOrder,-1);


		WorkOrder.setMandatory(true);
		//fillsalesorderField(form, salesorder,-1);

		form.addSubmitButton('Display');


		response.writePage(form);

	}
	else  
	{ 

		var form = nlapiCreateForm('Work Order Report');

		var woid=request.getParameter('custpage_workorder');
		nlapiLogExecution('ERROR','woid',woid);

		var WorkOrder  = form.addField('custpage_workorder', 'select', 'Work Order').setLayoutType('startrow', 'none');
		WorkOrder.addSelectOption("","");
		fillWOField(form, WorkOrder,-1);
		WorkOrder.setDefaultValue(request.getParameter('custpage_workorder'));


		WorkOrder.setMandatory(true);
		//fillsalesorderField(form, salesorder,-1);

		form.addSubmitButton('Display');


		if(woid!=null && woid!='')
		{
			var ItemSubList = form.addSubList("custpage_woreport", "list", "Components");
			ItemSubList.addField("custpage_woreport_item", "text","Item");
			ItemSubList.addField("custpage_woreport_binloc", "text", "Bin Location");
			ItemSubList.addField("custpage_woreport_lp", "text", "LP");
			ItemSubList.addField("custpage_woreport_lotno", "text", "Lot#");  //added by santosh
			ItemSubList.addField("custpage_woreport_itemstatus", "text", "ItemStatus"); //added by santosh 
			ItemSubList.addField("custpage_woreport_qty", "text", "Quantity");   

			nlapiLogExecution('ERROR','INTOwoid1',woid);
			var WOFilers = new Array();
			WOFilers.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', woid));
			WOFilers.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', 5));//kts
			WOFilers.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));//STATUS.INBOUND.PUTAWAY_COMPLETE�


			var columns=new Array();
			columns[0]=new nlobjSearchColumn('custrecord_sku');
			//columns[0].setSort(true);
			columns[1]=new nlobjSearchColumn('custrecord_actbeginloc');//bin loc
			columns[2]=new nlobjSearchColumn('custrecord_actendloc');//bin loc
			columns[3]=new nlobjSearchColumn('custrecord_lpno');
			columns[4]=new nlobjSearchColumn('custrecord_act_qty');
			columns[5]=new nlobjSearchColumn('custrecord_batch_no');//added by santosh
			columns[6]=new nlobjSearchColumn('custrecord_sku_status');//added by santosh

			var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, WOFilers,columns);



			if(searchresults!=null && searchresults!='') 
			{ 	    	
				for (var i = 0; i < searchresults.length; i++) {

					taskid = searchresults[i].getId();
					var vitem = searchresults[i].getText('custrecord_sku');
					var vactbeginloc = searchresults[i].getText('custrecord_actbeginloc');
					var vactendloc = searchresults[i].getText('custrecord_actendloc');
					var vlpno = searchresults[i].getValue('custrecord_lpno');
					var vactqty = searchresults[i].getValue('custrecord_act_qty');

					var vlotno = searchresults[i].getValue('custrecord_batch_no');//added by santosh
					var Itemstatus = searchresults[i].getText('custrecord_sku_status');//added by santosh
					nlapiLogExecution('ERROR','vlotno',vlotno);

					form.getSubList('custpage_woreport').setLineItemValue('custpage_woreport_item', i+1, vitem);
					form.getSubList('custpage_woreport').setLineItemValue('custpage_woreport_binloc', i+1, vactendloc);
					form.getSubList('custpage_woreport').setLineItemValue('custpage_woreport_lp', i+1, vlpno);
					form.getSubList('custpage_woreport').setLineItemValue('custpage_woreport_qty', i+1, vactqty);  
					//code added by santosh on 08Oct12
					form.getSubList('custpage_woreport').setLineItemValue('custpage_woreport_lotno', i+1, vlotno);
					form.getSubList('custpage_woreport').setLineItemValue('custpage_woreport_itemstatus', i+1, Itemstatus);
					//end of the code

				}
			}

		} 
		response.writePage(form);

	}

	function fillWOField(form, WOField,maxno){
		var WOFilers = new Array();
		WOFilers.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
		WOFilers.push(new nlobjSearchFilter('status', null, 'is', 'WorkOrd:G'));
		//case# 20148583 starts
		var vRoleLocation = getRoledBasedLocationNew();
		if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
		{
			WOFilers.push(new nlobjSearchFilter('location', null, 'anyof', vRoleLocation));
		}
		//case# 20148583 ends
		if(maxno!=-1)
		{
			WOFilers.push(new nlobjSearchFilter('internalidnumber', null, 'lessthan', parseFloat(maxno)));
		}


		WOField.addSelectOption("", "");

		var columns=new Array();
		columns[0]=new nlobjSearchColumn('tranid');
		columns[0].setSort(true);
		columns[1]=new nlobjSearchColumn('internalid');
		columns[2]=new nlobjSearchColumn('type');


		var customerSearchResults = nlapiSearchRecord('workorder', null, WOFilers,columns);


		for (var i = 0; customerSearchResults != null && i < customerSearchResults.length; i++) {

			//nlapiLogExecution('ERROR', 'tranid', customerSearchResults[i].getValue('tranid')); 
			if(customerSearchResults[i].getValue('tranid') != null && customerSearchResults[i].getValue('tranid') != "" && customerSearchResults[i].getValue('tranid') != " ")
			{
				var resdo = form.getField('custpage_workorder').getSelectOptions(customerSearchResults[i].getValue('tranid'), 'is');
				if (resdo != null) {
					if (resdo.length > 0) {
						continue;
					}
				}
			}
			WOField.addSelectOption(customerSearchResults[i].getValue('internalid'), customerSearchResults[i].getValue('tranid'));
		}
		if(customerSearchResults!=null && customerSearchResults.length>=1000)
		{
			var column=new Array();
			column[0]=new nlobjSearchColumn('tranid');		
			column[1]=new nlobjSearchColumn('internalid');
			column[1].setSort(true);

			var OrderSearchResults = nlapiSearchRecord('workorder', null, WOFilers,column);

			var maxno=OrderSearchResults[OrderSearchResults.length-1].getValue(columns[1]);		
			fillWOField(form, WOField,maxno);	

		}
	}

}