/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Common/Suitelet/InboundGeneralFunctions.js,v $
 *     	   $Revision: 1.9.2.15.4.2.2.6 $
 *     	   $Date: 2013/10/29 13:36:25 $
 *     	   $Author: snimmakayala $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * 
 * PRAMETERS
 *
 * DESCRIPTION
 * 
 * REVISION HISTORY
 * $Log: InboundGeneralFunctions.js,v $
 * Revision 1.9.2.15.4.2.2.6  2013/10/29 13:36:25  snimmakayala
 * Standard Bundle Fix.
 * Case# : 20124515
 * Empty location check during putaway location generation.
 *
 * Revision 1.9.2.15.4.2.2.5  2013/10/29 08:42:35  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 20125154
 *
 * Revision 1.9.2.15.4.2.2.4  2013/09/19 15:20:55  rmukkera
 * Case# 20124445
 *
 * Revision 1.9.2.15.4.2.2.3  2013/06/11 14:30:56  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.9.2.15.4.2.2.2  2013/05/31 15:10:00  grao
 * CASE201112/CR201113/LOG201121
 * PMM Issues fixes
 *
 * Revision 1.9.2.15.4.2.2.1  2013/03/15 09:28:43  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.9.2.15.4.2  2013/02/01 15:22:50  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.9.2.15.4.1  2012/11/03 01:37:54  gkalla
 * CASE201112/CR201113/LOG201121
 * invalid location issue. added inactive condition
 *
 * Revision 1.9.2.15  2012/07/03 21:53:16  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Putaway Strategy changes
 *
 * Revision 1.9.2.14  2012/03/09 19:16:42  gkalla
 * CASE201112/CR201113/LOG201121
 * Changed the operator any to anyof  while fetching generate location
 *
 * Revision 1.9.2.13  2012/03/09 14:09:48  spendyala
 * CASE201112/CR201113/LOG201121
 * Changed in GeneratePutawayLocation for fetching the record based on bin location id.
 *
 * Revision 1.9.2.12  2012/03/09 07:34:19  spendyala
 * CASE201112/CR201113/LOG201121
 * Code Merged from trunck.
 *
 * Revision 1.9.2.11  2012/02/14 06:45:12  spendyala
 * CASE201112/CR201113/LOG201121
 * while checking the condition to fetch the required binlocation, 'equalto' operator is missing.
 *
 * Revision 1.9.2.10  2012/02/13 23:21:11  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge
 *
 * Revision 1.16  2012/02/13 23:11:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge
 *
 * Revision 1.15  2012/02/10 09:44:42  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge
 *
 * Revision 1.14  2012/02/09 14:43:42  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Physical and Virtual Location changes
 *
 * Revision 1.13  2012/02/08 15:53:30  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge to brach
 *
 * Revision 1.12  2012/02/08 09:41:27  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged files from batch up to 1.9.2.3
 * issue: while searching for empty binloc,
 * " for loop"  is not termination  eventhough empty binloc is found .
 * so added break stmt to  for loop to terminate the looping process.
 *
 * Revision 1.11  2012/02/07 09:03:21  spendyala
 * CASE201112/CR201113/LOG201121
 *  Merged files from batch up to 1.9.2.2 revision.i.e.,
 *  In GetPutAwayLocation function while passing filter,
 *  incrementing 'i' value was missing in the previous version.
 *
 * Revision 1.10  2012/02/01 13:41:55  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged files from batch up to 1.9.2.1 revision.
 * issue related to searching empty binloc  which causing script execution limit exceed is resolved.
 *
 * Revision 1.9  2012/01/04 16:01:03  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Merge/Mix strategy issue fixes
 *
 * Revision 1.8  2011/12/27 23:24:07  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Merge LP Fixes
 *
 * Revision 1.7  2011/12/27 16:44:15  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Merge LP Fixes
 *
 * Revision 1.6  2011/12/19 14:59:34  snimmakayala
 * CASE201112/CR201113/LOG201121
 * FIFO in RF Receiving.
 *
 * Revision 1.5  2011/12/02 13:20:09  schepuri
 * CASE201112/CR201113/LOG201121
 * RF To generate location acc to the location defined in PO
 *
 * Revision 1.4  2011/10/03 09:02:42  jrnarsupalli
 * CASE201112/CR201113/LOG201121
 * RM Comments: Corrected CVS header to have CVS key words.
 *
 *
 *****************************************************************************/

function getItemFields(item){
	nlapiLogExecution('DEBUG',  'getItemFields', 'Start');
	var fields = ['custitem_item_family','custitem_item_group','custitem_ebizdefskustatus',
	              'custitem_item_info_1','custitem_item_info_2','custitem_item_info_3'];

	var columns = nlapiLookupField('item', item, fields);
	var usage = getUnitsConsumed('nlapiLookupField');

	nlapiLogExecution('DEBUG', 'getItemFields|USAGE', usage);
	nlapiLogExecution('DEBUG',  'getItemFields', 'End');
	return columns;
}

/**
 * Function to return the putaway rules for the specified item parameters
 * 
 * @param itemFamily
 * @param itemGroup
 * @param itemStatus
 * @param itemInfo1
 * @param itemInfo2
 * @param itemInfo3
 * @param item
 * @returns [List of putaway rules configured]
 */
function getPutawayRules(itemFamily, itemGroup, itemStatus, itemInfo1, itemInfo2, itemInfo3, item,poLoc){
	var columns = new Array();
	var filters = new Array();

	columns[0] = new nlobjSearchColumn('formulanumeric');
	columns[0].setFormula("TO_NUMBER({custrecord_sequencenumberpickrule})");		// SEQUENCE NUMBER
	columns[1] = new nlobjSearchColumn('custrecord_locationgrouppickrule');			// LOCN GROUP
	columns[2] = new nlobjSearchColumn('custrecord_putawayzonepickrule');			// PUTAWAY ZONE
	columns[3] = new nlobjSearchColumn('custrecord_putawaymethod');					// PUTAWAY METHOD
	columns[4] = new nlobjSearchColumn('custrecord_ruleidpickrule');				// RULE ID
	columns[5] = new nlobjSearchColumn('custrecord_manual_location_generation');	// MANUAL LOCN GEN FLAG

	columns[0].setSort();		// SORT BY SEQUENCE ASCENDING

	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	if(itemFamily != null && itemFamily != "")
		filters.push(new nlobjSearchFilter('custrecord_skufamilypickrule', null, 'anyof', ['@NONE@', itemFamily]));

	if(itemGroup != null && itemGroup != "")
		filters.push(new nlobjSearchFilter('custrecord_skugrouppickrule', null, 'anyof', ['@NONE@', itemGroup]));

	if(itemStatus !=null && itemStatus != "")
		filters.push(new nlobjSearchFilter('custrecord_skustatuspickrule', null, 'anyof', ['@NONE@', itemStatus]));

	if(itemInfo1 != null && itemInfo1 != "")
		filters.push(new nlobjSearchFilter('custrecord_skuinfo1pickrule', null, 'any', ['@NONE@', itemInfo1]));

	if(itemInfo2 != null && itemInfo2 != "")
		filters.push(new nlobjSearchFilter('custrecord_skuinfo1pickrule', null, 'any', ['@NONE@', itemInfo2]));

	if(itemInfo3 != null && itemInfo3 != "")
		filters.push(new nlobjSearchFilter('custrecord_skuinfo1pickrule', null, 'any', ['@NONE@', itemInfo3]));

	if(item != null && item != "")
		//filters.push(new nlobjSearchFilter('custrecord_skupickrule', null, 'anyof', ['@NONE@', item]));
		filters.push(new nlobjSearchFilter('custrecord_skupickrule', null, 'anyof', ['@NONE@']));

	if(poLoc != null && poLoc != "")
		filters.push(new nlobjSearchFilter('custrecord_ebizsitepickput', null, 'anyof', ['@NONE@', poLoc]));
	nlapiLogExecution('DEBUG', 'poLoc', poLoc);

	var putawayRules = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters, columns);
	var usage = getUnitsConsumed('nlapiSearchRecord');

	nlapiLogExecution('DEBUG', 'getPutawayRules|USAGE', usage);
	logCountMessage('DEBUG', putawayRules);
	//nlapiLogExecution('DEBUG', 'getPutawayRules', 'End');

	return putawayRules;
}

/**
 * Function to retrieve the list of put methods for all the putaway rules
 * 
 * @returns
 */
function getPutMethods(){
	nlapiLogExecution('DEBUG', 'getPutMethods', 'Start');

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_dimension_putawaymethod');
	columns[2] = new nlobjSearchColumn('custrecord_emptylocation');
	columns[3] = new nlobjSearchColumn('custrecord_mergeputaway');
	columns[4] = new nlobjSearchColumn('custrecord_mixlotsputaway');
	columns[5] = new nlobjSearchColumn('custrecord_mixpackcode');
	columns[6] = new nlobjSearchColumn('custrecord_mixsku');
	columns[7] = new nlobjSearchColumn('custrecord_mergelp');
	columns[8] = new nlobjSearchColumn('custrecord_allowsplit');
	columns[9] = new nlobjSearchColumn('custrecord_methodid');

	var putMethodList = nlapiSearchRecord('customrecord_ebiznet_putaway_method', null, filters, columns);
	var usage = getUnitsConsumed('nlapiSearchRecord');

	nlapiLogExecution('DEBUG', 'getPutMethods|USAGE', usage);
	logCountMessage('DEBUG', putMethodList);
	nlapiLogExecution('DEBUG', 'getPutMethods', 'End');
	return putMethodList;
}

/**
 * Function to get the list of putaway zones for the retrieved putaway rules
 * 
 * @param putawayRuleList
 * @returns {Array}
 */
function getPutZoneList(putawayRuleList){
	nlapiLogExecution('DEBUG', 'getPutZoneList', 'Start');
	var zoneList = new Array();

	if(putawayRuleList != null && putawayRuleList.length > 0){
		for(var i = 0; i < putawayRuleList.length; i++){
			zoneList.push(putawayRuleList[i].getValue('custrecord_putawayzonepickrule'));
		}
	}

	logCountMessage('DEBUG', zoneList);
	nlapiLogExecution('DEBUG', 'getPutZoneList', 'End');
	return zoneList;
}

/**
 * Function to get the list of locn groups for the given zone list
 * 
 * @param zoneList
 * @returns
 */
function getLocnGroupsForZoneList(zoneList){
	nlapiLogExecution('DEBUG', 'getLocnGroupsForZoneList', 'Start');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_locgroup_no');
	columns[1] = new nlobjSearchColumn('custrecordcustrecord_putzoneid');
	columns[0].setSort();

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'anyof', zoneList));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var locnGroupList = nlapiSearchRecord('customrecord_zone_locgroup', null, filters, columns);
	var usage = getUnitsConsumed('nlapiSearchRecord');

	nlapiLogExecution('DEBUG', 'getLocnGroupsForZoneList|USAGE', usage);
	logCountMessage('DEBUG', locnGroupList);
	nlapiLogExecution('DEBUG', 'getLocnGroupsForZoneList', 'End');
	return locnGroupList;
}

/**
 * 
 * @param locnGroupList
 * @param zoneId
 * @returns
 */
function getLocnGroupForZoneId(locnGroupList, zoneId){
	nlapiLogExecution('DEBUG', 'getLocnGroupForZoneId', 'Start');
	var locnGroupId;
	var locnGroupFound = false;

	if(locnGroupList != null && locnGroupList.length > 0){
		for(var i = 0; i < locnGroupList.length; i++){
			if(!locnGroupFound){
				if(locnGroupList[i].getValue('custrecordcustrecord_putzoneid') == zoneId){
					locnGroupId = locnGroupList[i].getValue('custrecord_locgroup_no');
					locnGroupFound = true;
				}
			}
		}
	}

	nlapiLogExecution('DEBUG', 'locnGroupId', locnGroupId);
	nlapiLogExecution('DEBUG', 'getLocnGroupForZoneId', 'End');
	return locnGroupId;
}

/**
 * 
 * @param item
 * @param locnGroupList
 * @returns
 */
function getInventoryForLocnGroups(item, locnGroupList){
	nlapiLogExecution('DEBUG', 'getInventoryForLocnGroups', 'Start');

	var locnGroups = new Array();
	if(locnGroupList != null && locnGroupList.length > 0){
		for(var i = 0; i < locnGroupList.length; i++){
			locnGroups.push(locnGroupList[i].getValue('custrecord_locgroup_no'));
		}
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_putseqno');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[2] = new nlobjSearchColumn('custrecord_outboundinvlocgroupid');
	columns[3] = new nlobjSearchColumn('custrecord_pickseqno');
	columns[4] = new nlobjSearchColumn('custrecord_inboundinvlocgroupid');
	columns[5] = new nlobjSearchColumn('custrecord_putseqno');
	columns[6] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');

	columns[0].setSort();		// SORT BY PUT SEQ NO ASCENDING

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', item);
	filters[1] = new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'anyof', locnGroups);
	var locnInventory = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
	var usage = getUnitsConsumed('nlapiSearchRecord');

	nlapiLogExecution('DEBUG', 'getInventoryForLocnGroups|USAGE', usage);
	logCountMessage('DEBUG', locnInventory);
	nlapiLogExecution('DEBUG', 'getInventoryForLocnGroups', 'End');
	return locnInventory;
}

/**
 * 
 * @param locnInventory
 * @returns {nlapiSearchRecord}
 */
function getRemainingCubeForLocnIds(locnInventory){
	nlapiLogExecution('DEBUG', 'getRemainingCubeForLocnIds', 'Start');

	var binLocnNameList = new Array();
	if(locnInventory != null && locnInventory.length > 0){
		for(var j = 0; j < locnInventory.length; j++){
			binLocnNameList.push(locnInventory[j].getText('custrecord_ebiz_inv_binloc'));
		}
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_remainingcube');

	var filters = new Array();
	filters.push(new nlobjSearchFilter('name', null, 'any', binLocnNameList));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var remCubeList = new nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);
	var usage = getUnitsConsumed('nlapiSearchRecord');

	logCountMessage('DEBUG', remCubeList);
	nlapiLogExecution('DEBUG', 'getRemainingCubeForLocnIds|USAGE', usage);
	nlapiLogExecution('DEBUG', 'getRemainingCubeForLocnIds', 'End');
	return remCubeList;
}

/**
 * Function to retrieve the remaining cube for the specified location from the list of inventory
 * details
 * 
 * @param remCubeList
 * @param locnName
 * @returns {Number}
 */
function getRemCubeFromList(remCubeList, locnName){
	//nlapiLogExecution('DEBUG', 'getRemCubeFromList', 'Start');
	var locnFound = false;
	var remCube = 0;
	if(remCubeList != null && remCubeList.length > 0){
		for(var i = 0; i < remCubeList.length; i++){
			if(!locnFound){
				if(remCubeList[i].getValue('name') == locnName){
					remCube = remCubeList[i].getValue('custrecord_remainingcube');
					locnFound = true;
				}
			}
		}
	}
	//nlapiLogExecution('DEBUG', 'getRemCubeFromList', 'End');
	return remCube;
}

/**
 * Function to retrieve the setting for an attribute for the putmethod
 * 
 * @param putMethodList
 * @param putAttr
 * @param putMethodId
 * @returns {String}
 */
function getAttributeForPutMethod(putMethodList, putAttr, putMethodId){
	nlapiLogExecution('DEBUG', 'getAttributeForPutMethod', 'Start');
	var attrValue = "";
	if(putMethodList != null && putMethodList.length > 0){
		for(var i = 0; i < putMethodList.length; i++){
			var currPutMethod = putMethodList[i].getValue('name');
			nlapiLogExecution('DEBUG', 'currPutMethod', currPutMethod);
			nlapiLogExecution('DEBUG', 'putMethodId', putMethodId);
			if(currPutMethod == putMethodId){
				if(putAttr == 'DIMENSION')
					attrValue = putMethodList[i].getValue('custrecord_dimension_putawaymethod');
				else if(putAttr == 'EMPTY')
					attrValue = putMethodList[i].getValue('custrecord_dimension_putawaymethod');
				else if(putAttr == 'MERGE')
					attrValue = putMethodList[i].getValue('custrecord_mergeputaway');
				else if(putAttr == 'MIXLOTS')
					attrValue = putMethodList[i].getValue('custrecord_mixlotsputaway');
				else if(putAttr == 'MIXPACKCODE')
					attrValue = putMethodList[i].getValue('custrecord_mixpackcode');
				else if(putAttr == 'MIXSKU')
					attrValue = putMethodList[i].getValue('custrecord_mixsku');
				else if(putAttr == 'MERGELP')
					attrValue = putMethodList[i].getValue('custrecord_mergelp');
				else if(putAttr == 'ALLOWSPLIT')
					attrValue = putMethodList[i].getValue('custrecord_allowsplit');
			}
		}
	}

	nlapiLogExecution('DEBUG', putAttr, attrValue);
	nlapiLogExecution('DEBUG', 'getAttributeForPutMethod', 'End');
	return attrValue;
}

/**
 * Function to return the list of bin locations with the passed location group
 * as the inbound location group
 * 
 * @param locnGroupId
 * @returns
 */
function getBinLocnsForLocnGroup(locnGroupId){
	nlapiLogExecution('DEBUG', 'getBinLocnsForLocnGroup', 'Start');
	nlapiLogExecution('DEBUG', 'locnGroupId', locnGroupId);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_startingputseqno');		// PUT SEQUENCE NO.
	columns[1] = new nlobjSearchColumn('name');								// BIN LOCATION NAME
	columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid');	// OUTBOUND LOCN GROUP ID
	columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno');		// STARTING PICK SEQ NO.
	columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid');		// INBOUND LOCN GROUP ID
	columns[5] = new nlobjSearchColumn('custrecord_remainingcube');			// REMAINING CUBE

	columns[0].setSort();  // SORT BY PUT SEQUENCE NO ASCENDING

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [locnGroupId]));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	//filters[0] = new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'is', locnGroupId);

	var binLocnList = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);
	var usage = getUnitsConsumed('nlapiSearchRecord');

	nlapiLogExecution('DEBUG', 'getBinLocnsForLocnGroup:USAGE', usage);
	logCountMessage('DEBUG', binLocnList);
	nlapiLogExecution('DEBUG', 'getBinLocnsForLocnGroup', 'End');

	return binLocnList;
}

/**
 * Function to generate a list of location Ids from the location results
 * 
 * @param locnResults
 * @returns
 */
function getLocationIdList(locnResults){
	nlapiLogExecution('DEBUG', 'getLocationList', 'Start');
	var locnIdList = new Array();

	if(locnResults != null && locnResults.length > 0){
		for(var i = 0; i < locnResults.length; i++){
			locnIdList.push(locnResults[i].getId());
		}
	}

	logCountMessage('DEBUG', locnIdList);
	nlapiLogExecution('DEBUG', 'getLocationList', 'End');

	return locnIdList;
}

/**
 * Function to retrieve the inventory for the specified bin locations
 * 
 * @param locnResults
 * @returns
 */
function getInvtDtlsForBinLocns(locnResults){
	nlapiLogExecution('DEBUG', 'getInvDtlsForBinLocns', 'Start');

	var locnIdList = getLocationIdList(locnResults);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');			// BIN LOCATION
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');				// ITEM / SKU

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', locnIdList);

	var invtDtls = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns );
	var usage = getUnitsConsumed('nlapiSearchRecord');

	nlapiLogExecution('DEBUG', 'getInvDtlsForBinLocns|USAGE', usage);
	logCountMessage('DEBUG', invtDtls);
	nlapiLogExecution('DEBUG', 'getInvDtlsForBinLocns', 'Start');
	return invtDtls;
}

/**
 * Function to retrieve the open task records for the specified location list
 * 
 * @param locnResults
 * @returns
 */
function getOpenTaskDtlsForBinLocns(locnResults){
	nlapiLogExecution('DEBUG', 'getOpenTaskDtlsForBinLocns', 'Start');

	var locnIdList = getLocationIdList(locnResults);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');				// ACTUAL BEGIN LOCATION
	columns[1] = new nlobjSearchColumn('custrecord_sku');						// ITEM / SKU

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', locnIdList);

	var openTaskDtls = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	var usage = getUnitsConsumed('nlapiSearchRecord');

	nlapiLogExecution('DEBUG', 'getOpenTaskDtlsForBinLocns|USAGE', usage);
	logCountMessage('DEBUG', openTaskDtls);
	nlapiLogExecution('DEBUG', 'getOpenTaskDtlsForBinLocns', 'End');
	return openTaskDtls;
}

/**
 * Function to retrieve if the specified location is empty or not
 * 
 * @param locnId
 * @param invtDtls
 * @param openTaskDtls
 * @returns {String}
 */
function isLocationEmpty(locnId, invtDtls, openTaskDtls){
	nlapiLogExecution('DEBUG', 'isLocationEmpty', 'Start');
	var emptyInd = 'N';
	var itemFound = false;

	if(invtDtls != null && invtDtls.length > 0){
		for(var i = 0; i < invtDtls.length; i++){
			var currLocn = invtDtls[i].getValue('custrecord_ebiz_inv_binloc');
			var currSKU = invtDtls[i].getValue('custrecord_ebiz_inv_sku');
			if(!itemFound && currLocn == locnId){
				nlapiLogExecution('DEBUG', 'Location Found in Inventory', currSKU + "@" + currLocn);
				emptyInd = 'Y';
				itemFound = true;
			}
		}
	}

	if(!itemFound && openTaskDtls != null && openTaskDtls.length > 0){
		for(var i = 0; i < openTaskDtls.length; i++){
			var currBeginLocn = openTaskDtls[i].getValue('custrecord_actbeginloc');
			var currSKU = openTaskDtls[i].getValue('custrecord_sku');
			if(!itemFound &&  currBeginLocn == locnId){
				nlapiLogExecution('DEBUG', 'Location Found in OpenTask', currSKU + "@" + currBeginLocn);
				emptyInd = 'Y';
				itemFound = true;
			}
		}
	}

	nlapiLogExecution('DEBUG', 'isLocationEmpty', 'End');
	return emptyInd;
}

/**
 * 
 * @param item
 * @param itemCube
 * @param zoneId
 * @param binLocnList
 * @returns {Array}
 */
function getLocnAllocation(item, itemCube, zoneId, binLocnList,putMethod,putRuleId){
	nlapiLogExecution('DEBUG', 'getLocnAllocation', 'Start');
	var locnFound = false;
	var locnDtls = new Array();

	if(binLocnList != null && binLocnList.length > 0){
		for(var i = 0; i < binLocnList.length; i++){
			if(!locnFound){
				var locn = binLocnList[i].getValue('name');											// LOCN NAME
				var locnInternalId = binLocnList[i].getId();										// LOCN INTERNAL ID
				var locnRemCube = binLocnList[i].getValue('custrecord_remainingcube');				// REMAINING CUBE
				var obLocnGroupId = binLocnList[i].getValue('custrecord_outboundinvlocgroupid');	// OUTBOUND LOCN GROUP ID
				var ibLocnGroupId = binLocnList[i].getValue('custrecord_inboundinvlocgroupid');		// INBOUND LOCN GROUP ID
				var putSeqNo = binLocnList[i].getValue('custrecord_startingputseqno');				// STARTING PUT SEQ NO
				var pickSeqNo = binLocnList[i].getValue('custrecord_startingpickseqno');			// STARTING PICK SEQ NO

				nlapiLogExecution('DEBUG', 'locn', locn);
				nlapiLogExecution('DEBUG', 'locnInternalId', locnInternalId);
				nlapiLogExecution('DEBUG', 'locnRemCube', locnRemCube);
				nlapiLogExecution('DEBUG', 'itemCube', itemCube);
				nlapiLogExecution('DEBUG', 'obLocnGroupId', obLocnGroupId);
				nlapiLogExecution('DEBUG', 'ibLocnGroupId', ibLocnGroupId);
				nlapiLogExecution('DEBUG', 'startPutSeqNo', putSeqNo);
				nlapiLogExecution('DEBUG', 'startpickSeqNo', pickSeqNo);
				nlapiLogExecution('DEBUG', 'zoneId', zoneId);

				if(parseFloat(locnRemCube) > parseFloat(itemCube)){
					var remCube = parseFloat(locnRemCube) - parseFloat(itemCube);

					locnDtls[0] = locn;
					locnDtls[1] = remCube;
					locnDtls[2] = locnInternalId;		//locResults[i].getId();
					locnDtls[3] = obLocnGroupId;
					locnDtls[4] = pickSeqNo;
					locnDtls[5] = ibLocnGroupId;
					locnDtls[6] = putSeqNo;
					locnDtls[7] = locnRemCube;
					locnDtls[8] = zoneId;
					locnDtls[9] = putMethod;
					locnDtls[10] = putRuleId;

					nlapiLogExecution('DEBUG', 'Allocating location', locn);
					nlapiLogExecution('DEBUG', 'remCube', remCube);
					locnFound = true;
				}
			}
		}    	
	}		

	nlapiLogExecution('DEBUG', 'getLocnAllocation', 'End');
	return locnDtls;
}

/**
 * Function to allocate a location based on the merge flag setting
 * 
 * @param locnInventory
 * @param remCubeList
 * @param itemCube
 * @returns
 */
function getMergeLocnAllocation(locnInventory, remCubeList, itemCube,putMethod,putRuleId){
	nlapiLogExecution('DEBUG', 'getMergeLocnAllocation', 'Start');
	var locnFound = false;
	var locnDtls = null;

	for(var j = 0; j < locnInventory.length; j++){
		if(!locnFound){
			var binLocn = locnInventory[j].getText('custrecord_ebiz_inv_binloc');
			var binLocnInternalId = locnInventory[j].getValue('custrecord_ebiz_inv_binloc');
			var obLocnGroup = locnInventory[j].getValue('custrecord_outboundinvlocgroupid');
			var pickSeqNo = locnInventory[j].getValue('custrecord_pickseqno');
			var ibLocnGroup = locnInventory[j].getValue('custrecord_inboundinvlocgroupid');
			var putSeqNo = locnInventory[j].getValue('custrecord_putseqno');
			var locnRemCube = getRemCubeFromList(remCubeList, binLocn);

			if(parseFloat(locnRemCube) > parseFloat(itemCube)){
				var remCube = parseFloat(locnRemCube) - parseFloat(itemCube);

				locnDtls = new Array();
				locnDtls[0] = binLocn;
				locnDtls[1] = remCube;
				locnDtls[2] = binLocnInternalId;		//locResults[i].getId();
				locnDtls[3] = obLocnGroup;
				locnDtls[4] = pickSeqNo;
				locnDtls[5] = ibLocnGroup;
				locnDtls[6] = putSeqNo;
				locnDtls[7] = locnRemCube;
				locnDtls[8] = '';
				locnDtls[9] = putMethod;
				locnDtls[10] = putRuleId;

				nlapiLogExecution('DEBUG', 'Allocating merge location', binLocn);
				nlapiLogExecution('DEBUG', 'remCube', remCube);
				locnFound = true;
			}
		}
	}

	nlapiLogExecution('DEBUG', 'getMergeLocnAllocation', 'End');
	return locnDtls;
}

/**
 * Function to allocate a location for the empty flag setting
 * 
 * @param currLocation
 * @param itemCube
 * @param zoneId
 * @returns {Array}
 */
function getEmptyLocnAllocation(currLocation, itemCube, zoneId,putMethod,putRuleId){
	nlapiLogExecution('DEBUG', 'getEmptyLocnAllocation', 'Start');
	var locnDtls = new Array();

	locnRemCube = currLocation.getValue('custrecord_remainingcube');
	obLocnGroupId = currLocation.getValue('custrecord_outboundlocgroupid');
	pickSeqNo = currLocation.getValue('custrecord_startingpickseqno');
	ibLocnGroupId = currLocation.getValue('custrecord_inboundlocgroupid');
	putSeqNo = currLocation.getValue('custrecord_startingputseqno');
	locnId = currLocation.getId();

	nlapiLogExecution('DEBUG', 'locnRemCube', locnRemCube);
	nlapiLogExecution('DEBUG', 'obLocnGroupId', obLocnGroupId);
	nlapiLogExecution('DEBUG', 'pickSeqNo', pickSeqNo);
	nlapiLogExecution('DEBUG', 'ibLocnGroupId', ibLocnGroupId);
	nlapiLogExecution('DEBUG', 'putSeqNo', putSeqNo);

	if(parseFloat(locnRemCube) > parseFloat(itemCube)){
		var remCube = parseFloat(locnRemCube) - parseFloat(itemCube);
		var location = currLocation.getValue('name');
		locnDtls[0] = location;
		locnDtls[1] = remCube;
		locnDtls[2] = locnId;
		locnDtls[3] = obLocnGroupId;
		locnDtls[4] = pickSeqNo;
		locnDtls[5] = ibLocnGroupId;
		locnDtls[6] = putSeqNo;
		locnDtls[7] = locnRemCube;
		locnDtls[8] = zoneId;
		locnDtls[9] = putMethod;
		locnDtls[10] = putRuleId;

		nlapiLogExecution('DEBUG', 'Allocating for empty location check', location);
		nlapiLogExecution('DEBUG', 'remCube', remCube);
	}

	logCountMessage('DEBUG', locnDtls);
	nlapiLogExecution('DEBUG', 'getEmptyLocnAllocation', 'End');
	return locnDtls;
}

/**
 * 	Get the Item Fields
 * 	Get matching putaway rules for item
 * 	For all putaway rules with manual location generation flag = 'F'
 * 		Get corresponding location group id
 * 		If location group is valid,
 * 			Get all bin locations with matching inbound location group id
 * 			For all bin locations
 * 				Retrieve the remaining cube
 * 				If remaining cube > item cube
 * 					Assign location to item
 * 				End if
 * 			End for
 * 		Else if location group is null or blank
 * 			Get merge flag for putaway method
 * 			If zone id is valid
 * 				Retrieve the location group for the zone id
 * 				For all location groups retrieved
 * 					Get location group id
 * 					If put method has merge flag = 'T'
 * 						Retrieve inventory for all bin locations for the current item and location group
 * 						For all the bin locations
 * 						Retrieve the remaining cube
 * 						If remaining cube > item cube
 * 							Assign location to item
 * 							Return location
 * 						End If
 * 					End If
 * 				End For
 * 			End if
 * 		End if
 * 	End For
 * 	Return Location
 * 
 * @param Item
 * @param PackCode
 * @param ItemStatus
 * @param UOM
 * @param ItemCube
 * @param ItemType
 * @returns {Array}
 */
function generatePutawayLocationold(Item, PackCode, ItemStatus, UOM, ItemCube, ItemType,poLoc){
	nlapiLogExecution('DEBUG', 'generatePutawayLocation', 'Start');

	var allocDtls = null;

	// Input parameters
	nlapiLogExecution('DEBUG', 'Item', Item);
	nlapiLogExecution('DEBUG', 'PackCode', PackCode);
	nlapiLogExecution('DEBUG', 'ItemStatus', ItemStatus);
	nlapiLogExecution('DEBUG', 'UOM', UOM);
	nlapiLogExecution('DEBUG', 'ItemCube', ItemCube);
	nlapiLogExecution('DEBUG', 'ItemType', ItemType);

	// Get fields related to the item
	var itemFields = getItemFields(Item);
	var itemFamily = itemFields.custitem_item_family;
	var itemGroup = itemFields.custitem_item_group;
	var itemStat = itemFields.custitem_ebizdefskustatus;
	var itemInfo1 = itemFields.custitem_item_info_1;
	var itemInfo2 = itemFields.custitem_item_info_2;
	var itemInfo3 = itemFields.custitem_item_info_3;

	nlapiLogExecution('DEBUG', 'itemFamily', itemFamily);
	nlapiLogExecution('DEBUG', 'itemGroup', itemGroup);
	nlapiLogExecution('DEBUG', 'itemStat', itemStat);
	nlapiLogExecution('DEBUG', 'itemInfo1', itemInfo1);
	nlapiLogExecution('DEBUG', 'itemInfo2', itemInfo2);
	nlapiLogExecution('DEBUG', 'itemInfo3', itemInfo3);
	// Get list of putaway rules configured
	var putawayRuleList = getPutawayRules(itemFamily, itemGroup, ItemStatus, itemInfo1, itemInfo2, itemInfo3, Item,poLoc);
	nlapiLogExecution('DEBUG', 'putawayRuleList', putawayRuleList);
	if(putawayRuleList != null && putawayRuleList.length > 0){
		// Get all the put methods
		var putMethodList = getPutMethods();
		nlapiLogExecution('DEBUG', 'putMethodList', putMethodList);

		// Get all the putzones for the retrieved putaway rules
		var zoneList = getPutZoneList(putawayRuleList);
		nlapiLogExecution('DEBUG', 'zoneList', zoneList);

		// Get all locn groups for the zone list
		var locnGroupList = getLocnGroupsForZoneList(zoneList);
		nlapiLogExecution('DEBUG', 'locnGroupList', locnGroupList);

		// Get inventory for locn groups
		var locnInventory = getInventoryForLocnGroups(Item, locnGroupList);
		nlapiLogExecution('DEBUG', 'locnInventory', locnInventory);

		// Get Remaining Cube for Locations
		var remCubeList = getRemainingCubeForLocnIds(locnInventory);
		nlapiLogExecution('DEBUG', 'remCubeList', remCubeList);


		for(var i = 0; i < putawayRuleList.length; i++){
			var locnGroupId = putawayRuleList[i].getValue('custrecord_locationgrouppickrule');
			var zoneId = putawayRuleList[i].getValue('custrecord_putawayzonepickrule');
			var putMethod = putawayRuleList[i].getText('custrecord_putawaymethod');
			var putRuleId = putawayRuleList[i].getValue('custrecord_ruleidpickrule');
			var manualLocnGenFlag = putawayRuleList[i].getValue('custrecord_manual_location_generation');

			nlapiLogExecution('DEBUG', 'putRuleId', putRuleId);
			nlapiLogExecution('DEBUG', 'putMethod', putMethod);
			nlapiLogExecution('DEBUG', 'locnGroupId', locnGroupId);
			nlapiLogExecution('DEBUG', 'manualLocnGenFlag', manualLocnGenFlag);

			if(manualLocnGenFlag == 'F'){
				nlapiLogExecution('DEBUG', 'Processing with Manual Location Generation = FALSE', '');
				if(locnGroupId != 0 && locnGroupId != null){
					nlapiLogExecution('DEBUG', 'Processing with non-null Locn Group Id', locnGroupId);

					// Get bin locations with inbound locn group = locnGroupId
					var binLocnList = getBinLocnsForLocnGroup(locnGroupId);
					allocDtls = getLocnAllocation(Item, ItemCube, zoneId, binLocnList,putMethod,putRuleId);
				} else {
					nlapiLogExecution('DEBUG', 'Proceeding with Putaway Rule Attributes Check', '');
					// Determine merge flag for putmethod
					var mergeFlag = getAttributeForPutMethod(putMethodList, 'MERGE', putMethod);
					nlapiLogExecution('DEBUG', 'mergeFlag', mergeFlag);

					// Check if mergeFlag is set for putaway rule
					if(mergeFlag == 'T' && zoneId != null && zoneId != ""){
						nlapiLogExecution('DEBUG', 'Processing with mergeFlag = TRUE', '');

						if(locnInventory != null && locnInventory.length > 0){
							nlapiLogExecution('DEBUG', 'Processing for available locnInventory', locnInventory.length);
							allocDtls = getMergeLocnAllocation(locnInventory, remCubeList, ItemCube,putMethod,putRuleId);
						}
					}
					// Check if locn was allocated with the merge flag algorithm
					if(allocDtls == null || allocDtls.length == 0){
						var tLocGroupId = getLocnGroupForZoneId(locnGroupList, zoneId);
						nlapiLogExecution('DEBUG', 'Location Not found for Merge Location for Location Group', tLocGroupId);
						var locResults = getBinLocnsForLocnGroup(tLocGroupId);
						var invtDtls = getInvtDtlsForBinLocns(locResults);
						var openTaskDtls = getOpenTaskDtlsForBinLocns(locResults);

						if(locResults != null && locResults.length > 0){
							nlapiLogExecution('DEBUG', 'Proceeding with empty location check', '');
							for(var i = 0; i < locResults.length; i++){
								var emptyInd = isLocationEmpty(locResults[i].getId(), invtDtls, openTaskDtls);
								if(emptyInd == 'N'){
									allocDtls = getEmptyLocnAllocation(locResults[i], ItemCube, zoneId,putMethod,putRuleId);
								}
							}
						}
					}
				}
			}
		}
	}
	else
		nlapiLogExecution('DEBUG', 'Putway rules are not fetched', '');


	logCountMessage('DEBUG', allocDtls);
	nlapiLogExecution('DEBUG', 'generatePutawayLocation', 'End');

	return allocDtls;
}

/**
 * Function to return the number of usage units consumed by the Netsuite API call
 * 
 * @param funcName
 * @returns {Number}
 */
function getUnitsConsumed(funcName){
	var units = 0;
	switch(funcName){
	case 'nlapiDeleteFile':
		units = 20;
		break;
	case 'nlapiInitiateWorkflow':
		units = 20;
		break;
	case 'nlapiTriggerWorkflow':
		units = 20;
		break;
	case 'nlapiScheduleScript':
		units = 20;
		break;
	case 'nlapiSubmitConfiguration':
		units = 20;
		break;
	case 'nlapiSubmitFile':
		units = 20;
		break;
	case 'nlapiDeleteRecord':
		units = 4;
		break;
	case 'nlapiSubmitRecord':
		units = 4;
		break;
	case 'nlapiExchangeRate':
		units = 10;
		break;
	case 'nlapiLoadConfiguration':
		units = 10;
		break;
	case 'nlapiLoadFile':
		units = 10;
		break;
	case 'nlapiMergeRecord':
		units = 10;
		break;
	case 'nlapiRequestURL':
		units = 10;
		break;
	case 'nlapiSearchGlobal':
		units = 10;
		break;
	case 'nlapiSearchRecord':
		units = 10;
		break;
	case 'nlapiSendCampaignEmail':
		units = 10;
		break;
	case 'nlapiSendEmail':
		units = 10;
		break;
	case 'nlapiCreateRecord':
		units = 2;
		break;
	case 'nlapiCopyRecord':
		units = 2;
		break;
	case 'nlapiLookupField':
		units = 2;
		break;
	case 'nlapiLoadRecord':
		units = 2;
		break;
	case 'nlapiSubmitField':
		units = 2;
		break;
	case 'nlapiTransformRecord':
		units = 2;
		break;
	}

	return units;
}

function generatePutawayLocation(Item, PackCode, ItemStatus, UOM, ItemCube, ItemType,poLocn){
	var context = nlapiGetContext();
	nlapiLogExecution('DEBUG','Remaining usage at the start',context.getRemainingUsage());
	var ItemFamily, ItemGroup, Location;
	var location_found = false;
	var LocArry = new Array();
	var OBLocGroup, IBLocGroup;
	var PickSeqNo, PutSeqNo, PutMethod,PutMethodName,PutRuleId,PutBinLoc, MergeFlag = 'F',Mixsku='F',DimentionCheck='F';
	nlapiLogExecution('DEBUG', 'ItemCube', ItemCube);//0
	nlapiLogExecution('DEBUG', 'ItemType', ItemType);
	var itemresults;

	var fields = ['custitem_item_family','custitem_item_group','custitem_ebizdefskustatus',
	              'custitem_item_info_1','custitem_item_info_2','custitem_item_info_3','custitem_ebizabcvelitem'];


	if(ItemCube==null||ItemCube==''||isNaN(ItemCube))
		ItemCube=0;

	var columns= nlapiLookupField('item',Item,fields);
	var ItemFamily = columns.custitem_item_family;
	var ItemGroup = columns.custitem_item_group;
	//var ItemStatus = columns.custitem_ebizdefskustatus;
	var ItemInfo1 = columns.custitem_item_info_1;
	var ItemInfo2 = columns.custitem_item_info_2;
	var ItemInfo3 = columns.custitem_item_info_3;
	var putVelocity=columns.custitem_ebizabcvelitem;

	nlapiLogExecution('DEBUG', 'Item', Item);
	nlapiLogExecution('DEBUG', 'ItemFamily', ItemFamily);
	nlapiLogExecution('DEBUG', 'ItemGroup', ItemGroup);
	nlapiLogExecution('DEBUG', 'ItemStatus', ItemStatus);
	nlapiLogExecution('DEBUG', 'ItemInfo1', ItemInfo1);
	nlapiLogExecution('DEBUG', 'ItemInfo2', ItemInfo2);
	nlapiLogExecution('DEBUG', 'ItemInfo3', ItemInfo3);
	nlapiLogExecution('DEBUG', 'putVelocity', putVelocity);
	nlapiLogExecution('DEBUG', 'poLocn', poLocn);

	var locgroupid = 0;
	var zoneid = 0;

	// if (ItemFamily != null || ItemFamily != "") {
	var columns = new Array();
	var i=0;
	var filters = new Array();
	columns[0] = new nlobjSearchColumn('formulanumeric');
	columns[0].setFormula("TO_NUMBER({custrecord_sequencenumberpickrule})");
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
	columns[2] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
	columns[3] = new nlobjSearchColumn('custrecord_putawaymethod');
	columns[4] = new nlobjSearchColumn('custrecord_ruleidpickrule');
	columns[5] = new nlobjSearchColumn('custrecord_manual_location_generation');
	columns[6] = new nlobjSearchColumn('custrecord_locationpickrule');

	var ItemFamArr = new Array();
	ItemFamArr[0] = ItemFamily;
	ItemFamArr[1] = '@NONE@';

	if(ItemFamily!=null && ItemFamily != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skufamilypickrule', null, 'anyof', ['@NONE@', ItemFamily]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_skufamilypickrule', null, 'anyof', ['@NONE@']));
	}

	if(ItemGroup!=null && ItemGroup != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skugrouppickrule', null, 'anyof', ['@NONE@', ItemGroup]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_skugrouppickrule', null, 'anyof', ['@NONE@']));
	}

	if(ItemStatus!=null && ItemStatus != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skustatuspickrule', null, 'anyof', ['@NONE@', ItemStatus]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_skustatuspickrule', null, 'anyof', ['@NONE@']));
	}

	if(ItemInfo1!=null && ItemInfo1 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo1pickrule', null, 'anyof',['@NONE@', ItemInfo1]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo1pickrule', null, 'anyof', ['@NONE@']));
	}

	if(ItemInfo2!=null && ItemInfo2 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo2pickrule', null, 'anyof',['@NONE@', ItemInfo2]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo2pickrule', null, 'anyof', ['@NONE@']));
	}

	if(ItemInfo3!=null && ItemInfo3 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo3pickrule', null, 'anyof',['@NONE@', ItemInfo3]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo3pickrule', null, 'anyof', ['@NONE@']));
	}

	if(Item != null && Item != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skupickrule', null, 'anyof', ['@NONE@', Item]));		
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_skupickrule', null, 'anyof', ['@NONE@']));
	}

	if(putVelocity != null && putVelocity != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_abcvelpickrule', null, 'anyof', ['@NONE@', putVelocity]));
	}

	if(poLocn != null && poLocn != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizsitepickput', null, 'anyof', [poLocn]));
	}

	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var t1 = new Date();
	var rulesearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters, columns);
	var t2 = new Date();
	nlapiLogExecution('DEBUG', 'nlapiSearchRecord:GetPutawayLocation:putaway_rule',
			getElapsedTimeDuration(t1, t2));

	if (rulesearchresults != null) {
		nlapiLogExecution('DEBUG', 'rules count', rulesearchresults.length);
		for (var s = 0; s < rulesearchresults.length; s++) {
			locgroupid = rulesearchresults[s].getValue('custrecord_locationgrouppickrule');
			zoneid = rulesearchresults[s].getValue('custrecord_putawayzonepickrule');
			PutMethod = rulesearchresults[s].getValue('custrecord_putawaymethod');
			PutMethodName = rulesearchresults[s].getText('custrecord_putawaymethod');
			PutRuleId = rulesearchresults[s].getValue('custrecord_ruleidpickrule');
			PutBinLoc = rulesearchresults[s].getValue('custrecord_locationpickrule');
			nlapiLogExecution('DEBUG', 'Put Rule', PutRuleId);
			nlapiLogExecution('DEBUG', 'Put Method', PutMethod);
			nlapiLogExecution('DEBUG', 'PutBinLoc', PutBinLoc);
			nlapiLogExecution('DEBUG', 'Location Group Id from Putaway Rule', locgroupid);

			// To indicate if the location has to be generated manually based on the parameter in putaway strategy
			ManualLocationGeneration = rulesearchresults[s].getValue('custrecord_manual_location_generation');
			nlapiLogExecution('DEBUG', 'Manual Location Generation', ManualLocationGeneration);

			if (ManualLocationGeneration == 'F') {
				if (locgroupid != 0 && locgroupid != null) {
					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_startingputseqno');
					columns[0].setSort();
					columns[1] = new nlobjSearchColumn('name');
					columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid');
					columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno');
					columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid');
					columns[5] = new nlobjSearchColumn('custrecord_remainingcube');	
					columns[6] = new nlobjSearchColumn('internalid');
					columns[6].setSort();

					nlapiLogExecution('DEBUG', 'locgroupid', locgroupid);

					var filters = new Array();
					filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [locgroupid]));	
					filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
					if(poLocn != null && poLocn != "")
						filters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof', [poLocn]));
					if(PutBinLoc != null && PutBinLoc != "")
						filters.push(new nlobjSearchFilter('internalid', null, 'is', PutBinLoc));

					t1 = new Date();
					var locResults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);
					t2 = new Date();
					nlapiLogExecution('DEBUG', 'nlapiSearchRecord:GetPutawayLocation:location',
							getElapsedTimeDuration(t1, t2));

					nlapiLogExecution('DEBUG', 'locResults', locResults);
					if (locResults != null) {
						nlapiLogExecution('DEBUG', 'ifnot ');
						for (var u = 0; u < locResults.length; u++) {

							Location = locResults[u].getValue('name');
							var locationIntId = locResults[u].getId();

							nlapiLogExecution('DEBUG', 'Location', Location);
							nlapiLogExecution('DEBUG', 'Location Id', locationIntId);

							//Commented by satish.N on 04-JAN-2012 as the search results will give the same value.
							//LocRemCube = GeteLocCube(locationIntId);
							LocRemCube = locResults[u].getValue('custrecord_remainingcube');							
							OBLocGroup = locResults[u].getValue('custrecord_outboundinvlocgroupid');
							PickSeqNo  = locResults[u].getValue('custrecord_startingpickseqno');
							IBLocGroup = locResults[u].getValue('custrecord_inboundinvlocgroupid');
							PutSeqNo   = locResults[u].getValue('custrecord_startingputseqno');

							nlapiLogExecution('DEBUG', 'LocRemCube', LocRemCube);
							nlapiLogExecution('DEBUG', 'ItemCube', ItemCube);

							if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {
								RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);
								location_found = true;

								LocArry[0] = Location;
								LocArry[1] = RemCube;
								LocArry[2] = locationIntId;	
								LocArry[3] = OBLocGroup;
								LocArry[4] = PickSeqNo;
								LocArry[5] = IBLocGroup;
								LocArry[6] = PutSeqNo;
								LocArry[7] = LocRemCube;
								LocArry[8] = zoneid;
								LocArry[9] = PutMethodName;
								LocArry[10] = PutRuleId;

								nlapiLogExecution('DEBUG', 'SA-Location', Location);
								nlapiLogExecution('DEBUG', 'Location Internal Id',locationIntId);
								nlapiLogExecution('DEBUG', 'Sa-Location Cube', RemCube);
								return LocArry;
							}

						}
					}
				}
				else {
					nlapiLogExecution('DEBUG', 'Fetching Location Group from Zone', zoneid);
					var mixarray =  new Array();
					if(PutMethod!=null && PutMethod!='')
						mixarray=GetMergeFlag(PutMethod);
					if(mixarray!=null && mixarray!='' && mixarray.length>0)
					{
						MergeFlag = mixarray[0][0];
						Mixsku = mixarray[0][1];
						DimentionCheck=mixarray[0][2];
					}
					nlapiLogExecution('DEBUG', 'Merge Flag', MergeFlag);
					nlapiLogExecution('DEBUG', 'Mix SKU', Mixsku);
					nlapiLogExecution('DEBUG', 'DimentionCheck', DimentionCheck);

					if (zoneid != null && zoneid != ""){ 
						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_zone_seq');
						columns[0].setSort();
						columns[1] = new nlobjSearchColumn('custrecord_locgroup_no');

						var filters = new Array();
						filters.push(new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'is', zoneid));
						nlapiLogExecution('DEBUG','poLocn',poLocn);
						filters.push(new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', ['@NONE@', poLocn]));
						filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
						t1 = new Date();
						var searchresults = nlapiSearchRecord('customrecord_zone_locgroup', null, filters, columns);
						t2 = new Date();
						nlapiLogExecution('DEBUG', 'nlapiSearchRecord:GetPutawayLocation:zone_locgroup',
								getElapsedTimeDuration(t1, t2));

						var RemCube = 0;
						var LocRemCube = 0;

						if (searchresults != null && searchresults != '') 
						{

							for (var i = 0; i < Math.min(30, searchresults.length); i++) {
								locgroupid = searchresults[i].getValue('custrecord_locgroup_no');
								nlapiLogExecution('DEBUG', 'Fetched Location Group Id from Putaway Zone', locgroupid);

								if (MergeFlag == 'T') {
									nlapiLogExecution('DEBUG', 'Inside Merge ');
									var locResults;
									nlapiLogExecution('DEBUG', 'ItemCube', ItemCube);
									nlapiLogExecution('DEBUG', 'poLocn', poLocn);
									nlapiLogExecution('DEBUG', 'locgroupid', locgroupid);

									try {
										var columns = new Array();
										columns[0] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');
										columns[0].setSort();
										columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
										columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc');
										columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno', 'custrecord_ebiz_inv_binloc');
										columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc');
										columns[5] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');
										columns[6] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
										columns[7] = new nlobjSearchColumn('custrecord_remainingcube','custrecord_ebiz_inv_binloc');

										var filters = new Array();
										if(Mixsku!='T')
										{
											filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', Item));
										}

										if(poLocn!=null && poLocn!='')
											filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [poLocn]));
										filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'is', locgroupid));
										filters.push(new nlobjSearchFilter('custrecord_remainingcube', 'custrecord_ebiz_inv_binloc', 'greaterthanorequalto', ItemCube));                                        
										filters.push(new nlobjSearchFilter('isinactive', 'custrecord_ebiz_inv_binloc', 'is', 'F'));
										filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F')); 
										t1 = new Date();
										locResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
										t2 = new Date();
										nlapiLogExecution('DEBUG', 'nlapiSearchRecord:GetPutawayLocation:createinv',
												getElapsedTimeDuration(t1, t2));
									} //try close.
									catch (Error) {
										nlapiLogExecution('DEBUG', 'Into Error', Error);
									}
									try {
										nlapiLogExecution('DEBUG', 'Inside Try', 'TRY');

										if (locResults != null && locResults != '') {
											nlapiLogExecution('DEBUG', 'LocationResults', locResults.length );
											for (var j = 0; j < locResults.length; j++) {
												nlapiLogExecution('DEBUG', 'locResults[j].getId()', locResults[j].getId());
												Location = locResults[j].getText('custrecord_ebiz_inv_binloc');
												var locationInternalId = locResults[j].getValue('custrecord_ebiz_inv_binloc');

												nlapiLogExecution('DEBUG', 'Location', Location );
												nlapiLogExecution('DEBUG', 'Location Id', locationInternalId);

												//LocRemCube = GeteLocCube(locationInternalId);
												LocRemCube = locResults[j].getValue('custrecord_remainingcube', 'custrecord_ebiz_inv_binloc');
												OBLocGroup = locResults[j].getValue('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc');
												PickSeqNo = locResults[j].getValue('custrecord_startingpickseqno', 'custrecord_ebiz_inv_binloc');
												IBLocGroup = locResults[j].getValue('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc');
												PutSeqNo = locResults[j].getValue('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');

												nlapiLogExecution('DEBUG', 'Loc Rem Cube', LocRemCube);
												nlapiLogExecution('DEBUG', 'Item Cube', ItemCube);

												//if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {
												if ((parseFloat(LocRemCube) >= parseFloat(ItemCube)) || DimentionCheck=='F') {

													RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);

													location_found = true;

													LocArry[0] = Location;
													LocArry[1] = RemCube;
													LocArry[2] = locationInternalId;		//locResults[j].getId();
													LocArry[3] = OBLocGroup;
													LocArry[4] = PickSeqNo;
													LocArry[5] = IBLocGroup;
													LocArry[6] = PutSeqNo;
													LocArry[7] = LocRemCube;
													LocArry[8] = zoneid;
													LocArry[9] = PutMethodName;
													LocArry[10] = PutRuleId;

													nlapiLogExecution('DEBUG', 'after changes Location Cube', RemCube);
													return LocArry;
												}
											}
										}
									} 
									catch (exps) {
										nlapiLogExecution('DEBUG', 'included exps', exps);
									}

									//Added by satish.N on 04-JAN-2012 to consider open taks while merging.
									if (location_found == false) {

										try{
											nlapiLogExecution('DEBUG', 'Inside  consider open taks while merging');

											var columns = new Array();
											columns[0] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_actbeginloc');
											columns[0].setSort();
											columns[1] = new nlobjSearchColumn('custrecord_actbeginloc');
											columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid', 'custrecord_actbeginloc');
											columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno', 'custrecord_actbeginloc');
											columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid', 'custrecord_actbeginloc');
											columns[5] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_actbeginloc');
											columns[6] = new nlobjSearchColumn('custrecord_sku');
											columns[7] = new nlobjSearchColumn('custrecord_remainingcube','custrecord_actbeginloc');

											var filters = new Array();
											if(Mixsku!='T')
											{
												filters.push(new nlobjSearchFilter('custrecord_sku', null, 'is', Item));
											}	
											if(poLocn!=null && poLocn!='')
												filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', [poLocn]));
											filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
											filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_actbeginloc', 'is', locgroupid));
											filters.push(new nlobjSearchFilter('custrecord_remainingcube', 'custrecord_actbeginloc', 'greaterthanorequalto', ItemCube));

											locResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

											if (locResults != null && locResults != '') {
												for (var j = 0; j < locResults.length; j++) {

													Location = locResults[j].getText('custrecord_actbeginloc');
													var locationInternalId = locResults[j].getValue('custrecord_actbeginloc');

													nlapiLogExecution('DEBUG', 'Location', Location );
													nlapiLogExecution('DEBUG', 'Location Id', locationInternalId);

													LocRemCube = locResults[j].getValue('custrecord_remainingcube', 'custrecord_actbeginloc');
													OBLocGroup = locResults[j].getValue('custrecord_outboundlocgroupid', 'custrecord_actbeginloc');
													PickSeqNo = locResults[j].getValue('custrecord_startingpickseqno', 'custrecord_actbeginloc');
													IBLocGroup = locResults[j].getValue('custrecord_inboundlocgroupid', 'custrecord_actbeginloc');
													PutSeqNo = locResults[j].getValue('custrecord_startingputseqno', 'custrecord_actbeginloc');

													nlapiLogExecution('DEBUG', 'Loc Rem Cube', LocRemCube);
													nlapiLogExecution('DEBUG', 'Item Cube', ItemCube);

													//if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {
													if ((parseFloat(LocRemCube) >= parseFloat(ItemCube)) || DimentionCheck=='F') {

														RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);

														location_found = true;

														LocArry[0] = Location;
														LocArry[1] = RemCube;
														LocArry[2] = locationInternalId;		//locResults[j].getId();
														LocArry[3] = OBLocGroup;
														LocArry[4] = PickSeqNo;
														LocArry[5] = IBLocGroup;
														LocArry[6] = PutSeqNo;
														LocArry[7] = LocRemCube;
														LocArry[8] = zoneid;
														LocArry[9] = PutMethodName;
														LocArry[10] = PutRuleId;

														nlapiLogExecution('DEBUG', 'after changes Location Cube', RemCube);
														return LocArry;
													}
												}										
											}
										}
										catch(exps){
											nlapiLogExecution('DEBUG', 'included exps in opentasks', exps);
										}
									}
								}
								if (location_found == false) {

//									if (locResults != null && locResults != '') {
//									nlapiLogExecution('DEBUG', 'locResults.length', locResults.length);
//									for (var j = 0; j < Math.min(300, locResults.length); j++) {

//									var emptyBinLocationId = locResults[j].getId();
//									nlapiLogExecution('DEBUG', 'emptyBinLocationId', emptyBinLocationId);
//									var emptyloccheck =	binLocationCheck(emptyBinLocationId);
//									nlapiLogExecution('DEBUG', 'emptyloccheck', emptyloccheck);

//									if(emptyloccheck == 'N'){




									//code modified by suman on 27/01/12
									//Instead of searching each and individual binloc in inventory and open task record it seems to have a goverence issue.
									//So I bought all binloc from open task and inventory records depending upon "locgr"Id and placing it in an array so that on comparing this array with the binlocation 
									//will decides weather it is empty or not.

									nlapiLogExecution('DEBUG', 'Location Not found for Merge Location for Location Group', locgroupid);
									var locResults = GetLocation(locgroupid,ItemCube);

									if (locResults != null && locResults != '') {
										var TotalListOfBinLoc=new Array();
										for ( var x = 0; x < locResults.length; x++) 
										{
											TotalListOfBinLoc[x]=locResults[x].getId();

										}

										//The below changes done by Satish.N on 29OCT2013 - Case # 20124515
										var emptyloccheck =	binLocationChecknew1(TotalListOfBinLoc);
										nlapiLogExecution('DEBUG', 'emptyloccheck', emptyloccheck);
										// Upto here on 29OCT2013 - Case # 20124515

										nlapiLogExecution('DEBUG', 'locResults.length', locResults.length);
										for (var j = 0; j < Math.min(300, locResults.length); j++) {
											var LocFlag='N';
											var emptyBinLocationId = locResults[j].getId();
											nlapiLogExecution('DEBUG', 'emptyBinLocationId', emptyBinLocationId);
//											var emptyloccheck =	binLocationCheck(emptyBinLocationId);
//											nlapiLogExecution('DEBUG', 'emptyloccheck', emptyloccheck);

											for ( var InvBinLocCount = 0; InvBinLocCount < emptyloccheck[0].length; InvBinLocCount++)
											{
//												nlapiLogExecution('DEBUG', 'emptyloccheck[0][InvBinLocCount]', emptyloccheck[0][InvBinLocCount]);

												if(emptyloccheck[0][InvBinLocCount]==emptyBinLocationId)
												{
													nlapiLogExecution('DEBUG', 'emptyBinLocationId+InvBinLocCount', emptyBinLocationId+','+emptyloccheck[0][InvBinLocCount]);
													LocFlag='Y';
													break;
												}
											}
											for ( var OpenTaskBinLocCount = 0; OpenTaskBinLocCount < emptyloccheck[1].length; OpenTaskBinLocCount++)
											{
												if(emptyloccheck[1][OpenTaskBinLocCount]==emptyBinLocationId)
												{
													nlapiLogExecution('DEBUG', 'emptyBinLocationId+OpenTaskBinLocCount', emptyBinLocationId+','+emptyloccheck[1][OpenTaskBinLocCount]);
													LocFlag='Y';
													break;
												}
											}
//											end of code changed 27/01/12 
											nlapiLogExecution('DEBUG', 'LocFlag', LocFlag);
											nlapiLogExecution('DEBUG', 'Main Loop ', s);
											nlapiLogExecution('DEBUG','Remaining usage ',context.getRemainingUsage());
											if(LocFlag == 'N'){
												//Changes done by Sarita (index i is changed to j).
												LocRemCube = locResults[j].getValue('custrecord_remainingcube');
												OBLocGroup = locResults[j].getValue('custrecord_outboundlocgroupid');
												PickSeqNo = locResults[j].getValue('custrecord_startingpickseqno');
												IBLocGroup = locResults[j].getValue('custrecord_inboundlocgroupid');
												PutSeqNo = locResults[j].getValue('custrecord_startingputseqno');

												nlapiLogExecution('DEBUG', 'LocRemCube', LocRemCube);
												nlapiLogExecution('DEBUG', 'ItemCube', ItemCube);

												//if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {
												if ((parseFloat(LocRemCube) >= parseFloat(ItemCube)) || DimentionCheck=='F') {
													RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);
													location_found = true;
													Location = locResults[j].getValue('name');
													LocArry[0] = Location;
													LocArry[1] = RemCube;
													LocArry[2] = locResults[j].getId();
													LocArry[3] = OBLocGroup;
													LocArry[4] = PickSeqNo;
													LocArry[5] = IBLocGroup;
													LocArry[6] = PutSeqNo;
													LocArry[7] = LocRemCube;
													LocArry[8] = zoneid;
													LocArry[9] = PutMethodName;
													LocArry[10] = PutRuleId;

													nlapiLogExecution('DEBUG', 'Location', Location);
													nlapiLogExecution('DEBUG', 'Location Cube', RemCube);
													return LocArry;
													break;
												}
											}
										}
									}
								}
							}
						}//end of searchresult null checking
					}
				}
				nlapiLogExecution('DEBUG', 'Main Loop ', s);
				nlapiLogExecution('DEBUG','Remaining usage ',context.getRemainingUsage());
			}
		}
	}// end of put rule for loop
	else
	{
		nlapiLogExecution('DEBUG', 'No Searchresults with above mentioned criteria ', 'Null Results');
	}
	//  } // end of main if
	return LocArry;
}

function binLocationChecknew1(emptyBinLocationId)
{

	var binLocationCheckResult = 'Y';
	var inventoryColumns = new Array();
	inventoryColumns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	inventoryColumns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');

	var inventoryFilters = new Array();
	if(emptyBinLocationId!=null && emptyBinLocationId!="")
		inventoryFilters[0] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', emptyBinLocationId);

	inventoryItemSearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, inventoryFilters, inventoryColumns );
	nlapiLogExecution('DEBUG', 'GetLocation:inventoryItemSearchResults',inventoryItemSearchResults);

	var InvtBinLoc=new Array();
	if(inventoryItemSearchResults !=null && inventoryItemSearchResults !="")
	{
		for ( var count = 0; count < inventoryItemSearchResults.length; count++) {
			InvtBinLoc[count]=inventoryItemSearchResults[count].getValue('custrecord_ebiz_inv_binloc');
		}
	}
	else
	{
		InvtBinLoc[0]=-1;
	}

	var taskColumns = new Array();
	taskColumns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
	taskColumns[1] = new nlobjSearchColumn('custrecord_sku');

	var taskFilters = new Array();
	if(emptyBinLocationId!=null && emptyBinLocationId!="")
		taskFilters[0] = new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', emptyBinLocationId);

	var taskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, taskFilters, taskColumns);
	nlapiLogExecution('DEBUG', 'taskSearchResults',taskSearchResults);
	var OpenTaskBinLoc=new Array();
	if(taskSearchResults!=null && taskSearchResults!="")
	{
		for ( var count = 0; count < taskSearchResults.length; count++) 
		{
			OpenTaskBinLoc[count]=taskSearchResults[count].getValue('custrecord_actbeginloc');

		}
	}
	else
	{
		OpenTaskBinLoc[0]=-1;
	}


	var TotalBinLoc=new Array();
	TotalBinLoc[0]=InvtBinLoc;
	TotalBinLoc[1]=OpenTaskBinLoc;
	nlapiLogExecution('DEBUG', 'TotalBinLoc[0]',TotalBinLoc[0]);
	nlapiLogExecution('DEBUG', 'TotalBinLoc[1]',TotalBinLoc[1]);
	return TotalBinLoc;
//	if (inventoryItemSearchResults == null)
//	{
//	nlapiLogExecution('DEBUG', 'GetLocation:inventoryItemSearchResults','here');
//	var taskColumns = new Array();
//	taskColumns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
//	taskColumns[1] = new nlobjSearchColumn('custrecord_sku');

//	var taskFilters = new Array();
//	taskFilters[0] = new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', emptyBinLocationId);
//	taskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, taskFilters, taskColumns);
//	if (taskSearchResults == null){
//	binLocationCheckResult = 'N';
//	}
//	else{
//	binLocationCheckResult = 'Y';
//	}
//	}
//	else
//	{
//	binLocationCheckResult = 'Y';
//	}
//	return binLocationCheckResult;
}
