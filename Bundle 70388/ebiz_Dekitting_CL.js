
/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inventory/Client/Attic/ebiz_Dekitting_CL.js,v $
 *     	   $Revision: 1.1.2.1.4.1.4.1 $
 *     	   $Date: 2014/06/05 15:14:39 $
 *     	   $Author: sponnaganti $
 *     	   $Name:  $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_Dekitting_CL.js,v $
 * Revision 1.1.2.1.4.1.4.1  2014/06/05 15:14:39  sponnaganti
 * case# 20148764 20148765
 * Stnd Bundle Issue Fix
 *
 * Revision 1.1.2.1.4.1  2012/11/01 14:54:56  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.1.2.1  2012/05/14 12:03:16  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Picking Stageloc
 *
 * Revision 1.2.2.15  2012/05/07 09:54:38  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.2.2.14  2012/05/04 22:52:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.2.2.13  2012/04/20 22:52:20  gkalla
 * CASE201112/CR201113/LOG201121
 * To resolve selected into wave
 *
 * Revision 1.2.2.12  2012/04/20 14:30:50  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.2.2.11  2012/04/17 08:40:17  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pool of records
 *
 * Revision 1.2.2.10  2012/03/20 20:42:32  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Moved from Trunk to Branch
 *
 * Revision 1.14  2012/03/20 18:05:54  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Wave Creation by sales order header
 *
 * Revision 1.13  2012/03/16 10:01:52  schepuri
 * CASE201112/CR201113/LOG201121
 * code merge from 1.2.2.9
 *
 * Revision 1.12  2012/03/16 06:34:16  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Fine tunned
 *
 * Revision 1.11  2012/03/14 07:07:41  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Report changes  and undefined container lp issue for TPP
 *
 * Revision 1.10  2012/03/14 07:06:01  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Report changes  and undefined container lp issue for TPP
 *
 * Revision 1.9  2012/03/13 12:31:06  schepuri
 * CASE201112/CR201113/LOG201121
 * code merge from 1.2.2.6
 *
 * Revision 1.8  2012/03/09 10:47:01  rmukkera
 * CASE201112/CR201113/LOG201121
 * added wmscarrier for to
 *
 * Revision 1.7  2012/03/09 08:30:40  rmukkera
 * CASE201112/CR201113/LOG201121
 * Paging functionality added newly.
 *
 * Revision 1.6  2012/03/02 01:11:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.5  2012/02/23 17:58:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.4  2012/02/01 15:10:30  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Wave Creation User Event
 *
 * Revision 1.3  2012/01/27 06:53:03  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Generation UE
 *
 * Revision 1.2  2012/01/05 14:01:49  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code tunning
 *
 * Revision 1.1  2011/12/21 16:45:13  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Gen User Event
 *  
 *
 *****************************************************************************/
function OnSave(){
	var lineCnt = nlapiGetLineItemCount('custpage_items');
	var returnvar=true;var isselected=false;
	if(lineCnt!=-1)
	{
		for(var j=1;j<=lineCnt;j++)
		{
			var chkselect=nlapiGetLineItemValue('custpage_items','custpage_items_select', j);
			if(chkselect=='T')
			{
				isselected=true;
				var dekitqty=nlapiGetLineItemValue('custpage_items','custpage_items_dekitqty', j);

				if(dekitqty=='')
				{
					alert('Please enter dekitQty .');
					var avalqty=nlapiGetLineItemValue('custpage_items','custpage_items_qty', j);
					nlapiSetLineItemValue('custpage_items','custpage_items_dekitqty', j,avalqty);
					nlapiSetLineItemValue('custpage_items','custpage_items_select', j,'F');
					returnvar=false;
				}
				else if(isNaN(dekitqty))
				{
					alert('Please enter dekitQty as number.');
					var avalqty=nlapiGetLineItemValue('custpage_items','custpage_items_qty', j);
					nlapiSetLineItemValue('custpage_items','custpage_items_dekitqty', j,avalqty);
					nlapiSetLineItemValue('custpage_items','custpage_items_select', j,'F');
					returnvar=false;
				}
				else if(parseFloat(nlapiGetLineItemValue('custpage_items','custpage_items_dekitqty', j))>parseFloat(nlapiGetLineItemValue('custpage_items','custpage_items_qty', j)))
				{
					alert(' dekitQty should be lessthan Avialable qty.');
					var avalqty=nlapiGetLineItemValue('custpage_items','custpage_items_qty', j);
					nlapiSetLineItemValue('custpage_items','custpage_items_dekitqty', j,avalqty);
					nlapiSetLineItemValue('custpage_items','custpage_items_select', j,'F');
					returnvar=false;
				}
				else if(dekitqty<0)
				{
					alert(' dekitQty should not be negative.');
					var avalqty=nlapiGetLineItemValue('custpage_items','custpage_items_qty', j);
					nlapiSetLineItemValue('custpage_items','custpage_items_dekitqty', j,avalqty);
					nlapiSetLineItemValue('custpage_items','custpage_items_select', j,'F');
					returnvar=false;
				}
				else
				{

					returnvar=true;

				}

			}
			else
			{
				if(isselected!=true)
					returnvar=false;
			}
		}
	}

	if(returnvar==false && isselected==false)
	{
		alert('please select atlease one Item');
	}
	return returnvar;
}