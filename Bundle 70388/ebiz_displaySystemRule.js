/***************************************************************************
������������������������������� � ����������������������������� 
 eBizNET Solutions
****************************************************************************
*
*� $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Masters/Suitelet/ebiz_displaySystemRule.js,v $
*� $Revision: 1.1.14.1 $
*� $Date: 2013/03/19 12:08:11 $
*� $Author: schepuri $
*� $Name: t_NSWMS_2013_1_3_9 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_displaySystemRule.js,v $
*� Revision 1.1.14.1  2013/03/19 12:08:11  schepuri
*� CASE201112/CR201113/LOG201121
*� change url path
*�
*� Revision 1.1  2011/12/28 07:33:09  spendyala
*�  CASE201112/CR201113/LOG201121
*�  system rules
*�
*
****************************************************************************/

/**
 * Display the system rules
 * one can edit and insert a new record into the system rule custom record
 * @param request
 * @param response
 */
function ShowSystemRules(request, response)
{
	if (request.getMethod() == 'GET') 
	{
		nlapiLogExecution('ERROR','Method ','Get');
		nlapiLogExecution('ERROR','event',request.getParameter('evt'));
		var mode="";
		if(request.getParameter('evt') != null && request.getParameter('evt') != "")
			mode=request.getParameter('evt');

		//display records in form of grid
		if(mode=="")
		{
			var form = nlapiCreateForm('System Rule List');
			var flagField=form.addField('flag','text','FLAG').setDisplayType('hidden');
			flagField.setDefaultValue('1');
			var sublist = form.addSubList('custpage_rules', 'list', 'SysRule List');
			sublist.addField("custpage_location", "select", "Location","Location").setDisplayType('inline');
			sublist.addField("custpage_company", "select", "Company","customrecord_ebiznet_company").setDisplayType('inline');
			sublist.addField("custpage_processtype", "select", "Process Type","customlist_ebiz_process_type").setDisplayType('inline');
			sublist.addField("custpage_tasktype", "select", "Task Type", "customrecord_ebiznet_tasktype").setDisplayType('inline');
			sublist.addField("custpage_defaultid", "text", "Rule ID");
			sublist.addField("custpage_description", "text", "Description").setDisplayType('inline');
			sublist.addField("custpage_ruletype", "text", "Rule Type").setDisplayType('inline');
			sublist.addField("custpage_rulevalue", "text", "Rule Value","customlist_ebiznet_rule_value");
			sublist.addField("custpage_internalid","text","internalid").setDisplayType('hidden');
			sublist.addField("custpage_edit_link","text","");


			var column=new Array();
			column[0]=new nlobjSearchColumn('custrecord_ebizsite');
			column[1]=new nlobjSearchColumn('custrecord_ebizcomp');
			column[2]=new nlobjSearchColumn('custrecord_ebizprocesstype');
			column[3]=new nlobjSearchColumn('custrecord_ebiztasktype');
			column[4]=new nlobjSearchColumn('name');
			column[5]=new nlobjSearchColumn('custrecord_ebizdescription');
			column[6]=new nlobjSearchColumn('custrecord_ebizruletype');
			column[7]=new nlobjSearchColumn('custrecord_ebizrulevalue');
			var searchrecord=nlapiSearchRecord('customrecord_ebiznet_sysrules', null, null, column);
			if(searchrecord!=null&&searchrecord!='')
			{
				for (var i=0;i<searchrecord.length;i++)
				{
					var id=searchrecord[i].getId();
					var name=searchrecord[i].getValue('name');
					form.getSubList('custpage_rules').setLineItemValue('custpage_location', 1+i, searchrecord[i].getValue('custrecord_ebizsite'));
					form.getSubList('custpage_rules').setLineItemValue('custpage_company', 1+i, searchrecord[i].getValue('custrecord_ebizcomp'));
					form.getSubList('custpage_rules').setLineItemValue('custpage_processtype', 1+i, searchrecord[i].getValue('custrecord_ebizprocesstype'));
					form.getSubList('custpage_rules').setLineItemValue('custpage_tasktype', 1+i, searchrecord[i].getValue('custrecord_ebiztasktype'));
					form.getSubList('custpage_rules').setLineItemValue('custpage_defaultid', 1+i, name);
					form.getSubList('custpage_rules').setLineItemValue('custpage_description', 1+i, searchrecord[i].getValue('custrecord_ebizdescription'));
					form.getSubList('custpage_rules').setLineItemValue('custpage_ruletype', 1+i, searchrecord[i].getValue('custrecord_ebizruletype'));
					form.getSubList('custpage_rules').setLineItemValue('custpage_rulevalue', 1+i, searchrecord[i].getValue('custrecord_ebizrulevalue').toString());
					form.getSubList('custpage_rules').setLineItemValue('custpage_internalid', 1+i, id);
				}
				systemRuleAddLink('view', form, request);
			}
			var button = form.addSubmitButton('New');
			response.writePage(form);
		}

		else
		{
			var intId=request.getParameter('internalId');
			var form = nlapiCreateForm('System Rule List');
			var flagField=form.addField('flag','text','FLAG').setDisplayType('hidden');
			flagField.setDefaultValue('2');
			form.setScript('customscript_systemrule_cl');
			var sublist = form.addSubList('custpage_rules', 'list', 'SysRule List');
//			var location = sublist.addField("custpage_location", "select", "Location").setDisplayType('inline');
			sublist.addField("custpage_location", "text", "Location").setDisplayType('inline');

//			var company = sublist.addField("custpage_company", "select", "Company").setDisplayType('inline');
			sublist.addField("custpage_company", "text", "Company").setDisplayType('inline');

			sublist.addField("custpage_processtype", "select", "Process Type","customlist_ebiz_process_type").setDisplayType('inline');
			sublist.addField("custpage_tasktype", "select", "Task Type", "customrecord_ebiznet_tasktype").setDisplayType('inline');
//			var defaultid=sublist.addField("custpage_defaultid", "select", "Rule ID").setDisplayType('inline');
			sublist.addField("custpage_defaultid", "text", "Rule ID").setDisplayType('inline');

			sublist.addField("custpage_description", "text", "Description").setDisplayType('inline');
			sublist.addField("custpage_ruletype", "text", "Rule Type").setDisplayType('inline');
			var ruleValue=sublist.addField("custpage_rulevalue", "select", "Rule Value");
			sublist.addField("custpage_internalid","text","internalid").setDisplayType('hidden');
			sublist.addField("custpage_id","text","Id").setDisplayType('hidden');

			var filter=new Array();
			filter.push(new nlobjSearchFilter('internalId', null,'is',intId));

			var column=new Array();
			column[0]=new nlobjSearchColumn('custrecord_ebizsite').setSort(true);
			column[1]=new nlobjSearchColumn('custrecord_ebizcomp');
			column[2]=new nlobjSearchColumn('custrecord_ebizprocesstype');
			column[3]=new nlobjSearchColumn('custrecord_ebiztasktype');
			column[4]=new nlobjSearchColumn('name');
			column[5]=new nlobjSearchColumn('custrecord_ebizdescription');
			column[6]=new nlobjSearchColumn('custrecord_ebizruletype');
			column[7]=new nlobjSearchColumn('custrecord_ebizrulevalue');
			column[8]=new nlobjSearchColumn('internalId').setSort();
			column[9]=new nlobjSearchColumn('custrecord_ebizruleval_refid');

			var searchrecord=nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filter, column);

			if(searchrecord!=null&&searchrecord!='')
			{
				var id=searchrecord[0].getId();
				var name=searchrecord[0].getValue('name');
				if(searchrecord[0].getValue('custrecord_ebizsite')== "" && searchrecord[0].getValue('custrecord_ebizcomp')== "")
				{
					showInlineMessage(form, 'Error', 'User Unauthorised to Edit this record', "");
					response.writePage(form);
				}
				else
				{
					form.getSubList('custpage_rules').setLineItemValue('custpage_location', 1, searchrecord[0].getText('custrecord_ebizsite'));
					form.getSubList('custpage_rules').setLineItemValue('custpage_company', 1, searchrecord[0].getText('custrecord_ebizcomp'));
					form.getSubList('custpage_rules').setLineItemValue('custpage_processtype', 1, searchrecord[0].getValue('custrecord_ebizprocesstype'));
					form.getSubList('custpage_rules').setLineItemValue('custpage_tasktype', 1, searchrecord[0].getValue('custrecord_ebiztasktype'));
					form.getSubList('custpage_rules').setLineItemValue('custpage_defaultid', 1, name);
					form.getSubList('custpage_rules').setLineItemValue('custpage_description', 1, searchrecord[0].getValue('custrecord_ebizdescription'));
					form.getSubList('custpage_rules').setLineItemValue('custpage_ruletype', 1, searchrecord[0].getValue('custrecord_ebizruletype'));
					form.getSubList('custpage_rules').setLineItemValue('custpage_rulevalue', 1, searchrecord[0].getValue('custrecord_ebizrulevalue'));
					form.getSubList('custpage_rules').setLineItemValue('custpage_internalid', 1, name);
					form.getSubList('custpage_rules').setLineItemValue('custpage_id', 1, id);


					var filterRuleValues=new Array();
					filterRuleValues[0]=new nlobjSearchFilter('name', null, 'is', searchrecord[0].getText('custrecord_ebizruleval_refid'));
					var columnRuleValues=new Array();
					columnRuleValues[0]=new nlobjSearchColumn('custrecord_ebiznet_sysrule_refvalue').setSort();

					var searchresult=nlapiSearchRecord('customrecord_ebiznet_sysrulevalues', null, filterRuleValues, columnRuleValues);

					ruleValue.addSelectOption('','');
					for ( var count = 0; count < searchresult.length; count++) 
					{
						ruleValue.addSelectOption(searchresult[count].getValue('custrecord_ebiznet_sysrule_refvalue'),searchresult[count].getValue('custrecord_ebiznet_sysrule_refvalue'));
					}
					/*
					//get all the default id
					defaultid.addSelectOption('','');
					for(var count=0;count<searchrecord.length;count++)
					{
						defaultid.addSelectOption(searchrecord[count].getId(),searchrecord[count].getValue('name'));
					}


					//Get the Location-records whose make warehouse site is said to T
					location.addSelectOption('','');
					var filterLocation = new Array();
					filterLocation.push(new nlobjSearchFilter('custrecord_ebizwhsite', null, 'is', 'T'));
					var columnLocation = new Array();
					columnLocation[0] = new nlobjSearchColumn('name');
					var searchLocationRecord = nlapiSearchRecord('Location', null, filterLocation, columnLocation);
					for(var count=0;count < searchLocationRecord.length ;count++)
					{
						location.addSelectOption(searchLocationRecord[count].getId(),searchLocationRecord[count].getValue('name'));

					}


					//Get the Company_records whose status is active 
					var filterCompany = new Array();
					filterCompany.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
					var columnCompany = new Array();
					columnCompany[0] = new nlobjSearchColumn('name');
					var searchLocationRecord = nlapiSearchRecord('customrecord_ebiznet_company', null, filterCompany, columnCompany);
					company.addSelectOption('','');
					for(var count1=0;count1 < searchLocationRecord.length ;count1++)
					{
						company.addSelectOption(searchLocationRecord[count1].getId(),searchLocationRecord[count1].getValue('name'));
					}

					 */
					var button = form.addSubmitButton('Save');
					response.writePage(form);
				}
			}
		}
	}
	else
	{
		var flag=request.getParameter('flag');

		if(flag==1)
		{
			response.sendRedirect('SUITELET', 'customscript_create_new_sysrule', 'customdeploy_create_new_sysrule',
					false,'');
		}

		else
		{
			var intId=request.getLineItemValue('custpage_rules','custpage_id',1);
			nlapiSubmitField('customrecord_ebiznet_sysrules', intId, 'custrecord_ebizrulevalue', request.getLineItemValue('custpage_rules','custpage_rulevalue',1));
			response.sendRedirect('SUITELET', 'customscript_system_rules', 'customdeploy_system_rules', false,'');
		}
	}
}


function systemRuleAddLink(type, form, request)
{
//	obtain the context object
	//var ctx = nlapiGetContext();

	//resolve the generic relative URL for the Suitelet
	var systemRuleURL = nlapiResolveURL('SUITELET', 'customscript_system_rules', 'customdeploy_system_rules');

	//complete the URL as either for production or sandbox environment
	/*if (ctx.getEnvironment() == 'PRODUCTION') 
	{
		systemRuleURL = 'https://system.netsuite.com' + systemRuleURL;
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') 
		{
			systemRuleURL = 'https://system.sandbox.netsuite.com' + systemRuleURL;
		}*/

	//loop through the item sublist
	for (var i = 0; i < form.getSubList('custpage_rules').getLineItemCount(); i++) 
	{
		var linkURL;
		var lineno=i+1;
		//add the PO ID, item, and quantity to the URL
		linkURL = systemRuleURL + '&internalId=' + form.getSubList('custpage_rules').getLineItemValue('custpage_internalid', i + 1) ;
		linkURL = linkURL + '&evt=E';

		//log the URL
		nlapiLogExecution('ERROR', 'url', linkURL);

		//populate the URL into the  field as a hyperlink to the Suitelet
		form.getSubList('custpage_rules').setLineItemValue('custpage_edit_link', i + 1, '<a href="' + linkURL + '">Edit</a>');
	}
}


