/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/UserEvents/ebiz_Pickgeneration_UE.js,v $
<<<<<<< ebiz_Pickgeneration_UE.js
 *     	   $Revision: 1.2.2.52.4.14.2.85.2.10 $
 *     	   $Date: 2015/12/04 09:49:08 $
 *     	   $Author: grao $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
=======
 *     	   $Revision: 1.2.2.52.4.14.2.85.2.10 $
 *     	   $Date: 2015/12/04 09:49:08 $
 *     	   $Author: grao $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
>>>>>>> 1.2.2.52.4.14.2.82
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_Pickgeneration_UE.js,v $
 * Revision 1.2.2.52.4.14.2.85.2.10  2015/12/04 09:49:08  grao
 * 2015.2 Issue Fixes 201415825
 *
 * Revision 1.2.2.52.4.14.2.85.2.9  2015/12/01 15:25:17  grao
 * 2015.2 Issue Fixes 20145825
 *
 * Revision 1.2.2.52.4.14.2.85.2.8  2015/11/30 17:11:44  nneelam
 * case# 201415827 , 201415823
 *
 * Revision 1.2.2.52.4.14.2.85.2.7  2015/11/27 11:48:37  svanama
 * Case # 201415291
 *
 * Revision 1.2.2.52.4.14.2.85.2.6  2015/11/24 16:15:54  nneelam
 * case# 201415801
 *
 * Revision 1.2.2.52.4.14.2.85.2.5  2015/11/23 15:39:33  grao
 * 2015.2 Issue Fixes 201415810
 *
 * Revision 1.2.2.52.4.14.2.85.2.4  2015/11/09 15:45:11  mpragada
 * case# 201413537
 * UCC # not updating in LP Master
 *
 * Revision 1.2.2.52.4.14.2.85.2.3  2015/10/14 14:09:18  grao
 * 2015.2 Issue Fixes 201414817
 *
 * Revision 1.2.2.52.4.14.2.85.2.2  2015/10/06 10:16:51  gkalla
 * Case# 201414824
 * Added status of pick report same as wave status report. fetching status from scheduler status custom record.
 *
 * Revision 1.2.2.52.4.14.2.85.2.1  2015/09/23 14:57:40  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.2.2.52.4.14.2.85  2015/09/01 18:16:23  snimmakayala
 * no message
 *
 * Revision 1.2.2.52.4.14.2.84  2015/07/27 15:33:13  skreddy
 * Case# 201413638
 * PD SB issue fix
 *
 * Revision 1.2.2.52.4.14.2.83  2015/07/13 15:34:27  grao
 * 2015.2 ssue fixes  201413447
 *
 * Revision 1.2.2.52.4.14.2.82  2015/07/13 07:42:55  snimmakayala
 * Case#: 201413420
 * Wave List and Order List options.
 *
 * Revision 1.2.2.52.4.14.2.81  2015/04/13 09:25:59  rrpulicherla
 * Case#201412277
 *
 * Revision 1.2.2.52.4.14.2.80  2015/03/26 14:05:02  schepuri
 * case# 201412157
 *
 * Revision 1.2.2.52.4.14.2.79  2015/03/18 15:47:16  sponnaganti
 * Case# 201412069
 * LP SB issue fix
 *
 * Revision 1.2.2.52.4.14.2.78  2014/11/26 15:30:20  grao
 * Case# 201411130 Deal med Issue fixes
 *
 * Revision 1.2.2.52.4.14.2.77  2014/11/24 09:15:49  snimmakayala
 * Case#: 201411116
 * Issue: Multiple Clusters per Container LP
 *
 * Revision 1.2.2.52.4.14.2.67.2.10  2014/11/24 09:12:37  snimmakayala
 * Case#: 201411116
 * Issue: Multiple Clusters per Container LP
 *
 * Revision 1.2.2.52.4.14.2.67.2.9  2014/11/24 09:06:43  snimmakayala
 * no message
 *
 * Revision 1.2.2.52.4.14.2.67.2.8  2014/09/19 12:56:33  snimmakayala
 * Case: 201410288
 * Pick Strategies by Order Priority
 *
 * Revision 1.2.2.52.4.14.2.67.2.7  2014/09/13 02:25:06  snimmakayala
 * Case: 201410288
 * Pick Strategies by Order Priority
 *
 * Revision 1.2.2.52.4.14.2.67.2.6  2014/08/05 12:22:31  snimmakayala
 * Case: 20149802
 * Pick Rules by order type.
 *
 * Revision 1.2.2.52.4.14.2.67.2.5  2014/08/04 15:23:54  skreddy
 * case # 20149500
 * jawbone SB issue fix
 *
 * Revision 1.2.2.52.4.14.2.67.2.4  2014/08/01 15:09:50  grao
 * Case#: 20149789� and  20149788  �Standard bundel  issue fixes
 *
 * Revision 1.2.2.52.4.14.2.67.2.3  2014/07/25 13:50:57  grao
 * Case#: 20149664 Dealmed issue fixes
 *
 * Revision 1.2.2.52.4.14.2.67.2.2  2014/07/07 16:06:15  sponnaganti
 * case# 20149047
 * Comaptibility Issue fix
 *
 * Revision 1.2.2.52.4.14.2.67.2.1  2014/06/26 07:09:32  sponnaganti
 * Case# 20149102
 * Stnd Bundle Issue fix
 *
 * Revision 1.2.2.52.4.14.2.67  2014/06/23 14:50:34  snimmakayala
 * 20148940
 *
 * Revision 1.2.2.52.4.14.2.66  2014/06/23 14:41:35  snimmakayala
 * 20148940
 *
 * Revision 1.2.2.52.4.14.2.65  2014/06/05 14:21:56  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148753
 *
 * Revision 1.2.2.52.4.14.2.64  2014/04/16 15:32:40  nneelam
 * case#  20148032
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.2.2.52.4.14.2.63  2014/04/15 13:07:43  snimmakayala
 * Case #: 201219227
 *
 * Revision 1.2.2.52.4.14.2.62  2014/04/09 15:35:57  gkalla
 * case#20127911,20140010
 * LL Pick generation Kit issue and location group null issue
 *
 * Revision 1.2.2.52.4.14.2.61  2014/04/09 11:01:56  nneelam
 * case#  20141576
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.2.2.52.4.14.2.60  2014/04/08 15:52:56  nneelam
 * case#  20141263
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.2.2.52.4.14.2.59  2014/03/27 13:57:18  rmukkera
 * Case # 20127751
 *
 * Revision 1.2.2.52.4.14.2.58  2014/03/26 14:38:08  rmukkera
 * Case # 20127728
 *
 * Revision 1.2.2.52.4.14.2.57  2014/03/25 16:13:00  gkalla
 * case#20127739
 * MHP Cartonization CR
 *
 * Revision 1.2.2.52.4.14.2.56  2014/03/24 14:37:57  rmukkera
 * Case # 20127751�
 *
 * Revision 1.2.2.52.4.14.2.55  2014/03/20 15:03:13  rmukkera
 * Case # 20126958,20127751�
 *
 * Revision 1.2.2.52.4.14.2.54  2014/03/13 15:23:19  snimmakayala
 * Case #: 201218745
 *
 * Revision 1.2.2.52.4.14.2.53  2014/03/13 15:06:20  sponnaganti
 * case# 20127685
 * (cluster number not generating issue)
 *
 * Revision 1.2.2.52.4.14.2.52  2014/02/25 15:12:57  rmukkera
 * Case # 20127328,20127083
 *
 * Revision 1.2.2.52.4.14.2.51  2014/02/21 16:33:19  svanama
 * Case# 20125416
 * Auto pickreport .Customer specific pickreport print based on System Rules
 *
 * Revision 1.2.2.52.4.14.2.50  2014/02/18 09:33:22  snimmakayala
 * Case# : 201218481
 *
 * Revision 1.2.2.52.4.14.2.49  2014/02/05 07:09:41  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#201218314
 *
 * Revision 1.2.2.52.4.14.2.48  2014/01/31 15:50:36  rmukkera
 * Case # 20126983
 *
 * Revision 1.2.2.52.4.14.2.47  2014/01/31 15:41:40  gkalla
 * case#201218314�
 * Sports HQ for pick gen if bin location mapped to the pick rule
 *
 * Revision 1.2.2.52.4.14.2.46  2014/01/23 06:55:19  snimmakayala
 * Case# : 20126913
 * Cartonization Issue for Cube & Weight.
 *
 * Revision 1.2.2.52.4.14.2.45  2014/01/22 09:16:59  snimmakayala
 * Case# : 20126913
 * Cartonization Issue for Cube & Weight.
 *
 * Revision 1.2.2.52.4.14.2.44  2014/01/10 17:07:14  gkalla
 * case#20126729
 * MHP Auto Replenishment: system is generated replenishment more than Max Qty of pick face location
 *
 * Revision 1.2.2.52.4.14.2.43  2014/01/10 06:46:50  snimmakayala
 * Case# : 20126503
 * Cartonization Issue
 *
 * Revision 1.2.2.52.4.14.2.42  2014/01/07 13:53:35  schepuri
 * 20126634
 *
 * Revision 1.2.2.52.4.14.2.41  2013/12/13 15:02:58  rmukkera
 * Case #  20126064�
 *
 * Revision 1.2.2.52.4.14.2.40  2013/12/12 16:42:19  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * prodissue fixes
 *
 * Revision 1.2.2.52.4.14.2.39  2013/12/05 09:46:27  snimmakayala
 * Case# : 20125976
 * MHP UAT Fixes.
 *
 * Revision 1.2.2.52.4.14.2.38  2013/12/05 09:37:15  snimmakayala
 * Case# : 20125976
 * MHP UAT Fixes.
 *
 * Revision 1.2.2.52.4.14.2.37  2013/11/27 14:09:43  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * prodissue fixes
 *
 * Revision 1.2.2.52.4.14.2.36  2013/11/22 15:55:14  gkalla
 * case#20125427
 * Wrong picks generating if item quantity is greater than case or pallet qty
 *
 * Revision 1.2.2.52.4.14.2.35  2013/11/18 16:05:32  gkalla
 * case#20125754  
 * CWD Wave generation allocation issue
 *
 * Revision 1.2.2.52.4.14.2.34  2013/11/08 15:36:06  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * SportsHQ issue fixes
 *
 * Revision 1.2.2.52.4.14.2.33  2013/10/25 16:57:37  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * MHPissue fixes
 *
 * Revision 1.2.2.52.4.14.2.32  2013/10/23 17:06:01  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related case#20124707 was fixed
 *
 * Revision 1.2.2.52.4.14.2.31  2013/10/23 10:17:57  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related case#201216950 was fixed
 *
 * Revision 1.2.2.52.4.14.2.30  2013/10/19 13:19:32  rmukkera
 * Case # 20123993,20124981
 *
 * Revision 1.2.2.52.4.14.2.29  2013/10/11 06:07:52  skreddy
 * Case# 20124822
 * CWD prod issue fix
 *
 * Revision 1.2.2.52.4.14.2.28  2013/10/08 23:47:51  gkalla
 * Case# 20124954
 * Ryonet for kitpackage item in fulfillment order we are getting failed task
 *
 * Revision 1.2.2.52.4.14.2.27  2013/09/30 15:55:04  rmukkera
 * Case# 20124697
 *
 * Revision 1.2.2.52.4.14.2.26  2013/09/24 07:23:31  gkalla
 * Case# 20124547
 * Wave generation over allocation issue fix from pick face location
 *
 * Revision 1.2.2.52.4.14.2.25  2013/09/20 15:24:15  nneelam
 * Case#. 20124489
 * Cluster Not generating Issue Fixed.
 *
 * Revision 1.2.2.52.4.14.2.24  2013/09/19 15:17:04  rmukkera
 * Case# 20124446
 *
 * Revision 1.2.2.52.4.14.2.23  2013/09/17 16:04:11  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * standardbundlechanges
 * case#20124221
 *
 * Revision 1.2.2.52.4.14.2.22  2013/09/11 14:23:51  skreddy
 * Case# 20124364
 * afosa  sb issue fix
 *
 * Revision 1.2.2.52.4.14.2.21  2013/08/30 18:38:04  grao
 * Dynacraft Specific issue fixes case#:20124169
 *
 * Revision 1.2.2.52.4.14.2.20  2013/08/30 00:09:44  grao
 * Dynacraft Specific issue fixes case#:20124169
 *
 * Revision 1.2.2.52.4.14.2.19  2013/08/30 00:02:35  grao
 * Dynacraft Specific issue fixes case#:20124169
 *
 * Revision 1.2.2.52.4.14.2.18  2013/08/27 16:34:09  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * stdchanges
 * case#20123755
 *
 * Revision 1.2.2.52.4.14.2.14.2.1  2013/08/12 15:25:25  rmukkera
 * case# 20123845�,20123846�,20123849
 * Issue FIx for caseNo�
 *
 * Revision 1.2.2.52.4.14.2.14  2013/08/05 13:55:21  rmukkera
 * Standard bundle Issue Fix for  case NO 20123735,20123736,20123737
 *
 * Revision 1.2.2.52.4.14.2.13  2013/08/01 06:32:57  grao
 * Case# 20123649
 * TSG SB:Pick Report Related issue fixes(Pickgen Flag not updating in Wave Order Details)
 *
 * Revision 1.2.2.52.4.14.2.12  2013/07/29 14:54:55  rmukkera
 * no message
 *
 * Revision 1.2.2.52.4.14.2.11  2013/07/25 08:51:40  rmukkera
 * forward pick cr changes
 *
 * Revision 1.2.2.52.4.14.2.10  2013/07/04 12:46:49  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA :: Issue Fixes
 * Case# : 20123239
 *
 * Revision 1.2.2.52.4.14.2.9  2013/07/01 10:38:49  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA :: Issue Fixes
 * Case# : 20123239
 *
 * Revision 1.2.2.52.4.14.2.8  2013/06/25 15:44:35  gkalla
 * CASE201112/CR201113/LOG201121
 * PMM Issue Case# 20123019
 *
 * Revision 1.2.2.52.4.14.2.7  2013/06/19 23:00:37  gkalla
 * CASE201112/CR201113/LOG201121
 * As part of optimistic locking
 *
 * Revision 1.2.2.52.4.14.2.6  2013/06/18 14:12:21  mbpragada
 * 20120490
 *
 * Revision 1.2.2.52.4.14.2.5  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.2.2.52.4.14.2.4  2013/04/30 23:30:47  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.2.2.52.4.14.2.3  2013/03/19 11:45:09  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.2.2.52.4.14.2.2  2013/03/06 09:48:26  gkalla
 * CASE201112/CR201113/LOG201121
 * Lexjet Files merging for Standard bundle
 *
 * Revision 1.2.2.52.4.14.2.1  2013/03/02 12:36:21  gkalla
 * CASE201112/CR201113/LOG201121
 * Merged from Factory mation as part of standard bundle
 *
 * Revision 1.2.2.52.4.14  2013/02/16 12:11:10  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.2.2.52.4.13  2013/01/21 14:47:39  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Fine tuning for Kit items.
 *
 * Revision 1.2.2.52.4.12  2013/01/09 15:20:51  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * inactived sub components issue in pick generation.
 * .
 *
 * Revision 1.2.2.52.4.11  2012/12/21 09:05:15  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA after go live Fixes.
 *
 * Revision 1.2.2.52.4.10  2012/12/16 01:39:08  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA after go live Fixes.
 *
 * Revision 1.2.2.52.4.9  2012/11/30 23:56:48  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA UAT Fixes.
 *
 * Revision 1.2.2.52.4.8  2012/11/09 13:35:50  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Pickgenchanges
 *
 * Revision 1.2.2.52.4.7  2012/11/07 06:31:38  mbpragada
 * 20120490
 *
 * Revision 1.2.2.52.4.6  2012/11/01 14:54:57  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.2.2.52.4.5  2012/10/26 08:04:19  gkalla
 * CASE201112/CR201113/LOG201121
 * Lot# selection popup in wave selection by line
 *
 * Revision 1.2.2.52.4.3  2012/10/07 23:06:03  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue Fixes for FISK,BOOMBAH and TDG.
 *
 * Revision 1.2.2.52.4.2  2012/10/03 11:37:29  schepuri
 * CASE201112/CR201113/LOG201121
 * Visibility of BatchNo in PickReport
 *
 * Revision 1.2.2.52.4.1  2012/10/01 05:18:22  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code from 2012.2.
 *
 * Revision 1.2.2.52  2012/09/14 20:12:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production Issue Fixes for FISK and BOOMBAH.
 *
 * Revision 1.2.2.48  2012/09/03 19:10:00  snimmakayala
 * CASE201112/CR201113/LOG201121
 * LEFO based picking
 *
 * Revision 1.2.2.47  2012/08/30 16:16:20  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * KIt item replens
 *
 * Revision 1.2.2.46  2012/08/29 18:17:22  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * replens generation based on Auto replen flag
 *
 * Revision 1.2.2.45  2012/08/27 04:05:47  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Boombah Issue Fixes
 *
 * Revision 1.2.2.44  2012/08/26 17:55:03  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Pick Confirm issues for FISK
 *
 * Revision 1.2.2.43  2012/08/26 04:33:50  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pickgen issue for FISK
 * Case # 20120693
 *
 * Revision 1.2.2.42  2012/08/25 02:55:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Boombah Cartonization issues and FISK pickgen issues.
 *
 * Revision 1.2.2.41  2012/08/24 17:42:44  gkalla
 * CASE201112/CR201113/LOG201121
 * Fulfillment ord line is not updating to pickgenerated issue
 *
 * Revision 1.2.2.40  2012/08/22 13:51:51  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UCC label generation validation at customer and order type level for Dynacraft.
 *
 * Revision 1.2.2.39  2012/08/16 07:53:35  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Boombah UAT Issue Fixes.
 * Cartonization and Customization
 *
 * Revision 1.2.2.38  2012/08/01 07:23:26  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Routing
 *
 * Revision 1.2.2.37  2012/07/30 17:06:29  gkalla
 * CASE201112/CR201113/LOG201121
 * replenishment merging
 *
 * Revision 1.2.2.35  2012/07/17 12:41:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Performance Tuning (Bulk Insert & Update)
 *
 * Revision 1.2.2.34  2012/07/17 08:16:55  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Performance Tuning (Bulk Insert)
 *
 * Revision 1.2.2.33  2012/07/13 00:41:23  gkalla
 * CASE201112/CR201113/LOG201121
 * Added GerUOM function
 *
 * Revision 1.2.2.32  2012/07/12 14:19:14  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Performance Tuning
 *
 * Revision 1.2.2.31  2012/07/11 14:34:00  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Updating LP Type in LP Master.
 *
 * Revision 1.2.2.30  2012/07/10 23:12:06  gkalla
 * CASE201112/CR201113/LOG201121
 * Alert Mails
 *
 * Revision 1.2.2.29  2012/06/25 11:29:36  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Strategy after UOM breakdown
 *
 * Revision 1.2.2.28  2012/06/22 12:25:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT and Prod issue fixes
 *
 * Revision 1.2.2.27  2012/06/15 07:21:16  spendyala
 * CASE201112/CR201113/LOG201121
 * While picks generation all UOM are taken consideration
 * i.e., Pallet,Case,InnerPallet and Each
 *
 * Revision 1.2.2.26  2012/06/04 14:16:30  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Strategy after UOM breakdown
 *
 * Revision 1.2.2.25  2012/05/22 15:28:55  snimmakayala
 * CASE201112/CR201113/LOG201121
 * To fix replenishment issue
 *
 * Revision 1.2.2.24  2012/05/21 17:16:27  gkalla
 * CASE201112/CR201113/LOG201121
 * To fix replenishment issue
 *
 * Revision 1.2.2.23  2012/05/18 18:01:56  gkalla
 * CASE201112/CR201113/LOG201121
 * For replenishment issue
 *
 * Revision 1.2.2.22  2012/05/17 15:56:33  gkalla
 * CASE201112/CR201113/LOG201121
 * To fix Replenishment issue
 *
 * Revision 1.2.2.21  2012/05/16 23:35:48  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * kit to order
 *
 * Revision 1.2.2.20  2012/05/14 23:19:13  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Kit to order
 *
 * Revision 1.2.2.19  2012/05/11 09:54:23  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue fixes for GSUSA
 *
 * Revision 1.2.2.18  2012/05/09 07:52:37  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Partial Status Handling
 *
 * Revision 1.2.2.17  2012/05/02 12:33:26  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Pick generation
 *
 * Revision 1.2.2.16  2012/04/30 21:27:09  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.2.2.15  2012/04/30 12:33:25  svanama
 * CASE201112/CR201113/LOG201121
 * PickReport Code  added
 *
 * Revision 1.2.2.14  2012/04/27 14:12:33  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Replenishment
 *
 * Revision 1.2.2.13  2012/04/25 15:21:34  spendyala
 * CASE201112/CR201113/LOG201121
 * While Setting Distinct Container# fail picks rec shld not take under consideration.
 *
 * Revision 1.2.2.12  2012/04/19 16:12:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Zone Picking and SKIP
 *
 * Revision 1.17  2012/04/19 15:13:12  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Zone Picking and SKIP
 *
 * Revision 1.16  2012/04/17 08:16:39  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pool of records
 *
 * Revision 1.15  2012/03/14 07:06:01  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Report changes  and undefined container lp issue for TPP
 *
 * Revision 1.14  2012/03/09 09:41:59  schepuri
 * CASE201112/CR201113/LOG201121
 * code merge from 1.2.2.8
 *
 * Revision 1.13  2012/03/09 07:16:55  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.12  2012/03/02 01:11:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.11  2012/02/28 01:22:19  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.10  2012/02/24 12:19:33  gkalla
 * CASE201112/CR201113/LOG201121
 * To fix the issue for TPP , LPs are creating for each line
 *
 * Revision 1.9  2012/02/23 17:58:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.8  2012/02/22 14:04:37  gkalla
 * CASE201112/CR201113/LOG201121
 * Added WMS Carrier line level functionality
 *
 * Revision 1.7  2012/02/21 15:04:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.6  2012/02/16 00:55:58  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.5  2012/02/09 14:43:43  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Physical and Virtual Location changes
 *
 * Revision 1.4  2012/01/27 06:51:52  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Generation UE
 *
 * Revision 1.3  2012/01/13 16:50:36  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Generation User Event changes
 *
 * Revision 1.2  2011/12/26 22:50:10  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Checkin changes
 *
 * Revision 1.1  2011/12/21 16:45:13  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Gen User Event
 *
 * Revision 1.27  2011/12/05 14:58:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.26  2011/11/28 07:02:47  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Confirm changes
 *
 * Revision 1.25  2011/11/25 08:54:37  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Default LOT
 *
 * Revision 1.23  2011/11/24 12:28:29  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Exception.
 *
 * Revision 1.22  2011/11/21 13:18:02  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Default Lot Functionality. (Item as Lot)
 *
 * Revision 1.21  2011/11/15 16:05:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * set display flag to 'N' when updating inventory
 *
 * Revision 1.20  2011/11/04 11:03:04  svanama
 * CASE201112/CR201113/LOG201121
 * InsertExceptionLog  is added in try catch block
 *
 * Revision 1.19  2011/10/04 16:06:19  gkalla
 * CASE201112/CR201113/LOG201121
 * For Lot# entry updation
 *
 * Revision 1.18  2011/10/04 12:11:54  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.17  2011/10/03 09:30:49  jrnarsupalli
 * CASE201112/CR201113/LOG201121
 * RM Comments: Corrected CMLOG number in the previous version.
 *
 * Revision 1.16  2011/09/30 07:09:10  snimmakayala
 * CASE201112/CR201113/LOG20112
 * Label printing related changes done by Siva Kumar.V
 *
 * Revision 1.12  2011/09/26 08:44:47  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.11  2011/09/24 08:46:00  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.10  2011/09/23 13:21:21  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.9  2011/09/23 07:24:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Actual End Date Filter is commented
 *
 * Revision 1.8  2011/09/22 15:42:37  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Logs Added
 *
 * Revision 1.7  2011/09/21 14:13:05  rmukkera
 * CASE201112/CR201113/LOG201121
 * code  modification in calling transform record
 *
 * Revision 1.6  2011/09/20 13:27:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.5  2011/09/19 10:28:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UOM Conversion
 *
 * Revision 1.4  2011/09/16 07:28:58  rmukkera
 * CASE201112/CR201113/LOG201121
 * source code changes included both open and close tasks
 * added cvs header
 *


 **************************************************************************** */

function PickGenerationUE(type){

	var context = nlapiGetContext();
	nlapiLogExecution('DEBUG','Remaining usage at the start',context.getRemainingUsage());

	var vordno="";
	var vordlineno="";
	var vwaveno="";
	var vlastline="";
	var vlocation="";
	var vcompany="";
	var userId="";
	var allocstrategy='';
	var vlastlineinwave='';
	var vwaveordrecid=-1;
	var vusedflag='';
	var vLotExcepId='';
	var vwaveassignedto='';
	try{
		nlapiLogExecution('DEBUG', 'type', type);

		var newRecord = nlapiGetNewRecord();		

		try{

			if(type =='create' || type =='edit' || type=='xedit')
			{ 
				vusedflag = newRecord.getFieldValue('custrecord_ebiz_waveorder_usedflag');

				nlapiLogExecution('DEBUG', 'Used', vusedflag);

				if(vusedflag=='T')
				{
					vwaveno = newRecord.getFieldValue('custrecord_ebiz_waveorder_waveno');
					vordno = newRecord.getFieldValue('custrecord_ebiz_waveorder_orderno');
					vordlineno = newRecord.getFieldValue('custrecord_ebiz_waveorder_orderline');
					vlastline = newRecord.getFieldValue('custrecord_ebiz_waveorder_lastline');
					vlocation = newRecord.getFieldValue('custrecord_ebiz_waveorder_location');
					vcompany = newRecord.getFieldValue('custrecord_ebiz_waveorder_company');
					vlastlineinwave = newRecord.getFieldValue('custrecord_ebiz_waveorder_wavelastline');
					vwaveordrecid = newRecord.getId();
					nlapiLogExecution('DEBUG', 'vordlineno', vordlineno);
					vLotExcepId = newRecord.getFieldValue('custrecord_ebiz_lotnumber_internalid');
					vLotExcepQty = newRecord.getFieldValue('custrecord_ebiz_lotno_qty');
					vwaveassignedto = newRecord.getFieldValue('custrecord_ebiz_waveorder_waveassignedto');

					var fulfillOrderList = getOrderDetailsForWaveGen(vordno,vordlineno,vwaveno);
					//10

					if(fulfillOrderList!=null && fulfillOrderList.length > 0)
					{
						var sointernalid = fulfillOrderList[0][1];

						nlapiLogExecution('DEBUG', 'sointernalid',sointernalid);
						var vordertype;
						if(sointernalid!=null && sointernalid!='')
						{
							var SOFields = ['custbody_nswmssoordertype'];
							var SOColumns = nlapiLookupField('salesorder', sointernalid, SOFields);

							if(SOColumns != null && SOColumns != '')
							{
								vordertype=SOColumns.custbody_nswmssoordertype;
							}	
						}

						//var skuQtyList = getSKUList(fulfillOrderList);

						//var skuList = getItemList(skuQtyList);

						//var itemDimList = getItemDimensions(skuList);
						//20

						//var itemTypeArray = getItemTypesForList(skuList);

						//var itemGroupFamilyArray = getItemGroupAndFamily(itemTypeArray);

						//var pickRulesList = getListForAllPickZones();
						//30

						//var allocConfigDtls = getPickConfigForAllOrder(fulfillOrderList, pickRulesList, itemGroupFamilyArray, vlocation,skuList);

//						if(allocConfigDtls!=null && allocConfigDtls!='')
//						allocstrategy=allocConfigDtls[0][6];

						//nlapiLogExecution('DEBUG', 'allocstrategy', allocstrategy);

						//var allLocnGroups = getAllLocnGroups(allocConfigDtls);

						//var inventorySearchResults = getItemDetails(allLocnGroups, skuList,allocstrategy);
						//40
						/***Below code is merged from Lexjet Production by Ganesh on 6th Mar 2013***/
						//fetching the carriertype from carrierservicelevel
						var vCarrierType='';
						var nsshipmethod=fulfillOrderList[0][36];//custrecord_do_carrier
						nlapiLogExecution('DEBUG', 'nsshipmethod', nsshipmethod);		
						if(nsshipmethod!=null && nsshipmethod!='')
						{
							var filterscarr = new Array();
							var columnscarr = new Array();

							filterscarr.push(new nlobjSearchFilter('custrecord_carrier_nsmethod', null, 'is',nsshipmethod));
							filterscarr.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

							columnscarr[0] = new nlobjSearchColumn('custrecord_carrier_type');	
							columnscarr[1] = new nlobjSearchColumn('custrecord_carrier_service_level');	
							columnscarr[2] = new nlobjSearchColumn('id');

							var searchresultscarr = nlapiSearchRecord('customrecord_ebiznet_carrier', null, filterscarr, columnscarr);
							if(searchresultscarr!=null && searchresultscarr!='' && searchresultscarr.length>0)
							{
								vCarrierType = searchresultscarr[0].getText('custrecord_carrier_type');
								var	vcarrier = searchresultscarr[0].getId();
							}
						}
						nlapiLogExecution('DEBUG', 'vCarrierType', vCarrierType);	
						/***Upto here***/
						var inventorySearchResults = new Array();
						//vlocation pushed to array added on 18-03-2015
						var tempLocArray=new Array();
						if(vlocation!=null && vlocation!='')
						{
							tempLocArray.push('@NONE@');
							tempLocArray.push(vlocation);
						}
						//var containerslist = getAllContainersList(vlocation);
						var containerslist = getAllContainersList(tempLocArray);
						//50

						var allocConfigDtls = new Array();
						var LPparent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); //create new parent record on LP Master.

						allocateQuantity(inventorySearchResults, fulfillOrderList, vwaveno, allocConfigDtls,containerslist,LPparent,vLotExcepId,vLotExcepQty,
								vordertype,vwaveassignedto);

						nlapiLogExecution('DEBUG', 'vlastline: ',vlastline);
						nlapiLogExecution('DEBUG', 'vlastlineinwave: ',vlastlineinwave);

						if(vlastline=='T')
						{
							var EbizOrderNo = fulfillOrderList[0][1]; 
							var company = fulfillOrderList[0][13];
							var whlocation = fulfillOrderList[0][19];

							nlapiLogExecution('DEBUG','Remaining usage at the start of ActualCartonization',context.getRemainingUsage());
							ActualCartonization(vwaveno,EbizOrderNo,containerslist,whlocation,LPparent);	
							nlapiSubmitRecord(LPparent); //submit the parent record, all child records will also be created
							//Set the Count value against each ContainerLP in sequence order
							//eg:If for order 4 Contaienerlp's(lp10,lp20,lp30 and lp40) are generated 
							//Then v r setting the values as lp10 of 1,lp20 of 2,lp30 of 3 and lp40 of 4.

							setCountAgainstEachContainerLp(vwaveno,EbizOrderNo);

							if(vlastlineinwave=='T')
							{
								nlapiLogExecution('DEBUG', 'vordertype: ',vordertype);
								nlapiLogExecution('DEBUG','Remaining usage at the start of Clustering',context.getRemainingUsage());
								//case #20124489 Start
								//if(vordertype=='3'){
								setClusterNo(vwaveno);
								//}
								//case #20124489 End
								nlapiSubmitField('customrecord_ebiz_waveord', vwaveordrecid, 'custrecord_ebiz_waveorder_pickgenflag', 'T');
								//generatePickreporthtml(vwaveno);
								var systemrulevalue=generatepickreportbasedcustomerspecific();
								if(systemrulevalue=="Y")
								{
									generateCustomspecificPickreporthtml(vwaveno);
								}
								var systemIntegrationRulevalue=getIntegrationSystemRuleValue("SHIP-PickLabel", "");
								if(systemIntegrationRulevalue!=null && systemIntegrationRulevalue!="")
								{
									var systemrulevalue = systemIntegrationRulevalue[0];
									var systemruletype = systemIntegrationRulevalue[1];
									if (systemruletype == "BartenderFormat") 
									{

										if (systemrulevalue == "Standard")
										{
											generatebartenderpicklabel(vwaveno);
										}
										else if(systemrulevalue == "Custom")
										{
											try
											{
												generateCustomBartenderPickLabel(vwaveno);
											}
											catch(exp)
											{
												nlapiLogExecution('DEBUG', 'Expection in CustomerSpecific Pick Label Generation',exp);
											}
										}
									}
								}
								var vcuruserId = context.getUser();
								var tranType = nlapiLookupField('transaction', EbizOrderNo, 'recordType');
								updateScheduleScriptStatus('WAVE RELEASE',vcuruserId,'Completed',vwaveno,tranType);
							}

							try
							{
								nlapiLogExecution('DEBUG','Remaining usage at the start of GenerateChildLabel',context.getRemainingUsage());
								//GenerateChildLabel("",EbizOrderNo,vwaveno,"",company);
							}
							catch(exp)
							{
								nlapiLogExecution('DEBUG', 'Exception in UccLabelGeneration: ',exp);
							}						
						}
						else
						{
							nlapiSubmitRecord(LPparent); //submit the parent record, all child records will also be created
						}

						nlapiLogExecution('DEBUG', 'vwaveordrecid: ',vwaveordrecid);

						nlapiSubmitField('customrecord_ebiz_waveord', vwaveordrecid, 'custrecord_ebiz_waveorder_pickgenflag', 'T');

					}
				}
			}

		}
		catch(exp) {
			nlapiLogExecution('DEBUG', 'Exception in PickGenerationUE1 : ', exp);		
		}
	}
	catch(exp) {
		nlapiLogExecution('DEBUG', 'Exception in PickGenerationUE2 : ', exp);	
	}

	nlapiLogExecution('DEBUG','Remaining usage at the end',context.getRemainingUsage());
}

function getDistinctMethodsList(distinctorderslist){
	var distinctorders = new Array();
	if(distinctorderslist!=null && distinctorderslist.length>0){	
		label:for (var z = 0; z < distinctorderslist.length; z++){
			for (var y = 0; y < distinctorders.length; y++){
				if((distinctorderslist[z][0]==distinctorders[y][0]) && (distinctorderslist[z][1]==distinctorders[y][1])){
					continue label;
				}					
			}	
			distinctorders[distinctorders.length]=distinctorderslist[z];
		}
	}
	return distinctorders;
}

var optmizationarray = new Array();

function ActualCartonization(vwaveno,vordno,containerslist,whLocation,LPparent)
{
	nlapiLogExecution('DEBUG', 'Into ActualCartonization - Wave#', vwaveno);
	nlapiLogExecution('DEBUG', 'whLocation', whLocation);
	var totalweight=0;
	var totalcube=0;
	var contsize = "";	
	var uompackflag='';
	var itemsarray = new Array();
	var allitemdimsarray =  new Array();

	var opentasklist = getPickTaskDetailsforCartonization(vwaveno,vordno);//
	if(opentasklist!=null && opentasklist!='' && opentasklist.length>0)
	{
		var ordermethodlist=new Array();

		for (var n = 0; n < opentasklist.length; n++)
		{
			var vebizmethodno = opentasklist[n].getValue('custrecord_ebizmethod_no');
			var vWMSCarrier = opentasklist[n].getValue('custrecord_ebizwmscarrier');

			itemsarray.push(opentasklist[n].getValue('custrecord_ebiz_sku_no'));

			var currentRow = [vordno, vebizmethodno,vWMSCarrier];

			ordermethodlist[n] = currentRow;

			totalweight=parseFloat(totalweight)+parseFloat(opentasklist[n].getValue('custrecord_total_weight'));
			totalcube=parseFloat(totalcube)+parseFloat(opentasklist[n].getValue('custrecord_totalcube'));
		}


		if(itemsarray!=null && itemsarray!='')
		{
			nlapiLogExecution('DEBUG', 'itemsarray', itemsarray);
			itemsarray=removeDuplicateElement(itemsarray);
			nlapiLogExecution('DEBUG', 'itemsarray', itemsarray);

			allitemdimsarray = getdimsforallitems(itemsarray,whLocation);
			nlapiLogExecution('DEBUG', 'allitemdimsarray',allitemdimsarray);
		}

		nlapiLogExecution('DEBUG', 'allitemdimsarray',allitemdimsarray);

		var distinctmethods = getDistinctMethodsList(ordermethodlist);
		var wmsCarrier='';
		var prvWMSCarrier='-1';
		var newContainerLpNo='';
		var vpickzoneno='';
		var prvpickzoneno='-1';
		for (var m = 0; m < distinctmethods.length; m++)
		{
			var ebizorderno = distinctmethods[m][0];
			var ebizmethodno = distinctmethods[m][1];
			var cartonizationmethod='';
			var cartonizationStrategy='';
			nlapiLogExecution('DEBUG','ebizmethodno',ebizmethodno);

			var arrcubeweight = getCubeanddWeightbyMethod(vwaveno,ebizorderno,ebizmethodno)
			if(arrcubeweight!=null && arrcubeweight!='')
			{
				totalweight = arrcubeweight[0].getValue('custrecord_total_weight',null,'sum');
				totalcube = arrcubeweight[0].getValue('custrecord_totalcube',null,'sum');

				var str17 = 'totalweight. = ' + totalweight + '<br>';
				str17 = str17 + 'totalcube. = ' + totalcube + '<br>';
				str17 = str17 + 'ebizmethodno. = ' + ebizmethodno + '<br>';	

				nlapiLogExecution('DEBUG', 'Weight & Cube by Method', str17);
			}

			if(ebizmethodno!=null && ebizmethodno!='')
			{				
				var fields = ['custrecord_cartonization_method','custrecord_ebiz_cartonization_strategy'];
				var pickmethodcolumns = nlapiLookupField('customrecord_ebiznet_pick_method', ebizmethodno, fields);
				if(pickmethodcolumns!=null)
				{	
					cartonizationmethod = pickmethodcolumns.custrecord_cartonization_method;
					cartonizationStrategy = pickmethodcolumns.custrecord_ebiz_cartonization_strategy;
					if(cartonizationStrategy == null || cartonizationStrategy == '')
						cartonizationStrategy=1;
				}	
			}

			nlapiLogExecution('DEBUG','cartonizationmethod',cartonizationmethod);
			nlapiLogExecution('DEBUG','cartonizationStrategy',cartonizationStrategy);
			if(cartonizationmethod==null || cartonizationmethod=="")					
				cartonizationmethod='4'; // By Order

			if(cartonizationmethod == '4') 
			{
				for (var k = 0; k < opentasklist.length; k++)
				{
					if(ebizmethodno==opentasklist[k].getValue('custrecord_ebizmethod_no'))
					{
						wmsCarrier = opentasklist[k].getValue('custrecord_ebizwmscarrier');
						vpickzoneno = opentasklist[k].getValue('custrecord_ebizzone_no');

						if(wmsCarrier == null || wmsCarrier == '')
							wmsCarrier='-2';

						if(vpickzoneno == null || vpickzoneno == '')
							vpickzoneno='-2';
						var vOTLocation=opentasklist[k].getValue('custrecord_wms_location');
						if(prvpickzoneno != vpickzoneno)
						{	
							nlapiLogExecution('DEBUG', 'whLocation', vOTLocation);
							newContainerLpNo=GetMaxLPNo('1', '2',vOTLocation);
							nlapiLogExecution('DEBUG','newContainerLpNo',newContainerLpNo);
							CreateMasterLPRecord(newContainerLpNo,'',totalweight,totalcube,'',vOTLocation,LPparent);
							prvpickzoneno = vpickzoneno;
							prvWMSCarrier = wmsCarrier;
						} 
						else if(prvWMSCarrier != wmsCarrier)
						{	
							nlapiLogExecution('DEBUG', 'whLocation', vOTLocation);
							newContainerLpNo=GetMaxLPNo('1', '2',vOTLocation);
							nlapiLogExecution('DEBUG','newContainerLpNo',newContainerLpNo);
							CreateMasterLPRecord(newContainerLpNo,'',totalweight,totalcube,'',vOTLocation,LPparent);
							prvWMSCarrier = wmsCarrier;
						} 

						var Recid = opentasklist[k].getId();

						var fieldNames = new Array(); 
						fieldNames.push('custrecord_container_lp_no');
						fieldNames.push('custrecord_ebiz_lpno');


						var newValues = new Array(); 
						newValues.push(newContainerLpNo);
						newValues.push(newContainerLpNo);

						nlapiSubmitField('customrecord_ebiznet_trn_opentask', Recid, fieldNames, newValues);

					}
				}
			}
			else
			{
				if(containerslist != null && containerslist.length>0){
					contsize = getContainerSizeforOrder(containerslist,totalcube,totalweight,cartonizationmethod,null,
							opentasklist,whLocation);

					if(contsize!=null && contsize!='')
					{						
						for(j=0;j<opentasklist.length;j++){
							if(opentasklist[j].getValue('custrecord_ebizmethod_no')==ebizmethodno)
							{
								vpickzoneno = opentasklist[j].getValue('custrecord_ebizzone_no');
								if(vpickzoneno == null || vpickzoneno == '')
									vpickzoneno='-2';
								nlapiLogExecution('DEBUG','vpickzoneno',vpickzoneno);
								var containerLP1;
								if(cartonizationStrategy == 2 && prvpickzoneno != vpickzoneno)
								{	
									containerLP1 = GetMaxLPNo('1', '2',whLocation);
									nlapiLogExecution('DEBUG', 'Container LP (Total Order) : ', containerLP1);
									CreateMasterLPRecord(containerLP1,contsize,totalweight,totalcube,'',whLocation,LPparent);

									prvpickzoneno = vpickzoneno;

								} 
								else if(containerLP1 == null || containerLP1 =='')
								{	
									containerLP1 = GetMaxLPNo('1', '2',whLocation);
									nlapiLogExecution('DEBUG', 'Container LP (Total Order) : ', containerLP1);
									CreateMasterLPRecord(containerLP1,contsize,totalweight,totalcube,'',whLocation,LPparent);							


								} 

								nlapiLogExecution('DEBUG', 'Container LP (Before Update) : ', containerLP1);
								var internalid = opentasklist[j].getId();							
								updatesizeidinopentask(internalid,containerLP1,contsize);
							}
						}
					}
					else
					{
						var arrusedcartons = new Array();
						var sizearray  = new Array();
						for(k=0;k<opentasklist.length;k++){
							if(opentasklist[k].getValue('custrecord_ebizmethod_no')==ebizmethodno)
							{
								var uomlevel = opentasklist[k].getValue('custrecord_uom_level');
								var ebizskuno = opentasklist[k].getValue('custrecord_ebiz_sku_no');
								var itemdims = getUOMCube(uomlevel,ebizskuno,whLocation);								

								vpickzoneno = opentasklist[k].getValue('custrecord_ebizzone_no');
								if(vpickzoneno == null || vpickzoneno == '')
									vpickzoneno='-2';
								nlapiLogExecution('DEBUG','vpickzoneno',vpickzoneno);

								if(containerslist != null && containerslist.length>0){								

									//if(contsize==null || contsize=='')
									//	{
									if(cartonizationmethod=="1"){
										buildcartonsforcubeandweight(opentasklist[k],containerslist,uompackflag,LPparent);
									}
									else if(cartonizationmethod=="2"){
										buildcartonsforweight(opentasklist[k],containerslist,uompackflag,LPparent);
									} 
									else if(cartonizationmethod=="3"){
										buildcartonsforcube(opentasklist[k],containerslist,uompackflag,LPparent);
									}	
									else if(cartonizationmethod=="5"){
										//buildcartonsforminandmaxdims(opentasklist[k],containerslist,uompackflag,itemdims);
										var opentask = opentasklist[k];
										nlapiLogExecution('DEBUG', 'Into  buildcartonsforminandmaxdims Task Split', '');
										try{
											var totaltaskweight = parseFloat(opentask.getValue('custrecord_total_weight'));
											var totaltaskcube = parseFloat(opentask.getValue('custrecord_totalcube'));	
											var totaltaskqty = parseFloat(opentask.getValue('custrecord_expe_qty'));	
											var pickzone1 = opentask.getValue('custrecord_ebizzone_no');
											nlapiLogExecution('DEBUG', 'pickzone123', pickzone1);
											var remianingqty=totaltaskqty;
											nlapiLogExecution('DEBUG', 'totaltaskqty : ', totaltaskqty);
											var remweight = parseFloat(totaltaskweight);
											var remcube = parseFloat(totaltaskcube);
											var contsize="";
											var containerLP="";
											var cube="";
											var weight="";
											var exptdqty =0;
											var itemcube=0;
											var itemweight=0;
											var itemlength=0;
											var itemwidth=0;
											var itemheight=0;
											var itemmaxdim = 0;
											var itemmindim = 0;
											var itemuomqty = 0;

											var whsite=opentask.getValue('custrecord_wms_location');

											var uomlevel = opentask.getValue('custrecord_uom_level');
											var ebizskuno = opentask.getValue('custrecord_ebiz_sku_no');
											var indxarray = new Array();
											//var uomdims = getUOMCube(uomlevel,ebizskuno);
											if(itemdims!=null && itemdims!='')
											{
												itemcube = itemdims[0].getValue('custrecord_ebizcube');
												itemweight = itemdims[0].getValue('custrecord_ebizweight');
												itemlength = itemdims[0].getValue('custrecord_ebizlength');
												itemwidth = itemdims[0].getValue('custrecord_ebizwidth');
												itemheight = itemdims[0].getValue('custrecord_ebizheight');
												itemuomqty = itemdims[0].getValue('custrecord_ebizqty');

												itemmaxdim = Math.max(parseFloat(itemlength), parseFloat(itemwidth), parseFloat(itemheight));
												itemmindim = Math.min(parseFloat(itemlength), parseFloat(itemwidth), parseFloat(itemheight));
											}

											if(containerslist != null){ 

												nlapiLogExecution('DEBUG', 'arrusedcartons',arrusedcartons);

												if(arrusedcartons!=null && arrusedcartons!='' && arrusedcartons.length>0)
												{
													var splitarray = new Array();

													nlapiLogExecution('DEBUG', 'arrusedcartons length',arrusedcartons.length);
													for(t=0;t<arrusedcartons.length;t++)
													{				
														var usedCarton = arrusedcartons[t][0];
														var usedSize = arrusedcartons[t][1];
														var cartonremcube = arrusedcartons[t][2];
														var cartonremwght = arrusedcartons[t][3];
														var usedZone = arrusedcartons[t][4];
														nlapiLogExecution('DEBUG', 'usedZone',usedZone);
														nlapiLogExecution('DEBUG', 'vpickzoneno',vpickzoneno);
														if(parseFloat(itemcube)<=parseFloat(cartonremcube) && vpickzoneno == usedZone )
														{
															for(s=(containerslist.length)-1;s>=0;s--)
															{
																if(usedSize == containerslist[s].getValue('Internalid'))
																{
																	var vcontainerlength = containerslist[s].getValue('custrecord_length');
																	var vcontainerwidth = containerslist[s].getValue('custrecord_widthcontainer');
																	var vcontainerheight = containerslist[s].getValue('custrecord_heightcontainer');

																	var vcontainermaxdim = Math.max(parseFloat(vcontainerlength), parseFloat(vcontainerwidth), parseFloat(vcontainerheight));
																	var vcontainermindim = Math.min(parseFloat(vcontainerlength), parseFloat(vcontainerwidth), parseFloat(vcontainerheight));

																	var str7 = 'usedCarton. = ' + usedCarton + '<br>';
																	str7 = str7 + 'usedSize. = ' + usedSize + '<br>';
																	str7 = str7 + 'cartonremcube. = ' + cartonremcube + '<br>';	
																	str7 = str7 + 'cartonremwght. = ' + cartonremwght + '<br>';	
																	str7 = str7 + 'itemcube. = ' + itemcube + '<br>';	
																	str7 = str7 + 'itemmaxdim. = ' + itemmaxdim + '<br>';	
																	str7 = str7 + 'vcontainermaxdim. = ' + vcontainermaxdim + '<br>';
																	str7 = str7 + 'itemmindim. = ' + itemmindim + '<br>';
																	str7 = str7 + 'vcontainermindim. = ' + vcontainermindim + '<br>';
																	str7 = str7 + 'itemuomqty. = ' + itemuomqty + '<br>';

																	nlapiLogExecution('DEBUG', 'Size Determination in Used Cartons1', str7);

																	if(itemmaxdim<=vcontainermaxdim && itemmindim<=vcontainermindim)
																	{
																		var vqtybycube = Math.floor(parseFloat(cartonremcube)/parseFloat(itemcube));
																		var vqtybyweight = Math.floor(parseFloat(cartonremwght)/parseFloat(itemweight));
																		var vtasksplitqty = Math.min(parseFloat(vqtybycube), parseFloat(vqtybyweight));								
																		exptdqty = parseFloat(vtasksplitqty)*parseFloat(itemuomqty);;

//																		var vsplittaskweight = parseFloat(exptdqty)*parseFloat(itemweight);
//																		var vsplittaskcube = parseFloat(exptdqty)*parseFloat(itemcube);

																		var str5 = 'vqtybycube. = ' + vqtybycube + '<br>';
																		str5 = str5 + 'vqtybyweight. = ' + vqtybyweight + '<br>';
																		str5 = str5 + 'vtasksplitqty. = ' + vtasksplitqty + '<br>';	
																		str5 = str5 + 'itemuomqty. = ' + itemuomqty + '<br>';	
																		str5 = str5 + 'exptdqty. = ' + exptdqty + '<br>';	
																		str5 = str5 + 'remianingqty. = ' + remianingqty + '<br>';	

																		nlapiLogExecution('DEBUG', 'Size Determination in Used Cartons2', str5);

																		var taskCreated = 'F';
																		var totcartremcube = cartonremcube;
																		var totcartremwght = cartonremwght;


																		while((parseFloat(exptdqty)>0) && (parseFloat(remianingqty)>0) 
																				&& (Math.min(parseFloat(exptdqty),parseFloat(remianingqty))<=parseFloat(remianingqty))
																				//&& parseFloat(itemcube)<=parseFloat(cartonremcube)
																				//&& parseFloat(remcube)>=parseFloat(cartonremcube)  //Commented by Satish.N on 01SEP2014
																				&& (parseFloat(cartonremcube)>0) && (parseFloat(cartonremwght)>0)
																				&& (((Math.min(parseFloat(exptdqty),parseFloat(remianingqty))/parseFloat(itemuomqty))*parseFloat(itemcube))<=parseFloat(cartonremcube))
																				&& (((Math.min(parseFloat(exptdqty),parseFloat(remianingqty))/parseFloat(itemuomqty))*parseFloat(itemweight))<=parseFloat(cartonremwght))
																		)
																		{
																			var taskqty = Math.min(parseFloat(exptdqty),parseFloat(remianingqty));

																			var vsplittaskweight = (parseFloat(taskqty)*parseFloat(itemweight))/parseFloat(itemuomqty);
																			var vsplittaskcube = (parseFloat(taskqty)*parseFloat(itemcube))/parseFloat(itemuomqty);

																			taskCreated = 'T';
																			contsize =  containerslist[s].getValue('Internalid');
																			containerLP = usedCarton;																			
																			nlapiLogExecution('DEBUG', 'containerLP1 in buildcartonsforminandmaxdims used cartons', containerLP);
																			createNewPickTask(opentask,containerLP,vsplittaskcube,vsplittaskweight,contsize,taskqty);	
																			remweight = parseFloat(remweight) - parseFloat(vsplittaskweight);
																			remcube = parseFloat(remcube) - parseFloat(vsplittaskcube);										
																			remianingqty = parseFloat(remianingqty) - parseFloat(taskqty);
//																			var vcontainerremcube = parseFloat(cartonremcube) - parseFloat(vsplittaskcube);
//																			var vcontainerremweight = parseFloat(cartonremwght) - parseFloat(vsplittaskweight);

																			cartonremcube = parseFloat(cartonremcube) - parseFloat(vsplittaskcube);
																			cartonremwght = parseFloat(cartonremwght) - parseFloat(vsplittaskweight);

																			totcartremcube = totcartremcube-vsplittaskcube;
																			totcartremwght = totcartremwght-vsplittaskweight;

																			var str8 = 'task remweight. = ' + remweight + '<br>';
																			str8 = str8 + 'task remcube. = ' + remcube + '<br>';
																			str8 = str8 + 'remianingqty. = ' + remianingqty + '<br>';
																			str8 = str8 + 'containerremcube. = ' + cartonremcube + '<br>';	
																			str8 = str8 + 'containerremweight. = ' + cartonremwght + '<br>';

																			nlapiLogExecution('DEBUG', 'Log inside While', str8);
																			indxarray.push(containerLP);
																			//	optmizationarray.splice(t, 1);

																		}

																		if(taskCreated=='T')
																		{
																			var currow = [containerLP,contsize,totcartremcube,totcartremwght,vpickzoneno];
																			splitarray.push(currow);
																		}
																	}
																}
															}
														}
													}

													if(splitarray!=null && splitarray!='' && splitarray.length>0)
													{
														arrusedcartons = updateusedcartons(arrusedcartons,splitarray);
														//nlapiLogExecution('DEBUG', 'arrusedcartons : ', arrusedcartons);
														for(var v=0;v<splitarray.length;v++)
														{	
															arrusedcartons.push(splitarray[v]);
														} 
													}
												}

												nlapiLogExecution('DEBUG', 'Remaining Qty after processing Used Cartons : ', remianingqty);

												if(parseFloat(remianingqty) > 0)
												{
													for(l=(containerslist.length)-1;l>=0;l--){

														var containername = containerslist[l].getValue('custrecord_containername');
														var containerweight = containerslist[l].getValue('custrecord_maxweight');
														var containercube = containerslist[l].getValue('custrecord_cubecontainer');
														var packfactor = containerslist[l].getValue('custrecord_packfactor');
														var containerlength = containerslist[l].getValue('custrecord_length');
														var containerwidth = containerslist[l].getValue('custrecord_widthcontainer');
														var containerheight = containerslist[l].getValue('custrecord_heightcontainer');

														var containermaxdim = Math.max(parseFloat(containerlength), parseFloat(containerwidth), parseFloat(containerheight));
														var containermindim = Math.min(parseFloat(containerlength), parseFloat(containerwidth), parseFloat(containerheight));

														if(packfactor!=null && packfactor!='')
														{
															containercube = parseFloat(containercube)-(parseFloat(containercube)*(parseFloat(packfactor)/100));
														}

														var str = 'task remweight. = ' + remweight + '<br>';				
														str = str + 'task remcube. = ' + remcube + '<br>';
														str = str + 'containername. = ' + containername + '<br>';
														str = str + 'containerweight. = ' + containerweight + '<br>';	
														str = str + 'itemweight. = ' + itemweight + '<br>';
														str = str + 'containercube. = ' + containercube + '<br>';	
														str = str + 'itemcube. = ' + itemcube + '<br>';			
														str = str + 'itemlength. = ' + itemlength + '<br>';
														str = str + 'itemwidth. = ' + itemwidth + '<br>';
														str = str + 'itemheight. = ' + itemheight + '<br>';
														str = str + 'itemuomqty. = ' + itemuomqty + '<br>';														
														str = str + 'containerlength. = ' + containerlength + '<br>';
														str = str + 'containerwidth. = ' + containerwidth + '<br>';
														str = str + 'containerheight. = ' + containerheight + '<br>';

														nlapiLogExecution('DEBUG', 'Size Determination1', str);

														if((containername != 'SHIPASIS') 
																&& (parseFloat(itemcube)<=parseFloat(containercube)))
														{
															var str1 = 'itemmaxdim. = ' + itemmaxdim + '<br>';				
															str1 = str1 + 'itemmindim. = ' + itemmindim + '<br>';
															str1 = str1 + 'containermaxdim. = ' + containermaxdim + '<br>';
															str1 = str1 + 'containermindim. = ' + containermindim + '<br>';

															nlapiLogExecution('DEBUG', 'Size Determination2', str1);

															if(itemmaxdim<=containermaxdim && itemmindim<=containermindim)
															{
																var qtybycube = Math.floor(parseFloat(containercube)/parseFloat(itemcube));
																var qtybyweight = Math.floor(parseFloat(containerweight)/parseFloat(itemweight));
																var tasksplitqty = Math.min(parseFloat(qtybycube), parseFloat(qtybyweight));
																//exptdqty = Math.min(parseFloat(tasksplitqty), parseFloat(remianingqty));
																exptdqty = parseFloat(tasksplitqty) * parseFloat(itemuomqty);

																var str2 = 'qtybycube. = ' + qtybycube + '<br>';
																str2 = str2 + 'qtybyweight. = ' + qtybyweight + '<br>';
																str2 = str2 + 'tasksplitqty. = ' + tasksplitqty + '<br>';	
																str2 = str2 + 'itemuomqty. = ' + itemuomqty + '<br>';	
																str2 = str2 + 'exptdqty. = ' + exptdqty + '<br>';	
																str2 = str2 + 'remianingqty. = ' + remianingqty + '<br>';	

																nlapiLogExecution('DEBUG', 'Size Determination3', str2);


																while((parseFloat(exptdqty)>0) && (parseFloat(remianingqty)>0) 
																		&& (Math.min(parseFloat(exptdqty),parseFloat(remianingqty))<=parseFloat(remianingqty))
																		//&& parseFloat(itemcube)<=parseFloat(containercube)
																		//&& parseFloat(remcube)>=parseFloat(containercube) //This line is commented by Satish.N on 09JAN2014
																		&& (parseFloat(containercube)>0) && (parseFloat(containerweight)>0)
																		&& (((Math.min(parseFloat(exptdqty),parseFloat(remianingqty))/parseFloat(itemuomqty))*parseFloat(itemcube))<=parseFloat(containercube))
																)
																{
																	var taskqty = Math.min(parseFloat(exptdqty),parseFloat(remianingqty));

																	var splittaskweight = (parseFloat(taskqty)*parseFloat(itemweight))/parseFloat(itemuomqty);
																	var splittaskcube = (parseFloat(taskqty)*parseFloat(itemcube))/parseFloat(itemuomqty);

																	contsize =  containerslist[l].getValue('Internalid');
																	containerLP = GetMaxLPNo('1', '2',whsite);
																	cube = containerslist[l].getValue('custrecord_cubecontainer');
																	weight = containerslist[l].getValue('custrecord_maxweight');
																	CreateMasterLPRecord(containerLP,contsize,weight,cube,uompackflag,whsite,LPparent);
																	nlapiLogExecution('DEBUG', 'containerLP1 in buildcartonsforminandmaxdims ', containerLP);
																	createNewPickTask(opentask,containerLP,splittaskcube,splittaskweight,contsize,taskqty,pickzone1);		              
																	remweight = parseFloat(remweight) - parseFloat(splittaskweight);
																	remcube = parseFloat(remcube) - parseFloat(splittaskcube);
																	remianingqty = parseFloat(remianingqty) - parseFloat(taskqty);

																	//The below code is uncommented by Satish.N on 26-DEC-2013
																	var containerremcube = parseFloat(containercube) - parseFloat(splittaskcube);
																	var containerremweight = parseFloat(containerweight) - parseFloat(splittaskweight);
																	//Upto here

																	//The below code is commented by Satish.N on 26-DEC-2013
																	//containercube = parseFloat(containercube) - parseFloat(splittaskcube);
																	//containerweight = parseFloat(containerweight) - parseFloat(splittaskweight);
																	//Upto here


																	var str4 = 'task remweight. = ' + remweight + '<br>';
																	str4 = str4 + 'task remcube. = ' + remcube + '<br>';
																	str4 = str4 + 'remianingqty. = ' + remianingqty + '<br>';
																	str4 = str4 + 'containerremcube. = ' + containerremcube + '<br>';	
																	str4 = str4 + 'containerremweight. = ' + containerremweight + '<br>';

																	nlapiLogExecution('DEBUG', 'Log inside While', str4);

																	//The below code is changed by Satish.N on 26-DEC-2013
																	//var currow = [containerLP,contsize,containercube,containerweight];
																	var currow = [containerLP,contsize,containerremcube,containerremweight,vpickzoneno];
																	//Upto here
																	arrusedcartons.push(currow);
																}	
															}
														}
													} 
												}
												//if still there is some qty exists and the cube is less than the container(s) cube(no matching container),
												//then place it in the smallest container.
												nlapiLogExecution('DEBUG', 'remianingqty at end : ', remianingqty);	
												if(parseFloat(remianingqty)> 0){

													var q = containerslist.length;
													if (parseFloat(totaltaskqty)>parseFloat(remianingqty))
														contsize =  containerslist[q-1].getValue('Internalid');
													else
														contsize=getDefaultSize(whsite);
													containerLP = GetMaxLPNo('1', '2',whsite);
													CreateMasterLPRecord(containerLP,contsize,remweight,remcube,uompackflag,whsite,LPparent);
													nlapiLogExecution('DEBUG', 'containerLP2 in buildcartonsforminandmaxdims ', containerLP);
													createNewPickTask(opentask,containerLP,remcube,remweight,contsize,remianingqty,pickzone1);	             
													cube = containerslist[q-1].getValue('custrecord_cubecontainer');
													weight = containerslist[q-1].getValue('custrecord_maxweight');
													remianingqty = 0;
												}

												if(parseFloat(remianingqty)<=0)
												{
													//delete the actual record
													var id = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', opentask.getId());
													nlapiLogExecution('DEBUG', 'Deleted Actual Record : ', id);	
												}

											}
										}
										catch(exp) {
											nlapiLogExecution('DEBUG', 'Exception in buildcartonsforminandmaxdims : ', exp);		
										}
										nlapiLogExecution('DEBUG', 'out of  buildcartonsforminandmaxdims Task Split', '');

									}
									//	}
								}
							}
						}

						if(arrusedcartons!=null && arrusedcartons!='' && arrusedcartons.length>0)
						{
							//nlapiLogExecution('DEBUG', 'arrusedcartons before sort : ', arrusedcartons);
							arrusedcartons.sort(function(a,b){return parseFloat(a[2]) - parseFloat(b[2]);});
							nlapiLogExecution('DEBUG', 'arrusedcartons after sort : ', arrusedcartons);
							optimizecartons(arrusedcartons,containerslist,allitemdimsarray,vwaveno,vordno);
						}
					}
				}
			}
		}
	}
}

function getDefaultSize(whsite)
{
	nlapiLogExecution('DEBUG', 'Into getDefaultSize', whsite);

	var defaultsize='';

	var containerslist = new Array();

	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebiz_container_defaultflag', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(whsite!=null && whsite!='')
		filters.push(new nlobjSearchFilter('custrecord_ebizsitecontainer', null, 'anyof', ['@NONE@',whsite]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('Internalid');
	columns[1] = new nlobjSearchColumn('custrecord_cubecontainer');
	columns[2] = new nlobjSearchColumn('custrecord_maxweight');	
	columns[3] = new nlobjSearchColumn('custrecord_tareweight');
	columns[4] = new nlobjSearchColumn('custrecord_containername');
	columns[5] = new nlobjSearchColumn('custrecord_packfactor');
	columns[6] = new nlobjSearchColumn('custrecord_length');
	columns[7] = new nlobjSearchColumn('custrecord_widthcontainer');
	columns[8] = new nlobjSearchColumn('custrecord_heightcontainer');

	columns[1].setSort(false);
	columns[2].setSort(false);

	containerslist = nlapiSearchRecord('customrecord_ebiznet_container', null, filters, columns);
	if(containerslist!=null && containerslist!='')
	{
		defaultsize = containerslist[0].getId();
	}

	nlapiLogExecution('DEBUG', 'out of getDefaultSize', defaultsize);

	return defaultsize;
}

function optimizecartons(arrusedcartons,containerslist,allitemdimsarray,vwaveno,vordno)
{
	nlapiLogExecution('DEBUG', 'Into optimizecartons');
	//nlapiLogExecution('DEBUG', 'allitemdimsarray',allitemdimsarray);

	var usedcartonscnt = arrusedcartons.length;

	var picktasklist = getPickTaskDetailsforSizeOptimization(vwaveno,vordno);
	if(picktasklist!=null && picktasklist!='' && picktasklist.length>0)
	{
		for(t=0;t<arrusedcartons.length;t++)
		{
			var maxitemdimarr = new Array();
			var minitemdimarr = new Array();
			var carton = arrusedcartons[t][0];
			var size = arrusedcartons[t][1];
			var gitemmaxdim=0;
			var gitemmindim=0;
			var totaltaskswght = 0;
			var totaltaskscube = 0;

			var str = 'carton. = ' + carton + '<br>';				
			str = str + 'size. = ' + size + '<br>';

			nlapiLogExecution('DEBUG', 'Used Cartons', str);

			for(s=0;s<picktasklist.length;s++)
			{
				var pickcarton = picktasklist[s].getValue('custrecord_container_lp_no');
				var pickitem = picktasklist[s].getValue('custrecord_ebiz_sku_no');

				nlapiLogExecution('DEBUG', 'pickcarton', pickcarton);

				if(carton==pickcarton)
				{
					var taskwght = picktasklist[s].getValue('custrecord_total_weight');
					if(taskwght==null || taskwght=='' || isNaN(taskwght))
						taskwght=0;

					var taskcube = picktasklist[s].getValue('custrecord_totalcube');
					if(taskcube==null || taskcube=='' || isNaN(taskcube))
						taskcube=0;

					totaltaskswght=parseFloat(totaltaskswght)+parseFloat(taskwght);
					totaltaskscube=parseFloat(totaltaskscube)+parseFloat(taskcube);
					if(allitemdimsarray!=null && allitemdimsarray!='')
					{
						for(k=0;k<allitemdimsarray.length;k++)
						{
							var dimsitem = allitemdimsarray[k].getValue('custrecord_ebizitemdims');
//							nlapiLogExecution('DEBUG', 'dimsitem', dimsitem);
//							nlapiLogExecution('DEBUG', 'pickitem', pickitem);

							if(dimsitem==pickitem)
							{
								var itemlength = allitemdimsarray[k].getValue('custrecord_ebizlength');
								var itemwidth = allitemdimsarray[k].getValue('custrecord_ebizwidth');
								var itemheight = allitemdimsarray[k].getValue('custrecord_ebizheight');

								var itemmaxdim = Math.max(parseFloat(itemlength), parseFloat(itemwidth), parseFloat(itemheight));
								var itemmindim = Math.min(parseFloat(itemlength), parseFloat(itemwidth), parseFloat(itemheight));

								maxitemdimarr.push(itemmaxdim);
								minitemdimarr.push(itemmindim);

								//nlapiLogExecution('DEBUG', 'maxitemdimarr', maxitemdimarr);
								maxitemdimarr.sort(function(a,b){return b - a;});
								//nlapiLogExecution('DEBUG', 'maxitemdimarr', maxitemdimarr);
								//nlapiLogExecution('DEBUG', 'minitemdimarr', minitemdimarr);
								minitemdimarr.sort(function(a,b){return a - b;});
								//nlapiLogExecution('DEBUG', 'minitemdimarr', minitemdimarr);
							}
						}
					}
				}
			}

			var contsize = getSizeforOptmization(containerslist,maxitemdimarr,minitemdimarr,totaltaskswght,totaltaskscube,
					pickitem,size);

			nlapiLogExecution('DEBUG', 'Size Id after optimization...',contsize);

			if(contsize!=null && contsize!='' && contsize!=size)
			{
				for(x=0;x<picktasklist.length;x++)
				{
					if(carton==picktasklist[x].getValue('custrecord_container_lp_no'))
					{
						nlapiLogExecution('DEBUG', 'Updating Size...',contsize);
						var fieldNames = new Array(); 
						fieldNames.push('custrecord_container');

						var newValues = new Array(); 
						newValues.push(contsize);

						nlapiSubmitField('customrecord_ebiznet_trn_opentask', picktasklist[x].getId(), fieldNames, newValues);
					}
				}

				// Update Carton
				updateCartons(carton,contsize,totaltaskswght,totaltaskscube);
			}			
		}
	}
	nlapiLogExecution('DEBUG', 'Out of optimizecartons');
}

function updateCartons(carton,contsize,totaltaskswght,totaltaskscube)
{
	nlapiLogExecution('DEBUG', 'Into updateCartons');

	var str = 'carton. = ' + carton + '<br>';				
	str = str + 'contsize. = ' + contsize + '<br>';
	str = str + 'totaltaskswght. = ' + totaltaskswght + '<br>';
	str = str + 'totaltaskscube. = ' + totaltaskscube + '<br>';	

	nlapiLogExecution('DEBUG', 'Parameters', str);

	var cartonFilters = new Array();
	var cartonColumns = new Array();

	cartonFilters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', carton));
	cartonFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	cartonColumns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
	cartonColumns[0].setSort();
	cartonColumns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sizeid');
	cartonColumns[2] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
	cartonColumns[3] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totcube');

	var cartonResults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, cartonFilters, cartonColumns);
	if(cartonResults!=null && cartonResults!='' && cartonResults.length>0)
	{
		nlapiLogExecution('DEBUG', 'Updating LP Master...',contsize);

		var fieldNames = new Array(); 
		fieldNames.push('custrecord_ebiz_lpmaster_totwght');
		fieldNames.push('custrecord_ebiz_lpmaster_totcube');

		var newValues = new Array(); 
		newValues.push(parseFloat(totaltaskswght).toFixed(5));
		newValues.push(parseFloat(totaltaskscube).toFixed(5));

		if(contsize!=null && contsize!='')
		{
			fieldNames.push('custrecord_ebiz_lpmaster_sizeid');
			newValues.push(contsize);
		}

		nlapiSubmitField('customrecord_ebiznet_master_lp', cartonResults[0].getId(), fieldNames, newValues);
	}

	nlapiLogExecution('DEBUG', 'Out of updateCartons');
}

function getSizeforOptmization(containerslist,maxitemdimarr,minitemdimarr,totaltaskswght,totaltaskscube,pickitem,size)
{
	nlapiLogExecution('DEBUG', 'Into getSizeforOptmization');
	var contsize='';
	var gitemmaxdim=0;
	var gitemmindim=0;

	if (parseFloat(totaltaskswght) >0)
	{
		for(l=0;l<containerslist.length;l++)
		{ 
			var containerlength = containerslist[l].getValue('custrecord_length');
			var containerwidth = containerslist[l].getValue('custrecord_widthcontainer');
			var containerheight = containerslist[l].getValue('custrecord_heightcontainer');
			var containercube = containerslist[l].getValue('custrecord_cubecontainer');
			var containerweight= containerslist[l].getValue('custrecord_maxweight');
			var containername = containerslist[l].getValue('custrecord_containername');
			var packfactor=containerslist[l].getValue('custrecord_packfactor');
			if(packfactor==null || packfactor=='')
				packfactor=0;
			var containeractcube=parseFloat(containercube)-(parseFloat(containercube)*(parseFloat(packfactor)/100));

			var str = 'totaltaskscube. = ' + totaltaskscube + '<br>';				
			str = str + 'containeractcube. = ' + containeractcube + '<br>';
			str = str + 'containername. = ' + containername + '<br>';
			str = str + 'totaltaskswght. = ' + totaltaskswght + '<br>';	
			str = str + 'containerweight. = ' + containerweight + '<br>';	

			nlapiLogExecution('DEBUG', 'Size Determination1', str);

			if(containername!='SHIPASIS' && 
					parseFloat(containeractcube)>=parseFloat(totaltaskscube) && 
					parseFloat(containerweight)>=parseFloat(totaltaskswght))
			{

				var containermaxdim = Math.max(parseFloat(containerlength), parseFloat(containerwidth), parseFloat(containerheight));
				var containermindim = Math.min(parseFloat(containerlength), parseFloat(containerwidth), parseFloat(containerheight));

				for(a=0;maxitemdimarr!=null&&a<maxitemdimarr.length;a++)
				{
					gitemmaxdim = maxitemdimarr[0];
				}

				for(b=0;minitemdimarr!=null&&b<minitemdimarr.length;b++)
				{
					gitemmindim = minitemdimarr[0];
				}

				if(isNaN(gitemmaxdim))
					gitemmaxdim=0;

				if(isNaN(gitemmindim))
					gitemmindim=0;

				var str = 'containermaxdim. = ' + containermaxdim + '<br>';				
				str = str + 'containermindim. = ' + containermindim + '<br>';
				str = str + 'gitemmaxdim. = ' + gitemmaxdim + '<br>';
				str = str + 'gitemmindim. = ' + gitemmindim + '<br>';

				nlapiLogExecution('DEBUG', 'Size Determination2', str);

				if(parseFloat(containermaxdim)>=parseFloat(gitemmaxdim) &&
						parseFloat(containermindim)>=parseFloat(gitemmindim))
				{
					contsize=containerslist[l].getId();
					return contsize;
				}
			}
		}
	}

	nlapiLogExecution('DEBUG', 'Out of getSizeforOptmization',contsize);

	return contsize;
}

function updateusedcartons(arrusedcartons,sizearray)
{
	nlapiLogExecution('DEBUG', 'Into updateusedcartons');

	var newarray = new Array();

	for(x=0;arrusedcartons!=null && x<arrusedcartons.length;x++)
	{
		//nlapiLogExecution('DEBUG', 'X',x);
		var boolfound = 'F';
		for(y=0;sizearray!=null && y<sizearray.length;y++)
		{
			var str4 = 'arrusedcartons[x][0]. = ' + arrusedcartons[x][0] + '<br>';
			str4 = str4 + 'sizearray[y][0]. = ' + sizearray[y][0] + '<br>';

			//nlapiLogExecution('DEBUG', 'Array Comparison Log', str4);

			if(arrusedcartons[x][0] == sizearray[y][0] )
			{				
				boolfound='T';
				nlapiLogExecution('DEBUG', 'Match Found',boolfound);
			}
		}

		if(boolfound=='F')
		{
			//nlapiLogExecution('DEBUG', 'boolfound',boolfound);
			var currow = [arrusedcartons[x][0],arrusedcartons[x][1],arrusedcartons[x][2],arrusedcartons[x][3],arrusedcartons[x][4]];
			newarray.push(currow);
		}
	}

	nlapiLogExecution('DEBUG', 'Out of updateusedcartons',newarray);

	return newarray;
}

function buildcartonsforcube(opentasklist,containerslist,uompackflag,LPparent)
{
	nlapiLogExecution('DEBUG', 'into buildcartonsforcube Task Split', '');
	try { 
		var totaltaskweight = parseFloat(opentasklist.getValue('custrecord_total_weight'));
		var totaltaskcube = parseFloat(opentasklist.getValue('custrecord_totalcube'));	
		var whsite=opentasklist.getValue('custrecord_wms_location');
		nlapiLogExecution('DEBUG', 'Total Task Weight', totaltaskweight);
		nlapiLogExecution('DEBUG', 'Total Task Cube', totaltaskcube);
		var remcube = parseFloat(totaltaskcube);
		var remweight = parseFloat(totaltaskweight);
		var contsize="";
		var containerLP="";
		var cube="";
		var weight="";
		var exptdqty=0;

		var uomlevel = opentasklist.getValue('custrecord_uom_level');
		var ebizskuno = opentasklist.getValue('custrecord_ebiz_sku_no');
		var uomcube = getUOMCube(uomlevel,ebizskuno);

		if(containerslist != null){ 
			for(l=(containerslist.length)-1;l>=0;l--){                                        
				var containercube = containerslist[l].getValue('custrecord_cubecontainer');
				var packfactor = containerslist[l].getValue('custrecord_packfactor');
				if(packfactor!=null && packfactor!='')
				{
					containercube = parseFloat(containercube)-(parseFloat(containercube)*(parseFloat(packfactor)/100));
				}
				nlapiLogExecution('DEBUG', 'Container Cube  ',containercube);
				nlapiLogExecution('DEBUG', 'SKU Uom Cube  ',uomcube);
				exptdqty = Math.floor(parseFloat(containercube)/parseFloat(uomcube));
				nlapiLogExecution('DEBUG', 'exptdqty  ',exptdqty);
				while (parseFloat(remcube)> 0 && parseFloat(remcube)>= parseFloat(containercube) && (exptdqty>0)){
					contsize =  containerslist[l].getValue('Internalid');
					containerLP = GetMaxLPNo('1', '2',whsite);
					cube = containerslist[l].getValue('custrecord_cubecontainer');
					weight = containerslist[l].getValue('custrecord_maxweight');
					CreateMasterLPRecord(containerLP,contsize,weight,cube,uompackflag,whsite,LPparent);
					nlapiLogExecution('DEBUG', 'containerLP1 in buildcartonsforcube ', containerLP);
					createNewPickTask(opentasklist,containerLP,cube,weight,contsize,exptdqty);
					remcube = parseFloat(remcube) - parseFloat(cube);
					remweight = parseFloat(remweight) - parseFloat(weight);
					nlapiLogExecution('DEBUG', 'Task Remaining Cube', remcube);
				}
			}

			//if still there is some qty exists and the cube is less than the container(s) cube(no matching container),
			//then place it in the smallest container.
			if(parseFloat(remcube)> 0){

				nlapiLogExecution('DEBUG', 'Task Remaining Cube', remcube);

				exptdqty = Math.floor(parseFloat(remcube)/parseFloat(uomcube));

				nlapiLogExecution('DEBUG', 'exptdqty  ',exptdqty);

				if(parseFloat(exptdqty)>0)
				{
					var k = containerslist.length;
					contsize =  containerslist[k-1].getValue('Internalid');
					containerLP = GetMaxLPNo('1', '2',whsite);				   
					cube = containerslist[k-1].getValue('custrecord_cubecontainer');
					weight = containerslist[k-1].getValue('custrecord_maxweight');	
					CreateMasterLPRecord(containerLP,contsize,weight,cube,uompackflag,whsite,LPparent);
					nlapiLogExecution('DEBUG', 'containerLP2 in buildcartonsforcube ', containerLP);
					createNewPickTask(opentasklist,containerLP,remcube,remweight,contsize,exptdqty);
				}
			}
			if(parseFloat(exptdqty)>0)
			{
				//delete the actual record
				var id = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', opentasklist.getId());
				nlapiLogExecution('DEBUG', 'Deleted Actual Record : ', id);	
			}
		}
	}
	catch(exp) {
		nlapiLogExecution('DEBUG', 'Exception in buildcartonsforcube : ', exp);		
	}

	nlapiLogExecution('DEBUG', 'out of  buildcartonsforcube Task Split', '');
}

function buildcartonsforweight(opentasklist,containerslist,uompackflag,LPparent)
{
	nlapiLogExecution('DEBUG', 'into  buildcartonsforweight Task Split', '');
	try {
		var totaltaskweight = parseFloat(opentasklist.getValue('custrecord_total_weight'));
		var totaltaskcube = parseFloat(opentasklist.getValue('custrecord_totalcube'));	
		var whsite=opentasklist.getValue('custrecord_wms_location');
		var remweight = parseFloat(totaltaskweight);
		var remcube = parseFloat(totaltaskcube);
		var contsize="";
		var containerLP="";
		var cube="";
		var weight="";
		var exptdqty=0;

		var uomlevel = opentasklist.getValue('custrecord_uom_level');
		var ebizskuno = opentasklist.getValue('custrecord_ebiz_sku_no');
		var uomcube = getUOMCube(uomlevel,ebizskuno);

		if(containerslist != null){ 
			for(l=(containerslist.length)-1;l>=0;l--){                                      
				var containerweight = containerslist[l].getValue('custrecord_maxweight');
				var containercube = containerslist[l].getValue('custrecord_cubecontainer');
				nlapiLogExecution('DEBUG', 'Container Cube  ',containercube);
				nlapiLogExecution('DEBUG', 'SKU UOM Cube  ',uomcube);

				exptdqty = Math.floor(parseFloat(containercube)/parseFloat(uomcube));

				nlapiLogExecution('DEBUG', 'exptdqty  ',exptdqty);
				while (parseFloat(remweight)> 0 && parseFloat(remweight)>= parseFloat(containerweight) 
						&& parseFloat(exptdqty)>0){
					contsize =  containerslist[l].getValue('Internalid');
					containerLP = GetMaxLPNo('1', '2',whsite);		
					cube = containerslist[l].getValue('custrecord_cubecontainer');
					weight = containerslist[l].getValue('custrecord_maxweight');	
					CreateMasterLPRecord(containerLP,contsize,weight,cube,uompackflag,whsite,LPparent);
					nlapiLogExecution('DEBUG', 'containerLP1 in buildcartonsforweight ', containerLP);
					createNewPickTask(opentasklist,containerLP,cube,weight,contsize,exptdqty);
					remweight = parseFloat(remweight) - parseFloat(weight);
					remcube = parseFloat(remcube) - parseFloat(cube);
				}
			} 

			//if still there is some qty exists and the cube is less than the container(s) cube(no matching container),
			//then place it in the smallest container.
			if(parseFloat(remweight)> 0){

				nlapiLogExecution('DEBUG', 'remweight  ',remweight);
				nlapiLogExecution('DEBUG', 'remcube  ',remcube);

				exptdqty = Math.floor(parseFloat(remcube)/parseFloat(uomcube));

				nlapiLogExecution('DEBUG', 'exptdqty  ',exptdqty);

				if(parseFloat(exptdqty)>0)
				{
					var k = containerslist.length;
					contsize =  containerslist[k-1].getValue('Internalid');
					containerLP = GetMaxLPNo('1', '2',whsite);
					CreateMasterLPRecord(containerLP,contsize,remweight,remcube,uompackflag,whsite,LPparent);
					nlapiLogExecution('DEBUG', 'containerLP2 in buildcartonsforweight ', containerLP);
					createNewPickTask(opentasklist,containerLP,remcube,remweight,contsize,exptdqty);
					cube = containerslist[k-1].getValue('custrecord_cubecontainer');
					weight = containerslist[k-1].getValue('custrecord_maxweight');
				}
			}
			if(parseFloat(exptdqty)>0)
			{
				//delete the actual record
				var id = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', opentasklist.getId());
				nlapiLogExecution('DEBUG', 'Deleted Actual Record : ', id);	
			}
		}
	}
	catch(exp) {
		nlapiLogExecution('DEBUG', 'Exception in buildcartonsforweight : ', exp);		
	}

	nlapiLogExecution('DEBUG', 'out of  buildcartonsforweight Task Split', '');
}

function buildcartonsforcubeandweight(opentasklist,containerslist,uompackflag,LPparent)
{
	nlapiLogExecution('DEBUG', 'Into  buildcartonsforcubeandweight Task Split', '');
	try{
		var totaltaskweight = parseFloat(opentasklist.getValue('custrecord_total_weight'));
		var totaltaskcube = parseFloat(opentasklist.getValue('custrecord_totalcube'));	
		var totaltaskqty = parseFloat(opentasklist.getValue('custrecord_expe_qty'));	
		var remianingqty=totaltaskqty;

		var remweight = parseFloat(totaltaskweight);
		var remcube = parseFloat(totaltaskcube);
		var contsize="";
		var containerLP="";
		var cube="";
		var weight="";
		var exptdqty =0;
		var uomcube=0;
		var uomweight=0;
		var whsite=opentasklist.getValue('custrecord_wms_location');

		var uomlevel = opentasklist.getValue('custrecord_uom_level');
		var ebizskuno = opentasklist.getValue('custrecord_ebiz_sku_no');
		var uomdims = getUOMCube(uomlevel,ebizskuno);
		if(uomdims!=null && uomdims!='')
		{
			uomcube = uomdims[0].getValue('custrecord_ebizcube');
			uomweight = uomdims[0].getValue('custrecord_ebizweight');
		}

		if(containerslist != null){ 

			for(l=(containerslist.length)-1;l>=0;l--){

				var containername = containerslist[l].getValue('custrecord_containername');
				var containerweight = containerslist[l].getValue('custrecord_maxweight');
				var containercube = containerslist[l].getValue('custrecord_cubecontainer');
				var packfactor = containerslist[l].getValue('custrecord_packfactor');
				var nooftasks=0;

				if(packfactor!=null && packfactor!='')
				{
					containercube = parseFloat(containercube)-(parseFloat(containercube)*(parseFloat(packfactor)/100));
				}

				var qtybycube = Math.floor(parseFloat(containercube)/parseFloat(uomcube));
				var qtybyweight = Math.floor(parseFloat(containerweight)/parseFloat(uomweight));
				var tasksplitqty = Math.min(parseFloat(qtybycube), parseFloat(qtybyweight));
				exptdqty = tasksplitqty;
				//exptdqty = Math.min(parseFloat(tasksplitqty), parseFloat(remianingqty));

				if(parseFloat(exptdqty)>0)
					nooftasks=Math.floor(parseFloat(remianingqty)/parseFloat(exptdqty));


				var str = 'task remweight. = ' + remweight + '<br>';				
				str = str + 'task remcube. = ' + remcube + '<br>';
				str = str + 'containername. = ' + containername + '<br>';
				str = str + 'containerweight. = ' + containerweight + '<br>';	
				str = str + 'containercube. = ' + containercube + '<br>';	
				str = str + 'qtybycube. = ' + qtybycube + '<br>';
				str = str + 'qtybyweight. = ' + qtybyweight + '<br>';
				str = str + 'tasksplitqty. = ' + tasksplitqty + '<br>';
				str = str + 'remianingqty. = ' + remianingqty + '<br>';
				str = str + 'exptdqty. = ' + exptdqty + '<br>';
				str = str + 'nooftasks. = ' + nooftasks + '<br>';

				nlapiLogExecution('DEBUG', 'Size Determination', str);

				var vloop=0;

				//while((parseFloat(exptdqty)>0) && (parseFloat(nooftasks)>parseFloat(vloop)))
				while((parseFloat(exptdqty)>0) && (parseFloat(exptdqty)<=parseFloat(remianingqty)))
				{
					contsize =  containerslist[l].getValue('Internalid');
					containerLP = GetMaxLPNo('1', '2',whsite);
					cube = containerslist[l].getValue('custrecord_cubecontainer');
					weight = containerslist[l].getValue('custrecord_maxweight');
					CreateMasterLPRecord(containerLP,contsize,weight,cube,uompackflag,whsite,LPparent);
					nlapiLogExecution('DEBUG', 'containerLP1 in buildcartonsforcubeandweight ', containerLP);
					createNewPickTask(opentasklist,containerLP,cube,weight,contsize,exptdqty);		              
					remweight = parseFloat(remweight) - (parseFloat(exptdqty)*parseFloat(uomweight));
					remcube = parseFloat(remcube) - (parseFloat(exptdqty)*parseFloat(uomcube));
					remianingqty = parseFloat(remianingqty) - parseFloat(exptdqty);
					vloop=vloop+1;

					var str1 = 'task remweight. = ' + remweight + '<br>';
					str1 = str1 + 'task remcube. = ' + remcube + '<br>';
					str1 = str1 + 'remianingqty. = ' + remianingqty + '<br>';
					str1 = str1 + 'vloop. = ' + vloop + '<br>';

					nlapiLogExecution('DEBUG', 'Log inside While', str1);
				}
			} 

			//if still there is some qty exists and the cube is less than the container(s) cube(no matching container),
			//then place it in the smallest container.
			if(parseFloat(remianingqty)> 0){

				var k = containerslist.length;
				contsize =  containerslist[k-1].getValue('Internalid');
				containerLP = GetMaxLPNo('1', '2',whsite);
				CreateMasterLPRecord(containerLP,contsize,remweight,remcube,uompackflag,whsite,LPparent);
				nlapiLogExecution('DEBUG', 'containerLP2 in buildcartonsforcubeandweight ', containerLP);
				createNewPickTask(opentasklist,containerLP,remcube,remweight,contsize,remianingqty);	             
				cube = containerslist[k-1].getValue('custrecord_cubecontainer');
				weight = containerslist[k-1].getValue('custrecord_maxweight');
				remianingqty = 0;
			}

			if(parseFloat(remianingqty)==0)
			{
				//delete the actual record
				var id = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', opentasklist.getId());
				nlapiLogExecution('DEBUG', 'Deleted Actual Record : ', id);	
			}
		}
	}
	catch(exp) {
		nlapiLogExecution('DEBUG', 'Exception in buildcartonsforcubeandweight : ', exp);		
	}
	nlapiLogExecution('DEBUG', 'out of  buildcartonsforcubeandweight Task Split', '');
}

function buildcartonsforminandmaxdims(opentasklist,containerslist,uompackflag,itemdims,LPparent)
{
	nlapiLogExecution('DEBUG', 'Into  buildcartonsforminandmaxdims Task Split', '');
	try{
		var totaltaskweight = parseFloat(opentasklist.getValue('custrecord_total_weight'));
		var totaltaskcube = parseFloat(opentasklist.getValue('custrecord_totalcube'));	
		var totaltaskqty = parseFloat(opentasklist.getValue('custrecord_expe_qty'));	
		var pickzone1 = opentasklist.getValue('custrecord_ebizzone_no');
		nlapiLogExecution('DEBUG', 'pickzone123', pickzone1);
		var remianingqty=totaltaskqty;

		var remweight = parseFloat(totaltaskweight);
		var remcube = parseFloat(totaltaskcube);
		var contsize="";
		var containerLP="";
		var cube="";
		var weight="";
		var exptdqty =0;
		var itemcube=0;
		var itemweight=0;
		var itemlength=0;
		var itemwidth=0;
		var itemheight=0;
		var itemmaxdim = 0;
		var itemmindim = 0;

		var whsite=opentasklist.getValue('custrecord_wms_location');

		var uomlevel = opentasklist.getValue('custrecord_uom_level');
		var ebizskuno = opentasklist.getValue('custrecord_ebiz_sku_no');
		var indxarray = new Array();
		//var uomdims = getUOMCube(uomlevel,ebizskuno);
		if(itemdims!=null && itemdims!='')
		{
			itemcube = itemdims[0].getValue('custrecord_ebizcube');
			itemweight = itemdims[0].getValue('custrecord_ebizweight');
			itemlength = itemdims[0].getValue('custrecord_ebizlength');
			itemwidth = itemdims[0].getValue('custrecord_ebizwidth');
			itemheight = itemdims[0].getValue('custrecord_ebizheight');

			itemmaxdim = Math.max(parseFloat(itemlength), parseFloat(itemwidth), parseFloat(itemheight));
			itemmindim = Math.min(parseFloat(itemlength), parseFloat(itemwidth), parseFloat(itemheight));
		}

		if(containerslist != null){ 

			nlapiLogExecution('DEBUG', 'optmizationarray',optmizationarray);
			if(optmizationarray!=null && optmizationarray!='' && optmizationarray.length>0)
			{
				nlapiLogExecution('DEBUG', 'optmizationarray length',optmizationarray.length);
				for(t=0;t<optmizationarray.length;t++)
				{				
					var usedCarton = optmizationarray[t][0];
					var usedSize = optmizationarray[t][1];
					var cartonremcube = optmizationarray[t][2];
					var cartonremwght = optmizationarray[t][3];

					if(parseFloat(itemcube)<=parseFloat(cartonremcube))
					{
						for(s=(containerslist.length)-1;s>=0;s--)
						{
							if(usedSize == containerslist[s].getValue('Internalid'))
							{
								var vcontainerlength = containerslist[s].getValue('custrecord_length');
								var vcontainerwidth = containerslist[s].getValue('custrecord_widthcontainer');
								var vcontainerheight = containerslist[s].getValue('custrecord_heightcontainer');

								var vcontainermaxdim = Math.max(parseFloat(vcontainerlength), parseFloat(vcontainerwidth), parseFloat(vcontainerheight));
								var vcontainermindim = Math.min(parseFloat(vcontainerlength), parseFloat(vcontainerwidth), parseFloat(vcontainerheight));

								var str7 = 'usedCarton. = ' + usedCarton + '<br>';
								str7 = str7 + 'usedSize. = ' + usedSize + '<br>';
								str7 = str7 + 'cartonremcube. = ' + cartonremcube + '<br>';	
								str7 = str7 + 'cartonremwght. = ' + cartonremwght + '<br>';	
								str7 = str7 + 'itemcube. = ' + itemcube + '<br>';	
								str7 = str7 + 'itemmaxdim. = ' + itemmaxdim + '<br>';	
								str7 = str7 + 'vcontainermaxdim. = ' + vcontainermaxdim + '<br>';
								str7 = str7 + 'itemmindim. = ' + itemmindim + '<br>';
								str7 = str7 + 'vcontainermindim. = ' + vcontainermindim + '<br>';

								nlapiLogExecution('DEBUG', 'Size Determination in Used Cartons1', str7);

								if(itemmaxdim<=vcontainermaxdim && itemmindim<=vcontainermindim)
								{
									var vqtybycube = Math.floor(parseFloat(cartonremcube)/parseFloat(itemcube));
									var vqtybyweight = Math.floor(parseFloat(cartonremwght)/parseFloat(itemweight));
									var vtasksplitqty = Math.min(parseFloat(vqtybycube), parseFloat(vqtybyweight));								
									exptdqty = vtasksplitqty;

									var vsplittaskweight = parseFloat(exptdqty)*parseFloat(itemweight);
									var vsplittaskcube = parseFloat(exptdqty)*parseFloat(itemcube);

									var str5 = 'vqtybycube. = ' + vqtybycube + '<br>';
									str5 = str5 + 'vqtybyweight. = ' + vqtybyweight + '<br>';
									str5 = str5 + 'vtasksplitqty. = ' + vtasksplitqty + '<br>';	

									nlapiLogExecution('DEBUG', 'Size Determination in Used Cartons2', str5);

									var taskCreated = 'F';
									var totcartremcube = cartonremcube;
									var totcartremwght = cartonremwght;

									while((parseFloat(exptdqty)>0) && (parseFloat(exptdqty)<=parseFloat(remianingqty)))
									{
										taskCreated = 'T';
										contsize =  containerslist[s].getValue('Internalid');
										containerLP = usedCarton;																			
										nlapiLogExecution('DEBUG', 'containerLP1 in buildcartonsforminandmaxdims used cartons', containerLP);
										createNewPickTask(opentasklist,containerLP,vsplittaskcube,vsplittaskweight,contsize,exptdqty,pickzone1);	
										remweight = parseFloat(remweight) - parseFloat(vsplittaskweight);
										remcube = parseFloat(remcube) - parseFloat(vsplittaskcube);										
										remianingqty = parseFloat(remianingqty) - parseFloat(exptdqty);
										var vcontainerremcube = parseFloat(cartonremcube) - parseFloat(vsplittaskcube);
										var vcontainerremweight = parseFloat(cartonremwght) - parseFloat(vsplittaskweight);
										totcartremcube = totcartremcube-vsplittaskcube;
										totcartremwght = totcartremwght-vsplittaskweight;

										var str8 = 'task remweight. = ' + remweight + '<br>';
										str8 = str8 + 'task remcube. = ' + remcube + '<br>';
										str8 = str8 + 'remianingqty. = ' + remianingqty + '<br>';
										str8 = str8 + 'containerremcube. = ' + vcontainerremcube + '<br>';	
										str8 = str8 + 'containerremweight. = ' + vcontainerremweight + '<br>';

										nlapiLogExecution('DEBUG', 'Log inside While', str8);
										indxarray.push(containerLP);
										//	optmizationarray.splice(t, 1);

									}

									if(taskCreated=='T')
									{
										var currow = [containerLP,contsize,totcartremcube,totcartremwght,pickzone1];
										optmizationarray.push(currow);
									}
								}
							}
						}
					}
				}

				if(indxarray!=null && indxarray!='' && indxarray.length>0)
				{
					indxarray = removeDuplicateElement(indxarray);
					optmizationarray = resetArray(optmizationarray,indxarray);
				}
			}


			for(l=(containerslist.length)-1;l>=0;l--){

				var containername = containerslist[l].getValue('custrecord_containername');
				var containerweight = containerslist[l].getValue('custrecord_maxweight');
				var containercube = containerslist[l].getValue('custrecord_cubecontainer');
				var packfactor = containerslist[l].getValue('custrecord_packfactor');
				var containerlength = containerslist[l].getValue('custrecord_length');
				var containerwidth = containerslist[l].getValue('custrecord_widthcontainer');
				var containerheight = containerslist[l].getValue('custrecord_heightcontainer');

				var containermaxdim = Math.max(parseFloat(containerlength), parseFloat(containerwidth), parseFloat(containerheight));
				var containermindim = Math.min(parseFloat(containerlength), parseFloat(containerwidth), parseFloat(containerheight));

				if(packfactor!=null && packfactor!='')
				{
					containercube = parseFloat(containercube)-(parseFloat(containercube)*(parseFloat(packfactor)/100));
				}

				var str = 'task remweight. = ' + remweight + '<br>';				
				str = str + 'task remcube. = ' + remcube + '<br>';
				str = str + 'containername. = ' + containername + '<br>';
				str = str + 'containerweight. = ' + containerweight + '<br>';	
				str = str + 'itemweight. = ' + itemweight + '<br>';
				str = str + 'containercube. = ' + containercube + '<br>';	
				str = str + 'itemcube. = ' + itemcube + '<br>';			
				str = str + 'itemlength. = ' + itemlength + '<br>';
				str = str + 'itemwidth. = ' + itemwidth + '<br>';
				str = str + 'itemheight. = ' + itemheight + '<br>';
				str = str + 'containerlength. = ' + containerlength + '<br>';
				str = str + 'containerwidth. = ' + containerwidth + '<br>';
				str = str + 'containerheight. = ' + containerheight + '<br>';

				nlapiLogExecution('DEBUG', 'Size Determination1', str);

				if(parseFloat(itemcube)<=parseFloat(containercube))
				{
					var str1 = 'itemmaxdim. = ' + itemmaxdim + '<br>';				
					str1 = str1 + 'itemmindim. = ' + itemmindim + '<br>';
					str1 = str1 + 'containermaxdim. = ' + containermaxdim + '<br>';
					str1 = str1 + 'containermindim. = ' + containermindim + '<br>';

					nlapiLogExecution('DEBUG', 'Size Determination2', str1);

					if(itemmaxdim<=containermaxdim && itemmindim<=containermindim)
					{
						var qtybycube = Math.floor(parseFloat(containercube)/parseFloat(itemcube));
						var qtybyweight = Math.floor(parseFloat(containerweight)/parseFloat(itemweight));
						var tasksplitqty = Math.min(parseFloat(qtybycube), parseFloat(qtybyweight));
						//exptdqty = Math.min(parseFloat(tasksplitqty), parseFloat(remianingqty));
						exptdqty = tasksplitqty;

						var splittaskweight = parseFloat(exptdqty)*parseFloat(itemweight);
						var splittaskcube = parseFloat(exptdqty)*parseFloat(itemcube);

						var str2 = 'qtybycube. = ' + qtybycube + '<br>';
						str2 = str2 + 'qtybyweight. = ' + qtybyweight + '<br>';
						str2 = str2 + 'tasksplitqty. = ' + tasksplitqty + '<br>';	

						nlapiLogExecution('DEBUG', 'Size Determination3', str2);

						while((parseFloat(exptdqty)>0) && (parseFloat(exptdqty)<=parseFloat(remianingqty)))
						{
							contsize =  containerslist[l].getValue('Internalid');
							containerLP = GetMaxLPNo('1', '2',whsite);
							cube = containerslist[l].getValue('custrecord_cubecontainer');
							weight = containerslist[l].getValue('custrecord_maxweight');
							CreateMasterLPRecord(containerLP,contsize,weight,cube,uompackflag,whsite,LPparent);
							nlapiLogExecution('DEBUG', 'containerLP1 in buildcartonsforminandmaxdims ', containerLP);
							createNewPickTask(opentasklist,containerLP,splittaskcube,splittaskweight,contsize,exptdqty,pickzone1);		              
							remweight = parseFloat(remweight) - parseFloat(splittaskweight);
							remcube = parseFloat(remcube) - parseFloat(splittaskcube);
							remianingqty = parseFloat(remianingqty) - parseFloat(exptdqty);
							var containerremcube = parseFloat(containercube) - parseFloat(splittaskcube);
							var containerremweight = parseFloat(containerweight) - parseFloat(splittaskweight);

							var str4 = 'task remweight. = ' + remweight + '<br>';
							str4 = str4 + 'task remcube. = ' + remcube + '<br>';
							str4 = str4 + 'remianingqty. = ' + remianingqty + '<br>';
							str4 = str4 + 'containerremcube. = ' + containerremcube + '<br>';	
							str4 = str4 + 'containerremweight. = ' + containerremweight + '<br>';

							nlapiLogExecution('DEBUG', 'Log inside While', str4);

							var currow = [containerLP,contsize,containerremcube,containerremweight,pickzone1];
							optmizationarray.push(currow);

						}	
					}
				}
			} 

			//if still there is some qty exists and the cube is less than the container(s) cube(no matching container),
			//then place it in the smallest container.
			nlapiLogExecution('DEBUG', 'remianingqty at end : ', remianingqty);	
			if(parseFloat(remianingqty)> 0){

				var k = containerslist.length;
				if (parseFloat(totaltaskqty)>parseFloat(remianingqty))
					contsize =  containerslist[k-1].getValue('Internalid');
				else
					contsize='';
				containerLP = GetMaxLPNo('1', '2',whsite);
				CreateMasterLPRecord(containerLP,contsize,remweight,remcube,uompackflag,whsite,LPparent);
				nlapiLogExecution('DEBUG', 'containerLP2 in buildcartonsforminandmaxdims ', containerLP);
				createNewPickTask(opentasklist,containerLP,remcube,remweight,contsize,remianingqty,pickzone1);	             
				cube = containerslist[k-1].getValue('custrecord_cubecontainer');
				weight = containerslist[k-1].getValue('custrecord_maxweight');
				remianingqty = 0;
			}

			if(parseFloat(remianingqty)==0)
			{
				//delete the actual record
				var id = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', opentasklist.getId());
				nlapiLogExecution('DEBUG', 'Deleted Actual Record : ', id);	
			}

		}
	}
	catch(exp) {
		nlapiLogExecution('DEBUG', 'Exception in buildcartonsforminandmaxdims : ', exp);		
	}
	nlapiLogExecution('DEBUG', 'out of  buildcartonsforminandmaxdims Task Split', '');
}


function resetArray(optmizationarray,indxarray)
{
	nlapiLogExecution('DEBUG', 'Into resetArray');

	var newarray = new Array();

	for(x=0;optmizationarray!=null && x<optmizationarray.length;x++)
	{
		var boolfound = 'F';
		for(y=0;indxarray!=null && y<indxarray.length;y++)
		{
			var str4 = 'optmizationarray[x][0]. = ' + optmizationarray[x][0] + '<br>';
			str4 = str4 + 'indxarray[y]. = ' + indxarray[y] + '<br>';

			nlapiLogExecution('DEBUG', 'Array Comparison Log', str4);

			if(optmizationarray[x][0] == indxarray[y] )
			{
				boolfound='T';
			}
		}

		if(boolfound=='F')
		{
			var currow = [optmizationarray[x][0],optmizationarray[x][1],optmizationarray[x][2],optmizationarray[x][3]];
			newarray.push(currow);
		}
	}

	nlapiLogExecution('DEBUG', 'Out of resetArray');

	return newarray;
}

/**
 * 
 * @param oldTaskList
 * [0]  - containerlpno, [1]  - totalweight, [2]  - totalcube,  [3]  - fforderno,    [4]  - fforderinternalno, [5]  - sku,       [6]  - ebizskuno
 * [7]  - ebizwaveno,    [8]  - expqty,      [9]  - linenumber, [10] - lpno,         [11] - packcode ,         [12] - skustatus, [13] - statusflag   
 * [14] - tasktype,      [15] - uom,         [16] - uomlevel,   [17] - ebizreceiptno,[18] - beginlocation,     [19] - invrefno,  [20] - fromlpno
 * [21] - ebizordno,     [22] - wmslocation, [23] - ebizruleno, [24] - ebizmethodno, [25] - pickzone
 */
function createNewPickTask(oldTaskList,containerLP,cube,weight,containersize,exptdqty,pickzone){

	nlapiLogExecution('DEBUG', 'into createNewPickTask - exptdqty ',exptdqty);	

	if(oldTaskList!=null){

		if(exptdqty>0)
		{

			var openTaskRecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');

			openTaskRecord.setFieldValue('name', oldTaskList.getValue('name'));							// DELIVERY ORDER NAME
			openTaskRecord.setFieldValue('custrecord_ebiz_sku_no', oldTaskList.getValue('custrecord_ebiz_sku_no'));			// ITEM INTERNAL ID
			openTaskRecord.setFieldValue('custrecord_sku', oldTaskList.getValue('custrecord_sku'));					// ITEM NAME
			openTaskRecord.setFieldValue('custrecord_expe_qty', parseFloat(exptdqty).toFixed(5));			// FULFILMENT ORDER QUANTITY
			openTaskRecord.setFieldValue('custrecord_container_lp_no', containerLP);
			openTaskRecord.setFieldValue('custrecord_ebiz_lpno', containerLP);
			openTaskRecord.setFieldValue('custrecord_line_no', oldTaskList.getValue('custrecord_line_no'));
			openTaskRecord.setFieldValue('custrecord_tasktype', '3'); 						// TASK TYPE = PICK

			openTaskRecord.setFieldValue('custrecord_actbeginloc', oldTaskList.getValue('custrecord_actbeginloc'));
			openTaskRecord.setFieldValue('custrecord_ebiz_cntrl_no', oldTaskList.getValue('custrecord_ebiz_cntrl_no'));
			openTaskRecord.setFieldValue('custrecord_ebiz_order_no', oldTaskList.getValue('custrecord_ebiz_order_no'));
			openTaskRecord.setFieldValue('custrecord_ebiz_receipt_no', oldTaskList.getValue('custrecord_ebiz_receipt_no'));
			openTaskRecord.setFieldValue('custrecord_ebiz_wave_no',  oldTaskList.getValue('custrecord_ebiz_wave_no'));
			openTaskRecord.setFieldValue('custrecordact_begin_date',DateStamp());
			openTaskRecord.setFieldValue('custrecord_wms_status_flag', '9'); 				// STATUS FLAG = G

			openTaskRecord.setFieldValue('custrecord_from_lp_no', oldTaskList.getValue('custrecord_from_lp_no'));
			openTaskRecord.setFieldValue('custrecord_lpno', oldTaskList.getValue('custrecord_from_lp_no'));
			openTaskRecord.setFieldValue('custrecord_actualbegintime', TimeStamp());
			openTaskRecord.setFieldValue('custrecord_invref_no', oldTaskList.getValue('custrecord_invref_no'));
			openTaskRecord.setFieldValue('custrecord_packcode', oldTaskList.getValue('custrecord_packcode'));
			openTaskRecord.setFieldValue('custrecord_sku_status', oldTaskList.getValue('custrecord_sku_status'));
			openTaskRecord.setFieldValue('custrecord_uom_id', oldTaskList.getValue('custrecord_uom_id'));
			openTaskRecord.setFieldValue('custrecord_ebizrule_no', oldTaskList.getValue('custrecord_ebizrule_no'));
			openTaskRecord.setFieldValue('custrecord_ebizmethod_no', oldTaskList.getValue('custrecord_ebizmethod_no'));
			openTaskRecord.setFieldValue('custrecord_ebizzone_no', oldTaskList.getValue('custrecord_ebizzone_no'));
			//Case # 20126064� Start
			if(oldTaskList.getValue('custrecord_ebizzone_no')!=null && oldTaskList.getValue('custrecord_ebizzone_no')!='')
			{
				openTaskRecord.setFieldValue('custrecord_ebiz_zoneid', oldTaskList.getValue('custrecord_ebizzone_no'));
			}
			//Case # 20126064� End
			openTaskRecord.setFieldValue('custrecord_wms_location', oldTaskList.getValue('custrecord_wms_location'));						
			var currentContext = nlapiGetContext();  
			var currentUserID = currentContext.getUser();		
			openTaskRecord.setFieldValue('custrecord_ebizuser', currentUserID);		
			openTaskRecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);						
			//openTaskRecord.setFieldValue('custrecord_taskassignedto', currentUserID);
			openTaskRecord.setFieldValue('custrecord_uom_level', oldTaskList.getValue('custrecord_uom_level'));
			openTaskRecord.setFieldValue('custrecord_total_weight', parseFloat(weight).toFixed(5));
			openTaskRecord.setFieldValue('custrecord_totalcube', parseFloat(cube).toFixed(5));
			if(containersize!=null && containersize!='')
				openTaskRecord.setFieldValue('custrecord_container', containersize);
			openTaskRecord.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', oldTaskList.getValue('custrecord_ebiz_nsconfirm_ref_no'));

			openTaskRecord.setFieldValue('custrecord_skiptask', '0');

			openTaskRecord.setFieldValue('custrecord_batch_no', oldTaskList.getValue('custrecord_batch_no')); // LOT #
			nlapiLogExecution('DEBUG', 'custrecord_expirydate',oldTaskList.getValue('custrecord_expirydate'));
			if(oldTaskList.getValue('custrecord_expirydate') != null && oldTaskList.getValue('custrecord_expirydate') != '')
				openTaskRecord.setFieldValue('custrecord_expirydate', oldTaskList.getValue('custrecord_expirydate')); // expiry date #

			/***The below code is merged from Factory mation production account by Ganesh K on March 1st 2013 ***/
			if(oldTaskList.getValue('custrecord_ebizwmscarrier') != null && oldTaskList.getValue('custrecord_ebizwmscarrier') != '')
			{
				nlapiLogExecution('DEBUG', 'Old WMS Carrier ',oldTaskList.getValue('custrecord_ebizwmscarrier'));
				openTaskRecord.setFieldValue('custrecord_ebizwmscarrier', oldTaskList.getValue('custrecord_ebizwmscarrier'));
			}
			if(oldTaskList.getValue('custrecord_parent_sku_no') != null && oldTaskList.getValue('custrecord_parent_sku_no') != '')
			{
				nlapiLogExecution('DEBUG', 'Old Parent SKU ',oldTaskList.getValue('custrecord_parent_sku_no'));
				openTaskRecord.setFieldValue('custrecord_parent_sku_no', oldTaskList.getValue('custrecord_parent_sku_no'));
			}
			/*** Upto here ***/		



			//Code Added on 24th Feb 2014 
			if(oldTaskList.getValue('custrecord_ebiz_act_solocation')!=null && oldTaskList.getValue('custrecord_ebiz_act_solocation')!='')
			{
				openTaskRecord.setFieldValue('custrecord_ebiz_act_solocation', oldTaskList.getValue('custrecord_ebiz_act_solocation'));
			}
			//end

			nlapiSubmitRecord(openTaskRecord,false,true);
			nlapiLogExecution('DEBUG', 'Opentask updation Completed');
		}
	}
	nlapiLogExecution('DEBUG', 'out of createNewPickTask ','');
}

function updatesizeidinopentask(recordId, containerLP,sizeid){
	nlapiLogExecution('DEBUG', 'In to updatesizeidinopentask function : sizeid',sizeid);

	var fieldNames = new Array(); 
	fieldNames.push('custrecord_container');  
	fieldNames.push('custrecord_container_lp_no');

	var newValues = new Array(); 
	newValues.push(sizeid);
	newValues.push(containerLP);

	nlapiSubmitField('customrecord_ebiznet_trn_opentask', recordId, fieldNames, newValues);

	nlapiLogExecution('DEBUG', 'Out of updatesizeidinopentask function');
}

function getContainerSize(containerslist,totaltaskcube,totaltaskweight,cartonizationmethod,itemdims,opentasklist,whLocation)
{	
	var str = 'Container List. = ' + containerslist + '<br>';
	str = str + 'totaltaskcube. = ' + totaltaskcube + '<br>';	
	str = str + 'totaltaskweight. = ' + totaltaskweight + '<br>';
	str = str + 'cartonizationmethod. = ' + cartonizationmethod + '<br>';
	str = str + 'itemdims. = ' + itemdims + '<br>';
	str = str + 'opentasklist. = ' + opentasklist + '<br>';

	nlapiLogExecution('DEBUG', 'Into getContainerSize', str);

	if(isNaN(totaltaskcube))
		totaltaskcube=0;

	if(isNaN(totaltaskweight))
		totaltaskweight=0;

	var contsize='';

	for(l=(containerslist.length)-1;l>=0;l--)
	{  
		var containerlength = containerslist[l].getValue('custrecord_length');
		var containerwidth = containerslist[l].getValue('custrecord_widthcontainer');
		var containerheight = containerslist[l].getValue('custrecord_heightcontainer');
		var containercube = containerslist[l].getValue('custrecord_cubecontainer');
		var containerweight= containerslist[l].getValue('custrecord_maxweight');
		var containername = containerslist[l].getValue('custrecord_containername');
		var packfactor=containerslist[l].getValue('custrecord_packfactor');
		if(packfactor==null || packfactor=='')
			packfactor=0;
		var containeractcube=parseFloat(containercube)-(parseFloat(containercube)*(parseFloat(packfactor)/100));
		nlapiLogExecution('DEBUG', 'containeractcube',containeractcube);
		nlapiLogExecution('DEBUG', 'containerweight',containerweight);

		if(cartonizationmethod=='1') // Cube and Weight
		{
			nlapiLogExecution('DEBUG', 'Into Cube and Weight');
			if((containername!='SHIPASIS') && (parseFloat(containeractcube)>=parseFloat(totaltaskcube)) 
					&& (parseFloat(containerweight)>=parseFloat(totaltaskweight)))
			{
				contsize=containerslist[l].getValue('Internalid');
				//nlapiLogExecution('DEBUG', 'contsize',contsize);	
				return contsize;
			}
		}
		else if(cartonizationmethod=='2') // Weight
		{
			nlapiLogExecution('DEBUG', 'Into Weight');
			if((containername!='SHIPASIS') && (parseFloat(containerweight)>=parseFloat(totaltaskweight)))
			{
				contsize=containerslist[l].getValue('Internalid');
				//nlapiLogExecution('DEBUG', 'contsize',contsize);
				return contsize;
			}
		}
		else if(cartonizationmethod=='3') // Cube
		{
			nlapiLogExecution('DEBUG', 'Into Cube');
			if((containername!='SHIPASIS') && (parseFloat(containeractcube)>=parseFloat(totaltaskcube)))
			{
				contsize=containerslist[l].getValue('Internalid');
				//nlapiLogExecution('DEBUG', 'contsize',contsize);	
				return contsize;
			}
		}
		else if(cartonizationmethod=='5') // Max and Min
		{
			nlapiLogExecution('DEBUG', 'Into Min and Max');

			if(itemdims!=null && itemdims!='')
				nlapiLogExecution('DEBUG', 'itemdims',itemdims.length);

			if(itemdims!=null && itemdims!='' && itemdims.length>0)
			{
				var itemcube = itemdims[0].getValue('custrecord_ebizcube');
				var itemweight = itemdims[0].getValue('custrecord_ebizweight');
				var itemlength = itemdims[0].getValue('custrecord_ebizlength');
				var itemwidth = itemdims[0].getValue('custrecord_ebizwidth');
				var itemheight = itemdims[0].getValue('custrecord_ebizheight');

				var str = 'totaltaskcube. = ' + totaltaskcube + '<br>';				
				str = str + 'totaltaskweight. = ' + totaltaskweight + '<br>';
				str = str + 'containername. = ' + containername + '<br>';
				str = str + 'containerweight. = ' + containerweight + '<br>';	
				str = str + 'itemweight. = ' + itemweight + '<br>';
				str = str + 'containeractcube. = ' + containeractcube + '<br>';	
				str = str + 'itemcube. = ' + itemcube + '<br>';			
				str = str + 'itemlength. = ' + itemlength + '<br>';
				str = str + 'itemwidth. = ' + itemwidth + '<br>';
				str = str + 'itemheight. = ' + itemheight + '<br>';
				str = str + 'containerlength. = ' + containerlength + '<br>';
				str = str + 'containerwidth. = ' + containerwidth + '<br>';
				str = str + 'containerheight. = ' + containerheight + '<br>';

				nlapiLogExecution('DEBUG', 'Size Determination1', str);

				if(isNaN(itemcube))
					itemcube=0;

				if(isNaN(itemweight))
					itemweight=0;

				if(isNaN(itemlength))
					itemlength=0;

				if(isNaN(itemwidth))
					itemwidth=0;

				if(isNaN(itemheight))
					itemheight=0;

				if((containername!='SHIPASIS') && (parseFloat(totaltaskcube)<=parseFloat(containeractcube)) && 
						(parseFloat(totaltaskweight)<=parseFloat(containerweight)))
				{
					var itemmaxdim = Math.max(parseFloat(itemlength), parseFloat(itemwidth), parseFloat(itemheight));
					var itemmindim = Math.min(parseFloat(itemlength), parseFloat(itemwidth), parseFloat(itemheight));

					var containermaxdim = Math.max(parseFloat(containerlength), parseFloat(containerwidth), parseFloat(containerheight));
					var containermindim = Math.min(parseFloat(containerlength), parseFloat(containerwidth), parseFloat(containerheight));

					var str1 = 'itemmaxdim. = ' + itemmaxdim + '<br>';				
					str1 = str1 + 'itemmindim. = ' + itemmindim + '<br>';
					str1 = str1 + 'containermaxdim. = ' + containermaxdim + '<br>';
					str1 = str1 + 'containermindim. = ' + containermindim + '<br>';

					nlapiLogExecution('DEBUG', 'Size Determination2', str1);

					if(itemmaxdim<=containermaxdim && itemmindim<=containermindim)
					{
						contsize=containerslist[l].getValue('Internalid');
						//nlapiLogExecution('DEBUG', 'contsize',contsize);	
						return contsize;
					}
				}
			}
			else
			{
				if((containername!='SHIPASIS') && (parseFloat(containeractcube)>=parseFloat(totaltaskcube)) 
						&& (parseFloat(containerweight)>=parseFloat(totaltaskweight)))
				{
					var itemarray = new Array();
					var gitemmaxdim=0;
					var gitemmindim=0;
					for(t=0;t<opentasklist.length;t++)
					{ 
						itemarray.push(opentasklist[t].getValue('custrecord_ebiz_sku_no'));
					}
					var maxitemdimarr = new Array();
					var minitemdimarr = new Array();
					if(itemarray!=null && itemarray!='')
					{
						nlapiLogExecution('DEBUG', 'itemarray', itemarray);
						itemarray=removeDuplicateElement(itemarray);
						nlapiLogExecution('DEBUG', 'itemarray', itemarray);

						var allitemdimsarray = getdimsforallitems(itemarray,whLocation);

						for(s=0;allitemdimsarray!=null && s<allitemdimsarray.length;s++)
						{ 
							var itemlength = allitemdimsarray[s].getValue('custrecord_ebizlength');
							var itemwidth = allitemdimsarray[s].getValue('custrecord_ebizwidth');
							var itemheight = allitemdimsarray[s].getValue('custrecord_ebizheight');

							var itemmaxdim = Math.max(parseFloat(itemlength), parseFloat(itemwidth), parseFloat(itemheight));
							var itemmindim = Math.min(parseFloat(itemlength), parseFloat(itemwidth), parseFloat(itemheight));

							maxitemdimarr.push(itemmaxdim);
							minitemdimarr.push(itemmindim);
						}
					}

					nlapiLogExecution('DEBUG', 'maxitemdimarr', maxitemdimarr);
					maxitemdimarr.sort(function(a,b){return b - a;});
					nlapiLogExecution('DEBUG', 'maxitemdimarr', maxitemdimarr);
					nlapiLogExecution('DEBUG', 'minitemdimarr', minitemdimarr);
					minitemdimarr.sort(function(a,b){return a - b;});
					nlapiLogExecution('DEBUG', 'minitemdimarr', minitemdimarr);

					for(a=0;maxitemdimarr!=null&&a<maxitemdimarr.length;a++)
					{
						gitemmaxdim = maxitemdimarr[0];
					}

					for(b=0;minitemdimarr!=null&&b<minitemdimarr.length;b++)
					{
						gitemmindim = minitemdimarr[0];
					}

					if(isNaN(gitemmaxdim))
						gitemmaxdim=0;

					if(isNaN(gitemmindim))
						gitemmindim=0;

					var containermaxdim = Math.max(parseFloat(containerlength), parseFloat(containerwidth), parseFloat(containerheight));
					var containermindim = Math.min(parseFloat(containerlength), parseFloat(containerwidth), parseFloat(containerheight));

					var str1 = 'itemmaxdim. = ' + gitemmaxdim + '<br>';				
					str1 = str1 + 'itemmindim. = ' + gitemmindim + '<br>';
					str1 = str1 + 'containermaxdim. = ' + containermaxdim + '<br>';
					str1 = str1 + 'containermindim. = ' + containermindim + '<br>';

					nlapiLogExecution('DEBUG', 'Size Determination in MAX and MIN by order level', str1);

					if(gitemmaxdim<=containermaxdim && gitemmindim<=containermindim)
					{
						contsize=containerslist[l].getValue('Internalid');
						//nlapiLogExecution('DEBUG', 'contsize',contsize);	
						return contsize;
					}
				}
			}
		}

	}

	nlapiLogExecution('DEBUG', 'Out of getContainerSize (return Size) ',contsize);

	return contsize;
}

function getContainerSizeforOrder(containerslist,totaltaskcube,totaltaskweight,cartonizationmethod,itemdims,
		opentasklist,whLocation)
{	
	var str = 'Container List. = ' + containerslist + '<br>';
	str = str + 'totaltaskcube. = ' + totaltaskcube + '<br>';	
	str = str + 'totaltaskweight. = ' + totaltaskweight + '<br>';
	str = str + 'cartonizationmethod. = ' + cartonizationmethod + '<br>';
	str = str + 'whLocation. = ' + whLocation + '<br>';

	nlapiLogExecution('DEBUG', 'Into getContainerSizeforOrder', str);

	if(isNaN(totaltaskcube))
		totaltaskcube=0;

	if(isNaN(totaltaskweight))
		totaltaskweight=0;

	var contsize='';


	for(l=0;l<containerslist.length;l++)
	{  
		var containerlength = containerslist[l].getValue('custrecord_length');
		var containerwidth = containerslist[l].getValue('custrecord_widthcontainer');
		var containerheight = containerslist[l].getValue('custrecord_heightcontainer');
		var containercube = containerslist[l].getValue('custrecord_cubecontainer');
		var containerweight= containerslist[l].getValue('custrecord_maxweight');
		var containername = containerslist[l].getValue('custrecord_containername');
		var packfactor=containerslist[l].getValue('custrecord_packfactor');
		if(packfactor==null || packfactor=='')
			packfactor=0;
		var containeractcube=parseFloat(containercube)-(parseFloat(containercube)*(parseFloat(packfactor)/100));
		nlapiLogExecution('DEBUG', 'containeractcube',containeractcube);
		nlapiLogExecution('DEBUG', 'containerweight',containerweight);

		if(cartonizationmethod=='1') // Cube and Weight
		{
			nlapiLogExecution('DEBUG', 'Into Cube and Weight');
			if((containername!='SHIPASIS') && (parseFloat(containeractcube)>=parseFloat(totaltaskcube)) 
					&& (parseFloat(containerweight)>=parseFloat(totaltaskweight)))
			{
				contsize=containerslist[l].getValue('Internalid');
				//nlapiLogExecution('DEBUG', 'contsize',contsize);	
				return contsize;
			}
		}
		else if(cartonizationmethod=='2') // Weight
		{
			nlapiLogExecution('DEBUG', 'Into Weight');
			if((containername!='SHIPASIS') && (parseFloat(containerweight)>=parseFloat(totaltaskweight)))
			{
				contsize=containerslist[l].getValue('Internalid');
				//nlapiLogExecution('DEBUG', 'contsize',contsize);
				return contsize;
			}
		}
		else if(cartonizationmethod=='3') // Cube
		{
			nlapiLogExecution('DEBUG', 'Into Cube');
			if((containername!='SHIPASIS') && (parseFloat(containeractcube)>=parseFloat(totaltaskcube)))
			{
				contsize=containerslist[l].getValue('Internalid');
				//nlapiLogExecution('DEBUG', 'contsize',contsize);	
				return contsize;
			}
		}
		else if(cartonizationmethod=='5') // Max and Min
		{
			nlapiLogExecution('DEBUG', 'Into Min and Max');

			if(itemdims!=null && itemdims!='' && itemdims.length>0)
			{
				var itemcube = itemdims[0].getValue('custrecord_ebizcube');
				var itemweight = itemdims[0].getValue('custrecord_ebizweight');
				var itemlength = itemdims[0].getValue('custrecord_ebizlength');
				var itemwidth = itemdims[0].getValue('custrecord_ebizwidth');
				var itemheight = itemdims[0].getValue('custrecord_ebizheight');

				var str = 'totaltaskcube. = ' + totaltaskcube + '<br>';				
				str = str + 'totaltaskweight. = ' + totaltaskweight + '<br>';
				str = str + 'containername. = ' + containername + '<br>';
				str = str + 'containerweight. = ' + containerweight + '<br>';	
				str = str + 'itemweight. = ' + itemweight + '<br>';
				str = str + 'containeractcube. = ' + containeractcube + '<br>';	
				str = str + 'itemcube. = ' + itemcube + '<br>';			
				str = str + 'itemlength. = ' + itemlength + '<br>';
				str = str + 'itemwidth. = ' + itemwidth + '<br>';
				str = str + 'itemheight. = ' + itemheight + '<br>';
				str = str + 'containerlength. = ' + containerlength + '<br>';
				str = str + 'containerwidth. = ' + containerwidth + '<br>';
				str = str + 'containerheight. = ' + containerheight + '<br>';

				nlapiLogExecution('DEBUG', 'Size Determination1', str);

				if(isNaN(itemcube))
					itemcube=0;

				if(isNaN(itemweight))
					itemweight=0;

				if(isNaN(itemlength))
					itemlength=0;

				if(isNaN(itemwidth))
					itemwidth=0;

				if(isNaN(itemheight))
					itemheight=0;

				if((containername!='SHIPASIS') && (parseFloat(totaltaskcube)<=parseFloat(containeractcube)) && 
						(parseFloat(totaltaskweight)<=parseFloat(containerweight)))
				{
					var itemmaxdim = Math.max(parseFloat(itemlength), parseFloat(itemwidth), parseFloat(itemheight));
					var itemmindim = Math.min(parseFloat(itemlength), parseFloat(itemwidth), parseFloat(itemheight));

					var containermaxdim = Math.max(parseFloat(containerlength), parseFloat(containerwidth), parseFloat(containerheight));
					var containermindim = Math.min(parseFloat(containerlength), parseFloat(containerwidth), parseFloat(containerheight));

					var str1 = 'itemmaxdim. = ' + itemmaxdim + '<br>';				
					str1 = str1 + 'itemmindim. = ' + itemmindim + '<br>';
					str1 = str1 + 'containermaxdim. = ' + containermaxdim + '<br>';
					str1 = str1 + 'containermindim. = ' + containermindim + '<br>';

					nlapiLogExecution('DEBUG', 'Size Determination2', str1);

					if(itemmaxdim<=containermaxdim && itemmindim<=containermindim)
					{
						contsize=containerslist[l].getValue('Internalid');
						//nlapiLogExecution('DEBUG', 'contsize',contsize);	
						return contsize;
					}
				}
			}
			else
			{
				if((containername!='SHIPASIS') && (parseFloat(containeractcube)>=parseFloat(totaltaskcube)) 
						&& (parseFloat(containerweight)>=parseFloat(totaltaskweight)))
				{
					var itemarray = new Array();
					var gitemmaxdim=0;
					var gitemmindim=0;
					for(t=0;t<opentasklist.length;t++)
					{ 
						itemarray.push(opentasklist[t].getValue('custrecord_ebiz_sku_no'));
					}
					var maxitemdimarr = new Array();
					var minitemdimarr = new Array();
					if(itemarray!=null && itemarray!='')
					{
						nlapiLogExecution('DEBUG', 'itemarray', itemarray);
						itemarray=removeDuplicateElement(itemarray);
						nlapiLogExecution('DEBUG', 'itemarray', itemarray);

						var allitemdimsarray = getdimsforallitems(itemarray,whLocation);

						for(s=0;allitemdimsarray!=null && s<allitemdimsarray.length;s++)
						{ 
							var itemlength = allitemdimsarray[s].getValue('custrecord_ebizlength');
							var itemwidth = allitemdimsarray[s].getValue('custrecord_ebizwidth');
							var itemheight = allitemdimsarray[s].getValue('custrecord_ebizheight');

							var itemmaxdim = Math.max(parseFloat(itemlength), parseFloat(itemwidth), parseFloat(itemheight));
							var itemmindim = Math.min(parseFloat(itemlength), parseFloat(itemwidth), parseFloat(itemheight));

							maxitemdimarr.push(itemmaxdim);
							minitemdimarr.push(itemmindim);
						}
					}

					nlapiLogExecution('DEBUG', 'maxitemdimarr', maxitemdimarr);
					maxitemdimarr.sort(function(a,b){return b - a;});
					nlapiLogExecution('DEBUG', 'maxitemdimarr', maxitemdimarr);
					nlapiLogExecution('DEBUG', 'minitemdimarr', minitemdimarr);
					minitemdimarr.sort(function(a,b){return a - b;});
					nlapiLogExecution('DEBUG', 'minitemdimarr', minitemdimarr);

					for(a=0;maxitemdimarr!=null&&a<maxitemdimarr.length;a++)
					{
						gitemmaxdim = maxitemdimarr[0];
					}

					for(b=0;minitemdimarr!=null&&b<minitemdimarr.length;b++)
					{
						gitemmindim = minitemdimarr[0];
					}

					if(isNaN(gitemmaxdim))
						gitemmaxdim=0;

					if(isNaN(gitemmindim))
						gitemmindim=0;

					var containermaxdim = Math.max(parseFloat(containerlength), parseFloat(containerwidth), parseFloat(containerheight));
					var containermindim = Math.min(parseFloat(containerlength), parseFloat(containerwidth), parseFloat(containerheight));

					var str1 = 'itemmaxdim. = ' + gitemmaxdim + '<br>';				
					str1 = str1 + 'itemmindim. = ' + gitemmindim + '<br>';
					str1 = str1 + 'containermaxdim. = ' + containermaxdim + '<br>';
					str1 = str1 + 'containermindim. = ' + containermindim + '<br>';

					nlapiLogExecution('DEBUG', 'Size Determination in MAX and MIN by order level', str1);

					if(gitemmaxdim<=containermaxdim && gitemmindim<=containermindim)
					{
						contsize=containerslist[l].getValue('Internalid');
						//nlapiLogExecution('DEBUG', 'contsize',contsize);	
						return contsize;
					}
				}
			}
		}

	}

	nlapiLogExecution('DEBUG', 'Out of getContainerSizeforOrder (return Size) ',contsize);

	return contsize;
}


function getSizefromUsedCartons(containerslist,totalcube,totalweight,cartonizationmethod,itemdims,arrusedcartons,vpckzone,cartonstrategy)
{	
	var str = 'Container List. = ' + containerslist + '<br>';
	str = str + 'cartonizationmethod. = ' + cartonizationmethod + '<br>';
	str = str + 'totalcube. = ' + totalcube + '<br>';	
	str = str + 'totalweight. = ' + totalweight + '<br>';	
	str = str + 'vpckzone. = ' + vpckzone + '<br>';	

	nlapiLogExecution('DEBUG', 'Into getSizefromUsedCartons', str);

	if(isNaN(totalcube))
		totalcube=0;

	if(isNaN(totalweight))
		totalweight=0;

	var contsizearray = new Array();

	if(arrusedcartons!=null && arrusedcartons!='' && arrusedcartons.length>0)
	{		
		nlapiLogExecution('DEBUG', 'arrusedcartons length', arrusedcartons.length);

		for(k=0;k<arrusedcartons.length;k++)
		{  
			var contremcube = arrusedcartons[k][2];
			var contremwght = arrusedcartons[k][3];
			var size = arrusedcartons[k][1];
			var cartonlp = arrusedcartons[k][0];
			var vusdzone = arrusedcartons[k][4];

			var str = 'cartonlp. = ' + cartonlp + '<br>';
			str = str + 'size. = ' + size + '<br>';
			str = str + 'totalcube. = ' + totalcube + '<br>';	
			str = str + 'totalweight. = ' + totalweight + '<br>';	
			str = str + 'contremwght. = ' + contremwght + '<br>';	
			str = str + 'contremcube. = ' + contremcube + '<br>';	

			nlapiLogExecution('DEBUG', 'Size Determination1', str);
			if(vusdzone==vpckzone)
			{
				if((parseFloat(totalcube)<=parseFloat(contremcube)) && 
						(parseFloat(totalweight)<=parseFloat(contremwght)))
				{
					for(l=0;l<containerslist.length;l++)
					{  
						if(size == containerslist[l].getValue('Internalid'))
						{
							var containername = containerslist[l].getValue('custrecord_containername');
							var containerlength = containerslist[l].getValue('custrecord_length');
							var containerwidth = containerslist[l].getValue('custrecord_widthcontainer');
							var containerheight = containerslist[l].getValue('custrecord_heightcontainer');

							var containermaxdim = Math.max(parseFloat(containerlength), parseFloat(containerwidth), parseFloat(containerheight));
							var containermindim = Math.min(parseFloat(containerlength), parseFloat(containerwidth), parseFloat(containerheight));

							if(itemdims!=null && itemdims!='' && itemdims.length>0)
							{
								var itemcube = itemdims[0].getValue('custrecord_ebizcube');
								var itemweight = itemdims[0].getValue('custrecord_ebizweight');
								var itemlength = itemdims[0].getValue('custrecord_ebizlength');
								var itemwidth = itemdims[0].getValue('custrecord_ebizwidth');
								var itemheight = itemdims[0].getValue('custrecord_ebizheight');

								var itemmaxdim = Math.max(parseFloat(itemlength), parseFloat(itemwidth), parseFloat(itemheight));
								var itemmindim = Math.min(parseFloat(itemlength), parseFloat(itemwidth), parseFloat(itemheight));

								var str = 'totalcube. = ' + totalcube + '<br>';				
								str = str + 'totalweight. = ' + totalweight + '<br>';
								str = str + 'containername. = ' + containername + '<br>';
								str = str + 'contremwght. = ' + contremwght + '<br>';	
								str = str + 'itemweight. = ' + itemweight + '<br>';
								str = str + 'contremcube. = ' + contremcube + '<br>';	
								str = str + 'itemcube. = ' + itemcube + '<br>';			
								str = str + 'itemlength. = ' + itemlength + '<br>';
								str = str + 'itemwidth. = ' + itemwidth + '<br>';
								str = str + 'itemheight. = ' + itemheight + '<br>';
								str = str + 'containerlength. = ' + containerlength + '<br>';
								str = str + 'containerwidth. = ' + containerwidth + '<br>';
								str = str + 'containerheight. = ' + containerheight + '<br>';
								str = str + 'itemmaxdim. = ' + itemmaxdim + '<br>';
								str = str + 'itemmindim. = ' + itemmindim + '<br>';
								str = str + 'containermaxdim. = ' + containermaxdim + '<br>';
								str = str + 'containermindim. = ' + containermindim + '<br>';


								nlapiLogExecution('DEBUG', 'Size Determination2', str);

								if(isNaN(itemcube))
									itemcube=0;

								if(isNaN(itemweight))
									itemweight=0;

								if(isNaN(itemlength))
									itemlength=0;

								if(isNaN(itemwidth))
									itemwidth=0;

								if(isNaN(itemheight))
									itemheight=0;

								if(itemmaxdim<=containermaxdim && itemmindim<=containermindim)
								{
									var newcontremcube = parseFloat(contremcube)-parseFloat(totalcube);
									var newcontremwght = parseFloat(contremwght)-parseFloat(totalweight);
									var currow = [cartonlp,size,newcontremcube,newcontremwght];
									contsizearray.push(currow);
									nlapiLogExecution('DEBUG', 'Out of getSizefromUsedCartons (return Size) ',contsizearray);
									return contsizearray;
								}
							}
						}
					}
				}
			}
		}
	}

	nlapiLogExecution('DEBUG', 'Out of getSizefromUsedCartons (return Size) ',contsizearray);

	return contsizearray;
}

function getTaskDetailsforCartonization(vEbizOrdNo,vEbizMethodNo)
{
	var str = 'Ord No. = ' + vEbizOrdNo + '<br>';
	str = str + 'Method No. = ' + vEbizMethodNo + '<br>';	

	nlapiLogExecution('DEBUG', 'Into getTaskDetailsforCartonization', str);

	var opentasks = new Array();

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', vEbizOrdNo));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
	filters.push(new nlobjSearchFilter('custrecord_container', null, 'anyof', ['@NONE@']));
	filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'noneof', ['@NONE@']));
	filters.push(new nlobjSearchFilter('custrecord_ebizmethod_no', null, 'is', vEbizMethodNo));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[1] = new nlobjSearchColumn('custrecord_total_weight');
	columns[2] = new nlobjSearchColumn('custrecord_totalcube');	
	columns[3] = new nlobjSearchColumn('name');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[5] = new nlobjSearchColumn('custrecord_sku');
	columns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[7] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
	columns[8] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[9] = new nlobjSearchColumn('custrecord_line_no');
	columns[10] = new nlobjSearchColumn('custrecord_lpno');
	columns[11] = new nlobjSearchColumn('custrecord_packcode');
	columns[12] = new nlobjSearchColumn('custrecord_sku_status');
	columns[13] = new nlobjSearchColumn('custrecord_wms_status_flag');
	columns[14] = new nlobjSearchColumn('custrecord_tasktype');	
	columns[15] = new nlobjSearchColumn('custrecord_uom_id');
	columns[16] = new nlobjSearchColumn('custrecord_uom_level');
	columns[17] = new nlobjSearchColumn('custrecord_ebiz_receipt_no');
	columns[18] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[19] = new nlobjSearchColumn('custrecord_invref_no');	
	columns[20] = new nlobjSearchColumn('custrecord_from_lp_no');
	columns[21] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[22] = new nlobjSearchColumn('custrecord_wms_location');
	columns[23] = new nlobjSearchColumn('custrecord_ebizrule_no');	
	columns[24] = new nlobjSearchColumn('custrecord_ebizmethod_no');
	columns[25] = new nlobjSearchColumn('custrecord_ebizzone_no');

	opentasks = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	

	nlapiLogExecution('DEBUG', 'out of getTaskDetailsforCartonization', '');

	return opentasks;
}

function getPickTaskDetailsforCartonization(vwaveno,vEbizOrdNo)
{
	var str = 'Ord No. = ' + vEbizOrdNo + '<br>';
	str = str + 'Wave No. = ' + vwaveno + '<br>';	

	nlapiLogExecution('DEBUG', 'Into getPickTaskDetailsforCartonization', str);

	var opentasks = new Array();

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', vEbizOrdNo));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
	filters.push(new nlobjSearchFilter('custrecord_container', null, 'anyof', ['@NONE@']));
	filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'noneof', ['@NONE@']));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', vwaveno));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));
	//Case # 20126983 start (System is not generating the Pick LP as per the configured LP range)
	filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'isempty'));
	//Case # 20126983 End

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
	columns[1] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[2] = new nlobjSearchColumn('custrecord_total_weight');
	columns[3] = new nlobjSearchColumn('custrecord_totalcube');	
	columns[4] = new nlobjSearchColumn('name');
	columns[5] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[6] = new nlobjSearchColumn('custrecord_sku');
	columns[7] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[8] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
	columns[9] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[10] = new nlobjSearchColumn('custrecord_line_no');
	columns[11] = new nlobjSearchColumn('custrecord_lpno');
	columns[12] = new nlobjSearchColumn('custrecord_packcode');
	columns[13] = new nlobjSearchColumn('custrecord_sku_status');
	columns[14] = new nlobjSearchColumn('custrecord_wms_status_flag');
	columns[15] = new nlobjSearchColumn('custrecord_tasktype');	
	columns[16] = new nlobjSearchColumn('custrecord_uom_id');
	columns[17] = new nlobjSearchColumn('custrecord_uom_level');
	columns[18] = new nlobjSearchColumn('custrecord_ebiz_receipt_no');
	columns[19] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[20] = new nlobjSearchColumn('custrecord_invref_no');	
	columns[21] = new nlobjSearchColumn('custrecord_from_lp_no');
	columns[22] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[23] = new nlobjSearchColumn('custrecord_wms_location');
	columns[24] = new nlobjSearchColumn('custrecord_ebizrule_no');	
	columns[25] = new nlobjSearchColumn('custrecord_ebizmethod_no');
	columns[26] = new nlobjSearchColumn('custrecord_ebizzone_no');
	columns[27] = new nlobjSearchColumn('custrecord_ebizwmscarrier');
	columns[28] = new nlobjSearchColumn('custrecord_batch_no');
	columns[29] = new nlobjSearchColumn('custrecord_expirydate');
	columns[30] = new nlobjSearchColumn('custrecord_parent_sku_no');
	columns[31] = new nlobjSearchColumn('custrecord_ebiz_act_solocation');
	columns[0].setSort();
	columns[22].setSort();
	columns[25].setSort();
	columns[26].setSort();
	columns[27].setSort();

	opentasks = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	

	nlapiLogExecution('DEBUG', 'out of getPickTaskDetailsforCartonization', '');

	return opentasks;
}

function getCubeanddWeightbyMethod(vwaveno,vEbizOrdNo,vmethodno)
{
	var str = 'Ord No. = ' + vEbizOrdNo + '<br>';
	str = str + 'Wave No. = ' + vwaveno + '<br>';	
	str = str + 'Method No. = ' + vmethodno + '<br>';	

	nlapiLogExecution('DEBUG', 'Into getCubeanddWeightbyMethod', str);

	var opentasks = new Array();

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', vEbizOrdNo));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
	filters.push(new nlobjSearchFilter('custrecord_container', null, 'anyof', ['@NONE@']));
	filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'noneof', ['@NONE@']));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', vwaveno));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));
	filters.push(new nlobjSearchFilter('custrecord_ebizmethod_no', null, 'is', vmethodno));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_total_weight',null,'sum');
	columns[1] = new nlobjSearchColumn('custrecord_totalcube',null,'sum');	
	columns[2] = new nlobjSearchColumn('custrecord_ebizmethod_no',null,'group');

	opentasks = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	

	nlapiLogExecution('DEBUG', 'out of getCubeanddWeightbyMethod', '');

	return opentasks;
}

function getPickTaskDetailsforSizeOptimization(vwaveno,vEbizOrdNo)
{
	var str = 'Ord No. = ' + vEbizOrdNo + '<br>';
	str = str + 'Wave No. = ' + vwaveno + '<br>';	

	nlapiLogExecution('DEBUG', 'Into getPickTaskDetailsforSizeOptimization', str);

	var opentasks = new Array();

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', vEbizOrdNo));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
	filters.push(new nlobjSearchFilter('custrecord_container', null, 'noneof', ['@NONE@']));
	filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'noneof', ['@NONE@']));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', vwaveno));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
	columns[1] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[2] = new nlobjSearchColumn('custrecord_total_weight');
	columns[3] = new nlobjSearchColumn('custrecord_totalcube');	
	columns[4] = new nlobjSearchColumn('name');
	columns[5] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[6] = new nlobjSearchColumn('custrecord_sku');
	columns[7] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[8] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
	columns[9] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[10] = new nlobjSearchColumn('custrecord_line_no');
	columns[11] = new nlobjSearchColumn('custrecord_lpno');
	columns[12] = new nlobjSearchColumn('custrecord_packcode');
	columns[13] = new nlobjSearchColumn('custrecord_sku_status');
	columns[14] = new nlobjSearchColumn('custrecord_wms_status_flag');
	columns[15] = new nlobjSearchColumn('custrecord_tasktype');	
	columns[16] = new nlobjSearchColumn('custrecord_uom_id');
	columns[17] = new nlobjSearchColumn('custrecord_uom_level');
	columns[18] = new nlobjSearchColumn('custrecord_ebiz_receipt_no');
	columns[19] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[20] = new nlobjSearchColumn('custrecord_invref_no');	
	columns[21] = new nlobjSearchColumn('custrecord_from_lp_no');
	columns[22] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[23] = new nlobjSearchColumn('custrecord_wms_location');
	columns[24] = new nlobjSearchColumn('custrecord_ebizrule_no');	
	columns[25] = new nlobjSearchColumn('custrecord_ebizmethod_no');
	columns[26] = new nlobjSearchColumn('custrecord_ebizzone_no');
	columns[27] = new nlobjSearchColumn('custrecord_ebizwmscarrier');

	columns[1].setSort();

	opentasks = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	

	nlapiLogExecution('DEBUG', 'out of getPickTaskDetailsforSizeOptimization', '');

	return opentasks;
}

function getSKUList(fulfillOrderList){
	var skuList = new Array();

	if(fulfillOrderList != null && fulfillOrderList.length > 0){
		for(var i = 0; i < fulfillOrderList.length; i++){
			var currSKU = fulfillOrderList[i][6];
			var orderQty = fulfillOrderList[i][9];
			var currSKUtext = fulfillOrderList[i][5];
			var itemType = nlapiLookupField('item', currSKU, 'recordType');
			nlapiLogExecution('DEBUG', 'itemType', itemType);
			nlapiLogExecution('DEBUG', 'currSKU', currSKU);
			if(itemType=='kititem' || itemType=='itemgroup' )
			{
				var searchresultsitem = nlapiLoadRecord(itemType, currSKU);
				var SkuNo=searchresultsitem.getFieldValue('itemid');
				var recCount=0; 
				var filters = new Array(); 
				//filters[0] = new nlobjSearchFilter('itemid', null, 'is', request.getParameter("custparam_item"));//kit_id is the parameter name 
				filters[0] = new nlobjSearchFilter('itemid', null, 'is', SkuNo);//kit_id is the parameter name 
				nlapiLogExecution('DEBUG', 'SkuNo', SkuNo);
				//	filters[1] = new nlobjSearchFilter('type', null, 'anyof', 'kititem');//For kit/package type Items 
				var columns1 = new Array(); 
				columns1[0] = new nlobjSearchColumn( 'memberitem' ); 
				//  columns1[0] = new nlobjSearchColumn( 'internalid' ); 
				columns1[1] = new nlobjSearchColumn( 'memberquantity' );

				var searchresults = nlapiSearchRecord( 'item', null, filters, columns1 ); 			
				for(var i=0; i<searchresults.length;i++) 
				{ 
					currSKU = searchresults[i].getValue('memberitem');
					orderQty = searchresults[i].getValue('memberquantity');
					nlapiLogExecution('DEBUG','strItem',currSKU);
					nlapiLogExecution('DEBUG','strQty',orderQty);
					updateSKUList(skuList, currSKU, orderQty);
				}
			}
			else
			{
				updateSKUList(skuList, currSKU, orderQty);
			}


		}
	}

	return skuList;
}

function updateSKUList(skuList, sku, orderQty){
	var skuFound = false;

	// Search for the SKU in the SKU list and add the order quantity if the SKU already exists
	for(var i = 0; i < skuList.length; i++){
		if(parseFloat(skuList[i][0]) == parseFloat(sku)){
			skuList[i][1] = parseFloat(skuList[i][1]) + parseFloat(orderQty);
			skuFound = true;
		}
	}

	// Adding a new item to the list if the SKU is not found in this list
	if(!skuFound){
		skuList[skuList.length] = [sku, orderQty];
	}
}

function getAllLocnGroups(allocConfigDtls){
	nlapiLogExecution('DEBUG', 'into getAllLocnGroups : allocConfigDtls  ', allocConfigDtls);
	var locnGroupListForAll = new Array();

	if(allocConfigDtls != null && allocConfigDtls.length > 0){
		for(var i = 0; i < allocConfigDtls.length; i++){
			var tempLocnGroupList = allocConfigDtls[i][8];
			if(tempLocnGroupList != null && tempLocnGroupList != '')
				locnGroupListForAll.push(tempLocnGroupList);
		}
	}

	nlapiLogExecution('DEBUG', 'out of getAllLocnGroups : locnGroupListForAll  ', locnGroupListForAll);

	return locnGroupListForAll;
}

function getItemList(skuList){
	var itemList = new Array();

	if (skuList != null && skuList.length > 0){
		for (var i = 0; i < skuList.length; i++){
			itemList.push(skuList[i][0]);
			nlapiLogExecution('DEBUG', 'Item', skuList[i][0]);
		}
	} 

	return itemList;
}

function getItemDimensions(skuList,DOwhLocation){
	nlapiLogExecution('DEBUG','into getItemDimensions ', skuList.length);
	var itemDimensionList = new Array();
	var itemDimCount = 0;

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', skuList));		// ALL SKUs IN SKU LIST
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));							// ACTIVE RECORDS ONLY
	filters.push(new nlobjSearchFilter('custrecord_ebiz_notallowedforpicking', null, 'is', 'F'));	
	if(DOwhLocation!=null && DOwhLocation!='' && DOwhLocation!='null')
		filters.push(new nlobjSearchFilter('custrecord_ebizsiteskudim', null, 'anyof', ['@NONE@', DOwhLocation]));	

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');								// ITEM/SKU
	columns[1] = new nlobjSearchColumn('custrecord_ebizuomskudim');								// UOM
	columns[2] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim');						// UOM LEVEL
	columns[3] = new nlobjSearchColumn('custrecord_ebizqty');									// QUANTITY
	columns[4] = new nlobjSearchColumn('custrecord_ebizbaseuom');								// BASE UOM FLAG
	columns[5] = new nlobjSearchColumn('custrecord_ebizdims_packflag');							// PACK FLAG	
	columns[6] = new nlobjSearchColumn('custrecord_ebizweight');								// WEIGHT	
	columns[7] = new nlobjSearchColumn('custrecord_ebizcube');	
	columns[8] = new nlobjSearchColumn('custrecord_ebiz_dims_bulkpick');	// CUBE	
	columns[2].setSort(false);
	// SEARCH FROM ITEM DIMENSIONS
	var searchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

	if(searchResults != null && searchResults.length > 0){
		for(var i = 0; i < searchResults.length; i++){
			var sku = searchResults[i].getValue('custrecord_ebizitemdims');
			var uom = searchResults[i].getValue('custrecord_ebizuomskudim');
			var uomLevel = searchResults[i].getValue('custrecord_ebizuomlevelskudim');
			var qty = searchResults[i].getValue('custrecord_ebizqty');
			var baseUOMFlag = searchResults[i].getValue('custrecord_ebizbaseuom');
			var packflag = searchResults[i].getValue('custrecord_ebizdims_packflag');
			var weight = searchResults[i].getValue('custrecord_ebizweight');
			var cube = searchResults[i].getValue('custrecord_ebizcube');
			var vbulkpick = searchResults[i].getValue('custrecord_ebiz_dims_bulkpick');

			var skuDimRecord = [sku, uom, uomLevel, qty, baseUOMFlag, packflag,weight,cube,uomLevel,vbulkpick];
			itemDimensionList[itemDimCount++] = skuDimRecord;
		}
	}
	nlapiLogExecution('DEBUG','out of getItemDimensions ', skuList.length);
	return itemDimensionList;
}

function getItemTypesForList(itemList){
	var itemTypeArray = new Array();
	var itemTypeCount = 0;

	for(var i = 0; i < itemList.length; i++){
		var itemType = nlapiLookupField('item', itemList[i], 'recordType');
		var currentRow = [itemList[i], itemType];
		itemTypeArray[itemTypeCount] = currentRow;
		itemTypeCount=itemTypeCount+1;
	}

	return itemTypeArray;
}

function getItemGroupAndFamily(itemTypeArray){
	var itemGroupFamilyArray = new Array();
	var itemGroupFamilyCount = 0;

	for(var i = 0; i < itemTypeArray.length; i++){
		var itemTypeElement = itemTypeArray[i];
		var item = itemTypeElement[0];
		var itemType = itemTypeElement[1];
		var itemRecord = getItemRecord(item, itemType);

		var currentRow = [item, itemType, itemRecord.getFieldValue('custitem_item_group'),
		                  itemRecord.getFieldValue('custitem_item_family')];
		itemGroupFamilyArray[itemGroupFamilyCount++] = currentRow;
	}

	return itemGroupFamilyArray;
}

function getItemRecord(item, itemType){
	var itemRecord = null;

	//This code is generalized to work for any kind of item types on 100411 ..Sudheer &Ganesh
	itemRecord = nlapiLoadRecord(itemType, item);


	/*if(itemType == 'inventoryitem')
		itemRecord = nlapiLoadRecord('inventoryitem', item);
	else if(itemType == 'assemblyitem')
		itemRecord = nlapiLoadRecord('assemblyitem', item);
	else if (itemType == 'lotnumberedinventoryitem')
		itemRecord = nlapiLoadRecord('lotnumberedinventoryitem', item);
	else if (ItemType == 'serializedinventoryitem')
		itemRecord = nlapiLoadRecord('serializedinventoryitem', item);
	else if (ItemType == 'lotnumberedassemblyitem')
		itemRecord = nlapiLoadRecord('serializedinventoryitem', item);
	 */
	return itemRecord;
}

function getListForAllPickZones(){

	nlapiLogExecution('DEBUG', 'into getListForAllPickZones','');
	var pickRuleList = new Array();
	var pickRuleCount = 0;

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizruleidpick');
	columns[1] = new nlobjSearchColumn('custrecord_ebizpickmethod');
	columns[2] = new nlobjSearchColumn('custrecord_ebizpickzonerul');
	columns[3] = new nlobjSearchColumn('custrecord_ebizlocationgrouppickrul');
	columns[4] = new nlobjSearchColumn('custrecord_ebizlocationpickrul');
	//columns[5] = new nlobjSearchColumn('custrecord_ebizsequencenopickrul');
	columns[5]=new nlobjSearchColumn('formulanumeric');
	columns[5].setFormula("TO_NUMBER({custrecord_ebizsequencenopickrul})").setSort();
	columns[6] = new nlobjSearchColumn('custrecord_ebizskufamilypickrul');
	columns[7] = new nlobjSearchColumn('custrecord_ebizskugrouppickrul');
	columns[8] = new nlobjSearchColumn('custrecord_ebizskupickrul');
	columns[9] = new nlobjSearchColumn('custrecord_ebizskuinfo1');
	columns[10] = new nlobjSearchColumn('custrecord_ebizskuinfo2');
	columns[11] = new nlobjSearchColumn('custrecord_ebizskuinfo3');
	columns[12] = new nlobjSearchColumn('custrecord_ebizpackcodepickrul');
	columns[13] = new nlobjSearchColumn('custrecord_ebizuomlevelpickrul');
	columns[14] = new nlobjSearchColumn('custrecord_ebizskustatspickrul');
	columns[15] = new nlobjSearchColumn('custrecord_ebizordertypepickrul');
	columns[16] = new nlobjSearchColumn('custrecord_ebizuompickrul');
	columns[17] = new nlobjSearchColumn('custrecord_allocation_strategy','custrecord_ebizpickmethod');
	columns[18] = new nlobjSearchColumn('custrecord_ebizpickfacelocation','custrecord_ebizpickmethod');
	columns[19] = new nlobjSearchColumn('custrecord_ebizcreatecluster','custrecord_ebizpickmethod');
	columns[20] = new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxorders','custrecord_ebizpickmethod');
	columns[21] = new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxpicks','custrecord_ebizpickmethod');
	columns[22] = new nlobjSearchColumn('custrecord_ebizabcvel');
	columns[23] = new nlobjSearchColumn('custrecord_cartonization_method','custrecord_ebizpickmethod');
	columns[24] = new nlobjSearchColumn('custrecord_ebizsitepickrule');
	//columns[5].setSort();

	var pickRuleSearchResult = new nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters, columns);

	if(pickRuleSearchResult != null){
		for (var i=0; i<pickRuleSearchResult.length; i++){
			var pickRuleId = pickRuleSearchResult[i].getValue('custrecord_ebizruleidpick');
			var pickMethodId = pickRuleSearchResult[i].getValue('custrecord_ebizpickmethod');
			var pickZoneId = pickRuleSearchResult[i].getValue('custrecord_ebizpickzonerul');
			var locationGroupId = pickRuleSearchResult[i].getValue('custrecord_ebizlocationgrouppickrul');
			var binLocationId = pickRuleSearchResult[i].getValue('custrecord_ebizlocationpickrul');
			var sequenceNo = pickRuleSearchResult[i].getValue('custrecord_ebizsequencenopickrul');
			var itemFamilyId = pickRuleSearchResult[i].getValue('custrecord_ebizskufamilypickrul');
			var itemGroupId = pickRuleSearchResult[i].getValue('custrecord_ebizskugrouppickrul');
			var itemId = pickRuleSearchResult[i].getValue('custrecord_ebizskupickrul');
			var itemInfo1 = pickRuleSearchResult[i].getValue('custrecord_ebizskuinfo1');
			var itemInfo2 = pickRuleSearchResult[i].getValue('custrecord_ebizskuinfo2');
			var itemInfo3 = pickRuleSearchResult[i].getValue('custrecord_ebizskuinfo3');
			var packCode = pickRuleSearchResult[i].getValue('custrecord_ebizpackcodepickrul');
			var uomLevel = pickRuleSearchResult[i].getValue('custrecord_ebizuomlevelpickrul');
			var itemStatus = pickRuleSearchResult[i].getValue('custrecord_ebizskustatspickrul');
			var orderType = pickRuleSearchResult[i].getValue('custrecord_ebizordertypepickrul');
			var uom = pickRuleSearchResult[i].getValue('custrecord_ebizuompickrul');
			var allocationStrategy = pickRuleSearchResult[i].getValue('custrecord_allocation_strategy','custrecord_ebizpickmethod');
			var pickfaceLocationFlag = pickRuleSearchResult[i].getValue('custrecord_ebizpickfacelocation','custrecord_ebizpickmethod');
			var clusterPickFlag = pickRuleSearchResult[i].getValue('custrecord_ebizcreatecluster','custrecord_ebizpickmethod');
			var maxOrders = pickRuleSearchResult[i].getValue('custrecord_ebiz_pickmethod_maxorders','custrecord_ebizpickmethod');
			var maxPicks = pickRuleSearchResult[i].getValue('custrecord_ebiz_pickmethod_maxpicks','custrecord_ebizpickmethod');
			var abcVel = pickRuleSearchResult[i].getValue('custrecord_abcvelpickrule');
			var cartonizationmethod = pickRuleSearchResult[i].getValue('custrecord_cartonization_method','custrecord_ebizpickmethod');
			var picklocation=pickRuleSearchResult[i].getValue('custrecord_ebizsitepickrule');

			var currentRow = [i, pickRuleId, pickMethodId, pickZoneId, locationGroupId, binLocationId, 
			                  sequenceNo, itemFamilyId, itemGroupId, itemId, itemInfo1, itemInfo2, itemInfo3, 
			                  packCode, uomLevel, itemStatus, orderType, uom, 
			                  allocationStrategy, pickfaceLocationFlag, clusterPickFlag, maxOrders, maxPicks, abcVel,cartonizationmethod,picklocation];


//			nlapiLogExecution('DEBUG', 'pickRule ID', pickRuleId);
//			nlapiLogExecution('DEBUG', 'pickMethod ID', pickMethodId);
//			nlapiLogExecution('DEBUG', 'pickZone ID', pickZoneId);
//			nlapiLogExecution('DEBUG', 'Location Group ID', locationGroupId);
//			nlapiLogExecution('DEBUG', 'Location ID', binLocationId);
//			nlapiLogExecution('DEBUG', 'pickRule - Current Row', currentRow);

			pickRuleList.push(currentRow);
		}
	}

	nlapiLogExecution('DEBUG', 'out of getListForAllPickZones','');

	return pickRuleList;
}

function getListForAllPickRules(Item,uomlevel,whLocation,ItemStatus,Packcode,OrderType,UOM,OrdPriority){

	nlapiLogExecution('DEBUG', 'into getListForAllPickRules','');
	var pickRulesList = new Array();
	var allocConfigDtls = new Array();
	var pickRuleCount = 0;

	var fields = ['custitem_item_family','custitem_item_group','custitem_ebizdefskustatus',
	              'custitem_item_info_1','custitem_item_info_2','custitem_item_info_3','custitem_ebizabcvelitem','custitem_ebiz_forwardpick'];


	var columns= nlapiLookupField('Item',Item,fields);
	var ItemFamily = columns.custitem_item_family;
	var ItemGroup = columns.custitem_item_group;
	var ItemInfo1 = columns.custitem_item_info_1;
	var ItemInfo2 = columns.custitem_item_info_2;
	var ItemInfo3 = columns.custitem_item_info_3;
	var putVelocity=columns.custitem_ebizabcvelitem;
	var ForwardPick=columns.custitem_ebiz_forwardpick;

	var str = 'ItemFamily. = ' + ItemFamily + '<br>';
	str = str + 'ItemGroup. = ' + ItemGroup + '<br>';
	str = str + 'ItemInfo1. = ' + ItemInfo1 + '<br>';
	str = str + 'ItemInfo2. = ' + ItemInfo2 + '<br>';
	str = str + 'ItemInfo3. = ' + ItemInfo3 + '<br>';
	str = str + 'putVelocity. = ' + putVelocity + '<br>';
	str = str + 'ItemStatus. = ' + ItemStatus + '<br>';
	str = str + 'Item. = ' + Item + '<br>';
	str = str + 'whLocation. = ' + whLocation + '<br>';
	str = str + 'uomlevel. = ' + uomlevel + '<br>';
	str = str + 'Packcode. = ' + Packcode + '<br>';
	str = str + 'OrderType. = ' + OrderType + '<br>';
	str = str + 'ForwardPick. = ' + ForwardPick + '<br>';
	str = str + 'OrdPriority. = ' + OrdPriority + '<br>';

	nlapiLogExecution('Debug', 'getListForAllPickRules Parameters', str);

	var filters = new Array();

	if(ItemFamily!=null && ItemFamily != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskufamilypickrul', null, 'anyof', ['@NONE@', ItemFamily]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskufamilypickrul', null, 'anyof', ['@NONE@']));
	}

	if(ItemGroup!=null && ItemGroup != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskugrouppickrul', null, 'anyof', ['@NONE@', ItemGroup]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskugrouppickrul', null, 'anyof', ['@NONE@']));
	}

	if(ItemStatus!=null && ItemStatus != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskustatspickrul', null, 'anyof', ['@NONE@', ItemStatus]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskustatspickrul', null, 'anyof', ['@NONE@']));
	}

	if(ItemInfo1!=null && ItemInfo1 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo1', null, 'anyof',['@NONE@', ItemInfo1]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo1', null, 'anyof', ['@NONE@']));
	}

	if(ItemInfo2!=null && ItemInfo2 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo2', null, 'anyof',['@NONE@', ItemInfo2]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo2', null, 'anyof', ['@NONE@']));
	}

	if(ItemInfo3!=null && ItemInfo3 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo3', null, 'anyof',['@NONE@', ItemInfo3]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo3', null, 'anyof', ['@NONE@']));
	}

	if(Item != null && Item != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskupickrul', null, 'anyof', ['@NONE@', Item]));		
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskupickrul', null, 'anyof', ['@NONE@']));
	}

	if(putVelocity != null && putVelocity != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizabcvel', null, 'anyof', ['@NONE@', putVelocity]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizabcvel', null, 'anyof', ['@NONE@']));
	}

	if(whLocation != null && whLocation != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizsitepickrule', null, 'anyof', [whLocation]));
	}

	if(uomlevel != null && uomlevel != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizuomlevelpickrul', null, 'anyof', ['@NONE@', uomlevel]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizuomlevelpickrul', null, 'anyof', ['@NONE@']));
	}

	if(Packcode != null && Packcode != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizpackcodepickrul', null, 'anyof', ['@NONE@', Packcode]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizpackcodepickrul', null, 'anyof', ['@NONE@']));
	}

	if(OrderType != null && OrderType != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizordertypepickrul', null, 'anyof', ['@NONE@', OrderType]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizordertypepickrul', null, 'anyof', ['@NONE@']));
	}

	if(OrdPriority != null && OrdPriority != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizorderpriority', null, 'anyof', ['@NONE@', OrdPriority]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizorderpriority', null, 'anyof', ['@NONE@']));
	}

	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizruleidpick');
	columns[1] = new nlobjSearchColumn('custrecord_ebizpickmethod');
	columns[2] = new nlobjSearchColumn('custrecord_ebizpickzonerul');
	columns[3] = new nlobjSearchColumn('custrecord_ebizlocationgrouppickrul');
	columns[4] = new nlobjSearchColumn('custrecord_ebizlocationpickrul');
	//columns[5] = new nlobjSearchColumn('custrecord_ebizsequencenopickrul');
	//columns[5] = new nlobjSearchColumn('custrecord_ebizsequencenopickrul');
	columns[5]=new nlobjSearchColumn('formulanumeric');
	columns[5].setFormula("TO_NUMBER({custrecord_ebizsequencenopickrul})").setSort();
	columns[6] = new nlobjSearchColumn('custrecord_ebizskufamilypickrul');
	columns[7] = new nlobjSearchColumn('custrecord_ebizskugrouppickrul');
	columns[8] = new nlobjSearchColumn('custrecord_ebizskupickrul');
	columns[9] = new nlobjSearchColumn('custrecord_ebizskuinfo1');
	columns[10] = new nlobjSearchColumn('custrecord_ebizskuinfo2');
	columns[11] = new nlobjSearchColumn('custrecord_ebizskuinfo3');
	columns[12] = new nlobjSearchColumn('custrecord_ebizpackcodepickrul');
	columns[13] = new nlobjSearchColumn('custrecord_ebizuomlevelpickrul');
	columns[14] = new nlobjSearchColumn('custrecord_ebizskustatspickrul');
	columns[15] = new nlobjSearchColumn('custrecord_ebizordertypepickrul');
	columns[16] = new nlobjSearchColumn('custrecord_ebizuompickrul');
	columns[17] = new nlobjSearchColumn('custrecord_allocation_strategy','custrecord_ebizpickmethod');
	columns[18] = new nlobjSearchColumn('custrecord_ebizpickfacelocation','custrecord_ebizpickmethod');
	columns[19] = new nlobjSearchColumn('custrecord_ebizcreatecluster','custrecord_ebizpickmethod');
	columns[20] = new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxorders','custrecord_ebizpickmethod');
	columns[21] = new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxpicks','custrecord_ebizpickmethod');
	columns[22] = new nlobjSearchColumn('custrecord_ebizabcvel');
	columns[23] = new nlobjSearchColumn('custrecord_cartonization_method','custrecord_ebizpickmethod');
	columns[24] = new nlobjSearchColumn('custrecord_ebizsitepickrule');
	columns[25] = new nlobjSearchColumn('custrecord_ebiz_stagedetermination','custrecord_ebizpickmethod');
	columns[26] = new nlobjSearchColumn('custrecord_ebiz_isforwardpick','custrecord_ebizpickmethod');
	//columns[5].setSort();

	var pickRuleSearchResult = new nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters, columns);

	if(pickRuleSearchResult != null){
		nlapiLogExecution('DEBUG', 'pickRuleSearchResult length',pickRuleSearchResult.length);
		for (var i=0; i<pickRuleSearchResult.length; i++){
			var pickRuleId = pickRuleSearchResult[i].getValue('custrecord_ebizruleidpick');
			var pickMethodId = pickRuleSearchResult[i].getValue('custrecord_ebizpickmethod');
			var pickZoneId = pickRuleSearchResult[i].getValue('custrecord_ebizpickzonerul');
			var locationGroupId = pickRuleSearchResult[i].getValue('custrecord_ebizlocationgrouppickrul');
			var binLocationId = pickRuleSearchResult[i].getValue('custrecord_ebizlocationpickrul');
			var sequenceNo = pickRuleSearchResult[i].getValue('custrecord_ebizsequencenopickrul');
			var itemFamilyId = pickRuleSearchResult[i].getValue('custrecord_ebizskufamilypickrul');
			var itemGroupId = pickRuleSearchResult[i].getValue('custrecord_ebizskugrouppickrul');
			var itemId = pickRuleSearchResult[i].getValue('custrecord_ebizskupickrul');
			var itemInfo1 = pickRuleSearchResult[i].getValue('custrecord_ebizskuinfo1');
			var itemInfo2 = pickRuleSearchResult[i].getValue('custrecord_ebizskuinfo2');
			var itemInfo3 = pickRuleSearchResult[i].getValue('custrecord_ebizskuinfo3');
			var packCode = pickRuleSearchResult[i].getValue('custrecord_ebizpackcodepickrul');
			var uomLevel = pickRuleSearchResult[i].getValue('custrecord_ebizuomlevelpickrul');
			var itemStatus = pickRuleSearchResult[i].getValue('custrecord_ebizskustatspickrul');
			var orderType = pickRuleSearchResult[i].getValue('custrecord_ebizordertypepickrul');
			var uom = pickRuleSearchResult[i].getValue('custrecord_ebizuompickrul');
			var allocationStrategy = pickRuleSearchResult[i].getValue('custrecord_allocation_strategy','custrecord_ebizpickmethod');
			var pickfaceLocationFlag = pickRuleSearchResult[i].getValue('custrecord_ebizpickfacelocation','custrecord_ebizpickmethod');
			var clusterPickFlag = pickRuleSearchResult[i].getValue('custrecord_ebizcreatecluster','custrecord_ebizpickmethod');
			var maxOrders = pickRuleSearchResult[i].getValue('custrecord_ebiz_pickmethod_maxorders','custrecord_ebizpickmethod');
			var maxPicks = pickRuleSearchResult[i].getValue('custrecord_ebiz_pickmethod_maxpicks','custrecord_ebizpickmethod');
			var abcVel = pickRuleSearchResult[i].getValue('custrecord_abcvelpickrule');
			var cartonizationmethod = pickRuleSearchResult[i].getValue('custrecord_cartonization_method','custrecord_ebizpickmethod');
			var picklocation=pickRuleSearchResult[i].getValue('custrecord_ebizsitepickrule');
			var stagedetermination = pickRuleSearchResult[i].getValue('custrecord_ebiz_stagedetermination','custrecord_ebizpickmethod');
			var forwardPick = pickRuleSearchResult[i].getValue('custrecord_ebiz_isforwardpick','custrecord_ebizpickmethod');
			var currentRow = [i, pickRuleId, pickMethodId, pickZoneId, locationGroupId, binLocationId, 
			                  sequenceNo, itemFamilyId, itemGroupId, itemId, itemInfo1, itemInfo2, itemInfo3, 
			                  packCode, uomLevel, itemStatus, orderType, uom, 
			                  allocationStrategy, pickfaceLocationFlag, clusterPickFlag, maxOrders, maxPicks, abcVel,cartonizationmethod,picklocation,stagedetermination,forwardPick];

			pickRulesList.push(currentRow);
		}

		if(pickRulesList!=null && pickRulesList!='' && pickRulesList.length>0)
		{
			nlapiLogExecution('DEBUG', 'pickRulesList length',pickRulesList.length);
			var pfLocationResults = getPFLocationsForOrder(whLocation,Item,UOM);

			for (var j=0; j<pickRulesList.length; j++){

				var str = 'pickStrategyId. = ' + pickRulesList[j][1] + '<br>';
				str = str + 'pickMethod. = ' + pickRulesList[j][2] + '<br>';	
				str = str + 'pickZone. = ' + pickRulesList[j][3] + '<br>';
				str = str + 'ForwardPick. = ' + pickRulesList[j][27] + '<br>';

				nlapiLogExecution('DEBUG', 'Pick Strategy Details', str);

				var pickStrategyId = pickRulesList[j][1];
				var pickMethod = pickRulesList[j][2];
				var pickZone = pickRulesList[j][3];
				var locnGroup = pickRulesList[j][4];
				var binLocation = pickRulesList[j][5];
				var allocationStrategy = pickRulesList[j][18];
				var pickfaceLocationFlag = pickRulesList[j][19];
				var clusterPickFlag = pickRulesList[j][20];
				var maxOrders = pickRulesList[j][21];
				var maxPicks = pickRulesList[j][22];
				var cartonizationmethod =  pickRulesList[j][24];
				var stagedetermination =  pickRulesList[j][26];
				var forwardPick =  pickRulesList[j][27];
				var tempPFLocationResults = new Array();

				if(pickfaceLocationFlag == 'T')
					tempPFLocationResults = pfLocationResults;
				else
					tempPFLocationResults = null;

				var locnGroupList = new Array();

				// Pick strategy details for the fulfillment order; Using index in the order list to identify the order
				var currentRow1 = [j, pickStrategyId, pickMethod, pickZone, locnGroup, binLocation, allocationStrategy,
				                   tempPFLocationResults, locnGroupList, clusterPickFlag, maxOrders, maxPicks,cartonizationmethod,stagedetermination,forwardPick];

				allocConfigDtls.push(currentRow1);
			}
		}
	}

	nlapiLogExecution('DEBUG', 'out of getListForAllPickRules','');

	return allocConfigDtls;
}

function getPickConfigForAllOrder(fulfillOrderList, pickRulesList, itemGroupFamilyArray,whlocation,skuList){

	nlapiLogExecution('DEBUG','into getPickConfigForAllOrder : pickRulesList',pickRulesList);
	var allocConfigDtls = new Array();

	// Get all pickface locations
	var pfLocationResults = getPFLocationsForOrder(whlocation,skuList);

	// For all fulfilment orders
	for (var i = 0; i < fulfillOrderList.length; i++){
		// Get index of matching pick strategy
		var index = getPickStrategyForFulfilmentOrder(fulfillOrderList[i], pickRulesList,
				itemGroupFamilyArray);

		var pickStrategyId = pickRulesList[index][1];
		var pickMethod = pickRulesList[index][2];
		var pickZone = pickRulesList[index][3];
		var locnGroup = pickRulesList[index][4];
		var binLocation = pickRulesList[index][5];
		var allocationStrategy = pickRulesList[index][18];
		var pickfaceLocationFlag = pickRulesList[index][19];
		var clusterPickFlag = pickRulesList[index][20];
		var maxOrders = pickRulesList[index][21];
		var maxPicks = pickRulesList[index][22];
		var cartonizationmethod =  pickRulesList[index][24];

		var tempPFLocationResults = new Array();
		if(pickfaceLocationFlag == 'T')
			tempPFLocationResults = pfLocationResults;
		else
			tempPFLocationResults = null;

		var locnGroupList = getLocnGroupListForOrder(locnGroup, binLocation, pickZone);

		// Pick strategy details for the fulfillment order; Using index in the order list to identify the order
		var currentRow = [i, pickStrategyId, pickMethod, pickZone, locnGroup, binLocation, allocationStrategy,
		                  tempPFLocationResults, locnGroupList, clusterPickFlag, maxOrders, maxPicks,cartonizationmethod];

		allocConfigDtls.push(currentRow);
	}

	nlapiLogExecution('DEBUG','out of getPickConfigForAllOrder : allocConfigDtls.length ',allocConfigDtls.length);	


	return allocConfigDtls;
}

function getLocnGroupListForOrder(locnGroup, binLocation, pickZone){

	var locnGroupList = new Array();
	// Get the bin location group list
//	if (locnGroup == null || locnGroup == "")

	//IF binlocation and loc group is not defined then get loc gr against the zone
	if ((locnGroup == null || locnGroup == "")&&(binLocation==null||binLocation==""))
		locnGroupList = getZoneLocnGroupsList(pickZone);
	else{
		//If binlocation is empty then take loc gr as default.
		//If location itself defined then in getItemDetails function we are filtering the inv rec with binlocation.
		//No need to pass any paramter from here.
		if (binLocation == null || binLocation == ''){
			locnGroupList.push(locnGroup);
		}
	}
	return locnGroupList;
}

function getItemGroupFamilyForItem(item, itemGroupFamilyList){
	var groupFamilyArray = new Array();
	var itemFound = false;

	if(itemGroupFamilyList != null && itemGroupFamilyList.length > 0){
		for(var i = 0; i < itemGroupFamilyList.length; i++){
			if(!itemFound){
				if(item == parseFloat(itemGroupFamilyList[i][0])){
					groupFamilyArray.push(itemGroupFamilyList[i][2]);	// ITEM GROUP
					groupFamilyArray.push(itemGroupFamilyList[i][3]);	// ITEM FAMILY

					itemFound = true;
				}
			}
		}
	}
	nlapiLogExecution('DEBUG','itemFound',itemFound);
	return groupFamilyArray;
}

function getPickStrategyForFulfilmentOrder(currentOrder, pickRulesList, itemGroupFamilyList){
	var pickStrategyIndex = -1;
	var matchFound = false;

	nlapiLogExecution('DEBUG', 'currentOrder in getItemGroupFamilyForItem ',currentOrder[6]);

	// Get item group and family for item
	var groupFamilyList = getItemGroupFamilyForItem(currentOrder[6], itemGroupFamilyList);

	// Traverse through all the pick strategies and determine the match
	if(pickRulesList != null && pickRulesList.length > 0){
		for(var i = 0; i < pickRulesList.length; i++){
			if(!matchFound){
				if(pickRulesList[i][25]==currentOrder[19])											// WH SITE
				{
					if(parseFloat(pickRulesList[i][9]) == parseFloat(currentOrder[6]) ||				// ITEM
							parseFloat(pickRulesList[i][15]) == parseFloat(currentOrder[7]) ||			// ITEM STATUS
							parseInt(pickRulesList[i][10]) == parseInt(currentOrder[14]) ||				// ITEM INFO 1
							parseInt(pickRulesList[i][11]) == parseInt(currentOrder[15]) ||				// ITEM INFO 2
							parseInt(pickRulesList[i][12]) == parseInt(currentOrder[16]) ||				// ITEM INFO 3
							parseFloat(pickRulesList[i][17]) == parseFloat(currentOrder[11]) ||			// UOM
							parseFloat(pickRulesList[i][8]) == parseFloat(groupFamilyList[0]) ||		// ITEM GROUP
							parseFloat(pickRulesList[i][16]) == parseFloat(currentOrder[9]) ||			// ORDER TYPE
							parseFloat(pickRulesList[i][7]) == parseFloat(groupFamilyList[1])){			// ITEM FAMILY
						pickStrategyIndex = i;
						matchFound = true;
					}
				}

				if(! matchFound && i == parseFloat(pickRulesList.length - 1)){
					pickStrategyIndex = 0;
					matchFound = true;
				}
			}
		}
	}

	return pickStrategyIndex;
}

function getPFLocationsForOrder(whlocation,skuList,UOM){
	var pfLocationResults = new Array();
	pfLocationResults = getSKUQtyInPickfaceLocns(whlocation,skuList,UOM);
	return pfLocationResults;
}

function getOrderDetailsForWaveGen(ordno,ordlineno,ebizwaveno)
{
	nlapiLogExecution('DEBUG', 'Into getOrderDetailsForWaveGen');
	var orderList = new Array();
	var filters = new Array();
	var fulfillmentordlist = new Array();
	var orderInfoCount = 0;

	nlapiLogExecution('DEBUG', 'ebizwaveno', ebizwaveno);
	nlapiLogExecution('DEBUG', 'ordno', ordno);
	nlapiLogExecution('DEBUG', 'ordlineno', ordlineno);

	filters.push(new nlobjSearchFilter('custrecord_ebiz_wave', null, 'equalto', parseFloat(ebizwaveno)));

	if(ordno!=null && ordno!='')
	{
		filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', ordno));
	}

	if(ordlineno!=null && ordlineno!='')
	{
		//filters.push(new nlobjSearchFilter('custrecord_ordline', null, 'is', ordlineno));
		filters.push(new nlobjSearchFilter('custrecord_ordline', null, 'equalto', parseFloat(ordlineno))); 
		//11-Partially Picked, 13-Partially Shipped, 15-Selected Into Wave, 26-Picks Failed
		filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [11,13,15,26]));
	}

	if((ordno==null || ordno=='') && (ordlineno==null || ordlineno==''))
	{
		filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [15]));
	}

	var columns = new Array();

	columns[0] = new nlobjSearchColumn('custrecord_linesku_status');
	columns[1] = new nlobjSearchColumn('custrecord_do_customer');
	columns[2] = new nlobjSearchColumn('custrecord_do_carrier');
	columns[3] = new nlobjSearchColumn('custrecord_do_order_type');
	columns[4] = new nlobjSearchColumn('custrecord_do_order_priority');
	columns[5] = new nlobjSearchColumn('custrecord_lineord');
	columns[6] = new nlobjSearchColumn('custrecord_ebiz_linesku');
	columns[7] = new nlobjSearchColumn('custrecord_ord_qty');
	columns[8] = new nlobjSearchColumn('name');
	columns[8].setSort();
	columns[9] = new nlobjSearchColumn('custrecord_lineord');
	columns[9].setSort();		
	columns[10] = new nlobjSearchColumn('custrecord_ordline');
	columns[10].setSort();
	columns[11] = new nlobjSearchColumn('custrecord_linepackcode');
	columns[12] = new nlobjSearchColumn('custrecord_lineuom_id');
	columns[13] = new nlobjSearchColumn('custrecord_batch');
	columns[14] = new nlobjSearchColumn('custrecord_ordline_wms_location');
	columns[15] = new nlobjSearchColumn('custrecord_ordline_company');
	columns[16] = new nlobjSearchColumn('custrecord_fulfilmentiteminfo1');
	columns[17] = new nlobjSearchColumn('custrecord_fulfilmentiteminfo2');
	columns[18] = new nlobjSearchColumn('custrecord_fulfilmentiteminfo3');	
	columns[19] = new nlobjSearchColumn('custrecord_pickgen_qty');
	columns[20] = new nlobjSearchColumn('custrecord_do_wmscarrier');
	columns[21] = new nlobjSearchColumn('custrecord_nsconfirm_ref_no');
	columns[22] = new nlobjSearchColumn('custrecord_linenotes1');
	columns[23] = new nlobjSearchColumn('custrecord_linenotes2');
	columns[24] = new nlobjSearchColumn('custrecord_pickgen_flag');
	columns[25] = new nlobjSearchColumn('custrecord_pickqty');
	columns[26] = new nlobjSearchColumn('custrecord_ship_qty');
	columns[27] = new nlobjSearchColumn('custrecord_ns_ord');
	columns[28] = new nlobjSearchColumn('custrecord_shipment_no');
	columns[29] = new nlobjSearchColumn('custrecord_printflag');
	columns[30] = new nlobjSearchColumn('custrecord_print_count');
	columns[31] = new nlobjSearchColumn('custrecord_ebiz_freightterms');
	columns[32] = new nlobjSearchColumn('custrecord_shipcomplete');
	columns[33] = new nlobjSearchColumn('custrecord_ebiz_pr_dateprinted');
	columns[34] = new nlobjSearchColumn('custentity_ebiz_lefo_required','custrecord_do_customer');
	columns[35] = new nlobjSearchColumn('custentity_ebiz_notto_split','custrecord_do_customer');
	columns[36] = new nlobjSearchColumn('custentity_ebiz_samelot','custrecord_do_customer');
	columns[37] = new nlobjSearchColumn('custentity_ebiz_shelflife','custrecord_do_customer');
	columns[38] = new nlobjSearchColumn('custrecord_actuallocation');
	orderList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);

	if(orderList!=null && orderList!='' && orderList.length>0)
	{
		nlapiLogExecution('DEBUG', 'orderList length', orderList.length);

		for(var i=0;i<orderList.length;i++)
		{
			var currentOrder=orderList[i];

			var vOrdQty = parseFloat(currentOrder.getValue('custrecord_ord_qty'));

			//nlapiLogExecution('DEBUG', 'vOrdQty', vOrdQty);

			var vPickGenQty=0;

			if(currentOrder.getValue('custrecord_pickgen_qty')!=null && currentOrder.getValue('custrecord_pickgen_qty')!='')	
				vPickGenQty = parseFloat(currentOrder.getValue('custrecord_pickgen_qty'));

			//nlapiLogExecution('DEBUG', 'vPickGenQty', vPickGenQty);

			vOrdQty = parseFloat(vOrdQty)-parseFloat(vPickGenQty);

			nlapiLogExecution('DEBUG', 'vOrdQty', vOrdQty);

			var itemName = currentOrder.getText('custrecord_ebiz_linesku');
			var itemNo = currentOrder.getValue('custrecord_ebiz_linesku');
			var foordqty = currentOrder.getValue('custrecord_ord_qty');
			var availQty = 0;
			var orderQty = vOrdQty;
			var fointrid = currentOrder.getId();
			var doNo = currentOrder.getId();
			var lineNo = currentOrder.getValue('custrecord_ordline');
			var foname = currentOrder.getValue('custrecord_lineord');
			var doName = currentOrder.getValue('custrecord_lineord');
			var soInternalID = currentOrder.getValue('name');
			var itemStatus = currentOrder.getValue('custrecord_linesku_status');
			var packCode = currentOrder.getText('custrecord_linepackcode');
			var uom = currentOrder.getValue('custrecord_lineuom_id');
			var lotBatchNo = currentOrder.getValue('custrecord_batch');
			var location = currentOrder.getValue('custrecord_ordline_wms_location');
			var company = currentOrder.getValue('custrecord_ordline_company');
			var itemInfo1 = currentOrder.getValue('custrecord_fulfilmentiteminfo1');
			var itemInfo2 = currentOrder.getValue('custrecord_fulfilmentiteminfo2');
			var itemInfo3 = currentOrder.getValue('custrecord_fulfilmentiteminfo3');
			var orderType = currentOrder.getValue('custrecord_do_order_type');
			var orderPriority = currentOrder.getValue('custrecord_do_order_priority');
			var wmsCarrier = currentOrder.getValue('custrecord_do_wmscarrier');
			var nsrefno = currentOrder.getValue('custrecord_nsconfirm_ref_no');			
			var note1 = currentOrder.getValue('custrecord_linenotes1');
			var note2 = currentOrder.getValue('custrecord_linenotes2');
			var pickgenflag = currentOrder.getValue('custrecord_pickgen_flag');
			var pickqty = currentOrder.getValue('custrecord_pickqty');
			var shipqty = currentOrder.getValue('custrecord_ship_qty');
			var parentordno = currentOrder.getValue('custrecord_ns_ord');
			var shipmentno = currentOrder.getValue('custrecord_shipment_no');
			var printflag = currentOrder.getValue('custrecord_printflag');
			var printcount = currentOrder.getValue('custrecord_print_count');
			var freightterms = currentOrder.getValue('custrecord_ebiz_freightterms');
			var shipcomplete = currentOrder.getValue('custrecord_shipcomplete');
			var pickreportprintdt = currentOrder.getValue('custrecord_ebiz_pr_dateprinted');
			var customer = currentOrder.getValue('custrecord_do_customer');
			var shipmethod = currentOrder.getValue('custrecord_do_carrier');
			var lefoflag = currentOrder.getValue('custentity_ebiz_lefo_required','custrecord_do_customer');
			var PickDontSplitFlag = currentOrder.getValue('custentity_ebiz_notto_split','custrecord_do_customer');
			var sameLot = currentOrder.getValue('custentity_ebiz_samelot','custrecord_do_customer');
			var shelfLife = currentOrder.getValue('custentity_ebiz_shelflife','custrecord_do_customer');
			var ActSolocation=currentOrder.getValue('custrecord_actuallocation');
			//Case# 20149102 ActSolocation field defined
			if(PickDontSplitFlag == null || PickDontSplitFlag == '')
				PickDontSplitFlag='N';

			if(nsrefno == null || isNaN(nsrefno))
				nsrefno='';

			var currentRow = [i, soInternalID, lineNo, fointrid, foname, itemName, itemNo, itemStatus,foordqty, orderQty, 
			                  packCode, uom, lotBatchNo, company,itemInfo1, itemInfo2, itemInfo3, orderType, orderPriority, 
			                  location,wmsCarrier,nsrefno,note1,note2,pickgenflag,pickqty,shipqty,parentordno,shipmentno,
			                  printflag,printcount,freightterms,shipcomplete,pickreportprintdt,vPickGenQty,customer,shipmethod,
			                  lefoflag,PickDontSplitFlag,sameLot,shelfLife,ActSolocation];

			/*var currentRow = [i, soInternalID, lineNo, doNo, doName, itemName, itemNo, itemStatus, 
			                  availQty, orderQty, packCode, uom, lotBatchNo, company, 
			                  itemInfo1, itemInfo2, itemInfo3, orderType, orderPriority,location,wmsCarrier,nsrefno];*/
			//nlapiLogExecution('DEBUG', 'currentRow', currentRow);
			fulfillmentordlist[orderInfoCount++] = currentRow;

		}
	}

	nlapiLogExecution('DEBUG', 'Out of getOrderDetailsForWaveGen');

	return fulfillmentordlist;
}

function allocateQuantity(inventorySearchResults, fulfillOrderList, eBizWaveNo, allocConfigDtls,containerslist,LPparent,
		vLotExcepId,vLotExcepQty,vordertype,waveassignedto)
{
	nlapiLogExecution('DEBUG', 'into allocateQuantity', eBizWaveNo);
	var fulfilmentItem;
	var inventoryItem;
	var inventoryQuantity = 0;
	var allocationQuantity = 0;
	var recordId;
	var remainingQuantity = 0;
	var actualQty = 0; 
	var salesOrderList = new Array();
	var whLocation = "";
	var DOwhLocation = "";
	var fulfillmentItemStatus;
	// Maintaining the allocation by bin location
	var allocatedInv = new Array();
	var containerLP="";
	var cartonizationmethod="";
	var vnotes='';
	var wmsCarrier="";
	var memberitemqty;
	var nsrefno='';
	var ordtype='';
	var lefoflag='';
	var expiryDate='';
	var vLotNoSplit='';
	var samelot='';
	var shelflife='';
	var latestexpirydt = '';
	var ActSoLoc = '';
	var ordpriority='';
	/*
	 *	For all fulfillment orders
	 *		If PF Location Flag is set, then allocate to PF Locations
	 *		Allocate remaining quantity to inventory search results
	 */
	try{

		var prvSalesord='';
		var invtcontLP = "";
		var pfcontLP="";
		var packCode = "";
		var totordqty=0;
		var totreplenqty=0;
		var dimsbulkpick='';
		var otparent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); //create new parent record on open task.

		var parent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); //create new parent record on fo line.
		var foparentid = nlapiSubmitRecord(parent); 
		var foparent = nlapiLoadRecord('customrecord_ebiz_throwaway_parent', foparentid);
		var KitFailed=false;

		for (var i = 0; i < fulfillOrderList.length; i++)
		{
			//var itemType = nlapiLookupField('item', fulfillOrderList[i][6], 'recordType');
			//nlapiLogExecution('DEBUG', 'itemType', itemType);

			var fields = ['recordType', 'custitem_ebizbatchlot'];
			var itemRec = nlapiLookupField('item', fulfillOrderList[i][6], fields);
			var itemType = itemRec.recordType;
			var batchflag = itemRec.custitem_ebizbatchlot;

			ordtype = fulfillOrderList[i][17];
			ordpriority = fulfillOrderList[i][18];
			
			// case# 201417254  
			var vInvFailedArr=new Array();
			var vFOFailedArr=new Array();
			var vOTFailedArr=new Array();
			
			
			if(itemType=='kititem' || itemType=='itemgroup')
			{

				DOwhLocation = fulfillOrderList[i][19];		

				var kititemdimsarr = new Array();
				var kititemsarr = new Array();
				var pflocationsarr = new Array();

				var searchresultsitem = nlapiLoadRecord(itemType, fulfillOrderList[i][6]);
				var SkuNo=searchresultsitem.getFieldValue('itemid');
				nlapiLogExecution('DEBUG', 'SkuNo', SkuNo);
				var recCount=0; 
				var filters = new Array(); 			 
				filters[0] = new nlobjSearchFilter('itemid', null, 'is', SkuNo);	
				filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');	
				filters[2] = new nlobjSearchFilter('type', null, 'is', 'Kit');

				var columns1 = new Array(); 
				columns1[0] = new nlobjSearchColumn( 'memberitem' ); 			
				columns1[1] = new nlobjSearchColumn( 'memberquantity' );

				var searchresults = nlapiSearchRecord( 'item', null, filters, columns1 );

				for(var q=0; searchresults!=null && q<searchresults.length;q++) 
				{
					kititemsarr.push(searchresults[q].getValue('memberitem'));
				}

				kititemdimsarr = getItemDimensions(kititemsarr,DOwhLocation);

				var pickruleslist = getAllPickRules(null,null,null,null,null,ordtype,ordpriority);
				var pickzonearray = new Array();
				for(var f = 0; f < parseFloat(pickruleslist.length); f++)
				{
					if(pickruleslist[f].getValue('custrecord_ebizpickzonerul')!=null && pickruleslist[f].getValue('custrecord_ebizpickzonerul')!='')
						pickzonearray.push(pickruleslist[f].getValue('custrecord_ebizpickzonerul'));
				}

				var allZoneLocnGroups = getAllZoneLocnGroupsList(pickzonearray,-1);		
				var replenRulesList = getListForAllReplenZones();		
				pflocationsarr = getPFLocationsForOrder(null,kititemsarr);

				for(var k=0; k<searchresults.length;k++) 
				{ 
					var MemberItemId = searchresults[k].getValue('memberitem');
					//Case 20124822 :start : to find seriviceitem and noninvtitem
					var CompItemType = nlapiLookupField('item', MemberItemId, 'recordType');
					nlapiLogExecution('DEBUG', 'itemType', CompItemType);
					if(CompItemType != 'Service' && CompItemType != 'NonInvtPart' && CompItemType != 'serviceitem')
					{ //end of Case 20124822
						//var containerslist=getAllContainersList();
						fulfilmentItem = searchresults[k].getValue('memberitem');
						memberitemqty = searchresults[k].getValue('memberquantity');
						fulfilmentQuantity=fulfillOrderList[i][9]*parseFloat(memberitemqty);
						remainingQuantity = parseFloat(fulfilmentQuantity);		
						fulfilmentOrderNo = fulfillOrderList[i][3]; 			// ORDER NO.
						salesOrderNo = fulfillOrderList[i][1]; 					// SALES ORDER INTERNAL ID.
						fulfilmentOrderLineNo = fulfillOrderList[i][2];			// FULFILMENT ORDER LINE NO.
						fulfillmentItemStatus=fulfillOrderList[i][7];			// FULFILMENT Item Status.
						DOwhLocation = fulfillOrderList[i][19];					// FULFILMENT WAREHOUSE LOCATION
						packCode=fulfillOrderList[i][10];
						wmsCarrier = fulfillOrderList[i][20];
						nsrefno = fulfillOrderList[i][21];
						vLotNoSplit = fulfillOrderList[i][38];
						samelot = fulfillOrderList[i][39];
						shelflife = fulfillOrderList[i][40];
						customer = fulfillOrderList[i][35];
						lefoflag = fulfillOrderList[i][37];
						ordtype = fulfillOrderList[i][17];
						ActSoLoc = fulfillOrderList[i][41];
						ordpriority = fulfillOrderList[i][18];

						var subfields = ['recordType', 'custitem_ebizbatchlot'];
						var subitemRec = nlapiLookupField('item', fulfilmentItem, subfields);
						var subitemType = subitemRec.recordType;
						var subbatchflag = subitemRec.custitem_ebizbatchlot;

						var str = 'packCode. = ' + packCode + '<br>';
						str = str + 'fulfilmentItem. = ' + fulfilmentItem + '<br>';	
						str = str + 'fulfillmentItemStatus. = ' + fulfillmentItemStatus + '<br>';	
						str = str + 'fulfillment Warehouse Location. = ' + DOwhLocation + '<br>';
						str = str + 'wmsCarrier. = ' + wmsCarrier + '<br>';
						str = str + 'customer. = ' + customer + '<br>';
						str = str + 'samelot. = ' + samelot + '<br>';
						str = str + 'shelflife. = ' + shelflife + '<br>';
						str = str + 'lefoflag. = ' + lefoflag + '<br>';
						str = str + 'subitemType. = ' + subitemType + '<br>';
						str = str + 'subbatchflag. = ' + subbatchflag + '<br>';
						str = str + 'ordtype. = ' + ordtype + '<br>';
						str = str + 'ordpriority. = ' + ordpriority + '<br>';

						nlapiLogExecution('DEBUG', 'Fulfillment Ord Line Details', str);

						if((subitemType=='lotnumberedinventoryitem' || subitemType=='lotnumberedassemblyitem' || subbatchflag=='T')
								&&(customer!=null && customer!='') && samelot=='T')
						{
							var customerlotdet = getLatestExpiry(customer,fulfilmentItem,DOwhLocation);
							if(customerlotdet!=null && customerlotdet!='')
							{
								latestexpirydt = customerlotdet[0].getValue('custrecord_ebiz_lot_expiry');
							}
						}

						nlapiLogExecution('DEBUG', 'latestexpirydt', latestexpirydt);	
						var arrLPRequired = pickgen_palletisationforkititems(fulfilmentItem,fulfilmentQuantity,kititemdimsarr,DOwhLocation);

						if(arrLPRequired==null || arrLPRequired==''||arrLPRequired.length==0)
						{
							vnotes='Missing Dimensions';
							nlapiLogExecution('DEBUG', 'CallingFunction1', 'Sucess');
							createRecordInOpenTask(fulfillOrderList[i], remainingQuantity, eBizWaveNo, "", "", "",
									"", "", "", "", "", DOwhLocation, "","","","","","",vnotes,"",fulfilmentItem,nsrefno,"",
									customer,dimsbulkpick,otparent,waveassignedto);

							//updateFulfilmentOrder(fulfilmentOrderNo, fulfilmentOrderLineNo, 0, eBizWaveNo);
							updateFulfilmentOrder(fulfillOrderList[i], 0, eBizWaveNo,foparent);

						}
						else
						{
							var nooflps = "";
							var uomlevel = "";
							var lpbrkqty = "";
							var uompackflag = "";
							var uomweight="";
							var uomcube="";
							var uomqty="";
							//The below line is commented by Satish.N on 09AOR2014
							//var KitFailed=false;
							var vuom='';
							nlapiLogExecution('DEBUG', 'arrLPRequired.length',arrLPRequired.length);
							for(var s = 0;s < parseFloat(arrLPRequired.length);s++)
							{
								nooflps = arrLPRequired[s][0];
								uomlevel = arrLPRequired[s][1];
								lpbrkqty = arrLPRequired[s][2];
								uompackflag = arrLPRequired[s][3];
								uomweight = arrLPRequired[s][4];
								uomcube= arrLPRequired[s][5];
								uomqty = arrLPRequired[s][6];
								dimsbulkpick = arrLPRequired[s][7];
								vuom = arrLPRequired[s][8];

								remainingQuantity = parseFloat(lpbrkqty);
								nlapiLogExecution('DEBUG', 'nooflps',nooflps);

								if(parseFloat(nooflps)>0)
								{

//									var allocConfigDtls1 = getListForAllPickRules(fulfilmentItem,uomlevel,DOwhLocation,fulfillmentItemStatus,
//									packCode,ordtype);

									var allocConfigDtls1 = getListOfAllPickRulesKit(fulfilmentItem,uomlevel,DOwhLocation,fulfillmentItemStatus,
											packCode,ordtype,pickruleslist,pflocationsarr);

									if(allocConfigDtls1==null || allocConfigDtls1==''||allocConfigDtls1.length==0)
									{
										vnotes='Pick Strategy Error';
										KitFailed=true;
										nlapiLogExecution('DEBUG', 'CallingFunction1', 'Sucess');
										createRecordInOpenTask(fulfillOrderList[i], remainingQuantity, eBizWaveNo, "", "", "",
												"", "", "", "", "", DOwhLocation, "","","","","","",vnotes,"",fulfilmentItem,nsrefno,"",
												customer,dimsbulkpick,otparent,waveassignedto);

										//updateFulfilmentOrder(fulfilmentOrderNo, fulfilmentOrderLineNo, 0, eBizWaveNo);
										updateFulfilmentOrder(fulfillOrderList[i], 0, eBizWaveNo,foparent);
									}
									else
									{
										nlapiLogExecution('DEBUG', 'secondtest', 'secondtest');

										var vBoolFailedForSameLP=false;

										nlapiLogExecution('DEBUG', 'nooflps', 'nooflps');

										for(var t=1; t<=parseFloat(nooflps);t++)
										{
											nlapiLogExecution('DEBUG', 't',t);
											nlapiLogExecution('DEBUG', 'vBoolFailedForSameLP',vBoolFailedForSameLP);

											var str = 'nooflps. = ' + nooflps + '<br>';
											str = str + 'lpbrkqty. = ' + lpbrkqty + '<br>';	
											str = str + 'uomlevel. = ' + uomlevel + '<br>';
											str = str + 'remainingQuantity. = ' + remainingQuantity + '<br>';

											nlapiLogExecution('DEBUG', 'Breakdown Details', str);

											if(remainingQuantity==null || remainingQuantity==''|| remainingQuantity==0)
												remainingQuantity=lpbrkqty;									

											if(vBoolFailedForSameLP ==true && t>1)
											{
												//If there is still some quantity remaining, then insert a failed task
												if(parseFloat(nooflps)>0 && parseFloat(remainingQuantity) > 0)
												{
													vnotes='Insufficient Inventory';
													KitFailed=true;
													nlapiLogExecution('DEBUG', 'Creating Failed Task');
													nlapiLogExecution('DEBUG', 'remainingQuantity',remainingQuantity);
													createRecordInOpenTask(fulfillOrderList[i], remainingQuantity, eBizWaveNo, "", "", "",
															"", "", "", "", "", DOwhLocation, "",uomlevel,"","","","",vnotes,"",fulfilmentItem,nsrefno,"",
															customer,dimsbulkpick,otparent,waveassignedto);
													//updateFulfilmentOrder(fulfillOrderList[i], fulfilmentOrderLineNo, 0, eBizWaveNo,foparent);
													updateFulfilmentOrder(fulfillOrderList[i], 0, eBizWaveNo,foparent);
													nlapiLogExecution('DEBUG', 'Creating Failed Task','SUCCESS');
													vBoolFailedForSameLP=true;
													remainingQuantity=0;

												}	
											}
											else
											{

												nlapiLogExecution('DEBUG', 'remainingQuantity', remainingQuantity);

												if(parseFloat(remainingQuantity) > 0) 
												{
													for(var v = 0; v < parseFloat(allocConfigDtls1.length) && parseFloat(remainingQuantity) > 0; v++)
													{

														var pfLocationResults = allocConfigDtls1[v][7];
														var pickMethod = allocConfigDtls1[v][2];
														var pickZone = allocConfigDtls1[v][3];
														var pickStrategyId = allocConfigDtls1[v][1];
														var allocationStrategy = allocConfigDtls1[v][6];
														var binlocid=allocConfigDtls1[v][5];
														var binlocgrpid=allocConfigDtls1[v][4];
														cartonizationmethod=allocConfigDtls1[v][12];
														var stagedetermination=allocConfigDtls1[v][13];

														if(cartonizationmethod=="" && cartonizationmethod==null)
															cartonizationmethod='4';

														var str = 'pfLocationResults. = ' + pfLocationResults + '<br>';
														str = str + 'pickMethod. = ' + pickMethod + '<br>';	
														str = str + 'pickStrategyId. = ' + pickStrategyId + '<br>';
														str = str + 'pickZone. = ' + pickZone + '<br>';
														str = str + 'binlocgrpid. = ' + binlocgrpid + '<br>';
														str = str + 'binlocid. = ' + binlocid + '<br>';
														str = str + 'allocationStrategy. = ' + allocationStrategy + '<br>';
														str = str + 'cartonizationmethod. = ' + cartonizationmethod + '<br>';

														nlapiLogExecution('DEBUG', 'Pick Strategy Details', str);

														//var allLocnGroups =  getLocnGroupListForOrder(binlocgrpid, binlocid, pickZone);

														var allLocnGroups =  getLocnGroupsForOrderKit(binlocgrpid, binlocid, pickZone,allZoneLocnGroups);

														nlapiLogExecution('DEBUG', 'allLocnGroups', allLocnGroups);

														if (pfLocationResults != null && pfLocationResults.length > 0)
														{
															for(var P = 0; P < pfLocationResults.length; P++)
															{
																var pfBinLocationgroupId = pfLocationResults[P].getValue('custrecord_inboundlocgroupid', 'custrecord_pickbinloc');

																nlapiLogExecution('DEBUG', 'pfBinLocationgroupId', pfBinLocationgroupId);
															}
														}

														if((allLocnGroups!=null && allLocnGroups!='') || (pfBinLocationgroupId!=null && pfBinLocationgroupId!='') || (binlocid!=null && binlocid!='')){   
															inventorySearchResults = getItemDetails(allLocnGroups, fulfilmentItem,allocationStrategy,pfBinLocationgroupId,
																	lefoflag,vLotExcepId,vLotNoSplit,fulfilmentQuantity,samelot,latestexpirydt,binlocid);
														}
														else
															inventorySearchResults=null;
														nlapiLogExecution('DEBUG', 'inventorySearchResults', inventorySearchResults);
														var pfBinLocationId;
														var pickfaceArr=new Array();
														// case# 201417254 commented below and again declared above globally to this loop because again array is declarig newly if it is here
														/*var vInvFailedArr=new Array();
														var vFOFailedArr=new Array();
														var vOTFailedArr=new Array();*/
														if((inventorySearchResults!=null && inventorySearchResults!='') || (pfLocationResults!=null && pfLocationResults!=''))
														{
															if (pfLocationResults != null && pfLocationResults.length > 0)
															{
																for(var z = 0; z < pfLocationResults.length; z++)
																{
																	if(parseFloat(remainingQuantity) > 0)
																	{
																		var pfItem = pfLocationResults[z].getValue('custrecord_pickfacesku');
																		var Autoreplenflag=pfLocationResults[z].getValue('custrecord_autoreplen');
																		if (pfItem == fulfilmentItem)
																		{
																			nlapiLogExecution('DEBUG', 'item Match',pfItem);
																			pfBinLocationId = pfLocationResults[z].getValue('custrecord_pickbinloc');
																			pfMaximumQuantity = pfLocationResults[z].getValue('custrecord_maxqty');
																			pfMaximumPickQuantity = pfLocationResults[z].getValue('custrecord_maxpickqty');
																			pffixedlp = pfLocationResults[z].getText('custrecord_pickface_ebiz_lpno');
																			pfstatus=pfLocationResults[z].getValue('custrecord_pickface_itemstatus');
																			replenrule=pfLocationResults[z].getValue('custrecord_pickruleid');
																			pfRoundQty=pfLocationResults[z].getValue('custrecord_roundqty');
																			pfWHLoc=pfLocationResults[z].getValue('custrecord_pickface_location');
																			pfpickzoneid = pfLocationResults[z].getValue('custrecord_pickzone');

																			var str = 'pfBinLocationId. = ' + pfBinLocationId + '<br>';
																			str = str + 'pfMaximumQuantity. = ' + pfMaximumQuantity + '<br>';	
																			str = str + 'pfMaximumPickQuantity. = ' + pfMaximumPickQuantity + '<br>';
																			str = str + 'remainingQuantity. = ' + remainingQuantity + '<br>';
																			str = str + 'pffixedlp. = ' + pffixedlp + '<br>';
																			str = str + 'pfstatus. = ' + pfstatus + '<br>';
																			str = str + 'replenrule. = ' + replenrule + '<br>';
																			str = str + 'pfpickzoneid. = ' + pfpickzoneid + '<br>';

																			nlapiLogExecution('DEBUG', 'Pickface Details', str);
																			//case# 20127685 starts (cluster number not generating issue)
																			//if(pfpickzoneid==null && pfpickzoneid=='')
																			if(pfpickzoneid==null || pfpickzoneid=='')
																			{
																				var filtersLocGrp = new Array();
																				filtersLocGrp.push(new nlobjSearchFilter('custrecord_locgroup_no', null, 'anyof', pfBinLocationgroupId));
																				var columnsLocGrp = new Array();
																				columnsLocGrp[0] = new nlobjSearchColumn('custrecordcustrecord_putzoneid');
																				var SearchLocGrp = nlapiSearchRecord('customrecord_zone_locgroup', null, filtersLocGrp, columnsLocGrp);
																				if(SearchLocGrp != null && SearchLocGrp != '')
																				{
																					pfpickzoneid = SearchLocGrp[0].getValue('custrecordcustrecord_putzoneid');
																				}
																				else
																				{
																					pfpickzoneid=pickZone;
																				}
																				nlapiLogExecution('ERROR', 'pfpickzoneid', pfpickzoneid);

																			}

																			nlapiLogExecution('DEBUG', 'pfpickzoneid ', pfpickzoneid);
																			//case# 20127685 end
																			if((parseFloat(pfMaximumPickQuantity) >= parseFloat(remainingQuantity)) 
																					|| (uompackflag!="1" && uompackflag!="3"))
																			{
																				var pickqty=0;
																				var searchpickyqty=getOpenPickqty(pfBinLocationId);
																				if(searchpickyqty!=null && searchpickyqty!='')
																				{
																					pickqty = searchpickyqty[0].getValue('custrecord_expe_qty', null, 'sum');
																				}

																				nlapiLogExecution('DEBUG', 'pickqty', pickqty);

																				var availableQty ="";
																				var allocQty = "";
																				var recordId = "";

																				var itemStatus = "";
																				var fromLP = "";
																				var inventoryItem="";
																				var fromLot="";
																				// Get inventory details for this pickface location
																				var binLocnInvDtls = getAvailQtyFromInventory(inventorySearchResults, pfBinLocationId, allocatedInv,DOwhLocation,fulfilmentItem,
																						fulfillmentItemStatus,samelot,latestexpirydt,shelflife);
																				nlapiLogExecution('DEBUG', 'binLocnInvDtls', binLocnInvDtls);

																				if(binLocnInvDtls!=null && binLocnInvDtls.length>0)
																				{
																					for(var w=0;w<binLocnInvDtls.length;w++)
																					{
																						if(parseFloat(remainingQuantity) > 0) 
																						{

																							availableQty = binLocnInvDtls[w][2];
																							allocQty = binLocnInvDtls[w][3];
																							recordId = binLocnInvDtls[w][8];
																							if(binLocnInvDtls[w][4]!=null && binLocnInvDtls[w][4]!='')
																								packCode = binLocnInvDtls[w][4];
																							itemStatus = binLocnInvDtls[w][5];										
																							whLocation = binLocnInvDtls[w][6];
																							fromLP = binLocnInvDtls[w][7];											
																							inventoryItem = binLocnInvDtls[w][9];
																							fromLot = binLocnInvDtls[w][10];
																							expiryDate = binLocnInvDtls[0][11];


																							/*
																							 * If the allocation strategy is not null check in inventory for that item.						 
																							 * 		if the allocation strategy = min quantity on the pallet
																							 * 			search for the inventory where the inventory has the least quantity
																							 * 		else
																							 * 			search for the inventory where the inventory has the maximum quantity 
																							 * 			on the pallet 
																							 */

																							var str = 'Inventory Item Status. = ' + itemStatus + '<br>';
																							str = str + 'Fulfillment Item Status. = ' + fulfillmentItemStatus + '<br>';	
																							str = str + 'availableQty. = ' + availableQty + '<br>';	
																							str = str + 'fromLot. = ' + fromLot + '<br>';	
																							str = str + 'expiryDate. = ' + expiryDate + '<br>';
																							str = str + 'remainingQuantity. = ' + remainingQuantity + '<br>';	


																							nlapiLogExecution('DEBUG', 'Item Status details', str);

																							//if(parseFloat(availableQty) > 0 && itemStatus== fulfillmentItemStatus)
																							if(parseFloat(availableQty) > 0 && itemStatus== fulfillmentItemStatus && remainingQuantity>0 )
																							{
																								// allocate to this bin location
																								var actualAllocQty = Math.min(parseFloat(availableQty), parseFloat(remainingQuantity));
																								var newAllocQty = parseFloat(allocQty) + parseFloat(actualAllocQty);

																								var expectedQuantity = parseFloat(actualAllocQty);

																								var str = 'actualAllocQty. = ' + actualAllocQty + '<br>';
																								str = str + 'newAllocQty. = ' + newAllocQty + '<br>';	
																								str = str + 'remainingQuantity. = ' + remainingQuantity + '<br>';	
																								str = str + 'pfMaximumPickQuantity. = ' + pfMaximumPickQuantity + '<br>';																				

																								nlapiLogExecution('DEBUG', 'Alloc Qty details', str);

																								var contsize="";								
																								var totalweight = parseFloat((expectedQuantity*uomweight))/parseFloat(uomqty);
																								var totalcube = parseFloat((expectedQuantity*uomcube))/parseFloat(uomqty);

																								// Allocate the quantity to the pickface location if the allocation quantity is 
																								// 	less than or equal to the pick face maximum quantity
																								if(actualAllocQty <= pfMaximumPickQuantity)
																								{
																									var str = 'uomlevel. = ' + uomlevel + '<br>';
																									str = str + 'uompackflag. = ' + uompackflag + '<br>';	
																									str = str + 'uomqty. = ' + uomqty + '<br>';	
																									str = str + 'expectedQuantity. = ' + expectedQuantity + '<br>';	

																									nlapiLogExecution('DEBUG', 'UOM Details', str);

																									//1-Ship Cartons	2-Build Cartons		3-Ship Pallets
																									if((uompackflag=="1" || uompackflag=="3") &&(parseFloat(expectedQuantity) == parseFloat(uomqty)))
																									{	
																										for(var l=0;containerslist!=null && l<containerslist.length;l++){     

																											var containername = containerslist[l].getValue('custrecord_containername');

																											if(containername=='SHIPASIS')
																											{
																												contsize = containerslist[l].getValue('Internalid');
																												pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																												CreateMasterLPRecordBulk(pfcontLP,contsize,totalweight,totalcube,uompackflag,DOwhLocation,LPparent);
																											}
																										}

																										nlapiLogExecution('DEBUG', 'Container Size in Pick Face : ', contsize);
																									}
																									else if(cartonizationmethod=='4')
																									{
																										if(prvSalesord!=salesOrderNo)
																										{
																											pfcontLP='';
																											//pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																											prvSalesord=salesOrderNo;
																											//CreateMasterLPRecord(pfcontLP,contsize,totalweight,totalcube,uompackflag);
																										}
																									}

																									nlapiLogExecution('DEBUG', 'Container LP in Pick Face : ', pfcontLP);

																									// Update custom records
																									//updateInventoryWithAllocation(recordId, newAllocQty);
																									nlapiLogExecution('DEBUG', 'KitFailed1 : ', KitFailed);
																									if(KitFailed== false)
																									{

																										updateInventoryWithAllocation(recordId, actualAllocQty);

																										createRecordInOpenTask(fulfillOrderList[i], expectedQuantity, eBizWaveNo, pfcontLP, 
																												pfBinLocationId, recordId, packCode, itemStatus, pickMethod, pfpickzoneid, pickStrategyId,
																												whLocation, fromLP,uomlevel,totalweight,totalcube,contsize,fromLot,'',wmsCarrier,fulfilmentItem,nsrefno,
																												expiryDate,customer,dimsbulkpick,otparent,waveassignedto);

																										remainingQuantity = parseFloat(remainingQuantity) - parseFloat(actualAllocQty);

																										var fopickgenqty = Math.floor(parseFloat(actualAllocQty)/parseFloat(memberitemqty));

																										var str = 'actualAllocQty. = ' + actualAllocQty + '<br>';
																										str = str + 'memberitemqty. = ' + memberitemqty + '<br>';	
																										str = str + 'fopickgenqty. = ' + fopickgenqty + '<br>';	

																										nlapiLogExecution('DEBUG', 'FO Qty Details', str);

																										updateFulfilmentOrder(fulfillOrderList[i], fopickgenqty, eBizWaveNo,foparent);
																										if((subitemType=='lotnumberedinventoryitem' || subitemType=='lotnumberedassemblyitem' || subbatchflag=='T')
																												&& (customer!=null && customer!=''))
																										{
																											createCustomerLot(fulfillOrderList[i],customer,fulfilmentItem,fromLot,expiryDate,whLocation);
																										}

																										var vInvTempArr1=new Array();

																										vInvTempArr1.push(recordId);
																										vInvTempArr1.push(actualAllocQty);
																										vInvFailedArr.push(vInvTempArr1);

																										var vFOTempArr=new Array();
																										vFOTempArr.push(fulfillOrderList[i]);
																										vFOTempArr.push(actualAllocQty);
																										vFOTempArr.push(eBizWaveNo);
																										vFOTempArr.push(foparent);
																										vFOFailedArr.push(vFOTempArr);

																										var vOTTempArr=new Array();
																										vOTTempArr.push(fulfillOrderList[i]);
																										vOTTempArr.push(expectedQuantity);
																										vOTTempArr.push(eBizWaveNo);
																										vOTTempArr.push(pfcontLP);
																										vOTTempArr.push(fulfilmentItem);
																										vOTFailedArr.push(vOTTempArr);

																										nlapiLogExecution('DEBUG', 'vFOFailedArr : ', vFOFailedArr);
																										nlapiLogExecution('DEBUG', 'vInvFailedArr : ', vInvFailedArr);
																										nlapiLogExecution('DEBUG', 'vFulfillArr : ', vFulfillArr);
																										// Maintain the allocation temporarily
																										var allocatedInvRow = [pfBinLocationId, expectedQuantity,fromLP,inventoryItem,fromLot];
																										allocatedInv.push(allocatedInvRow);
																										pickfaceArr.push(pfBinLocationId);
																									}
																									else
																									{
																										vnotes='Insufficient Inventory for component items';
																										nlapiLogExecution('DEBUG', 'Creating Failed Task');
																										nlapiLogExecution('DEBUG', 'expectedQuantity',expectedQuantity);
																										createRecordInOpenTask(fulfillOrderList[i], expectedQuantity, eBizWaveNo, "", "", "",
																												"", "", "", "", "", whLocation, "","","","","","",vnotes,"",fulfilmentItem,nsrefno,
																												"",customer,dimsbulkpick,otparent,waveassignedto);

																										//updateFulfilmentOrder(fulfilmentOrderNo, fulfilmentOrderLineNo, 0, eBizWaveNo);
																										updateFulfilmentOrder(fulfillOrderList[i], 0, eBizWaveNo,foparent);
																										nlapiLogExecution('DEBUG', 'Creating Failed Task','SUCCESS');
																										remainingQuantity=0;

																									}	
																								}
																							}
																							nlapiLogExecution('DEBUG','remainingQuantity',remainingQuantity);
																							totordqty=totordqty+remainingQuantity;
																							nlapiLogExecution('DEBUG','totordqty',totordqty);
																							nlapiLogExecution('DEBUG','totreplenqty',totreplenqty);
																							nlapiLogExecution('DEBUG','totreplenqty',totreplenqty);
																							//Case # 20127328,20127083 Start
																						}
																					}
																				}
																				//Case # 20127328,20127083 End
																				if(remainingQuantity > 0 &&  Autoreplenflag=='T')
																				{
																					if(totordqty==0 || totordqty>totreplenqty)
																					{
																						nlapiLogExecution('DEBUG','availableQty',availableQty);
																						nlapiLogExecution('DEBUG','remainingQuantity',remainingQuantity);
																						if(availableQty==null || availableQty=='')
																							availableQty=0;
																						var actualAllocQty = Math.min(parseFloat(availableQty), parseFloat(remainingQuantity));
																						var newAllocQty = parseFloat(allocQty) + parseFloat(actualAllocQty);
																						var expectedQuantity = parseFloat(remainingQuantity);
																						//remainingQuantity = parseFloat(remainingQuantity) - parseFloat(actualAllocQty);
																						nlapiLogExecution('DEBUG','actualAllocQty',actualAllocQty);
																						//var expectedQuantity = parseFloat(remainingQuantity);
																						nlapiLogExecution('DEBUG','EnterReplen','EnterReplen');
																						nlapiLogExecution('DEBUG','remainingQuantity',remainingQuantity);
																						nlapiLogExecution('DEBUG','uompackflag',uompackflag);
																						nlapiLogExecution('DEBUG','uomqty',uomqty);

																						if((uompackflag=="1" || uompackflag=="3") &&(parseFloat(expectedQuantity) == parseFloat(uomqty)))
																						{
																							for(l=0;containerslist!=null && l<containerslist.length;l++){     

																								var containername = containerslist[l].getValue('custrecord_containername');

																								if(containername=='SHIPASIS')
																								{
																									contsize = containerslist[l].getValue('Internalid');
																									pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);

																									//CreateMasterLPRecord(pfcontLP,contsize,totalweight,totalcube,uompackflag,DOwhLocation,LPparent);

																									CreateMasterLPRecordBulk(pfcontLP,contsize,totalweight,totalcube,uompackflag,DOwhLocation,LPparent);

																								}
																							}

																							nlapiLogExecution('DEBUG', 'Container Size in Pick Face : ', contsize);
																						}
																						else if(cartonizationmethod=='4')
																						{
																							if(prvSalesord!=salesOrderNo)
																							{
																								pfcontLP='';
																								//pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																								prvSalesord=salesOrderNo;
																								//CreateMasterLPRecord(pfcontLP,contsize,totalweight,totalcube,uompackflag);
																							}
																						}
																						else
																						{
																							pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																						}
																						//var contsize="";								
																						var totalweight = parseFloat((expectedQuantity*uomweight))/parseFloat(uomqty);
																						var totalcube = parseFloat((expectedQuantity*uomcube))/parseFloat(uomqty);
																						var replenItemArray =  new Array();
																						var currentRow = new Array();
																						var j = 0;
																						var idx = 0;
																						var putawatqty=0;
																						var pickqty=0;
																						var pickfaceQty=0;
																						var openreplensqty=0;


																						var serchreplensqty=getopenreplens(salesOrderNo,fulfilmentItem,pfBinLocationId,pfWHLoc);
																						if(serchreplensqty!=null && serchreplensqty!='')
																						{
																							openreplensqty = serchreplensqty[0].getValue('custrecord_expe_qty', null, 'sum');
																						}

																						var searchputawyqty=getOpenputawayqty(pfBinLocationId);
																						if(searchputawyqty!=null && searchputawyqty!='')
																						{
																							putawatqty = searchputawyqty[0].getValue('custrecord_expe_qty', null, 'sum');
																						}



																						var searchpickfaceQty = getQOHForAllSKUsinPFLocation(pfBinLocationId,fulfilmentItem,pfWHLoc);
																						if(searchpickfaceQty!=null && searchpickfaceQty!='')
																						{
																							pickfaceQty=searchpickfaceQty[0].getValue('custrecord_ebiz_qoh',null,'sum');
																						}
																						var replenQty=pfLocationResults[z].getValue('custrecord_replenqty');
																						nlapiLogExecution('DEBUG','putawatqty',putawatqty);
																						nlapiLogExecution('DEBUG','pickqty',pickqty);
																						nlapiLogExecution('DEBUG','pickfaceQty',pickfaceQty);
																						nlapiLogExecution('DEBUG','replenQty',replenQty);
																						nlapiLogExecution('DEBUG','openreplensqty',openreplensqty);
																						nlapiLogExecution('DEBUG','pfRoundQty',pfRoundQty);

																						if(pfRoundQty == null || pfRoundQty == '')
																							pfRoundQty=1;																					
																						if(openreplensqty == null || openreplensqty == '')
																							openreplensqty=0;
																						if(pickqty == null || pickqty == '')
																							pickqty=0;
																						if(putawatqty == null || putawatqty == '')
																							putawatqty=0;
																						if(pickfaceQty == null || pickfaceQty == '')
																							pickfaceQty=0;
																						var tempQty = (parseFloat(pfMaximumQuantity) - (parseFloat(openreplensqty) + parseFloat(pickfaceQty)  - parseFloat(pickqty) + parseFloat(putawatqty)));
																						//var tempQty = parseFloat(pfMaximumQuantity) -  parseFloat(openreplensqty) - parseFloat(pickqty) + parseFloat(putawatqty)+parseFloat(pickfaceQty);

																						nlapiLogExecution('DEBUG','tempQty1',tempQty);

																						var tempwithRoundQty = Math.floor(parseFloat(tempQty)/parseFloat(pfRoundQty));

																						if(tempwithRoundQty == null || tempwithRoundQty == '' || isNaN(tempwithRoundQty))
																							tempwithRoundQty=0;

																						nlapiLogExecution('DEBUG','tempwithRoundQty',tempwithRoundQty);

																						tempQty = parseFloat(pfRoundQty)*parseFloat(tempwithRoundQty);

																						nlapiLogExecution('DEBUG','tempQty2',tempQty);

																						var replenTaskCount = Math.ceil(parseFloat(tempQty) / parseFloat(replenQty));
																						nlapiLogExecution('DEBUG','tempQty',tempQty);


																						currentRow = [pfItem, replenQty, replenrule, pfMaximumQuantity, pfMaximumPickQuantity, pickfaceQty, whLocation, 
																						              pfBinLocationId, tempQty, replenTaskCount, pffixedlp,pfstatus,'',pfRoundQty];
																						if(idx == 0)
																							idx = currentRow.length;

																						replenItemArray[j++] = currentRow;
																						var taskCount;
																						var zonesArray = new Array();

																						//var replenRulesList = getListForAllReplenZones();

																						var replenZoneList = "";
																						var replenZoneTextList = "";
																						var replenzonestatuslist="";
																						var replenzonestatustextlist="";
																						var Reportno = "";

																						if (replenRulesList != null){
																							for (var w = 0; w < replenRulesList.length; w++){

																								if (w == 0){
																									replenZoneList += replenRulesList[w].getValue('custrecord_replen_strategy_zone');
																									replenZoneTextList += replenRulesList[w].getText('custrecord_replen_strategy_zone');
																									replenzonestatuslist +=replenRulesList[w].getValue('custrecord_replen_strategy_itemstatus');
																									replenzonestatustextlist +=replenRulesList[w].getText('custrecord_replen_strategy_itemstatus');
																								} else {
																									replenZoneList += ',';
																									replenZoneList += replenRulesList[w].getValue('custrecord_replen_strategy_zone');

																									replenZoneTextList += ',';
																									replenZoneTextList += replenRulesList[w].getText('custrecord_replen_strategy_zone');

																									replenzonestatuslist += ',';
																									replenzonestatuslist += replenRulesList[w].getValue('custrecord_replen_strategy_itemstatus');

																									replenzonestatustextlist += ',';
																									replenzonestatustextlist += replenRulesList[w].getText('custrecord_replen_strategy_itemstatus');
																								}
																							}
																						} 

																						nlapiLogExecution('DEBUG','replenZoneList',replenZoneList);

																						var itemList = buildItemListForReplenProcessing(replenItemArray, j);
																						nlapiLogExecution('DEBUG','replen Item List',itemList);



																						var locnGroupList = getZoneLocnGroupsList(replenZoneList);	
																						nlapiLogExecution('DEBUG','Location Group List',locnGroupList);


																						var inventorySearchResultsReplen = getItemDetailsforRepln(locnGroupList, itemList,replenzonestatuslist);
																						nlapiLogExecution('DEBUG','inventorySearchResultsReplen',inventorySearchResultsReplen);
																						if ((inventorySearchResultsReplen != null && inventorySearchResultsReplen != "") || (remainingQuantity<=openreplensqty)){
																							nlapiLogExecution('DEBUG','test2','test2');
																							// Get the report number
																							/*if (Reportno == null || Reportno == "") {
																								Reportno = GetMaxTransactionNo('REPORT');
																								Reportno = Reportno.toString();
																							}*/


																							if (Reportno == null || Reportno == "") {
																								Reportno =  fnGetReplenNo(eBizWaveNo);

																								if (Reportno == null || Reportno == "") {

																									Reportno = GetMaxTransactionNo('REPORT');
																									Reportno = Reportno.toString();
																								}
																							}

																							/*
																							 * Loop through the total task count and get the location from where the item has to be 
																							 * replenished.
																							 */

																							//Get the Stage Location in case of two step replenishment process
																							var stageLocation='';
																							//GetStageLocation(item, "", whLocation, "", "BOTH");
																							nlapiLogExecution('DEBUG','Reportno',Reportno);
																							nlapiLogExecution('DEBUG','fulfilmentOrderNo',fulfilmentOrderNo);
																							nlapiLogExecution('DEBUG','replenQty',replenQty);
																							nlapiLogExecution('DEBUG','openreplensqty',openreplensqty);
																							nlapiLogExecution('DEBUG','remainingQuantity',remainingQuantity);


																							var resultArray=""; 																					


																							if(tempQty>0 && tempQty!='NaN')
																							{
																								resultArray=	replenRuleZoneLocation(inventorySearchResultsReplen, replenItemArray, Reportno, stageLocation, whLocation,fulfilmentOrderNo,eBizWaveNo);
																							}

																							var result=resultArray[0];

																							if(result==true || (remainingQuantity<=openreplensqty))
																							{

																								if(inventorySearchResultsReplen!=null && inventorySearchResultsReplen!='')
																									fromLot=inventorySearchResultsReplen[0].getText('custrecord_ebiz_inv_lot');	
																								//remainingQuantity = parseFloat(remainingQuantity) - parseFloat(actualAllocQty);
																								//updateInventoryWithAllocation(recordId, actualAllocQty);
																								nlapiLogExecution('DEBUG','fulfillOrderList',fulfillOrderList[i]);
																								nlapiLogExecution('DEBUG', 'KitFailed2 : ', KitFailed);
																								if(KitFailed== false)
																								{	
																									var test=fulfillOrderList[i][6];
																									fromLP = getfromLP(eBizWaveNo,fulfilmentItem);
																									nlapiLogExecution('DEBUG','test',test);
																									createRecordInOpenTask(fulfillOrderList[i], expectedQuantity, eBizWaveNo, pfcontLP, 
																											pfBinLocationId, recordId, packCode, fulfillmentItemStatus, pickMethod, pfpickzoneid, pickStrategyId,
																											whLocation, fromLP,uomlevel,totalweight,totalcube,contsize,fromLot,'',wmsCarrier,fulfilmentItem,nsrefno,
																											"",customer,dimsbulkpick,otparent,waveassignedto);

																									//updateFulfilmentOrder(fulfilmentOrderNo, fulfilmentOrderLineNo, remainingQuantity, eBizWaveNo);

																									var fopickgenqty = Math.floor(parseFloat(remainingQuantity)/parseFloat(memberitemqty));

																									var str = 'remainingQuantity. = ' + remainingQuantity + '<br>';
																									str = str + 'memberitemqty. = ' + memberitemqty + '<br>';	
																									str = str + 'fopickgenqty. = ' + fopickgenqty + '<br>';	
																									str = str + 'expectedQuantity. = ' + expectedQuantity + '<br>';	

																									nlapiLogExecution('DEBUG', 'FO Qty Details', str);

																									if(parseFloat(fopickgenqty)<=0)
																										fopickgenqty=expectedQuantity;

																									updateFulfilmentOrder(fulfillOrderList[i], fopickgenqty, eBizWaveNo,foparent);
																									/*var vInvTempArr1=new Array();

																								vInvTempArr1.push(recordId);
																								vInvTempArr1.push(actualAllocQty);
																								vInvFailedArr.push(vInvTempArr1);*/

																									var vFOTempArr=new Array();
																									vFOTempArr.push(fulfillOrderList[i]);
																									vFOTempArr.push(remainingQuantity);
																									vFOTempArr.push(eBizWaveNo);
																									vFOTempArr.push(foparent);
																									vFOFailedArr.push(vFOTempArr);

																									var vOTTempArr=new Array();
																									vOTTempArr.push(fulfillOrderList[i]);
																									vOTTempArr.push(expectedQuantity);
																									vOTTempArr.push(eBizWaveNo);
																									vOTTempArr.push(pfcontLP);
																									vOTTempArr.push(fulfilmentItem);
																									vOTFailedArr.push(vOTTempArr);
																									nlapiLogExecution('DEBUG', 'vFOFailedArr : ', vFOFailedArr);
																									nlapiLogExecution('DEBUG', 'vInvFailedArr : ', vInvFailedArr);
																									nlapiLogExecution('DEBUG', 'vFulfillArr : ', vFulfillArr);
																								}
																								else
																								{
																									vnotes='Insufficient Inventory for component items';
																									nlapiLogExecution('DEBUG', 'Creating Failed Task');
																									nlapiLogExecution('DEBUG', 'expectedQuantity',expectedQuantity);
																									createRecordInOpenTask(fulfillOrderList[i], expectedQuantity, eBizWaveNo, "", "", "",
																											"", "", "", "", "", whLocation, "","","","","","",vnotes,"",fulfilmentItem,nsrefno,
																											"",customer,dimsbulkpick,otparent,waveassignedto);

																									//updateFulfilmentOrder(fulfilmentOrderNo, fulfilmentOrderLineNo, 0, eBizWaveNo);
																									updateFulfilmentOrder(fulfillOrderList[i], 0, eBizWaveNo,foparent);
																									nlapiLogExecution('DEBUG', 'Creating Failed Task','SUCCESS');
																								}	
																								if((remainingQuantity>tempQty && tempQty>0) && (remainingQuantity>openreplensqty) )
																								{
																									remainingQuantity=parseFloat(remainingQuantity) - parseFloat(tempQty);
																								}
																								else
																								{
																									remainingQuantity=0;
																								}

																								// Maintain the allocation temporarily
																								//var allocatedInvRow = [beginLocationId, expectedQuantity,fromLP,inventoryItem,fromLot];
																								//allocatedInv.push(allocatedInvRow);
																							}
																						}
																						totreplenqty=totreplenqty+replenQty;

																					}
																				}
																				//Case # 20127328,20127083 Start
																				//}
																				//}
																				//Case # 20127328,20127083 End
																			}

																		}
																	}
																}
															}//

															if(inventorySearchResults != null && inventorySearchResults.length > 0)
															{
																for (var j = 0; j < inventorySearchResults.length; j++)
																{
																	inventoryItem = inventorySearchResults[j].getValue('custrecord_ebiz_inv_sku');
																	var InvWHLocation = inventorySearchResults[j].getValue('custrecord_ebiz_inv_loc');

																	var str = 'remainingQuantity. = ' + remainingQuantity + '<br>';
																	str = str + 'inventoryItem. = ' + inventoryItem + '<br>';	
																	str = str + 'fulfilmentItem. = ' + fulfilmentItem + '<br>';

																	nlapiLogExecution('DEBUG', 'Remaining Qty & Item', str);

																	if ((DOwhLocation==InvWHLocation)&&(inventoryItem.toString() == fulfilmentItem.toString()) && (parseFloat(remainingQuantity) > 0))
																	{
																		actualQty = inventorySearchResults[j].getValue('custrecord_ebiz_qoh');
																		inventoryAvailableQuantity = inventorySearchResults[j].getValue('custrecord_ebiz_avl_qty');
																		allocationQuantity = inventorySearchResults[j].getValue('custrecord_ebiz_alloc_qty');
																		recordId = inventorySearchResults[j].getId();
																		beginLocationId = inventorySearchResults[j].getValue('custrecord_ebiz_inv_binloc');
																		if(inventorySearchResults[j].getValue('custrecord_ebiz_inv_packcode')!=null && inventorySearchResults[j].getValue('custrecord_ebiz_inv_packcode')!='')
																			packCode = inventorySearchResults[j].getValue('custrecord_ebiz_inv_packcode');
																		itemStatus = inventorySearchResults[j].getValue('custrecord_ebiz_inv_sku_status');
																		whLocation = inventorySearchResults[j].getValue('custrecord_ebiz_inv_loc');
																		fromLP = inventorySearchResults[j].getValue('custrecord_ebiz_inv_lp');
																		fromLot = inventorySearchResults[j].getText('custrecord_ebiz_inv_lot');
																		expiryDate = inventorySearchResults[j].getValue('custrecord_ebiz_expdate');

																		var isValidRec = validateShelflife(samelot,latestexpirydt,shelflife,expiryDate);
																		if(isValidRec!='F')
																		{
																			// Get already allocated inv
																			var alreadyAllocQty = getAlreadyAllocatedInv(allocatedInv, beginLocationId,fromLP,inventoryItem,fromLot);

																			// Adjust for already allocated inv
																			//inventoryAvailableQuantity = parseFloat(inventoryAvailableQuantity) - parseFloat(alreadyAllocQty);

																			var str = 'Inventory Item Status. = ' + itemStatus + '<br>';
																			str = str + 'Fulfillment Item Status. = ' + fulfillmentItemStatus + '<br>';	
																			str = str + 'already AllocQty. = ' + alreadyAllocQty + '<br>';	
																			str = str + 'inventory Available Quantity. = ' + inventoryAvailableQuantity + '<br>';	
																			str = str + 'remaining Quantity. = ' + remainingQuantity + '<br>';	

																			nlapiLogExecution('DEBUG', 'Inventory Details', str);
																			var vPickfaceAllocLoc=false;
																			if(pickfaceArr != null && pickfaceArr != '')
																			{
																				if(pickfaceArr.indexOf(beginLocationId)!=-1)
																					vPickfaceAllocLoc=true;
																			}	
																			if(vPickfaceAllocLoc == false)
																			{
																				if((parseFloat(inventoryAvailableQuantity) > 0) && (itemStatus == fulfillmentItemStatus)
																						&& (beginLocationId != pfBinLocationId)){
																					var actualAllocationQuantity = Math.min(parseFloat(inventoryAvailableQuantity), parseFloat(remainingQuantity));
																					allocationQuantity = parseFloat(alreadyAllocQty) + parseFloat(actualAllocationQuantity);
																					remainingQuantity = parseFloat(remainingQuantity) - parseFloat(actualAllocationQuantity);
																					var expectedQuantity = parseFloat(actualAllocationQuantity);

																					var contsize="";	

																					var totalweight = parseFloat((expectedQuantity*uomweight))/parseFloat(uomqty);
																					var totalcube = parseFloat((expectedQuantity*uomcube))/parseFloat(uomqty);

																					//var totalweight = expectedQuantity*uomweight;
																					//var totalcube = expectedQuantity*uomcube;

																					var str = 'uompackflag. = ' + uompackflag + '<br>';
																					str = str + 'uomqty. = ' + uomqty + '<br>';	
																					str = str + 'expectedQuantity. = ' + expectedQuantity + '<br>';	
																					str = str + 'allocationQuantity. = ' + allocationQuantity + '<br>';	
																					str = str + 'cartonizationmethod. = ' + cartonizationmethod + '<br>';	
																					str = str + 'remaining Quantity. = ' + remainingQuantity + '<br>';	

																					nlapiLogExecution('DEBUG', 'Qty Details', str);

																					//1-Ship Cartons	2-Build Cartons		3-Ship Pallets
																					if((uompackflag=="1" || uompackflag=="3") &&(parseFloat(expectedQuantity) == parseFloat(uomqty)))
																					{
																						for(l=0;containerslist!=null && l<containerslist.length;l++){     

																							var containername = containerslist[l].getValue('custrecord_containername');

																							if(containername=='SHIPASIS')
																							{
																								contsize = containerslist[l].getValue('Internalid');
																								invtcontLP = GetMaxLPNo('1', '2',DOwhLocation);

																								//CreateMasterLPRecord(pfcontLP,contsize,totalweight,totalcube,uompackflag,DOwhLocation,LPparent);

																								CreateMasterLPRecordBulk(invtcontLP,contsize,totalweight,totalcube,uompackflag,DOwhLocation,LPparent);

																							}
																						}

																						nlapiLogExecution('DEBUG', 'Container Size in Bulk Inventory : ', contsize);
																					}
																					else if(cartonizationmethod=='4') 
																					{
																						if(prvSalesord!=salesOrderNo)
																						{
																							invtcontLP='';
																							//invtcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																							prvSalesord=salesOrderNo;
																							//CreateMasterLPRecord(invtcontLP,contsize,totalweight,totalcube,uompackflag);
																						}
																					}

																					nlapiLogExecution('DEBUG', 'Container LP in Inventory : ', invtcontLP);
																					nlapiLogExecution('DEBUG', 'KitFailed3 : ', KitFailed);
																					if(KitFailed== false)
																					{	
																						//updateInventoryWithAllocation(recordId, allocationQuantity);
																						updateInventoryWithAllocation(recordId, actualAllocationQuantity);

																						createRecordInOpenTask(fulfillOrderList[i], expectedQuantity, eBizWaveNo, invtcontLP, beginLocationId, recordId,
																								packCode, itemStatus, pickMethod, pickZone, pickStrategyId, whLocation, fromLP,uomlevel,
																								totalweight,totalcube,contsize,fromLot,'',wmsCarrier,fulfilmentItem,nsrefno,expiryDate,customer,
																								dimsbulkpick,otparent,waveassignedto);

																						//var fopickgenqty = Math.floor(parseFloat(remainingQuantity)/parseFloat(memberitemqty));
																						var fopickgenqty = Math.floor(parseFloat(actualAllocationQuantity)/parseFloat(memberitemqty));
																						var str = 'actualAllocationQuantity. = ' + actualAllocationQuantity + '<br>';
																						str = str + 'memberitemqty. = ' + memberitemqty + '<br>';	
																						str = str + 'fopickgenqty. = ' + fopickgenqty + '<br>';	
																						str = str + 'expectedQuantity. = ' + expectedQuantity + '<br>';	

																						nlapiLogExecution('DEBUG', 'FO Qty Details', str);

																						if(parseFloat(fopickgenqty)<=0)
																							fopickgenqty = expectedQuantity;
																						//updateFulfilmentOrder(fulfilmentOrderNo, fulfilmentOrderLineNo, actualAllocationQuantity, eBizWaveNo);
																						updateFulfilmentOrder(fulfillOrderList[i], fopickgenqty, eBizWaveNo,foparent);

																						if((subitemType=='lotnumberedinventoryitem' || subitemType=='lotnumberedassemblyitem' || subbatchflag=='T')
																								&& (customer!=null && customer!=''))
																						{
																							createCustomerLot(fulfillOrderList[i],customer,fulfilmentItem,fromLot,expiryDate,whLocation);
																						}
																						var vInvTempArr1=new Array();

																						vInvTempArr1.push(recordId);
																						vInvTempArr1.push(actualAllocationQuantity);
																						vInvFailedArr.push(vInvTempArr1);

																						var vFOTempArr=new Array();
																						vFOTempArr.push(fulfillOrderList[i]);
																						vFOTempArr.push(actualAllocationQuantity);
																						vFOTempArr.push(eBizWaveNo);
																						vFOTempArr.push(foparent);
																						vFOFailedArr.push(vFOTempArr);

																						var vOTTempArr=new Array();
																						vOTTempArr.push(fulfillOrderList[i]);
																						vOTTempArr.push(expectedQuantity);
																						vOTTempArr.push(eBizWaveNo);
																						vOTTempArr.push(invtcontLP);
																						vOTTempArr.push(fulfilmentItem);
																						vOTFailedArr.push(vOTTempArr);
																						nlapiLogExecution('DEBUG', 'vFOFailedArr : ', vFOFailedArr);
																						nlapiLogExecution('DEBUG', 'vInvFailedArr : ', vInvFailedArr);
																						nlapiLogExecution('DEBUG', 'vFulfillArr : ', vFulfillArr);
																						// Maintain the allocation temporarily
																						var allocatedInvRow = [beginLocationId, expectedQuantity,fromLP,inventoryItem,fromLot];
																						allocatedInv.push(allocatedInvRow);
																					}
																					else
																					{
																						vnotes='Insufficient Inventory for component items';
																						nlapiLogExecution('DEBUG', 'Creating Failed Task');
																						nlapiLogExecution('DEBUG', 'expectedQuantity',expectedQuantity);
																						createRecordInOpenTask(fulfillOrderList[i], expectedQuantity, eBizWaveNo, "", "", "",
																								"", "", "", "", "", whLocation, "",uomlevel,"","","","",vnotes,"",fulfilmentItem,nsrefno,
																								"",customer,dimsbulkpick,otparent,waveassignedto);

																						//updateFulfilmentOrder(fulfilmentOrderNo, fulfilmentOrderLineNo, 0, eBizWaveNo);
																						updateFulfilmentOrder(fulfillOrderList[i], 0, eBizWaveNo,foparent);
																						nlapiLogExecution('DEBUG', 'Creating Failed Task','SUCCESS');
																					}

																				}	
																			}
																		}
																	}
																}
															}

														}

													}

													//If there is still some quantity remaining, then insert a failed task
													if(parseFloat(nooflps)>0 && parseFloat(remainingQuantity) > 0)
													{
														vnotes='Insufficient Inventory';
														KitFailed= true;
														nlapiLogExecution('DEBUG', 'Creating Failed Task');
														nlapiLogExecution('DEBUG', 'remainingQuantity',remainingQuantity);
														createRecordInOpenTask(fulfillOrderList[i], remainingQuantity, eBizWaveNo, "", "", "",
																"", "", "", "", "", DOwhLocation, "",uomlevel,"","","","",vnotes,"",fulfilmentItem,nsrefno,
																"",customer,dimsbulkpick,otparent,waveassignedto);

														//updateFulfilmentOrder(fulfilmentOrderNo, fulfilmentOrderLineNo, 0, eBizWaveNo);
														updateFulfilmentOrder(fulfillOrderList[i], 0, eBizWaveNo,foparent);
														nlapiLogExecution('DEBUG', 'Creating Failed Task','SUCCESS');
														vBoolFailedForSameLP=true;
														remainingQuantity=0;
														nlapiLogExecution('DEBUG', 'KitFailed5 : ', KitFailed);
													}
												}
											}
										}
									}
								}
							}
						}
						//}
						//}
					}
				}
			}
			else
			{
				fulfilmentItem = fulfillOrderList[i][6];				// ITEM/SKU
				fulfilmentQuantity = fulfillOrderList[i][9];			// ORDER QUANTITY
				remainingQuantity = parseFloat(fulfilmentQuantity);		
				fulfilmentOrderNo = fulfillOrderList[i][3]; 			// ORDER NO.
				salesOrderNo = fulfillOrderList[i][1]; 					// SALES ORDER INTERNAL ID.
				fulfilmentOrderLineNo = fulfillOrderList[i][2];			// FULFILMENT ORDER LINE NO.
				fulfillmentItemStatus=fulfillOrderList[i][7];			// FULFILMENT Item Status.
				DOwhLocation = fulfillOrderList[i][19];					// FULFILMENT WAREHOUSE LOCATION
				packCode=fulfillOrderList[i][10];						// PACKCODE
				wmsCarrier = fulfillOrderList[i][20];					// WMS CARRIER
				nsrefno = fulfillOrderList[i][21];						// ITEM FULFILLMENT REFERENCE #
				ordtype=fulfillOrderList[i][17];						// Order Type  	
				whLocation=fulfillOrderList[i][19];
				lefoflag = fulfillOrderList[i][37];
				vLotNoSplit = fulfillOrderList[i][38];
				samelot = fulfillOrderList[i][39];
				shelflife = fulfillOrderList[i][40];
				customer = fulfillOrderList[i][35];
				ActSoLoc = fulfillOrderList[i][41];
				ordpriority = fulfillOrderList[i][18];

				var str = 'fulfilmentOrderNo. = ' + fulfilmentOrderNo + '<br>';
				str = str + 'fulfilmentQuantity. = ' + fulfilmentQuantity + '<br>';	
				str = str + 'fulfilmentItem. = ' + fulfilmentItem + '<br>';	
				str = str + 'packCode. = ' + packCode + '<br>';	
				str = str + 'fulfillmentItemStatus. = ' + fulfillmentItemStatus + '<br>';	
				str = str + 'fulfillment Warehouse Location. = ' + DOwhLocation + '<br>';
				str = str + 'wmsCarrier. = ' + wmsCarrier + '<br>';
				str = str + 'itemType. = ' + itemType + '<br>';
				str = str + 'batchflag. = ' + batchflag + '<br>';
				str = str + 'lefoflag. = ' + lefoflag + '<br>';
				str = str + 'vLotNoSplit. = ' + vLotNoSplit + '<br>';
				str = str + 'samelot. = ' + samelot + '<br>';
				str = str + 'shelflife. = ' + shelflife + '<br>';
				str = str + 'customer. = ' + customer + '<br>';
				str = str + 'ActSoLocation. = ' + ActSoLoc + '<br>';
				str = str + 'ordtype. = ' + ordtype + '<br>';
				str = str + 'ordpriority. = ' + ordpriority + '<br>';

				nlapiLogExecution('DEBUG', 'Fulfillment Ord Line Details', str);
				if((itemType=='lotnumberedinventoryitem' || itemType=='lotnumberedassemblyitem' || batchflag=='T')
						&&(customer!=null && customer!='') && samelot=='T')
				{
					var customerlotdet = getLatestExpiry(customer,fulfilmentItem,whLocation);
					if(customerlotdet!=null && customerlotdet!='')
					{
						latestexpirydt = customerlotdet[0].getValue('custrecord_ebiz_lot_expiry');
					}
				}

				var arrLPRequired=new Array();
				if(vLotNoSplit != 'T' && vLotExcepId != null && vLotExcepId != '')
				{	
					nlapiLogExecution('DEBUG', 'vLotExcepId', vLotExcepId);
					var vLotExc=vLotExcepId.split(',');
					nlapiLogExecution('DEBUG', 'vLotExc', vLotExc);
					var vLotExcQt=vLotExcepQty.split(',');
					nlapiLogExecution('DEBUG', 'vLotExcQt', vLotExcQt);
					for(var p=0;p<vLotExc.length;p++)
					{	
						var vLotRemQty=vLotExcQt[p];
						vLotNo=vLotExc[p];
						var arrLPRequiredTemp = pickgen_palletisation(fulfilmentItem,vLotRemQty,vLotNo,DOwhLocation);
						if(arrLPRequiredTemp != null && arrLPRequiredTemp != '')
						{
							for(var z=0;z<arrLPRequiredTemp.length;z++)
							{
								arrLPRequired.push(arrLPRequiredTemp[z]);
							}
						}
					}
				}
				else
				{	
					arrLPRequired = pickgen_palletisation(fulfilmentItem,fulfilmentQuantity,null,DOwhLocation);
				}
				if(arrLPRequired==null || arrLPRequired==''||arrLPRequired.length==0)
				{
					vnotes='Missing Dimensions';
					nlapiLogExecution('DEBUG', 'CallingFunction1', 'Sucess');
					createRecordInOpenTask(fulfillOrderList[i], remainingQuantity, eBizWaveNo, "", "", "",
							"", "", "", "", "", DOwhLocation, "","","","","","",vnotes,"",fulfilmentItem,nsrefno,
							"",customer,dimsbulkpick,otparent,waveassignedto);

					//updateFulfilmentOrder(fulfilmentOrderNo, fulfilmentOrderLineNo, 0, eBizWaveNo);
					updateFulfilmentOrder(fulfillOrderList[i], 0, eBizWaveNo,foparent);

				}
				else
				{
					var nooflps = "";
					var uomlevel = "";
					var lpbrkqty = "";
					var uompackflag = "";
					var uomweight="";
					var uomcube="";
					var uomqty="";
					var vuom=''
						var baseuomid='';
					for(s = 0;s < parseFloat(arrLPRequired.length);s++)
					{
						nlapiLogExecution('DEBUG', 'arrLPRequired.length',arrLPRequired.length);
						nooflps = arrLPRequired[s][0];
						uomlevel = arrLPRequired[s][1];
						lpbrkqty = arrLPRequired[s][2];
						uompackflag = arrLPRequired[s][3];
						uomweight = arrLPRequired[s][4];
						uomcube= arrLPRequired[s][5];
						uomqty = arrLPRequired[s][6];
						dimsbulkpick = arrLPRequired[s][8];
						var vSplitLot = arrLPRequired[s][7];
						vuom = arrLPRequired[s][9];
						baseuomid = arrLPRequired[s][10];
						remainingQuantity = parseFloat(lpbrkqty);
						nlapiLogExecution('DEBUG', 'nooflps',nooflps);

						if(parseFloat(nooflps)>0)
						{

							var allocConfigDtls1 = getListForAllPickRules(fulfilmentItem,uomlevel,DOwhLocation,fulfillmentItemStatus,
									packCode,ordtype,vuom,ordpriority);

							if(allocConfigDtls1==null || allocConfigDtls1==''||allocConfigDtls1.length==0)
							{
								vnotes='Pick Strategy Error';
								nlapiLogExecution('DEBUG', 'CallingFunction1', 'Sucess');
								createRecordInOpenTask(fulfillOrderList[i], remainingQuantity, eBizWaveNo, "", "", "",
										"", "", "", "", "", DOwhLocation, "","","","","","",vnotes,"",fulfilmentItem,nsrefno,
										"",customer,dimsbulkpick,otparent,waveassignedto,baseuomid);

								//updateFulfilmentOrder(fulfilmentOrderNo, fulfilmentOrderLineNo, 0, eBizWaveNo);
								updateFulfilmentOrder(fulfillOrderList[i], 0, eBizWaveNo,foparent);
							}
							else
							{
								nlapiLogExecution('DEBUG', 'secondtest', 'secondtest');

								var vBoolFailedForSameLP=false;

								nlapiLogExecution('DEBUG', 'nooflps', 'nooflps');

								for(t=1; t<=parseFloat(nooflps);t++)
								{
									nlapiLogExecution('DEBUG', 't',t);
									nlapiLogExecution('DEBUG', 'vBoolFailedForSameLP',vBoolFailedForSameLP);

									var str = 'nooflps. = ' + nooflps + '<br>';
									str = str + 'lpbrkqty. = ' + lpbrkqty + '<br>';	
									str = str + 'uomlevel. = ' + uomlevel + '<br>';
									str = str + 'remainingQuantity. = ' + remainingQuantity + '<br>';

									nlapiLogExecution('DEBUG', 'Breakdown Details', str);

									if(remainingQuantity==null || remainingQuantity==''|| remainingQuantity==0)
										remainingQuantity=lpbrkqty;									

									if(vBoolFailedForSameLP ==true && t>1)
									{
										//If there is still some quantity remaining, then insert a failed task
										if(parseFloat(nooflps)>0 && parseFloat(remainingQuantity) > 0)
										{
											vnotes='Insufficient Inventory';
											nlapiLogExecution('DEBUG', 'Creating Failed Task');
											nlapiLogExecution('DEBUG', 'remainingQuantity',remainingQuantity);
											createRecordInOpenTask(fulfillOrderList[i], remainingQuantity, eBizWaveNo, "", "", "",
													"", "", "", "", "", DOwhLocation, "",uomlevel,"","","","",vnotes,"",fulfilmentItem,nsrefno,
													"",customer,dimsbulkpick,otparent,waveassignedto,baseuomid);
											//updateFulfilmentOrder(fulfillOrderList[i], fulfilmentOrderLineNo, 0, eBizWaveNo,foparent);
											updateFulfilmentOrder(fulfillOrderList[i],  0, eBizWaveNo,foparent);
											nlapiLogExecution('DEBUG', 'Creating Failed Task','SUCCESS');
											vBoolFailedForSameLP=true;
											remainingQuantity=0;

										}	
									}
									else
									{

										nlapiLogExecution('DEBUG', 'remainingQuantity', remainingQuantity);
										var isForwardpickReplen='F';
										var pickZone='';
										var fields = ['custitem_ebiz_forwardpick'];
										var columns= nlapiLookupField('Item',fulfilmentItem,fields);
										var IsItemForwardPick = columns.custitem_ebiz_forwardpick;
										nlapiLogExecution('DEBUG', 'item.forwardpick', IsItemForwardPick);
										if(parseFloat(remainingQuantity) > 0) 
										{
											if(IsItemForwardPick=='T'){
												//checking for the pickzone that has forward pick true.
												for(v = 0; v < parseFloat(allocConfigDtls1.length) && (pickZone ==''); v++)
												{

													var pfLocationResults = allocConfigDtls1[v][7];
													var pickMethod = allocConfigDtls1[v][2];
													//pickZone = allocConfigDtls1[v][3];
													var pickStrategyId = allocConfigDtls1[v][1];
													var allocationStrategy = allocConfigDtls1[v][6];
													var binlocid=allocConfigDtls1[v][5];
													var binlocgrpid=allocConfigDtls1[v][4];
													cartonizationmethod=allocConfigDtls1[v][12];
													var stagedetermination=allocConfigDtls1[v][13];

													var vforwardPick=allocConfigDtls1[v][14];
													if(vforwardPick=='T')
													{
														pickZone = allocConfigDtls1[v][3];
													}

													if(cartonizationmethod=="" && cartonizationmethod==null)
														cartonizationmethod='4';

													var str = 'pfLocationResults. = ' + pfLocationResults + '<br>';
													str = str + 'pickMethod. = ' + pickMethod + '<br>';	
													str = str + 'pickStrategyId. = ' + pickStrategyId + '<br>';
													str = str + 'pickZone. = ' + pickZone + '<br>';
													str = str + 'binlocgrpid. = ' + binlocgrpid + '<br>';
													str = str + 'binlocid. = ' + binlocid + '<br>';
													str = str + 'allocationStrategy. = ' + allocationStrategy + '<br>';
													str = str + 'cartonizationmethod. = ' + cartonizationmethod + '<br>';
													str = str + 'forwardPick. = ' + vforwardPick + '<br>';

													nlapiLogExecution('DEBUG', 'Pick Strategy Details', str);

												}

												nlapiLogExecution('DEBUG', 'pivkmethod.forwardpick', vforwardPick);
												if(IsItemForwardPick!=null && IsItemForwardPick!='' && IsItemForwardPick!='null' && IsItemForwardPick=='T' && vforwardPick!=null && vforwardPick!='' && vforwardPick!='null' &&  vforwardPick=='T')
												{
													var filters = new Array();
													filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
													filters.push(new nlobjSearchFilter('custrecord_ebiz_forwardpick_replen', null, 'is', 'T'));

													var columns = new Array();
													columns[0] = new nlobjSearchColumn('custrecord_replen_strategy_method');
													columns[1] = new nlobjSearchColumn('custrecord_replen_strategy_zone');
													columns[2] = new nlobjSearchColumn('custrecord_replen_strategy_itemstatus');
													columns[3] = new nlobjSearchColumn('custrecord_ebiz_forwardpick_replen');

													var replenRulesList = new nlapiSearchRecord('customrecord_ebiznet_replenish_strategy', null, filters, columns);

													var replenZoneList = "";
													var replenZoneTextList = "";
													var replenzonestatuslist="";
													var replenzonestatustextlist="";
													var Reportno = "";

													if (replenRulesList != null){
														for (var w = 0; w < replenRulesList.length; w++){
															isForwardpickReplen=replenRulesList[w].getValue('custrecord_ebiz_forwardpick_replen');
															if (w == 0){
																replenZoneList += replenRulesList[w].getValue('custrecord_replen_strategy_zone');
																replenZoneTextList += replenRulesList[w].getText('custrecord_replen_strategy_zone');
																replenzonestatuslist +=replenRulesList[w].getValue('custrecord_replen_strategy_itemstatus');
																replenzonestatustextlist +=replenRulesList[w].getText('custrecord_replen_strategy_itemstatus');
															} else {
																replenZoneList += ',';
																replenZoneList += replenRulesList[w].getValue('custrecord_replen_strategy_zone');

																replenZoneTextList += ',';
																replenZoneTextList += replenRulesList[w].getText('custrecord_replen_strategy_zone');

																replenzonestatuslist += ',';
																replenzonestatuslist += replenRulesList[w].getValue('custrecord_replen_strategy_itemstatus');

																replenzonestatustextlist += ',';
																replenzonestatustextlist += replenRulesList[w].getText('custrecord_replen_strategy_itemstatus');
															}
														}
													} 

													nlapiLogExecution('DEBUG','replenZoneList',replenZoneList);

													var itemList = new Array();
													itemList.push(fulfilmentItem);//buildItemListForReplenProcessing(replenItemArray, j);
													nlapiLogExecution('DEBUG','replen Item List',itemList);

													var TolocnGroupList = getZoneLocnGroupsList(pickZone);	
													nlapiLogExecution('DEBUG','Location Group List',TolocnGroupList);

													var inventorySearchResultsReplen = getItemDetailsforRepln(locnGroupList, itemList,replenzonestatuslist,lefoflag);
													nlapiLogExecution('DEBUG','inventorySearchResultsReplen',inventorySearchResultsReplen);

													if ((inventorySearchResultsReplen != null && inventorySearchResultsReplen != "") ){
														nlapiLogExecution('DEBUG','test2','test2');
														// Get the report number
														/*if (Reportno == null || Reportno == "") {
															Reportno = GetMaxTransactionNo('REPORT');
															Reportno = Reportno.toString();
														}*/
														
														if (Reportno == null || Reportno == "") {
															Reportno =  fnGetReplenNo(eBizWaveNo);

															if (Reportno == null || Reportno == "") {

																Reportno = GetMaxTransactionNo('REPORT');
																Reportno = Reportno.toString();
															}
														}

														/*
														 * Loop through the total task count and get the location from where the item has to be 
														 * replenished.
														 */

														//Get the Stage Location in case of two step replenishment process
														var stageLocation='';
														//GetStageLocation(item, "", whLocation, "", "BOTH");
														nlapiLogExecution('DEBUG','Reportno',Reportno);
														nlapiLogExecution('DEBUG','fulfilmentOrderNo',fulfilmentOrderNo);
														nlapiLogExecution('DEBUG','replenQty',replenQty);
														nlapiLogExecution('DEBUG','openreplensqty',openreplensqty);
														nlapiLogExecution('DEBUG','remainingQuantity',remainingQuantity);
														//fetching the inventory results of the fetched pick zone for getting the empty bin locations
														var invtResults=getInventorySearchResults(-1,TolocnGroupList);
														var replenItemArray=new Array();														
														var filters = new Array();										
														filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
														if(whLocation!=null && whLocation!='')
															filters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'is', whLocation));
														if(invtResults!=null && invtResults!='')
															filters.push(new nlobjSearchFilter('internalid',null,'noneof',invtResults));

														if(TolocnGroupList!=null && TolocnGroupList!="" && TolocnGroupList!=","){	
															filters.push(new nlobjSearchFilter('custrecord_outboundlocgroupid',null, 'anyof', TolocnGroupList));
														}
														nlapiLogExecution('DEBUG', 'TolocnGroupList', TolocnGroupList);																
													}
													var columns = new Array();
													columns[0] = new nlobjSearchColumn('custrecord_outboundlocgroupid');
													columns[1] = new nlobjSearchColumn('name');
													columns[0].setSort();

													// Specify the sort sequence

													var binlocationSearchResults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);
													var endLocationId='';
													if(binlocationSearchResults!=null && binlocationSearchResults!='')
													{
														nlapiLogExecution('DEBUG', 'binlocationSearchResults', binlocationSearchResults.length);	
														for(var l1=0;l1<binlocationSearchResults.length;l1++)
														{
															tempendLocationId=binlocationSearchResults[l1].getId();
															//checking for any open replens for the fetched bin
															var serchreplensqty=getopenreplensNew(salesOrderNo,fulfilmentItem,tempendLocationId,whLocation);
															nlapiLogExecution('DEBUG', 'serchreplensqty', serchreplensqty);
															if(serchreplensqty!=null && serchreplensqty!='')
															{
																continue;
															}
															else
															{
																endLocationId=tempendLocationId;
																break;
															}
														}
													}
													/*if(endLocationId!='')
                                                         {*/

													currentRow = [fulfilmentItem,endLocationId,remainingQuantity];


													replenItemArray[0] = currentRow;


													var result=	pickRuleZoneLocation(inventorySearchResultsReplen, replenItemArray, Reportno, stageLocation, whLocation,fulfilmentOrderNo,eBizWaveNo);


													if(result==true)
													{

														nlapiLogExecution('DEBUG','fulfillOrderList',fulfillOrderList[i]);
														var test=fulfillOrderList[i][6];

														nlapiLogExecution('DEBUG','test',test);
														recordId="";
														if(fromLP==null || fromLP=='null' || fromLP=='undefined' || fromLP=='')
														{
															fromLP='';
														}
														if(fromLot==null || fromLot=='null' || fromLot=='undefined' || fromLot=='')
														{
															fromLot='';
														}
														createRecordInOpenTask(fulfillOrderList[i], remainingQuantity, eBizWaveNo, pfcontLP, 
																endLocationId, recordId, packCode, fulfillmentItemStatus, pickMethod, '', pickStrategyId,
																whLocation, fromLP,uomlevel,totalweight,totalcube,contsize,fromLot,'',wmsCarrier,fulfilmentItem,
																nsrefno,expiryDate,customer,dimsbulkpick,otparent,waveassignedto,baseuomid);

														//updateFulfilmentOrder(fulfilmentOrderNo, fulfilmentOrderLineNo, remainingQuantity, eBizWaveNo);
														updateFulfilmentOrder(fulfillOrderList[i], remainingQuantity, eBizWaveNo,foparent);													
														remainingQuantity=0;
													}
													//}
												}
											}
											//this is normal scenario(items  forward pick as false)
											if(isForwardpickReplen == 'F')
											{	
												for(v = 0; v < parseFloat(allocConfigDtls1.length) && parseFloat(remainingQuantity) > 0; v++)
												{

													var pfLocationResults = allocConfigDtls1[v][7];
													var pickMethod = allocConfigDtls1[v][2];
													var pickZone = allocConfigDtls1[v][3];
													var pickStrategyId = allocConfigDtls1[v][1];
													var allocationStrategy = allocConfigDtls1[v][6];
													var binlocid=allocConfigDtls1[v][5];
													var binlocgrpid=allocConfigDtls1[v][4];
													cartonizationmethod=allocConfigDtls1[v][12];
													var stagedetermination=allocConfigDtls1[v][13];

													var vforwardPick=allocConfigDtls1[v][27];

													if(cartonizationmethod=="" && cartonizationmethod==null)
														cartonizationmethod='4';

													var str = 'pfLocationResults. = ' + pfLocationResults + '<br>';
													str = str + 'pickMethod. = ' + pickMethod + '<br>';	
													str = str + 'pickStrategyId. = ' + pickStrategyId + '<br>';
													str = str + 'pickZone. = ' + pickZone + '<br>';
													str = str + 'binlocgrpid. = ' + binlocgrpid + '<br>';
													str = str + 'binlocid. = ' + binlocid + '<br>';
													str = str + 'allocationStrategy. = ' + allocationStrategy + '<br>';
													str = str + 'cartonizationmethod. = ' + cartonizationmethod + '<br>';
													str = str + 'forwardPick. = ' + vforwardPick + '<br>';

													nlapiLogExecution('DEBUG', 'Pick Strategy Details', str);

													//var allLocnGroups = getAllLocnGroups(allocConfigDtls1[v]);

													var allLocnGroups =  getLocnGroupListForOrder(binlocgrpid, binlocid, pickZone);

													nlapiLogExecution('DEBUG', 'allLocnGroups', allLocnGroups);

													if (pfLocationResults != null && pfLocationResults.length > 0)
													{
														for(var P = 0; P < pfLocationResults.length; P++)
														{
															var pfBinLocationgroupId = pfLocationResults[P].getValue('custrecord_inboundlocgroupid', 'custrecord_pickbinloc');

															nlapiLogExecution('DEBUG', 'pfBinLocationgroupId', pfBinLocationgroupId);
														}
													}
//													case # 20148032
													if((allLocnGroups!=null && allLocnGroups!='') || (pfBinLocationgroupId!=null && pfBinLocationgroupId!='') || (binlocid!=null && binlocid!='')){
														inventorySearchResults = getItemDetails(allLocnGroups, fulfilmentItem,allocationStrategy,pfBinLocationgroupId,lefoflag,
																vLotExcepId,vLotNoSplit,fulfilmentQuantity,samelot,latestexpirydt,binlocid);		

													}
													else
														inventorySearchResults=null;
													nlapiLogExecution('DEBUG', 'inventorySearchResults', inventorySearchResults);
													var pfBinLocationId;
													var pickfaceArr=new Array();
													if((inventorySearchResults!=null && inventorySearchResults!='') || (pfLocationResults!=null && pfLocationResults!=''))
													{
														if (pfLocationResults != null && pfLocationResults.length > 0)
														{
															for(var z = 0; z < pfLocationResults.length; z++)
															{
																if(parseFloat(remainingQuantity) > 0)
																{
																	var pfItem = pfLocationResults[z].getValue('custrecord_pickfacesku');
																	var Autoreplenflag=pfLocationResults[z].getValue('custrecord_autoreplen');
																	if (pfItem == fulfilmentItem)
																	{
																		nlapiLogExecution('DEBUG', 'item Match',pfItem);
																		pfBinLocationId = pfLocationResults[z].getValue('custrecord_pickbinloc');
																		pfMaximumQuantity = pfLocationResults[z].getValue('custrecord_maxqty');
																		pfMaximumPickQuantity = pfLocationResults[z].getValue('custrecord_maxpickqty');
																		pffixedlp = pfLocationResults[z].getText('custrecord_pickface_ebiz_lpno');
																		pfstatus=pfLocationResults[z].getValue('custrecord_pickface_itemstatus');
																		replenrule=pfLocationResults[z].getValue('custrecord_pickruleid');
																		pfRoundQty=pfLocationResults[z].getValue('custrecord_roundqty');
																		pfWHLoc=pfLocationResults[z].getValue('custrecord_pickface_location');
																		pfpickzoneid = pfLocationResults[z].getValue('custrecord_pickzone');

																		var str = 'pfBinLocationId. = ' + pfBinLocationId + '<br>';
																		str = str + 'pfMaximumQuantity. = ' + pfMaximumQuantity + '<br>';	
																		str = str + 'pfMaximumPickQuantity. = ' + pfMaximumPickQuantity + '<br>';
																		str = str + 'remainingQuantity. = ' + remainingQuantity + '<br>';
																		str = str + 'pffixedlp. = ' + pffixedlp + '<br>';
																		str = str + 'pfstatus. = ' + pfstatus + '<br>';
																		str = str + 'replenrule. = ' + replenrule + '<br>';
																		str = str + 'pfpickzoneid. = ' + pfpickzoneid + '<br>';

																		nlapiLogExecution('DEBUG', 'Pickface Details', str);
																		//case# 20127685 starts (cluster number not generating issue)
																		//if(pfpickzoneid==null && pfpickzoneid=='')
																		if(pfpickzoneid==null || pfpickzoneid=='')
																		{
																			var filtersLocGrp = new Array();
																			filtersLocGrp.push(new nlobjSearchFilter('custrecord_locgroup_no', null, 'anyof', pfBinLocationgroupId));
																			var columnsLocGrp = new Array();
																			columnsLocGrp[0] = new nlobjSearchColumn('custrecordcustrecord_putzoneid');
																			var SearchLocGrp = nlapiSearchRecord('customrecord_zone_locgroup', null, filtersLocGrp, columnsLocGrp);
																			if(SearchLocGrp != null && SearchLocGrp != '')
																			{
																				pfpickzoneid = SearchLocGrp[0].getValue('custrecordcustrecord_putzoneid');
																			}
																			else
																			{
																				pfpickzoneid=pickZone;
																			}
																			nlapiLogExecution('ERROR', 'pfpickzoneid', pfpickzoneid);

																		}
																		nlapiLogExecution('DEBUG', 'pfpickzoneid ', pfpickzoneid);
																		//case# 20127685 end
																		if((parseFloat(pfMaximumPickQuantity) >= parseFloat(remainingQuantity)) 
																				|| (uompackflag!="1" && uompackflag!="3"))
																		{
																			var pickqty=0;
																			var searchpickyqty=getOpenPickqty(pfBinLocationId);
																			if(searchpickyqty!=null && searchpickyqty!='')
																			{
																				pickqty = searchpickyqty[0].getValue('custrecord_expe_qty', null, 'sum');
																			}

																			nlapiLogExecution('DEBUG', 'pickqty', pickqty);

																			var availableQty ="";
																			var allocQty = "";
																			var recordId = "";

																			var itemStatus = "";
																			var fromLP = "";
																			var inventoryItem="";
																			var fromLot="";
																			// Get inventory details for this pickface location
//																			var binLocnInvDtls = getAvailQtyFromInventory(inventorySearchResults, pfBinLocationId, allocatedInv,DOwhLocation,fulfilmentItem
//																			,fulfillmentItemStatus,samelot,latestexpirydt,shelflife);
																			var binLocnInvDtls = getAvailQtyFromInventory(inventorySearchResults, pfBinLocationId, null,DOwhLocation,fulfilmentItem
																					,fulfillmentItemStatus,samelot,latestexpirydt,shelflife);

																			nlapiLogExecution('DEBUG', 'binLocnInvDtls', binLocnInvDtls);

																			if(binLocnInvDtls!=null && binLocnInvDtls.length>0)
																			{
																				for(var w=0;w<binLocnInvDtls.length;w++)
																				{	
																					if(parseFloat(remainingQuantity) > 0) 
																					{
																						availableQty = binLocnInvDtls[w][2];
																						allocQty = binLocnInvDtls[w][3];
																						recordId = binLocnInvDtls[w][8];
																						if(binLocnInvDtls[w][4]!=null && binLocnInvDtls[w][4]!='')
																							packCode = binLocnInvDtls[w][4];
																						itemStatus = binLocnInvDtls[w][5];										
																						whLocation = binLocnInvDtls[w][6];
																						fromLP = binLocnInvDtls[w][7];											
																						inventoryItem = binLocnInvDtls[w][9];
																						fromLot = binLocnInvDtls[w][10];
																						expiryDate = binLocnInvDtls[w][11];



																						/*
																						 * If the allocation strategy is not null check in inventory for that item.						 
																						 * 		if the allocation strategy = min quantity on the pallet
																						 * 			search for the inventory where the inventory has the least quantity
																						 * 		else
																						 * 			search for the inventory where the inventory has the maximum quantity 
																						 * 			on the pallet 
																						 */

																						var str = 'Inventory Item Status. = ' + itemStatus + '<br>';
																						str = str + 'Fulfillment Item Status. = ' + fulfillmentItemStatus + '<br>';	
																						str = str + 'availableQty. = ' + availableQty + '<br>';	
																						str = str + 'remainingQuantity. = ' + remainingQuantity + '<br>';	

																						nlapiLogExecution('DEBUG', 'Item Status details', str);

																						//if(parseFloat(availableQty) > 0 && itemStatus== fulfillmentItemStatus)
																						if(parseFloat(availableQty) > 0 && itemStatus== fulfillmentItemStatus && remainingQuantity>0 )
																						{
																							// allocate to this bin location
																							var actualAllocQty = Math.min(parseFloat(availableQty), parseFloat(remainingQuantity));
																							var newAllocQty = parseFloat(allocQty) + parseFloat(actualAllocQty);
																							//remainingQuantity = parseFloat(remainingQuantity) - parseFloat(actualAllocQty);

																							var expectedQuantity = parseFloat(actualAllocQty);

																							var str = 'actualAllocQty. = ' + actualAllocQty + '<br>';
																							str = str + 'newAllocQty. = ' + newAllocQty + '<br>';	
																							str = str + 'remainingQuantity. = ' + remainingQuantity + '<br>';	
																							str = str + 'pfMaximumPickQuantity. = ' + pfMaximumPickQuantity + '<br>';																				

																							nlapiLogExecution('DEBUG', 'Alloc Qty details', str);

																							var contsize="";								
																							var totalweight = parseFloat((expectedQuantity*uomweight))/parseFloat(uomqty);
																							var totalcube = parseFloat((expectedQuantity*uomcube))/parseFloat(uomqty);

																							// Allocate the quantity to the pickface location if the allocation quantity is 
																							// 	less than or equal to the pick face maximum quantity
																							if(actualAllocQty <= pfMaximumPickQuantity)
																							{
																								var str = 'uomlevel. = ' + uomlevel + '<br>';
																								str = str + 'uompackflag. = ' + uompackflag + '<br>';	
																								str = str + 'uomqty. = ' + uomqty + '<br>';	
																								str = str + 'expectedQuantity. = ' + expectedQuantity + '<br>';	

																								nlapiLogExecution('DEBUG', 'UOM Details', str);

																								//1-Ship Cartons	2-Build Cartons		3-Ship Pallets
																								if((uompackflag=="1" || uompackflag=="3") &&(parseFloat(expectedQuantity) == parseFloat(uomqty)))
																								{	
																									//pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);

																									for(l=0;containerslist!=null && l<containerslist.length;l++){     

																										var containername = containerslist[l].getValue('custrecord_containername');

																										if(containername=='SHIPASIS')
																										{
																											contsize = containerslist[l].getValue('Internalid');
																											pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);

																											//CreateMasterLPRecord(pfcontLP,contsize,totalweight,totalcube,uompackflag,DOwhLocation,LPparent);

																											CreateMasterLPRecordBulk(pfcontLP,contsize,totalweight,totalcube,uompackflag,DOwhLocation,LPparent);

																										}
																									}

																									nlapiLogExecution('DEBUG', 'Container Size in Pick Face : ', contsize);
																								}
																								else if(cartonizationmethod=='4')
																								{
																									if(prvSalesord!=salesOrderNo)
																									{
																										pfcontLP='';
																										//pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																										prvSalesord=salesOrderNo;
																										//CreateMasterLPRecord(pfcontLP,contsize,totalweight,totalcube,uompackflag);
																									}
																								}

																								nlapiLogExecution('DEBUG', 'Container LP in Pick Face : ', pfcontLP);

																								// Update custom records
																								//updateInventoryWithAllocation(recordId, newAllocQty);

																								updateInventoryWithAllocation(recordId, actualAllocQty);

																								createRecordInOpenTask(fulfillOrderList[i], expectedQuantity, eBizWaveNo, pfcontLP, 
																										pfBinLocationId, recordId, packCode, itemStatus, pickMethod, pfpickzoneid, pickStrategyId,
																										whLocation, fromLP,uomlevel,totalweight,totalcube,contsize,fromLot,'',wmsCarrier,fulfilmentItem,
																										nsrefno,expiryDate,customer,dimsbulkpick,otparent,waveassignedto,baseuomid);

																								//updateFulfilmentOrder(fulfilmentOrderNo, fulfilmentOrderLineNo, actualAllocQty, eBizWaveNo);
																								updateFulfilmentOrder(fulfillOrderList[i], actualAllocQty, eBizWaveNo,foparent);

																								remainingQuantity = parseFloat(remainingQuantity) - parseFloat(actualAllocQty);

																								if((itemType=='lotnumberedinventoryitem' || itemType=='lotnumberedassemblyitem' || batchflag=='T')
																										&& (customer!=null && customer!='') && samelot == 'T')
																								{
																									createCustomerLot(fulfillOrderList[i],customer,fulfilmentItem,fromLot,expiryDate,whLocation);
																								}
																								// Maintain the allocation temporarily
																								var allocatedInvRow = [pfBinLocationId, expectedQuantity,fromLP,inventoryItem,fromLot];
																								allocatedInv.push(allocatedInvRow);
																								pickfaceArr.push(pfBinLocationId);
																							}
																						}

																						nlapiLogExecution('DEBUG','remainingQuantity',remainingQuantity);
																						totordqty=totordqty+remainingQuantity;
																						nlapiLogExecution('DEBUG','totordqty',totordqty);
																						nlapiLogExecution('DEBUG','totreplenqty',totreplenqty);
//																						Case # 20127328,20127083 Start
																					}
																				}
																			}
																			//Case # 20127328 ,20127083End
																			if( remainingQuantity > 0 && Autoreplenflag=='T')
																			{
																				if(totordqty==0 || totordqty>totreplenqty)
																				{
																					nlapiLogExecution('DEBUG','availableQty',availableQty);//
																					nlapiLogExecution('DEBUG','remainingQuantity',remainingQuantity);
																					if(availableQty==null || availableQty=='')
																						availableQty=0;
																					var actualAllocQty = Math.min(parseFloat(availableQty), parseFloat(remainingQuantity));
																					var newAllocQty = parseFloat(allocQty) + parseFloat(actualAllocQty);
																					var expectedQuantity = parseFloat(remainingQuantity);

																					if((uompackflag=="1" || uompackflag=="3") &&(parseFloat(expectedQuantity) == parseFloat(uomqty)))
																					{	
																						//pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);

																						for(l=0;containerslist!=null && l<containerslist.length;l++){     

																							var containername = containerslist[l].getValue('custrecord_containername');

																							if(containername=='SHIPASIS')
																							{
																								contsize = containerslist[l].getValue('Internalid');
																								pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);

																								//CreateMasterLPRecord(pfcontLP,contsize,totalweight,totalcube,uompackflag,DOwhLocation,LPparent);

																								CreateMasterLPRecordBulk(pfcontLP,contsize,totalweight,totalcube,uompackflag,DOwhLocation,LPparent);

																							}
																						}

																						nlapiLogExecution('DEBUG', 'Container Size in Pick Face : ', contsize);
																					}
																					else if(cartonizationmethod=='4')
																					{
																						if(prvSalesord!=salesOrderNo)
																						{
																							pfcontLP='';
																							//pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																							prvSalesord=salesOrderNo;
																							//CreateMasterLPRecord(pfcontLP,contsize,totalweight,totalcube,uompackflag);
																						}
																					}
																					/*else
																						{
																							pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																						}*/
																					//var contsize="";								
																					var totalweight = parseFloat((expectedQuantity*uomweight))/parseFloat(uomqty);
																					var totalcube = parseFloat((expectedQuantity*uomcube))/parseFloat(uomqty);
																					var replenItemArray =  new Array();
																					var currentRow = new Array();
																					var j = 0;
																					var idx = 0;
																					var putawatqty=0;
																					var pickqty=0;
																					var pickfaceQty=0;
																					var openreplensqty=0;


																					var serchreplensqty=getopenreplens(salesOrderNo,fulfilmentItem,pfBinLocationId,pfWHLoc);
																					if(serchreplensqty!=null && serchreplensqty!='')
																					{
																						openreplensqty = serchreplensqty[0].getValue('custrecord_expe_qty', null, 'sum');
																					}

																					var searchputawyqty=getOpenputawayqty(pfBinLocationId);
																					if(searchputawyqty!=null && searchputawyqty!='')
																					{
																						putawatqty = searchputawyqty[0].getValue('custrecord_expe_qty', null, 'sum');
																					}

																					var searchpickfaceQty = getQOHForAllSKUsinPFLocation(pfBinLocationId,fulfilmentItem,pfWHLoc,fulfillmentItemStatus);
																					if(searchpickfaceQty!=null && searchpickfaceQty!='')
																					{
																						pickfaceQty=searchpickfaceQty[0].getValue('custrecord_ebiz_qoh',null,'sum');
																					}
																					var replenQty=pfLocationResults[z].getValue('custrecord_replenqty');
																					nlapiLogExecution('DEBUG','putawatqty',putawatqty);
																					nlapiLogExecution('DEBUG','pickqty',pickqty);
																					nlapiLogExecution('DEBUG','pickfaceQty',pickfaceQty);
																					nlapiLogExecution('DEBUG','replenQty',replenQty);
																					nlapiLogExecution('DEBUG','openreplensqty',openreplensqty);
																					nlapiLogExecution('DEBUG','pfRoundQty',pfRoundQty);

																					if(pfRoundQty == null || pfRoundQty == '')
																						pfRoundQty=1;																				
																					if(openreplensqty == null || openreplensqty == '')
																						openreplensqty=0;
																					if(pickqty == null || pickqty == '')
																						pickqty=0;
																					if(putawatqty == null || putawatqty == '')
																						putawatqty=0;
																					if(pickfaceQty == null || pickfaceQty == '')
																						pickfaceQty=0;
																					var tempQty = (parseFloat(pfMaximumQuantity) - (parseFloat(openreplensqty) + parseFloat(pickfaceQty)  - parseFloat(pickqty) + parseFloat(putawatqty)));
																					//var tempQty = parseFloat(pfMaximumQuantity) -  parseFloat(openreplensqty) - parseFloat(pickqty) + parseFloat(putawatqty)+parseFloat(pickfaceQty);

																					nlapiLogExecution('DEBUG','tempQty1',tempQty);

																					var tempwithRoundQty = Math.floor(parseFloat(tempQty)/parseFloat(pfRoundQty));

																					if(tempwithRoundQty == null || tempwithRoundQty == '' || isNaN(tempwithRoundQty))
																						tempwithRoundQty=0;

																					nlapiLogExecution('DEBUG','tempwithRoundQty',tempwithRoundQty);

																					tempQty = parseFloat(pfRoundQty)*parseFloat(tempwithRoundQty);

																					nlapiLogExecution('DEBUG','tempQty2',tempQty);																				

																					var replenTaskCount = Math.ceil(parseFloat(tempQty) / parseFloat(replenQty));
																					nlapiLogExecution('DEBUG','tempQty',tempQty);


																					currentRow = [pfItem, replenQty, replenrule, pfMaximumQuantity, pfMaximumPickQuantity, pickfaceQty, whLocation, 
																					              pfBinLocationId, tempQty, replenTaskCount, pffixedlp,pfstatus,'',pfRoundQty];
																					if(idx == 0)
																						idx = currentRow.length;

																					replenItemArray[j++] = currentRow;
																					var taskCount;
																					var zonesArray = new Array();

																					var replenRulesList = getListForAllReplenZones(whLocation);

																					var replenZoneList = "";
																					var replenZoneTextList = "";
																					var replenzonestatuslist="";
																					var replenzonestatustextlist="";
																					var Reportno = "";

																					if (replenRulesList != null){
																						for (var w = 0; w < replenRulesList.length; w++){

																							if (w == 0){
																								replenZoneList += replenRulesList[w].getValue('custrecord_replen_strategy_zone');
																								replenZoneTextList += replenRulesList[w].getText('custrecord_replen_strategy_zone');
																								replenzonestatuslist +=replenRulesList[w].getValue('custrecord_replen_strategy_itemstatus');
																								replenzonestatustextlist +=replenRulesList[w].getText('custrecord_replen_strategy_itemstatus');
																							} else {
																								replenZoneList += ',';
																								replenZoneList += replenRulesList[w].getValue('custrecord_replen_strategy_zone');

																								replenZoneTextList += ',';
																								replenZoneTextList += replenRulesList[w].getText('custrecord_replen_strategy_zone');

																								replenzonestatuslist += ',';
																								replenzonestatuslist += replenRulesList[w].getValue('custrecord_replen_strategy_itemstatus');

																								replenzonestatustextlist += ',';
																								replenzonestatustextlist += replenRulesList[w].getText('custrecord_replen_strategy_itemstatus');
																							}
																						}
																					} 

																					nlapiLogExecution('DEBUG','replenZoneList',replenZoneList);

																					var itemList = buildItemListForReplenProcessing(replenItemArray, j);
																					nlapiLogExecution('DEBUG','replen Item List',itemList);

																					var locnGroupList = getZoneLocnGroupsList(replenZoneList);	//case# 201416311
																					nlapiLogExecution('DEBUG','Location Group List',locnGroupList);

																					var ZoneAndLocationGrp =getZoneLocnGroupsListforRepln(replenZoneList,whLocation);
																					nlapiLogExecution('DEBUG','ZoneAndLocationGrp',ZoneAndLocationGrp);

																					var inventorySearchResultsReplen = getItemDetailsforRepln(locnGroupList, itemList,replenzonestatuslist,whLocation,lefoflag,fulfillmentItemStatus);
																					nlapiLogExecution('DEBUG','inventorySearchResultsReplen',inventorySearchResultsReplen);
																					if ((inventorySearchResultsReplen != null && inventorySearchResultsReplen != "") || (remainingQuantity<=openreplensqty)){
																						nlapiLogExecution('DEBUG','test2','test2');
																						// Get the report number
																						/*if (Reportno == null || Reportno == "") {
																							Reportno = GetMaxTransactionNo('REPORT');
																							Reportno = Reportno.toString();
																						}*/
																						
																						
																						if (Reportno == null || Reportno == "") {
																							Reportno =  fnGetReplenNo(eBizWaveNo);

																							if (Reportno == null || Reportno == "") {

																								Reportno = GetMaxTransactionNo('REPORT');
																								Reportno = Reportno.toString();
																							}
																						}
																						

																						/*
																						 * Loop through the total task count and get the location from where the item has to be 
																						 * replenished.
																						 */

																						//Get the Stage Location in case of two step replenishment process
																						var stageLocation='';
																						//GetStageLocation(item, "", whLocation, "", "BOTH");
																						var resultArray=""; 

																						if(tempQty>0 && tempQty!='NaN')
																						{
																							resultArray =	replenRuleZoneLocation(inventorySearchResultsReplen, replenItemArray, Reportno, stageLocation, whLocation,fulfilmentOrderNo,eBizWaveNo,null,ZoneAndLocationGrp,expectedQuantity);
																						}

																						var result=resultArray[0];
																						nlapiLogExecution('DEBUG','result',result);

																						if(result==true || (remainingQuantity<=openreplensqty))
																						{
																							//remainingQuantity = parseFloat(remainingQuantity) - parseFloat(actualAllocQty);
																							//updateInventoryWithAllocation(recordId, actualAllocQty);

																							var vREMQtyafterReplnGenerated=resultArray[1];
																							var vActualReplnQtyGenerated=resultArray[2];

																							if(inventorySearchResultsReplen!=null && inventorySearchResultsReplen!='')
																								fromLot=inventorySearchResultsReplen[0].getText('custrecord_ebiz_inv_lot');	

																							nlapiLogExecution('DEBUG','fulfillOrderList',fulfillOrderList[i]);
																							var test=fulfillOrderList[i][6];

																							nlapiLogExecution('DEBUG','test',test);
																							recordId="";
																							fromLP = getfromLP(eBizWaveNo,fulfilmentItem);
																							//Case # 20127728�Start
																							//Case # 20123993 Start.
																							if((fromLP == null || fromLP == '') && (recordId !=null && recordId!='' && recordId!='null' && recordId!='undefined'))
																							{
																								fromLP=getfromLPfromInv(recordId);
																							}
																							else
																							{
																								fromLP=pffixedlp;
																							}
																							//Case # 20127728�End
																							nlapiLogExecution('DEBUG','fromLP1',fromLP);

																							nlapiLogExecution('DEBUG','vActualReplnQtyGenerated',vActualReplnQtyGenerated);
																							nlapiLogExecution('DEBUG','expectedQuantity',expectedQuantity);

																							var actualexpectedQuantity = Math.min(parseFloat(vActualReplnQtyGenerated), parseFloat(expectedQuantity));
																							//Case # 20123993�End.
																							/*	createRecordInOpenTask(fulfillOrderList[i], expectedQuantity, eBizWaveNo, pfcontLP, 
																										pfBinLocationId, recordId, packCode, fulfillmentItemStatus, pickMethod, pfpickzoneid, pickStrategyId,
																										whLocation, fromLP,uomlevel,totalweight,totalcube,contsize,fromLot,'',wmsCarrier,fulfilmentItem,nsrefno,
																										expiryDate,customer,dimsbulkpick,otparent,waveassignedto);*/

																							if(remainingQuantity<=openreplensqty)
																							{
																								actualexpectedQuantity = expectedQuantity;
																							}
																							createRecordInOpenTask(fulfillOrderList[i], actualexpectedQuantity, eBizWaveNo, pfcontLP, 
																									pfBinLocationId, recordId, packCode, fulfillmentItemStatus, pickMethod, pfpickzoneid, pickStrategyId,
																									whLocation, fromLP,uomlevel,totalweight,totalcube,contsize,fromLot,'',wmsCarrier,fulfilmentItem,nsrefno,
																									expiryDate,customer,dimsbulkpick,otparent,waveassignedto,baseuomid);

																							//updateFulfilmentOrder(fulfilmentOrderNo, fulfilmentOrderLineNo, remainingQuantity, eBizWaveNo);
																							//	updateFulfilmentOrder(fulfillOrderList[i], remainingQuantity, eBizWaveNo,foparent);

																							updateFulfilmentOrder(fulfillOrderList[i], actualexpectedQuantity, eBizWaveNo,foparent);

																							if((remainingQuantity>tempQty && tempQty>0) && (remainingQuantity>openreplensqty) )
																							{
																								remainingQuantity=parseFloat(remainingQuantity) - parseFloat(tempQty);//
																							}
																							else
																							{
																								remainingQuantity=vREMQtyafterReplnGenerated;
																								//remainingQuantity=0;
																							}

																							// Maintain the allocation temporarily
																							//var allocatedInvRow = [beginLocationId, expectedQuantity,fromLP,inventoryItem,fromLot];
																							//allocatedInv.push(allocatedInvRow);
																						}
																					}
																					totreplenqty=totreplenqty+replenQty;

																				}
																			}
																			//
																			//Case # 20127328,20127083 Start
																		}
																	}

																}//Case # 20127328,20127083 End
															}
														}



														if(inventorySearchResults != null && inventorySearchResults.length > 0)
														{
															if(vLotNoSplit != 'T' && vLotExcepId != null && vLotExcepId != '')
															{	
																/*nlapiLogExecution('DEBUG', 'vLotExcepId', vLotExcepId);
																var vLotExc=vLotExcepId.split(',');
																nlapiLogExecution('DEBUG', 'vLotExc', vLotExc);
																var vLotExcQt=vLotExcepQty.split(',');
																nlapiLogExecution('DEBUG', 'vLotExcQt', vLotExcQt);
																for(var p=0;p<vLotExc.length;p++)
																{	
																	var vLotRemQty=vLotExcQt[p];*/
																for (var j = 0; j < inventorySearchResults.length; j++)
																{
																	inventoryItem = inventorySearchResults[j].getValue('custrecord_ebiz_inv_sku');
																	var InvWHLocation = inventorySearchResults[j].getValue('custrecord_ebiz_inv_loc');
																	var InvLot = inventorySearchResults[j].getValue('custrecord_ebiz_inv_lot');

																	var str = 'remainingQuantity. = ' + remainingQuantity + '<br>';
																	str = str + 'inventoryItem. = ' + inventoryItem + '<br>';	
																	str = str + 'fulfilmentItem. = ' + fulfilmentItem + '<br>';
																	str = str + 'vLotRemQty. = ' + vLotRemQty + '<br>';
																	str = str + 'vSplitLot. = ' + vSplitLot + '<br>';

																	nlapiLogExecution('DEBUG', 'Remaining Qty & Item', str);
																	var InvLot = inventorySearchResults[j].getValue('custrecord_ebiz_inv_lot');
																	if ((DOwhLocation==InvWHLocation)&&(inventoryItem.toString() == fulfilmentItem.toString()) && (parseFloat(remainingQuantity)>0)  && vSplitLot==InvLot)
																	{
																		actualQty = inventorySearchResults[j].getValue('custrecord_ebiz_qoh');
																		inventoryAvailableQuantity = inventorySearchResults[j].getValue('custrecord_ebiz_avl_qty');
																		allocationQuantity = inventorySearchResults[j].getValue('custrecord_ebiz_alloc_qty');
																		recordId = inventorySearchResults[j].getId();
																		beginLocationId = inventorySearchResults[j].getValue('custrecord_ebiz_inv_binloc');
																		if(inventorySearchResults[j].getValue('custrecord_ebiz_inv_packcode')!=null && inventorySearchResults[j].getValue('custrecord_ebiz_inv_packcode')!='')
																			packCode = inventorySearchResults[j].getValue('custrecord_ebiz_inv_packcode');
																		itemStatus = inventorySearchResults[j].getValue('custrecord_ebiz_inv_sku_status');
																		whLocation = inventorySearchResults[j].getValue('custrecord_ebiz_inv_loc');
																		fromLP = inventorySearchResults[j].getValue('custrecord_ebiz_inv_lp');
																		fromLot = inventorySearchResults[j].getText('custrecord_ebiz_inv_lot');
																		expiryDate = inventorySearchResults[j].getValue('custrecord_ebiz_expdate');
																		// Get already allocated inv
																		var alreadyAllocQty = getAlreadyAllocatedInv(allocatedInv, beginLocationId,fromLP,inventoryItem,fromLot);

																		// Adjust for already allocated inv
																		//inventoryAvailableQuantity = parseFloat(inventoryAvailableQuantity) - parseFloat(alreadyAllocQty);

																		var str = 'Inventory Item Status. = ' + itemStatus + '<br>';
																		str = str + 'Fulfillment Item Status. = ' + fulfillmentItemStatus + '<br>';	
																		str = str + 'already AllocQty. = ' + alreadyAllocQty + '<br>';	
																		str = str + 'inventory Available Quantity. = ' + inventoryAvailableQuantity + '<br>';	
																		str = str + 'remaining Quantity. = ' + remainingQuantity + '<br>';
																		//str = str + 'vLotRemQty. = ' + vLotRemQty + '<br>';

																		nlapiLogExecution('DEBUG', 'Inventory Details', str);
																		var vPickfaceAllocLoc=false;
																		if(pickfaceArr != null && pickfaceArr != '')
																		{
																			nlapiLogExecution('DEBUG', 'pickfaceArr1', pickfaceArr);
																			nlapiLogExecution('DEBUG', 'beginLocationId', beginLocationId);
																			if(pickfaceArr.indexOf(beginLocationId)!=-1)
																				vPickfaceAllocLoc=true;
																		}	
																		if(vPickfaceAllocLoc == false)
																		{
																			if((parseFloat(inventoryAvailableQuantity) > 0) && (itemStatus==fulfillmentItemStatus) 
																					&& (beginLocationId != pfBinLocationId)){
																				var actualAllocationQuantity = Math.min(parseFloat(inventoryAvailableQuantity), parseFloat(remainingQuantity));
																				allocationQuantity = parseFloat(alreadyAllocQty) + parseFloat(actualAllocationQuantity);
																				remainingQuantity = parseFloat(remainingQuantity) - parseFloat(actualAllocationQuantity);
																				//vLotRemQty = parseFloat(remainingQuantity) - parseFloat(actualAllocationQuantity);
																				var expectedQuantity = parseFloat(actualAllocationQuantity);

																				var contsize="";	

																				var totalweight = parseFloat((expectedQuantity*uomweight))/parseFloat(uomqty);
																				var totalcube = parseFloat((expectedQuantity*uomcube))/parseFloat(uomqty);

																				//var totalweight = expectedQuantity*uomweight;
																				//var totalcube = expectedQuantity*uomcube;

																				var str = 'uompackflag. = ' + uompackflag + '<br>';
																				str = str + 'uomqty. = ' + uomqty + '<br>';	
																				str = str + 'expectedQuantity. = ' + expectedQuantity + '<br>';	
																				str = str + 'allocationQuantity. = ' + allocationQuantity + '<br>';	
																				str = str + 'cartonizationmethod. = ' + cartonizationmethod + '<br>';	
																				str = str + 'remaining Quantity. = ' + remainingQuantity + '<br>';	

																				nlapiLogExecution('DEBUG', 'Qty Details', str);

																				//1-Ship Cartons	2-Build Cartons		3-Ship Pallets
																				if((uompackflag=="1" || uompackflag=="3")&&(parseFloat(expectedQuantity) == parseFloat(uomqty))) 
																				{
																					//invtcontLP = GetMaxLPNo('1', '2',DOwhLocation);

																					for(l=0;containerslist!=null && l<containerslist.length;l++)
																					{                                         
																						var containername = containerslist[l].getValue('custrecord_containername');
																						//nlapiLogExecution('DEBUG', 'containername', containername);
																						if(containername=='SHIPASIS')
																						{
																							invtcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																							contsize = containerslist[l].getValue('Internalid');	
																							CreateMasterLPRecordBulk(invtcontLP,contsize,totalweight,totalcube,uompackflag,DOwhLocation,LPparent);
																						}
																					}
																				}
																				else if(cartonizationmethod=='4') 
																				{
																					if(prvSalesord!=salesOrderNo)
																					{
																						invtcontLP='';
																						//invtcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																						prvSalesord=salesOrderNo;
																						//CreateMasterLPRecord(invtcontLP,contsize,totalweight,totalcube,uompackflag);
																					}
																				}
																				else
																				{
																					invtcontLP='';
																				}

																				nlapiLogExecution('DEBUG', 'Container LP in Inventory : ', invtcontLP);

																				//updateInventoryWithAllocation(recordId, allocationQuantity);
																				updateInventoryWithAllocation(recordId, actualAllocationQuantity);
																				nlapiLogExecution('DEBUG', 'Before updating in open task : ', expiryDate);
																				createRecordInOpenTask(fulfillOrderList[i], expectedQuantity, eBizWaveNo, invtcontLP, beginLocationId, recordId,
																						packCode, itemStatus, pickMethod, pickZone, pickStrategyId, whLocation, fromLP,uomlevel,
																						totalweight,totalcube,contsize,fromLot,'',wmsCarrier,fulfilmentItem,nsrefno,
																						expiryDate,customer,dimsbulkpick,otparent,waveassignedto,baseuomid);

																				//updateFulfilmentOrder(fulfilmentOrderNo, fulfilmentOrderLineNo, actualAllocationQuantity, eBizWaveNo);
																				updateFulfilmentOrder(fulfillOrderList[i], actualAllocationQuantity, eBizWaveNo,foparent);

																				// Maintain the allocation temporarily
																				var allocatedInvRow = [beginLocationId, expectedQuantity,fromLP,inventoryItem,fromLot];
																				allocatedInv.push(allocatedInvRow);

																			}
																		}
																	}
																}
																//}
															}
															else
															{
																for (var j = 0; j < inventorySearchResults.length; j++)
																{
																	inventoryItem = inventorySearchResults[j].getValue('custrecord_ebiz_inv_sku');
																	var InvWHLocation = inventorySearchResults[j].getValue('custrecord_ebiz_inv_loc');

																	var str = 'remainingQuantity. = ' + remainingQuantity + '<br>';
																	str = str + 'inventoryItem. = ' + inventoryItem + '<br>';	
																	str = str + 'fulfilmentItem. = ' + fulfilmentItem + '<br>';

																	nlapiLogExecution('DEBUG', 'Remaining Qty & Item', str);

																	if ((DOwhLocation==InvWHLocation)&&(inventoryItem.toString() == fulfilmentItem.toString()) && (parseFloat(remainingQuantity) > 0))
																	{
																		actualQty = inventorySearchResults[j].getValue('custrecord_ebiz_qoh');
																		inventoryAvailableQuantity = inventorySearchResults[j].getValue('custrecord_ebiz_avl_qty');
																		allocationQuantity = inventorySearchResults[j].getValue('custrecord_ebiz_alloc_qty');
																		recordId = inventorySearchResults[j].getId();
																		beginLocationId = inventorySearchResults[j].getValue('custrecord_ebiz_inv_binloc');
																		if(inventorySearchResults[j].getValue('custrecord_ebiz_inv_packcode')!=null && inventorySearchResults[j].getValue('custrecord_ebiz_inv_packcode')!='')
																			packCode = inventorySearchResults[j].getValue('custrecord_ebiz_inv_packcode');
																		itemStatus = inventorySearchResults[j].getValue('custrecord_ebiz_inv_sku_status');
																		whLocation = inventorySearchResults[j].getValue('custrecord_ebiz_inv_loc');
																		fromLP = inventorySearchResults[j].getValue('custrecord_ebiz_inv_lp');
																		fromLot = inventorySearchResults[j].getText('custrecord_ebiz_inv_lot');
																		expiryDate = inventorySearchResults[j].getValue('custrecord_ebiz_expdate');
																		var isValidRec = validateShelflife(samelot,latestexpirydt,shelflife,expiryDate);
																		if(isValidRec!='F')
																		{
																			// Get already allocated inv
																			var alreadyAllocQty = getAlreadyAllocatedInv(allocatedInv, beginLocationId,fromLP,inventoryItem,fromLot);

																			// Adjust for already allocated inv
																			//inventoryAvailableQuantity = parseFloat(inventoryAvailableQuantity) - parseFloat(alreadyAllocQty);

																			var str = 'Inventory Item Status. = ' + itemStatus + '<br>';
																			str = str + 'Fulfillment Item Status. = ' + fulfillmentItemStatus + '<br>';	
																			str = str + 'already AllocQty. = ' + alreadyAllocQty + '<br>';	
																			str = str + 'inventory Available Quantity. = ' + inventoryAvailableQuantity + '<br>';	
																			str = str + 'remaining Quantity. = ' + remainingQuantity + '<br>';	

																			nlapiLogExecution('DEBUG', 'Inventory Details', str);
																			var vPickfaceAllocLoc=false;
																			if(pickfaceArr != null && pickfaceArr != '')
																			{
																				nlapiLogExecution('DEBUG', 'pickfaceArr1', pickfaceArr);
																				nlapiLogExecution('DEBUG', 'beginLocationId', beginLocationId);
																				if(pickfaceArr.indexOf(beginLocationId)!=-1)
																					vPickfaceAllocLoc=true;
																			}	
																			if(vPickfaceAllocLoc == false)
																			{
																				if(parseFloat(inventoryAvailableQuantity) > 0 && itemStatus==fulfillmentItemStatus && (beginLocationId != pfBinLocationId)){
																					var actualAllocationQuantity = Math.min(parseFloat(inventoryAvailableQuantity), parseFloat(remainingQuantity));
																					allocationQuantity = parseFloat(alreadyAllocQty) + parseFloat(actualAllocationQuantity);
																					remainingQuantity = parseFloat(remainingQuantity) - parseFloat(actualAllocationQuantity);
																					var expectedQuantity = parseFloat(actualAllocationQuantity);

																					var contsize="";	

																					var totalweight = parseFloat((expectedQuantity*uomweight))/parseFloat(uomqty);
																					var totalcube = parseFloat((expectedQuantity*uomcube))/parseFloat(uomqty);

																					//var totalweight = expectedQuantity*uomweight;
																					//var totalcube = expectedQuantity*uomcube;

																					var str = 'uompackflag. = ' + uompackflag + '<br>';
																					str = str + 'uomqty. = ' + uomqty + '<br>';	
																					str = str + 'expectedQuantity. = ' + expectedQuantity + '<br>';	
																					str = str + 'allocationQuantity. = ' + allocationQuantity + '<br>';	
																					str = str + 'cartonizationmethod. = ' + cartonizationmethod + '<br>';	
																					str = str + 'remaining Quantity. = ' + remainingQuantity + '<br>';	

																					nlapiLogExecution('DEBUG', 'Qty Details', str);

																					//1-Ship Cartons	2-Build Cartons		3-Ship Pallets
																					if((uompackflag=="1" || uompackflag=="3")&&(parseFloat(expectedQuantity) == parseFloat(uomqty))) 
																					{
																						//invtcontLP = GetMaxLPNo('1', '2',DOwhLocation);

																						for(l=0;containerslist!=null && l<containerslist.length;l++)
																						{                                         
																							var containername = containerslist[l].getValue('custrecord_containername');
																							//nlapiLogExecution('DEBUG', 'containername', containername);
																							if(containername=='SHIPASIS')
																							{
																								invtcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																								contsize = containerslist[l].getValue('Internalid');	
																								CreateMasterLPRecordBulk(invtcontLP,contsize,totalweight,totalcube,uompackflag,DOwhLocation,LPparent);
																							}
																						}									

																					}
																					else if(cartonizationmethod=='4') 
																					{
																						if(prvSalesord!=salesOrderNo)
																						{
																							invtcontLP='';
																							//invtcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																							prvSalesord=salesOrderNo;
																							//CreateMasterLPRecord(invtcontLP,contsize,totalweight,totalcube,uompackflag);
																						}
																					}
																					else
																					{
																						invtcontLP='';
																					}

																					nlapiLogExecution('DEBUG', 'Container LP in Inventory@ : ', invtcontLP);

																					//updateInventoryWithAllocation(recordId, allocationQuantity);
																					updateInventoryWithAllocation(recordId, actualAllocationQuantity);

																					createRecordInOpenTask(fulfillOrderList[i], expectedQuantity, eBizWaveNo, invtcontLP, beginLocationId, recordId,
																							packCode, itemStatus, pickMethod, pickZone, pickStrategyId, whLocation, fromLP,uomlevel,
																							totalweight,totalcube,contsize,fromLot,'',wmsCarrier,fulfilmentItem,nsrefno,expiryDate,
																							customer,dimsbulkpick,otparent,waveassignedto,baseuomid);

																					//updateFulfilmentOrder(fulfilmentOrderNo, fulfilmentOrderLineNo, actualAllocationQuantity, eBizWaveNo);
																					updateFulfilmentOrder(fulfillOrderList[i], actualAllocationQuantity, eBizWaveNo,foparent);

																					if((itemType=='lotnumberedinventoryitem' || itemType=='lotnumberedassemblyitem' || batchflag=='T')
																							&& (customer!=null && customer!=''))
																					{
																						createCustomerLot(fulfillOrderList[i],customer,fulfilmentItem,fromLot,expiryDate,whLocation);
																					}
																					// Maintain the allocation temporarily
																					var allocatedInvRow = [beginLocationId, expectedQuantity,fromLP,inventoryItem,fromLot];
																					allocatedInv.push(allocatedInvRow);
																				}

																			}
																		}
																	}
																}
															}
														}
													}
												}
											}

											//If there is still some quantity remaining, then insert a failed task
											if(parseFloat(nooflps)>0 && parseFloat(remainingQuantity) > 0)
											{
												vnotes='Insufficient Inventory';
												nlapiLogExecution('DEBUG', 'Creating Failed Task');
												nlapiLogExecution('DEBUG', 'remainingQuantity',remainingQuantity);
												createRecordInOpenTask(fulfillOrderList[i], remainingQuantity, eBizWaveNo, "", "", "",
														"", "", "", "", "", DOwhLocation, "",uomlevel,"","","","",vnotes,"",fulfilmentItem,nsrefno,
														"",customer,dimsbulkpick,otparent,waveassignedto,baseuomid);

												//updateFulfilmentOrder(fulfilmentOrderNo, fulfilmentOrderLineNo, 0, eBizWaveNo);
												updateFulfilmentOrder(fulfillOrderList[i], 0, eBizWaveNo,foparent);
												nlapiLogExecution('DEBUG', 'Creating Failed Task','SUCCESS');
												vBoolFailedForSameLP=true;
												remainingQuantity=0;
											}
										}
									}
								}
							}
						}

					}
					//}
					//}
				}

			}
			nlapiLogExecution('DEBUG', 'itemType : ', itemType);
			nlapiLogExecution('DEBUG', 'KitFailed6 : ', KitFailed);
			if(itemType=='kititem' && KitFailed==true)
			{
				nlapiLogExecution('DEBUG', 'vFOFailedArr : ', vFOFailedArr);
				nlapiLogExecution('DEBUG', 'vInvFailedArr : ', vInvFailedArr);
				nlapiLogExecution('DEBUG', 'vFulfillArr : ', vFulfillArr);
				nlapiLogExecution('DEBUG', 'KitFailed',KitFailed);
				if(vFOFailedArr != null && vFOFailedArr != '' && vFOFailedArr.length>0)
				{
					for(var p=0;p<vFOFailedArr.length;p++)
					{	
						updateFulfilmentOrderBackForFailedComps(vFOFailedArr[p][0],vFOFailedArr[p][1],vFOFailedArr[p][2],vFOFailedArr[p][3]);
					}
				}
				if(vInvFailedArr != null && vInvFailedArr != '' && vInvFailedArr.length>0)
				{
					for(var q=0;q<vInvFailedArr.length;q++)
					{	
						nlapiLogExecution('DEBUG', 'Deallocate the Failed Inv',vInvFailedArr[q][0]);
						CancelInventoryAllocation(vInvFailedArr[q][0],vInvFailedArr[q][1]);
					}
				}
			}	
			if(vFulfillArr != null && vFulfillArr != '' && vFulfillArr.length>0)
				updateFulfilmentOrderBulk(vFulfillArr,foparent);
			nlapiSubmitRecord(otparent); //submit the parent record, all child records will also be updated
			nlapiSubmitRecord(foparent); //submit the parent record, all child records will also be updated
			if(itemType=='kititem' && KitFailed==true && vOTFailedArr != null && vOTFailedArr != '')
			{
				nlapiLogExecution('DEBUG', 'in Deallocate Opentask KitFailed',KitFailed);
				for(var r=0;r<vOTFailedArr.length;r++)
				{	
					fnDeallocateOpentask(vOTFailedArr[r][0],vOTFailedArr[r][1],vOTFailedArr[r][2],vOTFailedArr[r][3],vOTFailedArr[r][4]);
				}

			}	

		}
	}
	catch(exp) {
		nlapiLogExecution('DEBUG', 'Exception in allocateQuantity : ', exp);		
	}

	nlapiLogExecution('DEBUG', 'out of allocateQuantity ', eBizWaveNo);
}

function getSKUDimCount(orderQty, dimQty){

	var str = 'order qty. = ' + orderQty + '<br>';
	str = str + 'dimensions qty. = ' + dimQty + '<br>';	
	nlapiLogExecution('DEBUG', 'into getSKUDimCount', str);

	var retSKUDimCount = 0;

	if(dimQty > 0 && (parseFloat(orderQty) >= parseFloat(dimQty)))
	{
		skuDimCount = parseFloat(orderQty)/parseFloat(dimQty);
		nlapiLogExecution('DEBUG', 'skuDimCount', skuDimCount);
		retSKUDimCount = Math.floor(skuDimCount);
		if(retSKUDimCount == 0)
			retSKUDimCount = 1;
	}

	nlapiLogExecution('DEBUG', 'out of getSKUDimCount', retSKUDimCount);

	return retSKUDimCount;
}

function getAlreadyAllocatedInv(allocatedInv, binLocation,fromLP,item,lot){
	nlapiLogExecution('DEBUG', 'Into getAlreadyAllocatedInv',allocatedInv);
	var alreadyAllocQty = 0;
	if(allocatedInv != null && allocatedInv.length > 0){
		for(var i = 0; i < allocatedInv.length; i++){
//			nlapiLogExecution('DEBUG', 'fromLP',fromLP);
//			nlapiLogExecution('DEBUG', 'allocatedInv[i][2]',allocatedInv[i][2]);
//			nlapiLogExecution('DEBUG', 'binLocation',binLocation);
//			nlapiLogExecution('DEBUG', 'allocatedInv[i][0]',allocatedInv[i][0]);
//			nlapiLogExecution('DEBUG', 'item',item);
//			nlapiLogExecution('DEBUG', 'allocatedInv[i][3]',allocatedInv[i][3]);
//			nlapiLogExecution('DEBUG', 'lot',lot);
//			nlapiLogExecution('DEBUG', 'allocatedInv[i][4]',allocatedInv[i][4]);

			if(fromLP == allocatedInv[i][2] && binLocation==allocatedInv[i][0] && item==allocatedInv[i][3] && lot==allocatedInv[i][4])
				alreadyAllocQty = parseFloat(alreadyAllocQty) + parseFloat(allocatedInv[i][1]);
		}
	}
	nlapiLogExecution('DEBUG', 'Out of getAlreadyAllocatedInv');
	return alreadyAllocQty;
}

function getAvailQtyFromInventory(inventorySearchResults, binLocation, allocatedInv,dowhlocation,fulfilmentItem){

	nlapiLogExecution('DEBUG','Into getAvailQtyFromInventory');
	var binLocnInvDtls = new Array();
	var matchFound = false;

	nlapiLogExecution('DEBUG','inventorySearchResults',inventorySearchResults);
	if(inventorySearchResults != null && inventorySearchResults.length > 0){
		nlapiLogExecution('DEBUG','inventorySearchResults length',inventorySearchResults.length);
		for(var i = 0; i < inventorySearchResults.length; i++){
			//if(!matchFound)
			{
				invBinLocnId = inventorySearchResults[i].getValue('custrecord_ebiz_inv_binloc');
				var InvWHLocation = inventorySearchResults[i].getValue('custrecord_ebiz_inv_loc');
				var invItem=inventorySearchResults[i].getValue('custrecord_ebiz_inv_sku');

				var str = 'dowhlocation. = ' + dowhlocation + '<br>';
				str = str + 'InvWHLocation. = ' + InvWHLocation + '<br>';	
				str = str + 'invBinLocnId. = ' + invBinLocnId + '<br>';
				str = str + 'binLocation. = ' + binLocation + '<br>';
				str = str + 'fulfilmentItem. = ' + fulfilmentItem + '<br>';
				str = str + 'invItem. = ' + invItem + '<br>';

				nlapiLogExecution('DEBUG', 'Inventory & FO Details', str);

				if((dowhlocation==InvWHLocation)&&(parseFloat(invBinLocnId) == parseFloat(binLocation)) && fulfilmentItem==invItem){

					var actualQty = inventorySearchResults[i].getValue('custrecord_ebiz_qoh');
					var inventoryAvailableQuantity = inventorySearchResults[i].getValue('custrecord_ebiz_avl_qty');
					var allocationQuantity = inventorySearchResults[i].getValue('custrecord_ebiz_alloc_qty');
					var packCode = inventorySearchResults[i].getValue('custrecord_ebiz_inv_packcode');
					var itemStatus = inventorySearchResults[i].getValue('custrecord_ebiz_inv_sku_status');
					var whLocation = inventorySearchResults[i].getValue('custrecord_ebiz_inv_loc');
					var fromLP = inventorySearchResults[i].getValue('custrecord_ebiz_inv_lp');
					var inventoryItem=inventorySearchResults[i].getValue('custrecord_ebiz_inv_sku');
					var fromLot = inventorySearchResults[i].getText('custrecord_ebiz_inv_lot');
					var expiryDate = inventorySearchResults[i].getValue('custrecord_ebiz_expdate');
					var str = 'Invt QOH. = ' + actualQty + '<br>';
					str = str + 'Alloc Qty. = ' + allocationQuantity + '<br>';	
					str = str + 'Avail Qty. = ' + inventoryAvailableQuantity + '<br>';
					str = str + 'packCode. = ' + packCode + '<br>';
					str = str + 'itemStatus. = ' + itemStatus + '<br>';
					str = str + 'whLocation. = ' + whLocation + '<br>';
					str = str + 'fromLP. = ' + fromLP + '<br>';
					str = str + 'inventoryItem. = ' + inventoryItem + '<br>';
					str = str + 'fromLot. = ' + fromLot + '<br>';

					nlapiLogExecution('DEBUG', 'Inventory Details', str);

					var recordId = inventorySearchResults[i].getId();

					// Get allocation already done.
					//Commenting this line because of we are updating directly into inventory. not using bulk updates
					//var alreadyAllocQty = getAlreadyAllocatedInv(allocatedInv, binLocation,fromLP,inventoryItem,fromLot);
					var alreadyAllocQty = 0;
					nlapiLogExecution('DEBUG', 'alreadyAllocQty', alreadyAllocQty);

					// Adjusting for allocations already done
					//The below line is commneted by Satish.N on 11MAR2014
					//inventoryAvailableQuantity = parseInt(inventoryAvailableQuantity) - parseInt(alreadyAllocQty);

					nlapiLogExecution('DEBUG', 'inventoryAvailableQuantity', inventoryAvailableQuantity);

					var currentRow = [i, actualQty, inventoryAvailableQuantity, allocationQuantity, packCode,
					                  itemStatus, whLocation, fromLP, recordId,inventoryItem,fromLot,expiryDate];

					binLocnInvDtls.push(currentRow);

					matchFound = true;
				}
			}
		}
	}

	nlapiLogExecution('DEBUG','Out of getAvailQtyFromInventory');
	return binLocnInvDtls;
}
function getAllocConfigForOrder(allocConfigDtls, index){
	var allocConfig = new Array();
	var matchFound = false;

	if(allocConfigDtls != null && allocConfigDtls.length > 0){
		for (var i = 0; i < allocConfigDtls.length; i++){
			if(!matchFound){
				if(parseFloat(allocConfigDtls[i][0]) == parseFloat(index)){
					allocConfig = allocConfigDtls[i];
					matchFound = true;
				}
			}
		}
	}

	return allocConfig;
}

/**********************************************
 * This function splits the line
 * based on Item Dimensions for each items
 * sublist in a Sales Order.
 *
 */
function pickgen_palletisation(itemId,ordqty,vLotNo,DOwhLocation)
{
	nlapiLogExecution('DEBUG', 'into pickgen_palletisation', itemId);
	nlapiLogExecution('DEBUG', 'SKU', itemId);
	nlapiLogExecution('DEBUG', 'QTY', ordqty);
	nlapiLogExecution('DEBUG', 'vLotNo', vLotNo);
	nlapiLogExecution('DEBUG', 'DOwhLocation', DOwhLocation);

	try {
		var skulist=new Array();
		skulist.push(itemId);
		var arrLPRequired=new Array();			
		var AllItemDims = getItemDimensions(skulist,DOwhLocation);
		var eachQty = 0;
		var caseQty = 0;
		var palletQty = 0;
		var InnerPackQty = 0;
		var noofPallets = 0;
		var noofCases = 0;
		var noofInnerpack = 0;
		var remQty = 0;
		var tOrdQty = ordqty;
		var eachpackflag="";
		var casepackflag="";
		var palletpackflag="";
		var InnerPackflag="";
		var eachweight="";
		var caseweight="";
		var palletweight="";
		var InnerPackweight="";
		var eachcube="";
		var casecube="";
		var palletcube="";
		var InnerPackcube="";
		var BaseUOMLevelMarked=new Array();
		var eachUomlevel="";
		var caseUomlevel="";
		var palletUomlevel="";
		var innerpackUomlevel="";

		// case# 201412157
		var eachUom="";
		var caseUom="";
		var palletUom="";
		var innerpackUom="";
		var baseuomid='';
		//AllItemDims = [sku, uom, uomLevel, qty, baseUOMFlag, packflag,weight,cube];

		if (AllItemDims != null && AllItemDims.length > 0) 
		{	
			nlapiLogExecution('DEBUG', 'AllItemDims.length', AllItemDims.length);
			for(var p = 0; p < AllItemDims.length; p++)
			{
				//var itemval = AllItemDims[p].getValue('custrecord_ebizitemdims');
				//var itemtext = AllItemDims[p].getText('custrecord_ebizitemdims');

				var itemval = AllItemDims[p][0];

				if(AllItemDims[p][0] == itemId)
				{
					nlapiLogExecution('DEBUG', 'Item Dims configured for SKU: ', itemId);

					var skuDim = AllItemDims[p][2];
					var skuQty = AllItemDims[p][3];	
					var packflag = AllItemDims[p][5];	
					var weight = AllItemDims[p][6];
					var cube = AllItemDims[p][7];
					var uom=AllItemDims[p][1];
					var uomflag=AllItemDims[p][4];
					var Uomlevel=AllItemDims[p][8];
					var vbulkpick=AllItemDims[p][9];
//					nlapiLogExecution('DEBUG', 'UOM Level : ', skuDim);
					BaseUOMLevelMarked.push(uomflag);
//					nlapiLogExecution('DEBUG', 'UOM Qty : ', skuQty);
//					nlapiLogExecution('DEBUG', 'UOM Pack Flag : ', packflag);
//					nlapiLogExecution('DEBUG', 'Weight : ', weight);
//					nlapiLogExecution('DEBUG', 'Cube : ', cube);
					var baseuomflag = AllItemDims[p][4];
					if(baseuomflag=='T')
					{
						baseuomid=uom;
					}
					//UOM LEVEL(Internal ID)  : 1-EACH		2-CASE		3-PALLET	4-InnerPack

					if(skuDim == '1'){
						eachQty = skuQty;
						eachpackflag=packflag;
						eachweight = weight;
						eachcube = cube;
						eachUomlevel=Uomlevel;
						eachUom=uom;
					}								
					else if(skuDim == '2'){
						caseQty = skuQty;
						casepackflag=packflag;
						caseweight = weight;
						casecube=cube;
						caseUomlevel=Uomlevel;
						caseUom= uom;
					}								
					else if(skuDim == '3'){
						palletQty = skuQty;
						palletpackflag = packflag;
						palletweight=weight;
						palletcube=cube;
						palletUomlevel=Uomlevel;
						palletUom=uom;
					}	
					else if(skuDim == '4'){
						InnerPackQty = skuQty;
						InnerPackflag = packflag;
						InnerPackweight=weight;
						InnerPackcube=cube;
						innerpackUomlevel=Uomlevel;
						innerpackUom=uom;
					}
				}	
			}

			nlapiLogExecution("ERROR","BaseUOMLevelMarked",BaseUOMLevelMarked);
			if(BaseUOMLevelMarked.indexOf("T")==-1)
			{
				eachQty=0;
				caseQty=0; 
				palletQty=0;
			}



			if(parseFloat(eachQty) == 0)
			{
				nlapiLogExecution('DEBUG', 'UOM Dims', 'Item dim EACH is not configured for SKU: '+ itemId);
			}
			if(parseFloat(caseQty) == 0)
			{
				nlapiLogExecution('DEBUG', 'UOM Dims', 'Item dim CASE is Not Configured for SKU: '+ itemId);
			}
			if(parseFloat(palletQty) == 0)
			{
				nlapiLogExecution('DEBUG', 'UOM Dims', 'Item dim PALLET is not Configured for SKU: '+ itemId);
			}

			//Packflag : 1-Ship Cartons		2-Build Cartons		3-Ship Pallets

			if(eachQty>0 ||caseQty>0 || palletQty>0) 
			{
				var noofLpsRequired = 0;

				// Get the number of pallets required for this item (there will be one LP per pallet)
				noofPallets = getSKUDimCount(ordqty, palletQty);
				remQty = parseFloat(ordqty) - (noofPallets * palletQty);				
				nlapiLogExecution('DEBUG', 'noofPallets', noofPallets);
				nlapiLogExecution('DEBUG', 'palletpackflag', palletpackflag);
				var skuPltRecord=new Array();
				if(noofPallets > 0){
					if(palletpackflag==1 || palletpackflag==3)
					{
						noofLpsRequired = parseFloat(noofPallets);
						skuPltRecord = [Math.round(noofPallets), palletUomlevel, palletQty, palletpackflag,palletweight,palletcube,palletQty,vLotNo,vbulkpick,palletUom,baseuomid];					
					}
					else
					{
						var palletTaskQty=Math.round(noofPallets * palletQty);
						noofPallets=1;
						noofLpsRequired = parseFloat(noofPallets);
						skuPltRecord = [Math.round(noofPallets), palletUomlevel, palletTaskQty, palletpackflag,palletweight,palletcube,palletQty,vLotNo,vbulkpick,palletUom,baseuomid];
					}
				}
				else{						
					skuPltRecord = [Math.round(noofPallets), palletUomlevel, palletQty, palletpackflag,palletweight,palletcube,palletQty,vLotNo,vbulkpick,palletUom,baseuomid];
				}
				arrLPRequired[0] = skuPltRecord ;

				nlapiLogExecution('DEBUG', 'pickgen_palletisation:No. of Pallets', noofPallets);
				nlapiLogExecution('DEBUG', 'pickgen_palletisation:Remaining Quantity', remQty);


				// check whether we need to break down into cases or not
				if (parseFloat(remQty) > 0 && parseFloat(caseQty) != 0) 
				{
					// Get the number of cases required for this item (only one LP for all cases)
					noofCases = getSKUDimCount(remQty, caseQty);
					remQty = parseFloat(remQty) - (noofCases * caseQty);

					var skuCaseRecord=new Array();

					if(noofCases > 0){
						noofLpsRequired = parseFloat(noofLpsRequired) + 1;

						if(casepackflag==1 || casepackflag==3)
						{
							skuCaseRecord = [Math.round(noofCases), caseUomlevel, caseQty, casepackflag,caseweight,casecube,caseQty,vLotNo,vbulkpick,caseUom,baseuomid];							
						}
						else{
							var caseTaskQty=Math.round(noofCases * caseQty);
							noofCases=1;
							skuCaseRecord = [Math.round(noofCases), caseUomlevel, caseTaskQty, casepackflag,caseweight,casecube,caseQty,vLotNo,vbulkpick,caseUom,baseuomid];
						}
					}
					else{						
						skuCaseRecord = [Math.round(noofCases), caseUomlevel, caseQty, casepackflag,caseweight,casecube,caseQty,vLotNo,vbulkpick,caseUom,baseuomid];
					}

					arrLPRequired[1] = skuCaseRecord ;

					nlapiLogExecution('DEBUG', 'pickgen_palletisation:No. of Cases', noofCases);
					nlapiLogExecution('DEBUG', 'pickgen_palletisation:Remaining Quantity', remQty);
				}
				else{
					var skuCaseRecord = [Math.round(0), caseUomlevel, caseQty, casepackflag,caseweight,casecube,caseQty,vLotNo,vbulkpick,caseUom,baseuomid];
					arrLPRequired[1] = skuCaseRecord ;					
				}

				// check whether we need to break down into InnerPack or not
				if (parseFloat(remQty) > 0 && parseFloat(InnerPackQty) != 0) 
				{
					// Get the number of innerpack required for this item (only one LP for all innerpack)
					noofInnerpack = getSKUDimCount(remQty, InnerPackQty);
					remQty = parseFloat(remQty) - (noofInnerpack * InnerPackQty);

					var skuInnerpackRecord=new Array();

					if(noofInnerpack > 0){
						noofLpsRequired = parseFloat(noofLpsRequired) + 1;

						if(InnerPackflag==1 || InnerPackflag==3)
						{
							skuInnerpackRecord = [Math.round(noofInnerpack), innerpackUomlevel, InnerPackQty, InnerPackflag ,InnerPackweight,InnerPackcube,InnerPackQty,vLotNo,vbulkpick,innerpackUom,baseuomid];							
						}
						else{
							var InnerpackTaskQty=Math.round(noofInnerpack * InnerPackQty);
							noofInnerpack=1;
							skuInnerpackRecord = [Math.round(noofInnerpack), innerpackUomlevel, InnerpackTaskQty, InnerPackflag ,InnerPackweight,InnerPackcube,InnerPackQty,vLotNo,vbulkpick,innerpackUom,baseuomid];	
						}
					}
					else{						
						skuInnerpackRecord = [Math.round(noofInnerpack), innerpackUomlevel, InnerPackQty, InnerPackflag ,InnerPackweight,InnerPackcube,InnerPackQty,vLotNo,vbulkpick,innerpackUom,baseuomid];
					}

					arrLPRequired[2] = skuInnerpackRecord ;

					nlapiLogExecution('DEBUG', 'pickgen_palletisation:No. of Innerpack', noofInnerpack);
					nlapiLogExecution('DEBUG', 'pickgen_palletisation:Remaining Quantity', remQty);
				}
				else{
					var skuinnerpackRecord = [Math.round(0), innerpackUomlevel, InnerPackQty, InnerPackflag ,InnerPackweight,InnerPackcube,InnerPackQty,vLotNo,vbulkpick,innerpackUom,baseuomid];
					arrLPRequired[2] = skuinnerpackRecord ;					
				}

				nlapiLogExecution('DEBUG', 'pickgen_palletisation:Remaining Quantity (Eaches)', remQty);

				// check whether we need to further break down into eaches or not (there will be one LP for all eaches)
				if (parseFloat(remQty) > 0)  {

					if(eachpackflag==1 || eachpackflag==3)
					{
						var skuEachRecord = [Math.round(remQty), eachUomlevel, eachQty, eachpackflag ,eachweight,eachcube,eachQty,vLotNo,vbulkpick,eachUom,baseuomid];							
					}
					else{
						var skuEachRecord = [Math.round(1), eachUomlevel, remQty, eachpackflag,eachweight,eachcube,eachQty,vLotNo,vbulkpick,eachUom,baseuomid];	
					}		

					arrLPRequired[3] = skuEachRecord ;		
				}
			}
		} 
		else 
		{ 
			nlapiLogExecution('DEBUG', 'UOM Dims', 'For all items, Dimensions are not configured ');
		}		
	} 
	catch(exp) {
		nlapiLogExecution('DEBUG', 'Exception in pickgen_palletisation:', exp);	
		return arrLPRequired;
	}
	nlapiLogExecution('DEBUG', 'out of pickgen_palletisation', itemId);
	return arrLPRequired;
}

/*function CreateMasterLPRecord(vContainerLpNo,vContainerSize,TotalWeight,ContainerCube,uompackflag,location,LPparent)
{
	var ResultText='';//GenerateLable(uompackflag);
	var MastLP=nlapiCreateRecord('customrecord_ebiznet_master_lp');		
	MastLP.setFieldValue('name', vContainerLpNo);
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_lp', vContainerLpNo);
	if(vContainerSize!=null && vContainerSize!='')
		MastLP.setFieldValue('custrecord_ebiz_lpmaster_sizeid', vContainerSize);
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_totwght', parseFloat(TotalWeight).toFixed(5));
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(ContainerCube).toFixed(5));
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_sscc', ResultText);	 
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_site', location);	 
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_lptype', 2);	

	var currentContext = nlapiGetContext();
	var retktoval = nlapiSubmitRecord(MastLP);
	nlapiLogExecution('DEBUG', 'Return Value from CreateMasterLPRecord',retktoval);
}*/

function CreateMasterLPRecordBulk(vContainerLpNo,vContainerSize,TotalWeight,ContainerCube,uompackflag,location,LPparent)
{
	var ResultText='';//GenerateLable(uompackflag,location);
	LPparent.selectNewLineItem('recmachcustrecord_ebiz_lp_parent');
	if(vContainerLpNo!=null && vContainerLpNo!='')
		LPparent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','name', vContainerLpNo); 
	if(vContainerLpNo!=null && vContainerLpNo!='')
		LPparent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_lp', vContainerLpNo);
	if(vContainerSize!=null && vContainerSize!='')
		LPparent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_sizeid', vContainerSize); 
	LPparent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_totwght', parseFloat(TotalWeight).toFixed(5));
	LPparent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_totcube', parseFloat(ContainerCube).toFixed(5)); 
	if(ResultText!=null && ResultText!='')
		LPparent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_sscc', ResultText); 
	if(location!=null && location!='')
		LPparent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_site', location); 
	LPparent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_lptype', 2); 
	LPparent.commitLineItem('recmachcustrecord_ebiz_lp_parent');	


}

function CreateMasterLPRecord(vContainerLpNo,vContainerSize,TotalWeight,ContainerCube,uompackflag,location,LPparent)
{
	var ResultText='';//GenerateLable(uompackflag,location);
	nlapiLogExecution('DEBUG', 'Return Value from GenerateLable',ResultText);
	LPparent.selectNewLineItem('recmachcustrecord_ebiz_lp_parent');
	LPparent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','name', vContainerLpNo); // ITEM INTERNAL ID
	LPparent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_lp', vContainerLpNo); // ITEM INTERNAL ID
	if(vContainerSize!=null && vContainerSize!='')
		LPparent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_sizeid', vContainerSize); // ITEM INTERNAL ID
	LPparent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_totwght', parseFloat(TotalWeight).toFixed(5)); // ITEM INTERNAL ID
	LPparent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_totcube', parseFloat(ContainerCube).toFixed(5)); // ITEM INTERNAL ID
	LPparent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_sscc', ResultText); // ITEM INTERNAL ID
	LPparent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_site', location); // ITEM INTERNAL ID
	LPparent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_lptype', 2); // ITEM INTERNAL ID
	LPparent.commitLineItem('recmachcustrecord_ebiz_lp_parent');	

	nlapiLogExecution('DEBUG', 'Return Value from CreateMasterLPRecord Bulk creation');


}

function GenerateLable(uompackflag,location){

	nlapiLogExecution('DEBUG', 'CreateMasterLPRecord uompackflag', uompackflag);
	var finaltext="";
	var duns="";
	var label="",uom="",ResultText="";
	//added by mahesh


	try 
	{	
		var lpMaxValue=GetMaxLPNo('1', '5',location);
		nlapiLogExecution('DEBUG', 'lpMaxValue', lpMaxValue);

		var prefixlength=lpMaxValue.length;
		nlapiLogExecution('DEBUG', 'prefixlength', prefixlength);

		if(prefixlength==0)
			label="000000000";
		else if(prefixlength==1)
			label="00000000"+lpMaxValue;
		else if(prefixlength==2)
			label="0000000"+lpMaxValue;
		else if(prefixlength==3)
			label="000000"+lpMaxValue;
		else if(prefixlength==4)
			label="00000"+lpMaxValue;
		else if(prefixlength==5)
			label="0000"+lpMaxValue;
		else if(prefixlength==6)
			label="000"+lpMaxValue;
		else if(prefixlength==7)
			label="00"+lpMaxValue;
		else if(prefixlength==8)
			label="0"+lpMaxValue;
		else if(prefixlength==9)
			label=lpMaxValue;

		//to get company id

		duns=GetCompanyDUNSnumber('');

		if(uompackflag == "1" || uompackflag == "EACH" ) 
			uom="0"; 
		else if(uompackflag == "3" || uompackflag == "PALLET") 
			uom="2";
		else
			uom="0";
		nlapiLogExecution('DEBUG', 'uom', uom);
		nlapiLogExecution('DEBUG', 'CreateMasterLPRecord finaltext', finaltext);
		finaltext=uom+duns+label;
		nlapiLogExecution('DEBUG', 'CreateMasterLPRecord finaltext', finaltext);



		//to get chk digit
		var checkStr=finaltext;
		var ARL=0;
		var BRL=0;
		var CheckDigitValue="";
		for (i = checkStr.length-1;  i > 0;  i--)
		{
			ARL = ARL+parseFloat(checkStr.charAt(i));
			i--;
		}		
		ARL=ARL*3;
		for (i = checkStr.length-2;  i > 0;  i--)
		{
			BRL = BRL+parseFloat(checkStr.charAt(i));
			i--;
		}		
		var sumOfARLBRL=ARL+BRL;
		var CheckDigit=0;

		while(CheckDigit<10)
		{
			if(sumOfARLBRL%10==0)
			{ 
				CheckDigitValue=CheckDigit; 
				break; 
			} 

			sumOfARLBRL++;
			CheckDigit++;
		}
		nlapiLogExecution('DEBUG', 'CreateMasterLPRecord CheckDigit', CheckDigitValue.toString());
		//ResultText="00"+finaltext+CheckDigitValue.toString();
		ResultText=finaltext+CheckDigitValue.toString();
		nlapiLogExecution('DEBUG', 'CreateMasterLPRecord ResultText', ResultText);
	} 
	catch (err) 
	{

	}
	return ResultText;
}

function updateInventoryWithAllocationbulk(invrecord, allocationQuantity,parent){
	nlapiLogExecution('DEBUG', 'Into updateInventoryWithAllocation - Qty', allocationQuantity);

	parent.selectNewLineItem('recmachcustrecord_ebiz_inv_parent');

	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'id', invrecord.getId());
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','name', invrecord.getValue('name'));
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_binloc', invrecord.getValue('custrecord_ebiz_inv_binloc'));
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_lp', invrecord.getValue('custrecord_ebiz_inv_lp'));
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_sku', invrecord.getValue('custrecord_ebiz_inv_sku'));
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_sku_status', invrecord.getValue('custrecord_ebiz_inv_sku_status'));
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_packcode', invrecord.getValue('custrecord_ebiz_inv_packcode'));
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_qty', parseFloat(invrecord.getValue('custrecord_ebiz_inv_qty')).toFixed(5));
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_inv_ebizsku_no', invrecord.getValue('custrecord_ebiz_inv_sku'));
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_qoh', parseFloat(invrecord.getValue('custrecord_ebiz_qoh')).toFixed(5));
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_itemdesc', invrecord.getValue('custrecord_ebiz_itemdesc'));
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_loc', invrecord.getValue('custrecord_ebiz_inv_loc'));
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_invttasktype', invrecord.getValue('custrecord_invttasktype')); // PUTW.
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_wms_inv_status_flag', 19); // STORAGE	
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_callinv', 'N');
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_displayfield', 'N');
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_fifo', invrecord.getValue('custrecord_ebiz_inv_fifo'));	
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_updated_user_no',  nlapiGetContext().getUser());

	var alreadyallocqty = invrecord.getValue('custrecord_ebiz_alloc_qty');
	if (isNaN(alreadyallocqty))
	{
		alreadyallocqty = 0;
	}

	var totalallocqty=parseFloat(allocationQuantity)+parseFloat(alreadyallocqty);

	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_alloc_qty', parseFloat(totalallocqty).toFixed(5));

	if(invtuomlevel!=null && invtuomlevel!='')
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_uomlvl', invrecord.getValue('custrecord_ebiz_uomlvl'));
	if(invtlot!=null && invtlot!='')
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_lot', invrecord.getValue('custrecord_ebiz_inv_lot'));
	if(invtcompany!=null && invtcompany!='')
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_company', invrecord.getValue('custrecord_ebiz_inv_company'));
	if(invtadjtype!=null && invtadjtype!='')
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_adjusttype', invrecord.getValue('custrecord_ebiz_inv_adjusttype'));

	parent.commitLineItem('recmachcustrecord_ebiz_inv_parent');	 


	nlapiLogExecution('DEBUG', 'Out of updateInventoryWithAllocation - Record ID');

}

function updateInventoryWithAllocation(recordId, allocationQuantity){
	nlapiLogExecution('DEBUG', 'Into updateInventoryWithAllocation - Qty', allocationQuantity);

	var scount=1;
	LABL1: for(var j=0;j<scount;j++)
	{	
		nlapiLogExecution('DEBUG', 'CUSTOM_RECORD_COLLISION', j);
		try
		{
			var inventoryTransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', recordId);
			var alreadyallocqty = inventoryTransaction.getFieldValue('custrecord_ebiz_alloc_qty');
			if (isNaN(alreadyallocqty)||alreadyallocqty==null||alreadyallocqty=="")
			{
				alreadyallocqty = 0;
			}
			nlapiLogExecution('DEBUG','alreadyallocqty',alreadyallocqty);
			nlapiLogExecution('DEBUG','Present QOH ',inventoryTransaction.getFieldValue('custrecord_ebiz_qoh'));
			var totalallocqty=parseFloat(allocationQuantity)+parseFloat(alreadyallocqty);
			inventoryTransaction.setFieldValue('custrecord_ebiz_alloc_qty', parseFloat(totalallocqty).toFixed(5));
			inventoryTransaction.setFieldValue('custrecord_ebiz_callinv', 'N');
			inventoryTransaction.setFieldValue('custrecord_ebiz_displayfield', 'N');

			nlapiSubmitRecord(inventoryTransaction,false,true);
			nlapiLogExecution('DEBUG', 'Out of updateInventoryWithAllocation - Record ID',recordId);

		}
		catch(ex)
		{
			var exCode='CUSTOM_RECORD_COLLISION'; 

			if (ex instanceof nlobjError) 
			{	
				wmsE=ex.getCode() + '\n' + ex.getDetails();
				exCode=ex.getCode();
			}
			else
			{
				wmsE=ex.toString();
				exCode=ex.toString();
			}				 
			if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
			{ 
				scount=scount+1;
				continue LABL1;
			}
			else break LABL1;
		}
	}






}

function IsFailedTaskExist(fulfillOrderList,waveNo,uomlevel)
{
	var IsFailedTaskFound='F';
	nlapiLogExecution('DEBUG', 'into IsFailedTaskExist ');

	var filter=new Array();
	filter.push(new nlobjSearchFilter('name',null,'is',fulfillOrderList[4]));
	filter.push(new nlobjSearchFilter('custrecord_ebiz_wave_no',null,'is',waveNo));
	filter.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no',null,'is',fulfillOrderList[3]));
	filter.push(new nlobjSearchFilter('custrecord_line_no',null,'is',fulfillOrderList[2]));
	filter.push(new nlobjSearchFilter('custrecord_uom_level',null,'is',uomlevel));
	filter.push(new nlobjSearchFilter('custrecord_wms_status_flag',null,'anyof',['26']));
	filter.push(new nlobjSearchFilter('custrecord_tasktype',null,'anyof',['3']));

	var column=new Array();
	column[0]=new nlobjSearchColumn('custrecord_ebiz_wave_no');

	var searchresult=nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filter, column);
	if(searchresult!=null && searchresult!='' && searchresult.length>0)
	{
		IsFailedTaskFound='T';
	}

	nlapiLogExecution('DEBUG', 'out of IsFailedTaskExist', IsFailedTaskFound);
	return IsFailedTaskFound;

}

function createRecordInOpenTask(fulfillOrderList,expectedQuantity, waveNo, containerLP, beginLocationId, recordId,packCode,
		itemStatus, pickMethod, pickZone, pickStrategyId, whLocation, LP,uomlevel,totalweight,totalcube,containersize,fromLot,
		vnotes,wmsCarrier,fulfilmentItem,nsrefno,expiryDate,customer,dimsbulkpick,otparent,vwaveassignedto,baseuomid){

	nlapiLogExecution('DEBUG', 'Into createRecordInOpenTaskBulk ...');
	var str = 'Wave. = ' + waveNo + '<br>';
	str = str + 'expectedQuantity. = ' + expectedQuantity + '<br>';	
	str = str + 'containerLP. = ' + containerLP + '<br>';	
	str = str + 'beginLocationId. = ' + beginLocationId + '<br>';	
	str = str + 'recordId. = ' + recordId + '<br>';	
	str = str + 'packCode. = ' + packCode + '<br>';
	str = str + 'itemStatus. = ' + itemStatus+ '<br>';
	str = str + 'pickMethod. = ' + pickMethod + '<br>';
	str = str + 'pickZone. = ' + pickZone + '<br>';
	str = str + 'pickStrategyId. = ' + pickStrategyId + '<br>';
	str = str + 'whLocation. = ' + whLocation + '<br>';
	str = str + 'LP. = ' + LP + '<br>';
	str = str + 'uomlevel. = ' + uomlevel + '<br>';
	str = str + 'totalweight. = ' + totalweight + '<br>';
	str = str + 'totalcube. = ' + totalcube + '<br>';
	str = str + 'containersize. = ' + containersize + '<br>';
	str = str + 'fromLot. = ' + fromLot + '<br>';
	str = str + 'vnotes. = ' + vnotes+ '<br>';
	str = str + 'wmsCarrier. = ' + wmsCarrier+ '<br>';
	str = str + 'fulfilmentItem. = ' + fulfilmentItem+ '<br>';
	str = str + 'nsrefno. = ' + nsrefno+ '<br>';
	str = str + 'expiryDate. = ' + expiryDate+ '<br>';
	str = str + 'customer. = ' + customer+ '<br>';
	str = str + 'otparent. = ' + otparent+ '<br>';
	str = str + 'dimsbulkpick. = ' + dimsbulkpick+ '<br>';
	str = str + 'vwaveassignedto. = ' + vwaveassignedto+ '<br>';
	str = str + 'baseuomid. = ' + baseuomid+ '<br>';
	nlapiLogExecution('DEBUG', 'Open Task Update Parameters', str);

	var currentContext = nlapiGetContext();  
	var currentUserID = currentContext.getUser();	

	otparent.selectNewLineItem('recmachcustrecord_ebiz_ot_parent');
	if(fulfillOrderList[4] != null && fulfillOrderList[4] != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','name', fulfillOrderList[4]); // FULFILLMENT ORDER #
	if(fulfillOrderList[6] != null && fulfillOrderList[6] != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_parent_sku_no', fulfillOrderList[6]); // ITEM INTERNAL ID
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_sku_no', fulfilmentItem); // ITEM INTERNAL ID
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_sku', fulfilmentItem); // ITEM NAME
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_expe_qty', parseFloat(expectedQuantity).toFixed(5)); // FULFILMENT ORDER QUANTITY
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_container_lp_no', containerLP); // CONTAINER LP
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_lpno', containerLP); // CONTAINER LP
	if(fulfillOrderList[2] != null && fulfillOrderList[2] != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_line_no', fulfillOrderList[2]); // ORDER LINE #
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_tasktype', '3'); // TASK TYPE = PICK	
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actbeginloc', beginLocationId);  // BEGIN LOCATION
	nlapiLogExecution('DEBUG', 'beginLocationId',beginLocationId);
	if(beginLocationId!=null && beginLocationId!='')
	{
		var locgroupfields = ['custrecord_inboundlocgroupid','custrecord_outboundlocgroupid'];
		var locgroupcolumns = nlapiLookupField('customrecord_ebiznet_location', beginLocationId, locgroupfields);		
		var inblocgroupid=locgroupcolumns.custrecord_inboundlocgroupid;
		var oublocgroupid=locgroupcolumns.custrecord_outboundlocgroupid;
		nlapiLogExecution('DEBUG', 'inblocgroupid',inblocgroupid);
		var locgroupseqfields = ['custrecord_sequenceno'];
		//code added on 011012 by suman
		//Allow to insert sequence no only if we get some value form inbonlog group Id.
		//if(inblocgroupid!=null&&inblocgroupid!="")
		if(oublocgroupid!=null && oublocgroupid!="")
		{
			//var locgroupseqcolumns = nlapiLookupField('customrecord_ebiznet_loc_group', inblocgroupid, locgroupseqfields);
			var locgroupseqcolumns = nlapiLookupField('customrecord_ebiznet_loc_group', oublocgroupid, locgroupseqfields);
			var locgroupidseq=locgroupseqcolumns.custrecord_sequenceno;
			nlapiLogExecution('DEBUG', 'locgroupidseq',locgroupidseq);

			otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_bin_locgroup_seq', locgroupidseq);  // BEGIN LOCATION SEQ
		}
	}

	if(fulfillOrderList[3] != null && fulfillOrderList[3] != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_cntrl_no', fulfillOrderList[3]); // FO INTRL ID
	if(fulfillOrderList[1] != null && fulfillOrderList[1] != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_order_no', parseFloat(fulfillOrderList[1])); // SO INTR ID
	if(fulfillOrderList[3] != null && fulfillOrderList[3] != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_receipt_no', fulfillOrderList[3]); 
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_wave_no', waveNo);  // WAVE #
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecordact_begin_date', DateStamp()); // BEGIN DATE	
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_from_lp_no', LP); // PALLET LP
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_lpno', LP); // PALLET LP
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualbegintime', TimeStamp()); // BEGIN TIME
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_invref_no', recordId); // INV REC ID
	if(fulfillOrderList[10] != null && fulfillOrderList[10] != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_packcode', fulfillOrderList[10]); // PACKCODE
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_sku_status', itemStatus); // ITEM STATUS
	/*if(fulfillOrderList[11] != null && fulfillOrderList[11] != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_uom_id', fulfillOrderList[11]); // UOM ID
	 */	
	if(baseuomid!=null && baseuomid!='' && baseuomid!='null')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_uom_id', baseuomid);
	if(fulfillOrderList[12] != null && fulfillOrderList[12] != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_batch_no', fulfillOrderList[12]); // LOT #
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebizrule_no', pickStrategyId); // PICK STRATEGY
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebizmethod_no', pickMethod); // PICK METHOD
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebizzone_no', pickZone); // PICK ZONE	
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebizuser', currentUserID); // CURRENT USER
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_upd_ebiz_user_no', currentUserID); // CURRENT USER
	if(vwaveassignedto!=null && vwaveassignedto!='' && vwaveassignedto!='null')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_taskassignedto', vwaveassignedto); // CURRENT USER
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_uom_level', uomlevel); // UOM LEVEL
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_total_weight', parseFloat(totalweight).toFixed(5)); // TOTAL WEIGHT
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_totalcube', parseFloat(totalcube).toFixed(5)); // TOTAL CUBE
	if(containersize != null && containersize != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_container', containersize); // CONTAINER SIZE
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_batch_no', fromLot); // LOT #
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_notes', vnotes); // NOTES
	if(fulfillOrderList[13] != null && fulfillOrderList[13] != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_comp_id', fulfillOrderList[13]); // COMP ID

	if(fulfillOrderList[41] != null && fulfillOrderList[41] != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_act_solocation', fulfillOrderList[41]);// Actual SO Location

	if(wmsCarrier != null && wmsCarrier != "")
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebizwmscarrier', wmsCarrier); // WMS CARRIER

	if(beginLocationId == "")
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_status_flag', '26'); // STATUS FLAG = F
	else
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_status_flag', '9'); // STATUS FLAG = G

	if(pickZone!=null && pickZone!='')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_zoneid', pickZone); // PICK ZONE

	if(whLocation != null && whLocation!='')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_location', whLocation); // SITE

	if(nsrefno != null && nsrefno!='')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_nsconfirm_ref_no', nsrefno); // NS Ref #

	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_skiptask', '0'); // SKIP
	nlapiLogExecution('DEBUG','expiryDate2',expiryDate);
	if(expiryDate != null && expiryDate != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_expirydate', expiryDate); // Exp Date

	if(customer != null && customer != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_customer', customer);
	if(dimsbulkpick=='T')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_shipasis', 'T');

	otparent.commitLineItem('recmachcustrecord_ebiz_ot_parent');	

	nlapiLogExecution('DEBUG', 'Out of createRecordInOpenTaskBulk ...');
}


function createCustomerLot(fulfillOrderList,customer,fulfilmentItem,fromLot,expiryDate,whLocation)
{
	nlapiLogExecution('DEBUG', 'Into createCustomerLot');

	var str = 'customer. = ' + customer + '<br>';
	str = str + 'fulfilmentItem. = ' + fulfilmentItem + '<br>';	
	str = str + 'fromLot. = ' + fromLot + '<br>';	
	str = str + 'expiryDate. = ' + expiryDate + '<br>';	
	str = str + 'whLocation. = ' + whLocation + '<br>';	

	nlapiLogExecution('DEBUG', 'createCustomerLot Parameters', str);

	var IsExpiryDtFound='F';

//	if(customer!=null && customer!='')
//	{

//	IsExpiryDtFound=IsRecordExist(customer,fulfilmentItem,expiryDate,whLocation);
//	}

	if(IsExpiryDtFound=='F')
	{
		var customerlotRec = nlapiCreateRecord('customrecord_ebiz_customer_lot');
		customerlotRec.setFieldValue('name', fromLot);							// DELIVERY ORDER NAME
		customerlotRec.setFieldValue('custrecord_ebiz_lot_customer', customer);		// ITEM INTERNAL ID
		customerlotRec.setFieldValue('custrecord_ebiz_lotno', fromLot);				// ITEM NAME
		customerlotRec.setFieldValue('custrecord_ebiz_lot_item', fulfilmentItem);
		customerlotRec.setFieldValue('custrecord_ebiz_lot_expiry', expiryDate);				// FULFILMENT ORDER QUANTITY
		customerlotRec.setFieldValue('custrecord_ebiz_lot_pickdate', DateStamp());
		customerlotRec.setFieldValue('custrecord_ebiz_lot_fono', fulfillOrderList[4]);
		customerlotRec.setFieldValue('custrecord_ebiz_lot_whlocation', whLocation);
		nlapiSubmitRecord(customerlotRec,false,true);
	}

	nlapiLogExecution('DEBUG', 'Out of createCustomerLot');
}

function IsRecordExist(customer,fulfilmentItem,expiryDate,whLocation)
{	
	var IsRecordFound='F';
	nlapiLogExecution('DEBUG', 'Into IsRecordExist ');

	var str = 'customer. = ' + customer + '<br>';
	str = str + 'fulfilmentItem. = ' + fulfilmentItem + '<br>';	
	str = str + 'expiryDate. = ' + expiryDate + '<br>';
	str = str + 'whLocation. = ' + whLocation + '<br>';

	nlapiLogExecution('Debug', 'Parameter Details', str);

	var filter=new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebiz_lot_customer',null,'anyof',customer));

	if(fulfilmentItem!=null && fulfilmentItem!='')
		filter.push(new nlobjSearchFilter('custrecord_ebiz_lot_item',null,'anyof',fulfilmentItem));

	if(whLocation!=null && whLocation!='')
		filter.push(new nlobjSearchFilter('custrecord_ebiz_lot_whlocation',null,'anyof',whLocation));

	filter.push(new nlobjSearchFilter('custrecord_ebiz_lot_expiry',null,'is',expiryDate));

	var column=new Array();
	column[0]=new nlobjSearchColumn('custrecord_ebiz_lot_expiry');

	var searchresult=nlapiSearchRecord('customrecord_ebiz_customer_lot', null, filter, column);
	if(searchresult!=null && searchresult!='' && searchresult.length>0)
	{
		IsRecordFound='T';
	}

	nlapiLogExecution('DEBUG', 'Out of IsRecordExist', IsRecordFound);
	return IsRecordFound;
}

function createRecordInOpenTaskold(fulfillOrderList,expectedQuantity, waveNo, containerLP, beginLocationId, recordId,packCode,
		itemStatus, pickMethod, pickZone, pickStrategyId, whLocation, LP,uomlevel,totalweight,totalcube,containersize,fromLot,
		vnotes,wmsCarrier,fulfilmentItem,nsrefno,otparent){
	nlapiLogExecution('DEBUG', 'into createRecordInOpenTask with container lp : ',containerLP);

	var IsFailedTaskFound='F';
	if(beginLocationId == "" || beginLocationId==null)
	{
		IsFailedTaskFound=IsFailedTaskExist(fulfillOrderList,waveNo,uomlevel);
	}

	if(IsFailedTaskFound=='F')
	{
		var openTaskRecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'fulfillOrderList[4]',fulfillOrderList[4]);
		nlapiLogExecution('DEBUG', 'test','test');
		openTaskRecord.setFieldValue('name', fulfillOrderList[4]);							// DELIVERY ORDER NAME
		openTaskRecord.setFieldValue('custrecord_ebiz_sku_no', fulfilmentItem);		// ITEM INTERNAL ID
		openTaskRecord.setFieldValue('custrecord_sku', fulfilmentItem);				// ITEM NAME
		openTaskRecord.setFieldValue('custrecord_parent_sku_no', fulfillOrderList[6]);
		openTaskRecord.setFieldValue('custrecord_expe_qty', parseFloat(expectedQuantity).toFixed(5));				// FULFILMENT ORDER QUANTITY
		openTaskRecord.setFieldValue('custrecord_container_lp_no', containerLP);
		openTaskRecord.setFieldValue('custrecord_ebiz_lpno', containerLP);
		openTaskRecord.setFieldValue('custrecord_line_no', fulfillOrderList[2]);
		openTaskRecord.setFieldValue('custrecord_tasktype', '3'); 							// TASK TYPE = PICK

		openTaskRecord.setFieldValue('custrecord_actbeginloc', beginLocationId);
		openTaskRecord.setFieldValue('custrecord_ebiz_cntrl_no', fulfillOrderList[3]);
		openTaskRecord.setFieldValue('custrecord_ebiz_order_no', parseFloat(fulfillOrderList[1]));
		openTaskRecord.setFieldValue('custrecord_ebiz_receipt_no', fulfillOrderList[3]);
		openTaskRecord.setFieldValue('custrecord_ebiz_wave_no',  waveNo);
		openTaskRecord.setFieldValue('custrecordact_begin_date',DateStamp());
		if(wmsCarrier != null && wmsCarrier != "")
			openTaskRecord.setFieldValue('custrecord_ebizwmscarrier',  wmsCarrier);
		// Setting for failed pick
		if(beginLocationId == "")
			openTaskRecord.setFieldValue('custrecord_wms_status_flag', '26'); 					// STATUS FLAG = F
		else
			openTaskRecord.setFieldValue('custrecord_wms_status_flag', '9'); 					// STATUS FLAG = G

		openTaskRecord.setFieldValue('custrecord_from_lp_no', LP);
		openTaskRecord.setFieldValue('custrecord_lpno', LP);
		openTaskRecord.setFieldValue('custrecord_actualbegintime', TimeStamp());
		openTaskRecord.setFieldValue('custrecord_invref_no', recordId);
		openTaskRecord.setFieldValue('custrecord_packcode', fulfillOrderList[10]);
		openTaskRecord.setFieldValue('custrecord_sku_status', itemStatus);
		openTaskRecord.setFieldValue('custrecord_uom_id', fulfillOrderList[11]);
		openTaskRecord.setFieldValue('custrecord_batch_no', fulfillOrderList[12]);
		openTaskRecord.setFieldValue('custrecord_ebizrule_no', pickStrategyId);
		openTaskRecord.setFieldValue('custrecord_ebizmethod_no', pickMethod);
		openTaskRecord.setFieldValue('custrecord_ebizzone_no', pickZone);
		if(pickZone!=null && pickZone!='')
			openTaskRecord.setFieldValue('custrecord_ebiz_zoneid', pickZone);
		openTaskRecord.setFieldValue('custrecord_comp_id', fulfillOrderList[13]);
		nlapiLogExecution('DEBUG', 'whLocation',whLocation);

		if(whLocation != null && whLocation!='')
			openTaskRecord.setFieldValue('custrecord_wms_location', whLocation);	

		var currentContext = nlapiGetContext();  
		var currentUserID = currentContext.getUser();		
		openTaskRecord.setFieldValue('custrecord_ebizuser', currentUserID);		
		openTaskRecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);						
		//openTaskRecord.setFieldValue('custrecord_taskassignedto', currentUserID);
		openTaskRecord.setFieldValue('custrecord_uom_level', uomlevel);
		openTaskRecord.setFieldValue('custrecord_total_weight', parseFloat(totalweight).toFixed(5));
		openTaskRecord.setFieldValue('custrecord_totalcube', parseFloat(totalcube).toFixed(5));
		openTaskRecord.setFieldValue('custrecord_container', containersize);
		openTaskRecord.setFieldValue('custrecord_batch_no', fromLot);
		openTaskRecord.setFieldValue('custrecord_notes', vnotes);
		openTaskRecord.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', nsrefno);

		nlapiLogExecution('DEBUG', 'fromLot',fromLot);
		nlapiSubmitRecord(openTaskRecord,false,true);


	}
	nlapiLogExecution('DEBUG', 'out of createRecordInOpenTask with container lp : ',containerLP);
}

var vFulfillArr=new Array();
var vFulfillIdArr=new Array();

function updateFulfilmentOrder(fulfillmentOrder, orderQuantity, eBizWaveNo,foparent){

	nlapiLogExecution('DEBUG', 'Into updateFulfillmentOrder...');

	if (orderQuantity == "" || isNaN(orderQuantity)) {
		orderQuantity = 0;
	}

	var str = 'Wave. = ' + eBizWaveNo + '<br>';
	str = str + 'FO Intr Id. = ' + fulfillmentOrder[3] + '<br>';	
	str = str + 'FO Name. = ' + fulfillmentOrder[1] + '<br>';	
	str = str + 'Item Status. = ' + fulfillmentOrder[7] + '<br>';	
	str = str + 'Customer. = ' + fulfillmentOrder[35] + '<br>';	
	str = str + 'Ship Method. = ' + fulfillmentOrder[36] + '<br>';
	str = str + 'wms Carrier. = ' + fulfillmentOrder[20] + '<br>';
	str = str + 'Order Type. = ' + fulfillmentOrder[17] + '<br>';
	str = str + 'Order Priority. = ' + fulfillmentOrder[18] + '<br>';
	str = str + 'Item. = ' + fulfillmentOrder[6] + '<br>';
	str = str + 'Packcode. = ' + fulfillmentOrder[10] + '<br>';
	str = str + 'UOM. = ' + fulfillmentOrder[11] + '<br>';
	str = str + 'Site. = ' + fulfillmentOrder[19] + '<br>';
	str = str + 'Company. = ' + fulfillmentOrder[13] + '<br>';
	str = str + 'Parent Ord #. = ' + fulfillmentOrder[27] + '<br>';
	str = str + 'Freight Term. = ' + fulfillmentOrder[31] + '<br>';
	str = str + 'Prev Pickgen Qty. = ' + fulfillmentOrder[34] + '<br>';
	str = str + 'Pickgen Qty. = ' + orderQuantity + '<br>';

	nlapiLogExecution('DEBUG', 'FO Update Parameters', str);

	if(vFulfillIdArr.indexOf(fulfillmentOrder[3]) == -1)
	{
		nlapiLogExecution('DEBUG','Into IF1');
		vFulfillIdArr.push(fulfillmentOrder[3]);
		var prevpickgenqty=fulfillmentOrder[34];
		nlapiLogExecution('DEBUG','prevpickgenqty',prevpickgenqty);
		if (prevpickgenqty == "" || isNaN(prevpickgenqty)) {
			prevpickgenqty = 0;
		}
		var totalPickGeneratedQuantity = parseFloat(prevpickgenqty) + parseFloat(orderQuantity);
		nlapiLogExecution('DEBUG','totalPickGeneratedQuantity',totalPickGeneratedQuantity);

		//var vFO=[fulfillmentOrder,totalPickGeneratedQuantity,eBizWaveNo];
		var vFO=[fulfillmentOrder,totalPickGeneratedQuantity,eBizWaveNo,orderQuantity];
		vFulfillArr.push(vFO);
	}
	else
	{
		nlapiLogExecution('DEBUG','Into Else Exists');
		var vFOIndex=vFulfillIdArr.indexOf(fulfillmentOrder[3]);
		var vPrevPickGenQty=vFulfillArr[vFOIndex][1];
		nlapiLogExecution('DEBUG','vPrevPickGenQty Exists',vPrevPickGenQty);
		var totalPickGeneratedQuantity = parseFloat(vPrevPickGenQty) + parseFloat(orderQuantity);
		nlapiLogExecution('DEBUG','totalPickGeneratedQuantity Exists',totalPickGeneratedQuantity);
		vFulfillArr[vFOIndex][1]=totalPickGeneratedQuantity;
	}

	/*//	var currentRow = [0-i, 1-soInternalID, 2-lineNo, 3-fointrid, 4-foname, 5-itemName, 6-itemNo, 7-itemStatus,8-foordqty, 9-orderQty, 
//	10-packCode, 11-uom, 12-lotBatchNo, 13-company,14-itemInfo1, 15-itemInfo2, 16-itemInfo3, 17-orderType, 18-orderPriority,
//	19-location,20-wmsCarrier,21-nsrefno,22-note1,23-note2,24-pickgenflag,25-pickqty,26-shipqty,27-parentordno,28-shipmentno,
//	29-printflag,30-printcount,31-freightterms,32-shipcomplete,33-pickreportprintdt,34-vPickGenQty,35-customer,36-shipmethod];

	foparent.selectNewLineItem('recmachcustrecord_ebiz_fo_parent');

	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'id', fulfillmentOrder[3]);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','name', fulfillmentOrder[1]);
	if(fulfillmentOrder[7]!=null && fulfillmentOrder[7]!='')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linesku_status', fulfillmentOrder[7]);
	if(fulfillmentOrder[35]!=null && fulfillmentOrder[35]!='')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_customer', fulfillmentOrder[35]);
	if(fulfillmentOrder[36]!=null && fulfillmentOrder[36]!='')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_carrier', fulfillmentOrder[36]);
	if(fulfillmentOrder[17]!=null && fulfillmentOrder[17]!='')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_order_type', fulfillmentOrder[17]);
	if(fulfillmentOrder[18]!=null && fulfillmentOrder[18]!='')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_order_priority', fulfillmentOrder[18]);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_lineord', fulfillmentOrder[4]);
	if(fulfillmentOrder[6]!=null && fulfillmentOrder[6]!='')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_linesku', fulfillmentOrder[6]);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ord_qty', fulfillmentOrder[8]);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ordline', fulfillmentOrder[2]);
	if(fulfillmentOrder[10]!=null && fulfillmentOrder[10]!='')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linepackcode', fulfillmentOrder[10]); 
	if(fulfillmentOrder[11]!=null && fulfillmentOrder[11]!='')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_lineuom_id', fulfillmentOrder[11]); 
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_batch', fulfillmentOrder[12]);
	if(fulfillmentOrder[19]!=null && fulfillmentOrder[19]!='')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ordline_wms_location', fulfillmentOrder[19]);
	if(fulfillmentOrder[13]!=null && fulfillmentOrder[13]!='')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ordline_company', fulfillmentOrder[13]);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_fulfilmentiteminfo1', fulfillmentOrder[14]);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_fulfilmentiteminfo2',  fulfillmentOrder[15]);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_fulfilmentiteminfo3',  fulfillmentOrder[16]);
	if(fulfillmentOrder[20]!=null && fulfillmentOrder[20]!='')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_wmscarrier',  fulfillmentOrder[20]);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_nsconfirm_ref_no',  fulfillmentOrder[21]);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linenotes1',  fulfillmentOrder[22]);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linenotes2',  fulfillmentOrder[23]);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_pickgen_flag',  fulfillmentOrder[24]);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_pickqty',  fulfillmentOrder[25]);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ship_qty',  fulfillmentOrder[26]);
	if(fulfillmentOrder[27]!=null && fulfillmentOrder[27]!='')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ns_ord',  fulfillmentOrder[27]);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_shipment_no',  fulfillmentOrder[28]);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_printflag',  fulfillmentOrder[29]);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_print_count',  fulfillmentOrder[30]);
	if(fulfillmentOrder[31]!=null && fulfillmentOrder[31]!='')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_freightterms',  fulfillmentOrder[31]);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_shipcomplete',  fulfillmentOrder[32]);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_pr_dateprinted',  fulfillmentOrder[33]);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_wave',  parseFloat(eBizWaveNo));

	var prevpickgenqty=fulfillmentOrder[34];
	nlapiLogExecution('DEBUG','prevpickgenqty',prevpickgenqty);
	if (prevpickgenqty == "" || isNaN(prevpickgenqty)) {
		prevpickgenqty = 0;
	}
	var totalPickGeneratedQuantity = parseFloat(prevpickgenqty) + parseFloat(orderQuantity);
	nlapiLogExecution('DEBUG','totalPickGeneratedQuantity',totalPickGeneratedQuantity);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_pickgen_qty',  totalPickGeneratedQuantity);

	if(parseFloat(orderQuantity)>0)
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linestatus_flag',  9);
	else
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linestatus_flag',  26);


	foparent.commitLineItem('recmachcustrecord_ebiz_fo_parent'); 

	nlapiLogExecution('DEBUG', 'Out of updateFulfillmentOrder...');*/
	nlapiLogExecution('DEBUG', 'Out of updateFulfillmentOrder...');
}
function updateFulfilmentOrderBulk(fulfillmentOrderArr,foparent){

	nlapiLogExecution('DEBUG', 'Into updateFulfillmentOrder...New');

	if(fulfillmentOrderArr != null && fulfillmentOrderArr != '' && fulfillmentOrderArr.length>0)
	{
		nlapiLogExecution('DEBUG', 'FO Update Parameters New', fulfillmentOrderArr.length);
		for(var i=0;i<fulfillmentOrderArr.length;i++)
		{
			nlapiLogExecution('DEBUG', 'FO Update Parameters New Arr Main', fulfillmentOrderArr[i]);
			nlapiLogExecution('DEBUG', 'FO Update Parameters New Arr', fulfillmentOrderArr[i][0]);
//			var currentRow = [0-i, 1-soInternalID, 2-lineNo, 3-fointrid, 4-foname, 5-itemName, 6-itemNo, 7-itemStatus,8-foordqty, 9-orderQty, 
//			10-packCode, 11-uom, 12-lotBatchNo, 13-company,14-itemInfo1, 15-itemInfo2, 16-itemInfo3, 17-orderType, 18-orderPriority,
//			19-location,20-wmsCarrier,21-nsrefno,22-note1,23-note2,24-pickgenflag,25-pickqty,26-shipqty,27-parentordno,28-shipmentno,
//			29-printflag,30-printcount,31-freightterms,32-shipcomplete,33-pickreportprintdt,34-vPickGenQty,35-customer,36-shipmethod];
			var fulfillmentOrder=fulfillmentOrderArr[i][0];
			foparent.selectNewLineItem('recmachcustrecord_ebiz_fo_parent');

			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'id', fulfillmentOrder[3]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','name', fulfillmentOrder[1]);
			if(fulfillmentOrder[7]!=null && fulfillmentOrder[7]!='')
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linesku_status', fulfillmentOrder[7]);
			if(fulfillmentOrder[35]!=null && fulfillmentOrder[35]!='')
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_customer', fulfillmentOrder[35]);
			if(fulfillmentOrder[36]!=null && fulfillmentOrder[36]!='')
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_carrier', fulfillmentOrder[36]);
			if(fulfillmentOrder[17]!=null && fulfillmentOrder[17]!='')
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_order_type', fulfillmentOrder[17]);
			if(fulfillmentOrder[18]!=null && fulfillmentOrder[18]!='')
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_order_priority', fulfillmentOrder[18]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_lineord', fulfillmentOrder[4]);
			if(fulfillmentOrder[6]!=null && fulfillmentOrder[6]!='')
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_linesku', fulfillmentOrder[6]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ord_qty', parseFloat(fulfillmentOrder[8]).toFixed(5));
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ordline', fulfillmentOrder[2]);
			if(fulfillmentOrder[10]!=null && fulfillmentOrder[10]!='')
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linepackcode', fulfillmentOrder[10]); 
			if(fulfillmentOrder[11]!=null && fulfillmentOrder[11]!='')
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_lineuom_id', fulfillmentOrder[11]); 
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_batch', fulfillmentOrder[12]);
			if(fulfillmentOrder[19]!=null && fulfillmentOrder[19]!='')
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ordline_wms_location', fulfillmentOrder[19]);
			if(fulfillmentOrder[13]!=null && fulfillmentOrder[13]!='')
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ordline_company', fulfillmentOrder[13]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_fulfilmentiteminfo1', fulfillmentOrder[14]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_fulfilmentiteminfo2',  fulfillmentOrder[15]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_fulfilmentiteminfo3',  fulfillmentOrder[16]);
			if(fulfillmentOrder[20]!=null && fulfillmentOrder[20]!='')
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_wmscarrier',  fulfillmentOrder[20]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_nsconfirm_ref_no',  fulfillmentOrder[21]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linenotes1',  fulfillmentOrder[22]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linenotes2',  fulfillmentOrder[23]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_pickgen_flag',  fulfillmentOrder[24]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_pickqty',  parseFloat(fulfillmentOrder[25]).toFixed(5));
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ship_qty',  parseFloat(fulfillmentOrder[26]).toFixed(5));
			if(fulfillmentOrder[27]!=null && fulfillmentOrder[27]!='')
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ns_ord',  fulfillmentOrder[27]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_shipment_no',  fulfillmentOrder[28]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_printflag',  fulfillmentOrder[29]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_print_count',  fulfillmentOrder[30]);
			if(fulfillmentOrder[31]!=null && fulfillmentOrder[31]!='')
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_freightterms',  fulfillmentOrder[31]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_shipcomplete',  fulfillmentOrder[32]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_pr_dateprinted',  fulfillmentOrder[33]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_wave',  parseFloat(fulfillmentOrderArr[i][2]));

			/*var prevpickgenqty=fulfillmentOrder[34];
			nlapiLogExecution('DEBUG','prevpickgenqty',prevpickgenqty);
			if (prevpickgenqty == "" || isNaN(prevpickgenqty)) {
				prevpickgenqty = 0;
			}
			var totalPickGeneratedQuantity = parseFloat(prevpickgenqty) + parseFloat(orderQuantity);*/
			nlapiLogExecution('DEBUG','totalPickGeneratedQuantity',fulfillmentOrderArr[i][1]);

			/*var orderQuantity=0;
			if(fulfillmentOrderArr[i][1] != null && fulfillmentOrderArr[i][1] != '')
				orderQuantity=parseFloat(fulfillmentOrderArr[i][1]);

			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_pickgen_qty',parseFloat(orderQuantity).toFixed(5));

			if(parseFloat(orderQuantity)>0)
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linestatus_flag',  9);
			else
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linestatus_flag',  26);*/


			var orderQuantity=0;
			var neworderQuanttity = 0;
			if(fulfillmentOrderArr[i][1] != null && fulfillmentOrderArr[i][1] != '')
				orderQuantity=parseFloat(fulfillmentOrderArr[i][1]);

			if(fulfillmentOrderArr[i][3] != null && fulfillmentOrderArr[i][3] != '')
				neworderQuanttity=parseFloat(fulfillmentOrderArr[i][3]);

			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_pickgen_qty',parseFloat(orderQuantity).toFixed(5));

			if(parseFloat(neworderQuanttity)>0)
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linestatus_flag',  9);
			else
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linestatus_flag',  26);


			foparent.commitLineItem('recmachcustrecord_ebiz_fo_parent'); 

			nlapiLogExecution('DEBUG', 'Out of updateFulfillmentOrder...');
		}
	}	 



}

function updateFulfilmentOrderold(fulfilmentOrderNo, fulfilmentOrderLineNo, orderQuantity, eBizWaveNo){

	if (orderQuantity == "" || isNaN(orderQuantity)) {
		orderQuantity = 0;
	}
	var fulfilmentOrderline = nlapiLoadRecord('customrecord_ebiznet_ordline', fulfilmentOrderNo);
	var prevpickgenqty=fulfilmentOrderline.getFieldValue('custrecord_pickgen_qty');
	var totalPickGeneratedQuantity = parseFloat(prevpickgenqty) + parseFloat(orderQuantity);
	nlapiLogExecution('DEBUG', 'totalPickGeneratedQuantity ', totalPickGeneratedQuantity);
	nlapiLogExecution('DEBUG', 'orderQuantity', orderQuantity);
	fulfilmentOrderline.setFieldValue('custrecord_ebiz_wave', parseFloat(eBizWaveNo));		

	if(parseFloat(orderQuantity)>0)
		fulfilmentOrderline.setFieldValue('custrecord_linestatus_flag', '9'); //status flag--'G' (Picks Generated)	
	else
		fulfilmentOrderline.setFieldValue('custrecord_linestatus_flag', '26'); //status flag--'F' (Picks Failed)

	fulfilmentOrderline.setFieldValue('custrecord_pickgen_qty', parseFloat(totalPickGeneratedQuantity).toFixed(5));

	nlapiSubmitRecord(fulfilmentOrderline, false, true);
	nlapiLogExecution('DEBUG', 'updateFulfilmentOrder ', 'Success');

//	if(parseFloat(orderQuantity)>0)
//	updatesalesorderline(salesorderinternalid,fulfilmentOrderLineNo,orderQuantity,'G');
	//}
	//}
}

function getpickmethod()
{
	nlapiLogExecution('DEBUG','ClustergetPickMethod','chkptTrue');
	var pickInternalId=new Array();
	var filter=new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizcreatecluster',null,'is','T'));
	var column=new Array();
	column[0]=new nlobjSearchColumn('internalid').setSort();
	column[1]=new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxorders');
	column[2]=new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxpicks');
	var searchresult=nlapiSearchRecord('customrecord_ebiznet_pick_method', null, filter, column);

	if(searchresult!=null)
	{
		for ( var i = 0; i < searchresult.length; i++)
		{
			pickInternalId[i]=new Array();
			pickInternalId[i][0]=searchresult[i].getValue('internalid');
			pickInternalId[i][1]=searchresult[i].getValue('custrecord_ebiz_pickmethod_maxorders');
			pickInternalId[i][2]=searchresult[i].getValue('custrecord_ebiz_pickmethod_maxpicks');
		}
	}

	return pickInternalId;
}

function getEntity(vOrderNo)
{	
	var Result = new Array();
	nlapiLogExecution('DEBUG','Into getEntity.....',vOrderNo);

	if(vOrderNo!=null && vOrderNo!='')
	{
		var filters = new Array();
		var columns = new Array();

		filters.push(new nlobjSearchFilter('internalid', null, 'is', vOrderNo));
		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

		columns[0] = new nlobjSearchColumn('entity');
		columns[1] = new nlobjSearchColumn('custbody_nswmssoordertype');

		Result = nlapiSearchRecord('salesorder', null, filters,columns);

		nlapiLogExecution('DEBUG','Out of getEntity.....',Result);
	}
	return Result;
}

function getASNReqdFlag(ventity)
{
	var vASNreqdflag='F';
	nlapiLogExecution('DEBUG','Into getASNReqdFlag.....',ventity);

	var filters = new Array();
	var columns = new Array();

	filters.push(new nlobjSearchFilter('internalid', null, 'is', ventity));

	columns[0] = new nlobjSearchColumn('custentity_ebiz_asn_required');

	var Result = nlapiSearchRecord('customer', null, filters,columns);
	if(Result!=null && Result!='')
	{
		vASNreqdflag = Result[0].getValue('custentity_ebiz_asn_required');
	}

	nlapiLogExecution('DEBUG','Out of getASNReqdFlag.....',vASNreqdflag);
	return vASNreqdflag;
}

function getOrdTypeASNReqdFlag(vordtype)
{
	var vOrdTypeASNreqdflag='F';
	nlapiLogExecution('DEBUG','Into getOrdTypeASNReqdFlag.....',vordtype);

	var filters = new Array();
	var columns = new Array();

	filters.push(new nlobjSearchFilter('internalid', null, 'is', vordtype));

	columns[0] = new nlobjSearchColumn('custrecord_ebiz_asn_required');

	var Result = nlapiSearchRecord('customrecord_ebiznet_order_type', null, filters,columns);
	if(Result!=null && Result!='')
	{
		vOrdTypeASNreqdflag = Result[0].getValue('custrecord_ebiz_asn_required');
	}

	nlapiLogExecution('DEBUG','Out of getOrdTypeASNReqdFlag.....',vOrdTypeASNreqdflag);
	return vOrdTypeASNreqdflag;
}
function generatepickreportbasedcustomerspecific()
{
	var systemrulevalue="N";
	var systemrulename="PickReportHtml";
	var filtercolumarray = new Array();
	var columnarray = new Array();


	var filtercolumarray = new Array();
	filtercolumarray [0] = new nlobjSearchFilter('name', null, 'is', systemrulename);

	var columnarray = new Array();
	columnarray [0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
	//case # 20141263�
	var systemrulesearchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules',null,filtercolumarray ,columnarray);
	if((systemrulesearchresults!=null)&&(systemrulesearchresults!=""))
	{
		systemrulevalue=systemrulesearchresults[0].getValue('custrecord_ebizrulevalue');
	}
	nlapiLogExecution('DEBUG','systemrulevalue',systemrulevalue);
	return systemrulevalue;

}
function GenerateChildLabel(vContLpNo,vOrderNo,vwaveno,uompackflag,company)
{
	nlapiLogExecution('DEBUG','Into GenerateChildLabel.....');
//	nlapiLogExecution('DEBUG','uompackflag',uompackflag);
//	nlapiLogExecution('DEBUG','vwaveno',vwaveno);
//	nlapiLogExecution('DEBUG','vOrderNo',vOrderNo);
//	nlapiLogExecution('DEBUG','company',company);
//	nlapiLogExecution('DEBUG','vContLpNo',vContLpNo);

	var companyDUNSnumber="";
	var ventity='';
	var vordtype = '';
	var vEntityASNreqdflag ='F';
	var vOrdTypeASNreqdflag='F';

	var searchRes = getEntity(vOrderNo);

	if(searchRes!=null && searchRes!='') 
	{
		ventity =  searchRes[0].getValue('entity');
		vordtype = searchRes[0].getValue('custbody_nswmssoordertype');		
	}

	if(ventity!=null && ventity!='') 
	{
		vEntityASNreqdflag = getASNReqdFlag(ventity);
	}

	nlapiLogExecution('DEBUG','vEntityASNreqdflag',vEntityASNreqdflag);

	if(vEntityASNreqdflag!='T')
	{
		vOrdTypeASNreqdflag = getOrdTypeASNReqdFlag(vordtype);
	}

	nlapiLogExecution('DEBUG','vOrdTypeASNreqdflag',vOrdTypeASNreqdflag);

	if(vEntityASNreqdflag=='T' || vOrdTypeASNreqdflag=='T')
	{
		companyDUNSnumber=GetCompanyDUNSnumber(company);

		//get qty from opentask against to containerlp
		var nooflps=0;var uomId="",itemId="",itemText="";
		var qtyfilters = new Array();

		if(vContLpNo !=null && vContLpNo !=""){
			qtyfilters.push( new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vContLpNo));
		}

		if(vOrderNo !=null && vOrderNo !=""){
			qtyfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', [vOrderNo]));
		}

		if(vwaveno !=null && vwaveno !=""){
			qtyfilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', vwaveno));
		}

		qtyfilters.push( new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); 		//Task Type -PICK    
		qtyfilters.push( new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//Status Flag - G (Picks Generated)  

		var qtycolumns = new Array();
		qtycolumns[0] = new nlobjSearchColumn('custrecord_container_lp_no', null, 'group');
		qtycolumns[1] = new nlobjSearchColumn('custrecord_sku', null, 'group');
		qtycolumns[2] = new nlobjSearchColumn('custrecord_uom_id', null, 'group');
		qtycolumns[3] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');	
		qtycolumns[4] = new nlobjSearchColumn('custrecord_packcode', null, 'group');
		qtycolumns[5] = new nlobjSearchColumn('custrecord_ebiz_order_no',null,'min');
		qtycolumns[6] = new nlobjSearchColumn('custrecord_wms_location', null, 'group');
		qtycolumns[1].setSort();

		var qtysearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, qtyfilters, qtycolumns);
		var company="";

		if(qtysearchresults!=null)
		{
			//Creating record in throwaway parent


			var parent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); //create new parent record
			//var parentid = nlapiSubmitRecord(newParent); //save parent record


			var MaxLpNoArray = new Array();

			for(var g=0;g<qtysearchresults.length;g++)
			{		
				var vContainerLpNo = qtysearchresults[g].getValue('custrecord_container_lp_no', null, 'group');
				var itemText = qtysearchresults[g].getText('custrecord_sku', null, 'group');
				var itemId = qtysearchresults[g].getValue('custrecord_sku', null, 'group');
				var itemName=qtysearchresults[g].getText('custrecord_sku', null, 'group');
				var uomId = qtysearchresults[g].getValue('custrecord_uom_id', null, 'group');
				var itemtotalqty = qtysearchresults[g].getValue('custrecord_expe_qty', null, 'sum');
				var packcode = qtysearchresults[g].getValue('custrecord_packcode', null, 'group');
				var eBizOrder=qtysearchresults[g].getValue('custrecord_ebiz_order_no', null,'min');
				var loc=qtysearchresults[g].getValue('custrecord_wms_location', null, 'group');
				//nlapiLogExecution('DEBUG','itemtotalqty',itemtotalqty);
				var veBizOrder=''; 
				if(eBizOrder!=null)
				{
					veBizOrder=eBizOrder.split('#');
				}
				veBizOrder=veBizOrder[1];
				var nooflps="";
				if((packcode!=null)&&(packcode!=""))
				{
					nooflps=Math.ceil(itemtotalqty/packcode);
				}
				else
				{
					nooflps=itemtotalqty;
				}
				nlapiLogExecution('DEBUG','nooflps',nooflps);
//				nlapiLogExecution('DEBUG','Vpackcode',packcode);
//				nlapiLogExecution('DEBUG','vebizOrdNoId',vOrderNo);
//				nlapiLogExecution('DEBUG','uomId',uomId);
//				nlapiLogExecution('DEBUG','itemId',itemId);
				nlapiLogExecution('DEBUG','itemName',itemName);
//				nlapiLogExecution('DEBUG','vContainerLpNo',vContainerLpNo);
//				nlapiLogExecution('DEBUG','qtysearchresults.length',qtysearchresults.length);
				nlapiLogExecution('DEBUG','veBizOrder',veBizOrder);

				MaxLpNoArray=GetMaxLPNoType('1', '5',loc);
				// case no 20126634
				//var lpMaxValue=MaxLpNoArray["maxLpPrefix"];
				var lpMaxValue=MaxLpNoArray["maxLP"];
				var lpMaxPrefixValue=MaxLpNoArray["maxLpPrefix"];
				nlapiLogExecution('DEBUG','sMaxValue',lpMaxValue);
				var lpinternlaid=MaxLpNoArray["maxInternalId"];
				var company=MaxLpNoArray["maxCompany"];


				if((qtysearchresults !=null))
				{
					var seriallineno=1;
//					nlapiLogExecution('DEBUG', 'AcySearch',qtysearchresults.length);
//					nlapiLogExecution('DEBUG', 'AcFirstLoop','AcFirstLoop');
					var lpMaxNo=lpMaxValue;
					for(var i=0; i < nooflps.toString();i++)
					{
						lpMaxNo = parseFloat(lpMaxNo) +parseFloat(1);
//						nlapiLogExecution('DEBUG', 'into for loop','done');
//						nlapiLogExecution('DEBUG', 'lpMaxNo',lpMaxNo);
//						nlapiLogExecution('DEBUG', 'companyDUNSnumber',companyDUNSnumber);
						var ResultText=GenerateLabelCode(uomId,parseFloat(lpMaxNo),companyDUNSnumber);
						var substring=ResultText.substring(10, 20);	
						var vContLpNo='PICK'+substring;
//						nlapiLogExecution('DEBUG', 'substring',substring);

						parent.selectNewLineItem('recmachcustrecord_ebiz_lp_parent');
						parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','name', vContLpNo);
						parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_controlno', veBizOrder);
						parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lp_seq', seriallineno);
						parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_lp', vContLpNo);
						parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_sscc', ResultText);
						parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_lptype', "5");
						parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_item', itemName);
						parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_masterlp', vContainerLpNo);
						parent.commitLineItem('recmachcustrecord_ebiz_lp_parent');

						/*var MastLP=nlapiCreateRecord('customrecord_ebiznet_master_lp');		
					MastLP.setFieldValue('name', vContLpNo);
					MastLP.setFieldValue('custrecord_ebiz_lpmaster_lp', vContLpNo);	
					MastLP.setFieldValue('custrecord_ebiz_lpmaster_sscc', ResultText);
					MastLP.setFieldValue('custrecord_ebiz_lpmaster_item', itemName);
					MastLP.setFieldValue('custrecord_ebiz_lpmaster_masterlp', vContainerLpNo);
					var retktoval = nlapiSubmitRecord(MastLP);*/
						//genarateCartonLabelTemplate(vContainerLpNo,sOrderDetailsArray,itemName,labeltype,ResultText,seriallineno,nooflps,Label);
//						nlapiLogExecution('DEBUG', 'Acseriallineno',seriallineno);
						seriallineno++;
					}

					//Insert MaxLp number in LP Range List 
					InsertMaxLpNo(lpinternlaid,lpMaxNo);
				}
			}
			nlapiSubmitRecord(parent); //submit the parent record, all child records will also be Created
			//nlapiLogExecution('DEBUG','Remaining usage 4',context.getRemainingUsage());

		}
	}

	nlapiLogExecution('DEBUG','Out of GenerateChildLabel.....');
}


function InsertMaxLpNo(internalid,lpMaxValue)
{
	// update the new max LP in the custom record
	nlapiSubmitField('customrecord_ebiznet_lp_range', internalid,'custrecord_ebiznet_lprange_lpmax', parseFloat(lpMaxValue));
}

function GetMaxLPNoType(lpGenerationType, lpType,whsite)
{

	var maxLP = 1;
	var maxLPPrefix = "";
	var internalid="";
	var company="";
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpmax');
	columns[1] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix');
	columns[2] = new nlobjSearchColumn('custrecord_ebiznet_lprange_company');

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'is', lpGenerationType));
	filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lptype', null, 'anyof', [lpType]));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(whsite!=null && whsite!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_site', null, 'anyof', ['@NONE@', whsite]));

	var results = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, filters, columns);

	if(results != null)
	{
		if(results.length > 1)
		{
			alert('More records returned than expected');
			nlapiLogExecution('DEBUG', 'GetMaxLPNo:LP Max Query returned more than 1 row');
		}
		else
		{
			// Incrementing the max LP number only if the search returns 1 row
			for (var t = 0; t < results.length; t++)
			{
				if (results[t].getValue('custrecord_ebiznet_lprange_lpmax') != null) 
				{
					internalid=results[t].getId();
					maxLP = results[t].getValue('custrecord_ebiznet_lprange_lpmax');
					maxLPPrefix = results[t].getValue('custrecord_ebiznet_lprange_lpprefix'); 
					company=results[t].getText('custrecord_ebiznet_lprange_company'); 
				}

			}
		}
		if((maxLP==null)||(maxLP==''))
		{
			maxLP=0;
		}
		maxLP = maxLP.toString();
		nlapiLogExecution('DEBUG', 'GetMaxLPNo:New MaxLP', maxLP);
		var maxArray = new Array();
		//maxArray["maxLpPrefix"]=maxLPPrefix+parseFloat(maxLP);
		maxArray["maxLpPrefix"]=maxLPPrefix;//+parseFloat(maxLP);
		maxArray["maxLP"]=parseFloat(maxLP);
		nlapiLogExecution('DEBUG', 'GetMaxLPNo:maxLpPrefix',maxArray["maxLpPrefix"]);
		maxArray["maxInternalId"]=internalid;
		maxArray["maxCompany"]=company;
		return maxArray;

	}
}

function GenerateLabelCode(uompackflag,lpvalue,duns)
{

	var finaltext="";

	var label="",uom="",ResultText="";
	nlapiLogExecution('DEBUG', 'CreateMasterLPRecord uompackflag',uompackflag);
	nlapiLogExecution('DEBUG', 'lpValue',lpvalue);
	//added by mahesh
	try 
	{	
		var lpMaxValue=lpvalue.toString();
		nlapiLogExecution('DEBUG', 'lpMaxValueInnercode',parseFloat(lpMaxValue.length));
		var prefixlength=parseFloat(lpMaxValue.length);
		nlapiLogExecution('DEBUG', 'prefixlength',prefixlength);
		if(prefixlength==0)
			label="000000000";
		else if(prefixlength==1)
			label="00000000"+lpMaxValue;
		else if(prefixlength==2)
			label="0000000"+lpMaxValue;
		else if(prefixlength==3)
			label="000000"+lpMaxValue;
		else if(prefixlength==4)
			label="00000"+lpMaxValue;
		else if(prefixlength==5)
			label="0000"+lpMaxValue;
		else if(prefixlength==6)
			label="000"+lpMaxValue;
		else if(prefixlength==7)
			label="00"+lpMaxValue;
		else if(prefixlength==8)
			label="0"+lpMaxValue;
		else if(prefixlength==9)
			label=lpMaxValue;


		nlapiLogExecution('DEBUG', 'label', label);


		if(uompackflag == "1" || uompackflag == "EACH" ) 
			uom="0"; 
		else if(uompackflag == "3" || uompackflag == "PALLET") 
			uom="2";
		else
			uom="0";
		nlapiLogExecution('DEBUG', 'uom', uom);
		nlapiLogExecution('DEBUG', 'duns', duns);

		finaltext=uom+duns+label;
		nlapiLogExecution('DEBUG', 'CreateMasterLPRecord finaltext', finaltext);



		//to get chk digit
		var checkStr=finaltext;
		var ARL=0;
		var BRL=0;
		var CheckDigitValue="";
		for (i = checkStr.length-1;  i > 0;  i--)
		{
			ARL = ARL+parseFloat(checkStr.charAt(i));
			i--;
		}		
		ARL=ARL*3;
		for (i = checkStr.length-2;  i > 0;  i--)
		{
			BRL = BRL+parseFloat(checkStr.charAt(i));
			i--;
		}		
		var sumOfARLBRL=ARL+BRL;
		var CheckDigit=0;

		while(CheckDigit<10)
		{
			if(sumOfARLBRL%10==0)
			{ 
				CheckDigitValue=CheckDigit; 
				break; 
			} 

			sumOfARLBRL++;
			CheckDigit++;
		}
		nlapiLogExecution('DEBUG', 'CreateMasterLPRecord CheckDigit', CheckDigitValue.toString());
		ResultText="00"+finaltext+CheckDigitValue.toString();
		nlapiLogExecution('DEBUG', 'CreateMasterLPRecord ResultText', ResultText);


	} 
	catch (err) 
	{
		nlapiLogExecution('DEBUG', 'Exception in  GenerateLabelCode', err);
	}
	return ResultText;
}

function GetCompanyDUNSnumber(company)
{
	var dunsfilters = new Array();
	var duns='';

	if(company!=null && company!='')
		dunsfilters.push(new nlobjSearchFilter('internalid', null, 'is', company));

	dunsfilters.push(new nlobjSearchFilter('custrecord_compduns', null, 'isnotempty'));
	dunsfilters.push(new nlobjSearchFilter('isinactive', null,'is', 'F'));

	var dunscolumns = new Array();
	dunscolumns[0] = new nlobjSearchColumn('custrecord_compduns');			 

	var dunssearchresults = nlapiSearchRecord('customrecord_ebiznet_company', null, dunsfilters, dunscolumns);
	if (dunssearchresults !=null && dunssearchresults !="") 
	{
		duns = dunssearchresults[0].getValue('custrecord_compduns');
		nlapiLogExecution('DEBUG', 'duns', duns);
	}
	return duns;
}
function generatePickreporthtml(vwaveno)
{

	nlapiLogExecution('DEBUG','GeneratePickReportHtml','GeneratePickReportHtml');
	nlapiLogExecution('DEBUG','GeneratePickReportHtmlvwaveno',vwaveno);
	try
	{


		var waverfilterarray= new Array();
		waverfilterarray.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(vwaveno)));
		waverfilterarray.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
		var wavecolumnarray = new Array();
		wavecolumnarray [0] = new nlobjSearchColumn('custrecord_ebiz_clus_no', null, 'group');
		wavecolumnarray [1] = new nlobjSearchColumn('name', null, 'group');
		wavecolumnarray [2] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');
		wavecolumnarray [3] = new nlobjSearchColumn('custrecord_line_no', null, 'count');
		wavecolumnarray [4] = new nlobjSearchColumn('custrecord_ebiz_order_no', null, 'min');
		var wavesearchresults= nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, waverfilterarray, wavecolumnarray);	

		var strVar="";
		if(wavesearchresults!=null)
			totalcount=wavesearchresults.length;
		else
			totalcount=0;
		var grouplength=0;
		while(0<totalcount)
		{
			var count=0;

			if(wavesearchresults!=null)
			{       
				var waverfilterarray= new Array();

				waverfilterarray.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(vwaveno)));
				waverfilterarray.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
				var wavecolumnarray = new Array();
				wavecolumnarray [0] = new nlobjSearchColumn('custrecord_ebiz_clus_no', null, 'group');
				wavecolumnarray [1] = new nlobjSearchColumn('name', null, 'group');
				wavecolumnarray [2] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');
				wavecolumnarray [3] = new nlobjSearchColumn('custrecord_line_no', null, 'count');
				wavecolumnarray [4] = new nlobjSearchColumn('custrecord_ebiz_order_no', null, 'min');
				var wavesearchresults= nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, waverfilterarray, wavecolumnarray);	

				strVar +="<html><body   style=\"font-size:7px padding-right:1px; padding-left:1px; padding-bottom:1px; padding-top:1px\">";
				strVar +="<table><td><tr style=\"Height:1000px\">";
				strVar += "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" frame=\"box\" rules=\"all\">";
				strVar += "<thead style=\"font-family:Verdana;font-size: 10px;line-height: 20px;font-style: normal;	font-weight: normal;text-align: left;text-shadow: white 1px 1px 1px;\">";
				strVar += "<tr style=\" border-width: 7px; border-color:Black; \">";
				strVar += "<th style=\"width: 80px\" >Wave#<\/th>";
				strVar += "<th style=\"width: 80px\" >Cluster#<\/th> ";
				strVar += "<th style=\"width: 80px\" >Ord#<\/th> ";
				strVar += "<th style=\"width: 140px\" >Customer<\/th> ";
				strVar += "<th style=\"width: 140px\" >ShipVia<\/th> ";
				strVar += "<th style=\"width: 90px\" >Total Lines<\/th> ";
				strVar += "<th style=\"width: 90px\" >Total Pieces<\/th> ";
				strVar += "<th style=\"width: 80px\" >Zone<\/th> ";
				strVar += "<th style=\"width: 90px\" >Aisle<\/th>  ";
				strVar += "<th style=\"width: 160px\" >Ship To<\/th> <\/tr><\/thead>";
				strVar += "<tbody  style=\"font-family:Verdana;font-size: 10px;line-height: 20px;font-style: normal;	font-weight: normal;text-align: left;text-shadow: white 1px 1px 1px;\">";
				if(wavesearchresults!=null)
				{
					var name=null;
					var clusterno;

					for(var g=grouplength;g<wavesearchresults.length;g++)
					{
						count++;
						grouplength++;
						name=wavesearchresults[g].getValue('name', null, 'group');
						var lineno=wavesearchresults[g].getValue('custrecord_line_no', null,'count');
						clusterno=wavesearchresults[g].getValue('custrecord_ebiz_clus_no', null, 'group');
						var orderno=wavesearchresults[g].getValue('custrecord_ebiz_order_no',null,'min');
						var totalqty=wavesearchresults[g].getValue('custrecord_expe_qty',null,'sum');
						nlapiLogExecution('DEBUG','orderno',orderno);
						var splitordno=''; 
						if(orderno!=null)
						{
							splitordno=orderno.split('#');
						}
						splitordno=splitordno[1];
						var searchresults = new Array();
						var filters = new Array();
						filters.push(new nlobjSearchFilter('tranid', null, 'is', splitordno));
						var columns = new Array();
						columns[0] = new nlobjSearchColumn('shipaddress');
						columns[1] = new nlobjSearchColumn('shipmethod');
						columns[2] = new nlobjSearchColumn('shipaddressee'); 
						columns[3] = new nlobjSearchColumn('shipaddress1'); 
						columns[4] = new nlobjSearchColumn('shipcity'); 
						columns[5] = new nlobjSearchColumn('shipcountry'); 
						columns[6] = new nlobjSearchColumn('shipstate'); 
						columns[7] = new nlobjSearchColumn('shipzip'); 
						columns[8] = new nlobjSearchColumn('entity'); 
						var searchresults = nlapiSearchRecord('salesorder', null, filters, columns);
						var shipadress=searchresults[0].getValue('shipaddress');
						var shipmethod=searchresults[0].getText('shipmethod');
						var customer=searchresults[0].getText('entity');
						var shipaddressee="";var shipaddr1="";var shipcity="";var shipcountry="";var shipstate="";var shipzip="";var shipstateandcountry="";
						shipaddressee=searchresults[0].getValue('shipaddressee'); 
						shipaddr1=searchresults[0].getValue('shipaddress1'); 
						shipcity=searchresults[0].getValue('shipcity'); 
						shipcountry=searchresults[0].getValue('shipcountry'); 
						shipstate=searchresults[0].getValue('shipstate'); 
						shipzip=searchresults[0].getValue('shipzip'); 
						shipstateandcountry=shipstate+","+shipzip;
						if(clusterno=="- None -")
						{
							clusterno="&nbsp;";
						}

						nlapiLogExecution('DEBUG', 'clusterno',clusterno);

						strVar += "<tr>";
						strVar += "<td style=\"width: 80px;border-bottom: 1px solid black;\" ><font size=1 face=\"MRV Code39extMA\">"+vwaveno+"<\/font><\/td>";
						strVar += "<td style=\"width: 80px;border-bottom: 1px solid black;\" ><font size=1 face=\"MRV Code39extMA\">"+clusterno+"<\/font><\/td> ";
						strVar += "<td style=\"width: 80px;border-bottom: 1px solid black;\" ><font size=1 face=\"MRV Code39extMA\">"+name+"<\/font><\/td> ";
						strVar += "<td style=\"width: 140px\" >"+customer+"<\/td> ";
						strVar += "<td style=\"width: 140px\"  >"+shipmethod+"<\/td> ";
						strVar += "<td style=\"width: 90px\" align=\"center\" >"+lineno+"<\/td> ";
						strVar += "<td style=\"width: 90px\" align=\"center\">"+totalqty+"<\/td> ";
						nlapiLogExecution('DEBUG','orderno',orderno);
						var filterzone = new Array();
						filterzone.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(vwaveno)));
						filterzone.push(new nlobjSearchFilter('name', null, 'is',name));
						filterzone.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));

						var columnzone = new Array();
						columnzone[0]= new nlobjSearchColumn('custrecord_ebiz_zoneid',null,'group');
						columnzone[1]= new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
						var searchopentaskrecords =nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filterzone,columnzone);
						if(searchopentaskrecords!=null)
						{
							strVar +="<td style=\"width: 80px\"><table>";
							strVar +="<tbody  style=\"font-family:Verdana;font-size: 10px;line-height: 20px;font-style: normal;	font-weight: normal;text-align: left;text-shadow: white 1px 1px 1px;\">";
							var repeatvalue='';

							for(var i=0;i<searchopentaskrecords.length;i++)
							{
								var zonevalue=searchopentaskrecords[i].getText('custrecord_ebiz_zoneid',null,'group');			
								nlapiLogExecution('DEBUG','zonevalue',zonevalue);											
								if(zonevalue!=repeatvalue)
								{
									strVar += "<tr  style=\"height:10px;\"><td align=\"center\">"+zonevalue+"<\/td><\/tr>";
									repeatvalue=zonevalue;
								}
							}
							strVar +="<\/body> <\/table><\/td>";
							strVar +="<td style=\"width: 90px\"><table><tr>";
							strVar +="<tbody  style=\"font-family:Verdana;font-size: 9px;\">";
							strVar += " <tr  style=\"height:10px;\">";
							var repeatvalue='';
							var stringappend='';
							var aislecount=0;
							for(var i=0;i<searchopentaskrecords.length;i++)
							{
								var acubiginloc=searchopentaskrecords[i].getText('custrecord_actbeginloc',null,'group');

								var aisle=acubiginloc.split('-');
								if(aisle[0]!=repeatvalue)
								{
									aislecount++;
									stringappend +=aisle[0];
									if(aislecount==6)
									{
										stringappend +="<br\/>";
										aislecount=0;
									}
									else
									{
										stringappend +=",";

									}
									repeatvalue=aisle[0];
								}

							}
							var newstringappend=stringappend.substring(0, stringappend.length-1);
							strVar +="<td>"+newstringappend+"<\/td><\/tr>";
							strVar +="<\/body><\/table><\/td>";
						}
						strVar += "<td style=\"width: 170px\" >" ;
						strVar += "                    <table>";
						strVar += "                        <tr>";
						strVar += "                            <td style=\"font-size: 9px;\">";
						strVar += "                                &nbsp;"+shipaddressee+"";
						strVar += "                            <\/td>";
						strVar += "                        <\/tr>";
						strVar += "                        <tr>";
						strVar += "                            <td style=\"font-size: 9px;\">";
						strVar += "                                &nbsp;"+shipaddr1+"";
						strVar += "                            <\/td>";
						strVar += "                        <\/tr>";
						strVar += "                        <tr>";
						strVar += "                            <td style=\"font-size: 9px;\">";
						strVar += "                                &nbsp;"+shipcity+"";
						strVar += "                            <\/td>";
						strVar += "                        <\/tr>";
						strVar += "                        <tr>";
						strVar += "                            <td style=\"font-size: 9px;\">";
						strVar += "                                &nbsp;"+shipstateandcountry+"";
						strVar += "                            <\/td>";
						strVar += "                        <\/tr>";
						strVar += "                    <\/table>";

						strVar +="<\/td> <\/tr>";

						if(count==5)
						{
							break;
						}
					}
					strVar += "<\/tbody>";
					strVar += "<\/table><\/td><\/tr><\/table>";
					strVar +="<\/body><\/html>";
					if(grouplength==wavesearchresults.length)
					{
						strVar +="<p style=\" page-break-after:avoid\"></p>";
					}
					else
					{
						strVar +="<p style=\" page-break-after:always\"></p>";
					}
					//nlapiLogExecution('DEBUG', 'totalcount', totalcount);
					totalcount=parseFloat(totalcount)-parseFloat(count);
					//nlapiLogExecution('DEBUG', 'totalcountafter', totalcount);

				}

			}
		}
		var tasktype='3';
		var labeltype='PickReport';
		var print='F';
		var reprint='F';
		var company='';
		var location='';
		var formattype='html';
		var labelrecord = nlapiCreateRecord('customrecord_labelprinting'); 
		labelrecord.setFieldValue('name', vwaveno); 
		labelrecord.setFieldValue('custrecord_labeldata',strVar);  
		labelrecord.setFieldValue('custrecord_label_refno',vwaveno);     
		labelrecord.setFieldValue('custrecord_label_tasktype',tasktype);   
		labelrecord.setFieldValue('custrecord_labeltype',labeltype);    
		labelrecord.setFieldValue('custrecord_label_lp',vwaveno);

		labelrecord.setFieldValue('custrecord_label_print', print);
		labelrecord.setFieldValue('custrecord_label_reprint', reprint);
		labelrecord.setFieldValue('custrecord_label_company', company);
		labelrecord.setFieldValue('custrecord_label_location', location);
		var tranid = nlapiSubmitRecord(labelrecord);

	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in generatepickreporthtml',exp);
	}
}


function setClusterNo(eBizWaveNo)
{
	nlapiLogExecution('DEBUG','ClusterWaveNo',eBizWaveNo);
	var pickMethodIds=new Array();
	pickMethodIds=getpickmethod();

	if(pickMethodIds!=null && pickMethodIds.length>0)
	{		
		var ebizMethodNo=new Array();
		var ebizInternalRecordId=new Array();
		var ebizOrderId=new Array();
		var ebizZoneIds = new Array();

		var filter=new Array();
		filter.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is',eBizWaveNo));
		filter.push(new nlobjSearchFilter('custrecord_tasktype',null,'anyof',[3]));
		filter.push(new nlobjSearchFilter('custrecord_wms_status_flag',null,'anyof',[9]));
		filter.push(new nlobjSearchFilter('custrecord_actualendtime',null,'isempty'));//Change from Factory mation production
		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizmethod_no').setSort();
		column[1]=new nlobjSearchColumn('internalid');
		column[2]=new nlobjSearchColumn('name').setSort();
		column[3]=new nlobjSearchColumn('custrecord_ebiz_zoneid').setSort();

		var searchRecord=nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filter, column);
		//nlapiLogExecution('DEBUG','ClusterSearchRecord',searchRecord.length);
		var distEbizZoneId;
		if(searchRecord!=null && searchRecord!='')
		{
			nlapiLogExecution('DEBUG','searchRecord',searchRecord.length);
			for(var count=0;count < searchRecord.length; count++)
			{
				ebizMethodNo[count] = searchRecord[count].getValue('custrecord_ebizmethod_no');
				ebizInternalRecordId[count] = searchRecord[count].getValue('internalid');
				ebizOrderId[count] = searchRecord[count].getValue('name');
				if(searchRecord[count].getValue('custrecord_ebiz_zoneid')!=null && searchRecord[count].getValue('custrecord_ebiz_zoneid')!='')
					ebizZoneIds[count] = searchRecord[count].getValue('custrecord_ebiz_zoneid');
				else
					ebizZoneIds[count] = '-1';
			}

			distEbizZoneId = removeDuplicateElement(ebizZoneIds);
		}

		var str = 'pickMethodIds. = ' + pickMethodIds + '<br>';
		str = str + 'ebizMethodNo. = ' + ebizMethodNo + '<br>';	
		str = str + 'pickMethodIds[0][1]. = ' + pickMethodIds[0][1] + '<br>';	
		str = str + 'pickMethodIds[0][2]. = ' + pickMethodIds[0][2] + '<br>';	
		str = str + 'distEbizZoneId. = ' + distEbizZoneId + '<br>';	

		nlapiLogExecution('DEBUG', 'Pick Method Parameters', str);

		/*var clusterno = GetMaxTransactionNo('WAVECLUSTER');
		nlapiLogExecution('DEBUG','clusternobeforefor',clusterno);*/
		//start 31/07/13
		if(distEbizZoneId!=null && distEbizZoneId!='')
		{
			nlapiLogExecution('DEBUG','zonecount',distEbizZoneId.length);

			for(var zonecount=0;zonecount<distEbizZoneId.length;zonecount++)
			{
				var clusterno = GetMaxTransactionNo('WAVECLUSTER');

				var str = 'zone. = ' + distEbizZoneId[zonecount] + '<br>';
				str = str + 'clusterno. = ' + clusterno + '<br>';	

				nlapiLogExecution('DEBUG', 'Zone & Cluster#', str);

				for(var itemcount=0;itemcount<pickMethodIds.length;itemcount++)
				{
					var ordno="";
					var maxPickFlagCount=0;
					var tempcount=0;

					for(var loopcount=0;(searchRecord!=null&&loopcount<searchRecord.length);loopcount++)
					{  	
//						nlapiLogExecution('DEBUG','loopcount',loopcount);
//						nlapiLogExecution('DEBUG','clustervalues',pickMethodIds[itemcount][0]+','+ebizMethodNo[loopcount]);
//						nlapiLogExecution('DEBUG','clustervalues_zones',distEbizZoneId[zonecount]+','+ebizZoneIds[loopcount]);

						if(pickMethodIds[itemcount][0]==ebizMethodNo[loopcount] && distEbizZoneId[zonecount] == ebizZoneIds[loopcount])
						{
//							nlapiLogExecution('DEBUG','distEbizZoneId[zonecount]',distEbizZoneId[zonecount]);
//							nlapiLogExecution('DEBUG','ebizZoneIds[loopcount]',ebizZoneIds[loopcount]);
//							nlapiLogExecution('DEBUG','pickMethodIds[itemcount][1]',pickMethodIds[itemcount][1]);
//							nlapiLogExecution('DEBUG','pickMethodIds[itemcount][2]',pickMethodIds[itemcount][2]);
							//for a particular pickmethod ,if max order no. is not null and max pickno. is null
							//then the below code execute.

							if(pickMethodIds[itemcount][1]!="" && pickMethodIds[itemcount][2]=="")
							{
								nlapiLogExecution('DEBUG','BlockOfCodeForMaxPickIsNULL',ebizOrderId[loopcount]);
								if(ordno!=ebizOrderId[loopcount])
								{
									//if(parseFloat(tempcount)<pickMethodIds[itemcount][1] && tempcount !=0)
									//above code is commented becuase seq# is missing while generating clusters.
									if(parseFloat(tempcount)<pickMethodIds[itemcount][1])
									{
										tempcount=parseFloat(tempcount)+1;
										nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
										ordno = ebizOrderId[loopcount];
									}
									else
									{
										clusterno = GetMaxTransactionNo('WAVECLUSTER');
										nlapiLogExecution('DEBUG','clusternoafterelse1',clusterno);
										nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
										ordno = ebizOrderId[loopcount];
										tempcount=1;
									}
								}
								else
								{
									nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
								}
							}



							//for a particular pickmethod ,if max orderno. is null and max pickno.is not null 
							//then the below code execute.
							if(pickMethodIds[itemcount][1] == "" && pickMethodIds[itemcount][2] != "")
							{
								nlapiLogExecution('DEBUG','BlockOfCodeForMaxPickIsNULL',pickMethodIds[itemcount][2]);
								tempcount=parseFloat(tempcount)+1;
								if(parseFloat(tempcount)<=pickMethodIds[itemcount][2])
								{
									nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
								}
								else
								{
									clusterno = GetMaxTransactionNo('WAVECLUSTER');
									nlapiLogExecution('DEBUG','clusternoafterelse2',clusterno);
									nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
									tempcount=1;
								}
							}



							//for a pickmethod, if max orderno. and max pickno is not null 
							//then the below code execute.
							if(pickMethodIds[itemcount][1]!= "" && pickMethodIds[itemcount][2] != "")
							{
								//nlapiLogExecution('DEBUG','BlockOfCodeForNONEIsNULL','chkpt');
								maxPickFlagCount= parseFloat(maxPickFlagCount)+1;
//								nlapiLogExecution('DEBUG','maxPickFlagCount',maxPickFlagCount);
//								nlapiLogExecution('DEBUG','pickMethodIds[itemcount][2]',pickMethodIds[itemcount][2]);
//								nlapiLogExecution('DEBUG','ordno',ordno);
//								nlapiLogExecution('DEBUG','ebizOrderId[loopcount]',ebizOrderId[loopcount]);
								if(parseFloat(maxPickFlagCount)<=pickMethodIds[itemcount][2])
								{
									if(ordno!=ebizOrderId[loopcount])
									{
										tempcount=parseFloat(tempcount)+1;

										if(parseFloat(tempcount)<=pickMethodIds[itemcount][1])
										{
											nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
											ordno = ebizOrderId[loopcount];
										}
										else
										{
											clusterno = GetMaxTransactionNo('WAVECLUSTER');
											nlapiLogExecution('DEBUG','clusternoafterelse3',clusterno);
											nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
											ordno = ebizOrderId[loopcount];
											tempcount=1;
										}
									}
									else
									{
										nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
									}
								}
								else
								{
									clusterno = GetMaxTransactionNo('WAVECLUSTER');
//									nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
									maxPickFlagCount= 1;

									if(ordno!=ebizOrderId[loopcount])
									{
										tempcount=parseFloat(tempcount)+1;

										if(parseFloat(tempcount)<=pickMethodIds[itemcount][1])
										{
											nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
											ordno = ebizOrderId[loopcount];
										}
										else
										{
											clusterno = GetMaxTransactionNo('WAVECLUSTER');
											nlapiLogExecution('DEBUG','clusternoafterelse4',clusterno);
											nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
											ordno = ebizOrderId[loopcount];
											tempcount=1;
										}
									}
									else
									{
										nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

function setCountAgainstEachContainerLp(vwaveno,vordno)
{
	try
	{
		var vopentaskRec=getOpentaskRec(vwaveno,vordno);
		var ID;
		nlapiLogExecution('DEBUG','vopentaskRec',vopentaskRec);
		var vContainerLP=new Array();
		for ( var count = 0; vopentaskRec!=null && count < vopentaskRec.length; count++) 
		{
			var vConLp=vopentaskRec[count].getValue('custrecord_container_lp_no');
			if(vConLp!=""&&vConLp!=null)
				vContainerLP[count]=vConLp;
		}
		nlapiLogExecution('DEBUG','vcontainerLP',vContainerLP);
		var vDistinctContainerLp=removeDuplicateElement(vContainerLP);
		vDistinctContainerLp.sort();
		nlapiLogExecution('DEBUG','vDistinctContainerLp',vDistinctContainerLp);
		for ( var vcount = 0; vDistinctContainerLp!=null && vcount < vDistinctContainerLp.length; vcount++) 
		{
			var Maxcount=parseFloat(vcount)+1;
			var filterLpMaster=new Array();
			filterLpMaster[0]=new nlobjSearchFilter('name',null,'is',vDistinctContainerLp[vcount]);

			var rec=nlapiSearchRecord('customrecord_ebiznet_master_lp',null,filterLpMaster,null);
			if(rec!=null&&rec!="")
			{
				var fields=['custrecord_ebiz_lpmaster_pkgno','custrecord_ebiz_lpmaster_pkgcount'];
				var values=[Maxcount,vDistinctContainerLp.length.toString()];
				ID=nlapiSubmitField('customrecord_ebiznet_master_lp', rec[0].getId(),fields,values );
			}
			nlapiLogExecution('DEBUG','ID',ID);
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in setCountAgainstEachContainerLp',exp);
	}
}

function getOpentaskRec(vwaveno,vordno)
{

	try{
		nlapiLogExecution('DEBUG','vwaveno',vwaveno);
		nlapiLogExecution('DEBUG','vordno',vordno);
		var opentasks;

		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', vordno));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', vwaveno));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
		columns[0].setSort();

		opentasks = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	

		return opentasks;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','getOpentaskRec',getOpentaskRec);
	}
}

function getOpenPickqty(actEndLocation)
{
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', actEndLocation);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]);				
	filters[2] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	return searchresults;
}

function getOpenputawayqty(actEndLocation)
{
	nlapiLogExecution('DEBUG','actEndLocation',actEndLocation);
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', actEndLocation);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);				
	filters[2] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return searchresults;
}

function getopenreplens(controlno,fulfilmentItem,pfLocationId,pfWHLoc)
{
	nlapiLogExecution('DEBUG','controlno',controlno);
	nlapiLogExecution('DEBUG','fulfilmentItem',fulfilmentItem);
	var filters = new Array();
	//filters[0] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', controlno); // Remove this from where clause
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8])); // Replen task
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
	filters.push(new nlobjSearchFilter('custrecord_sku', null,'anyof' ,fulfilmentItem));
	filters.push(new nlobjSearchFilter('custrecord_actendloc', null,'anyof' ,pfLocationId));
//	if(pfWHLoc != null && pfWHLoc != '')
	//	filters.push(new nlobjSearchFilter('custrecord_site_id', null,'is' ,pfWHLoc));
	// Add pickface location filter
	// Add location(site) for filter

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return searchresults;
}
function getopenreplensNew(controlno,fulfilmentItem,pfLocationId,pfWHLoc)
{
	nlapiLogExecution('DEBUG','controlno',controlno);
	nlapiLogExecution('DEBUG','fulfilmentItem',fulfilmentItem);
	nlapiLogExecution('DEBUG','pfLocationId',pfLocationId);
	var filters = new Array();
	//filters[0] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', controlno); // Remove this from where clause
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8])); // Replen task
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
	filters.push(new nlobjSearchFilter('custrecord_sku', null,'anyof' ,fulfilmentItem));
	filters.push(new nlobjSearchFilter('custrecord_actendloc', null,'anyof' ,pfLocationId));
//	if(pfWHLoc != null && pfWHLoc != '')
	//	filters.push(new nlobjSearchFilter('custrecord_site_id', null,'is' ,pfWHLoc));
	// Add pickface location filter
	// Add location(site) for filter

	var columns = new Array();



	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	nlapiLogExecution('DEBUG','pfLocationIdsearchresults',searchresults);
	return searchresults;
}

function getQOHForAllSKUsinPFLocation(actEndLocation,ItemId,WHLoc,fulfillmentItemStatus){

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', actEndLocation));
	//filters.push(new nlobjSearchFilter('custrecord_ebizlocationtype','custrecord_ebiz_inv_binloc', 'anyof', [6,7]));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_avl_qty', null, 'greaterthan', 0)); //Comment this
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', ItemId));
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19','3']));
	if(fulfillmentItemStatus!=null && fulfillmentItemStatus!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'anyof', fulfillmentItemStatus));
	//if(WHLoc != null && WHLoc !='')
	//filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', WHLoc));
	var columns = new Array();	
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh',null,'sum');



	var pfSKUInvList = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

	return pfSKUInvList;
}
function buildItemListForReplenProcessing(replenItemArray, itemArrayLength){
	var replenItemList = new Array();

	for (var p = 0; p < itemArrayLength; p++){
		replenItemList.push(replenItemArray[p][0]);
	}

	return replenItemList;
}

function getdimsforallitems(ebizskuno,whLocation) {
	nlapiLogExecution('DEBUG', 'into getdimsforallitems', whLocation);

	var cubelist = new Array();
	var uomcube="";

	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', ebizskuno));
	filters.push(new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	
	if(whLocation!=null && whLocation!='' && whLocation!='null')
		filters.push(new nlobjSearchFilter('custrecord_ebizsiteskudim', null, 'anyof', ['@NONE@', whLocation]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
	columns[1] = new nlobjSearchColumn('custrecord_ebizweight');
	columns[2] = new nlobjSearchColumn('custrecord_ebizlength');
	columns[3] = new nlobjSearchColumn('custrecord_ebizwidth');
	columns[4] = new nlobjSearchColumn('custrecord_ebizheight');
	columns[5] = new nlobjSearchColumn('custrecord_ebizitemdims');	

	cubelist = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

	nlapiLogExecution('DEBUG', 'out of getdimsforallitems', '');

	return cubelist;

}

function getUOMCube(uomlevel,ebizskuno,whlocation) {
	nlapiLogExecution('DEBUG', 'Into getUOMCube', str);
	var str = 'uomlevel. = ' + uomlevel + '<br>';
	str = str + 'ebizskuno. = ' + ebizskuno + '<br>';	
	str = str + 'whlocation. = ' + whlocation + '<br>';	

	nlapiLogExecution('DEBUG', 'getUOMCube Parameters', str);

	var cubelist = new Array();
	var uomcube="";

	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', ebizskuno));
	//filters.push(new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'is', uomlevel));	
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(whlocation!=null && whlocation!='' && whlocation!='null')
		filters.push(new nlobjSearchFilter('custrecord_ebizsiteskudim', null, 'anyof',['@NONE@',whlocation]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
	columns[1] = new nlobjSearchColumn('custrecord_ebizweight');
	columns[2] = new nlobjSearchColumn('custrecord_ebizlength');
	columns[3] = new nlobjSearchColumn('custrecord_ebizwidth');
	columns[4] = new nlobjSearchColumn('custrecord_ebizheight');
	columns[5] = new nlobjSearchColumn('custrecord_ebizqty');

	cubelist = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

	nlapiLogExecution('DEBUG', 'out of getUOMCube', cubelist);

	return cubelist;

}

function pickgen_palletisationforkititems(itemId,ordqty,AllItemDims,DOwhLocation)
{
	nlapiLogExecution('DEBUG', 'Into pickgen_palletisation');
	var str = 'itemId. = ' + itemId + '<br>';
	str = str + 'ordqty. = ' + ordqty + '<br>';	
	str = str + 'AllItemDims. = ' + AllItemDims + '<br>';
	str = str + 'DOwhLocation. = ' + DOwhLocation + '<br>';

	nlapiLogExecution('DEBUG', 'Parameter Details', str);

	try {
		var skulist=new Array();
		skulist.push(itemId);
		var arrLPRequired=new Array();
		var eachQty = 0;
		var caseQty = 0;
		var palletQty = 0;
		var InnerPackQty = 0;
		var noofPallets = 0;
		var noofCases = 0;
		var noofInnerpack = 0;
		var remQty = 0;
		var tOrdQty = ordqty;
		var eachpackflag="";
		var casepackflag="";
		var palletpackflag="";
		var InnerPackflag="";
		var eachweight="";
		var caseweight="";
		var palletweight="";
		var InnerPackweight="";
		var eachcube="";
		var casecube="";
		var palletcube="";
		var InnerPackcube="";

		//AllItemDims = [sku, uom, uomLevel, qty, baseUOMFlag, packflag,weight,cube];

		if (AllItemDims != null && AllItemDims.length > 0) 
		{	
			nlapiLogExecution('DEBUG', 'AllItemDims.length', AllItemDims.length);
			for(var p = 0; p < AllItemDims.length; p++)
			{
				//var itemval = AllItemDims[p].getValue('custrecord_ebizitemdims');
				//var itemtext = AllItemDims[p].getText('custrecord_ebizitemdims');

				var itemval = AllItemDims[p][0];

				if(AllItemDims[p][0] == itemId)
				{
					nlapiLogExecution('DEBUG', 'Item Dims configured for SKU: ', itemId);

					var skuDim = AllItemDims[p][2];
					var skuQty = AllItemDims[p][3];	
					var packflag = AllItemDims[p][5];	
					var weight = AllItemDims[p][6];
					var cube = AllItemDims[p][7];
					var uom=AllItemDims[p][1];
					var bulkpick=AllItemDims[p][9];

//					nlapiLogExecution('DEBUG', 'UOM Level : ', skuDim);
//					nlapiLogExecution('DEBUG', 'UOM Qty : ', skuQty);
//					nlapiLogExecution('DEBUG', 'UOM Pack Flag : ', packflag);
//					nlapiLogExecution('DEBUG', 'Weight : ', weight);
//					nlapiLogExecution('DEBUG', 'Cube : ', cube);

					//UOM LEVEL(Internal ID)  : 1-EACH		2-CASE		3-PALLET	4-InnerPack

					if(skuDim == '1'){
						eachQty = skuQty;
						eachpackflag=packflag;
						eachweight = weight;
						eachcube = cube;
					}								
					else if(skuDim == '2'){
						caseQty = skuQty;
						casepackflag=packflag;
						caseweight = weight;
						casecube=cube;
					}								
					else if(skuDim == '3'){
						palletQty = skuQty;
						palletpackflag = packflag;
						palletweight=weight;
						palletcube=cube;
					}	
					else if(skuDim == '4'){
						InnerPackQty = skuQty;
						InnerPackflag = packflag;
						InnerPackweight=weight;
						InnerPackcube=cube;
					}
				}	
			}

			if(parseFloat(eachQty) == 0)
			{
				nlapiLogExecution('DEBUG', 'UOM Dims', 'Item dim EACH is not configured for SKU: '+ itemId);
			}
			if(parseFloat(caseQty) == 0)
			{
				nlapiLogExecution('DEBUG', 'UOM Dims', 'Item dim CASE is Not Configured for SKU: '+ itemId);
			}
			if(parseFloat(palletQty) == 0)
			{
				nlapiLogExecution('DEBUG', 'UOM Dims', 'Item dim PALLET is not Configured for SKU: '+ itemId);
			}

			//Packflag : 1-Ship Cartons		2-Build Cartons		3-Ship Pallets

			if(eachQty>0 ||caseQty>0 || palletQty>0) 
			{
				var noofLpsRequired = 0;

				// Get the number of pallets required for this item (there will be one LP per pallet)
				noofPallets = getSKUDimCount(ordqty, palletQty);
				remQty = parseFloat(ordqty) - (noofPallets * palletQty);				

				var skuPltRecord=new Array();
				if(noofPallets > 0){
					if(palletpackflag==1 || palletpackflag==3)
					{
						noofLpsRequired = parseFloat(noofPallets);
						skuPltRecord = [Math.round(noofPallets), '3', palletQty, palletpackflag,palletweight,palletcube,palletQty,bulkpick,uom];					
					}
					else
					{
						var palletTaskQty=Math.round(noofPallets * palletQty);
						noofPallets=1;
						noofLpsRequired = parseFloat(noofPallets);
						skuPltRecord = [Math.round(noofPallets), '3', palletTaskQty, palletpackflag,palletweight,palletcube,palletQty,bulkpick,uom];
					}
				}
				else{						
					skuPltRecord = [Math.round(noofPallets), '3', palletQty, palletpackflag,palletweight,palletcube,palletQty,bulkpick,uom];
				}
				arrLPRequired[0] = skuPltRecord ;

				nlapiLogExecution('DEBUG', 'pickgen_palletisation:No. of Pallets', noofPallets);
				nlapiLogExecution('DEBUG', 'pickgen_palletisation:Remaining Quantity', remQty);


				// check whether we need to break down into cases or not
				if (parseFloat(remQty) > 0 && parseFloat(caseQty) != 0) 
				{
					// Get the number of cases required for this item (only one LP for all cases)
					noofCases = getSKUDimCount(remQty, caseQty);
					remQty = parseFloat(remQty) - (noofCases * caseQty);

					var skuCaseRecord=new Array();

					if(noofCases > 0){
						noofLpsRequired = parseFloat(noofLpsRequired) + 1;

						if(casepackflag==1 || casepackflag==3)
						{
							skuCaseRecord = [Math.round(noofCases), '2', caseQty, casepackflag,caseweight,casecube,caseQty,bulkpick,uom];							
						}
						else{
							var caseTaskQty=Math.round(noofCases * caseQty);
							noofCases=1;
							skuCaseRecord = [Math.round(noofCases), '2', caseTaskQty, casepackflag,caseweight,casecube,caseQty,bulkpick,uom];
						}
					}
					else{						
						skuCaseRecord = [Math.round(noofCases), '2', caseQty, casepackflag,caseweight,casecube,caseQty,bulkpick,uom];
					}

					arrLPRequired[1] = skuCaseRecord ;

					nlapiLogExecution('DEBUG', 'pickgen_palletisation:No. of Cases', noofCases);
					nlapiLogExecution('DEBUG', 'pickgen_palletisation:Remaining Quantity', remQty);
				}
				else{
					var skuCaseRecord = [Math.round(0), '2', caseQty, casepackflag,caseweight,casecube,caseQty,bulkpick,uom];
					arrLPRequired[1] = skuCaseRecord ;					
				}

				// check whether we need to break down into InnerPack or not
				if (parseFloat(remQty) > 0 && parseFloat(InnerPackQty) != 0) 
				{
					// Get the number of innerpack required for this item (only one LP for all innerpack)
					noofInnerpack = getSKUDimCount(remQty, InnerPackQty);
					remQty = parseFloat(remQty) - (noofInnerpack * InnerPackQty);

					var skuInnerpackRecord=new Array();

					if(noofInnerpack > 0){
						noofLpsRequired = parseFloat(noofLpsRequired) + 1;

						if(InnerPackflag==1 || InnerPackflag==3)
						{
							skuInnerpackRecord = [Math.round(noofInnerpack), '4', InnerPackQty, InnerPackflag ,InnerPackweight,InnerPackcube,InnerPackQty,bulkpick,uom];							
						}
						else{
							var InnerpackTaskQty=Math.round(noofInnerpack * InnerPackQty);
							noofInnerpack=1;
							skuInnerpackRecord = [Math.round(noofInnerpack), '4', InnerpackTaskQty, InnerPackflag ,InnerPackweight,InnerPackcube,InnerPackQty,bulkpick,uom];	
						}
					}
					else{						
						skuInnerpackRecord = [Math.round(noofInnerpack), '4', InnerPackQty, InnerPackflag ,InnerPackweight,InnerPackcube,InnerPackQty,bulkpick,uom];
					}

					arrLPRequired[2] = skuInnerpackRecord ;

					nlapiLogExecution('DEBUG', 'pickgen_palletisation:No. of Innerpack', noofInnerpack);
					nlapiLogExecution('DEBUG', 'pickgen_palletisation:Remaining Quantity', remQty);
				}
				else{
					var skuinnerpackRecord = [Math.round(0), '4', InnerPackQty, InnerPackflag ,InnerPackweight,InnerPackcube,InnerPackQty,bulkpick,uom];
					arrLPRequired[2] = skuinnerpackRecord ;					
				}

				nlapiLogExecution('DEBUG', 'pickgen_palletisation:Remaining Quantity (Eaches)', remQty);

				// check whether we need to further break down into eaches or not (there will be one LP for all eaches)
				if (parseFloat(remQty) > 0)  {
					if(eachpackflag==1 || eachpackflag==3)
					{
						var skuEachRecord = [Math.round(remQty), "1", eachQty, eachpackflag ,eachweight,eachcube,eachQty,bulkpick,uom];							
					}
					else{
						var skuEachRecord = [Math.round(1), "1", remQty, eachpackflag,eachweight,eachcube,eachQty,bulkpick,uom];	
					}		
					//	var skuEachRecord = [Math.round(1), '1', remQty, eachpackflag,eachweight,eachcube,eachQty];
					arrLPRequired[3] = skuEachRecord ;		
				}
			}
		} 
		else 
		{ 
			nlapiLogExecution('DEBUG', 'UOM Dims', 'For all items, Dimensions are not configured ');
		}		
	} 
	catch(exp) {
		nlapiLogExecution('DEBUG', 'Exception in pickgen_palletisation:', exp);	
		return arrLPRequired;
	}
	nlapiLogExecution('DEBUG', 'out of pickgen_palletisation', itemId);
	return arrLPRequired;
}

function getAllPickRules(Item,uomlevel,whLocation,ItemStatus,Packcode,OrderType,OrdPriority){

	nlapiLogExecution('DEBUG', 'Into getAllPickRules','');

	var pickRulesList = new Array();

	var ItemFamily = null;
	var ItemGroup = null;
	var ItemInfo1 = null;
	var ItemInfo2 = null;
	var ItemInfo3 = null;
	var putVelocity = null;

	var filters = new Array();

	if(ItemFamily!=null && ItemFamily != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskufamilypickrul', null, 'anyof', ['@NONE@', ItemFamily]));
	}

	if(ItemGroup!=null && ItemGroup != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskugrouppickrul', null, 'anyof', ['@NONE@', ItemGroup]));
	}

	if(ItemStatus!=null && ItemStatus != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskustatspickrul', null, 'anyof', ['@NONE@', ItemStatus]));
	}

	if(ItemInfo1!=null && ItemInfo1 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo1', null, 'anyof',['@NONE@', ItemInfo1]));
	}

	if(ItemInfo2!=null && ItemInfo2 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo2', null, 'anyof',['@NONE@', ItemInfo2]));
	}

	if(ItemInfo3!=null && ItemInfo3 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo3', null, 'anyof',['@NONE@', ItemInfo3]));
	}

	if(Item != null && Item != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskupickrul', null, 'anyof', ['@NONE@', Item]));		
	}

	if(putVelocity != null && putVelocity != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizabcvel', null, 'anyof', ['@NONE@', putVelocity]));
	}

	if(whLocation != null && whLocation != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizsitepickrule', null, 'anyof', [whLocation]));
	}

	if(uomlevel != null && uomlevel != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizuomlevelpickrul', null, 'anyof', ['@NONE@', uomlevel]));
	}

	if(Packcode != null && Packcode != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizpackcodepickrul', null, 'anyof', ['@NONE@', Packcode]));
	}

	if(OrderType != null && OrderType != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizordertypepickrul', null, 'anyof', ['@NONE@', OrderType]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizordertypepickrul', null, 'anyof', ['@NONE@']));
	}

	if(OrdPriority != null && OrdPriority != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizorderpriority', null, 'anyof', ['@NONE@', OrdPriority]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizorderpriority', null, 'anyof', ['@NONE@']));
	}

	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizruleidpick');
	columns[1] = new nlobjSearchColumn('custrecord_ebizpickmethod');
	columns[2] = new nlobjSearchColumn('custrecord_ebizpickzonerul');
	columns[3] = new nlobjSearchColumn('custrecord_ebizlocationgrouppickrul');
	columns[4] = new nlobjSearchColumn('custrecord_ebizlocationpickrul');
	//columns[5] = new nlobjSearchColumn('custrecord_ebizsequencenopickrul');
	columns[5]=new nlobjSearchColumn('formulanumeric');
	columns[5].setFormula("TO_NUMBER({custrecord_ebizsequencenopickrul})").setSort();
	columns[6] = new nlobjSearchColumn('custrecord_ebizskufamilypickrul');
	columns[7] = new nlobjSearchColumn('custrecord_ebizskugrouppickrul');
	columns[8] = new nlobjSearchColumn('custrecord_ebizskupickrul');
	columns[9] = new nlobjSearchColumn('custrecord_ebizskuinfo1');
	columns[10] = new nlobjSearchColumn('custrecord_ebizskuinfo2');
	columns[11] = new nlobjSearchColumn('custrecord_ebizskuinfo3');
	columns[12] = new nlobjSearchColumn('custrecord_ebizpackcodepickrul');
	columns[13] = new nlobjSearchColumn('custrecord_ebizuomlevelpickrul');
	columns[14] = new nlobjSearchColumn('custrecord_ebizskustatspickrul');
	columns[15] = new nlobjSearchColumn('custrecord_ebizordertypepickrul');
	columns[16] = new nlobjSearchColumn('custrecord_ebizuompickrul');
	columns[17] = new nlobjSearchColumn('custrecord_allocation_strategy','custrecord_ebizpickmethod');
	columns[18] = new nlobjSearchColumn('custrecord_ebizpickfacelocation','custrecord_ebizpickmethod');
	columns[19] = new nlobjSearchColumn('custrecord_ebizcreatecluster','custrecord_ebizpickmethod');
	columns[20] = new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxorders','custrecord_ebizpickmethod');
	columns[21] = new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxpicks','custrecord_ebizpickmethod');
	columns[22] = new nlobjSearchColumn('custrecord_ebizabcvel');
	columns[23] = new nlobjSearchColumn('custrecord_cartonization_method','custrecord_ebizpickmethod');
	columns[24] = new nlobjSearchColumn('custrecord_ebizsitepickrule');
	columns[25] = new nlobjSearchColumn('custrecord_ebiz_stagedetermination','custrecord_ebizpickmethod');
	//columns[5].setSort();

	pickRulesList = new nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters, columns);

	nlapiLogExecution('DEBUG', 'Out of getAllPickRules','');

	return pickRulesList;
}

function getListOfAllPickRulesKit(Item,uomlevel,whLocation,ItemStatus,Packcode,OrderType,pickRulesList,pflocationsarr){

	nlapiLogExecution('DEBUG', 'Into getListOfAllPickRules','');

	var ItemPickRules = new Array();
	var allocConfigDtls = new Array();
	var pickRuleCount = 0;

	var fields = ['custitem_item_family','custitem_item_group','custitem_ebizdefskustatus',
	              'custitem_item_info_1','custitem_item_info_2','custitem_item_info_3','custitem_ebizabcvelitem'];


	var columns= nlapiLookupField('Item',Item,fields);
	var ItemFamily = columns.custitem_item_family;
	var ItemGroup = columns.custitem_item_group;
	var ItemInfo1 = columns.custitem_item_info_1;
	var ItemInfo2 = columns.custitem_item_info_2;
	var ItemInfo3 = columns.custitem_item_info_3;
	var putVelocity = columns.custitem_ebizabcvelitem;

	var str = 'Item. = ' + Item + '<br>';
	str = str + 'ItemFamily. = ' + ItemFamily + '<br>';	
	str = str + 'ItemGroup. = ' + ItemGroup + '<br>';
	str = str + 'ItemInfo1. = ' + ItemInfo1 + '<br>';
	str = str + 'ItemInfo2. = ' + ItemInfo2 + '<br>';
	str = str + 'ItemInfo3. = ' + ItemInfo3 + '<br>';
	str = str + 'putVelocity. = ' + putVelocity + '<br>';
	str = str + 'uomlevel. = ' + uomlevel + '<br>';
	str = str + 'whLocation. = ' + whLocation + '<br>';
	str = str + 'ItemStatus. = ' + ItemStatus + '<br>';
	str = str + 'Packcode. = ' + Packcode + '<br>';
	str = str + 'OrderType. = ' + OrderType + '<br>';

	nlapiLogExecution('DEBUG', 'Parameter Details', str);


	if(pickRulesList != null && pickRulesList != '' && pickRulesList.length>0)
	{
		nlapiLogExecution('DEBUG', 'pickRulesList length', pickRulesList.length);

		for(var j = 0; j < pickRulesList.length; j++){

			var ruleitemfamily = pickRulesList[j].getValue('custrecord_ebizskufamilypickrul');
			var ruleitemgroup = pickRulesList[j].getValue('custrecord_ebizskugrouppickrul');
			var ruleitem = pickRulesList[j].getValue('custrecord_ebizskupickrul');
			var ruleitemstatus = pickRulesList[j].getValue('custrecord_ebizskustatspickrul');
			var ruleiteminfo1 = pickRulesList[j].getValue('custrecord_ebizskuinfo1');
			var ruleiteminfo2 = pickRulesList[j].getValue('custrecord_ebizskuinfo2');
			var ruleiteminfo3 = pickRulesList[j].getValue('custrecord_ebizskuinfo3');
			var ruleputvelocity = pickRulesList[j].getValue('custrecord_ebizabcvel');
			var ruleordertype = pickRulesList[j].getValue('custrecord_ebizordertypepickrul');
			var rulepackcode = pickRulesList[j].getValue('custrecord_ebizpackcodepickrul');
			var rulewhlocation = pickRulesList[j].getValue('custrecord_ebizsitepickrule');
			var ruleuomlevel = pickRulesList[j].getValue('custrecord_ebizuomlevelpickrul');

			if((ruleitemfamily==null || ruleitemfamily=='')||(ruleitemfamily != null && ruleitemfamily != '' && ruleitemfamily == ItemFamily))	
			{
				if((ruleitemgroup==null || ruleitemgroup=='')||(ruleitemgroup != null && ruleitemgroup != '' && ruleitemgroup == ItemGroup))	
				{
					if((ruleitemstatus==null || ruleitemstatus=='')||(ruleitemstatus != null && ruleitemstatus != '' && ruleitemstatus == ItemStatus))	
					{
						if((ruleiteminfo1==null || ruleiteminfo1=='')||(ruleiteminfo1 != null && ruleiteminfo1 != '' && ruleiteminfo1 == ItemInfo1))	
						{
							if((ruleiteminfo2==null || ruleiteminfo2=='')||(ruleiteminfo2 != null && ruleiteminfo2 != '' && ruleiteminfo2 == ItemInfo2))	
							{
								if((ruleiteminfo3==null || ruleiteminfo3=='')||(ruleiteminfo3 != null && ruleiteminfo3 != '' && ruleiteminfo3 == ItemInfo3))	
								{
									if((ruleitem==null || ruleitem=='')||(ruleitem != null && ruleitem != '' && ruleitem == Item))	
									{
										if((ruleputvelocity==null || ruleputvelocity=='')||(ruleputvelocity != null && ruleputvelocity != '' && ruleputvelocity == putVelocity))	
										{
											if((rulewhlocation==null || rulewhlocation=='')||(rulewhlocation != null && rulewhlocation != '' && rulewhlocation == whLocation))	
											{
												if((ruleuomlevel==null || ruleuomlevel=='')||(ruleuomlevel != null && ruleuomlevel != '' && ruleuomlevel == uomlevel))	
												{
													if((rulepackcode==null || rulepackcode=='')||(rulepackcode != null && rulepackcode != '' && rulepackcode == Packcode))	
													{
														if((ruleordertype==null || ruleordertype=='')||(ruleordertype != null && ruleordertype != '' && ruleordertype == OrderType))	
														{
															var pickRuleId = pickRulesList[j].getValue('custrecord_ebizruleidpick');
															var pickMethodId = pickRulesList[j].getValue('custrecord_ebizpickmethod');
															var pickZoneId = pickRulesList[j].getValue('custrecord_ebizpickzonerul');
															var locationGroupId = pickRulesList[j].getValue('custrecord_ebizlocationgrouppickrul');
															var binLocationId = pickRulesList[j].getValue('custrecord_ebizlocationpickrul');
															var sequenceNo = pickRulesList[j].getValue('custrecord_ebizsequencenopickrul');
															var itemFamilyId = pickRulesList[j].getValue('custrecord_ebizskufamilypickrul');
															var itemGroupId = pickRulesList[j].getValue('custrecord_ebizskugrouppickrul');
															var itemId = pickRulesList[j].getValue('custrecord_ebizskupickrul');
															var itemInfo1 = pickRulesList[j].getValue('custrecord_ebizskuinfo1');
															var itemInfo2 = pickRulesList[j].getValue('custrecord_ebizskuinfo2');
															var itemInfo3 = pickRulesList[j].getValue('custrecord_ebizskuinfo3');
															var packCode = pickRulesList[j].getValue('custrecord_ebizpackcodepickrul');
															var uomLevel = pickRulesList[j].getValue('custrecord_ebizuomlevelpickrul');
															var itemStatus = pickRulesList[j].getValue('custrecord_ebizskustatspickrul');
															var orderType = pickRulesList[j].getValue('custrecord_ebizordertypepickrul');
															var uom = pickRulesList[j].getValue('custrecord_ebizuompickrul');
															var allocationStrategy = pickRulesList[j].getValue('custrecord_allocation_strategy','custrecord_ebizpickmethod');
															var pickfaceLocationFlag = pickRulesList[j].getValue('custrecord_ebizpickfacelocation','custrecord_ebizpickmethod');
															var clusterPickFlag = pickRulesList[j].getValue('custrecord_ebizcreatecluster','custrecord_ebizpickmethod');
															var maxOrders = pickRulesList[j].getValue('custrecord_ebiz_pickmethod_maxorders','custrecord_ebizpickmethod');
															var maxPicks = pickRulesList[j].getValue('custrecord_ebiz_pickmethod_maxpicks','custrecord_ebizpickmethod');
															var abcVel = pickRulesList[j].getValue('custrecord_abcvelpickrule');
															var cartonizationmethod = pickRulesList[j].getValue('custrecord_cartonization_method','custrecord_ebizpickmethod');
															var picklocation=pickRulesList[j].getValue('custrecord_ebizsitepickrule');
															var stagedetermination = pickRulesList[j].getValue('custrecord_ebiz_stagedetermination','custrecord_ebizpickmethod');
															var pickmethodforwardpick = pickRulesList[j].getValue('custrecord_ebiz_isforwardpick','custrecord_ebizpickmethod');

															var currentRow = [j, pickRuleId, pickMethodId, pickZoneId, locationGroupId, binLocationId, 
															                  sequenceNo, itemFamilyId, itemGroupId, itemId, itemInfo1, itemInfo2, itemInfo3, 
															                  packCode, uomLevel, itemStatus, orderType, uom, allocationStrategy, pickfaceLocationFlag, 
															                  clusterPickFlag, maxOrders, maxPicks, abcVel,cartonizationmethod,picklocation,
															                  stagedetermination,pickmethodforwardpick];

															ItemPickRules.push(currentRow);

														}
													}
												}
											}
										}
									}
								}
							}							
						}
					}
				}				
			}
		}

		if(ItemPickRules!=null && ItemPickRules!='' && ItemPickRules.length>0)
		{
			nlapiLogExecution('DEBUG', 'ItemPickRules length',ItemPickRules.length);

			var pfLocationResults = new Array();

			pfLocationResults = getPFLocationsForItem(pflocationsarr,whLocation,Item);
			if(pfLocationResults!=null && pfLocationResults!='' && pfLocationResults.length>0)
			{
				nlapiLogExecution('DEBUG', 'pfLocationResults length',pfLocationResults.length);
			}

			for (var k=0; k<ItemPickRules.length; k++){

				var pickStrategyId = ItemPickRules[k][1];
				var pickMethod = ItemPickRules[k][2];
				var pickZone = ItemPickRules[k][3];
				var locnGroup = ItemPickRules[k][4];
				var binLocation = ItemPickRules[k][5];
				var allocationStrategy = ItemPickRules[k][18];
				var pickfaceLocationFlag = ItemPickRules[k][19];
				var clusterPickFlag = ItemPickRules[k][20];
				var maxOrders = ItemPickRules[k][21];
				var maxPicks = ItemPickRules[k][22];
				var cartonizationmethod =  ItemPickRules[k][24];
				var stagedetermination =  ItemPickRules[k][26];
				var isforwardpickmethod = ItemPickRules[k][27];

				var tempPFLocationResults = new Array();

				if(pickfaceLocationFlag == 'T')
					tempPFLocationResults = pfLocationResults;
				else
					tempPFLocationResults = null;

				var locnGroupList = new Array();

				// Pick strategy details for the fulfillment order; Using index in the order list to identify the order
				var currentRow1 = [k, pickStrategyId, pickMethod, pickZone, locnGroup, binLocation, allocationStrategy,
				                   tempPFLocationResults, locnGroupList, clusterPickFlag, maxOrders, maxPicks,cartonizationmethod,
				                   stagedetermination,isforwardpickmethod];

				allocConfigDtls.push(currentRow1);
			}
		}
	}

	nlapiLogExecution('DEBUG', 'Out of getListOfAllPickRules','');

	return allocConfigDtls;
}

function getPFLocationsForItem(pflocationsarr,whLocation,Item)
{
	nlapiLogExecution('DEBUG', 'Into getPFLocationsForItem','');

	var pfLocations = new Array();

	if(pflocationsarr!=null && pflocationsarr!='' && pflocationsarr.length>0)
	{
		nlapiLogExecution('DEBUG', 'pflocationsarr length',pflocationsarr.length);

		for (var k2=0; k2<pflocationsarr.length; k2++){

			var pfwhlocation = pflocationsarr[k2].getValue('custrecord_pickface_location');
			var pfItem = pflocationsarr[k2].getValue('custrecord_pickfacesku');

			if(pfwhlocation == whLocation && pfItem == Item)
			{
				pfLocations.push(pflocationsarr[k2]);
			}
		}
	}

	nlapiLogExecution('DEBUG', 'Out of getPFLocationsForItem','');

	return pfLocations;
}

function getLocnGroupsForOrderKit(locnGroup, binLocation, pickZone,allzonelocgroups){

	var locnGroupList = new Array();
	// Get the bin location group list
	if (locnGroup == null || locnGroup == "")
		locnGroupList = getZoneLocnGroups(pickZone,allzonelocgroups);
	else{
		if (binLocation == null || binLocation == ''){
			locnGroupList.push(locnGroup);
		}
	}
	return locnGroupList;
}

var tempInvtResultsArray=new Array();


function getInventorySearchResults(maxno,vBinlocGroup)
{
	nlapiLogExecution('DEBUG', 'Into getInventorySearchResults');
	nlapiLogExecution('DEBUG', 'maxno',maxno);
	nlapiLogExecution('DEBUG', 'vBinlocGroup',vBinlocGroup);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [18,19]));	
	if(vBinlocGroup!=null && vBinlocGroup!='')
		filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'anyof', vBinlocGroup));	

	if(maxno!=-1)
	{
		filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));
	}

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	column[1] = new nlobjSearchColumn('internalid');
	column[1].setSort(false);
	var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, column);
	if(invtsearchresults!=null)
	{
		if(invtsearchresults.length>=1000)
		{
			nlapiLogExecution('DEBUG', 'invtsearchresults',invtsearchresults.length);

			var maxno1=invtsearchresults[invtsearchresults.length-1].getId();
			for(var j=0;j<invtsearchresults.length;j++)
			{
				if(tempInvtResultsArray.indexOf(invtsearchresults[j].getValue('custrecord_ebiz_inv_binloc'))==-1)
					tempInvtResultsArray.push(invtsearchresults[j].getValue('custrecord_ebiz_inv_binloc'));
			}
			getInventorySearchResults(maxno1,vBinlocGroup);
		}
		else
		{
			for(var j=0;j<invtsearchresults.length;j++)
			{
				if(tempInvtResultsArray.indexOf(invtsearchresults[j].getValue('custrecord_ebiz_inv_binloc'))==-1)
					tempInvtResultsArray.push(invtsearchresults[j].getValue('custrecord_ebiz_inv_binloc'));
			}
		}
	}

	nlapiLogExecution('DEBUG', 'Out of getInventorySearchResults');
	return tempInvtResultsArray;
}

function pickRuleZoneLocation(inventorySearchResults, replenItemArray, reportNo, stageLocation,whLocation,salesOrderNo,
		eBizWaveNo,taskpriority){
	var returnval=false;
	for (var s = 0; s < replenItemArray.length; s++){
		var replenItem = replenItemArray[s][0];
		var replenQty=replenItemArray[s][2];
		var actEndLocationId = replenItemArray[s][1];

		var stageLPNo = 0;

		nlapiLogExecution('DEBUG','Item', replenItem);

		var stageLPs = getStageLPsAndUpdateLPRange(parseFloat(1),whLocation);


		var totallocqty=0;

		if (inventorySearchResults != null) {
			for (var t = 0; t < inventorySearchResults.length; t++) {
				var item = inventorySearchResults[t].getValue('custrecord_ebiz_inv_sku');

				var actBatchText = "";
				if(inventorySearchResults[t].getValue('custrecord_ebiz_inv_lot') != null)
					actBatchText=inventorySearchResults[t].getText('custrecord_ebiz_inv_lot');	

				var RecordId = inventorySearchResults[t].getId();
				var actBinLocationId = inventorySearchResults[t].getValue('custrecord_ebiz_inv_binloc');
				if (replenItem == item && actEndLocationId != actBinLocationId){
//					nlapiLogExecution('DEBUG','Inside the item condition check',item);
					var actualQty = inventorySearchResults[t].getValue('custrecord_ebiz_qoh');
					var inventoryQty = inventorySearchResults[t].getValue('custrecord_ebiz_inv_qty');
					var allocatedQty = inventorySearchResults[t].getValue('custrecord_ebiz_alloc_qty');
					var lpNo = inventorySearchResults[t].getValue('custrecord_ebiz_inv_lp');
					var actBinLocationId = inventorySearchResults[t].getValue('custrecord_ebiz_inv_binloc');
					var actBinLocation = inventorySearchResults[t].getText('custrecord_ebiz_inv_binloc');
					var itemStatus = inventorySearchResults[t].getValue('custrecord_ebiz_inv_sku_status');
					var whLocation = inventorySearchResults[t].getValue('custrecord_ebiz_inv_loc');

//					nlapiLogExecution('DEBUG','WH Location',whLocation);

					/*
					 * While actualQty > replenQty and TaskCount > 0
					 * 		Create open task record
					 * 		update everything else
					 * 		reduce actual quantity and task count
					 * loop
					 */
					if(allocatedQty==''||allocatedQty==null)
						allocatedQty=0;

					var Availqty=parseFloat(actualQty)-parseFloat(allocatedQty);
					nlapiLogExecution('DEBUG', 'Availqty', Availqty);
					nlapiLogExecution('DEBUG', 'replenQty', replenQty);
					//if(parseFloat(totalReplenRequired) > 0 && parseFloat(Availqty) <= parseFloat(totalReplenRequired))
					//{
					//	returnval=true;
					if(parseFloat(replenQty) > 0 && parseFloat(Availqty) > 0){

						var tempqty;
						if(parseFloat(Availqty) > parseFloat(replenQty))
						{
							tempqty=replenQty;
						}
						else
						{
							tempqty=Availqty;
						}
						nlapiLogExecution('DEBUG', 'tempqty', tempqty);
						var tempAllocQty = getAllocationQty(tempqty, replenQty, replenQty);


						nlapiLogExecution('DEBUG', 'tempAllocQty', tempAllocQty);

						if(parseInt(tempAllocQty)>parseInt(replenQty))
						{
							tempqty=replenQty;
						}
						else
						{
							tempqty=tempAllocQty;
						}
						nlapiLogExecution('DEBUG', 'tempqty', tempqty);
						//nlapiLogExecution('DEBUG','taskpriority from pick excep in inv gen func if', taskpriority);
						createOpenTaskRecord(tempqty, actBinLocationId, item, whLocation, reportNo, 
								'', stageLPs[stageLPNo], actBatchText, stageLocation, 
								actEndLocationId, lpNo,RecordId,itemStatus,salesOrderNo,eBizWaveNo,taskpriority);


						updateInventoryWithAllocationQuantity(RecordId, tempAllocQty, itemStatus);

						stageLPNo++;
						replenQty=parseInt(replenQty)-parseInt(tempqty);


					}				
					else
					{
						if(replenQty==0)
						{
							returnval=true;
							break;
						}
					}
					/*else {
						var tempActQty = Availqty;
						while(parseFloat(tempActQty) > 0 ){

							var tempAllocQty = getAllocationQty(tempActQty, replenQty, replenQty);
							//nlapiLogExecution('DEBUG','taskpriority from pick excep in inv gen func else', taskpriority);
							createOpenTaskRecord(tempAllocQty, actBinLocationId, item, whLocation, reportNo, 
									'', stageLPs[stageLPNo], actBatchText, stageLocation, 
									actEndLocationId, lpNo,RecordId,itemStatus,salesOrderNo,eBizWaveNo,taskpriority);

						// Updating the inventory with allocation quantity
							updateInventoryWithAllocationQuantity(RecordId, tempAllocQty, itemStatus);


							stageLPNo++;
							returnval=true;
							nlapiLogExecution('DEBUG', 'else: StageLPNo', stageLPNo);
						//}
					}*/
					nlapiLogExecution('DEBUG', 'StageLPNo', stageLPNo);
					//}
				}
			}
		}
	}
	if(returnval==false)
	{
		return -1;
	}
	else
	{
		return true;	
	}
}


function updateFulfilmentOrderBackForFailedComps(fulfillmentOrder, orderQuantity, eBizWaveNo,foparent){

	nlapiLogExecution('DEBUG', 'Into updateFulfillmentOrderBack...');

	if (orderQuantity == "" || isNaN(orderQuantity)) {
		orderQuantity = 0;
	}

	var str = 'Wave. = ' + eBizWaveNo + '<br>';
	str = str + 'FO Intr Id. = ' + fulfillmentOrder[3] + '<br>';	
	str = str + 'FO Name. = ' + fulfillmentOrder[1] + '<br>';	
	str = str + 'Item Status. = ' + fulfillmentOrder[7] + '<br>';	
	str = str + 'Customer. = ' + fulfillmentOrder[35] + '<br>';	
	str = str + 'Ship Method. = ' + fulfillmentOrder[36] + '<br>';
	str = str + 'wms Carrier. = ' + fulfillmentOrder[20] + '<br>';
	str = str + 'Order Type. = ' + fulfillmentOrder[17] + '<br>';
	str = str + 'Order Priority. = ' + fulfillmentOrder[18] + '<br>';
	str = str + 'Item. = ' + fulfillmentOrder[6] + '<br>';
	str = str + 'Packcode. = ' + fulfillmentOrder[10] + '<br>';
	str = str + 'UOM. = ' + fulfillmentOrder[11] + '<br>';
	str = str + 'Site. = ' + fulfillmentOrder[19] + '<br>';
	str = str + 'Company. = ' + fulfillmentOrder[13] + '<br>';
	str = str + 'Parent Ord #. = ' + fulfillmentOrder[27] + '<br>';
	str = str + 'Freight Term. = ' + fulfillmentOrder[31] + '<br>';
	str = str + 'Prev Pickgen Qty. = ' + fulfillmentOrder[34] + '<br>';
	str = str + 'Pickgen Qty. = ' + orderQuantity + '<br>';

	nlapiLogExecution('DEBUG', 'FO Update Parameters', str);


	var vFOIndex=vFulfillIdArr.indexOf(fulfillmentOrder[3]);
	var vPrevPickGenQty=vFulfillArr[vFOIndex][1];
	nlapiLogExecution('DEBUG','vPrevPickGenQty Exists',vPrevPickGenQty);
	var totalPickGeneratedQuantity = parseFloat(vPrevPickGenQty) - parseFloat(orderQuantity);
	nlapiLogExecution('DEBUG','totalPickGeneratedQuantity Exists',totalPickGeneratedQuantity);
	vFulfillArr[vFOIndex][1]=totalPickGeneratedQuantity;

	nlapiLogExecution('DEBUG', 'Out of updateFulfillmentOrder...');
}

function CancelInventoryAllocation(recordId, allocationQuantity){
	nlapiLogExecution('DEBUG', 'Into CancelInventoryWithAllocation - Qty', allocationQuantity);
	var inventoryTransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', recordId);
	var alreadyallocqty = inventoryTransaction.getFieldValue('custrecord_ebiz_alloc_qty');
	if (isNaN(alreadyallocqty)||alreadyallocqty==null||alreadyallocqty=="")
	{
		alreadyallocqty = 0;
	}
	nlapiLogExecution('DEBUG','alreadyallocqty',alreadyallocqty);
	var totalallocqty=parseFloat(alreadyallocqty)-parseFloat(allocationQuantity);
	//inventoryTransaction.setFieldValue('custrecord_ebiz_alloc_qty', parseFloat(totalallocqty).toFixed(5));
	inventoryTransaction.setFieldValue('custrecord_ebiz_alloc_qty', parseFloat(totalallocqty));
	inventoryTransaction.setFieldValue('custrecord_ebiz_callinv', 'N');
	inventoryTransaction.setFieldValue('custrecord_ebiz_displayfield', 'N');

	nlapiSubmitRecord(inventoryTransaction,false,true);
	nlapiLogExecution('DEBUG', 'Out of CancelInventoryWithAllocation - Record ID',recordId);

}

function fnDeallocateOpentask(fulfillOrderList,expectedQuantity,eBizWaveNo,pfcontLP,fulfilmentItem)
{
	var filtersOT = new Array();

	filtersOT.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof',fulfilmentItem));
	filtersOT.push(new nlobjSearchFilter('custrecord_expe_qty', null, 'equalto',expectedQuantity));
	filtersOT.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is',pfcontLP));
	filtersOT.push(new nlobjSearchFilter('custrecord_line_no', null, 'is',fulfillOrderList[2]));
	filtersOT.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is','3'));
	filtersOT.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is',eBizWaveNo));


	var lotSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersOT, null);
	if(lotSearchResults != null && lotSearchResults != '')
	{
		if(lotSearchResults[0].getId() != null)
		{
			var fields = new Array();
			var values = new Array();

			fields.push('custrecord_wms_status_flag');
			fields.push('custrecord_notes');
			fields.push('custrecord_batch_no');
			fields.push('custrecord_container_lp_no');
			fields.push('custrecord_lpno');
			fields.push('custrecord_sku_status');
			fields.push('custrecord_totalcube');
			fields.push('custrecord_total_weight');
			fields.push('custrecord_actbeginloc');
			fields.push('custrecord_invref_no');
			fields.push('custrecord_from_lp_no');
			fields.push('custrecord_ebizrule_no');
			fields.push('custrecord_ebizmethod_no');
			fields.push('custrecord_ebiz_clus_no');
			fields.push('custrecord_bin_locgroup_seq ');


			values.push('26');
			values.push('Insufficient Inventory for component items');
			values.push('');
			values.push('');
			values.push('');
			values.push('');
			values.push('');
			values.push('');
			values.push('');
			values.push('');
			values.push('');
			values.push('');
			values.push('');
			values.push('');
			values.push('');

			nlapiSubmitField('customrecord_ebiznet_trn_opentask', lotSearchResults[0].getId(), fields, values);

			nlapiLogExecution('DEBUG', 'updating to failed task',lotSearchResults[0].getId());

		}
	}	
}

function getfromLP(ebizwaveno,item) {
	nlapiLogExecution('DEBUG', 'into getdimsforallitems', '');

	var fromlp;

	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', ebizwaveno));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', '8'));
	if(item!=null && item!='')
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', item));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_lpno');


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	//Case # 20123993 Start.
	nlapiLogExecution('DEBUG', 'searchresults', searchresults);
	if(searchresults!=null && searchresults!='')
	{
		fromlp=searchresults[0].getValue('custrecord_lpno');
	}
	nlapiLogExecution('DEBUG', 'fromlp', fromlp);
	return fromlp;
	//Case # 20123993 End.

}
//Case # 20123993 Start.
function getfromLPfromInv(InvRefNo) {
	nlapiLogExecution('DEBUG', 'into FromLP2', '');

	var fromlp;

	var filters = new Array();
	nlapiLogExecution('DEBUG', 'InvRefNo', InvRefNo);

	var invRec=nlapiLoadRecord('customrecord_ebiznet_createinv',InvRefNo);
	if(invRec != null && invRec!= '')
	{

		fromlp=invRec.getFieldValue('custrecord_ebiz_inv_lp');
	}
	nlapiLogExecution('DEBUG', 'fromlp', fromlp);
	return fromlp;

}
//Case # 20123993 End.

// case# 201417182
function fnGetReplenNo(eBizWaveNo)
{
	var taskColumns = new Array();
	taskColumns[0] = new nlobjSearchColumn('name',null,'group'); 
	var taskFilters = new Array();

	taskFilters[0] = new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', eBizWaveNo);
	taskFilters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', '8');

	var taskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, taskFilters, taskColumns);
	var vReportNo="";
	if(taskSearchResults!= null && taskSearchResults != '')
		vReportNo= taskSearchResults[0].getValue('name',null,'group');
	nlapiLogExecution('ERROR', 'REPORTNO in new func', vReportNo);
	return vReportNo;
}

