/***************************************************************************
                 eBizNET Solutions
 ****************************************************************************
 *
 *  $Source: /cvs/products/NSWMS/EoN/WMS_UI/Reports/Suitelet/Attic/ebiz_InventoryReport_Excel.js,v $
 *  $Revision: 1.1.2.1.4.7.2.1 $
 *  $Date: 2015/11/13 15:39:47 $
 *  $Author: snimmakayala $
 *  $Name: t_WMS_2015_2_StdBundle_1_127 $
 *
 * DESCRIPTION
 *  Functionality
 *
 * REVISION HISTORY
 *  $Log: ebiz_InventoryReport_Excel.js,v $
 *  Revision 1.1.2.1.4.7.2.1  2015/11/13 15:39:47  snimmakayala
 *  201415623
 *
 *  Revision 1.1.2.1.4.4.2.3  2014/11/06 15:16:06  sponnaganti
 *  Case# 201410956
 *  Stnd Bundle Issue fix
 *
 *  Revision 1.1.2.1.4.4.2.2  2014/09/25 15:43:38  grao
 *  Case#: 201410533  Adding Bin Location group field in SportsHq Production
 *
 *  Revision 1.1.2.1.4.4.2.1  2014/07/22 15:57:27  skavuri
 *  Case# 20149571 SB Issue Fixed
 *
 *  Revision 1.1.2.1.4.4  2014/01/16 17:29:20  grao
 *  Case# 201218155  related issue fixes in Sb issue fixes
 *
 *  Revision 1.1.2.1.4.3  2013/11/22 07:15:26  spendyala
 *  CASE201112/CR201113/LOG201121
 *  Issue fixed related to case#20125860
 *
 *  Revision 1.1.2.1.4.2  2013/06/03 14:20:19  schepuri
 *  PCT SB Export to Excel issue
 *
 *  Revision 1.1.2.1.4.1  2013/03/19 12:08:11  schepuri
 *  CASE201112/CR201113/LOG201121
 *  change url path
 *
 *  Revision 1.1.2.1  2013/01/08 14:17:47  spendyala
 *  CASE201112/CR201113/LOG201121
 *  Merged code form 2012.2 branch.
 *
 *
 ****************************************************************************/

function InventoryReportExcel(request, response)
{
	if (request.getMethod() == 'GET') 
	{
		try
		{
			var result;
			var url;
			/*var ctx = nlapiGetContext();
			nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
			if (ctx.getEnvironment() == 'PRODUCTION') 
			{
				url = 'https://system.netsuite.com';			
			}
			else if (ctx.getEnvironment() == 'SANDBOX') 
			{
				url = 'https://system.sandbox.netsuite.com';				
			}*/
			var imageurl = '';
			finalimageurl='';
			nlapiLogExecution('ERROR', 'PDF URL',url);	
			try
			{
				var filefound = nlapiLoadFile('Images/LOGOCOMP.jpg'); 
				if (filefound) 
				{ 
					nlapiLogExecution('ERROR', 'Event', 'file;'+filefound.getId()); 
					imageurl = filefound.getURL();
					nlapiLogExecution('ERROR','imageurl',imageurl);
					finalimageurl = imageurl;//+';';
					//finalimageurl=finalimageurl+ '&expurl=T;';
					nlapiLogExecution('ERROR','imageurl',finalimageurl);
					finalimageurl=finalimageurl.replace(/&/g,"&amp;");
				} 
				else 
				{
					nlapiLogExecution('ERROR', 'Event', 'No file;');
				}
			}
			catch(exp)
			{
				nlapiLogExecution('ERROR', 'Exception in Load File', exp);
			}
			//date
			var sysdate=DateStamp();
			var systime=TimeStamp();
			var Timez=calcTime('-5.00');
			nlapiLogExecution('ERROR','TimeZone',Timez);
			var datetime= new Date();
			datetime=datetime.toLocaleTimeString() ;

			var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<head><macrolist><macro id='myfooter'><p align='center'>Page <pagenumber/> of <totalpages/></p></macro></macrolist></head><body  font-size=\"7\"  size=\"A4-landscape\"  padding-top=\" 0mm\"   footer='myfooter' footer-height='20mm'>\n";

			var replaceChar =/\$|,|@|#|~|`|\%|\*|\^|\&|\+|\=|\-|\_|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|\./g;
			var strxml= "";

			result=InventorySearchResults(request,-1);
			if(result!=null && result!="")
			{
				strxml += "<table width='100%'>";
				strxml += "<tr><td><table width='100%' >";
				strxml += "<tr><td valign='middle' align='left' colspan='2'><img src='" + finalimageurl + "'></img><p align='center' style='font-size:xx-large;'>";
				strxml += "Inventory Report";
				strxml += "</p></td></tr></table></td></tr>";
				strxml += "<tr><td><table width='100%' >";
				strxml += "<tr><td style='width:41px;font-size: 12px;'>";
				strxml += "<p align='right'>Date/Time:"+Timez+"</p>"; 
				strxml += "</td></tr>";
				strxml += "</table>";
				strxml += "</td></tr>";

				strxml += "<tr><td><table width='100%' valign='top' >";
				strxml += "<tr><td>";

				strxml +="<table width='200%'>";
				strxml +="<tr style=\"font-weight:bold;background-color:gray;color:white;\">";

				strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "Item";
				strxml += "</td>";

				strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "Item Description";
				strxml += "</td>";

				strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "Location";
				strxml += "</td>";


				strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "FIFO Date";
				strxml += "</td>";

				strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "Expiry Date";
				strxml += "</td>";


				strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "Pkg#";
				strxml += "</td>";

				strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "Item Status";
				strxml += "</td>";

				strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "Model";
				strxml += "</td>";

				strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "LP #";
				strxml += "</td>";

				strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "LOT#";
				strxml += "</td>";


				strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "Bin Location";
				strxml += "</td>";

				strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "Alloc Qty";
				strxml += "</td>";


				strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "Avail Qty";
				strxml += "</td>";

				strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "Rsv Qty";
				strxml += "</td>";

				strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "Item Info1";
				strxml += "</td>";

				strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "Item Info2";
				strxml += "</td>";


				strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "BinLocation Group";
				strxml += "</td>";

				strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "Zone";
				strxml += "</td>";

				strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "Status Flag";
				strxml += "</td></tr>";

				var sku='', skudesc='', skustatus='', batchno='', fifodate='', expirydate='', modelno='', 
				actendloc='', packcode='', expeqty='', inventorysts='', totalqty='', allocqty='', resvqty='', 
				availqty='', lotbat='', invtLp='', item='', iteminfo1='', iteminfo2='', binloc='', binlocgroup='', binlocgroupvalue='',
				whlocation='',qtyonHand='',statusflag='',zone='';

				var zonecols = new Array();
				zonecols[0] = new nlobjSearchColumn('custrecord_locgroup_no'); 
				zonecols[1]=new nlobjSearchColumn('custrecordcustrecord_putzoneid');
				var zonefilters = new Array();

				if (request.getParameter('custpage_itemzone') != "") { 


					zonefilters[0] = new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'is',request.getParameter('custpage_itemzone'));


				} 


				var zonessearchresult=nlapiSearchRecord('customrecord_zone_locgroup', null, zonefilters, zonecols);

				nlapiLogExecution('ERROR', 'zonessearchresult length', zonessearchresult.length);  

				var index=1;
				var sumtotalqty = 0;
				var sumavailqty = 0;
				var sumallocqty = 0;
				var sumresvqty = 0;
				var sumqtyonHand =0;
				var reportview  = request.getParameter('custpage_reportviewby');
				var InvtListArray=new Array();
				for(k=0;k<result.length;k++)
				{
//					nlapiLogExecution('ERROR', 'orderList[k]', result[k]); 
					var ordsearchresult = result[k];
					if(ordsearchresult!=null)
					{
//						nlapiLogExecution('ERROR', 'ordsearchresult.length ', ordsearchresult.length); 
						for(var j=0;j<ordsearchresult.length;j++)
						{
							InvtListArray[InvtListArray.length]=ordsearchresult[j];				
						}
					}
				}
				for(var j = 0; j < InvtListArray.length; j++)
				{		
//					nlapiLogExecution('ERROR', 'InvtListArray ', InvtListArray[j]); 
//					nlapiLogExecution('ERROR', 'reportview ', reportview); 
					var searchresult = InvtListArray[j];

					if(reportview == "a")
					{
						sku = searchresult.getText('custrecord_ebiz_inv_sku'); //Itemgetvalue
//						nlapiLogExecution('ERROR', 'sku ', sku); 
//						nlapiLogExecution('ERROR', 'SKU', searchresult.getValue('custrecord_ebiz_sku_no'));
						skudesc = searchresult.getValue('custrecord_ebiz_itemdesc'); // Item Desc
						skustatus = searchresult.getText('custrecord_ebiz_inv_sku_status'); //Item Status
						batchno = searchresult.getValue('custrecord_ebiz_inv_lot'); //Batch no
						fifodate = searchresult.getValue('custrecord_ebiz_inv_fifo'); //FIFO Date
						expirydate = searchresult.getValue('custrecord_ebiz_expdate');//Expiry Date
						modelno = searchresult.getValue('custrecord_ebiz_model'); //Model no
						actendloc = searchresult.getText('custrecord_ebiz_inv_binloc'); //Bin location
						packcode = searchresult.getText('custrecord_ebiz_inv_packcode');//Packcode
						totalqty = searchresult.getValue('custrecord_ebiz_inv_qty'); //Total Qty
						availqty = searchresult.getValue('custrecord_ebiz_avl_qty'); //Avail Qty
						qtyonHand = searchresult.getValue('custrecord_ebiz_qoh'); //Qty on Hand
						allocqty = searchresult.getValue('custrecord_ebiz_alloc_qty'); //Total Qty
						resvqty = searchresult.getValue('custrecord_ebiz_resinventory'); //Resv Qty
						invtLp = searchresult.getValue('custrecord_ebiz_inv_lp');
						lotbat = searchresult.getText('custrecord_ebiz_inv_lot');
						iteminfo1 = searchresult.getText('custitem_item_info_1','custrecord_ebiz_inv_sku');
						iteminfo2 = searchresult.getText('custitem_item_info_2','custrecord_ebiz_inv_sku');
						binlocgroup = searchresult.getText('custrecord_inboundlocgroupid','custrecord_ebiz_inv_binloc');
						binlocgroupvalue = searchresult.getValue('custrecord_inboundlocgroupid','custrecord_ebiz_inv_binloc');
						statusflag = searchresult.getText('custrecord_wms_inv_status_flag');
						whlocation = searchresult.getText('custrecord_ebiz_inv_loc');
					}
					else if(reportview == "b")
					{
						sku = searchresult.getText('custrecord_ebiz_inv_sku', null, 'group'); //Itemgetvalue 
						//nlapiLogExecution('ERROR', 'SKU', searchresult.getValue('custrecord_ebiz_sku_no'));
						skudesc = searchresult.getValue('custrecord_ebiz_itemdesc', null, 'group'); // Item Desc
						skustatus = searchresult.getText('custrecord_ebiz_inv_sku_status', null, 'group'); //Item Status
						batchno = searchresult.getValue('custrecord_ebiz_inv_lot', null, 'group'); //Batch no
						fifodate = searchresult.getValue('custrecord_ebiz_inv_fifo', null, 'group'); //FIFO Date
						expirydate = searchresult.getValue('custrecord_ebiz_expdate', null, 'group');//Expiry Date
						modelno = '';//searchresult.getValue('custrecord_ebiz_model', null, 'group'); //Model no
						actendloc ='';// searchresult.getText('custrecord_ebiz_inv_binloc', null, 'group'); //Bin location
						packcode = searchresult.getText('custrecord_ebiz_inv_packcode', null, 'group');//Packcode
						totalqty = searchresult.getValue('custrecord_ebiz_inv_qty', null, 'group'); //Total Qty
						qtyonHand = searchresult.getValue('custrecord_ebiz_qoh', null, 'sum'); //Qty on Hand
						availqty = searchresult.getValue('custrecord_ebiz_avl_qty', null, 'sum'); //Avail Qty
						allocqty = searchresult.getValue('custrecord_ebiz_alloc_qty', null, 'sum'); //Total Qty
						resvqty = searchresult.getValue('custrecord_ebiz_resinventory', null, 'sum'); //Resv Qty
						invtLp = '';//searchresult.getValue('custrecord_ebiz_inv_lp', null, 'group');
						lotbat = searchresult.getText('custrecord_ebiz_inv_lot', null, 'group');
						iteminfo1 = '';//searchresult.getValue('custitem_item_info_1','custrecord_ebiz_inv_sku', null, 'group');
						iteminfo2 = '';//searchresult.getValue('custitem_item_info_2','custrecord_ebiz_inv_sku', null, 'group');
						binlocgroup = '';//searchresult.getText('custrecord_inboundlocgroupid','custrecord_ebiz_inv_binloc', null, 'group');
						binlocgroupvalue = '';//searchresult.getValue('custrecord_inboundlocgroupid','custrecord_ebiz_inv_binloc', null, 'group');
						statusflag = searchresult.getText('custrecord_wms_inv_status_flag', null, 'group');
						whlocation = searchresult.getText('custrecord_ebiz_inv_loc', null, 'group');
					}

					/*var zonesarray=new Array(); 
					var elementfound =false;
					for (var m = 0; zonessearchresult!= null && m < zonessearchresult.length; m++) 
					{
						if (zonessearchresult[m].getText('custrecord_locgroup_no')== binlocgroup)
						{                
							elementfound=true;
							if (zone=='')
							{

								zone = zone+zonessearchresult[m].getText('custrecordcustrecord_putzoneid')+',';
								zonesarray[m]=zone.slice(0, zone.lastIndexOf(','));

							}
							else
							{
								var tempZone=zonessearchresult[m].getText('custrecordcustrecord_putzoneid');

								if(findIndex(tempZone,zonesarray)==-1)
								{
									zone = zone+zonessearchresult[m].getText('custrecordcustrecord_putzoneid')+',';
									zonesarray[m]=tempZone;
								}

							}
						}                       
					}             	

					zone = zone.slice(0, zone.lastIndexOf(','));*/


					var zonesarray=new Array(); 
					var elementfound =false;
					for (var m = 0; zonessearchresult!= null && m < zonessearchresult.length; m++) 
					{
						if (zonessearchresult[m].getText('custrecord_locgroup_no')== binlocgroup)
						{
							zone = zonessearchresult[m].getText('custrecordcustrecord_putzoneid');
							zonesarray.push(zone);							

						}                       
					}

					var distinctzones = removeDuplicateElement(zonesarray);
					var vzonetext='',vtempzonetext='';
					for(var p1 = 0; p1<distinctzones.length;p1++)
					{
						vzonetext = distinctzones[p1];
						if (vtempzonetext=='')
						{							
							vtempzonetext = vzonetext;
						}
						else
						{
							vtempzonetext = vtempzonetext + ',' + vzonetext;
						}
					}

					nlapiLogExecution('ERROR','vtempzonetext',vtempzonetext);

					if(sku== null || sku =="")
						sku="";

					if(skudesc== null || skudesc =="")	
						skudesc="&nbsp;";

					if(skustatus== null || skustatus =="")
						skustatus="&nbsp;";

					if(batchno== null || batchno =="")
						batchno="&nbsp;";

					if(fifodate== null || fifodate =="")
						fifodate="&nbsp;";

					if(expirydate== null || expirydate =="")
						expirydate="&nbsp;";		

					if(modelno== null || modelno =="")
						modelno="&nbsp;";

					if(actendloc== null || actendloc =="")
						actendloc="&nbsp;";

					if(packcode== null || packcode =="")
						packcode="&nbsp;";	

					if(totalqty== null || totalqty =="")
						totalqty="";

					if(qtyonHand== null || qtyonHand =="")	
						qtyonHand="&nbsp;";

					if(availqty== null || availqty =="")
						availqty="&nbsp;";

					if(allocqty== null || allocqty =="")
						allocqty="&nbsp;";

					if(resvqty== null || resvqty =="")
						resvqty="&nbsp;";

					if(invtLp== null || invtLp =="")
						invtLp="&nbsp;";		

					if(lotbat== null || lotbat =="")
						lotbat="&nbsp;";	

					if(iteminfo1== null || iteminfo1 =="")
						iteminfo1="&nbsp;";

					if(iteminfo2== null || iteminfo2 =="")
						iteminfo2="&nbsp;";		

					if(binlocgroup== null || binlocgroup =="")
						binlocgroup="&nbsp;";	

					if(binlocgroupvalue== null || binlocgroupvalue =="")
						binlocgroupvalue="&nbsp;";	

					if(statusflag== null || statusflag =="")
						statusflag="&nbsp;";	

					if(whlocation== null || whlocation =="")
						whlocation="&nbsp;";

					if(zone== null || zone =="")
						zone="&nbsp;";


					//strxml += "<table width='100%' height='100%'>";
					strxml += "<tr>";
					strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += sku;
					strxml += "</td>";

					strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += skudesc;
					strxml += "</td>";

					strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += whlocation;
					strxml += "</td>";

					strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += fifodate;
					strxml += "</td>";

					strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += expirydate;
					strxml += "</td>";

					strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += packcode;
					strxml += "</td>";

					strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += skustatus;
					strxml += "</td>";

					strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += modelno;
					strxml += "</td>";

					strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += invtLp;
					strxml += "</td>";

					strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += lotbat;
					strxml += "</td>";

					strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += actendloc;
					strxml += "</td>";

					strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += allocqty;
					strxml += "</td>";

					strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += availqty;
					strxml += "</td>";

					strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += resvqty;
					strxml += "</td>";

					strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += iteminfo1;
					strxml += "</td>";

					strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += iteminfo2;
					strxml += "</td>";

					strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += binlocgroup;
					strxml += "</td>";


					strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += zone;
					strxml += "</td>";

					strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += statusflag;
					strxml += "</td>";

					strxml += "</tr>";
				}
				strxml += "</table>";
				strxml += "</td></tr>";
				strxml += "</table>";
				strxml += "</td></tr>";
				strxml += "</table>"; 
				strxml += "\n</body>\n</pdf>";
				xml=xml+strxml;
				var exportXML = strxml;
				response.setContentType('PLAINTEXT', 'ExcelResults.xls', 'attachment');
				response.write(exportXML);	
			}
			}
			catch(exp)
			{
				nlapiLogExecution('ERROR','Exception in Inventory report Excel Main',exp);
			}
	}
	else
	{

	}
}

var tempOTResultsArray=new Array();
function InventorySearchResults(request,max)
{
	try
	{
		nlapiLogExecution('ERROR','max',max);
		var filters = new Array();
		var i = 0; var lpname="";   

		var reportview  = request.getParameter('custpage_reportviewby');

		if(request.getParameter('custpage_lp') !=null && request.getParameter('custpage_lp') !=""){
			var fields = ['custrecord_ebiz_lpmaster_lp'];
			var columns = nlapiLookupField('customrecord_ebiznet_master_lp', request.getParameter('custpage_lp'), fields);
			lpname = columns.custrecord_ebiz_lpmaster_lp;
		}

		nlapiLogExecution('ERROR','lpname',lpname);

		nlapiLogExecution('ERROR','Value of Inventory Flag.',request.getParameter('custpage_inventorysts'));
		var invStatus=request.getParameter('custpage_inventorysts');
		if (request.getParameter('custpage_inventorysts') != "") {
			nlapiLogExecution('ERROR','invStatus',invStatus);
			if(invStatus == '4')
			{
				filters[i] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['3','19']);
			}
			else if(invStatus == '3')
			{
				filters[i] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['18']);
			}  
			else if(invStatus == '2')      			
			{
				filters[i] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['1','17']);
			}
			else if(invStatus == '1')      			
			{
				filters[i] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['1','3','17','18','19']);
			}
			i++;

		}

		if (request.getParameter('custpage_itemfamily') != null && request.getParameter('custpage_itemfamily') != "") {
			nlapiLogExecution('ERROR','itemFamily',request.getParameter('custpage_itemfamily'));
			filters[i] = new nlobjSearchFilter('custitem_item_family', 'custrecord_ebiz_inv_sku', 'is', request.getParameter('custpage_itemfamily'));
			i++;
		}

		if (request.getParameter('custpage_itemgroup') != null && request.getParameter('custpage_itemgroup') != "") {
			filters[i] = new nlobjSearchFilter('custitem_item_group', 'custrecord_ebiz_inv_sku', 'is', request.getParameter('custpage_itemgroup'));
			i++;
		}

		if (request.getParameter('custpage_item') != null && request.getParameter('custpage_item') != "") {
			nlapiLogExecution('ERROR','custpage_item',request.getParameter('custpage_item'));
			filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', request.getParameter('custpage_item'));
			i++;
		}

		if (request.getParameter('custpage_itemstatus') != null && request.getParameter('custpage_itemstatus') != "") {
			nlapiLogExecution('ERROR','SKUSTATUS',request.getParameter('custpage_itemstatus'));
			filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', request.getParameter('custpage_itemstatus'));
			i++;
		}

		if (lpname != "" && lpname !=null) {

			filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', lpname);
			i++;
		}
		if (request.getParameter('custpage_lotbatch') != null && request.getParameter('custpage_lotbatch') != "") {
			filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'is', request.getParameter('custpage_lotbatch'));
			i++;
		}
		if (request.getParameter('custpage_binlocation') != null && request.getParameter('custpage_binlocation') != "") {
			filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', request.getParameter('custpage_binlocation'));
			i++;
		}
		nlapiLogExecution('ERROR','Location Group',request.getParameter('custpage_binlocgroup'));
		//if (request.getParameter('custpage_binlocgroup') != "") {
		if (request.getParameter('custpage_binlocgroup') != "" && request.getParameter('custpage_binlocgroup') != null) {
			filters[i] = new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'anyof', request.getParameter('custpage_binlocgroup'));
			//filters[i] = new nlobjSearchFilter('custrecord_inboundinvlocgroupid', 'anyof', request.getParameter('custpage_binlocgroup'));
			i++;
		}
		if(max!=-1)
		{
			filters.push(new nlobjSearchFilter('id', null, 'greaterthan', max));
		}

		/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh as part of Standard bundle***/
		var vRoleLocation=getRoledBasedLocation();
		nlapiLogExecution('DEBUG','vRoleLocation',vRoleLocation);
		nlapiLogExecution('DEBUG','request.getParameter(custpage_loc_id)',request.getParameter('custpage_loc_id'));

		//if(vRoleLocation!=""&&vRoleLocation!=null)

		/*if(vRoleLocation != "" && vRoleLocation != null && vRoleLocation != 0) //Case# 20149571
		{
			if(request.getParameter('custpage_loc_id') ==null || request.getParameter('custpage_loc_id')=='')
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',vRoleLocation));
			else
			{
				nlapiLogExecution('DEBUG','index',vRoleLocation.indexOf(request.getParameter('custpage_loc_id')));
				if(vRoleLocation.indexOf(request.getParameter('custpage_loc_id'))!=-1)
					filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',request.getParameter('custpage_loc_id')));
				else
					filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',["@NONE@"]));
			}
		}
		else
		{
			if(request.getParameter('custpage_loc_id') !=null && request.getParameter('custpage_loc_id') !='')
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',request.getParameter('custpage_loc_id')));
		}*/

		if(request.getParameter('custpage_loc_id') !=null && request.getParameter('custpage_loc_id') !='')
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',request.getParameter('custpage_loc_id')));


		//upto here
		filters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0));

		var columns = new Array();
		if( reportview== "a"){

			columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku'); //Item
			columns[1] = new nlobjSearchColumn('custrecord_ebiz_itemdesc'); // Item Desc
			columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_sku_status'); //Item Status
			columns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_lot'); //Batch no
			columns[4] = new nlobjSearchColumn('custrecord_ebiz_inv_fifo'); //FIFO Date
			columns[5] = new nlobjSearchColumn('custrecord_ebiz_expdate');//Expiry Date
			columns[6] = new nlobjSearchColumn('custrecord_ebiz_model'); //Model no
			columns[7] = new nlobjSearchColumn('custrecord_ebiz_inv_packcode');//Packcode
			columns[8] = new nlobjSearchColumn('custrecord_ebiz_inv_qty'); //Total Qty
			columns[9] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty'); //Allocated Qty
			columns[10] = new nlobjSearchColumn('custrecord_ebiz_qoh'); //Avail Qty
			columns[11] = new nlobjSearchColumn('custrecord_ebiz_resinventory'); //Resv Qty
			columns[12] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');//invt Lp
			columns[13] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
			columns[14] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');       
			columns[15] = new nlobjSearchColumn('custitem_item_info_1','custrecord_ebiz_inv_sku');
			columns[16] = new nlobjSearchColumn('custitem_item_info_2','custrecord_ebiz_inv_sku'); 
			columns[17] = new nlobjSearchColumn('custrecord_inboundlocgroupid','custrecord_ebiz_inv_binloc');
			columns[18] = new nlobjSearchColumn('internalid');
			columns[18].setSort();
			columns[19] = new nlobjSearchColumn('custrecord_wms_inv_status_flag');
			columns[20] = new nlobjSearchColumn('custrecord_ebiz_inv_loc');
			columns[21] = new nlobjSearchColumn('custrecord_ebiz_avl_qty'); //Avail Qty
		}
		else if(reportview == "b"){

			columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku', null, 'group');  //Item
			columns[1] = new nlobjSearchColumn('custrecord_ebiz_itemdesc', null, 'group');  // Item Desc
			columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_sku_status', null, 'group');  //Item Status
			columns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_lot', null, 'group');  //Batch no
			columns[4] = new nlobjSearchColumn('custrecord_ebiz_inv_fifo', null, 'group');  //FIFO Date
			columns[5] = new nlobjSearchColumn('custrecord_ebiz_expdate', null, 'group'); //Expiry Date
			columns[6] = new nlobjSearchColumn('custrecord_ebiz_inv_packcode', null, 'group'); //Packcode
			columns[7] = new nlobjSearchColumn('custrecord_ebiz_inv_qty', null, 'group');  //Total Qty
			columns[8] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty', null, 'sum'); ; //Allocated Qty
			columns[9] = new nlobjSearchColumn('custrecord_ebiz_qoh', null, 'sum');  //Avail Qty
			columns[10] = new nlobjSearchColumn('custrecord_ebiz_resinventory', null, 'sum');  //Resv Qty
			columns[11] = new nlobjSearchColumn('custrecord_ebiz_inv_lot', null, 'group');        
			columns[12] = new nlobjSearchColumn('internalid');
			columns[12].setSort();
			columns[13] = new nlobjSearchColumn('custrecord_wms_inv_status_flag', null, 'group');
			columns[14] = new nlobjSearchColumn('custrecord_ebiz_inv_loc', null, 'group');
			columns[15] = new nlobjSearchColumn('custrecord_ebiz_avl_qty', null, 'sum');  //Avail Qty

		}
		// execute the  search by passing the parameters as filters and getting the output columns by providing coluns as paramerts


		var searchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);	
		var test='';
		var statusflag='';
		nlapiLogExecution('ERROR','searchresults',searchresults);
		if(searchresults!=null)
		{
			nlapiLogExecution('ERROR','searchresults',searchresults.length);
			if(searchresults.length>=1000)
			{
				nlapiLogExecution('ERROR','searchresults','more than 1000');
				if( reportview== "a"){
					var maxno=searchresults[searchresults.length-1].getValue(columns[18]);
				}
				else if(reportview == "b"){
					var maxno=searchresults[searchresults.length-1].getValue(columns[12]);	
				}
				tempOTResultsArray.push(searchresults);
				InventorySearchResults(request,maxno);

			}
			else
			{
				tempOTResultsArray.push(searchresults);
			}
		}
		return tempOTResultsArray;
	}
	catch(Exp)
	{
		nlapiLogExecution('ERROR','Exception in InventorySearchResults',Exp);
	}
}

function TimeStamp(){
	var timestamp;
	var now = new Date();
	var a_p = "";

	//Getting time in hh:mm tt format.
	var curr_hour = now.getHours();
	var curr_min = now.getMinutes();

	// determining the am/pm indicator
	if (curr_hour < 12)
		a_p = "am";
	else
		a_p = "pm";

	// finalizing hours depending on 24hr clock
	if (curr_hour == 0)
		curr_hour = 12;
	else if(curr_hour > 12)
		curr_hour -= 12;

	if (curr_min.length == 1)
		curr_min = "0" + curr_min;

	//Adding fields to update time zones.
	timestamp = curr_hour + ":" + curr_min + " " + a_p;

	return timestamp;
}

function DateStamp(){
	var now = new Date();

	var dtsettingFlag = DateSetting();

	nlapiLogExecution('ERROR', 'dtsettingFlag', dtsettingFlag);
	if(dtsettingFlag == 'DD/MM/YYYY')
	{
		return ((parseInt(now.getDate())) + '/' + (parseInt(now.getMonth()) + 1) + '/' +now.getFullYear());
	}
	else
	{
		return ((parseInt(now.getMonth()) + 1) + '/' + (parseInt(now.getDate())) + '/' + now.getFullYear());
	}
	//
}

function DateSetting()
{
	/*var flag = 'DD';
	var ctx = nlapiGetContext();
	 var setpreferencesdateformate = ctx.getPreference('USER_DATEFORMAT');

	 nlapiLogExecution('ERROR', 'setpreferencesdateformate', setpreferencesdateformate);

	 vPOoverageChecked='';
	  var vConfig=nlapiLoadConfiguration('companypreferences');
	 if(vConfig != null && vConfig != '')
	 {
	  vPOoverageChecked=vConfig.getFieldValue('DATEFORMAT');
	 }*/
	var ctx = nlapiGetContext();
	var setpreferencesdateformate = ctx.getPreference('DATEFORMAT');

	return setpreferencesdateformate;

}

function calcTime(offset) {

	//var vtime=new Date();

	//nlapiLogExecution('ERROR', 'TimeOffset', vtime.getTimezoneOffset());

	// create Date object for current location
	d = new Date();

	// convert to msec
	// add local time zone offset 
	// get UTC time in msec
	utc = d.getTime() + (d.getTimezoneOffset() * 60000);

	// create new Date object for different city
	// using supplied offset
	nd = new Date(utc + (3600000*offset));

	// return time as a string
	//return "The local time in " + city + " is " + nd.toLocaleString();

	var stringDt=((parseInt(nd.getMonth()) + 1) + '/' + (parseInt(nd.getDate())) + '/' + nd.getFullYear());

	var timestamp;
	var a_p = "";

	//Getting time in hh:mm tt format.
	var curr_hour = nd.getHours();
	var curr_min = nd.getMinutes();

	// determining the am/pm indicator
	if (curr_hour < 12)
		a_p = "am";
	else
		a_p = "pm";

	// finalizing hours depending on 24hr clock
	if (curr_hour == 0)
		curr_hour = 12;
	else if(curr_hour > 12)
		curr_hour -= 12;

	if (curr_min.length == 1)
		curr_min = "0" + curr_min;

	//Adding fields to update time zones.
	timestamp = curr_hour + ":" + curr_min + " " + a_p;

	return stringDt + " " + timestamp;

}


function findIndex(value,testarray){
	var ctr = -1;
	for (var i=0; i < testarray.length; i++) {
		// use === to check for Matches. ie., identical (===), ;
		if (testarray[i] == value) {
			return i;
		}
	}
	return ctr;
};
