/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_LoadTrailer_SL.js,v $
 *     	   $Revision: 1.17.2.9.4.6.2.10.2.1 $
 *     	   $Date: 2014/11/05 15:43:33 $
 *     	   $Author: schepuri $
 *     	   $Name: t_eBN_2014_2_StdBundle_0_160 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_LoadTrailer_SL.js,v $
 * Revision 1.17.2.9.4.6.2.10.2.1  2014/11/05 15:43:33  schepuri
 * 201410439 issue fix
 *
 * Revision 1.17.2.9.4.6.2.10  2014/06/16 06:42:10  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148886
 *
 * Revision 1.17.2.9.4.6.2.9  2014/06/11 15:33:49  skavuri
 * Case# 20148822 SB Issue Fixed
 *
 * Revision 1.17.2.9.4.6.2.8  2013/12/12 16:42:18  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * prodissue fixes
 *
 * Revision 1.17.2.9.4.6.2.7  2013/08/26 15:31:41  rmukkera
 * Case#  20123991
 *
 * Revision 1.17.2.9.4.6.2.6  2013/07/10 14:11:35  mbpragada
 * Case# 20123375
 * records not displaying in Load trailer screen
 *
 * Revision 1.17.2.9.4.6.2.5  2013/07/10 14:04:48  mbpragada
 * Case# 20123375
 * records not displaying in Load trailer screen
 *
 * Revision 1.17.2.9.4.6.2.4  2013/06/14 16:00:58  nneelam
 * CASE201112/CR201113/LOG201121
 * Standard Bundle, BuildShip Units:Issue No. 20122622
 *
 * Revision 1.17.2.9.4.6.2.3  2013/04/09 13:21:16  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related to display no records
 *
 * Revision 1.17.2.9.4.6.2.2  2013/04/03 01:49:28  kavitha
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.17.2.9.4.6.2.1  2013/03/19 11:53:20  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.17.2.9.4.6  2013/02/21 14:32:53  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.17.2.9.4.5  2013/02/21 07:20:54  grao
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.17.2.9.4.4  2012/12/04 16:33:45  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA UAT Fixes.
 *
 * Revision 1.17.2.9.4.3  2012/11/07 06:31:38  mbpragada
 * 20120490
 *
 * Revision 1.17.2.9.4.2  2012/11/01 14:55:02  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.17.2.9.4.1  2012/09/24 22:45:58  spendyala
 * CASE201112/CR201113/LOG201121
 * check if any open picks present for the wave/order.
 * if yes promt a msg to user or else continue with process.
 *
 * Revision 1.17.2.9  2012/08/26 14:21:18  spendyala
 * CASE201112/CR201113/LOG201121
 * Mater Tracking# will not be overwritten if user had enter while creating Trailer.
 *
 * Revision 1.17.2.8  2012/07/23 06:30:17  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Dynacraft UAT issue fixes
 *
 * Revision 1.17.2.7  2012/07/04 07:50:29  gkalla
 * CASE201112/CR201113/LOG201121
 * Fine tuned
 *
 * Revision 1.17.2.5  2012/05/23 14:16:17  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.17.2.4  2012/05/15 06:04:42  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.17.2.3  2012/04/20 14:10:45  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.17.2.2  2012/04/10 14:52:01  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing
 *
 * Revision 1.17.2.1  2012/01/31 13:18:21  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing related to checkbox to select atleast one line
 *
 * Revision 1.17  2011/11/23 11:15:27  schepuri
 * CASE201112/CR201113/LOG201121
 * splitting load and depart trailer functionality
 *
 * Revision 1.16  2011/11/11 17:20:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Inventory Deletion
 *
 * Revision 1.15  2011/10/05 12:54:24  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.14  2011/10/04 13:49:13  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.13  2011/10/04 12:16:34  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.12  2011/10/03 14:43:05  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.11  2011/09/26 10:49:44  mbpragada
 * CASE201112/CR201113/LOG201121
 * Load/Unload Trailer screen level Issue Fixes
 *
 * Revision 1.10  2011/09/26 08:45:52  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.9  2011/09/24 13:28:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.8  2011/09/06 09:31:39  mbpragada
 * CASE201112/CR201113/LOG201121
 * Completed Load/Unload Trailer Logic.
 *
 * Revision 1.7  2011/09/06 06:58:32  mbpragada
 * CASE201112/CR201113/LOG201121
 * Completed Load/Unload Trailer Logic.
 *
 * Revision 1.6  2011/08/31 11:10:29  mbpragada
 * CASE201112/CR201113/LOG201121 
 * Completed Load/Unload Trailer Logic.
 *
 * Revision 1.5  2011/08/31 10:36:05  mbpragada
 * CASE201112/CR201113/LOG201121
 * Completed Load/Unload Trailer Logic.
 *
 * Revision 1.4  2011/07/21 05:05:53  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *
 *****************************************************************************/
/**
 * This is the main function which performs the complete Trailer Loading process.
 * @param request
 * @param response
 */
function filltrailer(form, trailer,location){
	trailer.addSelectOption("", "");

	if(location != null && location != '')
	{
		var Filers = new Array();
		Filers.push(new nlobjSearchFilter('custrecord_ebizsitetrailer', null, 'anyof', location)); //Ship task
		//Filers.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_wmsstatusflag', null, 'anyof', ['7','28'])); //Status 'B'


		var SearchResults = nlapiSearchRecord('customrecord_ebiznet_trailer', null, Filers, new nlobjSearchColumn('name'));

		for (var i = 0; SearchResults != null && i < SearchResults.length; i++) {
			var resdo = form.getField('custpage_trailer').getSelectOptions(SearchResults[i].getValue('name'), 'is');
			if (resdo != null) {
				if (resdo.length > 0) {
					continue;
				}
			}
			trailer.addSelectOption(SearchResults[i].getId(), SearchResults[i].getValue('name'));
		}
	}
}
function LoadTrailerSuitelet(request, response){
	if (request.getMethod() == 'GET'){
		var form = nlapiCreateForm('Load Trailer');
		var waveNumber = "";
		var fulfillmentOrderNumber = "";
		var shipLPNumber = "",Route='',Destination='',Carrier='';

		//code added to check weather the given wave/order has any open pickes or not
		//added by suman on 310812.
		var results=IsOpenPicksPresent(request.getParameter('custpage_wave'),request.getParameter('custpage_fulfillmentorder'),request.getParameter('custpage_carrier'),request.getParameter('custpage_shiplp'));
		if(results=='T')
		{
			nlapiLogExecution('ERROR','Request cant be processed as there are still Open Picks for the Wave');
			dataNotFoundMsg="Request cant be processed as there are still Open Picks for the Wave";
			showInlineMessage(form, 'Error', dataNotFoundMsg, "");
			response.writePage(form);
		}
		else{
			//end of code as of 31012.
			if (request.getParameter('custpage_wave')!=null && request.getParameter('custpage_wave')!="" ){
				waveNumber = request.getParameter('custpage_wave');
				var wave=form.addField('custpage_wave', 'text', 'Wave').setDisplayType('hidden');	
				wave.setDefaultValue(waveNumber);
			}

			if (request.getParameter('custpage_fulfillmentorder')!=null && request.getParameter('custpage_fulfillmentorder')!="" ){
				fulfillmentOrderNumber = request.getParameter('custpage_fulfillmentorder');
			}

			if (request.getParameter('custpage_shiplp') != null && request.getParameter('custpage_shiplp') != "") {
				shipLPNumber = request.getParameter('custpage_shiplp');
			}

			if (request.getParameter('custpage_carrier') != null && request.getParameter('custpage_carrier') != "") {
				Carrier = request.getParameter('custpage_carrier');
			}
			if (request.getParameter('custpage_dest') != null && request.getParameter('custpage_dest') != "") {
				Destination = request.getParameter('custpage_dest');
			}
			if (request.getParameter('custpage_route') != null && request.getParameter('custpage_route') != "") {
				Route = request.getParameter('custpage_route');
			}
            nlapiLogExecution('ERROR','waveNumber',waveNumber);
			nlapiLogExecution('ERROR','fulfillmentOrderNumber',fulfillmentOrderNumber);
			nlapiLogExecution('ERROR','shipLPNumber',shipLPNumber);
			nlapiLogExecution('ERROR','Carrier',Carrier);

			var Filters = new Array();
			var columns=new Array();
			var location = '';
			if((waveNumber  != null && waveNumber != '') ||  (fulfillmentOrderNumber  != null && fulfillmentOrderNumber != '') || (shipLPNumber  != null && shipLPNumber != ''))
			{

				if(waveNumber  != null && waveNumber != '')
					Filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', waveNumber));
				if(fulfillmentOrderNumber  != null && fulfillmentOrderNumber != '')
					Filters.push(new nlobjSearchFilter('name', null, 'is', fulfillmentOrderNumber));
				if(shipLPNumber  != null && shipLPNumber != '')
					Filters.push(new nlobjSearchFilter('custrecord_ship_lp_no', null, 'is', shipLPNumber));

				Filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3')); //Ship task
				Filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['7','28'])); //status 'B'


				columns[0]=new nlobjSearchColumn('custrecord_wms_location');
				columns[1]=new nlobjSearchColumn('id');
				columns[1].setSort(true);
				var SearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, Filters, columns);
				if(SearchResults != null && SearchResults != '')
				{
					location = SearchResults[0].getValue('custrecord_wms_location');
				}
			}
			else if(Carrier != null && Carrier != '')
			{
				Filters.push(new nlobjSearchFilter('custrecord_do_carrier', null, 'anyof', Carrier));

				//Filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3')); //Ship task
				//Filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['7','28'])); //status 'B'

				var columns=new Array();
				columns[0]=new nlobjSearchColumn('custrecord_ordline_wms_location');
				columns[1]=new nlobjSearchColumn('id');
				columns[1].setSort(true);
				var SearchResults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, Filters, columns);
				if(SearchResults != null && SearchResults != '')
				{
					location = SearchResults[0].getValue('custrecord_ordline_wms_location');
				}
			}

			//var trailer = form.addField('custpage_trailer', 'select', 'Trailer','customrecord_ebiznet_trailer');

			var trailer = form.addField('custpage_trailer', 'select', 'Trailer');
			trailer.setLayoutType('startrow', 'none');
			filltrailer(form, trailer,location);

			//var trailer = form.addField('custpage_trailer', 'select', 'Trailer','customrecord_ebiznet_trailer');
		//	trailer.setLayoutType('startrow', 'none');

			createLoadTrailerSubList(form);
			var filters = specifyLoadTrailerFilters(waveNumber,fulfillmentOrderNumber,shipLPNumber,Carrier);
			var columns = specifyLoadTrailerColumns();
			var searchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
			nlapiLogExecution('ERROR','searchResults',searchResults);
			var Salesord;
			if(searchResults != null){
				for (var i = 0; i < searchResults.length; i++) {

					resultsetShipLP = searchResults[i].getValue('custrecord_ship_lp_no',null,'group');
//					var Filers = new Array;
//					var Columns = new Array();

//					//Commented by Satish.N on 06/18/2012

////					Filers.push(new nlobjSearchFilter('custrecord_ebiztask_ship_lp_no', null, 'is', resultsetShipLP)); 			
////					Columns[0] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_order_no');				
////					var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, Filers, Columns);

//					Filers.push(new nlobjSearchFilter('custrecord_ship_lp_no', null, 'is', resultsetShipLP)); 			
//					Columns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');				
//					var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, Filers, Columns);

//					var Salesord,closedtasktrailername;
//					if(SOSearchResults != "" || SOSearchResults != null){
//					Salesord=SOSearchResults[0].getValue('custrecord_ebiz_order_no');
//					//closedtasktrailername=SOSearchResults[0].getValue('custrecord_ebiztask_ebiz_trailer_no');
//					nlapiLogExecution('ERROR', "Salesordp", Salesord);			 
//					}
					Salesord=searchResults[i].getValue('internalid','custrecord_ebiz_order_no','group');
					if(blnShipComplete(Salesord))
					{
						nlapiLogExecution('ERROR', "Salesordpshipcompletetrue", '');	
						//filters to display only completed build ship units to load whose ship complete flag is true
						var Filers = new Array;
						//CASE# 20123375 -Start
						Filers.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', Salesord)); 
						//Filers.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', resultsetShipLP));
						//CASE# 20123375 -End
						Filers.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); 
						Filers.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['28'])); 

						var Columns = new Array();
						//Columns[0] = new nlobjSearchColumn('custrecord_ship_lp_no');
						Columns[0] = new nlobjSearchColumn('custrecord_container_lp_no');

						var BuildShipUnitcompleteSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, Filers, Columns);
						if(BuildShipUnitcompleteSearchResults == "" || BuildShipUnitcompleteSearchResults == null)
						{
							nlapiLogExecution('ERROR', "BuildShipUnitcompleteSearchResults is empty", 'ttt');	
							//filters to display only closed build ship units for the above completed build ship units to load whose ship complete flag is true
							var BuildShipUnitclosedFilers = new Array;
							BuildShipUnitclosedFilers.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', Salesord)); 
							BuildShipUnitclosedFilers.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['4'])); 
							BuildShipUnitclosedFilers.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['7'])); 

							var BuildShipUnitclosedColumns = new Array();
							BuildShipUnitclosedColumns[0] = new nlobjSearchColumn('custrecord_ship_lp_no');


							var BuildShipUnitclosedSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, BuildShipUnitclosedFilers, BuildShipUnitclosedColumns);

							if(BuildShipUnitclosedSearchResults == "" || BuildShipUnitclosedSearchResults == null)
							{
								nlapiLogExecution('ERROR', "BuildShipUnitclosedSearchResults is empty", 'eee');	
								currentRecord = searchResults[i];
								addLoadTrailerDetailsToSubList(form, currentRecord, parseInt(i));
							}
							else
							{
								for (var k = 0; k < BuildShipUnitclosedSearchResults.length; k++) 
								{
									nlapiLogExecution('ERROR', "BuildShipUnitclosedSearchResults",BuildShipUnitclosedSearchResults.length );		
									var shiplp = BuildShipUnitclosedSearchResults[k].getValue('custrecord_ship_lp_no');

								}
							}

						}
						else
						{
							for (var k = 0; k < BuildShipUnitcompleteSearchResults.length; k++) 
							{
								//nlapiLogExecution('ERROR', "BuildShipUnitcompleteSearchResults",BuildShipUnitcompleteSearchResults.length );		
								var containerlp = BuildShipUnitcompleteSearchResults[k].getValue('custrecord_container_lp_no');

							}
						}



					}
					else
					{
						nlapiLogExecution('ERROR', "shipcomplteflagfalse is empty", '');	
						currentRecord = searchResults[i];
						IndividualShipmanfDetList = GetShipmanifestDetails(currentRecord.getValue('custrecord_ship_lp_no',null,'group'));
						//IndividualShipmanfDetList = GetShipmanifestDetails(currentRecord.getValue('custrecord_ship_lp_no'));
						addLoadTrailerDetailsToSubList(form, currentRecord, parseInt(i),IndividualShipmanfDetList);

					}
					// Case# 20148822 starts
					//var currentRecord = searchResults[i];
					//addLoadTrailerDetailsToSubList(form, currentRecord, parseInt(i));
					var currentRecord = searchResults[i];
					var IndividualShipmanfDetList = GetShipmanifestDetails(currentRecord.getValue('custrecord_ship_lp_no',null,'group'));
					addLoadTrailerDetailsToSubList(form, currentRecord, parseInt(i),IndividualShipmanfDetList);
					// Case# 20148822 ends
				}
			}
			form.setScript('customscript_loadtrailer_cl');
			trailer.setMandatory(true);		
			form.addSubmitButton('Load');
			response.writePage(form);
		}
	}
	else{
		var form = nlapiCreateForm('Load Trailer');
		var wave='';
		wave=request.getParameter('custpage_wave');
		//form.setScript('customscript_loadtrailer_cl');
		nlapiLogExecution('ERROR','else wave',wave);
		var lineCnt = request.getLineItemCount('custpage_loadtrailer');
		var trailerNo = request.getParameter('custpage_trailer');
		var PronId,Tripno,Actdept,sealno;
		//var trailerName = getTrailerName(trailerNo);

		var pronoresearch = nlapiLoadRecord('customrecord_ebiznet_trailer', trailerNo); 
		var PronId = pronoresearch.getFieldValue('custrecord_ebizpro');
		var Actdept=pronoresearch.getFieldValue('custrecord_ebizdepartdate');
		var sealno = pronoresearch.getFieldValue('custrecord_ebizseal');
		var Tripno=pronoresearch.getFieldValue('custrecord_ebizvehicle');
		var trailerName=pronoresearch.getFieldValue('name');

		//code added on 250712 by suman
		var MasterShipno=pronoresearch.getFieldValue('custrecord_ebizmastershipper');
		var MasterProNo=pronoresearch.getFieldValue('custrecord_ebizpro');
		//end of code as of 250712.

		nlapiLogExecution('ERROR','PronId',PronId);
		nlapiLogExecution('ERROR','Actdept',Actdept);
		nlapiLogExecution('ERROR','sealno',sealno);
		nlapiLogExecution('ERROR','Tripno',Tripno);
		nlapiLogExecution('ERROR','trailerName',trailerName);

		//nlapiLogExecution('ERROR','SalesordId',SalesordId);

//		var SalesordId=getSalesorrderinternalid(wave);
//		nlapiLogExecution('ERROR','SalesordId',SalesordId);

		nlapiLogExecution('ERROR','false','false');
		var vBoolAnythingChecked=false;var shipLPInternalId='';
		var recordIdArr=new Array();
		var shiplpArr=new Array();
		var WaveArr=new Array();
		nlapiLogExecution('ERROR', 'Remaining Usage at end1', nlapiGetContext().getRemainingUsage());
		var otparent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent');
		var parentid = nlapiSubmitRecord(otparent); 
		var newParent = nlapiLoadRecord('customrecord_ebiz_throwaway_parent', parentid);



		for (var k = 1; k <= lineCnt; k++) {
			var itemname= request.getLineItemValue('custpage_loadtrailer','custpage_itemname',k);
			var itemNo= request.getLineItemValue('custpage_loadtrailer','custpage_ebizskuno',k);
			var avlqty= request.getLineItemValue('custpage_loadtrailer','custpage_ordqty',k);			 
			var ebizDono= request.getLineItemValue('custpage_loadtrailer','custpage_ebizdono',k);
			var Lineno= request.getLineItemValue('custpage_loadtrailer','custpage_lineno',k);				
			var vDoname= request.getLineItemValue('custpage_loadtrailer','custpage_dono',k);
			var vactLocation = request.getLineItemValue('custpage_loadtrailer','custpage_actbeginloc',k);
			var picktaskid = request.getLineItemValue('custpage_loadtrailer','custpage_pickid',k);

			//new values
			var chkVal = request.getLineItemValue('custpage_loadtrailer', 'custpage_select', k);
			var serialno= request.getLineItemValue('custpage_loadtrailer','custpage_serialno',k);				
			var ordernumber= request.getLineItemValue('custpage_loadtrailer','custpage_ordernumber',k);
			var wavenumber = request.getLineItemValue('custpage_loadtrailer','custpage_wavenumber',k);
			var consignee = request.getLineItemValue('custpage_loadtrailer','custpage_consignee',k);
			var shiplp = request.getLineItemValue('custpage_loadtrailer','custpage_shiplp',k);
			//var prono = request.getLineItemValue('custpage_loadtrailer','custpage_prono',k);
			var recordId = request.getLineItemValue('custpage_loadtrailer','custpage_internalid',k);
			var orderno=request.getLineItemValue('custpage_loadtrailer','custpage_orderno',k);
			var begindate= request.getLineItemValue('custpage_loadtrailer','custpage_begindate',k);
			var actbegintime= request.getLineItemValue('custpage_loadtrailer','custpage_actbegintime',k);
			var taskweight = request.getLineItemValue('custpage_loadtrailer','custpage_weight',k);
			var taskcube = request.getLineItemValue('custpage_loadtrailer','custpage_cube',k);
			var SalesordId=request.getLineItemValue('custpage_loadtrailer','custpage_orderno',k);


			nlapiLogExecution('ERROR','orderno',orderno);
			nlapiLogExecution('ERROR','chkVal',chkVal);
			nlapiLogExecution('ERROR','ordernumber',ordernumber);
			nlapiLogExecution('ERROR','shiplp',shiplp);
			nlapiLogExecution('ERROR','vDoname',vDoname);
			nlapiLogExecution('ERROR','ebizDono',ebizDono);
			nlapiLogExecution('ERROR','vactLocation',vactLocation);


			//var msg = form.addField('custpage_messages', 'inlinehtml', null, null, null);

			if (chkVal == 'T') {
				vBoolAnythingChecked=true;
				nlapiLogExecution('ERROR','Before updateOpentask',recordId);					
				var opentaskcurrentRow = [recordId,trailerName,shiplp,itemNo,wavenumber,orderno,vactLocation,begindate,actbegintime,taskweight,taskcube];

				nlapiLogExecution('ERROR', 'Remaining Usage before update open task', nlapiGetContext().getRemainingUsage());
				//code added on 250712 by suman
				if(MasterShipno!=""||MasterProNo!="")
					UpdateShipAndProNo(SalesordId,MasterShipno,MasterProNo);
				//end of code.

				//var opentaskRecordId = updateOpentask(recordId, trailerName);
				//var opentaskRecordId = updateOpentask(opentaskcurrentRow, newParent);

				nlapiLogExecution('ERROR','Before fillShipLP',shiplp);
				if(shiplpArr.indexOf(shiplp) == -1){
					shiplpArr.push(shiplp);
					nlapiLogExecution('ERROR','fillShipLP','done');
					shipLPInternalId = fillShipLP(shiplp);
					nlapiLogExecution('ERROR','fillShipLP','complete');
					var lpmastercurrentRow = [shipLPInternalId,trailerName,shiplp];
//					var lpRecordId = updateLPMaster(shipLPInternalId, trailerName);
					nlapiLogExecution('ERROR','Before updateLPMaster','done');
					var lpRecordId = updateLPMaster(lpmastercurrentRow, newParent);
					nlapiLogExecution('ERROR','after updateLPMaster','sucess');

					updateShiptask(shiplp,trailerName);
				}
				if(WaveArr.indexOf(wavenumber) == -1){
					WaveArr.push(wavenumber);
					nlapiLogExecution('ERROR','after submiting','done');
					nlapiLogExecution('ERROR',' before updateClosedTask','done');
					var closeTaskDetails = updateClosedTask(wavenumber, trailerName);
					nlapiLogExecution('ERROR',' after updateClosedTask','done');
				}				
			}
		}
		nlapiLogExecution('ERROR','Before submiting','done');
		nlapiSubmitRecord(newParent); 
		nlapiLogExecution('ERROR', 'Remaining Usage at end2', nlapiGetContext().getRemainingUsage());

		nlapiLogExecution('ERROR',' after SalesordId',SalesordId);
		var bol=getbolno(SalesordId);
		nlapiLogExecution('ERROR','Bol[0]',bol[0]);
		nlapiLogExecution('ERROR','Bol[1]',bol[1]);
		var trailerRecordId = updateTrailer(trailerNo,bol,PronId,pronoresearch);
		nlapiLogExecution('ERROR', 'Remaining Usage at end3', nlapiGetContext().getRemainingUsage());
		if(vBoolAnythingChecked)
		{
			var msg = form.addField('custpage_messages', 'inlinehtml', null, null, null);
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Trailer loaded successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
			nlapiLogExecution('ERROR','after completed','after completed');

			response.writePage(form);
		}
		else
		{
			var lpErrorMessage1 = 'Please select atleast one line ';
			showInlineMessage(form, 'Error', lpErrorMessage1, null);
			response.writePage(form);
		}

	}
}

function getbolno(SalesordId)
{
	nlapiLogExecution('ERROR', "getbolno", SalesordId);
	var bollable,pronumber;
	var bolnpro=new Array();
	var bolfilters = new Array();		
	bolfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['14']));
	if(SalesordId !=null && SalesordId !=""){
		bolfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', SalesordId));	}	

	var bolcolumns = new Array();
	bolcolumns[0] = new nlobjSearchColumn('custrecord_bol');
	bolcolumns[1] = new nlobjSearchColumn('custrecord_pro');
	searchresultsbol = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, bolfilters, bolcolumns);
	if( searchresultsbol != null &&  searchresultsbol != "")
	{
		bollable=searchresultsbol[0].getValue('custrecord_bol');
		pronumber=searchresultsbol[0].getValue('custrecord_pro');
		nlapiLogExecution('ERROR', "pronumber", pronumber);
		bolnpro.push(bollable);
		bolnpro.push(pronumber);

	}
	return bolnpro;
}

function distinctordcount(shiplp,PronId,Tripno,Actdept,sealno,trailerName)
{
	nlapiLogExecution('ERROR', "distinctordcount", shiplp);
	var count=1;
	var ordarrray = new Array();
	var distinctordFilers = new Array(); 
	var distinctordColumns = new Array();
	var salesOrderNumber='';

//	distinctordFilers.push(new nlobjSearchFilter('custrecord_ebiztask_ship_lp_no', null, 'is', shiplp)); 	
//	distinctordColumns[0] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_order_no');
//	var distinctordSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, distinctordFilers, distinctordColumns);

	distinctordFilers.push(new nlobjSearchFilter('custrecord_ship_lp_no', null, 'is', shiplp)); 	
	distinctordColumns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	var distinctordSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, distinctordFilers, distinctordColumns);

	if(distinctordSearchResults != null && distinctordSearchResults!=""){
		nlapiLogExecution('ERROR', "distinctordSearchResults", distinctordSearchResults.length);	
		if(distinctordSearchResults.length == 1){
			var Salesord=distinctordSearchResults[0].getValue('custrecord_ebiz_order_no');
			nlapiLogExecution('ERROR', "Salesordp", Salesord);			 

			var salesorderheadresearch = nlapiLoadRecord('salesorder', Salesord); 
			var soname = salesorderheadresearch.getFieldValue('tranid');
			var carrier=salesorderheadresearch.getFieldText('custbody_salesorder_carrier');
			nlapiLogExecution('ERROR', "after salesorder", Salesord);
			var weight=getweight(Salesord);
			var createShiManifestRecordResult = createShiManifestRecord(PronId,Tripno,Actdept,sealno,trailerName,Salesord,carrier,weight);
		}
		else{
			for (var i = 0; i < distinctordSearchResults.length; i++){

				if (salesOrderNumber.indexOf(distinctordSearchResults[i].getValue('custrecord_ebiz_order_no')) == -1 || i==0 ){

					var Salesord=distinctordSearchResults[i].getValue('custrecord_ebiz_order_no');
					nlapiLogExecution('ERROR', "Salesordp", Salesord);
					count +=1;

					var salesorderheadresearch = nlapiLoadRecord('salesorder', Salesord); 
					var soname = salesorderheadresearch.getFieldValue('tranid');
					var carrier=salesorderheadresearch.getFieldText('custbody_salesorder_carrier');
					nlapiLogExecution('ERROR', "after salesorder", Salesord);
					var weight=getweight(Salesord);
					var createShiManifestRecordResult = createShiManifestRecord(PronId,Tripno,Actdept,sealno,trailerName,Salesord,carrier,weight);

				}			
				salesOrderNumber =salesOrderNumber+","+distinctordSearchResults[i].getValue('custrecord_ebiz_order_no');			
			}
		}
	}

	nlapiLogExecution('ERROR', "record count", count);
	return count;
}

function getweight(SalesordId)
{
	nlapiLogExecution('ERROR', "getweight", SalesordId);

	// Changed to opentask on 06/18/2012 by Satish.N
	var weight;
	var closedTaskFilers = new Array(); 

	closedTaskFilers.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', SalesordId)); 

	var closedTaskColumns = new Array();
	closedTaskColumns[0] = new nlobjSearchColumn('custrecord_total_weight');

	var closedTaskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, closedTaskFilers, closedTaskColumns);
	if(closedTaskSearchResults!=null && closedTaskSearchResults!='')
	{
		for (var i = 0; i < closedTaskSearchResults.length; i++) {
			weight = closedTaskSearchResults[0].getValue('custrecord_total_weight');
		}
	}
	return weight;
}

function GetShipmanifestDetails(shiplp){
	nlapiLogExecution('ERROR', 'Into GetShipmanifestDetails', shiplp);
	var shipmanifestDetailsListT = new Array();

	try
	{
		if(shiplp!=null && shiplp!='')
		{
			var filters = new Array();
			var columns = new Array();
			filters.push(new nlobjSearchFilter('custrecord_ship_contlp', null, 'is', shiplp));
			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_ship_carrier');
			columns[1] =  new nlobjSearchColumn('custrecord_ship_city');

			var ResShipmanfDetails = nlapiSearchRecord('customrecord_ship_manifest', null, filters,columns);

			if ( ResShipmanfDetails !=null && ResShipmanfDetails.length > 0){ 
				for ( var i = 0 ; i < ResShipmanfDetails.length; i++ ){
					var shipmanifestDetailsList = new Array();
					shipmanifestDetailsList[0] = ResShipmanfDetails[i].getValue('custrecord_ship_carrier');
					shipmanifestDetailsList[1] = ResShipmanfDetails[i].getValue('custrecord_ship_city');

					shipmanifestDetailsListT.push(shipmanifestDetailsList);
				}
			}
		}
	}
	catch(exp) 
	{
		nlapiLogExecution('ERROR', 'Get GetShipmanifestDetails', exp);
	}
	nlapiLogExecution('ERROR', 'Out of GetShipmanifestDetails');
	return shipmanifestDetailsListT;

}

function getSalesorrderinternalid(waveno){
	nlapiLogExecution('ERROR', "into getSalesorrderinternalid", waveno);
	// Changed to opentask on 06/18/2012 by Satish.N
	var salesordId;
	var getSalesorrderFilers = new Array();	 
	if(waveno !=null && waveno !="")
		getSalesorrderFilers.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', waveno));
	getSalesorrderFilers.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'noneof','@NONE@'));

	var getSalesorderColumns = new Array();
	getSalesorderColumns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');

	var getSalesorderSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, getSalesorrderFilers, getSalesorderColumns);
	if(getSalesorderSearchResults!=null && getSalesorderSearchResults!="")
	{
		for (var i = 0; i < getSalesorderSearchResults.length; i++) {
			salesordId = getSalesorderSearchResults[0].getValue('custrecord_ebiz_order_no');
		}
	}
	nlapiLogExecution('ERROR', "end getSalesorrderinternalid", 'done');
	return salesordId;
}




///**
//* This function is to get the trailer name for the trailer selected in the drop down of the query block.
//* @param trailerNo
//* @returns
//*/
//function getTrailerName(trailerNo){
//var fields = ['name'];
//var columns = nlapiLookupField('customrecord_ebiznet_trailer', trailerNo, fields);
//var trailerName = columns.name;
//nlapiLogExecution('ERROR', "updateOpentask: trailerName", trailerName);

//return trailerName;
//}

/**
 * This function is to update the SHIP task in open task record 
 * 	with the trailer number and the status to Loaded.
 * @param recordId
 */
function updateOpentask(opentaskcurrentRow, newParent){
	/*var opentaskRecordId =-1;
	nlapiLogExecution('ERROR', "updateOpentask: trailerName", trailerName);
	nlapiLogExecution('ERROR', "updateOpentask: recordId", recordId);
	if(recordId!=null && recordId!="")
	{
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recordId);
		//set the status flag as STATUS.OUTBOUND.TRAILER_LOADED
		transaction.setFieldValue('custrecord_wms_status_flag', "10");
		transaction.setFieldValue('custrecord_ebiz_trailer_no', trailerName);
		opentaskRecordId = nlapiSubmitRecord(transaction, true);
	}

	nlapiLogExecution('ERROR', "updateOpentask: opentaskRecordId", opentaskRecordId);
	recordId,trailerName,shiplp,itemNo,wavenumber,orderno,vactLocation,begindate,actbegintime*/
	if(opentaskcurrentRow!=null && opentaskcurrentRow!='' && opentaskcurrentRow.length>0)
	{
		nlapiLogExecution('ERROR', "opentaskcurrentRow", opentaskcurrentRow);
		newParent.selectNewLineItem('recmachcustrecord_ebiz_ot_parent');

		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'id', opentaskcurrentRow[0]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_status_flag', '10');			
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_trailer_no', opentaskcurrentRow[1]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','name', opentaskcurrentRow[2]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ship_lp_no', opentaskcurrentRow[2]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_sku', opentaskcurrentRow[3]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_wave_no', opentaskcurrentRow[4]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_order_no', opentaskcurrentRow[5]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecordact_begin_date',opentaskcurrentRow[7]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualbegintime',opentaskcurrentRow[8]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_total_weight',opentaskcurrentRow[9]);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_totalcube',opentaskcurrentRow[10]);

		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_tasktype', '4');


		newParent.commitLineItem('recmachcustrecord_ebiz_ot_parent');		
	}
}

function updateShiptask(shiplp,trailerName)
{
	nlapiLogExecution('ERROR','Into  updateShiptask',trailerName);

	var filters = new Array();
	if(shiplp != "" && shiplp!=null){
		nlapiLogExecution('ERROR','Inside shiplp',shiplp);
		filters.push(new nlobjSearchFilter('custrecord_ship_lp_no', null, 'is', shiplp));			
	}
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [4]));
	filters.push( new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [7,28]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');

	var searchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(searchResults != null){
		for (var i = 0; i < searchResults.length; i++) {

			var shiptaskid = searchResults[i].getId();

			var fields = new Array();
			var values = new Array();

			fields.push('custrecord_ebiz_trailer_no');
			fields.push('custrecord_wms_status_flag');


			values.push(trailerName);
			values.push(10);

			nlapiSubmitField('customrecord_ebiznet_trn_opentask', shiptaskid, fields, values);

		}
	}

	nlapiLogExecution('ERROR','Out of  updateShiptask',trailerName);
}

function updateTrailer(recordId,bol,PronId,transaction){
	var TrailRecordId =-1;	
	nlapiLogExecution('ERROR', "updateTrailer: recordId", recordId);
	nlapiLogExecution('ERROR', "updateTrailer: PronId", PronId);
	if(transaction!=null && transaction!="" && bol !="" && bol !=null)
	{
		//var transaction = nlapiLoadRecord('customrecord_ebiznet_trailer', recordId);	
		if(bol[0]!= null && bol[0] != '')
			transaction.setFieldValue('custrecord_ebizmastershipper', bol[0]);
		if(PronId == null || PronId == ''){
			transaction.setFieldValue('custrecord_ebizpro', bol[1]);
		}
		TrailRecordId = nlapiSubmitRecord(transaction, true);
	}

	nlapiLogExecution('ERROR', "updateTrailer: opentaskRecordId", TrailRecordId);
	return TrailRecordId;
}

/**
 * This function is to update the SHIP task in LP Master record 
 * 	with the trailer number and the status to Loaded.
 * @param picktaskid
 */
function updateLPMaster(lpmastercurrentRow, newParent){

	nlapiLogExecution('ERROR', "Into updateLPMaster", lpmastercurrentRow);

	/*	var lpRecordId = -1;
	nlapiLogExecution('ERROR', "updateLPMaster: trailerName", trailerName);
	nlapiLogExecution('ERROR', "updateLPMaster: shipLPId", shipLPId);
	if(shipLPId!=null && shipLPId!="")
	{
		var transaction = nlapiLoadRecord('customrecord_ebiznet_master_lp', shipLPId);
		//var weight=transaction.getFieldValue('custrecord_ebiz_lpmaster_totwght');
		//set the status flag as STATUS.OUTBOUND.TRAILER_LOADED
		transaction.setFieldValue('custrecord_ebiz_lpmaster_wmsstatusflag', "10");
		transaction.setFieldValue('custrecord_ebiz_lpmaster_masterlp', trailerName);
		lpRecordId = nlapiSubmitRecord(transaction, true);
	}

	nlapiLogExecution('ERROR', "updateLPMaster: lpRecordId", lpRecordId);

	return lpRecordId;shipLPInternalId,trailerName,shiplp*/

	nlapiLogExecution('ERROR', "lpmastercurrentRow", lpmastercurrentRow);

	if(lpmastercurrentRow!=null && lpmastercurrentRow!='' && lpmastercurrentRow.length>0)
	{
		nlapiLogExecution('ERROR', "lpmastercurrentRow[0]", lpmastercurrentRow[0]);
		if(lpmastercurrentRow[0]!=null && lpmastercurrentRow[0]!='')
		{
			newParent.selectNewLineItem('recmachcustrecord_ebiz_lp_parent');

			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent', 'id', lpmastercurrentRow[0]);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_wmsstatusflag', '10');			
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_masterlp', lpmastercurrentRow[1]);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','name', lpmastercurrentRow[2]);
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_lp', lpmastercurrentRow[2]);		
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_lptype', '3');
			//newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_sizeid', '1');

			newParent.commitLineItem('recmachcustrecord_ebiz_lp_parent');	
		}
	}

	nlapiLogExecution('ERROR', "Out of updateLPMaster", lpmastercurrentRow);
}

/**
 *  This function is to specify the Load Trailer filters.
 * 	This function returns an array of filter criteria specified. 
 * @param waveno
 * @param fulfillmentorder
 * @param shiplp
 * @returns {Array}
 */
function specifyLoadTrailerFilters(waveno,fulfillmentorder,shiplp,VCarrier){

	nlapiLogExecution('ERROR','Into specifyLoadTrailerFilters','');
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
	filters.push( new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [7]));
	if(waveno != "" && waveno!=null){
		nlapiLogExecution('ERROR','Inside waveno',waveno);
		filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', waveno));			
	}
	if(fulfillmentorder != "" && fulfillmentorder!=null){
		nlapiLogExecution('ERROR','Inside fulfillmentorder',fulfillmentorder);
		filters.push(new nlobjSearchFilter('name', null, 'is', fulfillmentorder));			
	}	
	if(shiplp != "" && shiplp!=null){
		nlapiLogExecution('ERROR','Inside shiplp',shiplp);
		filters.push(new nlobjSearchFilter('custrecord_ship_lp_no', null, 'is', shiplp));			
	}	
	if(VCarrier!=null && VCarrier!="")
		filters.push(new nlobjSearchFilter('custrecord_ebizwmscarrier', null, 'anyof', VCarrier)); //for OrdNo'
	nlapiLogExecution('ERROR','Out of specifyLoadTrailerFilters','');
	return filters;
}

/**
 *  This function is to specify the Load Trailer columns.
 * 	This returns an array of columns to be fetched from the search record. 
 * @returns {Array}
 */
function specifyLoadTrailerColumns(){
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name',null,'group');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_wave_no',null,'group');
	columns[2] = new nlobjSearchColumn('custrecord_ship_lp_no',null,'group');
	columns[3] = new nlobjSearchColumn('internalid','custrecord_ebiz_order_no','group');
	columns[4] = new nlobjSearchColumn('custrecordact_begin_date',null,'group');
	columns[5] = new nlobjSearchColumn('custrecord_container',null,'group');
	columns[6] = new nlobjSearchColumn('custrecord_current_date',null,'group');
	columns[7] = new nlobjSearchColumn('custrecord_sku',null,'group');
	columns[8] = new nlobjSearchColumn('custrecord_ebiz_trailer_no',null,'group');
	columns[9] = new nlobjSearchColumn('custrecord_tasktype',null,'group');
	columns[10] = new nlobjSearchColumn('custrecord_totalcube',null,'group');
	columns[11] = new nlobjSearchColumn('custrecord_total_weight',null,'group');
	columns[12] = new nlobjSearchColumn('custrecord_actualbegintime',null,'group');
	columns[13] = new nlobjSearchColumn('custrecord_container_lp_no',null,'group');

	columns[0].setSort();

	return columns;
}
/**
 * This function is to create Load Trailer Sublist.
 *  This requires form to be sent as a parameter to build the sublist.
 * @param form
 */
function createLoadTrailerSubList(form){
	var sublist = form.addSubList("custpage_loadtrailer", "list", "Load Trailer");
	sublist.addField("custpage_select", "checkbox", "Confirm").setDefaultValue('T');
	sublist.addField("custpage_serialno", "text", "SL #");
	sublist.addField("custpage_ordernumber", "text", "Order #").setDisplayType('hidden');
	sublist.addField("custpage_orderno", "text", "Order #").setDisplayType('hidden');
	sublist.addField("custpage_wavenumber", "text", "Wave #");	 
	sublist.addField("custpage_shiplp", "text", "Ship LP");
	sublist.addField("custpage_carton", "text", "Carton #");
	//sublist.addField("custpage_prono", "text", "Pro #").setDisplayType('entry');
	sublist.addField("custpage_internalid", "text", "Internal Id").setDisplayType('hidden');

	sublist.addField("custpage_lineno", "text", "Line #").setDisplayType('hidden');
	sublist.addField("custpage_tasktype", "text", "TaskType").setDisplayType('hidden');
	sublist.addField("custpage_itemname", "text", "Item").setDisplayType('hidden');
	sublist.addField("custpage_packcode", "text", "Pack Code").setDisplayType('hidden');
	sublist.addField("custpage_lot", "text", "LOT#").setDisplayType('hidden');
	sublist.addField("custpage_itemstatus", "text", "Item Status").setDisplayType('hidden');		
	sublist.addField("custpage_container", "text", "Container").setDisplayType('hidden');
	sublist.addField("custpage_ordqty", "text", "Qty").setDisplayType('hidden');
	sublist.addField("custpage_actbeginloc", "text", "Location").setDisplayType('hidden');	
	sublist.addField("custpage_ebizskuno", "text", "eizskuno").setDisplayType('hidden');
	sublist.addField("custpage_ebizdono", "text", "eizdono").setDisplayType('hidden');
	sublist.addField("custpage_pickid", "text", "Ship task Id").setDisplayType('hidden');
	sublist.addField("custpage_begindate", "text", "begin date").setDisplayType('hidden');
	sublist.addField("custpage_actbegintime", "text", "actual begin time").setDisplayType('hidden');
	sublist.addField("custpage_weight", "text", "weight").setDisplayType('hidden');
	sublist.addField("custpage_cube", "text", "cube").setDisplayType('hidden');
	sublist.addField("custpage_carriernamep", "text", "Carrier");
	sublist.addField("custpage_destination", "text", "Destination");
	sublist.addField("custpage_routeno", "text", "Route# / Shipment#");
	/*sublist.addField("custpage_internalid", "text", "Internal Id");

	sublist.addField("custpage_lineno", "text", "Line #");
	sublist.addField("custpage_tasktype", "text", "TaskType");
	sublist.addField("custpage_itemname", "text", "Item");
	sublist.addField("custpage_packcode", "text", "Pack Code");
	sublist.addField("custpage_lot", "text", "LOT/Batch#");
	sublist.addField("custpage_itemstatus", "text", "Item Status");		
	sublist.addField("custpage_container", "text", "Container");
	sublist.addField("custpage_ordqty", "text", "Qty");
	sublist.addField("custpage_actbeginloc", "text", "Location");	
	sublist.addField("custpage_ebizskuno", "text", "eizskuno");
	sublist.addField("custpage_ebizdono", "text", "eizdono");
	sublist.addField("custpage_pickid", "text", "Ship task Id");*/
}

/**
 * This function is to add the Load Trailer details to the sublist.
 * The Load Trailer details to the container management screen include order, wave number,
 * consignee,ship LP, load trailer internal id 
 * @param form
 * @param currentRecord
 * @param icustpage_orderno
 */
function addLoadTrailerDetailsToSubList(form, currentRecord, i,IndividualShipmanfDetList){
	form.getSubList('custpage_loadtrailer').setLineItemValue('custpage_serialno', i + 1, 
			parseInt(i) + 1);
	form.getSubList('custpage_loadtrailer').setLineItemValue('custpage_ordernumber', i + 1, 
			currentRecord.getValue('name',null,'group'));
	form.getSubList('custpage_loadtrailer').setLineItemValue('custpage_wavenumber', i + 1, 
			currentRecord.getValue('custrecord_ebiz_wave_no',null,'group'));	 
	form.getSubList('custpage_loadtrailer').setLineItemValue('custpage_shiplp', i + 1, 
			currentRecord.getValue('custrecord_ship_lp_no',null,'group'));

	form.getSubList('custpage_loadtrailer').setLineItemValue('custpage_carton', i + 1, 
			currentRecord.getValue('custrecord_container_lp_no',null,'group'));

	form.getSubList('custpage_loadtrailer').setLineItemValue('custpage_internalid', i + 1, 
			currentRecord.getId());
	form.getSubList('custpage_loadtrailer').setLineItemValue('custpage_orderno', i + 1, 
			currentRecord.getValue('internalid','custrecord_ebiz_order_no','group'));

	form.getSubList('custpage_loadtrailer').setLineItemValue('custpage_ebizskuno', i + 1, 
			currentRecord.getValue('custrecord_sku',null,'group'));
	form.getSubList('custpage_loadtrailer').setLineItemValue('custpage_begindate', i + 1, 
			currentRecord.getValue('custrecordact_begin_date',null,'group'));
	form.getSubList('custpage_loadtrailer').setLineItemValue('custpage_actbegintime', i + 1, 
			currentRecord.getValue('custrecord_actualbegintime',null,'group'));

	form.getSubList('custpage_loadtrailer').setLineItemValue('custpage_weight', i + 1, 
			currentRecord.getValue('custrecord_total_weight',null,'group'));

	form.getSubList('custpage_loadtrailer').setLineItemValue('custpage_cube', i + 1, 
			currentRecord.getValue('custrecord_totalcube',null,'group'));
	//new fields
	if (IndividualShipmanfDetList != null && IndividualShipmanfDetList.length > 0) {
		form.getSubList('custpage_loadtrailer').setLineItemValue('custpage_carriernamep', i + 1, 
				IndividualShipmanfDetList[0][0]);

		form.getSubList('custpage_loadtrailer').setLineItemValue('custpage_destination', i + 1, 
				IndividualShipmanfDetList[0][1]);

//		form.getSubList('custpage_loadtrailer').setLineItemValue('custpage_routeno', i + 1, 
//		currentRecord.getValue('custrecord_totalcube'));
	}

}

/**
 * This function is to get the ship LP id for the ship lp selected.
 * @param shipLP
 * @returns
 */
function fillShipLP(shipLP){

	nlapiLogExecution('ERROR','Into fillShipLP',shipLP);

	var shipLPId;
	var shipLPFilers = new Array();
	// Add filter criteria for LP type;
	// Search for 'SHIP' ;
	shipLPFilers.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lptype', null, 'is', '3'));
	// Add filter criteria for WMS Status Flag;
	// Search for 'PACK_COMPLETE' ;
	shipLPFilers.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_wmsstatusflag', null, 'anyof',['7','28']));
	shipLPFilers.push(new nlobjSearchFilter('name', null, 'is', shipLP));

	var shipLPColumns = new Array();
	shipLPColumns[0] = new nlobjSearchColumn('name');

	var shipLPSearchResults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, shipLPFilers, shipLPColumns);
	if(shipLPSearchResults!=null && shipLPSearchResults!="")
	{
		for (var i = 0; i < shipLPSearchResults.length; i++) {
			shipLPId = shipLPSearchResults[0].getId();
		}
	}

	nlapiLogExecution('ERROR','Out of fillShipLP',shipLPId);

	return shipLPId;
}

/*
 * To check Ship Complete or not
 * @param vSOInternalId
 * returns true if Ship complete and all qty picked
 * returns true if ship complete flag is false
 * returns false if ship not complete 
 */
function blnShipComplete(vSOInternalId)
{
	var vBlnShipComp=true;
	var filters = new Array();

	filters.push(new nlobjSearchFilter('name', null,
			'is', vSOInternalId)); 


	var columns = new Array();
	columns.push(new nlobjSearchColumn('custrecord_ord_qty'));
	columns.push(new nlobjSearchColumn('custrecord_pickqty'));
	columns.push(new nlobjSearchColumn('custrecord_shipcomplete'));


	var searchresultsordline = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);

	if(searchresultsordline != null && searchresultsordline != "" && searchresultsordline.length>0)
	{
		nlapiLogExecution('ERROR', 'searchresultsordline.length',searchresultsordline.length);
		for(var v=0;v<searchresultsordline.length;v++)
		{
			var vlinePickQty=0;
			var vlineOrdQty=0;
			if(searchresultsordline[v].getValue('custrecord_ord_qty')!= null && searchresultsordline[v].getValue('custrecord_ord_qty') != "")
				vlineOrdQty=parseFloat(searchresultsordline[v].getValue('custrecord_ord_qty'));

			if(searchresultsordline[v].getValue('custrecord_pickqty') != null && searchresultsordline[v].getValue('custrecord_pickqty') != "")
				vlinePickQty= parseFloat(searchresultsordline[v].getValue('custrecord_pickqty'));

			var vlineShipCompFlag=searchresultsordline[v].getValue('custrecord_shipcomplete');

			nlapiLogExecution('ERROR', 'vlineOrdQty',vlineOrdQty);
			nlapiLogExecution('ERROR', 'vlinePickQty',vlinePickQty);
			nlapiLogExecution('ERROR', 'vlineShipCompFlag',vlineShipCompFlag);

			if(vlineShipCompFlag!= null && vlineShipCompFlag != "" && vlineShipCompFlag =='T' && vlineOrdQty >0)
			{
				if(vlineOrdQty!=vlinePickQty)
				{
					vBlnShipComp=false;

				}
				else
				{
					vBlnShipComp=true;

				}	

			}
			else if(vlineShipCompFlag!= null && vlineShipCompFlag != "" && vlineShipCompFlag =='F' && vlineOrdQty >0)
			{
				vBlnShipComp=false;

			}

		}
	}
	else
		vBlnShipComp=false;
	nlapiLogExecution('ERROR', 'vBlnShipComp',vBlnShipComp);
	return vBlnShipComp;
}

/**
 * This function is used to update the trailer number in closed task record.
 * @param waveNumber
 * @param trailerName
 */
function updateClosedTask(waveNumber,trailerName)
{
	nlapiLogExecution('ERROR', "updateClosedTask:waveNumber", waveNumber);
	var closedTaskId;
	var closedTaskFilers = new Array();
	var closeTaskDetails = new Array();
	// Add filter criteria for Task type ;
	// Search for 'PICK';
	closedTaskFilers.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));
	closedTaskFilers.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'is', '7'));
	if(waveNumber !=null && waveNumber !="")
		closedTaskFilers.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', waveNumber));	 

	var closedTaskColumns = new Array();
	closedTaskColumns[0] = new nlobjSearchColumn('name');
	closedTaskColumns[1] = new nlobjSearchColumn('custrecord_ebiz_lpno');

	var closedTaskSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, closedTaskFilers, closedTaskColumns);
	if(closedTaskSearchResults!=null && closedTaskSearchResults!='')
	{
		nlapiLogExecution('ERROR', "closedTaskSearchResults", closedTaskSearchResults.length);
		for (var i = 0; i < closedTaskSearchResults.length; i++) {
			closedTaskId = closedTaskSearchResults[i].getId();

			var fields = new Array();
			var values = new Array();

			fields.push('custrecord_ebiz_trailer_no');
			fields.push('custrecord_wms_status_flag');


			values.push(trailerName);
			values.push(10);

			nlapiSubmitField('customrecord_ebiznet_trn_opentask', closedTaskId, fields, values);



			//clearOutboundInventory(vcontainerlp);

		}	
	}

	return closeTaskDetails;
}


function UpdateShipAndProNo(recordId,MasterShipno,MasterProNo)
{
	try
	{
		nlapiLogExecution('ERROR','Into UpdateShipAndProNo',rec);
		nlapiLogExecution('ERROR','recordId',recordId);
		nlapiLogExecution('ERROR','MasterShipno',MasterShipno);
		nlapiLogExecution('ERROR','MasterProNo',MasterProNo);

		var bolfilters = new Array();		
		bolfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['14']));
		if(recordId !=null && recordId !="")
			bolfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', recordId));		

		var searchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, bolfilters, null);
		if(searchresult!=null&&searchresult!="")
		{
			var fields=new Array();
			if(MasterShipno!=""&&MasterShipno!=null)
				fields.push('custrecord_bol');

			if(MasterProNo!=""&&MasterProNo!=null)
				fields.push('custrecord_pro');


			var values=new Array();
			if(MasterShipno!=""&&MasterShipno!=null)
				values.push(MasterShipno);

			if(MasterProNo!=""&&MasterProNo!=null)
				values.push(MasterProNo);

			var rec=nlapiSubmitField('customrecord_ebiznet_trn_opentask',searchresult[0].getId(),fields,values);
			nlapiLogExecution('ERROR','Updated rec',rec);
		}

	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in UpdateShipAndProNo',exp);
	}

}


/**
 * @param VQbwave
 * @param VOrdNo
 * @returns {String}
 */
function IsOpenPicksPresent(VQbwave,VOrdNo,VCarrier)
{
	try
	{
		nlapiLogExecution('ERROR','Into IsOpenPicksPresent');
		nlapiLogExecution('ERROR','VQbwave',VQbwave);
		nlapiLogExecution('ERROR','VOrdNo',VOrdNo);
		nlapiLogExecution('ERROR','VOrdNo',VOrdNo);
		nlapiLogExecution('ERROR','VCarrier',VCarrier);
		var IsOpenPicks='F';
		var TotalSO;
		if(VQbwave!=null&&VQbwave!="")
			TotalSO=GetAlltheOrders(VQbwave);

		nlapiLogExecution('ERROR','TotalSO',TotalSO);

		var filters=new Array();
		if(VQbwave!=null && VQbwave!="")
			filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is',VQbwave));
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));  //Pick Task
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['8'])); //8=pick confirmed ; 30=Short picks

		if(TotalSO!=null && TotalSO!="")
			filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', TotalSO)); //for OrdNo'
		else if(VOrdNo!=null && VOrdNo!="")
			filters.push(new nlobjSearchFilter('name', null, 'is', VOrdNo)); //for OrdNo'
		 if(VCarrier!=null && VCarrier!="")
			filters.push(new nlobjSearchFilter('custrecord_ebizwmscarrier', null, 'anyof', VCarrier)); //for OrdNo'
		 
		 filter.push(new nlobjSearchFilter('custrecord_act_qty',  null,'notequalto','0')); 
			filter.push(new nlobjSearchFilter('custrecord_act_qty',  null,'notequalto','0.0'));

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, null);
		if(searchresults!=null&&searchresults!="")
			IsOpenPicks='T';
		nlapiLogExecution('ERROR','IsOpenPicks',IsOpenPicks);
		return IsOpenPicks;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in IsOpenPicksPresent',exp);
	}
}


function GetAlltheOrders(wave)
{
	try
	{
		var SoIdArray=new Array();
		nlapiLogExecution('ERROR','Into GetAlltheOrders');
		nlapiLogExecution('ERROR','VQbwave',wave);
		var filters=new Array();
		if(wave!=null && wave!="")
			filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is',wave));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebiz_order_no');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, column);
		if(searchresults!=null&&searchresults!="")
		{
			for ( var x = 0; x < searchresults.length; x++)
			{
				var soid=searchresults[x].getValue('custrecord_ebiz_order_no');
				SoIdArray.push(soid);
			}
		}
		nlapiLogExecution('ERROR','SoIdArray',SoIdArray);
		return SoIdArray;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetAlltheOrders',exp);
	}
}
