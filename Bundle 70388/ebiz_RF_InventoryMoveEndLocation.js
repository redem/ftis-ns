/***************************************************************************
 eBizNET Solutions               
 ****************************************************************************/
/* Prologue - 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_InventoryMoveEndLocation.js,v $
 *     	   $Revision: 1.14.2.7.4.6.4.32.2.1 $
 *     	   $Date: 2015/11/09 15:20:54 $
 *     	   $Author: grao $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_InventoryMoveEndLocation.js,v $
 * Revision 1.14.2.7.4.6.4.32.2.1  2015/11/09 15:20:54  grao
 * 2015.2 Issue Fixes 201415340
 *
 * Revision 1.14.2.7.4.6.4.32  2015/07/22 15:35:02  skreddy
 * Case# 201413412
 * PD SB  issue fix
 *
 * Revision 1.14.2.7.4.6.4.31  2015/07/13 15:30:43  skreddy
 * Case# 201413311
 * Signwarehouse SB  issue fix
 *
 * Revision 1.14.2.7.4.6.4.30  2015/07/02 13:53:30  schepuri
 * case# 201413301
 *
 * Revision 1.14.2.7.4.6.4.29  2015/06/30 13:22:13  schepuri
 * case# 201413274
 *
 * Revision 1.14.2.7.4.6.4.28  2015/06/24 13:38:24  schepuri
 * case# 201413198
 *
 * Revision 1.14.2.7.4.6.4.27  2015/03/16 07:51:15  snimmakayala
 * 201223282
 *
 * Revision 1.14.2.7.4.6.4.26  2015/01/23 15:15:02  sponnaganti
 * case# 201411464
 * DCD Prod issue fix
 *
 * Revision 1.14.2.7.4.6.4.25  2014/09/19 19:04:15  grao
 * Case#: 201410460 CT issue Sb fixes
 *
 * Revision 1.14.2.7.4.6.4.24  2014/08/29 17:53:27  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20149988
 *
 * Revision 1.14.2.7.4.6.4.23  2014/07/14 06:09:27  skreddy
 * case # 20149403
 * Dealmed  issue fix
 *
 * Revision 1.14.2.7.4.6.4.22  2014/06/19 08:06:21  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148948
 *
 * Revision 1.14.2.7.4.6.4.21  2014/06/16 06:44:41  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148866
 *
 * Revision 1.14.2.7.4.6.4.20  2014/06/13 09:53:57  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.14.2.7.4.6.4.19  2014/06/02 15:45:19  skavuri
 * Case # 20148685 SB Issue Fixed
 *
 * Revision 1.14.2.7.4.6.4.18  2014/05/30 14:40:21  grao
 * Case#: 20148619� Standard  issue fixes
 *
 * Revision 1.14.2.7.4.6.4.17  2014/05/30 00:34:21  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.14.2.7.4.6.4.16  2014/04/09 16:13:31  gkalla
 * case#201218724
 * Ryonet Fixed LP issue fix
 *
 * Revision 1.14.2.7.4.6.4.15  2014/03/14 15:17:48  skavuri
 * Case # 20127699 issue fixed
 *
 * Revision 1.14.2.7.4.6.4.14  2014/02/24 14:51:20  rmukkera
 * Case # 20127327
 *
 * Revision 1.14.2.7.4.6.4.13  2014/02/19 07:22:52  sponnaganti
 * case# 20127232
 * (Filter index number changed)
 *
 * Revision 1.14.2.7.4.6.4.12  2014/02/18 09:27:48  snimmakayala
 * Case# : 20127160
 * Inventory Move Merge LP issue for MHP
 *
 * Revision 1.14.2.7.4.6.4.11  2013/12/04 11:22:07  rmukkera
 * no message
 *
 * Revision 1.14.2.7.4.6.4.10  2013/12/02 13:37:11  gkalla
 * case#20126056
 * Invalid Location message
 *
 * Revision 1.14.2.7.4.6.4.9  2013/11/06 15:25:20  rmukkera
 * Case # 20125558
 *
 * Revision 1.14.2.7.4.6.4.8  2013/10/25 20:02:30  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 20125294
 *
 * Revision 1.14.2.7.4.6.4.7  2013/09/19 15:19:33  rmukkera
 * Case# 20124445
 *
 * Revision 1.14.2.7.4.6.4.6  2013/09/04 15:38:21  skreddy
 * Case# 20124231
 * standard bundle issue fix
 *
 * Revision 1.14.2.7.4.6.4.5  2013/07/22 16:47:30  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * GFT Issue fixes
 *
 * Revision 1.14.2.7.4.6.4.4  2013/07/15 07:52:28  spendyala
 * case# 20123442
 * We are validating user when user leave the binloc empty.
 *
 * Revision 1.14.2.7.4.6.4.3  2013/05/31 15:15:48  grao
 * CASE201112/CR201113/LOG201121
 * PMM Issues fixes
 *
 * Revision 1.14.2.7.4.6.4.2  2013/04/17 16:02:35  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.14.2.7.4.6.4.1  2013/03/26 13:27:42  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.14.2.7.4.6  2013/01/09 15:19:34  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Pickface Location Vs Item validation.
 * .
 *
 * Revision 1.14.2.7.4.5  2012/11/28 15:05:24  schepuri
 * CASE201112/CR201113/LOG201121
 * Auto gen loc is not displayed
 *
 * Revision 1.14.2.7.4.4  2012/11/01 14:55:42  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.14.2.7.4.3  2012/09/26 12:40:14  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.14.2.7.4.2  2012/09/24 22:45:21  spendyala
 * CASE201112/CR201113/LOG201121
 * Issues related to Expiry date is resolved.
 *
 * Revision 1.14.2.7.4.1  2012/09/21 15:01:30  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.14.2.7  2012/08/24 18:21:35  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * NSUOMfield code changes
 *
 * Revision 1.14.2.6  2012/07/31 08:30:27  skreddy
 * CASE201112/CR201113/LOG201121
 * Changed old Itemstatus to newItemstatus while autogenerate loaction
 *
 * Revision 1.14.2.5  2012/06/26 13:21:58  skreddy
 * CASE201112/CR201113/LOG201121
 * Added Autogenerate Button
 *
 * Revision 1.14.2.4  2012/06/07 13:26:11  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * restict stage location in inventory move
 *
 * Revision 1.14.2.3  2012/03/16 14:08:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.14.2.2  2012/02/23 00:27:34  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * supress lp in Invtmove and lp merge based on fifodate
 *
 * Revision 1.14.2.1  2012/02/07 12:52:23  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Inventory Move Changes
 *
 * Revision 1.17  2012/01/20 01:10:19  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Inventory Move Changes
 *
 * Revision 1.16  2012/01/19 14:48:32  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Invt Move
 *
 * Revision 1.15  2012/01/13 23:58:56  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Invt Move
 *
 * Revision 1.14  2011/12/30 01:16:23  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Inventory Move Changes
 *
 * Revision 1.13  2011/12/28 23:11:17  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Inventory Move Changes
 *
 * Revision 1.12  2011/12/26 22:40:12  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Inventory Move Changes
 *
 * Revision 1.10  2011/10/05 17:00:05  snimmakayala
 * CASE201112/CR201113/LOG20112
 * Inventory Move Changes
 *
 * Revision 1.9  2011/09/28 12:10:52  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.8  2011/09/28 10:08:03  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.7  2011/09/21 18:58:16  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Inventory Move Fixes
 *
 *****************************************************************************/

/**
 * @author Phani
 * @version
 * @date
 */
function InventoryMoveToNewLocation(request, response)
{
	var getNewLP = request.getParameter('custparam_newLP');
	var getMoveQuantity = request.getParameter('custparam_moveqty');
	var getMoveQty = request.getParameter('custparam_moveqty');
	var getBinLocationId = request.getParameter('custparam_imbinlocationid');
	var getBinLocation = request.getParameter('custparam_imbinlocationname');
	var getBatchNo = request.getParameter('custparam_batch');  
	var getItemId = request.getParameter('custparam_imitemid');
	var getItem = request.getParameter('custparam_imitem');
	var getTotQuantity = request.getParameter('custparam_totquantity');
	var getAvailQuantity = request.getParameter('custparam_availquantity');    
	var getUOMId = request.getParameter('custparam_uomid');
	var getUOM = request.getParameter('custparam_uom');
	var getStatus = request.getParameter('custparam_status');
	var getStatusId = request.getParameter('custparam_statusid');
	var getLOTId = request.getParameter('custparam_lotid');
	var getLOTNo = request.getParameter('custparam_lot');  
	var getLPId = request.getParameter('custparam_lpid');
	var getLP = request.getParameter('custparam_lp');  
	var getItemType = request.getParameter('custparam_itemtype');
	var getActualBeginDate = request.getParameter('custparam_actualbegindate');
	var getActualBeginTime = request.getParameter('custparam_actualbegintime');
	var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
	var getInvtRecID=request.getParameter('custparam_invtrecid');
	var totqtymoved=request.getParameter('custparam_totqtymoveed');
	var locationId=request.getParameter('custparam_locationId');//
	var getNewStatus=request.getParameter('custparam_skustatus');
	var adjustmenttype=request.getParameter('custparam_adjusttype');
	var adjustflag=request.getParameter('custparam_adjustflag');
	var ItemDesc = request.getParameter('custparam_itemdesc');
	var fifodate=request.getParameter('custparam_fifodate');//
	var putmethodID=request.getParameter('custparam_NewPutMethodID');//
	var	tempBinLocationId=request.getParameter('custparam_hdntmplocation');
	var tempItemId=request.getParameter('custparam_hdntempitem');
	var tempLP=request.getParameter('custparam_hdntemplp');

	nlapiLogExecution('ERROR', 'locationId ', request.getParameter('custparam_locationId'));
	var compId=request.getParameter('custparam_compid');//
	//code added on 22 Jun2012 by santosh
	var getNewLoctionName='';

	if(request.getParameter('custparam_NewLocationName')!=null && request.getParameter('custparam_NewLocationName') !='')
	{
		getNewLoctionName=request.getParameter('custparam_NewLocationName');

	}
	else if(request.getParameter('custparam_newbinlocationname')!=null && request.getParameter('custparam_newbinlocationname') !='' && request.getParameter('custparam_newbinlocationname')!=getBinLocation)
	{
		getNewLoctionName=request.getParameter('custparam_newbinlocationname');
	}
	else
	{
		getNewLoctionName='';
	}
	//end of the code on  22 Jun2012 by santosh


	nlapiLogExecution('ERROR', 'getInvtRecID ',getInvtRecID);

	if (request.getMethod() == 'GET') 
	{	

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);

		var st0,st1,st2,st3;
		if( getLanguage == 'es_ES')
		{

			st0 = "INVENTARIO DE MUDANZA";
			st1 = "MOVER CANT";
			st2 = "ENTER NUEVA UBICACI&#211;N :";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";
			st5 = "AUTO GENERADA";
			st6= "UBICACI�N ACTUAL";
			st7= "ubicaci�n asignada";

		}
		else
		{
			st0 = "INVENTORY MOVE";
			st1 = "MOVE QTY";
			st2 = "ENTER NEW LOCATION :";
			st3 = "SEND";
			st4 = "PREV";
			st5 = "ASSIGN LOCATION";	

			st6="CURRENT LOCATION";
			st7="ASSIGNED LOCATION";

		}		

		var functionkeyHtml=getFunctionkeyScript('_rf_inventorymove_endloc');
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//	html = html + " document.getElementById('enterlocation').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head>";
		html = html + "<body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_inventorymove_endloc' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
//		html = html + "			<tr>";
//		html = html + "				<td align = 'left'>To LP :<label>" + getNewLP + "</label>";
//		html = html + "				</td>";
//		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + " :<label>" + getMoveQty + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		if(getNewLoctionName=="")
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st2;
			html = html + "				</td>";        
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'><label>" + getBinLocation + "</label>";
			html = html + "				</td>";
			html = html + "			</tr>";
		}
		else
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st6 +": <label>" + getBinLocation + "</label>";
			html = html + "				</td>";        
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st7 +": <label>" + getNewLoctionName + "</label>";
			html = html + "				</td>";   
			html = html + "			</tr>";	
		}
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlocation' id='enterlocation' type='text'>";
		html = html + "					<input type='hidden' name='hdnBinLocationId' value=" + getBinLocationId + ">";
		html = html + "					<input type='hidden' name='hdnBinLocation' value='" + getBinLocation + "'>";
		html = html + "					<input type='hidden' name='hdnItemId' value=" + getItemId + ">";
		html = html + "					<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "					<input type='hidden' name='hdntotqtymoved' value=" + totqtymoved + ">";
		html = html + "					<input type='hidden' name='hdnLP' value=" + getLP + ">";
		html = html + "					<input type='hidden' name='hdnLPId' value=" + getLPId + ">";        
		html = html + "					<input type='hidden' name='hdnLOTNo' value=" + getLOTNo + ">";
		html = html + "					<input type='hidden' name='hdnLOTNoId' value=" + getLOTId + ">";
		html = html + "					<input type='hidden' name='hdnStatus' value='" + getStatus + "'>";
		html = html + "					<input type='hidden' name='hdnStatusId' value=" + getNewStatus + ">";
		html = html + "					<input type='hidden' name='hdnUOM' value=" + getUOM + ">";
		html = html + "					<input type='hidden' name='hdnUOMId' value=" + getUOMId + ">";
		html = html + "					<input type='hidden' name='hdnTotQty' value=" + getTotQuantity + ">";
		html = html + "					<input type='hidden' name='hdnAvailQty' value=" + getAvailQuantity + ">";
		html = html + "					<input type='hidden' name='hdnItemType' value=" + getItemType + ">";
		html = html + "					<input type='hidden' name='hdnMoveQty' value=" + getMoveQuantity + ">";
		html = html + "					<input type='hidden' name='hdnBatchNo' value=" + getBatchNo + ">";
		html = html + "					<input type='hidden' name='hdnInvtRecID' value=" + getInvtRecID + ">";
		//html = html + "					<input type='hidden' name='hdnBatchId' value=" + getBatchId + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				    <input type='hidden' name='hdnlocationId' value=" + locationId + ">";
		html = html + "				    <input type='hidden' name='hdnadjusttype' value=" + adjustmenttype + ">";
		html = html + "				    <input type='hidden' name='hdnAdjustflag' value=" + adjustflag + ">";
		html = html + "				    <input type='hidden' name='hdnItemdesc' value='" + ItemDesc + "'>";
		html = html + "				    <input type='hidden' name='hdncompId' value=" + compId + ">";
		html = html + "				    <input type='hidden' name='hdnfifodate' value='" + fifodate + "'>";
		html = html + "				    <input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				    <input type='hidden' name='hdntmplocation' value=" + tempBinLocationId + ">";
		html = html + "				    <input type='hidden' name='hdntemplp' value='" + tempLP + "'>";
		html = html + "				    <input type='hidden' name='hdntempitem' value='" + tempItemId + "'>";
		html = html + "				    <input type='hidden' name='hdnputmethodid' value='" + putmethodID + "'>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		//html = html + "				RECOMMENDED <input name='cmdRecomend' type='submit' value='F10'/>";
		html = html + "				" + st3 + " <input name='cmdSearch' type='submit' value='F8' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "				" + st4 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>";
		html = html + "				" + st5 + " <input name='cmdAutoGenerate' type='submit' value='F6'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlocation').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		var vLprequired='Y';
		var getNewLPId = "";
		var getActualBeginDate = request.getParameter('hdnActualBeginDate');
		var getActualBeginTime = request.getParameter('hdnActualBeginTime');
		var getActualBeginTimeAMPM = request.getParameter('hdnActualBeginTimeAMPM');
		var getNewBinLocation = request.getParameter('enterlocation');
		var getOldBinLocation = request.getParameter('hdnBinLocation');

		nlapiLogExecution('ERROR', 'getNewlocation', getNewBinLocation);

		getMoveQuantity = request.getParameter('hdnMoveQty');
		getAvailQuantity = request.getParameter('hdnAvailQty');
		var adjustflag=request.getParameter('hdnAdjustflag');
		var IMarray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		IMarray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR', 'getLanguage', IMarray["custparam_language"]);

		vLprequired =IsLprequiredformove();
		nlapiLogExecution('ERROR', 'vLprequired', vLprequired);	

		var st6,st7,st8,st9,st10;
		if( getLanguage == 'es_ES')
		{

			st6 = "LP NO V&#193;LIDO";
			st7 = "LOCALIZACI&#211;N GENERADOS MISMO";
			st8 = "NO HAY RESULTADOS PARA VALORES MENCIONADOS";
			st9 = "UBICACI&#211;N NO V&#193;LIDA";
			st10 ="CANT excede la capacidad UBICACI�N EN QUE LUGAR";
		}
		else
		{

			st6 = "INVALID LP";
			st7 = "GENERATED SAME LOCATION";
			st8 = "NO RESULTS FOUND FOR MENTIONED VALUES";
			st9 = "INVALID LOCATION";
			st10 = "QTY EXCEEDS LOCATION CAPACITY OF TO LOCATION";

		}

		IMarray["custparam_newLP"] = getNewLP;	

		IMarray["custparam_actualbegindate"] = getActualBeginDate;

		IMarray["custparam_screenno"] = '22';

		var TimeArray = new Array();
		IMarray["custparam_actualbegintime"] = getActualBeginTime;
		IMarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM;
		IMarray["custparam_error"] = st6;

		IMarray["custparam_totqtymoveed"] = request.getParameter('hdntotqtymoved');
		IMarray["custparam_imbinlocationid"] = request.getParameter('hdnBinLocationId');
		IMarray["custparam_imbinlocationname"] = request.getParameter('hdnBinLocation');
		IMarray["custparam_imitemid"] = request.getParameter('hdnItemId');
		IMarray["custparam_imitem"] = request.getParameter('hdnItem');
		IMarray["custparam_totquantity"] = request.getParameter('hdnTotQty');
		IMarray["custparam_availquantity"] = request.getParameter('hdnAvailQty');
		IMarray["custparam_moveqty"] = request.getParameter('hdnMoveQty');
		IMarray["custparam_status"] = request.getParameter('hdnStatus');
		IMarray["custparam_statusid"] = request.getParameter('custparam_statusid');
		IMarray["custparam_uom"] = request.getParameter('hdnUOM');
		IMarray["custparam_uomid"] = request.getParameter('hdnUOMId');		
		IMarray["custparam_lot"] = request.getParameter('hdnLOTNo');
		IMarray["custparam_lotid"] = request.getParameter('hdnLOTNoId');
		IMarray["custparam_lp"] = request.getParameter('hdnLP');
		IMarray["custparam_lpid"] = request.getParameter('hdnLPId');
		IMarray["custparam_itemtype"]=request.getParameter('hdnitemType');
		IMarray["custparam_invtrecid"] = request.getParameter('hdnInvtRecID');
		IMarray["custparam_locationId"] = request.getParameter('hdnlocationId');		
		IMarray["custparam_adjusttype"] = request.getParameter('hdnadjusttype');
		//IMarray["custparam_imbinlocationname"]=getNewBinLocation;
		IMarray["custparam_adjustflag"]=adjustflag;
		IMarray["custparam_existingLP"]='';
		IMarray["custparam_itemdesc"] = request.getParameter('hdnItemdesc');
		IMarray["custparam_fifodate"] = request.getParameter('hdnfifodate');

		IMarray["custparam_hdntmplocation"] = request.getParameter('hdntmplocation');
		IMarray["custparam_hdntemplp"] = request.getParameter('hdntemplp');
		IMarray["custparam_hdntempitem"] = request.getParameter('hdntempitem');
		nlapiLogExecution('ERROR', 'IMarray["custparam_locationId"] ', IMarray["custparam_locationId"]);

		IMarray["custparam_actualbegindate"] = request.getParameter('custparam_actualbegindate');
		IMarray["custparam_skustatus"] = request.getParameter('custparam_skustatus');
		IMarray["custparam_compid"] = request.getParameter('hdncompId');
		IMarray["custparam_NewPutMethodID"] = request.getParameter('hdnputmethodid');
		nlapiLogExecution('ERROR', 'Compid', request.getParameter('hdncompId'));

		var newItemStatus = request.getParameter('hdnStatusId');
		nlapiLogExecution('ERROR', 'newItemStatus', request.getParameter('hdnStatusId'));

		var optedEvent = request.getParameter('cmdPrevious');




		var itemSubtype = nlapiLookupField('item', getItemId, ['recordType','custitem_ebizserialin','custitem_ebizserialout','custitem_ebizbatchlot']);
		IMarray["custparam_itemtype"] = itemSubtype.recordType;
		var batchlot = itemSubtype.custitem_ebizbatchlot;
		nlapiLogExecution('ERROR', 'itemSubtype.recordType =', itemSubtype);
		//case# 20127043 starts(In if condition one more condition is added for not to ask serial numbers when we are moving total aval qty)
		nlapiLogExecution('ERROR', 'getAvailQuantity', getAvailQuantity);
		nlapiLogExecution('ERROR', 'getMoveQty', getMoveQty);





		if (optedEvent == 'F7') 
		{  
			if(adjustflag=='Y')
			{
				response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_adjust', 'customdeploy_rf_inventory_move_adjust_di', false, IMarray);
			}
			else
			{
				response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_status', 'customdeploy_rf_inventory_move_status_di', false, IMarray);
			}
			//response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_qty', 'customdeploy_rf_inventory_move_qty_di', false, IMarray);	   		 
		}
		else 
		{
			// code added on  21 Jun2012 by santosh
			var getPackcode='';
			var getBaseUOM='';
			var getuomlevel='';
			var getBaseUomQty='';
			var getItemCube='';
			var optedAutoEvent = request.getParameter('cmdAutoGenerate');
			if(optedAutoEvent == 'F6')
			{
				nlapiLogExecution('ERROR', 'ButtonOption', optedAutoEvent);
				nlapiLogExecution('ERROR', 'AutoGenerate','Autogenerate');
				var ItemId=request.getParameter('hdnItemId');
				var itemstatus=request.getParameter('hdnStatusId');
				var ItemType= request.getParameter('hdnitemType');
				var ItemNewQty =request.getParameter('hdnMoveQty');				
				var ItemLocId=request.getParameter('hdnBinLocationId');
				var PoLoc=request.getParameter('hdnlocationId');

				var filter = new Array();
				filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', ItemId));
				filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	

				var column = new Array();
				column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim');
				column[1] = new nlobjSearchColumn('custrecord_ebizqty') ;
				column[2] = new nlobjSearchColumn('custrecord_ebizbaseuom');
				column[3] = new nlobjSearchColumn('custrecord_ebiznsuom') ;
				column[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim');
				column[5] = new nlobjSearchColumn('custrecord_ebizpackcodeskudim') ;
				column[6] = new nlobjSearchColumn('custrecord_ebizcube');

				eBizItemDims= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);
				if(eBizItemDims!=null&&eBizItemDims.length>0)
				{
					for(z=0; z < eBizItemDims.length; z++)
					{
						if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
						{
							getPackcode = eBizItemDims[z].getText('custrecord_ebizpackcodeskudim');
							getBaseUOM = eBizItemDims[z].getText('custrecord_ebizuomskudim');
							getuomlevel = eBizItemDims[z].getText('custrecord_ebizuomlevelskudim');
							getBaseUomQty = eBizItemDims[z].getValue('custrecord_ebizqty');
							getItemCube = eBizItemDims[z].getValue('custrecord_ebizcube');
							nlapiLogExecution('ERROR', 'Packcode', getPackcode);
							nlapiLogExecution('ERROR', 'BaseUOM', getBaseUOM);
							nlapiLogExecution('ERROR', 'Itemcube', getItemCube);


						}
					}
				}



				var TotalItemCube=CubeCapacity(ItemNewQty,getBaseUomQty,getItemCube);			
				nlapiLogExecution('ERROR', 'TotalItemCube', TotalItemCube);
				var filters2 = new Array();

				var columns2 = new Array();
				columns2[0] = new nlobjSearchColumn('custitem_item_family');
				columns2[1] = new nlobjSearchColumn('custitem_item_group');
				columns2[2] = new nlobjSearchColumn('custitem_ebizdefskustatus');
				columns2[3] = new nlobjSearchColumn('custitem_item_info_1');
				columns2[4] = new nlobjSearchColumn('custitem_item_info_2');
				columns2[5] = new nlobjSearchColumn('custitem_item_info_3');
				columns2[6] = new nlobjSearchColumn('custitem_ebizabcvelitem');

				filters2.push(new nlobjSearchFilter('internalid', null, 'is', ItemId));

				var ItemInfoResults = nlapiSearchRecord('item', null, filters2, columns2);
				var filters3=new Array();
				filters3.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

				var columns3=new Array();
				columns3[0] = new nlobjSearchColumn('custrecord_skufamilypickrule');
				columns3[1] = new nlobjSearchColumn('custrecord_skugrouppickrule');
				columns3[2] = new nlobjSearchColumn('custrecord_skustatuspickrule');
				columns3[3] = new nlobjSearchColumn('custrecord_skuinfo1pickrule');
				columns3[4] = new nlobjSearchColumn('custrecord_skuinfo2pickrule');
				columns3[5] = new nlobjSearchColumn('custrecord_skuinfo3pickrule');
				columns3[6] = new nlobjSearchColumn('custrecord_skupickrule');
				columns3[7] = new nlobjSearchColumn('custrecord_abcvelpickrule');
				columns3[8] = new nlobjSearchColumn('custrecord_ebizsitepickput');
				columns3[9] = new nlobjSearchColumn('custrecord_ruleidpickrule');
				columns3[10] = new nlobjSearchColumn('custrecord_putawaymethod');
				columns3[11] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
				columns3[12] = new nlobjSearchColumn('custrecord_manual_location_generation');
				columns3[13] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
				columns3[14] = new nlobjSearchColumn('formulanumeric');
				columns3[14].setFormula("TO_NUMBER({custrecord_sequencenumberpickrule})");
				columns3[15] = new nlobjSearchColumn('custrecord_locationpickrule');
				//The below column is added by Satish.N on 29OCT2013 - Case # 20124515
				columns3[16] = new nlobjSearchColumn('custrecord_emptylocation','custrecord_putawaymethod');
				// Upto here on 29OCT2013 - Case # 20124515
				columns3[14].setSort();

				var putrulesearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters3, columns3);
				var NewLocArray=new Array();
				//NewLocArray=GetPutawayLocation(ItemId, getPackcode, itemstatus, getBaseUOM, parseFloat(TotalItemCube), ItemType,PoLoc);					
				//NewLocArray=vGetPutawayLocation(ItemId, getPackcode, itemstatus, getBaseUOM, parseFloat(TotalItemCube), ItemType,PoLoc,ItemLocId);
				//NewLocArray=GetPutawayLocationNew(ItemId, getPackcode, itemstatus, getBaseUOM, parseFloat(TotalItemCube), ItemType,PoLoc,ItemInfoResults,putrulesearchresults,ItemLocId)
				NewLocArray=vGetPutawayLocation(ItemId, getPackcode, itemstatus, getBaseUOM, parseFloat(TotalItemCube), 
						ItemType,PoLoc,ItemLocId,'','');
				if(NewLocArray!=null && NewLocArray.length>0)
				{
					if(ItemLocId!=NewLocArray[2])
					{
						var getBinLocationId = NewLocArray[2];
						var vLocationname= NewLocArray[0];
						nlapiLogExecution('ERROR', 'NewBinLocationId', getBinLocationId);
						nlapiLogExecution('ERROR', 'NewBinLocationName', vLocationname+"/"+NewLocArray[11]);
						IMarray["custparam_NewLocationId"] = getBinLocationId;
						IMarray["custparam_NewLocationName"] = vLocationname;
						IMarray["custparam_NewPutMethodID"] = NewLocArray[11];

						response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_endloc', 'customdeploy_rf_inventory_move_endloc_di', false, IMarray);
						nlapiLogExecution('ERROR', 'Navigating to same page', 'Success');
					}
					else
					{
						IMarray["custparam_error"] = st7;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);

					}

				}
				else
				{
					IMarray["custparam_error"] = st8;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
				}


			}
			//end of the code on 22Jun2012 by santosh
			else
			{
				// Case#20127699 starts
				var PoLoc=request.getParameter('hdnlocationId');
				nlapiLogExecution('ERROR', 'PoLoc is', PoLoc);
				//Case#20127699 end
				// Case # 20148685 starts
				var ItemId=request.getParameter('hdnItemId');
				// Case # 20148685 ends
				nlapiLogExecution('ERROR', 'getNewBinLocation', getNewBinLocation );
				nlapiLogExecution('ERROR', 'item id', request.getParameter('hdnItemId') );
				nlapiLogExecution('ERROR', 'item', request.getParameter('hdnItem') );
				nlapiLogExecution('ERROR', 'warehouse location', PoLoc);
				if(getNewBinLocation!=null&&getNewBinLocation!="")
				{


					if(PoLoc==null || PoLoc=='')
						PoLoc=getRoledBasedLocation();

					nlapiLogExecution('ERROR', 'warehouse location', PoLoc);

					// The below changes are done by Satish.N on 23-OCT-2013

					/*				
				var BinLocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null, new nlobjSearchFilter('name', null, 'is', getNewBinLocation.toUpperCase()));
				if (BinLocationSearch != null){
					var Location=BinLocationSearch[0].getId();}
				nlapiLogExecution('ERROR', 'Location id', Location );
					 */

					var Location='';
					var NewBinLocationFilters = new Array();
					NewBinLocationFilters[0] = new nlobjSearchFilter('name', null, 'is', getNewBinLocation);
					NewBinLocationFilters[1] = new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'anyof', ['6','7']);
					NewBinLocationFilters[2] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
					//Case# 20127327 Start
					if(PoLoc!=null && PoLoc!='' && PoLoc!='undefined' && PoLoc!=0)
					{
						NewBinLocationFilters[3] = new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'is', PoLoc);
					}
					//Case# 20127327 End
					var LocationColumns = new Array();
					LocationColumns[0]=new nlobjSearchColumn('isinactive'); 
					LocationColumns[1] = new nlobjSearchColumn('custrecord_inboundlocgroupid');
					LocationColumns[2]=new nlobjSearchColumn('custrecord_remainingcube');

					var NewLocationSearchResults = nlapiSearchRecord('customrecord_ebiznet_location', null, NewBinLocationFilters, LocationColumns);

					if (NewLocationSearchResults != null && NewLocationSearchResults.length > 0) 
					{
						nlapiLogExecution('ERROR', 'NewLocationSearchResults', NewLocationSearchResults.length);


//						case# 201413198
						var getNewBinLocId = NewLocationSearchResults[0].getId();
						Location = NewLocationSearchResults[0].getId();
						nlapiLogExecution('ERROR', 'Location Id', getNewBinLocId );
						IMarray["custparam_newbinlocationid"] = getNewBinLocId;
						IMarray["custparam_newbinlocationname"] = getNewBinLocation;
						var existinglp=getexistingLP(getNewBinLocId,getItemId,getItem,getStatus,getLOTNo,getMoveQuantity,IMarray["custparam_fifodate"]);
						IMarray["custparam_existingLP"] = existinglp;
						//var PickFaceLocation=getPickFaceLocation(Location,request.getParameter('hdnItemId'));
						var PickFaceLocation=getPickFaceLocation(Location,null);// to get pickface details for the New bin locations , no need to pass itemid
						IMarray["custparam_newLP"] = '';




						nlapiLogExecution('ERROR', 'NewLocationSearchResults', NewLocationSearchResults.length);
						var getNewBinLocId = NewLocationSearchResults[0].getId();
						nlapiLogExecution('ERROR', 'Location Id', getNewBinLocId );

						var vnewLocRemainingCube =  NewLocationSearchResults[0].getValue('custrecord_remainingcube');

						nlapiLogExecution('ERROR', 'vnewLocRemainingCube ',vnewLocRemainingCube);

						if(parseFloat(vnewLocRemainingCube)<=0 && getOldBinLocation !=getNewBinLocation)
						{
							IMarray["custparam_error"] = st10;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
							nlapiLogExecution('ERROR', 'LocationRemaininCube is zero', vnewLocRemainingCube);
							return;
						}
						else
						{
							//Case # 20125558 Start
							var itemCube;
							var ItemId=request.getParameter('hdnItemId');
							var filters = new Array();
							filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', ItemId);
							filters[1] = new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T');
							filters[2] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

							var columns = new Array();
							columns[0] = new nlobjSearchColumn('custrecord_ebizcube'); 
							var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

							//nlapiLogExecution('ERROR', 'Search Length of SKU Dims in inventory Move Screen',skuDimsSearchResults.length);

							if(skuDimsSearchResults != null && skuDimsSearchResults != "" ){
								nlapiLogExecution('ERROR', 'skuDimsSearchResults', skuDimsSearchResults.length);
								for (var i = 0; i < skuDimsSearchResults.length; i++) {
									var skuDim = skuDimsSearchResults[i];
									itemCube  =  skuDim.getValue('custrecord_ebizcube');		    	
								}
							}
							var vnewLocCube = parseFloat(vnewLocRemainingCube)  - (parseFloat(getMoveQuantity) * parseFloat(itemCube));

							if(parseFloat(vnewLocCube)<0 && getOldBinLocation !=getNewBinLocation)
							{
								IMarray["custparam_error"] = st10;
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
								nlapiLogExecution('ERROR', 'LocationRemaininCube is zero', vnewLocRemainingCube);
								return;
							}
							//Case # 20125558 End
							IMarray["custparam_newbinlocationid"] = getNewBinLocId;
							IMarray["custparam_newbinlocationname"] = getNewBinLocation;
							var existinglp=getexistingLP(getNewBinLocId,getItemId,getItem,getStatus,getLOTNo,getMoveQuantity);
							IMarray["custparam_existingLP"] = existinglp;
						}

						if(PickFaceLocation != null && PickFaceLocation !='')
						{
							nlapiLogExecution('ERROR', 'PickFaceLocation', PickFaceLocation.length);
							var moveallowed='N';
							for(var z = 0; z < PickFaceLocation.length; z++)
							{
								var pickfaceitem=PickFaceLocation[z].getValue('custrecord_pickfacesku');
								if(pickfaceitem == request.getParameter('hdnItemId'))
								{
									moveallowed='Y';
									if(PickFaceLocation[z].getValue('custrecord_pickface_ebiz_lpno') != null && PickFaceLocation[z].getValue('custrecord_pickface_ebiz_lpno') != '')
									{	
										ItemNewLP=PickFaceLocation[z].getText('custrecord_pickface_ebiz_lpno');
										IMarray["custparam_newLP"]=ItemNewLP;
									}
								}
							}

							if(moveallowed=='N')
							{
								IMarray["custparam_newbinlocationname"]='';								
								IMarray["custparam_error"] = getNewBinLocation.toUpperCase() + ' IS THE PICKFACE FOR ANOTHER ITEM. MOVE IS NOT ALLOWED.';
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);						
								return;
							}

							if(vLprequired=='N')
							{

								if ((itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.custitem_ebizserialin == 'T' || itemSubtype.custitem_ebizserialout == 'T')&&(getAvailQuantity!=getMoveQty)) 
								{

									response.sendRedirect('SUITELET', 'customscript_rf_inventory_serialscan', 'customdeploy_rf_inventory_serialscan_di', false, IMarray);
									return;
								}


								response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_complete', 'customdeploy_rf_inventory_move_comple_di', false, IMarray);
								return;
							}
							else
							{
								response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_lp', 'customdeploy_rf_inventory_move_lp_di', false, IMarray);
								return;
							}

						}


						var inactiveflag = NewLocationSearchResults[0].getValue('isinactive');		
						var getNewBinLocGrpId = NewLocationSearchResults[0].getValue('custrecord_inboundlocgroupid');

						var vValidNewBinarr= vValidateNewBin(getNewBinLocation,getNewBinLocGrpId,ItemId, newItemStatus, PoLoc);

						if(vValidNewBinarr==null || vValidNewBinarr=='')
						{
							IMarray["custparam_error"] = 'Invalid Bin Location';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
							return;
						}
						else
						{
							if(vValidNewBinarr.length>0)
							{
								vValidNewBin = vValidNewBinarr[0];
								
								if(vValidNewBin=='F')
								{
									IMarray["custparam_error"] = 'Invalid Bin Location';
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
									return;
								}
								else
								{
									IMarray["custparam_NewPutMethodID"] = vValidNewBinarr[1];
								}
							}
							else
							{
								IMarray["custparam_error"] = 'Invalid Bin Location';
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
								return;
							}
						}
						/*if(inactiveflag=='T')
						{
							IMarray["custparam_error"] = 'INACTIVE LOCATION';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
							nlapiLogExecution('ERROR', 'INACTIVE LOCATION', getBinLocation);
							return;
						}*/

						/*}  				  
					else 
					{
						IMarray["custparam_error"] = st9;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
						nlapiLogExecution('ERROR', 'INVALID LOCATION', getBinLocation);
						return;
					}*/

						//Upto here by Satish.N on 23-OCT-2013.
						//added on 22/01/15
						//var PickFaceLocation=getPickFaceLocation(Location);

						/*var NewBinLocationFilters = new Array();
						NewBinLocationFilters[0] = new nlobjSearchFilter('name', null, 'is', getNewBinLocation);
						NewBinLocationFilters[1] = new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'anyof', ['6','7']);

						var newLocationColumns = new Array();
						newLocationColumns[0]=new nlobjSearchColumn('custrecord_remainingcube');     

						var NewLocationSearchResults = nlapiSearchRecord('customrecord_ebiznet_location', null, NewBinLocationFilters, newLocationColumns);

						if (NewLocationSearchResults != null && NewLocationSearchResults.length > 0) 
						{*/
						/*nlapiLogExecution('ERROR', 'NewLocationSearchResults', NewLocationSearchResults.length);
						var getNewBinLocId = NewLocationSearchResults[0].getId();
						nlapiLogExecution('ERROR', 'Location Id', getNewBinLocId );

						var vnewLocRemainingCube =  NewLocationSearchResults[0].getValue('custrecord_remainingcube');

						nlapiLogExecution('ERROR', 'vnewLocRemainingCube ',vnewLocRemainingCube);

						if(parseFloat(vnewLocRemainingCube)<=0)
						{
							IMarray["custparam_error"] = st10;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
							nlapiLogExecution('ERROR', 'LocationRemaininCube is zero', vnewLocRemainingCube);
							return;
						}
						else
						{
							//Case # 20125558 Start
							var itemCube;
							var ItemId=request.getParameter('hdnItemId');
							var filters = new Array();
							filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', ItemId);
							filters[1] = new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T');
							filters[2] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

							var columns = new Array();
							columns[0] = new nlobjSearchColumn('custrecord_ebizcube'); 
							var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

							//nlapiLogExecution('ERROR', 'Search Length of SKU Dims in inventory Move Screen',skuDimsSearchResults.length);

							if(skuDimsSearchResults != null && skuDimsSearchResults != "" ){
								nlapiLogExecution('ERROR', 'skuDimsSearchResults', skuDimsSearchResults.length);
								for (var i = 0; i < skuDimsSearchResults.length; i++) {
									var skuDim = skuDimsSearchResults[i];
									itemCube  =  skuDim.getValue('custrecord_ebizcube');		    	
								}
							}
							var vnewLocCube = parseFloat(vnewLocRemainingCube)  - (parseFloat(getMoveQuantity) * parseFloat(itemCube));

							if(parseFloat(vnewLocCube)<0)
							{
								IMarray["custparam_error"] = st10;
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
								nlapiLogExecution('ERROR', 'LocationRemaininCube is zero', vnewLocRemainingCube);
								return;
							}
							//Case # 20125558 End
							IMarray["custparam_newbinlocationid"] = getNewBinLocId;
							IMarray["custparam_newbinlocationname"] = getNewBinLocation;
							var existinglp=getexistingLP(getNewBinLocId,getItemId,getItem,getStatus,getLOTNo,getMoveQuantity);
							IMarray["custparam_existingLP"] = existinglp;
						}*/
//						}
					}  				  
					else 
					{
						IMarray["custparam_error"] = st9;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
						nlapiLogExecution('ERROR', 'INVALID LOCATION', getBinLocation);
						return;
					}
				}  				  
				else 
				{
					IMarray["custparam_error"] = st9;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
					nlapiLogExecution('ERROR', 'INVALID LOCATION', getBinLocation);
					return;
				}
//				if(getNewBinLocation == request.getParameter('hdnBinLocation'))
//				{
//				IMarray["custparam_error"] = 'Are you sure want move the inventory to the same bin location?';
//				IMarray["custparam_screenno"] = '22A';
//				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
//				nlapiLogExecution('ERROR', 'Are you sure want move the inventory to the same bin location?', getBinLocation);
//				return;
//				}

				nlapiLogExecution('ERROR', 'getNewBinLocId ', IMarray["custparam_newbinlocationid"]);
				nlapiLogExecution('ERROR', 'getNewBinLocation ', IMarray["custparam_newbinlocationname"]);
				nlapiLogExecution('ERROR', 'getInvtRecID ', IMarray["custparam_invtrecid"]);	


				nlapiLogExecution('ERROR', 'vLprequired@123 ', vLprequired);
				//IMarray["custparam_newLP"] = '';
				if(vLprequired=='N')
				{

					if ((itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.custitem_ebizserialin == 'T' || itemSubtype.custitem_ebizserialout == 'T')&&(getAvailQuantity!=getMoveQty)) 
					{

						response.sendRedirect('SUITELET', 'customscript_rf_inventory_serialscan', 'customdeploy_rf_inventory_serialscan_di', false, IMarray);
						return;
					}
					response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_complete', 'customdeploy_rf_inventory_move_comple_di', false, IMarray);

				}					
				else
					response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_lp', 'customdeploy_rf_inventory_move_lp_di', false, IMarray);


				//response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_lp', 'customdeploy_rf_inventory_move_lp_di', false, IMarray);

//				try 
//				{	 


//				response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_endloc', 'customdeploy_rf_inventory_move_endloc_di', false, IMarray);
//				nlapiLogExecution('ERROR', 'Navigating to To LP', 'Success');
//				}
//				catch (e) 
//				{
//				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
//				nlapiLogExecution('ERROR', 'Catching the Search Results', 'Length is null');
//				}
//				nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
			}
		}        
	}
}

function getexistingLP(location,getItemId,getItem,getStatus,lot,getMoveQuantity,fifodate)
{
	nlapiLogExecution('ERROR', 'Into getexistingLP','');

	var existingLP;
	var maxuomqty=0;
	var qtyonhand=0;

	try {		

		nlapiLogExecution('ERROR', 'Location',location);
		nlapiLogExecution('ERROR', 'Item Internal Id',getItemId);
		nlapiLogExecution('ERROR', 'Item',getItem);
		nlapiLogExecution('ERROR', 'Item Status',getStatus);
		nlapiLogExecution('ERROR', 'LOT',lot);
		nlapiLogExecution('ERROR', 'Move Qty',getMoveQuantity);

		maxuomqty = getmaxuomqty(getItemId);

		if(maxuomqty!=null && maxuomqty!='')
			maxuomqty = parseFloat(maxuomqty);
		else
			maxuomqty=0;

		nlapiLogExecution('ERROR', 'Max. UOM Qty',maxuomqty);

		var filtersmlp = new Array();

		if( location !="" && location != null){
			filtersmlp.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', location));
		}
		if( getItemId !="" && getItemId != null){
			filtersmlp.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', getItemId));
		}
		if( getStatus !="" && getStatus != null){
			filtersmlp.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', getStatus));
		}
		if( lot !="" && lot != null){
			filtersmlp.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', [lot]));
		}
		nlapiLogExecution('ERROR', 'fifodate',fifodate);
		if(fifodate!=null&&fifodate!="")
		{
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_fifo', null, 'on', fifodate));
		}
		filtersmlp.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['3','19']));//Storage Inventory
		filtersmlp.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthanorequalto', 0));

		var col=new Array();

		col[0] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
		col[1] = new nlobjSearchColumn('custrecord_ebiz_qoh');		
		col[1].setSort('True');

		var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersmlp,col);

		if (SrchRecord != null && SrchRecord !="" && SrchRecord.length>0)
		{	
			for (var i=0;i<SrchRecord.length;i++)
			{
				qtyonhand = SrchRecord[i].getValue('custrecord_ebiz_qoh');

				//Move Qty plus Available Qty of the existing LP should not be greater than Max.UOM Qty
				if((parseFloat(qtyonhand)+parseFloat(getMoveQuantity))<=parseFloat(maxuomqty))
				{
					existingLP = SrchRecord[i].getValue('custrecord_ebiz_inv_lp');
					nlapiLogExecution('ERROR', 'out of existingLP',existingLP);
					return existingLP;
				}
			}
		}
	} 
	catch (e) 
	{
		nlapiLogExecution('ERROR', 'Failed to Update/Insert into Master LP Record');
	}
	nlapiLogExecution('ERROR', 'out of existingLP',existingLP);
	return existingLP;
}

function getPickFaceLocation(location,item)
{
	nlapiLogExecution('ERROR', 'Into getPickFaceLocation',location+","+item);
	var Result='';
	var PickFaceSearchResults = new Array();
	var PickFaceFilters = new Array();
	var PickFaceColumns = new Array();
	if(item!=null && item!='' && item!='null')
		PickFaceFilters.push(new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', item)); 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_pickbinloc', null, 'anyof', location)); 
	PickFaceFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	//PickFaceFilters.push(new nlobjSearchFilter('custrecord_priorityputaway', null, 'is', 'T'));

	PickFaceColumns[0] = new nlobjSearchColumn('custrecord_pickbinloc');
	PickFaceColumns[1] = new nlobjSearchColumn('custrecord_pickfacesku');
	PickFaceColumns[2] = new nlobjSearchColumn('custrecord_pickface_ebiz_lpno');

	PickFaceSearchResults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, PickFaceFilters, PickFaceColumns);

	nlapiLogExecution('ERROR', 'Return Value',PickFaceSearchResults);
	nlapiLogExecution('ERROR', 'Out of getPickFaceLocation');

	return PickFaceSearchResults;
}

function getmaxuomqty(getItemId)
{
	nlapiLogExecution('ERROR', 'Into getmaxuomqty','');
	nlapiLogExecution('ERROR', 'Item ID',getItemId);

	var maxuomqty=0;
	var filtersmlp = new Array();

	filtersmlp.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', getItemId));
	filtersmlp.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var col=new Array();
	col[0] = new nlobjSearchColumn('custrecord_ebizqty');
	col[0].setSort('True');

	var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filtersmlp,col);
	if (SrchRecord != null && SrchRecord !="" && SrchRecord.length>0)
	{
		maxuomqty = SrchRecord[0].getValue('custrecord_ebizqty');
	}

	nlapiLogExecution('ERROR', 'Out of getmaxuomqty',maxuomqty);
	return maxuomqty;
}

//code on 22Jun2012 by santosh
function CubeCapacity(RcvQty,BaseUOMQty,cube)
{
	nlapiLogExecution('ERROR', 'CubeCapacity : RcvQty', RcvQty);
	nlapiLogExecution('ERROR', 'CubeCapacity : BaseUOMQty', BaseUOMQty);
	nlapiLogExecution('ERROR', 'CubeCapacity : cube', cube);
	try{
		var itemCube;
		if (cube != "" && cube != null) 
		{
			if (!isNaN(cube)) 
			{
				var uomqty = ((parseFloat(RcvQty))/(parseFloat(BaseUOMQty)));			
				nlapiLogExecution('ERROR', 'uomqty', uomqty);
				itemCube = (parseFloat(uomqty) * parseFloat(cube));
				nlapiLogExecution('ERROR', 'ItemCube', itemCube);
			}
			else 
			{
				itemCube = 0;
			}
		}
		else 
		{
			itemCube = 0;
		}	
		nlapiLogExecution('ERROR','itemcube',itemCube);
		return itemCube;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in cubecapacity',exp);
	}
}
//End of code on 22Jun2012 by santosh

function vGetPutawayLocation1(Item, PackCode, ItemStatus, UOM, ItemCube, ItemType,poLocn,vItemLocId){
	var context = nlapiGetContext();
	nlapiLogExecution('ERROR','Remaining usage at the start',context.getRemainingUsage());
	var ItemFamily, ItemGroup, Location;
	var location_found = false;
	var LocArry = new Array();
	var OBLocGroup, IBLocGroup;
	var PickSeqNo, PutSeqNo, PutMethod,PutMethodName,PutRuleId, MergeFlag = 'F',Mixsku='F',PutBinLoc;
	nlapiLogExecution('ERROR', 'ItemCube', ItemCube);//0
	nlapiLogExecution('ERROR', 'ItemType', ItemType);
	var itemresults;

	var fields = ['custitem_item_family','custitem_item_group','custitem_ebizdefskustatus',
	              'custitem_item_info_1','custitem_item_info_2','custitem_item_info_3','custitem_ebizabcvelitem'];


	var columns= nlapiLookupField('item',Item,fields);
	var ItemFamily = columns.custitem_item_family;
	var ItemGroup = columns.custitem_item_group;
	var ItemInfo1 = columns.custitem_item_info_1;
	var ItemInfo2 = columns.custitem_item_info_2;
	var ItemInfo3 = columns.custitem_item_info_3;
	var putVelocity=columns.custitem_ebizabcvelitem;

	nlapiLogExecution('ERROR', 'Item', Item);
	nlapiLogExecution('ERROR', 'ItemFamily', ItemFamily);
	nlapiLogExecution('ERROR', 'ItemGroup', ItemGroup);
	nlapiLogExecution('ERROR', 'ItemStatus', ItemStatus);
	nlapiLogExecution('ERROR', 'ItemInfo1', ItemInfo1);
	nlapiLogExecution('ERROR', 'ItemInfo2', ItemInfo2);
	nlapiLogExecution('ERROR', 'ItemInfo3', ItemInfo3);
	nlapiLogExecution('ERROR', 'putVelocity', putVelocity);
	nlapiLogExecution('ERROR', 'poLocn', poLocn);
	nlapiLogExecution('ERROR', 'vItemLocId', vItemLocId);

	var locgroupid = 0;
	var zoneid = 0;

	// if (ItemFamily != null || ItemFamily != "") {
	var columns = new Array();
	var i=0;
	var filters = new Array();
	columns[0] = new nlobjSearchColumn('formulanumeric');
	columns[0].setFormula("TO_NUMBER({custrecord_sequencenumberpickrule})");
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
	columns[2] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
	columns[3] = new nlobjSearchColumn('custrecord_putawaymethod');
	columns[4] = new nlobjSearchColumn('custrecord_ruleidpickrule');
	columns[5] = new nlobjSearchColumn('custrecord_manual_location_generation');
	columns[6] = new nlobjSearchColumn('custrecord_locationpickrule');
	var ItemFamArr = new Array();
	ItemFamArr[0] = ItemFamily;
	ItemFamArr[1] = '@NONE@';

	if(ItemFamily!=null && ItemFamily != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skufamilypickrule', null, 'anyof', ['@NONE@', ItemFamily]));
	}

	if(ItemGroup!=null && ItemGroup != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skugrouppickrule', null, 'anyof', ['@NONE@', ItemGroup]));
	}

	if(ItemStatus!=null && ItemStatus != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skustatuspickrule', null, 'anyof', ['@NONE@', ItemStatus]));
	}

	if(ItemInfo1!=null && ItemInfo1 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo1pickrule', null, 'anyof',['@NONE@', ItemInfo1]));
	}

	if(ItemInfo2!=null && ItemInfo2 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo2pickrule', null, 'anyof',['@NONE@', ItemInfo2]));
	}

	if(ItemInfo3!=null && ItemInfo3 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo3pickrule', null, 'anyof',['@NONE@', ItemInfo3]));
	}

	if(Item != null && Item != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skupickrule', null, 'anyof', ['@NONE@', Item]));		
	}

	if(putVelocity != null && putVelocity != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_abcvelpickrule', null, 'anyof', ['@NONE@', putVelocity]));
	}

	if(poLocn != null && poLocn != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizsitepickput', null, 'anyof', [poLocn]));
	}

	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var t1 = new Date();
	var rulesearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters, columns);
	var t2 = new Date();
	nlapiLogExecution('ERROR', 'nlapiSearchRecord:GetPutawayLocation:putaway_rule',
			getElapsedTimeDuration(t1, t2));

	if (rulesearchresults != null) {
		nlapiLogExecution('ERROR', 'rules count', rulesearchresults.length);
		for (var s = 0; s < rulesearchresults.length; s++) {
			locgroupid = rulesearchresults[s].getValue('custrecord_locationgrouppickrule');
			zoneid = rulesearchresults[s].getValue('custrecord_putawayzonepickrule');
			PutMethod = rulesearchresults[s].getValue('custrecord_putawaymethod');
			PutMethodName = rulesearchresults[s].getText('custrecord_putawaymethod');
			PutRuleId = rulesearchresults[s].getValue('custrecord_ruleidpickrule');
			PutBinLoc = rulesearchresults[s].getValue('custrecord_locationpickrule');
			nlapiLogExecution('ERROR', 'Put Rule', PutRuleId);
			nlapiLogExecution('ERROR', 'Put Method', PutMethod);
			nlapiLogExecution('ERROR', 'Location Group Id from Putaway Rule', locgroupid);
			nlapiLogExecution('DEBUG', 'PutBinLoc', PutBinLoc);

			// To indicate if the location has to be generated manually based on the parameter in putaway strategy
			ManualLocationGeneration = rulesearchresults[s].getValue('custrecord_manual_location_generation');
			nlapiLogExecution('ERROR', 'Manual Location Generation', ManualLocationGeneration);

			if (ManualLocationGeneration == 'F') {
				if (locgroupid != 0 && locgroupid != null) {
					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_startingputseqno');
					columns[0].setSort();
					columns[1] = new nlobjSearchColumn('name');
					columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid');
					columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno');
					columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid');
					columns[5] = new nlobjSearchColumn('custrecord_remainingcube');	

					nlapiLogExecution('ERROR', 'locgroupid', locgroupid);

					var filters = new Array();
					filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [locgroupid]));	
					filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
					if(poLocn != null && poLocn != "")
						filters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof', [poLocn]));
					//if(vItemLocId!=null&&vItemLocId!="")
					if((vItemLocId!=null && vItemLocId!="" && vItemLocId !='null')&& (PutBinLoc == null || PutBinLoc == ""))
						filters.push(new nlobjSearchFilter('internalid', null, 'noneof', vItemLocId));
					if(PutBinLoc != null && PutBinLoc != "")
						filters.push(new nlobjSearchFilter('internalid', null, 'is', PutBinLoc));
					t1 = new Date();
					var locResults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);
					t2 = new Date();
					nlapiLogExecution('ERROR', 'nlapiSearchRecord:GetPutawayLocation:location',
							getElapsedTimeDuration(t1, t2));

					nlapiLogExecution('ERROR', 'locResults', locResults);
					if (locResults != null) {
						nlapiLogExecution('ERROR', 'ifnot ');
						for (var u = 0; u < locResults.length; u++) {

							Location = locResults[u].getValue('name');
							var locationIntId = locResults[u].getId();

							nlapiLogExecution('ERROR', 'Location', Location);
							nlapiLogExecution('ERROR', 'Location Id', locationIntId);

							//Commented by satish.N on 04-JAN-2012 as the search results will give the same value.
							//LocRemCube = GeteLocCube(locationIntId);
							LocRemCube = locResults[u].getValue('custrecord_remainingcube');							
							OBLocGroup = locResults[u].getValue('custrecord_outboundinvlocgroupid');
							PickSeqNo  = locResults[u].getValue('custrecord_startingpickseqno');
							IBLocGroup = locResults[u].getValue('custrecord_inboundinvlocgroupid');
							PutSeqNo   = locResults[u].getValue('custrecord_startingputseqno');

							nlapiLogExecution('ERROR', 'LocRemCube', LocRemCube);
							nlapiLogExecution('ERROR', 'ItemCube', ItemCube);

							if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {
								RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);
								location_found = true;

								LocArry[0] = Location;
								LocArry[1] = RemCube;
								LocArry[2] = locationIntId;	
								LocArry[3] = OBLocGroup;
								LocArry[4] = PickSeqNo;
								LocArry[5] = IBLocGroup;
								LocArry[6] = PutSeqNo;
								LocArry[7] = LocRemCube;
								LocArry[8] = zoneid;
								LocArry[9] = PutMethodName;
								LocArry[10] = PutRuleId;
								LocArry[11] = PutMethod;

								nlapiLogExecution('ERROR', 'SA-Location', Location);
								nlapiLogExecution('ERROR', 'Location Internal Id',locationIntId);
								nlapiLogExecution('ERROR', 'Sa-Location Cube', RemCube);
								return LocArry;
							}

						}
					}
				}
				else {
					nlapiLogExecution('ERROR', 'Fetching Location Group from Zone', zoneid);
					var mixarray =  new Array();
					if(PutMethod!=null && PutMethod!='')
						mixarray=GetMergeFlag(PutMethod);
					if(mixarray!=null && mixarray!='' && mixarray.length>0)
					{
						MergeFlag = mixarray[0][0];
						Mixsku = mixarray[0][1];
					}
					nlapiLogExecution('ERROR', 'Merge Flag', MergeFlag);
					nlapiLogExecution('ERROR', 'Mix SKU', Mixsku);

					if (zoneid != null && zoneid != ""){ 
						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_zone_seq');
						columns[0].setSort();
						columns[1] = new nlobjSearchColumn('custrecord_locgroup_no');

						var filters = new Array();
						filters.push(new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'is', zoneid));
						filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
						filters.push(new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', ['@NONE@', poLocn]));
						t1 = new Date();
						var searchresults = nlapiSearchRecord('customrecord_zone_locgroup', null, filters, columns);
						t2 = new Date();
						nlapiLogExecution('ERROR', 'nlapiSearchRecord:GetPutawayLocation:zone_locgroup',
								getElapsedTimeDuration(t1, t2));

						var RemCube = 0;
						var LocRemCube = 0;

						if (searchresults != null && searchresults != '') 
						{

							for (var i = 0; i < Math.min(30, searchresults.length); i++) {
								locgroupid = searchresults[i].getValue('custrecord_locgroup_no');
								nlapiLogExecution('ERROR', 'Fetched Location Group Id from Putaway Zone', locgroupid);

								if (MergeFlag == 'T') {
									nlapiLogExecution('ERROR', 'Inside Merge ');
									var locResults;
									nlapiLogExecution('ERROR', 'ItemCube', ItemCube);
									nlapiLogExecution('ERROR', 'poLocn', poLocn);
									nlapiLogExecution('ERROR', 'locgroupid', locgroupid);

									try {
										var columns = new Array();
										columns[0] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');
										columns[0].setSort();
										columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
										columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc');
										columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno', 'custrecord_ebiz_inv_binloc');
										columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc');
										columns[5] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');
										columns[6] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
										columns[7] = new nlobjSearchColumn('custrecord_remainingcube','custrecord_ebiz_inv_binloc');

										var filters = new Array();
										if(Mixsku!='T')
										{
											filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', Item));
										}
										filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
										filters.push(new nlobjSearchFilter('isinactive', 'custrecord_ebiz_inv_binloc', 'is', 'F'));
										if(poLocn!=null && poLocn!='')
											filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', poLocn));
										filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'is', locgroupid));
										filters.push(new nlobjSearchFilter('custrecord_remainingcube', 'custrecord_ebiz_inv_binloc', 'greaterthanorequalto', ItemCube));                                        
										if(vItemLocId!=null&&vItemLocId!="")
											filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'noneof', vItemLocId));
										t1 = new Date();
										locResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
										t2 = new Date();
										nlapiLogExecution('ERROR', 'nlapiSearchRecord:GetPutawayLocation:createinv',
												getElapsedTimeDuration(t1, t2));
									} //try close.
									catch (Error) {
										nlapiLogExecution('ERROR', 'Into Error', Error);
									}
									try {
										nlapiLogExecution('ERROR', 'Inside Try', 'TRY');

										if (locResults != null && locResults != '') {
											nlapiLogExecution('ERROR', 'LocationResults', locResults.length );
											for (var j = 0; j < locResults.length; j++) {
												nlapiLogExecution('ERROR', 'locResults[j].getId()', locResults[j].getId());
												Location = locResults[j].getText('custrecord_ebiz_inv_binloc');
												var locationInternalId = locResults[j].getValue('custrecord_ebiz_inv_binloc');

												nlapiLogExecution('ERROR', 'Location', Location );
												nlapiLogExecution('ERROR', 'Location Id', locationInternalId);

												//LocRemCube = GeteLocCube(locationInternalId);
												LocRemCube = locResults[j].getValue('custrecord_remainingcube', 'custrecord_ebiz_inv_binloc');
												OBLocGroup = locResults[j].getValue('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc');
												PickSeqNo = locResults[j].getValue('custrecord_startingpickseqno', 'custrecord_ebiz_inv_binloc');
												IBLocGroup = locResults[j].getValue('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc');
												PutSeqNo = locResults[j].getValue('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');

												nlapiLogExecution('ERROR', 'Loc Rem Cube', LocRemCube);
												nlapiLogExecution('ERROR', 'Item Cube', ItemCube);

												if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {

													RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);

													location_found = true;

													LocArry[0] = Location;
													LocArry[1] = RemCube;
													LocArry[2] = locationInternalId;		//locResults[j].getId();
													LocArry[3] = OBLocGroup;
													LocArry[4] = PickSeqNo;
													LocArry[5] = IBLocGroup;
													LocArry[6] = PutSeqNo;
													LocArry[7] = LocRemCube;
													LocArry[8] = zoneid;
													LocArry[9] = PutMethodName;
													LocArry[10] = PutRuleId;
													LocArry[11] = PutMethod;
													nlapiLogExecution('ERROR', 'after changes Location Cube', RemCube);
													return LocArry;
												}
											}
										}
									} 
									catch (exps) {
										nlapiLogExecution('ERROR', 'included exps', exps);
									}

									//Added by satish.N on 04-JAN-2012 to consider open taks while merging.
									if (location_found == false) {

										try{
											nlapiLogExecution('ERROR', 'Inside  consider open taks while merging');

											var columns = new Array();
											columns[0] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_actbeginloc');
											columns[0].setSort();
											columns[1] = new nlobjSearchColumn('custrecord_actbeginloc');
											columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid', 'custrecord_actbeginloc');
											columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno', 'custrecord_actbeginloc');
											columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid', 'custrecord_actbeginloc');
											columns[5] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_actbeginloc');
											columns[6] = new nlobjSearchColumn('custrecord_sku');
											columns[7] = new nlobjSearchColumn('custrecord_remainingcube','custrecord_actbeginloc');

											var filters = new Array();
											if(Mixsku!='T')
											{
												filters.push(new nlobjSearchFilter('custrecord_sku', null, 'is', Item));
											}	
											if(poLocn!=null && poLocn!='')
												filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', [poLocn]));
											filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
											filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_actbeginloc', 'is', locgroupid));
											filters.push(new nlobjSearchFilter('custrecord_remainingcube', 'custrecord_actbeginloc', 'greaterthanorequalto', ItemCube));

											locResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

											if (locResults != null && locResults != '') {
												for (var j = 0; j < locResults.length; j++) {

													Location = locResults[j].getText('custrecord_actbeginloc');
													var locationInternalId = locResults[j].getValue('custrecord_actbeginloc');

													nlapiLogExecution('ERROR', 'Location', Location );
													nlapiLogExecution('ERROR', 'Location Id', locationInternalId);

													LocRemCube = locResults[j].getValue('custrecord_remainingcube', 'custrecord_actbeginloc');
													OBLocGroup = locResults[j].getValue('custrecord_outboundlocgroupid', 'custrecord_actbeginloc');
													PickSeqNo = locResults[j].getValue('custrecord_startingpickseqno', 'custrecord_actbeginloc');
													IBLocGroup = locResults[j].getValue('custrecord_inboundlocgroupid', 'custrecord_actbeginloc');
													PutSeqNo = locResults[j].getValue('custrecord_startingputseqno', 'custrecord_actbeginloc');

													nlapiLogExecution('ERROR', 'Loc Rem Cube', LocRemCube);
													nlapiLogExecution('ERROR', 'Item Cube', ItemCube);

													if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {

														RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);

														location_found = true;

														LocArry[0] = Location;
														LocArry[1] = RemCube;
														LocArry[2] = locationInternalId;		//locResults[j].getId();
														LocArry[3] = OBLocGroup;
														LocArry[4] = PickSeqNo;
														LocArry[5] = IBLocGroup;
														LocArry[6] = PutSeqNo;
														LocArry[7] = LocRemCube;
														LocArry[8] = zoneid;
														LocArry[9] = PutMethodName;
														LocArry[10] = PutRuleId;
														LocArry[11] = PutMethod;
														nlapiLogExecution('ERROR', 'after changes Location Cube', RemCube);
														return LocArry;
													}
												}										
											}
										}
										catch(exps){
											nlapiLogExecution('ERROR', 'included exps in opentasks', exps);
										}
									}
								}
								if (location_found == false) {

									//code modified by suman on 27/01/12
									//Instead of searching each and individual binloc in inventory and open task record it seems to have a goverence issue.
									//So I bought all binloc from open task and inventory records depending upon "locgr"Id and placing it in an array so that on comparing this array with the binlocation 
									//will decides weather it is empty or not.

									nlapiLogExecution('ERROR', 'Location Not found for Merge Location for Location Group', locgroupid);
									var locResults = GetLocation(locgroupid,ItemCube);

									if (locResults != null && locResults != '') {

										var TotalListOfBinLoc=new Array();
										for ( var x = 0; x < locResults.length; x++) 
										{
											TotalListOfBinLoc[x]=locResults[x].getId();

										}
										var emptyloccheck =	binLocationChecknew(TotalListOfBinLoc);
										nlapiLogExecution('ERROR', 'emptyloccheck', emptyloccheck);

										nlapiLogExecution('ERROR', 'locResults.length', locResults.length);
										for (var j = 0; j < Math.min(300, locResults.length); j++) {
											var LocFlag='N';
											var emptyBinLocationId = locResults[j].getId();
											nlapiLogExecution('ERROR', 'emptyBinLocationId', emptyBinLocationId);
//											var emptyloccheck =	binLocationCheck(emptyBinLocationId);
//											nlapiLogExecution('ERROR', 'emptyloccheck', emptyloccheck);

											for ( var InvBinLocCount = 0; InvBinLocCount < emptyloccheck[0].length; InvBinLocCount++)
											{
//												nlapiLogExecution('ERROR', 'emptyloccheck[0][InvBinLocCount]', emptyloccheck[0][InvBinLocCount]);

												if(emptyloccheck[0][InvBinLocCount]==emptyBinLocationId)
												{
													nlapiLogExecution('ERROR', 'emptyBinLocationId+InvBinLocCount', emptyBinLocationId+','+emptyloccheck[0][InvBinLocCount]);
													LocFlag='Y';
													break;
												}
											}
											for ( var OpenTaskBinLocCount = 0; OpenTaskBinLocCount < emptyloccheck[1].length; OpenTaskBinLocCount++)
											{
												if(emptyloccheck[1][OpenTaskBinLocCount]==emptyBinLocationId)
												{
													nlapiLogExecution('ERROR', 'emptyBinLocationId+OpenTaskBinLocCount', emptyBinLocationId+','+emptyloccheck[1][OpenTaskBinLocCount]);
													LocFlag='Y';
													break;
												}
											}
//											end of code changed 27/01/12 
											nlapiLogExecution('ERROR', 'LocFlag', LocFlag);
											nlapiLogExecution('ERROR', 'Main Loop ', s);
											nlapiLogExecution('ERROR','Remaining usage ',context.getRemainingUsage());
											if(LocFlag == 'N'){
												//Changes done by Sarita (index i is changed to j).
												LocRemCube = locResults[j].getValue('custrecord_remainingcube');
												OBLocGroup = locResults[j].getValue('custrecord_outboundlocgroupid');
												PickSeqNo = locResults[j].getValue('custrecord_startingpickseqno');
												IBLocGroup = locResults[j].getValue('custrecord_inboundlocgroupid');
												PutSeqNo = locResults[j].getValue('custrecord_startingputseqno');

												nlapiLogExecution('ERROR', 'LocRemCube', LocRemCube);
												nlapiLogExecution('ERROR', 'ItemCube', ItemCube);

												if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {
													RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);
													location_found = true;
													Location = locResults[j].getValue('name');
													LocArry[0] = Location;
													LocArry[1] = RemCube;
													LocArry[2] = locResults[j].getId();
													LocArry[3] = OBLocGroup;
													LocArry[4] = PickSeqNo;
													LocArry[5] = IBLocGroup;
													LocArry[6] = PutSeqNo;
													LocArry[7] = LocRemCube;
													LocArry[8] = zoneid;
													LocArry[9] = PutMethodName;
													LocArry[10] = PutRuleId;
													LocArry[11] = PutMethod;
													nlapiLogExecution('ERROR', 'Location', Location);
													nlapiLogExecution('ERROR', 'Location Cube', RemCube);
													return LocArry;
													break;
												}
											}
										}
									}
								}
							}
						}//end of searchresult null checking
					}
				}
				nlapiLogExecution('ERROR', 'Main Loop ', s);
				nlapiLogExecution('ERROR','Remaining usage ',context.getRemainingUsage());
			}
		}
	}// end of put rule for loop
	else
	{
		nlapiLogExecution('ERROR', 'No Searchresults with above mentioned criteria ', 'Null Results');
	}
	//  } // end of main if
	return LocArry;
}


function vValidateNewBin(getNewBinLocId,getNewBinLocGrpId,ItemId, itemstatus, PoLoc)
{
	//alert('hi223rr');
	var vValidLoc=false;
	var LocArry = new Array();
	// case no 20126185
	var vZoneIdSearch='';
	if(getNewBinLocGrpId != null && getNewBinLocGrpId != '')
		vZoneIdSearch=vGetZoneIdByLocGroup(getNewBinLocGrpId,PoLoc);
	var vMapZoneId = null;
	if(vZoneIdSearch != null && vZoneIdSearch != '')
	{
		//var vMapZoneId=new Array();
		vMapZoneId = new Array();
		for(var s=0;s<vZoneIdSearch.length;s++ )
		{
			vMapZoneId.push(vZoneIdSearch[s].getValue('custrecordcustrecord_putzoneid'));	
		}	
	}// 201413301
//	zone is not considered here for putrule because while auto generating loc,in generate putrule function we are considering either zone or locgrp so here also we removing zone madatory for putrule
	//alert('ItemId,itemstatus,PoLoc,vMapZoneId,getNewBinLocGrpId' + ItemId + ',' + itemstatus + ',' + PoLoc + ',' + vMapZoneId + ',' + getNewBinLocGrpId);
	var vPutRules=vGetPutawayRules(ItemId,itemstatus,PoLoc,vMapZoneId,getNewBinLocGrpId);	
	if(vPutRules != null && vPutRules != '')
	{
		vValidLoc=true;
		//alert('vValidLoc' +vValidLoc);

		LocArry[0] = vValidLoc;
		LocArry[1] = vPutRules[0].getValue('custrecord_putawaymethod');
	}



	return LocArry;
}

function vGetPutawayRules(Item,ItemStatus,poLocn,NewPutZoneId,NewPutLocGrpId)
{
	var itemresults;

	var fields = ['custitem_item_family','custitem_item_group','custitem_ebizdefskustatus',
	              'custitem_item_info_1','custitem_item_info_2','custitem_item_info_3','custitem_ebizabcvelitem'];

	var columns= nlapiLookupField('item',Item,fields);
	var ItemFamily = columns.custitem_item_family;
	var ItemGroup = columns.custitem_item_group;
	var ItemInfo1 = columns.custitem_item_info_1;
	var ItemInfo2 = columns.custitem_item_info_2;
	var ItemInfo3 = columns.custitem_item_info_3;
	var putVelocity=columns.custitem_ebizabcvelitem;

	nlapiLogExecution('ERROR', 'Item', Item);
	nlapiLogExecution('ERROR', 'ItemFamily', ItemFamily);
	nlapiLogExecution('ERROR', 'ItemGroup', ItemGroup);
	nlapiLogExecution('ERROR', 'ItemStatus', ItemStatus);
	nlapiLogExecution('ERROR', 'ItemInfo1', ItemInfo1);
	nlapiLogExecution('ERROR', 'ItemInfo2', ItemInfo2);
	nlapiLogExecution('ERROR', 'ItemInfo3', ItemInfo3);
	nlapiLogExecution('ERROR', 'putVelocity', putVelocity);
	nlapiLogExecution('ERROR', 'poLocn', poLocn);


	// if (ItemFamily != null || ItemFamily != "") {
	var columns = new Array();
	var i=0;
	var filters = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_sequencenumberpickrule');
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
	columns[2] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
	columns[3] = new nlobjSearchColumn('custrecord_putawaymethod');
	columns[4] = new nlobjSearchColumn('custrecord_ruleidpickrule');
	columns[5] = new nlobjSearchColumn('custrecord_manual_location_generation');
	var ItemFamArr = new Array();
	ItemFamArr[0] = ItemFamily;
	ItemFamArr[1] = '@NONE@';

	if(ItemFamily!=null && ItemFamily != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skufamilypickrule', null, 'anyof', ['@NONE@', ItemFamily]));
	}

	if(ItemGroup!=null && ItemGroup != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skugrouppickrule', null, 'anyof', ['@NONE@', ItemGroup]));
	}

	if(ItemStatus!=null && ItemStatus != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skustatuspickrule', null, 'anyof', ['@NONE@', ItemStatus]));
	}

	if(ItemInfo1!=null && ItemInfo1 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo1pickrule', null, 'anyof',['@NONE@', ItemInfo1]));
	}

	if(ItemInfo2!=null && ItemInfo2 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo2pickrule', null, 'anyof',['@NONE@', ItemInfo2]));
	}

	if(ItemInfo3!=null && ItemInfo3 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo3pickrule', null, 'anyof',['@NONE@', ItemInfo3]));
	}

	if(Item != null && Item != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skupickrule', null, 'anyof', ['@NONE@', Item]));		
	}

	if(putVelocity != null && putVelocity != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_abcvelpickrule', null, 'anyof', ['@NONE@', putVelocity]));
	}

	if(poLocn != null && poLocn != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizsitepickput', null, 'anyof', [poLocn]));
	}

	if(NewPutZoneId != null && NewPutZoneId != "" && NewPutZoneId != 'null')
	{
		NewPutZoneId.push('@NONE@');
		filters.push(new nlobjSearchFilter('custrecord_putawayzonepickrule', null, 'anyof', NewPutZoneId));
	}
	if(NewPutLocGrpId != null && NewPutLocGrpId != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_locationgrouppickrule', null, 'anyof', ['@NONE@', NewPutLocGrpId]));
	}
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var t1 = new Date();
	var rulesearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters, columns);
	return rulesearchresults;
}

function vGetZoneIdByLocGroup(getNewBinLocGrpId,PoLoc)
{
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_zone_seq');
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecordcustrecord_putzoneid');

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_locgroup_no', null, 'anyof', getNewBinLocGrpId));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', ['@NONE@', PoLoc]));	
	var searchresults = nlapiSearchRecord('customrecord_zone_locgroup', null, filters, columns);
	return searchresults;
}



function vGetPutawayLocation(Item, PackCode, ItemStatus, UOM, ItemCube, 
		ItemType,poLocn,vItemLocId,lotbinlocarray,vlot){
	var context = nlapiGetContext();
	nlapiLogExecution('DEBUG','Remaining usage at the start',context.getRemainingUsage());
	var ItemFamily, ItemGroup, Location;
	var location_found = false;
	var LocArry = new Array();
	var OBLocGroup, IBLocGroup;
	var PickSeqNo, PutSeqNo, PutMethod,PutMethodName,PutRuleId, MergeFlag = 'F',Mixsku='F',PutBinLoc;
	nlapiLogExecution('DEBUG', 'ItemCube', ItemCube);//0
	nlapiLogExecution('DEBUG', 'ItemType', ItemType);
	var itemresults;

	var fields = ['custitem_item_family','custitem_item_group','custitem_ebizdefskustatus',
	              'custitem_item_info_1','custitem_item_info_2','custitem_item_info_3','custitem_ebizabcvelitem'];


	var columns= nlapiLookupField('item',Item,fields);
	var ItemFamily = columns.custitem_item_family;
	var ItemGroup = columns.custitem_item_group;
	var ItemInfo1 = columns.custitem_item_info_1;
	var ItemInfo2 = columns.custitem_item_info_2;
	var ItemInfo3 = columns.custitem_item_info_3;
	var putVelocity=columns.custitem_ebizabcvelitem;

	nlapiLogExecution('DEBUG', 'Item', Item);
	nlapiLogExecution('DEBUG', 'ItemFamily', ItemFamily);
	nlapiLogExecution('DEBUG', 'ItemGroup', ItemGroup);
	nlapiLogExecution('DEBUG', 'ItemStatus', ItemStatus);
	nlapiLogExecution('DEBUG', 'ItemInfo1', ItemInfo1);
	nlapiLogExecution('DEBUG', 'ItemInfo2', ItemInfo2);
	nlapiLogExecution('DEBUG', 'ItemInfo3', ItemInfo3);
	nlapiLogExecution('DEBUG', 'putVelocity', putVelocity);
	nlapiLogExecution('DEBUG', 'poLocn', poLocn);
	nlapiLogExecution('DEBUG', 'vItemLocId', vItemLocId);

	var locgroupid = 0;
	var zoneid = 0;

	// if (ItemFamily != null || ItemFamily != "") {
	var columns = new Array();
	var i=0;
	var filters = new Array();
	columns[0] = new nlobjSearchColumn('formulanumeric');
	columns[0].setFormula("TO_NUMBER({custrecord_sequencenumberpickrule})");
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
	columns[2] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
	columns[3] = new nlobjSearchColumn('custrecord_putawaymethod');
	columns[4] = new nlobjSearchColumn('custrecord_ruleidpickrule');
	columns[5] = new nlobjSearchColumn('custrecord_manual_location_generation');
	columns[6] = new nlobjSearchColumn('custrecord_locationpickrule');


	var ItemFamArr = new Array();
	ItemFamArr[0] = ItemFamily;
	ItemFamArr[1] = '@NONE@';
	//Case # 20125990 start
	if(ItemFamily!=null && ItemFamily != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skufamilypickrule', null, 'anyof', ['@NONE@', ItemFamily]));
	}
	else
	{
		//filters.push(new nlobjSearchFilter('custrecord_skufamilypickrule', null, 'anyof', ['@NONE@']));	
	}

	if(ItemGroup!=null && ItemGroup != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skugrouppickrule', null, 'anyof', ['@NONE@', ItemGroup]));
	}
	else
	{
		//filters.push(new nlobjSearchFilter('custrecord_skugrouppickrule', null, 'anyof', ['@NONE@']));	
	}

	if(ItemStatus!=null && ItemStatus != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skustatuspickrule', null, 'anyof', ['@NONE@', ItemStatus]));
	}
	else
	{
		//filters.push(new nlobjSearchFilter('custrecord_skustatuspickrule', null, 'anyof', ['@NONE@']));	
	}

	if(ItemInfo1!=null && ItemInfo1 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo1pickrule', null, 'anyof',['@NONE@', ItemInfo1]));
	}

	if(ItemInfo2!=null && ItemInfo2 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo2pickrule', null, 'anyof',['@NONE@', ItemInfo2]));
	}

	if(ItemInfo3!=null && ItemInfo3 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo3pickrule', null, 'anyof',['@NONE@', ItemInfo3]));
	}

	if(Item != null && Item != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skupickrule', null, 'anyof', ['@NONE@', Item]));		
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_skupickrule', null, 'anyof', ['@NONE@']));	
	}

	if(putVelocity != null && putVelocity != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_abcvelpickrule', null, 'anyof', ['@NONE@', putVelocity]));
	}

	if(poLocn != null && poLocn != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizsitepickput', null, 'anyof', [poLocn]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizsitepickput', null, 'anyof', ['@NONE@']));	
	}
	//Case # 20125990 end
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var t1 = new Date();
	var rulesearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters, columns);
	var t2 = new Date();
	nlapiLogExecution('DEBUG', 'nlapiSearchRecord:GetPutawayLocation:putaway_rule',
			getElapsedTimeDuration(t1, t2));

	if (rulesearchresults != null) {
		nlapiLogExecution('DEBUG', 'rules count', rulesearchresults.length);
		for (var s = 0; s < rulesearchresults.length; s++) {
			locgroupid = rulesearchresults[s].getValue('custrecord_locationgrouppickrule');
			zoneid = rulesearchresults[s].getValue('custrecord_putawayzonepickrule');
			PutMethod = rulesearchresults[s].getValue('custrecord_putawaymethod');
			PutMethodName = rulesearchresults[s].getText('custrecord_putawaymethod');
			PutRuleId = rulesearchresults[s].getValue('custrecord_ruleidpickrule');
			PutBinLoc = rulesearchresults[s].getValue('custrecord_locationpickrule');
			nlapiLogExecution('DEBUG', 'Put Rule', PutRuleId);
			nlapiLogExecution('DEBUG', 'Put Method', PutMethod);
			nlapiLogExecution('DEBUG', 'PutBinLoc', PutBinLoc);
			nlapiLogExecution('DEBUG', 'Location Group Id from Putaway Rule', locgroupid);

			// To indicate if the location has to be generated manually based on the parameter in putaway strategy
			ManualLocationGeneration = rulesearchresults[s].getValue('custrecord_manual_location_generation');
			nlapiLogExecution('DEBUG', 'Manual Location Generation', ManualLocationGeneration);

			if (ManualLocationGeneration == 'F') {
				//if (locgroupid != 0 && locgroupid != null) {
				if ((locgroupid != 0 && locgroupid != null) || (PutBinLoc != "" && PutBinLoc != null)){
					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_startingputseqno');
					columns[0].setSort();
					columns[1] = new nlobjSearchColumn('name');
					columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid');
					columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno');
					columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid');
					columns[5] = new nlobjSearchColumn('custrecord_remainingcube');	

					nlapiLogExecution('DEBUG', 'locgroupid', locgroupid);

					var filters = new Array();
					filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [locgroupid]));	
					filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
					if(poLocn != null && poLocn != "")
						filters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof', [poLocn]));
					if((vItemLocId!=null && vItemLocId!="" && vItemLocId !='null')&& (PutBinLoc == null || PutBinLoc == ""))
						filters.push(new nlobjSearchFilter('internalid', null, 'noneof', vItemLocId));
					if(PutBinLoc != null && PutBinLoc != "")
						filters.push(new nlobjSearchFilter('internalid', null, 'is', PutBinLoc));
					//case# 201413037
					filters.push(new nlobjSearchFilter('custrecord_remainingcube', null, 'greaterthanorequalto', ItemCube));
					t1 = new Date();
					var locResults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);
					t2 = new Date();
					nlapiLogExecution('DEBUG', 'nlapiSearchRecord:GetPutawayLocation:location',
							getElapsedTimeDuration(t1, t2));

					nlapiLogExecution('DEBUG', 'locResults', locResults);
					if (locResults != null) {
						nlapiLogExecution('DEBUG', 'ifnot ');
						for (var u = 0; u < locResults.length; u++) {

							Location = locResults[u].getValue('name');
							var locationIntId = locResults[u].getId();

							nlapiLogExecution('DEBUG', 'Location', Location);
							nlapiLogExecution('DEBUG', 'Location Id', locationIntId);

							//Commented by satish.N on 04-JAN-2012 as the search results will give the same value.
							//LocRemCube = GeteLocCube(locationIntId);
							LocRemCube = locResults[u].getValue('custrecord_remainingcube');							
							OBLocGroup = locResults[u].getValue('custrecord_outboundinvlocgroupid');
							PickSeqNo  = locResults[u].getValue('custrecord_startingpickseqno');
							IBLocGroup = locResults[u].getValue('custrecord_inboundinvlocgroupid');
							PutSeqNo   = locResults[u].getValue('custrecord_startingputseqno');

							var IsExistsPFloc='F';
							var isExistsPFlocresults = getPickFaceLocation(locationIntId);

							if(isExistsPFlocresults!=null && isExistsPFlocresults!='')
							{
								for(var pf = 0; pf < isExistsPFlocresults.length; pf++)
								{
									var pickfaceitem=isExistsPFlocresults[pf].getValue('custrecord_pickfacesku');
									nlapiLogExecution('DEBUG', 'pickfaceitem', pickfaceitem);
									nlapiLogExecution('DEBUG', 'Item', Item);
									if(pickfaceitem != Item)
									{
										IsExistsPFloc='T';
									}
									else //  case# 201413037
									{
										IsExistsPFloc='F';
										break;
									}
								}
							}
							nlapiLogExecution('DEBUG', 'isExistsPFloc', IsExistsPFloc);
							nlapiLogExecution('DEBUG', 'LocRemCube', LocRemCube);
							nlapiLogExecution('DEBUG', 'ItemCube', ItemCube);
							var isDiffLotExist='F';

							if(lotbinlocarray != null && lotbinlocarray != '')
							{
								isDiffLotExist = CheckDiffLotExist(lotbinlocarray,vlot,locationIntId);
							}


							if(IsExistsPFloc=='F' && isDiffLotExist!='T')
							{
								if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {
									RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);
									location_found = true;

									LocArry[0] = Location;
									LocArry[1] = RemCube;
									LocArry[2] = locationIntId;	
									LocArry[3] = OBLocGroup;
									LocArry[4] = PickSeqNo;
									LocArry[5] = IBLocGroup;
									LocArry[6] = PutSeqNo;
									LocArry[7] = LocRemCube;
									LocArry[8] = zoneid;
									LocArry[9] = PutMethodName;
									LocArry[10] = PutRuleId;
									LocArry[11] = PutMethod;
									nlapiLogExecution('DEBUG', 'SA-Location', Location);
									nlapiLogExecution('DEBUG', 'Location Internal Id',locationIntId);
									nlapiLogExecution('DEBUG', 'Sa-Location Cube', RemCube);
									return LocArry;
								}
							}
						}
					}
				}
				else {
					nlapiLogExecution('DEBUG', 'Fetching Location Group from Zone', zoneid);
					var mixarray =  new Array();
					if(PutMethod!=null && PutMethod!='')
						mixarray=GetMergeFlag(PutMethod);
					if(mixarray!=null && mixarray!='' && mixarray.length>0)
					{
						MergeFlag = mixarray[0][0];
						Mixsku = mixarray[0][1];
					}
					nlapiLogExecution('DEBUG', 'Merge Flag', MergeFlag);
					nlapiLogExecution('DEBUG', 'Mix SKU', Mixsku);

					if (zoneid != null && zoneid != ""){ 
						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_zone_seq');
						columns[0].setSort();
						columns[1] = new nlobjSearchColumn('custrecord_locgroup_no');

						var filters = new Array();
						filters.push(new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'is', zoneid));
						filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
						filters.push(new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', ['@NONE@', poLocn]));
						t1 = new Date();
						var searchresults = nlapiSearchRecord('customrecord_zone_locgroup', null, filters, columns);
						t2 = new Date();
						nlapiLogExecution('DEBUG', 'nlapiSearchRecord:GetPutawayLocation:zone_locgroup',
								getElapsedTimeDuration(t1, t2));

						var RemCube = 0;
						var LocRemCube = 0;

						if (searchresults != null && searchresults != '') 
						{

							for (var i = 0; i < Math.min(30, searchresults.length); i++) {
								locgroupid = searchresults[i].getValue('custrecord_locgroup_no');
								nlapiLogExecution('DEBUG', 'Fetched Location Group Id from Putaway Zone', locgroupid);

								if (MergeFlag == 'T') {
									nlapiLogExecution('DEBUG', 'Inside Merge ');
									var locResults;
									nlapiLogExecution('DEBUG', 'ItemCube', ItemCube);
									nlapiLogExecution('DEBUG', 'poLocn', poLocn);
									nlapiLogExecution('DEBUG', 'locgroupid', locgroupid);

									try {
										var columns = new Array();
										columns[0] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');
										columns[0].setSort();
										columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
										columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc');
										columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno', 'custrecord_ebiz_inv_binloc');
										columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc');
										columns[5] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');
										columns[6] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
										columns[7] = new nlobjSearchColumn('custrecord_remainingcube','custrecord_ebiz_inv_binloc');

										var filters = new Array();
										if(Mixsku!='T')
										{
											filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', Item));
										}

										if(poLocn!=null && poLocn!='')
											filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', poLocn));
										filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'is', locgroupid));
										filters.push(new nlobjSearchFilter('custrecord_remainingcube', 'custrecord_ebiz_inv_binloc', 'greaterthanorequalto', ItemCube));                                        
										if(vItemLocId!=null&&vItemLocId!="")
											filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'noneof', vItemLocId));

										filters.push(new nlobjSearchFilter('isinactive', 'custrecord_ebiz_inv_binloc', 'is', 'F'));

										t1 = new Date();
										locResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
										t2 = new Date();
										nlapiLogExecution('DEBUG', 'nlapiSearchRecord:GetPutawayLocation:createinv',
												getElapsedTimeDuration(t1, t2));
									} //try close.
									catch (Error) {
										nlapiLogExecution('DEBUG', 'Into Error', Error);
									}
									try {
										nlapiLogExecution('DEBUG', 'Inside Try', 'TRY');

										if (locResults != null && locResults != '') {
											nlapiLogExecution('DEBUG', 'LocationResults', locResults.length );
											for (var j = 0; j < locResults.length; j++) {
												nlapiLogExecution('DEBUG', 'locResults[j].getId()', locResults[j].getId());
												Location = locResults[j].getText('custrecord_ebiz_inv_binloc');
												var locationInternalId = locResults[j].getValue('custrecord_ebiz_inv_binloc');

												nlapiLogExecution('DEBUG', 'Location', Location );
												nlapiLogExecution('DEBUG', 'Location Id', locationInternalId);

												var IsExistsPFloc='F';
												var isExistsPFlocresults = getPickFaceLocation(locationInternalId);
												if(isExistsPFlocresults!=null && isExistsPFlocresults!='')
												{
													for(var pf = 0; pf < isExistsPFlocresults.length; pf++)
													{
														var pickfaceitem=isExistsPFlocresults[pf].getValue('custrecord_pickfacesku');
														if(pickfaceitem != Item)
														{
															IsExistsPFloc='T';
														}
														else
														{
															IsExistsPFloc='F';
															break;
														}
													}
												}
												nlapiLogExecution('DEBUG', 'IsExistsPFloc', IsExistsPFloc);

												//var isDiffLotExist = CheckDiffLotExist(lotbinlocarray,vlot,locationInternalId);
												var isDiffLotExist='F';

												if(lotbinlocarray != null && lotbinlocarray != '')
												{
													isDiffLotExist = CheckDiffLotExist(lotbinlocarray,vlot,locationIntId);
												}

												//LocRemCube = GeteLocCube(locationInternalId);
												if(IsExistsPFloc=='F' && isDiffLotExist!='T'){
													LocRemCube = locResults[j].getValue('custrecord_remainingcube', 'custrecord_ebiz_inv_binloc');
													OBLocGroup = locResults[j].getValue('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc');
													PickSeqNo = locResults[j].getValue('custrecord_startingpickseqno', 'custrecord_ebiz_inv_binloc');
													IBLocGroup = locResults[j].getValue('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc');
													PutSeqNo = locResults[j].getValue('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');

													nlapiLogExecution('DEBUG', 'Loc Rem Cube', LocRemCube);
													nlapiLogExecution('DEBUG', 'Item Cube', ItemCube);

													if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {

														RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);

														location_found = true;

														LocArry[0] = Location;
														LocArry[1] = RemCube;
														LocArry[2] = locationInternalId;		//locResults[j].getId();
														LocArry[3] = OBLocGroup;
														LocArry[4] = PickSeqNo;
														LocArry[5] = IBLocGroup;
														LocArry[6] = PutSeqNo;
														LocArry[7] = LocRemCube;
														LocArry[8] = zoneid;
														LocArry[9] = PutMethodName;
														LocArry[10] = PutRuleId;
														LocArry[11] = PutMethod;
														nlapiLogExecution('DEBUG', 'after changes Location Cube', RemCube);
														return LocArry;
													}
												}
											}
										}
									} 
									catch (exps) {
										nlapiLogExecution('DEBUG', 'included exps', exps);
									}

									//Added by satish.N on 04-JAN-2012 to consider open taks while merging.
									if (location_found == false) {

										try{
											nlapiLogExecution('DEBUG', 'Inside  consider open taks while merging');

											var columns = new Array();
											columns[0] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_actbeginloc');
											columns[0].setSort();
											columns[1] = new nlobjSearchColumn('custrecord_actbeginloc');
											columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid', 'custrecord_actbeginloc');
											columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno', 'custrecord_actbeginloc');
											columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid', 'custrecord_actbeginloc');
											columns[5] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_actbeginloc');
											columns[6] = new nlobjSearchColumn('custrecord_sku');
											columns[7] = new nlobjSearchColumn('custrecord_remainingcube','custrecord_actbeginloc');

											var filters = new Array();
											if(Mixsku!='T')
											{
												filters.push(new nlobjSearchFilter('custrecord_sku', null, 'is', Item));
											}	
											if(poLocn!=null && poLocn!='')
												filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', [poLocn]));
											filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
											filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_actbeginloc', 'is', locgroupid));
											filters.push(new nlobjSearchFilter('custrecord_remainingcube', 'custrecord_actbeginloc', 'greaterthanorequalto', ItemCube));
											if(vItemLocId!=null&&vItemLocId!="")
												filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'noneof', vItemLocId));
											locResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

											if (locResults != null && locResults != '') {
												for (var j = 0; j < locResults.length; j++) {

													Location = locResults[j].getText('custrecord_actbeginloc');
													var locationInternalId = locResults[j].getValue('custrecord_actbeginloc');

													nlapiLogExecution('DEBUG', 'Location', Location );
													nlapiLogExecution('DEBUG', 'Location Id', locationInternalId);

													var IsExistsPFloc='F';
													var isExistsPFlocresults = getPickFaceLocation(locationInternalId);
													if(isExistsPFlocresults!=null && isExistsPFlocresults!='')
													{
														for(var pf = 0; pf < isExistsPFlocresults.length; pf++)
														{
															var pickfaceitem=isExistsPFlocresults[pf].getValue('custrecord_pickfacesku');
															if(pickfaceitem != Item)
															{
																IsExistsPFloc='T';
															}
															else
															{
																IsExistsPFloc='F';
																break;
															}
														}
													}
													nlapiLogExecution('DEBUG', 'isExistsPFloc', IsExistsPFloc);

													//var isDiffLotExist = CheckDiffLotExist(lotbinlocarray,vlot,locationInternalId);
													var isDiffLotExist='F';

													if(lotbinlocarray != null && lotbinlocarray != '')
													{
														isDiffLotExist = CheckDiffLotExist(lotbinlocarray,vlot,locationIntId);
													}

													if(IsExistsPFloc=='F' && isDiffLotExist!='T'){
														LocRemCube = locResults[j].getValue('custrecord_remainingcube', 'custrecord_actbeginloc');
														OBLocGroup = locResults[j].getValue('custrecord_outboundlocgroupid', 'custrecord_actbeginloc');
														PickSeqNo = locResults[j].getValue('custrecord_startingpickseqno', 'custrecord_actbeginloc');
														IBLocGroup = locResults[j].getValue('custrecord_inboundlocgroupid', 'custrecord_actbeginloc');
														PutSeqNo = locResults[j].getValue('custrecord_startingputseqno', 'custrecord_actbeginloc');

														nlapiLogExecution('DEBUG', 'Loc Rem Cube', LocRemCube);
														nlapiLogExecution('DEBUG', 'Item Cube', ItemCube);

														if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {

															RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);

															location_found = true;

															LocArry[0] = Location;
															LocArry[1] = RemCube;
															LocArry[2] = locationInternalId;		//locResults[j].getId();
															LocArry[3] = OBLocGroup;
															LocArry[4] = PickSeqNo;
															LocArry[5] = IBLocGroup;
															LocArry[6] = PutSeqNo;
															LocArry[7] = LocRemCube;
															LocArry[8] = zoneid;
															LocArry[9] = PutMethodName;
															LocArry[10] = PutRuleId;
															LocArry[11] = PutMethod;
															nlapiLogExecution('DEBUG', 'after changes Location Cube', RemCube);
															return LocArry;
														}
													}		
												}
											}
										}
										catch(exps){
											nlapiLogExecution('DEBUG', 'included exps in opentasks', exps);
										}
									}
								}
								if (location_found == false) {

									//code modified by suman on 27/01/12
									//Instead of searching each and individual binloc in inventory and open task record it seems to have a goverence issue.
									//So I bought all binloc from open task and inventory records depending upon "locgr"Id and placing it in an array so that on comparing this array with the binlocation 
									//will decides weather it is empty or not.

									nlapiLogExecution('DEBUG', 'Location Not found for Merge Location for Location Group', locgroupid);
									var locResults = GetLocation(locgroupid,ItemCube);

									if (locResults != null && locResults != '') {

										var TotalListOfBinLoc=new Array();
										for ( var x = 0; x < locResults.length; x++) 
										{
											TotalListOfBinLoc[x]=locResults[x].getId();

										}
										var emptyloccheck =	binLocationChecknew(TotalListOfBinLoc);
										nlapiLogExecution('DEBUG', 'emptyloccheck', emptyloccheck);

										nlapiLogExecution('DEBUG', 'locResults.length', locResults.length);
										for (var j = 0; j < Math.min(300, locResults.length); j++) {
											var LocFlag='N';
											var emptyBinLocationId = locResults[j].getId();
											nlapiLogExecution('DEBUG', 'emptyBinLocationId', emptyBinLocationId);
//											var emptyloccheck =	binLocationCheck(emptyBinLocationId);
//											nlapiLogExecution('DEBUG', 'emptyloccheck', emptyloccheck);

											for ( var InvBinLocCount = 0; InvBinLocCount < emptyloccheck[0].length; InvBinLocCount++)
											{
//												nlapiLogExecution('DEBUG', 'emptyloccheck[0][InvBinLocCount]', emptyloccheck[0][InvBinLocCount]);

												if(emptyloccheck[0][InvBinLocCount]==emptyBinLocationId)
												{
													nlapiLogExecution('DEBUG', 'emptyBinLocationId+InvBinLocCount', emptyBinLocationId+','+emptyloccheck[0][InvBinLocCount]);
													LocFlag='Y';
													break;
												}
											}
											for ( var OpenTaskBinLocCount = 0; OpenTaskBinLocCount < emptyloccheck[1].length; OpenTaskBinLocCount++)
											{
												if(emptyloccheck[1][OpenTaskBinLocCount]==emptyBinLocationId)
												{
													nlapiLogExecution('DEBUG', 'emptyBinLocationId+OpenTaskBinLocCount', emptyBinLocationId+','+emptyloccheck[1][OpenTaskBinLocCount]);
													LocFlag='Y';
													break;
												}
											}
//											end of code changed 27/01/12 
											nlapiLogExecution('DEBUG', 'LocFlag', LocFlag);
											nlapiLogExecution('DEBUG', 'Main Loop ', s);
											nlapiLogExecution('DEBUG','Remaining usage ',context.getRemainingUsage());
											if(LocFlag == 'N'){
												//Changes done by Sarita (index i is changed to j).
												LocRemCube = locResults[j].getValue('custrecord_remainingcube');
												OBLocGroup = locResults[j].getValue('custrecord_outboundlocgroupid');
												PickSeqNo = locResults[j].getValue('custrecord_startingpickseqno');
												IBLocGroup = locResults[j].getValue('custrecord_inboundlocgroupid');
												PutSeqNo = locResults[j].getValue('custrecord_startingputseqno');

												nlapiLogExecution('DEBUG', 'LocRemCube', LocRemCube);
												nlapiLogExecution('DEBUG', 'ItemCube', ItemCube);

												var IsExistsPFloc='F';
												var isExistsPFlocresults = getPickFaceLocation(locResults[j].getId());

												if(isExistsPFlocresults!=null && isExistsPFlocresults!='')
												{
													for(var pf = 0; pf < isExistsPFlocresults.length; pf++)
													{
														var pickfaceitem=isExistsPFlocresults[pf].getValue('custrecord_pickfacesku');
														if(pickfaceitem != Item)
														{
															IsExistsPFloc='T';
														}
														else
														{
															IsExistsPFloc='F';
															break;
														}
													}
												}
												nlapiLogExecution('DEBUG', 'isExistsPFloc', IsExistsPFloc);

												//var isDiffLotExist = CheckDiffLotExist(lotbinlocarray,vlot,locResults[j].getId());
												var isDiffLotExist='F';

												if(lotbinlocarray != null && lotbinlocarray != '')
												{
													isDiffLotExist = CheckDiffLotExist(lotbinlocarray,vlot,locationIntId);
												}

												if(IsExistsPFloc=='F' && isDiffLotExist!='T')
												{
													if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {
														RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);
														location_found = true;
														Location = locResults[j].getValue('name');
														LocArry[0] = Location;
														LocArry[1] = RemCube;
														LocArry[2] = locResults[j].getId();
														LocArry[3] = OBLocGroup;
														LocArry[4] = PickSeqNo;
														LocArry[5] = IBLocGroup;
														LocArry[6] = PutSeqNo;
														LocArry[7] = LocRemCube;
														LocArry[8] = zoneid;
														LocArry[9] = PutMethodName;
														LocArry[10] = PutRuleId;
														LocArry[11] = PutMethod;
														nlapiLogExecution('DEBUG', 'Location', Location);
														nlapiLogExecution('DEBUG', 'Location Cube', RemCube);
														return LocArry;
														break;
													}
												}
											}
										}
									}
								}
							}
						}//end of searchresult null checking
					}
				}
				nlapiLogExecution('DEBUG', 'Main Loop ', s);
				nlapiLogExecution('DEBUG','Remaining usage ',context.getRemainingUsage());
			}
		}
	}// end of put rule for loop
	else
	{
		nlapiLogExecution('DEBUG', 'No Searchresults with above mentioned criteria ', 'Null Results');
	}
	//  } // end of main if
	return LocArry;
}
function getPickFaceLocation(location)
{
	nlapiLogExecution('DEBUG', 'Into getPickFaceLocation',location);
	var Result='';
	var PickFaceSearchResults = new Array();
	var PickFaceFilters = new Array();
	var PickFaceColumns = new Array();
	if(location!=null && location!='' && location!='null')//sri Case# 20148123
		PickFaceFilters.push(new nlobjSearchFilter('custrecord_pickbinloc', null, 'anyof', location)); 
	PickFaceFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	PickFaceColumns[0] = new nlobjSearchColumn('custrecord_pickbinloc');
	PickFaceColumns[1] = new nlobjSearchColumn('custrecord_pickfacesku');

	PickFaceSearchResults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, PickFaceFilters, PickFaceColumns);

	nlapiLogExecution('DEBUG', 'Return Value',PickFaceSearchResults);
	nlapiLogExecution('DEBUG', 'Out of getPickFaceLocation');

	return PickFaceSearchResults;
}

function CheckDiffLotExist(lotbinlocarray,vlot,locationIntId)
{
	nlapiLogExecution('DEBUG', 'Into  CheckDiffLotExist', vlot);
	var isDiffLotExist='F';

	if(vlot!=null && vlot!='')
	{
		if(lotbinlocarray!=null && lotbinlocarray!='')
		{
			for (var e = 0; e < lotbinlocarray.length; e++) 
			{ 
				if(lotbinlocarray[e][0]==locationIntId)
				{
					if(lotbinlocarray[e][1]!=vlot)
					{
						isDiffLotExist='T';
					}
				}
			}
		}
	}

	nlapiLogExecution('DEBUG', 'Out of  CheckDiffLotExist', isDiffLotExist);
	return isDiffLotExist;
}