/***************************************************************************
�������������������������������eBizNET
�����������������������eBizNET Solutions Inc
****************************************************************************
*
*� $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/Attic/ebiz_WOReportPDF_SL.js,v $
*� $Revision: 1.1.2.1.10.1 $
*� $Date: 2015/04/10 21:36:25 $
*� $Author: skreddy $
*� $Name: t_eBN_2015_1_StdBundle_1_39 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_WOReportPDF_SL.js,v $
*� Revision 1.1.2.1.10.1  2015/04/10 21:36:25  skreddy
*� Case# 201412323�
*� changed the url path which was hard coded
*�
*� Revision 1.1.2.1  2012/10/26 08:02:57  gkalla
*� CASE201112/CR201113/LOG201121
*� Lot# exception enhancement in work orders
*�
*� Revision 1.1.2.1  2012/07/05 14:22:37  spendyala
*� CASE201112/CR201113/LOG201121
*� New Script for generating PDF for Work order.
*�
*
****************************************************************************/

/**
 * This is the main function to print the BOM LIST against the given WO number  
 */
function WOReportPDFSuitelet(request, response)
{
	if (request.getMethod() == 'GET') 
	{

		var form = nlapiCreateForm('WO Report'); 
		var woid=request.getParameter('custparam_wo_no');

		nlapiLogExecution('ERROR','woid',woid);

		if(woid!=null && woid!='')
		{
			nlapiLogExecution('ERROR','INTOwoid1',woid);

			var wonumber;
			//santosh
			var WOFilers = new Array();
			WOFilers.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', woid));
			WOFilers.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', 5));//kts
			WOFilers.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [3]));//STATUS.INBOUND.PUTAWAY_COMPLETE�


			var columns=new Array();
			columns[0]=new nlobjSearchColumn('custrecord_sku');
			//columns[0].setSort(true);
			columns[1]=new nlobjSearchColumn('custrecord_actbeginloc');//bin loc
			columns[2]=new nlobjSearchColumn('custrecord_actendloc');//bin loc
			columns[3]=new nlobjSearchColumn('custrecord_lpno');
			columns[4]=new nlobjSearchColumn('custrecord_act_qty');
			columns[5]=new nlobjSearchColumn('custrecord_ebiz_order_no');


			nlapiLogExecution('ERROR','serachresult',woid);


			var url;
			var ctx = nlapiGetContext();
			nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
			/*if (ctx.getEnvironment() == 'PRODUCTION') 
			{
				url = 'https://system.netsuite.com';			
			}
			else if (ctx.getEnvironment() == 'SANDBOX') 
			{
				url = 'https://system.sandbox.netsuite.com';				
			}
			nlapiLogExecution('ERROR', 'PDF URL',url);	*/
			var filefound = nlapiLoadFile('Images/LOGOCOMP.jpg'); 
			if (filefound) 
			{ 
				nlapiLogExecution('ERROR', 'Event', 'file;'+filefound.getId()); 
				var imageurl = filefound.getURL();
				nlapiLogExecution('ERROR','imageurl',imageurl);
				//var finalimageurl = url + imageurl;//+';';
				var finalimageurl = imageurl;
				//finalimageurl=finalimageurl+ '&expurl=T;';
				nlapiLogExecution('ERROR','imageurl',finalimageurl);
				finalimageurl=finalimageurl.replace(/&/g,"&amp;");

			} 
			else 
			{
				nlapiLogExecution('ERROR', 'Event', 'No file;');
			}

			//date
			var sysdate=DateStamp();
			var systime=TimeStamp();
			var Timez=calcTime('-5.00');
			nlapiLogExecution('ERROR','TimeZone',Timez);
			var datetime= new Date();
			datetime=datetime.toLocaleTimeString() ;
			var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, WOFilers,columns);
			nlapiLogExecution('ERROR', 'result;',searchresults.length);
			var wonumber=searchresults[0].getText('custrecord_ebiz_order_no');
			nlapiLogExecution('ERROR', 'resultwoidnumber;',wonumber);

			var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<head><macrolist><macro id='myfooter'><p align='center'>Page <pagenumber/> of <totalpages/></p></macro></macrolist></head><body  font-size=\"7\"  size=\"A4-landscape\"    padding-top=\" 0mm\"   footer='myfooter' footer-height='20mm'>\n";
			//var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<head></head><body  font-size=\"7\"  size=\"A4-landscape\"    padding-top=\" 0mm\"   footer='myfooter' footer-height='20mm'>\n";
//			nlapiLogExecution('ERROR','xml0 ',"<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body font-size=\"7\">\n");

			var replaceChar =/\$|,|@|#|~|`|\%|\*|\^|\&|\+|\=|\-|\_|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|\./g;
			var strxml= "";
			strxml += "<table width='100%'>";
			strxml += "<tr><td><table width='100%' >";
			strxml += "<tr ><td valign='middle' align='left'><img src='" + finalimageurl + "'></img></td><td valign='middle' align='left'  style='font-size:xx-large;'>";
			strxml += "Work Order Report ";
			strxml += "</td><td align='right'>&nbsp;</td></tr></table></td></tr>";
			strxml += "<tr><td><table width='100%' >";
			strxml += "<tr><td style='width:41px;font-size: 12px;'>";
			strxml += "<p align='right'>Date/Time:"+Timez+"</p>"; 
			strxml += "</td></tr>";
			strxml += "</table>";

			strxml += "</td></tr>";
			strxml += "<tr><td><table width='100%' >";
			strxml += "<tr><td>";
			strxml +="<table align='left'>";

			strxml +="<tr><td align='left' style='width:20px; font-size: 16px;'>WO# :</td>"; 
			strxml +="<td align='right' style='width:41px; font-size: 14px;'>";
			strxml += wonumber;
			strxml +="</td>";
			strxml +="</tr>";

			strxml +="</table>";
			strxml += "</td></tr>";
			strxml += "</table>";

			strxml += "</td></tr>";

			strxml += "<tr><td><table width='100%' >";
			strxml += "<tr><td>";

			strxml +="<table  width='100%'>";
			strxml +="<tr style=\"font-weight:bold;background-color:gray;color:white;\">";

			strxml += "<td width='7%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += "Item#";
			strxml += "</td>";

			strxml += "<td width='16%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px; '>";
			strxml += " Bin Location";
			strxml += "</td>";

			strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += "Lp#";
			strxml += "</td>";

			strxml += "<td width='15%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += "Quantity";
			strxml += "</td>";

//			strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
//			strxml += "&nbsp;";
			strxml =strxml+  "</tr>";

			//var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, WOFilers,columns);
			//nlapiLogExecution('ERROR', 'result;',searchresults.length);

			for(var m=0; m< searchresults.length; m++)
			{				

				var lineItem=searchresults[m].getText('custrecord_sku');
				if(lineItem== null || lineItem =="")
					lineItem="";

				var LineBinLoc =searchresults[m].getText('custrecord_actendloc');
				if(LineBinLoc== null || LineBinLoc =="")	
					LineBinLoc="&nbsp;";

				var LineLp=searchresults[m].getValue('custrecord_lpno');
				if(LineLp== null || LineLp =="")
					LineLp="&nbsp;";

				var LineQuantity =searchresults[m].getValue('custrecord_act_qty');
				if(LineQuantity== null || LineQuantity =="")
					LineQuantity="&nbsp;";



				strxml =strxml+  "<tr>";
				strxml += "<td width='7%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += lineItem;
				strxml += "</td>";

				strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += LineBinLoc;
				strxml += "</td>";

				strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += LineLp;
				strxml += "</td>";

				strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += parseFloat(LineQuantity);
				strxml += "</td></tr>";

			}

			strxml =strxml+"</table>";

			strxml += "</td></tr>";
			strxml += "</table>";

			strxml += "</td></tr>";

			strxml += "</table>"; 

			strxml =strxml+ "\n</body>\n</pdf>";		

			xml=xml +strxml;

			//nlapiLogExecution('ERROR','xml',xml);

			var file = nlapiXMLToPDF(xml);	
			response.setContentType('PDF','WOReport.pdf');
			response.write( file.getValue() );


		}
	}
	else 
	{

	}
}



function SetPrintFlag(waveno,fullfillment)
{
	nlapiLogExecution('ERROR','GETwaveno',waveno);
	nlapiLogExecution('ERROR','GETfullfillment',fullfillment);
	var filter=new Array();

	if(waveno!=null && waveno!="")
		filter.push(new nlobjSearchFilter('custrecord_ebiz_wave','null','equalto',parseFloat(waveno)));

	if(fullfillment!=null&&fullfillment!="")
		filter.push(new nlobjSearchFilter('custrecord_lineord','null','is',fullfillment.toString()));

	var column=new Array();
	column[0]=new nlobjSearchColumn('custrecord_printflag');
	column[1]=new nlobjSearchColumn('custrecord_print_count');

	var searchrecord=nlapiSearchRecord('customrecord_ebiznet_ordline',null,filter,column);
	if(searchrecord!=null)
	{
		for ( var count = 0; count < searchrecord.length; count++) {
			var intId=searchrecord[count].getId();
			var flagcount=searchrecord[count].getValue('custrecord_print_count');
			var Newflagcount;
			if(flagcount==null || flagcount=="")
				Newflagcount=1;
			else
				Newflagcount=parseFloat(flagcount)+1;

			nlapiSubmitField('customrecord_ebiznet_ordline', intId, 'custrecord_printflag','T');
			nlapiSubmitField('customrecord_ebiznet_ordline', intId, 'custrecord_print_count',Newflagcount);
			nlapiSubmitField('customrecord_ebiznet_ordline', intId, 'custrecord_ebiz_pr_dateprinted',DateStamp());
		}
	}

}
function calcTime(offset) {

	//var vtime=new Date();

	//nlapiLogExecution('ERROR', 'TimeOffset', vtime.getTimezoneOffset());

	// create Date object for current location
	d = new Date();

	// convert to msec
	// add local time zone offset 
	// get UTC time in msec
	utc = d.getTime() + (d.getTimezoneOffset() * 60000);

	// create new Date object for different city
	// using supplied offset
	nd = new Date(utc + (3600000*offset));

	// return time as a string
	//return "The local time in " + city + " is " + nd.toLocaleString();

	var stringDt=((parseFloat(nd.getMonth()) + 1) + '/' + (parseFloat(nd.getDate())) + '/' + nd.getFullYear());

	var timestamp;
	var a_p = "";

	//Getting time in hh:mm tt format.
	var curr_hour = nd.getHours();
	var curr_min = nd.getMinutes();

	// determining the am/pm indicator
	if (curr_hour < 12)
		a_p = "am";
	else
		a_p = "pm";

	// finalizing hours depending on 24hr clock
	if (curr_hour == 0)
		curr_hour = 12;
	else if(curr_hour > 12)
		curr_hour -= 12;

	if (curr_min.length == 1)
		curr_min = "0" + curr_min;

	//Adding fields to update time zones.
	timestamp = curr_hour + ":" + curr_min + " " + a_p;

	return stringDt + " " + timestamp;


}

