
/***************************************************************************
 eBizNET SOLUTIONS LTD
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_UPCPlanNo.js,v $
 *     	   $Revision: 1.1.2.3 $
 *     	   $Date: 2014/06/13 09:01:35 $
 *     	   $Author: skavuri $
 *     	   $Name: t_NSWMS_2014_1_3_125 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 * 
* REVISION HISTORY
 * $Log: ebiz_RF_UPCPlanNo.js,v $
 * Revision 1.1.2.3  2014/06/13 09:01:35  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.2  2014/05/30 00:34:23  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.1  2014/01/07 15:47:34  rmukkera
 * Case # 20126443
 *
 * Revision 1.3.4.2  2012/03/16 14:08:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.3.4.1  2012/02/21 13:23:31  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.4  2012/02/21 11:44:22  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 */
function UPCPlanNo(request, response){
	if (request.getMethod() == 'GET') {

		var functionkeyHtml=getFunctionkeyScript('_rf_upcplan_no'); 
		var html = "<html><head><title>UPC PLAN No</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";   
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterplanno').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cyclecountplan_no' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>ENTER PLAN #:";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterplanno' id='enterplanno' ype='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "				<td align = 'left'>CONT <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterplanno').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {

		var getupcPlanNo = request.getParameter('enterplanno');
		nlapiLogExecution('ERROR', 'getupcPlanNo', getupcPlanNo);

		var filters = new Array();
	
		filters[0] = new nlobjSearchFilter('custrecord_ebiz_report_no', null, 'is', getupcPlanNo);
		filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [22]);	
		filters[2] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');
		
		
		var SOColumns = new Array();				
		SOColumns[0] = new nlobjSearchColumn('custrecord_sku');
		SOColumns[1] = new nlobjSearchColumn('custrecord_tasktype');
		SOColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
		SOColumns[3] = new nlobjSearchColumn('custrecord_wms_location');
		SOColumns[4] = new nlobjSearchColumn('custrecord_ebiz_report_no');	
		SOColumns[5] = new nlobjSearchColumn('custrecord_ebiz_sku_no');	
		SOColumns[6] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
		SOColumns[6].setSort(false);
		nlapiLogExecution('ERROR', 'Before Search Results Id', 'getSearchResultsId');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, SOColumns);
		nlapiLogExecution('ERROR', 'After Search Results Id', 'getSearchResultsId');
		var UPCarray = new Array();

		UPCarray["custparam_error"] = 'INVALID UPC PLAN #';
		UPCarray["custparam_screenno"] = 'UPC1';
		var optedEvent = request.getParameter('cmdPrevious');

		if (optedEvent == 'F7') {
			response.sendRedirect('SUITELET', 'customscript_rf_inventorymenu', 'customdeploy_rf_inventorymenu_di', false, UPCarray);
		}
		else {
			try {
				if (getupcPlanNo != '') {
					if (searchresults != null && searchresults.length > 0) {
						var getSearchResultsId = searchresults[0].getId();
						nlapiLogExecution('ERROR', 'Search Results Id', getSearchResultsId);

						UPCarray["custparam_planno"] = getupcPlanNo;
						UPCarray["custparam_sku"] = searchresults[0].getValue('custrecord_sku');
						UPCarray["custparam_skuno"] = searchresults[0].getValue('custrecord_ebiz_sku_no');
						UPCarray["custparam_tasktype"] = searchresults[0].getValue('custrecord_tasktype');
						UPCarray["custparam_location"] = searchresults[0].getValue('custrecord_wms_location');
						UPCarray["custparam_actbeginlocationid"] = searchresults[0].getValue('custrecord_actbeginloc');
						UPCarray["custparam_actbeginlocationname"] = searchresults[0].getText('custrecord_actbeginloc');
					
						response.sendRedirect('SUITELET', 'customscript_rf_upccode_location', 'customdeploy_rf_upccode_location_di', false, UPCarray);
					}
					else {
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, UPCarray);
						nlapiLogExecution('ERROR', 'Cycle Plan No. not found', getupcPlanNo);
					}
				}
				else {
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, UPCarray);
					nlapiLogExecution('ERROR', 'Cycle Plan No. not entered', getupcPlanNo);
				}
			} 
			catch (e) {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, UPCarray);
				nlapiLogExecution('ERROR', 'Catching the Search Results', 'Length is null');
			}
			nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
		}
	}
}