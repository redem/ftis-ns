/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Client/Attic/ebiz_CheckLotno_CL.js,v $
<<<<<<< ebiz_CheckLotno_CL.js
 *     	   $Revision: 1.1.4.3.4.7.2.3 $
 *     	   $Date: 2015/12/01 15:30:56 $
 *     	   $Author: grao $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_208 $
=======
 *     	   $Revision: 1.1.4.3.4.7.2.3 $
 *     	   $Date: 2015/12/01 15:30:56 $
 *     	   $Author: grao $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_208 $
>>>>>>> 1.1.4.3.4.7.2.2
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
*
 *****************************************************************************/

function CheckLotNumbers(type,name)
{
	//alert('HI');
	var woid=nlapiGetFieldValue('custpage_workorder');
	var Location=nlapiGetFieldValue('cusppage_site');
	
	//#case 20149371
	var Qty=nlapiGetFieldValue('custpage_kitqty');
	var expQty=nlapiGetFieldValue('custpage_kitqtyexp');
	
	//alert(expQty);
	
	if(expQty.indexOf(' ') !=-1)
	{

		alert('Please enter Exp Qty as number.');// case# 201415877
		return false;
	}
		
	
	if(parseFloat(expQty)<=0){
		alert("Exp Qty should be greater than zero.");
		return false;
	}
	if(parseFloat(expQty)>parseFloat(Qty)){
		alert("Exp Qty should not be greater than Qty.");
		return false;
		
	}
	else if(isNaN(expQty))
	{
		alert('Please enter Exp Qty as number.');// case# 201415877
		return false;
	}
	
	//alert(woid);
	//Case# 20149242 starts
	var wosearchresultsitem='';
	if(woid!='' && woid!='null' && woid!=null)//Case# 20149242 ends
	wosearchresultsitem = nlapiLoadRecord('workorder', woid);
	if(wosearchresultsitem!='' && wosearchresultsitem!='null' && wosearchresultsitem!=null)
	var lineItems= wosearchresultsitem.getLineItemCount('item');
	var lineNum = nlapiGetLineItemCount('custpage_deliveryord_items');	
	//case# 20148656 starts 
	var lineLoc = nlapiGetFieldValue('custpage_binlocation');
	//alert(Location);
	//alert(lineLoc);
	if(lineLoc !=null && lineLoc !='')
	{
		var filterLoc = new Array();
		filterLoc.push(new nlobjSearchFilter('internalid',null,'anyof',lineLoc));
		filterLoc.push(new nlobjSearchFilter('custrecord_ebizsitelocf',null,'anyof',Location));
		//Case# 20125980�start
		filterLoc.push(new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'anyof', ['6','7']));
		//Case# 20125980�End
		var SearchRec=nlapiSearchRecord('customrecord_ebiznet_location',null,filterLoc,null);
		if(SearchRec == null || SearchRec == '')
		{
			alert('Select valid Bin Location');
			return false;
		}
		
	}
	//case# 20148656 ends
	for (m=1; m <= lineNum; m++) 
	{
		var ItemName="";				
		//var ItemName = nlapiGetLineItemText('custpage_deliveryord_items', 'custpage_deliveryord_sku', m);
		var ItemId = nlapiGetLineItemValue('custpage_deliveryord_items', 'custpage_hiddenitemid', m);
		
		var fields = ['recordType','custitem_ebizbatchlot','itemid'];
		var columns = nlapiLookupField('item',ItemId, fields);
		var ItemType = columns.recordType;					
		var batchflag = columns.custitem_ebizbatchlot;
		var itemfamId= columns.custitem_item_family;
		var itemgrpId= columns.custitem_item_group;
		ItemName= columns.itemid;
		//alert(ItemName);
		//alert('m'+m);
		var oldbinloc = nlapiGetLineItemValue('custpage_deliveryord_items', 'custpage_hiddenlocation', m);
		var newbinloc = nlapiGetLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_location', m);
			var oldlotno=nlapiGetLineItemValue('custpage_deliveryord_items', 'custpage_hiddenlotno', m);
			var newlotno=nlapiGetLineItemValue('custpage_deliveryord_items', 'custpage_lotno', m);
			var linelqty =nlapiGetLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_expqty', m);
		var vBackorderQty =nlapiGetLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_backordqty', m);
		if(vBackorderQty >0)
		{
			alert('Item:'+ItemName+' has Backordered quantity');
			return false;
		}
			//alert(linelqty);
			if(ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem"  || batchflag=="T")
			{
				if((newbinloc == "" || newbinloc == null) || (newlotno == "" || newlotno == null))
				{
					alert('Select Lotnumber/BinLocation for the LotnumberedItem');
					return false;
				}

			}
			//case# 20148656 starts
			if(oldbinloc!=newbinloc)
			{
				var filterLoc = new Array();
				filterLoc.push(new nlobjSearchFilter('internalid',null,'anyof',newbinloc));
				filterLoc.push(new nlobjSearchFilter('custrecord_ebizsitelocf',null,'anyof',Location));
				//Case# 20125980�start
				filterLoc.push(new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'anyof', ['6','7']));
				//Case# 20125980�End
				var SearchRec=nlapiSearchRecord('customrecord_ebiznet_location',null,filterLoc,null);
				if(SearchRec == null || SearchRec == '')
				{
					alert('Select valid Bin Location');
					return false;
				}
			}
			//case# 20148656 ends
			
			if((oldbinloc != "" && oldbinloc != null) || (oldlotno != "" && oldlotno != null)){
				if(oldbinloc!=newbinloc || oldlotno!=newlotno)
				{ 
				var res= CheckLotno(ItemId,Location,newlotno,newbinloc,ItemType,batchflag,ItemName,linelqty);
				return res;
				}
				
			}
	}
	return true;
		
}
		






function CheckLotno(ItemId,Location,newlotno,binlocation,ItemType,batchflag,ItemName,linelqty){
	var filterLotno = new Array();
	filterLotno.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', ItemId));
	filterLotno.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', Location));
	//filterLotno.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', newlotno));
	if(ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem"  || batchflag=="T")
	{
		filterLotno.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', newlotno));
	}
	
	filterLotno.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', binlocation));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('internalid');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_qoh');
		
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filterLotno,columns);
	if(searchresults!=null)
	{
		//case# 20149511 starts
		var errorflag="F";
		for(var i=0;i<searchresults.length;i++)
		{
			var Qoh=searchresults[i].getValue('custrecord_ebiz_qoh');
			if(Qoh >0 && parseFloat(linelqty)< parseFloat(Qoh))
			{
				errorflag="T";
				return true;
			}
			/*else
			{
			alert('No sufficient Inventory available for this Item:'+ItemName);
			return false;

			}*/
		}
		if(errorflag=="F")
		{
			alert('No sufficient Inventory available for this Item:'+ItemName);
			return false;
		}
	}
	else
	{
		alert('Select Valid Lotnumber/BinLocation for the Item:'+ItemName);
		return false;
	}
	
}

//function CheckBinlocation(ItemId,Location,newbinloc){
//	var filterbinloc = new Array();
//	filterbinloc[0] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', ItemId);
//	filterbinloc[1] = new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', Location);
//	filterbinloc[2] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', newbinloc);
//
//	var columns = new Array();
//	columns[0] = new nlobjSearchColumn('name');
//	columns[1] = new nlobjSearchColumn('internalid');
//		
//	var searchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filterbinloc,columns);
//	if(searchresults!=null)
//	{		
//		return true;
//	}
//	else
//	{
//		alert('Select Valid Binlocation for the Item');
//		return false;
//	}
//	
//}



