/***************************************************************************
 eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Inbound/Suitelet/Attic/ebiz_RF_FifoDateConfirm.js,v $
 *     	   $Revision: 1.1.2.1.2.5 $
 *     	   $Date: 2014/05/30 00:26:49 $
 *     	   $Author: nneelam $
 *     	   $Name: t_eBN_2014_1_StdBundle_3_62 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_FifoDateConfirm.js,v $
 * Revision 1.1.2.1.2.5  2014/05/30 00:26:49  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.1.2.4  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.2.1.2.3  2013/06/04 07:26:55  skreddy
 * CASE201112/CR201113/LOG201121
 * Lot Parsing CR for PCT
 *
 * Revision 1.1.2.1.2.2  2013/04/17 16:04:01  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.1.2.1  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.1.2.1  2013/02/07 08:40:20  skreddy
 * CASE201112/CR201113/LOG201121
 * RF Lot auto generating FIFO enhancement
 *
 * Revision 1.18.2.55  2012/08/28 15:49:03  spendyala
 * CASE201112/CR201113/LOG201121
 * Added screen for cluster pick qty exception.
 *
 *
 *****************************************************************************/

function FIFODateConfirm(request, response){
	if (request.getMethod() == 'GET') {
		//	Get the Error Message from the previous screen, which is passed as a parameter		
		getError = request.getParameter('custparam_error');
		nlapiLogExecution('DEBUG', 'Into Request', getError);
		var screenNo=request.getParameter('custparam_screenno');
		var functionkeyHtml=getFunctionkeyScript('_rf_error'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_error' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>CONFIRMATION: <br><label>" + getError + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>CONTINUE <input name='cmdContinue' type='submit' value='F8'/>";
		html = html + "					BACK <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var optedEvent = request.getParameter('cmdPrevious');
		var getError = request.getParameter('custparam_error');
		var getScreenNo = request.getParameter('custparam_screenno');
		nlapiLogExecution('DEBUG', 'Screen No', getScreenNo);
		nlapiLogExecution('DEBUG', 'optedEvent', optedEvent);

		var POarray = new Array();

		// From Check-In process
		POarray["custparam_poid"] = request.getParameter('custparam_poid');
		POarray["custparam_poitem"] = request.getParameter('custparam_poitem');
		POarray["custparam_lineno"] = request.getParameter('custparam_lineno');
		POarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
		POarray["custparam_pointernalid"] = request.getParameter('custparam_pointernalid');
		POarray["custparam_poqtyentered"] = request.getParameter('custparam_poqtyentered');
		POarray["custparam_poitemremainingqty"] = request.getParameter('custparam_poitemremainingqty');
		POarray["custparam_polinepackcode"] = request.getParameter('custparam_polinepackcode');
		POarray["custparam_polinequantity"] = request.getParameter('custparam_polinequantity');
		POarray["custparam_polinequantityreceived"] = request.getParameter('custparam_polinequantityreceived');
		POarray["custparam_polineitemstatus"] = request.getParameter('custparam_polineitemstatus');
		POarray["custparam_polineitemstatusValue"] = request.getParameter('custparam_polineitemstatusValue');
		POarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
		POarray["custparam_itemcube"] = request.getParameter('custparam_itemcube');
		POarray["custparam_actualbegindate"] = request.getParameter('custparam_actualbegindate');
		POarray["custparam_actualbegintime"] = request.getParameter('custparam_actualbegintime');
		POarray["custparam_actualbegintimeampm"] = request.getParameter('custparam_actualbegintimeampm');
		POarray["custparam_whlocation"] = request.getParameter('custparam_whlocation');
		POarray["custparam_batchno"] = request.getParameter('custparam_batchno');
		POarray["custparam_fifodate"]=request.getParameter('custparam_fifodate');
		POarray["custparam_baseuomqty"] = request.getParameter('custparam_baseuomqty');		
		POarray["custparam_shelflife"] = request.getParameter('custparam_shelflife');
		POarray["custparam_captureexpirydate"] = request.getParameter('custparam_captureexpirydate');
		POarray["custparam_capturefifodate"] = request.getParameter('custparam_capturefifodate');
		POarray["custparam_fifocode"] = request.getParameter('custparam_fifocode');
		POarray["custparam_bestbeforedate"] = request.getParameter('custparam_bestbeforedate');
		POarray["custparam_mfgdate"] = request.getParameter('custparam_mfgdate');
		POarray["custparam_lastdate"] = request.getParameter('custparam_lastdate');
		POarray["custparam_expdate"] = request.getParameter('custparam_expdate');
		POarray["custparam_actscanbatchno"] = request.getParameter('custparam_actscanbatchno');



		// From Putaway process
		/*POarray["custparam_lpno"] = request.getParameter('custparam_lpno');
		POarray["custparam_quantity"] = request.getParameter('custparam_quantity');
		POarray["custparam_exceptionquantity"] = request.getParameter('custparam_exceptionquantity');
		POarray["custparam_exceptionQuantityflag"] = request.getParameter('custparam_exceptionQuantityflag');
		POarray["custparam_location"] = request.getParameter('custparam_location');
		POarray["custparam_beginlocation"] =  request.getParameter('custparam_beginlocation');
		POarray["custparam_item"] = request.getParameter('custparam_item');
		POarray["custparam_itemDescription"] = request.getParameter('custparam_itemDescription');
		POarray["custparam_itemid"] = request.getParameter('custparam_itemid');
		POarray["custparam_pointernalid"] = request.getParameter('custparam_pointernalid');
		POarray["custparam_polinenumber"] = request.getParameter('custparam_polinenumber');
		POarray["custparam_poitemstatus"] = request.getParameter('custparam_poitemstatus');
		POarray["custparam_popackcode"] = request.getParameter('custparam_popackcode');
		POarray["custparam_beginlocation"] = request.getParameter('custparam_beginlocation');
		POarray["custparam_endlocation"] = request.getParameter('custparam_endlocation');
		POarray["custparam_itemtext"] = request.getParameter('custparam_itemtext');
		POarray["custparam_mfgdate"] = request.getParameter('custparam_mfgdate');
		POarray["custparam_expdate"] = request.getParameter('custparam_expdate');
		POarray["custparam_baseuomqty"] = request.getParameter('custparam_baseuomqty');
		POarray["custparam_recordid"] = request.getParameter('custparam_recordid');
		POarray["custparam_option"] = request.getParameter('custparam_option');
		POarray["custparam_recordcount"] = request.getParameter('custparam_recordcount');
		POarray["custparam_lpNumbersArray"] = request.getParameter('custparam_lpNumbersArray');
		POarray["custparam_skipid"] = request.getParameter('custparam_skipid');
		POarray["custparam_trantype"] = request.getParameter('custparam_trantype');
		POarray["custparam_itemstatus"] = request.getParameter('custparam_itemstatus');
		nlapiLogExecution('DEBUG', 'trantype', POarray["custparam_trantype"]);
		POarray["custparam_uomidtext"] = request.getParameter('custparam_uomidtext');
		POarray["custparam_number"] = parseInt(request.getParameter('custparam_number'));
		POarray["custparam_polineitemlp"]= request.getParameter('custparam_polineitemlp');
		POarray["custparam_serialno"] = request.getParameter('custparam_serialno');*/

		if (optedEvent == 'F7') {
			nlapiLogExecution('DEBUG', 'CHECK IN FIFO DATE F7 Pressed');
			response.sendRedirect('SUITELET', 'customscript_rf_checkin_fifo_date', 'customdeploy_rf_checkin_fifo_date_di', false, POarray);

		}

		else
		{
			response.sendRedirect('SUITELET', 'customscript_rf_checkin_lp', 'customdeploy_rf_checkin_lp_di', false, POarray);
		}

		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}
}
