/***************************************************************************
  eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_PutawayConfirmation.js,v $
 *     	   $Revision: 1.5.2.7.4.6.2.14 $
 *     	   $Date: 2015/08/11 22:09:48 $
 *     	   $Author: mvarre $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_PutawayConfirmation.js,v $
 * Revision 1.5.2.7.4.6.2.14  2015/08/11 22:09:48  mvarre
 * case# 201413964
 * Dealmed Issue
 * To show Item name at Putaway confirmation
 *
 * Revision 1.5.2.7.4.6.2.13  2015/07/23 15:31:14  grao
 * 2015.2   issue fixes  201413500
 *
 * Revision 1.5.2.7.4.6.2.12  2015/07/20 15:29:25  grao
 * 2015.2   issue fixes  201413500
 *
 * Revision 1.5.2.7.4.6.2.11  2014/08/04 15:15:24  skreddy
 * case # 20149764
 * jawbone SB issue fix
 *
 * Revision 1.5.2.7.4.6.2.10  2014/06/27 11:20:38  sponnaganti
 * case# 20149037
 * Compatability Test Acc issue fix
 *
 * Revision 1.5.2.7.4.6.2.9  2014/06/13 06:40:09  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.5.2.7.4.6.2.8  2014/06/06 06:32:52  skavuri
 * Case# 20148749 (Refresh Functionality ) SB Issue Fixed
 *
 * Revision 1.5.2.7.4.6.2.7  2014/05/30 00:26:49  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.5.2.7.4.6.2.6  2014/01/06 13:12:59  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.5.2.7.4.6.2.5  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.5.2.7.4.6.2.4  2013/05/22 15:13:54  grao
 * While Doing Location Exception Inventory is Updated with Old Lp related issues fixed
 *
 * Revision 1.5.2.7.4.6.2.3  2013/04/17 16:04:02  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.5.2.7.4.6.2.2  2013/04/02 14:38:18  schepuri
 * tsg sb putaway qty exception
 *
 * Revision 1.5.2.7.4.6.2.1  2013/03/05 14:45:05  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Lexjet production as part of Standard bundle
 *
 * Revision 1.5.2.7.4.6  2013/02/07 15:10:34  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.5.2.7.4.5  2012/12/12 08:53:51  grao
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.5.2.7.4.4  2012/11/01 14:55:35  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.5.2.7.4.3  2012/09/27 10:56:31  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.5.2.7.4.2  2012/09/26 12:48:01  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.5.2.7.4.1  2012/09/24 10:07:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.5.2.7  2012/09/03 19:09:37  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Fisk Production Issue Fixes
 *
 * Revision 1.5.2.6  2012/08/10 14:45:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Navigation issue when F7 is pressed resolved.
 *
 * Revision 1.5.2.5  2012/06/04 09:24:49  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.5.2.4  2012/04/12 21:41:29  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to showing correct  details of the scanned  LP .
 *
 * Revision 1.5.2.3  2012/03/19 10:08:29  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.5.2.2  2012/02/22 12:36:33  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.5.2.1  2012/02/20 15:24:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.5  2011/12/28 06:57:00  spendyala
 * CASE201112/CR201113/LOG201121
 * made changes for qty exception
 *
 * Revision 1.4  2011/08/24 12:42:30  schepuri
 * CASE201112/CR201113/LOG201121
 * RF PutAway confirmation based on putseq no
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/
//to sort an array
function arr(seq, lp) {
	nlapiLogExecution('DEBUG', 'arr seq', seq);
	nlapiLogExecution('DEBUG', 'arr lp', lp);
	this.Seq= parseFloat(seq);
	this.Lp = lp;
}
//to sort an array
function sortBySeq(a, b) {
	var x = a.Seq;
	var y = b.Seq;
	return ((x < y) ? -1 : ((x > y) ? 1 : 0));
}

function PutawayConfirmation(request, response){
	if (request.getMethod() == 'GET') {
		//	Get the LP#, Quantity, Location 
		//  from the previous screen, which is passed as a parameter
		var TempLPNoArray = request.getParameter('custparam_lpNumbersArray');
		nlapiLogExecution('DEBUG', 'lpNumbersFromArray', TempLPNoArray);
		
		var getLPNo = request.getParameter('custparam_lpno');
		var newgetLPNo = request.getParameter('custparam_newlpno');
		
		if(getLPNo == '' || getLPNo == null)
		{
			getLPNo = newgetLPNo;
		}
		var confirmedLPCount = request.getParameter('custparam_confirmedLPCount');
		
		var getSerialNo = request.getParameter('custparam_serialno');

		var NewGenLP = request.getParameter('custparam_polineitemlp');
		nlapiLogExecution('DEBUG', 'before starting confirmedLPCount', confirmedLPCount);
		nlapiLogExecution('DEBUG', 'before starting getSerialNo', getSerialNo);
		nlapiLogExecution('DEBUG', 'before starting NewGenLP', NewGenLP);

		if (confirmedLPCount == null){
			confirmedLPCount = 0;
		}

		nlapiLogExecution('DEBUG', 'starting confirmedLPCount', confirmedLPCount);

		var EnteredLpNo = request.getParameter('custparam_enteredLPNo');

		var filters = new Array(); 
		var newarray = new Array();
		var TempLPArray;
		var lpCount=0;
		var TArray = new Array();

		if(TempLPNoArray != "" && TempLPNoArray != null)
			TempLPArray = TempLPNoArray .split(',');
		if(TempLPArray != "" && TempLPArray != null)
		{
			nlapiLogExecution('DEBUG', 'TempLPArray.length', TempLPArray.length);
			for(i=0;i<TempLPArray.length;i++)
			{                       
				filters[0] = new nlobjSearchFilter('custrecord_lpno', null, 'is',TempLPArray[i]); 
				filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'is','2'); // PUTW task 

				var columns = new Array(); 
				columns[0] = new nlobjSearchColumn('custrecord_startingputseqno','custrecord_actbeginloc'); 
				columns[1] = new nlobjSearchColumn('custrecord_lpno'); 

				var searchresults1 = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null, filters, columns);
				if(searchresults1 != null)
					newarray.push(new  arr(searchresults1[0].getValue('custrecord_startingputseqno','custrecord_actbeginloc'),searchresults1[0].getValue('custrecord_lpno')));
				//var f = newarray[i];
				//nlapiLogExecution('DEBUG', 'newarray.sort(sortBySeq)   f.seq', f.Seq);
				//nlapiLogExecution('DEBUG', 'newarray.sort(sortBySeq)   f.lp', f.Lp);
			}



			TArray  = newarray.sort(sortBySeq);
			lpCount = TArray.length;
		}
		for ( var count = 0; count < TArray.length; count++) 
		{
			if(TArray[count].Lp==EnteredLpNo)
				confirmedLPCount=count;
		}

		//for(confirmedLPCount=0; confirmedLPCount<lpCount; confirmedLPCount++)
		if(confirmedLPCount>0)
		{
			if(confirmedLPCount<lpCount)
			{
				nlapiLogExecution('DEBUG', 'inif confirmedLPCount as index', confirmedLPCount);
				var b = TArray[confirmedLPCount];
				//if(b!=null && b!='')
				//{
				var sequnceNo = '';
				var sortedLP = '';
				try
				{
					sequnceNo = b.Seq;
					sortedLP = b.Lp;
				}
				catch(exp)
				{
					nlapiLogExecution('DEBUG', 'Exception ', exp);
					sequnceNo='';
					sortedLP='';
				}

			nlapiLogExecution('DEBUG', 'newarray.sort(sortBySeq)   sequnceNo', sequnceNo);
			nlapiLogExecution('DEBUG', 'newarray.sort(sortBySeq)   sortedLP', sortedLP);



				 getLPNo = sortedLP;
				//}
				//break;
			}
		}
		//var getLPNo = request.getParameter('custparam_lpno');
		var getCartLPNo = request.getParameter('custparam_cartno');
		var getRecordCount;

		nlapiLogExecution('DEBUG', 'getLPNo', getLPNo);
		nlapiLogExecution('DEBUG', 'getCartLPNo', getCartLPNo);

		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);

		var filters = new Array();
		//        filters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
		//        filters[1] = new nlobjSearchFilter('custrecord_transport_lp', null, 'is', getCartLPNo);
		//		filters[2] = new nlobjSearchFilter('custrecord_lpno', null, 'is', getLPNo);

		if (getOptedField == "4") {
			filters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
			filters[1] = new nlobjSearchFilter('custrecord_transport_lp', null, 'is', getCartLPNo);
		}
		else {
			filters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
			if(getLPNo!=null && getLPNo!='')
				filters[1] = new nlobjSearchFilter('custrecord_lpno', null, 'is', getLPNo);
		}

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_transport_lp');
		columns[1] = new nlobjSearchColumn('custrecord_lpno');
		columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[3] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[4] = new nlobjSearchColumn('custrecord_sku');
		columns[5] = new nlobjSearchColumn('custrecord_skudesc');
		columns[6] = new nlobjSearchColumn('custrecord_wms_location');
		
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

		if (getCartLPNo != null || getLPNo != null) {
			if (searchresults != null && searchresults.length > 0) {
				nlapiLogExecution('DEBUG', 'SearchResults of given LP', 'LP is found');

				var getLPNo = searchresults[0].getValue('custrecord_lpno');

				var getQuantity = searchresults[0].getValue('custrecord_expe_qty');
				var getFetchedItem = searchresults[0].getValue('custrecord_sku');
				var getFetchedItemText = searchresults[0].getText('custrecord_sku');
				var getFetchedItemDescription = searchresults[0].getValue('custrecord_skudesc');
				var getFetchedLocationId = searchresults[0].getValue('custrecord_actbeginloc');
				var getFetchedLocation = searchresults[0].getText('custrecord_actbeginloc');
				nlapiLogExecution('DEBUG', 'SearchResults of given LP getFetchedLocation', getFetchedLocation);
				var getWHLocation = searchresults[0].getValue('custrecord_wms_location');

				nlapiLogExecution('DEBUG', 'SearchResults of given LP getWHLocation', getWHLocation);
				getRecordCount = request.getParameter('custparam_recordcount');
				nlapiLogExecution('DEBUG', 'SearchResults of given LP getRecordCount', getRecordCount);
			}
		}

		var qtyexceptionFlag=request.getParameter('custparam_qtyexceptionflag');
		nlapiLogExecution('DEBUG', 'qtyexceptionFlag',qtyexceptionFlag);
		var vremqty='';
		if(qtyexceptionFlag=='true')
		{
			getQuantity=request.getParameter('custparam_exceptionqty');
			vremqty=request.getParameter('custparam_remainingqty');
			nlapiLogExecution('DEBUG', 'getQuantity',getQuantity);
			nlapiLogExecution('DEBUG', 'vremqty',vremqty);
		}
		else
			qtyexceptionFlag='false';

		var itemPalletQuantity = request.getParameter('custparam_itempalletquantity');

		nlapiLogExecution('DEBUG', 'getLPNo', getLPNo);


		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4,st5,st6,st7;
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "CONFIRMAR LA CANTIDAD";
			st2 = "CONFIRMAR UBICACI&#211;N";
			st3 = "EXCEPCI&#211;N LOC";
			st4 = "CANT EXCEPCI&#211;N";
			st5 = "CONFIRMAR";
			st6 = "ANTERIOR";
			st7 = "ART&#205;CULO"; 

		}
		else
		{
			st0 = "";
			st1 = "CONFIRM QUANTITY";
			st2 = "CONFIRM LOCATION";
			st3 = "LOC EXCEPTION";
			st4 = "QTY EXCEPTION";
			st5 = "CONF";
			st6 = "PREV";
			st7 = "ITEM";

		}
		var functionkeyHtml=getFunctionkeyScript('_rf_putaway_lp'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";     
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		
		//html = html + " document.getElementById('cmdConfirm').focus();";    


		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdConfirm').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_putaway_lp' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>LP : <label>" + getLPNo + "</label>";
		html = html + "				<input type='hidden' name='hdnLPNo' value=" + getLPNo + "></td>";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getQuantity + "></td>";
		html = html + "				<input type='hidden' name='hdnRemQuantity' value=" + vremqty + "></td>";
		html = html + "				<input type='hidden' name='hdnFetchedLocation' value=" + getFetchedLocation + "></td>";
		html = html + "				<input type='hidden' name='hdnFetchedItem' value=" + getFetchedItem + "></td>";
		html = html + "				<input type='hidden' name='hdnFetchedItemDescription' value='" + getFetchedItemDescription + "'></td>";
		html = html + "				<input type='hidden' name='hdnFetchedLocationId' value=" + getFetchedLocationId + "></td>";
		html = html + "				<input type='hidden' name='hdnRecordCount' value=" + getRecordCount + "></td>";
		html = html + "				<input type='hidden' name='hdnConfirmedLPCount' value=" + confirmedLPCount + "></td>";
		html = html + "				<input type='hidden' name='hdnlpCount' value=" + lpCount + "></td>";
		html = html + "				<input type='hidden' name='hdnlpNumbersArray' value=" + TempLPNoArray + "></td>";
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		html = html + "				<input type='hidden' name='hdnCartLPNo' value=" + getCartLPNo + ">";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + getWHLocation + ">";
		html = html + "				<input type='hidden' name='hdnItemPalletQuantity' value=" + itemPalletQuantity + ">";
		html = html + "				<input type='hidden' name='hdnexceptionQuantityflag' value=" + qtyexceptionFlag + ">";
		html = html + "				<input type='hidden' name='hdnenteredlp' value=" + EnteredLpNo + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
			html = html + "				<input type='hidden' name='hdnSerialNo' value=" + getSerialNo + ">";
		html = html + "				<input type='hidden' name='hdnNewGenLP' value=" + NewGenLP + ">";
		html = html + "				<input type='hidden' name='hdnflag' >";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + ": <label>" + getQuantity + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + ": </td></tr>";
		html = html + "				<tr><td align = 'left'><label>" + getFetchedLocation + "</label></td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st7 + ": <label>" + getFetchedItemText + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " <input name='cmdLocException' type='submit' value='F10'/></td></tr>";
		html = html + "				 <tr><td align = 'left'>" + st4 + " <input name='cmdQtyException' type='submit' value='F2'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" +  st5 + " <input name='cmdConfirm' id='cmdConfirm' type='submit' value='ENT' onclick='this.form.hdnflag.value=this.value;this.disabled=true; form.submit();this.form.cmdLocException.disabled=true;this.form.cmdQtyException.disabled=true;this.form.cmdPrevious.disabled=true;return false'/>";
		html = html + "				" + st6 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('cmdConfirm').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);


	}
	else {
		nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is not null');
		nlapiLogExecution('DEBUG', 'Clicked on Confirm', request.getParameter('hdnflag'));
		// This variable is to hold the LP entered.
		var getLPNo = request.getParameter('hdnLPNo');	//request.getParameter('custparam_lpno');
		nlapiLogExecution('DEBUG', 'getLPNo1', getLPNo);

		var getFetchedLocation = request.getParameter('hdnFetchedLocation');
		var getFetchedItem = request.getParameter('hdnFetchedItem');
		var getFetchedItemDescription = request.getParameter('hdnFetchedItemDescription');
		var getFetchedLocationId = request.getParameter('hdnFetchedLocationId');
		var getWHLocation = request.getParameter('hdnWhLocation');

		nlapiLogExecution('DEBUG', 'getFetchedLocation', getFetchedLocation);
		nlapiLogExecution('DEBUG', 'getQuantity', getQuantity);
		nlapiLogExecution('DEBUG', 'getFetchedItem', getFetchedItem);
		nlapiLogExecution('DEBUG', 'getFetchedItemDescription', getFetchedItemDescription);
		nlapiLogExecution('DEBUG', 'getWHLocation', getWHLocation);

		var getOptedField = request.getParameter('hdnOptedField');
		nlapiLogExecution('DEBUG', 'hdnOptedField', getOptedField);

		var getCartLPNo = request.getParameter('hdnCartLPNo');

		var filters = new Array();

		if (getOptedField == "4") 
		{
			filters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
			//filters[1] = new nlobjSearchFilter('custrecord_transport_lp', null, 'is', getCartLPNo);
			filters[1] = new nlobjSearchFilter('custrecord_lpno', null, 'is', getLPNo);
		}
		else
		{
			filters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
			filters[1] = new nlobjSearchFilter('custrecord_lpno', null, 'is', getLPNo);
		}

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_transport_lp');
		columns[1] = new nlobjSearchColumn('custrecord_lpno');
		columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[3] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[4] = new nlobjSearchColumn('custrecord_sku');
		columns[5] = new nlobjSearchColumn('custrecord_skudesc');
		columns[6] = new nlobjSearchColumn('custrecord_line_no');
		columns[7] = new nlobjSearchColumn('name');

		// Added by Phani on 03-25-2011
		columns[8] = new nlobjSearchColumn('custrecord_wms_location');

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

		var POarray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);

		var getQuantity = request.getParameter('hdnQuantity');
		var getRemQuantity = request.getParameter('hdnRemQuantity');
		
		POarray["custparam_exceptionquantity"] = getQuantity;
		POarray["custparam_remainingqty"] = getRemQuantity;
		POarray["custparam_whlocation"] = getWHLocation;
		POarray["custparam_qtyexceptionflag"]=request.getParameter('hdnexceptionQuantityflag');
		var getOptedField = request.getParameter('hdnOptedField');
		POarray["custparam_option"] = getOptedField;

		POarray["custparam_screenno"] = '7';
		POarray["custparam_serialno"] = request.getParameter('hdnSerialNo');
		POarray["custparam_polineitemlp"] = request.getParameter('hdnNewGenLP');
		nlapiLogExecution('DEBUG', 'POarray["custparam_serialno"]', POarray["custparam_serialno"]);
		nlapiLogExecution('DEBUG', 'POarray["custparam_qtyexceptionflag"] in post', POarray["custparam_qtyexceptionflag"]);
		var getRecordCount = request.getParameter('hdnRecordCount');

		POarray["custparam_cartno"] = request.getParameter('hdnCartLPNo');
		POarray["custparam_itempalletquantity"] = request.getParameter('hdnItemPalletQuantity');

		if (searchresults != null && searchresults.length > 0) {
			nlapiLogExecution('DEBUG', 'SearchResults of given LP', 'LP is found');

			var getLPId = searchresults[0].getId();
			nlapiLogExecution('DEBUG', 'get LP Internal Id', getLPId);

			POarray["custparam_lpno"] = searchresults[0].getValue('custrecord_lpno');
			POarray["custparam_quantity"] = searchresults[0].getValue('custrecord_expe_qty');
			POarray["custparam_item"] = searchresults[0].getValue('custrecord_sku');
			POarray["custparam_itemtext"] = searchresults[0].getText('custrecord_sku');
			POarray["custparam_itemDescription"] = searchresults[0].getValue('custrecord_skudesc');
			POarray["custparam_location"] = searchresults[0].getValue('custrecord_actbeginloc');
			POarray["custparam_beginlocation"] = searchresults[0].getText('custrecord_actbeginloc');
			POarray["custparam_recordcount"] = getRecordCount;
			POarray["custparam_recordid"] = searchresults[0].getId();
			POarray["custparam_lineno"] = searchresults[0].getValue('custrecord_line_no');
			POarray["custparam_pono"] = searchresults[0].getValue('name');

			POarray["custparam_lpNumbersArray"] = request.getParameter('hdnlpNumbersArray');
			POarray["custparam_confirmedLpCount"] = parseFloat(request.getParameter('hdnConfirmedLPCount')) + 1;
			POarray["custparam_lpCount"] = parseFloat(request.getParameter('hdnlpCount'));
			POarray["custparam_enteredLPNo"] = request.getParameter('hdnenteredlp');
		}
		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept PO #.
		if (request.getParameter('cmdPrevious') == 'F7') {
			nlapiLogExecution('DEBUG', 'Clicked on Previous', request.getParameter('cmdPrevious'));
			response.sendRedirect('SUITELET', 'customscript_rf_putaway_lp', 'customdeploy_rf_putaway_lp_di', false, POarray);
		}
		else 
			if (request.getParameter('hdnflag') == 'ENT')
			{


				if(getFetchedItem == null || getFetchedItem == '')
				{
					getFetchedItem = POarray["custparam_item"];
				}
// case# 201417140
				var columns = nlapiLookupField('item', getFetchedItem, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
				//var serialFlag = itemSubtype.custitem_ebizserialin;
				var Itype ='';
				var serialFlag = 'F';
				if(columns != null && columns != '')
				{
					Itype = columns.recordType;
					serialFlag = columns.custitem_ebizserialin;
					nlapiLogExecution('DEBUG', 'Itype', Itype);
					nlapiLogExecution('DEBUG', 'columns', columns);
				}

				if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem" || serialFlag == 'T') 
				{
					nlapiLogExecution('DEBUG', 'LP#', POarray["custparam_lpno"]);

					POarray["custparam_exceptionqty"] = getQuantity;
					POarray["custparam_putawayscreen"] = "PUTWCONFIRMSERIAL";
					var vfiltersLP = new Array();
					vfiltersLP[0] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', POarray["custparam_lpno"]);

					var vSrchRecordLP = nlapiSearchRecord('customrecord_ebiznetserialentry', null, vfiltersLP);
					if(vSrchRecordLP==null || vSrchRecordLP =='')				
					{
						response.sendRedirect('SUITELET', 'customscript_putawayqtyexp_serial_no', 'customdeploy_putawayqtyexp_serial_no', false, POarray);
						return;
					}
				}



				nlapiLogExecution('DEBUG', 'Clicked on Confirm', request.getParameter('cmdConfirm'));
				response.sendRedirect('SUITELET', 'customscript_rf_putaway_location', 'customdeploy_rf_putaway_location_di', false, POarray);
			}
			else 
				if (request.getParameter('cmdLocException') == 'F10') {
					nlapiLogExecution('DEBUG', 'Clicked on Location Exception', request.getParameter('cmdLocException'));
					response.sendRedirect('SUITELET', 'customscript_rf_putaway_loc_exception', 'customdeploy_rf_putaway_loc_exception_di', false, POarray);
				}
				else 
					if (request.getParameter('cmdQtyException') == 'F2') {
						nlapiLogExecution('DEBUG', 'Clicked on Quantity Exception', request.getParameter('cmdQtyException'));
						response.sendRedirect('SUITELET', 'customscript_rf_putaway_qty_exception', 'customdeploy_rf_putaway_qty_exception_di', false, POarray);
					}
		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}
}
