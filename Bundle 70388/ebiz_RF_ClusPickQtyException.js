/***************************************************************************
 	eBizNET Solutions
 ****************************************************************************/
/*******************************************************************************
 * ***************************************************************************
 * 
 * $Source:
 * /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_PickConfirmation_SL.js,v $
<<<<<<< ebiz_RF_ClusPickQtyException.js
 * $Revision: 1.1.2.17.4.5.2.54 $ $Date: 2015/08/11 15:38:18 $ $Author: grao $ $Name: b_WMS_2015_2_StdBundle_Issues $
=======
 * $Revision: 1.1.2.17.4.5.2.54 $ $Date: 2015/08/11 15:38:18 $ $Author: grao $ $Name: b_WMS_2015_2_StdBundle_Issues $
>>>>>>> 1.1.2.17.4.5.2.31
 * 
 * eBizNET version and checksum stamp. Do not remove. $eBiznet_VER:
 * .............. $eBizNET_SUM: ..... PRAMETERS
 * 
 * 
 * DESCRIPTION
 * 
 * Default Data for Interfaces
 * 
 * NOTES AND WARNINGS
 * 
 * INITATED FROM
 * 
 * 
 * REVISION HISTORY Pick Confirmation Changes
 * REVISION HISTORY Revision 1.7
 * 2011/07/29 11:41:38 rmukkera CASE201112/CR201113/LOG201121 Added
 * CreateShipManifest Functionality
 * 
 * Revision 1.6 2011/07/29 11:01:59 pattili CASE201112/CR201113/LOG201121
 * Corrected few minor issues
 * 
 * Revision 1.5 2011/07/21 06:52:08 pattili CASE201112/CR201113/LOG201121 1.
 * Added the CVS tag which was missing in this file 2. Issue pertaining to
 * container LP is fixed
 * 
 ******************************************************************************/

function ClusterPickingQtyException(request, response){

	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);
	if (request.getMethod() == 'GET') {

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7,st8,st9,st10;

		if( getLanguage == 'es_ES')
		{
			st1 = "CONFIRME LA REPLENS ABIERTAS";
			st2 = "INVENTARIO INSUFICIENTE EN LUGAR A GRANEL";
			st3 = "CANTIDAD ESPERADO:";
			st4 = "ART&#205;CULO:";
			st5 = "N&#218;MERO DE EMPAQUE:";
			st6 = "CANTIDAD REAL:";
			st7 = "RAZ&#211;N:";
			st8 = "INTRODUZCA LA CANTIDAD RESTANTE";
			st9 = "ENVIAR";
			st10 = "ANTERIOR";

		}
		else
		{
			st1 = "CONFIRM THE OPEN REPLENS"; 
			st2 = "INSUFFICIENT INVENTORY IN BULK LOCATION";
			st3 = "EXPECTED QTY : ";
			st4 = "ITEM: ";
			st5 = "CARTON NO : ";
			st6 = "ACTUAL QUANTITY : ";
			st7 = "REASON : ";
			st8 = "ENTER REMAINING QUANTITY ";
			st9 = "SEND ";
			st10 = "PREV";

		}

		var getWaveno = request.getParameter('custparam_waveno');
		var vClusterNo = request.getParameter('custparam_clusterno');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getBeginLocationId = request.getParameter('custparam_beginLocation');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var pickType=request.getParameter('custparam_picktype');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');        
		var getEnteredLocation = request.getParameter('custparam_endlocation');
		var getBeginBinLocation = request.getParameter('custparam_beginlocationname');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var getSerialNo = request.getParameter('custparam_serialno');
		var Itype = nlapiLookupField('item', getItemInternalId, 'recordType');
		var ItemRec = nlapiLoadRecord(Itype, getItemInternalId);
		var vClusterNo = request.getParameter('custparam_clusterno');
		var vBatchno = request.getParameter('custparam_batchno');		
		var RecordCount=request.getParameter('custparam_nooflocrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');	
		var NextItem=request.getParameter('custparam_nextitem');	
		//var pickType=request.getParameter('custparam_picktype'); Case# 201410696
		var getItem = ItemRec.getFieldValue('itemid');
		var name=request.getParameter('name');
		var getOrderNo=request.getParameter('custparam_ebizordno');
		var whLocation = request.getParameter('custparam_whlocation');
		var getLPContainerSize=request.getParameter('custparam_containersize');
		var remainqty=request.getParameter('custparam_remainqty');
		var getEnteredContainerNo = request.getParameter('custparam_newcontainerlp');
		var detailTask=request.getParameter('custparam_detailtask');
		var serialscanned = request.getParameter('custparam_serialscanned');
		var TotOrderqty = request.getParameter('custparam_TotOrderqty');

		if(getEnteredContainerNo==null || getEnteredContainerNo=='')
			getEnteredContainerNo=getContainerLpNo;

		//code added on 15Feb 2012 by suman
		var EntLoc = request.getParameter('custparam_EntLoc');
		var EntLocRec=request.getParameter('custparam_EntLocRec');
		var EntLocID=request.getParameter('custparam_EntLocId');
		var ExceptionFlag = request.getParameter('custparam_Exc');
		var EntLotID=request.getParameter('custparam_entLotId');
		var getReason='';


		nlapiLogExecution('DEBUG', 'getEnteredLocation', getEnteredLocation);
		nlapiLogExecution('DEBUG', 'getBeginBinLocation', getBeginBinLocation);
		nlapiLogExecution('DEBUG', 'NextLocation', NextLocation);
		nlapiLogExecution('DEBUG', 'NextItemInternalId', NextItemInternalId);
		nlapiLogExecution('DEBUG', 'NextItem', NextItem);
		nlapiLogExecution('DEBUG', 'getExpectedQuantity', getExpectedQuantity);
		nlapiLogExecution('DEBUG', 'getContainerLpNo', getContainerLpNo);

		// Case # 20126921�Start
		nlapiLogExecution('DEBUG', 'getDOLineId', getDOLineId);

		if(getDOLineId!=null && getDOLineId!='')
		{
			var folinerec = nlapiLoadRecord('customrecord_ebiznet_ordline', getDOLineId);
			if(folinerec!=null && folinerec!='')
			{
				var shipcompleteflag = folinerec.getFieldValue('custrecord_shipcomplete');
				nlapiLogExecution('DEBUG', 'shipcompleteflag', shipcompleteflag);
				if(shipcompleteflag=='T')
				{
					/*var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',getRecordInternalId,'custrecord_skiptask');
							if(skiptask==null || skiptask=='')
							{
								skiptask=1;
							}
							else
							{
								skiptask=parseInt(skiptask)+1;
							}

							nlapiLogExecution('DEBUG', 'skiptask',skiptask);

							nlapiSubmitField('customrecord_ebiznet_trn_opentask', getRecordInternalId, 'custrecord_skiptask', skiptask);*/

					var SOarray=new Array();					
					SOarray=buildTaskArray(getWaveno,vClusterNo,name,getOrderNo,null);				

					SOarray["custparam_language"] = getLanguage;
					//SOarray["custparam_screenno"] = '11EXP';


					SOarray["custparam_waveno"] = getWaveno;
					SOarray["custparam_recordinternalid"] = getRecordInternalId;
					SOarray["custparam_containerlpno"] = getContainerLpNo;
					SOarray["custparam_expectedquantity"] = getExpectedQuantity;
					SOarray["custparam_beginLocation"] = getBeginLocationId;
					//SOarray["custparam_item"] = getItem;
					SOarray["custparam_itemdescription"] = getItemDescription;
					SOarray["custparam_iteminternalid"] = getItemInternalId;
					SOarray["custparam_dolineid"] = getDOLineId;
					SOarray["custparam_invoicerefno"] = getInvoiceRefNo;
					//Case# 201410013 starts
					SOarray["custparam_beginlocationname"] = getBeginBinLocation;//201410845
					//SOarray["custparam_beginlocationname"] = NextLocation;
					//Case# 201410013 ends
					SOarray["custparam_endlocinternalid"] = getEndLocInternalId;
					SOarray["custparam_endlocation"] = getEnteredLocation;
					SOarray["custparam_orderlineno"] = getOrderLineNo;
					SOarray["custparam_clusterno"] = vClusterNo;
					SOarray["custparam_batchno"] = vBatchno;
					SOarray["custparam_nooflocrecords"] = RecordCount;
					SOarray["custparam_nextlocation"] = NextLocation;
					SOarray["name"] = name;
					SOarray["custparam_containersize"] = getLPContainerSize;
					SOarray["custparam_ebizordno"] = getOrderNo;
					SOarray["custparam_detailtask"] = detailTask;// case# 201412989
					//SOarray["custparam_number"] = getnumber;
					//SOarray["custparam_RecType"] = getRecType;
					//	SOarray["custparam_SerOut"] = getSerOut;
					//SOarray["custparam_SerIn"] = getSerIn;
					SOarray["custparam_nextiteminternalid"] = NextItemInternalId;
					SOarray["custparam_picktype"] = pickType;
					//SOarray["custparam_screenno"] = '11EXP';
					//screen no added if its comes cluster picking then we are redirecting to cluster picking screens only
					if(pickType=="CL")
					{
						SOarray["custparam_screenno"] = 'CL4';
					}
					else
					{
						SOarray["custparam_screenno"] = '11EXP';
					}

					SOarray["custparam_error"]='THIS ORDER HAS SHIP COMPLETE.CONTACT SUPERVISIOR.';
					SOarray["custparam_picktype"] = pickType;
					SOarray["custparam_whlocation"] = whLocation;
					SOarray["custparam_serialno"] = getSerialNo;
					SOarray["custparam_nextitem"] = NextItem;
					SOarray["custparam_newcontainerlp"] = getEnteredContainerNo;
					SOarray["custparam_remainqty"] = remainqty;
					SOarray["custparam_EntLoc"] = EntLoc;
					SOarray["custparam_EntLocRec"] = EntLocRec;
					SOarray["custparam_EntLocId"] = EntLocID;
					SOarray["custparam_Exc"] = ExceptionFlag;
					SOarray["custparam_entLotId"] = EntLotID;

					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;
				}
			}
		}
		// Case # 20126921�end






		var IsPickFaceLoc = 'F';

		IsPickFaceLoc = isPickFaceLocation(getItemInternalId,getEndLocInternalId);
		if(IsPickFaceLoc=='T' && ExceptionFlag !='Y')
		{
			var SOarray=new Array();
			SOarray["custparam_language"] = getLanguage;
			SOarray["custparam_screenno"] = '11EXP';
			SOarray["custparam_error"]=st1;//'CONFIRM THE OPEN REPLENS';

			SOarray["custparam_waveno"] = getWaveno;
			SOarray["custparam_recordinternalid"] = getRecordInternalId;
			SOarray["custparam_containerlpno"] = getContainerLpNo;
			SOarray["custparam_expectedquantity"] = getExpectedQuantity;
			SOarray["custparam_beginLocation"] = getBeginLocationId;
			//SOarray["custparam_item"] = getItem;
			SOarray["custparam_itemdescription"] = getItemDescription;
			SOarray["custparam_iteminternalid"] = getItemInternalId;
			SOarray["custparam_dolineid"] = getDOLineId;
			SOarray["custparam_invoicerefno"] = getInvoiceRefNo;
			SOarray["custparam_beginlocationname"] = getBeginBinLocation;
			SOarray["custparam_endlocinternalid"] = getEndLocInternalId;
			SOarray["custparam_endlocation"] = getEnteredLocation;
			SOarray["custparam_orderlineno"] = getOrderLineNo;
			SOarray["custparam_clusterno"] = vClusterNo;
			SOarray["custparam_batchno"] = vBatchno;
			SOarray["custparam_nooflocrecords"] = RecordCount;
			SOarray["custparam_nextlocation"] = NextLocation;
			SOarray["name"] = name;
			SOarray["custparam_containersize"] = getLPContainerSize;
			SOarray["custparam_ebizordno"] = getOrderNo;
			//SOarray["custparam_number"] = getnumber;
			//SOarray["custparam_RecType"] = getRecType;
			//	SOarray["custparam_SerOut"] = getSerOut;
			//SOarray["custparam_SerIn"] = getSerIn;
			SOarray["custparam_nextiteminternalid"] = NextItemInternalId;
			//SOarray["custparam_nextexpectedquantity"]=getNextExpectedQuantity;
			//	SOarray["custparam_nextrecordid"]=NextRecordId;
//			if(getZoneNo!=null && getZoneNo!="")
//			{
//			SOarray["custparam_ebizzoneno"] =  getZoneNo;
//			}
//			else
//			SOarray["custparam_ebizzoneno"] = '';

			nlapiLogExecution('DEBUG', 'getNextLocation',NextLocation);
			var openreplns = new Array();
			openreplns=getOpenReplns(getItemInternalId,getEndLocInternalId);
			if(openreplns!=null && openreplns!='' && openreplns.length>0)
			{
				//for(var i=0; i< openreplns.length;i++)
				//{
				//var taskpriority=openreplns[i].getValue('custrecord_taskpriority');
				var expqty=openreplns[0].getValue('custrecord_expe_qty',null,'sum');
				if(expqty == null || expqty == '')
				{
					expqty=0;
				}	
				nlapiLogExecution('DEBUG', 'expqty',expqty);
				nlapiLogExecution('DEBUG', 'getExpectedQuantity',getExpectedQuantity);
				if(parseFloat(expqty)>=parseFloat(getExpectedQuantity))
				{
					//nlapiSubmitField('customrecord_ebiznet_trn_opentask', openreplns[0].getId(), 'custrecord_taskpriority', '0');
					//nlapiSubmitField('customrecord_ebiznet_trn_opentask', getRecordInternalId, 'custrecord_skiptask', 'Y');	
					var SOarray=new Array();
					SOarray=buildTaskArray(getWaveno,vClusterNo,name,getOrderNo,null);
					//SOarray["custparam_skipid"] = vSkipId;
					if(NextLocation!=null && NextLocation!='')
						SOarray["custparam_screenno"] = 'CL4EXP';
					else
						//SOarray["custparam_screenno"] = 'CL4LOCEXP';
						SOarray["custparam_screenno"] = 'CL4EXP';
					SOarray["custparam_error"]=st1;//'CONFIRM THE OPEN REPLENS';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;
				}
				else
				{
					nlapiLogExecution('DEBUG', 'Into Else');
					var replengen = generateReplenishment(getEndLocInternalId,getItemInternalId,whLocation,getWaveno);
					if(replengen==true)
					{
						nlapiSubmitField('customrecord_ebiznet_trn_opentask', getRecordInternalId, 'custrecord_skiptask', 'Y');
						var SOarray=new Array();
						SOarray=buildTaskArray(getWaveno,vClusterNo,name,getOrderNo,null);
						//SOarray["custparam_skipid"] = vSkipId;
						//SOarray["custparam_skipid"] = vSkipId;
						if(NextLocation!=null && NextLocation!='')
							SOarray["custparam_screenno"] = 'CL4EXP';
						else
//							SOarray["custparam_screenno"] = 'CL4LOCEXP';
							SOarray["custparam_screenno"] = 'CL4EXP';

						SOarray["custparam_error"]=st1;//'CONFIRM THE OPEN REPLENS';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
					}
				}	
				//}
			}
			else
			{
				var replengen = generateReplenishment(getEndLocInternalId,getItemInternalId,whLocation,getWaveno);
				if(replengen==true)
				{
					nlapiSubmitField('customrecord_ebiznet_trn_opentask', getRecordInternalId, 'custrecord_skiptask', 'Y');
					var SOarray=new Array();
					SOarray=buildTaskArray(getWaveno,vClusterNo,name,getOrderNo,null);
					//SOarray["custparam_skipid"] = vSkipId;
					//SOarray["custparam_skipid"] = vSkipId;
					if(NextLocation!=null && NextLocation!='')
						SOarray["custparam_screenno"] = 'CL4EXP';
					else
//						SOarray["custparam_screenno"] = 'CL4LOCEXP';
						SOarray["custparam_screenno"] = 'CL4EXP';
					SOarray["custparam_error"]=st1;//'CONFIRM THE OPEN REPLENS';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;
				}
				else
				{
					nlapiSubmitField('customrecord_ebiznet_trn_opentask', getRecordInternalId, 'custrecord_skiptask', 'Y');
					var SOarray=new Array();
					SOarray=buildTaskArray(getWaveno,vClusterNo,name,getOrderNo,null);
					//SOarray["custparam_skipid"] = vSkipId;
					SOarray["custparam_screenno"] = 'CL5Qtyexp';					 
					//SOarray["custparam_error"]=st1;//'CONFIRM THE OPEN REPLENS';
					SOarray["custparam_error"]=st2;//'Insufficient inventory in Bulk location';
					ExceptionFlag='Y';
					SOarray["custparam_Exceptionflag"]=ExceptionFlag;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;
				}

			}
		}
		//


		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no');  
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";

		//html = html + " document.getElementById('entercontainerno').focus();";    

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 +"<label>" + getExpectedQuantity + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st4 +"<label>" + getItem + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st5 +"<label>" + getEnteredContainerNo + "</label>";
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";
		html = html + "				<input type='hidden' name='hdnFetchedContainerLPNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";		
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnRemainQuantity' value=" + remainqty + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEnteredLocation' value=" + getEnteredLocation + ">";
		html = html + "				<input type='hidden' name='hdnBeginBinLocation' value=" + getBeginBinLocation + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnserialno' value=" + getSerialNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vClusterNo + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value='" + vBatchno + "'>";	//Case# 201411153	
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnNextLocation' value=" + NextLocation + ">";
		html = html + "				<input type='hidden' name='hdnNextItem' value=" + NextItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnNextItemname' value=" + NextItem + ">";
		html = html + "				<input type='hidden' name='hdnName' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + getLPContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnebizOrdNo' value=" + getOrderNo + ">";
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		html = html + "				<input type='hidden' name='hdnReason' value=" + getReason + ">";
		html = html + "				<input type='hidden' name='hdnActQty' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnlocreccount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnEnteredContainerNo' value=" + getEnteredContainerNo + ">";
		html = html + "				<input type='hidden' name='hdndetailTask' value=" + detailTask + ">";
		html = html + "				<input type='hidden' name='hdnserialscanned' value=" + serialscanned + ">";

		//code added on 15Feb 2012
		html = html + "				<input type='hidden' name='hdnEntLoc' value=" + EntLoc + ">";
		html = html + "				<input type='hidden' name='hdnEntLocRec' value=" + EntLocRec + ">";
		html = html + "				<input type='hidden' name='hdnEntLocID' value=" + EntLocID + ">";
		html = html + "				<input type='hidden' name='hdnExceptionFlag' value=" + ExceptionFlag + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				<input type='hidden' name='hdnEntLotID' value=" + EntLotID + ">";
		html = html + "				<input type='hidden' name='hdntotalordqty' value=" + TotOrderqty + ">";
		//End of code.
		//html = html + "				<input type='hidden' name='hdnDisplayedQty' value=" + getDisplayedQty + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st6 +"";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterquantity' id='enterquantity' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st7 +"";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterreason' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		//Case # 20126818 Start
		/*
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st8 +"";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterremainqty' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";*/
		//Case # 20126818 End
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st9 +"<input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					"+ st10 +"<input name='cmdPrevious' type='submit' value='F7'/>";

		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterquantity').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var getLanguage = request.getParameter('hdngetLanguage');

		var st11,st12,st13;

		if( getLanguage == 'es_ES')
		{

			st11 = "CAJA NO V&#193;LIDO #";
			st12 = "RECOGER EN M&#218;LTIPLOS DE 2 YA QUE ESTO ES PARTE DE LA SELECCI&#211;N DEL ART&#205;CULO KIT PAQUETE";
			st13 = "CANTIDADES RESTANTES ACTUAL Y NO DEBE SER MAYOR QUE ESPERA CANT";
			st14 = "CANTIDAD REAL NO DEBE ESTAR EN BLANCO"; // Case# 20149652
			st15 = "CANT ACTUAL NO V&#193;LIDO #";

		}
		else
		{

			st11 = "INVALID CARTON #";
			//st12 = "PICK IN MULTIPLES OF 2 AS THIS PICK IS PART OF KIT PACKAGE ITEM";
			st12 = "QUANTITY EXCEPTION IS NOT ALLOWED FOR KIT ITEMS";
			st13 = "REMAINING AND ACTUAL QUANTITIES SHOULD NOT BE GREATER THAN EXPECTED QTY";
			st14 = "ACTUAL QTY SHOULD NOT BE BLANK";// Case# 20149652
			st15 = "INVALID ACTUAL QTY";
		}

		nlapiLogExecution('DEBUG', 'BeginLocationName', request.getParameter('hdnBeginBinLocation'));
		nlapiLogExecution('DEBUG', 'EnteredLocationName', request.getParameter('hdnEnteredLocation'));
		nlapiLogExecution('DEBUG', 'NextShowLocation', request.getParameter('hdnNextLocation'));
		nlapiLogExecution('DEBUG', 'ItemNo', request.getParameter('hdnItemInternalId'));
		nlapiLogExecution('DEBUG', 'NextShowItem', request.getParameter('hdnNextItem'));
		nlapiLogExecution('DEBUG', 'NextShowItemname', request.getParameter('hdnNextItemname'));

		var getEnteredQuantity = request.getParameter('enterquantity');
		nlapiLogExecution('DEBUG', 'getEnteredQuantity', getEnteredQuantity);
		var detailTask=request.getParameter('hdndetailTask');
		nlapiLogExecution('DEBUG', 'detailTask', detailTask);

		var getEnteredReason = request.getParameter('enterreason');
		nlapiLogExecution('DEBUG', 'getEnteredReason', getEnteredReason);

		//Case # 20126818 Start
		var getRemQuantity = 0;//request.getParameter('enterremainqty');
		//Case # 20126818 End
		nlapiLogExecution('DEBUG', 'getRemQuantity', getRemQuantity);
		var serialscanned = request.getParameter('hdnserialscanned');

		var vRemaningqty=0,vDisplayedQty=0,vActqty=0;
		var TotalWeight=0;
		var vCarrier;

		var getEnteredContainerNo = request.getParameter('hdnEnteredContainerNo');
		nlapiLogExecution('DEBUG', 'getEnteredContainerNo', getEnteredContainerNo);

		if(getEnteredContainerNo==null || getEnteredContainerNo=='')
			getEnteredContainerNo = request.getParameter('hdnFetchedContainerLPNo');

		var locreccount=request.getParameter('hdnlocreccount');
		vActqty = getEnteredQuantity;
		var vReason = getEnteredReason;
		var vSite = request.getParameter('hdnwhlocation');
		var vPickType = request.getParameter('hdnpicktype');
		var stgDirection="OUB";
		var vDoname=request.getParameter('hdnName');
		var vdono=request.getParameter('hdnDOLineId');
		var vPrevinvrefno=request.getParameter('hdnInvoiceRefNo');
		var SalesOrderInternalId=request.getParameter('hdnebizOrdNo');
		var getFetchedContainerNo = request.getParameter('hdnFetchedContainerLPNo');
		var getRecordInternalId = request.getParameter('hdnRecordInternalId');
		var getContainerSize=request.getParameter('hdnContainerSize');		
		var getWaveNo = request.getParameter('hdnWaveNo');
		var RecordInternalId = request.getParameter('hdnRecordInternalId');
		var ContainerLPNo = request.getParameter('hdnContainerLpNo');
		var BeginLocation = request.getParameter('hdnBeginLocationId');
		var vbatchno = request.getParameter('hdnbatchno');		
		var ItemNo = request.getParameter('hdnItemInternalId');
		var ItemDesc = request.getParameter('hdnItemDescription');
		var Item = request.getParameter('hdnItem');
		var PickQty = request.getParameter('hdnExpectedQuantity');
		var RcId = request.getParameter('hdnRecordInternalId');
		var EndLocation = request.getParameter('hdnEndLocInternalId');
		var RecCount = request.getParameter('hdnRecCount');
		var NextShowLocation=request.getParameter('hdnNextLocation');
		var NextShowItem=request.getParameter('hdnNextItem');
		var NextShowItemname=request.getParameter('hdnNextItemname');
		var OrdName=request.getParameter('hdnName');
		var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var BeginLocationName = request.getParameter('hdnBeginBinLocation');
		var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
		var OrderLineNo = request.getParameter('hdnOrderLineNo');
		var SerialNo = request.getParameter('hdnserialno');
		var vClusterNo = request.getParameter('hdnClusterNo');
		var vBatchno = request.getParameter('hdnbatchno');	
		var remqtybycontlp=request.getParameter('hdnRemainQuantity');
		
		var TotalOrderqty = request.getParameter('hdntotalordqty');	

		//code added on 15Feb by suman
		var EntLoc = request.getParameter('hdnEntLoc');
		var EntLocRec = request.getParameter('hdnEntLocRec');
		var EntLocID = request.getParameter('hdnEntLocID');
		var ExceptionFlag = request.getParameter('hdnExceptionFlag');
		var EntLotID = request.getParameter('hdnEntLotID');
		if(ExceptionFlag=='L')
		{
			if(EntLocID!=null&&EntLocID!="")
			{
				EndLocation=EntLocID;
				vbatchno = EntLotID;
			}
		}
		//End of code.

		//ItemFulfillment(getWaveNo, RecordInternalId, ContainerLPNo, PickQty, BeginLocation, Item, ItemDesc, ItemNo, vdono, InvoiceRefNo, BeginLocationName, EndLocInternalId, EndLocation, OrderLineNo, SerialNo,vBatchno);
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		// This variable is to hold the SO# entered.
		var SOarray = new Array();
		SOarray["custparam_language"] = getLanguage;
		SOarray["custparam_error"] = st11;//'INVALID CARTON #';
		SOarray["custparam_screenno"] = 'CL5';

		SOarray["custparam_nooflocrecords"] = locreccount;
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		SOarray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
		SOarray["custparam_ebizordno"] = request.getParameter('hdnebizOrdNo');	
		SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
		SOarray["custparam_recordinternalid"] = request.getParameter('hdnRecordInternalId');
		SOarray["custparam_containerlpno"] = request.getParameter('hdnContainerLpNo');
		SOarray["custparam_expectedquantity"] = request.getParameter('hdnExpectedQuantity');
		SOarray["custparam_beginLocation"] = request.getParameter('hdnBeginLocationId');
		SOarray["custparam_item"] = request.getParameter('hdnItem');
		SOarray["custparam_itemdescription"] = request.getParameter('hdnItemDescription');
		SOarray["custparam_iteminternalid"] = request.getParameter('hdnItemInternalId');
		SOarray["custparam_dolineid"] = request.getParameter('hdnDOLineId');
		SOarray["custparam_invoicerefno"] = request.getParameter('hdnInvoiceRefNo');
		SOarray["custparam_beginlocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
		SOarray["custparam_orderlineno"] = request.getParameter('hdnOrderLineNo');
		SOarray["custparam_serialno"] = request.getParameter('hdnserialno');
		SOarray["custparam_clusterno"] = request.getParameter('hdnClusterNo');
		SOarray["custparam_detailtask"] = request.getParameter('hdndetailTask');
		SOarray["custparam_serialscanned"] = request.getParameter('hdnserialscanned');
		SOarray["custparam_batchno"] = vbatchno;
		SOarray["custparam_nextlocation"] = NextShowLocation;
		SOarray["custparam_nextiteminternalid"] = NextShowItem;
		SOarray["custparam_nextitem"] = NextShowItemname;
		SOarray["custparam_newcontainerlp"] = getEnteredContainerNo;
		SOarray["custparam_remainqty"] = remqtybycontlp;
		SOarray["custparam_TotOrderqty"] = TotalOrderqty;
		
		SOarray["name"] = OrdName;
		SOarray["custparam_EntLoc"] = EntLoc;
		SOarray["custparam_EntLocRec"] = EntLocRec;
		SOarray["custparam_EntLocId"] = EntLocID;
		SOarray["custparam_Exc"] = ExceptionFlag;
		SOarray["custparam_entLotId"] = EntLotID; 
		nlapiLogExecution('DEBUG', 'EntLoc', EntLoc);
		nlapiLogExecution('DEBUG', 'EntLocID', EntLocID);
		nlapiLogExecution('DEBUG', 'vbatchno', vbatchno);



		try
		{

			var itemattribute='';
			if(ItemNo!=null && ItemNo!='')
			{
				var itemSubtype = nlapiLookupField('item', ItemNo, ['recordType', 'custitem_ebiz_captureattribute']);
				itemattribute = itemSubtype.custitem_ebiz_captureattribute;
			}
			nlapiLogExecution('DEBUG', 'itemattribute', itemattribute);



			nlapiLogExecution('DEBUG', 'Kitting ItemNo', ItemNo);
			nlapiLogExecution('DEBUG', 'Kitting SalesOrderInternalId', SalesOrderInternalId);

			var vkititem = '';
			var vkititemtext = '';
			var kititemTypesku = '';

			var filters = new Array(); 			 
			filters[0] = new nlobjSearchFilter('custrecord_sku', null, 'is', ItemNo);	
			filters[1] = new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', SalesOrderInternalId);	

			var columns1 = new Array(); 
			columns1[0] = new nlobjSearchColumn('custrecord_parent_sku_no' ); 

			var searchresultskititem = nlapiSearchRecord( 'customrecord_ebiznet_trn_opentask', null, filters, columns1 ); 
			if(searchresultskititem!=null && searchresultskititem!='')
			{
				vkititem = searchresultskititem[0].getValue('custrecord_parent_sku_no');
				vkititemtext = searchresultskititem[0].getText('custrecord_parent_sku_no');
			}

			if(vkititem!=null && vkititem!='')
				kititemTypesku = nlapiLookupField('item', vkititem, 'recordType');

			nlapiLogExecution('DEBUG', 'kititemTypesku', kititemTypesku);

			if(kititemTypesku=='kititem')
			{
				var searchresultsitem = nlapiLoadRecord(kititemTypesku, vkititem);
				var SkuNo=searchresultsitem.getFieldValue('itemid');

				var kitfilters = new Array(); 			 
				kitfilters[0] = new nlobjSearchFilter('itemid', null, 'is', SkuNo);	

				var kitcolumns1 = new Array(); 
				kitcolumns1[0] = new nlobjSearchColumn( 'memberitem' ); 			
				kitcolumns1[1] = new nlobjSearchColumn( 'memberquantity' );

				var searchresults = nlapiSearchRecord( 'item', null, kitfilters, kitcolumns1 ); 
				if(searchresults!=null && searchresults!='')
				{

					for(var w=0; w<searchresults.length;w++) 
					{
						fulfilmentItem = searchresults[w].getValue('memberitem');
						memberitemqty = searchresults[w].getValue('memberquantity');

						if(fulfilmentItem==ItemNo)
						{
							var kitqty=Math.floor(getEnteredQuantity/memberitemqty)*memberitemqty;

							//if((getEnteredQuantity!=kitqty) && (parseFloat(getEnteredQuantity) < parseFloat(FetchedQuantity)))
							if(parseFloat(getEnteredQuantity) < parseFloat(PickQty))
							{	
								//pickQtyExceptionarray["custparam_screenno"] = 'CL5';		
								SOarray["custparam_screenno"] = 'CL4'
								SOarray["custparam_error"] = st12;//'Pick in multiples of 2 as this pick is part of Kit Package item';
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								nlapiLogExecution('DEBUG', 'Error: ', 'Pick in multiples of 2 as this pick is part of Kit Package item');
								return;

							}

						}
					}
				}	

			}
		}
		catch(exp)
		{
			nlapiLogExecution('DEBUG', 'Exception in kitting item: ', exp);
		}

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept SO #.


		if (sessionobj!=context.getUser()) 
		{
			try
			{

				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}
				if (optedEvent == 'F7') 
				{
					response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_dettask', 'customdeploy_rf_cluspicking_dettask', false, SOarray);
				}
				else
				{
					nlapiLogExecution('DEBUG', 'getEnteredContainerNo', getEnteredContainerNo);
					nlapiLogExecution('DEBUG', 'getFetchedContainerNo', getFetchedContainerNo);
					nlapiLogExecution('DEBUG', 'getWaveNo', getWaveNo);
					nlapiLogExecution('DEBUG', 'vClusterNo', vClusterNo);
					nlapiLogExecution('DEBUG', 'PickQty', PickQty);
					nlapiLogExecution('DEBUG', 'getRemQuantity', getRemQuantity);
					nlapiLogExecution('DEBUG', 'getEnteredQuantity', getEnteredQuantity);
					nlapiLogExecution('DEBUG', 'vbatchno', vbatchno);
					// Case# 20149652 starts
					//if(getEnteredQuantity==null || getEnteredQuantity=='')
					if(getEnteredQuantity==null || getEnteredQuantity=='' || getEnteredQuantity==' ' || isNaN(getEnteredQuantity))
					{
						//SOarray["custparam_error"] = st14;
						SOarray["custparam_error"] = st15;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						nlapiLogExecution('DEBUG', 'Error: ', 'invalid actual quantity');
						return;
					}
					if(getEnteredQuantity!=null && getEnteredQuantity!='' && getEnteredQuantity<0)
					{
						SOarray["custparam_error"] = 'Actual Qty Should Be Greater Than Or Equal To Zero';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						nlapiLogExecution('DEBUG', 'Error: ', 'Actual Qty Should Be Greater Than Or Equal To Zero');
						return;
					}
					//Case# 20149652 ends
					if(getEnteredQuantity==null || getEnteredQuantity=='' || getEnteredQuantity==' ' || isNaN(getEnteredQuantity))
						getEnteredQuantity=0;

					if(getRemQuantity==null || getRemQuantity=='' || getRemQuantity==' ' || isNaN(getRemQuantity))
						getRemQuantity=0;

					if(parseFloat(PickQty)< (parseFloat(getEnteredQuantity)+parseFloat(getRemQuantity)))
					{
						SOarray["custparam_screenno"] = 'CL5';			
						SOarray["custparam_error"] = st13;//'Remaining and Actual Quantities should not be greater than Expected Qty';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
					}
					nlapiLogExecution('DEBUG', 'detailTask', detailTask);
					nlapiLogExecution('DEBUG', 'serialscanned', serialscanned);
					SOarray["custparam_detailtask"] = detailTask;
					ExceptionFlag='Y';
					SOarray["custparam_Exc"] = ExceptionFlag;// case# 201416462
					var itemSubtype = nlapiLookupField('item', ItemNo, ['recordType', 'custitem_ebizserialin','custitem_ebizserialout', 'custitem_ebizbatchlot']);

					nlapiLogExecution('DEBUG', 'itemSubtype', itemSubtype);
					//case# 20127586 starts (when we give qty '0' it asking serial numbers issue)
					if((itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.custitem_ebizserialout == 'T' ) && detailTask !='' && detailTask !='F' && 
							(serialscanned == null || serialscanned == '')&& (parseFloat(getEnteredQuantity)+parseFloat(getRemQuantity))>0)
					{
						//case# 20127586 end
						if (itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.custitem_ebizserialout == 'T') 
						{
							SOarray["custparam_number"] = 0;
							SOarray["custparam_RecType"] = itemSubtype.recordType;
							SOarray["custparam_SerOut"] = itemSubtype.custitem_ebizserialout;
							SOarray["custparam_SerIn"] = itemSubtype.custitem_ebizserialin;
							SOarray["custparam_expectedquantity"] = parseInt(getEnteredQuantity);
							response.sendRedirect('SUITELET', 'customscript_rf_pick_serialscan', 'customdeploy_rf_pick_serialscan_di', false, SOarray);
							return;
						}
					}
					else
					{
						if (getEnteredContainerNo==''||getEnteredContainerNo==null||getEnteredContainerNo == getFetchedContainerNo)
						{
							nlapiLogExecution('DEBUG', 'getEnteredContainerNo = getFetchedContainerNo');
							nlapiLogExecution('DEBUG', 'getItem', ItemNo);
							nlapiLogExecution('DEBUG', 'remqtybycontlp', remqtybycontlp);
							nlapiLogExecution('DEBUG', 'locreccount', locreccount);
							nlapiLogExecution('DEBUG', 'BeginLocationName', BeginLocationName);
							nlapiLogExecution('DEBUG', 'NextShowLocation', NextShowLocation);
							nlapiLogExecution('DEBUG', 'ItemNo', ItemNo);
							nlapiLogExecution('DEBUG', 'NextShowItem', NextShowItem);
							nlapiLogExecution('DEBUG', 'NextShowItemname', NextShowItemname);
							nlapiLogExecution('DEBUG', 'EntLocRec', EntLocRec);
							//var Systemrules = SystemRuleForStockAdjustment();//Case# 201410405
							var Systemrules = SystemRuleForStockAdjustment(vSite);
							nlapiLogExecution('ERROR', 'Systemrules', Systemrules);
							if(Systemrules == null || Systemrules =='' || Systemrules.length==0)
								Systemrules=null;

							ConfirmPickTasks(vClusterNo,getFetchedContainerNo,ItemNo,BeginLocation,EndLocation,vReason,vActqty,
									kititemTypesku,SalesOrderInternalId,vkititem,getRemQuantity,getEnteredContainerNo,vbatchno,ExceptionFlag,EntLocRec,SalesOrderInternalId,Systemrules,request);

							if(parseFloat(remqtybycontlp)>0)
							{
								if(itemattribute == 'T')
								{
									nlapiLogExecution('ERROR', 'Navigating To11', 'Item Attribute1');
									SOarray["custparam_expectedquantity"] = parseFloat(remqtybycontlp);														
									SOarray["custparam_nooflocrecords"] = parseInt(locreccount)-1;
									SOarray["custparam_redirectscreen"] = "DETTASK";
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
								}
								else
								{					

									SOarray["custparam_expectedquantity"] = parseFloat(remqtybycontlp);
									nlapiLogExecution('DEBUG', 'Navigating To', 'Picking Location');
									response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_dettask', 'customdeploy_rf_cluspicking_dettask', false, SOarray);
								}
							}

							else if(parseFloat(locreccount) > 1)
							{					
								if(BeginLocationName != NextShowLocation)
								{ 
									var SOFilters = new Array();

									SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
									SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK

									if(getWaveno != null && getWaveno != "")
									{
										nlapiLogExecution('DEBUG', 'WaveNo inside If', getWaveno);

										SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(getWaveno)));
									}

									if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
									{
										nlapiLogExecution('ERROR', 'ClusNo inside If', vClusterNo);

										SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
									}

									var SOColumns = new Array();

									//SOColumns[0] = new nlobjSearchColumn('custrecord_skiptask',null,'group');
									SOColumns[0] = new nlobjSearchColumn('formulanumeric',null,'group').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)");
									SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
									SOColumns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
									SOColumns[3] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');		
									SOColumns[4] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');		
									SOColumns[5] = new nlobjSearchColumn('custrecord_skudesc',null,'group');
									SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
									SOColumns[7] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
									SOColumns[8] = new nlobjSearchColumn('custrecord_comp_id',null,'group');
									SOColumns[9] = new nlobjSearchColumn('custrecord_ebiz_wave_no',null,'group');
									SOColumns[10] = new nlobjSearchColumn('custrecord_ebiz_clus_no',null,'group');
									SOColumns[11] = new nlobjSearchColumn('internalid',null,'group');
									SOColumns[12] = new nlobjSearchColumn('custrecord_batch_no',null,'group');

									SOColumns[0].setSort(); 	//Location Pick Sequence
									SOColumns[1].setSort();		//SKU
									SOColumns[2].setSort();		//Location

									var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
									if (SOSearchResults != null && SOSearchResults.length > 0) {

										var SOSearchResult = SOSearchResults[0];
										var beginlocname = SOSearchResult.getText('custrecord_actbeginloc',null,'group');
										var itemintrid = SOSearchResult.getValue('custrecord_ebiz_sku_no',null,'group');

										nlapiLogExecution('ERROR', 'SOSearchResults length 11', SOSearchResults.length);
										//nlapiLogExecution('ERROR', 'SOSearchResults getid 11', SOSearchResults[0].getId());
										if(SOSearchResults.length > 1)
										{
											var SOSearchnextResult = SOSearchResults[1];
											//SOarray["custparam_recordinternalid"] = SOSearchnextResult.getId();
											SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc',null,'group');
											SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no',null,'group');
											SOarray["custparam_nextitem"] = SOSearchnextResult.getText('custrecord_sku',null,'group');									
										}
										if(itemattribute == 'T')
										{
											nlapiLogExecution('ERROR', 'Navigating To3', 'Item Attribute3');
											nlapiLogExecution('ERROR', 'custparam_recordinternalid To3', SOarray["custparam_recordinternalid"]);
											SOarray["custparam_beginlocationname"]= beginlocname;
											SOarray["custparam_redirectscreen"] = "LOC";
											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
										}
										else
										{
											SOarray["custparam_beginlocationname"]= beginlocname;
											var vOTTaskId=SOSearchResult.getValue('internalid',null,'max');
											nlapiLogExecution('ERROR', 'OT Int Id', vOTTaskId);
											if(vOTTaskId != null && vOTTaskId != '')
											{
												var vOTRec=nlapiLoadRecord('customrecord_ebiznet_trn_opentask',vOTTaskId);
												SOarray["custparam_ebizordno"]=vOTRec.getFieldValue('custrecord_ebiz_order_no');
											}	




											nlapiLogExecution('ERROR', 'Navigating To', 'Picking Location');
											response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_location', 'customdeploy_rf_cluspicking_location', false, SOarray);
										}
									}
								}
								else if((BeginLocationName == NextShowLocation) && ItemNo != NextShowItem)
								{
									//added for sending order no also to item screen

									var SOFilters = new Array();

									SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
									SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK

									if(getWaveno != null && getWaveno != "")
									{
										nlapiLogExecution('DEBUG', 'WaveNo inside If', getWaveno);

										SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(getWaveno)));
									}

									if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
									{
										nlapiLogExecution('ERROR', 'ClusNo inside If', vClusterNo);

										SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
									}

									var SOColumns = new Array();

									//SOColumns[0] = new nlobjSearchColumn('custrecord_skiptask',null,'group');
									SOColumns[0] = new nlobjSearchColumn('formulanumeric',null,'group').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)");
									SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
									SOColumns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
									SOColumns[3] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');		
									SOColumns[4] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');		
									SOColumns[5] = new nlobjSearchColumn('custrecord_skudesc',null,'group');
									SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
									SOColumns[7] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
									SOColumns[8] = new nlobjSearchColumn('custrecord_comp_id',null,'group');
									SOColumns[9] = new nlobjSearchColumn('custrecord_ebiz_wave_no',null,'group');
									SOColumns[10] = new nlobjSearchColumn('custrecord_ebiz_clus_no',null,'group');
									SOColumns[11] = new nlobjSearchColumn('internalid',null,'group');
									SOColumns[12] = new nlobjSearchColumn('custrecord_batch_no',null,'group');

									SOColumns[0].setSort(); 	//Location Pick Sequence
									SOColumns[1].setSort();		//SKU
									SOColumns[2].setSort();		//Location

									var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
									if (SOSearchResults != null && SOSearchResults.length > 0) {

										var SOSearchResult = SOSearchResults[0];
										var beginlocname = SOSearchResult.getText('custrecord_actbeginloc',null,'group');
										var itemintrid = SOSearchResult.getValue('custrecord_ebiz_sku_no',null,'group');

										nlapiLogExecution('ERROR', 'SOSearchResults length 11111', SOSearchResults.length);
										//nlapiLogExecution('ERROR', 'SOSearchResults getid 11', SOSearchResults[0].getId());
										if(SOSearchResults.length > 1)
										{
											var SOSearchnextResult = SOSearchResults[1];
											//SOarray["custparam_recordinternalid"] = SOSearchnextResult.getId();
											SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc',null,'group');
											SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no',null,'group');
											SOarray["custparam_nextitem"] = SOSearchnextResult.getText('custrecord_sku',null,'group');									
										}

										if(itemattribute == 'T')
										{
											nlapiLogExecution('DEBUG', 'Navigating To2', 'Item Attribute2');
											SOarray["custparam_item"] = NextShowItem;
											SOarray["custparam_iteminternalid"] = NextShowItem;
											SOarray["custparam_nooflocrecords"] = parseFloat(locreccount)-1;
											SOarray["custparam_redirectscreen"] = "ITEM";
											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
										}
										else
										{
											//SOarray["custparam_item"] = NextShowItemname;
											SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_ebiz_sku_no',null,'group');
											SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no',null,'group');
											SOarray["custparam_nooflocrecords"] = parseFloat(locreccount)-1;

											var vOTTaskId=SOSearchResult.getValue('internalid',null,'group');
											nlapiLogExecution('ERROR', 'OT Int Id', vOTTaskId);
											if(vOTTaskId != null && vOTTaskId != '')
											{
												var vOTRec=nlapiLoadRecord('customrecord_ebiznet_trn_opentask',vOTTaskId);
												SOarray["custparam_ebizordno"]=vOTRec.getFieldValue('custrecord_ebiz_order_no');
											}
											nlapiLogExecution('DEBUG', 'Navigating To 123', 'Picking Item');
											response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_item', 'customdeploy_rf_cluspicking_item', false, SOarray);
										}
									}
								}						
								else if((BeginLocationName == NextShowLocation) && ItemNo == NextShowItem)
								{ 
									var SOFilters = new Array();

									SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
									SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK

									if(getWaveno != null && getWaveno != "")
									{
										nlapiLogExecution('DEBUG', 'WaveNo inside If', getWaveno);

										SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(getWaveno)));
									}

									if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
									{
										nlapiLogExecution('ERROR', 'ClusNo inside If', vClusterNo);

										SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
									}

									var SOColumns = new Array();

									//SOColumns[0] = new nlobjSearchColumn('custrecord_skiptask',null,'group');
									SOColumns[0] = new nlobjSearchColumn('formulanumeric',null,'group').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)");
									SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
									SOColumns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
									SOColumns[3] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');		
									SOColumns[4] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');		
									SOColumns[5] = new nlobjSearchColumn('custrecord_skudesc',null,'group');
									SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
									SOColumns[7] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
									SOColumns[8] = new nlobjSearchColumn('custrecord_comp_id',null,'group');
									SOColumns[9] = new nlobjSearchColumn('custrecord_ebiz_wave_no',null,'group');
									SOColumns[10] = new nlobjSearchColumn('custrecord_ebiz_clus_no',null,'group');
									SOColumns[11] = new nlobjSearchColumn('internalid',null,'group');
									SOColumns[12] = new nlobjSearchColumn('custrecord_batch_no',null,'group');

									SOColumns[0].setSort(); 	//Location Pick Sequence
									SOColumns[1].setSort();		//SKU
									SOColumns[2].setSort();		//Location

									var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
									if (SOSearchResults != null && SOSearchResults.length > 0) {

										var SOSearchResult = SOSearchResults[0];
										var beginlocname = SOSearchResult.getText('custrecord_actbeginloc',null,'group');
										var itemintrid = SOSearchResult.getValue('custrecord_ebiz_sku_no',null,'group');

										nlapiLogExecution('ERROR', 'SOSearchResults length 112', SOSearchResults.length);
										//nlapiLogExecution('ERROR', 'SOSearchResults getid 11', SOSearchResults[0].getId());
										if(SOSearchResults.length > 1)
										{
											var SOSearchnextResult = SOSearchResults[1];
											//SOarray["custparam_recordinternalid"] = SOSearchnextResult.getId();
											SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc',null,'group');
											SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no',null,'group');
											SOarray["custparam_nextitem"] = SOSearchnextResult.getText('custrecord_sku',null,'group');									
										}

										if(BeginLocationName != beginlocname)
										{ 
											if(itemattribute == 'T')
											{
												nlapiLogExecution('ERROR', 'Navigating To3', 'Item Attribute3');
												nlapiLogExecution('ERROR', 'custparam_recordinternalid To3', SOarray["custparam_recordinternalid"]);
												SOarray["custparam_beginlocationname"]= beginlocname;
												SOarray["custparam_redirectscreen"] = "LOC";
												response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
											}
											else
											{
												SOarray["custparam_beginlocationname"]= beginlocname;
												var vOTTaskId=SOSearchResult.getValue('internalid',null,'group');
												nlapiLogExecution('ERROR', 'OT Int Id', vOTTaskId);
												if(vOTTaskId != null && vOTTaskId != '')
												{
													var vOTRec=nlapiLoadRecord('customrecord_ebiznet_trn_opentask',vOTTaskId);
													SOarray["custparam_ebizordno"]=vOTRec.getFieldValue('custrecord_ebiz_order_no');
												}	
												nlapiLogExecution('ERROR', 'Navigating To', 'Picking Location');
												response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_location', 'customdeploy_rf_cluspicking_location', false, SOarray);
											}
										}
										else if((BeginLocationName == beginlocname) && ItemNo != itemintrid)
										{ 
											if(itemattribute == 'T')
											{
												nlapiLogExecution('ERROR', 'Navigating To4', 'Item Attribute4');
												SOarray["custparam_item"] = itemintrid;
												SOarray["custparam_iteminternalid"] = itemintrid;
												SOarray["custparam_nooflocrecords"] = parseFloat(locreccount)-1;
												SOarray["custparam_redirectscreen"] = "ITEM";
												response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
											}
											else
											{
												SOarray["custparam_item"] = itemintrid;
												SOarray["custparam_iteminternalid"] = itemintrid;
												SOarray["custparam_nooflocrecords"] = parseFloat(locreccount)-1;

												nlapiLogExecution('ERROR', 'Navigating To', 'Picking Item');
												response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_item', 'customdeploy_rf_cluspicking_item', false, SOarray);
											}
										}
										else
										{
											nlapiLogExecution('ERROR', 'Same BinLocation and Same Item', 'Picking Location');
											if(itemattribute == 'T')
											{
												nlapiLogExecution('ERROR', 'Navigating To5', 'Item Attribute5');
												SOarray["custparam_item"] = itemintrid;
												SOarray["custparam_iteminternalid"] = itemintrid;
												SOarray["custparam_nooflocrecords"] = parseFloat(locreccount)-1;
												SOarray["custparam_redirectscreen"] = "ITEM";
												response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
											}
											else
											{
												SOarray["custparam_item"] = itemintrid;
												SOarray["custparam_iteminternalid"] = itemintrid;
												SOarray["custparam_nooflocrecords"] = parseFloat(locreccount)-1;

												nlapiLogExecution('ERROR', 'Navigating To Else', 'Picking Item');
												response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_item', 'customdeploy_rf_cluspicking_item', false, SOarray);
											}
										}

									}
									else{
										if(itemattribute == 'T')
										{
											nlapiLogExecution('DEBUG', 'Navigating To7', 'Item Attribute7');
											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
										}
										else
										{
											nlapiLogExecution('DEBUG', 'Navigating To', 'Stage Location');
											SOarray["custparam_picktype"] =  'CL';
											response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);
										}

									}
								}
							}
							else{

								if(itemattribute == 'T')
								{
									nlapiLogExecution('ERROR', 'Navigating To7', 'Item Attribute7');
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
								}
								else
								{
									nlapiLogExecution('ERROR', 'Navigating To', 'Stage Location');
									response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);
								}
							}
						}
						else if (getEnteredContainerNo != '' && getEnteredContainerNo.replace(/\s+$/,"") != getFetchedContainerNo)
						{
							nlapiLogExecution('ERROR', 'getEnteredContainerNo', 'userdefined1');
							var vOpenTaskRecs=fnValidateEnteredContLP(getEnteredContainerNo.replace(/\s+$/,""),OrdName);
							//if(vOpenTaskRecs != null && vOpenTaskRecs.length > 0)
							nlapiLogExecution('ERROR', 'vOpenTaskRecs', vOpenTaskRecs);
							if(vOpenTaskRecs!=null && vOpenTaskRecs!='')
							{
								nlapiLogExecution('ERROR', 'vOpenTaskRecs.length', vOpenTaskRecs);
								nlapiLogExecution('ERROR', 'test1', 'test1');					
								SOarray["custparam_error"] = st11;//'INVALID CARTON #';
								SOarray["custparam_screenno"] = 'CL4';					
								SOarray["custparam_expectedquantity"] = request.getParameter('hdnOldExpectedQuantity');

								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							}
							else
							{
								nlapiLogExecution('DEBUG', 'test3', 'test3');
								var getEnteredContainerNoPrefix = getEnteredContainerNo.substring(0, 3).toUpperCase();
								var LPReturnValue = ebiznet_LPRange_CL_withLPType(getEnteredContainerNo.replace(/\s+$/,""), '2','2',vSite);//'2'UserDefiend,'2'PICK
								if(LPReturnValue == true){
									nlapiLogExecution('DEBUG', 'getEnteredContainerNo != getFetchedContainerNo');
									nlapiLogExecution('DEBUG', 'getItem', ItemNo);
									nlapiLogExecution('DEBUG', 'remqtybycontlp', remqtybycontlp);
									nlapiLogExecution('DEBUG', 'locreccount', locreccount);
									nlapiLogExecution('DEBUG', 'BeginLocationName', BeginLocationName);
									nlapiLogExecution('DEBUG', 'NextShowLocation', NextShowLocation);
									nlapiLogExecution('DEBUG', 'ItemNo', ItemNo);
									nlapiLogExecution('DEBUG', 'NextShowItem', NextShowItem);
									nlapiLogExecution('DEBUG', 'NextShowItemname', NextShowItemname);



									//nlapiLogExecution('DEBUG', 'getItem', ItemNo);
									var arrDims = getSKUCubeAndWeightforconfirm(ItemNo, 1);
									var itemCube = 0;
									var itemWeight=0;						
									if (arrDims[0] != "" && (!isNaN(arrDims[1]))){
										itemCube = (parseFloat(PickQty) * parseFloat(arrDims[0]));
										itemWeight = (parseFloat(PickQty) * parseFloat(arrDims[1]));//	
									}
									var ContainerCube;					
									var containerInternalId;
									var ContainerSize;
									if(getContainerSize=="" || getContainerSize==null)
									{
										getContainerSize=ContainerSize;
										nlapiLogExecution('DEBUG', 'ContainerSize', ContainerSize);	
									}	


									var arrContainerDetails = getContainerCubeAndTarWeight("",getContainerSize);					
									if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) {

										ContainerCube =  parseFloat(arrContainerDetails[0]);						
										containerInternalId = arrContainerDetails[3];
										ContainerSize=arrContainerDetails[4];
										TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));
									} 
									nlapiLogExecution('DEBUG', 'ContainerSizeInternalId', containerInternalId);	

									CreateRFMasterLPRecord(getEnteredContainerNo,containerInternalId,ContainerCube,itemWeight);
									//var Systemrules = SystemRuleForStockAdjustment(); //Case# 201410405
									var Systemrules = SystemRuleForStockAdjustment(vSite); 
									nlapiLogExecution('ERROR', 'Systemrules', Systemrules);
									if(Systemrules == null || Systemrules =='' || Systemrules.length==0)
										Systemrules=null;

									ConfirmPickTasks(vClusterNo,getFetchedContainerNo,ItemNo,BeginLocation,EndLocation,vReason,vActqty,
											kititemTypesku,SalesOrderInternalId,vkititem,getRemQuantity,getEnteredContainerNo,vbatchno,ExceptionFlag,EntLocRec,SalesOrderInternalId,Systemrules,request);

									//added by suman as on 05/08/12.
									//update all the open picks with the new container LP#.
									UpdateAllContainerLp(getEnteredContainerNo,getFetchedContainerNo,SalesOrderInternalId,OrdName);
									//end of the code as of 05/08/12.


									if(parseFloat(remqtybycontlp)>0)
									{
										if(itemattribute == 'T')
										{
											nlapiLogExecution('ERROR', 'Navigating To12', 'Item Attribute1');
											SOarray["custparam_expectedquantity"] = parseFloat(remqtybycontlp);															
											SOarray["custparam_nooflocrecords"] = parseInt(locreccount)-1;
											SOarray["custparam_redirectscreen"] = "DETTASK";
											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
										}
										else
										{

											SOarray["custparam_expectedquantity"] = parseFloat(remqtybycontlp);
											nlapiLogExecution('DEBUG', 'Navigating To', 'Same Screen');
											response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_dettask', 'customdeploy_rf_cluspicking_dettask', false, SOarray);
										}
									}

									else if(parseFloat(locreccount) > 1)
									{					
										if(BeginLocationName != NextShowLocation)
										{ 
											var SOFilters = new Array();

											SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
											SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK

											if(getWaveno != null && getWaveno != "")
											{
												nlapiLogExecution('DEBUG', 'WaveNo inside If', getWaveno);

												SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(getWaveno)));
											}

											if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
											{
												nlapiLogExecution('ERROR', 'ClusNo inside If', vClusterNo);

												SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
											}

											var SOColumns = new Array();

											//SOColumns[0] = new nlobjSearchColumn('custrecord_skiptask',null,'group');
											SOColumns[0] = new nlobjSearchColumn('formulanumeric',null,'group').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)");
											SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
											SOColumns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
											SOColumns[3] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');		
											SOColumns[4] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');		
											SOColumns[5] = new nlobjSearchColumn('custrecord_skudesc',null,'group');
											SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
											SOColumns[7] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
											SOColumns[8] = new nlobjSearchColumn('custrecord_comp_id',null,'group');
											SOColumns[9] = new nlobjSearchColumn('custrecord_ebiz_wave_no',null,'group');
											SOColumns[10] = new nlobjSearchColumn('custrecord_ebiz_clus_no',null,'group');
											SOColumns[11] = new nlobjSearchColumn('internalid',null,'group');
											SOColumns[12] = new nlobjSearchColumn('custrecord_batch_no',null,'group');

											SOColumns[0].setSort(); 	//Location Pick Sequence
											SOColumns[1].setSort();		//SKU
											SOColumns[2].setSort();		//Location

											var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
											if (SOSearchResults != null && SOSearchResults.length > 0) {

												var SOSearchResult = SOSearchResults[0];
												var beginlocname = SOSearchResult.getText('custrecord_actbeginloc',null,'group');
												var itemintrid = SOSearchResult.getValue('custrecord_ebiz_sku_no',null,'group');

												nlapiLogExecution('ERROR', 'SOSearchResults length 11', SOSearchResults.length);
												//nlapiLogExecution('ERROR', 'SOSearchResults getid 11', SOSearchResults[0].getId());
												if(SOSearchResults.length > 1)
												{
													var SOSearchnextResult = SOSearchResults[1];
													//SOarray["custparam_recordinternalid"] = SOSearchnextResult.getId();
													SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc',null,'group');
													SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no',null,'group');
													SOarray["custparam_nextitem"] = SOSearchnextResult.getText('custrecord_sku',null,'group');									
												}
												if(itemattribute == 'T')
												{
													nlapiLogExecution('ERROR', 'Navigating To3', 'Item Attribute3');
													nlapiLogExecution('ERROR', 'custparam_recordinternalid To3', SOarray["custparam_recordinternalid"]);
													SOarray["custparam_beginlocationname"]= beginlocname;
													SOarray["custparam_redirectscreen"] = "LOC";
													response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
												}
												else
												{
													SOarray["custparam_beginlocationname"]= beginlocname;
													var vOTTaskId=SOSearchResult.getValue('internalid',null,'max');
													nlapiLogExecution('ERROR', 'OT Int Id', vOTTaskId);
													if(vOTTaskId != null && vOTTaskId != '')
													{
														var vOTRec=nlapiLoadRecord('customrecord_ebiznet_trn_opentask',vOTTaskId);
														SOarray["custparam_ebizordno"]=vOTRec.getFieldValue('custrecord_ebiz_order_no');
													}	




													nlapiLogExecution('ERROR', 'Navigating To', 'Picking Location');
													response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_location', 'customdeploy_rf_cluspicking_location', false, SOarray);
												}
											}
										}
										else if((BeginLocationName == NextShowLocation) && ItemNo != NextShowItem)
										{
											//added for sending order no also to item screen

											var SOFilters = new Array();

											SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
											SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK

											if(getWaveno != null && getWaveno != "")
											{
												nlapiLogExecution('DEBUG', 'WaveNo inside If', getWaveno);

												SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(getWaveno)));
											}

											if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
											{
												nlapiLogExecution('ERROR', 'ClusNo inside If', vClusterNo);

												SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
											}

											var SOColumns = new Array();

											//SOColumns[0] = new nlobjSearchColumn('custrecord_skiptask',null,'group');
											SOColumns[0] = new nlobjSearchColumn('formulanumeric',null,'group').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)");
											SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
											SOColumns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
											SOColumns[3] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');		
											SOColumns[4] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');		
											SOColumns[5] = new nlobjSearchColumn('custrecord_skudesc',null,'group');
											SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
											SOColumns[7] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
											SOColumns[8] = new nlobjSearchColumn('custrecord_comp_id',null,'group');
											SOColumns[9] = new nlobjSearchColumn('custrecord_ebiz_wave_no',null,'group');
											SOColumns[10] = new nlobjSearchColumn('custrecord_ebiz_clus_no',null,'group');
											SOColumns[11] = new nlobjSearchColumn('internalid',null,'group');
											SOColumns[12] = new nlobjSearchColumn('custrecord_batch_no',null,'group');

											SOColumns[0].setSort(); 	//Location Pick Sequence
											SOColumns[1].setSort();		//SKU
											SOColumns[2].setSort();		//Location

											var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
											if (SOSearchResults != null && SOSearchResults.length > 0) {

												var SOSearchResult = SOSearchResults[0];
												var beginlocname = SOSearchResult.getText('custrecord_actbeginloc',null,'group');
												var itemintrid = SOSearchResult.getValue('custrecord_ebiz_sku_no',null,'group');

												nlapiLogExecution('ERROR', 'SOSearchResults length 11111', SOSearchResults.length);
												//nlapiLogExecution('ERROR', 'SOSearchResults getid 11', SOSearchResults[0].getId());
												if(SOSearchResults.length > 1)
												{
													var SOSearchnextResult = SOSearchResults[1];
													//SOarray["custparam_recordinternalid"] = SOSearchnextResult.getId();
													SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc',null,'group');
													SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no',null,'group');
													SOarray["custparam_nextitem"] = SOSearchnextResult.getText('custrecord_sku',null,'group');									
												}

												if(itemattribute == 'T')
												{
													nlapiLogExecution('DEBUG', 'Navigating To2', 'Item Attribute2');
													SOarray["custparam_item"] = NextShowItem;
													SOarray["custparam_iteminternalid"] = NextShowItem;
													SOarray["custparam_nooflocrecords"] = parseFloat(locreccount)-1;
													SOarray["custparam_redirectscreen"] = "ITEM";
													response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
												}
												else
												{
													//SOarray["custparam_item"] = NextShowItemname;
													SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_ebiz_sku_no',null,'group');
													SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no',null,'group');
													SOarray["custparam_nooflocrecords"] = parseFloat(locreccount)-1;

													var vOTTaskId=SOSearchResult.getValue('internalid',null,'group');
													nlapiLogExecution('ERROR', 'OT Int Id', vOTTaskId);
													if(vOTTaskId != null && vOTTaskId != '')
													{
														var vOTRec=nlapiLoadRecord('customrecord_ebiznet_trn_opentask',vOTTaskId);
														SOarray["custparam_ebizordno"]=vOTRec.getFieldValue('custrecord_ebiz_order_no');
													}
													nlapiLogExecution('DEBUG', 'Navigating To 123', 'Picking Item');
													response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_item', 'customdeploy_rf_cluspicking_item', false, SOarray);
												}
											}
										}						
										else if((BeginLocationName == NextShowLocation) && ItemNo == NextShowItem)
										{ 
											var SOFilters = new Array();

											SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
											SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK

											if(getWaveno != null && getWaveno != "")
											{
												nlapiLogExecution('DEBUG', 'WaveNo inside If', getWaveno);

												SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(getWaveno)));
											}

											if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
											{
												nlapiLogExecution('ERROR', 'ClusNo inside If', vClusterNo);

												SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
											}

											var SOColumns = new Array();

											//SOColumns[0] = new nlobjSearchColumn('custrecord_skiptask',null,'group');
											SOColumns[0] = new nlobjSearchColumn('formulanumeric',null,'group').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)");
											SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
											SOColumns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
											SOColumns[3] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');		
											SOColumns[4] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');		
											SOColumns[5] = new nlobjSearchColumn('custrecord_skudesc',null,'group');
											SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');
											SOColumns[7] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
											SOColumns[8] = new nlobjSearchColumn('custrecord_comp_id',null,'group');
											SOColumns[9] = new nlobjSearchColumn('custrecord_ebiz_wave_no',null,'group');
											SOColumns[10] = new nlobjSearchColumn('custrecord_ebiz_clus_no',null,'group');
											SOColumns[11] = new nlobjSearchColumn('internalid',null,'group');
											SOColumns[12] = new nlobjSearchColumn('custrecord_batch_no',null,'group');

											SOColumns[0].setSort(); 	//Location Pick Sequence
											SOColumns[1].setSort();		//SKU
											SOColumns[2].setSort();		//Location

											var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
											if (SOSearchResults != null && SOSearchResults.length > 0) {

												var SOSearchResult = SOSearchResults[0];
												var beginlocname = SOSearchResult.getText('custrecord_actbeginloc',null,'group');
												var itemintrid = SOSearchResult.getValue('custrecord_ebiz_sku_no',null,'group');

												nlapiLogExecution('ERROR', 'SOSearchResults length 112', SOSearchResults.length);
												//nlapiLogExecution('ERROR', 'SOSearchResults getid 11', SOSearchResults[0].getId());
												if(SOSearchResults.length > 1)
												{
													var SOSearchnextResult = SOSearchResults[1];
													//SOarray["custparam_recordinternalid"] = SOSearchnextResult.getId();
													SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc',null,'group');
													SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no',null,'group');
													SOarray["custparam_nextitem"] = SOSearchnextResult.getText('custrecord_sku',null,'group');									
												}

												if(BeginLocationName != beginlocname)
												{ 
													if(itemattribute == 'T')
													{
														nlapiLogExecution('ERROR', 'Navigating To3', 'Item Attribute3');
														nlapiLogExecution('ERROR', 'custparam_recordinternalid To3', SOarray["custparam_recordinternalid"]);
														SOarray["custparam_beginlocationname"]= beginlocname;
														SOarray["custparam_redirectscreen"] = "LOC";
														response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
													}
													else
													{
														SOarray["custparam_beginlocationname"]= beginlocname;
														var vOTTaskId=SOSearchResult.getValue('internalid',null,'group');
														nlapiLogExecution('ERROR', 'OT Int Id', vOTTaskId);
														if(vOTTaskId != null && vOTTaskId != '')
														{
															var vOTRec=nlapiLoadRecord('customrecord_ebiznet_trn_opentask',vOTTaskId);
															SOarray["custparam_ebizordno"]=vOTRec.getFieldValue('custrecord_ebiz_order_no');
														}	
														nlapiLogExecution('ERROR', 'Navigating To', 'Picking Location');
														response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_location', 'customdeploy_rf_cluspicking_location', false, SOarray);
													}
												}
												else if((BeginLocationName == beginlocname) && ItemNo != itemintrid)
												{ 
													if(itemattribute == 'T')
													{
														nlapiLogExecution('ERROR', 'Navigating To4', 'Item Attribute4');
														SOarray["custparam_item"] = itemintrid;
														SOarray["custparam_iteminternalid"] = itemintrid;
														SOarray["custparam_nooflocrecords"] = parseFloat(locreccount)-1;
														SOarray["custparam_redirectscreen"] = "ITEM";
														response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
													}
													else
													{
														SOarray["custparam_item"] = itemintrid;
														SOarray["custparam_iteminternalid"] = itemintrid;
														SOarray["custparam_nooflocrecords"] = parseFloat(locreccount)-1;

														nlapiLogExecution('ERROR', 'Navigating To', 'Picking Item');
														response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_item', 'customdeploy_rf_cluspicking_item', false, SOarray);
													}
												}
												else
												{
													nlapiLogExecution('ERROR', 'Same BinLocation and Same Item', 'Picking Location');
													if(itemattribute == 'T')
													{
														nlapiLogExecution('ERROR', 'Navigating To5', 'Item Attribute5');
														SOarray["custparam_item"] = itemintrid;
														SOarray["custparam_iteminternalid"] = itemintrid;
														SOarray["custparam_nooflocrecords"] = parseFloat(locreccount)-1;
														SOarray["custparam_redirectscreen"] = "ITEM";
														response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
													}
													else
													{
														SOarray["custparam_item"] = itemintrid;
														SOarray["custparam_iteminternalid"] = itemintrid;
														SOarray["custparam_nooflocrecords"] = parseFloat(locreccount)-1;

														nlapiLogExecution('ERROR', 'Navigating To Else', 'Picking Item');
														response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_item', 'customdeploy_rf_cluspicking_item', false, SOarray);
													}
												}

											}
											else{

												if(itemattribute == 'T')
												{
													nlapiLogExecution('ERROR', 'Navigating To6', 'Item Attribute6');
													response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
												}
												else
												{
													nlapiLogExecution('ERROR', 'Navigating To', 'Stage Location');
													SOarray["custparam_picktype"] =  'CL';
													response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);
												}
											}
										}
									}
									else{

										if(itemattribute == 'T')
										{
											nlapiLogExecution('ERROR', 'Navigating To7', 'Item Attribute7');
											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
										}
										else
										{
											nlapiLogExecution('ERROR', 'Navigating To', 'Stage Location');
											response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);
										}
									}
								}
								else  
								{
									nlapiLogExecution('ERROR', 'Error: ', 'Container # is not in User Defined Range');

									SOarray["custparam_expectedquantity"] = request.getParameter('hdnOldExpectedQuantity');							
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								}
							}
							/*else //if (getEnteredContainerNo != '' && getEnteredContainerNo == getFetchedContainerNo) 
					{
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						nlapiLogExecution('DEBUG', 'Error: ', 'Wave # not found');
							}*/

						}	
					}
				}


			}
			catch (e)  {
				nlapiLogExecution('DEBUG', 'Exception',e);
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');

			}
		}
		else
		{
			SOarray["custparam_screenno"] = 'CL1';
			SOarray["custparam_error"] = 'CONTAINER# ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
		}
	}
}

function getOpenTasksCount(vdono,clusno,containerlpno)
{
	nlapiLogExecution('DEBUG', 'into getOpenTasksCount');
	nlapiLogExecution('DEBUG', 'cluster no',clusno);
	nlapiLogExecution('DEBUG', 'containerlp no',containerlpno);

	var openreccount=0;
	var SOFilters = new Array();
	var SOColumns = new Array();

	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK		
	SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', containerlpno));	
	SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', clusno));
	SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', vdono));

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	if(SOSearchResults!=null && SOSearchResults!=''&& SOSearchResults.length>0)
		openreccount=SOSearchResults.length;	

	return openreccount;

}

function ConfirmPickTasks(vClusterNo,getFetchedContainerNo,ItemNo,BeginLocation,EndLocation,Reason,ActQty,kititemTypesku,
		SalesOrderInternalId,vkititem,getRemQuantity,getEnteredContainerNo,vbatchno,ExceptionFlag,EntLocRec,SalesOrderInternalId,Systemrules,request)
{
	nlapiLogExecution('DEBUG', 'Into ConfirmPickTasks function');
	nlapiLogExecution('DEBUG', 'vClusterNo',vClusterNo);
	nlapiLogExecution('DEBUG', 'getFetchedContainerNo',getFetchedContainerNo);
	nlapiLogExecution('DEBUG', 'ItemNo',ItemNo);
	nlapiLogExecution('DEBUG', 'BeginLocation',BeginLocation);
	nlapiLogExecution('DEBUG', 'EndLocation',EndLocation);
	nlapiLogExecution('DEBUG', 'Reason',Reason);
	nlapiLogExecution('DEBUG', 'ActQty',ActQty);
	nlapiLogExecution('DEBUG', 'getRemQuantity',getRemQuantity);
	nlapiLogExecution('DEBUG', 'getEnteredContainerNo',getEnteredContainerNo);
	nlapiLogExecution('DEBUG', 'vbatchno',vbatchno);
	nlapiLogExecution('DEBUG', 'ExceptionFlag',ExceptionFlag);
	nlapiLogExecution('DEBUG', 'EntLocRec',EntLocRec);
	nlapiLogExecution('DEBUG', 'Systemrules',Systemrules);

	var ExpEndLocation = EndLocation;
	var Expbatchno= vbatchno;

	if(getRemQuantity==null || getRemQuantity=='')
		getRemQuantity=0;

	var vAdjQty=0;

	var SOFilters = new Array();

	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK

	if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
	}

	if(ItemNo != null && ItemNo != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_sku_no', null, 'is', ItemNo));
	}

	if(BeginLocation != null && BeginLocation != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', BeginLocation));
	}

	if(getFetchedContainerNo != null && getFetchedContainerNo != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getFetchedContainerNo));
	}
	//filter added because some times we have same binlocation, item ,container but batchno is diffrent
	if(vbatchno != null && vbatchno != "" && vbatchno!='- None -')
	{
		nlapiLogExecution('ERROR', 'Inside Batchno filter', vbatchno);
		SOFilters.push(new nlobjSearchFilter('custrecord_batch_no', null, 'is', vbatchno));
	}
	var SOColumns = new Array();				
	SOColumns[0] = new nlobjSearchColumn('custrecord_sku');
	SOColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
	SOColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
	SOColumns[3] = new nlobjSearchColumn('custrecord_skudesc');
	SOColumns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');					
	SOColumns[5] = new nlobjSearchColumn('custrecord_wms_location');
	SOColumns[6] = new nlobjSearchColumn('custrecord_comp_id',null);
	SOColumns[7] = new nlobjSearchColumn('custrecord_container_lp_no');
	SOColumns[8] = new nlobjSearchColumn('custrecord_container');
	SOColumns[9] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	SOColumns[10] = new nlobjSearchColumn('custrecord_batch_no');

	SOColumns[0].setSort();	//SKU
	SOColumns[2].setSort();	//Location
	SOColumns[7].setSort();//Container LP

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	if (SOSearchResults != null && SOSearchResults.length > 0) 
	{	
		for (var i = 0; i < SOSearchResults.length; i++)
		{
			var RcId=SOSearchResults[i].getId();
			var PickQty = SOSearchResults[i].getValue('custrecord_expe_qty');
			var EndLocation=SOSearchResults[i].getValue('custrecord_actbeginloc');
			var vdono=SOSearchResults[i].getValue('custrecord_ebiz_cntrl_no');
			var vtotalweight=SOSearchResults[i].getValue('custrecord_total_weight');
			var vbatchno=SOSearchResults[i].getValue('custrecord_batch_no');
			nlapiLogExecution('DEBUG', 'vbatchno', vbatchno);

			if(vbatchno == null || vbatchno=='')
				vbatchno='';

			if(ExceptionFlag =='L') // for Location expection + Qty expection
			{
				if(ExpEndLocation !=null && ExpEndLocation!='')
				{
					if(ExpEndLocation != EndLocation)
					{
						EndLocation=ExpEndLocation;
					}
					if(Expbatchno !=null && Expbatchno !='')
					{
						if(Expbatchno != vbatchno)
						{
							vbatchno = Expbatchno;
						}

					}
				}
			}

			var opentaskcount=0;

			opentaskcount=getOpenTasksCount(vdono,vClusterNo,getFetchedContainerNo);

			nlapiLogExecution('DEBUG', 'opentaskcount', opentaskcount);

			//For fine tuning pick confirmation and doing autopacking in user event after last item
			var IsitLastPick='F';
			if(opentaskcount > 1)
				IsitLastPick='F';
			else
				IsitLastPick='T';

			if(ActQty==null || ActQty=='')
				ActQty=0;

			if(parseFloat(PickQty)>=parseFloat(ActQty))
			{
				vAdjQty=ActQty;
				ActQty=0;
			}
			else
			{
				vAdjQty=PickQty;
				ActQty=ActQty-vAdjQty;
			}

			nlapiLogExecution('DEBUG', 'PickQty', PickQty);
			nlapiLogExecution('DEBUG', 'vAdjQty', vAdjQty);
			nlapiLogExecution('DEBUG', 'vdono', vdono);
			nlapiLogExecution('DEBUG', 'vtotalweight', vtotalweight);

			UpdateRFFulfillOrdLine(vdono,vAdjQty);

			UpdateRFOpenTask(RcId, PickQty, '', getEnteredContainerNo, '','', EndLocation,Reason,
					vAdjQty,IsitLastPick,ExceptionFlag,EntLocRec,getRemQuantity,vbatchno,SalesOrderInternalId,Systemrules);



			if(PickQty != vAdjQty)
			{
				if(kititemTypesku=='kititem')
				{
					nlapiLogExecution('DEBUG', 'vsalesordInternid', SalesOrderInternalId);

					var filters = new Array(); 			 
					filters[0] = new nlobjSearchFilter('custrecord_parent_sku_no', null, 'anyof', vkititem);	
					filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]);	
					filters[2] = new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', SalesOrderInternalId);	

					var columns1 = new Array(); 
					columns1[0] = new nlobjSearchColumn( 'custrecord_expe_qty' ); 			
					columns1[1] = new nlobjSearchColumn( 'custrecord_act_qty' );

					var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, null ); 
					for(var z=0; z<searchresults.length;z++) 
					{										
						var kitrcid=searchresults[z].getId();
						var Expqty=searchresults[z].getValue('custrecord_expe_qty');
						var Actqty=searchresults[z].getValue('custrecord_act_qty');


						if(parseFloat(Expqty) != parseFloat(Actqty))
						{
							var kitopentask = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', Recid);
							kitopentask.setFieldValue('custrecord_kit_exception_flag', 'T');
							nlapiSubmitRecord(kitopentask, false, true);
						}
					}

				}

				var vRemaningqty = getRemQuantity;
				CreateNewShortPick(RcId,PickQty,vAdjQty,vdono,1,getEnteredContainerNo,vtotalweight,vbatchno,vRemaningqty,Systemrules,request);//have to check for wt and cube calculation
			}
		}		
	}

	nlapiLogExecution('DEBUG', 'out of ConfirmPickTasks function');
}

/**
 * 
 * @param RcId
 * @param PickQty
 * @param containerInternalId
 * @param getEnteredContainerNo
 * @param itemCube
 * @param itemWeight
 * @param EndLocation
 * @param vReason
 * @param ActPickQty
 * @param IsItLastPick
 * @param ExceptionFlag
 * @param NewRecId
 */
function UpdateRFOpenTask(RcId, PickQty, containerInternalId, getEnteredContainerNo, itemCube,
		itemWeight, EndLocation,vReason,ActPickQty,IsItLastPick,ExceptionFlag,NewRecId,remqty,batchno,SalesOrderInternalId,Systemrules)
{
	nlapiLogExecution('DEBUG', 'into UpdateRFOpenTask containerlp: ', getEnteredContainerNo);
	var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RcId);
	var vinvrefno=transaction.getFieldValue('custrecord_invref_no');
	transaction.setFieldValue('custrecord_wms_status_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')
	if(PickQty!= null && PickQty !="")
		transaction.setFieldValue('custrecord_act_qty', parseFloat(ActPickQty).toFixed(4));
	if(containerInternalId!= null && containerInternalId !="")
		transaction.setFieldValue('custrecord_container', containerInternalId);
	if(EndLocation!=null && EndLocation!="")
		transaction.setFieldValue('custrecord_actendloc', EndLocation);	
	if(itemWeight!=null && itemWeight!="")
		transaction.setFieldValue('custrecord_total_weight', parseFloat(itemWeight).toFixed(4));
	if(itemCube!=null && itemCube!="")
		transaction.setFieldValue('custrecord_totalcube', parseFloat(itemCube).toFixed(4));	
	if(getEnteredContainerNo!=null && getEnteredContainerNo!="")
		transaction.setFieldValue('custrecord_container_lp_no', getEnteredContainerNo);
	transaction.setFieldValue('custrecord_act_end_date', DateStamp());
	transaction.setFieldValue('custrecord_actualendtime',TimeStamp());
	if(vReason!=null && vReason!="")
		transaction.setFieldValue('custrecord_notes', vReason);	
	var currentContext = nlapiGetContext();  
	var currentUserID = currentContext.getUser();
	transaction.setFieldValue('custrecord_upd_ebiz_user_no',currentUserID);
	nlapiLogExecution('DEBUG', 'IsItLastPick: ', IsItLastPick);
	transaction.setFieldValue('custrecord_device_upload_flag',IsItLastPick);
	var vemployee = request.getParameter('custpage_employee');
	nlapiLogExecution('DEBUG', 'vemployee :', vemployee);
	nlapiLogExecution('DEBUG', 'currentUserID :', currentUserID);

	if(batchno!=null && batchno!="")
		transaction.setFieldValue('custrecord_batch_no', batchno);	
	nlapiLogExecution('DEBUG', 'batchno :', batchno);

	nlapiLogExecution('DEBUG', 'Updating RF Open Task Record Id: ', RcId);
	nlapiSubmitRecord(transaction, false, true);
	nlapiLogExecution('DEBUG', 'Updated RF Open Task successfully');

	//code added on 15Feb by suman.
	if(ExceptionFlag!='L')
		deleteAllocations(vinvrefno,PickQty,getEnteredContainerNo,ActPickQty,remqty,SalesOrderInternalId,Systemrules);
	else
	{
		deleteOldAllocations(vinvrefno,PickQty,getEnteredContainerNo,ActPickQty,SalesOrderInternalId,Systemrules);
		deleteNewAllocations(NewRecId,PickQty,getEnteredContainerNo,ActPickQty,SalesOrderInternalId,Systemrules);
	}
	//End of code as of 15Feb.
}

/**
 * This function clear the allocation qty from inventory.
 * @param invrefno
 * @param PickQty
 */
function deleteAllocations(invrefno,pickqty,contlpno,ActPickQty,remQty,SalesOrderInternalId,Systemrules)
{
	nlapiLogExecution('DEBUG', 'Into deleteAllocations (Inv Ref NO)',invrefno);

	var str = 'Inv Ref No. = ' + invrefno + '<br>';
	str = str + 'CARTON LP. = ' + contlpno + '<br>';	
	str = str + 'ActPickQty. = ' + ActPickQty + '<br>';	
	str = str + 'pickqty. = ' + pickqty + '<br>';	
	str = str + 'remQty. = ' + remQty + '<br>';	

	nlapiLogExecution('Debug', 'Parameter Values new', str);



	/*try
			{
				var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
				var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
				var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

				if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {
					Invttran.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(Invallocqty)- parseFloat(pickqty)).toFixed(4));
				}

				var newqoh = (parseFloat(InvQOH) - parseFloat(pickqty))+parseFloat(remqty);

				Invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(newqoh).toFixed(4)); 
				Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
				Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

				var invtrecid = nlapiSubmitRecord(Invttran, false, true);
				nlapiLogExecution('DEBUG', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);

				if(invtrecid!=null && invtrecid!='')
					CreateSTGInvtRecord(invtrecid, contlpno,ActPickQty,SalesOrderInternalId);

				if(parseFloat(newqoh)== 0)
				{		
					nlapiLogExecution('DEBUG', 'Deleting record from inventory if QOH becomes zero', invrefno);
					var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
				}
			}
			catch(exp)
			{
				nlapiLogExecution('DEBUG', 'Exception in deleteAllocations',exp);
			}		*/


	try
	{

		var scount=1;
		var invtrecid;
		var newqoh = 0;
		LABL1: for(var i=0;i<scount;i++)
		{	

			nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);
			try
			{
				var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
				var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
				var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

				var vNewAllocQty=0;
				if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {
					vNewAllocQty=(parseFloat(Invallocqty)- parseFloat(pickqty)).toFixed(4);
					if(parseFloat(vNewAllocQty)<0)
						vNewAllocQty=0;
					//	Invttran.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(Invallocqty)- parseFloat(ActPickQty)).toFixed(4));
					Invttran.setFieldValue('custrecord_ebiz_alloc_qty',parseFloat(vNewAllocQty));
				}
				else
					Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);

				nlapiLogExecution('Debug', 'vNewAllocQty',vNewAllocQty);



				var remainqty=0;
				//var newqoh = 0;

				/*if(remQty==null || remQty=='null'  || remQty=='' || isNaN(remQty))
				{
					newqoh = (parseFloat(InvQOH) - parseFloat(pickqty)).toFixed(4);
					if(parseFloat(newqoh)<0)
						newqoh=0;
					Invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(newqoh)); 
				}
				else
				{
					newqoh = parseFloat(vNewAllocQty) + parseFloat(remQty);
					Invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(newqoh)); 
				}
				 */


				//Case # 20126289,20126844 Start

				if(parseInt(pickqty) != parseInt(ActPickQty))
				{

					nlapiLogExecution('ERROR', 'Systemrules.length', Systemrules.length);
					nlapiLogExecution('ERROR', 'Systemrules[0]', Systemrules[0]);
					nlapiLogExecution('ERROR', 'Systemrules[1]', Systemrules[1]);

					if(Systemrules.length>0 && Systemrules[0]=='Y' && (Systemrules[1] =="STND" ||Systemrules[1] =="NOADJUST" ))
					{

//						if(remQty==null || remQty=='null'  || remQty=='' || isNaN(remQty))
//						{
//						newqoh = parseFloat(InvQOH) - parseFloat(pickqty);
//						}
//						else
//						{
//						newqoh = parseFloat(InvQOH) - parseFloat(actPickqty);
//						}

						if(Systemrules[1] =="STND")
							newqoh = parseFloat(InvQOH) - parseFloat(pickqty);
						else if (Systemrules[1] =="NOADJUST")
							newqoh = parseFloat(InvQOH) - parseFloat(ActPickQty);
					}
					else if(Systemrules.length>0 && Systemrules[0]=='Y' && Systemrules[1] =="ADJUSTBINLOC")
					{
						// consider binlocation quantity

//						if(remQty==null || remQty=='null'  || remQty=='' || isNaN(remQty))
//						{
//						newqoh = parseInt(InvQOH) - parseInt(pickqty);
//						}
//						else
//						{
//						newqoh = parseInt(vNewAllocQty) + parseInt(remQty);
//						}

						newqoh = parseFloat(InvQOH) - parseFloat(ActPickQty);

					}
					else
					{
						newqoh = parseFloat(InvQOH) - parseFloat(ActPickQty);
					}

				}
				else
				{
					newqoh = parseInt(InvQOH) - parseInt(actPickqty);
				}

				Invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(newqoh)); 

				//newqoh = parseInt(InvQOH) - parseInt(ActPickQty);

				//Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(newqoh)); 
				Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
				Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

				var invtrecid = nlapiSubmitRecord(Invttran, false, true);
				nlapiLogExecution('ERROR', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);
			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 

				nlapiLogExecution('Debug', 'Exception in RF Inv move : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}



		if(invtrecid!=null && invtrecid!='')
			CreateSTGInvtRecord(invtrecid, contlpno,ActPickQty,SalesOrderInternalId);

		if(parseInt(newqoh)== 0)
		{		
			nlapiLogExecution('ERROR', 'Deleting record from inventory if QOH becomes zero', invrefno);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
		}
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR', 'Exception in deleteAllocations',exp);
	}		
}

/**
 * This function creats outbound staging inventory
 * @param invtrecid
 * @param vContLp
 * @param vqty
 */

function CreateSTGInvtRecord(invtrecid, vContLp,vqty,SalesOrderInternalId) 
{
	nlapiLogExecution('DEBUG', 'Into CreateSTGInvtRecord (Container LP)',vContLp);

	try
	{
		var stgmInvtRec = nlapiCopyRecord('customrecord_ebiznet_createinv',invtrecid);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_lp', vContLp);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(vqty).toFixed(4));
		stgmInvtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(vqty).toFixed(4));
		stgmInvtRec.setFieldValue('custrecord_ebiz_alloc_qty', '0');
		stgmInvtRec.setFieldValue('custrecord_wms_inv_status_flag', '18');	//FLAG.INVENTORY.OUTBOUND('O') 		
		stgmInvtRec.setFieldValue('custrecord_invttasktype', '3'); //Task Type - PICK
		stgmInvtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
		stgmInvtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');	
		if(SalesOrderInternalId!=null && SalesOrderInternalId!='')
			stgmInvtRec.setFieldValue('custrecord_ebiz_transaction_no', SalesOrderInternalId);	
		nlapiSubmitRecord(stgmInvtRec, false, true);
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in CreateSTGInvtRecord',exp);
	}

	nlapiLogExecution('DEBUG', 'Out of CreateSTGInvtRecord');
}

/**
 * 
 * @param lp
 * @returns
 */
function containerRFLPExists(lp){
	var filters = new Array();
	nlapiLogExecution('DEBUG', 'lp:',lp); 
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lp));	 
	nlapiLogExecution('DEBUG', 'Master LP Feaching','Success');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
//	var lpmastArr=new Array();
//	if(searchresults != null && searchresults!= "" && searchresults.length>0)
//	{
//	lpmastArr.push(searchresults[0].getValue('custrecord_ebiz_lpmaster_lp'));
//	lpmastArr.push(searchresults[0].getValue('custrecord_ebiz_lpmaster_totwght'));

//	}
	return searchresults;
}

function OpenTaskRFLPExists(lp){
	var filters = new Array();
	nlapiLogExecution('DEBUG', 'lp:',lp); 
	filters.push( new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', lp));	 

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');

	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	return searchresults;
}

function getLPWeight(lp){
	var filters = new Array();
	nlapiLogExecution('DEBUG', 'lp:',lp); 
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lp));	 
	nlapiLogExecution('DEBUG', 'Master LP Feaching','Success');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
	var lpmastArr=new Array();
	nlapiLogExecution('DEBUG', 'Before:',lp); 
	if(searchresults != null && searchresults!= "" && searchresults.length>0)
	{
		lpmastArr.push(searchresults[0].getValue('custrecord_ebiz_lpmaster_lp'));
		lpmastArr.push(searchresults[0].getValue('custrecord_ebiz_lpmaster_totwght'));
		nlapiLogExecution('DEBUG', 'lp:',searchresults[0].getValue('custrecord_ebiz_lpmaster_lp')); 
		nlapiLogExecution('DEBUG', 'lpweight:',searchresults[0].getValue('custrecord_ebiz_lpmaster_totwght')); 
	}
	return lpmastArr;
}
function containerSizeExists(lp,SizeId){
	var filters = new Array();
	nlapiLogExecution('DEBUG', 'lp:',lp); 
	nlapiLogExecution('DEBUG', 'SizeId:',SizeId); 
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lp));
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_sizeid', null, 'anyof', SizeId));	
	nlapiLogExecution('DEBUG', 'Size Feaching','Success');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sizeid');
	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);        	       	
	return searchresults;
}
/**
 * 
 * @param vContLpNo
 * @param vContainerSize
 * @param TotalWeight
 * @param ContainerCube
 */
function CreateRFMasterLPRecord(vContLpNo,containerInternalId,ContainerCube,ItemWeight)
{
	var contLPExists = containerRFLPExists(vContLpNo);
	var LPWeightExits = getLPWeight(vContLpNo);
	if(LPWeightExits!=null && LPWeightExits!="")
	{		
		var ContLP=LPWeightExits[0];
		var LPWeight=LPWeightExits[1];
		nlapiLogExecution('DEBUG', 'LPWeight',LPWeight);
	}

	//var ContSizeExits=containerSizeExists(vContLpNo,containerInternalId);

	var TotalWeight;	
	var arrContainerDetails = getContainerCubeAndTarWeight(containerInternalId,"");

	if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) 
	{		
		TotalWeight = (parseFloat(ItemWeight) + parseFloat(arrContainerDetails[1]));
	} 

	if(contLPExists==null){		

		var MastLP=nlapiCreateRecord('customrecord_ebiznet_master_lp');
		MastLP.setFieldValue('name', vContLpNo);
		MastLP.setFieldValue('custrecord_ebiz_lpmaster_lp', vContLpNo);
		if(containerInternalId!="" && containerInternalId!=null)
		{
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_sizeid', containerInternalId);
		}	
		nlapiLogExecution('DEBUG', 'TotalWeight',TotalWeight);
		nlapiLogExecution('DEBUG', 'ContainerCube',ContainerCube);
		if(TotalWeight!="" && TotalWeight!=null)
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_totwght', parseFloat(TotalWeight).toFixed(4));
		if(ContainerCube!="" && ContainerCube!=null)
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(ContainerCube).toFixed(4));
		//MastLP.setFieldValue('custrecord_ebiz_lpmaster_company', vCompany);

		var currentContext = nlapiGetContext();		        		
		var retktoval = nlapiSubmitRecord(MastLP);
		nlapiLogExecution('DEBUG', 'Master LP Insertion','Success');

	}
	else
	{
		var recordId="";

		if(contLPExists!=null){
//			if(ContSizeExits)
//			{

			for (var i = 0; i < contLPExists.length; i++){
				recordId = contLPExists[i].getId();
				nlapiLogExecution('DEBUG', 'recordId',recordId);		

				var MastLPUpdate = nlapiLoadRecord('customrecord_ebiznet_master_lp', recordId);	
				TotWeight=parseFloat(ItemWeight)+parseFloat(LPWeight);
				nlapiLogExecution('DEBUG', 'itemWeight',ItemWeight);
				nlapiLogExecution('DEBUG', 'LPWeight',LPWeight);
				nlapiLogExecution('DEBUG', 'TotWeight',TotWeight);

				//	MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_sizeid', vContainerSize);
				if(TotWeight!="" && TotWeight!=null)
					MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_totwght', parseFloat(TotWeight).toFixed(4));
				if(ContainerCube!="" && ContainerCube!=null)
					MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(ContainerCube).toFixed(4));

				var currentContext = nlapiGetContext();	        				
				nlapiSubmitRecord(MastLPUpdate, false, true);        				 
				nlapiLogExecution('DEBUG', 'Master LP Updation','Success');

			}
			//}
//			else
//			{

//			for (var i = 0; i < contLPExists.length; i++){
//			recordId = contLPExists[i].getId();
//			nlapiLogExecution('DEBUG', 'recordId',recordId);		

//			var MastLPUpdate = nlapiLoadRecord('customrecord_ebiznet_master_lp', recordId);	

//			if(containerInternalId!="")
//			{
//			MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_sizeid', containerInternalId);
//			}
//			MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_totwght', TotalWeight);
//			MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_totcube', ContainerCube);

//			var currentContext = nlapiGetContext();	        				
//			nlapiSubmitRecord(MastLPUpdate, false, true);        				 
//			nlapiLogExecution('DEBUG', 'Master LP Updation','Success');

//			}

//			}

		}
	}
}


function UpdateRFFulfillOrdLine(vdono,vActqty,request)
{
	var pickqtyfinal =0;
	nlapiLogExecution('DEBUG', 'into UpdateRFFulfillOrdLine : ', vdono);

	var doline = nlapiLoadRecord('customrecord_ebiznet_ordline',vdono);	

	var oldpickQty=doline.getFieldValue('custrecord_pickqty');
	var ordQty=doline.getFieldValue('custrecord_ord_qty');

	//case # 20122625 start
	if(oldpickQty=='' || oldpickQty==null)
		oldpickQty=0;
	//case # 20122625 end


	if(isNaN(oldpickQty))
		oldpickQty=0;

	if(isNaN(ordQty))
		ordQty=0;

	pickqtyfinal=parseFloat(oldpickQty)+parseFloat(vActqty);
	nlapiLogExecution('DEBUG', 'pickqtyfinal in UpdateRFFulfillOrdLine', pickqtyfinal);	

	doline.setFieldValue('custrecord_upddate', DateStamp());
	doline.setFieldValue('custrecord_pickqty', parseFloat(pickqtyfinal).toFixed(4));
	//doline.setFieldValue('custrecord_pickgen_qty', parseFloat(pickqtyfinal).toFixed(4));
	if(parseFloat(pickqtyfinal)<parseFloat(ordQty))
		doline.setFieldValue('custrecord_linestatus_flag', '11'); //STATUS.OUTBOUND.PARTIALLY_PROCESSED
	else
		doline.setFieldValue('custrecord_linestatus_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')

	nlapiSubmitRecord(doline, false, true);

	nlapiLogExecution('DEBUG', 'Out of UpdateRFFulfillOrdLine ');
}

/**
 * This function clear the allocation qty from inventory.
 * @param invrefno
 * @param PickQty
 */
function deleteOldAllocations(invrefno,pickqty,contlpno,ActPickQty,Systemrules)
{
	nlapiLogExecution('DEBUG', 'Into deleteAllocations (Inv Ref NO)',invrefno);

	/*try
	{
		var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
		var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
		var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

		if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {
			Invttran.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(Invallocqty)- parseFloat(ActPickQty)).toFixed(4));
		}

		//Invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(InvQOH) - parseFloat(ActPickQty)); 
		Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
		Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

		var invtrecid = nlapiSubmitRecord(Invttran, false, true);
		nlapiLogExecution('DEBUG', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);

		if(invtrecid!=null && invtrecid!='')
			CreateSTGInvtRecord(invtrecid, contlpno,pickqty);

		if((parseFloat(InvQOH) - parseFloat(ActPickQty)) == 0)
		{		
			nlapiLogExecution('DEBUG', 'Deleting record from inventory if QOH becomes zero', invrefno);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in deleteAllocations',exp);
	}		*/


	try
	{
		var scount=1;
		var invtrecid;

		LABL1: for(var i=0;i<scount;i++)
		{	

			nlapiLogExecution('ERROR', 'CUSTOM_RECORD_COLLISION', i);
			try
			{
				var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
				var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
				var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

				if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {

					//Case # 20126639� Start
					nlapiLogExecution('ERROR', 'pickqty',pickqty);
					nlapiLogExecution('ERROR', 'ActPickQty',ActPickQty);
					/*if(parseFloat(Invallocqty)- parseFloat(ActPickQty)>=0)
						Invttran.setFieldValue('custrecord_ebiz_alloc_qty',parseFloat(Invallocqty)- parseFloat(ActPickQty));
					else
						Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);*/
					if(parseFloat(Invallocqty)- parseFloat(pickqty)>=0)
						Invttran.setFieldValue('custrecord_ebiz_alloc_qty',parseFloat(Invallocqty)- parseFloat(pickqty));
					else
						Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);
					//Case # 20126639� End

				}
				else
				{
					Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);
				}

				//Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(InvQOH) - parseInt(ActPickQty)); 
				Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
				Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

				var invtrecid = nlapiSubmitRecord(Invttran, false, true);
				nlapiLogExecution('ERROR', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);

			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 

				nlapiLogExecution('Debug', 'Exception in RF Inv move : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}

		if(invtrecid!=null && invtrecid!='')
			CreateSTGInvtRecord(invtrecid, contlpno,pickqty);

		if((parseInt(InvQOH) - parseInt(ActPickQty)) == 0)
		{		
			nlapiLogExecution('ERROR', 'Deleting record from inventory if QOH becomes zero', invrefno);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
		}
	}
	catch(exp)
	{
		nlapiLogExecution('Debug', 'Exception in deleteAllocations',exp);
	}

	nlapiLogExecution('Debug', 'Out of deleteOldAllocations (Inv Ref NO)',invrefno);
}

/**
 * This function clear the allocation qty from inventory.
 * @param invrefno
 * @param PickQty
 */
function deleteNewAllocations(invrefno,pickqty,contlpno,ActPickQty,SalesOrderInternalId,Systemrules)
{
	nlapiLogExecution('DEBUG', 'Into deleteAllocations (Inv Ref NO)',invrefno);

	/*try
	{
		var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
		var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
		var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');



		Invttran.setFieldValue('custrecord_ebiz_qoh',(parseFloat(InvQOH) - parseFloat(ActPickQty)).toFixed(4)); 
		Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
		Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

		var invtrecid = nlapiSubmitRecord(Invttran, false, true);
		nlapiLogExecution('DEBUG', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);

		if(invtrecid!=null && invtrecid!='')
			CreateSTGInvtRecord(invtrecid, contlpno,pickqty,SalesOrderInternalId);

		if((parseFloat(InvQOH) - parseFloat(ActPickQty)) == 0)
		{		
			nlapiLogExecution('DEBUG', 'Deleting record from inventory if QOH becomes zero', invrefno);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in deleteAllocations',exp);
	}		*/


	nlapiLogExecution('Debug', 'Into deleteNewAllocations (Inv Ref NO)',invrefno);
//	nlapiLogExecution('Debug', 'actPickQty',actPickQty);

	try
	{

		var scount=1;
		var invtrecid;
		var InvQOH =0;
		LABL1: for(var i=0;i<scount;i++)
		{	

			nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);
			try
			{
				var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
				var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
				InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

				if (InvQOH != null && InvQOH != "" && InvQOH>0) {


					//Case # 20126289 Start

					nlapiLogExecution('ERROR', 'Systemrules.length', Systemrules.length);
					nlapiLogExecution('ERROR', 'Systemrules[0]', Systemrules[0]);
					nlapiLogExecution('ERROR', 'Systemrules[1]', Systemrules[1]);

					if(Systemrules.length>0 && Systemrules[0]=='Y' && (Systemrules[1] =="STND" ||Systemrules[1] =="NOADJUST" ))
					{

//						if(remQty==null || remQty=='null'  || remQty=='' || isNaN(remQty))
//						{
//						newqoh = parseFloat(InvQOH) - parseFloat(pickqty);
//						}
//						else
//						{
//						newqoh = parseFloat(InvQOH) - parseFloat(actPickqty);
//						}

						if(Systemrules[1] =="STND")
							newqoh = parseFloat(InvQOH) - parseFloat(pickqty);
						else if (Systemrules[1] =="NOADJUST")
							newqoh = parseFloat(InvQOH) - parseFloat(ActPickQty);
					}
					else if(Systemrules.length>0 && Systemrules[0]=='Y' && Systemrules[1] =="ADJUSTBINLOC")
					{
						// consider binlocation quantity

//						if(remQty==null || remQty=='null'  || remQty=='' || isNaN(remQty))
//						{
//						newqoh = parseInt(InvQOH) - parseInt(pickqty);
//						}
//						else
//						{
//						newqoh = parseInt(vNewAllocQty) + parseInt(remQty);
//						}

						newqoh = parseFloat(InvQOH) - parseFloat(ActPickQty);

					}
					else
					{
						newqoh = parseFloat(InvQOH) - parseFloat(ActPickQty);
					}

					Invttran.setFieldValue('custrecord_ebiz_qoh',newqoh);

					/*if(parseFloat(InvQOH)- parseFloat(ActPickQty)>=0)
						Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(InvQOH) - parseInt(ActPickQty));
					else
						Invttran.setFieldValue('custrecord_ebiz_qoh',0);*/

				}
				else
				{
					Invttran.setFieldValue('custrecord_ebiz_qoh',0);
				}

				//Invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(InvQOH) - parseFloat(pickqty)); 
				Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
				Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

				var invtrecid = nlapiSubmitRecord(Invttran, false, true);
				nlapiLogExecution('ERROR', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);
			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 
				nlapiLogExecution('Debug', 'Exception in RF Inv move : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}
		if(invtrecid!=null && invtrecid!='')
			CreateSTGInvtRecord(invtrecid, contlpno,pickqty,SalesOrderInternalId);

		if((parseInt(InvQOH) - parseInt(ActPickQty)) == 0)
		{		
			nlapiLogExecution('ERROR', 'Deleting record from inventory if QOH becomes zero', invrefno);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
		}
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR', 'Exception in deleteAllocations',exp);
	}	

	nlapiLogExecution('ERROR', 'Out of deleteNewAllocations (Inv Ref NO)',invrefno);

}

function getOpenReplns(item,location)
{
	nlapiLogExecution('DEBUG', 'Into getOpenReplns');
	var PickFaceSearchResults= new Array();
	var PickFaceFilters = new Array();
	var PickFaceColumns = new Array();

	PickFaceFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8])); // Task Type - RPLN	 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', item)); 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'is', location));	
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));

	PickFaceColumns[0] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
	//PickFaceColumns[1] = new nlobjSearchColumn('custrecord_taskpriority');

	PickFaceSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, PickFaceFilters, PickFaceColumns);



	return PickFaceSearchResults;

}

function getQOHForAllSKUsinPFLocation(location,item){

	nlapiLogExecution('DEBUG','location',location);
	nlapiLogExecution('DEBUG','item',item);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', location));	
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', item));
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19','3']));

	var columns = new Array();	
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty',null,'sum');

	var pfSKUInvList = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

	return pfSKUInvList;
}

function buildTaskArray(WaveNo,ClusNo,OrdNo,vSOId,vZoneId)
{
	var SOarray = new Array();
	var SOFilters = new Array();
	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

	if(WaveNo != null && WaveNo != "")
	{
		nlapiLogExecution('DEBUG', 'WaveNo inside If', WaveNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
	}

	if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
	{
		nlapiLogExecution('DEBUG', 'ClusNo inside If', ClusNo);

		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
	}

	if(OrdNo!=null && OrdNo!="" && OrdNo!= "null")
	{
		nlapiLogExecution('DEBUG', 'OrdNo inside If', OrdNo);

		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdNo));
	}

	if(vSOId!=null && vSOId!="" && vSOId!= "null")
	{
		nlapiLogExecution('DEBUG', 'SO Id inside If', vSOId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vSOId));
	}

	if(vZoneId!=null && vZoneId!="" && vZoneId!= "null")
	{
		nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
	}
	SOarray["custparam_ebizordno"] = vSOId;
	var SOColumns = new Array();
	SOColumns[0] = new nlobjSearchColumn('custrecord_skiptask');
	SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
	SOColumns[2] = new nlobjSearchColumn('custrecord_sku');
	SOColumns[3] = new nlobjSearchColumn('custrecord_expe_qty');
	SOColumns[4] = new nlobjSearchColumn('custrecord_actbeginloc');		
	SOColumns[5] = new nlobjSearchColumn('custrecord_skudesc');
	SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	SOColumns[7] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	SOColumns[8] = new nlobjSearchColumn('custrecord_invref_no');
	SOColumns[9] = new nlobjSearchColumn('custrecord_line_no');
	SOColumns[10] = new nlobjSearchColumn('custrecord_actbeginloc');
	SOColumns[11] = new nlobjSearchColumn('custrecord_batch_no');
	SOColumns[12] = new nlobjSearchColumn('custrecord_wms_location');
	SOColumns[13] = new nlobjSearchColumn('custrecord_comp_id');
	SOColumns[14] = new nlobjSearchColumn('name');
	SOColumns[15] = new nlobjSearchColumn('custrecord_container');
	SOColumns[16] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	SOColumns[17] = new nlobjSearchColumn('custrecord_ebizzone_no');
	SOColumns[18] = new nlobjSearchColumn('custrecord_lpno');
	SOColumns[19] = new nlobjSearchColumn('custrecord_ebiz_clus_no');

	SOColumns[0].setSort(true);
	SOColumns[1].setSort(false);
	SOColumns[2].setSort(false);

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

	if (SOSearchResults != null && SOSearchResults.length > 0) {

		var SOSearchResult = SOSearchResults[0];
		if(SOSearchResults.length > 1)
		{
			var SOSearchnextResult = SOSearchResults[1];
			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
			nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
		}
		else
		{
			var SOSearchnextResult = SOSearchResults[0];
			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
			nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
			nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
		}

		SOarray["custparam_waveno"] = WaveNo;
		SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
		SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
		SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
		SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
		SOarray["custparam_endlocinternalid"] = SOSearchResult.getValue('custrecord_actbeginloc');		
		SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
		SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
		SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
		SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
		SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
		SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
		SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
		SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
		SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
		SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
		SOarray["custparam_noofrecords"] = SOSearchResults.length;		
		SOarray["name"] =  SOSearchResult.getValue('name');
		SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
		SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
		SOarray["custparam_clusterno"] = SOSearchResult.getValue('custrecord_ebiz_clus_no');
		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
		}
		else
			SOarray["custparam_ebizzoneno"] = '';
	}

	return SOarray;
}

function fnValidateEnteredContLP(EnteredContLP,foname)
{
	var retVal = -1;
	nlapiLogExecution('DEBUG', 'Into fnValidateEnteredContLP',EnteredContLP);
	nlapiLogExecution('DEBUG', 'foname',foname);
	var filters=new Array();
	filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', EnteredContLP));
	if(foname!=null && foname!='')
		filters.push(new nlobjSearchFilter('name', null, 'isnot', foname));  
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');

	var salesOrderList = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(salesOrderList != null && salesOrderList != '')
	{
		retVal = salesOrderList.length;
		nlapiLogExecution('DEBUG', 'retVal',retVal);
	}
	else
		retVal = 0;

	nlapiLogExecution('DEBUG', 'Out of fnValidateEnteredContLP',retVal);
	return retVal;
}

/**
 * @param getEnteredContainerNo
 * @param getFetchedContainerNo
 * @param SOOrder
 * @param OrdName
 */
function UpdateAllContainerLp(getEnteredContainerNo,getFetchedContainerNo,SOOrder,OrdName)
{
	try
	{
		nlapiLogExecution('DEBUG','Into UpdateAllContainerLp');
		nlapiLogExecution('DEBUG','getEnteredContainerNo',getEnteredContainerNo);
		nlapiLogExecution('DEBUG','getFetchedContainerNo',getFetchedContainerNo);
		nlapiLogExecution('DEBUG','SOOrder',SOOrder);
		nlapiLogExecution('DEBUG','OrdName',OrdName);

		var context = nlapiGetContext();
		var orderno, containerno,ebizcntrlno,siteid,compid,containersize,totweight,internalid,item,waveno,
		beginloc,endloc,lot,actqty,expqty,uomlevel,empid,begindate,begintime,packcode,lp,lineno,Zoneno,ebizLP;

		var otparent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent');
		var parentid = nlapiSubmitRecord(otparent); 
		var newParent = nlapiLoadRecord('customrecord_ebiz_throwaway_parent', parentid);


		var filteropentask=new Array();
		filteropentask.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
		filteropentask.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK		
		filteropentask.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getFetchedContainerNo));	
		filteropentask.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));

//		if(SOOrder!=null&&SOOrder!="")
//		filteropentask.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', parseFloat(SOOrder)));

//		filteropentask.push(new nlobjSearchFilter('name', null, 'is',OrdName));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('name');
		columns[1] = new nlobjSearchColumn('custrecord_container_lp_no');
		columns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		columns[4] = new nlobjSearchColumn('custrecord_comp_id');
		columns[5] = new nlobjSearchColumn('custrecord_wms_location');
		columns[6] = new nlobjSearchColumn('custrecord_container');
		columns[7] = new nlobjSearchColumn('custrecord_total_weight');
		columns[8] = new nlobjSearchColumn('custrecord_sku');
		columns[9] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[10] = new nlobjSearchColumn('custrecord_actendloc');
		columns[11] = new nlobjSearchColumn('custrecord_batch_no');
		columns[12] = new nlobjSearchColumn('custrecord_act_qty');
		columns[13] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[14] = new nlobjSearchColumn('custrecord_uom_level');
		columns[15] = new nlobjSearchColumn('custrecord_ebizuser');
		columns[16] = new nlobjSearchColumn('custrecordact_begin_date');
		columns[17] = new nlobjSearchColumn('custrecord_actualbegintime');
		columns[18] = new nlobjSearchColumn('custrecord_packcode');
		columns[19] = new nlobjSearchColumn('custrecord_lpno');
		columns[20] = new nlobjSearchColumn('custrecord_line_no');
		columns[21] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
		columns[22] = new nlobjSearchColumn('custrecord_ebiz_zoneid');
		columns[23] = new nlobjSearchColumn('custrecord_ebiz_lpno');


		var SearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filteropentask, columns);
		if(SearchResults!=null&&SearchResults!="")
		{
			nlapiLogExecution('DEBUG','SearchResults.length',SearchResults.length);
			for ( var i = 0; i < SearchResults.length; i++) 
			{

//				var recid=SearchResults[count].getId();
//				var UpdateRec=nlapiSubmitField('customrecord_ebiznet_trn_opentask',recid,'custrecord_container_lp_no',getEnteredContainerNo);
//				nlapiLogExecution('DEBUG','UpdateRec',UpdateRec);

				orderno = SearchResults[i].getValue('name');
				containerno = SearchResults[i].getValue('custrecord_container_lp_no');
				ebizorderno = SearchResults[i].getValue('custrecord_ebiz_order_no');
				ebizcntrlno = SearchResults[i].getValue('custrecord_ebiz_cntrl_no');
				compid = SearchResults[i].getValue('custrecord_comp_id');
				siteid = SearchResults[i].getValue('custrecord_wms_location');
				containersize = SearchResults[i].getValue('custrecord_container');
				totweight = SearchResults[i].getValue('custrecord_total_weight');
				item = SearchResults[i].getValue('custrecord_sku');
				beginloc = SearchResults[i].getValue('custrecord_actbeginloc');
				endloc = SearchResults[i].getValue('custrecord_actendloc');
				lot = SearchResults[i].getValue('custrecord_batch_no');
				actqty = SearchResults[i].getValue('custrecord_act_qty');
				expqty = SearchResults[i].getValue('custrecord_expe_qty');
				uomlevel = SearchResults[i].getValue('custrecord_uom_level');
				empid = SearchResults[i].getValue('custrecord_ebizuser');
				internalid = SearchResults[i].getId();
				begindate=SearchResults[i].getValue('custrecordact_begin_date');
				begintime=SearchResults[i].getValue('custrecord_actualbegintime');
				packcode=SearchResults[i].getValue('custrecord_packcode');
				lp=SearchResults[i].getValue('custrecord_lpno');
				lineno=SearchResults[i].getValue('custrecord_line_no');
				waveno = SearchResults[i].getValue('custrecord_ebiz_wave_no');
				Zoneno = SearchResults[i].getValue('custrecord_ebiz_zoneid');
				ebizLP = SearchResults[i].getValue('custrecord_ebiz_lpno');

				nlapiLogExecution('DEBUG','internalid',internalid);
				newParent.selectNewLineItem('recmachcustrecord_ebiz_ot_parent');

				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'id', internalid);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','name', orderno);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_wave_no', waveno);
				//newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualendtime', TimeStamp());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_recordupdatetime', TimeStamp());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_upd_date', DateStamp());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actendloc', endloc);
				//newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_end_date', DateStamp());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_batch_no', lot);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_status_flag', 9);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_location', siteid);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_uom_level', uomlevel);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_qty', parseFloat(actqty).toFixed(4));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_upd_ebiz_user_no', context.getUser());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actbeginloc', beginloc);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecordact_begin_date',begindate);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualbegintime', begintime);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_taskassignedto', empid);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_cntrl_no', ebizcntrlno);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_tasktype', 3);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_expe_qty', parseFloat(expqty).toFixed(4));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_sku', item);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_sku_no', item);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_packcode', packcode);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_lpno', lp);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_order_no', ebizorderno);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_line_no', lineno);

				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_comp_id', compid);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_container_lp_no', getEnteredContainerNo);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_container', containersize);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_total_weight', parseFloat(totweight).toFixed(4));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_zoneid', Zoneno);
				if(ebizLP==null||ebizLP=="")
				{
					nlapiLogExecution('DEBUG','INTO OLD Lp');
					newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_lpno', getFetchedContainerNo);
				}
				newParent.commitLineItem('recmachcustrecord_ebiz_ot_parent');


			}
			nlapiSubmitRecord(newParent);
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in UpdateAllContianerLp',exp);	
	}
}
