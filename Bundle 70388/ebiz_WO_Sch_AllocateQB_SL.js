/***************************************************************************
  eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inventory/Suitelet/Attic/ebiz_WO_Sch_AllocateQB_SL.js,v $
 *     	   $Revision: 1.1.4.2 $
 *     	   $Date: 2014/09/26 15:25:23 $
 *     	   $Author: skavuri $
 *     	   $Name: t_eBN_2014_2_StdBundle_0_130 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_WO_Sch_AllocateQB_SL.js,v $
 * Revision 1.1.4.2  2014/09/26 15:25:23  skavuri
 * Case# 201410092 jawbone cr
 *
 * Revision 1.1.6.2.4.1  2013/03/08 14:43:37  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.1.6.2  2012/12/03 16:09:30  skreddy
 * CASE201112/CR201113/LOG201121
 * Changed QC Status
 *
 * Revision 1.1.6.1  2012/10/26 14:00:27  skreddy
 * CASE201112/CR201113/LOG201121
 * Changed the custom id of Qc stautus
 *
 * Revision 1.1  2011/12/30 16:56:38  gkalla
 * CASE201112/CR201113/LOG201121
 * For WO allocation
 * 
 *
 *****************************************************************************/
/*
 * Processing HTTP GET Request
 */

function GenerateLocationsGet(request, response){
	nlapiLogExecution('ERROR', 'WOGenerateLocations Start', 'Generate locations Start');
	var form = nlapiCreateForm('Generate Bin Locations for Work orders');


	var WorkOrder  = form.addField('custpage_workorder', 'select', 'Work Order').setLayoutType('startrow', 'none');
	WorkOrder.addSelectOption("","");
	fillWOField(form, WorkOrder,-1);

	// Retrieve all sales orders
	//var WorkOrderList = getAllWorkOrders();		
	// Add all sales orders to SO Field
	//WorkOrder.setDefaultValue(request.getParameter('custpage_workorder'));
	//addAllWorkOrdersToField(WorkOrder, WorkOrderList,form);

	WorkOrder.setMandatory(true);
	//fillsalesorderField(form, salesorder,-1);

	form.addSubmitButton('Display');

	response.writePage(form);
}

/*
 * Processing HTTP POST Request
 */
function GenerateLocationsPost(request, response){
	nlapiLogExecution('ERROR', 'Generate Bin locations Start', 'Generate Bin locations'); 
	var vWorkOrder = request.getParameter('custpage_workorder');

	nlapiLogExecution('ERROR','vWorkOrder ',vWorkOrder);
	var kitArray = new Array(); 
	kitArray["custpage_workorder"] = vWorkOrder;
	response.sendRedirect('SUITELET', 'customscript_ebiz_wo_generateloc_sch_sl',
			'customdeploy_ebiz_wo_generateloc_sch_di', false, kitArray);

}

function GenerateLocationsSchWO(request, response){
	if (request.getMethod() == 'GET') 
	{
		GenerateLocationsGet(request, response);
	} 
	else 
	{
		//code added by santosh on 5sep12
		var form = nlapiCreateForm('Generate Bin Locations for Work orders');
		var vWorkOrder = request.getParameter('custpage_workorder');
		var searchresults = nlapiLoadRecord('workorder',vWorkOrder);
		var ApproveFlag=searchresults.getFieldValue('custbody_ebiz_wo_qc_status');
		var Approveflagtext=searchresults.getFieldText('custbody_ebiz_wo_qc_status');
		nlapiLogExecution('ERROR','Approveflagtext ',Approveflagtext);
		var vWOId=searchresults.getFieldValue('tranid');
		GenerateLocationsPost(request, response);
		response.writePage(form);
		/*if(ApproveFlag=='2')//For Approve
		{
			nlapiLogExecution('ERROR','Approve flag ',ApproveFlag);
    	GenerateLocationsPost(request, response);
			response.writePage(form);
		}
		else//For Pending
		{
			nlapiLogExecution('ERROR','Approve flag ',ApproveFlag);
			showInlineMessage(form, 'Error','Please Approve this Work order ' + vWOId + '', '');
			response.writePage(form);
		} //end of the code on 5sep12
		 */    }    
}

function fillWOField(form, WOField,maxno){
	var WOFilers = new Array();
	WOFilers.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
	WOFilers.push(new nlobjSearchFilter('status', null, 'is', ['WorkOrd:B','WorkOrd:D']));

	if(maxno!=-1)
	{
		WOFilers.push(new nlobjSearchFilter('internalidnumber', null, 'lessthan', parseFloat(maxno)));
	}


	WOField.addSelectOption("", "");

	var columns=new Array();
	columns[0]=new nlobjSearchColumn('tranid');
	columns[0].setSort(true);
	columns[1]=new nlobjSearchColumn('internalid');
	columns[2]=new nlobjSearchColumn('type');


	var customerSearchResults = nlapiSearchRecord('workorder', null, WOFilers,columns);


	for (var i = 0; customerSearchResults != null && i < customerSearchResults.length; i++) {

		//nlapiLogExecution('ERROR', 'tranid', customerSearchResults[i].getValue('tranid')); 
		if(customerSearchResults[i].getValue('tranid') != null && customerSearchResults[i].getValue('tranid') != "" && customerSearchResults[i].getValue('tranid') != " ")
		{
			var resdo = form.getField('custpage_workorder').getSelectOptions(customerSearchResults[i].getValue('tranid'), 'is');
			if (resdo != null) {
				if (resdo.length > 0) {
					continue;
				}
			}
		}
		WOField.addSelectOption(customerSearchResults[i].getValue('internalid'), customerSearchResults[i].getValue('tranid'));
	}
	if(customerSearchResults!=null && customerSearchResults.length>=1000)
	{
		var column=new Array();
		column[0]=new nlobjSearchColumn('tranid');		
		column[1]=new nlobjSearchColumn('internalid');
		column[1].setSort(true);

		var OrderSearchResults = nlapiSearchRecord('workorder', null, WOFilers,column);

		var maxno=OrderSearchResults[OrderSearchResults.length-1].getValue(columns[1]);		
		fillWOField(form, WOField,maxno);	

	}
}