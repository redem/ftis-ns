/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Client/Attic/ebiz_ReleaseOrders_CL.js,v $
 *     	   $Revision: 1.1.2.1 $
 *     	   $Date: 2013/06/03 07:26:30 $
 *     	   $Author: snimmakayala $
 *     	   $Name: t_NSWMS_2013_1_3_29 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_ReleaseOrders_CL.js,v $
 * Revision 1.1.2.1  2013/06/03 07:26:30  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 *
 *****************************************************************************/

function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}


function backtoreleaseorders()
{	
	var WaveQBURL = nlapiResolveURL('SUITELET', 'customscript_ebiz_release_orders_qb', 'customdeploy_ebiz_release_orders_qb');	
	window.location.href = WaveQBURL;
}

function gotowavestatus()
{
	var WaveQBURL = nlapiResolveURL('SUITELET', 'customscript_waveschstatusreport', 'customdeploy_waveschstatusreport');
	window.location.href = WaveQBURL;
}
function Onchange(type,name ,linenum)
{
	if(trim(name)==trim('custpage_selectpage'))
	{
		nlapiSetFieldValue('custpage_hiddenfieldselectpage','T');
		NLDoMainFormButtonAction("submitter",true);	
	}
	else
	{
		return true;
	}	

}