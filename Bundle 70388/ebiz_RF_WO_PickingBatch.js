/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_WO_PickingBatch.js,v $
 *     	   $Revision: 1.1.2.2.4.10.2.2 $
 *     	   $Date: 2015/11/03 15:33:41 $
 *     	   $Author: grao $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_67 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_WO_PickingBatch.js,v $
 * Revision 1.1.2.2.4.10.2.2  2015/11/03 15:33:41  grao
 * 2015.2 Issue Fixes 201415089
 *
 * Revision 1.1.2.2.4.10.2.1  2015/10/09 15:35:11  deepshikha
 * 2015.2 issue fixes
 * 201414895
 *
 * Revision 1.1.2.2.4.10  2015/08/04 14:05:22  grao
 * 2015.2   issue fixes  20143669
 *
 * Revision 1.1.2.2.4.9  2015/01/27 13:48:54  schepuri
 * issue # 201411437
 *
 * Revision 1.1.2.2.4.8  2014/06/13 08:55:58  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.2.4.7  2014/05/30 00:34:24  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.2.4.6  2014/02/13 15:10:37  sponnaganti
 * case# 20127056
 * For Keyboard Enter button to work
 *
 * Revision 1.1.2.2.4.5  2014/02/05 15:09:49  sponnaganti
 * case# 20127056
 * For Keyboard Enter button to work
 *
 * Revision 1.1.2.2.4.4  2013/06/17 14:08:07  skreddy
 * CASE201112/CR201113/LOG201121
 * Lot Scanning driven by systen rule
 *
 * Revision 1.1.2.2.4.3  2013/06/10 06:18:06  skreddy
 * CASE201112/CR201113/LOG201121
 * Lot Scanning driven by systen rule for PCT
 *
 * Revision 1.1.2.2.4.2  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.2.4.1  2013/03/08 14:38:41  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.1.2.2  2012/12/03 15:41:49  rmukkera
 * CASE201112/CR201113/LOG2012392
 * Override button hide based on condition
 *
 * Revision 1.1.2.1  2012/11/23 09:10:31  skreddy
 * CASE201112/CR201113/LOG201121
 * RF version of WO Assembly building process
 *
 * Revision 1.6.2.6  2012/07/20 15:33:31  spendyala
 * CASE201112/CR201113/LOG201121
 *
 *
 *****************************************************************************/
function PickingBatch(request, response){
	if (request.getMethod() == 'GET') {
		var getFetchedBeginLocation = request.getParameter('custparam_beginLocation');
		var getWaveno = request.getParameter('custparam_waveno');

		var getWOid = request.getParameter('custparam_woid');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocation = request.getParameter('custparam_beginLocation');
		var getItem = request.getParameter('custparam_item');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var getFetchedLocation = request.getParameter('custparam_beginLocationname');
		var vClusterno = request.getParameter('custparam_clusterno');
		var vBatchno = request.getParameter('custparam_batchno');		
		var whLocation = request.getParameter('custparam_whlocation');
		var whCompany = request.getParameter('custparam_whcompany');
		
		var vRecordCount=request.getParameter('custparam_nooflocrecords');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');        
		var getEnteredLocation = request.getParameter('custparam_endlocation');
		var name=request.getParameter('name');
		var ContainerSize=request.getParameter('custparam_containersize');
		var getOrderNo=request.getParameter('custparam_ebizordno');
		nlapiLogExecution('DEBUG', 'getBeginLocation', NextLocation);
		var Itype = nlapiLookupField('item', getItemInternalId, 'recordType');
		var ItemRec = nlapiLoadRecord(Itype, getItemInternalId);
		var getnextExpectedQuantity = request.getParameter('custparam_nextexpectedquantity');// case# 201411437
		nlapiLogExecution('ERROR', 'getnextExpectedQuantity', getnextExpectedQuantity);

		nlapiLogExecution('ERROR', 'getItem', getItem);
		var getItemName = ItemRec.getFieldValue('itemid');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		nlapiLogExecution('DEBUG', 'NextItemInternalId', NextItemInternalId);
		var pickType=request.getParameter('custparam_picktype');
		nlapiLogExecution('ERROR', 'getItem', getItemName);
		nlapiLogExecution('ERROR', 'Next Location', NextLocation);
		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');

		//Code added on 20July 2012
		var vname="Lot Scanning required at Work Order Picking (Valid values Y/N)";
		var LotRequired=GetSystemRuleForLotRequired(vname);

		var SysRuleName="Lot exception allowed at Work Order Picking (Valid values Y/N)";
		var LotValidation=GetSystemRuleForLotRequired(SysRuleName);

		//End of code as of 20July
		var itemstatus=request.getParameter('custparam_itemstatus');
		var itemType=request.getParameter('custparam_itemType');
		// var BinLocationRec = nlapiLoadRecord('customrecord_ebiznet_location', getFetchedBeginLocation);

		//  var getFetchedLocation = BinLocationRec.getFieldValue('custrecord_ebizlocname');
		//   nlapiLogExecution('DEBUG', 'Location Name is', getFetchedLocation);
		//case# 20127056 starts (Now form name is passed correctly in function to work keyboard enter button)
		var functionkeyHtml=getFunctionkeyScript('_ebiz_rf_wo_picking_item');
		//case# 20127056 end
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('venterbatch').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_ebiz_rf_wo_picking_item' method='POST'>";
		html = html + "		<table>";
		/*html = html + "			<tr>";
		html = html + "				<td align = 'left'>ORDER# :<label>" + name + "</label>,WAVE# :<label>" + getWaveno + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";	*/
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>LOT#: <label>" + vBatchno + "</label>";
		html = html + "				<input type='hidden' name='hdnWOid' value=" + getWOid + ">";
		html = html + "				<input type='hidden' name='hdnItemName' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + getFetchedLocation + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vClusterno + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value=" + vBatchno + ">";
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";

		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnwhCompany' value=" + whCompany + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnnext' value=" + NextLocation + ">";
		html = html + "				<input type='hidden' name='hdnName' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + ContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnebizOrdNo' value=" + getOrderNo + ">";
		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEnteredLocation' value=" + getEnteredLocation + ">";
		html = html + "				<input type='hidden' name='hdnNextItemId' value=" + NextItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdnitemstatus' value=" + itemstatus + ">";
		html = html + "				<input type='hidden' name='hdnlocreccount' value=" + vRecordCount + ">";
		html = html + "				<input type='hidden' name='hdnitemtype' value=" + itemType + ">";
		html = html + "				<input type='hidden' name='hdnextExpectedQuantity' value=" + getnextExpectedQuantity + ">";// case# 201411437
		html = html + "				</td>";
		html = html + "			</tr>";
//		html = html + "			<tr>";
//		html = html + "				<td align = 'left'>ENTER/SCAN BATCH/LOT# ";
//		html = html + "				</td>";
//		html = html + "			</tr>";
		if(LotRequired=='Y')
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'><input name='venterbatch' id='venterbatch' type='text'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
		}
		html = html + "			<tr>";
//		html = html + "				<td align = 'left'><input name='enterbatch' type='text'/>";
		html = html + "				<td align = 'left'><input type='hidden' name='enterbatch' value=" + vBatchno+">";
		html = html + "				<td align = 'left'><input type='hidden' name='lotrequired' value=" + LotRequired+">";
		html = html + "				<td align = 'left'><input type='hidden' name='lotvalidation' value=" + LotValidation+">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>SEND <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
		if(LotRequired=='Y'&& LotValidation =='Y')
		{
			html = html + "					OVERRIDE <input name='cmdOverride' type='submit' value='F11'/>";
		}
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('venterbatch').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('ERROR', 'Into Response', 'Into Response');
		var lotrequired=request.getParameter('lotrequired');
		var LotValidation=request.getParameter('lotvalidation');
		if(lotrequired=='Y')
		{
			var vgetEnteredbatch = request.getParameter('venterbatch');
			nlapiLogExecution('ERROR', 'Entered Item', getEnteredbatch);
		}

		else
			var vgetEnteredbatch = request.getParameter('enterbatch');

		nlapiLogExecution('ERROR', 'vgetEnteredbatch', vgetEnteredbatch);

		// check system rule for Lot# Validations
		/*var SysRuleName="Is Lot# Validation required in RF WorkOrder Picking?";
		var LotValidation=GetSystemRuleForLotRequired(SysRuleName);*/
		var getWaveNo = request.getParameter('hdnWaveNo');
		var locreccount=request.getParameter('hdnlocreccount');

		var getEnteredbatch = request.getParameter('enterbatch');
		nlapiLogExecution('ERROR', 'Entered Item', getEnteredbatch);
		var getWOid = request.getParameter('hdnWOid');
		var getRecordInternalId = request.getParameter('hdnRecordInternalId');
		var getContainerLpNo = request.getParameter('hdnContainerLpNo');
		var getExpectedQuantity = request.getParameter('hdnExpectedQuantity');
		var getBeginLocation = request.getParameter('hdnBeginLocation');
		var getItem = request.getParameter('hdnItem');
		var getItemDescription = request.getParameter('hdnItemDescription');
		var getItemInternalId = request.getParameter('hdnItemInternalId');
		var getDOLineId = request.getParameter('hdnDOLineId');
		var getInvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var getOrderLineNo = request.getParameter('hdnOrderLineNo');
		var getItem = request.getParameter('hdnItemName');
		var vClusterNo = request.getParameter('hdnClusterNo');
		var vBatchNo = request.getParameter('hdnbatchno');	
		var whLocation = request.getParameter('hdnwhlocation');
		//SOarray["custparam_whlocation"] = whLocation;
		var whCompany = request.getParameter('hdnwhCompany');
		var RecCount=request.getParameter('hdnRecCount');
		var getNext=request.getParameter('hdnnext');
		var OrdName=request.getParameter('hdnName');
		var ContainerSize=request.getParameter('hdnContainerSize');
		var ebizOrdNo=request.getParameter('hdnebizOrdNo');
		var ItemStatus=request.getParameter('hdnitemstatus');
		var ItemType=request.getParameter('hdnitemtype');
		var nextexpqty=request.getParameter('hdnextExpectedQuantity');
		var pickType=request.getParameter('hdnpicktype');
		var SOarray = new Array();
		SOarray["custparam_whlocation"] = whLocation; 
		SOarray["custparam_nextiteminternalid"] = request.getParameter('hdnNextItemId');
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		var vSkipId=request.getParameter('hdnskipid');
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');
		var optedEvent1 = request.getParameter('cmdOverride');

		// This variable is to hold the SO# entered.


		SOarray["custparam_error"] = 'INVALID LOT#';
		SOarray["custparam_screenno"] = 'WOPickingBatch';
		SOarray["custparam_waveno"] = getWaveNo;
		SOarray["custparam_nooflocrecords"] = locreccount;
		SOarray["custparam_woid"] = getWOid;
		SOarray["custparam_recordinternalid"] = getRecordInternalId;
		SOarray["custparam_containerlpno"] = getContainerLpNo;
		SOarray["custparam_expectedquantity"] = getExpectedQuantity;
		SOarray["custparam_beginLocation"] = getBeginLocation;
		SOarray["custparam_item"] = getItem;
		SOarray["custparam_itemdescription"] = getItemDescription;
		SOarray["custparam_iteminternalid"] = getItemInternalId;
		SOarray["custparam_dolineid"] = getDOLineId;
		SOarray["custparam_invoicerefno"] = getInvoiceRefNo;
		SOarray["custparam_orderlineno"] = getOrderLineNo;
		SOarray["custparam_clusterno"] = vClusterNo;
		SOarray["custparam_batchno"] = getEnteredbatch;	//vBatchNo;
		SOarray["custparam_Actbatchno"] = vBatchNo;
		//SOarray["custparam_Expbatchno"] = vgetEnteredbatch;
		SOarray["custparam_noofrecords"] = RecCount;
		SOarray["custparam_nextlocation"] = getNext;
		SOarray["name"] = OrdName;
		SOarray["custparam_containersize"] = ContainerSize;
		SOarray["custparam_ebizordno"] = ebizOrdNo;
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
		SOarray["custparam_itemstatus"] = ItemStatus;
		SOarray["custparam_itemType"] = ItemType;
		SOarray["custparam_nextexpectedquantity"] = nextexpqty;
		nlapiLogExecution('ERROR', 'Order LineNo is', getOrderLineNo);
		nlapiLogExecution('ERROR', 'woid', SOarray["custparam_woid"]);
		nlapiLogExecution('ERROR', 'vgetEnteredbatch', vgetEnteredbatch);
		nlapiLogExecution('ERROR', 'nextexpqty', SOarray["custparam_nextexpectedquantity"]);
		nlapiLogExecution('ERROR', 'Order getWaveNo is', getWaveNo);

		// Fetch the actual location based on the begin location value that is fetched and passed as a parameter
		// var BinLocationRec = nlapiLoadRecord('customrecord_ebiznet_location', getBeginLocation);

		// getBeginBinLocation = BinLocationRec.getFieldTx('custrecord_ebizlocname');
		//  nlapiLogExecution('DEBUG', 'Location Name is', getBeginBinLocation);
		/*       
        getBeginLocationInternalId = BinLocationRec.getId();
        nlapiLogExecution('DEBUG', 'Begin Location Internal Id', getBeginLocationInternalId);
		 */        
		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept SO #.
		if (optedEvent == 'F7') {
			//response.sendRedirect('SUITELET', 'customscript_rf_picking_container_no', 'customdeploy_rf_picking_container_no_di', false, SOarray);
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_item', 'customdeploy_ebiz_rf_wo_picking_item_di', false, SOarray);
		}
		else {
			if (optedEvent1 == 'F11'){
				if(vgetEnteredbatch!=null&&vgetEnteredbatch!="")
				{
					if((vgetEnteredbatch!=getEnteredbatch)&& (LotValidation =='Y'))
					{
						nlapiLogExecution('ERROR', 'override', 'override');
						var result=GetBatchId(getItemInternalId,vgetEnteredbatch);
						var BatchId=result[0];
						var IsValidflag=result[1];
						if(IsValidflag=='T')
						{
							var resultArray=IsBatchNoValid(SOarray["custparam_endlocinternalid"],getItemInternalId,getExpectedQuantity,ItemStatus,BatchId);
							if(resultArray.length>0){
								var IsValidBatchForBinLoc=resultArray[0];
								var NewInvtRecId=resultArray[1];
								var NewLp=resultArray[2];
								var NewPackcode=resultArray[3];
								var NewItemStatus=resultArray[4];

								nlapiLogExecution('ERROR', 'IsValidBatchForBinLoc', IsValidBatchForBinLoc);
								nlapiLogExecution('ERROR', 'NewInvtRecId', NewInvtRecId);
								nlapiLogExecution('ERROR', 'NewLp', NewLp);
								nlapiLogExecution('ERROR', 'getInvoiceRefNo', getInvoiceRefNo);
								nlapiLogExecution('ERROR', 'vgetEnteredbatch', vgetEnteredbatch);
								nlapiLogExecution('ERROR', 'getExpectedQuantity', getExpectedQuantity);
								nlapiLogExecution('ERROR', 'getRecordInternalId', getRecordInternalId);
								if(IsValidBatchForBinLoc=='T')
								{				
									//update open task with new lot# and Invtrefno
									var UpdateOpentask = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', getRecordInternalId);
									UpdateOpentask.setFieldValue('custrecord_batch_no', vgetEnteredbatch);
									UpdateOpentask.setFieldValue('custrecord_lpno', NewLp);
									UpdateOpentask.setFieldValue('custrecord_packcode', NewPackcode);
									UpdateOpentask.setFieldValue('custrecord_sku_status', NewItemStatus);
									UpdateOpentask.setFieldValue('custrecord_invref_no',NewInvtRecId); 
									nlapiSubmitRecord(UpdateOpentask, false, true);

									//deallocate old lot# allocated Qty in create inventory					
									var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv',getInvoiceRefNo);
									var qty = transaction.getFieldValue('custrecord_ebiz_qoh');
									var allocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
									var vLP=transaction.getFieldValue('custrecord_ebiz_inv_lp');
									var vPackcode=transaction.getFieldValue('custrecord_ebiz_inv_packcode');
									var vSKUStatus=transaction.getFieldValue('custrecord_ebiz_inv_sku_status');

									nlapiLogExecution('ERROR', 'qty', qty);
									nlapiLogExecution('ERROR', 'allocqty', allocqty);
									nlapiLogExecution('ERROR', 'getExpectedQuantity', getExpectedQuantity);
									transaction.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(allocqty)- parseFloat(getExpectedQuantity)).toFixed(4));
									nlapiSubmitRecord(transaction, false, true);


									//update allocated qty to new Lot# in create inventory
									var InvtDetails = nlapiLoadRecord('customrecord_ebiznet_createinv', NewInvtRecId);
									var Invtqty = InvtDetails.getFieldValue('custrecord_ebiz_qoh');
									var Invtallocqty = InvtDetails.getFieldValue('custrecord_ebiz_alloc_qty');
									var InvtvLP=InvtDetails.getFieldValue('custrecord_ebiz_inv_lp');
									var InvtvPackcode=InvtDetails.getFieldValue('custrecord_ebiz_inv_packcode');

									InvtDetails.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(Invtallocqty)+ parseFloat(getExpectedQuantity)).toFixed(4));
									nlapiSubmitRecord(InvtDetails, false, true);


									SOarray["custparam_containerlpno"] = NewLp;
									SOarray["custparam_itemstatus"] = NewItemStatus;
									SOarray["custparam_batchno"] = vgetEnteredbatch;
									//SOarray["custparam_entloc"] = getBeginLocation;
									SOarray["custparam_recid"] = NewInvtRecId;
									//SOarray["custparam_entlocid"] = SOarray["custparam_endlocinternalid"]; 
									SOarray["custparam_Exc"] = 'L';
									SOarray["custparam_Expbatchno"] = vgetEnteredbatch;
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_confirm', 'customdeploy_ebiz_rf_wo_pick_confirm_di', false, SOarray);
									nlapiLogExecution('ERROR', 'Done customrecord1', 'Success');
								}
								else
								{
									SOarray["custparam_error"] = 'INVENTORY IS NOT AVAILABLE FOR THE LOT#';
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
									nlapiLogExecution('ERROR', 'Error: ', 'Does not belong to the bin location');
								}
							}
							else
							{
								SOarray["custparam_error"] = 'INVENTORY IS NOT AVAILABLE FOR THE LOT#';
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								nlapiLogExecution('ERROR', 'Error: ', 'Scanned batch is invalid');

							}
						}
						else 
						{
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							nlapiLogExecution('ERROR', 'Error: ', 'Scanned batch is invalid');
						}
					}
					else if((vgetEnteredbatch!=getEnteredbatch)&& (LotValidation =='N'))
					{
						SOarray["custparam_error"] = 'INVALID LOT SCANNED';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						nlapiLogExecution('ERROR', 'Error: ', 'Lotvalidation is Yes');						
					}
					else if(pickType=='CL')
					{
						nlapiLogExecution('ERROR', 'cl f11 ');
						SOarray["custparam_picktype"] = pickType;
						response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_summtask', 'customdeploy_rf_wocluspicking_summtask', false, SOarray);
							
					}
					else
					{
						SOarray["custparam_Expbatchno"] = vBatchNo;
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_confirm', 'customdeploy_ebiz_rf_wo_pick_confirm_di', false, SOarray);
						nlapiLogExecution('ERROR', 'Done customrecord', 'Success');		
					}
				}
				else
				{
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					nlapiLogExecution('ERROR', 'Error: ', 'Scanned batch is invalid');
				}
			}
			//if (getEnteredLocation != '' && getEnteredLocation == getBeginBinLocation) {
			else{			// && getEnteredbatch == vBatchNo) {

				if(vgetEnteredbatch!=getEnteredbatch)
				{
					//SOarray["custparam_error"] = 'ENTERED BATCH NO IS DIFFERENT FROM ACTUAL BATCH NO';
					SOarray["custparam_error"] = 'INVALID LOT SCANNED';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					nlapiLogExecution('ERROR', 'Error: ', 'Scanned batch is invalid');

				}
				else if(pickType=='CL')
				{
					nlapiLogExecution('ERROR', 'cl overide ');	
					SOarray["custparam_picktype"] = pickType;
					response.sendRedirect('SUITELET', 'customscript_rf_wocluspicking_summtask', 'customdeploy_rf_wocluspicking_summtask', false, SOarray);
					
				}
				else
					{
					SOarray["custparam_Expbatchno"] = vBatchNo;
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_confirm', 'customdeploy_ebiz_rf_wo_pick_confirm_di', false, SOarray);
					nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
				}				
			}
		}
	}
}



function GetSystemRuleForLotRequired(vname)
{
	try
	{
		nlapiLogExecution('ERROR','vname',vname);
		var rulevalue='N';
		var filter=new Array();
		//filter.push(new nlobjSearchFilter('name',null,'is','Is Lot# Scan required in RF Picking?'));
		filter.push(new nlobjSearchFilter('name',null,'is',vname));
		filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));


		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizrulevalue');

		var searchresult=nlapiSearchRecord('customrecord_ebiznet_sysrules',null,filter,column);

		if(searchresult!=null&&searchresult!="")
			rulevalue=searchresult[0].getValue('custrecord_ebizrulevalue');
		nlapiLogExecution('ERROR','rulevalue',rulevalue);
		return rulevalue;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetSystemRuleForLPRequired',exp);
	}
}

function GetBatchId(itemid,getEnteredbatch)
{
	try
	{
		var globalArray=new Array();
		var BatchID;
		var flag='F';
		var filter=new Array();
		if(itemid!=null&&itemid!="")
			filter.push(new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',itemid));
		filter.push(new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',getEnteredbatch));
		var rec=nlapiSearchRecord('customrecord_ebiznet_batch_entry',null,filter,null);
		if(rec!=null&&rec!="")
		{
			BatchID=rec[0].getId();
			flag='T';
		}
		globalArray.push(BatchID);
		globalArray.push(flag);
		nlapiLogExecution('ERROR','globalArray',globalArray);
		return globalArray;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetBatchId',exp);
	}
}


function IsBatchNoValid(BeginLocationId,ItemId,getExpectedQuantity,ItemStatus,BatchId)
{
	try
	{
		nlapiLogExecution('ERROR','BeginLocationId',BeginLocationId);
		nlapiLogExecution('ERROR','ItemId',ItemId);
		nlapiLogExecution('ERROR','getExpectedQuantity',getExpectedQuantity);
		nlapiLogExecution('ERROR','ItemStatus',ItemStatus);
		nlapiLogExecution('ERROR','BatchId',BatchId);

		var summaryArray=new Array();
		var tempflag='F';
		var recid="";
		var Lp="";
		var ItemStatus="";
		var Packcode="";
		var filter=new Array();
		filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc',null,'anyof',BeginLocationId));
		filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku',null,'anyof',ItemId));
		filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot',null,'anyof',BatchId));
		//filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status',null,'anyof',ItemStatus));
		filter.push(new nlobjSearchFilter('custrecord_ebiz_avl_qty',null, 'greaterthan', getExpectedQuantity));
		filter.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19','3']));


		var Columns = new Array();
		Columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
		Columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_packcode');
		Columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_sku_status');


		var searchrecord = nlapiSearchRecord('customrecord_ebiznet_createinv',null,filter,Columns);

		if(searchrecord!=null && searchrecord!="")
		{
			nlapiLogExecution('ERROR','searchrecord',searchrecord.length);
			tempflag='T';
			recid=searchrecord[0].getId();
			Lp=searchrecord[0].getValue('custrecord_ebiz_inv_lp');
			ItemStatus=searchrecord[0].getValue('custrecord_ebiz_inv_sku_status');
			Packcode=searchrecord[0].getValue('custrecord_ebiz_inv_packcode');
		}
		summaryArray.push(tempflag);
		summaryArray.push(recid);
		summaryArray.push(Lp);
		summaryArray.push(Packcode);
		summaryArray.push(ItemStatus);

		nlapiLogExecution('ERROR','summaryArray',summaryArray);
		return summaryArray;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in IsBatchNoValid',exp);
	}
}
