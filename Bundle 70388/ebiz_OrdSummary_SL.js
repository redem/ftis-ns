/***************************************************************************
	  		   eBizNET Solutions Inc               
 ****************************************************************************/
/* Prologue - 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Suitelet/ebiz_OrdSummary_SL.js,v $
 *     	   $Revision: 1.6.4.2.4.1.4.4 $
 *     	   $Date: 2013/09/12 15:44:21 $
 *     	   $Author: nneelam $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_OrdSummary_SL.js,v $
 * Revision 1.6.4.2.4.1.4.4  2013/09/12 15:44:21  nneelam
 * Case#. 20124379�
 * Order Summary Details  Issue Fix..
 *
 * Revision 1.6.4.2.4.1.4.3  2013/09/10 15:30:52  nneelam
 * Case#.20124296��
 * Order Summary  Issue Fix..
 *
 * Revision 1.6.4.2.4.1.4.2  2013/05/08 15:11:46  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.6.4.2.4.1.4.1  2013/05/06 15:44:04  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.6.4.2.4.1  2012/11/01 14:55:15  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.6.4.2  2012/09/03 13:54:56  schepuri
 * CASE201112/CR201113/LOG201121
 * added date stamp
 *
 * Revision 1.6.4.1  2012/02/28 14:00:04  spendyala
 * CASE201112/CR201113/LOG201121
 * Updated the source to enhance readability and understandability.
 *
 * Revision 1.6  2011/10/25 07:58:39  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.5  2011/10/25 06:47:25  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.4  2011/10/24 12:44:22  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.3  2011/10/24 10:37:34  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.2  2011/10/24 06:15:23  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.1  2011/10/20 18:05:08  mbpragada
 * CASE201112/CR201113/LOG201121
 *

 *****************************************************************************/
function ebiznet_OrdSummary(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Order Summary Report');
		var vQbWave = "";

form.setScript('customscript_inventoryclientvalidations');

		var fromdate = form.addField('custpage_fromdate', 'date', 'From Date');
		var sysdate=DateStamp();
		fromdate.setDefaultValue(sysdate);


//		var fulfillordfield = form.addField('custpage_fulfillmentorder', 'select', 'Fulfillment Order Status');
//		fulfillordfield.setLayoutType('startrow', 'none');
//		fulfillordfield.addSelectOption("", "");

//		var fulfillordfilters = new Array();
//		fulfillordfilters[0] = new nlobjSearchFilter('custrecord_ebiz_process', null, 'anyof', ['2']);  		

//		var fulfillordsearchresults = nlapiSearchRecord('customrecord_wms_status_flag', null, fulfillordfilters, new nlobjSearchColumn('name'));		
//		if (fulfillordsearchresults != null) {
//		for (var i = 0; i < fulfillordsearchresults.length; i++) {

//		var res=  form.getField('custpage_fulfillmentorder').getSelectOptions(fulfillordsearchresults[i].getValue('name'), 'is');
//		if (res != null) {				
//		if (res.length > 0) {
//		continue;
//		}
//		}

//		fulfillordfield.addSelectOption(fulfillordsearchresults[i].getId(), fulfillordsearchresults[i].getValue('name'));
//		}
//		}
		var todate = form.addField('custpage_todate', 'date', 'To Date');	
		//var printfield = form.addField('custpage_printed', 'checkbox', 'Printed');
		todate.setDefaultValue(sysdate);

		var location = form.addField('custpage_location', 'select', 'Location');

		var filterLocation = new Array();
		filterLocation.push(new nlobjSearchFilter('custrecord_ebizwhsite', null, 'is', 'T'));
		var columnLocation = new Array();
		columnLocation[0] = new nlobjSearchColumn('name');
		location.addSelectOption("", "");
		var searchLocationRecord = nlapiSearchRecord('Location', null, filterLocation, columnLocation);
		for(var count=0;count < searchLocationRecord.length ;count++)
		{
			location.addSelectOption(searchLocationRecord[count].getId(),searchLocationRecord[count].getValue('name'));

		}
		location.setMandatory(true ); 
		form.addSubmitButton('Display');

		response.writePage(form);
	}
	else //this is the POST block
	{
		var createdsoord=0,createdsolines=0,Waveprintedord=0,Waveprintedsolines=0,PickTicketsolines=0,PickTicketord=0,
		fulfillsoord=0,fulfillsolines=0,inpickssoord=0,inpickssolines=0,pickconfirmedsoord=0,
		pickconfirmedsolines=0,Shippedsoord=0,Shippedsolines=0;
		var form = nlapiCreateForm('Order Summary Report');
		form.setScript('customscript_inventoryclientvalidations');
		var vQbfromdate = request.getParameter('custpage_fromdate');
		var vQbtodate = request.getParameter('custpage_todate');
		var vQblocation = request.getParameter('custpage_location');

//		var vQbordstatus = request.getParameter('custpage_fulfillmentorder');
//		var vQbprintflag = request.getParameter('custpage_printed');
//		nlapiLogExecution('ERROR', 'vQbordstatus', vQbordstatus);
//		nlapiLogExecution('ERROR', 'vQbprintflag', vQbprintflag);

		var fromdate = form.addField('custpage_fromdate', 'date', 'From Date').setDefaultValue(vQbfromdate);	

//		var fulfillordfield = form.addField('custpage_fulfillmentorder', 'select', 'Fulfillment Order Status');
//		fulfillordfield.setLayoutType('startrow', 'none');
//		fulfillordfield.addSelectOption("", "");

//		var fulfillordfilters = new Array();
//		fulfillordfilters[0] = new nlobjSearchFilter('custrecord_ebiz_process', null, 'anyof', ['2']);  		

//		var fulfillordsearchresults = nlapiSearchRecord('customrecord_wms_status_flag', null, fulfillordfilters, new nlobjSearchColumn('name'));		
//		if (fulfillordsearchresults != null) {
//		for (var i = 0; i < fulfillordsearchresults.length; i++) {

//		var res=  form.getField('custpage_fulfillmentorder').getSelectOptions(fulfillordsearchresults[i].getValue('name'), 'is');
//		if (res != null) {				
//		if (res.length > 0) {
//		continue;
//		}
//		}

//		fulfillordfield.addSelectOption(fulfillordsearchresults[i].getId(), fulfillordsearchresults[i].getValue('name'));
//		}
//		}
		var todate = form.addField('custpage_todate', 'date', 'To Date').setDefaultValue(vQbtodate);
		//var printfield = form.addField('custpage_printed', 'checkbox', 'Printed');

		var location = form.addField('custpage_location', 'select', 'Location');

		var filterLocation = new Array();
		filterLocation.push(new nlobjSearchFilter('custrecord_ebizwhsite', null, 'is', 'T'));
		var columnLocation = new Array();
		columnLocation[0] = new nlobjSearchColumn('name');
		location.addSelectOption("", "");
		var searchLocationRecord = nlapiSearchRecord('Location', null, filterLocation, columnLocation);
		for(var count=0;count < searchLocationRecord.length ;count++)
		{
			location.addSelectOption(searchLocationRecord[count].getId(),searchLocationRecord[count].getValue('name'));

		}
		location.setMandatory(true ); 
		location.setDefaultValue(vQblocation);
		form.addSubmitButton('Display');

		nlapiLogExecution('ERROR', 'vQbfromdate', vQbfromdate);
		nlapiLogExecution('ERROR', 'vQbtodate', vQbtodate);
		
		//Modified on 28Feb 2012 by suman.
		//Modified the source to enhance readability and understandability.
		
		//	beginning of sublist filters

		//To get sales orders count
		createdsoord=GetSoOrder(request);

		//To get sales orders line count
		createdsolines=GetSOorderLines(request);

		//To get fulfillment orders and order lines count 
		var FulfillRec=GetFulfillmentOrder(request);
		fulfillsoord=FulfillRec[0];
		fulfillsolines=FulfillRec[1];

		//To get in pick orders and  lines count 
		var PickRec=GetPickOrder(request);
		inpickssoord=PickRec[0];
		inpickssolines=PickRec[1];

		//To get pick confirmed orders and  lines count 
		var PickConfirmedRec=GetPickConfirmedOrder(request);
		pickconfirmedsoord=PickConfirmedRec[0];
		pickconfirmedsolines=PickConfirmedRec[1];

		//To get shipped orders and  lines count
		var ShipRec=GetShipOrder(request);
		Shippedsoord=ShipRec[0];
		Shippedsolines=ShipRec[1];

		//To get  In Wave but not printed orders and lines count
		var WaveRec=GetWaveOrder_NotPrinted(request);
		Waveprintedord=WaveRec[0];
		Waveprintedsolines=WaveRec[1];

		// To get Pick Tickets printed orders and lines count
		var PickTicketRec=GetPickTicketOrder(request);
		PickTicketord=PickTicketRec[0];
		PickTicketsolines=PickTicketRec[1];

		
		//End of code modification as of 28Feb 2012.
		
		//Adding sublist
		var sublist = form.addSubList("custpage_items", "staticlist", "ItemList");

		sublist.addField("custpage_null", "text", " ");
		sublist.addField("custpage_so", "text", "SO Orders");
		sublist.addField("custpage_soline", "text", "SO Order Lines");	
		var rowname='',col1='',col2='';
		for (var i = 0;  i <=5 ; i++) {
			if(i == 0){
				rowname='Created';
				col1=createdsoord.toString();
				col2=createdsolines;
			}
			else if(i == 1){
				rowname='Fulfillment Orders';
				col1=fulfillsoord.toString();
				col2=fulfillsolines.toString();	
			}
//			else if(i == 2){
//			rowname='In Picks';
//			col1=inpickssoord;
//			col2=inpickssolines;}
			else if(i == 4){
				rowname='In Picks Confirmed';
				col1=pickconfirmedsoord.toString();
				col2=pickconfirmedsolines.toString();}
			else if(i == 5){
				rowname='Shipped';
				col1=Shippedsoord.toString();
				col2=Shippedsolines.toString();}
			else if(i == 2){
				rowname='In Wave but not printed';
				col1=Waveprintedord.toString();
				col2=Waveprintedsolines.toString();}
			else if(i == 3){
				rowname='  Pick Tickets printed';
				col1=PickTicketord.toString();
				col2=PickTicketsolines.toString();}

			form.getSubList('custpage_items').setLineItemValue('custpage_null',  i+1, rowname);
			form.getSubList('custpage_items').setLineItemValue('custpage_so', i+1, col1);
			form.getSubList('custpage_items').setLineItemValue('custpage_soline',i+1, col2);
		}
		response.writePage(form);
	}
}
/**
 * Function to return the current date in mm/dd/yyyy format
 * @author Phani
 * @returns Current system date
 */
/*function DateStamp(){
	var now = new Date();
	return ((parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
}*/


/**
 * @param request
 * @returns {Number}
 */
function GetSoOrder(request)
{
	try{
		nlapiLogExecution('ERROR', 'request.getParameter(custpage_fromdate)',request.getParameter('custpage_fromdate'));
		var createdsoord=0;
		var salessearchfilters = new Array();
		if ((request.getParameter('custpage_fromdate') != "" && request.getParameter('custpage_fromdate') != null) && (request.getParameter('custpage_todate') != "" && request.getParameter('custpage_todate') != null) ) {

			salessearchfilters.push(new nlobjSearchFilter('trandate', null, 'within', request.getParameter('custpage_fromdate'), request.getParameter('custpage_todate')));			 
		} 
		if(request.getParameter('custpage_location') != null && request.getParameter('custpage_location') != ""){
			salessearchfilters.push(new nlobjSearchFilter('location', null, 'is', request.getParameter('custpage_location')));}
		var salesColumns = new Array();
		salesColumns[0] = new nlobjSearchColumn('tranid',null, 'count');
		//salesColumns[1] = new nlobjSearchColumn('line',null, 'count');

		var salessearchresults = nlapiSearchRecord('salesorder', null, salessearchfilters, salesColumns);

		for (var i = 0; salessearchresults != null && i < salessearchresults.length; i++) {
			createdsoord = salessearchresults[i].getValue('tranid', null, 'count');
			//createdsolines = salessearchresults[i].getValue('line', null, 'count');
			//createdsolines=salessearchresults[i].getLineItemCount('item');
			nlapiLogExecution('ERROR', 'createdsoord', createdsoord);
		}
		if(salessearchresults != null && salessearchresults !='')
		{
			
				createdsoord = parseInt(createdsoord) + 1;
			
		}
		return createdsoord;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception In GetSoOrder',exp);
	}
}

/**
 * @param request
 * @returns {Number}
 */
function GetSOorderLines(request)
{
	try
	{
		var createdsolines=0;
		var filters = new Array();
		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));						// Retrieve order lines
		filters.push(new nlobjSearchFilter('quantitycommitted', null, 'greaterthan', 0));		// Committed Quantity > 0

		// Pending Fulfillment / Partially Fulfilled / Pending Billing_Partially Fulfilled
		filters.push(new nlobjSearchFilter('status', null, 'anyof', ['SalesOrd:B','SalesOrd:D','SalesOrd:E']));
		if ((request.getParameter('custpage_fromdate') != "" && request.getParameter('custpage_fromdate') != null) && (request.getParameter('custpage_todate') != "" && request.getParameter('custpage_todate') != null) ) {

			filters.push(new nlobjSearchFilter('trandate', null, 'within', request.getParameter('custpage_fromdate'), request.getParameter('custpage_todate')));			 
		} 
		if(request.getParameter('custpage_location') != null && request.getParameter('custpage_location') != ""){
			filters.push(new nlobjSearchFilter('location', null, 'is', request.getParameter('custpage_location')));}
		
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('line');

		var salesOrderLineDetails =  nlapiSearchRecord('salesorder', null, filters, columns);
		if(salesOrderLineDetails != null && salesOrderLineDetails != ""){
			nlapiLogExecution('ERROR', 'salesOrderLineDetails', salesOrderLineDetails.length);
			createdsolines=(salesOrderLineDetails.length).toString();
		}
		return createdsolines;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception In GetSOorderLines',exp);
	}
}

/**
 * @param request
 * @returns {Array}
 */
function GetFulfillmentOrder(request)
{
	try
	{
		var fulfillsoord=0;
		var fulfillsolines=0;
		var recCount=new Array();
		var fullfilmentfilters = new Array();
		if ((request.getParameter('custpage_fromdate') != "" && request.getParameter('custpage_fromdate') != null) && (request.getParameter('custpage_todate') != "" && request.getParameter('custpage_todate') != null) ) {

			fullfilmentfilters.push(new nlobjSearchFilter('custrecord_record_linedate', null, 'within', request.getParameter('custpage_fromdate'), request.getParameter('custpage_todate')));			 
		} 
		//fullfilmentfilters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'is', '25'));
		if(request.getParameter('custpage_location') != null && request.getParameter('custpage_location') != ""){
			fullfilmentfilters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'is', request.getParameter('custpage_location')));}

		var fullfilmentColumns = new Array();
		fullfilmentColumns[0] = new nlobjSearchColumn('custrecord_ns_ord',null, 'count');
		fullfilmentColumns[1] = new nlobjSearchColumn('id',null, 'count');

		var fullfilmentsearchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, fullfilmentfilters, fullfilmentColumns);

		for (var i = 0; fullfilmentsearchresults != null && i < fullfilmentsearchresults.length; i++) {
			fulfillsoord = fullfilmentsearchresults[i].getValue('custrecord_ns_ord', null, 'count');
			fulfillsolines = fullfilmentsearchresults[i].getValue('id', null, 'count');
			nlapiLogExecution('ERROR', 'fulfillsoord', fulfillsoord);
			nlapiLogExecution('ERROR', 'fulfillsolines', fulfillsolines);
		}
		if(fullfilmentsearchresults != null && fullfilmentsearchresults !='')
		{
			fulfillsoord = parseInt(fulfillsoord) + 1;
			fulfillsolines = parseInt(fulfillsolines) + 1;
		}
		
		recCount[0]=fulfillsoord;
		recCount[1]=fulfillsolines;
		return recCount;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception In GetFulfillmentOrder',exp);
	}
}

/**
 * @param request
 * @returns {Array}
 */
function GetPickOrder(request)
{
	try
	{
		var inpickssoord=0;
		var inpickssolines=0;
		var recCount=new Array();
		var inpicksfilters = new Array();
		if ((request.getParameter('custpage_fromdate') != "" && request.getParameter('custpage_fromdate') != null) && (request.getParameter('custpage_todate') != "" && request.getParameter('custpage_todate') != null) ) {

			inpicksfilters.push(new nlobjSearchFilter('custrecord_record_linedate', null, 'within', request.getParameter('custpage_fromdate'), request.getParameter('custpage_todate')));			 
		} 
		if(request.getParameter('custpage_location') != null && request.getParameter('custpage_location') != ""){
			inpicksfilters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'is', request.getParameter('custpage_location')));}
		inpicksfilters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'is', '9'));

		var inpicksColumns = new Array();
		inpicksColumns[0] = new nlobjSearchColumn('custrecord_ns_ord',null, 'count');
		inpicksColumns[1] = new nlobjSearchColumn('id',null, 'count');

		var inpickssearchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, inpicksfilters, inpicksColumns);

		for (var i = 0; inpickssearchresults != null && i < inpickssearchresults.length; i++) {
			inpickssoord = inpickssearchresults[i].getValue('custrecord_ns_ord', null, 'count');
			inpickssolines = inpickssearchresults[i].getValue('id', null, 'count');

			nlapiLogExecution('ERROR', 'inpickssoord', inpickssoord);
			nlapiLogExecution('ERROR', 'inpickssolines', inpickssolines);
		}	
		if(inpickssearchresults != null && inpickssearchresults !='')
		{
			//case # 20124296 Start
			
			inpickssoord = parseInt(inpickssoord) + 1;
			inpickssolines = parseInt(inpickssolines) + 1;
			// case # 20124296 End
		}
		
		recCount[0]=inpickssoord;
		recCount[1]=inpickssolines;
		return recCount;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception In GetPickOrder',exp);
	}
}

/**
 * @param request
 * @returns {Array}
 */
function GetPickConfirmedOrder(request)
{
	try 
	{
		var pickconfirmedsoord=0;
		var pickconfirmedsolines=0;
		var recCount=new Array();
		var confirmedfilters = new Array();
		if ((request.getParameter('custpage_fromdate') != "" && request.getParameter('custpage_fromdate') != null) && (request.getParameter('custpage_todate') != "" && request.getParameter('custpage_todate') != null) ) {

			confirmedfilters.push(new nlobjSearchFilter('custrecordact_begin_date', null, 'within', request.getParameter('custpage_fromdate'), request.getParameter('custpage_todate')));			 
		} 

		confirmedfilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8,28]));//28=STATUS.OUTBOUND.PACK_COMPLETE,8=STATUS.OUTBOUND.PICK_CONFIRMED.
		if(request.getParameter('custpage_location') != null && request.getParameter('custpage_location') != ""){
			confirmedfilters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'is', request.getParameter('custpage_location')));}
		var confirmeColumns = new Array();
		confirmeColumns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no',null, 'count');
		confirmeColumns[1] = new nlobjSearchColumn('internalid',null, 'count');

		var confirmesearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, confirmedfilters, confirmeColumns);

		for (var i = 0; confirmesearchresults != null && i < confirmesearchresults.length; i++) {
			pickconfirmedsoord = confirmesearchresults[i].getValue('custrecord_ebiz_order_no', null, 'count');
			pickconfirmedsolines = confirmesearchresults[i].getValue('internalid', null, 'count');

			nlapiLogExecution('ERROR', 'pickconfirmedsoord', pickconfirmedsoord);
			nlapiLogExecution('ERROR', 'pickconfirmedsolines', pickconfirmedsolines);
		}
		
		if(confirmesearchresults != null && confirmesearchresults !='')
		{
			pickconfirmedsoord = parseInt(pickconfirmedsoord) + 1;
			pickconfirmedsolines = parseInt(pickconfirmedsolines) + 1;
		}
		recCount[0]=pickconfirmedsoord;
		recCount[1]=pickconfirmedsolines;
		return recCount;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception In GetPickConfirmedOrder',exp);
	}
}

/**
 * @param request
 * @returns {Array}
 */
function GetShipOrder(request)
{
	try
	{
		var Shippedsoord=0;
		var Shippedsolines=0;
		var recCount=new Array();
		var shippedfilters = new Array();
		if ((request.getParameter('custpage_fromdate') != "" && request.getParameter('custpage_fromdate') != null) && (request.getParameter('custpage_todate') != "" && request.getParameter('custpage_todate') != null) ) {

			shippedfilters.push(new nlobjSearchFilter('custrecord_upddate', null, 'within', request.getParameter('custpage_fromdate'), request.getParameter('custpage_todate')));			 
		} 

		shippedfilters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'is', '14'));
		if(request.getParameter('custpage_location') != null && request.getParameter('custpage_location') != ""){
			shippedfilters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'is', request.getParameter('custpage_location')));}
		var shippedColumns = new Array();
		shippedColumns[0] = new nlobjSearchColumn('custrecord_ns_ord',null, 'count');
		shippedColumns[1] = new nlobjSearchColumn('id',null, 'count');

		var shippedsearchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, shippedfilters, shippedColumns);

		for (var i = 0; shippedsearchresults != null && i < shippedsearchresults.length; i++) {
			Shippedsoord = shippedsearchresults[i].getValue('custrecord_ns_ord', null, 'count');
			Shippedsolines = shippedsearchresults[i].getValue('id', null, 'count');

			nlapiLogExecution('ERROR', 'Shippedsoord', Shippedsoord);
			nlapiLogExecution('ERROR', 'Shippedsolines', Shippedsolines);
		}
		if(shippedsearchresults != null && shippedsearchresults !='')
		{
			Shippedsoord = parseInt(Shippedsoord) + 1;
			Shippedsolines = parseInt(Shippedsolines) + 1;
		}
		recCount[0]=Shippedsoord;
		recCount[1]=Shippedsolines;
		return recCount;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception In GetShipOrder',exp);
	}
}

/**
 * @param request
 * @returns {Array}
 */
function GetWaveOrder_NotPrinted(request)
{
	try 
	{
		var Waveprintedord=0;
		var Waveprintedsolines=0;
		var recCount=new Array();
		var Waveprintedfilters = new Array();
		if ((request.getParameter('custpage_fromdate') != "" && request.getParameter('custpage_fromdate') != null) && (request.getParameter('custpage_todate') != "" && request.getParameter('custpage_todate') != null) ) {

					Waveprintedfilters.push(new nlobjSearchFilter('custrecord_upddate', null, 'within', request.getParameter('custpage_fromdate'), request.getParameter('custpage_todate')));
		} 
		Waveprintedfilters.push(new nlobjSearchFilter('custrecord_printflag', null, 'is', 'F'));
		Waveprintedfilters.push(new nlobjSearchFilter('custrecord_ebiz_wave', null, 'noneof', '@NONE@'));		
		if(request.getParameter('custpage_location') != null && request.getParameter('custpage_location') != ""){
			Waveprintedfilters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'is', request.getParameter('custpage_location')));}
		var WaveprintedColumns = new Array();
		WaveprintedColumns[0] = new nlobjSearchColumn('custrecord_ns_ord',null, 'count');
		WaveprintedColumns[1] = new nlobjSearchColumn('id',null, 'count');

		var Waveprintedsearchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, Waveprintedfilters, WaveprintedColumns);

		for (var i = 0; Waveprintedsearchresults != null && i < Waveprintedsearchresults.length; i++) {
			Waveprintedord = Waveprintedsearchresults[i].getValue('custrecord_ns_ord', null, 'count');
			Waveprintedsolines = Waveprintedsearchresults[i].getValue('id', null, 'count');

			nlapiLogExecution('ERROR', 'Waveprintedord', Waveprintedord);
			nlapiLogExecution('ERROR', 'Waveprintedsolines', Waveprintedsolines);
		}
		if(Waveprintedsearchresults != null && Waveprintedsearchresults !='')
		{
			Waveprintedord = parseInt(Waveprintedord) + 1;
			Waveprintedsolines = parseInt(Waveprintedsolines) + 1;
		}
		
		recCount[0]=Waveprintedord;
		recCount[1]=Waveprintedsolines;
		return recCount;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception In GetWaveOrder_NotPrinted',exp);
	}
}
/**
 * @param request
 * @returns {Array}
 */
function GetPickTicketOrder(request)
{
	try
	{
		var PickTicketsolines=0;
		var PickTicketord=0;
		var recCount=new Array();
		var PickTicketsfilters = new Array();
		if ((request.getParameter('custpage_fromdate') != "" && request.getParameter('custpage_fromdate') != null) && (request.getParameter('custpage_todate') != "" && request.getParameter('custpage_todate') != null) ) {
              //case # 20124379� Start
			PickTicketsfilters.push(new nlobjSearchFilter('custrecord_ebiz_pr_dateprinted', null, 'within', request.getParameter('custpage_fromdate'), request.getParameter('custpage_todate')));			 
			 //case # 20124379� End
		} 
		PickTicketsfilters.push(new nlobjSearchFilter('custrecord_printflag', null, 'is', 'T'));
		if(request.getParameter('custpage_location') != null && request.getParameter('custpage_location') != ""){
			PickTicketsfilters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'is', request.getParameter('custpage_location')));}

		var PickTicketColumns = new Array();
		PickTicketColumns[0] = new nlobjSearchColumn('custrecord_ns_ord',null, 'count');
		PickTicketColumns[1] = new nlobjSearchColumn('id',null, 'count');

		var PickTicketearchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, PickTicketsfilters, PickTicketColumns);

		for (var i = 0; PickTicketearchresults != null && i < PickTicketearchresults.length; i++) {
			PickTicketord = PickTicketearchresults[i].getValue('custrecord_ns_ord', null, 'count');
			PickTicketsolines = PickTicketearchresults[i].getValue('id', null, 'count');
			nlapiLogExecution('ERROR', 'PickTicketord', PickTicketord);
			nlapiLogExecution('ERROR', 'PickTicketsolines', PickTicketsolines);
		}
		
		if(PickTicketearchresults != null && PickTicketearchresults !='')
		{
			PickTicketord = parseInt(PickTicketord) + 1;
			PickTicketsolines = parseInt(PickTicketsolines) + 1;
		}
		
		recCount[0]=PickTicketord;
		recCount[1]=PickTicketsolines;
		return recCount;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception In GetPickTicketOrder',exp);
	}
}