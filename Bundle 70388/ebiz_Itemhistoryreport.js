/***************************************************************************
                                                                 
   eBizNET Solutions Inc 
****************************************************************************
*
*  $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Suitelet/Attic/ebiz_Itemhistoryreport.js,v $
*  $Revision: 1.1.2.2 $
*  $Date: 2013/09/11 15:23:51 $
*  $Author: rmukkera $
*  $Name: t_NSWMS_2014_1_1_174 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_Itemhistoryreport.js,v $
*  Revision 1.1.2.2  2013/09/11 15:23:51  rmukkera
*  Case# 20124376
*
*
****************************************************************************/

function ItemHistoryReport_SL(request,response)
{

	var vItem = request.getParameter('custpage_item');
	var vfromdate = request.getParameter('custpage_fromdate');
	var vtodate = request.getParameter('custpage_todate');		

	nlapiLogExecution('ERROR', 'vItem', vItem);
	nlapiLogExecution('ERROR', 'vfromdate', vfromdate);
	nlapiLogExecution('ERROR', 'vtodate', vtodate);

	var rptItemHistory = nlapiCreateReportDefinition("Item History Report");
	rptItemHistory.setTitle(" Item History Report");


	var vDate = rptItemHistory.addColumn("vdate", true, "Date", null, "text", null);
	var vTransaction = rptItemHistory.addColumn("transaction", true, "Transaction", null, "text", null);
	var vTasktype = rptItemHistory.addColumn("tasktype", true, "Task Type", null, "text", null);
	var vQty = rptItemHistory.addColumn("quantity", true, "Quantity", null, "text", null);

	// bind the Open Task search
	var OpenTaskColumns = new Array();
	OpenTaskColumns[0] = new nlobjSearchColumn("custrecord_act_end_date", null, null);
	OpenTaskColumns[1] = new nlobjSearchColumn("custrecord_ebiz_order_no", null, null);
	OpenTaskColumns[2] = new nlobjSearchColumn("custrecord_tasktype", null, null);
	OpenTaskColumns[3] = new nlobjSearchColumn("custrecord_act_qty", null, null);
	OpenTaskColumns[4] = new nlobjSearchColumn('name');	



	var OpenTaskfilters = new Array();
	if (vItem != "" && vItem != null) {
		//OpenTaskfilters[0] = new nlobjSearchFilter("custrecord_sku", null, "is", vItem);
		OpenTaskfilters.push(new nlobjSearchFilter("custrecord_sku", null, "is", vItem));
	}
	if (vfromdate != "" && vfromdate != null) {
		nlapiLogExecution('ERROR', 'vfromdate', vfromdate);
		OpenTaskfilters.push(new nlobjSearchFilter('custrecordact_begin_date', null, 'within', vfromdate, vtodate));
	} 

	OpenTaskfilters.push(new nlobjSearchFilter("custrecord_tasktype", null, 'noneof', [13,14]));
	rptItemHistory.addSearchDataSource("customrecord_ebiznet_trn_opentask", null, OpenTaskfilters, OpenTaskColumns, { "vdate":OpenTaskColumns[0], "transaction":OpenTaskColumns[1],"tasktype":OpenTaskColumns[2],"quantity":OpenTaskColumns[3]});





	// bind the Closed Task search
	var ClosedTaskColumns = new Array();


	ClosedTaskColumns[0] = new nlobjSearchColumn("custrecord_ebiztask_act_end_date", null, null);
	ClosedTaskColumns[1] = new nlobjSearchColumn("custrecord_ebiztask_ebiz_order_no", null, null);
	ClosedTaskColumns[2] = new nlobjSearchColumn("custrecord_ebiztask_tasktype", null, null);
	ClosedTaskColumns[3] = new nlobjSearchColumn("custrecord_ebiztask_act_qty", null, null);
	ClosedTaskColumns[4] = new nlobjSearchColumn('name');	

	var ClosedTaskfilters = new Array();
	if (vItem != "" && vItem != null) {
		//ClosedTaskfilters[0] = new nlobjSearchFilter("custrecord_ebiztask_sku", null, "is", vItem);
		ClosedTaskfilters.push(new nlobjSearchFilter("custrecord_ebiztask_sku", null, "is", vItem));
	}
	if (vfromdate != "" && vfromdate != null) {
		nlapiLogExecution('ERROR', 'vfromdate', vfromdate);
		ClosedTaskfilters.push(new nlobjSearchFilter('custrecord_ebiztask_act_begin_date', null, 'within', vfromdate, vtodate));
	} 
	ClosedTaskfilters.push(new nlobjSearchFilter("custrecord_ebiztask_tasktype", null, 'noneof', [13,14]));

	rptItemHistory.addSearchDataSource("customrecord_ebiznet_trn_ebiztask", null, ClosedTaskfilters, ClosedTaskColumns, { "vdate":ClosedTaskColumns[0], "transaction":ClosedTaskColumns[1],"tasktype":ClosedTaskColumns[2],"quantity":ClosedTaskColumns[3]});
	var form = nlapiCreateReportForm();
	var pvtTable = rptItemHistory.executeReport(form);	

	response.writePage(form);

}



