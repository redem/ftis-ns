/***************************************************************************
 eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_TwoStepReplen_batch.js,v $
 *     	   $Revision: 1.1.2.1.2.1 $
 *     	   $Date: 2015/11/16 15:37:30 $
 *     	   $Author: schepuri $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_TwoStepReplen_batch.js,v $
 * Revision 1.1.2.1.2.1  2015/11/16 15:37:30  schepuri
 * case# 201414947
 *
 * Revision 1.1.2.1  2015/01/02 15:04:25  skreddy
 * Case# 201411349
 * Two step replen process
 *
 * Revision 1.6.2.6.4.5.2.14  2014/05/30 00:41:04  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 *
 *****************************************************************************/
function TwoStep_ReplenPickingBatch(request, response){
	if (request.getMethod() == 'GET') {
		var getFetchedBeginLocation = request.getParameter('custparam_beginLocation');

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7,st8,st9,st10;

		if( getLanguage == 'es_ES')
		{
			st1 = "LOTE # : ";
			st2 = "OVERRIDE";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";


		}
		else
		{
			st1 = "LOT# : ";
			st2 = "SEND ";
			st3 = "PREV";
			st4 = "OVERRIDE";
		}

		var reportNo = request.getParameter('custparam_repno');
		var beginLoc = request.getParameter('custparam_repbegloc');
		var sku = request.getParameter('custparam_repsku');
		var skuNo = request.getParameter('custparam_repskuno');
		var expQty = request.getParameter('custparam_repexpqty');
		var repInternalId = request.getParameter('custparam_repid');
		var clustNo = request.getParameter('custparam_clustno');
		var whlocation = request.getParameter('custparam_whlocation');
		var actEndLocationId = request.getParameter('custparam_reppicklocno');
		var actEndLocation = request.getParameter('custparam_reppickloc');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var nextlocation=request.getParameter('custparam_nextlocation');
		var nextqty=request.getParameter('custparam_nextqty');
		var batchno=request.getParameter('custparam_batchno');
		var fromlpno=request.getParameter('custparam_fromlpno');
		var tolpno=request.getParameter('custparam_tolpno');
		var nextitem=request.getParameter('custparam_nextitem');
		var nextitemno=request.getParameter('custparam_nextitemno');
		var taskpriority = request.getParameter('custparam_entertaskpriority');
		var getreportNo=request.getParameter('custparam_enterrepno');
		var getitem = request.getParameter('custparam_entersku');
		var getitemid = request.getParameter('custparam_enterskuid');
		var getloc = request.getParameter('custparam_enterpickloc');
		var getlocid = request.getParameter('custparam_enterpicklocid');
		var replenType = request.getParameter('custparam_replentype');
		var getNumber = request.getParameter('custparam_number');
		var beginLocId = request.getParameter('custparam_repbeglocId');
		var enterfromloc = request.getParameter('custparam_enterfromloc');
		var getLanguage = request.getParameter('custparam_language');

		var btnValue = request.getParameter('hdnclickedbtnid');
		var vzone = request.getParameter('custparam_zoneno');
		var vBatchno = request.getParameter('custparam_batchno');

		var zoneno = request.getParameter('custparam_zoneno');
		var cartLpNo= request.getParameter('custparam_cartlpno');
		nlapiLogExecution('ERROR', 'expQty',expQty);
		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		html = html + " document.getElementById('enterbatch').focus();"; 

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +"<label>" + vBatchno + "</label>";
		html = html + "				<input type='hidden' name='hdnNo' value=" + reportNo + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + beginLoc + ">";
		html = html + "				<input type='hidden' name='hdnSku' value='" + sku + "'>";
		html = html + "				<input type='hidden' name='hdnExpQty' value=" + expQty + ">";
		html = html + "				<input type='hidden' name='hdnSkuNo' value=" + skuNo + ">";
		html = html + "				<input type='hidden' name='hdnid' value=" + repInternalId + ">";
		html = html + "				<input type='hidden' name='hdnclustno' value=" + clustNo + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whlocation + ">";
		html = html + "				<input type='hidden' name='hdnactendlocationid' value=" + actEndLocationId + ">";
		html = html + "				<input type='hidden' name='hdnactendlocation' value=" + actEndLocation + ">";
		html = html + "				<input type='hidden' name='hdnbeginLocId' value=" + beginLocId + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnfromLpno' value=" + fromlpno + ">";
		html = html + "				<input type='hidden' name='hdntoLpno' value=" + tolpno + ">";
		html = html + "				<input type='hidden' name='hdnnextloc' value=" + nextlocation + ">";
		html = html + "				<input type='hidden' name='hdnnextqty' value=" + nextqty + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value=" + batchno + ">";
		html = html + "				<input type='hidden' name='hdnnitem' value='" + nextitem + "'>";
		html = html + "				<input type='hidden' name='hdnnextitemno' value=" + nextitemno + ">";
		html = html + "				<input type='hidden' name='hdntaskpriority' value=" + taskpriority + ">";
		html = html + "				<input type='hidden' name='hdngetNumber' value=" + getNumber + ">";
		html = html + "				<input type='hidden' name='hdnReplenType' value=" + replenType + ">";
		html = html + "				<input type='hidden' name='hdngetitem' value=" + getitem + ">";
		html = html + "				<input type='hidden' name='hdngetitemid' value=" + getitemid + ">";
		html = html + "				<input type='hidden' name='hdngetloc' value=" + getloc + ">";
		html = html + "				<input type='hidden' name='hdngetlocid' value=" + getlocid + ">";
		html = html + "				<input type='hidden' name='hdngetreportno' value=" + getreportNo + ">";
		html = html + "				<input type='hidden' name='hdnenterfromloc' value=" + enterfromloc + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdnCartlpno' value=" + cartLpNo + ">";
		html = html + "				<input type='hidden' id='hdnclickedbtnid' value="+btnValue+" >";
//		html = html + "				<input type='hidden' id='hdnvzone' value="+vzone+" >";
		html = html + "				<input type='hidden' name='hdnvzone' value="+vzone+" >";// case# 201416058
		html = html + "				<input type='hidden' name='hdnclustno' value=" + clustNo + ">";
		html = html + "				<input type='hidden' name='hdnvbatchno' value='" + vBatchno + "'>";
		html = html + "				</td>";
		html = html + "			</tr>";
//		html = html + "			<tr>";
//		html = html + "				<td align = 'left'>ENTER/SCAN BATCH/LOT# ";
//		html = html + "				</td>";
//		html = html + "			</tr>";
		/*if(LotRequired=='Y')
		{*/
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='venterbatch' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		//}
		html = html + "			<tr>";
//		html = html + "				<td align = 'left'><input name='enterbatch' type='text'/>";
		html = html + "				<td align = 'left'><input type='hidden' name='enterbatch' value=" + vBatchno+">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'>"+ st2 +" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "				<td align = 'left'>"+ st2 +" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					"+ st3 +" <input name='cmdPrevious' type='submit' value='F7'/>";
		/*	if(LotRequired=='Y')
		{
		html = html + "					"+ st4 +" <input name='cmdOverride' type='submit' value='F11'/>";
		}*/
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');
		//var lotrequired=request.getParameter('lotrequired');
		/*if(lotrequired=='Y')
		{*/
		var vgetEnteredbatch = request.getParameter('venterbatch');
		//nlapiLogExecution('DEBUG', 'Entered Item', getEnteredbatch);
		/*}

		else*/
		//var vgetEnteredbatch = request.getParameter('enterbatch');// case# 201414947

		nlapiLogExecution('DEBUG', 'vgetEnteredbatch', vgetEnteredbatch);

		var getEnteredbatch = request.getParameter('enterbatch');
		nlapiLogExecution('DEBUG', 'Entered Item', getEnteredbatch);
		var getWaveNo = request.getParameter('hdnWaveNo');
		var getRecordInternalId = request.getParameter('hdnRecordInternalId');
		var getContainerLpNo = request.getParameter('hdnContainerLpNo');
		var getExpectedQuantity = request.getParameter('hdnExpectedQuantity');
		var getBeginLocation = request.getParameter('hdnBeginLocation');
		var getBeginLocationname = request.getParameter('hdnBeginLocationname');
		var getItem = request.getParameter('hdnItem');
		var getItemDescription = request.getParameter('hdnItemDescription');
		var getItemInternalId = request.getParameter('hdnSkuNo');
		var getDOLineId = request.getParameter('hdnDOLineId');
		var getInvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var getOrderLineNo = request.getParameter('hdnOrderLineNo');
		var getItem = request.getParameter('hdnItemName');
		var vClusterNo = request.getParameter('hdnClusterNo');
		var vBatchNo = request.getParameter('hdnbatchno');	
		var whLocation = request.getParameter('hdnwhlocation');
		//SOarray["custparam_whlocation"] = whLocation;
		var whCompany = request.getParameter('hdnwhCompany');
		var RecCount=request.getParameter('hdnRecCount');
		var getNext=request.getParameter('hdnnext');
		var OrdName=request.getParameter('hdnName');
		var ContainerSize=request.getParameter('hdnContainerSize');
		var ebizOrdNo=request.getParameter('hdnebizOrdNo');
		var ItemStatus=request.getParameter('hdnitemstatus');
		var ItemType=request.getParameter('hdnitemtype');
		var Reparray = new Array();
		var getLanguage =  request.getParameter('hdngetLanguage');
		var Bulkpickflag=request.getParameter('hdnbulkpickflag'); 
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var hdnBeginLocation = request.getParameter('hdnBeginLocation');			
		var reportNo = request.getParameter('hdnNo');	
		var beginLocID = request.getParameter('hdnbeginLocId');
		var zoneID = request.getParameter('hdnZoneId');
		var toloc = request.getParameter('hdnactendlocation');
		var whLoc= request.getParameter('hdnwhlocation');
		var expQty= request.getParameter('hdnExpQty');


		var st5;

		if( getLanguage == 'es_ES')
		{

			st5 = "LOT # NO V�LIDO";

		}
		else
		{	
			st5 = "INVALID LOT#";
		}



		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		// This variable is to hold the SO# entered.


		Reparray["custparam_error"] = st5;//'INVALID LOT#';
		Reparray["custparam_screenno"] = '14PickingReplnBatch';
		//var vZoneNo=request.getParameter('hdnZoneNo');
		var vZoneNo=request.getParameter('hdnvzone');// case# 201416058;the above line is commented because hdnZoneNo is not defined anywhere
		var vZoneId=request.getParameter('hdnebizzoneno');
		if(vZoneId!=null && vZoneId!="")
		{
			Reparray["custparam_ebizzoneno"] =  vZoneId;
		}
		else
			Reparray["custparam_ebizzoneno"] = '';
		Reparray["custparam_zoneno"] =  vZoneNo;
		Reparray["custparam_repno"] = request.getParameter('hdnNo');
		Reparray["custparam_repbegloc"] = request.getParameter('hdnBeginLocation');;
		Reparray["custparam_repbeglocId"] = request.getParameter('hdnbeginLocId');
		Reparray["custparam_repsku"] = request.getParameter('hdnSku');
		Reparray["custparam_repexpqty"] = expQty;
		nlapiLogExecution('ERROR', 'expQty',expQty);
		Reparray["custparam_repskuno"] = request.getParameter('hdnSkuNo');

		Reparray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
		Reparray["custparam_reppicklocno"] = request.getParameter('hdnactendlocationid');
		Reparray["custparam_reppickloc"] = request.getParameter('hdnactendlocation');
		Reparray["custparam_zoneno"] = request.getParameter('hdnZoneId');
		Reparray["custparam_cartlpno"] = request.getParameter('hdnCartlpno');
		Reparray["custparam_batchno"] = request.getParameter('hdnvbatchno');
		Reparray["custparam_zoneno"] = request.getParameter('custparam_zoneno');
		//var vzone = request.getParameter('custparam_zoneno');

		var getBeginLocIntrId=request.getParameter('hdnEndLocInternalId');
		// Fetch the actual location based on the begin location value that is fetched and passed as a parameter
		// var BinLocationRec = nlapiLoadRecord('customrecord_ebiznet_location', getBeginLocation);

		// getBeginBinLocation = BinLocationRec.getFieldTx('custrecord_ebizlocname');
		//  nlapiLogExecution('DEBUG', 'Location Name is', getBeginBinLocation);
		/*       
        getBeginLocationInternalId = BinLocationRec.getId();
        nlapiLogExecution('DEBUG', 'Begin Location Internal Id', getBeginLocationInternalId);
		 */        
		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept SO #.
		if (optedEvent == 'F7') {

			nlapiLogExecution('ERROR', 'PREV  F10 Pressed');
			response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenitem', 'customdeploy_ebiz_twostepreplenitem', false, Reparray);
			return
		}
		else {
			if(vgetEnteredbatch!=null&&vgetEnteredbatch!="")
			{
				if(vgetEnteredbatch!=getEnteredbatch)
				{
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
					nlapiLogExecution('DEBUG', 'Error: ', 'Scanned batch is different');
					return;

					/*
					var result=GetBatchId(getItemInternalId,vgetEnteredbatch);
					var BatchId=result[0];
					var IsValidflag=result[1];
					if(IsValidflag=='T')
					{
						var result=IsBatchNoValid(SOarray["custparam_endlocinternalid"],getItemInternalId,getExpectedQuantity,ItemStatus,BatchId);
						if(result.length>0){
							var IsValidBatchForBinLoc=result[0];
							var NewInvtRecId=result[1];
							var NewLp=result[2];
							var NewPackcode=result[3];
							var NewItemStatus=result[4];

							nlapiLogExecution('DEBUG', 'IsValidBatchForBinLoc', IsValidBatchForBinLoc);
							nlapiLogExecution('DEBUG', 'NewInvtRecId', NewInvtRecId);
							nlapiLogExecution('DEBUG', 'NewItemStatus', NewItemStatus);

							if(IsValidBatchForBinLoc=='T')
							{


								if(pickType =='CL')
								{
									//for cluster picking
									nlapiLogExecution('DEBUG', 'Cluster picking', 'Cluster Picking');
									nlapiLogExecution('DEBUG', 'vClusterNo', vClusterNo);
									nlapiLogExecution('DEBUG', 'getItemInternalId', getItemInternalId);
									nlapiLogExecution('DEBUG', 'getBeginLocIntrId', getBeginLocIntrId);

									var SOFilters = new Array();

									SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
									SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK


									if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
									{
										SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
									}

									if(getItemInternalId != null && getItemInternalId != "")
									{
										SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_sku_no', null, 'is', getItemInternalId));
									}

									if(getBeginLocIntrId != null && getBeginLocIntrId != "")
									{
										SOFilters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', getBeginLocIntrId));
									}

									var SOColumns = new Array();				
									SOColumns[0] = new nlobjSearchColumn('custrecord_sku');
									SOColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
									SOColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
									SOColumns[3] = new nlobjSearchColumn('custrecord_skudesc');
									SOColumns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');					
									SOColumns[5] = new nlobjSearchColumn('custrecord_wms_location');
									SOColumns[6] = new nlobjSearchColumn('custrecord_comp_id');
									SOColumns[7] = new nlobjSearchColumn('custrecord_ebizmethod_no');
									SOColumns[8] = new nlobjSearchColumn('custrecord_ebiz_clus_no');
									SOColumns[0].setSort();//SKU
									SOColumns[2].setSort();//Location

									var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
									if (SOSearchResults != null && SOSearchResults !='')
									{
										if(SOSearchResults.length>0)
										{
											nlapiLogExecution('DEBUG', 'SOSearchResults.length', SOSearchResults.length);
											for (var i=0; i< SOSearchResults.length;i++)
											{
												var getRecordId =SOSearchResults[i].getId();
												nlapiLogExecution('DEBUG', 'getRecordId', getRecordId);
												var UpdateOpentask = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', getRecordId);
												UpdateOpentask.setFieldValue('custrecord_batch_no', vgetEnteredbatch);
												UpdateOpentask.setFieldValue('custrecord_lpno', NewLp);
												//UpdateOpentask.setFieldValue('custrecord_packcode', NewPackcode);
												//UpdateOpentask.setFieldValue('custrecord_sku_status', NewItemStatus);
												UpdateOpentask.setFieldValue('custrecord_invref_no',NewInvtRecId); 
												nlapiSubmitRecord(UpdateOpentask, false, true);

											}

										}

										//deallocate old lot# allocated Qty in create inventory					
										var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv',getInvoiceRefNo);
										var qty = transaction.getFieldValue('custrecord_ebiz_qoh');
										var allocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
										var vLP=transaction.getFieldValue('custrecord_ebiz_inv_lp');
										var vPackcode=transaction.getFieldValue('custrecord_ebiz_inv_packcode');
										var vSKUStatus=transaction.getFieldValue('custrecord_ebiz_inv_sku_status');

										nlapiLogExecution('DEBUG', 'qty', qty);
										nlapiLogExecution('DEBUG', 'allocqty', allocqty);
										nlapiLogExecution('DEBUG', 'getExpectedQuantity', getExpectedQuantity);
										transaction.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(allocqty)- parseFloat(getExpectedQuantity)));
										nlapiSubmitRecord(transaction, false, true);


										//update allocated qty to new Lot# in create inventory
										var InvtDetails = nlapiLoadRecord('customrecord_ebiznet_createinv', NewInvtRecId);
										var Invtqty = InvtDetails.getFieldValue('custrecord_ebiz_qoh');
										var Invtallocqty = InvtDetails.getFieldValue('custrecord_ebiz_alloc_qty');
										var InvtvLP=InvtDetails.getFieldValue('custrecord_ebiz_inv_lp');
										var InvtvPackcode=InvtDetails.getFieldValue('custrecord_ebiz_inv_packcode');
										if(Invtallocqty == null || Invtallocqty =='')
											Invtallocqty=0;

										InvtDetails.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(Invtallocqty)+ parseFloat(getExpectedQuantity)));
										nlapiSubmitRecord(InvtDetails, false, true);
									}


									SOarray["custparam_prevscreen"] = request.getParameter('custparam_prevscreen');
									response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_summtask', 'customdeploy_rf_cluspicking_summtask', false, SOarray);
									nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');		
								}
								else
								{


									//for order picking

									nlapiLogExecution('DEBUG', 'Wave picking', 'Wave Picking');
									//update open task with new lot# and Invtrefno
									var UpdateOpentask = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', getRecordInternalId);
									UpdateOpentask.setFieldValue('custrecord_batch_no', vgetEnteredbatch);
									UpdateOpentask.setFieldValue('custrecord_lpno', NewLp);
									//UpdateOpentask.setFieldValue('custrecord_packcode', NewPackcode);
									//UpdateOpentask.setFieldValue('custrecord_sku_status', NewItemStatus);
									UpdateOpentask.setFieldValue('custrecord_invref_no',NewInvtRecId); 
									nlapiSubmitRecord(UpdateOpentask, false, true);

									//deallocate old lot# allocated Qty in create inventory					
									var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv',getInvoiceRefNo);
									var qty = transaction.getFieldValue('custrecord_ebiz_qoh');
									var allocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
									var vLP=transaction.getFieldValue('custrecord_ebiz_inv_lp');
									var vPackcode=transaction.getFieldValue('custrecord_ebiz_inv_packcode');
									var vSKUStatus=transaction.getFieldValue('custrecord_ebiz_inv_sku_status');

									nlapiLogExecution('DEBUG', 'qty', qty);
									nlapiLogExecution('DEBUG', 'allocqty', allocqty);
									nlapiLogExecution('DEBUG', 'getExpectedQuantity', getExpectedQuantity);
									transaction.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(allocqty)- parseFloat(getExpectedQuantity)));
									nlapiSubmitRecord(transaction, false, true);


									//update allocated qty to new Lot# in create inventory
									var InvtDetails = nlapiLoadRecord('customrecord_ebiznet_createinv', NewInvtRecId);
									var Invtqty = InvtDetails.getFieldValue('custrecord_ebiz_qoh');
									var Invtallocqty = InvtDetails.getFieldValue('custrecord_ebiz_alloc_qty');
									var InvtvLP=InvtDetails.getFieldValue('custrecord_ebiz_inv_lp');
									var InvtvPackcode=InvtDetails.getFieldValue('custrecord_ebiz_inv_packcode');

									InvtDetails.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(Invtallocqty)+ parseFloat(getExpectedQuantity)));
									nlapiSubmitRecord(InvtDetails, false, true);


									SOarray["custparam_batchno"] = vgetEnteredbatch;
									//SOarray["custparam_entloc"] = getBeginLocation;
									SOarray["custparam_recid"] = NewInvtRecId;
									//SOarray["custparam_entlocid"] = SOarray["custparam_endlocinternalid"]; 
									SOarray["custparam_Exc"] = 'L';
									//response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);
									//nlapiLogExecution('DEBUG', 'Done customrecord1', 'Success');

									if(Bulkpickflag=="Y")
									{
										SOarray["custparam_bulkpickflag"] = Bulkpickflag;
										response.sendRedirect('SUITELET', 'customscript_rf_bulkpick_confirm', 'customdeploy_rf_bulkpick_confirm_di', false, SOarray);
										nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
										return;
									}



									response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);
									nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');	
								}

							}
							else
							{
								SOarray["custparam_error"] = 'INVENTORY IS NOT AVAILABLE FOR THE LOT#';
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								nlapiLogExecution('DEBUG', 'Error: ', 'Does not belong to the bin location');
							}

						}
						else
						{
							SOarray["custparam_error"] = 'INVENTORY IS NOT AVAILABLE FOR THE LOT#';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						}
					}
					else 
					{
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						nlapiLogExecution('DEBUG', 'Error: ', 'Scanned batch is invalid');
					}
					 */}
				else 
				{

					var result=GetBatchId(getItemInternalId,vgetEnteredbatch);
					var BatchId=result[0];
					var IsValidflag=result[1];
					nlapiLogExecution('DEBUG', 'result', result);

					if(IsValidflag=='T')
					{
						Reparray["custparam_batchnoId"] = BatchId;
						response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenqty', 'customdeploy_ebiz_twostepreplenqty', false, Reparray);
						//response.sendRedirect('SUITELET', 'customscript_ebiz_hook_sl_2', 'customdeploy_ebiz_hook_sl_2', false, Reparray);
						nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
						return;
					}
					else
					{
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
						nlapiLogExecution('DEBUG', 'Error: ', 'Scanned batch is invalid');
						return;
					}
				}

			}
			//if (getEnteredLocation != '' && getEnteredLocation == getBeginBinLocation) {
			else{			// && getEnteredbatch == vBatchNo) {

				Reparray["custparam_error"] = 'ENTER LOT#';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
				nlapiLogExecution('DEBUG', 'Error: ', 'Scanned batch is invalid');
				return;
			}
		}
	}
}



function GetSystemRuleForLotRequired()
{
	try
	{
		var rulevalue='N';
		var filter=new Array();
		filter.push(new nlobjSearchFilter('name',null,'is','Is Lot# Scan required in RF Picking?'));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizrulevalue');

		var searchresult=nlapiSearchRecord('customrecord_ebiznet_sysrules',null,filter,column);

		if(searchresult!=null&&searchresult!="")
			rulevalue=searchresult[0].getValue('custrecord_ebizrulevalue');
		nlapiLogExecution('DEBUG','rulevalue',rulevalue);
		return rulevalue;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in GetSystemRuleForLPRequired',exp);
	}
}

function GetBatchId(itemid,getEnteredbatch)
{
	try
	{
		var globalArray=new Array();
		var BatchID;
		var flag='F';
		var filter=new Array();
		if(itemid!=null&&itemid!="")
			filter.push(new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',itemid));
		filter.push(new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',getEnteredbatch));
		var rec=nlapiSearchRecord('customrecord_ebiznet_batch_entry',null,filter,null);
		if(rec!=null&&rec!="")
		{
			BatchID=rec[0].getId();
			flag='T';
		}
		globalArray.push(BatchID);
		globalArray.push(flag);
		nlapiLogExecution('DEBUG','globalArray',globalArray);
		return globalArray;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in GetBatchId',exp);
	}
}


function IsBatchNoValid(BeginLocationId,ItemId,getExpectedQuantity,getItemStatusId,BatchId)
{
	try
	{
		nlapiLogExecution('DEBUG','BeginLocationId',BeginLocationId);
		nlapiLogExecution('DEBUG','ItemId',ItemId);
		nlapiLogExecution('DEBUG','getExpectedQuantity',getExpectedQuantity);
		nlapiLogExecution('DEBUG','getItemStatusId',getItemStatusId);
		nlapiLogExecution('DEBUG','BatchId',BatchId);

		var summaryArray=new Array();
		var tempflag='F';
		var recid="";
		var Lp="";
		var ItemStatus="";
		var Packcode="";
		var filter=new Array();
		filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc',null,'anyof',BeginLocationId));
		filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku',null,'anyof',ItemId));
		filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot',null,'anyof',BatchId));

//		if(getItemStatusId !=null && getItemStatusId !=''){
//		filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status',null, 'anyof',getItemStatusId));
//		}

		filter.push(new nlobjSearchFilter('custrecord_ebiz_avl_qty',null, 'greaterthan', getExpectedQuantity));
		filter.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19','3']));

		var Columns = new Array();
		Columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
		Columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_packcode');
		Columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_sku_status');

		var searchrecord=nlapiSearchRecord('customrecord_ebiznet_createinv',null,filter,Columns);

		if(searchrecord!=null&&searchrecord!="")
		{
			tempflag='T';
			recid=searchrecord[0].getId();
			Lp=searchrecord[0].getValue('custrecord_ebiz_inv_lp');
			ItemStatus=searchrecord[0].getValue('custrecord_ebiz_inv_sku_status');
			Packcode=searchrecord[0].getValue('custrecord_ebiz_inv_packcode');
		}
		summaryArray.push(tempflag);
		summaryArray.push(recid);
		summaryArray.push(Lp);
		summaryArray.push(Packcode);
		summaryArray.push(ItemStatus);
		nlapiLogExecution('DEBUG','summaryArray',summaryArray);
		return summaryArray;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in IsBatchNoValid',exp);
	}
}
