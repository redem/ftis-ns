/***************************************************************************
                                                                 
                                                  eBizNET Solutions Inc 
****************************************************************************
*
*  $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inventory/Suitelet/Attic/ebiz_CYCResolve_Expitem_Invt.js,v $
*  $Revision: 1.1.2.3 $
*  $Date: 2013/09/11 15:23:51 $
*  $Author: rmukkera $
*  $Name: t_NSWMS_2014_1_1_174 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_CYCResolve_Expitem_Invt.js,v $
*  Revision 1.1.2.3  2013/09/11 15:23:51  rmukkera
*  Case# 20124376
*
*
****************************************************************************/
function ebiznet_ExpItemInventory(request, response){
	if (request.getMethod() == 'GET'){

		var form = nlapiCreateForm('Cycle Count Resolve Actual Item Total Qty');
		var actitem = request.getParameter('custparam_expsku');
		var planno = request.getParameter('custparam_planno');
		nlapiLogExecution('ERROR','actitem',actitem);
		nlapiLogExecution('ERROR','planno',planno);

		var sublist = form.addSubList("custpage_items", "list", "Expected Item Total Qty");

		sublist.addField("custpage_expqty", "text", "Total Expe Qty");
		sublist.addField("custpage_actqty", "text", "Total Actual Qty");
		sublist.addField("custpage_qtyvariance", "text", " Net Qty Variance");
		sublist.addField("custpage_dollarvariance", "text", "Net Amt Variance");


		var filtersitem = new Array();
		if(planno!=null && planno!=''){
			nlapiLogExecution('ERROR','planno1',planno);
			filtersitem.push(new nlobjSearchFilter('custrecord_cycle_count_plan',null, 'equalto', planno));
			
		}
		filtersitem.push(new nlobjSearchFilter('custrecord_cycleact_sku',null, 'anyof', actitem));
		filtersitem.push(new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [31]));

		var columns = new Array();


		columns[0]=new nlobjSearchColumn('formulanumeric',null,'SUM');
		columns[0].setFormula("TO_NUMBER({custrecord_cycle_act_qty})");
		columns[1]=new nlobjSearchColumn('formulanumeric',null,'SUM');
		columns[1].setFormula("TO_NUMBER({custrecord_cycleexp_qty})");



		var searchresults = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filtersitem,columns);


		if(searchresults!=null && searchresults!='')
		{
			nlapiLogExecution('ERROR','searchresults',searchresults.length);
			for(var i=0;i<searchresults.length;i++)
			{				
				var searchresult = searchresults[i];
				var actqty = searchresult.getValue(columns[0]);
				var expqty = searchresult.getValue(columns[1]);

				var vAvgCost = '0';


				nlapiLogExecution('ERROR','expqty',expqty);
				nlapiLogExecution('ERROR','actqty',actqty);


				if(actqty==null || actqty=='' || isNaN(actqty)) 
					actqty = 0;
				
				if(expqty==null || expqty=='' || isNaN(expqty)) 
					expqty = 0;
				
				var qtyvariance = parseFloat(actqty) - parseFloat(expqty);

				if(qtyvariance==null || qtyvariance=='' || isNaN(qtyvariance)) 
					qtyvariance = 0;

				var filters = new Array();          
				filters.push(new nlobjSearchFilter('internalid', null, 'is',actitem));
				filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));

				var columns = new Array();				
				columns[0] = new nlobjSearchColumn('averagecost');				

				var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
				if (itemdetails !=null) 
				{					
					vAvgCost = itemdetails[0].getValue('averagecost');
					nlapiLogExecution('ERROR', 'Average Cost', vAvgCost);          
				}


				var dollarvariance = (parseFloat(actqty) - parseFloat(expqty)) * vAvgCost;

				if(isNaN(dollarvariance)||dollarvariance==null||dollarvariance=="")
				{
					nlapiLogExecution('ERROR', 'dollarvariance: ', dollarvariance);
					dollarvariance=0;
				}
				else
				{
					//dollarvariance=Math.round(dollarvariance,2);
					dollarvariance = Math.round(dollarvariance * 100) / 100;
					//parseFloat(dollarvariance).toFixed(2);
				}


				nlapiLogExecution('ERROR','qtyvariance',qtyvariance);
				nlapiLogExecution('ERROR','dollarvariance',dollarvariance);


				form.getSubList('custpage_items').setLineItemValue('custpage_expqty', i + 1, expqty);
				form.getSubList('custpage_items').setLineItemValue('custpage_actqty', i + 1, actqty);
				form.getSubList('custpage_items').setLineItemValue('custpage_qtyvariance', i + 1, qtyvariance); 				
				form.getSubList('custpage_items').setLineItemValue('custpage_dollarvariance', i + 1, dollarvariance);


			}

		}
		response.writePage(form);

	}
	else
	{
		//post
	}
}