/***************************************************************************
	 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/ebiz_BOM_Report_SL.js,v $
 *     	   $Revision: 1.1.2.3.4.2.4.6.2.1 $
 *     	   $Date: 2015/09/21 14:02:09 $
 *     	   $Author: deepshikha $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 * *
 * DESCRIPTION
 *  	
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_BOM_Report_SL.js,v $
 * Revision 1.1.2.3.4.2.4.6.2.1  2015/09/21 14:02:09  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.1.2.3.4.2.4.6  2014/08/04 15:19:33  skreddy
 * case # 20149745
 * jawbone SB issue fix
 *
 * Revision 1.1.2.3.4.2.4.5  2014/07/25 13:37:15  skreddy
 * case # 20149396
 * jawbone SB issue fix
 *
 * Revision 1.1.2.3.4.2.4.4  2014/01/29 06:24:21  skreddy
 * case :20126946
 * PCT Prod issue fix
 *
 * Revision 1.1.2.3.4.2.4.3  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.1.2.3.4.2.4.2  2013/03/08 14:43:37  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.1.2.3.4.2.4.1  2013/03/01 14:34:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.1.2.3.4.2  2013/01/25 06:50:44  skreddy
 * CASE201112/CR201113/LOG201121
 * Added Expiry Date
 *
 * Revision 1.1.2.3.4.1  2012/10/26 08:02:57  gkalla
 * CASE201112/CR201113/LOG201121
 * Lot# exception enhancement in work orders
 *
 * Revision 1.1.2.3  2012/08/16 16:20:18  gkalla
 * CASE201112/CR201113/LOG201121
 * Changed Deployment id
 *
 * Revision 1.1.2.2  2012/06/22 10:15:35  gkalla
 * CASE201112/CR201113/LOG201121
 * Button text changed
 *
 * Revision 1.1.2.1  2012/02/21 06:48:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Code syn b/w Trunck and Branch.
 *
 * Revision 1.1  2011/12/30 16:57:00  gkalla
 * CASE201112/CR201113/LOG201121
 * For WO allocation
 *
 * Revision 1.18  2011/10/31 11:39:48  gkalla
 * CASE201112/CR201113/LOG201121
 * To get the Wave#, fulfillment Order# and cluster#s in Sorting order
 *
 * Revision 1.17  2011/10/13 17:06:09  spendyala
 * CASE201112/CR201113/LOG201121
 * added printreport2 function for wavepickreport generation
 *
 * Revision 1.16  2011/10/12 20:00:15  spendyala
 * CASE201112/CR201113/LOG201121
 * uncommitted the display button
 *
 * Revision 1.15  2011/10/12 19:34:02  spendyala
 * CASE201112/CR201113/LOG201121
 * T&T issue is fixed
 *
 *
 *
 **********************************************************************************************************************/

function fillWOField(form, WOField,maxno){
	var WOFilers = new Array();
	WOFilers.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
	WOFilers.push(new nlobjSearchFilter('status', null, 'is', 'WorkOrd:B'));

	if(maxno!=-1)
	{
		WOFilers.push(new nlobjSearchFilter('internalidnumber', null, 'lessthan', parseFloat(maxno)));
	}


	WOField.addSelectOption("", "");

	var columns=new Array();
	columns[0]=new nlobjSearchColumn('tranid');
	columns[0].setSort(true);
	columns[1]=new nlobjSearchColumn('internalid');
	columns[2]=new nlobjSearchColumn('type');


	var customerSearchResults = nlapiSearchRecord('workorder', null, WOFilers,columns);


	for (var i = 0; customerSearchResults != null && i < customerSearchResults.length; i++) {

		//nlapiLogExecution('ERROR', 'tranid', customerSearchResults[i].getValue('tranid')); 
		if(customerSearchResults[i].getValue('tranid') != null && customerSearchResults[i].getValue('tranid') != "" && customerSearchResults[i].getValue('tranid') != " ")
		{
			var resdo = form.getField('custpage_workorder').getSelectOptions(customerSearchResults[i].getValue('tranid'), 'is');
			if (resdo != null) {
				if (resdo.length > 0) {
					continue;
				}
			}
		}
		WOField.addSelectOption(customerSearchResults[i].getValue('internalid'), customerSearchResults[i].getValue('tranid'));
	}
	if(customerSearchResults!=null && customerSearchResults.length>=1000)
	{
		var column=new Array();
		column[0]=new nlobjSearchColumn('tranid');		
		column[1]=new nlobjSearchColumn('internalid');
		column[1].setSort(true);

		var OrderSearchResults = nlapiSearchRecord('workorder', null, WOFilers,column);

		var maxno=OrderSearchResults[OrderSearchResults.length-1].getValue(columns[1]);		
		fillWOField(form, WOField,maxno);	

	}
}


function BOMReportSuitelet(request, response){
	if (request.getMethod() == 'GET') {

		var form = nlapiCreateForm('WO Pick Report');
		/*var WorkOrder  = form.addField('custpage_workorder', 'select', 'Work Order').setLayoutType('startrow', 'none');
		WorkOrder.addSelectOption("","");
		fillWOField(form, WorkOrder,-1); 
		WorkOrder.setMandatory(true);*/
		var woid = request.getParameter('custparam_wo');
		form.addSubmitButton('Confirm Assembly Build');
		form.setScript('customscript_ebiz_bomreport_print');
		form.addButton('custpage_print','Print','Printreport('+woid+')');


		nlapiLogExecution('ERROR','woid',woid);

		var wointid = form.addField('custpage_hdnwono', 'text', 'wono');
		wointid.setDisplayType('hidden');
		wointid.setDefaultValue(woid); 





		if(woid!=null && woid!='')
		{
			nlapiLogExecution('ERROR','INTOwoid1',woid);
			var searchresults = nlapiLoadRecord('workorder', woid);
			//var lineItems= searchresults.getLineItemCount('item');
			var vLineCount=searchresults.getLineItemCount('item');
			//to get data  from opentask
			  var filterOpentask = new Array();
			  filterOpentask[0] = new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', woid);
			  filterOpentask[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]);
		
			     /* var columns = new Array();
                    columns[0] = new nlobjSearchColumn('custrecord_line_no', null, 'group');
                    columns[0] = new nlobjSearchColumn('custrecord_sku', null, 'group');
                    columns[1] = new nlobjSearchColumn('custrecord_expe_qty', null ,'sum');
                    columns[2] = new nlobjSearchColumn('custrecord_lpno', null, 'group');
                    columns[3] = new nlobjSearchColumn('custrecord_batch_no', null, 'group');
                    columns[4] = new nlobjSearchColumn('custrecord_actbeginloc', null, 'group');
                    columns[5] = new nlobjSearchColumn('custrecord_act_qty', null, 'group');                                    
                    columns[0].setSort();*/
			  
		/*	  var columns = new Array();
              columns[0] = new nlobjSearchColumn('custrecord_line_no');
              columns[1] = new nlobjSearchColumn('custrecord_sku');
              columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
              columns[3] = new nlobjSearchColumn('custrecord_lpno');
              columns[4] = new nlobjSearchColumn('custrecord_batch_no');
              columns[5] = new nlobjSearchColumn('custrecord_actbeginloc');
              columns[6] = new nlobjSearchColumn('custrecord_act_qty');                                    
	     
              columns[7] = new nlobjSearchColumn('custrecord_expirydate'); 
	     
              columns[0].setSort();*/
			  
			  //case 20126946  start
			  var columns = new Array();
			  columns.push(new nlobjSearchColumn('custrecord_line_no'));
			  columns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
			  columns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
			  columns.push(new nlobjSearchColumn('custrecord_sku'));
			  columns.push(new nlobjSearchColumn('custrecord_expe_qty'));				
			  columns.push(new nlobjSearchColumn('custrecord_lpno'));
			  columns.push(new nlobjSearchColumn('custrecord_batch_no'));
			  columns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
			  columns.push(new nlobjSearchColumn('custrecord_act_qty'));   
			  columns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
			  columns.push(new nlobjSearchColumn('custrecord_invref_no'));
			  columns.push(new nlobjSearchColumn('custrecord_wms_location'));
			  columns.push(new nlobjSearchColumn('custrecord_comp_id'));
			  columns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));				
			  columns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));	
			  columns.push(new nlobjSearchColumn('custrecord_sku_status'));	
			  columns.push(new nlobjSearchColumn('custrecord_expirydate'));	
			  columns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));	
			  
			  columns[1].setSort();
			  columns[2].setSort();
			  columns[3].setSort();
			  columns[4].setSort(true);
			  
			//case 20126946  end
			  
           	var Opentaskresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterOpentask,columns);	
           				
			if(Opentaskresults!=null && Opentaskresults!=''&& searchresults!=null && searchresults!='') 
			{ 	  nlapiLogExecution('ERROR', 'searchresults.length',searchresults.length);  	
				var qty = searchresults.getFieldValue('quantity');    	     
				var itemid=searchresults.getFieldValue('assemblyitem');
				var itemText=searchresults.getFieldText('assemblyitem');
				var WoCreatedDate=searchresults.getFieldValue('trandate');
				var WoDueDate=searchresults.getFieldValue('custbody_ebiz_woduedate');
				var WoExpcompletionDate=searchresults.getFieldValue('custbody_ebiz_expcompletiondate');
				var itemQty=searchresults.getFieldValue('quantity');
				var itemStatus=searchresults.getFieldValue('status');
				var itemLocation=searchresults.getFieldValue('location');
				var WOName=searchresults.getFieldValue('tranid');
				var assmblyinstruction=searchresults.getFieldValue('custitem_assmblyinstr');
				var vMemo=searchresults.getFieldValue('memo');
				if(assmblyinstruction==null||assmblyinstruction=="")
					assmblyinstruction="";
				var itemLocationtext="";
				if(itemLocation == null || itemLocation =="")
				{
					itemLocation="&nbsp;";
				}
				else
				{
					var searchresultslocation = nlapiLoadRecord('location', itemLocation);
					if(searchresultslocation!=null && searchresultslocation!='') 
					{
						itemLocationtext=searchresultslocation.getFieldValue('name');
					}
				}
				var built='';
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('internalid', null, 'is', woid);
				filters[1] = new nlobjSearchFilter('mainline', null, 'is', 'T');//ship task


				var columns = new Array();
				columns[0] = new nlobjSearchColumn('quantityshiprecv');

				var searchresultsWaveNo = nlapiSearchRecord('workorder', null, filters, columns);
				if(searchresultsWaveNo!=null && searchresultsWaveNo!=''){
					built = searchresultsWaveNo[0].getValue('quantityshiprecv');
				}
				nlapiLogExecution('ERROR','qty',qty);
				nlapiLogExecution('ERROR','built',built);
				if(built==null || built =="")
					built=0;
				qty = parseInt(qty) - parseInt(built);
				nlapiLogExecution('ERROR','qty',qty);
				var msg= form.addField('custpage_message', 'inlinehtml', null, null, null);

				msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Bin location generated sucessfully to the Work Order# " + WOName + "', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
				nlapiLogExecution('ERROR', 'a1','a1');

				form.addField('custpage_wo', 'text', 'WO# : ').setLayoutType("startrow",'none').setDisplayType('inline').setDefaultValue(WOName);	
				form.addField('custpage_kititem', 'text', 'Item : ').setDisplayType('inline').setDefaultValue(itemText);
				form.addField('custpage_kitqty', 'text', 'Qty : ').setDisplayType('inline').setLayoutType("startrow").setDefaultValue(qty);
				form.addField('custpage_kitlocation', 'text', 'Location : ').setDisplayType('inline').setDefaultValue(itemLocationtext);
				form.addField('custpage_createdate', 'text', 'Created Date : ').setDisplayType('inline').setDefaultValue(WoCreatedDate);
				form.addField('custpage_duedate', 'text', 'Due Date : ').setDisplayType('inline').setLayoutType("startrow").setDefaultValue(WoDueDate);
				form.addField('custpage_woexpdate', 'text', 'Expected Completion date : ').setDisplayType('inline').setDefaultValue(WoExpcompletionDate);
				form.addField('custpage_itemstatus', 'text', 'WO Status : ').setDisplayType('inline').setDefaultValue(itemStatus);
				nlapiLogExecution('ERROR', 'a2','a2');
				var hdnBackOrdered = form.addField('custpage_hdnbackordered', 'text', 'Backordered Bool : ').setDisplayType('inline').setDisplayType('hidden');
				form.addField('custpage_hdnitemtext', 'text', 'WorkOrder Text : ').setDisplayType('inline').setDisplayType('hidden').setDefaultValue(WOName);
				form.addField('custpage_kititemid', 'text', 'Item id : ').setDisplayType('inline').setDisplayType('hidden').setDefaultValue(itemid);
				form.addField('custpage_kititemlocation', 'text', 'Location : ').setDisplayType('inline').setDisplayType('hidden').setDefaultValue(itemLocation);
				form.addField('custpage_woid', 'text', 'WO Id: ').setDisplayType('inline').setDisplayType('hidden').setDefaultValue(woid);					  
				nlapiLogExecution('ERROR', 'a3','a3');  
				form.addField('custpage_assminst', 'text', 'Assembly Instructions : ').setDisplayType('inline').setLayoutType("startrow").setDefaultValue(assmblyinstruction);
				if(vMemo != null && vMemo != '')
					form.addField('custpage_memo', 'text', 'Memo : ').setDisplayType('inline').setLayoutType("startrow").setDefaultValue(vMemo);
				nlapiLogExecution('ERROR', 'a4','a4');


				var vLineCount=searchresults.getLineItemCount('item');
				//Adding  ItemSubList

				var ItemSubList = form.addSubList("custpage_deliveryord_items", "list", "Components");
				ItemSubList.addField("custpage_deliveryord_sku", "text","Item");
				ItemSubList.addField("custpage_deliveryord_itemdesc", "text", "Description");
				ItemSubList.addField("custpage_deliveryord_memqty", "text", "Quantity");
				nlapiLogExecution('ERROR', 'a5','a5');
				ItemSubList.addField("custpage_deliveryord_availqty", "text", "Pick Allocated Quantity");
				ItemSubList.addField("custpage_deliveryord_bomqty", "text", "BOM Quantity");
				//ItemSubList.addField("custpage_deliveryord_memqty", "text", "Required Qty").setDisplayType('hidden');       
				ItemSubList.addField("custpage_deliveryord_comitqty", "text", "Committed");        
				ItemSubList.addField("custpage_deliveryord_backordqty", "text", 'Back Ordered');
				nlapiLogExecution('ERROR', 'a6','a6');
				//ItemSubList.addField("custpage_skurecid", "text", "Recid").setDisplayType('hidden');
				ItemSubList.addField("custpage_deliveryord_lp", "text", "LP#");
				ItemSubList.addField("custpage_deliveryord_binloc", "text", "Bin Location").setDisplayType('hidden');
				ItemSubList.addField("custpage_deliveryord_binlocval", "text", "Bin Location");
				ItemSubList.addField("custpage_deliveryord_lotno", "text", "Serial/Lot Numbers"); 
				ItemSubList.addField("custpage_deliveryord_expirydate", "text", "Expiration Date"); 
				ItemSubList.addField("custpage_deliveryord_zoneid", "text", "Zone Id"); 
				//ItemSubList.addField("custpage_lotno", "text", "Commit"); 
				/*ItemSubList.addField("custpage_lineno", "text", "LP#");
				ItemSubList.addField("custpage_lineno", "text", "Bin Location");
				ItemSubList.addField("custpage_lineno", "text", "Inventory Ref#");
				ItemSubList.addField("custpage_lineno", "text", "Available Qty");
				ItemSubList.addField("custpage_lineno", "text", "Bin Location");
				ItemSubList.addField("custpage_lineno", "text", "Lot Info");
				ItemSubList.addField("custpage_lineno", "text", "Line No");*/
				nlapiLogExecution('ERROR', 'a7','a7');
				var vCount=1;
				//code added by santosh on 20Aug12
				for (m=0; m<Opentaskresults.length; m++) 
				{
					nlapiLogExecution('ERROR', 'm',m);
				    nlapiLogExecution('ERROR', 'Opentaskresults.length',Opentaskresults.length);
				
					/*var lineItem=Opentaskresults[m].getValue('custrecord_sku', null, 'group');
					var itemtext=Opentaskresults[m].getText('custrecord_sku', null, 'group');
					var LineNo =Opentaskresults[m].getValue('custrecord_line_no', null, 'group');
					var LineQty=Opentaskresults[m].getValue('custrecord_expe_qty', null ,'sum');
					var LineBinLOcation=Opentaskresults[m].getText('custrecord_actbeginloc', null, 'group');
					var LineBinLOcationVal=Opentaskresults[m].getValue('custrecord_actbeginloc', null, 'group');
					var LineLP=Opentaskresults[m].getValue('custrecord_lpno', null, 'group');
					var LineQuantity=Opentaskresults[m].getValue('custrecord_act_qty', null, 'group');
					var LineLotNo=Opentaskresults[m].getValue('custrecord_batch_no', null, 'group');*/
				    
				    var lineItem=Opentaskresults[m].getValue('custrecord_sku');
					var itemtext=Opentaskresults[m].getText('custrecord_sku');
					var LineNo =Opentaskresults[m].getValue('custrecord_line_no');
					var LineQty=Opentaskresults[m].getValue('custrecord_expe_qty');
					var LineBinLOcation=Opentaskresults[m].getText('custrecord_actbeginloc');
					var LineBinLOcationVal=Opentaskresults[m].getValue('custrecord_actbeginloc');
					var LineLP=Opentaskresults[m].getValue('custrecord_lpno');
					var LineQuantity=Opentaskresults[m].getValue('custrecord_act_qty');
					var LineLotNo=Opentaskresults[m].getValue('custrecord_batch_no');
					var ExpiryDate=Opentaskresults[m].getValue('custrecord_expirydate');
					if(LineLotNo== null || LineLotNo =="")
						LineLotNo="";
					var ZoneId=Opentaskresults[m].getText('custrecord_ebiz_zoneid');
					if(ZoneId== null || ZoneId =="")
						ZoneId=""
					
					
					nlapiLogExecution('ERROR', 'lineItem',lineItem);
					nlapiLogExecution('ERROR', 'itemtext',itemtext);
					nlapiLogExecution('ERROR', 'LineBinLOcation',LineBinLOcation);
					nlapiLogExecution('ERROR', 'LineBinLOcationVal',LineBinLOcationVal);
					nlapiLogExecution('ERROR', 'ExpiryDate',ExpiryDate);
					nlapiLogExecution('DEBUG', 'ZoneId',ZoneId);
					
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_sku', vCount, itemtext);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_memqty', vCount,LineQuantity);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_lotno', vCount, LineLotNo);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_lp', vCount, LineLP);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_binloc', vCount,LineBinLOcationVal );
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_binlocval', vCount, LineBinLOcation);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_availqty', vCount, LineQty);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_expirydate', vCount, ExpiryDate);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_zoneid', vCount, ZoneId);
					for(var i=1;i<=vLineCount;i++)
					{		
					 var WOLineNo =searchresults.getLineItemValue('item','line',i)+".0";
					 if(WOLineNo== null || WOLineNo =="")
						 WOLineNo="&nbsp;";
					 
					 var WOlineItemvalue = searchresults.getLineItemValue('item', 'item', i);
					 
					 var WOlineItem = searchresults.getLineItemText('item', 'item', i);
						if(WOlineItem== null || WOlineItem =="")
							WOlineItem="";					
						
						/*var LineAvailQty =searchresults.getLineItemValue('item','custcol_ebizwoavalqty',i);
						if(LineAvailQty== null || LineAvailQty =="")
							LineAvailQty="&nbsp;";*/
						
						var WOLineQty = searchresults.getLineItemValue('item','quantity',i);
						if(WOLineQty== null || WOLineQty =="")
							WOLineQty="&nbsp;";
						
						var BomQty =WOLineQty/itemQty;
						if(BomQty== null || BomQty =="")
							BomQty="&nbsp;";

						var ItemDesc = searchresults.getLineItemValue('item', 'description', i);
						if(ItemDesc== null || ItemDesc =="")
							ItemDesc="";

						var BackOrdQty = 0;
						if(searchresults.getLineItemValue('item', 'quantitybackordered', i) != null && searchresults.getLineItemValue('item', 'quantitybackordered', i) != "")
							BackOrdQty=parseFloat(searchresults.getLineItemValue('item', 'quantitybackordered', i));

						var CommittedQty = searchresults.getLineItemValue('item', 'quantitycommitted', i);
						if(CommittedQty== null || CommittedQty =="" || CommittedQty == 'undefined')
							CommittedQty="";
						nlapiLogExecution('ERROR', 'BackOrdQty',BackOrdQty);
						nlapiLogExecution('ERROR', 'WOLineNo',WOLineNo);
						nlapiLogExecution('ERROR', 'LineNo',LineNo);
						nlapiLogExecution('ERROR', 'WOlineItemvalue',WOlineItemvalue);
						nlapiLogExecution('ERROR', 'lineItem',lineItem);
						
						
						if(parseInt(WOLineNo)==parseInt(LineNo) && WOlineItemvalue==lineItem)
						{
						nlapiLogExecution('ERROR', 'if conditon','sucess');
						form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_itemdesc', vCount, ItemDesc);
						form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_backordqty', vCount, BackOrdQty);			
						form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_comitqty', vCount, CommittedQty);					
						nlapiLogExecution('ERROR', 'a9','a9');
						form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_bomqty', vCount, parseFloat(BomQty).toFixed(5));
						break;
						}
					}
						vCount=vCount+1;
					
					
				}
				//end of the code on 20aug12
				
			}

		} 

		response.writePage(form);

	}
	else
	{

		var vWorkOrder = request.getParameter('custpage_woid');
		var item = request.getParameter('custpage_kititemid');
		var location = request.getParameter('custpage_kititemlocation');
		var qty = request.getParameter('custpage_kitqty');

		var kitArray = new Array();
		kitArray["custparam_item"] = item;
		kitArray["custparam_locaiton"] = location;
		if(qty!= null && qty !="")
			kitArray["custpage_kitqty"] = parseFloat(qty); 
		kitArray["custpage_workorder"] = vWorkOrder;
		response.sendRedirect('SUITELET', 'customscript_kittostockdtls_suitelet',
				'customdeploy_kittostockdtls_suitelet_di', false, kitArray);
	}	
}

function Printreport(ebizWo){
	var wono = nlapiGetFieldValue('custpage_woid');

	nlapiLogExecution('ERROR', 'into Printreport wono',wono);
	var WOPDFURL = nlapiResolveURL('SUITELET', 'customscript_ebiz_bomreport_pdf_sl', 'customdeploy_ebiz_bomreport_pdf_di');
	/*var ctx = nlapiGetContext();
	nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
	if (ctx.getEnvironment() == 'PRODUCTION') {
		WOPDFURL = 'https://system.netsuite.com' + WOPDFURL;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			WOPDFURL = 'https://system.sandbox.netsuite.com' + WOPDFURL;				
		}*/
	nlapiLogExecution('ERROR', 'WO PDF URL',WOPDFURL);					
	WOPDFURL = WOPDFURL + '&custparam_wo_no='+ wono;
	nlapiLogExecution('ERROR', 'WOPDFURL',WOPDFURL);
//	alert(WavePDFURL);
	window.open(WOPDFURL);

}
