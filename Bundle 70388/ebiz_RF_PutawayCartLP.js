/***************************************************************************
 eBizNET Solutions Inc  
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_PutawayCartLP.js,v $
 *     	   $Revision: 1.4.4.5.4.7.2.11.2.4 $
 *     	   $Date: 2014/12/02 08:51:05 $
 *     	   $Author: schepuri $
 *     	   $Name: t_eBN_2014_2_StdBundle_0_189 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_PutawayCartLP.js,v $
 * Revision 1.4.4.5.4.7.2.11.2.4  2014/12/02 08:51:05  schepuri
 * 201411169�  issue fix
 *
 * Revision 1.4.4.5.4.7.2.11.2.3  2014/11/05 09:58:28  sponnaganti
 * Case# 201410676
 * TPP SB Issue fixed
 *
 * Revision 1.4.4.5.4.7.2.11.2.2  2014/09/16 14:41:37  skreddy
 * case # 201410259
 *  TPP SB issue fix
 *
 * Revision 1.4.4.5.4.7.2.11.2.1  2014/07/01 15:21:22  skavuri
 * Case# 20148710 Compatibility Issue fixed
 *
 * Revision 1.4.4.5.4.7.2.11  2014/06/17 05:46:48  grao
 * Case#: 20148907   New GUI account issue fixes
 *
 * Revision 1.4.4.5.4.7.2.10  2014/06/13 06:45:18  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.4.4.5.4.7.2.9  2014/06/06 06:52:07  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.4.4.5.4.7.2.8  2014/05/30 00:26:49  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.4.4.5.4.7.2.7  2014/02/25 15:13:52  rmukkera
 * Case # 20127339
 *
 * Revision 1.4.4.5.4.7.2.6  2013/12/02 08:59:59  schepuri
 * 20126048
 *
 * Revision 1.4.4.5.4.7.2.5  2013/09/27 15:08:00  schepuri
 * for Batch item while scanning the Cart# system is saying "Error: null"
 *
 * Revision 1.4.4.5.4.7.2.4  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.4.4.5.4.7.2.3  2013/05/22 11:39:03  spendyala
 * CASE201112/CR201113/LOG201121
 * Validating PO Status before user perform transaction.
 *
 * Revision 1.4.4.5.4.7.2.2  2013/04/17 16:04:01  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.4.4.5.4.7.2.1  2013/03/01 15:01:12  rmukkera
 * code is merged from FactoryMation production as part of Standard bundle
 *
 * Revision 1.4.4.5.4.7  2013/02/07 15:10:34  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.4.4.5.4.6  2012/12/21 15:47:53  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form 2012.2 branch.
 *
 * Revision 1.4.4.5.4.5  2012/10/03 05:09:30  grao
 * no message
 *
 * Revision 1.4.4.5.4.4  2012/09/28 06:38:47  grao
 * no message
 *
 * Revision 1.4.4.5.4.3  2012/09/27 10:56:31  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.4.4.5.4.2  2012/09/26 12:48:01  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.4.4.5.4.1  2012/09/24 10:07:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.4.4.5  2012/05/14 14:48:11  spendyala
 * CASE201112/CR201113/LOG201121
 * Validating the CARTLP issue is fixed.
 *
 * Revision 1.4.4.4  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.4.4.3  2012/03/15 12:55:15  schepuri
 * CASE201112/CR201113/LOG201121
 * cart closing functionality modified
 *
 * Revision 1.4.4.2  2012/02/23 13:48:08  schepuri
 * CASE201112/CR201113/LOG201121
 * fixed TPP issues
 *
 * Revision 1.7  2012/02/23 11:51:46  rmukkera
 * CASE201112/CR201113/LOG201121
 * focus given to the input box
 *
 * Revision 1.6  2012/02/16 10:41:51  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 * Revision 1.5  2012/02/02 09:01:52  rmukkera
 * CASE201112/CR201113/LOG201121
 * modifications in code
 *
 * Revision 1.4  2011/09/22 08:29:26  snimmakayala
 * CASE201112/CR201113/LOG201121
 * The spelling in License Plate is  " LICENSE"  not "LICENCE"
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/

function PutawayCartLP(request, response){
	if (request.getMethod() == 'GET') {
		var getCartLP;

		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);

		try {
			var filters = new Array();
			filters[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);
			filters[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
			filters[2] = new nlobjSearchFilter('custrecord_transport_lp', null, 'isnot', '');

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_transport_lp');          
			columns[1] = new nlobjSearchColumn('custrecord_lpno');
			columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
			columns[3] = new nlobjSearchColumn('custrecord_actbeginloc');
			columns[4] = new nlobjSearchColumn('custrecord_sku');
			columns[5] = new nlobjSearchColumn('custrecord_skudesc');

			var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

			var lpmasterfilters =new Array();           
			lpmasterfilters.push(new nlobjSearchFilter('custrecord_ebiz_cart_closeflag', null, 'is', 'T'));
			lpmasterfilters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lptype', null, 'anyof', 4));
			lpmasterfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
			var lpmastercolumns = new Array();
			lpmastercolumns.push(new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp'));        	
			// To get the data from Master LP based on selection criteria
			lpMasterSearchResults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, lpmasterfilters, lpmastercolumns);

			var lpmasterArray= new Array();
			if(lpMasterSearchResults!=null)
			{
				for(var j=0;j<lpMasterSearchResults.length;j++)
				{
					lpmasterArray[j]=lpMasterSearchResults[j].getValue('custrecord_ebiz_lpmaster_lp');

				}
			}  	
			var checkLp =false;
			for (var i = 0; searchresults != null && i < searchresults.length; i++) {
				var searchresult = searchresults[i];            
				getCartLP = searchresults[i].getValue('custrecord_transport_lp');
				//nlapiLogExecution('DEBUG', 'CART LP NO', getCartLP);
				if(lpmasterArray.indexOf(getCartLP)!=-1)
				{
					checkLp=true;
					break;
				}
			}
			if(checkLp==false)
			{
				getCartLP="";
			}
		} 
		catch (exps) {
			nlapiLogExecution('DEBUG', 'Into exception', exps);
		}

		nlapiLogExecution('DEBUG', 'getCartLP', getCartLP);
		
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
	    
		var st0,st1,st2,st3,st4,st5;
		// 20126048
		 
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st0 = "";
			st1 = "LP Carro";
			st2 = "ENTRAR / ESCANEAR N&#218;MERO DE CARRO";	
			st3 = "N&#250;mero de placa";
			st4 = "ENVIAR";
			st5 = "ANTERIOR";
			
		}
		else
		{
			st0 = "";
			st1 = "CART LP";
			st2 = "ENTER/SCAN CART ";
			st3 = "LICENSE PLATE NUMBER";
			st4 = "SEND";
			st5 = "PREV";

		}
		
		

		var functionkeyHtml=getFunctionkeyScript('_rf_putaway_lp'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('entercartlp').focus();";    
		
		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";
		
		html = html + "</script>"; 
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_putaway_lp' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + ": <label>" + getCartLP + "</label>";
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><label id='CartLPNo'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + "</td></tr>";
		html = html + "				<td align = 'left'>" + st3;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entercartlp' id='entercartlp' type='text'/>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st4  +" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					" + st5 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('entercartlp').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is not null');

		// This variable is to hold the LP entered.
		var getCartLPNo = request.getParameter('entercartlp');
		nlapiLogExecution('DEBUG', 'getCartLPNo', getCartLPNo);

		/*var filters = new Array();
		//		filters[0] = new nlobjSearchFilter('custrecord_status_flag', null, 'is', 'L');
		filters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
		filters[1] = new nlobjSearchFilter('custrecord_transport_lp', null, 'is', getCartLPNo);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_transport_lp');
		columns[1] = new nlobjSearchColumn('custrecord_lpno');
		columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[3] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[4] = new nlobjSearchColumn('custrecord_sku');
		columns[5] = new nlobjSearchColumn('custrecord_skudesc');
		columns[6] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		columns[7] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		var lpmasterfilters =new Array();           
		lpmasterfilters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', getCartLPNo));
		//lpmasterfilters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lptype', null, 'anyof', 4));
		lpmasterfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		var lpmastercolumns = new Array();
		lpmastercolumns.push(new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp'));        	
		lpmastercolumns.push(new nlobjSearchColumn('custrecord_ebiz_cart_closeflag')); 
		// To get the data from Master LP based on selection criteria
		lpMasterSearchResults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, lpmasterfilters, lpmastercolumns);

		var lpmasterArray= new Array();
		if(lpMasterSearchResults!=null)
		{
			for(var j=0;j<lpMasterSearchResults.length;j++)
			{
				lpmasterArray[j]=lpMasterSearchResults[j].getValue('custrecord_ebiz_lpmaster_lp');

			}
		}  	
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);*/

		var st4,st5,st6;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st4 = "ESTE LP CART NO EXISTE";
			st5 = "NO V&#193;LIDO LP CART";
			st6 = "Introduzca CARRO LP";
			
		}
		else
		{

			st4 = "THIS CART LP DOESN'T EXISTS";
			st5 = "INVALID CART LP";
			st6 = "Please Enter CART LP";//Case# 20148710
			
		}
		


		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');
		nlapiLogExecution('DEBUG', 'optedEvent', optedEvent);
		if(optedEvent==null || optedEvent=="")
		optedEvent = request.getParameter('cmdSend');
		nlapiLogExecution('DEBUG', 'optedEvent', optedEvent);
		var POarray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		POarray["custparam_screenno"] = 'CRT9'; //Case# 20148710
		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept PO #.
		if (optedEvent == 'F7') {
			response.sendRedirect('SUITELET', 'customscript_rf_receiving_menu', 'customdeploy_rf_receiving_menu_di', false, POarray);
		}
		else {
			try {
				if (getCartLPNo != null && getCartLPNo !='') {
					//var POarray = new Array();
					//var getLanguage = request.getParameter('hdngetLanguage');
					//POarray["custparam_language"] = getLanguage;
					nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);

					var filters = new Array();
					//		filters[0] = new nlobjSearchFilter('custrecord_status_flag', null, 'is', 'L');
					filters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
					filters[1] = new nlobjSearchFilter('custrecord_transport_lp', null, 'is', getCartLPNo);

					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_transport_lp');
					columns[1] = new nlobjSearchColumn('custrecord_lpno');
					columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
					columns[3] = new nlobjSearchColumn('custrecord_actbeginloc');
					columns[4] = new nlobjSearchColumn('custrecord_sku');
					columns[5] = new nlobjSearchColumn('custrecord_skudesc');
					columns[6] = new nlobjSearchColumn('custrecord_ebiz_order_no');
					columns[7] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
					var lpmasterfilters =new Array();           
					lpmasterfilters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', getCartLPNo));
					//lpmasterfilters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lptype', null, 'anyof', 4));
					lpmasterfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
					var lpmastercolumns = new Array();
					lpmastercolumns.push(new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp'));        	
					lpmastercolumns.push(new nlobjSearchColumn('custrecord_ebiz_cart_closeflag')); 
					// To get the data from Master LP based on selection criteria
					lpMasterSearchResults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, lpmasterfilters, lpmastercolumns);

					var lpmasterArray= new Array();
					if(lpMasterSearchResults!=null)
					{
						for(var j=0;j<lpMasterSearchResults.length;j++)
						{
							lpmasterArray[j]=lpMasterSearchResults[j].getValue('custrecord_ebiz_lpmaster_lp');

						}
					}  	
					var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

					var getOptedField = request.getParameter('hdnOptedField');
					POarray["custparam_option"] = getOptedField;
					POarray["custparam_screenno"] = 'CRT9';
					// if (searchresults != null && searchresults.length > 0) {
					//check whether the CART# exists or not
					//Case # 20127339 start
					var tempLpNo=getCartLPNo.toUpperCase();
					if(lpmasterArray.indexOf(tempLpNo)==-1)
					{
						//Case # 20127339 End
						POarray["custparam_error"] = st4;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					}
					else
					{
						if(searchresults!=null && searchresults.length>0)
						{
							var poid=searchresults[0].getValue('custrecord_ebiz_order_no');
							var contrlno=searchresults[0].getValue('custrecord_ebiz_cntrl_no');
							if(poid==null||poid=="")
								poid=contrlno;
							nlapiLogExecution("ERROR","poid",poid);
							var trantype = nlapiLookupField('transaction', poid, 'recordType');
							var validate=CheckOrderStatus(poid,trantype);
							nlapiLogExecution("ERROR","validate",validate);
							if(validate=="F")
							{
								POarray["custparam_error"]="INVALID PO STATUS";
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								nlapiLogExecution('DEBUG', 'Invalid PO ', 'PO closed/cancelled');
								return;
							}
							else
							{
								//if CART# exists check whether it is closed or not
								var cart_closeflag='F';
								for(var j=0;j<lpMasterSearchResults.length;j++)
								{
									if(lpMasterSearchResults[j].getValue('custrecord_ebiz_lpmaster_lp')==getCartLPNo)
									{
										cart_closeflag=lpMasterSearchResults[j].getValue('custrecord_ebiz_cart_closeflag');
									}
								}
								nlapiLogExecution('DEBUG', 'cart_closeflag', cart_closeflag);
								//if CART# is closed normal putaway process
								if(cart_closeflag=='T')
								{
									POarray["custparam_recordcount"] = searchresults.length;
									nlapiLogExecution('DEBUG', 'Record Count', POarray["custparam_recordcount"]);
									if (searchresults != null && searchresults.length > 0) {
										nlapiLogExecution('DEBUG', 'SearchResults of given LP', 'LP is found');
										POarray["custparam_cartno"] = searchresults[0].getValue('custrecord_transport_lp');
										nlapiLogExecution('DEBUG', 'get LP', POarray["custparam_lpno"]);

										var getLPId = searchresults[0].getId();
										nlapiLogExecution('DEBUG', 'get LP Internal Id', getLPId);

										POarray["custparam_quantity"] = searchresults[0].getValue('custrecord_expe_qty');
										POarray["custparam_location"] = searchresults[0].getValue('custrecord_actbeginloc');
										nlapiLogExecution('DEBUG', 'Location', POarray["custparam_location"]);
										POarray["custparam_item"] = searchresults[0].getValue('custrecord_sku');
										POarray["custparam_itemtext"] = searchresults[0].getText('custrecord_sku');
										POarray["custparam_itemDescription"] = searchresults[0].getValue('custrecord_skudesc');
										POarray["custparam_lpno"] = searchresults[0].getValue('custrecord_lpno');





										if (POarray["custparam_location"] != null) {
											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawayconfirm', 'customdeploy_ebiz_rf_cart_putawayconfirm', false, POarray);


										}
										else
										{
											nlapiLogExecution('DEBUG', 'SearchResults', 'Opening a new screen to scan the Item');
											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawaysku', 'customdeploy_ebiz_rf_cart_putawaysku_di', false, POarray);

										}
									}////////s
									else {
										response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
										nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
									}
								}
								else
								{
									POarray["custparam_cartlpno"] = getCartLPNo;
									nlapiLogExecution('DEBUG', 'from else', 'from else');        			
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_confirmclose', 'customdeploy_ebiz_rf_cart_confirmclose', false, POarray);
								}
							}
						}
						else
						{
POarray["custparam_error"] = st5;// issue# 201411169
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
						}
					}
				}/////////////
				else {
					//Case# 20148710 starts
					POarray["custparam_screenno"] = 'CRT9'; 
					POarray["custparam_error"] =st6 ;
					//Case# 20148710 ends
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					nlapiLogExecution('DEBUG', 'getCartLPNo is null ', getCartLPNo);
				}
			} 
			catch (e) {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'In error catch block ', e);
			}
}
}
}
if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}
