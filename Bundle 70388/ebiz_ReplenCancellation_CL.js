/***************************************************************************
                                                                 
                                                  eBizNET Solutions Inc 
****************************************************************************
*
*  $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Client/Attic/ebiz_ReplenCancellation_CL.js,v $
*  $Revision: 1.1.2.1.4.1 $
*  $Date: 2015/11/27 15:22:50 $
*  $Author: aanchal $
*  $Name: t_WMS_2015_2_StdBundle_1_193 $
*
* DESCRIPTION
*  Functionality
*/





function Fieldchange(type,name)
{

	//nlapiSetFieldValue('custpage_tempflag','display');

	if(name=='custpage_selectpage'||name=='custpage_pagesize')
	{
		NLDoMainFormButtonAction("submitter",true);
	}
	else
	{

		return true;
	}
}
function onSave(type,name)
{
	var flag ="";
	var lineCout = nlapiGetLineItemCount('custpage_rplndetails');
	
	var count = 0;
	if(lineCout!=null&&lineCout!=''&&lineCout!='null')
		{
		for(var i=1;i<=lineCout;i++)
			{
			flag = nlapiGetLineItemValue('custpage_rplndetails','custpage_rplncancel',i);	
			
				if(flag =='T')
				{	
					count++;		
				}
			}
		}
	if(count==0)
		{
		alert('Please select atleast one line');
		return false;
		}
	else 
		return true;
	
}

