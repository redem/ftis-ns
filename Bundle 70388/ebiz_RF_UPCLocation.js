/***************************************************************************
����������������������eBizNET
����������������eBizNET SOLUTIONS LTD
 ****************************************************************************
 *
 *� $Source: /cvs/products/eBizNET4.5/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_UPCLocation.js,v $
 *� $Revision: 1.1.2.3 $
 *� $Date: 2014/06/13 09:02:35 $
 *� $Author: skavuri $
 *� $Name: t_NSWMS_2014_1_3_125 $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_RF_UPCLocation.js,v $
 *� Revision 1.1.2.3  2014/06/13 09:02:35  skavuri
 *� Case# 20148882 (added Focus Functionality for Textbox)
 *�
 *� Revision 1.1.2.2  2014/05/30 00:34:23  nneelam
 *� case#  20148622
 *� Stanadard Bundle Issue Fix.
 *�
 *� Revision 1.1.2.1  2014/01/07 15:47:11  rmukkera
 *� Case # 20126443
 *�
 *� Revision 1.7.2.3  2012/07/31 06:57:42  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Depending Upon the System Rule is LP scan required or not will drive the user to scan LP.
 *�
 *� Revision 1.7.2.2  2012/03/16 14:08:54  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Disable-button functionality is been added.
 *�
 *� Revision 1.7.2.1  2012/02/21 13:22:48  schepuri
 *� CASE201112/CR201113/LOG201121
 *� function Key Script code merged
 *�
 *� Revision 1.7  2011/12/08 07:21:05  spendyala
 *� CASE201112/CR201113/LOG201121
 *� added expected item internal id to CCArray
 *�
 *
 ****************************************************************************/




/**
 * @author LN
 * @version
 * @date
 */
function UPCLocation(request, response){
	if (request.getMethod() == 'GET') {

		nlapiLogExecution('ERROR', 'Into Request', 'Success');

		//	Get the PO# from the previous screen, which is passed as a parameter		
		var getPlanNo = request.getParameter('custparam_planno');
		var getsku = request.getParameter('custparam_sku');
		var getskuno = request.getParameter('custparam_skuno');
		var gettasktype = request.getParameter('custparam_tasktype');
		var getlocation = request.getParameter('custparam_location');
		var getbeginlocationid = request.getParameter('custparam_actbeginlocationid');
		var getbeginlocationname = request.getParameter('custparam_actbeginlocationname');
		var getnextlocation = request.getParameter('custparam_nextlocation');
		
		var filters = new Array();

		filters[0] = new nlobjSearchFilter('custrecord_ebiz_report_no', null, 'is', getPlanNo);
		filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [22]);	
		filters[2] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');


		var SOColumns = new Array();				
		SOColumns[0] = new nlobjSearchColumn('custrecord_sku');
		SOColumns[1] = new nlobjSearchColumn('custrecord_tasktype');
		SOColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
		SOColumns[3] = new nlobjSearchColumn('custrecord_wms_location');
		SOColumns[4] = new nlobjSearchColumn('custrecord_ebiz_report_no');	
		SOColumns[5] = new nlobjSearchColumn('custrecord_ebiz_sku_no');	
		SOColumns[6] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
		SOColumns[6].setSort(false);
		nlapiLogExecution('ERROR', 'Before Search Results Id', 'getSearchResultsId');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, SOColumns);
		nlapiLogExecution('ERROR', 'After Search Results Id', 'getSearchResultsId');
		if (searchresults != null && searchresults.length > 0) {
			var getRecordId = searchresults[0].getId();

			var getBeginLocationId = searchresults[0].getValue('custrecord_actbeginloc');
			var getBeginLocationName = searchresults[0].getText('custrecord_actbeginloc');

			nlapiLogExecution('ERROR', 'Location Name', getBeginLocationName);
		}

		var functionkeyHtml=getFunctionkeyScript('_rf_cyclecountlocation'); 
		var html = "<html><head><title>UPC CODE</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";      
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterlocation').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cyclecountlocation' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>GO TO LOCATION: </td></tr>";
		html = html + "				<tr><td align = 'left'><label>" + getBeginLocationName + "</label></td></tr>";
		html = html + "				<tr><td><input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
		html = html + "						<input type='hidden' name='hdnPlanNo' value=" + getPlanNo + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>ENTER/SCAN</td></tr>";
		html = html + "				<tr><td align = 'left'>LOCATION:";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlocation' id='enterlocation' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>CONT <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlocation').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);

	}
	else {
		//try {
		var getupcLocation = request.getParameter('enterlocation');
		nlapiLogExecution('ERROR', 'Location', getupcLocation);

		var getupcPlanno = request.getParameter('custparam_planno');
		nlapiLogExecution('ERROR', 'getPlanNo', getupcPlanno);

		var UPCarray = new Array();

		UPCarray["custparam_error"] = 'INVALID LOCATION';
		UPCarray["custparam_screenno"] = 'UPC2';			
		nlapiLogExecution('ERROR', 'test1', 'test1');		



		nlapiLogExecution('ERROR', 'test2', 'test2');
		var Columns = new Array();				
		Columns[0] = new nlobjSearchColumn('custrecord_sku');
		Columns[1] = new nlobjSearchColumn('custrecord_tasktype');
		Columns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
		Columns[3] = new nlobjSearchColumn('custrecord_wms_location');
		Columns[4] = new nlobjSearchColumn('custrecord_ebiz_report_no');	
		Columns[5] = new nlobjSearchColumn('custrecord_ebiz_sku_no');	
		Columns[6] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
		Columns[6].setSort(false);
		var Sofilters=new Array();			
		nlapiLogExecution('ERROR', 'getupcPlanNo', getupcPlanno);	
		Sofilters[0] = new nlobjSearchFilter('custrecord_ebiz_report_no', null, 'is', getupcPlanno);
		nlapiLogExecution('ERROR', 'test2', 'test2');
		Sofilters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [22]);	
		nlapiLogExecution('ERROR', 'test32', 'test3');
		Sofilters[2] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');

		nlapiLogExecution('ERROR', 'Before Search Results Id', 'getSearchResultsId');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, Sofilters, Columns);
		nlapiLogExecution('ERROR', 'After Search Results Id', 'getSearchResultsId');

		if (searchresults != null && searchresults.length > 0) {
			nlapiLogExecution('ERROR', 'searchresultslength', searchresults.length);
			var getRecordId = searchresults[0].getId();
			UPCarray["custparam_planno"] = getupcPlanno;
			UPCarray["custparam_sku"] = searchresults[0].getValue('custrecord_sku');
			UPCarray["custparam_skuno"] = searchresults[0].getValue('custrecord_ebiz_sku_no');
			UPCarray["custparam_tasktype"] = searchresults[0].getValue('custrecord_tasktype');
			UPCarray["custparam_location"] = searchresults[0].getValue('custrecord_wms_location');
			UPCarray["custparam_actbeginlocationid"] = searchresults[0].getValue('custrecord_actbeginloc');
			UPCarray["custparam_actbeginlocationname"] = searchresults[0].getText('custrecord_actbeginloc');
			nlapiLogExecution('ERROR', 'Search Results Id', getRecordId);

			if(searchresults.length > 1)
			{
				var SearchnextResult = searchresults[1];
				UPCarray["custparam_nextlocation"] = SearchnextResult.getText('custrecord_actbeginloc');

			}



		}

		var optedEvent = request.getParameter('cmdPrevious');
		nlapiLogExecution('ERROR', ' optedEvent',optedEvent);

		if (optedEvent == 'F7') {
			nlapiLogExecution('ERROR', ' Location F7 Pressed');
			response.sendRedirect('SUITELET', 'customscript_rf_upccode_plan_no', 'customdeploy_rf_upccode_plan_no_di', false, UPCarray);
		}
		else {
			if (request.getParameter('enterlocation') != '' && UPCarray["custparam_actbeginlocationname"] == getupcLocation) {
				nlapiLogExecution('ERROR', 'custparam_begin_location_name', UPCarray["custparam_actbeginlocationname"]);

				response.sendRedirect('SUITELET', 'customscript_rf_upccode_item', 'customdeploy_rf_upccode_item_di', false, UPCarray);

			}
			else {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, UPCarray);
				nlapiLogExecution('ERROR', 'UPC Location not found');
			}
		}
	} 
//	catch (e) {
//	response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, UPCarray);
//	nlapiLogExecution('ERROR', 'Catching the Search Results', 'Length is null');
//	}
	nlapiLogExecution('ERROR', 'Done customrecord', 'Success');

}


