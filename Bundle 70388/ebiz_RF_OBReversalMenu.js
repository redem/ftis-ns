/***************************************************************************
  		   eBizNET Solutions Inc               
 ***************************************************************************
 **     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_OBReversalMenu.js,v $
 *     	   $Revision: 1.1.2.1.4.3.4.6 $
 *     	   $Date: 2014/06/13 13:06:36 $
 *     	   $Author: skavuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_OBReversalMenu.js,v $
 * Revision 1.1.2.1.4.3.4.6  2014/06/13 13:06:36  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.1.4.3.4.5  2014/06/12 15:26:50  skavuri
 * Case # 20148880 (LinkButton functionality added to options in RF Screens)
 *
 * Revision 1.1.2.1.4.3.4.4  2014/05/30 00:41:02  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.1.4.3.4.3  2013/11/28 06:06:31  skreddy
 * Case# 20125929
 * Afosa SB issue fix
 *
 * Revision 1.1.2.1.4.3.4.2  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.2.1.4.3.4.1  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.1.4.3  2012/12/17 23:00:13  kavitha
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal Process Issue fixes
 *
 * Revision 1.1.2.1.4.2  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.1.2.1.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.1.2.1  2012/06/25 14:54:17  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Reversal
 *
 *
 *****************************************************************************/

function OBReversalMenu(request, response)
{
	if (request.getMethod() == 'GET') 
	{	
		
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st7;
		//case20125929 start : for spanish conversion
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			//case20125929 end
			st1 = "POR TAREA ";
			st2 = "POR UCC ";
			st3 = "ENTRAR SELECCI&#211;N";
			st4 = "ENVIAR";
			st5 = "ANTERIOR";
			st7 = "OB MEN� DE INVERSI�N";
			
			
		}
		else
		{
			st1 = "BY TASK : ";
			st2 = "BY UCC : ";
			st3 = "ENTER SELECTION";
			st4 = "SEND";
			st5 = "PREV";
			st7 = "OB REVERSAL MENU";
			
		}
		//Case# 20148880 (LinkButton Functionality)
		var checkInURL_1 = nlapiResolveURL('SUITELET', 'customscript_rf_outboundreversalbytask', 'customdeploy_rf_outboundreversalbytask');
		var linkURL_1 = checkInURL_1; 
		var checkInURL_2 = nlapiResolveURL('SUITELET', 'customscript_rf_scanucclabel', 'customdeploy_rf_scanucclabel');
		var linkURL_2 = checkInURL_2; 
		
		var functionkeyHtml=getFunctionkeyScript('_rfpickingmenu'); 
		var html = "<html><head><title>"+ st7 +"</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('selectoption').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rfpickingmenu' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 1. "+ st1;//By Task :
		html = html + "				<td align = 'left'> 1. <a href='" + linkURL_1 + "' style='text-decoration: none'>" + st1 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'> 2. "+ st2;//By UCC :
		html = html + "				<td align = 'left'> 2. <a href='" + linkURL_2 + "' style='text-decoration: none'>" + st2 + "</a>";
		html = html + "				</td>";
		html = html + "			</tr> ";		
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3;//ENTER SELECTION";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='selectoption' id='selectoption' type='text'/>";
		html = html + "				</td>";
		html = html + "				<td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st4 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st5 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('selectoption').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else
	{
		nlapiLogExecution('DEBUG', 'Into Response','Into Response');

		var getLanguage =  request.getParameter('hdngetLanguage');
		
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7;
		
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st6 = "OPCI&#211;N V&#193;LIDA";
			st7 = "OB MEN� DE INVERSI�N";

			
			
		}
		else
		{
			st6 = "INVALID OPTION";
			st7 = "OB REVERSAL MENU";
		}
		var optedField = request.getParameter('selectoption');
		var optedEvent = request.getParameter('cmdPrevious');
		//case 20125929 start : for spanish conversion
		var SOarray=new Array();
		SOarray["custparam_language"] = getLanguage;
		SOarray["custparam_screenno"] = 'OBReversalMenu';
		//case 20125929 end :

		if (request.getParameter('cmdPrevious') == 'F7') {
			nlapiLogExecution('DEBUG', 'Clicked on Previous', request.getParameter('cmdPrevious'));
			response.sendRedirect('SUITELET', 'customscript_rf_picking_menu', 'customdeploy_rf_picking_menu_di', false, optedEvent);
		}
		else 
			if (optedField == '1') {
				response.sendRedirect('SUITELET', 'customscript_rf_outboundreversalbytask', 'customdeploy_rf_outboundreversalbytask', false, optedField);
			}
			else 
				if (optedField == '2') {
					response.sendRedirect('SUITELET', 'customscript_rf_scanucclabel', 'customdeploy_rf_scanucclabel', false, optedField);
				}
				else
				{
					/*var SOarray=new Array();
					SOarray["custparam_language"] = getLanguage;
					SOarray["custparam_screenno"] = 'OBReversalMenu';*/
					SOarray["custparam_error"] = st6;//"Invalid Option";
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				}
	}
}