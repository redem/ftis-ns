/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Suitelet/ebiz_QCscreening_SL.js,v $
 *     	   $Revision: 1.3.4.1.4.1.12.2 $
 *     	   $Date: 2015/10/06 10:25:58 $
 *     	   $Author: deepshikha $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_QCscreening_SL.js,v $
 * Revision 1.3.4.1.4.1.12.2  2015/10/06 10:25:58  deepshikha
 * 2015.2 issues
 * 201414592
 *
 * Revision 1.3.4.1.4.1.12.1  2015/09/21 11:25:41  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.3.4.1.4.1  2012/11/01 14:55:15  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.3.4.1  2012/04/27 07:50:21  gkalla
 * CASE201112/CR201113/LOG201121
 * QC check based on rule configuration
 *
 * Revision 1.3  2011/07/21 08:08:40  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/

function QcTestSuitelet(request, response)
{
	if (request.getMethod() == 'GET') 
	{   
		var form = nlapiCreateForm('QC Screening');
		var  vQbLpno = request.getParameter('custparam_lp');
		var  vQbLastval = request.getParameter('custparam_lastval');		
		var  vQbitemNo = request.getParameter('custparam_sku');
		var  vQbiteminternalid = request.getParameter('custparam_ebiz_sku_no');
		var  vQbopentaskid = request.getParameter('custparam_internalid');
		var  vQbcompid = request.getParameter('custparam_compid');
		var  vQbwmslocation = request.getParameter('custparam_wmslocation');
		var skustatus='';
		
	    var filternew = new Array();
	    filternew.push(new nlobjSearchFilter('custrecord_ebiz_sku_no', null, 'is', vQbiteminternalid));
	    filternew.push(new nlobjSearchFilter('custrecord_lpno', null, 'is', vQbLpno));
	    filternew.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [23]));
	    filternew.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]));
	    
	    var columns = new Array();
	    columns[0] = new nlobjSearchColumn('name');
	   // columns[1] = new nlobjSearchColumn('custrecord_sku');
	    columns[1] = new nlobjSearchColumn('custrecord_sku_status');

	    var searchres = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filternew, columns);
		 if(searchres != null && searchres != '' && searchres.length>0) {
	        
	        nlapiLogExecution('DEBUG', 'into status');
	        skustatus = searchres[0].getText('custrecord_sku_status');
	        
	    }
		nlapiLogExecution('ERROR','skustatus',skustatus);
		
		 
		var  vQbPono = request.getParameter('custparam_poid');

		var LPField = form.addField('custpage_lp', 'text', 'LP #').setDisplayType('readonly') ;
		LPField.setDefaultValue(vQbLpno);				

		var itemField = form.addField('custpage_item', 'text', 'Item #').setDisplayType('readonly');
		itemField.setDefaultValue(vQbitemNo);

		var iteminternalField = form.addField('custpage_iteminternalid', 'text', 'Item internalid');
		iteminternalField.setDisplayType('hidden');
		iteminternalField.setDefaultValue(vQbiteminternalid);

		var opentaskinternalField = form.addField('custpage_opentaskinternalid', 'text', 'opentask internalid');
		opentaskinternalField.setDisplayType('hidden');
		opentaskinternalField.setDefaultValue(vQbopentaskid);

		var PoField = form.addField('custpage_pono', 'text', 'PO #').setDisplayType('readonly');
		PoField.setDefaultValue(vQbPono);

		var ItemType = nlapiLookupField('item', vQbiteminternalid, 'recordType');
		var chknCompany = form.addField('custpage_compid', 'select', 'Company', 'customrecord_ebiznet_company');
		chknCompany.setDefaultValue(vQbcompid);
		chknCompany.setDisplayType('inline');

		var chknlocation = form.addField('custpage_location', 'select', 'WMS Location', 'Location');
		chknlocation .setDefaultValue(vQbwmslocation);

		var itemresults;

		if (ItemType == "assemblyitem") {
			itemresults = nlapiLoadRecord('assemblyitem', vQbiteminternalid);
		}
		else 
			if (ItemType == "inventoryitem") {
				itemresults = nlapiLoadRecord('inventoryitem', vQbiteminternalid);
			}
			else 
				if (ItemType == "serializedinventoryitem") {
					itemresults = nlapiLoadRecord('serializedinventoryitem', vQbiteminternalid);
				}
				else 
					if (ItemType == "lotnumberedinventoryitem") {
						itemresults = nlapiLoadRecord('lotnumberedinventoryitem', vQbiteminternalid);
					}
					else if (ItemType == "serializedassemblyitem") {
			itemresults = nlapiLoadRecord('serializedassemblyitem', vQbiteminternalid);
		}
		var ItemFamily = itemresults.getFieldValue('custitem_item_family');
		var  ItemGroup = itemresults.getFieldValue('custitem_item_group');

		var stratFilters = new Array();
		stratFilters.push(new nlobjSearchFilter('custrecord_qcrules_item', null, 'anyof', ['@NONE@', vQbiteminternalid]));
		if(ItemGroup != null && ItemGroup != '')
			stratFilters.push(new nlobjSearchFilter('custrecord_qcrules_item_group', null, 'anyof', ['@NONE@', ItemGroup]));
		if(ItemFamily != null && ItemFamily != '')
			stratFilters.push(new nlobjSearchFilter('custrecord_qcrules_item_family', null,'anyof', ['@NONE@', ItemFamily]));

		var stratColumns = new Array();
		stratColumns[0]= new nlobjSearchColumn('custrecord_qcrules_qc_validation');
		stratColumns[1]= new nlobjSearchColumn('custrecord_qcrules_item');
		stratColumns[1].setSort();
		stratColumns[2]= new nlobjSearchColumn('custrecord_qcrules_item_group');
		stratColumns[2].setSort();
		stratColumns[3]= new nlobjSearchColumn('custrecord_qcrules_item_family');
		stratColumns[3].setSort();

		var results = nlapiSearchRecord('customrecord_ebiznet_qc_rules', null, stratFilters, stratColumns);
		var vQcValidation="";
		var vQcValidationtext="";
		if(results!=null)
		{
			for (var s = 0; results != null && s < Math.min(500, results.length); s++) {
				vQcValidation=  results[s].getValue('custrecord_qcrules_qc_validation');
				vQcValidationtext=  results[s].getText('custrecord_qcrules_qc_validation');
			}
		}
		else{
			var formnew = nlapiCreateForm('QC Screening');
			var msg = formnew.addField('custpage_message', 'inlinehtml', null, null, null);
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'QC Rule not defined', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
			response.writePage(formnew);
			return;
		}
		if(vQcValidation!=null)
		{
			nlapiLogExecution('ERROR','vQcValidation',vQcValidation);
			nlapiLogExecution('ERROR','vQcValidation',vQcValidationtext);
			var currval="";
			var currvaltext="";
			var arrval=vQcValidation.split(',');
			var arrvaltext=vQcValidationtext.split(',');
			var valcnt=arrval.length;
			var nextvalexists='F';
			if(vQbLastval==null || vQbLastval=="" )
			{
				currval=arrval[0];
				currvaltext=arrvaltext[0];
				if(arrval.length>1)
				{
					nextvalexists='T';
				}
			}
			else
			{
				var nextvval=false;
				for (n = 0; n < arrval.length; n++) {
					if(nextvval)
					{
						currval=arrval[n];
						currvaltext=arrvaltext[n];	
						if(arrval.length>(n+1))
						{
							nextvalexists='T';
						}	
					}
					if(vQbLastval==arrval[n])
					{
						nextvval=true;
					}
				}
			}
			
			var filternew1 = new Array(); 
			var skustatusvalid='';
			 filternew1.push(new nlobjSearchFilter('internalid', null, 'is', vQcValidation));
			 filternew1.push(new nlobjSearchFilter('custrecord_validation_name', null, 'is', vQcValidationtext));

			 var columns = new Array();
			    columns[0] = new nlobjSearchColumn('name');
			    columns[1] = new nlobjSearchColumn('custrecord_pass_status');

			var searchrec = nlapiSearchRecord('customrecord_ebiznet_qc_validations', null, filternew1, columns);
			nlapiLogExecution('DEBUG', 'into status 2',searchrec.length);
			 if(searchrec != null && searchrec != '' && searchrec.length>0) {
			        
			        nlapiLogExecution('DEBUG', 'into status 2');
			        skustatusvalid = searchrec[0].getText('custrecord_pass_status');
			        
			    }
				nlapiLogExecution('ERROR','skustatusvalid',skustatusvalid);
			nlapiLogExecution('ERROR','currval',currval);				 
			var columns = nlapiLookupField('customrecord_ebiznet_qc_validations', currval, 'custrecord_validation_name');
			currvaltext = columns;
			nlapiLogExecution('ERROR','currvaltext',currvaltext);

			var sublist = form.addSubList("custpage_items", "list", currvaltext);
			sublist.addField("custpage_slno", "text", "Sl.No").setDisplayType('inline');
			sublist.addField("custpage_qc_item", "text", "QC Item").setDisplayType('inline');
			sublist.addField("custpage_qc_value", "select", "QC Value",'customlist_ebiznet_qc_value').setDisplayType('entry');;
			sublist.addField("custpage_result", "text", "Result").setDisplayType('inline');				
			sublist.addField("custpage_notes", "text", "Notes").setDisplayType('entry');
			sublist.addField("custpage_qcvalhidden", "text", "qc value").setDisplayType('hidden');
			sublist.addField("custpage_qcinternid", "text", "qc internid").setDisplayType('hidden');

			var servitemfield=sublist.addField("custpage_servitem", "select", "Service Item").setDisplayType('entry');
			sublist.addField("custpage_quantity", "text", "Quantity").setDisplayType('entry');
			fillServiceItem(form, servitemfield,-1);
			var filtersQc = new Array();		
			filtersQc[0] = new nlobjSearchFilter('custrecord_validation_level', null, 'anyof', currval);	

			var columnsQc = new Array();
			columnsQc[0] = new nlobjSearchColumn('custrecord_qcdescription');
			columnsQc[1] = new nlobjSearchColumn('custrecord_qc_value');		
			columnsQc[2] = new nlobjSearchColumn('custrecord_validation_level');

			var searchresultsQc = nlapiSearchRecord('customrecord_ebiznet_qc_validation_check', null, filtersQc,columnsQc);

			for (var s = 0; searchresultsQc != null && s < Math.min(500, searchresultsQc.length); s++) {
				form.getSubList('custpage_items').setLineItemValue('custpage_slno', s+1, s+1);
				form.getSubList('custpage_items').setLineItemValue('custpage_qc_item', s+1, searchresultsQc[s].getValue('custrecord_qcdescription'));
				form.getSubList('custpage_items').setLineItemValue('custpage_qc_value', s+1, searchresultsQc[s].getValue('custrecord_qc_value'));	
				if(skustatus!=skustatusvalid)
					{
					form.getSubList('custpage_items').setLineItemValue('custpage_result', s+1,  'FAIL');
					}
				else
					{
					form.getSubList('custpage_items').setLineItemValue('custpage_result', s+1,  'PASS');
					}
				
				form.getSubList('custpage_items').setLineItemValue('custpage_notes', s+1,  '');
				form.getSubList('custpage_items').setLineItemValue('custpage_qcvalhidden', s+1,searchresultsQc[s].getValue('custrecord_qc_value'));
				form.getSubList('custpage_items').setLineItemValue('custpage_qcinternid', s+1,searchresultsQc[s].getId());
			}	
		}

		nlapiLogExecution('ERROR','vQbLpno',vQbLpno);
		nlapiLogExecution('ERROR','vQbitemNo',vQbitemNo);
		nlapiLogExecution('ERROR','vQbiteminternalid',vQbiteminternalid);
		nlapiLogExecution('ERROR','vQbopentaskid',vQbopentaskid);
		nlapiLogExecution('ERROR','vQbPono',vQbPono);

		//customrecord_ebiznet_qc_rules
		var nextval = form.addField('custpage_nextval', 'text', 'Next Val').setDisplayType('hidden');
		nextval.setDefaultValue(nextvalexists);
		nlapiLogExecution('ERROR','nextvalexists',nextvalexists);
		var QcvalId = form.addField('custpage_qcvalid', 'text', 'Next Val').setDisplayType('hidden');
		QcvalId.setDefaultValue(currval);

		var autobutton = form.addSubmitButton('Save');
		var resetbtn = form.addResetButton('Reset');
		response.writePage(form);
	}
	else
	{
		//try {
		var form = nlapiCreateForm('QC Screening');
		var vqclevel = request.getParameter('custpage_qclevel');
		var vpoid = request.getParameter('custpage_pono');
		var vLpno = request.getParameter('custpage_lp');				
		var vqcitem = request.getParameter('custpage_item');
		var vqciteminternid = request.getParameter('custpage_iteminternalid');
		var vqcopentaskinternid = request.getParameter('custpage_opentaskinternalid');
		var vqnextvalExists = request.getParameter('custpage_nextval');
		var vQCValue = request.getParameter('custpage_qcvalid');
		var vnextval = request.getParameter('custpage_nextval');
		var vCompany = request.getParameter('custpage_compid');
		var vwmsLocation = request.getParameter('custpage_location');				

		nlapiLogExecution('ERROR','vqclevel',vqclevel);
		nlapiLogExecution('ERROR','vLpno',vLpno);
		nlapiLogExecution('ERROR','vqcitem',vqcitem);
		nlapiLogExecution('ERROR','vqcopentaskinternid',vqcopentaskinternid);
		nlapiLogExecution('ERROR','vQCValue',vQCValue);	

		//var vitemstatus='PASS';	
		
		var vitemstatus=''	;
		var lineCnt = request.getLineItemCount('custpage_items');
		nlapiLogExecution('ERROR','lineCnt',lineCnt);
		for (var s = 1; s <= lineCnt; s++) {
			var vinternid= request.getLineItemValue('custpage_items', 'custpage_qcinternid', s);
			var vqcscreeningval= request.getLineItemValue('custpage_items', 'custpage_qc_value', s);				   
			var selectedval = request.getLineItemValue('custpage_items', 'custpage_qc_value',s);
			var passval = request.getLineItemValue('custpage_items', 'custpage_qcvalhidden',s);
			var vqcNotes = request.getLineItemValue('custpage_items', 'custpage_notes',s);
			var vServQty = request.getLineItemValue('custpage_items', 'custpage_quantity',s);
			var vServItem = request.getLineItemValue('custpage_items', 'custpage_servitem',s);
			var resultline = request.getLineItemValue('custpage_items', 'custpage_result',s);
			//var status="";					
			/*if (selectedval == passval) {
				status='PASS';
			}
			else
			{
				vitemstatus='FAIL';
				status='FAIL';
			}*/
			nlapiLogExecution('ERROR','resultline',resultline);
			vitemstatus = resultline;
			nlapiLogExecution('ERROR','vitemstatus',vitemstatus);

			var qcscreening = nlapiCreateRecord('customrecord_ebiznet_qc_screening');
			qcscreening.setFieldValue('name', 'QC Test');
			qcscreening.setFieldValue('custrecord_qc_lp_no', vLpno);
			qcscreening.setFieldValue('custrecord_qc_validation', vqclevel);
			//qcscreening.setFieldValue('custrecord_qc_validation_check', selectedval);
			qcscreening.setFieldValue('custrecord_qc_validation_check', vinternid);
			qcscreening.setFieldValue('custrecord_qc_screening_value', vqcscreeningval);
			//qcscreening.setFieldValue('custrecord_qc_result', status);
			qcscreening.setFieldValue('custrecord_qc_result', resultline);

			qcscreening.setFieldValue('custrecord_qc_item', vqciteminternid);
			qcscreening.setFieldValue('custrecord_qcnotes', vqcNotes);					 
			qcscreening.setFieldValue('custrecord_qc_compid', vCompany);
			qcscreening.setFieldValue('custrecord_qc_location', vwmsLocation);
			if(vServItem != null && vServItem != '')
				qcscreening.setFieldValue('custrecord_qcservitem', vServItem);
			if(vServQty != null && vServQty != '')
				qcscreening.setFieldValue('custrecord_qcservqty', parseFloat(vServQty).toFixed(5));
			nlapiSubmitRecord(qcscreening);		
		}

		var filtersval = new Array();		
		filtersval[0] = new nlobjSearchFilter('custrecord_validation_name', null, 'is', vqclevel);		

		var passstatus="";
		var failstatus="";
		var proceednextval="";
		var fields=new Array();
		fields[0]='custrecord_pass_status';
		fields[1]='custrecord_fail_status';
		fields[2]='custrecord_next_validation_if_result';
		var columns = nlapiLookupField('customrecord_ebiznet_qc_validations', vQCValue, fields);
		passstatus=columns.custrecord_pass_status;
		failstatus=columns.custrecord_fail_status;
		proceednextval=columns.custrecord_next_validation_if_result;

		nlapiLogExecution('ERROR','vitemstatus',vitemstatus);
		nlapiLogExecution('ERROR','passstatus',passstatus);
		nlapiLogExecution('ERROR','failstatus',failstatus);
		opentaskrec=nlapiLoadRecord('customrecord_ebiznet_trn_opentask',vqcopentaskinternid);
		var strmessage="";
		var vitemCode;
		var msg= form.addField('custpage_message', 'inlinehtml', null, null, null); 

		if (vitemstatus == 'PASS') {
			nlapiLogExecution('ERROR','Opentask Pass status update');
			opentaskrec.setFieldValue('custrecord_wms_status_flag', '6');
			strmessage="Quality Check Passed";
			vitemCode=1;
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', '"+ strmessage +"', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");

			
		}
		else {
			nlapiLogExecution('ERROR','Opentask Fail status update');
			opentaskrec.setFieldValue('custrecord_wms_status_flag', '6');
			opentaskrec.setFieldValue('custrecord_sku_status', failstatus);
			strmessage="Quality Check Failed";
			vitemCode=2;
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', '"+ strmessage +"', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");

			
			
		}
		nlapiSubmitRecord(opentaskrec); 

		/*var msg= form.addField('custpage_message', 'inlinehtml', null, null, null); 
		if(strmessage=="Quality Check Failed")
			{
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', '"+ strmessage +"', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
			}
		else if(strmessage="Quality Check Passed")
			{
		msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', '"+ strmessage +"', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
			}*/
		response.writePage(form);

		nlapiLogExecution('ERROR','vnextval',vnextval);
		nlapiLogExecution('ERROR','vitemCode',vitemCode);

		nlapiLogExecution('ERROR','proceednextval',proceednextval);
		if(proceednextval==vitemCode && vnextval=="T" )
		{
			var Qcparam = new Array();
			Qcparam ["custparam_poid"] = vpoid;
			Qcparam ["custparam_sku"]=vqcitem;
			Qcparam ["custparam_lp"]=vLpno;
			Qcparam ["custparam_internalid"]=vqcopentaskinternid;
			Qcparam ["custparam_ebiz_sku_no"]=vqciteminternid;
			Qcparam ["custparam_lastval"]=vQCValue;
			Qcparam ["custparam_compid"]=vCompany;
			Qcparam ["custparam_wmslocation"]=vwmsLocation;

			response.sendRedirect('SUITELET', 'customscript_qcscreening', 'customdeploy_qcscreening', false, Qcparam );
		}
	}
}

function ebiznet_qcvalueValdation_CL(type, name){
	if (name == 'custpage_items') {
		var selectedval = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_qc_value');
		var passval = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_qcvalhidden');
		alert(selectedval);
		alert(passval);
		if(selectedval==passval)
		{
			nlapiSetLineItemValue('custpage_items', 'custpage_result','1','PASS');
		}
		else
		{
			nlapiSetLineItemValue('custpage_items', 'custpage_result','1','FAIL');
		}
	}
	if (name == 'custpage_lp') {
		var selLP=nlapiGetFieldText('custpage_lp');
		alert(selLP);
		alert(name);
		var filtersitem = new Array();
		filtersitem[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'is', '23');
		filtersitem[1] = new nlobjSearchFilter('custrecord_lpno', null, 'is', selLP);	
		var itemField=nlapiGetField(name);	
		itemField.addSelectOption("", "");

		var columnsitem = new Array();
		columnsitem[0] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columnsitem[1] = new nlobjSearchColumn('custrecord_sku');	  	

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersitem, columnsitem);

		for (var i = 0; searchresults != null && i < Math.min(500, searchresults.length); i++) {
			//Searching for Duplicates		
			var res = form.getField('custpage_item').getSelectOptions(searchresults[i].getValue('custrecord_ebiz_sku_no'), 'is');
			if (res != null) {
				if (res.length > 0) {
					continue;
				}
			}
			itemField.addSelectOption(searchresults[i].getValue('custrecord_ebiz_sku_no'), searchresults[i].getValue('custrecord_sku'));
		}
	}

	return true;
}

function fillServiceItem(form, ServiceItemField,maxno){
	var filtersso = new Array();		

	if(maxno!=-1)
	{
		filtersso.push(new nlobjSearchFilter('internalid', null, 'lessthan', parseFloat(maxno)));
	}

	var columnsinvt = new Array();
	columnsinvt[0] = new nlobjSearchColumn('internalid');	 
	columnsinvt[1] = new nlobjSearchColumn('itemid');
	columnsinvt[0].setSort(true);
	ServiceItemField.addSelectOption("", "");


	var customerSearchResults = nlapiSearchRecord('serviceitem', null, filtersso,columnsinvt);

	for (var i = 0; customerSearchResults != null && i < customerSearchResults.length; i++) {

		//nlapiLogExecution('ERROR', 'tranid', customerSearchResults[i].getValue('tranid')); 
		if(customerSearchResults[i].getValue('itemid') != null && customerSearchResults[i].getValue('itemid') != "" && customerSearchResults[i].getValue('itemid') != " ")
		{
			var resdo = ServiceItemField.getSelectOptions(customerSearchResults[i].getValue('itemid'), 'is');
			if (resdo != null) {
				if (resdo.length > 0) {
					continue;
				}
			}
		}
		ServiceItemField.addSelectOption(customerSearchResults[i].getValue('internalid'), customerSearchResults[i].getValue('itemid'));
	}

	if(customerSearchResults!=null && customerSearchResults.length>=1000)
	{
		var maxno=customerSearchResults[customerSearchResults.length-1].getValue('internalid');		
		fillServiceItem(form, ServiceItemField,maxno);	
	}
}