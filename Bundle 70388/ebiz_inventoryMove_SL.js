/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/ebiz_inventoryMove_SL.js,v $

 *     	   $Revision: 1.30.2.26.4.11.2.50.2.4 $
 *     	   $Date: 2015/11/27 22:11:48 $
 *     	   $Author: skreddy $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
=======
 *     	   $Revision: 1.30.2.26.4.11.2.50.2.4 $
 *     	   $Date: 2015/11/27 22:11:48 $
 *     	   $Author: skreddy $
=======
 *     	   $Revision: 1.30.2.26.4.11.2.50.2.4 $
 *     	   $Date: 2015/11/27 22:11:48 $
 *     	   $Author: skreddy $
>>>>>>> 1.30.2.26.4.11.2.25
=======
 *     	   $Revision: 1.30.2.26.4.11.2.50.2.4 $
 *     	   $Date: 2015/11/27 22:11:48 $
 *     	   $Author: skreddy $
>>>>>>> 1.30.2.26.4.11.2.26
=======
 *     	   $Revision: 1.30.2.26.4.11.2.50.2.4 $
 *     	   $Date: 2015/11/27 22:11:48 $
 *     	   $Author: skreddy $
>>>>>>> 1.30.2.26.4.11.2.28
=======
 *     	   $Revision: 1.30.2.26.4.11.2.50.2.4 $
 *     	   $Date: 2015/11/27 22:11:48 $
 *     	   $Author: skreddy $
>>>>>>> 1.30.2.26.4.11.2.31
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
>>>>>>> 1.30.2.26.4.11.2.22
=======

 *     	   $Revision: 1.30.2.26.4.11.2.50.2.4 $
 *     	   $Date: 2015/11/27 22:11:48 $
 *     	   $Author: skreddy $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $

>>>>>>> 1.30.2.26.4.11.2.41
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_inventoryMove_SL.js,v $
 * Revision 1.30.2.26.4.11.2.50.2.4  2015/11/27 22:11:48  skreddy
 * case# 201415832
 * PCT prod issue fix
 *
 * Revision 1.30.2.26.4.11.2.50.2.3  2015/10/13 15:22:41  grao
 * 2015.2 Issue Fixes 201414982
 *
 * Revision 1.30.2.26.4.11.2.50.2.2  2015/10/05 07:49:38  sponnaganti
 * 201414823
 * LP Prod issue fix
 *
 * Revision 1.30.2.26.4.11.2.50.2.1  2015/09/21 14:02:17  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.30.2.26.4.11.2.50  2015/07/10 13:27:59  schepuri
 * case# 201413357
 *
 * Revision 1.30.2.26.4.11.2.49  2015/06/10 14:27:34  schepuri
 * case# 201413037
 *
 * Revision 1.30.2.26.4.11.2.48  2015/05/02 02:29:53  sponnaganti
 * Case# 201412592
 * LP SB1 Issue fix
 * while checking given location is there in rolelocation getting false value if we are not checking with
 * parseFloat condition, so added parseFloat to the if condition
 *
 * Revision 1.30.2.26.4.11.2.47  2015/04/17 15:28:58  grao
 * SB issue fixes  201412377
 *
 * Revision 1.30.2.26.4.11.2.46  2015/04/13 09:25:58  rrpulicherla
 * Case#201412277
 *
 * Revision 1.30.2.26.4.11.2.45  2015/04/10 21:28:09  skreddy
 * Case# 201412323�
 * changed the url path which was hard coded
 *
 * Revision 1.30.2.26.4.11.2.44  2015/04/06 13:19:12  schepuri
 * case# 201412253
 *
 * Revision 1.30.2.26.4.11.2.43  2014/09/25 18:02:06  skreddy
 * case # 201410359
 * TPP SB issue fix
 *
 * Revision 1.30.2.26.4.11.2.42  2014/09/19 19:05:37  grao
 * Case#: 201410460 CT issue Sb fixes
 *
 * Revision 1.30.2.26.4.11.2.41  2014/09/19 11:33:56  skavuri
 * case # 201410310
 *
 * Revision 1.30.2.26.4.11.2.40  2014/09/16 14:57:12  skreddy
 * case # 201410300
 * True Fab SB issue fix
 *
 * Revision 1.30.2.26.4.11.2.39  2014/09/02 11:08:58  gkalla
 * case#201220844
 * PCT Inventory move GUI wrong updating actual qty in open task
 *
 * Revision 1.30.2.26.4.11.2.38  2014/08/14 10:02:14  sponnaganti
 * case# 20149950
 * stnd bundle issue fix
 *
 * Revision 1.30.2.26.4.11.2.37  2014/08/06 15:38:31  sponnaganti
 * case# 20149825
 * Stnd Bundle issue fix
 *
 * Revision 1.30.2.26.4.11.2.36  2014/07/28 16:09:29  sponnaganti
 * Case# 20149636
 * Compatibility Issue fix
 *
 * Revision 1.30.2.26.4.11.2.35  2014/07/24 15:02:18  sponnaganti
 * Case# 20149636
 * Compatibility Issue fix
 *
 * Revision 1.30.2.26.4.11.2.34  2014/07/09 12:41:22  rmukkera
 * Case # 20149224
 * jawbone issues applicable to standard also
 *
 * Revision 1.30.2.26.4.11.2.33  2014/06/19 15:46:54  sponnaganti
 * case# 20148996
 * Compatability test acc issue fixes
 *
 * Revision 1.30.2.26.4.11.2.32  2014/06/12 14:34:01  grao
 * Case#: 20148839  New GUI account issue fixes
 *
 * Revision 1.30.2.26.4.11.2.31  2014/06/05 15:42:50  nneelam
 * case#  20148727
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.30.2.26.4.11.2.30  2014/05/29 15:37:37  sponnaganti
 * case# 20148582
 * Stnd Bundle Issue fix
 *
 * Revision 1.30.2.26.4.11.2.29  2014/05/22 06:57:51  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148482
 *
 * Revision 1.30.2.26.4.11.2.28  2014/04/22 15:54:22  skavuri
 * Case # 20148123 SB Issue fixed
 *
 * Revision 1.30.2.26.4.11.2.27  2014/04/07 15:45:08  nneelam
 * Case# 20140497
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.30.2.26.4.11.2.26  2014/02/21 15:55:43  nneelam
 * case#  20127235
 * Standard Bundle Issue Fix.
 *
 * Revision 1.30.2.26.4.11.2.25  2014/01/30 15:23:41  rmukkera
 * Case # 20127018
 *
 * Revision 1.30.2.26.4.11.2.24  2014/01/29 15:23:12  rmukkera
 * Case # 20126967 ,20126957
 *
 * Revision 1.30.2.26.4.11.2.23  2014/01/21 14:30:56  rmukkera
 * Case # 20126884
 *
 * Revision 1.30.2.26.4.11.2.22  2013/12/26 14:26:03  schepuri
 * 20126516
 *
 * Revision 1.30.2.26.4.11.2.21  2013/12/24 14:12:47  schepuri
 * 20126516
 *
 * Revision 1.30.2.26.4.11.2.20  2013/12/20 15:42:59  rmukkera
 * Case # 20126422 ,20126039,20126344
 *
 * Revision 1.30.2.26.4.11.2.19  2013/12/12 15:36:58  gkalla
 * case#20126051
 * MHP issue in inventory move cube updation
 *
 * Revision 1.30.2.26.4.11.2.18  2013/12/10 14:28:16  snimmakayala
 * Case# : 20126051
 * MHP UAT Fixes.
 *
 * Revision 1.30.2.26.4.11.2.17  2013/12/06 15:12:30  skreddy
 * Case# 20126182
 * 2014.1 stnd bundle issue fix
 *
 * Revision 1.30.2.26.4.11.2.16  2013/10/22 15:05:37  skreddy
 * Case# 20125155
 * standard bundle issue fix
 *
 * Revision 1.30.2.26.4.11.2.15  2013/09/19 15:19:47  rmukkera
 * Case# 20124445
 *
 * Revision 1.30.2.26.4.11.2.14  2013/08/21 14:19:56  skreddy
 * Case# 20123285
 * expiry date in not updating after innventory move
 *
 * Revision 1.30.2.26.4.11.2.13  2013/06/24 15:33:33  skreddy
 * CASE201112/CR201113/LOG201121
 * standard bundle issue fixes
 *
 * Revision 1.30.2.26.4.11.2.12  2013/06/19 22:57:21  gkalla
 * CASE201112/CR201113/LOG201121
 * As part of optimistic locking
 *
 * Revision 1.30.2.26.4.11.2.11  2013/06/05 22:11:20  spendyala
 * CASE201112/CR201113/LOG201121
 * FIFO field updating with data stamp is removed.
 *
 * Revision 1.30.2.26.4.11.2.10  2013/05/09 12:53:43  spendyala
 * CASE201112/CR201113/LOG2012392
 * Issue related to invt move.
 *
 * Revision 1.30.2.26.4.11.2.9  2013/04/22 15:38:57  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.30.2.26.4.11.2.8  2013/04/02 16:02:08  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * TSg Issue fixes
 *
 * Revision 1.30.2.26.4.11.2.7  2013/03/19 11:53:20  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.30.2.26.4.11.2.6  2013/03/15 09:28:45  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.30.2.26.4.11.2.5  2013/03/08 14:43:37  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.30.2.26.4.11.2.4  2013/03/05 13:35:46  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.30.2.26.4.11.2.3  2013/03/01 14:34:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.30.2.26.4.11.2.2  2013/02/27 13:25:06  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Merged from Monobind.
 *
 * Revision 1.30.2.26.4.11.2.1  2013/02/27 13:06:28  rmukkera
 * monobid production bundle changes were merged to cvs with tag
 * t_eBN_2013_1_StdBundle_2
 *
 * Revision 1.30.2.26.4.11  2013/02/11 17:01:22  kavitha
 * CASE201112/CR201113/LOG201121
 * Serial # functionality - Inventory process - Gaps fixes
 *
 * Revision 1.30.2.26.4.10  2013/02/06 01:04:43  kavitha
 * CASE201112/CR201113/LOG201121
 * Serial # functionality - Inventory process
 *
 * Revision 1.30.2.26.4.9  2013/01/24 03:05:27  kavitha
 * CASE201112/CR201113/LOG201121
 * Converted Bin Location,LP,Location dropdown - DBCombo
 *
 * Revision 1.30.2.26.4.8  2013/01/11 13:24:01  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related Intenventory move for different lots
 *
 * Revision 1.30.2.26.4.7  2013/01/09 15:19:34  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Pickface Location Vs Item validation.
 * .
 *
 * Revision 1.30.2.26.4.6  2012/12/11 03:06:44  kavitha
 * CASE201112/CR201113/LOG201121
 * Incorporated Serial # functionality
 *
 * Revision 1.30.2.26.4.5  2012/11/28 15:05:10  schepuri
 * CASE201112/CR201113/LOG201121
 * Auto gen loc is not displayed
 *
 * Revision 1.30.2.26.4.4  2012/11/01 14:54:57  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.30.2.26.4.3  2012/10/11 10:27:04  gkalla
 * CASE201112/CR201113/LOG201121
 * Pick confirmation and other inventory issues
 *
 * Revision 1.30.2.26.4.2  2012/10/01 05:18:40  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code from 2012.2.
 *
 * Revision 1.30.2.26.4.1  2012/09/24 22:46:09  spendyala
 * CASE201112/CR201113/LOG201121
 * New item status is not populating the item status is resolved.
 *
 * Revision 1.30.2.26  2012/09/13 23:02:14  spendyala
 * CASE201112/CR201113/LOG201121
 * While merging Inventory WMS status flag should be either inventory storage or putaway complete.
 *
 * Revision 1.30.2.25  2012/09/05 11:11:38  schepuri
 * CASE201112/CR201113/LOG201121
 * added date stamp
 *
 * Revision 1.30.2.24  2012/08/29 16:21:35  spendyala
 * CASE201112/CR201113/LOG201121
 * Site and Bin location(Line Level) are not showing all the records so added fetching more than 1000 records.
 *
 * Revision 1.30.2.23  2012/08/29 00:55:24  gkalla
 * CASE201112/CR201113/LOG201121
 * To populate bin locations of morethan 1000
 *
 * Revision 1.30.2.22  2012/08/16 16:38:18  spendyala
 * CASE201112/CR201113/LOG201121
 * Issues fix for the 339 bundle i.e,
 * Inventory move Issue was resolved.
 *
 * Revision 1.30.2.21  2012/08/11 00:01:09  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.30.2.20  2012/08/10 17:47:52  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.30.2.19  2012/07/30 16:51:09  gkalla
 * CASE201112/CR201113/LOG201121
 * To get all Bin locations more than 1000
 *
 * Revision 1.30.2.18  2012/07/02 06:29:45  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to Expected date while creating inventory record is resolved.
 *
 * Revision 1.30.2.17  2012/06/27 06:26:20  skreddy
 * no message
 *
 * Revision 1.30.2.16  2012/06/26 13:30:42  skreddy
 * CASE201112/CR201113/LOG201121
 * Added AutoGenerate New bin Loc  Button
 *
 * Revision 1.30.2.15  2012/06/04 12:56:30  schepuri
 * CASE201112/CR201113/LOG201121
 * loc type filter added to new binloc dropdoen
 *
 * Revision 1.30.2.14  2012/05/25 22:30:16  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.30.2.13  2012/05/24 12:57:35  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fix in moving complete qty
 *
 * Revision 1.30.2.12  2012/05/14 12:09:53  rrpulicherla
 * Inventory Move
 *
 * Revision 1.30.2.11  2012/05/07 16:34:37  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Create Inventory
 *
 * Revision 1.30.2.10  2012/05/07 12:16:24  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Create Inventory
 *
 * Revision 1.30.2.9  2012/04/30 11:38:54  spendyala
 * CASE201112/CR201113/LOG201121
 * While Searching of Item in ItemMaster,
 * 'name' filter is changed to 'nameinternal' and also checking weather the item is inactive state or not.
 *
 * Revision 1.30.2.8  2012/04/20 13:30:13  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.30.2.7  2012/04/11 12:16:47  gkalla
 * CASE201112/CR201113/LOG201121
 * True lot functionality
 *
 * Revision 1.30.2.6  2012/03/02 01:32:13  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.30.2.5  2012/02/27 10:46:33  spendyala
 * CASE201112/CR201113/LOG201121
 * Instead of Alert msg showing error screen when ever user choose an bin location whose cube capacity is zero or less that zero.
 *
 * Revision 1.30.2.4  2012/02/23 00:27:34  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * supress lp in Invtmove and lp merge based on fifodate
 *
 * Revision 1.30.2.3  2012/02/16 15:00:31  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Invtmove
 *
 * Revision 1.30.2.2  2012/02/10 15:41:11  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Physical and Virtual Location changes
 *
 * Revision 1.32  2012/02/10 15:38:02  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Physical and Virtual Location changes
 *
 * Revision 1.31  2012/02/08 07:23:25  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Invt Move
 *
 * Revision 1.30  2011/12/28 23:20:53  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Inventory Move Changes
 *
 * Revision 1.29  2011/12/15 07:45:13  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.28  2011/12/14 14:31:00  mbpragada
 * CASE201112/CR201113/LOG201121
 * changed itemqty
 *
 * Revision 1.27  2011/12/13 21:06:04  gkalla
 * CASE201112/CR201113/LOG201121
 * Add new fields in opentask
 *
 * Revision 1.26  2011/12/12 13:04:13  snimmakayala
 * CASE201112/CR201113/LOG201121
 * spelling corrections in success message
 *
 * Revision 1.25  2011/12/08 13:45:28  gkalla
 * CASE201112/CR201113/LOG201121
 * Added Item status drop down
 *
 * Revision 1.24  2011/12/08 13:31:26  gkalla
 * CASE201112/CR201113/LOG201121
 * Added Item status drop down
 *
 * Revision 1.23  2011/12/08 11:15:38  gkalla
 * CASE201112/CR201113/LOG201121
 * uncommented the make warehouse flag
 *
 * Revision 1.22  2011/12/05 14:58:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.21  2011/11/25 20:38:56  gkalla
 * CASE201112/CR201113/LOG201121
 * multi select Item list
 *
 * Revision 1.20  2011/11/25 07:34:05  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Converted Item List to Multiselect
 *
 * Revision 1.12  2011/10/13 12:26:50  gkalla
 * CASE201112/CR201113/LOG201121
 * For Inventory move XFER
 *
 * Revision 1.11  2011/10/12 15:51:08  gkalla
 * CASE201112/CR201113/LOG201121
 * For Inventory move XFER
 *
 * Revision 1.10  2011/10/12 14:24:31  gkalla
 * CASE201112/CR201113/LOG201121
 * For Inventory move XFER
 *
 * Revision 1.9  2011/10/12 11:59:52  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Inventory Move  Files
 *
 * Revision 1.8  2011/10/12 10:08:43  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Inventory Move  Files
 *
 * Revision 1.7  2011/07/21 05:39:06  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/
var tempInventoryResultsArray=new Array();
function getInventorySearchResults(maxno,request)
{
	var binlocid = request.getParameter('custpage_binlocation');
	nlapiLogExecution('DEBUG', 'binLoc Internal ID', binlocid);

	var itemId = request.getParameter('custpage_item');
	var vItemArr=new Array();
	if(itemId != null && itemId != "")
	{
		vItemArr = itemId.split('');
	}
	nlapiLogExecution('ERROR', 'item Internal ID', vItemArr);
	var LPvalue =null;
	if(request.getParameter('custpage_invtlpid')!='' && request.getParameter('custpage_invtlpid')!=null)
	{
		LPvalue=request.getParameter('custpage_invtlpid');	
	}
	else
	{
		LPvalue = request.getParameter('custpage_invtlp');
	}
	//Case # 20127018  Start
	if(LPvalue!=null && LPvalue!='' && LPvalue!='null' && LPvalue!='undefined')
	{//Case # 20127018  End
		var Id=request.getParameter('custpage_invtlp');
		LPvalue= nlapiLookupField('customrecord_ebiznet_master_lp',LPvalue,'name');
	}
	nlapiLogExecution('ERROR', 'LP value', LPvalue);
	//case20126182 start 
	/*var lpname = "";
	if(LPvalue !=null && LPvalue !=""){
		var fields = ['custrecord_ebiz_lpmaster_lp'];
		var columns = nlapiLookupField('customrecord_ebiznet_master_lp', LPvalue, fields);
		lpname = columns.custrecord_ebiz_lpmaster_lp;
	}
	LPvalue = lpname; */
	//case20126182 end

	var vStatus = request.getParameter('custpage_itemstatus');
	nlapiLogExecution('DEBUG', 'Item Status ', vStatus);

	var i = 0;
	var filtersinv = new Array();
	if (binlocid!=null && binlocid != "") {
		filtersinv[i] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', binlocid);
		i++;
	}
	if (vItemArr!=null && vItemArr != "") {
		filtersinv[i] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', vItemArr);
		i++;
	}
	if (LPvalue!=null && LPvalue != "") {
		filtersinv[i] = new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', LPvalue);
		i++;
	}

	if (request.getParameter('custpage_location') != "" && request.getParameter('custpage_location') != null) {
		//filtersinv[i] = new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'is', request.getParameter('custpage_location'));
		//case# 20148582 starts
		var vRoleLocation=getRoledBasedLocationNew();
		nlapiLogExecution('ERROR', 'vRoleLocation', vRoleLocation);
		if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0){
			//case# 20149636 starts (displaying all location values when we select one location)
			//if(vRoleLocation.indexOf(request.getParameter('custpage_location'))!=-1)
			nlapiLogExecution('ERROR', 'vRoleLocation.indexOf(request.getParameter(custpage_location))!=-1', vRoleLocation.indexOf(request.getParameter('custpage_location'))!=-1);
			nlapiLogExecution('ERROR', 'vRoleLocation.indexOf(parseFloat(request.getParameter(custpage_location)))!=-1', vRoleLocation.indexOf(parseFloat(request.getParameter('custpage_location')))!=-1);
			//getting false value if we are not checking with parseFloat condition, so added parseFloat to the if condition
			//if(vRoleLocation.indexOf(request.getParameter('custpage_location'))!=-1)// case# 201412253
			if(vRoleLocation.indexOf(parseFloat(request.getParameter('custpage_location')))!=-1)
			{
				nlapiLogExecution('DEBUG','request.getParameter(custpage_location)',request.getParameter('custpage_location'));
				filtersinv[i] = new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',request.getParameter('custpage_location'));				
				i++;
			}
			//case# 20149636 ends
			//case# 20149949 starts(when selected location is not available in vRoleLocatio)
			else
			{
				filtersinv[i] = new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',["@NONE@"]);				
				i++;
			}
			//case# 20149949 ends

		}
		//case# 20149636 starts(when vRoleLocation is zero in else we are added filter)
		else
		{
			filtersinv[i] = new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof',request.getParameter('custpage_location'));				
			i++;
		}//case# 20149636 ends
		//case# 20148582 ends

	}
	if (request.getParameter('custpage_company') != "" && request.getParameter('custpage_company') != null) {
		filtersinv[i] = new nlobjSearchFilter('custrecord_ebiz_inv_company', null, 'is', request.getParameter('custpage_company'));
		i++;
	}
	if (vStatus != null && vStatus != "") {
		filtersinv[i] = new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', vStatus);
		i++;
	}

	filtersinv[i] = new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0);
	i++;
	/* The below code is merged from Lexjet production account on 03-02-2013 by Radhika as part of Standard bundle*/
	filtersinv[i] = new nlobjSearchFilter('isinactive', 'custrecord_ebiz_inv_binloc', 'is', 'F');
	i++;
	/* Up to here */ 
	filtersinv[i] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['3','19']);
	if(maxno!=-1)
	{
		i++;
		filtersinv[i] = new nlobjSearchFilter('id', null, 'greaterthan', maxno);
	}


	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_qoh');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');
	columns[5] = new nlobjSearchColumn('custrecord_ebiz_inv_sku_status');
	columns[6] = new nlobjSearchColumn('custrecord_ebiz_inv_packcode');
	columns[7] = new nlobjSearchColumn('custrecord_ebiz_inv_account_no');
	columns[8] = new nlobjSearchColumn('custrecord_ebiz_inv_loc');
	columns[9] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columns[10] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
	columns[11] = new nlobjSearchColumn('internalid');
	columns[12] = new nlobjSearchColumn('custrecord_ebiz_inv_company');
	columns[13] = new nlobjSearchColumn('custrecord_ebiz_inv_fifo');
	columns[14] = new nlobjSearchColumn('custrecord_ebiz_itemdesc');
	columns[15] = new nlobjSearchColumn('custrecord_ebiz_expdate');
	columns[16] = new nlobjSearchColumn('custrecord_invttasktype');
	columns[11].setSort();

	var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinv, columns);
	if(invtsearchresults!=null)
	{
		if(invtsearchresults.length>=1000)
		{
			var maxno1=invtsearchresults[invtsearchresults.length-1].getValue(columns[11]);
			tempInventoryResultsArray.push(invtsearchresults);
			getInventorySearchResults(maxno1,request);
		}
		else
		{
			tempInventoryResultsArray.push(invtsearchresults);
		}
	}
	return tempInventoryResultsArray;
}
function FillForm(request,response)
{
	var pagesizevalue;
	var whloc='';
	var form = nlapiCreateForm('Inventory Move');
	var save = form.addField('custpage_save', 'checkbox','isfromlocation').setDisplayType('hidden');
	var binLocationField = form.addField('custpage_binlocation', 'select', 'Bin Location','customrecord_ebiznet_location');
	//binLocationField.addSelectOption('', '');
	if(request.getParameter('custpage_binlocation')!='' && request.getParameter('custpage_binlocation')!=null)
	{
		binLocationField.setDefaultValue(request.getParameter('custpage_binlocation'));	
	}
	nlapiLogExecution('DEBUG', 'multiselect',request.getParameter('custpage_item'));
	var hiddenfieldselectpage=form.addField('custpage_hiddenfieldselectpage', 'text', 'selectPage').setDisplayType('hidden');
	hiddenfieldselectpage.setDefaultValue('F');
	var itemField = form.addField('custpage_item', 'multiselect', 'Item','item');

	if(request.getParameter('custpage_item')!='' && request.getParameter('custpage_item')!=null)
	{
		var itemValue=request.getParameter('custpage_item');
		var itemArray = new Array();
		itemArray = itemValue.split('');
		nlapiLogExecution('DEBUG', 'multiselect',itemArray.length);	
	}
	itemField.setDefaultValue(request.getParameter('custpage_item'));

	var invtlp = form.addField('custpage_invtlp', 'select', 'LP','customrecord_ebiznet_master_lp');
	//invtlp.addSelectOption('', '');
	if(request.getParameter('custpage_invtlpid')!='' && request.getParameter('custpage_invtlpid')!=null)
	{
		invtlp.setDefaultValue(request.getParameter('custpage_invtlpid'));	
	}

	nlapiLogExecution('DEBUG', 'Location',request.getParameter('custpage_location'));
	var varLoc = form.addField('custpage_location', 'select', 'Location','location');
	//varLoc.addSelectOption('', '');
	varLoc.setMandatory(true);
	if(request.getParameter('custpage_location')!='' && request.getParameter('custpage_location')!=null)
	{
		varLoc.setDefaultValue(request.getParameter('custpage_location'));	
		whloc = request.getParameter('custpage_location');
	}

	var varComp = form.addField('custpage_company', 'select', 'Company');
	varComp.addSelectOption('', '');

	if(request.getParameter('custpage_company')!='' && request.getParameter('custpage_company')!=null)
	{
		varComp.setDefaultValue(request.getParameter('custpage_company'));	
	}

	var vItemStatus = form.addField('custpage_itemstatus', 'select', 'Item Status');
	vItemStatus.addSelectOption('', '');
	var filtersStatus = new Array();
	filtersStatus.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	//Case # 20126884 Start
	if(whloc !=null && whloc!='')
	{
		filtersStatus.push(new nlobjSearchFilter('custrecord_ebizsiteskus', null, 'anyof', whloc));
	}
	//Case # 20126884 End
	var columnsstatus = new Array();
	columnsstatus.push(new nlobjSearchColumn('custrecord_statusdescriptionskustatus'));

	var searchStatus = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, filtersStatus, columnsstatus);

	if (searchStatus != null) 
	{
		for (var i = 0; i < searchStatus.length; i++) 
		{
			var res = form.getField('custpage_itemstatus').getSelectOptions(searchStatus[i].getValue('custrecord_statusdescriptionskustatus'), 'is');
			if (res != null) 
			{
				if (res.length > 0) 
					continue;                    
			}
			vItemStatus.addSelectOption(searchStatus[i].getId(), searchStatus[i].getValue('custrecord_statusdescriptionskustatus'));
		}
	}

	if(request.getParameter('custpage_itemstatus')!='' && request.getParameter('custpage_itemstatus')!=null)
	{
		vItemStatus.setDefaultValue(request.getParameter('custpage_itemstatus'));	
	}


	/*var searchlocresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, null, new nlobjSearchColumn('custrecord_ebiz_inv_loc',null,'group').setSort());

	if (searchlocresults != null)
	{
		nlapiLogExecution('DEBUG', 'Inventory count', searchlocresults.length);

		for (var i = 0; i < searchlocresults.length; i++) 
		{
			var res=searchlocresults[i].getValue('custrecord_ebiz_inv_loc',null,'group');
			if(res!=null&&res!="")
			varLoc.addSelectOption(searchlocresults[i].getValue('custrecord_ebiz_inv_loc',null,'group'), searchlocresults[i].getText('custrecord_ebiz_inv_loc',null,'group'));
		}
	}*/    

	var searchcompresults = nlapiSearchRecord('customrecord_ebiznet_company', null, new nlobjSearchFilter('isinactive',null,'is','F'), new nlobjSearchColumn('Name').setSort());

	if (searchcompresults != null)
	{
		for (var i = 0; i < searchcompresults.length; i++) 
		{
			var res = form.getField('custpage_company').getSelectOptions(searchcompresults[i].getValue('Name'), 'is');
			if (res != null) 
			{
				if (res.length > 0) 
					continue;                
			}
			if(searchcompresults[i].getValue('Name') != "")
				varComp.addSelectOption(searchcompresults[i].getValue('Name'), searchcompresults[i].getValue('Name'));
		}
	}

	//3 - In bound Storage
	//19 - Inventory Storage
	//nlapiLogExecution('DEBUG', 'Test1');
	//To get bin locations more than 1000 records:
	//var searchInventoryMain=FillBinLocation(-1);	

	//if (searchInventoryMain != null) 
	//{
	//for (var j = 0; j < searchInventoryMain.length; j++) {
	//	var searchInventory=searchInventoryMain[j];
	//for (var i = 0; i < searchInventory.length; i++) 
	//{
	//	var res = form.getField('custpage_binlocation').getSelectOptions(searchInventory[i].getText('custrecord_ebiz_inv_binloc'), 'is');
	//	if (res != null) 
	//	{
	//		if (res.length > 0) 
	//			continue;                    
	//	}
	//	binLocationField.addSelectOption(searchInventory[i].getValue('custrecord_ebiz_inv_binloc'), searchInventory[i].getText('custrecord_ebiz_inv_binloc'));
	//}

	//for (var i = 0; i < searchInventory.length; i++) 
	//{
	//	invtlp.addSelectOption(searchInventory[i].getValue('custrecord_ebiz_inv_lp'), searchInventory[i].getValue('custrecord_ebiz_inv_lp'));
	//}
	//nlapiLogExecution('DEBUG', 'Test2',request.getParameter('custpage_checkfield'));
	//}


	if (request.getParameter('custpage_checkfield') == null || request.getParameter('custpage_checkfield') == '') //&& request.getParameter('custpage_checkfield') == "") {
	{  
		form.setScript('customscript_cs');
		form.addField('custpage_checkfield', 'select', 'check').setDisplayType('hidden');

		var sublist = form.addSubList("custpage_invtmovelist", "list", "Inventory Move List");
		sublist.addField("custpage_sno", "text", "SL NO").setDisplayType('disabled');
		sublist.addField("custpage_invlocmove", "checkbox", "Move");
		sublist.addField("custpage_invlocation", "text", "Bin Location").setDisplayType('inline');
		sublist.addField("custpage_itemname", "text", "Item", "items").setDisplayType('inline');
		sublist.addField("custpage_itemstatus", "select", "Item Status", "customrecord_ebiznet_sku_status").setDisplayType('inline');
		sublist.addField("custpage_pc", "select", "Pack Code", "customlist_ebiznet_packcode").setDisplayType('inline');
		sublist.addField("custpage_lot", "select", "LOT/Batch #", "customrecord_ebiznet_batch_entry").setDisplayType('inline');
		sublist.addField("custpage_lp", "text", "LP#").setDisplayType('inline');
		sublist.addField("custpage_totqty", "text", "Total Quantity").setDisplayType('hidden');
		sublist.addField("custpage_itemqty", "text", "Quantity on Hand").setDisplayType('inline');
		sublist.addField("custpage_allocqty", "text", "Allocated Quantity").setDisplayType('inline');
		sublist.addField("custpage_availqty", "text", "Available Quantity").setDisplayType('inline');
		//sublist.addField("custpage_itemnewstatus", "select", "New Item Status", "customrecord_ebiznet_sku_status").setDisplayType('entry');

		nlapiLogExecution('DEBUG', 'warehouse location',whloc);
		//code added on 240912 by suman.
		//this will return the physical site id which is tied up against the main warehouse.
		var newWhloc=GetMainWareHouseSite(whloc);
		//end of code as of 240912.

		nlapiLogExecution('DEBUG', 'warehouse location',newWhloc);
		var InvItemStatus = sublist.addField("custpage_itemnewstatus", "select", "New Item Status");
		InvItemStatus.addSelectOption('', '');
		var InvItemStatusFilters = new Array();		
		if(newWhloc!=null && newWhloc!='')
			InvItemStatusFilters.push(new nlobjSearchFilter('custrecord_ebizsiteskus', null, 'anyOf', [newWhloc,whloc])); //Case # 201410310
		else
			InvItemStatusFilters.push(new nlobjSearchFilter('custrecord_ebizsiteskus', null, 'anyOf', [whloc]));

		InvItemStatusFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

		var columnsitemstatus = new Array();
		columnsitemstatus[0] = new nlobjSearchColumn('name');
		columnsitemstatus[1] = new nlobjSearchColumn('internalid');
		columnsitemstatus[0].setSort();

		var itemstatusresults = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, InvItemStatusFilters,columnsitemstatus);
		for (var k = 0; itemstatusresults != null && k < itemstatusresults.length; k++) {
			//Searching for Duplicates		
			var res =  InvItemStatus.getSelectOptions(itemstatusresults[k].getValue('name'), 'is');
			if (res != null) {

				if (res.length > 0) {
					continue;
				}
			}		
			InvItemStatus.addSelectOption(itemstatusresults[k].getValue('internalid'), itemstatusresults[k].getValue('name'));
		}


		//sublist.addField("custpage_itemnewloc", "select", "New Bin Location", "customrecord_ebiznet_location");
		var selectBinloc = sublist.addField('custpage_itemnewloc', 'select', 'New Bin Location');
		/*var filtersBinloc = new Array();
			filtersBinloc.push(new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'anyof', ['6','7']));

			var colsBinloc = new Array();
			colsBinloc[0]=new nlobjSearchColumn('name');
			//colsBinloc[0].setSort(true);

			var searchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersBinloc, colsBinloc);*/
		var vsearchInventoryMain =FillNewBinLocation(-1,whloc);
		selectBinloc.addSelectOption("", "");
		if (vsearchInventoryMain != null)
		{
			for (var j = 0; j < vsearchInventoryMain.length; j++) 
			{
				var searchresults=vsearchInventoryMain[j];
				for (var i = 0; i < searchresults.length; i++) 
				{
					selectBinloc.addSelectOption(searchresults[i].getId(), searchresults[i].getValue('name'));
				}
			}
		}

		sublist.addField("custpage_itemnewqty", "text", "New Quantity").setDisplayType('entry');
		sublist.addField("custpage_itemnewlp", "text", "New LP").setDisplayType('entry');
		sublist.addField("custpage_lottext", "text", "LOT").setDisplayType('hidden');
		sublist.addField("custpage_itemnamenew", "text", "Item Name").setDisplayType('hidden');

//		var InvAdjType= sublist.addField("custpage_invadjtype", "select", "Adjustment Type");
//		InvAdjType.addSelectOption('', '');
//		var AdjustmentTypeFilters = new Array();		
//		AdjustmentTypeFilters.push(new nlobjSearchFilter( 'custrecord_adjusttasktype', null, 'anyOf', '18'));
//		AdjustmentTypeFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

//		var columnsinvt = new Array();
//		columnsinvt[0] = new nlobjSearchColumn('name');
//		columnsinvt[1] = new nlobjSearchColumn('internalid');
//		columnsinvt[0].setSort();

//		//columnsinvt[2] = new nlobjSearchColumn('custrecord_route_no');

//		var searchresults = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, AdjustmentTypeFilters,columnsinvt);

//		for (var i = 0; searchresults != null && i < searchresults.length; i++) {
//		//Searching for Duplicates		
//		var res=  InvAdjType.getSelectOptions(searchresults[i].getValue('name'), 'is');
//		if (res != null) {

//		if (res.length > 0) {
//		continue;
//		}
//		}		
//		InvAdjType.addSelectOption(searchresults[i].getValue('internalid'), searchresults[i].getValue('name'));
//		}


		sublist.addField("custpage_recid", "text", "RecId").setDisplayType('hidden');
		sublist.addField("custpage_rectype", "text", "RecType").setDisplayType('hidden');

		sublist.addField("custpage_itemid", "text", "itemid").setDisplayType('hidden');
		sublist.addField("custpage_locid", "text", "locid").setDisplayType('hidden');
		sublist.addField("custpage_oldqty", "text", "oldquantity").setDisplayType('hidden');
		sublist.addField("custpage_oldlp", "text", "LP#").setDisplayType('hidden');
		sublist.addField("custpage_olditemstatus", "text", "ItemStatus").setDisplayType('hidden');
		sublist.addField("custpage_accno", "text", "accountno").setDisplayType('hidden');
		sublist.addField("custpage_siteloc", "text", "siteloc").setDisplayType('hidden');
		sublist.addField("custpage_allocqtyhid", "text", "qtyalloc").setDisplayType('hidden');
		sublist.addField("custpage_company", "text", "Company").setDisplayType('hidden');
		sublist.addField("custpage_fifodate", "text", "fifodate").setDisplayType('hidden');
		sublist.addField("custpage_expdate", "text", "expdate").setDisplayType('hidden');
		sublist.addField("custpage_itemdesc", "textarea", "ItemDesc").setDisplayType('hidden');
		sublist.addField("custpage_id", "text", "internal id#").setDisplayType('hidden');//santosh
		sublist.addField("custpage_tasktype", "text", "TaskType").setDisplayType('hidden');//santosh
		sublist.addField("custpage_putmethod", "text", "putmethod").setDisplayType('hidden');
		var invtsearchresults = getInventorySearchResults(-1,request);

		if (invtsearchresults != null) 
		{
			var temparraynew=new Array();var tempindex=0;
			for(k=0;k<invtsearchresults.length;k++)
			{
				var invtsearchresult = invtsearchresults[k];

				for(var j=0;j<invtsearchresult.length;j++)
				{
					temparraynew[tempindex]=invtsearchresult[j];
					tempindex=tempindex+1;
				}
			}
			nlapiLogExecution('DEBUG', 'temparraynew.length ', temparraynew.length); 
			// paging dropdown  
			var test='';
			if(temparraynew.length>0)
			{
				if(temparraynew.length>25)
				{

					var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('entry');
					pagesize.setDisplaySize(10,10);
					pagesize.setLayoutType('outsidebelow', 'startrow');
					var select= form.addField('custpage_selectpage','select', 'Select Records');	
					select.setLayoutType('outsidebelow', 'startrow');			
					select.setDisplaySize(200,30);

					if (request.getMethod() == 'GET'){

						pagesize.setDefaultValue("25");
						pagesizevalue=25;
					}
					else
					{
						if(request.getParameter('custpage_pagesize')!=null)
						{pagesizevalue= request.getParameter('custpage_pagesize');}
						else
						{pagesizevalue= 25;pagesize.setDefaultValue("25");}

					}
					form.setScript('customscript_inventoryclientvalidations');
					var len=temparraynew.length/parseFloat(pagesizevalue);
					for(var k=1;k<=Math.ceil(len);k++)
					{

						var from;var to;

						to=parseFloat(k)*parseFloat(pagesizevalue);
						from=(parseFloat(to)-parseFloat(pagesizevalue))+1;

						if(parseFloat(to)>temparraynew.length)
						{
							to=temparraynew.length;
							test=from.toString()+","+to.toString(); 
						}

						var temp=from.toString()+" to "+to.toString();
						var tempto=from.toString()+","+to.toString();
						select.addSelectOption(tempto,temp);

					} 
					if (request.getMethod() == 'POST'){

						if(request.getParameter('custpage_selectpage')!=null ){

							select.setDefaultValue(request.getParameter('custpage_selectpage'));	

						}
						if(request.getParameter('custpage_pagesize')!=null ){

							pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	

						}
					}
					else
					{

					}
				}
				else
				{
					pagesizevalue=25;
				}
				var minval=0;var maxval=parseFloat(pagesizevalue);
				if(parseFloat(pagesizevalue)>temparraynew.length)
				{
					maxval=temparraynew.length;
				}
				var selectno=request.getParameter('custpage_selectpage');
				if(selectno!=null )
				{
					var temp= request.getParameter('custpage_selectpage');
					var temparray=temp.split(',');
					nlapiLogExecution('DEBUG', 'temparray',temparray.length);

					var diff=parseFloat(temparray[1])-(parseFloat(temparray[0])-1);
					nlapiLogExecution('DEBUG', 'diff',diff);

					var pagevalue=request.getParameter('custpage_pagesize');
					nlapiLogExecution('DEBUG', 'pagevalue',pagevalue);
					if(pagevalue!=null)
					{
						if(parseFloat(diff)==parseFloat(pagevalue)|| test==selectno)
						{

							var temparray=selectno.split(',');	
							nlapiLogExecution('DEBUG', 'temparray.length ', temparray.length);  
							minval=parseFloat(temparray[0])-1;
							nlapiLogExecution('DEBUG', 'temparray[0] ', temparray[0]);  
							maxval=parseFloat(temparray[1]);
							nlapiLogExecution('DEBUG', 'temparray[1] ', temparray[1]);  
						}
					}
				}
				nlapiLogExecution('DEBUG', 'invtsearchresults.length ', temparraynew.length);
				var index=1;
				nlapiLogExecution('DEBUG', 'minval', minval);  

				nlapiLogExecution('DEBUG', 'maxval ', maxval);  
				if(parseInt(temparraynew.length)<parseInt(maxval))
				{
					minval=0;
					maxval=temparraynew.length;
				}

				nlapiLogExecution('DEBUG', 'maxval ', minval+"/"+maxval); 
				for (var s = minval; s < maxval; s++) 
				{
					var invtsearchresult = temparraynew[s];
					if(invtsearchresult!=null && invtsearchresult!='')
					{

						location = invtsearchresult.getValue('custrecord_ebiz_inv_binloc');
						locationname = invtsearchresult.getText('custrecord_ebiz_inv_binloc');
						item = invtsearchresult.getValue('custrecord_ebiz_inv_sku');
						itemname = invtsearchresult.getText('custrecord_ebiz_inv_sku');
						lp = invtsearchresult.getValue('custrecord_ebiz_inv_lp');
						qty = invtsearchresult.getValue('custrecord_ebiz_qoh');
						lot = invtsearchresult.getValue('custrecord_ebiz_inv_lot');
						var lotText = invtsearchresult.getText('custrecord_ebiz_inv_lot');
						//nlapiLogExecution('DEBUG', 'lot ', lot);
						//nlapiLogExecution('DEBUG', 'lot text', lotText);
						itemstatus = invtsearchresult.getValue('custrecord_ebiz_inv_sku_status');
						packcode = invtsearchresult.getValue('custrecord_ebiz_inv_packcode');
						accontno = invtsearchresult.getValue('custrecord_ebiz_inv_account_no');
						invsiteloc = invtsearchresult.getValue('custrecord_ebiz_inv_loc');
						invcompany = invtsearchresult.getValue('custrecord_ebiz_inv_company');
						invfifodate = invtsearchresult.getValue('custrecord_ebiz_inv_fifo');
						invitemdesc = invtsearchresult.getValue('custrecord_ebiz_itemdesc');
						invexpdate = invtsearchresult.getValue('custrecord_ebiz_expdate');
						var vTasktype = invtsearchresult.getValue('custrecord_ebiz_expdate');
						var totQty = 0;
						var allocQty = 0;						
						if(invtsearchresult.getValue('custrecord_ebiz_qoh') != null && invtsearchresult.getValue('custrecord_ebiz_qoh') != "")
						{
							totQty = parseFloat(invtsearchresult.getValue('custrecord_ebiz_qoh'));
							if(totQty<0)
								totQty=0;
						}
						if(invtsearchresult.getValue('custrecord_ebiz_alloc_qty') != null && invtsearchresult.getValue('custrecord_ebiz_alloc_qty') != "")
						{
							allocQty = parseFloat(invtsearchresult.getValue('custrecord_ebiz_alloc_qty'));
							if(allocQty<0)
								allocQty=0;
						}


						var availQty=0;
						availQty=parseFloat(totQty)-parseFloat(allocQty);

						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_sno',index, (parseFloat(s)+1).toString());
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_invlocation',index, locationname);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_itemname', index, itemname);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_lp', index, lp);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_oldlp', index, lp);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_itemqty', index, qty);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_oldqty', index, qty);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_olditemstatus', index, itemstatus);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_accno', index, accontno);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_siteloc', index, invsiteloc);
						//for record Id.
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_recid', index, invtsearchresult.getId());					 
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_tasktype', index,invtsearchresult.getValue('custrecord_invttasktype'));
						//for loc id and itemid.
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_rectype', index, invtsearchresult.getRecordType());
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_itemid', index, item);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_locid', index, location);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_lot', index, lot);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_itemstatus', index, itemstatus);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_pc', index, packcode);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_lottext', index, lotText);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_itemnamenew', index, itemname);

						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_totqty', index, totQty);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_allocqty', index, allocQty);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_allocqtyhid', index, allocQty);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_availqty', index, availQty);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_company', index, invcompany);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_fifodate', index, invfifodate);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_itemdesc', index, invitemdesc);
						form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_expdate', index, invexpdate);

						index=index+1;
					}

				}
			}
		}

	}
	var button = form.addSubmitButton('Save');

	// code added on 20 Jun2012 by santosh

	var tempflag = form.addField('custpage_tempflag', 'text','tempory flag').setDisplayType('hidden');

	var NewbinLocId = form.addField('custpage_newbinlocid_hidden', 'text','NewBinLocId').setDisplayType('hidden');

	var fullfillmentOrderfield = form.addField('custpage_fullinventroy','text', 'Fulfillment Order#');
	var date = form.addField('custpage_date', 'date', 'Date').setDisplaySize('30', '30');
	var Period=form.addField('custpage_accountingperiod', 'select', 'Account Period');

	var filterLocation = new Array();
	filterLocation.push(new nlobjSearchFilter('closed', null, 'is', 'F'));
	filterLocation.push(new nlobjSearchFilter('isquarter', null, 'is', 'F'));
	filterLocation.push(new nlobjSearchFilter('isyear', null, 'is', 'F'));
	var columnLocation = new Array();
	columnLocation[0] = new nlobjSearchColumn('periodname');
	columnLocation[1] = new nlobjSearchColumn('internalId'); 
	columnLocation[1].setSort();
	Period.addSelectOption("", "");
	var searchLocationRecord = nlapiSearchRecord('AccountingPeriod', null, filterLocation, columnLocation);
	for(var count=0;count < searchLocationRecord.length ;count++)
	{
		Period.addSelectOption(searchLocationRecord[count].getId(),searchLocationRecord[count].getValue('periodname'));

	} 
	if(request.getParameter('custpage_accountingperiod')!=null)
	{
		Period.setDefaultValue(request.getParameter('custpage_accountingperiod'));
	}
	//invtlp.addSelectOption('', '');
	if(request.getParameter('custpage_date')!='' && request.getParameter('custpage_date')!=null)
	{
		date.setDefaultValue(request.getParameter('custpage_date'));	
	}
	form.setScript('customscript_cs');
	form.addButton('custpage_generateloc', 'Generate New Bin Location','Display()');
	// end of code 20 Jun 2012

	//} else {
	//	var msg = form.addField('custpage_message', 'inlinehtml', null, null,null);
	//	msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'No Records', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
	//}
	response.writePage(form);
}

function inventoryMoveSuitelet(request, response){
	var ctx = nlapiGetContext();
	if (request.getMethod() == 'GET') {
		FillForm(request, response);

	} else {
		// code added on 20 Jun2012 by santosh

		var	getBaseUOM='';
		var	getuomlevel='';
		var	getBaseUomQty='';
		var getItemCube='';
		var itemstatus='';
		var ItemStatusId='';
		var tempflag = request.getParameter('custpage_tempflag');
		//var Savetempflag = request.getParameter('custpage_tempflagSave');
		nlapiLogExecution('DEBUG', 'GenerateLoc', tempflag);
		if (tempflag == 'GenrateNewLoc') {
			var form = nlapiCreateForm('Inventory Move');
			FillForm_newBinLoc(form,request, response,'');
			try {
				nlapiLogExecution('DEBUG', 'buttonclick', 'generatebutton');
				var lineCount = request.getLineItemCount('custpage_invtmovelist');
				nlapiLogExecution('DEBUG', 'count', lineCount);

				var whlocation = request.getParameter('custpage_location');
				nlapiLogExecution('DEBUG', 'whlocation', whlocation);

				var NewLocArray = new Array();
				var InvSelectArray = new Array();
				var lotbinlocarray = new Array();
				for ( var p = 1; p <= lineCount; p++) {
					//nlapiLogExecution('DEBUG', 'P value', p);
					var checkboxChk = request.getLineItemValue('custpage_invtmovelist', 'custpage_invlocmove', p);
					if (checkboxChk == "T") {

						var ItemLocId = request.getLineItemValue('custpage_invtmovelist', 'custpage_locid', p);
						nlapiLogExecution('DEBUG', 'ItemLocId', ItemLocId);
						var itemLocName = request.getLineItemValue('custpage_invtmovelist', 'custpage_invlocation', p);
						nlapiLogExecution('DEBUG', 'itemLocName', itemLocName);
						var ItemName = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemname', p);
						var ItemId = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemid', p);
						var ItemLP = request.getLineItemValue('custpage_invtmovelist', 'custpage_lp', p);
						var ItemQty = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemqty', p);
						var ItemNewLocId = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemnewloc', p);
						var ItemNewLocName = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemnewloc', p);
						var ItemNewQty = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemnewqty', p);
						var ItemOldQty = request.getLineItemValue('custpage_invtmovelist', 'custpage_oldqty', p);
						var ItemNewLP = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemnewlp', p);
						var RecdID = request.getLineItemValue('custpage_invtmovelist', 'custpage_recid', p);
						var LotBatch = request.getLineItemValue('custpage_invtmovelist', 'custpage_lot', p);
						var LotBatchText = request.getLineItemValue('custpage_invtmovelist', 'custpage_lottext', p);
						var olditemstatus = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemstatus', p);
						var packcode = request.getLineItemValue('custpage_invtmovelist', 'custpage_pc', p);
						var InternalId=request.getLineItemValue('custpage_invtmovelist', 'custpage_id', p);
						var tasktype = request.getLineItemValue('custpage_invtmovelist', 'custpage_tasktype', p);


						var ItemNewStatus = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemnewstatus',p);
						//Case # 20125990,20126186�Start 

						if(ItemNewStatus=='' || ItemNewStatus=='null' || ItemNewStatus==null)
						{
							itemstatus=olditemstatus;
							ItemStatusId =request.getLineItemValue('custpage_invtmovelist', 'custpage_itemstatus', p);
						}
						else
						{
							itemstatus=ItemNewStatus;
							ItemStatusId = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemnewstatus', p);

						}
						//Case # 20125990 end
						nlapiLogExecution('DEBUG', 'ItemNewLocId', ItemNewLocId);
						nlapiLogExecution('DEBUG', 'ItemNewStatus', ItemNewStatus);
						nlapiLogExecution('DEBUG', 'LotBatch', LotBatch);

						var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group'];
						var columns = nlapiLookupField('item', ItemId, fields);
						var ItemType = columns.recordType;					
						var batchflg = columns.custitem_ebizbatchlot;
						var itemfamId= columns.custitem_item_family;
						var itemgrpId= columns.custitem_item_group;

						nlapiLogExecution('DEBUG', 'ItemId', ItemId);
						var eBizItemDims=geteBizItemDimensions(ItemId);
						if(eBizItemDims!=null&&eBizItemDims.length>0)
						{
							for(z=0; z < eBizItemDims.length; z++)
							{
								if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
								{
									getBaseUOM = eBizItemDims[z].getText('custrecord_ebizuomskudim');
									getuomlevel = eBizItemDims[z].getText('custrecord_ebizuomlevelskudim');
									getBaseUomQty = eBizItemDims[z].getValue('custrecord_ebizqty');
									getItemCube = eBizItemDims[z].getValue('custrecord_ebizcube');
								}
							}
						}
						var TotalItemCube=CubeCapacity(ItemNewQty,getBaseUomQty,getItemCube,ItemQty);			
						nlapiLogExecution('DEBUG', 'TotalItemCube', TotalItemCube);

						if(TotalItemCube==null || TotalItemCube=='' || isNaN(TotalItemCube))
							TotalItemCube=0;


						//NewLocArray=GetPutawayLocation(ItemId, packcode, itemstatus, getBaseUOM, parseFloat(TotalItemCube), ItemType,null);					
						NewLocArray=vGetPutawayLocation(ItemId, packcode, itemstatus, getBaseUOM, parseFloat(TotalItemCube), 
								ItemType,whlocation,ItemLocId,lotbinlocarray,LotBatch);
						if(NewLocArray!=null && NewLocArray.length>0)
						{

							if(ItemLocId!=NewLocArray[2])
							{
								nlapiLogExecution('DEBUG', 'ResultArray', 'loc generated');	
								nlapiLogExecution('DEBUG', 'Linecount',p);
								var getBinLocationId = NewLocArray[2];
								var vLocationname= NewLocArray[0];
								//nlapiSetLineItemValue('custpage_invtmovelist','custpage_itemnewloc',p,getBinLocationId);

								nlapiLogExecution('DEBUG', 'NewBinLocationId', getBinLocationId);
								nlapiLogExecution('DEBUG', 'NewBinLocation name', vLocationname);
								nlapiLogExecution('DEBUG', 'LocationId', ItemLocId);

								nlapiLogExecution('DEBUG', 'InternalId_NEW',InternalId);

								form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_itemnewloc', p, getBinLocationId);
								form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_itemnewstatus', p, ItemStatusId);
								form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_itemnewqty', p, ItemNewQty);
								form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_invlocmove', p, 'T');
								form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_putmethod', p,NewLocArray[11] );
								var currRec = [getBinLocationId,LotBatch];

								lotbinlocarray.push(currRec);

							}
						}
						else
						{
							var getBeginLocationId = "";
							nlapiLogExecution('DEBUG', 'resultArray', 'no loc generated');
							//case# 20149825 starts(when bin location not fetching all entered fields are becomes empty)
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_itemnewloc', p, getBeginLocationId);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_itemnewstatus', p, ItemStatusId);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_itemnewqty', p, ItemNewQty);
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_invlocmove', p, 'T');
							form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_putmethod', p,'');
							//case# 20149825 ends
						}
					}
				}

			} catch (exp) {
				nlapiLogExecution('DEBUG', 'ExecptionInLoc', exp);
			}
		} 
		else 
		{
			var isfromlocationchange=request.getParameter('custpage_save');

			if(isfromlocationchange!='T')
			{

				var form = nlapiCreateForm("Inventory Move_save button");
				var linkURL = nlapiResolveURL('SUITELET', 'customscript_moveinventory_qb', 'customdeploy_moveinventory_qb_di');
				//form.addPageLink('crosslink', 'Back to Inventory Move','https://system.netsuite.com/app/site/hosting/scriptlet.nl?script=83&deploy=1');
				form.addPageLink('crosslink', 'Back to Inventory Move',linkURL);
				/*
				 * var ctx = nlapiGetContext(); if (ctx.getExecutionContext() ==
				 * 'userinterface' && type == 'view') { var InvURL =
				 * nlapiResolveURL('SUITELET', 'customscript_moveinventory',
				 * 'customdeploy_moveinventory_di'); nlapiLogExecution('DEBUG', 'Inv
				 * url', InvURL);
				 * 
				 * InvURL = getFQDNForHost(ctx.getEnvironment()) + InvURL;
				 * nlapiLogExecution('DEBUG', 'InvURL', InvURL); }
				 * form.addPageLink('crosslink', 'Back to Inventory Move', InvURL);
				 */
				var lpExists = 'N';
				var userselection = false;
				var message = false;
				var oldlocs = new Array();
				var newlocs = "";// new Array();
				var items = "";// new Array();
				var selectedcount = 0;
				var moveFlag;
				nlapiLogExecution('DEBUG', 'Test3');
				var lincount = request.getLineItemCount('custpage_invtmovelist');
				var reportno = GetMaxTransactionNo('MOVEREPORT');
				nlapiLogExecution('DEBUG', 'reportno', reportno);

				/* code merged from monobid on 27th feb 2013 by Radhika */
				var systemruleflag=stageruleForPalletLabel();
				/* upto here */
				for ( var p = 1; p <= lincount; p++) {
					moveFlag = request.getLineItemValue('custpage_invtmovelist','custpage_invlocmove', p);
					if (moveFlag == 'T') {
						userselection = true;
					}

					nlapiLogExecution('DEBUG', 'moveFlag', moveFlag);
					nlapiLogExecution('DEBUG', 'lincount', lincount);


					//alert(moveFlag);
					if (moveFlag == 'T') {
						var ItemLocId = request.getLineItemValue('custpage_invtmovelist', 'custpage_locid', p);
						nlapiLogExecution('DEBUG', 'ItemLocId', ItemLocId);
						var itemLocName = request.getLineItemValue('custpage_invtmovelist', 'custpage_invlocation', p);
						nlapiLogExecution('DEBUG', 'itemLocName', itemLocName);
						var ItemName = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemname', p);
						var ItemId = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemid', p);
						var ItemLP = request.getLineItemValue('custpage_invtmovelist', 'custpage_lp', p);
						var ItemQty = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemqty', p);
						var ItemNewLocId = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemnewloc', p);
						var ItemNewLocName = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemnewloc', p);
						var ItemNewQty = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemnewqty', p);
						var ItemOldQty = request.getLineItemValue('custpage_invtmovelist', 'custpage_oldqty', p);				
						var ItemNewLP = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemnewlp', p);
						var RecdID = request.getLineItemValue('custpage_invtmovelist', 'custpage_recid', p);
						//var RecdType = nlapiGetLineItemValue('custpage_invtmovelist', 'custpage_rectype', p);
						var LotBatch = request.getLineItemValue('custpage_invtmovelist', 'custpage_lot', p);
						var LotBatchText = request.getLineItemValue('custpage_invtmovelist', 'custpage_lottext', p);

						var itmstatus = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemstatus', p);
						var packcode = request.getLineItemValue('custpage_invtmovelist', 'custpage_pc', p);
						var accountno = request.getLineItemValue('custpage_invtmovelist', 'custpage_accno', p);
						var siteloc = request.getLineItemValue('custpage_invtmovelist', 'custpage_siteloc', p);
						var ItemNewStatus = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemnewstatus', p);
						//var InvAdjType = request.getLineItemValue('custpage_invtmovelist', 'custpage_invadjtype', p);
						var Invcompany = request.getLineItemValue('custpage_invtmovelist', 'custpage_company', p);
						var Invfifodate = request.getLineItemValue('custpage_invtmovelist', 'custpage_fifodate', p);
						var Invitemdesc = request.getLineItemValue('custpage_invtmovelist', 'custpage_itemdesc', p);
						var Invexpdate = request.getLineItemValue('custpage_invtmovelist', 'custpage_expdate', p);
						var tasktype = request.getLineItemValue('custpage_invtmovelist', 'custpage_tasktype', p);
						var putmethod = request.getLineItemValue('custpage_invtmovelist', 'custpage_putmethod', p);
						nlapiLogExecution('DEBUG', 'tasktype', tasktype);
						nlapiLogExecution('DEBUG', 'Item LP', ItemNewLP);

						var fields = ['recordType', 'custitem_ebizserialin'];
						var columns = nlapiLookupField('item', ItemId, fields);
						var ItemType = columns.recordType;
						nlapiLogExecution('DEBUG','ItemType',ItemType);

						var serialInflg="F";		
						serialInflg = columns.custitem_ebizserialin;

						var itemCube = 0;		

						//Pick face validation

						nlapiLogExecution('DEBUG', 'ItemNewLocId', ItemNewLocId );
						nlapiLogExecution('DEBUG', 'item id', ItemId);

						var PickFaceLocation=getPickFaceLocation(ItemNewLocId);
						if(PickFaceLocation != null && PickFaceLocation !='')
						{
							var moveallowed='N';
							for(var z = 0; z < PickFaceLocation.length; z++)
							{
								var pickfaceitem=PickFaceLocation[z].getValue('custrecord_pickfacesku');
								if(pickfaceitem == ItemId)
								{
									moveallowed='Y';
								}
							}

							if(moveallowed=='N')
							{
								var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
								msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'DEBUG', 'NEW BINLOCATION IS THE PICKFACE FOR ANOTHER ITEM.MOVE IS NOT ALLOWED.', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
								response.writePage(form);
								return;	
							}
						}
						//ending of pickface validation 

						var filters = new Array();
						filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', ItemId);
						filters[1] = new nlobjSearchFilter('custrecord_ebizuomskudim', null, 'is', 1);

						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_ebizcube'); 
						var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

						//nlapiLogExecution('DEBUG', 'Search Length of SKU Dims in inventory Move Screen',skuDimsSearchResults.length);

						if(skuDimsSearchResults != null && skuDimsSearchResults != "" ){
							nlapiLogExecution('DEBUG', 'skuDimsSearchResults', skuDimsSearchResults.length);
							for (var i = 0; i < skuDimsSearchResults.length; i++) {
								var skuDim = skuDimsSearchResults[i];
								itemCube  =  skuDim.getValue('custrecord_ebizcube');		    	
							}
						}


						//alert("OLD QTY::"+ItemQty);
						//alert("NEW QTY::"+ItemNewQty);			    
						//alert("itemCube::"+itemCube);
						if(ItemLocId!=null && ItemLocId!='')
						{
							var filterLocation = new Array();
							filterLocation[0] = new nlobjSearchFilter('id', null, 'equalto', ItemLocId);          
							var OldLocationColumns = new Array();
							OldLocationColumns[0]=new nlobjSearchColumn('custrecord_remainingcube');     
							var LocationResults =nlapiSearchRecord('customrecord_ebiznet_location', null, filterLocation,OldLocationColumns);
							var voldLocRemainingCube =  LocationResults[0].getValue('custrecord_remainingcube');
							nlapiLogExecution('DEBUG', 'LocationResults', LocationResults.length);
						}
						//alert("voldLocRemainingCube::"+voldLocRemainingCube);
						//alert("old Location" + itemLocName + "," + ItemLocId);


						if(ItemNewLocId!=null && ItemNewLocId!='')
						{
							var newfilterLocation = new Array();
							newfilterLocation[0] = new nlobjSearchFilter('id', null, 'equalto', ItemNewLocId);          
							var newLocationColumns = new Array();
							newLocationColumns[0]=new nlobjSearchColumn('custrecord_remainingcube');     
							var newLocationResults =nlapiSearchRecord('customrecord_ebiznet_location', null, newfilterLocation,newLocationColumns);
							var vnewLocRemainingCube =  newLocationResults[0].getValue('custrecord_remainingcube');
							nlapiLogExecution('DEBUG', 'newLocationResults', newLocationResults.length);
							nlapiLogExecution('DEBUG', 'vnewLocRemainingCube ',vnewLocRemainingCube);
							nlapiLogExecution('DEBUG', 'parseFloat(vnewLocRemainingCube) ',parseFloat(vnewLocRemainingCube));
							//alert("New Location" + ItemNewLocName + "," + ItemNewLocId);

							if(parseFloat(vnewLocRemainingCube)<=0)
							{
//								nlapiLogExecution('DEBUG', 'Qty Exceeds Location Capacity in To Location',ItemNewLocId);
//								alert("Qty Exceeds Location Capacity in To Location" + vnewLocRemainingCube);
//								return false;
								var localSerialNoArray = new Array();
								if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") 
								{
									localSerialNoArray = AdjustSerialNumbers(localSerialNoArray,ItemNewQty,ItemId,ItemLP,ItemNewLP,"N",'');
								}

								var form = nlapiCreateForm('Inventory Move');
								var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
								msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'DEBUG', 'Qty Exceeds Location Capacity in To Location', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
								response.writePage(form);
								return;	

							}

							var voldLocCube;
							var vnewLocCube;

							voldLocCube =  parseFloat(voldLocRemainingCube)  + (ItemNewQty * parseFloat(itemCube));
							vnewLocCube = parseFloat(vnewLocRemainingCube)  - (ItemNewQty * parseFloat(itemCube));

							nlapiLogExecution('DEBUG', 'vnewLocCube ',vnewLocCube);
							nlapiLogExecution('DEBUG', 'parsefloat(vnewLocCube) ',parseFloat(vnewLocCube));

							if(parseFloat(vnewLocCube)<0)
							{
//								alert("Qty Exceeds Location Capacity,Max move Qty : " + parseFloat(parseFloat(vnewLocRemainingCube)/parseFloat(itemCube)));
//								return false;
//								showInlineMessage(form, 'DEBUG', "Qty Exceeds Location Capacity,Max move Qty : " + parseFloat(parseFloat(vnewLocRemainingCube)/parseFloat(itemCube)));
//								response.writePage(form);
								var localSerialNoArray = new Array();
								if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") 
								{
									localSerialNoArray = AdjustSerialNumbers(localSerialNoArray,ItemNewQty,ItemId,ItemLP,ItemNewLP,"N",'');
								}

								var form = nlapiCreateForm('Inventory Move');
								var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
								msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'DEBUG', 'Qty Exceeds Location Capacity in To Location', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
								response.writePage(form);
								return;	
							}

							//alert("voldLocCube::"+voldLocCube);
							//alert("vnewLocCube::"+vnewLocCube);
							nlapiLogExecution('DEBUG', 'ItemLocId ',ItemLocId);
							nlapiLogExecution('DEBUG', 'ItemNewLocId ',ItemNewLocId);
							nlapiLogExecution('DEBUG', 'voldLocCube ',voldLocCube);
							nlapiLogExecution('DEBUG', 'vnewLocCube ',vnewLocCube);
//							9-Move task type, 10- INVT type, 2- PUTW
							//case 20140497....commented below if condition.
							//	if(tasktype != null && (tasktype == '9' || tasktype == '10' || tasktype == '2'))
							//	var oldretValue = UpdateLocCube(ItemLocId, voldLocCube);	

							//case # 20127235, Remaining cube updating two times here once and in after create inventory user event once.
							//so commented updateloccube.

							//	var newretValue = UpdateLocCube(ItemNewLocId, vnewLocCube);

							//nlapiLogExecution('DEBUG', 'oldretValue ',oldretValue);
							//nlapiLogExecution('DEBUG', 'newretValue ',newretValue);
						}
						//upto to here
						if(RecdID != null && RecdID != '')
						{	
							var filterstmp = new Array();
							filterstmp.push(new nlobjSearchFilter('internalid', null, 'is', RecdID));

							var searchresultstmp = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filterstmp, null);
							if(searchresultstmp == null || searchresultstmp == '' || searchresultstmp.length<=0)
							{
								var form = nlapiCreateForm('Inventory Move');
								var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
								msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'Move is already performed for this LP/Location', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
								response.writePage(form);
								return;	
							}	
						}

						//LP Checking in masterlp record starts
						//if (LPReturnValue == true) 
						//{
						try {
							nlapiLogExecution('DEBUG', 'INTO MASTER LP INSERTION');

							/* The below code(checking for null) is merged from Lexjet production account on 03-02-2013 by Radhika as part of Standard bundle*/
							if(ItemNewLP!=null && ItemNewLP!='')
							{
								var filtersmlp = new Array();
								filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', ItemNewLP);

								var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp,null);

								if (SrchRecord != null && SrchRecord.length > 0) 
								{
									nlapiLogExecution('DEBUG', 'LP FOUND');
									if(ItemQty==ItemNewQty)
									{
										lpExists = 'N';
									}
									else
									{
										lpExists = 'Y';
									}
								}
								else 
								{
									nlapiLogExecution('DEBUG', 'LP NOT FOUND');
									var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
									customrecord.setFieldValue('name', ItemNewLP);
									customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', ItemNewLP);
									var rec = nlapiSubmitRecord(customrecord, false, true);
								}
							}

							/*if(lpExists == 'Y')
							{
								alert('LP Already Exists!!!');
								return false;					
						}*/
						} 
						catch (e) 
						{
							nlapiLogExecution('DEBUG', 'Failed to Update/Insert into Master LP Record');
						}
						/* code merged from monobid on feb 27th 2013 by Radhika */
						fnMoveInventory(itmstatus,ItemNewStatus,ItemLocId,itemLocName,ItemName,ItemId,ItemLP,ItemQty,ItemNewLocId,ItemNewLocName,ItemNewQty,ItemNewLP,RecdID,LotBatch,LotBatchText,itmstatus,packcode,accountno,siteloc,ItemNewStatus,reportno,ItemOldQty,siteloc,Invcompany,Invfifodate,Invitemdesc,Invexpdate,putmethod);
						nlapiLogExecution('DEBUG', 'systemruleflag ',systemruleflag);
						if(systemruleflag== 'Y')
						{	
							nlapiLogExecution('DEBUG', 'createpalletreceivelabel ','createpalletreceivelabel');
							nlapiLogExecution('DEBUG', 'itmstatusold ',itmstatus);
							nlapiLogExecution('DEBUG', 'ItemNewStatus ',ItemNewStatus);

							if((itmstatus == 4)&& (ItemNewStatus == 1))
							{
								nlapiLogExecution('DEBUG', 'ItemLP ',ItemLP);
								try
								{
									CreatePalletReceiveLabel(siteloc,Invfifodate,Invitemdesc,ItemName,ItemLP,LotBatchText,ItemLocId,Invexpdate);
								}
								catch(exp)
								{
									nlapiLogExecution('DEBUG', 'palletreceivelabelexp',exp);
								}
							}

						}
					}
				}
				var invmoveDetails = new Array();
				invmoveDetails["custpage_reportno"] = reportno;
				/* upto here */

				if(userselection==true)
				{
					response.sendRedirect('SUITELET', 'customscript_inv_move_report', 'customdeploy_inv_move_report_di', false, invmoveDetails);
//					var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
//					msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Inventory Move Successful', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
//					response.writePage(form);
				}
				else
				{
					FillForm(request,response);
				}

				//response.writePage(form);
			}
			else
			{
				FillForm(request,response);	
			}

		}
	}
}
/* code merged from monobid on feb 27th 2013 by Radhika */
function CreatePalletReceiveLabel(wmslocation,Invfifodate,Invitemdesc,ItemName,ItemLP,LotBatchText,ItemLocId,Invexpdate)
{

	var userId;
	userId = nlapiGetUser();
	var transaction = nlapiLoadRecord('Employee', userId);
	var variable=transaction.getFieldValue('email');
	username=variable.split('@')[0]; 
	var shipfromaddress;
	var shipfromcity;
	var shipfromstate;
	var shipfromzipcode;
	var companyname;
	var shipfromphone;
	var shipfromcountry;
	if(wmslocation !="" && wmslocation !=null)
	{


		var record = nlapiLoadRecord('location', wmslocation);

		var addr1=record.getFieldValue('addr1');
		var addr2=record.getFieldValue('addr2');
		shipfromaddress=addr1+" " + addr2;
		shipfromcity=record.getFieldValue('city');
		shipfromstate=record.getFieldValue('state');
		shipfromzipcode =record.getFieldValue('zip');
		companyname=record.getFieldValue('addressee');
		shipfromphone=record.getFieldValue('addrphone');
		shipfromcountry =record.getFieldValue('country');



	}
	var labeltype="PALLETLABEL";
	var itemStatus="QCAPPROVE";
	var createreceivelabel = nlapiCreateRecord('customrecord_ext_labelprinting'); 
	createreceivelabel.setFieldValue('name',ItemLP);  
	createreceivelabel.setFieldValue('custrecord_label_item',ItemName);  
	createreceivelabel.setFieldValue('custrecord_label_itemdesc',Invitemdesc);  
	createreceivelabel.setFieldValue('custrecord_label_licenseplatenumber',ItemLP);  
	createreceivelabel.setFieldValue('custrecord_label_receivedate',Invfifodate);  
	createreceivelabel.setFieldValue('custrecord_label_lotnumber',LotBatchText);  
	createreceivelabel.setFieldValue('custrecord_label_itempulldate',Invexpdate);  
	createreceivelabel.setFieldValue('custrecord_label_user',username);  
	createreceivelabel.setFieldValue('custrecord_label_itemstatus',itemStatus); 
	createreceivelabel.setFieldValue('custrecord_label_order',ItemLP);

	createreceivelabel.setFieldValue('custrecord_label_shipfromaddressee',companyname); 
	createreceivelabel.setFieldValue('custrecord_label_addr1',shipfromaddress); 
	createreceivelabel.setFieldValue('custrecord_label_state',shipfromstate);
	createreceivelabel.setFieldValue('custrecord_label_zip',shipfromzipcode);
	createreceivelabel.setFieldValue('custrecord_label_city',shipfromcity);
	createreceivelabel.setFieldValue('custrecord_label_labeltype',labeltype);

	var tranid = nlapiSubmitRecord(createreceivelabel);


}

function stageruleForPalletLabel()
{
	var requiredfalg;
	var filtersystemrule=new Array();
	filtersystemrule.push(new nlobjSearchFilter('name', null,'is', 'QC1001'));
	var columnsystemrule=new Array();
	columnsystemrule[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
	var systemrulesrecordlist= nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filtersystemrule, columnsystemrule);
	if(systemrulesrecordlist!=null)
	{
		requiredfalg=systemrulesrecordlist[0].getValue('custrecord_ebizrulevalue');
	}
	else
	{
		requiredfalg="N";
	}
	return requiredfalg;
}

/* upto here */
function fnMoveInventory(itmstatus,ItemNewStatus,ItemLocId,itemLocName,ItemName,ItemId,ItemLP,ItemQty,ItemNewLocId,ItemNewLocName,ItemNewQty,ItemNewLP,RecdID,LotBatch,LotBatchText,itmstatus,packcode,accountno,siteloc,ItemNewStatus,reportno,ItemOldQty,siteloc,Invcompany,Invfifodate,Invitemdesc,Invexpdate,putmethod)
{
	try
	{
		var now = new Date();
		//now= now.getHours();
		//Getting time in hh:mm tt format.
		var a_p = "";
		var d = new Date();
		var curr_hour = now.getHours();
		if (curr_hour < 12) {
			a_p = "am";
		}
		else {
			a_p = "pm";
		}
		if (curr_hour == 0) {
			curr_hour = 12;
		}
		if (curr_hour > 12) {
			curr_hour = curr_hour - 12;
		}

		var curr_min = now.getMinutes();

		curr_min = curr_min + "";

		if (curr_min.length == 1) {
			curr_min = "0" + curr_min;
		}

		var CallFlag='N';
		var OldMakeWHFlag='Y';
		var NewMakeWHFlag='Y';
		var vNewWHlocatin;
		var vOldWHlocatin;
		var vNSFlag;
		var UpdateFlag;
		var vNSCallFlag='N';
		nlapiLogExecution('DEBUG', 'putmethod',putmethod);
		if(itmstatus==ItemNewStatus)
			CallFlag='N';
		else
		{

			vNSFlag=GetNSCallFlag(itmstatus,ItemNewStatus);
			nlapiLogExecution('DEBUG', 'vNSFlag',vNSFlag);

			if(vNSFlag!= null && vNSFlag!= "" && vNSFlag.length>0)
			{
				CallFlag=vNSFlag[0];
				nlapiLogExecution('DEBUG', 'vNSFlag[0]',vNSFlag[0]);
			}
			if(vNSFlag[0]=='N')
				CallFlag='N';
			else if(vNSFlag[0]=='Y') 
			{ 
				OldMakeWHFlag= GetMWFlag(vNSFlag[1]);
				NewMakeWHFlag= GetMWFlag(vNSFlag[2]);
				vNewWHlocatin=vNSFlag[2]; 
				vOldWHlocatin=vNSFlag[1]; 
			}


		}
		var vTaskType='9';
		if(CallFlag=='N')
			vTaskType='9';
		else
			vTaskType='18';

		if(ItemNewStatus==null||ItemNewStatus=='')
			ItemNewStatus=itmstatus;

		if( (ItemNewLP == "" || ItemNewLP == null))
		{
			if(parseFloat(ItemOldQty) == parseFloat(ItemNewQty))
			{
				ItemNewLP=ItemLP;
			}
			else
			{
				nlapiLogExecution('DEBUG', 'siteloc :', siteloc);
				ItemNewLP=GetMaxLPNo('1', '1',siteloc);
				createLPRecord(ItemNewLP,siteloc);
				nlapiLogExecution('DEBUG', 'auto generated lp :', ItemNewLP);
			}
		}

		if(NewMakeWHFlag=='Y')
		{ 
			if(vNewWHlocatin!=null && vNewWHlocatin!='')
			{
//				ItemNewLP = CreateINVTRecord(itemLocName, ItemLocId,ItemNewLocId, ItemNewLP, ItemId, ItemNewQty, ItemName, ItemNewStatus, packcode, LotBatch, vNewWHlocatin, accountno,RecdID,vNSCallFlag,vTaskType,Invcompany,Invfifodate,Invitemdesc,Invexpdate);
				ItemNewLP = CreateINVTRecord(itemLocName, ItemLocId,ItemNewLocId, ItemNewLP, ItemId, ItemNewQty, ItemName, ItemNewStatus, packcode, LotBatch, siteloc, accountno,RecdID,vNSCallFlag,vTaskType,Invcompany,Invfifodate,Invitemdesc,Invexpdate,putmethod);

			}
			else
			{
				ItemNewLP = CreateINVTRecord(itemLocName, ItemLocId,ItemNewLocId, ItemNewLP, ItemId, ItemNewQty, ItemName, ItemNewStatus, packcode, LotBatch, siteloc, accountno,RecdID,vNSCallFlag,vTaskType,Invcompany,Invfifodate,Invitemdesc,Invexpdate,putmethod);
			}
		}

		if(ItemNewLP=='ERROR')
		{	
			var form = nlapiCreateForm('Inventory Move');
			var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', 'Another thread is currently processing inventory move to the same location, which prevents your request from executed', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
			response.writePage(form);
			return;	
		}
		//Check condition if old LP not equal to New LP.
		//If LP is not equal need to create new record in trn_opentask
		//reduce the quantity in old LP.
		//if (ItemLP != ItemNewLP) {
		//Create a new record in trn_opentask with task type='MOVE' and new record in trn_inventory .
		var opentaskrec = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');


		nlapiLogExecution('DEBUG', 'itmstatus :', itmstatus);
		nlapiLogExecution('DEBUG', 'ItemNewStatus :', ItemNewStatus);
		nlapiLogExecution('DEBUG', 'packcode :', packcode);
		nlapiLogExecution('DEBUG', 'LotBatch :', LotBatch);
		nlapiLogExecution('DEBUG', 'LotBatchText :', LotBatchText);
		nlapiLogExecution('DEBUG', 'ItemNewQty :', ItemNewQty);
		nlapiLogExecution('DEBUG', 'ItemOldQty :', ItemOldQty);
		nlapiLogExecution('DEBUG', 'ItemNewLP :', ItemNewLP);
		//nlapiLogExecution('DEBUG', 'InvAdjType :', InvAdjType);
		nlapiLogExecution('DEBUG', 'ItemLP :', ItemLP);
		nlapiLogExecution('DEBUG', 'ItemLocId :', ItemLocId);
		nlapiLogExecution('DEBUG', 'itemLocName :', itemLocName);
		nlapiLogExecution('DEBUG', 'reportno :', reportno);
		nlapiLogExecution('DEBUG', 'siteloc :', siteloc);



		//Added for Item id.
		opentaskrec.setFieldValue('name', itemLocName);
		opentaskrec.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		//opentaskrec.setFieldValue('custrecord_act_qty', parseFloat(ItemQty).toFixed(5));//Assigning new quantity
		opentaskrec.setFieldValue('custrecord_act_qty', parseFloat(ItemNewQty).toFixed(5));//Assigning new quantity

		opentaskrec.setFieldValue('custrecord_lpno', ItemNewLP);
		opentaskrec.setFieldValue('custrecord_ebiz_lpno', ItemNewLP);


		opentaskrec.setFieldValue('custrecord_tasktype', vTaskType);

		opentaskrec.setFieldValue('custrecord_actbeginloc', ItemLocId);
		opentaskrec.setFieldValue('custrecord_actendloc', ItemNewLocId);

		//Added for Item name and desc
		opentaskrec.setFieldValue('custrecord_sku', ItemId);
		opentaskrec.setFieldValue('custrecord_skudesc', ItemName);

		opentaskrec.setFieldValue('custrecordact_begin_date', DateStamp());
		opentaskrec.setFieldValue('custrecord_act_end_date', DateStamp());

		//Adding fields to update time zones.
		opentaskrec.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
		opentaskrec.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
		opentaskrec.setFieldValue('custrecord_recordtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
		opentaskrec.setFieldValue('custrecord_recordupdatetime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
		opentaskrec.setFieldValue('custrecord_current_date', DateStamp());
		opentaskrec.setFieldValue('custrecord_upd_date', DateStamp());
		//Status flag .
		opentaskrec.setFieldValue('custrecord_wms_status_flag', 19);
		opentaskrec.setFieldValue('custrecord_batch_no', LotBatchText);
		opentaskrec.setFieldValue('custrecord_upd_ebiz_user_no', nlapiGetContext().getUser());

		//added by mahesh


		if(ItemNewQty == null || ItemNewQty == ""){
			ItemNewQty="0";}
		opentaskrec.setFieldValue('custrecord_sku_status', itmstatus);
		opentaskrec.setFieldValue('custrecord_ebiz_new_sku_status', ItemNewStatus);
		opentaskrec.setFieldValue('custrecord_packcode', packcode);
		if(LotBatchText != null && LotBatchText !=""){
			opentaskrec.setFieldValue('custrecord_lotnowithquantity', LotBatchText+'('+ItemNewQty+')');}
		//opentaskrec.setFieldValue('custrecord_expe_qty', parseFloat(ItemNewQty).toFixed(5));
		opentaskrec.setFieldValue('custrecord_expe_qty', parseFloat(ItemQty).toFixed(5));
		opentaskrec.setFieldValue('custrecord_ebiz_new_lp', ItemNewLP);
		//	opentaskrec.setFieldValue('custrecord_ebiz_ot_adjtype', InvAdjType);
		opentaskrec.setFieldValue('custrecord_from_lp_no', ItemLP);
		opentaskrec.setFieldValue('custrecord_wms_location', siteloc);
		opentaskrec.setFieldValue('custrecord_ebiz_report_no', reportno.toString());

		nlapiLogExecution('DEBUG', 'Submitting MOVE record', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(opentaskrec, false, true);
		nlapiLogExecution('DEBUG', 'Done MOVE Record Insertion :', 'Success');
		//alert("Done MOVE Record Insertion :");

		nlapiLogExecution('DEBUG', 'NewMakeWHFlag :', NewMakeWHFlag);
		nlapiLogExecution('DEBUG', 'itmstatus :', itmstatus);
		nlapiLogExecution('DEBUG', 'ItemNewStatus :', ItemNewStatus);

		nlapiLogExecution('DEBUG', 'ItemLocId :', ItemLocId);		
		nlapiLogExecution('DEBUG', 'ItemNewLocId :', ItemNewLocId);



		var fields = ['recordType', 'custitem_ebizserialin'];
		var columns = nlapiLookupField('item', ItemId, fields);
		var ItemType = columns.recordType;
		nlapiLogExecution('DEBUG','ItemType',ItemType);

		var serialInflg="F";		
		serialInflg = columns.custitem_ebizserialin;		

		var localSerialNoArray = new Array();
		if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") 
		{
			localSerialNoArray = AdjustSerialNumbers(localSerialNoArray,ItemNewQty,ItemId,ItemLP,ItemNewLP,"Y",ItemNewLocId,ItemOldQty,vNewWHlocatin);
		}

		nlapiLogExecution('DEBUG', 'CallFlag :', CallFlag);		

		if(CallFlag=='Y')//To trigger NS automatically
		{
			var serialnumbers = "";
			if(localSerialNoArray != null || localSerialNoArray != "")
			{
				//nlapiLogExecution('DEBUG','localSerialNoArray',localSerialNoArray.ToString());
				if(localSerialNoArray.length != 0)
				{
					nlapiLogExecution('DEBUG','localSerialNoArray.length',localSerialNoArray.length);
					serialnumbers = getSerialNoCSV(localSerialNoArray);
					nlapiLogExecution('DEBUG','serialnumbers',serialnumbers);
				}
			}
			nlapiLogExecution('DEBUG','lot before',LotBatchText);
			if(LotBatchText == '' || LotBatchText == null)
			{
				LotBatchText = serialnumbers;
			}
			if (ItemType != "lotnumberedinventoryitem" && ItemType != "lotnumberedassemblyitem" ) 
			{	//no need to send lot# to NS,if item is not Lotnumbered item in NS

				LotBatchText="";
				nlapiLogExecution('DEBUG','lot before1',LotBatchText);
			}
			nlapiLogExecution('DEBUG','lot after',LotBatchText);

			var date = request.getParameter('custpage_date');
			var period=request.getParameter('custpage_accountingperiod');
			nlapiLogExecution('DEBUG','date',date);
			InvokeNSInventoryTransfer(ItemId,ItemNewStatus,vOldWHlocatin,vNewWHlocatin,ItemNewQty,LotBatchText,date,period);
			var expdate;
			var filterspor = new Array();
			nlapiLogExecution('DEBUG', 'LotBatch', LotBatchText);
			filterspor.push(new nlobjSearchFilter('name', null, 'is', LotBatchText));
			/*if(whLocation!=null && whLocation!="")
				filterspor[1] = new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', whLocation);*/
			if(ItemId!=null && ItemId!="")
				filterspor.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', ItemId));

			var column=new Array();
			column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
			column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');

			var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
			if(receiptsearchresults!=null)
			{
				//getlotnoid= receiptsearchresults[0].getId();

				expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');
				//vfifoDate = receiptsearchresults[0].getValue('custrecord_ebizfifodate');
				nlapiLogExecution('DEBUG', 'expdate',expdate);
			}

			/*
			 * This part of code is commented as per inputs from NS team
			 * For all vitual location inventory move NS team suggested us to us Inventory Transfer object instead of inventory adjustment object
			 * Averate cost/price of the item will not change if we use Inventory Transfer object

			//Code for fetching account no from stockadjustment custom record	
			var AccountNo = getStockAdjustmentAccountNoNew(InvAdjType);

			if(AccountNo !=null && AccountNo != "" && AccountNo.length>0)
			{
				var FromAccNo=AccountNo[0];
				var ToAccNo=AccountNo[1];
				nlapiLogExecution('DEBUG', 'vOldWHlocatin',vOldWHlocatin);
				nlapiLogExecution('DEBUG', 'vNewWHlocatin',vNewWHlocatin);

				nlapiLogExecution('DEBUG', 'FromAccNo',FromAccNo);
				nlapiLogExecution('DEBUG', 'ToAccNo',ToAccNo);
				nlapiLogExecution('DEBUG', 'LotBatchText',LotBatchText);
				InvokeNSInventoryAdjustmentNew(ItemId,itmstatus,vOldWHlocatin,-(parseFloat(ItemNewQty)),"",vTaskType,InvAdjType,LotBatchText,FromAccNo);
				InvokeNSInventoryAdjustmentNew(ItemId,ItemNewStatus,vNewWHlocatin,ItemNewQty,"",vTaskType,InvAdjType,LotBatchText,ToAccNo);

			}
			 */ 
		}
		//Need to load that Record and update the qty.
		nlapiLogExecution('DEBUG', 'Updating RecID ', RecdID);	


		var scount=1;
		var InvQOH=0;
		LABL1: for(var i=0;i<scount;i++)
		{	

			nlapiLogExecution('DEBUG', 'CUSTOM_RECORD_COLLISION', i);
			try
			{

				var recLoad = nlapiLoadRecord('customrecord_ebiznet_createinv', RecdID);
				InvQOH = recLoad.getFieldValue('custrecord_ebiz_qoh');
				//recLoad.setFieldValue('custrecord_ebiz_inv_qty', (parseFloat(InvQOH) - parseFloat(ItemNewQty)));
				recLoad.setFieldValue('custrecord_ebiz_qoh', (parseFloat(InvQOH) - parseFloat(ItemNewQty)).toFixed(5));
				recLoad.setFieldValue('custrecord_updated_user_no', nlapiGetContext().getUser());
				recLoad.setFieldValue('custrecord_ebiz_callinv','N');
				recLoad.setFieldValue('custrecord_ebiz_displayfield','N');	
				if(expdate!=null && expdate!='')
					recLoad.setFieldValue('custrecord_ebiz_expdate', expdate);	
				nlapiSubmitRecord(recLoad, false, true);




//				var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
//				var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
//				var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');
//				var vNewAllocQty=0;
//				if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {
//				vNewAllocQty=parseInt(Invallocqty)- parseInt(expPickQty);
//				if(parseFloat(vNewAllocQty)<0)
//				vNewAllocQty=0;

//				Invttran.setFieldValue('custrecord_ebiz_alloc_qty',parseInt(vNewAllocQty));
//				}
//				else
//				Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);

//				nlapiLogExecution('DEBUG', 'vNewAllocQty',vNewAllocQty);

//				var remainqty=0;


//				if(remQty==null || remQty=='null'  || remQty=='' || isNaN(remQty))
//				{
//				newqoh = parseInt(InvQOH) - parseInt(expPickQty);
//				Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(newqoh)); 
//				}
//				else
//				{
//				newqoh = parseInt(vNewAllocQty) + parseInt(remQty);
//				Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(newqoh)); 
//				}

//				//Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(InvQOH) - (parseInt(expPickQty)-parseInt(remQty))); 
//				Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
//				Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

//				invtrecid = nlapiSubmitRecord(Invttran, false, true);
//				nlapiLogExecution('DEBUG', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);


			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 
				/*var exceptionname='RF Picking';
				var functionality='RFPick';
				var trantype=3;
				var vInvRecId=invrefno;
				var vcontainerLp=contlpno;
				nlapiLogExecution('DEBUG', 'DetailsError', functionality);	
				nlapiLogExecution('DEBUG', 'vcontainerLp', vcontainerLp);
				nlapiLogExecution('DEBUG', 'vInvRecId', invrefno);
				var reference3=ActPickQty;
				var reference4 ="";
				var reference5 ="";
				var userId = nlapiGetUser();
				InsertExceptionLog(exceptionname,trantype, functionality, wmsE, invrefno, vcontainerLp,reference3,reference4,reference5, userId);*/
				nlapiLogExecution('DEBUG', 'Exception in RF Inv move : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{  
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}






		if((parseFloat(InvQOH) - parseFloat(ItemNewQty)) == 0)
		{					
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', RecdID);	
			nlapiLogExecution('DEBUG', 'Deleted RecID ', id);	
		}
	}
	catch(exp) {
		nlapiLogExecution('DEBUG', 'Exception in fnMoveInventory ', exp);	

	}
//	return true;
}

function CreateINVTRecord(itemLocName, ItemLocId,ItemNewLocId, ItemNewLP, ItemId, ItemNewQty, ItemName, itemstatus, packcode, LotBatch, siteloc, accountno,RecdID,CallFlag,vTaskType,Invcompany,Invfifodate,Invitemdesc,Invexpdate,putmethod)
{
	nlapiLogExecution('DEBUG', 'Into CreateINVTRecord',itemLocName);

	nlapiLogExecution('DEBUG', 'itemstatus',itemstatus);
	nlapiLogExecution('DEBUG', 'itemstatus',ItemId);
	nlapiLogExecution('DEBUG', 'itemstatus',Invfifodate);
	nlapiLogExecution('DEBUG', 'ItemNewQty',ItemNewQty);
	nlapiLogExecution('DEBUG', 'Invexpdate',Invexpdate);
	nlapiLogExecution('DEBUG', 'putmethod',putmethod);
	//if item,itemstatus,lot and packcode already exists in inventory then 
	//update the qty else create a new record in inventory
	/*if(ItemLocId!=ItemNewLocId)
	{*/


	if(RecdID!=null && RecdID!='')
	{
		try
		{
			var recLoad = nlapiLoadRecord('customrecord_ebiznet_createinv', RecdID);
			/*if(compId==null || compId=='')
				compId=recLoad.getFieldValue('custrecord_ebiz_inv_company');*/
			var expdate=recLoad.getFieldValue('custrecord_ebiz_expdate');
			var invOldExistAvailQty = recLoad.getFieldValue('custrecord_ebiz_avl_qty');
			nlapiLogExecution('ERROR', 'invOldExistAvailQty', invOldExistAvailQty);
			if(invOldExistAvailQty == null || invOldExistAvailQty == '')
				invOldExistAvailQty=0;
			if(parseInt(invOldExistAvailQty) < parseInt(ItemNewQty))
			{
				return 'ERROR';

			}
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR', 'Exception in CreateInventory', exp);
		}
	}




	/*if(ItemNewLP!=null && ItemNewLP!='')
	{
		var filtersmlp = new Array();
		filtersmlp[0] = new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', ItemNewLP);

		var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersmlp,null);

		if (SrchRecord != null && SrchRecord.length > 0) 
		{
			nlapiLogExecution('DEBUG', 'siteloc :', siteloc);//
			ItemNewLP=GetMaxLPNo('1', '1',siteloc);
			createLPRecord(ItemNewLP,siteloc);
			nlapiLogExecution('DEBUG', 'auto generated lp :', ItemNewLP);
		}

	}*/


	var varMergeLP = 'T';
	nlapiLogExecution('DEBUG', 'putmethodID', putmethod);
	if(putmethod!=null && putmethod!='')
	{
		varMergeLP = isMergeLP(putmethod);
	}

	nlapiLogExecution('DEBUG', 'varMergeLP', varMergeLP);

	if(varMergeLP == 'T')
	{
		var invExistQty=0;
		var filters = new Array();
		var i = 0;
		if (ItemId != "") {		

			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', ItemId));
		}
		if(itemstatus!="")
		{

			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', itemstatus));
		}
		if(Invfifodate!="" && Invfifodate!=null)
		{

			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_fifo', null, 'on', Invfifodate));

		}
		/*
	if(LotBatch!=null)
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'is', LotBatch));


	}
		 */
		nlapiLogExecution('DEBUG', 'packcode',packcode);
		if(packcode !=null && packcode!="")
		{
			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_packcode', null, 'is', packcode));


		}
		nlapiLogExecution('DEBUG', 'ItemNewLocId',ItemNewLocId);
		if(ItemNewLocId!=null && ItemNewLocId!="")
		{

			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', ItemNewLocId));
		}
		nlapiLogExecution('DEBUG', 'LotBatch',LotBatch);
		if(LotBatch!=null && LotBatch!="")
		{

			filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'is', LotBatch));
		}

		//Added filter on 130912 by Suman
		//Need to fetch only those records whose status is inventory storage.
		filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['3','19']));//3=Putaway -complete;19=Inventory storage.
		filters.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0));
		//end of code as of 130912.

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh'); 
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_lp'); 
		var inventorysearch = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

		var vid=0;
		var invExistLp='';
		var maxuomqty = getmaxuomqty(ItemId);

		if(maxuomqty!=null && maxuomqty!='')
			maxuomqty = parseFloat(maxuomqty);
		else
			maxuomqty=0;

		nlapiLogExecution('DEBUG', 'maxuomqty',maxuomqty);
		if(inventorysearch != null)
		{
			nlapiLogExecution('DEBUG', 'Inventory found in same location for same item',ItemId);
			//for (var k = 0; k < inventorysearch.length; k++) {
			//var invqty = inventorysearch[k];
			//invExistQty  =  invqty.getValue('custrecord_ebiz_avl_qty');		    	
			//}
			//custrecord_ebiz_avl_qty
			for (var i=0;i<inventorysearch.length;i++)
			{
				invExistQty = inventorysearch[i].getValue('custrecord_ebiz_qoh');
				nlapiLogExecution('DEBUG', 'invExistQty',invExistQty);
				var variableqty=parseFloat(invExistQty)+parseFloat(ItemNewQty);
				nlapiLogExecution('DEBUG', 'variableqty',variableqty);
				//Move Qty plus Available Qty of the existing LP should not be greater than Max.UOM Qty
				if(parseFloat(variableqty)<=parseFloat(maxuomqty))
				{
					invExistLp = inventorysearch[i].getValue('custrecord_ebiz_inv_lp');	
					vid = inventorysearch[i].getId();
					break;
				}
			}
		}

		nlapiLogExecution('DEBUG', 'invExistQty',invExistQty);
		nlapiLogExecution('DEBUG', 'invExistLp',invExistLp);
		nlapiLogExecution('DEBUG', 'vid',vid);

		//alert("vid::"+vid);
		//alert("ItemNewLocId::"+ItemNewLocId);
		//alert("ItemNewQty::"+ItemNewQty);	    
		//alert("invExistQty::"+invExistQty);
//		Case # 20126039 Start
		if(parseFloat(invExistQty) > 0 && parseFloat(vid) >0){  
			//Case # 20126039 End
			nlapiLogExecution('ERROR', 'If invExistQty > 0',invExistQty);


			var scount=1;
			LABL1: for(var i=0;i<scount;i++)
			{	

				nlapiLogExecution('DEBUG', 'CUSTOM_RECORD_COLLISION', i);
				try
				{
					var invLoad = nlapiLoadRecord('customrecord_ebiznet_createinv', vid);
					var updatedInventory = parseFloat(invExistQty) + parseFloat(ItemNewQty);
					//alert('updatedInventory: '+updatedInventory);
					invLoad.setFieldValue('custrecord_ebiz_qoh', parseFloat(updatedInventory).toFixed(5));		
					//alert("Sum Value::" + (parseFloat(invExistQty) + parseFloat(ItemNewQty)));		
					invLoad.setFieldValue('custrecord_updated_user_no', nlapiGetContext().getUser());
					invLoad.setFieldValue('custrecord_ebiz_displayfield','N');	
					invLoad.setFieldValue('custrecord_ebiz_callinv', 'N');
					//case start '20123696
					if(LotBatch!=null && LotBatch!="")
					{
						var rec=nlapiLookupField('customrecord_ebiznet_batch_entry',LotBatch,['custrecord_ebizfifodate','custrecord_ebizexpirydate']);
						var fifodate=rec.custrecord_ebizfifodate;
						var expdate=rec.custrecord_ebizexpirydate;
						if(fifodate!=null && fifodate!='')
							invLoad.setFieldValue('custrecord_ebiz_inv_fifo',fifodate);
						/*else
						invtRec.setFieldValue('custrecord_ebiz_inv_fifo',DateStamp());*/
						nlapiLogExecution('DEBUG', 'expdate', expdate);
						if(expdate !=null && expdate!='')
							invLoad.setFieldValue('custrecord_ebiz_expdate', expdate);
						nlapiLogExecution('DEBUG', 'expdate', expdate);
					}//end

					var tresult = nlapiSubmitRecord(invLoad, false, true);



//					var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
//					var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
//					var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');
//					var vNewAllocQty=0;
//					if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {
//					vNewAllocQty=parseInt(Invallocqty)- parseInt(expPickQty);
//					if(parseFloat(vNewAllocQty)<0)
//					vNewAllocQty=0;

//					Invttran.setFieldValue('custrecord_ebiz_alloc_qty',parseInt(vNewAllocQty));
//					}
//					else
//					Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);

//					nlapiLogExecution('DEBUG', 'vNewAllocQty',vNewAllocQty);

//					var remainqty=0;


//					if(remQty==null || remQty=='null'  || remQty=='' || isNaN(remQty))
//					{
//					newqoh = parseInt(InvQOH) - parseInt(expPickQty);
//					Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(newqoh)); 
//					}
//					else
//					{
//					newqoh = parseInt(vNewAllocQty) + parseInt(remQty);
//					Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(newqoh)); 
//					}

//					//Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(InvQOH) - (parseInt(expPickQty)-parseInt(remQty))); 
//					Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
//					Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

//					invtrecid = nlapiSubmitRecord(Invttran, false, true);
//					nlapiLogExecution('DEBUG', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);


				}
				catch(ex)
				{
					var exCode='CUSTOM_RECORD_COLLISION'; 
					var wmsE='Inventory record being updated by another user. Please try again...';
					if (ex instanceof nlobjError) 
					{	
						wmsE=ex.getCode() + '\n' + ex.getDetails();
						exCode=ex.getCode();
					}
					else
					{
						wmsE=ex.toString();
						exCode=ex.toString();
					} 
					/*var exceptionname='RF Picking';
				var functionality='RFPick';
				var trantype=3;
				var vInvRecId=invrefno;
				var vcontainerLp=contlpno;
				nlapiLogExecution('DEBUG', 'DetailsError', functionality);	
				nlapiLogExecution('DEBUG', 'vcontainerLp', vcontainerLp);
				nlapiLogExecution('DEBUG', 'vInvRecId', invrefno);
				var reference3=ActPickQty;
				var reference4 ="";
				var reference5 ="";
				var userId = nlapiGetUser();
				InsertExceptionLog(exceptionname,trantype, functionality, wmsE, invrefno, vcontainerLp,reference3,reference4,reference5, userId);*/
					nlapiLogExecution('DEBUG', 'Exception in RF Inv move : ', wmsE); 
					if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
					{  
						scount=scount+1;
						continue LABL1;
					}
					else break LABL1;
				}
			}





			//var tresult = nlapiSubmitRecord(invLoad,true);

			//alert("tresult::"  + tresult);
			//alert("inventory record is updated!!!");
			nlapiLogExecution('DEBUG', 'Inventory Merged to the existing LP',invExistLp);
			ItemNewLP = invExistLp;
		}
		else
		{	
			nlapiLogExecution('DEBUG', 'If invExistQty Not Greater than 0',invExistQty);
			//Create Trn_inventory Record.
			var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');
			//var vIntValue="N";
			nlapiLogExecution('DEBUG', 'Creating Inventory Record', 'INVT');
			invtRec.setFieldValue('name', itemLocName);
			invtRec.setFieldValue('custrecord_ebiz_inv_binloc', ItemNewLocId);
			invtRec.setFieldValue('custrecord_ebiz_inv_lp', ItemNewLP);
			invtRec.setFieldValue('custrecord_ebiz_inv_sku', ItemId);
			invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(ItemNewQty).toFixed(5));
			invtRec.setFieldValue('custrecord_inv_ebizsku_no', ItemId);
			invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(ItemNewQty).toFixed(5));
			invtRec.setFieldValue('custrecord_ebiz_itemdesc', ItemName);
			//invtRec.setFieldValue('custrecord_invttasktype', 9);
			invtRec.setFieldValue('custrecord_ebiz_inv_loc', siteloc);
			invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountno);
			invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
			invtRec.setFieldValue('custrecord_ebiz_inv_packcode', packcode);
			//invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
			//invtRec.setFieldValue('custrecord_ebiz_callinv', vIntValue);
			invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
			invtRec.setFieldValue('custrecord_ebiz_displayfield','N');				
			invtRec.setFieldValue('custrecord_wms_inv_status_flag',19);		
			//invtRec.setFieldValue('custrecord_ebiz_inv_recorddate',(parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
			//invtRec.setFieldValue('custrecord_ebiz_inv_rectime',((curr_hour) + ":" + (curr_min) + " " + a_p));
			//invtRec.setFieldValue('custrecord_invt_ebizlp',putLP);
			//invtRec.setFieldValue('custrecord_ebiz_uomlvl','1');
			nlapiLogExecution('DEBUG', 'Before Submitting inventory LotBatch', LotBatch);

			//Added on 20Feb by suman
			//checking the condition whether lot is null or not,If it is not null then allow to pass the value to the field.
			if(LotBatch!=null&&LotBatch!="")
			{
				invtRec.setFieldValue('custrecord_ebiz_inv_lot', LotBatch);	
				var rec=nlapiLookupField('customrecord_ebiznet_batch_entry',LotBatch,['custrecord_ebizfifodate','custrecord_ebizexpirydate']);
				var fifodate=rec.custrecord_ebizfifodate;
				var expdate=rec.custrecord_ebizexpirydate;
				if(fifodate!=null && fifodate!='')
					invtRec.setFieldValue('custrecord_ebiz_inv_fifo',fifodate);
				/*else
				invtRec.setFieldValue('custrecord_ebiz_inv_fifo',DateStamp());*/
				nlapiLogExecution('DEBUG', 'expdate', expdate);
				if(expdate !=null && expdate!='')
					invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
			}
			else
			{
				var vfifodate=nlapiLookupField('customrecord_ebiznet_createinv', RecdID, 'custrecord_ebiz_inv_fifo');
				invtRec.setFieldValue('custrecord_ebiz_inv_fifo',vfifodate);
				var vexpdate=nlapiLookupField('customrecord_ebiznet_createinv', RecdID, 'custrecord_ebiz_expdate');
				invtRec.setFieldValue('custrecord_ebiz_expdate',vexpdate);
			}
			//

			//code added on 130812 by suman.
			//Code to update user id and recorded time.
			var currentContext = nlapiGetContext();
			var currentUserID = currentContext.getUser();
			invtRec.setFieldValue('custrecord_updated_user_no',currentUserID);
			invtRec.setFieldValue('custrecord_ebiz_inv_updatedtime',TimeStamp());
			//end of code as of 130812.

			//invtRec.setFieldValue('custrecord_ebiz_inv_lot', LotBatch);
			invtRec.setFieldValue('custrecord_invttasktype', vTaskType);
			//invtRec.setFieldValue('custrecord_ebiz_inv_adjusttype', vAdjType);
			invtRec.setFieldValue('custrecord_ebiz_inv_company', Invcompany);
			//invtRec.setFieldValue('custrecord_ebiz_inv_fifo', Invfifodate);
			invtRec.setFieldValue('custrecord_ebiz_itemdesc', Invitemdesc);
			//invtRec.setFieldValue('custrecord_ebiz_expdate', Invexpdate);
			nlapiLogExecution('DEBUG', 'ItemNewLP ',ItemNewLP);

			nlapiLogExecution('DEBUG', 'Before Submitting inventory recid', 'Inventory Records');
			var invtrecid = nlapiSubmitRecord(invtRec, false, true);
			//var invtrecid = nlapiSubmitRecord(invtRec,true);
		}
	}
	else
	{	
		nlapiLogExecution('DEBUG', 'If invExistQty Not Greater than 0',invExistQty);
		//Create Trn_inventory Record.
		var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');
		//var vIntValue="N";
		nlapiLogExecution('DEBUG', 'Creating Inventory Record', 'INVT');
		invtRec.setFieldValue('name', itemLocName);
		invtRec.setFieldValue('custrecord_ebiz_inv_binloc', ItemNewLocId);
		invtRec.setFieldValue('custrecord_ebiz_inv_lp', ItemNewLP);
		invtRec.setFieldValue('custrecord_ebiz_inv_sku', ItemId);
		invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(ItemNewQty).toFixed(5));
		invtRec.setFieldValue('custrecord_inv_ebizsku_no', ItemId);
		invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(ItemNewQty).toFixed(5));
		invtRec.setFieldValue('custrecord_ebiz_itemdesc', ItemName);
		//invtRec.setFieldValue('custrecord_invttasktype', 9);
		invtRec.setFieldValue('custrecord_ebiz_inv_loc', siteloc);
		invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountno);
		invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
		invtRec.setFieldValue('custrecord_ebiz_inv_packcode', packcode);
		//invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
		//invtRec.setFieldValue('custrecord_ebiz_callinv', vIntValue);
		invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
		invtRec.setFieldValue('custrecord_ebiz_displayfield','N');				
		invtRec.setFieldValue('custrecord_wms_inv_status_flag',19);		
		//invtRec.setFieldValue('custrecord_ebiz_inv_recorddate',(parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
		//invtRec.setFieldValue('custrecord_ebiz_inv_rectime',((curr_hour) + ":" + (curr_min) + " " + a_p));
		//invtRec.setFieldValue('custrecord_invt_ebizlp',putLP);
		//invtRec.setFieldValue('custrecord_ebiz_uomlvl','1');
		nlapiLogExecution('DEBUG', 'Before Submitting inventory LotBatch', LotBatch);

		//Added on 20Feb by suman
		//checking the condition whether lot is null or not,If it is not null then allow to pass the value to the field.
		if(LotBatch!=null&&LotBatch!="")
		{
			invtRec.setFieldValue('custrecord_ebiz_inv_lot', LotBatch);	
			var rec=nlapiLookupField('customrecord_ebiznet_batch_entry',LotBatch,['custrecord_ebizfifodate','custrecord_ebizexpirydate']);
			var fifodate=rec.custrecord_ebizfifodate;
			var expdate=rec.custrecord_ebizexpirydate;
			if(fifodate!=null && fifodate!='')
				invtRec.setFieldValue('custrecord_ebiz_inv_fifo',fifodate);
			/*else
			invtRec.setFieldValue('custrecord_ebiz_inv_fifo',DateStamp());*/
			nlapiLogExecution('DEBUG', 'expdate', expdate);
			if(expdate !=null && expdate!='')
				invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
		}
		else
		{
			var vfifodate=nlapiLookupField('customrecord_ebiznet_createinv', RecdID, 'custrecord_ebiz_inv_fifo');
			//invtRec.setFieldValue('custrecord_ebiz_inv_fifo',vfifodate);
			// case# 201413357

			if(vfifodate!=null && vfifodate!='')
				invtRec.setFieldValue('custrecord_ebiz_inv_fifo',vfifodate);
			else
				invtRec.setFieldValue('custrecord_ebiz_inv_fifo',DateStamp());
			var vexpdate=nlapiLookupField('customrecord_ebiznet_createinv', RecdID, 'custrecord_ebiz_expdate');
			invtRec.setFieldValue('custrecord_ebiz_expdate',vexpdate);
		}
		//

		//code added on 130812 by suman.
		//Code to update user id and recorded time.
		var currentContext = nlapiGetContext();
		var currentUserID = currentContext.getUser();
		invtRec.setFieldValue('custrecord_updated_user_no',currentUserID);
		invtRec.setFieldValue('custrecord_ebiz_inv_updatedtime',TimeStamp());
		//end of code as of 130812.

		//invtRec.setFieldValue('custrecord_ebiz_inv_lot', LotBatch);
		invtRec.setFieldValue('custrecord_invttasktype', vTaskType);
		//invtRec.setFieldValue('custrecord_ebiz_inv_adjusttype', vAdjType);
		invtRec.setFieldValue('custrecord_ebiz_inv_company', Invcompany);
		//invtRec.setFieldValue('custrecord_ebiz_inv_fifo', Invfifodate);
		invtRec.setFieldValue('custrecord_ebiz_itemdesc', Invitemdesc);
		//invtRec.setFieldValue('custrecord_ebiz_expdate', Invexpdate);
		nlapiLogExecution('DEBUG', 'ItemNewLP ',ItemNewLP);

		nlapiLogExecution('DEBUG', 'Before Submitting inventory recid', 'Inventory Records');
		var invtrecid = nlapiSubmitRecord(invtRec, false, true);
		//var invtrecid = nlapiSubmitRecord(invtRec,true);
	}
	return ItemNewLP;
	/*}
	else
	{
		//Create Trn_inventory Record.
		var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');
		//var vIntValue="N";
		nlapiLogExecution('DEBUG', 'Creating Inventory Record', 'INVT');
		invtRec.setFieldValue('name', itemLocName);
		invtRec.setFieldValue('custrecord_ebiz_inv_binloc', ItemNewLocId);
		invtRec.setFieldValue('custrecord_ebiz_inv_lp', ItemNewLP);
		invtRec.setFieldValue('custrecord_ebiz_inv_sku', ItemId);
		invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(ItemNewQty).toFixed(5));
		invtRec.setFieldValue('custrecord_inv_ebizsku_no', ItemId);
		invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(ItemNewQty).toFixed(5));

		invtRec.setFieldValue('custrecord_ebiz_itemdesc', ItemName);
		//invtRec.setFieldValue('custrecord_invttasktype', 9);
		invtRec.setFieldValue('custrecord_ebiz_inv_loc', siteloc);
		invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountno);
		invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
		invtRec.setFieldValue('custrecord_ebiz_inv_packcode', packcode);
		//invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
		//invtRec.setFieldValue('custrecord_ebiz_callinv', vIntValue);
		invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
		invtRec.setFieldValue('custrecord_ebiz_displayfield','N');				
		invtRec.setFieldValue('custrecord_wms_inv_status_flag',19);		
		//invtRec.setFieldValue('custrecord_ebiz_inv_recorddate',(parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
		//invtRec.setFieldValue('custrecord_ebiz_inv_rectime',((curr_hour) + ":" + (curr_min) + " " + a_p));
		//invtRec.setFieldValue('custrecord_invt_ebizlp',putLP);
		//invtRec.setFieldValue('custrecord_ebiz_uomlvl','1');
		nlapiLogExecution('DEBUG', 'Before Submitting inventory LotBatch', LotBatch);
		invtRec.setFieldValue('custrecord_ebiz_inv_lot', LotBatch);
		invtRec.setFieldValue('custrecord_invttasktype', vTaskType);
		//invtRec.setFieldValue('custrecord_ebiz_inv_adjusttype', vAdjType);
		invtRec.setFieldValue('custrecord_ebiz_inv_company', Invcompany);
		invtRec.setFieldValue('custrecord_ebiz_inv_fifo', Invfifodate);
		invtRec.setFieldValue('custrecord_ebiz_itemdesc', Invitemdesc);


		nlapiLogExecution('DEBUG', 'Before Submitting inventory recid', 'Inventory Records');
		var invtrecid = nlapiSubmitRecord(invtRec, false, true);
		//var invtrecid = nlapiSubmitRecord(invtRec,true);
	}*/
}
function GetStockAdjustTypes(AdjType)
{
	var filtersso = new Array();		
	filtersso[0] = new nlobjSearchFilter( 'custrecord_ebiz_adjtype', null, 'anyOf', AdjType );

	var columnsinvt = new Array();
	columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_tasktype');
	columnsinvt[0].setSort();

	//columnsinvt[2] = new nlobjSearchColumn('custrecord_route_no');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_invadj', null, filtersso,columnsinvt);
	return searchresults;
}

function GetNSCallFlag(Status,NewStatus)
{
	nlapiLogExecution('DEBUG', 'Into GetNSCallFlag', '');
	nlapiLogExecution('DEBUG', 'Status', Status);
	nlapiLogExecution('DEBUG', 'New Status', NewStatus);
	var result=new Array();
	var retFlag='N';
	var OldMapLocation;
	var NewMapLocation;
	if(Status!=null && Status!="")
	{
		var OldIStatus = nlapiLoadRecord('customrecord_ebiznet_sku_status', Status);
		if(OldIStatus!= null && OldIStatus != "")
		{
			OldMapLocation=OldIStatus.getFieldValue('custrecord_item_status_location_map');
			OldIStatus=null;
		} 
	}
	if(NewStatus!=null && NewStatus!="")
	{
		var NewIStatus = nlapiLoadRecord('customrecord_ebiznet_sku_status', NewStatus);
		if(NewIStatus!= null && NewIStatus != "")
		{
			NewMapLocation=NewIStatus.getFieldValue('custrecord_item_status_location_map');
		} 
	}
	if(OldMapLocation != null && OldMapLocation !="" && NewMapLocation != null && NewMapLocation != "")
	{
		if(OldMapLocation==NewMapLocation)
		{
			retFlag='N';
			result.push('N');
		}
		else
		{
			result.push('Y');
		}
		result.push(OldMapLocation);
		result.push(NewMapLocation);
	}
	nlapiLogExecution('DEBUG', 'retFlag', retFlag);
	nlapiLogExecution('DEBUG', 'OldMapLocation', OldMapLocation);
	nlapiLogExecution('DEBUG', 'NewMapLocation', NewMapLocation);
	nlapiLogExecution('DEBUG', 'Out of GetNSCallFlag', '');
	return result;
}


function GetMWFlag(WHLocation)
{
	nlapiLogExecution('DEBUG', 'Into GetMWFlag : WHLocation', WHLocation);
	var retFlag='Y';
	var MWHSiteFlag;

	if(WHLocation!=null && WHLocation!="")
	{
		var filters = new Array();
		filters.push(new nlobjSearchFilter('internalid', null, 'is', WHLocation));
		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_ebizwhsite')); 

		var WHOld = nlapiSearchRecord('location', null, filters, columns);

		if(WHOld!= null && WHOld != "" && WHOld.length>0)
		{
			MWHSiteFlag=WHOld[0].getValue('custrecord_ebizwhsite');
			nlapiLogExecution('DEBUG', 'MWHSiteFlag', MWHSiteFlag);
			WHOld=null;
		} 
	} 
	if(MWHSiteFlag != null && MWHSiteFlag !="" )
	{
		if(MWHSiteFlag=='F')
			retFlag='N';
		else
			retFlag='Y';

	}
	nlapiLogExecution('DEBUG', 'retFlag', retFlag);
	nlapiLogExecution('DEBUG', 'Out of GetMWFlag ', '');
	return retFlag;
}

function getStockAdjustmentAccountNoNew(adjusttype)
{	
	var StAdjustmentAccountNo=new Array();
	var filterStAccNo = new Array();
	var colsStAccNo = new Array();



	if(adjusttype!=null && adjusttype!="")
		filterStAccNo.push(new nlobjSearchFilter('internalid',null,'is',adjusttype));

	filterStAccNo.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	colsStAccNo[0] = new nlobjSearchColumn('custrecord_ebiz_mapto_ns_glaccount');
	colsStAccNo[1] = new nlobjSearchColumn('custrecord_ebiz_to_ns_glaccount');

	var StockAdjustAccResults = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, filterStAccNo, colsStAccNo);
	if(StockAdjustAccResults!=null && StockAdjustAccResults != "" && StockAdjustAccResults.length>0)
	{
		StAdjustmentAccountNo.push(StockAdjustAccResults[0].getValue('custrecord_ebiz_mapto_ns_glaccount'));
		StAdjustmentAccountNo.push(StockAdjustAccResults[0].getValue('custrecord_ebiz_to_ns_glaccount'));
	}

	return StAdjustmentAccountNo;	
}

/**
 * Passing item,item status, qty and location to NS
 * 
 * @param item
 * @param itemstatus
 * @param loc
 * @param qty
 * @param notes
 */
function  InvokeNSInventoryAdjustmentNew(item,itemstatus,loc,qty,notes,tasktype,adjusttype,lot,AccNo)
{
	try{
		nlapiLogExecution('DEBUG', 'Location info::', loc);
		nlapiLogExecution('DEBUG', 'Task Type::', tasktype);
		nlapiLogExecution('DEBUG', 'Adjustment Type::', adjusttype);
		nlapiLogExecution('DEBUG', 'qty::', qty);
		nlapiLogExecution('DEBUG', 'AccNo::', AccNo);
		nlapiLogExecution('DEBUG', 'lot::', lot);
		var confirmLotToNS='Y';
		confirmLotToNS=GetConfirmLotToNS(loc);

		var vItemname;

		var filters = new Array();          
		filters.push(new nlobjSearchFilter('internalid', null, 'is',item));
		// Changed On 30/4/12 by Suman
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		// End of Changes as On 30/4/12
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('cost');
		columns[1] = new nlobjSearchColumn('averagecost');
		columns[2] = new nlobjSearchColumn('itemid');

		var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
		if (itemdetails !=null) 
		{
			vItemname=itemdetails[0].getValue('itemid');
			vCost = itemdetails[0].getValue('cost');
			nlapiLogExecution('DEBUG', 'vCost', vCost);
			vAvgCost = itemdetails[0].getValue('averagecost');
			nlapiLogExecution('DEBUG', 'Average Cost', vAvgCost);	         
		}		

		var outAdj = nlapiCreateRecord('inventoryadjustment');			
		outAdj.setFieldValue('account', AccNo);
		outAdj.setFieldValue('memo', notes);
		outAdj.setFieldValue('adjlocation', loc);
		outAdj.insertLineItem('inventory', 1);
		outAdj.setLineItemValue('inventory', 'item', 1, item);
		outAdj.setLineItemValue('inventory', 'location', 1, loc);	
		outAdj.setLineItemValue('inventory', 'adjustqtyby', 1, parseFloat(qty));
		/*** The below code is merged from Endochoice account on 08thMar13 by Santosh as part of Standard bundle***/
		var subs = nlapiGetContext().getFeature('subsidiaries');
		if(subs==true)
		{
			var vSubsidiaryVal=getSubsidiaryNew(loc);
			if(vSubsidiaryVal != null && vSubsidiaryVal != '')
				outAdj.setFieldValue('subsidiary', vSubsidiaryVal);
		}
		/*** up to here ***/
		if(vAvgCost != null &&  vAvgCost != "")	
		{
			nlapiLogExecution('DEBUG', 'into if cond vAvgCost', vAvgCost);
			outAdj.setLineItemValue('inventory', 'unitcost', 1, vAvgCost);
		}
		else
		{
			nlapiLogExecution('DEBUG', 'into else cond.unit cost', vCost);
			outAdj.setLineItemValue('inventory', 'unitcost', 1, vCost);
		}
		outAdj.setFieldValue('subsidiary', 4);

		//Added by Sudheer on 093011 to send lot# for inventory posting

		if(lot!=null && lot!='')
		{
			//vItemname=vItemname.replace(" ","-");
			vItemname=vItemname.replace(/ /g,"-");
			if(parseFloat(qty)<0)
				qty=parseFloat(qty)*(-1);
			if(confirmLotToNS=='N')
				lot=vItemname;		

			nlapiLogExecution('DEBUG', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");
			outAdj.setLineItemValue('inventory', 'serialnumbers', 1, lot + "(" + parseFloat(qty) + ")");
		}

		nlapiSubmitRecord(outAdj, true, true);
		nlapiLogExecution('DEBUG', 'type argument', 'type is create');
	}


	catch(exp) {
		nlapiLogExecution('DEBUG', 'Exception in InvokeNSInventoryAdjustmentNew ', exp);	

	}
}


function  InvokeNSInventoryTransfer(item,itemstatus,loc,toloc,qty,lot,date,period)
{
	try{
		nlapiLogExecution('DEBUG', 'Into InvokeNSInventoryTransfer');		
		nlapiLogExecution('DEBUG', 'item', item);		
		nlapiLogExecution('DEBUG', 'itemstatus', itemstatus);
		nlapiLogExecution('DEBUG', 'loc', loc);
		nlapiLogExecution('DEBUG', 'toloc', toloc);
		nlapiLogExecution('DEBUG', 'qty', qty);
		nlapiLogExecution('DEBUG', 'lot', lot);

		var ItemType='';
		var serialOutflg="F";			

		if(item!= null && item != "")
		{
			var columns = nlapiLookupField('item', item, [ 'recordType','custitem_ebizserialout' ]);
			ItemType = columns.recordType;
			serialOutflg = columns.custitem_ebizserialout;
		}

		nlapiLogExecution('DEBUG', 'ItemType', ItemType);
		var confirmLotToNS='Y';
		confirmLotToNS=GetConfirmLotToNS(loc);
		var vItemname;	

		var filters = new Array();          
		filters.push(new nlobjSearchFilter('internalid', null, 'is',item));
		// Changed On 30/4/12 by Suman
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		// End of Changes as On 30/4/12
		var columns = new Array();		
		columns[0] = new nlobjSearchColumn('itemid');
		var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
		if (itemdetails !=null) 
		{
			vItemname=itemdetails[0].getValue('itemid');
		}
		/* The below code is merged from Lexjet production account on 03-02-2013 by Radhika as part of Standard bundle*/
		var invttransfer = nlapiCreateRecord('inventorytransfer');	

		var subs = nlapiGetContext().getFeature('subsidiaries');		
		if(subs==true)
		{
			var vSubsidiaryVal=getSubsidiaryNew(loc);
			if(vSubsidiaryVal != null && vSubsidiaryVal != '')
				invttransfer.setFieldValue('subsidiary', vSubsidiaryVal);

		}		

		/* Up to here */ 
		invttransfer.setFieldValue('location', loc);//from Location
		invttransfer.setFieldValue('transferlocation', toloc);
		if(date !=null && date !='' && date !='null' && date !='undefined')
		{
			invttransfer.setFieldValue('trandate', date);	
		}
		if(period !=null && period !='' && period !='null' && period !='undefined')
		{
			invttransfer.setFieldValue('postingperiod', period);	
		}
		invttransfer.selectNewLineItem('inventory');
		invttransfer.setCurrentLineItemValue('inventory', 'item', item);		
		invttransfer.setCurrentLineItemValue('inventory', 'adjustqtyby', parseFloat(qty));


		//if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem") 
		if(lot!=null && lot!='')
		{
			//vItemname=vItemname.replace(" ","-");
			vItemname=vItemname.replace(/ /g,"-");

			if(confirmLotToNS=='N')
				lot=vItemname;	





			//For advanced Bin serial Lot management check
			var vAdvBinManagement=false;
			var ctx = nlapiGetContext();
			if(ctx != null && ctx != '')
			{
				if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
					vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
			}  
			nlapiLogExecution('DEBUG', 'vAdvBinManagement', vAdvBinManagement);

			if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
			{
				// case no 20126516
				var templot = lot.split(',');

				nlapiLogExecution('ERROR', 'templot', templot.length);

				var compSubRecord = invttransfer.createCurrentLineItemSubrecord('inventory','inventorydetail');
				if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem") 
				{
					for(var t=0; t< templot.length; t++)
					{
						nlapiLogExecution('ERROR', 'test11', templot[t]);
						compSubRecord.selectNewLineItem('inventoryassignment');
						compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
						//compSubRecord.setCurrentLineItemValue('inventoryassignment', 'binnumber', vSubsidiaryVal);
						compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', templot[t]);

						compSubRecord.commitLineItem('inventoryassignment');
					}
				}
				else
				{
					nlapiLogExecution('ERROR', 'test1', 'test1');
					compSubRecord.selectNewLineItem('inventoryassignment');
					compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', qty);
					//compSubRecord.setCurrentLineItemValue('inventoryassignment', 'binnumber', vSubsidiaryVal);
					compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lot);

					compSubRecord.commitLineItem('inventoryassignment');
				}

				compSubRecord.commit();
			}
			else
			{
				if(parseInt(qty)<0)
					qty=parseInt(qty)*(-1);

				if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem") 
				{
					nlapiLogExecution('DEBUG', 'LotNowithQty', lot);
					invttransfer.setCurrentLineItemValue('inventory', 'serialnumbers',  lot);
				}	
				else
				{
					nlapiLogExecution('DEBUG', 'LotNowithQty', lot + "(" + parseInt(qty) + ")");
					invttransfer.setCurrentLineItemValue('inventory', 'serialnumbers', lot + "(" + parseInt(qty) + ")");
				}
			}



		}


		invttransfer.commitLineItem('inventory'); 
		nlapiSubmitRecord(invttransfer, true, true);
		nlapiLogExecution('DEBUG', 'Out of InvokeNSInventoryTransfer');
	}


	catch(exp) {
		nlapiLogExecution('DEBUG', 'Exception in InvokeNSInventoryTransferNew ', exp);	

	}
}
/**To get confirm LOT toNS based on system rule
 * 
 * @param Site
 * @returns Y or N
 */
function GetConfirmLotToNS(Site)
{

	try{
		var filters = new Array();

		filters[0] = new nlobjSearchFilter('custrecord_ebizruletype', null, 'is', 'WMSLOT');
		filters[1] = new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', ['@NONE@', Site]);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
		columns[1] = new nlobjSearchColumn('custrecord_ebizsite');

		columns[1].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters, columns);	
		if(searchresults != null && searchresults != '')
		{
			if(searchresults[0].getValue('custrecord_ebizrulevalue') != null && searchresults[0].getValue('custrecord_ebizrulevalue') != '')
			{
				return searchresults[0].getValue('custrecord_ebizrulevalue');
			}
			else
				return 'Y';
		}
		else
			return 'Y';
	}
	catch (exp) 
	{
		nlapiLogExecution('DEBUG', 'Exception in GetConfirmLotToNS and returning Y: ', exp);
		return 'Y';
	}	
}

function GeteLocCube(LocID){
	var remainingCube = 0;
	try {
		var fields = ['custrecord_remainingcube'];
		var columns= nlapiLookupField('customrecord_ebiznet_location',LocID,fields);
		remainingCube = columns.custrecord_remainingcube;
	}catch(exception) {
		nlapiLogExecution('DEBUG', 'GeteLocCube:Lookup for remainingCube failed', exception);
	}
	nlapiLogExecution('DEBUG', 'GeteLocCube:Remaining Cube for Location', remainingCube);
	return remainingCube;
}

function UpdateLocCube(locnId, remainingCube){
	var fields = new Array();
	var values = new Array();
	fields[0] = 'custrecord_remainingcube';
	values[0] = parseFloat(remainingCube).toFixed(5);

	nlapiLogExecution('DEBUG','Location Internal Id', locnId);

	var t1 = new Date();
	nlapiSubmitField('customrecord_ebiznet_location', locnId, fields, values);
	var t2 = new Date();
	nlapiLogExecution('DEBUG', 'nlapiSubmitField:UpdateLocCube:location', getElapsedTimeDuration(t1, t2));
}

function getElapsedTimeDuration(timestamp1, timestamp2){
	return (parseFloat(timestamp2.getTime()) - parseFloat(timestamp1.getTime()));
}

function createLPRecord(lp,whloc) {
	var timestamp1 = new Date();
	var t1 = new Date();
	var lpRecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
	var t2 = new Date();
	nlapiLogExecution('DEBUG', 'createLPRecord:nlapiCreateRecord|master_lp',
			getElapsedTimeDuration(t2, t1));

	lpRecord.setFieldValue('name', lp);
	lpRecord.setFieldValue('custrecord_ebiz_lpmaster_lp', lp);
	lpRecord.setFieldValue('custrecord_ebiz_lpmaster_lp', lp);
	lpRecord.setFieldValue('custrecord_ebiz_lpmaster_site', whloc);

	t1 = new Date();
	var recid = nlapiSubmitRecord(lpRecord);
	t2 = new Date();
	nlapiLogExecution('DEBUG', 'createLPRecord:nlapiSubmitRecord|master_lp',
			getElapsedTimeDuration(t2, t1));

	var timestamp2 = new Date();
	nlapiLogExecution('DEBUG', 'createLPRecord Duration',
			getElapsedTimeDuration(timestamp1, timestamp2));
}


function CubeCapacity(RcvQty,BaseUOMQty,cube,oldqty)
{
	nlapiLogExecution('DEBUG', 'CubeCapacity : RcvQty', RcvQty);
	nlapiLogExecution('DEBUG', 'CubeCapacity : BaseUOMQty', BaseUOMQty);
	nlapiLogExecution('DEBUG', 'CubeCapacity : cube', cube);
	nlapiLogExecution('DEBUG', 'CubeCapacity : oldqty', oldqty);

	if(RcvQty==null || RcvQty=='' || isNaN(RcvQty))
		RcvQty=oldqty;

	try{
		var itemCube;
		if (cube != "" && cube != null) 
		{
			if (!isNaN(cube)) 
			{
				var uomqty = ((parseFloat(RcvQty))/(parseFloat(BaseUOMQty)));			
				nlapiLogExecution('DEBUG', 'uomqty', uomqty);
				itemCube = (parseFloat(uomqty) * parseFloat(cube));
				nlapiLogExecution('DEBUG', 'ItemCube', itemCube);
			}
			else 
			{
				itemCube = 0;
			}
		}
		else 
		{
			itemCube = 0;
		}	
		nlapiLogExecution('DEBUG','itemcube',itemCube);
		return itemCube;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in cubecapacity',exp);
	}
}



function FillForm_newBinLoc(form,request, response,newBinLoc) {

	nlapiLogExecution('DEBUG', 'NewCreateForm','NewForm');
	var pagesizevalue;
	var whloc = '';
//	var form = nlapiCreateForm('Inventory Move');
	var binLocationField = form.addField('custpage_binlocation', 'select','Bin Location','customrecord_ebiznet_location');
	//binLocationField.addSelectOption('', '');
	if (request.getParameter('custpage_binlocation') != ''&& request.getParameter('custpage_binlocation') != null) {
		binLocationField.setDefaultValue(request.getParameter('custpage_binlocation'));
	}
	nlapiLogExecution('DEBUG', 'multiselect', request.getParameter('custpage_item'));

	var itemField = form.addField('custpage_item', 'multiselect', 'Item','item');
	if (request.getParameter('custpage_item') != ''&& request.getParameter('custpage_item') != null) {
		var itemValue = request.getParameter('custpage_item');
		var itemArray = new Array();
		itemArray = itemValue.split('');
		nlapiLogExecution('DEBUG', 'multiselect', itemArray.length);
	}
	itemField.setDefaultValue(request.getParameter('custpage_item'));

	var invtlp = form.addField('custpage_invtlp', 'select', 'LP','customrecord_ebiznet_master_lp');
	//invtlp.addSelectOption('', '');
	if (request.getParameter('custpage_invtlp') != ''&& request.getParameter('custpage_invtlp') != null) {
		invtlp.setDefaultValue(request.getParameter('custpage_invtlp'));
	}

	nlapiLogExecution('DEBUG', 'Location', request.getParameter('custpage_location'));
	var varLoc = form.addField('custpage_location', 'select', 'Location','location');
	//varLoc.addSelectOption('', '');
	varLoc.setMandatory(true);
	if (request.getParameter('custpage_location') != ''&& request.getParameter('custpage_location') != null) {
		varLoc.setDefaultValue(request.getParameter('custpage_location'));
		whloc = request.getParameter('custpage_location');
	}

	var varComp = form.addField('custpage_company', 'select', 'Company');
	varComp.addSelectOption('', '');

	if (request.getParameter('custpage_company') != ''
		&& request.getParameter('custpage_company') != null) {
		varComp.setDefaultValue(request.getParameter('custpage_company'));
	}

	var vItemStatus = form.addField('custpage_itemstatus', 'select','Item Status');
	vItemStatus.addSelectOption('', '');
	var filtersStatus = new Array();
	filtersStatus.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	//Case # 20126884 Start
	if(whloc !=null && whloc!='')
	{
		filtersStatus.push(new nlobjSearchFilter('custrecord_ebizsiteskus', null, 'anyof', whloc));
	}
	//Case # 20126884 End
	var columnsstatus = new Array();
	columnsstatus.push(new nlobjSearchColumn('custrecord_statusdescriptionskustatus'));

	var searchStatus = nlapiSearchRecord('customrecord_ebiznet_sku_status',
			null, filtersStatus, columnsstatus);

	if (searchStatus != null) {
		for ( var i = 0; i < searchStatus.length; i++) {
			var res = form.getField('custpage_itemstatus').getSelectOptions(searchStatus[i].getValue('custrecord_statusdescriptionskustatus'),'is');
			if (res != null) {
				if (res.length > 0)
					continue;
			}
			vItemStatus.addSelectOption(searchStatus[i].getId(),searchStatus[i].getValue('custrecord_statusdescriptionskustatus'));
		}
	}

	if (request.getParameter('custpage_itemstatus') != ''&& request.getParameter('custpage_itemstatus') != null) {
		vItemStatus.setDefaultValue(request.getParameter('custpage_itemstatus'));
	}

	/*var searchlocresults = nlapiSearchRecord('customrecord_ebiznet_createinv',null, null, new nlobjSearchColumn('custrecord_ebiz_inv_loc').setSort());

	if (searchlocresults != null) {
		nlapiLogExecution('DEBUG', 'Inventory count', searchlocresults.length);

		for ( var i = 0; i < searchlocresults.length; i++) {
			var res = form.getField('custpage_location').getSelectOptions(searchlocresults[i].getText('custrecord_ebiz_inv_loc'),'is');
			if (res != null) {
				if (res.length > 0)
					continue;
			}
			varLoc.addSelectOption(searchlocresults[i].getValue('custrecord_ebiz_inv_loc'), searchlocresults[i].getText('custrecord_ebiz_inv_loc'));
		}
	}*/

	var searchcompresults = nlapiSearchRecord('customrecord_ebiznet_company',null, new nlobjSearchFilter('isinactive', null, 'is', 'F'),new nlobjSearchColumn('Name').setSort());

	if (searchcompresults != null) {
		for ( var i = 0; i < searchcompresults.length; i++) {
			var res = form.getField('custpage_company').getSelectOptions(
					searchcompresults[i].getValue('Name'), 'is');
			if (res != null) {
				if (res.length > 0)
					continue;
			}
			if (searchcompresults[i].getValue('Name') != "")
				varComp.addSelectOption(searchcompresults[i].getValue('Name'),
						searchcompresults[i].getValue('Name'));
		}
	}

	// 3 - In bound Storage
	// 19 - Inventory Storage
	//nlapiLogExecution('DEBUG', 'Test1');
	//var filtersinv = new Array();
	//filtersinv[0] = new nlobjSearchFilter('custrecord_wms_inv_status_flag',null, 'anyof', [ 3, 19 ]);

	//var columns = new Array();
	//columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	//columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	//columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	//columns[3]= new nlobjSearchColumn('internalid');//santosh

	//var searchInventory = nlapiSearchRecord('customrecord_ebiznet_createinv',null, filtersinv, columns);

	//if (searchInventory != null) {
	//for ( var i = 0; i < searchInventory.length; i++) {
	//	var res = form.getField('custpage_binlocation').getSelectOptions(searchInventory[i].getText('custrecord_ebiz_inv_binloc'),
	//	'is');
	//	if (res != null) {
	//		if (res.length > 0)
	//			continue;
	//	}
	//	binLocationField.addSelectOption(searchInventory[i].getValue('custrecord_ebiz_inv_binloc'), searchInventory[i].getText('custrecord_ebiz_inv_binloc'));
	//}

	//for ( var i = 0; i < searchInventory.length; i++) {
	//	invtlp.addSelectOption(searchInventory[i].getValue('custrecord_ebiz_inv_lp'), searchInventory[i].getValue('custrecord_ebiz_inv_lp'));
	//}
	//nlapiLogExecution('DEBUG', 'Test2', request.getParameter('custpage_checkfield'));

	if (request.getParameter('custpage_checkfield') == null
			|| request.getParameter('custpage_checkfield') == '') // &&
		// request.getParameter('custpage_checkfield')
		// ==
		// "") {
	{
		form.setScript('customscript_cs');
		form.addField('custpage_checkfield', 'select', 'check').setDisplayType('hidden');

		var sublist = form.addSubList("custpage_invtmovelist", "list","Inventory Move List");
		sublist.addField("custpage_sno", "text", "SL NO").setDisplayType('disabled');
		sublist.addField("custpage_invlocmove", "checkbox", "Move");
		sublist.addField("custpage_invlocation", "text", "Bin Location").setDisplayType('inline');
		sublist.addField("custpage_itemname", "text", "Item", "items").setDisplayType('inline');
		sublist.addField("custpage_itemstatus", "select", "Item Status","customrecord_ebiznet_sku_status").setDisplayType('inline');
		sublist.addField("custpage_pc", "select", "Pack Code","customlist_ebiznet_packcode").setDisplayType('inline');
		sublist.addField("custpage_lot", "select", "LOT/Batch #","customrecord_ebiznet_batch_entry").setDisplayType('inline');
		sublist.addField("custpage_lp", "text", "LP#").setDisplayType('inline');
		sublist.addField("custpage_totqty", "text", "Total Quantity")
		.setDisplayType('hidden');
		sublist.addField("custpage_itemqty", "text", "Quantity on Hand")
		.setDisplayType('inline');
		sublist.addField("custpage_allocqty", "text", "Allocated Quantity")
		.setDisplayType('inline');
		sublist.addField("custpage_availqty", "text", "Available Quantity")
		.setDisplayType('inline');
		// sublist.addField("custpage_itemnewstatus", "select", "New Item
		// Status",
		// "customrecord_ebiznet_sku_status").setDisplayType('entry');

		nlapiLogExecution('DEBUG', 'warehouse location', whloc);

		var InvItemStatus = sublist.addField("custpage_itemnewstatus","select", "New Item Status");
		InvItemStatus.addSelectOption('', '');
		var InvItemStatusFilters = new Array();
		if (whloc != null && whloc != '')
			InvItemStatusFilters.push(new nlobjSearchFilter('custrecord_ebizsiteskus', null, 'anyOf', [ whloc ]));

		InvItemStatusFilters.push(new nlobjSearchFilter('isinactive', null,	'is', 'F'));

		var columnsitemstatus = new Array();
		columnsitemstatus[0] = new nlobjSearchColumn('name');
		columnsitemstatus[1] = new nlobjSearchColumn('internalid');
		columnsitemstatus[0].setSort();

		var itemstatusresults = nlapiSearchRecord('customrecord_ebiznet_sku_status', null,InvItemStatusFilters, columnsitemstatus);
		for ( var k = 0; itemstatusresults != null && k < itemstatusresults.length; k++) {
			// Searching for Duplicates
			var res = InvItemStatus.getSelectOptions(itemstatusresults[k].getValue('name'), 'is');
			if (res != null) {

				if (res.length > 0) {
					continue;
				}
			}
			InvItemStatus.addSelectOption(itemstatusresults[k].getValue('internalid'), itemstatusresults[k].getValue('name'));
		}

		// sublist.addField("custpage_itemnewloc", "select", "New Bin
		// Location", "customrecord_ebiznet_location");
		var selectBinloc = sublist.addField('custpage_itemnewloc','select', 'New Bin Location');
		/*var filtersBinloc = new Array();
			filtersBinloc.push(new nlobjSearchFilter('custrecord_ebizlocationtype',	null, 'anyof', [ '6', '7' ]));

			var colsBinloc = new Array();
			colsBinloc[0] = new nlobjSearchColumn('name');
			// colsBinloc[0].setSort(true);

			var searchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersBinloc,colsBinloc);
			selectBinloc.addSelectOption("", "");
			if (searchresults != null) {
				for ( var i = 0; i < searchresults.length; i++) {
					selectBinloc.addSelectOption(searchresults[i].getId(),searchresults[i].getValue('name'));
				}
			}
		 */
		var vsearchInventoryMain = FillNewBinLocation(-1,whloc);


		selectBinloc.addSelectOption("", "");
		if (vsearchInventoryMain != null)
		{
			for (var j = 0; j < vsearchInventoryMain.length; j++) 
			{
				var searchresults=vsearchInventoryMain[j];
				for (var i = 0; i < searchresults.length; i++) 
				{
					selectBinloc.addSelectOption(searchresults[i].getId(), searchresults[i].getValue('name'));
				}
			}
		}





		sublist.addField("custpage_itemnewqty", "text", "New Quantity").setDisplayType('entry');
		sublist.addField("custpage_itemnewlp", "text", "New LP").setDisplayType('entry');
		sublist.addField("custpage_lottext", "text", "LOT").setDisplayType('hidden');

		// var InvAdjType= sublist.addField("custpage_invadjtype", "select",
		// "Adjustment Type");
		// InvAdjType.addSelectOption('', '');
		// var AdjustmentTypeFilters = new Array();
		// AdjustmentTypeFilters.push(new nlobjSearchFilter(
		// 'custrecord_adjusttasktype', null, 'anyOf', '18'));
		// AdjustmentTypeFilters.push(new nlobjSearchFilter('isinactive',
		// null, 'is', 'F'));
		//
		// var columnsinvt = new Array();
		// columnsinvt[0] = new nlobjSearchColumn('name');
		// columnsinvt[1] = new nlobjSearchColumn('internalid');
		// columnsinvt[0].setSort();
		//
		// //columnsinvt[2] = new nlobjSearchColumn('custrecord_route_no');
		//
		// var searchresults =
		// nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null,
		// AdjustmentTypeFilters,columnsinvt);
		//
		// for (var i = 0; searchresults != null && i <
		// searchresults.length; i++) {
		// //Searching for Duplicates
		// var res=
		// InvAdjType.getSelectOptions(searchresults[i].getValue('name'),
		// 'is');
		// if (res != null) {
		//
		// if (res.length > 0) {
		// continue;
		// }
		// }
		// InvAdjType.addSelectOption(searchresults[i].getValue('internalid'),
		// searchresults[i].getValue('name'));
		// }

		sublist.addField("custpage_recid", "text", "RecId").setDisplayType('hidden');
		sublist.addField("custpage_rectype", "text", "RecType").setDisplayType('hidden');

		sublist.addField("custpage_itemid", "text", "itemid").setDisplayType('hidden');
		sublist.addField("custpage_locid", "text", "locid").setDisplayType('hidden');
		sublist.addField("custpage_oldqty", "text", "oldquantity").setDisplayType('hidden');
		sublist.addField("custpage_oldlp", "text", "LP#").setDisplayType('hidden');
		sublist.addField("custpage_olditemstatus", "text", "ItemStatus").setDisplayType('hidden');
		sublist.addField("custpage_accno", "text", "accountno").setDisplayType('hidden');
		sublist.addField("custpage_siteloc", "text", "siteloc").setDisplayType('hidden');
		sublist.addField("custpage_allocqtyhid", "text", "qtyalloc").setDisplayType('hidden');
		sublist.addField("custpage_company", "text", "Company").setDisplayType('hidden');
		sublist.addField("custpage_fifodate", "text", "fifodate").setDisplayType('hidden');
		sublist.addField("custpage_itemdesc", "textarea", "ItemDesc").setDisplayType('hidden');
		sublist.addField("custpage_id", "text", "internal id#").setDisplayType('hidden');//santosh
		sublist.addField("custpage_tasktype", "text", "TaskType").setDisplayType('hidden');//santosh
		sublist.addField("custpage_putmethod", "text", "putmethod").setDisplayType('hidden');//santosh

		var invtsearchresults = getInventorySearchResults(-1,request);

		if (invtsearchresults != null) {
			var temparraynew = new Array();
			var tempindex = 0;
			for (k = 0; k < invtsearchresults.length; k++) {
				var invtsearchresult = invtsearchresults[k];

				for ( var j = 0; j < invtsearchresult.length; j++) {
					temparraynew[tempindex] = invtsearchresult[j];
					tempindex = tempindex + 1;
				}
			}
			nlapiLogExecution('DEBUG', 'temparraynew.length ',temparraynew.length);
			// paging dropdown
			var test = '';
			if (temparraynew.length > 0) {
				if (temparraynew.length > 25) {

					var pagesize = form.addField('custpage_pagesize','text', 'Page Size').setDisplayType('entry');
					pagesize.setDisplaySize(10, 10);
					pagesize.setLayoutType('outsidebelow', 'startrow');
					var select = form.addField('custpage_selectpage','select', 'Select Records');
					select.setLayoutType('outsidebelow', 'startrow');
					select.setDisplaySize(200, 30);

					if (request.getMethod() == 'GET') {

						pagesize.setDefaultValue("25");
						pagesizevalue = 25;
					} else {
						if (request.getParameter('custpage_pagesize') != null) {
							pagesizevalue = request.getParameter('custpage_pagesize');
						} else {
							pagesizevalue = 25;
							pagesize.setDefaultValue("25");
						}

					}
					//form.setScript('customscript_inventoryclientvalidations');
					var len = temparraynew.length / parseFloat(pagesizevalue);
					for ( var k = 1; k <= Math.ceil(len); k++) {

						var from;
						var to;

						to = parseFloat(k) * parseFloat(pagesizevalue);
						from = (parseFloat(to) - parseFloat(pagesizevalue)) + 1;

						if (parseFloat(to) > temparraynew.length) {
							to = temparraynew.length;
							test = from.toString() + "," + to.toString();
						}

						var temp = from.toString() + " to " + to.toString();
						var tempto = from.toString() + "," + to.toString();
						select.addSelectOption(tempto, temp);

					}
					if (request.getMethod() == 'POST') {

						if (request.getParameter('custpage_selectpage') != null) {

							select.setDefaultValue(request
									.getParameter('custpage_selectpage'));

						}
						if (request.getParameter('custpage_pagesize') != null) {

							pagesize.setDefaultValue(request
									.getParameter('custpage_pagesize'));

						}
					} else {

					}
				} else {
					pagesizevalue = 25;
				}
				var minval = 0;
				var maxval = parseFloat(pagesizevalue);
				if (parseFloat(pagesizevalue) > temparraynew.length) {
					maxval = temparraynew.length;
				}
				var selectno = request.getParameter('custpage_selectpage');
				if (selectno != null) {
					var temp = request.getParameter('custpage_selectpage');
					var temparray = temp.split(',');
					nlapiLogExecution('DEBUG', 'temparray',
							temparray.length);

					var diff = parseFloat(temparray[1])
					- (parseFloat(temparray[0]) - 1);
					nlapiLogExecution('DEBUG', 'diff', diff);

					var pagevalue = request
					.getParameter('custpage_pagesize');
					nlapiLogExecution('DEBUG', 'pagevalue', pagevalue);
					if (pagevalue != null) {
						if (parseFloat(diff) == parseFloat(pagevalue)
								|| test == selectno) {

							var temparray = selectno.split(',');
							nlapiLogExecution('DEBUG', 'temparray.length ',
									temparray.length);
							minval = parseFloat(temparray[0]) - 1;
							nlapiLogExecution('DEBUG', 'temparray[0] ',
									temparray[0]);
							maxval = parseFloat(temparray[1]);
							nlapiLogExecution('DEBUG', 'temparray[1] ',
									temparray[1]);
						}
					}
				}
				nlapiLogExecution('DEBUG', 'invtsearchresults.length ',
						temparraynew.length);
				var index = 1;
				nlapiLogExecution('DEBUG', 'minval', minval);

				nlapiLogExecution('DEBUG', 'maxval ', maxval);
				for ( var s = minval; s < maxval; s++) {
					var invtsearchresult = temparraynew[s];

					location = invtsearchresult
					.getValue('custrecord_ebiz_inv_binloc');
					locationname = invtsearchresult
					.getText('custrecord_ebiz_inv_binloc');
					item = invtsearchresult
					.getValue('custrecord_ebiz_inv_sku');
					itemname = invtsearchresult
					.getText('custrecord_ebiz_inv_sku');
					lp = invtsearchresult
					.getValue('custrecord_ebiz_inv_lp');
					qty = invtsearchresult.getValue('custrecord_ebiz_qoh');
					lot = invtsearchresult
					.getValue('custrecord_ebiz_inv_lot');
					var lotText = invtsearchresult.getText('custrecord_ebiz_inv_lot');
					//nlapiLogExecution('DEBUG', 'lot ', lot);
					//nlapiLogExecution('DEBUG', 'lot text', lotText);
					itemstatus = invtsearchresult.getValue('custrecord_ebiz_inv_sku_status');
					packcode = invtsearchresult.getValue('custrecord_ebiz_inv_packcode');
					accontno = invtsearchresult.getValue('custrecord_ebiz_inv_account_no');
					invsiteloc = invtsearchresult.getValue('custrecord_ebiz_inv_loc');
					invcompany = invtsearchresult.getValue('custrecord_ebiz_inv_company');
					invfifodate = invtsearchresult.getValue('custrecord_ebiz_inv_fifo');
					invitemdesc = invtsearchresult.getValue('custrecord_ebiz_itemdesc');

					var totQty = 0;
					var allocQty = 0;
					if (invtsearchresult.getValue('custrecord_ebiz_qoh') != null
							&& invtsearchresult
							.getValue('custrecord_ebiz_qoh') != "") {
						totQty = parseFloat(invtsearchresult
								.getValue('custrecord_ebiz_qoh'));
						if (totQty < 0)
							totQty = 0;
					}
					if (invtsearchresult.getValue('custrecord_ebiz_alloc_qty') != null
							&& invtsearchresult
							.getValue('custrecord_ebiz_alloc_qty') != "") {
						allocQty = parseFloat(invtsearchresult
								.getValue('custrecord_ebiz_alloc_qty'));
						if (allocQty < 0)
							allocQty = 0;
					}

					var availQty = 0;
					availQty = parseFloat(totQty) - parseFloat(allocQty);

					form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_sno', index,(parseFloat(s) + 1).toString());
					form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_invlocation',index, locationname);
					form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_itemname', index,itemname);
					form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_lp', index, lp);
					form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_oldlp', index, lp);
					form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_itemqty', index,qty);
					form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_oldqty', index, qty);
					form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_olditemstatus',index, itemstatus);
					form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_accno', index,accontno);
					form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_siteloc', index,invsiteloc);
					// for record Id.
					form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_recid', index,invtsearchresult.getId());
					form.getSubList('custpage_invtmovelist').setLineItemValue('custpage_tasktype', index,invtsearchresult.getValue('custrecord_invttasktype'));
					// for loc id and itemid.
					form.getSubList('custpage_invtmovelist')
					.setLineItemValue('custpage_rectype', index,
							invtsearchresult.getRecordType());
					form.getSubList('custpage_invtmovelist')
					.setLineItemValue('custpage_itemid', index,
							item);
					form.getSubList('custpage_invtmovelist')
					.setLineItemValue('custpage_locid', index,
							location);
					form.getSubList('custpage_invtmovelist')
					.setLineItemValue('custpage_lot', index, lot);
					form.getSubList('custpage_invtmovelist')
					.setLineItemValue('custpage_itemstatus', index,
							itemstatus);
					form.getSubList('custpage_invtmovelist')
					.setLineItemValue('custpage_pc', index,
							packcode);
					form.getSubList('custpage_invtmovelist')
					.setLineItemValue('custpage_lottext', index,
							lotText);

					form.getSubList('custpage_invtmovelist')
					.setLineItemValue('custpage_totqty', index,
							totQty);
					form.getSubList('custpage_invtmovelist')
					.setLineItemValue('custpage_allocqty', index,
							allocQty);
					form.getSubList('custpage_invtmovelist')
					.setLineItemValue('custpage_allocqtyhid',
							index, allocQty);
					form.getSubList('custpage_invtmovelist')
					.setLineItemValue('custpage_availqty', index,
							availQty);
					form.getSubList('custpage_invtmovelist')
					.setLineItemValue('custpage_company', index,
							invcompany);
					form.getSubList('custpage_invtmovelist')
					.setLineItemValue('custpage_fifodate', index,
							invfifodate);
					form.getSubList('custpage_invtmovelist')
					.setLineItemValue('custpage_itemdesc', index,
							invitemdesc);

					index = index + 1;

				}
			}
		}

	}
	var button = form.addSubmitButton('Save');

	// code added on 20 Jun2012 by santosh

	var tempflag = form.addField('custpage_tempflag', 'text','tempory flag').setDisplayType('hidden');

	var fullfillmentOrderfield = form.addField('custpage_fullinventroy','text', 'Fulfillment Order#');
	var date = form.addField('custpage_date', 'date', 'Date').setDisplaySize('30', '30');
	var Period=form.addField('custpage_accountingperiod', 'select', 'Account Period');

	var filterLocation = new Array();
	filterLocation.push(new nlobjSearchFilter('closed', null, 'is', 'F'));
	filterLocation.push(new nlobjSearchFilter('isquarter', null, 'is', 'F'));
	filterLocation.push(new nlobjSearchFilter('isyear', null, 'is', 'F'));
	var columnLocation = new Array();
	columnLocation[0] = new nlobjSearchColumn('periodname');
	columnLocation[1] = new nlobjSearchColumn('internalId'); 
	columnLocation[1].setSort();
	Period.addSelectOption("", "");
	var searchLocationRecord = nlapiSearchRecord('AccountingPeriod', null, filterLocation, columnLocation);
	for(var count=0;count < searchLocationRecord.length ;count++)
	{
		Period.addSelectOption(searchLocationRecord[count].getId(),searchLocationRecord[count].getValue('periodname'));

	} 
	if(request.getParameter('custpage_accountingperiod')!=null)
	{
		Period.setDefaultValue(request.getParameter('custpage_accountingperiod'));
	}
	//invtlp.addSelectOption('', '');
	if(request.getParameter('custpage_date')!='' && request.getParameter('custpage_date')!=null)
	{
		date.setDefaultValue(request.getParameter('custpage_date'));	
	}
	form.setScript('customscript_cs');
	form.addButton('custpage_generateloc', 'Generate New Bin Location','Display()');
	//end of code 20 Jun 2012

	//} else {
	//	var msg = form.addField('custpage_message', 'inlinehtml', null, null,null);
	//	msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'No Records', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
	//}
	response.writePage(form);
}


var searchResultsForBinLoc = new Array();
function FillBinLocation(maxno)
{
	var filtersinv = new Array();
	filtersinv[0] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3, 19]);
	if(parseFloat(maxno)!=-1)
	{
		nlapiLogExecution('DEBUG', 'maxno', maxno);
		filtersinv[1]=new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno));
	}
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	//santosh
	//columns[3]= new nlobjSearchColumn('internalid');
	columns[3]= new nlobjSearchColumn('id');
	columns[3].setSort(true);
	var searchInventory = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinv, columns);
	if(searchInventory!=null && searchInventory.length>=1000)
	{
		nlapiLogExecution('DEBUG', 'searchInventory for binloc', searchInventory.length);
		//var maxno=searchInventory[searchInventory.length-1].getValue(columns[1]);		
		var maxno=searchInventory[searchInventory.length-1].getValue('id');	
		searchResultsForBinLoc.push(searchInventory);
		FillBinLocation(maxno);	

	}
	else if(searchInventory!=null&&searchInventory!="")
	{
		nlapiLogExecution('DEBUG', 'openTaskRecords for binloc2', searchInventory.length);
		searchResultsForBinLoc.push(searchInventory);
	}
	return searchResultsForBinLoc;
}


function vGetPutawayLocation(Item, PackCode, ItemStatus, UOM, ItemCube, 
		ItemType,poLocn,vItemLocId,lotbinlocarray,vlot){
	var context = nlapiGetContext();
	nlapiLogExecution('DEBUG','Remaining usage at the start',context.getRemainingUsage());
	var ItemFamily, ItemGroup, Location;
	var location_found = false;
	var LocArry = new Array();
	var OBLocGroup, IBLocGroup;
	var PickSeqNo, PutSeqNo, PutMethod,PutMethodName,PutRuleId, MergeFlag = 'F',Mixsku='F',PutBinLoc;
	nlapiLogExecution('DEBUG', 'ItemCube', ItemCube);//0
	nlapiLogExecution('DEBUG', 'ItemType', ItemType);
	var itemresults;

	var fields = ['custitem_item_family','custitem_item_group','custitem_ebizdefskustatus',
	              'custitem_item_info_1','custitem_item_info_2','custitem_item_info_3','custitem_ebizabcvelitem'];


	var columns= nlapiLookupField('item',Item,fields);
	var ItemFamily = columns.custitem_item_family;
	var ItemGroup = columns.custitem_item_group;
	var ItemInfo1 = columns.custitem_item_info_1;
	var ItemInfo2 = columns.custitem_item_info_2;
	var ItemInfo3 = columns.custitem_item_info_3;
	var putVelocity=columns.custitem_ebizabcvelitem;

	nlapiLogExecution('DEBUG', 'Item', Item);
	nlapiLogExecution('DEBUG', 'ItemFamily', ItemFamily);
	nlapiLogExecution('DEBUG', 'ItemGroup', ItemGroup);
	nlapiLogExecution('DEBUG', 'ItemStatus', ItemStatus);
	nlapiLogExecution('DEBUG', 'ItemInfo1', ItemInfo1);
	nlapiLogExecution('DEBUG', 'ItemInfo2', ItemInfo2);
	nlapiLogExecution('DEBUG', 'ItemInfo3', ItemInfo3);
	nlapiLogExecution('DEBUG', 'putVelocity', putVelocity);
	nlapiLogExecution('DEBUG', 'poLocn', poLocn);
	nlapiLogExecution('DEBUG', 'vItemLocId', vItemLocId);

	var locgroupid = 0;
	var zoneid = 0;

	// if (ItemFamily != null || ItemFamily != "") {
	var columns = new Array();
	var i=0;
	var filters = new Array();
	columns[0] = new nlobjSearchColumn('formulanumeric');
	columns[0].setFormula("TO_NUMBER({custrecord_sequencenumberpickrule})");
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
	columns[2] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
	columns[3] = new nlobjSearchColumn('custrecord_putawaymethod');
	columns[4] = new nlobjSearchColumn('custrecord_ruleidpickrule');
	columns[5] = new nlobjSearchColumn('custrecord_manual_location_generation');
	columns[6] = new nlobjSearchColumn('custrecord_locationpickrule');


	var ItemFamArr = new Array();
	ItemFamArr[0] = ItemFamily;
	ItemFamArr[1] = '@NONE@';
	//Case # 20125990 start
	if(ItemFamily!=null && ItemFamily != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skufamilypickrule', null, 'anyof', ['@NONE@', ItemFamily]));
	}
	else
	{
		//filters.push(new nlobjSearchFilter('custrecord_skufamilypickrule', null, 'anyof', ['@NONE@']));	
	}

	if(ItemGroup!=null && ItemGroup != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skugrouppickrule', null, 'anyof', ['@NONE@', ItemGroup]));
	}
	else
	{
		//filters.push(new nlobjSearchFilter('custrecord_skugrouppickrule', null, 'anyof', ['@NONE@']));	
	}

	if(ItemStatus!=null && ItemStatus != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skustatuspickrule', null, 'anyof', ['@NONE@', ItemStatus]));
	}
	else
	{
		//filters.push(new nlobjSearchFilter('custrecord_skustatuspickrule', null, 'anyof', ['@NONE@']));	
	}

	if(ItemInfo1!=null && ItemInfo1 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo1pickrule', null, 'anyof',['@NONE@', ItemInfo1]));
	}

	if(ItemInfo2!=null && ItemInfo2 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo2pickrule', null, 'anyof',['@NONE@', ItemInfo2]));
	}

	if(ItemInfo3!=null && ItemInfo3 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo3pickrule', null, 'anyof',['@NONE@', ItemInfo3]));
	}

	if(Item != null && Item != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skupickrule', null, 'anyof', ['@NONE@', Item]));		
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_skupickrule', null, 'anyof', ['@NONE@']));	
	}

	if(putVelocity != null && putVelocity != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_abcvelpickrule', null, 'anyof', ['@NONE@', putVelocity]));
	}

	if(poLocn != null && poLocn != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizsitepickput', null, 'anyof', [poLocn]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizsitepickput', null, 'anyof', ['@NONE@']));	
	}
	//Case # 20125990 end
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var t1 = new Date();
	var rulesearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters, columns);
	var t2 = new Date();
	nlapiLogExecution('DEBUG', 'nlapiSearchRecord:GetPutawayLocation:putaway_rule',
			getElapsedTimeDuration(t1, t2));

	if (rulesearchresults != null) {
		nlapiLogExecution('DEBUG', 'rules count', rulesearchresults.length);
		for (var s = 0; s < rulesearchresults.length; s++) {
			locgroupid = rulesearchresults[s].getValue('custrecord_locationgrouppickrule');
			zoneid = rulesearchresults[s].getValue('custrecord_putawayzonepickrule');
			PutMethod = rulesearchresults[s].getValue('custrecord_putawaymethod');
			PutMethodName = rulesearchresults[s].getText('custrecord_putawaymethod');
			PutRuleId = rulesearchresults[s].getValue('custrecord_ruleidpickrule');
			PutBinLoc = rulesearchresults[s].getValue('custrecord_locationpickrule');
			nlapiLogExecution('DEBUG', 'Put Rule', PutRuleId);
			nlapiLogExecution('DEBUG', 'Put Method', PutMethod);
			nlapiLogExecution('DEBUG', 'PutBinLoc', PutBinLoc);
			nlapiLogExecution('DEBUG', 'Location Group Id from Putaway Rule', locgroupid);

			// To indicate if the location has to be generated manually based on the parameter in putaway strategy
			ManualLocationGeneration = rulesearchresults[s].getValue('custrecord_manual_location_generation');
			nlapiLogExecution('DEBUG', 'Manual Location Generation', ManualLocationGeneration);

			if (ManualLocationGeneration == 'F') {
				//if (locgroupid != 0 && locgroupid != null) {
				if ((locgroupid != 0 && locgroupid != null) || (PutBinLoc != "" && PutBinLoc != null)){
					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_startingputseqno');
					columns[0].setSort();
					columns[1] = new nlobjSearchColumn('name');
					columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid');
					columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno');
					columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid');
					columns[5] = new nlobjSearchColumn('custrecord_remainingcube');	

					nlapiLogExecution('DEBUG', 'locgroupid', locgroupid);

					var filters = new Array();
					filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [locgroupid]));	
					filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
					if(poLocn != null && poLocn != "")
						filters.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof', [poLocn]));
					if((vItemLocId!=null && vItemLocId!="" && vItemLocId !='null')&& (PutBinLoc == null || PutBinLoc == ""))
						filters.push(new nlobjSearchFilter('internalid', null, 'noneof', vItemLocId));
					if(PutBinLoc != null && PutBinLoc != "")
						filters.push(new nlobjSearchFilter('internalid', null, 'is', PutBinLoc));
					//case# 201413037
					filters.push(new nlobjSearchFilter('custrecord_remainingcube', null, 'greaterthanorequalto', ItemCube));
					t1 = new Date();
					var locResults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);
					t2 = new Date();
					nlapiLogExecution('DEBUG', 'nlapiSearchRecord:GetPutawayLocation:location',
							getElapsedTimeDuration(t1, t2));

					nlapiLogExecution('DEBUG', 'locResults', locResults);
					if (locResults != null) {
						nlapiLogExecution('DEBUG', 'ifnot ');
						for (var u = 0; u < locResults.length; u++) {

							Location = locResults[u].getValue('name');
							var locationIntId = locResults[u].getId();

							nlapiLogExecution('DEBUG', 'Location', Location);
							nlapiLogExecution('DEBUG', 'Location Id', locationIntId);

							//Commented by satish.N on 04-JAN-2012 as the search results will give the same value.
							//LocRemCube = GeteLocCube(locationIntId);
							LocRemCube = locResults[u].getValue('custrecord_remainingcube');							
							OBLocGroup = locResults[u].getValue('custrecord_outboundinvlocgroupid');
							PickSeqNo  = locResults[u].getValue('custrecord_startingpickseqno');
							IBLocGroup = locResults[u].getValue('custrecord_inboundinvlocgroupid');
							PutSeqNo   = locResults[u].getValue('custrecord_startingputseqno');

							var IsExistsPFloc='F';
							var isExistsPFlocresults = getPickFaceLocation(locationIntId);

							if(isExistsPFlocresults!=null && isExistsPFlocresults!='')
							{
								for(var pf = 0; pf < isExistsPFlocresults.length; pf++)
								{
									var pickfaceitem=isExistsPFlocresults[pf].getValue('custrecord_pickfacesku');
									nlapiLogExecution('DEBUG', 'pickfaceitem', pickfaceitem);
									nlapiLogExecution('DEBUG', 'Item', Item);
									if(pickfaceitem != Item)
									{
										IsExistsPFloc='T';
									}
									else //  case# 201413037
									{
										IsExistsPFloc='F';
										break;
									}
								}
							}
							nlapiLogExecution('DEBUG', 'isExistsPFloc', IsExistsPFloc);
							nlapiLogExecution('DEBUG', 'LocRemCube', LocRemCube);
							nlapiLogExecution('DEBUG', 'ItemCube', ItemCube);

							var isDiffLotExist = CheckDiffLotExist(lotbinlocarray,vlot,locationIntId);

							if(IsExistsPFloc=='F' && isDiffLotExist!='T')
							{
								if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {
									RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);
									location_found = true;

									LocArry[0] = Location;
									LocArry[1] = RemCube;
									LocArry[2] = locationIntId;	
									LocArry[3] = OBLocGroup;
									LocArry[4] = PickSeqNo;
									LocArry[5] = IBLocGroup;
									LocArry[6] = PutSeqNo;
									LocArry[7] = LocRemCube;
									LocArry[8] = zoneid;
									LocArry[9] = PutMethodName;
									LocArry[10] = PutRuleId;
									LocArry[11] = PutMethod;
									nlapiLogExecution('DEBUG', 'SA-Location', Location);
									nlapiLogExecution('DEBUG', 'Location Internal Id',locationIntId);
									nlapiLogExecution('DEBUG', 'Sa-Location Cube', RemCube);
									return LocArry;
								}
							}
						}
					}
				}
				else {
					nlapiLogExecution('DEBUG', 'Fetching Location Group from Zone', zoneid);
					var mixarray =  new Array();
					if(PutMethod!=null && PutMethod!='')
						mixarray=GetMergeFlag(PutMethod);
					if(mixarray!=null && mixarray!='' && mixarray.length>0)
					{
						MergeFlag = mixarray[0][0];
						Mixsku = mixarray[0][1];
					}
					nlapiLogExecution('DEBUG', 'Merge Flag', MergeFlag);
					nlapiLogExecution('DEBUG', 'Mix SKU', Mixsku);

					if (zoneid != null && zoneid != ""){ 
						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_zone_seq');
						columns[0].setSort();
						columns[1] = new nlobjSearchColumn('custrecord_locgroup_no');

						var filters = new Array();
						filters.push(new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'is', zoneid));
						filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
						filters.push(new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', ['@NONE@', poLocn]));
						t1 = new Date();
						var searchresults = nlapiSearchRecord('customrecord_zone_locgroup', null, filters, columns);
						t2 = new Date();
						nlapiLogExecution('DEBUG', 'nlapiSearchRecord:GetPutawayLocation:zone_locgroup',
								getElapsedTimeDuration(t1, t2));

						var RemCube = 0;
						var LocRemCube = 0;

						if (searchresults != null && searchresults != '') 
						{

							for (var i = 0; i < Math.min(30, searchresults.length); i++) {
								locgroupid = searchresults[i].getValue('custrecord_locgroup_no');
								nlapiLogExecution('DEBUG', 'Fetched Location Group Id from Putaway Zone', locgroupid);

								if (MergeFlag == 'T') {
									nlapiLogExecution('DEBUG', 'Inside Merge ');
									var locResults;
									nlapiLogExecution('DEBUG', 'ItemCube', ItemCube);
									nlapiLogExecution('DEBUG', 'poLocn', poLocn);
									nlapiLogExecution('DEBUG', 'locgroupid', locgroupid);

									try {
										var columns = new Array();
										columns[0] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');
										columns[0].setSort();
										columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
										columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc');
										columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno', 'custrecord_ebiz_inv_binloc');
										columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc');
										columns[5] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');
										columns[6] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
										columns[7] = new nlobjSearchColumn('custrecord_remainingcube','custrecord_ebiz_inv_binloc');

										var filters = new Array();
										if(Mixsku!='T')
										{
											filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', Item));
										}

										if(poLocn!=null && poLocn!='')
											filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', poLocn));
										filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'is', locgroupid));
										filters.push(new nlobjSearchFilter('custrecord_remainingcube', 'custrecord_ebiz_inv_binloc', 'greaterthanorequalto', ItemCube));                                        
										if(vItemLocId!=null&&vItemLocId!="")
											filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'noneof', vItemLocId));

										filters.push(new nlobjSearchFilter('isinactive', 'custrecord_ebiz_inv_binloc', 'is', 'F'));

										t1 = new Date();
										locResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
										t2 = new Date();
										nlapiLogExecution('DEBUG', 'nlapiSearchRecord:GetPutawayLocation:createinv',
												getElapsedTimeDuration(t1, t2));
									} //try close.
									catch (Error) {
										nlapiLogExecution('DEBUG', 'Into Error', Error);
									}
									try {
										nlapiLogExecution('DEBUG', 'Inside Try', 'TRY');

										if (locResults != null && locResults != '') {
											nlapiLogExecution('DEBUG', 'LocationResults', locResults.length );
											for (var j = 0; j < locResults.length; j++) {
												nlapiLogExecution('DEBUG', 'locResults[j].getId()', locResults[j].getId());
												Location = locResults[j].getText('custrecord_ebiz_inv_binloc');
												var locationInternalId = locResults[j].getValue('custrecord_ebiz_inv_binloc');

												nlapiLogExecution('DEBUG', 'Location', Location );
												nlapiLogExecution('DEBUG', 'Location Id', locationInternalId);

												var IsExistsPFloc='F';
												var isExistsPFlocresults = getPickFaceLocation(locationInternalId);
												if(isExistsPFlocresults!=null && isExistsPFlocresults!='')
												{
													for(var pf = 0; pf < isExistsPFlocresults.length; pf++)
													{
														var pickfaceitem=isExistsPFlocresults[pf].getValue('custrecord_pickfacesku');
														if(pickfaceitem != Item)
														{
															IsExistsPFloc='T';
														}
														else
														{
															IsExistsPFloc='F';
															break;
														}
													}
												}
												nlapiLogExecution('DEBUG', 'IsExistsPFloc', IsExistsPFloc);

												var isDiffLotExist = CheckDiffLotExist(lotbinlocarray,vlot,locationInternalId);

												//LocRemCube = GeteLocCube(locationInternalId);
												if(IsExistsPFloc=='F' && isDiffLotExist!='T'){
													LocRemCube = locResults[j].getValue('custrecord_remainingcube', 'custrecord_ebiz_inv_binloc');
													OBLocGroup = locResults[j].getValue('custrecord_outboundlocgroupid', 'custrecord_ebiz_inv_binloc');
													PickSeqNo = locResults[j].getValue('custrecord_startingpickseqno', 'custrecord_ebiz_inv_binloc');
													IBLocGroup = locResults[j].getValue('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc');
													PutSeqNo = locResults[j].getValue('custrecord_startingputseqno', 'custrecord_ebiz_inv_binloc');

													nlapiLogExecution('DEBUG', 'Loc Rem Cube', LocRemCube);
													nlapiLogExecution('DEBUG', 'Item Cube', ItemCube);

													if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {

														RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);

														location_found = true;

														LocArry[0] = Location;
														LocArry[1] = RemCube;
														LocArry[2] = locationInternalId;		//locResults[j].getId();
														LocArry[3] = OBLocGroup;
														LocArry[4] = PickSeqNo;
														LocArry[5] = IBLocGroup;
														LocArry[6] = PutSeqNo;
														LocArry[7] = LocRemCube;
														LocArry[8] = zoneid;
														LocArry[9] = PutMethodName;
														LocArry[10] = PutRuleId;
														LocArry[11] = PutMethod;
														nlapiLogExecution('DEBUG', 'after changes Location Cube', RemCube);
														return LocArry;
													}
												}
											}
										}
									} 
									catch (exps) {
										nlapiLogExecution('DEBUG', 'included exps', exps);
									}

									//Added by satish.N on 04-JAN-2012 to consider open taks while merging.
									if (location_found == false) {

										try{
											nlapiLogExecution('DEBUG', 'Inside  consider open taks while merging');

											var columns = new Array();
											columns[0] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_actbeginloc');
											columns[0].setSort();
											columns[1] = new nlobjSearchColumn('custrecord_actbeginloc');
											columns[2] = new nlobjSearchColumn('custrecord_outboundlocgroupid', 'custrecord_actbeginloc');
											columns[3] = new nlobjSearchColumn('custrecord_startingpickseqno', 'custrecord_actbeginloc');
											columns[4] = new nlobjSearchColumn('custrecord_inboundlocgroupid', 'custrecord_actbeginloc');
											columns[5] = new nlobjSearchColumn('custrecord_startingputseqno', 'custrecord_actbeginloc');
											columns[6] = new nlobjSearchColumn('custrecord_sku');
											columns[7] = new nlobjSearchColumn('custrecord_remainingcube','custrecord_actbeginloc');

											var filters = new Array();
											if(Mixsku!='T')
											{
												filters.push(new nlobjSearchFilter('custrecord_sku', null, 'is', Item));
											}	
											if(poLocn!=null && poLocn!='')
												filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', [poLocn]));
											filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
											filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_actbeginloc', 'is', locgroupid));
											filters.push(new nlobjSearchFilter('custrecord_remainingcube', 'custrecord_actbeginloc', 'greaterthanorequalto', ItemCube));
											if(vItemLocId!=null&&vItemLocId!="")
												filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'noneof', vItemLocId));
											locResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

											if (locResults != null && locResults != '') {
												for (var j = 0; j < locResults.length; j++) {

													Location = locResults[j].getText('custrecord_actbeginloc');
													var locationInternalId = locResults[j].getValue('custrecord_actbeginloc');

													nlapiLogExecution('DEBUG', 'Location', Location );
													nlapiLogExecution('DEBUG', 'Location Id', locationInternalId);

													var IsExistsPFloc='F';
													var isExistsPFlocresults = getPickFaceLocation(locationInternalId);
													if(isExistsPFlocresults!=null && isExistsPFlocresults!='')
													{
														for(var pf = 0; pf < isExistsPFlocresults.length; pf++)
														{
															var pickfaceitem=isExistsPFlocresults[pf].getValue('custrecord_pickfacesku');
															if(pickfaceitem != Item)
															{
																IsExistsPFloc='T';
															}
															else
															{
																IsExistsPFloc='F';
																break;
															}
														}
													}
													nlapiLogExecution('DEBUG', 'isExistsPFloc', IsExistsPFloc);

													var isDiffLotExist = CheckDiffLotExist(lotbinlocarray,vlot,locationInternalId);

													if(IsExistsPFloc=='F' && isDiffLotExist!='T'){
														LocRemCube = locResults[j].getValue('custrecord_remainingcube', 'custrecord_actbeginloc');
														OBLocGroup = locResults[j].getValue('custrecord_outboundlocgroupid', 'custrecord_actbeginloc');
														PickSeqNo = locResults[j].getValue('custrecord_startingpickseqno', 'custrecord_actbeginloc');
														IBLocGroup = locResults[j].getValue('custrecord_inboundlocgroupid', 'custrecord_actbeginloc');
														PutSeqNo = locResults[j].getValue('custrecord_startingputseqno', 'custrecord_actbeginloc');

														nlapiLogExecution('DEBUG', 'Loc Rem Cube', LocRemCube);
														nlapiLogExecution('DEBUG', 'Item Cube', ItemCube);

														if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {

															RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);

															location_found = true;

															LocArry[0] = Location;
															LocArry[1] = RemCube;
															LocArry[2] = locationInternalId;		//locResults[j].getId();
															LocArry[3] = OBLocGroup;
															LocArry[4] = PickSeqNo;
															LocArry[5] = IBLocGroup;
															LocArry[6] = PutSeqNo;
															LocArry[7] = LocRemCube;
															LocArry[8] = zoneid;
															LocArry[9] = PutMethodName;
															LocArry[10] = PutRuleId;
															LocArry[11] = PutMethod;
															nlapiLogExecution('DEBUG', 'after changes Location Cube', RemCube);
															return LocArry;
														}
													}		
												}
											}
										}
										catch(exps){
											nlapiLogExecution('DEBUG', 'included exps in opentasks', exps);
										}
									}
								}
								if (location_found == false) {

									//code modified by suman on 27/01/12
									//Instead of searching each and individual binloc in inventory and open task record it seems to have a goverence issue.
									//So I bought all binloc from open task and inventory records depending upon "locgr"Id and placing it in an array so that on comparing this array with the binlocation 
									//will decides weather it is empty or not.

									nlapiLogExecution('DEBUG', 'Location Not found for Merge Location for Location Group', locgroupid);
									var locResults = GetLocation(locgroupid,ItemCube);

									if (locResults != null && locResults != '') {

										var TotalListOfBinLoc=new Array();
										for ( var x = 0; x < locResults.length; x++) 
										{
											TotalListOfBinLoc[x]=locResults[x].getId();

										}
										var emptyloccheck =	binLocationChecknew(TotalListOfBinLoc);
										nlapiLogExecution('DEBUG', 'emptyloccheck', emptyloccheck);

										nlapiLogExecution('DEBUG', 'locResults.length', locResults.length);
										for (var j = 0; j < Math.min(300, locResults.length); j++) {
											var LocFlag='N';
											var emptyBinLocationId = locResults[j].getId();
											nlapiLogExecution('DEBUG', 'emptyBinLocationId', emptyBinLocationId);
//											var emptyloccheck =	binLocationCheck(emptyBinLocationId);
//											nlapiLogExecution('DEBUG', 'emptyloccheck', emptyloccheck);

											for ( var InvBinLocCount = 0; InvBinLocCount < emptyloccheck[0].length; InvBinLocCount++)
											{
//												nlapiLogExecution('DEBUG', 'emptyloccheck[0][InvBinLocCount]', emptyloccheck[0][InvBinLocCount]);

												if(emptyloccheck[0][InvBinLocCount]==emptyBinLocationId)
												{
													nlapiLogExecution('DEBUG', 'emptyBinLocationId+InvBinLocCount', emptyBinLocationId+','+emptyloccheck[0][InvBinLocCount]);
													LocFlag='Y';
													break;
												}
											}
											for ( var OpenTaskBinLocCount = 0; OpenTaskBinLocCount < emptyloccheck[1].length; OpenTaskBinLocCount++)
											{
												if(emptyloccheck[1][OpenTaskBinLocCount]==emptyBinLocationId)
												{
													nlapiLogExecution('DEBUG', 'emptyBinLocationId+OpenTaskBinLocCount', emptyBinLocationId+','+emptyloccheck[1][OpenTaskBinLocCount]);
													LocFlag='Y';
													break;
												}
											}
//											end of code changed 27/01/12 
											nlapiLogExecution('DEBUG', 'LocFlag', LocFlag);
											nlapiLogExecution('DEBUG', 'Main Loop ', s);
											nlapiLogExecution('DEBUG','Remaining usage ',context.getRemainingUsage());
											if(LocFlag == 'N'){
												//Changes done by Sarita (index i is changed to j).
												LocRemCube = locResults[j].getValue('custrecord_remainingcube');
												OBLocGroup = locResults[j].getValue('custrecord_outboundlocgroupid');
												PickSeqNo = locResults[j].getValue('custrecord_startingpickseqno');
												IBLocGroup = locResults[j].getValue('custrecord_inboundlocgroupid');
												PutSeqNo = locResults[j].getValue('custrecord_startingputseqno');

												nlapiLogExecution('DEBUG', 'LocRemCube', LocRemCube);
												nlapiLogExecution('DEBUG', 'ItemCube', ItemCube);

												var IsExistsPFloc='F';
												var isExistsPFlocresults = getPickFaceLocation(locResults[j].getId());

												if(isExistsPFlocresults!=null && isExistsPFlocresults!='')
												{
													for(var pf = 0; pf < isExistsPFlocresults.length; pf++)
													{
														var pickfaceitem=isExistsPFlocresults[pf].getValue('custrecord_pickfacesku');
														if(pickfaceitem != Item)
														{
															IsExistsPFloc='T';
														}
														else
														{
															IsExistsPFloc='F';
															break;
														}
													}
												}
												nlapiLogExecution('DEBUG', 'isExistsPFloc', IsExistsPFloc);

												var isDiffLotExist = CheckDiffLotExist(lotbinlocarray,vlot,locResults[j].getId());

												if(IsExistsPFloc=='F' && isDiffLotExist!='T')
												{
													if (parseFloat(LocRemCube) >= parseFloat(ItemCube)) {
														RemCube = parseFloat(LocRemCube) - parseFloat(ItemCube);
														location_found = true;
														Location = locResults[j].getValue('name');
														LocArry[0] = Location;
														LocArry[1] = RemCube;
														LocArry[2] = locResults[j].getId();
														LocArry[3] = OBLocGroup;
														LocArry[4] = PickSeqNo;
														LocArry[5] = IBLocGroup;
														LocArry[6] = PutSeqNo;
														LocArry[7] = LocRemCube;
														LocArry[8] = zoneid;
														LocArry[9] = PutMethodName;
														LocArry[10] = PutRuleId;
														LocArry[11] = PutMethod;
														nlapiLogExecution('DEBUG', 'Location', Location);
														nlapiLogExecution('DEBUG', 'Location Cube', RemCube);
														return LocArry;
														break;
													}
												}
											}
										}
									}
								}
							}
						}//end of searchresult null checking
					}
				}
				nlapiLogExecution('DEBUG', 'Main Loop ', s);
				nlapiLogExecution('DEBUG','Remaining usage ',context.getRemainingUsage());
			}
		}
	}// end of put rule for loop
	else
	{
		nlapiLogExecution('DEBUG', 'No Searchresults with above mentioned criteria ', 'Null Results');
	}
	//  } // end of main if
	return LocArry;
}


function CheckDiffLotExist(lotbinlocarray,vlot,locationIntId)
{
	nlapiLogExecution('DEBUG', 'Into  CheckDiffLotExist', vlot);
	var isDiffLotExist='F';

	if(vlot!=null && vlot!='')
	{
		if(lotbinlocarray!=null && lotbinlocarray!='')
		{
			for (var e = 0; e < lotbinlocarray.length; e++) 
			{ 
				if(lotbinlocarray[e][0]==locationIntId)
				{
					if(lotbinlocarray[e][1]!=vlot)
					{
						isDiffLotExist='T';
					}
				}
			}
		}
	}

	nlapiLogExecution('DEBUG', 'Out of  CheckDiffLotExist', isDiffLotExist);
	return isDiffLotExist;
}

var searchresultsForNewBinloc=new Array();
function FillNewBinLocation(maxno,whloc)
{
	nlapiLogExecution('DEBUG', ' whloc', whloc);
	var filtersBinloc = new Array();
	filtersBinloc.push(new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'anyof', ['6','7']));
	filtersBinloc.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof', whloc));
	filtersBinloc.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(parseFloat(maxno)!=-1)
	{
		nlapiLogExecution('DEBUG', 'maxno', maxno);
		filtersBinloc.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno)));
	}
	var colsBinloc = new Array();
	colsBinloc[0]=new nlobjSearchColumn('name');
	colsBinloc[1]= new nlobjSearchColumn('id');
	colsBinloc[1].setSort(true);//For ascending order

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersBinloc, colsBinloc);
	if(searchresults!=null && searchresults.length>=1000)
	{
		nlapiLogExecution('DEBUG', 'searchresults for binloc', searchresults.length);
		var maxno=searchresults[searchresults.length-1].getValue('id');	
		searchresultsForNewBinloc.push(searchresults);
		//case 20125155  Start : added location as parameter 
		FillNewBinLocation(maxno,whloc);	
		//case 20125155  end :

	}
	else if(searchresults!=null&&searchresults!="")
	{
		nlapiLogExecution('DEBUG', 'searchresults for binloc2', searchresults.length);
		searchresultsForNewBinloc.push(searchresults);
	}
	return searchresultsForNewBinloc;
}

function getmaxuomqty(getItemId)
{
	nlapiLogExecution('DEBUG', 'Into getmaxuomqty','');
	nlapiLogExecution('DEBUG', 'Item ID',getItemId);

	var maxuomqty=0;
	var filtersmlp = new Array();

	filtersmlp.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', getItemId));
	filtersmlp.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var col=new Array();
	col[0] = new nlobjSearchColumn('custrecord_ebizqty');
	col[0].setSort('True');

	var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filtersmlp,col);
	if (SrchRecord != null && SrchRecord !="" && SrchRecord.length>0)
	{
		maxuomqty = SrchRecord[0].getValue('custrecord_ebizqty');
	}

	nlapiLogExecution('DEBUG', 'Out of getmaxuomqty',maxuomqty);
	return maxuomqty;
}

function GetMainWareHouseSite(whloc)
{
	try
	{
		nlapiLogExecution('DEBUG','Into GetMainWareHouseSite',whloc);
		var MHsite="";
		if(whloc!=null&&whloc!="")
		{
			var rec=nlapiLoadRecord('location',whloc);
			MHsite=rec.getFieldValue('parent');
		}
		nlapiLogExecution('DEBUG','MHsite',MHsite);
		return MHsite;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in GetMainWareHouseSite',exp);
	}
}

function AdjustSerialNumbers(localSerialNoArray,qty,item,lp,newlp,Continuemove,NewLoc,oldqty,NewWHSite)
{	
	nlapiLogExecution('DEBUG','AdjustSerialNumbers qty',qty);
	nlapiLogExecution('DEBUG','item',item);
	nlapiLogExecution('DEBUG','lp',lp);
	if(parseInt(qty) > 0)
	{
		if(Continuemove == "Y")
		{
			if(parseFloat(qty) == parseFloat(oldqty))
			{
				var filters1 = new Array();
				filters1[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
				filters1[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
				//filters1[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'D');

				var columns1 = new Array();
				columns1[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults1 = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters1, columns1);             

				if(searchResults1 != null && searchResults1 != "")
				{ 
					for (var i = 0; i < searchResults1.length; i++) 
					{   
						if(searchResults1[i].getValue('custrecord_serialnumber') != null && searchResults1[i].getValue('custrecord_serialnumber') != "")
						{
							var SerialNo1 = searchResults1[i].getValue('custrecord_serialnumber');
							var InternalID1 = searchResults1[i].getId();

							var currentRow1 = [SerialNo1];					
							localSerialNoArray.push(currentRow1);
							nlapiLogExecution('DEBUG', 'localSerialNoArray : ', localSerialNoArray.toString());

							var LoadSerialNumbers1 = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID1);
							LoadSerialNumbers1.setFieldValue('custrecord_serialparentid', newlp);
							LoadSerialNumbers1.setFieldValue('custrecord_serialbinlocation', NewLoc);
							if(NewWHSite != null && NewWHSite != "")
								LoadSerialNumbers1.setFieldValue('custrecord_serial_location', NewWHSite);
							LoadSerialNumbers1.setFieldValue('custrecord_serialstatus', '');

							var recid1 = nlapiSubmitRecord(LoadSerialNumbers1, false, true);
							nlapiLogExecution('DEBUG', 'Serial Entry rec id',recid1);
						}
					}
				}
			}
			else
			{
				var filters1 = new Array();
				filters1[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
				filters1[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
				filters1[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'D');

				var columns1 = new Array();
				columns1[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults1 = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters1, columns1);             

				if(searchResults1 != null && searchResults1 != "")
				{ 
					for (var i = 0; i < searchResults1.length; i++) 
					{   
						if(searchResults1[i].getValue('custrecord_serialnumber') != null && searchResults1[i].getValue('custrecord_serialnumber') != "")
						{
							var SerialNo1 = searchResults1[i].getValue('custrecord_serialnumber');
							var InternalID1 = searchResults1[i].getId();

							var currentRow1 = [SerialNo1];					
							localSerialNoArray.push(currentRow1);
							nlapiLogExecution('DEBUG', 'localSerialNoArray : ', localSerialNoArray.toString());

							var LoadSerialNumbers1 = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID1);
							LoadSerialNumbers1.setFieldValue('custrecord_serialparentid', newlp);
							LoadSerialNumbers1.setFieldValue('custrecord_serialbinlocation', NewLoc);
							LoadSerialNumbers1.setFieldValue('custrecord_serialstatus', '');
							if(NewWHSite != null && NewWHSite != "")
								LoadSerialNumbers1.setFieldValue('custrecord_serial_location', NewWHSite);

							var recid1 = nlapiSubmitRecord(LoadSerialNumbers1, false, true);
							nlapiLogExecution('DEBUG', 'Serial Entry rec id',recid1);
						}
					}
				}
			}
		}

		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
		filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
		//filters[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'isnot', 'D');

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
		var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

		if(searchResults != null && searchResults != "")
		{ 
			for (var i = 0; i < searchResults.length; i++) 
			{   
				if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
				{
					var SerialNo = searchResults[i].getValue('custrecord_serialnumber');
					var InternalID = searchResults[i].getId();

					var LoadSerialNumbers = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
					LoadSerialNumbers.setFieldValue('custrecord_serialstatus', '');

					var recid = nlapiSubmitRecord(LoadSerialNumbers, false, true);
					nlapiLogExecution('DEBUG', 'Serial Entry rec id',recid);
				}
			}
		}


	}
	return localSerialNoArray;
}

function getSerialNoCSV(arrayLP){
	var csv = "";
	nlapiLogExecution('DEBUG', 'arrayLP.length',arrayLP.length);
	if(arrayLP.length == 1)
	{
		csv = arrayLP[0][0];
	}
	else
	{
		for (var i = 0; i < arrayLP.length; i++) {
			if(i == arrayLP.length -1)
			{
				csv += arrayLP[i][0];
			}
			else
			{
				csv += arrayLP[i][0] + ",";
			}
		}
	}
	return csv;
}

function getPickFaceLocation(location)
{
	nlapiLogExecution('DEBUG', 'Into getPickFaceLocation',location);
	var Result='';
	var PickFaceSearchResults = new Array();
	var PickFaceFilters = new Array();
	var PickFaceColumns = new Array();
	if(location!=null && location!='' && location!='null')//sri Case# 20148123
		PickFaceFilters.push(new nlobjSearchFilter('custrecord_pickbinloc', null, 'anyof', location)); 
	PickFaceFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	PickFaceColumns[0] = new nlobjSearchColumn('custrecord_pickbinloc');
	PickFaceColumns[1] = new nlobjSearchColumn('custrecord_pickfacesku');

	PickFaceSearchResults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, PickFaceFilters, PickFaceColumns);

	nlapiLogExecution('DEBUG', 'Return Value',PickFaceSearchResults);
	nlapiLogExecution('DEBUG', 'Out of getPickFaceLocation');

	return PickFaceSearchResults;
}


function isMergeLP(methodid)
{
	var varMergeLP = 'F';
	var filtersputmethod = new Array();
	if(methodid!=null && methodid!='')
		filtersputmethod.push(new nlobjSearchFilter('internalid', null, 'anyof', methodid));
	filtersputmethod.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var colsputmethod = new Array();
	colsputmethod[0] = new nlobjSearchColumn('custrecord_mergelp');            

	var putwmethodsearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_method', null, filtersputmethod, colsputmethod);

	if(putwmethodsearchresults != null &&putwmethodsearchresults != ''&& putwmethodsearchresults.length > 0)
	{

		varMergeLP = putwmethodsearchresults[0].getValue('custrecord_mergelp');
	}

	return varMergeLP;
}
