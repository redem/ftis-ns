
function GetWaves()
{
	var filtersso = new Array();
	filtersso .push(new nlobjSearchFilter('custrecord_linestatus_flag', null,'anyof', ['9']))
	var columnso = new Array();
	columnso.push(new nlobjSearchColumn('custrecord_ebiz_wave').setSort('true'));
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null,filtersso,columnso);
	return searchresults;
}

function GetOrders()
{
	var filtersso = new Array();
	filtersso[0] = new nlobjSearchFilter('mainline', null, 'is', 'T');  // Pick task
	var columnso = new Array();
	columnso.push(new nlobjSearchColumn('tranid').setSort('true'));
	var searchresults = nlapiSearchRecord('salesorder', null, filtersso,columnso);
	return searchresults;
}

function LabelGeneration(request, response)
{
	if (request.getMethod() == 'GET') 
	{	
		nlapiLogExecution('ERROR', "Get", 'Get');
		var form = nlapiCreateForm('LabelGeneration');
		var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
		var vhdnBackValue = request.getParameter('custpage_hidback');
		nlapiLogExecution('ERROR', "AcvhdnBackValue",vhdnBackValue);
		form.addField('custpage_hidback','text','backbuttonvalue').setDisplayType('hidden').setDefaultValue('true');
		var WaveField = form.addField('custpage_wave', 'select', 'Wave #');
		WaveField.addSelectOption('','');	 
		WaveField.setLayoutType('normal', 'startcol')
		var selectOrder = form.addField('custpage_order', 'select', 'Order#');
		selectOrder.addSelectOption('','');
		selectOrder.setLayoutType('normal', 'startcol')
		var button = form.addSubmitButton('Display');
		var searchResultesWave= GetWaves();
		if (searchResultesWave != null && searchResultesWave != "" && searchResultesWave.length>0) {
			for (var i = 0; i < searchResultesWave.length; i++) {
				if(searchResultesWave[i].getValue('custrecord_ebiz_wave')!=null && searchResultesWave[i].getValue('custrecord_ebiz_wave')!="")
				{
					var res=  form.getField('custpage_wave').getSelectOptions(searchResultesWave[i].getValue('custrecord_ebiz_wave'), 'is');
					if (res != null) {				
						if (res.length > 0) {
							continue;
						}
					}
					WaveField.addSelectOption(searchResultesWave[i].getValue('custrecord_ebiz_wave'), searchResultesWave[i].getValue('custrecord_ebiz_wave'));
				}
			}
		}
		var searchResultesOrder= GetOrders();
		if (searchResultesOrder != null && searchResultesOrder != "" && searchResultesOrder.length>0) {
			for (var i = 0; i < searchResultesOrder.length; i++) {
				if(searchResultesOrder[i].getValue('tranid')!=null && searchResultesOrder[i].getValue('tranid')!="")
				{
					var res=  form.getField('custpage_order').getSelectOptions(searchResultesOrder[i].getValue('tranid'), 'is');
					if (res != null) {				
						if (res.length > 0) {
							continue;
						}
					}

					selectOrder.addSelectOption(searchResultesOrder[i].getValue('tranid'), searchResultesOrder[i].getValue('tranid'));
				}
			}
		}
		form.setScript('customscript_labelclientscript_cl');
		form.addButton('custpage_displaybtn','Back','Display()');
		response.writePage(form);
	}
	else
	{

		nlapiLogExecution('ERROR', "POST", 'POST');
		var form = nlapiCreateForm('LabelGeneration');

		form.setScript('customscript_labelclientscript_cl');
		var vhdnBackValue = request.getParameter('custpage_hidback');
		nlapiLogExecution('ERROR', "vhdnBackValue",vhdnBackValue);
		if((vhdnBackValue=='true'))
		{


			nlapiLogExecution('ERROR', "AcvhdnBackValue",vhdnBackValue);
			form.addField('custpage_hidback','text','backbuttonvalue').setDisplayType('hidden').setDefaultValue('true');
			var vQbWave = request.getParameter('custpage_wave');
			var vQbdo = request.getParameter('custpage_order');
			nlapiLogExecution('ERROR', "Line Wave", vQbWave);
			nlapiLogExecution('ERROR', "Line vQbdo", vQbdo);
			var WaveField = form.addField('custpage_wave', 'text', 'Wave').setDisplayType( "inline" );
			var labeltypeselection =  form.addField('custpage_labeltype','select','Label Type #');
			labeltypeselection.addSelectOption('','');
			labeltypeselection.addSelectOption('CARTON','CARTON');	
			labeltypeselection.addSelectOption('ADDRESS','ADDRESS');	
			var labeltype=request.getParameter('custpage_labeltype');
			var printtype =  form.addField('custpage_print','select','PrinterName #');
			printtype.addSelectOption('','');
			printtype.addSelectOption('ZRFID1','ZRFID1');
			printtype.addSelectOption('ZRFID2','ZRFID2');
			printtype.addSelectOption('ZRFID3','ZRFID3');
			printtype.addSelectOption('ZRFID4','ZRFID4');
			var  printername=request.getParameter('custpage_print');
			var selectOrder = form.addField('custpage_order', 'text', 'Order#').setDisplayType( "inline" );
			var labelgeneration =  form.addField('custpage_labelgeneration','text','# of Labels');
			var shiplabelcount=	request.getParameter('custpage_labelgeneration');
			selectOrder.setDefaultValue(vQbdo);
			WaveField.setDefaultValue(vQbWave);

			var sublist = form.addSubList("custpage_items", "inlineeditor","ItemList");
			sublist.addField("custpage_check", "checkbox", "Confirm").setDefaultValue('F');
			sublist.addField("custpage_fullfilorder", "text", "Fullfillment Order#");
			sublist.addField("custpage_soline", "text", "Line#");
			sublist.addField("custpage_customer", "text", "Customer");
			sublist.addField("custpage_item", "text", "Item");
			sublist.addField("custpage_status", "text", "Status");
			sublist.addField("custpage_packcode", "text", "Pack Code");
			sublist.addField("custpage_qty", "text", "Qty");
			sublist.addField("custpage_labelstatus", "text", "Print Status");
			sublist.addField("custpage_printdate", "text", "Print Date");
			sublist.addField("custpage_user", "text", "User");
			sublist.addField("custpage_order", "text", "OrderId#").setDisplayType('hidden'); 
			nlapiLogExecution('ERROR', "Line Wave", vQbWave);
			nlapiLogExecution('ERROR', "Line vQbdo", vQbdo);
			form.addSubmitButton('GenerateLabel');
			form.addButton('custpage_displaybtn','Back','Display()');

			var msg = form.addField('custpage_message','inlinehtml', null, null, null).setLayoutType('endrow');
			msg.setLayoutType('outsidebelow', 'startcol');
			var vordintid;
			if ((vQbdo != "")&&(vQbdo!=null))
			{
				var filtersso = new Array();
				filtersso[0] = new nlobjSearchFilter('mainline', null, 'is', 'T');
				filtersso[1] = new nlobjSearchFilter('tranid', null, 'is',vQbdo); 
				var columnso = new Array();
				columnso.push(new nlobjSearchColumn('Internalid'));
				var searchresults = nlapiSearchRecord('salesorder', null, filtersso,columnso);
				vordintid=searchresults[0].getValue('Internalid');
			}
			var filters = new Array();
			if ((vQbWave != "")&&(vQbWave!=null))
			{
				filters.push(new nlobjSearchFilter('custrecord_ebiz_wave', null,'equalto', vQbWave));			
			}

			if ((vordintid != "")&&(vordintid!=null))
			{
				filters.push(new nlobjSearchFilter('custrecord_ns_ord', null, 'is', vordintid));
			}
			filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null,'anyof', ['9']))
			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_do_customer');
			columns[1] = new nlobjSearchColumn('custrecord_ebiz_linesku');
			columns[2] = new nlobjSearchColumn('custrecord_linesku_status');
			columns[3] = new nlobjSearchColumn('custrecord_ord_qty');
			columns[4] = new nlobjSearchColumn('custrecord_linepackcode');
			columns[5] = new nlobjSearchColumn('custrecord_ordline');
			columns[6] = new nlobjSearchColumn('custrecord_ebiz_shipdate');
			columns[7] = new nlobjSearchColumn('custrecord_lineord');
			columns[8] = new nlobjSearchColumn('custrecord_ns_ord');
			columns[5].setSort(true);
			var searchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
			var lineno,item,ebizuser,itemstatus,itemqty,packcode,forder,company,salesorder,itemid;

			if(searchresults!=null)
			{
				for (var i = 0; searchresults != null && i < searchresults.length; i++) 
				{
					var searchresult = searchresults[i];
					line = searchresult.getValue('custrecord_ordline');
					item = searchresult.getText('custrecord_ebiz_linesku');
					itemstatus = searchresult.getText('custrecord_linesku_status');
					itemqty=searchresult.getValue('custrecord_ord_qty');
					packcode = searchresult.getValue('custrecord_linepackcode');
					ebizdate=searchresult.getValue('custrecord_ebiz_shipdate');
					company=searchresult.getText('custrecord_do_customer');
					forder=searchresult.getValue('custrecord_lineord');
					salesorder=searchresult.getValue('custrecord_ns_ord');
					itemid=searchresult.getValue('custrecord_ebiz_linesku');
					form.getSubList('custpage_items').setLineItemValue('custpage_check', i + 1, 'T');
					form.getSubList('custpage_items').setLineItemValue('custpage_soline', i + 1, line);
					form.getSubList('custpage_items').setLineItemValue('custpage_item', i + 1, item);
					form.getSubList('custpage_items').setLineItemValue('custpage_printdate', i + 1, ebizdate);
					form.getSubList('custpage_items').setLineItemValue('custpage_status', i + 1, itemstatus);
					form.getSubList('custpage_items').setLineItemValue('custpage_qty', i + 1, itemqty);
					form.getSubList('custpage_items').setLineItemValue('custpage_packcode', i + 1, packcode);
					form.getSubList('custpage_items').setLineItemValue('custpage_fullfilorder', i + 1, forder);
					form.getSubList('custpage_items').setLineItemValue('custpage_customer', i + 1, company);
					form.getSubList('custpage_items').setLineItemValue('custpage_order', i + 1, salesorder);

				}
			}

			response.writePage(form);
			var lineCnt = request.getLineItemCount('custpage_items');
			var ItemArray = new Array();
			var checkboxchecked,fulorder;
			for (var k = 1; k <= lineCnt; k++) 
			{
				checkboxchecked=request.getLineItemValue('custpage_items','custpage_check', k);
				fulorder = request.getLineItemValue('custpage_items','custpage_order', k);

				if(checkboxchecked=="T")
				{
					if(labeltype=="ADDRESS")
					{

						ItemArray.push(fulorder);
					}
					else if(labeltype=="CARTON")
					{

						ItemArray.push(fulorder);
					}

				}
			}
		
			var distinctArray = new Array();
			distinctArray=removeDuplicateElement(ItemArray);
			for(var i=0;i<distinctArray.length;i++)
			{
				var vOrderNo=distinctArray[i];
				nlapiLogExecution('ERROR', "labelgeneration", shiplabelcount);
				nlapiLogExecution('ERROR', "fulorder", vOrderNo);
				if(labeltype=="ADDRESS")
				{
					
					genarateShipLabelTemplate("",vOrderNo,"SHIPLABEL",shiplabelcount,printername); 
					msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Label Created Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '75%', null, null, null);</script></div>");
				}
				else if(labeltype=="CARTON")
				{
					try
					{
						
						GenerateChildLabel("",vOrderNo,"","","LABELCARTON",printername);
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Label Created Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '75%', null, null, null);</script></div>");
					}
					catch(exp)
					{
						nlapiLogExecution('ERROR','MaximumUsageExp',exp);
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', '"+exp+"', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '75%', null, null, null);</script></div>");
					}
				}
			}
		}
		else
		{	
			nlapiLogExecution('ERROR', "AfterElse",'AfterElse');
			nlapiLogExecution('ERROR', "AcvhdnBackValueinsideElse",vhdnBackValue);
			response.sendRedirect('SUITELET','customscript_ebiz_labelgenration_sl', 'customdeploy_ebiz_labelgenration_sl_dl',false,null);	

		} 


	}
}
function removeDuplicateElement(arrayName)
{
	var newArray=new Array();
	label:for(var i=0; i<arrayName.length;i++ )
	{  
		for(var j=0; j<newArray.length;j++ )
		{
			if(newArray[j]==arrayName[i]) 
				continue label;
		}
		newArray[newArray.length] = arrayName[i];
	}
	return newArray;
} 
function GenerateChildLabel(vContLpNo,vOrderNo,vwaveno,uompackflag,labeltype,printername)
{


	nlapiLogExecution('ERROR','Labelgen','done');
	 

	//get qty from opentask against to containerlp
	var nooflps=0;var uomId="",itemId="",itemText="";
	var qtyfilters = new Array();
	if(vContLpNo !=null && vContLpNo !=""){
		qtyfilters.push( new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vContLpNo));
	}
	if(vOrderNo !=null && vOrderNo !=""){
		qtyfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', [vOrderNo]));
	}
	if(vwaveno !=null && vwaveno !=""){
		qtyfilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', [vwaveno]));
	}
	qtyfilters.push( new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));               

	var qtycolumns = new Array();
	qtycolumns[0] = new nlobjSearchColumn('custrecord_container_lp_no', null, 'group');
	qtycolumns[1] = new nlobjSearchColumn('custrecord_sku', null, 'group');
	qtycolumns[2] = new nlobjSearchColumn('custrecord_uom_id', null, 'group');
	qtycolumns[3] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');	

	qtycolumns[1].setSort();

	var qtysearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, qtyfilters, qtycolumns);
	var oldsku="";
	if(qtysearchresults.length!=null)
	{
		for(var g=0;g<qtysearchresults.length;g++)
		{		
			var vContainerLpNo = qtysearchresults[g].getValue('custrecord_container_lp_no', null, 'group');
			var itemText = qtysearchresults[g].getText('custrecord_sku', null, 'group');
			var itemId = qtysearchresults[g].getValue('custrecord_sku', null, 'group');
			var itemName=qtysearchresults[g].getText('custrecord_sku', null, 'group');
			var uomId = qtysearchresults[g].getValue('custrecord_uom_id', null, 'group');
			var nooflps = qtysearchresults[g].getValue('custrecord_expe_qty', null, 'sum');
			nlapiLogExecution('ERROR','nooflps',nooflps);
			nlapiLogExecution('ERROR','vebizOrdNoId',vOrderNo);
			nlapiLogExecution('ERROR','uomId',uomId);
			nlapiLogExecution('ERROR','itemId',itemId);
			nlapiLogExecution('ERROR','itemName',itemName);
			nlapiLogExecution('ERROR','vContainerLpNo',vContainerLpNo);
			nlapiLogExecution('ERROR','qtysearchresults.length',qtysearchresults.length);

			 var sOrderDetailsArray=new Array();
			 sOrderDetailsArray=GetSalesOrderInformation(vOrderNo,itemId,labeltype);
			 var shiptocompanyid=sOrderDetailsArray["shiptocompanyid"];
			 nlapiLogExecution('ERROR','shiptocompanyid',shiptocompanyid);
			 var Label=GetLabelTemplate(shiptocompanyid);
			 genarateCartonLabelTemplate(vContainerLpNo,sOrderDetailsArray,itemName,labeltype,Label,nooflps,printername);
			
			}
		}
		
	}




function GetSalesOrderInformation(vebizOrdNo,skuno,labeltype)
{
	nlapiLogExecution('ERROR', 'GetSalesOrderInformation','GetSalesOrderInformation');
	var shiptoAddress1,shiptoAddress2,shiptocity,shiptostate,shiptocountry,shiptocompany,shiptozipcode;
	var shipfromaddress1,shipfromaddress2,shipfromcity,shipfromstate,shipfromcountry,shipfromzipcode,shipfromcompanyname;
	var customerpo;
	var location="";
	var bol="";
	var shipcarrier="";
	var companyid;
	var shiptocompanyid="" ,dpci="";
	var shiproute="",shipdate="",salesorder;
	var shipfromcompanyid;
	var tasktype="";
	var XREFNo="";
	var potype="";
	var edidept="";
	var shipfromaddress3="";
	var sOrderArray = new Array();
	var salesorderrecord = nlapiLoadRecord('salesorder', vebizOrdNo);
	shiptocity=salesorderrecord.getFieldValue('shipcity');
	sOrderArray["shiptocity"]=shiptocity;
	shiptostate=salesorderrecord.getFieldValue('shipstate');
	sOrderArray["shiptostate"]=shiptostate;
	shiptocountry=salesorderrecord.getFieldValue('shipcountry');
	sOrderArray["shiptocountry"]=shiptocountry;
	shiptocompany=salesorderrecord.getFieldText('entity');
	sOrderArray["shiptocompany"]=shiptocompany;
	shiptozipcode=salesorderrecord.getFieldValue('shipzip');
	nlapiLogExecution('ERROR', 'shiptozipcodegen', shiptozipcode);
	sOrderArray["shiptozipcode"]=shiptozipcode;
	shiptoAddress2=salesorderrecord.getFieldValue('shipaddr2');
	sOrderArray["shiptoAddress2"]=shiptoAddress2;
	shiptoAddress1=salesorderrecord.getFieldValue('shipaddr1');
	sOrderArray["shiptoAddress1"]=shiptoAddress1;
	customerpo=salesorderrecord.getFieldValue('otherrefnum');
	sOrderArray["customerpo"]=customerpo;
	location=salesorderrecord.getFieldValue('location');
	sOrderArray["location"]=location;
	shipcarrier=salesorderrecord.getFieldText('shipmethod');
	sOrderArray["shipcarrier"]=shipcarrier;
	shiptocompanyid=salesorderrecord.getFieldValue('entity');
	sOrderArray["shiptocompanyid"]=shiptocompanyid;
	shipfromcompanyid=salesorderrecord.getFieldValue('custbody_nswms_company');
	sOrderArray["shipfromcompanyid"]=shipfromcompanyid;
	shiproute=salesorderrecord.getFieldText('custbody_nswmssoroute');
	sOrderArray["shiproute"]=shiproute;
	shipdate=salesorderrecord.getFieldValue('trandate');
	sOrderArray["shipdate"]=shipdate;
	salesorder=salesorderrecord.getFieldValue('tranid');
	sOrderArray["salesorder"]=salesorder;
	if((location !="") && (location !=null))
	{
		//ShipFromAdress
		nlapiLogExecution('ERROR', 'location', location);
		var record = nlapiLoadRecord('location', location);
		shipfromaddress1=record.getFieldValue('addr1');
		sOrderArray["shipfromaddress1"]=shipfromaddress1;
		shipfromaddress2=record.getFieldValue('addr2');
		sOrderArray["shipfromaddress2"]=shipfromaddress2;
		shipfromcity=record.getFieldValue('city');
		sOrderArray["shipfromcity"]=shipfromcity;
		shipfromstate=record.getFieldValue('state');
		sOrderArray["shipfromstate"]=shipfromstate;
		shipfromzipcode =record.getFieldValue('zip');
		sOrderArray["shipfromzipcode"]=shipfromzipcode;
		shipfromcompanyname=record.getFieldValue('addressee');
		sOrderArray["shipfromcompanyname"]=shipfromcompanyname;
		shipfromcountry =record.getFieldValue('country');
		sOrderArray["shipfromcountry"]=shipfromcountry;
		//This code not in Dev.Code For Production Dynacraft
		//shipfromaddress3=record.getFieldValue('addr3');
		sOrderArray["shipfromaddress3"]=shipfromaddress3;
	}
	//This code not in Dev.Code For Production Dynacraft
	//potype=salesorderrecord.getFieldValue('custbody29');
	//sOrderArray["potype"]=potype;
	//edidept=salesorderrecord.getFieldValue('custbody_edi_ref_dp');
	//sOrderArray["edidept"]=edidept;
	//var totalLine=salesorderrecord .getLineItemCount('item');
	//nlapiLogExecution('ERROR', 'lineNum ', totalLine);
	//for(var i=1; i<= parseInt(totalLine); i++)
	//{
	//	var item =salesorderrecord.getLineItemValue('item','item',i);

	//	if(item==skuno)
	//	{
	//		dpci=salesorderrecord.getLineItemValue('item','custcol13',i);
	//	sOrderArray["dpci"]=dpci;
	//	}
	//nlapiLogExecution('ERROR', 'dpci ', dpci);
	//}
	//var filtersku = new Array();
	//filtersku.push(new nlobjSearchFilter('custrecord16', null,'is','E')); 
	//filtersku.push(new nlobjSearchFilter('custrecord17', null,'is',skuno)); 
	//var columnssku = new Array();
	//columnssku[0] = new nlobjSearchColumn('custrecord14'); 
	//var searchdesc= nlapiSearchRecord('customrecord265', null, filtersku,columnssku);
	//if(searchdesc!=null)
	//{
	//	XREFNo=searchdesc[0].getValue('custrecord14');
	//	sOrderArray["XREFNo"]=XREFNo;
	//	nlapiLogExecution('ERROR', 'XREFNo', XREFNo);
	//} 

	return sOrderArray;
}
function InsertMaxLpNo(internalid,lpMaxValue)
{

	// update the new max LP in the custom record
	nlapiSubmitField('customrecord_ebiznet_lp_range', internalid,'custrecord_ebiznet_lprange_lpmax', parseInt(lpMaxValue));
}
function GetMaxLPNoType(lpGenerationType, lpType,whsite)
{

	var maxLP = 1;
	var maxLPPrefix = "";
	var internalid="";
	var company="";
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpmax');
	columns[1] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix');
	columns[2] = new nlobjSearchColumn('custrecord_ebiznet_lprange_company');

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'is', lpGenerationType));
	filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lptype', null, 'anyof', [lpType]));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(whsite!=null && whsite!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_site', null, 'anyof', ['@NONE@', whsite]));

	var results = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, filters, columns);

	if(results != null)
	{
		if(results.length > 1)
		{
			alert('More records returned than expected');
			nlapiLogExecution('ERROR', 'GetMaxLPNo:LP Max Query returned more than 1 row');
		}
		else
		{
			// Incrementing the max LP number only if the search returns 1 row
			for (var t = 0; t < results.length; t++)
			{
				if (results[t].getValue('custrecord_ebiznet_lprange_lpmax') != null) 
				{
					internalid=results[t].getId();
					maxLP = results[t].getValue('custrecord_ebiznet_lprange_lpmax');
					maxLPPrefix = results[t].getValue('custrecord_ebiznet_lprange_lpprefix'); 
					company=results[t].getText('custrecord_ebiznet_lprange_company'); 
				}

			}
		}
		if((maxLP==null)||(maxLP==''))
		{
			maxLP=0;
		}
		maxLP = maxLP.toString();
		nlapiLogExecution('DEBUG', 'GetMaxLPNo:New MaxLP', maxLP);
		var maxArray = new Array();
		maxArray["maxLpPrefix"]=maxLPPrefix+parseInt(maxLP);
		nlapiLogExecution('DEBUG', 'GetMaxLPNo:maxLpPrefix',maxArray["maxLpPrefix"]);
		maxArray["maxInternalId"]=internalid;
		maxArray["maxCompany"]=company;
		return maxArray;

	}
}
function GetLabelTemplate(shiptocompanyid)
{
	var filtertempalte = new Array();
	labeltype="CARTONLABEL";
	nlapiLogExecution('ERROR', 'shiptocompanyid', shiptocompanyid);
	filtertempalte.push(new nlobjSearchFilter('custrecord_labeltemplate_name',null,'is',shiptocompanyid));
	filtertempalte.push(new nlobjSearchFilter('custrecord_labeltemplate_type',null,'is',labeltype));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_labeltemplate_data');
	var searchtemplate= nlapiSearchRecord('customrecord_label_templates',null,filtertempalte,columns);
	var Label="";
	if((searchtemplate !=null)&&(searchtemplate!=""))
	{
		nlapiLogExecution('ERROR', 'LabelTemplate','LabelTemplate');
		Label=searchtemplate[0].getValue('custrecord_labeltemplate_data');
	}	
	return Label;
}
function GenerateLabelCode(uompackflag,lpvalue,duns)
{

	var finaltext="";
	var duns="";
	var label="",uom="",ResultText="";
	nlapiLogExecution('ERROR', 'CreateMasterLPRecord uompackflag',uompackflag);
	nlapiLogExecution('ERROR', 'lpValue',lpvalue);
	//added by mahesh
	try 
	{	
		var lpMaxValue=lpvalue.toString();
		nlapiLogExecution('ERROR', 'lpMaxValueInnercode',parseInt(lpMaxValue.length));
		var prefixlength=parseInt(lpMaxValue.length);
		nlapiLogExecution('ERROR', 'prefixlength',prefixlength);
		if(prefixlength==0)
			label="000000000";
		else if(prefixlength==1)
			label="00000000"+lpMaxValue;
		else if(prefixlength==2)
			label="0000000"+lpMaxValue;
		else if(prefixlength==3)
			label="000000"+lpMaxValue;
		else if(prefixlength==4)
			label="00000"+lpMaxValue;
		else if(prefixlength==5)
			label="0000"+lpMaxValue;
		else if(prefixlength==6)
			label="000"+lpMaxValue;
		else if(prefixlength==7)
			label="00"+lpMaxValue;
		else if(prefixlength==8)
			label="0"+lpMaxValue;
		else if(prefixlength==9)
			label=lpMaxValue;

		//to get company id

		//var filters = new Array();
		//filters[0] = new nlobjSearchFilter('custrecord_ebiznet_lprange_lptype', null, 'is', 5);
		//filters[1] = new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'is', 1);

		//var columns = new Array();	
		//columns[0] = new nlobjSearchColumn('custrecord_ebiznet_lprange_company');

		//var searchresults = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, filters, columns);
		//nlapiLogExecution('ERROR', 'Lp Range Searchresults', searchresults.length);
		//if (searchresults !=null && searchresults !="") 
		//{
		//	var company = searchresults[0].getText('custrecord_ebiznet_lprange_company');	
		//}
		nlapiLogExecution('ERROR', 'label', label);


		if(uompackflag == "1" || uompackflag == "EACH" ) 
			uom="0"; 
		else if(uompackflag == "3" || uompackflag == "PALLET") 
			uom="2";
		else
			uom="0";
		nlapiLogExecution('ERROR', 'uom', uom);
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord finaltext', finaltext);
		finaltext=uom+duns+label;
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord finaltext', finaltext);



		//to get chk digit
		var checkStr=finaltext;
		var ARL=0;
		var BRL=0;
		var CheckDigitValue="";
		for (i = checkStr.length-1;  i > 0;  i--)
		{
			ARL = ARL+parseInt(checkStr.charAt(i));
			i--;
		}		
		ARL=ARL*3;
		for (i = checkStr.length-2;  i > 0;  i--)
		{
			BRL = BRL+parseInt(checkStr.charAt(i));
			i--;
		}		
		var sumOfARLBRL=ARL+BRL;
		var CheckDigit=0;

		while(CheckDigit<10)
		{
			if(sumOfARLBRL%10==0)
			{ 
				CheckDigitValue=CheckDigit; 
				break; 
			} 

			sumOfARLBRL++;
			CheckDigit++;
		}
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord CheckDigit', CheckDigitValue.toString());
		ResultText="00"+finaltext+CheckDigitValue.toString();
		nlapiLogExecution('ERROR', 'CreateMasterLPRecord ResultText', ResultText);


	} 
	catch (err) 
	{

	}
	return ResultText;
}
function GetCompanyDUNSnumber(company)
{
	var dunsfilters = new Array();
	dunsfilters[0] = new nlobjSearchFilter('custrecord_company', null, 'is', company);
	var dunscolumns = new Array();
	dunscolumns[0] = new nlobjSearchColumn('custrecord_compduns');			 

	var dunssearchresults = nlapiSearchRecord('customrecord_ebiznet_company', null, dunsfilters, dunscolumns);
	if (dunssearchresults !=null && dunssearchresults !="") 
	{
		duns = dunssearchresults[0].getValue('custrecord_compduns');
		nlapiLogExecution('ERROR', 'duns', duns);
	}
	return duns;
}
function removeDuplicateElement(arrayName)
{
	var newArray=new Array();
	label:for(var i=0; i<arrayName.length;i++ )
	{  
		for(var j=0; j<newArray.length;j++ )
		{
			if(newArray[j]==arrayName[i]) 
				continue label;
		}
		newArray[newArray.length] = arrayName[i];
	}
	return newArray;
}
