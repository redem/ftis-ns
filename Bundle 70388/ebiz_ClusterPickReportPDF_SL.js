/***************************************************************************
 eBizNET SOLUTIONS
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_ClusterPickReportPDF_SL.js,v $
 *     	   $Revision: 1.2.2.7.4.2.4.2 $
 *     	   $Date: 2013/03/21 14:18:14 $
 *     	   $Author: snimmakayala $
 *     	   $Name: t_NSWMS_2013_1_3_10 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_ClusterPickReportPDF_SL.js,v $
 * Revision 1.2.2.7.4.2.4.2  2013/03/21 14:18:14  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.2.2.7.4.2.4.1  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.2.2.7.4.2  2012/11/01 14:55:02  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.2.2.7.4.1  2012/10/07 23:06:03  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue Fixes for FISK,BOOMBAH and TDG.
 *
 * Revision 1.2.2.7  2012/08/29 01:25:08  gkalla
 * CASE201112/CR201113/LOG201121
 * To show total weight
 *
 * Revision 1.2.2.6  2012/08/29 01:00:02  gkalla
 * CASE201112/CR201113/LOG201121
 * To show total weight
 *
 * Revision 1.2.2.5  2012/08/26 15:36:00  spendyala
 * CASE201112/CR201113/LOG201121
 * cluster picks are not generating properly issue fixed
 *
 * Revision 1.2.2.4  2012/08/10 17:47:52  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.2.2.3  2012/04/20 13:57:06  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.2.2.2  2012/02/24 20:22:56  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * putaway confirmation
 *
 * Revision 1.2  2012/02/24 16:52:10  mbpragada
 * CASE201112/CR201113/LOG201121
 * Cluster pick report new files
 *
 * Revision 1.1  2012/02/24 16:44:30  mbpragada
 * CASE201112/CR201113/LOG201121
 * Cluster pick report new files
 *

 *****************************************************************************/
/**
 * This is the main function to print the PICK LIST against the given wave number  
 */
function ClusterPickReportPDF(request, response){
	if (request.getMethod() == 'GET') {

		var form = nlapiCreateForm('Pick Report');
		var vQbWave,vQbfullfillmentNo ="",vqbcluster;
		var getwaveNo=request.getParameter('custparam_ebiz_wave_no');
		var getFullfillmentNo=request.getParameter('custparam_ebiz_fullfilmentno');
		var getcluster = request.getParameter('custparam_ebiz_clusterno');
		//var getButtonId=request.getParameter('id');
		//SetPrintFlag(getwaveNo,getFullfillmentNo);
		var filters = new Array();
//		9-STATUS.OUTBOUND.PICK_GENERATED ,26-STATUS.OUTBOUND.FAILED
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','26']));
		//filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9']));
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));

		if(request.getParameter('custparam_ebiz_wave_no')!=null && request.getParameter('custparam_ebiz_wave_no')!="")
		{
			vQbWave = request.getParameter('custparam_ebiz_wave_no');
			nlapiLogExecution('ERROR','vQbWave',vQbWave);
			filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(vQbWave)));
		} 


		if(request.getParameter('custparam_ebiz_fullfilmentno')!= null && request.getParameter('custparam_ebiz_fullfilmentno')!= "")
		{
			vQbfullfillmentNo = request.getParameter('custparam_ebiz_fullfilmentno');
			nlapiLogExecution('ERROR','vQbfullfillmentNo',vQbfullfillmentNo);
			filters.push(new nlobjSearchFilter('name', null, 'is', vQbfullfillmentNo));
		}

		if(request.getParameter('custparam_ebiz_clusterno')!= null && request.getParameter('custparam_ebiz_clusterno')!= "")
		{
			vqbcluster = request.getParameter('custparam_ebiz_clusterno');
			nlapiLogExecution('ERROR','vqbcluster',vqbcluster);
			filters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vqbcluster));
		}

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_line_no');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[3] = new nlobjSearchColumn('custrecord_tasktype');
		columns[4] = new nlobjSearchColumn('custrecord_lpno');
		columns[5] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[6] = new nlobjSearchColumn('custrecord_sku');
		columns[7] = new nlobjSearchColumn('custrecord_sku_status');
		columns[8] = new nlobjSearchColumn('custrecord_packcode');
		columns[9] = new nlobjSearchColumn('name');
		columns[10] = new nlobjSearchColumn('custrecord_ebiz_clus_no');
		columns[11] = new nlobjSearchColumn('custrecord_container_lp_no');
		columns[12] = new nlobjSearchColumn('custrecord_container');
		columns[13] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
		columns[14] = new nlobjSearchColumn('description','custrecord_sku');
		columns[15] = new nlobjSearchColumn('upccode','custrecord_sku');
		columns[16] = new nlobjSearchColumn('custrecord_ebiz_order_no').setSort('true');
		columns[17] = new nlobjSearchColumn('custrecord_batch_no');
		columns[18] = new nlobjSearchColumn('custrecord_total_weight');
		columns[19] = new nlobjSearchColumn('custrecord_ebizwmscarrier');


		columns[10].setSort(false);
		columns[13].setSort(false);

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
		if(searchresults!=null&&searchresults!=''&&searchresults.length>0)
		{
			nlapiLogExecution('ERROR','searchresults',searchresults.length);
			var url;
			/*var ctx = nlapiGetContext();
			nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
			if (ctx.getEnvironment() == 'PRODUCTION') 
			{
				url = 'https://system.netsuite.com';			
			}
			else if (ctx.getEnvironment() == 'SANDBOX') 
			{
				url = 'https://system.sandbox.netsuite.com';			
		}*/	
			nlapiLogExecution('ERROR', 'PDF URL',url);	


			var filefound = nlapiLoadFile('Images/LOGOCOMP.jpg'); 
			if (filefound) 
			{ 
				nlapiLogExecution('ERROR', 'Event', 'file;'+filefound.getId()); 
				var imageurl = filefound.getURL();
				nlapiLogExecution('ERROR','imageurl',imageurl);
				//var finalimageurl = url + imageurl;//+';';
				var finalimageurl = imageurl;//+';';
				//finalimageurl=finalimageurl+ '&expurl=T;';
				nlapiLogExecution('ERROR','imageurl',finalimageurl);
				finalimageurl=finalimageurl.replace(/&/g,"&amp;");

			} 
			else 
			{
				nlapiLogExecution('ERROR', 'Event', 'No file;');
			}
			var totalwt=0;

			var SoIds=new Array();
			var ContLPIds=new Array();
			for ( var intg = 0; intg < searchresults.length; intg++)
			{
				SoIds[intg]=searchresults[intg].getValue('custrecord_ebiz_order_no');
				ContLPIds[intg]=searchresults[intg].getValue('custrecord_container_lp_no');
			}


			var distinctLPs = removeDuplicateElementFromContLP(SoIds,ContLPIds);

			nlapiLogExecution('ERROR','distinctLPs',distinctLPs);
			nlapiLogExecution('ERROR','SOIDS',SoIds);

			var vline, vitem, vqty, vTaskType,vlotbatch, vmainline, vrecid, vlocation, vLpno, vSKU, vlocationid, vinvrefno, vskustatus, vpackcode, vdono,vcontlp,vcontsize,vclusno,vweight;
			var upccode="";
			var itemdesc="";
//			var replaceChar =/[^a-zA-Z 0-9 ()]+/g;
			var replaceChar =/\$|,|@|#|~|`|\%|\*|\^|\&|\+|\=|\-|\_|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|\./g;

			//date
			var sysdate=DateStamp();
			var systime=TimeStamp();
			var Timez=calcTime('-5.00');
			nlapiLogExecution('ERROR','TimeZone',Timez);
			var datetime= new Date();
			datetime=datetime.toLocaleTimeString() ;
			for ( var count = 0; count < distinctLPs.length; count++)
			{
				var distinctSoIds =distinctLPs[count];
				var totalwt=0;var pageno=0;
				nlapiLogExecution('ERROR','distinctSoContLP[1]',distinctSoIds);
				if(distinctSoIds != null && distinctSoIds != '')
				{	
					//var trantype = nlapiLookupField('transaction', SoIds, 'recordType');
					var trantype = nlapiLookupField('transaction', distinctSoIds[0], 'recordType');
					var salesorder = nlapiLoadRecord(trantype, distinctSoIds[0]);
					var	address = salesorder.getFieldValue('shipaddressee');
					if(address != null && address !="")
						address=address.replace(replaceChar,'');
					else
						address="";

					var	HNo = salesorder.getFieldValue('shipaddr1');
					if(HNo != null && HNo !="")
						HNo=HNo.replace(replaceChar,'');
					else
						HNo="";
					var	city = salesorder.getFieldValue('shipcity');
					if(city != null && city !="")
						city=city.replace(replaceChar,'');
					else
						city="";
					var	state = salesorder.getFieldValue('shipstate');
					if(state != null && state !="")
						state=state.replace(replaceChar,'');
					else
						state="";
					var	country = salesorder.getFieldValue('shipcountry');
					if(country != null && country !="")
						country=country.replace(replaceChar,'');
					else
						country="";
					var	zipcode = salesorder.getFieldValue('shipzip');
					var	carrier = salesorder.getFieldText('shipmethod');
					var SalesorderNo= salesorder.getFieldValue('tranid');
					var ismultishipto= salesorder.getFieldValue('ismultishipto');

					var tempInstructions=salesorder.getFieldValue('custbody_nswmspoinstructions');
					var Instructions="";
					if(tempInstructions!=null)
					{
						Instructions=tempInstructions;
					}
					if(Instructions != null && Instructions !="")
						Instructions=Instructions.replace(replaceChar,'');
					else
						Instructions="";

					nlapiLogExecution('ERROR','ADDRESS',address+','+HNo+','+carrier);
					nlapiLogExecution('ERROR','COUNT',count);

					if(address!=null && address!='')
						address=address.replace('&','&amp;');

					nlapiLogExecution('ERROR','address',address);

					//calculate total wt for particular so#
					for (var x = 0; x < searchresults.length; x++)
					{
						var searchresult = searchresults[x];
						vdono = searchresult.getValue('name');
						if(vdono.split('.')[0] == SalesorderNo)
						{

							if( searchresults[x].getValue('custrecord_total_weight')!=null && searchresults[x].getValue('custrecord_total_weight')!="")
								totalwt=parseFloat(totalwt)+parseFloat(searchresults[x].getValue('custrecord_total_weight'));
						}
					}


					//nlapiLogExecution('ERROR','searchresults',searchresults.length);
					var tempLineNo;var pagetotalno=0;
					var vLineXML='';
					if (searchresults != null) {

						for (var i = 0; i < searchresults.length; i++) {
							var searchresult = searchresults[i];

							vline = searchresult.getValue('custrecord_line_no');
							vlotbatch = searchresult.getValue('custrecord_batch_no');
							vitem = searchresult.getValue('custrecord_ebiz_sku_no');
							vqty = searchresult.getValue('custrecord_expe_qty');
							vTaskType = searchresult.getText('custrecord_tasktype');
							vLpno = searchresult.getValue('custrecord_lpno');
							vlocationid = searchresult.getValue('custrecord_actbeginloc');
							vlocation = searchresult.getText('custrecord_actbeginloc');
							vSKU = searchresult.getText('custrecord_sku');
							vskustatus = searchresult.getText('custrecord_sku_status');
							vpackcode = searchresult.getValue('custrecord_packcode');
							vdono = searchresult.getValue('name');
							vcontlp=searchresult.getValue('custrecord_container_lp_no');
							vcontsize=searchresult.getText('custrecord_container');
							//vclusno=searchresult.getValue('custrecord_ebiz_clus_no');
							upccode=searchresult.getValue('upccode','custrecord_sku');
							itemdesc=searchresult.getValue('description','custrecord_sku');

							vweight=searchresult.getValue('custrecord_total_weight');
							var vWMSCarrier=searchresult.getText('custrecord_ebizwmscarrier');


							nlapiLogExecution('ERROR','SoIds[count]',SalesorderNo +','+vdono.split('.')[0]);
							nlapiLogExecution('ERROR','SalesorderNo',SalesorderNo);
							nlapiLogExecution('ERROR','vdono',vdono.split('.')[0]);
							nlapiLogExecution('ERROR','tempLineNo',tempLineNo);
							nlapiLogExecution('ERROR','vline',vline);
							nlapiLogExecution('ERROR','vcontlp',vcontlp);
							nlapiLogExecution('ERROR','Main ContLP',distinctSoIds[1]);
							//if(vdono.split('.')[0] == SalesorderNo)
							if(distinctSoIds[1] == vcontlp)
							{	
								vclusno=searchresult.getValue('custrecord_ebiz_clus_no');
								nlapiLogExecution('ERROR','vclusno',vclusno);
								if(tempLineNo==null)
								{
									tempLineNo=vline;
								}

								if(vline!=tempLineNo)
								{
									var lineInstructions=salesorder.getLineItemValue('item','custcol2',tempLineNo);
									if(lineInstructions!=null && lineInstructions!="")
									{
										nlapiLogExecution('ERROR','itemInstructions',lineInstructions);
										nlapiLogExecution('ERROR','itemInstructions',lineInstructions);
										vLineXML +=  "<tr><td width='100%' colspan='15' style='border-width: 1px; border-color: #000000'>";
										vLineXML +="<span style='font-size:9'> Line Instructions :</span>"+ "<p>"+lineInstructions+"</p>";
										vLineXML =vLineXML+  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
									}
								}

								vLineXML =vLineXML+  "<tr>";

								vLineXML += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
								if(vdono != null && vdono != "")
								{
									vLineXML += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
									vLineXML += vdono;
									vLineXML += "\"/>";
								}
								vLineXML += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

								vLineXML += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
								vLineXML += vline;
								vLineXML += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

								vLineXML += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
								if(vSKU != null && vSKU != "")
								{
									vLineXML += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
									vLineXML += vSKU;
									vLineXML += "\"/>";
								}
								vLineXML += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

								vLineXML += "<td width='8%' style='border-width: 1px; border-color: #000000'>";
								vLineXML += upccode;
								vLineXML += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

								vLineXML += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
								if(itemdesc != null || itemdesc != "")
									vLineXML += itemdesc.replace(replaceChar,'');
								vLineXML += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

								vLineXML += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
								vLineXML += vskustatus;
								vLineXML += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

								vLineXML += "<td width='7%' style='border-width: 1px; border-color: #000000'>";//uncommented for value
								vLineXML += vpackcode;
								vLineXML += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";	

								vLineXML += "<td width='3%' align='right' style='border-width: 1px; border-color: #000000'>";			
								vLineXML += vqty;
								vLineXML += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

								vLineXML += "<td width='5%' style='border-width: 1px; border-color: #000000'>";
								vLineXML += vlocation;
								vLineXML += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

								vLineXML += "<td width='6%' style='border-width: 1px; border-color: #000000'>";//uncommented for value
								vLineXML += vLpno;
								vLineXML += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

								vLineXML += "<td width='6%' style='border-width: 1px; border-color: #000000'>";
								vLineXML += vlotbatch;
								vLineXML += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

								vLineXML += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented for value
								vLineXML += vcontlp;
								vLineXML += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

								vLineXML += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented for value
								vLineXML += vcontsize;
								vLineXML += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

								vLineXML += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented for value
								if(vclusno != null && vclusno != "")
								{
									vLineXML += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
									vLineXML += vclusno;
									vLineXML += "\"/>";
								}
								vLineXML += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

								vLineXML += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
								vLineXML += vweight;
								vLineXML =vLineXML+  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";

								nlapiLogExecution('ERROR','vline',vline);
								nlapiLogExecution('ERROR','tempLineNo',tempLineNo);								  
								tempLineNo=vline;
								pagetotalno=parseFloat(pagetotalno)+1;
								// }
								// }
							}
						}
					}

					var lineInstructions=salesorder.getLineItemValue('item','custcol2',tempLineNo);
					nlapiLogExecution('ERROR','tempLineNotest1',tempLineNo);
					nlapiLogExecution('ERROR','lineInstructions',lineInstructions);
					if(lineInstructions!=null && lineInstructions!="")
					{
						lineInstructions=lineInstructions.replace(replaceChar,'');

						nlapiLogExecution('ERROR','tempLineNotest',tempLineNo);		
						nlapiLogExecution('ERROR','itemInstructions',lineInstructions);
						nlapiLogExecution('ERROR','itemInstructions',lineInstructions);
						vLineXML +=  "<tr><td width='100%' colspan='15' style='border-width: 1px; border-color: #000000'>";
						vLineXML +="<span style='font-size:9'> Line Instructions :</span>"+ "<p>"+lineInstructions+"</p>";
						vLineXML =vLineXML+  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
						tempLineNo=null;
					}
					vLineXML =vLineXML+"</table>";

					var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<head><macrolist><macro id='myfooter'><p align='center'>Page <pagenumber/> of <totalpages/></p></macro></macrolist></head>" ;
					xml += "<body  font-size=\"7\"  size=\"A4-landscape\"  padding-right=\"5mm\"  padding-left=\"5mm\"  padding-top=\" 0mm\"   footer='myfooter' footer-height='20mm'>\n";

					if(count==0)
						var strxml = "<table width='100%' >";
					else
					{
						var strxml=strxml+"";
						strxml += "<table width='100%' >";
					}
					if(pageno==0)
					{
						pageno=parseFloat(pageno+1);
					}


					strxml += "<tr ><td valign='middle' align='left'><img src='" + finalimageurl + "'></img></td><td valign='middle' align='left'  style='font-size:xx-large;'>";
					strxml += "Pick Report ";
					strxml += "</td><td align='right'>&nbsp;</td></tr></table>";
					strxml += "<p align='right'>Date/Time:"+Timez+"</p>";
					strxml +="<table style='width:100%;'>";
					strxml +="<tr><td valign='top'>";
					strxml +="<table align='left' style='width:70%;' border='1'>";
					strxml +="<tr><td align='left' style='width:51px'>Wave# :</td>";

					strxml +="<td>";
					if(vQbWave != null && vQbWave!= "")
					{
						strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
						strxml += vQbWave;
						strxml += "\"/>";
					}
					strxml += "</td></tr>";

					strxml +="<tr><td align='left' style='width:51px'>Order# :</td>";

					//strxml +="<td>"+SalesorderNo+"</td></tr>";
					strxml +="<td>";
					if(SalesorderNo != null && SalesorderNo != "")
					{
						strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
						strxml += SalesorderNo;
						strxml += "\"/>";
					}
					strxml += "</td></tr>";

					strxml +="<tr><td align='left' style='width:51px'>Carrier:</td>";
					strxml +="<td>"+carrier+"</td></tr>";
					//strxml +="</table><table><tr><td>&nbsp;</td></tr></table></td>";
					strxml +="</table></td>";
					strxml +="<td>";
					strxml +="<table align='right' style='width:90%;' border='1'>";
					strxml +="<tr><td align='left' style='width:51px'>Container LP# :</td></tr>";
					strxml +="<tr><td align='left' style='width:51px'>";
					if(distinctSoIds[1] != null && distinctSoIds[1] != "")
					{
						strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
						strxml += distinctSoIds[1];
						strxml += "\"/>";
					}
					strxml +="</td></tr>"; 
					strxml +="</table>";
					strxml +="</td>";

					strxml +="<td>";
					strxml +="<table align='right' style='width:90%;' border='1'>";
					strxml +="<tr><td align='left' style='width:51px'>Cluster# :</td></tr>"; 
					strxml +="<tr><td align='left' style='width:51px'>";
					if(vclusno != null && vclusno != "")
					{
						strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
						strxml += vclusno;
						strxml += "\"/>";
					}
					strxml +="</td></tr>";
					strxml +="</table>";
					strxml +="</td>";

					strxml +="<td>";
					strxml +="<table align='right' style='width:80%;' border='1'>";
					strxml +="<tr><td align='center' colspan='2'><b>Ship To</b></td></tr>";

					if(ismultishipto != 'T'){

						strxml +="<tr><td align='right' style='width:51px'>Address:</td>";
						strxml +="<td>"+address+"</td></tr>";
						strxml +="<tr><td style='width:51px'>&nbsp;</td>";
						strxml +="<td>"+HNo+"</td></tr>";
						strxml +="<tr><td align='right' style='width:51px'>City:</td>";
						strxml +="<td>"+city+"</td></tr>";
						strxml +="<tr><td align='right' style='width:51px'>State:</td>";
						strxml +="<td>"+state+"</td></tr>";
						strxml +="<tr><td align='right' style='width:51px'>Zip:</td>";
						strxml +="<td>"+zipcode+"</td></tr>";
						strxml +="<tr><td align='right' style='width:51px'>Country:</td>";
						strxml +="<td>"+country+"</td></tr>";	
					}
					else
					{
						strxml +="<tr height='60'><td style='padding-left:80px' valign='middle' align='center'> ** Multiple Addresses **</td></tr>";
					}

					strxml +="</table>";
					strxml +=" <p>&nbsp;</p>";
					strxml +="</td></tr></table>";
					if(Instructions!="")
					{
						strxml +=" <p>&nbsp;</p>";
						strxml +="<table  width='100%' style='width:100%;' >";
						strxml +="<tr style=\"font-weight:bold\"><td width='100%' style='border-width: 0.5px; border-color: #000000'>";
						strxml +="<span style='font-size:9'>Order Instructions: <p> "+Instructions+"</p></span>";
						strxml +="</td></tr></table>";
						strxml +=" <p>&nbsp;</p>";
					}
					strxml +="<table  width='100%'>";
					strxml +="<tr style=\"font-weight:bold;background-color:gray;color:white;\">";

					strxml += "<td width='16%' style='border-width: 1px; border-color: #000000'>";
					strxml += " Fulfillment#";
					strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

					strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
					strxml += "Line #";
					strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

					strxml += "<td width='16%' style='border-width: 1px; border-color: #000000'>";
					strxml += " Part#/Item";
					strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

					strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
					strxml += "UPC Code";
					strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

					strxml += "<td width='15%' style='border-width: 1px; border-color: #000000'>";
					strxml += "Item Description";
					strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

					strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
					strxml += "Status";
					strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

					strxml += "<td width='8%' style='border-width: 1px; border-color: #000000'>";//uncommented
					strxml += "Pack Code";
					strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";	

					strxml += "<td width='3%' style='border-width: 1px; border-color: #000000'>";			
					strxml += "Qty";
					strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

					strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
					strxml += "Bin Location";
					strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

					strxml += "<td width='6%' style='border-width: 1px; border-color: #000000'>";//uncommented
					strxml += "LP #";
					strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

					strxml += "<td width='6%' style='border-width: 1px; border-color: #000000'>";
					strxml += "Lot#";
					strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

					strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented
					strxml += "Container LP";
					strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

					strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented
					strxml += "Container Size";
					strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

					strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented
					strxml += "Cluster #";
					strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

					strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
					strxml += "Weight(lbs)";
					strxml =strxml+  "</td></tr>";


					if(vLineXML != null && vLineXML != '')
						strxml = strxml + vLineXML;

					if((distinctLPs.length-count)>1)
					{
						pageno=parseFloat(pageno)+1;


						//strxml=strxml+ "<p id='myfooter'>Page No: "+pageno+" </p>";
						strxml=strxml+ "<p style='page-break-after:always'></p>";
					}
					else
					{
						//pageno=parseFloat(pageno)+1;
						//strxml=strxml+ "<p style='vertical-align:bottom;align:right;font-size:9'>Page No: "+pageno+" </p>";
					}
				}
			}
			strxml =strxml+ "\n</body>\n</pdf>";		
			//	nlapiLogExecution('ERROR', 'strxml', strxml);
			xml=xml +strxml;
			//	nlapiLogExecution('ERROR','XML',xml);


			var file = nlapiXMLToPDF(xml);	
			response.setContentType('PDF','ClusterPickReport.pdf');
			response.write( file.getValue() );
		}

	}
	else //this is the POST block
	{

	}
}


/**
 * Remove duplicates from an array
 * @param SOIds
 * @param ContLPIds
 * @returns {Array}
 */
function removeDuplicateElementFromContLP(SOIds,ContLPIds){

	var MainArray = new Array();
	label:for (var i = 0; i < ContLPIds.length; i++) {
		var newArray = new Array();
		for (var j = 0; j < MainArray.length; j++) {
			if (MainArray[j][1] == ContLPIds[i]) 
				continue label;
		}
		newArray[0] = SOIds[i];
		newArray[1] = ContLPIds[i];
		MainArray.push(newArray);
	}
	return MainArray;
}

function SetPrintFlag(waveno,fullfillment)
{
	nlapiLogExecution('ERROR','GETwaveno',waveno);
	nlapiLogExecution('ERROR','GETfullfillment',fullfillment);
	var filter=new Array();

	if(waveno!=null && waveno!="")
		filter.push(new nlobjSearchFilter('custrecord_ebiz_wave','null','equalto',parseFloat(waveno)));

	if(fullfillment!=null&&fullfillment!="")
		filter.push(new nlobjSearchFilter('custrecord_lineord','null','is',fullfillment.toString()));

	var column=new Array();
	column[0]=new nlobjSearchColumn('custrecord_printflag');
	column[1]=new nlobjSearchColumn('custrecord_print_count');

	var searchrecord=nlapiSearchRecord('customrecord_ebiznet_ordline',null,filter,column);
	if(searchrecord!=null)
	{
		for ( var count = 0; count < searchrecord.length; count++) {
			var intId=searchrecord[count].getId();
			var flagcount=searchrecord[count].getValue('custrecord_print_count');
			var Newflagcount;
			if(flagcount==null || flagcount=="")
				Newflagcount=1;
			else
				Newflagcount=parseFloat(flagcount)+1;

			nlapiSubmitField('customrecord_ebiznet_ordline', intId, 'custrecord_printflag','T');
			nlapiSubmitField('customrecord_ebiznet_ordline', intId, 'custrecord_print_count',Newflagcount);
			nlapiSubmitField('customrecord_ebiznet_ordline', intId, 'custrecord_ebiz_pr_dateprinted',DateStamp());
		}
	}

}
function calcTime(offset) {

	//var vtime=new Date();

	//nlapiLogExecution('ERROR', 'TimeOffset', vtime.getTimezoneOffset());

	// create Date object for current location
	d = new Date();

	// convert to msec
	// add local time zone offset 
	// get UTC time in msec
	utc = d.getTime() + (d.getTimezoneOffset() * 60000);

	// create new Date object for different city
	// using supplied offset
	nd = new Date(utc + (3600000*offset));

	// return time as a string
	//return "The local time in " + city + " is " + nd.toLocaleString();

	var stringDt=((parseFloat(nd.getMonth()) + 1) + '/' + (parseFloat(nd.getDate())) + '/' + nd.getFullYear());

	var timestamp;
	var a_p = "";

	//Getting time in hh:mm tt format.
	var curr_hour = nd.getHours();
	var curr_min = nd.getMinutes();

	// determining the am/pm indicator
	if (curr_hour < 12)
		a_p = "am";
	else
		a_p = "pm";

	// finalizing hours depending on 24hr clock
	if (curr_hour == 0)
		curr_hour = 12;
	else if(curr_hour > 12)
		curr_hour -= 12;

	if (curr_min.length == 1)
		curr_min = "0" + curr_min;

	//Adding fields to update time zones.
	timestamp = curr_hour + ":" + curr_min + " " + a_p;

	return stringDt + " " + timestamp;


}


function DateStamp(){
	var now = new Date();
	return ((parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
}