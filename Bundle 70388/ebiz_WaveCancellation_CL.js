/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Client/ebiz_WaveCancellation_CL.js,v $
 *     	   $Revision: 1.1.14.1.8.1 $
 *     	   $Date: 2015/11/12 16:30:54 $
 *     	   $Author: aanchal $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_121 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_WaveCancellation_CL.js,v $
 * Revision 1.1.14.1.8.1  2015/11/12 16:30:54  aanchal
 * 2015.2 Issue fix
 * 201414544
 *
 * Revision 1.1.14.1  2013/02/26 12:56:03  skreddy
 * CASE201112/CR201113/LOG201121
 * merged boombah changes
 *
 * Revision 1.1  2011/12/08 05:49:56  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Wave Cacellation
 *
 * 
 *
 *
 *****************************************************************************/


function WaveCancellation_CL()
{

	try 
	{
		var criteria = nlapiGetFieldValue('custpage_selectcriteria');
		var waveno = nlapiGetFieldValue('custpage_wavelist');
		var ordno = nlapiGetFieldValue('custpage_orderlist');
		var ordlineno = nlapiGetFieldValue('custpage_orderlinelist');
		// code added from Boombah account on 25Feb13 by santosh
		// validation for cluster#
		var cluster = nlapiGetFieldValue('custpage_cluster');
		//up to here


		if (criteria == 'WAVE' && waveno=='') 
		{	alert('Please Select Wave #');
		return false;
		}	
		if (criteria == 'HEADER_LEVEL' && ordno=='') 
		{	alert('Please Select Order #');
		return false;
		}
		if (criteria == 'DETAIL_LEVEL' && ordlineno=='') 
		{	alert('Please Select Fullfilment order #');
		return false;
		}
		// code added from Boombah account on 25Feb13 by santosh
		if (criteria == 'CLUSTER' && cluster=='') 
		{	alert('Please Select Cluster #');
		return false;
		}
		
			
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is',waveno));
		if(ordlineno!=null&&ordlineno!='')
		filters.push(new nlobjSearchFilter('name', null, 'is', ordlineno));//fo
		if(cluster!=null&&cluster!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', cluster));//cluster
		if(ordno!=null&&ordno!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', ordno));//order#
		
		
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, null);
		
		if(searchresults==null||searchresults==''||searchresults=='null')
		{
			alert('No Records found!!!!');
			return false;
		}
		
		//up to here


	} 
	catch (exps) 
	{
		alert('Error' + exps);
		return false;
	}

	return true;
}