/***************************************************************************
���eBizNET Solutions
 ****************************************************************************
 *
 *� $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_WaveCancellation_SL.js,v $
 *� $Revision: 1.3.2.10.4.6.2.28.2.2 $
 *� $Date: 2015/12/02 15:33:52 $
 *� $Author: deepshikha $
 *� $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_WaveCancellation_SL.js,v $
 *� Revision 1.3.2.10.4.6.2.28.2.2  2015/12/02 15:33:52  deepshikha
 *� 2015.2 issues
 *� 201415914
 *�
 *� Revision 1.3.2.10.4.6.2.28.2.1  2015/09/23 14:58:06  deepshikha
 *� 2015.2 issueFix
 *� 201414466
 *�
 *� Revision 1.3.2.10.4.6.2.28  2015/07/15 15:15:40  rrpulicherla
 *� Case#201413343
 *�
 *� Revision 1.3.2.10.4.6.2.27  2015/04/21 10:01:41  rrpulicherla
 *� Case#201412277
 *�
 *� Revision 1.3.2.10.4.6.2.26  2015/04/13 09:25:59  rrpulicherla
 *� Case#201412277
 *�
 *� Revision 1.3.2.10.4.6.2.25  2015/04/07 08:01:44  snimmakayala
 *� 201412229
 *�
 *� Revision 1.3.2.10.4.6.2.24  2015/03/31 07:48:51  skreddy
 *� Case# 201412193
 *� True fabProd  issue fix
 *�
 *� Revision 1.3.2.10.4.6.2.23  2015/03/19 15:27:38  rrpulicherla
 *� Case#201411317
 *�
 *� Revision 1.3.2.10.4.6.2.22  2015/01/21 14:31:29  rrpulicherla
 *� Case#201411317
 *�
 *� Revision 1.3.2.10.4.6.2.21  2014/08/11 06:53:02  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Issue fixed related to case#20149801
 *�
 *� Revision 1.3.2.10.4.6.2.20  2014/07/04 14:51:58  skavuri
 *� Case # 20148702 Compatibility Issue Fixed
 *�
 *� Revision 1.3.2.10.4.6.2.19  2014/07/04 14:44:12  skavuri
 *� Case # 20148702 Compatibility Issue Fixed
 *�
 *� Revision 1.3.2.10.4.6.2.18  2014/06/12 14:40:39  grao
 *� Case#: 20148787  New GUI account issue fixes
 *�
 *� Revision 1.3.2.10.4.6.2.17  2014/05/27 13:58:12  snimmakayala
 *� Case#: 201219808
 *� Ship Complete Flag update issue
 *�
 *� Revision 1.3.2.10.4.6.2.16  2013/11/27 14:10:28  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *�
 *� prodissue fixes
 *�
 *� Revision 1.3.2.10.4.6.2.15  2013/09/23 07:19:19  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Issue related case#20124483 is resolved.
 *�
 *� Revision 1.3.2.10.4.6.2.14  2013/09/13 15:49:32  nneelam
 *� Case#. 20124398
 *� Wave Cancle  Issue Fix..
 *�
 *� Revision 1.3.2.10.4.6.2.13  2013/07/15 11:34:51  snimmakayala
 *� Case# 201216072
 *� TPP PROD ISSUE
 *�
 *� Revision 1.3.2.10.4.6.2.12  2013/06/26 06:59:08  gkalla
 *� Case# 20123205
 *� Standard bundle Issue fix
 *�
 *� Revision 1.3.2.10.4.6.2.11  2013/06/19 22:59:45  gkalla
 *� CASE201112/CR201113/LOG201121
 *� As part of optimistic locking
 *�
 *� Revision 1.3.2.10.4.6.2.10  2013/06/03 15:44:32  grao
 *� CASE201112/CR201113/LOG201121
 *� PMM Issues fixes
 *�
 *� Revision 1.3.2.10.4.6.2.9  2013/05/15 07:36:54  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Prod and UAT issue fixes.
 *�
 *� Revision 1.3.2.10.4.6.2.8  2013/05/14 14:54:13  grao
 *� CASE201112/CR201113/LOG201121
 *� Standard bundle issues fixes
 *�
 *� Revision 1.3.2.10.4.6.2.7  2013/04/30 23:30:47  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Prod and UAT issue fixes.
 *�
 *� Revision 1.3.2.10.4.6.2.6  2013/04/16 13:24:26  spendyala
 *� CASE201112/CR201113/LOG2012392
 *� Issue fixes.
 *�
 *� Revision 1.3.2.10.4.6.2.5  2013/04/04 20:20:53  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Issue fixes
 *�
 *� Revision 1.3.2.10.4.6.2.4  2013/03/19 11:53:20  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Production and UAT issue fixes.
 *�
 *� Revision 1.3.2.10.4.6.2.3  2013/03/07 14:25:52  gkalla
 *� CASE201112/CR201113/LOG201121
 *� Chenaged the code by Lexjet Code review
 *�
 *� Revision 1.3.2.10.4.6.2.2  2013/03/05 14:57:31  skreddy
 *� CASE201112/CR201113/LOG201121
 *� Code merged from Lexjet production as part of Standard bundle
 *�
 *� Revision 1.3.2.10.4.6.2.1  2013/03/01 14:34:54  skreddy
 *� CASE201112/CR201113/LOG201121
 *� Merged from FactoryMation and change the Company name
 *�
 *� Revision 1.3.2.10.4.6  2013/02/19 00:51:39  gkalla
 *� CASE201112/CR201113/LOG201121
 *� Issue fixing
 *�
 *� Revision 1.3.2.10.4.5  2013/01/09 15:16:18  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Inventory Sync issue fixes
 *� .
 *�
 *� Revision 1.3.2.10.4.4  2012/11/20 23:23:59  gkalla
 *� CASE201112/CR201113/LOG201121
 *� While wave cancellation serial no is deleteing from inventory
 *�
 *� Revision 1.3.2.10.4.3  2012/11/15 14:40:52  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *� wavecancellation issuefix
 *�
 *� Revision 1.3.2.10.4.2  2012/11/01 14:55:02  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Decimal Qty Conversions
 *�
 *� Revision 1.3.2.10.4.1  2012/09/26 22:43:33  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Production Issue Fixes for FISK,BOOMBAH and TDG.
 *�
 *� Revision 1.3.2.10  2012/09/13 12:49:33  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Added new  filter  criteria i.e., Cluster# in canceling the wave.
 *�
 *� Revision 1.3.2.9  2012/08/26 04:34:44  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Wave Cancellation issue for FISK
 *� Case # 20120727
 *�
 *� Revision 1.3.2.8  2012/08/08 15:17:29  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *� Kit to order
 *�
 *� Revision 1.3.2.7  2012/07/18 15:09:59  gkalla
 *� CASE201112/CR201113/LOG201121
 *� Fine tuned
 *�
 *� Revision 1.3.2.6  2012/06/04 14:56:18  spendyala
 *� CASE201112/CR201113/LOG201121
 *� issue related to JAE is resolved.
 *�
 *� Revision 1.3.2.5  2012/03/20 20:42:32  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Moved from Trunk to Branch
 *�
 *� Revision 1.8  2012/03/19 23:41:53  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Production issue fixes - TPP
 *�
 *� Revision 1.7  2012/03/15 07:25:31  rmukkera
 *� CASE201112/CR201113/LOG201121
 *�   pagging functionality added
 *�
 *� Revision 1.6  2012/02/21 15:04:46  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Stable bundle issue fixes
 *�
 *� Revision 1.2.2.1  2012/01/25 13:47:51  spendyala
 *� CASE201112/CR201113/LOG201121
 *� moved to branch.
 *�
 *
 ****************************************************************************/

/**
 * 
 * @param alreadyAddedList
 * @param currentValue
 * @returns {Boolean}
 */
function isValueAlreadyAdded(alreadyAddedList, currentValue){
	var alreadyAdded = false;

	for(var i = 0; i < alreadyAddedList.length; i++){
		if(alreadyAddedList[i] == currentValue){
			alreadyAdded = true;
			break;
		}
	}

	return alreadyAdded;
}

function isFulfillOrderLineAdded(fulfillOrderList, currentOrder, currentLine){
	var alreadyAdded = false;

	for(var i = 0; i < fulfillOrderList.length; i++){
		if(fulfillOrderList[i][0] == currentOrder && fulfillOrderList[i][1] == currentLine){
			alreadyAdded = true;
			break;
		}
	}

	return alreadyAdded;
}

/**
 * 
 * @param fulfillOrderField
 * @param waveListField
 * @param fulfillOrderList
 */
function addValuesToFields(OrderField, waveListField, fulfillOrderList, searchResult){
	nlapiLogExecution('Debug', 'addValuesToFields', 'Start');
	var ordersAlreadyAddedList = new Array();
	var wavesAlreadyAddedList = new Array();

	if(searchResult != null && searchResult.length > 0){
		for(var j = 0; j < searchResult.length; j++){
			var openTaskRecords=searchResult[j];
			for(var i = 0; i < openTaskRecords.length; i++){
				var currentTask = openTaskRecords[i];
				var fulfillOrderNo = currentTask.getValue('name');
				var waveNo = currentTask.getValue('custrecord_ebiz_wave_no');
				var OrderNo=currentTask.getText('custrecord_ebiz_order_no');
				var ebizOrderNo=currentTask.getValue('custrecord_ebiz_order_no');
				if(fulfillOrderList != null){
					if(!isValueAlreadyAdded(ordersAlreadyAddedList, fulfillOrderNo)){
//						nlapiLogExecution('Debug', 'Adding fulfillment order', fulfillOrderNo);
						fulfillOrderList.addSelectOption(fulfillOrderNo, fulfillOrderNo);
						ordersAlreadyAddedList.push(fulfillOrderNo);
					}
				}
				if(OrderField != null){
					if(!isValueAlreadyAdded(ordersAlreadyAddedList, ebizOrderNo)){
//						nlapiLogExecution('Debug', 'Adding fulfillment order', OrderNo);
						OrderField.addSelectOption(ebizOrderNo, OrderNo);
						ordersAlreadyAddedList.push(ebizOrderNo);
					}
				}
				if(waveListField != null)
				{
					if(!isValueAlreadyAdded(wavesAlreadyAddedList, waveNo)){
//						nlapiLogExecution('Debug', 'Adding wave number', parseFloat(waveNo));
						waveListField.addSelectOption(waveNo, waveNo);
						wavesAlreadyAddedList.push(waveNo);
					}
				}
			}
		}
	}
}

/**
 * 
 * @param levelInd
 * @returns {Array}
 */
function getFulfillOrderColumns(levelInd){
	var columns = new Array();

	if(levelInd == 'HEADER_LEVEL'){
		columns[0] = new nlobjSearchColumn('custrecord_ns_ord');
		columns[1] = new nlobjSearchColumn('custrecord_lineord');
		columns[2] = new nlobjSearchColumn('custrecord_ordline');
		columns[3] = new nlobjSearchColumn('custrecord_ebiz_wave');
		columns[0].setSort(true);
	} else if(levelInd == 'DETAIL_LEVEL') {
		columns[0] = new nlobjSearchColumn('custrecord_ns_ord');
		columns[1] = new nlobjSearchColumn('custrecord_lineord');
		columns[2] = new nlobjSearchColumn('custrecord_ordline');
		columns[3] = new nlobjSearchColumn('custrecord_ebiz_wave');
		columns[4] = new nlobjSearchColumn('custrecord_base_uom_id');
		columns[5] = new nlobjSearchColumn('custrecord_base_ord_qty');
		columns[6] = new nlobjSearchColumn('custrecord_base_pick_qty');
		columns[7] = new nlobjSearchColumn('custrecord_base_pickgen_qty');
		columns[8] = new nlobjSearchColumn('custrecord_base_ship_qty');
		columns[0].setSort(true);
	}

	return columns;
}

function getOpenTaskColumns(levelInd){
	var columns = new Array();

	if(levelInd == 'HEADER_LEVEL'){
		columns[0] = new nlobjSearchColumn('name');
		columns[1] = new nlobjSearchColumn('custrecord_line_no');
		columns[2] = new nlobjSearchColumn('custrecord_tasktype');
		columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		columns[4] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		columns[5] = new nlobjSearchColumn('custrecord_ebiz_receipt_no');
		columns[6] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
		columns[7] = new nlobjSearchColumn('custrecord_wms_status_flag');
		columns[8] = new nlobjSearchColumn('custrecord_parent_sku_no');
		columns[9] = new nlobjSearchColumn('custrecord_container_lp_no');
		columns[10] = new nlobjSearchColumn('custrecord_sku');
		columns[11] = new nlobjSearchColumn("type",'custrecord_parent_sku_no');
		columns[6].setSort(true); // Sort by wave no.
	} else if(levelInd == 'DETAIL_LEVEL') {
		columns[0] = new nlobjSearchColumn('name');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[2] = new nlobjSearchColumn('custrecord_sku');
		columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[4] = new nlobjSearchColumn('custrecord_container_lp_no');
		columns[5] = new nlobjSearchColumn('custrecord_ebiz_lpno');
		columns[6] = new nlobjSearchColumn('custrecord_line_no');
		columns[7] = new nlobjSearchColumn('custrecord_tasktype');
		columns[8] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[9] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		columns[10] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		columns[11] = new nlobjSearchColumn('custrecord_ebiz_receipt_no');
		columns[12] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
		columns[13] = new nlobjSearchColumn('custrecordact_begin_date');
		columns[14] = new nlobjSearchColumn('custrecord_wms_status_flag');
		columns[15] = new nlobjSearchColumn('custrecord_from_lp_no');
		columns[16] = new nlobjSearchColumn('custrecord_actualbegintime');
		columns[17] = new nlobjSearchColumn('custrecord_invref_no');
		columns[18] = new nlobjSearchColumn('custrecord_packcode');
		columns[19] = new nlobjSearchColumn('custrecord_sku_status');
		columns[20] = new nlobjSearchColumn('custrecord_uom_id');
		columns[21] = new nlobjSearchColumn('custrecord_batch_no');
		columns[22] = new nlobjSearchColumn('custrecord_ebizrule_no');
		columns[23] = new nlobjSearchColumn('custrecord_ebizmethod_no');
		columns[24] = new nlobjSearchColumn('custrecord_ebizzone_no');
		columns[25] = new nlobjSearchColumn('custrecord_comp_id');
		columns[26] = new nlobjSearchColumn('custrecord_wms_location');						
		columns[27] = new nlobjSearchColumn('custrecord_ebizuser');		
		columns[28] = new nlobjSearchColumn('custrecord_upd_ebiz_user_no');						
		columns[29] = new nlobjSearchColumn('custrecord_taskassignedto');
		columns[30] = new nlobjSearchColumn('custrecord_parent_sku_no');
		columns[31] = new nlobjSearchColumn("type",'custrecord_parent_sku_no');

		columns[12].setSort(true);  // Sort by wave number
	}

	return columns;
}

function getInvtDtlsForOneTask(pickTask){
	nlapiLogExecution('Debug', 'getInvtDtlsRow', 'Start');

	var currentRow = new Array();

	var fulfillmentOrder = pickTask.getValue('name');
	var itemNo = pickTask.getValue('custrecord_ebiz_sku_no');
	var item = pickTask.getValue('custrecord_sku');
	var expectedQty = pickTask.getValue('custrecord_expe_qty');
	var containerLP = pickTask.getValue('custrecord_container_lp_no');
	var eBizLPNo = pickTask.getValue('custrecord_ebiz_lpno');
	var lineNo = pickTask.getValue('custrecord_line_no');
	var taskType = pickTask.getValue('custrecord_tasktype');
	var actBeginLocn = pickTask.getValue('custrecord_actbeginloc');
	var eBizControlNo = pickTask.getValue('custrecord_ebiz_cntrl_no');
	var waveNo = pickTask.getValue('custrecord_ebiz_wave_no');
	var wmsStatusFlag = pickTask.getValue('custrecord_wms_status_flag');
	var invtRefNo = pickTask.getValue('custrecord_invref_no');
	var itemStatus = pickTask.getValue('custrecord_sku_status');
	var uomId = pickTask.getValue('custrecord_uom_id');
	var wmsLocation = pickTask.getValue('custrecord_wms_location');
	var eBizOrderNo = pickTask.getValue('custrecord_ebiz_order_no');

	currentRow['fulfillmentOrder'] = fulfillmentOrder;				// Fulfillment Order
	currentRow['itemNo'] = itemNo;									// Item No.
	currentRow['item'] = item;										// Item
	currentRow['expectedQty'] = expectedQty;						// Expected Qty
	currentRow['containerLP'] = containerLP;						// Container LP
	currentRow['eBizLPNo'] = eBizLPNo;								// eBizNET LP No.
	currentRow['lineNo'] = lineNo;									// Order Line No.
	currentRow['taskType'] = taskType;								// Task Type
	currentRow['actBeginLocn'] = actBeginLocn;						// Actual Begin Location
	currentRow['eBizControlNo'] = eBizControlNo;					// eBizNET Control No
	currentRow['waveNo'] = waveNo;									// Wave No.
	currentRow['wmsStatusFlag'] = wmsStatusFlag;					// WMS Status Flag
	currentRow['invtRefNo'] = invtRefNo;							// Inventory Reference No.
	currentRow['itemStatus'] = itemStatus;							// Item Status
	currentRow['uomId'] = uomId;									// UOM ID
	currentRow['wmsLocation'] = wmsLocation;						// Warehouse Location
	currentRow['eBizOrderNo'] = eBizOrderNo;						// eBizNET Order No.

	nlapiLogExecution('Debug', 'getInvtDtlsRow', 'End');
	return currentRow;
}

function getOpenTaskColumns(){
	nlapiLogExecution('Debug', 'getOpenTaskColumns', 'Start');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[2] = new nlobjSearchColumn('custrecord_sku');
	columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[4] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[5] = new nlobjSearchColumn('custrecord_ebiz_lpno');
	columns[6] = new nlobjSearchColumn('custrecord_line_no');
	columns[7] = new nlobjSearchColumn('custrecord_tasktype');
	columns[8] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[9] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[10] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[11] = new nlobjSearchColumn('custrecord_ebiz_receipt_no');
	columns[12] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
	columns[13] = new nlobjSearchColumn('custrecordact_begin_date');
	columns[14] = new nlobjSearchColumn('custrecord_wms_status_flag');
	columns[15] = new nlobjSearchColumn('custrecord_from_lp_no');
	columns[16] = new nlobjSearchColumn('custrecord_actualbegintime');
	columns[17] = new nlobjSearchColumn('custrecord_invref_no');
	columns[18] = new nlobjSearchColumn('custrecord_packcode');
	columns[19] = new nlobjSearchColumn('custrecord_sku_status');
	columns[20] = new nlobjSearchColumn('custrecord_uom_id');
	columns[21] = new nlobjSearchColumn('custrecord_batch_no');
	columns[22] = new nlobjSearchColumn('custrecord_ebizrule_no');
	columns[23] = new nlobjSearchColumn('custrecord_ebizmethod_no');
	columns[24] = new nlobjSearchColumn('custrecord_ebizzone_no');
	columns[25] = new nlobjSearchColumn('custrecord_comp_id');
	columns[26] = new nlobjSearchColumn('custrecord_wms_location');						
	columns[27] = new nlobjSearchColumn('custrecord_ebizuser');		
	columns[28] = new nlobjSearchColumn('custrecord_upd_ebiz_user_no');						
	columns[29] = new nlobjSearchColumn('custrecord_taskassignedto');
	columns[10].setSort(true);  // Sort by wave number
	columns[30] = new nlobjSearchColumn('id');
	columns[30].setSort(true); 
	columns[31] = new nlobjSearchColumn('custrecord_parent_sku_no');
	columns[32] = new nlobjSearchColumn("type",'custrecord_parent_sku_no');
	nlapiLogExecution('Debug', 'getOpenTaskColumns', 'End');
	return columns;
}

/**
 * Function to retrieve open task records for the specified wave number with
 * WMS STATUS FLAG having either of the values, STATUS.OUTBOUND.PICK_GENERATED or
 * STATUS.OUTBOUND.ORDER_PARTIALLY_PROCESSED or STATUS.OUTBOUND.ORDERLINE_PARTIALLY_LOADED
 * or STATUS.OUTBOUND.ORDERLINE_PARTIALLY_SHIPPED
 * 
 * @param waveNo
 * @returns [LIST OF OPEN TASK RECORDS]
 */
var searchResultArray=new Array();
function getOpenTaskRecordsForWave(waveNo,orderno,orderlineno,maxid,cluster){
	nlapiLogExecution('Debug', 'getOpenTaskRecordsForWave', 'Start');
	nlapiLogExecution('Debug', 'orderlineno', orderlineno);
	nlapiLogExecution('Debug', 'waveNo', waveNo);
	nlapiLogExecution('Debug', 'cluster', cluster);
	// Filter open task records by wave number and wms_status_flag
	var filters = new Array();
	if(waveNo!=null && waveNo!='')
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(waveNo)));
	}
	if(orderno!=null && orderno!='')
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', orderno));
	}
	if(orderlineno!=null && orderlineno!='')
	{
		filters.push(new nlobjSearchFilter('name', null, 'is', orderlineno));
	}
	if(cluster!=null && cluster!='')
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', cluster.toString()));
	}
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','11','12','13','26']));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));
	if(maxid!=0)
		filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxid));
	var columns = getOpenTaskColumns();

	// Retrieve all open task records for the selected wave
	var openTaskRecords = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(openTaskRecords!=null && openTaskRecords.length>=1000)
	{ 
		searchResultArray.push(openTaskRecords); 
		var maxno=openTaskRecords[openTaskRecords.length-1].getValue(columns[30]);
		getOpenTaskRecordsForWave(waveNo,orderno,orderlineno,maxno,cluster);	
	}
	else
	{
		searchResultArray.push(openTaskRecords); 
	}
	logCountMessage('Debug', openTaskRecords);
	nlapiLogExecution('Debug', 'openTaskRecords', openTaskRecords.length);
	nlapiLogExecution('Debug', 'getOpenTaskRecordsForWave', 'End');
	return searchResultArray;
}

function getOpenTaskRecordsForWavecancel(waveNo,orderno,orderlineno,taskid,request){
	nlapiLogExecution('Debug', 'getOpenTaskRecordsForWavecancel', 'Start');
	var ebizordno=request.getParameter('custpage_orderlist');
	nlapiLogExecution('Debug', 'waveNo', waveNo);
	nlapiLogExecution('Debug', 'orderno', orderno);
	nlapiLogExecution('Debug', 'orderlineno', orderlineno);
	nlapiLogExecution('Debug', 'orderlineno', ebizordno);
	nlapiLogExecution('Debug', 'taskid', taskid);
	// Filter open task records by wave number and wms_status_flag


	//var ebizordno=request.getParameter('custpage_orderlist');
	var filters = new Array();

	if(taskid!=null && taskid!='')
		filters.push(new nlobjSearchFilter('internalid', null, 'is', taskid));

	if(waveNo!=null && waveNo!='')
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', waveNo));
	}
	if(ebizordno!=null && ebizordno!='')
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', ebizordno));
	}
	if(orderno!=null && orderno!='')
	{
		filters.push(new nlobjSearchFilter('name', null, 'is', orderno));
	}
	if(orderlineno!=null && orderlineno!='')
	{
		filters.push(new nlobjSearchFilter('custrecord_line_no', null, 'is', orderlineno));
	}
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','11','12','13','26']));

	var columns = getOpenTaskColumns();

	// Retrieve all open task records for the selected wave
	var openTaskRecords = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	logCountMessage('Debug', openTaskRecords);
	nlapiLogExecution('Debug', 'openTaskRecords', openTaskRecords.length);
	nlapiLogExecution('Debug', 'getOpenTaskRecordsForWavecancel', 'End');
	return openTaskRecords;
}

function getDtlsForInvtUpdate(openTaskRecords){
	nlapiLogExecution('Debug', 'getDtlsForInvtUpdate', 'Start');

	var invtDtlsArray = new Array();

	if(openTaskRecords != null && openTaskRecords.length > 0){
		for(var i = 0; i < openTaskRecords.length; i++){
			var pickTask = openTaskRecords[i];
			var invtDtls = getInvtDtlsForOneTask(pickTask);

			invtDtlsArray.push(invtDtls);
		}
	}

	logCountMessage('Debug', invtDtlsArray);
	nlapiLogExecution('Debug', 'getDtlsForInvtUpdate', 'End');
	return invtDtlsArray;
}

/**
 * Function to retrieve the list of active fulfillment order records for 
 * the specified wave number
 * 
 * @param waveNo
 * @returns [LIST OF FULFILLMENT ORDER RECORDS]
 */
function getFulfillOrderInfo(waveNo,orderNo,orderlineNo){
	nlapiLogExecution('Debug', 'getFulfillOrderInfo', 'Start');
	nlapiLogExecution('Debug', 'waveNo', waveNo);
	nlapiLogExecution('Debug', 'orderNo', orderNo);
	nlapiLogExecution('Debug', 'orderlineNo', orderlineNo);

	var filters = new Array();
	if(waveNo!=null && waveNo!='')
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiz_wave', null, 'equalto', [waveNo]));
	}
	if(orderNo!=null && orderNo!='')
	{
		filters.push(new nlobjSearchFilter('name', null, 'is', orderNo));
	}
	if(orderlineNo!=null && orderlineNo!='')
	{
		filters.push(new nlobjSearchFilter('custrecord_ordline', null, 'equalto', [orderlineNo]));
	}

	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ns_ord');
	columns[1] = new nlobjSearchColumn('custrecord_lineord');
	columns[2] = new nlobjSearchColumn('custrecord_ordline');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_wave');
	columns[4] = new nlobjSearchColumn('custrecord_base_uom_id');
	columns[5] = new nlobjSearchColumn('custrecord_base_ord_qty');
	columns[6] = new nlobjSearchColumn('custrecord_base_pick_qty');
	columns[7] = new nlobjSearchColumn('custrecord_base_pickgen_qty');
	columns[8] = new nlobjSearchColumn('custrecord_base_ship_qty');
	columns[9] = new nlobjSearchColumn('custrecord_wave_status_flag');
	columns[10] = new nlobjSearchColumn('name');

	var fOrdSearchResults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
	var usage = getUnitsConsumed('nlapiSearchRecord');

	logCountMessage('Debug', fOrdSearchResults);
	nlapiLogExecution('Debug', 'getFulfillOrderInfo|USAGE', usage);
	nlapiLogExecution('Debug', 'getFulfillOrderInfo', 'End');
	return fOrdSearchResults;
}

function getOrderNoArray(fulfillOrderList){
	nlapiLogExecution('Debug', 'getOrderNoArray', 'Start');

	if(fulfillOrderList != null && fulfillOrderList.length > 0){
		for(var i = 0; i < fulfillOrderList.length; i++){

		}
	}
	nlapiLogExecution('Debug', 'getOrderNoArray', 'End');	
}

function getFulfillOrderList(openTaskRecords){
	nlapiLogExecution('Debug', 'getFulfillOrderList', 'Start');
	var fulfillOrderList = new Array();
	var orderNoList = new Array();
	var lineNoList = new Array();

	if(openTaskRecords != null && openTaskRecords.length > 0){
		for(var i = 0; i < openTaskRecords.length; i++){
			var pickTask = openTaskRecords[i];
			var fulfillOrderNo = pickTask.getValue('name');
			var lineNo = pickTask.getValue('custrecord_line_no');
			if(!isValueAlreadyAdded(orderNoList, fulfillOrderNo)){
				orderNoList.push(fulfillOrderNo);
			}

			if(!isFulfillOrderLineAdded(fulfillOrderList, fulfillOrderNo, lineNo)){
				var currentRow = new Array();
				currentRow['fulfillOrderNo'] = fulfillOrderNo;
				currentRow['lineNo'] = lineNo;
				lineNoList.push(currentRow);
			}
		}

		fulfillOrderList = [orderNoList, lineNoList];
	}

	logCountMessage('Debug', fulfillOrderList);
	nlapiLogExecution('Debug', 'getFulfillOrderList', 'End');
	return fulfillOrderList;
}

//function getFulfillOrderDtls(fulfillOrderList){
//nlapiLogExecution('Debug', 'getFulfillOrderDtls', 'Start');

//var filters = new Array();
//filters.push(new nlobjSearchFilter('custrecord_lineord', null, ));
//columns[0] = new nlobjSearchColumn('custrecord_ns_ord');
//columns[1] = new nlobjSearchColumn('custrecord_lineord');
//columns[2] = new nlobjSearchColumn('custrecord_ordline');
//columns[3] = new nlobjSearchColumn('custrecord_ebiz_wave');

//nlapiLogExecution('Debug', 'getFulfillOrderDtls', 'End');
//}
var searchResultArray=new Array();
function getAllOpenTaskRecordsForWaveCancel(levelInd, waveCancelLevel, waveNo,maxid){
	nlapiLogExecution('Debug', 'getAllOpenTaskRecordsForWaveCancel', 'Start');

	var filters = new Array();
	// Look for valid wave#
	if((levelInd == 'HEADER_LEVEL') || (levelInd == 'DETAIL_LEVEL' && waveCancelLevel != 'WAVE')){
		filters[0] = new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'isnotempty');
		nlapiLogExecution('Debug', 'Setting filter for non-empty wave no search');
	}else if(levelInd == 'DETAIL_LEVEL' && waveCancelLevel != 'WAVE'){
		filters[0] = new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'equals', waveNo);
		nlapiLogExecution('Debug', 'Setting filter for wave no', waveNo);
	}

	// Look for records with any of the following statuses
	// STATUS.OUTBOUND.PICK_GENERATED = 9; STATUS.OUTBOUND.ORDER_PARTIALLY_PROCESSED = 11
	// STATUS.OUTBOUND.ORDER_PARTIALLY_LOADED = 12; STATUS.OUTBOUND.ORDER_PARTIALLY_SHIPPED = 13
	filters[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','11','12','13','26']);
	filters[2] = new nlobjSearchFilter('id', null, 'greaterthan',maxid);
	// Get list of columns to retrieve based on the level of granularity
	var columns = getOpenTaskColumns(levelInd);

	// Load all the open task records as per filter criteria
	var openTaskRecords = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(openTaskRecords!=null && openTaskRecords.length>=1000)
	{ 
		searchResultArray.push(openTaskRecords); 
		var maxno=openTaskRecords[openTaskRecords.length-1].getValue(columns[30]);
		getAllOpenTaskRecordsForWaveCancel(levelInd, waveCancelLevel, waveNo,maxno);	
	}
	else
	{
		searchResultArray.push(openTaskRecords); 
	}
	logCountMessage('Debug', openTaskRecords);
	nlapiLogExecution('Debug', 'openTaskRecords', openTaskRecords.length);
	nlapiLogExecution('Debug', 'getAllOpenTaskRecordsForWaveCancel', 'End');
	return searchResultArray;
}

/**
 * 
 * @returns
 */
function getFulfillmentOrdersForWaveCancel(levelInd){
	nlapiLogExecution('Debug', 'getFulfillmentOrdersForWaveCancel', 'Start');
	var filters = new Array();
	// Search for fulfillment orders with any of the following statuses
	// STATUS.OUTBOUND.PICK_GENERATED = 9; STATUS.OUTBOUND.ORDER_PARTIALLY_PROCESSED = 11
	// STATUS.OUTBOUND.ORDER_PARTIALLY_LOADED = 12; STATUS.OUTBOUND.ORDER_PARTIALLY_SHIPPED = 13
	filters[0] = new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', ['9','11','12','13','26']);

	// Get list of columns based on the level of granularity
	var columns = getFulfillOrderColumns('HEADER_LEVEL');

	// Retrieve fulfillment orders
	var fulfillOrders = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);

	logCountMessage('Debug', fulfillOrders);
	nlapiLogExecution('Debug', 'getFulfillmentOrdersForWaveCancel', 'End');
	return fulfillOrders;
}

function updateInventoryForTask(invtRefNo, expectedQty){
	nlapiLogExecution('Debug', 'updateInventoryForWaveCancel', 'Start');
	nlapiLogExecution('Debug', 'invtRefNo', invtRefNo);
	var inventoryRecord = null;
	try
	{
		var scount=1;
		LABL1: for(var j=0;j<scount;j++)
		{	
			nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', j);
			try
			{

				inventoryRecord = nlapiLoadRecord('customrecord_ebiznet_createinv', invtRefNo); 		// 2 UNITS

				if(inventoryRecord!=null && inventoryRecord!='')
				{
					//var usage = getUnitsConsumed('nlapiLoadRecord');
					var allocationQty = inventoryRecord.getFieldValue('custrecord_ebiz_alloc_qty');

					nlapiLogExecution('Debug', 'Inventory Allocation Qty[PREV]', allocationQty);

					// Updating allocation qty
					if(parseFloat(allocationQty) <= 0){
						allocationQty = parseFloat(allocationQty) + parseFloat(expectedQty);
					} 
					else if(parseFloat(allocationQty) > 0){
						allocationQty = parseFloat(allocationQty) - parseFloat(expectedQty);
					}

					if(parseFloat(allocationQty) <= 0){
						allocationQty=0;
					}

					inventoryRecord.setFieldValue('custrecord_ebiz_alloc_qty', parseFloat(allocationQty).toFixed(5));
					inventoryRecord.setFieldValue('custrecord_ebiz_callinv', 'N');
					nlapiSubmitRecord(inventoryRecord, false, true); 										// 2 UNITS
					//usage = parseFloat(usage) + parseFloat(getUnitsConsumed('nlapiSubmitRecord'));

					nlapiLogExecution('Debug', 'Inventory Allocation Qty[NEW]', allocationQty);

					//nlapiLogExecution('Debug', 'updateInventoryForWaveCancel|USAGE', usage);
					nlapiLogExecution('Debug', 'updateInventoryForWaveCancel', 'End');

					// Total 4 UNITS


					/*var inventoryTransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', vInvRec.getId());
				var allocationQty = inventoryTransaction.getFieldValue('custrecord_ebiz_alloc_qty');
				if (isNaN(allocationQty))
				{
					allocationQty = 0;
				}
				if(parseInt(allocationQty) <= 0){
					allocationQty = parseInt(allocationQty) + parseInt(expectedQty);
				} 
				else if(parseInt(allocationQty) > 0){
					allocationQty = parseInt(allocationQty) - parseInt(expectedQty);
				}

				if(parseInt(allocationQty) <= 0){
					allocationQty=0;
				}
				//var totalallocqty=parseInt(allocationQuantity)+parseInt(alreadyallocqty);
				inventoryTransaction.setFieldValue('custrecord_ebiz_alloc_qty', allocationQty);
				inventoryTransaction.setFieldValue('custrecord_ebiz_callinv', 'N');
				inventoryTransaction.setFieldValue('custrecord_ebiz_displayfield', 'N');
				if(emp != null && emp != '')
					inventoryTransaction.setFieldValue('custrecord_updated_user_no', emp);					 
				else
					inventoryTransaction.setFieldValue('custrecord_updated_user_no', nlapiGetContext().getUser());

				nlapiSubmitRecord(inventoryTransaction,false,true);*/
				}
			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 

				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				}				 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}

	}
	catch(exp) {
		nlapiLogExecution('Debug', 'Exception in Wave Cancelation ', exp);	
		//showInlineMessage(form, 'Debug', 'Wave Cancellation Failed', "");
		//response.writePage(form);
	}


}

function removePickTasksAndUpdateInvt(openTaskRecords, InvtDetails,Adjustfullfillqty,waveNo){
	nlapiLogExecution('Debug', 'removePickTasksAndUpdateInvt', 'Start');
	nlapiLogExecution('Debug', 'openTaskRecords.length', openTaskRecords.length);
	nlapiLogExecution('Debug', 'InvtDetails', InvtDetails);
	var usage = 0;
	if(openTaskRecords != null && openTaskRecords.length > 0){
		for(var i = 0; i < openTaskRecords.length; i++){
			// Check if it is a PICK task
			nlapiLogExecution('Debug', 'TaskType', parseFloat(openTaskRecords[i].getValue('custrecord_tasktype')));
			if(parseFloat(openTaskRecords[i].getValue('custrecord_tasktype')) == parseFloat(3)){
				var task = openTaskRecords[i];
				var recordId = task.getId();
				var invtRefNo = task.getValue('custrecord_invref_no');
				var expectedQty = task.getValue('custrecord_expe_qty');
				var fointrid = task.getValue('custrecord_ebiz_cntrl_no');
				nlapiLogExecution('Debug', 'invtRefNo', invtRefNo);
				nlapiLogExecution('Debug', 'expectedQty', expectedQty);
				// Update the inventory to reduce the allocation quantity
				if(invtRefNo!=null && invtRefNo!='')
				{
					updateInventoryForTask(invtRefNo, expectedQty);									// 4 UNITS
				}

				// Delete the open task record for PICK task
				var deletedId = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', recordId); 	// 4 UNITS
				nlapiLogExecution('Debug', 'Deleting open task record', recordId);
				var unitsConsumed = getUnitsConsumed('nlapiDeleteRecord');
				//usage = parseFloat(usage) + parseFloat(unitsConsumed);
				updateFulfillOrderRecords(fointrid,expectedQty,Adjustfullfillqty,waveNo); 					// 8 UNITS

				// Total 16 UNITS
			}
		}
	}

	//nlapiLogExecution('Debug', 'removePickTasksAndUpdateInvt|USAGE', usage);
	nlapiLogExecution('Debug', 'removePickTasksAndUpdateInvt', 'End');
}

function updateFulfillOrderRecords(fointrid,expectedQty,Adjustfullfillqty,waveNo){
	nlapiLogExecution('Debug', 'updateFulfillOrderRecords', 'Start');
	nlapiLogExecution('Debug', 'Adjustfullfillqty', Adjustfullfillqty);
	nlapiLogExecution('Debug', 'expectedQty', expectedQty);
	nlapiLogExecution('Debug', 'fointrid', fointrid);
	nlapiLogExecution('Debug', 'waveNo', waveNo);
	if(fointrid != null && fointrid!=''){

		var FullfilmrntRecord = nlapiLoadRecord('customrecord_ebiznet_ordline', fointrid); // 2 UNITS
		var pickgenqty = FullfilmrntRecord.getFieldValue('custrecord_pickgen_qty');
		var pickconfirmqty=FullfilmrntRecord.getFieldValue('custrecord_pickqty');
		var shipqty=FullfilmrntRecord.getFieldValue('custrecord_ship_qty');
		var fowaveno=FullfilmrntRecord.getFieldValue('custrecord_ebiz_wave');

		nlapiLogExecution('Debug', 'fowaveno', fowaveno);

		if(fowaveno==waveNo)
		{

			if(pickgenqty==null || pickgenqty=='' || isNaN(pickgenqty))
				pickgenqty=0;

			if(pickconfirmqty==null || pickconfirmqty=='' || isNaN(pickconfirmqty))
				pickconfirmqty=0;

			nlapiLogExecution('Debug', 'total pickgenQty', pickgenqty);

			var rempickgenqty =  parseFloat(pickgenqty) - parseFloat(expectedQty);

			if(parseFloat(rempickgenqty)<=parseFloat(pickconfirmqty))
				rempickgenqty=pickconfirmqty;

			/*** The below code is merged from FactoryMation production account on 01Mar13 by Santosh as part of Standard bundle***/
			FullfilmrntRecord.setFieldValue('custrecord_pickgen_qty', parseFloat(rempickgenqty).toFixed(5));
			/*** up to here ***/
			FullfilmrntRecord.setFieldValue('custrecord_linestatus_flag', 25); //Status Edit
			FullfilmrntRecord.setFieldValue('custrecord_ebiz_wave', ''); //Status Edit
			nlapiSubmitRecord(FullfilmrntRecord, false, true);	


			if(Adjustfullfillqty=='T')
			{
				nlapiLogExecution('Debug', 'rempickgenqty', rempickgenqty);
				if(rempickgenqty=='0' || rempickgenqty==null)
				{
					nlapiLogExecution('Debug', 'Deleting fullfillment  record', fointrid);
					var deletedId = nlapiDeleteRecord('customrecord_ebiznet_ordline', fointrid); // 4 UNITS

				}
			}		
		}
	}
	nlapiLogExecution('Debug', 'updateFulfillOrderRecords', 'End');

	//Total 8 UNITS
}

function executeWaveCancel(openTaskRecords, invtDtls,Adjustfullfillqty,waveNo){
	nlapiLogExecution('Debug', 'executeWaveCancel', 'Start');

	/*
	 * Remove all the PICK tasks from Open Task
	 * Reduce inventory by expected quantity
	 * Reduce pickgen quantity by expected quantity
	 * Update fulfillment order status
	 */
	removePickTasksAndUpdateInvt(openTaskRecords,invtDtls,Adjustfullfillqty,waveNo); // 16 UNITS/TASK
	//updateFulfillOrderRecords(fOrdRecords,Adjustfullfillqty);


	nlapiLogExecution('Debug', 'executeWaveCancel', 'End');
}


function buildWaveCancellationForm(form, request, response){
	nlapiLogExecution('Debug', 'buildWaveCancellationForm', 'Start');

	/*var selectcriteria = form.addField('custpage_selectcriteria', 'select', 'Criteria').setLayoutType('startrow','none');
	selectcriteria.addSelectOption('WAVE', 'Wave Level');
	selectcriteria.addSelectOption('HEADER_LEVEL', 'Order Level');
	selectcriteria.addSelectOption('DETAIL_LEVEL', 'Order Line Level');

	var criteria;
	if(request.getParameter('custpage_selectcriteria')!='' && request.getParameter('custpage_selectcriteria')!=null)
	{
		selectcriteria.setDefaultValue(request.getParameter('custpage_selectcriteria'));
		criteria=parseFloat(request.getParameter('custpage_selectcriteria'));
	}

	var waveListField = form.addField('custpage_wavelist', 'select', 'Wave #').setLayoutType('startrow', 'none');
	waveListField.addSelectOption("","");
	var waveno;
	if(request.getParameter('custpage_wavelist')!='' && request.getParameter('custpage_wavelist')!=null)
	{
		waveListField.setDefaultValue(request.getParameter('custpage_wavelist'));
		waveno=parseFloat(request.getParameter('custpage_wavelist'));
	}

        	// Retrieve all open task records with status flag as wave generated and wave# non-empty
       var	 openTaskRecords1 = getAllOpenTaskRecordsForWaveCancel('HEADER_LEVEL', null, null,0);


	// Add information to the selection fields
	addValuesToFields(null, waveListField, null, openTaskRecords1);

	var orderField = form.addField('custpage_orderlist', 'select', 'Order #').setLayoutType('startrow', 'none');
	orderField.addSelectOption("","");	

	addValuesToFields(orderField, null, null, openTaskRecords1);
	var Order;
	if(request.getParameter('custpage_orderlist')!='' && request.getParameter('custpage_orderlist')!=null)
	{
		orderField.setDefaultValue(request.getParameter('custpage_orderlist'));
		Order=parseFloat(request.getParameter('custpage_orderlist'));
	}

	var orderlineField = form.addField('custpage_orderlinelist', 'select', 'Fullfilment Order #').setLayoutType('startrow', 'none');
	orderlineField.addSelectOption("","");

	addValuesToFields(null, null, orderlineField, openTaskRecords1);

	var OrderLines;
	if(request.getParameter('custpage_orderlinelist')!='' && request.getParameter('custpage_orderlinelist')!=null)
	{
		orderlineField.setDefaultValue(request.getParameter('custpage_orderlinelist'));
		OrderLines=request.getParameter('custpage_orderlinelist');
	}*/
	var hiddenField = form.addField('custpage_hiddenfield', 'text', '').setDisplayType('hidden');
	hiddenField.setDefaultValue('T');
	var waveno;
	var waveListField = form.addField('custpage_wavelist', 'text', 'Wave #').setLayoutType('startrow', 'none');
	waveListField.setDisplayType('inline');
	if(request.getParameter('custpage_wavelist')!='' && request.getParameter('custpage_wavelist')!=null)
	{
		waveListField.setDefaultValue(request.getParameter('custpage_wavelist'));
		waveno=parseFloat(request.getParameter('custpage_wavelist'));
	}
	var orderField = form.addField('custpage_orderlist', 'text', 'Order #').setLayoutType('startrow', 'none');
	orderField.setDisplayType('inline');
	var Order;
	if(request.getParameter('custpage_orderlist')!='' && request.getParameter('custpage_orderlist')!=null)
	{
		var ordernum;
		var idno = request.getParameter('custpage_orderlist');
		var salesorderFilers = new Array();
		salesorderFilers.push(new nlobjSearchFilter('internalid', null, 'is', idno));
		var columns=new Array();
		columns[0]=new nlobjSearchColumn('tranid');
		var customeSearchResults = nlapiSearchRecord('transaction', null, salesorderFilers,columns);
		if(customeSearchResults!=null && customeSearchResults!='' && customeSearchResults.length>0)
		{
			ordernum = customeSearchResults[0].getValue('tranid');
			orderField.setDefaultValue(ordernum);
		}

		//Order=parseFloat(request.getParameter('custpage_orderlist'));
		Order = ordernum;
	}
	var orderlineField = form.addField('custpage_orderlinelist', 'text', 'Fulfillment Order #').setLayoutType('startrow', 'none');
	orderlineField.setDisplayType('inline');
	var OrderLines;
	if(request.getParameter('custpage_orderlinelist')!='' && request.getParameter('custpage_orderlinelist')!=null)
	{
		orderlineField.setDefaultValue(request.getParameter('custpage_orderlinelist'));
		OrderLines=request.getParameter('custpage_orderlinelist');
	}

	//code added as of 120912 by suman
	//displaying the cluster no.
	var cluster;
	var ClusterField = form.addField('custpage_cluster', 'text', 'Cluster #').setLayoutType('startrow', 'none');
	ClusterField.setDisplayType('inline');
	if(request.getParameter('custpage_cluster')!='' && request.getParameter('custpage_cluster')!=null)
	{
		ClusterField.setDefaultValue(request.getParameter('custpage_cluster'));
		cluster=parseFloat(request.getParameter('custpage_cluster'));
	}
	//end of code as of 120912.
	var openTaskRecords = getOpenTaskRecordsForWave(waveno,Order,OrderLines,0,cluster);

	var orderlinecount=0;
	var prelinecount;
	var Ordercount=0;
	var vPrevOrd;
	var ordWaveno;
	var ParentOrdNo;
	var lineOrdNovalue;
	var fulfillordno;
	var Lineno;
	var prewaveno;
	var preordno;
	var precount;
	var orderline;
	var item;
	var ordqty;
	var pickgenqty;
	var taskid;

	if(openTaskRecords != null && openTaskRecords != "")
	{		
		setPagingForSublist(openTaskRecords,form,request);		

	}



	// Add submit button



}
function setPagingForSublist(openTaskRecords,form,request)
{

	//case # 20124398�Start
	var temparray = openTaskRecords[0];
	//case # 20124398�End
	nlapiLogExecution('Debug', 'openTaskRecords.length', openTaskRecords.length);
	nlapiLogExecution('Debug', 'temparray.length', temparray.length);
	if(temparray.length!=null && temparray.length!='')
	{
		var submit = form.addSubmitButton('Cancel Wave');
		var sublist = form.addSubList("custpage_wavecancellist", "list", "Wave Cancellation List");
		sublist.addMarkAllButtons();
		sublist.addField("custpage_wavecancel", "checkbox", "Cancel").setDefaultValue("T");
		sublist.addField("custpage_waveno", "text", "Wave #").setDisplayType('inline');		
		sublist.addField("custpage_orderno", "text", "Order #").setDisplayType('inline');
		sublist.addField("custpage_fulfillordno", "text", "Fulfillment Order #").setDisplayType('inline');
		sublist.addField("custpage_fulfillordlineno", "text", "Fulfillment Order Line #").setDisplayType('inline');
		sublist.addField("custpage_item", "text", "Item").setDisplayType('inline');
		sublist.addField("custpage_pickgenqty", "text", "Pickgen Qty").setDisplayType('inline');
		sublist.addField("custpage_waveordernovalue", "text", "Order").setDisplayType('hidden');
		sublist.addField("custpage_waveordernolinevalue", "text", "Order").setDisplayType('hidden');
		sublist.addField("custpage_waveadjustfullfillqty", "checkbox", "Adjust Fulfillment Order qty");
		sublist.addField("custpage_taskid", "text", "Task Id").setDisplayType('hidden');
		sublist.addField("custpage_invref", "text", "Inv RefNo").setDisplayType('hidden');
		sublist.addField("custpage_fulfillno", "text", "FONo").setDisplayType('hidden');
		sublist.addField("custpage_kititem", "text", "kititem").setDisplayType('hidden');
		sublist.addField("custpage_kititemtype", "text", "kititemtype").setDisplayType('hidden');
		sublist.addField("custpage_contlp", "text", "ContLP").setDisplayType('hidden');
		sublist.addField("custpage_kititemtext", "text", "kititemtext").setDisplayType('hidden');
		sublist.addField("custpage_itemvalue", "text", "ItemValue").setDisplayType('hidden');
		var openTaskRecordsArray=new Array();	
		var waveno=parseFloat(request.getParameter('custpage_wavelist'));
		var Order=parseFloat(request.getParameter('custpage_orderlist'));
		var OrderLines=request.getParameter('custpage_orderlinelist');
		for(k=0;k<openTaskRecords.length;k++)
		{
			nlapiLogExecution('Debug', 'openTaskRecords[k]', openTaskRecords[k]); 
			var openTasksearchresult = openTaskRecords[k];

			if(openTasksearchresult!=null)
			{
				nlapiLogExecution('Debug', 'openTasksearchresult.length ', openTasksearchresult.length); 
				for(var j=0;j<openTasksearchresult.length;j++)
				{
					openTaskRecordsArray[openTaskRecordsArray.length]=openTasksearchresult[j];				
				}
			}
		}
		var test='';

		if(openTaskRecordsArray.length>0 && openTaskRecordsArray.length>100)
		{
			var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('hidden');
			pagesize.setDisplaySize(10,10);
			pagesize.setLayoutType('outsidebelow', 'startrow');
			var select= form.addField('custpage_selectpage','select', 'Select Records');	
			select.setLayoutType('outsidebelow', 'startrow');			
			select.setDisplaySize(200,30);
			if (request.getMethod() == 'GET'){
				pagesize.setDefaultValue("100");
				pagesizevalue=100;
			}
			else
			{
				if(request.getParameter('custpage_pagesize')!=null)
				{pagesizevalue= request.getParameter('custpage_pagesize');}
				else
				{pagesizevalue= 100;pagesize.setDefaultValue("100");}
			}
			//this is to add the pageno's to the dropdown.
			var len=openTaskRecordsArray.length/parseFloat(pagesizevalue);
			for(var k=1;k<=Math.ceil(len);k++)
			{

				var from;var to;

				to=parseFloat(k)*parseFloat(pagesizevalue);
				from=(parseFloat(to)-parseFloat(pagesizevalue))+1;

				if(parseFloat(to)>openTaskRecordsArray.length)
				{
					to=openTaskRecordsArray.length;
					test=from.toString()+","+to.toString(); 
				}

				var temp=from.toString()+" to "+to.toString();
				var tempto=from.toString()+","+to.toString();
				select.addSelectOption(tempto,temp);

			} 
			if (request.getMethod() == 'POST'){

				if(request.getParameter('custpage_selectpage')!=null ){

					select.setDefaultValue(request.getParameter('custpage_selectpage'));	

				}
				if(request.getParameter('custpage_pagesize')!=null ){

					pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	

				}
			}
		}
		else
		{
			pagesizevalue=parseFloat(openTaskRecordsArray.length);
		}
		var minval=0;var maxval=parseFloat(pagesizevalue);
		if(parseFloat(pagesizevalue)>openTaskRecordsArray.length)
		{
			maxval=openTaskRecordsArray.length;
		}
		//var selectno=request.getParameter('custpage_selectpage');
		if(openTaskRecordsArray.length>20)
			var selectno=request.getParameter('custpage_selectpage');
		else
			var selectno="1,"+openTaskRecordsArray.length;

		nlapiLogExecution('Debug', 'selectno ', selectno); 
		if(selectno!=null )
		{
			var selectedPage= request.getParameter('custpage_selectpage');
			var selectedPageArray=selectno.split(',');			
			var diff=parseFloat(selectedPageArray[1])-(parseFloat(selectedPageArray[0])-1);
			nlapiLogExecution('Debug', 'diff',diff);

			var pagevalue=request.getParameter('custpage_pagesize');
			nlapiLogExecution('Debug', 'pagevalue',pagevalue);
			if(pagevalue!=null)
			{

				if(parseFloat(diff)==parseFloat(pagevalue)|| test==selectno)
				{

					var selectedPageArray=selectno.split(',');	
					nlapiLogExecution('Debug', 'selectedPageArray.length ', selectedPageArray.length);  
					minval=parseFloat(selectedPageArray[0])-1;
					nlapiLogExecution('Debug', 'selectedPageArray[0] ', selectedPageArray[0]);  
					maxval=parseFloat(selectedPageArray[1]);
					nlapiLogExecution('Debug', 'selectedPageArray[1] ', selectedPageArray[1]);  
				}
			}
		}
		form.setScript('customscript_kitordwavecancellation_cl');

		var count=1;
		for (var i = minval; i < maxval; i++) 
		{
			var openTaskRecord = openTaskRecordsArray[i];
			if(openTaskRecord!=null && openTaskRecord!='')
			{
				ordWaveno=openTaskRecord.getValue('custrecord_ebiz_wave_no');
				ParentOrdNo=openTaskRecord.getText('custrecord_ebiz_order_no');
				fulfillordno=openTaskRecord.getValue('name');
				orderline=openTaskRecord.getValue('custrecord_line_no');
				item=openTaskRecord.getText('custrecord_sku');
				pickgenqty=openTaskRecord.getValue('custrecord_expe_qty');
				lineOrdNovalue=openTaskRecord.getValue('custrecord_ebiz_order_no');
				taskid=openTaskRecord.getId();
				var vInvRef=openTaskRecord.getValue('custrecord_invref_no');
				var vFOID=openTaskRecord.getValue('custrecord_ebiz_cntrl_no');
				var kititem=openTaskRecord.getValue('custrecord_parent_sku_no');
				var kititemtype=openTaskRecord.getValue('type','custrecord_parent_sku_no');
				var vContLP=openTaskRecord.getValue('custrecord_container_lp_no');
				var kititemText=openTaskRecord.getText('custrecord_parent_sku_no');
				var itemId=openTaskRecord.getValue('custrecord_sku');
				nlapiLogExecution('Debug', 'orderline', orderline);
				nlapiLogExecution('ERROR', 'kititemtype', kititemtype);
				nlapiLogExecution('ERROR', 'kititemText', kititemText);

//				if((waveno!=null && waveno!='') || selectno!=null)
//				{ 
				//prewaveno=waveno;

				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_waveno', count,ordWaveno);		
				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_orderno', count,ParentOrdNo);
				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_fulfillordno', count,fulfillordno);
				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_fulfillordlineno', count,orderline);
				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_item', count,item);
				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_pickgenqty', count,pickgenqty);		
				if((waveno!=null && waveno!='')||(OrderLines!=null && OrderLines!='') )
				{
					if((waveno!=null && waveno!=''))
					{
						form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_waveordernolinevalue', count,lineOrdNovalue);
					}

					form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_waveordernovalue', count,lineOrdNovalue);
				}

				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_taskid', count,taskid);
				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_invref', count,vInvRef);
				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_fulfillno', count,vFOID);
				if(vContLP != null && vContLP != '')
					form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_contlp', count,vContLP);
				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_kititem', count,kititem);
				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_kititemtext', count,kititemText);
				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_kititemtype', count,kititemtype);
				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_itemvalue', count,itemId);


				//}
//				if((Order!=null && Order!='' ) || selectno!=null)
//				{
//				//preordno=Order;
//				//precount=orderlinecount;
//				//var sublist = form.addSubList("custpage_ordercancellist", "list", "Order Cancellation List");

//				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_waveno', count,ordWaveno);		
//				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_orderno', count,ParentOrdNo);
//				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_fulfillordno', count,fulfillordno);
//				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_fulfillordlineno', count,orderline);
//				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_item', count,item);
//				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_pickgenqty', count,pickgenqty);
//				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_taskid', count,taskid);

//				//form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_ordtotalorderlines', i+1,orderlinecount.toString());
//				}
//				if((OrderLines!=null && OrderLines!='') || selectno!=null)
//				{
//				nlapiLogExecution('Debug', 'test1', 'test1');
//				nlapiLogExecution('Debug', 'ordWaveno', ordWaveno);
//				nlapiLogExecution('Debug', 'ParentOrdNo', ParentOrdNo);
//				nlapiLogExecution('Debug', 'fulfillordno', fulfillordno);
//				//var sublist1 = form.addSubList("custpage_orderlinecancellist", "list", "OrderLine Cancellation List");


//				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_waveno', count,ordWaveno);		
//				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_orderno', count,ParentOrdNo);
//				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_fulfillordno', count,fulfillordno);
//				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_fulfillordlineno', count,orderline);
//				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_waveordernovalue', count,lineOrdNovalue);
//				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_item', count,item);
//				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_pickgenqty', count,pickgenqty);
//				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_taskid', count,taskid);

//				}
				count=count+1;
			}
		}
	}
	//case # 20124398�Start
	else{
		form.getField('custpage_wavelist').setDisplayType('hidden');
		form.getField('custpage_orderlist').setDisplayType('hidden');
		form.getField('custpage_orderlinelist').setDisplayType('hidden');
		form.getField('custpage_cluster').setDisplayType('hidden');
	}
	//case # 20124398�End

}
function processWaveCancellation(waveNo,orderNo,orderlineNo,Adjustfullfillqty,fulfillordno,taskid,request){
	nlapiLogExecution('Debug', 'processWaveCancellation', 'Start');
	nlapiLogExecution('Debug', 'waveNo', waveNo);
	nlapiLogExecution('Debug', 'orderNo',orderNo);
	nlapiLogExecution('Debug', 'orderlineNo', orderlineNo);
	nlapiLogExecution('Debug', 'fulfillordno', fulfillordno);

	// Get all open task records associated with this wave
	var openTaskRecords = getOpenTaskRecordsForWavecancel(waveNo,fulfillordno,orderlineNo,taskid,request);//10 UNITS
	nlapiLogExecution('Debug', 'openTaskRecords', openTaskRecords.length);

	// Get all fulfillment order records for the selected wave no
	//var fOrdRecords = getFulfillOrderInfo(waveNo,orderNo,orderlineNo);

	// Get inventory adjustment details
	var invtDtls = getDtlsForInvtUpdate(openTaskRecords);

	// Get list of fulfillment orders to be updated
	//var fulfillOrderList = getFulfillOrderList(openTaskRecords);

	executeWaveCancel(openTaskRecords,invtDtls,Adjustfullfillqty,waveNo);

	nlapiLogExecution('Debug', 'processWaveCancellation', 'End');
}

/**
 * 
 * @param request
 * @param response
 */
function cancelWave(request, response){
	if(request.getMethod() == 'GET'){
		nlapiLogExecution('Debug', 'cancelWave - GET', 'Start');
		var form = nlapiCreateForm('Wave Cancellation');
		buildWaveCancellationForm(form, request, response);
		nlapiLogExecution('Debug', 'cancelWave - GET', 'End');
	} else if (request.getMethod() == 'POST'){
		nlapiLogExecution('Debug', 'cancelWave - POST', 'Start');
		try
		{
			var form = nlapiCreateForm('Wave Cancellation');

			var context = nlapiGetContext();
			var currentUserID = context.getUser();


			var userselection = false;
			var CancelFlag;
			var lincount = request.getLineItemCount('custpage_wavecancellist');
			var waveNo;
			var orderNo;
			var Adjustfullfillqty;
			var orderlineNo;
			var fulfillordno;
			var taskid;
			var boolProcessWavecancellation="F";
			var vTaskIntIdArr=new Array();
			var vAdjFulfillCheckArr=new Array();
			var vInvRefArr=new Array();
			var vFOIdArr=new Array();
			//allow user to cancel the waves only when CancelWave button is clicked.
			if(request.getParameter('custpage_hiddenfield')!=null && request.getParameter('custpage_hiddenfield')!='' && request.getParameter('custpage_hiddenfield')!='F')
			{		
				if(lincount!=null && lincount!='')
				{
					nlapiLogExecution('Debug', 'Remaining Usage 2', nlapiGetContext().getRemainingUsage());
					for (var p = 1; p <= lincount; p++) 
					{
						CancelFlag = request.getLineItemValue('custpage_wavecancellist', 'custpage_wavecancel', p);
						waveNo = request.getLineItemValue('custpage_wavecancellist', 'custpage_waveno', p);
						orderNo = request.getLineItemValue('custpage_wavecancellist', 'custpage_waveordernovalue', p);
						orderlineNo = request.getLineItemValue('custpage_wavecancellist', 'custpage_fulfillordlineno', p);
						fulfillordno = request.getLineItemValue('custpage_wavecancellist', 'custpage_fulfillordno', p);
						Adjustfullfillqty=request.getLineItemValue('custpage_wavecancellist', 'custpage_waveadjustfullfillqty', p);
						var vFOId=request.getLineItemValue('custpage_wavecancellist', 'custpage_fulfillno', p);
						taskid=request.getLineItemValue('custpage_wavecancellist', 'custpage_taskid', p);
						var vInvRef=request.getLineItemValue('custpage_wavecancellist', 'custpage_invref', p);
						var vContLP=request.getLineItemValue('custpage_wavecancellist', 'custpage_contlp', p);
						nlapiLogExecution('Debug', 'vInvRef', vInvRef);

						if (CancelFlag == 'T') 
						{
							userselection = true;	        	
						}
						nlapiLogExecution('Debug', 'moveFlag', CancelFlag);
						// Retrieve the wave no. that needs to be cancelled
						if (CancelFlag == 'T') {
							vTaskIntIdArr.push(taskid);
							vAdjFulfillCheckArr.push(Adjustfullfillqty);
							if(vInvRef!=null && vInvRef!='')
								vInvRefArr.push(vInvRef);
							vFOIdArr.push(vFOId);
							nlapiLogExecution('Debug', 'WaveNo selected for cancellation', waveNo);

							//processWaveCancellation(waveNo,orderNo,orderlineNo,Adjustfullfillqty,fulfillordno,taskid);
							//boolProcessWavecancellation="T";
						}
					}
					nlapiLogExecution('Debug', 'vInvRefArr', vInvRefArr);
					if(vTaskIntIdArr!= null && vTaskIntIdArr != '' && vTaskIntIdArr.length>0)
					{
						var vOpenTaskDets=GetAllOpenTaskRecs(vTaskIntIdArr,0);
						var vInvDets=GetAllInvtDetails(vInvRefArr,0);
						var vFODets=GetAllFODetails(vFOIdArr,0);
						if(vOpenTaskDets != null && vOpenTaskDets != '' && vOpenTaskDets.length>0)
						{
							// Get inventory adjustment details
							//var invtDtls = getDtlsForInvtUpdate(vOpenTaskDets,vInvRef);
							var context = nlapiGetContext();
							var currentUserID = context.getUser();	
							WaveCancelTransaction(vOpenTaskDets, vInvDets,vAdjFulfillCheckArr,vFODets,currentUserID,waveNo);
							boolProcessWavecancellation="T";
						}
					}	



					if(boolProcessWavecancellation=="T")
					{
						var msg = form.addField('custpage_messages', 'inlinehtml', null, null, null);
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Wave Cancelled Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
					}
					nlapiLogExecution('Debug', 'cancelWave - POST', 'End');
				}
			}
			buildWaveCancellationForm(form, request, response);
		}
		catch(exp) {
			nlapiLogExecution('Debug', 'Exception in Wave Cancelation ', exp);	
			showInlineMessage(form, 'Debug', 'Wave Cancellation Failed', "");
			response.writePage(form);
		}

	}

	response.writePage(form);
}
var searchResultArr=new Array();
function GetAllOpenTaskRecs(TaskIntIdArr,maxid){
	nlapiLogExecution('Debug', 'getOpenTaskRecordsForWavecancel', 'Start');
	//var ebizordno=request.getParameter('custpage_orderlist');

	nlapiLogExecution('Debug', 'taskid', TaskIntIdArr);
	// Filter open task records by wave number and wms_status_flag

	if(TaskIntIdArr != null && TaskIntIdArr != '' && TaskIntIdArr.length>0)
	{	
		//var ebizordno=request.getParameter('custpage_orderlist');
		var filters = new Array();

		filters.push(new nlobjSearchFilter('internalid', null, 'is', TaskIntIdArr));

		if(maxid!=0)
			filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxid));
		var columns = getOpenTaskColumns();

		// Retrieve all open task records for the selected wave
		var openTaskRecords = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
		if(openTaskRecords!=null && openTaskRecords.length>=1000)
		{ 
			for(var p=0;p<openTaskRecords.length;p++)
				searchResultArr.push(openTaskRecords); 
			var maxno=openTaskRecords[openTaskRecords.length-1].getValue(columns[30]);
			GetAllOpenTaskRecs(TaskIntIdArr,maxno);	
		}
		else
		{
			for(var p=0;p<openTaskRecords.length;p++)
				searchResultArr.push(openTaskRecords); 
		}
	}
	logCountMessage('Debug', openTaskRecords);
	nlapiLogExecution('Debug', 'openTaskRecords', openTaskRecords.length);
	nlapiLogExecution('Debug', 'getOpenTaskRecordsForWavecancel', 'End');

	return openTaskRecords;
}

function WaveCancelTransaction(openTaskRecords, InvtDetails,AdjustfullfillqtyArr,FODetails,emp,waveNo){
	nlapiLogExecution('Debug', 'removePickTasksAndUpdateInvt', 'Start');
	nlapiLogExecution('Debug', 'openTaskRecords.length', openTaskRecords.length);
	nlapiLogExecution('Debug', 'InvtDetails', InvtDetails);
	var usage = 0;
	var vFORecNew=new Array();
	var vInvRecNew=new Array();
	var vFORecIdNew=new Array();
	var vInvRecIdNew=new Array();
	var vInvExpQtyNew=new Array();
	var vFOExpQtyNew=new Array();
	var vFOAllocQtyNew=new Array();
	var vContLPMastArr=new Array();
	var vItemTextArr=new Array();
	if(openTaskRecords != null && openTaskRecords.length > 0){
		var otparent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent');
		var parentid = nlapiSubmitRecord(otparent); 
		var newParent = nlapiLoadRecord('customrecord_ebiz_throwaway_parent', parentid);
		for(var i = 0; i < openTaskRecords.length; i++){
			// Check if it is a PICK task
			nlapiLogExecution('Debug', 'TaskType', parseFloat(openTaskRecords[i].getValue('custrecord_tasktype')));
			if(parseFloat(openTaskRecords[i].getValue('custrecord_tasktype')) == parseFloat(3)){
				var task = openTaskRecords[i];
				var recordId = task.getId();
				var opentaskrecords = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recordId);
				var wmsstatus = opentaskrecords.getFieldValue('custrecord_wms_status_flag');
				//var invtRefNo = task.getValue('custrecord_invref_no');
				var expectedQty = task.getValue('custrecord_expe_qty');
				var fointrid = task.getValue('custrecord_ebiz_cntrl_no');
				nlapiLogExecution('Debug', 'expectedQty', expectedQty);
				//nlapiLogExecution('Debug', 'InvtDetails[0].getId()', InvtDetails[0].getId());
				var invtRefNo = task.getValue('custrecord_invref_no');
				var FORefNo = task.getValue('custrecord_ebiz_cntrl_no');
				var vContLP = task.getValue('custrecord_container_lp_no');
				var vItemText = task.getText('custrecord_sku');
				//var wmsstatus = task.getValue('custrecord_wms_status_flag');
				nlapiLogExecution('Debug', 'FORefNo', FORefNo);
				nlapiLogExecution('Debug', 'invtRefNo', invtRefNo);
				nlapiLogExecution('Debug', 'vContLP', vContLP);
				if(vContLP != null && vContLP != '')
				{
					if(vContLPMastArr.indexOf(vContLP) == -1)
					{ 	 
						vContLPMastArr.push(vContLP);
						vItemTextArr.push(vItemText);
					}
					else
					{
						var contLPItemExists=false;
						for(var z=0; z< vContLPMastArr.length; z++)
						{
							if(vContLPMastArr[z]==vContLP && vItemTextArr[z] == vItemText)
							{
								contLPItemExists=true;
								break;
							}	
						}	
						if(contLPItemExists==false)
						{
							vContLPMastArr.push(vContLP);
							vItemTextArr.push(vItemText);
						}	
					}	

				}	
				if(invtRefNo != null && invtRefNo != '' && wmsstatus!='8' && wmsstatus!='28')
				{
					var vInvRec=GetInvDetails(invtRefNo,InvtDetails);

					nlapiLogExecution('Debug', 'vInvRec', vInvRec);


					// Update the inventory to reduce the allocation quantity
					if(vInvRec!=null && vInvRec!='')
					{
						if(vInvRecIdNew.indexOf(invtRefNo) == -1)
						{
							vInvRecIdNew.push(invtRefNo);
							vInvRecNew.push(vInvRec);
							vInvExpQtyNew.push(expectedQty);
						}
						else
						{
							var vOldInvExpQty=vInvExpQtyNew[vInvRecIdNew.indexOf(invtRefNo)];
							if(vOldInvExpQty == null || vOldInvExpQty == '')
								vOldInvExpQty=0;
							vInvExpQtyNew[vInvRecIdNew.indexOf(invtRefNo)]=parseFloat(expectedQty) + parseFloat(vOldInvExpQty);
							//vInvExpQtyNew.push(parseFloat(expectedQty) + parseFloat(vOldInvExpQty));
						}
						//updateInventoryForTaskParent(vInvRec, expectedQty,emp,newParent);									// 4 UNITS
					}
				}
				if(FORefNo != null && FORefNo != '' && wmsstatus!='8' && wmsstatus!='28')
				{
					var vFORec=GetFODetails(FORefNo,FODetails);


					nlapiLogExecution('Debug', 'vFORec', vFORec);
					if(vFORec!=null && vFORec!='')
					{

						if(vFORecIdNew.indexOf(FORefNo) == -1)
						{
							vFORecIdNew.push(FORefNo);
							vFORecNew.push(vFORec);
							vFOExpQtyNew.push(expectedQty);

							vFOAllocQtyNew.push(AdjustfullfillqtyArr[i]);
						}
						else
						{
							var vOldFOExpQty=vFOExpQtyNew[vFORecIdNew.indexOf(FORefNo)];
							/*** The below code is merged from FactoryMation production account on 01Mar13 by santosh as part of Standard bundle***/
							// checking qty 
							if(vOldFOExpQty == null || vOldFOExpQty == '' || isNaN(vOldFOExpQty))
								vOldFOExpQty=0;
							/*** upto here ***/
							vFOExpQtyNew[vFORecIdNew.indexOf(FORefNo)]=parseFloat(expectedQty) + parseFloat(vOldFOExpQty);
							if(AdjustfullfillqtyArr[i]=='T')
								vFOAllocQtyNew[vFORecIdNew.indexOf(FORefNo)]='T';
						}

						//updateFOForTaskParent(vFORec, expectedQty,AdjustfullfillqtyArr[i],newParent);									// 4 UNITS
					}
				}
				// Delete the open task record for PICK task
				/*				if(recordId != null && recordId !='')
					UpdatePickTaskToDel(recordId);*/
				//var deletedId = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', recordId); 	// 4 UNITS

				//var unitsConsumed = getUnitsConsumed('nlapiDeleteRecord');
				//usage = parseFloat(usage) + parseFloat(unitsConsumed);
				//updateFulfillOrderRecords(fointrid,expectedQty,AdjustfullfillqtyArr[i],newParent); 					// 8 UNITS

				// Total 16 UNITS
			}
		}
		if(vInvRecIdNew != null && vInvRecIdNew != '')
		{	
			nlapiLogExecution('Debug', 'vInvRecIdNew', vInvRecIdNew);
			nlapiLogExecution('Debug', 'vInvExpQtyNew', vInvExpQtyNew);
			for(var t=0;t<vInvRecIdNew.length;t++)
			{
				if(vInvRecNew[t] != null)
					updateInventoryForTaskParent(vInvRecNew[t], vInvExpQtyNew[t],emp,newParent);	

			}
		}
		if(vFORecIdNew != null && vFORecIdNew != '')
		{	
			nlapiLogExecution('Debug', 'vFORecIdNew', vFORecIdNew);
			nlapiLogExecution('Debug', 'vFOExpQtyNew', vFOExpQtyNew);
			for(var s=0;s<vFORecIdNew.length;s++)
			{
				if(vFORecNew[s] != null)
					updateFOForTaskParent(vFORecNew[s], vFOExpQtyNew[s],vFOAllocQtyNew[s],newParent,waveNo);	
			}
		}
		nlapiLogExecution('Debug', 'vContLPMastArr', vContLPMastArr);
		if(vContLPMastArr != null && vContLPMastArr != '' && vItemTextArr != null && vItemTextArr != '')
		{
			for(var l=0;l<vContLPMastArr.length;l++)
			{
				var vTempLPNo=vContLPMastArr[l];
				var vTempItemText=vItemTextArr[l];
				if(vTempLPNo != "")
				{	
					var filters=new Array();
					filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_masterlp', null, 'is', vTempLPNo));
					filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_item', null, 'is', vTempItemText));
					//filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_sscc', null, 'isnotempty'));
					var columns = new Array();
					columns[0]=new nlobjSearchColumn('custrecord_ebiz_lpmaster_site');
					columns[1]=new nlobjSearchColumn('name');
					columns[2]=new nlobjSearchColumn('custrecord_ebiz_lpmaster_company');
					columns[3] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lptype');
					columns[4]=new nlobjSearchColumn('custrecord_ebiz_lpmaster_sizeid');
					columns[5] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
					columns[6] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totcube');
					columns[7] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_wmsstatusflag');
					columns[8] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sscc');
					columns[9] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_controlno');
					columns[10] = new nlobjSearchColumn('custrecord_ebiz_cart_closeflag');
					columns[11] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_item');
					columns[12] = new nlobjSearchColumn('custrecord_ebiz_lp_seq');
					var vLPMastRec = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
					if(vLPMastRec != null && vLPMastRec != '')
					{
						for(var h=0;h<vLPMastRec.length;h++)
							updateLPMastParent(vLPMastRec[h],newParent);
						nlapiLogExecution('Debug', 'Outof LP update', vLPMastRec);
					}	
				}
			}	
		}	
		nlapiSubmitRecord(newParent);
		for(var l = 0; l < openTaskRecords.length; l++){
			// Check if it is a PICK task
			nlapiLogExecution('Debug', 'TaskType', parseFloat(openTaskRecords[l].getValue('custrecord_tasktype')));
			var vPickLPNo=openTaskRecords[l].getValue('custrecord_container_lp_no');
			if(parseFloat(openTaskRecords[l].getValue('custrecord_tasktype')) == parseFloat(3)){
				var task = openTaskRecords[l];
				var recordId = task.getId();
				var opentaskrecords = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recordId);
				var wmsstatus = opentaskrecords.getFieldValue('custrecord_wms_status_flag');
				// Delete the open task record for PICK task
				if(recordId != null && recordId !='' && wmsstatus!='8' && wmsstatus!='28')
					UpdatePickTaskToDel(recordId);
				nlapiLogExecution('Debug', 'Deleted open task record', recordId);
			}
			/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh as part of Standard bundle, It is Lexjet specific to delete shipmanifest records, So we have commented this code, otherwise we need to make it as role based***/
			nlapiLogExecution('Debug', 'vPickLPNo', vPickLPNo);
			/*if(vPickLPNo != null)
			{
				// Deleting ShipManifest Record
				var smfilters = new Array();

				smfilters.push(new nlobjSearchFilter('custrecord_ship_ref5', null, 'is', vPickLPNo));

				var smcolumns = new Array();
				smcolumns[0] = new nlobjSearchColumn('custrecord_ship_ref5');
				smcolumns[0].setSort();

				var shipmanifesttasks = nlapiSearchRecord('customrecord_ship_manifest', null, smfilters, smcolumns);	
				nlapiLogExecution('Debug', 'shipmanifesttasks', shipmanifesttasks);
				if(shipmanifesttasks!=null && shipmanifesttasks!='' && shipmanifesttasks.length>0)
				{
					for(var k=0;k < shipmanifesttasks.length; k++)
					{
						nlapiLogExecution('Debug', 'Deleting Ship Manifest Record...');
						nlapiDeleteRecord('customrecord_ship_manifest', shipmanifesttasks[k].getId());
					}
				}
			}*/
			/*** Up to here ***/
		}
		updatelastpicktaskforoldcarton(vContLPMastArr,null);
	}
	nlapiLogExecution('Debug', 'Remaining Usage 2', nlapiGetContext().getRemainingUsage());
	//nlapiLogExecution('Debug', 'removePickTasksAndUpdateInvt|USAGE', usage);
	nlapiLogExecution('Debug', 'removePickTasksAndUpdateInvt', 'End');
}
function updateInventoryForTaskParent(vInvRec, expectedQty,emp,newParent){
	nlapiLogExecution('Debug', 'updateInventoryForWaveCancel', 'Start');
	nlapiLogExecution('Debug', 'invtRefNo', vInvRec);
	nlapiLogExecution('Debug', 'expectedQty', expectedQty);
	var inventoryRecord = null;
	try
	{
		//var allocationQty = vInvRec.getValue('custrecord_ebiz_alloc_qty'); 
		//	nlapiLogExecution('Debug', 'Inventory Allocation Qty[PREV]', allocationQty); 
		// Updating allocation qty
		/*if(parseFloat(allocationQty) <= 0){
			allocationQty = parseFloat(allocationQty) + parseFloat(expectedQty);
		} 
		else if(parseFloat(allocationQty) > 0){
			allocationQty = parseFloat(allocationQty) - parseFloat(expectedQty);
		}

		if(parseFloat(allocationQty) <= 0){
			allocationQty=0;
		}*/

		var scount=1;
		LABL1: for(var j=0;j<scount;j++)
		{	
			nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', j);
			try
			{
				var inventoryTransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', vInvRec.getId());
				var allocationQty = inventoryTransaction.getFieldValue('custrecord_ebiz_alloc_qty');
				if (isNaN(allocationQty))
				{
					allocationQty = 0;
				}
				if(parseInt(allocationQty) <= 0){
					allocationQty = parseInt(allocationQty) + parseInt(expectedQty);
				} 
				else if(parseInt(allocationQty) > 0){
					allocationQty = parseInt(allocationQty) - parseInt(expectedQty);
				}

				if(parseInt(allocationQty) <= 0){
					allocationQty=0;
				}
				//var totalallocqty=parseInt(allocationQuantity)+parseInt(alreadyallocqty);
				inventoryTransaction.setFieldValue('custrecord_ebiz_alloc_qty', allocationQty);
				inventoryTransaction.setFieldValue('custrecord_ebiz_callinv', 'N');
				inventoryTransaction.setFieldValue('custrecord_ebiz_displayfield', 'N');
				if(emp != null && emp != '')
					inventoryTransaction.setFieldValue('custrecord_updated_user_no', emp);					 
				else
					inventoryTransaction.setFieldValue('custrecord_updated_user_no', nlapiGetContext().getUser());

				nlapiSubmitRecord(inventoryTransaction,false,true);
			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 

				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				}				 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}


		/*//Commented as part of optimistic locking

	/*	newParent.selectNewLineItem('recmachcustrecord_ebiz_inv_parent');
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'id', vInvRec.getId());
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'name', vInvRec.getValue('name'));
		if(vInvRec.getValue('custrecord_ebiz_inv_binloc') != null && vInvRec.getValue('custrecord_ebiz_inv_binloc') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_binloc', vInvRec.getValue('custrecord_ebiz_inv_binloc'));

		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_invholdflg', vInvRec.getValue('custrecord_ebiz_invholdflg'));		
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_lp', vInvRec.getValue('custrecord_ebiz_inv_lp'));
		if(vInvRec.getValue('custrecord_ebiz_inv_sku') != null && vInvRec.getValue('custrecord_ebiz_inv_sku') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_sku', vInvRec.getValue('custrecord_ebiz_inv_sku'));
		if(vInvRec.getValue('custrecord_ebiz_inv_sku_status') != null && vInvRec.getValue('custrecord_ebiz_inv_sku_status') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_sku_status', vInvRec.getValue('custrecord_ebiz_inv_sku_status'));

		if(vInvRec.getValue('custrecord_ebiz_inv_packcode') != null && vInvRec.getValue('custrecord_ebiz_inv_packcode') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_packcode', vInvRec.getValue('custrecord_ebiz_inv_packcode'));
		else
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_packcode', '1');
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_qty', parseFloat(vInvRec.getValue('custrecord_ebiz_inv_qty')).toFixed(5));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_lot', vInvRec.getValue('custrecord_ebiz_inv_lot'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_fifo', vInvRec.getValue('custrecord_ebiz_inv_fifo'));
		if(vInvRec.getValue('custrecord_ebiz_inv_loc') != null && vInvRec.getValue('custrecord_ebiz_inv_loc') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_loc', vInvRec.getValue('custrecord_ebiz_inv_loc'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_alloc_qty', parseFloat(allocationQty).toFixed(5));
		if(vInvRec.getValue('custrecord_inv_ebizsku_no') != null && vInvRec.getValue('custrecord_inv_ebizsku_no') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_inv_ebizsku_no', vInvRec.getValue('custrecord_inv_ebizsku_no'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_invt_ebizlp', vInvRec.getValue('custrecord_invt_ebizlp'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_qoh', parseFloat(vInvRec.getValue('custrecord_ebiz_qoh')).toFixed(5));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_expdate', vInvRec.getValue('custrecord_ebiz_expdate'));
		if(vInvRec.getValue('custrecord_wms_inv_status_flag') != null && vInvRec.getValue('custrecord_wms_inv_status_flag') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_wms_inv_status_flag', vInvRec.getValue('custrecord_wms_inv_status_flag'));
		if(vInvRec.getValue('custrecord_invttasktype') != null && vInvRec.getValue('custrecord_invttasktype') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_invttasktype', vInvRec.getValue('custrecord_invttasktype'));
		if(vInvRec.getValue('custrecord_outboundinvlocgroupid') != null && vInvRec.getValue('custrecord_outboundinvlocgroupid') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_outboundinvlocgroupid', vInvRec.getValue('custrecord_outboundinvlocgroupid'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_pickseqno', vInvRec.getValue('custrecord_pickseqno'));
		if(vInvRec.getValue('custrecord_inboundinvlocgroupid') != null && vInvRec.getValue('custrecord_inboundinvlocgroupid') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_inboundinvlocgroupid', vInvRec.getValue('custrecord_inboundinvlocgroupid'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_putseqno', vInvRec.getValue('custrecord_putseqno'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_callinv', 'N');
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_avl_qty', parseFloat(vInvRec.getValue('custrecord_ebiz_avl_qty')).toFixed(5));
		if(vInvRec.getValue('custrecord_ebiz_transaction_no') != null && vInvRec.getValue('custrecord_ebiz_transaction_no') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_transaction_no', vInvRec.getValue('custrecord_ebiz_transaction_no'));
		//newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_serialnumbers', vInvRec.getValue('custrecord_ebiz_serialnumbers'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_putseqno', vInvRec.getValue('custrecord_putseqno'));
		if(emp != null && emp != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_updated_user_no', emp);
		else
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_updated_user_no', nlapiGetContext().getUser());

		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_serialnumbers', vInvRec.getValue('custrecord_ebiz_serialnumbers'));

		newParent.commitLineItem('recmachcustrecord_ebiz_inv_parent');*/
		nlapiLogExecution('Debug', 'updateInventoryForWaveCancel', 'End');

	}
	catch(exp) {
		nlapiLogExecution('Debug', 'Exception in Wave Cancelation Inv updation', exp);	
		//showInlineMessage(form, 'Debug', 'Wave Cancellation Failed', "");
		//response.writePage(form);
	}
}

var searchInvArr=new Array();
function GetAllInvtDetails(InvRefId,maxid){
	nlapiLogExecution('Debug', 'getInventoryRecordsForWavecancel', 'Start');
	//var ebizordno=request.getParameter('custpage_orderlist');

	nlapiLogExecution('Debug', 'InvRefId', InvRefId);
	// Filter open task records by wave number and wms_status_flag

	if(InvRefId != null && InvRefId != '' && InvRefId.length>0)
	{	
		for(var cc=0;cc<InvRefId.length;cc++)
		{
			nlapiLogExecution('Debug', 'InvRefId', InvRefId[cc]);
		}
		//var ebizordno=request.getParameter('custpage_orderlist');
		var filters = new Array();

		//filters.push(new nlobjSearchFilter('internalid', null, 'is', InvRefId));
		filters.push(new nlobjSearchFilter('internalid', null, 'anyof', InvRefId));
		if(maxid!=0)
			filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxid));
		var columns = new Array();
		columns.push(new nlobjSearchColumn('id'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_alloc_qty'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_binloc'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_lp'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_sku'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_sku_status'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_packcode'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_qty'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_lot'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_fifo'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_loc'));
		columns.push(new nlobjSearchColumn('custrecord_inv_ebizsku_no'));
		columns.push(new nlobjSearchColumn('custrecord_invt_ebizlp'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_qoh'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_expdate'));
		columns.push(new nlobjSearchColumn('custrecord_wms_inv_status_flag'));
		columns.push(new nlobjSearchColumn('custrecord_invttasktype'));
		columns.push(new nlobjSearchColumn('custrecord_outboundinvlocgroupid'));
		columns.push(new nlobjSearchColumn('custrecord_pickseqno'));
		columns.push(new nlobjSearchColumn('custrecord_inboundinvlocgroupid'));
		columns.push(new nlobjSearchColumn('custrecord_putseqno'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_avl_qty'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_transaction_no'));
		//columns.push(new nlobjSearchColumn('custrecord_ebiz_serialnumbers'));
		columns.push(new nlobjSearchColumn('name'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_invholdflg'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_serialnumbers'));
		// Retrieve all open task records for the selected wave
		var InvRecs = new nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
		if(InvRecs!=null && InvRecs.length>=1000)
		{ 
			for(var p=0;p<InvRecs.length;p++)
				searchInvArr.push(InvRecs[p]); 
			var maxno=InvRecs[InvRecs.length-1].getValue(columns[0]);
			GetAllInvtDetails(InvRefId,maxno);	
		}
		else
		{
			for(var p=0;p<InvRecs.length;p++)
				searchInvArr.push(InvRecs[p]); 
		}
	}
	logCountMessage('Debug', searchInvArr);
	nlapiLogExecution('Debug', 'InvRecords', searchInvArr.length);
	nlapiLogExecution('Debug', 'getInventoryRecordsForWavecancel', 'End');

	return searchInvArr;
}
function GetInvDetails(invtRefNo,InvtDetails)
{
	nlapiLogExecution('Debug', 'invtRefNo', invtRefNo);
	for(var p=0;p<InvtDetails.length;p++)
	{
		if(InvtDetails[p].getId()==invtRefNo)
		{
			nlapiLogExecution('Debug', 'InvtDetails[p].getId()', InvtDetails[p].getId());
			return InvtDetails[p];
			nlapiLogExecution('Debug', 'InvtDetails[p]', InvtDetails[p]);
		}
	}
	/*nlapiLogExecution('Debug', 'flag', flag);
	if(flag=="F")
	return null;*/
}
function UpdatePickTaskToDel(recordId)
{
	var deletedId = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', recordId); 	// 4 UNITS
	nlapiLogExecution('Debug', 'Deleting open task record1', recordId);

}

var searchFOArr=new Array();
function GetAllFODetails(FORefId,maxid){
	nlapiLogExecution('Debug', 'getFulfillmentRecordsForWavecancel', 'Start');
	//var ebizordno=request.getParameter('custpage_orderlist');

	nlapiLogExecution('Debug', 'FORefId', FORefId);
	// Filter open task records by wave number and wms_status_flag

	if(FORefId != null && FORefId != '' && FORefId.length>0)
	{	
		//var ebizordno=request.getParameter('custpage_orderlist');
		var filters = new Array();

		filters.push(new nlobjSearchFilter('internalid', null, 'is', FORefId));
		if(maxid!=0)
			filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxid));
		var columns = new Array();
		columns.push(new nlobjSearchColumn('id'));
		columns.push(new nlobjSearchColumn('name'));
		columns.push(new nlobjSearchColumn('custrecord_base_uom_id'));
		columns.push(new nlobjSearchColumn('custrecord_base_ord_qty'));
		columns.push(new nlobjSearchColumn('custrecord_base_pick_qty'));
		columns.push(new nlobjSearchColumn('custrecord_base_pickgen_qty'));
		columns.push(new nlobjSearchColumn('custrecord_base_ship_qty'));
		columns.push(new nlobjSearchColumn('custrecord_batch_flag'));
		columns.push(new nlobjSearchColumn('custrecord_batch'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_lineord'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_linesku'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_wave'));
		columns.push(new nlobjSearchColumn('custrecord_lineord'));
		columns.push(new nlobjSearchColumn('custrecord_ordline'));
		columns.push(new nlobjSearchColumn('custrecord_ord_qty'));
		columns.push(new nlobjSearchColumn('custrecord_linepackcode'));
		columns.push(new nlobjSearchColumn('custrecord_pickgen_flag'));
		columns.push(new nlobjSearchColumn('custrecord_pickgen_qty'));
		columns.push(new nlobjSearchColumn('custrecord_pickqty'));
		columns.push(new nlobjSearchColumn('custrecord_ship_qty'));
		columns.push(new nlobjSearchColumn('custrecord_linesku'));
		columns.push(new nlobjSearchColumn('custrecord_linesku_status'));
		columns.push(new nlobjSearchColumn('custrecord_linestatus_flag'));
		columns.push(new nlobjSearchColumn('custrecord_lineuom_id'));
		columns.push(new nlobjSearchColumn('custrecord_back_order_flag'));
		columns.push(new nlobjSearchColumn('custrecord_serial'));
		columns.push(new nlobjSearchColumn('custrecord_invoiced_qty'));
		columns.push(new nlobjSearchColumn('custrecord_wave_status_flag'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_lp'));
		columns.push(new nlobjSearchColumn('custrecord_ns_ord'));
		columns.push(new nlobjSearchColumn('custrecord_do_order_priority'));
		columns.push(new nlobjSearchColumn('custrecord_do_order_type'));
		columns.push(new nlobjSearchColumn('custrecord_do_customer'));
		columns.push(new nlobjSearchColumn('custrecord_do_carrier'));
		columns.push(new nlobjSearchColumn('custrecord_do_item_family'));
		columns.push(new nlobjSearchColumn('custrecord_do_item_group'));
		columns.push(new nlobjSearchColumn('custrecord_fulfilmentiteminfo1'));
		columns.push(new nlobjSearchColumn('custrecord_fulfilmentiteminfo2'));
		columns.push(new nlobjSearchColumn('custrecord_fulfilmentiteminfo3'));
		columns.push(new nlobjSearchColumn('custrecord_ordline_wms_location'));
		columns.push(new nlobjSearchColumn('custrecord_ordline_company'));
		columns.push(new nlobjSearchColumn('custrecord_shipment_no'));
		columns.push(new nlobjSearchColumn('custrecord_do_wmscarrier'));
		columns.push(new nlobjSearchColumn('custrecord_printflag'));
		columns.push(new nlobjSearchColumn('custrecord_print_count'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipdate'));		
		columns.push(new nlobjSearchColumn('custrecord_ebiz_orderdate'));
		columns.push(new nlobjSearchColumn('custrecord_shipcomplete'));
		//columns.push(new nlobjSearchColumn('custrecord_ordline_pickmethod'));
		//columns.push(new nlobjSearchColumn('custrecord_ordline_pickstrategy'));
		columns.push(new nlobjSearchColumn('custrecord_nsconfirm_ref_no'));
		columns.push(new nlobjSearchColumn('custrecord_linenotes1'));
		columns.push(new nlobjSearchColumn('custrecord_linenotes2'));

		// Retrieve all open task records for the selected wave
		var FORecs = new nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
		if(FORecs!=null && FORecs.length>=1000)
		{ 
			for(var p=0;p<FORecs.length;p++)
				searchFOArr.push(FORecs[p]); 
			var maxno=FORecs[FORecs.length-1].getValue(columns[0]);
			GetAllFODetails(FORefId,maxno);	
		}
		else
		{
			for(var p=0;p<FORecs.length;p++)
				searchFOArr.push(FORecs[p]); 
		}
	}
	logCountMessage('Debug', searchFOArr);
	nlapiLogExecution('Debug', 'Fulfillment Results', searchFOArr.length);
	nlapiLogExecution('Debug', 'getFulfillmentRecordsForWavecancel', 'End');

	return searchFOArr;
}
function GetFODetails(FORefNo,FODetails)
{
	for(var p=0;p<FODetails.length;p++)
	{
		if(FODetails[p].getId()==FORefNo)
			return FODetails[p];
	}	
	return FODetails;
}

function updateFOForTaskParent(vFORec, expectedQty,Adjustfullfillqty,newParent,waveNo){
	nlapiLogExecution('Debug', 'updateFOForTaskParent', 'Start');
	nlapiLogExecution('Debug', 'Adjustfullfillqty', Adjustfullfillqty);
	nlapiLogExecution('Debug', 'expectedQty', expectedQty);
	nlapiLogExecution('Debug', 'fointridRec', vFORec);
	nlapiLogExecution('Debug', 'waveNo', waveNo);


	try
	{
		if(vFORec != null && vFORec != '')
		{
			if(Adjustfullfillqty!='T')
			{ 	 
				var pickgenqty = vFORec.getValue('custrecord_pickgen_qty');
				var pickconfirmqty=vFORec.getValue('custrecord_pickqty');
				var shipqty=vFORec.getValue('custrecord_ship_qty');
				var fowaveno=vFORec.getValue('custrecord_ebiz_wave');
				var vOrderqty=vFORec.getValue('custrecord_ord_qty');
				nlapiLogExecution('Debug', 'fowaveno', fowaveno);
				nlapiLogExecution('Debug', 'vOrderqty', vOrderqty);

				if(fowaveno==null || fowaveno=='' || parseInt(waveNo)==parseInt(fowaveno))
				{
					nlapiLogExecution('Debug', 'Wave # matched');

					var fointrid=vFORec.getId();

					if(pickgenqty==null || pickgenqty=='' || isNaN(pickgenqty))
						pickgenqty=0;	

					if(shipqty==null || shipqty=='' || isNaN(shipqty))
						shipqty=0;

					if(pickconfirmqty==null || pickconfirmqty=='' || isNaN(pickconfirmqty))
						pickconfirmqty=0;

					nlapiLogExecution('Debug', 'fointrid', fointrid);
					nlapiLogExecution('Debug', 'total pickgenQty', pickgenqty);
					nlapiLogExecution('Debug', 'pickconfirmqty', pickconfirmqty);
					nlapiLogExecution('Debug', 'shipqty', shipqty);
					/*** The below code is merged from FactoryMation production account on 01Mar13 by santosh as part of Standard bundle***/
					// assigning expectedQty =0 if expectedQty is empty/null
					if(expectedQty==null || expectedQty=='' || isNaN(expectedQty))
						expectedQty=0;
					/*** upto here ***/
					var rempickgenqty =  parseFloat(pickgenqty) - parseFloat(expectedQty);	

					if(parseFloat(rempickgenqty)<=parseFloat(pickconfirmqty))
						rempickgenqty=pickconfirmqty;

					if(parseFloat(rempickgenqty)<=0)
						rempickgenqty=0;
					nlapiLogExecution('Debug', 'rempickgenqty', parseFloat(rempickgenqty));
					newParent.selectNewLineItem('recmachcustrecord_ebiz_fo_parent');
					newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'id', fointrid);
					newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_pickgen_qty', parseFloat(rempickgenqty).toFixed(5));
					//newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_linestatus_flag', '25');
					if(parseFloat(rempickgenqty)>0)
					{
						nlapiLogExecution('Debug', 'parseFloat(rempickgenqty)', parseFloat(rempickgenqty));
						nlapiLogExecution('Debug', 'parseInt(shipqty)', parseInt(shipqty));
						nlapiLogExecution('Debug', 'parseInt(pickconfirmqty)', parseInt(pickconfirmqty));
						nlapiLogExecution('Debug', 'parseInt(vOrderqty)', parseInt(vOrderqty));

						if(parseInt(shipqty)!= 0 && parseInt(shipqty)<parseInt(vOrderqty))
						{
							nlapiLogExecution('Debug', 'PARTIAL SHIP', vFORec.getValue('custrecord_linestatus_flag'));
							newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_linestatus_flag', '13');
						}
						else
						{
							if(parseInt(pickconfirmqty)!= 0 && parseInt(pickconfirmqty)<parseInt(vOrderqty))
							{
								nlapiLogExecution('Debug', 'PARTIAL PICK', vFORec.getValue('custrecord_linestatus_flag'));
								newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_linestatus_flag', '11');
							}
						}
					}
					else
					{
						newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_linestatus_flag', '25');
					}
					newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_ebiz_wave', '');
					newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'name', vFORec.getValue('name'));
					newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_ebiz_lineord', vFORec.getValue('custrecord_ebiz_lineord'));
					newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_ordline', vFORec.getValue('custrecord_ordline'));
					newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_ord_qty', parseFloat(vFORec.getValue('custrecord_ord_qty')).toFixed(5));
					newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_lineord', vFORec.getValue('custrecord_lineord'));				
					if(vFORec.getValue('custrecord_linepackcode') != null && vFORec.getValue('custrecord_linepackcode') != '')
						newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_linepackcode', vFORec.getValue('custrecord_linepackcode'));
					else
						newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_linepackcode', '1');
					if(vFORec.getValue('custrecord_do_carrier') != null && vFORec.getValue('custrecord_do_carrier') != '')
						newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_do_carrier', vFORec.getValue('custrecord_do_carrier'));
					if(vFORec.getValue('custrecord_ns_ord') != null && vFORec.getValue('custrecord_ns_ord') != '')
						newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_ns_ord', vFORec.getValue('custrecord_ns_ord'));
					if(vFORec.getValue('custrecord_do_order_type') != null && vFORec.getValue('custrecord_do_order_type') != '')
						newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_do_order_type', vFORec.getValue('custrecord_do_order_type'));
					//newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_ebiz_wave', vFORec.getValue('custrecord_ebiz_wave')); // case# 201416182
					newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_shipcomplete', vFORec.getValue('custrecord_shipcomplete'));


					/*newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_batch', vFORec.getValue('custrecord_batch'));

				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_ebiz_linesku', vFORec.getValue('custrecord_ebiz_linesku'));


				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_linenotes1', vFORec.getValue('custrecord_linenotes1'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_linenotes2', vFORec.getValue('custrecord_linenotes2'));


				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_pickgen_flag', vFORec.getValue('custrecord_pickgen_flag'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_pickgen_qty', vFORec.getValue('custrecord_pickgen_qty'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_pickqty', vFORec.getValue('custrecord_pickqty'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_ship_qty', vFORec.getValue('custrecord_ship_qty'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_linesku', vFORec.getValue('custrecord_linesku'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_linesku_status', vFORec.getValue('custrecord_linesku_status'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_linestatus_flag', vFORec.getValue('custrecord_linestatus_flag'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_lineuom_id', vFORec.getValue('custrecord_lineuom_id'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_back_order_flag', vFORec.getValue('custrecord_back_order_flag'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_ebiz_lp', vFORec.getValue('custrecord_ebiz_lp'));

				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_do_order_priority', vFORec.getValue('custrecord_do_order_priority'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_do_order_type', vFORec.getValue('custrecord_do_order_type'));

				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_do_item_family', vFORec.getValue('custrecord_do_item_family'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_do_item_group', vFORec.getValue('custrecord_do_item_group'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_fulfilmentiteminfo1', vFORec.getValue('custrecord_fulfilmentiteminfo1'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_fulfilmentiteminfo2', vFORec.getValue('custrecord_fulfilmentiteminfo2'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_fulfilmentiteminfo3', vFORec.getValue('custrecord_fulfilmentiteminfo3'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_ordline_wms_location', vFORec.getValue('custrecord_ordline_wms_location'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_ordline_company', vFORec.getValue('custrecord_ordline_company'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_shipment_no', vFORec.getValue('custrecord_shipment_no'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_do_wmscarrier', vFORec.getValue('custrecord_do_wmscarrier'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_printflag', vFORec.getValue('custrecord_printflag'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_print_count', vFORec.getValue('custrecord_print_count'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_ebiz_shipdate', vFORec.getValue('custrecord_ebiz_shipdate'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_ebiz_orderdate', vFORec.getValue('custrecord_ebiz_orderdate'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_shipcomplete', vFORec.getValue('custrecord_shipcomplete'));
				//newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_ordline_pickmethod', vFORec.getValue('custrecord_ordline_pickmethod'));
				//newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_ordline_pickstrategy', vFORec.getValue('custrecord_ordline_pickstrategy'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_nsconfirm_ref_no', vFORec.getValue('custrecord_nsconfirm_ref_no'));
					 */
					newParent.commitLineItem('recmachcustrecord_ebiz_fo_parent');
				}
			}
			else
			{
				nlapiLogExecution('Debug', 'Into Deletion of Fulfillment Order', 'End');
				var pickgenqty = vFORec.getValue('custrecord_pickgen_qty');
				var pickconfirmqty=vFORec.getValue('custrecord_pickqty');
				var shipqty=vFORec.getValue('custrecord_ship_qty');
				var fowaveno=vFORec.getValue('custrecord_ebiz_wave');
				var vOrdqty=vFORec.getValue('custrecord_ord_qty');
				var vLineStatus=vFORec.getValue('custrecord_ord_qty')

				nlapiLogExecution('Debug', 'fowaveno', fowaveno);
				nlapiLogExecution('Debug', 'vOrdqty', vOrdqty);
				nlapiLogExecution('Debug', 'vLineStatus', vLineStatus);

				if(fowaveno==null || fowaveno=='' || parseInt(waveNo)==parseInt(fowaveno))
				{					
					nlapiLogExecution('Debug', 'Wave # matched');

					var fointrid=vFORec.getId(); 

					if(pickgenqty==null || pickgenqty=='' || isNaN(pickgenqty))
						pickgenqty=0;	

					if(shipqty==null || shipqty=='' || isNaN(shipqty))
						shipqty=0;

					if(pickconfirmqty==null || pickconfirmqty=='' || isNaN(pickconfirmqty))
						pickconfirmqty=0;

					nlapiLogExecution('Debug', 'fointrid', fointrid);
					nlapiLogExecution('Debug', 'total pickgenQty', pickgenqty);
					nlapiLogExecution('Debug', 'pickconfirmqty', pickconfirmqty);
					nlapiLogExecution('Debug', 'shipqty', shipqty);

					var rempickgenqty =  parseFloat(pickgenqty) - parseFloat(expectedQty);

					if(parseFloat(rempickgenqty)<=parseFloat(pickconfirmqty))
						rempickgenqty=pickconfirmqty;

					if(parseFloat(rempickgenqty)<=0)
						rempickgenqty=0;

					nlapiLogExecution('Debug', 'rempickgenqty', rempickgenqty);

					if(rempickgenqty=='0' || rempickgenqty==null)
					{
						nlapiLogExecution('Debug', 'Deleting fullfillment  record', fointrid);
						var deletedId = nlapiDeleteRecord('customrecord_ebiznet_ordline', fointrid); // 4 UNITS

					}
					else if(parseFloat(rempickgenqty) >0)
					{						
						try
						{
							var fieldNames = new Array(); 
							var newValues = new Array(); 

							fieldNames.push('custrecord_pickgen_qty'); 
							newValues.push(rempickgenqty);

							//if(parseInt(shipqty)<parseInt(vOrdqty))
							if(parseInt(shipqty)!= 0 && parseInt(shipqty)<parseInt(vOrdqty))
							{
								fieldNames.push('custrecord_linestatus_flag'); 							
								newValues.push('13');
							}
							else if(parseInt(pickconfirmqty)!= 0 && parseInt(pickconfirmqty)<parseInt(vOrdqty))
							{
								fieldNames.push('custrecord_linestatus_flag'); 							
								newValues.push('11');
							}
							else
							{
								fieldNames.push('custrecord_linestatus_flag'); 							
								newValues.push('25');
							}

							nlapiSubmitField('customrecord_ebiznet_ordline', fointrid, fieldNames, newValues);	
						}
						catch(exp1)
						{
							nlapiLogExecution('Debug', 'exception exp1', exp1);
						}

					}
				}

			}
			nlapiLogExecution('Debug', 'updateFOForTaskParent', 'End');
		} 
	}
	catch(exp) {
		nlapiLogExecution('Debug', 'Exception in Wave Cancelation Inv updation', exp);	
		//showInlineMessage(form, 'Debug', 'Wave Cancellation Failed', "");
		//response.writePage(form);
	}


}


//For schedule script

/**
 * 
 * @param request
 * @param response
 */
function cancelWaveSchedule(request, response){
	if(request.getMethod() == 'GET'){
		nlapiLogExecution('Debug', 'cancelWave - GET', 'Start');
		var form = nlapiCreateForm('Wave Cancellation');
		buildWaveCancellationForm(form, request, response);
		nlapiLogExecution('Debug', 'cancelWave - GET', 'End');
	} else if (request.getMethod() == 'POST'){
		nlapiLogExecution('Debug', 'cancelWave - POST', 'Start');
		try
		{
			var form = nlapiCreateForm('Wave Cancellation');

			var context = nlapiGetContext();
			var currentUserID = context.getUser();


			var userselection = false;
			var CancelFlag;
			var lincount = request.getLineItemCount('custpage_wavecancellist');
			var waveNo;
			var orderNo;
			var Adjustfullfillqty;
			var orderlineNo;
			var fulfillordno;
			var taskid;
			var boolProcessWavecancellation="F";
			var vTaskIntIdArr;
			var vAdjFulfillCheckArr;
			var vInvRefArr;
			var vFOIdArr;
			//allow user to cancel the waves only when CancelWave button is clicked.
			if(request.getParameter('custpage_hiddenfield')!=null && request.getParameter('custpage_hiddenfield')!='' && request.getParameter('custpage_hiddenfield')!='F')
			{		
				if(lincount!=null && lincount!='')
				{
					nlapiLogExecution('Debug', 'Remaining Usage 2', nlapiGetContext().getRemainingUsage());
					for (var p = 1; p <= lincount; p++) 
					{
						CancelFlag = request.getLineItemValue('custpage_wavecancellist', 'custpage_wavecancel', p);
						waveNo = request.getLineItemValue('custpage_wavecancellist', 'custpage_waveno', p);
						orderNo = request.getLineItemValue('custpage_wavecancellist', 'custpage_waveordernovalue', p);
						orderlineNo = request.getLineItemValue('custpage_wavecancellist', 'custpage_fulfillordlineno', p);
						fulfillordno = request.getLineItemValue('custpage_wavecancellist', 'custpage_fulfillordno', p);
						Adjustfullfillqty=request.getLineItemValue('custpage_wavecancellist', 'custpage_waveadjustfullfillqty', p);
						var vFOId=request.getLineItemValue('custpage_wavecancellist', 'custpage_fulfillno', p);
						taskid=request.getLineItemValue('custpage_wavecancellist', 'custpage_taskid', p);
						var vInvRef=request.getLineItemValue('custpage_wavecancellist', 'custpage_invref', p);
						var vContLP=request.getLineItemValue('custpage_wavecancellist', 'custpage_contlp', p);

						if (CancelFlag == 'T') 
						{
							userselection = true;	        	
						}
						nlapiLogExecution('Debug', 'moveFlag', CancelFlag);
						// Retrieve the wave no. that needs to be cancelled
						if (CancelFlag == 'T') {
							if(vTaskIntIdArr == null || vTaskIntIdArr =='')
								vTaskIntIdArr=taskid;
							else
								vTaskIntIdArr= vTaskIntIdArr + ',' + taskid;

							if(vAdjFulfillCheckArr == null || vAdjFulfillCheckArr =='')
								vAdjFulfillCheckArr=Adjustfullfillqty;
							else
								vAdjFulfillCheckArr= vAdjFulfillCheckArr + ',' + Adjustfullfillqty;

							if(vInvRefArr == null || vInvRefArr =='')
								vInvRefArr=vInvRef;
							else
								vInvRefArr= vInvRefArr + ',' + vInvRef;

							if(vFOIdArr == null || vFOIdArr =='')
								vFOIdArr=vFOId;
							else
								vFOIdArr= vFOIdArr + ',' + vFOId;


							nlapiLogExecution('Debug', 'WaveNo selected for cancellation', waveNo);

							//processWaveCancellation(waveNo,orderNo,orderlineNo,Adjustfullfillqty,fulfillordno,taskid);
							//boolProcessWavecancellation="T";
						}
					}
					if(vTaskIntIdArr!= null && vTaskIntIdArr != '' && vTaskIntIdArr.length>0)
					{
						var context = nlapiGetContext();
						var currentUserID = context.getUser();
						//invoke a sched script to replenish the current user�s pool of blank records
						var param = new Array();


						param['custscript_ebiz_wavecancel_emp'] = currentUserID;
						param['custscript_ebiz_wavecancel_opentask'] = vTaskIntIdArr;
						param['custscript_ebiz_wavecancel_adjufulfill'] = vAdjFulfillCheckArr;
						param['custscript_ebiz_wavecancel_inv'] = vInvRefArr;
						param['custscript_ebiz_wavecancel_fulfill'] = vFOIdArr;
						nlapiLogExecution('Debug', 'Open task Params for scheduler ', vTaskIntIdArr);
						nlapiScheduleScript('customscript_ebiz_wavecancel_schedule', null,param);

						/*var vOpenTaskDets=GetAllOpenTaskRecs(vTaskIntIdArr,0);
						var vInvDets=GetAllInvtDetails(vInvRefArr,0);
						var vFODets=GetAllFODetails(vFOIdArr,0);
						if(vOpenTaskDets != null && vOpenTaskDets != '' && vOpenTaskDets.length>0)
						{
							// Get inventory adjustment details
							//var invtDtls = getDtlsForInvtUpdate(vOpenTaskDets,vInvRef);
							WaveCancelTransaction(vOpenTaskDets, vInvDets,vAdjFulfillCheckArr,vFODets);
							boolProcessWavecancellation="T";
						}*/
						boolProcessWavecancellation="T";
					}	



					if(boolProcessWavecancellation=="T")
					{
						var msg = form.addField('custpage_messages', 'inlinehtml', null, null, null);
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Wave Cancelled Successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
					}
					nlapiLogExecution('Debug', 'cancelWave - POST', 'End');
				}
			}
			//buildWaveCancellationForm(form, request, response);
			response.writePage(form);
		}
		catch(exp) {
			nlapiLogExecution('Debug', 'Exception in Wave Cancelation ', exp);	
			showInlineMessage(form, 'Debug', 'Wave Cancellation Failed', "");
			response.writePage(form);
		}

	}

	response.writePage(form);
}

function WaveCancelScript(type)
{
	nlapiLogExecution('Debug','Into  Wave Cancellation Scheduler Script','');

	var context = nlapiGetContext();  
	var emp = context.getSetting('SCRIPT', 'custscript_ebiz_wavecancel_emp');
	var vTaskIntIdArr1 = context.getSetting('SCRIPT', 'custscript_ebiz_wavecancel_opentask');
	var vAdjFulfillCheckArr1 = context.getSetting('SCRIPT', 'custscript_ebiz_wavecancel_adjufulfill');
	var vInvRefArr1 = context.getSetting('SCRIPT', 'custscript_ebiz_wavecancel_inv');
	var vFOIdArr1 = context.getSetting('SCRIPT', 'custscript_ebiz_wavecancel_fulfill');


	var str = 'emp. = ' + emp + '<br>';
	str = str + 'vOpenTaskIntIdArr. = ' + vTaskIntIdArr1 + '<br>';
	str = str + 'vAdjyFulfillIntIdArr. = ' + vAdjFulfillCheckArr1 + '<br>';
	str = str + 'vInvRefArr. = ' + vInvRefArr1 + '<br>';
	str = str + 'vFOIdArr. = ' + vFOIdArr1 + '<br>';
	str = str + 'type. = ' + type + '<br>';	

	nlapiLogExecution('Debug', 'Parameter Details', str);

	nlapiLogExecution('Debug','Remaining Usage 1',context.getRemainingUsage());
	var vTaskIntIdArr=new Array();
	var vAdjFulfillCheckArr=new Array();
	var vInvRefArr=new Array();
	var vFOIdArr=new Array();
	if(vTaskIntIdArr1!= null && vTaskIntIdArr1 != '' && vTaskIntIdArr1.length>0)
	{
		vTaskIntIdArr=vTaskIntIdArr1.split(',');
		vAdjFulfillCheckArr=vAdjFulfillCheckArr1.split(',');
		if(vInvRefArr1 != null && vInvRefArr1 != '')
			vInvRefArr=vInvRefArr1.split(',');
		vFOIdArr=vFOIdArr1.split(',');
		nlapiLogExecution('Debug', 'vTaskIntIdArr.length', vTaskIntIdArr.length);
		nlapiLogExecution('Debug', 'vAdjFulfillCheckArr.length', vAdjFulfillCheckArr.length);
		nlapiLogExecution('Debug', 'vInvRefArr.length', vInvRefArr.length);
		nlapiLogExecution('Debug', 'vFOIdArr.length', vFOIdArr.length);

	}
	if(vTaskIntIdArr!= null && vTaskIntIdArr != '' && vTaskIntIdArr.length>0)
	{
		var vOpenTaskDets=GetAllOpenTaskRecs(vTaskIntIdArr,0);
		var vInvDets=new Array();
		if(vInvRefArr != null && vInvRefArr != '')
			vInvDets=GetAllInvtDetails(vInvRefArr,0);
		var vFODets=GetAllFODetails(vFOIdArr,0);
		if(vOpenTaskDets != null && vOpenTaskDets != '' && vOpenTaskDets.length>0)
		{
			// Get inventory adjustment details
			//var invtDtls = getDtlsForInvtUpdate(vOpenTaskDets,vInvRef);
			WaveCancelTransaction(vOpenTaskDets, vInvDets,vAdjFulfillCheckArr,vFODets,emp);
			boolProcessWavecancellation="T";
		}
	}

}

if (!Array.prototype.indexOf) {
	Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
		"use strict";

		if (this === void 0 || this === null) throw new TypeError();

		var t = Object(this);
		var len = t.length >>> 0;
		if (len === 0) return -1;

		var n = 0;
		if (arguments.length > 0) {
			n = Number(arguments[1]);
			if (n !== n) // shortcut for verifying if it's NaN
				n = 0;
			else if (n !== 0 && n !== (1 / 0) && n !== -(1 / 0)) n = (n > 0 || -1) * Math.floor(Math.abs(n));
		}

		if (n >= len) return -1;

		var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);

		for (; k < len; k++) {
			if (k in t && t[k] === searchElement) return k;
		}
		return -1;
	};
}

function updateLPMastParent(LPMastRec,newParent)
{
	nlapiLogExecution('Debug','LPMastRec.getId()',LPMastRec.getId());
	newParent.selectNewLineItem('recmachcustrecord_ebiz_lp_parent');

	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent', 'id', LPMastRec.getId());
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','isinactive', 'T');           
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_lp', null);
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','name', LPMastRec.getValue('name'));
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_masterlp', '');
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_site', LPMastRec.getValue('custrecord_ebiz_lpmaster_site'));
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_company', LPMastRec.getValue('custrecord_ebiz_lpmaster_company'));
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_lptype', LPMastRec.getValue('custrecord_ebiz_lpmaster_lptype'));
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_sizeid',LPMastRec.getValue('custrecord_ebiz_lpmaster_sizeid'));
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_totwght',LPMastRec.getValue('custrecord_ebiz_lpmaster_totwght'));
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_totcube',LPMastRec.getValue('custrecord_ebiz_lpmaster_totcube'));
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_wmsstatusflag',LPMastRec.getValue('custrecord_ebiz_lpmaster_wmsstatusflag'));

	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_sscc', LPMastRec.getValue('custrecord_ebiz_lpmaster_sscc'));
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_controlno', LPMastRec.getValue('custrecord_ebiz_lpmaster_controlno'));
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_cart_closeflag', LPMastRec.getValue('custrecord_ebiz_cart_closeflag'));
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_item', LPMastRec.getValue('custrecord_ebiz_lpmaster_item'));
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lp_seq', LPMastRec.getValue('custrecord_ebiz_lp_seq'));
	//newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_lpmaster_sscc', request.getLineItemValue('custpage_details', 'custpage_lp', k));


	newParent.commitLineItem('recmachcustrecord_ebiz_lp_parent'); 
}


function updatelastpicktaskforoldcarton(vContLPMastArr,SOOrder)
{
	nlapiLogExecution('DEBUG', 'Into updatelastpicktaskforoldcarton', vContLPMastArr);
	nlapiLogExecution('DEBUG', 'SOOrder', SOOrder);

	var openreccount=0;

	var SOFilters = new Array();
	var SOColumns = new Array();
	var updatetask='T';

	for(var w=0;w<vContLPMastArr.length;w++)
	{
		var vTempLPNo=vContLPMastArr[w];

		SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK	

		if(SOOrder!=null&&SOOrder!="")
			SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', parseInt(SOOrder)));

		if(vTempLPNo!=null && vTempLPNo!='' && vTempLPNo!='null')
			SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vTempLPNo));	
		else
			SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'isempty'));	
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty'));

		SOColumns[0] = new nlobjSearchColumn('custrecord_wms_status_flag');
		SOColumns[1] = new nlobjSearchColumn('custrecord_device_upload_flag');

		var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
		if(SOSearchResults!=null && SOSearchResults!=''&& SOSearchResults.length>0)
		{
			for (var i = 0; i < SOSearchResults.length; i++){

				var status  = SOSearchResults[i].getValue('custrecord_wms_status_flag');
				var deviceuploadflag  = SOSearchResults[i].getValue('custrecord_device_upload_flag');

				nlapiLogExecution('DEBUG', 'status', status);
				nlapiLogExecution('DEBUG', 'deviceuploadflag', deviceuploadflag);

				if(status=='9')//Pick Generated
				{
					updatetask='F';
				}
			}
			nlapiLogExecution('DEBUG', 'updatetask', updatetask);

			if(updatetask=='T')
			{
				nlapiSubmitField('customrecord_ebiznet_trn_opentask', SOSearchResults[0].getId(),'custrecord_device_upload_flag', 'T');
			}
		}
	}

	nlapiLogExecution('DEBUG', 'Out of updatelastpicktaskforoldcarton', updatetask);
}
