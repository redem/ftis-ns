/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inventory/Suitelet/Attic/ebiz_LPSerialNos_SL.js,v $
 *     	   $Revision: 1.1.2.1 $
 *     	   $Date: 2013/05/01 01:27:50 $
 *     	   $Author: kavitha $
 *     	   $Name: t_NSWMS_2014_1_1_174 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_LPSerialNos_SL.js,v $
 * Revision 1.1.2.1  2013/05/01 01:27:50  kavitha
 * CASE201112/CR201113/LOG201121
 * TSG Issue fixes
 *
 * Revision 1.4.4.1  2012/05/04 13:30:02  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * confirm putaway
 *
 * Revision 1.4  2011/07/21 05:00:55  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 * Revision 1.3  2011/07/21 04:59:52  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/
function ebiznet_LPSerialNos_onclick(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm("Serial Numbers");
		/*var actitem,planno;
		if (request.getParameter('custparam_item') != null && request.getParameter('custparam_item') != "") {
			actitem = request.getParameter('custparam_item');
		}
		if (request.getParameter('custparam_planno') != null && request.getParameter('custparam_planno') != "") {
			planno = request.getParameter('custparam_planno');
		}*/

		var expserialno='',actserialno='',expskustatus='',actskustatus='',expskustatusvalue,actskustatusvalue;
		var recid = "";

		if (request.getParameter('custparam_expserial') != null && request.getParameter('custparam_expserial') != "") {
			expserialno = request.getParameter('custparam_expserial');
		}
		if (request.getParameter('custparam_actserial') != null && request.getParameter('custparam_actserial') != "") {
			actserialno = request.getParameter('custparam_actserial');
		}
		if (request.getParameter('custparam_expskustatus') != null && request.getParameter('custparam_expskustatus') != "") {
			expskustatusvalue = request.getParameter('custparam_expskustatus');
		}
		if (request.getParameter('custparam_actskustatus') != null && request.getParameter('custparam_actskustatus') != "") {
			actskustatusvalue = request.getParameter('custparam_actskustatus');
		}
		
		if (request.getParameter('custparam_recid') != null && request.getParameter('custparam_recid') != "") {
			recid = request.getParameter('custparam_recid');
		}

		var CyccRecord = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe',recid); // 2 UNITS
		expserialno = CyccRecord.getFieldValue('custrecord_cycle_expserialno');
		actserialno = CyccRecord.getFieldValue('custrecord_cycle_serialno');
		
		nlapiLogExecution('ERROR', 'expskustatusvalue', expskustatusvalue);
		nlapiLogExecution('ERROR', 'actskustatusvalue', actskustatusvalue);

		//var sublist = form.addSubList("custpage_items", "list", "Serial #");

		var frmexpserialno = form.addField("custpage_expserial", "longtext", "Expected Serial#");//.setDisplaySize('2','2');
		var frmactserialno = form.addField("custpage_actserial", "longtext", "Actual Serial#"); 

		var frmexpskustatus = form.addField("custpage_expitemstatus", "text", "Expected Item Status");
		var frmactskustatus = form.addField("custpage_actitemstatus", "text", "Actual Item Status");
		var frmexpserialvar = form.addField("custpage_varianceinexpserial", "longtext", "Variance in Expected Serial#");
		var frmactserialvar = form.addField("custpage_varianceinactserial", "longtext", "Variance in Actual Serial#");


		/*nlapiLogExecution('ERROR','actitem',actitem);
		nlapiLogExecution('ERROR','planno',planno);*/

		/*var filters = new Array();

		if(planno != null && planno != "")
			filters.push(new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto',planno));

		filters.push(new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [31]));//Cycle Count Record
		var vRoleLocation=getRoledBasedLocation();
		if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
		{
			filters.push(new nlobjSearchFilter('custrecord_cyclesite_id', null, 'is', vRoleLocation));
		} 
		if(actitem != null && actitem != "")
			filters.push(new nlobjSearchFilter('custrecord_cycleact_sku', null, 'anyof',actitem));

		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_cycle_serialno'));
		columns.push(new nlobjSearchColumn('custrecord_cycle_expserialno'));
		columns.push(new nlobjSearchColumn('custrecord_expcyclesku_status'));
		columns.push(new nlobjSearchColumn('custrecord_cyclesku_status'));
		columns.push(new nlobjSearchColumn('custrecord_cycleexp_qty'));
		columns.push(new nlobjSearchColumn('custrecord_cycle_act_qty'));

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters,columns);
		var actskutext,actsku,expqty,actqty,expserialno,actserialno,expskustatus,expskustatusvalue,actskustatus,actskustatusvalue;
		for (var i = 0; searchresults != null && i < searchresults.length; i++) {

			var searchresult = searchresults[i];
			actskutext = searchresult.getText('custrecord_cycleact_sku');
			actsku = searchresult.getValue('custrecord_cyclecount_act_ebiz_sku_no');
			expqty = searchresult.getValue('custrecord_cycleexp_qty');
			actqty = searchresult.getValue('custrecord_cycle_act_qty');
			//explp = searchresult.getValue('custrecord_cyclelpno');
			//actlp = searchresult.getValue('custrecord_cycact_lpno');
			expserialno = searchresult.getValue('custrecord_cycle_expserialno');
			actserialno = searchresult.getValue('custrecord_cycle_serialno');

			nlapiLogExecution('ERROR', 'expserialno in link', expserialno);		
			nlapiLogExecution('ERROR', 'actserialno in link', actserialno);		

			expskustatus = searchresult.getText('custrecord_expcyclesku_status');
			expskustatusvalue = searchresult.getValue('custrecord_expcyclesku_status');
			actskustatus = searchresult.getText('custrecord_cyclesku_status');
			actskustatusvalue = searchresult.getValue('custrecord_cyclesku_status');

			if(actskustatus == null || actskustatus == "")
				actskustatus = expskustatus;
			if(actskustatusvalue == null || actskustatusvalue == "")
				actskustatusvalue = expskustatusvalue;

			actserialno = actserialno.trim();
			expserialno = expserialno.trim();

			if(expserialno != null && expserialno != '')
				var getexpserialArr = expserialno.split(',');

			if(actserialno != null && actserialno != '')
				var getactserialArr = actserialno.split(',');

			var varianceinExpserialNo='';
			var varianceinActserialNo='';


			for (var p = 0; p < getexpserialArr.length; p++) 
			{

				if(actserialno != null && actserialno != '')
				{
					//nlapiLogExecution('ERROR', 'getexpserialArr[p]', getexpserialArr[p]);
					if(actserialno.indexOf(getexpserialArr[p])==-1)//not found temSeriIdArr
					{
						if (varianceinExpserialNo == "") {
							varianceinExpserialNo= getexpserialArr[p];

						}
						else {
							varianceinExpserialNo = varianceinExpserialNo + "," + getexpserialArr[p];

						}
					}



				}
			}

			for (var q = 0; q < getactserialArr.length; q++) 
			{

				if(getexpserialArr != null && getexpserialArr != '')
				{
					nlapiLogExecution('ERROR', 'getactserialArr[q]', getactserialArr[q]);
					if(getexpserialArr.indexOf(getactserialArr[q])==-1)//not found temSeriIdArr
					{
						if (varianceinActserialNo == "") {
							varianceinActserialNo= getactserialArr[q];

						}
						else {
							varianceinActserialNo = varianceinActserialNo + "," + getactserialArr[q];

						}
					}



				}
			}

			nlapiLogExecution('ERROR', 'getexpserialArr.length in link', getexpserialArr.length);		
			nlapiLogExecution('ERROR', 'varianceinExpserialNo.length in link', varianceinExpserialNo.length);		
			nlapiLogExecution('ERROR', 'varianceinActserialNo.length in link', varianceinActserialNo.length);	

			if(varianceinExpserialNo != null && varianceinExpserialNo != '')
				var expvarianceqty =varianceinExpserialNo.split(',').length;

			if(varianceinActserialNo != null && varianceinActserialNo != '')
				var actualvarianceqty =varianceinActserialNo.split(',').length;

			nlapiLogExecution('ERROR', 'expvarianceqty ', expvarianceqty);		
			nlapiLogExecution('ERROR', 'actualvarianceqty ', actualvarianceqty);	

			form.getSubList('custpage_items').setLineItemValue('custpage_expserial', i + 1, expserialno);
			form.getSubList('custpage_items').setLineItemValue('custpage_actserial', i + 1, actserialno);
			form.getSubList('custpage_items').setLineItemValue('custpage_expitemstatus', i + 1, expskustatus);
			form.getSubList('custpage_items').setLineItemValue('custpage_actitemstatus', i + 1, actskustatus);
			form.getSubList('custpage_items').setLineItemValue('custpage_varianceinexpserial', i + 1, varianceinExpserialNo);
			form.getSubList('custpage_items').setLineItemValue('custpage_varianceinactserial', i + 1, varianceinActserialNo);
			}*/
		if(expskustatusvalue != null && expskustatusvalue != '')
			{
		var fields = ['custrecord_statusdescriptionskustatus'];
		var columns = nlapiLookupField('customrecord_ebiznet_sku_status', expskustatusvalue, fields);
		expskustatus = columns.custrecord_statusdescriptionskustatus;
			}
		if(actskustatusvalue != null && actskustatusvalue != '')
		{
		var fields1 = ['custrecord_statusdescriptionskustatus'];
		var columns1 = nlapiLookupField('customrecord_ebiznet_sku_status', actskustatusvalue, fields1);
		actskustatus = columns1.custrecord_statusdescriptionskustatus;
		}

		var varianceinExpserialNo='';
		var varianceinActserialNo='';

		if(actserialno != null && actserialno != '' && expserialno != null && expserialno != '')
		{
			actserialno = actserialno.trim();
			expserialno = expserialno.trim();

			if(expserialno != null && expserialno != '')
				var getexpserialArr = expserialno.split(',');

			if(actserialno != null && actserialno != '')
				var getactserialArr = actserialno.split(',');




			for (var p = 0; p < getexpserialArr.length; p++) 
			{

				if(actserialno != null && actserialno != '')
				{
					//nlapiLogExecution('ERROR', 'getexpserialArr[p]', getexpserialArr[p]);
					if(actserialno.indexOf(getexpserialArr[p])==-1)//not found temSeriIdArr
					{
						if (varianceinExpserialNo == "") {
							varianceinExpserialNo= getexpserialArr[p];

						}
						else {
							varianceinExpserialNo = varianceinExpserialNo + "," + getexpserialArr[p];

						}
					}



				}
			}

			for (var q = 0; q < getactserialArr.length; q++) 
			{

				if(getexpserialArr != null && getexpserialArr != '')
				{
					nlapiLogExecution('ERROR', 'getactserialArr[q]', getactserialArr[q]);
					if(getexpserialArr.indexOf(getactserialArr[q])==-1)//not found temSeriIdArr
					{
						if (varianceinActserialNo == "") {
							varianceinActserialNo= getactserialArr[q];

						}
						else {
							varianceinActserialNo = varianceinActserialNo + "," + getactserialArr[q];

						}
					}



				}
			}

			nlapiLogExecution('ERROR', 'getexpserialArr.length in link', getexpserialArr.length);		
			nlapiLogExecution('ERROR', 'varianceinExpserialNo.length in link', varianceinExpserialNo.length);		
			nlapiLogExecution('ERROR', 'varianceinActserialNo.length in link', varianceinActserialNo.length);	

			if(varianceinExpserialNo != null && varianceinExpserialNo != '')
				var expvarianceqty =varianceinExpserialNo.split(',').length;

			if(varianceinActserialNo != null && varianceinActserialNo != '')
				var actualvarianceqty =varianceinActserialNo.split(',').length;

			nlapiLogExecution('ERROR', 'expvarianceqty ', expvarianceqty);		
			nlapiLogExecution('ERROR', 'actualvarianceqty ', actualvarianceqty);	
		}
		
		frmexpserialno.setDefaultValue(expserialno);
		frmactserialno.setDefaultValue(actserialno);
		frmexpskustatus.setDefaultValue(expskustatus);
		frmactskustatus.setDefaultValue(actskustatus);
		frmexpserialvar.setDefaultValue(varianceinExpserialNo);
		frmactserialvar.setDefaultValue(varianceinActserialNo);
		
		/*form.getSubList('custpage_items').setLineItemValue('custpage_expserial', 1, expserialno);
		form.getSubList('custpage_items').setLineItemValue('custpage_actserial', 1, actserialno);
		form.getSubList('custpage_items').setLineItemValue('custpage_expitemstatus', 1, expskustatus);
		form.getSubList('custpage_items').setLineItemValue('custpage_actitemstatus', 1, actskustatus);
		form.getSubList('custpage_items').setLineItemValue('custpage_varianceinexpserial', 1, varianceinExpserialNo);
		form.getSubList('custpage_items').setLineItemValue('custpage_varianceinactserial', 1, varianceinActserialNo);*/

		response.writePage(form);
	}
	else {
	}
}

if (!Array.prototype.indexOf)
{
	Array.prototype.indexOf = function(elt /*, from*/)
	{
		var len = this.length;

		var from = Number(arguments[1]) || 0;
		from = (from < 0)
		? Math.ceil(from)
				: Math.floor(from);
		if (from < 0)
			from += len;

		for (; from < len; from++)
		{
			if (from in this &&
					this[from] === elt)
				return from;
		}
		return -1;
	};
}
