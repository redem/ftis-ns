/***************************************************************************
 eBizNET Solutions 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_CheckInItemStatus.js,v $
 *     	   $Revision: 1.15.2.10.4.8.2.12.2.1 $
 *     	   $Date: 2015/11/14 12:45:26 $
 *     	   $Author: aanchal $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_CheckInItemStatus.js,v $
 * Revision 1.15.2.10.4.8.2.12.2.1  2015/11/14 12:45:26  aanchal
 * 2015.2 Issue fix
 * 201415633
 *
 * Revision 1.15.2.10.4.8.2.12  2014/09/26 15:57:41  sponnaganti
 * Case# 201410548
 * DC Dental SB CR Expected Bin Size
 *
 * Revision 1.15.2.10.4.8.2.11  2014/06/13 07:18:38  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.15.2.10.4.8.2.10  2014/05/30 00:26:48  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.15.2.10.4.8.2.9  2014/02/11 15:12:49  sponnaganti
 * case# 20127115
 * (single quotes added for item)
 *
 * Revision 1.15.2.10.4.8.2.8  2014/01/06 13:12:59  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.15.2.10.4.8.2.7  2013/08/16 15:40:29  rmukkera
 * issue fix related to 20123249
 *
 * Revision 1.15.2.10.4.8.2.6  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.15.2.10.4.8.2.5  2013/06/04 10:54:02  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Country of origin changes
 *
 * Revision 1.15.2.10.4.8.2.4  2013/05/15 14:52:16  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.15.2.10.4.8.2.3  2013/04/17 16:04:02  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.15.2.10.4.8.2.2  2013/03/22 11:44:32  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.15.2.10.4.8.2.1  2013/03/01 15:01:12  rmukkera
 * code is merged from FactoryMation production as part of Standard bundle
 *
 * Revision 1.15.2.10.4.8  2013/02/07 15:10:34  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.15.2.10.4.7  2013/01/25 17:55:52  kavitha
 * CASE201112/CR201113/LOG201121
 * CASE20121578 - LexJet
 *
 * Revision 1.15.2.10.4.6  2012/11/01 14:55:35  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.15.2.10.4.5  2012/10/23 17:08:38  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form branch.
 *
 * Revision 1.15.2.10.4.4  2012/10/03 11:31:56  schepuri
 * CASE201112/CR201113/LOG201121
 * Auto Gen Lot No
 *
 * Revision 1.15.2.10.4.3  2012/09/27 10:56:31  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.15.2.10.4.2  2012/09/26 12:47:07  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.15.2.10.4.1  2012/09/24 10:07:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.15.2.10  2012/09/14 07:22:29  schepuri
 * CASE201112/CR201113/LOG201121
 * checking for null
 *
 * Revision 1.15.2.9  2012/07/31 06:53:32  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to Item Status was resolved.
 *
 * Revision 1.15.2.8  2012/07/23 22:49:45  spendyala
 * CASE201112/CR201113/LOG201121
 * Checking for the condition weather Item Status is configured on not.
 *
 * Revision 1.15.2.7  2012/06/11 13:28:08  spendyala
 * CASE201112/CR201113/LOG201121
 * Invalid parameter passing.
 *
 * Revision 1.15.2.6  2012/05/10 15:00:34  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to clicking previous screen was resolved.
 *
 * Revision 1.15.2.5  2012/05/02 14:32:21  gkalla
 * CASE201112/CR201113/LOG201121
 * Added Location filter for Item status to fix reg trailing issue
 *
 * Revision 1.15.2.4  2012/04/06 00:40:00  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * checkin item status
 *
 * Revision 1.15.2.3  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.15.2.2  2012/03/14 09:41:05  gkalla
 * CASE201112/CR201113/LOG201121
 * Added disable button functionality
 *
 * Revision 1.15.2.1  2012/02/13 13:50:10  spendyala
 * CASE201112/CR201113/LOG201121
 * Added BaseUomQty parameter to the query string.
 *
 * Revision 1.15  2011/12/30 10:54:38  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing, fetch all  itemstatus according to display seqno
 *
 * Revision 1.14  2011/12/21 22:24:45  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.13  2011/12/07 09:57:35  schepuri
 * CASE201112/CR201113/LOG201121
 * To fetch all  itemstatus according to display seqno
 *
 * Revision 1.12  2011/12/06 11:51:38  schepuri
 * CASE201112/CR201113/LOG201121
 * To fetch po line itemstatus
 *
 * Revision 1.11  2011/12/02 13:17:55  schepuri
 * CASE201112/CR201113/LOG201121
 * RF inbound issue fixing to fetch poline item status
 *
 * Revision 1.10  2011/09/24 19:47:31  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.7  2011/06/30 13:06:55  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.6  2011/06/14 07:45:49  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.5  2011/06/06 14:10:04  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 * In Check-in, Remaining cube calculation and putaway logic.
 *
 * Revision 1.4  2011/06/03 06:29:22  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/

function CheckInItemStatus(request, response){
	if (request.getMethod() == 'GET') {
		var ItemStatus;
		var itemStatusId;
		var itemStatusLoopCount = 0;
		var nextClicked;

		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);
		 var userAccountId = request.getParameter('custparam_userAccountId');
			nlapiLogExecution('DEBUG', 'userAccountId', userAccountId);
		//	Get the PO# from the previous screen, which is passed as a parameter		
		var getPONo = request.getParameter('custparam_poid');
		var getPOItem = request.getParameter('custparam_poitem');
		var getPOLineNo = request.getParameter('custparam_lineno');
		var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		var getPOInternalId = request.getParameter('custparam_pointernalid');
		var getPOQtyEntered = request.getParameter('custparam_poqtyentered');
		var getPOItemRemainingQty = request.getParameter('custparam_poitemremainingqty');
		var getPOLinePackCode = request.getParameter('custparam_polinepackcode');
		var getPOLineItemStatusValue = request.getParameter('custparam_polineitemstatusValue');
		nlapiLogExecution('DEBUG','getPOLineItemStatusValue',getPOLineItemStatusValue);
var getCountryOfOrigin=request.getParameter('custparam_countryoforigin');
var getCountryid=request.getParameter('custparam_countryid');
		var trantype= request.getParameter('custparam_trantype');

		var getItemCube = request.getParameter('custparam_itemcube');
		var getBaseUomQty = request.getParameter('custparam_baseuomqty');
		var getItemQuantity = request.getParameter('custparam_polinequantity');
		var getItemQuantityReceived = request.getParameter('custparam_polinequantityreceived');

		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');

		var getWHLocation = request.getParameter('custparam_whlocation');
		nlapiLogExecution('DEBUG','getWHLocation',getWHLocation);
		var getExpectedBinSize1=request.getParameter('custparam_expectedbinsize1');
		var getExpectedBinSize2=request.getParameter('custparam_expectedbinsize2');
		
		nlapiLogExecution('DEBUG','getExpectedBinSize1',getExpectedBinSize1);
		nlapiLogExecution('DEBUG','getExpectedBinSize2',getExpectedBinSize2);
		var pickfaceEnteredOption = request.getParameter('custparam_enteredOption');

		var itemStatusFilters = new Array();
		itemStatusFilters[0] = new nlobjSearchFilter('isinactive',null, 'is','F');
		itemStatusFilters[1] = new nlobjSearchFilter('custrecord_allowrcvskustatus',null, 'is','T');
		if(getWHLocation!=null && getWHLocation!='')
		itemStatusFilters[2] = new nlobjSearchFilter('custrecord_ebizsiteskus',null, 'anyof',getWHLocation);

		var itemStatusColumns = new Array();
		itemStatusColumns[0] = new nlobjSearchColumn('custrecord_display_sequence');
		itemStatusColumns[1] = new nlobjSearchColumn('name');
		itemStatusColumns[0].setSort();
		itemStatusColumns[1].setSort();

		var itemStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);
		var itemStatusCount=0;
		if(itemStatusSearchResult!=null&&itemStatusSearchResult!="")
			itemStatusCount = itemStatusSearchResult.length;
		var poloneitemStatus;
		var polineitemstatusvalue;
		var displayseq;
		if(itemStatusSearchResult!=null&&itemStatusSearchResult!="")
		{
			for(var k =0; k<itemStatusSearchResult.length ;k++)
			{
				polineitemstatusvalue =  itemStatusSearchResult[k].getId();
				if(polineitemstatusvalue == getPOLineItemStatusValue)
				{
					poloneitemStatus = itemStatusSearchResult[k].getValue('name');
					displayseq = itemStatusSearchResult[k].getValue('custrecord_display_sequence');
				}
			}
		}
		if (request.getParameter('custparam_count') != null)
		{
			var itemStatusRetrieved = request.getParameter('custparam_count');
			nlapiLogExecution('DEBUG', 'itemStatusRetrieved', itemStatusRetrieved);

			nextClicked = request.getParameter('custparam_nextclicked');
			nlapiLogExecution('DEBUG', 'nextClicked', nextClicked);

			itemStatusCount = itemStatusCount - itemStatusRetrieved;
			nlapiLogExecution('DEBUG', 'itemStatusCount', itemStatusCount);
		}
		else
		{
			var itemStatusRetrieved = 0;
			nlapiLogExecution('DEBUG', 'itemStatusRetrieved', itemStatusRetrieved);
		}

		/*if (itemStatusCount > 4)
		{
			nlapiLogExecution('DEBUG', 'itemStatusCount greater than 4', itemStatusCount);
			itemStatusLoopCount = 4;
		}
		else
		{
			nlapiLogExecution('DEBUG', 'itemStatusCount less than 4', itemStatusCount);
			itemStatusLoopCount = itemStatusCount;
		}*/
		
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
	    
		var st0,st1,st2,st3,st4,st5,st6;
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "INGRESAR SELECCI&#211;N";
			st2 = "ENVIAR";
			st3 = "ANTERIOR";
			
		}
		else
		{
			st0 = "";
			st1 = "ENTER ITEM STATUS";
			st2 = "SEND";
			st3 = "PREV";

		}
			
		
		
		itemStatusLoopCount = itemStatusCount;
		var functionkeyHtml=getFunctionkeyScript('_rf_checkin_itemstatus'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";    
		
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		
		//html = html + " document.getElementById('enterstatus').focus();";        
		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL()'>";
		html = html + "	<form name='_rf_checkin_itemstatus' method='POST'>";
		html = html + "		<table>";

		nlapiLogExecution('DEBUG', 'itemStatusLoopCount', itemStatusLoopCount);
		var StatusNo=1;
		for (var i = itemStatusRetrieved; i < (parseFloat(itemStatusRetrieved) + parseFloat(itemStatusLoopCount)); i++) {
			var itemStatusSearchResults = itemStatusSearchResult[i];

			if (itemStatusSearchResults != null)
			{
				itemStatus = itemStatusSearchResults.getValue('name');
				itemStatusId = itemStatusSearchResults.getId();				
			}
			var value = parseFloat(i) + 1;
			//html = html + "			<tr><td align = 'left'>"+ value +". <label>" + itemStatus + "</label></td></tr>";   
			if(poloneitemStatus==itemStatus)
			{
				StatusNo=value; 
			}

			nlapiLogExecution('DEBUG', 'StatusNo', StatusNo);
		}
		html = html + "			<tr>";
//		html = html + "				<td align = 'left'>ENTER ITEM STATUS : <label>" + getPOLineItemStatus + "</label>";
		html = html + "			<tr><td align = 'left'>" + st1 +"</td></tr> ";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterstatus' id='enterstatus' type='text' value='"+ StatusNo +"'/>";

		//html = html + "				<input type='hidden' name='hdndisplayseq' value=" + displayseq + "></td>";
		html = html + "				<input type='hidden' name='hdnItemStatus' value=" + poloneitemStatus + "></td>";
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				<input type='hidden' name='hdnItemPackCode' value=" + getPOLinePackCode + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getItemQuantity + ">";
		html = html + "				<input type='hidden' name='hdnQuantityReceived' value=" + getItemQuantityReceived + ">";
		html = html + "				<input type='hidden' name='hdnItemCube' value=" + getItemCube + ">";
		html = html + "				<input type='hidden' name='hdntrantype' value=" + trantype + ">";
		//case# 20127115 starts
		html = html + "				<input type='hidden' name='hdnitemname' value='" + getPOItem + "'>";
		//case# 20127115 end
		html = html + "				<input type='hidden' name='hdnBaseUomQty' value=" + getBaseUomQty + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				<input type='hidden' name='hdnItemRemaininingQuantity' value=" + parseFloat(getPOItemRemainingQty) + ">";
		html = html + "				<input type='hidden' name='hdnPOQuantityEntered' value=" + parseFloat(getPOQtyEntered) + ">";
		html = html + "				<input type='hidden' name='hdnPOInternalId' value=" + getPOInternalId + ">";
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		html = html + "				<input type='hidden' name='hdnNextClicked' value=" + nextClicked + ">";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + getWHLocation + ">";
		html = html + "				<input type='hidden' name='hdnuserAccountId' value=" + userAccountId + ">";
		html = html + "				<input type='hidden' name='hdngetExpectedBinSize1' value=" + getExpectedBinSize1 + ">";
		html = html + "				<input type='hidden' name='hdngetExpectedBinSize2' value=" + getExpectedBinSize2 + ">";
		html = html + "				<input type='hidden' name='hdncountryoforigin' value=" + getCountryOfOrigin + ">";
		html = html + "				<input type='hidden' name='hdncountryid' value=" + getCountryid + ">";
		html = html + "				<input type='hidden' name='hdnPickfaceEnteredOption' value=" + pickfaceEnteredOption + ">";
		html = html + "				<input type='hidden' name='hdnItemStatusNo' value=" + getPOLineItemStatusValue + "></td>";
		//html = html + "				<input type='hidden' name='hdnItemStatusNo' value=" + StatusNo + "></td>";
		html = html + "				<input type='hidden' name='hdnCount' value=" + i + "></td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					" + st3 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";




		for (var i = itemStatusRetrieved; i < (parseFloat(itemStatusRetrieved) + parseFloat(itemStatusLoopCount)); i++) {
			var itemStatusSearchResults = itemStatusSearchResult[i];

			if (itemStatusSearchResults != null)
			{
				itemStatus = itemStatusSearchResults.getValue('name');
				itemStatusId = itemStatusSearchResults.getId();				
			}
			var value = parseFloat(i) + 1;
			html = html + "			<tr><td align = 'left'>"+ value +". <label>" + itemStatus + "</label></td></tr>";   
			if(poloneitemStatus==itemStatus)
			{
				StatusNo=value; 
			}

			nlapiLogExecution('DEBUG', 'StatusNo', StatusNo);
		}


		/*if (itemStatusLoopCount > 4)
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>NEXT <input name='cmdNext' type='submit' value='F8'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
		}*/
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterstatus').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
//		Commented by Phani 04-08-2011    	
//		var getItemStatus = request.getParameter('enterstatus');
		var getReturnedItemStatus = request.getParameter('hdnItemStatus');
		var itemStatus = '';
		var itemStatusId = '';

//		Added by Phani 04-08-2011
		var getItemStatusOption = request.getParameter('enterstatus');

		var getActualBeginTime = request.getParameter('hdnActualBeginTime');
		var getActualBeginTimeAMPM = request.getParameter('hdnActualBeginTimeAMPM');

		var nextClicked = request.getParameter('hdnNextClicked');
		nlapiLogExecution('DEBUG', 'nextClicked', nextClicked);

		// This variable is to hold the Quantity entered.
		var POarray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);
		var userAccountId = request.getParameter('hdnuserAccountId');
		nlapiLogExecution('DEBUG', 'userAccountId', userAccountId);
		POarray["custparam_userAccountId"] = userAccountId;
		POarray["custparam_expectedbinsize1"]=request.getParameter('hdngetExpectedBinSize1');
		POarray["custparam_expectedbinsize2"]=request.getParameter('hdngetExpectedBinSize2');
		var st4,st5;
		if( getLanguage == 'es_ES')
		{
			st4 = "ESTADO ELEMENTO NO V&#193;LIDO";
			st5 = "USTED NO PUEDE RECIBIR CON EL ESTADO";
			
		}
		else
		{

			st4 = "INVALID ITEM STATUS";
			st5 = " YOU CANNOT RECEIVE WITH THIS STATUS";
			
		}
		

//		var itemStatus;
		POarray["custparam_polineitemstatusValue"]=request.getParameter('hdnItemStatusNo');
		POarray["custparam_trantype"] = request.getParameter('hdntrantype');
		POarray["custparam_poid"] = request.getParameter('custparam_poid');
		POarray["custparam_StatusNo"] = request.getParameter('hdnItemStatusNo');
		//case# 20127115 starts
		POarray["custparam_poitem"] = request.getParameter('hdnitemname');
		
		nlapiLogExecution('DEBUG', 'Item name', POarray["custparam_poitem"]);
		//case# 20127115 end
		POarray["custparam_lineno"] = request.getParameter('custparam_lineno');
		POarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
		POarray["custparam_pointernalid"] = request.getParameter('hdnPOInternalId'); //request.getParameter('custparam_pointernalid');
		POarray["custparam_poqtyentered"] = request.getParameter('hdnPOQuantityEntered'); //request.getParameter('custparam_poqtyentered');
		POarray["custparam_poitemremainingqty"] = request.getParameter('hdnItemRemaininingQuantity'); //request.getParameter('custparam_poitemremainingqty');
		POarray["custparam_polinepackcode"] = request.getParameter('hdnItemPackCode'); //request.getParameter('custparam_polinepackcode');
		POarray["custparam_polinequantity"] = request.getParameter('hdnQuantity'); //request.getParameter('custparam_polinequantity');
		POarray["custparam_polinequantityreceived"] = request.getParameter('hdnQuantityReceived'); //request.getParameter('custparam_polinequantityreceived');
		POarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
		POarray["custparam_enteredOption"] = request.getParameter('hdnPickfaceEnteredOption');
POarray["custparam_countryoforigin"] = request.getParameter('hdncountryoforigin');
POarray["custparam_countryid"] = request.getParameter('hdncountryid');
		POarray["custparam_itemcube"] = request.getParameter('custparam_itemcube');
		//code added on 13 feb 2012 by suman
		//To get the baseuom qty .
		POarray["custparam_baseuomqty"] = request.getParameter('hdnBaseUomQty');

		nlapiLogExecution('DEBUG','ITEMINFO[0]',POarray["custparam_itemcube"]);
		nlapiLogExecution('DEBUG','ITEMINFO[1]',POarray["custparam_baseuomqty"]);
		//end of code as of 13 feb 2012.

		nlapiLogExecution('DEBUG', 'custparam_poqtyentered', POarray["custparam_poqtyentered"]);

		POarray["custparam_error"] = st4;
		POarray["custparam_screenno"] = '4';

		POarray["custparam_actualbegindate"] = request.getParameter('hdnActualBeginDate');

		//        var ActualBeginTime = request.getParameter('custparam_actualbegintime')+ ' ' + request.getParameter('custparam_actualbegintimeampm');

		//		var TimeArray = new Array();
		//		TimeArray = ActualBeginTime.split(' ');
		POarray["custparam_actualbegintime"] = getActualBeginTime; //TimeArray[0];
		POarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM; //TimeArray[1];
		//nlapiLogExecution('DEBUG', 'custparam_actualbegintime', POarray["custparam_actualbegintime"]);
		//nlapiLogExecution('DEBUG', 'custparam_actualbegintimeampm', POarray["custparam_actualbegintimeampm"]);
		//nlapiLogExecution('DEBUG', 'POarray["custparam_poitem"] ', POarray["custparam_poitem"]);
		//nlapiLogExecution('DEBUG', 'POarray["custparam_poid"] ', POarray["custparam_poid"]);

		// Added by Phani on 03-25-2011
		POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
		nlapiLogExecution('DEBUG', 'WH Location', POarray["custparam_whlocation"]);

		var getOptedField = request.getParameter('hdnOptedField');
		POarray["custparam_option"] = getOptedField;

		nlapiLogExecution('DEBUG', 'optedField', POarray["custparam_option"]);

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent; //= request.getParameter('cmdPrevious');

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
//		if (optedEvent == 'F7') {		
		if (request.getParameter('cmdPrevious') == 'F7') {
			if (nextClicked != 'Y')
			{
				//response.sendRedirect('SUITELET', 'customscript_rf_checkin_qty', 'customdeploy_rf_checkin_qty_di', false, POarray);
				if(userAccountId!="TSTDRV909212")
				{
					POarray["custparam_poqtyentered"] = POarray["custparam_poqtyentered"] - 1;
					response.sendRedirect('SUITELET', 'customscript_rf_checkin_qty', 'customdeploy_rf_checkin_qty_di', false, POarray);
					return;
				}
				else
				{
					response.sendRedirect('SUITELET', 'customscript_rf_expectedbinsize', 'customdeploy_rf_expectedbinsize', false, POarray);
					return;
				}
			}
			else
			{
				POarray["custparam_count"] = 0;
				nlapiLogExecution('DEBUG', 'nextClicked', nextClicked);
				nlapiLogExecution('DEBUG', 'Next in F7', POarray["custparam_count"]);
				response.sendRedirect('SUITELET', 'customscript_rf_itemstatus', 'customdeploy_rf_itemstatus_di', false, POarray);
			}
		}
		//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
		else if (request.getParameter('cmdNext') == 'F8') {
			POarray["custparam_count"] = request.getParameter('hdnCount');
			POarray["custparam_nextclicked"] = "Y";

			nlapiLogExecution('DEBUG', 'Next', POarray["custparam_count"]);
			response.sendRedirect('SUITELET', 'customscript_rf_checkin_item_status', 'customdeploy_rf_checkin_item_status_di', false, POarray);
		}
		else// if (request.getParameter('cmdSend') == 'ENT')
		{
			var fields = ['recordType', 'custitem_ebizserialin','custitem_ebizbatchlot'];
			var columns = nlapiLookupField('item', POarray["custparam_fetcheditemid"], fields);
			/* Added by Sudheer A  on 08/09/2012 for null check */ 
			var serialInflg="F";
			var batchflag="F";
			var itemSubtype;
			if(columns != null && columns!='')
			{
				itemSubtype = columns.recordType;	
				serialInflg = columns.custitem_ebizserialin;
				batchflag= columns.custitem_ebizbatchlot;

			}
			//var itemSubtype = nlapiLookupField('item', POarray["custparam_fetcheditemid"], 'recordType');
			nlapiLogExecution('DEBUG', 'citem subtype is =', itemSubtype);

//			Added by Phani on 04-08-2011            
			if (getItemStatusOption == "")
			{
				//	if the 'Send' button is clicked without any option value entered,
				//  it has to show an error message. The next screen to which it has to navigate is to the error screen.
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'Entered Item Status', 'No data');
			}

			/* 
			 * This is commented as the item status has to drive based on the menu rather than the default item status 
			 *            if (getItemStatus == "") {
                if (getReturnedItemStatus == "") {
                //	if the 'Send' button is clicked without any option value entered,
                //  it has to show an error message. The next screen to which it has to navigate is to the error screen.
                response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
                nlapiLogExecution('DEBUG', 'Entered Item Status', 'No data');
                 }
                else {
                    var defstatus;
                    var defsearchresults = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, new nlobjSearchFilter('custrecord_defaultskustaus', null, 'is', 'T'));
                    if (defsearchresults != null && defsearchresults.length > 0) {
                        defstatus = defsearchresults[0].getId();
                        nlapiLogExecution('DEBUG', 'DefStatus', defstatus);
                        if (itemSubtype == 'lotnumberedinventoryitem') {
                            POarray["custparam_polineitemstatus"] = defstatus;// request.getParameter('hdnItemStatus');
                            response.sendRedirect('SUITELET', 'customscript_rf_checkin_batch_no', 'customdeploy_rf_checkin_batch_no_di', false, POarray);
                            nlapiLogExecution('DEBUG', 'Routing towards LOT/BATCH screens');
                        }
                        else {

                            POarray["custparam_polineitemstatus"] = defstatus;// request.getParameter('hdnItemStatus');
                            response.sendRedirect('SUITELET', 'customscript_rf_checkin_lp', 'customdeploy_rf_checkin_lp_di', false, POarray);
                        }
                    }

			else {
				POarray["custparam_error"] = 'NO DEFAULT ITEM STATUS';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'Item Status Not Found as DEFAULT.Please check Default checkbox');
			}
		}
	}
			 */
			else {
				/*
				 * Added by Phani on 04-08-2011.
				 * This part of the code is to fetch the Item Status from the option selected from the Item Status menu.
				 */
				nlapiLogExecution('DEBUG', 'Item Status Option', getItemStatusOption);
				var getWHLocation='';
				if(POarray["custparam_whlocation"] != null && POarray["custparam_whlocation"] != '')
					getWHLocation=POarray["custparam_whlocation"];
				var itemStatusFilters = new Array();
				itemStatusFilters[0] = new nlobjSearchFilter('isinactive',null, 'is','F');
				itemStatusFilters[1] = new nlobjSearchFilter('custrecord_allowrcvskustatus',null, 'is','T');
				if(getWHLocation!=null && getWHLocation!='')
				itemStatusFilters[2] = new nlobjSearchFilter('custrecord_ebizsiteskus', null, 'anyof', getWHLocation);

				var itemStatusColumns = new Array();
				//itemStatusColumns[0] = new nlobjSearchColumn('name');
				//itemStatusColumns[1] = new nlobjSearchColumn('custrecord_allowrcvskustatus');
				//itemStatusColumns[0].setSort();

				itemStatusColumns[0] = new nlobjSearchColumn('custrecord_display_sequence');
				itemStatusColumns[1] = new nlobjSearchColumn('name');
				itemStatusColumns[2] = new nlobjSearchColumn('custrecord_allowrcvskustatus');
				itemStatusColumns[0].setSort();
				itemStatusColumns[1].setSort();

				var itemStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);

				var itemStatusCount=0;
				if(itemStatusSearchResult!=null && itemStatusSearchResult!='')
				 itemStatusCount = itemStatusSearchResult.length;

				var count = request.getParameter('hdnCount');
				nlapiLogExecution('DEBUG', 'count', count);

				if (parseFloat(count) >= parseFloat(getItemStatusOption))
				{
					if (itemStatusSearchResult != null)
					{
						var ItemStatusRcvFlag;
						for (var i = 0; i <= getItemStatusOption; i++) {
							var itemStatusSearchResults = itemStatusSearchResult[i];
							if (parseFloat(getItemStatusOption) == parseFloat(i+1))
							{
								nlapiLogExecution('DEBUG', 'here');
								itemStatus = itemStatusSearchResults.getValue('name');
								itemStatusId = itemStatusSearchResults.getId();
								nlapiLogExecution('DEBUG', 'Latest Item Status', itemStatus);
								ItemStatusRcvFlag=itemStatusSearchResults.getValue('custrecord_allowrcvskustatus');
							}
							else
							{
								POarray["custparam_error"] = st4;
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								nlapiLogExecution('DEBUG', 'Item Status entered is not a valid status');
							}
						}
						nlapiLogExecution('DEBUG', 'ItemStatusRcvFlag', ItemStatusRcvFlag);
						if(ItemStatusRcvFlag=='T')
						{
							if (itemStatus != null)
							{
								
								POarray["custparam_polineitemstatustext"] = itemStatus;
								/*           	
								 * This part of the code is commented on 04-08-2011 by Phani.
								 * The item status screen on the RF is changed in order to display all the item status available.
								 * Based on the list, the user can select any option required.

                var chksearchresults = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, new nlobjSearchFilter('name', null, 'is', request.getParameter('enterstatus')));
                if (chksearchresults != null && chksearchresults.length > 0) {
                    nlapiLogExecution('DEBUG', 'Check Status Whether Exists OR Not', chksearchresults[0].getId());
								 */
								if (itemSubtype == "lotnumberedinventoryitem" || itemSubtype=="lotnumberedassemblyitem" || batchflag=="T" )
								{
									
									var vAutoLotgenFlag=getSystemRule('Auto Generate Lot Flag');
									nlapiLogExecution('DEBUG', 'vAutoLotgenFlag',vAutoLotgenFlag);
									if(vAutoLotgenFlag=='Y')
									{
										if(POarray["custparam_fetcheditemid"] != null && POarray["custparam_fetcheditemid"] != '')
										{											
											var vLotArr=AutolotNumber(POarray["custparam_fetcheditemid"]);
											nlapiLogExecution('DEBUG', 'vLotArr',vLotArr);
											if(vLotArr != null && vLotArr != '' && vLotArr.length>0)
											{
												var vExpDate=vLotArr[1];
									            var vSeq=01;
									            if(vLotArr[2] != null && vLotArr[2].length==1)
									             vSeq='0'+vLotArr[2];
									            else
									             vSeq=vLotArr[2];
									            var vBatchNo=vSeq+vLotArr[0];
												POarray["custparam_autolot"]= vAutoLotgenFlag;
												POarray["custparam_expdate"]= vExpDate;
												POarray["custparam_fifodate"]= DateStamp();
												POarray["custparam_batchno"]= vBatchNo;
												POarray["custparam_polineitemstatus"] = itemStatusId;	//chksearchresults[0].getId();
												response.sendRedirect('SUITELET', 'customscript_rf_checkin_lp', 'customdeploy_rf_checkin_lp_di', false, POarray);
												nlapiLogExecution('DEBUG', 'Item Status ', POarray["custparam_polineitemstatus"]);
											}
										}
									}
									else
									{	
										POarray["custparam_polineitemstatus"] = itemStatusId;	//chksearchresults[0].getId();
										response.sendRedirect('SUITELET', 'customscript_rf_checkin_batch_no', 'customdeploy_rf_checkin_batch_no_di', false, POarray);
										nlapiLogExecution('DEBUG', 'Routing towards LOT/BATCH screens');
									}
								}
								else {
									POarray["custparam_polineitemstatus"] = itemStatusId;	//chksearchresults[0].getId();
									response.sendRedirect('SUITELET', 'customscript_rf_checkin_lp', 'customdeploy_rf_checkin_lp_di', false, POarray);
									nlapiLogExecution('DEBUG', 'Item Status ', POarray["custparam_polineitemstatus"]);
								}
							}
							else {
								POarray["custparam_error"] = st4;
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								nlapiLogExecution('DEBUG', 'Item Status Not Found as Valid in ITEMSTATUS RECORD');
							}
						}
						else
						{
							POarray["custparam_error"] = st5;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							nlapiLogExecution('DEBUG', 'RCV Status flag F');

						}
					}
				}
				else {
					POarray["custparam_error"] = st4;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					nlapiLogExecution('DEBUG', 'Item Status entered is not a valid status');
				}
				nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
			}
		}
	}
}
