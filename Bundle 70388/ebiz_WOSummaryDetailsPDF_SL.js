
/***************************************************************************
  �eBizNET Solutions     
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Suitelet/ebiz_WOSummaryDetailsPDF_SL.js,v $
 *     	   $Revision: 1.1.10.1.4.2 $
 *     	   $Date: 2013/03/19 12:08:11 $
 *     	   $Author: schepuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *


 *****************************************************************************/
function WOSummaryDetailsPDFSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('WO Summary Details Report');
		var vQbWO ="";
		 
		if(request.getParameter('custparam_wono')!=null && request.getParameter('custparam_wono')!="")
		{
			vQbWO = request.getParameter('custparam_wono');
		} 
		
		

		var filters = new Array();
		if(vQbWO!="" && vQbWO!=null)
		{
		filters.push(new nlobjSearchFilter('internalid', null, 'is', vQbWO));
		}
		if ((request.getParameter('custparam_fromdate') != "" && request.getParameter('custparam_fromdate') != null) && (request.getParameter('custparam_todate') != "" && request.getParameter('custparam_todate') != null) ) {

			filters.push(new nlobjSearchFilter('trandate', null, 'within', request.getParameter('custparam_fromdate'), request.getParameter('custparam_todate')));			 
		}
		if(request.getParameter('custparam_status')!=null && request.getParameter('custparam_status')!="")
		{
			filters.push(new nlobjSearchFilter('status', null, 'anyof', request.getParameter('custparam_status')));
		}
		if(request.getParameter('custparam_item')!=null && request.getParameter('custparam_item')!="")
		{
			filters.push(new nlobjSearchFilter('item', null, 'anyof', request.getParameter('custparam_item')));
		}
		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
		
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('tranid');
		columns[1] = new nlobjSearchColumn('internalid');
		columns[2] = new nlobjSearchColumn('status');
		columns[3] = new nlobjSearchColumn('trandate');
		columns[4] = new nlobjSearchColumn('quantity');
		columns[5] = new nlobjSearchColumn('status');
		columns[6] = new nlobjSearchColumn('custbody_ebiz_isprinted');
		columns[7] = new nlobjSearchColumn('item');		
		
		var searchResults = nlapiSearchRecord('workorder', null, filters , columns);
		//nlapiLogExecution('ERROR','searchresults',searchresults.length);
		var WO ,WOid,itemid,itemText,WoCreatedDate,	itemQty	,itemStatus	,printflag,WOno;
		
		var url;
		/*var ctx = nlapiGetContext();
		nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
		if (ctx.getEnvironment() == 'PRODUCTION') 
		{
			url = 'https://system.netsuite.com';			
		}
		else if (ctx.getEnvironment() == 'SANDBOX') 
		{
			url = 'https://system.sandbox.netsuite.com';				
		}*/
		nlapiLogExecution('ERROR', 'PDF URL',url);	
		var filefound = nlapiLoadFile('Images/LOGOCOMP.jpg');
		var finalimageurl='';
		if (filefound) 
		{ 
			nlapiLogExecution('ERROR', 'Event', 'file;'+filefound.getId()); 
			var imageurl = filefound.getURL();
			nlapiLogExecution('ERROR','imageurl',imageurl);
			//var finalimageurl = url + imageurl;//+';';
			var finalimageurl = imageurl;//+';';
			//finalimageurl=finalimageurl+ '&expurl=T;';
			nlapiLogExecution('ERROR','imageurl',finalimageurl);
			finalimageurl=finalimageurl.replace(/&/g,"&amp;");

		} 
		else 
		{
			nlapiLogExecution('ERROR', 'Event', 'No file;');
		}


		
		
		var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body  font-size=\"7\">\n";
		nlapiLogExecution('ERROR','xml0 ',"<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body font-size=\"18\">\n");
		var strxml ="<table width='100%' > ";
		//code added from Monobind account on 27Feb13 by santosh
		//only changed the borber style
	//	strxml += "<tr ><td valign='top' align='left' width='30%'><img src='" + finalimageurl + "'  /></td></tr><tr><td   align='center'   style='font-size:14;text-decoration:underline;'>";
		strxml += "<tr ><td valign='top' align='left' width='30%'><img src='" + finalimageurl + "' /></td><td valign='middle' align='left'  style='font-weight:bold;font-size:14;text-decoration:underline;'>";
		strxml += "WO Summary Details Report ";
		strxml += "</td></tr></table>";			
		strxml +="<table  width='100%'  border='1'> ";
		strxml =strxml+  "<tr style=\"font-weight:bold;background-color:gray;color:white;font-size:10\"><td width='12%' style='border-bottom: 1px; border-right: 1px;border-color: #000000;white-space: nowrap;'>";
		strxml += "WO #";
		strxml += "</td>";
		strxml += "<td width='12%' style='border-bottom: 1px;border-right: 1px; border-color: #000000;white-space: nowrap;'>";
		strxml += "Date Created";
		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		strxml += "<td width='12%' style='border-bottom: 1px;border-right: 1px; border-color: #000000;white-space: nowrap;'>";
		strxml += "Assembly Item";
		strxml += "</td>";
		strxml += "<td width='12%' style='border-bottom: 1px;border-right: 1px; border-color: #000000;white-space: nowrap;'>";
		strxml += "Quantity";
		strxml += "</td>";
		strxml += "<td width='12%' style='border-bottom: 1px;border-right: 1px; border-color: #000000;white-space: nowrap;'>";
		strxml += "Status";
		strxml += "</td>";
		strxml += "<td width='12%' style='border-bottom: 1px;border-right: 1px; border-color: #000000;white-space: nowrap;'>";			
		strxml += "Printed";	
		strxml =strxml+  "</td></tr>";
		//nlapiLogExecution('ERROR','searchresults',searchresults.length);
		if (searchResults != null) {
			for(var i = 0; searchResults != null && i < searchResults.length; i++) 
			{ 
				
				 WO = searchResults[i].getValue('tranid'); 
				 WOid = searchResults[i].getValue('internalid');							
				 itemid=searchResults[i].getValue('item'); 			
				 itemText = searchResults[i].getText('item');			
				 WoCreatedDate=searchResults[i].getValue('trandate');			
				 itemQty=searchResults[i].getValue('quantity');			
				 itemStatus=searchResults[i].getValue('status');			
				 printflag=searchResults[i].getValue('custbody_ebiz_isprinted');
				 if(printflag=='T')
						printflag='Yes';
					else
						printflag='No';

				strxml =strxml+  "<tr><td width='12%' style='border-bottom: 1px;border-right: 1px; border-color: #000000'>";
				strxml += WO;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td width='12%' style='border-bottom: 1px;border-right: 1px; border-color: #000000'>";
				strxml += WoCreatedDate;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td width='12%' style='border-bottom: 1px;border-right: 1px; border-color: #000000'>";
				strxml += itemText;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td width='12%' style='border-bottom: 1px;border-right: 1px; border-color: #000000'>";
				strxml += itemQty;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td width='12%' style='border-bottom: 1px;border-right: 1px; border-color: #000000'>";
				strxml += itemStatus;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td width='12%' style='border-bottom: 1px;border-right: 1px; border-color: #000000'>";			
				strxml += printflag;				
				strxml =strxml+  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
			}
		} // upto here
		strxml =strxml+"</table>";		
		strxml =strxml+ "\n</body>\n</pdf>";		
		nlapiLogExecution('ERROR', 'strxml', strxml);
		xml=xml +strxml;
		nlapiLogExecution('ERROR','XML',xml);
		
		var file = nlapiXMLToPDF(xml);	
		response.setContentType('PDF','WOSummary.pdf');
		response.write( file.getValue() );
	}
	else //this is the POST block
	{

	}
}
