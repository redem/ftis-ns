/***************************************************************************
 eBizNET Solutions Inc             
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Reports/Suitelet/ebiz_PackingList_SL.js,v $
 *     	   $Revision: 1.5.4.3.8.4 $
 *     	   $Date: 2015/09/02 15:39:05 $
 *     	   $Author: deepshikha $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_PackingList_SL.js,v $
 * Revision 1.5.4.3.8.4  2015/09/02 15:39:05  deepshikha
 * 2015.2 issue fixes
 * 201414170
 * 201414192�
 *
 * Revision 1.5.4.3.8.3  2013/12/17 15:18:17  rmukkera
 * Case # 20126346
 *
 * Revision 1.5.4.3.8.2  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.5.4.3.8.1  2013/02/27 13:22:58  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from Monobind
 *
 * Revision 1.5.4.3  2012/09/07 03:47:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.5.4.2  2012/04/30 10:41:37  spendyala
 * CASE201112/CR201113/LOG201121
 * While Searching of Item in ItemMaster,
 * 'name' filter is changed to 'nameinternal' and also checking weather the item is inactive state or not.
 *
 * Revision 1.5.4.1  2012/04/11 21:59:18  spendyala
 * CASE201112/CR201113/LOG201121
 * issue related to fetching the records is fixed.
 *
 * Revision 1.5  2011/10/03 06:10:31  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.4  2011/09/30 12:24:18  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.3  2011/09/27 14:13:02  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.2  2011/09/25 11:44:56  mbpragada
 * CASE201112/CR201113/LOG201121
 * Completed Packing List Report  Development
 *
 * Revision 1.1  2011/09/24 06:41:39  mbpragada
 * CASE201112/CR201113/LOG201121
 * Completed Packing List Report  Development
 *

 *****************************************************************************/
function ebiznet_PackingList(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('PackingList Report');
		var vQbWave = "";
		form.setScript('customscript_pakinglistreport');


		var fulfillmentField = form.addField('custpage_fulfillment', 'select', 'Fulfillment Ord#');
		fulfillmentField.setLayoutType('startrow', 'none');
		fulfillmentField.addSelectOption("", "");

		var filters = new Array();
		//7 STATUS.OUTBOUND.SHIP_UNIT_BUILT ;8 STATUS.OUTBOUND.PICK_CONFIRMED ;10 STATUS.OUTBOUND.TRAILER_LOADED ;14 STATUS.OUTBOUND.SHIPPED ;28 STATUS.OUTBOUND.PACK_COMPLETE ;
		filters[0] = new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', ['7','8','10','11','13','28']); 
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_lineord');
		columns[1] = new nlobjSearchColumn('internalid').setSort(true);

		var fulfillmentsearchResults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);	
		if (fulfillmentsearchResults != null) {
			for (var i = 0; i < fulfillmentsearchResults.length; i++) {

				var res=  form.getField('custpage_fulfillment').getSelectOptions(fulfillmentsearchResults[i].getValue('custrecord_lineord'), 'is');				
				if (res != null) {				
					if (res.length > 0) {
						continue;
					}
				}

				fulfillmentField.addSelectOption(fulfillmentsearchResults[i].getValue('internalid'), fulfillmentsearchResults[i].getValue('custrecord_lineord'));
			}
		}

		//var ShipField = form.addField('custpage_shiplp', 'select', 'Ship LP','customrecord_ebiznet_master_lp');

		var containerLp = form.addField('custpage_contaienrlp', 'select', 'Container LP');
		containerLp.setLayoutType('startrow', 'none');
		
		var LPfilters = new Array();
		//7 STATUS.OUTBOUND.SHIP_UNIT_BUILT ;8 STATUS.OUTBOUND.PICK_CONFIRMED ;10 STATUS.OUTBOUND.TRAILER_LOADED ;14 STATUS.OUTBOUND.SHIPPED ;28 STATUS.OUTBOUND.PACK_COMPLETE ;
		LPfilters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['7','8','10','14','28']);  
		containerLp.addSelectOption("", "");
		var columnsn = new Array();
		columnsn[0] = new nlobjSearchColumn('custrecord_container_lp_no');
		columnsn[1] = new nlobjSearchColumn('internalid').setSort(true);

		
		
		var LPsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, LPfilters,columnsn);		
		if (LPsearchresults != null) {
			for (var i = 0; i < LPsearchresults.length; i++) {

				var res=  form.getField('custpage_contaienrlp').getSelectOptions(LPsearchresults[i].getValue('custrecord_container_lp_no'), 'is');
				if (res != null) {				
					if (res.length > 0) {
						continue;
					}
				}

				containerLp.addSelectOption(LPsearchresults[i].getValue('custrecord_container_lp_no'), LPsearchresults[i].getValue('custrecord_container_lp_no'));
			}
		}

		form.addSubmitButton('Display');

		response.writePage(form);
	}
	else //this is the POST block
	{
		
		var form = nlapiCreateForm('PackingList Report');
		var vQbFullfillment = request.getParameter('custpage_fulfillment');
		var vQbcontaienrlp = request.getParameter('custpage_contaienrlp');
		
		if(vQbFullfillment != null && vQbFullfillment != ""){

			var vQbFullfillmentName = getFullfillmentName(vQbFullfillment);
		}		

		form.setScript('customscript_pakinglistreport');
		form.addButton('custpage_print','Print','Printreport('+ vQbFullfillment +')');		

		var fulfillmentField = form.addField('custpage_fulfillment', 'select', 'Fulfillment Ord#');
		fulfillmentField.setLayoutType('startrow', 'none');
		fulfillmentField.addSelectOption("", "");

		var filters = new Array();
		//7 STATUS.OUTBOUND.SHIP_UNIT_BUILT ;8 STATUS.OUTBOUND.PICK_CONFIRMED ;10 STATUS.OUTBOUND.TRAILER_LOADED ;14 STATUS.OUTBOUND.SHIPPED ;28 STATUS.OUTBOUND.PACK_COMPLETE ;
		filters[0] = new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', ['7','8','10','11','13','28']); 
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_lineord');
		columns[1] = new nlobjSearchColumn('internalid').setSort(true);

		var fulfillmentsearchResults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);	
		if (fulfillmentsearchResults != null) {
			for (var i = 0; i < fulfillmentsearchResults.length; i++) {

				var res=  form.getField('custpage_fulfillment').getSelectOptions(fulfillmentsearchResults[i].getValue('custrecord_lineord'), 'is');				
				if (res != null) {				
					if (res.length > 0) {
						continue;
					}
				}

				fulfillmentField.addSelectOption(fulfillmentsearchResults[i].getValue('internalid'), fulfillmentsearchResults[i].getValue('custrecord_lineord'));
			}
		}
		//var ShipField = form.addField('custpage_shiplp', 'select', 'Ship LP','customrecord_ebiznet_master_lp');

		var containerLp = form.addField('custpage_contaienrlp', 'select', 'Container LP');
		containerLp.setLayoutType('startrow', 'none');
		
		var LPfilters = new Array();
		//7 STATUS.OUTBOUND.SHIP_UNIT_BUILT ;8 STATUS.OUTBOUND.PICK_CONFIRMED ;10 STATUS.OUTBOUND.TRAILER_LOADED ;14 STATUS.OUTBOUND.SHIPPED ;28 STATUS.OUTBOUND.PACK_COMPLETE ;
		LPfilters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['7','8','10','14','28']);  
		containerLp.addSelectOption("", "");
		
		var columnsn = new Array();
		columnsn[0] = new nlobjSearchColumn('custrecord_container_lp_no');
		columnsn[1] = new nlobjSearchColumn('internalid').setSort(true);
		
		var LPsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, LPfilters,columnsn);		
		if (LPsearchresults != null) {
			for (var i = 0; i < LPsearchresults.length; i++) {

				var res=  form.getField('custpage_contaienrlp').getSelectOptions(LPsearchresults[i].getValue('custrecord_container_lp_no'), 'is');
				if (res != null) {				
					if (res.length > 0) {
						continue;
					}
				}

				containerLp.addSelectOption(LPsearchresults[i].getValue('custrecord_container_lp_no'), LPsearchresults[i].getValue('custrecord_container_lp_no'));
			}
		}

		form.addSubmitButton('Display');

		var sublist = form.addSubList("custpage_items", "staticlist", "ItemList");
		
		sublist.addField("custpage_fulfillment", "text", "Fulfillment Ord#");
		sublist.addField("custpage_item", "text", "Line #");
		sublist.addField("custpage_material", "text", "Item");			
		sublist.addField("custpage_itemname", "text", "Description");
		//code added from Monobind on 27Feb13 by santosh
		// added Lot#
		sublist.addField("custpage_lot", "text", "LOT#");
		//up to here
		sublist.addField("custpage_customer", "text", "Customer SKU");	
		//sublist.addField("custpage_skuno", "text", "SKU #");	
		sublist.addField("custpage_upc", "text", "UPC");
		sublist.addField("custpage_qty", "text", "Quantity");		 
		nlapiLogExecution('ERROR', 'into else part before line search','done');
		var searchfilters = new Array();
		if (vQbFullfillmentName != "" && vQbFullfillmentName != null) {
			nlapiLogExecution('ERROR', 'vQbFullfillmentName', vQbFullfillmentName);
			searchfilters.push(new nlobjSearchFilter('name', null, 'is', vQbFullfillmentName));			 
		}	
		if (vQbcontaienrlp != "" && vQbcontaienrlp != null) {
			nlapiLogExecution('ERROR', 'vQbcontaienrlp', vQbcontaienrlp);
			searchfilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vQbcontaienrlp));			 
		}	
		//7 STATUS.OUTBOUND.SHIP_UNIT_BUILT ;8 STATUS.OUTBOUND.PICK_CONFIRMED ;10 STATUS.OUTBOUND.TRAILER_LOADED ;14 STATUS.OUTBOUND.SHIPPED ;28 STATUS.OUTBOUND.PACK_COMPLETE ;
		searchfilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['7','8','10','14','28']));
		searchfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));
		
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_line_no');
		columns[1] = new nlobjSearchColumn('custrecord_sku');
		columns[2] = new nlobjSearchColumn('custrecord_skudesc');	
		columns[3] = new nlobjSearchColumn('custrecord_act_qty');		
		columns[4] = new nlobjSearchColumn('name');	
		//code added from Monobind on 27Feb13 by santosh
		columns[5] = new nlobjSearchColumn('custrecord_batch_no');	
	//Case # 20126346� Start

		columns[6] = new nlobjSearchColumn('custrecord_serial_no');	
		//Case # 20126346� End
		//upto here
		var linesearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, searchfilters, columns);

		var item,material,description,customer='',skuno,quantity,upccode,fulfillment,lot='';

		for (var i = 0; linesearchresults != null && i < linesearchresults.length; i++) {
			var linesearchresultscount = linesearchresults[i];
			item = linesearchresults[i].getValue('custrecord_line_no');
			fulfillment= linesearchresults[i].getValue('name');
			material = linesearchresults[i].getText('custrecord_sku');
			description = linesearchresults[i].getValue('custrecord_skudesc');
			//customer = linesearchresults.getValue('custrecord_sku');
			//skuno = linesearchresults.getValue('custrecord_ebiz_sku_no');
			quantity = linesearchresults[i].getValue('custrecord_act_qty');
			//code added from Monobind on 27Feb13 by santosh
			lot = linesearchresults[i].getValue('custrecord_batch_no');
			//Case # 20126346� Start
			if(lot==null || lot=='' || lot=='null' || lot=='undefined')
				{
				lot=linesearchresults[i].getValue('custrecord_serial_no');
				}
			//Case # 20126346� End
			//upto here 
			//upc			
			var upcfilters = new Array();
			//Changed as on 30/4/12 by suman
			upcfilters.push(new nlobjSearchFilter('nameinternal', null, 'is', material));	
			upcfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
			//end of changes as on 30/4/12.
		
			var upccolumns = new Array();
			upccolumns[0] = new nlobjSearchColumn('upccode');	
			upccolumns[1] = new nlobjSearchColumn('custitem_ebizdescriptionitems');
			var upcsearchresults = nlapiSearchRecord('item', null, upcfilters, upccolumns);
			if(upcsearchresults != null && upcsearchresults != ""){
				upccode=upcsearchresults[0].getValue('upccode');
				itemdesc=upcsearchresults[0].getValue('custitem_ebizdescriptionitems');
			}

			form.getSubList('custpage_items').setLineItemValue('custpage_item', i + 1, item);
			form.getSubList('custpage_items').setLineItemValue('custpage_fulfillment', i + 1, fulfillment);
			form.getSubList('custpage_items').setLineItemValue('custpage_material', i + 1, material);
			form.getSubList('custpage_items').setLineItemValue('custpage_itemname', i + 1, itemdesc);
			form.getSubList('custpage_items').setLineItemValue('custpage_customer', i + 1, customer);
			//form.getSubList('custpage_items').setLineItemValue('custpage_skuno', i + 1, skuno);
			form.getSubList('custpage_items').setLineItemValue('custpage_upc', i + 1, upccode);
			form.getSubList('custpage_items').setLineItemValue('custpage_qty', i + 1, quantity);
			//code added from Monobind on 27Feb13 by santosh
			form.getSubList('custpage_items').setLineItemValue('custpage_lot', i + 1, lot);
			//upto here
			

		}

		response.writePage(form);
	}
}
function getFullfillmentName(FullfillmentInternalid){ 
	var columns = nlapiLookupField('customrecord_ebiznet_ordline',FullfillmentInternalid, 'custrecord_lineord');	 
	nlapiLogExecution('ERROR', "Fullfillmentname", columns);	
	return columns;
}

function Printreport(ebizwaveno){
	nlapiLogExecution('ERROR', 'into Printreport ebizwaveno',ebizwaveno);
	var WavePDFURL = nlapiResolveURL('SUITELET', 'customscript_pakinglistreportpdf', 'customdeploy_ebizpackinglistpdf_di');
	/*var ctx = nlapiGetContext();
	nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
	if (ctx.getEnvironment() == 'PRODUCTION') {
		WavePDFURL = 'https://system.netsuite.com' + WavePDFURL;			
	}
	else 
		if (ctx.getEnvironment() == 'SANDBOX') {
			WavePDFURL = 'https://system.sandbox.netsuite.com' + WavePDFURL;				
		}*/
	nlapiLogExecution('ERROR', 'Wave PDF URL',WavePDFURL);					
	WavePDFURL = WavePDFURL + '&custparam_ebiz_fullfill_no='+ ebizwaveno;	
	window.open(WavePDFURL);

}
