/***************************************************************************
  						eBizNET Solutions Inc                
 ***************************************************************************
 **        $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_ContainerExceptions_SL.js,v $
 *     	   $Revision: 1.1.2.1.4.1.4.1.8.1 $
 *     	   $Date: 2015/09/23 14:57:49 $
 *     	   $Author: deepshikha $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 *
 *****************************************************************************/

function getAllheaderDetails()
{
	var filters = new Array();
	// Add filter criteria for WMS Status Flag ;
	// Search for C:'Pick Confirmed';
	filters.push( new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));
	// Add filter criteria for Task Type;
	// Search for Task Type = 'PICK';
	filters.push( new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_lpno');
	columns[2] = new nlobjSearchColumn('custrecord_sku');
	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	
	
	return searchresults;
}

function createContainerManagementSubList(form){
	var sublist = form.addSubList("custpage_items", "list", "Container Exceptions");
	sublist.addField("custpage_select", "checkbox", "Confirm");
	sublist.addField("custpage_order", "text", "order #");
	sublist.addField("custpage_orderline", "text", "order Line #");
	sublist.addField("custpage_itemno", "select", "Item", "item");
	sublist.addField("custpage_lotbatch", "text", "LOT#");
	sublist.addField("custpage_existingcontainer", "text", "Existing Container").setDisplayType('hidden');
	sublist.addField("custpage_newcontainer", "text", "New Container").setDisplayType('hidden');	
	sublist.addField("custpage_quantity", "text", "Quantity");
	sublist.addField("custpage_newquantity", "text", "Actual Pick Quantity").setDisplayType('entry');	
	sublist.addField("custpage_binlocation", "select", "Bin Location","customrecord_ebiznet_location");
	sublist.addField("custpage_paltlp", "text", "LP").setDisplayType('entry');	
	sublist.addField("custpage_internalid", "text", "Internal Id").setDisplayType('hidden');
	sublist.addField("custpage_sointernalid", "text", "SO Internal Id").setDisplayType('hidden');
	sublist.addField("custpage_itemstatus", "text", "Item Status").setDisplayType('hidden');
	sublist.addField("custpage_packcode", "text", "Packcode").setDisplayType('hidden');
}
function specifyContainerManagementColumns(){
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_lpno');
	columns[2] = new nlobjSearchColumn('custrecord_sku');
	columns[3] = new nlobjSearchColumn('custrecord_act_qty');
	columns[4] = new nlobjSearchColumn('custrecord_line_no');
	columns[5] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[6] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[7] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[8] = new nlobjSearchColumn('custrecord_sku_status');
	columns[9] = new nlobjSearchColumn('custrecord_packcode');
	

	columns[0].setSort();
	
	return columns;
}
function addContainerDetailsToSubList(form, currentRecord, i){
	form.getSubList('custpage_items').setLineItemValue('custpage_order', i + 1, currentRecord.getValue('name'));
	form.getSubList('custpage_items').setLineItemValue('custpage_orderline', i + 1, 
			currentRecord.getValue('custrecord_line_no'));
	form.getSubList('custpage_items').setLineItemValue('custpage_itemno', i + 1, 
			currentRecord.getValue('custrecord_sku'));
	form.getSubList('custpage_items').setLineItemValue('custpage_existingcontainer', i + 1, 
			currentRecord.getValue('custrecord_container_lp_no'));
	form.getSubList('custpage_items').setLineItemValue('custpage_quantity', i + 1, 
			currentRecord.getValue('custrecord_expe_qty'));
	form.getSubList('custpage_items').setLineItemValue('custpage_newquantity', i + 1, 
			currentRecord.getValue('custrecord_act_qty'));
	form.getSubList('custpage_items').setLineItemValue('custpage_internalid', i + 1, 
			currentRecord.getId());
	form.getSubList('custpage_items').setLineItemValue('custpage_sointernalid', i + 1, 
			currentRecord.getValue('custrecord_ebiz_order_no'));
	form.getSubList('custpage_items').setLineItemValue('custpage_itemstatus', i + 1, 
			currentRecord.getValue('custrecord_sku_status'));
	form.getSubList('custpage_items').setLineItemValue('custpage_packcode', i + 1, 
			currentRecord.getValue('custrecord_packcode'));
}

function specifyContainerManagementFilters(orderno,containerno,itemno){
	var filters = new Array();

	if(orderno != ""){
		nlapiLogExecution('ERROR','Inside orderno',orderno);
		filters.push(new nlobjSearchFilter('name', null, 'is', orderno));			
	}
	if(containerno != ""){
		nlapiLogExecution('ERROR','Inside containerno',containerno);		 
		filters.push(new nlobjSearchFilter('custrecord_lpno', null, 'is', containerno));		 
	}
	if(itemno != ""){
		nlapiLogExecution('ERROR','Inside itemno',itemno);		 
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'is', itemno));
	}

	filters.push( new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));

	return filters;
}
function ebiz_ContainerExceptions_SL(request, response)
{
	if (request.getMethod() == 'GET'){
		var form = nlapiCreateForm('Container Exceptions');

		var order = form.addField('custpage_order', 'select', 'Order #');
		var container= form.addField('custpage_container', 'select', 'Container #');
		var item = form.addField('custpage_item', 'select', 'Item');

		order.addSelectOption("", "");
		container.addSelectOption("", "");
		item.addSelectOption("", "");
		 
		var searchresults = getAllheaderDetails();

		if(searchresults){
			for (var i = 0; i < searchresults.length; i++){
				// This is for order numbers to avoid duplicates.
				var orderResult = form.getField('custpage_order').getSelectOptions(searchresults[i].getValue('name'), 'is');
				if (orderResult != null) {
					if (orderResult.length > 0) {
						continue;
					}
				}
				order.addSelectOption(searchresults[i].getValue('name'), searchresults[i].getValue('name'));

				// This is for container numbers to avoid duplicates.
				var ContainerResult = form.getField('custpage_container').getSelectOptions(searchresults[i].getValue('custrecord_lpno'), 'is');
				if (ContainerResult != null) {
					if (ContainerResult.length > 0) {
						continue;
					}
				}
				container.addSelectOption(searchresults[i].getValue('custrecord_lpno'), searchresults[i].getValue('custrecord_lpno'));

				// This is for Item numbers to avoid duplicates.
				var ItemResult = form.getField('custpage_item').getSelectOptions(searchresults[i].getText('custrecord_sku'), 'is');
				if (ItemResult != null) {
					if (ItemResult.length > 0) {
						continue;
					}
				}
				item.addSelectOption(searchresults[i].getValue('custrecord_sku'), searchresults[i].getText('custrecord_sku'));
			}
		}	

		var button = form.addSubmitButton('Display');
		response.writePage(form);
	}
	else
	{
		
		var orderno;
		var form = nlapiCreateForm('Container Exceptions');
		form.setScript('customscript_containeexceptions_cl');
		var orderno = request.getParameter('custpage_order');
		var containerno = request.getParameter('custpage_container');
		var itemno = request.getParameter('custpage_item');
		
		//inline
		var ordernos = form.addField('custpage_order', 'text', 'Order #').setDisplayType('hidden').setDefaultValue(orderno);
		var containernos = form.addField('custpage_container', 'text', 'Container #').setDisplayType('hidden').setDefaultValue(containerno);
		var itemnos = form.addField('custpage_item', 'select', 'Item','item').setDisplayType('hidden').setDefaultValue(itemno);

		createContainerManagementSubList(form);
		
		var filters = specifyContainerManagementFilters(orderno,containerno,itemno);
		var columns = specifyContainerManagementColumns();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

		if(searchresults){
			for (var i = 0; i < searchresults.length; i++) {
				var currentRecord = searchresults[i];
				addContainerDetailsToSubList(form, currentRecord, i);
			}
		}
		performContainerManagementOperations(request,form);
		var button = form.addSubmitButton('Submit');
		response.writePage(form);		 
	}
}
function CreateInvetory(newQty,ItemId,binlocation,lp,itemstatus,packcode)
{
	var CreateInventoryRecord = nlapiCreateRecord('customrecord_ebiznet_createinv');
	nlapiLogExecution('ERROR', 'Create Inventory Move Record', 'Inventory Record');

	CreateInventoryRecord.setFieldValue('name', lp);
	CreateInventoryRecord.setFieldValue('custrecord_wms_inv_status_flag',19); //Status - STORAGE

	nlapiLogExecution('ERROR', 'IN  Create Inventory Record: ItemId', ItemId);
	
	
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_sku', ItemId);
	CreateInventoryRecord.setFieldValue('custrecord_inv_ebizsku_no', ItemId);
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(newQty).toFixed(5));
	//var vNewMovQty=MoveQty;
	nlapiLogExecution('ERROR', 'IN  Create Inventory Record: custrecord_ebiz_avl_qty', parseFloat(newQty).toFixed(5));
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_avl_qty', parseFloat(newQty).toFixed(5));
	nlapiLogExecution('ERROR', 'IN  Create Inventory Record: custrecord_ebiz_avl_qty', parseFloat(newQty).toFixed(5));

	CreateInventoryRecord.setFieldValue('custrecord_ebiz_qoh', parseFloat(newQty).toFixed(5));
	nlapiLogExecution('ERROR', 'IN  Create Inventory Record: custrecord_ebiz_qoh', parseFloat(newQty).toFixed(5));
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_packcode', packcode);
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);	
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());
	nlapiLogExecution('ERROR', 'locationid', binlocation);	
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_binloc', binlocation);//Inventory Location);
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_lp', lp);//Item LP #);	
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_callinv', 'N');//Bin Location);
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_displayfield', 'N');//Bin Location);
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_packcode', packcode);
	CreateInventoryRecord.setFieldValue('custrecord_invttasktype', 2);
	
	nlapiLogExecution('ERROR', 'Submitting Create Inventory Record', CreateInventoryRecord);

	//commit the record to NetSuite
	var invtrecid = nlapiSubmitRecord(CreateInventoryRecord);
	nlapiLogExecution('ERROR', 'CreateInventoryRecordId', 'test');
	nlapiLogExecution('ERROR', 'CreateInventoryRecordId', invtrecid);

	nlapiLogExecution('ERROR', 'Inventory Move Creation', 'Success');
	return invtrecid;

}

function updateInOpenTask(recordId, newQty){
	nlapiLogExecution('ERROR', 'open task recordId ', recordId);
	var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recordId);
	transaction.setFieldValue('custrecord_wms_status_flag', 28);
	transaction.setFieldValue('custrecord_act_qty', parseFloat(newQty).toFixed(5));
	 
	var opentaskrecord=nlapiSubmitRecord(transaction, false, true);
	nlapiLogExecution('ERROR', 'open task recordId ', 'Sucess');
	return opentaskrecord;
}

function createLPRecord(lp) {
	var timestamp1 = new Date();
	var t1 = new Date();
	var lpRecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
	var t2 = new Date();
	nlapiLogExecution('ERROR', 'createLPRecord:nlapiCreateRecord|master_lp',
			getElapsedTimeDuration(t2, t1));

	lpRecord.setFieldValue('name', lp);
	lpRecord.setFieldValue('custrecord_ebiz_lpmaster_lp', lp);
	t1 = new Date();
	var recid = nlapiSubmitRecord(lpRecord);
	t2 = new Date();
	nlapiLogExecution('ERROR', 'createLPRecord:nlapiSubmitRecord|master_lp',
			getElapsedTimeDuration(t2, t1));

	var timestamp2 = new Date();
	nlapiLogExecution('ERROR', 'createLPRecord Duration',
			getElapsedTimeDuration(timestamp1, timestamp2));
}
function performContainerManagementOperations(request,form)
{
	try
	{
		var LineCount = request.getLineItemCount('custpage_items');
		var newContainer ="";
		var newContainerSize ="";
		var newQty = "";
		var internalId = ""; 
		var flag = "";
		var LPInOpentask = new Array();
		var matchFound = true;
		var totalCube = 0;
		var totalWeight = 0;
		var falseCount = 0;

		for(var s=1; s <= LineCount; s++){
			flag = request.getLineItemValue('custpage_items', 'custpage_select', s);

			if(flag == "T"){	
				
				Qty = request.getLineItemValue('custpage_items', 'custpage_quantity', s);
				newQty = request.getLineItemValue('custpage_items', 'custpage_newquantity', s);
				itemId = request.getLineItemValue('custpage_items', 'custpage_itemno', s);
				binlocation = request.getLineItemValue('custpage_items', 'custpage_binlocation', s);				
				lp = request.getLineItemValue('custpage_items', 'custpage_paltlp', s);
				itemstatus = request.getLineItemValue('custpage_items', 'custpage_itemstatus', s);
				packcode = request.getLineItemValue('custpage_items', 'custpage_packcode', s);
				internalId = request.getLineItemValue('custpage_items', 'custpage_internalid', s);
				salesOrderInternalId = request.getLineItemValue('custpage_items', 'custpage_sointernalid', s);
				nlapiLogExecution('ERROR', 'internalId ', internalId);
				nlapiLogExecution('ERROR', 'newContainer ', newContainer);
				nlapiLogExecution('ERROR', 'newContainerSize ', newContainerSize);
				nlapiLogExecution('ERROR', 'newQty ', newQty);
				nlapiLogExecution('ERROR', 'lp ', lp);
				var Remainingqty=parseFloat(Qty)-parseFloat(newQty);
			
				if (lp != "")
					createLPRecord(lp);
				
				if(falseCount == 0){
					var invtrecord=CreateInvetory(Remainingqty,itemId,binlocation,lp,itemstatus,packcode);
					if(invtrecord!=null && invtrecord!='')
					{
					var opentaskrecord=updateInOpenTask(internalId, newQty);
					}
					if(opentaskrecord!=null && opentaskrecord!='')
					{
						var msg1 = form.addField('custpage_messages', 'inlinehtml', null, null, null);
						msg1.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Inventory Created Sucessfully ', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
						response.writePage(form);
					}
					else
					{
						var msg1 = form.addField('custpage_messages', 'inlinehtml', null, null, null);
						msg1.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Inventory not updated ', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
						response.writePage(form);
					}
				
					
				}
				else{
					var message = 'Container LP already assigned to another sales order ';
					var messageVar = ' Container LP# = ' + newContainer;
					showInlineMessage(form, 'Error', message, messageVar);
					response.writePage(form);
				}
			}
		}
	}	
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception',exp);
	}
	nlapiLogExecution('ERROR','Container Move Successful');
}