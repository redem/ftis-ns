/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/ebiz_RF_PickingContainerNo.js,v $
 *     	   $Revision: 1.23.2.35.4.19.2.49.2.7 $
 *     	   $Date: 2015/06/30 16:29:56 $
 *     	   $Author: mvarre $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * 
 * PRAMETERS
 *
 * DESCRIPTION
 * 
 * REVISION HISTORY
 * $Log: ebiz_RF_PickingContainerNo.js,v $
 * Revision 1.23.2.35.4.19.2.49.2.7  2015/06/30 16:29:56  mvarre
 * Case# 201413187
 * MHP Kit item pick qty and ship qty issue
 *
 * Revision 1.23.2.35.4.19.2.49.2.6  2015/04/13 07:41:50  rrpulicherla
 * Case#201412277
 *
 * Revision 1.23.2.35.4.19.2.49.2.5  2014/09/02 11:00:14  gkalla
 * case#20149227
 * ACE and JB cycle count issues
 *
 * Revision 1.23.2.35.4.19.2.49.2.4  2014/08/27 15:34:46  skavuri
 * Case# 20149929 Std Bundle Issue Fixed
 *
 * Revision 1.23.2.35.4.19.2.49.2.3  2014/08/18 15:13:20  skavuri
 * Case# 20149997 Std Bundle Issue Fixed
 *
 * Revision 1.23.2.35.4.19.2.49.2.2  2014/08/11 15:46:42  skavuri
 * Case# 20149927, 20149929 sb issue fixed
 *
 * Revision 1.23.2.35.4.19.2.49.2.1  2014/08/06 15:42:17  nneelam
 * case#  20149844
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.23.2.35.4.19.2.49  2014/06/13 12:32:55  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.23.2.35.4.19.2.48  2014/06/13 09:54:14  grao
 * Case#: 20148834  New GUI account issue fixes
 *
 * Revision 1.23.2.35.4.19.2.47  2014/06/06 08:11:36  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.23.2.35.4.19.2.46  2014/06/03 16:04:44  gkalla
 * case#201219550
 * Boombah picking issue
 *
 * Revision 1.23.2.35.4.19.2.45  2014/05/30 00:41:04  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.23.2.35.4.19.2.44  2014/05/08 14:49:03  sponnaganti
 * case# 20148263
 * (New Inv Ref# updation)
 *
 * Revision 1.23.2.35.4.19.2.43  2014/04/23 15:09:52  rmukkera
 * Case # 20145843
 *
 * Revision 1.23.2.35.4.19.2.42  2014/04/16 07:55:29  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to validating LP
 *
 * Revision 1.23.2.35.4.19.2.41  2014/04/15 13:10:38  snimmakayala
 * *** empty log message ***
 *
 * Revision 1.23.2.35.4.19.2.40  2014/01/20 13:40:10  snimmakayala
 * Case# : 20126712
 *
 * Revision 1.23.2.35.4.19.2.39  2014/01/06 13:19:17  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.23.2.35.4.19.2.38  2013/12/20 15:43:52  rmukkera
 * Case # 20126039
 *
 * Revision 1.23.2.35.4.19.2.37  2013/12/12 15:45:18  gkalla
 * case#20126330
 * GFT not to allow exisiting contLP of other order
 *
 * Revision 1.23.2.35.4.19.2.36  2013/12/11 22:07:43  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#220126225/20125991
 *
 * Revision 1.23.2.35.4.19.2.35  2013/11/22 14:29:43  rmukkera
 * Case# 20125591
 *
 * Revision 1.23.2.35.4.19.2.34  2013/11/22 08:40:38  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#201216680
 *
 * Revision 1.23.2.35.4.19.2.33  2013/11/20 23:28:13  skreddy
 * Case# 20125850
 * Lost and Found CR for MHP
 * issue fix
 *
 * Revision 1.23.2.35.4.19.2.32  2013/10/31 16:59:56  gkalla
 * case#20125306
 * Inventory hold flag check
 *
 * Revision 1.23.2.35.4.19.2.31  2013/10/19 13:13:28  grao
 * Issue fixes 20124624
 *
 * Revision 1.23.2.35.4.19.2.30  2013/10/09 16:27:24  skreddy
 * Case# 20124967,20124886,20124887
 * tpp SB and surftech issue fixs
 *
 * Revision 1.23.2.35.4.19.2.29  2013/10/08 23:59:38  gkalla
 * Case# 20124962
 * CWD not posting item fulfillment for second fulfillment order of same order
 *
 * Revision 1.23.2.35.4.19.2.28  2013/09/16 09:40:01  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201215000
 * RF Fast Picking
 *
 * Revision 1.23.2.35.4.19.2.27  2013/09/13 14:29:37  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Afosa changes
 * case#20124221
 *
 * Revision 1.23.2.35.4.19.2.26  2013/09/11 14:25:05  skreddy
 * Case# 20124314
 * standard bundle  issue fix
 *
 * Revision 1.23.2.35.4.19.2.25  2013/09/05 15:39:15  skreddy
 * Case# 20124248
 * standard bundle issue fix
 *
 * Revision 1.23.2.35.4.19.2.24  2013/08/23 06:11:38  snimmakayala
 * Case# 20124031
 * NLS - UAT ISSUES
 * Weight is not getting updated properly in LP master for SHIPASIS cartons.
 *
 * Revision 1.23.2.35.4.19.2.23  2013/08/06 03:48:12  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201214549
 * Performance tuning
 *
 * Revision 1.23.2.35.4.19.2.22  2013/08/05 06:31:46  schepuri
 * cwd qty exceptin issue fix
 *
 * Revision 1.23.2.35.4.19.2.21  2013/06/26 15:03:30  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * PMM :: Issue Fixes
 * Case# : 201213197
 *
 * Revision 1.23.2.35.4.19.2.20  2013/06/25 15:51:56  gkalla
 * CASE201112/CR201113/LOG201121
 * Standard bundle Issue Case# 20123171
 *
 * Revision 1.23.2.35.4.19.2.19  2013/06/25 10:07:08  gkalla
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fix
 *
 * Revision 1.23.2.35.4.19.2.18  2013/06/24 13:28:21  grao
 * CASE201112/CR201113/LOG201121
 * PMM Issues fixes
 * Displaying stage location for each pick confirmation
 *
 * Revision 1.23.2.35.4.19.2.17  2013/06/20 15:09:34  nneelam
 * CASE201112/CR201113/LOG201121
 * Standard Bundle, BuildShip Units:Issue No. 20122710
 *
 * Revision 1.23.2.35.4.19.2.16  2013/06/20 14:13:17  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA :: Issue Fixes
 * Case# : 201215543
 *
 * Revision 1.23.2.35.4.19.2.15  2013/06/19 22:53:41  gkalla
 * CASE201112/CR201113/LOG201121
 * As part of optimistic locking
 *
 * Revision 1.23.2.35.4.19.2.14  2013/06/17 14:06:23  skreddy
 * CASE201112/CR201113/LOG201121
 * issue rellated to serial numbers updating
 *
 * Revision 1.23.2.35.4.19.2.13  2013/06/14 13:11:43  grao
 * CASE201112/CR201113/LOG201121
 * PMM Issues fixes
 *
 * Issue fixed for  Attribute capturing for multiple items
 *
 * Revision 1.23.2.35.4.19.2.12  2013/06/11 14:30:20  schepuri
 * Error Code Change DEBUG to DEBUG
 *
 * Revision 1.23.2.35.4.19.2.11  2013/05/15 07:36:54  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.23.2.35.4.19.2.10  2013/05/14 14:45:40  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.23.2.35.4.19.2.9  2013/04/24 15:54:26  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fixes
 *
 * Revision 1.23.2.35.4.19.2.8  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.23.2.35.4.19.2.7  2013/04/10 05:37:39  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.23.2.35.4.19.2.6  2013/04/08 08:47:03  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.23.2.35.4.19.2.5  2013/03/26 13:27:42  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.23.2.35.4.19.2.4  2013/03/21 14:18:14  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.23.2.35.4.19.2.3  2013/03/13 13:57:17  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Time Stamp related changes.
 *
 * Revision 1.23.2.35.4.19.2.2  2013/03/06 09:48:26  gkalla
 * CASE201112/CR201113/LOG201121
 * Lexjet Files merging for Standard bundle
 *
 * Revision 1.23.2.35.4.19.2.1  2013/03/01 15:01:12  rmukkera
 * code is merged from FactoryMation production as part of Standard bundle
 *
 * Revision 1.23.2.35.4.19  2013/02/06 14:38:20  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.23.2.35.4.18  2013/02/05 15:34:32  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.23.2.35.4.17  2013/02/01 15:22:50  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.23.2.35.4.16  2013/01/25 06:57:24  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related to wrong location
 *
 * Revision 1.23.2.35.4.15  2013/01/21 09:49:20  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * RF Picking fine tuning in Carton scan.
 *
 * Revision 1.23.2.35.4.14  2013/01/17 06:03:25  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Carton override by zone.
 *
 * Revision 1.23.2.35.4.13  2012/12/31 05:53:10  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Inventory Sync issue fixes
 * .
 *
 * Revision 1.23.2.35.4.12  2012/12/16 01:39:08  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA after go live Fixes.
 *
 * Revision 1.23.2.35.4.11  2012/12/04 09:44:59  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA UAT Fixes.
 *
 * Revision 1.23.2.35.4.10  2012/11/30 23:56:48  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA UAT Fixes.
 *
 * Revision 1.23.2.35.4.9  2012/11/26 16:50:49  gkalla
 * CASE201112/CR201113/LOG201121
 * Case 20120953 for RF inventory move
 *
 * Revision 1.23.2.35.4.8  2012/11/12 15:44:48  gkalla
 * CASE201112/CR201113/LOG201121
 * RF Qty exception
 *
 * Revision 1.23.2.35.4.7  2012/11/01 14:55:23  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.23.2.35.4.6  2012/10/29 20:43:34  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GUUSA UAT ISSUE FIXES
 *
 * Revision 1.23.2.35.4.5  2012/10/23 17:08:39  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form branch.
 *
 * Revision 1.23.2.35.4.4  2012/10/11 10:27:04  gkalla
 * CASE201112/CR201113/LOG201121
 * Pick confirmation and other inventory issues
 *
 * Revision 1.23.2.35.4.3  2012/10/07 23:06:03  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue Fixes for FISK,BOOMBAH and TDG.
 *
 * Revision 1.23.2.35.4.2  2012/10/04 10:28:16  grao
 * CASE201112/CR201113/LOG201121
 * Converting multilanguage with given suggestions
 *
 * Revision 1.23.2.35.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.23.2.35  2012/09/14 20:12:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production Issue Fixes for FISK and BOOMBAH.
 *
 * Revision 1.23.2.30  2012/08/25 20:56:10  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Item Fulfillment Issue for FISK.
 * Case # 20120719
 *
 * Revision 1.23.2.29  2012/08/10 07:45:10  spendyala
 * CASE201112/CR201113/LOG201121
 * Invalid Parameter to SKU field while creating rec in Open task.
 *
 * Revision 1.23.2.28  2012/08/08 15:14:59  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Kit to order
 *
 * Revision 1.23.2.27  2012/08/08 07:05:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Updating few more fields while overriding the Container Lp.
 *
 * Revision 1.23.2.26  2012/08/07 12:10:52  spendyala
 * CASE201112/CR201113/LOG201121
 * Added new function to override All Container Lp with valid new Container LP entered by User.
 *
 * Revision 1.23.2.25  2012/08/07 09:13:37  gkalla
 * CASE201112/CR201113/LOG201121
 * To restrict not to enter existing Container LP
 *
 * Revision 1.23.2.24  2012/06/02 09:17:56  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to picking resolved.
 *
 * Revision 1.23.2.23  2012/05/25 22:27:10  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.23.2.22  2012/05/17 12:29:39  schepuri
 * CASE201112/CR201113/LOG201121
 * modified CONTAINER label with CARTON
 *
 * Revision 1.23.2.21  2012/05/09 07:52:37  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Partial Status Handling
 *
 * Revision 1.23.2.20  2012/05/04 22:52:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.23.2.19  2012/05/03 21:17:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.23.2.18  2012/04/26 07:10:21  mbpragada
 * CASE201112/CR201113/LOG201121
 * fixed the issue unable to scan the item when we placed two items in a single
 *  location.
 *
 * Revision 1.23.2.17  2012/04/24 13:49:35  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Picking Qty Exception (with REPLNS)
 *
 * Revision 1.23.2.16  2012/04/23 14:56:09  mbpragada
 * CASE201112/CR201113/LOG201121
 * fixed the issues in skip location functionality in RF Picking
 *
 * Revision 1.23.2.15  2012/04/19 15:00:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Zone Picking
 *
 * Revision 1.23.2.14  2012/04/18 14:27:31  mbpragada
 * CASE201112/CR201113/LOG201121
 * added Skip Location functionality
 *
 * Revision 1.23.2.13  2012/04/11 11:12:11  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing
 *
 * Revision 1.23.2.12  2012/04/09 15:41:48  spendyala
 * CASE201112/CR201113/LOG201121
 * Trimming the Container No if the user had typed any blank space .
 *
 * Revision 1.23.2.11  2012/04/06 15:02:21  spendyala
 * CASE201112/CR201113/LOG201121
 * Showing error when container LP is invalid
 *
 * Revision 1.23.2.10  2012/03/29 06:30:32  snimmakayala
 * CASE201112/CR201113/LOG201121
 * STG Locatoion update issue for outbound inventory records.
 *
 * Revision 1.42  2012/03/29 06:11:29  snimmakayala
 * CASE201112/CR201113/LOG201121
 * STG Locatoion update issue for outbound inventory records.
 *
 * Revision 1.41  2012/03/12 07:56:29  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.19  2012/03/09 16:38:16  gkalla
 * CASE201112/CR201113/LOG201121
 * Changed the operator any to anyof  while fetching generate location
 *
 * Revision 1.4  2011/10/03 09:02:42  jrnarsupalli
 * CASE201112/CR201113/LOG201121
 * RM Comments: Corrected CVS header to have CVS key words.
 *
 *
 *****************************************************************************/


function PickingContainerNo(request, response){
	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();

	if (request.getMethod() == 'GET') {
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('Debug', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7;

		if( getLanguage == 'es_ES')
		{
			st1 = "N&#218;MERO DE EMPAQUE: ";
			st2 = "TAMA&#209;O DEL EMPAQUE:";
			st3 = "INGRESAR / ESCANEAR DEL N&#218;MERO DE EMPAQUE";
			st4 = "INTRODUZCA EL TAMA&#209;O";
			st5 = "ENVIAR";
			st6 = "ANTERIOR";				
		}
		else
		{
			st1 = "CARTON NO : ";
			st2 = "CARTON SIZE : ";
			st3 = "ENTER/SCAN CARTON NO ";
			st4 = "ENTER SIZE ";
			st5 = "SEND";
			st6 = "PREV";


		}
		var getWaveno = request.getParameter('custparam_waveno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocationId = request.getParameter('custparam_beginLocation');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getItemName = request.getParameter('custparam_itemname');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');
		//Case # 20126303�Start
		if(request.getParameter('custparam_Newendlocid')!=null &&request.getParameter('custparam_Newendlocid')!='' && request.getParameter('custparam_Newendlocid')!='null' && request.getParameter('custparam_Newendlocid')!='undefined')
		{
			getEndLocInternalId = request.getParameter('custparam_Newendlocid');
			getInvoiceRefNo=request.getParameter('custparam_NewInvRecId');

		}
		//Case # 20126303� End
		var getEnteredLocation = request.getParameter('custparam_endlocation');
		var getBeginBinLocation = request.getParameter('custparam_beginlocationname');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var getSerialNo = request.getParameter('custparam_serialno');
		var vClusterNo = request.getParameter('custparam_clusterno');
		var vBatchno = request.getParameter('custparam_batchno');		
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var pickType=request.getParameter('custparam_picktype');
		var name=request.getParameter('name');
		var getOrderNo=request.getParameter('custparam_ebizordno');
		var whLocation = request.getParameter('custparam_whlocation');
		var getZoneNo=request.getParameter('custparam_ebizzoneno');
		var getRemQty = request.getParameter('custparam_remqty');
		var NextRecordId=request.getParameter('custparam_nextrecordid');
		var NextRecordinternalId=request.getParameter('custparam_nextrecordinternalid');//from f8 button
		var getItem = '';

		nlapiLogExecution('Debug', 'getItemName', getItemName);
		nlapiLogExecution('Debug', 'NextRecordinternalId', NextRecordinternalId);
		if(getItemName==null || getItemName=='')
		{
			//removed as on 051113 by suman to increase the performance.
			/*var Itype = nlapiLookupField('item', getItemInternalId, 'recordType');
			var ItemRec = nlapiLoadRecord(Itype, getItemInternalId);
			var getItem = ItemRec.getFieldValue('itemid');*/
			var filter=new Array();
			if(getItemInternalId!=null&&getItemInternalId!="")
				filter[0]=new nlobjSearchFilter("internalid",null,"anyof",getItemInternalId);

			var column=new Array();
			column[0]=new nlobjSearchColumn("itemid");
			column[1]=new nlobjSearchColumn("description");
			var Itemdescription='';
			var getItem="";
			var searchres=nlapiSearchRecord("item",null,filter,column);
			if(searchres!=null&&searchres!="")
			{
				getItem=searchres[0].getValue("itemid");
				Itemdescription=searchres[0].getValue("description");
			}
			//end of code as on 051113
		}
		else
		{
			getItem=getItemName;
		}

		var itemType=request.getParameter('custparam_itemType');
		var ActBatchno = request.getParameter('custparam_Actbatchno');
		var ExpBatchno=request.getParameter('custparam_Expbatchno');
		var NextBatchno=request.getParameter('custparam_nextbatchno');

		//code added on 15Feb 2012 by suman
		var EntLoc = request.getParameter('custparam_EntLoc');
		var EntLocRec=request.getParameter('custparam_EntLocRec');
		var EntLocID=request.getParameter('custparam_EntLocId');
		var ExceptionFlag = request.getParameter('custparam_Exc');
		var getnextExpectedQuantity = request.getParameter('custparam_nextexpectedquantity');
		nlapiLogExecution('Debug', 'EntLoc', EntLoc);
		nlapiLogExecution('Debug', 'EntLocRec', EntLocRec);
		nlapiLogExecution('Debug', 'EntLocID', EntLocID);
		nlapiLogExecution('Debug', 'ExceptionFlag', ExceptionFlag);
		nlapiLogExecution('Debug', 'getnextExpectedQuantity', getnextExpectedQuantity);
		/***upto here***/
		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');
		//end of code as of 15Feb.

		var getPickQty=0;
		nlapiLogExecution('Debug', 'request.getParameter(custparam_enteredqty)', request.getParameter('custparam_enteredqty'));
		getPickQty=request.getParameter('custparam_enteredqty');
		if(getPickQty == "" || getPickQty == null)
		{
			getPickQty=request.getParameter('custparam_expectedquantity');
		}
		var getReason=request.getParameter('custparam_enteredReason');
		var getnextExpectedQuantity = request.getParameter('custparam_nextexpectedquantity');

		var getLPContainerSize= '';

		var str = 'EntLoc. = ' + EntLoc + '<br>';
		str = str + 'EntLocRec. = ' + EntLocRec + '<br>';	
		str = str + 'EntLocID. = ' + EntLocID + '<br>';	
		str = str + 'ExceptionFlag. = ' + ExceptionFlag + '<br>';	
		str = str + 'getnextExpectedQuantity. = ' + getnextExpectedQuantity + '<br>';	
		str = str + 'getItem. = ' + getItem + '<br>';	
		str = str + 'getOrderNo. = ' + getOrderNo + '<br>';	
		str = str + 'getPickQty. = ' + getPickQty + '<br>';	
		str = str + 'getExpectedQuantity. = ' + getExpectedQuantity + '<br>';	
		str = str + 'getReason. = ' + getReason + '<br>';	
		str = str + 'getEnteredLocation. = ' + getEnteredLocation + '<br>';	
		str = str + 'getContainerLpNo. = ' + getContainerLpNo + '<br>';	
		str = str + 'getDOLineId1. = ' + getDOLineId + '<br>';	
		nlapiLogExecution('Debug', 'Parameter Values', str);

		try
		{
			nlapiLogExecution('Debug', 'Lading Open Task...', getRecordInternalId);
			var SORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', getRecordInternalId);
			getContainerLpNo= SORec.getFieldValue('custrecord_container_lp_no');
			getLPContainerSize= SORec.getFieldText('custrecord_container');
			getDOLineId = SORec.getFieldValue('custrecord_ebiz_cntrl_no');
		}
		catch(exp)
		{
			nlapiLogExecution('Debug', 'Exception in Loading Open Task', exp);
		}
		nlapiLogExecution('Debug', 'getDOLineId2', getDOLineId);

		nlapiLogExecution('Debug', 'getContainerLpNo', getContainerLpNo);

		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no');
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		//html = html + " document.getElementById('entercontainerno').focus();";   

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 

		//html = html + "	  alert(evt.keyCode);";
		//html = html + "	  alert(node.type);";

		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";

		//html = html + "	  alert(document.getElementById('cmdSend').disabled);";

		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><label>" + name + "</label> ## WAVE# :<label>" + getWaveno + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";	
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +"<label>" + getContainerLpNo + "</label>";
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";
		html = html + "				<input type='hidden' name='hdnFetchedContainerLPNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";		
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationId' value='" + getBeginLocationId + "'>";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemName' value='" + getItemName + "'>";
		html = html + "				<input type='hidden' name='hdnItemDescription' value='" + getItemDescription + "'>";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEnteredLocation' value='" + getEnteredLocation + "'>";
		html = html + "				<input type='hidden' name='hdnBeginBinLocation' value='" + getBeginBinLocation + "'>";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnserialno' value=" + getSerialNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vClusterNo + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value=" + vBatchno + ">";		
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnNextLocation' value='" + NextLocation + "'>";
		html = html + "				<input type='hidden' name='hdnNextItem' value=" + NextItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnName' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + getLPContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnebizOrdNo' value=" + getOrderNo + ">";
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		html = html + "				<input type='hidden' name='hdnReason' value=" + getReason + ">";
		html = html + "				<input type='hidden' name='hdnActQty' value=" + getPickQty + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdnremqty' value=" + getRemQty + ">";
		//code added on 15Feb 2012
		html = html + "				<input type='hidden' name='hdnNextRecordinternalId' value=" + NextRecordinternalId + ">";
		html = html + "				<input type='hidden' name='hdnEntLoc' value=" + EntLoc + ">";
		html = html + "				<input type='hidden' name='hdnEntLocRec' value=" + EntLocRec + ">";
		html = html + "				<input type='hidden' name='hdnEntLocID' value=" + EntLocID + ">";
		html = html + "				<input type='hidden' name='hdnExceptionFlag' value=" + ExceptionFlag + ">";
		html = html + "				<input type='hidden' name='hdnitemtype' value=" + itemType + ">";
		html = html + "				<input type='hidden' name='hdnactlot' value=" + ActBatchno + ">";
		html = html + "				<input type='hidden' name='hdnexplot' value=" + ExpBatchno + ">";
		html = html + "				<input type='hidden' name='hdnnextlot' value=" + NextBatchno + ">";
		//End of code.
		html = html + "				<input type='hidden' name='hdnebizzoneno' value=" + getZoneNo + ">";
		html = html + "				<input type='hidden' name='hdnextExpectedQuantity' value=" + getnextExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnNextrecordid' value=" + NextRecordId + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st2 +"<label>" + getLPContainerSize + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st3;//ENTER/SCAN CARTON NO ;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entercontainerno' id='entercontainerno' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st4;//ENTER SIZE 
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entersize' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st5 +" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					"+ st6 +" <input name='cmdPrevious' type='submit' value='F7'/>";
//		html = html + "					SUSPEND <input name='cmdSuspend' type='submit' value='F10'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('entercontainerno').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('Debug', 'Into Response', 'Into Response');

		var getLanguage = request.getParameter('hdngetLanguage');

		var st7;

		if( getLanguage == 'es_ES')
		{
			st7 = "CAJA NO V&#193;LIDO #";

		}
		else
		{
			st7 = "INVALID CARTON #";

		}

		nlapiLogExecution('Debug', 'Time Stamp at the start of response',TimeStampinSec());
		var vRemaningqty=0,vDisplayedQty=0,vActqty=0;
		var vZoneId=request.getParameter('hdnebizzoneno');
		vActqty = request.getParameter('hdnActQty');
		var vReason = request.getParameter('hdnReason');
		var vhdnNextRecordinternalId = request.getParameter('hdnNextRecordinternalId');
		var TotalWeight=0;
		var getEnteredContainerNo = request.getParameter('entercontainerno');
		if(getEnteredContainerNo==null || getEnteredContainerNo=="")
		{
			getEnteredContainerNo=request.getParameter('hdnFetchedContainerLPNo');
		}
		var getFetchedContainerNo = request.getParameter('hdnFetchedContainerLPNo');
		var getRecordInternalId = request.getParameter('hdnRecordInternalId');
		var getContainerSize=request.getParameter('hdnContainerSize');

		if(getContainerSize==null || getContainerSize=="")
		{
			getContainerSize = request.getParameter('entersize');
		}			
		var getWaveNo = request.getParameter('hdnWaveNo');
		var RecordInternalId = request.getParameter('hdnRecordInternalId');
		var ContainerLPNo = request.getParameter('hdnContainerLpNo');
		var BeginLocation = request.getParameter('hdnBeginLocationId');
		var vbatchno = request.getParameter('hdnbatchno');		
		var ItemNo = request.getParameter('hdnItemInternalId');
		var ItemDesc = request.getParameter('hdnItemDescription');
		var Item = request.getParameter('hdnItem');
		var ItemName = request.getParameter('hdnItemName');
		var PickQty = request.getParameter('hdnExpectedQuantity');
		var RcId = request.getParameter('hdnRecordInternalId');
		var EndLocation = request.getParameter('hdnEndLocInternalId');
		var RecCount = request.getParameter('hdnRecCount');
		var NextShowLocation=request.getParameter('hdnNextLocation');
		var NextShowItem=request.getParameter('hdnNextItem');
		var OrdName=request.getParameter('hdnName');
		var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var BeginLocationName = request.getParameter('hdnBeginBinLocation');
		var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
		var OrderLineNo = request.getParameter('hdnOrderLineNo');
		var SerialNo = request.getParameter('hdnserialno');
		var vClusterNo = request.getParameter('hdnClusterNo');
		var vBatchno = request.getParameter('hdnbatchno');	
		var vRemQty = request.getParameter('hdnremqty');
		var NextrecordID=request.getParameter('hdnNextrecordid');

		//code added on 15Feb by suman
		var EntLoc = request.getParameter('hdnEntLoc');
		var EntLocRec = request.getParameter('hdnEntLocRec');
		var EntLocID = request.getParameter('hdnEntLocID');
		var ExceptionFlag = request.getParameter('hdnExceptionFlag');
		if(ExceptionFlag=='L')
			if(EntLocID!=null&&EntLocID!="")
				EndLocation=EntLocID;
		//End of code.

		var vCarrier;
		var vSite;
		var vCompany;
		var stgDirection="OUB";
		var vDoname=request.getParameter('hdnName');
		var vdono=request.getParameter('hdnDOLineId');
		var vPrevinvrefno=request.getParameter('hdnInvoiceRefNo');
		var SalesOrderInternalId=request.getParameter('hdnebizOrdNo');
		var vPickType = request.getParameter('hdnpicktype');
		vSite = request.getParameter('hdnwhlocation');
		nlapiLogExecution('Debug', 'RcId', RcId);
		var str = 'Container. = ' + request.getParameter('hdnContainerSize') + '<br>';
		str = str + 'vActqty. = ' + request.getParameter('hdnActQty') + '<br>';	
		str = str + 'getEnteredContainerNo. = ' + request.getParameter('entercontainerno') + '<br>';	
		str = str + 'OrdName. = ' + OrdName + '<br>';	
		str = str + 'NextShowLocation. = ' + NextShowLocation + '<br>';	
		str = str + 'getRecordInternalId. = ' + getRecordInternalId + '<br>';	
		str = str + 'getFetchedContainerNo. = ' + getFetchedContainerNo + '<br>';	
		str = str + 'getContainerSize. = ' + getContainerSize + '<br>';	
		str = str + 'NextShowItem. = ' + NextShowItem + '<br>';	
		str = str + 'ItemNo. = ' + ItemNo + '<br>';	
		str = str + 'SalesOrderInternalId. = ' + SalesOrderInternalId + '<br>';	
		str = str + 'vPickType. = ' + vPickType + '<br>';	
		str = str + 'vSite. = ' + vSite + '<br>';	
		str = str + 'getWaveNo. = ' + getWaveNo + '<br>';	
		str = str + 'InvoiceRefNo. = ' + InvoiceRefNo + '<br>';	
		str = str + 'ItemName. = ' + ItemName + '<br>';	
		str = str + 'EndLocation. = ' + EndLocation + '<br>';	
		str = str + 'BeginLocationName. = ' + BeginLocationName + '<br>';	

		nlapiLogExecution('Debug', 'Parameter Values in Response', str);

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		// This variable is to hold the SO# entered.
		var SOarray = new Array();
		SOarray["custparam_language"] = getLanguage;
		SOarray["custparam_error"] = st7;//'INVALID CARTON #';
		SOarray["custparam_screenno"] = '13';
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		SOarray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
		SOarray["custparam_ebizordno"] = request.getParameter('hdnebizOrdNo');
		var vSkipId=request.getParameter('hdnskipid');
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');
		vSkipId=0;
		vSite = request.getParameter('hdnwhlocation');
		nlapiLogExecution('Debug', 'vSite', vSite);

		var vPickType = request.getParameter('hdnpicktype');

		SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
		if(request.getParameter('hdnNextrecordid')!=null&&request.getParameter('hdnNextrecordid')!="")	
			SOarray["custparam_nextrecordid"] = request.getParameter('hdnNextrecordid');
		var nextrecid = request.getParameter('hdnNextrecordid');

		SOarray["custparam_recordinternalid"] = request.getParameter('hdnRecordInternalId');
		SOarray["custparam_containerlpno"] = request.getParameter('hdnContainerLpNo');
		SOarray["custparam_expectedquantity"] = request.getParameter('hdnExpectedQuantity');
		SOarray["custparam_beginLocation"] = request.getParameter('hdnBeginLocationId');
		SOarray["custparam_item"] = request.getParameter('hdnItem');
		SOarray["custparam_itemname"] = request.getParameter('hdnItemName');
		SOarray["custparam_itemdescription"] = request.getParameter('hdnItemDescription');
		SOarray["custparam_iteminternalid"] = request.getParameter('hdnItemInternalId');
		SOarray["custparam_dolineid"] = request.getParameter('hdnDOLineId');
		SOarray["custparam_invoicerefno"] = request.getParameter('hdnInvoiceRefNo');
		SOarray["custparam_beginlocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
		SOarray["custparam_orderlineno"] = request.getParameter('hdnOrderLineNo');
		SOarray["custparam_serialno"] = request.getParameter('hdnserialno');
		SOarray["custparam_clusterno"] = request.getParameter('hdnClusterNo');
		SOarray["custparam_batchno"] = vbatchno;
		SOarray["custparam_nextlocation"] = NextShowLocation;
		SOarray["custparam_nextiteminternalid"] = NextShowItem;
		SOarray["custparam_newcontainerlp"] = getEnteredContainerNo;
		SOarray["custparam_nextexpectedquantity"] = request.getParameter('hdnextExpectedQuantity');
		var nextexpqty= request.getParameter('hdnextExpectedQuantity');
		SOarray["name"] = OrdName;
		var ItemType=request.getParameter('hdnitemtype');
		SOarray["custparam_itemType"] = ItemType;
		SOarray["custparam_Actbatchno"]=request.getParameter('hdnactlot');
		SOarray["custparam_Expbatchno"] = request.getParameter('hdnexplot');
		SOarray["custparam_nextbatchno"] = request.getParameter('hdnnextlot');
		SOarray["custparam_nextrecordinternalid"] = vhdnNextRecordinternalId;
		nlapiLogExecution('Debug', 'Navigating To vhdnNextRecordinternalId', vhdnNextRecordinternalId);

		var itemSubtype = nlapiLookupField('item', ItemNo, ['recordType', 'custitem_ebiz_captureattribute']);
		var itemattribute = itemSubtype.custitem_ebiz_captureattribute;

		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  request.getParameter('hdnebizzoneno');
		}
		else
			SOarray["custparam_ebizzoneno"] = '';
		nlapiLogExecution('Debug', 'NextLocation', NextShowLocation);
		if (sessionobj!=context.getUser()) 
		{
			try
			{
				//Case # 20145843  start
				var Systemrules = SystemRuleForStockAdjustment(vSite);
				//Case # 20145843  End
				nlapiLogExecution('ERROR', 'Systemrules', Systemrules);
				if(Systemrules == null || Systemrules =='' || Systemrules.length==0)
					Systemrules=null;
				//nlapiLogExecution('Debug','Order Line Number',SORec.getFieldValue('custrecord_line_no'));

				//	if the previous button 'F7' is clicked, it has to go to the previous screen 
				//  ie., it has to go to accept SO #.
				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}
				if (optedEvent == 'F7') 
				{
					//response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, SOarray);
					response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);
				}
				else 
				{

					var opentaskcount=0;

					//opentaskcount=getOpenTasksCount(getWaveNo,getFetchedContainerNo,vPickType,SalesOrderInternalId,vdono,vZoneId);
					nlapiLogExecution('Debug', 'Time Stamp at the start of getOpenTasksCount',TimeStampinSec());
					opentaskcount=getOpenTasksCount(getWaveNo,getFetchedContainerNo,vPickType,SalesOrderInternalId,OrdName,vZoneId,vSite);
					nlapiLogExecution('Debug', 'Time Stamp at the end of getOpenTasksCount',TimeStampinSec());

					var opentaskcountNav=0;
					var SORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RcId);
					var getContainerLPNo= SORec.getFieldValue('custrecord_container_lp_no');
					getLPContainerSize= SORec.getFieldText('custrecord_container');
					var pickMethod=SORec.getFieldValue('custrecord_ebizmethod_no');
					if(pickMethod!=null && pickMethod!='' && pickMethod!='null')
					{
						pickMethod=parseInt(pickMethod);
					}
					//opentaskcount=getOpenTasksCount(getWaveNo,getFetchedContainerNo,vPickType,SalesOrderInternalId,vdono,vZoneId);
					nlapiLogExecution('Debug', 'Time Stamp at the start of getOpenTasksCountForNavigation',TimeStampinSec());
					opentaskcountNav=getOpenTasksCountForNavigation(getWaveNo,getFetchedContainerNo,vPickType,SalesOrderInternalId,OrdName,vZoneId,vSite,pickMethod);
					nlapiLogExecution('Debug', 'Time Stamp at the end of getOpenTasksCountForNavigation',TimeStampinSec());
					//opentaskcount=getOpenTasksCount(getWaveNo,getEnteredContainerNo);

					nlapiLogExecution('Debug', 'opentaskcount', opentaskcount);
					nlapiLogExecution('Debug', 'opentaskcountNav', opentaskcountNav);
					//For fine tuning pick confirmation and doing autopacking in user event after last item
					var IsitLastPick='F';
					vSkipId=0;
					if(opentaskcount > parseFloat(vSkipId) + 1)
						IsitLastPick='F';
					else
						IsitLastPick='T';

					//For qty exception
					nlapiLogExecution('Debug', 'RcId', RcId);
					var InvQOH=0;

					if(request.getParameter('entercontainerno')==''||request.getParameter('entercontainerno')==null)
					{
						var cartonrequiredflag = getSystemRule('RF Picking Carton # Scan Required?');
						nlapiLogExecution('Debug', 'cartonrequiredflag', cartonrequiredflag);

						if(cartonrequiredflag=='Y')
						{
							SOarray["custparam_error"] = 'PLEASE ENTER/SCAN CARTON#';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							return;
						}
					}

					/*var invtholdflag = IsInvtonHold(ItemNo,EndLocInternalId);
					if(invtholdflag=='T')
					{
						SOarray["custparam_screenno"] = '13';	
						SOarray["custparam_error"]='CYCLE COUNT IS IN PROGRESS FOR THIS LOCATION. PLEASE DO THE LOCATION EXCEPTION TO COMPLETE THIS PICK';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
					}*/

					if (getEnteredContainerNo==''||getEnteredContainerNo==null||getEnteredContainerNo.replace(/\s+$/,"") == getFetchedContainerNo)
					{
						SOarray["custparam_newcontainerlp"] = getFetchedContainerNo;

						nlapiLogExecution('Debug', 'getEnteredContainerNo = getFetchedContainerNo');

						nlapiLogExecution('Debug', 'Time Stamp at the start of getSKUCubeAndWeightforconfirm',TimeStampinSec());
						var arrDims = getSKUCubeAndWeightforconfirm(ItemNo, 1);
						nlapiLogExecution('Debug', 'Time Stamp at the end of getSKUCubeAndWeightforconfirm',TimeStampinSec());
						var itemCube = 0;
						var itemWeight=0;						

						if (arrDims[0] != "" && (!isNaN(arrDims[1]))){
							//	these are commented because here  PickQty is excepted qty but we have to cal based on act qty
							itemCube = (parseFloat(PickQty) * parseFloat(arrDims[0]));
							itemWeight = (parseFloat(PickQty) * parseFloat(arrDims[1]));//	
						}

						var ContainerCube;					
						var containerInternalId;
						var ContainerSize;
						nlapiLogExecution('Debug', 'getContainerSize', getContainerSize);	

						if(getContainerSize=="" || getContainerSize==null || getContainerSize=='SHIPASIS')
						{
							getContainerSize=ContainerSize;					
						}
						nlapiLogExecution('Debug', 'getContainerSize', getContainerSize);	
						nlapiLogExecution('Debug', 'Time Stamp at the start of getContainerCubeAndTarWeight',TimeStampinSec());
						//var arrContainerDetails = getContainerCubeAndTarWeight("",getContainerSize);	
						var arrContainerDetails = getContainerCubeAndTarWeight("",getContainerSize,vSite);// Case# 20149997
						nlapiLogExecution('Debug', 'Time Stamp at the end of getContainerCubeAndTarWeight',TimeStampinSec());
						if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) {

							ContainerCube =  parseFloat(arrContainerDetails[0]);						
							containerInternalId = arrContainerDetails[3];
							ContainerSize=arrContainerDetails[4];
							TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));//now added
						} 
						nlapiLogExecution('Debug', 'ContainerSizeInternalId', containerInternalId);	
						nlapiLogExecution('Debug', 'ContainerSize', ContainerSize);	

						//UpdateOpenTask,fulfillmentorder

						nlapiLogExecution('Debug', 'Time Stamp at the start of UpdateRFFulfillOrdLine',TimeStampinSec());
				UpdateRFFulfillOrdLine(vdono,vActqty,ItemNo);
						nlapiLogExecution('Debug', 'Time Stamp at the end of UpdateRFFulfillOrdLine',TimeStampinSec());
						nlapiLogExecution('Debug', 'Before Process', RcId);	

						nlapiLogExecution('Debug', 'Time Stamp at the start of UpdateRFOpenTask',TimeStampinSec());
						var vResults= UpdateRFOpenTask(RcId,vActqty, containerInternalId,getEnteredContainerNo,itemCube,itemWeight,EndLocation,
								vReason,PickQty,IsitLastPick,ExceptionFlag,EntLocRec,ItemNo,SalesOrderInternalId,vRemQty,InvoiceRefNo,SerialNo,Systemrules,request);
						if(vResults != null && vResults.indexOf('ERROR') != -1)
						{
							SOarray["custparam_error"] = 'Insufficient Inventory.  Please do quantity exception';//'INVALID CARTON #';
							SOarray["custparam_screenno"] = '12EXP';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							nlapiLogExecution('Debug', 'Error: ', 'Insufficient Inventory');
						}
						nlapiLogExecution('Debug', 'Time Stamp at the end of UpdateRFOpenTask',TimeStampinSec());

						//create new record in opentask,fullfillordline when we have qty exception

						vRemaningqty = PickQty-vActqty;
						var recordcount=1;//it is iterator in GUI
						nlapiLogExecution('Debug', 'vDisplayedQty', PickQty);	
						nlapiLogExecution('Debug', 'vActqty', vActqty);	
						if(PickQty != vActqty)
						{
							nlapiLogExecution('Debug', 'inexception condition1', 'inexception condition');	
							//case#:Start 20124624-->serial# item related issue fixes.
							CreateNewShortPick(RcId,PickQty,vActqty,vdono,recordcount,getEnteredContainerNo,TotalWeight,vbatchno,vRemaningqty,Systemrules,request);//have to check for wt and cube calculation //Case# 20149927 (request passing)
//							end
						}

						nlapiLogExecution('Debug', 'opentaskcount', opentaskcount);
						nlapiLogExecution('Debug', 'BeginLocation', BeginLocation);
						nlapiLogExecution('Debug', 'NextShowLocation', NextShowLocation);
						nlapiLogExecution('Debug', 'ItemNo', ItemNo);
						nlapiLogExecution('Debug', 'NextShowItem', NextShowItem);
						nlapiLogExecution('Debug', 'opentaskcountNav', opentaskcountNav);
						if(opentaskcountNav >  1)
						{
							if(BeginLocation != NextShowLocation)
							{ 
								if(itemattribute == 'T')
								{
									nlapiLogExecution('Debug', 'Navigating To1', 'Item Attribute1');
									SOarray["custparam_beginlocationname"]= NextShowLocation;
									//SOarray["custparam_nooflocrecords"] = parseInt(locreccount)-1;
									SOarray["custparam_redirectscreen"] = "LOC";
									SOarray["custparam_containerlpno"] = getEnteredContainerNo;
									//	response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_item_attribute', 'customdeploy_ebiz_rf_item_attribute_di', false, SOarray);
								}
								else
								{
									SOarray["custparam_beginLocationname"]=null;
									SOarray["custparam_iteminternalid"]=null;
									SOarray["custparam_expectedquantity"]=null;
									nlapiLogExecution('Debug', 'Navigating To1', 'Picking Location');
									response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);
								}

							}
							else if(ItemNo != NextShowItem)
							{ 
								if(itemattribute == 'T')
								{
									nlapiLogExecution('Debug', 'Navigating To1', 'Item Attribute1');
									SOarray["custparam_beginlocationname"]= NextShowLocation;
									//SOarray["custparam_nooflocrecords"] = parseInt(locreccount)-1;
									SOarray["custparam_redirectscreen"] = "LOC";
									SOarray["custparam_containerlpno"] = getEnteredContainerNo;
									//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_item_attribute', 'customdeploy_ebiz_rf_item_attribute_di', false, SOarray);
								}
								else
								{
									nlapiLogExecution('Debug', 'Navigating To1', 'Picking Item');
									SOarray["custparam_iteminternalid"] = NextShowItem;
									SOarray["custparam_beginLocationname"]=null;
									SOarray["custparam_expectedquantity"]=null;
									if(vhdnNextRecordinternalId != null && vhdnNextRecordinternalId != '')
									{
										SOarray["custparam_recordinternalid"] = vhdnNextRecordinternalId;
										SOarray["custparam_nextrecordinternalid"] = vhdnNextRecordinternalId;
									}
									else
									{
										SOarray["custparam_recordinternalid"] = nextrecid;
										SOarray["custparam_nextrecordinternalid"] = nextrecid;
									}
									nlapiLogExecution('Debug', 'Navigating To vhdnRecordinternalId', SOarray["custparam_recordinternalid"]);
									nlapiLogExecution('Debug', 'Navigating To vhdnNextRecordinternalId', SOarray["custparam_nextrecordinternalid"]);
									//SOarray["custparam_iteminternalid"]=null;
									response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);								
								}
							}
							else if(ItemNo == NextShowItem)
							{ 
								SOarray["custparam_iteminternalid"] = NextShowItem;
								SOarray["custparam_beginLocationname"]=null;
								SOarray["custparam_expectedquantity"]=nextexpqty;
								SOarray["custparam_Actbatchno"]=request.getParameter('hdnnextlot');
								SOarray["custparam_Expbatchno"] = request.getParameter('hdnnextlot');
								//SOarray["custparam_iteminternalid"]=null;
								nlapiLogExecution('Debug', 'Navigating To1', 'Picking Qty Confirm');
								response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);								

							}

						}
						else{
							nlapiLogExecution('Debug', 'Before AutoPacking 1 : Sales Order InternalId', SalesOrderInternalId);
							//For fine tuning pick confirmation and doing autopacking in user event after last item				
							nlapiLogExecution('Debug', 'Navigating To1', 'Stage Location');

							if(itemattribute == 'T')
							{
								SOarray["custparam_containerlpno"] = getEnteredContainerNo;
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_item_attribute', 'customdeploy_ebiz_rf_item_attribute_di', false, SOarray);
							}
							else
							{
								response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);
							}
						}
					}

					else if (getEnteredContainerNo != '' && getEnteredContainerNo != getFetchedContainerNo)
					{
						SOarray["custparam_newcontainerlp"] = getEnteredContainerNo;

						nlapiLogExecution('Debug', 'getEnteredContainerNo', 'userdefined');
						nlapiLogExecution('Debug', 'Time Stamp at the start of fnValidateEnteredContLP',TimeStampinSec());
						var vOpenTaskRecs=fnValidateEnteredContLP(getEnteredContainerNo,SalesOrderInternalId,vSite);
						nlapiLogExecution('Debug', 'Time Stamp at the end of fnValidateEnteredContLP',TimeStampinSec());
						if(vOpenTaskRecs != null && vOpenTaskRecs != '')
						{
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							nlapiLogExecution('Debug', 'Error: ', 'Container# is invalid');
						}
						else
						{	
							var result = fnValidateContainer(getEnteredContainerNo,getFetchedContainerNo, '2','2',vSite);
							if(result == false)
							{
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								nlapiLogExecution('Debug', 'Error: ', 'Container# is invalid');
								return;
							}

							var getEnteredContainerNoPrefix = getEnteredContainerNo.substring(0, 3).toUpperCase();
							//var LPReturnValue = ebiznet_LPRange_CL_withLPType(getEnteredContainerNo.replace(/\s+$/,""), '2','2',vSite);//'2'UserDefiend,'2'PICK
							var LPReturnValue = ebiznet_LPRange_CL_withLPType(getEnteredContainerNo, '2','2',vSite);//'2'UserDefiend,'2'PICK
							nlapiLogExecution('Debug', 'Time Stamp at the end of ebiznet_LPRange_CL_withLPType',TimeStampinSec());
							if(LPReturnValue == true){
								nlapiLogExecution('Debug', 'getItem', ItemNo);
								var arrDims = getSKUCubeAndWeightforconfirm(ItemNo, 1);
								nlapiLogExecution('Debug', 'Time Stamp at the end of getSKUCubeAndWeightforconfirm',TimeStampinSec());
								var itemCube = 0;
								var itemWeight=0;						
								if (arrDims[0] != "" && (!isNaN(arrDims[1]))){
									itemCube = (parseFloat(PickQty) * parseFloat(arrDims[0]));
									itemWeight = (parseFloat(PickQty) * parseFloat(arrDims[1]));//									
								} 

								var ContainerCube;					
								var containerInternalId;
								var ContainerSize;
								if(getContainerSize=="" || getContainerSize==null || getContainerSize=='SHIPASIS')
								{
									getContainerSize=ContainerSize;
									nlapiLogExecution('Debug', 'ContainerSize', ContainerSize);	
									ContainerCube = itemCube;
									TotalWeight=itemWeight;
								}	
								else
								{
									//var arrContainerDetails = getContainerCubeAndTarWeight("",getContainerSize);
									var arrContainerDetails = getContainerCubeAndTarWeight("",getContainerSize,vSite); //Case# 20149997
									nlapiLogExecution('Debug', 'Time Stamp at the end of getContainerCubeAndTarWeight',TimeStampinSec());
									if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) {

										ContainerCube =  parseFloat(arrContainerDetails[0]);						
										containerInternalId = arrContainerDetails[3];
										ContainerSize=arrContainerDetails[4];
										TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));// added on 28 nov
									} 
								}
								nlapiLogExecution('Debug', 'ContainerSizeInternalId', containerInternalId);				
								//Insert LP Record
								var ResultText='';
								CreateRFMasterLPRecord(getEnteredContainerNo,containerInternalId,ContainerCube,TotalWeight,getContainerSize,ResultText);
								nlapiLogExecution('Debug', 'Time Stamp at the end of CreateRFMasterLPRecord',TimeStampinSec());
								//added by suman as on 05/08/12.
								//update all the open picks with the new container LP#.
								UpdateAllContainerLp(getEnteredContainerNo,getFetchedContainerNo,SalesOrderInternalId,OrdName,getWaveNo,vZoneId,vPickType);
								//end of the code as of 05/08/12.
								nlapiLogExecution('Debug', 'Time Stamp at the end of UpdateAllContainerLp',TimeStampinSec());
								UpdateRFFulfillOrdLine(vdono,vActqty);
								nlapiLogExecution('Debug', 'Time Stamp at the end of UpdateRFFulfillOrdLine',TimeStampinSec());
								//UpdateOpenTask
								var vResults= UpdateRFOpenTask(RcId,vActqty, containerInternalId,getEnteredContainerNo,itemCube,itemWeight,EndLocation,
										vReason,PickQty,IsitLastPick,ExceptionFlag,EntLocRec,ItemNo,SalesOrderInternalId,vRemQty,InvoiceRefNo,SerialNo,Systemrules,request);
						if(vResults != null && vResults.indexOf('ERROR') != -1)
						{
							SOarray["custparam_error"] = 'Insufficient Inventory.  Please do quantity exception';//'INVALID CARTON #';
							SOarray["custparam_screenno"] = '12EXP';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							nlapiLogExecution('Debug', 'Error: ', 'Insufficient Inventory');
						}
								nlapiLogExecution('Debug', 'Time Stamp at the end of UpdateRFOpenTask',TimeStampinSec());
								//Below code is merged from Lexjet production and its lexjet specific, so commented theis code by Ganesh on 5th Mar 2013***/
								//UpdateNewContainelpinShipmanifest(getEnteredContainerNo,getFetchedContainerNo,OrdName);
								/***upto here***/
								updatelastpicktaskforoldcarton(getFetchedContainerNo,SalesOrderInternalId);
								nlapiLogExecution('Debug', 'Time Stamp at the end of updatelastpicktaskforoldcarton',TimeStampinSec());

								//create new record in opentask,fullfillordline when we have qty exception
								vRemaningqty = PickQty-vActqty;
								var recordcount=1;//it is iterator in GUI
								if(PickQty != vActqty)
								{
									nlapiLogExecution('Debug', 'inexception condition2', 'inexception condition');	
									//case#:Start 20124624-->serial# item related issue fixes.
									//Case# 20149929 starts (here passing Systemrules)
									//CreateNewShortPick(RcId,PickQty,vActqty,vdono,recordcount,getEnteredContainerNo,TotalWeight,vbatchno,vRemaningqty,request);//have to check for wt and cube calculation//Case# 20149927 (request passing
									CreateNewShortPick(RcId,PickQty,vActqty,vdono,recordcount,getEnteredContainerNo,TotalWeight,vbatchno,vRemaningqty,Systemrules,request);
									//Case# 20149929 ends
									//end
								}

								if(opentaskcountNav > 1)
								{
									if(BeginLocation != NextShowLocation)
									{ 							
										if(itemattribute == 'T')
										{
											nlapiLogExecution('Debug', 'Navigating To1', 'Item Attribute1');
											SOarray["custparam_beginlocationname"]= NextShowLocation;
											//SOarray["custparam_nooflocrecords"] = parseInt(locreccount)-1;
											SOarray["custparam_redirectscreen"] = "LOC";
											SOarray["custparam_containerlpno"] = getEnteredContainerNo;
											//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_item_attribute', 'customdeploy_ebiz_rf_item_attribute_di', false, SOarray);
										}
										else
										{

											SOarray["custparam_beginLocationname"]=null;
											SOarray["custparam_iteminternalid"]=null;
											SOarray["custparam_expectedquantity"]=null;
											nlapiLogExecution('Debug', 'Navigating To', 'Picking Location');
											//SOarray["custparam_nooflocrecords"] = parseInt(locreccount)-1;
											response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);
										}

										/*SOarray["custparam_beginLocationname"]=null;
								SOarray["custparam_iteminternalid"]=null;
								SOarray["custparam_expectedquantity"]=null;
								response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);								
								nlapiLogExecution('Debug', 'Navigating To1', 'Picking Location');*/
									}
									else if(ItemNo != NextShowItem)
									{ 

										if(itemattribute == 'T')
										{
											nlapiLogExecution('Debug', 'Navigating To2', 'Item Attribute2');
											SOarray["custparam_item"] = NextShowItem;
											SOarray["custparam_iteminternalid"] = NextShowItem;
											//SOarray["custparam_nooflocrecords"] = parseFloat(locreccount)-1;
											SOarray["custparam_redirectscreen"] = "ITEM";
											SOarray["custparam_containerlpno"] = getEnteredContainerNo;
											//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
											response.sendRedirect('SUITELET', 'customscript_ebiz_rf_item_attribute', 'customdeploy_ebiz_rf_item_attribute_di', false, SOarray);
										}
										else
										{
											SOarray["custparam_iteminternalid"] = NextShowItem;
											SOarray["custparam_beginLocationname"]=null;
											SOarray["custparam_expectedquantity"]=null;
											SOarray["custparam_iteminternalid"] = NextShowItem;
											response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);								
											nlapiLogExecution('Debug', 'Navigating To1', 'Picking Item');
										}


										/*SOarray["custparam_iteminternalid"] = NextShowItem;
								SOarray["custparam_beginLocationname"]=null;
								SOarray["custparam_expectedquantity"]=null;
								SOarray["custparam_iteminternalid"] = NextShowItem;
								response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);								
								nlapiLogExecution('Debug', 'Navigating To1', 'Picking Item');*/
									}
									else if(ItemNo == NextShowItem)
									{ 
										SOarray["custparam_iteminternalid"] = NextShowItem;
										SOarray["custparam_beginLocationname"]=null;
										SOarray["custparam_expectedquantity"]=nextexpqty;
										SOarray["custparam_Actbatchno"]=request.getParameter('hdnnextlot');
										SOarray["custparam_Expbatchno"] = request.getParameter('hdnnextlot');
										response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);								
										nlapiLogExecution('Debug', 'Navigating To1', 'Picking Qty Confirm');
										nlapiLogExecution('Debug', 'test2', 'done');
									}
								}
								else{
									nlapiLogExecution('Debug', 'Before AutoPacking 2 : Sales Order InternalId', SalesOrderInternalId);
									nlapiLogExecution('Debug', 'Navigating To2', 'Stage Location');

									if(itemattribute == 'T')
									{
										SOarray["custparam_containerlpno"] = getEnteredContainerNo;
										response.sendRedirect('SUITELET', 'customscript_ebiz_rf_item_attribute', 'customdeploy_ebiz_rf_item_attribute_di', false, SOarray);
									}
									else
									{
										response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);
									}
								}
							}
							else  
							{
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								nlapiLogExecution('Debug', 'Error: ', 'Container# is invalid');
							}
						}
					}
					else if((getEnteredContainerNo != "" && getEnteredContainerNo != null) || getFetchedContainerNo=="" || getFetchedContainerNo==null)
					{
						nlapiLogExecution('Debug', 'else', 'userdefined');
						nlapiLogExecution('Debug', 'Before Process', RcId);	
						var vResults= UpdateRFOpenTask(RcId,vActqty, null,null,null,null,EndLocation,vReason,PickQty,
								IsitLastPick,ExceptionFlag,EntLocRec,ItemNo,SalesOrderInternalId,vRemQty,InvoiceRefNo,'',Systemrules,request);
						if(vResults != null && vResults.indexOf('ERROR') != -1)
						{
							SOarray["custparam_error"] = 'Insufficient Inventory.  Please do quantity exception';//'INVALID CARTON #';
							SOarray["custparam_screenno"] = '12EXP';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							nlapiLogExecution('Debug', 'Error: ', 'Insufficient Inventory');
						}	
						//create new record in opentask,fullfillordline when we have qty exception
						vRemaningqty = PickQty-vActqty;
						var recordcount=1;//it is iterator in GUI
						if(PickQty != vActqty)
						{
							nlapiLogExecution('Debug', 'inexception condition3', 'inexception condition');	
							//case#:Start 20124624-->serial# item related issue fixes.
							CreateNewShortPick(RcId,PickQty,vActqty,vdono,recordcount,getEnteredContainerNo,TotalWeight,vbatchno,vRemQty,Systemrules,request);//have to check for wt and cube calculation // Case# 20149927 (request paasing)
							//end
						}

						if(opentaskcountNav >  1)
						{
							if(BeginLocation != NextShowLocation)
							{  

								if(itemattribute == 'T')
								{
									nlapiLogExecution('Debug', 'Navigating To1', 'Item Attribute1');
									SOarray["custparam_beginlocationname"]= NextShowLocation;
									//	SOarray["custparam_nooflocrecords"] = parseInt(locreccount)-1;
									SOarray["custparam_redirectscreen"] = "LOC";
									SOarray["custparam_containerlpno"] = getEnteredContainerNo;
									//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_item_attribute', 'customdeploy_ebiz_rf_item_attribute_di', false, SOarray);
								}
								else
								{
									response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);								
									nlapiLogExecution('Debug', 'Navigating To1', 'Picking Location');
								}
							}
							else if(ItemNo != NextShowItem)
							{ 
								if(itemattribute == 'T')
								{
									nlapiLogExecution('Debug', 'Navigating To1', 'Item Attribute1');
									SOarray["custparam_beginlocationname"]= NextShowLocation;
									//	SOarray["custparam_nooflocrecords"] = parseInt(locreccount)-1;
									SOarray["custparam_redirectscreen"] = "LOC";
									SOarray["custparam_containerlpno"] = getEnteredContainerNo;
									//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_clusitem_attribute', 'customdeploy_ebiz_rf_clusitem_atribut_di', false, SOarray);
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_item_attribute', 'customdeploy_ebiz_rf_item_attribute_di', false, SOarray);
								}
								else
								{
									SOarray["custparam_iteminternalid"] = NextShowItem;
									response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);								
									nlapiLogExecution('Debug', 'Navigating To1', 'Picking Item');
								}
							}
							else if(ItemNo == NextShowItem)
							{ 
								response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);								
								nlapiLogExecution('Debug', 'Navigating To1', 'Picking Qty Confirm');
								nlapiLogExecution('Debug', 'test3', 'done');
							}

						}
						else{
							nlapiLogExecution('Debug', 'Before AutoPacking 3 : Sales Order InternalId', SalesOrderInternalId);
							nlapiLogExecution('Debug', 'Navigating To3', 'Stage Location');

							if(itemattribute == 'T')
							{
								SOarray["custparam_containerlpno"] = getEnteredContainerNo;
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_item_attribute', 'customdeploy_ebiz_rf_item_attribute_di', false, SOarray);
							}
							else
							{
								response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);
							}
						}
					}
					else
					{
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						nlapiLogExecution('Debug', 'Error: ', 'Wave # not found');
					}
				}
			}
			catch (e) 
			{
				nlapiLogExecution('Debug', 'Exception: ', e);
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			} 
			finally 
			{					
				context.setSessionObject('session', null);
			}
		}
		else
		{
			SOarray["custparam_screenno"] = '12';
			SOarray["custparam_error"] = 'CARTON ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
		}
	}
}

function getOpenTasksCount(waveno,containerlpno,vPickType,SalesOrderInternalId,OrdName,vZoneId,vSite)
{
	nlapiLogExecution('Debug', 'Into getOpenTasksCount...');

	var str = 'waveno. = ' + waveno + '<br>';
	str = str + 'containerlpno. = ' + containerlpno + '<br>';	
	str = str + 'vPickType. = ' + vPickType + '<br>';	
	str = str + 'SalesOrderInternalId. = ' + SalesOrderInternalId + '<br>';
	str = str + 'OrdName. = ' + OrdName + '<br>';
	str = str + 'vZoneId. = ' + vZoneId + '<br>';
	str = str + 'vSite. = ' + vSite + '<br>';

	nlapiLogExecution('Debug', 'Function Parameters', str);



	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_ebizsitepickrule', null, 'anyof', vSite));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizpickmethod');
	columns[1] = new nlobjSearchColumn('custrecord_cartonization_method','custrecord_ebizpickmethod');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_stagedetermination','custrecord_ebizpickmethod');

	var pickRuleSearchResult = new nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters, columns);

	var pickMethodId ;
	var Stagedet;
	if(pickRuleSearchResult != null){
		for (var i=0; i<pickRuleSearchResult.length; i++){			
			pickMethodId = pickRuleSearchResult[0].getValue('custrecord_ebizpickmethod');

			Stagedet = pickRuleSearchResult[i].getValue('custrecord_ebiz_stagedetermination','custrecord_ebizpickmethod');
		}
	}
	nlapiLogExecution('DEBUG', 'pickMethodId', pickMethodId);


	/*var fields=new Array();	
	fields[0]='custrecord_ebiz_stagedetermination';
	var pickmethodcolumns = nlapiLookupField('customrecord_ebiznet_pick_method', pickMethodId, fields);	
	if(pickmethodcolumns!=null)

	var Stagedet = pickmethodcolumns.custrecord_ebiz_stagedetermination;*/

	nlapiLogExecution('DEBUG', 'Stagedet', Stagedet);

	var openreccount=0;
	var SOFilters = new Array();
	var SOColumns = new Array();

	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK

	/*if(Stagedet == 3)//Carton Level
	{
		nlapiLogExecution('ERROR', 'containerlpno', containerlpno);
		if(containerlpno !=null && containerlpno !='' && containerlpno !='null')
			SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', containerlpno));
	}*/	
	if(containerlpno !=null && containerlpno !='' && containerlpno !='null')
		SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', containerlpno));
	if(OrdName != null && OrdName != '')
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));

	if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && waveno != null && waveno != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(waveno)));
	}	 
	if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
	{
		nlapiLogExecution('Debug', 'OrdNo inside If', OrdName);
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
	}
	if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && SalesOrderInternalId!=null && SalesOrderInternalId!="" && SalesOrderInternalId!= "null")
	{
		nlapiLogExecution('Debug', 'SO Id inside If', SalesOrderInternalId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId));
	}
	if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
	{
		nlapiLogExecution('Debug', 'Zone # inside If', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
	}

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	if(SOSearchResults!=null && SOSearchResults!=''&& SOSearchResults.length>0)
		openreccount=SOSearchResults.length;	

	nlapiLogExecution('Debug', 'Out of getOpenTasksCount...',openreccount);

	return openreccount;

}


function updatelastpicktaskforoldcarton(containerlpno,SOOrder)
{
	nlapiLogExecution('Debug', 'Into updatelastpicktaskforoldcarton', containerlpno);
	nlapiLogExecution('Debug', 'SOOrder', SOOrder);

	var openreccount=0;

	var SOFilters = new Array();
	var SOColumns = new Array();
	var updatetask='T';

	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK	

	if(SOOrder!=null&&SOOrder!="")
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', parseFloat(SOOrder)));

	if(containerlpno!=null && containerlpno!='' && containerlpno!='null')
		SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', containerlpno));	
	else
		SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'isempty'));	
	SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty'));

	SOColumns[0] = new nlobjSearchColumn('custrecord_wms_status_flag');
	SOColumns[1] = new nlobjSearchColumn('custrecord_device_upload_flag');

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	if(SOSearchResults!=null && SOSearchResults!=''&& SOSearchResults.length>0)
	{
		for (var i = 0; i < SOSearchResults.length; i++){

			var status  = SOSearchResults[i].getValue('custrecord_wms_status_flag');
			var deviceuploadflag  = SOSearchResults[i].getValue('custrecord_device_upload_flag');

			nlapiLogExecution('Debug', 'status', status);
			nlapiLogExecution('Debug', 'deviceuploadflag', deviceuploadflag);

			if(status=='9')//Pick Generated
			{
				updatetask='F';
				break;
			}
		}
		nlapiLogExecution('Debug', 'updatetask', updatetask);

		if(updatetask=='T')
		{
			nlapiSubmitField('customrecord_ebiznet_trn_opentask', SOSearchResults[0].getId(),'custrecord_device_upload_flag', 'T');
		}
	}

	nlapiLogExecution('Debug', 'Out of updatelastpicktaskforoldcarton', updatetask);
}

function getAllContainers(containersize)
{
	nlapiLogExecution('Debug', 'into getAllContainers', '');
	var containerslist = new Array();

	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_containertype', null, 'anyof', ['1','4']));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_containername', null, 'is', containersize));				

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('Internalid');

	containerslist = nlapiSearchRecord('customrecord_ebiznet_container', null, filters, columns);

	nlapiLogExecution('Debug', 'out of getAllContainers', '');

	return containerslist;
}

/**
 * @param RcId
 * @param PickQty
 * @param containerInternalId
 * @param getEnteredContainerNo
 * @param itemCube
 * @param itemWeight
 * @param EndLocation
 * @param vReason
 * @param ActPickQty
 * @param IsItLastPick
 * @param ExceptionFlag
 * @param NewRecId
 */
function UpdateRFOpenTask(RcId, ActPickQty, containerInternalId, getEnteredContainerNo, itemCube,itemWeight,
		EndLocation,vReason,expPickQty,IsItLastPick,ExceptionFlag,NewRecId,ItemNo,SalesOrderInternalId,vRemQty,InvoiceRefNo,SerialNo,Systemrules,request)
{
	nlapiLogExecution('Debug', 'into UpdateRFOpenTask containerlp: ', getEnteredContainerNo);
	nlapiLogExecution('Debug', 'into UpdateRFOpenTask containerlp: ', RcId);
	nlapiLogExecution('Debug', 'SerialNo: ', SerialNo);
	nlapiLogExecution('ERROR', 'Systemrules: ', Systemrules);

	try
	{
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RcId);
		var vinvrefno=transaction.getFieldValue('custrecord_invref_no');
		var vwmsstatus = transaction.getFieldValue('custrecord_wms_status_flag');

		nlapiLogExecution('DEBUG', 'vwmsstatus: ', vwmsstatus);

		if(vwmsstatus!='8')
		{
			//To check for sufficient inv available or not
			var InvttranNew = nlapiLoadRecord('customrecord_ebiznet_createinv', vinvrefno);
			nlapiLogExecution('Debug', 'InvttranNew', InvttranNew);		 
			var InvPresentQOH = InvttranNew.getFieldValue('custrecord_ebiz_qoh');
			if(InvPresentQOH == null || InvPresentQOH == '')
				InvPresentQOH=0;
			if(parseFloat(InvPresentQOH) < parseFloat(ActPickQty))
				return 'ERROR,No Sufficient Inv';
			//End here

			transaction.setFieldValue('custrecord_wms_status_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')
			if(ActPickQty!= null && ActPickQty !="")
				transaction.setFieldValue('custrecord_act_qty', ActPickQty);
			if(containerInternalId!= null && containerInternalId !="")
				transaction.setFieldValue('custrecord_container', containerInternalId);
			if(EndLocation!=null && EndLocation!="")
				transaction.setFieldValue('custrecord_actendloc', EndLocation);	
			if(itemWeight!=null && itemWeight!="")
				transaction.setFieldValue('custrecord_total_weight', parseFloat(itemWeight).toFixed(4));
			if(itemCube!=null && itemCube!="")
				transaction.setFieldValue('custrecord_totalcube', parseFloat(itemCube).toFixed(4));	
			if(getEnteredContainerNo!=null && getEnteredContainerNo!="")
				transaction.setFieldValue('custrecord_container_lp_no', getEnteredContainerNo);
			transaction.setFieldValue('custrecord_act_end_date', DateStamp());
			transaction.setFieldValue('custrecord_actualendtime',TimeStamp());
			if(vReason!=null && vReason!="")
				transaction.setFieldValue('custrecord_notes', vReason);	
			if(SerialNo!=null && SerialNo!="")
				transaction.setFieldValue('custrecord_serial_no', SerialNo);
			var currentContext = nlapiGetContext();  
			var currentUserID = currentContext.getUser();
			transaction.setFieldValue('custrecord_upd_ebiz_user_no',currentUserID);
			nlapiLogExecution('Debug', 'IsItLastPick: ', IsItLastPick);
			transaction.setFieldValue('custrecord_device_upload_flag',IsItLastPick);
			var vemployee = request.getParameter('custpage_employee');
			//case# 20148263 starts(New Inv Ref# updation)
			nlapiLogExecution('DEBUG', 'NewRecId', NewRecId);
			transaction.setFieldValue('custrecord_invref_no',NewRecId);
			//case# 20148263 end
			nlapiLogExecution('Debug', 'Updating RF Open Task Record Id: ', RcId);
			if(ActPickQty != expPickQty)
			{
				var vkititem = '';
				var vkititemtext = '';

				var filters = new Array(); 			 
				filters[0] = new nlobjSearchFilter('custrecord_sku', null, 'is', ItemNo);	
				filters[1] = new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', SalesOrderInternalId);	

				var columns1 = new Array(); 
				columns1[0] = new nlobjSearchColumn('custrecord_parent_sku_no' ); 

				var searchresultskititem = nlapiSearchRecord( 'customrecord_ebiznet_trn_opentask', null, filters, columns1 ); 
				if(searchresultskititem!=null && searchresultskititem!='')
				{
					vkititem = searchresultskititem[0].getValue('custrecord_parent_sku_no');
					vkititemtext = searchresultskititem[0].getText('custrecord_parent_sku_no');
				}

				var kititemTypesku ='';

				if(vkititem!=null && vkititem!='')
				{
					kititemTypesku = nlapiLookupField('item', vkititem, 'recordType');
				}

				if(kititemTypesku=='kititem')
				{
					nlapiLogExecution('Debug', 'vsalesordInternid', SalesOrderInternalId);
					nlapiLogExecution('Debug', 'kititem', vkititem);
					var kitfilters = new Array(); 			 
					kitfilters[0] = new nlobjSearchFilter('custrecord_parent_sku_no', null, 'anyof', vkititem);	
					kitfilters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]);	
					kitfilters[2] = new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', SalesOrderInternalId);	

					var kitcolumns1 = new Array(); 
					kitcolumns1[0] = new nlobjSearchColumn( 'custrecord_expe_qty' ); 			
					kitcolumns1[1] = new nlobjSearchColumn( 'custrecord_act_qty' );

					var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, kitfilters, kitcolumns1 ); 
					for(var z=0; z<searchresults.length;z++) 
					{										
						var kitrcid=searchresults[z].getId();
						var Expqty=searchresults[z].getValue('custrecord_expe_qty');
						var Actqty=searchresults[z].getValue('custrecord_act_qty');

						nlapiLogExecution('Debug', 'Actqty', Actqty);
						nlapiLogExecution('Debug', 'Expqty', Expqty);
						if(Actqty==Expqty || Actqty=='')
						{
							nlapiSubmitField('customrecord_ebiznet_trn_opentask', kitrcid, 'custrecord_kit_exception_flag', 'T');
						}
					}

				}

			}

			if(ExceptionFlag=='L')
			{
				var rec=nlapiLoadRecord('customrecord_ebiznet_createinv',NewRecId);
				var LotNO=rec.getFieldText('custrecord_ebiz_inv_lot');
				nlapiLogExecution('Debug','LotNO',LotNO);
				transaction.setFieldValue('custrecord_batch_no',LotNO);
			}

			nlapiSubmitRecord(transaction, false, true);
			nlapiLogExecution('Debug', 'Time Stamp after updating open task',TimeStampinSec());
			nlapiLogExecution('Debug', 'Updated RF Open Task successfully');
			nlapiLogExecution('Debug', 'ExceptionFlag',ExceptionFlag);
			nlapiLogExecution('Debug', 'Systemrules[0]',Systemrules[1]);
			nlapiLogExecution('Debug', 'Systemrules[1]',Systemrules[1]);
			if((SerialNo!=null && SerialNo!='') && SerialNo!='null' )
				UpdateSerialentry(RcId,SerialNo);
			//code added on 15Feb by suman.
			if(ExceptionFlag!='L')
			{
				try
				{
					nlapiLogExecution('Debug', 'Time Stamp at the start of deleteAllocations',TimeStampinSec());
					deleteAllocations(vinvrefno,ActPickQty,getEnteredContainerNo,expPickQty,vRemQty,SalesOrderInternalId,ItemNo,EndLocation,Systemrules);
					nlapiLogExecution('Debug', 'Time Stamp at the end of deleteAllocations',TimeStampinSec());
				}
				catch(exp)
				{
					nlapiLogExecution('Debug', 'Exception in deleteAllocations',exp);
				}
			}
			else
			{
				nlapiLogExecution('Debug', 'vinvrefno',vinvrefno);
				nlapiLogExecution('Debug', 'NewRecId',NewRecId);

				try
				{
					if(vinvrefno!=null && vinvrefno!='')
						deleteOldAllocations(vinvrefno,ActPickQty,getEnteredContainerNo,expPickQty,vRemQty,SalesOrderInternalId,ItemNo,EndLocation);
					deleteNewAllocations(NewRecId,ActPickQty,getEnteredContainerNo,expPickQty,vRemQty,SalesOrderInternalId,ItemNo,EndLocation);
				}
				catch(exp)
				{
					nlapiLogExecution('Debug', 'Exception in deleteOldNewAllocations',exp);
				}
			}
		}
		//End of code as of 15Feb.
	}
	catch(exp1)
	{
		nlapiLogExecution('Debug', 'Exception in UpdateRFOpenTask',exp1);
	}
}

/**
 * This function clear the allocation qty from inventory.
 * @param invrefno
 * @param PickQty
 */
function deleteAllocations(invrefno,ActPickQty,contlpno,expPickQty,remQty,SalesOrderInternalId,itemno,binlocation,Systemrules)
{
	nlapiLogExecution('Debug', 'Into deleteAllocations (Inv Ref NO)',invrefno);
	var str = 'Inv Ref No. = ' + invrefno + '<br>';
	str = str + 'CARTON LP. = ' + contlpno + '<br>';	
	str = str + 'ActPickQty. = ' + ActPickQty + '<br>';	
	str = str + 'expPickQty. = ' + expPickQty + '<br>';	
	str = str + 'remQty. = ' + remQty + '<br>';	
	str = str + 'SalesOrderInternalId. = ' + SalesOrderInternalId + '<br>';	
	str = str + 'itemno. = ' + itemno + '<br>';	
	str = str + 'binlocation. = ' + binlocation + '<br>';	
	nlapiLogExecution('Debug', 'Parameter Values', str);

	try
	{
		var invtfilters = new Array(); 			 
		invtfilters[0] = new nlobjSearchFilter('internalidnumber', null, 'equalto', invrefno);

		var invtcolumns1 = new Array(); 
		invtcolumns1[0] = new nlobjSearchColumn( 'custrecord_ebiz_inv_sku' ); 			
		invtcolumns1[1] = new nlobjSearchColumn( 'custrecord_ebiz_inv_binloc' );

		var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, invtfilters, invtcolumns1); 

		nlapiLogExecution('Debug', 'invtsearchresults', invtsearchresults);

		if(invtsearchresults!=null && invtsearchresults!='' && invtsearchresults.length>0)
		{
			nlapiLogExecution('Debug', 'invtsearchresults length', invtsearchresults.length);

			var scount=1;
			var invtrecid;
			var newqoh = 0;
			LABL1: for(var i=0;i<scount;i++)
			{	

				nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);
				try
				{
					var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
					nlapiLogExecution('Debug', 'Invttran', Invttran);
					var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
					var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');
					var Itemname = Invttran.getFieldText('custrecord_ebiz_inv_sku');
					var vNewAllocQty=0;
					if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {
						vNewAllocQty=parseInt(Invallocqty)- parseInt(expPickQty);
						if(parseFloat(vNewAllocQty)<0)
							vNewAllocQty=0;

						Invttran.setFieldValue('custrecord_ebiz_alloc_qty',parseInt(vNewAllocQty));
					}
					else
						Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);

					nlapiLogExecution('Debug', 'vNewAllocQty',vNewAllocQty);

					var remainqty=0;

					if(parseInt(expPickQty) != parseInt(ActPickQty))
					{

						nlapiLogExecution('Debug', 'Systemrules.length', Systemrules.length);
						nlapiLogExecution('Debug', 'Systemrules[0]', Systemrules[0]);
						nlapiLogExecution('Debug', 'Systemrules[1]', Systemrules[1]);

						if(Systemrules.length>0 && Systemrules[0]=='Y' && (Systemrules[1] =="STND" ||Systemrules[1] =="NOADJUST" ))
						{

//							if(remQty==null || remQty=='null'  || remQty=='' || isNaN(remQty))
//							{
//							newqoh = parseFloat(InvQOH) - parseFloat(expPickQty);
//							}
//							else
//							{
//							newqoh = parseFloat(InvQOH) - parseFloat(ActPickQty);
//							}

							if(Systemrules[1] =="STND")
								newqoh = parseFloat(InvQOH) - parseFloat(expPickQty);
							else if (Systemrules[1] =="NOADJUST")
								newqoh = parseFloat(InvQOH) - parseFloat(ActPickQty);
						}
						else if(Systemrules.length>0 && Systemrules[0]=='Y' && Systemrules[1] =="ADJUSTBINLOC")
						{
							// consider binlocation quantity

//							if(remQty==null || remQty=='null'  || remQty=='' || isNaN(remQty))
//							{
//							newqoh = parseInt(InvQOH) - parseInt(expPickQty);
//							}
//							else
//							{
//							newqoh = parseInt(vNewAllocQty) + parseInt(remQty);
//							}

							newqoh = parseFloat(InvQOH) - parseFloat(ActPickQty);

						}
						else
						{
							newqoh = parseFloat(InvQOH) - parseFloat(ActPickQty);
						}

					}
					else
					{
						newqoh = parseInt(InvQOH) - parseInt(ActPickQty);
					}
					nlapiLogExecution('Debug', 'newqoh',newqoh);
					Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(newqoh));  
					Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
					Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');
					var getlotnoid=Invttran.getFieldValue('custrecord_ebiz_inv_lot');
					var desc=Invttran.getFieldValue('custrecord_ebiz_itemdesc');
					var expdate=Invttran.getFieldValue('custrecord_ebiz_expdate');
					var vfifodate=Invttran.getFieldValue('custrecord_ebiz_inv_fifo');
					invtrecid = nlapiSubmitRecord(Invttran, false, true);
					nlapiLogExecution('Debug', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);

					if(invtrecid!=null && invtrecid!='' && parseFloat(ActPickQty)>0)
						CreateSTGInvtRecord(invtrecid, contlpno,ActPickQty,SalesOrderInternalId,getlotnoid,expdate,vfifodate,desc);

					if((parseInt(newqoh)) <= 0)
					{		
						nlapiLogExecution('Debug', 'Deleting record from inventory if QOH becomes zero', invrefno);
						var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
					}


				}
				catch(ex)
				{
					var exCode='CUSTOM_RECORD_COLLISION'; 
					var wmsE='Inventory record being updated by another user. Please try again...';
					if (ex instanceof nlobjError) 
					{	
						wmsE=ex.getCode() + '\n' + ex.getDetails();
						exCode=ex.getCode();
					}
					else
					{
						wmsE=ex.toString();
						exCode=ex.toString();
					} 

					nlapiLogExecution('Debug', 'Exception in RF Inv move : ', wmsE); 
					if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
					{ 
						scount=scount+1;
						continue LABL1;
					}
					else break LABL1;
				}
			}
		}
		else
		{
			clearItemAllocations(ActPickQty,contlpno,expPickQty,remQty,SalesOrderInternalId,itemno,binlocation);
		}	
	}
	catch(exp)
	{
		nlapiLogExecution('Debug', 'Exception in deleteAllocations',exp);
	}		
}

/**
 * This function creats outbound staging inventory
 * @param invtrecid
 * @param vContLp
 * @param vqty
 */

function CreateSTGInvtRecord(invtrecid, vContLp,vqty,SalesOrderInternalId,getlotnoid,expdate,vfifodate,desc) 
{
	nlapiLogExecution('Debug', 'Into CreateSTGInvtRecord');
	var str = 'Inv Ref No. = ' + invtrecid + '<br>';
	str = str + 'CARTON LP. = ' + vContLp + '<br>';	
	str = str + 'Qty. = ' + vqty + '<br>';	
	str = str + 'SalesOrderInternalId. = ' + SalesOrderInternalId + '<br>';	
	str = str + 'getlotnoid = ' + getlotnoid + '<br>';	
	str = str + 'expdate. = ' + expdate + '<br>';
	str = str + 'desc. = ' + desc + '<br>';
	nlapiLogExecution('Debug', 'STG INVT Details', str);

	try
	{
		var stgmInvtRec = nlapiCopyRecord('customrecord_ebiznet_createinv',invtrecid);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_lp', vContLp);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(vqty).toFixed(4));
		stgmInvtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(vqty).toFixed(4));
		stgmInvtRec.setFieldValue('custrecord_ebiz_alloc_qty', '0');
		stgmInvtRec.setFieldValue('custrecord_wms_inv_status_flag', '18');	//FLAG.INVENTORY.OUTBOUND('O') 		
		stgmInvtRec.setFieldValue('custrecord_invttasktype', '3'); //Task Type - PICK
		stgmInvtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
		stgmInvtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');	
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_note1', '');	
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_note2', '');	
		// Temporarily hard coded by Satish.N
		//stgmInvtRec.setFieldValue('custrecord_ebiz_inv_binloc', 7218);	
		if(SalesOrderInternalId!=null && SalesOrderInternalId!='')
			stgmInvtRec.setFieldValue('custrecord_ebiz_transaction_no', SalesOrderInternalId);	
		if(getlotnoid!=null&&getlotnoid!="")
		{
			stgmInvtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
			stgmInvtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
			stgmInvtRec.setFieldValue('custrecord_ebiz_inv_fifo', vfifodate);
		}
		stgmInvtRec.setFieldValue('custrecord_ebiz_itemdesc',desc);
		nlapiSubmitRecord(stgmInvtRec, false, true);
	}
	catch(exp)
	{
		nlapiLogExecution('Debug', 'Exception in CreateSTGInvtRecord',exp);
	}

	nlapiLogExecution('Debug', 'Out of CreateSTGInvtRecord');
}

/**
 * 
 * @param lp
 * @returns
 */
function containerRFLPExists(lp){
	var filters = new Array();
	nlapiLogExecution('Debug', 'lp:',lp); 
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lp));	 
	nlapiLogExecution('Debug', 'Master LP Feaching','Success');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
	return searchresults;
}

function OpenTaskRFLPExists(lp){
	var filters = new Array();
	nlapiLogExecution('Debug', 'lp:',lp); 
	filters.push( new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', lp));	 

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');

	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	return searchresults;
}

function getLPWeight(lp){
	var filters = new Array();
	nlapiLogExecution('Debug', 'lp:',lp); 
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lp));	 
	nlapiLogExecution('Debug', 'Master LP Feaching','Success');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
	var lpmastArr=new Array();
	nlapiLogExecution('Debug', 'Before:',lp); 
	if(searchresults != null && searchresults!= "" && searchresults.length>0)
	{
		lpmastArr.push(searchresults[0].getValue('custrecord_ebiz_lpmaster_lp'));
		lpmastArr.push(searchresults[0].getValue('custrecord_ebiz_lpmaster_totwght'));
		nlapiLogExecution('Debug', 'lp:',searchresults[0].getValue('custrecord_ebiz_lpmaster_lp')); 
		nlapiLogExecution('Debug', 'lpweight:',searchresults[0].getValue('custrecord_ebiz_lpmaster_totwght')); 
	}
	return lpmastArr;
}

function containerSizeExists(lp,SizeId){
	var filters = new Array();
	nlapiLogExecution('Debug', 'lp:',lp); 
	nlapiLogExecution('Debug', 'SizeId:',SizeId); 
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lp));
	filters.push( new nlobjSearchFilter('custrecord_ebiz_lpmaster_sizeid', null, 'anyof', SizeId));	
	nlapiLogExecution('Debug', 'Size Feaching','Success');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sizeid');
	columns[0].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);        	       	
	return searchresults;
}

/**
 * 
 * @param vContLpNo
 * @param vContainerSize
 * @param TotalWeight
 * @param ContainerCube
 */
function CreateRFMasterLPRecord(vContLpNo,containerInternalId,ContainerCube,ItemWeight,getContainerSize,ResultText)
{																							
	var contLPExists = containerRFLPExists(vContLpNo);

	var TotalWeight=ItemWeight;


	if(contLPExists==null){		

		var MastLP=nlapiCreateRecord('customrecord_ebiznet_master_lp');
		MastLP.setFieldValue('name', vContLpNo);
		MastLP.setFieldValue('custrecord_ebiz_lpmaster_lp', vContLpNo);
		if(containerInternalId!="" && containerInternalId!=null)
		{
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_sizeid', containerInternalId);
		}	
		nlapiLogExecution('Debug', 'TotalWeight',TotalWeight);
		nlapiLogExecution('Debug', 'ContainerCube',ContainerCube);
		if(TotalWeight!="" && TotalWeight!=null)
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_totwght', parseFloat(TotalWeight).toFixed(4));
		if(ContainerCube!="" && ContainerCube!=null)
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(ContainerCube).toFixed(4));
		if(ResultText != null && ResultText != '')
			MastLP.setFieldValue('custrecord_ebiz_lpmaster_sscc', ResultText);
		var currentContext = nlapiGetContext();		        		
		var retktoval = nlapiSubmitRecord(MastLP);
		nlapiLogExecution('Debug', 'Master LP Insertion','Success');

	}
	else
	{
		var recordId="";

		if(contLPExists!=null){


			for (var i = 0; i < contLPExists.length; i++){
				recordId = contLPExists[i].getId();
				LPWeight = contLPExists[i].getValue('custrecord_ebiz_lpmaster_totwght');
				nlapiLogExecution('Debug', 'recordId',recordId);		

				var MastLPUpdate = nlapiLoadRecord('customrecord_ebiznet_master_lp', recordId);	
				TotWeight=parseFloat(ItemWeight)+parseFloat(LPWeight);
				nlapiLogExecution('Debug', 'itemWeight',ItemWeight);
				nlapiLogExecution('Debug', 'LPWeight',LPWeight);
				nlapiLogExecution('Debug', 'TotWeight',TotWeight);

				if(TotWeight!="" && TotWeight!=null)
					MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_totwght', parseFloat(TotWeight).toFixed(4));
				if(ContainerCube!="" && ContainerCube!=null)
					MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(ContainerCube).toFixed(4));
				if(ResultText != null && ResultText != '')
					MastLPUpdate.setFieldValue('custrecord_ebiz_lpmaster_sscc', ResultText);
				var currentContext = nlapiGetContext();	        				
				nlapiSubmitRecord(MastLPUpdate, false, true);        				 
				nlapiLogExecution('Debug', 'Master LP Updation','Success');

			}

		}
	}
}

function UpdateRFFulfillOrdLine(vdono,vActqty,ItemNo)
{
	var pickqtyfinal =0;
	nlapiLogExecution('Debug', 'into UpdateRFFulfillOrdLine : ', vdono);

	try
	{

		var doline = nlapiLoadRecord('customrecord_ebiznet_ordline',vdono);	
		var vParentItem = doline.getFieldValue('custrecord_ebiz_linesku');
		
		var oldpickQty = doline.getFieldValue('custrecord_pickqty');
		var ordQty = doline.getFieldValue('custrecord_ord_qty');
		var pickgenqty = doline.getFieldValue('custrecord_pickgen_qty');

		if(isNaN(oldpickQty) || oldpickQty==null || oldpickQty=='')
			oldpickQty=0;

		if(isNaN(ordQty) || ordQty==null || ordQty=='')
			ordQty=0;

		if(isNaN(vActqty) || vActqty==null || vActqty=='')
			vActqty=0;

		if(isNaN(pickgenqty) || pickgenqty==null || pickgenqty=='')
			pickgenqty=0;

		pickqtyfinal=parseFloat(oldpickQty)+parseFloat(vActqty);

		var str = 'pickqtyfinal. = ' + pickqtyfinal + '<br>';
		str = str + 'oldpickQty. = ' + oldpickQty + '<br>';	
		str = str + 'ordQty. = ' + ordQty + '<br>';	
		str = str + 'vActqty. = ' + vActqty + '<br>';	
		str = str + 'pickgenqty. = ' + pickgenqty + '<br>';	
		str = str + 'vParentItem. = ' + vParentItem + '<br>';	
		str = str + 'ItemNo. = ' + ItemNo + '<br>';	

		nlapiLogExecution('Debug', 'Qty Details', str);

			
		if( vParentItem !=null && vParentItem !='')
		{
			var kititemTypesku = nlapiLookupField('item', vParentItem, 'recordType');
			
			nlapiLogExecution('Debug', 'kititemTypesku', kititemTypesku);
			if(kititemTypesku=='kititem')
			{

				var filters = new Array(); 			 
				filters[0] = new nlobjSearchFilter('internalid', null, 'is', vParentItem);	
				filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');	
				filters[2] = new nlobjSearchFilter('type', null, 'is', 'Kit');

				var columns1 = new Array(); 
				columns1[0] = new nlobjSearchColumn( 'memberitem' ); 			
				columns1[1] = new nlobjSearchColumn( 'memberquantity' );

				var searchresults = nlapiSearchRecord( 'item', null, filters, columns1 );

				for(var q=0; searchresults!=null && q<searchresults.length;q++) 
				{
					var kitiCompItem=searchresults[q].getValue('memberitem');
					if(kitiCompItem == ItemNo)
					{
						var memberitemqty = searchresults[q].getValue('memberquantity');	

						var fopickgenqty = Math.floor(parseFloat(pickqtyfinal)/parseFloat(memberitemqty));

						var str1 = 'pickqtyfinal. = ' + pickqtyfinal + '<br>';
						str1 = str1 + 'memberitemqty = ' + memberitemqty + '<br>';	
						str1 = str1 + 'fopickgenqty = ' + fopickgenqty + '<br>';	
						str1 = str1 + 'kitiCompItem = ' + kitiCompItem + '<br>';	
						str1 = str1 + 'ItemNo = ' + ItemNo + '<br>';	

						nlapiLogExecution('Debug', 'Kit item Details', str1);
						doline.setFieldValue('custrecord_upddate', DateStamp());
						doline.setFieldValue('custrecord_pickqty', parseFloat(fopickgenqty));
						if(parseFloat(fopickgenqty)<parseFloat(ordQty))
							doline.setFieldValue('custrecord_linestatus_flag', '11'); //STATUS.OUTBOUND.PARTIALLY_PROCESSED
						else
							doline.setFieldValue('custrecord_linestatus_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')

						nlapiSubmitRecord(doline, false, true);
						break;
					}
				}

			}
			else
			{
				if(parseFloat(pickgenqty)>=parseFloat(pickqtyfinal))
				{
					doline.setFieldValue('custrecord_upddate', DateStamp());
					doline.setFieldValue('custrecord_pickqty', parseFloat(pickqtyfinal));

					if(parseFloat(pickqtyfinal)<parseFloat(ordQty))
						doline.setFieldValue('custrecord_linestatus_flag', '11'); //STATUS.OUTBOUND.PARTIALLY_PROCESSED
					else
						doline.setFieldValue('custrecord_linestatus_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')

					nlapiSubmitRecord(doline, false, true);
				}
			}
		}
		
	}
	catch(exp)
	{
		nlapiLogExecution('Debug', 'Exception in UpdateRFFulfillOrdLine',exp);
	}

	nlapiLogExecution('Debug', 'Out of UpdateRFFulfillOrdLine ');
}

/**
 * This function clear the allocation qty from inventory.
 * @param invrefno
 * @param PickQty
 */
function deleteOldAllocations(invrefno,actPickQty,contlpno,expPickQty,vRemQty,SalesOrderInternalId,itemno,binlocation)
{
	nlapiLogExecution('Debug', 'Into deleteOldAllocations (Inv Ref NO)',invrefno);

	try
	{
		var scount=1;
		var invtrecid;

		LABL1: for(var i=0;i<scount;i++)
		{	

			nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);
			try
			{
				var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
				var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
				var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

				if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {

					if(parseFloat(Invallocqty)- parseFloat(expPickQty)>=0)
						Invttran.setFieldValue('custrecord_ebiz_alloc_qty',parseFloat(Invallocqty)- parseFloat(expPickQty));
					else
						Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);

				}
				else
				{
					Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);
				}

				//Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(InvQOH) - parseInt(ActPickQty)); 
				Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
				Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

				invtrecid = nlapiSubmitRecord(Invttran, false, true);
				nlapiLogExecution('Debug', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);

//				var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
//				var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
//				var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

//				if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {

//				if(parseFloat(Invallocqty)- parseFloat(expPickQty)>=0)
//				Invttran.setFieldValue('custrecord_ebiz_alloc_qty',parseFloat(Invallocqty)- parseFloat(expPickQty));
//				else
//				Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);

//				}
//				else
//				{
//				Invttran.setFieldValue('custrecord_ebiz_alloc_qty',0);
//				}

//				//Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(InvQOH) - parseInt(ActPickQty)); 
//				Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
//				Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

//				var invtrecid = nlapiSubmitRecord(Invttran, false, true);
//				nlapiLogExecution('Debug', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);


			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 
				/*var exceptionname='RF Picking';
				var functionality='RFPick';
				var trantype=3;
				var vInvRecId=invrefno;
				var vcontainerLp=contlpno;
				nlapiLogExecution('Debug', 'DetailsError', functionality);	
				nlapiLogExecution('Debug', 'vcontainerLp', vcontainerLp);
				nlapiLogExecution('Debug', 'vInvRecId', invrefno);
				var reference3=ActPickQty;
				var reference4 ="";
				var reference5 ="";
				var userId = nlapiGetUser();
				InsertExceptionLog(exceptionname,trantype, functionality, wmsE, invrefno, vcontainerLp,reference3,reference4,reference5, userId);*/
				nlapiLogExecution('Debug', 'Exception in RF Inv move : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}

		/*if(invtrecid!=null && invtrecid!='')
			CreateSTGInvtRecord(invtrecid, contlpno,pickqty);
		 */
		/*if((parseFloat(InvQOH) - parseFloat(ActPickQty)) == 0)
		{		
			nlapiLogExecution('Debug', 'Deleting record from inventory if QOH becomes zero', invrefno);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
		}*/
	}
	catch(exp)
	{
		nlapiLogExecution('Debug', 'Exception in deleteAllocations',exp);
	}

	nlapiLogExecution('Debug', 'Out of deleteOldAllocations (Inv Ref NO)',invrefno);
}

/**
 * This function clear the allocation qty from inventory.
 * @param invrefno
 * @param PickQty
 */
function deleteNewAllocations(invrefno,actPickQty,contlpno,expPickQty,vRemQty,SalesOrderInternalId,itemno,binlocation)
{
	nlapiLogExecution('Debug', 'Into deleteNewAllocations (Inv Ref NO)',invrefno);

	var str = 'invrefno. = ' + invrefno + '<br>';
	str = str + 'actPickQty. = ' + actPickQty + '<br>';	
	str = str + 'expPickQty. = ' + expPickQty + '<br>';	
	str = str + 'contlpno. = ' + contlpno + '<br>';	
	str = str + 'vRemQty. = ' + vRemQty + '<br>';	
	str = str + 'SalesOrderInternalId. = ' + SalesOrderInternalId + '<br>';	
	str = str + 'itemno. = ' + itemno + '<br>';	
	str = str + 'binlocation. = ' + binlocation + '<br>';	

	nlapiLogExecution('Debug', 'Parameter Details', str);

	try
	{
		var invtfilters = new Array(); 			 
		invtfilters[0] = new nlobjSearchFilter('internalidnumber', null, 'equalto', invrefno);

		var invtcolumns1 = new Array(); 
		invtcolumns1[0] = new nlobjSearchColumn( 'custrecord_ebiz_inv_sku' ); 			
		invtcolumns1[1] = new nlobjSearchColumn( 'custrecord_ebiz_inv_binloc' );

		var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, invtfilters, invtcolumns1); 

		nlapiLogExecution('Debug', 'invtsearchresults', invtsearchresults);

		if(invtsearchresults!=null && invtsearchresults!='' && invtsearchresults.length>0)
		{
			nlapiLogExecution('Debug', 'invtsearchresults length', invtsearchresults.length);
			var scount=1;
			var invtrecid;
			var InvQOH =0;
			LABL1: for(var i=0;i<scount;i++)
			{	

				nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);
				try
				{
					var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
					var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
					InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

					//Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(InvQOH) - (parseInt(expPickQty)-parseInt(vRemQty))); 
					Invttran.setFieldValue('custrecord_ebiz_qoh',parseInt(InvQOH) - parseInt(expPickQty)); 
					Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
					Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

					var invtrecid = nlapiSubmitRecord(Invttran, false, true);
					nlapiLogExecution('Debug', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);


				}
				catch(ex)
				{
					var exCode='CUSTOM_RECORD_COLLISION'; 
					var wmsE='Inventory record being updated by another user. Please try again...';
					if (ex instanceof nlobjError) 
					{	
						wmsE=ex.getCode() + '\n' + ex.getDetails();
						exCode=ex.getCode();
					}
					else
					{
						wmsE=ex.toString();
						exCode=ex.toString();
					} 

					nlapiLogExecution('Debug', 'Exception in RF Inv move : ', wmsE); 
					if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
					{ 
						scount=scount+1;
						continue LABL1;
					}
					else break LABL1;
				}
			}

			if(invtrecid!=null && invtrecid!='' && parseFloat(actPickQty)>0)
				CreateSTGInvtRecord(invtrecid, contlpno,actPickQty,SalesOrderInternalId);

			if((parseInt(InvQOH) - parseInt(expPickQty)) <= 0)
			{		
				nlapiLogExecution('Debug', 'Deleting record from inventory if QOH becomes zero', invrefno);
				var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
			}
		}
		else
		{
			clearItemAllocations(actPickQty,contlpno,expPickQty,vRemQty,SalesOrderInternalId,itemno,binlocation);
		}
	}
	catch(exp)
	{
		nlapiLogExecution('Debug', 'Exception in deleteAllocations',exp);
	}	

	nlapiLogExecution('Debug', 'Out of deleteNewAllocations (Inv Ref NO)',invrefno);
}



/**
 * @param getEnteredContainerNo
 * @param getFetchedContainerNo
 * @param SOOrder
 * @param OrdName
 */
function UpdateAllContainerLp(getEnteredContainerNo,getFetchedContainerNo,SOOrder,OrdName,waveno,vZoneId,vPickType)
{
	nlapiLogExecution('Debug','Into UpdateAllContainerLp');

	try
	{

		var str = 'waveno. = ' + waveno + '<br>';
		str = str + 'SOOrder. = ' + SOOrder + '<br>';	
		str = str + 'OrdName. = ' + OrdName + '<br>';
		str = str + 'vZoneId. = ' + vZoneId + '<br>';
		str = str + 'getFetchedContainerNo. = ' + getFetchedContainerNo + '<br>';
		str = str + 'getEnteredContainerNo. = ' + getEnteredContainerNo + '<br>';
		str = str + 'vPickType. = ' + vPickType + '<br>';

		nlapiLogExecution('Debug', 'Function Parameters', str);

		var context = nlapiGetContext();
		var orderno, containerno,ebizcntrlno,siteid,compid,containersize,totweight,internalid,item,waveno,
		beginloc,endloc,lot,actqty,expqty,uomlevel,empid,begindate,begintime,packcode,lp,lineno,Zoneno,ebizLP,
		taskassignedto;

		var otparent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent');
		var parentid = nlapiSubmitRecord(otparent); 
		var newParent = nlapiLoadRecord('customrecord_ebiz_throwaway_parent', parentid);

		var filteropentask=new Array();
		filteropentask.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
		filteropentask.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK	

		if(getFetchedContainerNo!=null && getFetchedContainerNo!='' && getFetchedContainerNo!='null')
			filteropentask.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getFetchedContainerNo));	
		else
			filteropentask.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'isempty'));	

		filteropentask.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));

		if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && waveno != null && waveno != "")
		{
			filteropentask.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(waveno)));
		}	 
		if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
		{
			filteropentask.push(new nlobjSearchFilter('name', null, 'is', OrdName));
		}
		if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && SOOrder!=null && SOOrder!="" && SOOrder!= "null")
		{
			filteropentask.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SOOrder));
		}
		if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
		{	
			filteropentask.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
		}

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('name');
		columns[1] = new nlobjSearchColumn('custrecord_container_lp_no');
		columns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		columns[4] = new nlobjSearchColumn('custrecord_comp_id');
		columns[5] = new nlobjSearchColumn('custrecord_wms_location');
		columns[6] = new nlobjSearchColumn('custrecord_container');
		columns[7] = new nlobjSearchColumn('custrecord_total_weight');
		columns[8] = new nlobjSearchColumn('custrecord_sku');
		columns[9] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[10] = new nlobjSearchColumn('custrecord_actendloc');
		columns[11] = new nlobjSearchColumn('custrecord_batch_no');
		columns[12] = new nlobjSearchColumn('custrecord_act_qty');
		columns[13] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[14] = new nlobjSearchColumn('custrecord_uom_level');
		columns[15] = new nlobjSearchColumn('custrecord_ebizuser');
		columns[16] = new nlobjSearchColumn('custrecordact_begin_date');
		columns[17] = new nlobjSearchColumn('custrecord_actualbegintime');
		columns[18] = new nlobjSearchColumn('custrecord_packcode');
		columns[19] = new nlobjSearchColumn('custrecord_lpno');
		columns[20] = new nlobjSearchColumn('custrecord_line_no');
		columns[21] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
		columns[22] = new nlobjSearchColumn('custrecord_ebiz_zoneid');
		columns[23] = new nlobjSearchColumn('custrecord_ebiz_lpno');
		columns[24] = new nlobjSearchColumn('custrecord_ebizzone_no');
		columns[25] = new nlobjSearchColumn('custrecord_taskassignedto');

		var SearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filteropentask, columns);
		if(SearchResults!=null&&SearchResults!="")
		{
			for ( var i = 0; i < SearchResults.length; i++) 
			{
				orderno = SearchResults[i].getValue('name');
				containerno = SearchResults[i].getValue('custrecord_container_lp_no');
				ebizorderno = SearchResults[i].getValue('custrecord_ebiz_order_no');
				ebizcntrlno = SearchResults[i].getValue('custrecord_ebiz_cntrl_no');
				compid = SearchResults[i].getValue('custrecord_comp_id');
				siteid = SearchResults[i].getValue('custrecord_wms_location');
				containersize = SearchResults[i].getValue('custrecord_container');
				totweight = SearchResults[i].getValue('custrecord_total_weight');
				item = SearchResults[i].getValue('custrecord_sku');
				beginloc = SearchResults[i].getValue('custrecord_actbeginloc');
				endloc = SearchResults[i].getValue('custrecord_actendloc');
				lot = SearchResults[i].getValue('custrecord_batch_no');
				actqty = SearchResults[i].getValue('custrecord_act_qty');
				expqty = SearchResults[i].getValue('custrecord_expe_qty');
				uomlevel = SearchResults[i].getValue('custrecord_uom_level');
				empid = SearchResults[i].getValue('custrecord_taskassignedto');
				internalid = SearchResults[i].getId();
				begindate=SearchResults[i].getValue('custrecordact_begin_date');
				begintime=SearchResults[i].getValue('custrecord_actualbegintime');
				packcode=SearchResults[i].getValue('custrecord_packcode');
				lp=SearchResults[i].getValue('custrecord_lpno');
				lineno=SearchResults[i].getValue('custrecord_line_no');
				waveno = SearchResults[i].getValue('custrecord_ebiz_wave_no');
				Zoneno = SearchResults[i].getValue('custrecord_ebiz_zoneid');
				ebizLP = SearchResults[i].getValue('custrecord_ebiz_lpno');
				var vZoneno=SearchResults[i].getValue('custrecord_ebizzone_no');
				taskassignedto = SearchResults[i].getValue('custrecord_taskassignedto');

				newParent.selectNewLineItem('recmachcustrecord_ebiz_ot_parent');

				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'id', internalid);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','name', orderno);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_wave_no', waveno);
				//newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualendtime', TimeStamp());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_recordupdatetime', TimeStamp());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_upd_date', DateStamp());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actendloc', endloc);
				//newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_end_date', DateStamp());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_batch_no', lot);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_status_flag', 9);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_location', siteid);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_uom_level', uomlevel);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_act_qty', parseFloat(actqty).toFixed(4));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_upd_ebiz_user_no', context.getUser());
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actbeginloc', beginloc);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecordact_begin_date',begindate);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualbegintime', begintime);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_taskassignedto', empid);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_cntrl_no', ebizcntrlno);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_tasktype', 3);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_expe_qty', parseFloat(expqty).toFixed(4));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_sku', item);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_sku_no', item);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_packcode', packcode);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_lpno', lp);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_order_no', ebizorderno);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_line_no', lineno);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_comp_id', compid);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_container_lp_no', getEnteredContainerNo);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_container', containersize);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_total_weight', parseFloat(totweight).toFixed(4));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_zoneid', Zoneno);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebizzone_no', vZoneno);

				if(ebizLP==null||ebizLP=="")
					newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_lpno', ebizLP);

				if(taskassignedto!=null&&taskassignedto!="")
					newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_taskassignedto', taskassignedto);

				newParent.commitLineItem('recmachcustrecord_ebiz_ot_parent');


			}
			nlapiSubmitRecord(newParent);
		}
	}
	catch(exp)
	{
		nlapiLogExecution('Debug','Exception in UpdateAllContianerLp',exp);	
	}

	nlapiLogExecution('Debug','Out of UpdateAllContianerLp');	
}



/*function fnValidateEnteredContLP(EnteredContLP,SalesOrderInternalId,vsite)
{
	nlapiLogExecution('Debug', 'Into fnValidateEnteredContLP',EnteredContLP);	
	nlapiLogExecution('Debug', 'SalesOrderInternalId',SalesOrderInternalId);	
	var filters=new Array();
	filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', EnteredContLP));
	if(SalesOrderInternalId!=null && SalesOrderInternalId!='' && SalesOrderInternalId!='null')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'noneof', SalesOrderInternalId));  
	if(vsite!=null && vsite!='')
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vsite));  
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');



	var salesOrderList = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return salesOrderList;
}*/

function fnValidateEnteredContLP(EnteredContLP,SalesOrderInternalId,vsite)
{
	nlapiLogExecution('Debug', 'Into fnValidateEnteredContLP',EnteredContLP);	
	nlapiLogExecution('Debug', 'SalesOrderInternalId',SalesOrderInternalId);	
	var filters=new Array();
	filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', EnteredContLP));
	if(SalesOrderInternalId!=null && SalesOrderInternalId!='' && SalesOrderInternalId!='null')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'noneof', SalesOrderInternalId));  
	if(vsite!=null && vsite!='')
		filters.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vsite));  
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');


	var salesOrderList = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);


	if(salesOrderList == null || salesOrderList == '')
	{

		nlapiLogExecution('Debug', 'Into Closed Task',SalesOrderInternalId);
		var closedTskfilters =new Array();
		//closedTskfilters.push(new nlobjSearchFilter('custrecord_ebiztask_contlp_no', null, 'is', EnteredContLP));
		closedTskfilters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_contlp_no', null, 'is', EnteredContLP));		if(SalesOrderInternalId!=null && SalesOrderInternalId!='' && SalesOrderInternalId!='null')
			closedTskfilters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no', null, 'noneof', SalesOrderInternalId));  
		if(vsite!=null && vsite!='')
			closedTskfilters.push(new nlobjSearchFilter('custrecord_ebiztask_wms_location', null, 'anyof', vsite));  
		var ClosedTaskcolumns = new Array();
		ClosedTaskcolumns[0] = new nlobjSearchColumn('name');

		salesOrderList = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, closedTskfilters, ClosedTaskcolumns);

	}
	if(salesOrderList == null || salesOrderList == '')
	{
		var filtersmlp = new Array();
		filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', EnteredContLP);
		filtersmlp[1] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_site', null, 'anyof', vsite);
		var salesOrderList = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);
	}


	return salesOrderList;
}

function GenerateLable(uompackflag){

	nlapiLogExecution('Debug', 'CreateMasterLPRecord uompackflag', uompackflag);
	var finaltext="";
	var duns="";
	var label="",uom="",ResultText="";
	//added by mahesh


	try 
	{	
		var lpMaxValue=GetMaxLPNo('2', '5','');
		nlapiLogExecution('Debug', 'lpMaxValue', lpMaxValue);

		var prefixlength=lpMaxValue.length;
		nlapiLogExecution('Debug', 'prefixlength', prefixlength);

		if(prefixlength==0)
			label="000000000";
		else if(prefixlength==1)
			label="00000000"+lpMaxValue;
		else if(prefixlength==2)
			label="0000000"+lpMaxValue;
		else if(prefixlength==3)
			label="000000"+lpMaxValue;
		else if(prefixlength==4)
			label="00000"+lpMaxValue;
		else if(prefixlength==5)
			label="0000"+lpMaxValue;
		else if(prefixlength==6)
			label="000"+lpMaxValue;
		else if(prefixlength==7)
			label="00"+lpMaxValue;
		else if(prefixlength==8)
			label="0"+lpMaxValue;
		else if(prefixlength==9)
			label=lpMaxValue;

		//to get company id

		duns=GetCompanyDUNSnumber('');

		if(uompackflag == "1" || uompackflag == "EACH" ) 
			uom="0"; 
		else if(uompackflag == "3" || uompackflag == "PALLET") 
			uom="2";
		else
			uom="0";
		nlapiLogExecution('Debug', 'uom', uom);
		nlapiLogExecution('Debug', 'CreateMasterLPRecord finaltext', finaltext);
		finaltext=uom+duns+label;
		nlapiLogExecution('Debug', 'CreateMasterLPRecord finaltext', finaltext);



		//to get chk digit
		var checkStr=finaltext;
		var ARL=0;
		var BRL=0;
		var CheckDigitValue="";
		for (i = checkStr.length-1;  i > 0;  i--)
		{
			ARL = ARL+parseInt(checkStr.charAt(i));
			i--;
		}		
		ARL=ARL*3;
		for (i = checkStr.length-2;  i > 0;  i--)
		{
			BRL = BRL+parseInt(checkStr.charAt(i));
			i--;
		}		
		var sumOfARLBRL=ARL+BRL;
		var CheckDigit=0;

		while(CheckDigit<10)
		{
			if(sumOfARLBRL%10==0)
			{ 
				CheckDigitValue=CheckDigit; 
				break; 
			} 

			sumOfARLBRL++;
			CheckDigit++;
		}
		nlapiLogExecution('Debug', 'CreateMasterLPRecord CheckDigit', CheckDigitValue.toString());
		ResultText="00"+finaltext+CheckDigitValue.toString();
		nlapiLogExecution('Debug', 'CreateMasterLPRecord ResultText', ResultText);
	} 
	catch (err) 
	{

	}
	return ResultText;
}

function GetCompanyDUNSnumber(company)
{
	var dunsfilters = new Array();
	var duns='';

	if(company!=null && company!='')
		dunsfilters.push(new nlobjSearchFilter('internalid', null, 'is', company));

	dunsfilters.push(new nlobjSearchFilter('custrecord_compduns', null, 'isnotempty'));
	dunsfilters.push(new nlobjSearchFilter('isinactive', null,'is', 'F'));

	var dunscolumns = new Array();
	dunscolumns[0] = new nlobjSearchColumn('custrecord_compduns');			 

	var dunssearchresults = nlapiSearchRecord('customrecord_ebiznet_company', null, dunsfilters, dunscolumns);
	if (dunssearchresults !=null && dunssearchresults !="") 
	{
		duns = dunssearchresults[0].getValue('custrecord_compduns');
		nlapiLogExecution('Debug', 'duns', duns);
	}
	return duns;
}

function updateWtInLPMaster(containerlp){
	nlapiLogExecution('Debug', 'In to updateWtInLPMaster...',containerlp);
	var recordId="";

	var filtersSO = new Array();
	filtersSO.push(new nlobjSearchFilter('name', null, 'is', containerlp));
	filtersSO.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_sscc', null, 'isempty'));

	var colsSO = new Array();
	colsSO[0] = new nlobjSearchColumn('name');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersSO, colsSO);

	if(searchresults){
		var ResultText=GenerateLable('1');
		for (var i = 0; i < searchresults.length; i++){

			recordId = searchresults[i].getId();
			nlapiLogExecution('Debug', 'recordId',recordId);

			nlapiSubmitField('customrecord_ebiznet_master_lp', recordId,'custrecord_ebiz_lpmaster_sscc', ResultText);

			nlapiLogExecution('Debug', 'Updated LP Master with SSCC',ResultText);
		}
	}

	nlapiLogExecution('Debug', 'Out of updateWtInLPMaster');
}


function fnValidateContainer(EnteredContainerNo,FetchedContainerNo, type ,lptype,whloc)
{
	var vResult = "";
	var vargetlpno = EnteredContainerNo;

	nlapiLogExecution('Debug', 'vargetlpno.length', vargetlpno.length);
	if (vargetlpno.length > 0) {
		var filters = new Array();

		filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'anyof', [type]));
		filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lptype', null, 'anyof', [lptype]));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		if(whloc!=null && whloc!="")
			filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_site', null, 'anyof', [whloc]));

		nlapiLogExecution('Debug', 'userdefined type', type);

		var columns=new Array();
		columns.push(new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix'));
		columns.push(new nlobjSearchColumn('custrecord_ebiznet_lprange_begin'));
		columns.push(new nlobjSearchColumn('custrecord_ebiznet_lprange_end'));
		columns.push(new nlobjSearchColumn('custrecord_ebiznet_lprange_lpgentype'));
		columns.push(new nlobjSearchColumn('custrecord_ebiznet_lprange_lptype'));

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, filters,columns);
		if(searchresults!=null && searchresults.length>0)
		{
			for (var i = 0; i < searchresults.length; i++) {
				try {
					var getLPPrefix = searchresults[i].getValue('custrecord_ebiznet_lprange_lpprefix');

					var varBeginLPRange = searchresults[i].getValue('custrecord_ebiznet_lprange_begin');

					var varEndRange = searchresults[i].getValue('custrecord_ebiznet_lprange_end');

					var getLPGenerationTypeValue = searchresults[i].getValue('custrecord_ebiznet_lprange_lpgentype');
					var getLPTypeValue = searchresults[i].getValue('custrecord_ebiznet_lprange_lptype');

					if (getLPGenerationTypeValue == type) {
						var getLPrefix = searchresults[i].getValue('custrecord_ebiznet_lprange_lpprefix');
						var LPprefixlen = getLPrefix.length;
						var vLPLen = vargetlpno.substring(0, LPprefixlen);

						if (vLPLen.toUpperCase() == getLPrefix.toUpperCase()) {
							var varnum = vargetlpno.substring(LPprefixlen, vargetlpno.length);
							nlapiLogExecution('Debug', 'varnum', varnum);

							if (varnum.length > varEndRange.length) {
								vResult = "N";
								nlapiLogExecution('Debug', 'length more than range', vResult);
								break;
							}								

							for(var j = 0; j < varnum.length; j++)
							{
								var a = varnum.charAt(j);

								if(a >= 'a' && a <= 'z')
								{
									vResult = "N";
									nlapiLogExecution('Debug', 'contains lowercase a-z', vResult);
									break;
								}

								if(a >= 'A' && a <= 'Z')
								{
									vResult = "N";
									nlapiLogExecution('Debug', 'contains uppercase A-Z', vResult);
									break;
								}
							}

							if(vResult == 'N')
							{
								break;
							}

							var EnteredContainer = "";
							for(var k = 0; k < varnum.length; k++)
							{
								var c = varnum.charAt(k);
								if(c != "0")
								{
									EnteredContainer = varnum.substring(k, varnum.length);
									break;
								}							   
							}
							nlapiLogExecution('Debug', 'EnteredContainer', EnteredContainer);

							/*var FetchedContLPPrefix = FetchedContainerNo.substring(0, LPprefixlen);
							if(FetchedContLPPrefix == getLPrefix)
							{
								var FetchedContainerNo = FetchedContainerNo.substring(LPprefixlen, FetchedContainerNo.length);
								var FetchedContainer = "";
								for(var l = 0; l < FetchedContainerNo.length; l++)
								{
								   var d = FetchedContainerNo.charAt(l);
								   if(d != "0")
								   {
									   FetchedContainer = FetchedContainerNo.substring(l, FetchedContainerNo.length);
									   break;
								   }
								}

								nlapiLogExecution('Debug', 'FetchedContainer', FetchedContainer);

								var diffContainerNo = 0;
								if(parseInt(EnteredContainer) > parseInt(FetchedContainer))
								{
									diffContainerNo = parseInt(EnteredContainer) - parseInt(FetchedContainer);
								}
								else
								{
									diffContainerNo = parseInt(FetchedContainer) - parseInt(EnteredContainer);
								}
								nlapiLogExecution('Debug', 'diffContainerNo', diffContainerNo);
								if(parseInt(diffContainerNo) > 5000)
								{
									vResult = "N";
									nlapiLogExecution('Debug', 'diff more than 5000', vResult);
									break;
								}
								else 
								{
									vResult = "Y";
									nlapiLogExecution('Debug', 'diff less than 5000', vResult);
									break;
								}

								if(vResult == 'N')
								{
									break;
								}
							}*/						

							if ((parseFloat(varnum, 10) < parseFloat(varBeginLPRange)) || (parseFloat(varnum, 10) > parseFloat(varEndRange))) {
								vResult = "N";
								nlapiLogExecution('Debug', 'not in range', vResult);
								break;
							}
							else {
								vResult = "Y";
								nlapiLogExecution('Debug', 'in range', vResult);
								break;
							}						

						}
						else {
							nlapiLogExecution('Debug', 'else', 'here');
							vResult = "N";
						}
					} //end of if statement
				} 

				catch (err) {
				}
			} //end of for loop
		}
		if (vResult == "Y") {
			nlapiLogExecution('Debug', 'final if', 'true');
			return true;
		}
		else {
			nlapiLogExecution('Debug', 'final else', 'false');
			return false;
		}
	}
	else {
		return false;
	}
}



//Updating Serial numbers
function UpdateSerialentry(RcId,SerialNo)
{
	try
	{
		nlapiLogExecution('Debug', 'RcId', RcId);
		nlapiLogExecution('Debug', 'SerialNo', SerialNo);
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RcId);
		var  fromLpno=transaction.getFieldValue('custrecord_from_lp_no');
		var  getOrdName=transaction.getFieldValue('name');
		var  getOrdLineNo=transaction.getFieldValue('custrecord_line_no');
		var item=transaction.getFieldValue('custrecord_sku');
		var getEbizOrd=transaction.getFieldValue('custrecord_ebiz_order_no');
		var getContainerLP=transaction.getFieldValue('custrecord_container_lp_no');


		var getSerialNoarr=SerialNo.split(',');
		if(getSerialNoarr.length>0)
		{
			for(var z=0; z< getSerialNoarr.length ;z++)
			{
				var filtersser = new Array();
				filtersser[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getSerialNoarr[z]);
				if(item !=null && item!='')
					filtersser[1] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', item);


				var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser);
				for ( var i = 0; i < SrchRecord.length; i++) {
					var searchresultserial = SrchRecord[i];
					var vserialid = searchresultserial.getId();
					//nlapiLogExecution('Debug', 'vserialid', vserialid);
				}

				var fields = new Array();
				var values = new Array();
				fields[0] = 'custrecord_serialwmsstatus';
				fields[1] = 'custrecord_serialebizsono';
				fields[2] = 'custrecord_serialsono';
				fields[3] = 'custrecord_serialsolineno';
				fields[4] = 'custrecord_serialparentid';
				fields[5] = 'custrecord_serialitem';
				values[0] = '8';//STATUS.OUTBOUND.PICK_CONFIRMED
				values[1] = getEbizOrd;
				values[2] = getOrdName;
				values[3] = getOrdLineNo;
				values[4] = getContainerLP;
				values[5] = item;

				var updatefields = nlapiSubmitField('customrecord_ebiznetserialentry',vserialid, fields, values);

			}
		}
	}
	catch (e) 
	{
		nlapiLogExecution('Debug', 'Exception in Updating Serial',e);
	}
}

function IsInvtonHold(ItemInternalId,EndLocInternalId)
{
	nlapiLogExecution('ERROR', 'Into IsInvtonHold');
	nlapiLogExecution('ERROR', 'ItemInternalId', ItemInternalId);
	nlapiLogExecution('ERROR', 'EndLocInternalId', EndLocInternalId);

	var holdflag='F';

	var SOFilters = new Array();
	var SOColumns = new Array();

	SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', ItemInternalId)); //Status Flag - Picks Generated
	SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', EndLocInternalId)); // Task Type - PICK	 
	SOFilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [19]));

	SOColumns[0] = new nlobjSearchColumn('custrecord_ebiz_cycl_count_hldflag' ); 
	SOColumns[1] = new nlobjSearchColumn('custrecord_ebiz_invholdflg' ); 

	var searchresults = nlapiSearchRecord( 'customrecord_ebiznet_createinv', null, SOFilters, SOColumns ); 
	if(searchresults!=null && searchresults!='')
	{
		for(var z=0; z<searchresults.length;z++) 
		{										
			var cyclholdflag=searchresults[z].getValue('custrecord_ebiz_cycl_count_hldflag');
			var invtholdflag=searchresults[z].getValue('custrecord_ebiz_invholdflg');

			if(invtholdflag=='T')
			{
				holdflag='T';
				nlapiLogExecution('ERROR', 'Out of IsInvtonHold',holdflag);
				return holdflag;
			}
		}
	}

	nlapiLogExecution('ERROR', 'Out of IsInvtonHold',holdflag);

	return holdflag;	
}
function getOpenTasksCountForNavigation(waveno,containerlpno,vPickType,SalesOrderInternalId,OrdName,vZoneId,vSite,pickMethodId)
{
	nlapiLogExecution('Debug', 'Into getOpenTasksCountNav...');

	var str = 'waveno. = ' + waveno + '<br>';
	str = str + 'containerlpno. = ' + containerlpno + '<br>';	
	str = str + 'vPickType. = ' + vPickType + '<br>';	
	str = str + 'SalesOrderInternalId. = ' + SalesOrderInternalId + '<br>';
	str = str + 'OrdName. = ' + OrdName + '<br>';
	str = str + 'vZoneId. = ' + vZoneId + '<br>';
	str = str + 'vSite. = ' + vSite + '<br>';
	str = str + 'pickMethodId. = ' + pickMethodId + '<br>';
	nlapiLogExecution('Debug', 'Function Parameters', str);



	/*var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_ebizsitepickrule', null, 'anyof', vSite));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizpickmethod');
	columns[1] = new nlobjSearchColumn('custrecord_cartonization_method','custrecord_ebizpickmethod');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_stagedetermination','custrecord_ebizpickmethod');

	var pickRuleSearchResult = new nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters, columns);

	var pickMethodId ;
	var Stagedet;
	if(pickRuleSearchResult != null){
		for (var i=0; i<pickRuleSearchResult.length; i++){			
			pickMethodId = pickRuleSearchResult[0].getValue('custrecord_ebizpickmethod');

			Stagedet = pickRuleSearchResult[i].getValue('custrecord_ebiz_stagedetermination','custrecord_ebizpickmethod');
		}
	}
	nlapiLogExecution('ERROR', 'pickMethodId', pickMethodId);*/
	var pickmethodSearchResult=null;
	if(pickMethodId!=null && pickMethodId!='')
	{

		var filters = new Array();
		filters.push(new nlobjSearchFilter('internalid', null, 'is', pickMethodId));


		var columns = new Array();

		columns.push(new nlobjSearchColumn('custrecord_ebiz_stagedetermination'));

		var pickmethodSearchResult = new nlapiSearchRecord('customrecord_ebiznet_pick_method', null, filters, columns);
	}

	var pickMethodId ;
	var Stagedet;
	if(pickmethodSearchResult != null){
		for (var i=0; i<pickmethodSearchResult.length; i++){			
			Stagedet = pickmethodSearchResult[i].getValue('custrecord_ebiz_stagedetermination');
		}
	}
	nlapiLogExecution('ERROR', 'pickMethodId', pickMethodId);

	//The below changes done by Satish.N
	//If stage determination is not defined,take carton level as default.

	if(Stagedet==null || Stagedet=='')
		Stagedet=3;

	//Upto here.


	/*var fields=new Array();	
	fields[0]='custrecord_ebiz_stagedetermination';
	var pickmethodcolumns = nlapiLookupField('customrecord_ebiznet_pick_method', pickMethodId, fields);	
	if(pickmethodcolumns!=null)

	var Stagedet = pickmethodcolumns.custrecord_ebiz_stagedetermination;*/

	nlapiLogExecution('ERROR', 'Stagedet', Stagedet);

	var openreccount=0;
	var SOFilters = new Array();
	var SOColumns = new Array();

	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK

	if(Stagedet == 3)//Carton Level
	{
		nlapiLogExecution('ERROR', 'containerlpno', containerlpno);
		if(containerlpno !=null && containerlpno !='' && containerlpno !='null')
			SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', containerlpno));
	}
	else if(Stagedet == 2)//Wave Level
	{
		nlapiLogExecution('ERROR', 'waveno', waveno);
		if(waveno !=null && waveno !='' && waveno !='null')
			SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(waveno)));
	}
	else if(Stagedet == 1)//Order Level
	{
		nlapiLogExecution('ERROR', 'OrdName', OrdName);
		if(OrdName !=null && OrdName !='' && OrdName !='null')
			SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
	}
	/*if(OrdName != null && OrdName != '')
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));*/

	if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && waveno != null && waveno != "")
	{
		nlapiLogExecution('ERROR', 'waveno', waveno);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(waveno)));
	}	 
	if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
	{
		nlapiLogExecution('Debug', 'OrdNo inside If', OrdName);
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
	}
	if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && SalesOrderInternalId!=null && SalesOrderInternalId!="" && SalesOrderInternalId!= "null")
	{
		nlapiLogExecution('Debug', 'SO Id inside If', SalesOrderInternalId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', SalesOrderInternalId));
	}
	if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
	{
		nlapiLogExecution('Debug', 'Zone # inside If', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
	}

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	if(SOSearchResults!=null && SOSearchResults!=''&& SOSearchResults.length>0)
		openreccount=SOSearchResults.length;	

	nlapiLogExecution('Debug', 'Out of getOpenTasksCountForNav...',openreccount);

	return openreccount;

}
