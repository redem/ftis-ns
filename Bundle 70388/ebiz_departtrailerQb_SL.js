/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_departtrailerQb_SL.js,v $
 *     	   $Revision: 1.1 $
 *     	   $Date: 2011/11/23 11:15:53 $
 *     	   $Author: schepuri $
 *     	   $Name:  $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_departtrailerQb_SL.js,v $
 * Revision 1.1  2011/11/23 11:15:53  schepuri
 * CASE201112/CR201113/LOG201121
 * splitting load and depart trailer functionality
 *
 
 *****************************************************************************/

/**
 * This function is the main function which call the fill functions.
 * 	The parameters are the request and the response.
 * @param request
 * @param response
 */
function DepartTrailerQb(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Depart Trailer');

		var trailer = form.addField('custpage_trailer', 'select', 'Trailer','customrecord_ebiznet_trailer');
		trailer.setLayoutType('startrow', 'none');
		
		form.addSubmitButton('Display');
		response.writePage(form);
	}
	else{
		var vtrailerno = request.getParameter('custpage_trailer');
		
		var DepartTrailerArray = new Array();
		DepartTrailerArray["custpage_trailer"] = vtrailerno;
				 
		response.sendRedirect('SUITELET', 'customscript_departtrailer', 'customdeploy_departtrailer_di', false, DepartTrailerArray);
	}
}