/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/ebiz_WO_Allocate_SL.js,v $
 *     	   $Revision: 1.3.2.11.4.4.4.29.2.4 $
 *     	   $Date: 2015/11/16 05:57:45 $
 *     	   $Author: skreddy $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 *  $Log: ebiz_WO_Allocate_SL.js,v $
 *  Revision 1.3.2.11.4.4.4.29.2.4  2015/11/16 05:57:45  skreddy
 *  case 201414924
 *  2015.2 issue fix
 *
 *  Revision 1.3.2.11.4.4.4.29.2.3  2015/11/14 14:53:28  skreddy
 *  case 201414924
 *  2015.2 issue fix
 *
 *  Revision 1.3.2.11.4.4.4.29.2.2  2015/10/09 07:28:28  skreddy
 *  case:201414864
 *  PCT Prod issue fix
 *
 *  Revision 1.3.2.11.4.4.4.29.2.1  2015/09/21 14:02:24  deepshikha
 *  2015.2 issueFix
 *  201414466
 *
 *  Revision 1.3.2.11.4.4.4.29  2015/05/13 15:29:42  skreddy
 *  Case# 201412744
 *  JB Sync issue fix
 *
 *  Revision 1.3.2.11.4.4.4.28  2015/04/13 09:25:58  rrpulicherla
 *  Case#201412277
 *
 *  Revision 1.3.2.11.4.4.4.27  2015/04/08 14:48:08  skreddy
 *  Case# 201412284
 *  PCT and JB Prod  issue fix
 *
 *  Revision 1.3.2.11.4.4.4.26  2014/12/09 14:37:45  skreddy
 *  Case# 201411181
 *  JB Prod Issue Fixed
 *
 *  Revision 1.3.2.11.4.4.4.25  2014/08/20 14:48:31  grao
 *  Case#: 20149991 �Standard bundle  issue fixes
 *
 *  Revision 1.3.2.11.4.4.4.24  2014/08/15 15:37:40  snimmakayala
 *  Case: 2014997
 *  JAWBONE WO LOCATION GENERATION FIXES
 *
 *  Revision 1.3.2.11.4.4.4.23  2014/08/05 12:07:56  snimmakayala
 *  Case: 20149724
 *  Performance fine tune
 *
 *  Revision 1.3.2.11.4.4.4.22  2014/08/04 15:19:33  skreddy
 *  case # 20149745
 *  jawbone SB issue fix
 *
 *  Revision 1.3.2.11.4.4.4.21  2014/08/04 06:49:41  spendyala
 *  CASE201112/CR201113/LOG201121
 *  Issue fixed related to case#20149810
 *
 *  Revision 1.3.2.11.4.4.4.20  2014/07/18 15:25:17  skreddy
 *  case # 20149504
 *  jawbone SB issue fix
 *
 *  Revision 1.3.2.11.4.4.4.19  2014/06/27 15:53:15  skreddy
 *  case # 20148988
 *  Dylan SB  issue fix
 *
 *  Revision 1.3.2.11.4.4.4.18  2014/06/27 10:47:04  sponnaganti
 *  case# 20149004
 *  Compatability Test Acc issue fix
 *
 *  Revision 1.3.2.11.4.4.4.17  2014/06/19 15:23:43  skreddy
 *  case # 20149011
 *  PCT prod  issue fix
 *
 *  Revision 1.3.2.11.4.4.4.16  2014/06/18 13:57:50  grao
 *  Case#: 20148957  New GUI account issue fixes
 *
 *  Revision 1.3.2.11.4.4.4.15  2014/06/12 14:30:38  grao
 *  Case#: 20148786  New GUI account issue fixes
 *
 *  Revision 1.3.2.11.4.4.4.14  2014/05/14 14:05:49  gkalla
 *  case#201219548
 *  Line numbers not matching
 *
 *  Revision 1.3.2.11.4.4.4.13  2014/04/09 06:03:46  skreddy
 *  case 20127925
 *  LL sb  issue fix
 *
 *  Revision 1.3.2.11.4.4.4.12  2013/09/24 07:15:58  gkalla
 *  Case# 20124548
 *  Work order issue of not picking from bulk
 *
 *  Revision 1.3.2.11.4.4.4.11  2013/09/02 08:06:08  spendyala
 *  case# 20124214
 *  issue fixed related to case20124214
 *
 *  Revision 1.3.2.11.4.4.4.10  2013/07/11 14:57:43  gkalla
 *  Case# 20123352
 *  PCT Issue
 *
 *  Revision 1.3.2.11.4.4.4.9  2013/07/09 15:40:57  skreddy
 *  Case# 20123356
 *  remove duplicate error message
 *
 *  Revision 1.3.2.11.4.4.4.8  2013/06/18 14:52:08  skreddy
 *  CASE201112/CR201113/LOG201121
 *  expiry date for Lot numbers
 *
 *  Revision 1.3.2.11.4.4.4.7  2013/06/03 07:49:17  gkalla
 *  CASE201112/CR201113/LOG201121
 *  Getting lot numbers from inventory changed for PCT
 *
 *  Revision 1.3.2.11.4.4.4.6  2013/06/03 06:54:40  rrpulicherla
 *  CASE201112/CR201113/LOG201121
 *  Inventory Changes
 *
 *  Revision 1.3.2.11.4.4.4.5  2013/04/01 20:59:31  snimmakayala
 *  CASE201112/CR201113/LOG2012392
 *  Prod and UAT issue fixes.
 *
 *  Revision 1.3.2.11.4.4.4.4  2013/03/19 11:46:26  snimmakayala
 *  CASE201112/CR201113/LOG2012392
 *  Production and UAT issue fixes.
 *
 *  Revision 1.3.2.11.4.4.4.3  2013/03/08 14:43:37  skreddy
 *  CASE201112/CR201113/LOG201121
 *  Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.25.2.20  2012/08/30 02:04:56  gkalla
 *
 *****************************************************************************/

function GeneratLocationsSuitelet(request, response){
	if (request.getMethod() == 'GET') {

		var form = nlapiCreateForm('Generate Bin Locations for Work orders');




		//var WorkOrder  = form.addField('custpage_workorder', 'select', 'Work Order').setLayoutType('startrow', 'none');
		//WorkOrder.addSelectOption("","");			
		// Retrieve all sales orders
		//var WorkOrderList = getAllWorkOrders();		
		// Add all sales orders to SO Field
		//WorkOrder.setDefaultValue(request.getParameter('custpage_workorder'));
		//addAllWorkOrdersToField(WorkOrder, WorkOrderList,form);

		//WorkOrder.setMandatory(true);
		//fillsalesorderField(form, salesorder,-1);
		//WorkOrder.setDefaultValue(woidno);	

		form.addSubmitButton('Generate Bin Location');
		var woid=request.getParameter('custpage_workorder');
		nlapiLogExecution('ERROR','woid',woid);

		var wointid = form.addField('custpage_hdnwono', 'text', 'wono');
		wointid.setDisplayType('hidden');
		wointid.setDefaultValue(woid); 
		var built="";
		if(woid!=null && woid!='')
		{
			nlapiLogExecution('ERROR','INTOwoid1',woid);
			var searchresults = nlapiLoadRecord('workorder', woid);
			//var lineItems= searchresults.getLineItemCount('item');
			if(searchresults!=null && searchresults!='') 
			{ 	    	
				var qty = searchresults.getFieldValue('quantity');    	     
				var itemid=searchresults.getFieldValue('assemblyitem');
				var itemText=searchresults.getFieldText('assemblyitem');
				var WoCreatedDate=searchresults.getFieldValue('trandate');
				var WoDueDate=searchresults.getFieldValue('custbody_ebiz_woduedate');
				var WoExpCompDate=searchresults.getFieldValue('custbody_ebiz_expcompletiondate');
				var WoActCompDate=searchresults.getFieldValue('custbody_ebiz_actcompletiondate');
				var WoExpcompletionDate=searchresults.getFieldValue('custbody_ebiz_expcompletiondate');
				var itemQty=searchresults.getFieldValue('quantity');
				var itemStatus=searchresults.getFieldValue('status');
				var itemLocation=searchresults.getFieldValue('location');
				var WOName=searchresults.getFieldValue('tranid');
				var Approveflagtext=searchresults.getFieldText('custbody_ebiz_wo_qc_status');
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('internalid', null, 'is', woid);
				filters[1] = new nlobjSearchFilter('mainline', null, 'is', 'T');//ship task


				var columns = new Array();
				columns[0] = new nlobjSearchColumn('quantityshiprecv');

				var searchresultsWaveNo = nlapiSearchRecord('workorder', null, filters, columns);
				if(searchresultsWaveNo!=null && searchresultsWaveNo!=''){
					built = searchresultsWaveNo[0].getValue('quantityshiprecv');
				}
				nlapiLogExecution('ERROR','qty',qty);
				nlapiLogExecution('ERROR','built',built);
				if(built==null || built =="")
					built=0;
				qty = parseInt(qty) - parseInt(built);
				nlapiLogExecution('ERROR','qty',qty);
				var wointname = form.addField('custpage_hdnwoname', 'text', 'woname');
				wointname.setDisplayType('hidden');
				wointname.setDefaultValue(WOName);
				/* code merged from monobid prod bundle on 27th feb 2013 by Radhika */

				var kitItemStatus="";
				var ItemFilters = new Array();
				ItemFilters.push(new nlobjSearchFilter('internalid', null, 'anyof',itemid));

				var ItemColumns = new Array();
				ItemColumns.push(new nlobjSearchColumn('custitem_ebizdefskustatus'));

				var ItemResults = nlapiSearchRecord('item', null, ItemFilters, ItemColumns);
				if(ItemResults !=null && ItemResults!="")
				{
					kitItemStatus= ItemResults[0].getValue('custitem_ebizdefskustatus');
					nlapiLogExecution('ERROR','kitItemStatus',kitItemStatus);

				}

				/* upto here */
				var itemLocationtext="";
				if(itemLocation == null || itemLocation =="")
				{
					itemLocation="&nbsp;";
				}
				else
				{

					var searchresultslocation = nlapiLoadRecord('location', itemLocation);
					if(searchresultslocation!=null && searchresultslocation!='') 
					{
						itemLocationtext=searchresultslocation.getFieldValue('name');
					}
				}

				form.addField('custpage_wo', 'text', 'WO# : ').setLayoutType("startrow").setDisplayType('inline').setDefaultValue(WOName);	
				form.addField('custpage_kititem', 'text', 'Item : ').setDisplayType('inline').setDefaultValue(itemText);
				form.addField('custpage_kitqty', 'text', 'Qty : ').setDisplayType('inline').setLayoutType("startrow").setDefaultValue(qty);
				form.addField('custpage_kitlocation', 'text', 'Location : ').setDisplayType('inline').setDefaultValue(itemLocationtext);
				form.addField('custpage_createdate', 'text', 'Created Date : ').setDisplayType('inline').setDefaultValue(WoCreatedDate);
				form.addField('custpage_duedate', 'text', 'Due Date : ').setDisplayType('inline').setLayoutType("startrow").setDefaultValue(WoDueDate);
				form.addField('custpage_expdate', 'text', 'Expected Completion Date : ').setDisplayType('inline').setLayoutType("startrow").setDefaultValue(WoExpCompDate);
				form.addField('custpage_actdate', 'text', 'Actual Completion Date : ').setDisplayType('inline').setLayoutType("startrow").setDefaultValue(WoActCompDate);
				form.addField('custpage_itemstatus', 'text', 'WO Status : ').setDisplayType('inline').setDefaultValue(itemStatus);
				form.addField('custpage_qcstatus', 'text', 'QC Status : ').setDisplayType('inline').setDefaultValue(Approveflagtext);
				var hdnBackOrdered = form.addField('custpage_hdnbackordered', 'text', 'Backordered Bool : ').setDisplayType('inline').setDisplayType('hidden');
				form.addField('custpage_hdnitemtext', 'text', 'Assembly Item Text : ').setDisplayType('inline').setDisplayType('hidden').setDefaultValue(WOName);





				var vLineCount=searchresults.getLineItemCount('item');
				//Adding  ItemSubList

				var ItemSubList = form.addSubList("custpage_deliveryord_items", "list", "Components");
				ItemSubList.addField("custpage_deliveryord_sku", "text","Item");
				ItemSubList.addField("custpage_hidden_skuid", "text","Itemid").setDisplayType('hidden');//added by santosh;
				ItemSubList.addField("custpage_deliveryord_itemdesc", "text", "Description");
				ItemSubList.addField("custpage_deliveryord_memqty", "text", "Quantity");
				//ItemSubList.addField("custpage_deliveryord_memqty", "text", "Required Qty").setDisplayType('hidden');       
				ItemSubList.addField("custpage_deliveryord_comitqty", "text", "Committed");        
				ItemSubList.addField("custpage_deliveryord_backordqty", "text", 'Back Ordered');

				var serialno= ItemSubList.addField("custpage_deliveryord_lotno", "select", "Serial/Lot Numbers");
				serialno.addSelectOption('', '');
				ItemSubList.addField("custpage_comments", "text", "Comments").setDisplayType('entry');
				ItemSubList.addField("custpage_hiddenlotno", "text", "lotno").setDisplayType('hidden');//added by santosh
				ItemSubList.addField("custpage_lineno", "text", "Lineno").setDisplayType('hidden');//added by santosh

				//ItemSubList.addField("custpage_skurecid", "text", "Recid").setDisplayType('hidden');
				//ItemSubList.addField("custpage_deliveryord_lotno", "select", "Serial/Lot Numbers");
				//ItemSubList.addField("custpage_lotno", "text", "Commit"); 
				/*ItemSubList.addField("custpage_lineno", "text", "LP#");
				ItemSubList.addField("custpage_lineno", "text", "Bin Location");
				ItemSubList.addField("custpage_lineno", "text", "Inventory Ref#");
				ItemSubList.addField("custpage_lineno", "text", "Available Qty");
				ItemSubList.addField("custpage_lineno", "text", "Bin Location");
				ItemSubList.addField("custpage_lineno", "text", "Lot Info");
				ItemSubList.addField("custpage_lineno", "text", "Line No");*/

				//to get distinct Itemid 
				var skuarray=new Array();
				for (z=1; z<=vLineCount; z++) 
				{
					var skuid =searchresults.getLineItemValue('item', 'item', z);	
					nlapiLogExecution('ERROR', 'skuid', skuid);
					if(skuarray.indexOf(skuid)==-1)
					{
						skuarray.push(skuid);
					}

				}
				//end of the code 

				//binding sublist dropdown
				/* code merged from monobid on feb 27th 2013 by Radhika (added Kititemstatus field to the addSerialno fun) */
				addSerialno(skuarray,itemLocation,form,serialno,kitItemStatus);


				var filter=new Array();
				filter.push(new nlobjSearchFilter('custrecord_ebiz_order_no','null','is',woid));
				filter.push(new nlobjSearchFilter('custrecord_wms_status_flag','null','anyof',['9','8']));
				//filter.push(new nlobjSearchFilter('custrecord_line_no','null','is',LineNo));
				//filter.push(new nlobjSearchFilter('custrecord_sku','null','anyof',lineItemvalue));
				filter.push(new nlobjSearchFilter('custrecord_tasktype','null','anyof','3'));

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
				columns[1] = new nlobjSearchColumn('custrecord_sku',null,'group');
				columns[2] = new nlobjSearchColumn('custrecord_line_no',null,'group');

				var searchrecord=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filter,columns);	

				//Code to fetch pick assigned or confirmed records from open task.
				var k=0;c=1;
				var display = "false";
				var vFlag = "false";
				for(var m=1;m<=vLineCount;m++)
				{
					nlapiLogExecution('ERROR','m',m);
					var LineNo =searchresults.getLineItemValue('item','line',m);
					var lineItem = searchresults.getLineItemText('item', 'item', m);
					var lineItemvalue = searchresults.getLineItemValue('item', 'item', m);
					var CompItemType = searchresults.getLineItemValue('item', 'itemtype', m);
					var CommittedQty = searchresults.getLineItemValue('item', 'quantitycommitted', m);
					var vBackOrdQty = searchresults.getLineItemValue('item', 'quantitybackordered', m);
					nlapiLogExecution('ERROR','CompItemType',CompItemType);
					var vNSUOM = searchresults.getLineItemValue('item','units', m);
					nlapiLogExecution('ERROR','vBackOrdQty',vBackOrdQty);
					var checkUOMQty = new Array();
					checkUOMQty = checkUOM(lineItemvalue,vNSUOM);		
						var vBaseUOMQty = checkUOMQty[0];	
						var veBizUOMQty = checkUOMQty[1];
						if(veBizUOMQty!=0 && veBizUOMQty > 1){
							itemQty = parseInt((parseFloat(itemQty)*parseFloat(veBizUOMQty))/parseFloat(vBaseUOMQty));
							CommittedQty = parseInt((parseFloat(CommittedQty)*parseFloat(veBizUOMQty))/parseFloat(vBaseUOMQty));
							nlapiLogExecution('DEBUG', 'CommittedQty UOM', CommittedQty);
							nlapiLogExecution('DEBUG', 'itemQty', itemQty);
						}				
					nlapiLogExecution('ERROR','CommittedQty',CommittedQty);
					if((CompItemType != 'Service' && CompItemType != 'NonInvtPart') && ((CommittedQty !=null && CommittedQty !='' && CommittedQty !=0)||((CommittedQty == null||CommittedQty==0||CommittedQty=='')&&(vBackOrdQty>0))))
					{	
						if(lineItem== null || lineItem =="")
							lineItem="";

						var LineQty =searchresults.getLineItemValue('item','quantity',m);
						if(LineQty== null || LineQty =="")	
							LineQty="&nbsp;";

						var BackOrdQty = searchresults.getLineItemValue('item', 'quantitybackordered', m);
						if(BackOrdQty== null || BackOrdQty =="")
							BackOrdQty="";

						nlapiLogExecution('ERROR','BackOrdQty',BackOrdQty);

						if(BackOrdQty != null && BackOrdQty != "" && parseFloat(BackOrdQty) >0)
						{
							hdnBackOrdered.setDefaultValue('T');

						}	
						//hdnBackOrdered.setDefaultValue('T');
						//var CommittedQty = searchresults.getLineItemValue('item', 'quantitycommitted', m);
						//Code to fetch pick assigned or confirmed records from open task.
						var expqty = 0;
						if(searchrecord!=null && searchrecord!=''){
							nlapiLogExecution('DEBUG','searchrecord.length',searchrecord.length);
							for(var i =0; i<searchrecord.length; i++){
								var otlineno =searchrecord[i].getValue('custrecord_line_no',null,'group'); 
								var otitemname = searchrecord[i].getText('custrecord_sku',null,'group');
								nlapiLogExecution('DEBUG','otitemname',otitemname);
								nlapiLogExecution('DEBUG','lineItem',lineItem);
								nlapiLogExecution('DEBUG','otlineno',otlineno);
								nlapiLogExecution('DEBUG','LineNo',LineNo);
								if((otitemname == lineItem) && (otlineno == LineNo)){
									expqty = searchrecord[i].getValue('custrecord_expe_qty',null,'sum');
									break;
								}
							}
						}
						nlapiLogExecution('DEBUG','expqty',expqty);
						nlapiLogExecution('DEBUG','CommittedQty',CommittedQty);

						if(parseFloat(expqty)<parseFloat(CommittedQty))
							display = "true";

						CommittedQty = (CommittedQty) - (expqty);

						//end
						if(CommittedQty== null || CommittedQty =="")
							CommittedQty="";

						// code added from FactoryMation on 01Mar13 by santosh; to restrict ItemDescription 
						var ItemDesc='';
						var ItemDescription = searchresults.getLineItemValue('item', 'description', m);
						if(ItemDescription !=null && ItemDescription!='')
						{
							ItemDesc=ItemDescription.substring(0, 280);
						}//upto here
						//var ItemDesc = searchresults.getLineItemValue('item', 'description', m);
						if(ItemDesc== null || ItemDesc =="")
							ItemDesc="";

						var Lotno = searchresults.getLineItemValue('item', 'serialnumbers', m);
						if(Lotno== null || Lotno =="")
							Lotno="";

						nlapiLogExecution('ERROR', 'Lotno',Lotno);
						var vLotnoArr=new Array();
						var vLotnoArr = Lotno.split('');
						if(display == "true"){
							vFlag = "true";
						if(vLotnoArr.length>1)
						{
							k=c;
							nlapiLogExecution('ERROR', 'vLotnoArr.length',vLotnoArr.length);
							for(var j=0;j<vLotnoArr.length;j++)
							{
								nlapiLogExecution('ERROR', 'k',k);
								var LotNumber=vLotnoArr[j];
								nlapiLogExecution('ERROR', 'LotNumber',LotNumber);
								var pos = LotNumber.indexOf("(")+1;
								nlapiLogExecution('ERROR', 'pos ', pos);
								var vQty=LotNumber.slice(pos, -1);
								nlapiLogExecution('ERROR', 'vQty', vQty);
								var vLotno=LotNumber.slice(0, pos-1);
								nlapiLogExecution('ERROR', 'vLotno', vLotno);
								nlapiLogExecution('ERROR', 'lineItemvalue', lineItemvalue);
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_sku', k, lineItem);
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_hidden_skuid', k, lineItemvalue);
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_itemdesc', k, ItemDesc);
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_memqty', k, vQty);
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_backordqty', k, BackOrdQty);                       		        		   
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_comitqty', k, CommittedQty);
								form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_lineno', k, LineNo);	
								//form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_lotno', m, Lotno);
								// addSerialno(lineItemvalue, itemLocation,form,k,serialno);
								GetLotno(vLotno,lineItemvalue,k,form);
								k++;
							}
							c=k;
						}
						else
						{
							nlapiLogExecution('ERROR', 'c',c);
							form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_sku', c, lineItem);
							form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_hidden_skuid', c, lineItemvalue);
							form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_itemdesc', c, ItemDesc);
							form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_memqty', c, LineQty);
							form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_backordqty', c, BackOrdQty);                       		        		   
							form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_comitqty', c, CommittedQty);
							form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_lineno', c, LineNo);	
							//form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_lotno', m, Lotno);
							// addSerialno(lineItemvalue, itemLocation,form,c,serialno);
							GetLotno(Lotno,lineItemvalue,c,form);
							c++;
						}
						}
					}
				}
				if(vFlag == "false")
				{
					nlapiLogExecution('ERROR','Already allocated ');
					var ErrorMsg = nlapiCreateError('CannotAllocate','Bin locations are already generated for this Work Order ' + WOName, true);
					throw ErrorMsg;
				}
			}

		} 

		response.writePage(form);

	}
	else  
	{ 
		var BackorderAvailable= request.getParameter('custpage_hdnbackordered');
		var AssemblyItemText= request.getParameter('custpage_hdnitemtext');

		
		
		var WOId=request.getParameter('custpage_hdnwono');
		nlapiLogExecution('ERROR', 'WOId',WOId);
		nlapiLogExecution('ERROR', 'BackorderAvailable',BackorderAvailable);
		// below code commented becuase no need to check with BOM defination
	/*	var ComponentItemArray = new Array();
		var skuarray=new Array();
		if(WOId !=null && WOId!='' && WOId!='null')
		{
			var searchresults = nlapiLoadRecord('workorder', WOId);
			if(searchresults!=null && searchresults!='') 
			{ 
				var assemblyitem=searchresults.getFieldValue('assemblyitem');
				
				if(assemblyitem !=null && assemblyitem!=''&& assemblyitem!='null' && assemblyitem!='undefined')
				{
					var results =getMemberItemsDetails(assemblyitem);
					if(results !=null && results!='')
					{
						var memberItemsCount = results.length;
						
						if(memberItemsCount !=null && memberItemsCount!='' && memberItemsCount!='null' && memberItemsCount!='unefined' && memberItemsCount > 0)
						{
							
							for(var k1=0;k1<results.length;k1++)
							{
								ComponentItemArray.push(results[k1].getValue('memberitem'));
							}
							
							var vLineCount=searchresults.getLineItemCount('item');
							
							
							for (z=1; z<=vLineCount; z++) 
							{
								var skuid =searchresults.getLineItemValue('item', 'item', z);	
							
								if(ComponentItemArray.indexOf(skuid)!=-1)
								{
								
									skuarray.push(skuid);
								}

							}
						
							
						}

					}
				}

			}
		}
		nlapiLogExecution('ERROR', 'ComponentItemArray.length',ComponentItemArray.length);
		nlapiLogExecution('ERROR', 'skuarray.length',skuarray.length);
		if(parseInt(ComponentItemArray.length)!=parseInt(skuarray.length))
		{
			
			var ErrorMsg = nlapiCreateError('CannotAllocate','Some of the Component items were removed from Work Order ' + AssemblyItemText + ' ', true);
			throw ErrorMsg;
		}*/
		if(BackorderAvailable == 'T') 
		{
			//var woidno = request.getParameter('custpage_hdnwono');
			nlapiLogExecution('ERROR','Backordered Workorder ', AssemblyItemText);
			var ErrorMsg = nlapiCreateError('CannotAllocate','Work Order ' + AssemblyItemText + ' has Backordered quantity components ', true);
			throw ErrorMsg;
		}
		else
		{
			GenerateLocationsPOSTRequest(request, response);
		}	


	}
}
function getMemberItemsDetails(itemnoarr)
{
	nlapiLogExecution('Debug', 'Into getMemberItemsDetails', itemnoarr);

	var filters = new Array(); 			 
	filters[0] = new nlobjSearchFilter('internalid', null, 'anyof', itemnoarr);	

	var columns1 = new Array(); 
	columns1[0] = new nlobjSearchColumn( 'memberitem' ); 			
	columns1[1] = new nlobjSearchColumn( 'memberquantity' );
	columns1[2] = new nlobjSearchColumn( 'itemid' ).setSort();

	var searchresults = nlapiSearchRecord( 'item', null, filters, columns1 ); 

	nlapiLogExecution('Debug', 'Out of getMemberItemsDetails', searchresults);



	return searchresults;

}
function GenerateLocationsPOSTRequest(request, response)
{
	nlapiLogExecution('DEBUG', 'Into GenerateLocationsPOSTRequest', TimeStampinSec());

	var form = nlapiCreateForm('Generate Bin Location');
	var woidno = request.getParameter('custpage_hdnwono');
	var woidname = request.getParameter('custpage_hdnwoname');

	var str = 'woidno.' + woidno + '<br>';
	str = str + 'woidname.' + woidname + '<br>';

	nlapiLogExecution('DEBUG', 'WO Values', str);

	var filter=new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebiz_order_no','null','is',woidno));
	filter.push(new nlobjSearchFilter('custrecord_wms_status_flag','null','anyof',['9','8','14']));

	var searchrecord=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filter,null);
	nlapiLogExecution('ERROR', 'Time Stamp after search record on open task',TimeStampinSec());
	if(searchrecord != null && searchrecord != "")
	{
		nlapiLogExecution('ERROR','Already allocated ');
		var ErrorMsg = nlapiCreateError('CannotAllocate','Bin locations are already generated for this Work Order ' + woidname, true);
		throw ErrorMsg; //throw this error object, do not catch it
	}	
	else
	{	
		nlapiLogExecution('ERROR', 'Time Stamp before CheckAvailability',TimeStampinSec());
		CheckAvailability(woidno,request);
		nlapiLogExecution('ERROR', 'Time Stamp after CheckAvailability',TimeStampinSec());
		//AllocateInv(woidno,request);
		response.writePage(form);
	}

	nlapiLogExecution('ERROR', 'Out of GenerateLocationsPOSTRequest',TimeStampinSec());

}	
/*var form = nlapiCreateForm('Generate Bin Location');
	var vitem = request.getParameter('cusppage_item');		
	var vloc  = request.getParameter('cusppage_site');
	var vqty  = request.getParameter('custpage_kitqty');
	var vdate = request.getParameter('custpage_sodate');
	var vmemo = request.getParameter('custpage_notes');
	var vlp   = request.getParameter('custpage_lp');
	var vheaderlotno=request.getParameter('custpage_lotnoserialno');
	//var vheaderlotnoVal;
	var vbinloc = request.getParameter('custpage_binlocation');
	var vBeginDate=request.getParameter('custpage_bgdate');
	var vBeginTime=request.getParameter('custpage_bgtime');
	var vSointernalId=request.getParameter('custpage_sointernalid');


	//var vaccountno = request.getParameter('cusppage_accountno');

	//Create Work Order 
	var Wo=nlapiCreateRecord('workorder');
	Wo.setFieldValue('assemblyitem',vitem);
	Wo.setFieldValue('location', vloc);			
	Wo.setFieldValue('quantity', vqty);
	Wo.setFieldValue('trandate', vdate);
	Wo.setFieldValue('createdfrom', vSointernalId);
	var lineCnt = request.getLineItemCount('custpage_deliveryord_items');

	for (var k = 1; k <= lineCnt; k++) 
	{				
		var vitem = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_sku', k);
		var vLocation = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_location', k);
		var vlp =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_lp', k);
		var vQty =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_memqty', k);
		var vLotNo =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_lotno', k);

		nlapiLogExecution('ERROR', 'vLocation',vLocation);
		nlapiLogExecution('ERROR', 'vlp',vlp);	
		nlapiLogExecution('ERROR', 'vQty',vQty);	
		Wo.setLineItemValue('item', 'item', k,vitem);
		Wo.setLineItemValue('item', 'quantity', k,vQty) ;
		Wo.setLineItemValue('item', 'serialnumbers', k,vLotNo + "(" + vQty + ")") ;

		Wo.setLineItemValue('item', 'custcol_locationsitemreceipt', k,vLocation);
		Wo.setLineItemValue('item', 'custcol_ebiz_lp', k,vlp) ;

		Wo.selectLineItem('item', k);
		//Wo.commitLineItem('item');

	}

	var woid=nlapiSubmitRecord(Wo);
	nlapiLogExecution('ERROR', 'Outside If=', woid); 

	var filters = new Array();			   

	filters[0] = new nlobjSearchFilter('internalid', null, 'is', woid);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('tranid');			


	var searchResults = nlapiSearchRecord('workorder', null, filters, columns);
	var wovalue;
	if(searchResults!= null && searchResults != "" && searchResults.length >0)
	{
		wovalue=searchResults[0].getValue('tranid');
	}

	showInlineMessage(form, 'Confirmation', 'Work Order Created', wovalue);	

 */

function CheckAvailability(woidno,request)
{
	nlapiLogExecution('ERROR', 'Into Check Availablity '); 
	var vMessage= fnGetDetails(woidno,request);

	if(vMessage== 'SUCCESS') 
	{ 
		return true;
	} 
	else
	{
		if(vMessage!= null && vMessage != "")
		{
			var vArrMsg=vMessage.split('~');
			if(vArrMsg.length>1)
			{
				nlapiLogExecution('ERROR','Allocation Failed ',vArrMsg[1]);
				var ErrorMsg = nlapiCreateError('CannotAllocate',vArrMsg[1], true);
				throw ErrorMsg; //throw this error object, do not catch it
			}
			else
			{
				nlapiLogExecution('ERROR','eBiz Allocation Failed ');
				var ErrorMsg = nlapiCreateError('CannotAllocate','Allocation Failed', true);
				throw ErrorMsg; //throw this error object, do not catch it
			}	
		}
		return false;
	}

}

function fnGetDetails(vId,request)
{
	nlapiLogExecution('ERROR','Into Get Details');
	//var vId = request.getParameter('custpage_workorder');
	var SearchresultsArray = new Array();//added by santosh
	nlapiLogExecution('Debug','Time stamp 3',TimeStampinSec());
	var searchresults = nlapiLoadRecord('workorder', vId);
	nlapiLogExecution('Debug','Time stamp 4',TimeStampinSec());
	var itemcount= searchresults.getLineItemCount('item');
	var vWOId=searchresults.getFieldValue('tranid');
	var SkuNo=searchresults.getFieldValue('assemblyitem');
	var vordertype=searchresults.getFieldValue('custbody_nswmssoordertype');
	var varAssemblyQty=searchresults.getFieldValue('quantity');
	var vLocation= searchresults.getFieldValue('location');
	var vWMSlocation=searchresults.getFieldValue('location');
	var qty=0;
	if(qty1!= null && qty1!="" && parseFloat(qty1)>0)
		qty=qty1;

	//nlapiLogExecution('ERROR', 'vId', vId);
	//nlapiLogExecution('ERROR', 'vWOId', vWOId);
	/*var searchresults = nlapiLoadRecord('workorder', vId);
	if(searchresults !=null && searchresults != "" && searchresults.length>0)
		vWOId=searchresults.getFieldValue('tranid');
	nlapiLogExecution('ERROR', 'vWOId', vWOId);*/

	nlapiLogExecution('ERROR', 'ItemCount', itemcount);
	//nlapiLogExecution('ERROR', 'assemItem', assemItem);



	var vInvReturn=new Array();
	//alert("3");
	var strItem="",strQty=0,vcomponent,vlineno=0,strItemstatus='';

	/*	var ItemType = nlapiLookupField('item', vitem, 'recordType');=0
	nlapiLogExecution('ERROR','ItemType',ItemType);
	var searchresultsitem;
	if(ItemType!=null && ItemType !='')

	{
		nlapiLogExecution('ERROR','Item Type',ItemType);
		nlapiLogExecution('ERROR','Item Type is NULL ',ItemType);

		searchresultsitem = nlapiLoadRecord(ItemType, vitem); //1020
	}

	else
	{
		nlapiLogExecution('ERROR','Item Type is NULL ',ItemType);
	}
	var SkuNo=searchresultsitem.getFieldValue('itemid');*/
	var recCount=0; 
	//Written by Ganesh
	//alert("ItemCount : "+ itemcount);
	var vAlert='';
	//nlapiLogExecution('ERROR','itemcount ',itemcount);
	var vItemArr=new Array();

	for(var s=1; s<=itemcount;s++)
	{
		var vCompItem = searchresults.getLineItemValue('item', 'item', s);
		var CompItemType = searchresults.getLineItemValue('item', 'itemtype', s);
		//nlapiLogExecution('ERROR','CompItemType',CompItemType);
		var vCommittedQty = searchresults.getLineItemValue('item', 'quantitycommitted', s);

		if(CompItemType != 'Service' && CompItemType != 'NonInvtPart' && vItemArr.indexOf(vCompItem) == -1 && (vCommittedQty !=null && vCommittedQty !='' && vCommittedQty !=0))
			vItemArr.push(vCompItem);
	}
	nlapiLogExecution('Debug','Time stamp 5',TimeStampinSec());
	if(vItemArr.length>0)
	{
		var filters2 = new Array();

		var columns2 = new Array();
		columns2[0] = new nlobjSearchColumn('custitem_item_family');
		columns2[1] = new nlobjSearchColumn('custitem_item_group');
		columns2[2] = new nlobjSearchColumn('custitem_ebizdefskustatus');
		columns2[3] = new nlobjSearchColumn('custitem_item_info_1');
		columns2[4] = new nlobjSearchColumn('custitem_item_info_2');
		columns2[5] = new nlobjSearchColumn('custitem_item_info_3');
		columns2[6] = new nlobjSearchColumn('custitem_ebizabcvelitem');

		filters2.push(new nlobjSearchFilter('internalid', null, 'is', vItemArr));

		var ItemInfoResults = nlapiSearchRecord('item', null, filters2, columns2);

		var filters3=new Array();
		filters3.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

		if(vordertype!=null&&vordertype!="")
			filters3.push(new nlobjSearchFilter('custrecord_ebizordertypepickrul', null, 'anyof', ['@NONE@',vordertype]));

		var columns3=new Array();

		columns3[0] = new nlobjSearchColumn('name');
		columns3[1] = new nlobjSearchColumn('custrecord_ebizpickzonerul');
		columns3[2] = new nlobjSearchColumn('custrecord_ebizpickmethod');
		//columns3[3] = new nlobjSearchColumn('custrecord_ebizsequencenopickrul');
		columns3[3]=new nlobjSearchColumn('formulanumeric');
		columns3[3].setFormula("TO_NUMBER({custrecord_ebizsequencenopickrul})").setSort();
		columns3[4] = new nlobjSearchColumn('custrecord_ebizruleidpick');
		columns3[5] = new nlobjSearchColumn('custrecord_ebizlocationgrouppickrul');
		columns3[6] = new nlobjSearchColumn('custrecord_ebizlocationpickrul');
		columns3[7] = new nlobjSearchColumn('custrecord_ebizpickfacelocation','custrecord_ebizpickmethod');

		//columns3[3].setSort();
		var pickrulesarray = nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters3, columns3);
	}
	
	var pfLocationResults = getPFLocationsForOrder(vWMSlocation,vItemArr);
	
	nlapiLogExecution('Debug','Time stamp 6',TimeStampinSec());
	for(var m=1; m<=itemcount;m++) 
	{
		var serialNo="";
		//var vBatch="";
		//alert("SkuNo" + SkuNo);
		strItem = searchresults.getLineItemValue('item', 'item', m);
		strItemstatus = searchresults.getLineItemValue('item', 'custcol_ebiznet_item_status', m);
		
		//alert("strItem" + strItem);
		strQty = searchresults.getLineItemValue('item', 'quantity', m);
		vlineno = searchresults.getLineItemValue('item','line',m);
		nlapiLogExecution('ERROR', 'vlineno', vlineno);
		var CompItemType = searchresults.getLineItemValue('item', 'itemtype', m);
		nlapiLogExecution('ERROR','CompItemType',CompItemType);
		var CommittedQty = searchresults.getLineItemValue('item', 'quantitycommitted', m);
		if(CommittedQty !=null && CommittedQty !='' && CommittedQty!=0)
			strQty=CommittedQty;
		if(CompItemType != 'Service' && CompItemType != 'NonInvtPart' && (CommittedQty !=null && CommittedQty !='' && CommittedQty !=0))
		{
			var Lotno = searchresults.getLineItemValue('item', 'serialnumbers', m);
			nlapiLogExecution('ERROR','Lotno',Lotno);
			if(Lotno== null || Lotno =="")
				Lotno="";

			var LotnoArray = Lotno.split('');
			if(LotnoArray.length>0)
			{
				nlapiLogExecution('ERROR', 'LotnoArray.length',LotnoArray.length);
				var vitemqty=0;
				var vrecid="" ;
				var vlp="",vlocation="",vqty="",vlotnoWithQty="",vremainqty="",location,vlocationText="";
				var vMultipleLp="",vMultipleQty="",vMultipleTempLot="",vMultipleLocation="",vMultipleRecid="";
				var vTempLot="";
				for(var z=0; z<LotnoArray.length;z++)
				{				
					nlapiLogExecution('ERROR', 'Z',z);
					var vBatch="";
					var vQty="";
					var vLotname="";
					var vLotno="";
					if(LotnoArray.length>1)// for multiple Lotno s
					{
						var vLotNumber=LotnoArray[z];      // ex:ebiz(1),batch(1)
						nlapiLogExecution('ERROR', 'LotNumber',vLotNumber);

						var pos = vLotNumber.indexOf("(")+1; 
						nlapiLogExecution('ERROR', 'pos ', pos);

						vQty=vLotNumber.slice(pos, -1);
						nlapiLogExecution('ERROR', 'vQty', vQty); //ex:1

						vLotname=vLotNumber.slice(0, pos-1);
						nlapiLogExecution('ERROR', 'vLotname', vLotname); //Ex:ebiz

					}
					else
					{
						vLotname=Lotno;
						vQty=strQty;
					}

					//Get Lotno Id;
					if(vLotname!=null && vLotname!='')
					{
						nlapiLogExecution('Debug','Time stamp 7',TimeStampinSec());
						var filterbatch = new Array();
						filterbatch[0] = new nlobjSearchFilter('name', null, 'is', vLotname);
						filterbatch[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', strItem);
						var Lotnosearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterbatch);
						nlapiLogExecution('Debug','Time stamp 8',TimeStampinSec());
						if(Lotnosearchresults!=null)
						{
							vLotno= Lotnosearchresults[0].getId();
						}
					}

					var lineCount = request.getLineItemCount('custpage_deliveryord_items');
					nlapiLogExecution('ERROR','lineCount',lineCount);

					vBatch=vLotno;
					for(var k=1;k<=lineCount;k++){

						var vSku = request.getLineItemValue('custpage_deliveryord_items', 'custpage_hidden_skuid', k);
						var vOldLOTno = request.getLineItemValue('custpage_deliveryord_items', 'custpage_hiddenlotno', k);
						var vNewLOTno = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_lotno', k);
						var gridLineno=request.getLineItemValue('custpage_deliveryord_items', 'custpage_lineno', k);
						var vComments = request.getLineItemValue('custpage_deliveryord_items', 'custpage_comments', k);
						var newQty = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_comitqty', k);
						if(vOldLOTno=="" ||vOldLOTno==null)
						{
							vOldLOTno="";
						}
						if(vLotno=="" ||vLotno==null)
						{
							vLotno="";
						}
						if(vNewLOTno=="" ||vNewLOTno==null)
						{
							vNewLOTno="";
						}

						/*var str = 'vSku.' + vSku + '<br>';
						str = str + 'vLotno.' + vLotno + '<br>';	
						str = str + 'vNewLOTno. ' + vNewLOTno + '<br>';	
						str = str + 'vOldLOTno. ' + vOldLOTno + '<br>';	
						str = str + 'gridLineno. ' + gridLineno + '<br>';	
						str = str + 'vlineno. ' + vlineno + '<br>';	
						str = str + 'vQty. ' + vQty + '<br>';	
						str = str + 'strItem. ' + strItem + '<br>';	

						nlapiLogExecution('DEBUG', 'Lot Values', str);*/

						if((vNewLOTno!=vLotno) && (gridLineno==vlineno) && (strItem==vSku) && (vOldLOTno==vLotno))
						{
							vBatch=vNewLOTno;
							nlapiLogExecution('ERROR','Lot# s are not same',vBatch);
							break;
						}
						if((parseFloat(newQty)!=parseFloat(strQty)) && (gridLineno==vlineno) && (strItem==vSku))
						{
							vQty = newQty;
							strQty = newQty
							//nlapiLogExecution('ERROR','vQty',vQty);
							break;
						}
//						else
//						{
//						vBatch=vLotno;
//						}

					}
					nlapiLogExecution('Debug','Time stamp 9',TimeStampinSec());
					var fields = ['custitem_item_family', 'custitem_item_group','recordType','custitem_ebizbatchlot'];
					var ComponentItemType = nlapiLookupField('item', strItem, 'recordType');
					var vItemType="";
					var batchflag="F";
					nlapiLogExecution('Debug','Time stamp 10',TimeStampinSec());
					if (ComponentItemType != null && ComponentItemType !='')
					{
						//nlapiLogExecution('ERROR','Hi2 ');
						var columns = nlapiLookupField(ComponentItemType, strItem, fields);

						nlapiLogExecution('Debug','Time stamp 11',TimeStampinSec());
						var itemfamily = columns.custitem_item_family;
						var itemgroup = columns.custitem_item_group;
						vItemType = columns.recordType;
						batchflag= columns.custitem_ebizbatchlot;
					}
					//avlqty= (parseFloat(strQty)*parseFloat(varAssemblyQty));
					//alert("avlqty " + avlqty);
					//var arryinvt=PickStrategieKittoStock(strItem, itemfamily, itemgroup, avlqty);
					//nlapiLogExecution('ERROR','Hi3 ');

					//var arryinvt=PickStrategieKittoStock(strItem, itemfamily, itemgroup, strQty,vLocation,vBatch);

					/*** The below code is merged from Endochoice account on 07Mar13 by Santosh as part of Standard bundle***/	
					// to get PickfaceLocaion details for the Item
					nlapiLogExecution('Debug','Time stamp before getPFLocationsForOrder',TimeStampinSec());
					//var pfLocationResults = getPFLocationsForOrder(vWMSlocation,strItem);
					nlapiLogExecution('Debug','Time stamp after getPFLocationsForOrder',TimeStampinSec());
					// PickfaceLocationResults added as a parameter to PickStrategieKittoStockNew function
					var arryinvt=PickStrategieKittoStockNew(strItem,vQty,vLocation,ItemInfoResults,pickrulesarray,vBatch,
							pfLocationResults,vItemType,batchflag,strItemstatus);
					nlapiLogExecution('Debug','Time stamp after PickStrategieKittoStockNew',TimeStampinSec());
					/*** up to here ***/
					vitemText=searchresults.getLineItemText('item', 'item', m);
					if(arryinvt.length>0)
					{
						nlapiLogExecution('ERROR','arryinvt.length',arryinvt.length);
						var vLot="";
						var vPrevLot="";
						var vLotQty=0;
						var vPickzone='';
						var vPickStrategy='';
						var vPickMethod='';
						var vInvtResultFlag='';
						//var vTempLot="";
						var vBoolFound=false;
						for (j=0; j < arryinvt.length; j++) 
						{			
							var invtarray= arryinvt[j];  	

							//vitem =searchresults[i].getValue('memberitem');		
							//vitemText =searchresults[i].getText('memberitem');


							if(vlp == "" || vlp == null)
								vlp = invtarray[2];
							else
							{
								vlp = vlp+","+ invtarray[2];
								vMultipleLp=invtarray[2];
							}
							if(vTempLot == "" || vTempLot == null)
								vTempLot = invtarray[4];
							else
							{
								vTempLot = vTempLot+","+ invtarray[4];
								vMultipleTempLot=invtarray[4];
								nlapiLogExecution('ERROR','vMultipleTempLot',vMultipleTempLot);
							}
							if(vPrevLot==null || vPrevLot== "")
							{
								vPrevLot=invtarray[4];
								vLotQty=parseFloat(invtarray[0]);
								vLot=invtarray[4];
								vBoolFound=true;
							}
							else if(vPrevLot==invtarray[4])
							{
								vLotQty=vLotQty+ parseFloat(invtarray[0]);						
							}
							else if(vPrevLot!=invtarray[4])
							{
								if(vlotnoWithQty == "" || vlotnoWithQty == null)
									vlotnoWithQty = vPrevLot+"(" + vLotQty +")";//invtarray[7] is lotno id
								else
									vlotnoWithQty = vlotnoWithQty+ String.fromCharCode(5) + vPrevLot +"(" + vLotQty +")";

								if(vLot == "" || vLot == null)
									vLot = invtarray[4];
								else
									vLot = vLot+","+ invtarray[4];

								vPrevLot=invtarray[4];
								vLotQty=parseFloat(invtarray[0]);

							}
							if(vqty == "" || vqty == null)
								vqty = invtarray[0];
							else
							{
								vqty = vqty+","+ invtarray[0];
								vMultipleQty=invtarray[0];
							}

							var qty1 = invtarray[0];

							if(vlocationText == "" || vlocationText == null)
								vlocationText = invtarray[6];
							else
								vlocationText = vlocationText+","+ invtarray[6];

							if(vlocation == "" || vlocation == null)
								vlocation = invtarray[1];
							else
							{
								vlocation = vlocation+","+ invtarray[1];
								vMultipleLocation=invtarray[1];
							}

							nlapiLogExecution('ERROR','invtarray[3] ',invtarray[3]);

							if(vrecid == "" || vrecid == null)
								vrecid = invtarray[3];
							else
							{
								vrecid = vrecid+","+ invtarray[3];
								vMultipleRecid=invtarray[3];
							}



							if(vremainqty == "" || vremainqty == null)
								vremainqty = invtarray[5];
							else
								vremainqty = vremainqty+","+ invtarray[5];
							location = invtarray[1];
							vitemqty=parseFloat(vitemqty)+parseFloat(qty1);

							vPickzone=invtarray[8];
							vPickStrategy=invtarray[9];
							vPickMethod=invtarray[10];
							vInvtResultFlag =invtarray[11];
							if(vMultipleLp!=""||vMultipleQty!=""||vMultipleTempLot!=""||vMultipleLocation!=""||vMultipleRecid!="")
							{
								nlapiLogExecution('ERROR', 'If_muliple ','sucess');
								var CurrentRow=[vId,vMultipleRecid,vWOId,strItem,vMultipleLp,vMultipleQty,vMultipleTempLot,vMultipleLocation,vWMSlocation,vComments,vlineno,vPickzone,vPickStrategy,vPickMethod,vInvtResultFlag];
								SearchresultsArray.push(CurrentRow);
								nlapiLogExecution('ERROR', 'CurrentRow for multiple ',CurrentRow);

							}
							else
							{
								nlapiLogExecution('ERROR', 'else_muliple','sucess');
								var CurrentRow=[vId,vrecid,vWOId,strItem,vlp,vqty,vTempLot,vlocation,vWMSlocation,vComments,vlineno,vPickzone,vPickStrategy,vPickMethod,vInvtResultFlag];
								SearchresultsArray.push(CurrentRow);
								nlapiLogExecution('ERROR', 'CurrentRow ',CurrentRow);

							}				
						}
						nlapiLogExecution('Debug','Time stamp 15',TimeStampinSec());
						if(vBoolFound==true)
						{
							/*if(vlotnoWithQty == "" || vlotnoWithQty == null)
							vlotnoWithQty = invtarray[4]+"(" + vLotQty +")";//invtarray[7] is lotno id
						else
							vlotnoWithQty = vlotnoWithQty+","+ invtarray[4]+"(" + vLotQty +")";*/

							if(vlotnoWithQty == "" || vlotnoWithQty == null)
								vlotnoWithQty = invtarray[4]+"(" + vLotQty +")";
							else
								vlotnoWithQty = vlotnoWithQty+ String.fromCharCode(5) + invtarray[4]+"(" + vLotQty +")";


							//alert("vTempLot" + vTempLot);
							//alert("vLot.split(',').length" + vLot.split(',').length);
							// alert("vLotQty " + vlotnoWithQty);				
							nlapiLogExecution('ERROR', 'vLot ', vLot);
							nlapiLogExecution('ERROR', 'vlotnoWithQty ', vlotnoWithQty);
							if(vLot.split(',').length>1)
								vLot=vlotnoWithQty;
						}
						nlapiLogExecution('ERROR', 'EndOf for Loop ', 'FORLoopEND');
					}
					else
					{
						// case # 20123356 start
						if(LotnoArray.length>1)
						{
							vAlert=vAlert+vitemText+ ": You have only " + vitemqty +" available out of " +strQty + "    \n";
							nlapiLogExecution('ERROR', 'if muliple lot#s for Item,no Inventory for ',vitemText);
						}
						// case # 20123356 end				
					}
					nlapiLogExecution('Debug','Time stamp 16',TimeStampinSec());

				}//end of lotnoarray forloop

				if(strQty > vitemqty )			
				{ 
					vAlert=vAlert+vitemText+ ": You have only " + vitemqty +" available out of " +strQty + "    \n";

				}
				else 
				{
					/*alert("vlp" + vlp);
					alert("location" + location);
					alert("vrecid" + vrecid);
					alert("vLot" + vLot);
					alert("vTempLot" + vTempLot);
					alert("vqty" + vqty);
					alert("vlocation" + vlocation);*/


					/*var filters = new Array();
					filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));
					filters.push(new nlobjSearchFilter('status', null, 'is', 'WorkOrd:B'));
					filters.push(new nlobjSearchFilter('line', null, 'is', m));
					filters.push(new nlobjSearchFilter('id', null, 'is', m));


					var searchResultsWO = nlapiSearchRecord('workorder', null, filters, null);*/

					//nlapiSelectLineItem('item',m);
					//if(searchResultsWO != null && searchResultsWO != "")
					{
						try {

							//var InvValues = [vlp, location, vrecid, vLot,vTempLot,vqty,vlocation,vitemText,strItem,strQty];
							//vInvReturn.push(InvValues);
							/*searchResults.nlapiSetCurrentLineItemValue('item','custcol_ebiz_lp',vlp);
							nlapiSetCurrentLineItemValue('item','custcol_locationsitemreceipt', location);//previously it is vlocation
							nlapiSetCurrentLineItemValue('item','custcol_ebizwoinventoryref', vrecid);
							//alert("vLot "+ vLot);
							//nlapiSetCurrentLineItemValue('item','serialnumbers', vLot);
							// For default Lot
							vLot=vitemText.replace(/ /g,"-");
							//alert("vLot "+ vLot);
							nlapiSetCurrentLineItemValue('item','serialnumbers', vLot);
							//nlapiSetCurrentLineItemValue('item','serialnumbers_display', vLot);					
							var newLot = vLot.split(','); 
						newLot = newLot.join(String.fromCharCode(5)); 
						nlapiSetCurrentLineItemValue('item','serialnumbers', newLot);

							var newLot = vLot.split(','); 
							newLot = newLot.join(String.fromCharCode(5)); 
							nlapiLogExecution('ERROR', 'Before Set Lot no', newLot);
							nlapiSetCurrentLineItemValue('item','serialnumbers', newLot);
							nlapiLogExecution('ERROR', 'After Set Lot no', newLot);
							if(vTempLot != null && vTempLot != "")
								nlapiSetCurrentLineItemValue('item','custcol_ebizwolotinfo', vTempLot);
							nlapiSetCurrentLineItemValue('item','custcol_ebizwoavalqty', vqty);
							nlapiSetCurrentLineItemValue('item','custcol_ebizwobinloc', vlocation);
							nlapiSetCurrentLineItemValue('item','custcol_ebizwolotlineno', m);
							nlapiCommitLineItem('item');
							nlapiLogExecution('ERROR', 'linelot ', nlapiGetCurrentLineItemValue('item','custcol_ebizwolotlineno'));
							nlapiLogExecution('ERROR', 'Committed ', 'Success');*/
//							nlapiLogExecution('ERROR', 'vrecid ', vrecid);
//							nlapiLogExecution('ERROR', 'vlp ', vlp);
//							nlapiLogExecution('ERROR', 'location ', location);
//							nlapiLogExecution('ERROR', 'vTempLot ', vTempLot);
							searchresults.setLineItemValue('item','custcol_ebiz_lp',m,vlp);
							searchresults.setLineItemValue('item','custcol_locationsitemreceipt',m, location);//previously it is vlocation
							searchresults.setLineItemValue('item','custcol_ebizwoinventoryref', m,vrecid);
							nlapiLogExecution('ERROR', 'After ', 'After');

							var confirmLotToNS='Y';
							nlapiLogExecution('Debug','Time stamp 17',TimeStampinSec());
							confirmLotToNS=GetConfirmLotToNS(location);
							nlapiLogExecution('Debug','Time stamp 18',TimeStampinSec());
							if(confirmLotToNS=='N')
								vLot=vitemText.replace(/ /g,"-");

							nlapiLogExecution('ERROR', 'vItemType',vItemType);

							nlapiLogExecution('ERROR', 'confirmLotToNS',confirmLotToNS);
							nlapiLogExecution('ERROR', 'vLot',vLot);				 
							//if (vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem" || batchflag=="T" )
							if (vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem")
								searchresults.setLineItemValue('item','serialnumbers',m, vLot);


							/*var newLot = vLot.split(','); 
							newLot = newLot.join(String.fromCharCode(5)); 
							nlapiLogExecution('ERROR', 'Before Set Lot no',m, newLot);
							searchresults.setLineItemValue('item','serialnumbers',m, newLot);*/
							//nlapiLogExecution('ERROR', 'After Set Lot no', newLot);

							//nlapiLogExecution('ERROR', 'invref ', searchresults.getLineItemValue('item', 'custcol_ebizwoinventoryref', m));

							searchresults.setFieldValue('custbody_ebiz_update_confirm','T');
							//if((vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem" || batchflag=="T") && vTempLot != null && vTempLot != "")
							if((vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem") && vTempLot != null && vTempLot != "")
								searchresults.setLineItemValue('item','custcol_ebizwolotinfo',m, vTempLot);

							searchresults.setLineItemValue('item','custcol_ebizwoavalqty',m, vqty);
							searchresults.setLineItemValue('item','custcol_ebizwobinloc',m, vlocation);
							searchresults.setLineItemValue('item','custcol_ebizwobinlocvalue',m, vlocationText);						
							searchresults.setLineItemValue('item','custcol_ebizwolotlineno', m,m);

							//nlapiCommitLineItem('item');
							nlapiLogExecution('ERROR', 'linelot ', searchresults.getLineItemValue('item','custcol_ebizwolotlineno',m));


						}
						catch (e) {

							if (e instanceof nlobjError) 
							{
								nlapiLogExecution('ERROR', 'system error', e.getCode()+ '\n' + e.getDetails());

							}

							else 
							{ 
								nlapiLogExecution('ERROR', 'unexpected error', e.toString());
								//alert('unexpected error' + '\n' + 'Error: ' + e.toString() );
							}
//							nlapiCancelLineItem('item');

							nlapiLogExecution('ERROR','Error ', e.toString());
							var ErrorMsg = nlapiCreateError('CannotAllocate',e.toString()+'For Item'+vitemText, true);
							throw ErrorMsg;

							break;

						}
					}
				}


			}

		}
	}
	if(vAlert!=null && vAlert!='')
	{ 
		/*var vErrorRet=new Array();
		vErrorRet.push('FAIL');
		vErrorRet.push(vAlert);
		return vErrorRet;*/
		return 'FAIL~' + vAlert;
	}
	else
	{
		nlapiLogExecution('ERROR', 'SearchresultsArray',SearchresultsArray.length);
		//nlapiLogExecution('ERROR', 'Before commit ');
		//nlapiLogExecution('Debug','Time stamp 19',TimeStampinSec());
		var CommittedId = nlapiSubmitRecord(searchresults, true);
		nlapiLogExecution('Debug','Time stamp before AllocateInv',TimeStampinSec());
		AllocateInv(vId,SearchresultsArray,vWOId,response,ItemInfoResults,pickrulesarray,pfLocationResults);
		nlapiLogExecution('Debug','Time stamp after AllocateInv',TimeStampinSec());
		//nlapiLogExecution('ERROR', 'CommittedId ', CommittedId);
		//nlapiLogExecution('ERROR', 'Committed ', 'Success');
		var context = nlapiGetContext();
		nlapiLogExecution('ERROR','Remaining usage at End',context.getRemainingUsage());
		return 'SUCCESS';
	}
}

/**To get confirm LOT toNS based on system rule
 * 
 * @param Site
 * @returns Y or N
 */
function GetConfirmLotToNS(Site)
{

	try{
		var filters = new Array();

		filters[0] = new nlobjSearchFilter('custrecord_ebizruletype', null, 'is', 'WMSLOT');
		filters[1] = new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', ['@NONE@', Site]);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
		columns[1] = new nlobjSearchColumn('custrecord_ebizsite');

		columns[1].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, filters, columns);	
		if(searchresults != null && searchresults != '')
		{
			if(searchresults[0].getValue('custrecord_ebizrulevalue') != null && searchresults[0].getValue('custrecord_ebizrulevalue') != '')
			{
				return searchresults[0].getValue('custrecord_ebizrulevalue');
			}
			else
				return 'Y';
		}
		else
			return 'Y';
	}
	catch (exp) 
	{
		nlapiLogExecution('ERROR', 'Exception in GetConfirmLotToNS and returning Y: ', exp);
		return 'Y';
	}	
}

function AllocateInv(vId,SearchresultsArray,vWOName,response,ItemInfoResults,pickrulesarray,pfLocationResults)
{
	// Create a Record in Opentask
	var now = new Date();
	//a Date object to be used for a random value
	var now = new Date();
	//now= now.getHours();
	//Getting time in hh:mm tt format.
	var a_p = "";
	var d = new Date();
	var curr_hour = now.getHours();
	if (curr_hour < 12) {
		a_p = "am";
	}
	else {
		a_p = "pm";
	}
	if (curr_hour == 0) {
		curr_hour = 12;
	}
	if (curr_hour > 12) {
		curr_hour = curr_hour - 12;
	}

	var curr_min = now.getMinutes();

	curr_min = curr_min + "";

	if (curr_min.length == 1) {
		curr_min = "0" + curr_min;
	}

	nlapiLogExecution('Debug','Time stamp 21',TimeStampinSec());
	nlapiLogExecution('Debug','ItemInfoResults',ItemInfoResults);
	nlapiLogExecution('Debug','pickrulesarray',pickrulesarray);
	nlapiLogExecution('Debug','pfLocationResults',pfLocationResults);
	var tempFlag = "F";
	if(SearchresultsArray!=null && SearchresultsArray!='' && SearchresultsArray.length>0)
	{
		nlapiLogExecution('ERROR', 'SearchresultsArray',SearchresultsArray.length);
		var currentContext = nlapiGetContext();  
		var currentUserID = currentContext.getUser();
		var InvtRecordArray = new Array();
		var OpentaskInternalId = new Array();


		for (var i=0; i<SearchresultsArray.length; i++){
			var vId=SearchresultsArray[i][0];
			var vLineRec=SearchresultsArray[i][1];
			var vWOId=SearchresultsArray[i][2];
			var varsku=SearchresultsArray[i][3];
			var vLP=SearchresultsArray[i][4];
			var vLineQty=SearchresultsArray[i][5];
			var vBatch=SearchresultsArray[i][6];
			var vactLocationtext=SearchresultsArray[i][7];
			var vLocation=SearchresultsArray[i][8];
			var vComments=SearchresultsArray[i][9];
			var vlineno=SearchresultsArray[i][10];
			var vpickzone=SearchresultsArray[i][11];
			var vpickruleId=SearchresultsArray[i][12];
			var vpickmethod=SearchresultsArray[i][13];
			var vInvtResFlag = SearchresultsArray[i][14];

			var str = 'vId.' + vId + '<br>';
			str = str + 'vLineRec.' + vLineRec + '<br>';	
			str = str + 'varsku. ' + varsku + '<br>';	
			str = str + 'vBatch. ' + vBatch + '<br>';	
			str = str + 'vactLocationtext. ' + vactLocationtext + '<br>';	
			str = str + 'vComments. ' + vComments + '<br>';	
			str = str + 'vLineQty. ' + vLineQty + '<br>';	
			str = str + 'vlineno. ' + vlineno + '<br>';	
			str = str + 'vpickzone. ' + vpickzone + '<br>';	
			str = str + 'vpickruleId. ' + vpickruleId + '<br>';	
			str = str + 'vpickmethod. ' + vpickmethod + '<br>';	
			str = str + 'vInvtResFlag. ' + vInvtResFlag + '<br>';	


			nlapiLogExecution('DEBUG', 'SearchresultsArray values', str);

			var SkipFlag ="F";
			nlapiLogExecution('Debug','Time stamp 22',TimeStampinSec());
			var scount=1;
			LABL1: for(var k=0;k<scount;k++)
			{
				try
				{
					var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', vLineRec);
					nlapiLogExecution('Debug','Time stamp 23',TimeStampinSec());
					var qty = transaction.getFieldValue('custrecord_ebiz_qoh');
					var allocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
					var vLP=transaction.getFieldValue('custrecord_ebiz_inv_lp');
					var vPackcode=transaction.getFieldValue('custrecord_ebiz_inv_packcode');
					var vSKUStatus=transaction.getFieldValue('custrecord_ebiz_inv_sku_status');
					var vAvailableQty=transaction.getFieldValue('custrecord_ebiz_avl_qty');

					nlapiLogExecution('ERROR', 'qty', qty);
					nlapiLogExecution('ERROR', 'allocqty', allocqty);
					nlapiLogExecution('ERROR', 'vAvailableQty', vAvailableQty);
					if(allocqty == null || allocqty == '')
						allocqty=0;
					nlapiLogExecution('ERROR', 'varqty', vLineQty);
					if(parseFloat(vAvailableQty)>=parseFloat(vLineQty))
					{
						transaction.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(allocqty)+ parseFloat(vLineQty)).toFixed(5));
						transaction.setFieldValue('custrecord_ebiz_displayfield', 'N');			 
						transaction.setFieldValue('custrecord_ebiz_callinv', 'N');
						nlapiSubmitRecord(transaction, false, true);

						//capture createinvt internal ids
						var currentArray = new Array(); 
						currentArray = [vLineRec,vLineQty];
						InvtRecordArray.push(currentArray);
					}
					else
					{
						nlapiLogExecution('Debug','Time stamp 1011',TimeStampinSec());
						// if inventory is available in another binlocation / handling  inventory concurrency 
						if(vInvtResFlag == 'T')
						{

							tempFlag =ConcurrencyHandlingForInvt(varsku,vLineQty,vLocation,vlineno,vComments,ItemInfoResults,pickrulesarray,vBatch,pfLocationResults,tempFlag,InvtRecordArray,OpentaskInternalId,vId,vWOName);
							nlapiLogExecution('Debug','tempFlag',tempFlag);
							if(tempFlag == "F")
								SkipFlag = "T";
						}
						else
						{
							tempFlag ="T";
						}

					}
					nlapiLogExecution('Debug','InvtRecordArray',InvtRecordArray);
					nlapiLogExecution('Debug','tempFlag',tempFlag);
					nlapiLogExecution('Debug','Time stamp 24',TimeStampinSec());
				}
				catch(ex)
				{
					nlapiLogExecution('ERROR', 'Exception in deleteAllocations',ex);

					var exCode='CUSTOM_RECORD_COLLISION'; 
					var wmsE='Inventory record being updated by another user. Please try again...';
					if (ex instanceof nlobjError) 
					{	
						wmsE=ex.getCode() + '\n' + ex.getDetails();
						exCode=ex.getCode();
					}
					else
					{
						wmsE=ex.toString();
						exCode=ex.toString();
					}  

					nlapiLogExecution('ERROR', 'Exception in RF Inv move : ', ex);

					if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
					{ 
						scount=scount+1;
						continue LABL1;
					}
					else break LABL1;
				}
			}

			if(tempFlag == "T")
			{
				//break the main Forloop, if any of the component item is failed for location generation. 
				break;
			}

			if(vWOName == null || vWOName == '')
			{
				var rcptrecordid3 = nlapiLoadRecord('workorder', vId);

				var woidWO= rcptrecordid3.getFieldValue('tranid');
				nlapiLogExecution('ERROR','woidWO  ',woidWO);
				vWOId=woidWO;
			}
			else
				vWOId=vWOName;

			nlapiLogExecution('Debug','Time stamp 25',TimeStampinSec());
			nlapiLogExecution('Debug','SkipFlag',SkipFlag);

			//if SkipFlag ="T" means open task record is created in concurrency function no need to create again.

			if(tempFlag == "F" && SkipFlag != "T")
			{
				var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
				customrecord.setFieldValue('custrecord_tasktype', 3); // 5 for KTS,3 for PICK, 4 for SHIP 
				//customrecord.setFieldValue('custrecordact_begin_date', (parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
				customrecord.setFieldValue('custrecordact_begin_date', DateStamp());
				customrecord.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));

				//customrecord.setFieldValue('custrecord_act_end_date', (parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
				//customrecord.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
				//customrecord.setFieldValue('custrecord_expe_qty', varqty); 
				//nlapiLogExecution('ERROR','expe qty ',vLineQty);
				nlapiLogExecution('ERROR','binlocation',vactLocationtext);

				customrecord.setFieldValue('custrecord_expe_qty', parseFloat(vLineQty).toFixed(5));  //Commented and added to resovle decimal issue
				customrecord.setFieldValue('custrecord_act_qty', parseFloat(vLineQty).toFixed(5));
				customrecord.setFieldValue('custrecord_wms_status_flag', 9);	// 14 stands for outbound process


				customrecord.setFieldValue('custrecord_actbeginloc',vactLocationtext);	
				//customrecord.setFieldValue('custrecord_actendloc', vbinloc);						
				//customrecord.setFieldValue('custrecord_lpno', vlp); 
				customrecord.setFieldValue('custrecord_upd_date', DateStamp());
				//customrecord.setFieldValue('custrecord_sku', vskuText);
				customrecord.setFieldValue('custrecord_ebiz_sku_no', varsku);
				customrecord.setFieldValue('name', vWOId);		

				customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
				customrecord.setFieldValue('custrecord_ebizuser', currentUserID);
				//customrecord.setFieldValue('custrecord_ebiz_wave_no', eBizWaveNo);
				customrecord.setFieldValue('custrecord_ebiz_order_no', vId);
				customrecord.setFieldValue('custrecord_batch_no', vBatch);
				customrecord.setFieldValue('custrecord_ebiz_cntrl_no', vId);
				//customrecord.setFieldValue('custrecord_ebiz_cntrl_no', rcptrecordid);

				customrecord.setFieldValue('custrecord_sku', varsku);
				customrecord.setFieldValue('custrecord_line_no', parseFloat(vlineno));
				customrecord.setFieldValue('custrecord_lpno', vLP);
				customrecord.setFieldValue('custrecord_packcode', vPackcode);					
				customrecord.setFieldValue('custrecord_sku_status', vSKUStatus);
				customrecord.setFieldValue('custrecord_wms_location', vLocation);
				customrecord.setFieldValue('custrecord_invref_no', vLineRec);
				customrecord.setFieldValue('custrecord_notes', vComments);	
				customrecord.setFieldValue('custrecord_ebizrule_no', vpickruleId);
				customrecord.setFieldValue('custrecord_ebizmethod_no', vpickmethod);
				customrecord.setFieldValue('custrecord_ebizzone_no', vpickzone);

				if(vpickzone !=null && vpickzone !='')
				{
					customrecord.setFieldValue('custrecord_ebiz_zoneid', vpickzone);
				}


				nlapiLogExecution('DEBUG', 'beginLocationId',vactLocationtext);
				if(vactLocationtext!=null && vactLocationtext!='')
				{
					nlapiLogExecution('Debug','Time stamp 26',TimeStampinSec());
					var locgroupfields = ['custrecord_inboundlocgroupid','custrecord_outboundlocgroupid'];
					var locgroupcolumns = nlapiLookupField('customrecord_ebiznet_location', vactLocationtext, locgroupfields);
					nlapiLogExecution('Debug','Time stamp 27',TimeStampinSec());
					var inblocgroupid=locgroupcolumns.custrecord_inboundlocgroupid;
					var oublocgroupid=locgroupcolumns.custrecord_outboundlocgroupid;
					nlapiLogExecution('DEBUG', 'inblocgroupid',inblocgroupid);
					var locgroupseqfields = ['custrecord_sequenceno'];
					//code added on 011012 by suman
					//Allow to insert sequence no only if we get some value form inbonlog group Id.
					//if(inblocgroupid!=null&&inblocgroupid!="")
					if(oublocgroupid!=null && oublocgroupid!="")
					{
						nlapiLogExecution('Debug','Time stamp 28',TimeStampinSec());
						//var locgroupseqcolumns = nlapiLookupField('customrecord_ebiznet_loc_group', inblocgroupid, locgroupseqfields);
						var locgroupseqcolumns = nlapiLookupField('customrecord_ebiznet_loc_group', oublocgroupid, locgroupseqfields);
						var locgroupidseq=locgroupseqcolumns.custrecord_sequenceno;
						nlapiLogExecution('DEBUG', 'locgroupidseq',locgroupidseq);

						customrecord.setFieldValue('custrecord_bin_locgroup_seq', locgroupidseq);
						nlapiLogExecution('Debug','Time stamp 29',TimeStampinSec());
					}
				}

				var expiryDateInLot='';
				if(vBatch!=null && vBatch !=''){
					var filterbatch = new Array();
					filterbatch[0] = new nlobjSearchFilter('name', null, 'is', vBatch);
					filterbatch[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', varsku);
					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_ebizexpirydate'); 
					var Lotnosearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterbatch,columns);
					if(Lotnosearchresults!=null && Lotnosearchresults!='')
					{
						nlapiLogExecution('ERROR', 'expiryDateInLot',expiryDateInLot);
						vLotno= Lotnosearchresults[0].getId();
						expiryDateInLot= Lotnosearchresults[0].getValue('custrecord_ebizexpirydate');
					}
					nlapiLogExecution('ERROR', 'expiryDateInLot',expiryDateInLot);
					if(expiryDateInLot !=null && expiryDateInLot!='')
						customrecord.setFieldValue('custrecord_expirydate', expiryDateInLot);

				}

				nlapiLogExecution('Debug','Time stamp 30',TimeStampinSec());
				//nlapiLogExecution('ERROR','eBizWaveNo ',eBizWaveNo);


				var OpentaskId= nlapiSubmitRecord(customrecord);			
				nlapiLogExecution('Debug','Time stamp 31',TimeStampinSec());
				nlapiLogExecution('ERROR', 'Success');

				if(OpentaskId !=null && OpentaskId !='')
				{
					nlapiLogExecution('ERROR', 'OpentaskId',OpentaskId);
					OpentaskInternalId.push(OpentaskId);
				}
			}
		}

		nlapiLogExecution('ERROR', 'tempFlag',tempFlag);


		if(tempFlag == "T")
			ComponentItemsReversal(InvtRecordArray,OpentaskInternalId);

	}
	nlapiLogExecution('ERROR', 'tempFlag',tempFlag);
	if(tempFlag == "F")
	{
		var WOarray = new Array();
		nlapiLogExecution('ERROR', 'vId',vId);
		WOarray["custparam_wo"] = vId;
		response.sendRedirect('SUITELET', 'customscript_ebiz_bom_report_sl', 'customdeploy_ebiz_bom_report_di', false,WOarray);
		return false;
	}
	else
	{

		nlapiLogExecution('ERROR','Allocation Failed ');
		var ErrorMsg = nlapiCreateError('CannotAllocate','Bin location generation is failed for this Work Order', true);
		throw ErrorMsg;
	}



}


function PickStrategieKittoStock(item,itemfamily,itemgroup,avlqty,location,serialNo){
	//alert("5");
	var actallocqty = 0;
	var Resultarray = new Array();
	nlapiLogExecution('ERROR','item',item);
	nlapiLogExecution('ERROR','itemgroup',itemgroup);
	nlapiLogExecution('ERROR','itemfamily',itemfamily);
	nlapiLogExecution('ERROR','location',location);
	nlapiLogExecution('ERROR','serialNo',serialNo);

	var filters = new Array();	

	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	filters.push(new nlobjSearchFilter('custrecord_ebizskupickrul', null, 'anyof', ['@NONE@',item]));

	if (itemgroup != "" && itemgroup != null) {
		filters.push(new nlobjSearchFilter('custrecord_ebizskugrouppickrul',null,  'anyof', ['@NONE@',itemgroup]));
	}
	if (itemfamily!= "" && itemfamily!= null) {
		filters.push(new nlobjSearchFilter('custrecord_ebizskufamilypickrul', null, 'anyof', ['@NONE@',itemfamily]));
	}

	if (location!= "" && location!= null) {
		filters.push(new nlobjSearchFilter('custrecord_ebizsitepickrule', null, 'anyof', ['@NONE@',location]));
	}


	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_ebizpickzonerul');
	columns[2] = new nlobjSearchColumn('custrecord_ebizpickmethod');
	columns[3] = new nlobjSearchColumn('custrecord_ebizsequencenopickrul');

	//Fetching pick rule
	nlapiLogExecution('ERROR','Pick rule Fectching');
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters, columns);

	for (var i = 0; searchresults != null && i < searchresults.length; i++) {

		var searchresultpick = searchresults[i];				
		var vpickzone=searchresultpick.getValue('custrecord_ebizpickzonerul');			

		nlapiLogExecution('ERROR', "LP: " + LP);
		nlapiLogExecution('ERROR','PickZone',vpickzone);			
		var filterszone = new Array();	
		filterszone.push(new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'anyof', ['@NONE@',vpickzone]));
		filterszone.push(new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'is', vpickzone,null));
		filterszone.push(new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', ['@NONE@',location]));

		var columnzone = new Array();
		columnzone[0] = new nlobjSearchColumn('custrecord_locgroup_no');

		var searchresultszone = nlapiSearchRecord('customrecord_zone_locgroup', null, filterszone, columnzone);
		nlapiLogExecution('ERROR','Loc Group Fetching');

		for (var j = 0; searchresultszone != null && j < searchresultszone.length; j++) {

			var searchresultzone = searchresultszone[j];
			var vlocgroupno = searchresultzone.getValue('custrecord_locgroup_no');

			nlapiLogExecution('ERROR', 'LocGroup'+ searchresultzone.getValue('custrecord_locgroup_no'), searchresultzone.getText('custrecord_locgroup_no'));

			var filtersinvt = new Array();

			filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', item));
			filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['3','19']));
			filtersinvt.push(new nlobjSearchFilter('custrecord_outboundlocgroupid','custrecord_ebiz_inv_binloc', 'is', vlocgroupno));

			if (location!= "" && location!= null) {
				filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [location]));
			}
			//code added by santosh on 13aug12
			if (serialNo!= "" && serialNo!= null) {
				filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', serialNo));
			}
			//end of the code on 13Aug12
			nlapiLogExecution('ERROR', "custrecord_ebiz_inv_sku: " + item);

			var columnsinvt = new Array();
			columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
			columnsinvt[1] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
			columnsinvt[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
			columnsinvt[3] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
			columnsinvt[4] = new nlobjSearchColumn('custrecord_ebiz_inv_fifo');
			columnsinvt[5] = new nlobjSearchColumn('custrecord_pickseqno'); 
			columnsinvt[6] = new nlobjSearchColumn('custrecord_ebiz_qoh');			
			columnsinvt[7] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');
			columnsinvt[4].setSort();
			columnsinvt[5].setSort();
			columnsinvt[6].setSort(false);

			var searchresultsinvt = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);
			for (var l = 0; searchresultsinvt != null && l < searchresultsinvt.length; l++) {

				var searchresult = searchresultsinvt[l];
				var actqty = searchresult.getValue('custrecord_ebiz_qoh');
				var allocqty = searchresult.getValue('custrecord_ebiz_alloc_qty');
				var LP = searchresult.getValue('custrecord_ebiz_inv_lp');
				var vactLocation = searchresult.getValue('custrecord_ebiz_inv_binloc');
				var vactLocationtext = searchresult.getText('custrecord_ebiz_inv_binloc');
				var vlotno = searchresult.getValue('custrecord_ebiz_inv_lot');
				var vlotText = searchresult.getText('custrecord_ebiz_inv_lot');
				var Recid = searchresult.getId(); 


				//alert(" 664 actqty:" +actqty);
				//alert(" 664 allocqty:" +allocqty);
				//alert(" 664 LP:" +LP);
				//alert(" 664 vactLocation:" +vactLocation);
				//alert(" 664 vactLocationtext:" +vactLocationtext);
				//alert(" 664 vlotno:" +vlotno);
				//alert(" 664 vlotText:" +vlotText);
				//alert(" 664 Recid:" +Recid);


				nlapiLogExecution('ERROR', 'Recid1', Recid);
				if (isNaN(allocqty)) {
					allocqty = 0;
				}
				if (allocqty == "") {
					allocqty = 0;
				}
				if( parseFloat(allocqty)<0)
				{
					allocqty=0;
				}
				if( parseFloat(actqty)<0)
				{
					actqty=0;
				}
				nlapiLogExecution('ERROR', "vactLocation: " + vactLocation);
				nlapiLogExecution('ERROR', "vactLocationtext: " + vactLocationtext);
				nlapiLogExecution('ERROR', "allocqty: " + allocqty);
				nlapiLogExecution('ERROR', "LP: " + LP);
				var remainqty=0;

				//Added by Ganesh not to allow negative or zero Qtys 
				if(parseFloat(actqty) >=0 && parseFloat(allocqty)>=0)
					remainqty = parseFloat(actqty) - parseFloat(allocqty);


				var cnfmqty;

////				alert("remainqty"+remainqty );
////				alert("avlqty"+avlqty );
				if (parseFloat(remainqty) > 0) {

					////alert("parseFloat(avlqty) "+parseFloat(avlqty));

					if ((parseFloat(avlqty) - parseFloat(actallocqty)) <= parseFloat(remainqty)) {
						cnfmqty = parseFloat(avlqty) - parseFloat(actallocqty);
						actallocqty = avlqty;
						////alert('cnfmqty in if '+cnfmqty);
					}
					else {
						cnfmqty = remainqty;
						////alert('cnfmqty2 ' +cnfmqty);
						actallocqty = parseFloat(actallocqty) + parseFloat(remainqty);
					}

					//alert(" 708 cnfmqty:" +cnfmqty);
					//alert(" 709 actallocqty:" +actallocqty);

					////alert('BeforeIf ' +cnfmqty);
					if (parseFloat(cnfmqty) > 0) {
						////alert('AfterIf ' +cnfmqty);
						//Resultarray
						var invtarray = new Array();
						////alert('cnfmqty3 ' +cnfmqty);
						invtarray[0]= cnfmqty;
						////alert('cnfmqty4 ' +cnfmqty);
						invtarray[1]= vactLocation;
						invtarray[2] = LP;
						invtarray[3] = Recid;
						invtarray[4] = vlotText;
						invtarray[5] =remainqty;
						invtarray[6] =vactLocationtext;
						invtarray[7] =vlotno;
						//alert("cnfmqty "+cnfmqty);
						//alert("cnfmqty " + cnfmqty);
						//alert(vlotno);
						//alert(vlotText);
						nlapiLogExecution('ERROR', 'RecId:',Recid);
						//invtarray[3] = vpackcode;
						//invtarray[4] = vskustatus;
						//invtarray[5] = vuomid;
						Resultarray.push(invtarray);								
					}
				}

				if ((avlqty - actallocqty) == 0) {

					return Resultarray;

				}
			} 
		}
	}

	return Resultarray;  
}

/*code merged from monobind on feb 27th 2013 by Radhika added kititemstatus filter condition */
function addSerialno(temparray,Location,form,serialno,kitItemStatus){

	nlapiLogExecution('ERROR', 'temparray',temparray.length);
	nlapiLogExecution('ERROR', 'kitItemStatus',kitItemStatus);
	nlapiLogExecution('ERROR', 'Location',Location);
	nlapiLogExecution('ERROR', 'temparray',temparray);
	var filterStatus = new Array();

	filterStatus.push(new nlobjSearchFilter('custrecord_allowpickskustatus', null, 'is', 'T'));
	filterStatus.push(new nlobjSearchFilter('custrecord_activeskustatus', null, 'is','T'));
	if(Location != null && Location != '')
		filterStatus.push(new nlobjSearchFilter('custrecord_ebizsiteskus', null, 'anyof',Location));
	var searchresultsStatus = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, filterStatus, null);
	var vStatusArr=new Array();
	if(searchresultsStatus != null && searchresultsStatus != '')
	{
		for(var k=0;k<searchresultsStatus.length;k++)
		{
			vStatusArr.push(searchresultsStatus[k].getId());
		}
	}
	nlapiLogExecution('ERROR', 'vStatusArr',vStatusArr);

	var filter= new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku',null, 'anyof', temparray));
	//if(kitItemStatus!=null && kitItemStatus!='')
	//	filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status',null, 'anyof', kitItemStatus));
	if(vStatusArr != null && vStatusArr != '' && vStatusArr.length>0)
		filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status',null, 'anyof', vStatusArr));
	filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc',null, 'anyof', Location));
	filter.push(new nlobjSearchFilter('custrecord_ebiz_avl_qty', null, 'greaterthan', 0));
	filter.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19']));


	filter.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'noneof','@NONE@'));




	var columns = new Array();	
	//columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_lp'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_sku',null,'group'));			 
	columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_lot',null,'group'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_avl_qty',null,'sum'));
	//columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_sku_status'));
	//columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_binloc'));
	columns[0].setSort(true);
	columns[1].setSort();
	columns[2].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filter, columns);
	/*
	var filterLotno = new Array();
	filterLotno[0] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', temparray);
	filterLotno[1] = new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', Location);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizlotbatch');
	columns[1] = new nlobjSearchColumn('internalid');
	columns[2] = new nlobjSearchColumn('custrecord_ebizsku');
	columns[2].setSort(true);


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterLotno,columns);*/
	if(searchresults!=null)
	{
		nlapiLogExecution('ERROR', 'Lotnoresult', searchresults.length);

		for(var i=0;i<searchresults.length;i++)
		{
			serialno.addSelectOption(searchresults[i].getValue('custrecord_ebiz_inv_lot',null,'group'), (searchresults[i].getText('custrecord_ebiz_inv_lot',null,'group')+"(Qty : "+searchresults[i].getValue('custrecord_ebiz_avl_qty',null,'sum') + ")" +"_"+searchresults[i].getText('custrecord_ebiz_inv_sku',null,'group')));


		}
	}

}

function GetLotname(id)
{
	var batchname="";
	var filterbatch = new Array();
	filterbatch[0] = new nlobjSearchFilter('InternalId', null, 'is', id);

	var columns=new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('InternalId');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterbatch,columns);
	if(searchresults!=null)
	{
		batchname= searchresults[0].getValue('name');
		nlapiLogExecution('ERROR', 'NewBatchno', batchname);
		return batchname;
	}

}

var allocatedInv=new Array();
var vInrefArray=new Array();
var vQtyArray=new Array();
//added pickfacelocationresults as a parameter
function PickStrategieKittoStockNew(item,avlqty,location,ItemInfoArr,pickrulesarray,serialNo,pfLocationResults,vItemType,
		batchflag,Itemstatus){
	nlapiLogExecution('ERROR','Into PickStrategieKittoStockNew');
	var actallocqty = 0;
	var Resultarray = new Array();
	/*** The below code is merged from Endochoice account on 07Mar13 by Santosh as part of Standard bundle***/	
	var PfLocArray =new Array();
	/*** up to here ***/

	var ItemFamily;
	var ItemGroup;

	if(ItemInfoArr!= null && ItemInfoArr != '' )
	{
		nlapiLogExecution('ERROR', 'ItemInfoArr.length', ItemInfoArr.length);
		for(var p=0;p<ItemInfoArr.length;p++)
		{
			if(ItemInfoArr[p].getId()==item)
			{
				ItemFamily = ItemInfoArr[p].getValue('custitem_item_family');
				ItemGroup = ItemInfoArr[p].getValue('custitem_item_group');

			}	
		}	
	}

	var str = 'item.' + item + '<br>';
	str = str + 'ItemInfoArr.' + ItemInfoArr + '<br>';	
	str = str + 'ItemFamily.' + ItemFamily + '<br>';	
	str = str + 'ItemGroup. ' + ItemGroup + '<br>';	
	str = str + 'avlqty. ' + avlqty + '<br>';	
	str = str + 'location. ' + location + '<br>';
	str = str + 'pickrulesarray. ' + pickrulesarray + '<br>';
	str = str + 'serialNo. ' + serialNo + '<br>';
	str = str + 'pfLocationResults. ' + pfLocationResults + '<br>';
	str = str + 'vItemType. ' + vItemType + '<br>';
	str = str + 'batchflag. ' + batchflag + '<br>';
	str = str + 'Itemstatus. ' + Itemstatus + '<br>';

	nlapiLogExecution('DEBUG', 'Function parameters', str);

	nlapiLogExecution('Debug','Time stamp 101',TimeStampinSec());

	var filterStatus = new Array();

	filterStatus.push(new nlobjSearchFilter('custrecord_allowpickskustatus', null, 'is', 'T'));
	filterStatus.push(new nlobjSearchFilter('custrecord_activeskustatus', null, 'is','T'));
	if(location != null && location != '')
		filterStatus.push(new nlobjSearchFilter('custrecord_ebizsiteskus', null, 'anyof',location));
	if(Itemstatus != null && Itemstatus != '')
		filterStatus.push(new nlobjSearchFilter('internalid', null, 'is',Itemstatus));
	var searchresultsStatus = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, filterStatus, null);
	nlapiLogExecution('Debug','Time stamp 102',TimeStampinSec());
	var vStatusArr=new Array();
	if(searchresultsStatus != null && searchresultsStatus != '')
	{
		for(var k=0;k<searchresultsStatus.length;k++)
		{
			vStatusArr.push(searchresultsStatus[k].getId());
		}

		nlapiLogExecution('ERROR', 'vStatusArr',vStatusArr);
	}	

	nlapiLogExecution('Debug','Time stamp 103',TimeStampinSec());


	var vPickRules=new Array();
	if(pickrulesarray != null && pickrulesarray != '' && pickrulesarray.length>0)
	{
		nlapiLogExecution('ERROR', 'pickrulesarray.length', pickrulesarray.length);
		for(var j = 0; j < pickrulesarray.length; j++)
		{
			if((pickrulesarray[j].getValue('custrecord_ebizskufamilypickrul')==null||pickrulesarray[j].getValue('custrecord_ebizskufamilypickrul')=='')||(pickrulesarray[j].getValue('custrecord_ebizskufamilypickrul')!= null && pickrulesarray[j].getValue('custrecord_ebizskufamilypickrul') != '' && pickrulesarray[j].getValue('custrecord_ebizskufamilypickrul') ==ItemFamily))	
			{
				if((pickrulesarray[j].getValue('custrecord_ebizskugrouppickrul')==null||pickrulesarray[j].getValue('custrecord_ebizskugrouppickrul')=='')||(pickrulesarray[j].getValue('custrecord_ebizskugrouppickrul')!= null && pickrulesarray[j].getValue('custrecord_ebizskugrouppickrul') != '' && pickrulesarray[j].getValue('custrecord_ebizskugrouppickrul') ==ItemGroup))	
				{
					if((pickrulesarray[j].getValue('custrecord_ebizskupickrul')==null||pickrulesarray[j].getValue('custrecord_ebizskupickrul')=='')||(pickrulesarray[j].getValue('custrecord_ebizskupickrul')!= null && pickrulesarray[j].getValue('custrecord_ebizskupickrul') != '' && pickrulesarray[j].getValue('custrecord_ebizskupickrul') ==item))	
					{
						vPickRules.push(j);
					}	
				}	
			}	
		}	
	}

	nlapiLogExecution('Debug','Time stamp 104',TimeStampinSec());



	if (vPickRules != null) {
		nlapiLogExecution('ERROR', 'rules count', vPickRules.length);

		var vRemainingQty=avlqty;
		nlapiLogExecution('ERROR', 'vRemainingQty', vRemainingQty);
		for (var s = 0; s < parseFloat(vPickRules.length) && parseFloat(vRemainingQty) > 0; s++) {
			var vPickZoneArr=new Array();

			var vpickzone=pickrulesarray[vPickRules[s]].getValue('custrecord_ebizpickzonerul');
			var pickRuleId = pickrulesarray[vPickRules[s]].getValue('custrecord_ebizruleidpick');
			var pickMethodId = pickrulesarray[vPickRules[s]].getValue('custrecord_ebizpickmethod');
			var pickBinlocGroup = pickrulesarray[vPickRules[s]].getValue('custrecord_ebizlocationgrouppickrul');
			var vBinloction = pickrulesarray[vPickRules[s]].getValue('custrecord_ebizlocationpickrul');
			var pickmethodpickface = pickrulesarray[vPickRules[s]].getValue('custrecord_ebizpickfacelocation','custrecord_ebizpickmethod');

			var pfBinLocationgroupId=new Array();
			if ((pickmethodpickface == 'T') && (pfLocationResults != null && pfLocationResults.length > 0))
			{
				for(var p = 0; p < pfLocationResults.length; p++)
				{
					var vPFSKU=pfLocationResults[p].getValue('custrecord_pickfacesku');
					if(item == vPFSKU)
					pfBinLocationgroupId.push(pfLocationResults[p].getValue('custrecord_inboundlocgroupid', 'custrecord_pickbinloc'));
				}
			}

			var str = 'vPickRules[s].' + vPickRules[s] + '<br>';
			str = str + 'vRemainingQty.' + vRemainingQty + '<br>';	
			str = str + 'Id.' + pickrulesarray[vPickRules[s]].getId() + '<br>';	
			str = str + 'vpickzone. ' + vpickzone + '<br>';	
			str = str + 'pickRuleId. ' + pickRuleId + '<br>';	
			str = str + 'pickMethodId. ' + pickMethodId + '<br>';
			str = str + 'pickBinlocGroup. ' + pickBinlocGroup + '<br>';
			str = str + 'vBinloction. ' + vBinloction + '<br>';
			str = str + 'pickmethodpickface. ' + pickmethodpickface + '<br>';
			str = str + 'pfBinLocationgroupId. ' + pfBinLocationgroupId + '<br>';

			nlapiLogExecution('DEBUG', 'Pick strategy parameters', str);

			vPickZoneArr.push(vpickzone);
			//}	
			//}

			var vLocGrpArr=new Array();
			if(vPickZoneArr != null && vPickZoneArr != '')
			{
				nlapiLogExecution('Debug','Time stamp 104',TimeStampinSec());
				if(pickBinlocGroup ==null || pickBinlocGroup =='')
					vLocGrpArr=GetAllLocGroups(vPickZoneArr,location,-1);
				else 
					vLocGrpArr.push(pickBinlocGroup);
				nlapiLogExecution('Debug','Time stamp 104',TimeStampinSec());

			}
			else if(pickBinlocGroup !=null && pickBinlocGroup !='')
				vLocGrpArr.push(pickBinlocGroup);	


			var searchresultsinvt;
			var InvtresultsFlag ='F';
			if((vLocGrpArr != null && vLocGrpArr != '' && vLocGrpArr.length>0) ||(vBinloction != null && vBinloction != ''))
			{
				searchresultsinvt=GetAllInventory(item,vLocGrpArr,location,serialNo,vStatusArr,vItemType,batchflag,vBinloction,pfBinLocationgroupId);
				nlapiLogExecution('Debug','Time stamp 105',TimeStampinSec());

			}
			if(searchresultsinvt != null && searchresultsinvt != '')
			{
				/*** The below code is merged from Endochoice account on 07Mar13 by Santosh as part of Standard bundle***/

				//Updating invt flag to'T' ,if invt available in multiple bin location || if any pick strategies are exists
				if((searchresultsinvt != null && searchresultsinvt != '' && searchresultsinvt.length>1)|| (s < parseFloat(vPickRules.length)))
					InvtresultsFlag ='T';

				//consider first Pickface location to fetch the Inventory for the Item
				if(pickmethodpickface == 'T')
				{
					if (pfLocationResults != null && pfLocationResults.length > 0)
					{
						var remainingQuantity=avlqty;
						//

						nlapiLogExecution('ERROR', 'Within PF Location Result search');
						nlapiLogExecution('ERROR', 'remainingQuantity', remainingQuantity);
						nlapiLogExecution('ERROR', 'pfLocationResults.length', pfLocationResults.length);

						for(var z = 0; z < pfLocationResults.length; z++)
						{
							if(parseFloat(remainingQuantity) > 0)
							{
								var pfItem = pfLocationResults[z].getValue('custrecord_pickfacesku');
								var Autoreplenflag=pfLocationResults[z].getValue('custrecord_autoreplen');

								if (pfItem == item)
								{
									nlapiLogExecution('ERROR', 'item Match',pfItem);
									pfBinLocationId = pfLocationResults[z].getValue('custrecord_pickbinloc');
									var pfBinLocationText = pfLocationResults[z].getText('custrecord_pickbinloc');
									pfMaximumQuantity = pfLocationResults[z].getValue('custrecord_maxqty');
									pfMaximumPickQuantity = pfLocationResults[z].getValue('custrecord_maxpickqty');
									pffixedlp = pfLocationResults[z].getValue('custrecord_pickface_ebiz_lpno');
									pfstatus=pfLocationResults[z].getValue('custrecord_pickface_itemstatus');
									replenrule=pfLocationResults[z].getValue('custrecord_pickruleid');
									var pfBinLocationGroup = pfLocationResults[z].getValue('custrecord_inboundlocgroupid', 'custrecord_pickbinloc');
									var pfzone=pfLocationResults[z].getValue('custrecord_pickzone');

									var str = 'pfBinLocationId. = ' + pfBinLocationId + '<br>';
									str = str + 'pfMaximumQuantity. = ' + pfMaximumQuantity + '<br>';	
									str = str + 'pfMaximumPickQuantity. = ' + pfMaximumPickQuantity + '<br>';
									str = str + 'pffixedlp. = ' + pffixedlp + '<br>';
									str = str + 'pfstatus. = ' + pfstatus + '<br>';
									str = str + 'replenrule. = ' + replenrule + '<br>';
									str = str + 'remainingQuantity. = ' + remainingQuantity + '<br>';
									str = str + 'pfBinLocationGroup. = ' + pfBinLocationGroup + '<br>';
									str = str + 'pfzone. = ' + pfzone + '<br>';

									nlapiLogExecution('ERROR', 'Pickface Details', str);

									PfLocArray.push(pfBinLocationId);

									if(parseFloat(pfMaximumPickQuantity) >= parseFloat(remainingQuantity))
									{

										var availableQty ="";
										var allocQty = "";
										var recordId = "";

										var itemStatus = "";
										var fromLP = "";
										var inventoryItem="";
										var fromLot="";
										// Get inventory details for this pickface location
										nlapiLogExecution('Debug','Time stamp 106',TimeStampinSec());
										var binLocnInvDtls = getAvailQtyFromInventory(searchresultsinvt, pfBinLocationId, allocatedInv,location,pfItem);
										nlapiLogExecution('Debug','Time stamp 107',TimeStampinSec());
										var fromLotId;
										if(binLocnInvDtls!=null && binLocnInvDtls.length>0)
										{
											nlapiLogExecution('Debug','binLocnInvDtls',binLocnInvDtls.length);

											for(var bincount=0;bincount<binLocnInvDtls.length;bincount++)
											{
												if(parseFloat(remainingQuantity) > 0)
												{
													availableQty = binLocnInvDtls[bincount][2];
													allocQty = binLocnInvDtls[bincount][3];
													recordId = binLocnInvDtls[bincount][8];
													if(binLocnInvDtls[bincount][4]!=null && binLocnInvDtls[bincount][4]!='')
														packCode = binLocnInvDtls[bincount][4];
													itemStatus = binLocnInvDtls[bincount][5];										
													whLocation = binLocnInvDtls[bincount][6];
													fromLP = binLocnInvDtls[bincount][7];											
													inventoryItem = binLocnInvDtls[bincount][9];
													fromLot = binLocnInvDtls[bincount][10];
													expiryDate = binLocnInvDtls[bincount][11];
													fromLotId = binLocnInvDtls[bincount][12];
													nlapiLogExecution('ERROR', 'binLocnInvDtls[0][10] For Batch',binLocnInvDtls[bincount][10]);
//													}
													/*
													 * If the allocation strategy is not null check in inventory for that item.						 
													 * 		if the allocation strategy = min quantity on the pallet
													 * 			search for the inventory where the inventory has the least quantity
													 * 		else
													 * 			search for the inventory where the inventory has the maximum quantity 
													 * 			on the pallet 
													 */

													var str = 'Inventory Item Status. = ' + itemStatus + '<br>';
													str = str + 'availableQty. = ' + availableQty + '<br>';	

													nlapiLogExecution('ERROR', 'Item Status', str);

													//if(parseFloat(availableQty) > 0 && itemStatus== pfstatus)
													if(parseFloat(availableQty) > 0)
													{
														nlapiLogExecution('ERROR', 'remainingQuantity', remainingQuantity);
														// allocate to this bin location
														var actualAllocQty = Math.min(parseFloat(availableQty), parseFloat(remainingQuantity));
														var newAllocQty = parseFloat(allocQty) + parseFloat(actualAllocQty);
														remainingQuantity = parseFloat(remainingQuantity) - parseFloat(actualAllocQty);
														vRemainingQty=remainingQuantity;
														//nlapiLogExecution('ERROR', 'actualAllocQty', actualAllocQty);
														//nlapiLogExecution('ERROR', 'remainingQuantity', remainingQuantity);
														var expectedQuantity = parseFloat(actualAllocQty);

														var contsize="";								
														//var totalweight = parseFloat((expectedQuantity*uomweight))/parseFloat(uomqty);
														//var totalcube = parseFloat((expectedQuantity*uomcube))/parseFloat(uomqty);

														// Allocate the quantity to the pickface location if the allocation quantity is 
														// 	less than or equal to the pick face maximum quantity
														//nlapiLogExecution('ERROR', 'actualAllocQty', actualAllocQty);
														//nlapiLogExecution('ERROR', 'pfMaximumPickQuantity', pfMaximumPickQuantity);

														var str = 'pfMaximumPickQuantity. = ' + pfMaximumPickQuantity + '<br>';
														str = str + 'actualAllocQty. = ' + actualAllocQty + '<br>';	
														str = str + 'remainingQuantity. = ' + remainingQuantity + '<br>';	

														nlapiLogExecution('ERROR', 'Qty Details Status', str);

														if(actualAllocQty <= pfMaximumPickQuantity)
														{
															//1-Ship Cartons	2-Build Cartons		3-Ship Pallets

															var allocatedInvRow = [pfBinLocationId, expectedQuantity,fromLP,inventoryItem,fromLot];
															allocatedInv.push(allocatedInvRow);
															//PfLocArray.push(pfBinLocationId);
														}
														if (parseFloat(actualAllocQty) > 0) {
															////alert('AfterIf ' +cnfmqty);
															//Resultarray

															nlapiLogExecution('Debug','Time stamp 108',TimeStampinSec());

															if(pfzone==null || pfzone=='')
															{

																var filtersLocGrp = new Array();
																filtersLocGrp.push(new nlobjSearchFilter('custrecord_locgroup_no', null, 'anyof', pfBinLocationGroup));
																var columnsLocGrp = new Array();
																columnsLocGrp[0] = new nlobjSearchColumn('custrecordcustrecord_putzoneid');
																var SearchLocGrp = nlapiSearchRecord('customrecord_zone_locgroup', null, filtersLocGrp, columnsLocGrp);
																nlapiLogExecution('ERROR', 'vpickzone b', vpickzone);
																if(SearchLocGrp != null && SearchLocGrp != '')
																{
																	pfzone = SearchLocGrp[0].getValue('custrecordcustrecord_putzoneid');
																}
															}

															nlapiLogExecution('ERROR', 'pfzone a', pfzone);


															var invtarray = new Array();
															////alert('cnfmqty3 ' +cnfmqty);
															invtarray[0]= actualAllocQty;
															////alert('cnfmqty4 ' +cnfmqty);
															invtarray[1]= pfBinLocationId;
															invtarray[2] = fromLP;
															invtarray[3] = recordId;
															invtarray[4] = fromLot;
															invtarray[5] =availableQty;
															invtarray[6] =pfBinLocationText;
															invtarray[7] =fromLotId;
															invtarray[8] =pfzone;
															invtarray[9] =pickRuleId;
															invtarray[10] =pickMethodId;
															invtarray[11] =InvtresultsFlag;
															//alert("cnfmqty "+cnfmqty);
															//alert("cnfmqty " + cnfmqty);
															//alert(vlotno);
															//alert(vlotText);
															nlapiLogExecution('ERROR', 'RecId:',Recid);
															nlapiLogExecution('Debug','Time stamp 109',TimeStampinSec());
															//invtarray[3] = vpackcode;
															//invtarray[4] = vskustatus;
															//invtarray[5] = vuomid;
															Resultarray.push(invtarray);
															if (parseFloat(remainingQuantity) == 0) {
																nlapiLogExecution('ERROR', 'remainingQuantity in jump statement:',remainingQuantity);
															}
														}
													}
													nlapiLogExecution('ERROR','remainingQuantity',remainingQuantity);
												}
											}
											if (parseFloat(remainingQuantity) <= 0)
												return Resultarray;
										}
									}
								}
							}
						}
					}
				}
				/*** Up to here ***/
				LABL2: for (var l = 0; l < searchresultsinvt.length; l++) 
				{

					var searchresult = searchresultsinvt[l];
					var actqty = searchresult.getValue('custrecord_ebiz_qoh');
					var allocqty = searchresult.getValue('custrecord_ebiz_alloc_qty');
					var LP = searchresult.getValue('custrecord_ebiz_inv_lp');
					var vactLocation = searchresult.getValue('custrecord_ebiz_inv_binloc');
					var vactLocationtext = searchresult.getText('custrecord_ebiz_inv_binloc');
					var vlotno = searchresult.getValue('custrecord_ebiz_inv_lot');
					var vlotText = searchresult.getText('custrecord_ebiz_inv_lot');
					var Recid = searchresult.getId(); 

					var alreadyAllocQty = getAlreadyAllocatedInv(allocatedInv, vactLocation,LP,item,vlotText);

					nlapiLogExecution('ERROR', 'alreadyAllocQty', alreadyAllocQty);
					nlapiLogExecution('ERROR', 'actqty', actqty);

					actqty = parseInt(actqty) - parseInt(alreadyAllocQty);

					nlapiLogExecution('ERROR', 'actqty', actqty);

					/*** The below code is merged from Endochoice account on 07Mar13 by Santosh as part of Standard bundle***/
					//to fetch remaining Invt from bulk Location
					if(remainingQuantity != null && remainingQuantity != '' && parseFloat(remainingQuantity)>0)
					{
						avlqty=remainingQuantity;
						vRemainingQty=remainingQuantity;
					}
					/*** up to here ***/
					//alert(" 664 actqty:" +actqty);
					//alert(" 664 allocqty:" +allocqty);
					//alert(" 664 LP:" +LP);
					//alert(" 664 vactLocation:" +vactLocation);
					//alert(" 664 vactLocationtext:" +vactLocationtext);
					//alert(" 664 vlotno:" +vlotno);
					//alert(" 664 vlotText:" +vlotText);
					//alert(" 664 Recid:" +Recid);
					/*** The below code is merged from Endochoice account on 07Mar13 by Santosh as part of Standard bundle***/
					//to restrict pickface Location for the Item
					var vBulkLocProceed='T';
					if(PfLocArray.length>0)
					{
						if(PfLocArray.indexOf(vactLocation)!=-1)
							vBulkLocProceed='F';
					}
					/*** up to here ***/

					nlapiLogExecution('ERROR', 'Recid1', Recid);
					if(vBulkLocProceed=='T')
					{
						if (isNaN(allocqty)) {
							allocqty = 0;
						}
						if (allocqty == "") {
							allocqty = 0;
						}
						if( parseFloat(allocqty)<0)
						{
							allocqty=0;
						}
						if( parseFloat(actqty)<0)
						{
							actqty=0;
						}

						var str = 'vactLocation. = ' + vactLocation + '<br>';
						str = str + 'vactLocationtext. = ' + vactLocationtext + '<br>';	
						str = str + 'allocqty. = ' + allocqty + '<br>';
						str = str + 'LP. = ' + LP + '<br>';
						str = str + 'vlotText. = ' + vlotText + '<br>';
						str = str + 'allocqty. = ' + allocqty + '<br>';
						str = str + 'actqty. = ' + actqty + '<br>';
						str = str + 'vpickzone. = ' + vpickzone + '<br>';

						nlapiLogExecution('ERROR', 'Bulk Loc Details', str);


						var remainqty=0;
						//Added by Ganesh not to allow negative or zero Qtys 
						if(parseFloat(actqty) >=0 && parseFloat(allocqty)>=0 && (parseFloat(actqty)-parseFloat(allocqty))>0)
						{
							remainqty = parseFloat(actqty) - parseFloat(allocqty);
							vRemainingQty=remainqty;
						}
						var cnfmqty;
						nlapiLogExecution('ERROR', "vRemainingQty: ",vRemainingQty );
						//nlapiLogExecution('ERROR', "remainqty: ",remainqty );
////						alert("remainqty"+remainqty );
////						alert("avlqty"+avlqty );
						if (parseFloat(remainqty) > 0) {
							////alert("parseFloat(avlqty) "+parseFloat(avlqty));
							if ((parseFloat(avlqty) - parseFloat(actallocqty)) <= parseFloat(remainqty)) {
								cnfmqty = parseFloat(avlqty) - parseFloat(actallocqty);
								actallocqty = avlqty;
								////alert('cnfmqty in if '+cnfmqty);
							}
							else {
								cnfmqty = remainqty;
								////alert('cnfmqty2 ' +cnfmqty);
								actallocqty = parseFloat(actallocqty) + parseFloat(remainqty);
							}
							//alert(" 708 cnfmqty:" +cnfmqty);
							//alert(" 709 actallocqty:" +actallocqty);
							////alert('BeforeIf ' +cnfmqty);
							if (parseFloat(cnfmqty) > 0) {
								//checking the invt record for multiple allocations for same item

								nlapiLogExecution('ERROR', "vInrefArray: " , vInrefArray);
								if(vInrefArray.indexOf(Recid)!=-1)
								{
									nlapiLogExecution('ERROR', "Recid exists " , Recid);
									for(var cnt=0;cnt<vInrefArray.length;cnt++)
									{
										var PrevAllocQty= vQtyArray[cnt];
										nlapiLogExecution('ERROR', "into if : " , PrevAllocQty);
										nlapiLogExecution('ERROR', "cnfmqty " , cnfmqty);
										var vTotalqty =parseFloat(PrevAllocQty) + parseFloat(cnfmqty);
										nlapiLogExecution('ERROR', "vTotalqty " , vTotalqty);
										if(parseFloat(vTotalqty)>parseFloat(remainqty))
										{
											actallocqty=0;
											continue LABL2;
										}
										else
										{

											vQtyArray[cnt]=vTotalqty;
											nlapiLogExecution('ERROR', "vQtyArray[cnt] " , vQtyArray[cnt]);
										}

									}
								}
								else
								{
									nlapiLogExecution('ERROR', "Recid  not exists " , Recid);
									vInrefArray.push(Recid);
									vQtyArray.push(cnfmqty);
								}


								////alert('AfterIf ' +cnfmqty);
								//Resultarray
								var invtarray = new Array();
								////alert('cnfmqty3 ' +cnfmqty);
								invtarray[0]= cnfmqty;
								////alert('cnfmqty4 ' +cnfmqty);
								invtarray[1]= vactLocation;
								invtarray[2] = LP;
								invtarray[3] = Recid;
								invtarray[4] = vlotText;
								invtarray[5] =remainqty;
								invtarray[6] =vactLocationtext;
								invtarray[7] =vlotno;
								invtarray[8] =vpickzone;
								invtarray[9] =pickRuleId;
								invtarray[10] =pickMethodId;
								invtarray[11] =InvtresultsFlag;
								//alert("cnfmqty "+cnfmqty);
								//alert("cnfmqty " + cnfmqty);
								//alert(vlotno);
								//alert(vlotText);
								nlapiLogExecution('ERROR', 'RecId:',Recid);
								//invtarray[3] = vpackcode;
								//invtarray[4] = vskustatus;
								//invtarray[5] = vuomid;
								Resultarray.push(invtarray);								
							}
						}
						if ((avlqty - actallocqty) == 0) {
							return Resultarray;
						}
					} 
				}
			}
		}
	}
	nlapiLogExecution('Debug','Time stamp 110',TimeStampinSec());
	return Resultarray;  
}

var vAllGrps=new Array();
function GetAllLocGroups(vpickzone,location,maxno){

	nlapiLogExecution('Debug','Into GetAllLocGroups',TimeStampinSec());

	var str = 'vpickzone.' + vpickzone + '<br>';
	str = str + 'location.' + location + '<br>';	
	str = str + 'maxno. ' + maxno + '<br>';	

	nlapiLogExecution('DEBUG', 'GetAllLocGroups Parameters', str);

	var vpickzoneList;
	if(vpickzone!= null && vpickzone !='')
	{
		for(var ct=0;ct<vpickzone.length;ct++)
		{
			if(ct==0)
				vpickzoneList=vpickzone[ct];
			else
				vpickzoneList=vpickzoneList+","+vpickzone[ct];
		}
	}
	nlapiLogExecution('ERROR','vpickzoneList',vpickzoneList);
	var filterszone = new Array();
	if(vpickzoneList!= null && vpickzoneList !='')
		filterszone.push(new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'anyof', vpickzoneList));
	else
		filterszone.push(new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'anyof', ['@NONE@']));

	filterszone.push(new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', ['@NONE@',location]));
	if(maxno!=-1)
	{
		filterszone.push(new nlobjSearchFilter('internalid', null, 'lessthan', parseFloat(maxno)));
	}

	var columnzone = new Array();
	columnzone[0] = new nlobjSearchColumn('custrecord_locgroup_no');
	columnzone[1]=new nlobjSearchColumn('internalid');
	columnzone[1].setSort(true);

	var searchresultszone = nlapiSearchRecord('customrecord_zone_locgroup', null, filterszone, columnzone);
	nlapiLogExecution('ERROR','Loc Group Fetching');
	if(searchresultszone != null && searchresultszone != '')
	{	
		for(var k=0;k<searchresultszone.length;k++)
		{
			vAllGrps.push(searchresultszone[k].getValue('custrecord_locgroup_no'));
		}
		if(searchresultszone!=null && searchresultszone.length>=1000)
		{ 
			var maxno=searchresultszone[searchresultszone.length-1].getValue('internalid');		
			GetAllLocGroups(vpickzone,location,maxno); 
		}
	}

	nlapiLogExecution('Debug','Out of GetAllLocGroups',TimeStampinSec());
	return vAllGrps;
}


var vAllInv=new Array();
function GetAllInventory(item,vlocgroupno,location,serialNo,vStatusArr,vItemType,batchflag,binlocation,
		pfBinLocationgroupId){

	nlapiLogExecution('Debug','Into GetAllInventory',TimeStampinSec());

	var filtersinvt = new Array();

	filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', item));
	filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['3','19']));

	if((vlocgroupno != null && vlocgroupno != '') && (pfBinLocationgroupId!=null && pfBinLocationgroupId !=''))
		filtersinvt.push(new nlobjSearchFilter('custrecord_outboundinvlocgroupid',null, 'anyof', vlocgroupno,pfBinLocationgroupId));
	//filtersinvt.push(new nlobjSearchFilter('custrecord_outboundlocgroupid','custrecord_ebiz_inv_binloc', 'anyof', vlocgroupno,pfBinLocationgroupId));

	if((vlocgroupno != null && vlocgroupno != '') && (pfBinLocationgroupId ==null || pfBinLocationgroupId ==''))
		filtersinvt.push(new nlobjSearchFilter('custrecord_outboundinvlocgroupid',null, 'anyof', vlocgroupno));
	//filtersinvt.push(new nlobjSearchFilter('custrecord_outboundlocgroupid','custrecord_ebiz_inv_binloc', 'anyof', vlocgroupno));

	if(binlocation != null && binlocation != '')
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc',null, 'anyof', binlocation));
	if (location!= "" && location!= null) {
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [location]));
	}

	//code added by santosh on 13aug12
	if (serialNo!= "" && serialNo!= null) {
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', serialNo));
	}

	if (vStatusArr!= "" && vStatusArr!= null) {
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'anyof', vStatusArr));
	}

	//end of the code on 13Aug12
	if (vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem" || batchflag=="T" )
	{
		var expdate=DateStamp();
		filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_expdate', null, 'after', expdate));
		//end of the code on 13Aug12

	}
	filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_avl_qty', null, 'greaterthan', 0));
	nlapiLogExecution('ERROR', "custrecord_ebiz_inv_sku:  :  vlocgroupno  : binlocation  :   serialNo " + item + ' : ' + vlocgroupno + ' : ' + binlocation + ' : ' + serialNo );


	var columnsinvt = new Array();
	columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_expdate');
	columnsinvt[1]=new nlobjSearchColumn('internalid');
	columnsinvt[0].setSort();
	columnsinvt[2] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columnsinvt[3] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
	columnsinvt[4] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columnsinvt[5] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columnsinvt[6] = new nlobjSearchColumn('custrecord_ebiz_inv_fifo');
	columnsinvt[7] = new nlobjSearchColumn('custrecord_pickseqno'); 
	columnsinvt[8] = new nlobjSearchColumn('custrecord_ebiz_qoh');			
	columnsinvt[9] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');
	/*** The below code is merged from Endochoice account on 07Mar13 by Santosh as part of Standard bundle***/
	columnsinvt[10] = new nlobjSearchColumn('custrecord_ebiz_inv_loc');
	columnsinvt[11] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columnsinvt[12] = new nlobjSearchColumn('custrecord_ebiz_avl_qty');
	columnsinvt[13] = new nlobjSearchColumn('custrecord_ebiz_inv_sku_status');
	/*** up to here ***/	
	columnsinvt[6].setSort();
	columnsinvt[7].setSort();
	columnsinvt[8].setSort(false);

	var searchresultsinvt = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);

	nlapiLogExecution('Debug','Out of GetAllInventory',TimeStampinSec());

	return searchresultsinvt;
}


function GetLotno(Lotno,lineItemvalue,m,form)
{
	if(Lotno != "" && Lotno != null)
	{
		nlapiLogExecution('ERROR', 'GetLotno', Lotno);	
		var filterbatch = new Array();
		filterbatch[0] = new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', Lotno);
		filterbatch[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', lineItemvalue);
		var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterbatch);
		if(receiptsearchresults!=null)
		{
			var lotid= receiptsearchresults[0].getId();
			nlapiLogExecution('ERROR', 'lotid', lotid);
			//serialno.setDefaultValue(lotid);
			form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_lotno', m, lotid);
			form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_hiddenlotno', m, lotid);
		}

	}	

}

/*** The below code is merged from Endochoice account on 07Mar13 by Santosh as part of Standard bundle***/

function getAvailQtyFromInventory(inventorySearchResults, binLocation, allocatedInv,dowhlocation,fulfilmentItem){

	nlapiLogExecution('ERROR','Into getAvailQtyFromInventory',TimeStampinSec());

	var str = 'inventorySearchResults.' + inventorySearchResults + '<br>';
	str = str + 'binLocation.' + binLocation + '<br>';	
	str = str + 'allocatedInv. ' + allocatedInv + '<br>';	
	str = str + 'dowhlocation. ' + dowhlocation + '<br>';	
	str = str + 'fulfilmentItem. ' + fulfilmentItem + '<br>';	

	nlapiLogExecution('DEBUG', 'getAvailQtyFromInventory Parameters', str);

	var binLocnInvDtls = new Array();
	var matchFound = false;

	if(inventorySearchResults != null && inventorySearchResults.length > 0){
		nlapiLogExecution('ERROR','inventorySearchResults length',inventorySearchResults.length);
		for(var i = 0; i < inventorySearchResults.length; i++){
			if(!matchFound){
				invBinLocnId = inventorySearchResults[i].getValue('custrecord_ebiz_inv_binloc');
				var InvWHLocation = inventorySearchResults[i].getValue('custrecord_ebiz_inv_loc');
				var invItem=inventorySearchResults[i].getValue('custrecord_ebiz_inv_sku');

				var str = 'dowhlocation. = ' + dowhlocation + '<br>';
				str = str + 'InvWHLocation. = ' + InvWHLocation + '<br>';	
				str = str + 'invBinLocnId. = ' + invBinLocnId + '<br>';
				str = str + 'binLocation. = ' + binLocation + '<br>';
				str = str + 'fulfilmentItem. = ' + fulfilmentItem + '<br>';
				str = str + 'invItem. = ' + invItem + '<br>';

				nlapiLogExecution('ERROR', 'Inventory & FO Details', str);

				if((dowhlocation==InvWHLocation)&&(parseFloat(invBinLocnId) == parseFloat(binLocation)) && fulfilmentItem==invItem){

					var actualQty = inventorySearchResults[i].getValue('custrecord_ebiz_qoh');
					var inventoryAvailableQuantity = inventorySearchResults[i].getValue('custrecord_ebiz_avl_qty');
					var allocationQuantity = inventorySearchResults[i].getValue('custrecord_ebiz_alloc_qty');
					var packCode = inventorySearchResults[i].getValue('custrecord_ebiz_inv_packcode');
					var itemStatus = inventorySearchResults[i].getValue('custrecord_ebiz_inv_sku_status');
					var whLocation = inventorySearchResults[i].getValue('custrecord_ebiz_inv_loc');
					var fromLP = inventorySearchResults[i].getValue('custrecord_ebiz_inv_lp');
					var inventoryItem=inventorySearchResults[i].getValue('custrecord_ebiz_inv_sku');
					var fromLot = inventorySearchResults[i].getText('custrecord_ebiz_inv_lot');
					var fromLotId = inventorySearchResults[i].getValue('custrecord_ebiz_inv_lot');
					var expiryDate = inventorySearchResults[i].getValue('custrecord_ebiz_expdate');
					var str = 'Invt QOH. = ' + actualQty + '<br>';
					str = str + 'Alloc Qty. = ' + allocationQuantity + '<br>';	
					str = str + 'Avail Qty. = ' + inventoryAvailableQuantity + '<br>';
					str = str + 'packCode. = ' + packCode + '<br>';
					str = str + 'itemStatus. = ' + itemStatus + '<br>';
					str = str + 'whLocation. = ' + whLocation + '<br>';
					str = str + 'fromLP. = ' + fromLP + '<br>';
					str = str + 'inventoryItem. = ' + inventoryItem + '<br>';
					str = str + 'fromLot. = ' + fromLot + '<br>';

					nlapiLogExecution('ERROR', 'Inventory Details', str);

					var recordId = inventorySearchResults[i].getId();

					// Get allocation already done
					var alreadyAllocQty = getAlreadyAllocatedInv(allocatedInv, binLocation,fromLP,inventoryItem,fromLot);

					nlapiLogExecution('ERROR', 'alreadyAllocQty', alreadyAllocQty);

					// Adjusting for allocations already done
					inventoryAvailableQuantity = parseFloat(inventoryAvailableQuantity) - parseFloat(alreadyAllocQty);

					nlapiLogExecution('ERROR', 'inventoryAvailableQuantity', inventoryAvailableQuantity);

					var currentRow = [i, actualQty, inventoryAvailableQuantity, allocationQuantity, packCode,
					                  itemStatus, whLocation, fromLP, recordId,inventoryItem,fromLot,expiryDate,fromLotId];

					binLocnInvDtls.push(currentRow);

					nlapiLogExecution('ERROR', 'inventoryAvailable_currentRow', currentRow);
					//matchFound = true;
					matchFound = false;
				}
			}
		}
	}

	nlapiLogExecution('ERROR','Out of getAvailQtyFromInventory',TimeStampinSec());
	return binLocnInvDtls;
}

function getPFLocationsForOrder(whlocation,skuList){
	var pfLocationResults = new Array();
	pfLocationResults = getSKUQtyInPickfaceLocns(whlocation,skuList);
	return pfLocationResults;
}

/**
 * Function to return the list of SKU quantity for all SKUs in all pickface locations
 * NOTE: WILL NEVER RETURN NULL; WILL ATLEAST RETURN AN EMPTY ARRAY
 * 
 * @returns {Array}
 */
function getSKUQtyInPickfaceLocns(whlocation,skulist){
	/*
	 * This function should return the pickface locn, min qty, max qty, qoh for all
	 * SKUs in all pickface locations
	 */

	nlapiLogExecution('ERROR', 'Into  getSKUQtyInPickfaceLocns',TimeStampinSec());
	nlapiLogExecution('ERROR', 'whlocation',whlocation);
	nlapiLogExecution('ERROR', 'skulist',skulist);
	var pfLocnResults = new Array();

	// Search for all active records with a valid replen rule id
	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	//filters.push(new nlobjSearchFilter('custrecord_pickruleid', null, 'noneof', ['@NONE@']));

	if(whlocation!=null && whlocation!='')
		filters.push(new nlobjSearchFilter('custrecord_pickface_location', null, 'anyof', ['@NONE@',whlocation]));

	if(skulist!=null && skulist!='')
		filters.push(new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', skulist));

	var columns = new Array();    
	columns[0] = new nlobjSearchColumn('custrecord_pickfacesku');
	columns[1] = new nlobjSearchColumn('custrecord_replenqty');
	columns[2] = new nlobjSearchColumn('custrecord_maxqty');
	columns[3] = new nlobjSearchColumn('custrecord_pickbinloc');
	columns[4] = new nlobjSearchColumn('custrecord_pickruleid');
	columns[5] = new nlobjSearchColumn('custrecord_minqty');    
	columns[6] = new nlobjSearchColumn('custrecord_pickface_location');
	columns[7] = new nlobjSearchColumn('custrecord_pickface_ebiz_lpno');
	columns[8] = new nlobjSearchColumn('custrecord_maxpickqty');
	columns[9] = new nlobjSearchColumn('custrecord_pickface_itemstatus');
	columns[10] = new nlobjSearchColumn('custrecord_pickface_location');
	columns[11] = new nlobjSearchColumn('custrecord_roundqty');
	columns[12] = new nlobjSearchColumn('custrecord_pickface_location');	
	columns[13] = new nlobjSearchColumn('custrecord_inboundlocgroupid', 'custrecord_pickbinloc');
	columns[14] = new nlobjSearchColumn('custrecord_pickzone');
	columns[15] = new nlobjSearchColumn('custrecord_autoreplen');


	// sort by pick face SKU
	columns[0].setSort();

	// Search for all active
	pfLocnResults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, filters, columns);    

	nlapiLogExecution('ERROR', 'Out of  getSKUQtyInPickfaceLocns',TimeStampinSec());

	return pfLocnResults;
}
function getAlreadyAllocatedInv(allocatedInv, binLocation,fromLP,item,lot){
	nlapiLogExecution('ERROR', 'Into getAlreadyAllocatedInv',TimeStampinSec());
	var alreadyAllocQty = 0;
	if(allocatedInv != null && allocatedInv.length > 0){
		for(var i = 0; i < allocatedInv.length; i++){
//			nlapiLogExecution('ERROR', 'fromLP',fromLP);
//			nlapiLogExecution('ERROR', 'allocatedInv[i][2]',allocatedInv[i][2]);
//			nlapiLogExecution('ERROR', 'binLocation',binLocation);
//			nlapiLogExecution('ERROR', 'allocatedInv[i][0]',allocatedInv[i][0]);
//			nlapiLogExecution('ERROR', 'item',item);
//			nlapiLogExecution('ERROR', 'allocatedInv[i][3]',allocatedInv[i][3]);
//			nlapiLogExecution('ERROR', 'lot',lot);
//			nlapiLogExecution('ERROR', 'allocatedInv[i][4]',allocatedInv[i][4]);

			if(fromLP == allocatedInv[i][2] && binLocation==allocatedInv[i][0] && item==allocatedInv[i][3] && lot==allocatedInv[i][4])
				alreadyAllocQty = parseFloat(alreadyAllocQty) + parseFloat(allocatedInv[i][1]);
		}
	}
	nlapiLogExecution('ERROR', 'Out of getAlreadyAllocatedInv',TimeStampinSec());
	return alreadyAllocQty;
}
/*** up to here ***/


function ComponentItemsReversal(InvtRecordArray,OpentaskInternalId) {

	try{

		nlapiLogExecution('ERROR', 'component Reversal','done');
		nlapiLogExecution('Debug','Time stamp 1',TimeStampinSec());

		//decrement 
		if (InvtRecordArray !=null && InvtRecordArray !='' && InvtRecordArray.length>0) {

			nlapiLogExecution('ERROR', 'InvtRecordArray length',InvtRecordArray.length);

			for(var k=0;k < InvtRecordArray.length; k++) {

				var vInvtRecordId =	InvtRecordArray[k][0];
				var vLineQty =	InvtRecordArray[k][1];
				nlapiLogExecution('Debug','vInvtRecordId',vInvtRecordId);

				var scount=1;
				LABL1: for(var z=0;z<scount;z++)
				{
					try
					{
						var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', vInvtRecordId);
						var qty = transaction.getFieldValue('custrecord_ebiz_qoh');
						var allocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
						var vLP=transaction.getFieldValue('custrecord_ebiz_inv_lp');
						var vPackcode=transaction.getFieldValue('custrecord_ebiz_inv_packcode');
						var vSKUStatus=transaction.getFieldValue('custrecord_ebiz_inv_sku_status');
						var vAvailableQty=transaction.getFieldValue('custrecord_ebiz_avl_qty');

						nlapiLogExecution('ERROR', 'qty', qty);
						nlapiLogExecution('ERROR', 'allocqty', allocqty);
						nlapiLogExecution('ERROR', 'vAvailableQty', vAvailableQty);
						if(allocqty == null || allocqty == '')
							allocqty=0;
						nlapiLogExecution('ERROR', 'vLineQty', vLineQty);
						var newAllocqty = parseFloat(allocqty)-parseFloat(vLineQty);

						if(newAllocqty<0)
							newAllocqty=0;
						transaction.setFieldValue('custrecord_ebiz_alloc_qty',parseFloat(newAllocqty));
						transaction.setFieldValue('custrecord_ebiz_displayfield', 'N');			 
						transaction.setFieldValue('custrecord_ebiz_callinv', 'N');
						nlapiSubmitRecord(transaction, false, true);
						nlapiLogExecution('ERROR', 'Inventory record updated',vInvtRecordId);
					}
					catch(ex)
					{
						nlapiLogExecution('ERROR', 'Exception in ComponentItemsReversal',ex);

						var exCode='CUSTOM_RECORD_COLLISION'; 
						var wmsE='Inventory record being updated by another user. Please try again...';
						if (ex instanceof nlobjError) 
						{	
							wmsE=ex.getCode() + '\n' + ex.getDetails();
							exCode=ex.getCode();
						}
						else
						{
							wmsE=ex.toString();
							exCode=ex.toString();
						}  

						nlapiLogExecution('ERROR', 'Exception in ComponentItemsReversal : ', ex);

						if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
						{ 
							scount=scount+1;
							continue LABL1;
						}
						else break LABL1;

					}
				}

			}
		}


		if(OpentaskInternalId !=null && OpentaskInternalId !='' && OpentaskInternalId.length>0)
		{
			nlapiLogExecution('ERROR', 'OpentaskInternalId length',OpentaskInternalId.length);

			for (var l = 0; l < OpentaskInternalId.length; l++) {

				nlapiLogExecution('ERROR', 'Deleted Open taskId', OpentaskInternalId[l]);
				var id = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', OpentaskInternalId[l]);
				nlapiLogExecution('ERROR', 'delete', 'sucess');
			}

		}

	}
	catch(e)
	{

		nlapiLogExecution('ERROR', 'exception in ComponentItemsReversal',e);
	}

}





function ConcurrencyHandlingForInvt(varsku,vLineQty,vWMSLocation,vlineno,vComments,ItemInfoResults,pickrulesarray,vBatch,pfLocationResults,tempFlag,InvtRecordArray,OpentaskInternalId,vId,vWOName) {

	try
	{
		nlapiLogExecution('DEBUG', 'In to ConcurrencyHandlingForInvt', 'sucess');


		var now = new Date();
		//a Date object to be used for a random value
		var now = new Date();
		//now= now.getHours();
		//Getting time in hh:mm tt format.
		var a_p = "";
		var d = new Date();
		var curr_hour = now.getHours();
		if (curr_hour < 12) {
			a_p = "am";
		}
		else {
			a_p = "pm";
		}
		if (curr_hour == 0) {
			curr_hour = 12;
		}
		if (curr_hour > 12) {
			curr_hour = curr_hour - 12;
		}

		var curr_min = now.getMinutes();

		curr_min = curr_min + "";

		if (curr_min.length == 1) {
			curr_min = "0" + curr_min;
		}


		var currentContext = nlapiGetContext();  
		var currentUserID = currentContext.getUser();
		var str = 'varsku.' + varsku + '<br>';
		str = str + 'vLineQty.' + vLineQty + '<br>';	
		str = str + 'vWMSLocation. ' + vWMSLocation + '<br>';	
		str = str + 'ItemInfoResults. ' + ItemInfoResults + '<br>';	
		str = str + 'pickrulesarray. ' + pickrulesarray + '<br>';	
		str = str + 'pfLocationResults. ' + pfLocationResults + '<br>';
		str = str + 'vBatch. ' + vBatch + '<br>';
		str = str + 'tempFlag. ' + tempFlag + '<br>';
		str = str + 'InvtRecordArray. ' + InvtRecordArray + '<br>';
		str = str + 'OpentaskInternalId. ' + OpentaskInternalId + '<br>';
		str = str + 'vId. ' + vId + '<br>';
		str = str + 'vWOName. ' + vWOName + '<br>';

		nlapiLogExecution('DEBUG', 'SearchresultsArrar values1', str);

		var Feilds= ['custitem_item_family', 'custitem_item_group','recordType','custitem_ebizbatchlot'];
		var columns = nlapiLookupField('item', varsku, Feilds);
		var vItemType="";
		var batchflag="F";
		nlapiLogExecution('Debug','Time stamp 10',TimeStampinSec());
		if (columns != null && columns !='')
		{
			nlapiLogExecution('Debug','Time stamp 11',TimeStampinSec());
			var itemfamily = columns.custitem_item_family;
			var itemgroup = columns.custitem_item_group;
			vItemType = columns.recordType;
			batchflag= columns.custitem_ebizbatchlot;
		}

		// LABL3 is used for, if secound time also failed due to concurrency
		var vCount=1;
		LABL3: for(var m=0; m<vCount; m++)
		{
			var arryinvt=PickStrategieKittoStockNew(varsku,vLineQty,vWMSLocation,ItemInfoResults,pickrulesarray,vBatch,
					pfLocationResults,vItemType,batchflag);


			if(arryinvt !=null && arryinvt !='' && arryinvt.length>0)
			{
				for (var z=0; z<arryinvt.length; z++)
				{
					var invtarray= arryinvt[z]; 
					var vTempId = '';
					var vTempLineRec = invtarray[3];
					var vTempWOId = invtarray[2];
					var varTempsku = varsku;
					var vTempLP = invtarray[4];
					var vTempLineQty = invtarray[0];
					var vTempBatch = invtarray[4];
					var vTempactLocationtext = invtarray[6];
					var vTempLocation = invtarray[1];
					var vTempComments = vComments;
					var vTempLineno = vlineno;
					var vTempPickzone = invtarray[8];
					var vTempPickruleId = invtarray[9];
					var vTempPickmethod = invtarray[10];
					var vTempInvtResFlag = invtarray[11];


					var str = 'vTempId.' + vTempId + '<br>';
					str = str + 'vLineRec.' + vTempLineRec + '<br>';	
					str = str + 'varsku. ' + varTempsku + '<br>';	
					str = str + 'vTempBatch. ' + vTempBatch + '<br>';	
					str = str + 'vTempactLocationtext. ' + vTempactLocationtext + '<br>';	
					str = str + 'vTempComments. ' + vTempComments + '<br>';	
					str = str + 'vLineQty. ' + vTempLineQty + '<br>';	
					str = str + 'vTempLineno. ' + vTempLineno + '<br>';	
					str = str + 'vTempPickzone. ' + vTempPickzone + '<br>';	
					str = str + 'vTempPickruleId. ' + vTempPickruleId + '<br>';	
					str = str + 'vTempPickmethod. ' + vTempPickmethod + '<br>';	
					str = str + 'vTempInvtResFlag. ' + vTempInvtResFlag + '<br>';	


					nlapiLogExecution('DEBUG', 'SearchresultsArray values2', str);


					var vscount=1;
					LABL2: for(var t=0;t<vscount;t++)
					{

						try
						{
							var transaction1 = nlapiLoadRecord('customrecord_ebiznet_createinv', vTempLineRec);
							nlapiLogExecution('Debug','Time stamp 23',TimeStampinSec());
							var vTempQty = transaction1.getFieldValue('custrecord_ebiz_qoh');
							var vTempallocqty = transaction1.getFieldValue('custrecord_ebiz_alloc_qty');
							var vTempLP = transaction1.getFieldValue('custrecord_ebiz_inv_lp');
							var vTempPackcode = transaction1.getFieldValue('custrecord_ebiz_inv_packcode');
							var vTempSKUStatus = transaction1.getFieldValue('custrecord_ebiz_inv_sku_status');
							var vTempAvailableQty = transaction1.getFieldValue('custrecord_ebiz_avl_qty');

							nlapiLogExecution('ERROR', 'vTempQty', vTempQty);
							nlapiLogExecution('ERROR', 'vTempallocqty', vTempallocqty);
							nlapiLogExecution('ERROR', 'vTempAvailableQty', vTempAvailableQty);
							if(vTempallocqty == null || vTempallocqty == '')
								vTempallocqty=0;
							nlapiLogExecution('ERROR', 'vTempLineQty', vTempLineQty);
							if(parseFloat(vTempAvailableQty)>=parseFloat(vTempLineQty))
							{
								transaction1.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(vTempallocqty)+ parseFloat(vTempLineQty)).toFixed(5));
								transaction1.setFieldValue('custrecord_ebiz_displayfield', 'N');			 
								transaction1.setFieldValue('custrecord_ebiz_callinv', 'N');
								nlapiSubmitRecord(transaction1, false, true);

								//capture createinvt internal ids
								var currentArray = new Array(); 
								currentArray = [vTempLineRec,vTempLineQty];
								InvtRecordArray.push(currentArray);

								//create Open task records

								var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
								customrecord.setFieldValue('custrecord_tasktype', 3); // 5 for KTS,3 for PICK, 4 for SHIP 
								//customrecord.setFieldValue('custrecordact_begin_date', (parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
								customrecord.setFieldValue('custrecordact_begin_date', DateStamp());
								customrecord.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));


								nlapiLogExecution('ERROR','vTempactLocationtext',vTempactLocationtext);

								customrecord.setFieldValue('custrecord_expe_qty', parseFloat(vTempLineQty).toFixed(5));  //Commented and added to resovle decimal issue
								customrecord.setFieldValue('custrecord_act_qty', parseFloat(vTempLineQty).toFixed(5));
								customrecord.setFieldValue('custrecord_wms_status_flag', 9);	// 14 stands for outbound process


								customrecord.setFieldValue('custrecord_actbeginloc',vTempLocation);	
								//customrecord.setFieldValue('custrecord_actendloc', vbinloc);						
								//customrecord.setFieldValue('custrecord_lpno', vlp); 
								customrecord.setFieldValue('custrecord_upd_date', DateStamp());// case# 
								//customrecord.setFieldValue('custrecord_sku', vskuText);
								customrecord.setFieldValue('custrecord_ebiz_sku_no', varTempsku);
								customrecord.setFieldValue('name', vWOName);		

								customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
								customrecord.setFieldValue('custrecord_ebizuser', currentUserID);
								//customrecord.setFieldValue('custrecord_ebiz_wave_no', eBizWaveNo);
								customrecord.setFieldValue('custrecord_ebiz_order_no', vId);
								customrecord.setFieldValue('custrecord_batch_no', vTempBatch);
								customrecord.setFieldValue('custrecord_ebiz_cntrl_no', vId);
								//customrecord.setFieldValue('custrecord_ebiz_cntrl_no', rcptrecordid);

								customrecord.setFieldValue('custrecord_sku', varTempsku);
								customrecord.setFieldValue('custrecord_line_no', parseFloat(vTempLineno));
								customrecord.setFieldValue('custrecord_lpno', vTempLP);
								customrecord.setFieldValue('custrecord_packcode', vTempPackcode);					
								customrecord.setFieldValue('custrecord_sku_status', vTempSKUStatus);
								customrecord.setFieldValue('custrecord_wms_location', vWMSLocation);
								customrecord.setFieldValue('custrecord_invref_no', vTempLineRec);
								customrecord.setFieldValue('custrecord_notes', vTempComments);	
								customrecord.setFieldValue('custrecord_ebizrule_no', vTempPickruleId);
								customrecord.setFieldValue('custrecord_ebizmethod_no', vTempPickmethod);
								customrecord.setFieldValue('custrecord_ebizzone_no', vTempPickzone);

								if(vTempPickzone !=null && vTempPickzone !='')
								{
									customrecord.setFieldValue('custrecord_ebiz_zoneid', vTempPickzone);
								}


								nlapiLogExecution('DEBUG', 'beginLocationId',vTempLocation);
								if(vTempLocation!=null && vTempLocation!='')
								{
									nlapiLogExecution('Debug','Time stamp 26',TimeStampinSec());
									var locgroupfields = ['custrecord_inboundlocgroupid','custrecord_outboundlocgroupid'];
									var locgroupcolumns = nlapiLookupField('customrecord_ebiznet_location', vTempLocation, locgroupfields);
									nlapiLogExecution('Debug','Time stamp 27',TimeStampinSec());
									var inblocgroupid=locgroupcolumns.custrecord_inboundlocgroupid;
									var oublocgroupid=locgroupcolumns.custrecord_outboundlocgroupid;
									nlapiLogExecution('DEBUG', 'inblocgroupid',inblocgroupid);
									var locgroupseqfields = ['custrecord_sequenceno'];
									//code added on 011012 by suman
									//Allow to insert sequence no only if we get some value form inbonlog group Id.
									//if(inblocgroupid!=null&&inblocgroupid!="")
									if(oublocgroupid!=null && oublocgroupid!="")
									{
										nlapiLogExecution('Debug','Time stamp 28',TimeStampinSec());
										//var locgroupseqcolumns = nlapiLookupField('customrecord_ebiznet_loc_group', inblocgroupid, locgroupseqfields);
										var locgroupseqcolumns = nlapiLookupField('customrecord_ebiznet_loc_group', oublocgroupid, locgroupseqfields);
										var locgroupidseq=locgroupseqcolumns.custrecord_sequenceno;
										nlapiLogExecution('DEBUG', 'locgroupidseq',locgroupidseq);

										customrecord.setFieldValue('custrecord_bin_locgroup_seq', locgroupidseq);
										nlapiLogExecution('Debug','Time stamp 29',TimeStampinSec());
									}
								}

								var expiryDateInLot='';
								if(vTempBatch!=null && vTempBatch !=''){
									var filterbatch = new Array();
									filterbatch[0] = new nlobjSearchFilter('name', null, 'is', vTempBatch);
									filterbatch[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', varTempsku);
									var columns = new Array();
									columns[0] = new nlobjSearchColumn('custrecord_ebizexpirydate'); 
									var Lotnosearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterbatch,columns);
									if(Lotnosearchresults!=null && Lotnosearchresults!='')
									{
										nlapiLogExecution('ERROR', 'expiryDateInLot',expiryDateInLot);
										vLotno= Lotnosearchresults[0].getId();
										expiryDateInLot= Lotnosearchresults[0].getValue('custrecord_ebizexpirydate');
									}
									nlapiLogExecution('ERROR', 'expiryDateInLot',expiryDateInLot);
									if(expiryDateInLot !=null && expiryDateInLot!='')
										customrecord.setFieldValue('custrecord_expirydate', expiryDateInLot);

								}

								nlapiLogExecution('Debug','Time stamp 30',TimeStampinSec());
								//nlapiLogExecution('ERROR','eBizWaveNo ',eBizWaveNo);


								var OpentaskId= nlapiSubmitRecord(customrecord);			
								nlapiLogExecution('Debug','Time stamp 31',TimeStampinSec());
								nlapiLogExecution('ERROR', 'Success');

								if(OpentaskId !=null && OpentaskId !='')
								{
									nlapiLogExecution('ERROR', 'OpentaskId',OpentaskId);
									OpentaskInternalId.push(OpentaskId);
								}



							}
							else
							{

								if(vTempAvailableQty == 'T')
								{						
									nlapiLogExecution('ERROR', '2nd time Concurrency occured',vTempAvailableQty);
									vCount=vCount+1;
									continue LABL3;
								}
								else
								{
									tempFlag ="T";
									nlapiLogExecution('ERROR', 'no inventory available',tempFlag);
									nlapiLogExecution('ERROR', 'tempFlag',tempFlag);
								}

							}
						}

						catch(ex1)
						{
							nlapiLogExecution('ERROR', 'Exception in deleteAllocations',ex1);

							var exCode='CUSTOM_RECORD_COLLISION'; 
							var wmsE='Inventory record being updated by another user. Please try again...';
							if (ex1 instanceof nlobjError) 
							{	
								wmsE=ex1.getCode() + '\n' + ex1.getDetails();
								exCode=ex1.getCode();
							}
							else
							{
								wmsE=ex1.toString();
								exCode=ex1.toString();
							}  

							nlapiLogExecution('ERROR', 'Exception in Invt Updation : ', ex1);

							if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
							{
								vscount=vscount+1;
								continue LABL2;
							}
							else break LABL2;
						}


					}// end of for loop of LABL2

				}  //end of for loop of arryinvt

			} //end of if condition of arryinvt

		} // end of for loop of LABL3
		nlapiLogExecution('ERROR', 'End of loop : ', tempFlag);
		return tempFlag;
	}
	catch(exp1)
	{
		nlapiLogExecution('ERROR', 'exception in ConcurrencyHandlingForInvt',exp1);
		tempFlag ="T";
		return tempFlag;
	}


}





function checkUOM(vitem,vNSUOM)
{
	if(vNSUOM!=null && vNSUOM!="")
	{
		var eBizItemDims=geteBizItemDimensions(vitem);
		nlapiLogExecution('DEBUG', 'eBizItemDims', eBizItemDims);
		var vBaseUOMQty=1;
		var veBizUOMQty=1;
		if(eBizItemDims!=null&&eBizItemDims.length>0)
		{
			nlapiLogExecution('DEBUG', 'eBizItemDims Length', eBizItemDims.length);
			for(z=0; z < eBizItemDims.length; z++)
			{
				var UomArr = new Array();
				nlapiLogExecution('DEBUG', 'Base UOM Flag', eBizItemDims[z].getValue('custrecord_ebizbaseuom'));
				if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
				{
					vBaseUOMQty = eBizItemDims[z].getValue('custrecord_ebizqty');
					nlapiLogExecution('DEBUG', 'vBaseUOMQty', vBaseUOMQty);
				}

				nlapiLogExecution('DEBUG', 'Order Units', vNSUOM);
				nlapiLogExecution('DEBUG', 'SKU Dim Units', eBizItemDims[z].getValue('custrecord_ebiznsuom'));

				if(vNSUOM == eBizItemDims[z].getValue('custrecord_ebiznsuom'))
				{
					nlapiLogExecution('DEBUG', 'NS UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
					veBizUOMQty = eBizItemDims[z].getValue('custrecord_ebizqty');	
					nlapiLogExecution('DEBUG', 'veBizUOMQty', veBizUOMQty);
				}
				UomArr.push(vBaseUOMQty);
				UomArr.push(veBizUOMQty);
			}		
		}
		else
		{
			var UomArr = new Array();
			UomArr.push(vBaseUOMQty);
			UomArr.push(veBizUOMQty);
		}
	}
	else
	{
		var UomArr = new Array();
		UomArr.push(vBaseUOMQty);
		UomArr.push(veBizUOMQty);
	}
	return UomArr;
}

function geteBizItemDimensions(itemid)
{
	var searchRec = new Array();
	if(itemid!=null && itemid!='')
	{
		var filter = new Array();
		filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', itemid));
		filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		filter.push(new nlobjSearchFilter('custrecord_ebiznsuom', null, 'noneof', '@NONE@'));
		var column = new Array();
		column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim') ;
		column[1] = new nlobjSearchColumn('custrecord_ebizqty') ;
		column[2] = new nlobjSearchColumn('custrecord_ebizbaseuom') ;
		column[3] = new nlobjSearchColumn('custrecord_ebiznsuom') ;
		searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);
	}

	return searchRec;
}

