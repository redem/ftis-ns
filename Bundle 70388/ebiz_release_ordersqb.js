/***************************************************************************
                                                                 
                                                  eBizNET Solutions Inc 
****************************************************************************
*
*  $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_release_ordersqb.js,v $
*  $Revision: 1.1.2.2 $
*  $Date: 2013/09/11 15:23:51 $
*  $Author: rmukkera $
*  $Name: t_NSWMS_2014_1_1_174 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_release_ordersqb.js,v $
*  Revision 1.1.2.2  2013/09/11 15:23:51  rmukkera
*  Case# 20124376
*
*
****************************************************************************/

function releaseordersqb(request, response)
{
	if (request.getMethod() == 'GET') 
	{	
		var form = nlapiCreateForm('Release Orders');

		var salesorder = form.addField('custpage_salesorder', 'select', 'Sales Order #','salesorder');
		//salesorder.addSelectOption('','');

		var orderdate = form.addField('custpage_orderdate','date','Order Date');

		var shipmethod = form.addField('custpage_shipmethod','select','Ship Method');

		var record = nlapiCreateRecord('salesorder', {recordmode: 'dynamic'});

		var methodsField = record.getField('shipmethod');
		var methodOptions = methodsField.getSelectOptions(null, null);

		shipmethod.addSelectOption("",""); 

		for (var i = 0; methodOptions != null && i < methodOptions.length; i++) {
			//nlapiLogExecution('ERROR', methodOptions[i].getId() + ',' + methodOptions[i].getText() );
			shipmethod.addSelectOption(methodOptions[i].getId(),methodOptions[i].getText());
		}	

		var customer = form.addField('custpage_customer','select','Customer','customer');
		//customer.addSelectOption('','');

		var shipdate = form.addField('custpage_shipdate','date','Ship Date');

		var location = form.addField('custpage_location','select','Location','location');
		//location.addSelectOption('','');

		//fillsalesorderField(form,salesorder,customer,orderdate,shipdate,shipmethod,location,-1);

		var orderStatus = form.addField('custpage_ordstatus', 'select', 'Order Status');
		orderStatus.addSelectOption('', 'ALL');
		orderStatus.addSelectOption('SalesOrd:B', 'Pending Fulfillment');
		orderStatus.addSelectOption('SalesOrd:D', 'Partially Fulfilled');
		orderStatus.addSelectOption('SalesOrd:E', 'Pending Billing/Partially Fulfilled');

		form.addSubmitButton("Display");

		response.writePage(form);
	}
	else
	{
		var vordno = request.getParameter('custpage_salesorder');
		var vorddate = request.getParameter('custpage_orderdate');
		var vshipmethod = request.getParameter('custpage_shipmethod');
		var vcustomer = request.getParameter('custpage_customer');
		var vshipdate = request.getParameter('custpage_shipdate');
		var vlocation = request.getParameter('custpage_location');
		var vordstatus = request.getParameter('custpage_ordstatus');

		var WaveQbparams = new Array();		
		if (vordno!=null &&  vordno != "") {
			WaveQbparams["custpage_qbso"] = vordno;
		}

		if (vorddate!=null &&  vorddate != "") {
			WaveQbparams["custpage_orddate"] = vorddate;
		}
		if (vcustomer!=null && vcustomer != "") {
			WaveQbparams ["custpage_consignee"] = vcustomer;
		}
		if (vshipmethod!=null && vshipmethod != "") {
			WaveQbparams ["custpage_shipmethod"] = vshipmethod;
		}
		if (vshipdate!=null && vshipdate != "") {
			WaveQbparams ["custpage_shipdate"] = vshipdate;
		}
		if (vlocation!=null && vlocation != "") {
			WaveQbparams ["custpage_location"] = vlocation;
		}

		if (vordstatus!=null && vordstatus != "") {
			WaveQbparams ["custpage_ordstatus"] = vordstatus;
		}
		response.sendRedirect('SUITELET', 'customscript_ebiz_release_orders_main', 'customdeploy_ebiz_release_orders_main', false, WaveQbparams );
	}
}

function fillsalesorderField(form, salesorderField,cutomerField,orderdateField,shipdateField,shipmethodField,locationField,maxno){
	var salesorderFilers = new Array();
	salesorderFilers.push(new nlobjSearchFilter('status', null, 'anyof', ['SalesOrd:B','SalesOrd:D','SalesOrd:E']));
	salesorderFilers.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
	if(maxno!=-1)
	{
		salesorderFilers.push(new nlobjSearchFilter('internalidnumber', null, 'lessthan', parseFloat(maxno)));
	}
	var columns=new Array();
	columns[0]=new nlobjSearchColumn('internalid');
	columns[0].setSort(true);
	columns[1]=new nlobjSearchColumn('tranid');
	columns[2]=new nlobjSearchColumn('entity');
	columns[3]=new nlobjSearchColumn('trandate');
	columns[4]=new nlobjSearchColumn('shipmethod');
	columns[5]=new nlobjSearchColumn('location');
	columns[6]=new nlobjSearchColumn('shipdate');


	var customerSearchResults = nlapiSearchRecord('salesorder', null, salesorderFilers,columns);
	nlapiLogExecution('ERROR', 'customerSearchResults.length ',customerSearchResults.length);

	for (var i = 0; customerSearchResults != null && i < customerSearchResults.length; i++) {
		if(customerSearchResults[i].getValue('internalid') != ''){
			salesorderField.addSelectOption(customerSearchResults[i].getValue('internalid'), customerSearchResults[i].getValue('tranid'));
		}
		if(customerSearchResults[i].getText('entity')!=''){
			cutomerField.addSelectOption(customerSearchResults[i].getValue('entity'), customerSearchResults[i].getText('entity'));
		}
		if(customerSearchResults[i].getText('shipmethod')!=''){
			shipmethodField.addSelectOption(customerSearchResults[i].getValue('shipmethod'), customerSearchResults[i].getText('shipmethod'));
		}
		if(customerSearchResults[i].getText('location')!=''){
			locationField.addSelectOption(customerSearchResults[i].getValue('location'), customerSearchResults[i].getText('location'));
		}
	}
//	if(customerSearchResults!=null && customerSearchResults.length>=1000)
//	{
//	var column=new Array();
//	column[0]=new nlobjSearchColumn('tranid');		
//	column[1]=new nlobjSearchColumn('internalid');
//	column[1].setSort(true);

//	var OrderSearchResults = nlapiSearchRecord('salesorder', null, salesorderFilers,column);

//	var maxno=OrderSearchResults[OrderSearchResults.length-1].getValue(columns[1]);		
//	fillsalesorderField(form, salesorderField,cutomerField,orderdateField,shipdateField,shipmethodField,locationField,maxno);	
//	}
}