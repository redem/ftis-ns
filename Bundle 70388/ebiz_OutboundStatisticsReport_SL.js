/***************************************************************************
 		eBizNET Solutions 			
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Suitelet/Attic/ebiz_OutboundStatisticsReport_SL.js,v $
 *     	   $Revision: 1.1.2.1.4.2.4.2 $
 *     	   $Date: 2013/03/01 14:34:54 $
 *     	   $Author: skreddy $
 *     	   $Name: t_NSWMS_2013_1_3_2 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 * * REVISION HISTORY
 * $Log: ebiz_OutboundStatisticsReport_SL.js,v $
 * Revision 1.1.2.1.4.2.4.2  2013/03/01 14:34:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.1.2.1.4.2.4.1  2013/02/26 12:56:04  skreddy
 * CASE201112/CR201113/LOG201121
 * merged boombah changes
 *
 * Revision 1.1.2.1.4.2  2012/11/01 14:55:15  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.1.2.1.4.1  2012/10/26 13:57:50  skreddy
 * CASE201112/CR201113/LOG201121
 * Changed the functionality to get more than 1000 records
 *
 * Revision 1.1.2.1.2.1  2012/10/01 14:36:04  skreddy
 * CASE201112/CR201113/LOG201121
 * Changed the functionality to get more than 1000 records
 *
 *  
 *
 *****************************************************************************/

function StatisticsReport(request,response)
{
	var ctx = nlapiGetContext();
	if(request.getMethod()=='GET')
	{
		var form = nlapiCreateForm('Outbound Statistics Executive Summary');


		var currentdate = DateStamp();
		nlapiLogExecution('ERROR','currentdate',currentdate);
		var fromdate = form.addField('custpage_fromdate', 'date', 'From Date').setMandatory(true);
		fromdate.setDefaultValue(currentdate);
		var todate = form.addField('custpage_todate', 'date', 'To Date').setMandatory(true);
		todate.setDefaultValue(currentdate);
		var Customer = form.addField('custpage_customer', 'select', 'Customer','customer');
		var site = form.addField('cusppage_site', 'select', 'Location', 'location');
		// site.setLayoutType('normal', 'startrow');

		var button = form.addSubmitButton('Display');
		response.writePage(form);

	}
	else
	{
		var form = nlapiCreateForm('Outbound Statistics Executive Summary');
		var fromdate = form.addField('custpage_fromdate', 'date', 'From Date').setMandatory(true);
		fromdate.setDefaultValue(request.getParameter('custpage_fromdate'));
		var todate = form.addField('custpage_todate', 'date', 'To Date').setMandatory(true);
		todate.setDefaultValue(request.getParameter('custpage_todate'));
		var Customer = form.addField('custpage_customer', 'select', 'Customer','customer');
		if(request.getParameter('custpage_customer')!=null && request.getParameter('custpage_customer')!="")
			Customer.setDefaultValue(request.getParameter('custpage_customer'));

		var site = form.addField('cusppage_site', 'select', 'Location', 'location');
		site.setLayoutType('normal', 'startrow');

		if(request.getParameter('cusppage_site')!=null && request.getParameter('cusppage_site')!="")
			site.setDefaultValue(request.getParameter('cusppage_site'));

		var vFromdate = request.getParameter('custpage_fromdate');
		var vTodate = request.getParameter('custpage_todate');
		var vcustomer=request.getParameter('custpage_customer');
		var vlocation =request.getParameter('cusppage_site');
		nlapiLogExecution('ERROR','vcustomer',vcustomer);
		nlapiLogExecution('ERROR','location',vlocation);

		var sublist = form.addSubList("custpage_items", "list", "Report");
		sublist.addField("custpage_date", "text", "Date");
		sublist.addField("custpage_salesorder", "text", "No. of Sales Orders");
		sublist.addField("custpage_fullfillment", "text", "No. of FO's");
		sublist.addField("custpage_waves", "text", "No. of Waves");
		sublist.addField("custpage_pickgen", "text", "No. of Picks Generated");
		sublist.addField("custpage_pickcomp", "text", "No. of Picks Completed");
		sublist.addField("custpage_ordership", "text", "No. of FO's Shipped");
		sublist.addField("custpage_fopartialship", "text", "No. of FO's Partially Shipped");
		sublist.addField("custpage_salesordshipped", "text", "No. of SO's Shipped");
		sublist.addField("custpage_orderbilled", "text", "No. of Orders Billed/Invoiced");
		//sublist.addField("custpage_invoiceamount", "text", "Invoice amount");

		//no of Sales orders
		var noofSalesOrder=GetSalesorderCount(vFromdate,vTodate,vcustomer,-1,vlocation);

		//no of Fullfillments
		var FulfillResults=GetFulfillmentOrders(vFromdate,vTodate,vcustomer,-1,vlocation)
		var noofFulfillmentOrders=GetFulfillmentCount(FulfillResults);




		//PicksGens orders count from open task and closed task
		var OpenTskPickGenCnt=PickGenFromOpenTsk(vFromdate,vTodate,vcustomer,-1,vlocation);
		var ClosedTskPickGenCnt=PickGenFromClosedTsk(vFromdate,vTodate,vcustomer,-1,vlocation);
		var noofPicksGenerated=OpenTskPickGenCnt +ClosedTskPickGenCnt;


		//PicksCompleted orders count from open task and closed task
		var PicksFrmOpenTsk=GetPickCompletedcount(vFromdate,vTodate,vcustomer,-1,vlocation);
		var PicksFrmClosedTsk=GetPickCompFromClosed(vFromdate,vTodate,vcustomer,-1,vlocation);
		var noofPicksCompleted=PicksFrmOpenTsk + PicksFrmClosedTsk; //total count from open task and closed task

		//no of FO'S Shipped
		var FOshippedResults=GetOrdersShippedCount(vFromdate,vTodate,vcustomer,-1,vlocation);
		var FOsCount = GetFOShippedCount(FOshippedResults);
		var noofOrdersShipped=FOsCount.split(',');
		var noofFOshipped = noofOrdersShipped[0];
		var noofPartialShipped =noofOrdersShipped[1];



		//no of orders billed 
		var noofOrderBilled = GetOrdersBilledCount(vFromdate,vTodate,vcustomer,-1,vlocation);


		//no of Waves count 
		var OpenTskResults = OpenTskWavesRes(vFromdate,vTodate,vcustomer,-1,vlocation);
		var ClosedTskResults = ClosedTskWavesRes(vFromdate,vTodate,vcustomer,-1,vlocation);
		var noofWavesgenerated=GetWaveCount(OpenTskResults,ClosedTskResults); 

		//no of SO's Shipped
		var noofSoShipped=GetSalesOrdShippedCnt(vFromdate,vTodate,vcustomer,-1,vlocation);



		nlapiLogExecution('ERROR','noofWavesgenerated',noofWavesgenerated);


		// nlapiLogExecution('ERROR','noofSalesOrder001',noofSalesOrder);
		nlapiLogExecution('ERROR','OpenTskPickGenCnt',OpenTskPickGenCnt);
		nlapiLogExecution('ERROR','ClosedTskPickGenCnt',ClosedTskPickGenCnt);

		nlapiLogExecution('ERROR','PicksFrmOpenTsk',PicksFrmOpenTsk);
		nlapiLogExecution('ERROR','PicksFrmClosedTsk',PicksFrmClosedTsk);
		nlapiLogExecution('ERROR','noofFulfillmentOrders',noofFulfillmentOrders);

		nlapiLogExecution('ERROR','noofFOshipped',noofFOshipped);
		nlapiLogExecution('ERROR','noofPartialShipped',noofPartialShipped);

		i=1;
		var date=vFromdate+ "-"+vTodate;

		form.getSubList('custpage_items').setLineItemValue('custpage_date',i,date.toString());
		form.getSubList('custpage_items').setLineItemValue('custpage_salesorder',i,noofSalesOrder.toString());
		form.getSubList('custpage_items').setLineItemValue('custpage_fullfillment',i,noofFulfillmentOrders.toString());
		form.getSubList('custpage_items').setLineItemValue('custpage_waves',i,noofWavesgenerated.toString());
		form.getSubList('custpage_items').setLineItemValue('custpage_pickgen',i,noofPicksGenerated.toString());
		form.getSubList('custpage_items').setLineItemValue('custpage_pickcomp',i,noofPicksCompleted.toString());
		form.getSubList('custpage_items').setLineItemValue('custpage_ordership',i,noofFOshipped.toString());			
		form.getSubList('custpage_items').setLineItemValue('custpage_fopartialship',i,noofPartialShipped.toString());			
		form.getSubList('custpage_items').setLineItemValue('custpage_salesordshipped',i,noofSoShipped.toString());
		form.getSubList('custpage_items').setLineItemValue('custpage_orderbilled',i,noofOrderBilled.toString());
		//form.getSubList('custpage_items').setLineItemValue('custpage_invoiceamount',i,"0");

		var button = form.addSubmitButton('Display');
		response.writePage(form); 



	}

}



/**
 * Function to Get SalesOrder Count 
 * 
 * @param Fromdate
 * @param Todate
 * @param maxno
 * @param Customer

 */
var vscount=0;
function GetSalesorderCount(vFromdate,vTodate,vcustomer,maxno,vlocation)
{
	nlapiLogExecution('ERROR','noofSalesOrder','salesorder;');
	nlapiLogExecution('ERROR','fromdate',vFromdate);
	nlapiLogExecution('ERROR','todate',vTodate);
	var filters=new Array();
	filters.push(new nlobjSearchFilter('trandate', null, 'within', vFromdate, vTodate));			 
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

	if(vcustomer!=null && vcustomer!="")
	{
		filters.push(new nlobjSearchFilter('entity', null, 'anyof',vcustomer));
	}
	if(vlocation!=null && vlocation!="")
	{
		filters.push(new nlobjSearchFilter('location', null, 'is',vlocation));
	}
	if(maxno!=-1)
	{
		filters.push(new nlobjSearchFilter('internalidnumber', null, 'greaterthan', maxno));

	}

	var columns = new Array();	
	columns[0] = new nlobjSearchColumn('internalid');
	columns[0].setSort();

	var searchresults = nlapiSearchRecord('salesorder', null, filters,columns);
	if(searchresults!=null && searchresults!="")
	{
		if(searchresults.length>=1000)
		{
			nlapiLogExecution('ERROR','searchresults','more than 1000');
			var maxno1=searchresults[searchresults.length-1].getId();
			nlapiLogExecution('ERROR','maxno1',maxno1);
			//tempSalesOrdArray.push(searchresults);
			vscount=vscount+searchresults.length;
			GetSalesorderCount(vFromdate,vTodate,vcustomer,maxno1,vlocation);

		}
		else
		{
			nlapiLogExecution('ERROR','vscount',searchresults.length);
			vscount=vscount+searchresults.length;


		}
	}


	return vscount;

}
/**
 * Function to Get WavesCount from Open task
 * 
 * @param Fromdate
 * @param Todate
 * @param Customer

 */

var TempOpenTskWaveArray=new Array();
function OpenTskWavesRes(vFromdate,vTodate,vcustomer,maxno,vlocation)
{
	nlapiLogExecution('ERROR','noofwavesFromOpen','wave;');
	nlapiLogExecution('ERROR','fromdate',vFromdate);
	nlapiLogExecution('ERROR','todate',vTodate);
	var vcount=0;

	var filters=new Array();
	filters.push(new nlobjSearchFilter('custrecordact_begin_date', null, 'within', vFromdate, vTodate));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof',['3'] ));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'isnotempty'));

	if(vcustomer!=null && vcustomer!="")
	{
		filters.push(new nlobjSearchFilter('entity', 'custrecord_ebiz_order_no', 'anyof', vcustomer));
	}


	if(vlocation!=null && vlocation!="")
	{
		filters.push(new nlobjSearchFilter('custrecord_wms_location',null,'anyof',vlocation));
	}


	if(maxno!=-1)
	{
		filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));
	}

	var columns=new Array();
	columns.push(new nlobjSearchColumn('internalid'));
	columns.push(new nlobjSearchColumn('custrecord_ebiz_wave_no'));
	columns[0].setSort();

	var OpenTskWaves = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters,columns);
	if(OpenTskWaves!=null && OpenTskWaves!="")
	{
		if(OpenTskWaves.length>=1000)
		{
			nlapiLogExecution('ERROR','OpenTskWaves','more than 1000');
			var maxno1=OpenTskWaves[OpenTskWaves.length-1].getValue(columns[0]);
			TempOpenTskWaveArray.push(OpenTskWaves);
			OpenTskWavesRes(vFromdate,vTodate,vcustomer,maxno1,vlocation);

		}
		else
		{
			nlapiLogExecution('ERROR','OpenTskWaves',OpenTskWaves.length);
			TempOpenTskWaveArray.push(OpenTskWaves);

		}

	}
	return TempOpenTskWaveArray;

}

/**
 * Function to Get WavesCount from Closed task
 * 
 * @param Fromdate
 * @param Todate
 * @param Customer

 */

var TempClosedTskWaveArray=new Array();
function ClosedTskWavesRes(vFromdate,vTodate,vcustomer,maxno,vlocation)
{
	nlapiLogExecution('ERROR','noofwavesFromClosed','wave;');
	nlapiLogExecution('ERROR','fromdate',vFromdate);
	nlapiLogExecution('ERROR','todate',vTodate);
	var vcount=0;

	var filters=new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiztask_act_begin_date', null, 'within', vFromdate, vTodate));
	filters.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof',['3'] ));
	filters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_wave_no', null, 'isnotempty'));
	//filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof',['9'] ));

	if(vcustomer!=null && vcustomer!="")
	{
		filters.push(new nlobjSearchFilter('entity', 'custrecord_ebiztask_ebiz_order_no', 'anyof', vcustomer));
	}

	if(vlocation!=null && vlocation!="")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiztask_wms_location',null,'anyof',vlocation));
	}

	if(maxno!=-1)
	{
		filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));
	}


	var columns = new Array();	
	var columns=new Array();
	columns.push(new nlobjSearchColumn('internalid'));
	columns.push(new nlobjSearchColumn('custrecord_ebiztask_ebiz_wave_no'));
	columns[0].setSort();

	var ClosedTskWaves = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filters,columns);
	if(ClosedTskWaves!=null && ClosedTskWaves!="")
	{
		if(ClosedTskWaves.length>=1000)
		{
			nlapiLogExecution('ERROR','ClosedTskWaves','more than 1000');
			var maxno1=ClosedTskWaves[ClosedTskWaves.length-1].getValue(columns[0]);
			TempOpenTskWaveArray.push(ClosedTskWaves);
			ClosedTskWavesRes(vFromdate,vTodate,vcustomer,maxno1,vlocation);

		}
		else
		{
			nlapiLogExecution('ERROR','ClosedTskWaves',ClosedTskWaves.length);
			TempClosedTskWaveArray.push(ClosedTskWaves);

		}

	}
	return TempClosedTskWaveArray;

}

var tempArray=new Array();
function GetWaveCount(OpenTskResults,ClosedTskResults)
{
	var vWaveCnt=0;
	if(OpenTskResults !=null && OpenTskResults!="")
	{
		for( var i=0;i<OpenTskResults.length;i++)
		{
			var WaveResult = OpenTskResults[i];
			//var OpenTskWave=WaveResult.getText('custrecord_ebiz_wave_no');
			if(WaveResult!=null)
			{			
				for(var j=0;j<WaveResult.length;j++)
				{
					var OpenTskWave=WaveResult[j].getValue('custrecord_ebiz_wave_no');
					//nlapiLogExecution('ERROR','OpenTskWave',OpenTskWave);
					if(tempArray.indexOf(OpenTskWave)==-1)
					{
						tempArray.push(OpenTskWave);
					}

				}
			}


		}

	}
	if(ClosedTskResults!=null && ClosedTskResults!="")
	{
		for( var i=0;i<ClosedTskResults.length;i++)
		{
			var ClosedWaveResult = ClosedTskResults[i];
			//var OpenTskWave=WaveResult.getText('custrecord_ebiz_wave_no');
			if(ClosedWaveResult!=null)
			{			
				for(var j=0;j<ClosedWaveResult.length;j++)
				{
					var ClosedTskWave=ClosedWaveResult[j].getValue('custrecord_ebiztask_ebiz_wave_no');
					//nlapiLogExecution('ERROR','ClosedTskWave',ClosedTskWave);
					if(tempArray.indexOf(ClosedTskWave)==-1)
					{
						tempArray.push(ClosedTskWave);
					}

				}
			}


		}

	}
	vWaveCnt=tempArray.length;	
	nlapiLogExecution('ERROR','TempWaveArray Length',vWaveCnt);
	return vWaveCnt;
}



/**
 * Function to GetPickGenerated Orders Count from Open task  
 * 
 * @param Fromdate
 * @param Todate
 * @param maxno
 * @param Customer

 */

var vpcount=0;
function PickGenFromOpenTsk(vFromdate,vTodate,vcustomer,maxno,vlocation)
{	
	nlapiLogExecution('ERROR','noofPickGenOpentask','Pickgen;');
	var filters=new Array();
	filters.push(new nlobjSearchFilter('custrecordact_begin_date', null, 'within', vFromdate, vTodate));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof',['3'] ));//3 pick
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'noneof',['26','30'] ));//26 STATUS.OUTBOUND.FAILED  30	STATUS.OUBOUND.SHORTPICK



	if(vcustomer!=null && vcustomer!="")
	{
		filters.push(new nlobjSearchFilter('entity', 'custrecord_ebiz_order_no', 'anyof', vcustomer));
	}

	if(vlocation!=null && vlocation!="")
	{
		filters.push(new nlobjSearchFilter('custrecord_wms_location',null,'anyof',vlocation));
	}

	if(maxno!=-1)
	{
		filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));
	}
	var columns=new Array();
	columns.push(new nlobjSearchColumn('internalid'));
	columns[0].setSort();


	var PickgenResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters,columns);
	if(PickgenResults!=null && PickgenResults!="")
	{

		if(PickgenResults.length>=1000)
		{
			nlapiLogExecution('ERROR','PickgenResults','more than 1000');
			var maxno1=PickgenResults[PickgenResults.length-1].getValue(columns[0]);
			vpcount=vpcount+PickgenResults.length;
			PickGenFromOpenTsk(vFromdate,vTodate,vcustomer,maxno1,vlocation);

		}
		else
		{
			nlapiLogExecution('ERROR','PickgenResults',PickgenResults.length);
			vpcount=vpcount+PickgenResults.length;

		}
	}

	return vpcount;

}

/**
 * Function to PickGenerated Orders Count from Closed task  
 * 
 * @param Fromdate
 * @param Todate
 * @param maxno
 * @param Customer

 */

var vClosedCnt=0;
function PickGenFromClosedTsk(vFromdate,vTodate,vcustomer,maxno,vlocation)
{	
	nlapiLogExecution('ERROR','noofPickGenClosedTask','Pickgen;');
	var filters=new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiztask_act_begin_date', null, 'within', vFromdate, vTodate));
	filters.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof',['3'] ));//3 pick
	filters.push(new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag', null, 'noneof',['26','30'] ));//26 STATUS.OUTBOUND.FAILED  30	STATUS.OUBOUND.SHORTPICK



	if(vcustomer!=null && vcustomer!="")
	{
		filters.push(new nlobjSearchFilter('entity', 'custrecord_ebiztask_ebiz_order_no', 'anyof', vcustomer));
	}

	if(vlocation!=null && vlocation!="")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiztask_wms_location',null,'anyof',vlocation));
	}

	if(maxno!=-1)
	{
		filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));
	}
	var columns=new Array();
	columns.push(new nlobjSearchColumn('internalid'));
	columns[0].setSort();


	var ClosedTskPickgenRes = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filters,columns);
	if(ClosedTskPickgenRes!=null && ClosedTskPickgenRes!="")
	{

		if(ClosedTskPickgenRes.length>=1000)
		{
			nlapiLogExecution('ERROR','ClosedTskPickgenRes','more than 1000');
			var maxno1=ClosedTskPickgenRes[ClosedTskPickgenRes.length-1].getValue(columns[0]);
			vClosedCnt=vClosedCnt+ClosedTskPickgenRes.length;
			PickGenFromClosedTsk(vFromdate,vTodate,vcustomer,maxno1,vlocation);

		}
		else
		{
			nlapiLogExecution('ERROR','ClosedTskPickgenRes',ClosedTskPickgenRes.length);
			vClosedCnt=vClosedCnt+ClosedTskPickgenRes.length;

		}
	}

	return vClosedCnt;

}




/**
 * Function to GetPickConfirmOrders Count from open task 
 * 
 * @param Fromdate
 * @param Todate
 * @param maxno
 * @param Customer

 */
var vOpenTaskCnt=0;
function GetPickCompletedcount(vFromdate,vTodate,vcustomer,maxno,vlocation)
{	
	nlapiLogExecution('ERROR','noofPickCompOpentask','PickComp;');
	var filters=new Array();
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'within', vFromdate, vTodate));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof',['3'] ));//3 Pick
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'noneof',['26','30'] ));
	//26 STATUS.OUTBOUND.FAILED  30	STATUS.OUBOUND.SHORTPICK


	if(vcustomer!=null && vcustomer!="")
	{
		filters.push(new nlobjSearchFilter('entity', 'custrecord_ebiz_order_no', 'anyof', vcustomer));
	}

	if(vlocation!=null && vlocation!="")
	{
		filters.push(new nlobjSearchFilter('custrecord_wms_location',null,'anyof',vlocation));
	}

	if(maxno!=-1)
	{
		filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));
	}
	var columns=new Array();
	columns.push(new nlobjSearchColumn('internalid'));
	columns[0].setSort();


	var OpenPickCompResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters,columns);
	if(OpenPickCompResults!=null && OpenPickCompResults!="")
	{

		if(OpenPickCompResults.length>=1000)
		{
			nlapiLogExecution('ERROR','OpenPickCompResults','more than 1000');
			nlapiLogExecution('ERROR','OpenPickCompResults',OpenPickCompResults.length);
			var maxno1=OpenPickCompResults[OpenPickCompResults.length-1].getValue(columns[0]);
			nlapiLogExecution('ERROR','maxno1',maxno1);
			vOpenTaskCnt=vOpenTaskCnt+OpenPickCompResults.length;
			GetPickCompletedcount(vFromdate,vTodate,vcustomer,maxno1,vlocation);

		}
		else
		{
			nlapiLogExecution('ERROR','OpenPickCompResults',OpenPickCompResults.length);
			vOpenTaskCnt=vOpenTaskCnt+OpenPickCompResults.length;

		}

	}


	return vOpenTaskCnt;

}


/**
 * Function to GetPickConfirmOrders Count from Closed task 
 * 
 * @param Fromdate
 * @param Todate
 * @param Customer
 * @param maxno

 */

var vClosedTaskCnt=0;
function GetPickCompFromClosed(vFromdate,vTodate,vcustomer,maxno,vlocation)
{nlapiLogExecution('ERROR','noofPickCompClosedtask','PickComp;');
var filters=new Array();
filters.push(new nlobjSearchFilter('custrecord_ebiztask_act_end_date', null, 'within', vFromdate, vTodate));
filters.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof',['3'] ));//3 Pick
filters.push(new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag', null, 'noneof',['26','30'] ));
//26 STATUS.OUTBOUND.FAILED  30	STATUS.OUBOUND.SHORTPICK


if(vcustomer!=null && vcustomer!="")
{
	filters.push(new nlobjSearchFilter('entity', 'custrecord_ebiztask_ebiz_order_no', 'anyof', vcustomer));
}

if(vlocation!=null && vlocation!="")
{
	filters.push(new nlobjSearchFilter('custrecord_ebiztask_wms_location',null,'anyof',vlocation));
}

if(maxno!=-1)
{
	filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));
}
var columns=new Array();
columns.push(new nlobjSearchColumn('internalid'));
columns[0].setSort();


var ClosedPickCompResults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filters,columns);
if(ClosedPickCompResults!=null && ClosedPickCompResults!="")
{

	if(ClosedPickCompResults.length>=1000)
	{
		nlapiLogExecution('ERROR','ClosedPickCompResults','more than 1000');
		nlapiLogExecution('ERROR','ClosedPickCompResults',ClosedPickCompResults.length);
		var maxno1=ClosedPickCompResults[ClosedPickCompResults.length-1].getId();
		vClosedTaskCnt=vClosedTaskCnt+ClosedPickCompResults.length;
		GetPickCompFromClosed(vFromdate,vTodate,vcustomer,maxno1,vlocation);

	}
	else
	{
		nlapiLogExecution('ERROR','ClosedPickCompResults',ClosedPickCompResults.length);
		vClosedTaskCnt=vClosedTaskCnt+ClosedPickCompResults.length;

	}

}

return vClosedTaskCnt;
}


/**
 * Function to GetShipDate Count from Closed task 
 * 
 * @param Fromdate
 * @param Todate
 * @param Customer
 * @param maxno

 */

var TempFOshipArray=new Array();
function GetOrdersShippedCount(vFromdate,vTodate,vcustomer,maxno,vlocation)
{	
	nlapiLogExecution('ERROR','noofshipped','Shipped;');
	var vcount=0;
	var filters =new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_shipdate', null, 'within', vFromdate, vTodate));
	//filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof',['14']));
	//filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

	if(vcustomer!=null && vcustomer!="")
	{
		filters.push(new nlobjSearchFilter('custrecord_do_customer',null,'anyof',vcustomer));
	}

	if(vlocation!=null && vlocation!="")
	{
		filters.push(new nlobjSearchFilter('custrecord_ordline_wms_location',null,'anyof',vlocation));
	}


	if(maxno!=-1)
	{
		filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));
	}

	var columns = new Array();	
	columns.push(new nlobjSearchColumn('internalid'));
	columns.push(new nlobjSearchColumn('custrecord_lineord'));
	columns.push(new nlobjSearchColumn('custrecord_linestatus_flag'));
	columns.push(new nlobjSearchColumn('custrecord_ns_ord'));
	columns[0].setSort();

	var OrdersShippedResults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters,columns);
	if(OrdersShippedResults!=null && OrdersShippedResults!="")
	{
		if(OrdersShippedResults.length>=1000)
		{
			nlapiLogExecution('ERROR','OrdersShippedResults','more than 1000');
			var maxno1=OrdersShippedResults[OrdersShippedResults.length-1].getValue(columns[0]);
			nlapiLogExecution('ERROR','maxno1',maxno1);
			TempFOshipArray.push(OrdersShippedResults);
			GetOrdersShippedCount(vFromdate,vTodate,vcustomer,maxno1,vlocation);

		}
		else
		{
			nlapiLogExecution('ERROR','OrdersShippedResults',OrdersShippedResults.length);
			TempFOshipArray.push(OrdersShippedResults);


		}  	

	}
	return TempFOshipArray;

}


//removing Duplicate Order numbers
var ShippedArray=new Array();
function GetFOShippedCount(FOshippedResults)
{
	var vFOOrderCnt=0;
	var vPartShipCnt=0;
	if(FOshippedResults !=null && FOshippedResults!="")
	{
		nlapiLogExecution('ERROR','FOshippedResults.length',FOshippedResults.length);
		for( var i=0;i<FOshippedResults.length;i++)
		{
			var Results = FOshippedResults[i];
			if(Results!=null)
			{			
				for(var j=0;j<Results.length;j++)
				{
					var OrderNo=Results[j].getValue('custrecord_lineord');
					var FoStatus=Results[j].getValue('custrecord_linestatus_flag');
					//nlapiLogExecution('ERROR','FoStatus',FoStatus);
					// code added from Boombah on 25Feb13 by santosh
					if(FoStatus=="14")
					{
						if(ShippedArray.indexOf(OrderNo)==-1)
						{
							//nlapiLogExecution('ERROR','OrderNo',OrderNo);
							ShippedArray.push(OrderNo);
						}
					}
					if(FoStatus!="14" && FoStatus!="26")
					{
						vPartShipCnt= vPartShipCnt+1;	
					}
					//up to here

				}
			}

		}

	}

	vFOOrderCnt=ShippedArray.length+","+vPartShipCnt;
	nlapiLogExecution('ERROR','ShippedArray.length',ShippedArray.length);
	nlapiLogExecution('ERROR','vPartialShipCnt',vPartShipCnt);
	nlapiLogExecution('ERROR','FulfullmentArray Length',vFOOrderCnt);
	return vFOOrderCnt;
}





/**
 * Function to GetBilled orders Count
 * 
 * @param Fromdate
 * @param Todate
 * @param Customer
 * @param maxno

 */
var vbcount=0;
function GetOrdersBilledCount(vFromdate,vTodate,vcustomer,maxno,vlocation)
{	
	nlapiLogExecution('ERROR','noofOrdersBilled','Billed;');
	//Pending Billing,"SalesOrd:F",  Billed:"SalesOrd:G"
	var filters =new Array();
	filters.push(new nlobjSearchFilter('trandate', null, 'within', vFromdate, vTodate));
	filters.push(new nlobjSearchFilter('Status', null, 'is',['SalesOrd:G']));
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

	if(vcustomer!=null && vcustomer!="")
	{
		filters.push(new nlobjSearchFilter('entity', null, 'anyof',vcustomer));
	}


	if(vlocation!=null && vlocation!="")
	{
		filters.push(new nlobjSearchFilter('location', null, 'is',vlocation));
	}

	if(maxno!=-1)
	{
		filters.push(new nlobjSearchFilter('internalidnumber', null, 'greaterthan', maxno));
	}

	var columns = new Array();	
	columns[0] = new nlobjSearchColumn('internalid');
	columns[0].setSort(); 

	var OrdersBilled = nlapiSearchRecord('salesorder', null, filters,columns);

	if(OrdersBilled!=null && OrdersBilled!="")
	{
		if(OrdersBilled.length>=1000)
		{
			nlapiLogExecution('ERROR','OrdersBilled','more than 1000');
			var maxno1=OrdersBilled[OrdersBilled.length-1].getId();
			nlapiLogExecution('ERROR','maxno1',maxno1);
			vbcount=vbcount+OrdersBilled.length;
			GetOrdersBilledCount(vFromdate,vTodate,vcustomer,maxno1,vlocation);

		}
		else
		{
			nlapiLogExecution('ERROR','vscount',OrdersBilled.length);
			vbcount=vbcount+OrdersBilled.length;


		}

	}
	return vbcount;

}
/**
 * Function to GetFulfillment Count
 * 
 * @param Fromdate
 * @param Todate
 * @param Customer
 * @param maxno

 */

var TempFulfullArray =new Array();
function GetFulfillmentOrders(vFromdate,vTodate,vcustomer,maxno,vlocation)
{
	nlapiLogExecution('ERROR','noofFulfillment','Fulfillment;');

	nlapiLogExecution('ERROR','fromdate',vFromdate);
	nlapiLogExecution('ERROR','todate',vTodate);
	nlapiLogExecution('ERROR','vlocation',vlocation);
	var vfcount=0;

	var filters=new Array();
	filters.push(new nlobjSearchFilter('custrecord_record_linedate', null, 'within', vFromdate, vTodate));			 
	filters.push(new nlobjSearchFilter('mainline','custrecord_ns_ord','is','T'));

	if(vcustomer!=null && vcustomer!="")
	{
		filters.push(new nlobjSearchFilter('custrecord_do_customer',null,'anyof',vcustomer));
	}

	if(vlocation!=null && vlocation!="")
	{
		filters.push(new nlobjSearchFilter('custrecord_ordline_wms_location',null,'anyof',vlocation));
	}


	if(maxno!=-1)
	{
		filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));
	}

	var columns = new Array();	
	columns.push(new nlobjSearchColumn('internalid'));
	columns.push(new nlobjSearchColumn('custrecord_ns_ord'));
	columns[0].setSort();

	var FulfillResults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters,columns);
	if(FulfillResults!=null && FulfillResults!="")
	{   
		if(FulfillResults.length>=1000)
		{
			nlapiLogExecution('ERROR','FO FulfillResults','more than 1000');
			var maxno1=FulfillResults[FulfillResults.length-1].getValue(columns[0]);
			nlapiLogExecution('ERROR','maxno1',maxno1);
			TempFulfullArray.push(FulfillResults);
			GetFulfillmentOrders(vFromdate,vTodate,vcustomer,maxno1,vlocation);

		}
		else
		{
			nlapiLogExecution('ERROR','FulfillResults',FulfillResults.length);
			TempFulfullArray.push(FulfillResults);


		}

	}

	return TempFulfullArray;

}



//removing Duplicate Fulfillment numbers
var FulfullArray=new Array();
function GetFulfillmentCount(FulfillmentResults)
{
	var vFulfillCnt=0;
	if(FulfillmentResults !=null && FulfillmentResults!="")
	{
		for( var i=0;i<FulfillmentResults.length;i++)
		{
			var FulfillResult = FulfillmentResults[i];
			//var OpenTskWave=WaveResult.getText('custrecord_ebiz_wave_no');
			if(FulfillResult!=null)
			{			
				for(var j=0;j<FulfillResult.length;j++)
				{
					var FulfillNo=FulfillResult[j].getValue('custrecord_ns_ord');
					//nlapiLogExecution('ERROR','OpenTskWave',OpenTskWave);
					if(FulfullArray.indexOf(FulfillNo)==-1)
					{
						FulfullArray.push(FulfillNo);
					}

				}
			}


		}

	}

	vFulfillCnt=FulfullArray.length;	
	nlapiLogExecution('ERROR','FulfullmentArray Length',vFulfillCnt);
	return vFulfillCnt;
}







/**
 * Function to GetPendingBilling orders Count
 * 
 * @param Fromdate
 * @param Todate
 * @param Customer
 * @param maxno

 */
var vOrdcount=0;
function GetSalesOrdShippedCnt(vFromdate,vTodate,vcustomer,maxno,vlocation)
{	
	nlapiLogExecution('ERROR','noof PendingBilling Orders','Pending;');
	//Pending Billing,"SalesOrd:F",  Billed:"SalesOrd:G",Pending Billing/Partially Fulfilled�, �SalesOrd:E�
	var filters =new Array();
	filters.push(new nlobjSearchFilter('actualshipdate', null, 'within', vFromdate, vTodate));
	filters.push(new nlobjSearchFilter('Status', null, 'is',['SalesOrd:E','SalesOrd:F','SalesOrd:G']));
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

	if(vcustomer!=null && vcustomer!="")
	{
		filters.push(new nlobjSearchFilter('entity', null, 'anyof',vcustomer));
	}

	if(vlocation!=null && vlocation!="")
	{
		filters.push(new nlobjSearchFilter('location', null, 'is',vlocation));
	}

	if(maxno!=-1)
	{
		filters.push(new nlobjSearchFilter('internalidnumber', null, 'greaterthan', maxno));
	}

	var columns = new Array();	
	columns[0] = new nlobjSearchColumn('internalid');
	columns[0].setSort(); 

	var OrdersShipped = nlapiSearchRecord('salesorder', null, filters,columns);

	if(OrdersShipped!=null && OrdersShipped!="")
	{
		if(OrdersShipped.length>=1000)
		{
			nlapiLogExecution('ERROR','SO OrdersShipped','more than 1000');
			var maxno1=OrdersShipped[OrdersShipped.length-1].getId();
			nlapiLogExecution('ERROR','maxno1',maxno1);
			vOrdcount=vOrdcount+OrdersShipped.length;
			GetOrdersBilledCount(vFromdate,vTodate,vcustomer,maxno1,vlocation);

		}
		else
		{
			nlapiLogExecution('ERROR','vOrdcount',OrdersShipped.length);
			vOrdcount=vOrdcount+OrdersShipped.length;


		}

	}
	return vOrdcount;

}




function DateStamp(){
	var now = new Date();
	return ((parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());

}