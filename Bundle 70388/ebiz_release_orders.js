/***************************************************************************
                                                                 
                                                  eBizNET Solutions Inc 
****************************************************************************
*
*  $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_release_orders.js,v $
*  $Revision: 1.1.2.2 $
*  $Date: 2013/09/11 15:23:51 $
*  $Author: rmukkera $
*  $Name: t_NSWMS_2014_1_1_174 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_release_orders.js,v $
*  Revision 1.1.2.2  2013/09/11 15:23:51  rmukkera
*  Case# 20124376
*
*
****************************************************************************/
function releaseordersmain(request, response)
{
	if (request.getMethod() == 'GET') 
	{	
		nlapiLogExecution('ERROR', 'start ','start');

		var form = nlapiCreateForm("Release Orders");

		form.setScript('customscript_ebiz_releaseorders_cl');

		var hiddenfieldselectpage=form.addField('custpage_hiddenfieldselectpage', 'text', 'hiddenflag').setDisplayType('hidden');
		hiddenfieldselectpage.setDefaultValue('F');

//		var salesorder = form.addField('custpage_qbso', 'select', 'Sales Order #');
//		salesorder.addSelectOption('','');		

//		var orderdate = form.addField('custpage_orddate','date','Order Date');

//		var shipmethod = form.addField('custpage_shipmethod','select','Ship Method');
//		shipmethod.addSelectOption('','');

//		var customer = form.addField('custpage_consignee','select','Customer');
//		customer.addSelectOption('','');

//		var shipdate = form.addField('custpage_shipdate','date','Ship Date');

//		var location = form.addField('custpage_location','select','Location');
//		location.addSelectOption('','');
//		if (request.getParameter('custpage_qbso')!= "" && request.getParameter('custpage_qbso')!= null) {
//		salesorder.setDefaultValue(request.getParameter('custpage_qbso'));
//		}
//		if (request.getParameter('custpage_consignee') != "" && request.getParameter('custpage_consignee')!= null) {
//		customer.setDefaultValue(request.getParameter('custpage_consignee'));

//		}
//		nlapiLogExecution('ERROR', 'request.getParameter(custpage_orderdate)',request.getParameter('custpage_orderdate'));

//		if (request.getParameter('custpage_orddate')!= "" && request.getParameter('custpage_orddate')!= null) {
//		orderdate.setDefaultValue(request.getParameter('custpage_orddate'));

//		}
//		nlapiLogExecution('ERROR', 'request.getParameter(custpage_shipmethod) ',request.getParameter('custpage_shipmethod'));

//		if (request.getParameter('custpage_shipmethod') != "" && request.getParameter('custpage_shipmethod')!= null) {
//		shipmethod.setDefaultValue(request.getParameter('custpage_shipmethod'));

//		}
//		nlapiLogExecution('ERROR', 'request.getParameter(custpage_location)',request.getParameter('custpage_location'));

//		if (request.getParameter('custpage_location') != "" && request.getParameter('custpage_location')!= null) {
//		location.setDefaultValue(request.getParameter('custpage_location'));

//		}
//		nlapiLogExecution('ERROR', 'request.getParameter(custpage_shipdate) ',request.getParameter('custpage_shipdate'));

//		if (request.getParameter('custpage_shipdate') != "" && request.getParameter('custpage_shipdate')!= null) {
//		shipdate.setDefaultValue(request.getParameter('custpage_shipdate'));

//		}
//		fillsalesorderField(form,salesorder,customer,orderdate,shipdate,shipmethod,location,-1);

		var sublist = form.addSubList('custpage_sorep', 'list', 'Sales Orders');
		sublist.addField("custpage_sactive", "checkbox", "Select");
		sublist.addField("custpage_sordrdate", "text", "Order Date");
		sublist.addField("custpage_so", "text", "SO #");
		sublist.addField("custpage_soid", "text", "SO id").setDisplayType('hidden');
		sublist.addField("custpage_scustomer", "text", "Customer");
		sublist.addField("custpage_sshipvia", "text", "Ship Via");

		var orderlist=displaySublist(request,0);

		if(orderlist!=null && orderlist.length>0)
		{
			setPagingForSublist(orderlist,form);
		}
		form.addSubmitButton('Release');
		form.addField('custpage_isfromselect', 'text', 'xxxx').setDisplayType('hidden');

		//form.addButton('custpage_release','Display',"nlapiSetFieldValue('custpage_isfromselect','T');NLDoMainFormButtonAction('submitter',true)");

		response.writePage(form);
	}
	else
	{


//		nlapiLogExecution('ERROR', 'else ','start');

//		var form = nlapiCreateForm("Release Orders");

//		form.setScript('customscript_ebiz_releaseorders_cl');
//		var hiddenfieldselectpage=form.addField('custpage_hiddenfieldselectpage', 'text', 'hiddenflag').setDisplayType('hidden');
//		hiddenfieldselectpage.setDefaultValue('F');

//		var salesorder = form.addField('custpage_qbso', 'select', 'Sales Order #');
//		salesorder.addSelectOption('','');

//		var orderdate = form.addField('custpage_orddate','date','Order Date');

//		var shipmethod = form.addField('custpage_shipmethod','select','Ship Method');
//		shipmethod.addSelectOption('','');

//		var customer = form.addField('custpage_consignee','select','Customer');
//		customer.addSelectOption('','');

//		var shipdate = form.addField('custpage_shipdate','date','Ship Date');

//		var location = form.addField('custpage_location','select','Location');
//		location.addSelectOption('','');
//		if (request.getParameter('custpage_qbso')!= "" && request.getParameter('custpage_qbso')!= null) {
//		salesorder.setDefaultValue(request.getParameter('custpage_qbso'));
//		}
//		if (request.getParameter('custpage_consignee') != "" && request.getParameter('custpage_consignee')!= null) {
//		customer.setDefaultValue(request.getParameter('custpage_consignee'));

//		}
//		nlapiLogExecution('ERROR', 'request.getParameter(custpage_orderdate)',request.getParameter('custpage_orderdate'));

//		if (request.getParameter('custpage_orddate')!= "" && request.getParameter('custpage_orddate')!= null) {
//		orderdate.setDefaultValue(request.getParameter('custpage_orddate'));

//		}
//		nlapiLogExecution('ERROR', 'request.getParameter(custpage_shipmethod) ',request.getParameter('custpage_shipmethod'));

//		if (request.getParameter('custpage_shipmethod') != "" && request.getParameter('custpage_shipmethod')!= null) {
//		shipmethod.setDefaultValue(request.getParameter('custpage_shipmethod'));

//		}
//		nlapiLogExecution('ERROR', 'request.getParameter(custpage_location)',request.getParameter('custpage_location'));

//		if (request.getParameter('custpage_location') != "" && request.getParameter('custpage_location')!= null) {
//		location.setDefaultValue(request.getParameter('custpage_location'));

//		}
//		nlapiLogExecution('ERROR', 'request.getParameter(custpage_shipdate) ',request.getParameter('custpage_shipdate'));

//		if (request.getParameter('custpage_shipdate') != "" && request.getParameter('custpage_shipdate')!= null) {
//		shipdate.setDefaultValue(request.getParameter('custpage_shipdate'));

//		}
		//fillsalesorderField(form,salesorder,customer,orderdate,shipdate,shipmethod,location,-1);

//		var sublist = form.addSubList('custpage_sorep', 'list', 'SalesOrder Report');
//		sublist.addField("custpage_sordrdate", "text", "Order Date");
//		sublist.addField("custpage_so", "text", "SO #");
//		sublist.addField("custpage_soid", "text", "SO id").setDisplayType('hidden');
//		sublist.addField("custpage_scustomer", "text", "Customer");
//		sublist.addField("custpage_sshipvia", "text", "Ship Via");
//		sublist.addField("custpage_sactive", "checkbox", "Select");
//		form.addField('custpage_isfromselect', 'text', 'xxxx').setDisplayType('hidden');

		var form = nlapiCreateForm("Release Orders");

		form.setScript('customscript_ebiz_releaseorders_cl');

		if (request.getParameter('custpage_hiddenfieldselectpage') != 'F')
		{
//			var orderlist=displaySublist(request,0);
//			if(orderlist!=null && orderlist.length>0)
//			{
//			setPagingForSublist(orderlist,form);
//			}

			nlapiLogExecution('ERROR', 'start ','start');

			var hiddenfieldselectpage=form.addField('custpage_hiddenfieldselectpage', 'text', 'hiddenflag').setDisplayType('hidden');
			hiddenfieldselectpage.setDefaultValue('F');

			/*
			var salesorder = form.addField('custpage_qbso', 'select', 'Sales Order #');
			salesorder.addSelectOption('','');			

			var orderdate = form.addField('custpage_orddate','date','Order Date');

			var shipmethod = form.addField('custpage_shipmethod','select','Ship Method');
			shipmethod.addSelectOption('','');

			var customer = form.addField('custpage_consignee','select','Customer');
			customer.addSelectOption('','');

			var shipdate = form.addField('custpage_shipdate','date','Ship Date');

			var location = form.addField('custpage_location','select','Location');
			location.addSelectOption('','');
			if (request.getParameter('custpage_qbso')!= "" && request.getParameter('custpage_qbso')!= null) {
				salesorder.setDefaultValue(request.getParameter('custpage_qbso'));
			}
			if (request.getParameter('custpage_consignee') != "" && request.getParameter('custpage_consignee')!= null) {
				customer.setDefaultValue(request.getParameter('custpage_consignee'));

			}
			nlapiLogExecution('ERROR', 'request.getParameter(custpage_orderdate)',request.getParameter('custpage_orderdate'));

			if (request.getParameter('custpage_orddate')!= "" && request.getParameter('custpage_orddate')!= null) {
				orderdate.setDefaultValue(request.getParameter('custpage_orddate'));

			}
			nlapiLogExecution('ERROR', 'request.getParameter(custpage_shipmethod) ',request.getParameter('custpage_shipmethod'));

			if (request.getParameter('custpage_shipmethod') != "" && request.getParameter('custpage_shipmethod')!= null) {
				shipmethod.setDefaultValue(request.getParameter('custpage_shipmethod'));

			}
			nlapiLogExecution('ERROR', 'request.getParameter(custpage_location)',request.getParameter('custpage_location'));

			if (request.getParameter('custpage_location') != "" && request.getParameter('custpage_location')!= null) {
				location.setDefaultValue(request.getParameter('custpage_location'));

			}
			nlapiLogExecution('ERROR', 'request.getParameter(custpage_shipdate) ',request.getParameter('custpage_shipdate'));

			if (request.getParameter('custpage_shipdate') != "" && request.getParameter('custpage_shipdate')!= null) {
				shipdate.setDefaultValue(request.getParameter('custpage_shipdate'));

			}
			fillsalesorderField(form,salesorder,customer,orderdate,shipdate,shipmethod,location,-1);

			 */

			var sublist = form.addSubList('custpage_sorep', 'list', 'Sales Orders');
			sublist.addField("custpage_sactive", "checkbox", "Select");
			sublist.addField("custpage_sordrdate", "text", "Order Date");
			sublist.addField("custpage_so", "text", "SO #");
			sublist.addField("custpage_soid", "text", "SO id").setDisplayType('hidden');
			sublist.addField("custpage_scustomer", "text", "Customer");
			sublist.addField("custpage_sshipvia", "text", "Ship Via");

			var orderlist=displaySublist(request,0);

			if(orderlist!=null && orderlist.length>0)
			{
				setPagingForSublist(orderlist,form);
			}
			form.addSubmitButton('Release');
			form.addField('custpage_isfromselect', 'text', 'xxxx').setDisplayType('hidden');

			//form.addButton('custpage_release','Display',"nlapiSetFieldValue('custpage_isfromselect','T');NLDoMainFormButtonAction('submitter',true)");

			response.writePage(form);


		}
		else
		{

			var linecount=request.getLineItemCount('custpage_sorep');

			if(linecount!=null && linecount>0)
			{
				var SOIntidArray=new Array();
				var soids='';
				var selectord=0;

				for(var k=1;k<=linecount;k++)
				{
					if(request.getLineItemValue('custpage_sorep','custpage_sactive',k)=='T')
					{
						nlapiLogExecution('ERROR', 'soid1', request.getLineItemValue('custpage_sorep','custpage_soid',k));	
						nlapiLogExecution('ERROR', 'so1', request.getLineItemValue('custpage_sorep','custpage_so',k));	
						nlapiLogExecution('ERROR', 'customer', request.getLineItemValue('custpage_sorep','custpage_scustomer',k));	

						SOIntidArray[SOIntidArray.length]=request.getLineItemValue('custpage_sorep','custpage_soid',k);

						if(selectord==0)
						{
							soids=request.getLineItemValue('custpage_sorep','custpage_soid',k);
						}
						else
						{
							soids=soids+","+request.getLineItemValue('custpage_sorep','custpage_soid',k);
						}

						selectord++;
					}
				}

				if(SOIntidArray.length==0)
				{
					var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
					msg.setDefaultValue("<div id='div__alert' align='center'></div><script>alert('Please Select atleast one So');</script></div>");
				}
				else
				{
					nlapiLogExecution('ERROR', 'soids', soids);	
					nlapiLogExecution('ERROR', 'Invoking Schedule Script Starts', TimeStampinSec());	

					var eBizWaveNo = GetMaxTransactionNo('WAVE');
					nlapiLogExecution('ERROR', 'eBizWaveNo', eBizWaveNo);

					var param = new Array();
					param['custscript_sointrids'] = soids;
					param['custscript_waveno'] = eBizWaveNo;
					nlapiScheduleScript('customscript_ebiz_release_orders_sch', null,param);

					nlapiLogExecution('ERROR', 'Invoking Schedule Script Ends', TimeStampinSec());

					showInlineMessage(form, 'Confirmation', 'Order release process has been initiated. Wave # : '+ eBizWaveNo);

					form.addButton('custpage_backtosearch','Back to Search','backtoreleaseorders()');
					form.addButton('custpage_wavestatus','Wave Status Report','gotowavestatus()');
				}
			}
//			if(orderlist!=null && orderlist.length>0)
//			{
//			setPagingForSublist(orderlist,form);
//			}

		}
		//form.addSubmitButton('Release');


		//form.addButton('custpage_release','Display',"nlapiSetFieldValue('custpage_isfromselect','T');NLDoMainFormButtonAction('submitter',true)");

		response.writePage(form);
	}
}

function fillsalesorderField(form, salesorderField,cutomerField,orderdateField,shipdateField,shipmethodField,locationField,maxno){
	var salesorderFilers = new Array();
	salesorderFilers.push(new nlobjSearchFilter('status', null, 'anyof', ['SalesOrd:B','SalesOrd:D','SalesOrd:E']));
	salesorderFilers.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
	if(maxno!=-1)
	{
		salesorderFilers.push(new nlobjSearchFilter('internalidnumber', null, 'lessthan', parseFloat(maxno)));
	}
	var columns=new Array();
	columns[0]=new nlobjSearchColumn('internalid');
	columns[0].setSort(true);
	columns[1]=new nlobjSearchColumn('tranid');
	columns[2]=new nlobjSearchColumn('entity');
	columns[3]=new nlobjSearchColumn('trandate');
	columns[4]=new nlobjSearchColumn('shipmethod');
	columns[5]=new nlobjSearchColumn('location');
	columns[6]=new nlobjSearchColumn('shipdate');


	var customerSearchResults = nlapiSearchRecord('salesorder', null, salesorderFilers,columns);
	nlapiLogExecution('ERROR', 'customerSearchResults.length ',customerSearchResults.length);

	for (var i = 0; customerSearchResults != null && i < customerSearchResults.length; i++) {
		if(customerSearchResults[i].getValue('internalid') != ''){
			salesorderField.addSelectOption(customerSearchResults[i].getValue('internalid'), customerSearchResults[i].getText('internalid'));
		}
		if(customerSearchResults[i].getText('entity')!=''){
			cutomerField.addSelectOption(customerSearchResults[i].getValue('entity'), customerSearchResults[i].getText('entity'));
		}
		if(customerSearchResults[i].getText('shipmethod')!=''){
			shipmethodField.addSelectOption(customerSearchResults[i].getValue('shipmethod'), customerSearchResults[i].getText('shipmethod'));
		}
		if(customerSearchResults[i].getText('location')!=''){
			locationField.addSelectOption(customerSearchResults[i].getValue('location'), customerSearchResults[i].getText('location'));
		}
	}
//	if(customerSearchResults!=null && customerSearchResults.length>=1000)
//	{
//	var column=new Array();
//	column[0]=new nlobjSearchColumn('tranid');		
//	column[1]=new nlobjSearchColumn('internalid');
//	column[1].setSort(true);

//	var OrderSearchResults = nlapiSearchRecord('salesorder', null, salesorderFilers,column);

//	var maxno=OrderSearchResults[OrderSearchResults.length-1].getValue(columns[1]);		
//	fillsalesorderField(form, salesorderField,cutomerField,orderdateField,shipdateField,shipmethodField,locationField,maxno);	
//	}
}

function setPagingForSublist(orderList,form)
{
	if(orderList != null && orderList.length > 0){
		nlapiLogExecution('Error', 'orderList ', orderList.length);
		var orderListArray=new Array();		
		for(k=0;k<orderList.length;k++)
		{
			//nlapiLogExecution('ERROR', 'orderList[k]', orderList[k]); 
			var ordsearchresult = orderList[k];

			if(ordsearchresult!=null)
			{
				nlapiLogExecution('ERROR', 'ordsearchresult.length ', ordsearchresult.length); 
				for(var j=0;j<ordsearchresult.length;j++)
				{
					orderListArray[orderListArray.length]=ordsearchresult[j];				
				}
			}
		}
		nlapiLogExecution('ERROR', 'orderListArray.length ', orderListArray.length); 
		var test='';

		if(orderListArray.length>0)
		{

			if(orderListArray.length>25)
			{
				var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('hidden');
				pagesize.setDisplaySize(10,10);
				pagesize.setLayoutType('outsidebelow', 'startrow');
				var select= form.addField('custpage_selectpage','select', 'Select Records');	
				select.setLayoutType('outsidebelow', 'startrow');			
				select.setDisplaySize(200,30);
				if (request.getMethod() == 'GET'){
					pagesize.setDefaultValue("25");
					pagesizevalue=25;
				}
				else
				{
					if(request.getParameter('custpage_pagesize')!=null)
					{pagesizevalue= request.getParameter('custpage_pagesize');}
					else
					{pagesizevalue= 25;pagesize.setDefaultValue("25");}
				}

				//this is to add the pageno's to the dropdown.
				var len=orderListArray.length/parseInt(pagesizevalue);
				for(var k=1;k<=Math.ceil(len);k++)
				{

					var from;var to;

					to=parseInt(k)*parseInt(pagesizevalue);
					from=(parseInt(to)-parseInt(pagesizevalue))+1;

					if(parseInt(to)>orderListArray.length)
					{
						to=orderListArray.length;
						test=from.toString()+","+to.toString(); 
					}

					var temp=from.toString()+" to "+to.toString();
					var tempto=from.toString()+","+to.toString();
					select.addSelectOption(tempto,temp);

				} 

				if (request.getMethod() == 'POST'){

					if(request.getParameter('custpage_selectpage')!=null ){

						select.setDefaultValue(request.getParameter('custpage_selectpage'));	

					}
					if(request.getParameter('custpage_pagesize')!=null ){

						pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	

					}
				}
			}
			else
			{
				pagesizevalue=parseInt(orderListArray.length);
			}
			//nlapiLogExecution('ERROR', 'pagesizevalue',pagesizevalue);
			//nlapiLogExecution('ERROR', 'orderListArray.length',orderListArray.length);
			var minval=0;var maxval=parseInt(pagesizevalue);
			if(parseInt(pagesizevalue)>orderListArray.length)
			{
				maxval=orderListArray.length;
			}
			//nlapiLogExecution('ERROR', 'maxval',maxval);
			var selectno=request.getParameter('custpage_selectpage');
			//nlapiLogExecution('ERROR', 'selectno',selectno);
			if(selectno!=null )
			{
				var selectedPage= request.getParameter('custpage_selectpage');
				var selectedPageArray=selectedPage.split(',');			
				var diff=parseInt(selectedPageArray[1])-(parseInt(selectedPageArray[0])-1);
				//nlapiLogExecution('ERROR', 'diff',diff);

				var pagevalue=request.getParameter('custpage_pagesize');
				//nlapiLogExecution('ERROR', 'pagevalue',pagevalue);
				if(pagevalue!=null)
				{

					if(parseInt(diff)==parseInt(pagevalue)|| test==selectno)
					{

						var selectedPageArray=selectno.split(',');	
						//nlapiLogExecution('ERROR', 'selectedPageArray.length ', selectedPageArray.length);  
						minval=parseInt(selectedPageArray[0])-1;
						//nlapiLogExecution('ERROR', 'selectedPageArray[0] ', selectedPageArray[0]);  
						maxval=parseInt(selectedPageArray[1]);
						//nlapiLogExecution('ERROR', 'selectedPageArray[1] ', selectedPageArray[1]);  
					}
				}
			}


			var c=1;
			var minvalue;
			minvalue=minval;
			//nlapiLogExecution('ERROR', 'minvalue',minvalue);
			//nlapiLogExecution('ERROR', 'maxval',maxval);
			for(var j = minvalue; j < maxval; j++){		

				var currentOrder = orderListArray[j];				
				addOrderToSublist(form, currentOrder, c);
				c=c+1;
			}
		}
	}
}

var Searchresult=new Array();
function displaySublist(request,maxno)
{

	var filters = new Array();

	nlapiLogExecution('ERROR', 'request.getParameter(custpage_qbso) ',request.getParameter('custpage_qbso'));

	if (request.getParameter('custpage_qbso')!= "" && request.getParameter('custpage_qbso')!= null) {

		if(maxno==0)
		{
			filters.push(new nlobjSearchFilter('internalid', null, 'is', request.getParameter('custpage_qbso')));
		}
		else
		{
			filters.push(new nlobjSearchFilter('internalidnumber', null, 'lessthan', maxno));
		}
	}
	else
	{
		if(maxno!=0)
		{
			filters.push(new nlobjSearchFilter('internalidnumber', null, 'lessthan', maxno));
		}


	}
	nlapiLogExecution('ERROR', 'request.getParameter(custpage_consignee) ',request.getParameter('custpage_consignee'));

	if (request.getParameter('custpage_consignee') != "" && request.getParameter('custpage_consignee')!= null) {

		filters.push(new nlobjSearchFilter('entity', null, 'anyof', request.getParameter('custpage_consignee')));
	}
	nlapiLogExecution('ERROR', 'request.getParameter(custpage_orderdate)',request.getParameter('custpage_orderdate'));

	if (request.getParameter('custpage_orddate')!= "" && request.getParameter('custpage_orddate')!= null) {

		filters.push(new nlobjSearchFilter('trandate', null, 'on', request.getParameter('custpage_orddate')));
	}
	nlapiLogExecution('ERROR', 'request.getParameter(custpage_shipmethod) ',request.getParameter('custpage_shipmethod'));

	if (request.getParameter('custpage_shipmethod') != "" && request.getParameter('custpage_shipmethod')!= null) {

		filters.push(new nlobjSearchFilter('shipmethod', null, 'anyof', request.getParameter('custpage_shipmethod')));
	}
	nlapiLogExecution('ERROR', 'request.getParameter(custpage_location)',request.getParameter('custpage_location'));

	if (request.getParameter('custpage_location') != "" && request.getParameter('custpage_location')!= null) {

		filters.push(new nlobjSearchFilter('location', null, 'anyof', request.getParameter('custpage_location')));
	}
	nlapiLogExecution('ERROR', 'request.getParameter(custpage_shipdate) ',request.getParameter('custpage_shipdate'));

	if (request.getParameter('custpage_shipdate') != "" && request.getParameter('custpage_shipdate')!= null) {

		filters.push(new nlobjSearchFilter('shipdate', null, 'on', request.getParameter('custpage_shipdate')));
	}

	if (request.getParameter('custpage_ordstatus') == "" || request.getParameter('custpage_ordstatus')== null) {

		filters.push(new nlobjSearchFilter('status', null, 'anyof', ['SalesOrd:B','SalesOrd:D','SalesOrd:E']));
	}
	else
	{
		filters.push(new nlobjSearchFilter('status', null, 'anyof', request.getParameter('custpage_ordstatus')));
	}


	//filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('custbody_create_fulfillment_order', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('custrecord_ebizwhsite', 'location', 'is', 'T'));
	//filters.push(new nlobjSearchFilter('custrecord_ns_ord', 'id', 'anyof', ['@NONE@']));

	var columns=new Array();
	columns[0]=new nlobjSearchColumn('internalid',null,'group');
	columns[1]=new nlobjSearchColumn('tranid',null,'group');
	columns[0].setSort(true);
	columns[2]=new nlobjSearchColumn('entity',null,'group');
	columns[3]=new nlobjSearchColumn('trandate',null,'group');
	columns[4]=new nlobjSearchColumn('shipmethod',null,'group');
	columns[5]=new nlobjSearchColumn('location',null,'group');
	columns[6]=new nlobjSearchColumn('shipdate',null,'group');

	//var customerSearchResults = nlapiSearchRecord('salesorder', null, filters,columns);

	var customerSearchResults = nlapiSearchRecord('transaction','customsearch_ebiz_missing_fos', filters, columns);

	if(customerSearchResults!=null && customerSearchResults!='' && customerSearchResults.length>=1000)
	{
		var maxno=customerSearchResults[customerSearchResults.length-1].getId();
		Searchresult.push(customerSearchResults);
		displaySublist(request,maxno);
	}
	else
	{
		Searchresult.push(customerSearchResults);
	}

	return Searchresult;   

}

function addOrderToSublist(form, currentOrder, lineno)
{
	form.getSubList('custpage_sorep').setLineItemValue('custpage_sordrdate', lineno, currentOrder.getValue('trandate',null,'group'));
	form.getSubList('custpage_sorep').setLineItemValue('custpage_so', lineno, currentOrder.getValue('tranid',null,'group'));
	form.getSubList('custpage_sorep').setLineItemValue('custpage_soid', lineno, currentOrder.getValue('internalid',null,'group'));
	form.getSubList('custpage_sorep').setLineItemValue('custpage_scustomer', lineno, currentOrder.getText('entity',null,'group'));
	form.getSubList('custpage_sorep').setLineItemValue('custpage_sshipvia', lineno, currentOrder.getText('shipmethod',null,'group'));
}