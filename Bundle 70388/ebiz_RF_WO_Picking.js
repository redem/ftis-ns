/***************************************************************************
  	eBizNET Solutions Inc             
 ***************************************************************************
 **     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_WO_Picking.js,v $
 *     	   $Revision: 1.1.2.2.4.6.2.1 $
 *     	   $Date: 2014/08/18 12:28:24 $
 *     	   $Author: snimmakayala $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_WO_Picking.js,v $
 * Revision 1.1.2.2.4.6.2.1  2014/08/18 12:28:24  snimmakayala
 * Case: 20149972 & 20149973
 * JAWBONE WO RF PICKING ISSUES
 *
 * Revision 1.1.2.2.4.6  2014/06/13 08:58:24  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.2.4.5  2014/05/30 00:34:24  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.2.4.4  2014/01/30 15:58:51  sponnaganti
 * case# 20127004
 * Form Name is changed correctly , keyboard Enter button working now
 *
 * Revision 1.1.2.2.4.3  2013/11/15 12:14:14  vmandala
 * Case# 201217491
 * PCT Ffx
 *
 * Revision 1.1.2.2.4.2  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.2.4.1  2013/03/08 14:38:41  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.1.2.2  2012/12/03 16:06:19  skreddy
 * CASE201112/CR201113/LOG201121
 * WO Picking
 *
 * Revision 1.1.2.1  2012/11/23 09:10:21  skreddy
 * CASE201112/CR201113/LOG201121
 * RF version of WO Assembly building process
 *
 *
 */

function WOPicking(request, response)
{
	if (request.getMethod() == 'GET') 
	{	
		var functionkeyHtml=getFunctionkeyScript('_ebiz_rf_wo_menu'); 
		//case# 20127004 starts(form name changed and title changed)
		var html = "<html><head><title>WORK ORDER NO</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//	html = html + " document.getElementById('enterwonumber').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_ebiz_rf_wo_menu' method='POST'>";
		//case# 20127004 end
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> WORK ORDER NO";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterwonumber' id='enterwonumber' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> ZONE NO";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterzoneno' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>SEND <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterwonumber').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else
	{
		nlapiLogExecution('DEBUG', 'Into Response','Into Response');

		var vWOnumber = request.getParameter('enterwonumber');
		var optedEvent = request.getParameter('cmdPrevious');
		var getZoneNo =request.getParameter('enterzoneno');
		var WOarray=new Array();
		var vZoneId='';
		if (request.getParameter('cmdPrevious') == 'F7') {
			nlapiLogExecution('DEBUG', 'Clicked on Previous', request.getParameter('cmdPrevious'));
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_menu', 'customdeploy_ebiz_rf_wo_menu_di', false, optedEvent);
		}
		else
			if(vWOnumber != null && vWOnumber !="")
			{
				var vWOId="";
				var WOFilters = new Array();
				var Columns = new Array();
				var WOarray=new Array();


				WOFilters.push(new nlobjSearchFilter('tranid', null, 'is', vWOnumber));
				WOFilters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
				WOFilters.push(new nlobjSearchFilter('custbody_ebiz_wo_qc_status', null, 'is', '2'));

				var SearchResults = nlapiSearchRecord('workorder', null, WOFilters, Columns);
				if(SearchResults != null && SearchResults != '')
				{
					vWOId=SearchResults[0].getId();
				}	
				else
				{
					WOarray["custparam_error"] = 'INVALID WORK ORDER #';
					WOarray["custparam_screenno"] = 'WOPicking';
					nlapiLogExecution('ERROR', 'Error: ', 'Work order Order not found');
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, WOarray);
					return;
				}


				if(getZoneNo != null && getZoneNo != '')
				{
					nlapiLogExecution('DEBUG', 'Into Zone Validate', getZoneNo);
					var ZoneFilters = new Array();
					var ZoneColumns = new Array();

					ZoneFilters.push(new nlobjSearchFilter('custrecord_putzoneid', null, 'is', getZoneNo));
					ZoneFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
					var ZoneSearchResults = nlapiSearchRecord('customrecord_ebiznet_putpick_zone', null, ZoneFilters, ZoneColumns);
					if(ZoneSearchResults != null && ZoneSearchResults != '' && ZoneSearchResults.length>0)
					{
						vZoneId=ZoneSearchResults[0].getId();
					}
					else
					{
						nlapiLogExecution('DEBUG', 'Into Zone Validate Else', 'Into Zone Validate Else');
						var ZoneFilters1 = new Array();
						var ZoneColumns1 = new Array();
						ZoneFilters1.push(new nlobjSearchFilter('name', null, 'is', getZoneNo));
						ZoneFilters1.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
						var ZoneSearchResults1 = nlapiSearchRecord('customrecord_ebiznet_putpick_zone', null, ZoneFilters1, ZoneColumns1);
						if(ZoneSearchResults1 != null && ZoneSearchResults1 != '' && ZoneSearchResults1.length>0)
						{
							vZoneId=ZoneSearchResults1[0].getId();
						}
						else
						{
							WOarray["custparam_error"] = "INVALID ZONE";
							WOarray["custparam_screenno"] = 'WOPicking';
							nlapiLogExecution('DEBUG', 'Error: ', 'Zone not found');
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, WOarray);
							return;
						}
					}
				}


				nlapiLogExecution('ERROR', 'vWOId',vWOId);
				nlapiLogExecution('ERROR', 'vZoneId',vZoneId);
				var filterOpentask = new Array();


				filterOpentask.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
				filterOpentask.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));

				if(vWOId != null && vWOId != "")
				{
					filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vWOId));
				}
				if(vZoneId!=null && vZoneId!="" && vZoneId!="null")
				{
					filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', vZoneId));
				}

				/***  The below code is merged from Endochoice account on 07thMar13  by Santosh  as part of Standard bundle ***/
				var WOcolumns = new Array();
				WOcolumns.push(new nlobjSearchColumn('custrecord_line_no'));
				WOcolumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
				WOcolumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
				WOcolumns.push(new nlobjSearchColumn('custrecord_sku'));
				WOcolumns.push(new nlobjSearchColumn('custrecord_expe_qty'));				
				WOcolumns.push(new nlobjSearchColumn('custrecord_lpno'));
				WOcolumns.push(new nlobjSearchColumn('custrecord_batch_no'));
				WOcolumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
				WOcolumns.push(new nlobjSearchColumn('custrecord_act_qty'));   
				WOcolumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
				WOcolumns.push(new nlobjSearchColumn('custrecord_invref_no'));
				WOcolumns.push(new nlobjSearchColumn('custrecord_wms_location'));
				WOcolumns.push(new nlobjSearchColumn('custrecord_comp_id'));
				WOcolumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));				
				WOcolumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));	
				WOcolumns.push(new nlobjSearchColumn('custrecord_sku_status'));	
				WOcolumns[1].setSort();
				WOcolumns[2].setSort();
				WOcolumns[3].setSort();
				WOcolumns[4].setSort(true);
				var WOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterOpentask,WOcolumns);	
				/*** up to here ***/
				if(WOSearchResults!=null && WOSearchResults!='' && WOSearchResults.length>0)
				{
					var SearchResult = WOSearchResults[0];
					if(WOSearchResults.length > 1)
					{
						var WOSearchnextResult = WOSearchResults[1];
						WOarray["custparam_nextlocation"] = WOSearchnextResult.getText('custrecord_actbeginloc');
						WOarray["custparam_nextiteminternalid"] = WOSearchnextResult.getValue('custrecord_ebiz_sku_no');
						WOarray["custparam_nextitem"] = WOSearchnextResult.getText('custrecord_sku');
						WOarray["custparam_nextserialno"] = WOSearchnextResult.getValue('custrecord_batch_no');
//						nlapiLogExecution('ERROR', 'Next Location', WOarray["custparam_nextlocation"]);
//						nlapiLogExecution('ERROR', 'Next SerialNo', WOarray["custparam_nextserialno"]);
					}

					WOarray["custparam_woid"] =vWOId; 
					WOarray["custparam_iteminternalid"] = SearchResult.getValue('custrecord_ebiz_sku_no');
					WOarray["custparam_item"] = SearchResult.getText('custrecord_sku');
					WOarray["custparam_recordinternalid"] = SearchResult.getId();
					WOarray["custparam_expectedquantity"] = SearchResult.getValue('custrecord_expe_qty');
					WOarray["custparam_beginLocation"] = SearchResult.getValue('custrecord_actbeginloc');
					WOarray["custparam_itemdescription"] = SearchResult.getValue('custrecord_skudesc');
					WOarray["custparam_dolineid"] = SearchResult.getValue('custrecord_ebiz_cntrl_no');
					WOarray["custparam_invoicerefno"] = SearchResult.getValue('custrecord_invref_no');
					WOarray["custparam_orderlineno"] = SearchResult.getValue('custrecord_line_no');
					WOarray["custparam_beginlocationname"] =SearchResult.getText('custrecord_actbeginloc');
					WOarray["custparam_beginLocationname"] =SearchResult.getText('custrecord_actbeginloc');					
					WOarray["custparam_batchno"] = SearchResult.getValue('custrecord_batch_no');
					WOarray["custparam_containerlpno"] = SearchResult.getValue('custrecord_container_lp_no');
					WOarray["custparam_whlocation"] = SearchResult.getValue('custrecord_wms_location');
					WOarray["custparam_whcompany"] = SearchResult.getValue('custrecord_comp_id');
					WOarray["custparam_noofrecords"] = SearchResult.length;
					WOarray["custparam_ebizordno"] =  SearchResult.getValue('custrecord_ebiz_order_no');
					WOarray["custparam_itemstatus"] =  SearchResult.getValue('custrecord_sku_status');
					//nlapiLogExecution('ERROR', 'Binlocation',SearchResult.getText('custrecord_actbeginloc') );
					WOarray["custparam_ebizzoneno"] =vZoneId; 

					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_summary', 'customdeploy_ebiz_rf_wo_summary_di', false, WOarray);
					//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_location', 'customdeploy_ebiz_rf_wo_picking_loc_di', false, WOarray);
				}
				else
				{
					//Case 201217491 start
					var filter =new Array();
					if(vWOId != null && vWOId != "")
					{
						filter.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vWOId));
					}				
					var SearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filter);
					if(SearchResults!=null && SearchResults!='' && SearchResults.length>0)
					{
						WOarray["custparam_screenno"] = 'WOPicking';
						WOarray["custparam_error"] = "THIS WORK ORDER # IS ALREADY PROCESSED";
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, WOarray);
					}
					else
					{
						WOarray["custparam_screenno"] = 'WOPicking';
						WOarray["custparam_error"] = "BIN LOCATIONS HAVE NOT BEEN GENERATED FOR THIS WORK ORDER #";
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, WOarray);
					}
					//case 201217491 end

				}
			}
			else
			{

				WOarray["custparam_screenno"] = 'WOPicking';
				WOarray["custparam_error"] = "ENTER WORK ORDER #";
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, WOarray);
			}
		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}
}