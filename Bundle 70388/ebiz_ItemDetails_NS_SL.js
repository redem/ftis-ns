/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *             $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_ItemDetails_NS_SL.js,v $
 *             $Revision: 1.1.2.2.4.2 $
 *             $Date: 2013/03/19 12:11:05 $
 *             $Author: schepuri $
 *             $Name: t_NSWMS_2013_1_3_9 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *    Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_ItemDetails_NS_SL.js,v $
 * Revision 1.1.2.2.4.2  2013/03/19 12:11:05  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.1.2.2.4.1  2013/03/19 11:46:05  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.1.2.2  2012/10/30 06:11:33  mbpragada
 * 20120490
 * Brand mapping changed
 *
 * Revision 1.1.2.1  2012/10/19 10:19:39  mbpragada
 * 20120490
 * ItemDetails new files
 *

 *****************************************************************************/

function ebiz_Itemdetails_SL(request, response)
{
	if (request.getMethod() == 'GET')
	{
		var form = nlapiCreateForm('Item Details');	
		form.setScript('customscript_ebiz_itemdetails_cl');
		var vSerialno=form.addField('custpage_serialno', 'text', 'Serial#');
		var vUPCcode=form.addField('custpage_upccode', 'text', 'UPC');
		var vScanItemFld=form.addField('custpage_scanitem', 'text', 'Item');		

		form.addSubmitButton('Display');

		response.writePage(form);
	}
	else
	{
		var form = nlapiCreateForm('Item Details');
		form.setScript('customscript_ebiz_itemdetails_cl');
		var serialno = request.getParameter('custpage_serialno');
		var UPCcode = request.getParameter('custpage_upccode');
		var Item = request.getParameter('custpage_scanitem');
		nlapiLogExecution('ERROR','Item',Item);

		var vSerialno=form.addField('custpage_serialno', 'text', 'Serial#');
		var vUPCcode=form.addField('custpage_upccode', 'text', 'UPC');
		var vScanItemFld=form.addField('custpage_scanitem', 'text', 'Item');
		vSerialno.setDefaultValue(serialno);
		vUPCcode.setDefaultValue(UPCcode);
		vScanItemFld.setDefaultValue(Item);

		form.addSubmitButton('Display');
//		sublist1
		var sublist = form.addSubList("custpage_items", "staticlist", "Item");
		sublist.addField('custpage_itemname', 'text', 'Item');
		sublist.addField('custpage_desc', 'text', 'Description');
		sublist.addField('custpage_brand', 'text', 'Brand');
		sublist.addField('custpage_color', 'text', 'Color');
		sublist.addField('custpage_size', 'text', 'Size');
		//sublist.addField('custpage_image', 'image', 'Size');
		var vitemimage=form.addField('custpage_image', 'inlinehtml', 'Item Image');//.setLayoutType('outsidebelow', 'startcol');	

		//sublist2
		var sublist1 = form.addSubList("custpage_items1", "staticlist", "Serial number");
		sublist1.addField('custpage_serail', 'text', 'Serial#');
		sublist1.addField('custpage_item', 'text', 'Item');
		sublist1.addField('custpage_supplier', 'text', 'Supplier');
		sublist1.addField('custpage_po', 'text', 'PO');
		sublist1.addField('custpage_location', 'text', 'Location');
		sublist1.addField('custpage_status', 'text', 'Status');
		//sublist1.addField('custpage_so', 'text', 'SO');
		sublist1.addField('custpage_date', 'text', 'Date Updated');
		sublist1.addField('custpage_user', 'text', 'User Updated');

		var sublist2 = form.addSubList("custpage_items2", "staticlist", "Other Serial No's");		
		sublist2.addField('custpage_serail1', 'text', 'Serial#');
		sublist2.addField('custpage_item1', 'text', 'Item');
		sublist2.addField('custpage_supplier1', 'text', 'Supplier');
		sublist2.addField('custpage_po1', 'text', 'PO');
		sublist2.addField('custpage_location1', 'text', 'Location');
		sublist2.addField('custpage_status1', 'text', 'Status');
		//sublist2.addField('custpage_so1', 'text', 'SO');
		sublist2.addField('custpage_date1', 'text', 'Date Updated');
		sublist2.addField('custpage_user1', 'text', 'User Updated');
		var Iteminternalid= "";
		var serialnumber=  "";
		var serialpono=	 "";
		var seriallocation="";
		var serialwmsstatus= "";
		var serialsono=	 "",supplier="", getItemintenalid="";
		if(UPCcode !="" && UPCcode !=""){
			Item = GetItemBasedOnUPCCode(UPCcode);
		}
		if(Item !="" && Item !=null){
			Iteminternalid=Getinternalid(Item);
		}
		nlapiLogExecution('ERROR','Iteminternalid',Iteminternalid);
		if(serialno!=null && serialno!='')
		{
			var serialitemfilters = new Array();

			serialitemfilters.push(new nlobjSearchFilter('custrecord_serialnumber',null, 'is',serialno));				
			serialitemfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));

			var serialLocCol = new Array();                 
			serialLocCol[0] = new nlobjSearchColumn('custrecord_serialitem');
			serialLocCol[1] = new nlobjSearchColumn('custrecord_serialnumber').setSort();
			serialLocCol[2] = new nlobjSearchColumn('custrecord_serialpono');
			serialLocCol[3] = new nlobjSearchColumn('custrecord_serialbinlocation');
			serialLocCol[4] = new nlobjSearchColumn('custrecord_serialwmsstatus');
			serialLocCol[5] = new nlobjSearchColumn('custrecord_serialsono');
			serialLocCol[6] = new nlobjSearchColumn('custrecord_ebiz_serial_supplier');



			var serialitemRes = nlapiSearchRecord('customrecord_ebiznetserialentry',null,serialitemfilters,serialLocCol);
			if(serialitemRes!=null && serialitemRes!='')
			{
				Item=serialitemRes[0].getText('custrecord_serialitem');
				Iteminternalid=serialitemRes[0].getValue('custrecord_serialitem');
				serialnumber=serialitemRes[0].getValue('custrecord_serialnumber');
				serialpono=serialitemRes[0].getValue('custrecord_serialpono');
				seriallocation=serialitemRes[0].getText('custrecord_serialbinlocation');
				serialwmsstatus=serialitemRes[0].getText('custrecord_serialwmsstatus');
				serialsono=serialitemRes[0].getValue('custrecord_serialsono');
				supplier=serialitemRes[0].getText('custrecord_ebiz_serial_supplier');
			}
		}
		var GetAllItemdetailsfrmSerialRes = GetAllItemdetailsfrmSerialEntry(Item);
		nlapiLogExecution('ERROR','GetAllItemdetailsfrmSerialRes',GetAllItemdetailsfrmSerialRes);
		var stringvalues="";
		if(Iteminternalid!=null && Iteminternalid!='')
		{
			nlapiLogExecution('ERROR','Iteminternalid','done');
			var Itype = nlapiLookupField('item', Iteminternalid, 'recordType');
			var ItemRec = nlapiLoadRecord(Itype, Iteminternalid);		
			var updateddate=ItemRec.getFieldValue('lastmodifieddate');
			var serialnocount=ItemRec.getLineItemCount('recmachcustrecord_ebiz_inv_sku');
			for (var b = 1; b <= serialnocount; b++){
				//stringvalues =ItemRec.getLineItemValue('numbers','serialnumber',b);
				stringvalues =ItemRec.getLineItemValue('recmachcustrecord_ebiz_inv_sku','custrecord_ebiz_serialnumbers',b);

				IndividualItemdetailsfrmSerial = GetIndividualInfo(GetAllItemdetailsfrmSerialRes, stringvalues);	
				if(IndividualItemdetailsfrmSerial !="")
				{
					var Item1=IndividualItemdetailsfrmSerial[0][0];
					var serialpono1=IndividualItemdetailsfrmSerial[0][3];
					var seriallocation1=IndividualItemdetailsfrmSerial[0][4];
					var serialwmsstatus1=IndividualItemdetailsfrmSerial[0][5];
					var serialsono1=IndividualItemdetailsfrmSerial[0][6];
					var serialnumber1=IndividualItemdetailsfrmSerial[0][2];
					var supplier1=IndividualItemdetailsfrmSerial[0][7];
				}

				//form.getSubList('custpage_items2').setLineItemValue('custpage_otherserialnos', b, stringvalues);

				form.getSubList('custpage_items2').setLineItemValue('custpage_serail1', b, serialnumber1);
				form.getSubList('custpage_items2').setLineItemValue('custpage_item1', b, Item1);
				form.getSubList('custpage_items2').setLineItemValue('custpage_supplier1', b, supplier1);
				form.getSubList('custpage_items2').setLineItemValue('custpage_po1', b, serialpono1);
				form.getSubList('custpage_items2').setLineItemValue('custpage_location1', b, seriallocation1);
				form.getSubList('custpage_items2').setLineItemValue('custpage_status1', b, serialwmsstatus1);
				form.getSubList('custpage_items2').setLineItemValue('custpage_so1', b, serialsono1);
				form.getSubList('custpage_items2').setLineItemValue('custpage_date1', b, updateddate);
				form.getSubList('custpage_items2').setLineItemValue('custpage_user1', b, "");
			}
		}

		var vItemArr = validateSKUId(Item);

		var vItemType="";
		var currItem="";
		var brand="";var Desc="",size="",color="",itemname="";
		nlapiLogExecution('ERROR','vItemArr',vItemArr);
		if(vItemArr != null && vItemArr != '' )
		{
			vItemType=vItemArr[0];
			currItem=vItemArr[1];
			brand=vItemArr[2];
			Desc=vItemArr[3];
			size=vItemArr[4];
			color=vItemArr[5];
			itemname=vItemArr[6];
		}
		if(brand ==null || brand == "")
			brand ="";
		nlapiLogExecution('ERROR','currItem',currItem);
		if(currItem!=null && currItem!='')
		{
			nlapiLogExecution('ERROR','currItem',currItem);

			var url;
			/*var ctx = nlapiGetContext();
			nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
			if (ctx.getEnvironment() == 'PRODUCTION') 
			{
				url = 'https://system.netsuite.com';                  
			}
			else if (ctx.getEnvironment() == 'SANDBOX') 
			{
				url = 'https://system.sandbox.netsuite.com';                      
			}*/
			nlapiLogExecution('ERROR','url',url);
			var file = currItem;
			var html = "<html><body>";
			html=html +"<img src='" + file + "' height='15%' width='50%'></img>";
			html = html + "</head></body>";

			vitemimage.setDefaultValue(html);
			//vmodelno.setDefaultValue(brand);

		}
		form.getSubList('custpage_items').setLineItemValue('custpage_desc', 1, Desc);
		form.getSubList('custpage_items').setLineItemValue('custpage_brand', 1, brand);
		form.getSubList('custpage_items').setLineItemValue('custpage_color', 1, color);
		form.getSubList('custpage_items').setLineItemValue('custpage_size', 1, size);
		form.getSubList('custpage_items').setLineItemValue('custpage_itemname', 1, itemname);


		form.getSubList('custpage_items1').setLineItemValue('custpage_serail', 1, serialnumber);
		form.getSubList('custpage_items1').setLineItemValue('custpage_item', 1, Item);
		form.getSubList('custpage_items1').setLineItemValue('custpage_supplier', 1, supplier);
		form.getSubList('custpage_items1').setLineItemValue('custpage_po', 1, serialpono);
		form.getSubList('custpage_items1').setLineItemValue('custpage_location', 1, seriallocation);
		form.getSubList('custpage_items1').setLineItemValue('custpage_status', 1, serialwmsstatus);
		form.getSubList('custpage_items1').setLineItemValue('custpage_so', 1, serialsono);
		form.getSubList('custpage_items1').setLineItemValue('custpage_date', 1, "");
		form.getSubList('custpage_items1').setLineItemValue('custpage_user', 1, "");
		response.writePage(form);
	}
}


function validateSKUId(itemNo)
{
	nlapiLogExecution('Error', 'Into validateSKU',itemNo);

	var actItem=new Array();

	var invitemfilters = new Array();
	invitemfilters.push(new nlobjSearchFilter('nameinternal',null, 'is',itemNo)); 
	invitemfilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));

	var invLocCol = new Array();  
	invLocCol[0] = new nlobjSearchColumn('storedisplayimage');  
	invLocCol[1] = new nlobjSearchColumn('displayname');
	invLocCol[2] = new nlobjSearchColumn('custitem_shoe_size');
	invLocCol[3] = new nlobjSearchColumn('custitem_brand');
	invLocCol[4] = new nlobjSearchColumn('custitem_bobcolor');
	invLocCol[5] = new nlobjSearchColumn('itemid');

	var invitemRes = nlapiSearchRecord('item',null,invitemfilters,invLocCol);
	if(invitemRes!=null && invitemRes!='')
	{
		actItem[0]=invitemRes[0].getId();
		actItem[1]=invitemRes[0].getText('storedisplayimage');
		actItem[2]=invitemRes[0].getText('custitem_brand');
		actItem[3]=invitemRes[0].getValue('displayname');

		actItem[4]=invitemRes[0].getText('custitem_shoe_size');
		actItem[5]=invitemRes[0].getValue('custitem_bobcolor');
		actItem[6]=invitemRes[0].getValue('itemid');


	}
	else
	{
		nlapiLogExecution('ERROR', 'inSide UPCCODE',itemNo);

		var invLocfilters = new Array();
		invLocfilters[0] = new nlobjSearchFilter('upccode', null, 'is', itemNo);

		var invtRes = nlapiSearchRecord('item', null, invLocfilters, invLocCol);

		if(invtRes!=null && invtRes!='')
		{
			//actItem=invtRes[0].getValue('externalid');
			actItem[0]=invtRes[0].getId();
			actItem[1]=invtRes[0].getText('custrecord_ebiz_item');
			actItem[3]=invtRes[0].getValue('displayname');
		}
		else
		{
			nlapiLogExecution('ERROR', 'inSide SKUALIAS',itemNo);

			var skuAliasFilters = new Array();
			skuAliasFilters.push(new nlobjSearchFilter('name', null, 'is', itemNo));

			var skuAliasCols = new Array();
			skuAliasCols[0] = new nlobjSearchColumn('custrecord_ebiz_item');

			var skuAliasResults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, skuAliasFilters, skuAliasCols);

			if(skuAliasResults != null)
			{
				actItem=skuAliasResults[0].getValue('custrecord_ebiz_item');
				/*var Itype = nlapiLookupField('item', actItem, 'recordType');
                        var ItemRec = nlapiLoadRecord(Itype, actItem);              
                        actItem=    ItemRec.getFieldValue('itemid');*/
				actItem[3]=skuAliasResults[0].getValue('displayname');
			}

		}                 
	}

	nlapiLogExecution('Error', 'Out of validateSKU',actItem);

	return actItem;
}
function GetItemBasedOnUPCCode(itemNo){
	nlapiLogExecution('ERROR', 'GetItemBasedOnUPCCode', 'Start');

	var currItem = "";

	var filters = new Array();
	filters.push(new nlobjSearchFilter('upccode', null, 'is', itemNo));	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('itemid');
	columns[0].setSort(true);

	var itemSearchResults = nlapiSearchRecord('item', null, filters, columns);

	if(itemSearchResults != null)
		currItem = itemSearchResults[0].getValue('itemid');

	nlapiLogExecution('ERROR', 'currItem', currItem);


	return currItem;
}
function Getinternalid(itemNo){
	nlapiLogExecution('ERROR', 'Getinternalid', 'Start');
	var internalidval = "";

	var filters = new Array();
	filters.push(new nlobjSearchFilter('itemid', null, 'is', itemNo));	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	columns[0].setSort(true);

	var itemSearchResults = nlapiSearchRecord('item', null, filters, columns);

	if(itemSearchResults != null)
		internalidval = itemSearchResults[0].getValue('internalid');

	nlapiLogExecution('ERROR', 'internalidval', internalidval);


	return internalidval;
}

function GetIndividualInfo(itemlist, seialno ){
	var IndividuaitemInfoList = new Array();
	var IndividualitemInfoListT = new Array();	
	try{
		nlapiLogExecution('ERROR', 'seialno', seialno);
		//nlapiLogExecution('ERROR', 'itemlist[i][2]', itemlist[i][2]);
		if (itemlist != null && itemlist.length >0 && seialno != null && seialno.length>0){
			for ( var i = 0 ; i < itemlist.length ; i++){
				nlapiLogExecution('ERROR', 'itemlist[i][2]', itemlist[i][2]);
				if (itemlist[i][2]  == seialno )
				{
					IndividuaitemInfoList = new Array();					
					IndividuaitemInfoList[0] = itemlist[i][0]; 	
					IndividuaitemInfoList[1] = itemlist[i][1];
					IndividuaitemInfoList[2] = itemlist[i][2];
					IndividuaitemInfoList[3] = itemlist[i][3];
					IndividuaitemInfoList[4] = itemlist[i][4];
					IndividuaitemInfoList[5] = itemlist[i][5];
					IndividuaitemInfoList[6] = itemlist[i][6];
					IndividuaitemInfoList[7] = itemlist[i][7];

					IndividualitemInfoListT.push(IndividuaitemInfoList);
					i = itemlist.length; 	
				}
			}
		}
	}
	catch(exception){
		nlapiLogExecution('ERROR', 'GetIndividualInfo', exception);
	}
	return IndividualitemInfoListT;
}
function GetAllItemdetailsfrmSerialEntry(Item){
	nlapiLogExecution('ERROR', 'GetAllItemdetailsfrmSerialEntry', 'Start');

	var AllDetailsListT = new Array();

	var DetailsListT = new Array();
	var filters = new Array();
	var columns = new Array();
	try
	{			

		filters.push(new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', Item));

		columns[0] = new nlobjSearchColumn('custrecord_serialitem');
		columns[1] = new nlobjSearchColumn('custrecord_serialnumber').setSort();
		columns[2] = new nlobjSearchColumn('custrecord_serialpono');
		columns[3] = new nlobjSearchColumn('custrecord_serialbinlocation');
		columns[4] = new nlobjSearchColumn('custrecord_serialwmsstatus');
		columns[5] = new nlobjSearchColumn('custrecord_serialsono');	
		columns[6] = new nlobjSearchColumn('custrecord_ebiz_serial_supplier');

		var Results = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
		if(Results != null)
			nlapiLogExecution('ERROR', 'Results.length', Results.length);
		if(Results != null && Results.length > 0){
			for(var i = 0; i < Results.length; i++ ){
				DetailsListT = new Array();		

				DetailsListT[0]=Results[i].getText('custrecord_serialitem');
				DetailsListT[1]=Results[i].getValue('custrecord_serialitem');
				DetailsListT[2]=Results[i].getValue('custrecord_serialnumber');
				DetailsListT[3]=Results[i].getValue('custrecord_serialpono');
				DetailsListT[4]=Results[i].getText('custrecord_serialbinlocation');
				DetailsListT[5]=Results[i].getText('custrecord_serialwmsstatus');
				DetailsListT[6]=Results[i].getValue('custrecord_serialsono');
				DetailsListT[7]=Results[i].getText('custrecord_ebiz_serial_supplier');

				AllDetailsListT.push (DetailsListT);
			}
		}
	}
	catch(exception){
		nlapiLogExecution('ERROR', 'GetAllItemdetailsfrmSerialEntry', exception);
	}			
	return AllDetailsListT;
}