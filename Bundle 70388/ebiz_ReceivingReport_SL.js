
function ebiznet_ReceivingReport(request, response){
	if (request.getMethod() == 'GET') {


		var form = nlapiCreateForm('Receiving Report');
		var vorder = form.addField('custpage_po', 'text', 'PO / TO / RMA #');
		/*var OrderType = form.addField('custpage_ordertype', 'select', 'Order Type').setMandatory(true);
		OrderType.addSelectOption("","");
		OrderType.addSelectOption("purchaseorder","PO");
		OrderType.addSelectOption("transferorder","TO");
		OrderType.addSelectOption("returnauthorization","RMA");

		if(request.getParameter('custpage_ordertype')!='' && request.getParameter('custpage_ordertype')!=null)
		{
			OrderType.setDefaultValue(request.getParameter('custpage_ordertype'));	
		}
		 */
		var button = form.addSubmitButton('Display');
		response.writePage(form);       

	}
	else {

		var form = nlapiCreateForm('Receiving Report');


		var vorderid = request.getParameter('custpage_po');
		var vorder = form.addField('custpage_po', 'text', 'PO / TO / RMA #');
		vorder.setDefaultValue(vorderid);
		var ordtype=request.getParameter('custpage_ordertype');


		var vordernew = GetPOInternalId(vorderid);

		if(vordernew != null && vordernew != "")
		{


			form.setScript('customscript_receivingreport_sl');
			form.addButton('custpage_print','Print','Printreport('+vordernew+')');

			/*var OrderType = form.addField('custpage_ordertype', 'select', 'Order Type').setMandatory(true);
			OrderType.addSelectOption("","");
			OrderType.addSelectOption("purchaseorder","PO");
			OrderType.addSelectOption("transferorder","TO");
			OrderType.addSelectOption("returnauthorization","RMA");
			if(request.getParameter('custpage_ordertype')!='' && request.getParameter('custpage_ordertype')!=null)
			{
				OrderType.setDefaultValue(request.getParameter('custpage_ordertype'));	
			}*/

			nlapiLogExecution('ERROR', 'vorderid',vorderid);
			var button = form.addSubmitButton('Display');


			var sublist = form.addSubList("custpage_items", "list", "Receiving Report");
			sublist.addField("custpage_line", "text", "Line#");
			sublist.addField("custpage_ord", "text", "Transaction#");
			sublist.addField("custpage_itemname", "select", "Item", "item").setDisplayType('inline');
			sublist.addField("custpage_itemdesc", "text", "Item Description");			
			sublist.addField("custpage_upc", "text", "UPC Code");
			sublist.addField("custpage_quantity", "text", "Order Qty");
			sublist.addField("custpage_rcvquantity", "text", "Qty Received");
			sublist.addField("custpage_remquantity", "text", "Qty Remaining to be Received");			

			nlapiLogExecution('ERROR', 'PO', request.getParameter('custpage_po'));					



			var trantype = nlapiLookupField('transaction', vordernew, 'recordType');
			nlapiLogExecution('ERROR', 'trantype', trantype);

			/*	var transaction;
			if(trantype=='purchaseorder')
			{
				transaction = nlapiLoadRecord('purchaseorder', vordernew);
			}
			else if(trantype=='returnauthorization')
			{
				transaction = nlapiLoadRecord('returnauthorization', vordernew);
			}
			else 
			{
				transaction = nlapiLoadRecord('transferorder', vordernew);
			}*/


			nlapiLogExecution('ERROR', 'request.getParameter(' + 'custpage_po' + ')', request.getParameter('custpage_po'));
			var polinefilters = new Array();
			polinefilters[0] = new nlobjSearchFilter('internalid', null, 'anyof', vordernew);			
			polinefilters[1] = new nlobjSearchFilter('billable', null, 'is', 'false');		
			polinefilters[2] = new nlobjSearchFilter('item', null, 'noneof','@NONE@');

			var polinecolumns = new Array();
			polinecolumns[0] = new nlobjSearchColumn('line');
			polinecolumns[1] = new nlobjSearchColumn('item');//,
			polinecolumns[2] = new nlobjSearchColumn('quantity');
			polinecolumns[3] = new nlobjSearchColumn('custcol_ebiznet_item_status');
			polinecolumns[4] = new nlobjSearchColumn('formulanumeric');
			polinecolumns[4].setFormula('ABS({quantity})');
			polinecolumns[5] = new nlobjSearchColumn('description','item');
			polinecolumns[6] = new nlobjSearchColumn('upccode','item');
			polinecolumns[7] = new nlobjSearchColumn('tranid');

			var posearchresults;
			if(trantype=='purchaseorder')
			{
				nlapiLogExecution('ERROR', 'purchaseorder', 'purchaseorder');
				// To filter null value records from the search results
				posearchresults=nlapiSearchRecord('purchaseorder', null, polinefilters, polinecolumns);
			}

			if(trantype=='transferorder')
			{
				nlapiLogExecution('ERROR', 'transferorder', 'transferorder');
				polinefilters[3] = new nlobjSearchFilter('transactionlinetype', null, 'anyof', 'ITEM');
				posearchresults=nlapiSearchRecord('transferorder',null,polinefilters,polinecolumns);
			}

			if(trantype=='returnauthorization')
			{
				nlapiLogExecution('ERROR', 'returnauthorization', 'returnauthorization');
				polinefilters[3] = new nlobjSearchFilter('taxline', null, 'is', 'F');
				posearchresults=nlapiSearchRecord('returnauthorization',null,polinefilters,polinecolumns);
			}

			var line, itemname, itemstatus, uom, ordqty, pconfqty,exceptionqty,orderno;

			var poqty,poitemdesc,poitemupc,poitemname;
			nlapiLogExecution('ERROR', 'posearchresults.length', posearchresults.length);
			if (posearchresults != null)
			{            	
				for (var i = 0; i < posearchresults.length; i++)  // Even though PO is having one line, search results are giving two records, assumtion is first record might be expense record 
				{     		
					nlapiLogExecution('ERROR', 'posearchresults.length', posearchresults.length);
					line = posearchresults[i].getValue('line');
					poitemname = posearchresults[i].getValue('item');
					poqty = posearchresults[i].getValue('formulanumeric');					
					nlapiLogExecution('ERROR', 'poqty', poqty);
					itemstatus = posearchresults[i].getValue('custcol_ebiznet_item_status');
					poitemdesc = posearchresults[i].getValue('description','item');
					poitemupc = posearchresults[i].getValue('upccode','item');
					//trsntype = posearchresults[i].getValue('item')
					orderno = posearchresults[i].getValue('tranid');

					nlapiLogExecution('ERROR', 'SKU Status', itemstatus);
					nlapiLogExecution('ERROR', 'vorderid', vorderid);
					nlapiLogExecution('ERROR', 'line', line);



					var filters = new Array();
					filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', vordernew));		
					filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_putgen_qty', null, 'greaterthan', 0));
					filters.push(new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', line));

					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_orderline_no');
					columns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_item');					
					columns[2] = new nlobjSearchColumn('custrecord_orderlinedetails_order_qty');
					columns[3] = new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty');
					columns[4] = new nlobjSearchColumn('custrecord_orderlinedetails_putgen_qty');
					columns[5] = new nlobjSearchColumn('custrecord_orderlinedetails_putconf_qty');
					columns[6] = new nlobjSearchColumn('custrecord_orderlinedetails_putexcep_qty');
					columns[7] = new nlobjSearchColumn('description','custrecord_orderlinedetails_item');
					columns[8] = new nlobjSearchColumn('upccode','custrecord_orderlinedetails_item');					
					var searchresults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, filters, columns);

					var	ordqty=0;
					var	premqty=0;
					var pconfqty=0;
					var itemname;
					var putgenqty;
					var itemdesc;
					var upccode;
					var recvqty = 0;
					var pexceqty=0;
					if (searchresults !=null )
					{
						for (var j = 0; j < searchresults.length; j++) 
						{

							nlapiLogExecution('ERROR', 'searchresults.length', searchresults.length);							
							var searchresult = searchresults[j];							
							ordqty = searchresult.getValue('custrecord_orderlinedetails_order_qty');
							putgenqty = searchresult.getValue('custrecord_orderlinedetails_putgen_qty');
							pconfqty = searchresult.getValue('custrecord_orderlinedetails_putconf_qty');
							pexceqty=searchresult.getValue('custrecord_orderlinedetails_putexcep_qty');
							//case# 20148732 starts
							//itemname = searchresult.getValue('custrecord_orderlinedetails_item');
							//itemdesc = searchresult.getValue('description','custrecord_orderlinedetails_item');
							//upccode = searchresult.getValue('upccode','custrecord_orderlinedetails_item');
							//case# 20148732 ends
							recvqty = searchresult.getValue('custrecord_orderlinedetails_checkin_qty');
							nlapiLogExecution('ERROR', 'ordqty', ordqty);
							nlapiLogExecution('ERROR', 'pconfqty', pconfqty);

							nlapiLogExecution('ERROR', 'itemname', itemname);
							nlapiLogExecution('ERROR', 'itemdesc', itemdesc);
							nlapiLogExecution('ERROR', 'upccode', upccode);	


						}
					}





					nlapiLogExecution('ERROR', 'poqty', poqty);
					nlapiLogExecution('ERROR', 'poitemname', poitemname);
					nlapiLogExecution('ERROR', 'poitemdesc', poitemdesc);
					nlapiLogExecution('ERROR', 'poitemupc', poitemupc);

					nlapiLogExecution('ERROR', 'recvqty', recvqty);
					if(recvqty==null || recvqty=='')
						recvqty = 0;

					if(pconfqty==null || pconfqty=='')
						pconfqty = 0;

					if(pexceqty==null || pexceqty=='')
						pexceqty = 0;

					nlapiLogExecution('ERROR', 'ordqty', ordqty);
					if(ordqty==null || ordqty =='')
						ordqty = poqty;
//					case# 201412500
					if(putgenqty==null || putgenqty=='')
						putgenqty = 0;
					premqty = parseInt(ordqty) - parseInt(recvqty);
					//premqty = parseInt(ordqty) - parseInt(pconfqty);
					nlapiLogExecution('ERROR', 'premqty', premqty);

					/*	if(premqty < 0 || isNaN(premqty) || premqty==null || premqty=='')
					{
						premqty=0;
					}*/
					nlapiLogExecution('ERROR', 'itemname', itemname);
					if(itemname==null || itemname=='')						
						itemname = poitemname;
					nlapiLogExecution('ERROR', 'itemname here', itemname);
					nlapiLogExecution('ERROR', 'itemdesc', itemdesc);
					if(itemdesc==null || itemdesc=='')						
						itemdesc = poitemdesc;

					nlapiLogExecution('ERROR', 'upccode', upccode);
					if(upccode==null || upccode=='')						
						upccode = poitemupc;



					form.getSubList('custpage_items').setLineItemValue('custpage_line', i+1, line);
					if(trantype=='purchaseorder'){
						form.getSubList('custpage_items').setLineItemValue('custpage_ord', i+1,'PO-'+orderno);
					}
					if(trantype=='returnauthorization'){
						form.getSubList('custpage_items').setLineItemValue('custpage_ord', i+1,'RA-'+orderno);
					}
					if(trantype=='transferorder'){
						form.getSubList('custpage_items').setLineItemValue('custpage_ord', i+1,'TO-'+orderno);
					}
				
					
					

					form.getSubList('custpage_items').setLineItemValue('custpage_itemname', i+1, poitemname);
					if(poitemdesc != null)
						form.getSubList('custpage_items').setLineItemValue('custpage_itemdesc', i+1, poitemdesc);
					if(poitemupc != null)
						form.getSubList('custpage_items').setLineItemValue('custpage_upc', i+1, poitemupc);

					if(ordqty != null)
						form.getSubList('custpage_items').setLineItemValue('custpage_quantity', i+1, poqty);
					//case# 20148714 starts
					//case# 20148714 starts
					if(recvqty != null)
						form.getSubList('custpage_items').setLineItemValue('custpage_rcvquantity', i+1, recvqty.toString());
					/*if(pconfqty != null)
						form.getSubList('custpage_items').setLineItemValue('custpage_rcvquantity', i+1, pconfqty.toString());*/


					if(premqty != null)
						form.getSubList('custpage_items').setLineItemValue('custpage_remquantity', i+1, premqty.toString());		
					/*if(pexceqty != null)
						form.getSubList('custpage_items').setLineItemValue('custpage_remquantity', i+1, pexceqty.toString());*/
					//case# 20148714 ends
				}

			}
		}
		else
		{
			var msg= form.addField('custpage_message', 'inlinehtml', null, null, null);
			var msgtext = "Invalid Order# : "+ vorderid;
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', '"+ msgtext + "', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
		}

		response.writePage(form);


	}

}


function GetPOInternalId(POid)
{
	var purchaseorderFilers = new Array();
	purchaseorderFilers.push(new nlobjSearchFilter('mainline', null, 'is', 'T')); 
	purchaseorderFilers.push(new nlobjSearchFilter('tranid', null, 'is', POid)); 
	//salesorderFilers.push(new nlobjSearchFilter('line', null, 'is', ordline));
	var customerSearchResults = nlapiSearchRecord('purchaseorder', null, purchaseorderFilers,null);	
	if(customerSearchResults==null)
	{
		nlapiLogExecution('ERROR', 'returnauthorization','returnauthorization');
		customerSearchResults=nlapiSearchRecord('returnauthorization',null,purchaseorderFilers,null);
	}
	if(customerSearchResults==null)
	{
		nlapiLogExecution('ERROR', 'transferorder','transferorder');
		customerSearchResults=nlapiSearchRecord('transferorder',null,purchaseorderFilers,null);
	}

	if(customerSearchResults != null && customerSearchResults != "")
	{
		nlapiLogExecution('ERROR', 'customerSearchResults.length ',customerSearchResults.length);
		return customerSearchResults[0].getId();
	}
	else
		return null;
}

/*function GetPOInternalId(POText,ordtype)
{
	nlapiLogExecution('ERROR','Into GetSOInternalId (Input)',POText);
	//nlapiLogExecution('ERROR','ordertype',ordertype);

	var ActualPOID='-1';

	var filter=new Array();
	filter.push(new nlobjSearchFilter('tranid',null,'is',POText.toString()));
	filter.push(new nlobjSearchFilter('mainline',null,'is','F'));
	var RoleLocation=getRoledBasedLocation();
	nlapiLogExecution('ERROR', 'RoleLocation', RoleLocation);
	if(RoleLocation != null && RoleLocation != '' && RoleLocation != 0)
	{
		filter.push(new nlobjSearchFilter('location', null, 'anyof', RoleLocation));
	}
	nlapiLogExecution('ERROR', 'ordtype', ordtype);


	var columns=new Array();
	//columns.push(new nlobjSearchColumn('shipmethod'));

	var searchrec=nlapiSearchRecord(ordtype,null,filter,columns);
	if(searchrec==null)
	{
		searchrec=nlapiSearchRecord('transferorder',null,filter,columns);
	}
	if(searchrec!=null && searchrec!='' && searchrec.length>0)
	{
		ActualPOID=searchrec[0].getId();
	}

	nlapiLogExecution('ERROR','Out of GetSOInternalId (Output)',ActualPOID);

	return ActualPOID;
}
 */


function Printreport(vordernew){	

	nlapiLogExecution('ERROR', 'into Printreport vordernew',vordernew);
	var vordername = nlapiGetFieldValue('custpage_po');
	nlapiLogExecution('ERROR', 'into Printreport vordername',vordername);
	var ReceivingPDFURL = nlapiResolveURL('SUITELET', 'customscript_receivingreportpdf_sl', 'customdeploy_receivingreportpdf_di');

	nlapiLogExecution('ERROR', 'ReceivingPDFURL',ReceivingPDFURL);					
	ReceivingPDFURL = ReceivingPDFURL + '&custparam_orderno='+ vordernew +'&custparam_ordername='+vordername;
	nlapiLogExecution('ERROR', 'ReceivingPDFURL',ReceivingPDFURL);
//	alert(WavePDFURL);
	window.open(ReceivingPDFURL);

}
