/***************************************************************************
��������������������������eBizNET
�������������������eBizNET Solutions Inc
 ****************************************************************************
 *
 *� $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/Attic/ebiz_ReplenCancellationQb_SL.js,v $
 *� $Revision: 1.1.2.2.4.1.2.2 $
 *� $Date: 2015/11/23 15:38:43 $
 *� $Author: grao $
 *� $Name: t_WMS_2015_2_StdBundle_1_177 $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_ReplenCancellationQb_SL.js,v $
 *� Revision 1.1.2.2.4.1.2.2  2015/11/23 15:38:43  grao
 *� 2015.2 Issue Fixes 201415809
 *�
 *� Revision 1.1.2.2.4.1.2.1  2015/11/16 17:39:59  gkalla
 *� case# 201414215
 *� Displaying blank in begin and end location dropdowns.
 *�
 *� Revision 1.1.2.2.4.1  2015/09/02 15:51:09  deepshikha
 *� 2015.2 issue fixes
 *� 201414215
 *�
 *� Revision 1.1.2.2  2012/12/21 09:05:15  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� GSUSA after go live Fixes.
 *�
 *
 ****************************************************************************/

function FillForm(request,response,form)
{
	var ReportNoField = form.addField('custpage_rplnreportno_qb', 'select', 'Report #');
	ReportNoField.addSelectOption('', '');
	if(request.getParameter('custpage_rplnreportno_qb')!='' && request.getParameter('custpage_rplnreportno_qb')!=null)
	{
		ReportNoField.setDefaultValue(request.getParameter('custpage_rplnreportno_qb'));	
	}
	
	ReportNoField.setLayoutType('startrow', 'none');
	var searchResultsRptNo = GetRplnReportNo();
	if (searchResultsRptNo != null && searchResultsRptNo != "" && searchResultsRptNo.length>0) {
		for (var i = 0; i < searchResultsRptNo.length; i++) {
			if(searchResultsRptNo[i].getValue('name')!=null && searchResultsRptNo[i].getValue('name')!="")
			{
				var res=  form.getField('custpage_rplnreportno_qb').getSelectOptions(searchResultsRptNo[i].getValue('name'), 'is');
				if (res != null) {				
					if (res.length > 0) {
						continue;
					}
				}
				ReportNoField.addSelectOption(searchResultsRptNo[i].getValue('name'), searchResultsRptNo[i].getValue('name'));
			}
		}
	}
	
	var BeginLocField = form.addField('custpage_rplnbeginloc_qb', 'select', 'Begin Location');
	BeginLocField.addSelectOption('', '');
	if(request.getParameter('custpage_rplnbeginloc_qb')!='' && request.getParameter('custpage_rplnbeginloc_qb')!=null)
	{
		BeginLocField.setDefaultValue(request.getParameter('custpage_rplnbeginloc_qb'));	
	}
	
	BeginLocField.setLayoutType('startrow', 'none');
	var filtersBeginLoc = new Array();
	filtersBeginLoc.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filtersBeginLoc.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
	filtersBeginLoc.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'noneof','@NONE@'));
	var columnsn = new Array();
	columnsn[0] = new nlobjSearchColumn('custrecord_actbeginloc')
	columnsn[1] = new nlobjSearchColumn('internalid').setSort(true);
	
	
	var searchresultsbeginloc = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersBeginLoc,columnsn);
	//searchresultsbeginloc = removeDuplicateElement(searchresultsbeginloc);
	for (var i = 0; searchresultsbeginloc != null && i < Math.min(500, searchresultsbeginloc.length); i++) {
		if (searchresultsbeginloc[i].getValue('custrecord_actbeginloc') != null) {
			var res = form.getField('custpage_rplnbeginloc_qb').getSelectOptions(searchresultsbeginloc[i].getText('custrecord_actbeginloc'), 'is');
			if (res != null) {
				if (res.length > 0) {
					continue;
				}
			}
			BeginLocField.addSelectOption(searchresultsbeginloc[i].getValue('custrecord_actbeginloc'), searchresultsbeginloc[i].getText('custrecord_actbeginloc'));
		}
	}
	
	var PriorityField = form.addField('custpage_rplnpriority_qb', 'select', 'Priority');
	PriorityField.addSelectOption('', '');
	if(request.getParameter('custpage_rplnpriority_qb')!='' && request.getParameter('custpage_rplnpriority_qb')!=null)
	{
		PriorityField.setDefaultValue(request.getParameter('custpage_rplnpriority_qb'));	
	}
	
	PriorityField.setLayoutType('startrow', 'none');
	var filtersPriority = new Array();
	filtersPriority.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filtersPriority.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
	filtersPriority.push(new nlobjSearchFilter('custrecord_taskpriority', null, 'isnotempty'));
	var searchresultspriority = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersPriority, new nlobjSearchColumn('custrecord_taskpriority'));
	for (var i = 0; searchresultspriority != null && i < Math.min(500, searchresultspriority.length); i++) {
		if (searchresultspriority[i].getValue('custrecord_taskpriority') != null) {
			var res = form.getField('custpage_rplnpriority_qb').getSelectOptions(searchresultspriority[i].getText('custrecord_taskpriority'), 'is');
			if (res != null) {
				if (res.length > 0) {
					continue;
				}
			}
			PriorityField.addSelectOption(searchresultspriority[i].getValue('custrecord_taskpriority'), searchresultspriority[i].getValue('custrecord_taskpriority'));
		}
	}
	
	var ItemField = form.addField('custpage_rplnitem_qb', 'select', 'Item','item');
	if(request.getParameter('custpage_rplnitem_qb')!='' && request.getParameter('custpage_rplnitem_qb')!=null)
	{
		ItemField.setDefaultValue(request.getParameter('custpage_rplnitem_qb'));	
	}
	ItemField.setLayoutType('startrow', 'none');	
	
	var EndLocField = form.addField('custpage_rplnendloc_qb', 'select', 'End Location');
	EndLocField.addSelectOption('', '');
	if(request.getParameter('custpage_rplnendloc_qb')!='' && request.getParameter('custpage_rplnendloc_qb')!=null)
	{
		EndLocField.setDefaultValue(request.getParameter('custpage_rplnendloc_qb'));	
	}
	
	EndLocField.setLayoutType('startrow', 'none');
	var filtersEndLoc = new Array();
	filtersEndLoc.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	filtersEndLoc.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
	filtersEndLoc.push(new nlobjSearchFilter('custrecord_actendloc', null, 'noneof','@NONE@'));
	var columnsn = new Array();
	columnsn[0] = new nlobjSearchColumn('custrecord_actendloc')
	columnsn[1] = new nlobjSearchColumn('internalid').setSort(true);
	
	var searchresultsendloc = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersEndLoc,columnsn);
	for (var i = 0; searchresultsendloc != null && i < Math.min(500, searchresultsendloc.length); i++) {
		if (searchresultsendloc[i].getValue('custrecord_actendloc') != null) {
			var res = form.getField('custpage_rplnendloc_qb').getSelectOptions(searchresultsendloc[i].getText('custrecord_actendloc'), 'is');
			if (res != null) {
				if (res.length > 0) {
					continue;
				}
			}
			EndLocField.addSelectOption(searchresultsendloc[i].getValue('custrecord_actendloc'), searchresultsendloc[i].getText('custrecord_actendloc'));
		}
	}
	
	form.addSubmitButton('Display');
	return form;
}

function ReplenCancellationQbSuitelet(request, response){
	var ctx = nlapiGetContext();
	if (request.getMethod() == 'GET') 
	{
		var context = nlapiGetContext();
		var form = nlapiCreateForm('Cancel Replenishment');

		FillForm(request,response,form);
		response.writePage(form);
	}
	else
	{
		var ReplenArray = new Array();
		ReplenArray["custpage_rplnreportno_qb"] = request.getParameter('custpage_rplnreportno_qb');
		ReplenArray["custpage_rplnitem_qb"] = request.getParameter('custpage_rplnitem_qb');
		ReplenArray["custpage_rplnbeginloc_qb"] = request.getParameter('custpage_rplnbeginloc_qb');
		ReplenArray["custpage_rplnendloc_qb"] = request.getParameter('custpage_rplnendloc_qb');
		ReplenArray["custpage_rplnpriority_qb"] = request.getParameter('custpage_rplnpriority_qb');

		response.sendRedirect('SUITELET', 'customscript_replen_cancellation','customdeploy_replen_cancellation_di', false, ReplenArray);
	}
}

function GetRplnReportNo()
{
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]);	//	8 = RPLN Task
	filters[1] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');
	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters,columns);
	return searchresults;
}
