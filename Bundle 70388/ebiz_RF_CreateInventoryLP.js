/***************************************************************************
 eBizNET Solutions 
 ****************************************************************************
 *
 * $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_CreateInventoryLP.js,v $
 * $Revision: 1.6.4.14.4.6.2.20.2.1 $
 * $Date: 2015/11/16 07:23:02 $
 * $Author: grao $
 * $Name: t_WMS_2015_2_StdBundle_1_147 $
 *
 * DESCRIPTION
 * Functionality
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_CreateInventoryLP.js,v $
 * Revision 1.6.4.14.4.6.2.20.2.1  2015/11/16 07:23:02  grao
 * 2015.2 Issue Fixes 201415615
 *
 * Revision 1.6.4.14.4.6.2.20  2015/08/11 15:34:51  grao
 * 2015.2   issue fixes  201413876
 *
 * Revision 1.6.4.14.4.6.2.19  2015/07/30 15:49:40  nneelam
 * case# 201413689
 *
 * Revision 1.6.4.14.4.6.2.18  2015/07/23 13:39:22  schepuri
 * case#201413594
 *
 * Revision 1.6.4.14.4.6.2.17  2015/07/21 15:08:27  grao
 * 2015.2   issue fixes  201413117
 *
 * Revision 1.6.4.14.4.6.2.16  2015/05/14 14:07:07  schepuri
 * case# 201412795
 *
 * Revision 1.6.4.14.4.6.2.15  2014/09/26 15:49:17  skavuri
 * Case#  sonic cr fixed
 *
 * Revision 1.6.4.14.4.6.2.14  2014/08/13 15:54:31  skavuri
 * Case# 20149947 StdBundle Issue Fixed
 *
 * Revision 1.6.4.14.4.6.2.13  2014/06/13 10:20:28  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.6.4.14.4.6.2.12  2014/06/10 15:42:45  skavuri
 * Case# 20148843 SB Issue Fixed
 *
 * Revision 1.6.4.14.4.6.2.11  2014/06/06 07:40:01  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.6.4.14.4.6.2.10  2014/05/30 00:34:19  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.6.4.14.4.6.2.9  2014/02/17 16:06:28  sponnaganti
 * case# 20127150
 * (getting company based on location)
 *
 * Revision 1.6.4.14.4.6.2.8  2014/01/06 13:16:03  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.6.4.14.4.6.2.7  2013/06/05 22:11:22  spendyala
 * CASE201112/CR201113/LOG201121
 * FIFO field updating with data stamp is removed.
 *
 * Revision 1.6.4.14.4.6.2.6  2013/05/02 14:24:47  schepuri
 * updating remaning cube issue fix
 *
 * Revision 1.6.4.14.4.6.2.5  2013/04/26 15:49:51  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fixes
 *
 * Revision 1.6.4.14.4.6.2.4  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.6.4.14.4.6.2.3  2013/04/09 08:39:53  grao
 * CASE201112/CR201113/LOG201121
 * Issue fixed for undefind s7 variable
 *
 * Revision 1.6.4.14.4.6.2.2  2013/03/26 13:27:42  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.6.4.14.4.6.2.1  2013/03/01 14:34:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.6.4.14.4.6  2013/02/22 06:39:56  skreddy
 * CASE201112/CR201113/LOG201121
 * Inserted Item Desc through Rf Create Inventory
 *
 * Revision 1.6.4.14.4.5  2012/11/27 17:50:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Serial Capturing file.
 *
 * Revision 1.6.4.14.4.4  2012/11/09 14:14:58  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form 2012.2 branch.
 *
 * Revision 1.6.4.14.4.3  2012/11/01 14:55:42  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.6.4.14.4.2  2012/10/05 06:34:12  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting Multiple language into given suggestions
 *
 * Revision 1.6.4.14.4.1  2012/09/21 15:01:30  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.6.4.14  2012/09/03 14:08:33  schepuri
 * CASE201112/CR201113/LOG201121
 * added date stamp
 *
 * Revision 1.6.4.13  2012/09/03 13:49:11  schepuri
 * CASE201112/CR201113/LOG201121
 * added date stamp
 *
 * Revision 1.6.4.12  2012/08/16 12:40:00  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fix as a part of 339 bundle i.e.,
 * merging the inventory with existing LP# in that location even though
 * we select option not to merging.
 *
 * Revision 1.6.4.11  2012/08/15 16:26:05  mbpragada
 * 20120490
 * Issue fix as a part of 339 bundle
 *
 * Revision 1.6.4.8  2012/07/20 15:31:06  spendyala
 * CASE201112/CR201113/LOG201121
 * Invalid argument passing to the Create Inventory function.
 *
 * Revision 1.6.4.7  2012/07/04 07:10:29  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to creating inventory for Monobind SB is resolved.
 *
 * Revision 1.6.4.6  2012/06/11 22:25:10  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT and Prod issue fixes
 *
 * Revision 1.6.4.5  2012/06/04 09:24:49  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.6.4.4  2012/03/16 14:08:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.6.4.3  2012/02/20 14:44:59  spendyala
 * CASE201112/CR201113/LOG201121
 * issue fix related to create inventory.
 *
 * Revision 1.6.4.2  2012/02/12 12:59:26  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing related create inventory for batch item
 *
 * Revision 1.6.4.1  2012/02/01 08:31:32  spendyala
 * CASE201112/CR201113/LOG201121
 * while creating inventory rec a/c ref invalid issue is been resolved by passing empty value to the field.
 *
 *
 ****************************************************************************/


/**
 * @param request
 * @param response
 */
function CreateInventoryLP(request, response)
{
	
	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');

	var user=context.getUser();
	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);
	if (request.getMethod() == 'GET') 
	{
		var locationId = request.getParameter('custparam_locationId');
		nlapiLogExecution('ERROR', 'Location in Create inventory LP Page load::',locationId);	


		var getItemLP;
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime')+ ' ' + request.getParameter('custparam_actualbegintimeampm');

		var getBinLocationId = request.getParameter('custparam_binlocationid');
		var getBinLocation = request.getParameter('custparam_binlocationname');
		var getItemId = request.getParameter('custparam_itemid');
		var getItem = request.getParameter('custparam_item');
		var getQuantity = request.getParameter('custparam_quantity');
		nlapiLogExecution('ERROR', 'getQuantity from prevscreen', getQuantity);

		var getItemStatus = request.getParameter('custparam_itemstatus');
		var getItemStatusId = request.getParameter('custparam_itemstatusid');
		var getPackCode = request.getParameter('custparam_packcode');
		var getPackCodeId = request.getParameter('custparam_packcodeid');
		var getAdjustmentType = request.getParameter('custparam_adjustmenttype');
		var getAdjustmentTypeId = request.getParameter('custparam_adjustmenttypeid');
		var sitelocation = request.getParameter('custparam_sitelocation');

		var getBatchId = request.getParameter('custparam_BatchId');
		var getBatchNo = request.getParameter('custparam_BatchNo');


		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4;
		if( getLanguage == 'es_ES')
		{

			st0 = "CREAR INVENTARIO";
			st1 = "INGRESAR / ESCANEO DEL PLACA: ";
			st2 = "INGRESAR NOTAS :";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";

		}
		else
		{
			st0 = "CREATE INVENTORY";
			st1 = "ENTER / SCAN LP :";
			st2 = "ENTER Notes :";
			st3 = "SEND";
			st4 = "PREV";

		}



		var functionkeyHtml=getFunctionkeyScript('_rf_createinv_lp');
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";      
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		
		//html = html + " document.getElementById('enterlp').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head>";
		html = html + "<body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_createinv_lp' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1;
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnBinLocationId' value=" + getBinLocationId + ">";
		html = html + "				<input type='hidden' name='hdnBinLocation' value=" + getBinLocation + ">";
		html = html + "				<input type='hidden' name='hdnItemId' value=" + getItemId + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getQuantity + ">";
		html = html + "				<input type='hidden' name='hdnPackCode' value=" + getPackCode + ">";
		html = html + "				<input type='hidden' name='hdnPackCodeId' value=" + getPackCodeId + ">";
		html = html + "				<input type='hidden' name='hdnItemStatus' value=" + getItemStatus + ">";
		html = html + "				<input type='hidden' name='hdnItemStatusId' value=" + getItemStatusId + ">";
		html = html + "				<input type='hidden' name='hdnAdjustmentType' value=" + getAdjustmentType + ">";
		html = html + "				<input type='hidden' name='hdnAdjustmentTypeId' value=" + getAdjustmentTypeId + ">";
		html = html + "				<input type='hidden' name='hdnsiteLocation' value=" + sitelocation + ">";
		html = html + "				<input type='hidden' name='hdnLocationName' value=" + locationId+ ">";
		html = html + "				<input type='hidden' name='hdnLocationInternalid' value=" + locationId+ ">";
		html = html + "				<input type='hidden' name='hdnCompname' value=" + locationId + ">";

		html = html + "				<input type='hidden' name='hdnBatchId' value=" + getBatchId + ">";
		html = html + "				<input type='hidden' name='hdnBatchNo' value=" + getBatchNo + ">";
		html = html + "			    <input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";

		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlp' id='enterlp' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";



		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enternotes' type='text'/>";
		html = html + "					<input type='hidden' name='hdnnotes' value=" + locationId + ">";
		html = html + "				</td>";
		html = html + "			</tr>";


		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st4 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlp').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('ERROR', 'Into Response', 'Into Response');

		getItemLP = request.getParameter('enterlp');
		getNotes = request.getParameter('enternotes');


		// This variable is to hold the Quantity entered.
		var CIarray = new Array();


		var getLanguage = request.getParameter('hdngetLanguage');
		CIarray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR', 'getLanguage', CIarray["custparam_language"]);


		var st4,st5,st6,st7;
		if( getLanguage == 'es_ES')
		{
			st4 = "LP NO V&#193;LIDO";
			st5 = "LP EST&#193; ASIGNADO AL ELEMENTO DIFERENTE ";
			st6 = "LP NO V&#193;LIDO EN RANGO";
			st7 = "";
		}
		else
		{
			st4 = "INVALID LP";
			st5 = "LP IS ALREADY ASSIGNED TO DIFFERENT ITEM";
			st6 = "LP NOT IN VALID RANGE";
			st7 = "INVALID LP";
		}

		//	Get the Inveentory data from the previous screen, which is passed as a parameter		
		var getBinLocationId = request.getParameter('hdnBinLocationId');
		var getBinLocation = request.getParameter('hdnBinLocation');
		var getItemId = request.getParameter('hdnItemId');
		var getItem = request.getParameter('hdnItem');
		var getQuantity = request.getParameter('hdnQuantity');
		nlapiLogExecution('ERROR','getQuantity',getQuantity);

		var getPackCode = request.getParameter('hdnPackCode');
		var getPackCodeId = request.getParameter('hdnPackCodeId');
		var getItemStatus = request.getParameter('hdnItemStatus');
		var getItemStatusId = request.getParameter('hdnItemStatusId');
		var getAdjustmentType = request.getParameter('hdnAdjustmentType');
		var getAdjustmentTypeId = request.getParameter('hdnAdjustmentTypeId');
		var SiteLocation = request.getParameter('hdnsiteLocation');
		var locationId = request.getParameter('hdnLocationInternalid');
		nlapiLogExecution('ERROR','SiteLocation',SiteLocation);
		nlapiLogExecution('ERROR','locationId',locationId);

		var getBatchId = request.getParameter('hdnBatchId');
		var getBatchNo = request.getParameter('hdnBatchNo');



		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime')+ ' ' + request.getParameter('custparam_actualbegintimeampm');

		var getBaseUOM = 'EACH';
		var getBinLocation = "";
		var lpExists;

		CIarray["custparam_error"] = st4;

		CIarray["custparam_binlocationid"] = getBinLocationId;
		CIarray["custparam_binlocationname"] = getBinLocation;
		CIarray["custparam_itemid"] = getItemId;
		CIarray["custparam_item"] = getItem;
		CIarray["custparam_quantity"] = getQuantity;
		CIarray["custparam_itemstatus"] = getItemStatus;
		CIarray["custparam_itemstatusid"] = getItemStatusId;
		CIarray["custparam_packcode"] = getPackCode;
		CIarray["custparam_packcodeid"] = getPackCodeId;
		CIarray["custparam_adjustmenttype"] = getAdjustmentType;
		CIarray["custparam_adjustmenttypeid"] = getAdjustmentTypeId;

		CIarray["custparam_actualbegindate"] = request.getParameter('custparam_actualbegindate');
		CIarray["custparam_locationId"] = locationId;
		CIarray["custparam_screenno"] = '17';

		CIarray["custparam_BatchId"] = getBatchId;
		CIarray["custparam_BatchNo"] = getBatchNo;
		CIarray["custparam_notes"] = getNotes;

		var TimeArray = new Array();
		TimeArray = getActualBeginTime.split(' ');
		CIarray["custparam_actualbegintime"] = TimeArray[0];
		CIarray["custparam_actualbegintimeampm"] = TimeArray[1];

		nlapiLogExecution('ERROR', 'custparam_actualbegintime', CIarray["custparam_actualbegintime"]);
		nlapiLogExecution('ERROR', 'custparam_actualbegintimeampm', CIarray["custparam_actualbegintimeampm"]);
		
		var itemSubtype = nlapiLookupField('item', getItemId, ['recordType','custitem_ebizserialin']);

		var itemType = '',serialInFlag='F';
		
		if(itemSubtype != null && itemSubtype != '')
		{
			itemType = itemSubtype.recordType;
			serialInFlag = itemSubtype.custitem_ebizserialin;
		}
		CIarray["custparam_itemtype"] = itemType;

//		CIarray["custparam_screenno"] = '5';

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to RF Main Menu.
		
		if (sessionobj!=context.getUser()) {

			try
			{

				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}
				if (optedEvent == 'F7') 
				{
					response.sendRedirect('SUITELET', 'customscript_rf_create_invt_itemstat', 'customdeploy_rf_create_invt_itemstat_di', false, CIarray);
				}
				//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
				//            if (optedEvent != '' && optedEvent != null) {
				else 
				{
					if (getItemLP == "") 
					{
						//	if the 'Send' button is clicked without any option value entered,
						//  it has to show an error message. The next screen to which it has to navigate is to the error screen.
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
						nlapiLogExecution('ERROR', 'Entered Item LP', getItemLP);
					}
					else 
					{
						CIarray["custparam_itemlp"] = getItemLP;                
						//Value 2 stands for user defined LP Range
						//Value 1 stands for LP Type= PALT type
						// Case# 20148843 starts
						//LPReturnValue = ebiznet_LPRange_CL(getItemLP, '2',locationId); 
						LPReturnValue = ebiznet_LPRange_CL(getItemLP, '2',locationId,'1');
						// Case# 20148843 ends
						var getActualEndDate = DateStamp();
						var getActualEndTime = TimeStamp();				
						if (LPReturnValue == true) 
						{									

					nlapiLogExecution('ERROR', 'INTO MASTER LP INSERTION');
					var filtersmlp = new Array();
					filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', getItemLP);

					var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);
					nlapiLogExecution('ERROR', 'SRCH_RECORD',SrchRecord);	

					if (SrchRecord != null && SrchRecord.length > 0) 
					{
						nlapiLogExecution('ERROR', 'LP FOUND');	
						lpExists = 'Y';

						//Need to write code for Over write LP
						//if yes continue the below process
						//else
						//move to LP screen.

						response.sendRedirect('SUITELET', 'customscript_rf_overwrite_lp', 'customdeploy_rf_overwrite_lp_di', false, CIarray);
						//nlapiLogExecution('ERROR', 'Entered Item LP', getItemLP);	
					}
					else
					{
						//checking whether inventory should be created or updated based on the location,bin location,sku,sku status,packcode
						//if exists update the qty in inventory else create a new record.						
						var filtersinv = new Array();
						filtersinv.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc',null, 'anyof',locationId));	
						filtersinv.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc',null, 'anyof',getBinLocationId));	
						filtersinv.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku',null, 'anyof',getItemId));
						filtersinv.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status',null, 'anyof',getItemStatusId));
						filtersinv.push(new nlobjSearchFilter('custrecord_ebiz_inv_packcode',null, 'anyof',getPackCode));
						filtersinv.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag',null, 'anyof',[3,19]));
						if(getBatchId!=null && getBatchId!="")
							filtersinv.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot',null,'anyof',getBatchId));
						var column = new Array();
						column[0]=new nlobjSearchColumn('custrecord_ebiz_qoh').setSort('True');

						nlapiLogExecution('ERROR', 'locationId::',locationId);
						nlapiLogExecution('ERROR', 'getBinLocationId::',getBinLocationId);
						nlapiLogExecution('ERROR', 'getItemId::',getItemId);
						nlapiLogExecution('ERROR', 'getItemStatusId::',getItemStatusId);
						nlapiLogExecution('ERROR', 'getPackCodeId::',getPackCode);

						var SrchINVRecord = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinv,column);
						
						

						if (SrchINVRecord != null && SrchINVRecord.length > 0) 
						{
							nlapiLogExecution('ERROR', 'SrchINVRecord.length::',SrchINVRecord.length);
						var tempflag=0;
						nlapiLogExecution('ERROR', 'Inventory Updated Mode::');
						var maxuomqty= getmaxuomqty(getItemId);
						for(var count=SrchINVRecord.length-1;count >= 0;count--)
						{
							var TotalQty = parseFloat(SrchINVRecord[count].getValue('custrecord_ebiz_qoh')) + parseFloat(getQuantity);
							nlapiLogExecution('ERROR','TOTALQTY',TotalQty);
							nlapiLogExecution('ERROR','MAXQTY',maxuomqty);
							if(parseFloat(TotalQty) <= parseFloat(maxuomqty))
							{
								tempflag=1;
								var id=SrchINVRecord[count].getId();
								break;
							}
						}
						nlapiLogExecution('ERROR', 'Inventory id in OverWriteLP is ::'+ id);
						CIarray["custparam_inventoryId"] = id;
						CIarray["custparam_flag"] = 1;
						nlapiLogExecution('ERROR', 'Record Exists in Inventory::');
						
						
						// case# 201412795
						/*var fields = ['recordType', 'custitem_ebizserialin'];
						var itemSubtype = nlapiLookupField('item', getItemId,fields);
						var itemType = '',serialInFlag='F';
						if(itemSubtype != null && itemSubtype != '')
						{
							itemType = itemSubtype.recordType;
							serialInFlag = itemSubtype.custitem_ebizserialin;
						}*/
						
						if(tempflag==1)
							response.sendRedirect('SUITELET', 'customscript_rf_inventory_merge_lp', 'customdeploy_rf_inventory_merge_lp_di', false, CIarray);
						else
						{
							//var itemSubtype = nlapiLookupField('item', getItemId, ['recordType','custitem_ebizserialin']);
						//	CIarray["custparam_itemtype"] = itemType;
							nlapiLogExecution('ERROR', 'itemSubtype.recordType =', itemType);
							if (itemType == 'serializedinventoryitem' || itemType == 'serializedassemblyitem' || serialInFlag == 'T') 
							{
								nlapiLogExecution('ERROR', 'SrchINVRecord.length::',SrchINVRecord.length);
								
								if(parseInt(getQuantity) != getQuantity)
								{
									CIarray["custparam_error"] = "For Serial item ,Decimal quantities not allowed";
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
									return;
								}
								
								
								
								response.sendRedirect('SUITELET', 'customscript_rf_create_invt_serialscan', 'customdeploy_rf_create_invt_serialscan_d', false, CIarray);
								return;
							}
							else
							{
								CreateInventory(getItemId, getItem, getItemStatusId, getItemStatus, getPackCodeId, getPackCode, 
										getAdjustmentTypeId, getAdjustmentType, getItemLP, getBinLocation, getBinLocationId, 
										getQuantity, getActualEndDate, getActualEndTime, getActualBeginDate, getActualBeginTime, 
										SiteLocation,locationId,getNotes,getBatchId,'');

								response.sendRedirect('SUITELET', 'customscript_rf_create_invt_complete', 'customdeploy_rf_create_invt_complete_di', false, CIarray);
							}
						}

						}
						else
						{
							nlapiLogExecution('ERROR', 'Else part of the search inventotry');						
							var filtersMergeinv = new Array();
							filtersMergeinv.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp',null, 'is',getItemLP));
							var SrchMergeINVRecord = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersMergeinv);
							if(SrchMergeINVRecord != null && SrchMergeINVRecord.length > 0)
							{							
								CIarray["custparam_error"] = st5;
								nlapiLogExecution('ERROR', 'CIarray["custparam_itemlp"]::1',CIarray["custparam_itemlp"]);
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
								nlapiLogExecution('ERROR', 'Entered Item LP', getItemLP);
							}
							else
							{
								
								nlapiLogExecution('ERROR', 'itemSubtype.recordType =', itemType);
								if (itemType == 'serializedinventoryitem'||itemType == 'serializedassemblyitem'|| serialInFlag == 'T') 
								{
									
									
									if(parseInt(getQuantity) != getQuantity)
									{
										CIarray["custparam_error"] = "For Serial item ,Decimal quantities not allowed";
										response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
										return;
									}
									
									response.sendRedirect('SUITELET', 'customscript_rf_create_invt_serialscan', 'customdeploy_rf_create_invt_serialscan_d', false, CIarray);
									return;
								}
								else
								{
									nlapiLogExecution('ERROR', 'into create inventory of Different LP');
									CreateInventory(getItemId, getItem, getItemStatusId, getItemStatus, getPackCodeId, getPackCode, 
											getAdjustmentTypeId, getAdjustmentType, getItemLP, getBinLocation, getBinLocationId, 
											getQuantity, getActualEndDate, getActualEndTime, getActualBeginDate, getActualBeginTime, 
											SiteLocation,locationId,getNotes,getBatchId,'');				                				                
									nlapiLogExecution('ERROR', 'Created', 'Successfully');

									response.sendRedirect('SUITELET', 'customscript_rf_create_invt_complete', 'customdeploy_rf_create_invt_complete_di', false, CIarray);
								}
							}
						}
					}

						}
						else
						{					
							CIarray["custparam_error"] = st7;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
							nlapiLogExecution('ERROR', 'Entered Item LP', getItemLP);
						}								
					}        
				}
			}
			catch (e)  {
				nlapiLogExecution('DEBUG', 'Exception',e);
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');

			}
		}
		else
		{
			//CIarray["custparam_screenno"] = '15';
			CIarray["custparam_screenno"] = '17'; //Case# 20149947
			CIarray["custparam_error"] = 'LP ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
		}
	}

	nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
}

/**
 * @param ItemId
 * @param Item
 * @param ItemStatusId
 * @param ItemStatus
 * @param PackCodeId
 * @param PackCode
 * @param AdjustmentTypeId
 * @param AdjustmentType
 * @param ItemLP
 * @param BinLocation
 * @param BinLocationId
 * @param Quantity
 * @param ActualEndDate
 * @param ActualEndTime
 * @param ActualBeginDate
 * @param ActualBeginTime
 * @param SiteLocation
 * @param locationId
 * @param Notes
 */
function CreateInventory(ItemId, Item, ItemStatusId,ItemStatus, PackCodeId, PackCode, AdjustmentTypeId, AdjustmentType, ItemLP, BinLocation, BinLocationId, Quantity, ActualEndDate, ActualEndTime, ActualBeginDate, ActualBeginTime, SiteLocation,locationId,Notes,lot,fifodate)
{
	//var fifodate='';
	var fifovalue='';
	// Sonic starts
	var confirm_createInv = getSystemRuleValue('Confirm RF Create Inventory to NetSuite? Y/N',locationId);
	nlapiLogExecution('ERROR', 'confirm_createInv', confirm_createInv);
	// Sonic 
	//create a inventory record
	var CreateInventoryRecord = nlapiCreateRecord('customrecord_ebiznet_createinv');
	nlapiLogExecution('ERROR', 'Create Inventory Record', 'Inventory Record');

	CreateInventoryRecord.setFieldValue('custrecordact_begin_date', ActualBeginDate);
	CreateInventoryRecord.setFieldValue('custrecord_act_end_date', ActualEndDate);
	//Adding fields to update time zones.
	CreateInventoryRecord.setFieldValue('custrecord_actualbegintime', ActualBeginTime);
	CreateInventoryRecord.setFieldValue('custrecord_actualendtime', ActualEndTime);
	CreateInventoryRecord.setFieldValue('custrecord_recordtime', TimeStamp());
	CreateInventoryRecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
	CreateInventoryRecord.setFieldValue('custrecord_current_date', DateStamp());
	CreateInventoryRecord.setFieldValue('custrecord_upd_date', DateStamp());

	//Status flag .
	//customrecord.setFieldValue('custrecord_status_flag', 'S');
	CreateInventoryRecord.setFieldValue('custrecord_wms_inv_status_flag', 19);
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_qoh', parseFloat(Quantity).toFixed(4));
	CreateInventoryRecord.setFieldValue('custrecord_invttasktype', 10);//PackCodeId);
	//
	//Added for Item name and desc
	CreateInventoryRecord.setFieldValue('custrecord_sku', Item);
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_sku', ItemId);
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(Quantity).toFixed(4));
//	CreateInventoryRecord.setFieldValue('custrecord_skudesc', ItemDesc);
	var Itype,batchflg='F',itemfamId,itemgrpId;
	if(ItemId !=null && ItemId !=''){

		var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group',
		              'custitem_ebizserialin','custitem_ebiz_merge_fifodates'];
		var columns = nlapiLookupField('item', ItemId, fields);
		Itype = columns.recordType;					
		batchflg = columns.custitem_ebizbatchlot;
		itemfamId= columns.custitem_item_family;
		itemgrpId= columns.custitem_item_group;
		//var serialInflg = columns.custitem_ebizserialin;
		//var mergefifodates=columns.custitem_ebiz_merge_fifodates;

		//var Itype = nlapiLookupField('item', ItemId, 'recordType');
		var ItemRec = nlapiLoadRecord(Itype, ItemId);
		var ItemDesc = ItemRec.getFieldValue('salesdescription');
		nlapiLogExecution('ERROR', 'ItemId', ItemId);
		nlapiLogExecution('ERROR', 'ItemDesc', ItemDesc);
		var ItemDecsript='';
		if(ItemDesc!=null && ItemDesc!='')
			{
			ItemDecsript=ItemDesc.substring(0,280);
			
			}
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_itemdesc', ItemDecsript);
	}

	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_sku_status', ItemStatusId);
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_packcode', 1);//PackCodeId);
	//Added on 20Feb by suman
	//checking the condition whether lot is null or not,If it is not null then allow to pass the value to the field.
	if(lot!=null&&lot!="")
	{
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_lot', lot);	
		var rec=nlapiLookupField('customrecord_ebiznet_batch_entry',lot,['custrecord_ebizfifodate','custrecord_ebizexpirydate']);
		var fifodate=rec.custrecord_ebizfifodate;
		var expdate=rec.custrecord_ebizexpirydate;
		if(fifodate!=null && fifodate!='')
			CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_fifo',fifodate);
		/*else
			CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_fifo',DateStamp());*/
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_expdate', expdate);
	}
	if(fifodate==null || fifodate=='')
	{
		//fifodate = FifovalueCheck(Itype,ItemId,itemfamId,itemgrpId,linenum,pointid,lot);
		fifodate = FifovalueCheck(Itype,ItemId,itemfamId,itemgrpId,'','',lot);
		fifovalue=fifodate;
	}
	else
	{
		fifovalue=fifodate;
	}
	if(fifovalue!=null && fifovalue!="" && (fifovalue != 'NaN/NaN/NaN'))
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
	else
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());
	/*else
	{
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_fifo',DateStamp());
	}*/
	//


	/*
		var filtersAccNo = new Array();
        filtersAccNo[0] = new nlobjSearchFilter('custrecord_location', null, 'is', SiteLocation);
        var colsAcc = new Array();
        colsAcc[0]=new nlobjSearchColumn('custrecord_accountno');                
        var Accsearchresults = nlapiSearchRecord('customrecord_inventoryaccountno', null, filtersAccNo, colsAcc);              
        var varAccountNo = Accsearchresults[0].getValue('custrecord_accountno');                                
        nlapiLogExecution('ERROR', 'Account ',varAccountNo);
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_account_no', varAccountNo);//Account #);
	 */

	//code added on 01/02/12 by suman.
	//without passing this value system throughing an error stating that a/c no ref is invalid .
	//So to overcome that bug v are passing empty value.
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_account_no', '');
	//end of code added on 01/02/12

	// sonic
	if(confirm_createInv =='N')
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_callinv', 'N'); //Added by ramana for invoking NSInventory;
	else
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_callinv', 'Y');
	//sonic
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_displayfield', 'N');

	nlapiLogExecution('ERROR', 'Location id fetched are', locationId);

	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_loc',locationId);

	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_lp', ItemLP);//Item LP #);
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_binloc', BinLocationId);//Bin Location);
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_note1', Notes);//Notes1);

	//Added by suman as on 090812
	//updating Adjustment type and Company while creating invt record.
	nlapiLogExecution('ERROR','AdjustmentTypeId',AdjustmentTypeId+'/'+AdjustmentType);
	if(AdjustmentTypeId!=null&&AdjustmentTypeId!="")
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_adjusttype', AdjustmentTypeId);

	var compId=getCompanyId();
	//case# 20127150 starts (getting company based on location)
	nlapiLogExecution('ERROR','compId before',compId);
	var newLocationFilters = new Array();	
	newLocationFilters.push(new nlobjSearchFilter('internalid',null, 'is',locationId));	
	
	newLocationFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var newLocationColumn = new Array();	    	
	newLocationColumn.push(new nlobjSearchColumn('custrecordcompany'));		
	var newLocationResults = nlapiSearchRecord('location', null, newLocationFilters, newLocationColumn);
	if(newLocationResults!=null && newLocationResults!='')
	{
		compId=newLocationResults[0].getValue('custrecordcompany');
		nlapiLogExecution('ERROR','compId after',compId);
	}
	//case# 20127150 end
	if(compId!=null&&compId!="")
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_company', compId);

	//end of code as of 090812.



	//code added on 130812 by suman.
	//Code to update user id and recorded time.
	var currentContext = nlapiGetContext();
	var currentUserID = currentContext.getUser();
	CreateInventoryRecord.setFieldValue('custrecord_updated_user_no',currentUserID);
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_updatedtime',TimeStamp());
	//end of code as of 130812.


	nlapiLogExecution('ERROR', 'Submitting Create Inventory Record', 'Inventory Record');

	//commit the record to NetSuite
	var CreateInventoryRecordId = nlapiSubmitRecord(CreateInventoryRecord, false, true);

	nlapiLogExecution('ERROR', 'Inventory Creation', 'Success');

	try 
	{
		nlapiLogExecution('ERROR', 'INTO MASTER LP INSERTION');
		var filtersmlp = new Array();
		filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', ItemLP);

		var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

		if (SrchRecord != null && SrchRecord.length > 0) 
		{
			nlapiLogExecution('ERROR', 'LP FOUND');			
		}
		else 
		{
			nlapiLogExecution('ERROR', 'LP NOT FOUND');
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
			customrecord.setFieldValue('name', ItemLP);
			customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', ItemLP);
			var rec = nlapiSubmitRecord(customrecord, false, true);
			nlapiLogExecution('ERROR', 'Master LP Insertion', 'Success');
		}
	} 
	catch (e) 
	{
		nlapiLogExecution('ERROR', 'Failed to Update/Insert into Master LP Record');
	}

	/*var arrDims = getSKUCubeAndWeight(ItemId, 1);
	var itemCube = 0;
	if (arrDims["BaseUOMItemCube"] != "" && (!isNaN(arrDims["BaseUOMItemCube"]))) 
	{
		var uomqty = ((parseFloat(Quantity))/(parseFloat(arrDims["BaseUOMQty"])));			
		itemCube = (parseFloat(uomqty) * parseFloat(arrDims["BaseUOMItemCube"]));
		nlapiLogExecution('ERROR', 'itemCube', itemCube);
	} 

	var vOldRemainingCube = GeteLocCube(BinLocationId);
	nlapiLogExecution('ERROR', 'vOldRemainingCube', vOldRemainingCube);

	var vTotalCubeValue = parseFloat(itemCube) - parseFloat(vOldRemainingCube);
	nlapiLogExecution('ERROR', 'vOldLocationRemainingCube', vTotalCubeValue);

	var retValue =  UpdateLocCube(BinLocationId,vTotalCubeValue);
	nlapiLogExecution('ERROR', 'Loc Cube Updation', 'Success');*/
}

/**
 * @param getItemId
 * @returns {Number}
 */
function getmaxuomqty(getItemId)
{
	nlapiLogExecution('ERROR', 'Into getmaxuomqty','');
	nlapiLogExecution('ERROR', 'Item ID',getItemId);

	var maxuomqty=0;
	var filtersmlp = new Array();

	filtersmlp.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', getItemId));
//	filtersmlp.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var col=new Array();
	col[0] = new nlobjSearchColumn('custrecord_ebizqty');
	col[0].setSort('True');

	var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filtersmlp,col);
	if (SrchRecord != null && SrchRecord !="" && SrchRecord.length>0)
	{
		maxuomqty = SrchRecord[0].getValue('custrecord_ebizqty');
	}

	nlapiLogExecution('ERROR', 'Out of getmaxuomqty',maxuomqty);
	return maxuomqty;
}

function getCompanyId()
{
	try{
		var CompName;	
		var CompanyFilters = new Array();
		//CompanyFilters.push(new nlobjSearchFilter('internalid',null,'is',1));
		CompanyFilters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
		var CompanyColumn = new Array();	
		CompanyColumn.push(new nlobjSearchColumn('custrecord_company'));		
		var CompanyResults = nlapiSearchRecord('customrecord_ebiznet_company', null, CompanyFilters,CompanyColumn);	    
		if(CompanyResults!=null)
		{
			CompName=CompanyResults[0].getId();
		}
		return CompName;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in getCompanyId',exp);

	}
}

/*function DateStamp(){
	var now = new Date();
	return ((parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
}*/