/**
 * @author Ganesh Kalla
 * This Suitelet is meant to display scan picking location Screen
 */
/***************************************************************************
 eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_Bulkpicking_Item_SL.js,v $ 
 *     	   $Revision: 1.1.2.8 $
 *     	   $Date: 2014/06/13 13:30:54 $
 *     	   $Author: skavuri $
 *     	   $Name: t_NSWMS_2014_1_3_125 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Bulkpicking_Item_SL.js,v $
 * Revision 1.1.2.8  2014/06/13 13:30:54  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.7  2014/06/03 15:43:02  skavuri
 * Case# 20148688 SB Issue Fixed
 *
 * Revision 1.1.2.6  2014/05/30 00:41:00  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.5  2014/03/11 15:00:00  sponnaganti
 * case# 20127638,20127661
 * (Standard Bundle Issue Fix)
 *
 * Revision 1.1.2.4  2014/03/10 16:22:27  skavuri
 * Case# 20127528 issue fixed
 *
 * Revision 1.1.2.3  2014/03/10 16:14:01  sponnaganti
 * case# 20127531
 * Standard Bundle Issue fix
 *
 * Revision 1.1.2.2  2014/03/07 15:04:27  sponnaganti
 * case# 20127530
 * (Getting lot number for lot item.)
 *
 * Revision 1.1.2.1  2013/07/19 08:18:22  gkalla
 * Case# 20123527
 * Bulk picking CR for Nautilus
 *
 * Revision 1.13.2.21.4.16.2.6  2013/04/19 15:40:01  skreddy
 * CASE201112/CR201113/LOG201121
 * issue fixes
 * 
 *
 *
 *****************************************************************************/
function PickingItem(request, response){
	if (request.getMethod() == 'GET') {
		var getFetchedBeginLocation = request.getParameter('custparam_beginLocation');

		var Bulkpickflag="Y";
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('Error', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7,st8;

		if( getLanguage == 'es_ES')
		{
			st1 = "ART&#205;CULO:";
			st2 = "INGRESAR / ESCANEO DEL ART&#205;CULO";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";
			st5 = "OVERRIDE";
			st6 = "SKIP";
			st8 ="DESCRIPCI&#211;N DEL ART&#205;CULO";
		}
		else
		{
			st1 = "ITEM: ";
			st2 = "ENTER/SCAN ITEM ";
			st3 = "SEND";
			st4 = "PREV";
			st5 = "OVERRIDE";
			st6 = "SKIP";
			st8 = "ITEM DESCRIPTION:";
		}

		var getWaveno = request.getParameter('custparam_waveno');
		var getZoneNo = request.getParameter('custparam_zoneno');
		var getZoneId = request.getParameter('custparam_zoneid');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocation = request.getParameter('custparam_beginLocation');
		var getItem = request.getParameter('custparam_item');
		var getItemName = request.getParameter('custparam_itemname');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');


		var getBeginLocationName = request.getParameter('custparam_beginlocationname');

		var whLocation = request.getParameter('custparam_whlocation');
		var whCompany = request.getParameter('custparam_whcompany');

		var getZoneId=request.getParameter('custparam_ebizzoneno');
		nlapiLogExecution('Debug', 'getItemInternalId', getItemInternalId);
		/*var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');*/
		var getFetchedLocation=getBeginLocation;
		nlapiLogExecution('Debug', 'before getFetchedLocation', getFetchedLocation);
		nlapiLogExecution('Debug', 'before getItemInternalId', getItemInternalId);

		//code added by santosh on 7Aug2012
		var Type='';
		if(request.getParameter('custparam_type') !=null &&  request.getParameter('custparam_type')!="")
		{
			Type=request.getParameter('custparam_type');
		}
		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');
		 

		var Itemdescription='';

		nlapiLogExecution('ERROR', 'getItemName', getItemName);	

		if(getItemInternalId!=null && getItemInternalId!='')
		{
			var Itemtype = nlapiLookupField('item', getItemInternalId, 'recordType');

			var filtersitem = new Array();
			var columnsitem = new Array();

			filtersitem.push(new nlobjSearchFilter('internalid', null, 'is',getItemInternalId));
			filtersitem.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

			columnsitem[0] = new nlobjSearchColumn('description');    
			columnsitem[1] = new nlobjSearchColumn('salesdescription');
			columnsitem[2] = new nlobjSearchColumn('itemid');

			var itemRecord = nlapiSearchRecord(Itemtype, null, filtersitem, columnsitem);
			if(itemRecord!=null && itemRecord!='' && itemRecord.length>0)
			{
				if(itemRecord[0].getValue('description') != null && itemRecord[0].getValue('description') != '')
				{
					Itemdescription = itemRecord[0].getValue('description');
				}
				else if(itemRecord[0].getValue('salesdescription') != null && itemRecord[0].getValue('salesdescription') != "")
				{	
					Itemdescription = itemRecord[0].getValue('salesdescription');
				}
				getItemName=itemRecord[0].getValue('itemid');
			}

			Itemdescription = Itemdescription.substring(0, 20);			
		}

		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');        
		var getEnteredLocation = request.getParameter('custparam_endlocation');

		nlapiLogExecution('ERROR', 'getItem', getItem);	
		nlapiLogExecution('ERROR', 'Itemdescription', Itemdescription);
		  
		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
	//	html = html + " document.getElementById('enteritem').focus();";    

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		//html = html + "	  alert(node.type);";
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		//html = html + "	  alert(document.getElementById('cmdSend').disabled);";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>WAVE# :<label>" + getWaveno + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";	
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>Zone# :<label>" + getZoneNo + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";		
		html = html + "				<td align = 'left'>"+ st1 +"<label>" + getItemName + "</label><br>"+ st8 +"<label>" + Itemdescription + "</label>";//<br>REMAINING QTY: <label>" + remqty + "</label>";

		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";
		html = html + "				<input type='hidden' name='hdnItemName' value='" + getItemName + "'>";
		  
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + getFetchedLocation + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
 		 
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnbulkpickflag' value=" + Bulkpickflag + ">";
		
		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEnteredLocation' value=" + getEnteredLocation + ">";
		html = html + "				<input type='hidden' name='hdnBeginBinLocation' value='" + getBeginLocationName + "'>";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationid' value=" + getBeginLocation + ">";
		html = html + "				<input type='hidden' name='hdnebizzoneno' value=" + getZoneId + ">";
		html = html + "				<input type='hidden' name='hdnZoneNo' value='" + getZoneNo + "'>";//case#20127528
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st2;//ENTER/SCAN ITEM 
		
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enteritem' id='enteritem' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 +" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.focus();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdOverride.disabled=true; this.form.cmdSKIP.disabled=true; return false'/>";
		html = html + "					"+ st4 +" <input name='cmdPrevious' type='submit' value='F7'/>";
		//html = html + "					"+ st5 +" <input name='cmdOverride' type='submit' value='F11'/>";
		//html = html + "					"+ st6 +" <input name='cmdSKIP' type='submit' value='F12'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enteritem').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('ERROR', 'Into Response', 'Into Response');

		var getLanguage = request.getParameter('hdngetLanguage');

		var st7,st8;

		if( getLanguage == 'es_ES')
		{
			st7 = "ART&#205;CULO INV&#193;LIDO";
			st8 = "DESCRIPCI&#211;N DEL ART&#205;CULO:";

		}
		else
		{
			st7 = "INVALID ITEM";
			st8 = "ITEM DESCRIPTION: ";
		}
		var getEnteredItem = request.getParameter('enteritem');
		nlapiLogExecution('ERROR', 'Entered Item', getEnteredItem);
	 
		var getWaveNo = request.getParameter('hdnWaveNo');
		 
		var getExpectedQuantity = request.getParameter('hdnExpectedQuantity');
		var getBeginLocation = request.getParameter('hdnBeginLocation');
		var getBeginLocationid = request.getParameter('hdnBeginLocationid');
		var getItem = request.getParameter('hdnItem');
		var getItemName = request.getParameter('hdnItemName');
		var getItemDescription = request.getParameter('hdnItemDescription');
		var getItemInternalId = request.getParameter('hdnItemInternalId');
		var Bulkpickflag=request.getParameter('hdnbulkpickflag');  
		nlapiLogExecution('ERROR', 'hdnItemName', getItemName);
		 	
		var whLocation = request.getParameter('hdnwhlocation');
		var vZoneId=request.getParameter('hdnebizzoneno');
		 
		var whCompany = request.getParameter('hdnwhCompany');
		 
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');
		var optskipEvent = request.getParameter('cmdSKIP');

		// This variable is to hold the SO# entered.
		var SOarray = new Array();
		SOarray["custparam_language"] = getLanguage;
		SOarray["custparam_bulkpickflag"] = Bulkpickflag; 
		SOarray["custparam_recordinternalid"]= request.getParameter('custparam_recordinternalid');// Case# 20148688
		SOarray["custparam_error"] = st7;//'INVALID ITEM';
		SOarray["custparam_screenno"] = 'BULK03';
		SOarray["custparam_whlocation"] = whLocation;
		SOarray["custparam_waveno"] = getWaveNo;
		 
		SOarray["custparam_expectedquantity"] = getExpectedQuantity;
		SOarray["custparam_beginLocation"] = getBeginLocation;
		SOarray["custparam_beginLocationname"] = getBeginLocation;
		SOarray["custparam_item"] = getItem;
		SOarray["custparam_itemname"] = getItemName;
		SOarray["custparam_itemdescription"] = getItemDescription;
		SOarray["custparam_iteminternalid"] = getItemInternalId;
		SOarray["custparam_beginlocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
		  
		var vZoneNo=request.getParameter('hdnZoneNo');
		var vZoneId=request.getParameter('hdnebizzoneno');
		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  vZoneId;
		}
		else
			SOarray["custparam_ebizzoneno"] = '';
		SOarray["custparam_zoneno"] =  vZoneNo;
		
		var vSkipId=request.getParameter('hdnskipid');
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');
		 

		// Fetch the actual location based on the begin location value that is fetched and passed as a parameter
		// var BinLocationRec = nlapiLoadRecord('customrecord_ebiznet_location', getBeginLocation);

		// getBeginBinLocation = BinLocationRec.getFieldTx('custrecord_ebizlocname');
		//  nlapiLogExecution('DEBUG', 'Location Name is', getBeginBinLocation);
		 
		nlapiLogExecution('ERROR', 'getEnteredItem',getEnteredItem);
		nlapiLogExecution('ERROR', 'getItemName',getItemName);
		nlapiLogExecution('ERROR', 'getItem',getItem);

		var str = 'getEnteredItem. = ' + getEnteredItem + '<br>';			
		str = str + 'getItemName. = ' + getItemName + '<br>';
		str = str + 'getItem. = ' + getItem + '<br>';
		str = str + 'getItemInternalId. = ' + getItemInternalId + '<br>';

		nlapiLogExecution('ERROR', 'Item Details1', str);

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept SO #.
		if (optedEvent == 'F7') {
			//response.sendRedirect('SUITELET', 'customscript_rf_picking_container_no', 'customdeploy_rf_picking_container_no_di', false, SOarray);
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_bulkpick_location', 'customdeploy_ebiz_rf_bulkpick_location', false, SOarray);
		}
/*		else if(optskipEvent == 'F12')
		{
			var vPickType=request.getParameter('hdnpicktype');
			nlapiLogExecution('ERROR', 'vPickType',vPickType);
			nlapiLogExecution('ERROR', 'getWaveNo',getWaveNo);
			var RecCount;
			var SOFilters = new Array();
			SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));
			if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && getWaveNo != null && getWaveNo != "")
			{
				nlapiLogExecution('ERROR', 'getWaveno inside If',getWaveno);
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(getWaveNo)));
			}
			if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
			{
				nlapiLogExecution('ERROR', 'ClusNo inside If', vClusterNo);
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
			}
			if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
			{
				nlapiLogExecution('ERROR', 'OrdNo inside If', OrdName);
				SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
			}
			if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && ebizOrdNo!=null && ebizOrdNo!="" && ebizOrdNo!= "null")
			{
				nlapiLogExecution('ERROR', 'SO Id inside If', ebizOrdNo);
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ebizOrdNo));
			}
			if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
			{
				nlapiLogExecution('ERROR', 'Zone # inside If', vZoneId);
				SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
			}
			SOarray["custparam_ebizordno"] = ebizOrdNo;
			var SOColumns = new Array();
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
			SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
			SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
			SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
			SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
			SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
			SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
			SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
			SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
			SOColumns.push(new nlobjSearchColumn('name'));
			SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
			SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
			SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
			SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
			SOColumns.push(new nlobjSearchColumn('custrecord_sku_status'));

			SOColumns[0].setSort();
			SOColumns[1].setSort();
			SOColumns[2].setSort();
			SOColumns[3].setSort();
			SOColumns[4].setSort();
			SOColumns[5].setSort(true);

			var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
			nlapiLogExecution('ERROR', 'Results', SOSearchResults);
			if (SOSearchResults != null && SOSearchResults.length > 0) {
				nlapiLogExecution('ERROR', 'SOSearchResults.length', SOSearchResults.length);
				nlapiLogExecution('ERROR', 'vSkipId', vSkipId);
				vSkipId=0;
				if(SOSearchResults.length <= parseFloat(vSkipId))
				{
					vSkipId=0;
				}
				nlapiLogExecution('ERROR', 'SearchResults of given Wave', getWaveno);
				//var SOSearchResult = SOSearchResults[0];
				var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];
				if(SOSearchResults.length > parseFloat(vSkipId) + 1)
				{
					//var SOSearchnextResult = SOSearchResults[1];
					var SOSearchnextResult = SOSearchResults[parseFloat(vSkipId) + 1];
					SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
					SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
					SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
					nlapiLogExecution('ERROR', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
					nlapiLogExecution('ERROR', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
				}
				else
				{
					var SOSearchnextResult = SOSearchResults[0];
					SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
					SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
					SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
					nlapiLogExecution('ERROR', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
					nlapiLogExecution('ERROR', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
				}
				nlapiLogExecution('ERROR', 'SearchResults of given Wave', SOSearchResult.getValue('custrecord_lpno'));
				nlapiLogExecution('ERROR', 'SearchResults of given Wave', SOSearchResult.getValue('custrecord_expe_qty'));
				nlapiLogExecution('ERROR', 'SearchResults of given Wave', getWaveno);
				SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
				SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
				getRecordInternalId = SOSearchResult.getId();
				SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
				SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
				SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
				SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
				SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
				SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
				SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
				SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
				SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
				SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
				SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
				SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
				SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
				SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
				SOarray["custparam_noofrecords"] = SOSearchResults.length;		
				SOarray["name"] =  SOSearchResult.getValue('name');
				SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
				SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
				getBeginLocation = SOSearchResult.getText('custrecord_actbeginloc');
				if(vZoneId!=null && vZoneId!="")
				{
					SOarray["custparam_ebizzoneno"] =  SOSearchResults[parseFloat(vSkipId)].getValue('custrecord_ebizzone_no');
				}
				else
					SOarray["custparam_ebizzoneno"] = '';
			}

			var skiptask = nlapiLookupField('customrecord_ebiznet_trn_opentask',getRecordInternalId,'custrecord_skiptask');
			if(skiptask==null || skiptask=='')
			{
				skiptask=1;
			}
			else
			{
				skiptask=parseInt(skiptask)+1;
			}

			nlapiLogExecution('ERROR', 'skiptask',skiptask);

			nlapiSubmitField('customrecord_ebiznet_trn_opentask', getRecordInternalId, 'custrecord_skiptask', skiptask);
			vSkipId= parseFloat(vSkipId) + 1;
			SOarray["custparam_skipid"] = vSkipId;		
//			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
//			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			nlapiLogExecution('ERROR', 'SOarray["custparam_nextiteminternalid"]', SOarray["custparam_nextiteminternalid"]);
			nlapiLogExecution('ERROR', 'SOarray["custparam_nextlocation"]', SOarray["custparam_nextlocation"]);
			nlapiLogExecution('ERROR', 'getBeginLocation', getBeginLocation);
			nlapiLogExecution('ERROR', 'getItemInternalId', getItemInternalId);
			if(getBeginLocation != SOarray["custparam_nextlocation"])
			{  
				SOarray["custparam_beginLocationname"]=null;
				SOarray["custparam_iteminternalid"]=null;
				response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);								
				nlapiLogExecution('ERROR', 'Navigating To1', 'Picking Location');
			}
			else if(getItemInternalId != SOarray["custparam_nextiteminternalid"])
			{ 
				SOarray["custparam_iteminternalid"] = SOarray["custparam_nextiteminternalid"];
				SOarray["custparam_beginLocationname"]=null;
				//SOarray["custparam_iteminternalid"]=null;
				response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);								
				nlapiLogExecution('ERROR', 'Navigating To1', 'Picking Item');
			}
			else
			{
				SOarray["custparam_iteminternalid"] = SOarray["custparam_nextiteminternalid"];
				SOarray["custparam_beginLocationname"]=null;
				//SOarray["custparam_iteminternalid"]=null;
				response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);								
				nlapiLogExecution('ERROR', 'Navigating To1', 'Picking Item');
			}
		}*/
		else {
			var vEnteredItemId;
			if (getEnteredItem != '' && getEnteredItem != getItemName) {

				var actItemidArray=validateSKUId(getEnteredItem,whLocation,'');
				nlapiLogExecution('ERROR', 'After validateSKU1',actItemidArray);
				var actItemid = actItemidArray[1];
				nlapiLogExecution('ERROR', 'After validateSKU11',actItemid);
				if(actItemid!=null && actItemid!="")
				{
					nlapiLogExecution('Error', 'actItemid ', actItemid);
					//POarray["custparam_poitem"]=actItemid;
					vEnteredItemId=actItemid;

					nlapiLogExecution('Error', 'getEnteredItem ', getEnteredItem);
					nlapiLogExecution('Error', 'getItem ', getItem);
					nlapiLogExecution('Error', 'getItemName ', getItemName);

					 
					var Itype = nlapiLookupField('item', actItemid, 'recordType');
					var ItemRec = nlapiLoadRecord(Itype, actItemid);			
					actItemName=ItemRec.getFieldValue('itemid'); 
				}
			} 
			if(getEnteredItem == getItemName)
			{
				vEnteredItemId=getItemInternalId;
			}	
			 
			var str = 'getEnteredItem. = ' + getEnteredItem + '<br>';			
			str = str + 'getItemName. = ' + getItemName + '<br>';
			str = str + 'getItem. = ' + getItem + '<br>';
			str = str + 'getItemInternalId. = ' + getItemInternalId + '<br>';

			nlapiLogExecution('ERROR', 'Item Details2', str);

			if (vEnteredItemId != '' && vEnteredItemId == getItemInternalId) {
				var ItemTypeRec = nlapiLookupField('item', getItemInternalId, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);				
				var batchflag = ItemTypeRec.custitem_ebizbatchlot;
				var ItemType = ItemTypeRec.recordType;

				var str = 'getWaveNo. = ' + getWaveNo + '<br>';
				 
				str = str + 'getItemInternalId. = ' + getItemInternalId + '<br>';
				   
				nlapiLogExecution('ERROR', 'ItemType', ItemType);
				//If Lotnumbered item the navigate to batch # scan
				if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem" ||batchflag=='T')
				{
					//case# 20127530 starts (Getting lot number for lot item.)
					//case# 20127538 starts (Batch no is not matching with pick report batchno issue)
					var SOFilters=new Array();
					SOFilters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof',getItemInternalId));
					SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is',getWaveNo));
					
					var SOColumns=new Array();
					SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
					
					var SOSearchResults1 = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
					if(SOSearchResults1!=null && SOSearchResults1!='')
						SOarray["custparam_batchno"]=SOSearchResults1[0].getValue('custrecord_batch_no');
					else
						SOarray["custparam_batchno"]='No Lot';
					//case# 20127538 end
					//case# 20127530 end
					SOarray["custparam_itemType"] ='lotnumberedinventoryitem';
					response.sendRedirect('SUITELET', 'customscript_rf_picking_batch', 'customdeploy_rf_picking_batch_di', false, SOarray);					  
				}
				else {

					response.sendRedirect('SUITELET', 'customscript_rf_bulkpick_confirm', 'customdeploy_rf_bulkpick_confirm_di', false, SOarray);
					nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
				}
			}
			else 
			{
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				nlapiLogExecution('DEBUG', 'Error: ', 'Did not scan the location');
			}
		}
	}
}
