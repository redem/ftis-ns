/***************************************************************************
�eBizNET Solutions
 ****************************************************************************
 *
 *� $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_CreateInventorySerialNo.js,v $
 *� $Revision: 1.1.2.1.4.11.2.3 $
 *� $Date: 2015/08/04 15:29:37 $
 *� $Author: skreddy $
 *� $Name: t_eBN_2015_1_StdBundle_1_243 $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_RF_CreateInventorySerialNo.js,v $
 *� Revision 1.1.2.1.4.11.2.3  2015/08/04 15:29:37  skreddy
 *� Case# 201413752
 *� 2015.2 compatibility issue fix
 *�
 *� Revision 1.1.2.1.4.11.2.2  2015/07/15 15:17:56  grao
 *� 2015.2   issue fixes  201412887
 *�
 *� Revision 1.1.2.1.4.11.2.1  2014/11/14 11:51:18  skavuri
 *� Case# 201410964 Std Bundle Issue Fixed
 *�
 *� Revision 1.1.2.1.4.11  2014/06/23 07:45:03  skavuri
 *� Case# 20148882 (added Focus Functionality for Textbox) Issue Fixed.
 *�
 *� Revision 1.1.2.1.4.10  2014/06/13 10:19:25  skavuri
 *� Case# 20148882 (added Focus Functionality for Textbox)
 *�
 *� Revision 1.1.2.1.4.9  2014/05/30 00:34:19  nneelam
 *� case#  20148622
 *� Stanadard Bundle Issue Fix.
 *�
 *� Revision 1.1.2.1.4.8  2014/01/21 15:25:12  nneelam
 *� case#  20126885
 *� Issue fix for Invalid LP in Create Inventory RF.
 *�
 *� Revision 1.1.2.1.4.7  2014/01/06 13:16:03  grao
 *� Case# 20126579 related issue fixes in Sb issue fixes
 *�
 *� Revision 1.1.2.1.4.6  2013/11/08 16:20:30  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *�
 *� Epicuren and surftech issue fixes
 *�
 *� Revision 1.1.2.1.4.5  2013/06/05 22:11:22  spendyala
 *� CASE201112/CR201113/LOG201121
 *� FIFO field updating with data stamp is removed.
 *�
 *� Revision 1.1.2.1.4.4  2013/04/17 16:02:37  skreddy
 *� CASE201112/CR201113/LOG201121
 *� added meta tag
 *�
 *� Revision 1.1.2.1.4.3  2013/04/03 01:51:31  kavitha
 *� CASE201112/CR201113/LOG2012392
 *� Prod and UAT issue fixes.
 *�
 *� Revision 1.1.2.1.4.2  2013/03/26 13:27:42  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Prod and UAT issue fixes.
 *�
 *� Revision 1.1.2.1.4.1  2013/03/22 11:44:30  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Prod and UAT issue fixes.
 *�
 *� Revision 1.1.2.1  2012/11/27 17:50:23  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Serial Capturing file.
 *�
 *
 ****************************************************************************/

function InventorySerial(request, response)
{
	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');

	var user=context.getUser();
	if (request.getMethod() == 'GET') 
	{
		try
		{
			var getNumber = request.getParameter('custparam_number');
			if(getNumber==null||getNumber==""||getNumber=='NaN')
				getNumber=0;
			var locationId = request.getParameter('custparam_locationId');
			nlapiLogExecution('ERROR', 'Location in Create inventory LP Page load::',locationId);	
			var getItemLP;
			var getActualBeginDate = request.getParameter('custparam_actualbegindate');
			var getActualBeginTime = request.getParameter('custparam_actualbegintime')+ ' ' + request.getParameter('custparam_actualbegintimeampm');
			var getBinLocationId = request.getParameter('custparam_binlocationid');
			var getBinLocation = request.getParameter('custparam_binlocationname');
			var getItemId = request.getParameter('custparam_itemid');
			var getItem = request.getParameter('custparam_item');
			var getQuantity = request.getParameter('custparam_quantity');
			nlapiLogExecution('ERROR', 'getQuantity from prevscreen', getQuantity);
			var getItemStatus = request.getParameter('custparam_itemstatus');
			var getItemStatusId = request.getParameter('custparam_itemstatusid');
			var getPackCode = request.getParameter('custparam_packcode');
			var getPackCodeId = request.getParameter('custparam_packcodeid');
			var getAdjustmentType = request.getParameter('custparam_adjustmenttype');
			var getAdjustmentTypeId = request.getParameter('custparam_adjustmenttypeid');
			var sitelocation = request.getParameter('custparam_sitelocation');
			var getBatchId = request.getParameter('custparam_BatchId');
			var getBatchNo = request.getParameter('custparam_BatchNo');
			var getLp = request.getParameter('custparam_itemlp');
			var getInventoryId= request.getParameter('custparam_inventoryId');
			var tempflag=request.getParameter('custparam_flag');
			var notes=request.getParameter('custparam_notes');
			var serialnos=request.getParameter('custparam_serialno');

			var functionkeyHtml=getFunctionkeyScript('_rf_replenishment_serialscan');
			var html = "<html><head><title>CreateInventory Serial Scan</title>";
			html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
			html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
			//Case# 20148749 Refresh Functionality starts
			html = html + "var version = navigator.appVersion;";
			html = html + "document.onkeydown = function (e) {";
			html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
			html = html + "if ((version.indexOf('MSIE') != -1)) { ";
			html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
			html = html + "else {if (keycode == 116)return false;}";
			html = html + "};";
			//Case# 20148749 Refresh Functionality ends
			html = html + "nextPage = new String(history.forward());";          
			html = html + "if (nextPage == 'undefined')";     
			html = html + "{}";     
			html = html + "else";     
			html = html + "{  location.href = window.history.forward();"; 
			html = html + "} ";
			
			//html = html + " document.getElementById('enterserialno').focus();";        
			html = html + "</script>";
			html = html +functionkeyHtml;
			html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
			html = html + "<body>";
			html = html + "	<form name='_rf_replenishment_serialscan' method='POST'>";
			html = html + "		<table>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>LP:  <label>" + getLp + "</label></td>";
			html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
			html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
			html = html + "				<input type='hidden' name='hdnBinLocationId' value=" + getBinLocationId + ">";
			html = html + "				<input type='hidden' name='hdnBinLocation' value=" + getBinLocation + ">";
			html = html + "				<input type='hidden' name='hdnItemId' value=" + getItemId + ">";
			html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
			html = html + "				<input type='hidden' name='hdnQuantity' value=" + getQuantity + ">";
			html = html + "				<input type='hidden' name='hdnPackCode' value=" + getPackCode + ">";
			html = html + "				<input type='hidden' name='hdnPackCodeId' value=" + getPackCodeId + ">";
			html = html + "				<input type='hidden' name='hdnItemStatus' value=" + getItemStatus + ">";
			html = html + "				<input type='hidden' name='hdnItemStatusId' value=" + getItemStatusId + ">";
			html = html + "				<input type='hidden' name='hdnAdjustmentType' value=" + getAdjustmentType + ">";
			html = html + "				<input type='hidden' name='hdnAdjustmentTypeId' value=" + getAdjustmentTypeId + ">";
			html = html + "				<input type='hidden' name='hdnsiteLocation' value=" + sitelocation + ">";
			html = html + "				<input type='hidden' name='hdnLocationName' value=" + locationId+ ">";
			html = html + "				<input type='hidden' name='hdnLocationInternalid' value=" + locationId+ ">";
			html = html + "				<input type='hidden' name='hdnCompname' value=" + locationId + ">";
			html = html + "				<input type='hidden' name='hdnBatchId' value=" + getBatchId + ">";
			html = html + "				<input type='hidden' name='hdnBatchNo' value=" + getBatchNo + ">";
			html = html + "				<input type='hidden' name='hdnlp' value=" + getLp + ">";
			html = html + "				<input type='hidden' name='hdninventoryid' value=" + getInventoryId + ">";
			html = html + "				<input type='hidden' name='hdntempflag' value=" + tempflag + ">";
			html = html + "				<input type='hidden' name='hdngetNumber' value=" + getNumber + ">";
			html = html + "				<input type='hidden' name='hdnnotes' value=" + notes + ">";
			html = html + "				<input type='hidden' name='hdnserialnos' value=" + serialnos + ">";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + (parseInt(getNumber) + 1) + " OF <label>" + getQuantity + "</label>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>ENTER SERIAL NO: ";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'><input name='enterserialno' id='enterserialno' type='text'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>PARENT ID : <label>" + getLp + "</label>";
			html = html + "			</tr>";
			if (sessionobj!=context.getUser()) {
				html = html + "			<tr>";
				html = html + "				<td align = 'left'>SEND <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
				//html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
				html = html + "					 <input type='hidden' name='cmdPrevious' type='submit' value='F7'/>";
				html = html + "				</td>";
				html = html + "			</tr>";
			}
			else
			{
				html = html + "			<tr>";
				html = html + "				<td align = 'left'>SEND <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
				//html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
				html = html + "					 <input type='hidden' name='cmdPrevious' type='submit' value='F7'/>";
				html = html + "				</td>";
				html = html + "			</tr>";
			}
			html = html + "		 </table>";
			html = html + "	</form>";
			//Case# 20148882 (added Focus Functionality for Textbox)
			html = html + "<script type='text/javascript'>document.getElementById('enterserialno').focus();</script>";
			html = html + "</body>";
			html = html + "</html>";
			response.write(html);

		}
		catch(exp)
		{
			nlapiLogExecution('ERROR','Exception in Get',exp);
		}
	}
	else
	{
		try
		{
			var CIarray = new Array();
			var getNumber = request.getParameter('hdngetNumber');
			var getItemQuantity=request.getParameter('hdnQuantity');
			var getSerialNo=request.getParameter('enterserialno');
			var getBinLocationId = request.getParameter('hdnBinLocationId');
			var getBinLocation = request.getParameter('hdnBinLocation');
			var getItemId = request.getParameter('hdnItemId');
			var getItem = request.getParameter('hdnItem');
			var getQuantity = request.getParameter('hdnQuantity');
			nlapiLogExecution('ERROR','getQuantity',getQuantity);
			var getPackCode = request.getParameter('hdnPackCode');
			var getPackCodeId = request.getParameter('hdnPackCodeId');
			var getItemStatus = request.getParameter('hdnItemStatus');
			var getItemStatusId = request.getParameter('hdnItemStatusId');
			var getAdjustmentType = request.getParameter('hdnAdjustmentType');
			var getAdjustmentTypeId = request.getParameter('hdnAdjustmentTypeId');
			var SiteLocation = request.getParameter('hdnsiteLocation');
			var locationId = request.getParameter('hdnLocationInternalid');
			nlapiLogExecution('ERROR','SiteLocation',SiteLocation);
			nlapiLogExecution('ERROR','locationId',locationId);
			var getBatchId = request.getParameter('hdnBatchId');
			var getBatchNo = request.getParameter('hdnBatchNo');
			var getActualBeginDate = request.getParameter('custparam_actualbegindate');
			var getActualBeginTime = request.getParameter('custparam_actualbegintime')+ ' ' + request.getParameter('custparam_actualbegintimeampm');
			var getBaseUOM = 'EACH';
			var getBinLocation = "";
			var lpExists;

			var getItemLP=request.getParameter('hdnlp');
			var inventoryId=request.getParameter('hdninventoryid');
			var tempflag = request.getParameter('hdntempflag');
			var getNotes = request.getParameter('hdnnotes');
			//case# 20126885 starts now we are showing valid msg
			CIarray["custparam_error"] = 'PLEASE ENTER/VALID SERIAL#';
			//end case# 20126885
			CIarray["custparam_binlocationid"] = getBinLocationId;
			CIarray["custparam_binlocationname"] = getBinLocation;
			CIarray["custparam_itemid"] = getItemId;
			CIarray["custparam_item"] = getItem;
			CIarray["custparam_quantity"] = getQuantity;
			CIarray["custparam_itemstatus"] = getItemStatus;
			CIarray["custparam_itemstatusid"] = getItemStatusId;
			CIarray["custparam_packcode"] = getPackCode;
			CIarray["custparam_packcodeid"] = getPackCodeId;
			CIarray["custparam_adjustmenttype"] = getAdjustmentType;
			CIarray["custparam_adjustmenttypeid"] = getAdjustmentTypeId;
			CIarray["custparam_actualbegindate"] = request.getParameter('custparam_actualbegindate');
			CIarray["custparam_locationId"] = locationId;
			CIarray["custparam_screenno"] = '17C';
			CIarray["custparam_BatchId"] = getBatchId;
			CIarray["custparam_BatchNo"] = getBatchNo;
			CIarray["custparam_itemlp"] = getItemLP;      
			CIarray["custparam_inventoryId"] = inventoryId;
			CIarray["custparam_flag"] =tempflag;
			CIarray["custparam_number"] = parseInt(request.getParameter('custparam_number'));
			CIarray["custparam_notes"]=getNotes;
			CIarray["custparam_serialno"] = request.getParameter('custparam_serialno');

			var TimeArray = new Array();
			TimeArray = getActualBeginTime.split(' ');
			CIarray["custparam_actualbegintime"] = TimeArray[0];
			CIarray["custparam_actualbegintimeampm"] = TimeArray[1];
			nlapiLogExecution('ERROR', 'custparam_actualbegintime', CIarray["custparam_actualbegintime"]);
			nlapiLogExecution('ERROR', 'custparam_actualbegintimeampm', CIarray["custparam_actualbegintimeampm"]);


			// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
			// to the previous screen.
			//var optedEvent = request.getParameter('cmdPrevious');
			if (sessionobj!=context.getUser()) {
				try
				{
					if(sessionobj==null || sessionobj=='')
					{
						sessionobj=context.getUser();
						context.setSessionObject('session', sessionobj); 
					}
					var optedEvent = request.getParameter('cmdSend');

					if (optedEvent == 'F7') {
						nlapiLogExecution('ERROR', 'REPLENISHMENT LOCATION F7 Pressed');
						//response.sendRedirect('SUITELET', 'customscript_replen_tolp', 'customdeploy_rf_replen_tolp_di', false, CIarray);
						nlapiLogExecution('ERROR', 'getNumber', getNumber);
						if(getNumber==null||getNumber==""||getNumber=='NaN')
							getNumber=0;
						if((parseInt(getNumber) + 1)==1){

							response.sendRedirect('SUITELET', 'customscript_rf_create_invt_lp', 'customdeploy_rf_create_invt_lp_di', false, CIarray);
						}
						else{
							nlapiLogExecution('ERROR', 'getNumber', getNumber);
							nlapiLogExecution('ERROR', 'TempSerialNoArray', TempSerialNoArray);
							if (request.getParameter('custparam_serialno') != null){
								var str=request.getParameter('custparam_serialno');
								nlapiLogExecution('ERROR', 'str', str);
								var index=str.lastIndexOf(",");
								nlapiLogExecution('ERROR', 'index', index);
								TempSerialNoArray = str.substring(0,index);

								//nlapiLogExecution('ERROR', 'TempSerialNoArray', TempSerialNoArray);
								/*TempSerialNoArray.pop();*/
								nlapiLogExecution('ERROR', 'TempSerialNoArrayInsidePrevious', TempSerialNoArray);
								CIarray["custparam_serialno"]=TempSerialNoArray;
								CIarray["custparam_number"] = parseInt(getNumber) - 1;
								response.sendRedirect('SUITELET', 'customscript_rf_create_invt_serialscan', 'customdeploy_rf_create_invt_serialscan_d', false, CIarray);
							}
						}
					}
					else if (getSerialNo == "") {
						if(request.getParameter('custparam_serialno') != null && request.getParameter('custparam_serialno') != '')
						{
							CIarray["custparam_serialno"] = request.getParameter('custparam_serialno');
						}
						//	if the 'Send' button is clicked without any option value entered,
						//  it has to show an error message. The next screen to which it has to navigate is to the error screen.
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
						nlapiLogExecution('ERROR', 'Enter Serial No', getSerialNo);
						CIarray["custparam_error"] = 'ENTER SERIAL NO';
					}
					else 
					{
						nlapiLogExecution('ERROR', 'Serial No', getSerialNo);
						nlapiLogExecution('ERROR', 'CIarray["custparam_serialno"]', CIarray["custparam_serialno"]);
						getSerialNo=SerialNoIdentification(CIarray["custparam_itemid"],getSerialNo);
						nlapiLogExecution('ERROR', 'After Serial No Parsing', getSerialNo);
						var getActualEndDate = DateStamp();
						var getActualEndTime = TimeStamp();
						var TempSerialNoArray = new Array();
						var TempSerialNoIDArray =new Array();
						if (request.getParameter('custparam_serialno') != null) 
							TempSerialNoArray = request.getParameter('custparam_serialno').split(',');
						nlapiLogExecution('ERROR', 'INTO SERIAL ENTRY');

						//checking serial no's in already scanned one's
						for (var t = 0; t < TempSerialNoArray.length; t++) {
							if (getSerialNo == TempSerialNoArray[t]) {
								if(request.getParameter('custparam_serialno') != null && request.getParameter('custparam_serialno') != '')
								{
									CIarray["custparam_serialno"] = request.getParameter('custparam_serialno');
								}
								CIarray["custparam_error"] = "Serial No. Already Scanned";
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
								return;
							}
						}

						//checking serial no's in records
						var filtersser = new Array();
						filtersser[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getSerialNo);
						filtersser[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

						var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser);
						if (SrchRecord !=null && SrchRecord!="") {
							if(request.getParameter('custparam_serialno') != null && request.getParameter('custparam_serialno') != '')
							{
								CIarray["custparam_serialno"] = request.getParameter('custparam_serialno');
							}
							CIarray["custparam_error"] = "Serial No. Already Exists";
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
							return;
						}
						else {
							// nlapiLogExecution('ERROR', 'SERIAL NO NOT FOUND');
							if (request.getParameter('custparam_serialno') == null || request.getParameter('custparam_serialno') == "") {
								CIarray["custparam_serialno"] = getSerialNo;
							}
							else {
								CIarray["custparam_serialno"] = request.getParameter('custparam_serialno') + ',' + getSerialNo;
							}

							CIarray["custparam_number"] = parseInt(getNumber) + 1;
						}
						TempSerialNoArray.push(getSerialNo);
						nlapiLogExecution('ERROR', '(getNumber + 1)', (parseInt(getNumber) + 1));
						nlapiLogExecution('ERROR', 'getItemQuantity', getItemQuantity);
						if ((parseInt(getNumber) + 1) < parseInt(getItemQuantity)) {
							nlapiLogExecution('ERROR', 'Scanning Serial No.');
							response.sendRedirect('SUITELET', 'customscript_rf_create_invt_serialscan', 'customdeploy_rf_create_invt_serialscan_d', false, CIarray);
							return;
						}
						else 
						{
							nlapiLogExecution('ERROR', 'Inserting Into serialno',CIarray["custparam_serialno"]);
							TempSerialNoArray=CIarray["custparam_serialno"].split(',');
							for (var j = 0; j < TempSerialNoArray.length; j++) 
							{
								nlapiLogExecution('ERROR','Into Create Serial Record');
								var customrecord = nlapiCreateRecord('customrecord_ebiznetserialentry');
								customrecord.setFieldValue('custrecord_serialitem', CIarray["custparam_itemid"]);
								customrecord.setFieldValue('custrecord_serialiteminternalid', CIarray["custparam_itemid"]);
								customrecord.setFieldValue('custrecord_serialparentid', CIarray["custparam_itemlp"]);
								customrecord.setFieldValue('custrecord_serialqty', 1);
								customrecord.setFieldValue('custrecord_serialnumber', TempSerialNoArray[j]);
								customrecord.setFieldValue('custrecord_serialuomid', 1);
								customrecord.setFieldValue('custrecord_serialwmsstatus', '19');
								if(locationId != null && locationId != "")
									customrecord.setFieldValue('custrecord_serial_location', locationId);
								if(getBinLocationId != null && getBinLocationId != "")
									customrecord.setFieldValue('custrecord_serialbinlocation', getBinLocationId);
								var rec = nlapiSubmitRecord(customrecord, false, true);
								nlapiLogExecution('ERROR','Recorde created',rec);
							}


							//checking whether inventory should be created or updated based on the location,bin location,sku,sku status,packcode
							//if exists update the qty in inventory else create a new record.						
							var filtersinv = new Array();
							filtersinv.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc',null, 'anyof',locationId));	
							filtersinv.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc',null, 'anyof',getBinLocationId));	
							filtersinv.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku',null, 'anyof',getItemId));
							filtersinv.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status',null, 'anyof',getItemStatusId));
							filtersinv.push(new nlobjSearchFilter('custrecord_ebiz_inv_packcode',null, 'anyof',getPackCode));
							filtersinv.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag',null, 'anyof',[3,19]));
							if(getBatchId!=null && getBatchId!="")
								filtersinv.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot',null,'anyof',getBatchId));
							var column = new Array();
							column[0]=new nlobjSearchColumn('custrecord_ebiz_qoh').setSort('True');

							nlapiLogExecution('ERROR', 'locationId::',locationId);
							nlapiLogExecution('ERROR', 'getBinLocationId::',getBinLocationId);
							nlapiLogExecution('ERROR', 'getItemId::',getItemId);
							nlapiLogExecution('ERROR', 'getItemStatusId::',getItemStatusId);
							nlapiLogExecution('ERROR', 'getPackCodeId::',getPackCode);

							var SrchINVRecord = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinv,column);

							if (SrchINVRecord != null && SrchINVRecord.length > 0) 
							{
								var id='';// Case# 201410964
								nlapiLogExecution('ERROR', 'SrchINVRecord.length::',SrchINVRecord.length);
								var tempflag=0;
								nlapiLogExecution('ERROR', 'Inventory Updated Mode::');
								var maxuomqty= getmaxuomqty(getItemId);
								for(var count=SrchINVRecord.length-1;count >= 0;count--)
								{
									var TotalQty = parseInt(SrchINVRecord[count].getValue('custrecord_ebiz_qoh')) + parseInt(getQuantity);
									nlapiLogExecution('ERROR','TOTALQTY',TotalQty);
									nlapiLogExecution('ERROR','MAXQTY',maxuomqty);
									if(parseInt(TotalQty) <= parseInt(maxuomqty))
									{
										tempflag=1;
										//var id=SrchINVRecord[count].getId();
										 id=SrchINVRecord[count].getId();//Case# 201410964
										break;
									}
								}
								nlapiLogExecution('ERROR', 'Inventory id in OverWriteLP is ::'+ id);
								CIarray["custparam_inventoryId"] = id;
								CIarray["custparam_flag"] = 1;
								nlapiLogExecution('ERROR', 'Record Exists in Inventory::');
								nlapiLogExecution('ERROR', 'inventoryId',inventoryId);
								if(inventoryId != null && inventoryId !=  "")
								{



									var lot = TempSerialNoArray;
									var InvrecLoad = nlapiLoadRecord('customrecord_ebiznet_createinv', id);
									var loc=InvrecLoad.getFieldValue('custrecord_ebiz_inv_loc');
									var tasktype=10;
									InvokeNSInventoryAdjustment(getItemId,getItemStatusId,loc,getQuantity,"",tasktype,getAdjustmentTypeId,lot);

									response.sendRedirect('SUITELET', 'customscript_rf_create_invt_complete', 'customdeploy_rf_create_invt_complete_di', false, CIarray);
								}
								else
								{						
									CreateInventory(getItemId, getItem, getItemStatusId, getItemStatus, getPackCodeId, getPackCode, 
											getAdjustmentTypeId, getAdjustmentType, getItemLP, getBinLocation, getBinLocationId, 
											getQuantity, getActualEndDate, getActualEndTime, getActualBeginDate, getActualBeginTime, 
											SiteLocation,locationId,getNotes,getBatchId,'');

									response.sendRedirect('SUITELET', 'customscript_rf_create_invt_complete', 'customdeploy_rf_create_invt_complete_di', false, CIarray);						
								}
							}
							else
							{							
								nlapiLogExecution('ERROR', 'into create inventory of Different LP');
								CreateInventory(getItemId, getItem, getItemStatusId, getItemStatus, getPackCodeId, getPackCode, 
										getAdjustmentTypeId, getAdjustmentType, getItemLP, getBinLocation, getBinLocationId, 
										getQuantity, getActualEndDate, getActualEndTime, getActualBeginDate, getActualBeginTime, 
										SiteLocation,locationId,getNotes,getBatchId,'');				                				                
								nlapiLogExecution('ERROR', 'Created', 'Successfully');

								response.sendRedirect('SUITELET', 'customscript_rf_create_invt_complete', 'customdeploy_rf_create_invt_complete_di', false, CIarray);							
							}
						}
					}
				}
				catch (e)  {
					CIarray["custparam_error"] = "Inventory creation Failed";
					nlapiLogExecution('ERROR', 'Exception', e);
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
					return;
				} finally {					
					context.setSessionObject('session', null);
//					if ((parseInt(getNumber) + 1) < parseInt(getItemQuantity)) {
//						nlapiLogExecution('ERROR', 'Scanning Serial No.');
//						response.sendRedirect('SUITELET', 'customscript_rf_create_invt_serialscan', 'customdeploy_rf_create_invt_serialscan_d', false, CIarray);
//						return;
//					}
//					else
//						{
//						response.sendRedirect('SUITELET', 'customscript_rf_create_invt_complete', 'customdeploy_rf_create_invt_complete_di', false, CIarray);
//						}
				}
			}
			else
			{
				CIarray["custparam_error"] = "Serial No. Already Exists";
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
				return;
			}
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR','Exception in Post',exp);
			CIarray["custparam_error"] = exp;
			CIarray["custparam_screenno"] = '15LOC';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CIarray);
			return;
		}
	}
}


/**
 * @param ItemId
 * @param Item
 * @param ItemStatusId
 * @param ItemStatus
 * @param PackCodeId
 * @param PackCode
 * @param AdjustmentTypeId
 * @param AdjustmentType
 * @param ItemLP
 * @param BinLocation
 * @param BinLocationId
 * @param Quantity
 * @param ActualEndDate
 * @param ActualEndTime
 * @param ActualBeginDate
 * @param ActualBeginTime
 * @param SiteLocation
 * @param locationId
 * @param Notes
 */
function CreateInventory(ItemId, Item, ItemStatusId,ItemStatus, PackCodeId, PackCode, AdjustmentTypeId, AdjustmentType, ItemLP, BinLocation, BinLocationId, Quantity, ActualEndDate, ActualEndTime, ActualBeginDate, ActualBeginTime, SiteLocation,locationId,Notes,lot,fifodate)
{
	//create a inventory record
	var CreateInventoryRecord = nlapiCreateRecord('customrecord_ebiznet_createinv');
	nlapiLogExecution('ERROR', 'Create Inventory Record', 'Inventory Record');

	CreateInventoryRecord.setFieldValue('custrecordact_begin_date', ActualBeginDate);
	CreateInventoryRecord.setFieldValue('custrecord_act_end_date', ActualEndDate);
	//Adding fields to update time zones.
	CreateInventoryRecord.setFieldValue('custrecord_actualbegintime', ActualBeginTime);
	CreateInventoryRecord.setFieldValue('custrecord_actualendtime', ActualEndTime);
	CreateInventoryRecord.setFieldValue('custrecord_recordtime', TimeStamp());
	CreateInventoryRecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
	CreateInventoryRecord.setFieldValue('custrecord_current_date', DateStamp());
	CreateInventoryRecord.setFieldValue('custrecord_upd_date', DateStamp());

	//Status flag .
	//customrecord.setFieldValue('custrecord_status_flag', 'S');
	CreateInventoryRecord.setFieldValue('custrecord_wms_inv_status_flag', 19);
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_qoh', Quantity);
	CreateInventoryRecord.setFieldValue('custrecord_invttasktype', 10);//PackCodeId);
	//
	//Added for Item name and desc
	CreateInventoryRecord.setFieldValue('custrecord_sku', Item);
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_sku', ItemId);
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_qty', Quantity);
//	CreateInventoryRecord.setFieldValue('custrecord_skudesc', ItemDesc);
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_sku_status', ItemStatusId);
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_packcode', 1);//PackCodeId);
	//Added on 20Feb by suman
	//checking the condition whether lot is null or not,If it is not null then allow to pass the value to the field.
	if(lot!=null&&lot!="")
	{
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_lot', lot);	
		var rec=nlapiLookupField('customrecord_ebiznet_batch_entry',lot,['custrecord_ebizfifodate','custrecord_ebizexpirydate']);
		var fifodate=rec.custrecord_ebizfifodate;
		var expdate=rec.custrecord_ebizexpirydate;
		if(fifodate!=null && fifodate!='')
			CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_fifo',fifodate);
		/*else
			CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_fifo',DateStamp());*/
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_expdate', expdate);
	}
	/*else
	{
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_fifo',DateStamp());
	}*/
	//


	/*
		var filtersAccNo = new Array();
        filtersAccNo[0] = new nlobjSearchFilter('custrecord_location', null, 'is', SiteLocation);
        var colsAcc = new Array();
        colsAcc[0]=new nlobjSearchColumn('custrecord_accountno');                
        var Accsearchresults = nlapiSearchRecord('customrecord_inventoryaccountno', null, filtersAccNo, colsAcc);              
        var varAccountNo = Accsearchresults[0].getValue('custrecord_accountno');                                
        nlapiLogExecution('ERROR', 'Account ',varAccountNo);
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_account_no', varAccountNo);//Account #);
	 */

	//code added on 01/02/12 by suman.
	//without passing this value system throughing an error stating that a/c no ref is invalid .
	//So to overcome that bug v are passing empty value.
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_account_no', '');
	//end of code added on 01/02/12

	CreateInventoryRecord.setFieldValue('custrecord_ebiz_callinv', 'Y'); //Added by ramana for invoking NSInventory;
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_displayfield', 'N');

	nlapiLogExecution('ERROR', 'Location id fetched are', locationId);

	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_loc',locationId);

	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_lp', ItemLP);//Item LP #);
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_binloc', BinLocationId);//Bin Location);
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_note1', Notes);//Notes1);

	//Added by suman as on 090812
	//updating Adjustment type and Company while creating invt record.
	nlapiLogExecution('ERROR','AdjustmentTypeId',AdjustmentTypeId+'/'+AdjustmentType);
	if(AdjustmentTypeId!=null&&AdjustmentTypeId!="")
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_adjusttype', AdjustmentTypeId);

	var compId=getCompanyId();
	if(compId!=null&&compId!="")
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_company', compId);

	//end of code as of 090812.



	//code added on 130812 by suman.
	//Code to update user id and recorded time.
	var currentContext = nlapiGetContext();
	var currentUserID = currentContext.getUser();
	CreateInventoryRecord.setFieldValue('custrecord_updated_user_no',currentUserID);
	CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_updatedtime',TimeStamp());
	//end of code as of 130812.


	nlapiLogExecution('ERROR', 'Submitting Create Inventory Record', 'Inventory Record');

	//commit the record to NetSuite
	var CreateInventoryRecordId = nlapiSubmitRecord(CreateInventoryRecord, false, true);

	nlapiLogExecution('ERROR', 'Inventory Creation', 'Success');

	try 
	{
		nlapiLogExecution('ERROR', 'INTO MASTER LP INSERTION');
		var filtersmlp = new Array();
		filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', ItemLP);

		var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

		if (SrchRecord != null && SrchRecord.length > 0) 
		{
			nlapiLogExecution('ERROR', 'LP FOUND');			
		}
		else 
		{
			nlapiLogExecution('ERROR', 'LP NOT FOUND');
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
			customrecord.setFieldValue('name', ItemLP);
			customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', ItemLP);
			var rec = nlapiSubmitRecord(customrecord, false, true);
			nlapiLogExecution('ERROR', 'Master LP Insertion', 'Success');
		}
	} 
	catch (e) 
	{
		nlapiLogExecution('ERROR', 'Failed to Update/Insert into Master LP Record');
	}

	/*	var arrDims = getSKUCubeAndWeight(ItemId, 1);
	var itemCube = 0;
	if (arrDims[0] != "" && (!isNaN(arrDims[0]))) 
	{
		var uomqty = ((parseFloat(Quantity))/(parseFloat(arrDims[1])));			
		itemCube = (parseFloat(uomqty) * parseFloat(arrDims[0]));
		nlapiLogExecution('ERROR', 'itemCube', itemCube);
	} 

	var vOldRemainingCube = GeteLocCube(BinLocationId);
	nlapiLogExecution('ERROR', 'vOldRemainingCube', vOldRemainingCube);

	var vTotalCubeValue = parseFloat(itemCube) - parseFloat(vOldRemainingCube);
	nlapiLogExecution('ERROR', 'vOldLocationRemainingCube', vTotalCubeValue);

	var retValue =  UpdateLocCube(BinLocationId,vTotalCubeValue);
	nlapiLogExecution('ERROR', 'Loc Cube Updation', 'Success');*/
}

/**
 * @param getItemId
 * @returns {Number}
 */
function getmaxuomqty(getItemId)
{
	nlapiLogExecution('ERROR', 'Into getmaxuomqty','');
	nlapiLogExecution('ERROR', 'Item ID',getItemId);

	var maxuomqty=0;
	var filtersmlp = new Array();

	filtersmlp.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', getItemId));
//	filtersmlp.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var col=new Array();
	col[0] = new nlobjSearchColumn('custrecord_ebizqty');
	col[0].setSort('True');

	var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filtersmlp,col);
	if (SrchRecord != null && SrchRecord !="" && SrchRecord.length>0)
	{
		maxuomqty = SrchRecord[0].getValue('custrecord_ebizqty');
	}

	nlapiLogExecution('ERROR', 'Out of getmaxuomqty',maxuomqty);
	return maxuomqty;
}

function getCompanyId()
{
	try{
		var CompName;	
		var CompanyFilters = new Array();
		//CompanyFilters.push(new nlobjSearchFilter('internalid',null,'is',1));
		CompanyFilters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
		var CompanyColumn = new Array();	
		CompanyColumn.push(new nlobjSearchColumn('custrecord_company'));		
		var CompanyResults = nlapiSearchRecord('customrecord_ebiznet_company', null, CompanyFilters,CompanyColumn);	    
		if(CompanyResults!=null)
		{
			CompName=CompanyResults[0].getId();
		}
		return CompName;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in getCompanyId',exp);

	}
}


function  InvokeNSInventoryAdjustment(item,itemstatus,loc,qty,notes,tasktype,adjusttype,lot,date,period)
{
	

	nlapiLogExecution('Debug', 'Location info ::', loc);
	nlapiLogExecution('Debug', 'Task Type ::', tasktype);
	nlapiLogExecution('Debug', 'Adjustment Type ::', adjusttype);
	nlapiLogExecution('Debug', 'Qty ::', qty);
	nlapiLogExecution('Debug', 'Item ::', item);

	var confirmLotToNS='Y';
	confirmLotToNS=GetConfirmLotToNS(loc);

	var vAccountNo;	
	//Code for fetching account no from stockadjustment custom record	
	vAccountNo = getStockAdjustmentAccountNo(loc,tasktype,adjusttype);			
	nlapiLogExecution('Debug', 'Fetcht Account from stockadjustment', vAccountNo);		
	//upto to here stockadjustment

	var vItemMapLocation =  getItemStatusMapLoc(itemstatus);	
	nlapiLogExecution('Debug', 'vItemMapLocation', vItemMapLocation);	

	if(vAccountNo==null || vAccountNo=="")
	{		
		vAccountNo = getAccountNo(loc,vItemMapLocation);
		nlapiLogExecution('Debug', 'Fetcht Account from Inventory AccountNo', vAccountNo);
	}

	var vCost;
	var vAvgCost;
	var vItemname;
	var avgcostlot;

	var filters = new Array();          
	filters.push(new nlobjSearchFilter('internalid', null, 'is',item));
	// Changed On 30/4/12 by Suman
	filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
	// End of Changes as On 30/4/12
	//Case # 20149077 Start
	if(loc !=null && loc!='' && loc!='null' && loc!='undefined' && loc > 0)
	{
		filters.push(new nlobjSearchFilter('inventorylocation',null, 'anyof',loc));
	}
	//Case # 20149077 end

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('cost');
	//columns[1] = new nlobjSearchColumn('averagecost');

	//As per email from NS on  02-May-2012  we have changed the code to use 'locationaveragecost' as item unit cost
	//Email from Thad Johnson to Sid
	columns[1] = new nlobjSearchColumn('locationaveragecost');

	columns[2] = new nlobjSearchColumn('itemid');

	//As per recommendation from NS PM Jeff hoffmiester This field 'custitem16' is used as avg cost for Default lot updation in Dec 2011 for TNT
	//columns[3] = new nlobjSearchColumn('custitem16');

	var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
	if (itemdetails !=null) 
	{
		vItemname=itemdetails[0].getValue('itemid');
		vCost = itemdetails[0].getValue('cost');
		nlapiLogExecution('Debug', 'vCost', vCost);
		//vAvgCost = itemdetails[0].getValue('averagecost');
		vAvgCost = itemdetails[0].getValue('locationaveragecost');
		//As per recommendation from NS PM Jeff hoffmiester This field custitem16 is used as avg cost for Default lot updation in Dec 2011 for TNT
		//avgcostlot=itemdetails[0].getValue('custitem16');
		//nlapiLogExecution('Debug', 'avgcostlot', avgcostlot);	
		nlapiLogExecution('Debug', 'Average Cost', vAvgCost);	         
	}		

	var outAdj = nlapiCreateRecord('inventoryadjustment');		
	if(vAccountNo!=null&&vAccountNo!=""&&vAccountNo!="null")
		outAdj.setFieldValue('account', vAccountNo);
	outAdj.setFieldValue('memo', notes);
	outAdj.selectNewLineItem('inventory');
	outAdj.setCurrentLineItemValue('inventory', 'item', item);
	outAdj.setCurrentLineItemValue('inventory', 'location',vItemMapLocation);	
	//Case# 201410949 starts
	//outAdj.setCurrentLineItemValue('inventory', 'adjustqtyby',parseInt(qty));
	outAdj.setCurrentLineItemValue('inventory', 'adjustqtyby',parseFloat(qty));
	//Case# 201410949 ends
	if(date !=null && date!='' && date!='null' && date!='undefined')
	{
		outAdj.setFieldValue('trandate', date);
	}
	if(period !=null && period!='' && period!='null' && period!='undefined')
	{
		outAdj.setFieldValue('postingperiod', period);
	}
	if(vAvgCost != null &&  vAvgCost != "")	
	{
		nlapiLogExecution('Debug', 'into if cond vAvgCost', vAvgCost);
		outAdj.setCurrentLineItemValue('inventory', 'unitcost',vAvgCost);
	}
	else
	{
		nlapiLogExecution('Debug', 'into else cond.unit cost', vCost);
		outAdj.setCurrentLineItemValue('inventory', 'unitcost', vCost);
	}
	//outAdj.setLineItemValue('inventory', 'unitcost', 1, avgcostlot);//avg cost send to inventory
	//outAdj.setFieldValue('subsidiary', 3);

	//This following line will return 'true' for ONE WORLD Account, 'false' for non-ONE WORLD Account.
	var subs = nlapiGetContext().getFeature('subsidiaries');
	nlapiLogExecution('Debug', 'subs', subs);
	if(subs==true)
	{
		var vSubsidiaryVal=getSubsidiaryNew(loc);
		nlapiLogExecution('Debug','vSubsidiaryVal',vSubsidiaryVal);
		if(vSubsidiaryVal != null && vSubsidiaryVal != '')
			outAdj.setFieldValue('subsidiary', vSubsidiaryVal);
	}


	//Added by Sudheer on 093011 to send lot# for inventory posting
	nlapiLogExecution('Debug', 'lot', lot);
	if(lot!=null && lot!='')
	{
		//vItemname=vItemname.replace(" ","-");
		vItemname=vItemname.replace(/ /g,"-");


		var fields = ['recordType', 'custitem_ebizserialin'];
		var columns = nlapiLookupField('item', item, fields);
		var vItemType = columns.recordType;
		nlapiLogExecution('Debug','vItemType',vItemType);

		var serialInflg="F";		
		serialInflg = columns.custitem_ebizserialin;
		nlapiLogExecution('Debug','serialInflg',serialInflg);

		nlapiLogExecution('Debug', 'lot', lot);
		nlapiLogExecution('Debug', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");
//		For advanced Bin serial Lot management check
		var vAdvBinManagement=false;
		var ctx = nlapiGetContext();
		if(ctx != null && ctx != '')
		{
			if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
				vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
		}  
		nlapiLogExecution('Debug', 'vAdvBinManagement', vAdvBinManagement);

		//if (vItemType == "serializedinventoryitem" || vItemType == "serializedassemblyitem" || serialInflg == "T") {
		if (vItemType == "serializedinventoryitem" || vItemType == "serializedassemblyitem"){
			nlapiLogExecution('Debug','lot',lot);
			try
			{
			if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
			{
				//case 20126071 start : posting serial numbers for AdvanceBinmanagement
				if(lot !=null && lot !='')
				{
					//case # 20126232ï¿½ start
					var tempQty;

					if(parseFloat(qty)<0)
					{
						tempQty=-1;
					}
					else
					{
						tempQty=1;
					}
					//case # 20126232Â  end
				//	var SerialArray = lot.split(',');
					var SerialArray = lot;
					if(SerialArray !=null && SerialArray !='' && SerialArray.length>0)
					{
						nlapiLogExecution('Debug','SerialArray.length',SerialArray.length);
						var compSubRecord = outAdj.createCurrentLineItemSubrecord('inventory','inventorydetail');
						for (var x = 0; x < SerialArray.length; x++)
						{
							nlapiLogExecution('Debug','SerialArray',SerialArray[x]);
							/*if(x==0)
							var compSubRecord = outAdj.createCurrentLineItemSubrecord('inventory','inventorydetail');*/
							compSubRecord.selectNewLineItem('inventoryassignment');
							//case # 20126232ï¿½ start
							compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', tempQty);
							//case # 20126232ï¿½ end
							compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', SerialArray[x]);

							compSubRecord.commitLineItem('inventoryassignment');

						}
						compSubRecord.commit();
					}
				}
				//case 20126071 end
			}
			else
			{
				if(parseFloat(qty)<0)
					qty=parseFloat(qty)*(-1);
				nlapiLogExecution('Debug', 'LotNowithQty', lot + "(" + parseInt(qty) + ")");
				//outAdj.setLineItemValue('inventory', 'serialnumbers', 1, lot);
				outAdj.setCurrentLineItemValue('inventory', 'serialnumbers',lot);
				}
			}
			catch(exp)
			{
				nlapiLogExecution('Debug','exp',exp);
			}
		}
		else if (vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem")
		{
			if(confirmLotToNS=='N')
				lot=vItemname;
			if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
			{
				var compSubRecord = outAdj.createCurrentLineItemSubrecord('inventory','inventorydetail');
				compSubRecord.selectNewLineItem('inventoryassignment');
				compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', qty);
				compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lot);

				compSubRecord.commitLineItem('inventoryassignment');
				compSubRecord.commit();
			}
			else
			{
				if(parseFloat(qty)<0)
					qty=parseFloat(qty)*(-1);
				nlapiLogExecution('Debug', 'LotNowithQty', lot + "(" + parseInt(qty) + ")");
				//outAdj.setLineItemValue('inventory', 'serialnumbers', 1, lot + "(" + parseFloat(qty) + ")");
				outAdj.setCurrentLineItemValue('inventory', 'serialnumbers',lot + "(" + parseFloat(qty) + ")");
			}
		}
	}
	outAdj.commitLineItem('inventory');
	var id=nlapiSubmitRecord(outAdj, false, true);
	nlapiLogExecution('Debug', 'type argument', 'type is create');
	nlapiLogExecution('Debug', 'type argument id', id);
	return id;
}