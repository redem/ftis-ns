/***************************************************************************
eBizNET Solutions
 ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Suitelet/Attic/ebiz_po_receipt_report.js,v $
 *     	   $Revision: 1.1.2.1 $
 *     	   $Date: 2014/05/06 15:13:29 $
 *     	   $Author: skavuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 *$Log: ebiz_po_receipt_report.js,v $
 *Revision 1.1.2.1  2014/05/06 15:13:29  skavuri
 *Case# 20126814
 *SB Issue Fixed
 *
 *Revision 1.1.2.2  2014/05/01 15:34:25  skavuri
 *Case# 20145823 issue fixed
 *
 *Revision 1.1.2.1  2014/04/08 11:24:53  skavuri
 *this file moving from reports to customizations
 *
 *Revision 1.1.2.6  2014/01/30 15:34:55  skavuri
 *case# 20126935 Carrier Name is displayed, po qty displayed
 *
 *Revision 1.1.2.5  2014/01/22 15:55:01  nneelam
 *case#  20126912
 *MHP PO Receipt Report
 *
 *Revision 1.1.2.4  2014/01/21 15:31:25  nneelam
 *case#  20126814
 *MHP PO Receipt Report
 *
 *Revision 1.1.2.3  2014/01/20 16:18:14  nneelam
 *case#  20126814
 *MHP PO Receipt Report
 *
 *Revision 1.1.2.2  2014/01/20 15:37:34  nneelam
 *case#  20126814
 *MHP PO Receipt Report
 *
 *Revision 1.1.2.1  2014/01/20 15:08:55  nneelam
 *case#  20126814
 *MHP PO Receipt Report
 *
 *Revision 1.1.2.1  2014/01/20 14:14:33  nneelam
 *case#  20126814
 *MHP PO Receipt Report
 *
 */
function PO_RECEIPTREPORT (request,response)
{
	
	if(request.getMethod()=='GET')
		{
			var form=nlapiCreateForm('PO Receipt Report');
			form.addField('custpage_poid','text','PO#');//Here need to change it to text type
			form.addSubmitButton('Display');
			response.writePage(form);
		}
	else
		{
			var vpoid=request.getParameter('custpage_poid');
			nlapiLogExecution('ERROR','vpoid',vpoid);
	
			var form=nlapiCreateForm('PO Receipt Report');
	
			var po1=form.addField('custpage_poid','text','PO#');//Here also need to change it to text
			po1.setDefaultValue(vpoid);
	
			form.addSubmitButton('Display');
	
			var item,poline,poqty,potranid,vendorname,podate;
	
			var form,lineno,sku,upccode,lotno,expdate,cases,pallets,unitspercase,recvdate,closedtask_qty=0,enterby;
			var index=0;
			// case# 20126935 starts
			var POorderqty=0;
			//	case# 20126935 end
			var uom_type,uomqty;
	
	
			var sublist = form.addSubList("custpage_sublist", "list", "PO Receipt Report");
	
			sublist.addField('custpage_internalid','text','Internal Id').setDisplayType('hidden');
			sublist.addField('custpage_line','text','Line');
			sublist.addField('custpage_sku','text','SKU');
			sublist.addField('custpage_mfgups','text','MFG UPS');
			sublist.addField('custpage_lot','text','LOT#');
			sublist.addField('custpage_expdate','text','EXP DATE');
			sublist.addField('custpage_pallets','text','#PALLETS');
			sublist.addField('custpage_cases','text','#CASES');
			sublist.addField('custpage_unitspercase','text','UNITS PER CASE');
			sublist.addField('custpage_receptdate','text','RECEIPT DATE');
		
		//case # 20126912 start, Label heading changed.
		
			sublist.addField('custpage_totalrecived','text','TOTAL RECEIVED');
			sublist.addField('custpage_poqty','text','PO QTY');
			sublist.addField('custpage_enterby','text','ENTERED BY');
	
	
	
	//		Search with po number starts
			var pointid="";
			if(vpoid!=null&&vpoid!="")
				pointid=GetPOInternalId(vpoid);
			nlapiLogExecution('ERROR','pointid',pointid);
	
	
			// open task
			var otfilters=new Array();
	
			if(pointid !=null && pointid !='')
				otfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no',null,'is',pointid));
			otfilters.push(new nlobjSearchFilter('mainline',"custrecord_ebiz_order_no",'is',"T"));
			otfilters.push(new nlobjSearchFilter('custrecord_tasktype',null,'anyof',"2")); // for putaway confirm only records
			otfilters.push(new nlobjSearchFilter('custrecord_wms_status_flag',null,'anyof',"3")); // for putaway confirm only records
	
			var otcolumns=CreateColumnsot();
			otsearchrecords=new nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,otfilters,otcolumns);
			nlapiLogExecution('ERROR','Open Task records length',otsearchrecords.length);
			// end of open task
	
			// closed task
			var ctfilters=new Array();
	
			if(pointid !=null && pointid !='')
				ctfilters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no',null,'is',pointid));
			ctfilters.push(new nlobjSearchFilter('mainline',"custrecord_ebiztask_ebiz_order_no",'is',"T"));
			ctfilters.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype',null,'anyof',"2")); // for putaway confirm only records
			ctfilters.push(new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag',null,'anyof',"3")); // for putaway confirm only records
	
			var columns=CreateColumnsct();
			searchrecords=new nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask',null,ctfilters,columns);
			nlapiLogExecution('ERROR','Closed Task records length',searchrecords.length);
			// end of closed task
	
	
	
			var porecArray=new Array();
			if(otsearchrecords!=null&&otsearchrecords!='')
			{
				for(var i=0;i<otsearchrecords.length;i++)
				{
					porecArray.push(otsearchrecords[i].getValue("custrecord_ebiz_cntrl_no",null,"group"))
	
				}
			}
	
			if(searchrecords!=null&&searchrecords!='')
			{
				for(var i=0;i<searchrecords.length;i++)
				{
					porecArray.push(searchrecords[i].getValue("custrecord_ebiztask_ebiz_cntrl_no",null,"group"))
	
				}
			}
			var result="";
			if(porecArray!=null&&porecArray!='')
			 result=GetPuracahseOrderValues(porecArray);
			nlapiLogExecution('ERROR','result',result.length);
			
			// for pdf report values
			var carrier,fob,podate,expshipdate,exparrdate,vendoraddres;
			if(result!=null && result!='')
			{
			for(var i=0;i<result.length;i++)
				{
				//carrier=result[i].getValue("custbody_poshippingitem");
				//case# 20126935 starts
				carrier=result[i].getText("custbody_poshippingitem");
				//case# 20126935 end
				nlapiLogExecution('ERROR','carrier',carrier);
				podate=result[i].getValue("trandate");
				expshipdate=result[i].getValue("custbody_nswmspoexpshipdate");
				exparrdate=result[i].getValue("custbody_nswmspoarrdate");
				 vendoraddres=result[i].getValue('billaddress');
				 nlapiLogExecution('ERROR','vendoraddres',vendoraddres);
				}
			
			}
		
		// for item dimensions getting sku items into an array,for compare the
		// opentask,closetask iems
		
		var itemdimensionArray=new Array();
		if(otsearchrecords!=null&&otsearchrecords!='')
		{
			for(var i=0;i<otsearchrecords.length;i++)
			{
			
			itemdimensionArray.push(otsearchrecords[i].getValue("custrecord_sku",null,"group"))
			}
		}

		if(searchrecords!=null&&searchrecords!='')
		{
			for(var i=0;i<searchrecords.length;i++)
			{
			
			itemdimensionArray.push(searchrecords[i].getValue("custrecord_ebiztask_sku",null,"group"))
			}
		}
		var itemdimresult="";
		if(itemdimensionArray!=''&&itemdimensionArray!=null)
		 itemdimresult=GetItemdimensionvalues(itemdimensionArray); // to
																		// retive
																		// all
																		// dimension
																		// from
																		// itemdimension
																		// record
		nlapiLogExecution('ERROR','itemdimresult length',itemdimresult.length);
		// opentask results start
		if(otsearchrecords!=null&&otsearchrecords!='')
		{
			for(var i=0;i<otsearchrecords.length;i++)
			{
				potranid=otsearchrecords[i].getValue("tranid","custrecord_ebiz_order_no","max");
				
				lineno=otsearchrecords[i].getValue("custrecord_line_no",null,"group");
				
				sku=otsearchrecords[i].getText("custrecord_sku",null,"group");
				
				var skuid=otsearchrecords[i].getValue("custrecord_sku",null,"group");
		
				upccode=otsearchrecords[i].getValue("upccode","custrecord_sku","max");
				
				lotno=otsearchrecords[i].getValue("custrecord_batch_no",null,"group");
				
				expdate=otsearchrecords[i].getValue('custrecord_expirydate',null,"max");
				
				recvdate=otsearchrecords[i].getValue('custrecord_act_end_date',null,"max");

				closedtask_qty=otsearchrecords[i].getValue('custrecord_act_qty',null,"sum");
				nlapiLogExecution('ERROR','Open task QTY ',closedtask_qty);
				
				enterby=otsearchrecords[i].getText('custrecord_taskassignedto',null,"group");
				
				// for pdf fields


				// pallets,cases,unitpercase
				nlapiLogExecution('ERROR','itemdimresult.length ',itemdimresult.length);
				var noofeaches,noofcases,noofpallets,veachuomqty,vcaseuomqty,vpalletuomqty,vremQty;
				noofpallets = 0;
				noofcases = 0;
				noofeaches = 0;
				vremQty = 0;
				for(var item=0;item<itemdimresult.length;item++)
				{
					var vitems=itemdimresult[item].getText('custrecord_ebizitemdims');
					nlapiLogExecution('ERROR','vitems',vitems);
					if(sku==vitems)
					{
						var vuom=itemdimresult[item].getValue('custrecord_ebizuomskudim');

						var vqty=itemdimresult[item].getValue('custrecord_ebizqty');
						nlapiLogExecution('ERROR','vqty',vqty);
						

						/*// var eBizItemDims = geteBizItemDimensions1(skuid);
				var eBizItemDims = GetItemdimensionvalues(sku);
				nlapiLogExecution('ERROR','eBizItemDims',eBizItemDims);
				if(eBizItemDims != null && eBizItemDims != '')
				{
					for(var i1= 0;i1< eBizItemDims.length;i1++)
					{
						var vUomlevel = eBizItemDims[i1].getValue('custrecord_ebizuomlevelskudim');
						if(vUomlevel=='3')
						{
							vpalletuomqty = eBizItemDims[i1].getValue('custrecord_ebizqty');
							//nlapiLogExecution('ERROR','vpalletuomqty',vpalletuomqty);
						}
						else
							if(vUomlevel=='2')
							{
								vcaseuomqty = eBizItemDims[i1].getValue('custrecord_ebizqty');
								//nlapiLogExecution('ERROR','vcaseuomqty',vcaseuomqty);
							}
							else
								if(vUomlevel=='1')
								{
									veachuomqty = eBizItemDims[i1].getValue('custrecord_ebizqty');
									//nlapiLogExecution('ERROR','veachuomqty',veachuomqty);

								}
					}
				}*/

						var vUomlevel = itemdimresult[item].getValue('custrecord_ebizuomlevelskudim');
						if(vUomlevel=='3')
						{
							vpalletuomqty = itemdimresult[item].getValue('custrecord_ebizqty'); 
							//nlapiLogExecution('ERROR','vpalletuomqty',vpalletuomqty);
						}
						else
							if(vUomlevel=='2')
							{
								vcaseuomqty = itemdimresult[item].getValue('custrecord_ebizqty'); 

								//nlapiLogExecution('ERROR','vcaseuomqty',vcaseuomqty);
							}
							else
								if(vUomlevel=='1')
								{
									veachuomqty = itemdimresult[item].getValue('custrecord_ebizqty');

									//nlapiLogExecution('ERROR','veachuomqty',veachuomqty);

								}
					}
				}
						vremQty=closedtask_qty;

						if(vremQty>0 && parseFloat(vpalletuomqty)>0){
							noofpallets = getSKUDimCount(vremQty, vpalletuomqty);
							vremQty = parseFloat(vremQty) - (noofpallets * vpalletuomqty);
							nlapiLogExecution('ERROR','vremQty pallet',vremQty);
						}

						if(vremQty>0 && parseFloat(vcaseuomqty)>0){
							noofcases = getSKUDimCount(vremQty, vcaseuomqty);
							vremQty = parseFloat(vremQty) - (noofcases * vcaseuomqty);									 
							nlapiLogExecution('ERROR','vremQty case',vremQty);
						}

						if(vremQty>0 && parseFloat(veachuomqty)>0){
							noofeaches = getSKUDimCount(vremQty, veachuomqty);
							vremQty = parseFloat(vremQty) - (noofeaches * veachuomqty);
							nlapiLogExecution('ERROR','vremQty each',vremQty);

						}

				for(var s=0;s<result.length;s++)
				{
					var vtranid=result[s].getValue("tranid");
					var vline=result[s].getValue("line");
					// Case # 20145823 starts
					//if((vtranid==potranid)&&(vline==lineno))
					if(vline==lineno)
					// Case # 20145823 ends
						POorderqty=result[s].getValue("quantity");
					nlapiLogExecution('ERROR','POorderqty',POorderqty);	
				}
				index=i+1;

					BindSublistValues(form,lineno,sku,upccode,lotno,expdate,noofpallets,noofcases,noofeaches,recvdate,closedtask_qty,POorderqty,enterby,index);

				}
		}
		// closed task results start
		if(searchrecords!=null&&searchrecords!='')
		{
			nlapiLogExecution('ERROR','searchrecords.length ',searchrecords.length);
			for(var i=index;i<searchrecords.length;i++)
			{
				potranid=searchrecords[i].getValue("tranid","custrecord_ebiztask_ebiz_order_no","max");

				lineno=searchrecords[i].getValue("custrecord_ebiztask_line_no",null,"group");

				sku=searchrecords[i].getText("custrecord_ebiztask_sku",null,"group");

				var skuid=searchrecords[i].getValue("custrecord_sku",null,"group");

				upccode=searchrecords[i].getValue("upccode","custrecord_ebiztask_sku","max");

				lotno=searchrecords[i].getValue("custrecord_ebiztask_batch_no",null,"group");

				expdate=searchrecords[i].getValue('custrecord_ebiztask_expirydate',null,"max");

				recvdate=searchrecords[i].getValue('custrecord_ebiztask_act_end_date',null,"max");

				closedtask_qty=searchrecords[i].getValue('custrecord_ebiztask_act_qty',null,"sum");
				nlapiLogExecution('ERROR','Closed task QTY ',closedtask_qty);

				enterby=searchrecords[i].getText('custrecord_ebiztask_taskassgndid',null,"group");
				nlapiLogExecution('ERROR','sku ',sku);
				// pallets,cases,unitpercase
				nlapiLogExecution('ERROR','itemdimresult.length ',itemdimresult.length);
				var noofeaches,noofcases,noofpallets,veachuomqty,vcaseuomqty,vpalletuomqty,vremQty;
				noofpallets = 0;
				noofcases = 0;
				noofeaches = 0;
				vremQty = 0;
				for(var item=0;item<itemdimresult.length;item++)
				{
					var vitems=itemdimresult[item].getText('custrecord_ebizitemdims');
					nlapiLogExecution('ERROR','vitems',vitems);
					if(sku==vitems)
					{
						var vuom=itemdimresult[item].getValue('custrecord_ebizuomskudim');

						var vqty=itemdimresult[item].getValue('custrecord_ebizqty');
						nlapiLogExecution('ERROR','vqty',vqty);
						

						/*// var eBizItemDims = geteBizItemDimensions1(skuid);
				var eBizItemDims = GetItemdimensionvalues(sku);
				nlapiLogExecution('ERROR','eBizItemDims',eBizItemDims);
				if(eBizItemDims != null && eBizItemDims != '')
				{
					for(var i1= 0;i1< eBizItemDims.length;i1++)
					{
						var vUomlevel = eBizItemDims[i1].getValue('custrecord_ebizuomlevelskudim');
						if(vUomlevel=='3')
						{
							vpalletuomqty = eBizItemDims[i1].getValue('custrecord_ebizqty');
							//nlapiLogExecution('ERROR','vpalletuomqty',vpalletuomqty);
						}
						else
							if(vUomlevel=='2')
							{
								vcaseuomqty = eBizItemDims[i1].getValue('custrecord_ebizqty');
								//nlapiLogExecution('ERROR','vcaseuomqty',vcaseuomqty);
							}
							else
								if(vUomlevel=='1')
								{
									veachuomqty = eBizItemDims[i1].getValue('custrecord_ebizqty');
									//nlapiLogExecution('ERROR','veachuomqty',veachuomqty);

								}
					}
				}*/

						var vUomlevel = itemdimresult[item].getValue('custrecord_ebizuomlevelskudim');
						if(vUomlevel=='3')
						{
							vpalletuomqty = itemdimresult[item].getValue('custrecord_ebizqty'); 
							//nlapiLogExecution('ERROR','vpalletuomqty',vpalletuomqty);
						}
						else
							if(vUomlevel=='2')
							{
								vcaseuomqty = itemdimresult[item].getValue('custrecord_ebizqty'); 

								//nlapiLogExecution('ERROR','vcaseuomqty',vcaseuomqty);
							}
							else
								if(vUomlevel=='1')
								{
									veachuomqty = itemdimresult[item].getValue('custrecord_ebizqty');

									//nlapiLogExecution('ERROR','veachuomqty',veachuomqty);

								}
					}
				}
						vremQty=closedtask_qty;

						if(vremQty>0 && parseFloat(vpalletuomqty)>0){
							noofpallets = getSKUDimCount(vremQty, vpalletuomqty);
							vremQty = parseFloat(vremQty) - (noofpallets * vpalletuomqty);
							nlapiLogExecution('ERROR','vremQty pallet',vremQty);
						}

						if(vremQty>0 && parseFloat(vcaseuomqty)>0){
							noofcases = getSKUDimCount(vremQty, vcaseuomqty);
							vremQty = parseFloat(vremQty) - (noofcases * vcaseuomqty);									 
							nlapiLogExecution('ERROR','vremQty case',vremQty);
						}

						if(vremQty>0 && parseFloat(veachuomqty)>0){
							noofeaches = getSKUDimCount(vremQty, veachuomqty);
							vremQty = parseFloat(vremQty) - (noofeaches * veachuomqty);
							nlapiLogExecution('ERROR','vremQty each',vremQty);

						}


					
				 
				for(var s=0;s<result.length;s++)
				{
					var vtranid=result[s].getValue("tranid");
					var vline=result[s].getValue("line");
					// Case # 20145823 starts
					//if((vtranid==potranid)&&(vline==lineno)) 
					if(vline==lineno)
					// Case # 20145823 ends
						POorderqty=result[s].getValue("quantity");
					nlapiLogExecution('ERROR','POorderqty',POorderqty);	
				}
				index=i+1;


				BindSublistValues(form,lineno,sku,upccode,lotno,expdate,noofpallets,noofcases,noofeaches,recvdate,closedtask_qty,POorderqty,enterby,index);

			}
		}


		form.setScript('customscript_ebiz_po_receiptreport1');
		form.addButton('custpage_print','Print','Printreport()');
		response.writePage(form);

//		Search with po number ends

	}// else(post)ending


}// funcion ending



function BindSublistValues(form,lineno,sku,upccode,lotno,expdate,pqty,cqty,eachqty,recvdate,closedtask_qty,POorderqty,enterby,index)
{
	
	form.getSubList('custpage_sublist').setLineItemValue('custpage_line',index,lineno);
	form.getSubList('custpage_sublist').setLineItemValue('custpage_sku',index,sku);
	form.getSubList('custpage_sublist').setLineItemValue('custpage_mfgups',index,upccode);
	form.getSubList('custpage_sublist').setLineItemValue('custpage_lot',index,lotno);
	form.getSubList('custpage_sublist').setLineItemValue('custpage_expdate',index,expdate);
		
	form.getSubList('custpage_sublist').setLineItemValue('custpage_pallets',index,pqty);
	form.getSubList('custpage_sublist').setLineItemValue('custpage_cases',index,cqty);
	form.getSubList('custpage_sublist').setLineItemValue('custpage_unitspercase',index,eachqty);
	form.getSubList('custpage_sublist').setLineItemValue('custpage_receptdate',index,recvdate);
	form.getSubList('custpage_sublist').setLineItemValue('custpage_totalrecived',index,closedtask_qty);
	form.getSubList('custpage_sublist').setLineItemValue('custpage_poqty',index,POorderqty);
	form.getSubList('custpage_sublist').setLineItemValue('custpage_enterby',index,enterby);
}

function CreateColumnsot()
{
	var columns=new Array();
	columns[0]=new nlobjSearchColumn("custrecord_ebiz_cntrl_no",null,"group");
	columns[1]=new nlobjSearchColumn('custrecord_line_no',null,"group");
	columns[2]=new nlobjSearchColumn('custrecord_sku',null,"group");
	columns[3]=new nlobjSearchColumn('upccode','custrecord_sku',"max");// mfgupc
	columns[4]=new nlobjSearchColumn('custrecord_batch_no',null,"group");
	columns[5]=new nlobjSearchColumn('custrecord_expirydate',null,"max"); // for
																			// expdate
	columns[6]=new nlobjSearchColumn('custrecord_act_end_date',null,"max"); // for
																			// receipt
																			// date
	columns[7]=new nlobjSearchColumn('custrecord_act_qty',null,"sum"); // for
																		// total
																		// recieced
																		// count
	columns[8]=new nlobjSearchColumn('custrecord_taskassignedto',null,"group"); 
	columns[9]=new nlobjSearchColumn("tranid","custrecord_ebiz_order_no","group"); 
	
	return columns;
}


function CreateColumnsct()
{
	var columns=new Array();
	columns[0]=new nlobjSearchColumn("custrecord_ebiztask_ebiz_cntrl_no",null,"group");
	columns[1]=new nlobjSearchColumn('custrecord_ebiztask_line_no',null,"group");
	columns[2]=new nlobjSearchColumn('custrecord_ebiztask_sku',null,"group");
	columns[3]=new nlobjSearchColumn('upccode','custrecord_ebiztask_sku',"max");// mfgupc
	columns[4]=new nlobjSearchColumn('custrecord_ebiztask_batch_no',null,"group");
	columns[5]=new nlobjSearchColumn('custrecord_ebiztask_expirydate',null,"max"); // for
																					// expdate
	columns[6]=new nlobjSearchColumn('custrecord_ebiztask_act_end_date',null,"max"); // for
																						// receipt
																						// date
	columns[7]=new nlobjSearchColumn('custrecord_ebiztask_act_qty',null,"sum"); // for
																				// total
																				// recieced
																				// count
	columns[8]=new nlobjSearchColumn('custrecord_ebiztask_taskassgndid',null,"group"); 
	columns[9]=new nlobjSearchColumn("tranid","custrecord_ebiztask_ebiz_order_no","max"); 
	
	return columns;
}

// for item dimensions
function GetItemdimensionvalues(itemvalue)
{
var itemdimensionfilter=new Array();
itemdimensionfilter.push(new nlobjSearchFilter('custrecord_ebizitemdims',null,'anyof',itemvalue));

var itemcolumns=new Array();

itemcolumns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');								// ITEM/SKU
itemcolumns[1] = new nlobjSearchColumn('custrecord_ebizuomskudim');								// UOM
itemcolumns[2] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim');						// UOM
																								// LEVEL
itemcolumns[3] = new nlobjSearchColumn('custrecord_ebizqty');									// QUANTITY
itemcolumns[4] = new nlobjSearchColumn('custrecord_ebizbaseuom');								// BASE
																								// UOM
																								// FLAG
itemcolumns[5] = new nlobjSearchColumn('custrecord_ebizdims_packflag');							// PACK
																								// FLAG
itemcolumns[6] = new nlobjSearchColumn('custrecord_ebizweight');								// WEIGHT
itemcolumns[7] = new nlobjSearchColumn('custrecord_ebizcube');	

var	itemdimension_searchrecords=new nlapiSearchRecord('customrecord_ebiznet_skudims',null,itemdimensionfilter,itemcolumns);
nlapiLogExecution('ERROR','itemdimension_searchrecords_length',itemdimension_searchrecords.length);
return itemdimension_searchrecords;
// end item
}
function GetPuracahseOrderValues(pointerid)
{
	/*
	 * var trantype = nlapiLookupField('transaction', pointerid, 'recordType');
	 * //nlapiLogExecution('ERROR', 'trantype', trantype);
	 * 
	 * var transaction; if(trantype =='purchaseorder') { transaction =
	 * nlapiLoadRecord('purchaseorder', pointerid); }
	 */
	
	var polinefilters = new Array();
	if(pointerid!=null&&pointerid!='')
	polinefilters[0] = new nlobjSearchFilter('internalid', null, 'anyof', pointerid);			
	polinefilters[1] = new nlobjSearchFilter('item', null, 'noneof','@NONE@');

	var polinecolumns = new Array();
	polinecolumns[0] = new nlobjSearchColumn('line');
	polinecolumns[1] = new nlobjSearchColumn('item');
	polinecolumns[2] = new nlobjSearchColumn('quantity');
	polinecolumns[3] = new nlobjSearchColumn('custcol_ebiznet_item_status');
	polinecolumns[4] = new nlobjSearchColumn('formulanumeric');
	polinecolumns[4].setFormula('ABS({quantity})');
	polinecolumns[5] = new nlobjSearchColumn('tranid'); // for poid
	polinecolumns[6] = new nlobjSearchColumn('entity');
	polinecolumns[7] = new nlobjSearchColumn('billaddressee');
	polinecolumns[8] = new nlobjSearchColumn('trandate'); // for pdf po date
	polinecolumns[9] = new nlobjSearchColumn('custbody_poshippingitem'); // for
																			// pdf
																			// carrier
	polinecolumns[10] = new nlobjSearchColumn('custbody_nswmspoexpshipdate'); // for
																				// exp
																				// shipdate
	polinecolumns[11] = new nlobjSearchColumn('custbody_nswmspoarrdate'); // for
																			// exp
																			// arrival
																			// date
	polinecolumns[12] = new nlobjSearchColumn('trandate');
	polinecolumns[13] = new nlobjSearchColumn('billaddress');//'tranid'); //for vendor address
	// polinecolumns[13] = new nlobjSearchColumn('defaultaddress','entity');
	
	var posearchresults = nlapiSearchRecord('purchaseorder', null, polinefilters, polinecolumns);
	if(posearchresults !=null && posearchresults !='')
		{
		return posearchresults;
		}
	else
		{
		return null;
		}
}

function GetPOInternalId(POid)
{
	var purchaseorderFilers = new Array();
	purchaseorderFilers.push(new nlobjSearchFilter('mainline', null, 'is', 'T')); 
	purchaseorderFilers.push(new nlobjSearchFilter('tranid', null, 'is', POid)); 
	// salesorderFilers.push(new nlobjSearchFilter('line', null, 'is',
	// ordline));
	
	
	var customerSearchResults = nlapiSearchRecord('purchaseorder', null, purchaseorderFilers,null);	
	if(customerSearchResults==null)
	{
		// nlapiLogExecution('ERROR', 'transferorder','transferorder');
		customerSearchResults=nlapiSearchRecord('transferorder',null,purchaseorderFilers,null);
	}
	
	if(customerSearchResults != null && customerSearchResults != "")
	{
		// nlapiLogExecution('ERROR', 'customerSearchResults.length
		// ',customerSearchResults.length);
		return customerSearchResults[0].getId();
	}
	else
		return null;
}
// for item dimension count calculations
function getSKUDimCount(orderQty, dimQty){

	var str = 'order qty. = ' + orderQty + '<br>';
	str = str + 'dimensions qty. = ' + dimQty + '<br>';	
	nlapiLogExecution('DEBUG', 'into getSKUDimCount', str);

	var retSKUDimCount = 0;

	if(dimQty > 0 && (parseFloat(orderQty) >= parseFloat(dimQty)))
	{
		skuDimCount = parseFloat(orderQty)/parseFloat(dimQty);
		nlapiLogExecution('DEBUG', 'skuDimCount', skuDimCount);
		retSKUDimCount = Math.floor(skuDimCount);
		if(retSKUDimCount == 0)
			retSKUDimCount = 1;
	}
	else
		retSKUDimCount = 0; 

	nlapiLogExecution('DEBUG', 'out of getSKUDimCount', retSKUDimCount);

	return retSKUDimCount;
}

function geteBizItemDimensions1(itemid)
{
	var searchRec = new Array();
	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', itemid));
	// filter.push(new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null,
	// 'anyof', uomlevel));
	// filter.push(new nlobjSearchFilter('custrecord_ebizuomskudim', null,
	// 'anyof', uomlevel));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim') ;
	column[1] = new nlobjSearchColumn('custrecord_ebizqty') ;
	column[2] = new nlobjSearchColumn('custrecord_ebizbaseuom') ;
	column[3] = new nlobjSearchColumn('custrecord_ebiznsuom') ;
	column[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim') ;
	column[5] = new nlobjSearchColumn('custrecord_ebizcube');
	searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);

	return searchRec;

}

function Printreport(){
	var po = nlapiGetFieldValue('custpage_poid');
	nlapiLogExecution('ERROR', 'Pdfs PO',po);
	
	var PDFURL = nlapiResolveURL('SUITELET', 'customscript_ebiz_po_receiptreport_pdf', 'customdeploy_ebiz_d_po_receipt');
	nlapiLogExecution('ERROR', ' PDF URL',PDFURL);					
	PDFURL = PDFURL + '&custparam_ebiz_po_no='+ po;
	nlapiLogExecution('ERROR', 'PDFURL',PDFURL);
	window.open(PDFURL);

}