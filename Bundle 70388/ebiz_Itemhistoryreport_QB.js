
/***************************************************************************
                                                                 
   eBizNET Solutions Inc 
****************************************************************************
*
*  $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Suitelet/Attic/ebiz_Itemhistoryreport_QB.js,v $
*  $Revision: 1.1.2.2 $
*  $Date: 2013/09/11 15:23:51 $
*  $Author: rmukkera $
*  $Name: t_NSWMS_2014_1_1_174 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_Itemhistoryreport_QB.js,v $
*  Revision 1.1.2.2  2013/09/11 15:23:51  rmukkera
*  Case# 20124376
*
*
****************************************************************************/
function ItemHistoryReportQB_SL(request,response)
{
	
	if (request.getMethod() == 'GET')
	{
		var form = nlapiCreateForm('Item History Report');
		var Item = form.addField('custpage_item', 'select', 'Item','Item');	
		var todate = form.addField('custpage_todate', 'date', 'To Date');
		var fromdate = form.addField('custpage_fromdate', 'date', 'From Date');	
		


		form.addSubmitButton('Display');
		response.writePage(form);
	}
	else
	{
		var form = nlapiCreateForm('Item History Report');
		var vItem = request.getParameter('custpage_item');
		var vfromdate = request.getParameter('custpage_fromdate');
		var vtodate = request.getParameter('custpage_todate');

		nlapiLogExecution('ERROR', 'vItem', vItem);
		nlapiLogExecution('ERROR', 'vfromdate', vfromdate);
		nlapiLogExecution('ERROR', 'vtodate', vtodate);
		
		var ItemArray = new Array();
		ItemArray["custpage_item"] = vItem;
		ItemArray["custpage_fromdate"] = vfromdate;
		ItemArray["custpage_todate"] = vtodate;		

		response.sendRedirect('SUITELET', 'customscript_itemhistory_rpt_sl','customdeploy_itemhistory_rpt_sl_di', false, ItemArray);



		response.writePage(form);
	}
}



