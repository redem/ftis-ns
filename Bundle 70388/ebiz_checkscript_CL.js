/***************************************************************************
  eBizNET Solutions Inc
 ****************************************************************************
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Client/ebiz_checkscript_CL.js,v $
 *
 *     	   $Revision: 1.22.2.5.4.2.4.36.2.4 $
 *     	   $Date: 2015/11/16 07:21:38 $
 *     	   $Author: grao $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *     	   $Revision: 1.22.2.5.4.2.4.36.2.4 $
 *     	   $Date: 2015/11/16 07:21:38 $
 *     	   $Author: grao $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_checkscript_CL.js,v $
 * Revision 1.22.2.5.4.2.4.36.2.4  2015/11/16 07:21:38  grao
 * 2015.2 Issue Fixes 201415553
 *
 * Revision 1.22.2.5.4.2.4.36.2.3  2015/11/14 14:33:22  grao
 * 2015.2 Issue Fixes 201415553
 *
 * Revision 1.22.2.5.4.2.4.36.2.2  2015/11/12 15:54:56  deepshikha
 * 2015.2 issues
 * 201414484
 *
 * Revision 1.22.2.5.4.2.4.36.2.1  2015/09/21 14:02:29  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.22.2.5.4.2.4.36  2015/07/22 15:47:33  grao
 * 2015.2   issue fixes  201413586
 *
 * Revision 1.22.2.5.4.2.4.35  2015/07/20 15:32:24  skreddy
 * Case# 201413511,201413512
 * PD prod issue fix
 *
 * Revision 1.22.2.5.4.2.4.34  2015/07/10 22:35:55  skreddy
 * Case# 201413204
 * Dynacraft  Prod  issue fix
 *
 * Revision 1.22.2.5.4.2.4.33  2015/07/02 13:53:31  schepuri
 * case# 201413301
 *
 * Revision 1.22.2.5.4.2.4.32  2015/06/30 15:19:53  skreddy
 * Case# 201413281
 * Signwarehouse SB issue fix
 *
 * Revision 1.22.2.5.4.2.4.31  2015/06/26 08:07:51  schepuri
 * case# 201413258
 *
 * Revision 1.22.2.5.4.2.4.30  2015/06/24 13:38:24  schepuri
 * case# 201413198
 *
 * Revision 1.22.2.5.4.2.4.29  2015/06/22 15:43:01  grao
 * SB issue fixes  201413179
 *
 * Revision 1.22.2.5.4.2.4.28  2015/06/16 15:39:37  nneelam
 * case# 201413038
 *
 * Revision 1.22.2.5.4.2.4.27  2015/04/17 15:40:49  skreddy
 * Case# 201223037
 * Jawbone prod issue fix
 *
 * Revision 1.22.2.5.4.2.4.26  2014/10/17 14:16:42  schepuri
 * 201410703 issue fix
 *
 * Revision 1.22.2.5.4.2.4.25  2014/09/25 18:02:33  skreddy
 * case # 201410359
 * TPP SB issue fix
 *
 * Revision 1.22.2.5.4.2.4.24  2014/07/08 15:06:37  rmukkera
 * Case # 20149270
 *
 * Revision 1.22.2.5.4.2.4.23  2014/06/05 15:36:06  nneelam
 * case#  20148727
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.22.2.5.4.2.4.22  2014/05/12 15:30:00  skavuri
 * Case# 20148315, 20148254 SB Issue are fixed
 *
 * Revision 1.22.2.5.4.2.4.21  2014/03/12 06:36:45  skreddy
 * case 20127215
 * Deal med SB issue fixs
 *
 * Revision 1.22.2.5.4.2.4.20  2014/01/31 13:33:14  schepuri
 * 20126999
 * standard bundle issue
 *
 * Revision 1.22.2.5.4.2.4.19  2014/01/29 15:23:44  rmukkera
 * Case # 20126967
 *
 * Revision 1.22.2.5.4.2.4.18  2014/01/21 14:31:03  rmukkera
 * Case # 20126884
 *
 * Revision 1.22.2.5.4.2.4.17  2014/01/06 13:56:28  schepuri
 * 20126654
 *
 * Revision 1.22.2.5.4.2.4.16  2013/12/23 14:09:46  schepuri
 * 20126515,20126436
 *
 * Revision 1.22.2.5.4.2.4.15  2013/12/20 13:53:53  schepuri
 * 20126039, 20126421
 *
 * Revision 1.22.2.5.4.2.4.14  2013/12/17 15:37:25  nneelam
 * Case# 20126365
 * LP Prefix Issue Fixed
 *
 * Revision 1.22.2.5.4.2.4.13  2013/12/17 15:15:31  rmukkera
 * Case # 20126344�
 *
 * Revision 1.22.2.5.4.2.4.12  2013/12/16 15:18:36  nneelam
 * Case#  20126072
 * std issue fix
 *
 * Revision 1.22.2.5.4.2.4.11  2013/12/12 14:02:37  schepuri
 * 20126185
 *
 * Revision 1.22.2.5.4.2.4.10  2013/11/29 15:07:03  grao
 * Case# 20125982 related issue fixes in SB 2014.1
 *
 * Revision 1.22.2.5.4.2.4.9  2013/11/21 15:41:19  grao
 * Issue fixes 20125078
 *
 * Revision 1.22.2.5.4.2.4.8  2013/10/19 13:10:53  grao
 * Issue fixes 20125156
 *
 * Revision 1.22.2.5.4.2.4.7  2013/09/25 06:49:42  schepuri
 * getting these script error messages
 *
 * Revision 1.22.2.5.4.2.4.6  2013/09/11 15:23:51  rmukkera
 * Case# 20124376
 *
 * Revision 1.22.2.5.4.2.4.5  2013/05/01 15:30:40  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.22.2.5.4.2.4.4  2013/04/23 15:26:31  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.22.2.5.4.2.4.3  2013/04/22 15:38:57  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.22.2.5.4.2.4.2  2013/03/19 12:08:11  schepuri
 * CASE201112/CR201113/LOG201121
 * change url path
 *
 * Revision 1.22.2.5.4.2.4.1  2013/03/05 14:53:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Lexjet production as part of Standard bundle
 *
 * Revision 1.22.2.5.4.2  2012/12/11 02:59:35  kavitha
 * CASE201112/CR201113/LOG201121
 * Incorporated Serial # functionality
 *
 * Revision 1.22.2.5.4.1  2012/11/01 14:54:56  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.22.2.5  2012/08/21 16:18:04  gkalla
 * CASE201112/CR201113/LOG201121
 * Inventory move validation
 *
 * Revision 1.22.2.4  2012/08/16 13:50:13  spendyala
 * CASE201112/CR201113/LOG201121
 * Issues fix for the 339 bundle i.e,
 * Inventory move is still forcing user to select the Bin Location from screen instead of generating.
 *
 * Revision 1.22.2.3  2012/08/15 15:30:52  spendyala
 * CASE201112/CR201113/LOG201121
 * Issues fix for the 339 bundle i.e, Generate bin location button is not working.
 *
 * Revision 1.22.2.2  2012/06/26 13:27:06  skreddy
 * CASE201112/CR201113/LOG201121
 * Added Code to handle Two Buttons
 *
 * Revision 1.22.2.1  2012/02/16 15:00:07  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Invtmove
 *
 * Revision 1.22  2011/12/13 21:05:31  gkalla
 * CASE201112/CR201113/LOG201121
 * Add new fields in opentask
 *
 * Revision 1.21  2011/11/21 13:58:58  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Default Lot Functionality. (Item as Lot)
 *
 * Revision 1.20  2011/10/20 14:43:50  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Inventory move within same location.
 *
 * Revision 1.19  2011/10/19 21:03:46  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Inventory Move files
 *
 * Revision 1.18  2011/10/17 06:42:14  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Inventory Move to same location
 *
 * Revision 1.17  2011/10/15 22:38:19  snimmakayala
 * CASE201112/CR201113/LOG201121
 * move to same location
 *
 * Revision 1.16  2011/10/13 18:56:48  gkalla
 * CASE201112/CR201113/LOG201121
 * For Inventory move XFER
 *
 * Revision 1.15  2011/10/12 14:24:18  gkalla
 * CASE201112/CR201113/LOG201121
 * For Inventory move XFER
 *
 * Revision 1.14  2011/10/12 12:00:12  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Inventory Move  Files
 *
 * Revision 1.13  2011/10/12 10:08:58  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Inventory Move  Files
 *
 * Revision 1.12  2011/09/14 16:15:37  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.11  2011/09/14 15:15:52  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.10  2011/09/14 14:55:50  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.9  2011/09/14 14:13:46  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.8  2011/07/28 09:18:00  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.7  2011/07/27 06:36:51  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.6  2011/07/26 14:18:17  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.5  2011/07/21 06:55:48  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/
/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh as part of Standard bundle***/
var isfromPagination='F';
/*** Up to here ***/ 
function client(){
	//try {   

	var lincount = nlapiGetLineItemCount('custpage_invtmovelist');
	var userselection = false; 

	for (var p = 1; p <= lincount; p++) 
	{
		var moveFlag = nlapiGetLineItemValue('custpage_invtmovelist', 'custpage_invlocmove', p);
		if (moveFlag == 'T') 
		{
			userselection = true;	        	
		}
	}

	if(userselection == false)
	{
		alert('Please select atleast one row');
		return false;
	} 
	else
		return true;
}



function fieldChanged(type, name)
{	
	//alert('name' + name);
	if(name!='custpage_selectpage' && name!='custpage_pagesize')
	{
		isfromPagination='F';
		var movechk = nlapiGetCurrentLineItemValue('custpage_invtmovelist','custpage_invlocmove');
		var CallFlag='N';	
		var vNSFlag;
		if(movechk == 'T')
		{	
			//Case 20125156 Start,depending on Location we are getting lp range here
			var whloc = nlapiGetFieldValue('custpage_location');

			//end

			//alert(whloc);
			var oldbinloc = parseFloat(nlapiGetCurrentLineItemValue('custpage_invtmovelist','custpage_locid'));
			var oldlp = nlapiGetCurrentLineItemValue('custpage_invtmovelist','custpage_oldlp');
			var oldqty = nlapiGetCurrentLineItemValue('custpage_invtmovelist','custpage_oldqty');
			var newbinloc = parseFloat(nlapiGetCurrentLineItemValue('custpage_invtmovelist','custpage_itemnewloc'));
			var newqty = nlapiGetCurrentLineItemValue('custpage_invtmovelist','custpage_itemnewqty');
			var newbinloctext = nlapiGetCurrentLineItemText('custpage_invtmovelist','custpage_itemnewloc');
			var newlp = nlapiGetCurrentLineItemValue('custpage_invtmovelist','custpage_itemnewlp');
			var oldItemStatus = nlapiGetCurrentLineItemValue('custpage_invtmovelist','custpage_olditemstatus');
			var newItemStatus = nlapiGetCurrentLineItemValue('custpage_invtmovelist','custpage_itemnewstatus');
			var AdjustmentType = nlapiGetCurrentLineItemValue('custpage_invtmovelist','custpage_invadjtype');
			var ItemId = nlapiGetCurrentLineItemValue('custpage_invtmovelist','custpage_itemid');
			var PoLoc = nlapiGetCurrentLineItemValue('custpage_invtmovelist','custpage_siteloc');
			var getLOTId = nlapiGetCurrentLineItemValue('custpage_invtmovelist', 'custpage_lot');
			// case no 20126515? 

			/*
			//Case # 20126344�Start
			var availQty=nlapiGetCurrentLineItemValue('custpage_invtmovelist','custpage_availqty');
			if(availQty==null || availQty=='' || availQty=='null' || availQty=='undefined')
				{
				availQty=0;
				}
			//Case # 20126344� End
			 */
			var oldQOH=0;
			var oldAllocAty=0;
			var arrDims = getSKUCubeAndWeight(ItemId); 
			var newLocRemCube = GeteLocCube(newbinloc);
			//alert("arrDims:" + arrDims);

			if(isNaN(newbinloc))
			{
				newbinloc = '';
			}

			if(nlapiGetCurrentLineItemValue('custpage_invtmovelist','custpage_oldqty') != null && nlapiGetCurrentLineItemValue('custpage_invtmovelist','custpage_oldqty') != "")
			{
				oldQOH = parseFloat(nlapiGetCurrentLineItemValue('custpage_invtmovelist','custpage_oldqty'));
				if(oldQOH<0)
					oldQOH=0;
			}
			if(nlapiGetCurrentLineItemValue('custpage_invtmovelist','custpage_allocqtyhid') != null && nlapiGetCurrentLineItemValue('custpage_invtmovelist','custpage_allocqtyhid') != "")
			{
				oldAllocAty = parseFloat(nlapiGetCurrentLineItemValue('custpage_invtmovelist','custpage_allocqtyhid'));
				if(oldQOH<0)
					oldQOH=0;

			}
			var vAvailQty=0;
			vAvailQty= parseFloat(oldQOH) - parseFloat(oldAllocAty);

			/*alert('oldqty:'+ parseFloat(oldqty));
		alert('newqty:'+parseFloat(newqty));
		alert('Old LP:'+ oldlp);
		alert('New LP:'+ newlp);*/

//			if(nlapiGetCurrentLineItemValue('custpage_invtmovelist','custpage_itemnewloc') == "")
//			{
//			alert('Please select New Bin Location');
//			nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_invlocmove','F');
//			//nlapiGetLineItemField('custpage_invtmovelist','custpage_itemnewloc',nlapiGetCurrentLineItemIndex('custpage_invtmovelist')).focus();
//			return false;
//			}
			/*if(newbinloc == null || newbinloc =='')
			{
				alert('Please select New Binlocation');
				nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_invlocmove','F');
				//nlapiGetLineItemField('custpage_invtmovelist','custpage_itemnewqty',nlapiGetCurrentLineItemIndex('custpage_invtmovelist')).focus();
				return false;
			}*/

			if(newqty == "")
			{
				alert('Please enter New Quantity');
				nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_invlocmove','F');
				//nlapiGetLineItemField('custpage_invtmovelist','custpage_itemnewqty',nlapiGetCurrentLineItemIndex('custpage_invtmovelist')).focus();
				return false;
			}
			else if(isNaN(newqty) == true)
			{
				alert('Please enter New Quantity as a number');
				nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_invlocmove','F');
				return false;
			}
			else if(newqty < 0)
			{
				alert('New Quantity cannot be -ve');
				nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_invlocmove','F');
				return false;
			}

			else if(parseFloat(newqty) == 0)
			{
				// case no 20126039
				alert('New Quantity cannot be Zero');
				nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_invlocmove','F');
				return false;
			}
			else if(parseFloat(vAvailQty) < parseFloat(newqty))
			{ 
				alert('New Quantity cannot be greater than available quantity');
				nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_invlocmove','F');
				return false;
			}
			var vUOMLevel=9; // this is internal id for pallet in UOM LOV value
			var itemQty =0;
			var filters = new Array();          
			filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof',ItemId);
			filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			//filters[1] = new nlobjSearchFilter('custrecord_ebizpackcodeskudim', null, 'is',pCode);

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_ebizqty');
			//Added by suman on 19-01-12
			columns[1] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim').setSort(true);//it fetches the records with highest uomlevel first. 

			var searchskuresults = nlapiSearchRecord('customrecord_ebiznet_skudims',null, filters, columns);

			if(searchskuresults!=null)
			{ 
				itemQty = searchskuresults[0].getValue('custrecord_ebizqty');  
			}    	
			//alert("itemQty :"+itemQty);
			//alert("newqty :"+newqty);
			if(parseFloat(newqty)>parseFloat(itemQty))
			{
				alert("New Quantity is greater than Pallet Qty");
				nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_invlocmove','F');
				return false;    		
			}    	

			/*		else if(newlp == "")
		{
//			alert('Please enter New LP');
//			nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_invlocmove','F');
//			return false;
		}*/			
			else if(oldbinloc == newbinloc)
			{	
				var result=confirm('Are you sure you want to move the inventory to the same bin location?');
				if(result==false)
				{
					nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_invlocmove','F');
					return false;
				}

			}		

			else if(parseFloat(oldqty) > parseFloat(newqty))
			{ 
				if(oldlp == newlp)
				{

					alert('Partial quantity cannot be moved to different location with the same LP');
					nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_invlocmove','F');
					return false;
				}
			}
			//201413204
			var itemCube = 0;
			if (arrDims[0] != "" && (!isNaN(arrDims[0]))) 
			{
				
				
				var uomqty = ((parseFloat(newqty))/(parseFloat(arrDims[1])));			
				//alert("uomqty:" + uomqty);
				itemCube = (parseFloat(uomqty) * parseFloat(arrDims[0]));
				//alert("itemCube:" + itemCube);
				
			} 
			else 
			{
				itemCube = 0;
			} 
			//alert("newLocRemCube:" + newLocRemCube);
			//alert("itemCube:" + itemCube);
			if(newLocRemCube > 0)
			{
				if (parseFloat(newLocRemCube) < parseFloat(itemCube)) 
				{
					alert('Bin Location Cube is less than the item cube');
					nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_invlocmove','F');
					return false;
				}
			
			}

//			if(newItemStatus==null ||newItemStatus=='')
//			{
//			CallFlag='N';
//			}
//			else if(oldItemStatus==newItemStatus)
//			{
//			CallFlag='N';
//			}

//			else
//			{	
//			vNSFlag=GetNSCallFlag(oldItemStatus,newItemStatus);
//			if(vNSFlag!= null && vNSFlag!= "" && vNSFlag.length>0)
//			{
//			CallFlag=vNSFlag;

//			}
//			}
			if(oldqty!=newqty || oldlp!=newlp)
			{
				if (newlp.length > 0) 
				{
					var type = "PALT";
					var vargetlpno = newlp;
					var vResult = "";
					var LPType=''
						var filters = new Array();
					filters[0] = new nlobjSearchFilter('custrecord_ebiznet_lprange_lptype', null, 'is', 1);
					filters[1] = new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'is', 2);
					//Case 20125156 Start,depending on Location we are getting lp range here
					filters[2]= new nlobjSearchFilter('custrecord_ebiznet_lprange_site', null, 'anyof', whloc);
//					end
					//case start 20125078 
					filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
					//end
					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix');
					columns[1] = new nlobjSearchColumn('custrecord_ebiznet_lprange_begin');
					columns[2] = new nlobjSearchColumn('custrecord_ebiznet_lprange_end');
					columns[3] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpgentype');
					columns[4] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lptype');

					var searchresults = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, filters, columns);

					if (searchresults) 
					{
						for (var i = 0; i < Math.min(50, searchresults.length); i++) 
						{
							try 
							{
								var getLPrefix = (searchresults[i].getValue('custrecord_ebiznet_lprange_lpprefix')).toUpperCase();					
								var varBeginLPRange = searchresults[i].getValue('custrecord_ebiznet_lprange_begin');
								var varEndRange = searchresults[i].getValue('custrecord_ebiznet_lprange_end');
								var getLPGenerationTypeValue = searchresults[i].getValue('custrecord_ebiznet_lprange_lpgentype');
								var getLPTypeValue = searchresults[i].getValue('custrecord_ebiznet_lprange_lptype');

								var LPprefixlen = getLPrefix.length;
								var vLPLen = vargetlpno.substring(0, LPprefixlen).toUpperCase();


								if (vLPLen == getLPrefix) 
								{
									LPType = getLPGenerationTypeValue;
									var varnum = vargetlpno.substring(LPprefixlen, vargetlpno.length);

									/*alert("varnum " + varnum);
									alert("varnum.length " + varnum.length);
									alert("varEndRange.length " + varEndRange.length);*/

									//Case # 20126365 Checking isNaN and varnum null or empty value for varnum
									if (isNaN(varnum)) 
									{

										vResult = "N";
										break;
									}
									if (varnum == '' || varnum == null) 
									{

										vResult = "N";
										break;
									}

									//End

									if (varnum.length > varEndRange.length) 
									{

										vResult = "N";
										break;
									}
									if ((parseFloat(varnum,10) < parseFloat(varBeginLPRange)) || (parseFloat(varnum,10) > parseFloat(varEndRange))) 
									{

										vResult = "N";
										break;
									}
									else 
									{
										vResult = "Y";
										break;
									}
								}
								else 
								{
									vResult = "N";
								}

							} 
							catch (err) 
							{

							}
						} //end of for loop
					}

					//alert("vResult :" +vResult);
					if (vResult == "Y") 
					{

						var filters1 = new Array();				
						//filters1[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lpno);
						filters1[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', vargetlpno);

						var columns1 = new Array();
						columns1[0] = new nlobjSearchColumn('name');

						var searchresults1 = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters1,columns1);		
						if(searchresults1!=null &&  searchresults1.length>0)
						{
							/*var res = CheckLPinInvt(vargetlpno,newbinloc,getLOTId,null,null);
							nlapiLogExecution('ERROR', 'res', res);
							if(res ==null || res =='')
							{
								return true;
							}*/
							/*else
							{*/
								alert('LP Already Exists');
								nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_itemnewlp','');
								nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_invlocmove','F');
								return false;
							//}
							/*					
							if(oldlp != newlp)
							{
								alert('LP Already Exists');
								nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_itemnewlp','');
								nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_invlocmove','F');
								return false;
							}
							else
							{
								return true;
							}*/
						}
						else
						{
							if(LPType =='1')
							{
								alert('Please enter User Defined LP ');
								nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_itemnewlp','');
								nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_invlocmove','F');
								return false;
							}
							/*else
							{
								return true;
							}*/

						}

					}
					else 
					{
						alert('Invalid LP Range!');
						nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_itemnewlp','');
						nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_invlocmove','F');					
						return false;
					}
				}
				/*			else 
			{
//				alert('Invalid LP No');
//				nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_itemnewlp','');
//				nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_invlocmove','F');
//				return false;				 
			}*/
			}		
			//alert(oldbinloc);
			//alert(newbinloc);

			if((oldbinloc != newbinloc) &&  newbinloc != null && newbinloc != '' )
			{	
				//alert('newbinloc' + newbinloc);
				var NewBinLocationFilters = new Array();
				NewBinLocationFilters[0] = new nlobjSearchFilter('internalid', null, 'is', newbinloc);
				NewBinLocationFilters[1] = new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'anyof', ['6','7']);
				NewBinLocationFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
				var columnsBinLoc = new Array();
				columnsBinLoc[0] = new nlobjSearchColumn('custrecord_inboundlocgroupid');

				var NewLocationSearchResults = nlapiSearchRecord('customrecord_ebiznet_location', null, NewBinLocationFilters, columnsBinLoc);
				if (NewLocationSearchResults != null && NewLocationSearchResults.length > 0) 
				{
					//alert('LocId=' + NewLocationSearchResults[0].getId());
					var getNewBinLocGrpId = NewLocationSearchResults[0].getValue('custrecord_inboundlocgroupid');

					nlapiLogExecution('ERROR', 'getNewBinLocGrpId', getNewBinLocGrpId );
					//alert('locGrpId=' + getNewBinLocGrpId);
					//alert('locGrpId=' + NewLocationSearchResults[0].getText('custrecord_inboundlocgroupid'));
//					case# 201413198

					var Location = NewLocationSearchResults[0].getId();
					//alert(newbinloc + ' IS THE PICKFwrwaqrqwrACE FOR ANOTHER ITEM. MOVE IS NOT ALLOWED.');
					var PickFaceLocation = getPickFaceLocation(Location,ItemId);
					if(PickFaceLocation != null && PickFaceLocation !='')
					{
						nlapiLogExecution('ERROR', 'PickFaceLocation', PickFaceLocation.length);
						var moveallowed='N';
						for(var z = 0; z < PickFaceLocation.length; z++)
						{
							var pickfaceitem=PickFaceLocation[z].getValue('custrecord_pickfacesku');
							if(pickfaceitem == ItemId)
							{
								moveallowed='Y';
								if(PickFaceLocation[z].getValue('custrecord_pickface_ebiz_lpno') != null && PickFaceLocation[z].getValue('custrecord_pickface_ebiz_lpno') != '')
								{	
									ItemNewLP=PickFaceLocation[z].getText('custrecord_pickface_ebiz_lpno');

								}
							}
						}

						if(moveallowed=='N')
						{
							alert(newbinloctext + ' IS THE PICKFACE FOR ANOTHER ITEM. MOVE IS NOT ALLOWED.');
							//alert('INVALID PICKFACE LOCATION,THIS PICKFACE FOR ANOTHER ITEM')
							return false;
						}

					}
					else
					{
						var vValidNewBin= vValidateNewBin(newbinloc,getNewBinLocGrpId,ItemId, newItemStatus, PoLoc);
						if(vValidNewBin==false)
						{
							alert('Invalid Bin Location!');
							nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_invlocmove','F');
							return false;
						}
					}
				}

				if(result==false)
				{
					alert('Invalid Bin Location!');
					nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_invlocmove','F');
					return false;
				}

			}

			var itmid = nlapiGetCurrentLineItemValue('custpage_invtmovelist', 'custpage_itemid');
			var itemname = nlapiGetCurrentLineItemValue('custpage_invtmovelist', 'custpage_itemnamenew');
			var LP = ""; 
			var ItemType="";
			var serialInflg="F";

			if(itmid!=null && itmid!='')
			{
				var fields = ['recordType', 'custitem_ebizserialin'];
				var columns = nlapiLookupField('item', itmid, fields);
				ItemType = columns.recordType;	


				serialInflg = columns.custitem_ebizserialin;		
			}
			if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem" || serialInflg == "T") {		
				
				
				if(parseInt(newqty) != newqty)
				{
					alert("For Serial item ,Decimal quantities not allowed");
					nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_invlocmove','F');
					return false;
				}
				/*if(newlp.length > 0)
				{
					LP = newlp;
				}
				else
				{
					LP = oldlp;

				}*/

				LP = oldlp;

				var seriallp=oldlp;
				if(newlp.length > 0)
				{
					seriallp=newlp;
				}


				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', itmid);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is',oldlp);
				filters[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof',[3,19]);
				filters[3] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is','D');

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				columns[1] = new nlobjSearchColumn('custrecord_serialparentid');
				var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);

				var serialResults=0;

				if(searchResults !=null && searchResults.length > 0)
				{
					serialResults=searchResults.length;

					if(newlp.length > 0)
					{
						for(var k1=0;k1<serialResults;k1++)
						{
							var LoadSerialNumbers = nlapiLoadRecord('customrecord_ebiznetserialentry',searchResults[k1].getId());
							//var id = nlapiDeleteRecord('customrecord_ebiznetserialentry', InternalID);
							LoadSerialNumbers.setFieldValue('custrecord_serialparentid', newlp);

							var recid = nlapiSubmitRecord(LoadSerialNumbers, false, true);
						}
					}

				}
				var vfilters = new Array();
				
				
				
				if(newlp== ''  || newlp ==null)
					newlp = oldlp;
				
				//alert("newlp " + newlp);
				
				vfilters[0] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is','D');
				if(newlp!=null && newlp!='')
					vfilters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', newlp);
				
				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				columns[1] = new nlobjSearchColumn('custrecord_serialparentid');
				
				var searchResults1 = nlapiSearchRecord('customrecord_ebiznetserialentry',null, vfilters, columns);
				
				//alert("searchResults1 " + searchResults1);

				//Case # 20126344? Start,20126654,20126999
	            if(parseFloat(newqty) != 0 && parseFloat(newqty)<=parseFloat(vAvailQty) && parseFloat(serialResults)!=parseFloat(newqty) && (searchResults1 == null || searchResults1 == '' || searchResults1 == 'null'))
				{



					//Case# 20149270 starts
					/*alert('name'+name)
					if (name == 'custpage_itemnewqty')
						{*/
					//Case# 20149270 ends
					//Case # //Case # 20126344� Start End
					alert("Please select required Serial numbers to move");

					//Case # 20126072,Uncheck the selected row from sublist.
					// case no 20126436
					//case# 201413281
					nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_invlocmove','F');

					//End

					var linkURL = nlapiResolveURL('SUITELET', 'customscript_serialnumber_adjustment', 'customdeploy_serialnumber_adjustment_di');
					/*var ctx = nlapiGetContext();
					if (ctx.getEnvironment() == 'PRODUCTION') {
						linkURL = 'https://system.netsuite.com' + linkURL;			
					}
					else 
						if (ctx.getEnvironment() == 'SANDBOX') {
							linkURL = 'https://system.sandbox.netsuite.com' + linkURL;				
						}*/
					//linkURL = linkURL + '&custparam_locationid=' + nlapiGetLineItemValue('custpage_invtmovelist','custpage_polocationid',p);
					//linkURL = linkURL + '&custparam_companyid=' + nlapiGetLineItemValue('custpage_invtmovelist','custpage_pocompanyid',p);
					linkURL = linkURL + '&custparam_serialskuid=' + itmid;
					linkURL = linkURL + '&custparam_serialsku=' + itemname;
					linkURL = linkURL + '&custparam_serialskulp=' + LP;
					linkURL = linkURL + '&custparam_serialskuqty=' + newqty;	
					//linkURL = linkURL + '&custparam_lot=' + BatchNo;
					//linkURL = linkURL + '&custparam_name=' + BatchNo;
					//linkURL = linkURL + '&custparam_itemstatus=' + nlapiGetLineItemValue('custpage_invtmovelist','custpage_poitemstatus',p);
					//linkURL = linkURL + '&custparam_packcode=' + nlapiGetLineItemValue('custpage_invtmovelist','custpage_popackcode',p);
					window.open(linkURL);	
					//}
				}

			}		

//			if(CallFlag=='Y' && AdjustmentType=="")
//			{
//			alert('Please select Adjustment Type');
//			//nlapiGetCurrentLineItemValue('custpage_invtmovelist','custpage_invadjtype','F');
//			nlapiSetCurrentLineItemValue('custpage_invtmovelist','custpage_invlocmove','F');	
//			return false;
//			}
		}// end of if(movechk == 'T')

	}
	if(name=='custpage_selectpage'||name=='custpage_pagesize')
	{
		isfromPagination='T';
		//nlapiSetFieldValue('custpage_hiddenfieldselectpage','T');
		NLDoMainFormButtonAction("submitter",true);	
	}
	//Case # 20126884 start
	if(name=='custpage_location' ||name=='custpage_itemstatus' )
	{
		//isfromPagination='T';
		nlapiSetFieldValue('custpage_save','T');
		NLDoMainFormButtonAction("submitter",true);	
	}
	//Case # 20126884 End
//	else
//	{
//	return true;
//	}


	return true;
	//}// end of if(name == 'custpage_invlocmove')
}
function GetNSCallFlag(Status,NewStatus)
{
	var result=new Array();
	var retFlag='N';
	var OldMapLocation;
	var NewMapLocation;
	if(Status!=null && Status!="")
	{
		var OldIStatus = nlapiLoadRecord('customrecord_ebiznet_sku_status', Status);
		if(OldIStatus!= null && OldIStatus != "")
		{
			OldMapLocation=OldIStatus.getFieldValue('custrecord_item_status_location_map');
			OldIStatus=null;
		} 
	}
	if(NewStatus!=null && NewStatus!="")
	{
		var NewIStatus = nlapiLoadRecord('customrecord_ebiznet_sku_status', NewStatus);
		if(NewIStatus!= null && NewIStatus != "")
		{
			NewMapLocation=NewIStatus.getFieldValue('custrecord_item_status_location_map');
		} 
	}
	if(OldMapLocation != null && OldMapLocation !="" && NewMapLocation != null && NewMapLocation != "")
	{
		if(OldMapLocation==NewMapLocation)
		{
			retFlag='N';			
		}
		else
		{
			retFlag='Y';
		}

	}
	return retFlag;
}

function getItemCube(skuNo)
{	
	var timestamp1 = new Date();
	var cube = 0;
	var qty = 0;

	var dimArray = new Array();	

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', skuNo);
	filters[1] = new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
	columns[1] = new nlobjSearchColumn('custrecord_ebizqty');   

	var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns); 

	if(skuDimsSearchResults != null)
	{
		for (var i = 0; i < skuDimsSearchResults.length; i++) 
		{
			var skuDim = skuDimsSearchResults[i];
			cube = skuDim.getValue('custrecord_ebizcube');
			qty = skuDim.getValue('custrecord_ebizqty');
		}
	}    
	dimArray["BaseUOMItemCube"] = cube;
	dimArray["BaseUOMQty"] = qty;  

	return dimArray;
}

function GeteLocCube(LocID){
	var remainingCube = 0;

	try {
		if(LocID !=null && LocID !='' && LocID !='null')
		{
		var fields = ['custrecord_remainingcube'];
		var columns= nlapiLookupField('customrecord_ebiznet_location',LocID,fields);
		remainingCube = columns.custrecord_remainingcube;
		}
	}
	catch(exception) {
		nlapiLogExecution('DEBUG', 'GeteLocCube:Lookup for remainingCube failed', exception);
	}
	nlapiLogExecution('DEBUG', 'GeteLocCube:Remaining Cube for Location', remainingCube);
	return remainingCube;
}
/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh as part of Standard bundle***/
function pageinit()
{
	isfromPagination='F';
}
/*** Up to here ***/ 
/*
function UpdateLocCube1(locnId, remainingCube)
{
	var fields = new Array();
	var values = new Array();
	fields[0] = 'custrecord_remainingcube';
	values[0] = remainingCube;

	nlapiLogExecution('ERROR','Location Internal Id', locnId);    
	nlapiSubmitField('customrecord_ebiznet_location', locnId, fields, values);
	return 1;
}
 */


function UpdateLocCube(locnId, remainingCube){
	var fields = new Array();
	var values = new Array();
	fields[0] = 'custrecord_remainingcube';
	values[0] = parseFloat(remainingCube).toFixed(5);

	nlapiLogExecution('ERROR','Location Internal Id', locnId);

	var t1 = new Date();
	nlapiSubmitField('customrecord_ebiznet_location', locnId, fields, values);
	var t2 = new Date();
	nlapiLogExecution('ERROR', 'nlapiSubmitField:UpdateLocCube:location', getElapsedTimeDuration(t1, t2));
}

function Display()
{
	nlapiSetFieldValue('custpage_tempflag','GenrateNewLoc');
	NLDoMainFormButtonAction("submitter",true);
}


function OnSave()
{
	/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh as part of Standard bundle***/

//	if(nlapiGetFieldValue('custpage_hiddenfieldselectpage')!='T')
	var isfromlocationChange=nlapiGetFieldValue('custpage_save');
	var date = nlapiGetFieldValue('custpage_date');
	var period =nlapiGetFieldText('custpage_accountingperiod');


	var lincount = nlapiGetLineItemCount('custpage_invtmovelist');
	var userselection = false; 

	for (var p = 1; p <= lincount; p++) 
	{
		var moveFlag = nlapiGetLineItemValue('custpage_invtmovelist', 'custpage_invlocmove', p);
		if (moveFlag == 'T') 
		{
			userselection = true;	        	
		}
	}

	if(date!=null && date!='' && period!=null && period!=''){
		var splitdate=date.split("/");

		var datemonth=splitdate[0];
		var dateyear=splitdate[2];	

		var months={Jan:1,Feb:2,Mar:3,Apr:4,May:5,Jun:6,Jul:7,Aug:8,Sep:9,Oct:10,Nov:11,Dec:12};

		var splitperiod=period.split(" ");

		var periodmonthsplit=splitperiod[0];
		var periodyear=splitperiod[1];



		var periodmonth=months[periodmonthsplit];


		if(datemonth!=periodmonth){
			var res=confirm("Transaction Date is not in the range of Period seleced. Do You Want to proceed?");
			if(res!=true){
				return false;
			}
		}		

	}
	if(isfromPagination=='F' && isfromlocationChange=='F')
	{
		/*** Up to here ***/ 
		try 
		{
			if(userselection != false)
				{
				return vcheckAtleast('custpage_invtmovelist', 'custpage_invlocmove','custpage_itemnewloc');
				}
			else
				{
				alert('Please select atleast one line');
				return false;
				}
				
		}
		catch(exp)
		{
			alert('Expception'+exp);
		}
	}
	return true;
}


function vcheckAtleast(subListID, checkBoxName,newloc){
	var flag = true;
	var flagbin=true;
	var lineNum = nlapiGetLineItemCount(subListID);
	var onGenerateClick=nlapiGetFieldValue('custpage_tempflag');
	for (var i = 1; i <= lineNum; i++) {
		if (nlapiGetLineItemValue(subListID, checkBoxName, i) == 'T') {
			//alert('True');
			var invtinternalid = nlapiGetLineItemValue('custpage_invtmovelist','custpage_recid',i);
			var vMoveQty = nlapiGetLineItemValue('custpage_invtmovelist','custpage_itemnewqty',i);
			var vMoveLP = nlapiGetLineItemValue('custpage_invtmovelist','custpage_lp',i);

			var filtersinvt = new Array();
			filtersinvt.push(new nlobjSearchFilter('internalid', null, 'is', invtinternalid));	

			var columnsinvt = new Array();
			columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');			
			columnsinvt[1] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');	
			columnsinvt[2] = new nlobjSearchColumn('custrecord_ebiz_avl_qty');	

			var searchresultsinvt = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);

			if(searchresultsinvt==null || searchresultsinvt=='')
			{
				alert('Inventory does not exist for this Lp#');
				nlapiSetLineItemValue('custpage_invtmovelist','custpage_invlocmove',i,'F');
				return false;

			}	
			else
			{
				var vAvailableQty = searchresultsinvt[0].getValue('custrecord_ebiz_avl_qty');
				if(parseFloat(vMoveQty) > parseFloat(vAvailableQty))
				{
					alert('No Sufficient Inventory is available for Lp#'+vMoveLP);
					nlapiSetLineItemValue('custpage_invtmovelist','custpage_invlocmove',i,'F');
					return false;
				}

			}
			var newbinloc=nlapiGetLineItemValue(subListID, newloc, i);
			if (newbinloc!= null&&newbinloc!="")
			{
				flagbin = false;
			}
			flag=false;
		}
	}
	/*if (flag == true && lineNum > 1) {
		alert('Please select atleast one line');
		return false;
	}*/
	//else if (flagbin == true&&onGenerateClick!='GenrateNewLoc' && lineNum >= 1){// Case# 20148315,8254
	if (flagbin == true&&onGenerateClick!='GenrateNewLoc' && lineNum >= 1){// Case# 20148315,8254
		alert('Please select NewBinlocation for the selected line(s)');
		return false;
	}
	else
		return true;
}
function vValidateNewBin(getNewBinLocId,getNewBinLocGrpId,ItemId, itemstatus, PoLoc)
{


	//alert('hi223rr');
	var vValidLoc=false;
	// case no 20126185
	var vZoneIdSearch='';
	if(getNewBinLocGrpId != null && getNewBinLocGrpId != '')
		vZoneIdSearch=vGetZoneIdByLocGroup(getNewBinLocGrpId,PoLoc);
	var vMapZoneId = null;
	if(vZoneIdSearch != null && vZoneIdSearch != '')
	{
		vMapZoneId = new Array();
		for(var s=0;s<vZoneIdSearch.length;s++ )
		{
			vMapZoneId.push(vZoneIdSearch[s].getValue('custrecordcustrecord_putzoneid'));	
		}	
	}//201413301
//	zone is not considered here for putrule becuase while auto generating loc,in generate putrule function we are considering either zone or locgrp so here also we removing zone madatory for putrule
	//alert('ItemId,itemstatus,PoLoc,vMapZoneId,getNewBinLocGrpId' + ItemId + ',' + itemstatus + ',' + PoLoc + ',' + vMapZoneId + ',' + getNewBinLocGrpId);
	var vPutRules=vGetPutawayRules(ItemId,itemstatus,PoLoc,vMapZoneId,getNewBinLocGrpId);	
	if(vPutRules != null && vPutRules != '')
	{
		vValidLoc=true;
		//alert('vValidLoc' +vValidLoc);
	}

	return vValidLoc;
}
function vGetPutawayRules(Item,ItemStatus,poLocn,NewPutZoneId,NewPutLocGrpId)
{
	var itemresults;

	var fields = ['custitem_item_family','custitem_item_group','custitem_ebizdefskustatus',
	              'custitem_item_info_1','custitem_item_info_2','custitem_item_info_3','custitem_ebizabcvelitem'];

	var columns= nlapiLookupField('item',Item,fields);
	var ItemFamily = columns.custitem_item_family;
	var ItemGroup = columns.custitem_item_group;
	var ItemInfo1 = columns.custitem_item_info_1;
	var ItemInfo2 = columns.custitem_item_info_2;
	var ItemInfo3 = columns.custitem_item_info_3;
	var putVelocity=columns.custitem_ebizabcvelitem;

	nlapiLogExecution('ERROR', 'Item', Item);
	nlapiLogExecution('ERROR', 'ItemFamily', ItemFamily);
	nlapiLogExecution('ERROR', 'ItemGroup', ItemGroup);
	nlapiLogExecution('ERROR', 'ItemStatus', ItemStatus);
	nlapiLogExecution('ERROR', 'ItemInfo1', ItemInfo1);
	nlapiLogExecution('ERROR', 'ItemInfo2', ItemInfo2);
	nlapiLogExecution('ERROR', 'ItemInfo3', ItemInfo3);
	nlapiLogExecution('ERROR', 'putVelocity', putVelocity);
	nlapiLogExecution('ERROR', 'poLocn', poLocn);


	// if (ItemFamily != null || ItemFamily != "") {
	var columns = new Array();
	var i=0;
	var filters = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_sequencenumberpickrule');
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationgrouppickrule');
	columns[2] = new nlobjSearchColumn('custrecord_putawayzonepickrule');
	columns[3] = new nlobjSearchColumn('custrecord_putawaymethod');
	columns[4] = new nlobjSearchColumn('custrecord_ruleidpickrule');
	columns[5] = new nlobjSearchColumn('custrecord_manual_location_generation');
	var ItemFamArr = new Array();
	ItemFamArr[0] = ItemFamily;
	ItemFamArr[1] = '@NONE@';

	if(ItemFamily!=null && ItemFamily != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skufamilypickrule', null, 'anyof', ['@NONE@', ItemFamily]));
	}

	if(ItemGroup!=null && ItemGroup != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skugrouppickrule', null, 'anyof', ['@NONE@', ItemGroup]));
	}

	if(ItemStatus!=null && ItemStatus != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skustatuspickrule', null, 'anyof', ['@NONE@', ItemStatus]));
	}

	if(ItemInfo1!=null && ItemInfo1 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo1pickrule', null, 'anyof',['@NONE@', ItemInfo1]));
	}

	if(ItemInfo2!=null && ItemInfo2 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo2pickrule', null, 'anyof',['@NONE@', ItemInfo2]));
	}

	if(ItemInfo3!=null && ItemInfo3 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skuinfo3pickrule', null, 'anyof',['@NONE@', ItemInfo3]));
	}

	if(Item != null && Item != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_skupickrule', null, 'anyof', ['@NONE@', Item]));		
	}

	if(putVelocity != null && putVelocity != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_abcvelpickrule', null, 'anyof', ['@NONE@', putVelocity]));
	}

	if(poLocn != null && poLocn != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizsitepickput', null, 'anyof', [poLocn]));
	}

	if(NewPutZoneId != null && NewPutZoneId != "" && NewPutZoneId != 'null')
	{
		NewPutZoneId.push('@NONE@');
		filters.push(new nlobjSearchFilter('custrecord_putawayzonepickrule', null, 'anyof', NewPutZoneId));
	}
	if(NewPutLocGrpId != null && NewPutLocGrpId != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_locationgrouppickrule', null, 'anyof', ['@NONE@', NewPutLocGrpId]));
	}
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var t1 = new Date();
	var rulesearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters, columns);
	return rulesearchresults;
}
function vGetZoneIdByLocGroup(getNewBinLocGrpId,PoLoc)
{
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_zone_seq');
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecordcustrecord_putzoneid');

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_locgroup_no', null, 'anyof', getNewBinLocGrpId));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_ebizlocation_zones', null, 'anyof', ['@NONE@', PoLoc]));	
	var searchresults = nlapiSearchRecord('customrecord_zone_locgroup', null, filters, columns);
	return searchresults;
}


function CheckLPinInvt(vlp,vnewBinlocationId,getLOTId,Itype,batchlot)
{
	try
	{

		var searchresults1='';

		var filters1 = new Array();				
		//filters1[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lpno);
		filters1[0] = new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', vlp);
		//case# 20148321 starts (system is allowing to create record with same lp of two different lots)
		/*filters1[1] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'noneof', vnewBinlocationId);
		if(Itype == "lotnumberedinventoryitem" || Itype == "lotnumberedassemblyitem" || batchlot=='T')
		{
			nlapiLogExecution('ERROR', 'getLOTId', getLOTId);
			filters1[2] = new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', getLOTId);
		}*/
		var columns1=new Array();
		columns1[0]=new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
		columns1[1]=new nlobjSearchColumn('custrecord_ebiz_inv_lot');

		searchresults1 = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters1,columns1);		
		if(searchresults1!=null && searchresults1!=null && searchresults1.length>0)
		{
			var intLotid=searchresults1[0].getValue('custrecord_ebiz_inv_lot');
			var intBinlocid=searchresults1[0].getValue('custrecord_ebiz_inv_binloc');
			//nlapiLogExecution('ERROR', 'intLotid', intLotid);
			//nlapiLogExecution('ERROR', 'intBinlocid', intBinlocid);
			if(intBinlocid!=vnewBinlocationId)
			{
				return searchresults1;
			}
			else if(intBinlocid==vnewBinlocationId)
			{
				if((getLOTId !=null && getLOTId !='' && getLOTId !='null' )&& (intLotid !=null && intLotid !='' && intLotid !='null' ))
				{
					//nlapiLogExecution('ERROR', 'getLOTId', getLOTId);
					if(intLotid!=getLOTId)
					{
						return searchresults1;
					}
					else
					{
						return '';
					}
				}
				else
				{
					return '';
				}
			}
			//nlapiLogExecution('ERROR', 'LP exists in Invt', searchresults1);
			//return searchresults1;
			//case# 20148321 ends
		}
		else
		{
			return searchresults1;
		}

	}
	catch(e)
	{
		nlapiLogExecution('ERROR', 'exception in check LP  ', e);
	}
}

function getPickFaceLocation(location,item)
{
	nlapiLogExecution('ERROR', 'Into getPickFaceLocation',location+","+item);
	var Result='';
	var PickFaceSearchResults = new Array();
	var PickFaceFilters = new Array();
	var PickFaceColumns = new Array();
	if(item!=null && item!='' && item!='null')
		PickFaceFilters.push(new nlobjSearchFilter('custrecord_pickfacesku', null, 'anyof', item)); 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_pickbinloc', null, 'anyof', location)); 
	PickFaceFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_priorityputaway', null, 'is', 'T'));

	PickFaceColumns[0] = new nlobjSearchColumn('custrecord_pickbinloc');
	PickFaceColumns[1] = new nlobjSearchColumn('custrecord_pickfacesku');
	PickFaceColumns[2] = new nlobjSearchColumn('custrecord_pickface_ebiz_lpno');

	PickFaceSearchResults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc', null, PickFaceFilters, PickFaceColumns);

	nlapiLogExecution('ERROR', 'Return Value',PickFaceSearchResults);
	nlapiLogExecution('ERROR', 'Out of getPickFaceLocation');

	return PickFaceSearchResults;
}