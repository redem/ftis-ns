/***************************************************************************
������������������������������� � ����������������������������� ��eBizNET
�����������������������                           eBizNET Solutions Inc
****************************************************************************
*
*� $Source: /cvs/products/eBizNET4.5/EoN/RF/Inbound/Suitelet/ebiz_RF_Cart_PickFaceException.js,v $
*� $Revision: 1.2.2.6.4.4.2.4 $
*� $Date: 2014/06/13 07:50:02 $
*� $Author: skavuri $
*� $Name: t_NSWMS_2014_1_3_125 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_RF_Cart_PickFaceException.js,v $
*� Revision 1.2.2.6.4.4.2.4  2014/06/13 07:50:02  skavuri
*� Case# 20148882 (added Focus Functionality for Textbox)
*�
*� Revision 1.2.2.6.4.4.2.3  2014/05/30 00:26:47  nneelam
*� case#  20148622
*� Stanadard Bundle Issue Fix.
*�
*� Revision 1.2.2.6.4.4.2.2  2013/06/11 14:30:41  schepuri
*� Error Code Change ERROR to DEBUG
*�
*� Revision 1.2.2.6.4.4.2.1  2013/04/17 16:04:01  skreddy
*� CASE201112/CR201113/LOG201121
*� added meta tag
*�
*� Revision 1.2.2.6.4.4  2013/02/07 15:10:34  schepuri
*� CASE201112/CR201113/LOG201121
*� disabling ENTER Button func added
*�
*� Revision 1.2.2.6.4.3  2012/09/27 10:54:36  grao
*� CASE201112/CR201113/LOG201121
*�
*� Converting multiple language with given Spanish terms
*�
*� Revision 1.2.2.6.4.2  2012/09/26 12:45:35  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multi language without small characters
*�
*� Revision 1.2.2.6.4.1  2012/09/21 14:57:16  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multilanguage
*�
*� Revision 1.2.2.6  2012/05/15 22:49:24  rrpulicherla
*� CASE201112/CR201113/LOG201121
*�
*� Record type issue fix
*�
*� Revision 1.2.2.5  2012/04/17 10:38:39  rrpulicherla
*� CASE201112/CR201113/LOG201121
*�
*� RF Cart To Checkin
*�
*� Revision 1.2.2.4  2012/03/16 13:56:23  spendyala
*� CASE201112/CR201113/LOG201121
*� Disable-button functionality is been added.
*�
*� Revision 1.2.2.3  2012/02/23 13:48:08  schepuri
*� CASE201112/CR201113/LOG201121
*� fixed TPP issues
*�
*� Revision 1.3  2012/02/23 09:06:39  rmukkera
*� CASE201112/CR201113/LOG201121
*� cartlp parameter added to poarray
*�
*� Revision 1.2  2012/02/16 10:32:55  schepuri
*� CASE201112/CR201113/LOG201121
*� Added FunctionkeyScript
*�
*� Revision 1.1  2012/02/02 08:58:04  rmukkera
*� CASE201112/CR201113/LOG201121
*�   cartlp new file
*�
*
****************************************************************************/


function PickFaceException(request, response){
	if (request.getMethod() == 'GET') {

		var poNo = request.getParameter('custparam_poid');
		var poItem = request.getParameter('custparam_poitem');
		var poLineNo = request.getParameter('custparam_lineno');
		var fetchedItemId = request.getParameter('custparam_fetcheditemid');
		var poInternalId = request.getParameter('custparam_pointernalid');
		var itemCube = request.getParameter('custparam_itemcube');
		var whLocation = request.getParameter('custparam_whlocation');
		var actualBeginDate = request.getParameter('custparam_actualbegindate');
		var actualBeginTime = request.getParameter('custparam_actualbegintime'); 
		var actualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var poQtyEntered = request.getParameter('custparam_poqtyentered');
		var poItemRemainingQty = request.getParameter('custparam_poitemremainingqty');
		var poLinePackcode = request.getParameter('custparam_polinepackcode');
		var poLineQuantity = request.getParameter('custparam_polinequantity');
		var poLineQuantityReceived = request.getParameter('custparam_polinequantityreceived');
		var poLineItemStatus = request.getParameter('custparam_polineitemstatus');
		var itemDescription = request.getParameter('custparam_itemdescription');
		var getPOLineItemStatusValue = request.getParameter('custparam_polineitemstatusValue');
		var trantype= request.getParameter('custparam_trantype');

		pickfaceRecommendedQuantity = request.getParameter('custparam_pickfaceexception');
		nlapiLogExecution('DEBUG', 'Pickface Recommended Quantity', pickfaceRecommendedQuantity);

		poQuantityEntered = request.getParameter('custparam_poqtyentered');
		nlapiLogExecution('DEBUG', 'PO Quantity Entered', poQuantityEntered);
		
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
	    
		var st0,st1,st2,st3,st4,st5,st6,st7;
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "RECOGER LA CARA CANTIDAD";
			st2 = "CANTIDAD IMPUTADA";   //INGRESAR LA CANTIDAD ACTUAL
			st3 = "&#191;QUIERES RECIBIR";
			st4 = "A GRANEL / PALLET UBICACI&#211;N (Y / N)?";	
			st5 = "Entre la opci&#243;n ";
			st6 = "ENVIAR";
			st7 = "ANTERIOR";

			
		}
		else
		{
			st0 = "";
			st1 = "PICKFACE QUANTITY";
			st2 = "ENTERED QUANTITY";
			st3 = "DO YOU WANT TO RECEIVE";
			st4 = "TO BULK / PALLET LOCATION (Y/N)?";
			st5 = "ENTER OPTION ";
			st6 = "SEND";
			st7 = "PREVIOUS";

		}
		
		
		

		var functionkeyHtml=getFunctionkeyScript('_rf_pickface_exception'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//html = html + " document.getElementById('cmdSend').focus();";        
		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";		
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "<body>";
		html = html + "	<form name='_rf_pickface_exception' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + " :<label>" + pickfaceRecommendedQuantity + "</label>";
		html = html + "				<input type='hidden' name='hdnpickfaceRecommendedQuantity' value=" + pickfaceRecommendedQuantity + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + " :<label>" + poQuantityEntered + "</label>";
		html = html + "				<input type='hidden' name='hdnpoQuantityEntered' value=" + poQuantityEntered + ">";
		html = html + "				<input type='hidden' name='hdnItemPackCode' value=" + poLinePackcode + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + poLineQuantity + ">";
		html = html + "				<input type='hidden' name='hdnQuantityReceived' value=" + poLineQuantityReceived + ">";
		html = html + "				<input type='hidden' name='hdnItemStatus' value=" + poLineItemStatus + "></td>";
		html = html + "				<input type='hidden' name='hdnItemCube' value=" + itemCube + "></td>";
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + actualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + actualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + actualBeginTimeAMPM + ">";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + itemDescription + ">";
		html = html + "				<input type='hidden' name='hdnPONo' value=" + poNo + ">";
		html = html + "				<input type='hidden' name='hdnPOItem' value=" + poItem + ">";
		html = html + "				<input type='hidden' name='hdnPOLineNo' value=" + poLineNo + ">";
		html = html + "				<input type='hidden' name='hdnFetchedItemId' value=" + fetchedItemId + ">";
		html = html + "				<input type='hidden' name='hdnPOInternalId' value=" + poInternalId + ">";
		html = html + "				<input type='hidden' name='hdnItemStatusValue' value=" + getPOLineItemStatusValue + ">";
		html = html + "				<input type='hidden' name='hdntrantype' value=" + trantype + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + "</td></tr>";
		html = html + "				<tr><td align = 'left'>" + st4;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st5;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enteroption' type='text' value='Y'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st6 + " <input name='cmdSend' id='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "				" + st7 + " <input name='cmdPrevious' type='submit' value='F7'/></td>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('cmdSend').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var optedEvent = request.getParameter('cmdPrevious');
		var pickfaceRecommendedQuantity = request.getParameter('hdnpickfaceRecommendedQuantity');
		var poQuantityEntered = request.getParameter('hdnpoQuantityEntered');
		var enteredOption = request.getParameter('enteroption').toUpperCase();

		var POarray = new Array();
		
		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);
    	
		
		var st8;
		if( getLanguage == 'es_ES')
		{
			st8 = "OPCI&#211;N V&#193;LIDA";
			
		}
		else
		{
			st8 = "INVALID OPTION";
			
		}
		

		// From Check-In process
		POarray["custparam_poid"] = request.getParameter('hdnPONo');
		POarray["custparam_poitem"] = request.getParameter('hdnPOItem');
		POarray["custparam_lineno"] = request.getParameter('hdnPOLineNo');
		POarray["custparam_fetcheditemid"] = request.getParameter('hdnFetchedItemId');
		POarray["custparam_pointernalid"] = request.getParameter('hdnPOInternalId');
		POarray["custparam_poqtyentered"] = request.getParameter('hdnpoQuantityEntered');
//		POarray["custparam_poitemremainingqty"] = request.getParameter('custparam_poitemremainingqty');
		POarray["custparam_polinepackcode"] = request.getParameter('hdnItemPackCode');
		POarray["custparam_polinequantity"] = request.getParameter('hdnQuantity');
		POarray["custparam_polinequantityreceived"] = request.getParameter('hdnQuantityReceived');
		POarray["custparam_polineitemstatus"] = request.getParameter('hdnItemStatus');
		POarray["custparam_itemdescription"] = request.getParameter('hdnItemDescription');
		POarray["custparam_itemcube"] = request.getParameter('hdnItemCube');
		POarray["custparam_actualbegindate"] = request.getParameter('hdnActualBeginDate');
		POarray["custparam_actualbegintime"] = request.getParameter('hdnActualBeginTime');
		POarray["custparam_actualbegintimeampm"] = request.getParameter('hdnActualBeginTimeAMPM');
		POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
		POarray["custparam_cartlpno"] = request.getParameter('custparam_cartlpno');
		POarray["custparam_polineitemstatusValue"] = request.getParameter('hdnItemStatusValue');
		POarray["custparam_trantype"] = request.getParameter('hdntrantype');
		//	if the previous button 'F8' is clicked, it has to go to the previous screen 
		//  ie., it has to go to RF Check-In PO screen.

		POarray["custparam_screenno"] = 'CRT14';
		POarray["custparam_error"] = st8;

		nlapiLogExecution('DEBUG', 'Entered Option', enteredOption);
		nlapiLogExecution('DEBUG', 'Warehouse Location', POarray["custparam_whlocation"]);

		if (optedEvent == 'F7'){
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_checkinqty', 'customdeploy_ebiz_rf_cart_checkinqty_di', false, POarray);
		}
		else{
			POarray["custparam_enteredOption"] = enteredOption;

			if (enteredOption == 'N'){
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_checkinqty', 'customdeploy_ebiz_rf_cart_checkinqty_di', false, POarray);
				nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
			}
			else if(enteredOption == 'Y')
			{
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_checkinitemsts', 'customdeploy_ebiz_rf_cart_checkinitemsts', false, POarray);
			}
			else if (enteredOption == "" || enteredOption != 'N' || enteredOption != 'Y'){
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'Entered Option', POarray["custparam_enteredOption"]);
			}
		}
	}
}
