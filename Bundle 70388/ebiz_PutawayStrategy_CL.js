/***************************************************************************
	  		   eBizNET Solutions Inc               
 ****************************************************************************/
/* 
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Masters/Client/ebiz_PutawayStrategy_CL.js,v $
 *     	   $Revision: 1.6.2.2.16.1 $
 *     	   $Date: 2015/11/14 13:01:36 $
 *     	   $Author: rrpulicherla $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * 
 * PRAMETERS
 *
 * DESCRIPTION
 * 
 * REVISION HISTORY
 * $Log: ebiz_PutawayStrategy_CL.js,v $
 * Revision 1.6.2.2.16.1  2015/11/14 13:01:36  rrpulicherla
 * 2015.2 issues
 *
 * Revision 1.6.2.2  2012/02/09 16:10:57  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Physical and Virtual Location changes
 *
 * Revision 1.8  2012/02/09 14:43:42  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Physical and Virtual Location changes
 *
 * Revision 1.7  2012/01/23 10:09:01  spendyala
 * CASE201112/CR201113/LOG201121
 * added new function CheckForMinimumRequiredFields to validate client for providing minimum fields.
 *
 * Revision 1.6  2012/01/06 13:05:02  spendyala
 * CASE201112/CR201113/LOG201121
 * added code to remove space.
 *
 * Revision 1.5  2012/01/06 09:17:55  spendyala
 * CASE201112/CR201113/LOG201121
 * added new filter to search criteria .
 *
 * Revision 1.4  2011/08/20 13:56:14  spendyala
 * CASE201112/CR201113/LOG201121
 * Fixed the issue while updating values and not allowing the user to create duplicate records
 *
 *  
 *
 *****************************************************************************/


function fnCheckPUTWStrategy(type)
{
	try
	{
		var varRuleID =  nlapiGetFieldValue('name');
		var vCompany = nlapiGetFieldValue('custrecord_ebizcompanypickput');  
		nlapiLogExecution('ERROR','vCompany',vCompany);
		var vSite	= nlapiGetFieldValue('custrecord_ebizsitepickput');  
		var vLocGroup	= nlapiGetFieldValue('custrecord_locationgrouppickrule');  
		var vSeqNo	= nlapiGetFieldValue('custrecord_sequencenumberpickrule'); 
		var vId = nlapiGetFieldValue('id');  
		if(varRuleID != "" && varRuleID != null )
		{
			var filters = new Array();
			filters[0] = new nlobjSearchFilter('name', null, 'is',varRuleID.replace(/\s+$/, ""));//Added by suman on 06/01/12 to trim spaces at right.
			filters[1] = new nlobjSearchFilter('custrecord_ebizsitepickput', null, 'anyof',['@NONE@',vSite]);
			if(vId!=null && vId!="")
			{
				filters[2] = new nlobjSearchFilter('internalid', null, 'noneof',vId);	
			}
			var searchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters);
			if (searchresults != null && searchresults.length > 0)
			{
				alert("This Record already exists.");
				return false;
			}
			else
			{ 
//				filters1[0] = new nlobjSearchFilter('custrecord_locationgrouppickrule', null, 'anyof',[vLocGroup]);
//				filters1[0] = new nlobjSearchFilter('name', null, 'is',varRuleID);
				var filters1 = new Array();
				filters1[0] = new nlobjSearchFilter('custrecord_sequencenumberpickrule', null, 'equalTo',[vSeqNo]);
				filters1[1] = new nlobjSearchFilter('custrecord_ebizsitepickput', null, 'anyof',['@NONE@',vSite]);
				if(vId!=null && vId!="")
				{
					filters1[2] = new nlobjSearchFilter('internalid', null, 'noneof',vId);	
				}
				var searchresults1 = nlapiSearchRecord('customrecord_ebiznet_putaway_rule', null, filters1);
				if (searchresults1 != null && searchresults1.length > 0){
					alert("This Location and Sequence Number Combination already exists.");
					return false;
				}		
				else
				{
					var result=CheckForMinimumRequiredFields();
					return result;
				}
			} 
		}
		else
		{
			var result=CheckForMinimumRequiredFields();
			return result;
		}
	}
	catch(error)
	{
		nlapiLogExecution('ERROR','vCompanyerror',error);
	}
}



/**
 * @returns {Boolean}
 */
function CheckForMinimumRequiredFields()
{
	var LocGr=nlapiGetFieldValue('custrecord_locationgrouppickrule');
	var PutZone=nlapiGetFieldValue('custrecord_putawayzonepickrule');
	//var AutoOverFlow=nlapiGetFieldValue('custrecord_autooverflowflagpickrule');
	if((LocGr==''||LocGr==null)&&(PutZone==''||PutZone==null))
	{
		alert('Please select atleast one among Location Group/Put zone');
		return false;
	}
	else
		return true;

}