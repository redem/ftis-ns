//profilerInitialize("eBiz_PickGenerationScheduler");

/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_Pickgeneration_Sch.js,v $
 *     	   $Revision: 1.1.4.4.2.63.2.8 $
 *     	   $Date: 2015/11/27 11:48:53 $
 *     	   $Author: svanama $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_Pickgeneration_Sch.js,v $
 * Revision 1.1.4.4.2.63.2.8  2015/11/27 11:48:53  svanama
 * Case # 201415291
 *
 * Revision 1.1.4.4.2.63.2.7  2015/11/13 15:38:37  snimmakayala
 * 201415635
 *
 * Revision 1.1.4.4.2.63.2.6  2015/11/03 23:36:30  rrpulicherla
 * FO changes changes
 *
 * Revision 1.1.4.4.2.63.2.5  2015/10/23 23:20:27  snimmakayala
 * 201415191
 *
 * Revision 1.1.4.4.2.63.2.4  2015/10/20 10:39:08  grao
 * Briggs issue fixes 201414878
 *
 * Revision 1.1.4.4.2.63.2.3  2015/09/23 14:58:03  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.1.4.4.2.63.2.2  2015/09/22 23:31:59  snimmakayala
 * 201414495
 *
 * Revision 1.1.4.4.2.63.2.1  2015/09/18 13:40:16  snimmakayala
 * Case#: 201413147
 *
 * Revision 1.1.4.4.2.64  2015/09/16 15:38:26  snimmakayala
 * no message
 *
 * Revision 1.1.4.4.2.63  2015/09/03 15:23:54  snimmakayala
 * Case#: 201414186
 *
 * Revision 1.1.4.4.2.62  2015/09/02 07:14:14  schepuri
 * case# 201413967
 *
 * Revision 1.1.4.4.2.61  2015/08/25 12:53:13  nneelam
 * case# 201413834
 *
 * Revision 1.1.4.4.2.60  2015/08/25 09:37:16  snimmakayala
 * no message
 *
 * Revision 1.1.4.4.2.59  2015/08/24 14:42:43  schepuri
 * case# 201414038
 *
 * Revision 1.1.4.4.2.58  2015/08/18 10:42:42  rrpulicherla
 * Case#201413343
 *
 * Revision 1.1.4.4.2.57  2015/08/07 19:06:02  snimmakayala
 * Case#: 201413918
 *
 * Revision 1.1.4.4.2.56  2015/08/07 15:39:58  skreddy
 * Case# 201413328
 * 2015.2 issue fix
 *
 * Revision 1.1.4.4.2.55  2015/07/15 15:23:06  skreddy
 * Case# 201413396
 * Briggs SB issue fix
 *
 * Revision 1.1.4.4.2.54  2015/07/13 07:42:51  snimmakayala
 * Case#: 201413420
 * Wave List and Order List options.
 *
 * Revision 1.1.4.4.2.53  2015/06/24 15:42:21  mpragada
 * Case# 201412758
 * EDI Issue Fixes
 *
 * Revision 1.1.4.4.2.52  2015/06/09 10:07:05  snimmakayala
 * 201224401
 *
 * Revision 1.1.4.4.2.27.2.21  2015/06/09 09:47:58  snimmakayala
 * 201224401
 *
 * Revision 1.1.4.4.2.27.2.20  2015/04/01 13:46:58  rrpulicherla
 * Case#201411317
 *
 * Revision 1.1.4.4.2.27.2.19  2015/03/24 14:46:23  grao
 * LP SB issue fixes  201412144
 *
 * Revision 1.1.4.4.2.27.2.18  2015/03/24 14:34:54  schepuri
 * case# 201412175
 *
 * Revision 1.1.4.4.2.27.2.17  2015/03/23 14:12:15  schepuri
 * case# 201412144
 *
 * Revision 1.1.4.4.2.27.2.16  2015/03/18 15:42:58  sponnaganti
 * Case# 201412069
 * LP SB issue fix
 *
 * Revision 1.1.4.4.2.27.2.15  2015/03/17 10:13:06  sponnaganti
 * Case# 201412049
 * Lonely Planet SB issue fix
 *
 * Revision 1.1.4.4.2.27.2.14  2015/02/04 08:16:00  sponnaganti
 * Case# 201411508
 * True Fab outbound process for selected lot in so/to
 *
 * Revision 1.1.4.4.2.27.2.13  2015/01/30 20:05:57  gkalla
 * Case# 201411334
 * TF Pick generation issue
 *
 * Revision 1.1.4.4.2.27.2.12  2015/01/29 14:48:28  grao
 * Alphacomm production 201222651 issue fixes
 *
 * Revision 1.1.4.4.2.27.2.11  2014/12/26 14:22:04  skreddy
 * Case# 201411180
 * CT Prod Issue Fixed
 *
 * Revision 1.1.4.4.2.27.2.10  2014/11/26 15:32:51  grao
 * Case# 201411130 Deal med Issue fixes
 *
 * Revision 1.1.4.4.2.27.2.9  2014/11/24 09:12:37  snimmakayala
 * Case#: 201411116
 * Issue: Multiple Clusters per Container LP
 *
 * Revision 1.1.4.4.2.27.2.8  2014/11/24 08:58:51  snimmakayala
 * no message
 *
 * Revision 1.1.4.4.2.27.2.7  2014/09/19 14:27:58  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Changes
 *
 * Revision 1.1.4.4.2.27.2.6  2014/09/19 12:56:33  snimmakayala
 * Case: 201410288
 * Pick Strategies by Order Priority
 *
 * Revision 1.1.4.4.2.27.2.5  2014/09/16 14:45:47  skreddy
 * case # 201410299
 *  TPP SB issue fix
 *
 * Revision 1.1.4.4.2.27.2.4  2014/09/13 02:25:06  snimmakayala
 * Case: 201410288
 * Pick Strategies by Order Priority
 *
 * Revision 1.1.4.4.2.27.2.3  2014/08/14 10:10:33  sponnaganti
 * case# 20149995
 * stnd bundle issue fix
 *
 * Revision 1.1.4.4.2.27.2.2  2014/07/18 15:10:34  sponnaganti
 * Case# 20148741
 * Stnd Bundle Issue fix
 *
 * Revision 1.1.4.4.2.27.2.1  2014/07/16 09:32:06  sponnaganti
 * Case# 20149469
 * Stnd Bundle issue fix
 *
 * Revision 1.1.4.4.2.27  2014/06/23 14:41:50  snimmakayala
 * 20148940
 *
 * Revision 1.1.4.4.2.26  2014/06/16 06:59:43  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148850
 *
 * Revision 1.1.4.4.2.25  2014/05/30 11:49:15  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20121897
 *
 * Revision 1.1.4.4.2.24  2014/05/27 07:36:58  snimmakayala
 * Case#: 20148135
 * Schedule Script Status
 *
 * Revision 1.1.4.4.2.23  2014/05/14 14:02:11  gkalla
 * case#201219405
 * LL Info1 reset to null
 *
 * Revision 1.1.4.4.2.22  2014/05/12 14:03:21  skreddy
 * case # 20148330
 * MHP SB issue fix
 *
 * Revision 1.1.4.4.2.21  2014/04/29 06:59:52  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148028
 *
 * Revision 1.1.4.4.2.20  2014/04/15 13:40:18  snimmakayala
 * Case #: 201219227
 *
 * Revision 1.1.4.4.2.19  2014/04/07 06:43:41  gkalla
 * case#20127911,20140010
 * LL Pick generation Kit issue and location group null issue
 *
 * Revision 1.1.4.4.2.18  2014/04/01 06:33:59  skreddy
 * case 20127847
 * LL prod  issue fix
 *
 * Revision 1.1.4.4.2.17  2014/03/29 21:34:31  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to 20127844
 *
 * Revision 1.1.4.4.2.16  2014/03/19 09:09:42  snimmakayala
 * Case #: 20127734
 *
 * Revision 1.1.4.4.2.15  2014/03/17 13:14:38  snimmakayala
 * no message
 *
 * Revision 1.1.4.4.2.14  2014/02/05 12:29:28  mbpragada
 * Case# 20125002
 * Duplicate UCC labels generation
 *
 * Revision 1.1.4.4.2.13  2014/01/20 10:18:15  snimmakayala
 * Case# : 20126503
 * Cartonization Issue
 *
 * Revision 1.1.4.4.2.12  2014/01/17 14:14:20  schepuri
 * 20126835
 * standard bundle issue fix
 *
 * Revision 1.1.4.4.2.11  2013/10/10 13:17:13  mbpragada
 * Case# 20125002
 * Duplicate UCC labels generation
 *
 * Revision 1.1.4.4.2.10  2013/10/10 12:04:15  mbpragada
 * Case# 20125002
 * Duplicate UCC labels generation
 *
 * Revision 1.1.4.4.2.9  2013/09/19 15:17:59  rmukkera
 * Case# 20124446
 *
 * Revision 1.1.4.4.2.8  2013/08/23 06:08:20  snimmakayala
 * Case# 20124030
 * NLS - UAT ISSUES
 * (Cartonization is not generating properly when the ship carton is enabled for EACH)
 *
 * Revision 1.1.4.4.2.7  2013/08/21 02:32:42  snimmakayala
 * Case# :20123984
 * GSUSA - PICKGEN ISSUE
 *
 * Revision 1.1.4.4.2.6  2013/06/19 22:59:45  gkalla
 * CASE201112/CR201113/LOG201121
 * As part of optimistic locking
 *
 * Revision 1.1.4.4.2.5  2013/06/18 13:33:11  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * nls issue fixes
 *
 * Revision 1.1.4.4.2.4  2013/04/16 14:55:58  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.1.4.4.2.3  2013/03/21 14:18:14  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.1.4.4.2.2  2013/03/19 11:53:20  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.1.4.4.2.1  2013/02/26 13:02:23  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Marged from Boombah.
 *
 * Revision 1.1.4.4  2013/02/16 12:11:10  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue fixes.
 *
 * Revision 1.1.4.3  2013/01/09 15:33:54  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * inactived sub components issue in pick generation.
 * .
 *
 * Revision 1.1.4.2  2012/12/05 08:37:37  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA UAT Fixes.
 *
 * Revision 1.2.2.35  2012/07/17 12:41:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Performance Tuning (Bulk Insert & Update)
 *
 * Revision 1.2.2.34  2012/07/17 08:16:55  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Performance Tuning (Bulk Insert)
 *
 * Revision 1.2.2.33  2012/07/13 00:41:23  gkalla
 * CASE201112/CR201113/LOG201121
 * Added GerUOM function
 *
 * Revision 1.2.2.32  2012/07/12 14:19:14  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Performance Tuning
 *
 * Revision 1.2.2.31  2012/07/11 14:34:00  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Updating LP Type in LP Master.
 *
 * Revision 1.2.2.30  2012/07/10 23:12:06  gkalla
 * CASE201112/CR201113/LOG201121
 * Alert Mails
 *
 * Revision 1.2.2.29  2012/06/25 11:29:36  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Strategy after UOM breakdown
 *
 * Revision 1.2.2.28  2012/06/22 12:25:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT and Prod issue fixes
 *
 * Revision 1.2.2.27  2012/06/15 07:21:16  spendyala
 * CASE201112/CR201113/LOG201121
 * While picks generation all UOM are taken consideration
 * i.e., Pallet,Case,InnerPallet and Each
 *
 * Revision 1.2.2.26  2012/06/04 14:16:30  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Strategy after UOM breakdown
 *
 * Revision 1.2.2.25  2012/05/22 15:28:55  snimmakayala
 * CASE201112/CR201113/LOG201121
 * To fix replenishment issue
 *
 * Revision 1.2.2.24  2012/05/21 17:16:27  gkalla
 * CASE201112/CR201113/LOG201121
 * To fix replenishment issue
 *
 * Revision 1.2.2.23  2012/05/18 18:01:56  gkalla
 * CASE201112/CR201113/LOG201121
 * For replenishment issue
 *
 * Revision 1.2.2.22  2012/05/17 15:56:33  gkalla
 * CASE201112/CR201113/LOG201121
 * To fix Replenishment issue
 *
 * Revision 1.2.2.21  2012/05/16 23:35:48  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * kit to order
 *
 * Revision 1.2.2.20  2012/05/14 23:19:13  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Kit to order
 *
 * Revision 1.2.2.19  2012/05/11 09:54:23  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue fixes for GSUSA
 *
 * Revision 1.2.2.18  2012/05/09 07:52:37  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Partial Status Handling
 *
 * Revision 1.2.2.17  2012/05/02 12:33:26  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Pick generation
 *
 * Revision 1.2.2.16  2012/04/30 21:27:09  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.2.2.15  2012/04/30 12:33:25  svanama
 * CASE201112/CR201113/LOG201121
 * PickReport Code  added
 *
 * Revision 1.2.2.14  2012/04/27 14:12:33  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Replenishment
 *
 * Revision 1.2.2.13  2012/04/25 15:21:34  spendyala
 * CASE201112/CR201113/LOG201121
 * While Setting Distinct Container# fail picks rec shld not take under consideration.
 *
 * Revision 1.2.2.12  2012/04/19 16:12:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Zone Picking and SKIP
 *
 * Revision 1.17  2012/04/19 15:13:12  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Zone Picking and SKIP
 *
 * Revision 1.16  2012/04/17 08:16:39  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pool of records
 *
 * Revision 1.15  2012/03/14 07:06:01  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Report changes  and undefined container lp issue for TPP
 *
 * Revision 1.14  2012/03/09 09:41:59  schepuri
 * CASE201112/CR201113/LOG201121
 * code merge from 1.2.2.8
 *
 * Revision 1.13  2012/03/09 07:16:55  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.12  2012/03/02 01:11:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.11  2012/02/28 01:22:19  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.10  2012/02/24 12:19:33  gkalla
 * CASE201112/CR201113/LOG201121
 * To fix the issue for TPP , LPs are creating for each line
 *
 * Revision 1.9  2012/02/23 17:58:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.8  2012/02/22 14:04:37  gkalla
 * CASE201112/CR201113/LOG201121
 * Added WMS Carrier line level functionality
 *
 * Revision 1.7  2012/02/21 15:04:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.6  2012/02/16 00:55:58  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.5  2012/02/09 14:43:43  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Physical and Virtual Location changes
 *
 * Revision 1.4  2012/01/27 06:51:52  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Generation UE
 *
 * Revision 1.3  2012/01/13 16:50:36  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Generation User Event changes
 *
 * Revision 1.2  2011/12/26 22:50:10  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Checkin changes
 *
 * Revision 1.1  2011/12/21 16:45:13  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Gen User Event
 *
 * Revision 1.27  2011/12/05 14:58:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.26  2011/11/28 07:02:47  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Confirm changes
 *
 * Revision 1.25  2011/11/25 08:54:37  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Default LOT
 *
 * Revision 1.23  2011/11/24 12:28:29  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Exception.
 *
 * Revision 1.22  2011/11/21 13:18:02  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Default Lot Functionality. (Item as Lot)
 *
 * Revision 1.21  2011/11/15 16:05:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * set display flag to 'N' when updating inventory
 *
 * Revision 1.20  2011/11/04 11:03:04  svanama
 * CASE201112/CR201113/LOG201121
 * InsertExceptionLog  is added in try catch block
 *
 * Revision 1.19  2011/10/04 16:06:19  gkalla
 * CASE201112/CR201113/LOG201121
 * For Lot# entry updation
 *
 * Revision 1.18  2011/10/04 12:11:54  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.17  2011/10/03 09:30:49  jrnarsupalli
 * CASE201112/CR201113/LOG201121
 * RM Comments: Corrected CMLOG number in the previous version.
 *
 * Revision 1.16  2011/09/30 07:09:10  snimmakayala
 * CASE201112/CR201113/LOG20112
 * Label printing related changes done by Siva Kumar.V
 *
 * Revision 1.12  2011/09/26 08:44:47  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.11  2011/09/24 08:46:00  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.10  2011/09/23 13:21:21  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.9  2011/09/23 07:24:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Actual End Date Filter is commented
 *
 * Revision 1.8  2011/09/22 15:42:37  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Logs Added
 *
 * Revision 1.7  2011/09/21 14:13:05  rmukkera
 * CASE201112/CR201113/LOG201121
 * code  modification in calling transform record
 *
 * Revision 1.6  2011/09/20 13:27:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.5  2011/09/19 10:28:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UOM Conversion
 *
 * Revision 1.4  2011/09/16 07:28:58  rmukkera
 * CASE201112/CR201113/LOG201121
 * source code changes included both open and close tasks
 * added cvs header
 *

 **************************************************************************** */

function getDistinctMethodsList(distinctorderslist){
	var distinctorders = new Array();
	if(distinctorderslist!=null && distinctorderslist.length>0){	
		label:for (var z = 0; z < distinctorderslist.length; z++){
			for (var y = 0; y < distinctorders.length; y++){
				if((distinctorderslist[z][0]==distinctorders[y][0]) && (distinctorderslist[z][1]==distinctorders[y][1])){
					continue label;
				}					
			}	
			distinctorders[distinctorders.length]=distinctorderslist[z];
		}
	}
	return distinctorders;
}

function getContainerSizeforOrder(containerslist,totaltaskcube,totaltaskweight,cartonizationmethod,itemdims,
		opentasklist,whLocation,vebizmethodno)
{	
	var str = 'Container List. = ' + containerslist + '<br>';
	str = str + 'totaltaskcube. = ' + totaltaskcube + '<br>';	
	str = str + 'totaltaskweight. = ' + totaltaskweight + '<br>';
	str = str + 'cartonizationmethod. = ' + cartonizationmethod + '<br>';
	str = str + 'whLocation. = ' + whLocation + '<br>';
	str = str + 'vebizmethodno. = ' + vebizmethodno + '<br>';

	nlapiLogExecution('DEBUG', 'Into getContainerSizeforOrder', str);

	if(isNaN(totaltaskcube))
		totaltaskcube=0;

	if(isNaN(totaltaskweight))
		totaltaskweight=0;

	var contsize='';


	for(l=0;l<containerslist.length;l++)
	{  
		var containerlength = containerslist[l].getValue('custrecord_length');
		var containerwidth = containerslist[l].getValue('custrecord_widthcontainer');
		var containerheight = containerslist[l].getValue('custrecord_heightcontainer');
		var containercube = containerslist[l].getValue('custrecord_cubecontainer');
		var containerweight= containerslist[l].getValue('custrecord_maxweight');
		var containername = containerslist[l].getValue('custrecord_containername');
		var packfactor=containerslist[l].getValue('custrecord_packfactor');
		if(packfactor==null || packfactor=='')
			packfactor=0;
		var containeractcube=parseFloat(containercube)-(parseFloat(containercube)*(parseFloat(packfactor)/100));
		nlapiLogExecution('DEBUG', 'containeractcube',containeractcube);
		nlapiLogExecution('DEBUG', 'containerweight',containerweight);

		if(cartonizationmethod=='1') // Cube and Weight
		{
			nlapiLogExecution('DEBUG', 'Into Cube and Weight');
			if((containername!='SHIPASIS') && (parseFloat(containeractcube)>=parseFloat(totaltaskcube)) 
					&& (parseFloat(containerweight)>=parseFloat(totaltaskweight)))
			{
				contsize=containerslist[l].getValue('Internalid');
				//nlapiLogExecution('DEBUG', 'contsize',contsize);	
				return contsize;
			}
		}
		else if(cartonizationmethod=='2') // Weight
		{
			nlapiLogExecution('DEBUG', 'Into Weight');
			if((containername!='SHIPASIS') && (parseFloat(containerweight)>=parseFloat(totaltaskweight)))
			{
				contsize=containerslist[l].getValue('Internalid');
				//nlapiLogExecution('DEBUG', 'contsize',contsize);
				return contsize;
			}
		}
		else if(cartonizationmethod=='3') // Cube
		{
			nlapiLogExecution('DEBUG', 'Into Cube');
			if((containername!='SHIPASIS') && (parseFloat(containeractcube)>=parseFloat(totaltaskcube)))
			{
				contsize=containerslist[l].getValue('Internalid');
				//nlapiLogExecution('DEBUG', 'contsize',contsize);	
				return contsize;
			}
		}
		else if(cartonizationmethod=='5') // Max and Min
		{
			nlapiLogExecution('DEBUG', 'Into Min and Max');

			if(itemdims!=null && itemdims!='' && itemdims.length>0)
			{
				var itemcube = itemdims[0].getValue('custrecord_ebizcube');
				var itemweight = itemdims[0].getValue('custrecord_ebizweight');
				var itemlength = itemdims[0].getValue('custrecord_ebizlength');
				var itemwidth = itemdims[0].getValue('custrecord_ebizwidth');
				var itemheight = itemdims[0].getValue('custrecord_ebizheight');

				var str = 'totaltaskcube. = ' + totaltaskcube + '<br>';				
				str = str + 'totaltaskweight. = ' + totaltaskweight + '<br>';
				str = str + 'containername. = ' + containername + '<br>';
				str = str + 'containerweight. = ' + containerweight + '<br>';	
				str = str + 'itemweight. = ' + itemweight + '<br>';
				str = str + 'containeractcube. = ' + containeractcube + '<br>';	
				str = str + 'itemcube. = ' + itemcube + '<br>';			
				str = str + 'itemlength. = ' + itemlength + '<br>';
				str = str + 'itemwidth. = ' + itemwidth + '<br>';
				str = str + 'itemheight. = ' + itemheight + '<br>';
				str = str + 'containerlength. = ' + containerlength + '<br>';
				str = str + 'containerwidth. = ' + containerwidth + '<br>';
				str = str + 'containerheight. = ' + containerheight + '<br>';

				nlapiLogExecution('DEBUG', 'Size Determination1', str);

				if(isNaN(itemcube))
					itemcube=0;

				if(isNaN(itemweight))
					itemweight=0;

				if(isNaN(itemlength))
					itemlength=0;

				if(isNaN(itemwidth))
					itemwidth=0;

				if(isNaN(itemheight))
					itemheight=0;

				if((containername!='SHIPASIS') && (parseFloat(totaltaskcube)<=parseFloat(containeractcube)) && 
						(parseFloat(totaltaskweight)<=parseFloat(containerweight)))
				{
					var itemmaxdim = Math.max(parseFloat(itemlength), parseFloat(itemwidth), parseFloat(itemheight));
					var itemmindim = Math.min(parseFloat(itemlength), parseFloat(itemwidth), parseFloat(itemheight));

					var containermaxdim = Math.max(parseFloat(containerlength), parseFloat(containerwidth), parseFloat(containerheight));
					var containermindim = Math.min(parseFloat(containerlength), parseFloat(containerwidth), parseFloat(containerheight));

					var str1 = 'itemmaxdim. = ' + itemmaxdim + '<br>';				
					str1 = str1 + 'itemmindim. = ' + itemmindim + '<br>';
					str1 = str1 + 'containermaxdim. = ' + containermaxdim + '<br>';
					str1 = str1 + 'containermindim. = ' + containermindim + '<br>';

					nlapiLogExecution('DEBUG', 'Size Determination2', str1);

					if(itemmaxdim<=containermaxdim && itemmindim<=containermindim)
					{
						contsize=containerslist[l].getValue('Internalid');
						//nlapiLogExecution('DEBUG', 'contsize',contsize);	
						return contsize;
					}
				}
			}
			else
			{
				var vtotaltaskcube=0;
				var vtotaltaskweight=0;
				var itemarray = new Array();

				for(t=0;t<opentasklist.length;t++)
				{ 
					if(vebizmethodno==opentasklist[t].getValue('custrecord_ebizmethod_no'))
					{
						itemarray.push(opentasklist[t].getValue('custrecord_ebiz_sku_no'));
						vtotaltaskweight = parseFloat(vtotaltaskweight)+parseFloat(opentasklist[t].getValue('custrecord_total_weight'));
						vtotaltaskcube = parseFloat(vtotaltaskcube)+parseFloat(opentasklist[t].getValue('custrecord_totalcube'));
					}					
				}	

				if((containername!='SHIPASIS') && (parseFloat(containeractcube)>=parseFloat(vtotaltaskcube)) 
						&& (parseFloat(containerweight)>=parseFloat(vtotaltaskweight)))
				{

					var gitemmaxdim=0;
					var gitemmindim=0;

					var maxitemdimarr = new Array();
					var minitemdimarr = new Array();
					if(itemarray!=null && itemarray!='')
					{
						nlapiLogExecution('DEBUG', 'itemarray', itemarray);
						itemarray=removeDuplicateElement(itemarray);
						nlapiLogExecution('DEBUG', 'itemarray', itemarray);

						var allitemdimsarray = getdimsforallitems(itemarray,whLocation);

						for(s=0;allitemdimsarray!=null && s<allitemdimsarray.length;s++)
						{ 
							var itemlength = allitemdimsarray[s].getValue('custrecord_ebizlength');
							var itemwidth = allitemdimsarray[s].getValue('custrecord_ebizwidth');
							var itemheight = allitemdimsarray[s].getValue('custrecord_ebizheight');

							var itemmaxdim = Math.max(parseFloat(itemlength), parseFloat(itemwidth), parseFloat(itemheight));
							var itemmindim = Math.min(parseFloat(itemlength), parseFloat(itemwidth), parseFloat(itemheight));

							maxitemdimarr.push(itemmaxdim);
							minitemdimarr.push(itemmindim);
						}
					}

					nlapiLogExecution('DEBUG', 'maxitemdimarr', maxitemdimarr);
					maxitemdimarr.sort(function(a,b){return b - a;});
					nlapiLogExecution('DEBUG', 'maxitemdimarr', maxitemdimarr);
					nlapiLogExecution('DEBUG', 'minitemdimarr', minitemdimarr);
					minitemdimarr.sort(function(a,b){return a - b;});
					nlapiLogExecution('DEBUG', 'minitemdimarr', minitemdimarr);

					for(a=0;maxitemdimarr!=null&&a<maxitemdimarr.length;a++)
					{
						gitemmaxdim = maxitemdimarr[0];
					}

					for(b=0;minitemdimarr!=null&&b<minitemdimarr.length;b++)
					{
						gitemmindim = minitemdimarr[0];
					}

					if(isNaN(gitemmaxdim))
						gitemmaxdim=0;

					if(isNaN(gitemmindim))
						gitemmindim=0;

					var containermaxdim = Math.max(parseFloat(containerlength), parseFloat(containerwidth), parseFloat(containerheight));
					var containermindim = Math.min(parseFloat(containerlength), parseFloat(containerwidth), parseFloat(containerheight));

					var str1 = 'itemmaxdim. = ' + gitemmaxdim + '<br>';				
					str1 = str1 + 'itemmindim. = ' + gitemmindim + '<br>';
					str1 = str1 + 'containermaxdim. = ' + containermaxdim + '<br>';
					str1 = str1 + 'containermindim. = ' + containermindim + '<br>';

					nlapiLogExecution('DEBUG', 'Size Determination in MAX and MIN by order level', str1);

					if(gitemmaxdim<=containermaxdim && gitemmindim<=containermindim)
					{
						contsize=containerslist[l].getValue('Internalid');
						//nlapiLogExecution('DEBUG', 'contsize',contsize);	
						return contsize;
					}
				}
			}
		}
	}

	nlapiLogExecution('DEBUG', 'Out of getContainerSizeforOrder (return Size) ',contsize);

	return contsize;
}


function getSizefromUsedCartons(containerslist,totalcube,totalweight,cartonizationmethod,itemdims,arrusedcartons,vpckzone,cartonstrategy)
{	
	var str = 'Container List. = ' + containerslist + '<br>';
	str = str + 'cartonizationmethod. = ' + cartonizationmethod + '<br>';
	str = str + 'totalcube. = ' + totalcube + '<br>';	
	str = str + 'totalweight. = ' + totalweight + '<br>';	
	str = str + 'vpckzone. = ' + vpckzone + '<br>';	

	nlapiLogExecution('DEBUG', 'Into getSizefromUsedCartons', str);

	if(isNaN(totalcube))
		totalcube=0;

	if(isNaN(totalweight))
		totalweight=0;

	var contsizearray = new Array();

	if(arrusedcartons!=null && arrusedcartons!='' && arrusedcartons.length>0)
	{		
		nlapiLogExecution('DEBUG', 'arrusedcartons length', arrusedcartons.length);

		for(k=0;k<arrusedcartons.length;k++)
		{  
			var contremcube = arrusedcartons[k][2];
			var contremwght = arrusedcartons[k][3];
			var size = arrusedcartons[k][1];
			var cartonlp = arrusedcartons[k][0];
			var vusdzone = arrusedcartons[k][4];

			var str = 'cartonlp. = ' + cartonlp + '<br>';
			str = str + 'size. = ' + size + '<br>';
			str = str + 'totalcube. = ' + totalcube + '<br>';	
			str = str + 'totalweight. = ' + totalweight + '<br>';	
			str = str + 'contremwght. = ' + contremwght + '<br>';	
			str = str + 'contremcube. = ' + contremcube + '<br>';	
			str = str + 'vusdzone. = ' + vusdzone + '<br>';	

			nlapiLogExecution('DEBUG', 'Size Determination1', str);
			if(vusdzone==vpckzone)
			{

				if((parseFloat(totalcube)<=parseFloat(contremcube)) && 
						(parseFloat(totalweight)<=parseFloat(contremwght)))
				{
					for(l=0;l<containerslist.length;l++)
					{  
						if(size == containerslist[l].getValue('Internalid'))
						{
							var containername = containerslist[l].getValue('custrecord_containername');
							var containerlength = containerslist[l].getValue('custrecord_length');
							var containerwidth = containerslist[l].getValue('custrecord_widthcontainer');
							var containerheight = containerslist[l].getValue('custrecord_heightcontainer');

							var containermaxdim = Math.max(parseFloat(containerlength), parseFloat(containerwidth), parseFloat(containerheight));
							var containermindim = Math.min(parseFloat(containerlength), parseFloat(containerwidth), parseFloat(containerheight));

							if(itemdims!=null && itemdims!='' && itemdims.length>0)
							{
								var itemcube = itemdims[0].getValue('custrecord_ebizcube');
								var itemweight = itemdims[0].getValue('custrecord_ebizweight');
								var itemlength = itemdims[0].getValue('custrecord_ebizlength');
								var itemwidth = itemdims[0].getValue('custrecord_ebizwidth');
								var itemheight = itemdims[0].getValue('custrecord_ebizheight');

								var itemmaxdim = Math.max(parseFloat(itemlength), parseFloat(itemwidth), parseFloat(itemheight));
								var itemmindim = Math.min(parseFloat(itemlength), parseFloat(itemwidth), parseFloat(itemheight));

								var str = 'totalcube. = ' + totalcube + '<br>';				
								str = str + 'totalweight. = ' + totalweight + '<br>';
								str = str + 'containername. = ' + containername + '<br>';
								str = str + 'contremwght. = ' + contremwght + '<br>';	
								str = str + 'itemweight. = ' + itemweight + '<br>';
								str = str + 'contremcube. = ' + contremcube + '<br>';	
								str = str + 'itemcube. = ' + itemcube + '<br>';			
								str = str + 'itemlength. = ' + itemlength + '<br>';
								str = str + 'itemwidth. = ' + itemwidth + '<br>';
								str = str + 'itemheight. = ' + itemheight + '<br>';
								str = str + 'containerlength. = ' + containerlength + '<br>';
								str = str + 'containerwidth. = ' + containerwidth + '<br>';
								str = str + 'containerheight. = ' + containerheight + '<br>';
								str = str + 'itemmaxdim. = ' + itemmaxdim + '<br>';
								str = str + 'itemmindim. = ' + itemmindim + '<br>';
								str = str + 'containermaxdim. = ' + containermaxdim + '<br>';
								str = str + 'containermindim. = ' + containermindim + '<br>';


								nlapiLogExecution('DEBUG', 'Size Determination2', str);

								if(isNaN(itemcube))
									itemcube=0;

								if(isNaN(itemweight))
									itemweight=0;

								if(isNaN(itemlength))
									itemlength=0;

								if(isNaN(itemwidth))
									itemwidth=0;

								if(isNaN(itemheight))
									itemheight=0;

								if(itemmaxdim<=containermaxdim && itemmindim<=containermindim)
								{
									var newcontremcube = parseFloat(contremcube)-parseFloat(totalcube);
									var newcontremwght = parseFloat(contremwght)-parseFloat(totalweight);
									var currow = [cartonlp,size,newcontremcube,newcontremwght];
									contsizearray.push(currow);
									nlapiLogExecution('DEBUG', 'Out of getSizefromUsedCartons (return Size) ',contsizearray);
									return contsizearray;
								}
							}
						}
					}
				}
			}
		}
	}

	nlapiLogExecution('DEBUG', 'Out of getSizefromUsedCartons (return Size) ',contsizearray);

	return contsizearray;
}


var optmizationarray = new Array();

function ActualCartonization(vwaveno,vordno,containerslist,whLocation,LPparent)
{
	nlapiLogExecution('DEBUG', 'Into ActualCartonization - Wave#', vwaveno);
	nlapiLogExecution('DEBUG', 'whLocation', whLocation);
	var totalweight=0;
	var totalcube=0;
	var contsize = "";	
	var uompackflag='';
	var itemsarray = new Array();
	var allitemdimsarray =  new Array();

	var opentasklist = getPickTaskDetailsforCartonization(vwaveno,vordno);
	if(opentasklist!=null && opentasklist!='' && opentasklist.length>0)
	{
		var ordermethodlist=new Array();

		for (var n = 0; n < opentasklist.length; n++)
		{
			var vebizmethodno = opentasklist[n].getValue('custrecord_ebizmethod_no');
			var vWMSCarrier = opentasklist[n].getValue('custrecord_ebizwmscarrier');
			whLocation = opentasklist[n].getValue('custrecord_wms_location');

			itemsarray.push(opentasklist[n].getValue('custrecord_ebiz_sku_no'));

			var currentRow = [vordno, vebizmethodno,vWMSCarrier];

			ordermethodlist[n] = currentRow;

			totalweight=parseFloat(totalweight)+parseFloat(opentasklist[n].getValue('custrecord_total_weight'));
			totalcube=parseFloat(totalcube)+parseFloat(opentasklist[n].getValue('custrecord_totalcube'));
		}


		if(itemsarray!=null && itemsarray!='')
		{
			//nlapiLogExecution('DEBUG', 'itemsarray', itemsarray);
			itemsarray=removeDuplicateElement(itemsarray);
			nlapiLogExecution('DEBUG', 'itemsarray', itemsarray);

			allitemdimsarray = getdimsforallitems(itemsarray,whLocation);
			//nlapiLogExecution('DEBUG', 'allitemdimsarray',allitemdimsarray);
		}

		nlapiLogExecution('DEBUG', 'allitemdimsarray',allitemdimsarray);

		var distinctmethods = getDistinctMethodsList(ordermethodlist);
		var wmsCarrier='';
		var prvWMSCarrier='-1';
		var newContainerLpNo='';
		var vpickzoneno='';
		var prvpickzoneno='-1';
		for (var m = 0; m < distinctmethods.length; m++)
		{
			var ebizorderno = distinctmethods[m][0];
			var ebizmethodno = distinctmethods[m][1];
			var cartonizationmethod='';
			nlapiLogExecution('DEBUG','ebizmethodno',ebizmethodno);

			var arrcubeweight = getCubeanddWeightbyMethod(vwaveno,ebizorderno,ebizmethodno)
			if(arrcubeweight!=null && arrcubeweight!='')
			{
				totalweight = arrcubeweight[0].getValue('custrecord_total_weight',null,'sum');
				totalcube = arrcubeweight[0].getValue('custrecord_totalcube',null,'sum');

				var str17 = 'totalweight. = ' + totalweight + '<br>';
				str17 = str17 + 'totalcube. = ' + totalcube + '<br>';
				str17 = str17 + 'ebizmethodno. = ' + ebizmethodno + '<br>';	

				nlapiLogExecution('DEBUG', 'Weight & Cube by Method', str17);
			}
			if(ebizmethodno!=null && ebizmethodno!='')
			{				
				var fields = ['custrecord_cartonization_method'];
				var pickmethodcolumns = nlapiLookupField('customrecord_ebiznet_pick_method', ebizmethodno, fields);
				if(pickmethodcolumns!=null)
					cartonizationmethod = pickmethodcolumns.custrecord_cartonization_method;				
			}

			nlapiLogExecution('DEBUG','cartonizationmethod',cartonizationmethod);

			if(cartonizationmethod==null || cartonizationmethod=="")					
				cartonizationmethod='4'; // By Order

			if(cartonizationmethod == '4') 
			{
				for (var k = 0; k < opentasklist.length; k++)
				{
					if(ebizmethodno==opentasklist[k].getValue('custrecord_ebizmethod_no'))
					{
						wmsCarrier = opentasklist[k].getValue('custrecord_ebizwmscarrier');
						vpickzoneno = opentasklist[k].getValue('custrecord_ebizzone_no');

						if(wmsCarrier == null || wmsCarrier == '')
							wmsCarrier='-2';

						if(vpickzoneno == null || vpickzoneno == '')
							vpickzoneno='-2';
						var vOTLocation=opentasklist[k].getValue('custrecord_wms_location');
						if(prvpickzoneno != vpickzoneno)
						{	
							newContainerLpNo=GetMaxLPNo('1', '2',vOTLocation);
							//nlapiLogExecution('DEBUG','newContainerLpNo',newContainerLpNo);
							//CreateMasterLPRecord(newContainerLpNo,'',totalweight,totalcube,'',whLocation,LPparent);
							CreateMasterLPRecordBulk(newContainerLpNo,'',totalweight,totalcube,'',vOTLocation,LPparent);
							prvpickzoneno = vpickzoneno;
							prvWMSCarrier = wmsCarrier;
						} 
						else if(prvWMSCarrier != wmsCarrier)
						{	
							newContainerLpNo=GetMaxLPNo('1', '2',vOTLocation);
							//nlapiLogExecution('DEBUG','newContainerLpNo',newContainerLpNo);
							//CreateMasterLPRecord(newContainerLpNo,'',totalweight,totalcube,'',whLocation,LPparent);
							CreateMasterLPRecordBulk(newContainerLpNo,'',totalweight,totalcube,'',vOTLocation,LPparent);
							prvWMSCarrier = wmsCarrier;
						} 

						var Recid = opentasklist[k].getId();

						var fieldNames = new Array(); 
						fieldNames.push('custrecord_container_lp_no');

						var newValues = new Array(); 
						newValues.push(newContainerLpNo);

						nlapiSubmitField('customrecord_ebiznet_trn_opentask', Recid, fieldNames, newValues);

					}
				}
			}
			else
			{
				if(containerslist != null && containerslist.length>0){
					contsize = getContainerSizeforOrder(containerslist,totalcube,totalweight,cartonizationmethod,null,
							opentasklist,whLocation,ebizmethodno);

					if(contsize!=null && contsize!='')
					{
						var containerLP1 = GetMaxLPNo('1', '2',whLocation);
						nlapiLogExecution('DEBUG', 'Container LP (Total Order) : ', containerLP1);
						//CreateMasterLPRecord(containerLP1,contsize,totalweight,totalcube,'',whLocation,LPparent);
						CreateMasterLPRecordBulk(containerLP1,contsize,totalweight,totalcube,'',whLocation,LPparent);
						for(j=0;j<opentasklist.length;j++){
							if(opentasklist[j].getValue('custrecord_ebizmethod_no')==ebizmethodno)
							{
								var internalid = opentasklist[j].getId();							
								updatesizeidinopentask(internalid,containerLP1,contsize);
							}
						}
					}
					else
					{
						var arrusedcartons = new Array();
						var sizearray  = new Array();
						for(k=0;k<opentasklist.length;k++){
							if(opentasklist[k].getValue('custrecord_ebizmethod_no')==ebizmethodno)
							{
								var uomlevel = opentasklist[k].getValue('custrecord_uom_level');
								var ebizskuno = opentasklist[k].getValue('custrecord_ebiz_sku_no');
								var itemdims = getUOMCube(uomlevel,ebizskuno,whLocation);								
								//totalweight = parseFloat(opentasklist[k].getValue('custrecord_total_weight'));
								//totalcube = parseFloat(opentasklist[k].getValue('custrecord_totalcube'));

								if(containerslist != null && containerslist.length>0){	

									if(cartonizationmethod=="1"){
										buildcartonsforcubeandweight(opentasklist[k],containerslist,uompackflag,LPparent);
									}
									else if(cartonizationmethod=="2"){
										buildcartonsforweight(opentasklist[k],containerslist,uompackflag,LPparent);
									} 
									else if(cartonizationmethod=="3"){
										buildcartonsforcube(opentasklist[k],containerslist,uompackflag,LPparent);
									}	
									else if(cartonizationmethod=="5"){
										//buildcartonsforminandmaxdims(opentasklist[k],containerslist,uompackflag,itemdims);
										var opentask = opentasklist[k];
										nlapiLogExecution('DEBUG', 'Into  buildcartonsforminandmaxdims Task Split', '');
										try{
											var totaltaskweight = parseFloat(opentask.getValue('custrecord_total_weight'));
											var totaltaskcube = parseFloat(opentask.getValue('custrecord_totalcube'));	
											var totaltaskqty = parseFloat(opentask.getValue('custrecord_expe_qty'));	
											var pickzone1 = opentask.getValue('custrecord_ebizzone_no');
											nlapiLogExecution('DEBUG', 'pickzone123', pickzone1);
											var remianingqty=totaltaskqty;
											nlapiLogExecution('DEBUG', 'totaltaskqty : ', totaltaskqty);
											var remweight = parseFloat(totaltaskweight);
											var remcube = parseFloat(totaltaskcube);
											var contsize="";
											var containerLP="";
											var cube="";
											var weight="";
											var exptdqty =0;
											var itemcube=0;
											var itemweight=0;
											var itemlength=0;
											var itemwidth=0;
											var itemheight=0;
											var itemmaxdim = 0;
											var itemmindim = 0;
											var itemuomqty = 0;

											var whsite=opentask.getValue('custrecord_wms_location');

											var uomlevel = opentask.getValue('custrecord_uom_level');
											var ebizskuno = opentask.getValue('custrecord_ebiz_sku_no');
											var indxarray = new Array();
											//var uomdims = getUOMCube(uomlevel,ebizskuno);
											if(itemdims!=null && itemdims!='')
											{
												itemcube = itemdims[0].getValue('custrecord_ebizcube');
												itemweight = itemdims[0].getValue('custrecord_ebizweight');
												itemlength = itemdims[0].getValue('custrecord_ebizlength');
												itemwidth = itemdims[0].getValue('custrecord_ebizwidth');
												itemheight = itemdims[0].getValue('custrecord_ebizheight');
												itemuomqty = itemdims[0].getValue('custrecord_ebizqty');

												itemmaxdim = Math.max(parseFloat(itemlength), parseFloat(itemwidth), parseFloat(itemheight));
												itemmindim = Math.min(parseFloat(itemlength), parseFloat(itemwidth), parseFloat(itemheight));
											}

											if(containerslist != null){ 

												nlapiLogExecution('DEBUG', 'arrusedcartons',arrusedcartons);

												if(arrusedcartons!=null && arrusedcartons!='' && arrusedcartons.length>0)
												{
													var splitarray = new Array();

													nlapiLogExecution('DEBUG', 'arrusedcartons length',arrusedcartons.length);
													for(t=0;t<arrusedcartons.length;t++)
													{				
														var usedCarton = arrusedcartons[t][0];
														var usedSize = arrusedcartons[t][1];
														var cartonremcube = arrusedcartons[t][2];
														var cartonremwght = arrusedcartons[t][3];
														var usedZone = arrusedcartons[t][4];
														nlapiLogExecution('DEBUG', 'usedZone',usedZone);
														nlapiLogExecution('DEBUG', 'vpickzoneno',vpickzoneno);

														if(parseFloat(itemcube)<=parseFloat(cartonremcube))
														{
															for(s=(containerslist.length)-1;s>=0;s--)
															{
																if(usedSize == containerslist[s].getValue('Internalid'))
																{
																	var vcontainerlength = containerslist[s].getValue('custrecord_length');
																	var vcontainerwidth = containerslist[s].getValue('custrecord_widthcontainer');
																	var vcontainerheight = containerslist[s].getValue('custrecord_heightcontainer');

																	var vcontainermaxdim = Math.max(parseFloat(vcontainerlength), parseFloat(vcontainerwidth), parseFloat(vcontainerheight));
																	var vcontainermindim = Math.min(parseFloat(vcontainerlength), parseFloat(vcontainerwidth), parseFloat(vcontainerheight));

																	var str7 = 'usedCarton. = ' + usedCarton + '<br>';
																	str7 = str7 + 'usedSize. = ' + usedSize + '<br>';
																	str7 = str7 + 'cartonremcube. = ' + cartonremcube + '<br>';	
																	str7 = str7 + 'cartonremwght. = ' + cartonremwght + '<br>';	
																	str7 = str7 + 'itemcube. = ' + itemcube + '<br>';	
																	str7 = str7 + 'itemmaxdim. = ' + itemmaxdim + '<br>';	
																	str7 = str7 + 'vcontainermaxdim. = ' + vcontainermaxdim + '<br>';
																	str7 = str7 + 'itemmindim. = ' + itemmindim + '<br>';
																	str7 = str7 + 'vcontainermindim. = ' + vcontainermindim + '<br>';
																	str7 = str7 + 'itemuomqty. = ' + itemuomqty + '<br>';

																	nlapiLogExecution('DEBUG', 'Size Determination in Used Cartons1', str7);

																	if(itemmaxdim<=vcontainermaxdim && itemmindim<=vcontainermindim)
																	{
																		var vqtybycube = Math.floor(parseFloat(cartonremcube)/parseFloat(itemcube));
																		var vqtybyweight = Math.floor(parseFloat(cartonremwght)/parseFloat(itemweight));
																		var vtasksplitqty = Math.min(parseFloat(vqtybycube), parseFloat(vqtybyweight));								
																		exptdqty = parseFloat(vtasksplitqty)*parseFloat(itemuomqty);;

//																		var vsplittaskweight = parseFloat(exptdqty)*parseFloat(itemweight);
//																		var vsplittaskcube = parseFloat(exptdqty)*parseFloat(itemcube);

																		var str5 = 'vqtybycube. = ' + vqtybycube + '<br>';
																		str5 = str5 + 'vqtybyweight. = ' + vqtybyweight + '<br>';
																		str5 = str5 + 'vtasksplitqty. = ' + vtasksplitqty + '<br>';	
																		str5 = str5 + 'itemuomqty. = ' + itemuomqty + '<br>';	
																		str5 = str5 + 'exptdqty. = ' + exptdqty + '<br>';	
																		str5 = str5 + 'remianingqty. = ' + remianingqty + '<br>';	

																		nlapiLogExecution('DEBUG', 'Size Determination in Used Cartons2', str5);

																		var taskCreated = 'F';
																		var totcartremcube = cartonremcube;
																		var totcartremwght = cartonremwght;


																		while((parseFloat(exptdqty)>0) && (parseFloat(remianingqty)>0) 
																				&& (Math.min(parseFloat(exptdqty),parseFloat(remianingqty))<=parseFloat(remianingqty))
																				//&& parseFloat(itemcube)<=parseFloat(cartonremcube)
																				//&& parseFloat(remcube)>=parseFloat(cartonremcube)  //Commented by Satish.N on 01SEP2014
																				&& (parseFloat(cartonremcube)>0) && (parseFloat(cartonremwght)>0)
																				&& (((Math.min(parseFloat(exptdqty),parseFloat(remianingqty))/parseFloat(itemuomqty))*parseFloat(itemcube))<=parseFloat(cartonremcube))
																				&& (((Math.min(parseFloat(exptdqty),parseFloat(remianingqty))/parseFloat(itemuomqty))*parseFloat(itemweight))<=parseFloat(cartonremwght))
																		)
																		{
																			var taskqty = Math.min(parseFloat(exptdqty),parseFloat(remianingqty));

																			var vsplittaskweight = (parseFloat(taskqty)*parseFloat(itemweight))/parseFloat(itemuomqty);
																			var vsplittaskcube = (parseFloat(taskqty)*parseFloat(itemcube))/parseFloat(itemuomqty);

																			taskCreated = 'T';
																			contsize =  containerslist[s].getValue('Internalid');
																			containerLP = usedCarton;																			
																			nlapiLogExecution('DEBUG', 'containerLP1 in buildcartonsforminandmaxdims used cartons', containerLP);
																			createNewPickTask(opentask,containerLP,vsplittaskcube,vsplittaskweight,contsize,taskqty);	
																			remweight = parseFloat(remweight) - parseFloat(vsplittaskweight);
																			remcube = parseFloat(remcube) - parseFloat(vsplittaskcube);										
																			remianingqty = parseFloat(remianingqty) - parseFloat(taskqty);
//																			var vcontainerremcube = parseFloat(cartonremcube) - parseFloat(vsplittaskcube);
//																			var vcontainerremweight = parseFloat(cartonremwght) - parseFloat(vsplittaskweight);

																			cartonremcube = parseFloat(cartonremcube) - parseFloat(vsplittaskcube);
																			cartonremwght = parseFloat(cartonremwght) - parseFloat(vsplittaskweight);

																			totcartremcube = totcartremcube-vsplittaskcube;
																			totcartremwght = totcartremwght-vsplittaskweight;

																			var str8 = 'task remweight. = ' + remweight + '<br>';
																			str8 = str8 + 'task remcube. = ' + remcube + '<br>';
																			str8 = str8 + 'remianingqty. = ' + remianingqty + '<br>';
																			str8 = str8 + 'containerremcube. = ' + cartonremcube + '<br>';	
																			str8 = str8 + 'containerremweight. = ' + cartonremwght + '<br>';

																			nlapiLogExecution('DEBUG', 'Log inside While', str8);
																			indxarray.push(containerLP);
																			//	optmizationarray.splice(t, 1);

																		}

																		if(taskCreated=='T')
																		{
																			var currow = [containerLP,contsize,totcartremcube,totcartremwght,vpickzoneno];
																			splitarray.push(currow);
																		}
																	}
																}
															}
														}
													}

													if(splitarray!=null && splitarray!='' && splitarray.length>0)
													{
														arrusedcartons = updateusedcartons(arrusedcartons,splitarray);
														//nlapiLogExecution('DEBUG', 'arrusedcartons : ', arrusedcartons);
														for(var v=0;v<splitarray.length;v++)
														{	
															arrusedcartons.push(splitarray[v]);
														} 
													}
												}

												nlapiLogExecution('DEBUG', 'Remaining Qty after processing Used Cartons : ', remianingqty);

												if(parseFloat(remianingqty) > 0)
												{
													for(l=(containerslist.length)-1;l>=0;l--){

														var containername = containerslist[l].getValue('custrecord_containername');
														var containerweight = containerslist[l].getValue('custrecord_maxweight');
														var containercube = containerslist[l].getValue('custrecord_cubecontainer');
														var packfactor = containerslist[l].getValue('custrecord_packfactor');
														var containerlength = containerslist[l].getValue('custrecord_length');
														var containerwidth = containerslist[l].getValue('custrecord_widthcontainer');
														var containerheight = containerslist[l].getValue('custrecord_heightcontainer');

														var containermaxdim = Math.max(parseFloat(containerlength), parseFloat(containerwidth), parseFloat(containerheight));
														var containermindim = Math.min(parseFloat(containerlength), parseFloat(containerwidth), parseFloat(containerheight));

														if(packfactor!=null && packfactor!='')
														{
															containercube = parseFloat(containercube)-(parseFloat(containercube)*(parseFloat(packfactor)/100));
														}

														var str = 'task remweight. = ' + remweight + '<br>';				
														str = str + 'task remcube. = ' + remcube + '<br>';
														str = str + 'containername. = ' + containername + '<br>';
														str = str + 'containerweight. = ' + containerweight + '<br>';	
														str = str + 'itemweight. = ' + itemweight + '<br>';
														str = str + 'containercube. = ' + containercube + '<br>';	
														str = str + 'itemcube. = ' + itemcube + '<br>';			
														str = str + 'itemlength. = ' + itemlength + '<br>';
														str = str + 'itemwidth. = ' + itemwidth + '<br>';
														str = str + 'itemheight. = ' + itemheight + '<br>';
														str = str + 'itemuomqty. = ' + itemuomqty + '<br>';														
														str = str + 'containerlength. = ' + containerlength + '<br>';
														str = str + 'containerwidth. = ' + containerwidth + '<br>';
														str = str + 'containerheight. = ' + containerheight + '<br>';

														nlapiLogExecution('DEBUG', 'Size Determination1', str);

														if((containername != 'SHIPASIS') 
																&& (parseFloat(itemcube)<=parseFloat(containercube)))
														{
															var str1 = 'itemmaxdim. = ' + itemmaxdim + '<br>';				
															str1 = str1 + 'itemmindim. = ' + itemmindim + '<br>';
															str1 = str1 + 'containermaxdim. = ' + containermaxdim + '<br>';
															str1 = str1 + 'containermindim. = ' + containermindim + '<br>';

															nlapiLogExecution('DEBUG', 'Size Determination2', str1);

															if(itemmaxdim<=containermaxdim && itemmindim<=containermindim)
															{
																var qtybycube = Math.floor(parseFloat(containercube)/parseFloat(itemcube));
																var qtybyweight = Math.floor(parseFloat(containerweight)/parseFloat(itemweight));
																var tasksplitqty = Math.min(parseFloat(qtybycube), parseFloat(qtybyweight));
																//exptdqty = Math.min(parseFloat(tasksplitqty), parseFloat(remianingqty));
																exptdqty = parseFloat(tasksplitqty) * parseFloat(itemuomqty);

																var str2 = 'qtybycube. = ' + qtybycube + '<br>';
																str2 = str2 + 'qtybyweight. = ' + qtybyweight + '<br>';
																str2 = str2 + 'tasksplitqty. = ' + tasksplitqty + '<br>';	
																str2 = str2 + 'itemuomqty. = ' + itemuomqty + '<br>';	
																str2 = str2 + 'exptdqty. = ' + exptdqty + '<br>';	
																str2 = str2 + 'remianingqty. = ' + remianingqty + '<br>';	

																nlapiLogExecution('DEBUG', 'Size Determination3', str2);


																while((parseFloat(exptdqty)>0) && (parseFloat(remianingqty)>0) 
																		&& (Math.min(parseFloat(exptdqty),parseFloat(remianingqty))<=parseFloat(remianingqty))
																		//&& parseFloat(itemcube)<=parseFloat(containercube)
																		//&& parseFloat(remcube)>=parseFloat(containercube) //This line is commented by Satish.N on 09JAN2014
																		&& (parseFloat(containercube)>0) && (parseFloat(containerweight)>0)
																		&& (((Math.min(parseFloat(exptdqty),parseFloat(remianingqty))/parseFloat(itemuomqty))*parseFloat(itemcube))<=parseFloat(containercube))
																)
																{
																	var taskqty = Math.min(parseFloat(exptdqty),parseFloat(remianingqty));

																	var splittaskweight = (parseFloat(taskqty)*parseFloat(itemweight))/parseFloat(itemuomqty);
																	var splittaskcube = (parseFloat(taskqty)*parseFloat(itemcube))/parseFloat(itemuomqty);

																	contsize =  containerslist[l].getValue('Internalid');
																	containerLP = GetMaxLPNo('1', '2',whsite);
																	cube = containerslist[l].getValue('custrecord_cubecontainer');
																	weight = containerslist[l].getValue('custrecord_maxweight');
																	CreateMasterLPRecord(containerLP,contsize,weight,cube,uompackflag,whsite,LPparent);
																	nlapiLogExecution('DEBUG', 'containerLP1 in buildcartonsforminandmaxdims ', containerLP);
																	createNewPickTask(opentask,containerLP,splittaskcube,splittaskweight,contsize,taskqty);		              
																	remweight = parseFloat(remweight) - parseFloat(splittaskweight);
																	remcube = parseFloat(remcube) - parseFloat(splittaskcube);
																	remianingqty = parseFloat(remianingqty) - parseFloat(taskqty);

																	//The below code is uncommented by Satish.N on 26-DEC-2013
																	var containerremcube = parseFloat(containercube) - parseFloat(splittaskcube);
																	var containerremweight = parseFloat(containerweight) - parseFloat(splittaskweight);
																	//Upto here

																	//The below code is commented by Satish.N on 26-DEC-2013
																	//containercube = parseFloat(containercube) - parseFloat(splittaskcube);
																	//containerweight = parseFloat(containerweight) - parseFloat(splittaskweight);
																	//Upto here


																	var str4 = 'task remweight. = ' + remweight + '<br>';
																	str4 = str4 + 'task remcube. = ' + remcube + '<br>';
																	str4 = str4 + 'remianingqty. = ' + remianingqty + '<br>';
																	str4 = str4 + 'containerremcube. = ' + containerremcube + '<br>';	
																	str4 = str4 + 'containerremweight. = ' + containerremweight + '<br>';

																	nlapiLogExecution('DEBUG', 'Log inside While', str4);

																	//The below code is changed by Satish.N on 26-DEC-2013
																	//var currow = [containerLP,contsize,containercube,containerweight];
																	var currow = [containerLP,contsize,containerremcube,containerremweight,vpickzoneno];
																	//Upto here
																	arrusedcartons.push(currow);
																}	
															}
														}
													} 
												}
												//if still there is some qty exists and the cube is less than the container(s) cube(no matching container),
												//then place it in the smallest container.
												nlapiLogExecution('DEBUG', 'remianingqty at end : ', remianingqty);	
												if(parseFloat(remianingqty)> 0){

													var q = containerslist.length;
													if (parseFloat(totaltaskqty)>parseFloat(remianingqty))
														contsize =  containerslist[q-1].getValue('Internalid');
													else
														contsize=getDefaultSize(whsite);
													containerLP = GetMaxLPNo('1', '2',whsite);
													CreateMasterLPRecord(containerLP,contsize,remweight,remcube,uompackflag,whsite,LPparent);
													nlapiLogExecution('DEBUG', 'containerLP2 in buildcartonsforminandmaxdims ', containerLP);
													createNewPickTask(opentask,containerLP,remcube,remweight,contsize,remianingqty);	             
													cube = containerslist[q-1].getValue('custrecord_cubecontainer');
													weight = containerslist[q-1].getValue('custrecord_maxweight');
													remianingqty = 0;
												}

												if(parseFloat(remianingqty)<=0)
												{
													//delete the actual record
													var id = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', opentask.getId());
													nlapiLogExecution('DEBUG', 'Deleted Actual Record : ', id);	
												}
											}
										}
										catch(exp) {
											nlapiLogExecution('DEBUG', 'Exception in buildcartonsforminandmaxdims : ', exp);		
										}
										nlapiLogExecution('DEBUG', 'out of  buildcartonsforminandmaxdims Task Split', '');
									}
								}
							}
						}

						if(arrusedcartons!=null && arrusedcartons!='' && arrusedcartons.length>0)
						{
							//nlapiLogExecution('DEBUG', 'arrusedcartons before sort : ', arrusedcartons);
							arrusedcartons.sort(function(a,b){return parseFloat(a[2]) - parseFloat(b[2]);});
							nlapiLogExecution('DEBUG', 'arrusedcartons after sort : ', arrusedcartons);
							optimizecartons(arrusedcartons,containerslist,allitemdimsarray,vwaveno,vordno);
						}
					}
				}
			}
		}
	}
}

function getDefaultSize(whsite)
{
	nlapiLogExecution('DEBUG', 'Into getDefaultSize', whsite);

	var defaultsize='';

	var containerslist = new Array();

	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebiz_container_defaultflag', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(whsite!=null && whsite!='')
		filters.push(new nlobjSearchFilter('custrecord_ebizsitecontainer', null, 'anyof', ['@NONE@',whsite]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('Internalid');
	columns[1] = new nlobjSearchColumn('custrecord_cubecontainer');
	columns[2] = new nlobjSearchColumn('custrecord_maxweight');	
	columns[3] = new nlobjSearchColumn('custrecord_tareweight');
	columns[4] = new nlobjSearchColumn('custrecord_containername');
	columns[5] = new nlobjSearchColumn('custrecord_packfactor');
	columns[6] = new nlobjSearchColumn('custrecord_length');
	columns[7] = new nlobjSearchColumn('custrecord_widthcontainer');
	columns[8] = new nlobjSearchColumn('custrecord_heightcontainer');

	columns[1].setSort(false);
	columns[2].setSort(false);

	containerslist = nlapiSearchRecord('customrecord_ebiznet_container', null, filters, columns);
	if(containerslist!=null && containerslist!='')
	{
		defaultsize = containerslist[0].getId();
	}

	nlapiLogExecution('DEBUG', 'out of getDefaultSize', defaultsize);

	return defaultsize;
}

function getPickTaskDetailsforSizeOptimization(vwaveno,vEbizOrdNo)
{
	var str = 'Ord No. = ' + vEbizOrdNo + '<br>';
	str = str + 'Wave No. = ' + vwaveno + '<br>';	

	nlapiLogExecution('DEBUG', 'Into getPickTaskDetailsforSizeOptimization', str);

	var opentasks = new Array();

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', vEbizOrdNo));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
	filters.push(new nlobjSearchFilter('custrecord_container', null, 'noneof', ['@NONE@']));
	filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'noneof', ['@NONE@']));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', vwaveno));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
	columns[1] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[2] = new nlobjSearchColumn('custrecord_total_weight');
	columns[3] = new nlobjSearchColumn('custrecord_totalcube');	
	columns[4] = new nlobjSearchColumn('name');
	columns[5] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[6] = new nlobjSearchColumn('custrecord_sku');
	columns[7] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[8] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
	columns[9] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[10] = new nlobjSearchColumn('custrecord_line_no');
	columns[11] = new nlobjSearchColumn('custrecord_lpno');
	columns[12] = new nlobjSearchColumn('custrecord_packcode');
	columns[13] = new nlobjSearchColumn('custrecord_sku_status');
	columns[14] = new nlobjSearchColumn('custrecord_wms_status_flag');
	columns[15] = new nlobjSearchColumn('custrecord_tasktype');	
	columns[16] = new nlobjSearchColumn('custrecord_uom_id');
	columns[17] = new nlobjSearchColumn('custrecord_uom_level');
	columns[18] = new nlobjSearchColumn('custrecord_ebiz_receipt_no');
	columns[19] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[20] = new nlobjSearchColumn('custrecord_invref_no');	
	columns[21] = new nlobjSearchColumn('custrecord_from_lp_no');
	columns[22] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[23] = new nlobjSearchColumn('custrecord_wms_location');
	columns[24] = new nlobjSearchColumn('custrecord_ebizrule_no');	
	columns[25] = new nlobjSearchColumn('custrecord_ebizmethod_no');
	columns[26] = new nlobjSearchColumn('custrecord_ebizzone_no');
	columns[27] = new nlobjSearchColumn('custrecord_ebizwmscarrier');

	columns[1].setSort();

	opentasks = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	

	nlapiLogExecution('DEBUG', 'out of getPickTaskDetailsforSizeOptimization', '');

	return opentasks;
}

function optimizecartons(arrusedcartons,containerslist,allitemdimsarray,vwaveno,vordno)
{
	nlapiLogExecution('DEBUG', 'Into optimizecartons');
	//nlapiLogExecution('DEBUG', 'allitemdimsarray',allitemdimsarray);

	var usedcartonscnt = arrusedcartons.length;

	var picktasklist = getPickTaskDetailsforSizeOptimization(vwaveno,vordno);
	if(picktasklist!=null && picktasklist!='' && picktasklist.length>0)
	{
		for(t=0;t<arrusedcartons.length;t++)
		{
			var maxitemdimarr = new Array();
			var minitemdimarr = new Array();
			var carton = arrusedcartons[t][0];
			var size = arrusedcartons[t][1];
			var gitemmaxdim=0;
			var gitemmindim=0;
			var totaltaskswght = 0;
			var totaltaskscube = 0;

			var str = 'carton. = ' + carton + '<br>';				
			str = str + 'size. = ' + size + '<br>';

			nlapiLogExecution('DEBUG', 'Used Cartons', str);

			for(s=0;s<picktasklist.length;s++)
			{
				var pickcarton = picktasklist[s].getValue('custrecord_container_lp_no');
				var pickitem = picktasklist[s].getValue('custrecord_ebiz_sku_no');

				nlapiLogExecution('DEBUG', 'pickcarton', pickcarton);

				if(carton==pickcarton)
				{
					var taskwght = picktasklist[s].getValue('custrecord_total_weight');
					if(taskwght==null || taskwght=='' || isNaN(taskwght))
						taskwght=0;

					var taskcube = picktasklist[s].getValue('custrecord_totalcube');
					if(taskcube==null || taskcube=='' || isNaN(taskcube))
						taskcube=0;

					totaltaskswght=parseFloat(totaltaskswght)+parseFloat(taskwght);
					totaltaskscube=parseFloat(totaltaskscube)+parseFloat(taskcube);
					if(allitemdimsarray!=null && allitemdimsarray!='')
					{
						for(k=0;k<allitemdimsarray.length;k++)
						{
							var dimsitem = allitemdimsarray[k].getValue('custrecord_ebizitemdims');
//							nlapiLogExecution('DEBUG', 'dimsitem', dimsitem);
//							nlapiLogExecution('DEBUG', 'pickitem', pickitem);

							if(dimsitem==pickitem)
							{
								var itemlength = allitemdimsarray[k].getValue('custrecord_ebizlength');
								var itemwidth = allitemdimsarray[k].getValue('custrecord_ebizwidth');
								var itemheight = allitemdimsarray[k].getValue('custrecord_ebizheight');

								var itemmaxdim = Math.max(parseFloat(itemlength), parseFloat(itemwidth), parseFloat(itemheight));
								var itemmindim = Math.min(parseFloat(itemlength), parseFloat(itemwidth), parseFloat(itemheight));

								maxitemdimarr.push(itemmaxdim);
								minitemdimarr.push(itemmindim);

								//nlapiLogExecution('DEBUG', 'maxitemdimarr', maxitemdimarr);
								maxitemdimarr.sort(function(a,b){return b - a;});
								//nlapiLogExecution('DEBUG', 'maxitemdimarr', maxitemdimarr);
								//nlapiLogExecution('DEBUG', 'minitemdimarr', minitemdimarr);
								minitemdimarr.sort(function(a,b){return a - b;});
								//nlapiLogExecution('DEBUG', 'minitemdimarr', minitemdimarr);
							}
						}
					}
				}
			}

			var contsize = getSizeforOptmization(containerslist,maxitemdimarr,minitemdimarr,totaltaskswght,totaltaskscube,
					pickitem,size);

			nlapiLogExecution('DEBUG', 'Size Id after optimization...',contsize);

			if(contsize!=null && contsize!='' && contsize!=size)
			{
				for(x=0;x<picktasklist.length;x++)
				{
					if(carton==picktasklist[x].getValue('custrecord_container_lp_no'))
					{
						nlapiLogExecution('DEBUG', 'Updating Size...',contsize);
						var fieldNames = new Array(); 
						fieldNames.push('custrecord_container');

						var newValues = new Array(); 
						newValues.push(contsize);

						nlapiSubmitField('customrecord_ebiznet_trn_opentask', picktasklist[x].getId(), fieldNames, newValues);
					}
				}

				// Update Carton
				updateCartons(carton,contsize,totaltaskswght,totaltaskscube);
			}			
		}
	}
	nlapiLogExecution('DEBUG', 'Out of optimizecartons');
}

function updateCartons(carton,contsize,totaltaskswght,totaltaskscube)
{
	nlapiLogExecution('DEBUG', 'Into updateCartons');

	var str = 'carton. = ' + carton + '<br>';				
	str = str + 'contsize. = ' + contsize + '<br>';
	str = str + 'totaltaskswght. = ' + totaltaskswght + '<br>';
	str = str + 'totaltaskscube. = ' + totaltaskscube + '<br>';	

	nlapiLogExecution('DEBUG', 'Parameters', str);

	var cartonFilters = new Array();
	var cartonColumns = new Array();

	cartonFilters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', carton));
	cartonFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	cartonColumns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
	cartonColumns[0].setSort();
	cartonColumns[1] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sizeid');
	cartonColumns[2] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
	cartonColumns[3] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totcube');

	var cartonResults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, cartonFilters, cartonColumns);
	if(cartonResults!=null && cartonResults!='' && cartonResults.length>0)
	{
		nlapiLogExecution('DEBUG', 'Updating LP Master...',contsize);

		var fieldNames = new Array(); 
		fieldNames.push('custrecord_ebiz_lpmaster_totwght');
		fieldNames.push('custrecord_ebiz_lpmaster_totcube');

		var newValues = new Array(); 
		newValues.push(parseFloat(totaltaskswght).toFixed(5));
		newValues.push(parseFloat(totaltaskscube).toFixed(5));

		if(contsize!=null && contsize!='')
		{
			fieldNames.push('custrecord_ebiz_lpmaster_sizeid');
			newValues.push(contsize);
		}

		nlapiSubmitField('customrecord_ebiznet_master_lp', cartonResults[0].getId(), fieldNames, newValues);
	}

	nlapiLogExecution('DEBUG', 'Out of updateCartons');
}

function getSizeforOptmization(containerslist,maxitemdimarr,minitemdimarr,totaltaskswght,totaltaskscube,pickitem,size)
{
	nlapiLogExecution('DEBUG', 'Into getSizeforOptmization');
	var contsize='';
	var gitemmaxdim=0;
	var gitemmindim=0;

	if (parseFloat(totaltaskswght) >0)
	{
		for(l=0;l<containerslist.length;l++)
		{ 
			var containerlength = containerslist[l].getValue('custrecord_length');
			var containerwidth = containerslist[l].getValue('custrecord_widthcontainer');
			var containerheight = containerslist[l].getValue('custrecord_heightcontainer');
			var containercube = containerslist[l].getValue('custrecord_cubecontainer');
			var containerweight= containerslist[l].getValue('custrecord_maxweight');
			var containername = containerslist[l].getValue('custrecord_containername');
			var packfactor=containerslist[l].getValue('custrecord_packfactor');
			if(packfactor==null || packfactor=='')
				packfactor=0;
			var containeractcube=parseFloat(containercube)-(parseFloat(containercube)*(parseFloat(packfactor)/100));

			var str = 'totaltaskscube. = ' + totaltaskscube + '<br>';				
			str = str + 'containeractcube. = ' + containeractcube + '<br>';
			str = str + 'containername. = ' + containername + '<br>';
			str = str + 'totaltaskswght. = ' + totaltaskswght + '<br>';	
			str = str + 'containerweight. = ' + containerweight + '<br>';	

			nlapiLogExecution('DEBUG', 'Size Determination1', str);

			if(containername!='SHIPASIS' && 
					parseFloat(containeractcube)>=parseFloat(totaltaskscube) && 
					parseFloat(containerweight)>=parseFloat(totaltaskswght))
			{

				var containermaxdim = Math.max(parseFloat(containerlength), parseFloat(containerwidth), parseFloat(containerheight));
				var containermindim = Math.min(parseFloat(containerlength), parseFloat(containerwidth), parseFloat(containerheight));

				for(a=0;maxitemdimarr!=null&&a<maxitemdimarr.length;a++)
				{
					gitemmaxdim = maxitemdimarr[0];
				}

				for(b=0;minitemdimarr!=null&&b<minitemdimarr.length;b++)
				{
					gitemmindim = minitemdimarr[0];
				}

				if(isNaN(gitemmaxdim))
					gitemmaxdim=0;

				if(isNaN(gitemmindim))
					gitemmindim=0;

				var str = 'containermaxdim. = ' + containermaxdim + '<br>';				
				str = str + 'containermindim. = ' + containermindim + '<br>';
				str = str + 'gitemmaxdim. = ' + gitemmaxdim + '<br>';
				str = str + 'gitemmindim. = ' + gitemmindim + '<br>';

				nlapiLogExecution('DEBUG', 'Size Determination2', str);

				if(parseFloat(containermaxdim)>=parseFloat(gitemmaxdim) &&
						parseFloat(containermindim)>=parseFloat(gitemmindim))
				{
					contsize=containerslist[l].getId();
					return contsize;
				}
			}
		}
	}

	nlapiLogExecution('DEBUG', 'Out of getSizeforOptmization',contsize);

	return contsize;
}

function updateusedcartons(arrusedcartons,sizearray)
{
	nlapiLogExecution('DEBUG', 'Into updateusedcartons');

	var newarray = new Array();

	for(x=0;arrusedcartons!=null && x<arrusedcartons.length;x++)
	{
		//nlapiLogExecution('DEBUG', 'X',x);
		var boolfound = 'F';
		for(y=0;sizearray!=null && y<sizearray.length;y++)
		{
			var str4 = 'arrusedcartons[x][0]. = ' + arrusedcartons[x][0] + '<br>';
			str4 = str4 + 'sizearray[y][0]. = ' + sizearray[y][0] + '<br>';

			//nlapiLogExecution('DEBUG', 'Array Comparison Log', str4);

			if(arrusedcartons[x][0] == sizearray[y][0] )
			{				
				boolfound='T';
				nlapiLogExecution('DEBUG', 'Match Found',boolfound);
			}
		}

		if(boolfound=='F')
		{
			//nlapiLogExecution('DEBUG', 'boolfound',boolfound);
			var currow = [arrusedcartons[x][0],arrusedcartons[x][1],arrusedcartons[x][2],arrusedcartons[x][3],arrusedcartons[x][4]];
			newarray.push(currow);
		}
	}

	nlapiLogExecution('DEBUG', 'Out of updateusedcartons',newarray);

	return newarray;
}

function buildcartonsforcube(opentasklist,containerslist,uompackflag)
{
	nlapiLogExecution('DEBUG', 'into buildcartonsforcube Task Split', '');
	try { 
		var totaltaskweight = parseFloat(opentasklist.getValue('custrecord_total_weight'));
		var totaltaskcube = parseFloat(opentasklist.getValue('custrecord_totalcube'));	
		var whsite=opentasklist.getValue('custrecord_wms_location');
		nlapiLogExecution('DEBUG', 'Total Task Weight', totaltaskweight);
		nlapiLogExecution('DEBUG', 'Total Task Cube', totaltaskcube);
		var remcube = parseFloat(totaltaskcube);
		var remweight = parseFloat(totaltaskweight);
		var contsize="";
		var containerLP="";
		var cube="";
		var weight="";
		var exptdqty=0;

		var uomlevel = opentasklist.getValue('custrecord_uom_level');
		var ebizskuno = opentasklist.getValue('custrecord_ebiz_sku_no');
		var uomcube = getUOMCube(uomlevel,ebizskuno);

		if(containerslist != null){ 
			for(l=(containerslist.length)-1;l>=0;l--){                                        
				var containercube = containerslist[l].getValue('custrecord_cubecontainer');
				var packfactor = containerslist[l].getValue('custrecord_packfactor');
				if(packfactor!=null && packfactor!='')
				{
					containercube = parseFloat(containercube)-(parseFloat(containercube)*(parseFloat(packfactor)/100));
				}
				nlapiLogExecution('DEBUG', 'Container Cube  ',containercube);
				nlapiLogExecution('DEBUG', 'SKU Uom Cube  ',uomcube);
				exptdqty = Math.floor(parseFloat(containercube)/parseFloat(uomcube));
				nlapiLogExecution('DEBUG', 'exptdqty  ',exptdqty);
				while (parseFloat(remcube)> 0 && parseFloat(remcube)>= parseFloat(containercube) && (exptdqty>0)){
					contsize =  containerslist[l].getValue('Internalid');
					containerLP = GetMaxLPNo('1', '2',whsite);
					cube = containerslist[l].getValue('custrecord_cubecontainer');
					weight = containerslist[l].getValue('custrecord_maxweight');
					CreateMasterLPRecord(containerLP,contsize,weight,cube,uompackflag,whsite);
					nlapiLogExecution('DEBUG', 'containerLP1 in buildcartonsforcube ', containerLP);
					createNewPickTask(opentasklist,containerLP,cube,weight,contsize,exptdqty);
					remcube = parseFloat(remcube) - parseFloat(cube);
					remweight = parseFloat(remweight) - parseFloat(weight);
					nlapiLogExecution('DEBUG', 'Task Remaining Cube', remcube);
				}
			}

			//if still there is some qty exists and the cube is less than the container(s) cube(no matching container),
			//then place it in the smallest container.
			if(parseFloat(remcube)> 0){

				nlapiLogExecution('DEBUG', 'Task Remaining Cube', remcube);

				exptdqty = Math.floor(parseFloat(remcube)/parseFloat(uomcube));

				nlapiLogExecution('DEBUG', 'exptdqty  ',exptdqty);

				if(parseFloat(exptdqty)>0)
				{
					var k = containerslist.length;
					contsize =  containerslist[k-1].getValue('Internalid');
					containerLP = GetMaxLPNo('1', '2',whsite);				   
					cube = containerslist[k-1].getValue('custrecord_cubecontainer');
					weight = containerslist[k-1].getValue('custrecord_maxweight');	
					CreateMasterLPRecord(containerLP,contsize,weight,cube,uompackflag,whsite);
					nlapiLogExecution('DEBUG', 'containerLP2 in buildcartonsforcube ', containerLP);
					createNewPickTask(opentasklist,containerLP,remcube,remweight,contsize,exptdqty);
				}
			}
			if(parseFloat(exptdqty)>0)
			{
				//delete the actual record
				var id = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', opentasklist.getId());
				nlapiLogExecution('DEBUG', 'Deleted Actual Record : ', id);	
			}
		}
	}
	catch(exp) {
		nlapiLogExecution('DEBUG', 'Exception in buildcartonsforcube : ', exp);		
	}

	nlapiLogExecution('DEBUG', 'out of  buildcartonsforcube Task Split', '');
}

function buildcartonsforweight(opentasklist,containerslist,uompackflag)
{
	nlapiLogExecution('DEBUG', 'into  buildcartonsforweight Task Split', '');
	try {
		var totaltaskweight = parseFloat(opentasklist.getValue('custrecord_total_weight'));
		var totaltaskcube = parseFloat(opentasklist.getValue('custrecord_totalcube'));	
		var whsite=opentasklist.getValue('custrecord_wms_location');
		var remweight = parseFloat(totaltaskweight);
		var remcube = parseFloat(totaltaskcube);
		var contsize="";
		var containerLP="";
		var cube="";
		var weight="";
		var exptdqty=0;

		var uomlevel = opentasklist.getValue('custrecord_uom_level');
		var ebizskuno = opentasklist.getValue('custrecord_ebiz_sku_no');
		var uomcube = getUOMCube(uomlevel,ebizskuno);

		if(containerslist != null){ 
			for(l=(containerslist.length)-1;l>=0;l--){                                      
				var containerweight = containerslist[l].getValue('custrecord_maxweight');
				var containercube = containerslist[l].getValue('custrecord_cubecontainer');
				nlapiLogExecution('DEBUG', 'Container Cube  ',containercube);
				nlapiLogExecution('DEBUG', 'SKU UOM Cube  ',uomcube);

				exptdqty = Math.floor(parseFloat(containercube)/parseFloat(uomcube));

				nlapiLogExecution('DEBUG', 'exptdqty  ',exptdqty);
				while (parseFloat(remweight)> 0 && parseFloat(remweight)>= parseFloat(containerweight) 
						&& parseFloat(exptdqty)>0){
					contsize =  containerslist[l].getValue('Internalid');
					containerLP = GetMaxLPNo('1', '2',whsite);		
					cube = containerslist[l].getValue('custrecord_cubecontainer');
					weight = containerslist[l].getValue('custrecord_maxweight');	
					CreateMasterLPRecord(containerLP,contsize,weight,cube,uompackflag,whsite);
					nlapiLogExecution('DEBUG', 'containerLP1 in buildcartonsforweight ', containerLP);
					createNewPickTask(opentasklist,containerLP,cube,weight,contsize,exptdqty);
					remweight = parseFloat(remweight) - parseFloat(weight);
					remcube = parseFloat(remcube) - parseFloat(cube);
				}
			} 

			//if still there is some qty exists and the cube is less than the container(s) cube(no matching container),
			//then place it in the smallest container.
			if(parseFloat(remweight)> 0){

				nlapiLogExecution('DEBUG', 'remweight  ',remweight);
				nlapiLogExecution('DEBUG', 'remcube  ',remcube);

				exptdqty = Math.floor(parseFloat(remcube)/parseFloat(uomcube));

				nlapiLogExecution('DEBUG', 'exptdqty  ',exptdqty);

				if(parseFloat(exptdqty)>0)
				{
					var k = containerslist.length;
					contsize =  containerslist[k-1].getValue('Internalid');
					containerLP = GetMaxLPNo('1', '2',whsite);
					CreateMasterLPRecord(containerLP,contsize,remweight,remcube,uompackflag,whsite);
					nlapiLogExecution('DEBUG', 'containerLP2 in buildcartonsforweight ', containerLP);
					createNewPickTask(opentasklist,containerLP,remcube,remweight,contsize,exptdqty);
					cube = containerslist[k-1].getValue('custrecord_cubecontainer');
					weight = containerslist[k-1].getValue('custrecord_maxweight');
				}
			}
			if(parseFloat(exptdqty)>0)
			{
				//delete the actual record
				var id = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', opentasklist.getId());
				nlapiLogExecution('DEBUG', 'Deleted Actual Record : ', id);	
			}
		}
	}
	catch(exp) {
		nlapiLogExecution('DEBUG', 'Exception in buildcartonsforweight : ', exp);		
	}

	nlapiLogExecution('DEBUG', 'out of  buildcartonsforweight Task Split', '');
}

function buildcartonsforcubeandweight(opentasklist,containerslist,uompackflag)
{
	nlapiLogExecution('DEBUG', 'into  buildcartonsforcubeandweight Task Split', '');
	try{
		var totaltaskweight = parseFloat(opentasklist.getValue('custrecord_total_weight'));
		var totaltaskcube = parseFloat(opentasklist.getValue('custrecord_totalcube'));	
		var remweight = parseFloat(totaltaskweight);
		var remcube = parseFloat(totaltaskcube);
		var contsize="";
		var containerLP="";
		var cube="";
		var weight="";
		var exptdqty =0;
		var whsite=opentasklist.getValue('custrecord_wms_location');

		var uomlevel = opentasklist.getValue('custrecord_uom_level');
		var ebizskuno = opentasklist.getValue('custrecord_ebiz_sku_no');
		var uomcube = getUOMCube(uomlevel,ebizskuno);
		if(containerslist != null){ 
			for(l=(containerslist.length)-1;l>=0;l--){
				var containerweight = containerslist[l].getValue('custrecord_maxweight');
				var containercube = containerslist[l].getValue('custrecord_cubecontainer');
				var packfactor = containerslist[l].getValue('custrecord_packfactor');
				if(packfactor!=null && packfactor!='')
				{
					containercube = parseFloat(containercube)-(parseFloat(containercube)*(parseFloat(packfactor)/100));
				}

				nlapiLogExecution('DEBUG', 'containercube', containercube);
				nlapiLogExecution('DEBUG', 'uomcube', uomcube);
				exptdqty = Math.floor(parseFloat(containercube)/parseFloat(uomcube));
				nlapiLogExecution('DEBUG', 'exptdqty  ',exptdqty);
				nlapiLogExecution('DEBUG', 'remweight', remweight);
				nlapiLogExecution('DEBUG', 'remcube', remcube);

				while ((parseFloat(remweight)> 0) && (parseFloat(remweight)>= parseFloat(containerweight)) && 
						(parseFloat(remcube)> 0) && (parseFloat(remcube)>= parseFloat(containercube)) &&
						(parseInt(exptdqty)>0)){
					contsize =  containerslist[l].getValue('Internalid');
					containerLP = GetMaxLPNo('1', '2',whsite);
					cube = containerslist[l].getValue('custrecord_cubecontainer');
					weight = containerslist[l].getValue('custrecord_maxweight');
					CreateMasterLPRecord(containerLP,contsize,weight,cube,uompackflag,whsite);
					nlapiLogExecution('DEBUG', 'containerLP1 in buildcartonsforcubeandweight ', containerLP);
					createNewPickTask(opentasklist,containerLP,cube,weight,contsize,exptdqty);		              
					remweight = parseFloat(remweight) - parseFloat(weight);
					remcube = parseFloat(remcube) - parseFloat(cube);
				}
			} 

			nlapiLogExecution('DEBUG', 'remweight', remweight);
			nlapiLogExecution('DEBUG', 'remcube', remcube);
			nlapiLogExecution('DEBUG', 'uomcube', uomcube);
			//if still there is some qty exists and the cube is less than the container(s) cube(no matching container),
			//then place it in the smallest container.
			if(parseFloat(remweight)> 0 || parseFloat(remcube)> 0){

				exptdqty = Math.floor(parseFloat(remcube)/parseFloat(uomcube));

				nlapiLogExecution('DEBUG', 'exptdqty  ',exptdqty);

				if(parseInt(exptdqty)>0)
				{
					var k = containerslist.length;
					contsize =  containerslist[k-1].getValue('Internalid');
					containerLP = GetMaxLPNo('1', '2',whsite);
					CreateMasterLPRecord(containerLP,contsize,remweight,remcube,uompackflag,whsite);
					nlapiLogExecution('DEBUG', 'containerLP2 in buildcartonsforcubeandweight ', containerLP);
					createNewPickTask(opentasklist,containerLP,remcube,remweight,contsize,exptdqty);	             
					cube = containerslist[k-1].getValue('custrecord_cubecontainer');
					weight = containerslist[k-1].getValue('custrecord_maxweight');
				}
			}
			if(parseInt(exptdqty)>0)
			{
				//delete the actual record
				var id = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', opentasklist.getId());
				nlapiLogExecution('DEBUG', 'Deleted Actual Record : ', id);	
			}
		}
	}
	catch(exp) {
		nlapiLogExecution('DEBUG', 'Exception in buildcartonsforcubeandweight : ', exp);		
	}
	nlapiLogExecution('DEBUG', 'out of  buildcartonsforcubeandweight Task Split', '');
}

/**
 * 
 * @param oldTaskList
 * [0]  - containerlpno, [1]  - totalweight, [2]  - totalcube,  [3]  - fforderno,    [4]  - fforderinternalno, [5]  - sku,       [6]  - ebizskuno
 * [7]  - ebizwaveno,    [8]  - expqty,      [9]  - linenumber, [10] - lpno,         [11] - packcode ,         [12] - skustatus, [13] - statusflag   
 * [14] - tasktype,      [15] - uom,         [16] - uomlevel,   [17] - ebizreceiptno,[18] - beginlocation,     [19] - invrefno,  [20] - fromlpno
 * [21] - ebizordno,     [22] - wmslocation, [23] - ebizruleno, [24] - ebizmethodno, [25] - pickzone
 */
function createNewPickTask(oldTaskList,containerLP,cube,weight,containersize,exptdqty){

	nlapiLogExecution('DEBUG', 'into createNewPickTask - exptdqty ',exptdqty);	

	if(oldTaskList!=null){

		if(exptdqty>0)
		{

			var openTaskRecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');

			openTaskRecord.setFieldValue('name', oldTaskList.getValue('name'));							// DELIVERY ORDER NAME
			openTaskRecord.setFieldValue('custrecord_ebiz_sku_no', oldTaskList.getValue('custrecord_ebiz_sku_no'));			// ITEM INTERNAL ID
			openTaskRecord.setFieldValue('custrecord_sku', oldTaskList.getValue('custrecord_sku'));					// ITEM NAME
			openTaskRecord.setFieldValue('custrecord_expe_qty', exptdqty);			// FULFILMENT ORDER QUANTITY
			openTaskRecord.setFieldValue('custrecord_container_lp_no', containerLP);
			openTaskRecord.setFieldValue('custrecord_ebiz_lpno', containerLP);
			openTaskRecord.setFieldValue('custrecord_line_no', oldTaskList.getValue('custrecord_line_no'));
			openTaskRecord.setFieldValue('custrecord_tasktype', '3'); 						// TASK TYPE = PICK

			openTaskRecord.setFieldValue('custrecord_actbeginloc', oldTaskList.getValue('custrecord_actbeginloc'));
			openTaskRecord.setFieldValue('custrecord_ebiz_cntrl_no', oldTaskList.getValue('custrecord_ebiz_cntrl_no'));
			openTaskRecord.setFieldValue('custrecord_ebiz_order_no', oldTaskList.getValue('custrecord_ebiz_order_no'));
			openTaskRecord.setFieldValue('custrecord_ebiz_receipt_no', oldTaskList.getValue('custrecord_ebiz_receipt_no'));
			openTaskRecord.setFieldValue('custrecord_ebiz_wave_no',  oldTaskList.getValue('custrecord_ebiz_wave_no'));
			openTaskRecord.setFieldValue('custrecordact_begin_date',DateStamp());
			openTaskRecord.setFieldValue('custrecord_wms_status_flag', '9'); 				// STATUS FLAG = G

			openTaskRecord.setFieldValue('custrecord_from_lp_no', oldTaskList.getValue('custrecord_from_lp_no'));
			openTaskRecord.setFieldValue('custrecord_lpno', oldTaskList.getValue('custrecord_from_lp_no'));
			openTaskRecord.setFieldValue('custrecord_actualbegintime', TimeStamp());
			openTaskRecord.setFieldValue('custrecord_invref_no', oldTaskList.getValue('custrecord_invref_no'));
			openTaskRecord.setFieldValue('custrecord_packcode', oldTaskList.getValue('custrecord_packcode'));
			openTaskRecord.setFieldValue('custrecord_sku_status', oldTaskList.getValue('custrecord_sku_status'));
			openTaskRecord.setFieldValue('custrecord_uom_id', oldTaskList.getValue('custrecord_uom_id'));
			openTaskRecord.setFieldValue('custrecord_ebizrule_no', oldTaskList.getValue('custrecord_ebizrule_no'));
			openTaskRecord.setFieldValue('custrecord_ebizmethod_no', oldTaskList.getValue('custrecord_ebizmethod_no'));
			openTaskRecord.setFieldValue('custrecord_ebizzone_no', oldTaskList.getValue('custrecord_ebizzone_no'));
			//Case # 20126064� Start
			if(oldTaskList.getValue('custrecord_ebizzone_no')!=null && oldTaskList.getValue('custrecord_ebizzone_no')!='')
			{
				openTaskRecord.setFieldValue('custrecord_ebiz_zoneid', oldTaskList.getValue('custrecord_ebizzone_no'));
			}
			//Case # 20126064� End
			openTaskRecord.setFieldValue('custrecord_wms_location', oldTaskList.getValue('custrecord_wms_location'));						
			var currentContext = nlapiGetContext();  
			var currentUserID = currentContext.getUser();	
			if(currentUserID>0)
			{
				openTaskRecord.setFieldValue('custrecord_ebizuser', currentUserID);		
				openTaskRecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);		
			}
			//openTaskRecord.setFieldValue('custrecord_taskassignedto', currentUserID); Commented by Sudheer on 1025
			openTaskRecord.setFieldValue('custrecord_uom_level', oldTaskList.getValue('custrecord_uom_level'));
			openTaskRecord.setFieldValue('custrecord_total_weight', weight);
			openTaskRecord.setFieldValue('custrecord_totalcube', cube);
			if(containersize!=null && containersize!='')
				openTaskRecord.setFieldValue('custrecord_container', containersize);
			openTaskRecord.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', oldTaskList.getValue('custrecord_ebiz_nsconfirm_ref_no'));

			openTaskRecord.setFieldValue('custrecord_skiptask', '0');

			openTaskRecord.setFieldValue('custrecord_batch_no', oldTaskList.getValue('custrecord_batch_no')); // LOT #
			nlapiLogExecution('DEBUG', 'custrecord_expirydate',oldTaskList.getValue('custrecord_expirydate'));
			if(oldTaskList.getValue('custrecord_expirydate') != null && oldTaskList.getValue('custrecord_expirydate') != '')
				openTaskRecord.setFieldValue('custrecord_expirydate', oldTaskList.getValue('custrecord_expirydate')); // expiry date #

			/***The below code is merged from Factory mation production account by Ganesh K on March 1st 2013 ***/
			if(oldTaskList.getValue('custrecord_ebizwmscarrier') != null && oldTaskList.getValue('custrecord_ebizwmscarrier') != '')
			{
				nlapiLogExecution('DEBUG', 'Old WMS Carrier ',oldTaskList.getValue('custrecord_ebizwmscarrier'));
				openTaskRecord.setFieldValue('custrecord_ebizwmscarrier', oldTaskList.getValue('custrecord_ebizwmscarrier'));
			}
			if(oldTaskList.getValue('custrecord_parent_sku_no') != null && oldTaskList.getValue('custrecord_parent_sku_no') != '')
			{
				nlapiLogExecution('DEBUG', 'Old Parent SKU ',oldTaskList.getValue('custrecord_parent_sku_no'));
				openTaskRecord.setFieldValue('custrecord_parent_sku_no', oldTaskList.getValue('custrecord_parent_sku_no'));
			}
			/*** Upto here ***/	

			//Code Added on 24th Feb 2014 By Ganapathi
			if(oldTaskList.getValue('custrecord_ebiz_act_solocation')!=null && oldTaskList.getValue('custrecord_ebiz_act_solocation')!='')
			{
				openTaskRecord.setFieldValue('custrecord_ebiz_act_solocation', oldTaskList.getValue('custrecord_ebiz_act_solocation'));
			}
			//end

			nlapiSubmitRecord(openTaskRecord,false,true);
			nlapiLogExecution('DEBUG', 'Opentask updation Completed');
		}
	}
	nlapiLogExecution('DEBUG', 'out of createNewPickTask ','');
}

function updatesizeidinopentask(recordId, containerLP,sizeid){
	nlapiLogExecution('DEBUG', 'In to updatesizeidinopentask function : sizeid',sizeid);

	var fieldNames = new Array(); 
	fieldNames.push('custrecord_container');  
	fieldNames.push('custrecord_container_lp_no');

	var newValues = new Array(); 
	newValues.push(sizeid);
	newValues.push(containerLP);

	nlapiSubmitField('customrecord_ebiznet_trn_opentask', recordId, fieldNames, newValues);

	nlapiLogExecution('DEBUG', 'Out of updatesizeidinopentask function');
}

function getContainerSize(containerslist,totaltaskcube,totaltaskweight,cartonizationmethod)
{	
	var str = 'Container List. = ' + containerslist + '<br>';
	str = str + 'totaltaskcube. = ' + totaltaskcube + '<br>';	
	str = str + 'totaltaskweight. = ' + totaltaskweight + '<br>';
	str = str + 'cartonizationmethod. = ' + cartonizationmethod + '<br>';

	nlapiLogExecution('DEBUG', 'Into getContainerSize', str);

	var contsize='';

	for(l=0;l<containerslist.length;l++)
	{      
		var containercube = containerslist[l].getValue('custrecord_cubecontainer');
		var containerweight= containerslist[l].getValue('custrecord_maxweight');
		var containername = containerslist[l].getValue('custrecord_containername');
		var packfactor=containerslist[l].getValue('custrecord_packfactor');
		if(packfactor==null || packfactor=='')
			packfactor=0;
		var actcube=parseFloat(containercube)-(parseFloat(containercube)*(parseFloat(packfactor)/100));
		nlapiLogExecution('DEBUG', 'actcube',actcube);

		if(cartonizationmethod=='1')
		{
			if((containername!='SHIPASIS') && (parseFloat(actcube)>=parseFloat(totaltaskcube)) 
					&& (parseFloat(containerweight)>=parseFloat(totaltaskweight)))
			{
				contsize=containerslist[l].getValue('Internalid');
				nlapiLogExecution('DEBUG', 'contsize',contsize);	
				return contsize;
			}
		}
		else if(cartonizationmethod=='2')
		{
			if((containername!='SHIPASIS') && (parseFloat(containerweight)>=parseFloat(totaltaskweight)))
			{
				contsize=containerslist[l].getValue('Internalid');
				nlapiLogExecution('DEBUG', 'contsize',contsize);
				return contsize;
			}
		}
		else if(cartonizationmethod=='3')
		{
			if((containername!='SHIPASIS') && (parseFloat(actcube)>=parseFloat(totaltaskcube)))
			{
				contsize=containerslist[l].getValue('Internalid');
				nlapiLogExecution('DEBUG', 'contsize',contsize);	
				return contsize;
			}
		}

	}

	nlapiLogExecution('DEBUG', 'Out of getContainerSize (return Size) ',contsize);

	return contsize;
}

function getTaskDetailsforCartonization(vEbizOrdNo,vEbizMethodNo)
{
	var str = 'Ord No. = ' + vEbizOrdNo + '<br>';
	str = str + 'Method No. = ' + vEbizMethodNo + '<br>';	

	nlapiLogExecution('DEBUG', 'Into getTaskDetailsforCartonization', str);

	var opentasks = new Array();

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', vEbizOrdNo));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
	filters.push(new nlobjSearchFilter('custrecord_container', null, 'anyof', ['@NONE@']));
	filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'noneof', ['@NONE@']));
	filters.push(new nlobjSearchFilter('custrecord_ebizmethod_no', null, 'is', vEbizMethodNo));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[1] = new nlobjSearchColumn('custrecord_total_weight');
	columns[2] = new nlobjSearchColumn('custrecord_totalcube');	
	columns[3] = new nlobjSearchColumn('name');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[5] = new nlobjSearchColumn('custrecord_sku');
	columns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[7] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
	columns[8] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[9] = new nlobjSearchColumn('custrecord_line_no');
	columns[10] = new nlobjSearchColumn('custrecord_lpno');
	columns[11] = new nlobjSearchColumn('custrecord_packcode');
	columns[12] = new nlobjSearchColumn('custrecord_sku_status');
	columns[13] = new nlobjSearchColumn('custrecord_wms_status_flag');
	columns[14] = new nlobjSearchColumn('custrecord_tasktype');	
	columns[15] = new nlobjSearchColumn('custrecord_uom_id');
	columns[16] = new nlobjSearchColumn('custrecord_uom_level');
	columns[17] = new nlobjSearchColumn('custrecord_ebiz_receipt_no');
	columns[18] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[19] = new nlobjSearchColumn('custrecord_invref_no');	
	columns[20] = new nlobjSearchColumn('custrecord_from_lp_no');
	columns[21] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[22] = new nlobjSearchColumn('custrecord_wms_location');
	columns[23] = new nlobjSearchColumn('custrecord_ebizrule_no');	
	columns[24] = new nlobjSearchColumn('custrecord_ebizmethod_no');
	columns[25] = new nlobjSearchColumn('custrecord_ebizzone_no');

	opentasks = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	

	nlapiLogExecution('DEBUG', 'out of getTaskDetailsforCartonization', '');

	return opentasks;
}

function getPickTaskDetailsforCartonization(vwaveno,vEbizOrdNo)
{
	var str = 'Ord No. = ' + vEbizOrdNo + '<br>';
	str = str + 'Wave No. = ' + vwaveno + '<br>';	

	nlapiLogExecution('DEBUG', 'Into getPickTaskDetailsforCartonization', str);

	var opentasks = new Array();

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', vEbizOrdNo));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
	filters.push(new nlobjSearchFilter('custrecord_container', null, 'anyof', ['@NONE@']));
	filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'noneof', ['@NONE@']));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'contains', vwaveno));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[1] = new nlobjSearchColumn('custrecord_total_weight');
	columns[2] = new nlobjSearchColumn('custrecord_totalcube');	
	columns[3] = new nlobjSearchColumn('name');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[5] = new nlobjSearchColumn('custrecord_sku');
	columns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[7] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
	columns[8] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[9] = new nlobjSearchColumn('custrecord_line_no');
	columns[10] = new nlobjSearchColumn('custrecord_lpno');
	columns[11] = new nlobjSearchColumn('custrecord_packcode');
	columns[12] = new nlobjSearchColumn('custrecord_sku_status');
	columns[13] = new nlobjSearchColumn('custrecord_wms_status_flag');
	columns[14] = new nlobjSearchColumn('custrecord_tasktype');	
	columns[15] = new nlobjSearchColumn('custrecord_uom_id');
	columns[16] = new nlobjSearchColumn('custrecord_uom_level');
	columns[17] = new nlobjSearchColumn('custrecord_ebiz_receipt_no');
	columns[18] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[19] = new nlobjSearchColumn('custrecord_invref_no');	
	columns[20] = new nlobjSearchColumn('custrecord_from_lp_no');
	columns[21] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[22] = new nlobjSearchColumn('custrecord_wms_location');
	columns[23] = new nlobjSearchColumn('custrecord_ebizrule_no');	
	columns[24] = new nlobjSearchColumn('custrecord_ebizmethod_no');
	columns[25] = new nlobjSearchColumn('custrecord_ebizzone_no');
	columns[26] = new nlobjSearchColumn('custrecord_ebizwmscarrier');
	columns[27] = new nlobjSearchColumn('custrecord_ebiz_act_solocation');
	columns[28] = new nlobjSearchColumn('custrecord_parent_sku_no');
	columns[21].setSort();
	columns[24].setSort();
	columns[25].setSort();
	columns[26].setSort();
	opentasks = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	

	nlapiLogExecution('DEBUG', 'out of getPickTaskDetailsforCartonization', '');

	return opentasks;
}

function getCubeanddWeightbyMethod(vwaveno,vEbizOrdNo,vmethodno)
{
	var str = 'Ord No. = ' + vEbizOrdNo + '<br>';
	str = str + 'Wave No. = ' + vwaveno + '<br>';	
	str = str + 'Method No. = ' + vmethodno + '<br>';	

	nlapiLogExecution('DEBUG', 'Into getCubeanddWeightbyMethod', str);

	var opentasks = new Array();

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', vEbizOrdNo));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
	filters.push(new nlobjSearchFilter('custrecord_container', null, 'anyof', ['@NONE@']));
	filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'noneof', ['@NONE@']));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', vwaveno));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));
	filters.push(new nlobjSearchFilter('custrecord_ebizmethod_no', null, 'is', vmethodno));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_total_weight',null,'sum');
	columns[1] = new nlobjSearchColumn('custrecord_totalcube',null,'sum');	
	columns[2] = new nlobjSearchColumn('custrecord_ebizmethod_no',null,'group');

	opentasks = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	

	nlapiLogExecution('DEBUG', 'out of getCubeanddWeightbyMethod', '');

	return opentasks;
}


function getSKUList(fulfillOrderList){
	var skuList = new Array();

	if(fulfillOrderList != null && fulfillOrderList.length > 0){
		for(var i = 0; i < fulfillOrderList.length; i++){
			var currSKU = fulfillOrderList[i][6];
			var orderQty = fulfillOrderList[i][9];
			var currSKUtext = fulfillOrderList[i][5];
			var itemType = nlapiLookupField('item', currSKU, 'recordType');
			nlapiLogExecution('DEBUG', 'itemType', itemType);
			nlapiLogExecution('DEBUG', 'currSKU', currSKU);
			if(itemType=='kititem' || itemType=='itemgroup')
			{
				var searchresultsitem = nlapiLoadRecord(itemType, currSKU);
				var SkuNo=searchresultsitem.getFieldValue('itemid');
				var recCount=0; 
				var filters = new Array(); 
				//filters[0] = new nlobjSearchFilter('itemid', null, 'is', request.getParameter("custparam_item"));//kit_id is the parameter name 
				filters[0] = new nlobjSearchFilter('itemid', null, 'is', SkuNo);//kit_id is the parameter name 
				nlapiLogExecution('DEBUG', 'SkuNo', SkuNo);
				//	filters[1] = new nlobjSearchFilter('type', null, 'anyof', 'kititem');//For kit/package type Items 
				var columns1 = new Array(); 
				columns1[0] = new nlobjSearchColumn( 'memberitem' ); 
				//  columns1[0] = new nlobjSearchColumn( 'internalid' ); 
				columns1[1] = new nlobjSearchColumn( 'memberquantity' );

				var searchresults = nlapiSearchRecord( 'item', null, filters, columns1 ); 			
				for(var i=0; i<searchresults.length;i++) 
				{ 
					currSKU = searchresults[i].getValue('memberitem');
					orderQty = searchresults[i].getValue('memberquantity');
					nlapiLogExecution('DEBUG','strItem',currSKU);
					nlapiLogExecution('DEBUG','strQty',orderQty);
					updateSKUList(skuList, currSKU, orderQty);
				}
			}
			else
			{
				updateSKUList(skuList, currSKU, orderQty);
			}


		}
	}

	return skuList;
}

function updateSKUList(skuList, sku, orderQty){
	var skuFound = false;

	// Search for the SKU in the SKU list and add the order quantity if the SKU already exists
	for(var i = 0; i < skuList.length; i++){
		if(parseInt(skuList[i][0]) == parseInt(sku)){
			skuList[i][1] = parseInt(skuList[i][1]) + parseInt(orderQty);
			skuFound = true;
		}
	}

	// Adding a new item to the list if the SKU is not found in this list
	if(!skuFound){
		skuList[skuList.length] = [sku, orderQty];
	}
}

function getAllLocnGroups(allocConfigDtls){
	nlapiLogExecution('DEBUG', 'into getAllLocnGroups : allocConfigDtls  ', allocConfigDtls);
	var locnGroupListForAll = new Array();

	if(allocConfigDtls != null && allocConfigDtls.length > 0){
		for(var i = 0; i < allocConfigDtls.length; i++){
			var tempLocnGroupList = allocConfigDtls[i][8];
			if(tempLocnGroupList != null && tempLocnGroupList != '')
				locnGroupListForAll.push(tempLocnGroupList);
		}
	}

	nlapiLogExecution('DEBUG', 'out of getAllLocnGroups : locnGroupListForAll  ', locnGroupListForAll);

	return locnGroupListForAll;
}

function getItemList(skuList){
	var itemList = new Array();

	if (skuList != null && skuList.length > 0){
		for (var i = 0; i < skuList.length; i++){
			itemList.push(skuList[i][0]);
			nlapiLogExecution('DEBUG', 'Item', skuList[i][0]);
		}
	} 

	return itemList;
}

var itemDimensionList = new Array();
function getItemDimensions(skuList,maxno){
	nlapiLogExecution('Debug','into  getItemDimensions - # of items', skuList.length);
	nlapiLogExecution('Debug','into  getItemDimensions - maxno', maxno);
	var itemDimCount;
	if(maxno==-1)
		itemDimCount = 0;
	else
		itemDimCount=itemDimensionList.length;

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', skuList));		// ALL SKUs IN SKU LIST
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));							// ACTIVE RECORDS ONLY
	filters.push(new nlobjSearchFilter('custrecord_ebiz_notallowedforpicking', null, 'is', 'F'));
	if(maxno!=-1)
	{
		filters.push(new nlobjSearchFilter('id', null, 'lessthan', parseInt(maxno)));
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');								// ITEM/SKU
	columns[1] = new nlobjSearchColumn('custrecord_ebizuomskudim');								// UOM
	columns[2] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim');						// UOM LEVEL
	columns[3] = new nlobjSearchColumn('custrecord_ebizqty');									// QUANTITY
	columns[4] = new nlobjSearchColumn('custrecord_ebizbaseuom');								// BASE UOM FLAG
	columns[5] = new nlobjSearchColumn('custrecord_ebizdims_packflag');							// PACK FLAG	
	columns[6] = new nlobjSearchColumn('custrecord_ebizweight');								// WEIGHT	
	columns[7] = new nlobjSearchColumn('custrecord_ebizcube');									// CUBE	
	columns[8] = new nlobjSearchColumn('internalid');
	columns[9] = new nlobjSearchColumn('custrecord_ebiz_dims_bulkpick');
	columns[8].setSort(true);

	// SEARCH FROM ITEM DIMENSIONS
	var searchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

	if(searchResults != null && searchResults != '' && searchResults.length >= 1000){
		var maxno1=searchResults[searchResults.length-1].getValue(columns[8]);
		for(var i = 0; i < searchResults.length; i++){
			var sku = searchResults[i].getValue('custrecord_ebizitemdims');
			var uom = searchResults[i].getValue('custrecord_ebizuomskudim');
			var uomLevel = searchResults[i].getValue('custrecord_ebizuomlevelskudim');
			var qty = searchResults[i].getValue('custrecord_ebizqty');
			var baseUOMFlag = searchResults[i].getValue('custrecord_ebizbaseuom');
			var packflag = searchResults[i].getValue('custrecord_ebizdims_packflag');
			var weight = searchResults[i].getValue('custrecord_ebizweight');
			var cube = searchResults[i].getValue('custrecord_ebizcube');
			var vbulkpick = searchResults[i].getValue('custrecord_ebiz_dims_bulkpick');

			var skuDimRecord = [sku, uom, uomLevel, qty, baseUOMFlag, packflag,weight,cube,vbulkpick];
			itemDimensionList.push(skuDimRecord);
		}

		getItemDimensions(skuList,maxno1);
	}
	else if(searchResults != null && searchResults != '')
	{
		for(var i = 0; i < searchResults.length; i++){
			var sku = searchResults[i].getValue('custrecord_ebizitemdims');
			var uom = searchResults[i].getValue('custrecord_ebizuomskudim');
			var uomLevel = searchResults[i].getValue('custrecord_ebizuomlevelskudim');
			var qty = searchResults[i].getValue('custrecord_ebizqty');
			var baseUOMFlag = searchResults[i].getValue('custrecord_ebizbaseuom');
			var packflag = searchResults[i].getValue('custrecord_ebizdims_packflag');
			var weight = searchResults[i].getValue('custrecord_ebizweight');
			var cube = searchResults[i].getValue('custrecord_ebizcube');
			var vbulkpick = searchResults[i].getValue('custrecord_ebiz_dims_bulkpick');

			var skuDimRecord = [sku, uom, uomLevel, qty, baseUOMFlag, packflag,weight,cube,vbulkpick];
			itemDimensionList.push(skuDimRecord);
		}

	}
	nlapiLogExecution('Debug','out of  getItemDimensions', skuList.length);
	return itemDimensionList;
}

var kititemDimensionList = new Array();
function getKitItemDimensions(skuList,maxno,DOwhLocation){
	nlapiLogExecution('Debug','into  getKitItemDimensions - # of items', skuList.length);
	nlapiLogExecution('Debug','into  getKitItemDimensions - maxno', maxno);
	var itemDimCount ;
	if(maxno==-1)
		itemDimCount = 0;
	else
		itemDimCount=kititemDimensionList.length;
	for (var r = 0; r < skuList.length; r++){
		nlapiLogExecution('Debug','skuList[r]', skuList[r]);
	}

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', skuList));		// ALL SKUs IN SKU LIST
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));							// ACTIVE RECORDS ONLY
	filters.push(new nlobjSearchFilter('custrecord_ebiz_notallowedforpicking', null, 'is', 'F'));
	if(DOwhLocation!=null && DOwhLocation!='' && DOwhLocation!='null')
		filters.push(new nlobjSearchFilter('custrecord_ebizsiteskudim', null, 'anyof', ['@NONE@', DOwhLocation]));	
	if(maxno!=-1)
	{
		filters.push(new nlobjSearchFilter('id', null, 'lessthan', parseInt(maxno)));
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');								// ITEM/SKU
	columns[1] = new nlobjSearchColumn('custrecord_ebizuomskudim');								// UOM
	columns[2] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim');						// UOM LEVEL
	columns[3] = new nlobjSearchColumn('custrecord_ebizqty');									// QUANTITY
	columns[4] = new nlobjSearchColumn('custrecord_ebizbaseuom');								// BASE UOM FLAG
	columns[5] = new nlobjSearchColumn('custrecord_ebizdims_packflag');							// PACK FLAG	
	columns[6] = new nlobjSearchColumn('custrecord_ebizweight');								// WEIGHT	
	columns[7] = new nlobjSearchColumn('custrecord_ebizcube');									// CUBE	
	columns[8] = new nlobjSearchColumn('internalid');
	columns[9] = new nlobjSearchColumn('custrecord_ebiz_dims_bulkpick');
	columns[8].setSort(true);

	// SEARCH FROM ITEM DIMENSIONS
	var searchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

	if(searchResults != null && searchResults != '' && searchResults.length >= 1000){
		var maxno1=searchResults[searchResults.length-1].getValue(columns[8]);
		for(var i = 0; i < searchResults.length; i++){
			var sku = searchResults[i].getValue('custrecord_ebizitemdims');
			var uom = searchResults[i].getValue('custrecord_ebizuomskudim');
			var uomLevel = searchResults[i].getValue('custrecord_ebizuomlevelskudim');
			var qty = searchResults[i].getValue('custrecord_ebizqty');
			var baseUOMFlag = searchResults[i].getValue('custrecord_ebizbaseuom');
			var packflag = searchResults[i].getValue('custrecord_ebizdims_packflag');
			var weight = searchResults[i].getValue('custrecord_ebizweight');
			var cube = searchResults[i].getValue('custrecord_ebizcube');
			var vbulkpick = searchResults[i].getValue('custrecord_ebiz_dims_bulkpick');

			var skuDimRecord = [sku, uom, uomLevel, qty, baseUOMFlag, packflag,weight,cube,vbulkpick];
			kititemDimensionList[itemDimCount++] = skuDimRecord;
		}

		getKitItemDimensions(skuList,maxno1,DOwhLocation);
	}
	else if(searchResults != null && searchResults != '')
	{
		for(var i = 0; i < searchResults.length; i++){
			var sku = searchResults[i].getValue('custrecord_ebizitemdims');
			var uom = searchResults[i].getValue('custrecord_ebizuomskudim');
			var uomLevel = searchResults[i].getValue('custrecord_ebizuomlevelskudim');
			var qty = searchResults[i].getValue('custrecord_ebizqty');
			var baseUOMFlag = searchResults[i].getValue('custrecord_ebizbaseuom');
			var packflag = searchResults[i].getValue('custrecord_ebizdims_packflag');
			var weight = searchResults[i].getValue('custrecord_ebizweight');
			var cube = searchResults[i].getValue('custrecord_ebizcube');
			var vbulkpick = searchResults[i].getValue('custrecord_ebiz_dims_bulkpick');

			var skuDimRecord = [sku, uom, uomLevel, qty, baseUOMFlag, packflag,weight,cube,vbulkpick];
			kititemDimensionList[itemDimCount++] = skuDimRecord;
		}

	}
	nlapiLogExecution('Debug','out of  getKitItemDimensions', skuList.length);
	return kititemDimensionList;
}

function getItemTypesForList(itemList){
	var itemTypeArray = new Array();
	var itemTypeCount = 0;

	for(var i = 0; i < itemList.length; i++){
		var itemType = nlapiLookupField('item', itemList[i], 'recordType');
		var currentRow = [itemList[i], itemType];
		itemTypeArray[itemTypeCount] = currentRow;
		itemTypeCount=itemTypeCount+1;
	}

	return itemTypeArray;
}

function getItemGroupAndFamily(itemTypeArray){
	var itemGroupFamilyArray = new Array();
	var itemGroupFamilyCount = 0;

	for(var i = 0; i < itemTypeArray.length; i++){
		var itemTypeElement = itemTypeArray[i];
		var item = itemTypeElement[0];
		var itemType = itemTypeElement[1];
		var itemRecord = getItemRecord(item, itemType);

		var currentRow = [item, itemType, itemRecord.getFieldValue('custitem_item_group'),
		                  itemRecord.getFieldValue('custitem_item_family')];
		itemGroupFamilyArray[itemGroupFamilyCount++] = currentRow;
	}

	return itemGroupFamilyArray;
}

function getItemRecord(item, itemType){
	var itemRecord = null;

	//This code is generalized to work for any kind of item types on 100411 ..Sudheer &Ganesh
	itemRecord = nlapiLoadRecord(itemType, item);


	/*if(itemType == 'inventoryitem')
		itemRecord = nlapiLoadRecord('inventoryitem', item);
	else if(itemType == 'assemblyitem')
		itemRecord = nlapiLoadRecord('assemblyitem', item);
	else if (itemType == 'lotnumberedinventoryitem')
		itemRecord = nlapiLoadRecord('lotnumberedinventoryitem', item);
	else if (ItemType == 'serializedinventoryitem')
		itemRecord = nlapiLoadRecord('serializedinventoryitem', item);
	else if (ItemType == 'lotnumberedassemblyitem')
		itemRecord = nlapiLoadRecord('serializedinventoryitem', item);
	 */
	return itemRecord;
}

function getListForAllPickZones(){

	nlapiLogExecution('Debug', 'into getListForAllPickZones','');
	var pickRuleList = new Array();
	var pickRuleCount = 0;

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizruleidpick');
	columns[1] = new nlobjSearchColumn('custrecord_ebizpickmethod');
	columns[2] = new nlobjSearchColumn('custrecord_ebizpickzonerul');
	columns[3] = new nlobjSearchColumn('custrecord_ebizlocationgrouppickrul');
	columns[4] = new nlobjSearchColumn('custrecord_ebizlocationpickrul');
	//columns[5] = new nlobjSearchColumn('custrecord_ebizsequencenopickrul');
	columns[5]=new nlobjSearchColumn('formulanumeric');
	columns[5].setFormula("TO_NUMBER({custrecord_ebizsequencenopickrul})").setSort();
	columns[6] = new nlobjSearchColumn('custrecord_ebizskufamilypickrul');
	columns[7] = new nlobjSearchColumn('custrecord_ebizskugrouppickrul');
	columns[8] = new nlobjSearchColumn('custrecord_ebizskupickrul');
	columns[9] = new nlobjSearchColumn('custrecord_ebizskuinfo1');
	columns[10] = new nlobjSearchColumn('custrecord_ebizskuinfo2');
	columns[11] = new nlobjSearchColumn('custrecord_ebizskuinfo3');
	columns[12] = new nlobjSearchColumn('custrecord_ebizpackcodepickrul');
	columns[13] = new nlobjSearchColumn('custrecord_ebizuomlevelpickrul');
	columns[14] = new nlobjSearchColumn('custrecord_ebizskustatspickrul');
	columns[15] = new nlobjSearchColumn('custrecord_ebizordertypepickrul');
	columns[16] = new nlobjSearchColumn('custrecord_ebizuompickrul');
	columns[17] = new nlobjSearchColumn('custrecord_allocation_strategy','custrecord_ebizpickmethod');
	columns[18] = new nlobjSearchColumn('custrecord_ebizpickfacelocation','custrecord_ebizpickmethod');
	columns[19] = new nlobjSearchColumn('custrecord_ebizcreatecluster','custrecord_ebizpickmethod');
	columns[20] = new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxorders','custrecord_ebizpickmethod');
	columns[21] = new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxpicks','custrecord_ebizpickmethod');
	columns[22] = new nlobjSearchColumn('custrecord_ebizabcvel');
	columns[23] = new nlobjSearchColumn('custrecord_cartonization_method','custrecord_ebizpickmethod');
	columns[24] = new nlobjSearchColumn('custrecord_ebizsitepickrule');
	//columns[5].setSort();

	var pickRuleSearchResult = new nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters, columns);

	if(pickRuleSearchResult != null){
		for (var i=0; i<pickRuleSearchResult.length; i++){
			var pickRuleId = pickRuleSearchResult[i].getValue('custrecord_ebizruleidpick');
			var pickMethodId = pickRuleSearchResult[i].getValue('custrecord_ebizpickmethod');
			var pickZoneId = pickRuleSearchResult[i].getValue('custrecord_ebizpickzonerul');
			var locationGroupId = pickRuleSearchResult[i].getValue('custrecord_ebizlocationgrouppickrul');
			var binLocationId = pickRuleSearchResult[i].getValue('custrecord_ebizlocationpickrul');
			var sequenceNo = pickRuleSearchResult[i].getValue('custrecord_ebizsequencenopickrul');
			var itemFamilyId = pickRuleSearchResult[i].getValue('custrecord_ebizskufamilypickrul');
			var itemGroupId = pickRuleSearchResult[i].getValue('custrecord_ebizskugrouppickrul');
			var itemId = pickRuleSearchResult[i].getValue('custrecord_ebizskupickrul');
			var itemInfo1 = pickRuleSearchResult[i].getValue('custrecord_ebizskuinfo1');
			var itemInfo2 = pickRuleSearchResult[i].getValue('custrecord_ebizskuinfo2');
			var itemInfo3 = pickRuleSearchResult[i].getValue('custrecord_ebizskuinfo3');
			var packCode = pickRuleSearchResult[i].getValue('custrecord_ebizpackcodepickrul');
			var uomLevel = pickRuleSearchResult[i].getValue('custrecord_ebizuomlevelpickrul');
			var itemStatus = pickRuleSearchResult[i].getValue('custrecord_ebizskustatspickrul');
			var orderType = pickRuleSearchResult[i].getValue('custrecord_ebizordertypepickrul');
			var uom = pickRuleSearchResult[i].getValue('custrecord_ebizuompickrul');
			var allocationStrategy = pickRuleSearchResult[i].getValue('custrecord_allocation_strategy','custrecord_ebizpickmethod');
			var pickfaceLocationFlag = pickRuleSearchResult[i].getValue('custrecord_ebizpickfacelocation','custrecord_ebizpickmethod');
			var clusterPickFlag = pickRuleSearchResult[i].getValue('custrecord_ebizcreatecluster','custrecord_ebizpickmethod');
			var maxOrders = pickRuleSearchResult[i].getValue('custrecord_ebiz_pickmethod_maxorders','custrecord_ebizpickmethod');
			var maxPicks = pickRuleSearchResult[i].getValue('custrecord_ebiz_pickmethod_maxpicks','custrecord_ebizpickmethod');
			var abcVel = pickRuleSearchResult[i].getValue('custrecord_abcvelpickrule');
			var cartonizationmethod = pickRuleSearchResult[i].getValue('custrecord_cartonization_method','custrecord_ebizpickmethod');
			var picklocation=pickRuleSearchResult[i].getValue('custrecord_ebizsitepickrule');

			var currentRow = [i, pickRuleId, pickMethodId, pickZoneId, locationGroupId, binLocationId, 
			                  sequenceNo, itemFamilyId, itemGroupId, itemId, itemInfo1, itemInfo2, itemInfo3, 
			                  packCode, uomLevel, itemStatus, orderType, uom, 
			                  allocationStrategy, pickfaceLocationFlag, clusterPickFlag, maxOrders, maxPicks, abcVel,cartonizationmethod,picklocation];


//			nlapiLogExecution('Debug', 'pickRule ID', pickRuleId);
//			nlapiLogExecution('Debug', 'pickMethod ID', pickMethodId);
//			nlapiLogExecution('Debug', 'pickZone ID', pickZoneId);
//			nlapiLogExecution('Debug', 'Location Group ID', locationGroupId);
//			nlapiLogExecution('Debug', 'Location ID', binLocationId);
//			nlapiLogExecution('Debug', 'pickRule - Current Row', currentRow);

			pickRuleList.push(currentRow);
		}
	}

	nlapiLogExecution('Debug', 'out of getListForAllPickZones','');

	return pickRuleList;
}

function getListForAllPickRules(Item,uomlevel,whLocation,ItemStatus,Packcode,OrderType){

	nlapiLogExecution('Debug', 'into getListForAllPickRules','');
	var pickRulesList = new Array();
	var allocConfigDtls = new Array();
	var pickRuleCount = 0;

	var fields = ['custitem_item_family','custitem_item_group','custitem_ebizdefskustatus',
	              'custitem_item_info_1','custitem_item_info_2','custitem_item_info_3','custitem_ebizabcvelitem'];


	var columns= nlapiLookupField('Item',Item,fields);
	var ItemFamily = columns.custitem_item_family;
	var ItemGroup = columns.custitem_item_group;
	var ItemInfo1 = columns.custitem_item_info_1;
	var ItemInfo2 = columns.custitem_item_info_2;
	var ItemInfo3 = columns.custitem_item_info_3;
	var putVelocity=columns.custitem_ebizabcvelitem;

//	nlapiLogExecution('Debug', 'ItemFamily',ItemFamily);
//	nlapiLogExecution('Debug', 'ItemGroup',ItemGroup);
//	nlapiLogExecution('Debug', 'ItemInfo1',ItemInfo1);
//	nlapiLogExecution('Debug', 'ItemInfo2',ItemInfo2);
//	nlapiLogExecution('Debug', 'ItemInfo3',ItemInfo3);
//	nlapiLogExecution('Debug', 'putVelocity',putVelocity);
//	nlapiLogExecution('Debug', 'ItemStatus',ItemStatus);
//	nlapiLogExecution('Debug', 'Item',Item);
//	nlapiLogExecution('Debug', 'whLocation',whLocation);
//	nlapiLogExecution('Debug', 'uomlevel',uomlevel);
//	nlapiLogExecution('Debug', 'Packcode',Packcode);
//	nlapiLogExecution('Debug', 'OrderType',OrderType);


	var filters = new Array();

	if(ItemFamily!=null && ItemFamily != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskufamilypickrul', null, 'anyof', ['@NONE@', ItemFamily]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskufamilypickrul', null, 'anyof', ['@NONE@']));
	}

	if(ItemGroup!=null && ItemGroup != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskugrouppickrul', null, 'anyof', ['@NONE@', ItemGroup]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskugrouppickrul', null, 'anyof', ['@NONE@']));
	}

	if(ItemStatus!=null && ItemStatus != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskustatspickrul', null, 'anyof', ['@NONE@', ItemStatus]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskustatspickrul', null, 'anyof', ['@NONE@']));
	}

	if(ItemInfo1!=null && ItemInfo1 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo1', null, 'anyof',['@NONE@', ItemInfo1]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo1', null, 'anyof', ['@NONE@']));
	}

	if(ItemInfo2!=null && ItemInfo2 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo2', null, 'anyof',['@NONE@', ItemInfo2]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo2', null, 'anyof', ['@NONE@']));
	}

	if(ItemInfo3!=null && ItemInfo3 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo3', null, 'anyof',['@NONE@', ItemInfo3]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo3', null, 'anyof', ['@NONE@']));
	}

	if(Item != null && Item != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskupickrul', null, 'anyof', ['@NONE@', Item]));		
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskupickrul', null, 'anyof', ['@NONE@']));
	}

	if(putVelocity != null && putVelocity != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizabcvel', null, 'anyof', ['@NONE@', putVelocity]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizabcvel', null, 'anyof', ['@NONE@']));
	}

	if(whLocation != null && whLocation != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizsitepickrule', null, 'anyof', [whLocation]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizsitepickrule', null, 'anyof', ['@NONE@']));
	}

	if(uomlevel != null && uomlevel != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizuomlevelpickrul', null, 'anyof', ['@NONE@', uomlevel]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizuomlevelpickrul', null, 'anyof', ['@NONE@']));
	}

	if(Packcode != null && Packcode != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizpackcodepickrul', null, 'anyof', ['@NONE@', Packcode]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizpackcodepickrul', null, 'anyof', ['@NONE@']));
	}

	if(OrderType != null && OrderType != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizordertypepickrul', null, 'anyof', ['@NONE@', OrderType]));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizordertypepickrul', null, 'anyof', ['@NONE@']));
	}

	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizruleidpick');
	columns[1] = new nlobjSearchColumn('custrecord_ebizpickmethod');
	columns[2] = new nlobjSearchColumn('custrecord_ebizpickzonerul');
	columns[3] = new nlobjSearchColumn('custrecord_ebizlocationgrouppickrul');
	columns[4] = new nlobjSearchColumn('custrecord_ebizlocationpickrul');
	//columns[5] = new nlobjSearchColumn('custrecord_ebizsequencenopickrul');
	columns[5]=new nlobjSearchColumn('formulanumeric');
	columns[5].setFormula("TO_NUMBER({custrecord_ebizsequencenopickrul})");
	columns[6] = new nlobjSearchColumn('custrecord_ebizskufamilypickrul');
	columns[7] = new nlobjSearchColumn('custrecord_ebizskugrouppickrul');
	columns[8] = new nlobjSearchColumn('custrecord_ebizskupickrul');
	columns[9] = new nlobjSearchColumn('custrecord_ebizskuinfo1');
	columns[10] = new nlobjSearchColumn('custrecord_ebizskuinfo2');
	columns[11] = new nlobjSearchColumn('custrecord_ebizskuinfo3');
	columns[12] = new nlobjSearchColumn('custrecord_ebizpackcodepickrul');
	columns[13] = new nlobjSearchColumn('custrecord_ebizuomlevelpickrul');
	columns[14] = new nlobjSearchColumn('custrecord_ebizskustatspickrul');
	columns[15] = new nlobjSearchColumn('custrecord_ebizordertypepickrul');
	columns[16] = new nlobjSearchColumn('custrecord_ebizuompickrul');
	columns[17] = new nlobjSearchColumn('custrecord_allocation_strategy','custrecord_ebizpickmethod');
	columns[18] = new nlobjSearchColumn('custrecord_ebizpickfacelocation','custrecord_ebizpickmethod');
	columns[19] = new nlobjSearchColumn('custrecord_ebizcreatecluster','custrecord_ebizpickmethod');
	columns[20] = new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxorders','custrecord_ebizpickmethod');
	columns[21] = new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxpicks','custrecord_ebizpickmethod');
	columns[22] = new nlobjSearchColumn('custrecord_ebizabcvel');
	columns[23] = new nlobjSearchColumn('custrecord_cartonization_method','custrecord_ebizpickmethod');
	columns[24] = new nlobjSearchColumn('custrecord_ebizsitepickrule');
	columns[25] = new nlobjSearchColumn('custrecord_ebiz_stagedetermination','custrecord_ebizpickmethod');
	columns[5].setSort();

	var pickRuleSearchResult = new nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters, columns);

	if(pickRuleSearchResult != null){
		nlapiLogExecution('Debug', 'pickRuleSearchResult length',pickRuleSearchResult.length);
		for (var i=0; i<pickRuleSearchResult.length; i++){
			var pickRuleId = pickRuleSearchResult[i].getValue('custrecord_ebizruleidpick');
			var pickMethodId = pickRuleSearchResult[i].getValue('custrecord_ebizpickmethod');
			var pickZoneId = pickRuleSearchResult[i].getValue('custrecord_ebizpickzonerul');
			var locationGroupId = pickRuleSearchResult[i].getValue('custrecord_ebizlocationgrouppickrul');
			var binLocationId = pickRuleSearchResult[i].getValue('custrecord_ebizlocationpickrul');
			var sequenceNo = pickRuleSearchResult[i].getValue('custrecord_ebizsequencenopickrul');
			var itemFamilyId = pickRuleSearchResult[i].getValue('custrecord_ebizskufamilypickrul');
			var itemGroupId = pickRuleSearchResult[i].getValue('custrecord_ebizskugrouppickrul');
			var itemId = pickRuleSearchResult[i].getValue('custrecord_ebizskupickrul');
			var itemInfo1 = pickRuleSearchResult[i].getValue('custrecord_ebizskuinfo1');
			var itemInfo2 = pickRuleSearchResult[i].getValue('custrecord_ebizskuinfo2');
			var itemInfo3 = pickRuleSearchResult[i].getValue('custrecord_ebizskuinfo3');
			var packCode = pickRuleSearchResult[i].getValue('custrecord_ebizpackcodepickrul');
			var uomLevel = pickRuleSearchResult[i].getValue('custrecord_ebizuomlevelpickrul');
			var itemStatus = pickRuleSearchResult[i].getValue('custrecord_ebizskustatspickrul');
			var orderType = pickRuleSearchResult[i].getValue('custrecord_ebizordertypepickrul');
			var uom = pickRuleSearchResult[i].getValue('custrecord_ebizuompickrul');
			var allocationStrategy = pickRuleSearchResult[i].getValue('custrecord_allocation_strategy','custrecord_ebizpickmethod');
			var pickfaceLocationFlag = pickRuleSearchResult[i].getValue('custrecord_ebizpickfacelocation','custrecord_ebizpickmethod');
			var clusterPickFlag = pickRuleSearchResult[i].getValue('custrecord_ebizcreatecluster','custrecord_ebizpickmethod');
			var maxOrders = pickRuleSearchResult[i].getValue('custrecord_ebiz_pickmethod_maxorders','custrecord_ebizpickmethod');
			var maxPicks = pickRuleSearchResult[i].getValue('custrecord_ebiz_pickmethod_maxpicks','custrecord_ebizpickmethod');
			var abcVel = pickRuleSearchResult[i].getValue('custrecord_abcvelpickrule');
			var cartonizationmethod = pickRuleSearchResult[i].getValue('custrecord_cartonization_method','custrecord_ebizpickmethod');
			var picklocation=pickRuleSearchResult[i].getValue('custrecord_ebizsitepickrule');
			var stagedetermination = pickRuleSearchResult[i].getValue('custrecord_ebiz_stagedetermination','custrecord_ebizpickmethod');
			var currentRow = [i, pickRuleId, pickMethodId, pickZoneId, locationGroupId, binLocationId, 
			                  sequenceNo, itemFamilyId, itemGroupId, itemId, itemInfo1, itemInfo2, itemInfo3, 
			                  packCode, uomLevel, itemStatus, orderType, uom, 
			                  allocationStrategy, pickfaceLocationFlag, clusterPickFlag, maxOrders, maxPicks, abcVel,cartonizationmethod,picklocation,stagedetermination];

			pickRulesList.push(currentRow);
		}

		if(pickRulesList!=null && pickRulesList!='' && pickRulesList.length>0)
		{
			nlapiLogExecution('Debug', 'pickRulesList length',pickRulesList.length);
			var pfLocationResults = getPFLocationsForOrder(whLocation,Item);

			for (var j=0; j<pickRulesList.length; j++){

//				var str = 'pickStrategyId. = ' + pickRulesList[j][1] + '<br>';
//				str = str + 'pickMethod. = ' + pickRulesList[j][2] + '<br>';	
//				str = str + 'pickZone. = ' + pickRulesList[j][3] + '<br>';

//				//nlapiLogExecution('Debug', 'Pick Strategy Details', str);

				var pickStrategyId = pickRulesList[j][1];
				var pickMethod = pickRulesList[j][2];
				var pickZone = pickRulesList[j][3];
				var locnGroup = pickRulesList[j][4];
				var binLocation = pickRulesList[j][5];
				var allocationStrategy = pickRulesList[j][18];
				var pickfaceLocationFlag = pickRulesList[j][19];
				var clusterPickFlag = pickRulesList[j][20];
				var maxOrders = pickRulesList[j][21];
				var maxPicks = pickRulesList[j][22];
				var cartonizationmethod =  pickRulesList[j][24];
				var stagedetermination =  pickRulesList[j][26];

				var tempPFLocationResults = new Array();

				if(pickfaceLocationFlag == 'T')
					tempPFLocationResults = pfLocationResults;
				else
					tempPFLocationResults = null;

				var locnGroupList = new Array();

				// Pick strategy details for the fulfillment order; Using index in the order list to identify the order
				var currentRow1 = [j, pickStrategyId, pickMethod, pickZone, locnGroup, binLocation, allocationStrategy,
				                   tempPFLocationResults, locnGroupList, clusterPickFlag, maxOrders, maxPicks,cartonizationmethod,stagedetermination];

				allocConfigDtls.push(currentRow1);
			}
		}
	}

	nlapiLogExecution('Debug', 'out of getListForAllPickRules','');

	return allocConfigDtls;
}

function getPickConfigForAllOrder(fulfillOrderList, pickRulesList, itemGroupFamilyArray,whlocation,skuList){

	nlapiLogExecution('Debug','into getPickConfigForAllOrder : pickRulesList',pickRulesList);
	var allocConfigDtls = new Array();

	// Get all pickface locations
	var pfLocationResults = getPFLocationsForOrder(whlocation,skuList);

	// For all fulfilment orders
	for (var i = 0; i < fulfillOrderList.length; i++){
		// Get index of matching pick strategy
		var index = getPickStrategyForFulfilmentOrder(fulfillOrderList[i], pickRulesList,
				itemGroupFamilyArray);

		var pickStrategyId = pickRulesList[index][1];
		var pickMethod = pickRulesList[index][2];
		var pickZone = pickRulesList[index][3];
		var locnGroup = pickRulesList[index][4];
		var binLocation = pickRulesList[index][5];
		var allocationStrategy = pickRulesList[index][18];
		var pickfaceLocationFlag = pickRulesList[index][19];
		var clusterPickFlag = pickRulesList[index][20];
		var maxOrders = pickRulesList[index][21];
		var maxPicks = pickRulesList[index][22];
		var cartonizationmethod =  pickRulesList[index][24];

		var tempPFLocationResults = new Array();
		if(pickfaceLocationFlag == 'T')
			tempPFLocationResults = pfLocationResults;
		else
			tempPFLocationResults = null;

		var locnGroupList = getLocnGroupListForOrder(locnGroup, binLocation, pickZone);

		// Pick strategy details for the fulfillment order; Using index in the order list to identify the order
		var currentRow = [i, pickStrategyId, pickMethod, pickZone, locnGroup, binLocation, allocationStrategy,
		                  tempPFLocationResults, locnGroupList, clusterPickFlag, maxOrders, maxPicks,cartonizationmethod];

		allocConfigDtls.push(currentRow);
	}

	nlapiLogExecution('Debug','out of getPickConfigForAllOrder : allocConfigDtls.length ',allocConfigDtls.length);	


	return allocConfigDtls;
}

function getLocnGroupListForOrder(locnGroup, binLocation, pickZone){

	var locnGroupList = new Array();
	// Get the bin location group list
	if (locnGroup == null || locnGroup == "")
		locnGroupList = getZoneLocnGroupsList(pickZone);
	else{
		if (binLocation == null || binLocation == ''){
			locnGroupList.push(locnGroup);
		}
	}
	return locnGroupList;
}


function getLocnGroupsForOrder(locnGroup, binLocation, pickZone,allzonelocgroups){

	var locnGroupList = new Array();
	// Get the bin location group list
	if (locnGroup == null || locnGroup == "")
		locnGroupList = getZoneLocnGroups(pickZone,allzonelocgroups);
	else{
		if (binLocation == null || binLocation == ''){
			locnGroupList.push(locnGroup);
		}
	}
	return locnGroupList;
}

function getItemGroupFamilyForItem(item, itemGroupFamilyList){
	var groupFamilyArray = new Array();
	var itemFound = false;

	if(itemGroupFamilyList != null && itemGroupFamilyList.length > 0){
		for(var i = 0; i < itemGroupFamilyList.length; i++){
			if(!itemFound){
				if(item == parseInt(itemGroupFamilyList[i][0])){
					groupFamilyArray.push(itemGroupFamilyList[i][2]);	// ITEM GROUP
					groupFamilyArray.push(itemGroupFamilyList[i][3]);	// ITEM FAMILY

					itemFound = true;
				}
			}
		}
	}
	nlapiLogExecution('Debug','itemFound',itemFound);
	return groupFamilyArray;
}

function getPickStrategyForFulfilmentOrder(currentOrder, pickRulesList, itemGroupFamilyList){
	var pickStrategyIndex = -1;
	var matchFound = false;

	nlapiLogExecution('Debug', 'currentOrder in getItemGroupFamilyForItem ',currentOrder[6]);

	// Get item group and family for item
	var groupFamilyList = getItemGroupFamilyForItem(currentOrder[6], itemGroupFamilyList);

	// Traverse through all the pick strategies and determine the match
	if(pickRulesList != null && pickRulesList.length > 0){
		for(var i = 0; i < pickRulesList.length; i++){
			if(!matchFound){
				if(pickRulesList[i][25]==currentOrder[19])											// WH SITE
				{
					if(parseInt(pickRulesList[i][9]) == parseInt(currentOrder[6]) ||				// ITEM
							parseInt(pickRulesList[i][15]) == parseInt(currentOrder[7]) ||			// ITEM STATUS
							pickRulesList[i][10] == currentOrder[14] ||								// ITEM INFO 1
							pickRulesList[i][11] == currentOrder[15] ||								// ITEM INFO 2
							pickRulesList[i][12] == currentOrder[16] ||								// ITEM INFO 3
							parseInt(pickRulesList[i][17]) == parseInt(currentOrder[11]) ||			// UOM
							parseInt(pickRulesList[i][8]) == parseInt(groupFamilyList[0]) ||		// ITEM GROUP
							parseInt(pickRulesList[i][16]) == parseInt(currentOrder[9]) ||			// ORDER TYPE
							parseInt(pickRulesList[i][7]) == parseInt(groupFamilyList[1])){			// ITEM FAMILY
						pickStrategyIndex = i;
						matchFound = true;
					}
				}

				if(! matchFound && i == parseInt(pickRulesList.length - 1)){
					pickStrategyIndex = 0;
					matchFound = true;
				}
			}
		}
	}

	return pickStrategyIndex;
}

function getPFLocationsForOrder(whlocation,skuList){
	var pfLocationResults = new Array();
	pfLocationResults = getSKUQtyInPickfaceLocns(whlocation,skuList,null);
	return pfLocationResults;
}

function getOrderDetailsForWaveGen(ordno,ordlineno,ebizwaveno)
{
	nlapiLogExecution('Debug', 'Into getOrderDetailsForWaveGen');
	var orderList = new Array();
	var filters = new Array();
	var fulfillmentordlist = new Array();
	var orderInfoCount = 0;

	nlapiLogExecution('Debug', 'ebizwaveno', ebizwaveno);
	nlapiLogExecution('Debug', 'ordno', ordno);
	nlapiLogExecution('Debug', 'ordlineno', ordlineno);

	filters.push(new nlobjSearchFilter('custrecord_ebiz_wave', null, 'equalto', parseInt(ebizwaveno)));

	if(ordno!=null && ordno!='')
	{
		filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', ordno));
	}

	if(ordlineno!=null && ordlineno!='')
	{
		//filters.push(new nlobjSearchFilter('custrecord_ordline', null, 'is', ordlineno));
		filters.push(new nlobjSearchFilter('custrecord_ordline', null, 'equalto', parseInt(ordlineno))); 
		//11-Partially Picked, 13-Partially Shipped, 15-Selected Into Wave, 26-Picks Failed
		filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [11,13,15,26]));
	}

	if((ordno==null || ordno=='') && (ordlineno==null || ordlineno==''))
	{
		filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [15]));
	}

	var columns = new Array();

	columns[0] = new nlobjSearchColumn('custrecord_linesku_status');
	columns[1] = new nlobjSearchColumn('custrecord_do_customer');
	columns[2] = new nlobjSearchColumn('custrecord_do_carrier');
	columns[3] = new nlobjSearchColumn('custrecord_do_order_type');
	columns[4] = new nlobjSearchColumn('custrecord_do_order_priority');
	columns[5] = new nlobjSearchColumn('custrecord_lineord');
	columns[6] = new nlobjSearchColumn('custrecord_ebiz_linesku');
	columns[7] = new nlobjSearchColumn('custrecord_ord_qty');
	columns[8] = new nlobjSearchColumn('name');
	columns[8].setSort();
	columns[9] = new nlobjSearchColumn('custrecord_lineord');
	columns[9].setSort();		
	columns[10] = new nlobjSearchColumn('custrecord_ordline');
	columns[10].setSort();
	columns[11] = new nlobjSearchColumn('custrecord_linepackcode');
	columns[12] = new nlobjSearchColumn('custrecord_lineuom_id');
	columns[13] = new nlobjSearchColumn('custrecord_batch');
	columns[14] = new nlobjSearchColumn('custrecord_ordline_wms_location');
	columns[15] = new nlobjSearchColumn('custrecord_ordline_company');
	columns[16] = new nlobjSearchColumn('custrecord_fulfilmentiteminfo1');
	columns[17] = new nlobjSearchColumn('custrecord_fulfilmentiteminfo2');
	columns[18] = new nlobjSearchColumn('custrecord_fulfilmentiteminfo3');	
	columns[19] = new nlobjSearchColumn('custrecord_pickgen_qty');
	columns[20] = new nlobjSearchColumn('custrecord_do_wmscarrier');
	columns[21] = new nlobjSearchColumn('custrecord_nsconfirm_ref_no');
	columns[22] = new nlobjSearchColumn('custrecord_linenotes1');
	columns[23] = new nlobjSearchColumn('custrecord_linenotes2');
	columns[24] = new nlobjSearchColumn('custrecord_pickgen_flag');
	columns[25] = new nlobjSearchColumn('custrecord_pickqty');
	columns[26] = new nlobjSearchColumn('custrecord_ship_qty');
	columns[27] = new nlobjSearchColumn('custrecord_ns_ord');
	columns[28] = new nlobjSearchColumn('custrecord_shipment_no');
	columns[29] = new nlobjSearchColumn('custrecord_printflag');
	columns[30] = new nlobjSearchColumn('custrecord_print_count');
	columns[31] = new nlobjSearchColumn('custrecord_ebiz_freightterms');
	columns[32] = new nlobjSearchColumn('custrecord_shipcomplete');
	columns[33] = new nlobjSearchColumn('custrecord_ebiz_pr_dateprinted');
	columns[34] = new nlobjSearchColumn('custentity_ebiz_lefo_required','custrecord_do_customer');
	columns[35] = new nlobjSearchColumn('custentity_ebiz_notto_split','custrecord_do_customer');
	columns[36] = new nlobjSearchColumn('custentity_ebiz_samelot','custrecord_do_customer');
	columns[37] = new nlobjSearchColumn('custentity_ebiz_shelflife','custrecord_do_customer');
	columns[38] = new nlobjSearchColumn('isinactive','custrecord_ebiz_linesku');
	//case# 20149469 starts
	columns[39] = new nlobjSearchColumn('custrecord_actuallocation');
	//case# 20149469 ends
	columns[40] = new nlobjSearchColumn('custrecord_ebiz_lot_number');
	columns[41] = new nlobjSearchColumn('custrecord_ebiz_lotnumber_qty');
	columns[42] = new nlobjSearchColumn('custrecord_ebiz_lotno_internalid');
	orderList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);

	if(orderList!=null && orderList!='' && orderList.length>0)
	{
		var vlineCount=0;
		nlapiLogExecution('DEBUG', 'orderList length', orderList.length);
		for(i=0;i<orderList.length;i++)
		{
			var currentOrder=orderList[i];
			var ISItemInActive = currentOrder.getValue('isinactive','custrecord_ebiz_linesku');
			if(ISItemInActive=="F")
			{
				var vOrdQty = parseInt(currentOrder.getValue('custrecord_ord_qty'));

				//nlapiLogExecution('Debug', 'vOrdQty', vOrdQty);

				var vPickGenQty=0;

				if(currentOrder.getValue('custrecord_pickgen_qty')!=null && currentOrder.getValue('custrecord_pickgen_qty')!='')	
					vPickGenQty = parseInt(currentOrder.getValue('custrecord_pickgen_qty'));

				//nlapiLogExecution('Debug', 'vPickGenQty', vPickGenQty);

				vOrdQty = parseInt(vOrdQty)-parseInt(vPickGenQty);

				//nlapiLogExecution('Debug', 'vOrdQty', vOrdQty);

				var itemName = currentOrder.getText('custrecord_ebiz_linesku');
				var itemNo = currentOrder.getValue('custrecord_ebiz_linesku');
				var foordqty = currentOrder.getValue('custrecord_ord_qty');
				var availQty = 0;
				var orderQty = vOrdQty;
				var fointrid = currentOrder.getId();
				var doNo = currentOrder.getId();
				var lineNo = currentOrder.getValue('custrecord_ordline');
				var foname = currentOrder.getValue('custrecord_lineord');
				var doName = currentOrder.getValue('custrecord_lineord');
				var soInternalID = currentOrder.getValue('name');
				var itemStatus = currentOrder.getValue('custrecord_linesku_status');
				var packCode = currentOrder.getText('custrecord_linepackcode');
				var uom = currentOrder.getValue('custrecord_lineuom_id');
				var lotBatchNo = currentOrder.getValue('custrecord_batch');
				var location = currentOrder.getValue('custrecord_ordline_wms_location');
				var company = currentOrder.getValue('custrecord_ordline_company');
				var itemInfo1 = currentOrder.getValue('custrecord_fulfilmentiteminfo1');
				var itemInfo2 = currentOrder.getValue('custrecord_fulfilmentiteminfo2');
				var itemInfo3 = currentOrder.getValue('custrecord_fulfilmentiteminfo3');
				var orderType = currentOrder.getValue('custrecord_do_order_type');
				var orderPriority = currentOrder.getValue('custrecord_do_order_priority');
				var wmsCarrier = currentOrder.getValue('custrecord_do_wmscarrier');
				var nsrefno = currentOrder.getValue('custrecord_nsconfirm_ref_no');			
				var note1 = currentOrder.getValue('custrecord_linenotes1');
				var note2 = currentOrder.getValue('custrecord_linenotes2');
				var pickgenflag = currentOrder.getValue('custrecord_pickgen_flag');
				var pickqty = currentOrder.getValue('custrecord_pickqty');
				var shipqty = currentOrder.getValue('custrecord_ship_qty');
				var parentordno = currentOrder.getValue('custrecord_ns_ord');
				var shipmentno = currentOrder.getValue('custrecord_shipment_no');
				var printflag = currentOrder.getValue('custrecord_printflag');
				var printcount = currentOrder.getValue('custrecord_print_count');
				var freightterms = currentOrder.getValue('custrecord_ebiz_freightterms');
				var shipcomplete = currentOrder.getValue('custrecord_shipcomplete');
				var pickreportprintdt = currentOrder.getValue('custrecord_ebiz_pr_dateprinted');
				var customer = currentOrder.getValue('custrecord_do_customer');
				var shipmethod = currentOrder.getValue('custrecord_do_carrier');			
				var lefoflag = currentOrder.getValue('custentity_ebiz_lefo_required','custrecord_do_customer');
				var PickDontSplitFlag = currentOrder.getValue('custentity_ebiz_notto_split','custrecord_do_customer');
				var sameLot = currentOrder.getValue('custentity_ebiz_samelot','custrecord_do_customer');
				var shelfLife = currentOrder.getValue('custentity_ebiz_shelflife','custrecord_do_customer');
				//case# 20149469 starts
				var actsolcation = currentOrder.getValue('custrecord_actuallocation');
				//case# 20149469 ends
				var folotnumber = currentOrder.getValue('custrecord_ebiz_lot_number');
				var folotqty = currentOrder.getValue('custrecord_ebiz_lotnumber_qty');
				var folotinternalid = currentOrder.getValue('custrecord_ebiz_lotno_internalid');
				if(PickDontSplitFlag == null || PickDontSplitFlag == '')
					PickDontSplitFlag='N';

				if(nsrefno == null || isNaN(nsrefno))
					nsrefno='';

				var currentRow = [vlineCount, soInternalID, lineNo, fointrid, foname, itemName, itemNo, itemStatus,foordqty, orderQty, 
				                  packCode, uom, lotBatchNo, company,itemInfo1, itemInfo2, itemInfo3, orderType, orderPriority, 
				                  location,wmsCarrier,nsrefno,note1,note2,pickgenflag,pickqty,shipqty,parentordno,shipmentno,
				                  printflag,printcount,freightterms,shipcomplete,pickreportprintdt,vPickGenQty,customer,shipmethod,
				                  lefoflag,PickDontSplitFlag,sameLot,shelfLife,actsolcation,folotnumber,folotqty,folotinternalid];//case# 20149469 actsolcation passed to currentrow

				/*var currentRow = [i, soInternalID, lineNo, doNo, doName, itemName, itemNo, itemStatus, 
			                  availQty, orderQty, packCode, uom, lotBatchNo, company, 
			                  itemInfo1, itemInfo2, itemInfo3, orderType, orderPriority,location,wmsCarrier,nsrefno];*/
				//nlapiLogExecution('Debug', 'currentRow', currentRow);
				fulfillmentordlist[orderInfoCount++] = currentRow;
				vlineCount++;
			}
			else
			{
				var recId = currentOrder.getId();

				var fieldNames = new Array(); 				//fields to be updated
				fieldNames[1] = "custrecord_linestatus_flag";
				fieldNames[2] = "custrecord_linenotes1";

				var newValues = new Array(); 				//new field values
				newValues[1] =  '26';
				newValues[2] =  "ITEM IS INACTIVE";

				nlapiSubmitField('customrecord_ebiznet_ordline', recId, fieldNames, newValues);

			}
		}
	}

	nlapiLogExecution('Debug', 'Out of getOrderDetailsForWaveGen');

	return fulfillmentordlist;
}

//Maintaining the allocation by bin location
var allocatedInv = new Array();
function allocateQuantity(inventorySearchResults, fulfillOrderList, eBizWaveNo, allocConfigDtls,containerslist,
		LPparent,replenRulesList,pickruleslist,allZoneLocnGroups,itemdimsarr,pflocationsarr,allinventorySearchResults,
		otparent,foparent,invtparent,vShipAloneRULE,waveassignedto,Reportno)
{
	nlapiLogExecution('Error', 'Into allocateQuantity', eBizWaveNo);


	var fulfilmentItem;
	var inventoryItem;
	var inventoryQuantity = 0;
	var allocationQuantity = 0;
	var recordId;
	var remainingQuantity = 0;
	var actualQty = 0; 
	var salesOrderList = new Array();
	var whLocation = "";
	var DOwhLocation = "";
	var fulfillmentItemStatus;
	// Maintaining the allocation by bin location
	//var allocatedInv = new Array();
	var containerLP="";
	var cartonizationmethod="";
	var vnotes='';
	var wmsCarrier="";
	var memberitemqty;
	var nsrefno='';
	var ordtype='';
	var lefoflag='';
	var dimsbulkpick='';
	var customer='';
	var samelot='';
	var shelflife='';
	var latestexpirydt = '';
	var vfolotnumber='';
	var vfolotqty='';
	var vfolotinternalid='';
	var solinecount=0;

	if(vShipAloneRULE=='Y')
	{
		var vsointrid = fulfillOrderList[0][1]; 	
		solinecount = getSOLineCount(vsointrid);

		nlapiLogExecution('DEBUG', 'solinecount',solinecount);
	}
	/*
	 *	For all fulfillment orders
	 *		If PF Location Flag is set, then allocate to PF Locations
	 *		Allocate remaining quantity to inventory search results
	 */
	try{
		var itemsarr = new Array();

		for (var x = 0; x < fulfillOrderList.length; x++)
		{
			itemsarr.push(fulfillOrderList[x][6]);

			customer = fulfillOrderList[x][35];
			samelot = fulfillOrderList[x][39];
			shelflife = fulfillOrderList[x][40];
		}

		//nlapiLogExecution('Debug', 'customer', customer);

		/*
		if(allinventorySearchResults!=null && allinventorySearchResults!='' && allinventorySearchResults.length>=1000)			
			allinventorySearchResults = getItemDetails(null, itemsarr,null,null,lefoflag,
					null,null,null,samelot,latestexpirydt);			
		 */
//		nlapiLogExecution('Debug', 'Time Stamp After calling getItemDetails',TimeStampinSec());

		var prvSalesord='';
		var invtcontLP = "";
		var pfcontLP="";
		var packCode = "";
		var toatlcartonwht=0;
		var toatlcartoncube=0;

		DOwhLocation = fulfillOrderList[0][19];		

		//nlapiLogExecution('Debug', 'DOwhLocation', DOwhLocation);

		//var ordercarton = GetMaxLPNo('1', '2',DOwhLocation);

		var ordercarton = '';

		//nlapiLogExecution('Debug', 'ordercarton', ordercarton);

		for (var i = 0; i < fulfillOrderList.length; i++)
		{
			var KitFailed=false;
			if(nlapiGetContext().getRemainingUsage()<1000)
			{	
				//The below code is commented by Satish.N on 12/03/2015

				/*				
				//The parent records submition should be here
				if(vInvtArr != null && vInvtArr != '' && vInvtArr.length>0)
				{
					updateInventoryRecordBulk(vInvtArr,invtparent);
				}

				if(vFulfillArr != null && vFulfillArr != '' && vFulfillArr.length>0)
					updateFulfilmentOrderBulk(vFulfillArr,foparent);

				nlapiSubmitRecord(otparent); //submit the parent record, all child records will also be updated
				nlapiSubmitRecord(foparent); //submit the parent record, all child records will also be updated
				//nlapiSubmitRecord(invtparent); //submit the parent record, all child records will also be updated				
				 */

				//Upto here

				nlapiSubmitRecord(LPparent);

				//nlapiLogExecution('Debug','Calling Second Scheduler - Remaining Usage',context.getRemainingUsage());
				var param = new Array();
				param['custscript_ebizwaveno'] = eBizWaveNo;
				nlapiScheduleScript('customscript_ebiz_pickgen_scheduler', null,param);
				return;
			}

			var totordqty=0;
			var totreplenqty=0;

			nlapiLogExecution('DEBUG', 'Time Stamp Before calling nlapiLookupField for Item',TimeStampinSec());
			var itemTyperec = nlapiLookupField('item', fulfillOrderList[i][6], ['recordType','custitem_ebiz_shipbyitself','custitem_ebizbatchlot']);
			var itemType=itemTyperec.recordType;
			var vShipByItself=itemTyperec.custitem_ebiz_shipbyitself;
			var batchflag = itemTyperec.custitem_ebizbatchlot;
			nlapiLogExecution('DEBUG', 'Time Stamp After calling nlapiLookupField for Item',TimeStampinSec());
			nlapiLogExecution('Error', 'itemType', itemType);
			nlapiLogExecution('Error', 'vShipByItself', vShipByItself);

			if(itemType=='kititem' || itemType=='itemgroup')
			{
				DOwhLocation = fulfillOrderList[i][19];	

				var kititemdimsarr = new Array();
				var kititemsarr = new Array();
				var searchresultsitem = nlapiLoadRecord(itemType, fulfillOrderList[i][6]);
				var SkuNo=searchresultsitem.getFieldValue('itemid');
				nlapiLogExecution('Error', 'SkuNo', SkuNo);
				var recCount=0; 
				var filters = new Array(); 			 
				filters[0] = new nlobjSearchFilter('itemid', null, 'is', SkuNo);		
				filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
				filters[2] = new nlobjSearchFilter('type', null, 'is', 'Kit');
				var columns1 = new Array(); 
				columns1[0] = new nlobjSearchColumn( 'memberitem' ); 			
				columns1[1] = new nlobjSearchColumn( 'memberquantity' );

				var searchresults = nlapiSearchRecord( 'item', null, filters, columns1 ); 	

				for(var q=0; searchresults!=null && q<searchresults.length;q++) 
				{
					kititemsarr.push(searchresults[q].getValue('memberitem'));
				}
				var vItemInactive=IsMemberItemInactive(kititemsarr);

				if(vItemInactive==null)
				{
					kititemdimsarr = getKitItemDimensions(kititemsarr,-1,DOwhLocation);

					var allkitinventorySearchResults = getItemDetails(null, kititemsarr,null,null,null,
							null,null,null,samelot,latestexpirydt);

					for(var w=0; searchresults!=null && w<searchresults.length;w++) 
					{
						var totordqty=0;
						var totreplenqty=0;
						fulfilmentItem = searchresults[w].getValue('memberitem');
						memberitemqty = searchresults[w].getValue('memberquantity');
						fulfilmentQuantity=fulfillOrderList[i][9]*parseInt(memberitemqty);
						remainingQuantity = parseInt(fulfilmentQuantity);		
						fulfilmentOrderNo = fulfillOrderList[i][3]; 			// ORDER NO.
						salesOrderNo = fulfillOrderList[i][1]; 					// SALES ORDER INTERNAL ID.
						fulfilmentOrderLineNo = fulfillOrderList[i][2];			// FULFILMENT ORDER LINE NO.
						fulfillmentItemStatus=fulfillOrderList[i][7];			// FULFILMENT Item Status.
						DOwhLocation = fulfillOrderList[i][19];					// FULFILMENT WAREHOUSE LOCATION
						packCode=fulfillOrderList[i][10];
						wmsCarrier = fulfillOrderList[i][20];
						nsrefno = fulfillOrderList[i][21];
						samelot = fulfillOrderList[i][39];
						shelflife = fulfillOrderList[i][40];
						customer = fulfillOrderList[i][35];
						ordtype = fulfillOrderList[i][17];
						ordpriority = fulfillOrderList[i][18];
						//case# 20149995 (if cusomer is empty then we are not clling getLatestExpiry function)
						//var customerlotdet = getLatestExpiry(customer,fulfilmentItem,DOwhLocation);
						var customerlotdet="";
						if(customer!=null && customer!='')
							customerlotdet = getLatestExpiry(customer,fulfilmentItem,DOwhLocation);
						//end
						if(customerlotdet!=null && customerlotdet!='')
						{
							latestexpirydt = customerlotdet[0].getValue('custrecord_ebiz_lot_expiry');
						}

						var str = 'packCode. = ' + packCode + '<br>';
						str = str + 'fulfillmentItemStatus. = ' + fulfillmentItemStatus + '<br>';	
						str = str + 'fulfillment Warehouse Location. = ' + DOwhLocation + '<br>';
						str = str + 'wmsCarrier. = ' + wmsCarrier + '<br>';
						str = str + 'latestexpirydt. = ' + latestexpirydt + '<br>';

						nlapiLogExecution('Debug', 'Fulfillment Ord Line Details', str);

						var arrLPRequired = pickgen_palletisation(fulfilmentItem,fulfilmentQuantity,kititemdimsarr,DOwhLocation);

						if(arrLPRequired==null || arrLPRequired==''||arrLPRequired.length==0)
						{
							vnotes='Missing Dimensions';
							KitFailed=true;
							nlapiLogExecution('DEBUG', 'CallingFunction1', 'Sucess');

							//createRecordInOpenTask
							createpicktask(fulfillOrderList[i], remainingQuantity, eBizWaveNo, "", "", "",
									"", "", "", "", "", DOwhLocation, "","","","","","",vnotes,"",fulfilmentItem,nsrefno,customer,dimsbulkpick,otparent,waveassignedto);

							//updateFulfilmentOrder
							updateFulfilmentOrderline(fulfillOrderList[i], 0, eBizWaveNo,foparent);

						}
						else
						{
							var nooflps = "";
							var uomlevel = "";
							var lpbrkqty = "";
							var uompackflag = "";
							var uomweight="";
							var uomcube="";
							var uomqty="";
							var uom='';

							nlapiLogExecution('Debug', 'arrLPRequired.length',arrLPRequired.length);

							for(s = 0;s < parseInt(arrLPRequired.length);s++)
							{							
								nooflps = arrLPRequired[s][0];
								uomlevel = arrLPRequired[s][1];
								lpbrkqty = arrLPRequired[s][2];
								uompackflag = arrLPRequired[s][3];
								uomweight = arrLPRequired[s][4];
								uomcube= arrLPRequired[s][5];
								uomqty = arrLPRequired[s][6];
								dimsbulkpick = arrLPRequired[s][7];
								uom = arrLPRequired[s][8];

								var str = 'nooflps. = ' + nooflps + '<br>';
								str = str + 'lpbrkqty. = ' + lpbrkqty + '<br>';	
								str = str + 'uomlevel. = ' + uomlevel + '<br>';
								str = str + 'dimsbulkpick. = ' + dimsbulkpick + '<br>';
								str = str + 'uom. = ' + uom + '<br>';

								nlapiLogExecution('Debug', 'Breakdown Details', str);

								if(parseInt(nooflps)>0)
								{
									var allocConfigDtls1 = getListForAllPickRules(fulfilmentItem,uomlevel,DOwhLocation,fulfillmentItemStatus,packCode,ordtype);

									if(allocConfigDtls1==null || allocConfigDtls1==''||allocConfigDtls1.length==0)
									{
										vnotes='Pick Strategy Error';
										KitFailed=true;
										nlapiLogExecution('Debug', 'Calling Function1', 'Sucess');
										//createRecordInOpenTask
										createpicktask(fulfillOrderList[i], remainingQuantity, eBizWaveNo, "", "", "",
												"", "", "", "", "", DOwhLocation, "","","","","","",vnotes,"",fulfilmentItem,nsrefno,customer,dimsbulkpick,otparent,waveassignedto);

										//updateFulfilmentOrder
										updateFulfilmentOrderline(fulfillOrderList[i], 0, eBizWaveNo,foparent);
									}
									else
									{
										var vBoolFailedForSameLP=false;

										for(t=1; t<=parseInt(nooflps);t++)
										{									

											remainingQuantity = parseInt(lpbrkqty);

											if(remainingQuantity==null || remainingQuantity==''|| remainingQuantity==0)
												remainingQuantity=lpbrkqty;									

											if(vBoolFailedForSameLP ==true && t>1)
											{
												//If there is still some quantity remaining, then insert a failed task
												if(parseInt(nooflps)>0 && parseInt(remainingQuantity) > 0)
												{
													KitFailed=true;
//													vnotes='Insufficient Inventory';
//													nlapiLogExecution('Debug', 'Creating Failed Task');
//													nlapiLogExecution('Debug', 'remainingQuantity',remainingQuantity);
													//createRecordInOpenTask
													createpicktask(fulfillOrderList[i], remainingQuantity, eBizWaveNo, "", "", "",
															"", "", "", "", "", DOwhLocation, "",uomlevel,"","","","",vnotes,"",fulfilmentItem,nsrefno,customer,dimsbulkpick,otparent,waveassignedto);
													//updateFulfilmentOrder
													updateFulfilmentOrderline(fulfillOrderList[i], 0, eBizWaveNo,foparent);
													nlapiLogExecution('Debug', 'Creating Failed Task','SUCCESS');
													vBoolFailedForSameLP=true;
													remainingQuantity=0;

												}	
											}
											else
											{
												nlapiLogExecution('Debug', 'remainingQuantity1', remainingQuantity);

												if(parseInt(remainingQuantity) > 0) 
												{
													for(v = 0; v < parseInt(allocConfigDtls1.length) && parseInt(remainingQuantity) > 0; v++)
													{
														var pfLocationResults = allocConfigDtls1[v][7];
														var pickMethod = allocConfigDtls1[v][2];
														var pickZone = allocConfigDtls1[v][3];
														var pickStrategyId = allocConfigDtls1[v][1];
														var allocationStrategy = allocConfigDtls1[v][6];
														cartonizationmethod=allocConfigDtls1[v][12];
														var binlocid=allocConfigDtls1[v][5];
														var binlocgrpid=allocConfigDtls1[v][4];

														if(cartonizationmethod=="" || cartonizationmethod==null)
															cartonizationmethod='4';

														var str = 'pfLocationResults. = ' + pfLocationResults + '<br>';
														str = str + 'pickMethod. = ' + pickMethod + '<br>';	
														str = str + 'pickStrategyId. = ' + pickStrategyId + '<br>';
														str = str + 'pickZone. = ' + pickZone + '<br>';
														str = str + 'binlocgrpid. = ' + binlocgrpid + '<br>';
														str = str + 'binlocid. = ' + binlocid + '<br>';
														str = str + 'allocationStrategy. = ' + allocationStrategy + '<br>';
														str = str + 'cartonizationmethod. = ' + cartonizationmethod + '<br>';
														//str = str + 'remainingUsage. = ' + currContext.getRemainingUsage() + '<br>';

														nlapiLogExecution('Debug', 'Pick Strategy Details', str);

														/** The below line is commented by Satish.N on 03/29/2013 **/

														//var allLocnGroups = getAllLocnGroups(allocConfigDtls1);

														/** Upto here **/

														var allLocnGroups =  getLocnGroupsForOrder(binlocgrpid, binlocid, pickZone,allZoneLocnGroups);

														nlapiLogExecution('Debug', 'Time Stamp After calling getLocnGroupsForOrder',TimeStampinSec());

														var pfBinLocationgroupId = '';
														if (pfLocationResults != null && pfLocationResults.length > 0)
														{
															for(var P = 0; P < pfLocationResults.length; P++)
															{
																pfBinLocationgroupId = pfLocationResults[P].getValue('custrecord_inboundlocgroupid', 'custrecord_pickbinloc');

															}
														}

														//nlapiLogExecution('Debug', 'pfBinLocationgroupId', pfBinLocationgroupId);

														inventorySearchResults = getKitItemDetails(allLocnGroups, fulfilmentItem,allocationStrategy,
																pfBinLocationgroupId,lefoflag,samelot,latestexpirydt);

														var pfBinLocationId;
														var pickfaceArr=new Array();
														if((inventorySearchResults!=null && inventorySearchResults!='') || (pfLocationResults!=null && pfLocationResults!=''))
														{												

															if (pfLocationResults != null && pfLocationResults.length > 0)
															{
																for(var z = 0; z < pfLocationResults.length; z++)
																{
																	if(parseInt(remainingQuantity) > 0)
																	{
																		var pfItem = pfLocationResults[z].getValue('custrecord_pickfacesku');
																		var Autoreplenflag=pfLocationResults[z].getValue('custrecord_autoreplen');
																		if (pfItem == fulfilmentItem)
																		{
																			//nlapiLogExecution('Debug', 'item Match',pfItem);
																			pfBinLocationId = pfLocationResults[z].getValue('custrecord_pickbinloc');
																			pfMaximumQuantity = pfLocationResults[z].getValue('custrecord_maxqty');
																			pfMaximumPickQuantity = pfLocationResults[z].getValue('custrecord_maxpickqty');
																			pffixedlp = pfLocationResults[z].getText('custrecord_pickface_ebiz_lpno');
																			pfstatus=pfLocationResults[z].getValue('custrecord_pickface_itemstatus');
																			replenrule=pfLocationResults[z].getValue('custrecord_pickruleid');
																			pfRoundQty=pfLocationResults[z].getValue('custrecord_roundqty');
																			var pfWHLoc=pfLocationResults[z].getValue('custrecord_pickface_location');
																			var pfpickzoneid = pfLocationResults[z].getValue('custrecord_pickzone');

																			var str = 'pfBinLocationId. = ' + pfBinLocationId + '<br>';
																			str = str + 'pfMaximumQuantity. = ' + pfMaximumQuantity + '<br>';	
																			str = str + 'pfMaximumPickQuantity. = ' + pfMaximumPickQuantity + '<br>';
																			str = str + 'pffixedlp. = ' + pffixedlp + '<br>';
																			str = str + 'pfstatus. = ' + pfstatus + '<br>';
																			str = str + 'replenrule. = ' + replenrule + '<br>';
																			str = str + 'remainingQuantity. = ' + remainingQuantity + '<br>';
																			str = str + 'pfpickzoneid. = ' + pfpickzoneid + '<br>';
																			str = str + 'pfWHLoc. = ' + pfWHLoc + '<br>';
																			str = str + 'pfRoundQty. = ' + pfRoundQty + '<br>';

																			nlapiLogExecution('Debug', 'Pickface Details', str);

																			if((parseFloat(pfMaximumPickQuantity) >= parseFloat(remainingQuantity)) 
																					|| (uompackflag!="1" && uompackflag!="3"))
																			{

																				var availableQty ="";
																				var allocQty = "";
																				var recordId = "";

																				var itemStatus = "";
																				var fromLP = "";
																				var inventoryItem="";
																				var fromLot="";
																				// Get inventory details for this pickface location
																				var binLocnInvDtls = getAvailQtyFromInventory(inventorySearchResults, pfBinLocationId, allocatedInv,
																						DOwhLocation,fulfilmentItem,fulfillmentItemStatus,samelot,latestexpirydt,shelflife);

																				if(binLocnInvDtls!=null && binLocnInvDtls.length>0)
																				{
																					for(var pf=0;pf<binLocnInvDtls.length;pf++)
																					{
																						if(parseFloat(remainingQuantity) > 0) 
																						{
																							invtindex = binLocnInvDtls[pf][0];
																							availableQty = binLocnInvDtls[pf][2];
																							allocQty = binLocnInvDtls[pf][3];
																							recordId = binLocnInvDtls[pf][8];
																							if(binLocnInvDtls[pf][4]!=null && binLocnInvDtls[pf][4]!='')
																								packCode = binLocnInvDtls[pf][4];
																							itemStatus = binLocnInvDtls[pf][5];										
																							whLocation = binLocnInvDtls[pf][6];
																							fromLP = binLocnInvDtls[pf][7];											
																							inventoryItem = binLocnInvDtls[pf][9];
																							fromLot = binLocnInvDtls[pf][10];


																							/*
																							 * If the allocation strategy is not null check in inventory for that item.						 
																							 * 		if the allocation strategy = min quantity on the pallet
																							 * 			search for the inventory where the inventory has the least quantity
																							 * 		else
																							 * 			search for the inventory where the inventory has the maximum quantity 
																							 * 			on the pallet 
																							 */

																							var str = 'Inventory Item Status. = ' + itemStatus + '<br>';
																							str = str + 'Fulfillment Item Status. = ' + fulfillmentItemStatus + '<br>';	
																							str = str + 'Available Qty. = ' + availableQty + '<br>';	
																							str = str + 'remainingQuantity. = ' + remainingQuantity + '<br>';

																							nlapiLogExecution('Debug', 'PF - Item Status', str);

																							if(parseInt(availableQty) > 0 && itemStatus== fulfillmentItemStatus)
																							{
																								nlapiLogExecution('Debug', 'remainingQuantity', remainingQuantity);
																								// allocate to this bin location
																								var actualAllocQty = Math.min(parseInt(availableQty), parseInt(remainingQuantity));
																								var newAllocQty = parseInt(allocQty) + parseInt(actualAllocQty);
																								var expectedQuantity = parseInt(actualAllocQty);

																								var contsize="";								
																								var totalweight = parseFloat((expectedQuantity*uomweight))/parseFloat(uomqty);
																								var totalcube = parseFloat((expectedQuantity*uomcube))/parseFloat(uomqty);

																								// Allocate the quantity to the pickface location if the allocation quantity is 
																								// 	less than or equal to the pick face maximum quantity
																								nlapiLogExecution('Debug', 'actualAllocQty', actualAllocQty);
																								nlapiLogExecution('Debug', 'pfMaximumPickQuantity', pfMaximumPickQuantity);
																								if(actualAllocQty <= pfMaximumPickQuantity)
																								{
																									//1-Ship Cartons	2-Build Cartons		3-Ship Pallets																									
																									if(((uompackflag=="1" || uompackflag=="3") &&(parseInt(expectedQuantity) == parseInt(uomqty))) || (dimsbulkpick=='T'))
																									{
																										pfcontLP = ordercarton;

																										for(l=0;l<containerslist.length;l++){     

																											var containername = containerslist[l].getValue('custrecord_containername');
																											var vcontainerSite=containerslist[l].getValue('custrecord_ebizsitecontainer');
																											//if(containername=='SHIPASIS')
																											if(containername=='SHIPASIS'&&(vcontainerSite==DOwhLocation))
																											{
																												contsize = containerslist[l].getValue('Internalid');
																												pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																												CreateMasterLPRecordBulk(pfcontLP,contsize,totalweight,totalcube,uompackflag,DOwhLocation,LPparent);
																											}
																										}

																										nlapiLogExecution('DEBUG', 'Container Size in Pick Face : ', contsize);

																									}
																									else if(vShipByItself == "T" && (parseInt(fulfilmentQuantity) == parseInt(1)) && vShipAloneRULE=="Y"
																										&& (fulfillOrderList.length == 1)) //(parseInt(solinecount)==1))
																									{
																										pfcontLP = ordercarton;

																										for(l=0;l<containerslist.length;l++){     

																											var containername = containerslist[l].getValue('custrecord_containername');
																											var vcontainerSite=containerslist[l].getValue('custrecord_ebizsitecontainer');
																											//if(containername=='SHIPASIS')
																											if(containername=='SHIPASIS'&&(vcontainerSite==DOwhLocation))
																											{
																												contsize = containerslist[l].getValue('Internalid');
																												pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																												CreateMasterLPRecordBulk(pfcontLP,contsize,totalweight,totalcube,uompackflag,DOwhLocation,LPparent);
																											}
																										}

																										nlapiLogExecution('Debug', 'Container Size in Pick Face : ', contsize);

																									}
																									else if(cartonizationmethod=='4')
																									{
																										toatlcartonwht=toatlcartonwht+totalweight;
																										toatlcartoncube=toatlcartoncube+totalcube;

																										//pfcontLP = ordercarton;
																									}

																									nlapiLogExecution('Debug', 'Container LP in Pick Face : ', pfcontLP);

																									nlapiLogExecution('Debug', 'Inventory Record Index in Pick Face : ', invtindex);
																									nlapiLogExecution('DEBUG', 'KitFailed1 : ', KitFailed);
																									if(KitFailed== false)
																									{
																										//updateInventoryRecord(inventorySearchResults[invtindex], actualAllocQty);
																										updateInventoryWithAllocation(recordId,actualAllocQty);
																										//createRecordInOpenTask
																										createpicktask(fulfillOrderList[i], expectedQuantity, eBizWaveNo, pfcontLP, 
																												pfBinLocationId, recordId, packCode, itemStatus, pickMethod, pfpickzoneid, pickStrategyId,
																												whLocation, fromLP,uomlevel,totalweight,totalcube,contsize,fromLot,'',wmsCarrier,fulfilmentItem,nsrefno,customer,dimsbulkpick,otparent,waveassignedto);

																										remainingQuantity = parseInt(remainingQuantity) - parseInt(actualAllocQty);
																										nlapiLogExecution('Debug', 'remainingQuantity', remainingQuantity);

																										//updateFulfilmentOrder
																										updateFulfilmentOrderline(fulfillOrderList[i], actualAllocQty, eBizWaveNo,foparent);

																										// Maintain the allocation temporarily
																										var allocatedInvRow = [pfBinLocationId, expectedQuantity,fromLP,inventoryItem,fromLot];
																										allocatedInv.push(allocatedInvRow);
																										pickfaceArr.push(pfBinLocationId);
																									}
																									else
																									{
																										vnotes='Insufficient Inventory for component items';
																										nlapiLogExecution('DEBUG', 'Creating Failed Task');
																										nlapiLogExecution('DEBUG', 'expectedQuantity',expectedQuantity);
																										//createRecordInOpenTask
																										createpicktask(fulfillOrderList[i], expectedQuantity, eBizWaveNo, "", "", "",
																												"", "", "", "", "", whLocation, "","","","","","",vnotes,"",fulfilmentItem,nsrefno,
																												otparent,waveassignedto);

																										//updateFulfilmentOrder
																										updateFulfilmentOrderline(fulfillOrderList[i], 0, eBizWaveNo,foparent);

																										nlapiLogExecution('DEBUG', 'Creating Failed Task','SUCCESS');	

																									}
																								}
																							}

																							totordqty=totordqty+remainingQuantity;
																							nlapiLogExecution('DEBUG','totordqty',totordqty);
																							nlapiLogExecution('DEBUG','totreplenqty',totreplenqty);
																							nlapiLogExecution('DEBUG','remainingQuantity',remainingQuantity);

																						}
																					}
																				}
																				nlapiLogExecution('Debug','totreplenqty',totreplenqty);
																				nlapiLogExecution('Debug','remainingQuantity',remainingQuantity);

																				if(remainingQuantity>0 && Autoreplenflag=='T')
																				{
																					if(totordqty==0 || totordqty>totreplenqty)
																					{
																						recordId = "";
																						fromLP = "";
																						nlapiLogExecution('DEBUG','availableQty',availableQty);

																						if(availableQty==null || availableQty=='')
																							availableQty=0;

																						var actualAllocQty = Math.min(parseInt(availableQty), parseInt(remainingQuantity));
																						var newAllocQty = parseInt(allocQty) + parseInt(actualAllocQty);
																						var expectedQuantity = parseInt(remainingQuantity);

																						//remainingQuantity = parseInt(remainingQuantity) - parseInt(actualAllocQty);
																						nlapiLogExecution('Debug','actualAllocQty',actualAllocQty);
																						nlapiLogExecution('Debug','EnterReplen','EnterReplen');
																						nlapiLogExecution('Debug','remainingQuantity',remainingQuantity);
																						nlapiLogExecution('Debug','uompackflag',uompackflag);
																						nlapiLogExecution('Debug','uomqty',uomqty);

																						var totalweight = parseFloat((expectedQuantity*uomweight))/parseFloat(uomqty);
																						var totalcube = parseFloat((expectedQuantity*uomcube))/parseFloat(uomqty);
																						if(((uompackflag=="1" || uompackflag=="3") &&(parseInt(expectedQuantity) == parseInt(uomqty))) || (dimsbulkpick=='T'))
																						{
																							pfcontLP = ordercarton;

																							for(l=0;l<containerslist.length;l++){     

																								var containername = containerslist[l].getValue('custrecord_containername');

																								var vcontainerSite=containerslist[l].getValue('custrecord_ebizsitecontainer');
																								//if(containername=='SHIPASIS')
																								if(containername=='SHIPASIS'&&(vcontainerSite==DOwhLocation))
																								{
																									contsize = containerslist[l].getValue('Internalid');
																									pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																									CreateMasterLPRecordBulk(pfcontLP,contsize,totalweight,totalcube,uompackflag,DOwhLocation,LPparent);
																								}
																							}

																							nlapiLogExecution('DEBUG', 'Container Size in Pick Face : ', contsize);

																						}
																						else if((vShipByItself == "T") && (parseInt(fulfilmentQuantity) == parseInt(1)) && vShipAloneRULE=="Y"
																							&& (fulfillOrderList.length == 1)) //(parseInt(solinecount)==1))
																						{
																							pfcontLP = ordercarton;

																							for(l=0;l<containerslist.length;l++){     

																								var containername = containerslist[l].getValue('custrecord_containername');
																								var vcontainerSite=containerslist[l].getValue('custrecord_ebizsitecontainer');
																								//if(containername=='SHIPASIS')
																								if(containername=='SHIPASIS'&&(vcontainerSite==DOwhLocation))
																								{
																									contsize = containerslist[l].getValue('Internalid');
																									pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																									CreateMasterLPRecordBulk(pfcontLP,contsize,totalweight,totalcube,uompackflag,DOwhLocation,LPparent);
																								}
																							}

																							nlapiLogExecution('Debug', 'Container Size in Pick Face : ', contsize);

																						}
																						else if(cartonizationmethod=='4')
																						{
																							toatlcartonwht=toatlcartonwht+totalweight;
																							toatlcartoncube=toatlcartoncube+totalcube;

																							//pfcontLP = ordercarton;
																						}
																						else
																						{
																							pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																						}

																						////Satish on 04-15-2013

																						var generateReplen='Y';

																						if(generateReplen!='Y')
																						{

																							var binLocnInvDtls2 = getAvailQtyForReplenishment(allkitinventorySearchResults, pfBinLocationId, allocatedInv,DOwhLocation,fulfilmentItem,fulfillmentItemStatus);
																							nlapiLogExecution('Debug', 'binLocnInvDtls2', binLocnInvDtls2);
																							var invtavailqty=0;
																							if(binLocnInvDtls2!=null && binLocnInvDtls2.length>0)
																							{
																								for (var h = 0; h < binLocnInvDtls2.length; h++){

																									invtavailqty = invtavailqty + binLocnInvDtls2[h][2];																						
																								}
																							}

																							nlapiLogExecution('Debug', 'invtavailqty', invtavailqty);
																							nlapiLogExecution('Debug', 'remainingQuantity', remainingQuantity);
																							//invtavailqty=0;
																							if(invtavailqty>remainingQuantity)
																							{
																								//createRecordInOpenTask
																								createpicktask(fulfillOrderList[i], expectedQuantity, eBizWaveNo, pfcontLP, 
																										pfBinLocationId, recordId, packCode, fulfillmentItemStatus, pickMethod, pfpickzoneid, pickStrategyId,
																										whLocation, fromLP,uomlevel,totalweight,totalcube,contsize,fromLot,'',wmsCarrier,fulfilmentItem,nsrefno,customer,dimsbulkpick,otparent,waveassignedto);

																								//updateFulfilmentOrder
																								updateFulfilmentOrderline(fulfillOrderList[i], remainingQuantity, eBizWaveNo,foparent);

																								remainingQuantity=0;
																							}//Satish on 04-15-2013
																						}
																						else
																						{
																							var replenItemArray =  new Array();
																							var currentRow = new Array();
																							var j = 0;
																							var idx = 0;
																							var putawatqty=0;
																							var pickqty=0;
																							var pickfaceQty=0;
																							var openreplensqty=0;

																							var serchreplensqty=getopenreplens(salesOrderNo,fulfilmentItem,pfBinLocationId,pfWHLoc);
																							if(serchreplensqty!=null && serchreplensqty!='')
																							{
																								//openreplensqty = serchreplensqty[0].getValue('custrecord_expe_qty');
																								openreplensqty = serchreplensqty[0].getValue('custrecord_expe_qty', null, 'sum');
																							}

																							var searchputawyqty=getOpenputawayqty(pfBinLocationId);
																							if(searchputawyqty!=null && searchputawyqty!='')
																							{
																								//putawatqty = searchputawyqty[0].getValue('custrecord_expe_qty');
																								putawatqty = searchputawyqty[0].getValue('custrecord_expe_qty', null, 'sum');
																							}
																							var searchpickyqty=getOpenPickqty(pfBinLocationId);
																							if(searchpickyqty!=null && searchpickyqty!='')
																							{
																								//pickqty = searchpickyqty[0].getValue('custrecord_expe_qty');
																								pickqty = searchpickyqty[0].getValue('custrecord_expe_qty', null, 'sum');
																							}

																							var searchpickfaceQty = getQOHForAllSKUsinPFLocation(pfBinLocationId,fulfilmentItem,pfWHLoc);
																							if(searchpickfaceQty!=null && searchpickfaceQty!='')
																							{
																								//pickfaceQty=searchpickfaceQty[0].getValue('custrecord_ebiz_qoh');
																								pickfaceQty=searchpickfaceQty[0].getValue('custrecord_ebiz_qoh',null,'sum');
																							}
																							var replenQty=pfLocationResults[z].getValue('custrecord_replenqty');

																							if(pfRoundQty == null || pfRoundQty == '' || isNaN(pfRoundQty))
																								pfRoundQty=1;

																							if(putawatqty == null || putawatqty == '')
																								putawatqty=0;

																							if(pickqty == null || pickqty == '')
																								pickqty=0;

																							if(openreplensqty == null || openreplensqty == '')
																								openreplensqty=0;

																							if(pickfaceQty == null || pickfaceQty == '')
																								pickfaceQty=0;


																							if(parseInt(pickfaceQty)<0)
																								pickfaceQty=0;

																							nlapiLogExecution('Debug','putawatqty',putawatqty);
																							nlapiLogExecution('Debug','pickqty',pickqty);
																							nlapiLogExecution('Debug','pickfaceQty',pickfaceQty);
																							nlapiLogExecution('Debug','replenQty',replenQty);

																							var remQtyToCreate = parseInt(pfMaximumQuantity) - parseInt(pickfaceQty);
																							nlapiLogExecution('Debug','remQtyToCreate',remQtyToCreate);

																							var replenTaskCount = Math.ceil(parseInt(remQtyToCreate) / parseInt(replenQty));
																							nlapiLogExecution('Debug','replenTaskCount',replenTaskCount);
																							var qtyToFind = parseInt(replenTaskCount) * parseInt(replenQty);
																							nlapiLogExecution('Debug','qtyToFind',qtyToFind);

//																							var tempQty = (parseInt(pfMaximumQuantity) - (parseInt(qtyToFind) + parseInt(pickfaceQty) - parseInt(pickqty) + parseInt(putawatqty)));

//																							nlapiLogExecution('Debug','tempQty',tempQty);

																							var tempQty = parseInt(pfMaximumQuantity) -  (parseInt(openreplensqty) + parseInt(putawatqty) + parseInt(pickfaceQty) - parseInt(pickqty));

																							nlapiLogExecution('Debug','tempQty1',tempQty);

																							var tempwithRoundQty = Math.floor(parseFloat(tempQty)/parseFloat(pfRoundQty));

																							if(tempwithRoundQty == null || tempwithRoundQty == '' || isNaN(tempwithRoundQty))
																								tempwithRoundQty=0;

																							nlapiLogExecution('Debug','tempwithRoundQty',tempwithRoundQty);

																							tempQty = parseInt(pfRoundQty)*parseInt(tempwithRoundQty);


																							currentRow = [pfItem, replenQty, replenrule, pfMaximumQuantity, pfMaximumPickQuantity, pickfaceQty, whLocation, 
																							              pfBinLocationId, tempQty, replenTaskCount, pffixedlp,pfstatus,fulfillmentItemStatus,pfRoundQty];
																							if(idx == 0)
																								idx = currentRow.length;

																							replenItemArray[j++] = currentRow;
																							var taskCount;
																							var zonesArray = new Array();

																							var replenZoneList = "";
																							var replenZoneTextList = "";
																							var replenzonestatuslist="";
																							var replenzonestatustextlist="";
																							//var Reportno = "";

																							if (replenRulesList != null){
																								for (var w1 = 0; w1 < replenRulesList.length; w1++){

																									if (w1 == 0){
																										replenZoneList += replenRulesList[w1].getValue('custrecord_replen_strategy_zone');
																										replenZoneTextList += replenRulesList[w1].getText('custrecord_replen_strategy_zone');
																										replenzonestatuslist +=replenRulesList[w1].getValue('custrecord_replen_strategy_itemstatus');
																										replenzonestatustextlist +=replenRulesList[w1].getText('custrecord_replen_strategy_itemstatus');
																									} else {
																										replenZoneList += ',';
																										replenZoneList += replenRulesList[w1].getValue('custrecord_replen_strategy_zone');

																										replenZoneTextList += ',';
																										replenZoneTextList += replenRulesList[w1].getText('custrecord_replen_strategy_zone');

																										replenzonestatuslist += ',';
																										replenzonestatuslist += replenRulesList[w1].getValue('custrecord_replen_strategy_itemstatus');

																										replenzonestatustextlist += ',';
																										replenzonestatustextlist += replenRulesList[w1].getText('custrecord_replen_strategy_itemstatus');
																									}
																								}
																							} 

																							nlapiLogExecution('Debug','replenZoneList',replenZoneList);

																							var itemList = buildItemListForReplenProcessing(replenItemArray, j);
																							nlapiLogExecution('Debug','replen Item List',itemList);

																							var locnGroupList = getZoneLocnGroupsList(replenZoneList);	
																							nlapiLogExecution('Debug','Location Group List',locnGroupList);

																							var ZoneAndLocationGrp =getZoneLocnGroupsListforRepln(replenZoneList,whLocation);
																							nlapiLogExecution('DEBUG','ZoneAndLocationGrp2',ZoneAndLocationGrp);

																							var inventorySearchResultsReplen = getItemDetailsforRepln(locnGroupList, itemList,replenzonestatuslist,whLocation,lefoflag,fulfillmentItemStatus);
																							nlapiLogExecution('DEBUG','inventorySearchResultsReplen',inventorySearchResultsReplen);
																							if ((inventorySearchResultsReplen != null && inventorySearchResultsReplen != "")|| (remainingQuantity<=openreplensqty)){
																								nlapiLogExecution('DEBUG','test2','test2');
																								// Get the report number
																								if (Reportno == null || Reportno == "") {
																									Reportno = GetMaxTransactionNo('REPORT');
																									Reportno = Reportno.toString();
																								}

																								/*
																								 * Loop through the total task count and get the location from where the item has to be 
																								 * replenished.
																								 */

																								//Get the Stage Location in case of two step replenishment process
																								var stageLocation='';
																								nlapiLogExecution('Debug','Reportno',Reportno);
																								nlapiLogExecution('Debug','fulfilmentOrderNo',fulfilmentOrderNo);
																								nlapiLogExecution('Debug','replenQty',replenQty);
																								nlapiLogExecution('Debug','openreplensqty',openreplensqty);

																								var resultArray=replenRuleZoneLocationForAutoRepln(inventorySearchResultsReplen, replenItemArray, Reportno, stageLocation, whLocation,fulfilmentOrderNo,null,null,expectedQuantity,pickfaceArr,ZoneAndLocationGrp);
																								var result=resultArray[0];
																								if(result==true)
																								{
																									var vREMQtyafterReplnGenerated=resultArray[1];
																									var vActualReplnQtyGenerated=resultArray[2];
																									nlapiLogExecution('DEBUG','vActualReplnQtyGenerated',vActualReplnQtyGenerated);
																									nlapiLogExecution('DEBUG','fulfillOrderList',fulfillOrderList[i]);
																									nlapiLogExecution('DEBUG', 'KitFailed2 : ', KitFailed);


																									if(inventorySearchResultsReplen!=null && inventorySearchResultsReplen!='')
																									{
																										fromLot=inventorySearchResultsReplen[0].getText('custrecord_ebiz_inv_lot');	
																										nlapiLogExecution('DEBUG','test the id',inventorySearchResultsReplen[0].getId());
																									}

																									if(KitFailed== false)
																									{
																										var test=fulfillOrderList[i][6];

																										nlapiLogExecution('Debug','test',test);
																										//createRecordInOpenTask
																										createpicktask(fulfillOrderList[i], vActualReplnQtyGenerated, eBizWaveNo, pfcontLP, 
																												pfBinLocationId, recordId, packCode, itemStatus, pickMethod, pickZone, pickStrategyId,
																												whLocation, fromLP,uomlevel,totalweight,totalcube,contsize,fromLot,'',wmsCarrier,fulfilmentItem,nsrefno,customer,dimsbulkpick,otparent,waveassignedto);

																										//updateFulfilmentOrder
																										updateFulfilmentOrderline(fulfillOrderList[i], remainingQuantity, eBizWaveNo,foparent);
																										pickfaceArr.push(pfBinLocationId);
																									}
																									else
																									{
																										vnotes='Insufficient Inventory for component items';
																										nlapiLogExecution('DEBUG', 'Creating Failed Task');
																										nlapiLogExecution('DEBUG', 'expectedQuantity',expectedQuantity);
																										//createRecordInOpenTask
																										createpicktask(fulfillOrderList[i], expectedQuantity, eBizWaveNo, "", "", "",
																												"", "", "", "", "", whLocation, "","","","","","",vnotes,"",fulfilmentItem,nsrefno,otparent,waveassignedto);

																										//updateFulfilmentOrder
																										updateFulfilmentOrderline(fulfillOrderList[i], 0, eBizWaveNo,foparent);
																										nlapiLogExecution('DEBUG', 'Creating Failed Task','SUCCESS');
																									}
																									if(remainingQuantity>replenQty)
																									{
																										remainingQuantity=parseInt(remainingQuantity) - parseInt(replenQty);
																									}
																									else
																									{
																										remainingQuantity=vREMQtyafterReplnGenerated;
																									}
																									nlapiLogExecution('DEBUG', 'remainingQuantity : ', remainingQuantity);
																								}
																							}
																							totreplenqty=totreplenqty+replenQty;
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}

															if(inventorySearchResults != null && inventorySearchResults.length > 0)
															{
																nlapiLogExecution('Debug','inventorySearchResults',inventorySearchResults.length);

																for (var j = 0; j < inventorySearchResults.length; j++)
																{
																	//nlapiLogExecution('Debug','j',j);

																	inventoryItem = inventorySearchResults[j].getValue('custrecord_ebiz_inv_sku');
																	var InvWHLocation = inventorySearchResults[j].getValue('custrecord_ebiz_inv_loc');
//																	nlapiLogExecution('Debug','remainingQuantity',remainingQuantity);
//																	nlapiLogExecution('Debug','inventoryItem',inventoryItem);
//																	nlapiLogExecution('Debug','fulfilmentItem',fulfilmentItem);

																	if ((DOwhLocation==InvWHLocation)&&(inventoryItem == fulfilmentItem) && (parseInt(remainingQuantity) > 0))
																	{
																		actualQty = inventorySearchResults[j].getValue('custrecord_ebiz_qoh');
																		inventoryAvailableQuantity = inventorySearchResults[j].getValue('custrecord_ebiz_avl_qty');
																		allocationQuantity = inventorySearchResults[j].getValue('custrecord_ebiz_alloc_qty');
																		recordId = inventorySearchResults[j].getId();
																		beginLocationId = inventorySearchResults[j].getValue('custrecord_ebiz_inv_binloc');
																		if(inventorySearchResults[j].getValue('custrecord_ebiz_inv_packcode')!=null && inventorySearchResults[j].getValue('custrecord_ebiz_inv_packcode')!='')
																			packCode = inventorySearchResults[j].getValue('custrecord_ebiz_inv_packcode');
																		itemStatus = inventorySearchResults[j].getValue('custrecord_ebiz_inv_sku_status');
																		whLocation = inventorySearchResults[j].getValue('custrecord_ebiz_inv_loc');
																		fromLP = inventorySearchResults[j].getValue('custrecord_ebiz_inv_lp');
																		fromLot = inventorySearchResults[j].getText('custrecord_ebiz_inv_lot');
																		//nlapiLogExecution('Debug', 'fromLot If : ', fromLot);


																		// Get already allocated inv
																		var alreadyAllocQty = getAlreadyAllocatedInv(allocatedInv, beginLocationId,fromLP,inventoryItem,fromLot);

																		//nlapiLogExecution('Debug', 'alreadyAllocQty : ', alreadyAllocQty);

																		// Adjust for already allocated inv

																		//inventoryAvailableQuantity = parseInt(inventoryAvailableQuantity) - parseInt(alreadyAllocQty); 

																		var str = 'Inventory Item Status. = ' + itemStatus + '<br>';
																		str = str + 'Fulfillment Item Status. = ' + fulfillmentItemStatus + '<br>';	
																		str = str + 'already AllocQty. = ' + alreadyAllocQty + '<br>';	
																		str = str + 'inventory Available Quantity. = ' + inventoryAvailableQuantity + '<br>';	
																		str = str + 'remaining Quantity. = ' + remainingQuantity + '<br>';	
																		str = str + 'beginLocationId. = ' + beginLocationId + '<br>';	
																		str = str + 'pfBinLocationId. = ' + pfBinLocationId + '<br>';	
																		str = str + 'pickZone. = ' + pickZone + '<br>';	
																		str = str + 'pickStrategyId. = ' + pickStrategyId + '<br>';	
																		str = str + 'pickfaceArr. = ' + pickfaceArr + '<br>';
																		nlapiLogExecution('DEBUG', 'Inventory Details', str);
																		var vPickfaceAllocLoc=false;
																		if(pickfaceArr != null && pickfaceArr != '')
																		{
																			if(pickfaceArr.indexOf(beginLocationId)!=-1)
																				vPickfaceAllocLoc=true;
																		}	
																		if(vPickfaceAllocLoc == false)
																		{
																			if(parseInt(inventoryAvailableQuantity) > 0 && itemStatus==fulfillmentItemStatus  && (beginLocationId!=pfBinLocationId)){
																				var actualAllocationQuantity = Math.min(parseInt(inventoryAvailableQuantity), parseInt(remainingQuantity));
																				allocationQuantity = parseInt(alreadyAllocQty) + parseInt(actualAllocationQuantity);
																				remainingQuantity = parseInt(remainingQuantity) - parseInt(actualAllocationQuantity);
																				var expectedQuantity = parseInt(actualAllocationQuantity);

																				var contsize="";	

																				var totalweight = parseFloat((expectedQuantity*uomweight))/parseFloat(uomqty);
																				var totalcube = parseFloat((expectedQuantity*uomcube))/parseFloat(uomqty);

																				var str = 'uompackflag. = ' + uompackflag + '<br>';
																				str = str + 'uomqty. = ' + uomqty + '<br>';	
																				str = str + 'expectedQuantity. = ' + expectedQuantity + '<br>';	
																				str = str + 'allocationQuantity. = ' + allocationQuantity + '<br>';	
																				str = str + 'cartonizationmethod. = ' + cartonizationmethod + '<br>';	

																				nlapiLogExecution('Debug', 'Qty Details', str);

																				//1-Ship Cartons	2-Build Cartons		3-Ship Pallets
																				if(((uompackflag=="1" || uompackflag=="3") &&(parseInt(expectedQuantity) == parseInt(uomqty))) || (dimsbulkpick=='T'))
																				{
																					invtcontLP = ordercarton;

																					for(l=0;l<containerslist.length;l++)
																					{                                         
																						var containername = containerslist[l].getValue('custrecord_containername');
																						var vcontainerSite=containerslist[l].getValue('custrecord_ebizsitecontainer');
																						//if(containername=='SHIPASIS')
																						if(containername=='SHIPASIS'&&(vcontainerSite==DOwhLocation))
																						{
																							contsize = containerslist[l].getValue('Internalid');
																							invtcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																							CreateMasterLPRecordBulk(invtcontLP,contsize,totalweight,totalcube,uompackflag,DOwhLocation,LPparent);
																						}
																					}													
																				}
																				else if(vShipByItself == "T" && (parseInt(fulfilmentQuantity) == parseInt(1)) && vShipAloneRULE=="Y"
																					&& (fulfillOrderList.length == 1)) //(parseInt(solinecount)==1))
																				{
																					invtcontLP = ordercarton;

																					for(l=0;l<containerslist.length;l++)
																					{                                         
																						var containername = containerslist[l].getValue('custrecord_containername');
																						var vcontainerSite=containerslist[l].getValue('custrecord_ebizsitecontainer');
																						//if(containername=='SHIPASIS')
																						if(containername=='SHIPASIS'&&(vcontainerSite==DOwhLocation))
																						{
																							contsize = containerslist[l].getValue('Internalid');
																							invtcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																							CreateMasterLPRecordBulk(invtcontLP,contsize,totalweight,totalcube,uompackflag,DOwhLocation,LPparent);
																						}
																					}													
																				}
																				else if(cartonizationmethod=='4') 
																				{
																					//invtcontLP = ordercarton;

																					toatlcartonwht=toatlcartonwht+totalweight;
																					toatlcartoncube=toatlcartoncube+totalcube;
																				}

																				nlapiLogExecution('DEBUG', 'Container LP in Inventory : ', invtcontLP);
																				nlapiLogExecution('DEBUG', 'KitFailed3 : ', KitFailed);
																				if(KitFailed== false)
																				{
																					//updateInventoryRecord(inventorySearchResults[j], actualAllocationQuantity);
																					updateInventoryWithAllocation(recordId,actualAllocationQuantity);
																					//createRecordInOpenTask
																					createpicktask(fulfillOrderList[i], expectedQuantity, eBizWaveNo, invtcontLP, beginLocationId, recordId,
																							packCode, itemStatus, pickMethod, pickZone, pickStrategyId, whLocation, fromLP,uomlevel,
																							totalweight,totalcube,contsize,fromLot,'',wmsCarrier,fulfilmentItem,nsrefno,customer,dimsbulkpick,otparent,waveassignedto);

																					//updateFulfilmentOrder
																					updateFulfilmentOrderline(fulfillOrderList[i], actualAllocationQuantity, eBizWaveNo,foparent);


																					// Maintain the allocation temporarily
																					var allocatedInvRow = [beginLocationId, expectedQuantity,fromLP,inventoryItem,fromLot];
																					allocatedInv.push(allocatedInvRow);
																				}
																				else
																				{
																					vnotes='Insufficient Inventory for component items';
																					nlapiLogExecution('DEBUG', 'Creating Failed Task');
																					nlapiLogExecution('DEBUG', 'expectedQuantity',expectedQuantity);
																					//createRecordInOpenTask
																					createpicktask(fulfillOrderList[i], expectedQuantity, eBizWaveNo, "", "", "",
																							"", "", "", "", "", whLocation, "",uomlevel,"","","","",vnotes,"",fulfilmentItem,nsrefno,otparent,waveassignedto);
																					//updateFulfilmentOrder
																					updateFulfilmentOrderline(fulfillOrderList[i], 0, eBizWaveNo,foparent);
																					nlapiLogExecution('DEBUG', 'Creating Failed Task','SUCCESS');
																				}

																			}
																		}
																	}
																}
															}
														}
													}
													//If there is still some quantity remaining, then insert a failed task
													if(parseInt(nooflps)>0 && parseInt(remainingQuantity) > 0)
													{
														vnotes='Insufficient Inventory';
														KitFailed= true;
														nlapiLogExecution('Debug', 'CallingFunction1', 'Sucess');
														//createRecordInOpenTask
														createpicktask(fulfillOrderList[i], remainingQuantity, eBizWaveNo, "", "", "",
																"", "", "", "", "", DOwhLocation, "",uomlevel,"","","","",vnotes,"",fulfilmentItem,nsrefno,customer,dimsbulkpick,otparent,waveassignedto);

														//updateFulfilmentOrder
														updateFulfilmentOrderline(fulfillOrderList[i], 0, eBizWaveNo,foparent);

														vBoolFailedForSameLP=true;
														remainingQuantity=0;
														nlapiLogExecution('DEBUG', 'KitFailed5 : ', KitFailed);

													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				else
				{
					var recId = fulfillOrderList[i][3];

					var fieldNames = new Array(); 				//fields to be updated
					fieldNames[1] = "custrecord_linestatus_flag";
					fieldNames[2] = "custrecord_linenotes1";

					var newValues = new Array(); 				//new field values
					newValues[1] =  '26';
					newValues[2] =  "ITEM#"+vItemInactive+" IS INACTIVE";

					nlapiSubmitField('customrecord_ebiznet_ordline', recId, fieldNames, newValues);

					vnotes='One of the Member Item is INACTIVE';
					nlapiLogExecution('DEBUG', 'CallingFunction END', vnotes);
					/*createRecordInOpenTask(fulfillOrderList[i], remainingQuantity, eBizWaveNo, "", "", "",
							"", "", "", "", "", DOwhLocation, "","","","","","",vnotes,"",fulfillOrderList[i][6],nsrefno,otparent);*/
				}
			}
			else
			{
				var currContext = nlapiGetContext();
				fulfilmentItem = fulfillOrderList[i][6];				// ITEM/SKU
				fulfilmentQuantity = fulfillOrderList[i][9];			// ORDER QUANTITY
				remainingQuantity = parseInt(fulfilmentQuantity);		
				fulfilmentOrderNo = fulfillOrderList[i][3]; 			// ORDER NO.
				salesOrderNo = fulfillOrderList[i][1]; 					// SALES ORDER INTERNAL ID.
				fulfilmentOrderLineNo = fulfillOrderList[i][2];			// FULFILMENT ORDER LINE NO.
				fulfillmentItemStatus=fulfillOrderList[i][7];			// FULFILMENT Item Status.
				DOwhLocation = fulfillOrderList[i][19];					// FULFILMENT WAREHOUSE LOCATION
				packCode=fulfillOrderList[i][10];						// PACKCODE
				wmsCarrier = fulfillOrderList[i][20];					// WMS CARRIER
				nsrefno = fulfillOrderList[i][21];						// ITEM FULFILLMENT REFERENCE #
				ordtype=fulfillOrderList[i][17];						// Order Type  	
				whLocation=fulfillOrderList[i][19];
				//lefoflag = fulfillOrderList[i][37];
				samelot = fulfillOrderList[i][39];
				shelflife = fulfillOrderList[i][40];
				customer = fulfillOrderList[i][35];
				lefoflag = "";
				var latestexpirydt = '';
				vfolotnumber=fulfillOrderList[i][42];
				vfolotqty=fulfillOrderList[i][43];
				vfolotinternalid=fulfillOrderList[i][44];

				var str = 'fulfilmentOrderNo. = ' + fulfilmentOrderNo + '<br>';
				str = str + 'fulfilmentQuantity. = ' + fulfilmentQuantity + '<br>';	
				str = str + 'fulfilmentItem. = ' + fulfilmentItem + '<br>';	
				str = str + 'packCode. = ' + packCode + '<br>';	
				str = str + 'fulfillmentItemStatus. = ' + fulfillmentItemStatus + '<br>';	
				str = str + 'fulfillment Warehouse Location. = ' + DOwhLocation + '<br>';
				str = str + 'wmsCarrier. = ' + wmsCarrier + '<br>';
				str = str + 'itemType. = ' + itemType + '<br>';
				str = str + 'lefoflag. = ' + lefoflag + '<br>';
				str = str + 'batchflag. = ' + batchflag + '<br>';
				str = str + 'samelot. = ' + samelot + '<br>';
				str = str + 'shelflife. = ' + shelflife + '<br>';
				str = str + 'vfolotnumber. = ' + vfolotnumber + '<br>';
				str = str + 'vfolotqty. = ' + vfolotqty + '<br>';
				str = str + 'vfolotinternalid. = ' + vfolotinternalid + '<br>';

				nlapiLogExecution('Debug', 'Fulfillment Ord Line Details', str);

				//case# 20149995 (if customer is empty then we are not calling getLatestExpiry function)
				//var customerlotdet = getLatestExpiry(customer,fulfilmentItem,whLocation);
				var customerlotdet="";
				if((itemType=='lotnumberedinventoryitem' || itemType=='lotnumberedassemblyitem' || batchflag=='T')
						&&(customer!=null && customer!='') && samelot=='T')
				{
					customerlotdet = getLatestExpiry(customer,fulfilmentItem,whLocation);
					//end
					if(customerlotdet!=null && customerlotdet!='')
					{
						latestexpirydt = customerlotdet[0].getValue('custrecord_ebiz_lot_expiry');
					}
				}
				nlapiLogExecution('Debug', 'Time Stamp Before calling pickgen_palletisation',TimeStampinSec());

				var arrLPRequired = pickgen_palletisation(fulfilmentItem,fulfilmentQuantity,itemdimsarr,whLocation);

				nlapiLogExecution('Debug', 'Time Stamp After calling pickgen_palletisation',TimeStampinSec());

				if(arrLPRequired==null || arrLPRequired==''||arrLPRequired.length==0)
				{
					vnotes='Missing Dimensions';
					nlapiLogExecution('Debug', 'CallingFunction1', 'Sucess');
					//createRecordInOpenTask
					createpicktask(fulfillOrderList[i], remainingQuantity, eBizWaveNo, "", "", "",
							"", "", "", "", "", DOwhLocation, "","","","","","",vnotes,"",fulfilmentItem,nsrefno,customer,dimsbulkpick,otparent,waveassignedto);

					//updateFulfilmentOrder
					updateFulfilmentOrderline(fulfillOrderList[i], 0, eBizWaveNo,foparent);

				}
				else
				{
					var nooflps = "";
					var uomlevel = "";
					var lpbrkqty = "";
					var uompackflag = "";
					var uomweight="";
					var uomcube="";
					var uomqty="";
					var maxzoneno=-1;
					var uom='';
					var baseuomid='';

//					nlapiLogExecution('Debug', 'Time Stamp Before calling getItemDetails',TimeStampinSec());

					/*
					if(allinventorySearchResults!=null && allinventorySearchResults!='' && allinventorySearchResults.length>=1000)
						allinventorySearchResults = getItemDetails(null, fulfilmentItem,null,null,lefoflag,
								null,null,null,samelot,latestexpirydt);
					 */

//					nlapiLogExecution('Debug', 'Time Stamp After calling getItemDetails',TimeStampinSec());

					for(s = 0;s < parseInt(arrLPRequired.length);s++)
					{
						nooflps = arrLPRequired[s][0];
						uomlevel = arrLPRequired[s][1];
						lpbrkqty = arrLPRequired[s][2];
						uompackflag = arrLPRequired[s][3];
						uomweight = arrLPRequired[s][4];
						uomcube= arrLPRequired[s][5];
						uomqty = arrLPRequired[s][6];
						dimsbulkpick = arrLPRequired[s][7];
						uom = arrLPRequired[s][8];
						baseuomid = arrLPRequired[s][9];

						remainingQuantity = parseFloat(lpbrkqty);
						//nlapiLogExecution('Debug', 'nooflps',nooflps);

						if(parseFloat(nooflps)>0)
						{

							nlapiLogExecution('Debug', 'Time Stamp Before calling getListForAllPickRules',TimeStampinSec());

							var allocConfigDtls1 = getListOfAllPickRules(fulfilmentItem,uomlevel,DOwhLocation,fulfillmentItemStatus,
									packCode,ordtype,pickruleslist,pflocationsarr,uom);

							nlapiLogExecution('Debug', 'Time Stamp After calling getListForAllPickRules',TimeStampinSec());


							if(allocConfigDtls1==null || allocConfigDtls1==''||allocConfigDtls1.length==0)
							{
								vnotes='Pick Strategy Error';
								nlapiLogExecution('Debug', 'CallingFunction1', 'Sucess');
								//createRecordInOpenTask
								createpicktask(fulfillOrderList[i], remainingQuantity, eBizWaveNo, "", "", "",
										"", "", "", "", "", DOwhLocation, "","","","","","",vnotes,"",fulfilmentItem,nsrefno,customer,dimsbulkpick,otparent,waveassignedto,baseuomid);

								//updateFulfilmentOrder
								updateFulfilmentOrderline(fulfillOrderList[i], 0, eBizWaveNo,foparent);
							}
							else
							{

								var vBoolFailedForSameLP=false;

								//nlapiLogExecution('Debug', 'nooflps', nooflps);

								for(t=1; t<=parseFloat(nooflps);t++)
								{
//									nlapiLogExecution('Debug', 't',t);
//									nlapiLogExecution('Debug', 'vBoolFailedForSameLP',vBoolFailedForSameLP);

									//nlapiLogExecution('Debug','Remaining usage at the start',currContext.getRemainingUsage());

//									var str = 'nooflps. = ' + nooflps + '<br>';
//									str = str + 'lp no. = ' + t + '<br>';	
//									str = str + 'lpbrkqty. = ' + lpbrkqty + '<br>';	
//									str = str + 'uomlevel. = ' + uomlevel + '<br>';
//									str = str + 'remainingQuantity. = ' + remainingQuantity + '<br>';
//									str = str + 'remainingUsage. = ' + currContext.getRemainingUsage() + '<br>';

//									nlapiLogExecution('Debug', 'Breakdown Details', str);

									if(remainingQuantity==null || remainingQuantity==''|| remainingQuantity==0)
										remainingQuantity=lpbrkqty;									

									if(vBoolFailedForSameLP ==true && t>1)
									{
										//If there is still some quantity remaining, then insert a failed task
										if(parseFloat(nooflps)>0 && parseFloat(remainingQuantity) > 0)
										{
//											vnotes='Insufficient Inventory';
//											nlapiLogExecution('Debug', 'Creating Failed Task');
//											nlapiLogExecution('Debug', 'remainingQuantity',remainingQuantity);
											//createRecordInOpenTask
											createpicktask(fulfillOrderList[i], remainingQuantity, eBizWaveNo, "", "", "",
													"", "", "", "", "", DOwhLocation, "",uomlevel,"","","","",vnotes,"",fulfilmentItem,nsrefno,customer,dimsbulkpick,otparent,waveassignedto,baseuomid);

											//updateFulfilmentOrder
											updateFulfilmentOrderline(fulfillOrderList[i], 0, eBizWaveNo,foparent);
											nlapiLogExecution('Debug', 'Creating Failed Task','SUCCESS');
											vBoolFailedForSameLP=true;
											remainingQuantity=0;

										}	
									}
									else
									{

										//nlapiLogExecution('Debug', 'remainingQuantity', remainingQuantity);

										var isForwardpickReplen='F';
										var pickZone='';
										var fields = ['custitem_ebiz_forwardpick'];
										var columns= nlapiLookupField('Item',fulfilmentItem,fields);
										var IsItemForwardPick = columns.custitem_ebiz_forwardpick;
										nlapiLogExecution('Error', 'item.forwardpick', IsItemForwardPick);


										if(parseFloat(remainingQuantity) > 0) 
										{
											if(IsItemForwardPick=='T')
											{

												//checking for the pickzone that has forward pick true.
												for(v = 0; v < parseFloat(allocConfigDtls1.length) && (pickZone ==''); v++)
												{

													var pfLocationResults = allocConfigDtls1[v][7];
													var pickMethod = allocConfigDtls1[v][2];
													//pickZone = allocConfigDtls1[v][3];
													var pickStrategyId = allocConfigDtls1[v][1];
													var allocationStrategy = allocConfigDtls1[v][6];
													var binlocid=allocConfigDtls1[v][5];
													var binlocgrpid=allocConfigDtls1[v][4];
													cartonizationmethod=allocConfigDtls1[v][12];
													var stagedetermination=allocConfigDtls1[v][13];

													var vforwardPick=allocConfigDtls1[v][14];
													if(vforwardPick=='T')
													{
														pickZone = allocConfigDtls1[v][3];
													}

													if(cartonizationmethod=="" && cartonizationmethod==null)
														cartonizationmethod='4';

													var str = 'pfLocationResults. = ' + pfLocationResults + '<br>';
													str = str + 'pickMethod. = ' + pickMethod + '<br>';	
													str = str + 'pickStrategyId. = ' + pickStrategyId + '<br>';
													str = str + 'pickZone. = ' + pickZone + '<br>';
													str = str + 'binlocgrpid. = ' + binlocgrpid + '<br>';
													str = str + 'binlocid. = ' + binlocid + '<br>';
													str = str + 'allocationStrategy. = ' + allocationStrategy + '<br>';
													str = str + 'cartonizationmethod. = ' + cartonizationmethod + '<br>';
													str = str + 'forwardPick. = ' + vforwardPick + '<br>';

													nlapiLogExecution('Error', 'Pick Strategy Details', str);

												}

												//nlapiLogExecution('DEBUG', 'pivkmethod.forwardpick', vforwardPick);
												if(IsItemForwardPick!=null && IsItemForwardPick!='' && IsItemForwardPick!='null' && IsItemForwardPick=='T' && vforwardPick!=null && vforwardPick!='' && vforwardPick!='null' &&  vforwardPick=='T')
												{

													//nlapiLogExecution('DEBUG', 'whLocation@123', whLocation);
													var filters = new Array();
													filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
													if(whLocation!=null && whLocation!='')
														filters.push(new nlobjSearchFilter('custrecord_replen_location', null, 'anyof', whLocation));

													nlapiLogExecution('DEBUG', 'custrecord_ebiz_forwardpick_replen', 'T');
													//filters.push(new nlobjSearchFilter('custrecord_ebiz_forwardpick_replen', null, 'is', 'T'));

													var columns = new Array();
													columns[0] = new nlobjSearchColumn('custrecord_replen_strategy_method');
													columns[1] = new nlobjSearchColumn('custrecord_replen_strategy_zone');
													columns[2] = new nlobjSearchColumn('custrecord_replen_strategy_itemstatus');
													columns[3] = new nlobjSearchColumn('custrecord_ebiz_forwardpick_replen');

													var replenRulesList = new nlapiSearchRecord('customrecord_ebiznet_replenish_strategy', null, filters, columns);

													var replenZoneList = "";
													var replenZoneTextList = "";
													var replenzonestatuslist="";
													var replenzonestatustextlist="";
													//var Reportno = "";

													if (replenRulesList != null){
														var rznidx=0;
														for (var w = 0; w < replenRulesList.length; w++){
															isForwardpickReplen=replenRulesList[w].getValue('custrecord_ebiz_forwardpick_replen');
															nlapiLogExecution('DEBUG', 'isForwardpickReplen', isForwardpickReplen);
															if(isForwardpickReplen=='T')
															{
																if (w == 0){
																	replenZoneList += replenRulesList[w].getValue('custrecord_replen_strategy_zone');
																	replenZoneTextList += replenRulesList[w].getText('custrecord_replen_strategy_zone');
																	replenzonestatuslist +=replenRulesList[w].getValue('custrecord_replen_strategy_itemstatus');
																	replenzonestatustextlist +=replenRulesList[w].getText('custrecord_replen_strategy_itemstatus');
																	rznidx++;
																} else {
																	replenZoneList += ',';
																	replenZoneList += replenRulesList[w].getValue('custrecord_replen_strategy_zone');

																	replenZoneTextList += ',';
																	replenZoneTextList += replenRulesList[w].getText('custrecord_replen_strategy_zone');

																	replenzonestatuslist += ',';
																	replenzonestatuslist += replenRulesList[w].getValue('custrecord_replen_strategy_itemstatus');

																	replenzonestatustextlist += ',';
																	replenzonestatustextlist += replenRulesList[w].getText('custrecord_replen_strategy_itemstatus');
																}
															}
														}
													} 

													//nlapiLogExecution('DEBUG','replenZoneList',replenZoneList);

													var itemList = new Array();
													itemList.push(fulfilmentItem);//buildItemListForReplenProcessing(replenItemArray, j);
													//nlapiLogExecution('DEBUG','replen Item List',itemList);

													var vlocnGroupList = getZoneLocnGroupsList(replenZoneList);

													var TolocnGroupList = getZoneLocnGroupsList(pickZone);	
													nlapiLogExecution('DEBUG','Location Group List',TolocnGroupList);

													var inventorySearchResultsReplen = getItemDetailsforRepln(vlocnGroupList, itemList,replenzonestatuslist,whLocation,lefoflag,fulfillmentItemStatus);
													nlapiLogExecution('DEBUG','inventorySearchResultsReplen',inventorySearchResultsReplen);

													if ((inventorySearchResultsReplen != null && inventorySearchResultsReplen != "") ){
														nlapiLogExecution('DEBUG','test2','test2');
														// Get the report number
														if (Reportno == null || Reportno == "") {
															Reportno = GetMaxTransactionNo('REPORT');
															Reportno = Reportno.toString();
														}

														/*
														 * Loop through the total task count and get the location from where the item has to be 
														 * replenished.
														 */

														//Get the Stage Location in case of two step replenishment process
														var stageLocation='';
														//GetStageLocation(item, "", whLocation, "", "BOTH");
//														nlapiLogExecution('DEBUG','Reportno',Reportno);
//														nlapiLogExecution('DEBUG','fulfilmentOrderNo',fulfilmentOrderNo);
//														nlapiLogExecution('DEBUG','replenQty',replenQty);
//														nlapiLogExecution('DEBUG','openreplensqty',openreplensqty);
//														nlapiLogExecution('DEBUG','remainingQuantity',remainingQuantity);
														//fetching the inventory results of the fetched pick zone for getting the empty bin locations
														var invtResults=getInventorySearchResults(-1,TolocnGroupList);
														//nlapiLogExecution('DEBUG','invtResults',invtResults);

														var filters1 = new Array();										
														filters1.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
														if(whLocation!=null && whLocation!='')
															filters1.push(new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'is', whLocation));
														if(invtResults!=null && invtResults!='')
															filters1.push(new nlobjSearchFilter('internalid',null,'noneof',invtResults));

														if(TolocnGroupList!=null && TolocnGroupList!="" && TolocnGroupList!=","){	
															filters1.push(new nlobjSearchFilter('custrecord_outboundlocgroupid',null, 'anyof', TolocnGroupList));
														}
														nlapiLogExecution('DEBUG', 'TolocnGroupList', TolocnGroupList);																
														//}

														//var replenItemArray1=new Array();	

														var columns = new Array();
														columns[0] = new nlobjSearchColumn('custrecord_outboundlocgroupid');
														columns[1] = new nlobjSearchColumn('name');
														columns[0].setSort();

														// Specify the sort sequence

														var binlocationSearchResults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters1, columns);
														var endLocationId='';
														nlapiLogExecution('DEBUG', 'binlocationSearchResults', binlocationSearchResults);	
														if(binlocationSearchResults!=null && binlocationSearchResults!='')
														{
															nlapiLogExecution('DEBUG', 'binlocationSearchResults', binlocationSearchResults.length);	
															for(var l1=0;l1<binlocationSearchResults.length;l1++)
															{
																tempendLocationId=binlocationSearchResults[l1].getId();
																//checking for any open replens for the fetched bin
																var serchreplensqty=getopenreplensNew(salesOrderNo,fulfilmentItem,tempendLocationId,whLocation);
																nlapiLogExecution('DEBUG', 'serchreplensqty', serchreplensqty);
																if(serchreplensqty!=null && serchreplensqty!='')
																{
																	continue;
																}
																else
																{
																	endLocationId=tempendLocationId;
																	break;
																}
															}
														}
													}
													var replenItemArray1=new Array();	
													/*if(endLocationId!='')
                                                     {*/

													currentRow = [fulfilmentItem,endLocationId,remainingQuantity];


													replenItemArray1[0] = currentRow;


													var result=	pickRuleZoneLocation(inventorySearchResultsReplen, replenItemArray1, Reportno, stageLocation, 
															whLocation,fulfilmentOrderNo,eBizWaveNo);


													if(result==true)
													{

														nlapiLogExecution('DEBUG','fulfillOrderList',fulfillOrderList[i]);
														var test=fulfillOrderList[i][6];

														if(inventorySearchResultsReplen!=null && inventorySearchResultsReplen!='')
														{
															fromLot=inventorySearchResultsReplen[0].getText('custrecord_ebiz_inv_lot');	
															nlapiLogExecution('DEBUG','test the id',inventorySearchResultsReplen[0].getId());
														}

														nlapiLogExecution('DEBUG','test',test);
														recordId="";
														if(fromLP==null || fromLP=='null' || fromLP=='undefined' || fromLP=='')
														{
															fromLP='';
														}
														if(fromLot==null || fromLot=='null' || fromLot=='undefined' || fromLot=='')
														{
															fromLot='';
														}
														//createRecordInOpenTask
														createpicktask(fulfillOrderList[i], remainingQuantity, eBizWaveNo, pfcontLP, 
																endLocationId, recordId, packCode, fulfillmentItemStatus, pickMethod, '', pickStrategyId,
																whLocation, fromLP,uomlevel,totalweight,totalcube,contsize,fromLot,'',wmsCarrier,fulfilmentItem,
																nsrefno,customer,dimsbulkpick,otparent,waveassignedto,baseuomid);

														if(endLocationId !=null && endLocationId !='')
														{
															//updateFulfilmentOrder
															updateFulfilmentOrderline(fulfillOrderList[i], remainingQuantity, eBizWaveNo,foparent);	
														}
														else
														{
															// if enlocation id is empty system generate failed picks, so need to update WMS flag to 'Failed picks' in FO
															//updateFulfilmentOrder
															updateFulfilmentOrderline(fulfillOrderList[i], 0, eBizWaveNo,foparent);	
														}

														remainingQuantity=0;
													}
													//}
												}

											}
											if(isForwardpickReplen=='F')
											{
												for(v = 0; v < parseFloat(allocConfigDtls1.length) && parseFloat(remainingQuantity) > 0; v++)
												{

													var pfLocationResults = allocConfigDtls1[v][7];
													var pickMethod = allocConfigDtls1[v][2];
													var pickZone = allocConfigDtls1[v][3];
													var pickStrategyId = allocConfigDtls1[v][1];
													var allocationStrategy = allocConfigDtls1[v][6];
													var binlocid=allocConfigDtls1[v][5];
													var binlocgrpid=allocConfigDtls1[v][4];
													cartonizationmethod=allocConfigDtls1[v][12];
													var stagedetermination=allocConfigDtls1[v][13];

													if(cartonizationmethod=="" || cartonizationmethod==null)
														cartonizationmethod='4';

//													var str = 'pfLocationResults. = ' + pfLocationResults + '<br>';
//													str = str + 'pickMethod. = ' + pickMethod + '<br>';	
//													str = str + 'pickStrategyId. = ' + pickStrategyId + '<br>';
//													str = str + 'pickZone. = ' + pickZone + '<br>';
//													str = str + 'binlocgrpid. = ' + binlocgrpid + '<br>';
//													str = str + 'binlocid. = ' + binlocid + '<br>';
//													str = str + 'allocationStrategy. = ' + allocationStrategy + '<br>';
//													str = str + 'cartonizationmethod. = ' + cartonizationmethod + '<br>';
//													str = str + 'remainingUsage. = ' + currContext.getRemainingUsage() + '<br>';

													//nlapiLogExecution('Debug', 'Pick Strategy Details', str);

													//var allLocnGroups = getAllLocnGroups(allocConfigDtls1[v]);

													nlapiLogExecution('Debug', 'Time Stamp Before calling getLocnGroupsForOrder',TimeStampinSec());

													var allLocnGroups =  getLocnGroupsForOrder(binlocgrpid, binlocid, pickZone,allZoneLocnGroups);

													nlapiLogExecution('Debug', 'Time Stamp After calling getLocnGroupsForOrder',TimeStampinSec());

													//nlapiLogExecution('Debug', 'allLocnGroups', allLocnGroups);

													var pfBinLocationgroupId = '';

													if (pfLocationResults != null && pfLocationResults.length > 0)
													{
														for(var P = 0; P < pfLocationResults.length; P++)
														{
															pfBinLocationgroupId = pfLocationResults[P].getValue('custrecord_inboundlocgroupid', 'custrecord_pickbinloc');

															nlapiLogExecution('Debug', 'pfBinLocationgroupId', pfBinLocationgroupId);
														}
													}

													nlapiLogExecution('DEBUG', 'Time Stamp Before calling getIndividualItemDetails',TimeStampinSec());
													//inventorySearchResults = getIndividualItemDetails(allLocnGroups, fulfilmentItem,allocationStrategy,pfBinLocationgroupId,allinventorySearchResults);
													if((allLocnGroups!=null && allLocnGroups!='') || (pfBinLocationgroupId!=null && pfBinLocationgroupId!='') || (binlocid!=null && binlocid!='')){   
														//inventorySearchResults = getItemDetails(allLocnGroups, fulfilmentItem,allocationStrategy,pfBinLocationgroupId,null,null,null,null);
														inventorySearchResults = getItemDetails(allLocnGroups, fulfilmentItem,allocationStrategy,pfBinLocationgroupId,lefoflag,null,null,fulfilmentQuantity,samelot,latestexpirydt,binlocid);
													}
													else
														inventorySearchResults=null;
													nlapiLogExecution('DEBUG', 'Time Stamp After calling getIndividualItemDetails',TimeStampinSec());

//													nlapiLogExecution('Debug', 'inventorySearchResults', inventorySearchResults);
//													nlapiLogExecution('Debug', 'pfLocationResults', pfLocationResults);

													var pfBinLocationId;
													var pickfaceArr=new Array();
													if((inventorySearchResults!=null && inventorySearchResults!='') || (pfLocationResults!=null && pfLocationResults!=''))
													{

														if (pfLocationResults != null && pfLocationResults.length > 0)
														{
															for(var z = 0; z < pfLocationResults.length; z++)
															{
																if(parseFloat(remainingQuantity) > 0)
																{
																	var pfItem = pfLocationResults[z].getValue('custrecord_pickfacesku');
																	var Autoreplenflag = pfLocationResults[z].getValue('custrecord_autoreplen');

																	if (pfItem == fulfilmentItem)
																	{
																		//nlapiLogExecution('Debug', 'item Match',pfItem);
																		pfBinLocationId = pfLocationResults[z].getValue('custrecord_pickbinloc');
																		pfMaximumQuantity = pfLocationResults[z].getValue('custrecord_maxqty');
																		pfMaximumPickQuantity = pfLocationResults[z].getValue('custrecord_maxpickqty');
																		pffixedlp = pfLocationResults[z].getText('custrecord_pickface_ebiz_lpno');
																		pfstatus=pfLocationResults[z].getValue('custrecord_pickface_itemstatus');
																		replenrule=pfLocationResults[z].getValue('custrecord_pickruleid');
																		pfRoundQty=pfLocationResults[z].getValue('custrecord_roundqty');
																		pfWHLoc=pfLocationResults[z].getValue('custrecord_pickface_location');
																		pfpickzoneid = pfLocationResults[z].getValue('custrecord_pickzone');

																		var str = 'pfBinLocationId. = ' + pfBinLocationId + '<br>';
																		str = str + 'pfMaximumQuantity. = ' + pfMaximumQuantity + '<br>';	
																		str = str + 'pfMaximumPickQuantity. = ' + pfMaximumPickQuantity + '<br>';
																		str = str + 'remainingQuantity. = ' + remainingQuantity + '<br>';
//																		str = str + 'pffixedlp. = ' + pffixedlp + '<br>';
//																		str = str + 'pfstatus. = ' + pfstatus + '<br>';
//																		str = str + 'replenrule. = ' + replenrule + '<br>';
																		str = str + 'pfpickzoneid. = ' + pfpickzoneid + '<br>';

																		nlapiLogExecution('Debug', 'Pickface Details', str);

																		if(parseFloat(pfMaximumPickQuantity) >= parseFloat(remainingQuantity)
																				|| (uompackflag!="1" && uompackflag!="3"))
																		{																		

																			var availableQty ="";
																			var allocQty = "";
																			var recordId = "";
																			var itemStatus = "";
																			var fromLP = "";
																			var inventoryItem="";
																			var fromLot="";
																			var invtindex=-1;
																			var expiryDate='';
																			// Get inventory details for this pickface location
																			var binLocnInvDtls = getAvailQtyFromInventory(inventorySearchResults, pfBinLocationId, allocatedInv,
																					DOwhLocation,fulfilmentItem,fulfillmentItemStatus,samelot,latestexpirydt,shelflife);
																			nlapiLogExecution('Debug', 'binLocnInvDtls', binLocnInvDtls);

																			if(binLocnInvDtls!=null && binLocnInvDtls.length>0)
																			{
																				for(var pf=0;pf<binLocnInvDtls.length;pf++)
																				{
																					if(parseFloat(remainingQuantity) > 0) 
																					{
																						invtindex = binLocnInvDtls[pf][0];
																						availableQty = binLocnInvDtls[pf][2];
																						allocQty = binLocnInvDtls[pf][3];
																						recordId = binLocnInvDtls[pf][8];
																						if(binLocnInvDtls[pf][4]!=null && binLocnInvDtls[pf][4]!='')
																							packCode = binLocnInvDtls[pf][4];
																						itemStatus = binLocnInvDtls[pf][5];										
																						whLocation = binLocnInvDtls[pf][6];
																						fromLP = binLocnInvDtls[pf][7];											
																						inventoryItem = binLocnInvDtls[pf][9];
																						fromLot = binLocnInvDtls[pf][10];
																						expiryDate =binLocnInvDtls[pf][11];

																						/*
																						 * If the allocation strategy is not null check in inventory for that item.						 
																						 * 		if the allocation strategy = min quantity on the pallet
																						 * 			search for the inventory where the inventory has the least quantity
																						 * 		else
																						 * 			search for the inventory where the inventory has the maximum quantity 
																						 * 			on the pallet 
																						 */

//																						var str = 'Inventory Item Status. = ' + itemStatus + '<br>';
//																						str = str + 'Fulfillment Item Status. = ' + fulfillmentItemStatus + '<br>';	
//																						str = str + 'availableQty. = ' + availableQty + '<br>';	
//																						str = str + 'remainingQuantity. = ' + remainingQuantity + '<br>';	

//																						nlapiLogExecution('Debug', 'Item Status details', str);

																						if((parseFloat(availableQty) > 0) && (itemStatus== fulfillmentItemStatus) &&((vfolotnumber == fromLot)||(vfolotnumber==null||vfolotnumber==''||vfolotnumber=='null')))
																						{
																							// allocate to this bin location
																							var actualAllocQty = Math.min(parseFloat(availableQty), parseFloat(remainingQuantity));
																							var newAllocQty = parseFloat(allocQty) + parseFloat(actualAllocQty);
																							//remainingQuantity = parseInt(remainingQuantity) - parseInt(actualAllocQty);

																							var expectedQuantity = parseFloat(actualAllocQty);

//																							var str = 'actualAllocQty. = ' + actualAllocQty + '<br>';
//																							str = str + 'newAllocQty. = ' + newAllocQty + '<br>';	
//																							str = str + 'remainingQuantity. = ' + remainingQuantity + '<br>';	
//																							str = str + 'pfMaximumPickQuantity. = ' + pfMaximumPickQuantity + '<br>';																				

//																							nlapiLogExecution('Debug', 'Alloc Qty details', str);

																							var contsize="";								
																							var totalweight = parseFloat((expectedQuantity*uomweight))/parseFloat(uomqty);
																							var totalcube = parseFloat((expectedQuantity*uomcube))/parseFloat(uomqty);

																							// Allocate the quantity to the pickface location if the allocation quantity is 
																							// 	less than or equal to the pick face maximum quantity
																							if(actualAllocQty <= pfMaximumPickQuantity)
																							{
//																								var str = 'uomlevel. = ' + uomlevel + '<br>';
//																								str = str + 'uompackflag. = ' + uompackflag + '<br>';	
//																								str = str + 'uomqty. = ' + uomqty + '<br>';	
//																								str = str + 'expectedQuantity. = ' + expectedQuantity + '<br>';	

//																								nlapiLogExecution('Debug', 'UOM Details', str);

																								//1-Ship Cartons	2-Build Cartons		3-Ship Pallets
																								if(((uompackflag=="1" || uompackflag=="3") &&(parseFloat(expectedQuantity) == parseFloat(uomqty))) || (dimsbulkpick=='T'))
																								{	
																									pfcontLP = ordercarton;

																									for(l=0;l<containerslist.length;l++){     

																										var containername = containerslist[l].getValue('custrecord_containername');

																										var vcontainerSite=containerslist[l].getValue('custrecord_ebizsitecontainer');
																										//if(containername=='SHIPASIS')
																										if(containername=='SHIPASIS'&&(vcontainerSite==DOwhLocation))
																										{
																											contsize = containerslist[l].getValue('Internalid');
																											pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																											CreateMasterLPRecordBulk(pfcontLP,contsize,totalweight,totalcube,uompackflag,DOwhLocation,LPparent);
																										}
																									}

																									nlapiLogExecution('DEBUG', 'Container Size in Pick Face : ', contsize);
																								}
																								else if(vShipByItself == "T" && (parseFloat(fulfilmentQuantity) == parseFloat(1)) && vShipAloneRULE=="Y"
																									&& (fulfillOrderList.length == 1)) //(parseFloat(solinecount)==1))
																								{	
																									pfcontLP = ordercarton;

																									for(l=0;l<containerslist.length;l++){     

																										var containername = containerslist[l].getValue('custrecord_containername');
																										var vcontainerSite=containerslist[l].getValue('custrecord_ebizsitecontainer');
																										//if(containername=='SHIPASIS')
																										if(containername=='SHIPASIS'&&(vcontainerSite==DOwhLocation))
																										{
																											contsize = containerslist[l].getValue('Internalid');
																											pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																											CreateMasterLPRecordBulk(pfcontLP,contsize,totalweight,totalcube,uompackflag,DOwhLocation,LPparent);
																										}
																									}

																									nlapiLogExecution('Debug', 'Container Size in Pick Face : ', contsize);
																								}
																								else if(cartonizationmethod=='4')
																								{
																									//pfcontLP = ordercarton;

																									toatlcartonwht=toatlcartonwht+totalweight;
																									toatlcartoncube=toatlcartoncube+totalcube;

																									/*if(prvSalesord!=salesOrderNo)
																					{
																						pfcontLP='';
																						//pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																						prvSalesord=salesOrderNo;
																						//CreateMasterLPRecord(pfcontLP,contsize,totalweight,totalcube,uompackflag);
																					}*/
																								}

																								nlapiLogExecution('Debug', 'Container LP in Pick Face : ', pfcontLP);

																								//updateInventoryRecord(inventorySearchResults[invtindex], actualAllocQty);
																								updateInventoryWithAllocation(recordId, actualAllocQty);
																								//createRecordInOpenTask
																								createpicktask(fulfillOrderList[i], expectedQuantity, eBizWaveNo, pfcontLP, 
																										pfBinLocationId, recordId, packCode, itemStatus, pickMethod, pfpickzoneid, pickStrategyId,
																										whLocation, fromLP,uomlevel,totalweight,totalcube,contsize,fromLot,'',wmsCarrier,fulfilmentItem,nsrefno,customer,dimsbulkpick,otparent,waveassignedto,expiryDate);

																								//updateFulfilmentOrder
																								updateFulfilmentOrderline(fulfillOrderList[i], actualAllocQty, eBizWaveNo,foparent);

																								remainingQuantity = parseFloat(remainingQuantity) - parseFloat(actualAllocQty);

																								// Maintain the allocation temporarily
																								var allocatedInvRow = [pfBinLocationId, expectedQuantity,fromLP,inventoryItem,fromLot];
																								allocatedInv.push(allocatedInvRow);
																								pickfaceArr.push(pfBinLocationId);
																							}
																						}
																						//nlapiLogExecution('Debug','remainingQuantity',remainingQuantity);
																						totordqty=totordqty+remainingQuantity;
//																						nlapiLogExecution('Debug','totordqty',totordqty);
//																						nlapiLogExecution('Debug','totreplenqty',totreplenqty);
																					}
																				}
																			}
																			//Temporarily added by Satish.N
																			//Autoreplenflag='F';
																			//

																			if(remainingQuantity>0 && Autoreplenflag=='T')
																			{
																				if(totordqty==0 || totordqty>totreplenqty)
																				{
																					recordId="";
//																					nlapiLogExecution('Debug','availableQty',availableQty);
//																					nlapiLogExecution('Debug','remainingQuantity',remainingQuantity);
																					if(availableQty==null || availableQty=='')
																						availableQty=0;
																					var actualAllocQty = Math.min(parseFloat(availableQty), parseFloat(remainingQuantity));
																					var newAllocQty = parseFloat(allocQty) + parseFloat(actualAllocQty);
																					var expectedQuantity = parseFloat(remainingQuantity);

																					var totalweight = parseFloat((expectedQuantity*uomweight))/parseFloat(uomqty);
																					var totalcube = parseFloat((expectedQuantity*uomcube))/parseFloat(uomqty);

																					//remainingQuantity = parseInt(remainingQuantity) - parseInt(actualAllocQty);
																					//nlapiLogExecution('Debug','actualAllocQty',actualAllocQty);
																					//var expectedQuantity = parseInt(remainingQuantity);
//																					nlapiLogExecution('Debug','EnterReplen','EnterReplen');
//																					nlapiLogExecution('Debug','remainingQuantity',remainingQuantity);
//																					nlapiLogExecution('Debug','uompackflag',uompackflag);
//																					nlapiLogExecution('Debug','uomqty',uomqty);

																					if(((uompackflag=="1" || uompackflag=="3") &&(parseFloat(expectedQuantity) == parseFloat(uomqty))) || (dimsbulkpick=='T'))
																					{
																						pfcontLP = ordercarton;

																						for(l=0;l<containerslist.length;l++){     

																							var containername = containerslist[l].getValue('custrecord_containername');
																							var vcontainerSite=containerslist[l].getValue('custrecord_ebizsitecontainer');
																							//if(containername=='SHIPASIS')
																							if(containername=='SHIPASIS'&&(vcontainerSite==DOwhLocation))
																							{
																								contsize = containerslist[l].getValue('Internalid');
																								pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																								CreateMasterLPRecordBulk(pfcontLP,contsize,totalweight,totalcube,uompackflag,DOwhLocation,LPparent);
																							}
																						}

																						nlapiLogExecution('DEBUG', 'Container Size in Pick Face : ', contsize);
																					}
																					else if(vShipByItself == "T" && (parseFloat(fulfilmentQuantity) == parseFloat(1)) && vShipAloneRULE == "Y"
																						&& (fulfillOrderList.length == 1)) //(parseInt(solinecount)==1))
																					{
																						pfcontLP = ordercarton;

																						for(l=0;l<containerslist.length;l++){     

																							var containername = containerslist[l].getValue('custrecord_containername');
																							var vcontainerSite=containerslist[l].getValue('custrecord_ebizsitecontainer');
																							//if(containername=='SHIPASIS')
																							if(containername=='SHIPASIS'&&(vcontainerSite==DOwhLocation))
																							{
																								contsize = containerslist[l].getValue('Internalid');
																								pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																								CreateMasterLPRecordBulk(pfcontLP,contsize,totalweight,totalcube,uompackflag,DOwhLocation,LPparent);
																							}
																						}

																						nlapiLogExecution('Debug', 'Container Size in Pick Face : ', contsize);
																					}
																					else if(cartonizationmethod=='4')
																					{
																						toatlcartonwht=toatlcartonwht+totalweight;
																						toatlcartoncube=toatlcartoncube+totalcube;

																						//pfcontLP = ordercarton;

																						/*if(prvSalesord!=salesOrderNo)
																					{
																						pfcontLP='';
																						//pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																						prvSalesord=salesOrderNo;
																						//CreateMasterLPRecord(pfcontLP,contsize,totalweight,totalcube,uompackflag);
																					}*/
																					}
																					else
																					{
																						pfcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																					}

																					var generateReplen='Y';

																					if(generateReplen!='Y')
																					{
																						allinventorySearchResults = getItemDetails(null, fulfilmentItem,null,null,lefoflag,
																								null,null,null,samelot,latestexpirydt);

																						var binLocnInvDtls2 = getAvailQtyForReplenishment(allinventorySearchResults, pfBinLocationId, allocatedInv,DOwhLocation,fulfilmentItem,fulfillmentItemStatus);
																						nlapiLogExecution('Debug', 'binLocnInvDtls2', binLocnInvDtls2);
																						var invtavailqty=0;
																						if(binLocnInvDtls2!=null && binLocnInvDtls2.length>0)
																						{
																							for (var h = 0; h < binLocnInvDtls2.length; h++){

																								invtavailqty = invtavailqty + binLocnInvDtls2[h][2];																						
																							}
																						}

																						nlapiLogExecution('Debug', 'invtavailqty', invtavailqty);
																						//invtavailqty=0;
																						if(invtavailqty>=remainingQuantity)
																						{
																							//createRecordInOpenTask
																							createpicktask(fulfillOrderList[i], expectedQuantity, eBizWaveNo, pfcontLP, 
																									pfBinLocationId, recordId, packCode, fulfillmentItemStatus, pickMethod, pfpickzoneid, pickStrategyId,
																									whLocation, fromLP,uomlevel,totalweight,totalcube,contsize,fromLot,'',wmsCarrier,fulfilmentItem,nsrefno,customer,dimsbulkpick,otparent,waveassignedto,baseuomid);

																							//updateFulfilmentOrder
																							updateFulfilmentOrderline(fulfillOrderList[i], remainingQuantity, eBizWaveNo,foparent);

																							remainingQuantity=0;
																						}
																					}
																					else
																					{

																						var replenItemArray =  new Array();
																						var currentRow = new Array();
																						var j = 0;
																						var idx = 0;
																						var putawatqty=0;
																						var pickfaceQty=0;
																						var openreplensqty=0;

																						var pickqty=0;
																						var searchpickyqty=getOpenPickqty(pfBinLocationId);
																						if(searchpickyqty!=null && searchpickyqty!='')
																						{
																							pickqty = searchpickyqty[0].getValue('custrecord_expe_qty', null, 'sum');
																						}

																						nlapiLogExecution('Debug', 'pickqty', pickqty);

																						var serchreplensqty=getopenreplens(salesOrderNo,fulfilmentItem,pfBinLocationId,pfWHLoc);
																						if(serchreplensqty!=null && serchreplensqty!='')
																						{
																							openreplensqty = serchreplensqty[0].getValue('custrecord_expe_qty', null, 'sum');
																						}

																						var searchputawyqty=getOpenputawayqty(pfBinLocationId);
																						if(searchputawyqty!=null && searchputawyqty!='')
																						{
																							putawatqty = searchputawyqty[0].getValue('custrecord_expe_qty', null, 'sum');
																						}

																						var searchpickfaceQty = getQOHForAllSKUsinPFLocation(pfBinLocationId,fulfilmentItem,pfWHLoc);
																						if(searchpickfaceQty!=null && searchpickfaceQty!='')
																						{
																							pickfaceQty=searchpickfaceQty[0].getValue('custrecord_ebiz_qoh',null,'sum');
																						}

																						var replenQty=pfLocationResults[z].getValue('custrecord_replenqty');
																						nlapiLogExecution('Debug','putawatqty',putawatqty);
																						nlapiLogExecution('Debug','pickqty',pickqty);
																						nlapiLogExecution('Debug','pickfaceQty',pickfaceQty);
																						nlapiLogExecution('Debug','replenQty',replenQty);
																						nlapiLogExecution('Debug','openreplensqty',openreplensqty);

																						if(pfRoundQty == null || pfRoundQty == '')
																							pfRoundQty=0;

																						nlapiLogExecution('Debug','pfRoundQty',pfRoundQty);

																						if(openreplensqty == null || openreplensqty == '')
																							openreplensqty=0;

																						if(pickqty == null || pickqty == '')
																							pickqty=0;

																						if(putawatqty == null || putawatqty == '')
																							putawatqty=0;

																						if(pickfaceQty == null || pickfaceQty == '')
																							pickfaceQty=0;
																						//case# 20148741 starts(Replens not generating because getting negative values)
																						//var tempQty = parseInt(pfMaximumQuantity) -  parseInt(openreplensqty) - parseInt(pickqty) + parseInt(putawatqty)+parseInt(pickfaceQty);
																						var tempQty = (parseFloat(pfMaximumQuantity) - (parseFloat(openreplensqty) + parseFloat(pickfaceQty)  - parseFloat(pickqty) + parseFloat(putawatqty)));
																						//case# 20148741 ends
																						nlapiLogExecution('Debug','tempQty',tempQty);

																						nlapiLogExecution('DEBUG','tempQty1',tempQty);

																						var tempwithRoundQty = Math.floor(parseFloat(tempQty)/parseFloat(pfRoundQty));

																						if(tempwithRoundQty == null || tempwithRoundQty == '' || isNaN(tempwithRoundQty))
																							tempwithRoundQty=0;

																						nlapiLogExecution('DEBUG','tempwithRoundQty',tempwithRoundQty);

																						tempQty = parseFloat(pfRoundQty)*parseFloat(tempwithRoundQty);

																						nlapiLogExecution('DEBUG','tempQty2',tempQty);

																						var replenTaskCount = Math.ceil(parseInt(tempQty) / parseInt(replenQty));
																						nlapiLogExecution('Debug','tempQty',tempQty);

																						currentRow = [pfItem, replenQty, replenrule, pfMaximumQuantity, pfMaximumPickQuantity, pickfaceQty, whLocation, 
																						              pfBinLocationId, tempQty, replenTaskCount, pffixedlp,pfstatus,fulfillmentItemStatus,pfRoundQty];// case# 201413967
																						if(idx == 0)
																							idx = currentRow.length;

																						replenItemArray[j++] = currentRow;
																						var taskCount;
																						var zonesArray = new Array();

																						var replenZoneList = "";
																						var replenZoneTextList = "";
																						var replenzonestatuslist="";
																						var replenzonestatustextlist="";
																						//var Reportno = "";

																						if (replenRulesList != null){
																							for (var w = 0; w < replenRulesList.length; w++){

																								if (w == 0){
																									replenZoneList += replenRulesList[w].getValue('custrecord_replen_strategy_zone');
																									replenZoneTextList += replenRulesList[w].getText('custrecord_replen_strategy_zone');
																									replenzonestatuslist +=replenRulesList[w].getValue('custrecord_replen_strategy_itemstatus');
																									replenzonestatustextlist +=replenRulesList[w].getText('custrecord_replen_strategy_itemstatus');
																								} else {
																									replenZoneList += ',';
																									replenZoneList += replenRulesList[w].getValue('custrecord_replen_strategy_zone');

																									replenZoneTextList += ',';
																									replenZoneTextList += replenRulesList[w].getText('custrecord_replen_strategy_zone');

																									replenzonestatuslist += ',';
																									replenzonestatuslist += replenRulesList[w].getValue('custrecord_replen_strategy_itemstatus');

																									replenzonestatustextlist += ',';
																									replenzonestatustextlist += replenRulesList[w].getText('custrecord_replen_strategy_itemstatus');
																								}
																							}
																						} 

																						nlapiLogExecution('Debug','replenZoneList',replenZoneList);

																						var itemList = buildItemListForReplenProcessing(replenItemArray, j);
																						nlapiLogExecution('Debug','replen Item List',itemList);

																						var locnGroupList = getZoneLocnGroupsList(replenZoneList);	
																						nlapiLogExecution('Debug','Location Group List',locnGroupList);

																						var ZoneAndLocationGrp =getZoneLocnGroupsListforRepln(replenZoneList,whLocation);// case# 201416311
																						nlapiLogExecution('DEBUG','ZoneAndLocationGrp',ZoneAndLocationGrp);

																						var inventorySearchResultsReplen = getItemDetailsforRepln(locnGroupList, itemList,replenzonestatuslist,whLocation,lefoflag,fulfillmentItemStatus);// case# 201416715
																						nlapiLogExecution('Debug','inventorySearchResultsReplen',inventorySearchResultsReplen);
																						if ((inventorySearchResultsReplen != null && inventorySearchResultsReplen != "") || (remainingQuantity<=openreplensqty)){
																							nlapiLogExecution('Debug','test2','test2');
																							// Get the report number
																							if (Reportno == null || Reportno == "") {
																								Reportno = GetMaxTransactionNo('REPORT');
																								Reportno = Reportno.toString();
																							}

																							/*
																							 * Loop through the total task count and get the location from where the item has to be 
																							 * replenished.
																							 */

																							//Get the Stage Location in case of two step replenishment process
																							var stageLocation='';

																							nlapiLogExecution('Debug','Reportno',Reportno);
																							nlapiLogExecution('Debug','fulfilmentOrderNo',fulfilmentOrderNo);
																							nlapiLogExecution('Debug','replenQty',replenQty);
																							nlapiLogExecution('Debug','openreplensqty',openreplensqty);
																							nlapiLogExecution('Debug','remainingQuantity',remainingQuantity);
																							var resultArray=""; 
																							if(tempQty>0 && tempQty!='NaN')
																							{
																								resultArray =replenRuleZoneLocationForAutoRepln(inventorySearchResultsReplen, replenItemArray, Reportno, stageLocation, whLocation,fulfilmentOrderNo,eBizWaveNo,"",expectedQuantity,pickfaceArr,ZoneAndLocationGrp);
																							}
																							var result=resultArray[0];

																							if(result==true || (remainingQuantity<=openreplensqty))
																							{
																								var vREMQtyafterReplnGenerated=resultArray[1];
																								var vActualReplnQtyGenerated=resultArray[2];
																								nlapiLogExecution('DEBUG','fulfillOrderList',fulfillOrderList[i]);
																								nlapiLogExecution('DEBUG','vActualReplnQtyGenerated',vREMQtyafterReplnGenerated+"/"+vActualReplnQtyGenerated);


																								if(inventorySearchResultsReplen!=null && inventorySearchResultsReplen!='')
																								{
																									fromLot=inventorySearchResultsReplen[0].getText('custrecord_ebiz_inv_lot');	
																									nlapiLogExecution('DEBUG','test the id',inventorySearchResultsReplen[0].getId());
																								}

																								var test=fulfillOrderList[i][6];

																								nlapiLogExecution('DEBUG','test',test);
																								/*createRecordInOpenTask(fulfillOrderList[i], vActualReplnQtyGenerated, eBizWaveNo, pfcontLP, 
																									pfBinLocationId, recordId, packCode, fulfillmentItemStatus, pickMethod, pfpickzoneid, pickStrategyId,
																									whLocation, fromLP,uomlevel,totalweight,totalcube,contsize,fromLot,'',wmsCarrier,fulfilmentItem,nsrefno,customer,dimsbulkpick,otparent);*/

																								//createRecordInOpenTask
																								createpicktask(fulfillOrderList[i], expectedQuantity, eBizWaveNo, pfcontLP, 
																										pfBinLocationId, recordId, packCode, fulfillmentItemStatus, pickMethod, pfpickzoneid, pickStrategyId,
																										whLocation, fromLP,uomlevel,totalweight,totalcube,contsize,fromLot,'',wmsCarrier,fulfilmentItem,nsrefno,customer,dimsbulkpick,otparent,waveassignedto,baseuomid);

																								//updateFulfilmentOrder
																								updateFulfilmentOrderline(fulfillOrderList[i], remainingQuantity, eBizWaveNo,foparent);

																								if((remainingQuantity>tempQty && tempQty>0) && (remainingQuantity>openreplensqty) )
																								{
																									remainingQuantity=parseFloat(remainingQuantity) - parseFloat(tempQty);
																								}
																								else
																								{
																									remainingQuantity=vREMQtyafterReplnGenerated;
																									remainingQuantity=0;
																								}
																								pickfaceArr.push(pfBinLocationId);
																								// Maintain the allocation temporarily
																								//var allocatedInvRow = [beginLocationId, expectedQuantity,fromLP,inventoryItem,fromLot];
																								//allocatedInv.push(allocatedInvRow);
																							}
																						}

																						totreplenqty=totreplenqty+replenQty;
																					}
																				}
																			}
																		}
																	}
																}
															}
														}//

														if(inventorySearchResults != null && inventorySearchResults.length > 0)
														{
															for (var j = 0; j < inventorySearchResults.length; j++)
															{
																inventoryItem = inventorySearchResults[j].getValue('custrecord_ebiz_inv_sku');
																var InvWHLocation = inventorySearchResults[j].getValue('custrecord_ebiz_inv_loc');

//																var str = 'remainingQuantity. = ' + remainingQuantity + '<br>';
//																str = str + 'inventoryItem. = ' + inventoryItem + '<br>';	
//																str = str + 'fulfilmentItem. = ' + fulfilmentItem + '<br>';

																//nlapiLogExecution('Debug', 'Remaining Qty & Item', str);

																if ((DOwhLocation==InvWHLocation)&&(inventoryItem.toString() == fulfilmentItem.toString()) && (parseFloat(remainingQuantity) > 0))
																{
																	actualQty = inventorySearchResults[j].getValue('custrecord_ebiz_qoh');
																	inventoryAvailableQuantity = inventorySearchResults[j].getValue('custrecord_ebiz_avl_qty');
																	allocationQuantity = inventorySearchResults[j].getValue('custrecord_ebiz_alloc_qty');
																	recordId = inventorySearchResults[j].getId();
																	beginLocationId = inventorySearchResults[j].getValue('custrecord_ebiz_inv_binloc');
																	if(inventorySearchResults[j].getValue('custrecord_ebiz_inv_packcode')!=null && inventorySearchResults[j].getValue('custrecord_ebiz_inv_packcode')!='')
																		packCode = inventorySearchResults[j].getValue('custrecord_ebiz_inv_packcode');
																	itemStatus = inventorySearchResults[j].getValue('custrecord_ebiz_inv_sku_status');
																	whLocation = inventorySearchResults[j].getValue('custrecord_ebiz_inv_loc');
																	fromLP = inventorySearchResults[j].getValue('custrecord_ebiz_inv_lp');
																	fromLot = inventorySearchResults[j].getText('custrecord_ebiz_inv_lot');

																	// Get already allocated inv
																	var alreadyAllocQty = getAlreadyAllocatedInv(allocatedInv, beginLocationId,fromLP,inventoryItem,fromLot);

																	// Adjust for already allocated inv
																	//inventoryAvailableQuantity = parseInt(inventoryAvailableQuantity) - parseInt(alreadyAllocQty);// case# 201412175

																	var str = 'Inventory Item Status. = ' + itemStatus + '<br>';
																	str = str + 'Fulfillment Item Status. = ' + fulfillmentItemStatus + '<br>';	
																	str = str + 'already AllocQty. = ' + alreadyAllocQty + '<br>';	
																	str = str + 'inventory Available Quantity. = ' + inventoryAvailableQuantity + '<br>';	
																	str = str + 'remaining Quantity. = ' + remainingQuantity + '<br>';	
																	str = str + 'beginLocationId. = ' + beginLocationId + '<br>';	
																	str = str + 'pfBinLocationId. = ' + pfBinLocationId + '<br>';
																	str = str + 'cartonizationmethod. = ' + cartonizationmethod + '<br>';
																	str = str + 'fromLot. = ' + fromLot + '<br>';
																	str = str + 'vfolotnumber. = ' + vfolotnumber + '<br>';
																	nlapiLogExecution('DEBUG', 'Inventory Details', str);

																	var vPickfaceAllocLoc=false;
																	if(pickfaceArr != null && pickfaceArr != '')
																	{
																		if(pickfaceArr.indexOf(beginLocationId)!=-1)
																			vPickfaceAllocLoc=true;
																	}	
																	if(vPickfaceAllocLoc == false)
																	{
																		if((parseFloat(inventoryAvailableQuantity) > 0) && (itemStatus==fulfillmentItemStatus) && (beginLocationId!=pfBinLocationId) && ((vfolotnumber==fromLot)||(vfolotnumber==null||vfolotnumber==''||vfolotnumber=='null'))){
																			var actualAllocationQuantity = Math.min(parseFloat(inventoryAvailableQuantity), parseFloat(remainingQuantity));
																			allocationQuantity = parseFloat(alreadyAllocQty) + parseFloat(actualAllocationQuantity);
																			remainingQuantity = parseFloat(remainingQuantity) - parseFloat(actualAllocationQuantity);
																			var expectedQuantity = parseFloat(actualAllocationQuantity);

																			var contsize="";	

																			var totalweight = parseFloat((expectedQuantity*uomweight))/parseFloat(uomqty);
																			var totalcube = parseFloat((expectedQuantity*uomcube))/parseFloat(uomqty);

//																			var str = 'uompackflag. = ' + uompackflag + '<br>';
//																			str = str + 'uomqty. = ' + uomqty + '<br>';	
//																			str = str + 'expectedQuantity. = ' + expectedQuantity + '<br>';	
//																			str = str + 'allocationQuantity. = ' + allocationQuantity + '<br>';	
//																			str = str + 'cartonizationmethod. = ' + cartonizationmethod + '<br>';	
//																			str = str + 'remaining Quantity. = ' + remainingQuantity + '<br>';	

//																			nlapiLogExecution('Debug', 'Qty Details', str);

																			//1-Ship Cartons	2-Build Cartons		3-Ship Pallets
																			if(((uompackflag=="1" || uompackflag=="3") &&(parseInt(expectedQuantity) == parseInt(uomqty))) || (dimsbulkpick=='T'))
																			{	
																				invtcontLP = ordercarton;

																				for(l=0;l<containerslist.length;l++)
																				{                                         
																					var containername = containerslist[l].getValue('custrecord_containername');
																					//nlapiLogExecution('DEBUG', 'containername', containername);
																					var vcontainerSite=containerslist[l].getValue('custrecord_ebizsitecontainer');
																					//if(containername=='SHIPASIS')
																					if(containername=='SHIPASIS'&&(vcontainerSite==DOwhLocation))
																					{
																						invtcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																						contsize = containerslist[l].getValue('Internalid');
																						CreateMasterLPRecordBulk(invtcontLP,contsize,totalweight,totalcube,uompackflag,DOwhLocation,LPparent);
																					}
																				}
																			}
																			else if(vShipByItself=="T"&&(parseInt(fulfilmentQuantity) == parseInt(1))&&vShipAloneRULE=="Y"
																				&& (fulfillOrderList.length == 1)) //(parseInt(solinecount)==1))
																			{	
																				invtcontLP = ordercarton;

																				for(l=0;l<containerslist.length;l++)
																				{                                         
																					var containername = containerslist[l].getValue('custrecord_containername');
																					//nlapiLogExecution('DEBUG', 'containername', containername);
																					var vcontainerSite=containerslist[l].getValue('custrecord_ebizsitecontainer');
																					//if(containername=='SHIPASIS')
																					if(containername=='SHIPASIS'&&(vcontainerSite==DOwhLocation))
																					{
																						invtcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																						contsize = containerslist[l].getValue('Internalid');
																						CreateMasterLPRecordBulk(invtcontLP,contsize,totalweight,totalcube,uompackflag,DOwhLocation,LPparent);
																					}
																				}
																			}
																			else if(cartonizationmethod=='4') 
																			{
																				toatlcartonwht=toatlcartonwht+totalweight;
																				toatlcartoncube=toatlcartoncube+totalcube;

																				//invtcontLP = ordercarton;

																				/*if(prvSalesord!=salesOrderNo)
																		{
																			invtcontLP='';
																			//invtcontLP = GetMaxLPNo('1', '2',DOwhLocation);
																			prvSalesord=salesOrderNo;
																			//CreateMasterLPRecord(invtcontLP,contsize,totalweight,totalcube,uompackflag);
																		}*/
																			}
																			else
																			{
																				invtcontLP ='';
																			}

//																			updateInventoryRecord(inventorySearchResults[j], actualAllocationQuantity);
																			updateInventoryWithAllocation(recordId,actualAllocationQuantity);
																			//createRecordInOpenTask
																			createpicktask(fulfillOrderList[i], expectedQuantity, eBizWaveNo, invtcontLP, beginLocationId, recordId,
																					packCode, itemStatus, pickMethod, pickZone, pickStrategyId, whLocation, fromLP,uomlevel,
																					totalweight,totalcube,contsize,fromLot,'',wmsCarrier,fulfilmentItem,nsrefno,customer,dimsbulkpick,otparent,waveassignedto,baseuomid);

																			//updateFulfilmentOrder
																			updateFulfilmentOrderline(fulfillOrderList[i], actualAllocationQuantity, eBizWaveNo,foparent);

																			// Maintain the allocation temporarily
																			var allocatedInvRow = [beginLocationId, expectedQuantity,fromLP,inventoryItem,fromLot];
																			allocatedInv.push(allocatedInvRow);

																		}
																	}
																}
															}

														}

													}
												}
											}
											//If there is still some quantity remaining, then insert a failed task
											if(parseInt(nooflps)>0 && parseInt(remainingQuantity) > 0)
											{
												vnotes='Insufficient Inventory';
												nlapiLogExecution('Debug', 'Creating Failed Task');
												nlapiLogExecution('Debug', 'remainingQuantity',remainingQuantity);
												//createRecordInOpenTask
												createpicktask(fulfillOrderList[i], remainingQuantity, eBizWaveNo, "", "", "",
														"", "", "", "", "", DOwhLocation, "",uomlevel,"","","","",vnotes,"",fulfilmentItem,nsrefno,customer,dimsbulkpick,otparent,waveassignedto,baseuomid);


												//updateFulfilmentOrder
												updateFulfilmentOrderline(fulfillOrderList[i], 0, eBizWaveNo,foparent);
												nlapiLogExecution('Debug', 'Creating Failed Task','SUCCESS');
												vBoolFailedForSameLP=true;
												remainingQuantity=0;
											}
											//}
										}
									}
								}
							}

						}
						//}
						//}
					}

				}		

			}

			//CreateMasterLPRecordBulk(ordercarton,null,toatlcartonwht,toatlcartoncube,null,DOwhLocation,LPparent);
		}
	}
	catch(exp) {
		nlapiLogExecution('Debug', 'Exception in allocateQuantity : ', exp);		
	}

	nlapiLogExecution('Debug', 'out of allocateQuantity ', eBizWaveNo);
}

function getSKUDimCount(orderQty, dimQty){

	var str = 'order qty. = ' + orderQty + '<br>';
	str = str + 'dimensions qty. = ' + dimQty + '<br>';	
	nlapiLogExecution('Debug', 'into getSKUDimCount', str);

	var retSKUDimCount = 0;

	if(dimQty > 0 && (parseFloat(orderQty) >= parseFloat(dimQty)))
	{
		skuDimCount = parseFloat(orderQty)/parseFloat(dimQty);
		retSKUDimCount = Math.floor(skuDimCount);
		if(retSKUDimCount == 0)
			retSKUDimCount = 1;
	}

	nlapiLogExecution('Debug', 'out of getSKUDimCount', '');

	return retSKUDimCount;
}

function getAlreadyAllocatedInv(allocatedInv, binLocation,fromLP,item,lot){
	//nlapiLogExecution('Debug', 'Into getAlreadyAllocatedInv',allocatedInv);
	var alreadyAllocQty = 0;
	if(allocatedInv != null && allocatedInv.length > 0){
		for(var i = 0; i < allocatedInv.length; i++){
//			nlapiLogExecution('Debug', 'fromLP',fromLP);
//			nlapiLogExecution('Debug', 'allocatedInv[i][2]',allocatedInv[i][2]);
//			nlapiLogExecution('Debug', 'binLocation',binLocation);
//			nlapiLogExecution('Debug', 'allocatedInv[i][0]',allocatedInv[i][0]);
//			nlapiLogExecution('Debug', 'item',item);
//			nlapiLogExecution('Debug', 'allocatedInv[i][3]',allocatedInv[i][3]);
//			nlapiLogExecution('Debug', 'lot',lot);
//			nlapiLogExecution('Debug', 'allocatedInv[i][4]',allocatedInv[i][4]);

			if(fromLP == allocatedInv[i][2] && binLocation==allocatedInv[i][0] && item==allocatedInv[i][3] && lot==allocatedInv[i][4])
				alreadyAllocQty = parseFloat(alreadyAllocQty) + parseFloat(allocatedInv[i][1]);
		}
	}
	//nlapiLogExecution('Debug', 'Out of getAlreadyAllocatedInv');
	return alreadyAllocQty;
}

function getAvailQtyFromInventory(inventorySearchResults, binLocation, allocatedInv,dowhlocation,fulfilmentItem,fulfillmentItemStatus
		,samelot,latestexpirydt,shelflife){

	nlapiLogExecution('Debug','Into getAvailQtyFromInventory');
	var binLocnInvDtls = new Array();
	var matchFound = false;
	var recno=-1;

	nlapiLogExecution('Debug','inventorySearchResults',inventorySearchResults);
	if(inventorySearchResults != null && inventorySearchResults.length > 0){
		nlapiLogExecution('Debug','inventorySearchResults length',inventorySearchResults.length);
		for(var i = 0; i < inventorySearchResults.length; i++){
			//if(!matchFound){
			invBinLocnId = inventorySearchResults[i].getValue('custrecord_ebiz_inv_binloc');
			var InvWHLocation = inventorySearchResults[i].getValue('custrecord_ebiz_inv_loc');
			var invItem=inventorySearchResults[i].getValue('custrecord_ebiz_inv_sku');
			var itemStatus = inventorySearchResults[i].getValue('custrecord_ebiz_inv_sku_status');
			var invExprDt = inventorySearchResults[i].getText('custrecord_ebiz_expdate');

			var str = 'dowhlocation. = ' + dowhlocation + '<br>';
			str = str + 'InvWHLocation. = ' + InvWHLocation + '<br>';	
			str = str + 'invBinLocnId. = ' + invBinLocnId + '<br>';
			str = str + 'binLocation. = ' + binLocation + '<br>';
			str = str + 'fulfilmentItem. = ' + fulfilmentItem + '<br>';
			str = str + 'invItem. = ' + invItem + '<br>';
			str = str + 'itemStatus. = ' + itemStatus + '<br>';
			str = str + 'samelot. = ' + samelot + '<br>';
			str = str + 'latestexpirydt. = ' + latestexpirydt + '<br>';
			str = str + 'shelflife. = ' + shelflife + '<br>';
			str = str + 'invExprDt. = ' + invExprDt + '<br>';

			nlapiLogExecution('Debug', 'Inventory & FO Details', str);

			//Check if Same Lot is enabled for customer

			if((samelot!=null && samelot!='') && (latestexpirydt!=null && latestexpirydt!=''))
			{
				if((dowhlocation==InvWHLocation) && (parseFloat(invBinLocnId) == parseFloat(binLocation)) && (fulfilmentItem==invItem)
						&& (itemStatus==fulfillmentItemStatus)){

					//If Same Lot is 'T', check for the inventory with inventory expiry date 
					//greater than or equal to latest expiry date (i.e the lot which is shipped recently).
					//If Same Lot is 'F', check for the inventory with inventory expiry date 
					//greater than latest expiry date
					if(samelot=='T' && invExprDt>=latestexpirydt)
					{
						if(shelflife!=null && shelflife!='')
						{
							if(dateDiffInDays(DateStamp(),invExprDt)>shelflife)
							{
								recno=i;
							}
						}
						else
						{
							recno=i;
						}
					}
					/*else if(samelot=='F' && invExprDt>latestexpirydt)
					{
						if(shelflife!=null && shelflife!='')
						{
							if(dateDiffInDays(DateStamp(),invExprDt)>shelflife)
							{
								recno=i;
							}
						}
						else
						{
							recno=i;
						}
					}*/
					else
						recno=i;

					if(recno>=0)
					{
						var actualQty = inventorySearchResults[recno].getValue('custrecord_ebiz_qoh');
						var inventoryAvailableQuantity = inventorySearchResults[recno].getValue('custrecord_ebiz_avl_qty');
						var allocationQuantity = inventorySearchResults[recno].getValue('custrecord_ebiz_alloc_qty');
						var packCode = inventorySearchResults[recno].getValue('custrecord_ebiz_inv_packcode');					
						var whLocation = inventorySearchResults[recno].getValue('custrecord_ebiz_inv_loc');
						var fromLP = inventorySearchResults[recno].getValue('custrecord_ebiz_inv_lp');
						var inventoryItem=inventorySearchResults[recno].getValue('custrecord_ebiz_inv_sku');
						var fromLot = inventorySearchResults[recno].getText('custrecord_ebiz_inv_lot');


						var str = 'Invt QOH. = ' + actualQty + '<br>';
						str = str + 'Alloc Qty. = ' + allocationQuantity + '<br>';	
						str = str + 'Avail Qty. = ' + inventoryAvailableQuantity + '<br>';
						str = str + 'packCode. = ' + packCode + '<br>';
						str = str + 'itemStatus. = ' + itemStatus + '<br>';
						str = str + 'whLocation. = ' + whLocation + '<br>';
						str = str + 'fromLP. = ' + fromLP + '<br>';
						str = str + 'inventoryItem. = ' + inventoryItem + '<br>';
						str = str + 'fromLot. = ' + fromLot + '<br>';

						nlapiLogExecution('Debug', 'Inventory Details', str);

						var recordId = inventorySearchResults[recno].getId();

						// Get allocation already done
						var alreadyAllocQty = getAlreadyAllocatedInv(allocatedInv, binLocation,fromLP,inventoryItem,fromLot);

						nlapiLogExecution('Debug', 'alreadyAllocQty', alreadyAllocQty);

						// Adjusting for allocations already done
						inventoryAvailableQuantity = parseFloat(inventoryAvailableQuantity) - parseFloat(alreadyAllocQty);

						nlapiLogExecution('Debug', 'inventoryAvailableQuantity', inventoryAvailableQuantity);

						var currentRow = [recno, actualQty, inventoryAvailableQuantity, allocationQuantity, packCode,
						                  itemStatus, whLocation, fromLP, recordId,inventoryItem,fromLot];

						binLocnInvDtls.push(currentRow);

						matchFound = true;
					}
				}
			}

			else if((dowhlocation==InvWHLocation) && (parseFloat(invBinLocnId) == parseFloat(binLocation)) && (fulfilmentItem==invItem)
					&& (itemStatus==fulfillmentItemStatus)){

				var actualQty = inventorySearchResults[i].getValue('custrecord_ebiz_qoh');
				var inventoryAvailableQuantity = inventorySearchResults[i].getValue('custrecord_ebiz_avl_qty');
				var allocationQuantity = inventorySearchResults[i].getValue('custrecord_ebiz_alloc_qty');
				var packCode = inventorySearchResults[i].getValue('custrecord_ebiz_inv_packcode');					
				var whLocation = inventorySearchResults[i].getValue('custrecord_ebiz_inv_loc');
				var fromLP = inventorySearchResults[i].getValue('custrecord_ebiz_inv_lp');
				var inventoryItem=inventorySearchResults[i].getValue('custrecord_ebiz_inv_sku');
				var fromLot = inventorySearchResults[i].getText('custrecord_ebiz_inv_lot');

				var str = 'Invt QOH. = ' + actualQty + '<br>';
				str = str + 'Alloc Qty. = ' + allocationQuantity + '<br>';	
				str = str + 'Avail Qty. = ' + inventoryAvailableQuantity + '<br>';
				str = str + 'packCode. = ' + packCode + '<br>';
				str = str + 'itemStatus. = ' + itemStatus + '<br>';
				str = str + 'whLocation. = ' + whLocation + '<br>';
				str = str + 'fromLP. = ' + fromLP + '<br>';
				str = str + 'inventoryItem. = ' + inventoryItem + '<br>';
				str = str + 'fromLot. = ' + fromLot + '<br>';

				nlapiLogExecution('Debug', 'Inventory Details', str);

				var recordId = inventorySearchResults[i].getId();

				// Get allocation already done
				var alreadyAllocQty = getAlreadyAllocatedInv(allocatedInv, binLocation,fromLP,inventoryItem,fromLot);

				nlapiLogExecution('Debug', 'alreadyAllocQty', alreadyAllocQty);

				// Adjusting for allocations already done
				inventoryAvailableQuantity = parseFloat(inventoryAvailableQuantity) - parseFloat(alreadyAllocQty);

				nlapiLogExecution('Debug', 'inventoryAvailableQuantity', inventoryAvailableQuantity);

				var currentRow = [i, actualQty, inventoryAvailableQuantity, allocationQuantity, packCode,
				                  itemStatus, whLocation, fromLP, recordId,inventoryItem,fromLot];

				binLocnInvDtls.push(currentRow);

				matchFound = true;
			}
			//}
		}
	}

	nlapiLogExecution('Debug','Out of getAvailQtyFromInventory');
	return binLocnInvDtls;
}

function getAllocConfigForOrder(allocConfigDtls, index){
	var allocConfig = new Array();
	var matchFound = false;

	if(allocConfigDtls != null && allocConfigDtls.length > 0){
		for (var i = 0; i < allocConfigDtls.length; i++){
			if(!matchFound){
				if(parseFloat(allocConfigDtls[i][0]) == parseFloat(index)){
					allocConfig = allocConfigDtls[i];
					matchFound = true;
				}
			}
		}
	}

	return allocConfig;
}

/**********************************************
 * This function splits the line
 * based on Item Dimensions for each items
 * sublist in a Sales Order.
 *
 */
function pickgen_palletisation(itemId,ordqty,AllItemDims,whLocation)
{
	nlapiLogExecution('Debug', 'Into pickgen_palletisation');
	var str = 'itemId. = ' + itemId + '<br>';
	str = str + 'ordqty. = ' + ordqty + '<br>';	
	str = str + 'AllItemDims. = ' + AllItemDims + '<br>';

	nlapiLogExecution('Debug', 'Parameter Details', str);

	try {
		var skulist=new Array();
		skulist.push(itemId);
		var arrLPRequired=new Array();			
		//var AllItemDims = getItemDimensions(skulist);
		var eachQty = 0;
		var caseQty = 0;
		var palletQty = 0;
		var InnerPackQty = 0;
		var noofPallets = 0;
		var noofCases = 0;
		var noofInnerpack = 0;
		var remQty = 0;
		var tOrdQty = ordqty;
		var eachpackflag="";
		var casepackflag="";
		var palletpackflag="";
		var InnerPackflag="";
		var eachweight="";
		var caseweight="";
		var palletweight="";
		var InnerPackweight="";
		var eachcube="";
		var casecube="";
		var palletcube="";
		var InnerPackcube="";


		var eachUom="";
		var caseUom="";
		var palletUom="";
		var innerpackUom="";
		var baseuomid='';

		//AllItemDims = [sku, uom, uomLevel, qty, baseUOMFlag, packflag,weight,cube];

		if (AllItemDims != null && AllItemDims.length > 0) 
		{	
			nlapiLogExecution('Debug', 'AllItemDims.length', AllItemDims.length);
			for(var p = 0; p < AllItemDims.length; p++)
			{
				//var itemval = AllItemDims[p].getValue('custrecord_ebizitemdims');
				//var itemtext = AllItemDims[p].getText('custrecord_ebizitemdims');

				var itemval = AllItemDims[p][0];

				if(AllItemDims[p][0] == itemId)
				{
					nlapiLogExecution('Debug', 'Item Dims configured for SKU: ', itemId);

					var skuDim = AllItemDims[p][2];
					var skuQty = AllItemDims[p][3];	
					var packflag = AllItemDims[p][5];	
					var weight = AllItemDims[p][6];
					var cube = AllItemDims[p][7];
					var uom=AllItemDims[p][1];
					var bulkpick = AllItemDims[p][8];
					var baseuomflag = AllItemDims[p][4];

					if(baseuomflag=='T')
					{
						baseuomid=uom;
					}

//					nlapiLogExecution('Debug', 'UOM Level : ', skuDim);
//					nlapiLogExecution('Debug', 'UOM Qty : ', skuQty);
//					nlapiLogExecution('Debug', 'UOM Pack Flag : ', packflag);
//					nlapiLogExecution('Debug', 'Weight : ', weight);
//					nlapiLogExecution('Debug', 'Cube : ', cube);

					//UOM LEVEL(Internal ID)  : 1-EACH		2-CASE		3-PALLET	4-InnerPack

					if(skuDim == '1'){
						eachQty = skuQty;
						eachpackflag=packflag;
						eachweight = weight;
						eachcube = cube;

						eachUom=uom;
					}								
					else if(skuDim == '2'){
						caseQty = skuQty;
						casepackflag=packflag;
						caseweight = weight;
						casecube=cube;

						caseUom= uom;
					}								
					else if(skuDim == '3'){
						palletQty = skuQty;
						palletpackflag = packflag;
						palletweight=weight;
						palletcube=cube;

						palletUom=uom;
					}	
					else if(skuDim == '4'){
						InnerPackQty = skuQty;
						InnerPackflag = packflag;
						InnerPackweight=weight;
						InnerPackcube=cube;

						innerpackUom=uom;
					}
				}	
			}

			if(parseFloat(eachQty) == 0)
			{
				nlapiLogExecution('Debug', 'UOM Dims', 'Item dim EACH is not configured for SKU: '+ itemId);
			}
			if(parseFloat(caseQty) == 0)
			{
				nlapiLogExecution('Debug', 'UOM Dims', 'Item dim CASE is Not Configured for SKU: '+ itemId);
			}
			if(parseFloat(palletQty) == 0)
			{
				nlapiLogExecution('Debug', 'UOM Dims', 'Item dim PALLET is not Configured for SKU: '+ itemId);
			}

			//Packflag : 1-Ship Cartons		2-Build Cartons		3-Ship Pallets

			if(eachQty>0 ||caseQty>0 || palletQty>0) 
			{
				var noofLpsRequired = 0;

				// Get the number of pallets required for this item (there will be one LP per pallet)
				noofPallets = getSKUDimCount(ordqty, palletQty);
				remQty = parseFloat(ordqty) - (noofPallets * palletQty);				

				var skuPltRecord=new Array();
				if(noofPallets > 0){
					if(palletpackflag==1 || palletpackflag==3)
					{
						noofLpsRequired = parseFloat(noofPallets);
						skuPltRecord = [Math.round(noofPallets), '3', palletQty, palletpackflag,palletweight,palletcube,palletQty,bulkpick,palletUom];					
					}
					else
					{
						var palletTaskQty=Math.round(noofPallets * palletQty);
						noofPallets=1;
						noofLpsRequired = parseFloat(noofPallets);
						skuPltRecord = [Math.round(noofPallets), '3', palletTaskQty, palletpackflag,palletweight,palletcube,palletQty,bulkpick,palletUom];
					}
				}
				else{						
					skuPltRecord = [Math.round(noofPallets), '3', palletQty, palletpackflag,palletweight,palletcube,palletQty,bulkpick,palletUom,baseuomid];
				}
				arrLPRequired[0] = skuPltRecord ;

				nlapiLogExecution('Debug', 'pickgen_palletisation:No. of Pallets', noofPallets);
				nlapiLogExecution('Debug', 'pickgen_palletisation:Remaining Quantity', remQty);


				// check whether we need to break down into cases or not
				if (parseFloat(remQty) > 0 && parseFloat(caseQty) != 0) 
				{
					// Get the number of cases required for this item (only one LP for all cases)
					noofCases = getSKUDimCount(remQty, caseQty);//
					remQty = parseFloat(remQty) - parseFloat(noofCases * caseQty);
					nlapiLogExecution('Debug', 'remQty@@', remQty);
					nlapiLogExecution('Debug', 'noofCases@@', noofCases);
					var skuCaseRecord=new Array();

					if(noofCases > 0){
						noofLpsRequired = parseFloat(noofLpsRequired) + 1;

						if(casepackflag==1 || casepackflag==3)
						{
							skuCaseRecord = [Math.round(noofCases), '2', caseQty, casepackflag,caseweight,casecube,caseQty,bulkpick,caseUom,baseuomid];							
						}
						else{
							var caseTaskQty=Math.round(noofCases * caseQty);
							noofCases=1;
							skuCaseRecord = [Math.round(noofCases), '2', caseTaskQty, casepackflag,caseweight,casecube,caseQty,bulkpick,caseUom,baseuomid];
						}
					}
					else{						
						skuCaseRecord = [Math.round(noofCases), '2', caseQty, casepackflag,caseweight,casecube,caseQty,bulkpick,caseUom,baseuomid];
					}

					arrLPRequired[1] = skuCaseRecord ;

					nlapiLogExecution('Debug', 'pickgen_palletisation:No. of Cases', noofCases);
					nlapiLogExecution('Debug', 'pickgen_palletisation:Remaining Quantity', remQty);
				}
				else{
					var skuCaseRecord = [Math.round(0), '2', caseQty, casepackflag,caseweight,casecube,caseQty,bulkpick,caseUom,baseuomid];
					arrLPRequired[1] = skuCaseRecord ;					
				}

				// check whether we need to break down into InnerPack or not
				if (parseFloat(remQty) > 0 && parseFloat(InnerPackQty) != 0) 
				{
					// Get the number of innerpack required for this item (only one LP for all innerpack)
					noofInnerpack = getSKUDimCount(remQty, InnerPackQty);
					remQty = parseFloat(remQty) - (noofInnerpack * InnerPackQty);

					var skuInnerpackRecord=new Array();

					if(noofInnerpack > 0){
						noofLpsRequired = parseFloat(noofLpsRequired) + 1;

						if(InnerPackflag==1 || InnerPackflag==3)
						{
							skuInnerpackRecord = [Math.round(noofInnerpack), '4', InnerPackQty, InnerPackflag ,InnerPackweight,InnerPackcube,InnerPackQty,bulkpick,innerpackUom,baseuomid];							
						}
						else{
							var InnerpackTaskQty=Math.round(noofInnerpack * InnerPackQty);
							noofInnerpack=1;
							skuInnerpackRecord = [Math.round(noofInnerpack), '4', InnerpackTaskQty, InnerPackflag ,InnerPackweight,InnerPackcube,InnerPackQty,bulkpick,innerpackUom,baseuomid];	
						}
					}
					else{						
						skuInnerpackRecord = [Math.round(noofInnerpack), '4', InnerPackQty, InnerPackflag ,InnerPackweight,InnerPackcube,InnerPackQty,bulkpick,innerpackUom,baseuomid];
					}

					arrLPRequired[2] = skuInnerpackRecord ;

					nlapiLogExecution('Debug', 'pickgen_palletisation:No. of Innerpack', noofInnerpack);
					nlapiLogExecution('Debug', 'pickgen_palletisation:Remaining Quantity', remQty);
				}
				else{
					var skuinnerpackRecord = [Math.round(0), '4', InnerPackQty, InnerPackflag ,InnerPackweight,InnerPackcube,InnerPackQty,bulkpick,innerpackUom,baseuomid];
					arrLPRequired[2] = skuinnerpackRecord ;					
				}

				nlapiLogExecution('Debug', 'pickgen_palletisation:Remaining Quantity (Eaches)', remQty);

				// check whether we need to further break down into eaches or not (there will be one LP for all eaches)
				if (parseFloat(remQty) > 0)  {

					if(eachpackflag==1 || eachpackflag==3)
					{
						var skuEachRecord = [Math.round(remQty), '1', eachQty, eachpackflag ,eachweight,eachcube,eachQty,bulkpick,eachUom,baseuomid];							
					}
					else{
						var skuEachRecord = [Math.round(1), '1', remQty, eachpackflag,eachweight,eachcube,eachQty,bulkpick,eachUom,baseuomid];	
					}		

					arrLPRequired[3] = skuEachRecord ;	

//					var skuEachRecord = [Math.round(1), '1', remQty, eachpackflag,eachweight,eachcube,eachQty];
//					arrLPRequired[3] = skuEachRecord ;		
				}
			}
		} 
		else 
		{ 
			nlapiLogExecution('Debug', 'UOM Dims', 'For all items, Dimensions are not configured ');
		}		
	} 
	catch(exp) {
		nlapiLogExecution('Debug', 'Exception in pickgen_palletisation:', exp);	
		return arrLPRequired;
	}
	nlapiLogExecution('Debug', 'out of pickgen_palletisation', itemId);
	return arrLPRequired;
}

function CreateMasterLPRecord(vContainerLpNo,vContainerSize,TotalWeight,ContainerCube,uompackflag,location)
{
	var ResultText=GenerateLable(uompackflag,location);
	var MastLP=nlapiCreateRecord('customrecord_ebiznet_master_lp');		
	MastLP.setFieldValue('name', vContainerLpNo);
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_lp', vContainerLpNo);
	if(vContainerSize!=null && vContainerSize!='')
		MastLP.setFieldValue('custrecord_ebiz_lpmaster_sizeid', vContainerSize);
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_totwght', parseFloat(TotalWeight));
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(ContainerCube));
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_sscc', ResultText);	 
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_site', location);	 
	MastLP.setFieldValue('custrecord_ebiz_lpmaster_lptype', 2);	

	var currentContext = nlapiGetContext();
	var retktoval = nlapiSubmitRecord(MastLP);
	nlapiLogExecution('Debug', 'Return Value from CreateMasterLPRecord',retktoval);
}

function CreateMasterLPRecordBulk(vContainerLpNo,vContainerSize,TotalWeight,ContainerCube,uompackflag,location,LPparent)
{
	var ResultText=GenerateLable(uompackflag,location);
	LPparent.selectNewLineItem('recmachcustrecord_ebiz_lp_parent');
	if(vContainerLpNo!=null && vContainerLpNo!='')
		LPparent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','name', vContainerLpNo); 
	if(vContainerLpNo!=null && vContainerLpNo!='')
		LPparent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_lp', vContainerLpNo);
	if(vContainerSize!=null && vContainerSize!='')
		LPparent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_sizeid', vContainerSize); 
	LPparent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_totwght', parseFloat(TotalWeight));
	LPparent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_totcube', parseFloat(ContainerCube)); 
	if(ResultText!=null && ResultText!='')
		LPparent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_sscc', ResultText); 
	if(location!=null && location!='')
		LPparent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_site', location); 
	LPparent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_lptype', 2); 
	LPparent.commitLineItem('recmachcustrecord_ebiz_lp_parent');	

	nlapiLogExecution('Debug', 'Return Value from CreateMasterLPRecord Bulk creation');
}

function GenerateLable(uompackflag,location){

	nlapiLogExecution('Debug', 'CreateMasterLPRecord uompackflag', uompackflag);
	var finaltext="";
	var duns="";
	var label="",uom="",ResultText="";
	//added by mahesh


	try 
	{	
		var lpMaxValue=GetMaxLPNo('1', '5',location);
		nlapiLogExecution('Debug', 'lpMaxValue', lpMaxValue);

		var prefixlength=lpMaxValue.length;
		nlapiLogExecution('Debug', 'prefixlength', prefixlength);

		if(prefixlength==0)
			label="000000000";
		else if(prefixlength==1)
			label="00000000"+lpMaxValue;
		else if(prefixlength==2)
			label="0000000"+lpMaxValue;
		else if(prefixlength==3)
			label="000000"+lpMaxValue;
		else if(prefixlength==4)
			label="00000"+lpMaxValue;
		else if(prefixlength==5)
			label="0000"+lpMaxValue;
		else if(prefixlength==6)
			label="000"+lpMaxValue;
		else if(prefixlength==7)
			label="00"+lpMaxValue;
		else if(prefixlength==8)
			label="0"+lpMaxValue;
		else if(prefixlength==9)
			label=lpMaxValue;

		//to get company id

		duns=GetCompanyDUNSnumber('');

		if(uompackflag == "1" || uompackflag == "EACH" ) 
			uom="0"; 
		else if(uompackflag == "3" || uompackflag == "PALLET") 
			uom="2";
		else
			uom="0";
		nlapiLogExecution('Debug', 'uom', uom);
		nlapiLogExecution('Debug', 'CreateMasterLPRecord finaltext', finaltext);
		finaltext=uom+duns+label;
		nlapiLogExecution('Debug', 'CreateMasterLPRecord finaltext', finaltext);



		//to get chk digit
		var checkStr=finaltext;
		var ARL=0;
		var BRL=0;
		var CheckDigitValue="";
		for (i = checkStr.length-1;  i > 0;  i--)
		{
			ARL = ARL+parseInt(checkStr.charAt(i));
			i--;
		}		
		ARL=ARL*3;
		for (i = checkStr.length-2;  i > 0;  i--)
		{
			BRL = BRL+parseInt(checkStr.charAt(i));
			i--;
		}		
		var sumOfARLBRL=ARL+BRL;
		var CheckDigit=0;

		while(CheckDigit<10)
		{
			if(sumOfARLBRL%10==0)
			{ 
				CheckDigitValue=CheckDigit; 
				break; 
			} 

			sumOfARLBRL++;
			CheckDigit++;
		}
		nlapiLogExecution('Debug', 'CreateMasterLPRecord CheckDigit', CheckDigitValue.toString());
		//CASE # 201412758 Start
		ResultText="00"+finaltext+CheckDigitValue.toString();
		//ResultText=finaltext+CheckDigitValue.toString();
		nlapiLogExecution('Debug', 'CreateMasterLPRecord ResultText', ResultText);
		//CASE # 201412758 end
	} 
	catch (err) 
	{

	}
	return ResultText;
}

var vInvtArr=new Array();
var vInvtIdArr=new Array();

function updateInventoryRecord(inventoryresults, allocateqty){

	nlapiLogExecution('Debug', 'Into updateInventoryRecord...');

	if (allocateqty == "" || isNaN(allocateqty)) {
		allocateqty = 0;
	}

	if(vInvtIdArr.indexOf(inventoryresults.getId()) == -1)
	{
		nlapiLogExecution('Debug','Into IF1');
		vInvtIdArr.push(inventoryresults.getId());
		// case no 20126835� start

		var prevallocqty='';
		// commented becuase already we are adding old allloc qty + new qty in bulk update function
		//var prevallocqty=inventoryresults.getValue('custrecord_ebiz_alloc_qty');
		// case no 20126835� end

		nlapiLogExecution('Debug','prevallocqty',prevallocqty);
		if (prevallocqty == "" || isNaN(prevallocqty)) {
			prevallocqty = 0;
		}
		var totalAllocQuantity = parseInt(prevallocqty) + parseInt(allocateqty);
		nlapiLogExecution('Debug','totalAllocQuantity',totalAllocQuantity);

		var vInvt=[inventoryresults,totalAllocQuantity];
		vInvtArr.push(vInvt);
	}
	else
	{
		nlapiLogExecution('Debug','Into Else Exists');
		var vInvtIndex=vInvtIdArr.indexOf(inventoryresults.getId());
		var vPrevAllocQty=vInvtArr[vInvtIndex][1];
		nlapiLogExecution('Debug','vPrevAllocQty Exists',vPrevAllocQty);
		var totalAllocQuantity = parseInt(vPrevAllocQty) + parseInt(allocateqty);
		nlapiLogExecution('Debug','totalAllocQuantity Exists',totalAllocQuantity);
		vInvtArr[vInvtIndex][1]=totalAllocQuantity;
	}

	nlapiLogExecution('Debug', 'Out of updateInventoryRecord...');
}

function updateInventoryWithAllocationbulk(invrecord, allocationQuantity,parent){
	nlapiLogExecution('Debug', 'Into updateInventoryWithAllocation - Qty', allocationQuantity);

	parent.selectNewLineItem('recmachcustrecord_ebiz_inv_parent');

	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'id', invrecord.getId());
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','name', invrecord.getValue('name'));
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_binloc', invrecord.getValue('custrecord_ebiz_inv_binloc'));
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_lp', invrecord.getValue('custrecord_ebiz_inv_lp'));
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_sku', invrecord.getValue('custrecord_ebiz_inv_sku'));
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_sku_status', invrecord.getValue('custrecord_ebiz_inv_sku_status'));
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_packcode', invrecord.getValue('custrecord_ebiz_inv_packcode'));
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_qty', invrecord.getValue('custrecord_ebiz_inv_qty'));
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_inv_ebizsku_no', invrecord.getValue('custrecord_ebiz_inv_sku'));
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_qoh', invrecord.getValue('custrecord_ebiz_qoh'));
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_itemdesc', invrecord.getValue('custrecord_ebiz_itemdesc'));
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_loc', invrecord.getValue('custrecord_ebiz_inv_loc'));
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_invttasktype', invrecord.getValue('custrecord_invttasktype')); // PUTW.
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_wms_inv_status_flag', 19); // STORAGE	
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_callinv', 'N');
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_displayfield', 'N');
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_fifo', invrecord.getValue('custrecord_ebiz_inv_fifo'));	
	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_updated_user_no',  nlapiGetContext().getUser());

	var alreadyallocqty = invrecord.getValue('custrecord_ebiz_alloc_qty');
	if (isNaN(alreadyallocqty))
	{
		alreadyallocqty = 0;
	}

	var totalallocqty=parseInt(allocationQuantity)+parseInt(alreadyallocqty);

	parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_alloc_qty', parseInt(totalallocqty));

	if(invtuomlevel!=null && invtuomlevel!='')
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_uomlvl', invrecord.getValue('custrecord_ebiz_uomlvl'));
	if(invtlot!=null && invtlot!='')
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_lot', invrecord.getValue('custrecord_ebiz_inv_lot'));
	if(invtcompany!=null && invtcompany!='')
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_company', invrecord.getValue('custrecord_ebiz_inv_company'));
	if(invtadjtype!=null && invtadjtype!='')
		parent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent','custrecord_ebiz_inv_adjusttype', invrecord.getValue('custrecord_ebiz_inv_adjusttype'));

	parent.commitLineItem('recmachcustrecord_ebiz_inv_parent');	 


	nlapiLogExecution('Debug', 'Out of updateInventoryWithAllocation - Record ID');

}

function updateInventoryWithAllocation(recordId, allocationQuantity){
	nlapiLogExecution('Debug', 'Into updateInventoryWithAllocation - Qty', allocationQuantity);

	var scount=1;
	LABL1: for(var j=0;j<scount;j++)
	{	
		nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', j);
		try
		{
			var inventoryTransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', recordId);
			var alreadyallocqty = inventoryTransaction.getFieldValue('custrecord_ebiz_alloc_qty');
			if (isNaN(alreadyallocqty)||alreadyallocqty==null||alreadyallocqty=="")
			{
				alreadyallocqty = 0;
			}
			var totalallocqty=parseInt(allocationQuantity)+parseInt(alreadyallocqty);
			inventoryTransaction.setFieldValue('custrecord_ebiz_alloc_qty', parseInt(totalallocqty));
			inventoryTransaction.setFieldValue('custrecord_ebiz_callinv', 'N');
			inventoryTransaction.setFieldValue('custrecord_ebiz_displayfield', 'N');

			nlapiSubmitRecord(inventoryTransaction,false,true);
			nlapiLogExecution('Debug', 'Out of updateInventoryWithAllocation - Record ID',recordId);

		}
		catch(ex)
		{
			var exCode='CUSTOM_RECORD_COLLISION'; 

			if (ex instanceof nlobjError) 
			{	
				wmsE=ex.getCode() + '\n' + ex.getDetails();
				exCode=ex.getCode();
			}
			else
			{
				wmsE=ex.toString();
				exCode=ex.toString();
			}				 
			if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
			{ 
				scount=scount+1;
				continue LABL1;
			}
			else break LABL1;
		}
	}




}

function IsFailedTaskExist(fulfillOrderList,waveNo,uomlevel)
{
	var IsFailedTaskFound='F';
	nlapiLogExecution('Debug', 'into IsFailedTaskExist ');

	var filter=new Array();
	filter.push(new nlobjSearchFilter('name',null,'is',fulfillOrderList[4]));
	filter.push(new nlobjSearchFilter('custrecord_ebiz_wave_no',null,'is',waveNo));
	filter.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no',null,'is',fulfillOrderList[3]));
	filter.push(new nlobjSearchFilter('custrecord_line_no',null,'is',fulfillOrderList[2]));
	filter.push(new nlobjSearchFilter('custrecord_uom_level',null,'is',uomlevel));
	filter.push(new nlobjSearchFilter('custrecord_wms_status_flag',null,'anyof',['26']));
	filter.push(new nlobjSearchFilter('custrecord_tasktype',null,'anyof',['3']));

	var column=new Array();
	column[0]=new nlobjSearchColumn('custrecord_ebiz_wave_no');

	var searchresult=nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filter, column);
	if(searchresult!=null && searchresult!='' && searchresult.length>0)
	{
		IsFailedTaskFound='T';
	}

	nlapiLogExecution('Debug', 'out of IsFailedTaskExist', IsFailedTaskFound);
	return IsFailedTaskFound;
}



function createpicktask(fulfillOrderList,expectedQuantity, waveNo, containerLP, beginLocationId, recordId,packCode,
		itemStatus, pickMethod, pickZone, pickStrategyId, whLocation, LP,uomlevel,totalweight,totalcube,containersize,fromLot,
		vnotes,wmsCarrier,fulfilmentItem,nsrefno,customer,dimsbulkpick,otparent,vwaveassignedto,baseuomid,expiryDate)
{

	var currentContext = nlapiGetContext();  
	var currentUserID = currentContext.getUser();

	var pickrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');

	pickrecord.setFieldValue('name', fulfillOrderList[4]);
	if(fulfillOrderList[6]!=null && fulfillOrderList[6]!='')
		pickrecord.setFieldValue('custrecord_parent_sku_no', fulfillOrderList[6]);
	pickrecord.setFieldValue('custrecord_ebiz_sku_no', fulfilmentItem);
	pickrecord.setFieldValue('custrecord_sku', fulfilmentItem);
	pickrecord.setFieldValue('custrecord_expe_qty', expectedQuantity);
	pickrecord.setFieldValue('custrecord_container_lp_no', containerLP);
	pickrecord.setFieldValue('custrecord_ebiz_lpno', containerLP);
	pickrecord.setFieldValue('custrecord_line_no', fulfillOrderList[2]);
	pickrecord.setFieldValue('custrecord_tasktype', '3');
	pickrecord.setFieldValue('custrecord_line_no', fulfillOrderList[2]);
	pickrecord.setFieldValue('custrecord_actbeginloc', beginLocationId);

	if(beginLocationId!=null && beginLocationId!='')
	{
		var locgroupfields = ['custrecord_inboundlocgroupid','custrecord_outboundlocgroupid'];
		var locgroupcolumns = nlapiLookupField('customrecord_ebiznet_location', beginLocationId, locgroupfields);		
		var inblocgroupid=locgroupcolumns.custrecord_inboundlocgroupid;
		var oublocgroupid=locgroupcolumns.custrecord_outboundlocgroupid;

		var locgroupseqfields = ['custrecord_sequenceno'];
		if(oublocgroupid!=null && oublocgroupid!="")
		{
			var locgroupseqcolumns = nlapiLookupField('customrecord_ebiznet_loc_group', oublocgroupid, locgroupseqfields);
			var locgroupidseq=locgroupseqcolumns.custrecord_sequenceno;

			pickrecord.setFieldValue('custrecord_bin_locgroup_seq', locgroupidseq);
		}
	}

	var vwavearr = waveNo.split('.');

	if(vwavearr.length==1)
		waveNo=waveNo.toString()+'.0';

	if(fulfillOrderList[3] != null && fulfillOrderList[3] != '')
		pickrecord.setFieldValue('custrecord_ebiz_cntrl_no', fulfillOrderList[3]); 
	if(fulfillOrderList[1] != null && fulfillOrderList[1] != '')
		pickrecord.setFieldValue('custrecord_ebiz_order_no', parseInt(fulfillOrderList[1])); 
	if(fulfillOrderList[3] != null && fulfillOrderList[3] != '')
		pickrecord.setFieldValue('custrecord_ebiz_receipt_no', fulfillOrderList[3]); 
	pickrecord.setFieldValue('custrecord_ebiz_wave_no', waveNo); 
	pickrecord.setFieldValue('custrecordact_begin_date', DateStamp()); 
	pickrecord.setFieldValue('custrecord_from_lp_no', LP); 
	pickrecord.setFieldValue('custrecord_lpno', LP);
	pickrecord.setFieldValue('custrecord_actualbegintime', TimeStamp());
	pickrecord.setFieldValue('custrecord_invref_no', recordId); 
	if(fulfillOrderList[10] != null && fulfillOrderList[10] != '')
		pickrecord.setFieldValue('custrecord_packcode', fulfillOrderList[10]); 
	else
		pickrecord.setFieldValue('custrecord_packcode', 1); 

	pickrecord.setFieldValue('custrecord_sku_status', itemStatus);
	//if(fulfillOrderList[11] != null && fulfillOrderList[11] != '')
	//	pickrecord.setFieldValue('custrecord_uom_id', fulfillOrderList[11]); 
	if(baseuomid!=null && baseuomid!='' && baseuomid!='null')
		pickrecord.setFieldValue('custrecord_uom_id', baseuomid);
	if(fulfillOrderList[12] != null && fulfillOrderList[12] != '')
		pickrecord.setFieldValue('custrecord_batch_no', fulfillOrderList[12]); 
	pickrecord.setFieldValue('custrecord_ebizrule_no', pickStrategyId);
	pickrecord.setFieldValue('custrecord_ebizmethod_no', pickMethod); 
	pickrecord.setFieldValue('custrecord_ebizzone_no', pickZone);
	//added if condtion. temparary fix when ever system excutes scheduler it is throwing userID as -4 (System)
	if(parseInt(currentUserID)>0)
	{
		pickrecord.setFieldValue('custrecord_ebizuser', currentUserID); 
		pickrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID); 
	}
	if(vwaveassignedto!=null && vwaveassignedto!='' && vwaveassignedto!='null')
		pickrecord.setFieldValue('custrecord_taskassignedto', vwaveassignedto); 
	pickrecord.setFieldValue('custrecord_uom_level', uomlevel);
	pickrecord.setFieldValue('custrecord_total_weight', totalweight);
	pickrecord.setFieldValue('custrecord_totalcube', totalcube); 
	if(containersize != null && containersize != '')
		pickrecord.setFieldValue('custrecord_container', containersize);
	pickrecord.setFieldValue('custrecord_batch_no', fromLot);
	pickrecord.setFieldValue('custrecord_notes', vnotes); 
	if(fulfillOrderList[13] != null && fulfillOrderList[13] != '')
		pickrecord.setFieldValue('custrecord_comp_id', fulfillOrderList[13]); 
	if(fulfillOrderList[41] != null && fulfillOrderList[41] != '')
		pickrecord.setFieldValue('custrecord_ebiz_act_solocation', fulfillOrderList[41]);

	if(wmsCarrier != null && wmsCarrier != "")
		pickrecord.setFieldValue('custrecord_ebizwmscarrier', wmsCarrier); 

	if(beginLocationId == "")
		pickrecord.setFieldValue('custrecord_wms_status_flag', '26'); // Failed Picks
	else
		pickrecord.setFieldValue('custrecord_wms_status_flag', '9'); // Pick Location Assigned

	if(pickZone!=null && pickZone!='')
		pickrecord.setFieldValue('custrecord_ebiz_zoneid', pickZone); 

	if(whLocation != null && whLocation!='')
		pickrecord.setFieldValue('custrecord_wms_location', whLocation);

	if(nsrefno != null && nsrefno!='')
		pickrecord.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', nsrefno); 

	pickrecord.setFieldValue('custrecord_skiptask', '0'); 

	if(dimsbulkpick=='T')
		pickrecord.setFieldValue('custrecord_ebiz_shipasis', 'T'); 

	if(expiryDate != null && expiryDate != '' && expiryDate!='null')
		pickrecord.setFieldValue('custrecord_expirydate', expiryDate); 

	if(customer != null && customer!='')
		pickrecord.setFieldValue('custrecord_ebiz_customer', customer); 

	var pickrecordid = nlapiSubmitRecord(pickrecord);

}

function createRecordInOpenTask(fulfillOrderList,expectedQuantity, waveNo, containerLP, beginLocationId, recordId,packCode,
		itemStatus, pickMethod, pickZone, pickStrategyId, whLocation, LP,uomlevel,totalweight,totalcube,containersize,fromLot,
		vnotes,wmsCarrier,fulfilmentItem,nsrefno,customer,dimsbulkpick,otparent,vwaveassignedto,baseuomid,expiryDate){

	nlapiLogExecution('Debug', 'Into createRecordInOpenTaskBulk ...');
//	nlapiLogExecution('Debug', 'Creating task for qty ...',expectedQuantity);
//	nlapiLogExecution('Debug', 'fulfillOrderList ',fulfillOrderList);
//	nlapiLogExecution('Debug', 'fulfilmentItem ',fulfilmentItem);
//	nlapiLogExecution('Debug', 'expectedQuantity ',expectedQuantity);
//	nlapiLogExecution('Debug', 'containerLP ',containerLP);
//	nlapiLogExecution('Debug', 'beginLocationId ',beginLocationId);

	var currentContext = nlapiGetContext();  
	var currentUserID = currentContext.getUser();	

	otparent.selectNewLineItem('recmachcustrecord_ebiz_ot_parent');
	if(fulfillOrderList[4] != null && fulfillOrderList[4] != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','name', fulfillOrderList[4]); // FULFILLMENT ORDER #
	if(fulfillOrderList[6] != null && fulfillOrderList[6] != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_parent_sku_no', fulfillOrderList[6]); // ITEM INTERNAL ID
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_sku_no', fulfilmentItem); // ITEM INTERNAL ID
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_sku', fulfilmentItem); // ITEM NAME
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_expe_qty', expectedQuantity); // FULFILMENT ORDER QUANTITY
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_container_lp_no', containerLP); // CONTAINER LP
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_lpno', containerLP); // CONTAINER LP
	if(fulfillOrderList[2] != null && fulfillOrderList[2] != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_line_no', fulfillOrderList[2]); // ORDER LINE #
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_tasktype', '3'); // TASK TYPE = PICK	
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actbeginloc', beginLocationId);  // BEGIN LOCATION
	//nlapiLogExecution('Debug', 'beginLocationId',beginLocationId);
	if(beginLocationId!=null && beginLocationId!='')
	{
		var locgroupfields = ['custrecord_inboundlocgroupid','custrecord_outboundlocgroupid'];
		var locgroupcolumns = nlapiLookupField('customrecord_ebiznet_location', beginLocationId, locgroupfields);		
		var inblocgroupid=locgroupcolumns.custrecord_inboundlocgroupid;
		var oublocgroupid=locgroupcolumns.custrecord_outboundlocgroupid;
		//nlapiLogExecution('DEBUG', 'inblocgroupid',inblocgroupid);
		var locgroupseqfields = ['custrecord_sequenceno'];
		//code added on 011012 by suman
		//Allow to insert sequence no only if we get some value form inbonlog group Id.
		//if(inblocgroupid!=null&&inblocgroupid!="")
		if(oublocgroupid!=null && oublocgroupid!="")
		{
			//var locgroupseqcolumns = nlapiLookupField('customrecord_ebiznet_loc_group', inblocgroupid, locgroupseqfields);
			var locgroupseqcolumns = nlapiLookupField('customrecord_ebiznet_loc_group', oublocgroupid, locgroupseqfields);
			var locgroupidseq=locgroupseqcolumns.custrecord_sequenceno;
			//nlapiLogExecution('DEBUG', 'locgroupidseq',locgroupidseq);

			otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_bin_locgroup_seq', locgroupidseq);  // BEGIN LOCATION SEQ
		}

	}
//	nlapiLogExecution('Debug', 'waveNo',waveNo);
//	nlapiLogExecution('Debug', 'LP',LP);
//	nlapiLogExecution('Debug', 'recordId',recordId);
//	nlapiLogExecution('Debug', 'itemStatus',itemStatus);
//	nlapiLogExecution('Debug', 'pickStrategyId',pickStrategyId);
//	nlapiLogExecution('Debug', 'pickMethod',pickMethod);
//	nlapiLogExecution('Debug', 'pickZone',pickZone);
//	nlapiLogExecution('Debug', 'currentUserID',currentUserID);
//	nlapiLogExecution('Debug', 'uomlevel',uomlevel);
//	nlapiLogExecution('Debug', 'totalweight',totalweight);
//	nlapiLogExecution('Debug', 'totalcube',totalcube);
//	nlapiLogExecution('Debug', 'fromLot',fromLot);
//	nlapiLogExecution('Debug', 'vnotes',vnotes);
//	nlapiLogExecution('Debug', 'wmsCarrier',wmsCarrier);
//	nlapiLogExecution('Debug', 'pickZone',pickZone);
//	nlapiLogExecution('Debug', 'nsrefno',nsrefno);
//	nlapiLogExecution('Debug', 'whLocation',whLocation);
//	nlapiLogExecution('Debug', 'DateStamp()',DateStamp());
//	nlapiLogExecution('Debug', 'TimeStamp()',TimeStamp());

	var vwavearr = waveNo.split('.');

	if(vwavearr.length==1)
		waveNo=waveNo.toString()+'.0';

	if(fulfillOrderList[3] != null && fulfillOrderList[3] != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_cntrl_no', fulfillOrderList[3]); // FO INTRL ID
	if(fulfillOrderList[1] != null && fulfillOrderList[1] != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_order_no', parseInt(fulfillOrderList[1])); // SO INTR ID
	if(fulfillOrderList[3] != null && fulfillOrderList[3] != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_receipt_no', fulfillOrderList[3]); 
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_wave_no', waveNo);  // WAVE #
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecordact_begin_date', DateStamp()); // BEGIN DATE	
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_from_lp_no', LP); // PALLET LP
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_lpno', LP); // PALLET LP
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_actualbegintime', TimeStamp()); // BEGIN TIME
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_invref_no', recordId); // INV REC ID
	if(fulfillOrderList[10] != null && fulfillOrderList[10] != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_packcode', fulfillOrderList[10]); // PACKCODE
	else
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_packcode', 1); // PACKCODE

	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_sku_status', itemStatus); // ITEM STATUS
	/*if(fulfillOrderList[11] != null && fulfillOrderList[11] != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_uom_id', fulfillOrderList[11]);*/ // UOM ID

	//if(fulfillOrderList[46] != null && fulfillOrderList[46] != '')
	//	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_uom_id', fulfillOrderList[46]);

	if(baseuomid!=null && baseuomid!='' && baseuomid!='null')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_uom_id', baseuomid);

	if(fulfillOrderList[12] != null && fulfillOrderList[12] != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_batch_no', fulfillOrderList[12]); // LOT #
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebizrule_no', pickStrategyId); // PICK STRATEGY
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebizmethod_no', pickMethod); // PICK METHOD
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebizzone_no', pickZone); // PICK ZONE	
	//added if condtion. temparary fix when ever system excutes scheduler it is throwing userID as -4 (System)
	if(parseInt(currentUserID)>0)
	{
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebizuser', currentUserID); // CURRENT USER
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_upd_ebiz_user_no', currentUserID); // CURRENT USER
	}
	if(vwaveassignedto!=null && vwaveassignedto!='' && vwaveassignedto!='null')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_taskassignedto', vwaveassignedto); // CURRENT USER
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_uom_level', uomlevel); // UOM LEVEL
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_total_weight', totalweight); // TOTAL WEIGHT
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_totalcube', totalcube); // TOTAL CUBE
	if(containersize != null && containersize != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_container', containersize); // CONTAINER SIZE
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_batch_no', fromLot); // LOT #
	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_notes', vnotes); // NOTES
	if(fulfillOrderList[13] != null && fulfillOrderList[13] != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_comp_id', fulfillOrderList[13]); // COMP ID
	//case# 20149469 starts
	/*if(fulfillOrderList[39] != null && fulfillOrderList[39] != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_act_solocation', fulfillOrderList[39]);// Actual SO Location
	 */
	//nlapiLogExecution('DEBUG','fulfillOrderList[41]',fulfillOrderList[41]);
	if(fulfillOrderList[41] != null && fulfillOrderList[41] != '')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_act_solocation', fulfillOrderList[41]);// Actual SO Location
	//case# 20149469 ends
	if(wmsCarrier != null && wmsCarrier != "")
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebizwmscarrier', wmsCarrier); // WMS CARRIER

	if(beginLocationId == "")
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_status_flag', '26'); // STATUS FLAG = F
	else
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_status_flag', '9'); // STATUS FLAG = G

	if(pickZone!=null && pickZone!='')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_zoneid', pickZone); // PICK ZONE

	if(whLocation != null && whLocation!='')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_wms_location', whLocation); // SITE

	if(nsrefno != null && nsrefno!='')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_nsconfirm_ref_no', nsrefno); // NS Ref #

	otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_skiptask', '0'); // SKIP

	if(dimsbulkpick=='T')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_shipasis', 'T'); 


	//nlapiLogExecution('DEBUG','expiryDate2',expiryDate);
	if(expiryDate != null && expiryDate != '' && expiryDate!='null')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_expirydate', expiryDate); // Exp Date


	if(customer != null && customer!='')
		otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_customer', customer); // NS Ref #

	otparent.commitLineItem('recmachcustrecord_ebiz_ot_parent');	

	nlapiLogExecution('Debug', 'Out of createRecordInOpenTaskBulk ...');
}

function createRecordInOpenTaskold(fulfillOrderList,expectedQuantity, waveNo, containerLP, beginLocationId, recordId,packCode,
		itemStatus, pickMethod, pickZone, pickStrategyId, whLocation, LP,uomlevel,totalweight,totalcube,containersize,fromLot,
		vnotes,wmsCarrier,fulfilmentItem,nsrefno,otparent){
	nlapiLogExecution('Debug', 'into createRecordInOpenTask with container lp : ',containerLP);

	var IsFailedTaskFound='F';
	if(beginLocationId == "" || beginLocationId==null)
	{
		IsFailedTaskFound=IsFailedTaskExist(fulfillOrderList,waveNo,uomlevel);
	}

	if(IsFailedTaskFound=='F')
	{
		var openTaskRecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('Debug', 'fulfillOrderList[4]',fulfillOrderList[4]);
		nlapiLogExecution('Debug', 'test','test');
		openTaskRecord.setFieldValue('name', fulfillOrderList[4]);							// DELIVERY ORDER NAME
		openTaskRecord.setFieldValue('custrecord_ebiz_sku_no', fulfilmentItem);		// ITEM INTERNAL ID
		openTaskRecord.setFieldValue('custrecord_sku', fulfilmentItem);				// ITEM NAME
		openTaskRecord.setFieldValue('custrecord_parent_sku_no', fulfillOrderList[6]);
		openTaskRecord.setFieldValue('custrecord_expe_qty', expectedQuantity);				// FULFILMENT ORDER QUANTITY
		openTaskRecord.setFieldValue('custrecord_container_lp_no', containerLP);
		openTaskRecord.setFieldValue('custrecord_ebiz_lpno', containerLP);
		openTaskRecord.setFieldValue('custrecord_line_no', fulfillOrderList[2]);
		openTaskRecord.setFieldValue('custrecord_tasktype', '3'); 							// TASK TYPE = PICK

		openTaskRecord.setFieldValue('custrecord_actbeginloc', beginLocationId);
		openTaskRecord.setFieldValue('custrecord_ebiz_cntrl_no', fulfillOrderList[3]);
		openTaskRecord.setFieldValue('custrecord_ebiz_order_no', parseInt(fulfillOrderList[1]));
		openTaskRecord.setFieldValue('custrecord_ebiz_receipt_no', fulfillOrderList[3]);
		openTaskRecord.setFieldValue('custrecord_ebiz_wave_no',  waveNo);
		openTaskRecord.setFieldValue('custrecordact_begin_date',DateStamp());
		if(wmsCarrier != null && wmsCarrier != "")
			openTaskRecord.setFieldValue('custrecord_ebizwmscarrier',  wmsCarrier);
		// Setting for failed pick
		if(beginLocationId == "")
			openTaskRecord.setFieldValue('custrecord_wms_status_flag', '26'); 					// STATUS FLAG = F
		else
			openTaskRecord.setFieldValue('custrecord_wms_status_flag', '9'); 					// STATUS FLAG = G

		openTaskRecord.setFieldValue('custrecord_from_lp_no', LP);
		openTaskRecord.setFieldValue('custrecord_lpno', LP);
		openTaskRecord.setFieldValue('custrecord_actualbegintime', TimeStamp());
		openTaskRecord.setFieldValue('custrecord_invref_no', recordId);
		openTaskRecord.setFieldValue('custrecord_packcode', fulfillOrderList[10]);
		openTaskRecord.setFieldValue('custrecord_sku_status', itemStatus);
		openTaskRecord.setFieldValue('custrecord_uom_id', fulfillOrderList[11]);
		openTaskRecord.setFieldValue('custrecord_batch_no', fulfillOrderList[12]);
		openTaskRecord.setFieldValue('custrecord_ebizrule_no', pickStrategyId);
		openTaskRecord.setFieldValue('custrecord_ebizmethod_no', pickMethod);
		openTaskRecord.setFieldValue('custrecord_ebizzone_no', pickZone);
		if(pickZone!=null && pickZone!='')
			openTaskRecord.setFieldValue('custrecord_ebiz_zoneid', pickZone);
		openTaskRecord.setFieldValue('custrecord_comp_id', fulfillOrderList[13]);
		nlapiLogExecution('Debug', 'whLocation',whLocation);

		if(whLocation != null && whLocation!='')
			openTaskRecord.setFieldValue('custrecord_wms_location', whLocation);	

		var currentContext = nlapiGetContext();  
		var currentUserID = currentContext.getUser();		
		if(parseInt(currentUserID)>0)
		{
			openTaskRecord.setFieldValue('custrecord_ebizuser', currentUserID);		
			openTaskRecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);		
		}
		//openTaskRecord.setFieldValue('custrecord_taskassignedto', currentUserID);
		openTaskRecord.setFieldValue('custrecord_uom_level', uomlevel);
		openTaskRecord.setFieldValue('custrecord_total_weight', totalweight);
		openTaskRecord.setFieldValue('custrecord_totalcube', totalcube);
		openTaskRecord.setFieldValue('custrecord_container', containersize);
		openTaskRecord.setFieldValue('custrecord_batch_no', fromLot);
		openTaskRecord.setFieldValue('custrecord_notes', vnotes);
		openTaskRecord.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', nsrefno);

		nlapiLogExecution('Debug', 'fromLot',fromLot);
		nlapiSubmitRecord(openTaskRecord,false,true);


	}
	nlapiLogExecution('Debug', 'out of createRecordInOpenTask with container lp : ',containerLP);
}


function updateFulfilmentOrderline(fulfillmentOrder,orderQuantity,eBizWaveNo,pfoparent)
{
	if (orderQuantity == "" || isNaN(orderQuantity)) 
	{
		orderQuantity = 0;
	} 

	var fulfilmentOrderNo = fulfillmentOrder[3];

	nlapiLogExecution('Debug', 'Into updateFulfilmentOrderline: ',fulfilmentOrderNo);

	var fulfilmentOrderline = nlapiLoadRecord('customrecord_ebiznet_ordline', fulfilmentOrderNo);

	var prevpickgenqty=fulfilmentOrderline.getFieldValue('custrecord_pickgen_qty');
	if (prevpickgenqty == "" ||prevpickgenqty == " " || prevpickgenqty =='null' || prevpickgenqty ==null || isNaN(prevpickgenqty)) {
		prevpickgenqty = 0;
	}
	nlapiLogExecution('Debug', 'prevpickgenqty:orderQuantity ', prevpickgenqty+":"+orderQuantity);
	var totalPickGeneratedQuantity = parseFloat(prevpickgenqty) + parseFloat(orderQuantity);
	nlapiLogExecution('Debug', 'totalPickGeneratedQuantity ', totalPickGeneratedQuantity);
	fulfilmentOrderline.setFieldValue('custrecord_ebiz_wave', parseInt(eBizWaveNo)); 

	if(parseFloat(orderQuantity)>0)
		fulfilmentOrderline.setFieldValue('custrecord_linestatus_flag', '9'); //status flag--'G' (Picks Generated) 
	else
		fulfilmentOrderline.setFieldValue('custrecord_linestatus_flag', '26'); //status flag--'F' (Picks Failed)

	fulfilmentOrderline.setFieldValue('custrecord_pickgen_qty', parseFloat(totalPickGeneratedQuantity));

	nlapiSubmitRecord(fulfilmentOrderline, false, true);
	nlapiLogExecution('Debug', 'Out of updateFulfilmentOrderline ', 'Success');
}

var vFulfillArr=new Array();
var vFulfillIdArr=new Array();

function updateFulfilmentOrder(fulfillmentOrder, orderQuantity, eBizWaveNo,foparent){

	nlapiLogExecution('Debug', 'Into updateFulfillmentOrder...');

	if (orderQuantity == "" || isNaN(orderQuantity)) {
		orderQuantity = 0;
	}

//	var str = 'Wave. = ' + eBizWaveNo + '<br>';
//	str = str + 'FO Intr Id. = ' + fulfillmentOrder[3] + '<br>';	
//	str = str + 'FO Name. = ' + fulfillmentOrder[1] + '<br>';	
//	str = str + 'Item Status. = ' + fulfillmentOrder[7] + '<br>';	
//	str = str + 'Customer. = ' + fulfillmentOrder[35] + '<br>';	
//	str = str + 'Ship Method. = ' + fulfillmentOrder[36] + '<br>';
//	str = str + 'wms Carrier. = ' + fulfillmentOrder[20] + '<br>';
//	str = str + 'Order Type. = ' + fulfillmentOrder[17] + '<br>';
//	str = str + 'Order Priority. = ' + fulfillmentOrder[18] + '<br>';
//	str = str + 'Item. = ' + fulfillmentOrder[6] + '<br>';
//	str = str + 'Packcode. = ' + fulfillmentOrder[10] + '<br>';
//	str = str + 'UOM. = ' + fulfillmentOrder[11] + '<br>';
//	str = str + 'Site. = ' + fulfillmentOrder[19] + '<br>';
//	str = str + 'Company. = ' + fulfillmentOrder[13] + '<br>';
//	str = str + 'Parent Ord #. = ' + fulfillmentOrder[27] + '<br>';
//	str = str + 'Freight Term. = ' + fulfillmentOrder[31] + '<br>';
//	str = str + 'Prev Pickgen Qty. = ' + fulfillmentOrder[34] + '<br>';
//	str = str + 'Pickgen Qty. = ' + orderQuantity + '<br>';

//	nlapiLogExecution('Debug', 'FO Update Parameters', str);

	if(vFulfillIdArr.indexOf(fulfillmentOrder[3]) == -1)
	{
		//nlapiLogExecution('Debug','Into IF1');
		vFulfillIdArr.push(fulfillmentOrder[3]);
		var prevpickgenqty=fulfillmentOrder[34];
		//nlapiLogExecution('Debug','prevpickgenqty',prevpickgenqty);
		if (prevpickgenqty == "" || isNaN(prevpickgenqty)) {
			prevpickgenqty = 0;
		}

		var totalPickGeneratedQuantity = parseInt(prevpickgenqty) + parseInt(orderQuantity);		
		var vFO=[fulfillmentOrder,totalPickGeneratedQuantity,eBizWaveNo,orderQuantity];
		vFulfillArr.push(vFO);

		var str = 'prevpickgenqty. = ' + prevpickgenqty + '<br>';
		str = str + 'orderQuantity. = ' + orderQuantity + '<br>';	
		str = str + 'totalPickGeneratedQuantity. = ' + totalPickGeneratedQuantity + '<br>';

		nlapiLogExecution('DEBUG', 'FO Pick Gen Qty - IF', str);	
	}
	else
	{
		//nlapiLogExecution('Debug','Into Else Exists');
		var vFOIndex=vFulfillIdArr.indexOf(fulfillmentOrder[3]);
		var vPrevPickGenQty=vFulfillArr[vFOIndex][1];
		//nlapiLogExecution('Debug','vPrevPickGenQty Exists',vPrevPickGenQty);
		var totalPickGeneratedQuantity = parseInt(vPrevPickGenQty) + parseInt(orderQuantity);
		//nlapiLogExecution('Debug','totalPickGeneratedQuantity Exists',totalPickGeneratedQuantity);
		vFulfillArr[vFOIndex][1]=totalPickGeneratedQuantity;

		var str = 'vPrevPickGenQty. = ' + vPrevPickGenQty + '<br>';
		str = str + 'orderQuantity. = ' + orderQuantity + '<br>';	
		str = str + 'totalPickGeneratedQuantity. = ' + totalPickGeneratedQuantity + '<br>';

		nlapiLogExecution('DEBUG', 'FO Pick Gen Qty - IF', str);
	}

	nlapiLogExecution('Debug', 'Out of updateFulfillmentOrder...');
}

function updateFulfilmentOrderBulk(fulfillmentOrderArr,foparent){

	nlapiLogExecution('Debug', 'Into updateFulfillmentOrder...New');

	if(fulfillmentOrderArr != null && fulfillmentOrderArr != '' && fulfillmentOrderArr.length>0)
	{
		nlapiLogExecution('Debug', 'FO Update Parameters New', fulfillmentOrderArr.length);
		for(var i=0;i<fulfillmentOrderArr.length;i++)
		{
			//nlapiLogExecution('Debug', 'FO Update Parameters New Arr Main', fulfillmentOrderArr[i]);
			//nlapiLogExecution('Debug', 'FO Update Parameters New Arr', fulfillmentOrderArr[i][0]);
//			var currentRow = [0-i, 1-soInternalID, 2-lineNo, 3-fointrid, 4-foname, 5-itemName, 6-itemNo, 7-itemStatus,8-foordqty, 9-orderQty, 
//			10-packCode, 11-uom, 12-lotBatchNo, 13-company,14-itemInfo1, 15-itemInfo2, 16-itemInfo3, 17-orderType, 18-orderPriority,
//			19-location,20-wmsCarrier,21-nsrefno,22-note1,23-note2,24-pickgenflag,25-pickqty,26-shipqty,27-parentordno,28-shipmentno,
//			29-printflag,30-printcount,31-freightterms,32-shipcomplete,33-pickreportprintdt,34-vPickGenQty,35-customer,36-shipmethod];
			var fulfillmentOrder=fulfillmentOrderArr[i][0];
			foparent.selectNewLineItem('recmachcustrecord_ebiz_fo_parent');

			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'id', fulfillmentOrder[3]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','name', fulfillmentOrder[1]);
			if(fulfillmentOrder[7]!=null && fulfillmentOrder[7]!='')
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linesku_status', fulfillmentOrder[7]);
			if(fulfillmentOrder[35]!=null && fulfillmentOrder[35]!='')
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_customer', fulfillmentOrder[35]);
			if(fulfillmentOrder[36]!=null && fulfillmentOrder[36]!='')
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_carrier', fulfillmentOrder[36]);
			if(fulfillmentOrder[17]!=null && fulfillmentOrder[17]!='')
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_order_type', fulfillmentOrder[17]);
			if(fulfillmentOrder[18]!=null && fulfillmentOrder[18]!='')
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_order_priority', fulfillmentOrder[18]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_lineord', fulfillmentOrder[4]);
			if(fulfillmentOrder[6]!=null && fulfillmentOrder[6]!='')
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_linesku', fulfillmentOrder[6]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ord_qty', fulfillmentOrder[8]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ordline', fulfillmentOrder[2]);
			if(fulfillmentOrder[10]!=null && fulfillmentOrder[10]!='')
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linepackcode', fulfillmentOrder[10]);
			else
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linepackcode', 1);

			if(fulfillmentOrder[11]!=null && fulfillmentOrder[11]!='')
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_lineuom_id', fulfillmentOrder[11]); 
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_batch', fulfillmentOrder[12]);
			if(fulfillmentOrder[19]!=null && fulfillmentOrder[19]!='')
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ordline_wms_location', fulfillmentOrder[19]);
			if(fulfillmentOrder[13]!=null && fulfillmentOrder[13]!='')
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ordline_company', fulfillmentOrder[13]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_fulfilmentiteminfo1', fulfillmentOrder[14]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_fulfilmentiteminfo2',  fulfillmentOrder[15]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_fulfilmentiteminfo3',  fulfillmentOrder[16]);
			if(fulfillmentOrder[20]!=null && fulfillmentOrder[20]!='')
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_wmscarrier',  fulfillmentOrder[20]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_nsconfirm_ref_no',  fulfillmentOrder[21]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linenotes1',  fulfillmentOrder[22]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linenotes2',  fulfillmentOrder[23]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_pickgen_flag',  fulfillmentOrder[24]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_pickqty',  fulfillmentOrder[25]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ship_qty',  fulfillmentOrder[26]);
			if(fulfillmentOrder[27]!=null && fulfillmentOrder[27]!='')
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ns_ord',  fulfillmentOrder[27]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_shipment_no',  fulfillmentOrder[28]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_printflag',  fulfillmentOrder[29]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_print_count',  fulfillmentOrder[30]);
			if(fulfillmentOrder[31]!=null && fulfillmentOrder[31]!='')
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_freightterms',  fulfillmentOrder[31]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_shipcomplete',  fulfillmentOrder[32]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_pr_dateprinted',  fulfillmentOrder[33]);
			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_wave',  parseInt(fulfillmentOrderArr[i][2]));

			/*var prevpickgenqty=fulfillmentOrder[34];
			nlapiLogExecution('Debug','prevpickgenqty',prevpickgenqty);
			if (prevpickgenqty == "" || isNaN(prevpickgenqty)) {
				prevpickgenqty = 0;
			}
			var totalPickGeneratedQuantity = parseInt(prevpickgenqty) + parseInt(orderQuantity);*/
			//nlapiLogExecution('Debug','totalPickGeneratedQuantity',fulfillmentOrderArr[i][1]);

			/*var orderQuantity=0;
			if(fulfillmentOrderArr[i][1] != null && fulfillmentOrderArr[i][1] != '')
				orderQuantity=parseInt(fulfillmentOrderArr[i][1]);

			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_pickgen_qty',parseInt(orderQuantity));

			if(parseInt(orderQuantity)>0)
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linestatus_flag',  9);
			else
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linestatus_flag',  26);*/

			var orderQuantity=0;
			var neworderQuanttity = 0;
			if(fulfillmentOrderArr[i][1] != null && fulfillmentOrderArr[i][1] != '')
				orderQuantity=parseFloat(fulfillmentOrderArr[i][1]);

			if(fulfillmentOrderArr[i][3] != null && fulfillmentOrderArr[i][3] != '')
				neworderQuanttity=parseFloat(fulfillmentOrderArr[i][3]);

			foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_pickgen_qty',parseFloat(orderQuantity).toFixed(5));

			if(parseFloat(neworderQuanttity)>0)
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linestatus_flag',  9);
			else
				foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linestatus_flag',  26);


			foparent.commitLineItem('recmachcustrecord_ebiz_fo_parent'); 

		}
	}

	nlapiLogExecution('Debug', 'Out of updateFulfillmentOrder...');
}

function updateFulfilmentOrderold(fulfilmentOrderNo, fulfilmentOrderLineNo, orderQuantity, eBizWaveNo){

	if (orderQuantity == "" || isNaN(orderQuantity)) {
		orderQuantity = 0;
	}
	var fulfilmentOrderline = nlapiLoadRecord('customrecord_ebiznet_ordline', fulfilmentOrderNo);
	var prevpickgenqty=fulfilmentOrderline.getFieldValue('custrecord_pickgen_qty');
	var totalPickGeneratedQuantity = parseInt(prevpickgenqty) + parseInt(orderQuantity);
	nlapiLogExecution('Debug', 'totalPickGeneratedQuantity ', totalPickGeneratedQuantity);
	nlapiLogExecution('Debug', 'orderQuantity', orderQuantity);
	fulfilmentOrderline.setFieldValue('custrecord_ebiz_wave', parseInt(eBizWaveNo));		

	if(parseInt(orderQuantity)>0)
		fulfilmentOrderline.setFieldValue('custrecord_linestatus_flag', '9'); //status flag--'G' (Picks Generated)	
	else
		fulfilmentOrderline.setFieldValue('custrecord_linestatus_flag', '26'); //status flag--'F' (Picks Failed)

	fulfilmentOrderline.setFieldValue('custrecord_pickgen_qty', parseInt(totalPickGeneratedQuantity));

	nlapiSubmitRecord(fulfilmentOrderline, false, true);
	nlapiLogExecution('Debug', 'updateFulfilmentOrder ', 'Success');

//	if(parseInt(orderQuantity)>0)
//	updatesalesorderline(salesorderinternalid,fulfilmentOrderLineNo,orderQuantity,'G');
	//}
	//}
}

function getpickmethod()
{
	nlapiLogExecution('Debug','ClustergetPickMethod','chkptTrue');
	var pickInternalId=new Array();
	var filter=new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizcreatecluster',null,'is','T'));
	var column=new Array();
	column[0]=new nlobjSearchColumn('internalid').setSort();
	column[1]=new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxorders');
	column[2]=new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxpicks');
	var searchresult=nlapiSearchRecord('customrecord_ebiznet_pick_method', null, filter, column);

	if(searchresult!=null)
	{
		for ( var i = 0; i < searchresult.length; i++)
		{
			pickInternalId[i]=new Array();
			pickInternalId[i][0]=searchresult[i].getValue('internalid');
			pickInternalId[i][1]=searchresult[i].getValue('custrecord_ebiz_pickmethod_maxorders');
			pickInternalId[i][2]=searchresult[i].getValue('custrecord_ebiz_pickmethod_maxpicks');
		}
	}

	return pickInternalId;
}

function GenerateChildLabel(vContLpNo,vOrderNo,vwaveno,uompackflag,company)
{
	nlapiLogExecution('Debug','Into GenerateChildLabel.....');
	nlapiLogExecution('Debug','uompackflag',uompackflag);
	nlapiLogExecution('Debug','vwaveno',vwaveno);
	nlapiLogExecution('Debug','vOrderNo',vOrderNo);
	nlapiLogExecution('Debug','company',company);
	nlapiLogExecution('Debug','vContLpNo',vContLpNo);
//	CASE 20125002 -Start
	var companyDUNSnumber="";
	var ventity='';
	var vordtype = '';
	var vEntityASNreqdflag ='F';
	var vOrdTypeASNreqdflag='F';

	var searchRes = getEntity(vOrderNo);

	if(searchRes!=null && searchRes!='') 
	{
		ventity =  searchRes[0].getValue('entity');
		vordtype = searchRes[0].getValue('custbody_nswmssoordertype');		
	}

	if(ventity!=null && ventity!='') 
	{
		vEntityASNreqdflag = getASNReqdFlag(ventity);
	}

	nlapiLogExecution('Debug','vEntityASNreqdflag',vEntityASNreqdflag);

	if(vEntityASNreqdflag!='T')
	{
		vOrdTypeASNreqdflag = getOrdTypeASNReqdFlag(vordtype);
	}

	nlapiLogExecution('Debug','vOrdTypeASNreqdflag',vOrdTypeASNreqdflag);

	if(vEntityASNreqdflag=='T' || vOrdTypeASNreqdflag=='T')
	{
		companyDUNSnumber=GetCompanyDUNSnumber(company);

		//get qty from opentask against to containerlp
		var nooflps=0;var uomId="",itemId="",itemText="";
		var qtyfilters = new Array();

		if(vContLpNo !=null && vContLpNo !=""){
			qtyfilters.push( new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vContLpNo));
		}

		if(vOrderNo !=null && vOrderNo !=""){
			qtyfilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', [vOrderNo]));
		}

		if(vwaveno !=null && vwaveno !=""){
			qtyfilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', vwaveno));
		}

		qtyfilters.push( new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); 		//Task Type -PICK    
		qtyfilters.push( new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//Status Flag - G (Picks Generated)  

		var qtycolumns = new Array();
		qtycolumns[0] = new nlobjSearchColumn('custrecord_container_lp_no', null, 'group');
		qtycolumns[1] = new nlobjSearchColumn('custrecord_sku', null, 'group');
		qtycolumns[2] = new nlobjSearchColumn('custrecord_uom_id', null, 'group');
		qtycolumns[3] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');	
		qtycolumns[4] = new nlobjSearchColumn('custrecord_packcode', null, 'group');
		qtycolumns[1].setSort();

		var qtysearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, qtyfilters, qtycolumns);
		var company="";

		if(qtysearchresults!=null)
		{
			//Creating record in throwaway parent


			var parent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); //create new parent record
			//var parentid = nlapiSubmitRecord(newParent); //save parent record


			var MaxLpNoArray = new Array();

			for(var g=0;g<qtysearchresults.length;g++)
			{		
				var vContainerLpNo = qtysearchresults[g].getValue('custrecord_container_lp_no', null, 'group');
				var itemText = qtysearchresults[g].getText('custrecord_sku', null, 'group');
				var itemId = qtysearchresults[g].getValue('custrecord_sku', null, 'group');
				var itemName=qtysearchresults[g].getText('custrecord_sku', null, 'group');
				var uomId = qtysearchresults[g].getValue('custrecord_uom_id', null, 'group');
				var itemtotalqty = qtysearchresults[g].getValue('custrecord_expe_qty', null, 'sum');
				var packcode = qtysearchresults[g].getValue('custrecord_packcode', null, 'group');
				var eBizOrder=qtysearchresults[g].getValue('custrecord_ebiz_order_no', null,'min');
				//nlapiLogExecution('Debug','itemtotalqty',itemtotalqty);
				var veBizOrder=''; 
				if(eBizOrder!=null)
				{
					veBizOrder=eBizOrder.split('#');
				}
				veBizOrder=veBizOrder[1];
				var nooflps="";
				if((packcode!=null)&&(packcode!=""))
				{
					nooflps=Math.ceil(itemtotalqty/packcode);
				}
				else
				{
					nooflps=itemtotalqty;
				}
				nlapiLogExecution('Debug','nooflps',nooflps);
				nlapiLogExecution('Debug','Vpackcode',packcode);
				nlapiLogExecution('Debug','vebizOrdNoId',vOrderNo);
				nlapiLogExecution('Debug','uomId',uomId);
				nlapiLogExecution('Debug','itemId',itemId);
				nlapiLogExecution('Debug','itemName',itemName);
				nlapiLogExecution('Debug','vContainerLpNo',vContainerLpNo);
				nlapiLogExecution('Debug','qtysearchresults.length',qtysearchresults.length);

				//Duplicate ucc lable generation fix
				MaxLpNoArray=GetMaxLPNoType('1', '5','',nooflps);

				var lpMaxValue=MaxLpNoArray["maxLpPrefix"];
				nlapiLogExecution('Debug','sMaxValue',lpMaxValue);
				var lpinternlaid=MaxLpNoArray["maxInternalId"];
				var company=MaxLpNoArray["maxCompany"];


				if((qtysearchresults !=null))
				{
					var seriallineno=1;
					nlapiLogExecution('Debug', 'AcySearch',qtysearchresults.length);
					nlapiLogExecution('Debug', 'AcFirstLoop','AcFirstLoop');
					var lpMaxNo=lpMaxValue;
					for(var i=0; i < nooflps.toString();i++)
					{
						lpMaxNo = parseInt(lpMaxNo) +parseInt(1);
						nlapiLogExecution('Debug', 'into for loop','done');
						nlapiLogExecution('Debug', 'lpMaxNo',lpMaxNo);
						nlapiLogExecution('Debug', 'companyDUNSnumber',companyDUNSnumber);
						var ResultText=GenerateLabelCode(uomId,parseInt(lpMaxNo),companyDUNSnumber);
						var substring=ResultText.substring(10, 20);	
						var vContLpNo='PICK'+substring;
						nlapiLogExecution('Debug', 'substring',substring);

						parent.selectNewLineItem('recmachcustrecord_ebiz_lp_parent');
						parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','name', vContLpNo);
						parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_controlno', veBizOrder);
						parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lp_seq', seriallineno);
						parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_lp', vContLpNo);
						parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_sscc', ResultText);
						parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_item', itemName);
						parent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_masterlp', vContainerLpNo);
						parent.commitLineItem('recmachcustrecord_ebiz_lp_parent');

						/*var MastLP=nlapiCreateRecord('customrecord_ebiznet_master_lp');		
					MastLP.setFieldValue('name', vContLpNo);
					MastLP.setFieldValue('custrecord_ebiz_lpmaster_lp', vContLpNo);	
					MastLP.setFieldValue('custrecord_ebiz_lpmaster_sscc', ResultText);
					MastLP.setFieldValue('custrecord_ebiz_lpmaster_item', itemName);
					MastLP.setFieldValue('custrecord_ebiz_lpmaster_masterlp', vContainerLpNo);
					var retktoval = nlapiSubmitRecord(MastLP);*/
						//genarateCartonLabelTemplate(vContainerLpNo,sOrderDetailsArray,itemName,labeltype,ResultText,seriallineno,nooflps,Label);
						nlapiLogExecution('Debug', 'Acseriallineno',seriallineno);
						seriallineno++;
					}

					//Insert MaxLp number in LP Range List 
					//InsertMaxLpNo(lpinternlaid,lpMaxNo);
				}
			}
			nlapiSubmitRecord(parent); //submit the parent record, all child records will also be Created
			//nlapiLogExecution('Debug','Remaining usage 4',context.getRemainingUsage());

		}
		//CASE 20125002 -END
	}
}

function InsertMaxLpNo(internalid,noofLPs)
{

	// update the new max LP in the custom record
	var scount=1;
	LABL1: for(var i=0;i<scount;i++)
	{	
		nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);
		try
		{
			var LpRangetran = nlapiLoadRecord('customrecord_ebiznet_lp_range', internalid);
			var lpMaxValue = LpRangetran.getFieldValue('custrecord_ebiznet_lprange_lpmax');
			LpRangetran.setFieldValue('custrecord_ebiznet_lprange_lpmax', parseInt(lpMaxValue)+ parseInt(noofLPs));

			var LpRangetranid = nlapiSubmitRecord(LpRangetran, false, true);
		}
		catch(ex)
		{
			var exCode='CUSTOM_RECORD_COLLISION'; 
			var wmsE='Inventory record being updated by another user. Please try again...';
			if (ex instanceof nlobjError) 
			{	
				wmsE=ex.getCode() + '\n' + ex.getDetails();
				exCode=ex.getCode();
			}
			else
			{
				wmsE=ex.toString();
				exCode=ex.toString();
			} 
			var exceptionname='LP Range';
			var functionality='LP Range MAX Value';
			var trantype="";
			var vinternalid=internalid;
			var vlpMaxValue=lpMaxValue;
			nlapiLogExecution('Debug', 'DetailsError', functionality);	
			nlapiLogExecution('Debug', 'vlpMaxValue', vlpMaxValue);
			nlapiLogExecution('Debug', 'vinternalid', internalid);
			var reference3="";
			var reference4 ="";
			var reference5 ="";
			var userId = nlapiGetUser();
			InsertExceptionLog(exceptionname,trantype, functionality, wmsE, internalid, vlpMaxValue,reference3,reference4,reference5, userId);
			nlapiLogExecution('Debug', 'Exception in RF Inv move : ', wmsE); 
			if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
			{ 
				scount=scount+1;
				continue LABL1;
			}
			else break LABL1;
		}
	}
	//nlapiSubmitField('customrecord_ebiznet_lp_range', internalid,'custrecord_ebiznet_lprange_lpmax', parseInt(lpMaxValue));
}
function GetMaxLPNoType(lpGenerationType, lpType,whsite,nooflps)
{
	//CASE 20125002 -Start
	var maxLP = 1;
	var maxLPPrefix = "";
	var internalid="";
	var company="";
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpmax');
	columns[1] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix');
	columns[2] = new nlobjSearchColumn('custrecord_ebiznet_lprange_company');

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'is', lpGenerationType));
	filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lptype', null, 'anyof', [lpType]));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(whsite!=null && whsite!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_site', null, 'anyof', ['@NONE@', whsite]));

	var results = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, filters, columns);

	if(results != null)
	{
		if(results.length > 1)
		{
			alert('More records returned than expected');
			nlapiLogExecution('Debug', 'GetMaxLPNo:LP Max Query returned more than 1 row');
		}
		else
		{
			// Incrementing the max LP number only if the search returns 1 row
			for (var t = 0; t < results.length; t++)
			{
				if (results[t].getValue('custrecord_ebiznet_lprange_lpmax') != null) 
				{
					internalid=results[t].getId();
					maxLP = results[t].getValue('custrecord_ebiznet_lprange_lpmax');
					maxLPPrefix = results[t].getValue('custrecord_ebiznet_lprange_lpprefix'); 
					company=results[t].getText('custrecord_ebiznet_lprange_company'); 
				}

			}
		}
		if((maxLP==null)||(maxLP==''))
		{
			maxLP=0;
		}
		maxLP = maxLP.toString();
		nlapiLogExecution('DEBUG', 'GetMaxLPNo:New MaxLP', maxLP);
		var maxArray = new Array();
		maxArray["maxLpPrefix"]=maxLPPrefix+parseInt(maxLP);
		nlapiLogExecution('DEBUG', 'GetMaxLPNo:maxLpPrefix',maxArray["maxLpPrefix"]);
		maxArray["maxInternalId"]=internalid;
		maxArray["maxCompany"]=company;
		InsertMaxLpNo(internalid,parseInt(nooflps));
		return maxArray;
		//CASE 20125002 -END
	}
}

function GenerateLabelCode(uompackflag,lpvalue,duns)
{

	var finaltext="";

	var label="",uom="",ResultText="";
	nlapiLogExecution('Debug', 'CreateMasterLPRecord uompackflag',uompackflag);
	nlapiLogExecution('Debug', 'lpValue',lpvalue);
	//added by mahesh
	try 
	{	
		var lpMaxValue=lpvalue.toString();
		nlapiLogExecution('Debug', 'lpMaxValueInnercode',parseInt(lpMaxValue.length));
		var prefixlength=parseInt(lpMaxValue.length);
		nlapiLogExecution('Debug', 'prefixlength',prefixlength);
		if(prefixlength==0)
			label="000000000";
		else if(prefixlength==1)
			label="00000000"+lpMaxValue;
		else if(prefixlength==2)
			label="0000000"+lpMaxValue;
		else if(prefixlength==3)
			label="000000"+lpMaxValue;
		else if(prefixlength==4)
			label="00000"+lpMaxValue;
		else if(prefixlength==5)
			label="0000"+lpMaxValue;
		else if(prefixlength==6)
			label="000"+lpMaxValue;
		else if(prefixlength==7)
			label="00"+lpMaxValue;
		else if(prefixlength==8)
			label="0"+lpMaxValue;
		else if(prefixlength==9)
			label=lpMaxValue;


		nlapiLogExecution('Debug', 'label', label);


		if(uompackflag == "1" || uompackflag == "EACH" ) 
			uom="0"; 
		else if(uompackflag == "3" || uompackflag == "PALLET") 
			uom="2";
		else
			uom="0";
		nlapiLogExecution('Debug', 'uom', uom);
		nlapiLogExecution('Debug', 'duns', duns);

		finaltext=uom+duns+label;
		nlapiLogExecution('Debug', 'CreateMasterLPRecord finaltext', finaltext);



		//to get chk digit
		var checkStr=finaltext;
		var ARL=0;
		var BRL=0;
		var CheckDigitValue="";
		for (i = checkStr.length-1;  i > 0;  i--)
		{
			ARL = ARL+parseInt(checkStr.charAt(i));
			i--;
		}		
		ARL=ARL*3;
		for (i = checkStr.length-2;  i > 0;  i--)
		{
			BRL = BRL+parseInt(checkStr.charAt(i));
			i--;
		}		
		var sumOfARLBRL=ARL+BRL;
		var CheckDigit=0;

		while(CheckDigit<10)
		{
			if(sumOfARLBRL%10==0)
			{ 
				CheckDigitValue=CheckDigit; 
				break; 
			} 

			sumOfARLBRL++;
			CheckDigit++;
		}
		nlapiLogExecution('Debug', 'CreateMasterLPRecord CheckDigit', CheckDigitValue.toString());
		ResultText="00"+finaltext+CheckDigitValue.toString();
		nlapiLogExecution('Debug', 'CreateMasterLPRecord ResultText', ResultText);


	} 
	catch (err) 
	{
		nlapiLogExecution('Debug', 'Exception in  GenerateLabelCode', err);
	}
	return ResultText;
}

function GetCompanyDUNSnumber(company)
{
	var dunsfilters = new Array();
	var duns='';

	if(company!=null && company!='')
		dunsfilters.push(new nlobjSearchFilter('internalid', null, 'is', company));

	dunsfilters.push(new nlobjSearchFilter('custrecord_compduns', null, 'isnotempty'));
	dunsfilters.push(new nlobjSearchFilter('isinactive', null,'is', 'F'));

	var dunscolumns = new Array();
	dunscolumns[0] = new nlobjSearchColumn('custrecord_compduns');			 

	var dunssearchresults = nlapiSearchRecord('customrecord_ebiznet_company', null, dunsfilters, dunscolumns);
	if (dunssearchresults !=null && dunssearchresults !="") 
	{
		duns = dunssearchresults[0].getValue('custrecord_compduns');
		nlapiLogExecution('Debug', 'duns', duns);
	}
	return duns;
}

function generatePickreporthtml(vwaveno)
{

	nlapiLogExecution('Debug','GeneratePickReportHtml','GeneratePickReportHtml');
	nlapiLogExecution('Debug','GeneratePickReportHtmlvwaveno',vwaveno);
	try
	{


		var waverfilterarray= new Array();

		waverfilterarray.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(vwaveno)));
		waverfilterarray.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
		waverfilterarray.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

		var wavecolumnarray = new Array();
		wavecolumnarray [0] = new nlobjSearchColumn('custrecord_ebiz_clus_no', null, 'group');
		wavecolumnarray [1] = new nlobjSearchColumn('name', null, 'group');
		wavecolumnarray [2] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');
		wavecolumnarray [3] = new nlobjSearchColumn('custrecord_line_no', null, 'count');
		wavecolumnarray [4] = new nlobjSearchColumn('custrecord_ebiz_order_no', null, 'min');
		var wavesearchresults= nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, waverfilterarray, wavecolumnarray);	

		var strVar="";
		if(wavesearchresults!=null)
			totalcount=wavesearchresults.length;
		else
			totalcount=0;

		var grouplength=0;
		while(0<totalcount)
		{
			var count=0;

			if(wavesearchresults!=null && wavesearchresults!='')
			{    
				//nlapiLogExecution('Debug','wavesearchresults length1',wavesearchresults.length);

				var waverfilterarray= new Array();

				waverfilterarray.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(vwaveno)));
				waverfilterarray.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
				waverfilterarray.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

				var wavecolumnarray = new Array();
				wavecolumnarray [0] = new nlobjSearchColumn('custrecord_ebiz_clus_no', null, 'group');
				wavecolumnarray [1] = new nlobjSearchColumn('name', null, 'group');
				wavecolumnarray [2] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');
				wavecolumnarray [3] = new nlobjSearchColumn('custrecord_line_no', null, 'count');
				wavecolumnarray [4] = new nlobjSearchColumn('custrecord_ebiz_order_no', null, 'min');
				var wavesearchresults= nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, waverfilterarray, wavecolumnarray);	

				strVar +="<html><body   style=\"font-size:7px padding-right:1px; padding-left:1px; padding-bottom:1px; padding-top:1px\">";
				strVar +="<table><td><tr style=\"Height:1000px\">";
				strVar += "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" frame=\"box\" rules=\"all\">";
				strVar += "<thead style=\"font-family:Verdana;font-size: 10px;line-height: 20px;font-style: normal;	font-weight: normal;text-align: left;text-shadow: white 1px 1px 1px;\">";
				strVar += "<tr style=\" border-width: 7px; border-color:Black; \">";
				strVar += "<th align=\"center\" style=\"width: 110px\" >Wave#<\/th>";
				strVar += "<th align=\"center\" style=\"width: 140px\" >Customer<\/th> ";
				strVar += "<th align=\"center\" style=\"width: 140px\" >ShipVia<\/th> ";
				strVar += "<th align=\"center\" style=\"width: 70px\" >Total Lines<\/th> ";
				strVar += "<th align=\"center\" style=\"width: 70px\" >Total Pieces<\/th> ";
				strVar += "<th align=\"center\" style=\"width: 80px\" >Zone<\/th> ";
				strVar += "<th align=\"center\" style=\"width: 100px\" >Aisle<\/th>  ";
				strVar += "<th align=\"center\" style=\"width: 140px\" >Ship To<\/th>";
				strVar += "<th align=\"center\" style=\"width: 150px\" >Ord#<\/th> ";
				strVar +=" <\/tr><\/thead>";
				strVar += "<tbody  style=\"font-family:Verdana;font-size: 10px;line-height: 20px;font-style: normal;	font-weight: normal;text-align: left;text-shadow: white 1px 1px 1px;\">";
				if(wavesearchresults!=null)
				{
					//nlapiLogExecution('Debug','wavesearchresults length2',wavesearchresults.length);
					var name=null;
					var clusterno;

					for(var g=grouplength;g<wavesearchresults.length;g++)
					{
						count++;
						grouplength++;
						name=wavesearchresults[g].getValue('name', null, 'group');
						var lineno=wavesearchresults[g].getValue('custrecord_line_no', null,'count');
						clusterno=wavesearchresults[g].getValue('custrecord_ebiz_clus_no', null, 'group');
						var orderno=wavesearchresults[g].getValue('custrecord_ebiz_order_no',null,'min');
						var totalqty=wavesearchresults[g].getValue('custrecord_expe_qty',null,'sum');
						//nlapiLogExecution('Debug','orderno',orderno);
						var ordernumber="";
						if(orderno!=null)
						{
							ordernumber=orderno.split('#');
						}
						splitordno=ordernumber[1];
						var ordertype=ordernumber[0].replace(/\s/g, "");
						var searchresults = new Array();
						var filters = new Array();
						filters.push(new nlobjSearchFilter('tranid', null, 'is', splitordno));
						var columns = new Array();
						columns[0] = new nlobjSearchColumn('shipaddress');
						columns[1] = new nlobjSearchColumn('shipmethod');
						columns[2] = new nlobjSearchColumn('shipaddressee'); 
						columns[3] = new nlobjSearchColumn('shipaddress1'); 
						columns[4] = new nlobjSearchColumn('shipcity'); 
						columns[5] = new nlobjSearchColumn('shipcountry'); 
						columns[6] = new nlobjSearchColumn('shipstate'); 
						columns[7] = new nlobjSearchColumn('shipzip'); 
						columns[8] = new nlobjSearchColumn('entity'); 
						var searchresults = nlapiSearchRecord(ordertype, null, filters, columns);
						var shipadress=searchresults[0].getValue('shipaddress');
						var shipmethod=searchresults[0].getText('shipmethod');
						var customer=searchresults[0].getText('entity');
						var shipaddressee="";var shipaddr1="";var shipcity="";var shipcountry="";var shipstate="";var shipzip="";var shipstateandcountry="";
						shipaddressee=searchresults[0].getValue('shipaddressee'); 
						shipaddr1=searchresults[0].getValue('shipaddress1'); 
						shipcity=searchresults[0].getValue('shipcity'); 
						shipcountry=searchresults[0].getValue('shipcountry'); 
						shipstate=searchresults[0].getValue('shipstate'); 
						shipzip=searchresults[0].getValue('shipzip'); 
						shipstateandcountry=shipstate+","+shipzip;
						if(clusterno=="- None -")
						{
							clusterno="&nbsp;";
						}

						//nlapiLogExecution('Debug', 'clusterno',clusterno);

						strVar += "<tr>";
						strVar += "<td align=\"center\" style=\"width: 110px;border-bottom: 1px solid black;\" ><font size=1 face=\"MRV Code39extMA\">*"+vwaveno+"*<\/font><\/td>";
						strVar += "<td style=\"width: 140px\" >"+customer+"<\/td> ";
						strVar += "<td style=\"width: 140px\"  >"+shipmethod+"<\/td> ";
						strVar += "<td style=\"width: 70px\" align=\"center\" >"+lineno+"<\/td> ";
						strVar += "<td style=\"width: 70px\" align=\"center\">"+totalqty+"<\/td> ";
						//nlapiLogExecution('Debug','orderno',orderno);
						var filterzone = new Array();
						filterzone.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(vwaveno)));
						filterzone.push(new nlobjSearchFilter('name', null, 'is',name));
						filterzone.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
						filterzone.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

						var columnzone = new Array();
						columnzone[0]= new nlobjSearchColumn('custrecord_ebiz_zoneid',null,'group');
						columnzone[1]= new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
						var searchopentaskrecords =nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filterzone,columnzone);
						if(searchopentaskrecords!=null)
						{
							strVar +="<td style=\"width: 80px\"><table>";
							strVar +="<tbody  style=\"font-family:Verdana;font-size: 10px;line-height: 20px;font-style: normal;	font-weight: normal;text-align: left;text-shadow: white 1px 1px 1px;\">";
							var repeatvalue='';

							for(var i=0;i<searchopentaskrecords.length;i++)
							{
								var zonevalue=searchopentaskrecords[i].getText('custrecord_ebiz_zoneid',null,'group');			
								//nlapiLogExecution('Debug','zonevalue',zonevalue);											
								if(zonevalue!=repeatvalue)
								{
									strVar += "<tr  style=\"height:10px;\"><td align=\"center\">"+zonevalue+"<\/td><\/tr>";
									repeatvalue=zonevalue;
								}
							}
							strVar +="<\/body> <\/table><\/td>";
							strVar +="<td style=\"width: 100px\"><table><tr>";
							strVar +="<tbody  style=\"font-family:Verdana;font-size: 9px;\">";
							strVar += " <tr  style=\"height:10px;\">";
							var repeatvalue='';
							var stringappend='';
							var aislecount=0;
							for(var i=0;i<searchopentaskrecords.length;i++)
							{
								var acubiginloc=searchopentaskrecords[i].getText('custrecord_actbeginloc',null,'group');

								var aisle=acubiginloc.split('-');
								if(aisle[0]!=repeatvalue)
								{
									aislecount++;
									stringappend +=aisle[0];
									if(aislecount==6)
									{
										stringappend +="<br\/>";
										aislecount=0;
									}
									else
									{
										stringappend +=",";

									}
									repeatvalue=aisle[0];
								}

							}
							var newstringappend=stringappend.substring(0, stringappend.length-1);
							strVar +="<td>"+newstringappend+"<\/td><\/tr>";
							strVar +="<\/body><\/table><\/td>";
						}

						strVar += "<td style=\"width: 140px\" >" ;
						strVar += "                    <table>";
						strVar += "                        <tr>";
						strVar += "                            <td style=\"font-size: 9px;\">";
						strVar += "                                &nbsp;"+shipaddressee+"";
						strVar += "                            <\/td>";
						strVar += "                        <\/tr>";
						strVar += "                        <tr>";
						strVar += "                            <td style=\"font-size: 9px;\">";
						strVar += "                                &nbsp;"+shipaddr1+"";
						strVar += "                            <\/td>";
						strVar += "                        <\/tr>";
						strVar += "                        <tr>";
						strVar += "                            <td style=\"font-size: 9px;\">";
						strVar += "                                &nbsp;"+shipcity+"";
						strVar += "                            <\/td>";
						strVar += "                        <\/tr>";
						strVar += "                        <tr>";
						strVar += "                            <td style=\"font-size: 9px;\">";
						strVar += "                                &nbsp;"+shipstateandcountry+"";
						strVar += "                            <\/td>";
						strVar += "                        <\/tr>";
						strVar += "                    <\/table>";

						strVar +="<\/td>";
						strVar += "<td align=\"center\" style=\"width: 150px;border-bottom: 1px solid black;\" ><font size=1 face=\"MRV Code39extMA\">*"+name+"*<\/font><\/td> ";
						strVar +="<\/tr>";

						if(count==5)
						{
							break;
						}
					}
					strVar += "<\/tbody>";
					strVar += "<\/table><\/td><\/tr><\/table>";
					strVar +="<\/body><\/html>";
					if(grouplength==wavesearchresults.length)
					{
						strVar +="<p style=\" page-break-after:avoid\"></p>";
					}
					else
					{
						strVar +="<p style=\" page-break-after:always\"></p>";
					}
					//nlapiLogExecution('Debug', 'totalcount', totalcount);
					totalcount=parseInt(totalcount)-parseInt(count);
					//nlapiLogExecution('Debug', 'totalcountafter', totalcount);

				}

			}
		}
		var tasktype='3';
		var labeltype='PickReport';
		var print='F';
		var reprint='F';
		var company='';
		var location='';
		var formattype='html';
		var labelrecord = nlapiCreateRecord('customrecord_labelprinting'); 
		labelrecord.setFieldValue('name', vwaveno); 
		labelrecord.setFieldValue('custrecord_labeldata',strVar);  
		labelrecord.setFieldValue('custrecord_label_refno',vwaveno);     
		labelrecord.setFieldValue('custrecord_label_tasktype',tasktype);   
		labelrecord.setFieldValue('custrecord_labeltype',labeltype);    
		labelrecord.setFieldValue('custrecord_label_lp',vwaveno);

		labelrecord.setFieldValue('custrecord_label_print', print);
		labelrecord.setFieldValue('custrecord_label_reprint', reprint);
		labelrecord.setFieldValue('custrecord_label_company', company);
		labelrecord.setFieldValue('custrecord_label_location', location);
		var tranid = nlapiSubmitRecord(labelrecord);

	}
	catch(exp)
	{
		nlapiLogExecution('Debug', 'Exception in generatepickreporthtml',exp);
	}
}


function setClusterNo(eBizWaveNo)
{
	nlapiLogExecution('DEBUG','ClusterWaveNo',eBizWaveNo);
	var pickMethodIds=new Array();
	pickMethodIds=getpickmethod();

	if(pickMethodIds!=null && pickMethodIds.length>0)
	{		
		var ebizMethodNo=new Array();
		var ebizInternalRecordId=new Array();
		var ebizOrderId=new Array();
		var ebizZoneIds = new Array();

		var filter=new Array();
		filter.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is',parseInt(eBizWaveNo)));
		filter.push(new nlobjSearchFilter('custrecord_tasktype',null,'anyof',[3]));
		filter.push(new nlobjSearchFilter('custrecord_wms_status_flag',null,'anyof',[9]));
		filter.push(new nlobjSearchFilter('custrecord_actualendtime',null,'isempty'));//Change from Factory mation production
		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizmethod_no').setSort();
		column[1]=new nlobjSearchColumn('internalid');
		column[2]=new nlobjSearchColumn('name').setSort();
		column[3]=new nlobjSearchColumn('custrecord_ebiz_zoneid').setSort();

		var searchRecord=nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filter, column);
		//nlapiLogExecution('DEBUG','ClusterSearchRecord',searchRecord.length);
		var distEbizZoneId='';
		if(searchRecord!=null && searchRecord!='')
		{
			nlapiLogExecution('DEBUG','searchRecord',searchRecord.length);
			for(var count=0;count < searchRecord.length; count++)
			{
				ebizMethodNo[count] = searchRecord[count].getValue('custrecord_ebizmethod_no');
				ebizInternalRecordId[count] = searchRecord[count].getValue('internalid');
				ebizOrderId[count] = searchRecord[count].getValue('name');
				if(searchRecord[count].getValue('custrecord_ebiz_zoneid')!=null && searchRecord[count].getValue('custrecord_ebiz_zoneid')!='')
					ebizZoneIds[count] = searchRecord[count].getValue('custrecord_ebiz_zoneid');
				else
					ebizZoneIds[count] = '-1';
			}

			distEbizZoneId = removeDuplicateElement(ebizZoneIds);
		}

		var str = 'pickMethodIds. = ' + pickMethodIds + '<br>';
		str = str + 'ebizMethodNo. = ' + ebizMethodNo + '<br>';	
		str = str + 'pickMethodIds[0][1]. = ' + pickMethodIds[0][1] + '<br>';	
		str = str + 'pickMethodIds[0][2]. = ' + pickMethodIds[0][2] + '<br>';	
		str = str + 'distEbizZoneId. = ' + distEbizZoneId + '<br>';	

		nlapiLogExecution('DEBUG', 'Pick Method Parameters', str);

		/*var clusterno = GetMaxTransactionNo('WAVECLUSTER');
		nlapiLogExecution('DEBUG','clusternobeforefor',clusterno);*/
		//start 31/07/13
		if(distEbizZoneId!=null && distEbizZoneId!='')
		{
			nlapiLogExecution('DEBUG','zonecount',distEbizZoneId.length);

			for(var zonecount=0;zonecount<distEbizZoneId.length;zonecount++)
			{
				var clusterno = GetMaxTransactionNo('WAVECLUSTER');

				var str = 'zone. = ' + distEbizZoneId[zonecount] + '<br>';
				str = str + 'clusterno. = ' + clusterno + '<br>';	

				nlapiLogExecution('DEBUG', 'Zone & Cluster#', str);

				for(var itemcount=0;itemcount<pickMethodIds.length;itemcount++)
				{
					var ordno="";
					var maxPickFlagCount=0;
					var tempcount=0;

					for(var loopcount=0;(searchRecord!=null&&loopcount<searchRecord.length);loopcount++)
					{  	
//						nlapiLogExecution('DEBUG','loopcount',loopcount);
//						nlapiLogExecution('DEBUG','clustervalues',pickMethodIds[itemcount][0]+','+ebizMethodNo[loopcount]);
//						nlapiLogExecution('DEBUG','clustervalues_zones',distEbizZoneId[zonecount]+','+ebizZoneIds[loopcount]);

						if(pickMethodIds[itemcount][0]==ebizMethodNo[loopcount] && distEbizZoneId[zonecount] == ebizZoneIds[loopcount])
						{
//							nlapiLogExecution('DEBUG','distEbizZoneId[zonecount]',distEbizZoneId[zonecount]);
//							nlapiLogExecution('DEBUG','ebizZoneIds[loopcount]',ebizZoneIds[loopcount]);
//							nlapiLogExecution('DEBUG','pickMethodIds[itemcount][1]',pickMethodIds[itemcount][1]);
//							nlapiLogExecution('DEBUG','pickMethodIds[itemcount][2]',pickMethodIds[itemcount][2]);
							//for a particular pickmethod ,if max order no. is not null and max pickno. is null
							//then the below code execute.

							if(pickMethodIds[itemcount][1]!="" && pickMethodIds[itemcount][2]=="")
							{
								nlapiLogExecution('DEBUG','BlockOfCodeForMaxPickIsNULL',ebizOrderId[loopcount]);
								if(ordno!=ebizOrderId[loopcount])
								{
									//if(parseFloat(tempcount)<pickMethodIds[itemcount][1] && tempcount !=0)
									//above code is commented becuase seq# is missing while generating clusters.
									if(parseFloat(tempcount)<pickMethodIds[itemcount][1])
									{
										tempcount=parseFloat(tempcount)+1;
										nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
										ordno = ebizOrderId[loopcount];
									}
									else
									{
										clusterno = GetMaxTransactionNo('WAVECLUSTER');
										nlapiLogExecution('DEBUG','clusternoafterelse1',clusterno);
										nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
										ordno = ebizOrderId[loopcount];
										tempcount=1;
									}
								}
								else
								{
									nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
								}
							}

							//for a particular pickmethod ,if max orderno. is null and max pickno.is not null 
							//then the below code execute.
							if(pickMethodIds[itemcount][1] == "" && pickMethodIds[itemcount][2] != "")
							{
								nlapiLogExecution('DEBUG','BlockOfCodeForMaxPickIsNULL',pickMethodIds[itemcount][2]);
								tempcount=parseFloat(tempcount)+1;
								if(parseFloat(tempcount)<=pickMethodIds[itemcount][2])
								{
									nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
								}
								else
								{
									clusterno = GetMaxTransactionNo('WAVECLUSTER');
									nlapiLogExecution('DEBUG','clusternoafterelse2',clusterno);
									nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
									tempcount=1;
								}
							}

							//for a pickmethod, if max orderno. and max pickno is not null 
							//then the below code execute.
							if(pickMethodIds[itemcount][1]!= "" && pickMethodIds[itemcount][2] != "")
							{
								//nlapiLogExecution('DEBUG','BlockOfCodeForNONEIsNULL','chkpt');
								maxPickFlagCount= parseFloat(maxPickFlagCount)+1;
//								nlapiLogExecution('DEBUG','maxPickFlagCount',maxPickFlagCount);
//								nlapiLogExecution('DEBUG','pickMethodIds[itemcount][2]',pickMethodIds[itemcount][2]);
//								nlapiLogExecution('DEBUG','ordno',ordno);
//								nlapiLogExecution('DEBUG','ebizOrderId[loopcount]',ebizOrderId[loopcount]);
								if(parseFloat(maxPickFlagCount)<=pickMethodIds[itemcount][2])
								{
									if(ordno!=ebizOrderId[loopcount])
									{
										tempcount=parseFloat(tempcount)+1;

										if(parseFloat(tempcount)<=pickMethodIds[itemcount][1])
										{
											nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
											ordno = ebizOrderId[loopcount];
										}
										else
										{
											clusterno = GetMaxTransactionNo('WAVECLUSTER');
											nlapiLogExecution('DEBUG','clusternoafterelse3',clusterno);
											nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
											ordno = ebizOrderId[loopcount];
											tempcount=1;
										}
									}
									else
									{
										nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
									}
								}
								else
								{
									clusterno = GetMaxTransactionNo('WAVECLUSTER');
//									nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
									maxPickFlagCount= 1;

									if(ordno!=ebizOrderId[loopcount])
									{
										tempcount=parseFloat(tempcount)+1;

										if(parseFloat(tempcount)<=pickMethodIds[itemcount][1])
										{
											nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
											ordno = ebizOrderId[loopcount];
										}
										else
										{
											clusterno = GetMaxTransactionNo('WAVECLUSTER');
											nlapiLogExecution('DEBUG','clusternoafterelse4',clusterno);
											nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
											ordno = ebizOrderId[loopcount];
											tempcount=1;
										}
									}
									else
									{
										nlapiSubmitField('customrecord_ebiznet_trn_opentask', ebizInternalRecordId[loopcount], 'custrecord_ebiz_clus_no',clusterno.toString());
									}
								}
							}
						}
					}
				}
			}
		}
	}
}


function setCountAgainstEachContainerLp(vwaveno,vordno)
{
	try
	{
		var vopentaskRec=getOpentaskRec(vwaveno,vordno);
		var ID;
		//nlapiLogExecution('Debug','vopentaskRec',vopentaskRec);
		var vContainerLP=new Array();
		for ( var count = 0; vopentaskRec!=null && count < vopentaskRec.length; count++) 
		{
			var vConLp=vopentaskRec[count].getValue('custrecord_container_lp_no');
			if(vConLp!=""&&vConLp!=null)
				vContainerLP[count]=vConLp;
		}
		//nlapiLogExecution('Debug','vcontainerLP',vContainerLP);
		var vDistinctContainerLp=removeDuplicateElement(vContainerLP);
		vDistinctContainerLp.sort();
		//nlapiLogExecution('Debug','vDistinctContainerLp',vDistinctContainerLp);
		for ( var vcount = 0; vDistinctContainerLp!=null && vcount < vDistinctContainerLp.length; vcount++) 
		{
			var Maxcount=parseInt(vcount)+1;
			var filterLpMaster=new Array();
			filterLpMaster[0]=new nlobjSearchFilter('name',null,'is',vDistinctContainerLp[vcount]);

			var rec=nlapiSearchRecord('customrecord_ebiznet_master_lp',null,filterLpMaster,null);
			if(rec!=null&&rec!="")
			{
				var fields=['custrecord_ebiz_lpmaster_pkgno','custrecord_ebiz_lpmaster_pkgcount'];
				var values=[Maxcount,vDistinctContainerLp.length.toString()];
				ID=nlapiSubmitField('customrecord_ebiznet_master_lp', rec[0].getId(),fields,values );
			}
			//nlapiLogExecution('Debug','ID',ID);
		}
	}
	catch(exp)
	{
		nlapiLogExecution('Debug','Exception in setCountAgainstEachContainerLp',exp);
	}
}

function getOpentaskRec(vwaveno,vordno)
{

	try{
		nlapiLogExecution('Debug','vwaveno',vwaveno);
		nlapiLogExecution('Debug','vordno',vordno);
		var opentasks;

		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', vordno));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', vwaveno));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
		columns[0].setSort();

		opentasks = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	

		return opentasks;
	}
	catch(exp)
	{
		nlapiLogExecution('Debug','getOpentaskRec',getOpentaskRec);
	}
}

function getOpenPickqty(actEndLocation)
{
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', actEndLocation);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]);				
	filters[2] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	return searchresults;
}

function getOpenputawayqty(actEndLocation)
{
	nlapiLogExecution('Debug','actEndLocation',actEndLocation);
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', actEndLocation);
	filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);				
	filters[2] = new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return searchresults;
}

function getopenreplens(controlno,fulfilmentItem,pfLocationId,pfWHLoc)
{
	nlapiLogExecution('Debug','controlno',controlno);
	nlapiLogExecution('Debug','fulfilmentItem',fulfilmentItem);
	var filters = new Array();
	//filters[0] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', controlno); // Remove this from where clause
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8])); // Replen task
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
	filters.push(new nlobjSearchFilter('custrecord_sku', null,'anyof' ,fulfilmentItem));
	filters.push(new nlobjSearchFilter('custrecord_actendloc', null,'anyof' ,pfLocationId));
//	if(pfWHLoc != null && pfWHLoc != '')
	//	filters.push(new nlobjSearchFilter('custrecord_site_id', null,'is' ,pfWHLoc));
	// Add pickface location filter
	// Add location(site) for filter

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_expe_qty', null, 'sum');


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	return searchresults;
}

function getQOHForAllSKUsinPFLocation(actEndLocation,ItemId,WHLoc){

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', actEndLocation));
	//filters.push(new nlobjSearchFilter('custrecord_ebizlocationtype','custrecord_ebiz_inv_binloc', 'anyof', [6,7]));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_avl_qty', null, 'greaterthan', 0)); //Comment this
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', ItemId));
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19','3']));
	//if(WHLoc != null && WHLoc !='')
	//filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', WHLoc));
	var columns = new Array();	
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh',null,'sum');



	var pfSKUInvList = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

	return pfSKUInvList;
}
function buildItemListForReplenProcessing(replenItemArray, itemArrayLength){
	var replenItemList = new Array();

	for (var p = 0; p < itemArrayLength; p++){
		replenItemList.push(replenItemArray[p][0]);
	}

	return replenItemList;
}

function getdimsforallitems(ebizskuno,whLocation) {
	nlapiLogExecution('Debug', 'into getdimsforallitems', '');

	var cubelist = new Array();
	var uomcube="";

	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'anyof', ebizskuno));
	filters.push(new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	
	if(whLocation!=null && whLocation!='' && whLocation!='null')
		filters.push(new nlobjSearchFilter('custrecord_ebizsiteskudim', null, 'anyof', ['@NONE@', whLocation]));	

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
	columns[1] = new nlobjSearchColumn('custrecord_ebizweight');
	columns[2] = new nlobjSearchColumn('custrecord_ebizlength');
	columns[3] = new nlobjSearchColumn('custrecord_ebizwidth');
	columns[4] = new nlobjSearchColumn('custrecord_ebizheight');
	columns[5] = new nlobjSearchColumn('custrecord_ebizitemdims');	

	cubelist = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

	nlapiLogExecution('Debug', 'out of getdimsforallitems', '');

	return cubelist;

}

function getUOMCube(uomlevel,ebizskuno,whlocation) {
	nlapiLogExecution('DEBUG', 'Into getUOMCube', str);
	var str = 'uomlevel. = ' + uomlevel + '<br>';
	str = str + 'ebizskuno. = ' + ebizskuno + '<br>';	
	str = str + 'whlocation. = ' + whlocation + '<br>';	

	nlapiLogExecution('DEBUG', 'getUOMCube Parameters', str);

	var cubelist = new Array();
	var uomcube="";

	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', ebizskuno));
	//filters.push(new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'is', uomlevel));	
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(whlocation!=null && whlocation!='' && whlocation!='null')
		filters.push(new nlobjSearchFilter('custrecord_ebizsiteskudim', null, 'anyof',['@NONE@',whlocation]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
	columns[1] = new nlobjSearchColumn('custrecord_ebizweight');
	columns[2] = new nlobjSearchColumn('custrecord_ebizlength');
	columns[3] = new nlobjSearchColumn('custrecord_ebizwidth');
	columns[4] = new nlobjSearchColumn('custrecord_ebizheight');
	columns[5] = new nlobjSearchColumn('custrecord_ebizqty');

	cubelist = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

	nlapiLogExecution('DEBUG', 'out of getUOMCube', cubelist);

	return cubelist;

}



function PickGenerationScheduler(type)
{
	var context = nlapiGetContext(); 
	var vordno="";
	var vordlineno="";
	var vwaveno="";
	var vlastlineinorder='F';
	var vlocation="";
	var vcompany="";
	var userId="";
	var allocstrategy='';
	var vlastlineinwave='F';
	var vwaveordrecid=-1;
	var vusedflag='';
	var voldordno='';
	var lefoflag='';
	var vwaveassignedto='';
	var vMainReportNo="";

	try{
		var vcuruserId = context.getUser();

		vwaveno = context.getSetting('SCRIPT', 'custscript_ebizwaveno');
		vwaveassignedto = context.getSetting('SCRIPT', 'custscript_ebizwaveassignto');

		updateScheduleScriptStatus('WAVE RELEASE',vcuruserId,'In Progress',vwaveno,null);

		if (vMainReportNo == null || vMainReportNo == "") {
			vMainReportNo = GetMaxTransactionNo('REPORT');
			vMainReportNo = vMainReportNo.toString();
		}


		var str = 'vwaveno. = ' + vwaveno + '<br>';
		str = str + 'vwaveassignedto. = ' + vwaveassignedto + '<br>';	
		str = str + 'type. = ' + type + '<br>';	
		str = str + 'vMainReportNo. = ' + vMainReportNo + '<br>';	
		str = str + 'Remaining usage. = ' + context.getRemainingUsage() + '<br>';	
		str = str + 'Time Stamp. = ' + TimeStampinSec() + '<br>';	

		nlapiLogExecution('ERROR', 'Parameter Details', str);


		try{

			// The below code will be executed when JVM restarts
			if(type == 'aborted' &&
					vwaveno != null)
			{
				cancelwave(vwaveno);
			}

			//Upto here - JVM Restart

			//if(type!='aborted' && vwaveno != null)
			if(vwaveno != null)
			{
				var fulfillmentordarr = new Array();
				var itemsarr = new Array();
				var itemdimsarr = new Array();
				var pflocationsarr = new Array();
				var ordarr = new Array();
				var ordlocarray = new Array();

				//System Rule
				var vShipAloneRULE = getSystemRuleValue('ShipAlone');
				nlapiLogExecution('DEBUG', 'vShipAloneRULE',vShipAloneRULE);

				nlapiLogExecution('Debug', 'Time Stamp before getting fulfillment details',TimeStampinSec());
				var fulfillOrdersList = getOrderDetailsForWaveGen(vordno,vordlineno,vwaveno);
				nlapiLogExecution('Debug', 'Time Stamp After getting fulfillment details',TimeStampinSec());

				if(fulfillOrdersList!=null && fulfillOrdersList.length > 0)
				{
					for (var i1 = 0; i1 < fulfillOrdersList.length; i1++){

						if(fulfillmentordarr.indexOf(fulfillOrdersList[i1][4]) == -1)
						{
							fulfillmentordarr.push(fulfillOrdersList[i1][4]);
						}

						if(itemsarr.indexOf(fulfillOrdersList[i1][6]) == -1)
						{
							itemsarr.push(fulfillOrdersList[i1][6]);
						}	

						if(ordarr.indexOf(fulfillOrdersList[i1][1]) == -1)
						{
							ordarr.push(fulfillOrdersList[i1][1]);
						}	
						if(ordlocarray.indexOf(fulfillOrdersList[i1][19]) == -1)
						{
							ordlocarray.push(fulfillOrdersList[i1][19]);
						}
					}
				}

				nlapiLogExecution('DEBUG', 'itemsarr',itemsarr);

				nlapiLogExecution('Debug', 'Time Stamp before getting containers details',TimeStampinSec());

				var containerslist='';
				if(ordlocarray.length>0)
				{
					ordlocarray.push('@NONE@');
					nlapiLogExecution('DEBUG', 'ordlocarray',ordlocarray);
					containerslist = getAllContainersList(ordlocarray);

				}
				else
				{
					containerslist = getAllContainersList(null);
				}
				nlapiLogExecution('Debug', 'Time Stamp After getting containers details',TimeStampinSec());

				nlapiLogExecution('Debug', 'Time Stamp before getting getAllPickRules',TimeStampinSec());
				var pickruleslist = getAllPickRules(null,null,null,null,null,null);
				nlapiLogExecution('Debug', 'Time Stamp after getting getAllPickRules',TimeStampinSec());

				var pickzonearray = new Array();
				for(f = 0; f < parseInt(pickruleslist.length); f++)
				{
					if(pickruleslist[f].getValue('custrecord_ebizpickzonerul')!=null && pickruleslist[f].getValue('custrecord_ebizpickzonerul')!='')
						pickzonearray.push(pickruleslist[f].getValue('custrecord_ebizpickzonerul'));
				}

				//nlapiLogExecution('Debug', 'Time Stamp Before calling allZoneLocnGroups',TimeStampinSec());
				var allZoneLocnGroups = getAllZoneLocnGroupsList(pickzonearray,-1);
				//nlapiLogExecution('Debug', 'Time Stamp After calling allZoneLocnGroups',TimeStampinSec());

				//nlapiLogExecution('Debug', 'Time Stamp before getting getListForAllReplenZones',TimeStampinSec());
				var replenRulesList = getListForAllReplenZones();
				//nlapiLogExecution('Debug', 'Time Stamp After getting getListForAllReplenZones',TimeStampinSec());

				//nlapiLogExecution('Debug', 'Time Stamp Before calling  for all items',TimeStampinSec());
				itemdimsarr = getItemDimensions(itemsarr,-1);
				//nlapiLogExecution('Debug', 'Time Stamp After calling  for all items',TimeStampinSec());

				//nlapiLogExecution('Debug', 'Time Stamp Before calling getPFLocationsForOrder for all items',TimeStampinSec());
				pflocationsarr = getPFLocationsForOrder(null,itemsarr);
				//nlapiLogExecution('Debug', 'Time Stamp After calling getPFLocationsForOrder for all items',TimeStampinSec());

				nlapiLogExecution('Debug', 'Time Stamp Before calling getItemDetails',TimeStampinSec());

				var allinventorySearchResults = null;

				//The below code i commeneted by Satish.N as part of performance fine tuning.
				//allinventorySearchResults = getItemDetails(null, itemsarr,null,null,lefoflag);

				nlapiLogExecution('Debug', 'Time Stamp After calling getItemDetails',TimeStampinSec());


				//The below code is commented by Satish.N on 02DEC2015 as we are removing bulk updates due to JVM crash issue
				/*
				 * nlapiLogExecution('Debug', 'Time Stamp Before parent records creation',TimeStampinSec());
				var otparent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); //create new parent record on open task.

				var parent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); //create new parent record on fo line.
				var foparentid = nlapiSubmitRecord(parent); 
				var foparent = nlapiLoadRecord('customrecord_ebiz_throwaway_parent', foparentid);
				nlapiLogExecution('Debug', 'Time Stamp After parent records creation',TimeStampinSec());
				 */
				//Upto here

				/*var parentinv = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); //create new parent record on inventory.
				var invtparentid = nlapiSubmitRecord(parentinv); 
				var invtparent = nlapiLoadRecord('customrecord_ebiz_throwaway_parent', invtparentid);*/

				var invtparent=null;
				var foparent=null;
				var otparent=null;

				if(fulfillmentordarr!=null && fulfillmentordarr.length > 0)
				{
					for (var i2 = 0; i2 < fulfillmentordarr.length; i2++){

						//nlapiLogExecution('Debug','# of Order',i2);

						nlapiLogExecution('Debug','Remaining USage ',context.getRemainingUsage());

						if(context.getRemainingUsage()<1000)
						{	
							// The below code is commented by Satish.N on 02DEC2015

							/*
							//The parent records submition should be here
							if(vInvtArr != null && vInvtArr != '' && vInvtArr.length>0)
							{
								updateInventoryRecordBulk(vInvtArr,invtparent);
							}

							if(vFulfillArr != null && vFulfillArr != '' && vFulfillArr.length>0)
								updateFulfilmentOrderBulk(vFulfillArr,foparent);

							nlapiSubmitRecord(otparent); //submit the parent record, all child records will also be updated
							nlapiSubmitRecord(foparent); //submit the parent record, all child records will also be updated
							//nlapiSubmitRecord(invtparent); //submit the parent record, all child records will also be updated

							 */
							//Upto here commented by Satish.N on 02DEC2015

							nlapiLogExecution('Debug','Calling Second Scheduler - Remaining Usage',context.getRemainingUsage());
							var param = new Array();
							param['custscript_ebizwaveno'] = vwaveno;
							nlapiScheduleScript('customscript_ebiz_pickgen_scheduler', null,param);
							return;
						}

						var fulfillmentorder = new Array();

						var vfulfillmentordno = fulfillmentordarr[i2];
						nlapiLogExecution('Debug', 'vfulfillmentordno',vfulfillmentordno);

						for (var i3 = 0; i3 < fulfillOrdersList.length; i3++){

							if(vfulfillmentordno == fulfillOrdersList[i3][4])
							{								
								var str = 'vfulfillmentordno. = ' + vfulfillmentordno + '<br>';
								str = str + 'Remaining usage. = ' + context.getRemainingUsage() + '<br>';	
								str = str + 'Time Stamp. = ' + TimeStampinSec() + '<br>';	

								nlapiLogExecution('ERROR', 'Remaining Usage Details', str);

								fulfillmentorder.push(fulfillOrdersList[i3]);

								if((i3==fulfillOrdersList.length-1) || (vfulfillmentordno != fulfillOrdersList[i3+1][4]) )
								{
									var inventorySearchResults = new Array();
									var allocConfigDtls = new Array();						

									nlapiLogExecution('Debug', 'Time Stamp before calling allocateQuantity',TimeStampinSec());

									var LPparent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); 

									allocateQuantity(inventorySearchResults, fulfillmentorder, vwaveno, allocConfigDtls,
											containerslist,LPparent,replenRulesList,pickruleslist,allZoneLocnGroups,itemdimsarr,
											pflocationsarr,allinventorySearchResults,otparent,foparent,invtparent,vShipAloneRULE,vwaveassignedto,vMainReportNo);

									nlapiSubmitRecord(LPparent); 

									nlapiLogExecution('Debug', 'Time Stamp After calling allocateQuantity',TimeStampinSec());

									var EbizOrderNo = fulfillmentorder[0][1]; 
									var fono = fulfillmentorder[0][4]; 
									var company = fulfillmentorder[0][13];
									var whlocation = fulfillmentorder[0][19];


									nlapiLogExecution('Debug','Remaining usage at the start of ActualCartonization',context.getRemainingUsage());


									//Set the Count value against each ContainerLP in sequence order
									//eg:If for order 4 Contaienerlp's(lp10,lp20,lp30 and lp40) are generated 
									//Then v r setting the values as lp10 of 1,lp20 of 2,lp30 of 3 and lp40 of 4.

									nlapiLogExecution('Debug', 'Time Stamp After pick generation for an order',TimeStampinSec());

									//nlapiLogExecution('Debug', 'Time Stamp Before calling setCountAgainstEachContainerLp',TimeStampinSec());

									//setCountAgainstEachContainerLp(vwaveno,EbizOrderNo);

									//nlapiLogExecution('Debug', 'Time Stamp After calling setCountAgainstEachContainerLp',TimeStampinSec());


									if(i2==fulfillmentordarr.length-1 && i3==fulfillOrdersList.length-1)
									{
										// The below code is commented by Satish.N on 02DEC2015

										/*
										//The parent records submition should be here
										if(vInvtArr != null && vInvtArr != '' && vInvtArr.length>0)
										{
											updateInventoryRecordBulk(vInvtArr,invtparent);
										}

										if(vFulfillArr != null && vFulfillArr != '' && vFulfillArr.length>0)
											updateFulfilmentOrderBulk(vFulfillArr,foparent);

										nlapiSubmitRecord(otparent); //submit the parent record, all child records will also be updated
										nlapiSubmitRecord(foparent); //submit the parent record, all child records will also be updated
										//nlapiSubmitRecord(invtparent); //submit the parent record, all child records will also be updated
										nlapiLogExecution('DEBUG', 'Inventory Updated');	
										 */
										//Upto here

										//Code Added by suman on 24th June 2014.
										//Reverse the qty for kit/package item if any one member item has insufficient inventory.
										for (var z3 = 0; z3 < ordarr.length; z3++)
										{
											nlapiLogExecution('DEBUG', 'Order #',ordarr[z3]);
											ReverseInvtForKITItem(vwaveno,ordarr[z3],vFulfillArr);
										}
										//End of code as on 24th June
										var LPparent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); //create new parent record on LP Master.
										// case# 201412144
										nlapiLogExecution('Debug', 'Time Stamp before ActualCartonization loop',TimeStampinSec());
										for (var z3 = 0; z3 < ordarr.length; z3++){

											nlapiLogExecution('DEBUG', 'Order #',ordarr[z3]);

											ActualCartonization(vwaveno,ordarr[z3],containerslist,whlocation,LPparent);	

										}
										nlapiLogExecution('Debug', 'Time Stamp after ActualCartonization loop',TimeStampinSec());
										nlapiSubmitRecord(LPparent);

										setClusterNo(vwaveno);

										if(vShipAloneRULE=="Y")
										{
											nlapiLogExecution("ERROR","INTO Creating ShipManifest Record",vShipAloneRULE);
											for (var z3 = 0; z3 < ordarr.length; z3++){

												nlapiLogExecution('DEBUG', 'Order2 #',ordarr[z3]);

												createshipmanifestRecord(vwaveno,ordarr[z3]);	
											}
										}									

										generatePickreporthtml(vwaveno);
										try
										{
											var systemIntegrationRulevalue=getIntegrationSystemRuleValue("SHIP-PickLabel", "");
											if(systemIntegrationRulevalue!=null && systemIntegrationRulevalue!="")
											{
												var systemrulevalue = systemIntegrationRulevalue[0];
												var systemruletype = systemIntegrationRulevalue[1];
												if (systemruletype == "BartenderFormat") 
												{

													if (systemrulevalue == "Standard")
													{
														generatebartenderpicklabel(vwaveno);
													}
													else if(systemrulevalue == "Custom")
													{

														generateCustomBartenderPickLabel(vwaveno);

													}
												}
											}
										}
										catch(exp)
										{
											nlapiLogExecution('DEBUG', 'Expection in   Pick Label Generation',exp);
										}
										nlapiLogExecution('Debug', 'Time Stamp before generatePickreporthtml loop',TimeStampinSec());
										var Pickreportrequired = getSystemRuleValue('Email "pick report PDF" after wave creation & release?');
										if(Pickreportrequired=='Y')
										{
											pickreportscheduler(vwaveno);
										}
										nlapiLogExecution('Debug', 'Time Stamp before pickreportscheduler loop',TimeStampinSec());
										var tranType = nlapiLookupField('transaction', EbizOrderNo, 'recordType');
										updateScheduleScriptStatus('WAVE RELEASE',vcuruserId,'Completed',vwaveno,tranType);

										nlapiLogExecution('Debug', 'Time Stamp After calling generatePickreporthtml',TimeStampinSec());
									}
								}
							}
						}

						nlapiLogExecution('Debug', 'Time Stamp at the end of scheduler',TimeStampinSec());
					}
				}

			}

		}
		catch(exp) 
		{
			nlapiLogExecution('Debug', 'Exception in PickGenerationSchScript : ', exp);		
		}
	}
	catch(exp1) 
	{
		nlapiLogExecution('Debug', 'Exception in PickGenerationSchScript1 : ', exp1);	
	}

	nlapiLogExecution('Debug','Remaining usage at the end',context.getRemainingUsage());
}

function getAllPickRules(Item,uomlevel,whLocation,ItemStatus,Packcode,OrderType){

	nlapiLogExecution('Debug', 'Into getAllPickRules','');

	var pickRulesList = new Array();

	var ItemFamily = null;
	var ItemGroup = null;
	var ItemInfo1 = null;
	var ItemInfo2 = null;
	var ItemInfo3 = null;
	var putVelocity = null;

	var filters = new Array();

	if(ItemFamily!=null && ItemFamily != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskufamilypickrul', null, 'anyof', ['@NONE@', ItemFamily]));
	}

	if(ItemGroup!=null && ItemGroup != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskugrouppickrul', null, 'anyof', ['@NONE@', ItemGroup]));
	}

	if(ItemStatus!=null && ItemStatus != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskustatspickrul', null, 'anyof', ['@NONE@', ItemStatus]));
	}

	if(ItemInfo1!=null && ItemInfo1 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo1', null, 'anyof',['@NONE@', ItemInfo1]));
	}

	if(ItemInfo2!=null && ItemInfo2 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo2', null, 'anyof',['@NONE@', ItemInfo2]));
	}

	if(ItemInfo3!=null && ItemInfo3 != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskuinfo3', null, 'anyof',['@NONE@', ItemInfo3]));
	}

	if(Item != null && Item != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizskupickrul', null, 'anyof', ['@NONE@', Item]));		
	}

	if(putVelocity != null && putVelocity != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizabcvel', null, 'anyof', ['@NONE@', putVelocity]));
	}

	if(whLocation != null && whLocation != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizsitepickrule', null, 'anyof', [whLocation]));
	}

	if(uomlevel != null && uomlevel != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizuomlevelpickrul', null, 'anyof', ['@NONE@', uomlevel]));
	}

	if(Packcode != null && Packcode != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizpackcodepickrul', null, 'anyof', ['@NONE@', Packcode]));
	}

	if(OrderType != null && OrderType != "")
	{
		filters.push(new nlobjSearchFilter('custrecord_ebizordertypepickrul', null, 'anyof', ['@NONE@', OrderType]));
	}

	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizruleidpick');
	columns[1] = new nlobjSearchColumn('custrecord_ebizpickmethod');
	columns[2] = new nlobjSearchColumn('custrecord_ebizpickzonerul');
	columns[3] = new nlobjSearchColumn('custrecord_ebizlocationgrouppickrul');
	columns[4] = new nlobjSearchColumn('custrecord_ebizlocationpickrul');
	//columns[5] = new nlobjSearchColumn('custrecord_ebizsequencenopickrul');
	columns[5]=new nlobjSearchColumn('formulanumeric');
	columns[5].setFormula("TO_NUMBER({custrecord_ebizsequencenopickrul})");
	columns[6] = new nlobjSearchColumn('custrecord_ebizskufamilypickrul');
	columns[7] = new nlobjSearchColumn('custrecord_ebizskugrouppickrul');
	columns[8] = new nlobjSearchColumn('custrecord_ebizskupickrul');
	columns[9] = new nlobjSearchColumn('custrecord_ebizskuinfo1');
	columns[10] = new nlobjSearchColumn('custrecord_ebizskuinfo2');
	columns[11] = new nlobjSearchColumn('custrecord_ebizskuinfo3');
	columns[12] = new nlobjSearchColumn('custrecord_ebizpackcodepickrul');
	columns[13] = new nlobjSearchColumn('custrecord_ebizuomlevelpickrul');
	columns[14] = new nlobjSearchColumn('custrecord_ebizskustatspickrul');
	columns[15] = new nlobjSearchColumn('custrecord_ebizordertypepickrul');
	columns[16] = new nlobjSearchColumn('custrecord_ebizuompickrul');
	columns[17] = new nlobjSearchColumn('custrecord_allocation_strategy','custrecord_ebizpickmethod');
	columns[18] = new nlobjSearchColumn('custrecord_ebizpickfacelocation','custrecord_ebizpickmethod');
	columns[19] = new nlobjSearchColumn('custrecord_ebizcreatecluster','custrecord_ebizpickmethod');
	columns[20] = new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxorders','custrecord_ebizpickmethod');
	columns[21] = new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxpicks','custrecord_ebizpickmethod');
	columns[22] = new nlobjSearchColumn('custrecord_ebizabcvel');
	columns[23] = new nlobjSearchColumn('custrecord_cartonization_method','custrecord_ebizpickmethod');
	columns[24] = new nlobjSearchColumn('custrecord_ebizsitepickrule');
	columns[25] = new nlobjSearchColumn('custrecord_ebiz_stagedetermination','custrecord_ebizpickmethod');
	columns[26] = new nlobjSearchColumn('custrecord_ebiz_isforwardpick','custrecord_ebizpickmethod');
	columns[5].setSort();

	pickRulesList = new nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters, columns);

	nlapiLogExecution('Debug', 'Out of getAllPickRules','');

	return pickRulesList;
}

function getListOfAllPickRules(Item,uomlevel,whLocation,ItemStatus,Packcode,OrderType,
		pickRulesList,pflocationsarr,uom){

	nlapiLogExecution('Debug', 'Into getListOfAllPickRules','');

	var ItemPickRules = new Array();
	var allocConfigDtls = new Array();
	var pickRuleCount = 0;

	var fields = ['custitem_item_family','custitem_item_group','custitem_ebizdefskustatus',
	              'custitem_item_info_1','custitem_item_info_2','custitem_item_info_3','custitem_ebizabcvelitem'];


	var columns= nlapiLookupField('Item',Item,fields);
	var ItemFamily = columns.custitem_item_family;
	var ItemGroup = columns.custitem_item_group;
	var ItemInfo1 = columns.custitem_item_info_1;
	var ItemInfo2 = columns.custitem_item_info_2;
	var ItemInfo3 = columns.custitem_item_info_3;
	var putVelocity = columns.custitem_ebizabcvelitem;

	var str = 'Item. = ' + Item + '<br>';
	str = str + 'ItemFamily. = ' + ItemFamily + '<br>';	
	str = str + 'ItemGroup. = ' + ItemGroup + '<br>';
	str = str + 'ItemInfo1. = ' + ItemInfo1 + '<br>';
	str = str + 'ItemInfo2. = ' + ItemInfo2 + '<br>';
	str = str + 'ItemInfo3. = ' + ItemInfo3 + '<br>';
	str = str + 'putVelocity. = ' + putVelocity + '<br>';
	str = str + 'uomlevel. = ' + uomlevel + '<br>';
	str = str + 'whLocation. = ' + whLocation + '<br>';
	str = str + 'ItemStatus. = ' + ItemStatus + '<br>';
	str = str + 'Packcode. = ' + Packcode + '<br>';
	str = str + 'OrderType. = ' + OrderType + '<br>';
	str = str + 'uom. = ' + uom + '<br>';

	nlapiLogExecution('Debug', 'Parameter Details', str);


	if(pickRulesList != null && pickRulesList != '' && pickRulesList.length>0)
	{
		nlapiLogExecution('Debug', 'pickRulesList length', pickRulesList.length);

		for(var j = 0; j < pickRulesList.length; j++){

			var ruleitemfamily = pickRulesList[j].getValue('custrecord_ebizskufamilypickrul');
			var ruleitemgroup = pickRulesList[j].getValue('custrecord_ebizskugrouppickrul');
			var ruleitem = pickRulesList[j].getValue('custrecord_ebizskupickrul');
			var ruleitemstatus = pickRulesList[j].getValue('custrecord_ebizskustatspickrul');
			var ruleiteminfo1 = pickRulesList[j].getValue('custrecord_ebizskuinfo1');
			var ruleiteminfo2 = pickRulesList[j].getValue('custrecord_ebizskuinfo2');
			var ruleiteminfo3 = pickRulesList[j].getValue('custrecord_ebizskuinfo3');
			var ruleputvelocity = pickRulesList[j].getValue('custrecord_ebizabcvel');
			var ruleordertype = pickRulesList[j].getValue('custrecord_ebizordertypepickrul');
			var rulepackcode = pickRulesList[j].getValue('custrecord_ebizpackcodepickrul');
			var rulewhlocation = pickRulesList[j].getValue('custrecord_ebizsitepickrule');
			var ruleuomlevel = pickRulesList[j].getValue('custrecord_ebizuomlevelpickrul');

			if((ruleitemfamily==null || ruleitemfamily=='')||(ruleitemfamily != null && ruleitemfamily != '' && ruleitemfamily == ItemFamily))	
			{
				if((ruleitemgroup==null || ruleitemgroup=='')||(ruleitemgroup != null && ruleitemgroup != '' && ruleitemgroup == ItemGroup))	
				{
					if((ruleitemstatus==null || ruleitemstatus=='')||(ruleitemstatus != null && ruleitemstatus != '' && ruleitemstatus == ItemStatus))	
					{
						if((ruleiteminfo1==null || ruleiteminfo1=='')||(ruleiteminfo1 != null && ruleiteminfo1 != '' && ruleiteminfo1 == ItemInfo1))	
						{
							if((ruleiteminfo2==null || ruleiteminfo2=='')||(ruleiteminfo2 != null && ruleiteminfo2 != '' && ruleiteminfo2 == ItemInfo2))	
							{
								if((ruleiteminfo3==null || ruleiteminfo3=='')||(ruleiteminfo3 != null && ruleiteminfo3 != '' && ruleiteminfo3 == ItemInfo3))	
								{
									if((ruleitem==null || ruleitem=='')||(ruleitem != null && ruleitem != '' && ruleitem == Item))	
									{
										if((ruleputvelocity==null || ruleputvelocity=='')||(ruleputvelocity != null && ruleputvelocity != '' && ruleputvelocity == putVelocity))	
										{
											if((rulewhlocation==null || rulewhlocation=='')||(rulewhlocation != null && rulewhlocation != '' && rulewhlocation == whLocation))	
											{
												if((ruleuomlevel==null || ruleuomlevel=='')||(ruleuomlevel != null && ruleuomlevel != '' && ruleuomlevel == uomlevel))	
												{
													if((rulepackcode==null || rulepackcode=='')||(rulepackcode != null && rulepackcode != '' && rulepackcode == Packcode))	
													{
														if((ruleordertype==null || ruleordertype=='')||(ruleordertype != null && ruleordertype != '' && ruleordertype == OrderType))	
														{
															var pickRuleId = pickRulesList[j].getValue('custrecord_ebizruleidpick');
															var pickMethodId = pickRulesList[j].getValue('custrecord_ebizpickmethod');
															var pickZoneId = pickRulesList[j].getValue('custrecord_ebizpickzonerul');
															var locationGroupId = pickRulesList[j].getValue('custrecord_ebizlocationgrouppickrul');
															var binLocationId = pickRulesList[j].getValue('custrecord_ebizlocationpickrul');
															var sequenceNo = pickRulesList[j].getValue('custrecord_ebizsequencenopickrul');
															var itemFamilyId = pickRulesList[j].getValue('custrecord_ebizskufamilypickrul');
															var itemGroupId = pickRulesList[j].getValue('custrecord_ebizskugrouppickrul');
															var itemId = pickRulesList[j].getValue('custrecord_ebizskupickrul');
															var itemInfo1 = pickRulesList[j].getValue('custrecord_ebizskuinfo1');
															var itemInfo2 = pickRulesList[j].getValue('custrecord_ebizskuinfo2');
															var itemInfo3 = pickRulesList[j].getValue('custrecord_ebizskuinfo3');
															var packCode = pickRulesList[j].getValue('custrecord_ebizpackcodepickrul');
															var uomLevel = pickRulesList[j].getValue('custrecord_ebizuomlevelpickrul');
															var itemStatus = pickRulesList[j].getValue('custrecord_ebizskustatspickrul');
															var orderType = pickRulesList[j].getValue('custrecord_ebizordertypepickrul');
															var vuom = pickRulesList[j].getValue('custrecord_ebizuompickrul');
															var allocationStrategy = pickRulesList[j].getValue('custrecord_allocation_strategy','custrecord_ebizpickmethod');
															var pickfaceLocationFlag = pickRulesList[j].getValue('custrecord_ebizpickfacelocation','custrecord_ebizpickmethod');
															var clusterPickFlag = pickRulesList[j].getValue('custrecord_ebizcreatecluster','custrecord_ebizpickmethod');
															var maxOrders = pickRulesList[j].getValue('custrecord_ebiz_pickmethod_maxorders','custrecord_ebizpickmethod');
															var maxPicks = pickRulesList[j].getValue('custrecord_ebiz_pickmethod_maxpicks','custrecord_ebizpickmethod');
															var abcVel = pickRulesList[j].getValue('custrecord_abcvelpickrule');
															var cartonizationmethod = pickRulesList[j].getValue('custrecord_cartonization_method','custrecord_ebizpickmethod');
															var picklocation=pickRulesList[j].getValue('custrecord_ebizsitepickrule');
															var stagedetermination = pickRulesList[j].getValue('custrecord_ebiz_stagedetermination','custrecord_ebizpickmethod');
															var isforwardpickmethod = pickRulesList[j].getValue('custrecord_ebiz_isforwardpick','custrecord_ebizpickmethod');

															var currentRow = [j, pickRuleId, pickMethodId, pickZoneId, locationGroupId, binLocationId, 
															                  sequenceNo, itemFamilyId, itemGroupId, itemId, itemInfo1, itemInfo2, itemInfo3, 
															                  packCode, uomLevel, itemStatus, orderType, vuom,allocationStrategy, pickfaceLocationFlag, 
															                  clusterPickFlag, maxOrders, maxPicks, abcVel,cartonizationmethod,picklocation,stagedetermination,isforwardpickmethod];

															ItemPickRules.push(currentRow);

														}
													}
												}
											}
										}
									}
								}
							}							
						}
					}
				}				
			}
		}

		if(ItemPickRules!=null && ItemPickRules!='' && ItemPickRules.length>0)
		{
			nlapiLogExecution('Debug', 'ItemPickRules length',ItemPickRules.length);
			nlapiLogExecution('Debug', 'uom log new',uom);
			var pfLocationResults = new Array();

			pfLocationResults = getPFLocationsForItem(pflocationsarr,whLocation,Item,uom);
			if(pfLocationResults!=null && pfLocationResults!='' && pfLocationResults.length>0)
			{
				nlapiLogExecution('Debug', 'pfLocationResults length',pfLocationResults.length);
			}

			for (var k=0; k<ItemPickRules.length; k++){

				var pickStrategyId = ItemPickRules[k][1];
				var pickMethod = ItemPickRules[k][2];
				var pickZone = ItemPickRules[k][3];
				var locnGroup = ItemPickRules[k][4];
				var binLocation = ItemPickRules[k][5];
				var allocationStrategy = ItemPickRules[k][18];
				var pickfaceLocationFlag = ItemPickRules[k][19];
				var clusterPickFlag = ItemPickRules[k][20];
				var maxOrders = ItemPickRules[k][21];
				var maxPicks = ItemPickRules[k][22];
				var cartonizationmethod =  ItemPickRules[k][24];
				var stagedetermination =  ItemPickRules[k][26];
				var isforwardpickmethod =  ItemPickRules[k][27];

				var tempPFLocationResults = new Array();

				if(pickfaceLocationFlag == 'T')
					tempPFLocationResults = pfLocationResults;
				else
					tempPFLocationResults = null;

				var locnGroupList = new Array();

				// Pick strategy details for the fulfillment order; Using index in the order list to identify the order
				var currentRow1 = [k, pickStrategyId, pickMethod, pickZone, locnGroup, binLocation, allocationStrategy,
				                   tempPFLocationResults, locnGroupList, clusterPickFlag, maxOrders, maxPicks,cartonizationmethod
				                   ,stagedetermination,isforwardpickmethod];

				allocConfigDtls.push(currentRow1);
			}
		}
	}

	nlapiLogExecution('Debug', 'Out of getListOfAllPickRules','');

	return allocConfigDtls;
}


function getPFLocationsForItem(pflocationsarr,whLocation,Item,uom)
{
	nlapiLogExecution('Debug', 'Into getPFLocationsForItem','');

	var pfLocations = new Array();

	if(pflocationsarr!=null && pflocationsarr!='' && pflocationsarr.length>0)
	{
		nlapiLogExecution('Debug', 'pflocationsarr length',pflocationsarr.length);

		for (var k2=0; k2<pflocationsarr.length; k2++){

			var pfwhlocation = pflocationsarr[k2].getValue('custrecord_pickface_location');
			var pfItem = pflocationsarr[k2].getValue('custrecord_pickfacesku');
			var pfUom = pflocationsarr[k2].getValue('custrecord_packfaceuom');
			var str = 'pfwhlocation. = ' + pfwhlocation + '<br>';
			str = str + 'whLocation. = ' + whLocation + '<br>';	
			str = str + 'pfItem. = ' + pfItem + '<br>';
			str = str + 'Item. = ' + Item + '<br>';
			str = str + 'pfUom. = ' + pfUom + '<br>';
			str = str + 'uom. = ' + uom + '<br>';
			str = str + 'pflocationsarr[k2]. = ' + pflocationsarr[k2].getId() + '<br>';



			nlapiLogExecution('Debug', 'PF Details', str);
			if((pfwhlocation == whLocation) && (pfItem == Item)
					&& (pfUom==null || pfUom=='' || pfUom==uom))
			{
				pfLocations.push(pflocationsarr[k2]);
			}

		}

	}

	nlapiLogExecution('Debug', 'Out of getPFLocationsForItem','');

	return pfLocations;
}


function getAvailQtyForReplenishment(inventorySearchResults, binLocation, allocatedInv,dowhlocation,
		fulfilmentItem,fulfillmentItemStatus){

	nlapiLogExecution('Debug','Into getAvailQtyForReplenishment');
	var binLocnInvDtls = new Array();
	var matchFound = false;

	nlapiLogExecution('Debug','inventorySearchResults',inventorySearchResults);
	if(inventorySearchResults != null && inventorySearchResults.length > 0){
		nlapiLogExecution('Debug','inventorySearchResults length',inventorySearchResults.length);
		for(var i = 0; i < inventorySearchResults.length; i++){
			if(!matchFound){
				invBinLocnId = inventorySearchResults[i].getValue('custrecord_ebiz_inv_binloc');
				var InvWHLocation = inventorySearchResults[i].getValue('custrecord_ebiz_inv_loc');
				var invItem=inventorySearchResults[i].getValue('custrecord_ebiz_inv_sku');
				var itemStatus = inventorySearchResults[i].getValue('custrecord_ebiz_inv_sku_status');

				var str = 'dowhlocation. = ' + dowhlocation + '<br>';
				str = str + 'InvWHLocation. = ' + InvWHLocation + '<br>';	
				str = str + 'invBinLocnId. = ' + invBinLocnId + '<br>';
				str = str + 'binLocation. = ' + binLocation + '<br>';
				str = str + 'fulfilmentItem. = ' + fulfilmentItem + '<br>';
				str = str + 'invItem. = ' + invItem + '<br>';
				str = str + 'itemStatus. = ' + itemStatus + '<br>';

				nlapiLogExecution('Debug', 'Inventory & FO Details', str);

				if((dowhlocation==InvWHLocation) && (parseInt(invBinLocnId) != parseInt(binLocation)) && (fulfilmentItem==invItem)
						&& (itemStatus==fulfillmentItemStatus)){

					var actualQty = inventorySearchResults[i].getValue('custrecord_ebiz_qoh');
					var inventoryAvailableQuantity = inventorySearchResults[i].getValue('custrecord_ebiz_avl_qty');
					var allocationQuantity = inventorySearchResults[i].getValue('custrecord_ebiz_alloc_qty');
					var packCode = inventorySearchResults[i].getValue('custrecord_ebiz_inv_packcode');					
					var whLocation = inventorySearchResults[i].getValue('custrecord_ebiz_inv_loc');
					var fromLP = inventorySearchResults[i].getValue('custrecord_ebiz_inv_lp');
					var inventoryItem=inventorySearchResults[i].getValue('custrecord_ebiz_inv_sku');
					var fromLot = inventorySearchResults[i].getText('custrecord_ebiz_inv_lot');

					var str = 'Invt QOH. = ' + actualQty + '<br>';
					str = str + 'Alloc Qty. = ' + allocationQuantity + '<br>';	
					str = str + 'Avail Qty. = ' + inventoryAvailableQuantity + '<br>';
					str = str + 'packCode. = ' + packCode + '<br>';
					str = str + 'itemStatus. = ' + itemStatus + '<br>';
					str = str + 'whLocation. = ' + whLocation + '<br>';
					str = str + 'fromLP. = ' + fromLP + '<br>';
					str = str + 'inventoryItem. = ' + inventoryItem + '<br>';
					str = str + 'fromLot. = ' + fromLot + '<br>';

					nlapiLogExecution('Debug', 'Inventory Details', str);

					var recordId = inventorySearchResults[i].getId();

					// Get allocation already done
					var alreadyAllocQty = getAlreadyAllocatedInv(allocatedInv, binLocation,fromLP,inventoryItem,fromLot);

					nlapiLogExecution('Debug', 'alreadyAllocQty', alreadyAllocQty);

					// Adjusting for allocations already done
					inventoryAvailableQuantity = parseInt(inventoryAvailableQuantity) - parseInt(alreadyAllocQty);

					nlapiLogExecution('Debug', 'inventoryAvailableQuantity', inventoryAvailableQuantity);

					var currentRow = [i, actualQty, inventoryAvailableQuantity, allocationQuantity, packCode,
					                  itemStatus, whLocation, fromLP, recordId,inventoryItem,fromLot];

					binLocnInvDtls.push(currentRow);

					//matchFound = true;
				}
			}
		}
	}

	nlapiLogExecution('Debug','Out of getAvailQtyForReplenishment');
	return binLocnInvDtls;
}



function replenRuleZoneLocationForAutoRepln(inventorySearchResults, replenItemArray, reportNo, stageLocation,whLocation,salesOrderNo,eBizWaveNo,taskpriority,expectedQuantity,pickfaceArr,ZoneAndLocationGrp){
	var returnval=false;
	var replnArray=new Array();
	for (var s = 0; s < replenItemArray.length; s++){
		var replenItem = replenItemArray[s][0];
		var replenQty = replenItemArray[s][1];
		var replenTaskCount = replenItemArray[s][9];
		var replenTempQty = replenItemArray[s][8];
		var actEndLocationId = replenItemArray[s][7];
		var pickfaceFixedLP = replenItemArray[s][10];
		var replenstatus=replenItemArray[s][11];
		var fulfillmentItemStatus=replenItemArray[s][12];

		var pfRoundQty=replenItemArray[s][13];

		var stageLPNo = 0;

		nlapiLogExecution('DEBUG','Item', replenItem);
		nlapiLogExecution('DEBUG','pickface FixedLP', pickfaceFixedLP);
		nlapiLogExecution('DEBUG','actEndLocationId',actEndLocationId);
		nlapiLogExecution('DEBUG','Task Count', replenTaskCount);
		nlapiLogExecution('DEBUG','Replen Qty', replenQty);
		nlapiLogExecution('DEBUG','Replen Temp Qty', replenTempQty);
		nlapiLogExecution('DEBUG','replenstatus', replenstatus);
		nlapiLogExecution('DEBUG','stageLocation', stageLocation);
		nlapiLogExecution('DEBUG','salesOrderNo', salesOrderNo);
		nlapiLogExecution('DEBUG','fulfillmentItemStatus', fulfillmentItemStatus);
		nlapiLogExecution('DEBUG','expectedQuantity', expectedQuantity);

		//var totalReplenRequired = (parseFloat(replenTaskCount) * parseFloat(replenQty)) + parseFloat(replenTempQty);
		var totalReplenRequired = parseFloat(replenTempQty);

//		if (replenTempQty >0){
//		replenTaskCount++;
//		}

		var stageLPs = getStageLPsAndUpdateLPRange(parseFloat(replenTaskCount),whLocation);

		var qtypertask = Math.floor(parseFloat(totalReplenRequired)/parseFloat(replenTaskCount));

		nlapiLogExecution('DEBUG','totalReplenRequired', totalReplenRequired);
		nlapiLogExecution('DEBUG','totalReplenRequired', replenTaskCount);
		nlapiLogExecution('DEBUG','totalReplenRequired', qtypertask);

		var totallocqty=0;

		if (inventorySearchResults != null) {
			nlapiLogExecution('DEBUG','inventorySearchResults.length', inventorySearchResults.length);
			for (var t = 0; t < inventorySearchResults.length; t++) {


				var item = inventorySearchResults[t].getValue('custrecord_ebiz_inv_sku');

				var vPickZone ="";

				var vItemStatus = inventorySearchResults[t].getValue('custrecord_ebiz_inv_sku_status');
				var actBatchText = "";
				if(inventorySearchResults[t].getValue('custrecord_ebiz_inv_lot') != null)
					actBatchText=inventorySearchResults[t].getText('custrecord_ebiz_inv_lot');	

				var RecordId = inventorySearchResults[t].getId();
				nlapiLogExecution('DEBUG','RecordId new', RecordId);
				var actBinLocationId = inventorySearchResults[t].getValue('custrecord_ebiz_inv_binloc');
				if (replenItem == item && actEndLocationId != actBinLocationId && fulfillmentItemStatus == vItemStatus){
//					nlapiLogExecution('DEBUG','Inside the item condition check',item);
					var actualQty = inventorySearchResults[t].getValue('custrecord_ebiz_qoh');
					var inventoryQty = inventorySearchResults[t].getValue('custrecord_ebiz_inv_qty');
					var allocatedQty = inventorySearchResults[t].getValue('custrecord_ebiz_alloc_qty');
					var lpNo = inventorySearchResults[t].getValue('custrecord_ebiz_inv_lp');
					var actBinLocationId = inventorySearchResults[t].getValue('custrecord_ebiz_inv_binloc');
					var actBinLocation = inventorySearchResults[t].getText('custrecord_ebiz_inv_binloc');
					var itemStatus = inventorySearchResults[t].getValue('custrecord_ebiz_inv_sku_status');
					var whLocation = inventorySearchResults[t].getValue('custrecord_ebiz_inv_loc');
					var vLocationGroup = inventorySearchResults[t].getValue('custrecord_outboundinvlocgroupid');
					nlapiLogExecution('DEBUG','vLocationGroup new', vLocationGroup);

//					nlapiLogExecution('DEBUG','WH Location',whLocation);

					/*
					 * While actualQty > replenQty and TaskCount > 0
					 * 		Create open task record
					 * 		update everything else
					 * 		reduce actual quantity and task count
					 * loop
					 */

					if(allocatedQty==null || allocatedQty=='')
						allocatedQty=0;
					if(actualQty==null || actualQty=='')
						actualQty=0;
					var Availqty=parseFloat(actualQty)-parseFloat(allocatedQty);

					nlapiLogExecution('DEBUG','ZoneAndLocationGrp', ZoneAndLocationGrp);

					if(ZoneAndLocationGrp !=null && ZoneAndLocationGrp !='' && ZoneAndLocationGrp.length>0)
					{
						for( var k=0 ;k<ZoneAndLocationGrp.length; k++)
						{
							nlapiLogExecution('DEBUG','ZoneAndLocationGrp', ZoneAndLocationGrp[k][0]);
							if(parseFloat(vLocationGroup) == parseFloat(ZoneAndLocationGrp[k][0]))
							{
								vPickZone =ZoneAndLocationGrp[k][1]
							}

						}

					}


					//if(parseFloat(totalReplenRequired) > 0 && parseFloat(Availqty) <= parseFloat(totalReplenRequired))
					//{
					//	returnval=true;
					if(parseFloat(Availqty) < parseFloat(replenQty) && parseFloat(totalReplenRequired) > 0){
						var tempAllocQty = getAllocationQty(Availqty, replenQty, totalReplenRequired);
						nlapiLogExecution('DEBUG','taskpriority from pick excep in inv gen func if', taskpriority);


						nlapiLogExecution('Debug','tempAllocQty 1 in if',tempAllocQty);

						var tempActwithRoundQty = Math.floor(parseFloat(tempAllocQty)/parseFloat(pfRoundQty));

						if(tempActwithRoundQty == null || tempActwithRoundQty == '' || isNaN(tempActwithRoundQty))
							tempActwithRoundQty=0;

						nlapiLogExecution('Debug','tempActwithRoundQty',tempActwithRoundQty);

						tempAllocQty = parseInt(pfRoundQty)*parseInt(tempActwithRoundQty);

						nlapiLogExecution('Debug','tempAllocQty 2 in if',tempAllocQty);
						if(tempAllocQty!=0)
						{

							createOpenTaskRecord(tempAllocQty, actBinLocationId, item, whLocation, reportNo, 
									pickfaceFixedLP, stageLPs[stageLPNo], actBatchText, stageLocation, 
									actEndLocationId, lpNo,RecordId,itemStatus,salesOrderNo,eBizWaveNo,taskpriority,'','',vPickZone);

							totalReplenRequired = parseFloat(totalReplenRequired) - parseFloat(tempAllocQty);

							// Updating the inventory with allocation quantity
							updateInventoryWithAllocationQuantity(RecordId, tempAllocQty, itemStatus);
//							nlapiLogExecution('DEBUG', 'Bulk Bin Location', actBinLocationId);
//							nlapiLogExecution('DEBUG', 'totalReplenRequired', totalReplenRequired);
//							nlapiLogExecution('DEBUG', 'StageLPNo', stageLPs[stageLPNo]);

							//stageLPNo++;

//							nlapiLogExecution('DEBUG', 'StageLPNo', stageLPs[stageLPNo]);

							totallocqty=parseFloat(totallocqty)+parseFloat(tempAllocQty);
							nlapiLogExecution('DEBUG','totallocqty/tempAllocQty', totallocqty+"/"+tempAllocQty);
//							if(parseFloat(totallocqty) >= parseFloat(qtypertask))
//							stageLPNo++;
							pickfaceArr.push(actBinLocationId);
							returnval=true;
						}

					} else {
						var tempActQty = Availqty;
						while(parseFloat(tempActQty) > 0 && parseFloat(totalReplenRequired) > 0){

							var tempAllocQty = getAllocationQty(tempActQty, replenQty, totalReplenRequired);
							nlapiLogExecution('DEBUG','taskpriority from pick excep in inv gen func else', taskpriority);
							createOpenTaskRecord(tempAllocQty, actBinLocationId, item, whLocation, reportNo, 
									pickfaceFixedLP, stageLPs[stageLPNo], actBatchText, stageLocation, 
									actEndLocationId, lpNo,RecordId,itemStatus,salesOrderNo,eBizWaveNo,taskpriority,'','',vPickZone);

							totalReplenRequired = parseFloat(totalReplenRequired) - parseFloat(tempAllocQty);
							tempActQty = parseFloat(tempActQty) - parseFloat(tempAllocQty);

							// Updating the inventory with allocation quantity
							updateInventoryWithAllocationQuantity(RecordId, tempAllocQty, itemStatus);

//							nlapiLogExecution('DEBUG', 'Bulk Bin Location', actBinLocationId);
//							nlapiLogExecution('DEBUG', 'totalReplenRequired', totalReplenRequired);
//							nlapiLogExecution('DEBUG', 'else: StageLPNo', stageLPs[stageLPNo]);

							totallocqty=parseFloat(totallocqty)+parseFloat(tempAllocQty);

							nlapiLogExecution('Debug','tempActQty 1',tempActQty);

							var tempActwithRoundQty = Math.floor(parseFloat(tempActQty)/parseFloat(pfRoundQty));

							if(tempActwithRoundQty == null || tempActwithRoundQty == '' || isNaN(tempActwithRoundQty))
								tempActwithRoundQty=0;

							nlapiLogExecution('Debug','tempActwithRoundQty',tempActwithRoundQty);

							tempActQty = parseInt(pfRoundQty)*parseInt(tempActwithRoundQty);

							nlapiLogExecution('Debug','tempActQty 2',tempActQty);

//							if(parseFloat(totallocqty) >= parseFloat(qtypertask))
//							stageLPNo++;
							pickfaceArr.push(actBinLocationId);
							returnval=true;
							nlapiLogExecution('DEBUG', 'else: StageLPNo', stageLPNo);
						}
					}
					nlapiLogExecution('DEBUG', 'StageLPNo', stageLPNo);
					//}
				}
			}
		}
	}
	if(returnval==false)
	{
		replnArray.push(-1);
		replnArray.push(0);
	}
	else
	{
		var remainingqty=0;
		nlapiLogExecution('DEBUG', 'expectedQuantity', expectedQuantity);
		nlapiLogExecution('DEBUG', 'totallocqty', totallocqty);
		replnArray.push(true);
		if(parseInt(expectedQuantity)>parseInt(totallocqty))
			remainingqty=parseInt(expectedQuantity)-parseInt(totallocqty);
		replnArray.push(remainingqty);
		if(parseInt(expectedQuantity)>parseInt(totallocqty))
			replnArray.push(totallocqty);
		else
			replnArray.push(expectedQuantity);
	}
	nlapiLogExecution('DEBUG', 'replnArray', replnArray);
	return replnArray;
}


function createshipmanifestRecord(vwaveno,vordno)
{

	try{
		nlapiLogExecution('ERROR','intocreateshipmanufest',vwaveno);
		nlapiLogExecution('ERROR','vwaveno',vwaveno);
		nlapiLogExecution('ERROR','vordno',vordno);
		var opentasks;

		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', vordno));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(vwaveno)+".0"));
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));//pick task
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9']));
		filters.push(new nlobjSearchFilter('custrecord_expe_qty', null, 'equalto', 1));
		filters.push(new nlobjSearchFilter('custrecord_container', null, 'anyof', ['54']));
		filters.push(new nlobjSearchFilter('mainline', "custrecord_ebiz_order_no", 'is', "T"));
		filters.push(new nlobjSearchFilter('formulanumeric', null, 'equalto', 0).setFormula("NVL({custrecord_sku.internalid},0)-NVL({custrecord_parent_sku_no.internalid},0)"));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
		columns[0].setSort();
		columns[1] = new nlobjSearchColumn('custrecord_comp_id');		
		columns[2] = new nlobjSearchColumn('custrecord_container');
		columns[3] = new nlobjSearchColumn('custrecord_total_weight');
		columns[4] = new nlobjSearchColumn('custrecord_sku');
		columns[5] = new nlobjSearchColumn('custrecord_uom_level');
		columns[6] = new nlobjSearchColumn('name');
		columns[7] = new nlobjSearchColumn('shipmethod','custrecord_ebiz_order_no');
		opentasks = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

		nlapiLogExecution('ERROR','vopentaskRec',opentasks);
		if(opentasks!=null && opentasks!='')
		{
			var vCarrierType="";
			var vcarrier="";

			var filterscarr = new Array();
			var columnscarr = new Array();

			filterscarr.push(new nlobjSearchFilter('custrecord_carrier_nsmethod', null, 'is',opentasks[0].getValue('shipmethod','custrecord_ebiz_order_no')));
			filterscarr.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

			columnscarr[0] = new nlobjSearchColumn('custrecord_carrier_type');	
			columnscarr[1] = new nlobjSearchColumn('custrecord_carrier_service_level');	
			columnscarr[2] = new nlobjSearchColumn('id');

			var searchresultscarr = nlapiSearchRecord('customrecord_ebiznet_carrier', null, filterscarr, columnscarr);
			if(searchresultscarr!=null && searchresultscarr!='' && searchresultscarr.length>0)
			{
				vCarrierType = searchresultscarr[0].getText('custrecord_carrier_type');
				vcarrier = searchresultscarr[0].getId();
			}
			nlapiLogExecution('ERROR','vCarrierType',vCarrierType);
			if(vCarrierType=="PC")
			{
				var vContainerLP=new Array();
				for ( var count = 0; opentasks!=null && count < opentasks.length; count++) 
				{
					var vConLp=opentasks[count].getValue('custrecord_container_lp_no');
					if(vConLp!=""&&vConLp!=null)
						vContainerLP[vContainerLP.length]=vConLp;
				}
				nlapiLogExecution('ERROR','vcontainerLP',vContainerLP);
				var vDistinctContainerLp=new Array();
//				if(vContainerLP!=null && vContainerLP.length>1)
//				{
//				vDistinctContainerLp=removeDuplicateElement(vContainerLP);
//				nlapiLogExecution('ERROR','vcontainerLP',vDistinctContainerLp.length);
//				}
//				else
//				{
//				vDistinctContainerLp=vContainerLP;
//				}
				//vDistinctContainerLp.sort();indexOf
				nlapiLogExecution('ERROR','vDistinctContainerLp',vDistinctContainerLp);
				if(vContainerLP!=null && vContainerLP.length>0 )
				{
					nlapiLogExecution('ERROR','vcontainerLP',vContainerLP);

					for ( var vcount = 0;  vcount < vContainerLP.length; vcount++) 
					{
						if(vDistinctContainerLp.indexOf(vContainerLP[vcount])==-1)
						{
							nlapiLogExecution('ERROR','vDistinctContainerLp',vContainerLP[vcount]);
							//var vContLpNo=vDistinctContainerLp[vcount];
							//nlapiLogExecution('ERROR','vContLpNo',vContLpNo);
							var opentaskdetails=new Array();
							for(j=0;j<opentasks.length;j++)
							{
								if(vContainerLP[vcount]==opentasks[j].getValue('custrecord_container_lp_no'))
								{
									opentaskdetails[opentaskdetails.length]=opentasks[j];
								}
							}

							nlapiLogExecution('ERROR','opentaskdetails',opentaskdetails);

							CreateShippingManifestRecordLL(vordno,vContainerLP[vcount],'','','','','',opentaskdetails,vwaveno);
							vDistinctContainerLp[vDistinctContainerLp.length]=vContainerLP[vcount];
						}
					}
				}
				nlapiLogExecution('ERROR','createshipmanifestRecord','END');
			}
		}
		nlapiLogExecution('ERROR','end','');
	}
	catch(exp)
	{
		//nlapiLogExecution('ERROR','getOpentaskRec',getOpentaskRec);
	}
}


function CreateShippingManifestRecordLL(vebizOrdNo,vContLpNo,vCarrierType,lineno,weight,packagecount,totalpackagecount,vShippingRule,vwaveno) {
	try {

		nlapiLogExecution('Debug', 'into CreateShippingManifestRecord','MainFunction');		
		nlapiLogExecution('Debug', 'Order #',vebizOrdNo);	
		nlapiLogExecution('Debug', 'Container LP #',vContLpNo);	
		nlapiLogExecution('Debug', 'Carrier Type',vCarrierType);
		nlapiLogExecution('Debug', 'lineno',lineno);
		nlapiLogExecution('Debug', 'weight',weight);

		if (vebizOrdNo != null && vebizOrdNo != "") 
		{
			if(IsContLpExist(vContLpNo)!='T')
			{
				var freightterms ="";
				var otherrefnum="";
				var servicelevelvalue='';

				var trantype = nlapiLookupField('transaction', vebizOrdNo, 'recordType');
				nlapiLogExecution('Debug', 'trantype', trantype);
				//Get the records in SalesOrder
				nlapiLogExecution('Debug', 'SalesOrderList','');
				var  searchresults;
				if(trantype=="salesorder")
				{
					searchresults =SalesOrderList(vebizOrdNo);
				}

				else if(trantype=="transferorder")
				{
					nlapiLogExecution('Debug', 'SalesOrderList','');

					var filters = new Array();
					filters[0] = new nlobjSearchFilter('Internalid', null, 'is', vebizOrdNo);
					filters[1] = new nlobjSearchFilter('mainline', null, 'is', 'T');
					var columns = new Array();
					columns[0] = new nlobjSearchColumn('tranid');
					columns[1] = new nlobjSearchColumn('shipcarrier');
					columns[2] = new nlobjSearchColumn('shipaddress1');
					columns[3] = new nlobjSearchColumn('shipaddress2');
					columns[4] = new nlobjSearchColumn('shipcity');
					columns[5] = new nlobjSearchColumn('shipstate');
					columns[6] = new nlobjSearchColumn('shipcountry');
					columns[7] = new nlobjSearchColumn('shipzip');
					columns[8] = new nlobjSearchColumn('shipmethod');
					columns[9] = new nlobjSearchColumn('shipaddressee');
					columns[10] = new nlobjSearchColumn('custbody_salesorder_carrier');
					columns[11] = new nlobjSearchColumn('transferlocation');
					columns[12] = new nlobjSearchColumn('entity');
					columns[13] = new nlobjSearchColumn('custbodyshipping_carrier');

					searchresults = nlapiSearchRecord('transferorder', null, filters, columns);
					nlapiLogExecution('Debug', 'transferorder',searchresults.length);
				}
				nlapiLogExecution('Debug', 'SalesOrderList',searchresults);
				//Get the records in customrecord_ebiznet_trn_opentask
				var opentaskordersearchresult=getOpenTaskDetailsLL(vebizOrdNo,vContLpNo);
				nlapiLogExecution('Debug', 'getOpenTaskDetails',opentaskordersearchresult);

				//Code added by Sravan to getch customer default shipping address

				var entity=searchresults[0].getValue('entity');
				nlapiLogExecution('Debug', 'getOpenTaskDetails entity111',entity);

				var entityrecord ;
				if(entity != "" && entity != null)
				{
					var fields = ['entityid'];
					var columns = nlapiLookupField('customer',entity,fields);
					var customerid = columns.entityid;

					try
					{
						entityrecord = nlapiLoadRecord('customer', entity);
					}
					catch(exp)
					{
						nlapiLogExecution('Debug', 'Exception in Loading Customer',exp);
						entityrecord='';
					}
				}
				nlapiLogExecution('Debug', 'getOpenTaskDetails entity112','entity');



				var ShipManifest = nlapiCreateRecord('customrecord_ship_manifest');
				ShipManifest.setFieldValue('custrecord_ship_orderno',searchresults[0].getValue('tranid'));
				ShipManifest.setFieldValue('custrecord_ship_carrier',searchresults[0].getValue('custbody_salesorder_carrier'));

				ShipManifest.setFieldValue('custrecord_ship_city',   searchresults[0].getValue('shipcity'));
				ShipManifest.setFieldValue('custrecord_ship_state',	 searchresults[0].getValue('shipstate'));

				ShipManifest.setFieldValue('custrecord_ship_custid',customerid);
				ShipManifest.setFieldValue('custrecord_ship_country',searchresults[0].getValue('shipcountry'));
				ShipManifest.setFieldValue('custrecord_ship_addr1',searchresults[0].getValue('shipaddress1'));
				ShipManifest.setFieldValue('custrecord_ship_waveno',vwaveno);

				//sales order specific code 
				var islinelevelship="F";
				if(trantype=="salesorder")
				{
					//var contactName=searchresults[0].getText('entity');
					var contactName=searchresults[0].getValue('shipattention');
					var entity=searchresults[0].getText('entity');
					if(contactName!=null && contactName!='')
						contactName=contactName.replace(","," ");

					if(entity!=null && entity!='')
						entity=entity.replace(","," ");


					ShipManifest.setFieldValue('custrecord_ship_ordertype',searchresults[0].getText('custbody_nswmssoordertype'));
					//freightterms=searchresults[0].getText('custbody_nswmsfreightterms');

					otherrefnum=searchresults[0].getValue('otherrefnum');			 

					ShipManifest.setFieldValue('custrecord_ship_phone',searchresults[0].getValue('custbody_customer_phone'));
					//ShipManifest.setFieldValue('custrecord_ship_zip',searchresults[0].getValue('shipzip'));
					var saturdaydelivery= searchresults[0].getValue('custbody_nswmssosaturdaydelivery');
					ShipManifest.setFieldValue('custrecord_ship_satflag',saturdaydelivery);
					var cashondelivery= searchresults[0].getValue('custbody_nswmscodflag');
					ShipManifest.setFieldValue('custrecord_ship_codflag',cashondelivery);
					ShipManifest.setFieldValue('custrecord_ship_email',searchresults[0].getValue('email'));



					var rec= nlapiLoadRecord('salesorder', vebizOrdNo);
					var zipvalue=rec.getFieldValue('shipzip');
					var servicelevelvalue=rec.getFieldText('shipmethod');
					var consignee=rec.getFieldValue('shipaddressee');
					var signaturerequired=rec.getFieldValue('custbody_nswmssignaturerequired');
					ShipManifest.setFieldValue('custrecord_ship_signature_req',signaturerequired);
					var shipcomplete=rec.getFieldValue('shipcomplete');
					var termscondition=rec.getFieldText('terms');
					//Start Case#: 20123475
					islinelevelship=rec.getFieldValue('ismultishipto');
					nlapiLogExecution('Debug', 'islinelevelship',islinelevelship);
					//End Case#: 20123475

					freightterms=rec.getFieldText('custbody_pacejet_freight_terms');
					ShipManifest.setFieldValue('custrecord_ship_paymethod',freightterms);

					if(contactName==''||contactName==null ||contactName=="")
					{
						contactName=consignee;
					}
					ShipManifest.setFieldValue('custrecord_ship_contactname',contactName);		

					var residentialflag='F';
					residentialflag=rec.getFieldValue('shipisresidential');
					ShipManifest.setFieldValue('custrecord_ship_residential_flag',residentialflag);
					nlapiLogExecution('Debug', 'lineno',lineno);

					if(lineno!=null && lineno!='')
					{
						var shipmethodLineLevel = rec.getLineItemText('item','shipmethod',lineno);


					}

					nlapiLogExecution('Debug', 'shipmethodLineLevel',shipmethodLineLevel);
					nlapiLogExecution('Debug', 'signaturerequired',shipcomplete);
					var shiptotal="0.00";
					if((shipcomplete=="T")&&(termscondition=="C.O.D."))
					{

						ShipManifest.setFieldValue('custrecord_ship_codflag','T');
						shiptotal=rec.getFieldValue('subtotal');
						ShipManifest.setFieldValue('custrecord_ship_codamount',shiptotal);
					}
					else
					{
						ShipManifest.setFieldValue('custrecord_ship_codflag','F');
						ShipManifest.setFieldValue('custrecord_ship_codamount',shiptotal);
					}
					nlapiLogExecution('Debug', 'signaturerequired',signaturerequired);
					nlapiLogExecution('Debug', 'zipvalue=',zipvalue);
					nlapiLogExecution('Debug', 'servicelevelvalue=',servicelevelvalue);
					nlapiLogExecution('Debug', 'Consignee=',consignee);

					if(consignee!="" || consignee!=null)
						ShipManifest.setFieldValue('custrecord_ship_consignee',consignee);
					else
						ShipManifest.setFieldValue('custrecord_ship_consignee',entity);


					ShipManifest.setFieldValue('custrecord_ship_servicelevel',servicelevelvalue);	

					if(servicelevelvalue==null || servicelevelvalue=='')
					{					
						ShipManifest.setFieldValue('custrecord_ship_servicelevel',shipmethodLineLevel);	

					}



					ShipManifest.setFieldValue('custrecord_ship_zip',zipvalue);
					//ShipManifest.setFieldValue('custrecord_ship_zip',custzip);
					var companyname= searchresults[0].getText('custbody_nswms_company');
					ShipManifest.setFieldValue('custrecord_ship_company',companyname);
					ShipManifest.setFieldValue('custrecord_ship_ref2',otherrefnum);
					//if(weight == null || weight == '' || parseFloat(weight) == 0)
					//	weight='0.01';
					ShipManifest.setFieldValue('custrecord_ship_actwght',parseFloat(weight).toFixed(5));
					var tolocation = rec.getFieldValue('location');

					if(tolocation!=null&& tolocation!="")
					{
						try
						{
							var record = nlapiLoadRecord('location', tolocation);

							var shipfromaddress1=record.getFieldValue('addr1');
							var shipfromaddress2=record.getFieldValue('addr2');
							var shipfromcity=record.getFieldValue('city');
							var shipfromstate=record.getFieldValue('state');
							var shipfromzipcode =record.getFieldValue('zip');
							var shipfromcompanyname=record.getFieldValue('addressee');
							var shipfromphone=record.getFieldValue('addrphone');
							var shipfromcountry =record.getFieldValue('country');                                        
							ShipManifest.setFieldValue('custrecord_ship_from_city',shipfromcity);
							ShipManifest.setFieldValue('custrecord_ship_from_state',shipfromstate);
							ShipManifest.setFieldValue('custrecord_ship_country',shipfromcountry);
							ShipManifest.setFieldValue('custrecord_ship_from_addr1',shipfromaddress1);
							ShipManifest.setFieldValue('custrecord_ship_from_zip',shipfromzipcode);
							ShipManifest.setFieldValue('custrecord_ship_from_addr2',shipfromaddress2);
							ShipManifest.setFieldValue('custrecord_ship_from_phone',shipfromphone);
							ShipManifest.setFieldValue('custrecord_ship_from_company',shipfromcompanyname);
						}
						catch(exp)
						{
							nlapiLogExecution('Debug', 'exp', exp);
						}
					}


				}

				var address1=searchresults[0].getValue('shipaddress1');
				var address2=searchresults[0].getValue('shipaddress2');
				//var address1=custaddr1;
				//var address2=custaddr2;


				var zip=searchresults[0].getValue('shipzip');
				var servicelevel=searchresults[0].getText('shipmethod');

				/*nlapiLogExecution('Debug', 'Zip',zip);
				nlapiLogExecution('Debug', 'Service Level',servicelevel);
				nlapiLogExecution('Debug', 'Zip',zip);
				nlapiLogExecution('Debug', 'City',searchresults[0].getValue('shipcity'));
				nlapiLogExecution('Debug', 'State',searchresults[0].getValue('shipstate'));
				nlapiLogExecution('Debug', 'Zip=',searchresults[0].getValue('shipzip'));
				nlapiLogExecution('Debug', 'Service Level Value',searchresults[0].getText('shipmethod'));
				 */


				if(address1!=null && address1!='')
					address1=address1.replace(","," ");


				if(address2!=null && address2!='')
					address2=address2.replace(","," ");


				ShipManifest.setFieldValue('custrecord_ship_order',vebizOrdNo);
				ShipManifest.setFieldValue('custrecord_ship_custom5',"S");	
				ShipManifest.setFieldValue('custrecord_ship_void',"N");

				ShipManifest.setFieldValue('custrecord_ship_contactname',contactName);					
				ShipManifest.setFieldValue('custrecord_ship_ordertype',searchresults[0].getText('custbody_nswmssoordertype'));
				//freightterms=searchresults[0].getText('custbody_nswmsfreightterms');
				otherrefnum=searchresults[0].getValue('otherrefnum');			 
				ShipManifest.setFieldValue('custrecord_ship_paymethod',freightterms);

				ShipManifest.setFieldValue('custrecord_ship_addr1',address1);
				ShipManifest.setFieldValue('custrecord_ship_addr2',address2);

				//ShipManifest.setFieldValue('custrecord_ship_servicelevel',searchresults[0].getText('custbody_nswmssoservicelevel'));
				//ShipManifest.setFieldValue('custrecord_ship_servicelevel',searchresults[0].getText('shipmethod'));
				//shipmethod 
				//This code is customized code for factory mation, so we have commented that code as part of standard bundle, Commented by Ganesh on 1st Mar 2013
				/*if(freightterms!="SENDER")
				{
					if (entityrecord != null && entityrecord != '')
					{

						var thirdpartyacct=entityrecord.getFieldValue('thirdpartyacct');

						if((thirdpartyacct!=null)&&(thirdpartyacct!=''))
						{
							nlapiLogExecution('Debug', 'thirdpartyacct',thirdpartyacct);
							ShipManifest.setFieldValue('custrecord_ship_account',thirdpartyacct);

						}
					}
				}
				nlapiLogExecution('Debug', 'freightterms', freightterms);
				var freightvalue="";
				if(freightterms=="SENDER")
				{
					freightvalue="SHP";
				}
				if(freightterms=="RECEIVER")
				{
					freightvalue="REC";
				}
				if(freightterms=="3RDPARTY")
				{
					freightvalue="TP";
				}
				nlapiLogExecution('Debug', 'freightvalue', freightvalue);
				ShipManifest.setFieldValue('custrecord_ship_paymethod',freightvalue);*/
				//Upto here
				//if(lineno!=null && lineno!='')

				//Start Case#: 20123475
				if(islinelevelship=='T')
				{
					if (entityrecord != null && entityrecord != '')
					{
						var custaddr1 = entityrecord.getFieldValue('shipaddr1');
						var custaddr2 = entityrecord.getFieldValue('shipaddr2');
						var custaddresee = entityrecord.getFieldValue('shipaddressee');
						var custcity = entityrecord.getFieldValue('shipcity');
						var custstate = entityrecord.getFieldValue('shipstate');
						var custzip = entityrecord.getFieldValue('shipzip');
						var custcountry=entityrecord.getFieldValue('shipcountry');

						ShipManifest.setFieldValue('custrecord_ship_country', custcountry);
						ShipManifest.setFieldValue('custrecord_ship_city',  custcity);
						ShipManifest.setFieldValue('custrecord_ship_state',	custstate);
						ShipManifest.setFieldValue('custrecord_ship_addr1',	custaddr1);
						ShipManifest.setFieldValue('custrecord_ship_addr2',	custaddr2);
						ShipManifest.setFieldValue('custrecord_ship_zip',	custzip);
						ShipManifest.setFieldValue('custrecord_ship_consignee',	custaddresee);
					}
				}
				//nlapiLogExecution('Debug', 'hiii', 'hiii');
				nlapiLogExecution('Debug', 'vCarrierType', vCarrierType);
				/*if(vCarrierType!=null && vCarrierType!='')
				{
					var servicelevelList=GetSerViceLevel(vCarrierType);
					if((servicelevelList!=null)&&(servicelevelList !='')&&(servicelevelList.length>0))
					{
						vserlevel=servicelevelList[0].getValue('custrecord_carrier_service_level'); 
						nlapiLogExecution('Debug', 'vserlevel', vserlevel);
						ShipManifest.setFieldValue('custrecord_ship_servicelevel',vserlevel);
					}
				}*/

				var servicelevellbyshipmethod="";
				var ServiceLevelId="";
				ServiceLevelId=searchresults[0].getValue('shipmethod');
				nlapiLogExecution('Debug', 'ServiceLevelId',ServiceLevelId);
				if(ServiceLevelId!=null && ServiceLevelId!="")
				{
					servicelevellbyshipmethod=GetSerViceLevelByShipmethod(ServiceLevelId);
				}
				else
				{

					var foorder= opentaskordersearchresult[0].getValue('name');
					nlapiLogExecution('Debug', 'linelevelFoOrder',foorder);
					var site=opentaskordersearchresult[0].getValue('custrecord_wms_location');
					nlapiLogExecution('Debug', 'linelevelsite',site);
					var ServiceLevelId=getlinelevelshipmethod(foorder,site);
					servicelevellbyshipmethod=GetSerViceLevelByShipmethod(ServiceLevelId);
				}
				//End Case#:20123475
				if((servicelevellbyshipmethod!=null)&&(servicelevellbyshipmethod !='')&&(servicelevellbyshipmethod.length>0))
				{
					nlapiLogExecution('Debug', 'servicelevellbyshipmethod', 'servicelevellbyshipmethod');

					var shipserviceLevel=servicelevellbyshipmethod[0].getValue('custrecord_carrier_service_level'); 
					var wmscarriertype=servicelevellbyshipmethod[0].getValue('custrecord_carrier_id'); 
					nlapiLogExecution('Debug', 'shipmethodshipserviceLevel',shipserviceLevel);
					nlapiLogExecution('Debug', 'shipmethodshipserviceLevel',wmscarriertype);
					ShipManifest.setFieldValue('custrecord_ship_servicelevel',shipserviceLevel);
					ShipManifest.setFieldValue('custrecord_ship_carrier',wmscarriertype);

				}


				if(trantype=="transferorder")
				{
					nlapiLogExecution('Debug', 'tolocationtstt', trantype);
					var tolocation = searchresults[0].getValue('transferlocation');
					nlapiLogExecution('Debug', 'tolocation', tolocation);

					var record = nlapiLoadRecord('location', tolocation);

					var shipfromaddress1=record.getFieldValue('addr1');
					var shipfromaddress2=record.getFieldValue('addr2');
					var shipfromcity=record.getFieldValue('city');
					var shipfromstate=record.getFieldValue('state');
					var shipfromzipcode =record.getFieldValue('zip');
					var shipfromcompanyname=record.getFieldValue('addressee');
					var shipfromphone=record.getFieldValue('addrphone');
					var shipfromcountry =record.getFieldValue('country');
					var prefixshipmethod=record.getFieldValue('custrecord_preferred_ship_method');
					ShipManifest.setFieldValue('custrecord_ship_carrier',prefixshipmethod);
					ShipManifest.setFieldValue('custrecord_ship_city',shipfromcity);
					ShipManifest.setFieldValue('custrecord_ship_state',shipfromstate);
					ShipManifest.setFieldValue('custrecord_ship_country',shipfromcountry);
					ShipManifest.setFieldValue('custrecord_ship_addr1',shipfromaddress1);
					ShipManifest.setFieldValue('custrecord_ship_zip',shipfromzipcode);
					ShipManifest.setFieldValue('custrecord_ship_addr2',shipfromaddress2);
					ShipManifest.setFieldValue('custrecord_ship_phone',shipfromphone);
					ShipManifest.setFieldValue('custrecord_ship_custid',shipfromcompanyname);
					ShipManifest.setFieldValue('custrecord_ship_contactname',shipfromcompanyname);

				}
				ShipManifest.setFieldValue('custrecord_ship_trackno',"-1");
				ShipManifest.setFieldValue('custrecord_ship_masttrackno',"-1");

				if (opentaskordersearchresult != null && opentaskordersearchresult != "")
				{
					nlapiLogExecution('Debug', 'inside opentask search results', opentaskordersearchresult);

					var oldcontainer="";
					for (l = 0; l < opentaskordersearchresult.length; l++) 
					{ 
						nlapiLogExecution('Debug', 'inside opentask', containerid);

						var custlenght="";	
						var custheight="";
						var custwidht="";

						var sku="";
						var ebizskuno="";
						var uomlevel="";
						var shiplpno="";


						var containerlpno = opentaskordersearchresult[l].getValue('custrecord_container_lp_no');
						var shiplpno=opentaskordersearchresult[l].getValue('custrecord_ship_lp_no');
						sku = opentaskordersearchresult[l].getText('custrecord_sku');
						ebizskuno = opentaskordersearchresult[l].getValue('custrecord_sku');
						uomlevel = opentaskordersearchresult[l].getValue('custrecord_uom_level');					
						var name= opentaskordersearchresult[l].getValue('name');	
						var site=opentaskordersearchresult[l].getValue('custrecord_wms_location');
						var pickseq=opentaskordersearchresult[l].getValue('custrecord_startingpickseqno','custrecord_actbeginloc');
						nlapiLogExecution('Error', 'creasteshipmanifestsite', site);
						ShipManifest.setFieldValue('custrecord_ship_location',site);
						ShipManifest.setFieldValue('custrecord_ebiz_pick_seq',pickseq);

						if(oldcontainer!=containerlpno){
							ShipManifest.setFieldValue('custrecord_ship_ref3',name);
							if(vShippingRule=='BuildShip')
							{
								ShipManifest.setFieldValue('custrecord_ship_contlp',shiplpno);
								ShipManifest.setFieldValue('custrecord_ship_ref5',shiplpno);
							}
							else
							{
								ShipManifest.setFieldValue('custrecord_ship_contlp',containerlpno);
								ShipManifest.setFieldValue('custrecord_ship_ref5',containerlpno);
							}

							var containerid= opentaskordersearchresult[l].getValue('custrecord_container');
							var containername= opentaskordersearchresult[l].getText('custrecord_container');
							nlapiLogExecution('Debug', 'container id', containerid);
							nlapiLogExecution('Debug', 'container name', containername);
							if(containerid!="")
							{
								if(containername!="SHIPASIS")
								{
									//Lenght, Height,Width fields  in customrecord_ebiznet_container
									var containersearchresults = getContainerDims(containerid);
									if(containersearchresults != null)
									{
										custlenght=containersearchresults [0].getValue('custrecord_length');
										custwidht=containersearchresults [0].getValue('custrecord_widthcontainer');
										custheight=containersearchresults [0].getValue('custrecord_heightcontainer');								
										ShipManifest.setFieldValue('custrecord_ship_length',parseFloat(custlenght).toFixed(5));	
										ShipManifest.setFieldValue('custrecord_ship_width',parseFloat(custwidht).toFixed(5));
										ShipManifest.setFieldValue('custrecord_ship_height',parseFloat(custheight).toFixed(5));
										ShipManifest.setFieldValue('custrecord_ship_ref2',otherrefnum);
										ShipManifest.setFieldValue('custrecord_ship_pkgtype',containername);
									} 
								}
								else
								{
									var containersearchresults = getSKUDims(ebizskuno,uomlevel);
									if(containersearchresults != null)
									{
										custlenght=containersearchresults [0].getValue('custrecord_ebizlength');
										custwidht=containersearchresults [0].getValue('custrecord_ebizwidth');
										custheight=containersearchresults [0].getValue('custrecord_ebizheight');								
										ShipManifest.setFieldValue('custrecord_ship_length',parseFloat(custlenght).toFixed(5));	
										ShipManifest.setFieldValue('custrecord_ship_width',parseFloat(custwidht).toFixed(5));
										ShipManifest.setFieldValue('custrecord_ship_height',parseFloat(custheight).toFixed(5));
										ShipManifest.setFieldValue('custrecord_ship_ref2',otherrefnum);
										ShipManifest.setFieldValue('custrecord_ship_pkgtype',containername);
										ShipManifest.setFieldValue('custrecord_ship_ref1',sku);
									} 
								}

							}	

							if (containerlpno != null && containerlpno != "") {
								var PackageWeight = getTotalWeight(containerlpno);
								if(PackageWeight == null || PackageWeight == '' || parseFloat(PackageWeight) == 0)
									PackageWeight='0.0001';
								ShipManifest.setFieldValue('custrecord_ship_pkgwght',parseFloat(PackageWeight).toFixed(5));

								if(parseInt(PackageWeight)>70)
									ShipManifest.setFieldValue('custrecord_ship_servicelevel',"FedEx Ground");
								var lpmastersearchrecord =GetTotalPackageValues(containerlpno);
								var pkgno;
								var pkgcount;
								if(lpmastersearchrecord != null && lpmastersearchrecord != '')
								{	
									pkgno=lpmastersearchrecord[0].getValue('custrecord_ebiz_lpmaster_pkgno');
									pkgcount=lpmastersearchrecord[0].getValue('custrecord_ebiz_lpmaster_pkgcount');
								}
								nlapiLogExecution('Debug', 'lpmasterpkgno', pkgno);
								nlapiLogExecution('Debug', 'lpmasterpkgcount',pkgcount);

//								ShipManifest.setFieldValue('custrecord_ship_pkgno',parseFloat(pkgno.toString()));
//								ShipManifest.setFieldValue('custrecord_ship_pkgcount',parseFloat(pkgcount.toString()));

								if(packagecount!=null && packagecount!='' )
									ShipManifest.setFieldValue('custrecord_ship_pkgno',parseFloat(packagecount.toString()));
								if(totalpackagecount!=null && totalpackagecount!='' )
									ShipManifest.setFieldValue('custrecord_ship_pkgcount',parseFloat(totalpackagecount.toString()));

							}

							oldcontainer = containerlpno;

							//nlapiSubmitRecord(ShipManifest, false, true);	
							//nlapiLogExecution('Debug', 'unexpected error', 'I am success1');

						}					
					}
				}
				else
				{
					nlapiLogExecution('Debug', 'unexpected error', 'I am success2');
				}
				nlapiSubmitRecord(ShipManifest, false, true);
				try
				{
					nlapiLogExecution('Debug', 'CommodityInternationalShipment', 'CommodityInternationalShipment');
					CommodityInternationalShipment(vebizOrdNo,vContLpNo,trantype);
				}
				catch(exp)
				{

					nlapiLogExecution('Debug', 'CommodityshipmentExp',exp);
				}
			}
		}	
	}
	catch (e) {

		InsertExceptionLog('General Functions',2, 'Create Shipping Manifest', e, vebizOrdNo, vContLpNo,vCarrierType,'','', nlapiGetUser());
		if (e instanceof nlobjError)
			nlapiLogExecution('Debug', 'system error', e.getCode() + '\n'
					+ e.getDetails());
		//lapiLogExecution('Debug','SYS ERROR','Hd')
		else
			nlapiLogExecution('Debug', 'unexpected error', e.toString());
		nlapiLogExecution('Debug', 'unexpected error', 'I am unsuccess3');
	}

	nlapiLogExecution('Debug', 'Out of CreateShippingManifestRecord','');		
}

function getOpenTaskDetailsLL(vebizOrdNo,vContLpNo)
{
	nlapiLogExecution('Debug', 'General Functions', 'In to getOpenTaskDetails');
	var filter = new Array();
	var opentaskordersearchresult = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'is', vebizOrdNo));
	filter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9']));//Status Flag - Outbound Pack Complete)		
	filter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK
	filter.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'is', vContLpNo));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_comp_id');
	columns[1] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[2] = new nlobjSearchColumn('custrecord_container');
	columns[3] = new nlobjSearchColumn('custrecord_total_weight');
	columns[4] = new nlobjSearchColumn('custrecord_sku');
	columns[5] = new nlobjSearchColumn('custrecord_uom_level');
	columns[6] = new nlobjSearchColumn('name');
	columns[7] = new nlobjSearchColumn('custrecord_ship_lp_no');
	columns[8] = new nlobjSearchColumn('custrecord_wms_location');
	columns[9] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
	columns[1].setSort();

	opentaskordersearchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filter, columns);
	return  opentaskordersearchresult;
}


function IsMemberItemInactive(kititemsarr)
{
	try
	{
		nlapiLogExecution("ERROR","IsMemberItemInactive",kititemsarr);
		var Iteminactive=null;
		var filter=new Array();
		filter[0]=new nlobjSearchFilter("internalid",null,"anyof",kititemsarr);
		filter[1]=new nlobjSearchFilter("isinactive",null,"is","T");

		var column=new Array();
		column[0]=new nlobjSearchColumn("itemid");
		var searchres=nlapiSearchRecord("item",null,filter,column);
		if(searchres!=null&&searchres!="")
		{
			Iteminactive=searchres[0].getValue("itemid");
		}
		nlapiLogExecution("ERROR","Iteminactive",Iteminactive);
		return Iteminactive;
	}
	catch(exp)
	{
		nlapiLogExecution("ERROR","EXCEPTION IsMemberItemInactive",exp);
	}
}

function getSOLineCount(vsointrid)
{
	var solinecount=0;
	nlapiLogExecution("DEBUG","Into getSOLineCount",vsointrid);

	var filters = new Array();  
	var columns = new Array();  

	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));	
	filters.push(new nlobjSearchFilter('shipping', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('taxline', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('quantity', null, 'isnotempty'));

	if(parseInt(vsointrid)>0)
	{
		filters.push(new nlobjSearchFilter('internalidnumber', null, 'equalto', vsointrid));
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');	
	columns[1] = new nlobjSearchColumn('line');

	var salesOrderLineDetails = new nlapiSearchRecord('salesorder', null, filters, columns);
	if(salesOrderLineDetails!=null && salesOrderLineDetails!='')
	{
		solinecount=salesOrderLineDetails.length;
	}


	nlapiLogExecution("DEBUG","Out of getSOLineCount",solinecount);
	return solinecount;
}




function SetPrintFlag(waveno,fullfillment)
{
	nlapiLogExecution('ERROR','Into SetPrintFlag',waveno);
	var filter=new Array();

	if(waveno!=null && waveno!="")
		filter.push(new nlobjSearchFilter('custrecord_ebiz_wave','null','equalto',parseFloat(waveno)));

	/*if(fullfillment!=null&&fullfillment!="")
		filter.push(new nlobjSearchFilter('custrecord_lineord','null','is',fullfillment.toString()));*/

	var column=new Array();
	column[0]=new nlobjSearchColumn('custrecord_printflag');
	column[1]=new nlobjSearchColumn('custrecord_print_count');

	var searchrecord=nlapiSearchRecord('customrecord_ebiznet_ordline',null,filter,column);
	if(searchrecord!=null)
	{
		for ( var count = 0; count < searchrecord.length; count++) {
			var intId=searchrecord[count].getId();
			var flagcount=searchrecord[count].getValue('custrecord_print_count');
			var Newflagcount;
			if(flagcount==null || flagcount=="")
				Newflagcount=1;
			else
				Newflagcount=parseFloat(flagcount)+1;

			var fieldNames = new Array(); 
			fieldNames.push('custrecord_printflag');  
			fieldNames.push('custrecord_print_count');
			fieldNames.push('custrecord_ebiz_pr_dateprinted');

			var newValues = new Array(); 
			newValues.push('T');
			newValues.push(Newflagcount);
			newValues.push(DateStamp());			

			nlapiSubmitField('customrecord_ebiznet_ordline', intId, fieldNames, newValues);
		}
	}
	nlapiLogExecution('ERROR','Out of SetPrintFlag',waveno);
}
function calcTime(offset) {

	//var vtime=new Date();

	//nlapiLogExecution('ERROR', 'TimeOffset', vtime.getTimezoneOffset());

	// create Date object for current location
	d = new Date();

	// convert to msec
	// add local time zone offset 
	// get UTC time in msec
	utc = d.getTime() + (d.getTimezoneOffset() * 60000);

	// create new Date object for different city
	// using supplied offset
	nd = new Date(utc + (3600000*offset));

	// return time as a string
	//return "The local time in " + city + " is " + nd.toLocaleString();

	var stringDt=((parseFloat(nd.getMonth()) + 1) + '/' + (parseFloat(nd.getDate())) + '/' + nd.getFullYear());

	var timestamp;
	var a_p = "";

	//Getting time in hh:mm tt format.
	var curr_hour = nd.getHours();
	var curr_min = nd.getMinutes();

	// determining the am/pm indicator
	if (curr_hour < 12)
		a_p = "am";
	else
		a_p = "pm";

	// finalizing hours depending on 24hr clock
	if (curr_hour == 0)
		curr_hour = 12;
	else if(curr_hour > 12)
		curr_hour -= 12;

	if (curr_min.length == 1)
		curr_min = "0" + curr_min;

	//Adding fields to update time zones.
	timestamp = curr_hour + ":" + curr_min + " " + a_p;

	return stringDt + " " + timestamp;


}
function vremoveDuplicateElement(arr)
{
	var dups = {}; 
	return arr.filter(
			function(el) { 
				var hash = el.valueOf(); 
				var isDup = dups[hash]; 
				dups[hash] = true; 
				return !isDup; 
			}
	); 
}
function pickreportscheduler(getwaveNo)
{
	var context = nlapiGetContext();
	//var form = nlapiCreateForm('Pick Report');
	var vQbWave,vQbfullfillmentNo ="";
	var filter = new Array();
	if(getwaveNo!=null && getwaveNo!="")
		filter.push(new nlobjSearchFilter('custrecord_ebiz_wave','null','equalto',parseFloat(getwaveNo)));


	var column=new Array();
	column[0]=new nlobjSearchColumn('custrecord_printflag');
	column[1]=new nlobjSearchColumn('custrecord_print_count');
	column[2]=new nlobjSearchColumn('custrecord_lineord');
	column[3]=new nlobjSearchColumn('custrecord_ebiz_wave');
	column[4]=new nlobjSearchColumn('custrecord_shipment_no');
	var searchrecord=nlapiSearchRecord('customrecord_ebiznet_ordline',null,filter,column);
	var appendorder="";
	var salesorder="";
	var wavenumber="";
	var shipmentno="";
	if(searchrecord!=null)
	{
		for ( var count = 0; count < searchrecord.length; count++) {
			var intId=searchrecord[count].getId();
			var flagcount=searchrecord[count].getValue('custrecord_print_count');
			var fullflmentorder=searchrecord[count].getValue('custrecord_lineord');
			wavenumber=searchrecord[count].getValue('custrecord_ebiz_wave');
			shipmentno=searchrecord[count].getValue('custrecord_shipment_no');
			if(salesorder!=fullflmentorder)
			{
				salesorder=fullflmentorder;
				appendorder +=fullflmentorder+",";
			}
			var Newflagcount;
			if(flagcount==null || flagcount=="")
				Newflagcount=1;
			else
				Newflagcount=parseFloat(flagcount)+1;

			nlapiSubmitField('customrecord_ebiznet_ordline', intId, 'custrecord_printflag','T');
			nlapiSubmitField('customrecord_ebiznet_ordline', intId, 'custrecord_print_count',Newflagcount);
			nlapiSubmitField('customrecord_ebiznet_ordline', intId, 'custrecord_ebiz_pr_dateprinted',DateStamp());



		}
	}


	var getFullfillmentNo = appendorder.substring(0, appendorder .length-1);	


	//var getButtonId=request.getParameter('id');
	SetPrintFlag(getwaveNo,getFullfillmentNo);
	var filters = new Array();

//	9-STATUS.OUTBOUND.PICK_GENERATED ,26-STATUS.OUTBOUND.FAILED
	//filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','26']));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9']));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));

	if(getwaveNo!=null && getwaveNo!="")
	{
		nlapiLogExecution('ERROR','getwaveNo',getwaveNo);
		filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(getwaveNo)));
	} 


	/*if(getFullfillmentNo!= null && getFullfillmentNo!= "")
	{
		nlapiLogExecution('ERROR','getFullfillmentNo',getFullfillmentNo);
		filters.push(new nlobjSearchFilter('name', null, 'is', getFullfillmentNo));
	}*/

	vQbWave = getwaveNo;
	vQbfullfillmentNo = getFullfillmentNo;

	var columns = new Array();

	columns[0] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[1] = new nlobjSearchColumn('custrecord_skiptask');
	columns[2] = new nlobjSearchColumn('custrecord_line_no');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[4] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
	columns[5] = new nlobjSearchColumn('custrecord_bin_locgroup_seq');		
	columns[6] = new nlobjSearchColumn('custrecord_sku');
	columns[7] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[8] = new nlobjSearchColumn('custrecord_tasktype');
	columns[9] = new nlobjSearchColumn('custrecord_lpno');
	columns[10] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[11] = new nlobjSearchColumn('custrecord_sku_status');
	columns[12] = new nlobjSearchColumn('custrecord_packcode');
	columns[13] = new nlobjSearchColumn('name');
	columns[14] = new nlobjSearchColumn('custrecord_ebiz_clus_no');
	columns[15] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[16] = new nlobjSearchColumn('custrecord_container');
	columns[17] = new nlobjSearchColumn('description','custrecord_sku');
	columns[18] = new nlobjSearchColumn('upccode','custrecord_sku');
	columns[19] = new nlobjSearchColumn('custrecord_batch_no');
	columns[20] = new nlobjSearchColumn('custrecord_total_weight');
	columns[21] = new nlobjSearchColumn('custrecord_ebizwmscarrier');
	columns[22] = new nlobjSearchColumn('custrecord_ebiz_zoneid');
	columns[23] = new nlobjSearchColumn('custrecord_expirydate');

	//columns[0].setSort();
	//columns[1].setSort();
	//columns[4].setSort();
	columns[4].setSort();
	columns[5].setSort();
	//columns[6].setSort();
	//columns[7].setSort(true);

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	var itemsarr = new Array();
	var itemdimsarr = new Array();

	if(searchresults!=null && searchresults.length > 0)
	{
		for (var i1 = 0; i1 < searchresults.length; i1++){
			var searchresult = searchresults[i1];
			itemsarr.push(searchresult.getValue('custrecord_ebiz_sku_no'));
		}
	}

	itemsarr = removeDuplicateElement(itemsarr);
	nlapiLogExecution('ERROR', 'itemsarr',itemsarr);

	nlapiLogExecution('ERROR', 'Time Stamp Before calling  for all items',TimeStampinSec());
	itemdimsarr = getItemDimensions(itemsarr,-1);
	nlapiLogExecution('ERROR', 'Time Stamp After calling  for all items',TimeStampinSec());

	if(searchresults!=null&&searchresults!=''&&searchresults.length>0)
	{
		var totalwt=0;

		var SoIds=new Array();
		for ( var intg = 0; intg < searchresults.length; intg++)
		{
			SoIds[intg]=searchresults[intg].getValue('custrecord_ebiz_order_no');
		}


		var distinctSoIds = removeDuplicateElement(SoIds);

		var vline, vitem, vqty, vTaskType,vlotbatch, vmainline, vrecid, vlocation, vLpno, vSKU, vlocationid, vinvrefno, vskustatus, vpackcode, vdono,vcontlp,vcontsize,vclusno,vweight,vcaseqty,vnoofcases;
		var upccode="";
		var itemdesc="";
		var replaceChar =/\$|,|@|&|#|~|`|\%|\*|\^|\&|\+|\=|\-|\_|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|\./g;

			//date
			var sysdate=DateStamp();
		var systime=TimeStamp();
		var Timez=calcTime('-5.00');
		var datetime= new Date();
		datetime=datetime.toLocaleTimeString() ;
		var finalimageurl = '';

		var url;
		/*var ctx = nlapiGetContext();
		if (ctx.getEnvironment() == 'PRODUCTION') 
		{
			url = 'https://system.netsuite.com';			
		}
		else if (ctx.getEnvironment() == 'SANDBOX') 
		{
			url = 'https://system.sandbox.netsuite.com';				
		}*/

		/*var filefound = nlapiLoadFile('Images/LOGOCOMP.jpg'); 
		if (filefound) 
		{ 
			nlapiLogExecution('ERROR', 'Event', 'file;'+filefound.getId()); 
			var imageurl = filefound.getURL();
			//finalimageurl = url + imageurl;//+';';
			finalimageurl = imageurl;//+';';
			finalimageurl=finalimageurl.replace(/&/g,"&amp;");

		} 
		else 
		{
			nlapiLogExecution('ERROR', 'Event', 'No file;');
		}*/

		for ( var count = 0; count < distinctSoIds.length; count++)
		{
			var CartonArray=new Array();
			var totalwt=0;var pageno=0;
			var vtotalcube=0;
			var trantype = nlapiLookupField('transaction', SoIds, 'recordType');
			var salesorder = nlapiLoadRecord(trantype, distinctSoIds[count]);
			var	address = salesorder.getFieldValue('shipaddressee');
			nlapiLogExecution('ERROR', 'salesorder',salesorder);
			var ismultilineship=salesorder.getFieldValue('ismultishipto');
			if(searchresults!=null && searchresults!='')
			{
				var vlineno=searchresults[0].getValue('custrecord_line_no');

			}
			var shiptovalue=salesorder.getLineItemValue('item','shipaddress',vlineno);
			var shiptotext=salesorder.getLineItemText('item','shipaddress',vlineno);
			nlapiLogExecution('ERROR', 'ismultilineship',ismultilineship);
			nlapiLogExecution('ERROR', 'shiptovalue',shiptovalue);
			nlapiLogExecution('ERROR', 'shiptotext',shiptotext);
			var customerrecord=salesorder.getFieldValue('entity');



			var locationinternalid=salesorder.getFieldValue('location');

			var LogoValue;
			var LogoText;
			if(locationinternalid != null && locationinternalid != ""){
				var companylist = nlapiLoadRecord('location', locationinternalid);
				LogoValue=companylist.getFieldValue('logo');
				LogoText=companylist.getFieldText('logo');
			}
			nlapiLogExecution('ERROR','logo value',LogoValue);
			nlapiLogExecution('ERROR','logo text ',LogoText);
			var filefound='';
			//var filefound = nlapiLoadFile('Images/LOGOCOMP.jpg'); 
			if(LogoText !=null && LogoText !='')
				filefound = nlapiLoadFile('Images/'+LogoText+''); 
			else
				filefound = nlapiLoadFile('Images/LOGOCOMP.jpg');

			if (filefound) 
			{ 
				nlapiLogExecution('ERROR', 'Event', 'file;'+filefound.getId()); 
				var imageurl = filefound.getURL();
				nlapiLogExecution('ERROR','imageurl',imageurl);
				//var finalimageurl = url + imageurl;//+';';
				var finalimageurl = imageurl;//+';';
				//finalimageurl=finalimageurl+ '&expurl=T;';
				nlapiLogExecution('ERROR','imageurl',finalimageurl);
				finalimageurl=finalimageurl.replace(/&/g,"&amp;");

			} 
			else 
			{
				nlapiLogExecution('ERROR', 'Event', 'No file;');
			}

			nlapiLogExecution('ERROR', 'customerrecord',customerrecord);
			var entityrecord ;
			if(customerrecord != "" && customerrecord != null)
			{
				// case# 201417164
				if(trantype=="salesorder")
				{
					entityrecord = nlapiLoadRecord('customer', customerrecord);
					nlapiLogExecution('ERROR', 'entityrecord',entityrecord);
				}
				else
				{
					entityrecord = nlapiLoadRecord('vendor', customerrecord);
					nlapiLogExecution('ERROR', 'entityrecord',entityrecord);
				}

			}


			if(address != null && address !="")
				address=address.replace(replaceChar,'');
			else
				address="";

			var	HNo = salesorder.getFieldValue('shipaddr1');
			if(HNo != null && HNo !="")
				HNo=HNo.replace(replaceChar,'');
			else
				HNo="";
			var	addr2 = salesorder.getFieldValue('shipaddr2');
			if(addr2 != null && addr2 !="")
				addr2=addr2.replace(replaceChar,'');
			else
				addr2="";
			nlapiLogExecution('ERROR','addr2',addr2);
			var	city = salesorder.getFieldValue('shipcity');
			if(city != null && city !="")
				city=city.replace(replaceChar,'');
			else
				city="";
			var	state = salesorder.getFieldValue('shipstate');
			if(state != null && state !="")
				state=state.replace(replaceChar,'');
			else
				state="";
			var	country = salesorder.getFieldValue('shipcountry');
			if(country != null && country !="")
				country=country.replace(replaceChar,'');
			else
				country="";
			var	zipcode = salesorder.getFieldValue('shipzip');
			var	carrier = salesorder.getFieldText('shipmethod');
			if(carrier != null && carrier !="")
				carrier=carrier.replace(replaceChar,'');
			var SalesorderNo= salesorder.getFieldValue('tranid');

			//var tempInstructions=salesorder.getFieldValue('custbody_nswmspoinstructions');
			var tempInstructions=salesorder.getFieldValue('custbody_nswmsnote1');
			var Instructions="";
			if(tempInstructions!=null)
			{
				Instructions=tempInstructions;
			}
			if(Instructions != null && Instructions !="")
				Instructions=Instructions.replace(replaceChar,'');
			else
				Instructions="";

			//calculate total wt for particular so#
			/*for (var x = 0; x < searchresults.length; x++)
			{
				var searchresult = searchresults[x];
				var ContainerLP=searchresult.getValue('custrecord_container_lp_no');
				vdono = searchresult.getValue('name');
				if(vdono.split('.')[0] == SalesorderNo)
				{

					if( searchresults[x].getValue('custrecord_total_weight')!=null && searchresults[x].getValue('custrecord_total_weight')!="")
						totalwt=parseFloat(totalwt)+parseFloat(searchresults[x].getValue('custrecord_total_weight'));
				}
				var result=GetCubeWt_LpMaster(ContainerLP,salesorder.getFieldValue('location'));
				totalwt=parseFloat(totalwt)+parseFloat(result[0]);
			}*/

			if(ismultilineship=='T')
			{
				if(entityrecord!=null && entityrecord!='')
				{
					var custlineitemcount=entityrecord.getLineItemCount('addressbook');
					for(var customerline=1;customerline<=custlineitemcount;customerline++)
					{	var custline=parseInt(customerline).toString();
//					var customerlabel = entityrecord.getLineItemValue('addressbook','label',custline);
					//phonenumber=entityrecord.getLineItemValue('addressbook','phone',custline);
//					if(customerlabel==shiptotext)
					var customerlabelid = entityrecord.getLineItemValue('addressbook','internalid',custline);
					if(customerlabelid==shiptovalue)
					{
						nlapiLogExecution('ERROR', 'test','test1');
						address = entityrecord.getLineItemValue('addressbook','addressee',custline);
						if(address != null && address !="")
							address=address.replace(replaceChar,'');
						else
							address="";
						HNo= entityrecord.getLineItemValue('addressbook','addr1',custline);
						if(HNo != null && HNo !="")
							HNo=HNo.replace(replaceChar,'');
						else
							HNo="";
						addr2 = entityrecord.getLineItemValue('addressbook','addr2',custline);
						if(addr2 != null && addr2 !="")
							addr2=addr2.replace(replaceChar,'');
						else
							addr2="";

						city = entityrecord.getLineItemValue('addressbook','city',custline);
						if(city != null && city !="")
							city=city.replace(replaceChar,'');
						else
							city="";
						state = entityrecord.getLineItemValue('addressbook','dropdownstate',custline);
						if(state != null && state !="")
							state=state.replace(replaceChar,'');
						else
							state="";
						zipcode = entityrecord.getLineItemValue('addressbook','zip',custline);
						if(zipcode != null && zipcode !="")
							zipcode=zipcode.replace(replaceChar,'');
						country=entityrecord.getLineItemValue('addressbook','country',custline);
						if(country != null && country !="")
							country=country.replace(replaceChar,'');
						else
							country="";
					}
					}
				}
			}

			for (var x = 0; x < searchresults.length; x++)
			{
				var searchresult = searchresults[x];
				var ContainerLP=searchresult.getValue('custrecord_container_lp_no');
				CartonArray[x]=ContainerLP;
				//nlapiLogExecution('ERROR','ContainerLP',ContainerLP);
			}

			var duplicateContainerLp= vremoveDuplicateElement(CartonArray);
			nlapiLogExecution('ERROR','duplicateContainerLp',duplicateContainerLp);

			/*for ( var count = 0; count < duplicateContainerLp.length; count++) 
			{
				nlapiLogExecution('ERROR','duplicateContainerLp',duplicateContainerLp.length);
				var result=GetCubeWt_LpMaster(duplicateContainerLp[count],whLocation);
				nlapiLogExecution('ERROR','result',result[0]);

				totalwt=totalwt+(parseFloat(result[0])*parseFloat(1));
				vtotalcube=vtotalcube+(parseFloat(result[1])*parseFloat(1));
			}
			 */
			for (var x = 0; x < searchresults.length; x++)
			{
				var searchresult = searchresults[x];
				vdono = searchresult.getValue('name');
				if(vdono.split('.')[0] == SalesorderNo)
				{

					if( searchresults[x].getValue('custrecord_total_weight')!=null && searchresults[x].getValue('custrecord_total_weight')!="")
						totalwt=parseFloat(totalwt)+parseFloat(searchresults[x].getValue('custrecord_total_weight'));
				}
			}

			nlapiLogExecution('ERROR','totalwt',totalwt);
			var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<head><macrolist><macro id='myfooter'><p align='center'>Page <pagenumber/> of <totalpages/></p></macro></macrolist></head><body  font-size=\"7\"  size=\"A4-landscape\"    padding-top=\" 0mm\"   footer='myfooter' footer-height='20mm'>\n";

			if(count==0)
				var strxml = "<table width='100%' >";
			else
			{
				var strxml=strxml+"";
				strxml += "<table width='100%' >";
			}
			if(pageno==0)
			{
				pageno=parseFloat(pageno+1);
			}


			strxml += "<tr ><td valign='middle' align='left'><img src='" + finalimageurl + "'></img></td><td valign='middle' align='left'  style='font-size:xx-large;'>";
			strxml += "Pick Report ";
			strxml += "</td><td align='right'>&nbsp;</td></tr></table>";
			strxml += "<p align='right'>Date/Time:"+Timez+"</p>";
			strxml +="<table style='width:100%;'>";
			strxml +="<tr><td valign='top'>";
			strxml +="<table align='left' style='width:70%;' border='1'>";
			strxml +="<tr><td align='left' style='width:51px'>Wave# :</td>";

			strxml +="<td>";
			if(vQbWave != null && vQbWave!= "")
			{
				strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
				strxml += parseInt(vQbWave);
				strxml += "\"/>";
			}
			strxml += "</td></tr>";

			strxml +="<tr><td align='left' style='width:51px'>Order# :</td>";

			strxml +="<td>";
			if(SalesorderNo != null && SalesorderNo != "")
			{
				strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
				strxml += SalesorderNo;
				strxml += "\"/>";
			}
			strxml += "</td></tr>";

			strxml +="<tr><td align='left' style='width:51px'>Total Weight(lbs):</td>";
			strxml +="<td>"+totalwt+"</td></tr>";
			strxml +="</table><table><tr><td>&nbsp;</td></tr></table>	<table align='left' style='width:70%;' border='1'>";
			strxml +="<tr><td align='left' style='width:51px'>Carrier:</td>";
			strxml +="<td align='left'>"+carrier+"</td></tr>"; 

			/*//splinstructions = splinstructions.replace(replaceChar,'');
			strxml +="<tr><td align='left' style='width:51px'>Special Instructions:</td>";
			//strxml +="<td align='left'>"+splinstructions+"</td></tr>"; 
			strxml +="<td align='left'>"+Instructions+"</td></tr>"; */

			strxml +="</table></td>";
			strxml +="<td>";
			strxml +="<table align='right' style='width:60%;' border='1'>";
			strxml +="<tr><td align='center' colspan='2'><b>Ship To</b></td></tr>";
			strxml +="<tr><td align='right' style='width:51px'>Address:</td>";
			strxml +="<td>"+address+"</td></tr>";
			strxml +="<tr><td style='width:51px'>&nbsp;</td>";
			strxml +="<td>"+HNo+"</td></tr>";
			if(addr2 != null && addr2 != '')
			{	
				strxml +="<tr><td style='width:51px'>&nbsp;</td>";
				strxml +="<td>"+addr2.replace(replaceChar,'')+"</td></tr>";
			}
			strxml +="<tr><td align='right' style='width:51px'>City:</td>";
			strxml +="<td>"+city+"</td></tr>";
			strxml +="<tr><td align='right' style='width:51px'>State:</td>";
			strxml +="<td>"+state+"</td></tr>";
			strxml +="<tr><td align='right' style='width:51px'>Zip:</td>";
			strxml +="<td>"+zipcode+"</td></tr>";
			strxml +="<tr><td align='right' style='width:51px'>Country:</td>";
			strxml +="<td>"+country+"</td></tr>";			
			strxml +="</table>";
			strxml +=" <p>&nbsp;</p>";
			strxml +="</td></tr></table>";
			if(Instructions!="")
			{
				strxml +=" <p>&nbsp;</p>";
				strxml +="<table  width='100%' style='width:100%;' >";
				strxml +="<tr style=\"font-weight:bold\"><td width='100%' style='border-width: 0.5px; border-color: #000000'>";
				strxml +="<span style='font-size:9'>Notes: <p> "+Instructions.replace(replaceChar,'')+"</p></span>";
				strxml +="</td></tr></table>";
				strxml +=" <p>&nbsp;</p>";
			}
			strxml +="<table  width='100%'>";
			strxml +="<tr style=\"font-weight:bold;background-color:gray;color:white;\">";

			strxml += "<td width='16%' style='border-width: 1px; border-color: #000000'>";
			strxml += " Fulfillment#";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
			strxml += "Line #";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='16%' style='border-width: 1px; border-color: #000000'>";
			strxml += " Part#/Item";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
			strxml += "UPC Code";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='15%' style='border-width: 1px; border-color: #000000'>";
			strxml += "Item Description";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
			strxml += "Status";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='8%' style='border-width: 1px; border-color: #000000'>";//uncommented
			strxml += "Pack Code";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";	

			strxml += "<td width='3%' style='border-width: 1px; border-color: #000000'>";			
			strxml += "Qty";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='3%' style='border-width: 1px; border-color: #000000'>";			
			strxml += "No.Of Cases";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
			strxml += "Bin Location";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='6%' style='border-width: 1px; border-color: #000000'>";//uncommented
			strxml += "LP #";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='6%' style='border-width: 1px; border-color: #000000'>";
			strxml += "Lot #";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='6%' style='border-width: 1px; border-color: #000000'>";
			strxml += "Expiry Date";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented
			strxml += "Container LP";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented
			strxml += "Container Size";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented
			strxml += "Cluster #";
			strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

			strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
			strxml += "Weight(lbs)";
			strxml += "</td>";


			strxml += "<td style='border-width: 1px; border-color: #000000'>";
			strxml += "Zone";
			strxml += "</td>";

			strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
			strxml += "WMS Carrier";
			strxml =strxml+  "</td></tr>";
			var tempLineNo;var pagetotalno=0;
			if (searchresults != null) {
				for (var i = 0; i < searchresults.length; i++) {
					var searchresult = searchresults[i];

					vline = searchresult.getValue('custrecord_line_no');
					vlotbatch = searchresult.getValue('custrecord_batch_no');
					vitem = searchresult.getValue('custrecord_ebiz_sku_no');
					vqty = searchresult.getValue('custrecord_expe_qty');
					vTaskType = searchresult.getText('custrecord_tasktype');
					vLpno = searchresult.getValue('custrecord_lpno');
					vlocationid = searchresult.getValue('custrecord_actbeginloc');
					vlocation = searchresult.getText('custrecord_actbeginloc');
					vSKUID=searchresult.getValue('custrecord_sku');

					//The below code is commented by Satish.N on 12/05/2012 as we can deirectly get Item name from open task.
					//var Itype = nlapiLookupField('item', vSKUID, 'recordType');
					//var ItemRec = nlapiLoadRecord(Itype, vSKUID);
					//nlapiLogExecution('ERROR', 'Time Stamp after loading Item record',TimeStampinSec());
					//vSKU = ItemRec.getFieldValue('itemid');

					vSKU = searchresult.getText('custrecord_sku');
					vskustatus = searchresult.getText('custrecord_sku_status');
					vpackcode = searchresult.getValue('custrecord_packcode');
					vdono = searchresult.getValue('name');
					vcontlp=searchresult.getValue('custrecord_container_lp_no');
					vcontsize=searchresult.getText('custrecord_container');
					vclusno=searchresult.getValue('custrecord_ebiz_clus_no');
					upccode=searchresult.getValue('upccode','custrecord_sku');
					itemdesc=searchresult.getValue('description','custrecord_sku');
					vweight=searchresult.getValue('custrecord_total_weight');
					var vWMSCarrier=searchresult.getText('custrecord_ebizwmscarrier');
					var vzone=searchresult.getText('custrecord_ebiz_zoneid'); 
					var vExpiryDate=searchresult.getValue('custrecord_expirydate');

					if(upccode != null && upccode !="")
						upccode=upccode.replace(replaceChar,'');

					if(vskustatus != null && vskustatus !="")
						vskustatus=vskustatus.replace(replaceChar,'');

					if(vlotbatch != null && vlotbatch !="")
						vlotbatch=vlotbatch.replace(replaceChar,'');

					if(vcontsize != null && vcontsize !="")
						vcontsize=vcontsize.replace(replaceChar,'');

					if(vzone != null && vzone !="")
						vzone=vzone.replace(replaceChar,'');

					if(vWMSCarrier != null && vWMSCarrier !="")
						vWMSCarrier=vWMSCarrier.replace(replaceChar,'');

					if (itemdimsarr != null && itemdimsarr.length > 0) 
					{	
						//nlapiLogExecution('ERROR', 'AllItemDims.length', itemdimsarr.length);
						for(var p = 0; p < itemdimsarr.length; p++)
						{
							var itemval = itemdimsarr[p][0];

							if(itemdimsarr[p][0] == vitem)
							{
								//nlapiLogExecution('ERROR', 'Item Dims configured for SKU: ', vitem);

								var skuDim = itemdimsarr[p][2];
								var skuQty = itemdimsarr[p][3];	
								var packflag = itemdimsarr[p][5];	
								var weight = itemdimsarr[p][6];
								var cube = itemdimsarr[p][7];
								var uom = itemdimsarr[p][1];

								if(skuDim == '2'){
									vcaseqty = skuQty;
									break;
								}								
							}	
						}
					}
					//nlapiLogExecution('ERROR', 'Case Qty: ', vcaseqty);
//					case# 20124184ÃƒÂ¯Ã‚Â¿Ã‚Â½ Start
					if(vcaseqty != 0 && vcaseqty!='' && vcaseqty!=null && !isNaN(vcaseqty)){
						vnoofcases = parseFloat(vqty / vcaseqty);
					}
					else{
						//case# 20124229 Start
						if(vqty==null || vqty=='')
							vqty=0;
						//case# 20124229 End
						vnoofcases = parseFloat(vqty);
					}
					//nlapiLogExecution('ERROR', '# of Cases: ', vnoofcases);


					if(vdono.split('.')[0] == SalesorderNo)
					{
						if(tempLineNo==null)
						{
							tempLineNo=vline;
						}

						if(vline!=tempLineNo)
						{
							var lineInstructions=salesorder.getLineItemValue('item','custcol2',tempLineNo);
							if(lineInstructions!=null && lineInstructions!="")
							{
								nlapiLogExecution('ERROR','itemInstructions',lineInstructions);
								nlapiLogExecution('ERROR','itemInstructions',lineInstructions);
								strxml +=  "<tr><td width='100%' colspan='15' style='border-width: 1px; border-color: #000000'>";
								strxml +="<span style='font-size:9'> Line Instructions :</span>"+ "<p>"+lineInstructions.replace(replaceChar,'')+"</p>";
								strxml =strxml+  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
							}
						}

						strxml =strxml+  "<tr>";

						strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
						if(vdono != null && vdono != "")
						{
							//strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
							strxml += vdono.replace(replaceChar,'');
							//strxml += "\"/>";
						}
						strxml += "</td>";

						strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
						strxml += vline;
						strxml += "</td>";

						strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
						if(vSKU != null && vSKU != "")
						{
							strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
							strxml += vSKU.replace(/"/g,"&#34;");
							strxml += "\"/>";
						}
						strxml += "</td>";

						strxml += "<td width='8%' style='border-width: 1px; border-color: #000000'>";
						strxml += upccode;
						strxml += "</td>";

						strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
						if(itemdesc != null && itemdesc != "")
							strxml += itemdesc.replace(replaceChar,'');
						strxml += "</td>";

						strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
						strxml += vskustatus;
						strxml += "</td>";

						strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";//uncommented for value
						strxml += vpackcode;
						strxml += "</td>";	

						strxml += "<td width='3%' align='right' style='border-width: 1px; border-color: #000000'>";			
						strxml += vqty;
						strxml += "</td>";

						strxml += "<td width='3%' align='right' style='border-width: 1px; border-color: #000000'>";			
						strxml += vnoofcases.toFixed(2);
						strxml += "</td>";

						strxml += "<td width='5%' style='border-width: 1px; border-color: #000000'>";
						strxml += vlocation.replace(replaceChar,'');
						strxml += "</td>";

						strxml += "<td width='6%' style='border-width: 1px; border-color: #000000'>";//uncommented for value
						strxml += vLpno.replace(replaceChar,'');
						strxml += "</td>";

						strxml += "<td width='6%' style='border-width: 1px; border-color: #000000'>";
						strxml += vlotbatch;
						strxml += "</td>";

						strxml += "<td width='6%' style='border-width: 1px; border-color: #000000'>";
						strxml += vExpiryDate;
						strxml += "</td>";

						strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented for value
						strxml += vcontlp.replace(replaceChar,'');
						strxml += "</td>";

						strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented for value
						strxml += vcontsize;
						strxml += "</td>";

						strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented for value
						if(vclusno != null && vclusno != "")
						{
							//strxml += "<barcode codetype=\"code128\" showtext=\"true\" value=\"";
							strxml += vclusno;
							//strxml += "\"/>";
						}
						strxml += "</td>";

						strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
						strxml += vweight;
						strxml =strxml+  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";


						strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";//uncommented for value
						strxml += vzone;
						strxml +="</td>";

						strxml += "<td width='10%' style='border-width: 1px; border-color: #000000'>";
						strxml += vWMSCarrier;
						strxml =strxml+  "</td></tr>";						  
						tempLineNo=vline;
						pagetotalno=parseFloat(pagetotalno)+1;
					}
				}
			}

			var lineInstructions=salesorder.getLineItemValue('item','custcol2',tempLineNo);

			if(lineInstructions!=null && lineInstructions!="")
			{
				lineInstructions=lineInstructions.replace(replaceChar,'');

				strxml +=  "<tr><td width='100%' colspan='15' style='border-width: 1px; border-color: #000000'>";
				strxml +="<span style='font-size:9'> Line Instructions :</span>"+ "<p>"+lineInstructions.replace(replaceChar,'')+"</p>";
				strxml =strxml+  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
				tempLineNo=null;
			}
			strxml =strxml+"</table>";
			if((distinctSoIds.length-count)>1)
			{
				pageno=parseFloat(pageno)+1;
				strxml=strxml+ "<p style='page-break-after:always'></p>";
			}
			else
			{
				//pageno=parseFloat(pageno)+1;
				//strxml=strxml+ "<p style='vertical-align:bottom;align:right;font-size:9'>Page No: "+pageno+" </p>";
			}
		}
		strxml =strxml+ "\n</body>\n</pdf>";
		xml=xml +strxml;

		var file = nlapiXMLToPDF(xml);	
		//response.setContentType('PDF','PickReport.pdf');
		//response.write( file.getValue() );

		nlapiLogExecution('ERROR','file',file);

		var pdffilename = getwaveNo+'_PickReport.pdf';

		nlapiLogExecution('ERROR','pdffilename',pdffilename);

		var filevalue=file.getValue();
		var newAttachment = nlapiCreateFile(pdffilename,file.type,filevalue);

		var userId = context.getUser();
		nlapiLogExecution('ERROR','userId',userId);
		var userAccountId = context.getCompany();
		var username='';
		try
		{
			if(userId>0){
				var transaction = nlapiLoadRecord('Employee', userId);
				var variable=transaction.getFieldValue('email');
				username=variable.split('@')[0];
			}
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR','exp',exp);
		}

		var strContent="";

		var strSubject="Pick Report printed for Wave# "+vQbWave;//+" and Fulfillment Order# "+vQbfullfillmentNo;// +"  by user "+username+" at  "+Timez; 
		strContent +="Wave#:"+vQbWave;
		strContent +="<br/>";
		strContent +="AccountId#:"+userAccountId;
		strContent +="<br/>";
		strContent +=strSubject;

		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_email_alert_type', null, 'anyof', ['2']));
		filters.push(new nlobjSearchFilter('custrecord_email_trantype', null, 'anyof', ['2']));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_user_email');
		columns[1] = new nlobjSearchColumn('custrecord_email_option');
		var searchresults = nlapiSearchRecord('customrecord_email_config', null, filters, columns);		 
		var email= "";
		var emailbcc="";
		var emailcc="";
		var emailappend="";
		var emailbccappend= new Array();
		var emailccappend=new Array();
		var count=0;
		var bcccount=0;
		for(var g=0;searchresults!=null && g<searchresults.length;g++)
		{
			var emailtext=searchresults[g].getText('custrecord_email_option');
			nlapiLogExecution('ERROR','emailtext',emailtext);
			if(emailtext=="BCC")
			{
				emailbccappend[bcccount]=searchresults[g].getValue('custrecord_user_email');
				bcccount++;		 
			}
			else if(emailtext=="CC")
			{
				emailccappend[count]=searchresults[g].getValue('custrecord_user_email');
				count++;		 
			}
			else
			{
				email =searchresults[g].getValue('custrecord_user_email');
				emailappend +=email+";";
			}
		} 
		var substirngemail= emailappend .substring(0, emailappend .length-1);
		nlapiLogExecution('ERROR','variable ',variable );
		if(variable != null && variable != '')
		{
			substirngemail = substirngemail + ';' + variable;
		}
		nlapiLogExecution('ERROR','substirngemail',substirngemail);

		nlapiSendEmail(userId,substirngemail,strSubject,strContent,emailccappend,emailbccappend,null,newAttachment);

	}

}


function ReverseInvtForKITItem(vwaveno,vordno,vFulfillArr)
{
	try
	{
		nlapiLogExecution('ERROR','ReverseInvtForKITItem',vwaveno+"/"+vordno);
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', vordno));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(vwaveno)+".0"));
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));//pick task
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['26']));//Pick location not assinged
		filters.push(new nlobjSearchFilter('mainline', "custrecord_ebiz_order_no", 'is', "T"));
		filters.push(new nlobjSearchFilter('formulanumeric', null, 'notequalto', 0).setFormula("NVL({custrecord_sku.internalid},0)-NVL({custrecord_parent_sku_no.internalid},0)"));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no',null,'group');
		columns[1] = new nlobjSearchColumn('custrecord_parent_sku_no',null,'group');
		columns[2] = new nlobjSearchColumn('name',null,'group');

		var openFailedtasks = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

		nlapiLogExecution('ERROR','openFailedtasks',openFailedtasks);
		if(openFailedtasks!=null&&openFailedtasks!="")
		{
			for(var count=0;count<openFailedtasks.length;count++)
			{
				var parentitem=openFailedtasks[count].getValue('custrecord_parent_sku_no',null,'group');
				var cntrlno=openFailedtasks[count].getValue('custrecord_ebiz_cntrl_no',null,'group');
				var FoNo=openFailedtasks[count].getValue('name',null,'group');
				nlapiLogExecution('DEBUG', 'openFailedtasks count#'+count,parentitem+"/"+cntrlno+"/"+FoNo);
				var filters = new Array();
				filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', vordno));
				filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(vwaveno)+".0"));
				filters.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', cntrlno));
				filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));//pick task
				filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9']));//Pick location ssinged
				filters.push(new nlobjSearchFilter('custrecord_parent_sku_no', null, 'anyof', parentitem));
				filters.push(new nlobjSearchFilter('formulanumeric', null, 'notequalto', 0).setFormula("NVL({custrecord_sku.internalid},0)-NVL({custrecord_parent_sku_no.internalid},0)"));

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
				columns[1] = new nlobjSearchColumn('custrecord_sku');		
				columns[2] = new nlobjSearchColumn('custrecord_parent_sku_no');
				columns[3] = new nlobjSearchColumn('name');
				columns[4] = new nlobjSearchColumn('custrecord_invref_no');
				columns[5] = new nlobjSearchColumn('custrecord_expe_qty');

				var opentasks = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

				if(opentasks!=null&&opentasks!="")
				{
					for(var i=0;i<opentasks.length;i++)
					{
						var invtref=opentasks[i].getValue('custrecord_invref_no');
						var allocationQuantity=opentasks[i].getValue('custrecord_expe_qty');

						//Update Inventory record
						if(invtref!=null&&invtref!="")
						{
							var inventoryTransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invtref);
							var alreadyallocqty = inventoryTransaction.getFieldValue('custrecord_ebiz_alloc_qty');
							if (isNaN(alreadyallocqty)||alreadyallocqty==null||alreadyallocqty=="")
							{
								alreadyallocqty = 0;
							}
							nlapiLogExecution('DEBUG','alreadyallocqty',alreadyallocqty);
							var totalallocqty=parseFloat(alreadyallocqty)-parseFloat(allocationQuantity);
							inventoryTransaction.setFieldValue('custrecord_ebiz_alloc_qty', parseFloat(totalallocqty));
							inventoryTransaction.setFieldValue('custrecord_ebiz_callinv', 'N');
							inventoryTransaction.setFieldValue('custrecord_ebiz_displayfield', 'N');
							nlapiSubmitRecord(inventoryTransaction,false,true);
							nlapiLogExecution('DEBUG', 'Out of CancelInventoryWithAllocation - Record ID',invtref);
						}
						//End of updating inventory record.

						//Updating open task
						var fields = new Array();
						var values = new Array();

						fields.push('custrecord_wms_status_flag');
						fields.push('custrecord_notes');
						fields.push('custrecord_batch_no');
						fields.push('custrecord_container_lp_no');
						fields.push('custrecord_lpno');
						fields.push('custrecord_sku_status');
						fields.push('custrecord_totalcube');
						fields.push('custrecord_total_weight');
						fields.push('custrecord_actbeginloc');
						fields.push('custrecord_invref_no');
						fields.push('custrecord_from_lp_no');
						fields.push('custrecord_ebizrule_no');
						fields.push('custrecord_ebizmethod_no');
						fields.push('custrecord_ebiz_clus_no');
						fields.push('custrecord_bin_locgroup_seq ');


						values.push('26');
						values.push('Insufficient Inventory for component items');
						values.push('');
						values.push('');
						values.push('');
						values.push('');
						values.push('');
						values.push('');
						values.push('');
						values.push('');
						values.push('');
						values.push('');
						values.push('');
						values.push('');
						values.push('');

						nlapiSubmitField('customrecord_ebiznet_trn_opentask', opentasks[i].getId(), fields, values);

						nlapiLogExecution('DEBUG', 'updating to failed task',opentasks[i].getId());
						//End of updating open task record.
					}
					for(var j=0;j<vFulfillArr.length;j++)
					{
						var fulfillmentOrder=vFulfillArr[j][0];
						var FOInternalID=fulfillmentOrder[3];
						var PickGenQty=vFulfillArr[j][1];
						if(FOInternalID==cntrlno)
						{
							nlapiLogExecution('DEBUG', 'updating to failed task',FOInternalID+"/"+PickGenQty);
							var rec=nlapiLoadRecord('customrecord_ebiznet_ordline',cntrlno);
							var previouspickGenQty=rec.getFieldValue("custrecord_pickgen_qty");
							var newpickGenQty=parseInt(previouspickGenQty)-parseInt(PickGenQty);
							rec.setFieldValue("custrecord_pickgen_qty",newpickGenQty);
							rec.setFieldValue("custrecord_linestatus_flag","25");
							rec.setFieldValue("custrecord_linenotes1","Updated by pickgen sch to revert qty for KIT/ITEM if any member item has failed pick");

							nlapiSubmitRecord(rec,false,true);
						}
					}
				}
			}
		}
	}
	catch(exp)
	{
		nlapiLogExecution("Debug","Exception in ReverseInvtForKITItem",exp);
	}
}


var tempInvtResultsArray=new Array();
function getInventorySearchResults(maxno,vBinlocGroup)
{
	nlapiLogExecution('DEBUG', 'Into getInventorySearchResults');
	nlapiLogExecution('DEBUG', 'maxno',maxno);
	nlapiLogExecution('DEBUG', 'vBinlocGroup',vBinlocGroup);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [18,19]));	
	if(vBinlocGroup!=null && vBinlocGroup!='')
		filters.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', 'custrecord_ebiz_inv_binloc', 'anyof', vBinlocGroup));	

	if(maxno!=-1)
	{
		filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));
	}

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	column[1] = new nlobjSearchColumn('internalid');
	column[1].setSort(false);
	var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, column);
	if(invtsearchresults!=null)
	{
		if(invtsearchresults.length>=1000)
		{
			nlapiLogExecution('DEBUG', 'invtsearchresults',invtsearchresults.length);

			var maxno1=invtsearchresults[invtsearchresults.length-1].getId();
			for(var j=0;j<invtsearchresults.length;j++)
			{
				if(tempInvtResultsArray.indexOf(invtsearchresults[j].getValue('custrecord_ebiz_inv_binloc'))==-1)
					tempInvtResultsArray.push(invtsearchresults[j].getValue('custrecord_ebiz_inv_binloc'));
			}
			getInventorySearchResults(maxno1,vBinlocGroup);
		}
		else
		{
			for(var j=0;j<invtsearchresults.length;j++)
			{
				if(tempInvtResultsArray.indexOf(invtsearchresults[j].getValue('custrecord_ebiz_inv_binloc'))==-1)
					tempInvtResultsArray.push(invtsearchresults[j].getValue('custrecord_ebiz_inv_binloc'));
			}
		}
	}

	nlapiLogExecution('DEBUG', 'Out of getInventorySearchResults');
	return tempInvtResultsArray;
}

function getopenreplensNew(controlno,fulfilmentItem,pfLocationId,pfWHLoc)
{
	nlapiLogExecution('DEBUG','controlno',controlno);
	nlapiLogExecution('DEBUG','fulfilmentItem',fulfilmentItem);
	nlapiLogExecution('DEBUG','pfLocationId',pfLocationId);
	var filters = new Array();
	//filters[0] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', controlno); // Remove this from where clause
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8])); // Replen task
	filters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));
	filters.push(new nlobjSearchFilter('custrecord_sku', null,'anyof' ,fulfilmentItem));
	filters.push(new nlobjSearchFilter('custrecord_actendloc', null,'anyof' ,pfLocationId));
//	if(pfWHLoc != null && pfWHLoc != '')
	//	filters.push(new nlobjSearchFilter('custrecord_site_id', null,'is' ,pfWHLoc));
	// Add pickface location filter
	// Add location(site) for filter

	var columns = new Array();



	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	nlapiLogExecution('DEBUG','pfLocationIdsearchresults',searchresults);
	return searchresults;
}


function pickRuleZoneLocation(inventorySearchResults, replenItemArray, reportNo, stageLocation,whLocation,salesOrderNo,
		eBizWaveNo,taskpriority){
	var returnval=false;
	for (var s = 0; s < replenItemArray.length; s++){
		var replenItem = replenItemArray[s][0];
		var replenQty=replenItemArray[s][2];
		var actEndLocationId = replenItemArray[s][1];

		var stageLPNo = 0;

		nlapiLogExecution('DEBUG','Item', replenItem);

		var stageLPs = getStageLPsAndUpdateLPRange(parseFloat(1),whLocation);


		var totallocqty=0;

		if (inventorySearchResults != null) {
			for (var t = 0; t < inventorySearchResults.length; t++) {
				var item = inventorySearchResults[t].getValue('custrecord_ebiz_inv_sku');

				var actBatchText = "";
				if(inventorySearchResults[t].getValue('custrecord_ebiz_inv_lot') != null)
					actBatchText=inventorySearchResults[t].getText('custrecord_ebiz_inv_lot');	

				var RecordId = inventorySearchResults[t].getId();
				var actBinLocationId = inventorySearchResults[t].getValue('custrecord_ebiz_inv_binloc');
				if (replenItem == item && actEndLocationId != actBinLocationId){
//					nlapiLogExecution('DEBUG','Inside the item condition check',item);
					var actualQty = inventorySearchResults[t].getValue('custrecord_ebiz_qoh');
					var inventoryQty = inventorySearchResults[t].getValue('custrecord_ebiz_inv_qty');
					var allocatedQty = inventorySearchResults[t].getValue('custrecord_ebiz_alloc_qty');
					var lpNo = inventorySearchResults[t].getValue('custrecord_ebiz_inv_lp');
					var actBinLocationId = inventorySearchResults[t].getValue('custrecord_ebiz_inv_binloc');
					var actBinLocation = inventorySearchResults[t].getText('custrecord_ebiz_inv_binloc');
					var itemStatus = inventorySearchResults[t].getValue('custrecord_ebiz_inv_sku_status');
					var whLocation = inventorySearchResults[t].getValue('custrecord_ebiz_inv_loc');

//					nlapiLogExecution('DEBUG','WH Location',whLocation);

					/*
					 * While actualQty > replenQty and TaskCount > 0
					 * 		Create open task record
					 * 		update everything else
					 * 		reduce actual quantity and task count
					 * loop
					 */
					if(allocatedQty==''||allocatedQty==null)
						allocatedQty=0;

					var Availqty=parseFloat(actualQty)-parseFloat(allocatedQty);
					nlapiLogExecution('DEBUG', 'Availqty', Availqty);
					nlapiLogExecution('DEBUG', 'replenQty', replenQty);
					//if(parseFloat(totalReplenRequired) > 0 && parseFloat(Availqty) <= parseFloat(totalReplenRequired))
					//{
					//	returnval=true;
					if(parseFloat(replenQty) > 0 && parseFloat(Availqty) > 0){

						var tempqty;
						if(parseFloat(Availqty) > parseFloat(replenQty))
						{
							tempqty=replenQty;
						}
						else
						{
							tempqty=Availqty;
						}
						nlapiLogExecution('DEBUG', 'tempqty', tempqty);
						var tempAllocQty = getAllocationQty(tempqty, replenQty, replenQty);


						nlapiLogExecution('DEBUG', 'tempAllocQty', tempAllocQty);

						if(parseInt(tempAllocQty)>parseInt(replenQty))
						{
							tempqty=replenQty;
						}
						else
						{
							tempqty=tempAllocQty;
						}
						nlapiLogExecution('DEBUG', 'tempqty', tempqty);
						//nlapiLogExecution('DEBUG','taskpriority from pick excep in inv gen func if', taskpriority);
						createOpenTaskRecord(tempqty, actBinLocationId, item, whLocation, reportNo, 
								'', stageLPs[stageLPNo], actBatchText, stageLocation, 
								actEndLocationId, lpNo,RecordId,itemStatus,salesOrderNo,eBizWaveNo,taskpriority);


						updateInventoryWithAllocationQuantity(RecordId, tempAllocQty, itemStatus);

						stageLPNo++;
						replenQty=parseInt(replenQty)-parseInt(tempqty);


					}				
					else
					{
						if(replenQty==0)
						{
							returnval=true;
							break;
						}
					}
					/*else {
						var tempActQty = Availqty;
						while(parseFloat(tempActQty) > 0 ){

							var tempAllocQty = getAllocationQty(tempActQty, replenQty, replenQty);
							//nlapiLogExecution('DEBUG','taskpriority from pick excep in inv gen func else', taskpriority);
							createOpenTaskRecord(tempAllocQty, actBinLocationId, item, whLocation, reportNo, 
									'', stageLPs[stageLPNo], actBatchText, stageLocation, 
									actEndLocationId, lpNo,RecordId,itemStatus,salesOrderNo,eBizWaveNo,taskpriority);

						// Updating the inventory with allocation quantity
							updateInventoryWithAllocationQuantity(RecordId, tempAllocQty, itemStatus);


							stageLPNo++;
							returnval=true;
							nlapiLogExecution('DEBUG', 'else: StageLPNo', stageLPNo);
						//}
					}*/
					nlapiLogExecution('DEBUG', 'StageLPNo', stageLPNo);
					//}
				}
			}
		}
	}
	if(returnval==false)
	{
		return -1;
	}
	else
	{
		return true;	
	}
}

function GetContainerForPickingCart(PickingCart)
{
	var searchRecord = new Array();

	try
	{
		nlapiLogExecution('DEBUG', ' into PickingCart', PickingCart);
		if(PickingCart!=null && PickingCart!='')
		{
			var PickingCartArray=new Array();
			PickingCartArray= PickingCart.split(',');
			nlapiLogExecution('DEBUG', ' PickingCartArray', PickingCartArray);
			var filter=new Array();
			filter.push(new nlobjSearchFilter('internalid', null, 'anyof',PickingCartArray));

			var column=new Array();
			column[0]=new nlobjSearchColumn('internalid');
			column[1]=new nlobjSearchColumn('name');
			column[2]=new nlobjSearchColumn('custrecord_maxweight');
			column[3]=new nlobjSearchColumn('custrecord_cubecontainer').setSort();;

			searchRecord=nlapiSearchRecord('customrecord_ebiznet_container', null, filter, column);

			nlapiLogExecution('DEBUG', 'out of PickingCart', searchRecord);

		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'exception in GetContfoePickingCart', exp);
	}

	return searchRecord;

}

function GetOpenTaskforPickmethods(vWaveNo,PickmethodID)
{
	try
	{

		nlapiLogExecution('DEBUG', ' into GetOpenTaskforPickmethods', vWaveNo);
		nlapiLogExecution('DEBUG', ' PickmethodID', PickmethodID);
		var OpenTskfilter=new Array();
		OpenTskfilter.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is',parseFloat(vWaveNo)));
		OpenTskfilter.push(new nlobjSearchFilter('custrecord_ebizmethod_no', null, 'is',PickmethodID));
		OpenTskfilter.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'isempty'));

		var OpenTskcolumn=new Array();
		OpenTskcolumn[0]=new nlobjSearchColumn('name',null,'group').setSort();
		OpenTskcolumn[1]=new nlobjSearchColumn('custrecord_total_weight',null,'sum');
		OpenTskcolumn[2]=new nlobjSearchColumn('custrecord_totalcube',null,'sum');
		OpenTskcolumn[3]=new nlobjSearchColumn('custrecord_ebizmethod_no',null,'group');

		var OpenTaskResults=nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, OpenTskfilter, OpenTskcolumn);

		nlapiLogExecution('DEBUG', 'out of GetOpenTaskforPickmethods', OpenTaskResults);

		return OpenTaskResults;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'exception ', exp);
	}

}


//Get Pickmethods detials
function getpickmethodDetails(vPickmethodId)
{
	nlapiLogExecution('DEBUG',' into getPickMethod',vPickmethodId);
	var pickMethodArray=new Array();

	var filter=new Array();
	filter.push(new nlobjSearchFilter('internalid',null,'is',vPickmethodId));
	var column=new Array();
	column[0]=new nlobjSearchColumn('internalid').setSort();
	column[1]=new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxorders');
	column[2]=new nlobjSearchColumn('custrecord_ebiz_pickmethod_maxpicks');
	column[3]=new nlobjSearchColumn('custrecord_ebiz_cartoptimization');
	column[4]=new nlobjSearchColumn('custrecord_ebiz_pickingcarts');


	var searchresult=nlapiSearchRecord('customrecord_ebiznet_pick_method', null, filter, column);

	if(searchresult!=null)
	{
		for ( var i = 0; i < searchresult.length; i++)
		{
			pickMethodArray[i]=new Array();
			pickMethodArray[i][0]=searchresult[i].getValue('internalid');
			pickMethodArray[i][1]=searchresult[i].getValue('custrecord_ebiz_pickmethod_maxorders');
			pickMethodArray[i][2]=searchresult[i].getValue('custrecord_ebiz_pickmethod_maxpicks');
			pickMethodArray[i][3]=searchresult[i].getValue('custrecord_ebiz_cartoptimization');
			pickMethodArray[i][4]=searchresult[i].getValue('custrecord_ebiz_pickingcarts');
		}
	}

	nlapiLogExecution('DEBUG',' out pickMethodArray',pickMethodArray);
	return pickMethodArray;
}

//fetch the suitable container
function FetchContainer(vFO,OpentaskWeight,OpentaskCube,PickingCarts)
{

	try
	{
		nlapiLogExecution('DEBUG',' into FetchContainer',vFO);
		nlapiLogExecution('DEBUG',' OpentaskWeight',OpentaskWeight);
		nlapiLogExecution('DEBUG',' OpentaskCube',OpentaskCube);
		nlapiLogExecution('DEBUG',' PickingCarts',PickingCarts);

		var UsedContainerArray= new Array();
		var MatchedContainer='';
		var ContainerResults=GetContainerForPickingCart(PickingCarts);
		if(ContainerResults !=null && ContainerResults !='' && ContainerResults.length>0)
		{
			nlapiLogExecution('DEBUG', ' ContainerResults.length',  ContainerResults.length);
			for(var N=0; N<ContainerResults.length; N++)
			{
				var ContWeight= parseFloat(ContainerResults[N].getValue('custrecord_maxweight'));
				var ContCube = parseFloat(ContainerResults[N].getValue('custrecord_cubecontainer'));
				var ContInternalId =ContainerResults[N].getId();

				var str1 = 'OpentaskWeight. = ' + OpentaskWeight + '<br>';
				str1 = str1 + 'OpentaskCube. = ' + OpentaskCube + '<br>';	
				str1 = str1 + 'ContWeight. = ' + ContWeight + '<br>';	
				str1 = str1 + 'ContCube. = ' + ContCube + '<br>';	
				str1 = str1 + 'ContInternalId. = ' + ContInternalId + '<br>';	

				nlapiLogExecution('DEBUG', 'Parameters', str1);

				if((ContWeight>=OpentaskWeight) && (ContCube>=OpentaskCube))
				{
					//UsedContainerArray.push(ContInternalId);
					//MatchedContainer = ContInternalId;
					//break;

					var currow = [ContInternalId,ContCube,ContWeight];
					UsedContainerArray.push(currow);
					break;
				}


			}

		}
		nlapiLogExecution('DEBUG',' MatchedContainer',MatchedContainer);
		nlapiLogExecution('DEBUG',' UsedContainerArray',UsedContainerArray);	

	}
	catch(e)
	{
		nlapiLogExecution('DEBUG',' exception in FetchContainer',e);
	}

	return UsedContainerArray;
}


//Update Opentask with CARTLP and Cluster for Picking Cart Optimization
function UpdateOpenTaskRecords(AutoCartLPNo,vClusterno,OpenTskId,vConntainer)
{
	nlapiLogExecution('DEBUG',' into UpdateOpenTaskRecords',AutoCartLPNo);
	nlapiLogExecution('DEBUG',' vClusterno',vClusterno);
	nlapiLogExecution('DEBUG',' OpenTskId',OpenTskId);
	nlapiLogExecution('DEBUG',' vConntainer',vConntainer);

	try
	{

		if(OpenTskId !=null && OpenTskId !='')
		{
			var fields = new Array();
			var values = new Array();
			fields[0] = 'custrecord_transport_lp';
			fields[1] = 'custrecord_ebiz_clus_no';
			fields[2] = 'custrecord_ebiz_pickingcartsize';
			values[0] = AutoCartLPNo;
			values[1] = vClusterno;
			values[2] = vConntainer;
			var updateStatus = nlapiSubmitField('customrecord_ebiznet_trn_opentask', OpenTskId, fields, values);
		}

		nlapiLogExecution('DEBUG',' updateStatus',updateStatus);

	}
	catch(exp1)
	{
		nlapiLogExecution('DEBUG',' exp1',exp1);
	}

}

//insert CARTLP in LP master
function InsertLP(ItemNewLP)
{
	try {
		nlapiLogExecution('ERROR', 'INTO MASTER LP INSERTION');
		var filtersmlp = new Array();
		filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', ItemNewLP);

		var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

		if (SrchRecord != null && SrchRecord.length > 0) 
		{
			nlapiLogExecution('ERROR', 'LP FOUND');

			//lpExists = 'Y';
		}
		else 
		{
			nlapiLogExecution('ERROR', 'LP NOT FOUND');
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
			customrecord.setFieldValue('name', ItemNewLP);
			customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', ItemNewLP);
			customrecord.setFieldValue('custrecord_ebiz_lpmaster_lptype', 4);//LP type=CART
			var rec = nlapiSubmitRecord(customrecord, false, true);
		}
	} 
	catch (e) 
	{
		nlapiLogExecution('ERROR', 'Failed to Update/Insert into Master LP Record');
	}
}


function cancelwave(pwaveno)
{
	var openpicktasks = getpicktasksforcancellation(pwaveno);

	if(openpicktasks!=null && openpicktasks!='')
	{
		for(var z=0; z< openpicktasks.length; z++)
		{
			var vfointrid = openpicktasks[z].getValue('custrecord_ebiz_cntrl_no');
			var vpickgenqty = openpicktasks[z].getValue('custrecord_expe_qty');
			var vinvrefno = openpicktasks[z].getValue('custrecord_invref_no');
			var vrecordId = openpicktasks[z].getId();

			updatefopickgenqty(vfointrid,vpickgenqty);
			cancelinvtallocations(vinvrefno,vpickgenqty);			
			deletepicktask(vrecordId)
		}
	}
}

function getpicktasksforcancellation(pwaveno)
{
	var filters = new Array();

	filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', pwaveno));

	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','26']));

	var columns = new Array();

	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[2] = new nlobjSearchColumn('custrecord_sku');
	columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[4] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[5] = new nlobjSearchColumn('custrecord_ebiz_lpno');
	columns[6] = new nlobjSearchColumn('custrecord_line_no');
	columns[7] = new nlobjSearchColumn('custrecord_tasktype');
	columns[8] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[9] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[10] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[11] = new nlobjSearchColumn('custrecord_ebiz_receipt_no');
	columns[12] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
	columns[13] = new nlobjSearchColumn('custrecordact_begin_date');
	columns[14] = new nlobjSearchColumn('custrecord_wms_status_flag');
	columns[15] = new nlobjSearchColumn('custrecord_from_lp_no');
	columns[16] = new nlobjSearchColumn('custrecord_actualbegintime');
	columns[17] = new nlobjSearchColumn('custrecord_invref_no');
	columns[18] = new nlobjSearchColumn('custrecord_packcode');
	columns[19] = new nlobjSearchColumn('custrecord_sku_status');
	columns[20] = new nlobjSearchColumn('custrecord_uom_id');
	columns[21] = new nlobjSearchColumn('custrecord_batch_no');
	columns[22] = new nlobjSearchColumn('custrecord_ebizrule_no');
	columns[23] = new nlobjSearchColumn('custrecord_ebizmethod_no');
	columns[24] = new nlobjSearchColumn('custrecord_ebizzone_no');
	columns[25] = new nlobjSearchColumn('custrecord_comp_id');
	columns[26] = new nlobjSearchColumn('custrecord_wms_location');						
	columns[27] = new nlobjSearchColumn('custrecord_ebizuser');		
	columns[28] = new nlobjSearchColumn('custrecord_upd_ebiz_user_no');						
	columns[29] = new nlobjSearchColumn('custrecord_taskassignedto');
	columns[12].setSort(true);  // Sort by wave number

	// Retrieve all open task records for the selected wave
	var openTaskRecords = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	return openTaskRecords;
}

function updatefopickgenqty(pfointrid,ppickgenqty)
{

	if (ppickgenqty == "" || isNaN(ppickgenqty)) {
		ppickgenqty = 0;
	}
	var fulfilmentOrderline = nlapiLoadRecord('customrecord_ebiznet_ordline', pfointrid);

	var prevpickgenqty=fulfilmentOrderline.getFieldValue('custrecord_pickgen_qty');
	if (prevpickgenqty == "" || isNaN(prevpickgenqty)) {
		prevpickgenqty = 0;
	}

	var totalPickGeneratedQuantity = parseInt(prevpickgenqty) - parseInt(ppickgenqty);	
	if(totalPickGeneratedQuantity<0)
		totalPickGeneratedQuantity=0;
	fulfilmentOrderline.setFieldValue('custrecord_pickgen_qty', parseInt(totalPickGeneratedQuantity));
	fulfilmentOrderline.setFieldValue('custrecord_linestatus_flag', '15'); 
	nlapiSubmitRecord(fulfilmentOrderline, false, true);
	nlapiLogExecution('Debug', 'updatefopickgenqty ', 'Success');
}

function cancelinvtallocations(pinvrefno,ppickgenqty)
{

	nlapiLogExecution('Debug', 'cancelinvtallocations', 'Start');

	var inventoryRecord = null;
	try
	{

		var scount=1;
		LABL1: for(var j=0;j<scount;j++)
		{	
			nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', j);
			try
			{
				var inventoryTransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', pinvrefno);
				var allocationQty = inventoryTransaction.getFieldValue('custrecord_ebiz_alloc_qty');

				if (isNaN(allocationQty))
				{
					allocationQty = 0;
				}

				if(parseInt(allocationQty) > 0){
					allocationQty = parseInt(allocationQty) - parseInt(ppickgenqty);
				}

				if(parseInt(allocationQty) <= 0){
					allocationQty=0;
				}

				inventoryTransaction.setFieldValue('custrecord_ebiz_alloc_qty', allocationQty);
				inventoryTransaction.setFieldValue('custrecord_ebiz_callinv', 'N');
				inventoryTransaction.setFieldValue('custrecord_ebiz_displayfield', 'N');
				inventoryTransaction.setFieldValue('custrecord_updated_user_no', nlapiGetContext().getUser());

				nlapiSubmitRecord(inventoryTransaction,false,true);
			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 

				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				}				 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}

		nlapiLogExecution('Debug', 'cancelinvtallocations', 'End');

	}
	catch(exp) {
		nlapiLogExecution('Debug', 'Exception in cancelinvtallocations', exp);	
	}
}

function deletepicktask(precordId)
{
	var deletedId = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', precordId); 	// 4 UNITS
	nlapiLogExecution('Debug', 'Deleting open task record', precordId);
}