/***************************************************************************
	  		   eBizNET
                eBizNET Solutions Inc
 ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_WaveQb_SL.js,v $
 *     	   $Revision: 1.7 $
 *     	   $Date: 2011/09/10 11:18:22 $
 *     	   $Author: schepuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 *
 *****************************************************************************/
/**
 * This is the main function to insert records into stage Inventory custom record
 */
function createStageInventory(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Stage Inventory');

		var binlocation = form.addField('custpage_binlocation', 'select', 'Bin Location','customrecord_ebiznet_location');
		var lpno = form.addField('custpage_lpno', 'text', 'LP #');	
		var item = form.addField('custpage_item', 'select', 'Item','item');	
		var itemstatus = form.addField('custpage_itemstatus', 'select', 'Item Status','customrecord_ebiznet_sku_status');
		var packcode = form.addField('custpage_packcode', 'select', 'Pack Code','customlist_ebiznet_packcode');
		var qty = form.addField('custpage_qty', 'text', 'Qty');
		var lotbatchno = form.addField('custpage_lotbatchno', 'select', 'LOT/Batch #','customrecord_ebiznet_batch_entry');
		var location = form.addField('custpage_location', 'select', 'Location');

		var filterLocation = new Array();
		filterLocation.push(new nlobjSearchFilter('custrecord_ebizwhsite', null, 'is', 'T'));
		var columnLocation = new Array();
		columnLocation[0] = new nlobjSearchColumn('name');
		location.addSelectOption("", "");
		var searchLocationRecord = nlapiSearchRecord('Location', null, filterLocation, columnLocation);
		for(var count=0;count < searchLocationRecord.length ;count++)
		{
			location.addSelectOption(searchLocationRecord[count].getId(),searchLocationRecord[count].getValue('name'));

		}

		var memo = form.addField('custpage_memo', 'text', 'Memo');	
		var masterlp = form.addField('custpage_masterlp', 'text', 'Master LP #');
		var tasktype = form.addField('custpage_tasktype', 'select', 'Task Type','customrecord_ebiznet_tasktype');	
		var company = form.addField('custpage_company', 'select', 'Company','customrecord_ebiznet_company');	

		var userId = nlapiGetUser();
		nlapiLogExecution('ERROR', 'userId', userId);
		var updateduser = form.addField('custpage_updateduser', 'select', 'Updated User','Employee');		
		updateduser.setDefaultValue(userId);

		var qtyavailable = form.addField('custpage_qtyavailable', 'text','Qty Available');
		item.setMandatory(true);
		itemstatus.setMandatory(true);
		packcode.setMandatory(true);
		qty.setMandatory(true);
		binlocation.setMandatory(true);		
		lpno.setMandatory(true);
		location.setMandatory(true);

		form.addSubmitButton('Save');
		response.writePage(form);
	}
	else //this is the POST block
	{
		var form = nlapiCreateForm('Stage Inventory');

		var binLocation = request.getParameter('custpage_binlocation');
		var quantity = request.getParameter('custpage_qty');
		var tasktype = request.getParameter('custpage_tasktype');
		var lpno = request.getParameter('custpage_lpno');
		var lotBatchno = request.getParameter('custpage_lotbatchno');
		var company = request.getParameter('custpage_company');
		var item = request.getParameter('custpage_item');
		var location = request.getParameter('custpage_location');
		var updateduser = request.getParameter('custpage_updateduser');
		var itemstatus = request.getParameter('custpage_itemstatus');
		var memo = request.getParameter('custpage_memo');
		var qtyavailable = request.getParameter('custpage_qtyavailable');
		var packcode = request.getParameter('custpage_packcode');		
		var masterlp=request.getParameter('custpage_masterlp');

		form.setScript('customscript_cyclecountresolvesubmit');
		CreateStageInventory(binLocation,quantity,tasktype,lpno,lotBatchno,company,item,location,updateduser,itemstatus,memo,qtyavailable,packcode,masterlp);
	}
}
/**
 * This functions returns the bin location name based on bin location Id
 * @param binLocation
 * @returns
 */
function getBinLocationName(binLocation){

	var fields = ['name'];
	var columns = nlapiLookupField('customrecord_ebiznet_location', binLocation, fields);

	var binLocationName = columns.name;
	nlapiLogExecution('ERROR', "binLocationName", binLocationName);

	return binLocationName;
}
/**
 * This function is used to insert below parameters into stage inventory custom record and LP Master custom record.
 * Before inserting into LP Master its check whether the given LP is existing or not.If not we will insert that LP into LP Master.
 * 
 * @param binLocation
 * @param quantity
 * @param tasktype
 * @param lpno
 * @param lotBatchno
 * @param company
 * @param item
 * @param location
 * @param updateduser
 * @param itemstatus
 * @param memo
 * @param qtyavailable
 * @param packcode
 * @param masterlp
 */
function CreateStageInventory(binLocation,quantity,tasktype,lpno,lotBatchno,company,item,location,updateduser,itemstatus,memo,qtyavailable,packcode,masterlp)
{
	//create a Stage inventory record
	var CreateStageInventoryRecord = nlapiCreateRecord('customrecord_ebiznet_stageinventory');
	nlapiLogExecution('ERROR', 'Create Stage Inventory Record', 'Stage Inventory Record');		

	var binLocationname = getBinLocationName(binLocation);      
	var inBoundOutboundLocGroupIds=getInBoundOutboundLocGroupIds(binLocationname);

	CreateStageInventoryRecord.setFieldValue('custrecord_ebiz_sinv_binloc', binLocation);		 
	CreateStageInventoryRecord.setFieldValue('custrecord_ebiz_sinv_qty', quantity);		 
	CreateStageInventoryRecord.setFieldValue('custrecord_ebiz_sinv_tasktype', tasktype);
	CreateStageInventoryRecord.setFieldValue('custrecord_ebiz_sinv_lp', lpno);	
	CreateStageInventoryRecord.setFieldValue('custrecord_ebiz_sinv_lot', lotBatchno);
	CreateStageInventoryRecord.setFieldValue('custrecord_ebiz_sinv_company', company);
	CreateStageInventoryRecord.setFieldValue('custrecord_ebiz_sinv_sku', item);
	CreateStageInventoryRecord.setFieldValue('custrecord_ebiz_sinv_loc', location);
	CreateStageInventoryRecord.setFieldValue('custrecord_ebiz_sinv_updated_user_no', updateduser);
	CreateStageInventoryRecord.setFieldValue('custrecord_ebiz_sinv_sku_status', itemstatus);
	CreateStageInventoryRecord.setFieldValue('custrecord_ebiz_sinv_note1', memo);
	CreateStageInventoryRecord.setFieldValue('custrecord_ebiz_sinv_avl_qty', qtyavailable);
	CreateStageInventoryRecord.setFieldValue('custrecord_ebiz_sinv_packcode', packcode);
	CreateStageInventoryRecord.setFieldValue('custrecord_ebiz_sinv_masterlp', masterlp);

	CreateStageInventoryRecord.setFieldValue('custrecord_ebiz_sinv_inboundlocgroupid', inBoundOutboundLocGroupIds[0]);
	CreateStageInventoryRecord.setFieldValue('custrecord_ebiz_sinv_outboundslocgroupid', inBoundOutboundLocGroupIds[1]);
	//setting wms status flag = INVENTORY.STORAGE
	CreateStageInventoryRecord.setFieldValue('custrecord_ebiz_sinv_wms_statusflag', 19);

	nlapiLogExecution('ERROR', 'Submitting Create Inventory Record', 'Inventory Record');

	//commit the record to NetSuite
	var CreateStageInventoryRecordId = nlapiSubmitRecord(CreateStageInventoryRecord, false, true);

	nlapiLogExecution('ERROR', 'Stage Inventory Creation', 'Success');

	try 
	{
		nlapiLogExecution('ERROR', 'INTO MASTER LP INSERTION');
		var filtersmlp = new Array();
		filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', lpno);

		var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp,null);

		if (SrchRecord != null && SrchRecord.length > 0) 
		{
			nlapiLogExecution('ERROR', 'LP FOUND');			
		}
		else 
		{
			nlapiLogExecution('ERROR', 'LP NOT FOUND');
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
			customrecord.setFieldValue('name', lpno);
			customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', lpno);
			var rec = nlapiSubmitRecord(customrecord, false, true);
			nlapiLogExecution('ERROR', 'Master LP Insertion', 'Success');
		}
	} 
	catch (e) 
	{
		nlapiLogExecution('ERROR', 'Failed to Update/Insert into Master LP Record');
	}

	var arrDims = getSKUCubeAndWeight(item, 1);
	var itemCube = 0;
	if (arrDims["BaseUOMItemCube"] != "" && (!isNaN(arrDims["BaseUOMItemCube"]))) 
	{
		var uomqty = ((parseFloat(Quantity))/(parseFloat(arrDims["BaseUOMQty"])));			
		itemCube = (parseFloat(uomqty) * parseFloat(arrDims["BaseUOMItemCube"]));
		nlapiLogExecution('ERROR', 'itemCube', itemCube);
	} 

	var vOldRemainingCube = GeteLocCube(binLocation);
	nlapiLogExecution('ERROR', 'vOldRemainingCube', vOldRemainingCube);

	var vTotalCubeValue = parseFloat(itemCube) - parseFloat(vOldRemainingCube);
	nlapiLogExecution('ERROR', 'vOldLocationRemainingCube', vTotalCubeValue);

	var retValue =  UpdateLocCube(binLocation,vTotalCubeValue);
	nlapiLogExecution('ERROR', 'Loc Cube Updation', 'Success');

	response.sendRedirect('SUITELET', 'customscript_stage_inventory', 'customdeploy_stage_inventory_di', false, null);
}
/**
 * This function is used to fetch the inbound location group id and outbound location group id base on given binlocation
 * @param getLocation
 * @returns {Array}
 */
function getInBoundOutboundLocGroupIds(getLocation){

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_inboundlocgroupid');
	columns[1] = new nlobjSearchColumn('custrecord_outboundlocgroupid');
	var inoutlocgroup=new Array();
	var LocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null, new nlobjSearchFilter('name', null, 'is', getLocation),columns);

	var inboundLocationGrpId="";
	var outboundLocationGrpId="";

	if (LocationSearch!=null && LocationSearch.length != 0) {
		nlapiLogExecution('ERROR', 'Length of Location Search', LocationSearch.length);

		for (var s = 0; s < LocationSearch.length; s++) {
			var EndLocationId = LocationSearch[s].getId();
			inboundLocationGrpId=LocationSearch[s].getValue('custrecord_inboundlocgroupid'); 
			outboundLocationGrpId=LocationSearch[s].getValue('custrecord_outboundlocgroupid');
			inoutlocgroup[0]=inboundLocationGrpId;
			inoutlocgroup[1]=outboundLocationGrpId;

			nlapiLogExecution('ERROR', 'inboundLocationGrpId', inboundLocationGrpId);
			nlapiLogExecution('ERROR', 'outboundLocationGrpId', outboundLocationGrpId);
		}
	}
	return inoutlocgroup;
}