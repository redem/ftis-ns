/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Schedulers/Attic/ebiz_WaveTemplete_SL.js,v $

 *     	   $Revision: 1.1.2.5 $
 *     	   $Date: 2015/09/03 15:28:38 $
 *     	   $Author: snimmakayala $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 * *
 * DESCRIPTION
 *  	
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_WaveTemplete_SL.js,v $
 * Revision 1.1.2.5  2015/09/03 15:28:38  snimmakayala
 * Case#: 201414186
 *
 * Revision 1.1.2.4  2015/08/25 09:32:52  snimmakayala
 * no message
 *
 * Revision 1.1.2.3  2015/08/07 15:40:00  skreddy
 * Case# 201413328
 * 2015.2 issue fix
 *
 * Revision 1.1.2.2  2015/07/03 15:32:42  grao
 * 2015.2 issue fixes  201413328
 *
 * Revision 1.1.2.1  2015/06/16 11:29:03  skreddy
 * Case# 201413135
 * wave by template CR
 *
 * Revision 1.1.4.8.2.20.2.14  2015/05/07 13:25:34  schepuri
 * case# 201412685
 *
 *
 **********************************************************************************************************************/
/**
 * 
 * @param request
 * @param response
 */
function WaveTemplete(type){
	nlapiLogExecution('DEBUG','Into WaveTemplete');
	GenerateWavesBasedonTemplate();
}



var searchResultArray=new Array();

function getOrdersForWaveGen(){
	// Validating all the request parameters and pushing to a local array
	var localVarArray;
	//this is to maintain the querystring parameters while refreshing the paging drop down

	localVarArray = validateRequestParams();	

	// Get all the search filters for wave generation
	nlapiLogExecution('DEBUG','test1','test1');

	var maxno;
	var maxordno;
	var filters = new Array();   
	filters = specifyWaveFilters(localVarArray,maxno,maxordno);
	// Get all the columns that the search should return
	var columns = new Array();
	columns = getWaveColumns();

	var orderList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
	nlapiLogExecution('DEBUG','orderList',orderList);
//	if( orderList!=null && orderList.length>=1000)
//	{ 
//	searchResultArray.push(orderList); 
//	var maxordno1=orderList[orderList.length-1].getValue(columns[8]);
//	getOrdersForWaveGen(request,null,form,maxordno1);	
//	}
//	else
//	{
	searchResultArray.push(orderList); 
	//}
	return searchResultArray;
}



function GenerateWavesBasedonTemplate(){

	nlapiLogExecution('DEBUG',' Into GenerateWavesBasedonTemplate','start');
	/*	
var soNo = "";
	var companyName = "";
	var itemName= "";
	var customerName = "";
	var orderPriority = "";
	var orderType = "";
	var itemGroup = "";
	var itemFamily = "";
	var packCode = "";
	var UOM = "";
	var itemStatus = "";
	var itemInfo1 = "";
	var itemInfo2 = "";
	var itemInfo3 = "";
	var shippingCarrier = "";
	var shipCountry = ""; 
	var shipState = "";
	var shipCity = "";
	var shipAddr1 = "";
	var shipmentNo = "";
	var routeNo = "";
	var carrier = "";
	var shipdate = "";
	var shiptodate="";
	var ordertodate="";
	var Soid="";
	var wmsstatusflag = "",orderdate="";
	var LinesFrom="";
	var LinesTo="";
	var backorder='';
	var normalorder='';
	var orderselection="";
	var trantype="";
	var location="";
	var NextInterval="";
	//var localVarArray = new Array();
	var Wavetempfilters = new Array();
	var Wavetempletecolumns = new Array();
	var CurrentTime=TimeStamp();
	var Currentdate = DateStamp();
	var vJobInternal='';
	var vNextRunDate='';
	var vNextRunTime='';
	var vLastInternal='';
	var vJobInternal='';
	var vStartDate='';
	var vEndDate='';
	var vRepeat='';
	var vStartTime='';
	var RepeatArray = ["15","30","60","120","240","360","480","720"];// Repeat Array [15mints,30mints,1hour,2hour,4hour,6hour,8hour,12hour]
	 */
	var Wavetempfilters = new Array();
	var Wavetempletecolumns = new Array();
	var CurrentTime=TimeStamp();
	var Currentdate = DateStamp();
	try
	{
		nlapiLogExecution('DEBUG','Currentdate',Currentdate);
		nlapiLogExecution('DEBUG','CurrentTime',CurrentTime);
		var Wavetempfilters=new Array();
		var Wavetempletecolumns =new Array();

		Wavetempfilters.push(new nlobjSearchFilter('custrecord_wms_startdate', null, 'onorbefore', Currentdate));
		//Wavetempfilters.push(new nlobjSearchFilter('custrecord_wms_starttime', null, 'lessthanorequalto', CurrentTime));
		Wavetempfilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_fulfillmentorder'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_shifment'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_route'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_orderpriority'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_ordertype'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_consignee'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_itemfamily'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_itemgroup'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_item'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_iteminfo1'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_iteminfo2'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_iteminfo3'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_packcode'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_uom'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_company'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_shippingmethod'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_itemstatus'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_country'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_shipstate'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_shipaddress'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_shipcity'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_carrier'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_shipdate'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_orderdate'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_freightterms'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_statusflag'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_templatelocation'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_jobinterval'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_last_run_date'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_next_run_date'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_next_run_time'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_last_run_time'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_startdate'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_enddate'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_starttime'));
		Wavetempletecolumns.push(new nlobjSearchColumn('custrecord_wms_repeat'));

		var searchresults = nlapiSearchRecord('customrecord_wmswavetemplate', null, Wavetempfilters, Wavetempletecolumns);


		if(searchresults!=null && searchresults!='' && searchresults.length>0)
		{
			nlapiLogExecution('DEBUG','searchresults.length',searchresults.length);
			for(var i=0; i<searchresults.length;i++)
			{

				var soNo = "";
				var companyName = "";
				var itemName= "";
				var customerName = "";
				var orderPriority = "";
				var orderType = "";
				var itemGroup = "";
				var itemFamily = "";
				var packCode = "";
				var UOM = "";
				var itemStatus = "";
				var itemInfo1 = "";
				var itemInfo2 = "";
				var itemInfo3 = "";
				var shippingCarrier = "";
				var shipCountry = ""; 
				var shipState = "";
				var shipCity = "";
				var shipAddr1 = "";
				var shipmentNo = "";
				var routeNo = "";
				var carrier = "";
				var shipdate = "";
				var shiptodate="";
				var ordertodate="";
				var Soid="";
				var wmsstatusflag = "",orderdate="";
				var LinesFrom="";
				var LinesTo="";
				var backorder='';
				var normalorder='';
				var orderselection="";
				var trantype="";
				var location="";
				var NextInterval="";
				//var localVarArray = new Array();

				var vJobInternal='';
				var vNextRunDate='';
				var vNextRunTime='';
				var vLastInternal='';
				var vJobInternal='';
				var vStartDate='';
				var vEndDate='';
				var vRepeat='';
				var vStartTime='';
				var RepeatArray = ["15","30","60","120","240","360","480","720"];// Repeat Array [15mints,30mints,1hour,2hour,4hour,6hour,8hour,12hour]


				var localVarArray = new Array();
				var NextRunDate='';
				var NextRunTime='';
				var LastRunDate='';
				var LastRunTime='';

				if (searchresults[i].getValue('custrecord_wms_startdate')!=null && searchresults[i].getValue('custrecord_wms_startdate')!="" ){
					vStartDate=searchresults[i].getValue('custrecord_wms_startdate'); 
				}

				if (searchresults[i].getValue('custrecord_wms_enddate')!=null && searchresults[i].getValue('custrecord_wms_enddate')!="" ){
					vEndDate=searchresults[i].getValue('custrecord_wms_enddate'); 
				}


				if (searchresults[i].getValue('custrecord_wms_next_run_date')!=null && searchresults[i].getValue('custrecord_wms_next_run_date')!="" ){
					vNextRunDate=searchresults[i].getValue('custrecord_wms_next_run_date'); 
				}

				if (searchresults[i].getValue('custrecord_wms_next_run_time')!=null && searchresults[i].getValue('custrecord_wms_next_run_time')!="" ){
					vNextRunTime=searchresults[i].getValue('custrecord_wms_next_run_time'); 
				}
				if (searchresults[i].getValue('custrecord_wms_starttime')!=null && searchresults[i].getValue('custrecord_wms_starttime')!="" ){
					vStartTime=searchresults[i].getValue('custrecord_wms_starttime'); 
				}

				var TempNextRunDate=nlapiStringToDate(vNextRunDate);
				var vCurrentDate=nlapiStringToDate(Currentdate);
				var TempEndDate=nlapiStringToDate(vEndDate);

				var TimeinMilliseconds = CompareTime(CurrentTime,vNextRunTime); 

				var str = 'vNextRunDate. = ' + vNextRunDate + '<br>';				
				str = str + 'TempNextRunDate. = ' + TempNextRunDate + '<br>';
				str = str + 'vCurrentDate. = ' + vCurrentDate + '<br>';
				str = str + 'vNextRunTime. = ' + vNextRunTime + '<br>';
				str = str + 'TimeinMilliseconds. = ' + TimeinMilliseconds + '<br>';
				str = str + 'vEndDate. = ' + vEndDate + '<br>';
				str = str + 'TempEndDate. = ' + TempEndDate + '<br>';
				str = str + 'localVarArray. = ' + localVarArray + '<br>';
				str = str + 'i = ' + i + '<br>';
				str = str + 'id = ' + searchresults[i].getId() + '<br>';


				nlapiLogExecution('DEBUG', 'Function Parameters', str);

				if(TempNextRunDate<vCurrentDate)
					TimeinMilliseconds=0;


				//compare NextRunDate should be greater than or equal current date /

				if((vNextRunDate ==''||vNextRunDate==null ||(TempNextRunDate <= vCurrentDate)) && (vNextRunTime ==''||vNextRunTime==null ||(TimeinMilliseconds<=0)) && (vEndDate ==''||vEndDate==null ||(TempEndDate >= vCurrentDate)) )
				{


					nlapiLogExecution('DEBUG','dates are same',vCurrentDate);

					var Recid =searchresults[i].getId();

					if (searchresults[i].getValue('custrecord_wms_fulfillmentorder')!=null && searchresults[i].getValue('custrecord_wms_fulfillmentorder')!="" ){
						Soid=searchresults[i].getValue('custrecord_wms_fulfillmentorder'); 
					}

					if (searchresults[i].getValue('custrecord_wms_shifment')!=null && searchresults[i].getValue('custrecord_wms_shifment')!="" ){
						shipmentNo=searchresults[i].getValue('custrecord_wms_shifment'); 
					}	
					if (searchresults[i].getValue('custrecord_wms_route')!=null && searchresults[i].getValue('custrecord_wms_route')!="" ){
						routeNo=searchresults[i].getValue('custrecord_wms_route'); 
					}
					if (searchresults[i].getValue('custrecord_wms_orderpriority')!=null && searchresults[i].getValue('custrecord_wms_orderpriority')!="" ){
						orderPriority=searchresults[i].getValue('custrecord_wms_orderpriority'); 
					}
					if (searchresults[i].getValue('custrecord_wms_ordertype')!=null && searchresults[i].getValue('custrecord_wms_ordertype')!="" ){
						orderType=searchresults[i].getValue('custrecord_wms_ordertype'); 
					}
					if (searchresults[i].getValue('custrecord_wms_consignee')!=null && searchresults[i].getValue('custrecord_wms_consignee')!="" ){
						customerName=searchresults[i].getValue('custrecord_wms_consignee'); 
					}
					if (searchresults[i].getValue('custrecord_wms_itemfamily')!=null && searchresults[i].getValue('custrecord_wms_itemfamily')!="" ){
						itemFamily=searchresults[i].getValue('custrecord_wms_itemfamily'); 
					}
					if (searchresults[i].getValue('custrecord_wms_itemgroup')!=null && searchresults[i].getValue('custrecord_wms_itemgroup')!="" ){
						itemGroup=searchresults[i].getValue('custrecord_wms_itemgroup'); 
					}
					if (searchresults[i].getValue('custrecord_wms_item')!=null && searchresults[i].getValue('custrecord_wms_item')!="" ){
						itemName=searchresults[i].getValue('custrecord_wms_item'); 
					}
					if (searchresults[i].getValue('custrecord_wms_iteminfo1')!=null && searchresults[i].getValue('custrecord_wms_iteminfo1')!="" ){
						itemInfo1=searchresults[i].getValue('custrecord_wms_iteminfo1'); 
					}
					if (searchresults[i].getValue('custrecord_wms_iteminfo2')!=null && searchresults[i].getValue('custrecord_wms_iteminfo2')!="" ){
						itemInfo2=searchresults[i].getValue('custrecord_wms_iteminfo2'); 
					}
					if (searchresults[i].getValue('custrecord_wms_iteminfo3')!=null && searchresults[i].getValue('custrecord_wms_iteminfo3')!="" ){
						itemInfo3=searchresults[i].getValue('custrecord_wms_iteminfo3'); 
					}
					if (searchresults[i].getValue('custrecord_wms_packcode')!=null && searchresults[i].getValue('custrecord_wms_packcode')!="" ){
						packCode=searchresults[i].getValue('custrecord_wms_packcode'); 
					}
					if (searchresults[i].getValue('custrecord_wms_uom')!=null && searchresults[i].getValue('custrecord_wms_uom')!="" ){
						UOM=searchresults[i].getValue('custrecord_wms_uom'); 
					}
					if (searchresults[i].getValue('custrecord_wms_company')!=null && searchresults[i].getValue('custrecord_wms_company')!="" ){
						companyName=searchresults[i].getValue('custrecord_wms_company'); 
					}
					if (searchresults[i].getValue('custrecord_wms_shippingmethod')!=null && searchresults[i].getValue('custrecord_wms_shippingmethod')!="" ){
						shippingCarrier=searchresults[i].getValue('custrecord_wms_shippingmethod'); 
					}
					if (searchresults[i].getValue('custrecord_wms_itemstatus')!=null && searchresults[i].getValue('custrecord_wms_itemstatus')!="" ){
						itemStatus=searchresults[i].getValue('custrecord_wms_itemstatus'); 
					}
					if (searchresults[i].getValue('custrecord_wms_country')!=null && searchresults[i].getValue('custrecord_wms_country')!="" ){
						shipCountry=searchresults[i].getValue('custrecord_wms_country'); 
					}
					if (searchresults[i].getValue('custrecord_wms_shipstate')!=null && searchresults[i].getValue('custrecord_wms_shipstate')!="" ){
						shipState=searchresults[i].getValue('custrecord_wms_shipstate'); 
					}
					if (searchresults[i].getValue('custrecord_wms_shipaddress')!=null && searchresults[i].getValue('custrecord_wms_shipaddress')!="" ){
						shipAddr1=searchresults[i].getValue('custrecord_wms_shipaddress'); 
					}
					if (searchresults[i].getValue('custrecord_wms_shipcity')!=null && searchresults[i].getValue('custrecord_wms_shipcity')!="" ){
						shipCity=searchresults[i].getValue('custrecord_wms_shipcity'); 
					}

					if (searchresults[i].getValue('custrecord_wms_carrier')!=null && searchresults[i].getValue('custrecord_wms_carrier')!="" ){
						carrier=searchresults[i].getValue('custrecord_wms_carrier'); 
					}

					if (searchresults[i].getValue('custrecord_wms_shipdate')!=null && searchresults[i].getValue('custrecord_wms_shipdate')!="" ){
						shipdate=searchresults[i].getValue('custrecord_wms_shipdate'); 
					}

					if (searchresults[i].getValue('custrecord_wms_orderdate')!=null && searchresults[i].getValue('custrecord_wms_orderdate')!="" ){
						orderdate=searchresults[i].getValue('custrecord_wms_orderdate'); 
					}

//					if (searchresults[0].getValue('custrecord_wms_freightterms')!=null && searchresults[0].getValue('custrecord_wms_freightterms')!="" ){
//					location=searchresults[0].getValue('custrecord_wms_freightterms'); 
//					}
					if (searchresults[i].getValue('custrecord_wms_statusflag')!=null && searchresults[i].getValue('custrecord_wms_statusflag')!="" ){
						wmsstatusflag=searchresults[i].getValue('custrecord_wms_statusflag'); 
					}
					if (searchresults[i].getValue('custrecord_wms_templatelocation')!=null && searchresults[i].getValue('custrecord_wms_templatelocation')!="" ){
						location=searchresults[i].getValue('custrecord_wms_templatelocation'); 
					}
					if (searchresults[i].getValue('custrecord_wms_minlines')!=null && searchresults[i].getValue('custrecord_wms_minlines')!="" ){
						LinesFrom=searchresults[i].getValue('custrecord_wms_minlines'); 
					}
					if (searchresults[i].getValue('custrecord_max_lines')!=null && searchresults[i].getValue('custrecord_max_lines')!="" ){
						LinesTo=searchresults[i].getValue('custrecord_max_lines'); 
					}

					if (searchresults[i].getValue('custrecord_wms_jobinterval')!=null && searchresults[i].getValue('custrecord_wms_jobinterval')!="" ){
						vJobInternal=searchresults[i].getValue('custrecord_wms_jobinterval'); 
					}

					if (searchresults[i].getValue('custrecord_wms_nextinterval')!=null && searchresults[i].getValue('custrecord_wms_nextinterval')!="" ){
						vNextInternal=searchresults[i].getValue('custrecord_wms_nextinterval'); 
					}
					if (searchresults[i].getValue('custrecord_wms_lastinterval')!=null && searchresults[i].getValue('custrecord_wms_lastinterval')!="" ){
						vLastInternal=searchresults[i].getValue('custrecord_wms_jobinterval'); 
					}

					if (searchresults[i].getValue('custrecord_wms_repeat')!=null && searchresults[i].getValue('custrecord_wms_repeat')!="" ){
						vRepeat=searchresults[i].getValue('custrecord_wms_repeat'); 
					}

					nlapiLogExecution('DEBUG','soNo',soNo);
					nlapiLogExecution('DEBUG','companyName',companyName);
					nlapiLogExecution('DEBUG','itemName',itemName);
					nlapiLogExecution('DEBUG','customerName',customerName);
					nlapiLogExecution('DEBUG','orderPriority',orderPriority);
					nlapiLogExecution('DEBUG','orderType',orderType);
					nlapiLogExecution('DEBUG','itemGroup',itemGroup);
					nlapiLogExecution('DEBUG','itemFamily',itemFamily);
					nlapiLogExecution('DEBUG','packCode',packCode);
					nlapiLogExecution('DEBUG','UOM',UOM);
					nlapiLogExecution('DEBUG','itemStatus',itemStatus);
					nlapiLogExecution('DEBUG','itemInfo1',itemInfo1);
					nlapiLogExecution('DEBUG','itemInfo2',itemInfo2);
					nlapiLogExecution('DEBUG','itemInfo3',itemInfo3);
					nlapiLogExecution('DEBUG','shippingCarrier',shippingCarrier);
					nlapiLogExecution('DEBUG','shipCountry',shipCountry);
					nlapiLogExecution('DEBUG','shipState',shipState);
					nlapiLogExecution('DEBUG','shipCity',shipCity);
					nlapiLogExecution('DEBUG','shipAddr1',shipAddr1);
					nlapiLogExecution('DEBUG','shipmentNo',shipmentNo);
					nlapiLogExecution('DEBUG','routeNo',routeNo);
					nlapiLogExecution('DEBUG','carrier',carrier);
					nlapiLogExecution('DEBUG','shipdate',shipdate);
					nlapiLogExecution('DEBUG','wmsstatusflag',wmsstatusflag);
					nlapiLogExecution('DEBUG','orderdate',orderdate);
					nlapiLogExecution('DEBUG','LinesFrom',LinesFrom);
					nlapiLogExecution('DEBUG','LinesTo',LinesTo);
					nlapiLogExecution('DEBUG','backorder',backorder);
					nlapiLogExecution('DEBUG','shiptodate',shiptodate);
					nlapiLogExecution('DEBUG','ordertodate',ordertodate);
					nlapiLogExecution('DEBUG','Soid',Soid);
					nlapiLogExecution('DEBUG','orderselection',orderselection);
					nlapiLogExecution('DEBUG','trantype',trantype);
					nlapiLogExecution('DEBUG','location',location);


					var currentRow = [soNo, companyName, itemName, customerName, orderPriority, orderType, itemGroup, itemFamily,
					                  packCode, UOM, itemStatus, itemInfo1, itemInfo2, itemInfo3, shippingCarrier, shipCountry, 
					                  shipState, shipCity, shipAddr1, shipmentNo, routeNo, carrier, shipdate,wmsstatusflag,orderdate,
					                  LinesFrom,LinesTo,backorder,shiptodate,ordertodate,Soid,orderselection,trantype,location];

					localVarArray.push(currentRow);

					nlapiLogExecution('DEBUG','localVarArray',localVarArray);

					var maxno;
					var maxordno;
					var filters = new Array();   
					filters = specifyWaveFilters(localVarArray,maxno,maxordno);
					// Get all the columns that the search should return
					var columns = new Array();
					columns = getWaveColumns();
					var orderList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
					nlapiLogExecution('DEBUG','orderList',orderList);
					if(orderList!=null && orderList!='')
					{
						nlapiLogExecution('DEBUG','orderList length',orderList.length);
					}

					// generate wave and invoke pickgeneration process
					var eBizWaveNo =generateWave(orderList,localVarArray);
					nlapiLogExecution('DEBUG','eBizWaveNo',eBizWaveNo);	

					nlapiLogExecution('DEBUG','vJobInternal',vJobInternal);
					nlapiLogExecution('DEBUG','vRepeat',vRepeat);
					nlapiLogExecution('DEBUG','location',location);
					nlapiLogExecution('DEBUG','Recid',Recid);

					//LastRunDate = Currentdate;
					//LastRunTime = CurrentTime;

					// Repeat Array [15mints,30mints,1hour,2hour,4hour,6hour,8hour,12hour]
					//var RepeatArray = ["15","30","60","120","240","360","480","720"];[converted into mintues];
					// Repeatarray index =[0,1,2,3,4,5,6,7,8];

					if(vJobInternal == '1') //SINGLE EVENT
					{
						NextRunDate=Currentdate;
						NextRunTime=CurrentTime;
					}
					else if(vJobInternal == '2') //DAILY EVENT
					{
						nlapiLogExecution('Debug', 'vJobInternal==2 ', 'Daily Event' );
						//if Repeat value is configured
						if(vRepeat !=null && vRepeat !=''&& vRepeat !='null')
						{
							var today = new Date();
							var vTempRepeat=RepeatArray[vRepeat-1];
							nlapiLogExecution('Debug', 'vTempRepeat ', vTempRepeat );

							var minutes=today.setMinutes(today.getMinutes()+parseInt(vTempRepeat))
							nlapiLogExecution('Debug', 'minutes ', minutes );
							nlapiLogExecution('Debug', 'today2 ', today );
							var arrCurrentTime=TimeStampforSpecificDate(today);
							nlapiLogExecution('Debug', 'CurrentTime ', arrCurrentTime );

							NextRunTime=arrCurrentTime[0];
							nlapiLogExecution('Debug', 'NextRunTime ', NextRunTime );
							NextRunDate=arrCurrentTime[1];
							nlapiLogExecution('Debug', 'NextRunDate ', NextRunDate );
						}
						else
						{
							var NoofDays=1;
							NextRunDate=addDays(NoofDays);
							NextRunTime=CurrentTime;
						}

						nlapiLogExecution('Debug', 'vJobInternal==2 ', 'Daily Event Ends' );

					}
					else if(vJobInternal == '3') //WEEKLY EVENT
					{
						nlapiLogExecution('Debug', 'vJobInternal==3 ', vJobInternal );
						var NoofDays=7;
						//if Repeat value is configured
						if(vRepeat !=null && vRepeat !=''&& vRepeat !='null')
						{
							var today = new Date();
							var vTempRepeat=RepeatArray[vRepeat-1];
							nlapiLogExecution('Debug', 'vTempRepeat ', vTempRepeat );

							var minutes=today.setMinutes(today.getMinutes()+parseInt(vTempRepeat))
							nlapiLogExecution('Debug', 'today2 ', today );
							var arrCurrentTime=TimeStampforSpecificDate(today);
							nlapiLogExecution('Debug', 'arrCurrentTime ', arrCurrentTime );

							NextRunTime=arrCurrentTime[0];
							//NextRunDate=CurrentTime[1];
							NextRunDate=addDays(NoofDays);
						}
						else
						{
							NextRunDate=addDays(NoofDays);
							NextRunTime=CurrentTime;
						}

					}
					else if(vJobInternal == '4') //MONTHLY EVENT
					{
						nlapiLogExecution('Debug', 'vJobInternal==4 ', vJobInternal );
						var NoofDays=30;
						//if Repeat value is configured
						if(vRepeat !=null && vRepeat !=''&& vRepeat !='null')
						{
							var today = new Date();
							var vTempRepeat=RepeatArray[vRepeat-1];
							nlapiLogExecution('Debug', 'vTempRepeat ', vTempRepeat );

							var minutes=today.setMinutes(today.getMinutes()+parseInt(vTempRepeat))
							nlapiLogExecution('Debug', 'today2 ', today );
							var arrCurrentTime=TimeStampforSpecificDate(today);
							nlapiLogExecution('Debug', 'arrCurrentTime ', arrCurrentTime );

							NextRunTime=arrCurrentTime[0];
							//NextRunDate=CurrentTime[1];
							NextRunDate=addDays(NoofDays);
						}
						else
						{
							NextRunDate=addDays(NoofDays);
							NextRunTime=CurrentTime;
						}

					}
					else if(vJobInternal == '5') //YEARLY EVENT
					{
						nlapiLogExecution('Debug', 'vJobInternal==5 ', vJobInternal );
						var NoofDays=365;
						//if Repeat value is configured
						if(vRepeat !=null && vRepeat !=''&& vRepeat !='null')
						{
							var today = new Date();
							var vTempRepeat=RepeatArray[vRepeat-1];
							nlapiLogExecution('Debug', 'vTempRepeat ', vTempRepeat );

							var minutes=today.setMinutes(today.getMinutes()+parseInt(vTempRepeat))
							nlapiLogExecution('Debug', 'minutes ', minutes );
							nlapiLogExecution('Debug', 'today2 ', today );
							var arrCurrentTime=TimeStampforSpecificDate(today);
							nlapiLogExecution('Debug', 'arrCurrentTime ', arrCurrentTime );

							NextRunTime=arrCurrentTime[0];
							//NextRunDate=CurrentTime[1];
							NextRunDate=addDays(NoofDays);
						}
						else
						{
							NextRunDate=addDays(NoofDays);
							NextRunTime=CurrentTime;
						}

					}

					var str = 'vNextRunDate. = ' + NextRunDate + '<br>';				
					str = str + 'vNextRunTime. = ' + NextRunTime + '<br>';
					str = str + 'LastRunDate. = ' + LastRunDate + '<br>';
					str = str + 'LastRunTime. = ' + LastRunTime + '<br>';
					str = str + 'Recid. = ' + Recid + '<br>';


					nlapiLogExecution('DEBUG', 'Function Parameters1', str);

					//Updating Nextrundate,nextruntime,Lastrundate ,last runtime
//					if(eBizWaveNo!=null && eBizWaveNo!='')
//					{
					if(Recid !=null && Recid !='' && Recid !='null')
					{

						var fields = new Array();
						var values = new Array();

						fields.push('custrecord_wms_next_run_date');
						fields.push('custrecord_wms_next_run_time');
						fields.push('custrecord_wms_last_run_date');
						fields.push('custrecord_wms_last_run_time');

						values.push(NextRunDate);
						values.push(NextRunTime);
						values.push(DateStamp());
						values.push(TimeStamp());

						nlapiSubmitField('customrecord_wmswavetemplate', Recid, fields, values);

//						}

						localVarArray.length=0;
					}
				}
			}
		}
	}
	catch(e)
	{
		nlapiLogExecution('DEBUG', 'exception in wave template', e);	
	}

	nlapiLogExecution('DEBUG',' Out of GenerateWavesBasedonTemplate','end');
}

/**
 * Function to articulate all the search filters for wave generation
 * 
 * @param localVarArray
 * @returns {Array}
 */
function specifyWaveFilters(localVarArray,maxno,maxordno){

	nlapiLogExecution('DEBUG','maxno',maxno);
	nlapiLogExecution('DEBUG','maxordno',maxordno);

	var filters = new Array();

	// Sales Order No
	if(localVarArray[0][0] != "" && localVarArray[0][0] != null)
		filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', localVarArray[0][0]));

	nlapiLogExecution('DEBUG','localVarArray[0][0]',localVarArray[0][0]);

	// Company
	if(localVarArray[0][1] != "" && localVarArray[0][1] != null)
		filters.push(new nlobjSearchFilter('custrecord_ordline_company', null, 'is', localVarArray[0][1]));

	// Item Name
	if(localVarArray[0][2] != "" && localVarArray[0][2] != null)
	{
		var soidarr = new Array();
		var itemfilters = new Array();
		if(localVarArray[0][0] != "" && localVarArray[0][0] != null)
			filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', localVarArray[0][0]));

		itemfilters.push(new nlobjSearchFilter('custrecord_ebiz_linesku', null, 'anyof', localVarArray[0][2]));
		if(localVarArray[0][23] != "" && localVarArray[0][23] != null)
			itemfilters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [localVarArray[0][23]]));


		var itemcolumns = new Array();

		itemcolumns.push(new nlobjSearchColumn('custrecord_ns_ord'));

		var ordersList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, itemfilters, itemcolumns);
		if(ordersList!=null && ordersList!='')
		{
			nlapiLogExecution('DEBUG','ordersList',ordersList.length);
			for (var z = 0; ordersList != null && z < ordersList.length; z++) {
				soidarr.push(ordersList[z].getValue('custrecord_ns_ord'));
			}
		}
		nlapiLogExecution('DEBUG','soidarr',soidarr);
		if(soidarr != "" && soidarr != null && soidarr.length>0)
		{
			filters.push(new nlobjSearchFilter('custrecord_ns_ord', null, 'anyof', soidarr));
		}
		else
		{
			filters.push(new nlobjSearchFilter('custrecord_ebiz_linesku', null, 'anyof', localVarArray[0][2]));
		}
	}

	// Customer Name
	if(localVarArray[0][3] != "" && localVarArray[0][3] != null)
		filters.push(new nlobjSearchFilter('custrecord_do_customer', null, 'is', localVarArray[0][3]));

	// Order Priority
	if(localVarArray[0][4] != "" && localVarArray[0][4] != null)
		filters.push(new nlobjSearchFilter('custrecord_do_order_priority', null, 'is', localVarArray[0][4]));

	// Order Type
	if(localVarArray[0][5] != "" && localVarArray[0][5] != null)
		filters.push(new nlobjSearchFilter('custrecord_do_order_type', null, 'is', localVarArray[0][5]));

	// Item Group
	if(localVarArray[0][6] != "" && localVarArray[0][6] != null)
		//filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', localVarArray[0][6]));
		filters.push(new nlobjSearchFilter('custitem_item_group', 'custrecord_ebiz_linesku', 'is', localVarArray[0][6]));

	// Item Family
	if(localVarArray[0][7] != "" && localVarArray[0][7] != null)
		//filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', localVarArray[0][7]));
		filters.push(new nlobjSearchFilter('custitem_item_family', 'custrecord_ebiz_linesku', 'is', localVarArray[0][7]));

	// Pack Code
	if(localVarArray[0][8] != "" && localVarArray[0][8] != null)
		filters.push(new nlobjSearchFilter('custrecord_linepackcode', null, 'is', localVarArray[0][8]));

	// UOM
	if(localVarArray[0][9] != "" && localVarArray[0][9] != null)
		filters.push(new nlobjSearchFilter('custrecord_lineuom_id', null, 'is', localVarArray[0][9]));

	// Item Status
	if(localVarArray[0][10] != "" && localVarArray[0][10] != null)
		filters.push(new nlobjSearchFilter('custrecord_linesku_status', null, 'is', localVarArray[0][10]));

	// Item Info 1
//	if(localVarArray[0][11] != "" && localVarArray[0][11] != null)
//	filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo1', null, 'is', localVarArray[0][11]));

	if(localVarArray[0][11] != "" && localVarArray[0][11] != null)
	{

//		filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo1', null, 'is', localVarArray[0][11]));
		var soidarr = new Array();
		var itemfilters = new Array();
		itemfilters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo1', null, 'is', localVarArray[0][11]));
		if(localVarArray[0][23] != "" && localVarArray[0][23] != null)
			itemfilters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [localVarArray[0][23]]));

		var itemcolumns = new Array();
		itemcolumns.push(new nlobjSearchColumn('custrecord_ns_ord'));

		var ordersList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, itemfilters, itemcolumns);
		if(ordersList!=null && ordersList!='')
		{
			nlapiLogExecution('DEBUG','ordersList',ordersList.length);
			for (var z = 0; ordersList != null && z < ordersList.length; z++) {
				soidarr.push(ordersList[z].getValue('custrecord_ns_ord'));
			}
		}
		nlapiLogExecution('DEBUG','soidarr',soidarr);
		if(soidarr != "" && soidarr != null && soidarr.length>0)
		{
			filters.push(new nlobjSearchFilter('custrecord_ns_ord', null, 'anyof', soidarr));
		}
		else
		{
			filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo1', null, 'is', localVarArray[0][11]));
		}

	}
	// Item Info 2
	if(localVarArray[0][12] != "" && localVarArray[0][12] != null)
		filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo2', null, 'is', localVarArray[0][12]));

	// Item Info 3
	if(localVarArray[0][13] != "" && localVarArray[0][13] != null)
		filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo3', null, 'is', localVarArray[0][13]));

	// Shipping Carrier
	if(localVarArray[0][14] != "" && localVarArray[0][14] != null)
		filters.push(new nlobjSearchFilter('shipmethod', 'custrecord_ns_ord', 'anyof', [localVarArray[0][14]]));

	// Ship Country
	if(localVarArray[0][15] != "" && localVarArray[0][15] != null)
		filters.push(new nlobjSearchFilter('shipcountry', 'custrecord_ns_ord', 'anyof', [localVarArray[0][15]]));

	// Ship State
	if(localVarArray[0][16] != "" && localVarArray[0][16] != null)
		filters.push(new nlobjSearchFilter('shipstate', 'custrecord_ns_ord', 'is', localVarArray[0][16]));

	// Ship City
	if(localVarArray[0][17] != "" && localVarArray[0][17] != null)
		filters.push(new nlobjSearchFilter('shipcity', 'custrecord_ns_ord', 'is', localVarArray[0][17]));

	// Masquerading the ship address with ship city
	if(localVarArray[0][18] != "" && localVarArray[0][18] != null)
		filters.push(new nlobjSearchFilter('shipaddress', 'custrecord_ns_ord', 'is', localVarArray[0][17]));

	// Shipment No.
	if(localVarArray[0][19] != "" && localVarArray[0][19] != null)
		filters.push(new nlobjSearchFilter('custrecord_shipment_no', null, 'is', localVarArray[0][19]));

	// Route No.
	if(localVarArray[0][20] != "" && localVarArray[0][20] != null)
		filters.push(new nlobjSearchFilter('custrecord_route_no', null, 'anyof', [localVarArray[0][20]]));

	//WMS Staus Flag :: 11-Partially Picked, 13-Partially Shipped,15-Selected Into Wave, 25-Edit, 26-Picks Failed
	filters.push(new nlobjSearchFilter( 'custrecord_linestatus_flag', null, 'anyof', [11,13,15,25,26]));

	// Indicates the Sales Order header
	filters.push(new nlobjSearchFilter('mainline', 'custrecord_ns_ord', 'is', 'T'));			
	// carrier Added By Suman2q111q
	if(localVarArray[0][21] != "" && localVarArray[0][21] != null)
		filters.push(new nlobjSearchFilter('custrecord_do_wmscarrier', null, 'anyof', [localVarArray[0][21]]));

	nlapiLogExecution('DEBUG','Shipdate',localVarArray[0][22]);
	if((localVarArray[0][22] != "" && localVarArray[0][22] != null))
	{
		//filters.push(new nlobjSearchFilter('custrecord_ebiz_shipdate', null, 'onorafter', [localVarArray[0][22]]));
		//filters.push(new nlobjSearchFilter('custrecord_ebiz_shipdate', null, 'onorbefore', [localVarArray[0][22]]));
		//modified as on 15th July by suman.
		//If ship date is empty then replace the value with the created date of FO
		filters.push(new nlobjSearchFilter('formuladate', null, 'onorbefore', [localVarArray[0][22]]).setFormula("nvl({custrecord_ebiz_shipdate},{created})"));
	}

	nlapiLogExecution('DEBUG','status flag',localVarArray[0][23]);

	if(localVarArray[0][23] != "" && localVarArray[0][23] != null)
	{
		if(localVarArray[0][23]=="-1")
		{
			filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', ['11','15','25','26','13']));
		}
		else
		{
			filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [localVarArray[0][23]]));
		}

	}

	if(maxno!=null && maxno!='')
		//filters.push(new nlobjSearchFilter('id', null, 'lessthan', maxno));
		filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));

	if(maxordno!=null && maxordno!='' && maxordno!=0)
		filters.push(new nlobjSearchFilter('internalidnumber','custrecord_ns_ord', 'greaterthan', maxordno));

	if((localVarArray[0][24] != "" && localVarArray[0][24] != null) && (localVarArray[0][29] == "" || localVarArray[0][29] == null))
		filters.push(new nlobjSearchFilter('custrecord_ebiz_orderdate', null, 'on', [localVarArray[0][24]]));

	if((localVarArray[0][24] != "" && localVarArray[0][24] != null) && (localVarArray[0][29] != "" && localVarArray[0][29] != null))
		filters.push(new nlobjSearchFilter('custrecord_ebiz_orderdate', null, 'within', [localVarArray[0][24],localVarArray[0][29]]));

	nlapiLogExecution('DEBUG','localVarArray[0][25]',localVarArray[0][25]);
	if(localVarArray[0][25] !=null && localVarArray[0][25] !='')
		filters.push(new nlobjSearchFilter('custrecord_ordline', null, 'greaterThanorequalto', parseInt(localVarArray[0][25])).setSummaryType('count'));

	if(localVarArray[0][26] !=null && localVarArray[0][26] !='')
		filters.push(new nlobjSearchFilter('custrecord_ordline', null, 'lessthanorequalto', parseInt(localVarArray[0][26])).setSummaryType('count'));

	nlapiLogExecution('DEBUG','Order',localVarArray[0][30]);

	if(localVarArray[0][30] != "" && localVarArray[0][30] != null)
		filters.push(new nlobjSearchFilter('custrecord_lineord',null, 'contains', localVarArray[0][30]));

	if((localVarArray[0][31] == 'ALL'))
		filters.push(new nlobjSearchFilter('custrecord_ebiz_backorderno',null, 'greaterthanorequalto', 1));

	if((localVarArray[0][31] == 'BO'))
		filters.push(new nlobjSearchFilter('custrecord_ebiz_backorderno',null, 'greaterthan', 1));

	if((localVarArray[0][31] == 'RO'))
		filters.push(new nlobjSearchFilter('custrecord_ebiz_backorderno',null, 'equalto', 1));

	if((localVarArray[0][32] == '1'))
		filters.push(new nlobjSearchFilter('type', 'custrecord_ns_ord', 'anyof', ['SalesOrd']));
	else if((localVarArray[0][32] == '2'))
		filters.push(new nlobjSearchFilter('type', 'custrecord_ns_ord', 'anyof', ['TrnfrOrd']));

	filters.push(new nlobjSearchFilter('isinactive',"custrecord_ebiz_linesku", 'is', "F"));
	// Case# 20148579 starts
	//if(waveno!=null&&waveno!="")
	//filters.push(new nlobjSearchFilter("custrecord_ebiz_wave",null, 'notequalto', waveno));
	// Case# 20148579 ends
	/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh as part of Standard bundle***/
//	var vRoleLocation=getRoledBasedLocationNew();
//	nlapiLogExecution('DEBUG','vRoleLocation',vRoleLocation);
//	nlapiLogExecution('DEBUG','localVarArray[0][33]',localVarArray[0][33]);
//
//	if(vRoleLocation!="" && vRoleLocation!=null && vRoleLocation!=0)
//	{
//		if(localVarArray[0][33] ==null || localVarArray[0][33]=='')
//			filters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof',vRoleLocation));
//		else
//		{
//			nlapiLogExecution('DEBUG','index',vRoleLocation.indexOf(parseInt(localVarArray[0][33])));
//			if(vRoleLocation.indexOf(parseInt(localVarArray[0][33]))!=-1)
//				filters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof',localVarArray[0][33]));
//			else
//				filters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof',["@NONE@"]));
//		}
//	}
//	else
//	{
		if(localVarArray[0][33] !=null && localVarArray[0][33] !='')
			filters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof',localVarArray[0][33]));
	//}	
	//upto here
	//new filter added by suman as on 040114
	//case# 20149816 starts (custrecord_pickqty is changed to custrecord_pickgen_qty)
	//filters.push(new nlobjSearchFilter('formulanumeric', null, 'greaterthan', 0).setFormula("nvl(TO_NUMBER({custrecord_ord_qty}),0)-nvl(TO_NUMBER({custrecord_pickqty}),0)"));
	filters.push(new nlobjSearchFilter('formulanumeric', null, 'greaterthan', 0).setFormula("nvl(TO_NUMBER({custrecord_ord_qty}),0)-nvl(TO_NUMBER({custrecord_pickgen_qty}),0)"));
	//case# 20149816 ends
	//end of the code

	return filters;
}

/**
 * 
 * @returns {Array}
 */
function getWaveColumns(){
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_lineord',null,'group');
	columns[1] = new nlobjSearchColumn('custrecord_do_customer',null,'group');
	columns[2] = new nlobjSearchColumn('custrecord_do_carrier',null,'group');
	columns[3] = new nlobjSearchColumn('custrecord_do_order_type',null,'group');
	columns[4] = new nlobjSearchColumn('custrecord_do_order_priority',null,'group');
	columns[5] = new nlobjSearchColumn('custrecord_ebiz_orderdate',null,'group');
	columns[6] = new nlobjSearchColumn('custrecord_ebiz_shipdate',null,'group');
	columns[7] = new nlobjSearchColumn('custrecord_ordline',null,'count');
	columns[8] = new nlobjSearchColumn('internalid','custrecord_ns_ord','group');	
	columns[9] = new nlobjSearchColumn('entity','custrecord_ns_ord','group');
	columns[10] = new nlobjSearchColumn('status','custrecord_ns_ord','group');
	columns[11] = new nlobjSearchColumn('custrecord_ord_qty',null,'sum');
	columns[8].setSort();
	return columns;
}


/**
 * 
 * @param request
 * @param response
 */
function generateWave(orderList,localVarArray){

	nlapiLogExecution('DEBUG','Into generateWave');

	//var form = nlapiCreateForm('Wave Templete');
	try{
//		var hiddenField_selectpage = form.addField('custpage_hiddenfieldselectpage', 'text', '').setDisplayType('hidden');
//		hiddenField_selectpage.setDefaultValue('F');	
//		var context = nlapiGetContext();
//		nlapiLogExecution('DEBUG','Remaining usage at the start',context.getRemainingUsage());
//		form.setScript('customscript_wavegeneration_cl');

//		var hiddenQueryParams=form.addField('custpage_qeryparams', 'text', 'queryparams').setDisplayType('hidden');
//		if(request.getParameter('custpage_qeryparams')!=null)
//		{
//		hiddenQueryParams.setDefaultValue(request.getParameter('custpage_qeryparams'));
//		}

//		hiddenQueryParamsval = request.getParameter('custpage_qeryparams');
//		nlapiLogExecution('DEBUG','hiddenQueryParamsval',hiddenQueryParamsval);
		var temparray='';
		var wmsstatusflag='';
		var fono = '';
		var shipmentno='';
		var shipmethod='';

//		if(orderList!=null && orderList!='')
//		{
//		nlapiLogExecution('DEBUG','orderList',orderList.length);

//		for(z=0; z < orderList.length; z++)
//		{
//		fono=orderList[z].getValue('custrecord_ebizqty');
//		fono=orderList[z].getValue('custrecord_ebizqty');
//		fono=orderList[z].getValue('custrecord_ebizqty');
//		fono=orderList[z].getValue('custrecord_ebizqty');
//		}


////		temparray=orderList.split(',');
////		nlapiLogExecution('DEBUG','temparray 1',temparray);

////		if(temparray.length>0 && temparray[23]!='')
////		{
////		fono = temparray[0];
////		shipmethod = temparray[14];
////		shipmentno = temparray[19];
////		wmsstatusflag=temparray[23];				
////		}
//		}		

		// Get the next WAVE and REPORT sequences
		//var eBizWaveNo = GetMaxTransactionNo('WAVE');

		nlapiLogExecution('DEBUG', 'Wave.', eBizWaveNo);


		// Get the list of fulfillment orders selected for wave generation
		// Every element of the returned list contains index, soInternalID, lineNo, doNo, doName, 
		// itemName, itemNo, itemStatus, availQty, orderQty, packCode, uom, lotBatchNo, company, 
		// itemInfo1, itemInfo2, itemInfo3, orderType and orderPriority
		//var fulfillOrderList = getSelectedFulfillmentOrderInfo(request);
		var fulfillOrderList;

//		if(fulfillOrderList !=null)
//		nlapiLogExecution('DEBUG', 'fulfillOrderList.length', fulfillOrderList.length);

		// Proceed only if there is atleast 1 fulfillment order selected
		//if(fulfillOrderList!=null && fulfillOrderList.length > 0){

		var eBizWaveNo=createwaveordrecord(fulfillOrderList,wmsstatusflag,fono,shipmentno,shipmethod,orderList,localVarArray);
		var waveDetails = new Array();
		nlapiLogExecution('DEBUG', 'eBizWaveNo', eBizWaveNo);
		waveDetails["ebiz_wave_no"] = eBizWaveNo;
		waveDetails["short_pick"] = 'N';	

		if(eBizWaveNo!='')
		{
			//showInlineMessage(form, 'Confirmation', 'Wave created and Pick generation has been initiated for Wave '+ eBizWaveNo);
			// case # 20124327ÃƒÆ’Ã‚Â¯Ãƒâ€šÃ‚Â¿Ãƒâ€šÃ‚Â½ start
//			createWaveGenSublist(form);
			// case # 20124327ÃƒÆ’Ã‚Â¯Ãƒâ€šÃ‚Â¿Ãƒâ€šÃ‚Â½ end
			//Add two functions

		}
		else
		{
			//showInlineMessage(form, 'Confirmation', 'No lines are selected');
		}

		//response.writePage(form);
		//}

		return eBizWaveNo;
	}
	catch(exp) {
		nlapiLogExecution('DEBUG', 'Exception in generatewave ', exp);	
		//showInlineMessage(form, 'DEBUG', 'Wave Generation Failed', "");
		//response.writePage(form);
	}

	nlapiLogExecution('DEBUG','Out of generateWave');
}




function specifyOrderFilters(localVarArray,maxno){

	nlapiLogExecution('DEBUG','maxno',maxno);

	var filters = new Array();

	// Sales Order No
	if(localVarArray[0] != "" && localVarArray[0] != null)
		filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', localVarArray[0]));

	// Company
	if(localVarArray[1] != "" && localVarArray[1] != null)
		filters.push(new nlobjSearchFilter('custrecord_ordline_company', null, 'is', localVarArray[1]));

	// Item Name
//	if(localVarArray[2] != "" && localVarArray[2] != null)
//	filters.push(new nlobjSearchFilter('custrecord_ebiz_linesku', null, 'is', localVarArray[2]));

	// Item Name
	if(localVarArray[2] != "" && localVarArray[2] != null)
	{
		var soidarr = new Array();
		var itemfilters = new Array();

		itemfilters.push(new nlobjSearchFilter('custrecord_ebiz_linesku', null, 'anyof', localVarArray[2]));
		if(localVarArray[23] != "" && localVarArray[23] != null)
			itemfilters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [localVarArray[23]]));

		var itemcolumns = new Array();

		itemcolumns.push(new nlobjSearchColumn('internalid','custrecord_ns_ord','group').setSort());

		var ordersList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, itemfilters, itemcolumns);
		if(ordersList!=null && ordersList!='')
		{
			nlapiLogExecution('DEBUG','ordersList',ordersList.length);
			for (var z = 0; ordersList != null && z < ordersList.length; z++) {
				soidarr.push(ordersList[z].getValue('internalid','custrecord_ns_ord','group'));
			}
		}
		nlapiLogExecution('DEBUG','soidarr',soidarr);
		if(soidarr != "" && soidarr != null && soidarr.length>0)
		{
			filters.push(new nlobjSearchFilter('custrecord_ns_ord', null, 'anyof', soidarr));
		}
	}

	// Customer Name
	if(localVarArray[3] != "" && localVarArray[3] != null)
		filters.push(new nlobjSearchFilter('custrecord_do_customer', null, 'is', localVarArray[3]));

	// Order Priority
	if(localVarArray[4] != "" && localVarArray[4] != null)
		filters.push(new nlobjSearchFilter('custrecord_do_order_priority', null, 'is', localVarArray[4]));

	// Order Type
	if(localVarArray[5] != "" && localVarArray[5] != null)
		filters.push(new nlobjSearchFilter('custrecord_do_order_type', null, 'is', localVarArray[5]));

	// Item Group
	if(localVarArray[6] != "" && localVarArray[6] != null)
		filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', localVarArray[6]));

	// Item Family
	if(localVarArray[7] != "" && localVarArray[7] != null)
		filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', localVarArray[7]));

	// Pack Code
	if(localVarArray[8] != "" && localVarArray[8] != null)
		filters.push(new nlobjSearchFilter('custrecord_linepackcode', null, 'is', localVarArray[8]));

	// UOM
	if(localVarArray[9] != "" && localVarArray[9] != null)
		filters.push(new nlobjSearchFilter('custrecord_lineuom_id', null, 'is', localVarArray[9]));

	// Item Status
	if(localVarArray[10] != "" && localVarArray[10] != null)
		filters.push(new nlobjSearchFilter('custrecord_linesku_status', null, 'is', localVarArray[10]));

	// Item Info 1
//	if(localVarArray[11] != "" && localVarArray[11] != null)
//	filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo1', null, 'is', localVarArray[11]));

	if(localVarArray[11] != "" && localVarArray[11] != null)
	{

//		filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo1', null, 'is', localVarArray[0][11]));
		var soidarr = new Array();
		var itemfilters = new Array();
		itemfilters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo1', null, 'is', localVarArray[11]));
		if(localVarArray[23] != "" && localVarArray[23] != null)
			itemfilters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [localVarArray[23]]));

		var itemcolumns = new Array();
		itemcolumns.push(new nlobjSearchColumn('custrecord_ns_ord'));

		var ordersList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, itemfilters, itemcolumns);
		if(ordersList!=null && ordersList!='')
		{
			nlapiLogExecution('DEBUG','ordersList',ordersList.length);
			for (var z = 0; ordersList != null && z < ordersList.length; z++) {
				soidarr.push(ordersList[z].getValue('custrecord_ns_ord'));
			}
		}
		nlapiLogExecution('DEBUG','soidarr1',soidarr);
		if(soidarr != "" && soidarr != null && soidarr.length>0)
		{
			filters.push(new nlobjSearchFilter('custrecord_ns_ord', null, 'anyof', soidarr));
		}
		else
		{
			filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo1', null, 'is', localVarArray[11]));
		}

	}
	// Item Info 2
	if(localVarArray[12] != "" && localVarArray[12] != null)
		filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo2', null, 'is', localVarArray[12]));

	// Item Info 3
	if(localVarArray[13] != "" && localVarArray[13] != null)
		filters.push(new nlobjSearchFilter('custrecord_fulfilmentiteminfo3', null, 'is', localVarArray[13]));

	// Shipping Carrier
	if(localVarArray[14] != "" && localVarArray[14] != null)
		filters.push(new nlobjSearchFilter('shipmethod', 'custrecord_ns_ord', 'anyof', [localVarArray[14]]));

	// Ship Country
	if(localVarArray[15] != "" && localVarArray[15] != null)
		filters.push(new nlobjSearchFilter('shipcountry', 'custrecord_ns_ord', 'anyof', [localVarArray[15]]));

	// Ship State
	if(localVarArray[16] != "" && localVarArray[16] != null)
		filters.push(new nlobjSearchFilter('shipstate', 'custrecord_ns_ord', 'is', localVarArray[16]));

	// Ship City
	if(localVarArray[17] != "" && localVarArray[17] != null)
		filters.push(new nlobjSearchFilter('shipcity', 'custrecord_ns_ord', 'is', localVarArray[17]));

	// Masquerading the ship address with ship city
	if(localVarArray[18] != "" && localVarArray[18] != null)
		filters.push(new nlobjSearchFilter('shipaddress', 'custrecord_ns_ord', 'is', localVarArray[17]));

	// Shipment No.
	if(localVarArray[19] != "" && localVarArray[19] != null)
		filters.push(new nlobjSearchFilter('custrecord_shipment_no', null, 'is', localVarArray[19]));

	// Route No.
	if(localVarArray[20] != "" && localVarArray[20] != null)
		filters.push(new nlobjSearchFilter('custrecord_route_no', null, 'anyof', [localVarArray[20]]));

	//WMS Staus Flag :: 11-Partially Picked, 13-Partially Shipped,15-Selected Into Wave, 25-Edit, 26-Picks Failed
	filters.push(new nlobjSearchFilter( 'custrecord_linestatus_flag', null, 'anyof', [11,13,15,25,26]));

	// Indicates the Sales Order header
	filters.push(new nlobjSearchFilter('mainline', 'custrecord_ns_ord', 'is', 'T'));			

	if(localVarArray[21] != "" && localVarArray[21] != null)
		filters.push(new nlobjSearchFilter('custrecord_do_wmscarrier', null, 'anyof', [localVarArray[21]]));

//	if(localVarArray[22] != "" && localVarArray[22] != null)
//	filters.push(new nlobjSearchFilter('custrecord_ebiz_shipdate', null, 'on', [localVarArray[22]]));

	if((localVarArray[22] != "" && localVarArray[22] != null) && (localVarArray[28] == "" || localVarArray[28] == null) )
		filters.push(new nlobjSearchFilter('custrecord_ebiz_shipdate', null, 'onorbefore', [localVarArray[22]]));


	if((localVarArray[22] != "" && localVarArray[22] != null) && (localVarArray[28] != "" && localVarArray[28] != null))
		filters.push(new nlobjSearchFilter('custrecord_ebiz_shipdate', null, 'within', [localVarArray[22],localVarArray[28]]));


	nlapiLogExecution('DEBUG','status flag',localVarArray[23]);

	if(localVarArray[23] != "" && localVarArray[23] != null)
	{
		if(localVarArray[23]=="-1")
		{
			filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', ['11','15','25','26','13']));
		}
		else
		{
			filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [localVarArray[23]]));
		}
	}



	if(maxno!=null && maxno!='')
		filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));

//	if(localVarArray[24] != "" && localVarArray[24] != null)
//	filters.push(new nlobjSearchFilter('custrecord_record_linedate', null, 'on', [localVarArray[24]]));

//	if(localVarArray[24] != "" && localVarArray[24] != null)
//	filters.push(new nlobjSearchFilter('custrecord_ebiz_orderdate', null, 'on', [localVarArray[24]]));

	if((localVarArray[24] != "" && localVarArray[24] != null) && (localVarArray[29] == "" || localVarArray[29] == null))
		filters.push(new nlobjSearchFilter('custrecord_ebiz_orderdate', null, 'on', [localVarArray[24]]));

	if((localVarArray[24] != "" && localVarArray[24] != null) && (localVarArray[29] != "" && localVarArray[29] != null))
		filters.push(new nlobjSearchFilter('custrecord_ebiz_orderdate', null, 'within', [localVarArray[24],localVarArray[29]]));

//	nlapiLogExecution('DEBUG','Back Order',localVarArray[27]);
//	if(localVarArray[27] == 'T')
//	filters.push(new nlobjSearchFilter('custrecord_ebiz_backorderno', null, 'greaterthan', 1));

	if((localVarArray[31] == 'ALL'))
		filters.push(new nlobjSearchFilter('custrecord_ebiz_backorderno',null, 'greaterthanorequalto', 1));

	if((localVarArray[31] == 'BO'))
		filters.push(new nlobjSearchFilter('custrecord_ebiz_backorderno',null, 'greaterthan', 1));

	if((localVarArray[31] == 'RO'))
		filters.push(new nlobjSearchFilter('custrecord_ebiz_backorderno',null, 'equalto', 1));

	if(localVarArray[30] != "" && localVarArray[30] != null)
		filters.push(new nlobjSearchFilter('custrecord_lineord',null, 'contains', localVarArray[30]));

	if((localVarArray[32] == '1'))
		filters.push(new nlobjSearchFilter('type', 'custrecord_ns_ord', 'anyof', ['SalesOrd']));
	else if((localVarArray[32] == '2'))
		filters.push(new nlobjSearchFilter('type', 'custrecord_ns_ord', 'anyof', ['TrnfrOrd']));
	filters.push(new nlobjSearchFilter('isinactive',"custrecord_ebiz_linesku", 'is', "F"));

//	var vRoleLocation=getRoledBasedLocationNew();
//	nlapiLogExecution('DEBUG','vRoleLocation',vRoleLocation);
//	nlapiLogExecution('DEBUG','localVarArray[33]',localVarArray[33]);
//
//	if(vRoleLocation!="" && vRoleLocation!=null && vRoleLocation!=0)
//	{
//		if(localVarArray[33] ==null || localVarArray[33]=='')
//			filters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof',vRoleLocation));
//		else
//		{
//			nlapiLogExecution('DEBUG','index',vRoleLocation.indexOf(parseInt(localVarArray[0][33])));
//			if(vRoleLocation.indexOf(localVarArray[33])!=-1)
//				filters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof',localVarArray[33]));
//			else
//				filters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof',["@NONE@"]));
//		}
//	}
//	else
//	{
		if(localVarArray[33] !=null && localVarArray[33] !='')
			filters.push(new nlobjSearchFilter('custrecord_ordline_wms_location', null, 'anyof',localVarArray[33]));
//	}	
	//new filter added by suman as on 040114
	//case# 20149816 starts (custrecord_pickqty is changed to custrecord_pickgen_qty)
	//filters.push(new nlobjSearchFilter('formulanumeric', null, 'greaterthan', 0).setFormula("nvl(TO_NUMBER({custrecord_ord_qty}),0)-nvl(TO_NUMBER({custrecord_pickqty}),0)"));
	filters.push(new nlobjSearchFilter('formulanumeric', null, 'greaterthan', 0).setFormula("nvl(TO_NUMBER({custrecord_ord_qty}),0)-nvl(TO_NUMBER({custrecord_pickgen_qty}),0)"));
	//case# 20149816 ends
	//end of the code
	return filters;
}

var orderListArray = new Array();

function getOrdersList(parametersval,maxno,localVarArray)
{
	nlapiLogExecution('DEBUG', 'Into  getOrdersList',parametersval);	
	nlapiLogExecution('DEBUG', 'localVarArray',localVarArray);	

	//localVarArray = validateRequestParams();	

	var filters = new Array();   
	filters = specifyWaveFilters(localVarArray,maxno,null);
	// Get all the columns that the search should return

	var focolumns = new Array();
	focolumns[0] = new nlobjSearchColumn('custrecord_lineord');
	focolumns[1] = new nlobjSearchColumn('custrecord_ordline_company');
	focolumns[2] = new nlobjSearchColumn('custrecord_ordline_wms_location');
	focolumns[3] = new nlobjSearchColumn('custrecord_ordline');
	focolumns[4] = new nlobjSearchColumn('id').setSort();
	focolumns[5] = new nlobjSearchColumn('custrecord_ebiz_lot_number');
	focolumns[6] = new nlobjSearchColumn('custrecord_ebiz_lotnumber_qty');
	focolumns[7] = new nlobjSearchColumn('custrecord_ebiz_lotno_internalid');

	var orderList = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, focolumns);


	if( orderList!=null && orderList!='')
	{
		nlapiLogExecution('DEBUG', 'getOrdersList length', orderList.length);
		if(orderList.length>=1000)
		{ 
			for(var i = 0; i < orderList.length; i++)
			{
				orderListArray.push(orderList[i]); 
			}

			var maxno=orderList[orderList.length-1].getValue('id');
			getOrdersList(null,maxno,localVarArray);	
		}
		else
		{
			for(var i = 0; i < orderList.length; i++)
			{
				orderListArray.push(orderList[i]); 
			}
		}
	}

	nlapiLogExecution('DEBUG', 'Out of getOrdersList');	
	return orderListArray;
}


//function createwaveordrecord(fulfillOrderList,eBizWaveNo,vlineStatus,fono,shipmentno,shipmethod,parametersval)
//{
//nlapiLogExecution('DEBUG', 'Into  createwaveordrecord ', eBizWaveNo);	
function createwaveordrecord(fulfillOrderList,vlineStatus,fono,shipmentno,shipmethod,parametersval,localVarArray)
{

	try
	{
		var eBizWaveNo='';
		var str = 'vlineStatus. = ' + vlineStatus + '<br>';
		str = str + 'fono. = ' + fono + '<br>';	
		str = str + 'shipmentno. = ' + shipmentno + '<br>';	
		str = str + 'shipmethod. = ' + shipmethod + '<br>';	
		str = str + 'parametersval. = ' + parametersval + '<br>';	
		//	str = str + 'fulfillOrderList length. = ' + fulfillOrderList.length + '<br>';

		nlapiLogExecution('DEBUG', 'createwaveordrecord Parameters ', str);	
		var fosearchResults='';

		fosearchResults = getOrdersList(parametersval,null,localVarArray);

		if(fosearchResults != null && fosearchResults != '')
		{


			nlapiLogExecution('DEBUG', 'fosearchResults length', fosearchResults.length);	
		}

		//if(fulfillOrderList != null && fulfillOrderList.length > 0)
		//{
		var context = nlapiGetContext();
		var currentUserID = context.getUser();	
		var searchResults = new Array();


		//for(var i = 0; i < fulfillOrderList.length; i++)
		//{	
		for(var s = 0; fosearchResults!=null && s < fosearchResults.length; s++)
		{
			var vfono = fosearchResults[s].getValue('custrecord_lineord');

			//if(fulfillOrderList[i] == vfono)
			//{
			if(eBizWaveNo=='')
			{
				eBizWaveNo = GetMaxTransactionNo('WAVE');
				nlapiLogExecution('DEBUG', 'eBizWaveNo', eBizWaveNo);
			}

			var ordno = fosearchResults[s].getValue('custrecord_lineord');
			var ordlineno = fosearchResults[s].getValue('custrecord_ordline');
			var company = fosearchResults[s].getValue('custrecord_ordline_company');
			var wmslocation = fosearchResults[s].getValue('custrecord_ordline_wms_location');
			var fointrid = fosearchResults[s].getId();
			var folotnumber="";
			var folotqty="";
			var folotinternalid="";
			folotnumber = fosearchResults[s].getValue('custrecord_ebiz_lot_number');
			folotqty = fosearchResults[s].getValue('custrecord_ebiz_lotnumber_qty');
			folotinternalid = fosearchResults[s].getValue('custrecord_ebiz_lotno_internalid');

			var currentRow = [fointrid,ordno, ordlineno,company,wmslocation,folotnumber,folotqty,folotinternalid];

			searchResults.push(currentRow);
			var vAllowWave='T';
			if(fointrid != null && fointrid != '')
			{	
				var vFOLine= nlapiLoadRecord('customrecord_ebiznet_ordline', fointrid);
				if(vFOLine != null && vFOLine != '')
				{	
					var vFOPresentStatus= vFOLine.getFieldValue('custrecord_linestatus_flag');
					nlapiLogExecution('DEBUG', 'vFOPresentStatus', vFOPresentStatus);
					nlapiLogExecution('DEBUG', 'vlineStatus', vlineStatus);
					/*if(vFOPresentStatus == '15' && vlineStatus != '15')
									vAllowWave='F';*/
					if(vlineStatus != null && vlineStatus != '' && vFOPresentStatus != vlineStatus)
						vAllowWave='F';	
				}
			}
			if(vAllowWave == 'T')
			{
				updateFulfilmentOrder(fointrid, eBizWaveNo);
			}

			//}
		}						
		//}

//		for(var j=0;searchResults!=null && j<searchResults.length;j++)
//		{
//		updateFulfilmentOrder(searchResults[j][0], eBizWaveNo);
//		}	
		if(eBizWaveNo!='')
		{
			nlapiLogExecution('DEBUG', 'Invoking Schedule Script Starts', TimeStampinSec());	


			var param = new Array();
			param['custscript_ebizwaveno'] = eBizWaveNo;
			nlapiScheduleScript('customscript_ebiz_pickgen_scheduler', null,param);
			nlapiLogExecution('DEBUG', 'ordno', ordno);
			orderListArray.length=0;

			//nlapiLogExecution('DEBUG', 'tranType', tranType);
			if((currentUserID == null) || (currentUserID =='') || (currentUserID<0))
				currentUserID='';
			updateScheduleScriptStatus('WAVE RELEASE',currentUserID,'Submitted',eBizWaveNo,null);
			//updateScheduleScriptStatus('WAVE RELEASE',currentUserID,'Submitted',eBizWaveNo,null);

			nlapiLogExecution('DEBUG', 'Invoking Schedule Script Ends', TimeStampinSec());
		}

		//}
	}
	catch(exp) 
	{
		nlapiLogExecution('DEBUG', 'Exception in createwaveordrecord ', exp);	
	}
	nlapiLogExecution('DEBUG', 'Out of  createwaveordrecord ', eBizWaveNo);
	return eBizWaveNo;
}

/**
 * Return the fulfillment order information to be used for wave generation
 * NOTE: WILL NEVER RETURN NULL; WILL ALWAYS RETURN ATLEAST A BLANK ARRAY
 * 
 * @param request
 * @returns {Array}
 */
function getSelectedFulfillmentOrderInfo(request){
	nlapiLogExecution('DEBUG','into getSelectedFulfillmentOrderInfo', '');
	var fulfillOrderInfoArray = new Array();
	var orderInfoCount = 0;

	// Retrieve the number of items for which wave needs to be created
	var lineCount = request.getLineItemCount('custpage_items');

	for(var k = 1; k <= lineCount; k++){
		var selectValue = request.getLineItemValue('custpage_items', 'custpage_so', k);

		if(isItemSelected(request,'custpage_items', 'custpage_so', k)){	

			var doName = request.getLineItemValue('custpage_items', 'custpage_sono', k);
			fulfillOrderInfoArray[fulfillOrderInfoArray.length] = doName;
		}
	}

	nlapiLogExecution('DEBUG','out of getSelectedFulfillmentOrderInfo', '');

	return fulfillOrderInfoArray;
}


/**
 * 
 */
function isItemSelected(request, subList, element, index){
	var retVal = false;
	if(request.getLineItemValue(subList, element, index) == 'T')
		retVal = true;

	return retVal;
}

function updateFulfilmentOrder(fointrid, eBizWaveNo){

	nlapiLogExecution('DEBUG', 'Into updateFulfilmentOrder ',TimeStampinSec());

	var recId = fointrid;

	var fieldNames = new Array(); 				//fields to be updated
	fieldNames[0] = "custrecord_ebiz_wave";  	
	fieldNames[1] = "custrecord_linestatus_flag";

	var newValues = new Array(); 				//new field values
	newValues[0] = eBizWaveNo;
	newValues[1] =  '15';

	nlapiSubmitField('customrecord_ebiznet_ordline', recId, fieldNames, newValues);

	nlapiLogExecution('DEBUG', 'Fulfilment Order Updated Successfully', fointrid);
	nlapiLogExecution('DEBUG', 'Out of updateFulfilmentOrder',TimeStampinSec());
}



//Time Comparision	 

function CompareTime(CurrentTime,vNextRunTime)
{
	try{
		var difference_in_milliseconds=0;
		nlapiLogExecution('DEBUG','Comapre_time_currenttime;', CurrentTime);
		nlapiLogExecution('DEBUG','vNextRunTime;', vNextRunTime);

		if(vNextRunTime !=null && vNextRunTime !='' && CurrentTime!=null && CurrentTime !='')
		{
			var dtStart = new Date("1/1/2007 " + CurrentTime);
			var dtEnd = new Date("1/1/2007 " + vNextRunTime);
			difference_in_milliseconds = dtEnd - dtStart;
		}
	}
	catch (exp)
	{
		nlapiLogExecution('DEBUG','exception in TimeCampare function;', exp);
	}
	nlapiLogExecution('DEBUG','difference_in_milliseconds', difference_in_milliseconds);
	return difference_in_milliseconds;
}


//Add no of days to currentdate
function addDays(NoofDays)
{
	var now = new Date();

	var curdate = ((parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());

	var newdate = nlapiAddDays(new Date(curdate), NoofDays);
	nlapiLogExecution('DEBUG','newdate;', newdate);

	var dtsettingFlag = DateSetting();

	nlapiLogExecution('Debug', 'dtsettingFlag', dtsettingFlag);

	if(dtsettingFlag == 'DD/MM/YYYY')
	{
		return ((parseFloat(newdate.getDate())) + '/' + (parseFloat(newdate.getMonth()) + 1) + '/' +newdate.getFullYear());
	}
	else
	{
		return ((parseFloat(newdate.getMonth()) + 1) + '/' + (parseFloat(newdate.getDate())) + '/' + newdate.getFullYear());
	}

}


//fetching timestamp in hh:mm format
function TimeStampforSpecificDate(today){
	var timestamp;
	var now = new Date(today);
	var a_p = "";
	var ResultsArray=new Array();
	var newdate='';

	//Getting time in hh:mm tt format.
	var curr_hour = now.getHours();
	var curr_min = now.getMinutes();
	var curr_sec = now.getSeconds();

	nlapiLogExecution('DEBUG','curr_hour;', curr_hour);
	nlapiLogExecution('DEBUG','curr_min;', curr_min);

	// determining the am/pm indicator
	if (curr_hour < 12)
		a_p = "am";
	else
		a_p = "pm";

	// finalizing hours depending on 24hr clock
	if (curr_hour == 0)
		curr_hour = 12;
	else if(curr_hour > 12)
	{		
		/*if(curr_hour>23)
		{
			var NoofDays=1;
			newdate=addDays(NoofDays);
		}*/

		curr_hour -= 12;
	}

	if (curr_min.length == 1)
		curr_min = "0" + curr_min;

	//Adding fields to update time zones.
	timestamp = curr_hour + ":" + curr_min + " " + a_p;

	ResultsArray[0]=timestamp;
	if(newdate !=null && newdate !='' && newdate !='null')
		ResultsArray[1]=newdate;
	else
		ResultsArray[1]=DateStamp();

	nlapiLogExecution('DEBUG','ResultsArray;', ResultsArray);
	return ResultsArray;
}