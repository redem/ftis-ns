/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/ebiz_DeKitting_SL.js,v $
 *     	   $Revision: 1.3.4.4.4.4.4.20.2.3 $
 *     	   $Date: 2015/11/12 15:02:09 $
 *     	   $Author: skreddy $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_118 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_DeKitting_SL.js,v $
 * Revision 1.3.4.4.4.4.4.20.2.3  2015/11/12 15:02:09  skreddy
 * case 201415449
 * 2015.2  issue fix
 *
 * Revision 1.3.4.4.4.4.4.20.2.2  2015/11/05 17:41:57  grao
 * 2015.2 Issue Fixes 201415245
 *
 * Revision 1.3.4.4.4.4.4.20.2.1  2015/09/21 14:02:15  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.3.4.4.4.4.4.20  2015/07/22 15:05:05  skreddy
 * Case# 201413580,201413581
 * Briggs SB issue fix
 *
 * Revision 1.3.4.4.4.4.4.19  2015/07/13 15:37:41  grao
 * 2015.2 ssue fixes  201413141
 *
 * Revision 1.3.4.4.4.4.4.18  2015/05/27 15:34:53  grao
 * SB issue fixes  201412900
 *
 * Revision 1.3.4.4.4.4.4.17  2015/04/13 13:10:15  schepuri
 * case # 201412321
 *
 * Revision 1.3.4.4.4.4.4.16  2014/09/22 07:28:08  gkalla
 * case#201221192
 * LL issue Sending wrong inv adjustments while doing partial Dekit
 *
 * Revision 1.3.4.4.4.4.4.15  2014/09/10 15:56:26  sponnaganti
 * Case# 20148562
 * Stnd Bundle issue fixed
 *
 * Revision 1.3.4.4.4.4.4.14  2014/07/31 15:16:12  grao
 * Case#: 20148766  Standard bundel  issue fixes
 *
 * Revision 1.3.4.4.4.4.4.13  2014/07/11 15:32:07  sponnaganti
 * Case# 20149402
 * Compatibility Issue fix
 *
 * Revision 1.3.4.4.4.4.4.12  2014/07/03 15:36:05  sponnaganti
 * case# 20149234
 * Comaptability Acc Issue Fix
 *
 * Revision 1.3.4.4.4.4.4.11  2014/06/11 15:27:06  sponnaganti
 * case# 20148586
 * Stnd Bundle Issue Fix
 *
 * Revision 1.3.4.4.4.4.4.10  2014/06/05 15:12:13  sponnaganti
 * case# 20148654
 * Stnd Bundle Issue Fix
 *
 * Revision 1.3.4.4.4.4.4.9  2014/05/28 15:36:19  skreddy
 * case # 20148583
 * Standard Bundle issue fix
 *
 * Revision 1.3.4.4.4.4.4.8  2014/05/27 07:01:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148504
 *
 * Revision 1.3.4.4.4.4.4.7  2013/11/22 16:46:13  gkalla
 * case#20125682
 * Monobind Dekitting issue
 *
 * Revision 1.3.4.4.4.4.4.6  2013/09/25 06:47:24  schepuri
 * case#20124311, 20124584
 *
 * Revision 1.3.4.4.4.4.4.5  2013/09/11 14:25:53  skreddy
 * Case# 20124332
 * PUT SB  issue fix
 *
 * Revision 1.3.4.4.4.4.4.4  2013/09/02 06:55:28  schepuri
 * case no 20124167
 *
 * Revision 1.3.4.4.4.4.4.3  2013/03/19 11:46:26  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.3.4.4.4.4.4.2  2013/03/08 14:43:37  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.3.4.4.4.4.4.1  2013/03/01 14:34:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.3.4.4.4.4  2013/01/08 15:44:27  gkalla
 * CASE201112/CR201113/LOG201121
 * added WO number and Batch no
 *
 * Revision 1.3.4.4.4.3  2012/12/28 16:30:12  skreddy
 * CASE201112/CR201113/LOG201121
 * issues related to invalid lot
 *
 * Revision 1.3.4.4.4.2  2012/11/01 14:54:57  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.3.4.4.4.1  2012/10/29 12:10:41  mbpragada
 * 20120490
 *
 * Revision 1.3.4.4  2012/09/03 13:56:34  schepuri
 * CASE201112/CR201113/LOG201121
 * added date stamp
 *
 * Revision 1.3.4.3  2012/05/14 12:04:56  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Picking Stageloc
 *
 * Revision 1.2.2.15  2012/05/07 09:54:38  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.2.2.14  2012/05/04 22:52:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.2.2.13  2012/04/20 22:52:20  gkalla
 * CASE201112/CR201113/LOG201121
 * To resolve selected into wave
 *
 * Revision 1.2.2.12  2012/04/20 14:30:50  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.2.2.11  2012/04/17 08:40:17  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pool of records
 *
 * Revision 1.2.2.10  2012/03/20 20:42:32  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Moved from Trunk to Branch
 *
 * Revision 1.14  2012/03/20 18:05:54  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Wave Creation by sales order header
 *
 * Revision 1.13  2012/03/16 10:01:52  schepuri
 * CASE201112/CR201113/LOG201121
 * code merge from 1.2.2.9
 *
 * Revision 1.12  2012/03/16 06:34:16  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Fine tunned
 *
 * Revision 1.11  2012/03/14 07:07:41  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Report changes  and undefined container lp issue for TPP
 *
 * Revision 1.10  2012/03/14 07:06:01  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Report changes  and undefined container lp issue for TPP
 *
 * Revision 1.9  2012/03/13 12:31:06  schepuri
 * CASE201112/CR201113/LOG201121
 * code merge from 1.2.2.6
 *
 * Revision 1.8  2012/03/09 10:47:01  rmukkera
 * CASE201112/CR201113/LOG201121
 * added wmscarrier for to
 *
 * Revision 1.7  2012/03/09 08:30:40  rmukkera
 * CASE201112/CR201113/LOG201121
 * Paging functionality added newly.
 *
 * Revision 1.6  2012/03/02 01:11:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.5  2012/02/23 17:58:59  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.4  2012/02/01 15:10:30  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Wave Creation User Event
 *
 * Revision 1.3  2012/01/27 06:53:03  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Generation UE
 *
 * Revision 1.2  2012/01/05 14:01:49  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code tunning
 *
 * Revision 1.1  2011/12/21 16:45:13  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pick Gen User Event
 *  
 *
 *****************************************************************************/
function DekittingSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('De Kitting');
		form.setScript('customscript_ebiz_dekit_cl');
		var item = form.addField('cusppage_item', 'multiselect', 'Assembly Item', 'custom').setMandatory(true);

		//var itemdata = nlapiSearchRecord('item', null, new nlobjSearchFilter('type', null, 'is', 'Assembly'), [new nlobjSearchColumn('internalid'), new nlobjSearchColumn('itemid')]);
		FillItem(form, item,'-1');


//		for(i=0;i<itemdata.length;i++)
//		{
//		item.addSelectOption(itemdata[i].getValue("internalid"),itemdata[i].getValue("itemid"));
//		}
//		if(itemdata!=null && itemdata.length>=1000)
//		{
//		var maxno=itemdata[itemdata.length-1].getValue('id');		
//		FillWave(form, WaveField,maxno);	
//		}
		nlapiLogExecution('ERROR', 'after Fillg','done');
		item.setLayoutType('normal', 'startrow');
		//case# 20148654 starts
		//form.addSubmitButton('De Kit');
		form.addSubmitButton('Display');
		//case# 20148654 ends
		response.writePage(form);
	}
	else
	{
		var form = nlapiCreateForm('De Kitting');
		form.setScript('customscript_ebiz_dekit_cl');
		//var selecteditem = form.addField('cusppage_selecteditem', 'text', 'Assembly Item :').setDisplayType('inline');

		//selecteditem.setLayoutType('outside', 'startrow');
		nlapiLogExecution('ERROR', 'request.getParameter(cusppage_item)', request.getParameter('cusppage_item'));

		var filters = new Array(); 
		filters[0] = new nlobjSearchFilter('internalid', null, 'is', request.getParameter('cusppage_item'));//kit_id is the parameter name 
		filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');
		var columns1 = new Array(); 
		columns1[0] = new nlobjSearchColumn( 'memberitem' ); 
		columns1[1] = new nlobjSearchColumn( 'memberquantity' );
		columns1[2] = new nlobjSearchColumn( 'itemid' );

		var searchresults = nlapiSearchRecord( 'assemblyitem', null, filters, columns1 );
		var selectedItemArray=new Array();
		if(searchresults!=null)
		{

			nlapiLogExecution('ERROR', 'searchresults', searchresults.length);
			var count=1; var sno=1;		var componentitemsstring='';
			for(var k=0;k<searchresults.length;k++)
			{


				if(selectedItemArray.indexOf(searchresults[k].getValue('itemid'))==-1)
				{
					componentitemsstring='';
					selectedItemArray[selectedItemArray.length]=searchresults[k].getValue('itemid');
					var item = form.addField('cusppage_selecteditem'+selectedItemArray.length, 'label', '<span style ="font-size:13px;color:black;font-weight:bold;text-decoration:underline;">Assembly Item: </span><span style ="font-size:13px;color:black;">&nbsp;&nbsp;'+searchresults[k].getValue('itemid')+'</span>').setDisplayType('inline');
					//var item1 =
					item.setLayoutType('outside', 'startrow');
					//item.setDefaultValue(searchresults[k].getValue('itemid'));
					var comitem= form.addField('cusppage_componentitem'+selectedItemArray.length, 'label', '<span style ="font-size:13px;color:black;font-weight:bold;text-decoration:underline;">Component Item Details </span>');
					sno=1;
					comitem.setLayoutType('outside', 'startrow');
					componentitemsstring="<table cellpadding='0' cellspacing='2' style='border:1px solid black;'><tr><td style ='font-size:12px;color:black;font-weight:bold;border-right:1px solid black;border-bottom:1px solid black;'>S.No.</td> <td style ='font-size:12px;color:black;font-weight:bold;text-align:center;border-right:1px solid black;border-bottom:1px solid black;'>Item</td> <td style ='font-size:12px;color:black;font-weight:bold;border-bottom:1px solid black;'>Qty</td> </tr>";
					componentitemsstring+="<tr><td style ='font-size:12px;color:black;border-right:1px solid black;'>"+sno.toString()+"</td><td style ='font-size:12px;color:black;border-right:1px solid black;'>  "+searchresults[k].getText('memberitem')+" </td>   <td style ='font-size:12px;color:black;'> "+searchresults[k].getValue('memberquantity')+"</td></tr>";
					var fieldid="cusppage_itemdetails"+count;
					nlapiLogExecution('ERROR', 'fieldid',fieldid);
					//	var comitemdetails= form.addField(fieldid, 'label', componentitemsstring).setDisplayType('inline');
					//	comitemdetails.setLayoutType('outside', 'startrow');
					count=count+1;
					sno=sno+1;


				}
				else
				{
					if(searchresults[k+1]!=null)
					{
						if(selectedItemArray.indexOf(searchresults[k+1].getValue('itemid'))==-1)
						{
							componentitemsstring+="<tr><td style ='font-size:12px;color:black;border-right:1px solid black;'>"+sno.toString()+". </td><td style ='font-size:12px;color:black;border-right:1px solid black;'>  "+searchresults[k].getText('memberitem')+" </td> <td style ='font-size:12px;color:black;'>   "+searchresults[k].getValue('memberquantity')+"</td></tr></table>";
							var fieldid="cusppage_itemdetails"+count;
							nlapiLogExecution('ERROR', 'fieldid',fieldid);
							var comitemdetails= form.addField(fieldid, 'label', componentitemsstring).setDisplayType('inline');
							comitemdetails.setLayoutType('outside', 'startrow');
							count=count+1;
							sno=sno+1;
						}
						else
						{
							componentitemsstring+="<tr><td style ='font-size:12px;color:black;border-right:1px solid black;'>"+sno.toString()+". </td><td style ='font-size:12px;color:black;border-right:1px solid black;'>  "+searchresults[k].getText('memberitem')+" </td> <td style ='font-size:12px;color:black;'>   "+searchresults[k].getValue('memberquantity')+"</td></tr>";

							count=count+1;
							sno=sno+1;
						}
					}
					else
					{
						componentitemsstring+="<tr><td style ='font-size:12px;color:black;border-right:1px solid black;'>"+sno.toString()+". </td><td style ='font-size:12px;color:black;border-right:1px solid black;'>  "+searchresults[k].getText('memberitem')+" </td> <td style ='font-size:12px;color:black;'>   "+searchresults[k].getValue('memberquantity')+"</td></tr></table>";
						var fieldid="cusppage_itemdetails"+count;
						nlapiLogExecution('ERROR', 'fieldid',fieldid);
						var comitemdetails= form.addField(fieldid, 'label', componentitemsstring).setDisplayType('inline');
						comitemdetails.setLayoutType('outside', 'startrow');
						count=count+1;
						sno=sno+1;
					}
				}




			}

		}

		form.addSubmitButton('De Kit');  

		var item = form.addField('cusppage_item', 'multiselect', 'Assembly Item', 'custom').setDisplayType('hidden');
		item.setDefaultValue(request.getParameter('cusppage_item'));
		var ItemDTKSubList = form.addSubList("custpage_items", "list", "Items List");
		ItemDTKSubList.addMarkAllButtons();
		ItemDTKSubList.addField("custpage_items_select", "checkbox", "Confirm").setDefaultValue('F');;
		ItemDTKSubList.addField("custpage_items_sku", "text", "Ass Item", 'item').setDisplayType('inline');
		ItemDTKSubList.addField("custpage_items_binloc", "text", "BinLocation").setDisplayType('inline');
		ItemDTKSubList.addField("custpage_items_qty", "text", "Ass Item Qty Av").setDisplayType('inline');
		ItemDTKSubList.addField("custpage_items_dekitqty", "text", "Dekit Qty").setDisplayType('entry');
		ItemDTKSubList.addField("custpage_items_lotdisplay", "text", "Lot#").setDisplayType('inline');
		ItemDTKSubList.addField("custpage_items_wo", "text", "WO#").setDisplayType('inline');
		ItemDTKSubList.addField("custpage_items_remarks", "textarea", "Remarks").setDisplayType('inline');	
		
		ItemDTKSubList.addField("custpage_items_invtid", "text", "Invtid").setDisplayType('hidden');
		ItemDTKSubList.addField("custpage_items_remarks1", "textarea", "Remarks1").setDisplayType('hidden');
		ItemDTKSubList.addField("custpage_items_skuno", "text", "ass item id").setDisplayType('hidden');
		ItemDTKSubList.addField("custpage_items_sku_status", "text", "skustatus").setDisplayType('hidden');
		ItemDTKSubList.addField("custpage_items_packcode", "text", "packcode").setDisplayType('hidden');
		ItemDTKSubList.addField("custpage_items_lot", "text", "lot").setDisplayType('hidden');
		ItemDTKSubList.addField("custpage_items_lotid", "text", "lotID").setDisplayType('hidden');
		ItemDTKSubList.addField("custpage_items_adjttype", "text", "adjttype").setDisplayType('hidden');
		ItemDTKSubList.addField("custpage_items_accountno", "text", "accountNo").setDisplayType('hidden');
		ItemDTKSubList.addField("custpage_items_location", "text", "Location").setDisplayType('hidden');
		ItemDTKSubList.addField("custpage_items_wonumber", "text", "WO#number").setDisplayType('hidden');
		
		var lineCnt = request.getLineItemCount('custpage_items');
		nlapiLogExecution('ERROR', 'lineCnt', lineCnt);
		if(lineCnt!=-1)
		{


			for (var k = 1; k <= lineCnt; k++) {
				var chkselect=request.getLineItemValue('custpage_items','custpage_items_select', k);
				nlapiLogExecution('ERROR', 'chkselect', chkselect);

				if(chkselect=='T')
				{
//					Create Dekit records in Opentask with task type as DKIT
					var remarks= request.getLineItemValue('custpage_items','custpage_items_remarks1', k);
					var expectedskuno= request.getLineItemValue('custpage_items','custpage_items_skuno', k);
					nlapiLogExecution('ERROR', 'remarks', remarks);
					var tempRemarksArray=new Array();
					tempRemarksArray=remarks.split(',');
					var invtId=request.getLineItemValue('custpage_items','custpage_items_invtid', k);
					var dekiqty=request.getLineItemValue('custpage_items','custpage_items_dekitqty', k);
					var skustatus=request.getLineItemValue('custpage_items','custpage_items_sku_status', k);
					var lot=request.getLineItemValue('custpage_items','custpage_items_lot', k);
					var lotID=request.getLineItemValue('custpage_items','custpage_items_lotid', k);
					var adjttype=request.getLineItemValue('custpage_items','custpage_items_adjttype', k);
					var accno=request.getLineItemValue('custpage_items','custpage_items_accountno', k);
					var loc=request.getLineItemValue('custpage_items','custpage_items_location', k);
					var packcode=request.getLineItemValue('custpage_items','custpage_items_packcode', k);
					var WOnumber=request.getLineItemValue('custpage_items','custpage_items_wonumber', k);
					//case# 20148586 starts
					var assskutext=request.getLineItemValue('custpage_items','custpage_items_sku', k);
					//case# 20148586 ends
					for(var j=0;j<tempRemarksArray.length;j++)
					{
						var compponentItemArray=tempRemarksArray[j].split(':');
						var componentItem=compponentItemArray[0];
						var componentItemQty=compponentItemArray[1];
						var vCompItemType = compponentItemArray[2];
						nlapiLogExecution('ERROR', 'componentItem', componentItem);
						nlapiLogExecution('ERROR', 'componentItemQty', componentItemQty);
						nlapiLogExecution('ERROR', 'dekiqty', dekiqty);
					//	componentItemQty=parseFloat(componentItemQty) - parseFloat(dekiqty);
						componentItemQty=parseFloat(componentItemQty) * parseFloat(dekiqty);
					//	var CompItemType = nlapiLookupField('item', componentItem, 'recordType');
						/*var vCompItemType;
						var vfilter=new Array();						
						vfilter[0]=new nlobjSearchFilter("internalid",null,"anyof",componentItem);
						var vcolumn=new Array();
						vcolumn[0]=new nlobjSearchColumn("type");
						vcolumn[1]=new nlobjSearchColumn("custitem_ebizserialin");
						vcolumn[2]=new nlobjSearchColumn("custitem_ebizserialout");
						vcolumn[3]=new nlobjSearchColumn("custitem_ebizbatchlot");
						var searchres=nlapiSearchRecord("item",null,vfilter,vcolumn);
						if(searchres!=null&&searchres!="")
						{
							CompItemType = searchres[0].recordType;
						}*/
						
						//nlapiLogExecution('ERROR','CompItemType',CompItemType);
						if(CompItemType != 'serviceitem' && CompItemType != 'noninventoryitem')
						{	
							var getItemLP = GetMaxLPNo(1, 1,loc);
							nlapiLogExecution('ERROR', 'getItemLP', getItemLP);
						createOpenTaskRecord(componentItem,componentItemQty,expectedskuno,getItemLP,skustatus,packcode,lot);
						var notes='dekitting';var tasktype=11;
						createinventoryRecord(invtId,componentItemQty,getItemLP,packcode,componentItem,skustatus,loc,dekiqty,notes,tasktype,adjttype,lot,WOnumber);
						}
					}


					updateCreateInventoryAssemblyItem(invtId,dekiqty,expectedskuno,skustatus,loc,dekiqty,notes,tasktype,adjttype,lot,form,assskutext);


				}
			}
			//case# 20149402 starts
			showInlineMessage(form, 'Confirmation', 'Assembly Item Dekitted successfully', "");
			//case# 20149402 ends
		}
//		else
//		{
			var skunos=request.getParameterValues('cusppage_item');
			var skuarray=new Array();
			for(var k=0;k<skunos.length;k++)
				{
				skuarray[skuarray.length]=skunos[k];
				}
			nlapiLogExecution('ERROR', 'skuarray', skuarray.length);
			var vRoleLocation = getRoledBasedLocationNew();
			nlapiLogExecution('ERROR', 'vRoleLocation', vRoleLocation);
			var invtfilters = new Array(); 
			invtfilters[0] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof',  skuarray);//kit_id is the parameter name 
			invtfilters[1] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19']);
			invtfilters[2] = new nlobjSearchFilter('custrecord_ebiz_avl_qty', null, 'greaterthan', 0);
			if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
			{
				invtfilters[3] = new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', vRoleLocation);
			}
							
			var invcolumns = new Array(); 
			invcolumns[0] = new nlobjSearchColumn( 'custrecord_ebiz_avl_qty' ); 
			invcolumns[1] = new nlobjSearchColumn( 'custrecord_ebiz_inv_binloc' );
			invcolumns[2] = new nlobjSearchColumn( 'custrecord_inv_ebizsku_no' );
			invcolumns[3] = new nlobjSearchColumn( 'custrecord_ebiz_inv_sku' );
			invcolumns[4] = new nlobjSearchColumn('custrecord_ebiz_inv_sku_status');
			invcolumns[5] = new nlobjSearchColumn('custrecord_ebiz_inv_packcode');
			invcolumns[6] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
			invcolumns[7] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');
			invcolumns[8] = new nlobjSearchColumn('custrecord_ebiz_inv_fifo');
			invcolumns[9] = new nlobjSearchColumn('custrecord_ebiz_inv_adjusttype');
			invcolumns[10] = new nlobjSearchColumn('custrecord_ebiz_inv_account_no');
			invcolumns[11] = new nlobjSearchColumn('custrecord_ebiz_inv_loc');
			invcolumns[12] = new nlobjSearchColumn('custrecord_ebiz_transaction_no');
			var invtsearchresults = nlapiSearchRecord( 'customrecord_ebiznet_createinv', null, invtfilters, invcolumns );
			if(invtsearchresults!=null)
			{
				for(var m=0;m<invtsearchresults.length;m++)
				{
					var searchresult = invtsearchresults[m];
					vebizskuno = searchresult.getText('custrecord_ebiz_inv_sku');
					vqty = searchresult.getValue('custrecord_ebiz_avl_qty');
					vbinloc = searchresult.getText('custrecord_ebiz_inv_binloc');
					vitemstatus = searchresult.getValue('custrecord_ebiz_inv_sku_status');
					vpackcode = searchresult.getValue('custrecord_ebiz_inv_packcode');
					vlot= searchresult.getText('custrecord_ebiz_inv_lot');
					var vlotID= searchresult.getValue('custrecord_ebiz_inv_lot');
					vadjttype= searchresult.getText('custrecord_ebiz_inv_adjusttype');
					vaccountno = searchresult.getValue('custrecord_ebiz_inv_account_no');
					vloc=searchresult.getValue('custrecord_ebiz_inv_loc');
					form.getSubList('custpage_items').setLineItemValue('custpage_items_sku', m + 1, vebizskuno);
					form.getSubList('custpage_items').setLineItemValue('custpage_items_qty', m + 1, vqty);
					form.getSubList('custpage_items').setLineItemValue('custpage_items_dekitqty', m + 1, vqty);
					form.getSubList('custpage_items').setLineItemValue('custpage_items_binloc', m + 1, vbinloc);
					var remarks='';var remarks1='';
					var vWONo;
					var vWOnumber='';
					if(searchresult.getText('custrecord_ebiz_transaction_no') != null && searchresult.getText('custrecord_ebiz_transaction_no') != '')
					{
						vWONo=searchresult.getText('custrecord_ebiz_transaction_no');
						vWOnumber=searchresult.getValue('custrecord_ebiz_transaction_no');
					}
					for(var k=0;k<searchresults.length;k++)
					{
						var componentqty=searchresults[k].getValue('memberquantity');
						componentqty=parseFloat(componentqty)*(vqty) ;
						nlapiLogExecution('ERROR', 'searchresult.getValue(custrecord_ebiz_inv_sku)', searchresult.getValue('custrecord_ebiz_inv_sku'));
						nlapiLogExecution('ERROR', 'searchresults[k].getValue(memberitem)', searchresults[k].getId());
						if(searchresult.getValue('custrecord_ebiz_inv_sku')==searchresults[k].getId())
						{
							var CompItemType='';
							var vComItemid =searchresults[k].getId();
							var vfilter=new Array();						
							vfilter[0]=new nlobjSearchFilter("internalid",null,"anyof",vComItemid);
							var vcolumn=new Array();
							vcolumn[0]=new nlobjSearchColumn("type");
							vcolumn[1]=new nlobjSearchColumn("custitem_ebizserialin");
							vcolumn[2]=new nlobjSearchColumn("custitem_ebizserialout");
							vcolumn[3]=new nlobjSearchColumn("custitem_ebizbatchlot");
							var searchres=nlapiSearchRecord("item",null,vfilter,vcolumn);
							if(searchres!=null&&searchres!="")
							{
								CompItemType = searchres[0].recordType;
							}
								remarks=remarks+searchresults[k].getText('memberitem')+":  "+componentqty+" ,";
							remarks1=remarks1+searchresults[k].getValue('memberitem')+":  "+searchresults[k].getValue('memberquantity')+":"+CompItemType+" ,";
						}
					}
					
					remarks=remarks.substring(0,remarks.length-1);
					remarks1=remarks1.substring(0,remarks1.length-1);
					nlapiLogExecution('ERROR', 'custpage_items_invtid', searchresult.getId());
					nlapiLogExecution('ERROR', 'vWOnumber',vWOnumber);
					form.getSubList('custpage_items').setLineItemValue('custpage_items_remarks', m + 1, remarks);
					form.getSubList('custpage_items').setLineItemValue('custpage_items_remarks1', m + 1, remarks1);
					form.getSubList('custpage_items').setLineItemValue('custpage_items_invtid', m + 1, searchresult.getId());
					form.getSubList('custpage_items').setLineItemValue('custpage_items_skuno', m + 1, searchresult.getValue('custrecord_ebiz_inv_sku'));
					
					form.getSubList('custpage_items').setLineItemValue('custpage_items_sku_status', m + 1, vitemstatus);
					form.getSubList('custpage_items').setLineItemValue('custpage_items_packcode', m + 1, vpackcode);
					form.getSubList('custpage_items').setLineItemValue('custpage_items_lot', m + 1, vlot);
					form.getSubList('custpage_items').setLineItemValue('custpage_items_lotid', m + 1, vlotID);
					form.getSubList('custpage_items').setLineItemValue('custpage_items_adjttype', m + 1, vadjttype);
					form.getSubList('custpage_items').setLineItemValue('custpage_items_accountno', m + 1, vaccountno);
					form.getSubList('custpage_items').setLineItemValue('custpage_items_location', m + 1, vloc);
					form.getSubList('custpage_items').setLineItemValue('custpage_items_lotdisplay', m + 1, vlot);
					form.getSubList('custpage_items').setLineItemValue('custpage_items_wo', m + 1, vWONo);
					form.getSubList('custpage_items').setLineItemValue('custpage_items_wonumber', m + 1, vWOnumber);
				
				}
			}
		//}

		//case# 20149234 starts	
		form.setScript('customscript_ebiz_dekit_cl');	
		//case# 20149234 ends
		response.writePage(form);
	}
}
function createOpenTaskRecord(sku,qty,expecteditem,getItemLP,skustatus,packcode,lot)
{
	var now = new Date();

	var a_p = "";
	var d = new Date();
	var curr_hour = now.getHours();
	if (curr_hour < 12) {
		a_p = "am";
	}
	else {
		a_p = "pm";
	}
	if (curr_hour == 0) {
		curr_hour = 12;
	}
	if (curr_hour > 12) {
		curr_hour = curr_hour - 12;
	}

	var curr_min = now.getMinutes();

	curr_min = curr_min + "";

	if (curr_min.length == 1) {
		curr_min = "0" + curr_min;
	}
	nlapiLogExecution("ERROR", "Time of Day ", (curr_hour + " : " + curr_min + " " + a_p));
	var openTaskRec = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
	nlapiLogExecution('ERROR', 'CreATIN openTaskRec  REc ', 'DKIT');
	openTaskRec.setFieldValue('custrecordact_begin_date', DateStamp());
	openTaskRec.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
	openTaskRec.setFieldValue('custrecord_tasktype', 12);//DKIT  
	openTaskRec.setFieldValue('custrecord_ebiz_sku_no', sku);//
	openTaskRec.setFieldValue('custrecord_sku', sku);//
	openTaskRec.setFieldValue('custrecord_act_qty', parseFloat(qty).toFixed(5));
	openTaskRec.setFieldValue('custrecord_sku_status', skustatus);
	openTaskRec.setFieldValue('custrecord_packcode', parseFloat(packcode));
	openTaskRec.setFieldValue('custrecord_lpno', getItemLP);
	openTaskRec.setFieldValue('custrecord_ebiz_expected_item', expecteditem);
	openTaskRec.setFieldValue('custrecord_batch_no', lot);
	var opentaskid=  nlapiSubmitRecord(openTaskRec, false, true);
	nlapiLogExecution('ERROR', 'opentaskid', opentaskid);

}

function updateCreateInventoryAssemblyItem(invtId,dekitQty,varmembersku,skustatus,loc,dekiqty,notes,tasktype,adjttype,lot,form,assskutext)
{
	nlapiLogExecution('ERROR', 'into update updateCreateInventoryAssemblyItem', invtId);
	var invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invtId);

	var binloc=invttran.getFieldValue('custrecord_ebiz_inv_binloc');
	var qoh=invttran.getFieldValue('custrecord_ebiz_qoh');
	nlapiLogExecution('ERROR', 'qoh ', qoh);
	nlapiLogExecution('ERROR', 'dekitQty ', dekitQty);

	var newQoh=parseFloat(qoh)-parseFloat(dekitQty);
	nlapiLogExecution('ERROR', 'newQoh ', newQoh);
	if(newQoh >=0)
	invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(newQoh).toFixed(5));//	

	var avalqty=invttran.getFieldValue('custrecord_ebiz_avl_qty');
	nlapiLogExecution('ERROR', 'avalqty ', avalqty);
	nlapiLogExecution('ERROR', 'dekitQty ', dekitQty);
	/*var newavalqty=parseFloat(avalqty)-parseFloat(dekitQty);
	invttran.setFieldValue('custrecord_ebiz_avl_qty',parseFloat(newavalqty).toFixed(5));//	
*/
	var invtid=  nlapiSubmitRecord(invttran, false, true);
	if(invtid!=null && invtid!='')
	{
		//case# 20148562 (dekiqty is passing positive because it is throwing error in InvokeNSInventoryAdjustment function when advancedbinmanagement is enabled)
		//InvokeNSInventoryAdjustment(varmembersku,skustatus,loc,-(dekiqty),notes,tasktype,adjttype,lot);
		// when doing dekiting we need to decrement the main item qauntity in NS, SO we need to send the -Ve quantity
		InvokeNSInventoryAdjustment(varmembersku,skustatus,loc,-(dekiqty),notes,tasktype,adjttype,lot);
		//case# 20148586 starts
		//showInlineMessage(form, 'Confirmation', 'Item  ' + assskutext + '  Dekitted successfully', "");
		//case# 20148586 ends
	}
	nlapiLogExecution('ERROR', 'updated createinvt id ', invtid);

}
function createinventoryRecord(invtId,dekitQty,getItemLP,packcode,varmembersku,skustatus,loc,dekiqty,notes,tasktype,adjttype,lot,WOnumber,lotID)
{

	var opntsktran = nlapiLoadRecord('customrecord_ebiznet_createinv', invtId);

	var binloc=opntsktran.getFieldValue('custrecord_ebiz_inv_binloc');
	var MemberLotId="";
	if(lotID!=null&&lotID!="")
	{
		var rec=nlapiCopyRecord("customrecord_ebiznet_batch_entry",lotID);
		rec.setFieldValue('name', lot); 
		rec.setFieldValue('custrecord_ebizsku', varmembersku); 
		MemberLotId = nlapiSubmitRecord(rec, false, true);
		nlapiLogExecution('ERROR', 'MemberLotId ', MemberLotId);
	}

	
	var LineLotNo = '';
	// to fetch LOT# for component Item
	if(WOnumber !=null && WOnumber !='' && varmembersku !=null && varmembersku !='')
	{

		var filterOpentask = new Array();
		filterOpentask[0] = new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', WOnumber);
		filterOpentask[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]); 
		filterOpentask[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]);
		filterOpentask[3] = new nlobjSearchFilter('custrecord_sku', null, 'anyof', varmembersku);

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_line_no');
		columns[1] = new nlobjSearchColumn('custrecord_sku');
		columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[3] = new nlobjSearchColumn('custrecord_lpno');
		columns[4] = new nlobjSearchColumn('custrecord_batch_no');
		columns[5] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[6] = new nlobjSearchColumn('custrecord_act_qty');                                    
		columns[7] = new nlobjSearchColumn('custrecord_expirydate');
		columns[8] = new nlobjSearchColumn('custrecord_actendloc');
		columns[9] = new nlobjSearchColumn('custrecord_invref_no');
		var Opentaskresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterOpentask,columns);

		if(Opentaskresults!=null && Opentaskresults!='') 
		{ 	
			nlapiLogExecution('ERROR', 'Opentaskresults.length',Opentaskresults.length);  
			LineLotNo=Opentaskresults[0].getValue('custrecord_batch_no');
			nlapiLogExecution('ERROR', 'LineLotNo',LineLotNo); 	
			var vActEndLoc=Opentaskresults[0].getValue('custrecord_actendloc');
			nlapiLogExecution('ERROR', 'Actual End Location',vActEndLoc);
			var vActBeginLoc=Opentaskresults[0].getValue('custrecord_actbeginloc');
			nlapiLogExecution('ERROR', 'Actual Begin Location',vActBeginLoc);
			if(vActEndLoc == null || vActEndLoc == '')
				vActEndLoc=vActBeginLoc;			
			var vCompLP=Opentaskresults[0].getValue('custrecord_lpno');
			nlapiLogExecution('ERROR', 'vCompLP',vCompLP);
			var vInvtRef=Opentaskresults[0].getValue('custrecord_invref_no');
			nlapiLogExecution('ERROR', 'invref',vInvtRef);
			
			var filterbatch = new Array();
			filterbatch[0] = new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is', LineLotNo);
			filterbatch[1] = new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', varmembersku);

			var columsbatch =new Array();
			columsbatch [0] = new nlobjSearchColumn('custrecord_ebizexpirydate');  
			columsbatch [1] = new nlobjSearchColumn('custrecord_ebizfifodate');

			var batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry',null,filterbatch,columsbatch );
			var lotid = '';
			var expdate = '';
			var fifodate = '';
			
			if(batchsearchresults !=null && batchsearchresults !='')
			{
				 lotid = batchsearchresults [0].getId();
				 expdate = batchsearchresults [0].getValue('custrecord_ebizexpirydate');
				 fifodate = batchsearchresults [0].getValue('custrecord_ebizfifodate');
			}
			
			nlapiLogExecution('ERROR', 'lotid',lotid);

			nlapiLogExecution('ERROR', 'expdate',expdate); 
			nlapiLogExecution('ERROR', 'varmembersku',varmembersku); 
			nlapiLogExecution('ERROR', 'skustatus',skustatus); 
			nlapiLogExecution('ERROR', 'loc',loc); 
			nlapiLogExecution('ERROR', 'lotid',lotid); 
			nlapiLogExecution('ERROR', 'vInvtRef',vInvtRef); 
			
			
			var filters = new Array();
			if(vCompLP!=null && vCompLP!='' )
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', vCompLP));			
			
			if(varmembersku!=null && varmembersku!='')
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', varmembersku));
			
			if(skustatus!=null && skustatus!='' )
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', skustatus));	
			
			if(loc!=null&&loc!="")
			{				
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', loc));				
				
			}			
			nlapiLogExecution('ERROR', 'lotid',lotid);
			if(lotid!=null&&lotid!="")
			{
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', lotid));
			}
			nlapiLogExecution('ERROR', 'vActBeginLoc',vActBeginLoc);
			if(vActBeginLoc!=null&&vActBeginLoc!="")
			{
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', vActBeginLoc));
				
			}
			
			nlapiLogExecution('ERROR', 'fifodate',fifodate);
			if(fifodate!=null&&fifodate!="")
			{
				filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_fifo',  null, 'on', fifodate));
				
			}
				
			
			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh'); 
			columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_lp'); 
			var inventorysearch = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
			var invExistQty=0;
			if(inventorysearch != null && inventorysearch != '' && inventorysearch.length>0)
			{
				
				if(vInvtRef == null || vInvtRef =='' || vInvtRef =='null')
					vInvtRef= inventorysearch[0].getId();
				nlapiLogExecution('ERROR', 'vInvtRef', vInvtRef);
				try
				{
					var scount=1;
					LABL1: for(var i=0;i<scount;i++)
					{	
						nlapiLogExecution('ERROR', 'CUSTOM_RECORD_COLLISION', i);
						try
						{

							var invLoad = nlapiLoadRecord('customrecord_ebiznet_createinv', vInvtRef);
							invExistQty= invLoad.getFieldValue('custrecord_ebiz_qoh');
							var updatedInventory = parseFloat(invExistQty) + parseFloat(dekitQty);
							nlapiLogExecution('ERROR', 'invExistQty', invExistQty);
							nlapiLogExecution('ERROR', 'dekitQty', dekitQty);
							nlapiLogExecution('ERROR', 'updatedInventory', updatedInventory);
							invLoad.setFieldValue('custrecord_ebiz_qoh', parseFloat(updatedInventory).toFixed(5));		
							invLoad.setFieldValue('custrecord_updated_user_no', nlapiGetContext().getUser());
							invLoad.setFieldValue('custrecord_ebiz_displayfield','N');	
							invLoad.setFieldValue('custrecord_ebiz_callinv', 'N');

							invtrecid = nlapiSubmitRecord(invLoad, false, true);
							nlapiLogExecution('ERROR', 'Inventory Merged to the existing LP',vCompLP);


						}
						catch(ex)
						{
							var exCode='CUSTOM_RECORD_COLLISION'; 
							var wmsE='Inventory record being updated by another user. Please try again...';
							if (ex instanceof nlobjError) 
							{	
								wmsE=ex.getCode() + '\n' + ex.getDetails();
								exCode=ex.getCode();
							}
							else
							{
								wmsE=ex.toString();
								exCode=ex.toString();
							}  
							
							nlapiLogExecution('ERROR', 'Exception in RF Inv move : ', wmsE);
							if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
							{
								scount=scount+1;
								continue LABL1;
							}
							else break LABL1;
						}
					}
				}
				catch(exp)
				{
					nlapiLogExecution('ERROR','exception in Dekitting',exp.message);
					
				}
					

			}
			else
			{
				//Creating Inventory Record.
				var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');
				nlapiLogExecution('ERROR', 'CreATIN INVT  REc ', 'INVT');
				nlapiLogExecution('ERROR', 'WOnumber ', WOnumber);
				nlapiLogExecution('ERROR', 'varmembersku ', varmembersku);
				nlapiLogExecution('ERROR', 'dekitQty ', dekitQty);
				invtRec.setFieldValue('custrecord_ebiz_inv_binloc', binloc);
				invtRec.setFieldValue('custrecord_ebiz_inv_lp', getItemLP);
				invtRec.setFieldValue('custrecord_ebiz_inv_sku', varmembersku);
				invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', skustatus);
				invtRec.setFieldValue('custrecord_ebiz_inv_packcode', parseFloat(packcode));
				invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(dekitQty).toFixed(5));
				invtRec.setFieldValue('custrecord_inv_ebizsku_no', varmembersku);
				invtRec.setFieldValue('custrecord_ebiz_qoh',parseFloat(dekitQty).toFixed(5));
				//invtRec.setFieldValue('custrecord_ebiz_itemdesc', varmemberskutext);
				invtRec.setFieldValue('custrecord_invttasktype', 12);
				invtRec.setFieldValue('custrecord_wms_inv_status_flag', 19); //Inventory S 
				//invtRec.setFieldValue('custrecord_ebiz_inv_account_no', 1); // case# 201412321
				invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
				invtRec.setFieldValue('custrecord_ebiz_inv_loc', loc);
				//if(vCompLP != null && vCompLP != '')
				//	invtRec.setFieldValue('custrecord_ebiz_inv_lp', vCompLP);

				if(vActEndLoc != null && vActEndLoc != '')
					invtRec.setFieldValue('custrecord_ebiz_inv_binloc', vActEndLoc);
				if(lotid!=null && lotid!='')
					invtRec.setFieldValue('custrecord_ebiz_inv_lot', lotid);
				if(expdate !=null && expdate !='')
					invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);

				if(fifodate !=null && fifodate !='')
					invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifodate);


				nlapiLogExecution('ERROR', 'Before Submitting invtrecid', 'INVTRECORDS');
				var invtrecid = nlapiSubmitRecord(invtRec, false, true);


			}


		}

	}
	
	//invtRec.setFieldValue('custrecord_ebiz_inv_lot', lot);
	
	if(invtrecid!=null && invtrecid!='')
	{
		//var lot1='';

		//InvokeNSInventoryAdjustment(varmembersku,skustatus,loc,dekiqty,notes,tasktype,adjttype,lot);
		//passing lot of component item
		if(LineLotNo==null || LineLotNo=='')
			LineLotNo=lot;
		InvokeNSInventoryAdjustment(varmembersku,skustatus,loc,dekitQty,notes,tasktype,adjttype,LineLotNo);
	}
}
if (!Array.prototype.indexOf)
{
	Array.prototype.indexOf = function(elt /*, from*/)
	{
		var len = this.length;

		var from = Number(arguments[1]) || 0;
		from = (from < 0)
		? Math.ceil(from)
				: Math.floor(from);
		if (from < 0)
			from += len;

		for (; from < len; from++)
		{
			if (from in this &&
					this[from] === elt)
				return from;
		}
		return -1;
	};
}

function FillItem(form, ItemField,maxno){
	nlapiLogExecution('ERROR', 'Before FillItem', 'done');
	var filters = new Array();
	filters.push(new nlobjSearchFilter('type', null, 'is', 'Assembly'));  // Pick task

	if(maxno!=null && maxno!='' && maxno!='-1')
	{
		nlapiLogExecution('ERROR', 'into maxno', maxno);
		filters.push(new nlobjSearchFilter('internalidnumber', null, 'greaterthan', parseFloat(maxno)));
	}

	var column = new Array();
	column.push(new nlobjSearchColumn('internalid').setSort());		
	column.push(new nlobjSearchColumn('itemid'));

	ItemField.addSelectOption("", "");


	var searchresults = nlapiSearchRecord('item', null, filters,column);

	if(searchresults!=null)	
		nlapiLogExecution('ERROR', 'searchresults.length', searchresults.length);

	for (var i = 0; searchresults != null && i < searchresults.length; i++) {

		if(searchresults[i].getValue('itemid') != null && searchresults[i].getValue('itemid') != "" && searchresults[i].getValue('itemid') != " ")
		{
			var resdo = form.getField('cusppage_item').getSelectOptions(searchresults[i].getValue('itemid'), 'is');
			if (resdo != null) {
				if (resdo.length > 0) {
					continue;
				}
			}
		}
		ItemField.addSelectOption(searchresults[i].getValue('internalid'), searchresults[i].getValue('itemid'));
	}


	if(searchresults!=null && searchresults.length>=1000)
	{
		nlapiLogExecution('ERROR', 'more than 1000 ', 'done');
		var maxno=searchresults[searchresults.length-1].getValue('internalid');		
		FillItem(form, ItemField,maxno);	
	}
	nlapiLogExecution('ERROR', 'after FillItem', 'done');
}
