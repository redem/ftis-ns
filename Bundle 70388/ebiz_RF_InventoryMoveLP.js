/***************************************************************************
 eBizNET Solutions               
 ****************************************************************************/
/*  
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_InventoryMoveLP.js,v $
 *     	   $Revision: 1.14.2.7.4.4.4.23.2.2 $
 *     	   $Date: 2015/11/16 07:23:22 $
 *     	   $Author: grao $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_InventoryMoveLP.js,v $
 * Revision 1.14.2.7.4.4.4.23.2.2  2015/11/16 07:23:22  grao
 * 2015.2 Issue Fixes 201415553
 *
 * Revision 1.14.2.7.4.4.4.23.2.1  2015/11/14 14:53:40  skreddy
 * case 201414408
 * 2015.2 issue fix
 *
 * Revision 1.14.2.7.4.4.4.23  2015/07/02 13:55:54  schepuri
 * case# 201413321
 *
 * Revision 1.14.2.7.4.4.4.22  2014/11/12 13:43:37  sponnaganti
 * Case# 201411006
 * True Fab SB Issue fix
 *
 * Revision 1.14.2.7.4.4.4.21  2014/11/10 15:24:10  schepuri
 * 201410967  issue fix
 *
 * Revision 1.14.2.7.4.4.4.20  2014/07/17 15:35:30  sponnaganti
 * Case# 20148321
 * Stnd Bundle Issue fix
 *
 * Revision 1.14.2.7.4.4.4.19  2014/07/07 07:02:01  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20149310
 *
 * Revision 1.14.2.7.4.4.4.18  2014/07/02 13:55:00  rmukkera
 * no message
 *
 * Revision 1.14.2.7.4.4.4.17  2014/06/19 08:06:21  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148948
 *
 * Revision 1.14.2.7.4.4.4.16  2014/06/18 15:49:02  sponnaganti
 * case# 20148815
 * Stnd Bundle Issue fix
 *
 * Revision 1.14.2.7.4.4.4.15  2014/06/13 09:52:48  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.14.2.7.4.4.4.14  2014/06/06 07:31:11  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.14.2.7.4.4.4.13  2014/05/30 00:34:21  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.14.2.7.4.4.4.12  2014/04/09 16:13:31  gkalla
 * case#201218724
 * Ryonet Fixed LP issue fix
 *
 * Revision 1.14.2.7.4.4.4.11  2014/03/12 06:32:54  skreddy
 * case 20127217
 * Deal med SB issue fixs
 *
 * Revision 1.14.2.7.4.4.4.10  2014/03/05 12:05:18  snimmakayala
 * Case #: 20127451
 *
 * Revision 1.14.2.7.4.4.4.9  2014/02/18 09:27:48  snimmakayala
 * Case# : 20127160
 * Inventory Move Merge LP issue for MHP
 *
 * Revision 1.14.2.7.4.4.4.8  2014/02/04 15:54:19  sponnaganti
 * case# 20127043
 * (In if condition one more condition is added for not to ask serial numbers when we are moving total aval qty)
 *
 * Revision 1.14.2.7.4.4.4.7  2013/11/04 15:20:34  rmukkera
 * Case#  20125536
 *
 * Revision 1.14.2.7.4.4.4.6  2013/10/25 20:02:30  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 20125294
 *
 * Revision 1.14.2.7.4.4.4.5  2013/06/14 12:57:18  grao
 * CASE201112/CR201113/LOG201121
 * PMM Issues fixes
 *
 * Revision 1.14.2.7.4.4.4.4  2013/05/31 15:15:48  grao
 * CASE201112/CR201113/LOG201121
 * PMM Issues fixes
 *
 * Revision 1.14.2.7.4.4.4.3  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.14.2.7.4.4.4.2  2013/03/26 13:27:42  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.14.2.7.4.4.4.1  2013/03/01 14:34:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.14.2.7.4.4  2012/11/27 17:50:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Serial Capturing file.
 *
 * Revision 1.14.2.7.4.3  2012/11/01 14:55:42  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.14.2.7.4.2  2012/09/26 12:36:00  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.14.2.7.4.1  2012/09/21 15:01:30  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.14.2.7  2012/09/07 03:47:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Production issue fixes
 *
 * Revision 1.14.2.6  2012/04/30 10:02:37  spendyala
 * CASE201112/CR201113/LOG201121
 * While Searching of Item in ItemMaster,
 * 'name' filter is changed to 'nameinternal' and also checking weather the item is inactive state or not.
 *
 * Revision 1.14.2.5  2012/04/11 12:16:47  gkalla
 * CASE201112/CR201113/LOG201121
 * True lot functionality
 *
 * Revision 1.14.2.4  2012/03/16 14:08:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.14.2.3  2012/02/23 00:27:34  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * supress lp in Invtmove and lp merge based on fifodate
 *
 * Revision 1.14.2.2  2012/02/07 12:52:23  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Inventory Move Changes
 *
 * Revision 1.18  2012/02/01 13:29:20  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixes is merged up to  1.14.2.1 from branch.
 *
 * Revision 1.17  2012/01/20 01:10:19  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Inventory Move Changes
 *
 * Revision 1.16  2012/01/19 14:48:32  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Invt Move
 *
 * Revision 1.15  2012/01/13 23:58:56  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Invt Move
 *
 * Revision 1.14  2011/12/28 23:15:12  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Putaway changes
 *
 * Revision 1.13  2011/12/26 22:40:12  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Inventory Move Changes
 *
 * Revision 1.11  2011/10/07 08:57:30  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.10  2011/10/04 13:55:13  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.9  2011/09/28 14:25:25  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.8  2011/09/28 13:19:58  mbpragada
 * CASE201112/CR201113/LOG201121
 *

 *
 *****************************************************************************/
function InventoryMoveLP(request, response)
{	 
	var totqtymoved=request.getParameter('custparam_totqtymoveed');
	var getMoveQuantity = request.getParameter('custparam_moveqty');
	var getUOMId = request.getParameter('custparam_uomid');
	var getUOM = request.getParameter('custparam_uom');
	var getStatus = request.getParameter('custparam_status');
	var getStatusId = request.getParameter('custparam_statusid');
	var getLOTId = request.getParameter('custparam_lotid');
	var getLOTNo = request.getParameter('custparam_lot');  
	//var getBatchId = request.getParameter('custparam_batchid');
	var getItemType = request.getParameter('custparam_itemtype');
	var getBatchNo = request.getParameter('custparam_batch'); 	 
	var getActualBeginDate = request.getParameter('custparam_actualbegindate');
	var getActualBeginTime = request.getParameter('custparam_actualbegintime');
	var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
	var locationId=request.getParameter('custparam_locationId');//
	nlapiLogExecution('ERROR', 'locationId ', request.getParameter('custparam_locationId')); 
	var existingLP=request.getParameter('custparam_existingLP');//
	var getBinLocationId = request.getParameter('custparam_imbinlocationid');
	var getBinLocation = request.getParameter('custparam_imbinlocationname');
	var getItemId = request.getParameter('custparam_imitemid');
	var getItem = request.getParameter('custparam_imitem');
	var getTotQuantity = request.getParameter('custparam_totquantity');
	var getAvailQuantity = request.getParameter('custparam_availquantity');	 
	var getLP = request.getParameter('custparam_lp');
	var getLPId = request.getParameter('custparam_lpid');
	var getNewLP = request.getParameter('custparam_newLP');
	var getNewLPId = request.getParameter('custparam_newLPId');
	var getMoveQty = request.getParameter('custparam_moveqty');
	var getInvtRecID=request.getParameter('custparam_invtrecid');
	nlapiLogExecution('ERROR', 'getInvtRecID ',getInvtRecID);
	var locationId=request.getParameter('custparam_locationId');//
	var getBatchId = request.getParameter('custparam_batchid');
	var getActualBeginDate = request.getParameter('custparam_actualbegindate');
	var getActualBeginTime = request.getParameter('custparam_actualbegintime');
	var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
	var compId=request.getParameter('custparam_compid');//

	var getNewBinLocId= request.getParameter('custparam_newbinlocationid');
	var getNewBinLocation= request.getParameter('custparam_newbinlocationname');
	var getNewStatus=request.getParameter('custparam_skustatus');
	var adjustmenttype=request.getParameter('custparam_adjusttype');
	var ItemDesc = request.getParameter('custparam_itemdesc');
	var fifodate=request.getParameter('custparam_fifodate');//

	var	tempBinLocationId=request.getParameter('custparam_hdntmplocation');
	var tempItemId=request.getParameter('custparam_hdntempitem');
	var tempLP=request.getParameter('custparam_hdntemplp');
	var putmethodID=request.getParameter('custparam_NewPutMethodID');//
	nlapiLogExecution('ERROR', 'getNewBinLocId ', getNewBinLocId);
	nlapiLogExecution('ERROR', 'getNewBinLocation ', getNewBinLocation);
	nlapiLogExecution('ERROR', 'getInvtRecID ', getInvtRecID);	
	nlapiLogExecution('ERROR', 'fifodate ', fifodate);	

	var autofilllp='';
	nlapiLogExecution('ERROR', 'totqtymoved ', totqtymoved);
	if(totqtymoved == 'Y')
	{
		autofilllp=getLP;
	}

	nlapiLogExecution('ERROR', 'locationId ', request.getParameter('custparam_locationId'));
	var getPackCode='1';
	var getPackCodeId='1';
	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);

	if (request.getMethod() == 'GET') 
	{		
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('ERROR', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4;
		if( getLanguage == 'es_ES')
		{

			st0 = "INVENTARIO DE MUDANZA";
			st1 = "DESDE LP";
			st2 = "NUEVO LP :";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";


		}
		else
		{
			st0 = "INVENTORY MOVE";
			st1 = "FROM LP";
			st2 = "NEW LP :";
			st3 = "SEND";
			st4 = "PREV";



		}		


		var functionkeyHtml=getFunctionkeyScript('_rf_inventorymove_lp');		
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;alert('System is in process... do not refresh the page');return false;}}"; 
		html = html + "else {if (keycode == 116)(alert('System is in process... do not refresh the page');return false;}}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends  
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		//html = html + " document.getElementById('enternewLP').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head>";
		html = html + "<body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_inventorymove_lp' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> " + st1 + "  : <label>" + getLP + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
//		html = html + "			<tr>";
//		html = html + "				<td align = 'left'>To LP :<label>" + existingLP + "</label>";
//		html = html + "				</td>";
//		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enternewLP' id='enternewLP' type='text' value=" + autofilllp + ">";
		html = html + "					<input type='hidden' name='hdnBinLocationId' value=" + getBinLocationId + ">";
		html = html + "					<input type='hidden' name='hdnBinLocation' value=" + getBinLocation + ">";
		html = html + "					<input type='hidden' name='hdnItemId' value=" + getItemId + ">";
		html = html + "					<input type='hidden' name='hdnautofilllp' value=" + autofilllp + ">";
		html = html + "					<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "					<input type='hidden' name='hdnLP' value=" + getLP + ">";
		html = html + "					<input type='hidden' name='hdnLPId' value=" + getLPId + ">";
		html = html + "					<input type='hidden' name='hdntotqtymoved' value=" + totqtymoved + ">";
		html = html + "					<input type='hidden' name='hdnNewLP' value=" + getNewLP + ">";
		html = html + "					<input type='hidden' name='hdnNewLPId' value=" + getNewLPId + ">";
		html = html + "					<input type='hidden' name='hdnLOTNo' value=" + getLOTNo + ">";
		html = html + "					<input type='hidden' name='hdnLOTNoId' value=" + getLOTId + ">";
		html = html + "					<input type='hidden' name='hdnStatus' value='" + getStatus + "'>";// case# 201413321
		html = html + "					<input type='hidden' name='hdnStatusId' value=" + getStatusId + ">";
		html = html + "					<input type='hidden' name='hdnUOM' value=" + getUOM + ">";
		html = html + "					<input type='hidden' name='hdnUOMId' value=" + getUOMId + ">";
		html = html + "					<input type='hidden' name='hdnTotQty' value=" + getTotQuantity + ">";
		html = html + "					<input type='hidden' name='hdnAvailQty' value=" + getAvailQuantity + ">";
		html = html + "					<input type='hidden' name='hdnMoveQty' value=" + getMoveQty + ">";
		html = html + "					<input type='hidden' name='hdnItemType' value=" + getItemType + ">";
		html = html + "					<input type='hidden' name='hdnInvtRecID' value=" + getInvtRecID + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				    <input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				    <input type='hidden' name='hdnlocationId' value=" + locationId + ">";
		html = html + "				    <input type='hidden' name='hdncompId' value=" + compId + ">";
		html = html + "				    <input type='hidden' name='hdnexistingLP' value=" + existingLP + ">";
		html = html + "				    <input type='hidden' name='hdnNewstatus' value=" + getNewStatus + ">";
		html = html + "				    <input type='hidden' name='hdnAdjustment' value=" + adjustmenttype + ">";
		html = html + "				    <input type='hidden' name='hdnNewbinlocid' value=" + getNewBinLocId + ">";
		html = html + "				    <input type='hidden' name='hdnNewbinlocname' value='" + getNewBinLocation + "'>";
		html = html + "				    <input type='hidden' name='hdnItemdesc' value='" + ItemDesc + "'>";
		html = html + "				    <input type='hidden' name='hdnfifodate' value='" + fifodate + "'>";
		html = html + "				    <input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				    <input type='hidden' name='hdntmplocation' value=" + tempBinLocationId + ">";
		html = html + "				    <input type='hidden' name='hdntemplp' value='" + tempLP + "'>";
		html = html + "				    <input type='hidden' name='hdntempitem' value='" + tempItemId + "'>";
		html = html + "				    <input type='hidden' name='hdnputmethodid' value='" + putmethodID + "'>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSearch' type='submit' value='F8' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "				" + st4 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enternewLP').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		getNewLP=request.getParameter('enternewLP');	
		getexistingLP = request.getParameter('hdnexistingLP');
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		totqtymoved = request.getParameter('hdntotqtymoved');

		var vTaskType=10;

		var IMarray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		IMarray["custparam_language"] = getLanguage;
		nlapiLogExecution('ERROR', 'getLanguage', IMarray["custparam_language"]);


		var st5,st6;
		if( getLanguage == 'es_ES')
		{

			st5 = "LP NO V&#193;LIDO";
			st6 = "ENTER LP NEW";
		}
		else
		{
			st5 = "INVALID LP";
			st6 = "ENTER NEW LP";
		}
		IMarray["custparam_totqtymoveed"] = request.getParameter('hdntotqtymoved');
		IMarray["custparam_newLP"] = getNewLP;	

		IMarray["custparam_actualbegindate"] = getActualBeginDate;

		IMarray["custparam_screenno"] = '23';

		IMarray["custparam_actualbegintime"] = getActualBeginTime;
		IMarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM;
		IMarray["custparam_error"] = st5;

		IMarray["custparam_imbinlocationid"] = request.getParameter('hdnBinLocationId');
		IMarray["custparam_imbinlocationname"] = request.getParameter('hdnBinLocation');
		IMarray["custparam_imitemid"] = request.getParameter('hdnItemId');
		IMarray["custparam_imitem"] = request.getParameter('hdnItem');
		IMarray["custparam_totquantity"] = request.getParameter('hdnTotQty');
		IMarray["custparam_availquantity"] = request.getParameter('hdnAvailQty');
		IMarray["custparam_moveqty"] = request.getParameter('hdnMoveQty');
		IMarray["custparam_status"] = request.getParameter('hdnStatus');
		IMarray["custparam_statusid"] = request.getParameter('hdnStatusId');
		IMarray["custparam_uom"] = request.getParameter('hdnUOM');
		IMarray["custparam_uomid"] = request.getParameter('hdnUOMId');		
		IMarray["custparam_lot"] = request.getParameter('hdnLOTNo');
		IMarray["custparam_lotid"] = request.getParameter('hdnLOTNoId');
		IMarray["custparam_lp"] = request.getParameter('hdnLP');
		IMarray["custparam_lpid"] = request.getParameter('hdnLPId');
		//If Fixed LP is not null then we will consider fixed LP as new LP otherwise we will take entered LP
		if(request.getParameter('hdnNewLP') != null && request.getParameter('hdnNewLP') != '')
		{	
			IMarray["custparam_newLP"] = request.getParameter('hdnNewLP');
			nlapiLogExecution('ERROR', 'Fixed LP', request.getParameter('hdnNewLP'));
			getNewLP=request.getParameter('hdnNewLP');
			nlapiLogExecution('ERROR', 'getNewLP', getNewLP);
		}
		else
			IMarray["custparam_newLP"] = getNewLP;
		nlapiLogExecution('ERROR', 'getNewLP2', getNewLP);
		IMarray["custparam_newLPId"] = request.getParameter('hdnNewLPId');
		IMarray["custparam_itemtype"]=request.getParameter('hdnitemType');
		IMarray["custparam_actualbegindate"] = request.getParameter('custparam_actualbegindate');
		IMarray["custparam_locationId"] = request.getParameter('hdnlocationId');
		IMarray["custparam_skustatus"]=request.getParameter('hdnNewstatus');
		IMarray["custparam_adjusttype"]=request.getParameter('hdnAdjustment');
		IMarray["custparam_newbinlocationid"] = request.getParameter('hdnNewbinlocid');
		IMarray["custparam_newbinlocationname"] = request.getParameter('hdnNewbinlocname');
		IMarray["custparam_invtrecid"] = request.getParameter('hdnInvtRecID');
		IMarray["custparam_itemdesc"] = request.getParameter('hdnItemdesc');
		IMarray["custparam_compid"] = request.getParameter('hdncompId');
		IMarray["custparam_fifodate"] = request.getParameter('hdnfifodate');
		nlapiLogExecution('ERROR', 'Compid', request.getParameter('hdncompId'));
		IMarray["custparam_hdntmplocation"] = request.getParameter('hdntmplocation');
		IMarray["custparam_hdntemplp"] = request.getParameter('hdntemplp');
		IMarray["custparam_hdntempitem"] = request.getParameter('hdntempitem');
		IMarray["custparam_NewPutMethodID"] = request.getParameter('hdnputmethodid');
		nlapiLogExecution('ERROR', 'New Bin Location Id ',request.getParameter('hdnNewbinlocid'));
		nlapiLogExecution('ERROR', 'New Bin Location Name  ',request.getParameter('hdnNewbinlocname'));
		nlapiLogExecution('ERROR', 'Invt Rec Id  ',request.getParameter('hdnInvtRecID'));

		nlapiLogExecution('ERROR', 'IMarray["custparam_locationId"]11 ', IMarray["custparam_locationId"]);

		var locationid = request.getParameter('hdnlocationId');
		nlapiLogExecution('ERROR', 'request.getParameter(hdnlocationId)11 ', locationid);
		getBinLocationId = request.getParameter('hdnBinLocationId');
		getBinLocation = request.getParameter('hdnBinLocation');
		getItemId = request.getParameter('hdnItemId');
		getItem = request.getParameter('hdnItem');
		getTotQuantity = request.getParameter('hdnTotQty');
		getAvailQuantity = request.getParameter('hdnAvailQty');
		getMoveQty = request.getParameter('hdnMoveQty');
		getStatus = request.getParameter('hdnStatus');
		getStatusId = request.getParameter('hdnStatusId');
		getUOM = request.getParameter('hdnUOM');
		getUOMId = request.getParameter('hdnUOMId');		
		getLOTNo = request.getParameter('hdnLOTNo');
		getLOTId = request.getParameter('hdnLOTNoId');
		getLP = request.getParameter('hdnLP');
		autofilllp = request.getParameter('hdnautofilllp');
		getLPId = request.getParameter('hdnLPId');
		getInvtRecID = request.getParameter('hdnInvtRecID');
		getNewStatus=request.getParameter('hdnNewstatus');
		adjustmenttype=request.getParameter('hdnAdjustment');

		var newBinlocationId=request.getParameter('hdnNewbinlocid');
		nlapiLogExecution('ERROR', 'getInvtRecID ',getInvtRecID);
		nlapiLogExecution('ERROR', 'newBinlocationId ',newBinlocationId);

		nlapiLogExecution('ERROR', 'getStatusId ',getStatusId);
		nlapiLogExecution('ERROR', 'getNewStatus ',getNewStatus);

		var TimeArray = new Array();
		IMarray["custparam_actualbegintime"] = getActualBeginTime;
		IMarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM;
		var lpgentype='2';
		var optedEvent = request.getParameter('cmdPrevious');
		if( (getNewLP == "" || getNewLP == null))
		{
			if(autofilllp==null || autofilllp=='')
			{

				nlapiLogExecution('Error', 'locationid :', locationid);
				getNewLP=GetMaxLPNo('1', '1',locationid);
				nlapiLogExecution('Error', 'auto generated lp :', getNewLP);
				lpgentype='1';
			}
		}
		if (sessionobj!=context.getUser()) {	
			try
			{

				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}
		
		var itemSubtype = nlapiLookupField('item', getItemId, ['recordType','custitem_ebizserialin','custitem_ebizserialout','custitem_ebizbatchlot']);
		IMarray["custparam_itemtype"] = itemSubtype.recordType;
		var batchlot = itemSubtype.custitem_ebizbatchlot;
		nlapiLogExecution('ERROR', 'itemSubtype.recordType =', itemSubtype);
		//case# 20127043 starts(In if condition one more condition is added for not to ask serial numbers when we are moving total aval qty)
		nlapiLogExecution('ERROR', 'getAvailQuantity', getAvailQuantity);
		nlapiLogExecution('ERROR', 'getMoveQty', getMoveQty);
		
		if (optedEvent == 'F7') 
		{
			response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_endloc', 'customdeploy_rf_inventory_move_endloc_di', false, IMarray);
		}
		else 
		{
			if ((getNewLP == '' || getNewLP == null) && (autofilllp==null || autofilllp=='')) 
			{
				//	if the 'Send' button is clicked without any option value entered,
				//  it has to show an error message. The next screen to which it has to navigate is to the error screen.
				IMarray["custparam_error"] = st6;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
				nlapiLogExecution('ERROR', 'Entered New Location', getNewBinLocation);
				return;
			}
			else 
			{
				if (getNewLP != null && getNewLP != '') 
				{
					if(getNewLP==autofilllp)
					{
// case no 201410967
						nlapiLogExecution('ERROR', 'getNewLP tst1', getNewLP);
						nlapiLogExecution('ERROR', 'autofilllp tst1', autofilllp);
						nlapiLogExecution('ERROR', 'request.getParameter(hdnNewLP)', request.getParameter('hdnNewLP'));
						nlapiLogExecution('ERROR', 'IMarray["custparam_newLP"]', IMarray["custparam_newLP"]);
						
						if(getInvtRecID != null && getInvtRecID != '')
						{	
							var recLoad = nlapiLoadRecord('customrecord_ebiznet_createinv', getInvtRecID);
							var vCurrAllocQty=recLoad.getFieldValue('custrecord_ebiz_alloc_qty');
							if(vCurrAllocQty == null || vCurrAllocQty == '')
							{
								vCurrAllocQty='0';
							}	
							nlapiLogExecution('ERROR', 'vCurrAllocQty tst1', vCurrAllocQty);
							if(vCurrAllocQty != null && vCurrAllocQty != '' && vCurrAllocQty != '0' && vCurrAllocQty != 0 && parseFloat(vCurrAllocQty) >0)
							{
								IMarray["custparam_newLP"] = "";
								IMarray["custparam_error"] = st5;
								nlapiLogExecution('ERROR', 'vCurrAllocQty1', vCurrAllocQty);
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
								return;
							}
						}
						if(request.getParameter('hdnNewLP') != null && request.getParameter('hdnNewLP') != '')
						{	
							IMarray["custparam_newLP"] = request.getParameter('hdnNewLP');
							nlapiLogExecution('ERROR', 'Fixed LP', request.getParameter('hdnNewLP'));
							getNewLP=request.getParameter('hdnNewLP');
							nlapiLogExecution('ERROR', 'getNewLP', getNewLP);
						}
						//IMarray["custparam_newLP"] = getNewLP;
					}
					else
					{
						//added on 01/02/12 by suman
						//Passed one more parameter i.e., locationid to check the LP against the given location id.
						var containerLPValidate = ebiznet_LPRange_CL(getNewLP, lpgentype,locationid);
						nlapiLogExecution('ERROR', 'containerLPValidate', containerLPValidate);
						IMarray["custparam_error"] = st5;

						if(containerLPValidate)
						{
							var ValidLPArray = ValidateLP(getNewLP,totqtymoved,autofilllp,newBinlocationId,locationid,getLOTId,IMarray["custparam_itemtype"],batchlot,getInvtRecID);
							nlapiLogExecution('ERROR', 'ValidLPArray[0]', ValidLPArray[0]);
							nlapiLogExecution('ERROR', 'ValidLPArray[1]', ValidLPArray[1]);

							if(ValidLPArray[1] == "N")
							{
								IMarray["custparam_error"] = ValidLPArray[0];
								IMarray["custparam_newLP"] = "";
								nlapiLogExecution('ERROR', 'SearchResults of given LP', ValidLPArray[0]);		
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
								nlapiLogExecution('ERROR', 'LP Entered', getNewLP);
								return;
							}
							else
							{     
								if(request.getParameter('hdnNewLP') != null && request.getParameter('hdnNewLP') != '')
								{	
									IMarray["custparam_newLP"] = request.getParameter('hdnNewLP');
									nlapiLogExecution('ERROR', 'Fixed LP', request.getParameter('hdnNewLP'));
									getNewLP=request.getParameter('hdnNewLP');
									nlapiLogExecution('ERROR', 'getNewLP', getNewLP);
								}
								IMarray["custparam_newLP"] = getNewLP;
							}
							//var retLprec =	InsertLP(getNewLP);
						}
						else
						{
							IMarray["custparam_newLP"] = "";
							IMarray["custparam_error"] = st5;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
							return;
						}
					}
					
					if ((itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.custitem_ebizserialin == 'T' || itemSubtype.custitem_ebizserialout == 'T')&&(getAvailQuantity!=getMoveQty)) 
					{
						
						if(parseInt(getMoveQty) != getMoveQty)
						{
							IMarray["custparam_error"] = "For Serial item ,Decimal quantities not allowed";
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
							return;
						}
						//case# 20127043 end
						response.sendRedirect('SUITELET', 'customscript_rf_inventory_serialscan', 'customdeploy_rf_inventory_serialscan_di', false, IMarray);
						return;
					}
					response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_complete', 'customdeploy_rf_inventory_move_comple_di', false, IMarray);
				}
				else
				{
					IMarray["custparam_error"] = st6;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
					nlapiLogExecution('ERROR', 'Entered New Location', getNewBinLocation);
					return;
				}
					}
				}
			}
			catch (e)  {
				nlapiLogExecution('DEBUG', 'e',e);
				IMarray["custparam_error"] = st6;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');
				
			}
		}
		else
		{
			IMarray["custparam_screenno"] = 'InventoryMenu';
			IMarray["custparam_error"] = 'ITEM ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
		}
	}
}



function CreateInventory(ItemId, Item, ItemStatusId,ItemStatus, UOMId, UOM, LOTNo, LOTNoId, ItemLP, NewItemLP, BinLocation, BinLocationId, NewBinLocation, NewBinLocationId, getPackCode, getPackCodeId, TotQuantity, AvailQty, MoveQty, ActualEndDate, ActualEndTime, ActualBeginDate, ActualBeginTime, getInvtRecID,locationid,getNewStatus,adjustmenttype,vTaskType)
{
	nlapiLogExecution('ERROR', 'Into CreateInventory', '');
	var retLprec =	InsertLP(NewItemLP);
	//create a inventory record
	if(BinLocationId!=NewBinLocationId)
	{
		var CreateInventoryRecord = nlapiCreateRecord('customrecord_ebiznet_createinv');
		nlapiLogExecution('ERROR', 'Create Inventory Move Record', 'Inventory Record');

		CreateInventoryRecord.setFieldValue('name', 'Inventory Move');
		CreateInventoryRecord.setFieldValue('custrecord_wms_inv_status_flag',19); //Status - STORAGE

		nlapiLogExecution('ERROR', 'IN  Create Inventory Record: ItemId', ItemId);
		nlapiLogExecution('ERROR', 'IN  Create Inventory Record: Item', Item);
		var MoveQtyTemp='1';
		nlapiLogExecution('ERROR', 'IN  Create Inventory Record: MoveQtyTemp', MoveQtyTemp);
		//Added for Item name and desc
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_sku', ItemId);
		CreateInventoryRecord.setFieldValue('custrecord_inv_ebizsku_no', ItemId);
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(MoveQty).toFixed(4));
		//var vNewMovQty=MoveQty;
		nlapiLogExecution('ERROR', 'IN  Create Inventory Record: custrecord_ebiz_avl_qty', parseFloat(MoveQty).toFixed(4));
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_avl_qty', parseFloat(MoveQty).toFixed(4));
		nlapiLogExecution('ERROR', 'IN  Create Inventory Record: custrecord_ebiz_avl_qty', parseFloat(MoveQty).toFixed(4));

		CreateInventoryRecord.setFieldValue('custrecord_ebiz_qoh', parseFloat(MoveQty).toFixed(4));
		nlapiLogExecution('ERROR', 'IN  Create Inventory Record: custrecord_ebiz_qoh', parseFloat(MoveQty).toFixed(4));
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_packcode', 1);
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_sku_status', ItemStatusId);
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_lot', LOTNoId);
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_loc', locationid);//Inventory Location);
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_lp', NewItemLP);//Item LP #);
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_binloc', NewBinLocationId);//Bin Location);
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_callinv', 'N');//Bin Location);
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_displayfield', 'N');//Bin Location);
		CreateInventoryRecord.setFieldValue('custrecord_ebiz_inv_packcode', getPackCode);
		CreateInventoryRecord.setFieldValue('custrecord_invttasktype', vTaskType);

		nlapiLogExecution('ERROR', 'custrecord_status_flag', '19');
		nlapiLogExecution('ERROR', 'Submitting Create Inventory Record', CreateInventoryRecord);

		//commit the record to NetSuite
		var CreateInventoryRecordId = nlapiSubmitRecord(CreateInventoryRecord);
		nlapiLogExecution('ERROR', 'CreateInventoryRecordId', 'test');
		nlapiLogExecution('ERROR', 'CreateInventoryRecordId', CreateInventoryRecordId);

		nlapiLogExecution('ERROR', 'Inventory Move Creation', 'Success');

		var arrDims = getSKUCubeAndWeight(ItemId);           

		var itemCube = 0;
		if (arrDims["BaseUOMItemCube"] != "" && (!isNaN(arrDims["BaseUOMItemCube"]))) 
		{
			/*
			 * Item Cube Formula : ItemCube = ((TotalQty/BaseUOMQty)*BaseUOMCube)
			 */
			var uomqty = ((parseFloat(MoveQty))/(parseFloat(arrDims["BaseUOMQty"])));			
			itemCube = (parseFloat(uomqty) * parseFloat(arrDims["BaseUOMItemCube"]));
		} else 
		{
			itemCube = 0;
		}           

		var oldLocRemCube = GeteLocCube(BinLocationId);           

		var oldremcube = (parseFloat(oldLocRemCube) + parseFloat(itemCube));			
		oldLocRemCube = parseFloat(oldremcube);

		if (BinLocationId != null && BinLocationId != "") 
		{            		        		
			var retValue = UpdateLocCube(BinLocationId, oldLocRemCube);				        	 
		}
		nlapiLogExecution('ERROR', 'New Bin Loc ID', NewBinLocationId);
		var newLocRemCube = GeteLocCube(NewBinLocationId);
		nlapiLogExecution('ERROR', 'newLocRemCube', newLocRemCube);
		if (parseFloat(newLocRemCube) > parseFloat(itemCube)) 
		{
			var newremcube = parseFloat(newLocRemCube) - parseFloat(itemCube);
			newLocRemCube = parseFloat(newremcube);

			if (NewBinLocationId != null && NewBinLocationId != "") 
			{
				var retValue = UpdateLocCube(NewBinLocationId, newLocRemCube);				
			}
		}
//		if((parseFloat(TotQuantity) - parseFloat(MoveQty)) == 0)
//		{			
//		nlapiLogExecution('ERROR', 'before delete:', 'before delete');		
//		var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', getInvtRecID);		
//		nlapiLogExecution('ERROR', 'after delete:', 'after delete');	
//		}	
		nlapiLogExecution('ERROR', 'Out of CreateInventory', '');
	}
	else
	{
		var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');
		//var vIntValue="N";
		nlapiLogExecution('ERROR', 'Creating Inventory Record', 'INVT');
		invtRec.setFieldValue('name', 'Inventory Move');
		invtRec.setFieldValue('custrecord_ebiz_inv_binloc', NewBinLocationId);
		invtRec.setFieldValue('custrecord_ebiz_inv_lp', NewItemLP);
		invtRec.setFieldValue('custrecord_ebiz_inv_sku', ItemId);
		invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(MoveQty).toFixed(4));
		invtRec.setFieldValue('custrecord_inv_ebizsku_no', ItemId);
		invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(MoveQty).toFixed(4));
		invtRec.setFieldValue('custrecord_ebiz_inv_loc', locationid);
		invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', ItemStatusId);
		invtRec.setFieldValue('custrecord_ebiz_inv_packcode', getPackCode);
		invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
		invtRec.setFieldValue('custrecord_ebiz_displayfield','N');				
		invtRec.setFieldValue('custrecord_wms_inv_status_flag',19);	
		nlapiLogExecution('ERROR', 'Before Submitting inventory LotBatch', LotBatch);
		invtRec.setFieldValue('custrecord_ebiz_inv_lot', LOTNoId);
		invtRec.setFieldValue('custrecord_invttasktype', vTaskType);
		invtRec.setFieldValue('custrecord_ebiz_inv_adjusttype', adjustmenttype);

		nlapiLogExecution('ERROR', 'Before Submitting inventory recid', 'Inventory Records');
		var invtrecid = nlapiSubmitRecord(invtRec, false, true);
	}
}

function createOpenTaskAndInventoryRecord(ItemId, Item, ItemStatusId,ItemStatus, UOMId, UOM, LOTNo, LOTNoId, ItemLP, NewItemLP, BinLocation, BinLocationId, NewBinLocation, NewBinLocationId, getPackCode, getPackCodeId, TotQuantity, AvailQty, MoveQty, ActualEndDate, ActualEndTime, ActualBeginDate, ActualBeginTime, getInvtRecID,locationid,getNewStatus,adjustmenttype)
{
	nlapiLogExecution('ERROR', 'Into createOpenTaskAndInventoryRecord', '');

	try{

		var CallFlag='N';
		var OldMakeWHFlag='Y';
		var NewMakeWHFlag='Y';
		var vNewWHlocatin;
		var vOldWHlocatin;
		var vNSFlag;
		var UpdateFlag;
		var vNSCallFlag='N';
		if(ItemStatusId==getNewStatus)
			CallFlag='N';
		else
		{

			vNSFlag=GetNSCallFlag(ItemStatusId,getNewStatus);
			nlapiLogExecution('ERROR', 'vNSFlag',vNSFlag);

			if(vNSFlag!= null && vNSFlag!= "" && vNSFlag.length>0)
			{
				CallFlag=vNSFlag[0];
				nlapiLogExecution('ERROR', 'vNSFlag[0]',vNSFlag[0]);
			}
			if(vNSFlag[0]=='N')
				CallFlag='N';
			else if(vNSFlag[0]=='Y') 
			{ 
				OldMakeWHFlag= GetMWFlag(vNSFlag[1]);
				NewMakeWHFlag= GetMWFlag(vNSFlag[2]);
				vNewWHlocatin=vNSFlag[2]; 
				vOldWHlocatin=vNSFlag[1]; 
			}


		}
		var vTaskType='9';
		if(CallFlag=='N')
			vTaskType='9';
		else
			vTaskType='18';

		//added code on 060912 by suman
		//This will get the max movereport sequence no from WAVE record table.
		var reportno = GetMaxTransactionNo('MOVEREPORT');
		nlapiLogExecution('ERROR', 'reportno', reportno);


		var opentaskrec = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');

		//Added for Item id.
		//opentaskrec.setFieldValue('name', itemLocName);
		opentaskrec.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		opentaskrec.setFieldValue('custrecord_act_qty', parseFloat(MoveQty).toFixed(4));//Assigning new quantity
		opentaskrec.setFieldValue('custrecord_lpno', NewItemLP);
		opentaskrec.setFieldValue('custrecord_ebiz_lpno', NewItemLP);

		opentaskrec.setFieldValue('custrecord_tasktype', vTaskType);
		opentaskrec.setFieldValue('custrecord_actbeginloc', BinLocationId);
		opentaskrec.setFieldValue('custrecord_actendloc', NewBinLocationId);

		//Added for Item name and desc
		opentaskrec.setFieldValue('custrecord_sku', ItemId);
		opentaskrec.setFieldValue('custrecord_skudesc', Item);

		opentaskrec.setFieldValue('custrecordact_begin_date', ActualBeginDate);
		opentaskrec.setFieldValue('custrecord_act_end_date', ActualEndDate);
		opentaskrec.setFieldValue('custrecord_packcode', getPackCode);
		opentaskrec.setFieldValue('custrecord_wms_status_flag', 19);
		opentaskrec.setFieldValue('custrecord_batch_no', LOTNo);
		opentaskrec.setFieldValue('custrecord_upd_ebiz_user_no', nlapiGetContext().getUser());
		opentaskrec.setFieldValue('custrecord_ebiz_report_no', reportno.toString());
		nlapiLogExecution('DEBUG', 'Submitting MOVE record', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(opentaskrec, false, true);
		nlapiLogExecution('ERROR', 'Done MOVE Record Insertion in OpenTask:', 'Success');

		nlapiLogExecution('ERROR', 'Done MOVE Record Insertion In Create Inventory:', getInvtRecID);
		CreateInventory(ItemId, Item, ItemStatusId,ItemStatus, UOMId, UOM, LOTNo, LOTNoId, ItemLP, NewItemLP, BinLocation, BinLocationId, NewBinLocation, NewBinLocationId, getPackCode, getPackCodeId, TotQuantity, AvailQty, MoveQty, ActualEndDate, ActualEndTime, ActualBeginDate, ActualBeginTime, getInvtRecID,locationid,getNewStatus,adjustmenttype,vTaskType);
		nlapiLogExecution('ERROR', 'Done MOVE Record Insertion In Create Inventory:', getInvtRecID);

		if(CallFlag=='Y')//To trigger NS automatically
		{

			//Code for fetching account no from stockadjustment custom record	
			var AccountNo = getStockAdjustmentAccountNoNew(adjustmenttype);

			if(AccountNo !=null && AccountNo != "" && AccountNo.length>0)
			{
				var FromAccNo=AccountNo[0];
				var ToAccNo=AccountNo[1];
				nlapiLogExecution('ERROR', 'vOldWHlocatin',vOldWHlocatin);
				nlapiLogExecution('ERROR', 'vNewWHlocatin',vNewWHlocatin);

				nlapiLogExecution('ERROR', 'FromAccNo',FromAccNo);
				nlapiLogExecution('ERROR', 'ToAccNo',ToAccNo);
				InvokeNSInventoryAdjustmentNew(ItemId,ItemStatus,vOldWHlocatin,-(parseFloat(MoveQty)),"",vTaskType,adjustmenttype,"",FromAccNo);
				InvokeNSInventoryAdjustmentNew(ItemId,getNewStatus,vNewWHlocatin,MoveQty,"",vTaskType,adjustmenttype,"",ToAccNo);							 
			} 
		}


//		var recLoad = nlapiLoadRecord('customrecord_ebiznet_createinv', getInvtRecID);
//		nlapiLogExecution('ERROR', 'TotQuantity:', TotQuantity);
//		nlapiLogExecution('ERROR', 'MoveQty:', MoveQty);
//		recLoad.setFieldValue('custrecord_ebiz_qoh', (parseFloat(TotQuantity) - parseFloat(MoveQty)));
//		nlapiLogExecution('ERROR', 'custrecord_ebiz_qoh:', parseFloat(TotQuantity) - parseFloat(MoveQty));
//		recLoad.setFieldValue('custrecord_updated_user_no', nlapiGetContext().getUser());
//		nlapiLogExecution('ERROR', 'before submit:', 'before submit');
//		nlapiSubmitRecord(recLoad, true);
//		nlapiLogExecution('ERROR', 'after submit:', 'after submit');

//		nlapiLogExecution('ERROR', 'TotQuantity', TotQuantity);
//		nlapiLogExecution('ERROR', 'MoveQty', MoveQty);
//		if((parseFloat(TotQuantity) - parseFloat(MoveQty)) == 0)
//		{			
//		nlapiLogExecution('ERROR', 'before delete:', 'before delete');		
//		var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', getInvtRecID);		
//		nlapiLogExecution('ERROR', 'after delete:', 'after delete');	
//		}	
	}
	catch (e) {
		if (e instanceof nlobjError) 
			nlapiLogExecution('ERROR', 'system error', e.getCode() + '\n' + e.getDetails());
		else 
			nlapiLogExecution('ERROR', 'unexpected error', e.toString());
	}

	nlapiLogExecution('ERROR', 'Out of createOpenTaskAndInventoryRecord', '');
}

function getStockAdjustmentAccountNoNew(adjusttype)
{	
	var StAdjustmentAccountNo=new Array();
	var filterStAccNo = new Array();
	var colsStAccNo = new Array();

	if(adjusttype!=null && adjusttype!="")
		filterStAccNo.push(new nlobjSearchFilter('internalid',null,'is',adjusttype));

	filterStAccNo.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	colsStAccNo[0] = new nlobjSearchColumn('custrecord_ebiz_mapto_ns_glaccount');
	colsStAccNo[1] = new nlobjSearchColumn('custrecord_ebiz_to_ns_glaccount');

	var StockAdjustAccResults = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, filterStAccNo, colsStAccNo);
	if(StockAdjustAccResults!=null && StockAdjustAccResults != "" && StockAdjustAccResults.length>0)
	{
		StAdjustmentAccountNo.push(StockAdjustAccResults[0].getValue('custrecord_ebiz_mapto_ns_glaccount'));
		StAdjustmentAccountNo.push(StockAdjustAccResults[0].getValue('custrecord_ebiz_to_ns_glaccount'));
	}

	return StAdjustmentAccountNo;	
}

/**
 * Passing item,item status, qty and location to NS
 * 
 * @param item
 * @param itemstatus
 * @param loc
 * @param qty
 * @param notes
 */
function  InvokeNSInventoryAdjustmentNew(item,itemstatus,loc,qty,notes,tasktype,adjusttype,lot,AccNo)
{
	try{
		nlapiLogExecution('ERROR', 'Location info::', loc);
		nlapiLogExecution('ERROR', 'Task Type::', tasktype);
		nlapiLogExecution('ERROR', 'Adjustment Type::', adjusttype);
		nlapiLogExecution('ERROR', 'qty::', qty);
		nlapiLogExecution('ERROR', 'AccNo::', AccNo);

		var confirmLotToNS='N';
		confirmLotToNS=GetConfirmLotToNS(loc);
		var vItemname;

		var filters = new Array();          
		filters.push(new nlobjSearchFilter('internalid', null, 'is',item));
		//Changed on 30/4/12 by suman
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		//End of changes as on 30/4/12
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('cost');
		columns[1] = new nlobjSearchColumn('averagecost');
		columns[2] = new nlobjSearchColumn('itemid');

		var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
		if (itemdetails !=null) 
		{
			vItemname=itemdetails[0].getValue('itemid');
			vCost = itemdetails[0].getValue('cost');
			nlapiLogExecution('ERROR', 'vCost', vCost);
			vAvgCost = itemdetails[0].getValue('averagecost');
			nlapiLogExecution('ERROR', 'Average Cost', vAvgCost);	         
		}		

		var outAdj = nlapiCreateRecord('inventoryadjustment');			
		outAdj.setFieldValue('account', AccNo);
		outAdj.setFieldValue('memo', notes);
		outAdj.setFieldValue('adjlocation', loc);
		outAdj.insertLineItem('inventory', 1);
		outAdj.setLineItemValue('inventory', 'item', 1, item);
		outAdj.setLineItemValue('inventory', 'location', 1, loc);	
		outAdj.setLineItemValue('inventory', 'adjustqtyby', 1, parseFloat(qty));
		if(vAvgCost != null &&  vAvgCost != "")	
		{
			nlapiLogExecution('ERROR', 'into if cond vAvgCost', vAvgCost);
			outAdj.setLineItemValue('inventory', 'unitcost', 1, vAvgCost);
		}
		else
		{
			nlapiLogExecution('ERROR', 'into else cond.unit cost', vCost);
			outAdj.setLineItemValue('inventory', 'unitcost', 1, vCost);
		}
		//outAdj.setFieldValue('subsidiary', 4);

		//Added by Sudheer on 093011 to send lot# for inventory posting

		if(lot!=null && lot!='')
		{
			if(parseFloat(qty)<0)
				qty=parseFloat(qty)*(-1);

			if(confirmLotToNS=='N')
				lot=vItemname;		

			nlapiLogExecution('ERROR', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");
			outAdj.setLineItemValue('inventory', 'serialnumbers', 1, lot + "(" + parseFloat(qty) + ")");
		}

		nlapiSubmitRecord(outAdj, true, true);
		nlapiLogExecution('ERROR', 'type argument', 'type is create');
	}


	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in InvokeNSInventoryAdjustmentNew ', exp);	

	}
}

function InsertLP(ItemNewLP)
{

	try {
		nlapiLogExecution('ERROR', 'INTO MASTER LP INSERTION');
		var filtersmlp = new Array();
		filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', ItemNewLP);

		var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

		if (SrchRecord != null && SrchRecord.length > 0) 
		{
			nlapiLogExecution('ERROR', 'LP FOUND');

			//lpExists = 'Y';
		}
		else 
		{
			nlapiLogExecution('ERROR', 'LP NOT FOUND');
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
			customrecord.setFieldValue('name', ItemNewLP);
			customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', ItemNewLP);
			var rec = nlapiSubmitRecord(customrecord, false, true);
		}
	} 
	catch (e) 
	{
		nlapiLogExecution('ERROR', 'Failed to Update/Insert into Master LP Record');
	}

}

function ValidateLP(newLP,totqtymoved,autofilllp,vnewBinlocationId,loc,getLOTId,Itype,batchlot,getInvtRecID)
{
	nlapiLogExecution('ERROR', 'ValidateLP', totqtymoved);
	nlapiLogExecution('ERROR', 'New LP', newLP);
	nlapiLogExecution('ERROR', 'vnewBinlocationId', vnewBinlocationId);
	var LPType='';
	var ErrorMessage=new Array();

	if (newLP.length > 0) 
	{
		var type = "PALT";
		var vargetlpno = newLP;
		var vResult = "";

		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lptype', null, 'is', 1));
		filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_lpgentype', null, 'is', [1,2]));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		//Added on 31/01/12 by suman
		if(loc!=null && loc!="")
			filters.push(new nlobjSearchFilter('custrecord_ebiznet_lprange_site', null, 'anyof', loc));
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix');
		columns[1] = new nlobjSearchColumn('custrecord_ebiznet_lprange_begin');
		columns[2] = new nlobjSearchColumn('custrecord_ebiznet_lprange_end');
		columns[3] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lpgentype');
		columns[4] = new nlobjSearchColumn('custrecord_ebiznet_lprange_lptype');

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, filters, columns);
		nlapiLogExecution('ERROR', 'Lp Range Searchresults', searchresults.length);
		if (searchresults) 
		{
			for (var i = 0; i < Math.min(50, searchresults.length); i++) 
			{
				try 
				{
					var getLPrefix = (searchresults[i].getValue('custrecord_ebiznet_lprange_lpprefix')).toUpperCase();					
					var varBeginLPRange = searchresults[i].getValue('custrecord_ebiznet_lprange_begin');
					var varEndRange = searchresults[i].getValue('custrecord_ebiznet_lprange_end');
					var getLPGenerationTypeValue = searchresults[i].getValue('custrecord_ebiznet_lprange_lpgentype');
					var getLPTypeValue = searchresults[i].getValue('custrecord_ebiznet_lprange_lptype');

					var LPprefixlen = getLPrefix.length;
					var vLPLen = vargetlpno.substring(0, LPprefixlen).toUpperCase();
					nlapiLogExecution('ERROR', 'getLPrefix', getLPrefix);
					nlapiLogExecution('ERROR', 'LPprefixlen', LPprefixlen);
					nlapiLogExecution('ERROR', 'vLPLen', vLPLen);
					if (vLPLen == getLPrefix) 
					{
						LPType = getLPGenerationTypeValue;
						var varnum = vargetlpno.substring(LPprefixlen, vargetlpno.length);
						nlapiLogExecution('ERROR', 'varnum', varnum);
						nlapiLogExecution('ERROR', 'varEndRange', varEndRange);
						if (varnum.length > varEndRange.length) 
						{
							nlapiLogExecution('ERROR', 'varEndRange', varEndRange);
							vResult = "N";
							break;
						}
						if ((parseFloat(varnum,10) < parseFloat(varBeginLPRange)) || (parseFloat(varnum,10) > parseFloat(varEndRange))) 
						{
							nlapiLogExecution('ERROR', 'varBeginLPRange', varBeginLPRange);
							vResult = "N";
							break;
						}
						else 
						{
							vResult = "Y";
							break;
						}
					}
					else 
					{
						vResult = "N";
					}

				} 
				catch (err) 
				{

				}
			} //end of for loop
		}

		//alert("vResult :" +vResult);
		nlapiLogExecution('ERROR', 'b', totqtymoved);
		nlapiLogExecution('ERROR', 'autofilllp', autofilllp);
		nlapiLogExecution('ERROR', 'newLP', newLP);
		nlapiLogExecution('ERROR', 'vResult', vResult);
		nlapiLogExecution('ERROR', 'LPType', LPType);
		if (totqtymoved != 'Y' )
		{
			if (vResult == "Y" ) 
			{

				var filters1 = new Array();				
				//filters1[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lpno);
				filters1[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', vargetlpno);

				var columns1 = new Array();
				columns1[0] = new nlobjSearchColumn('name');

				var searchresults1 = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters1,columns1);		
				if(searchresults1!=null &&  searchresults1.length>0)
				{				

					var res = CheckLPinInvt(vargetlpno,vnewBinlocationId,getLOTId,Itype,batchlot);
					nlapiLogExecution('ERROR', 'res', res);
					//if(res !=null && res !='')
					if(res ==null || res =='')	
					{
						ErrorMessage[0] = "Success";					
						ErrorMessage[1] = "Y";	
					}
					else
					{
						//ErrorMessage[0] = "Inventory not Exists for LP";
						ErrorMessage[0] = "LP Already Exists";
						ErrorMessage[1] = "N";
					}


				}
				else
				{
					if(LPType =='1')
					{
						ErrorMessage[0] = "Invalid LP No";					
						ErrorMessage[1] = "N";
					}
					else
					{
						ErrorMessage[0] = "Success";					
						ErrorMessage[1] = "Y";
					}
				}

			}
			else 
			{	
				/*ErrorMessage[0] = "Invalid LP Range";					
				ErrorMessage[1] = "N";
				nlapiLogExecution('ERROR', 'Lp Range Here', ErrorMessage[0]);*/
				

				if (vResult == "Y" ) 
				{
					if(newLP == autofilllp)
					{
						if(getInvtRecID != null && getInvtRecID != '')
						{	
							var recLoad = nlapiLoadRecord('customrecord_ebiznet_createinv', getInvtRecID);
							var vCurrAllocQty=recLoad.getFieldValue('custrecord_ebiz_alloc_qty');
							if(vCurrAllocQty == null || vCurrAllocQty == '')
							{
								vCurrAllocQty='0';
							}	
							nlapiLogExecution('ERROR', 'vCurrAllocQty tst', vCurrAllocQty);
							if(vCurrAllocQty != null && vCurrAllocQty != '' && vCurrAllocQty != '0' && vCurrAllocQty != 0 && parseFloat(vCurrAllocQty) >0)
							{
								ErrorMessage[0] = "Invalid LP No";					
								ErrorMessage[1] = "N";
							}
						}
					}
				}
				else 
				{	
					ErrorMessage[0] = "Invalid LP Range";					
					ErrorMessage[1] = "N";
					nlapiLogExecution('ERROR', 'Lp Range Here', ErrorMessage[0]);
				}
			
			}
		}
	}
	else 
	{		
		ErrorMessage[0] = "Invalid LP No";					
		ErrorMessage[1] = "N";
	}

	return ErrorMessage;
}

function GetNSCallFlag(Status,NewStatus)
{
	nlapiLogExecution('ERROR', 'Into GetNSCallFlag', '');
	nlapiLogExecution('ERROR', 'Status', Status);
	nlapiLogExecution('ERROR', 'New Status', NewStatus);
	var result=new Array();
	var retFlag='N';
	var OldMapLocation;
	var NewMapLocation;
	if(Status!=null && Status!="")
	{
		var OldIStatus = nlapiLoadRecord('customrecord_ebiznet_sku_status', Status);
		if(OldIStatus!= null && OldIStatus != "")
		{
			OldMapLocation=OldIStatus.getFieldValue('custrecord_item_status_location_map');
			OldIStatus=null;
		} 
	}
	if(NewStatus!=null && NewStatus!="")
	{
		var NewIStatus = nlapiLoadRecord('customrecord_ebiznet_sku_status', NewStatus);
		if(NewIStatus!= null && NewIStatus != "")
		{
			NewMapLocation=NewIStatus.getFieldValue('custrecord_item_status_location_map');
		} 
	}
	if(OldMapLocation != null && OldMapLocation !="" && NewMapLocation != null && NewMapLocation != "")
	{
		if(OldMapLocation==NewMapLocation)
		{
			retFlag='N';
			result.push('N');
		}
		else
		{
			result.push('Y');
		}
		result.push(OldMapLocation);
		result.push(NewMapLocation);
	}
	nlapiLogExecution('ERROR', 'retFlag', retFlag);
	nlapiLogExecution('ERROR', 'OldMapLocation', OldMapLocation);
	nlapiLogExecution('ERROR', 'NewMapLocation', NewMapLocation);
	nlapiLogExecution('ERROR', 'Out of GetNSCallFlag', '');
	return result;
}
function GetMWFlag(WHLocation)
{
	nlapiLogExecution('ERROR', 'Into GetMWFlag : WHLocation', WHLocation);
	var retFlag='Y';
	var MWHSiteFlag;

	if(WHLocation!=null && WHLocation!="")
	{
		var filters = new Array();
		filters.push(new nlobjSearchFilter('internalid', null, 'is', WHLocation));
		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_ebizwhsite')); 

		var WHOld = nlapiSearchRecord('location', null, filters, columns);

		if(WHOld!= null && WHOld != "" && WHOld.length>0)
		{
			MWHSiteFlag=WHOld[0].getValue('custrecord_ebizwhsite');
			nlapiLogExecution('ERROR', 'MWHSiteFlag', MWHSiteFlag);
			WHOld=null;
		} 
	} 
	if(MWHSiteFlag != null && MWHSiteFlag !="" )
	{
		if(MWHSiteFlag=='F')
			retFlag='N';
		else
			retFlag='Y';

	}
	nlapiLogExecution('ERROR', 'retFlag', retFlag);
	nlapiLogExecution('ERROR', 'Out of GetMWFlag ', '');
	return retFlag;
}


function CheckLPinInvt(vlp,vnewBinlocationId,getLOTId,Itype,batchlot)
{
	try
	{

		var searchresults1='';
		nlapiLogExecution('ERROR', 'vlp', vlp);
		
		nlapiLogExecution('ERROR', 'Itype', Itype);
		nlapiLogExecution('ERROR', 'batchlot', batchlot);
		nlapiLogExecution('ERROR', 'vnewBinlocationId', vnewBinlocationId);
		var filters1 = new Array();				
		//filters1[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lpno);
		filters1[0] = new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', vlp);
		
		filters1[1] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'noneof', vnewBinlocationId);
		/*if(Itype == "lotnumberedinventoryitem" || Itype == "lotnumberedassemblyitem" || batchlot=='T')
		{
			nlapiLogExecution('ERROR', 'getLOTId', getLOTId);
			filters1[2] = new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', getLOTId);
		}*/
		var columns1=new Array();
		columns1[0]=new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
		columns1[1]=new nlobjSearchColumn('custrecord_ebiz_inv_lot');
		
		searchresults1 = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters1,columns1);		
		if(searchresults1!=null && searchresults1!=null && searchresults1.length>0)
		{
			var intLotid=searchresults1[0].getValue('custrecord_ebiz_inv_lot');
			var intBinlocid=searchresults1[0].getValue('custrecord_ebiz_inv_binloc');
			nlapiLogExecution('ERROR', 'intLotid', intLotid);
			nlapiLogExecution('ERROR', 'intBinlocid', intBinlocid);
			if(intBinlocid!=vnewBinlocationId)
			{
				return searchresults1;
			}
			else if(intBinlocid==vnewBinlocationId)
			{
				if(Itype == "lotnumberedinventoryitem" || Itype == "lotnumberedassemblyitem" || batchlot=='T')
				{
					nlapiLogExecution('ERROR', 'getLOTId', getLOTId);
					if(intLotid!=getLOTId)
					{
						return searchresults1;
					}
					else
					{
						return '';
					}
				}
				else
				{
					return '';
				}
			}
			nlapiLogExecution('ERROR', 'LP exists in Invt', searchresults1);
			//return searchresults1;
			//case# 20148321 ends
		}
		else
		{
			nlapiLogExecution('ERROR', 'in else', getLOTId);
			//return searchresults1;
			var filters1 = new Array();				
			//filters1[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', lpno);
			filters1[0] = new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', vlp);

			filters1[1] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', vnewBinlocationId);
			/*if(Itype == "lotnumberedinventoryitem" || Itype == "lotnumberedassemblyitem" || batchlot=='T')
			{
				nlapiLogExecution('ERROR', 'getLOTId', getLOTId);
				filters1[2] = new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', getLOTId);
			}*/
			var columns1=new Array();
			columns1[0]=new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
			columns1[1]=new nlobjSearchColumn('custrecord_ebiz_inv_lot');

			searchresults1 = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters1,columns1);
			if(searchresults1!=null && searchresults1!=null && searchresults1.length>0)
			{
				var intLotid=searchresults1[0].getValue('custrecord_ebiz_inv_lot');
				var intBinlocid=searchresults1[0].getValue('custrecord_ebiz_inv_binloc');
				nlapiLogExecution('ERROR', 'intLotid', intLotid);
				nlapiLogExecution('ERROR', 'intBinlocid', intBinlocid);

				if(Itype == "lotnumberedinventoryitem" || Itype == "lotnumberedassemblyitem" || batchlot=='T')
				{
					nlapiLogExecution('ERROR', 'getLOTId', getLOTId);
					if(intLotid!=getLOTId)
					{
						return searchresults1;
					}
					else
					{
						return '';
					}
				}
				else
				{
					return '';
				}
			}
			else
			{
				return '';
			}
			nlapiLogExecution('ERROR', 'LP exists in Invt', searchresults1);
		}

	}
	catch(e)
	{
		nlapiLogExecution('ERROR', 'exception in check LP  ', e);
	}
}