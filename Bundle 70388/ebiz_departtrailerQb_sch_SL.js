/***************************************************************************
 eBizNET SOLUTIONS LTD
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_departtrailerQb_sch_SL.js,v $
 *     	   $Revision: 1.1.2.2 $
 *     	   $Date: 2014/06/16 06:39:45 $
 *     	   $Author: spendyala $
 *     	   $Name: t_NSWMS_2014_1_3_125 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_departtrailerQb_sch_SL.js,v $
 * Revision 1.1.2.2  2014/06/16 06:39:45  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148885
 *
 * Revision 1.1.2.1  2014/05/22 22:14:14  snimmakayala
 * Case #: 20148508
 *
 * Revision 1.1  2011/11/23 11:15:53  schepuri
 * CASE201112/CR201113/LOG201121
 * splitting load and depart trailer functionality
 *
 
 *****************************************************************************/

/**
 * This function is the main function which call the fill functions.
 * 	The parameters are the request and the response.
 * @param request
 * @param response
 */
function DepartTrailerQb(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Depart Trailer');

		var trailer = form.addField('custpage_trailer', 'select', 'Trailer','customrecord_ebiznet_trailer');
		trailer.setLayoutType('startrow', 'none');
		
		form.addSubmitButton('Display');
		response.writePage(form);
	}
	else{
		var vtrailerno = request.getParameter('custpage_trailer');
		
		var DepartTrailerArray = new Array();
		DepartTrailerArray["custpage_trailer"] = vtrailerno;
				 
		response.sendRedirect('SUITELET', 'customscript_ebiz_departtrailersch', 'customdeploy_ebiz_departtrailersch', false, DepartTrailerArray);
	}
}