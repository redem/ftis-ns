/***************************************************************************
????????????????????????????eBizNET
?????????????????????eBizNET Solutions Inc
 ****************************************************************************
 *
 *? $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Masters/Client/ebiz_PutawayZone_CL.js,v $
 *? $Revision: 1.2.4.1 $
 *? $Date: 2012/02/09 16:10:57 $
 *? $Author: snimmakayala $
 *? $Name: t_NSWMS_LOG201121_26 $
 *
 * DESCRIPTION
 *? Functionality
 *
 * REVISION HISTORY
 *? $Log: ebiz_PutawayZone_CL.js,v $
 *? Revision 1.2.4.1  2012/02/09 16:10:57  snimmakayala
 *? CASE201112/CR201113/LOG201121
 *? Physical and Virtual Location changes
 *?
 *? Revision 1.4  2012/02/09 14:43:42  snimmakayala
 *? CASE201112/CR201113/LOG201121
 *? Physical and Virtual Location changes
 *?
 *? Revision 1.3  2012/01/10 07:19:33  spendyala
 *? CASE201112/CR201113/LOG201121
 *?  added code to remove space.
 *?
 *
 ****************************************************************************/

/**
 * @param type
 * @returns {Boolean}
 */
function fnCheckPUTWZone(type)
{
	var varZoneID =  nlapiGetFieldValue('name');  
	var vComapny	= nlapiGetFieldValue('custrecord_ebizcompanyput');  	 
	var vSite	= nlapiGetFieldValue('custrecord_ebizsiteput');
	var vId = nlapiGetFieldValue('id');
	if(varZoneID != "" && varZoneID != null && vSite != "" && vSite != null && vCompany != ""  && vCompany != null)
	{
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('name', null, 'is',varZoneID.replace(/\s+$/, ""));//Added by suman on 06/01/12 to trim spaces at right.
		filters[1] = new nlobjSearchFilter('custrecord_ebizcompanyput', null, 'anyof',['@NONE@',vComapny]);	 
		filters[2] = new nlobjSearchFilter('custrecord_ebizsiteput', null, 'anyof',['@NONE@',vSite]);

		if (vId != null && vId != "") {
			filters[3] = new nlobjSearchFilter('internalid', null, 'noneof', vId);
		}

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_putpick_zone', null, filters);


		if(searchresults)
		{
			alert("This Record already exists.");
			return false;
		}
		else
		{
			return true;
		} 
	}
	else
	{
		return true;
	}

}