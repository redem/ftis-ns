/***************************************************************************
                                                                 
                                                  eBizNET Solutions Inc 
****************************************************************************
*
*  $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inbound/Suitelet/ebiz_POReceipt_SL.js,v $
*  $Revision: 1.6.2.2.4.1.4.1 $
*  $Date: 2013/09/11 15:23:51 $
*  $Author: rmukkera $
*  $Name: t_NSWMS_2014_1_1_174 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_POReceipt_SL.js,v $
*  Revision 1.6.2.2.4.1.4.1  2013/09/11 15:23:51  rmukkera
*  Case# 20124376
*
*
****************************************************************************/
function poReceiptConceptSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		//create the Suitelet form
		var form = nlapiCreateForm('PO Receipt');

		//Calling Client Script.
		form.setScript('customscriptporeceipt_client');


		if(request.getParameter('custparam_poid')!=null)
		{


			// nlapiLogExecution('ERROR', 'PO Id from Client', request.getParameter('custparam_poid'));

			var poField = form.addField('custpage_po', 'text', 'Purchase Order');
			poField.setLayoutType('normal', 'startcol');

			var poNumField = form.addField('custpage_ponum', 'text', 'PO ID');
			poNumField.setDefaultValue(request.getParameter('custparam_poid'));
			poField.setLayoutType('normal', 'startrow');

			var receiptField = form.addField('custpage_receipt', 'text', 'Receipt');

			receiptField.setLayoutType('normal', 'startrow');

			var bolField = form.addField('custpage_bol', 'text', 'BOL');
			bolField.setLayoutType('normal', 'startrow');

			var trlField = form.addField('custpage_trailer', 'select', 'Trailer', 'customrecord_ebiznet_trailer');
			trlField.setLayoutType('normal', 'startrow');

			//Adding  ItemSubList
			var ItemSubList = form.addSubList("custpage_receipt_items", "inlineeditor", "ItemList");
			//	ItemSubList.addField("custpage_chkn_sel", "checkbox","Check-In");
			ItemSubList.addField("custpage_receipt_line", "text", "Line #");//.setDefaultValue(request.getParameter('custparam_po_line_num'));
			//ItemSubList.addField("custpage_receipt_sku", "select", "Item","item");
			var item = ItemSubList.addField('custpage_receipt_sku', 'select', 'Item', 'item');    	
			item.setDisabled(true);
			ItemSubList.addField("custpage_receipt_skustatus", "select", "Item Status", "customrecord_ebiznet_sku_status").setDefaultValue('1');
			ItemSubList.addField("custpage_receipt_lotbatch", "text", "LOT#");
			ItemSubList.addField("custpage_receipt_pc", "text", "Pack Code").setDefaultValue('1');
			ItemSubList.addField("custpage_receipt_buom", "text", "Base UOM").setDefaultValue('EACH');       
			ItemSubList.addField("custpage_receipt_expqty", "text", "Exp Qty");
			ItemSubList.addField("custpage_receipt_rcvngqty", "text", "Rcv Qty");  


			var lineno= request.getParameter('custparam_polinenum');

			var searchresults = nlapiLoadRecord('purchaseorder', request.getParameter('custparam_poid')); //1020

			var vponum,vline,vitem,vordqty,vavlqty,vpoid,vitemstatus,vpackcode;

			vponum= searchresults.getFieldValue('tranid');
			vline = searchresults.getLineItemValue('item','line',lineno);
			vitem=  searchresults.getLineItemValue('item','item',lineno);
			vordqty=searchresults.getLineItemValue('item','quantity',lineno);
			vpoid=searchresults.getId();
			vitemstatus=searchresults.getLineItemValue('item','custcol_ebiznet_item_status',lineno);
			vpackcode=searchresults.getLineItemValue('item','custcol_nswmspackcode',lineno);

			var filters = new Array();
			filters[0] = new nlobjSearchFilter('custrecord_ebiz_poreceipt_pono', null, 'is', vpoid);

			var columns = new Array();                              
			columns[0] = new nlobjSearchColumn('custrecord_ebiz_poreceipt_receiptno');


			var searchresultsRcpt = nlapiSearchRecord('customrecord_ebiznet_trn_poreceipt', null, filters, columns);	         
			var vline,vitem,vqty,vso,vmainline,vrecid,vreceiptno,vlocation,vLpno;
			vreceiptno="";

			for (var i = 0; searchresultsRcpt != null && i < searchresultsRcpt.length; i++) {
				var searchresultsRcpt = searchresultsRcpt[i];	      

				vrecid= searchresultsRcpt.getId();    
				vreceiptno = searchresultsRcpt.getValue('custrecord_ebiz_poreceipt_receiptno');

			}                            

			poField.setDefaultValue(vponum);

			receiptField.setDefaultValue(vreceiptno);

			if(vreceiptno=="")
				receiptField.setDefaultValue(vponum+".1");
			else
			{

				var receipt=vreceiptno.split(".");
				vreceiptno=receipt[0];
				var cnt=parseFloat(receipt[1])+1;	      
				receiptField.setDefaultValue(vreceiptno+"."+cnt );                            

			}              

			form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_line',  1, vline);                       
			form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_sku',  1, vitem);   
			form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_expqty',1, vordqty);
			form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_rcvngqty',1, vordqty);
			form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_skustatus',1, vitemstatus);
			form.getSubList('custpage_receipt_items').setLineItemValue('custpage_receipt_pc',1, vpackcode);

			form.addSubmitButton('Receipt');

		}
		else
		{
			// show inline message
			showInlineMessage(form, 'Confirmation', 'PO Receipt Added Successfully', null);
		}

		//   form.addButton("custpage_genlocaion", "Generate Locations", 'myGenerateLocationButton()');

		response.writePage(form);
	}
	else //this is the POST block
	{
		nlapiLogExecution('ERROR', 'Into Response', 'Into Response');
		//extract the values from the previous page
		var po = request.getParameter('custpage_po');
		var bol= request.getParameter('custpage_bol');
		var trailer= request.getParameter('custpage_trailer');
		var poValue = request.getParameter('custpage_po');
		var quan = request.getParameter('custpage_item_quan');
		var lineno = request.getParameter('custpage_line_itemlineno');
		var remQty = request.getParameter('custpage_remainitem_quan');
		var lineCnt = request.getLineItemCount('custpage_receipt_items');
		var receipt=request.getParameter('custpage_receipt');
		var poid=request.getParameter('custpage_ponum');

		nlapiLogExecution('ERROR', 'Into line Count', lineCnt);
		var now = new Date();
		//a Date object to be used for a random value

		for (var s = 1; s <= lineCnt; s++) {

			//Retreiving line item details
			var ItemName = request.getLineItemValue('custpage_receipt_items', 'custpage_receipt_sku', s);
			var ItemStatus = request.getLineItemValue('custpage_receipt_items', 'custpage_receipt_skustatus', s);
			var PackCode = request.getLineItemValue('custpage_receipt_items', 'custpage_receipt_pc', s);
			var BaseUOM = request.getLineItemValue('custpage_receipt_items', 'custpage_receipt_buom', s);
			//var LP = request.getLineItemValue('custpage_receipt_items', 'custpage_chkn_lp', s);
			//var BinLoc = request.getLineItemValue('custpage_receipt_items', 'custpage_chkn_binlocation', s);
			var ExpQty = request.getLineItemValue('custpage_receipt_items', 'custpage_receipt_expqty', s);
			var RcvQty = request.getLineItemValue('custpage_receipt_items', 'custpage_receipt_rcvngqty', s);
			var poline = request.getLineItemValue('custpage_receipt_items', 'custpage_receipt_line', s);


			//create the openTask record With PUTW Task Type



			var rcptrecord = nlapiCreateRecord('customrecord_ebiznet_trn_poreceipt');
			nlapiLogExecution('ERROR', 'Creating Receipt record', 'TRN_RECEIPT');

			var str = 'po. = ' + po + '<br>';
			str = str + 'poid. = ' + poid + '<br>';	
			str = str + 'bol. = ' + bol + '<br>';
			str = str + 'poline. = ' + poline + '<br>';
			str = str + 'RcvQty. = ' + RcvQty + '<br>';
			str = str + 'ExpQty. = ' + ExpQty + '<br>';
			str = str + 'receipt. = ' + receipt + '<br>';
			str = str + 'trailer. = ' + trailer + '<br>';
			str = str + 'ItemName. = ' + ItemName + '<br>';
			str = str + 'ItemStatus. = ' + ItemStatus + '<br>';
			str = str + 'PackCode. = ' + PackCode + '<br>';

			nlapiLogExecution('ERROR', 'Input Values', str);

			//populating the fields
			rcptrecord.setFieldValue('name', po);
			rcptrecord.setFieldValue('custrecord_ebiz_poreceipt_pono', poid); // 'need to pass internal id here'
			rcptrecord.setFieldValue('custrecord_ebiz_poreceipt_bol', bol);
			rcptrecord.setFieldValue('custrecord_ebiz_poreceipt_lineno', poline);
			rcptrecord.setFieldValue('custrecord_ebiz_poreceipt_receiptqty', RcvQty);
			rcptrecord.setFieldValue('custrecord_ebiz_poreceipt_polineordqty', ExpQty);
			rcptrecord.setFieldValue('custrecord_ebiz_poreceipt_receiptno', receipt);
			rcptrecord.setFieldValue('custrecord_ebiz_poreceipt_trailerno', trailer);
			rcptrecord.setFieldValue('custrecord_ebiz_poreceipt_item', ItemName);
			rcptrecord.setFieldValue('custrecord_ebiz_poreceipt_itemstatus', ItemStatus);
			rcptrecord.setFieldValue('custrecord_ebiz_poreceipt_packcode', PackCode);

			//commit the record to NetSuite
			var rcptrecordid = nlapiSubmitRecord(rcptrecord);
			nlapiLogExecution('ERROR', 'Done Receipt Record Insertion ', 'TRN_RECEIPT');


		}//end of For loop lines count.
		//redirect to the record in view mode(customscript_assignputaway)


		response.sendRedirect('SUITELET', 'customscript_ebiznet_poreceipt', 'customdeploy_poreceipt', false, null);

		//onSave();
		var POarray = new Array();
		POarray["custparam_poid"] = poid;
		// POarray["custparam_povalue"] = poValue;

		nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
		// response.sendRedirect('SUITELET', 'customscript_ebiznet_poreceipt', 'customdeploy_poreceipt', false, POarray);
	}
}

