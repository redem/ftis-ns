/***************************************************************************
	  		   eBizNET Solutions Inc
 ****************************************************************************/
/* Prologue - INTFMGR_defaults.sql
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_TOCheckinQty.js,v $
 *     	   $Revision: 1.3.4.11.4.6.2.18 $
 *     	   $Date: 2015/05/08 15:58:00 $
 *     	   $Author: rrpulicherla $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 * DESCRIPTION
 * REVISION HISTORY
 * $Log: ebiz_RF_TOCheckinQty.js,v $
 * Revision 1.3.4.11.4.6.2.18  2015/05/08 15:58:00  rrpulicherla
 * Case#201412277
 *
 * Revision 1.3.4.11.4.6.2.17  2014/06/23 06:42:23  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox) Issue Fixed.
 *
 * Revision 1.3.4.11.4.6.2.16  2014/06/13 05:55:33  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.3.4.11.4.6.2.15  2014/06/06 07:56:55  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.3.4.11.4.6.2.14  2014/06/03 13:57:28  skreddy
 * case # 20148476
 * Sonic SB issue fix
 *
 * Revision 1.3.4.11.4.6.2.13  2014/05/30 00:26:51  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.3.4.11.4.6.2.12  2014/05/26 15:04:49  skreddy
 * case # 20148500
 * Sonic SB issue fix
 *
 * Revision 1.3.4.11.4.6.2.11  2014/02/17 14:46:45  rmukkera
 * Case # 20127193
 *
 * Revision 1.3.4.11.4.6.2.10  2013/10/03 12:59:42  rmukkera
 * Case# 20124742�
 *
 * Revision 1.3.4.11.4.6.2.9  2013/09/16 14:21:32  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to Blind item receipt is fixed.
 *
 * Revision 1.3.4.11.4.6.2.8  2013/09/02 15:37:32  rmukkera
 * Case# 20124187
 *
 * Revision 1.3.4.11.4.6.2.7  2013/08/26 15:28:00  rmukkera
 * Case#  20124062
 *
 * Revision 1.3.4.11.4.6.2.6  2013/08/22 06:58:57  snimmakayala
 * Case# 20123979
 * NLS - UAT ISSUES(INETR COMPANY XFER)
 *
 * Revision 1.3.4.11.4.6.2.5  2013/07/15 14:26:38  rrpulicherla
 * case# 20123434
 * GFT Issue fixes
 *
 * Revision 1.3.4.11.4.6.2.4  2013/07/15 11:36:45  snimmakayala
 * GFT UAT ISSUE
 * Case# : 20123427
 *
 * Revision 1.3.4.11.4.6.2.3  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.3.4.11.4.6.2.2  2013/05/15 01:31:23  kavitha
 * CASE201112/CR201113/LOG201121
 * TSG Issue fixes
 *
 * Revision 1.3.4.11.4.6.2.1  2013/04/17 16:04:01  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.3.4.11.4.6  2013/02/15 14:59:20  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.3.4.11.4.5  2012/12/11 14:53:00  schepuri
 * CASE201112/CR201113/LOG201121
 * upc code issue
 *
 * Revision 1.3.4.11.4.4  2012/12/03 15:41:06  rmukkera
 * CASE201112/CR201113/LOG2012392
 * UOM conversions code added
 *
 * Revision 1.3.4.11.4.3  2012/11/01 14:55:35  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.3.4.11.4.2  2012/09/27 10:55:14  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.3.4.11.4.1  2012/09/21 14:57:16  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.3.4.11  2012/05/28 15:29:05  spendyala
 * CASE201112/CR201113/LOG201121
 * passing parameter to query string  was missing.
 *
 * Revision 1.3.4.10  2012/04/25 15:37:13  spendyala
 * CASE201112/CR201113/LOG201121
 * While Showing Recommended qty POoverage is not considered.
 *
 * Revision 1.3.4.9  2012/04/23 14:06:36  schepuri
 * CASE201112/CR201113/LOG201121
 *  validation on checkin once after checkin already completed
 *
 * Revision 1.3.4.8  2012/04/16 14:59:35  spendyala
 * CASE201112/CR201113/LOG201121
 * Calculating POoverage is moved to general function.
 *
 * Revision 1.3.4.7  2012/04/16 08:35:24  vrgurujala
 * t_NSWMS_LOG201121_120
 *
 * Revision 1.3.4.6  2012/04/13 22:18:00  spendyala
 * CASE201112/CR201113/LOG201121
 * Depending upon POoverage Value mentioned in the item master
 * value of poOverage will be calculated accordingly.
 *
 * Revision 1.3.4.5  2012/04/11 12:51:51  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * display pickface locand qty
 *
 * Revision 1.3.4.4  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.3.4.3  2012/03/14 07:24:47  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Checkin issues for WBC
 *
 * Revision 1.5  2012/03/14 07:11:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Checkin issues for WBC
 *
 * Revision 1.4  2012/02/16 10:53:12  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 * Revision 1.3  2011/09/29 11:34:49  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * TO Functionality New Files
 *
 * Revision 1.2  2011/09/29 10:40:27  spendyala
 * CASE201112/CR201113/LOG201121
 * for an event  ENTER is pressed , re-direct page is been changed
 *
 * Revision 1.1  2011/09/28 11:37:38  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RMA Functionality New Files
 *
 * Revision 1.1  2011/09/22 08:37:21  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RMA Functionality New Files
 *
 *
 *****************************************************************************/

function TOCheckInQty(request, response){
	if (request.getMethod() == 'GET') {
		var ItemDescription;
		var ItemQuantity;
		var ItemQuantityReceived;
		var ItemPackCode;
		var ItemStatus;
		var ItemCube;
		var ItemQuantityfulfilled;
		var toItemUOM='';
		var item_id='';
		var vuomqty;
		var vbaseuomqty=0;
		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);

		//	Get the PO#, PO Line Item, Line#, Entered Item and PO Internal Id 
		//  from the previous screen, which is passed as a parameter	
		var getPONo = request.getParameter('custparam_poid');
		var getPOItem = request.getParameter('custparam_poitem');
		nlapiLogExecution('DEBUG','getPOItem', getPOItem);
		var getPOLineNo = request.getParameter('custparam_lineno');
		var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		var getPOInternalId = request.getParameter('custparam_pointernalid');
		var getItemCube = request.getParameter('custparam_itemcube');
		var getItemBaseUOM =request.getParameter('custparam_baseuomqty'); 
		var getWHLocation = request.getParameter('custparam_whlocation');
		nlapiLogExecution('DEBUG','WH Location', getWHLocation);

		var enteredOption = "";
		var vbaseuom='';
		var vbaseuomqty='';
		var vuomqty='';
		var vuomlevel='';
		enteredOption = request.getParameter('custparam_enteredOption');

		// Load a record into a variable from Purchase Order for the PO Internal Id
		var PORec = nlapiLoadRecord('transferorder', getPOInternalId);

		// Fetched the count of lines available in the Purchase Order
		var LineItemCount = PORec.getLineItemCount('item');

		nlapiLogExecution('DEBUG','LineItemCount', LineItemCount);

		// Loop into fetch the PO Line details for the line# passed.
		for (var i = 1; i <= LineItemCount; i++) {
			var lineno = PORec.getLineItemValue('item', 'line', i);

			nlapiLogExecution('DEBUG','lineno', lineno);
			nlapiLogExecution('DEBUG','getPOLineNo', getPOLineNo);

			if (lineno == getPOLineNo) {
				ItemDescription = PORec.getLineItemValue('item', 'description', i);
				toItemUOM = PORec.getLineItemValue('item', 'units', i);
				item_id = PORec.getLineItemText('item', 'item', i);
				if (ItemDescription == null) {
					var poItemFields = ['custitem_ebizdescriptionitems'];
					var poItemColumns = nlapiLookupField('item', getFetchedItemId, poItemFields);

					ItemDescription = poItemColumns.custitem_ebizdescriptionitems;
					if (ItemDescription == null)
						ItemDescription = '';
				}
				ItemDescription = ItemDescription.substring(0, 20);

				ItemQuantity = PORec.getLineItemValue('item', 'quantity', i);
				nlapiLogExecution('DEBUG','ItemQuantity', ItemQuantity);
				if (ItemQuantity == null) {
					ItemQuantity = '';
				}

				nlapiLogExecution('DEBUG','ItemQuantity', ItemQuantity);

				ItemQuantityfulfilled = PORec.getLineItemValue('item', 'quantityfulfilled', i);
				nlapiLogExecution('DEBUG','ItemQuantityfulfilled', ItemQuantityfulfilled);
				if (ItemQuantityfulfilled == null || ItemQuantityfulfilled == '') {
					ItemQuantityfulfilled = 0;
				}

				if(parseFloat(ItemQuantity) > parseFloat(ItemQuantityfulfilled))
					ItemQuantity = ItemQuantityfulfilled;

				ItemQuantityReceived = PORec.getLineItemValue('item', 'quantityreceived', i);
				nlapiLogExecution('DEBUG','ItemQuantityReceived', ItemQuantityReceived);
				if (ItemQuantityReceived == null) {
					ItemQuantityReceived = '';
				}

				ItemPackCode = PORec.getLineItemText('item', 'custcol_nswmspackcode', i);
				if (ItemPackCode == null) {
					ItemPackCode = '';
				}

				ItemStatus = PORec.getLineItemText('item', 'custcol_ebiznet_item_status', i);
				if (ItemStatus == null) {
					ItemStatus = '';
				}

				break;
			}
		}

		if(toItemUOM!=null && toItemUOM!='')
		{
			var eBizItemDims=geteBizItemDimensions(getFetchedItemId);
			if(eBizItemDims!=null&&eBizItemDims.length>0)
			{
				nlapiLogExecution('DEBUG', 'Item Dimesions Length', eBizItemDims.length);
				for(z=0; z < eBizItemDims.length; z++)
				{

					if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
					{
						vbaseuom = eBizItemDims[z].getText('custrecord_ebizuomskudim');
						vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
					}
					nlapiLogExecution('DEBUG', 'toItemUOM', toItemUOM);
					nlapiLogExecution('DEBUG', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
					nlapiLogExecution('DEBUG', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
					if(toItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
					{
						vuomlevel = eBizItemDims[z].getText('custrecord_ebizuomlevelskudim');
						vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
					}
				}

				nlapiLogExecution('DEBUG','vuomqty tst', vuomqty);
				nlapiLogExecution('DEBUG','vbaseuomqty tst', vbaseuomqty);
				nlapiLogExecution('DEBUG','ItemQuantityReceived tst', ItemQuantityReceived);

				if(vuomqty==null || vuomqty=='')
				{
					vuomqty=vbaseuomqty;
				}

				if(ItemQuantity==null || ItemQuantity=='' || isNaN(ItemQuantity))
					ItemQuantity=0;
				else
					ItemQuantity = (parseFloat(ItemQuantity)*parseFloat(vuomqty))/parseFloat(vbaseuomqty);

				if(ItemQuantityReceived==null || ItemQuantityReceived=='' || isNaN(ItemQuantityReceived))
					ItemQuantityReceived=0;
				else
					ItemQuantityReceived = (parseFloat(ItemQuantityReceived)*parseFloat(vuomqty))/parseFloat(vbaseuomqty);			

			}
		}


		nlapiLogExecution('DEBUG','ItemQuantity', ItemQuantity);
		nlapiLogExecution('DEBUG','ItemQuantityReceived', ItemQuantityReceived);


		/*
		 * The below part of the code is to check if the PO Overage is allowed or not. 
		 * If the PO Overages are allowed, then the recommended quantity should be the pallet quantity.
		 * If the PO Overages are not allowed, then the recommended quantity should be the remaining quantity.
		 */        

		var poOverage=GetPoOverage(getFetchedItemId,getPOInternalId,getWHLocation);

		//var poOverage = checkPOOverage(getPOInternalId,getWHLocation, null);
		nlapiLogExecution('DEBUG','poOverage', poOverage);

//		var ItemRemaininingQuantity = parseFloat(ItemQuantity) - parseFloat(ItemQuantityReceived);

		var ItemRemaininingQuantity = itemRemainingQuantity(getPOInternalId, getFetchedItemId, getPOLineNo,
				ItemQuantity, ItemQuantityReceived, getWHLocation, null);

		nlapiLogExecution('DEBUG','ItemRemaininingQuantity', ItemRemaininingQuantity);

		var palletQuantity = fetchPalletQuantity(getFetchedItemId,getWHLocation,null); //'9999';
		nlapiLogExecution('DEBUG','palletQuantity', palletQuantity);
		var ItemRecommendedQuantity =0;
		var hdnItemRecmdQtyWithPOoverage=0;
		/*nlapiLogExecution('DEBUG','ItemRemaininingQuantity', ItemRemaininingQuantity);
		nlapiLogExecution('DEBUG','palletQuantity', palletQuantity);
		nlapiLogExecution('DEBUG','ItemQuantity', ItemQuantity);*/

		if (poOverage == null){
			poOverage = 0;	
		}
		//else{
//		ItemRecommendedQuantity = Math.min (ItemRemaininingQuantity + (poOverage * ItemQuantity)/100, palletQuantity);

		hdnItemRecmdQtyWithPOoverage = Math.min (ItemRemaininingQuantity + (poOverage * ItemQuantity)/100, palletQuantity);
		ItemRecommendedQuantity = Math.min (ItemRemaininingQuantity, palletQuantity);

		nlapiLogExecution('DEBUG','ItemRecommendedQuantity', ItemRecommendedQuantity);

		/*
			if(palletQuantity > ItemRemaininingQuantity){
				ItemRecommendedQuantity = ItemRemaininingQuantity;
			}
			else if (palletQuantity < ItemRemaininingQuantity){
				ItemRecommendedQuantity =  palletQuantity;
			}*/		

		//}
		nlapiLogExecution('DEBUG','ItemRecommendedQuantity', parseFloat(ItemRecommendedQuantity.toString()));
		var pickfaceRecommendedQuantity = priorityPutawayQuantity(getFetchedItemId,getWHLocation,null);
		nlapiLogExecution('DEBUG','Returned Pickface Recommended Quantity', pickfaceRecommendedQuantity);

		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime'); 
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');


		if(ItemRemaininingQuantity < 0)
			ItemRemaininingQuantity=0;


		nlapiLogExecution('DEBUG','ItemRemaininingQuantity', ItemRemaininingQuantity);
		nlapiLogExecution('DEBUG','palletQuantity', palletQuantity);
		nlapiLogExecution('DEBUG','ItemQuantity', ItemQuantity);

		var POarrayget = new Array();

		POarrayget["custparam_error"] = 'Check in already completed';
		POarrayget["custparam_screenno"] = '3TSKU';
		POarrayget["custparam_poid"] = request.getParameter('custparam_poid');

		//POarrayget["custparam_poitem"] = request.getParameter('custparam_poitem');
		POarrayget["custparam_poitem"] = item_id;

		POarrayget["custparam_lineno"] = request.getParameter('custparam_lineno');
		POarrayget["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
		POarrayget["custparam_pointernalid"] = request.getParameter('custparam_pointernalid');
		//POarrayget["custparam_trantype"] = request.getParameter('custparam_trantype');
		POarrayget["custparam_itemcube"] = request.getParameter('custparam_itemcube');
		POarrayget["custparam_whlocation"] = request.getParameter('custparam_whlocation');
		POarrayget["custparam_baseuomqty"] = request.getParameter('custparam_baseuomqty');
		POarrayget["custparam_actualbegintime"] = getActualBeginTime; //TimeArray[0];
		POarrayget["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM;	//TimeArray[1];
		
//		Added by Narasimha inorder to get the receipt type based on po.

		var POblindreceiptfilters=new Array();
		POblindreceiptfilters.push(new nlobjSearchFilter('tranid',null,'is',getPONo));
		POblindreceiptfilters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));

		var blindreceiptColumns = new Array();
		blindreceiptColumns[0] = new nlobjSearchColumn('custbody_nswmsporeceipttype');

		var blindreceiptSearchResults = nlapiSearchRecord('transferorder', null, POblindreceiptfilters, blindreceiptColumns);

		var receiptType='';
		if(blindreceiptSearchResults!=null && blindreceiptSearchResults!='')
		{
			receiptType=blindreceiptSearchResults[0].getValue('custbody_nswmsporeceipttype');
		}

		var poBlindReceipt='';


		nlapiLogExecution('DEBUG','receiptType',receiptType);

		if (receiptType != "" && receiptType != null) 
		{

			var receiptFieldsRec = ['custrecord_ebiz_blindreceipt'];
			var receiptColumns = nlapiLookupField('customrecord_ebiznet_receipt_type', receiptType, receiptFieldsRec);

			poBlindReceipt = receiptColumns.custrecord_ebiz_blindreceipt;

			nlapiLogExecution('DEBUG','poBlindReceipt', poBlindReceipt);
		}


		//	Upto here.

		if(parseFloat(ItemRemaininingQuantity) == 0 && parseFloat(ItemRecommendedQuantity) == 0)
		{
			if(ItemQuantity==0)
				POarrayget["custparam_error"] = 'Item Not fulfilled';
			nlapiLogExecution('DEBUG','cknin completed ItemRemaininingQuantity', ItemRemaininingQuantity);
			nlapiLogExecution('DEBUG','cknin completed ItemRecommendedQuantity', ItemRecommendedQuantity);
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarrayget);
			return;
		}

		else
		{


			var getLanguage = request.getParameter('custparam_language');
			nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);

			var st0,st1,st2,st3,st4,st5,st6,st7,st8,st9;
			if( getLanguage == 'es_ES')
			{
				st0 = "";
				st1 = "ART&#205;CULO";
				st2 = "DESCRIPCI&#211;N DEL ART&#205;CULO";
				st3 = "C&#211;DIGO DE PAQUETE";
				st4 = "CANTIDAD RESTANTE";
				st5 = "CANTIDAD RECOMENDADA";
				st6 = "RECOGIDA CANTIDAD FACE RECOMENDADO";
				st7 = "CANTIDAD";
				st8 = "ENVIAR";
				st9 = "ANTERIOR";					
			}
			else
			{
				st0 = "";
				st1 = "ITEM";
				st2 = "ITEM DESC";
				st3 = "PACK CODE";
				st4 = "REMAINING QTY";
				st5 = "RECOMMENDED QTY";
				st6 = "PICKFACE RECOMMENDED QTY";
				st7 = "QTY";
				st8 = "SEND";
				st9 = "PREV";

			}
			var caseQty = fetchCaseQuantity(getFetchedItemId,getWHLocation,null);

			var functionkeyHtml=getFunctionkeyScript('_rf_checkin_qty'); 
			var html = "<html><head><title>" + st0 +  "</title>";
			html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
			html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
			//Case# 20148749 Refresh Functionality starts
			html = html + "var version = navigator.appVersion;";
			html = html + "document.onkeydown = function (e) {";
			html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
			html = html + "if ((version.indexOf('MSIE') != -1)) { ";
			html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
			html = html + "else {if (keycode == 116)return false;}";
			html = html + "};";
			//Case# 20148749 Refresh Functionality ends
			//html = html + " document.getElementById('enterqty').focus();";        
			html = html + "function stopRKey(evt) { ";
			//html = html + "	  alert('evt');";
			html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
			html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
			html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
			html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
			html = html + "	  alert('System Processing, Please wait...');";
			html = html + "	  return false;}} ";
			html = html + "	} ";

			html = html + "	document.onkeypress = stopRKey; ";
			html = html + "</script>";
			html = html +functionkeyHtml;
			html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
			html = html + "	<form name='_rf_checkin_qty' method='POST'>";
			html = html + "		<table>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st1 + " : <label>" + item_id + "</label>";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st2 + ": <label>" + ItemDescription + "</label>";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st3 + ": <label>" + ItemPackCode + "</label>";
			html = html + "				<input type='hidden' name='hdnItemPackCode' value=" + ItemPackCode + ">";
			html = html + "				<input type='hidden' name='hdnQuantity' value=" + ItemQuantity + ">";
			html = html + "				<input type='hidden' name='hdnQuantityReceived' value=" + ItemQuantityReceived + ">";
			html = html + "				<input type='hidden' name='hdnItemStatus' value='" + ItemStatus + "'></td>";
			html = html + "				<input type='hidden' name='hdnItemCube' value=" + getItemCube + "></td>";
			html = html + "				<input type='hidden' name='hdnItemBaseUOM' value=" + getItemBaseUOM + "></td>";
			html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
			html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
			html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
			html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
			html = html + "				<input type='hidden' name='hdnEnteredOption' value=" + enteredOption + ">";
			html = html + "				<input type='hidden' name='hdnPalletQuantity' value=" + palletQuantity + "></td>";
			//Case # 20127193 Start
			html = html + "				<input type='hidden' name='hdngetPOItem' value='" + item_id + "'></td>";
			//Case # 20127193 
			html = html + "				<input type='hidden' name='hdnItemRecmdQtyWithPOoverage' value=" + hdnItemRecmdQtyWithPOoverage + ">";
			//Added by Phani 03-25-2011
			html = html + "				<input type='hidden' name='hdnWhLocation' value=" + getWHLocation + ">";
			html = html + "				<input type='hidden' name='hdnpoOverage' value=" + poOverage + ">";
			html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
			html = html + "			</tr>";
			if(poBlindReceipt!='T'){
			html = html + "			<tr>";
			if(ItemRemaininingQuantity < 0)
				ItemRemaininingQuantity=0;
			/*//html = html + "				<td align = 'left'>" + st4 + ": <label>" + parseFloat(ItemRemaininingQuantity.toString()) + "</label>";
			html = html + "				<td align = 'left'>" + st4 + ": <label>" + parseFloat(ItemRemaininingQuantity).toFixed(4) + "</label>";
			html = html + "				<input type='hidden' name='hdnItemRemaininingQuantity' value=" + parseFloat(ItemRemaininingQuantity).toFixed(4) + "></td>";
			html = html + "			</tr>";*/
		
			html = html + "				<td align = 'left'> "+st4+" :  <label>" + parseFloat(ItemRemaininingQuantity).toFixed(4) + "</label>";
			var pikQtyBreakup = getQuantityBreakdown(caseQty,ItemRemaininingQuantity);
			html = html + "	("+caseQty+"/Ca:"+pikQtyBreakup+")";
			html = html + "				</td>";
			html = html + "			</tr>";
			/*html = html + "			<tr>";
			//html = html + "				<td align = 'left'>" + st5 + ": <label>" + parseFloat(ItemRecommendedQuantity.toString()) + "</label>";
			html = html + "				<td align = 'left'>" + st5 + ": <label>" + parseFloat(ItemRecommendedQuantity).toFixed(4) + "</label>";
			html = html + "				<input type='hidden' name='hdnRecommendedQuantity' value=" + parseFloat(ItemRecommendedQuantity).toFixed(4) + "></td>";
			html = html + "			</tr>";*/
			
			html = html + "			<tr>";
			html = html + "				<td align = 'left'> "+st5+" :  <label>" + parseFloat(ItemRecommendedQuantity).toFixed(4) + "</label>";
			var pikQtyBreakup = getQuantityBreakdown(caseQty,ItemRecommendedQuantity);
			html = html + "	("+caseQty+"/Ca:"+pikQtyBreakup+")";
			html = html + "				</td>";
			html = html + "			</tr>";
			}
			if(pickfaceRecommendedQuantity > 0)
			{
				html = html + "			<tr>";
				html = html + "				<td align = 'left'>" + st6 + ": <label>" + pickfaceRecommendedQuantity + "</label>";
				html = html + "				<input type='hidden' name='hdnPickfaceRecommendedQuantity' value=" + parseFloat(pickfaceRecommendedQuantity) + "></td>";
				html = html + "			</tr>";			
			}
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" +  st7 + ": ";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'><input name='enterqty' id='enterqty' type='text'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st8 +" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
			html = html + "					" + st9 + " <input name='cmdPrevious' type='submit' value='F7'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "		 </table>";
			html = html + "	</form>";

			//Case# 20148882 (added Focus Functionality for Textbox)
			 html = html + "<script type='text/javascript'>document.getElementById('enterqty').focus();</script>";
			html = html + "</body>";
			html = html + "</html>";

			response.write(html);
		}
	}
	else {
		nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is not null');

		var getActualBeginTime = request.getParameter('hdnActualBeginTime'); 
		var getActualBeginTimeAMPM = request.getParameter('hdnActualBeginTimeAMPM');

		var ActualBeginTime;

		// This variable is to hold the Quantity entered.
		var POarray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;

		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);

		var st10,st11,st12,st13;
		if( getLanguage == 'es_ES')
		{

			st10 = "CANTIDAD INV&#193;LIDA";
			st11 = "OBERTURA NO PERMITIDO";
			st12 = "CANTIDAD excede el l&#237;mite de 1 OBERTURA";
			st13 = "CANTIDAD DE PALETA no est&#225; definido para el art&#237;culo";
		}
		else
		{

			st10 = "INVALID QUANTITY";
			st11 = "OVERAGE NOT ALLOWED";
			st12 = "QUANTITY EXCEEDS OVERAGE LIMIT1";
			st13 = "PALLET QUANTITY IS NOT DEFINED FOR ITEM : ";
		}

		POarray["custparam_poid"] = request.getParameter('custparam_poid');
		//POarray["custparam_poitem"] = request.getParameter('custparam_poitem');
		POarray["custparam_poitem"] =  request.getParameter('hdngetPOItem');
		POarray["custparam_lineno"] = request.getParameter('custparam_lineno');
		POarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
		POarray["custparam_pointernalid"] = request.getParameter('custparam_pointernalid');
		POarray["custparam_poqtyentered"] = request.getParameter('enterqty');
		POarray["custparam_poitemremainingqty"] = request.getParameter('hdnItemRemaininingQuantity');
		POarray["custparam_polinepackcode"] = request.getParameter('hdnItemPackCode');
		POarray["custparam_polineitemstatus"] = request.getParameter('hdnItemStatus');
		POarray["custparam_polinequantity"] = request.getParameter('hdnQuantity');
		POarray["custparam_polinequantityreceived"] = request.getParameter('hdnQuantityReceived');
		POarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');

		POarray["custparam_itemcube"] = request.getParameter('hdnItemCube');
		POarray["custparam_baseuomqty"] =request.getParameter('custparam_baseuomqty');

		nlapiLogExecution('DEBUG', 'Itemcubeparam', POarray["custparam_itemcube"]);
		nlapiLogExecution('DEBUG', 'Item quantity entered', POarray["custparam_poqtyentered"]);

		POarray["custparam_actualbegindate"] = request.getParameter('hdnActualBeginDate');
		POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
		nlapiLogExecution('DEBUG', 'WH Location', POarray["custparam_whlocation"]);

		var poOverage = request.getParameter('hdnpoOverage');
//		var recommendedQuantity = request.getParameter('hdnRecommendedQuantity');
		var recommendedQuantity = request.getParameter('hdnItemRecmdQtyWithPOoverage');
		var palletQuantity = request.getParameter('hdnPalletQuantity');
		var pickfaceRecommendedQuantity = request.getParameter('hdnPickfaceRecommendedQuantity');
		var getEnteredQty=request.getParameter('enterqty');

		POarray["custparam_actualbegintime"] = getActualBeginTime; //TimeArray[0];
		POarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM;	//TimeArray[1];
		POarray["custparam_getPOItem"] = request.getParameter('hdngetPOItem');

		nlapiLogExecution('DEBUG', 'custparam_actualbegintime', POarray["custparam_actualbegintime"]);
		nlapiLogExecution('DEBUG', 'custparam_actualbegintimeampm', POarray["custparam_actualbegintimeampm"]);

		POarray["custparam_error"] = st10;

		POarray["custparam_screenno"] = '3T';

		var getOptedField = request.getParameter('hdnOptedField');
		POarray["custparam_option"] = getOptedField;

		nlapiLogExecution('DEBUG', 'optedField', POarray["custparam_option"]);

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		var bulkLocation = request.getParameter('hdnEnteredOption');
		nlapiLogExecution('DEBUG','Entered Option', bulkLocation);

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		if (optedEvent == 'F7') {

			response.sendRedirect('SUITELET', 'customscript_rf_tocheckin_sku', 'customdeploy_rf_tocheckin_sku_di', false, POarray);
		}
		else {
			//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
			//            if (optedEvent != '' && optedEvent != null) {

			nlapiLogExecution('DEBUG','Pickface Recommended Quantity', pickfaceRecommendedQuantity);
			if(getEnteredQty==null  || getEnteredQty=='' ||getEnteredQty=='null' )
			{
				POarray["custparam_error"] = 'Please Enter Qty';
				
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				//nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_poqtyentered"]);
				return;
			}

			if (poOverage == 0)
			{
				var recommendedQuantity1 = parseFloat(recommendedQuantity.toString());
				nlapiLogExecution('DEBUG','Quantity Entered', POarray["custparam_poqtyentered"]);
				nlapiLogExecution('DEBUG','Recommended Quantity', parseFloat(recommendedQuantity));

				if (parseFloat(POarray["custparam_poqtyentered"]) <= recommendedQuantity1)
				{
					if (POarray["custparam_poqtyentered"] > 0 && POarray["custparam_poqtyentered"] != "")
					{
						response.sendRedirect('SUITELET', 'customscript_rf_to_checkin_item_status', 'customdeploy_rf_to_checkin_item_status_d', false, POarray);
					}
					else{
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_poqtyentered"]);
					}
				}
				else 
				{
					//	if the 'Send' button is clicked without any option value entered,
					//  it has to show an error message. The next screen to which it has to navigate is to the error screen.
					POarray["custparam_error"] = st11;

					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_poqtyentered"]);
				}

				/*
				 * Check the pickface recommended quantity is less than the quantity entered.
				 * If the quantity entered is more than the pickface recommended quantity, ask the user to
				 * 		confirm if the quantity is to be putaway to the bulk location
				 * else
				 * 		follow the normal process of putting away the item to the assigned fixed pick location. 
				 */

				if (pickfaceRecommendedQuantity > 0)
				{
					if (parseFloat(POarray["custparam_poqtyentered"]) > parseFloat(pickfaceRecommendedQuantity))
					{	
						POarray["custparam_pickfaceexception"] = pickfaceRecommendedQuantity;
						/*
						 * If the option entered in the pickface exception is 'N', then allow the user to enter
						 * 	only the pickface recommended quantity
						 * else if the option entered in the pickface exception is 'Y', then generate a binlocation
						 * 	from the bulk location. Do not putaway to the pickface location.
						 */

						if (bulkLocation != 'Y' || bulkLocation == null)
						{
							nlapiLogExecution('DEBUG','Into Pickface Recommended Quantity Validation','here');
							response.sendRedirect('SUITELET', 'customscript_rf_pickface_exception', 'customdeploy_rf_pickface_exception_di', false, POarray);
							nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_pickfaceexception"]);
						}
					}
				}
			}
			else{
				var palletQuantity1 = parseFloat(palletQuantity.toString());
				var recommendedQuantity1 = parseFloat(recommendedQuantity.toString());

				if(palletQuantity1 >= 1)
				{
					if (POarray["custparam_poqtyentered"] > 0 && POarray["custparam_poqtyentered"] != "" 
						&& parseFloat(POarray["custparam_poqtyentered"]) <= recommendedQuantity1)
					{
						response.sendRedirect('SUITELET', 'customscript_rf_to_checkin_item_status', 'customdeploy_rf_to_checkin_item_status_d', false, POarray);

					}
					else{
						POarray["custparam_error"] = st12;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_poqtyentered"]);
					}
				}
				else
				{
					POarray["custparam_error"] = st13 + POarray["custparam_getPOItem"];
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				}
			}
		}
		nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
	}//end of else loop.
}

function fetchPalletQuantity(itemId, location, company)
{
	nlapiLogExecution('DEBUG','itemId',itemId);
	nlapiLogExecution('DEBUG','location in fetchPalletQuantity',location);
	var itemPalletQuantity = 0;
	var itemFilters = new Array();
	itemFilters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemId));
	//itemFilters.push(new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'is', 3);
	if(location!=null&&location!='')
		itemFilters.push(new nlobjSearchFilter('custrecord_ebizsiteskudim',null,'anyof',['@NONE@',location]));


	var itemColumns = new Array();
	itemColumns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	itemColumns[1] = new nlobjSearchColumn('custrecord_ebizsiteskudim');
	itemColumns[2] = new nlobjSearchColumn('custrecord_ebizcompanyskudimension');
	itemColumns[3] = new nlobjSearchColumn('custrecord_ebizqty'); 
	itemColumns[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim'); 	
	itemColumns[4].setSort(true);
	var itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, itemFilters, itemColumns);

	if (itemSearchResults != null)
	{
		itemPalletQuantity = itemSearchResults[0].getValue(itemColumns[3]);
		nlapiLogExecution('DEBUG','Item Pallet Quantity',itemPalletQuantity);
	}
	return itemPalletQuantity;
}

function checkPOOverage(POId,location,company){
	nlapiLogExecution('DEBUG','PO Internal Id', POId);
	var poOverage = 0;

	var poFields = ['custbody_nswmsporeceipttype'];
	var poColumns = nlapiLookupField('transaction', POId, poFields);

	var receiptType = poColumns.custbody_nswmsporeceipttype;

	nlapiLogExecution('DEBUG','here', receiptType);

	if (receiptType != "" && receiptType != null) 
	{
		var receiptFields = ['custrecord_po_overages'];
		var receiptColumns = nlapiLookupField('customrecord_ebiznet_receipt_type', receiptType, receiptFields);

		nlapiLogExecution('DEBUG','here', receiptType);

		poOverage = receiptColumns.custrecord_po_overages;
	}
	nlapiLogExecution('DEBUG','Out of check PO Overage', poOverage);
	return poOverage;
}

function itemRemainingQuantity(poInternalId, itemId, lineno, itemQuantity, itemQuantityReceived,location, company){
	/*
	 * Fetch the check-in, putconfirm quantity from the transaction order details for the PO# and the line#
	 * if check-in quantity is 0 (Zero)
	 * 	remaining quantity = order quantity
	 * else if check-in quantity has value 
	 *		remaining quantity = order quantity - (check-in quantity) - quantity received
	 */

	nlapiLogExecution('DEBUG','itemId',itemId);
	nlapiLogExecution('DEBUG','lineno',lineno);
	nlapiLogExecution('DEBUG','poInternalId',poInternalId);

	nlapiLogExecution('DEBUG','Item Remaining Quantity','About to calculate');

	var systemRule= GetSystemRuleForPostItemReceiptby();
	nlapiLogExecution('DEBUG','systemRule',systemRule);
	var checkinQuantity = 0;
	var remainingQuantity = 0;
	var putConfirmQuantity = 0;

	var transactionFilters = new Array();
	transactionFilters[0] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', poInternalId);
	transactionFilters[1] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_sku_no', null, 'equalto', itemId);
	transactionFilters[2] = new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', lineno);

	var transactionColumns = new Array();
	transactionColumns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_ebiz_sku_no');
	transactionColumns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_orderline_no');
	transactionColumns[2] = new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty');
	transactionColumns[3] = new nlobjSearchColumn('custrecord_orderlinedetails_putconf_qty');

	var transactionSearchresults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, transactionFilters, transactionColumns);

	if (transactionSearchresults != null && transactionSearchresults.length > 0)
	{
		for(var i = 0; i <= transactionSearchresults.length; i++)
		{
			checkinQuantity = transactionSearchresults[0].getValue('custrecord_orderlinedetails_checkin_qty');
			putConfirmQuantity = transactionSearchresults[0].getValue('custrecord_orderlinedetails_putconf_qty');
		}
	}

	if(checkinQuantity == "")
	{
		checkinQuantity = 0;
	}

	if(putConfirmQuantity == "")
	{
		putConfirmQuantity = 0;
	}

	nlapiLogExecution('DEBUG','Check-In Quantity',checkinQuantity);
	nlapiLogExecution('DEBUG','Quantity',itemQuantity);
	nlapiLogExecution('DEBUG','Received Quantity',itemQuantityReceived);
	nlapiLogExecution('DEBUG','Put Confirm Quantity',putConfirmQuantity);

	if (checkinQuantity == 0)
	{
		remainingQuantity = parseFloat(itemQuantity);
	}
	else if(systemRule !='PO')
	{
		remainingQuantity = parseFloat(itemQuantity) - (parseFloat(checkinQuantity)-parseFloat(putConfirmQuantity)) - parseFloat(itemQuantityReceived);
		nlapiLogExecution('DEBUG','Remaining Quantity',remainingQuantity);

		/*		if(putConfirmQuantity == 0)		
		{
			remainingQuantity = parseFloat(itemQuantity) - parseFloat(checkinQuantity) - parseFloat(itemQuantityReceived);
			nlapiLogExecution('DEBUG','Remaining Quantity: Checked-in',remainingQuantity);
		}
		else
		{
			remainingQuantity = parseFloat(itemQuantity) - parseFloat(itemQuantityReceived);
			nlapiLogExecution('DEBUG','Remaining Quantity: Putaway Confirmed',remainingQuantity);
		}
		 */
	}
	else
	{
		var GetTotalQty = GetTotalReceivedQty(poInternalId, itemId, lineno);
		nlapiLogExecution('DEBUG','GetTotalQty',GetTotalQty);
		var DirectQty = parseFloat(itemQuantityReceived)-(parseFloat(GetTotalQty));
		nlapiLogExecution('DEBUG','DirectQty',DirectQty);
		
		remainingQuantity = parseFloat(itemQuantity) - (parseFloat(checkinQuantity)+parseFloat(DirectQty));
		nlapiLogExecution('DEBUG','remainingQuantity',remainingQuantity);
	}
	return remainingQuantity;
}

function priorityPutawayQuantity(itemId, location, company)
{
	nlapiLogExecution('DEBUG','Priority Putaway Quantity','Beginning');

//	/*
//	* Search for the item in Pick Face Location custom record. Fetch the required columns 
//	* viz., Replen Quantity, Maximum Quantity, Bin Location


	var priorityPutawayQuantity = 0;



	var priorityPutawayFilters = new Array();
	priorityPutawayFilters.push(new nlobjSearchFilter('custrecord_pickfacesku', null,'is', itemId));
	priorityPutawayFilters.push(new nlobjSearchFilter('custrecord_priorityputaway', null, 'is', 'T'));	
	priorityPutawayFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	
	if(location!=null && location!='')
		priorityPutawayFilters.push(new nlobjSearchFilter('custrecord_pickface_location', null, 'anyof', [location]));

	var priorityPutawayColumns = new Array();
	priorityPutawayColumns[0] = new nlobjSearchColumn('custrecord_replenqty');
	priorityPutawayColumns[1] = new nlobjSearchColumn('custrecord_maxqty');
	priorityPutawayColumns[2] = new nlobjSearchColumn('custrecord_pickfacesku');
	priorityPutawayColumns[3] = new nlobjSearchColumn('custrecord_pickbinloc');

	var priorityPutawaySearchresults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc',null,priorityPutawayFilters,priorityPutawayColumns);

	if (priorityPutawaySearchresults != null)
	{
		for(var i = 0; i < priorityPutawaySearchresults.length; i++)
		{
			var ReplenQuantity = priorityPutawaySearchresults[0].getValue('custrecord_replenqty');
			var MaximumQuantity = priorityPutawaySearchresults[0].getValue('custrecord_maxqty');
			var pickfaceItem = priorityPutawaySearchresults[0].getValue('custrecord_pickfacesku');
			var binLocation = priorityPutawaySearchresults[0].getValue('custrecord_pickbinloc');
		}

		nlapiLogExecution('DEBUG','Maximum Quantity',MaximumQuantity);
		nlapiLogExecution('DEBUG','binLocation',binLocation);

//		/*
//		* Search for the data in inventory for the item, bin location and in storage locations. 
//		* Retrieve the quantity for those locations and sum the quantity for the individual lines fetched.

		var inventoryQuantity = 0;

		var inventoryFilters = new Array();
		inventoryFilters[0] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', itemId);
		inventoryFilters[1] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', binLocation);
		inventoryFilters[2] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3,19]);

		var inventoryColumns = new Array();
		inventoryColumns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');

		var inventorySearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, inventoryFilters, inventoryColumns);

		if (inventorySearchResults != null)
		{
			for(var i = 0; i < inventorySearchResults.length; i++)
			{
				inventoryQuantity = parseFloat(inventoryQuantity) + parseFloat(inventorySearchResults[i].getValue('custrecord_ebiz_qoh'));
			}
		}

		nlapiLogExecution('DEBUG','Inventory Quantity',parseFloat(inventoryQuantity));

//		/*
//		* Search for any open tasks viz., Putaway, Replenishment, Inventory Move tasks

		var opentaskInventoryQuantity = 0;

		var opentaskInventoryFilters = new Array();
		opentaskInventoryFilters[0] = new nlobjSearchFilter('custrecord_sku', null, 'is', itemId);
		opentaskInventoryFilters[1] = new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', binLocation);
		opentaskInventoryFilters[2] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2,8,9]);
		opentaskInventoryFilters[3] = new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', ['@NONE@']);

		var opentaskInventoryColumns = new Array();
		opentaskInventoryColumns[0] = new nlobjSearchColumn('custrecord_expe_qty');

		var opentaskInventorySearch = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, opentaskInventoryFilters, opentaskInventoryColumns);

		if (opentaskInventorySearch != null)
		{
			for (var i = 0; i < opentaskInventorySearch.length; i++)
			{
				opentaskInventoryQuantity = opentaskInventoryQuantity + parseFloat(opentaskInventorySearch[i].getValue('custrecord_expe_qty')); 
			}
		}
		nlapiLogExecution('DEBUG','Opentask Inventory Quantity',opentaskInventoryQuantity);
//		/*
//		* Calculate the Recommended quantity for the pick face location from the above quantity retrieved.

		var pickfaceRecommendedQuantity = parseFloat(MaximumQuantity) - (parseFloat(inventoryQuantity)+ parseFloat(opentaskInventoryQuantity));

		nlapiLogExecution('DEBUG','Pickface Recommended Quantity',pickfaceRecommendedQuantity);
	}
	return pickfaceRecommendedQuantity;
}
function geteBizItemDimensions(itemid)
{
	var searchRec = new Array();
	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemid));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim') ;
	column[1] = new nlobjSearchColumn('custrecord_ebizqty') ;
	column[2] = new nlobjSearchColumn('custrecord_ebizbaseuom') ;
	column[3] = new nlobjSearchColumn('custrecord_ebiznsuom') ;
	column[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim') ;
	searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);

	return searchRec;

}


function GetSystemRuleForPostItemReceiptby()
{
	try
	{
		var rulevalue='LP';	
		var filter=new Array();
		filter.push(new nlobjSearchFilter('name',null,'is','Post Item Receipt by'));
		//filter.push(new nlobjSearchFilter('custrecord_ebizsite',null,'anyof',['@NONE@',whLocation]));

		// case no start 20126968
		var vRoleLocation=getRoledBasedLocation();
		var resloc=new Array();
		resloc.push("@NONE@");
		
		nlapiLogExecution('ERROR','vRoleLocation',vRoleLocation);

		if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
		{
			for(var count=0;count<vRoleLocation.length;count++)
				resloc.push(vRoleLocation[count]);
			filter.push(new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', resloc));
		}
		// case no end 20126968
		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizrulevalue');

		var searchresult=nlapiSearchRecord('customrecord_ebiznet_sysrules',null,filter,column);

		if(searchresult!=null && searchresult!="")
			rulevalue=searchresult[0].getValue('custrecord_ebizrulevalue');
		nlapiLogExecution('ERROR','rulevalue',rulevalue);
		if(rulevalue!=null && rulevalue!='undefined' && rulevalue!='null' && rulevalue!='')
		{
			if(rulevalue.trim()!='LP' && rulevalue.trim()!='PO')
			{
				rulevalue='LP';
			}
		}
		else
		{
			rulevalue='LP';
		}
		nlapiLogExecution('ERROR','rulevalue',rulevalue);
		return rulevalue;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetSystemRuleForPostItemReceiptby',exp);
	}
}