function statusFieldChanged(type, name)
{
//  This particular code is 
// based on the Cycle count method flag , and disables/enables both standard and custom fields.


if (name == 'custrecord_cyclecountflag')
{
var disable = nlapiGetFieldValue('custrecord_cyclecountflag') == 'T'?false:true;
nlapiDisableField('custrecord_cyclecountmethod', disable);
nlapiDisableField('custrecord_tolerancelevel', disable);
nlapiDisableField('custrecord_thresholdtype', disable);

}
}

