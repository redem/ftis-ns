/***************************************************************************
           eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/ebiz_KitToOrderDetails_SL.js,v $
 *     	   $Revision: 1.6.10.1.4.1.8.1 $
 *     	   $Date: 2015/09/21 14:02:18 $
 *     	   $Author: deepshikha $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_8 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_KitToOrderDetails_SL.js,v $
 * Revision 1.6.10.1.4.1.8.1  2015/09/21 14:02:18  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.6.10.1.4.1  2013/03/01 14:34:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.6.10.1  2012/11/01 14:54:57  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.6  2011/10/11 18:19:24  mbpragada
 * CASE201112/CR201113/LOG201121
 * inserting sku status and packcode into create inventory and
 * generated lp automatically
 *
 * Revision 1.5  2011/09/30 08:43:07  sallampati
 * CASE2011263/CR2011260/LOG2011188
 * The version code 1.1.2.3 is merged with 1.4.
 * This will be the latest with kitting
 *
 * Revision 1.4  2011/09/28 11:19:39  sallampati
 * CASE2011263/CR2011260/LOG2011188
 * Kit to order functionality is divided into two proceses. all the logic is incorporated
 *
 * Revision 1.3  2011/07/21 05:30:54  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 *****************************************************************************/

function addAllWorkOrdersToField(workorder, workorderList,form){
	if(workorderList != null && workorderList.length > 0){
		for (var i = 0; i < workorderList.length ; i++) {	
//			var res=  form.setDefaultValue('custpage_workorder').getSelectOptions(workorderList[i].getValue('tranid'), 'is');
//			if (res != null) {
//			if (res.length > 0) {
//			continue;
//			}
//			}
			workorder.addSelectOption(workorderList[i].getValue('internalid'), workorderList[i].getValue('tranid'));

		}
	}

}	
function getAllWorkOrders(){


	var columns = new Array();
	columns[0] = new nlobjSearchColumn('tranid');
	columns[1] = new nlobjSearchColumn('internalid');
	columns[0].setSort(true);

	var searchResults = nlapiSearchRecord('workorder', null, null, columns);	


	return searchResults;
}
function kitToOrderDtlsSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		//create the Suitelet form
		var form = nlapiCreateForm('Kit to Order Details');


		var WorkOrder  = form.addField('custpage_workorder', 'select', 'Work Order').setLayoutType('startrow', 'none');
		WorkOrder.addSelectOption("","");			
		// Retrieve all sales orders
		var WorkOrderList = getAllWorkOrders();		
		// Add all sales orders to SO Field
		WorkOrder.setDefaultValue(request.getParameter('custpage_workorder'));
		addAllWorkOrdersToField(WorkOrder, WorkOrderList);


		var qty;
		var location;
		var woid;
		var item;
		var tranid;
		var itemid;
		var lineItem;
		var LineQty;
		var LineLP;
		var LineLocation;
		var LineLotNo;

		var woid=request.getParameter('custpage_workorder');
		nlapiLogExecution('ERROR','woid',woid);
		if(woid!='')
		{
			nlapiLogExecution('ERROR','INTOwoid1',woid);
			var wosearchresultsitem = nlapiLoadRecord('workorder', woid);
			var lineItems= wosearchresultsitem.getLineItemCount('item');
			if(wosearchresultsitem!=null && wosearchresultsitem!='') 
			{ 	    	
				qty = wosearchresultsitem.getFieldValue('quantity'); 	    	     
				location = wosearchresultsitem.getFieldValue('location'); 
				woid = wosearchresultsitem.getFieldValue('id'); 
				tranid = wosearchresultsitem.getFieldValue('tranid');
				item=wosearchresultsitem.getFieldText('assemblyitem'); 
				itemid=wosearchresultsitem.getFieldValue('assemblyitem');
				nlapiLogExecution('ERROR','createdfrom',wosearchresultsitem.getFieldValue('createdfrom'));
				//nlapiLogExecution('ERROR','createdfrom',wosearchresultsitem.getFieldText('createdfrom'));

				nlapiLogExecution('ERROR','tranid',tranid);

			}


		}
		var soDate = form.addField('custpage_sodate', 'text', 'Date');
		soDate.setLayoutType('normal', 'startrow');
		soDate.setDefaultValue(request.getParameter('custpage_sodate'));

		var item = form.addField('cusppage_item', 'select', 'Kit Item', 'item');
		item.setLayoutType('normal', 'startrow');		 
		if(request.getParameter('custparam_item')!=null && request.getParameter('custparam_item')!="" && request.getParameter('custparam_item')!='select')
		{

			item.setDefaultValue(request.getParameter('custparam_item'));	
		}
		else
		{
			item.setDefaultValue(itemid);
			nlapiLogExecution('ERROR','item.setDefaultValue(itemid);',itemid);
		}

		var kitQty = form.addField('custpage_kitqty', 'text', 'Qty');
		kitQty.setLayoutType('normal', 'startrow');
		if(request.getParameter('custpage_kitqty') != null && request.getParameter('custpage_kitqty') != "")
		{
			kitQty.setDefaultValue(request.getParameter('custpage_kitqty'));
		}

		else
		{
			kitQty.setDefaultValue(qty);	
		}
		varAssemblyQty=request.getParameter('custpage_kitqty');



		var LPNo = form.addField('custpage_lp', 'text', 'LP #');        
		var SerialNo = form.addField("custpage_lotnoserialno", "select", "Serial/Lot#", "customrecord_ebiznet_batch_entry").setDisplayType('entry');

		//var SerialNo = form.addField('custpage_lotnoserialno', 'text', 'Serial/Lot#'); 
		var WOinternalid = form.addField('custpage_wointernalid', 'text', 'Wid').setDisplayType('hidden');;  

		//customrecord_ebiznet_location
		var binloc = form.addField('custpage_binlocation','select','Bin Location','customrecord_ebiznet_location');

		var site = form.addField('cusppage_site', 'select', 'Location', 'location');
		site.setLayoutType('normal', 'startrow');
		nlapiLogExecution('ERROR','Loc',request.getParameter('custparam_locaiton'));
		if(request.getParameter('custparam_locaiton')!='')
		{
			site.setDefaultValue(request.getParameter('custparam_locaiton'));	
		}
		else
		{
			site.setDefaultValue(location);	
		}

		var Sointernalid = form.addField('custparam_soid', 'text', 'Sointernalid').setDisplayType('hidden');
		if(request.getParameter('custparam_soid') != null && request.getParameter('custparam_soid') != "")
			Sointernalid.setDefaultValue(request.getParameter('custparam_soid'));
		var NotesField = form.addField('custpage_notes','text','Notes');
		NotesField.setLayoutType('normal', 'startrow');

		var chknCompany = form.addField('custpage_company', 'select', 'Company', 'customrecord_ebiznet_company');
		chknCompany.setDefaultValue(request.getParameter('custpage_company'));
		chknCompany.setDisplayType('inline');

		var now = new Date();
		var now = new Date();
		var a_p = "";
		var d = new Date();
		var curr_hour = now.getHours();
		if (curr_hour < 12) {
			a_p = "am";
		}
		else {
			a_p = "pm";
		}
		if (curr_hour == 0) {
			curr_hour = 12;
		}
		if (curr_hour > 12) {
			curr_hour = curr_hour - 12;
		}

		var curr_min = now.getMinutes();

		curr_min = curr_min + "";

		if (curr_min.length == 1) {
			curr_min = "0" + curr_min;
		}

		var bgDate = form.addField('custpage_bgdate', 'text', 'BeginDate');      
		bgDate.setDisplayType('hidden');
		bgDate.setDefaultValue((parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());

		var bgTime = form.addField('custpage_bgtime', 'text', 'BeginTime');      
		bgTime.setDisplayType('hidden');
		bgTime.setDefaultValue((curr_hour) + ":" + (curr_min) + " " + a_p);
		nlapiLogExecution('ERROR','beforeitemsublist');
		//Adding  ItemSubList
		var ItemSubList = form.addSubList("custpage_deliveryord_items", "inlineeditor", "ItemList");
		ItemSubList.addField("custpage_deliveryord_sku", "select", "Component","item");
		ItemSubList.addField("custpage_deliveryord_memqty", "text", "Required Qty").setDisplayType('hidden');       
		ItemSubList.addField("custpage_deliveryord_lp", "text", "LP #");        
		ItemSubList.addField("custpage_deliveryord_location", "select", "Bin Location", "customrecord_ebiznet_location");
		ItemSubList.addField("custpage_deliveryord_expqty", "text", "Qty");
		ItemSubList.addField("custpage_skurecid", "text", "Recid").setDisplayType('hidden');
		ItemSubList.addField("custpage_lotno", "text", "Serial/Lot#"); 

		var vitem="",strItem="",strQty=0,vcomponent,vlp,vlocation,vqty;
		var itemvalue=request.getParameter("custparam_item");
		nlapiLogExecution('ERROR','itemvalue',itemvalue);
		if(itemvalue==''|| itemvalue==null || itemvalue=='select')
		{
			itemvalue=itemid;
		}
		nlapiLogExecution('ERROR','itemvalue',itemvalue);
		var ItemType = nlapiLookupField('item', itemvalue, 'recordType');
		nlapiLogExecution('ERROR','ItemType',ItemType);
		var searchresultsitem;
		/*if(ItemType!=null && ItemType !='' && ItemType =='lotnumberedassemblyitem')
    	{
    	 searchresultsitem = nlapiLoadRecord('lotnumberedassemblyitem', request.getParameter("custparam_item")); //1020
    	}
    else if(ItemType!=null && ItemType !='' && ItemType =='kititem')
        	{
        	 searchresultsitem = nlapiLoadRecord('kititem', request.getParameter("custparam_item")); //1020
    	}
    else
    	{
    	searchresultsitem = nlapiLoadRecord('assemblyitem', request.getParameter("custparam_item")); //1020
    	}

		 */

//		The above code is commented by sudheer on 092111 to support for any type of items
		nlapiLogExecution('ERROR','Afteritemsublist');
		if(ItemType!=null && ItemType !='')

		{
			nlapiLogExecution('ERROR','Item Type',ItemType);
			nlapiLogExecution('ERROR','Item Type is NULL ',ItemType);

			searchresultsitem = nlapiLoadRecord(ItemType, itemvalue); //1020
		}

		else
		{
			nlapiLogExecution('ERROR','Item Type is NULL ',ItemType);
		}

		//var searchresultsitem = nlapiLoadRecord('assemblyitem', request.getParameter("custparam_item")); //1020
		//var searchresultsitemTemp = nlapiLoadRecord('assemblyitem', request.getParameter("custparam_item")); //1020
		//nlapiLogExecution('ERROR','searchresultsitemTemp',searchresultsitemTemp);

		nlapiLogExecution('ERROR','searchresultsitem',searchresultsitem);
		//var searchresultsitem = nlapiLoadRecord('assemblyitem', item); //1020  

		var SkuNo=searchresultsitem.getFieldValue('itemid');
		nlapiLogExecution('ERROR','SkuNo',SkuNo);   
		var recCount=0; 
		var filters = new Array(); 
		filters[0] = new nlobjSearchFilter('itemid', null, 'is', SkuNo);//kit_id is the parameter name 
		var columns1 = new Array(); 
		columns1[0] = new nlobjSearchColumn( 'memberitem' ); 
		columns1[1] = new nlobjSearchColumn( 'memberquantity' );

		var j=0,k=0;
		var searchresults = nlapiSearchRecord( 'item', null, filters, columns1 ); 
		for(var i=0; i<searchresults.length;i++) 
		{ 
			strItem = searchresults[i].getValue('memberitem');
			strQty = searchresults[i].getValue('memberquantity');
			nlapiLogExecution('ERROR','strItem',strItem);
			nlapiLogExecution('ERROR','strQty',strQty);
			var fields = ['custitem_item_family', 'custitem_item_group'];
			var columns = nlapiLookupField('inventoryitem', strItem, fields);
			var itemfamily = columns.custitem_item_family;
			var itemgroup = columns.custitem_item_group;
			avlqty= (parseFloat(strQty)*parseFloat(varAssemblyQty));
			var arryinvt=PickStrategieKittoOrder(strItem, itemfamily, itemgroup, avlqty);
			nlapiLogExecution('ERROR','Search End',searchresults[i].getText('memberitem'));
			// var searchresultsLine=getRecord(strItem)
			nlapiLogExecution('ERROR','arryinvt.length',arryinvt.length);
			if(arryinvt.length>0)
			{
				nlapiLogExecution('ERROR', 'Inside If=', i);
				for (j=0; j < arryinvt.length; j++) 
				{			
					var invtarray= arryinvt[j];  	

					vitem =searchresults[i].getValue('memberitem');		      	  		      	  	        
					vlp = invtarray[2];
					vqty = invtarray[0];
					vlocation = invtarray[1];
					var vrecid = invtarray[3];
				}
			}
			if(request.getParameter('custpage_workorder')==''||request.getParameter('custpage_workorder')==null)
			{         
				if(arryinvt.length>0)
				{

					nlapiLogExecution('ERROR', 'Inside If=', i);
					for (j=0; j < arryinvt.length; j++) 
					{			
						var invtarray= arryinvt[j];  	

						vitem =searchresults[i].getValue('memberitem');		      	  		      	  	        
						vlp = invtarray[2];
						vqty = invtarray[0];
						vlocation = invtarray[1];
						var vrecid = invtarray[3];
						vlotno=invtarray[4];
						nlapiLogExecution('ERROR', 'Outside If=', i); 

						form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_sku', k+1, vitem);
						form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_memqty', k + 1, (strQty * varAssemblyQty).toString());
						form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_location', k+1, vlocation);                       		        		   
						form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_lp', k+1, vlp);
						form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_expqty', k + 1, vqty.toString());
						form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_skurecid', k+1, vrecid);
						form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_lotno', k+1, vlotno);
						k=k+1;
						nlapiLogExecution('ERROR', 'loopEnd', i); 

					}            
				}


			} 
			else  

			{	 
				var s=0;
				for (m=1; m <= lineItems; m++) 
				{

					lineItem = wosearchresultsitem.getLineItemText('item', 'item', m);	
					lineItemvalue = wosearchresultsitem.getLineItemValue('item', 'item', m);	
					LineQty =wosearchresultsitem.getLineItemValue('item','quantity',m);
					LineLocation=wosearchresultsitem.getLineItemValue('item','custcol_locationsitemreceipt',m);
					LineLP =wosearchresultsitem.getLineItemValue('item','custcol_ebiz_lp',m);
					LineLotNo =wosearchresultsitem.getLineItemValue('item','serialnumbers',m);

					nlapiLogExecution('ERROR', 'lineItem', LineQty);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_sku',s+1, lineItemvalue);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_memqty',s+1,  LineQty);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_location',s+1,  LineLocation);                       		        		   
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_lp',s+1,  LineLP);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_deliveryord_expqty', s+1, LineQty);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_skurecid', s+1, vrecid);
					form.getSubList('custpage_deliveryord_items').setLineItemValue('custpage_lotno',s+1,  LineLotNo);
					s=s+1;
				}
			}
		}


		nlapiLogExecution('ERROR', 'Submit', 'Submit'); 
		//doField.setDefaultValue('4');

		form.addSubmitButton('Generate Kit to Order');  
		response.writePage(form);
	}
	else //this is the POST block Added by ramana from else part.
	{
		if(request.getParameter('custpage_workorder')!='')
		{
			fnGenerateKitOrder(request, response);
		}
		else
		{
			kitToOrderDetailsPOSTRequest(request, response);
		}







	}
}

function kitToOrderDetailsPOSTRequest(request, response)
{

	var form = nlapiCreateForm('Kit to Order Details');
	var vitem = request.getParameter('cusppage_item');		
	var vloc  = request.getParameter('cusppage_site');
	var vqty  = request.getParameter('custpage_kitqty');
	var vdate = request.getParameter('custpage_sodate');
	var vmemo = request.getParameter('custpage_notes');
	var vlp   = request.getParameter('custpage_lp');
	var vheaderlotno=request.getParameter('custpage_lotnoserialno');
	var vbinloc = request.getParameter('custpage_binlocation');
	var vBeginDate=request.getParameter('custpage_bgdate');
	var vBeginTime=request.getParameter('custpage_bgtime');
	var vSointernalId=request.getParameter('custparam_soid');


	//var vaccountno = request.getParameter('cusppage_accountno');

	//Create Work Order 
	var Wo=nlapiCreateRecord('workorder');
	Wo.setFieldValue('assemblyitem',vitem);
	Wo.setFieldValue('location', vloc);			
	Wo.setFieldValue('quantity', vqty);
	Wo.setFieldValue('trandate', vdate);
	Wo.setFieldValue('createdfrom', vSointernalId);
	nlapiLogExecution('ERROR','createdfrom vSointernalId',vSointernalId);
	var lineCnt = request.getLineItemCount('custpage_deliveryord_items');

	for (var k = 1; k <= lineCnt; k++) 
	{				
		var vitem = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_sku', k);
		var vLocation = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_location', k);
		var vlp =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_lp', k);
		var vQty =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_memqty', k);
		var vLotNo =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_lotno', k);

		nlapiLogExecution('ERROR', 'vLocation',vLocation);
		nlapiLogExecution('ERROR', 'vlp',vlp);	
		nlapiLogExecution('ERROR', 'vQty',vQty);	
		Wo.setLineItemValue('item', 'item', k,vitem);
		Wo.setLineItemValue('item', 'quantity', k,vQty) ;
		Wo.setLineItemValue('item', 'serialnumbers', k,vLotNo + "(" + vQty + ")") ;

		Wo.setLineItemValue('item', 'custcol_locationsitemreceipt', k,vLocation);
		Wo.setLineItemValue('item', 'custcol_ebiz_lp', k,vlp) ;

		Wo.selectLineItem('item', k);
		//Wo.commitLineItem('item');

	}

	var woid=nlapiSubmitRecord(Wo);
	nlapiLogExecution('ERROR', 'Outside If=', woid); 

	var filters = new Array();			   

	filters[0] = new nlobjSearchFilter('internalid', null, 'is', woid);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('tranid');			


	var searchResults = nlapiSearchRecord('workorder', null, filters, columns);
	var wovalue;
	if(searchResults!= null && searchResults != "" && searchResults.length >0)
	{
		wovalue=searchResults[0].getValue('tranid');
	}

	showInlineMessage(form, 'Confirmation', 'Work Order Created', wovalue);	


	response.writePage(form);

}


function fnGenerateKitOrder(request, response)
{
	//Reducing the Inventory Qty of Line level sku details from custom defined inventory table
	var form = nlapiCreateForm('Kit to Order Details');
	var woid = request.getParameter('custpage_workorder');
	var vitem = request.getParameter('cusppage_item');	
	nlapiLogExecution('ERROR', 'vitem', vitem);
	var vloc  = request.getParameter('cusppage_site');
	var vqty  = request.getParameter('custpage_kitqty');
	var vdate = request.getParameter('custpage_sodate');
	var vmemo = request.getParameter('custpage_notes');
	var vlp   = request.getParameter('custpage_lp');	
	//var vheaderlotno=request.getParameter('custpage_lotnoserialno');
	var Lotnovalue=request.getParameter('custpage_lotnoserialno');

	var vbinloc = request.getParameter('custpage_binlocation');
	var vBeginDate=request.getParameter('custpage_bgdate');
	var vBeginTime=request.getParameter('custpage_bgtime');
	var vSointernalId=request.getParameter('custparam_soid');
	nlapiLogExecution('ERROR', 'vSointernalId', vSointernalId);
	var wointernalid=request.getParameter('custpage_workorder');

	var filters = new Array();	

	if(Lotnovalue != null && Lotnovalue != "")
	{
		var searchresults = nlapiLoadRecord('customrecord_ebiznet_batch_entry', Lotnovalue);


		var vheaderlotno;
		if(searchresults!= null && searchresults!= "")
		{
			vheaderlotno=searchresults.getFieldValue('name');
			nlapiLogExecution('ERROR', 'vheaderlotno', vheaderlotno);
		}
		//Lotnovalue=vheaderlotno;
	}
	var lineCnt = request.getLineItemCount('custpage_deliveryord_items');
	var varqty ;
	for (var k = 1; k <= lineCnt; k++) 
	{
		nlapiLogExecution('ERROR', 'intoloop', lineCnt);
		var varsku = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_sku', k);	
		varqty =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_memqty', k);	
		var varRecid = request.getLineItemValue('custpage_deliveryord_items', 'custpage_skurecid', k);	
		var varLotNo = request.getLineItemValue('custpage_deliveryord_items', 'custpage_lotno', k);	
		var varLotQty = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_memqty', k);	
		nlapiLogExecution('ERROR', 'inventory intern id', varRecid);
		var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', varRecid);
		var qty = transaction.getFieldValue('custrecord_ebiz_qoh');
		var allocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
		if(allocqty==null || allocqty=="")
		{
			allocqty=0;
		}

		transaction.setFieldValue('custrecord_ebiz_qoh',(parseFloat(qty)-parseFloat(varqty)).toFixed(5));
		if(parseFloat(allocqty)>parseFloat(varqty))
		{
			transaction.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(allocqty)-parseFloat(varqty)).toFixed(5));	
		}
		else if(parseFloat(allocqty)>0)
		{
			transaction.setFieldValue('custrecord_ebiz_alloc_qty',0);
		}
		else
		{
			nlapiLogExecution('ERROR', 'intoloop', 'AllocatedQty is Zero');
		}
		nlapiSubmitRecord(transaction,false,true);
	}
	//upto to here for custom defined invenotry table.

	//Adding or updating the kitted SKU to custom defined inventory table.
	//var searchresultsLine=getRecord(vitem)

	var arrdims = fnGetSkucubeandweight(vitem, 1);

	var ItemCube = 0;
	if (arrdims.length > 0) {
		if (!isNaN(arrdims[0])) {
			ItemCube = (parseFloat(arrdims[0]) * parseFloat(vqty));
			nlapiLogExecution('ERROR', 'ItemCube', ItemCube);
		}
		else {
			ItemCube = 0;
		}
	}
	else {
		ItemCube = 0;
	}
	var ItemType = nlapiLookupField('item', vitem, 'recordType');
	nlapiLogExecution('ERROR','ItemType 1',ItemType);

	var Loctdims = GetPutawayLocation(vitem, null, null, null, ItemCube,ItemType);
	var LocName, LocId, LocRemCube, InbLocId, OubLocId, PickSeq, PutSeq, RemCube;
	if (Loctdims.length > 0) {
		if (Loctdims[0] != "") {
			LocName = Loctdims[0];
			nlapiLogExecution('ERROR', 'LocName', LocName);
		}
		else {
			LocName = "";
			nlapiLogExecution('ERROR', 'in else LocName', LocName);
		}
		if (!isNaN(Loctdims[1])) {
			RemCube = Loctdims[1];
		}
		else {
			RemCube = 0;
		}
		if (!isNaN(Loctdims[2])) {
			LocId = Loctdims[2];
			nlapiLogExecution('ERROR', 'LocId', LocId);
		}
		else {
			LocId = "";
			nlapiLogExecution('ERROR', 'in else LocId', LocId);
		}
		if (!isNaN(Loctdims[3])) {
			OubLocId = Loctdims[3];
			nlapiLogExecution('ERROR', 'OubLocId ', OubLocId );
		}
		else {
			OubLocId = "";
		}
		if (!isNaN(Loctdims[4])) {
			PickSeq = Loctdims[4];
			nlapiLogExecution('ERROR', 'PickSeq ', PickSeq );
		}
		else {
			PickSeq = "";
		}
		if (!isNaN(Loctdims[5])) {
			InbLocId = Loctdims[5];
			nlapiLogExecution('ERROR', 'InbLocId ', InbLocId );
		}
		else {
			InbLocId = "";
		}
		if (!isNaN(Loctdims[6])) {
			PutSeq = Loctdims[6];
			nlapiLogExecution('ERROR', 'PutSeq ', PutSeq );
		}
		else {
			PutSeq = "";
		}
		if (!isNaN(Loctdims[7])) {
			LocRemCube = Loctdims[7];
		}
		else {
			LocRemCube = "";
		}
	}

	//getting default skustaus and pack code

	var ItemDimensionsFilters = new Array();
	ItemDimensionsFilters.push(new nlobjSearchFilter('internalid', 'custrecord_ebizitemdims', 'is', vitem));

	var ItemDimensionsColumns = new Array();

	ItemDimensionsColumns.push(new nlobjSearchColumn('custrecord_ebizitemdims'));
	ItemDimensionsColumns.push(new nlobjSearchColumn('custrecord_ebizpackcodeskudim'));

	var ItemDimensionsResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, ItemDimensionsFilters, ItemDimensionsColumns);

	if (ItemDimensionsResults != null) 
	{
		var ItemDimensionsResult = ItemDimensionsResults[0];
		var getFetchedPackCodeId = ItemDimensionsResult.getId();
		var getFetchedPackCode = ItemDimensionsResult.getValue('custrecord_ebizpackcodeskudim');
	}

	nlapiLogExecution('ERROR', 'Fetched Pack Code', getFetchedPackCode);

	// Fetch the item status for the item entered
	var ItemStatusFilters = new Array();
	ItemStatusFilters.push(new nlobjSearchFilter('custrecord_defaultskustaus', null, 'is', 'T'));

	var ItemStatusColumns = new Array();

	ItemStatusColumns.push(new nlobjSearchColumn('internalid'));
	ItemStatusColumns.push(new nlobjSearchColumn('name'));
	ItemStatusColumns.push(new nlobjSearchColumn('custrecord_ebizsiteskus'));

	var ItemStatusResults = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, ItemStatusFilters, ItemStatusColumns);

	if (ItemStatusResults != null) 
	{
		var ItemStatusResult = ItemStatusResults[0];
		var getFetchedItemStatusId = ItemStatusResult.getId();
		var getFetchedItemStatus = ItemStatusResult.getValue('name');
		var getFetchedSiteLocation = ItemStatusResult.getValue('custrecord_ebizsiteskus');
	}

	nlapiLogExecution('ERROR', 'Fetched Item Status', getFetchedItemStatus);
	//end region
	nlapiLogExecution('ERROR', 'before getting lp', 'done');
	var noofLpsRequired=1;
	var MultipleLP = GetMultipleLP(1,1,noofLpsRequired);
	nlapiLogExecution('ERROR', 'MultipleLP', MultipleLP);
	
	var maxLPPrefix = MultipleLP.split(",")[0];
	var maxLP = MultipleLP.split(",")[1];
	maxLP = parseFloat(maxLP) - parseFloat(noofLpsRequired); 

	nlapiLogExecution('ERROR', 'MAX LP:', maxLP);
	// Calculate LP
	maxLP = parseFloat(maxLP) + 1;
	var LP = maxLPPrefix + maxLP.toString();
	nlapiLogExecution('ERROR', 'LP:', LP);

	var invrecord = nlapiCreateRecord('customrecord_ebiznet_createinv');
	invrecord.setFieldValue('name', vitem+1);					
	invrecord.setFieldValue('custrecord_ebiz_inv_sku', vitem); 	
	//invrecord.setFieldValue('custrecord_ebiz_inv_sku', varsku); 	
	//invrecord.setFieldValue('custrecord_ebiz_inv_qty', vqty);
	//invrecord.setFieldValue('custrecord_ebiz_inv_account_no',vaccountno);
	invrecord.setFieldValue('custrecord_ebiz_inv_note1',vmemo);
	invrecord.setFieldValue('custrecord_ebiz_inv_loc', vloc);
	if(vbinloc!=null && vbinloc!="")
	{
		nlapiLogExecution('ERROR','vbinloc',vbinloc);
		invrecord.setFieldValue('custrecord_ebiz_inv_binloc', vbinloc);
	}
	else
	{
		invrecord.setFieldValue('custrecord_ebiz_inv_binloc', LocId);
	}
	invrecord.setFieldValue('custrecord_ebiz_inv_binloc', LocId);
	invrecord.setFieldValue('custrecord_ebiz_qoh', parseFloat(vqty).toFixed(5));
	invrecord.setFieldValue('custrecord_ebiz_callinv', 'N');		
	invrecord.setFieldValue('custrecord_ebiz_inv_lp', vlp);	
	if(Lotnovalue!=null && Lotnovalue != "") 
		invrecord.setFieldValue('custrecord_ebiz_inv_lot', Lotnovalue);	
	invrecord.setFieldValue('custrecord_ebiz_inv_qty',parseFloat(vqty).toFixed(5));
	invrecord.setFieldValue('custrecord_outboundinvlocgroupid',OubLocId);
	invrecord.setFieldValue('custrecord_pickseqno',PickSeq);
	invrecord.setFieldValue('custrecord_inboundinvlocgroupid',InbLocId);
	invrecord.setFieldValue('custrecord_putseqno',PutSeq);	
	invrecord.setFieldValue('custrecord_ebiz_callinv', 'N');
	invrecord.setFieldValue('custrecord_ebiz_callinv', 'N');


	//newly added fields
	invrecord.setFieldValue('custrecord_ebiz_inv_sku_status',getFetchedItemStatus);	
	invrecord.setFieldValue('custrecord_ebiz_inv_packcode',getFetchedPackCode);	
	invrecord.setFieldValue('custrecord_ebiz_inv_lp',LP);	

	//invrecord.setFieldValue('custrecord_ebiz_inv_account_no', '120');					
	nlapiSubmitRecord(invrecord,false,true );			 

	var now = new Date();
	var now = new Date();
	var a_p = "";
	var d = new Date();
	var curr_hour = now.getHours();
	if (curr_hour < 12) {
		a_p = "am";
	}
	else {
		a_p = "pm";
	}
	if (curr_hour == 0) {
		curr_hour = 12;
	}
	if (curr_hour > 12) {
		curr_hour = curr_hour - 12;
	}

	var curr_min = now.getMinutes();

	curr_min = curr_min + "";

	if (curr_min.length == 1) {
		curr_min = "0" + curr_min;
	}

	for (var x = 1; x <= lineCnt; x++) 
	{	
		var varsku = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_sku', x);
		var varqty = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_memqty', x);
		var vactbeginloc = request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_location', x);


		var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		customrecord.setFieldValue('custrecord_tasktype', 3); // 5 for KTS,3 for PICK, 4 for SHIP
		customrecord.setFieldValue('custrecordact_begin_date', vBeginDate);
		customrecord.setFieldValue('custrecord_actualbegintime', vBeginTime);
		customrecord.setFieldValue('custrecord_act_end_date', DateStamp());
		customrecord.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
		customrecord.setFieldValue('custrecord_expe_qty', parseFloat(varqty).toFixed(5)); 
		customrecord.setFieldValue('custrecord_act_qty', parseFloat(varqty).toFixed(5));
		customrecord.setFieldValue('custrecord_wms_status_flag', 14);	// 14 stands for outbound process
		customrecord.setFieldValue('custrecord_actbeginloc', vactbeginloc);	
		customrecord.setFieldValue('custrecord_actendloc', vbinloc);						
		customrecord.setFieldValue('custrecord_upd_date', now);
		customrecord.setFieldValue('custrecord_ebiz_sku_no', varsku);
		customrecord.setFieldValue('name', vitem+1);		
		var currentContext = nlapiGetContext();  
		var currentUserID = currentContext.getUser();
		customrecord.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		customrecord.setFieldValue('custrecord_ebizuser', currentUserID);			
		nlapiSubmitRecord(customrecord);

		var customrecord1 = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		customrecord1.setFieldValue('custrecord_tasktype', 4); // 5 for KTS,3 for PICK, 4 for SHIP
		customrecord1.setFieldValue('custrecordact_begin_date', vBeginDate);
		customrecord1.setFieldValue('custrecord_actualbegintime', vBeginTime);
		customrecord1.setFieldValue('custrecord_act_end_date', DateStamp());
		customrecord1.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
		customrecord1.setFieldValue('custrecord_expe_qty', parseFloat(varqty).toFixed(5)); 
		customrecord1.setFieldValue('custrecord_act_qty', parseFloat(varqty).toFixed(5)); 
		customrecord1.setFieldValue('custrecord_actbeginloc', vactbeginloc);
		customrecord1.setFieldValue('custrecord_actendloc', vbinloc);
		customrecord1.setFieldValue('custrecord_wms_status_flag', 14);	 // 14 stands for outbound process
		customrecord1.setFieldValue('custrecord_upd_date', now);
		customrecord1.setFieldValue('custrecord_ebiz_sku_no', varsku);
		customrecord1.setFieldValue('name', vitem+1);	
		var currentContext = nlapiGetContext();  
		var currentUserID = currentContext.getUser();
		customrecord1.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
		customrecord1.setFieldValue('custrecord_ebizuser', currentUserID);				
		nlapiSubmitRecord(customrecord1);
		var customtranrecord = nlapiCreateRecord('customrecord_ebiznet_order_line_details');					
		customtranrecord.setFieldValue('custrecord_orderlinedetails_pickgen_qty', parseFloat(varqty).toFixed(5)); 
		customtranrecord.setFieldValue('custrecord_orderlinedetails_pickconf_qty', parseFloat(varqty).toFixed(5)); 
		customtranrecord.setFieldValue('custrecord_orderlinedetails_ship_qty', parseFloat(varqty).toFixed(5)); 
		customtranrecord.setFieldValue('custrecord_orderlinedetails_status_flag', 14);  // 14 stands for outbound process
		customtranrecord.setFieldValue('custrecord_orderlinedetails_item', varsku);
		customtranrecord.setFieldValue('custrecord_orderlinedetails_ord_category',4); // 3 stands for workorder				
		customtranrecord.setFieldValue('custrecord_orderlinedetails_order_no','WO'+varsku+x); 
		customtranrecord.setFieldValue('name','WO'+varsku+x); 
		nlapiSubmitRecord(customtranrecord);
	}

	var customrecord2 = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
	customrecord2.setFieldValue('custrecord_tasktype', 6); // 5 for KTS
	customrecord2.setFieldValue('custrecordact_begin_date', vBeginDate);
	customrecord2.setFieldValue('custrecord_actualbegintime', vBeginTime);
	customrecord2.setFieldValue('custrecord_act_end_date', DateStamp());
	customrecord2.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
	customrecord2.setFieldValue('custrecord_expe_qty', parseFloat(vqty).toFixed(5)); 
	customrecord2.setFieldValue('custrecord_act_qty', parseFloat(vqty).toFixed(5)); 
	if(vheaderlotno!= null && vheaderlotno != "")
		customrecord2.setFieldValue('custrecord_batch_no', vheaderlotno); 

	if(vbinloc!=null && vbinloc!="")
	{
		customrecord2.setFieldValue('custrecord_actbeginloc', vbinloc);
		customrecord2.setFieldValue('custrecord_actendloc', vbinloc);
	}
	else
	{
		customrecord2.setFieldValue('custrecord_actbeginloc', LocId);
		customrecord2.setFieldValue('custrecord_actendloc', LocId);					
	}

	customrecord2.setFieldValue('custrecord_wms_status_flag', 3);					
	customrecord2.setFieldValue('custrecord_upd_date', now);
	customrecord2.setFieldValue('custrecord_ebiz_sku_no', vitem);
	customrecord2.setFieldValue('name', vitem+1);		
	customrecord2.setFieldValue('custrecord_lpno', vlp);		
	var currentContext = nlapiGetContext();  
	var currentUserID = currentContext.getUser();
	customrecord2.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
	customrecord2.setFieldValue('custrecord_ebizuser', currentUserID);			
	nlapiSubmitRecord(customrecord2);

	var customkittranrecord = nlapiCreateRecord('customrecord_ebiznet_order_line_details');					
	customkittranrecord.setFieldValue('custrecord_orderlinedetails_checkin_qty', parseFloat(vqty).toFixed(5)); 
	customkittranrecord.setFieldValue('custrecord_orderlinedetails_putgen_qty', parseFloat(vqty).toFixed(5)); 
	customkittranrecord.setFieldValue('custrecord_orderlinedetails_putconf_qty', parseFloat(vqty).toFixed(5)); 
	customkittranrecord.setFieldValue('custrecord_orderlinedetails_status_flag', 3); //3 stands for s inbound process 
	customkittranrecord.setFieldValue('custrecord_orderlinedetails_item', vitem);
	customkittranrecord.setFieldValue('custrecord_orderlinedetails_ord_category',4); // 3 stands for workorder	
	customkittranrecord.setFieldValue('custrecord_orderlinedetails_order_no','WO'+varsku);
	customkittranrecord.setFieldValue('name','WO'+varsku); 
	nlapiSubmitRecord(customkittranrecord);

	nlapiLogExecution('ERROR','vLotNo',varLotNo);
	nlapiLogExecution('ERROR','varLotQty',varLotQty);


	//var assmid=transformAssemblyItem(woid,vqty,vloc,varLotNo);
	var assmid= transformAssemblyItem(wointernalid,vqty,vloc,vheaderlotno);

	nlapiLogExecution('ERROR','assmid',assmid);

	/*
					var customrecord3 = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
					customrecord3.setFieldValue('custrecord_tasktype', 1); // 5 for KTS, 1 for CHKN, 2 for PUTW
					//customrecord3.setFieldValue('custrecordact_begin_date', now);
					customrecord3.setFieldValue('custrecordact_begin_date', (parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
					customrecord3.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
					customrecord3.setFieldValue('custrecord_expe_qty', vqty); 
					//customrecord3.setFieldValue('custrecord_lpno', vlp); 
					customrecord3.setFieldValue('custrecord_upd_date', now);
					//customrecord3.setFieldValue('custrecord_sku', vskuText);
					customrecord3.setFieldValue('custrecord_ebiz_sku_no', vitem);
					customrecord3.setFieldValue('name', vitem+1);				
					customrecord3.setFieldValue('custrecord_lpno', vlp);
					nlapiSubmitRecord(customrecord3);

					var customrecord4 = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
					customrecord4.setFieldValue('custrecord_tasktype', 2); // 5 for KTS, 1 for CHKN, 2 for PUTW
					//customrecord4.setFieldValue('custrecordact_begin_date', now);
					customrecord4.setFieldValue('custrecordact_begin_date', (parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
					customrecord4.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
					customrecord4.setFieldValue('custrecord_expe_qty', vqty); 
					//customrecord4.setFieldValue('custrecord_lpno', vlp); 
					customrecord4.setFieldValue('custrecord_upd_date', now);
					//customrecord4.setFieldValue('custrecord_sku', vskuText);
					customrecord4.setFieldValue('custrecord_ebiz_sku_no', vitem);
					customrecord4.setFieldValue('name', vitem+1);					
					customrecord4.setFieldValue('custrecord_lpno', vlp);
					nlapiSubmitRecord(customrecord4);

	 */


	//upto to here for creation record in open task.


	//response.write("varsku::"+varsku);
	//response.write("\n");	
	//response.write("varqty::"+varqty);
	//response.write("\n");
	//response.write("varRecid::"+varRecid);
	//response.write("\n");
	// response.write("Kit to Order done Successfully!!!");
	//response.sendRedirect('TASKLINK','LIST_TRAN_BUILD',null,null,null);
	nlapiSetRedirectURL('RECORD', 'salesorder', vSointernalId, false);






}


function getRecord(itemid)
{
	var filtersso = new Array();
	var socount=0;		

	filtersso[0] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', itemid);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');

	var searchresultsLine = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersso, columns);
	if(searchresultsLine !=null)
	{
		nlapiLogExecution('ERROR', 'SO count', searchresultsLine.length);
		socount=searchresultsLine.length;  

	}        


	return searchresultsLine;

}

function PickStrategieKittoOrder(item,itemfamily,itemgroup,avlqty){
	var actallocqty = 0;
	var Resultarray = new Array();
	nlapiLogExecution('ERROR','item',item);
	nlapiLogExecution('ERROR','itemgroup',itemgroup);
	nlapiLogExecution('ERROR','itemfamily',itemfamily);

	var filters = new Array();	
	filters[0] = new nlobjSearchFilter('custrecord_ebizskupickrul', null, 'anyof', ['@NONE@',item]);
	var k=1;
	if (itemgroup != "" && itemgroup != null) {
		filters[k] = new nlobjSearchFilter('custrecord_ebizskugrouppickrul',null,  'anyof', ['@NONE@',itemgroup]);
		k=k+1;
	}
	if (itemfamily!= "" && itemfamily!= null) {
		filters[k] = new nlobjSearchFilter('custrecord_ebizskufamilypickrul', null, 'anyof', ['@NONE@',itemfamily]);
	}

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_ebizpickzonerul');
	columns[2] = new nlobjSearchColumn('custrecord_ebizpickmethod');
	columns[3] = new nlobjSearchColumn('custrecord_ebizsequencenopickrul');

	//Fetching pick rule
	nlapiLogExecution('ERROR','Pick rule Fectching');
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_pick_rule', null, filters, columns);
	for (var i = 0; searchresults != null && i < searchresults.length; i++) {

		var searchresultpick = searchresults[i];				
		var vpickzone=searchresultpick.getValue('custrecord_ebizpickzonerul');	
		nlapiLogExecution('ERROR','PickZone',vpickzone)	;		
		var filterszone = new Array();	
		filterszone[0] = new nlobjSearchFilter('custrecordcustrecord_putzoneid', null, 'is', vpickzone,null);

		var columnzone = new Array();
		columnzone[0] = new nlobjSearchColumn('custrecord_locgroup_no');

		var searchresultszone = nlapiSearchRecord('customrecord_zone_locgroup', null, filterszone, columnzone);
		nlapiLogExecution('ERROR','Loc Group Fetching');
		//fetching loc group
		for (var j = 0; searchresultszone != null && j < searchresultszone.length; j++) {

			var searchresultzone = searchresultszone[j];
			var vlocgroupno = searchresultzone.getValue('custrecord_locgroup_no');

			nlapiLogExecution('ERROR', 'LocGroup'+ searchresultzone.getValue('custrecord_locgroup_no'), searchresultzone.getText('custrecord_locgroup_no'));					
			var filtersinvt = new Array();

			filtersinvt[0] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', item);
			//filtersinvt[1] = new nlobjSearchFilter('custrecord_outboundinvlocgroupid', null, 'is', vlocgroupno);
			nlapiLogExecution('ERROR', "custrecord_ebiz_inv_sku: " + item);
			var columnsinvt = new Array();
			columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
			columnsinvt[1] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
			columnsinvt[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
			columnsinvt[3] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
			columnsinvt[4] = new nlobjSearchColumn('custrecord_ebiz_qoh');
			columnsinvt[5] = new nlobjSearchColumn('custrecord_pickseqno');
			columnsinvt[6] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');
			columnsinvt[5].setSort();

			var searchresultsinvt = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);
			for (var l = 0; searchresultsinvt != null && l < searchresultsinvt.length; l++) {

				var searchresult = searchresultsinvt[l];
				var actqty = searchresult.getValue('custrecord_ebiz_qoh');
				var allocqty = searchresult.getValue('custrecord_ebiz_alloc_qty');
				var LP = searchresult.getValue('custrecord_ebiz_inv_lp');
				var vactLocation = searchresult.getValue('custrecord_ebiz_inv_binloc');
				var vactLocationtext = searchresult.getText('custrecord_ebiz_inv_binloc');
				var vlotno = searchresult.getText('custrecord_ebiz_inv_lot');
				var Recid = searchresult.getId(); 
				nlapiLogExecution('ERROR', 'Recid1', Recid);
				if (isNaN(allocqty)) {
					allocqty = 0;
				}
				if (allocqty == "") {
					allocqty = 0;
				}
				if( parseFloat(actqty)<0)
				{
					actqty=0;
				}
				nlapiLogExecution('ERROR', "allocqty: " + allocqty);
				nlapiLogExecution('ERROR', "LP: " + LP);

				var remainqty = actqty - allocqty;
				nlapiLogExecution('ERROR', 'remainqty' + remainqty);

				var cnfmqty = 0;
				nlapiLogExecution('ERROR', 'LOOP BEGIN');

				if (remainqty > 0) {
					nlapiLogExecution('ERROR', 'INTO LOOP');
					if ((avlqty - actallocqty) <= remainqty) {
						cnfmqty = avlqty - actallocqty;
						actallocqty = avlqty;
					}
					else {
						cnfmqty = remainqty;
						actallocqty = actallocqty + remainqty;
					}
					nlapiLogExecution('ERROR','cnfmqty', cnfmqty);
					nlapiLogExecution('ERROR','LotNo', vlotno);
					if (cnfmqty > 0) {
						//Resultarray
						var invtarray = new Array();
						invtarray[0]= cnfmqty;							
						invtarray[1]= vactLocation;
						invtarray[2] = LP;
						invtarray[3] = Recid;
						invtarray[4] = vlotno;
						nlapiLogExecution('ERROR', 'RecId:',Recid);
						//invtarray[3] = vpackcode;
						//invtarray[4] = vskustatus;
						//invtarray[5] = vuomid;
						Resultarray.push(invtarray);								
					}


				}

				if ((avlqty - actallocqty) == 0) {
					nlapiLogExecution('ERROR', 'Into Break:');
					return Resultarray;

				}

			}


		}

	}
	return Resultarray;  
}

function transformAssemblyItem(assemblyrecid,vqty,vloc,varLotNo){

	try
	{
		var fromRecord = 'workorder'; 

		var toRecord = 'assemblybuild'; 
		var record = nlapiTransformRecord(fromRecord, assemblyrecid, toRecord);

		record.setFieldValue('quantity', vqty);
		record.setFieldValue('location', vloc);	
		nlapiLogExecution('ERROR', 'Location for main component',vloc);
		nlapiLogExecution('ERROR', 'Quantity for main component',vqty);

		record.setFieldValue('serialnumbers',  varLotNo + "(" + vqty + ")") ;


		nlapiLogExecution('ERROR', 'varLotNo1', varLotNo + "(" + vqty + ")");

		var lineCnt = request.getLineItemCount('custpage_deliveryord_items');
		nlapiLogExecution('ERROR', 'lineCnt',lineCnt);

		for (var k = 1; k <= lineCnt; k++) 
		{				

			var LotNo = request.getLineItemValue('custpage_deliveryord_items', 'custpage_lotno', k);
			var varqty =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_memqty', k);
			var varsku =  request.getLineItemValue('custpage_deliveryord_items', 'custpage_deliveryord_sku', k);
			nlapiLogExecution('ERROR', 'LotNo',LotNo);
			nlapiLogExecution('ERROR', 'k',k);
			nlapiLogExecution('ERROR', 'Lot number sending for component',LotNo + "(" + varqty + ")");
			record.setLineItemValue('component', 'item', k,varsku) ;
			record.setLineItemValue('component', 'componentnumbers', k,LotNo + "(" + varqty + ")") ;

		}


		var id = nlapiSubmitRecord(record, false);
		return id;

	} catch (e) {
		if (e instanceof nlobjError)
			lapiLogExecution('ERROR', 'system error', e.getCode() + '\n'
					+ e.getDetails());
		else
			nlapiLogExecution('ERROR', 'unexpected error', e.toString());
	}
} 		 



