/***************************************************************************
 eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_AssignPackStation_SL.js,v $
 *     	   $Revision: 1.7.2.2.4.1.4.3.2.1 $
 *     	   $Date: 2015/09/16 15:55:16 $
 *     	   $Author: deepshikha $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_6 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_AssignPackStation_SL.js,v $
 * Revision 1.7.2.2.4.1.4.3.2.1  2015/09/16 15:55:16  deepshikha
 * 2015.2 issueFix
 * 201414321
 *
 * Revision 1.7.2.2.4.1.4.3  2015/08/24 14:36:43  schepuri
 * case# 201413986
 *
 * Revision 1.7.2.2.4.1.4.2  2013/03/26 13:27:42  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.7.2.2.4.1.4.1  2013/03/01 15:01:12  rmukkera
 * code is merged from FactoryMation production as part of Standard bundle
 *
 * Revision 1.7.2.2.4.1  2013/01/24 06:31:30  spendyala
 * CASE201112/CR201113/LOG201121
 * Fetching more than 1000 records in populating orders in drop down.
 *
 * Revision 1.7.2.2  2012/04/20 09:35:32  vrgurujala
 * CASE201112/CR201113/LOG201121
 * t_NSWMS_LOG201121_120
 *
 * Revision 1.7.2.1  2012/04/12 21:44:55  spendyala
 * CASE201112/CR201113/LOG201121
 * Showing meaningful data instead of showing incomplete data.
 *
 * Revision 1.7  2011/12/22 18:42:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Shipping
 *
 * Revision 1.6  2011/09/12 07:04:55  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.5  2011/08/27 06:48:26  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Ship Manifest related fixes.
 *
 * Revision 1.4  2011/08/25 16:06:35  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Ship Manifest related fixes.
 *
 * Revision 1.3  2011/08/24 15:24:27  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Pack Location is added to the sublist and PACK task creation.
 *
 * Revision 1.2  2011/08/23 13:30:27  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Comments and Logs added
 *
 * Revision 1.1  2011/08/23 12:54:38  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Assign Pack Station - New Screen developed
 * 
 *****************************************************************************/

function ebiz_AssignPackStation_SL(request, response)
{
	if (request.getMethod() == 'GET'){
		// Call method to render the assign pack station criteria
		showAssignPackStationCriteria(request,response);
	} 
	else{
		if (request.getParameter('custpage_orderno') != null){
			nlapiLogExecution('ERROR', 'Displaying fulfillment order search results');

			var form = nlapiCreateForm('Assign Pack Station');

			// Call method to display the selected fulfillment order candidates
			displayPossibleFulfillmentOrders(request, response, form);
		}
		else{
			nlapiLogExecution('ERROR', 'Perform Packing for Selected Containers');

			var form = nlapiCreateForm('Assign Pack Station');
			processSelectedContainers(request, response, form);
		}
		response.writePage(form);	
	}
}


/**
 * This method will create a ship manifest record
 * @param vOrdNo
 * @param containerlp
 */
function CreateShipManifestRecord(vOrdNo,containerlp,compid,siteid)
{
	try {
		nlapiLogExecution('ERROR','into CreateShipManifestRecord');
		nlapiLogExecution('ERROR','vOrdNo',vOrdNo);
		nlapiLogExecution('ERROR','containerlp',containerlp);
		var vMainOrdNo="";
		var salesOrderNo="";		

		if (vOrdNo != null && vOrdNo != "") {

			var searchresults = getParentOrder(vOrdNo);
			if(searchresults){
				vMainOrdNo = searchresults[0].getValue('custrecord_ns_ord');
				salesOrderNo = searchresults[0].getText('custrecord_ns_ord');
				nlapiLogExecution('ERROR', 'Parent Order # ',salesOrderNo);
				nlapiLogExecution('ERROR', 'Internal Order # ',vMainOrdNo);
			}

			if (vMainOrdNo != null && vMainOrdNo != "") {
				CreateShippingManifestRecord(vMainOrdNo,containerlp);
			}
		}

	}
	catch (e) {
		if (e instanceof nlobjError) 
			lapiLogExecution('ERROR', 'system error', e.getCode() + '\n' + e.getDetails());
		else 
			nlapiLogExecution('ERROR', 'unexpected error', e.toString());
	}
}


/**
 * Function to render the search criteria to display all the possible containers
 * that can be packed based on the provided search criteria
 * 
 * @param request
 * @param response
 */
function showAssignPackStationCriteria(request, response){
	var form = nlapiCreateForm('Assign Pack Station');

	// Fullfillment order number in search criteria
	var selectSO = form.addField('custpage_orderno', 'select', 'Order #');
	selectSO.addSelectOption("","");

	// Retrieve all sales orders
	var salesOrderList = getAllSalesOrders(-1);

	// Add all sales orders to SO Field
	addAllSalesOrdersToField(form,selectSO, salesOrderList);

	var button = form.addSubmitButton('Display');
	response.writePage(form);
}

/**
 * Function to retrieve sales orders
 * @returns
 */
var GlobalArray;
function getAllSalesOrders(max){
	var filtersSO = new Array();		
	filtersSO.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//Status Flag - C (Picks Confirmed)		
	filtersSO.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK
	nlapiLogExecution('ERROR','max',max);
	if(max!=-1&&max!=null&&max!="")
		filtersSO.push(new nlobjSearchFilter('id', null, 'greaterthan', max));
	else
		GlobalArray = new Array();

	var colsSO = new Array();
	colsSO[0] = new nlobjSearchColumn('name');
	colsSO[1] = new nlobjSearchColumn('id').setSort();

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersSO, colsSO);
	if( searchresults!=null && searchresults.length>=1000)
	{ 
		for(var s=0;s<searchresults.length;s++)
		{	
			GlobalArray.push(searchresults[s]);
		}
		var maxno=searchresults[searchresults.length-1].getValue('id');
		getAllSalesOrders(maxno);	
	}
	else
	{
		for(var s=0;searchresults!=null && s<searchresults.length;s++)
		{	
			GlobalArray.push(searchresults[s]);
		} 
	}
	nlapiLogExecution('ERROR', 'out getdummylpdetails', GlobalArray);
	return GlobalArray;
}

/**
 * Function to add all sales orders in the list passed to the passed field
 * 
 * @param selectSO
 * @param salesOrderList
 */
function addAllSalesOrdersToField(form,selectSO, salesOrderList){
	if(salesOrderList != null && salesOrderList.length > 0){
		for (var i = 0; i < salesOrderList.length; i++) 
		{
			var res=  form.getField('custpage_orderno').getSelectOptions(salesOrderList[i].getValue('name'), 'is');
			if (res != null) {
				if (res.length > 0) {
					continue;
				}
			}
			selectSO.addSelectOption(salesOrderList[i].getValue('name'), salesOrderList[i].getValue('name'));
		}
	}
}

/**
 * 
 * @param request
 * @param response
 * @param form
 */
function displayPossibleFulfillmentOrders(request, response, form){

	var orderno = request.getParameter('custpage_orderno');
	nlapiLogExecution('ERROR','orderno',orderno);

	// Creating a sublist and adding fields to the sublist
	addFieldsToSublist(form);

	var button = form.addSubmitButton('Pack Complete');

	// Get containers to pack based on search criteria defined
	var salesOrderList = getSalesOrderlist(orderno);

	var ordersFound = false;

	nlapiLogExecution('ERROR','salesOrderList',salesOrderList);
	if(salesOrderList != null && salesOrderList.length > 0){
		var orderno, containerno,ebizorderno,ebizcntrlno,siteid,compid,containersize,totweight,skuno,qty,skutext;	
		for (var i = 0; i < salesOrderList.length; i++){
			orderno = salesOrderList[i].getValue('name');
			containerno = salesOrderList[i].getValue('custrecord_container_lp_no');
			ebizorderno = salesOrderList[i].getValue('custrecord_ebiz_order_no');
			ebizcntrlno = salesOrderList[i].getValue('custrecord_ebiz_cntrl_no');
			compid = salesOrderList[i].getValue('custrecord_comp_id');
			siteid = salesOrderList[i].getValue('custrecord_wms_location');
			containersize = salesOrderList[i].getValue('custrecord_container');
			totweight = salesOrderList[i].getValue('custrecord_total_weight');
			skuno = salesOrderList[i].getValue('custrecord_sku');
			skutext = salesOrderList[i].getText('custrecord_sku');
			qty = salesOrderList[i].getValue('custrecord_act_qty');

			//Added on 18th April 2012


			nlapiLogExecution('ERROR', 'Site Value in Assign Pack Station', siteid);


			var filtersPackLoc = new Array();			
			filtersPackLoc.push(new nlobjSearchFilter('custrecord_stg_tasktype', null, 'anyof', [14]));//Task Type - PACK
			filtersPackLoc.push(new nlobjSearchFilter('custrecord_ebizsitestage', null, 'anyof', siteid));


			var colsPackLoc = new Array();
			colsPackLoc[0] = new nlobjSearchColumn('custrecord_locationstgrule');			
			var searchPackLoc = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filtersPackLoc, colsPackLoc);

			if (searchPackLoc != null){
				/*for (var i = 0; i < searchPackLoc.length; i++) 
				{					
					ddlPackLoc.addSelectOption(searchPackLoc[i].getValue('custrecord_locationstgrule'), searchPackLoc[i].getText('custrecord_locationstgrule'));
					nlapiLogExecution('ERROR','searchPackLoc',searchPackLoc[i].getText('custrecord_locationstgrule'));
				}
				 */
				var vstgrule = searchPackLoc[0].getValue('custrecord_locationstgrule');
				form.getSubList('custpage_items').setLineItemValue('custpage_packlocation', i + 1, vstgrule);
			}



			//upto to here on 18th aprril




			form.getSubList('custpage_items').setLineItemValue('custpage_order', i + 1, orderno);
			form.getSubList('custpage_items').setLineItemValue('custpage_container', i + 1, containerno);
			form.getSubList('custpage_items').setLineItemValue('custpage_internalid', i + 1, salesOrderList[i].getId());
			form.getSubList('custpage_items').setLineItemValue('custpage_ebizordno', i + 1, ebizorderno);
			form.getSubList('custpage_items').setLineItemValue('custpage_ebizcntrlno', i + 1, ebizcntrlno);
			form.getSubList('custpage_items').setLineItemValue('custpage_compid', i + 1, compid);
			form.getSubList('custpage_items').setLineItemValue('custpage_siteid', i + 1, siteid);
			form.getSubList('custpage_items').setLineItemValue('custpage_containersize', i + 1, containersize);
			form.getSubList('custpage_items').setLineItemValue('custpage_totalweight', i + 1, totweight);
			form.getSubList('custpage_items').setLineItemValue('custpage_ebizskuno', i + 1, skuno);
			form.getSubList('custpage_items').setLineItemValue('custpage_ebizsku', i + 1, skutext);
			form.getSubList('custpage_items').setLineItemValue('custpage_qty', i + 1, qty);

			ordersFound = true;
		}
	}

	if(!ordersFound){
		nlapiLogExecution('ERROR', 'No containers to pack', '0');
		var form = nlapiCreateForm('Assign Pack Station');
		showInlineMessage(form, 'Confirmation', 'No closed containers', null);
	}
}

function addFieldsToSublist(form){
	var sublist = form.addSubList("custpage_items", "list", "Assign Pack Station");
	form.setScript('customscript_pickserialnovalidation');
	sublist.addMarkAllButtons();
	sublist.addField("custpage_so", "checkbox", "Assign"); //custpage_select
	sublist.addField("custpage_order", "text", "Order #");
	sublist.addField("custpage_container", "text", "Container #");	
	sublist.addField("custpage_ebizsku", "text", "Item");
	sublist.addField("custpage_qty", "text", "Qty");
	var ddlPackLoc = sublist.addField("custpage_packlocation", "select", "Packing Location").setMandatory(true);			
	var filtersPackLoc = new Array();			
	filtersPackLoc.push(new nlobjSearchFilter('custrecord_stg_tasktype', null, 'anyof', [14]));//Task Type - PACK
	var colsPackLoc = new Array();
	colsPackLoc[0] = new nlobjSearchColumn('custrecord_locationstgrule');			
	var searchPackLoc = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filtersPackLoc, colsPackLoc);

	if (searchPackLoc != null){
		for (var i = 0; i < searchPackLoc.length; i++) 
		{					
			ddlPackLoc.addSelectOption(searchPackLoc[i].getValue('custrecord_locationstgrule'), searchPackLoc[i].getText('custrecord_locationstgrule'));
			nlapiLogExecution('ERROR','searchPackLoc',searchPackLoc[i].getText('custrecord_locationstgrule'));
		}
	}

	sublist.addField("custpage_internalid", "text", "Internal Id").setDisplayType('hidden');
	sublist.addField("custpage_ebizordno", "text", "FF Ebiz ORD #").setDisplayType('hidden');
	sublist.addField("custpage_ebizcntrlno", "text", "FF Ebiz Cntrl #").setDisplayType('hidden');
	sublist.addField("custpage_compid", "text", "Comp Id").setDisplayType('hidden');
	sublist.addField("custpage_siteid", "text", "Site Id").setDisplayType('hidden');
	sublist.addField("custpage_containersize", "text", "Container Size").setDisplayType('hidden');
	sublist.addField("custpage_totalweight", "text", "Total Weight").setDisplayType('hidden');
	sublist.addField("custpage_ebizskuno", "text", "SKU").setDisplayType('hidden');

}

/**
 * 
 * @param orderno
 */
function getSalesOrderlist(orderno){
	var filters = new Array();
	nlapiLogExecution('ERROR','orderno in getSalesOrderlist',orderno);

	if(orderno!=""){
		filters.push( new nlobjSearchFilter('name', null, 'is', orderno));
	}

	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [8]));//Status Flag - C (Picks Confirmed)
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));//Task Type - PICK

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[4] = new nlobjSearchColumn('custrecord_comp_id');
	columns[5] = new nlobjSearchColumn('custrecord_site_id');
	columns[6] = new nlobjSearchColumn('custrecord_container');
	columns[7] = new nlobjSearchColumn('custrecord_total_weight');
	columns[8] = new nlobjSearchColumn('custrecord_sku');
	columns[9] = new nlobjSearchColumn('custrecord_act_qty');
	columns[10] = new nlobjSearchColumn('custrecord_wms_location');

	columns[0].setSort();
	columns[1].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);	
	return searchresults;
}

function processSelectedContainers(request, response, form){

	var lineCount = request.getLineItemCount('custpage_items');
	nlapiLogExecution('ERROR','linecount', lineCount);
	var isRowSelected="";
	var Container="";
	var internalId="";
	var ffordno="";
	var stageLoc="";
	var ebizordno="";
	var ebizcntrlno="";
	var siteid="";
	var compid="";
	var containersize="";
	var oldcontainer="";	
	var oldFFOrdNo="";
	var itemno='';
	//var bulkPACKarr = new Array();
// case# 201413986
	var foarr = new Array();
	var contLParr = new Array();

	var currentUserID = getCurrentUser();
	var vPackedFO=new Array();
	var context = nlapiGetContext();
	nlapiLogExecution('Debug','Remaining usage at the start of processselectedorders ',context.getRemainingUsage());
	for(var k = 1; k <= lineCount; k++)
	{
		nlapiLogExecution('Debug','Remaining usage at the start of for loop ',context.getRemainingUsage());
		isRowSelected= request.getLineItemValue('custpage_items', 'custpage_so', k);  //custpage_select
		nlapiLogExecution('ERROR','Record Selected?', isRowSelected);
		if(isRowSelected == "T"){

			Container = request.getLineItemValue('custpage_items', 'custpage_container', k);
			internalId= request.getLineItemValue('custpage_items', 'custpage_internalid', k);
			ffordno = request.getLineItemValue('custpage_items', 'custpage_order', k);
			stageLoc = request.getLineItemValue('custpage_items', 'custpage_packlocation', k);
			ebizordno = request.getLineItemValue('custpage_items', 'custpage_ebizordno', k);
			ebizcntrlno = request.getLineItemValue('custpage_items', 'custpage_ebizcntrlno', k);					
			compid = request.getLineItemValue('custpage_items', 'custpage_compid', k);
			siteid = request.getLineItemValue('custpage_items', 'custpage_siteid', k);
			containersize = request.getLineItemValue('custpage_items', 'custpage_containersize', k);
			itemno = request.getLineItemValue('custpage_items', 'custpage_ebizskuno', k);
			foarr.push(ffordno);
			contLParr.push(Container);

			updateStatusInOpenTask(internalId,28);//28 - PACK Complete


			if(oldcontainer != Container){

				nlapiLogExecution('ERROR','oldcontainer', Container);
				updateStatusInLPMaster(Container,28);
				CreateShipManifestRecord(ffordno,Container,containersize,compid,siteid);							

				oldcontainer = Container;
				oldFFOrdNo = ffordno;
			}					 


		}

	}

	nlapiLogExecution('Debug','Remaining usage after for loop ',context.getRemainingUsage());
	//for (var t = 0; t < foarr.length; t++) 
	for (var t = 0; t < contLParr.length; t++)
	{
		//var fono = foarr[t];
		var contLP = contLParr[t];
		//if(vPackedFO.indexOf(fono)==-1)
		if(vPackedFO.indexOf(contLP)==-1)
		{	
			try 
			{
				//vPackedFO.push(fono);
				vPackedFO.push(contLP);
				nlapiLogExecution('ERROR', 'Creating Pack Task',ffordno);
				nlapiLogExecution('Debug','Remaining usage at pack task ',context.getRemainingUsage());
				//var vFirstTaskIntId=request.getLineItemValue('custpage_items', 'custpage_internalid', LineIndexarr[t]);
				var AssignPackStationrec =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',internalId);
				
				//AssignPackStationrec.setFieldValue('name', fono); //14 stands for pack
				AssignPackStationrec.setFieldValue('name', ffordno); //14 stands for pack
				AssignPackStationrec.setFieldValue('custrecord_tasktype', 14); //14 stands for pack
				AssignPackStationrec.setFieldValue('custrecord_act_qty', 0); //
			//	AssignPackStationrec.setFieldValue('custrecord_container_lp_no', Container);
				AssignPackStationrec.setFieldValue('custrecord_container_lp_no', contLP);
				AssignPackStationrec.setFieldValue('custrecord_sku', null);
				AssignPackStationrec.setFieldValue('custrecord_wms_status_flag', 28); //28 stands for pack complete
				AssignPackStationrec.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
				AssignPackStationrec.setFieldValue('custrecord_ebizuser', currentUserID);
				AssignPackStationrec.setFieldValue('custrecord_taskassignedto', currentUserID);
				AssignPackStationrec.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', "");
				AssignPackStationrec.setFieldValue('custrecord_device_upload_flag', "");
				AssignPackStationrec.setFieldValue('custrecord_pack_confirmed_date', DateStamp());
				var recordid =	nlapiSubmitRecord(AssignPackStationrec, false, true);
				nlapiLogExecution('ERROR', 'Creating Pack Task recordid',recordid);
			}
			catch (e) 
			{
				if (e instanceof nlobjError)
					nlapiLogExecution('ERROR', 'system error', e.getCode() + '\n'
							+ e.getDetails());
				else
					nlapiLogExecution('ERROR', 'unexpected error', e.toString());
			}
		}
	}
	//CreatePACKTaskBulk(bulkPACKarr,compid,siteid) 
	nlapiLogExecution('Debug','Remaining usage at the end of processselectedorders ',context.getRemainingUsage());
	showInlineMessage(form, 'Confirmation', 'Pack Station Assigned successfully', "");
}

function getParentOrder(orderno){
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_lineord', null, 'is', orderno));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ns_ord');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
	return searchresults;
}
