/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************
 *
 *$Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_CycleCountItemStatus.js,v $
 *$Revision: 1.1.2.2.2.11.4.2 $
 *$Date: 2015/11/16 15:36:01 $
 *$Author: rmukkera $
 *$Name: t_WMS_2015_2_StdBundle_1_152 $
 *
 * DESCRIPTION
 *Functionality
 *
 * REVISION HISTORY
 *$Log: ebiz_RF_CycleCountItemStatus.js,v $
 *Revision 1.1.2.2.2.11.4.2  2015/11/16 15:36:01  rmukkera
 *case # 201415115
 *
 *Revision 1.1.2.2.2.11.4.1  2015/11/13 09:50:53  deepshikha
 *2015.2 issue fix
 *201414587
 *
 *Revision 1.1.2.2.2.11  2014/06/23 07:41:46  skavuri
 *Case# 20148882 (added Focus Functionality for Textbox) Issue Fixed.
 *
 *Revision 1.1.2.2.2.10  2014/06/13 10:11:49  skavuri
 *Case# 20148882 (added Focus Functionality for Textbox)
 *
 *Revision 1.1.2.2.2.9  2014/06/06 07:37:22  skavuri
 *Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 *Revision 1.1.2.2.2.8  2014/05/30 00:34:20  nneelam
 *case#  20148622
 *Stanadard Bundle Issue Fix.
 *
 *Revision 1.1.2.2.2.7  2014/01/07 09:38:14  rmukkera
 *Case # 20126547
 *
 *Revision 1.1.2.2.2.6  2013/12/20 15:41:23  rmukkera
 *Case # 20126438
 *
 *Revision 1.1.2.2.2.5  2013/11/01 12:52:03  rmukkera
 *Case# 20125424
 *
 *Revision 1.1.2.2.2.4  2013/06/03 06:56:47  rrpulicherla
 *CASE201112/CR201113/LOG201121
 *Inventory Changes
 *
 *Revision 1.1.2.2.2.3  2013/04/17 16:02:37  skreddy
 *CASE201112/CR201113/LOG201121
 *added meta tag
 *
 *Revision 1.1.2.2.2.2  2013/03/19 11:48:12  snimmakayala
 *CASE201112/CR201113/LOG2012392
 *Production and UAT issue fixes.
 *
 *Revision 1.1.2.2.2.1  2013/03/05 13:35:38  rmukkera
 *Merging of lexjet Bundle files to Standard bundle
 *
 *Revision 1.1.2.2  2013/02/20 22:30:57  kavitha
 *CASE201112/CR201113/LOG201121
 *Cycle count process - Issue fixes
 *
 *Revision 1.1.2.1  2013/02/14 01:39:44  kavitha
 *CASE201112/CR201113/LOG201121
 *Cycle count process - Issue fixes
 *
 *
 *
 ****************************************************************************/

/**
 * @author LN
 *@version
 *@date
 */
function CycleCountItemStatus(request, response){
	if (request.getMethod() == 'GET') {

		var getLanguage = request.getParameter('custparam_language');
		var st1,st2,st3,st4,st5;

		if( getLanguage == 'es_ES')
		{		
			st1 = "ESTADO DE CUENTA DE CICLO DE ART�CULO";
			st2 = "ESTADO ESPERADO ART�CULO";
			st3 = "ENTRAR EN EL ESTADO ACTUAL DEL ART�CULO";
			st4 = "CONT";
			st5 = "ANTERIOR";			
		}
		else
		{   
			st1 = "CYCLE COUNT ITEM STATUS";
			st2 = "EXPECTED ITEM STATUS";
			st3 = "ENTER ACTUAL ITEM STATUS";
			st4 = "CONT";
			st5 = "PREV";						
		}

		var getPlanNo = request.getParameter('custparam_planno');
		nlapiLogExecution('ERROR', 'getPlanNo', getPlanNo);

		var getRecordId = request.getParameter('custparam_recordid');
		var getBeginLocationId = request.getParameter('custparam_begin_location_id');
		var getBeginLocationName =  request.getParameter('custparam_begin_location_name');
		var getPackCode =  request.getParameter('custparam_packcode');
		var getExpectedQuantity =  request.getParameter('custparam_expqty');
		nlapiLogExecution('ERROR', 'Expected Quantity', getExpectedQuantity);

		var getExpLPNo = request.getParameter('custparam_lpno');
		var getActLPNo = request.getParameter('custparam_actlp');
		var getExpItem = request.getParameter('custparam_expitem');
		var getExpItemDescription = request.getParameter('custparam_expitemdescription');
		var getActualItem = request.getParameter('custparam_actitem');
		var getActualPackCode = request.getParameter('custparam_actpackcode');		
		var getActualItemInternamId = request.getParameter('custparam_expiteminternalid');
		var getActualBatch=request.getParameter('custparam_actbatch');
		var getActualQuantity = request.getParameter('custparam_actualqty');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		nlapiLogExecution('DEBUG', 'getBeginLocation', NextLocation);
		nlapiLogExecution('DEBUG', 'RecordCount', RecordCount);
		var Confirmationflag=request.getParameter('custparam_confmsg');
		var planitem=request.getParameter('custparam_planitem');
		var CCRec = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe', request.getParameter('custparam_recordid'));
		var locationId=CCRec.getFieldValue('custrecord_cyclesite_id');
		var getStatus=CCRec.getFieldText('custrecord_expcyclesku_status');
		var getStatusId=CCRec.getFieldValue('custrecord_expcyclesku_status');

		if(getStatusId == null || getStatusId == "")
		{
			var vRoleLocation=getRoledBasedLocation();
			nlapiLogExecution('ERROR', 'vRoleLocation', vRoleLocation);
			var itemStatusFilters = new Array();
			itemStatusFilters[0] = new nlobjSearchFilter('isinactive',null, 'is','F');
			itemStatusFilters[1] = new nlobjSearchFilter('custrecord_allowrcvskustatus',null, 'is','T');
			itemStatusFilters[2] = new nlobjSearchFilter('custrecord_defaultskustaus',null, 'is','T');
			if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
			{
				//Case # 20126438 Start
				itemStatusFilters[3] = new nlobjSearchFilter('custrecord_ebizsiteskus',null, 'anyof',vRoleLocation);
				//Case # 20126438 End
			}

			/*var itemStatusColumns = new Array();			
			itemStatusColumns[0] = new nlobjSearchColumn('name');*/
			
			
			var itemStatusColumns = new Array();
			/*itemStatusColumns[0] = new nlobjSearchColumn('custrecord_display_sequence');
			itemStatusColumns[1] = new nlobjSearchColumn('name');
			itemStatusColumns[0].setSort();
			itemStatusColumns[1].setSort();*/
			
			itemStatusColumns[0] = new nlobjSearchColumn('custrecord_display_sequence');
			itemStatusColumns[1] = new nlobjSearchColumn('name');
			itemStatusColumns[2] = new nlobjSearchColumn('internalid');
			itemStatusColumns[3] = new nlobjSearchColumn('custrecord_defaultskustaus');

			// Case # 20126898?end
			itemStatusColumns[0].setSort();
			itemStatusColumns[1].setSort();
			itemStatusColumns[2].setSort(); 

			var itemStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);			
			if(itemStatusSearchResult != null && itemStatusSearchResult != '')
			{
				getStatusId = itemStatusSearchResult[0].getId();
				getStatus = itemStatusSearchResult[0].getValue('name');
			}
			nlapiLogExecution('ERROR', 'getStatusId', getStatusId);
			nlapiLogExecution('ERROR', 'getStatus', getStatus);
		}

		var ItemStatus;
		var itemStatusId;
		var itemStatusLoopCount = 0;
		var nextClicked;
		var statusoptionno=0;

		var itemStatusSearchResult=getItemStatusList(locationId);

		if(itemStatusSearchResult!=null && itemStatusSearchResult!='')
		{

			var itemStatusCount = itemStatusSearchResult.length;

			for (var i = 0; itemStatusSearchResult!=null && i < itemStatusSearchResult.length; i++) 
			{		
				itemStatus = itemStatusSearchResult[i].getValue('internalid');				
				nlapiLogExecution('ERROR', 'itemStatus', itemStatus);
				nlapiLogExecution('ERROR', 'getStatus', getStatus);
				nlapiLogExecution('ERROR', 'getStatusId', getStatusId);
				if(itemStatus==getStatusId)
				{
					statusoptionno=parseInt(i) + 1;
				}
			}
		}

		if(statusoptionno == 0){
			getStatus ="";
		}

		if (request.getParameter('custparam_count') != null)
		{
			var itemStatusRetrieved = request.getParameter('custparam_count');
			nlapiLogExecution('ERROR', 'itemStatusRetrieved', itemStatusRetrieved);

			nextClicked = request.getParameter('custparam_nextclicked');
			nlapiLogExecution('ERROR', 'nextClicked', nextClicked);

			itemStatusCount = itemStatusCount - itemStatusRetrieved;
			nlapiLogExecution('ERROR', 'itemStatusCount', itemStatusCount);
		}
		else
		{
			var itemStatusRetrieved = 0;
			nlapiLogExecution('ERROR', 'itemStatusRetrieved', itemStatusRetrieved);
		}

		if (itemStatusCount > 10)
		{
			itemStatusLoopCount = 10;
		}
		else
		{
			itemStatusLoopCount = itemStatusCount;
		}

		var functionkeyHtml=getFunctionkeyScript('_rf_cyclecountitemstatus'); 
		var html = "<html><head><title>" + st1 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
	//	html = html + " document.getElementById('enterstatus').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cyclecountitemstatus' method='POST'>";
		html = html + "		<table>";		
		html = html + "				<tr><td><input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
		html = html + "				<input type='hidden' name='hdnCycleCountPlanNo' value=" + getPlanNo + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationName' value=" + getBeginLocationName + ">";
		html = html + "				<input type='hidden' name='hdnRecordId' value=" + getRecordId + ">";
		html = html + "				<input type='hidden' name='hdnPackCode' value=" + getPackCode + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";		
		html = html + "				<input type='hidden' name='hdnExpLPNo' value=" + request.getParameter('custparam_lpno') + "></td>";
		html = html + "				<input type='hidden' name='hdnActLPNo' value=" + request.getParameter('custparam_actlpno') + "></td>";
		html = html + "				<input type='hidden' name='hdnExpectedItemDescription' value=" + getExpItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnActualItem' value=" + getActualItem + ">";
		html = html + "				<input type='hidden' name='hdnActualPackCode' value=" + getActualPackCode + ">";
		html = html + "				<input type='hidden' name='hdnActualQty' value=" + getActualQuantity + ">";
		html = html + "				<input type='hidden' name='hdniteminternalid' value=" + getActualItemInternamId + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnnext' value=" + NextLocation + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				<input type='hidden' name='hdnnewstatusId' value=" + newstatusid + ">";
		html = html + "				<input type='hidden' name='hdnoldstatusoption' value=" + StatusNo + ">";
		html = html + "				<input type='hidden' name='hdnlocationId' value=" + locationId + ">";
		html = html + "				<input type='hidden' name='hdnconfflag' value=" + Confirmationflag + ">";
		html = html + "				<input type='hidden' name='hdnplanitem' value='" + planitem + "'>";	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st2+" :<label>" + getStatus + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st3+"";
		html = html + "				</td>";        
		html = html + "			</tr>";		
		html = html + "			<tr>";
		//Case # 20126547 Start
		if(statusoptionno != 0)
		{
			html = html + "				<td align = 'left'><input name='enterstatus' id='enterstatus' type='text' value=" + statusoptionno + ">";
		}
		else
		{
			html = html + "				<td align = 'left'><input name='enterstatus' id='enterstatus' type='text' >";	
		}
		//Case # 20126547 End
		html = html + "				</td>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st4+" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false' style='width:40;height:40;'/>";
		html = html + "					"+st5+" <input name='cmdPrevious' type='submit' value='F7' style='width:40;height:40;'/>";
		html = html + "					SKIP <input name='cmdSkip' type='submit' value='F8' style='width:40;height:40;'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		if (itemStatusLoopCount > 10)
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>NEXT <input name='cmdNext' type='submit' value='F8'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
		}

		nlapiLogExecution('ERROR', 'itemStatusLoopCount', itemStatusLoopCount);

		var StatusNo=itemStatusRetrieved;
		var newstatusid;
		for (var i = itemStatusRetrieved; i < (parseInt(itemStatusRetrieved) + parseInt(itemStatusLoopCount)); i++) {
			var itemStatusSearchResults = itemStatusSearchResult[i];

			if (itemStatusSearchResults != null)
			{			
				itemStatus = itemStatusSearchResults.getValue('name');
				itemStatusId = itemStatusSearchResults.getId();	
				//nlapiLogExecution('ERROR', 'itemStatus', itemStatus);
				//nlapiLogExecution('ERROR', 'getStatus', getStatus);
				if(itemStatus==getStatus)
				{
					StatusNo=parseInt(i) + 1;
					newstatusid=itemStatusId;
				}
			}
			var value = parseInt(i) + 1;
			html = html + "			<tr><td align = 'left'>"+ value +". <label>" + itemStatus + "</label></td></tr>";
		}
		html = html + "		 </table>";

		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterstatus').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		var CCarray = new Array();

		var optedEvent = request.getParameter('cmdPrevious');
		nlapiLogExecution('DEBUG', 'Key pressed', optedEvent);

		var CCarray = new Array();
		//var optedEvent = request.getParameter('cmdPrevious');
		var optedEvent1 = request.getParameter('cmdSkip');
		var getLanguage = request.getParameter('hdngetLanguage');
		CCarray["custparam_language"] = getLanguage;
		var getNewStatusoption = request.getParameter('enterstatus');
		var getNewStatus = request.getParameter('hdnnewstatusId');
		var oldstatusoption = request.getParameter('hdnoldstatusoption');

		nlapiLogExecution('ERROR', 'getLanguage', CCarray["custparam_language"]);		

		var st7;
		if( getLanguage == 'es_ES')
		{

			st7= "C&#211;DIGO DE PAQUETE NO V&#193;LIDO";
			//Case # 20126547 Start
			st8="POR FAVOR, ENTRE ESTADO TEMA";
			//Case # 20126547 End

		}
		else
		{

			st7="INVALID ITEM STATUS";
			//Case # 20126547 Start
			st8="PLEASE ENTER ITEM STATUS";
			//Case # 20126547 End
		}
		CCarray["custparam_error"] = st7;
		CCarray["custparam_screenno"] = '33C';
		CCarray["custparam_recordid"] = request.getParameter('custparam_recordid');
		CCarray["custparam_planno"] = request.getParameter('custparam_planno');
		CCarray["custparam_begin_location_id"] =request.getParameter('custparam_begin_location_id');
		CCarray["custparam_begin_location_name"] = request.getParameter('custparam_begin_location_name');
		CCarray["custparam_lpno"] = request.getParameter('custparam_lpno');
		CCarray["custparam_packcode"] = request.getParameter('custparam_packcode');
		CCarray["custparam_expqty"] = request.getParameter('custparam_expqty');
		CCarray["custparam_actlp"] = request.getParameter('custparam_actlp');
		CCarray["custparam_expitem"] = request.getParameter('custparam_expitem');
		CCarray["custparam_expitemdescription"] = request.getParameter('custparam_expitemdescription');
		CCarray["custparam_actitem"] = request.getParameter('custparam_actitem');
		CCarray["custparam_actpackcode"] = request.getParameter('custparam_actpackcode');
		CCarray["custparam_actualqty"] = request.getParameter('custparam_actualqty');
		CCarray["custparam_expiteminternalid"] = request.getParameter('hdniteminternalid');
		CCarray["custparam_noofrecords"] = request.getParameter('hdnRecCount');
		CCarray["custparam_nextlocation"] = request.getParameter('hdnnext');
		CCarray["custparam_skustatus"] = request.getParameter('custparam_skustatus');
		CCarray["custparam_actbatch"] = request.getParameter('custparam_actbatch');
		CCarray["custparam_expqty"] = request.getParameter('hdnExpectedQuantity');
		CCarray["custparam_confmsg"] = request.getParameter('hdnconfflag');
		CCarray["custparam_planitem"] = request.getParameter('hdnplanitem');
		if (optedEvent == 'F7') 
		{  
			
			nlapiLogExecution('DEBUG', 'Cycle Count Item Status F7 Pressed');
			response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_quantity', 'customdeploy_rf_cyclecount_quantity_di', false, CCarray);
		}
		else if(optedEvent1=='F8')
		{
			nlapiLogExecution('ERROR','SKIP','SKIP the task');
			nlapiLogExecution('ERROR', 'getRecordId', CCarray["custparam_recordid"]);
			var CCRec = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe', request.getParameter('custparam_recordid'));
			var skipcount=CCRec.getFieldValue('custrecord_cyc_skip_task');
			nlapiLogExecution('ERROR','skipcount',skipcount);

			if(skipcount=='' || skipcount==null)
			{
				skipcount=0;
			}
			skipcount=parseInt(skipcount)+1;
			CCRec.setFieldValue('custrecord_cyc_skip_task',skipcount);
			var id=	nlapiSubmitRecord(CCRec,true);
			nlapiLogExecution('ERROR','skipid',id);

			response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_location', 'customdeploy_rf_cyclecount_location_di', false, CCarray);
		}
		else 
		{
			var getNewStatus = '';
			if (getNewStatusoption != '') 
			{
				if(oldstatusoption!=getNewStatusoption)
				{
					var itemStatusSearchResult=getItemStatusList(request.getParameter('hdnlocationId'));
					if(itemStatusSearchResult!=null && itemStatusSearchResult!='')
					{
						for(i=0;i<itemStatusSearchResult.length;i++)
						{
							var statusoption=i+1;

							if(statusoption==getNewStatusoption)
							{
								getNewStatus = itemStatusSearchResult[i].getId();	
							}
						}
					}
				}

				nlapiLogExecution('ERROR', 'new status', getNewStatus);
				if(getNewStatus == null || getNewStatus == "")
				{
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
					return;
				}
				CCarray["custparam_actitemstatus"] = getNewStatus;
				var itemSubtype = nlapiLookupField('item', CCarray["custparam_actitem"], ['recordType','custitem_ebizserialin']);
				CCarray["custparam_itemtype"] = itemSubtype.recordType;
				nlapiLogExecution('ERROR', 'itemSubtype.recordType =', itemSubtype);
				var actQty=request.getParameter('custparam_actualqty');
				if (itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.custitem_ebizserialin == 'T') 
				{
					if(parseInt(actQty)>0)
					{
						response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_serialscan', 'customdeploy_rf_cyclecount_serialscan_di', false, CCarray);
						return;
					}
					else
					{
						nlapiLogExecution('ERROR', 'Cycle Count Item status Redirected OK');
						response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_confirm', 'customdeploy1', false, CCarray);
						return;
					}
										
				}
				else
				{
					nlapiLogExecution('ERROR', 'Cycle Count Item status Redirected OK');
					response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_confirm', 'customdeploy1', false, CCarray);
				}
			}
			else 
			{
				//Case # 20126547 Start
				CCarray["custparam_error"] = st8;
				//Case # 20126547 End
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
				nlapiLogExecution('DEBUG', 'Catch : Cycle Count Item Status not found');
			}
		}
	}
}

function getItemStatusList(whloc)
{
	var itemStatusSearchResult = new Array();
	var itemStatusColumns = new Array();
	var itemStatusFilters = new Array();
	itemStatusFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	nlapiLogExecution('Error', 'whloc',whloc);
	if(whloc!=null && whloc!='' && whloc!='null' )
		itemStatusFilters.push(new nlobjSearchFilter('custrecord_ebizsiteskus', null, 'anyOf', [whloc]));
	
	itemStatusColumns[0] = new nlobjSearchColumn('custrecord_display_sequence');
	itemStatusColumns[1] = new nlobjSearchColumn('name');
	itemStatusColumns[2] = new nlobjSearchColumn('internalid');
	itemStatusColumns[3] = new nlobjSearchColumn('custrecord_defaultskustaus');

	
	itemStatusColumns[0].setSort();
	itemStatusColumns[1].setSort();
	itemStatusColumns[2].setSort(); 

	var itemStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);

	return itemStatusSearchResult;

}
