/***************************************************************************
?????????????????????????????eBizNET
??????????????????????eBizNET Solutions Inc
 ****************************************************************************
 *
 *? $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Masters/Client/ebiz_PickZone_CL.js,v $
 *? $Revision: 1.6.2.1 $
 *? $Date: 2012/02/09 16:10:57 $
 *? $Author: snimmakayala $
 *? $Name: t_NSWMS_LOG201121_26 $
 *
 * DESCRIPTION
 *? Functionality
 *
 * REVISION HISTORY
 *? $Log: ebiz_PickZone_CL.js,v $
 *? Revision 1.6.2.1  2012/02/09 16:10:57  snimmakayala
 *? CASE201112/CR201113/LOG201121
 *? Physical and Virtual Location changes
 *?
 *? Revision 1.7  2012/02/09 14:43:43  snimmakayala
 *? CASE201112/CR201113/LOG201121
 *? Physical and Virtual Location changes
 *?
 *? Revision 1.6  2012/01/06 14:40:36  spendyala
 *? CASE201112/CR201113/LOG201121
 *? added code to trim the string.
 *?
 *
 ****************************************************************************/

/**
 * @param type
 * @returns {Boolean}
 */
function fnCheckPickZoneEntry(type)
{
	var varZonename = nlapiGetFieldValue('custrecord_putzoneid');        
	var vSite = nlapiGetFieldValue('custrecord_ebizsiteput');
	var vCompany = nlapiGetFieldValue('custrecord_ebizcompanyput');

	/*
	 * Added on 1/4/2011 by Phani
	 * The below variable will fetch the internal id of the record selected.
	 */    

	var vId = nlapiGetFieldValue('id');
	if(varZonename != "" && varZonename != null && vSite != "" && vSite != null && vCompany != ""  && vCompany != null)
	{
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_putzoneid', null, 'is', varZonename.replace(/\s+$/, ""));//Added by suman on 06/01/12 to trim spaces at right.        
		filters[1] = new nlobjSearchFilter('custrecord_ebizsiteput', null, 'anyof', ['@NONE@',vSite]);
		filters[2] = new nlobjSearchFilter('custrecord_ebizcompanyput', null, 'anyof', ['@NONE@',vCompany]);

		/*
		 * Added on 1/4/2011 by Phani
		 * The below variable will filter based on the internal id of the record selected.
		 */
		if (vId != null && vId != "") {
			filters[3] = new nlobjSearchFilter('internalid', null, 'noneof', vId);
		}

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_putpick_zone', null, filters);

		if (searchresults != null && searchresults.length > 0) 
		{
			alert("The Put/Pick Zone # already exists.");
			return false;
		}  
		return true;
	}
	else
	{
		return true;
	}
}

/*
function fnCheckPickMethodEdit(type,name)
{
	var vId = nlapiGetFieldValue('id');
	var varZonename = nlapiGetFieldValue('custrecord_putzoneid');    
    var vSite = nlapiGetFieldValue('custrecord_ebizsiteput');
    var vCompany = nlapiGetFieldValue('custrecord_ebizcompanyput');


    var filters = new Array();
    var filters = new Array();
    filters[0] = new nlobjSearchFilter('custrecord_putzoneid', null, 'is', varPickMethodname);        
    filters[1] = new nlobjSearchFilter('custrecord_ebizsiteput', null, 'anyof', [vSite]);
    filters[2] = new nlobjSearchFilter('custrecord_ebizcompanyput', null, 'anyof', [vCompany]);
    filters[3] = new nlobjSearchFilter('internalid', null, 'noneof',vId);    

 var searchresults = nlapiSearchRecord('customrecord_ebiznet_putpick_zone', null, filters);

    if (searchresults != null && searchresults.length > 0) 
	{
        alert("The Put/Pick Zone # already exists.");
        return false;
    }   
	return true;   
}
 */