/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Outbound/Suitelet/Attic/ebiz_RF_Shipping_UpdateSRR.js,v $
 *     	   $Revision: 1.1.2.1.4.5 $
 *     	   $Date: 2014/06/13 10:58:03 $
 *     	   $Author: skavuri $
 *     	   $Name: t_NSWMS_2014_1_3_125 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Shipping_UpdateSRR.js,v $
 * Revision 1.1.2.1.4.5  2014/06/13 10:58:03  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.1.4.4  2014/05/30 00:41:06  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.1.4.3  2013/06/11 14:30:20  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.2.1.4.2  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.1.4.1  2013/03/19 11:46:50  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.1.2.1  2012/11/28 03:18:24  kavitha
 * CASE201112/CR201113/LOG201121
 * Update SRR# CR - GS USA
 *
 *
 *****************************************************************************/
function EnterShipmentRouteRequestNo(request, response)
{
	var matchFound = true;
	if (request.getMethod() == 'GET') 
	{   

		var getLanguage = request.getParameter('custparam_language');	
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4;
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "INTRO/SCAN ID DE CLIENTE:";
			st2 = "INTRO/SCAN SRR#:";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";

		}
		else
		{
			st0 = "";
			st1 = "ENTER/SCAN CUSTOMER ID:";
			st2 = "ENTER/SCAN SRR#:";
			st3 = "SEND";
			st4 = "PREV";
		}    	
		var functionkeyHtml=getFunctionkeyScript('_rfUpdateSRR'); 
		var html = "<html><head><title>" + st0 + " </title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//html = html + " document.getElementById('entercustomerid').focus();";        
		html = html + "</script>";        
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rfUpdateSRR' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1;	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entercustomerid' id='entercustomerid' type='text'/>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2;	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterSRR' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + "<input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st4 + " <input name='cmdPrevious' type='submit' value='F7'/>";        
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('entercustomerid').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var varEnteredCustId = request.getParameter('entercustomerid');
		nlapiLogExecution('DEBUG', 'Entered Customer Id', varEnteredCustId);

		var varEnteredSRR = request.getParameter('enterSRR');
		nlapiLogExecution('DEBUG', 'Entered SRR #', varEnteredSRR);

		var SOarray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		SOarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', SOarray["custparam_language"]); 

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');       

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to Shipping menu.
		if (optedEvent == 'F7') {
			response.sendRedirect('SUITELET', 'customscript_rf_shipping_menu', 'customdeploy_rf_shipping_menu_di', false, SOarray);
		}
		else 
		{        
			if (varEnteredCustId != '' && varEnteredCustId != null) 
			{
				if (varEnteredSRR != '' && varEnteredSRR != null) 
				{
					var ErrorMessage = updateSRR(varEnteredCustId,varEnteredSRR);
					if(ErrorMessage=="")
					{
						response.sendRedirect('SUITELET', 'customscript_rf_shipping_updsrr_success', 'customdeploy_rf_shipping_updsrr_succ_di', false, null);
					}
					else
					{
						SOarray["custparam_error"] = ErrorMessage;
						SOarray["custparam_screenno"] = 'UpdateSRR';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					}
				}
				else
				{
					SOarray["custparam_error"] = "INVALID SRR#";
					SOarray["custparam_screenno"] = 'UpdateSRR';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
				}
			}
			else
			{
				SOarray["custparam_error"] = "INVALID CUSTOMER ID";
				SOarray["custparam_screenno"] = 'UpdateSRR';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			}	 
		}
	}
}
/**
 * To update SRR# field in ShipManifest table for the given Customer ID
 * 
 * @param CustomerID
 * @param RouteNo   
 * @returns message with proper error message
 */
function updateSRR(CustomerID,RouteNo)
{
	var Message = "";
	nlapiLogExecution('DEBUG', 'Into updateSRR CustomerID', CustomerID);
	nlapiLogExecution('DEBUG', 'Into updateSRR RouteNo', RouteNo);
	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ship_custid', null, 'is', CustomerID));
	filter.push(new nlobjSearchFilter('custrecord_ebiz_ship_routing_reqno', null, 'isempty'));
	var column = new Array();
	column[0] = new nlobjSearchColumn('internalid') ;
	var searchRec= nlapiSearchRecord('customrecord_ship_manifest', null, filter, column);
	if (searchRec!=null)
	{
		for (var s = 0; s < searchRec.length; s++)
		{
			var EbizOrdNo = searchRec[s].getValue('internalid');
			var transaction = nlapiLoadRecord('customrecord_ship_manifest', EbizOrdNo); 

			transaction.setFieldValue('custrecord_ebiz_ship_routing_reqno', RouteNo);
			nlapiSubmitRecord(transaction, true);
		}
	}
	else
	{	
		Message = "INVALID CUSTOMER ID";
	}	
	nlapiLogExecution('DEBUG', 'Out of updateSRR', RouteNo);
	return Message;
}