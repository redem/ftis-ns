/***************************************************************************
 eBizNET SOLUTIONS LTD
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/ebiz_RF_Shipping_SizeId.js,v $
 *     	   $Revision: 1.3.4.2.4.7.2.14 $
 *     	   $Date: 2015/09/01 15:05:34 $
 *     	   $Author: deepshikha $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Shipping_SizeId.js,v $
 * Revision 1.3.4.2.4.7.2.14  2015/09/01 15:05:34  deepshikha
 * 2015.2 issue fixes
 * 201414121
 *
 * Revision 1.3.4.2.4.7.2.13  2014/11/26 14:32:49  schepuri
 *  201411087,201411124�  issue fix
 *
 * Revision 1.3.4.2.4.7.2.12  2014/11/19 15:32:59  skavuri
 * Case# 201411087 Std bundle issue fixed
 *
 * Revision 1.3.4.2.4.7.2.11  2014/10/28 15:22:24  skavuri
 * Case# 201410521 Std bundle issue fixed
 *
 * Revision 1.3.4.2.4.7.2.10  2014/09/26 14:17:14  skavuri
 * Case# 201410521 SB issue Fixed
 *
 * Revision 1.3.4.2.4.7.2.9  2014/08/06 15:47:03  nneelam
 * case#  20149841
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.3.4.2.4.7.2.8  2014/07/11 07:24:59  gkalla
 * case#20149124
 * Sonic In RF Build ship we are not updating FO# in ship task of open
 * task
 *
 * Revision 1.3.4.2.4.7.2.7  2014/06/13 10:59:33  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.3.4.2.4.7.2.6  2014/05/30 00:41:06  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.3.4.2.4.7.2.5  2014/01/06 13:33:05  snimmakayala
 * Case# : 20125731
 * Productivity Report Related Changes
 *
 * Revision 1.3.4.2.4.7.2.4  2014/01/06 13:19:17  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.3.4.2.4.7.2.3  2013/06/11 14:30:20  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.3.4.2.4.7.2.2  2013/05/08 15:08:34  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.3.4.2.4.7.2.1  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.3.4.2.4.7  2013/02/15 14:59:20  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.3.4.2.4.6  2013/01/21 15:51:53  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA-BuildShip Unit Chnages.
 *
 * Revision 1.3.4.2.4.5  2012/12/24 13:21:51  schepuri
 * CASE201112/CR201113/LOG201121
 * checking for nulls
 *
 * Revision 1.3.4.2.4.4  2012/11/30 23:56:48  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * GSUSA UAT Fixes.
 *
 * Revision 1.3.4.2.4.3  2012/11/01 14:55:23  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.3.4.2.4.2  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.3.4.2.4.1  2012/09/25 07:13:53  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Language
 *
 * Revision 1.3.4.2  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.3.4.1  2012/02/22 13:05:06  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.3  2011/10/03 08:25:05  snimmakayala
 * CASE201112/CR201113/LOG20112
 *
 * Revision 1.1  2011/09/23 16:22:25  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.4  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.3  2011/04/21 07:42:42  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * RF Packing Functionality
 *
 * Revision 1.2  2011/04/21 07:03:02  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * RF Packing Functionality
 *
 * Revision 1.1  2011/04/19 14:19:06  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * RF Packing Functionality
 *
 *
 *****************************************************************************/
function EnterSizeId(request, response)
{

	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);
	var matchFound = true;
	if (request.getMethod() == 'GET') 
	{   
		var getShipLP = request.getParameter('custparam_shiplpno'); 
		var getTotWgt = request.getParameter('custparam_GWgt');
		var getTotCube = request.getParameter('custparam_GCube');
		var getSiteId = request.getParameter('custparam_site_id');
		var getCompId = request.getParameter('custparam_comp_id');
		var getEbizWave = request.getParameter('custparam_waveno');
		var getOrderIntid = request.getParameter('custparam_orderintid');
		var getFOId = request.getParameter('custparam_foname');
		nlapiLogExecution('DEBUG', 'SOarray["custparam_GWgt"]', getTotWgt);
		nlapiLogExecution('DEBUG', 'SOarray["custparam_GCube"]', getTotCube);
		nlapiLogExecution('DEBUG', 'getOrderIntid', getOrderIntid);
		nlapiLogExecution('DEBUG', 'getFOId', getFOId);
		var getLanguage = request.getParameter('custparam_language');	
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
		var whLocation = request.getParameter('custparam_locationId'); //Case# 201411087

		var st0,st1,st2,st3,st4,st5;
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "ENV&#205;O LP #";
			st2 = "ENTRAR ID SIZE:";
			st3 = "ENTER PESO BRUTO:";
			st4 = "GUARDAR";
			st5 = "GUARDAR Y CERRAR";

		}
		else
		{
			st0 = "";
			st1 = "SHIP LP#";
			st2 = "ENTER SIZE ID:";
			st3 = "ENTER GROSS WEIGHT:";
			st4 = "SAVE";
			st5 = "SAVE & CLOSE";

		}    	

		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head><title>" + st0 + " </title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";

		//html = html + " document.getElementById('entersize').focus();";        

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		//html = html + "	  alert(document.getElementById('cmdSend').disabled);";

		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";        
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + ": <label>" + getShipLP + "</label>"; 
		html = html + "				</td>";
		html = html + "			</tr>"; 
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2;	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entersize' id='entersize' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3;	

		html = html + "				<input type='hidden' name='hdntotwgt' value=" + getTotWgt + ">";
		html = html + "				<input type='hidden' name='hdntotcube' value=" + getTotCube + ">";
		html = html + "				<input type='hidden' name='hdnShipLP' value=" + getShipLP + ">";
		html = html + "				<input type='hidden' name='hdnSite' value=" + getSiteId + ">";
		html = html + "				<input type='hidden' name='hdnComp' value=" + getCompId + ">";
		html = html + "				<input type='hidden' name='hdnEbizWave' value=" + getEbizWave + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				<input type='hidden' name='hdnOrdIntid' value=" + getOrderIntid + ">";
		html = html + "				<input type='hidden' name='hdnfoid' value=" + getFOId + ">";
		/* html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
        html = html + "				<input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
        html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
        html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
        html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
        html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
        html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
        html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";        
        html = html + "				<input type='hidden' name='hdnBeginBinLocation' value=" + getBeginBinLocation + ">";
        html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vclusterNo + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value=" + vBatchno + ">";
		html = html + "				<input type='hidden' name='hdnlinecount' value=" + vlinecount + ">";
		html = html + "				<input type='hidden' name='hdnloopcount' value=" + vloopcount + ">";
		 */
		html = html + "				<input type='hidden' name='hdnwhLocation' value=" + whLocation + ">"; //Case# 201411087
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterGrossWgt' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" +st4 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdClose.disabled=true; return false'/>";
		html = html + "					" + st5 + " <input name='cmdClose' type='submit' value='F10'/>";        
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('entersize').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var varEnteredWgt = request.getParameter('enterGrossWgt');
		nlapiLogExecution('DEBUG', 'Entered Gross Weight', varEnteredWgt);

		var SOarray = new Array();
		var getLanguage = request.getParameter('hdngetLanguage');
		SOarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', SOarray["custparam_language"]);    	
		var whLocation = request.getParameter('hdnwhLocation');//CasE# 201411087
		var st9,st10;
		if( getLanguage == 'es_ES')
		{			
			st9 = "IDENTIFICACI&#211;N TAMA&#209;O NO V&#193;LIDO";
			st10 ="Peso Bruto V�LIDA";
		}
		else
		{			
			st9 = "INVALID SIZE ID";
			st10="INVALID GROSS WEIGHT"; //Case# 201410521
		}
		var getShipLP = request.getParameter('custparam_shiplpno');

		var getTotWgt = request.getParameter('custparam_GWgt');
		var getTotCube = request.getParameter('custparam_GCube');
		var getSiteId = request.getParameter('custparam_site_id');
		var getCompId = request.getParameter('custparam_comp_id');
		var getEbizwave = request.getParameter('hdnEbizWave');

		var getFOId = request.getParameter('hdnfoid');

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdClose');  
		var sizeId = request.getParameter('entersize');  
		var GrWeight = request.getParameter('enterGrossWgt'); 
		var ShipLP=request.getParameter('hdnShipLP');
		var vSiteId=request.getParameter('hdnSite');
		var vCompId=request.getParameter('hdnComp');
		var vOrderIntid = request.getParameter('hdnOrdIntid');
		SOarray["custparam_foname"]=getFOId;
		nlapiLogExecution('DEBUG', 'vOrderIntid', vOrderIntid);
		nlapiLogExecution('DEBUG', 'getFOId', getFOId);

//		case# 201417100
		nlapiLogExecution('DEBUG', 'vSiteId', vSiteId);
		nlapiLogExecution('DEBUG', 'sizeId', sizeId);
		nlapiLogExecution('DEBUG', 'whLocation', whLocation);

		if(vSiteId == null || vSiteId =='' || vSiteId == 'null')
			vSiteId=whLocation;
		nlapiLogExecution('DEBUG', 'vSiteId after', vSiteId);
		if (sessionobj!=context.getUser()) {
			try
			{

				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}
				var blnClosedFlag='N';
				if(optedEvent=='F10')
					blnClosedFlag='T';
				//Case# 201410521 starts
				if(varEnteredWgt!='' && varEnteredWgt !='null' && varEnteredWgt !=null && isNaN(varEnteredWgt))
				{
					SOarray["custparam_shiplpno"]=request.getParameter('hdnShipLP'); 
					SOarray["custparam_GWgt"]=request.getParameter('hdntotwgt');
					SOarray["custparam_GCube"]=request.getParameter('hdntotcube');
					SOarray["custparam_site_id"]=request.getParameter('hdnSite');
					SOarray["custparam_comp_id"]=request.getParameter('hdnComp');
					SOarray["custparam_error"] = st10;
					SOarray["custparam_screenno"] = '40';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;
				}
				//Case# 201410521 ends
				if (sizeId != '' && sizeId != null) 
				{
					var ContainerInternalId=GetContainerInternalId(sizeId,vSiteId);// To get internal Id of a container
					if(ContainerInternalId!= null && ContainerInternalId != "")
					{
						var vDIms=getContainerCubeAndTarWeight(ContainerInternalId,"");// To get Container weight and volume from container to calculate tar weight and cube
						var TotWeight= request.getParameter('hdntotwgt');  
						var TotVolume= request.getParameter('hdntotcube');



						var inventoryInout;
						var vTareWeight=0,vTareVol=0;
						if(vDIms.length >0)			{
							inventoryInout=vDIms[2];
							if(vDIms[1] != null && vDIms[1]!="")
								vTareWeight=parseFloat(vDIms[1]);
							if(vDIms[0] != null && vDIms[0]!="")
								vTareVol=parseFloat(vDIms[0]);
						}

						SaveSizeID(blnClosedFlag,ContainerInternalId,inventoryInout,vTareWeight,vTareVol,GrWeight,TotWeight,TotVolume,ShipLP,vSiteId,vCompId,getEbizwave,vOrderIntid,getFOId,whLocation);// case# 201411087,201411124
						nlapiLogExecution('ERROR', 'NewShipLPNo', ShipLP);
						nlapiLogExecution('ERROR', 'vOrdNo', vOrderIntid);
						// case# 201417277
						try
						{
							var SKIDLabelRule=getSystemRule('Use SKID Label');
							if(SKIDLabelRule!=null && SKIDLabelRule!='')
							{ 
								SKIDLabelgeneration(ShipLP,vOrderIntid);
							}
							
						}
						catch(e)
						{
							nlapiLogExecution('ERROR', 'e for labl generation', e);
						}
						// case# 201417315
						response.sendRedirect('SUITELET', 'customscript_rf_shipping_menu', 'customdeploy_rf_shipping_menu_di', false, SOarray);
						return;
					}
					else // to give the error     
					{
						SOarray["custparam_shiplpno"]=request.getParameter('hdnShipLP'); 
						SOarray["custparam_GWgt"]=request.getParameter('hdntotwgt');
						SOarray["custparam_GCube"]=request.getParameter('hdntotcube');
						SOarray["custparam_site_id"]=request.getParameter('hdnSite');
						SOarray["custparam_comp_id"]=request.getParameter('hdnComp');
						SOarray["custparam_locationId"]=whLocation;// case# 201416992
						SOarray["custparam_error"] = st9;
						SOarray["custparam_screenno"] = '40';
						SOarray["custparam_orderintid"]=request.getParameter('hdnOrdIntid');// case#
						nlapiLogExecution('DEBUG', 'request.getParameter(hdnfoid)1',request.getParameter('hdnfoid'));
						SOarray["custparam_foname"]=request.getParameter('hdnfoid');

						//SOarray["custparam_shiplpno"] = '';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);

						return;
					} 
				}
				else // to give the error
				{
					SOarray["custparam_shiplpno"]=request.getParameter('hdnShipLP'); 
					SOarray["custparam_GWgt"]=request.getParameter('hdntotwgt');
					SOarray["custparam_GCube"]=request.getParameter('hdntotcube');
					SOarray["custparam_site_id"]=request.getParameter('hdnSite');
					SOarray["custparam_comp_id"]=request.getParameter('hdnComp');
					SOarray["custparam_locationId"]=whLocation;
					SOarray["custparam_error"] = st9;
					SOarray["custparam_screenno"] = '40';
					SOarray["custparam_orderintid"]=request.getParameter('hdnOrdIntid');
					nlapiLogExecution('DEBUG', 'request.getParameter(hdnfoid)',request.getParameter('hdnfoid'));
					SOarray["custparam_foname"]=request.getParameter('hdnfoid');
					//SOarray["custparam_shiplpno"] = '';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);

					return;
				} 
			}
			catch (ex)  {
				nlapiLogExecution('DEBUG', 'ex catch',ex);
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');

			}
		}
		else
		{
			//38
			SOarray["custparam_screenno"] = '38';
			SOarray["custparam_error"] = 'SHIPPING ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
		}
	}
}
/**
 * To get internal Id of a container
 * @param ContainerName
 * @returns
 */
function GetContainerInternalId(ContainerName,whsite)
{
	var filters = new Array();
	if(ContainerName != "" && ContainerName != null){ 
		filters.push(new nlobjSearchFilter('name', null, 'is', [ContainerName]));
	}
	// case# 201416248
	filters.push(new nlobjSearchFilter('custrecord_containertype', null, 'anyof', ['1','4']));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	//filters.push(new nlobjSearchFilter('custrecord_containername', null, 'is', containersize));				
	if(whsite!=null && whsite!='')
		filters.push(new nlobjSearchFilter('custrecord_ebizsitecontainer', null, 'anyof', ['@NONE@',whsite]));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	var ContainerSearchResults = nlapiSearchRecord('customrecord_ebiznet_container', null, filters, columns);
	var internalId;
	if(ContainerSearchResults!= null && ContainerSearchResults != "" && ContainerSearchResults.length>0)
		internalId=ContainerSearchResults[0].getId();
	return internalId;
}
/**
 * It updates Opentask task type with Ship, WMS status flag with Built onto ship unit,
 * updates master lp WMS status flag with pack complete if it wants close otherwise status is Build ship,
 *  and tare weight (if user enters Gross weight then we update with Gross weight otherwise update with tare weight), tare cube.
 * 
 * @param blnClosedFlag
 * @param sizeId
 * @param inventoryInout
 * @param vTareWeight
 * @param vTareVol
 * @param GrWeight
 * @param TotWeight
 * @param TotVolume
 * @param NewShipLPNo
 * @param vSiteId
 * @param vCompId
 */
function SaveSizeID(blnClosedFlag,sizeId,inventoryInout,vTareWeight,vTareVol,GrWeight,TotWeight,TotVolume,NewShipLPNo,vSiteId,vCompId,getEbizwave,vOrderIntid,getFOId,whLocation)// case# 201411087,201411124
{

	var currentContext = nlapiGetContext();  
	var currentUserID = currentContext.getUser();

	//var vCompId="";
	//var vSiteId="";
	//If inventory Inour is IN then 
	//total weight=tot containers weight + tare weight and tot cube=tot containers cube + tare cube
	if(inventoryInout=="1" || inventoryInout =="IN")
	{
		TotWeight+=vTareWeight;
		TotVolume=vTareVol;
	}
	//If user entered the Gross weight then we consider Gross weight as total weight
	if(GrWeight!= null && GrWeight !='')
		TotWeight=GrWeight;
	/*if (request.getParameter('custpage_wave')!=null && request.getParameter('custpage_wave')!="" )
		{
			ebizWaveNo=request.getParameter('custpage_wave');
		}*/
	var actualDate = DateStamp();
	var actualTime = TimeStamp();

	var str = 'sizeId. = ' + sizeId + '<br>';
	str = str + 'TotVolume. = ' + TotVolume + '<br>';	
	str = str + 'parseFloat(TotWeight). = ' + parseFloat(TotWeight) + '<br>';
	str = str + 'NewShipLPNo. = ' + NewShipLPNo + '<br>';
	str = str + 'getFOId. = ' + getFOId + '<br>';
	str = str + 'actualDate. = ' + actualDate + '<br>';
	str = str + 'getEbizwave. = ' + getEbizwave + '<br>';
	str = str + 'actualTime. = ' + actualTime + '<br>';
	str = str + 'vCompId. = ' + vCompId + '<br>';
	str = str + 'vSiteId. = ' + vSiteId + '<br>';
	str = str + 'currentUserID. = ' + currentUserID + '<br>';
	str = str + 'vOrderIntid. = ' + vOrderIntid + '<br>';
	str = str + 'actualTime. = ' + actualTime + '<br>';



	nlapiLogExecution('ERROR', 'calculating Remaining Qty...', str);

	try
	{


		var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		//populating the fields
		customrecord.setFieldValue('custrecord_tasktype', '4'); //Ship Task
		if(sizeId!=null && sizeId!='' && sizeId!='undefined')
			customrecord.setFieldValue('custrecord_container', sizeId); //Ship Task 
		//customrecord.setFieldValue('custrecord_ebiz_wave_no', ebizWaveNo);
		customrecord.setFieldValue('custrecord_totalcube', parseFloat(TotVolume).toFixed(4));
		customrecord.setFieldValue('custrecord_total_weight', parseFloat(TotWeight).toFixed(4));
		customrecord.setFieldValue('custrecord_ship_lp_no', NewShipLPNo);
		//customrecord.setFieldValue('name', NewShipLPNo);
		customrecord.setFieldValue('name', getFOId);
		customrecord.setFieldValue('custrecordact_begin_date', actualDate);
		customrecord.setFieldValue('custrecord_act_end_date', actualDate);
		customrecord.setFieldValue('custrecord_ebiz_wave_no', getEbizwave);
		if(blnClosedFlag=='T')
			customrecord.setFieldValue('custrecord_wms_status_flag', '28');// For Closed(STATUS.OUTBOUND.PACK_COMPLETE)
		else
			customrecord.setFieldValue('custrecord_wms_status_flag', '7');// For Build ship

		//customrecord.setFieldValue('custrecord_wms_status_flag', '7');// for status B(Built onto Ship Unit)
		customrecord.setFieldValue('custrecord_actualbegintime', actualTime);
		if(vCompId!=null && vCompId!='' && vCompId!='undefined')
			customrecord.setFieldValue('custrecord_comp_id', vCompId); //For Compid
		if(vSiteId!=null && vSiteId!='' && vSiteId!='undefined')
			customrecord.setFieldValue('custrecord_site_id', vSiteId);//For SiteId 
		customrecord.setFieldValue('custrecord_taskassignedto', currentUserID);
		//Case# 201411087 starts
		if(whLocation!=null && whLocation!='' && whLocation!='undefined')
			customrecord.setFieldValue('custrecord_wms_location', whLocation);
		//Case# 201411087 ends
		//case# 201416856,201416857
		if(vOrderIntid!=null && vOrderIntid!=''&& vOrderIntid!=' '&&vOrderIntid!='undefined'&&vOrderIntid!='null')//list or rec field,dont pass undefined
			customrecord.setFieldValue('custrecord_ebiz_order_no', vOrderIntid);

		nlapiLogExecution('DEBUG', 'Submitting SHIP record', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(customrecord, true);

		nlapiLogExecution('DEBUG', 'Submitted SHIP record', 'TRN_OPENTASK');
	}
	catch(e)
	{
		nlapiLogExecution('DEBUG', 'new', e);
	}
	// To Create record in LP Master
	var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp'); 

	customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', NewShipLPNo);
	customrecord.setFieldValue('custrecord_ebiz_lpmaster_sizeid', sizeId);
	customrecord.setFieldValue('custrecord_ebiz_lpmaster_totwght', parseFloat(TotWeight).toFixed(4));
	customrecord.setFieldValue('custrecord_ebiz_lpmaster_lptype', '3');//SHIP
	customrecord.setFieldValue('name', NewShipLPNo);
	//customrecord.setFieldValue('custrecord_sku', itemname);
	customrecord.setFieldValue('custrecord_ebiz_lpmaster_totcube', parseFloat(TotVolume).toFixed(4));

	if(blnClosedFlag=='T')
		customrecord.setFieldValue('custrecord_ebiz_lpmaster_wmsstatusflag', '28');// For Closed(STATUS.OUTBOUND.PACK_COMPLETE)
	else
		customrecord.setFieldValue('custrecord_ebiz_lpmaster_wmsstatusflag', '7');// For Build ship

	//commit the record to NetSuite
	var recid = nlapiSubmitRecord(customrecord, true);

	var ruleid = 'Creation of Ship Manifest for LTL';

	var vShippingRule=getSystemRuleValue(ruleid);

	//if(vCarrierType=="PC")
	if(vShippingRule=='BuildShip')
	{	
		var PickOrdDetails=fnGetOrdDetails(getEbizwave);
		var vDiffContLps=new Array();
		var vDiffOrdNos=new Array();
		for(var p=0;p<PickOrdDetails.length;p++)
		{
			var vContLP=PickOrdDetails[p].getValue('custrecord_container_lp_no');
			var vOrdNo=PickOrdDetails[p].getValue('custrecord_ebiz_order_no');
			nlapiLogExecution('DEBUG', 'vContLP',vContLP); 
			if(vDiffContLps.indexOf(vContLP) == -1)
			{
				vDiffContLps.push(vContLP);
				vDiffOrdNos.push(vOrdNo);
			}
		}
		if(vDiffContLps != null && vDiffContLps != '')
		{
			var vStageAutoPackFlag = getAutoPackFlagforStage();
			if(vStageAutoPackFlag!=null && vStageAutoPackFlag.length>0){ 
				vStgCarrierName = vStageAutoPackFlag[0][2];
				nlapiLogExecution('DEBUG', 'vStgCarrierName(Stage)',vStgCarrierName); 
			}
			//commented becuase shipmanifest record should only be one irrespective of no.of orders and no.of cont LP's
			/*for(var k=0;k<vDiffContLps.length;k++)
			{
				var containerlpno=	vDiffContLps[k];
				var ebizorderno=	vDiffOrdNos[k];
				nlapiLogExecution('DEBUG', 'ebizorderno',ebizorderno); 
				nlapiLogExecution('DEBUG', 'containerlpno',containerlpno); 

			CreateShippingManifestRecord(ebizorderno,containerlpno,vStgCarrierName,null,null,null,null,vShippingRule);
			}*/
			var containerlpno=	vDiffContLps[0];
			var ebizorderno=	vDiffOrdNos[0];
			//CreateShippingManifestRecord(ebizorderno,containerlpno,vStgCarrierName,null,null,null,null,vShippingRule);
			//Creating shipmanifest record with ShipLP
//			var ShipManifest = nlapiCreateRecord('customrecord_ship_manifest');
//			ShipManifest.setFieldValue('name',NewShipLPNo);
//			ShipManifest.setFieldValue('custrecord_ship_contlp',NewShipLPNo);

//			nlapiSubmitRecord(ShipManifest, false, true);
		}	

	}
	nlapiLogExecution('DEBUG', 'Success', 'Success');
}

function fnGetOrdDetails(waveno)
{
	var filters = new Array();

	if(waveno != "" && waveno!=null){
		nlapiLogExecution('DEBUG','Inside waveno',waveno);
		filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', waveno));			
	}  
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3'])); 
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['7']));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_order_no');

	//columns[0].setSort();
	var searchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	return searchResults;
}


function getAutoPackFlagforStage(whlocation,vorderType){
	nlapiLogExecution('DEBUG', 'into getAutoPackFlagforStage');
	nlapiLogExecution('DEBUG', 'whlocation',whlocation);
	nlapiLogExecution('DEBUG', 'vorderType',vorderType);
	var vStgRule = new Array();
	//getStageRule(Item, vCarrier, vSite, vCompany, stgDirection,ordertype)
	vStgRule = getStageRule('', '', whlocation, '', 'OUB',vorderType);
	nlapiLogExecution('DEBUG', 'Stage Rule', vStgRule);

	nlapiLogExecution('DEBUG', 'out of getAutoPackFlagforStage');
	return vStgRule;
}