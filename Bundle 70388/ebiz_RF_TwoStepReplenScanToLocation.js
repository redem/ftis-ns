/***************************************************************************
 eBizNET Solutions Inc  
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_TwoStepReplenScanToLocation.js,v $
 *     	   $Revision: 1.1.2.2 $
 *     	   $Date: 2015/04/22 15:36:43 $
 *     	   $Author: skreddy $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_TwoStepReplenScanToLocation.js,v $
 * Revision 1.1.2.2  2015/04/22 15:36:43  skreddy
 * Case# 201412460
 * standard bundle issue fix
 *
 * Revision 1.1.2.1  2015/01/02 15:05:47  skreddy
 * Case# 201411349
 * Two step replen process
 *
 * Revision 1.4.4.5.4.7.2.12  2014/07/01 15:17:11  skavuri
 * Case# 20148710 Compatibility Issue fixed
 *
 * Revision 1.4.4.5.4.7.2.11  2014/06/17 05:46:48  grao
 * Case#: 20148907   New GUI account issue fixes
 *
 * Revision 1.4.4.5.4.7.2.10  2014/06/13 06:45:18  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.4.4.5.4.7.2.9  2014/06/06 06:52:07  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.4.4.5.4.7.2.8  2014/05/30 00:26:49  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.4.4.5.4.7.2.7  2014/02/25 15:13:52  rmukkera
 * Case # 20127339
 *
 * Revision 1.4.4.5.4.7.2.6  2013/12/02 08:59:59  schepuri
 * 20126048
 *
 * Revision 1.4.4.5.4.7.2.5  2013/09/27 15:08:00  schepuri
 * for Batch item while scanning the Cart# system is saying "Error: null"
 *
 * Revision 1.4.4.5.4.7.2.4  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.4.4.5.4.7.2.3  2013/05/22 11:39:03  spendyala
 * CASE201112/CR201113/LOG201121
 * Validating PO Status before user perform transaction.
 *
 * Revision 1.4.4.5.4.7.2.2  2013/04/17 16:04:01  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.4.4.5.4.7.2.1  2013/03/01 15:01:12  rmukkera
 * code is merged from FactoryMation production as part of Standard bundle
 *
 * Revision 1.4.4.5.4.7  2013/02/07 15:10:34  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.4.4.5.4.6  2012/12/21 15:47:53  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form 2012.2 branch.
 *
 * Revision 1.4.4.5.4.5  2012/10/03 05:09:30  grao
 * no message
 *
 * Revision 1.4.4.5.4.4  2012/09/28 06:38:47  grao
 * no message
 *
 * Revision 1.4.4.5.4.3  2012/09/27 10:56:31  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.4.4.5.4.2  2012/09/26 12:48:01  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.4.4.5.4.1  2012/09/24 10:07:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.4.4.5  2012/05/14 14:48:11  spendyala
 * CASE201112/CR201113/LOG201121
 * Validating the CARTLP issue is fixed.
 *
 * Revision 1.4.4.4  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.4.4.3  2012/03/15 12:55:15  schepuri
 * CASE201112/CR201113/LOG201121
 * cart closing functionality modified
 *
 * Revision 1.4.4.2  2012/02/23 13:48:08  schepuri
 * CASE201112/CR201113/LOG201121
 * fixed TPP issues
 *
 * Revision 1.7  2012/02/23 11:51:46  rmukkera
 * CASE201112/CR201113/LOG201121
 * focus given to the input box
 *
 * Revision 1.6  2012/02/16 10:41:51  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 *
 * Revision 1.5  2012/02/02 09:01:52  rmukkera
 * CASE201112/CR201113/LOG201121
 * modifications in code
 *
 * Revision 1.4  2011/09/22 08:29:26  snimmakayala
 * CASE201112/CR201113/LOG201121
 * The spelling in License Plate is  " LICENSE"  not "LICENCE"
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/

function TwoStepReplenScanToLocation(request, response){
	if (request.getMethod() == 'GET') {
		var getCartLP;

		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);
		getCartLP = request.getParameter('custparam_cartlpno');
		var vzone = request.getParameter('custparam_zoneno');
		var btnValue = request.getParameter('hdnclickedbtnid');
		nlapiLogExecution('DEBUG', 'getCartLP', getCartLP);

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);

		var st0,st1,st2,st3,st4,st5;
		// 20126048

		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st0 = "";
			st1 = "LP Carro";
			st2 = "ENTRAR / ESCANEAR N&#218;MERO DE CARRO";	
			st3 = "N&#250;mero de placa";
			st4 = "ENVIAR";
			st5 = "ANTERIOR";

		}
		else
		{
			st0 = "";
			st1 = "TO LOCATION";
			st2 = "ENTER/SCAN TO LOCATION";
			//st3 = "LICENSE PLATE NUMBER";
			st4 = "SEND";
			st5 = "PREV";

		}

		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['8']);
		filters[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['24']);
		filters[2] = new nlobjSearchFilter('custrecord_transport_lp', null, 'is', getCartLP);


		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actendloc','group');
		columns[1] = new nlobjSearchColumn('custrecord_sku',null,'group');
		columns[2] = new nlobjSearchColumn('custrecord_actendloc',null,'group');
		columns.push(new nlobjSearchColumn('formulanumeric',null,'group').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)"));
		columns[3].setSort(true);
		//columns[0].setSort();

		var toLocation='';
		var toLocationId='';
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
		nlapiLogExecution('DEBUG', 'searchresults', searchresults);       

		if(searchresults != null && searchresults != '')
		{
			toLocation	=  searchresults[0].getText('custrecord_actendloc',null,'group');
			toLocationId= searchresults[0].getValue('custrecord_actendloc',null,'group');
		}

		var functionkeyHtml=getFunctionkeyScript('_rf_putaway_lp'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
		html= html +"function DisableSubmitButton(buttonid) {  var forms = document.getElementsByTagName('form');  for (var i = 0; i < forms.length; i++) {  var input = forms[i].getElementsByTagName('input'); for (var y = 0; y < input.length; y++) { if (input[y].type == 'submit') {input[y].disabled = 'disabled';}}} document.getElementById('hdnclickedbtnid').value  = buttonid;alert(document.getElementById('hdnclickedbtnid').value);alert(buttonid);}";
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('entercartlp').focus();";    

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>"; 
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_putaway_lp' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> ";
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> TO LOCATION : "+ toLocation +"</td></tr>";
		html = html + "				<input type='hidden' name='hdntolocid' value=" + toLocationId + ">";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2 + "</td></tr>";

		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";

		html = html + "				<td align = 'left'><input name='entertolocation' id='entertolocation' type='text'/>";
		html = html + "				<input type='hidden' name='hdncartlp' value=" + getCartLP + ">";
		html = html + "				<input type='hidden' name='hdntolocation' value=" + toLocation + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdnvzone' value="+vzone+" >";
		html = html + "				<input type='hidden' name='hdnclickedbtnid' value="+ btnValue +" >";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		/*html = html + "				<td align = 'left'>" + st4  +" <input name='cmdSend' type='submit' id='cmdSend' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdSkip.disabled=true; return false'/>";
		html = html + "					" + st5 + " <input name='cmdPrevious' type='submit' id='cmdPrevious' value='F7' />";
		html = html + "					SKIP <input name='cmdSkip' type='submit' id='cmdSkip' value='F8' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdSend.disabled=true; return false'/>";
		html = html + "				</td>";*/
		
		html = html + "				<td align = 'left'>" + st4  +" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdSkip.disabled=true; return false'/>";
		html = html + "					" + st5 + " <input name='cmdPrevious' type='submit' value='F7' />";
		//html = html + "					SKIP <input name='cmdSkip' type='submit' value='F8' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdSend.disabled=true; return false'/>";
		html = html + "					SKIP <input name='cmdSkip' type='submit' value='F8' />";
		html = html + "				</td>";
		
		
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('entercartlp').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is not null');
		var getCartLPNo = request.getParameter('hdncartlp');
		// This variable is to hold the LP entered.
		var getTolocation = request.getParameter('hdntolocation');
		var toLocationId= request.getParameter('hdntolocid');
		nlapiLogExecution('DEBUG', 'getCartLPNo', getCartLPNo);
		nlapiLogExecution('DEBUG', 'getTolocation', getTolocation);
		nlapiLogExecution('DEBUG', 'toLocationId', toLocationId);
		var vZoneId=request.getParameter('hdnvzone');
		var eneteredLocation = request.getParameter('entertolocation');

		var POarray = new Array();



		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		
		nlapiLogExecution('ERROR', 'request.getParameter(cmdSkip) new', request.getParameter('cmdSkip'));
		
		var optedEvent = request.getParameter('hdnclickedbtnid');
		nlapiLogExecution('ERROR', 'optedEvent new', optedEvent);
		
		/*if(optedEvent==null || optedEvent  =='null' || optedEvent  == '' )
			optedEvent = request.getParameter('cmdPrevious');
		if(optedEvent==null || optedEvent  =='null' || optedEvent  == '' )
			optedEvent = request.getParameter('cmdSkip');
		if(optedEvent==null || optedEvent  =='null' || optedEvent  == '' )
			optedEvent = request.getParameter('cmdSend');*/
		
		
		if(optedEvent==null || optedEvent  =='null' || optedEvent  == '' )
		{
			nlapiLogExecution('ERROR', 'optedEvent new1', optedEvent);
			optedEvent =request.getParameter('cmdPrevious');
			nlapiLogExecution('ERROR', 'optedEvent new11', optedEvent);
		}
		if(optedEvent==null || optedEvent  =='null' || optedEvent  == '' )
		{
			nlapiLogExecution('ERROR', 'optedEvent new2', optedEvent);
			optedEvent =request.getParameter('cmdSkip');
			nlapiLogExecution('ERROR', 'optedEvent new22', optedEvent);
		}
		if(optedEvent==null || optedEvent  =='null' || optedEvent  == '' )
		{
			nlapiLogExecution('ERROR', 'optedEvent new3', optedEvent);
			optedEvent =request.getParameter('cmdSend');
			nlapiLogExecution('ERROR', 'optedEvent new33', optedEvent);
		}
		
		
		nlapiLogExecution('DEBUG', 'optedEvent', optedEvent);

		POarray["custparam_cartlpno"] =getCartLPNo;
		var getLanguage = request.getParameter('hdngetLanguage');
		var getOptedField = request.getParameter('hdnOptedField');
		POarray["custparam_option"] = getOptedField;
		POarray["custparam_screenno"] = '2StepBin';
		POarray["custparam_language"] = getLanguage;
		POarray["custparam_tolocation"] = toLocationId;
		POarray["custparam_zoneno"] = vZoneId;	
		var st4,st5,st6;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st4 = "ESTE LP CART NO EXISTE";
			st5 = "NO V&#193;LIDO LP CART";
			st6 = "Introduzca CARRO LP";

		}
		else
		{

			st4 = "THIS CART LP DOESN'T EXISTS";
			st5 = "INVALID CART LP";
			st6 = "Please Enter CART LP";//Case# 20148710

		}

		POarray["custparam_error"] = st5;
		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept PO #.
		if (optedEvent == 'F7') {
			response.sendRedirect('SUITELET', 'customscript_ebiz_twostep_putawaycartlp', 'customdeploy_ebiz_twostep_putawaycartlp', false, POarray);
			return;
		}
		else if(optedEvent == 'F8')
		{
			try
			{
				var filters = new Array();
				filters[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['8']);
				filters[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['24']);
				filters[2] = new nlobjSearchFilter('custrecord_transport_lp', null, 'is', getCartLPNo);
				if(toLocationId != null && toLocationId !='')
				{
					filters[3] = new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', toLocationId);
				}

				if(vZoneId!=null && vZoneId!=''&& vZoneId!='null')
				{
					nlapiLogExecution('ERROR','vZoneId',vZoneId);
					filters[2]=new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', vZoneId);

				}
				var columns = new Array();



				columns[0] = new nlobjSearchColumn('custrecord_skiptask');


				var toLocation='';
				var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
				nlapiLogExecution('DEBUG', 'searchresults', searchresults);       

				if(searchresults != null && searchresults != '')
				{
					nlapiLogExecution('DEBUG', 'searchresults length', searchresults.length);
					var parent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); //create new parent record on fo line.
					var otparentid = nlapiSubmitRecord(parent); 
					var otparent = nlapiLoadRecord('customrecord_ebiz_throwaway_parent', otparentid);
					for(var k=0;k<searchresults.length;k++)
					{
						var skipVal = searchresults[k].getValue('custrecord_skiptask');
						nlapiLogExecution('DEBUG', 'skipVal before Inc', skipVal);
						if(skipVal=='' || skipVal == null || skipVal == 'null')
						{
							skipVal = 0;
						}
						skipVal = parseInt(skipVal)+1;
						nlapiLogExecution('DEBUG', 'skipVal After Inc', skipVal);
						otparent.selectNewLineItem('recmachcustrecord_ebiz_ot_parent');
						otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent', 'id', searchresults[k].getId());
						otparent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_skiptask', skipVal);
						nlapiLogExecution('DEBUG', 'searchresults[k].getId()', searchresults[k].getId());
						otparent.commitLineItem('recmachcustrecord_ebiz_ot_parent');	
					}
					nlapiSubmitRecord(otparent);

					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_twostp_scanloc', 'customdeploy_ebiz_rf_twostp_scanloc', false, POarray);

					return;
				}
			}
			catch(e)
			{
				nlapiLogExecution('DEBUG', 'e new', e);
			}
		}
		else {
			if(eneteredLocation=='' || eneteredLocation==null || eneteredLocation == 'null')
			{
				POarray["custparam_error"] = "Please Enter To Location";
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'getCartLPNo is null ', getCartLPNo);
				return;
			}
			else if(eneteredLocation != null && eneteredLocation!='' && eneteredLocation != getTolocation)
			{
				POarray["custparam_error"] = "Please Enter Valid To Location";
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'getCartLPNo is null ', getCartLPNo);
				return;
			}
			else
			{
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_twostp_repenconfirm', 'customdeploy_ebiz_rf_twostp_repenconfirm', false, POarray);
				//response.sendRedirect('SUITELET', 'customscript_ebiz_hook_sl_4', 'customdeploy_ebiz_hook_sl_4', false, POarray);
				return;
			}

		}
	}
}
