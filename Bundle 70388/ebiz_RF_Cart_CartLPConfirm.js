/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/***************************************************************************
������������������������������� � ����������������������������� ��eBizNET
�����������������������                           eBizNET Solutions Inc
****************************************************************************
*
*� $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_Cart_CartLPConfirm.js,v $
*� $Revision: 1.1.2.3.4.3.2.10.2.2 $
*� $Date: 2014/11/04 12:05:50 $
*� $Author: vmandala $
*� $Name: t_eBN_2014_2_StdBundle_0_155 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_RF_Cart_CartLPConfirm.js,v $
*� Revision 1.1.2.3.4.3.2.10.2.2  2014/11/04 12:05:50  vmandala
*� case#  201410724 Stdbundle issue fixed
*�
*� Revision 1.1.2.3.4.3.2.10.2.1  2014/09/16 14:41:56  skreddy
*� case # 201410256
*�  TPP SB issue fix
*�
*� Revision 1.1.2.3.4.3.2.10  2014/06/13 08:28:23  skavuri
*� Case# 20148882 (added Focus Functionality for Textbox)
*�
*� Revision 1.1.2.3.4.3.2.9  2014/06/06 06:19:42  skavuri
*� Case# 20148749 (Refresh Functionality ) SB Issue Fixed
*�
*� Revision 1.1.2.3.4.3.2.8  2014/05/30 00:26:46  nneelam
*� case#  20148622
*� Stanadard Bundle Issue Fix.
*�
*� Revision 1.1.2.3.4.3.2.7  2014/05/22 15:41:55  sponnaganti
*� case# 20148485
*� Stnd Bundle Issue fix
*�
*� Revision 1.1.2.3.4.3.2.6  2014/05/16 13:23:49  sponnaganti
*� case# 20148345
*� standard BUndle issue fix
*�
*� Revision 1.1.2.3.4.3.2.5  2014/05/09 15:02:16  sponnaganti
*� case# 20148345
*� (Redirecting to error screen)
*�
*� Revision 1.1.2.3.4.3.2.4  2013/09/06 15:05:33  skreddy
*� Case# 20124293
*� standard bundle issue fix
*�
*� Revision 1.1.2.3.4.3.2.3  2013/09/05 16:51:44  gkalla
*� Case# 20124302
*� Not creating cart lp for auto cart creation in afosa. because of this while putaway system giving invalid lp error
*�
*� Revision 1.1.2.3.4.3.2.2  2013/06/11 14:30:40  schepuri
*� Error Code Change ERROR to DEBUG
*�
*� Revision 1.1.2.3.4.3.2.1  2013/04/17 16:04:01  skreddy
*� CASE201112/CR201113/LOG201121
*� added meta tag
*�
*� Revision 1.1.2.3.4.3  2013/02/07 15:10:34  schepuri
*� CASE201112/CR201113/LOG201121
*� disabling ENTER Button func added
*�
*� Revision 1.1.2.3.4.2  2012/09/27 10:53:53  grao
*� CASE201112/CR201113/LOG201121
*�
*� Converting multiple language with given Spanish terms
*�
*� Revision 1.1.2.3.4.1  2012/09/21 14:53:06  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multilanguage
*�
*� Revision 1.1.2.3  2012/03/16 13:56:23  spendyala
*� CASE201112/CR201113/LOG201121
*� Disable-button functionality is been added.
*�
*� Revision 1.1.2.2  2012/02/22 12:14:28  schepuri
*� CASE201112/CR201113/LOG201121
*� function Key Script code merged
*�
*� Revision 1.1  2012/02/02 08:52:54  rmukkera
*� CASE201112/CR201113/LOG201121
*�   cartlp new file
*�
*
****************************************************************************/

function CartLPConfirm(request, response)
{
	var matchFound = true;
	if (request.getMethod() == 'GET') 
	{    
		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);

		var getCartLP = request.getParameter('custparam_cartlpno');
		var getPONo = request.getParameter('custparam_poid');
		nlapiLogExecution('DEBUG', 'Into Request', getPONo);
		//var trantype = request.getParameter('custparam_trantype');
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage)
	     //Case# 201410724
	    var packcode= request.getParameter('custparam_packcode');
		
	    var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime'); 
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var whLocation = request.getParameter('custparam_whlocation');
		nlapiLogExecution('DEBUG', 'whLocation', whLocation);
		var whCompany= request.getParameter('custparam_company');
		var trantype= request.getParameter('custparam_trantype');
		nlapiLogExecution('DEBUG', 'trantype', trantype);
	    
	    
		
		var st0,st1,st2,st3;
		
		if( getLanguage == 'es_ES')
		{
		
			st0 = "";
			st1 = "&#191;QUIERES CREAR PLACA CARRO NUEVO?";
			st2 = "SI";
			st3 = "NO";
			
			
		}
		else
		{
			st0 = "";
			st1 = "DO YOU WANT TO CREATE THE NEW CART LP?";
			st2 = "YES";
			st3 = "NO";
			
		}
		
		if(getCartLP==null || getCartLP =="")
		{
			var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
			var html = "<html><head><title>" + st0 +  "</title>";
			html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
			html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
			//Case# 20148749 Refresh Functionality starts
			html = html + "var version = navigator.appVersion;";
			html = html + "document.onkeydown = function (e) {";
			html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
			html = html + "if ((version.indexOf('MSIE') != -1)) { ";
			html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
			html = html + "else {if (keycode == 116)return false;}";
			html = html + "};";
			//Case# 20148749 Refresh Functionality ends
			//html = html + " document.getElementById('cmdSend').focus();";     
			
			html = html + "function stopRKey(evt) { ";
			//html = html + "	  alert('evt');";
			html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
			html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
			html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
			html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
			html = html + "	  alert('System Processing, Please wait...');";
			html = html + "	  return false;}} ";
			html = html + "	} ";

			html = html + "	document.onkeypress = stopRKey; ";
			
			html = html + "</script>";        
			html = html +functionkeyHtml;
			html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
			html = html + "	<form name='_rf_cluster_no' method='POST'>";
			html = html + "		<table>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st1;	 
			html = html + "				</td>";
			html = html + "			</tr>"; 
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st2 + " <input name='cmdSend' id='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
			html = html + "					" + st3 + " <input name='cmdPrevious' type='submit' value='F7'/>";        
			html = html + "				<input type='hidden' name='hdnPoID' value=" + getPONo + ">";
			html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
			html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
			html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
			html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
			html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
			html = html + "				<input type='hidden' name='hdnWhLocation' value=" + whLocation + ">";
			html = html + "				<input type='hidden' name='hdnWhCompany' value=" + whCompany + ">";
			html = html + "				<input type='hidden' name='hdntrantype' value=" + trantype + ">";
			html = html + "				<input type='hidden' name='hdnpackcode' value=" + packcode + ">";//Case# 201410724
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "		 </table>";
			html = html + "	</form>";
			//Case# 20148882 (added Focus Functionality for Textbox)
			html = html + "<script type='text/javascript'>document.getElementById('cmdSend').focus();</script>";
			html = html + "</body>";
			html = html + "</html>";

			response.write(html);
		}
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var POarray =new Array();
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		
		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);
		POarray["custparam_poid"] = request.getParameter('custparam_poid');
		//POarray["custparam_error"] = st6;
		POarray["custparam_screenno"] = 'CRTSYSLP';
		POarray["custparam_actualbegindate"] = request.getParameter('hdnActualBeginDate');	
		POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
		POarray["custparam_company"] = request.getParameter('hdnWhCompany');
		POarray["custparam_option"] = request.getParameter('hdnOptedField');
		POarray["custparam_actualbegintime"] = request.getParameter('hdnActualBeginTime');
		POarray["custparam_actualbegintimeampm"] = request.getParameter('hdnActualBeginTimeAMPM');
		POarray["custparam_cartlpno"] = request.getParameter('custparam_cartlpno');
		POarray["custparam_trantype"] = request.getParameter('hdntrantype');
		//Case# 201410724
		POarray["custparam_packcode"] = request.getParameter('hdnpackcode');
		var whlocation = request.getParameter('hdnWhLocation');
		nlapiLogExecution('DEBUG', 'whlocation',whlocation);
		
		var optedEvent = request.getParameter('cmdPrevious');       

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 		
		if (optedEvent == 'F7') {
			POarray["custparam_poid"] =  request.getParameter('hdnPoID');
			//response.sendRedirect('SUITELET', 'customscript_rf_picking_container_no', 'customdeploy_rf_picking_container_no_di', false, SOarray);
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cartscan', 'customdeploy_ebiz_rf_cartscan_di', false, POarray);
		}
		else // TO create Auto generated CART LP#
		{      
			nlapiLogExecution('DEBUG', 'Entering to CartonLP ', '');
			var AutoCartLPNo;
			var AutoCartLPNo=getAutoCartLP(whlocation);
			nlapiLogExecution('DEBUG', 'AutoCartLPNo', AutoCartLPNo);
			if(AutoCartLPNo != null && AutoCartLPNo != '' && AutoCartLPNo!=0)
			{
				nlapiLogExecution('DEBUG', 'AutoCartLPNo in if', AutoCartLPNo);
				var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
				customrecord.setFieldValue('name', AutoCartLPNo);
				customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', AutoCartLPNo);
				customrecord.setFieldValue('custrecord_ebiz_lpmaster_lptype', 4);//LP type=CART
				var rec = nlapiSubmitRecord(customrecord, false, true);
			}
			//case# 20148345 starts (Redirecting to error screen)
			else
			{
				nlapiLogExecution('DEBUG', 'AutoCartLPNo else', AutoCartLPNo);
				POarray["custparam_poid"] =  request.getParameter('hdnPoID');
				POarray["custparam_error"]="SYSTEM GENERATED CART LP IS NOT DEFINED";
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				return false;
				nlapiLogExecution('DEBUG', 'AutoCartLPNo else1', AutoCartLPNo);
			}
			//case# 20148345 end
			//AutoShipLPNo='SHIP1001';
			nlapiLogExecution('DEBUG', 'AutoCartLPNo after', AutoCartLPNo);
			POarray["custparam_cartlpno"] = AutoCartLPNo;
			POarray["custparam_poid"] =  request.getParameter('hdnPoID');
			var trantype=request.getParameter('hdntrantype');
			POarray["custparam_trantype"] = trantype;
			POarray["custparam_packcode"] = request.getParameter('hdnpackcode');
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_checkinsku', 'customdeploy_ebiz_rf_cart_checkinsku_di', false, POarray);
			//response.sendRedirect('SUITELET', 'customscript_rf_shipping_cartonlp', 'customdeploy_rf_shipping_cartonlp_di', false, POarray);
			return;
		}
	}
}

/**
 * Function to get auto generated Ship LP# from lp range
 *  
 * @returns Auto generated CartLP
 * 
 */
function getAutoCartLP(whlocation)
{
	var AutoCartLPNo="1";
	nlapiLogExecution('DEBUG', 'before getmaxlpno function ', '');
	AutoCartLPNo=GetMaxLPNo('1', '4',whlocation);// for Auto generated with CART LPType
	nlapiLogExecution('DEBUG', 'after getmaxlpno function ', AutoCartLPNo);
	return AutoCartLPNo;
}

