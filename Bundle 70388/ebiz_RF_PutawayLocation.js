/***************************************************************************
 eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_PutawayLocation.js,v $
 *     	   $Revision: 1.23.2.41.4.12.2.84.2.5 $
 *     	   $Date: 2015/12/04 15:17:42 $
 *     	   $Author: skreddy $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_PutawayLocation.js,v $
 * Revision 1.23.2.41.4.12.2.84.2.5  2015/12/04 15:17:42  skreddy
 * case# 201415880
 * 2015.2 issue fix
 *
 * Revision 1.23.2.41.4.12.2.84.2.4  2015/11/27 22:11:06  skreddy
 * case 201415883
 * 2015.2 issue fix
 *
 * Revision 1.23.2.41.4.12.2.84.2.3  2015/11/19 15:25:20  grao
 * 2015.2 Issue Fixes 201414649
 *
 * Revision 1.23.2.41.4.12.2.84.2.2  2015/11/06 14:13:03  snimmakayala
 * 201415040
 *
 * Revision 1.23.2.41.4.12.2.84.2.1  2015/10/01 12:42:40  schepuri
 * case# 201414340
 *
 * Revision 1.23.2.41.4.12.2.84  2015/07/23 07:40:32  grao
 * 2015.2   issue fixes  201413499
 *
 * Revision 1.23.2.41.4.12.2.83  2015/07/23 07:18:44  grao
 * 2015.2   issue fixes  201413499
 *
 * Revision 1.23.2.41.4.12.2.81  2014/11/14 12:02:14  skavuri
 * Case# 201411030 Std Bundle Issue Fixed
 *
 * Revision 1.23.2.41.4.12.2.80  2014/09/23 14:31:22  snimmakayala
 * Case: 20149505
 * Merge FIFO dates
 *
 * Revision 1.23.2.41.4.12.2.79  2014/08/07 06:08:42  skreddy
 * case # 20149850
 * True Fabrications SB issue fix
 *
 * Revision 1.23.2.41.4.12.2.78  2014/08/05 15:26:57  nneelam
 * case#  20149815
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.23.2.41.4.12.2.77  2014/08/04 16:04:11  sponnaganti
 * Case# 20149817
 * Stnd Bundle Issue fix
 *
 * Revision 1.23.2.41.4.12.2.76  2014/07/30 15:33:43  sponnaganti
 * Case# 20149770
 * Stnd Bundle Issue fix
 *
 * Revision 1.23.2.41.4.12.2.75  2014/07/23 07:08:09  snimmakayala
 * Case: 20149505
 * FIFO merge changes
 *
 * Revision 1.23.2.41.4.12.2.74  2014/07/08 15:30:36  sponnaganti
 * case# 20149261
 * Compatibility issue fix
 *
 * Revision 1.23.2.41.4.12.2.73  2014/06/27 11:28:33  sponnaganti
 * case# 20149037
 * Compatability Test Acc issue fix
 *
 * Revision 1.23.2.41.4.12.2.72  2014/06/19 15:31:02  grao
 * Case#: 20148973ÃƒÆ’Ã‚Â¯Ãƒâ€šÃ‚Â¿Ãƒâ€šÃ‚Â½ New GUI account issue fixes
 *
 * Revision 1.23.2.41.4.12.2.71  2014/06/13 09:52:46  grao
 * Case#: 20148879 New GUI account issue fixes
 *
 * Revision 1.23.2.41.4.12.2.70  2014/06/13 06:36:45  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.23.2.41.4.12.2.69  2014/06/12 14:43:50  grao
 * Case#: 20148837  New GUI account issue fixes
 *
 * Revision 1.23.2.41.4.12.2.68  2014/06/09 15:29:57  skavuri
 * Case # 20148749 SB Issue Fixed
 *
 * Revision 1.23.2.41.4.12.2.67  2014/06/06 12:18:02  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Sonic changes
 *
 * Revision 1.23.2.41.4.12.2.66  2014/05/30 00:26:50  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.23.2.41.4.12.2.65  2014/05/14 15:37:57  skavuri
 * Case # 20148387 SB Issue Fixed
 *
 * Revision 1.23.2.41.4.12.2.64  2014/04/16 06:40:49  skreddy
 * case # 20147986
 * Dealmed sb  issue fix
 *
 * Revision 1.23.2.41.4.12.2.63  2014/03/24 14:38:17  rmukkera
 * Case # 20127751ÃƒÆ’Ã‚Â¯Ãƒâ€šÃ‚Â¿Ãƒâ€šÃ‚Â½
 *
 * Revision 1.23.2.41.4.12.2.62  2014/03/12 06:31:40  skreddy
 * case 20127624
 * Deal med SB issue fixs
 *
 * Revision 1.23.2.41.4.12.2.61  2014/02/27 15:47:52  skavuri
 * Case # 20127407 (to show the serialNo# in opentask after doing cart putaway ) issue fixed
 *
 * Revision 1.23.2.41.4.12.2.60  2014/02/19 13:30:48  sponnaganti
 * case# 20126998
 * Standard Bundle Issue (Code added from RYONET SB Acc)
 *
 * Revision 1.23.2.41.4.12.2.59  2014/02/13 15:08:26  rmukkera
 * Case # 20126900
 *
 * Revision 1.23.2.41.4.12.2.58  2014/02/06 15:32:57  nneelam
 * case#  20127075
 * std bundle issue fix
 *
 * Revision 1.23.2.41.4.12.2.57  2014/01/31 13:32:06  schepuri
 * 20126968
 * standard bundle issue
 *
 * Revision 1.23.2.41.4.12.2.56  2014/01/22 14:07:43  schepuri
 * 20126899
 * standard bundle issue fix
 *
 * Revision 1.23.2.41.4.12.2.55  2014/01/09 12:35:17  schepuri
 * 20126720
 *
 * Revision 1.23.2.41.4.12.2.54  2014/01/08 13:52:59  schepuri
 * 20126691
 *
 * Revision 1.23.2.41.4.12.2.53  2014/01/06 13:12:59  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.23.2.41.4.12.2.52  2014/01/06 08:00:52  schepuri
 * 20126617
 *
 * Revision 1.23.2.41.4.12.2.51  2013/12/05 15:22:37  skreddy
 * Case# 20125918
 * 2014.1 stnd bundle issue fix
 *
 * Revision 1.23.2.41.4.12.2.50  2013/12/02 10:29:57  snimmakayala
 * CASE#:20125764
 * MHP: PO2TO Conversion.
 *
 * Revision 1.23.2.41.4.12.2.49  2013/12/02 10:09:00  snimmakayala
 * CASE#:20125764
 * MHP: PO2TO Conversion.
 *
 * Revision 1.23.2.41.4.12.2.48  2013/11/22 14:45:25  grao
 * Case# 20125875   related issue fixes in SB 2014.1
 *
 * Revision 1.23.2.41.4.12.2.47  2013/10/18 08:51:05  schepuri
 * 20125008
 *
 * Revision 1.23.2.41.4.12.2.46  2013/10/07 13:48:35  schepuri
 * display correct message
 *
 * Revision 1.23.2.41.4.12.2.45  2013/10/03 13:02:21  rmukkera
 * Case# 20124742ÃƒÆ’Ã‚Â¯Ãƒâ€šÃ‚Â¿Ãƒâ€šÃ‚Â½
 *
 * Revision 1.23.2.41.4.12.2.44  2013/09/24 15:42:04  rmukkera
 * Case# 20124540ÃƒÆ’Ã‚Â¯Ãƒâ€šÃ‚Â¿Ãƒâ€šÃ‚Â½
 *
 * Revision 1.23.2.41.4.12.2.43  2013/09/17 06:09:01  snimmakayala
 * GSUSA PROD ISSUE
 * Case# : 201215000
 * RF Fast Picking
 *
 * Revision 1.23.2.41.4.12.2.42  2013/09/05 00:04:18  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Afosa changes
 * case#20123755
 *
 * Revision 1.23.2.41.4.12.2.41  2013/09/02 15:36:34  grao
 * Case# 20124217
 * Issue fixes related to the 20124217
 *
 * Revision 1.23.2.41.4.12.2.40  2013/09/02 08:03:29  spendyala
 * case# 20124215
 * issue fixed related to case20124215
 *
 * Revision 1.23.2.41.4.12.2.39  2013/08/22 15:00:39  rmukkera
 * Case# 20123998,20123996ÃƒÆ’Ã‚Â¯Ãƒâ€šÃ‚Â¿Ãƒâ€šÃ‚Â½,20123995ÃƒÆ’Ã‚Â¯Ãƒâ€šÃ‚Â¿Ãƒâ€šÃ‚Â½
 *
 * Revision 1.23.2.41.4.12.2.38  2013/08/20 15:34:13  rmukkera
 * Issue Fix related to 20123933
 *
 * Revision 1.23.2.41.4.12.2.37  2013/08/05 16:59:32  skreddy
 * Case# 20123720
 * issue rellated to location exception
 *
 * Revision 1.23.2.41.4.12.2.36  2013/08/05 14:17:36  gkalla
 * Case# 20123728
 * Standard bundle issue RF Confirm putaway failed
 *
 * Revision 1.23.2.41.4.12.2.35  2013/07/25 08:53:09  rmukkera
 * landed cost cr changes
 *
 * Revision 1.23.2.41.4.12.2.34  2013/07/17 06:32:46  mbpragada
 * Case# 20123469
 * ITEM RECEIPT ISSUE FIXES
 *
 * Revision 1.23.2.41.4.12.2.33  2013/07/12 16:00:39  skreddy
 * Case# 20123368
 * issue rellated to expiry date for Lot#
 *
 * Revision 1.23.2.41.4.12.2.32  2013/07/04 18:49:20  spendyala
 * CASE201112/CR201113/LOG2012392
 * Issue fixed against the case for PMM invt sync.
 * We are not checking weather binloc is active or not.
 *
 * Revision 1.23.2.41.4.12.2.31  2013/06/26 15:58:19  skreddy
 * Case#:20120633
 * issue rellated to batch no  in inventory report
 *
 * Revision 1.23.2.41.4.12.2.30  2013/06/20 20:56:23  gkalla
 * CASE201112/CR201113/LOG201121
 * To populate exeception in open task
 *
 * Revision 1.23.2.41.4.12.2.29  2013/06/19 22:51:29  gkalla
 * CASE201112/CR201113/LOG201121
 * To populate exeception in open task
 *
 * Revision 1.23.2.41.4.12.2.28  2013/06/11 16:01:25  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Stdbundle issue fixes
 *
 * Revision 1.23.2.41.4.12.2.27  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.23.2.41.4.12.2.26  2013/06/05 22:11:22  spendyala
 * CASE201112/CR201113/LOG201121
 * FIFO field updating with data stamp is removed.
 *
 * Revision 1.23.2.41.4.12.2.25  2013/06/04 11:54:23  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Country of origin changes
 *
 * Revision 1.23.2.41.4.12.2.24  2013/06/04 11:05:58  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Country of origin changes
 *
 * Revision 1.23.2.41.4.12.2.23  2013/06/04 10:54:02  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Country of origin changes
 *
 * Revision 1.23.2.41.4.12.2.22  2013/05/31 15:17:36  grao
 * CASE201112/CR201113/LOG201121
 * PMM Issues fixes
 *
 * Revision 1.23.2.41.4.12.2.21  2013/05/16 11:25:03  spendyala
 * CASE201112/CR201113/LOG201121
 * InvtRef# field of opentask record will be updated
 * to putaway task, once inbound process in completed
 *
 * Revision 1.23.2.41.4.12.2.20  2013/05/15 07:36:54  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.23.2.41.4.12.2.19  2013/05/14 14:17:28  schepuri
 * Surftech issues of serial status update
 *
 * Revision 1.23.2.41.4.12.2.18  2013/05/10 11:02:30  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.23.2.41.4.12.2.17  2013/05/09 12:47:51  spendyala
 * CASE201112/CR201113/LOG2012392
 * Line level location is considered first if it is empty then header level location is taken.
 *
 * Revision 1.23.2.41.4.12.2.16  2013/05/03 15:37:50  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fixes
 *
 * Revision 1.23.2.41.4.12.2.15  2013/05/01 12:45:02  rmukkera
 * Changes done in the code to submit the more than 3800 chars to the net suite serial numbers.
 *
 * Revision 1.23.2.41.4.12.2.14  2013/04/29 14:41:15  schepuri
 * uom conversion issues
 *
 * Revision 1.23.2.41.4.12.2.13  2013/04/17 16:04:02  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.23.2.41.4.12.2.12  2013/04/16 15:05:06  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.23.2.41.4.12.2.11  2013/04/16 12:48:56  spendyala
 * CASE201112/CR201113/LOG2012392
 * Issue fixes.
 *
 * Revision 1.23.2.41.4.12.2.10  2013/04/10 15:48:31  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.23.2.41.4.12.2.9  2013/04/04 16:00:13  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related  to Putgenqty in PO status Report
 *
 * Revision 1.23.2.41.4.12.2.8  2013/04/04 15:08:27  rmukkera
 * Issue Fix related to Tsg while TO receiving for Serial item, system allows to scan the new serial#
 *
 * Revision 1.23.2.41.4.12.2.7  2013/04/03 06:45:58  gkalla
 * CASE201112/CR201113/LOG201121
 * As part of standard bundle
 *
 * Revision 1.23.2.41.4.12.2.6  2013/04/02 14:38:18  schepuri
 * tsg sb putaway qty exception
 *
 * Revision 1.23.2.41.4.12.2.5  2013/04/01 20:59:31  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.23.2.41.4.12.2.4  2013/03/15 09:28:43  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.23.2.41.4.12.2.3  2013/03/13 13:57:16  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Time Stamp related changes.
 *
 * Revision 1.23.2.41.4.12.2.2  2013/03/01 15:01:12  rmukkera
 * code is merged from FactoryMation production as part of Standard bundle
 *
 * Revision 1.23.2.41.4.12.2.1  2013/02/26 12:52:10  rmukkera
 * code merged from boombah production bundle on 26th feb 2013
 * t_eBN_2013_1_StdBundle_1
 *
 * Revision 1.23.2.41.4.12  2013/02/07 15:10:34  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.23.2.41.4.11  2013/02/07 08:40:30  skreddy
 * CASE201112/CR201113/LOG201121
 * RF Lot auto generating FIFO enhancement
 *
 * Revision 1.23.2.41.4.10  2013/01/08 15:41:28  gkalla
 * CASE201112/CR201113/LOG201121
 * Lexjet issue for batch entry irrespective of location
 *
 * Revision 1.23.2.41.4.9  2013/01/08 14:17:47  spendyala
 * CASE201112/CR201113/LOG201121
 * Merged code form 2012.2 branch.
 *
 * Revision 1.23.2.41.4.8  2013/01/04 14:53:24  grao
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.23.2.41.4.7  2012/12/17 15:14:56  schepuri
 * CASE201112/CR201113/LOG201121
 * Reapting same lp after confirm putaway
 *
 * Revision 1.23.2.41.4.6  2012/12/12 16:02:32  gkalla
 * CASE201112/CR201113/LOG201121
 * Actual Bin management enhancement
 *
 * Revision 1.23.2.41.4.5  2012/12/11 16:24:45  grao
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.23.2.41.4.4  2012/11/01 14:55:35  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.23.2.41.4.3  2012/10/03 11:33:31  schepuri
 * CASE201112/CR201113/LOG201121
 * NSUOM Convertion
 *
 * Revision 1.23.2.41.4.2  2012/09/27 10:57:49  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.23.2.41.4.1  2012/09/21 14:57:16  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.23.2.41  2012/09/03 13:45:29  schepuri
 * CASE201112/CR201113/LOG201121
 * added date stamp
 *
 * Revision 1.23.2.40  2012/08/27 15:24:28  schepuri
 * CASE201112/CR201113/LOG201121
 * fetched location is considered with uppercase
 *
 * Revision 1.23.2.39  2012/08/24 18:13:58  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * NSUOMfield code changes
 *
 * Revision 1.23.2.38  2012/08/10 14:46:20  gkalla
 * CASE201112/CR201113/LOG201121
 * To create inventory if merged qty greater than pallet qty
 *
 * Revision 1.23.2.37  2012/08/10 14:45:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Navigation issue when F7 is pressed resolved.
 *
 * Revision 1.23.2.36  2012/08/06 06:49:03  spendyala
 * CASE201112/CR201113/LOG201121
 * Quantity exception issues is resolved.
 *
 * Revision 1.23.2.35  2012/07/31 06:54:44  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to Creating Inventory was resolved.
 *
 * Revision 1.23.2.34  2012/07/30 23:17:32  gkalla
 * CASE201112/CR201113/LOG201121
 * NSUOM issue
 *
 * Revision 1.23.2.33  2012/07/10 23:15:21  gkalla
 * CASE201112/CR201113/LOG201121
 * Alert Mails
 *
 * Revision 1.23.2.32  2012/06/15 07:13:11  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to Merge LP is fixed.
 *
 * Revision 1.23.2.31  2012/06/04 09:24:49  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.23.2.30  2012/05/31 12:49:33  mbpragada
 * CASE201112/CR201113/LOG201121
 * Dynacraft INB/OUB issue fix
 *
 * Revision 1.23.2.29  2012/05/23 14:16:17  mbpragada
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.23.2.28  2012/05/18 17:58:28  gkalla
 * CASE201112/CR201113/LOG201121
 * TO Item receipt issue fixed. Commented partial TO receipt check condition
 *
 * Revision 1.23.2.27  2012/05/16 13:22:17  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Multi UOM changes
 *
 * Revision 1.23.2.26  2012/05/11 14:50:02  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to item receipt is resolved.
 *
 * Revision 1.23.2.25  2012/05/10 15:00:34  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to clicking previous screen was resolved.
 *
 * Revision 1.23.2.24  2012/05/09 14:47:33  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to Fifo value for BatchItem while creating inv rec is fixed.
 *
 * Revision 1.23.2.23  2012/05/09 14:37:27  spendyala
 * CASE201112/CR201113/LOG201121
 * While creating Inv Record ExpireDate is not getting updated for a batch item,issue is resolved.
 *
 * Revision 1.23.2.22  2012/05/07 09:36:36  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * confirm putaway
 *
 * Revision 1.23.2.21  2012/05/04 13:54:05  gkalla
 * CASE201112/CR201113/LOG201121
 * Putaway qty exception for Batch item
 *
 * Revision 1.23.2.20  2012/04/30 15:09:07  gkalla
 * CASE201112/CR201113/LOG201121
 * To check lineno while doing Itemreceipt in transferorder
 *
 * Revision 1.23.2.19  2012/04/30 10:00:47  spendyala
 * CASE201112/CR201113/LOG201121
 * While Searching of Item in ItemMaster,
 * 'name' filter is changed to 'nameinternal'
 *
 * Revision 1.23.2.18  2012/04/13 23:12:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF CART PUTAWAY issues.
 *
 * Revision 1.23.2.17  2012/04/03 11:05:06  gkalla
 * CASE201112/CR201113/LOG201121
 * Added WMS status filter for Merge LP
 *
 * Revision 1.23.2.16  2012/04/02 06:16:19  spendyala
 * CASE201112/CR201113/LOG201121
 * set true lot in our custom records.
 *
 * Revision 1.23.2.15  2012/03/28 12:48:03  schepuri
 * CASE201112/CR201113/LOG201121
 * validating from entering another loc other than fetched location
 *
 * Revision 1.23.2.14  2012/03/21 11:09:44  schepuri
 * CASE201112/CR201113/LOG201121
 * Added movetask
 *
 * Revision 1.23.2.13  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.23.2.12  2012/03/12 13:51:22  spendyala
 * CASE201112/CR201113/LOG201121
 * Passing  Record id to the query string.
 *
 * Revision 1.23.2.11  2012/03/09 07:45:47  spendyala
 * CASE201112/CR201113/LOG201121
 * Code Merged from trunk.
 *
 * Revision 1.23.2.10  2012/03/02 01:25:46  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.23.2.9  2012/02/24 00:01:34  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * merge lp in cart putaway
 *
 * Revision 1.23.2.8  2012/02/23 23:09:35  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * merge lp
 *
 * Revision 1.23.2.7  2012/02/23 00:27:34  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * supress lp in Invtmove and lp merge based on fifodate
 *
 * Revision 1.23.2.6  2012/02/22 12:37:46  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.23.2.5  2012/02/16 14:08:11  spendyala
 * CASE201112/CR201113/LOG201121
 * While creating  inventory records, order# field is updating with purchase order#
 *
 * Revision 1.23.2.4  2012/02/13 23:21:11  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge
 *
 * Revision 1.28  2012/02/12 12:26:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Checkin MultiUOM issues
 *
 * Revision 1.27  2012/02/10 08:58:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge
 *
 * Revision 1.26  2012/01/20 19:02:57  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Confirm Putaway Issue
 *
 * Revision 1.25  2012/01/19 00:43:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Changes
 *
 * Revision 1.24  2012/01/09 13:16:10  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Replenishment
 *
 * Revision 1.23  2011/12/28 23:14:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * TO Putaway changes
 *
 * Revision 1.22  2011/12/28 13:20:51  spendyala
 * CASE201112/CR201113/LOG201121
 * resolved issues related to qty exception while entering data into transaction order line table
 *
 * Revision 1.21  2011/12/28 06:57:22  spendyala
 * CASE201112/CR201113/LOG201121
 * made changes for qty exception
 *
 * Revision 1.20  2011/12/24 15:52:57  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.19  2011/12/23 23:36:41  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.18  2011/12/23 13:32:26  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.17  2011/12/19 14:59:34  snimmakayala
 * CASE201112/CR201113/LOG201121
 * FIFO in RF Receiving.
 *
 * Revision 1.16  2011/11/17 08:55:20  spendyala
 * CASE201112/CR201113/LOG201121
 * changed the wms flag for DeleteInvtRecCreatedforCHKNTask rec in crete inventory
 *
 * Revision 1.15  2011/10/24 21:36:20  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Wrong Status updated in Inventory.
 * It should be 19.
 *
 * Revision 1.14  2011/09/29 12:04:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.13  2011/09/28 16:45:31  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.12  2011/09/28 11:37:38  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RMA Functionality New Files
 *
 * Revision 1.11  2011/09/24 19:47:31  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.9  2011/08/24 12:43:03  schepuri
 * CASE201112/CR201113/LOG201121
 * RF Putaway Location based on putseq no
 *
 * Revision 1.8  2011/08/23 09:42:59  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.7  2011/07/12 06:39:08  pattili
 * CASE201112/CR201113/LOG201121
 * 1. Changes to RF Putaway SKU and Putaway Location.
 *
 * Revision 1.6  2011/06/30 13:06:55  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.5  2011/06/06 14:10:04  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 * In Check-in, Remaining cube calculation and putaway logic.
 *
 * Revision 1.4  2011/06/03 06:29:22  pattili
 * CASE201112/CR201113/LOG201121
 * Fixed the issues.
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/

function PutawayLocation(request, response){

	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);
	if (request.getMethod() == 'GET') {
		var getRecordCount = request.getParameter('custparam_recordcount');
		nlapiLogExecution('DEBUG', 'getFetchedLocationId ', getFetchedLocation);

		var getconfirmedLPCount = request.getParameter('custparam_confirmedLpCount');
		var getlpCount = request.getParameter('custparam_lpCount');
		var TempLPNoArray = request.getParameter('custparam_lpNumbersArray');
		nlapiLogExecution('DEBUG', 'confirmedLPCount ', getconfirmedLPCount);
		nlapiLogExecution('DEBUG', 'lpCount ', getlpCount);
		nlapiLogExecution('DEBUG', 'TempLPNoArray ', TempLPNoArray);
		//	Get the LP#, Quantity, Location 
		//  from the previous screen, which is passed as a parameter	
		var getLPNo = request.getParameter('custparam_lpno');
		var getQuantity = request.getParameter('custparam_quantity');
		var getFetchedLocation = request.getParameter('custparam_beginlocation');
		var getFetchedItem = request.getParameter('custparam_item');
		var getFetchedItemText = request.getParameter('custparam_itemtext');
		var getFetchedItemDescription = request.getParameter('custparam_itemDescription');
		var getFetchedLocationId = request.getParameter('custparam_location');
		var getPONo = request.getParameter('custparam_pono');
		var getLineNo = request.getParameter('custparam_lineno');
		var EnteredLpNo = request.getParameter('custparam_enteredLPNo');
		var exceptionqtyflag = request.getParameter('custparam_exceptionQuantityflag');
		var exceptionqty = request.getParameter('custparam_exceptionquantity');
		var getActQuantity = request.getParameter('custparam_quantity');
		var getRecordId = request.getParameter('custparam_recordid');
		var getSerialNo = request.getParameter('custparam_serialno');
		var NewGenLP = request.getParameter('custparam_polineitemlp');
		var getCartLPNo = request.getParameter('custparam_cartno');
		var qtyexceptionFlag=request.getParameter('custparam_qtyexceptionflag');
		var vremqty=request.getParameter('custparam_remainingqty');
		nlapiLogExecution('DEBUG', 'vremqty',vremqty);
		nlapiLogExecution('DEBUG', 'qtyexceptionFlag',qtyexceptionFlag);

		nlapiLogExecution('DEBUG', 'getCartLPNo ', getCartLPNo);
		nlapiLogExecution('DEBUG', 'getFetchedItem ', getFetchedItem);
		nlapiLogExecution('DEBUG', 'getSerialNo ', getSerialNo);
		nlapiLogExecution('DEBUG', 'NewGenLP ', NewGenLP);

		var getOptedField = request.getParameter('custparam_option');



		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);


		var st0,st1,st2,st3,st4,st5,st6;

		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "IR A PUNTO DE"; 
			st2 = "INGRESAR / ESCANEAR UBICACI&#211;N";			
			st3 = "ART&#205;CULO";
			st4 = "PLACA";
			st5 = "CONT";
			st6 = "ANTERIOR";			

		}
		else
		{
			st0 = "";
			st1 = "GO TO LOCATION"; 
			st2 = "ENTER/SCAN LOCATION";			
			st3 = "ITEM";
			st4 = "LP";
			st5 = "CONT";
			st6 = "PREV";			

		}


		var functionkeyHtml=getFunctionkeyScript('_rf_putaway_lp'); 
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		//html = html + " document.getElementById('enterlocation').focus();";        
		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_putaway_lp' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + ": <label>" + getFetchedLocation + "</label>";
		html = html + "				<input type='hidden' name='hdnLPNo' value=" + getLPNo + "></td>";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getQuantity + "></td>";
		html = html + "				<input type='hidden' name='hdnFetchedLocation' value=" + getFetchedLocation + "></td>";
		html = html + "				<input type='hidden' name='hdnFetchedItem' value=" + getFetchedItem + "></td>";
		html = html + "				<input type='hidden' name='hdnFetchedItemDescription' value=" + getFetchedItemDescription + "></td>";
		html = html + "				<input type='hidden' name='hdnFetchedItemText' value=" + getFetchedItemText + "></td>";
		html = html + "				<input type='hidden' name='hdnFetchedLocationId' value=" + getFetchedLocationId + "></td>";
		html = html + "				<input type='hidden' name='hdnRecordCount' value=" + getRecordCount + "></td>";
		html = html + "				<input type='hidden' name='hdnconfirmedLPCount' value=" + getconfirmedLPCount + "></td>";
		html = html + "				<input type='hidden' name='hdnlpCount' value=" + getlpCount + "></td>";
		html = html + "				<input type='hidden' name='hdnlpNumbersArray' value=" + TempLPNoArray + "></td>";
		html = html + "				<input type='hidden' name='hdnRecordId' value=" + getRecordId + "></td>";
		html = html + "				<input type='hidden' name='hdnCartLPNo' value=" + getCartLPNo + ">";
		html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
		html = html + "				<input type='hidden' name='hdnLineNo' value=" + getLineNo + ">";
		html = html + "				<input type='hidden' name='hdnPONo' value=" + getPONo + ">";
		html = html + "				<input type='hidden' name='hdngetActQuantity' value=" + getActQuantity + ">";
		html = html + "				<input type='hidden' name='hdnexceptionqty' value=" + exceptionqty + ">";
		html = html + "				<input type='hidden' name='hdnexceptionqtyflag' value=" + exceptionqtyflag + ">";
		html = html + "				<input type='hidden' name='hdnenteredlp' value=" + EnteredLpNo + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdnSerialNo' value=" + getSerialNo + ">";
		html = html + "				<input type='hidden' name='hdnNewGenLP' value=" + NewGenLP + ">";
		html = html + "				<input type='hidden' name='hdnqtyexceptionFlag' value=" + qtyexceptionFlag + ">";
		html = html + "				<input type='hidden' name='hdnremQuantity' value=" + vremqty + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> " + st2;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlocation' id='enterlocation' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " : <label>" + getFetchedItemText + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st4 + " : <label>" + getLPNo + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st5 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					" + st6 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlocation').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'INTO Response');

		var getFetchedLocation = request.getParameter('custparam_beginlocation');
		nlapiLogExecution('DEBUG', 'getFetchedLocation', getFetchedLocation);
		// This variable is to hold the LP entered.
		var getLocation = request.getParameter('enterlocation');
		/* code merged from boombah to remove the spaces from the scanned location on 25th feb 2013 by Radhika */
		//RM JAN31: Modified code to remove the spaces from the Scanned/Entered location
		getLocation = getLocation.trim();
		/*upto here*/

		nlapiLogExecution('DEBUG', 'getLocation', getLocation.toUpperCase());

		var getLPNo = request.getParameter('hdnLPNo');
		nlapiLogExecution('DEBUG', 'getLPNo', getLPNo);

		var getScannedSerialNoinQtyexception1 = request.getParameter('hdnSerialNo');
		var  NewGenLP= request.getParameter('hdnNewGenLP');
		var  qtyexceptionFlag = request.getParameter('hdnqtyexceptionFlag');


		var getScannedSerialNoinQtyexception= new Array();
		nlapiLogExecution('DEBUG', 'getScannedSerialNoinQtyexception1 in array', getScannedSerialNoinQtyexception1);
		if(getScannedSerialNoinQtyexception1 != null && getScannedSerialNoinQtyexception1 != '')
			getScannedSerialNoinQtyexception = getScannedSerialNoinQtyexception1.split(',');
		nlapiLogExecution('DEBUG', 'NewGenLP inpost', NewGenLP);
		nlapiLogExecution('DEBUG', 'getScannedSerialNoinQtyexception inpost', getScannedSerialNoinQtyexception);
		/*var getActQuantity = request.getParameter('custparam_quantity');
		nlapiLogExecution('DEBUG', 'getActQuantity', getActQuantity);

		var exceptionqty=request.getParameter('custparam_exceptionquantity');
		nlapiLogExecution('DEBUG', 'exceptionqty', exceptionqty);

		var exceptionqtyflag=request.getParameter('custparam_exceptionQuantityflag');
		nlapiLogExecution('DEBUG', 'exceptionqtyflag', exceptionqtyflag);*/
//		var getActQuantity = request.getParameter('custparam_quantity');
		var getActQuantity = request.getParameter('hdngetActQuantity');
		nlapiLogExecution('DEBUG', 'getActQuantity', getActQuantity);
		//var exceptionqty=request.getParameter('custparam_exceptionquantity');
		var exceptionqty=request.getParameter('hdnexceptionqty');
		nlapiLogExecution('DEBUG', 'exceptionqty', exceptionqty);
		//var exceptionqtyflag=request.getParameter('custparam_exceptionQuantityflag');
		// Case# 20148348 starts
		//var exceptionqtyflag=request.getParameter('hdnexceptionqtyflag');
		var exceptionqtyflag=request.getParameter('hdnqtyexceptionFlag');
		var vremqty=request.getParameter('hdnremQuantity');

		// Case# 20148348 ends
		nlapiLogExecution('DEBUG', 'exceptionqtyflag', exceptionqtyflag);

		getRecordCount = request.getParameter('hdnRecordCount');
		nlapiLogExecution('DEBUG', 'getRecordCount', getRecordCount);

		getRecordId = request.getParameter('hdnRecordId');
		nlapiLogExecution('DEBUG', 'getRecordId old', getRecordId);

		var POarray = new Array();


		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;

		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);


		var st7;
		if( getLanguage == 'es_ES')
		{
			st7 = "UBICACI&#211;N NO V&#193;LIDA ";
		}
		else
		{
			st7 = "INVALID LOCATION";
		}



		var getOptedField = request.getParameter('hdnOptedField');
		nlapiLogExecution('DEBUG', 'getOptedField ', getOptedField);
		POarray["custparam_option"] = getOptedField;
		POarray["custparam_qtyexceptionflag"] = request.getParameter('hdnqtyexceptionFlag');
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2, 6]));
		//filters.push(new nlobjSearchFilter('custrecord_ebiz_lpno', null, 'is', getLPNo));

		if(getRecordId!=null && getRecordId!='')
			filters.push(new nlobjSearchFilter('internalid', null, 'is', getRecordId));

		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_lpno'));
		columns.push(new nlobjSearchColumn('custrecord_expe_qty'));
		columns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
		columns.push(new nlobjSearchColumn('custrecord_sku'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
		columns.push(new nlobjSearchColumn('custrecord_skudesc'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
		columns.push(new nlobjSearchColumn('custrecord_line_no'));
		columns.push(new nlobjSearchColumn('custrecord_sku_status'));
		columns.push(new nlobjSearchColumn('custrecord_packcode'));
		columns.push(new nlobjSearchColumn('custrecord_batch_no'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_trailer_no'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_ot_receipt_no'));
		columns.push(new nlobjSearchColumn('custrecord_wms_location'));
		columns.push(new nlobjSearchColumn('custrecord_ebizmethod_no'));


		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

		POarray["custparam_error"] = st7;
		POarray["custparam_screenno"] = '8';
		nlapiLogExecution('DEBUG', 'Screen #', POarray["custparam_screenno"]);
		//Added on 12Mar by suman.
		POarray["custparam_recordid"] = getRecordId;
		nlapiLogExecution('DEBUG', 'getRecordId new', getRecordId);
		POarray["custparam_quantity"] = getActQuantity;
		POarray["custparam_exceptionquantity"] = exceptionqty;
//		20125008
		POarray["custparam_exceptionqty"] = exceptionqty;
		POarray["custparam_exceptionQuantityflag"] = exceptionqtyflag;
		POarray["custparam_cartno"] = request.getParameter('hdnCartLPNo');
		getCartLPNo = request.getParameter('hdnCartLPNo');
		nlapiLogExecution('DEBUG', 'getCartLPNo', getCartLPNo);

		POarray["custparam_beginlocation"] = request.getParameter('custparam_beginlocation');
		POarray["custparam_itemtext"] = request.getParameter('hdnFetchedItemText');
		POarray["custparam_lpno"] = getLPNo;
		POarray["custparam_lpNumbersArray"] = request.getParameter('hdnlpNumbersArray');
		POarray["custparam_pono"] = request.getParameter('hdnPONo');
		POarray["custparam_confirmedLPCount"] = request.getParameter('hdnconfirmedLPCount');
		POarray["custparam_lpCount"] = request.getParameter('hdnlpCount');
		POarray["custparam_enteredLPNo"] = request.getParameter('hdnenteredlp');

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept PO #.

		if (sessionobj!=context.getUser()) {
			try
			{

				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}

				if (optedEvent == 'F7') {
//					response.sendRedirect('SUITELET', 'customscript_rf_putaway_lp', 'customdeploy_rf_putaway_lp_di', false, POarray);
					response.sendRedirect('SUITELET', 'customscript_rf_putaway_confirmation', 'customdeploy_rf_putaway_confirmation_di', false, POarray);
					return;
				}
				else {
					// try {
					if (getLocation.toUpperCase() != null) 
					{
						if(getFetchedLocation.toUpperCase() != getLocation.toUpperCase())
						{
							nlapiLogExecution('DEBUG', 'plz enter correct location', getLocation.toUpperCase());
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							return;
						}
						else
						{
							nlapiLogExecution('DEBUG', 'entered location is correct', getLocation.toUpperCase());
							if (searchresults != null && searchresults.length > 0) {
								nlapiLogExecution('DEBUG', 'SearchResults of given LP', 'LP is found');

								var getLPId = searchresults[0].getId();
								nlapiLogExecution('DEBUG', 'get LP Internal Id', getLPId);

								POarray["custparam_lpno"] = searchresults[0].getValue('custrecord_lpno');
								POarray["custparam_quantity"] = searchresults[0].getValue('custrecord_expe_qty');
								POarray["custparam_location"] = searchresults[0].getValue('custrecord_actbeginloc');
								POarray["custparam_item"] = searchresults[0].getValue('custrecord_sku');
								POarray["custparam_itemid"] = searchresults[0].getValue('custrecord_ebiz_sku_no');
								POarray["custparam_itemDescription"] = searchresults[0].getValue('custrecord_skudesc');
								POarray["custparam_pointernalid"] = searchresults[0].getValue('custrecord_ebiz_cntrl_no');
								POarray["custparam_polinenumber"] = searchresults[0].getValue('custrecord_line_no');
								POarray["custparam_poitemstatus"] = searchresults[0].getValue('custrecord_sku_status');
								POarray["custparam_popackcode"] = searchresults[0].getValue('custrecord_packcode');
								POarray["custparam_polotbatchno"] = searchresults[0].getValue('custrecord_batch_no');

								//POarray["custparam_itemtext"] = request.getParameter('hdnFetchedItemText');
								var whloc=searchresults[0].getValue('custrecord_wms_location');
								var methodid=searchresults[0].getValue('custrecord_ebizmethod_no');
								var vEbizTrailerNo='';
								if(searchresults[0].getValue('custrecord_ebiz_trailer_no') != null && searchresults[0].getValue('custrecord_ebiz_trailer_no') != '')
								{
									vEbizTrailerNo=searchresults[0].getValue('custrecord_ebiz_trailer_no');
								}
								var vEbizReceiptNo='';
								if(searchresults[0].getValue('custrecord_ebiz_ot_receipt_no') != null && searchresults[0].getValue('custrecord_ebiz_ot_receipt_no') != '')
								{
									vEbizReceiptNo=searchresults[0].getValue('custrecord_ebiz_ot_receipt_no');
								}
								POarray["custparam_ebiztrailer"] = vEbizTrailerNo;
								POarray["custparam_ebizreceipt"] = vEbizReceiptNo;

								nlapiLogExecution('DEBUG', 'geteBizControlNo', POarray["custparam_pointernalid"]);
								nlapiLogExecution('DEBUG', 'geteBizSKUNo', POarray["custparam_itemid"]);
								nlapiLogExecution('DEBUG', 'itemStatus', POarray["custparam_poitemstatus"]);
								nlapiLogExecution('DEBUG', 'custparam_confirmedLPCount', POarray["custparam_confirmedLPCount"]);
								nlapiLogExecution('DEBUG', 'custparam_lpCount', POarray["custparam_lpCount"]);
								nlapiLogExecution('DEBUG', 'custparam_optedEvent', POarray["custparam_optedEvent"]);
								nlapiLogExecution('DEBUG', 'custparam_ebiztrailer', POarray["custparam_ebiztrailer"]);
								nlapiLogExecution('DEBUG', 'custparam_ebizreceipt', POarray["custparam_ebizreceipt"]);

								if (getOptedField == 4) {
									POarray["custparam_location"] = getLocation.toUpperCase();
								}
								nlapiLogExecution('DEBUG', 'getLocation', getLocation.toUpperCase());
								nlapiLogExecution('DEBUG', 'whloc', whloc);
								// case# 201417281
								/*
								var columns = new Array();						
								columns.push(new nlobjSearchColumn('name'));

								var filters = new Array();
								filters.push(new nlobjSearchFilter('name', null, 'is', getLocation.toUpperCase()));
								filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));

								var BinLocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null, filters,columns);*/




								var filters = new Array();					
								filters[0] = new nlobjSearchFilter('name', null, 'is', getLocation.toUpperCase());
								if(whloc!=null && whloc!='')
									filters[1] = new nlobjSearchFilter('custrecord_ebizsitelocf',null, 'anyof',[whloc]);
								filters.push(new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'anyof', ['6','7']));
								filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));

								var columns = new Array();
								columns.push(new nlobjSearchColumn('name'));
								var BinLocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null,filters,columns);


								if (BinLocationSearch != null)
								{
									POarray["custparam_beginlocation"] = BinLocationSearch[0].getValue('name');
									nlapiLogExecution('DEBUG', 'Location Name is', POarray["custparam_beginlocation"]);
									POarray["custparam_beginlocationinternalid"] = BinLocationSearch[0].getId();
									nlapiLogExecution('DEBUG', 'Begin Location Internal Id', POarray["custparam_beginlocationinternalid"]);

									var getBeginLocationInternalId = POarray["custparam_beginlocationinternalid"];

									if (getOptedField != 4) {
										if (getLocation.toUpperCase() != POarray["custparam_beginlocation"]) {
											response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
											nlapiLogExecution('DEBUG', 'Entered Bin Location', 'Does not match with the actual bin location');
										}
									}
//									case# 201417281
									//	var LocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null, new nlobjSearchFilter('name', null, 'is', getLocation.toUpperCase()));

									var filters = new Array();					
									filters[0] = new nlobjSearchFilter('name', null, 'is', getLocation.toUpperCase());
									if(whloc!=null && whloc!='')
										filters[1] = new nlobjSearchFilter('custrecord_ebizsitelocf',null, 'anyof',[whloc]);
									filters.push(new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'anyof', ['6','7']));
									filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));

									var columns = new Array();
									columns.push(new nlobjSearchColumn('name'));
									var LocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null,filters,columns);


									if (LocationSearch!=null && LocationSearch.length != 0) {
										nlapiLogExecution('DEBUG', 'Length of Location Search', LocationSearch.length);

										for (var s = 0; s < LocationSearch.length; s++) {
											var EndLocationId = LocationSearch[s].getId();
											nlapiLogExecution('DEBUG', 'End Location Id', EndLocationId);
										}
									}
									var trantype = nlapiLookupField('transaction', POarray["custparam_pointernalid"], 'recordType');

									var qty;
									if(exceptionqtyflag=='true')
									{
										qty=exceptionqty;;
									}
									else
										qty=POarray["custparam_quantity"];

									var vputwrecid=-1;

									//created by narasimha to get contryid based on Lp. Start
									var LPNo = request.getParameter('custparam_lpno');
									nlapiLogExecution('DEBUG', 'LPNo', LPNo);
									var existcntryid;
									var context = nlapiGetContext();
									var userAccountId = context.getCompany();
									if(userAccountId=='1285441')
									{
										var lpfilters = new Array();
										lpfilters[0]=new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', LPNo);

										var lpcolumns = new Array();
										lpcolumns[0]=new nlobjSearchColumn('custrecord_ebiz_countryoforigin');

										var lpsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, lpfilters, lpcolumns);

										if(lpsearchresults!=null && lpsearchresults!='')
										{

											existcntryid=lpsearchresults[0].getValue('custrecord_ebiz_countryoforigin');
											var existcntry=lpsearchresults[0].getText('custrecord_ebiz_countryoforigin');

											nlapiLogExecution('DEBUG', 'existcntryid', existcntryid);

											POarray["custparam_countryoforigin"]=lpsearchresults[0].getText('custrecord_ebiz_countryoforigin');
											POarray["custparam_countryid"]=lpsearchresults[0].getValue('custrecord_ebiz_countryoforigin');
										}
									}

									//End.



									vputwrecid = rf_confirmputaway(POarray["custparam_pointernalid"], POarray["custparam_polinenumber"], POarray["custparam_itemid"], 
											POarray["custparam_itemDescription"], POarray["custparam_poitemstatus"], POarray["custparam_popackcode"], 
											qty, getLocation.toUpperCase(), getBeginLocationInternalId, POarray["custparam_lpno"], EndLocationId,exceptionqty, 
											getRecordId,trantype,POarray["custparam_polotbatchno"],whloc,methodid,getScannedSerialNoinQtyexception,NewGenLP,qtyexceptionFlag,existcntryid,response);
									//rf_confirmputaway(PORec.getFieldValue('custrecord_ebiz_cntrl_no'), POarray["custparam_polinenumber"], PORec.getFieldValue('custrecord_ebiz_sku_no'), PORec.getFieldValue('custrecord_skudesc'), PORec.getFieldValue('custrecord_sku_status'), PORec.getFieldValue('custrecord_packcode'), PORec.getFieldValue('custrecord_expe_qty'), getLocation, getBeginLocationInternalId, POarray["custparam_lpno"], EndLocationId, getActQuantity, getRecordId);

									nlapiLogExecution('DEBUG', 'vputwrecid', vputwrecid);
									if(vputwrecid!=null && vputwrecid!=''){
										if(vputwrecid[0]!=-1 && vputwrecid!=null && vputwrecid!='')
										{
											if(vEbizTrailerNo != null && vEbizTrailerNo != '')
											{
												if(vEbizReceiptNo != null && vEbizReceiptNo != '')
												{
													var poReceiptRec = nlapiLoadRecord('customrecord_ebiznet_trn_poreceipt', vEbizReceiptNo);
													if(poReceiptRec != null && poReceiptRec != '')
													{
														var previousrcvqty=poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_rcvqty');
														var newrcvQty= parseFloat(previousrcvqty)+parseFloat(qty);
														//var id=nlapiSubmitField('customrecord_ebiznet_trn_poreceipt', vEbizReceiptNo, 'custrecord_ebiz_poreceipt_rcvqty',newrcvQty.toString());
														//nlapiLogExecution('DEBUG','id',id);
													}	
												}	
											}	
											TrnLineUpdation(trantype, 'PUTW', POarray["custparam_pono"], 
													POarray["custparam_pointernalid"], POarray["custparam_polinenumber"], 
													POarray["custparam_itemid"], null, qty,"",
													POarray["custparam_poitemstatus"]);

											nlapiLogExecution('DEBUG', 'getCartLPNo after confirming', getCartLPNo);
											var Remqty=0;
											// below code is commented beacuse binlocation remianing cube is updating from userevent, no need to update again
											if(exceptionqtyflag=='true')
											{
												itemDimensions = getSKUCubeAndWeight(POarray["custparam_itemid"],"");
												var itemCube = itemDimensions[0];
												//Remqty=parseFloat(POarray["custparam_quantity"])-parseFloat(qty);
												//Remqty=parseFloat(getActQuantity)-parseFloat(qty);
												Remqty=vremqty;
												nlapiLogExecution('DEBUG', 'POarray["custparam_quantity"] ', POarray["custparam_quantity"] );	
												nlapiLogExecution('DEBUG', 'itemCube ', itemCube);	
												nlapiLogExecution('DEBUG', 'Remqty ', Remqty);
												var TotalItemCube = parseFloat(itemCube) * parseFloat(Remqty);
												nlapiLogExecution('DEBUG', 'Total Item Cube ', TotalItemCube );						

												var binLocationRemainingCube = GeteLocCube(getBeginLocationInternalId);
												nlapiLogExecution('DEBUG', 'binLocationRemainingCube ', binLocationRemainingCube );
												var remainingCube = parseFloat(binLocationRemainingCube) + parseFloat(TotalItemCube);
												//	UpdateLocCube(getBeginLocationInternalId, parseFloat(remainingCube));

												nlapiLogExecution('DEBUG', 'remainingCube ', remainingCube );
											}
										}
										else if(vputwrecid[0]==-1 && vputwrecid[1] == '')
										{
											nlapiLogExecution('DEBUG', 'Putaway Failed ');
											POarray["custparam_beginlocation"] = getLocation.toUpperCase();
											POarray["custparam_error"] = 'Putaway Failed';
											response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
											return;
										}
										else 
										{
											nlapiLogExecution('DEBUG', 'Putaway Failed in exception',vputwrecid[1]);
											POarray["custparam_beginlocation"] = getLocation.toUpperCase();
											POarray["custparam_error"] = vputwrecid[1];
											response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
											return;
										}
									}
									if (getRecordCount > 1) {
										var filters = new Array();
										filters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2, 6]);
										filters[1] = new nlobjSearchFilter('custrecord_transport_lp', null, 'is', getCartLPNo);

										var columns = new Array();
										columns[0] = new nlobjSearchColumn('custrecord_transport_lp');
										columns[1] = new nlobjSearchColumn('custrecord_lpno');

										var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

										if (searchresults != null && searchresults.length > 0) {
											nlapiLogExecution('DEBUG', 'SearchResults of given LP', 'LP is found');
											POarray["custparam_cartno"] = searchresults[0].getValue('custrecord_transport_lp');
											POarray["custparam_lpno"] = searchresults[0].getValue('custrecord_lpno');
											POarray["custparam_recordcount"] = searchresults.length;

											if (POarray["custparam_beginlocation"] == null) {
												response.sendRedirect('SUITELET', 'customscript_rf_putaway_sku', 'customdeploy_rf_putaway_sku_di', false, POarray);
												nlapiLogExecution('DEBUG', 'Back to Putaway SKU', 'Record count is not zero');
											}
											else {
												response.sendRedirect('SUITELET', 'customscript_rf_putaway_confirmation', 'customdeploy_rf_putaway_confirmation_di', false, POarray);
												nlapiLogExecution('DEBUG', 'Back to Putaway confirmation', 'Record count is not zero');
											}
										}
										else {
											response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
											nlapiLogExecution('DEBUG', 'Here the Search Results1 ', 'Length is null');
										}
									}						
									else {
										response.sendRedirect('SUITELET', 'customscript_rf_putaway_complete', 'customdeploy_rf_putaway_complete_di', false, POarray);
									}

									/*
									 * 	This is to delete the check-in transactions from the inventory record while putaway confirmation. 
									 */						
									DeleteInvtRecCreatedforCHKNTask(POarray["custparam_pointernalid"], POarray["custparam_lpno"]);
								}
								else
								{
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
									nlapiLogExecution('DEBUG', 'Entered Bin Location', 'Scanned bin location is wrong');
								}
							}
							else {
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								nlapiLogExecution('DEBUG', 'Here the Search Results2 ', 'Length is null');
							}
						}
					}
					else {
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('DEBUG', 'Location is not entered / scanned', getLocation.toUpperCase());
					}

					nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
				}
			}
			catch (e)  {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');

			}
		}
		else
		{
			POarray["custparam_screenno"] = '6';
			POarray["custparam_error"] = 'LOCATION ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
		}
	}
}

function rf_confirmputaway(pointid, linenum, itemid, itemdesc, itemstatus, itempackcode, quantity, binlocationid, 
		getBeginLocationInternalId, invtlp, EndLocationId, ActQuantity, RecordId,trantype,batchno,whloc,methodid,
		getScannedSerialNoinQtyexception,NewGenLP,qtyexceptionFlag,cntryid,response){
	nlapiLogExecution('DEBUG', 'inside rf_confirmputaway', 'Success');
	//Creating Item Receipt.
	var fromrecord;
	var fromid;
	var torecord;
	var qty;
	var tempserialId = "";
	//var vputwrecid=-1;
	var vputwrecid = new Array();
	vputwrecid[0] = -1;
	vputwrecid[1] = '';
	nlapiLogExecution('DEBUG', 'binlocationid is', binlocationid);
	nlapiLogExecution('DEBUG', 'Actual Quantity is', ActQuantity);
	nlapiLogExecution('DEBUG', 'binlocation internalid is', getBeginLocationInternalId);
	nlapiLogExecution('DEBUG', 'trantype', trantype);
	nlapiLogExecution('DEBUG', 'NewGenLP', NewGenLP);
	nlapiLogExecution('DEBUG', 'qtyexceptionFlag', qtyexceptionFlag);
	//To get WH Location.
	var whLocation= '';
	var vTORestrictFlag='F';
	if(trantype!='transferorder')
	{
		whLocation= nlapiLookupField('transaction',pointid,'location');
	}
	else
	{
		var vConfig=nlapiLoadConfiguration('accountingpreferences');
		if(vConfig != null && vConfig != '')
		{
			vTORestrictFlag=vConfig.getFieldValue('ITEMCOSTASTRNFRORDCOST');
		}	
		nlapiLogExecution('DEBUG', 'vTORestrictFlag Account pref', vTORestrictFlag);

		whLocation= nlapiLookupField('transferorder',pointid,'transferlocation');
	}
	nlapiLogExecution('DEBUG', 'whLocation internalid is', whLocation);
	var templocation=whLocation;
	fromrecord = trantype;
	var idl='';
	fromid = pointid; // Transform PO with ID = 26 ;
	torecord = 'itemreceipt'; // Transform a record with given id to a different record type.
	var Itype = "";
	var serialInflg="F";	
	// Get the object of the transformed record.       

	try{
		var systemRule= GetSystemRuleForPostItemReceiptby(templocation);

		if(systemRule=='LP')
		{
			idl=generateItemReceipt(fromrecord,fromid,ActQuantity,linenum,itemstatus,itemid,trantype,pointid,invtlp,systemRule,RecordId,EndLocationId,batchno,qtyexceptionFlag,templocation,getScannedSerialNoinQtyexception,response);
		}
		else
		{
			if(systemRule=='PO')
			{
				idl='-1';
			}
		}

		//
		if (idl != null || parseFloat(quantity)==0) {

			if(idl != null && idl != '' && idl != '-1')
				nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordId, 'custrecord_ebiz_nsconfirm_ref_no', idl);

			var vbatchno='';
			var filters = new Array();
			filters[0] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', pointid);
			filters[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'is', '2');
			filters[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2, 6]);
			filters[3] = new nlobjSearchFilter('internalid', null, 'is', RecordId);

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_actbeginloc');
			columns[1] = new nlobjSearchColumn('custrecord_actendloc');
			columns[2] = new nlobjSearchColumn('custrecordact_begin_date');
			columns[3] = new nlobjSearchColumn('custrecord_act_end_date');
			columns[4] = new nlobjSearchColumn('custrecord_wms_status_flag');
			columns[5] = new nlobjSearchColumn('custrecord_actualbegintime');
			columns[6] = new nlobjSearchColumn('custrecord_actualendtime');
			columns[7] = new nlobjSearchColumn('custrecord_upd_date');
			columns[8] = new nlobjSearchColumn('custrecord_recordupdatetime');
			columns[9] = new nlobjSearchColumn('custrecord_batch_no');
			columns[10] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			columns[11] = new nlobjSearchColumn('custrecord_sku_status');
			columns[12] = new nlobjSearchColumn('custrecord_packcode');
			columns[13] = new nlobjSearchColumn('custrecord_ebiz_lpno');
			columns[14] = new nlobjSearchColumn('custrecord_expe_qty');
			columns[15] = new nlobjSearchColumn('custrecord_ebiz_trailer_no');
			columns[16] = new nlobjSearchColumn('custrecord_ebiz_ot_receipt_no');

			//a Date object to be used for a random value
			var now = new Date();
			//now= now.getHours();
			//Getting time in hh:mm tt format.
			var a_p = "";
			var d = new Date();
			var curr_hour = now.getHours();
			if (curr_hour < 12) {
				a_p = "am";
			}
			else {
				a_p = "pm";
			}
			if (curr_hour == 0) {
				curr_hour = 12;
			}
			if (curr_hour > 12) {
				curr_hour = curr_hour - 12;
			}

			var curr_min = now.getMinutes();

			curr_min = curr_min + "";

			if (curr_min.length == 1) {
				curr_min = "0" + curr_min;
			}
			nlapiLogExecution("ERROR", "Time of Day ", (curr_hour + " : " + curr_min + " " + a_p));
			// case# 201414340
			var mergeputmethods = getMergeLPMethods(whloc);		
			var varMergeLP = 'F';

			if(methodid!=null && methodid!='')
				varMergeLP = isMergeLPEnabled(whloc,methodid,mergeputmethods);
			//varMergeLP = isMergeLP(whloc,methodid);

			nlapiLogExecution('ERROR', 'itemstatus', itemstatus);
			var priorityPutawayLP = priorityPutawayfixedLP(itemid,whloc,itemstatus,EndLocationId);

			// execute the  search, passing null filter and return columns

			nlapiLogExecution('DEBUG', 'varMergeLP', varMergeLP);
			nlapiLogExecution('DEBUG', 'ActQuantity', ActQuantity);
			nlapiLogExecution('DEBUG', 'batchno', batchno);
			var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
			var vEbizTrailerNo='';			
			var vEbizReceiptNo='';
			var getlotnoid="";

			var InvtRefNo="";



			if(varMergeLP=="T" || (priorityPutawayLP!=null && priorityPutawayLP!='') )
			{
				if(priorityPutawayLP!=null && priorityPutawayLP!='')				
					invtlp=priorityPutawayLP;
				var batchno = '';
				var putItemId,putItemStatus,putItemPC,putBinLoc,putLP,opentaskintid,putQty,putoldBinLoc;
				for (var i = 0; i < searchresults.length; i++)
				{
					nlapiLogExecution('DEBUG', 'test1', 'test1');
					nlapiLogExecution('DEBUG', 'searchresults.length', searchresults.length);
					var searchresult = searchresults[i];
					opentaskintid=searchresult.getId();
					nlapiLogExecution('DEBUG', 'opentaskintid', opentaskintid);
					putItemId=searchresult.getValue('custrecord_ebiz_sku_no');
					putItemStatus=searchresult.getValue('custrecord_sku_status');
					putItemPC=searchresult.getValue('custrecord_packcode');
					putBinLoc=searchresult.getValue('custrecord_actbeginloc');
					putLP=searchresult.getValue('custrecord_ebiz_lpno');
//					putQty = searchresult.getValue('custrecord_expe_qty');
					//putQty = quantity;
					batchno = searchresult.getValue('custrecord_batch_no');
					putQty = ActQuantity;//here inventory should be with eaches
					putoldBinLoc= searchresult.getValue('custrecord_actbeginloc');
					if(searchresult.getValue('custrecord_ebiz_trailer_no') != null && searchresult.getValue('custrecord_ebiz_trailer_no') != '')
					{
						vEbizTrailerNo=searchresult.getValue('custrecord_ebiz_trailer_no');
					}
					if(searchresult.getValue('custrecord_ebiz_ot_receipt_no') != null && searchresult.getValue('custrecord_ebiz_ot_receipt_no') != '')
					{
						vEbizReceiptNo=searchresult.getValue('custrecord_ebiz_ot_receipt_no');
					}

					nlapiLogExecution('DEBUG', 'End', 'END');
				}
				var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group',
				              'custitem_ebizserialin','custitem_ebiz_merge_fifodates'];
				var columns = nlapiLookupField('item', putItemId, fields);
				var ItemType = columns.recordType;					
				var batchflg = columns.custitem_ebizbatchlot;
				var itemfamId= columns.custitem_item_family;
				var itemgrpId= columns.custitem_item_group;
				var serialInflg = columns.custitem_ebizserialin;
				var mergefifodates=columns.custitem_ebiz_merge_fifodates;

				if(ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflg == "T")
				{
					nlapiLogExecution('DEBUG', 'batchno', batchno);
					if(batchno!="" && batchno!=null)
					{

						var filterspor = new Array();
						filterspor.push(new nlobjSearchFilter('name', null, 'is', batchno));
						/*if(whLocation!=null && whLocation!="")
							filterspor[1] = new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', whLocation);*/
						if(itemid!=null && itemid!="")
							filterspor.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', putItemId));

						var column=new Array();
						column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
						column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');

						var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
						if(receiptsearchresults!=null)
						{
							getlotnoid= receiptsearchresults[0].getId();
							nlapiLogExecution('DEBUG', 'getlotnoid', getlotnoid);
							//expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');
							//vfifoDate = receiptsearchresults[0].getValue('custrecord_ebizfifodate');
						}
						//nlapiLogExecution('DEBUG', 'expdate', expdate);
						//nlapiLogExecution('DEBUG', 'vfifoDate', vfifoDate);
					}
				}

				var fifodate;
				var filtersfifo = new Array();
				filtersfifo.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', putLP));
				var columnsfifo = new Array();
				columnsfifo[0] = new nlobjSearchColumn('custrecord_ebiz_inv_fifo');    
				var invttransactionforfifodate = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersfifo, columnsfifo);
				if(invttransactionforfifodate!=null && invttransactionforfifodate!='')
				{

					fifodate=invttransactionforfifodate[0].getValue('custrecord_ebiz_inv_fifo');
					nlapiLogExecution('DEBUG', 'fifodate inside', fifodate);
				}

				if(fifodate==null || fifodate=='')
				{
					fifodate = FifovalueCheck(ItemType,putItemId,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
					fifovalue=fifodate;
				}
				else
				{
					fifovalue=fifodate;
				}
				nlapiLogExecution('DEBUG', 'putItemId', putItemId);
				nlapiLogExecution('DEBUG', 'putItemStatus', putItemStatus);
				nlapiLogExecution('DEBUG', 'putItemPC', putItemPC);
				nlapiLogExecution('DEBUG', 'putBinLoc', putBinLoc);
				nlapiLogExecution('DEBUG', 'putLP', putLP);
				nlapiLogExecution('DEBUG', 'fifodate', fifodate);
				nlapiLogExecution('DEBUG', 'getlotnoid from new search', getlotnoid);
				var varPaltQty = getMaxUOMQty(putItemId,putItemPC);

				nlapiLogExecution('DEBUG', 'varPaltQty', varPaltQty);


				nlapiLogExecution('DEBUG', 'Checking for merge the item');
				var filtersinvt = new Array();

				filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', putItemId));
				filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', putItemStatus));
				filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', putBinLoc));
				filtersinvt.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', '19'));
				if((mergefifodates!='T') && (fifodate!=null && fifodate!=''))
					filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_fifo', null, 'on', fifodate));

				if(putItemPC!=null && putItemPC!='')
					filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_packcode', null, 'anyof', putItemPC));							

				if(getlotnoid!=null && getlotnoid!='')
				{
					nlapiLogExecution('DEBUG', 'getlotnoid inside', getlotnoid);
					filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'is', getlotnoid));
				}

				if(varPaltQty!=null && varPaltQty!='')
					filtersinvt.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'lessthan', varPaltQty));							

				var columnsinvt = new Array();
				columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');    
				columnsinvt[0].setSort();

				var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);
				nlapiLogExecution('DEBUG', 'invtsearchresults', invtsearchresults);
				if(invtsearchresults != null && invtsearchresults != ''  && invtsearchresults.length > 0)
				{
					nlapiLogExecution('DEBUG', 'invtsearchresults.length', invtsearchresults.length);
					var newputqty=putQty;
					var BoolInvMerged=false;
					for (var i = 0; i < invtsearchresults.length; i++) 
					{
						var qoh=invtsearchresults[i].getValue('custrecord_ebiz_qoh');

						nlapiLogExecution('DEBUG', 'exiting qoh', qoh);
						nlapiLogExecution('DEBUG', 'putQty', putQty);
						nlapiLogExecution('DEBUG', 'varPaltQty', varPaltQty);

						if(newputqty>0 && (parseFloat(putQty)+parseFloat(qoh))<=parseFloat(varPaltQty))
						{
							nlapiLogExecution('DEBUG', 'Inventory Record ID', invtsearchresults[i].getId());
							BoolInvMerged=true;
							nlapiLogExecution('DEBUG', 'BoolInvMerged', BoolInvMerged);
							var invttransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invtsearchresults[i].getId());											

							var varExistQOHQty = invttransaction.getFieldValue('custrecord_ebiz_qoh');
							var varExistInvQty = invttransaction.getFieldValue('custrecord_ebiz_inv_qty');
							var varExistLP=invttransaction.getFieldValue('custrecord_ebiz_inv_lp');

							nlapiLogExecution('DEBUG', 'varExistQOHQty', varExistQOHQty);
							nlapiLogExecution('DEBUG', 'varExistInvQty', varExistInvQty);

							var varupdatedQOHqty = parseFloat(varExistQOHQty) + parseFloat(putQty);
							var varupdatedInvqty = parseFloat(varExistInvQty) + parseFloat(putQty);


							putLP=varExistLP;

							if(parseFloat(varExistQOHQty)<parseFloat(putQty))
							{
								if(fifodate!=null && fifodate!='')
									invttransaction.setFieldValue('custrecord_ebiz_inv_fifo', fifodate);
							}

							invttransaction.setFieldValue('custrecord_ebiz_qoh', parseFloat(varupdatedQOHqty).toFixed(4));
							invttransaction.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(varupdatedInvqty).toFixed(4));
							invttransaction.setFieldValue('custrecord_ebiz_callinv', 'N');    

							var vinvrecid=nlapiSubmitRecord(invttransaction,false, true);
							InvtRefNo=invtsearchresults[i].getId();
							nlapiLogExecution('DEBUG', 'nlapiSubmitRecord', 'Record Submitted');
							nlapiLogExecution('DEBUG', 'Inventory is merged to the LP '+varExistLP);
							newputqty=0;

							if(vinvrecid != null && vinvrecid != '' && vinvrecid != '-1')
								nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordId, 'custrecord_invref_no', vinvrecid);

						}
					}
					if(BoolInvMerged==false)
					{
						var accountNumber = "";

						//Creating Inventory Record.
						var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');

						nlapiLogExecution('DEBUG', 'Creating INVT  Record if BoolInvMerged is false in merge lp true', 'INVT');

						invtRec.setFieldValue('name', idl);
						invtRec.setFieldValue('custrecord_ebiz_inv_binloc', EndLocationId);
						invtRec.setFieldValue('custrecord_ebiz_inv_lp', invtlp);
						invtRec.setFieldValue('custrecord_ebiz_inv_sku', itemid);
						invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
						if(itempackcode!=null&&itempackcode!="")
							invtRec.setFieldValue('custrecord_ebiz_inv_packcode', itempackcode);
						invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(ActQuantity).toFixed(4));
						//invtRec.setFieldValue('custrecord_ebiz_inv_lot', batchno);
						//invtRec.setFieldValue('custrecord_ebiz_inv_recorddate',(parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
						//invtRec.setFieldValue('custrecord_ebiz_inv_rectime',((curr_hour) + ":" + (curr_min) + " " + a_p));
						invtRec.setFieldValue('custrecord_inv_ebizsku_no', itemid);
						invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(ActQuantity).toFixed(4));
						invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemdesc);
						invtRec.setFieldValue('custrecord_ebiz_inv_loc', whLocation);
						invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');//WH Location.
						invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);
						invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
						invtRec.setFieldValue('custrecord_wms_inv_status_flag','19');//19=FLAG.INVENTORY.STORAGE
						invtRec.setFieldValue('custrecord_invttasktype', '2');
						invtRec.setFieldValue('custrecord_ebiz_transaction_no', pointid);
						if(cntryid!=null && cntryid!='')
							invtRec.setFieldValue('custrecord_ebiz_countryoforigin', cntryid);

						if(getuomlevel!=null && getuomlevel!='')
							invtRec.setFieldText('custrecord_ebiz_uomlvl', getuomlevel);

						/*var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group','custitem_ebizserialin'];
						var columns = nlapiLookupField('item', itemid, fields);
						var ItemType = columns.recordType;					
						var batchflg = columns.custitem_ebizbatchlot;
						var itemfamId= columns.custitem_item_family;
						var itemgrpId= columns.custitem_item_group;
						var serialInflg = columns.custitem_ebizserialin;*/
						//var getlotnoid="";
						var expdate='';
						nlapiLogExecution('DEBUG', 'ItemType', ItemType);
						nlapiLogExecution('DEBUG', 'batchflg', batchflg);
						nlapiLogExecution('DEBUG', 'batchno', batchno);
						/*if(ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflg == "T")
						{
							nlapiLogExecution('DEBUG', 'batchno', batchno);
							if(batchno!="" && batchno!=null)
							{

								var filterspor = new Array();
								filterspor.push(new nlobjSearchFilter('name', null, 'is', batchno));
								if(whLocation!=null && whLocation!="")
									filterspor[1] = new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', whLocation);
								if(itemid!=null && itemid!="")
									filterspor.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemid));

								var column=new Array();
								column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
								column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');

								var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
								if(receiptsearchresults!=null)
								{
									getlotnoid= receiptsearchresults[0].getId();
									nlapiLogExecution('DEBUG', 'getlotnoid', getlotnoid);
									expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');
									vfifoDate = receiptsearchresults[0].getValue('custrecord_ebizfifodate');
								}
								nlapiLogExecution('DEBUG', 'expdate', expdate);
								nlapiLogExecution('DEBUG', 'vfifoDate', vfifoDate);
							}
						}*/
						if(getlotnoid!=null && getlotnoid!='')
						{
							var fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
							invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
						}
						if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem"|| batchflg == "T")
//							if (ItemType == "lotnumberedinventoryitem" || batchflg == "T")
						{ 										
							try
							{
								//Checking FIFO Policy.					
								if (getlotnoid != "") {
									if(vfifoDate == null || vfifoDate =='')
									{
										fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
										nlapiLogExecution('DEBUG','FIFO Value Check.',fifovalue);
									}
									else
									{
										fifovalue=vfifoDate;
									}
									invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
									invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
									invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
								}
							}
							catch(exp)
							{
								nlapiLogExecution('DEBUG','Exception in FIFO Value Check.',exp);
							}
						}
						if(fifodate==null || fifodate=='')
						{
							fifodate = FifovalueCheck(ItemType,putItemId,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
							fifovalue=fifodate;
						}
						else
						{
							fifovalue=fifodate;
						}

						if(fifodate != null && fifodate != '')
							invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifodate);


						nlapiLogExecution('DEBUG', 'Before Submitting invtrecid', 'INVTRECORDS');

						var invtrecid = nlapiSubmitRecord(invtRec, false, true);
						InvtRefNo=invtrecid;
						nlapiLogExecution('DEBUG', 'After Submitting invtrecid', invtrecid);
						if (invtrecid != null) {
							nlapiLogExecution('DEBUG', 'Cust INVT Rec Creation Succes with ID ', invtrecid);
						}
						else {
							nlapiLogExecution('DEBUG', 'Cust INVT Rec Creation Fail ', 'Fail');
						}

						//Added for updation of Cube information by ramana on 10th june 2011

						nlapiLogExecution('DEBUG', 'OLD BIN LOCATION DETAILS ', putoldBinLoc);
						nlapiLogExecution('DEBUG', 'NEW BIN LOCATION DETAILS ', putBinLoc);
						nlapiLogExecution('DEBUG', 'ITEMID', putItemId);
						nlapiLogExecution('DEBUG', 'LP', putLP);

						if(invtrecid != null && invtrecid != '' && invtrecid != '-1')
							nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordId, 'custrecord_invref_no', invtrecid);
					}
					//Added for updation of Cube information by ramana on 10th june 2011

					nlapiLogExecution('DEBUG', 'OLD BIN LOCATION DETAILS ', putoldBinLoc);
					nlapiLogExecution('DEBUG', 'NEW BIN LOCATION DETAILS ', putBinLoc);
					nlapiLogExecution('DEBUG', 'ITEMID', putItemId);
					nlapiLogExecution('DEBUG', 'LP', putLP);

					if(putBinLoc!=putoldBinLoc)
					{   
						/*
								//For the OLD Location Updation Purpose.    											 											
						 */

						var arrDims = getSKUCubeAndWeight(putItemId, 1);
						var itemCube = 0;
						if (arrDims["BaseUOMItemCube"] != "" && (!isNaN(arrDims["BaseUOMItemCube"]))) 
						{
							var uomqty = ((parseFloat(putQty))/(parseFloat(arrDims["BaseUOMQty"])));			
							itemCube = (parseFloat(uomqty) * parseFloat(arrDims["BaseUOMItemCube"]));
							nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemCube', itemCube);
						} 
						if(putoldBinLoc != "" && putoldBinLoc != null)
						{
							var vOldRemainingCube = GeteLocCube(putoldBinLoc);
						}
						nlapiLogExecution('DEBUG', 'vOldRemainingCube', vOldRemainingCube);

						var vTotalCubeValue = parseFloat(itemCube)+ parseFloat(vOldRemainingCube);
						nlapiLogExecution('DEBUG', 'vOldLocationRemainingCube', vTotalCubeValue);
						if(putoldBinLoc != "" && putoldBinLoc != null)
						{
							var retValue =  UpdateLocCube(putoldBinLoc,vTotalCubeValue);
						}
						//upto to here old Location Updation purpose

						//For the New Location updation

						var vNewLocationRemainingCube = GeteLocCube(putBinLoc);   

						nlapiLogExecution('DEBUG', 'vNewLocationRemainingCube', vNewLocationRemainingCube);

						//var vTotalNewLocation =  parseFloat(vartotalCube)- parseFloat(vNewLocationRemainingCube);
						var vTotalNewLocationCube =  parseFloat(vNewLocationRemainingCube)- parseFloat(itemCube);

						nlapiLogExecution('DEBUG', 'vTotalNewLocation', vTotalNewLocationCube);

						if(vTotalNewLocationCube<0)
						{
							nlapiLogExecution('DEBUG', 'inside PO Overage Validation', 'inside loop');					  
							var form = nlapiCreateForm('Confirm Putaway');
							nlapiLogExecution('DEBUG', 'Form Called', 'form');					  
							var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);		  
							msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'DEBUG', 'Qty exceeds location capacity', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");		    		   
							nlapiLogExecution('DEBUG', 'message', msg);					  
							response.writePage(form);                  						
							return;			
						}
						else
						{
							var retValue1 = UpdateLocCube(putBinLoc,vTotalNewLocationCube);
						}
						//upto to here new location

					}						 
					var currentUserID = getCurrentUser();
					nlapiLogExecution('DEBUG', 'currentUserID', currentUserID);
					for (var i = 0; i < searchresults.length; i++) {
						vbatchno=searchresults[i].getValue('custrecord_batch_no');
						var searchresult = searchresults[i];
						var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', searchresult.getId());

						nlapiLogExecution('DEBUG', 'STATUS before', searchresult.getValue('custrecord_wms_status_flag'));

						if (searchresult.getValue('custrecord_wms_status_flag') == 6) {
							transaction.setFieldValue('custrecord_actbeginloc', getBeginLocationInternalId);// Give location Id.
							transaction.setFieldValue('custrecordact_begin_date', DateStamp());
							//transaction.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
						}

						transaction.setFieldValue('custrecord_actualendtime', TimeStamp());
						transaction.setFieldValue('custrecord_recordupdatetime', TimeStamp());
						transaction.setFieldValue('custrecord_upd_date', DateStamp());
						transaction.setFieldValue('custrecord_actendloc', EndLocationId);// Give location Id.
						transaction.setFieldValue('custrecord_act_end_date', DateStamp());
						transaction.setFieldValue('custrecord_wms_status_flag', 3);
						transaction.setFieldValue('custrecord_act_qty', parseFloat(ActQuantity).toFixed(4));
						transaction.setFieldValue('custrecord_taskassignedto', currentUserID);
						nlapiLogExecution("ERROR","InvtRefNo",InvtRefNo);
						transaction.setFieldValue('custrecord_invref_no', InvtRefNo);
						if(idl != null && idl != '' && idl != '-1')
							transaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', idl);

						transaction.setFieldValue('custrecord_lpno', putLP);
						vputwrecid[0]=nlapiSubmitRecord(transaction, false, true);
						vputwrecid[1]='';
						/*if(systemRule=='LP')
						{
							MoveTaskRecord(vputwrecid[0]);
						}*/
					} 

					//MoveTaskRecord(opentaskintid);
					if(vEbizTrailerNo != null && vEbizTrailerNo != '')
					{
						if(vEbizReceiptNo != null && vEbizReceiptNo != '')
						{
							nlapiLogExecution('DEBUG', 'vEbizReceiptNo', vEbizReceiptNo);
							nlapiLogExecution('DEBUG', 'vEbizTrailerNo', vEbizTrailerNo);
							var poReceiptRec = nlapiLoadRecord('customrecord_ebiznet_trn_poreceipt', vEbizReceiptNo);

							if(poReceiptRec != null && poReceiptRec != '')
							{
								var previousrcvqty=0;
								if(poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_rcvqty') != null && poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_rcvqty') != '')
								{
									previousrcvqty=poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_rcvqty');
								}	
								var newrcvQty= parseFloat(previousrcvqty)+parseFloat(putQty);

								nlapiLogExecution('DEBUG', 'previousrcvqty', previousrcvqty);
								nlapiLogExecution('DEBUG', 'qty', qty);
								nlapiLogExecution('DEBUG', 'newrcvQty', newrcvQty);

								var id=nlapiSubmitField('customrecord_ebiznet_trn_poreceipt', vEbizReceiptNo, 'custrecord_ebiz_poreceipt_rcvqty',newrcvQty.toString());
								nlapiLogExecution('DEBUG','id',id);
							}	
						}	
					}
				}
				else
				{
					var accountNumber = "";

					//Creating Inventory Record.
					var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');

					nlapiLogExecution('DEBUG', 'Creating INVT  Record in mergelp if invserach result is null', 'INVT');

					invtRec.setFieldValue('name', idl);
					invtRec.setFieldValue('custrecord_ebiz_inv_binloc', EndLocationId);
					invtRec.setFieldValue('custrecord_ebiz_inv_lp', invtlp);
					invtRec.setFieldValue('custrecord_ebiz_inv_sku', itemid);
					invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
					if(itempackcode!=null&&itempackcode!="")
						invtRec.setFieldValue('custrecord_ebiz_inv_packcode', itempackcode);
					invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(ActQuantity).toFixed(4));
					//invtRec.setFieldValue('custrecord_ebiz_inv_lot', batchno);
					//invtRec.setFieldValue('custrecord_ebiz_inv_recorddate',(parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
					//invtRec.setFieldValue('custrecord_ebiz_inv_rectime',((curr_hour) + ":" + (curr_min) + " " + a_p));
					invtRec.setFieldValue('custrecord_inv_ebizsku_no', itemid);
					nlapiLogExecution('DEBUG','parseFloat(ActQuantity).toFixed(4)',parseFloat(ActQuantity).toFixed(4));
					invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(ActQuantity).toFixed(4));
					invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemdesc);
					invtRec.setFieldValue('custrecord_ebiz_inv_loc', whLocation);
					invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');//WH Location.
					invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);
					invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
					invtRec.setFieldValue('custrecord_wms_inv_status_flag','19');//19=FLAG.INVENTORY.STORAGE
					invtRec.setFieldValue('custrecord_invttasktype', '2');
					invtRec.setFieldValue('custrecord_ebiz_transaction_no', pointid);
					invtRec.setFieldValue('custrecord_ebiz_countryoforigin', cntryid);
					if(getuomlevel!=null && getuomlevel!='')
						invtRec.setFieldText('custrecord_ebiz_uomlvl', getuomlevel);

					var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group','custitem_ebizserialin'];
					var columns = nlapiLookupField('item', itemid, fields);
					var ItemType = columns.recordType;					
					var batchflg = columns.custitem_ebizbatchlot;
					var itemfamId= columns.custitem_item_family;
					var itemgrpId= columns.custitem_item_group;
					var serialInflg = columns.custitem_ebizserialin;
					var getlotnoid="";
					var expdate='';
					var vfifoDate='';
					nlapiLogExecution('DEBUG', 'ItemType', ItemType);
					nlapiLogExecution('DEBUG', 'batchflg', batchflg);
					nlapiLogExecution('DEBUG', 'batchno', batchno);
					if(ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflg == "T")
					{
						nlapiLogExecution('DEBUG', 'batchno', batchno);
						if(batchno!="" && batchno!=null)
						{

							var filterspor = new Array();
							filterspor.push(new nlobjSearchFilter('name', null, 'is', batchno));
							/*if(whLocation!=null && whLocation!="")
								filterspor.psuh(new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', whLocation));*/
							if(itemid!=null && itemid!="")
								filterspor.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemid));

							var column=new Array();
							column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
							column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');

							var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
							if(receiptsearchresults!=null)
							{
								getlotnoid= receiptsearchresults[0].getId();
								nlapiLogExecution('DEBUG', 'getlotnoid', getlotnoid);
								expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');
								vfifoDate=receiptsearchresults[0].getValue('custrecord_ebizfifodate');
							}
							nlapiLogExecution('DEBUG', 'expdate', expdate);
							nlapiLogExecution('DEBUG', 'vfifoDate', vfifoDate);
						}
					}
					if(vfifoDate==null || vfifoDate=='')
					{
						vfifoDate = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
						//invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
					}

					if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem"|| batchflg == "T")
//						if (ItemType == "lotnumberedinventoryitem" || batchflg == "T")
					{ 										
						try
						{
							//Checking FIFO Policy.					
							if (getlotnoid != "") {
								if(vfifoDate ==null ||vfifoDate=='')
								{
									vfifoDate = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
									fifovalue = vfifoDate;
									nlapiLogExecution('DEBUG','FIFO Value Check.',fifovalue);
								}
								else
								{
									fifovalue = vfifoDate;
								}
								nlapiLogExecution('DEBUG','fifovalue.',fifovalue);
								invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
								invtRec.setFieldValue('custrecord_ebiz_inv_fifo', vfifoDate);
								invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
							}
							else
							{
								invtRec.setFieldValue('custrecord_ebiz_inv_fifo', vfifoDate);
							}
						}
						catch(exp)
						{
							nlapiLogExecution('DEBUG','Exception in FIFO Value Check.',exp);
						}
					}
					else					
						if(vfifoDate != null && vfifoDate != '')
							invtRec.setFieldValue('custrecord_ebiz_inv_fifo', vfifoDate);

					nlapiLogExecution('DEBUG', 'Before Submitting invtrecid', 'INVTRECORDS');

					var invtrecid = nlapiSubmitRecord(invtRec, false, true);
					InvtRefNo=invtrecid;
					nlapiLogExecution('DEBUG', 'After Submitting invtrecid', invtrecid);
					if (invtrecid != null) {
						nlapiLogExecution('DEBUG', 'Cust INVT Rec Creation in merge lp true and inv search rwsult is null Succes with ID ', invtrecid);
					}
					else {
						nlapiLogExecution('DEBUG', 'Cust INVT Rec Creation Fail ', 'Fail');
					}

					if(invtrecid != null && invtrecid != '' && invtrecid != '-1')
						nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordId, 'custrecord_invref_no', invtrecid);

					//Added for updation of Cube information by ramana on 10th june 2011

					nlapiLogExecution('DEBUG', 'OLD BIN LOCATION DETAILS ', putoldBinLoc);
					nlapiLogExecution('DEBUG', 'NEW BIN LOCATION DETAILS ', putBinLoc);
					nlapiLogExecution('DEBUG', 'ITEMID', putItemId);
					nlapiLogExecution('DEBUG', 'LP', putLP);

					if(putBinLoc!=putoldBinLoc)
					{   
						/*
								//For the OLD Location Updation Purpose.    											 											
						 */

						var arrDims = getSKUCubeAndWeight(putItemId, 1);
						var itemCube = 0;
						if (arrDims["BaseUOMItemCube"] != "" && (!isNaN(arrDims["BaseUOMItemCube"]))) 
						{
							var uomqty = ((parseFloat(putQty))/(parseFloat(arrDims["BaseUOMQty"])));			
							itemCube = (parseFloat(uomqty) * parseFloat(arrDims["BaseUOMItemCube"]));
							nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemCube', itemCube);
						} 
						if(putoldBinLoc != "" && putoldBinLoc != null)
						{
							var vOldRemainingCube = GeteLocCube(putoldBinLoc);
						}
						nlapiLogExecution('DEBUG', 'vOldRemainingCube', vOldRemainingCube);

						var vTotalCubeValue = parseFloat(itemCube)+ parseFloat(vOldRemainingCube);
						nlapiLogExecution('DEBUG', 'vOldLocationRemainingCube', vTotalCubeValue);
						if(putoldBinLoc != "" && putoldBinLoc != null)
						{
							var retValue =  UpdateLocCube(putoldBinLoc,vTotalCubeValue);
						}
						//upto to here old Location Updation purpose

						//For the New Location updation

						var vNewLocationRemainingCube = GeteLocCube(putBinLoc);   

						nlapiLogExecution('DEBUG', 'vNewLocationRemainingCube', vNewLocationRemainingCube);

						//var vTotalNewLocation =  parseFloat(vartotalCube)- parseFloat(vNewLocationRemainingCube);
						var vTotalNewLocationCube =  parseFloat(vNewLocationRemainingCube)- parseFloat(itemCube);

						nlapiLogExecution('DEBUG', 'vTotalNewLocation', vTotalNewLocationCube);

						if(vTotalNewLocationCube<0)
						{
							nlapiLogExecution('DEBUG', 'inside PO Overage Validation', 'inside loop');					  
							var form = nlapiCreateForm('Confirm Putaway');
							nlapiLogExecution('DEBUG', 'Form Called', 'form');					  
							var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);		  
							msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'DEBUG', 'Qty exceeds location capacity', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");		    		   
							nlapiLogExecution('DEBUG', 'message', msg);					  
							response.writePage(form);                  						
							return;			
						}
						else
						{
							var retValue1 = UpdateLocCube(putBinLoc,vTotalNewLocationCube);
						}
						//upto to here new location

					}                     
					//upto to here on 10th june 2011

					for (var i = 0; i < searchresults.length; i++) {
						vbatchno=searchresults[i].getValue('custrecord_batch_no');
						var searchresult = searchresults[i];
						var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', searchresult.getId());

						nlapiLogExecution('DEBUG', 'STATUS before', searchresult.getValue('custrecord_wms_status_flag'));

						if (searchresult.getValue('custrecord_wms_status_flag') == 6) {
							transaction.setFieldValue('custrecord_actbeginloc', getBeginLocationInternalId);// Give location Id.
							transaction.setFieldValue('custrecordact_begin_date', DateStamp());
							//transaction.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
						}

						transaction.setFieldValue('custrecord_actualendtime', TimeStamp());
						transaction.setFieldValue('custrecord_recordupdatetime', TimeStamp());
						transaction.setFieldValue('custrecord_upd_date', DateStamp());
						transaction.setFieldValue('custrecord_actendloc', EndLocationId);// Give location Id.
						transaction.setFieldValue('custrecord_act_end_date', DateStamp());
						transaction.setFieldValue('custrecord_wms_status_flag', 3);
						transaction.setFieldValue('custrecord_act_qty', parseFloat(ActQuantity).toFixed(4));
						nlapiLogExecution("ERROR","InvtRefNo",InvtRefNo);
						transaction.setFieldValue('custrecord_invref_no', InvtRefNo);
						transaction.setFieldValue('custrecord_lpno', putLP);//

						if(idl != null && idl != '' && idl != '-1')
							transaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', idl);
						//vputwrecid=nlapiSubmitRecord(transaction, false, true);
						//MoveTaskRecord(vputwrecid);
						vputwrecid[0]=nlapiSubmitRecord(transaction, false, true);
						vputwrecid[1]='';
						/*if(systemRule=='LP')
						{
							MoveTaskRecord(vputwrecid[0]);
						}*/
					} 

					//MoveTaskRecord(opentaskintid);
					if(vEbizTrailerNo != null && vEbizTrailerNo != '')
					{
						if(vEbizReceiptNo != null && vEbizReceiptNo != '')
						{
							nlapiLogExecution('DEBUG', 'vEbizReceiptNo', vEbizReceiptNo);
							nlapiLogExecution('DEBUG', 'vEbizTrailerNo', vEbizTrailerNo);
							var poReceiptRec = nlapiLoadRecord('customrecord_ebiznet_trn_poreceipt', vEbizReceiptNo);

							if(poReceiptRec != null && poReceiptRec != '')
							{
								var previousrcvqty=0;
								if(poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_rcvqty') != null && poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_rcvqty') != '')
								{
									previousrcvqty=poReceiptRec.getFieldValue('custrecord_ebiz_poreceipt_rcvqty');
								}	
								var newrcvQty= parseFloat(previousrcvqty)+parseFloat(putQty);

								nlapiLogExecution('DEBUG', 'previousrcvqty', previousrcvqty);
								nlapiLogExecution('DEBUG', 'qty', qty);
								nlapiLogExecution('DEBUG', 'newrcvQty', newrcvQty);

								var id=nlapiSubmitField('customrecord_ebiznet_trn_poreceipt', vEbizReceiptNo, 'custrecord_ebiz_poreceipt_rcvqty',newrcvQty.toString());
								nlapiLogExecution('DEBUG','id',id);
							}	
						}	
					}
				}
				//serial Entry status updation to S
				if (ItemType == "serializedinventoryitem"|| ItemType == "serializedassemblyitem" || serialInflg == "T") {
					try {						

						var filters = new Array();
						if(trantype=='returnauthorization')
						{
							nlapiLogExecution('ERROR', 'returnauthorization');
							filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
							filters[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
						}
						else if(trantype=='transferorder')
						{
							nlapiLogExecution('ERROR', 'transferorder');
							filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['8']);
							filters[2] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);
							filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
						}
						else
						{

							nlapiLogExecution('ERROR', 'purchasesorder');
							filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);							
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
							filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
							filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
						}

						var columns = new Array();

						columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
						var serialnumcsv = "";
						var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
						var serialnumArray=new Array();
						if (serchRec) {
							for (var n1 = 0; n1 < serchRec.length; n1++) {
								//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
								if (tempserialId == "") {
									tempserialId = serchRec[n1].getId();
									tempserial=serchRec[n1].getValue('custrecord_serialnumber');
								}
								else {
									tempserialId = tempserialId + "," + serchRec[n1].getId();
									tempserial=tempserial+","+serchRec[n1].getValue('custrecord_serialnumber');
								}
							}
						}//end

						if (tempserialId != null) {
							var temSeriIdArr = new Array();
							temSeriIdArr = tempserialId.split(',');
							nlapiLogExecution('DEBUG', 'updating Serial num Records to', 'Storage Status');
							for (var p = 0; p < temSeriIdArr.length; p++) {
								var fields = new Array();
								var values = new Array();
								fields[0] = 'custrecord_serialwmsstatus';
								values[0] = '3';
								var updateStatus = nlapiSubmitField('customrecord_ebiznetserialentry', temSeriIdArr[p], fields, values);
								nlapiLogExecution('DEBUG', 'Inside loop serialId ', temSeriIdArr[p]);

								nlapiLogExecution('DEBUG', 'Serial num Records to', 'Storage Status Success');
							}
						}
					} 
					catch (myexp) {
						nlapiLogExecution('DEBUG', 'Into Serial num exeption', myexp);
					}
				}

			}
			else
			{
				nlapiLogExecution('DEBUG', 'batchno', batchno);
				var fields = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group','custitem_ebizserialin'];
				var columns = nlapiLookupField('item', itemid, fields);
				var ItemType = columns.recordType;					
				var batchflg = columns.custitem_ebizbatchlot;
				var itemfamId= columns.custitem_item_family;
				var itemgrpId= columns.custitem_item_group;
				var serialInflg = columns.custitem_ebizserialin;
				var getlotnoid="";
				var expdate='';
				var vfifodate='';
				var vbatchno = batchno;
				if(ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflg == "T")
				{
					nlapiLogExecution('DEBUG', 'vbatchno', vbatchno);
					nlapiLogExecution('DEBUG', 'whLocation', whLocation);
					nlapiLogExecution('DEBUG', 'itemid', itemid);
					if(vbatchno!=""&&vbatchno!=null)
					{
						var filterspor = new Array();
						filterspor.push(new nlobjSearchFilter('name', null, 'is', vbatchno));
						/*if(whLocation!=null && whLocation!="")
							filterspor.push(new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', whLocation));*/
						if(itemid!=null && itemid!="")
							filterspor.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemid));

						var column=new Array();
						column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
						column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');

						var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
						if(receiptsearchresults!=null)
						{
							getlotnoid= receiptsearchresults[0].getId();
							nlapiLogExecution('DEBUG', 'getlotnoid', getlotnoid);
							expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');
							vfifodate=receiptsearchresults[0].getValue('custrecord_ebizfifodate');
						}
						nlapiLogExecution('DEBUG', 'expdate', expdate);
						nlapiLogExecution('DEBUG', 'vfifodate', vfifodate);
					}
				}
				if(vfifoDate==null || vfifoDate=='')
				{
					vfifoDate = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
					//invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
				}


				var getBaseUOM='';
				var getuomlevel='';

				var eBizItemDims=geteBizItemDimensions(itemid);
				if(eBizItemDims!=null&&eBizItemDims.length>0)
				{
					for(z=0; z < eBizItemDims.length; z++)
					{
						if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
						{
							getBaseUOM = eBizItemDims[z].getText('custrecord_ebizuomskudim');
							getuomlevel = eBizItemDims[z].getText('custrecord_ebizuomlevelskudim');
						}
					}
				}

				nlapiLogExecution('DEBUG', 'getBaseUOM', getBaseUOM);
				nlapiLogExecution('DEBUG', 'getuomlevel', getuomlevel);

				///Getting Records to insert in to inventory table.
				nlapiLogExecution('DEBUG', 'Cust INVT Rec Creation ', 'Record Creation');
				var accountNumber = "";

				//Creating Inventory Record.
				var invtRec = nlapiCreateRecord('customrecord_ebiznet_createinv');

				nlapiLogExecution('DEBUG', 'Creating INVT  Record if merge lp is false', 'INVT');

				invtRec.setFieldValue('name', idl);
				invtRec.setFieldValue('custrecord_ebiz_inv_binloc', EndLocationId);
				invtRec.setFieldValue('custrecord_ebiz_inv_lp', invtlp);
				invtRec.setFieldValue('custrecord_ebiz_inv_sku', itemid);
				invtRec.setFieldValue('custrecord_ebiz_inv_sku_status', itemstatus);
				if(itempackcode!=null&&itempackcode!="")
					invtRec.setFieldValue('custrecord_ebiz_inv_packcode', itempackcode);
				invtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(ActQuantity).toFixed(4));
				//invtRec.setFieldValue('custrecord_ebiz_inv_lot', batchno);
				//invtRec.setFieldValue('custrecord_ebiz_inv_recorddate',(parseFloat(d.getMonth()) + 1) + '/' + (parseFloat(d.getDate())) + '/' + d.getFullYear());
				//invtRec.setFieldValue('custrecord_ebiz_inv_rectime',((curr_hour) + ":" + (curr_min) + " " + a_p));
				invtRec.setFieldValue('custrecord_inv_ebizsku_no', itemid);
				invtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(ActQuantity).toFixed(4));
				invtRec.setFieldValue('custrecord_ebiz_itemdesc', itemdesc);
				invtRec.setFieldValue('custrecord_ebiz_inv_loc', whLocation);
				invtRec.setFieldValue('custrecord_ebiz_callinv', 'N');//WH Location.
				invtRec.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);
				invtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');
				invtRec.setFieldValue('custrecord_wms_inv_status_flag','19');//19=FLAG.INVENTORY.STORAGE
				invtRec.setFieldValue('custrecord_invttasktype', '2');
				invtRec.setFieldValue('custrecord_ebiz_transaction_no', pointid);
				invtRec.setFieldValue('custrecord_ebiz_countryoforigin', cntryid);
				if(getuomlevel!=null && getuomlevel!='')
					invtRec.setFieldText('custrecord_ebiz_uomlvl', getuomlevel);

				//commented on 08/02/12 by suman
				//since for normal inventory item fifovalue check is not necessary so fifovalue check is commented.

//				var fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
//				invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);

				// end of code

				var fifovalue='';
				var tempserialId='';var tempserial='';
				if (ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem"|| batchflg == "T")
//					if (ItemType == "lotnumberedinventoryitem" || batchflg == "T")
				{ 										
					try
					{
						//Checking FIFO Policy.					
						if (getlotnoid != "") {
							if(vfifodate ==null || vfifodate =='')
							{
								fifovalue = FifovalueCheck(ItemType,itemid,itemfamId,itemgrpId,linenum,pointid,getlotnoid);
								nlapiLogExecution('DEBUG','FIFO Value Check.',fifovalue);
							}
							else
							{
								fifovalue=vfifodate;
							}
							invtRec.setFieldValue('custrecord_ebiz_inv_fifo', fifovalue);
							invtRec.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
							invtRec.setFieldValue('custrecord_ebiz_expdate', expdate);
						}
					}
					catch(exp)
					{
						nlapiLogExecution('DEBUG','Exception in FIFO Value Check.',exp);
					}
				}			
				//Case# 201411030 starts
				//else if (ItemType == 'serializedinventoryitem' || ItemType.custitem_ebizserialin == 'T') { 
				else if (ItemType == 'serializedinventoryitem' || serialInflg == 'T') { 
					nlapiLogExecution('DEBUG','invtlp new',invtlp);
					//Case# 201411030 ends

					var filters = new Array();
					if(trantype=='returnauthorization')
					{
						nlapiLogExecution('ERROR', 'returnauthorization');
						filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
						filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
						filters[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
					}
					else if(trantype=='transferorder')
					{
						nlapiLogExecution('ERROR', 'transferorder');
						filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
						filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['8']);
						filters[2] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);
						filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
					}
					else
					{

						nlapiLogExecution('ERROR', 'purchasesorder');
						filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);							
						filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);						
						filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
						filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
					}

					var columns = new Array();

					columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
					var serialnumcsv = "";
					var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
					var serialnumArray=new Array();
					if (serchRec) {
						for (var n1 = 0; n1 < serchRec.length; n1++) {
							//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
							if (tempserialId == "") {
								tempserialId = serchRec[n1].getId();
								tempserial=serchRec[n1].getValue('custrecord_serialnumber');
							}
							else {
								tempserialId = tempserialId + "," + serchRec[n1].getId();
								tempserial=tempserial+","+serchRec[n1].getValue('custrecord_serialnumber');
							}
						}
					}
				}


				if(vfifoDate != null && vfifoDate != '')
					invtRec.setFieldValue('custrecord_ebiz_inv_fifo', vfifoDate);
				else
					invtRec.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());


				nlapiLogExecution('DEBUG', 'Before Submitting invtrecid', 'INVTRECORDS');

				//code added on 16Feb by suman
				if(pointid!=null && pointid!="")
					invtRec.setFieldValue('custrecord_ebiz_transaction_no', pointid);

				var invtrecid = nlapiSubmitRecord(invtRec, false, true);
				InvtRefNo=invtrecid;
				nlapiLogExecution('DEBUG', 'After Submitting invtrecid', invtrecid);
				if (invtrecid != null) {
					nlapiLogExecution('DEBUG', 'Cust INVT Rec Creation Succes with ID ', invtrecid);
				}
				else {
					nlapiLogExecution('DEBUG', 'Cust INVT Rec Creation Fail ', 'Fail');
				}

				if(invtrecid != null && invtrecid != '' && invtrecid != '-1')
					nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordId, 'custrecord_invref_no', invtrecid);

				for (var i = 0; i < searchresults.length; i++) {
					vbatchno=searchresults[i].getValue('custrecord_batch_no');
					var searchresult = searchresults[i];
					var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', searchresult.getId());

					nlapiLogExecution('DEBUG', 'STATUS before', searchresult.getValue('custrecord_wms_status_flag'));

					if (searchresult.getValue('custrecord_wms_status_flag') == 6) {
						transaction.setFieldValue('custrecord_actbeginloc', getBeginLocationInternalId);// Give location Id.
						transaction.setFieldValue('custrecordact_begin_date', DateStamp());
						//transaction.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
					}

					transaction.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
					transaction.setFieldValue('custrecord_recordupdatetime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
					transaction.setFieldValue('custrecord_upd_date', DateStamp());
					transaction.setFieldValue('custrecord_actendloc', EndLocationId);// Give location Id.
					transaction.setFieldValue('custrecord_act_end_date', DateStamp());
					transaction.setFieldValue('custrecord_wms_status_flag', 3);
					transaction.setFieldValue('custrecord_act_qty', parseFloat(ActQuantity).toFixed(4));
					nlapiLogExecution("ERROR","InvtRefNo",InvtRefNo);
					transaction.setFieldValue('custrecord_invref_no', InvtRefNo);
					if(idl != null && idl != '' && idl != '-1')
						transaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no', idl);
					transaction.setFieldValue('custrecord_serial_no', tempserial);
					//vputwrecid=nlapiSubmitRecord(transaction, false, true);
					//MoveTaskRecord(vputwrecid);
					vputwrecid[0]=nlapiSubmitRecord(transaction, false, true);
					vputwrecid[1]='';
					/*if(systemRule=='LP')
					{
						MoveTaskRecord(vputwrecid[0]);
					}*/
				} //for loop closing.

				var vitemSubtype = nlapiLookupField('item', itemid, ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot']);
				Itype = vitemSubtype.recordType;
				nlapiLogExecution('DEBUG', 'Itype', Itype);
				nlapiLogExecution('DEBUG', 'vitemSubtype', vitemSubtype);
				//serial Entry status updation to S
				if (vitemSubtype.recordType == 'serializedinventoryitem' || vitemSubtype.custitem_ebizserialin == 'T') {
					try {
						//search here 



						if (tempserialId != null && tempserialId!='') 
						{
							var getScannedserialId = '';//new Array();
							nlapiLogExecution('DEBUG', 'tempserialId.length new', tempserialId.length);
							nlapiLogExecution('DEBUG', 'tempserialId new', tempserialId);

							var temSeriIdArr = new Array();
							temSeriIdArr = tempserialId.split(',');

							nlapiLogExecution('DEBUG', 'updating Serial num Records to', 'Storage Status');
							nlapiLogExecution('DEBUG', 'getScannedSerialNoinQtyexception.length', getScannedSerialNoinQtyexception.length);
							nlapiLogExecution('DEBUG', 'getScannedSerialNoinQtyexception', getScannedSerialNoinQtyexception);
							if(getScannedSerialNoinQtyexception != null && getScannedSerialNoinQtyexception != '')
							{
								var filters = new Array();
								for (var g = 0; g < getScannedSerialNoinQtyexception.length; g++) 
								{


									filters[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getScannedSerialNoinQtyexception[g]);

									/*var columns = new Array();
								columns[0] = new nlobjSearchColumn('custrecord_serialnumber');*/

									var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, null);

									if (serchRec) {

										//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
										if (getScannedserialId == "") {
											getScannedserialId= serchRec[0].getId();

										}
										else {
											getScannedserialId = getScannedserialId + "," + serchRec[0].getId();

										}



									}
								}
							}
							if(getScannedserialId!=null && getScannedserialId!='')
								var getScannedserialIdArr = getScannedserialId.split(',');


							nlapiLogExecution('DEBUG', 'temSeriIdArr.length', temSeriIdArr.length);
							nlapiLogExecution('DEBUG', 'temSeriIdArr', temSeriIdArr);


							for (var p = 0; p <= temSeriIdArr.length; p++) 
							{
								if(getScannedSerialNoinQtyexception != null && getScannedSerialNoinQtyexception != '')
								{
									nlapiLogExecution('DEBUG', 'temSeriIdArr[p]', temSeriIdArr[p]);

									if(getScannedserialIdArr.indexOf(temSeriIdArr[p])!=-1)//not found temSeriIdArr
									{
										var fields = new Array();
										var values = new Array();
										fields[0] = 'custrecord_serialwmsstatus';
										fields[1] = 'custrecord_serial_location';
										fields[2] = 'custrecord_serialbinlocation';
										values[0] = '3';
										values[1] = whLocation;
										values[2] = EndLocationId;
										var updateStatus = nlapiSubmitField('customrecord_ebiznetserialentry', temSeriIdArr[p], fields, values);
										nlapiLogExecution('DEBUG', 'Inside loop serialId if', temSeriIdArr[p]);

										nlapiLogExecution('DEBUG', 'Serial num Records to2', 'Storage Status Success');
									}
									else
									{

										var fields = new Array();
										var values = new Array();
										fields[0] = 'custrecord_serialparentid';
										fields[1] = 'custrecord_serial_location';
										fields[2] = 'custrecord_serialbinlocation';
										values[0] = NewGenLP;// case# 201416671
										values[1] = whLocation;
										values[2] = EndLocationId;
										var updateStatus = nlapiSubmitField('customrecord_ebiznetserialentry', temSeriIdArr[p], fields, values);
										nlapiLogExecution('DEBUG', 'Inside loop serialId else', temSeriIdArr[p]);

										nlapiLogExecution('DEBUG', 'Serial num Records to3', 'Storage Status Success');
									}
								}
								else
								{
									var fields = new Array();
									var values = new Array();
									fields[0] = 'custrecord_serialwmsstatus';
									fields[1] = 'custrecord_serial_location';
									fields[2] = 'custrecord_serialbinlocation';
									values[0] = '3';
									values[1] = whLocation;
									values[2] = EndLocationId;
									var updateStatus = nlapiSubmitField('customrecord_ebiznetserialentry', temSeriIdArr[p], fields, values);
									nlapiLogExecution('DEBUG', 'Inside loop serialId main else', temSeriIdArr[p]);

									nlapiLogExecution('DEBUG', 'Serial num Records to4', 'Storage Status Success');
								}
							}
						}
					} 
					catch (myexp) {
						nlapiLogExecution('DEBUG', 'Into Serial num exeption', myexp);
					}
				}
			}

		} //if Item receipt is not null.
		else {
			nlapiLogExecution('DEBUG', 'If Item Receipt Failed', 'Error in Item Receipt');
			nlapiLogExecution('DEBUG', 'Updating PUTW task with end location - Record Id', RecordId);
			var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordId);
			transaction.setFieldValue('custrecord_actendloc', EndLocationId);
			nlapiSubmitRecord(transaction, false, true);
			nlapiLogExecution('DEBUG', 'PUTW task updated with act end location', EndLocationId);
		}
	} 
	catch (e) {

		var exceptionname=trantype;
		var functionality='Creating ItemReceipt';
		var trantype2=1;//1 for Inbound and 2 for outbound
		var vebizOrdNo=pointid;
		var vcontainerLp=invtlp;
		nlapiLogExecution('DEBUG', 'DetailsError', functionality);	
		nlapiLogExecution('DEBUG', 'vcontainerLp', vcontainerLp);
		nlapiLogExecution('DEBUG', 'vebizOrdNo35', vebizOrdNo);
		var reference2="";
		var reference3="";
		var reference4 ="";
		var reference5 ="";
		var alertType=1;//1 for exception and 2 for report
		var userId = nlapiGetUser();
		var exceptiondetails=e;
		if ( e instanceof nlobjError )
		{
			nlapiLogExecution( 'DEBUG', 'system error', e.getCode() + '\n' + e.getDetails() );
			exceptiondetails=e.getCode() + '\n' + e.getDetails();
		}
		else
		{
			exceptiondetails=e.toString();
			nlapiLogExecution( 'DEBUG', 'unexpected error', e.toString() );
		}
		errSendMailByProcess(trantype2,e,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
		InsertExceptionLog(exceptionname,trantype2, functionality, exceptiondetails, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId);
		//UpdateOpenTaskWithExceptionValue(vebizOrdNo,vebizOrdNo,vcontainerLp,exceptiondetails);
		nlapiLogExecution('DEBUG', 'Exception in TransformRec (PurchaseOrder) : ', e);

		if (e instanceof nlobjError) 
			nlapiLogExecution('DEBUG', 'system error', e.getCode() + '\n' + e.getDetails());
		else 
			nlapiLogExecution('DEBUG', 'unexpected error', e.toString());

		nlapiLogExecution('DEBUG', 'Updating PUTW task with end location - Record Id', RecordId);
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordId);
		transaction.setFieldValue('custrecord_actendloc', EndLocationId);
		transaction.setFieldValue('custrecord_ebiz_error_log', e);
		nlapiSubmitRecord(transaction, false, true);
		nlapiLogExecution('DEBUG', 'PUTW task updated with act end location', EndLocationId);
		nlapiLogExecution('DEBUG', 'redirecting to rf error', EndLocationId);

		//response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
		//return;
		//vputwrecid = e;
		vputwrecid[0]=-1;
		vputwrecid[1]=e;

	}

	return vputwrecid;
}

function isMergeLP(poloc,methodid)
{
	var varMergeLP = 'F';
	var filtersputmethod = new Array();
	filtersputmethod.push(new nlobjSearchFilter('custrecord_ebizsiteputaway', null, 'anyof', ['@NONE@',poloc]));
	if(methodid!=null && methodid!='')
		filtersputmethod.push(new nlobjSearchFilter('custrecord_methodid', null, 'is', methodid));
	filtersputmethod.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var colsputmethod = new Array();
	colsputmethod[0] = new nlobjSearchColumn('custrecord_mergelp');            

	var putwmethodsearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_method', null, filtersputmethod, colsputmethod);

	if(putwmethodsearchresults != null &&putwmethodsearchresults != ''&& putwmethodsearchresults.length > 0)
	{

		varMergeLP = putwmethodsearchresults[0].getValue('custrecord_mergelp');
	}

	return varMergeLP;
}
function getMergeLPMethods(poloc)
{
	nlapiLogExecution('ERROR','Into  getMergeLPMethods');

	var putwmethodsearchresults = new Array();
	var filtersputmethod = new Array();
	filtersputmethod.push(new nlobjSearchFilter('custrecord_ebizsiteputaway', null, 'anyof', ['@NONE@',poloc]));
	filtersputmethod.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filtersputmethod.push(new nlobjSearchFilter('custrecord_mergelp', null, 'is', 'T'));

	var colsputmethod = new Array();
	colsputmethod[0] = new nlobjSearchColumn('custrecord_methodid');  
	colsputmethod[1] = new nlobjSearchColumn('name');  

	putwmethodsearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_method', null, filtersputmethod, colsputmethod);

	nlapiLogExecution('ERROR','Out of  getMergeLPMethods');

	return putwmethodsearchresults;
}
function isMergeLPEnabled(poloc,methodid,mergeputmethods)
{
	nlapiLogExecution('ERROR','Into  isMergeLPEnabled');

	var varMergeLP = 'F';

	if(mergeputmethods!=null && mergeputmethods!='' && mergeputmethods.length>0)
	{
		nlapiLogExecution('ERROR','Merge Methods Length',mergeputmethods.length);

		for (var s = 0; s < mergeputmethods.length; s++) {

			var mergemethodid = mergeputmethods[s].getValue('custrecord_methodid');
			var mergemethodname = mergeputmethods[s].getValue('name');
			nlapiLogExecution('ERROR','mergemethodid',mergemethodid);
			nlapiLogExecution('ERROR','mergemethodname',mergemethodname);
			nlapiLogExecution('ERROR','task methodid',methodid);
			if(mergemethodid==methodid || mergemethodname==methodid)
			{
				varMergeLP='T';
			}
		}
	}
	else
	{
		varMergeLP='F';
	}

	nlapiLogExecution('ERROR','Out of  isMergeLPEnabled');
	return varMergeLP;
}


function getMaxUOMQty(putItemId,putItemPC)
{
	var varPaltQty=0;
	var searchresults = new Array();

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', putItemId));
	if(putItemPC!=null && putItemPC!='')
		filters.push(new nlobjSearchFilter('custrecord_ebizpackcodeskudim', null, 'is', putItemPC));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizqty');
	columns[1] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim'); 	
	columns[1].setSort(true);

	searchresults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);
	if(searchresults != null && searchresults != '' && searchresults.length > 0)
	{
		varPaltQty = searchresults[0].getValue('custrecord_ebizqty');
	}

	return varPaltQty;
}
function DeleteInvtRecCreatedforCHKNTask(pointid, lp)
{
	nlapiLogExecution('DEBUG','Inside DeleteInvtRecCreatedforCHKNTask ','Funciton');
	var Ifilters = new Array();
	Ifilters[0] = new nlobjSearchFilter('name', null, 'is', pointid);
	Ifilters[1] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [17]);
	Ifilters[2] = new nlobjSearchFilter('custrecord_invttasktype', null, 'anyof', [1]);
	Ifilters[3] = new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', lp);

	var invtId="";
	var invtType="";
	var serchInvtRec= nlapiSearchRecord('customrecord_ebiznet_createinv', null, Ifilters, null);
	if (serchInvtRec) 
	{
		for (var s = 0; s < serchInvtRec.length; s++) {
			nlapiLogExecution('DEBUG','Inside loop','loop');
			var searchresult = serchInvtRec[ s ];
			nlapiLogExecution('DEBUG','need to print this','print');
			invtId = serchInvtRec[s].getId();
			invtType= serchInvtRec[s].getRecordType();
			nlapiLogExecution('DEBUG','CHKN INVT Id',invtId);
			nlapiLogExecution('DEBUG','CHKN invtType ',invtType);
			nlapiDeleteRecord(serchInvtRec[s].getRecordType(),serchInvtRec[s].getId());
			nlapiLogExecution('DEBUG','Invt Deleted record Id',invtId);

		}
	}
}

function geteBizItemDimensions(itemid)
{
	var searchRec = new Array();
	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemid));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim') ;
	column[1] = new nlobjSearchColumn('custrecord_ebizqty') ;
	column[2] = new nlobjSearchColumn('custrecord_ebizbaseuom') ;
	column[3] = new nlobjSearchColumn('custrecord_ebiznsuom') ;
	column[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim') ;
	searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);

	return searchRec;

}

if (!Array.prototype.indexOf)
{
	Array.prototype.indexOf = function(elt /*, from*/)
	{
		var len = this.length;

		var from = Number(arguments[1]) || 0;
		from = (from < 0)
		? Math.ceil(from)
				: Math.floor(from);
		if (from < 0)
			from += len;

		for (; from < len; from++)
		{
			if (from in this &&
					this[from] === elt)
				return from;
		}
		return -1;
	};
}

function GetSystemRuleForPostItemReceiptby(whloc) //Case# 20149154
{
	try
	{
		var rulevalue='';	
		var filter=new Array();
		filter.push(new nlobjSearchFilter('name',null,'is','Post Item Receipt by'));
		//filter.push(new nlobjSearchFilter('custrecord_ebizsite',null,'anyof',['@NONE@',whLocation]));

		// case no start 20126968
		var vRoleLocation=getRoledBasedLocation();
		var resloc=new Array();
		resloc.push("@NONE@");

		nlapiLogExecution('ERROR','vRoleLocation',vRoleLocation);
		//case# 20149824
		if(whloc!=null && whloc!='')
		{
			filter.push(new nlobjSearchFilter('custrecord_ebizsite',null,'anyof',['@NONE@',whloc]));
		}
		else if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
		{
			for(var count=0;count<vRoleLocation.length;count++)
				resloc.push(vRoleLocation[count]);
			filter.push(new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', resloc));
		}
		// Case# 20149154 starts
		/*else
			{
			filter.push(new nlobjSearchFilter('custrecord_ebizsite',null,'anyof',['@NONE@',whloc]));
			}*/
		// Case# 20149154 ends
		// case no end 20126968
		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizrulevalue');
		column[1]=new nlobjSearchColumn('custrecord_ebizsite');

		var searchresult=nlapiSearchRecord('customrecord_ebiznet_sysrules',null,filter,column);
		var siteval = '';
		if(searchresult!=null && searchresult!="")
		{
			for(var k = 0;k<searchresult.length;k++)
			{
				siteval = searchresult[k].getValue('custrecord_ebizsite');
				nlapiLogExecution('ERROR','whloc',whloc);
				nlapiLogExecution('ERROR','siteval',siteval);

				if(whloc!=null && whloc!='')
				{
					if(whloc == siteval)
					{
						rulevalue=searchresult[k].getValue('custrecord_ebizrulevalue');
						break;
					}
				}
			}

			if(rulevalue==null || rulevalue=='')
			{
				for(var z = 0;z<searchresult.length;z++)
				{
					siteval = searchresult[z].getValue('custrecord_ebizsite');

					if(siteval==null || siteval=='')
					{
						rulevalue=searchresult[z].getValue('custrecord_ebizrulevalue');
						break;
					}
				}

			}
		}

		nlapiLogExecution('ERROR','rulevalue',rulevalue);
		if(rulevalue!=null && rulevalue!='undefined' && rulevalue!='null' && rulevalue!='')
		{
			if(rulevalue.trim()!='LP' && rulevalue.trim()!='PO')
			{
				rulevalue='LP';
			}
		}
		else
		{
			rulevalue='LP';
		}
		nlapiLogExecution('ERROR','rulevalue',rulevalue);
		return rulevalue;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetSystemRuleForPostItemReceiptby',exp);
	}
}


function generateItemReceipt(fromrecord,fromid,ActQuantity,linenum,itemstatus,itemid,trantype,pointid,invtlp,systemRule,
		RecordId,EndLocationId,batchno,qtyexceptionFlag,templocation,getScannedSerialNoinQtyexception,response)
{
	try {
		var TranferInvFlag = 'F';
		var tempserialId=0;
		var trecord = nlapiTransformRecord(fromrecord, fromid, 'itemreceipt');
		var polinelength = trecord.getLineItemCount('item');
		nlapiLogExecution('ERROR', "polinelength", polinelength);
		var idl;
		var vAdvBinManagement=true;
		var whLocation="";//case# 20149817
		var ctx = nlapiGetContext();
		if(ctx != null && ctx != '')
		{
			if(ctx.getFeature('advbinseriallotmgmt').toString() != null && ctx.getFeature('advbinseriallotmgmt').toString() != '')
				vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
			nlapiLogExecution('ERROR', 'vAdvBinManagement_if', vAdvBinManagement);
		}
		//}  
		nlapiLogExecution('ERROR', 'vAdvBinManagement', vAdvBinManagement);
		for (var j = 1; j <= polinelength; j++) {
			nlapiLogExecution('ERROR', "Get Item Id", trecord.getLineItemValue('item', 'item', j));
			nlapiLogExecution('ERROR', "Get Item Text", trecord.getLineItemText('item', 'item', j));

			var item_id = trecord.getLineItemValue('item', 'item', j);
			var itemrec = trecord.getLineItemValue('item', 'itemreceive', j);
			var itemLineNo = trecord.getLineItemValue('item', 'line', j);
			//var poItemUOM = trecord.getLineItemValue('item', 'unitsdisplay', j);//text
			var poItemUOM = trecord.getLineItemValue('item', 'units', j);//value

			var commitflag = 'N';
			var quantity=0;
			quantity = ActQuantity;

			nlapiLogExecution('ERROR', 'quantity', quantity);

			var inventorytransfer="";
			nlapiLogExecution('ERROR', "itemLineNo(PO/TO)", itemLineNo);
			nlapiLogExecution('ERROR', "linenum(Task)", linenum);
			nlapiLogExecution('ERROR', "poItemUOM", poItemUOM);

			if(trantype!='transferorder')
			{
				if (itemLineNo == linenum) {

					//This part of the code will fetch the line level location.
					//If line level is empty then header level location is considered.
					//code added by suman on 080513.
					whLocation=trecord.getLineItemValue('item', 'location', j);//value
					if(whLocation==null||whLocation=="")
						whLocation=templocation;
					//End of code as of 080513.

					//var vrcvqty=arrQty[i];
					var vrcvqty=quantity;
					if(poItemUOM!=null && poItemUOM!='')
					{
						var vbaseuomqty=0;
						var vuomqty=0;
						var eBizItemDims=geteBizItemDimensions(item_id);
						if(eBizItemDims!=null&&eBizItemDims.length>0)
						{
							nlapiLogExecution('ERROR', 'Dims Length', eBizItemDims.length);
							for(var z=0; z < eBizItemDims.length; z++)
							{
								if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
								{
									vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
								}
								nlapiLogExecution('ERROR', 'poItemUOM', poItemUOM);
								nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
								nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
								if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
								{
									vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
								}
							}
							if(vuomqty==null || vuomqty=='')
							{
								vuomqty=vbaseuomqty;
							}
							nlapiLogExecution('ERROR', 'vbaseuomqty', vbaseuomqty);
							nlapiLogExecution('ERROR', 'vuomqty', vuomqty);

							if(quantity==null || quantity=='' || isNaN(quantity))
								quantity=0;
							else
								quantity = (parseFloat(quantity)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

						}
					}
					//Code Added by Ramana
					var varItemlocation;
					nlapiLogExecution('ERROR', 'itemstatus', itemstatus);
					varItemlocation = getItemStatusMapLoc(itemstatus);
					//upto to here

					commitflag = 'Y';

					var fields = ['recordType', 'custitem_ebizserialin', 'custitem_ebizbatchlot'];
					var columns = nlapiLookupField('item', itemid, fields);
					if(columns != null && columns != '')
					{
						Itype = columns.recordType;	


						serialInflg = columns.custitem_ebizserialin;
					}

					nlapiLogExecution('ERROR', 'Value of J', j);
					/*if (Itype != "serializedinventoryitem" || serialInflg != "T")
					{*/
					trecord.selectLineItem('item', j);
					trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
					trecord.setCurrentLineItemValue('item', 'quantity', quantity);

					trecord.setCurrentLineItemValue('item', 'location', varItemlocation); //Added by ramana
					/*}*/

					nlapiLogExecution('ERROR', 'Serialquantity', quantity);
					nlapiLogExecution('ERROR', 'Into SerialNos');
					var totalconfirmedQty=0;
					if (Itype == "serializedinventoryitem" ||Itype == "serializedassemblyitem") {
						nlapiLogExecution('ERROR', 'Into SerialNos');
						nlapiLogExecution('ERROR', 'invtlp', invtlp);	
						nlapiLogExecution('ERROR', 'trantype', trantype);		
						var serialline = new Array();
						var serialId = new Array();

						var tempSerial = "";
						var filters = new Array();


//						filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
//						filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
//						filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
//						filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);

						if(trantype=='returnauthorization')
						{
							filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
							//Commented for RMA, For RMA no need to check  whether status is checkin/storage
							//filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
							filters[1] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);
							filters[2] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
							// case no 20126617 For RMA no need to check  whether status is checkin/storage
							//filters[3] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['1']);

						}
						else
						{
							filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['1']);
							filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
							filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
							// case# 201416543
							filters[4] = new nlobjSearchFilter('custrecord_serialstatus', null, 'isnot', 'P');// if we have total 5 serial# with putaway and if we do qty exception for 2/3 as actual/remaning then in serial entry er updating 3 remaing qty related serial# serial status 'P',here we have send 2 serial# whose serialstatus is empty.for this reason we have kept this filter
						}

						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
						var serialnumcsv = "";
						var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
						var serialnumArray=new Array();
						if (serchRec) {
							for (var n = 0; n < serchRec.length; n++) {
								//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
								if (tempserialId == "") {
									tempserialId = serchRec[n].getId();
								}
								else {
									tempserialId = tempserialId + "," + serchRec[n].getId();
								}

								//This is for Serial num loopin with Space separated.
								if (tempSerial == "") {
									tempSerial = serchRec[n].getValue('custrecord_serialnumber');

									serialnumArray.push(serchRec[n].getValue('custrecord_serialnumber'));

								}
								else {

									tempSerial = tempSerial + " " + serchRec[n].getValue('custrecord_serialnumber');
									if(tempSerial.length>3500)
									{
										tempSerial='';
										trecord.selectLineItem('item', j);
										trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
										var tempqty=serialnumArray.length;//parseInt(n-1)+parseInt(1);
										trecord.setCurrentLineItemValue('item', 'quantity', tempqty);

										trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
										trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumArray);
										nlapiLogExecution('ERROR', 'serialnumArray', serialnumArray);

										trecord.commitLineItem('item');
										idl = nlapiSubmitRecord(trecord);
										serialnumArray=new Array();
										totalconfirmedQty=parseInt(totalconfirmedQty)+parseInt(tempqty);
										if(n<serchRec.length)
										{
											var tQty=parseInt(quantity)-parseInt(totalconfirmedQty);
											nlapiLogExecution('ERROR', 'tQty1', tQty);
											var trecord = nlapiTransformRecord(fromrecord, fromid, 'itemreceipt');
											var polinelength = trecord.getLineItemCount('item');
											/*for (var j3 = 1; j3 <= polinelength; j3++) {*/
											var item_id = trecord.getLineItemValue('item', 'item', itemLineNo);
											var itemrec = trecord.getLineItemValue('item', 'itemreceive', itemLineNo);
											var itemLineNo = trecord.getLineItemValue('item', 'line', itemLineNo);
											var poItemUOM = trecord.getLineItemValue('item', 'unitsdisplay', itemLineNo);

											/*if (itemLineNo == linenum)
											{*/
											if(poItemUOM!=null && poItemUOM!='')
											{
												var vbaseuomqty=0;
												var vuomqty=0;
												var eBizItemDims=geteBizItemDimensions(item_id);
												if(eBizItemDims!=null&&eBizItemDims.length>0)
												{
													nlapiLogExecution('ERROR', 'Dims Length', eBizItemDims.length);
													for(var z=0; z < eBizItemDims.length; z++)
													{
														if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
														{
															vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
														}
														nlapiLogExecution('ERROR', 'poItemUOM', poItemUOM);
														nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
														nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
														if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
														{
															vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
														}
													}
													if(vuomqty==null || vuomqty=='')
													{
														vuomqty=vbaseuomqty;
													}
													nlapiLogExecution('ERROR', 'vbaseuomqty', vbaseuomqty);
													nlapiLogExecution('ERROR', 'vuomqty', vuomqty);

													if(tQty==null || tQty=='' || isNaN(tQty))
														tQty=0;
													else
														tQty = (parseFloat(tQty)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

												}
											}
											nlapiLogExecution('ERROR', 'tQtylast', tQty);
											trecord.selectLineItem('item', itemLineNo);
											trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
											var tempqty=parseInt(n)+parseInt(1);
											trecord.setCurrentLineItemValue('item', 'quantity', tQty);

											trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
											//}

											//}
										}

									}
									serialnumArray.push(serchRec[n].getValue('custrecord_serialnumber'));

								}
								if(vAdvBinManagement)
								{
									nlapiLogExecution('ERROR', 'custrecord_serialnumber', serchRec[n].getValue('custrecord_serialnumber'));
									//case 20125918 start : serial numbers posting
									if(n == 0)
										var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
									//case 20125918 end
									compSubRecord.selectNewLineItem('inventoryassignment');
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serchRec[n].getValue('custrecord_serialnumber'));

									compSubRecord.commitLineItem('inventoryassignment');
									//	compSubRecord.commit();
								}

								//if(parseInt(n)== parseInt(quantity-1))
								//	break;




							}
							//case 20125918 start : serial numbers posting
							if(vAdvBinManagement)
								compSubRecord.commit();
							//case 20125918 end
							nlapiLogExecution('ERROR', 'qtyexceptionFlag', qtyexceptionFlag);
							if(qtyexceptionFlag == "true")
							{
								serialnumcsv = getScannedSerialNoinQtyexception;
								nlapiLogExecution('ERROR', 'Serial nums assigned when qtyexceptionFlag is true', serialnumcsv);
							}
							else
							{
								serialnumcsv = tempSerial;
								nlapiLogExecution('ERROR', 'Serial nums assigned when qtyexceptionFlag is false', serialnumcsv);
							}


						}
						tempSerial = "";
						nlapiLogExecution('ERROR', 'Serial nums assigned', serialnumcsv);
						nlapiLogExecution('ERROR', 'Serial nums tempserialId', tempserialId);
						if(!vAdvBinManagement)
						{
							if ((Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem")  && (trecord!=null && trecord!='')) {
								//trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumArray);
								trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumcsv);
							}
						}


					}
					else if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem") {
						//if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem"  || Itype == "assemblyitem") {
						nlapiLogExecution('ERROR', 'Into LOT/Batch');
						nlapiLogExecution('ERROR', 'LP:', invtlp);
						var tempBatch = "";
						var batchcsv = "";
						var confirmLotToNS='Y';

						confirmLotToNS=GetConfirmLotToNS(varItemlocation);
						nlapiLogExecution('ERROR', 'confirmLotToNS:', confirmLotToNS);
						var batfilter = new Array();
						batfilter[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);
						batfilter[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
						batfilter[2] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', pointid);
						batfilter[3] = new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum);
						batfilter[4] = new nlobjSearchFilter('custrecord_lpno', null, 'is', invtlp);

						var vatColumns=new Array();
						vatColumns[0] = new nlobjSearchColumn('custrecord_lotnowithquantity');
						vatColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
						/*var batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, batfilter, vatColumns);

							if (batchsearchresults) {
								for (var n = 0; n < batchsearchresults.length; n++) {


									//This is for Batch num loopin with Space separated.
									if (tempBatch == "") {
										if(confirmLotToNS == 'Y')
											tempBatch = batchsearchresults[n].getValue('custrecord_lotnowithquantity');
										else
										{
											nlapiLogExecution('ERROR', 'item_id:', item_id);
											var filters1 = new Array();          
											filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
											//Changed on 30/4/12 by suman
											filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
											//End of Chages as of 30/4/12

											var columns = new Array();

											columns[0] = new nlobjSearchColumn('itemid');
											var vItemname;
											var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
											if (itemdetails !=null) 
											{
												vItemname=itemdetails[0].getValue('itemid');
												nlapiLogExecution('ERROR', 'vItemname:', vItemname);
												var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
												vItemname=vItemname.replace(/ /g,"-");
												tempBatch=vItemname + '('+ vItQty + ')';


											}
										}
									}
									else {
										if(confirmLotToNS == 'Y')
											tempBatch = tempBatch + " " + batchsearchresults[n].getValue('custrecord_lotnowithquantity');
										else
										{
											var filters1 = new Array();          
											filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
											//Changed on 30/4/12 by suman
											filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
											//End of Chages as of 30/4/12
											var columns = new Array();

											columns[0] = new nlobjSearchColumn('itemid');
											var vItemname;
											var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
											if (itemdetails !=null) 
											{
												vItemname=itemdetails[0].getValue('itemid');
												nlapiLogExecution('ERROR', 'vItemname:', vItemname);
												var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
												vItemname=vItemname.replace(/ /g,"-");
												tempBatch=tempBatch + " " + vItemname + '('+ vItQty + ')';


											}
										}

									}

								}

								batchcsv = tempBatch;
								tempBatch = "";
							}*/
						var vItemname;
						if(confirmLotToNS == 'Y')
							tempBatch = batchno + '('+ quantity + ')';
						else
						{
							nlapiLogExecution('ERROR', 'item_id:', item_id);
							var filters1 = new Array();          
							filters1[0] = new nlobjSearchFilter('internalid', null, 'is',item_id);
							var columns = new Array();

							columns[0] = new nlobjSearchColumn('itemid');

							var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
							if (itemdetails !=null) 
							{
								vItemname=itemdetails[0].getValue('itemid');
								nlapiLogExecution('ERROR', 'vItemname:', vItemname);
								//var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
								vItemname=vItemname.replace(/ /g,"-");
								tempBatch=vItemname + '('+ quantity + ')';


							}
						}
						nlapiLogExecution('ERROR', 'batchno', batchno);
						var expdate1=getLotExpdate(batchno,item_id);
						if(!vAdvBinManagement)
						{	
							batchcsv = tempBatch;
							nlapiLogExecution('ERROR', 'LOT/Batch nums assigned', batchcsv);
							trecord.setCurrentLineItemValue('item', 'serialnumbers', batchcsv);
							//20123720 start
							if(expdate1 !=null && expdate1 !='')
							{
								trecord.setCurrentLineItemValue('item', 'expirationdate', expdate1);

							}//end
						}
						else
						{
							var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
							compSubRecord.selectNewLineItem('inventoryassignment');
							compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', quantity);
							nlapiLogExecution('ERROR', 'expdate1', expdate1);
							if(expdate1 !=null && expdate1 !='')
								compSubRecord.setCurrentLineItemValue('inventoryassignment', 'expirationdate', expdate1);
							if(confirmLotToNS == 'Y')
								compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', batchno);
							else
								compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
							compSubRecord.commitLineItem('inventoryassignment');
							compSubRecord.commit();
						}


					}
					// case no 20126691
					trecord.setCurrentLineItemValue('item', 'custcol_lp', invtlp);
					trecord.setCurrentLineItemValue('item', 'custcol_ebiznet_item_status', itemstatus);
					if(trecord!=null  && trecord!='')
						trecord.commitLineItem('item');
				}
			}
			else
			{ 

				nlapiLogExecution('ERROR', 'itemid (Task)', itemid);	
				nlapiLogExecution('ERROR', 'item_id (Line)', item_id);	
				//if(vTORestrictFlag =='F')
				//{	
				itemLineNo=parseFloat(itemLineNo)-2;
				if (itemid == item_id && itemLineNo == linenum) {

					commitflag = 'Y';

					var varItemlocation;				
					varItemlocation = getItemStatusMapLoc(itemstatus);

					var fields = ['recordType', 'custitem_ebizserialin'];
					var columns = nlapiLookupField('item', itemid, fields);
					if(columns != null && columns != '')
					{
						Itype = columns.recordType;	

						//	var serialInflg="F";		
						serialInflg = columns.custitem_ebizserialin;
						nlapiLogExecution('ERROR', 'Itype', Itype);
						nlapiLogExecution('ERROR', 'columns', columns);
					}
					//case# 20149817 starts(whLocation is not defined issue)
					whLocation=trecord.getLineItemValue('item', 'location', j);
					if(whLocation==null||whLocation=="")
						whLocation=templocation;
					//case# 20149817 ends
					nlapiLogExecution('ERROR', 'Value of J', j);
					trecord.selectLineItem('item', j);
					trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
					trecord.setCurrentLineItemValue('item', 'quantity', quantity);
					if(trantype!='transferorder')
						trecord.setCurrentLineItemValue('item', 'location', varItemlocation); //Added by ramana

					if(whLocation == varItemlocation)
					{
						trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
					}
					else
					{
						TranferInvFlag = 'T';
						trecord.setCurrentLineItemValue('item', 'location', whLocation);
					}

					nlapiLogExecution('ERROR', 'Serialquantity', quantity);
					nlapiLogExecution('ERROR', 'Into SerialNos');
					if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem" ){//|| serialInflg == "T") {
						nlapiLogExecution('ERROR', 'Into SerialNos');
						nlapiLogExecution('ERROR', 'invtlp', invtlp);					
						var serialline = new Array();
						var serialId = new Array();



						var tempSerial = "";
						var filters = new Array();
						/*if(trantype=='returnauthorization' || trantype=='transferorder')
						{*/
						if(trantype=='returnauthorization')
						{
							nlapiLogExecution('ERROR', 'returnauthorization');
							filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
							filters[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);
						}
						else if(trantype=='transferorder')
						{
							nlapiLogExecution('ERROR', 'transferorder');
							filters[0] = new nlobjSearchFilter('custrecord_serialebizrmano', null, 'is', pointid);
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['8']);
							filters[2] = new nlobjSearchFilter('custrecord_ebiz_serial_rmalineno', null, 'is', linenum);
							filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
						}
						else
						{

							nlapiLogExecution('ERROR', 'purchasesorder');
							filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);							
							filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);						
							filters[2] = new nlobjSearchFilter('custrecord_serialpolineno', null, 'is', linenum);
							filters[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', invtlp);
						}
//						filters[0] = new nlobjSearchFilter('custrecord_serialebizpono', null, 'is', pointid);
//						filters[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', [1]);

						var columns = new Array();
						columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
						var serialnumcsv = "";
						var serchRec = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters, columns);
						var serialnumArray=new Array();var totalconfirmedQty=0;
						if (serchRec) {
							for (var n = 0; n < serchRec.length; n++) {
								nlapiLogExecution('ERROR', 'serchRec.length', serchRec.length);
								var tempserialcount=parseInt(n)+1;
								if(parseInt(tempserialcount)<=parseInt(quantity)){
									//This loop is for Serial Id to make them comma Separated and update the record with 'S' Status.
									if (tempserialId == "") {
										tempserialId = serchRec[n].getId();
									}
									else {
										tempserialId = tempserialId + "," + serchRec[n].getId();
									}

									//This is for Serial num loopin with Space separated.
									if (tempSerial == "") {
										tempSerial = serchRec[n].getValue('custrecord_serialnumber');

										serialnumArray.push(serchRec[n].getValue('custrecord_serialnumber'));

									}
									else {
										nlapiLogExecution('ERROR', 'tempSerial', tempSerial);
										tempSerial = tempSerial + " " + serchRec[n].getValue('custrecord_serialnumber');
										if(tempSerial.length>3500)
										{
											tempSerial='';
											trecord.selectLineItem('item', j);
											trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
											var tempqty=serialnumArray.length;//parseInt(n-1)+parseInt(1);
											trecord.setCurrentLineItemValue('item', 'quantity', tempqty);

											trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
											trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumArray);
											nlapiLogExecution('ERROR', 'serialnumArray', serialnumArray);

											trecord.commitLineItem('item');
											idl = nlapiSubmitRecord(trecord);
											serialnumArray=new Array();
											totalconfirmedQty=parseInt(totalconfirmedQty)+parseInt(tempqty);
											if(n<serchRec.length)
											{
												var tQty=parseInt(quantity)-parseInt(totalconfirmedQty);
												nlapiLogExecution('ERROR', 'tQty1', tQty);
												var trecord = nlapiTransformRecord(fromrecord, fromid, 'itemreceipt');
												var polinelength = trecord.getLineItemCount('item');
												/*for (var j3 = 1; j3 <= polinelength; j3++) {*/
												var item_id = trecord.getLineItemValue('item', 'item', itemLineNo);
												var itemrec = trecord.getLineItemValue('item', 'itemreceive', itemLineNo);
												//var itemLineNo = trecord.getLineItemValue('item', 'line', itemLineNo);
												var poItemUOM = trecord.getLineItemValue('item', 'unitsdisplay', itemLineNo);

												/*if (itemLineNo == linenum)
											{*/
												if(poItemUOM!=null && poItemUOM!='')
												{
													var vbaseuomqty=0;
													var vuomqty=0;
													var eBizItemDims=geteBizItemDimensions(item_id);
													if(eBizItemDims!=null&&eBizItemDims.length>0)
													{
														nlapiLogExecution('ERROR', 'Dims Length', eBizItemDims.length);
														for(var z=0; z < eBizItemDims.length; z++)
														{
															if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
															{
																vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
															}
															nlapiLogExecution('ERROR', 'poItemUOM', poItemUOM);
															nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
															nlapiLogExecution('ERROR', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
															if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
															{
																vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
															}
														}
														if(vuomqty==null || vuomqty=='')
														{
															vuomqty=vbaseuomqty;
														}
														nlapiLogExecution('ERROR', 'vbaseuomqty', vbaseuomqty);
														nlapiLogExecution('ERROR', 'vuomqty', vuomqty);

														if(tQty==null || tQty=='' || isNaN(tQty))
															tQty=0;
														else
															tQty = (parseFloat(tQty)*parseFloat(vbaseuomqty))/parseFloat(vuomqty);										

													}
												}
												nlapiLogExecution('ERROR', 'tQtylast', tQty);
												trecord.selectLineItem('item', itemLineNo);
												trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
												var tempqty=parseInt(n)+parseInt(1);
												trecord.setCurrentLineItemValue('item', 'quantity', tQty);

												trecord.setCurrentLineItemValue('item', 'location', varItemlocation);
												//}

												//}
											}
										}
										serialnumArray.push(serchRec[n].getValue('custrecord_serialnumber'));

									}
									//case # 20126900 Start
									/*if(vAdvBinManagement)
									{
										//case # 20127075ÃƒÆ’Ã‚Â¯Ãƒâ€šÃ‚Â¿Ãƒâ€šÃ‚Â½, uncommented first two lines
										if(n == 0)
											var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
										//End
										compSubRecord.selectNewLineItem('inventoryassignment');
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
										nlapiLogExecution('ERROR', 'Serial num', serchRec[n].getValue('custrecord_serialnumber'));
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', serchRec[n].getValue('custrecord_serialnumber'));

										compSubRecord.commitLineItem('inventoryassignment');
										//compSubRecord.commit();
									}	*/
									//case # 20126900 End
								}
							}
							//case # 20126900 Start
							/*if(vAdvBinManagement)
								compSubRecord.commit();*/
							//case # 20126900 End
							serialnumcsv = tempSerial;
							tempSerial = "";
						}

						nlapiLogExecution('ERROR', 'Serial nums assigned', serialnumcsv);
						nlapiLogExecution('ERROR', 'Serial nums tempserialId', tempserialId);
						if(!vAdvBinManagement)
						{
							if (Itype == "serializedinventoryitem" || Itype == "serializedassemblyitem") {
								trecord.setCurrentLineItemValue('item', 'serialnumbers', serialnumArray);
							}
						}
					}
					else 
						if (Itype == "lotnumberedinventoryitem" || Itype=="lotnumberedassemblyitem" ) {
							nlapiLogExecution('ERROR', 'Into LOT/Batch');
							nlapiLogExecution('ERROR', 'LP:', invtlp);
							var tempBatch = "";
							var batchcsv = "";

							var confirmLotToNS='Y';

							confirmLotToNS=GetConfirmLotToNS(varItemlocation);

							nlapiLogExecution('ERROR', 'confirmLotToNS:', confirmLotToNS);


							/*var batfilter = new Array();
								batfilter[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2]);
								batfilter[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2]);
								batfilter[2] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', pointid);
								batfilter[3] = new nlobjSearchFilter('custrecord_line_no', null, 'is', linenum);
								batfilter[4] = new nlobjSearchFilter('custrecord_lpno', null, 'is', invtlp);

								var vatColumns=new Array();
								vatColumns[0] = new nlobjSearchColumn('custrecord_lotnowithquantity');
								vatColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
								var batchsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, batfilter, vatColumns);
								if (batchsearchresults) {
									for (var n = 0; n < batchsearchresults.length; n++) {
										//This is for Batch num loopin with Space separated.
										if (tempBatch == "") {
											if(confirmLotToNS == 'Y')
												tempBatch = batchsearchresults[n].getValue('custrecord_lotnowithquantity');
											else
											{

												var filters1 = new Array();          
												filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
												//Changed on 30/4/12 by suman
												filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
												//End of Chages as of 30/4/12
												var columns = new Array();

												columns[0] = new nlobjSearchColumn('itemid');
												var vItemname;
												var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
												if (itemdetails !=null) 
												{
													vItemname=itemdetails[0].getValue('itemid');
													nlapiLogExecution('ERROR','vItemName',vItemname);
													vItemname=vItemname.replace(/ /g,"-");
													var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
													tempBatch=vItemname + '('+ vItQty + ')';


												}
											}
										}
										else {
											if(confirmLotToNS == 'Y')
												tempBatch = tempBatch + " " + batchsearchresults[n].getValue('custrecord_lotnowithquantity');
											else
											{
												var filters1 = new Array();          
												filters1.push(new nlobjSearchFilter('internalid', null, 'is',item_id));
												//Changed on 30/4/12 by suman
												filters1.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
												//End of Chages as of 30/4/12
												var columns = new Array();

												columns[0] = new nlobjSearchColumn('itemid');
												var vItemname;
												var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
												if (itemdetails !=null) 
												{
													vItemname=itemdetails[0].getValue('itemid');
													nlapiLogExecution('ERROR','vItemName1',vItemname);
													vItemname=vItemname.replace(/ /g,"-");
													var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
													tempBatch=tempBatch + " " + vItemname + '('+ vItQty + ')';


												}
											}

										}
									}

									batchcsv = tempBatch;
									tempBatch = "";
								}*/
							var vItemname;

							if(confirmLotToNS == 'Y')
								tempBatch = batchno + '('+ quantity + ')';
							else
							{
								nlapiLogExecution('ERROR', 'item_id:', item_id);
								var filters1 = new Array();          
								filters1[0] = new nlobjSearchFilter('internalid', null, 'is',item_id);
								var columns = new Array();

								columns[0] = new nlobjSearchColumn('itemid');

								var itemdetails = new nlapiSearchRecord('item', null, filters1, columns);
								if (itemdetails !=null) 
								{
									vItemname=itemdetails[0].getValue('itemid');
									nlapiLogExecution('ERROR', 'vItemname:', vItemname);
									//var vItQty=batchsearchresults[0].getValue('custrecord_expe_qty');
									vItemname=vItemname.replace(/ /g,"-");
									tempBatch=vItemname + '('+ quantity + ')';


								}
							}
							if(!vAdvBinManagement)
							{	
								batchcsv = tempBatch;
								nlapiLogExecution('ERROR', 'LOT/Batch nums assigned', batchcsv);
								trecord.setCurrentLineItemValue('item', 'serialnumbers', batchcsv);
								var expdate1=getLotExpdate(batchno,item_id);
								if(expdate1 !=null && expdate1 !='')
								{
									trecord.setCurrentLineItemValue('item', 'expirationdate', expdate1);

								}
							}
							else
							{
								//case # 20126900 Start
								/*
								var compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');
								compSubRecord.selectNewLineItem('inventoryassignment');
								//case start 20125875
								compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', quantity);
								//end
								if(confirmLotToNS == 'Y')
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', batchno);
								else
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
								compSubRecord.commitLineItem('inventoryassignment');
								compSubRecord.commit();*/
								//case # 20126900 End
								nlapiLogExecution('ERROR', 'quantity', quantity);
								nlapiLogExecution('ERROR', 'batchno', batchno);
								nlapiLogExecution('ERROR', 'vItemname', vItemname);

								var compSubRecord = trecord.viewCurrentLineItemSubrecord('item','inventorydetail');

								if(compSubRecord == null || compSubRecord == ''){
									compSubRecord = trecord.createCurrentLineItemSubrecord('item','inventorydetail');

									var complinelength = compSubRecord.getLineItemCount('inventoryassignment');
									if(parseInt(complinelength)>0)
									{
										var d1=1;
										for(var r1=1;r1<=complinelength;r1++)
										{ 

											var lot=compSubRecord.getLineItemValue('inventoryassignment', 'receiptinventorynumber',d1);
											if(batchno!=lot && lot!=null && lot!='' )
												compSubRecord.removeLineItem('inventoryassignment', d1);
											else
												d1++;

										}
									}
									compSubRecord.selectLineItem('inventoryassignment',1);
									compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', quantity);
									var expdate1=getLotExpdate(batchno,item_id);
									if(expdate1 !=null && expdate1 !='')
									{
										trecord.setCurrentLineItemValue('item', 'expirationdate', expdate1);

									}
									if(confirmLotToNS == 'Y')
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', batchno);
									else
										compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', vItemname);
									compSubRecord.commitLineItem('inventoryassignment');
									compSubRecord.commit();
								}
							}	
						}
					// case no 20126691
					trecord.setCurrentLineItemValue('item', 'custcol_lp', invtlp);
					if(trecord!=null  && trecord!='')
						trecord.commitLineItem('item');

				}
				//Code Added by Ramana
				/*}
				else
				{
					trecord=null;
					idl=pointid;
				}*/	
			}
			if (commitflag == 'N' && trecord != null && trecord != '') {
				nlapiLogExecution('ERROR', 'commitflag is N', commitflag);
				trecord.selectLineItem('item', j);
				trecord.setCurrentLineItemValue('item', 'itemreceive', 'F');
				trecord.commitLineItem('item');
			}
		}
		nlapiLogExecution('ERROR', 'Before Item Receipt---', 'Before');

		if(trecord != null && trecord != '')
			idl = nlapiSubmitRecord(trecord);

		if(TranferInvFlag == 'T')
			inventorytransfer = InvokeNSInventoryTransfer(itemid,itemstatus,whLocation,varItemlocation,quantity,batchno,null,null);
		nlapiLogExecution('ERROR', 'Item Receipt idl', idl);

		return idl;
	}
	catch(exp)
	{
		var exceptionname=trantype;
		var functionality='Creating ItemReceipt';
		var trantype2=1;//1 for Inbound and 2 for outbound
		var vebizOrdNo=pointid;
		var vcontainerLp='';
		var POarray = new Array();
		nlapiLogExecution('ERROR', 'DetailsError', functionality);	
		nlapiLogExecution('ERROR', 'vcontainerLp', vcontainerLp);
		nlapiLogExecution('ERROR', 'vebizOrdNo35', vebizOrdNo);
		var reference2="";
		var reference3="";
		var reference4 ="";
		var reference5 ="";
		var alertType=1;//1 for exception and 2 for report
		errSendMailByProcess(trantype2,exp,vebizOrdNo,exceptionname,functionality,alertType,reference2,reference3,reference4,reference5);
		//InsertExceptionLog(exceptionname,trantype, functionality, exp, vebizOrdNo, vcontainerLp,reference3,reference4,reference5, userId);
		nlapiLogExecution('ERROR', 'Exception in TransformRec (PurchaseOrder) : ', exp);

		if (exp instanceof nlobjError) 
			nlapiLogExecution('ERROR', 'system error', exp.getCode() + '\n' + exp.getDetails());
		else 
			nlapiLogExecution('ERROR', 'unexpected error', exp.toString());

		nlapiLogExecution('ERROR', 'Updating PUTW task with end location - Record Id', RecordId);
		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordId);
		transaction.setFieldValue('custrecord_actendloc', EndLocationId);
		transaction.setFieldValue('custrecord_ebiz_error_log', exp);
		nlapiSubmitRecord(transaction, false, true);
		nlapiLogExecution('ERROR', 'PUTW task updated with act end location', EndLocationId);

		nlapiLogExecution('ERROR', 'Putaway Failed ');
		POarray["custparam_error"] = exp.getCode() + '\n' + exp.getDetails();
		response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
		return;
	}	
}



function UpdateOpenTaskWithExceptionValue(poId,vebizOrdNo,vcontainerLp,ErrorCode)
{
	try
	{
		nlapiLogExecution("ERROR","poId",poId);
		nlapiLogExecution("ERROR","vebizOrdNo",vebizOrdNo);
		nlapiLogExecution("ERROR","vcontainerLp",vcontainerLp);
		nlapiLogExecution("ERROR","ErrorCode",ErrorCode);

		var filters= new Array();	

		//if(SalesOrderInternalId != null && SalesOrderInternalId != '')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', poId)); 
		if(vcontainerLp != null && vcontainerLp != '')
			filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', vcontainerLp)); 

		//filters.push(new nlobjSearchFilter('name', null,'is', vebizOrdNo));
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['2'])); 
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['2']));		
		filters.push(new nlobjSearchFilter('custrecord_act_qty',  null,'isnot','0')); 
		filters.push(new nlobjSearchFilter('custrecord_act_qty',  null,'isnot','0.0'));

		var searchresult = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filters,null);
		if(searchresult!=null&&searchresult!="")
		{
			for ( var count = 0; count < searchresult.length; count++)
			{
				var recid=searchresult[count].getId();
				nlapiLogExecution("ERROR","recid",recid);
				nlapiSubmitField("customrecord_ebiznet_trn_opentask",recid,"custrecord_ebiz_error_log",ErrorCode);
			}
		}
	}
	catch(e)
	{
		nlapiLogExecution("ERROR","Exception in UpdateOpenTaskWithExceptionValue",e);
	}
}


//Case# 20123368 start, to get expiry date for Lot#

function getLotExpdate(vbatchno,itemid)
{
	try{
		nlapiLogExecution('ERROR', 'vbatchno', vbatchno);
		nlapiLogExecution('ERROR', 'itemid', itemid);
		var expdate='';
		var filterspor = new Array();
		filterspor.push(new nlobjSearchFilter('name', null, 'is', vbatchno));
		if(itemid!=null && itemid!="")
			filterspor.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', itemid));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
		column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');

		var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
		if(receiptsearchresults!=null)
		{
			var getlotnoid= receiptsearchresults[0].getId();
			nlapiLogExecution('ERROR', 'getlotnoid', getlotnoid);
			expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');

		}
		nlapiLogExecution('ERROR', 'expdate', expdate);
		return expdate;
	}
	catch(e)
	{
		nlapiLogExecution('ERROR', 'expection in getLotExpdate', e);
	}

}

//Case# 20123368 end
