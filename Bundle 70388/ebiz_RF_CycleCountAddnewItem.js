/***************************************************************************
����������������������eBizNET
����������������eBizNET Solutions Inc
 ****************************************************************************
 *
 *� $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_CycleCountAddnewItem.js,v $
<<<<<<< ebiz_RF_CycleCountAddnewItem.js
 *� $Revision: 1.1.2.21.2.1 $
 *� $Date: 2015/01/14 13:36:46 $
 *� $Author: schepuri $
 *� $Name: t_eBN_2014_2_StdBundle_0_205 $
=======
 *� $Revision: 1.1.2.21.2.1 $
 *� $Date: 2015/01/14 13:36:46 $
 *� $Author: schepuri $
 *� $Name: t_eBN_2014_2_StdBundle_0_205 $
>>>>>>> 1.1.2.13
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_RF_CycleCountAddnewItem.js,v $
 *� Revision 1.1.2.21.2.1  2015/01/14 13:36:46  schepuri
 *� issue # 201411390
 *�
 *� Revision 1.1.2.21  2014/06/13 10:17:10  skavuri
 *� Case# 20148882 (added Focus Functionality for Textbox)
 *�
 *� Revision 1.1.2.20  2014/05/30 00:34:19  nneelam
 *� case#  20148622
 *� Stanadard Bundle Issue Fix.
 *�
 *� Revision 1.1.2.19  2014/02/04 15:46:12  sponnaganti
 *� case# 20127031 (passing itemid to other pages)
 *�
 *� Revision 1.1.2.18  2014/01/29 16:07:03  skavuri
 *� case# 20126987 ItemName  displayed in RF cyclecount confirmation instead of Item Id
 *�
 *� Revision 1.1.2.17  2014/01/09 14:52:37  rmukkera
 *� Case # 20126718
 *�
 *� Revision 1.1.2.16  2014/01/06 13:16:03  grao
 *� Case# 20126579 related issue fixes in Sb issue fixes
 *�
 *� Revision 1.1.2.15  2013/12/20 15:41:04  rmukkera
 *� Case # 20126437
 *�
 *� Revision 1.1.2.14  2013/12/12 06:38:05  grao
 *� Case# 20126335 related issue fixes in TSG Sb
 *�
 *� Revision 1.1.2.13  2013/12/10 13:06:54  schepuri
 *� 20126282
 *�
 *� Revision 1.1.2.12  2013/11/22 14:33:40  rmukkera
 *� Case# 20125747
 *�
 *� Revision 1.1.2.11  2013/11/12 06:40:38  skreddy
 *� Case# 20125616
 *� Afosa SB issue fix
 *�
 *� Revision 1.1.2.10  2013/11/08 16:20:30  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *�
 *� Epicuren and surftech issue fixes
 *�
 *� Revision 1.1.2.9  2013/11/01 12:51:20  rmukkera
 *� Case# 20124766
 *�
 *� Revision 1.1.2.8  2013/10/19 13:18:57  rmukkera
 *� Case # 20125161
 *�
 *� Revision 1.1.2.7  2013/10/11 14:12:07  rmukkera
 *� Case# 20124769�
 *�
 *� Revision 1.1.2.6  2013/10/10 15:47:13  rmukkera
 *� Case# 20124769�
 *�
 *� Revision 1.1.2.5  2013/09/16 15:36:58  rmukkera
 *� Case# 20124313
 *�
 *� Revision 1.1.2.4  2013/08/23 07:40:12  schepuri
 *� case no 20123895
 *�
 *� Revision 1.1.2.3  2013/08/14 06:31:12  grao
 *� TSG SB: Issue fixes for mismatching location(site) when retreiving the item status
 *�
 *� Revision 1.1.2.2  2013/08/03 21:16:08  snimmakayala
 *� Case# 201214994
 *� Cycle Count Issue Fixes
 *�
 *� Revision 1.1.2.1  2013/08/02 15:35:52  rmukkera
 *� add new item related changes
 *�
 *� Revision 1.7.2.4  2012/09/03 13:08:28  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Previous button is driven according to LP Required Rule Value.
 *�
 *� Revision 1.7.2.3  2012/07/31 06:57:42  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Depending Upon the System Rule is LP scan required or not will drive the user to scan LP.
 *�
 *� Revision 1.7.2.2  2012/03/16 14:08:54  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Disable-button functionality is been added.
 *�
 *� Revision 1.7.2.1  2012/02/21 13:22:48  schepuri
 *� CASE201112/CR201113/LOG201121
 *� function Key Script code merged
 *�
 *� Revision 1.7  2011/12/08 07:21:05  spendyala
 *� CASE201112/CR201113/LOG201121
 *� added expected item internal id to CCArray
 *�
 *
 ****************************************************************************/




/**
 * @author LN
 * @version
 * @date
 */
function CycleCountAddNewItem(request, response){
	
	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);
	if (request.getMethod() == 'GET') {

		nlapiLogExecution('ERROR', 'Into Request', 'Success');

		//	Get the PO# from the previous screen, which is passed as a parameter		
		var getPlanNo = request.getParameter('custparam_planno');
		nlapiLogExecution('ERROR', 'getPlanNo', getPlanNo);
		var getLocation=request.getParameter('custparam_begin_location_name');
		if(getLocation==null || getLocation=='null' || getLocation=='undefined')
		{
			getLocation='';
		}
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto', getPlanNo);
		filters[1] = new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [20]);
		var cols=new Array();

		cols[0]=new nlobjSearchColumn('custrecord_cyc_skip_task',null,'group').setSort(true);
		cols[1]=new nlobjSearchColumn('custrecord_inboundlocgroupid','custrecord_cycact_beg_loc','group').setSort();
		cols[2]=new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_cycact_beg_loc','group').setSort();
		cols[3]=new nlobjSearchColumn('custrecord_cycact_beg_loc',null,'group');
		cols[4]=new nlobjSearchColumn('custrecord_cyclesite_id',null,'group');
		

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, cols);
		var SiteLoc;
		if (searchresults != null && searchresults.length > 0) {

			var getBeginLocationId =searchresults[0].getValue('custrecord_cycact_beg_loc',null,'group');//CCRec.getFieldValue('custrecord_cycact_beg_loc');
			var getBeginLocationName = searchresults[0].getText('custrecord_cycact_beg_loc',null,'group');//CCRec.getFieldText('custrecord_cycact_beg_loc');
			 SiteLoc = searchresults[0].getValue('custrecord_cyclesite_id',null,'group');//CCRec.getFieldText('custrecord_cycact_beg_loc');
			nlapiLogExecution('ERROR', 'Location Name', getBeginLocationName);
		}
		//Case # 20125747 : start : spanish language conversion
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
		
		var st0,st1,st2,st3,st4,st5,st6,st7,st8,st9;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			
			st1 = "ENTER / SCAN UBICACI&#211;N";
			st2 = "ENTER / SCAN ART&#211;CULO";
			st3=  "ENTER / SCAN LP";
			st4 = "CONT";
			st5 = "ANTERIOR";
			
			
						
		}
		else
		{   
			st1 = "ENTER/SCAN LOCATION";
			st2 = "ENTER/SCAN ITEM";
			st3 = "ENTER/SCAN LP";
			st4 = "CONT";
			st5 = "PREV";
								
		}
		
		
		//case # 20125747 end
		var functionkeyHtml=getFunctionkeyScript('_rf_cyclecountlocation'); 
		var html = "<html><head><title>CYCLE COUNT</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";   
		
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		
		
		//html = html + " document.getElementById('enterlocation').focus();";
		

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		//html = html + "	  alert(document.getElementById('cmdSend').disabled);";

		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";


		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cyclecountlocation' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";		
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +"</td></tr>";
		html = html + "				<tr><td align = 'left'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlocation' id='enterlocation' type='text' value='"+getLocation+"' />";
		html = html + "				<input type='hidden' name='hdnsiteloc' value=" + SiteLoc + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st2 +"";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entersku' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 +":";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlP' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st4 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		//html = html + "					SKIP <input name='cmdSkip' type='submit' value='F8' style='width:40;height:40;'/>";
		html = html + "					" + st5 + " <input name='cmdPrevious' type='submit' value='F7'/>";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlocation').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);

	}
	else {
		try {
			
		
			
			
			var getCycleCountLocation = request.getParameter('enterlocation');
			nlapiLogExecution('ERROR', 'Location', getCycleCountLocation);
			var getSite = request.getParameter('hdnsiteloc');
			nlapiLogExecution('ERROR', 'getSite', getSite);
			var getCycleCountPlanNo = request.getParameter('custparam_planno');
			nlapiLogExecution('ERROR', 'getPlanNo', getCycleCountPlanNo);

			var getActItem=request.getParameter('entersku');

			var CCarray = new Array();
			
			// case no 20126282

			var getLanguage = request.getParameter('hdngetLanguage');
			CCarray["custparam_language"] = getLanguage;
			
			
			var st6;
			if( getLanguage == 'es_ES')
			{

				st6= "ubicaci�n no v�lida";	

			}
			else
			{
				
				st6="INVALID LOCATION";
			}
			
			
			CCarray["custparam_error"] = st6;
			CCarray["custparam_screenno"] = '29A';
			CCarray["custparam_planno"] = getCycleCountPlanNo;
			//case 20125616  start : passing binlocaiton
			CCarray["custparam_begin_location_name"]=getCycleCountLocation;
			//case 20125616  end


			var filters = new Array();
			filters[0] = new nlobjSearchFilter('id', null, 'equalto', getCycleCountPlanNo);
			//filters[1] = new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [20]);
			var cols=new Array();
			cols[0]=new nlobjSearchColumn('custrecord_ebiz_cyclecntplan_location');	



			//consolidating the binlocations
			var searchresults = nlapiSearchRecord('customrecord_ebiznet_cylc_createplan', null, filters, cols);
			var getRecordId='';
			var expecqty=0;
			var lp=new Array();
			var whloc;
			nlapiLogExecution('ERROR', 'searchresultslength', searchresults);
			if (searchresults != null && searchresults.length > 0) {
				nlapiLogExecution('ERROR', 'searchresults', searchresults.length);

				whloc= searchresults[0].getValue('custrecord_ebiz_cyclecntplan_location');
				nlapiLogExecution('ERROR', 'whloc', whloc);
				//CCarray["custparam_noofrecords"] = searchresults.length;

			}

			var optedEvent = request.getParameter('cmdPrevious');
			//var optedEvent1 = request.getParameter('cmdSkip');
			//var optedEvent2 = request.getParameter('cmdaddnewitem');	

			if (sessionobj!=context.getUser()) {
				try
				{

					if(sessionobj==null || sessionobj=='')
					{
						sessionobj=context.getUser();
						context.setSessionObject('session', sessionobj); 
					}
					if (optedEvent == 'F7') {
						nlapiLogExecution('ERROR', 'Cycle Count Location F7 Pressed');
						response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_location', 'customdeploy_rf_cyclecount_location_di', false, CCarray);
					}


			/*else if(optedEvent1=='F8')
			{
				nlapiLogExecution('ERROR','SKIP','SKIPthetask');
				nlapiLogExecution('ERROR', 'getRecordId', CCarray["custparam_recordid"]);

				var CCRec = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe', request.getParameter('custparam_recordid'));
				var skipcount=CCRec.getFieldValue('custrecord_cyc_skip_task');
				nlapiLogExecution('ERROR','skipcount',skipcount);

				if(skipcount=='' || skipcount==null)
				{
					skipcount=0;
				}
				skipcount=parseInt(skipcount)+1;
				CCRec.setFieldValue('custrecord_cyc_skip_task',skipcount);
				var id=	nlapiSubmitRecord(CCRec,true);
				nlapiLogExecution('ERROR','skipid',id);

				var filters2 = new Array();
				filters2[0] = new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto', getCycleCountPlanNo);
				filters2[1] = new nlobjSearchFilter('custrecord_cycle_act_qty', null, 'isempty');
				filters2[2] = new nlobjSearchFilter('custrecord_cycact_beg_loc', null, 'anyof', CCarray["custparam_begin_location_id"]);
				//filters2[3] = new nlobjSearchFilter('internalid', null, 'notequalto', request.getParameter('custparam_recordid'));

				var columns = new Array();
				columns[0]=new nlobjSearchColumn('custrecord_cyc_skip_task');

				var searchresults2 = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters2, null);
				if(searchresults2 != null && searchresults2 != '')
				{
					for(var p=0;p<searchresults2.length;p++)
					{
						var skipcount=searchresults2[p].getValue('custrecord_cyc_skip_task');
						if(skipcount=='' || skipcount==null)
						{
							skipcount=0;
						}
						skipcount=parseInt(skipcount)+1;
						var RecUpdate = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe', searchresults2[p].getId());
						RecUpdate.setFieldValue('custrecord_cyc_skip_task',skipcount);
						var recid = nlapiSubmitRecord(RecUpdate, false, true);
						nlapiLogExecution('ERROR', 'updated remaining id',recid);
					}	
				}	

				response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_location', 'customdeploy_rf_cyclecount_location_di', false, CCarray);
				return;
			}*/
			else {
				if (request.getParameter('enterlocation') != '') {
					nlapiLogExecution('ERROR', 'custparam_begin_location_name', CCarray["custparam_begin_location_name"]);
					var ruleValue=GetSystemRuleForLPRequired();
					nlapiLogExecution('ERROR', 'ruleValue',ruleValue);
					CCarray["custparam_ruleValue"] = ruleValue;


					nlapiLogExecution('ERROR', 'ruleValue',ruleValue);

					//Case # 20124766� Start.
					/*var lp = GetMaxLPNo(1, 1,whloc);
					nlapiLogExecution('ERROR', 'LP NOT FOUND');
					var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
					customrecord.setFieldValue('name', lp);//santosh
					customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', lp);//santosh 
					var rec = nlapiSubmitRecord(customrecord, false, true);*/
					var getActualLP = request.getParameter('enterlP');
					//Case # 20124766� Start.
				
					CCarray["custparam_expqty"] = expecqty;

					if (getActItem  != '') 
					{	
						nlapiLogExecution('ERROR','IF','chktpt');
						/*var filter=new Array();
							filter.push(new nlobjSearchFilter('name',null,'is',getActItem.toString()));

							var searchrec=nlapiSearchRecord('item',null,filter,null);*/

						// Changed On 30/4/12 by Suman
//						var filter=new Array();
//						filter.push(new nlobjSearchFilter('nameinternal', null, 'is', getActItem.toString()));
//						filter.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
//						var columns = new Array();
//						columns[0]=new nlobjSearchColumn('custitem_ebizdefskustatus');
//						var searchrec=nlapiSearchRecord('item',null,filter,columns);
						// End of Changes as On 30/4/12

						nlapiLogExecution('ERROR','getActItem',getActItem);
						var actItemid = validateSKUId(getActItem);
						nlapiLogExecution('ERROR', 'After validateSKU',actItemid);
						
						//Case # 20125161� Start.
						if(actItemid!=null && actItemid !="" && actItemid !="F")
						{
							//Case # 20125161� End.
							//Case # 20124766� Start.
							
							nlapiLogExecution('ERROR', 'Actual LP #', getActualLP);

							if(getActualLP=='' || getActualLP==null || getActualLP=='null')
							{
								
								var st7;
								if( getLanguage == 'es_ES')
								{

									st7="Pls Introduzca LP";

								}
								else
								{
									st7= "Pls Enter LP";
									
								}
								
								
								CCarray["custparam_error"] = st7;
								
								
								

								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
								nlapiLogExecution('DEBUG', 'Cycle Count invalid item');
								return false;
							}
							else
							{

								//nlapiLogExecution('ERROR', 'getsysItemLP', 'userdefined');
								var LPReturnValue = ebiznet_LPRange_CL(getActualLP, '2',whloc,1);// case#201411390
								if (LPReturnValue == true)
								{
									var LpStatus=CheckLpMaster(getActualLP);
									if(LpStatus=='N')
									{
										CCarray["custparam_actlp"] = getActualLP;
										nlapiLogExecution('ERROR', 'LpStatus is N');
										//response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_sku', 'customdeploy_rf_cyclecount_sku_di', false, CCarray);
									}
									else
									{
										nlapiLogExecution('ERROR', 'LpStatus is Y');
										
										
										
										var st7;
										if( getLanguage == 'es_ES')
										{

											st7="LP ya existe";

										}
										else
										{
											st7= "LP Already Exists";
											
										}
										
										
										CCarray["custparam_error"] = st7;
										
										
										
										
										response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
										nlapiLogExecution('ERROR', 'Cycle Count LP not found1');
										return false;
									}

								}
								else 
								{
									CCarray["custparam_actlp"] = getActualLP;
									
									
									var st7;
									if( getLanguage == 'es_ES')
									{

										
										st7="Rango Lp no v�lido";
									}
									else
									{
										st7= "Invalid Lp Range";
										
									}
									
									
									CCarray["custparam_error"] = st7;
									
									
									
									response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
									nlapiLogExecution('ERROR', 'Cycle Count LP not found2');
									return false;
								}

							}

							//Case # 20124766� End.

							var Actualitemid=actItemid[1];
							nlapiLogExecution('ERROR','ACTUAL ITEMID',Actualitemid);

							//var skustatus = searchrec[0].getValue('custitem_ebizdefskustatus');

							var skustatus;

							var fields=new Array();

							fields[0]=new nlobjSearchFilter('name', null, 'is', getCycleCountLocation);
							var sresult=nlapiSearchRecord('customrecord_ebiznet_location', null, fields, null);

							
							
							var filter=new Array(); 
							if(getSite!=null && getSite!='')
							filter.push(new nlobjSearchFilter('custrecord_ebizsitelocf',null, 'anyof', getSite));
							filter.push(new nlobjSearchFilter('name', null, 'is',getCycleCountLocation));
							var columns = new Array();
							columns[0] = new nlobjSearchColumn('name');	
																
							var Validbinlocres = nlapiSearchRecord('customrecord_ebiznet_location', null, filter, columns);
							var validbinloc;
							if(Validbinlocres!=null && Validbinlocres!='')											
								 validbinloc = Validbinlocres[0].getValue('name');
							
							var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
							customrecord.setFieldValue('custrecord_tasktype', 7);
							customrecord.setFieldValue('custrecordact_begin_date', DateStamp());//(parseInt(d.getMonth()) + 1) + '/' + (parseInt(d.getDate())) + '/' + d.getFullYear());
							customrecord.setFieldValue('custrecord_actualbegintime', TimeStamp()); //((curr_hour) + ":" + (curr_min) + " " + a_p));
							customrecord.setFieldValue('custrecord_expe_qty', '0');
							customrecord.setFieldValue('custrecord_lpno', getActualLP);
							customrecord.setFieldValue('custrecord_sku', Actualitemid);
							customrecord.setFieldValue('custrecord_ebiz_sku_no', Actualitemid);
							
							nlapiLogExecution('ERROR','Validbinlocres[0].getId()',Validbinlocres[0].getId());
							
							if(Validbinlocres!=null && Validbinlocres.length>0)
								customrecord.setFieldValue('custrecord_actbeginloc', Validbinlocres[0].getId());
							
							customrecord.setFieldValue('custrecord_ebiz_cntrl_no', getCycleCountPlanNo);					
							customrecord.setFieldValue('name', getCycleCountPlanNo);
							customrecord.setFieldValue('custrecord_site_id', whloc);	
							var opentaskRecId = nlapiSubmitRecord(customrecord);
							nlapiLogExecution('ERROR','opentaskRecId',opentaskRecId);
							var cyclecntrec = nlapiCreateRecord('customrecord_ebiznet_cyclecountexe');
							cyclecntrec.setFieldValue('custrecord_cycle_count_plan', getCycleCountPlanNo);
							cyclecntrec.setFieldValue('name', getCycleCountPlanNo);
							cyclecntrec.setFieldValue('custrecord_cyclerec_date', DateStamp());					
							cyclecntrec.setFieldValue('custrecord_cycrec_time', TimeStamp());
							//cyclecntrec.setFieldValue('custrecord_cyclesku',Actualitemid);
							cyclecntrec.setFieldValue('custrecord_cycleact_sku',Actualitemid);
							//cyclecntrec.setFieldValue('custrecord_cyclecount_exp_ebiz_sku_no', Actualitemid);
							cyclecntrec.setFieldValue('custrecord_cyclecount_act_ebiz_sku_no', Actualitemid);
							cyclecntrec.setFieldValue('custrecord_cycleexp_qty', '0');
							cyclecntrec.setFieldValue('custrecord_cyclelpno', getActualLP);
							cyclecntrec.setFieldValue('custrecord_cycact_lpno', getActualLP);
							cyclecntrec.setFieldValue('custrecord_cyclestatus_flag', 20);//status flag is 'R'
							if(Validbinlocres!=null && Validbinlocres.length>0)								
								cyclecntrec.setFieldValue('custrecord_cycact_beg_loc',Validbinlocres[0].getId());
							cyclecntrec.setFieldValue('custrecord_opentaskid', opentaskRecId);
							cyclecntrec.setFieldValue('custrecord_invtid', null);
							cyclecntrec.setFieldValue('custrecord_expcycleabatch_no',null);
							if(skustatus!=null && skustatus!='')
							cyclecntrec.setFieldValue('custrecord_expcyclesku_status', skustatus);
							cyclecntrec.setFieldValue('custrecord_expcyclepc', null);
							cyclecntrec.setFieldValue('custrecord_cyclesite_id', whloc);
							cyclecntrec.setFieldValue('custrecord_cycle_notes', 'fromaddnewitem');
							var cyclecntRecId = nlapiSubmitRecord(cyclecntrec);
							nlapiLogExecution('ERROR','cyclecntRecId',cyclecntRecId);
							CCarray["custparam_skustatus"] = skustatus;
							nlapiLogExecution('ERROR','ACTUAL ITEMID',Actualitemid);
							nlapiLogExecution('ERROR','CCarray["custparam_skustatus"]',CCarray["custparam_skustatus"]);

									// case# 20127031 starts (passing itemid to other pages)
									//CCarray["custparam_actitem"] = Actualitemid;
									CCarray["custparam_actitem"] =Actualitemid;
									// case# 20127031 end

									CCarray["custparam_recordid"] = cyclecntRecId;
									CCarray["custparam_planno"] = getCycleCountPlanNo;
									if(sresult!=null && sresult.length>0)
										CCarray["custparam_begin_location_id"] = sresult[0].getId();
									CCarray["custparam_begin_location_name"] = getCycleCountLocation;
									CCarray["custparam_lpno"] = getActualLP;
									CCarray["custparam_packcode"] = '';
									CCarray["custparam_expqty"] = '';
									//	CCarray["custparam_expitem"] = getActItem;
									//	CCarray["custparam_expitemId"] = Actualitemid;
									CCarray["custparam_expitemdescription"] = '';
									CCarray["custparam_expitemno"] = Actualitemid;
									CCarray["custparam_actlp"]=getActualLP;

							//nlapiLogExecution('ERROR', 'Actual Item/Actual ItemInternalID', getActItem+'/'+Actualitemid);
							//nlapiLogExecution('ERROR', 'CCarray["custparam_expiteminternalid"]', CCarray["custparam_expiteminternalid"]);
							response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_packcode', 'customdeploy_rf_cyclecount_packcode_di', false, CCarray);
						}
						else
						{
							
							
							
							var st7;
							if( getLanguage == 'es_ES')
							{

								
								st7="Art�culo Inv�lido";
							}
							else
							{
								st7= "INVALID ITEM";
								
							}
							
							
							CCarray["custparam_error"] = st7;
							
							
							
							nlapiLogExecution('ERROR','ELSE of invalid item','chktpt');
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
							nlapiLogExecution('DEBUG', 'Cycle Count invalid item');
							return false;
						}
					}
					else 
					{
						
						
						var st7;
						if( getLanguage == 'es_ES')
						{

							
							st7="PLS ENTER PUNTO";
						}
						else
						{

							st7= "PLS ENTER ITEM";
						}
						
						
						CCarray["custparam_error"] = st7;
						
						
						nlapiLogExecution('ERROR','ELSE','chktpt');
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
						nlapiLogExecution('DEBUG', 'Cycle Count SKU not found');
						return false;
					}


						}
						else {
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
							nlapiLogExecution('ERROR', 'Cycle Count Location not found');
							return false;
						}
					}
				}
				catch (e)  {
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
				} finally {					
					context.setSessionObject('session', null);
					nlapiLogExecution('DEBUG', 'finally','block');

				}
			}
			else
			{
				CCarray["custparam_screenno"] = '28';
				CCarray["custparam_error"] = 'ITEM ALREADY IN PROCESS';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
			}
		} 
		catch (e) {
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
			nlapiLogExecution('ERROR', 'Catching the Search Results', e);
		}
		nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
	}
}


function GetSystemRuleForLPRequired()
{
	try
	{
		var rulevalue='Y';
		var filter=new Array();
		filter.push(new nlobjSearchFilter('name',null,'is','Is LP Scan required for CycleCount?'));

		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizrulevalue');

		var searchresult=nlapiSearchRecord('customrecord_ebiznet_sysrules',null,filter,column);

		if(searchresult!=null&&searchresult!="")
			rulevalue=searchresult[0].getValue('custrecord_ebizrulevalue');
		nlapiLogExecution('ERROR','rulevalue',rulevalue);
		return rulevalue;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetSystemRuleForLPRequired',exp);
	}
}
function CheckLpMaster(vLp)
{
	try {
		//alert('HELLO');
		var vLpExists;
		nlapiLogExecution('ERROR', 'INTO MASTER LP INSERTION');
		var filtersmlp = new Array();
		filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', vLp);

		var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

		if (SrchRecord != null && SrchRecord.length > 0) {
			nlapiLogExecution('ERROR', 'LP FOUND');
			//alert('IF');
			vLpExists = 'Y';
		}
		else
		{

			nlapiLogExecution('ERROR', 'LP NOT FOUND');
			var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
			customrecord.setFieldValue('name', vLp);//santosh
			customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', vLp);//santosh 
			var rec = nlapiSubmitRecord(customrecord, false, true);
			//alert('ELSE');
			vLpExists = 'N';
		}

		return vLpExists;
	} 
	catch (e) {
		nlapiLogExecution('ERROR', 'Failed to Update/Insert into Master LP Record');
	}
}
