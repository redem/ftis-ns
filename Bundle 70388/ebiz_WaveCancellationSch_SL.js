/***************************************************************************
 eBizNET SOLUTIONS LTD
 ****************************************************************************
 *
 * $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/Attic/ebiz_WaveCancellationSch_SL.js,v $
 * $Revision: 1.1.2.4.2.1 $
 * $Date: 2015/07/06 17:47:02 $
 * $Author: grao $
 * $Name: t_eBN_2015_1_StdBundle_1_181 $
 *
 * DESCRIPTION
 * Functionality
 *
 * REVISION HISTORY
 * $Log: ebiz_WaveCancellationSch_SL.js,v $
 * Revision 1.1.2.4.2.1  2015/07/06 17:47:02  grao
 * 2015.2  issue fixes  201413372
 *
 * Revision 1.1.2.4  2014/05/27 07:36:58  snimmakayala
 * Case#: 20148135
 * Schedule Script Status
 *
 * Revision 1.1.2.3  2013/05/15 07:36:54  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.1.2.2  2013/05/13 11:16:15  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.1.2.1  2013/04/30 23:33:11  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.3.2.7  2012/07/18 15:09:59  gkalla
 * CASE201112/CR201113/LOG201121
 * Fine tuned
 *
 *
 ****************************************************************************/

/**
 * 
 * @param alreadyAddedList
 * @param currentValue
 * @returns {Boolean}
 */
function isValueAlreadyAdded(alreadyAddedList, currentValue){
	var alreadyAdded = false;

	for(var i = 0; i < alreadyAddedList.length; i++){
		if(alreadyAddedList[i] == currentValue){
			alreadyAdded = true;
			break;
		}
	}

	return alreadyAdded;
}

function isFulfillOrderLineAdded(fulfillOrderList, currentOrder, currentLine){
	var alreadyAdded = false;

	for(var i = 0; i < fulfillOrderList.length; i++){
		if(fulfillOrderList[i][0] == currentOrder && fulfillOrderList[i][1] == currentLine){
			alreadyAdded = true;
			break;
		}
	}

	return alreadyAdded;
}

/**
 * 
 * @param fulfillOrderField
 * @param waveListField
 * @param fulfillOrderList
 */
function addValuesToFields(OrderField, waveListField, fulfillOrderList, searchResult){
	nlapiLogExecution('ERROR', 'addValuesToFields', 'Start');
	var ordersAlreadyAddedList = new Array();
	var wavesAlreadyAddedList = new Array();

	if(searchResult != null && searchResult.length > 0){
		for(var j = 0; j < searchResult.length; j++){
			var openTaskRecords=searchResult[j];
			for(var i = 0; i < openTaskRecords.length; i++){
				var currentTask = openTaskRecords[i];
				var fulfillOrderNo = currentTask.getValue('name');
				var waveNo = currentTask.getValue('custrecord_ebiz_wave_no');
				var OrderNo=currentTask.getText('custrecord_ebiz_order_no');
				var ebizOrderNo=currentTask.getValue('custrecord_ebiz_order_no');
				if(fulfillOrderList != null){
					if(!isValueAlreadyAdded(ordersAlreadyAddedList, fulfillOrderNo)){
//						nlapiLogExecution('ERROR', 'Adding fulfillment order', fulfillOrderNo);
						fulfillOrderList.addSelectOption(fulfillOrderNo, fulfillOrderNo);
						ordersAlreadyAddedList.push(fulfillOrderNo);
					}
				}
				if(OrderField != null){
					if(!isValueAlreadyAdded(ordersAlreadyAddedList, ebizOrderNo)){
//						nlapiLogExecution('ERROR', 'Adding fulfillment order', OrderNo);
						OrderField.addSelectOption(ebizOrderNo, OrderNo);
						ordersAlreadyAddedList.push(ebizOrderNo);
					}
				}
				if(waveListField != null)
				{
					if(!isValueAlreadyAdded(wavesAlreadyAddedList, waveNo)){
//						nlapiLogExecution('ERROR', 'Adding wave number', parseInt(waveNo));
						waveListField.addSelectOption(waveNo, waveNo);
						wavesAlreadyAddedList.push(waveNo);
					}
				}
			}
		}
	}
}

/**
 * 
 * @param levelInd
 * @returns {Array}
 */
function getFulfillOrderColumns(levelInd){
	var columns = new Array();

	if(levelInd == 'HEADER_LEVEL'){
		columns[0] = new nlobjSearchColumn('custrecord_ns_ord');
		columns[1] = new nlobjSearchColumn('custrecord_lineord');
		columns[2] = new nlobjSearchColumn('custrecord_ordline');
		columns[3] = new nlobjSearchColumn('custrecord_ebiz_wave');
		columns[0].setSort(true);
	} else if(levelInd == 'DETAIL_LEVEL') {
		columns[0] = new nlobjSearchColumn('custrecord_ns_ord');
		columns[1] = new nlobjSearchColumn('custrecord_lineord');
		columns[2] = new nlobjSearchColumn('custrecord_ordline');
		columns[3] = new nlobjSearchColumn('custrecord_ebiz_wave');
		columns[4] = new nlobjSearchColumn('custrecord_base_uom_id');
		columns[5] = new nlobjSearchColumn('custrecord_base_ord_qty');
		columns[6] = new nlobjSearchColumn('custrecord_base_pick_qty');
		columns[7] = new nlobjSearchColumn('custrecord_base_pickgen_qty');
		columns[8] = new nlobjSearchColumn('custrecord_base_ship_qty');
		columns[0].setSort(true);
	}

	return columns;
}

function getOpenTaskColumns(levelInd){
	var columns = new Array();

	if(levelInd == 'HEADER_LEVEL'){
		columns[0] = new nlobjSearchColumn('name');
		columns[1] = new nlobjSearchColumn('custrecord_line_no');
		columns[2] = new nlobjSearchColumn('custrecord_tasktype');
		columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		columns[4] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		columns[5] = new nlobjSearchColumn('custrecord_ebiz_receipt_no');
		columns[6] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
		columns[7] = new nlobjSearchColumn('custrecord_wms_status_flag');
		columns[8] = new nlobjSearchColumn('custrecord_container_lp_no');
		columns[9] = new nlobjSearchColumn('custrecord_sku');
		columns[6].setSort(true); // Sort by wave no.
	} else if(levelInd == 'DETAIL_LEVEL') {
		columns[0] = new nlobjSearchColumn('name');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		columns[2] = new nlobjSearchColumn('custrecord_sku');
		columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
		columns[4] = new nlobjSearchColumn('custrecord_container_lp_no');
		columns[5] = new nlobjSearchColumn('custrecord_ebiz_lpno');
		columns[6] = new nlobjSearchColumn('custrecord_line_no');
		columns[7] = new nlobjSearchColumn('custrecord_tasktype');
		columns[8] = new nlobjSearchColumn('custrecord_actbeginloc');
		columns[9] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
		columns[10] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		columns[11] = new nlobjSearchColumn('custrecord_ebiz_receipt_no');
		columns[12] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
		columns[13] = new nlobjSearchColumn('custrecordact_begin_date');
		columns[14] = new nlobjSearchColumn('custrecord_wms_status_flag');
		columns[15] = new nlobjSearchColumn('custrecord_from_lp_no');
		columns[16] = new nlobjSearchColumn('custrecord_actualbegintime');
		columns[17] = new nlobjSearchColumn('custrecord_invref_no');
		columns[18] = new nlobjSearchColumn('custrecord_packcode');
		columns[19] = new nlobjSearchColumn('custrecord_sku_status');
		columns[20] = new nlobjSearchColumn('custrecord_uom_id');
		columns[21] = new nlobjSearchColumn('custrecord_batch_no');
		columns[22] = new nlobjSearchColumn('custrecord_ebizrule_no');
		columns[23] = new nlobjSearchColumn('custrecord_ebizmethod_no');
		columns[24] = new nlobjSearchColumn('custrecord_ebizzone_no');
		columns[25] = new nlobjSearchColumn('custrecord_comp_id');
		columns[26] = new nlobjSearchColumn('custrecord_wms_location');						
		columns[27] = new nlobjSearchColumn('custrecord_ebizuser');		
		columns[28] = new nlobjSearchColumn('custrecord_upd_ebiz_user_no');						
		columns[29] = new nlobjSearchColumn('custrecord_taskassignedto');
		columns[12].setSort(true);  // Sort by wave number
	}

	return columns;
}

function getInvtDtlsForOneTask(pickTask){
	nlapiLogExecution('ERROR', 'getInvtDtlsRow', 'Start');

	var currentRow = new Array();

	var fulfillmentOrder = pickTask.getValue('name');
	var itemNo = pickTask.getValue('custrecord_ebiz_sku_no');
	var item = pickTask.getValue('custrecord_sku');
	var expectedQty = pickTask.getValue('custrecord_expe_qty');
	var containerLP = pickTask.getValue('custrecord_container_lp_no');
	var eBizLPNo = pickTask.getValue('custrecord_ebiz_lpno');
	var lineNo = pickTask.getValue('custrecord_line_no');
	var taskType = pickTask.getValue('custrecord_tasktype');
	var actBeginLocn = pickTask.getValue('custrecord_actbeginloc');
	var eBizControlNo = pickTask.getValue('custrecord_ebiz_cntrl_no');
	var waveNo = pickTask.getValue('custrecord_ebiz_wave_no');
	var wmsStatusFlag = pickTask.getValue('custrecord_wms_status_flag');
	var invtRefNo = pickTask.getValue('custrecord_invref_no');
	var itemStatus = pickTask.getValue('custrecord_sku_status');
	var uomId = pickTask.getValue('custrecord_uom_id');
	var wmsLocation = pickTask.getValue('custrecord_wms_location');
	var eBizOrderNo = pickTask.getValue('custrecord_ebiz_order_no');

	currentRow['fulfillmentOrder'] = fulfillmentOrder;				// Fulfillment Order
	currentRow['itemNo'] = itemNo;									// Item No.
	currentRow['item'] = item;										// Item
	currentRow['expectedQty'] = expectedQty;						// Expected Qty
	currentRow['containerLP'] = containerLP;						// Container LP
	currentRow['eBizLPNo'] = eBizLPNo;								// eBizNET LP No.
	currentRow['lineNo'] = lineNo;									// Order Line No.
	currentRow['taskType'] = taskType;								// Task Type
	currentRow['actBeginLocn'] = actBeginLocn;						// Actual Begin Location
	currentRow['eBizControlNo'] = eBizControlNo;					// eBizNET Control No
	currentRow['waveNo'] = waveNo;									// Wave No.
	currentRow['wmsStatusFlag'] = wmsStatusFlag;					// WMS Status Flag
	currentRow['invtRefNo'] = invtRefNo;							// Inventory Reference No.
	currentRow['itemStatus'] = itemStatus;							// Item Status
	currentRow['uomId'] = uomId;									// UOM ID
	currentRow['wmsLocation'] = wmsLocation;						// Warehouse Location
	currentRow['eBizOrderNo'] = eBizOrderNo;						// eBizNET Order No.

	nlapiLogExecution('ERROR', 'getInvtDtlsRow', 'End');
	return currentRow;
}

function getOpenTaskColumns(){
	nlapiLogExecution('ERROR', 'getOpenTaskColumns', 'Start');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('name');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	columns[2] = new nlobjSearchColumn('custrecord_sku');
	columns[3] = new nlobjSearchColumn('custrecord_expe_qty');
	columns[4] = new nlobjSearchColumn('custrecord_container_lp_no');
	columns[5] = new nlobjSearchColumn('custrecord_ebiz_lpno');
	columns[6] = new nlobjSearchColumn('custrecord_line_no');
	columns[7] = new nlobjSearchColumn('custrecord_tasktype');
	columns[8] = new nlobjSearchColumn('custrecord_actbeginloc');
	columns[9] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	columns[10] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[11] = new nlobjSearchColumn('custrecord_ebiz_receipt_no');
	columns[12] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
	columns[13] = new nlobjSearchColumn('custrecordact_begin_date');
	columns[14] = new nlobjSearchColumn('custrecord_wms_status_flag');
	columns[15] = new nlobjSearchColumn('custrecord_from_lp_no');
	columns[16] = new nlobjSearchColumn('custrecord_actualbegintime');
	columns[17] = new nlobjSearchColumn('custrecord_invref_no');
	columns[18] = new nlobjSearchColumn('custrecord_packcode');
	columns[19] = new nlobjSearchColumn('custrecord_sku_status');
	columns[20] = new nlobjSearchColumn('custrecord_uom_id');
	columns[21] = new nlobjSearchColumn('custrecord_batch_no');
	columns[22] = new nlobjSearchColumn('custrecord_ebizrule_no');
	columns[23] = new nlobjSearchColumn('custrecord_ebizmethod_no');
	columns[24] = new nlobjSearchColumn('custrecord_ebizzone_no');
	columns[25] = new nlobjSearchColumn('custrecord_comp_id');
	columns[26] = new nlobjSearchColumn('custrecord_wms_location');						
	columns[27] = new nlobjSearchColumn('custrecord_ebizuser');		
	columns[28] = new nlobjSearchColumn('custrecord_upd_ebiz_user_no');						
	columns[29] = new nlobjSearchColumn('custrecord_taskassignedto');
	columns[10].setSort(true);  // Sort by wave number
	columns[30] = new nlobjSearchColumn('id');
	columns[30].setSort(true);  
	nlapiLogExecution('ERROR', 'getOpenTaskColumns', 'End');
	return columns;
}

/**
 * Function to retrieve open task records for the specified wave number with
 * WMS STATUS FLAG having either of the values, STATUS.OUTBOUND.PICK_GENERATED or
 * STATUS.OUTBOUND.ORDER_PARTIALLY_PROCESSED or STATUS.OUTBOUND.ORDERLINE_PARTIALLY_LOADED
 * or STATUS.OUTBOUND.ORDERLINE_PARTIALLY_SHIPPED
 * 
 * @param waveNo
 * @returns [LIST OF OPEN TASK RECORDS]
 */
var searchResultArray=new Array();
function getOpenTaskRecordsForWave(waveNo,orderno,orderlineno,maxid){
	nlapiLogExecution('ERROR', 'getOpenTaskRecordsForWave', 'Start');
	nlapiLogExecution('ERROR', 'orderlineno', orderlineno);
	nlapiLogExecution('ERROR', 'waveNo', waveNo);
	// Filter open task records by wave number and wms_status_flag
	var filters = new Array();
	if(waveNo!=null && waveNo!='')
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', waveNo));
	}
	if(orderno!=null && orderno!='')
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', orderno));
	}
	if(orderlineno!=null && orderlineno!='')
	{
		filters.push(new nlobjSearchFilter('name', null, 'is', orderlineno));
	}
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','11','12','13','26']));
	if(maxid!=0)
		filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxid));
	var columns = getOpenTaskColumns();

	// Retrieve all open task records for the selected wave
	var openTaskRecords = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(openTaskRecords!=null && openTaskRecords.length>=1000)
	{ 
		searchResultArray.push(openTaskRecords); 
		var maxno=openTaskRecords[openTaskRecords.length-1].getValue(columns[30]);
		getOpenTaskRecordsForWave(waveNo,orderno,orderlineno,maxno);	
	}
	else
	{
		searchResultArray.push(openTaskRecords); 
	}
	logCountMessage('ERROR', openTaskRecords);
	nlapiLogExecution('ERROR', 'openTaskRecords', openTaskRecords.length);
	nlapiLogExecution('ERROR', 'getOpenTaskRecordsForWave', 'End');
	return searchResultArray;
}

function getOpenTaskRecordsForWavecancel(waveNo,orderno,orderlineno,taskid){
	nlapiLogExecution('ERROR', 'getOpenTaskRecordsForWavecancel', 'Start');
	var ebizordno=request.getParameter('custpage_orderlist');
	nlapiLogExecution('ERROR', 'waveNo', waveNo);
	nlapiLogExecution('ERROR', 'orderno', orderno);
	nlapiLogExecution('ERROR', 'orderlineno', orderlineno);
	nlapiLogExecution('ERROR', 'orderlineno', ebizordno);
	nlapiLogExecution('ERROR', 'taskid', taskid);
	// Filter open task records by wave number and wms_status_flag


	//var ebizordno=request.getParameter('custpage_orderlist');
	var filters = new Array();

	if(taskid!=null && taskid!='')
		filters.push(new nlobjSearchFilter('internalid', null, 'is', taskid));

	if(waveNo!=null && waveNo!='')
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', waveNo));
	}
	if(ebizordno!=null && ebizordno!='')
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', ebizordno));
	}
	if(orderno!=null && orderno!='')
	{
		filters.push(new nlobjSearchFilter('name', null, 'is', orderno));
	}
	if(orderlineno!=null && orderlineno!='')
	{
		filters.push(new nlobjSearchFilter('custrecord_line_no', null, 'is', orderlineno));
	}
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','11','12','13','26']));

	var columns = getOpenTaskColumns();

	// Retrieve all open task records for the selected wave
	var openTaskRecords = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	logCountMessage('ERROR', openTaskRecords);
	nlapiLogExecution('ERROR', 'openTaskRecords', openTaskRecords.length);
	nlapiLogExecution('ERROR', 'getOpenTaskRecordsForWavecancel', 'End');
	return openTaskRecords;
}

function getDtlsForInvtUpdate(openTaskRecords){
	nlapiLogExecution('ERROR', 'getDtlsForInvtUpdate', 'Start');

	var invtDtlsArray = new Array();

	if(openTaskRecords != null && openTaskRecords.length > 0){
		for(var i = 0; i < openTaskRecords.length; i++){
			var pickTask = openTaskRecords[i];
			var invtDtls = getInvtDtlsForOneTask(pickTask);

			invtDtlsArray.push(invtDtls);
		}
	}

	logCountMessage('ERROR', invtDtlsArray);
	nlapiLogExecution('ERROR', 'getDtlsForInvtUpdate', 'End');
	return invtDtlsArray;
}

/**
 * Function to retrieve the list of active fulfillment order records for 
 * the specified wave number
 * 
 * @param waveNo
 * @returns [LIST OF FULFILLMENT ORDER RECORDS]
 */
function getFulfillOrderInfo(waveNo,orderNo,orderlineNo){
	nlapiLogExecution('ERROR', 'getFulfillOrderInfo', 'Start');
	nlapiLogExecution('ERROR', 'waveNo', waveNo);
	nlapiLogExecution('ERROR', 'orderNo', orderNo);
	nlapiLogExecution('ERROR', 'orderlineNo', orderlineNo);

	var filters = new Array();
	if(waveNo!=null && waveNo!='')
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiz_wave', null, 'equalto', [waveNo]));
	}
	if(orderNo!=null && orderNo!='')
	{
		filters.push(new nlobjSearchFilter('name', null, 'is', orderNo));
	}
	if(orderlineNo!=null && orderlineNo!='')
	{
		filters.push(new nlobjSearchFilter('custrecord_ordline', null, 'equalto', [orderlineNo]));
	}

	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ns_ord');
	columns[1] = new nlobjSearchColumn('custrecord_lineord');
	columns[2] = new nlobjSearchColumn('custrecord_ordline');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_wave');
	columns[4] = new nlobjSearchColumn('custrecord_base_uom_id');
	columns[5] = new nlobjSearchColumn('custrecord_base_ord_qty');
	columns[6] = new nlobjSearchColumn('custrecord_base_pick_qty');
	columns[7] = new nlobjSearchColumn('custrecord_base_pickgen_qty');
	columns[8] = new nlobjSearchColumn('custrecord_base_ship_qty');
	columns[9] = new nlobjSearchColumn('custrecord_wave_status_flag');
	columns[10] = new nlobjSearchColumn('name');

	var fOrdSearchResults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
	var usage = getUnitsConsumed('nlapiSearchRecord');

	logCountMessage('ERROR', fOrdSearchResults);
	nlapiLogExecution('ERROR', 'getFulfillOrderInfo|USAGE', usage);
	nlapiLogExecution('ERROR', 'getFulfillOrderInfo', 'End');
	return fOrdSearchResults;
}

function getOrderNoArray(fulfillOrderList){
	nlapiLogExecution('ERROR', 'getOrderNoArray', 'Start');

	if(fulfillOrderList != null && fulfillOrderList.length > 0){
		for(var i = 0; i < fulfillOrderList.length; i++){

		}
	}
	nlapiLogExecution('ERROR', 'getOrderNoArray', 'End');	
}

function getFulfillOrderList(openTaskRecords){
	nlapiLogExecution('ERROR', 'getFulfillOrderList', 'Start');
	var fulfillOrderList = new Array();
	var orderNoList = new Array();
	var lineNoList = new Array();

	if(openTaskRecords != null && openTaskRecords.length > 0){
		for(var i = 0; i < openTaskRecords.length; i++){
			var pickTask = openTaskRecords[i];
			var fulfillOrderNo = pickTask.getValue('name');
			var lineNo = pickTask.getValue('custrecord_line_no');
			if(!isValueAlreadyAdded(orderNoList, fulfillOrderNo)){
				orderNoList.push(fulfillOrderNo);
			}

			if(!isFulfillOrderLineAdded(fulfillOrderList, fulfillOrderNo, lineNo)){
				var currentRow = new Array();
				currentRow['fulfillOrderNo'] = fulfillOrderNo;
				currentRow['lineNo'] = lineNo;
				lineNoList.push(currentRow);
			}
		}

		fulfillOrderList = [orderNoList, lineNoList];
	}

	logCountMessage('ERROR', fulfillOrderList);
	nlapiLogExecution('ERROR', 'getFulfillOrderList', 'End');
	return fulfillOrderList;
}

var searchResultArray=new Array();
function getAllOpenTaskRecordsForWaveCancel(levelInd, waveCancelLevel, waveNo,maxid){
	nlapiLogExecution('ERROR', 'getAllOpenTaskRecordsForWaveCancel', 'Start');

	var filters = new Array();
	// Look for valid wave#
	if((levelInd == 'HEADER_LEVEL') || (levelInd == 'DETAIL_LEVEL' && waveCancelLevel != 'WAVE')){
		filters[0] = new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'isnotempty');
		nlapiLogExecution('ERROR', 'Setting filter for non-empty wave no search');
	}else if(levelInd == 'DETAIL_LEVEL' && waveCancelLevel != 'WAVE'){
		filters[0] = new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'equals', waveNo);
		nlapiLogExecution('ERROR', 'Setting filter for wave no', waveNo);
	}

	// Look for records with any of the following statuses
	// STATUS.OUTBOUND.PICK_GENERATED = 9; STATUS.OUTBOUND.ORDER_PARTIALLY_PROCESSED = 11
	// STATUS.OUTBOUND.ORDER_PARTIALLY_LOADED = 12; STATUS.OUTBOUND.ORDER_PARTIALLY_SHIPPED = 13
	filters[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['9','11','12','13','26']);
	filters[2] = new nlobjSearchFilter('id', null, 'greaterthan',maxid);
	// Get list of columns to retrieve based on the level of granularity
	var columns = getOpenTaskColumns(levelInd);

	// Load all the open task records as per filter criteria
	var openTaskRecords = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
	if(openTaskRecords!=null && openTaskRecords.length>=1000)
	{ 
		searchResultArray.push(openTaskRecords); 
		var maxno=openTaskRecords[openTaskRecords.length-1].getValue(columns[30]);
		getAllOpenTaskRecordsForWaveCancel(levelInd, waveCancelLevel, waveNo,maxno);	
	}
	else
	{
		searchResultArray.push(openTaskRecords); 
	}
	logCountMessage('ERROR', openTaskRecords);
	nlapiLogExecution('ERROR', 'openTaskRecords', openTaskRecords.length);
	nlapiLogExecution('ERROR', 'getAllOpenTaskRecordsForWaveCancel', 'End');
	return searchResultArray;
}

/**
 * 
 * @returns
 */
function getFulfillmentOrdersForWaveCancel(levelInd){
	nlapiLogExecution('ERROR', 'getFulfillmentOrdersForWaveCancel', 'Start');
	var filters = new Array();
	// Search for fulfillment orders with any of the following statuses
	// STATUS.OUTBOUND.PICK_GENERATED = 9; STATUS.OUTBOUND.ORDER_PARTIALLY_PROCESSED = 11
	// STATUS.OUTBOUND.ORDER_PARTIALLY_LOADED = 12; STATUS.OUTBOUND.ORDER_PARTIALLY_SHIPPED = 13
	filters[0] = new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', ['9','11','12','13','26']);

	// Get list of columns based on the level of granularity
	var columns = getFulfillOrderColumns('HEADER_LEVEL');

	// Retrieve fulfillment orders
	var fulfillOrders = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);

	logCountMessage('ERROR', fulfillOrders);
	nlapiLogExecution('ERROR', 'getFulfillmentOrdersForWaveCancel', 'End');
	return fulfillOrders;
}

function updateInventoryForTask(invtRefNo, expectedQty){
	nlapiLogExecution('ERROR', 'updateInventoryForWaveCancel', 'Start');
	nlapiLogExecution('ERROR', 'invtRefNo', invtRefNo);
	var inventoryRecord = null;
	try
	{
		inventoryRecord = nlapiLoadRecord('customrecord_ebiznet_createinv', invtRefNo); 		// 2 UNITS

		if(inventoryRecord!=null && inventoryRecord!='')
		{
			//var usage = getUnitsConsumed('nlapiLoadRecord');
			var allocationQty = inventoryRecord.getFieldValue('custrecord_ebiz_alloc_qty');

			nlapiLogExecution('ERROR', 'Inventory Allocation Qty[PREV]', allocationQty);

			// Updating allocation qty
			if(parseInt(allocationQty) <= 0){
				allocationQty = parseInt(allocationQty) + parseInt(expectedQty);
			} 
			else if(parseInt(allocationQty) > 0){
				allocationQty = parseInt(allocationQty) - parseInt(expectedQty);
			}

			if(parseInt(allocationQty) <= 0){
				allocationQty=0;
			}

			inventoryRecord.setFieldValue('custrecord_ebiz_alloc_qty', parseInt(allocationQty));
			inventoryRecord.setFieldValue('custrecord_ebiz_callinv', 'N');
			nlapiSubmitRecord(inventoryRecord, false, true); 										// 2 UNITS
			//usage = parseInt(usage) + parseInt(getUnitsConsumed('nlapiSubmitRecord'));

			nlapiLogExecution('ERROR', 'Inventory Allocation Qty[NEW]', allocationQty);

			//nlapiLogExecution('ERROR', 'updateInventoryForWaveCancel|USAGE', usage);
			nlapiLogExecution('ERROR', 'updateInventoryForWaveCancel', 'End');

			// Total 4 UNITS
		}
	}
	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in Wave Cancelation ', exp);	
		//showInlineMessage(form, 'Error', 'Wave Cancelation Failed', "");
		//response.writePage(form);
	}


}

function removePickTasksAndUpdateInvt(openTaskRecords, InvtDetails,Adjustfullfillqty){
	nlapiLogExecution('ERROR', 'removePickTasksAndUpdateInvt', 'Start');
	nlapiLogExecution('ERROR', 'openTaskRecords.length', openTaskRecords.length);
	nlapiLogExecution('ERROR', 'InvtDetails', InvtDetails);
	var usage = 0;
	if(openTaskRecords != null && openTaskRecords.length > 0){
		for(var i = 0; i < openTaskRecords.length; i++){
			// Check if it is a PICK task
			nlapiLogExecution('ERROR', 'TaskType', parseInt(openTaskRecords[i].getValue('custrecord_tasktype')));
			if(parseInt(openTaskRecords[i].getValue('custrecord_tasktype')) == parseInt(3)){
				var task = openTaskRecords[i];
				var recordId = task.getId();
				var invtRefNo = task.getValue('custrecord_invref_no');
				var expectedQty = task.getValue('custrecord_expe_qty');
				var fointrid = task.getValue('custrecord_ebiz_cntrl_no');
				nlapiLogExecution('ERROR', 'invtRefNo', invtRefNo);
				nlapiLogExecution('ERROR', 'expectedQty', expectedQty);
				// Update the inventory to reduce the allocation quantity
				if(invtRefNo!=null && invtRefNo!='')
				{
					updateInventoryForTask(invtRefNo, expectedQty);									// 4 UNITS
				}

				// Delete the open task record for PICK task
				var deletedId = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', recordId); 	// 4 UNITS
				nlapiLogExecution('ERROR', 'Deleting open task record', recordId);
				var unitsConsumed = getUnitsConsumed('nlapiDeleteRecord');
				//usage = parseInt(usage) + parseInt(unitsConsumed);
				updateFulfillOrderRecords(fointrid,expectedQty,Adjustfullfillqty); 					// 8 UNITS

				// Total 16 UNITS
			}
		}
	}

	//nlapiLogExecution('ERROR', 'removePickTasksAndUpdateInvt|USAGE', usage);
	nlapiLogExecution('ERROR', 'removePickTasksAndUpdateInvt', 'End');
}

function updateFulfillOrderRecords(fointrid,expectedQty,Adjustfullfillqty){
	nlapiLogExecution('ERROR', 'updateFulfillOrderRecords', 'Start');
	nlapiLogExecution('ERROR', 'Adjustfullfillqty', Adjustfullfillqty);
	nlapiLogExecution('ERROR', 'expectedQty', expectedQty);
	nlapiLogExecution('ERROR', 'fointrid', fointrid);
	if(fointrid != null && fointrid!=''){

		var FullfilmrntRecord = nlapiLoadRecord('customrecord_ebiznet_ordline', fointrid); // 2 UNITS
		var pickgenqty = FullfilmrntRecord.getFieldValue('custrecord_pickgen_qty');
		var pickqty = FullfilmrntRecord.getFieldValue('custrecord_pickqty');

		if(pickgenqty==null || pickgenqty=='' || isNaN(pickgenqty))
			pickgenqty=0;	

		if(pickqty==null || pickqty=='' || isNaN(pickqty))
			pickqty=0;	

		nlapiLogExecution('ERROR', 'total pickgenQty', pickgenqty);

		var rempickgenqty =  parseInt(pickgenqty) - parseInt(expectedQty);

		if(parseInt(rempickgenqty)<=parseInt(pickqty))
		{
			rempickgenqty=pickqty;
		}

		FullfilmrntRecord.setFieldValue('custrecord_pickgen_qty', parseInt(rempickgenqty));
		FullfilmrntRecord.setFieldValue('custrecord_linestatus_flag', 25); //Status Edit
		FullfilmrntRecord.setFieldValue('custrecord_ebiz_wave', ''); //Status Edit
		nlapiSubmitRecord(FullfilmrntRecord, false, true);	 								// 2 UNITS

		if(Adjustfullfillqty=='T')
		{
			nlapiLogExecution('ERROR', 'rempickgenqty', rempickgenqty);
			if(rempickgenqty=='0' || rempickgenqty==null)
			{
				nlapiLogExecution('ERROR', 'Deleting fullfillment  record', fointrid);
				var deletedId = nlapiDeleteRecord('customrecord_ebiznet_ordline', fointrid); // 4 UNITS

			}
		}		
	}
	nlapiLogExecution('ERROR', 'updateFulfillOrderRecords', 'End');

	//Total 8 UNITS
}

function executeWaveCancel(openTaskRecords, invtDtls,Adjustfullfillqty){
	nlapiLogExecution('ERROR', 'executeWaveCancel', 'Start');

	/*
	 * Remove all the PICK tasks from Open Task
	 * Reduce inventory by expected quantity
	 * Reduce pickgen quantity by expected quantity
	 * Update fulfillment order status
	 */
	removePickTasksAndUpdateInvt(openTaskRecords,invtDtls,Adjustfullfillqty); // 16 UNITS/TASK
	//updateFulfillOrderRecords(fOrdRecords,Adjustfullfillqty);


	nlapiLogExecution('ERROR', 'executeWaveCancel', 'End');
}


function buildWaveCancellationForm(form, request, response){
	nlapiLogExecution('ERROR', 'buildWaveCancellationForm', 'Start');

	var hiddenField = form.addField('custpage_hiddenfield', 'text', '').setDisplayType('hidden');
	hiddenField.setDefaultValue('T');
	var waveno;
	var waveListField = form.addField('custpage_wavelist', 'text', 'Wave #').setLayoutType('startrow', 'none');
	waveListField.setDisplayType('inline');
	if(request.getParameter('custpage_wavelist')!='' && request.getParameter('custpage_wavelist')!=null)
	{
		waveListField.setDefaultValue(request.getParameter('custpage_wavelist'));
		waveno=parseInt(request.getParameter('custpage_wavelist'));
	}
	var orderField = form.addField('custpage_orderlist', 'text', 'Order #').setLayoutType('startrow', 'none');
	orderField.setDisplayType('inline');
	var Order;
	if(request.getParameter('custpage_orderlist')!='' && request.getParameter('custpage_orderlist')!=null)
	{
		orderField.setDefaultValue(request.getParameter('custpage_orderlist'));
		Order=parseInt(request.getParameter('custpage_orderlist'));
	}
	var orderlineField = form.addField('custpage_orderlinelist', 'text', 'Fulfillment Order #').setLayoutType('startrow', 'none');
	orderlineField.setDisplayType('inline');
	var OrderLines;
	if(request.getParameter('custpage_orderlinelist')!='' && request.getParameter('custpage_orderlinelist')!=null)
	{
		orderlineField.setDefaultValue(request.getParameter('custpage_orderlinelist'));
		OrderLines=request.getParameter('custpage_orderlinelist');
	}

	var openTaskRecords = getOpenTaskRecordsForWave(waveno,Order,OrderLines,0);

	var orderlinecount=0;
	var prelinecount;
	var Ordercount=0;
	var vPrevOrd;
	var ordWaveno;
	var ParentOrdNo;
	var lineOrdNovalue;
	var fulfillordno;
	var Lineno;
	var prewaveno;
	var preordno;
	var precount;
	var orderline;
	var item;
	var ordqty;
	var pickgenqty;
	var taskid;

	if(openTaskRecords != null && openTaskRecords != "")
	{		
		setPagingForSublist(openTaskRecords,form);		

	}
	// Add submit button
	var submit = form.addSubmitButton('Cancel Wave');
}

function setPagingForSublist(openTaskRecords,form)
{

	nlapiLogExecution('ERROR', 'openTaskRecords.length', openTaskRecords.length);
	var sublist = form.addSubList("custpage_wavecancellist", "list", "Wave Cancellation List");

	sublist.addField("custpage_wavecancel", "checkbox", "Cancel").setDisplayType('hidden').setDefaultValue("T");
	sublist.addField("custpage_waveno", "text", "Wave #").setDisplayType('inline');		
	sublist.addField("custpage_orderno", "text", "Order #").setDisplayType('inline');
	sublist.addField("custpage_fulfillordno", "text", "Fulfillment Order #").setDisplayType('inline');
	sublist.addField("custpage_fulfillordlineno", "text", "Fulfillment Order Line #").setDisplayType('inline');
	sublist.addField("custpage_item", "text", "Item").setDisplayType('inline');
	sublist.addField("custpage_pickgenqty", "text", "Pickgen Qty").setDisplayType('inline');
	sublist.addField("custpage_waveordernovalue", "text", "Order").setDisplayType('hidden');
	sublist.addField("custpage_waveordernolinevalue", "text", "Order").setDisplayType('hidden');
	sublist.addField("custpage_waveadjustfullfillqty", "checkbox", "Adjust Fulfillment Order qty").setDisplayType('hidden');
	sublist.addField("custpage_taskid", "text", "Task Id").setDisplayType('hidden');
	sublist.addField("custpage_invref", "text", "Inv RefNo").setDisplayType('hidden');
	sublist.addField("custpage_fulfillno", "text", "FONo").setDisplayType('hidden');
	sublist.addField("custpage_contlp", "text", "ContLP").setDisplayType('hidden');
	var openTaskRecordsArray=new Array();	
	var waveno=parseInt(request.getParameter('custpage_wavelist'));
	var Order=parseInt(request.getParameter('custpage_orderlist'));
	var OrderLines=request.getParameter('custpage_orderlinelist');
	for(k=0;k<openTaskRecords.length;k++)
	{
		nlapiLogExecution('ERROR', 'openTaskRecords[k]', openTaskRecords[k]); 
		var openTasksearchresult = openTaskRecords[k];

		if(openTasksearchresult!=null)
		{
			nlapiLogExecution('ERROR', 'openTasksearchresult.length ', openTasksearchresult.length); 
			for(var j=0;j<openTasksearchresult.length;j++)
			{
				openTaskRecordsArray[openTaskRecordsArray.length]=openTasksearchresult[j];				
			}
		}
	}
	var test='';

	if(openTaskRecordsArray.length>0 && openTaskRecordsArray.length>500)
	{
		var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('hidden');
		pagesize.setDisplaySize(10,10);
		pagesize.setLayoutType('outsidebelow', 'startrow');
		var select= form.addField('custpage_selectpage','select', 'Select Records');	
		select.setLayoutType('outsidebelow', 'startrow');			
		select.setDisplaySize(200,30);
		if (request.getMethod() == 'GET'){
			pagesize.setDefaultValue("500");
			pagesizevalue=500;
		}
		else
		{
			if(request.getParameter('custpage_pagesize')!=null)
			{pagesizevalue= request.getParameter('custpage_pagesize');}
			else
			{pagesizevalue= 500;pagesize.setDefaultValue("500");}
		}
		//this is to add the pageno's to the dropdown.
		var len=openTaskRecordsArray.length/parseInt(pagesizevalue);
		for(var k=1;k<=Math.ceil(len);k++)
		{

			var from;var to;

			to=parseInt(k)*parseInt(pagesizevalue);
			from=(parseInt(to)-parseInt(pagesizevalue))+1;

			if(parseInt(to)>openTaskRecordsArray.length)
			{
				to=openTaskRecordsArray.length;
				test=from.toString()+","+to.toString(); 
			}

			var temp=from.toString()+" to "+to.toString();
			var tempto=from.toString()+","+to.toString();
			select.addSelectOption(tempto,temp);

		} 
		if (request.getMethod() == 'POST'){

			if(request.getParameter('custpage_selectpage')!=null ){

				select.setDefaultValue(request.getParameter('custpage_selectpage'));	

			}
			if(request.getParameter('custpage_pagesize')!=null ){

				pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	

			}
		}
	}
	else
	{
		pagesizevalue=parseInt(openTaskRecordsArray.length);
	}
	var minval=0;var maxval=parseInt(pagesizevalue);
	if(parseInt(pagesizevalue)>openTaskRecordsArray.length)
	{
		maxval=openTaskRecordsArray.length;
	}
	var selectno=request.getParameter('custpage_selectpage');
	nlapiLogExecution('ERROR', 'selectno ', selectno); 
	if(selectno!=null )
	{
		var selectedPage= request.getParameter('custpage_selectpage');
		var selectedPageArray=selectedPage.split(',');			
		var diff=parseInt(selectedPageArray[1])-(parseInt(selectedPageArray[0])-1);
		nlapiLogExecution('ERROR', 'diff',diff);

		var pagevalue=request.getParameter('custpage_pagesize');
		nlapiLogExecution('ERROR', 'pagevalue',pagevalue);
		if(pagevalue!=null)
		{

			if(parseInt(diff)==parseInt(pagevalue)|| test==selectno)
			{

				var selectedPageArray=selectno.split(',');	
				nlapiLogExecution('ERROR', 'selectedPageArray.length ', selectedPageArray.length);  
				minval=parseInt(selectedPageArray[0])-1;
				nlapiLogExecution('ERROR', 'selectedPageArray[0] ', selectedPageArray[0]);  
				maxval=parseInt(selectedPageArray[1]);
				nlapiLogExecution('ERROR', 'selectedPageArray[1] ', selectedPageArray[1]);  
			}
		}
	}
	form.setScript('customscript_inventoryclientvalidations');

	var count=1;
	for (var i = minval; i < maxval; i++) 
	{
		var openTaskRecord = openTaskRecordsArray[i];
		ordWaveno=openTaskRecord.getValue('custrecord_ebiz_wave_no');
		ParentOrdNo=openTaskRecord.getText('custrecord_ebiz_order_no');
		fulfillordno=openTaskRecord.getValue('name');
		orderline=openTaskRecord.getValue('custrecord_line_no');
		item=openTaskRecord.getText('custrecord_sku');
		pickgenqty=openTaskRecord.getValue('custrecord_expe_qty');
		lineOrdNovalue=openTaskRecord.getValue('custrecord_ebiz_order_no');
		taskid=openTaskRecord.getId();
		var vInvRef=openTaskRecord.getValue('custrecord_invref_no');
		var vFOID=openTaskRecord.getValue('custrecord_ebiz_cntrl_no');
		var vContLP=openTaskRecord.getValue('custrecord_container_lp_no');

		nlapiLogExecution('ERROR', 'orderline', orderline);

		form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_waveno', count,ordWaveno);		
		form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_orderno', count,ParentOrdNo);
		form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_fulfillordno', count,fulfillordno);
		form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_fulfillordlineno', count,orderline);
		form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_item', count,item);
		form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_pickgenqty', count,pickgenqty);		
		if((waveno!=null && waveno!='')||(OrderLines!=null && OrderLines!='') )
		{
			if((waveno!=null && waveno!=''))
			{
				form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_waveordernolinevalue', count,lineOrdNovalue);
			}

			form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_waveordernovalue', count,lineOrdNovalue);
		}

		form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_taskid', count,taskid);
		form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_invref', count,vInvRef);
		form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_fulfillno', count,vFOID);
		if(vContLP != null && vContLP != '')
			form.getSubList('custpage_wavecancellist').setLineItemValue('custpage_contlp', count,vContLP);

		count=count+1;
	}

}
function processWaveCancellation(waveNo,orderNo,orderlineNo,Adjustfullfillqty,fulfillordno,taskid){
	nlapiLogExecution('ERROR', 'processWaveCancellation', 'Start');
	nlapiLogExecution('ERROR', 'waveNo', waveNo);
	nlapiLogExecution('ERROR', 'orderNo',orderNo);
	nlapiLogExecution('ERROR', 'orderlineNo', orderlineNo);
	nlapiLogExecution('ERROR', 'fulfillordno', fulfillordno);

	// Get all open task records associated with this wave
	var openTaskRecords = getOpenTaskRecordsForWavecancel(waveNo,fulfillordno,orderlineNo,taskid);//10 UNITS
	nlapiLogExecution('ERROR', 'openTaskRecords', openTaskRecords.length);

	// Get all fulfillment order records for the selected wave no
	//var fOrdRecords = getFulfillOrderInfo(waveNo,orderNo,orderlineNo);

	// Get inventory adjustment details
	var invtDtls = getDtlsForInvtUpdate(openTaskRecords);

	// Get list of fulfillment orders to be updated
	//var fulfillOrderList = getFulfillOrderList(openTaskRecords);

	executeWaveCancel(openTaskRecords,invtDtls,Adjustfullfillqty);

	nlapiLogExecution('ERROR', 'processWaveCancellation', 'End');
}

/**
 * 
 * @param request
 * @param response
 */
function cancelWave(request, response){
	if(request.getMethod() == 'GET'){
		nlapiLogExecution('ERROR', 'cancelWave - GET', 'Start');
		var form = nlapiCreateForm('Wave Cancellation');
		buildWaveCancellationForm(form, request, response);
		nlapiLogExecution('ERROR', 'cancelWave - GET', 'End');
	} else if (request.getMethod() == 'POST'){
		nlapiLogExecution('ERROR', 'cancelWave - POST', 'Start');
		try
		{
			var form = nlapiCreateForm('Wave Cancellation');

			var context = nlapiGetContext();
			var currentUserID = context.getUser();


			var userselection = false;
			var CancelFlag;
			var lincount = request.getLineItemCount('custpage_wavecancellist');
			var waveNo;
			var orderNo;
			var Adjustfullfillqty;
			var orderlineNo;
			var fulfillordno;
			var taskid;
			var boolProcessWavecancellation="F";
			var vTaskIntIdArr=new Array();
			var vAdjFulfillCheckArr=new Array();
			var vInvRefArr=new Array();
			var vFOIdArr=new Array();
			//allow user to cancel the waves only when CancelWave button is clicked.
			if(request.getParameter('custpage_hiddenfield')!=null && request.getParameter('custpage_hiddenfield')!='' && request.getParameter('custpage_hiddenfield')!='F')
			{		
				if(lincount!=null && lincount!='')
				{
					nlapiLogExecution('ERROR', 'Remaining Usage 2', nlapiGetContext().getRemainingUsage());
					var cnt=0;
					var vtaskids='';
					for (var p = 1; p <= lincount; p++) 
					{
						CancelFlag = request.getLineItemValue('custpage_wavecancellist', 'custpage_wavecancel', p);
						waveNo = request.getLineItemValue('custpage_wavecancellist', 'custpage_waveno', p);
						orderNo = request.getLineItemValue('custpage_wavecancellist', 'custpage_waveordernovalue', p);
						orderlineNo = request.getLineItemValue('custpage_wavecancellist', 'custpage_fulfillordlineno', p);
						fulfillordno = request.getLineItemValue('custpage_wavecancellist', 'custpage_fulfillordno', p);
						Adjustfullfillqty=request.getLineItemValue('custpage_wavecancellist', 'custpage_waveadjustfullfillqty', p);
						var vFOId=request.getLineItemValue('custpage_wavecancellist', 'custpage_fulfillno', p);
						taskid=request.getLineItemValue('custpage_wavecancellist', 'custpage_taskid', p);
						var vInvRef=request.getLineItemValue('custpage_wavecancellist', 'custpage_invref', p);
						var vContLP=request.getLineItemValue('custpage_wavecancellist', 'custpage_contlp', p);

						if (CancelFlag == 'T') 
						{
							userselection = true;	        	
						}
						nlapiLogExecution('ERROR', 'moveFlag', CancelFlag);
						// Retrieve the wave no. that needs to be cancelled
						if (CancelFlag == 'T') {

							if(cnt==0)
							{
								vtaskids=taskid;
								cnt++;
							}
							else
							{
								vtaskids=vtaskids+','+taskid;
								cnt++;
							}							
						}
					}

					var param = new Array();
					param['custscript_canceltaskids'] = vtaskids;
					param['custscript_cancelwaveno'] = waveNo;
					nlapiScheduleScript('customscript_wavecancel_scheduler', null,param);
					boolProcessWavecancellation='T';
					
					var tranType = nlapiLookupField('transaction', orderNo, 'recordType');
					updateScheduleScriptStatus('WAVE CANCELLATION',currentUserID,'Submitted',waveNo,tranType);

					if(boolProcessWavecancellation=="T")
					{
						var msg = form.addField('custpage_messages', 'inlinehtml', null, null, null);
						msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Wave Cancellation process has been initiated successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
					}
					nlapiLogExecution('ERROR', 'cancelWave - POST', 'End');
				}
			}
			//buildWaveCancellationForm(form, request, response);
		}
		catch(exp) {
			nlapiLogExecution('ERROR', 'Exception in Wave Cancelation ', exp);	
			showInlineMessage(form, 'Error', 'Wave Cancelation Failed', "");
			response.writePage(form);
		}

	}

	response.writePage(form);
}

var searchResultArr=new Array();
function GetAllOpenTaskRecs(TaskIntIdArr){
	nlapiLogExecution('ERROR', 'Into GetAllOpenTaskRecs');

	nlapiLogExecution('ERROR', 'taskid', TaskIntIdArr);

	var loctaskids=new Array();

	var loctaskids=TaskIntIdArr.toString().split(',');

	nlapiLogExecution('ERROR', 'loctaskids',loctaskids);

	if(loctaskids != null && loctaskids != '' && loctaskids.length>0)
	{
		var filters = new Array();

		filters.push(new nlobjSearchFilter('internalid', null, 'anyof', loctaskids));

		var columns = getOpenTaskColumns();

		var openTaskRecords = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	}

	nlapiLogExecution('ERROR', 'Out of GetAllOpenTaskRecs');

	return openTaskRecords;
}

function WaveCancelTransaction(openTaskRecords, InvtDetails,AdjustfullfillqtyArr,FODetails,emp){
	nlapiLogExecution('ERROR', 'Into WaveCancelTransaction');
	nlapiLogExecution('ERROR', 'openTaskRecords.length', openTaskRecords.length);
	nlapiLogExecution('ERROR', 'InvtDetails', InvtDetails);
	var usage = 0;
	var vFORecNew=new Array();
	var vInvRecNew=new Array();
	var vFORecIdNew=new Array();
	var vInvRecIdNew=new Array();
	var vInvExpQtyNew=new Array();
	var vFOExpQtyNew=new Array();
	var vFOAllocQtyNew=new Array();
	var vInvRecIdManual=new Array();
	var vInvExpQtyManual=new Array();
	var vContLPMastArr=new Array();
	var vItemTextArr=new Array();
	if(openTaskRecords != null && openTaskRecords.length > 0){
		var otparent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent');
		var parentid = nlapiSubmitRecord(otparent); 
		var newParent = nlapiLoadRecord('customrecord_ebiz_throwaway_parent', parentid);
		for(var i = 0; i < openTaskRecords.length; i++){
			// Check if it is a PICK task
			nlapiLogExecution('ERROR', 'TaskType', parseInt(openTaskRecords[i].getValue('custrecord_tasktype')));
			if(parseInt(openTaskRecords[i].getValue('custrecord_tasktype')) == parseInt(3)){
				var task = openTaskRecords[i];
				var recordId = task.getId();
				//var invtRefNo = task.getValue('custrecord_invref_no');
				var expectedQty = task.getValue('custrecord_expe_qty');
				var fointrid = task.getValue('custrecord_ebiz_cntrl_no');
				nlapiLogExecution('ERROR', 'expectedQty', expectedQty);
				//nlapiLogExecution('ERROR', 'InvtDetails[0].getId()', InvtDetails[0].getId());
				var invtRefNo = task.getValue('custrecord_invref_no');
				var FORefNo = task.getValue('custrecord_ebiz_cntrl_no');
				var vContLP = task.getValue('custrecord_container_lp_no');
				var vItemText = task.getText('custrecord_sku');
				nlapiLogExecution('ERROR', 'FORefNo', FORefNo);
				nlapiLogExecution('ERROR', 'invtRefNo', invtRefNo);
				nlapiLogExecution('ERROR', 'vContLP', vContLP);
				if(vContLP != null && vContLP != '')
				{
					if(vContLPMastArr.indexOf(vContLP) == -1)
					{ 	 
						vContLPMastArr.push(vContLP);
						vItemTextArr.push(vItemText);
					}
					else
					{
						var contLPItemExists=false;
						for(var z=0; z< vContLPMastArr.length; z++)
						{
							if(vContLPMastArr[z]==vContLP && vItemTextArr[z] == vItemText)
							{
								contLPItemExists=true;
								break;
							}	
						}	
						if(contLPItemExists==false)
						{
							vContLPMastArr.push(vContLP);
							vItemTextArr.push(vItemText);
						}	
					}	

				}	
				if(invtRefNo != null && invtRefNo != '')
				{
					//InvtDetails=null;
					var vInvRec=GetInvDetails(invtRefNo,InvtDetails);

					nlapiLogExecution('ERROR', 'vInvRec', vInvRec);


					// Update the inventory to reduce the allocation quantity
					if(vInvRec!=null && vInvRec!='')
					{
						if(vInvRecIdNew.indexOf(invtRefNo) == -1)
						{
							vInvRecIdNew.push(invtRefNo);
							vInvRecNew.push(vInvRec);
							vInvExpQtyNew.push(expectedQty);
						}
						else
						{
							var vOldInvExpQty=vInvExpQtyNew[vInvRecIdNew.indexOf(invtRefNo)];
							if(vOldInvExpQty == null || vOldInvExpQty == '')
								vOldInvExpQty=0;
							vInvExpQtyNew[vInvRecIdNew.indexOf(invtRefNo)]=parseInt(expectedQty) + parseInt(vOldInvExpQty);
							//vInvExpQtyNew.push(parseInt(expectedQty) + parseInt(vOldInvExpQty));
						}
						//updateInventoryForTaskParent(vInvRec, expectedQty,emp,newParent);									// 4 UNITS
					}
					else
					{

						if(vInvRecIdManual.indexOf(invtRefNo) == -1)
						{
							vInvRecIdManual.push(invtRefNo);

							vInvExpQtyManual.push(expectedQty);
						}
						else
						{
							var vOldInvExpQtyManual=vInvExpQtyManual[vInvRecIdManual.indexOf(invtRefNo)];
							if(vOldInvExpQtyManual == null || vOldInvExpQtyManual == '')
								vOldInvExpQtyManual=0;
							vInvExpQtyManual[vInvRecIdManual.indexOf(invtRefNo)]=parseInt(expectedQty) + parseInt(vOldInvExpQtyManual);
							//vInvExpQtyNew.push(parseInt(expectedQty) + parseInt(vOldInvExpQty));
						}
					}	
				}
				if(FORefNo != null && FORefNo != '')
				{
					var vFORec=GetFODetails(FORefNo,FODetails);


					nlapiLogExecution('ERROR', 'vFORec', vFORec);
					if(vFORec!=null && vFORec!='')
					{

						if(vFORecIdNew.indexOf(FORefNo) == -1)
						{
							vFORecIdNew.push(FORefNo);
							vFORecNew.push(vFORec);
							vFOExpQtyNew.push(expectedQty);

							vFOAllocQtyNew.push(AdjustfullfillqtyArr[i]);
						}
						else
						{
							var vOldFOExpQty=vFOExpQtyNew[vFORecIdNew.indexOf(FORefNo)];
							if(vOldFOExpQty == null || vOldFOExpQty == '')
								vOldFOExpQty=0;
							vFOExpQtyNew[vFORecIdNew.indexOf(FORefNo)]=parseInt(expectedQty) + parseInt(vOldFOExpQty);
							if(AdjustfullfillqtyArr[i]=='T')
								vFOAllocQtyNew[vFORecIdNew.indexOf(FORefNo)]='T';
						}
					}
				}
			}
		}
		if(vInvRecIdNew != null && vInvRecIdNew != '')
		{	
			nlapiLogExecution('ERROR', 'vInvRecIdNew', vInvRecIdNew);
			for(var x=0;x<vInvRecIdNew.length;x++)
			{
				nlapiLogExecution('ERROR', 'vInvRecIdNew[x]', vInvRecIdNew[x]);
			}	
			nlapiLogExecution('ERROR', 'vInvExpQtyNew', vInvExpQtyNew);
			for(var t=0;t<vInvRecIdNew.length;t++)
			{
				nlapiLogExecution('ERROR', 'vInvRecIdNew[t]', vInvRecIdNew[t]);
				nlapiLogExecution('ERROR', 'vInvExpQtyNew[t]', vInvExpQtyNew[t]);
				if(vInvRecNew[t] != null)
					updateInventoryForTaskParent(vInvRecNew[t], vInvExpQtyNew[t],emp,newParent);	

			}
		}
		if((InvtDetails == null || InvtDetails == '') && vInvRecIdManual != null && vInvRecIdManual != '')
		{	
			nlapiLogExecution('ERROR', 'vInvRecIdManual', vInvRecIdManual);
			for(var x=0;x<vInvRecIdManual.length;x++)
			{
				nlapiLogExecution('ERROR', 'vInvRecIdManual[x]', vInvRecIdManual[x]);
			}	
			nlapiLogExecution('ERROR', 'vInvExpQtyManual', vInvExpQtyManual);
			for(var t=0;t<vInvRecIdManual.length;t++)
			{
				nlapiLogExecution('ERROR', 'vInvRecIdManual[t]', vInvRecIdManual[t]);
				nlapiLogExecution('ERROR', 'vInvExpQtyManual[t]', vInvExpQtyManual[t]);
				if(vInvRecIdManual[t] != null)
				{
					updateInventoryForTask(vInvRecIdManual[t], vInvExpQtyManual[t]);
				}	

			}
		}

		if(vFORecIdNew != null && vFORecIdNew != '')
		{	
			nlapiLogExecution('ERROR', 'vFORecIdNew', vFORecIdNew);
			nlapiLogExecution('ERROR', 'vFOExpQtyNew', vFOExpQtyNew);
			for(var s=0;s<vFORecIdNew.length;s++)
			{
				if(vFORecNew[s] != null)
					updateFOForTaskParent(vFORecNew[s], vFOExpQtyNew[s],vFOAllocQtyNew[s],newParent);	
			}
		}
		nlapiLogExecution('ERROR', 'vContLPMastArr', vContLPMastArr);
		if(vContLPMastArr != null && vContLPMastArr != '' && vItemTextArr != null && vItemTextArr != '')
		{
			for(var l=0;l<vContLPMastArr.length;l++)
			{
				var vTempLPNo=vContLPMastArr[l];
				var vTempItemText=vItemTextArr[l];
				if(vTempLPNo != "")
				{	
					var filters=new Array();
					filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_masterlp', null, 'is', vTempLPNo));
					filters.push(new nlobjSearchFilter('custrecord_ebiz_lpmaster_item', null, 'is', vTempItemText));
					var columns = new Array();
					columns[0]=new nlobjSearchColumn('custrecord_ebiz_lpmaster_site');
					columns[1]=new nlobjSearchColumn('name');
					columns[2]=new nlobjSearchColumn('custrecord_ebiz_lpmaster_company');
					columns[3] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lptype');
					columns[4]=new nlobjSearchColumn('custrecord_ebiz_lpmaster_sizeid');
					columns[5] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totwght');
					columns[6] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_totcube');
					columns[7] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_wmsstatusflag');
					columns[8] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_sscc');
					columns[9] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_controlno');
					columns[10] = new nlobjSearchColumn('custrecord_ebiz_cart_closeflag');
					columns[11] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_item');
					columns[12] = new nlobjSearchColumn('custrecord_ebiz_lp_seq');
					var vLPMastRec = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filters, columns);
					if(vLPMastRec != null && vLPMastRec != '')
					{
						for(var h=0;h<vLPMastRec.length;h++)
							updateLPMastParent(vLPMastRec[h],newParent);
						nlapiLogExecution('ERROR', 'Outof LP update', vLPMastRec);
					}	
				}
			}	
		}	
		nlapiSubmitRecord(newParent);
		for(var l = 0; l < openTaskRecords.length; l++){
			// Check if it is a PICK task
			nlapiLogExecution('ERROR', 'TaskType', parseInt(openTaskRecords[l].getValue('custrecord_tasktype')));
			if(parseInt(openTaskRecords[l].getValue('custrecord_tasktype')) == parseInt(3)){
				var task = openTaskRecords[l];
				var recordId = task.getId();
				// Delete the open task record for PICK task
				if(recordId != null && recordId !='')
					UpdatePickTaskToDel(recordId);
				nlapiLogExecution('ERROR', 'Deleted open task record', recordId);
			}
		}
	}
	nlapiLogExecution('ERROR', 'Remaining Usage 2', nlapiGetContext().getRemainingUsage());
	//nlapiLogExecution('ERROR', 'removePickTasksAndUpdateInvt|USAGE', usage);
	nlapiLogExecution('ERROR', 'removePickTasksAndUpdateInvt', 'End');
}

function updateInventoryForTaskParent(vInvRec, expectedQty,emp,newParent){
	nlapiLogExecution('ERROR', 'updateInventoryForWaveCancel', 'Start');
	nlapiLogExecution('ERROR', 'invtRefNo', vInvRec);
	nlapiLogExecution('ERROR', 'expectedQty', expectedQty);
	var inventoryRecord = null;
	try
	{
		var allocationQty = vInvRec.getValue('custrecord_ebiz_alloc_qty'); 
		nlapiLogExecution('ERROR', 'Inventory Allocation Qty[PREV]', allocationQty); 
		// Updating allocation qty
		if(parseInt(allocationQty) <= 0){
			allocationQty = parseInt(allocationQty) + parseInt(expectedQty);
		} 
		else if(parseInt(allocationQty) > 0){
			allocationQty = parseInt(allocationQty) - parseInt(expectedQty);
		}

		if(parseInt(allocationQty) <= 0){
			allocationQty=0;
		}

		newParent.selectNewLineItem('recmachcustrecord_ebiz_inv_parent');
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'id', vInvRec.getId());
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'name', vInvRec.getValue('name'));
		if(vInvRec.getValue('custrecord_ebiz_inv_binloc') != null && vInvRec.getValue('custrecord_ebiz_inv_binloc') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_binloc', vInvRec.getValue('custrecord_ebiz_inv_binloc'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_lp', vInvRec.getValue('custrecord_ebiz_inv_lp'));
		if(vInvRec.getValue('custrecord_ebiz_inv_sku') != null && vInvRec.getValue('custrecord_ebiz_inv_sku') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_sku', vInvRec.getValue('custrecord_ebiz_inv_sku'));
		if(vInvRec.getValue('custrecord_ebiz_inv_sku_status') != null && vInvRec.getValue('custrecord_ebiz_inv_sku_status') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_sku_status', vInvRec.getValue('custrecord_ebiz_inv_sku_status'));
		if(vInvRec.getValue('custrecord_ebiz_inv_packcode') != null && vInvRec.getValue('custrecord_ebiz_inv_packcode') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_packcode', vInvRec.getValue('custrecord_ebiz_inv_packcode'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_inv_qty', vInvRec.getValue('custrecord_ebiz_inv_qty'));
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_alloc_qty', allocationQty);
		newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_ebiz_callinv', 'N');
		if(emp != null && emp != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_updated_user_no', emp);
		else
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_updated_user_no', nlapiGetContext().getUser());
		if(vInvRec.getValue('custrecord_invttasktype') != null && vInvRec.getValue('custrecord_invttasktype') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_invttasktype', vInvRec.getValue('custrecord_invttasktype'));
		if(vInvRec.getValue('custrecord_wms_inv_status_flag') != null && vInvRec.getValue('custrecord_wms_inv_status_flag') != '')
			newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_inv_parent', 'custrecord_wms_inv_status_flag', vInvRec.getValue('custrecord_wms_inv_status_flag'));

		newParent.commitLineItem('recmachcustrecord_ebiz_inv_parent');
		nlapiLogExecution('ERROR', 'updateInventoryForWaveCancel', 'End');

	}
	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in Wave Cancelation Inv updation', exp);	
		//showInlineMessage(form, 'Error', 'Wave Cancelation Failed', "");
		//response.writePage(form);
	}


}

var searchInvArr=new Array();
function GetAllInvtDetails(InvRefId,maxid){
	nlapiLogExecution('ERROR', 'getInventoryRecordsForWavecancel', 'Start');
	//var ebizordno=request.getParameter('custpage_orderlist');

	nlapiLogExecution('ERROR', 'InvRefId', InvRefId);
	// Filter open task records by wave number and wms_status_flag

	if(InvRefId != null && InvRefId != '' && InvRefId.length>0)
	{	
		for(var z=0;z<InvRefId.length;z++)
			nlapiLogExecution('ERROR', 'InvRefId[z]', InvRefId[z]);
		//var ebizordno=request.getParameter('custpage_orderlist');
		var filters = new Array();

		filters.push(new nlobjSearchFilter('internalid', null, 'anyof', InvRefId));
		if(maxid!=0)
			filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxid));
		var columns = new Array();
		columns.push(new nlobjSearchColumn('id'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_alloc_qty'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_binloc'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_lp'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_sku'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_sku_status'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_packcode'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_qty'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_lot'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_fifo'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_inv_loc'));
		columns.push(new nlobjSearchColumn('custrecord_inv_ebizsku_no'));
		columns.push(new nlobjSearchColumn('custrecord_invt_ebizlp'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_qoh'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_expdate'));
		columns.push(new nlobjSearchColumn('custrecord_wms_inv_status_flag'));
		columns.push(new nlobjSearchColumn('custrecord_invttasktype'));
		columns.push(new nlobjSearchColumn('custrecord_outboundinvlocgroupid'));
		columns.push(new nlobjSearchColumn('custrecord_pickseqno'));
		columns.push(new nlobjSearchColumn('custrecord_inboundinvlocgroupid'));
		columns.push(new nlobjSearchColumn('custrecord_putseqno'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_avl_qty'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_transaction_no'));
		//columns.push(new nlobjSearchColumn('custrecord_ebiz_serialnumbers'));
		columns.push(new nlobjSearchColumn('name'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_invholdflg'));	
		columns[0].setSort();
		// Retrieve all open task records for the selected wave
		var InvRecs = new nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
		if(InvRecs!=null && InvRecs.length>=1000)
		{ 
			nlapiLogExecution('ERROR', 'InvRecords1', InvRecs.length);
			for(var p=0;p<InvRecs.length;p++)
				searchInvArr.push(InvRecs[p]); 
			var maxno=InvRecs[InvRecs.length-1].getValue(columns[0]);
			GetAllInvtDetails(InvRefId,maxno);	
		}
		else
		{
			nlapiLogExecution('ERROR', 'InvRecords2', InvRecs.length);
			for(var p=0;p<InvRecs.length;p++)
				searchInvArr.push(InvRecs[p]); 
		}
	}
	logCountMessage('ERROR', searchInvArr);
	nlapiLogExecution('ERROR', 'InvRecords', searchInvArr.length);
	nlapiLogExecution('ERROR', 'getInventoryRecordsForWavecancel', 'End');

	return searchInvArr;
}

function GetInvDetails(invtRefNo,InvtDetails)
{
	for(var p=0;InvtDetails != null && p<InvtDetails.length;p++)
	{
		if(InvtDetails[p].getId()==invtRefNo)
			return InvtDetails[p];
	}	
	return InvtDetails;
}

function UpdatePickTaskToDel(recordId)
{
	var deletedId = nlapiDeleteRecord('customrecord_ebiznet_trn_opentask', recordId); 	// 4 UNITS
	//nlapiLogExecution('ERROR', 'Deleting open task record', recordId);
}

var searchFOArr=new Array();
function GetAllFODetails(FORefId,maxid){
	nlapiLogExecution('ERROR', 'getFulfillmentRecordsForWavecancel', 'Start');
	//var ebizordno=request.getParameter('custpage_orderlist');

	nlapiLogExecution('ERROR', 'FORefId', FORefId);
	// Filter open task records by wave number and wms_status_flag

	if(FORefId != null && FORefId != '' && FORefId.length>0)
	{	
		//var ebizordno=request.getParameter('custpage_orderlist');
		var filters = new Array();

		filters.push(new nlobjSearchFilter('internalid', null, 'is', FORefId));
		if(maxid!=0)
			filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxid));
		var columns = new Array();
		columns.push(new nlobjSearchColumn('id'));
		columns.push(new nlobjSearchColumn('name'));
		columns.push(new nlobjSearchColumn('custrecord_base_uom_id'));
		columns.push(new nlobjSearchColumn('custrecord_base_ord_qty'));
		columns.push(new nlobjSearchColumn('custrecord_base_pick_qty'));
		columns.push(new nlobjSearchColumn('custrecord_base_pickgen_qty'));
		columns.push(new nlobjSearchColumn('custrecord_base_ship_qty'));
		columns.push(new nlobjSearchColumn('custrecord_batch_flag'));
		columns.push(new nlobjSearchColumn('custrecord_batch'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_lineord'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_linesku'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_wave'));
		columns.push(new nlobjSearchColumn('custrecord_lineord'));
		columns.push(new nlobjSearchColumn('custrecord_ordline'));
		columns.push(new nlobjSearchColumn('custrecord_ord_qty'));
		columns.push(new nlobjSearchColumn('custrecord_linepackcode'));
		columns.push(new nlobjSearchColumn('custrecord_pickgen_flag'));
		columns.push(new nlobjSearchColumn('custrecord_pickgen_qty'));
		columns.push(new nlobjSearchColumn('custrecord_pickqty'));
		columns.push(new nlobjSearchColumn('custrecord_ship_qty'));
		columns.push(new nlobjSearchColumn('custrecord_linesku'));
		columns.push(new nlobjSearchColumn('custrecord_linesku_status'));
		columns.push(new nlobjSearchColumn('custrecord_linestatus_flag'));
		columns.push(new nlobjSearchColumn('custrecord_lineuom_id'));
		columns.push(new nlobjSearchColumn('custrecord_back_order_flag'));
		columns.push(new nlobjSearchColumn('custrecord_serial'));
		columns.push(new nlobjSearchColumn('custrecord_invoiced_qty'));
		columns.push(new nlobjSearchColumn('custrecord_wave_status_flag'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_lp'));
		columns.push(new nlobjSearchColumn('custrecord_ns_ord'));
		columns.push(new nlobjSearchColumn('custrecord_do_order_priority'));
		columns.push(new nlobjSearchColumn('custrecord_do_order_type'));
		columns.push(new nlobjSearchColumn('custrecord_do_customer'));
		columns.push(new nlobjSearchColumn('custrecord_do_carrier'));
		columns.push(new nlobjSearchColumn('custrecord_do_item_family'));
		columns.push(new nlobjSearchColumn('custrecord_do_item_group'));
		columns.push(new nlobjSearchColumn('custrecord_fulfilmentiteminfo1'));
		columns.push(new nlobjSearchColumn('custrecord_fulfilmentiteminfo2'));
		columns.push(new nlobjSearchColumn('custrecord_fulfilmentiteminfo3'));
		columns.push(new nlobjSearchColumn('custrecord_ordline_wms_location'));
		columns.push(new nlobjSearchColumn('custrecord_ordline_company'));
		columns.push(new nlobjSearchColumn('custrecord_shipment_no'));
		columns.push(new nlobjSearchColumn('custrecord_do_wmscarrier'));
		columns.push(new nlobjSearchColumn('custrecord_printflag'));
		columns.push(new nlobjSearchColumn('custrecord_print_count'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_shipdate'));		
		columns.push(new nlobjSearchColumn('custrecord_ebiz_orderdate'));
		columns.push(new nlobjSearchColumn('custrecord_shipcomplete'));
		//columns.push(new nlobjSearchColumn('custrecord_ordline_pickmethod'));
		//columns.push(new nlobjSearchColumn('custrecord_ordline_pickstrategy'));
		columns.push(new nlobjSearchColumn('custrecord_nsconfirm_ref_no'));
		columns.push(new nlobjSearchColumn('custrecord_linenotes1'));
		columns.push(new nlobjSearchColumn('custrecord_linenotes2'));

		// Retrieve all open task records for the selected wave
		var FORecs = new nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
		if(FORecs!=null && FORecs.length>=1000)
		{ 
			for(var p=0;p<FORecs.length;p++)
				searchFOArr.push(FORecs[p]); 
			var maxno=FORecs[FORecs.length-1].getValue(columns[0]);
			GetAllFODetails(FORefId,maxno);	
		}
		else
		{
			for(var p=0;p<FORecs.length;p++)
				searchFOArr.push(FORecs[p]); 
		}
	}
	logCountMessage('ERROR', searchFOArr);
	nlapiLogExecution('ERROR', 'Fulfillment Results', searchFOArr.length);
	nlapiLogExecution('ERROR', 'getFulfillmentRecordsForWavecancel', 'End');

	return searchFOArr;
}
function GetFODetails(FORefNo,FODetails)
{
	for(var p=0;p<FODetails.length;p++)
	{
		if(FODetails[p].getId()==FORefNo)
			return FODetails[p];
	}	
	return FODetails;
}

function updateFOForTaskParent(vFORec, expectedQty,Adjustfullfillqty,newParent){
	nlapiLogExecution('ERROR', 'updateFulfillOrderRecords', 'Start');
	nlapiLogExecution('ERROR', 'Adjustfullfillqty', Adjustfullfillqty);
	nlapiLogExecution('ERROR', 'expectedQty', expectedQty);
	nlapiLogExecution('ERROR', 'fointridRec', vFORec);


	try
	{
		if(vFORec != null && vFORec != '')
		{
			if(Adjustfullfillqty!='T')
			{ 	 
				var pickgenqty = vFORec.getValue('custrecord_pickgen_qty');
				var fointrid=vFORec.getId();
				if(pickgenqty==null || pickgenqty=='' || isNaN(pickgenqty))
					pickgenqty=0;	
				nlapiLogExecution('ERROR', 'fointrid', fointrid);
				nlapiLogExecution('ERROR', 'total pickgenQty', pickgenqty);

				var rempickgenqty =  parseInt(pickgenqty) - parseInt(expectedQty);

				if(parseInt(rempickgenqty)<=0)
					rempickgenqty=0;

				newParent.selectNewLineItem('recmachcustrecord_ebiz_fo_parent');
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'id', fointrid);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_pickgen_qty', rempickgenqty);
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_linestatus_flag', '25');
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_ebiz_wave', '');
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'name', vFORec.getValue('name'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_ebiz_lineord', vFORec.getValue('custrecord_ebiz_lineord'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_ordline', vFORec.getValue('custrecord_ordline'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_lineord', vFORec.getValue('custrecord_lineord'));
				newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent', 'custrecord_ord_qty', vFORec.getValue('custrecord_ord_qty'));
				newParent.commitLineItem('recmachcustrecord_ebiz_fo_parent');
			}
			else
			{
				nlapiLogExecution('ERROR', 'Into Deletion of Fulfillment Order', 'End');
				var pickgenqty = vFORec.getValue('custrecord_pickgen_qty');
				var fointrid=vFORec.getValue(); 

				if(pickgenqty==null || pickgenqty=='' || isNaN(pickgenqty))
					pickgenqty=0;
				nlapiLogExecution('ERROR', 'total pickgenQty', pickgenqty);
				var rempickgenqty =  parseInt(pickgenqty) - parseInt(expectedQty);
				if(parseInt(rempickgenqty)<=0)
					rempickgenqty=0;
				nlapiLogExecution('ERROR', 'rempickgenqty', rempickgenqty);
				if(rempickgenqty=='0' || rempickgenqty==null)
				{
					nlapiLogExecution('ERROR', 'Deleting fullfillment  record', fointrid);
					var deletedId = nlapiDeleteRecord('customrecord_ebiznet_ordline', fointrid); // 4 UNITS

				}	

			}
			nlapiLogExecution('ERROR', 'updateInventoryForWaveCancel', 'End');
		} 
	}
	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in Wave Cancelation Inv updation', exp);	
		//showInlineMessage(form, 'Error', 'Wave Cancelation Failed', "");
		//response.writePage(form);
	}


}


function WaveCancelScript(type)
{
	nlapiLogExecution('ERROR','Into  Wave Cancellation Scheduler Script','');

	var context = nlapiGetContext();  
	var emp = context.getSetting('SCRIPT', 'custscript_ebiz_wavecancel_emp');
	var vTaskIntIdArr1 = context.getSetting('SCRIPT', 'custscript_ebiz_wavecancel_opentask');
	var vAdjFulfillCheckArr1 = context.getSetting('SCRIPT', 'custscript_ebiz_wavecancel_adjufulfill');
	var vInvRefArr1 = context.getSetting('SCRIPT', 'custscript_ebiz_wavecancel_inv');
	var vFOIdArr1 = context.getSetting('SCRIPT', 'custscript_ebiz_wavecancel_fulfill');


	var str = 'emp. = ' + emp + '<br>';
	str = str + 'vOpenTaskIntIdArr. = ' + vTaskIntIdArr1 + '<br>';
	str = str + 'vAdjyFulfillIntIdArr. = ' + vAdjFulfillCheckArr1 + '<br>';
	str = str + 'vInvRefArr. = ' + vInvRefArr1 + '<br>';
	str = str + 'vFOIdArr. = ' + vFOIdArr1 + '<br>';
	str = str + 'type. = ' + type + '<br>';	

	nlapiLogExecution('ERROR', 'Parameter Details', str);

	nlapiLogExecution('ERROR','Remaining Usage 1',context.getRemainingUsage());
	var vTaskIntIdArr=new Array();
	var vAdjFulfillCheckArr=new Array();
	var vInvRefArr=new Array();
	var vFOIdArr=new Array();
	if(vTaskIntIdArr1!= null && vTaskIntIdArr1 != '' && vTaskIntIdArr1.length>0)
	{
		vTaskIntIdArr=vTaskIntIdArr1.split(',');
		vAdjFulfillCheckArr=vAdjFulfillCheckArr1.split(',');
		if(vInvRefArr1 != null && vInvRefArr1 != '')
			vInvRefArr=vInvRefArr1.split(',');
		vFOIdArr=vFOIdArr1.split(',');
		nlapiLogExecution('ERROR', 'vTaskIntIdArr.length', vTaskIntIdArr.length);
		nlapiLogExecution('ERROR', 'vAdjFulfillCheckArr.length', vAdjFulfillCheckArr.length);
		nlapiLogExecution('ERROR', 'vInvRefArr.length', vInvRefArr.length);
		nlapiLogExecution('ERROR', 'vFOIdArr.length', vFOIdArr.length);

	}
	if(vTaskIntIdArr!= null && vTaskIntIdArr != '' && vTaskIntIdArr.length>0)
	{
		var vOpenTaskDets=GetAllOpenTaskRecs(vTaskIntIdArr,0);
		var vInvDets=new Array();
		if(vInvRefArr != null && vInvRefArr != '')
			vInvDets=GetAllInvtDetails(vInvRefArr,0);
		var vFODets=GetAllFODetails(vFOIdArr,0);
		if(vOpenTaskDets != null && vOpenTaskDets != '' && vOpenTaskDets.length>0)
		{
			// Get inventory adjustment details
			//var invtDtls = getDtlsForInvtUpdate(vOpenTaskDets,vInvRef);
			WaveCancelTransaction(vOpenTaskDets, vInvDets,vAdjFulfillCheckArr,vFODets,emp);
			boolProcessWavecancellation="T";
		}
	}

}

if (!Array.prototype.indexOf) {
	Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
		"use strict";

		if (this === void 0 || this === null) throw new TypeError();

		var t = Object(this);
		var len = t.length >>> 0;
		if (len === 0) return -1;

		var n = 0;
		if (arguments.length > 0) {
			n = Number(arguments[1]);
			if (n !== n) // shortcut for verifying if it's NaN
				n = 0;
			else if (n !== 0 && n !== (1 / 0) && n !== -(1 / 0)) n = (n > 0 || -1) * Math.floor(Math.abs(n));
		}

		if (n >= len) return -1;

		var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);

		for (; k < len; k++) {
			if (k in t && t[k] === searchElement) return k;
		}
		return -1;
	};
}

function updateLPMastParent(LPMastRec,newParent)
{	
	//nlapiLogExecution('ERROR','LPMastRec.getId()',LPMastRec.getId());
	newParent.selectNewLineItem('recmachcustrecord_ebiz_lp_parent');

	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent', 'id', LPMastRec.getId());
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','isinactive', 'T');           
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_lp', null);
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','name', LPMastRec.getValue('name'));
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_masterlp', '');
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_site', LPMastRec.getValue('custrecord_ebiz_lpmaster_site'));
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_company', LPMastRec.getValue('custrecord_ebiz_lpmaster_company'));
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_lptype', LPMastRec.getValue('custrecord_ebiz_lpmaster_lptype'));
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_sizeid',LPMastRec.getValue('custrecord_ebiz_lpmaster_sizeid'));
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_totwght',LPMastRec.getValue('custrecord_ebiz_lpmaster_totwght'));
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_totcube',LPMastRec.getValue('custrecord_ebiz_lpmaster_totcube'));
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_wmsstatusflag',LPMastRec.getValue('custrecord_ebiz_lpmaster_wmsstatusflag'));

	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_sscc', LPMastRec.getValue('custrecord_ebiz_lpmaster_sscc'));
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_controlno', LPMastRec.getValue('custrecord_ebiz_lpmaster_controlno'));
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_cart_closeflag', LPMastRec.getValue('custrecord_ebiz_cart_closeflag'));
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lpmaster_item', LPMastRec.getValue('custrecord_ebiz_lpmaster_item'));
	newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_lp_parent','custrecord_ebiz_lp_seq', LPMastRec.getValue('custrecord_ebiz_lp_seq'));
	//newParent.setCurrentLineItemValue('recmachcustrecord_ebiz_ot_parent','custrecord_ebiz_lpmaster_sscc', request.getLineItemValue('custpage_details', 'custpage_lp', k));


	newParent.commitLineItem('recmachcustrecord_ebiz_lp_parent'); 
}


function wavecancellationscheduler()
{
	nlapiLogExecution('ERROR','Into  wavecancellationscheduler','');

	var context = nlapiGetContext();  
	var taskids = context.getSetting('SCRIPT', 'custscript_canceltaskids');
	var ebizwaveno = context.getSetting('SCRIPT', 'custscript_cancelwaveno');
	//waveNo

	var str = 'taskids. = ' + taskids + '<br>';
	str = str + 'ebizwaveno. = ' + ebizwaveno + '<br>';
	str = str + 'type. = ' + type + '<br>';	

	nlapiLogExecution('ERROR', 'Parameter Details', str);

	nlapiLogExecution('ERROR','Remaining Usage at the START',context.getRemainingUsage());
	nlapiLogExecution('ERROR', 'Time Stamp at the START',TimeStampinSec());

	var currentUserID = context.getUser();

	updateScheduleScriptStatus('WAVE CANCELLATION',currentUserID,'In Progress',ebizwaveno,null);

	//var OpenTaskRecords=GetAllPickTaskRecs(null,ebizwaveno);
	var OpenTaskRecords=GetAllPickTaskRecs(taskids,ebizwaveno);

	if(OpenTaskRecords!=null && OpenTaskRecords!='')
	{
		nlapiLogExecution('ERROR', 'OpenTaskRecords Count', OpenTaskRecords.length);

		var vTaskIntIdArr = new Array();
		var vAdjFulfillCheckArr = new Array();
		var vFOIdArr = new Array();
		var vInvRefArr = new Array();

		for (var p = 0; p < OpenTaskRecords.length; p++) 
		{
			var vFOId=OpenTaskRecords[p].getValue('custrecord_ebiz_cntrl_no');		
			var vInvRef=OpenTaskRecords[p].getValue('custrecord_invref_no');	

			vTaskIntIdArr.push(OpenTaskRecords[p].getId());
			if(vFOId!=null && vFOId!='')
				vFOIdArr.push(vFOId);
			if(vInvRef!=null && vInvRef!='')
				vInvRefArr.push(vInvRef);

		}

		if(vTaskIntIdArr!= null && vTaskIntIdArr != '' && vTaskIntIdArr.length>0)
		{
			var vOpenTaskDets=GetAllOpenTaskRecs(vTaskIntIdArr);
			var vInvDets=GetAllInvtDetails(vInvRefArr,0);
			var vFODets=GetAllFODetails(vFOIdArr,0);
			if(vOpenTaskDets != null && vOpenTaskDets != '' && vOpenTaskDets.length>0)
			{
				var context = nlapiGetContext();
				var currentUserID = context.getUser();	
				WaveCancelTransaction(vOpenTaskDets, vInvDets,vAdjFulfillCheckArr,vFODets,currentUserID);
			}
		}	
	}

	updateScheduleScriptStatus('WAVE CANCELLATION',currentUserID,'Completed',ebizwaveno,null);

	nlapiLogExecution('ERROR', 'Time Stamp at the END',TimeStampinSec());
	nlapiLogExecution('ERROR','Remaining Usage at the END',context.getRemainingUsage());	
	nlapiLogExecution('ERROR','Out of  wavecancellationscheduler','');
}

function GetAllPickTaskRecs(TaskIntIdArr,ebizwaveno){
	nlapiLogExecution('ERROR', 'Into GetAllPickTaskRecs');

	nlapiLogExecution('ERROR', 'taskid', TaskIntIdArr);
	nlapiLogExecution('ERROR', 'ebizwaveno', ebizwaveno);

	var loctaskids=new Array();

	if(TaskIntIdArr!=null && TaskIntIdArr!='')
		loctaskids=TaskIntIdArr.toString().split(',');

	nlapiLogExecution('ERROR', 'loctaskids',loctaskids);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'contains', ebizwaveno));
	filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9,26]));

	if(loctaskids != null && loctaskids != '' && loctaskids.length>0)
	{
		filters.push(new nlobjSearchFilter('internalid', null, 'anyof', loctaskids));
	}

	var columns = getOpenTaskColumns();

	var openTaskRecords = new nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);

	nlapiLogExecution('ERROR', 'Out of GetAllPickTaskRecs');

	return openTaskRecords;
}