/***************************************************************************
 �	eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *  $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Suitelet/Attic/ebiz_OrderManagementPDF_SL.js,v $
 *� $Revision: 1.1.2.2.4.2.4.2 $
 *� $Date: 2013/03/19 12:08:11 $
 *� $Author: schepuri $
 *� $Name: t_NSWMS_2013_1_3_9 $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *
 *
 *****************************************************************************/
/**
 * This is the main function to Generate PDF and Excel  
 */
function OrderManagementPDF(request, response){
	if (request.getMethod() == 'GET') {

		try
		{

			var form = nlapiCreateForm('Order Management Report'); 
			var soid=request.getParameter('custparam_soid');
			var fromdate=request.getParameter('custparam_fromdate');
			var todate=request.getParameter('custparam_todate');
			var vTaskStatus=request.getParameter('custparam_taskstatus');
			var id=request.getParameter('custparam_id');

			nlapiLogExecution('ERROR','soid',soid);
			nlapiLogExecution('ERROR','fromdate',fromdate);
			nlapiLogExecution('ERROR','vTaskStatus',vTaskStatus);
			nlapiLogExecution('ERROR','todate',todate);
			nlapiLogExecution('ERROR','id',id);

			var vRecCount=0;
			var result;

			//1=FO Created but Wave not generated.
			//2=Wave Generated but Pick Report not printed.
			//3=Pick Report Printed but Pick yet to being.

			if(vTaskStatus==1||vTaskStatus==2||vTaskStatus==3)
			{
				if(soid=='undefined')
				{soid="";

				}

				result=OrderLineRecord(fromdate,todate,vTaskStatus,soid,-1);
				nlapiLogExecution('ERROR','vTaskStatus',vTaskStatus);
				nlapiLogExecution('ERROR','results',result.length);

				if(id=="1")
				{
					GeneratePDF(result,vTaskStatus);
				}
				else if(id=="2")
				{
					GenerateExcel(result,vTaskStatus);
				}
			}
			//4=Picking competed but item fulfillment not sent to NS
			else if(vTaskStatus==4)
			{
				if(soid=='undefined')
				{
					soid="";				
				}
				var Searchresult=Getpickcomplete_ItemFullFailedRecord(fromdate,todate,soid);
				if(Searchresult!=null&&Searchresult!="")
				{
					
					if(id=="1")
					{
						GeneratePDF(Searchresult,vTaskStatus);
					}
					else if(id=="2")
					{
						GenerateExcel(Searchresult,vTaskStatus);
					}

				}
				
				
			}
							
			//5=Pick Completed but no Shipping label; 
			//6=Shipping label generated but no Fulfillment confirmation
			else if(vTaskStatus==5||vTaskStatus==6)
			{
				if(soid=='undefined')
				{
					soid="";				
				}
				nlapiLogExecution('ERROR','vtaskstatus',vTaskStatus);
				var result=ShipManifestRecord(fromdate,todate,vTaskStatus,soid);
				if(result!=null&&result!="")
				{
					var uniqueSo=GetUniqueSO(result);
					var OpenTaskRec=GetOpenTaskRecord(uniqueSo,fromdate,todate);
					if(OpenTaskRec!=null&&OpenTaskRec!="")
					{
						if(id=="1")
						{
							GeneratePDF(OpenTaskRec,vTaskStatus);
						}
						else if(id=="2")
						{
							GenerateExcel(OpenTaskRec,vTaskStatus);
						}

					}
				}
								
			}
			//7=Pick Failed
			else if(vTaskStatus==7)
			{
				if(soid=='undefined')
				{soid="";				
				}
				var Searchresult=GetpickFailedRecord(fromdate,todate,vTaskStatus,soid);
				if(Searchresult!=null&&Searchresult!="")
				{
					if(id=="1")
					{
						GeneratePDF(Searchresult,vTaskStatus);
					}
					else if(id=="2")
					{
						GenerateExcel(Searchresult,vTaskStatus);
					}

				}
			}
			//8=Pack completed but item fulfillment is not posted to NS
			else if(vTaskStatus==8)
			{
				if(soid=='undefined')
				{soid="";				
				}
				var Searchresult=Getpackcomplete_ItemFullFailedRecord(fromdate,todate,vTaskStatus,soid);
				if(Searchresult!=null&&Searchresult!="")
				{
					if(id=="1")
					{
						GeneratePDF(Searchresult,vTaskStatus);
					}
					else if(id=="2")
					{
						GenerateExcel(Searchresult,vTaskStatus);
					}

				}
			}
			//9=Payment hold failed but picking completed
			else if(vTaskStatus==9)
			{
				if(soid=='undefined')
				{soid="";				
				}
				var Searchresult=PaymentHoldFailed_PickingCompleted(fromdate,todate,vTaskStatus,soid);
				if(Searchresult!=null&&Searchresult!="")
				{
					if(id=="1")
					{
						GeneratePDF(Searchresult,vTaskStatus);
					}
					else if(id=="2")
					{
						GenerateExcel(Searchresult,vTaskStatus);
					}

				}
			}
			
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR','Exception ',exp);
		}

	}
	else
	{

	}
}


/**
 * To generate To PDF 
 * @parameters  search results ,status value 
 */

function GeneratePDF(result,vTaskStatus)
{
	var ordno="";
	var sku="";
	var ordqty="";
	var nsOrdQty="";
	var Customer="";
	var url;
	/*var ctx = nlapiGetContext();
	nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
	if (ctx.getEnvironment() == 'PRODUCTION') 
	{
		url = 'https://system.netsuite.com';			
	}
	else if (ctx.getEnvironment() == 'SANDBOX') 
	{
		url = 'https://system.sandbox.netsuite.com';				
	}*/
	nlapiLogExecution('ERROR', 'PDF URL',url);	
	var filefound = nlapiLoadFile('Images/LOGOCOMP.jpg'); 
	if (filefound) 
	{ 
		nlapiLogExecution('ERROR', 'Event', 'file;'+filefound.getId()); 
		var imageurl = filefound.getURL();
		nlapiLogExecution('ERROR','imageurl',imageurl);
		var finalimageurl = url + imageurl;//+';';
		//finalimageurl=finalimageurl+ '&expurl=T;';
		nlapiLogExecution('ERROR','imageurl',finalimageurl);
		finalimageurl=finalimageurl.replace(/&/g,"&amp;");

	} 
	else 
	{
		nlapiLogExecution('ERROR', 'Event', 'No file;');
	}

	//date
	var sysdate=DateStamp();
	var systime=TimeStamp();
	var Timez=calcTime('-5.00');
	nlapiLogExecution('ERROR','TimeZone',Timez);
	var datetime= new Date();
	datetime=datetime.toLocaleTimeString() ;


	var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<head><macrolist><macro id='myfooter'><p align='center'>Page <pagenumber/> of <totalpages/></p></macro></macrolist></head><body  font-size=\"7\"  size=\"A4-landscape\"  padding-top=\" 0mm\"   footer='myfooter' footer-height='20mm'>\n";

	//var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<head></head><body  font-size=\"7\"  size=\"A4-landscape\"    padding-top=\" 0mm\"   footer='myfooter' footer-height='20mm'>\n";
//	nlapiLogExecution('ERROR','xml0 ',"<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body font-size=\"7\">\n");

	var replaceChar =/\$|,|@|#|~|`|\%|\*|\^|\&|\+|\=|\-|\_|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|\./g;
	var strxml= "";

	if(vTaskStatus==1||vTaskStatus==2||vTaskStatus==3)
	{

		var orderListArray=new Array();

		for(k=0;k<result.length;k++)
		{
			nlapiLogExecution('ERROR', 'orderList[k]', result[k]); 
			var ordsearchresult = result[k];

			if(ordsearchresult!=null)
			{
				nlapiLogExecution('ERROR', 'ordsearchresult.length ', ordsearchresult.length); 
				for(var j=0;j<ordsearchresult.length;j++)
				{
					orderListArray[orderListArray.length]=ordsearchresult[j];				
				}
			}
		}
		result=orderListArray;
	}




	if(result!=null && result!="")
	{
		var rowcount=0;

		var totalcount=result.length;
		nlapiLogExecution('ERROR','totalcount',totalcount);
		while(0<totalcount)
		{
			var count=0;

			strxml += "<table width='100%'>";
			strxml += "<tr><td><table width='100%' >";
			strxml += "<tr><td valign='middle' align='left'><img src='" + finalimageurl + "'></img></td><td valign='middle' align='left'  style='font-size:xx-large;'>";
			strxml += "Order Management Report ";
			strxml += "</td><td align='right'>&nbsp;</td></tr></table></td></tr>";
			strxml += "<tr><td><table width='100%' >";
			strxml += "<tr><td style='width:41px;font-size: 12px;'>";
			strxml += "<p align='right'>Date/Time:"+Timez+"</p>"; 
			strxml += "</td></tr>";
			strxml += "</table>";
			strxml += "</td></tr>";

			strxml += "<tr><td><table width='1000px' valign='top' >";
			strxml += "<tr><td>";

			strxml +="<table width='980px'>";
			strxml +="<tr style=\"font-weight:bold;background-color:gray;color:white;\">";

			strxml += "<td width='10px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += "NSOrderNo#";
			strxml += "</td>";

			strxml += "<td width='8px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += "FO#";
			strxml += "</td>";

			strxml += "<td width='15px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += "Item";
			strxml += "</td>";

			if(vTaskStatus==2)
			{
				strxml += "<td width='8px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "Wave#";
				strxml += "</td>";

				strxml += "<td width='8px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += "PickGenQty";
				strxml += "</td>";

			}			

			strxml += "<td width='8px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += "NSOrderQty";
			strxml += "</td>";

			strxml += "<td width='8px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += "eBizOrderQty";
			strxml += "</td>";

			strxml += "<td width='15px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += "Customer";
			strxml += "</td></tr>";



			for(var i=rowcount;i<result.length;i++)
			{
				count++;
				rowcount++;
				var NSOrderNo="";
				var Customer="";
				var ordno="";
				var sku="";
				var wave="";
				var nsOrdQty="";
				var ordqty="";
				var pickgenQty="";

				if(vTaskStatus==1||vTaskStatus==2||vTaskStatus==3)
				{
					
					var searchresults=result[i];

					NSOrderNo=searchresults.getText('custrecord_ns_ord');
					ordno=searchresults.getValue('custrecord_lineord');
					ordqty=searchresults.getValue('custrecord_ord_qty');
					sku=searchresults.getText('custrecord_ebiz_linesku');
					wave=searchresults.getValue('custrecord_ebiz_wave');
					pickgenQty=searchresults.getValue('custrecord_pickgen_qty');
					Customer=searchresults.getText('custrecord_do_customer');
					
					var lineNsOrderno=NSOrderNo;
					if(lineNsOrderno== null || lineNsOrderno =="")
						lineNsOrderno="";

					var LineFO =ordno;
					if(LineFO== null || LineFO =="")	
						LineFO="&nbsp;";

					var LineItem=sku;
					if(LineItem== null || LineItem =="")
						LineItem="&nbsp;";

					var LineNsOrderQty =nsOrdQty;
					if(LineNsOrderQty== null || LineNsOrderQty =="")
						LineNsOrderQty="&nbsp;";

					var LineeBizOrderQty =ordqty;
					if(LineeBizOrderQty== null || LineeBizOrderQty =="")
						LineeBizOrderQty="&nbsp;";

					var LineCustomer =Customer;
					if(LineCustomer== null || LineCustomer =="")
						LineCustomer="&nbsp;";	

					if(vTaskStatus==2)
					{
						var LineWave =wave;
						if(LineWave== null || LineWave =="")
							LineWave="&nbsp;";	

						var LinePickGenQty =pickgenQty;
						if(LinePickGenQty== null || LinePickGenQty =="")
							LinePickGenQty="&nbsp;";	
					}

					//strxml += "<table width='100%' height='100%'>";
					strxml += "<tr>";
					strxml += "<td width='10px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += lineNsOrderno;
					strxml += "</td>";

					strxml += "<td width='8px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += LineFO;
					strxml += "</td>";

					strxml += "<td width='15px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += LineItem;
					strxml += "</td>";

					if(vTaskStatus==2)
					{
						strxml += "<td width='8px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
						strxml += LineWave;
						strxml += "</td>";

						strxml += "<td width='8px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
						strxml += LinePickGenQty;
						strxml += "</td>";

					}				

					strxml += "<td width='8px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += LineNsOrderQty;
					strxml += "</td>";

					strxml += "<td width='8px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += LineeBizOrderQty;
					strxml += "</td>";

					strxml += "<td width='15px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += LineCustomer;
					strxml += "</td>";
					strxml += "</tr>";



				}
				else
				{ var searchresults=result;
				if(vTaskStatus==5||vTaskStatus==6||vTaskStatus==4)
				{
					
					var actqty=searchresults[i].getValue('custrecord_act_qty',null,'max');

					if(parseFloat(actqty)>0)
					{
						count++;
						NSOrderNo=searchresults[i].getValue('custrecord_ebiz_order_no',null,'max');
						ordno=searchresults[i].getValue('name',null,'group');
						ordqty=searchresults[i].getValue('custrecord_expe_qty',null,'sum');
						sku=searchresults[i].getText('custrecord_sku',null,'group');
						Customer=searchresults[i].getText('entity','custrecord_ebiz_order_no','group');
						
						
						var lineNsOrderno=NSOrderNo;
						if(lineNsOrderno== null || lineNsOrderno =="")
							lineNsOrderno="";

						var LineFO =ordno;
						if(LineFO== null || LineFO =="")	
							LineFO="&nbsp;";

						var LineItem=sku;
						if(LineItem== null || LineItem =="")
							LineItem="&nbsp;";

						var LineNsOrderQty =nsOrdQty;
						if(LineNsOrderQty== null || LineNsOrderQty =="")
							LineNsOrderQty="&nbsp;";

						var LineeBizOrderQty =ordqty;
						if(LineeBizOrderQty== null || LineeBizOrderQty =="")
							LineeBizOrderQty="&nbsp;";

						var LineCustomer =Customer;
						if(LineCustomer== null || LineCustomer =="")
							LineCustomer="&nbsp;";	

						if(vTaskStatus==2)
						{
							var LineWave =wave;
							if(LineWave== null || LineWave =="")
								LineWave="&nbsp;";	

							var LinePickGenQty =pickgenQty;
							if(LinePickGenQty== null || LinePickGenQty =="")
								LinePickGenQty="&nbsp;";	
						}

						//strxml += "<table width='100%' height='100%'>";
						strxml += "<tr>";
						strxml += "<td width='10px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
						strxml += lineNsOrderno;
						strxml += "</td>";

						strxml += "<td width='8px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
						strxml += LineFO;
						strxml += "</td>";

						strxml += "<td width='15px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
						strxml += LineItem;
						strxml += "</td>";

						if(vTaskStatus==2)
						{
							strxml += "<td width='8px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
							strxml += LineWave;
							strxml += "</td>";

							strxml += "<td width='8px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
							strxml += LinePickGenQty;
							strxml += "</td>";

						}				

						strxml += "<td width='8px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
						strxml += LineNsOrderQty;
						strxml += "</td>";

						strxml += "<td width='8px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
						strxml += LineeBizOrderQty;
						strxml += "</td>";

						strxml += "<td width='15px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
						strxml += LineCustomer;
						strxml += "</td>";
						strxml += "</tr>";


					}
				}
				else
					{
					
					NSOrderNo=searchresults[i].getValue('custrecord_ebiz_order_no',null,'max');
					ordno=searchresults[i].getValue('name',null,'group');
					ordqty=searchresults[i].getValue('custrecord_expe_qty',null,'sum');
					sku=searchresults[i].getText('custrecord_sku',null,'group');
					Customer=searchresults[i].getText('entity','custrecord_ebiz_order_no','group');
					
					var lineNsOrderno=NSOrderNo;
					if(lineNsOrderno== null || lineNsOrderno =="")
						lineNsOrderno="";

					var LineFO =ordno;
					if(LineFO== null || LineFO =="")	
						LineFO="&nbsp;";

					var LineItem=sku;
					if(LineItem== null || LineItem =="")
						LineItem="&nbsp;";

					var LineNsOrderQty =nsOrdQty;
					if(LineNsOrderQty== null || LineNsOrderQty =="")
						LineNsOrderQty="&nbsp;";

					var LineeBizOrderQty =ordqty;
					if(LineeBizOrderQty== null || LineeBizOrderQty =="")
						LineeBizOrderQty="&nbsp;";

					var LineCustomer =Customer;
					if(LineCustomer== null || LineCustomer =="")
						LineCustomer="&nbsp;";	

					if(vTaskStatus==2)
					{
						var LineWave =wave;
						if(LineWave== null || LineWave =="")
							LineWave="&nbsp;";	

						var LinePickGenQty =pickgenQty;
						if(LinePickGenQty== null || LinePickGenQty =="")
							LinePickGenQty="&nbsp;";	
					}

					//strxml += "<table width='100%' height='100%'>";
					strxml += "<tr>";
					strxml += "<td width='10px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += lineNsOrderno;
					strxml += "</td>";

					strxml += "<td width='8px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += LineFO;
					strxml += "</td>";

					strxml += "<td width='15px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += LineItem;
					strxml += "</td>";

					if(vTaskStatus==2)
					{
						strxml += "<td width='8px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
						strxml += LineWave;
						strxml += "</td>";

						strxml += "<td width='8px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
						strxml += LinePickGenQty;
						strxml += "</td>";

					}				

					strxml += "<td width='8px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += LineNsOrderQty;
					strxml += "</td>";

					strxml += "<td width='8px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += LineeBizOrderQty;
					strxml += "</td>";

					strxml += "<td width='15px' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += LineCustomer;
					strxml += "</td>";
					strxml += "</tr>";

					}
					
				}



				if(count==20)
				{
					break;
				}



			}
			strxml += "</table>";

			strxml += "</td></tr>";
			strxml += "</table>";

			strxml += "</td></tr>";

			strxml += "</table>"; 

			totalcount=parseFloat(totalcount)-parseFloat(count);
			if(rowcount==result.length)
			{
				strxml +="<p style=\" page-break-after:avoid\"></p>";
			}
			else
			{
				strxml +="<p style=\" page-break-after:always\"></p>";
			}



		}
		strxml += "\n</body>\n</pdf>";
		xml=xml+strxml;
		var file = nlapiXMLToPDF(xml);	
		response.setContentType('PDF','OrderManagementReport.pdf');
		response.write( file.getValue() );


	}
}
/**
 * To generate To Excel 
 * @parameters search results ,status value   
 */

function GenerateExcel(result,vTaskStatus)
{
	var ordno="";
	var sku="";
	var ordqty="";
	var nsOrdQty="";
	var Customer="";
	var url;
	/*var ctx = nlapiGetContext();
	nlapiLogExecution('ERROR', 'Environment',ctx.getEnvironment());
	if (ctx.getEnvironment() == 'PRODUCTION') 
	{
		url = 'https://system.netsuite.com';			
	}
	else if (ctx.getEnvironment() == 'SANDBOX') 
	{
		url = 'https://system.sandbox.netsuite.com';				
	}*/
	nlapiLogExecution('ERROR', 'PDF URL',url);	
	var filefound = nlapiLoadFile('Images/LOGOCOMP.jpg'); 
	if (filefound) 
	{ 
		nlapiLogExecution('ERROR', 'Event', 'file;'+filefound.getId()); 
		var imageurl = filefound.getURL();
		nlapiLogExecution('ERROR','imageurl',imageurl);
		var finalimageurl = url + imageurl;//+';';
		//finalimageurl=finalimageurl+ '&expurl=T;';
		nlapiLogExecution('ERROR','imageurl',finalimageurl);
		finalimageurl=finalimageurl.replace(/&/g,"&amp;");

	} 
	else 
	{
		nlapiLogExecution('ERROR', 'Event', 'No file;');
	}

	//date
	var sysdate=DateStamp();
	var systime=TimeStamp();
	var Timez=calcTime('-5.00');
	nlapiLogExecution('ERROR','TimeZone',Timez);
	var datetime= new Date();
	datetime=datetime.toLocaleTimeString() ;


	var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<head><macrolist><macro id='myfooter'><p align='center'>Page <pagenumber/> of <totalpages/></p></macro></macrolist></head><body  font-size=\"7\"  size=\"A4-landscape\"  padding-top=\" 0mm\"   footer='myfooter' footer-height='20mm'>\n";

	//var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<head></head><body  font-size=\"7\"  size=\"A4-landscape\"    padding-top=\" 0mm\"   footer='myfooter' footer-height='20mm'>\n";
//	nlapiLogExecution('ERROR','xml0 ',"<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body font-size=\"7\">\n");

	var replaceChar =/\$|,|@|#|~|`|\%|\*|\^|\&|\+|\=|\-|\_|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|\./g;
	var strxml= "";


	if(result!=null && result!="")
	{
		var rowcount=0;

		var totalcount=result.length;
		nlapiLogExecution('ERROR','totalcount',totalcount);

		strxml += "<table width='100%'>";
		strxml += "<tr><td><table width='100%' >";
		strxml += "<tr><td valign='middle' align='left' colspan='2'><img src='" + finalimageurl + "'></img><p align='center' style='font-size:xx-large;'>";
		strxml += "Order Management Report ";
		strxml += "</p></td></tr></table></td></tr>";
		strxml += "<tr><td><table width='100%' >";
		strxml += "<tr><td style='width:41px;font-size: 12px;'>";
		strxml += "<p align='right'>Date/Time:"+Timez+"</p>"; 
		strxml += "</td></tr>";
		strxml += "</table>";
		strxml += "</td></tr>";

		strxml += "<tr><td><table width='100%' valign='top' >";
		strxml += "<tr><td>";

		strxml +="<table width='100%'>";
		strxml +="<tr style=\"font-weight:bold;background-color:gray;color:white;\">";

		strxml += "<td width='15%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
		strxml += "NS OrderNo#";
		strxml += "</td>";

		strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
		strxml += "FO#";
		strxml += "</td>";

		strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
		strxml += "Item";
		strxml += "</td>";

		if(vTaskStatus==2)
		{
			strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += "Wave#";
			strxml += "</td>";

			strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
			strxml += "PickGenQty";
			strxml += "</td>";

		}



		strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
		strxml += "NS Order Qty";
		strxml += "</td>";

		strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
		strxml += "eBiz Order Qty";
		strxml += "</td>";

		strxml += "<td width='15%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
		strxml += "Customer";
		strxml += "</td></tr>";



		//for(var i=rowcount;i<result.length;i++)
		//{
		var NSOrderNo="";
		var Customer="";
		var ordno="";
		var sku="";
		var wave="";
		var nsOrdQty="";
		var ordqty="";
		var pickgenQty="";

		if(vTaskStatus==1||vTaskStatus==2||vTaskStatus==3)
		{

			var orderListArray=new Array();

			for(k=0;k<result.length;k++)
			{
				nlapiLogExecution('ERROR', 'orderList[k]', result[k]); 
				var ordsearchresult = result[k];

				if(ordsearchresult!=null)
				{
					nlapiLogExecution('ERROR', 'ordsearchresult.length ', ordsearchresult.length); 
					for(var j=0;j<ordsearchresult.length;j++)
					{
						orderListArray[orderListArray.length]=ordsearchresult[j];				
					}
				}
			}
			for(var j = 0; j < orderListArray.length; j++){		
				nlapiLogExecution('ERROR', 'orderListArray ', orderListArray[j]); 
				var searchresults = orderListArray[j];

				NSOrderNo=searchresults.getText('custrecord_ns_ord');
				ordno=searchresults.getValue('custrecord_lineord');
				ordqty=searchresults.getValue('custrecord_ord_qty');
				sku=searchresults.getText('custrecord_ebiz_linesku');
				wave=searchresults.getValue('custrecord_ebiz_wave');
				pickgenQty=searchresults.getValue('custrecord_pickgen_qty');
				Customer=searchresults.getText('custrecord_do_customer');
				var lineNsOrderno=NSOrderNo;
				if(lineNsOrderno== null || lineNsOrderno =="")
					lineNsOrderno="";

				var LineFO =ordno;
				if(LineFO== null || LineFO =="")	
					LineFO="&nbsp;";

				var LineItem=sku;
				if(LineItem== null || LineItem =="")
					LineItem="&nbsp;";

				var LineNsOrderQty =nsOrdQty;
				if(LineNsOrderQty== null || LineNsOrderQty =="")
					LineNsOrderQty="&nbsp;";

				var LineeBizOrderQty =ordqty;
				if(LineeBizOrderQty== null || LineeBizOrderQty =="")
					LineeBizOrderQty="&nbsp;";

				var LineCustomer =Customer;
				if(LineCustomer== null || LineCustomer =="")
					LineCustomer="&nbsp;";		

				if(vTaskStatus==2)
				{
					var LineWave =wave;
					if(LineWave== null || LineWave =="")
						LineWave="&nbsp;";	

					var LinePickGenQty =pickgenQty;
					if(LinePickGenQty== null || LinePickGenQty =="")
						LinePickGenQty="&nbsp;";	
				}


				//strxml += "<table width='100%' height='100%'>";
				strxml += "<tr>";
				strxml += "<td width='15%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += lineNsOrderno;
				strxml += "</td>";

				strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += LineFO;
				strxml += "</td>";

				strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += LineItem;
				strxml += "</td>";

				if(vTaskStatus==2)
				{
					strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += LineWave;
					strxml += "</td>";

					strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += LinePickGenQty;
					strxml += "</td>";

				}



				strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += LineNsOrderQty;
				strxml += "</td>";

				strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += LineeBizOrderQty;
				strxml += "</td>";

				strxml += "<td width='15%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += LineCustomer;
				strxml += "</td>";
				strxml += "</tr>";


			}



		}
		else
		{ 
			for(var i=rowcount;i<result.length;i++)
			{
				var searchresults=result;
				if(vTaskStatus==5||vTaskStatus==6||vTaskStatus==4)
				{
					var actqty=searchresults[i].getValue('custrecord_act_qty',null,'max');

					if(parseFloat(actqty)>0)
					{

						NSOrderNo=searchresults[i].getValue('custrecord_ebiz_order_no',null,'max');
						ordno=searchresults[i].getValue('name',null,'group');
						ordqty=searchresults[i].getValue('custrecord_expe_qty',null,'sum');
						sku=searchresults[i].getText('custrecord_sku',null,'group');
						Customer=searchresults[i].getText('entity','custrecord_ebiz_order_no','group');

					}
				}
				else
				{
				NSOrderNo=searchresults[i].getValue('custrecord_ebiz_order_no',null,'max');
				ordno=searchresults[i].getValue('name',null,'group');
				ordqty=searchresults[i].getValue('custrecord_expe_qty',null,'sum');
				sku=searchresults[i].getText('custrecord_sku',null,'group');
				Customer=searchresults[i].getText('entity','custrecord_ebiz_order_no','group');

				}				
				
			
				var lineNsOrderno=NSOrderNo;
				if(lineNsOrderno== null || lineNsOrderno =="")
					lineNsOrderno="";

				var LineFO =ordno;
				if(LineFO== null || LineFO =="")	
					LineFO="&nbsp;";

				var LineItem=sku;
				if(LineItem== null || LineItem =="")
					LineItem="&nbsp;";

				var LineNsOrderQty =nsOrdQty;
				if(LineNsOrderQty== null || LineNsOrderQty =="")
					LineNsOrderQty="&nbsp;";

				var LineeBizOrderQty =ordqty;
				if(LineeBizOrderQty== null || LineeBizOrderQty =="")
					LineeBizOrderQty="&nbsp;";

				var LineCustomer =Customer;
				if(LineCustomer== null || LineCustomer =="")
					LineCustomer="&nbsp;";		

				if(vTaskStatus==2)
				{
					var LineWave =wave;
					if(LineWave== null || LineWave =="")
						LineWave="&nbsp;";	

					var LinePickGenQty =pickgenQty;
					if(LinePickGenQty== null || LinePickGenQty =="")
						LinePickGenQty="&nbsp;";	
				}


				//strxml += "<table width='100%' height='100%'>";
				strxml += "<tr>";
				strxml += "<td width='15%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += lineNsOrderno;
				strxml += "</td>";

				strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += LineFO;
				strxml += "</td>";

				strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += LineItem;
				strxml += "</td>";

				if(vTaskStatus==2)
				{
					strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += LineWave;
					strxml += "</td>";

					strxml += "<td width='8%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
					strxml += LinePickGenQty;
					strxml += "</td>";

				}



				strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += LineNsOrderQty;
				strxml += "</td>";

				strxml += "<td width='10%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += LineeBizOrderQty;
				strxml += "</td>";

				strxml += "<td width='15%' align='center' style='border-width: 1px; border-color: #000000;font-size: 14px;'>";
				strxml += LineCustomer;
				strxml += "</td>";
				strxml += "</tr>";			


			}
		}

		strxml += "</table>";

		strxml += "</td></tr>";
		strxml += "</table>";

		strxml += "</td></tr>";

		strxml += "</table>"; 


		strxml += "\n</body>\n</pdf>";
		xml=xml+strxml;
		var exportXML = strxml;
		response.setContentType('PLAINTEXT', 'ExcelResults.xls', 'attachment');
		response.write(exportXML);	

	}
}






/**
 * @param fromdate,todate,soid
 * @param vTaskStatus
 * @returns
 */
function ShipManifestRecord(fromdate,todate,vTaskStatus,soid)
{
	try
	{	
		var filters = new Array();
		if (soid != ""&&soid!=null) {
			nlapiLogExecution('ERROR', 'Inside SO', soid);
			filters.push(new nlobjSearchFilter('custrecord_ship_order', null, 'is', soid));
		}
		if ((fromdate != "" && fromdate != null) && (todate != "" && todate != null) ) 
		{
			nlapiLogExecution('ERROR', 'fromdate',fromdate );
			nlapiLogExecution('ERROR', 'todate', todate);

			filters.push(new nlobjSearchFilter('lastmodified', null, 'within', fromdate, todate));
		} 

		if(vTaskStatus==5)
			filters.push(new nlobjSearchFilter('custrecord_ship_masttrackno',null,'isempty'));

		else if(vTaskStatus==6)
		{
			filters.push(new nlobjSearchFilter('custrecord_ship_masttrackno',null,'isnotempty'));
			filters.push(new nlobjSearchFilter('custrecord_ship_nsconf_no',null,'isempty'));
		}

		var columns=new Array();
		columns[0]=new nlobjSearchColumn('custrecord_ship_order');
		columns[1]=new nlobjSearchColumn('custrecord_ship_consignee');
		columns[2]=new nlobjSearchColumn('custrecord_ship_orderno');
//		columns[1]=new nlobjSearchColumn('created');
		var searchresult=nlapiSearchRecord('customrecord_ship_manifest',null,filters,columns);

		return searchresult;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in OpentaskRecord',exp);	
	}
}


/**
 * @param  fromdate,todate,soid
 * @returns
 */
function GetpickFailedRecord(fromdate,todate,option,soid)
{
	try
	{
		var filtersso= new Array();
		if (soid != ""&&soid!=null){
			filtersso.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'anyof',soid));
		}
		if ((fromdate != "" && fromdate != null) && (todate != "" && todate != null)) 
		{
			filtersso.push(new nlobjSearchFilter('custrecordact_begin_date', null, 'within', fromdate, todate));
		}

		filtersso.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', ['26']));//STATUS.OUTBOUND.FAILED
		filtersso.push(new nlobjSearchFilter('mainline','custrecord_ebiz_order_no','is', 'T'));

		/*var columnso = new Array();
		columnso[0]=new nlobjSearchColumn('custrecord_ebiz_order_no');
		columnso[1]=new nlobjSearchColumn('custrecord_sku');
		columnso[2]=new nlobjSearchColumn('custrecord_expe_qty');
		columnso[3]=new nlobjSearchColumn('name');
		columnso[4]=new nlobjSearchColumn('entity','custrecord_ebiz_order_no');*/
		var columnso = new Array();
		columnso[0]=new nlobjSearchColumn('custrecord_sku',null,'group');
		columnso[1]=new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
		columnso[2]=new nlobjSearchColumn('name',null,'group');
		columnso[3]=new nlobjSearchColumn('entity','custrecord_ebiz_order_no','group');
		columnso[4]=new nlobjSearchColumn('custrecord_ebiz_order_no',null,'max');
		columnso[5]=new nlobjSearchColumn('custrecord_act_qty',null,'max');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso, columnso);
		return searchresults;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetpickFailedRecord',exp);
	}
}


var tempOTResultsArray=new Array();

function OrderLineRecord(fromdate,todate,option,soid,maxno)
{
	nlapiLogExecution('ERROR', 'option',option);
	try{
		var i = 0;
		var filters = new Array();
		if (soid != ""&&soid!=null) {
			nlapiLogExecution('ERROR', 'Inside SO', soid);
			filters.push(new nlobjSearchFilter('custrecord_ns_ord', null, 'anyof', soid));
		}

		if ((fromdate != "" && fromdate != null) && (todate != "" && todate != null) ) {
			nlapiLogExecution('ERROR', 'fromdate', fromdate);
			nlapiLogExecution('ERROR', 'todate', todate);

			filters.push(new nlobjSearchFilter('custrecord_record_linedate', null, 'within', fromdate, todate));
		} 
		if(option==1)
			filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof',25 ));//25 STATUS.INBOUND_OUTBOUND.EDIT 
		else if(option==2)
		{
			filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof',[9] ));//9 STATUS.OUTBOUND.PICK_GENERATED
			filters.push(new nlobjSearchFilter('custrecord_printflag', null, 'is','F' ));
		}
		else if(option==3)
		{
			filters.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof',[9,11] ));//9 STATUS.OUTBOUND.PICK_GENERATED;11=STATUS.OUTBOUND.ORDER_PARTIALLY_PROCESSED
			filters.push(new nlobjSearchFilter('custrecord_printflag', null, 'is','T' ));
			filters.push(new nlobjSearchFilter('custrecord_ord_qty', null, 'greaterthan', 0));
			filters.push(new nlobjSearchFilter('custrecord_pickgen_qty', null, 'greaterthan', 0));
		}

		if(maxno!=-1)
		{

			filters.push(new nlobjSearchFilter('id', null, 'greaterthan', maxno));
		}
		var columns=new Array();
		columns.push(new nlobjSearchColumn('internalid'));
		columns.push(new nlobjSearchColumn('custrecord_ns_ord'));
		columns.push(new nlobjSearchColumn('custrecord_lineord'));
		columns.push(new nlobjSearchColumn('custrecord_ordline'));
		columns.push(new nlobjSearchColumn('custrecord_ord_qty'));
		columns.push(new nlobjSearchColumn('custrecord_pickgen_qty'));
		columns.push(new nlobjSearchColumn('custrecord_pickqty'));
		columns.push(new nlobjSearchColumn('custrecord_ship_qty'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_linesku'));
		columns.push(new nlobjSearchColumn('custrecord_record_linedate'));
		columns.push(new nlobjSearchColumn('custrecord_linestatus_flag'));
		columns.push(new nlobjSearchColumn('custrecord_printflag'));
		columns.push(new nlobjSearchColumn('custrecord_record_linetime'));
		columns.push(new nlobjSearchColumn('lastmodified'));
		columns.push(new nlobjSearchColumn('custrecord_do_customer'));
		columns[0].setSort();
		columns[1].setSort();
		columns.push(new nlobjSearchColumn('custrecord_ebiz_wave'));
//		columns.push(new nlobjSearchColumn('entity','custrecord_ns_ord'));

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters,columns);
		if(searchresults!=null)
		{
			nlapiLogExecution('ERROR','searchresults',searchresults.length);
			if(searchresults.length>=1000)
			{
				nlapiLogExecution('ERROR','searchresults','more than 1000');
				var maxno1=searchresults[searchresults.length-1].getValue(columns[0]);
				tempOTResultsArray.push(searchresults);
				OrderLineRecord(fromdate,todate,option,soid,maxno1);

			}
			else
			{
				tempOTResultsArray.push(searchresults);
			}
		}
		return tempOTResultsArray;
		//return searchresults;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in OrderLineRecord',exp);	
	}
}
function calcTime(offset) {

	//var vtime=new Date();

	//nlapiLogExecution('ERROR', 'TimeOffset', vtime.getTimezoneOffset());

	// create Date object for current location
	d = new Date();

	// convert to msec
	// add local time zone offset 
	// get UTC time in msec
	utc = d.getTime() + (d.getTimezoneOffset() * 60000);

	// create new Date object for different city
	// using supplied offset
	nd = new Date(utc + (3600000*offset));

	// return time as a string
	//return "The local time in " + city + " is " + nd.toLocaleString();

	var stringDt=((parseFloat(nd.getMonth()) + 1) + '/' + (parseFloat(nd.getDate())) + '/' + nd.getFullYear());

	var timestamp;
	var a_p = "";

	//Getting time in hh:mm tt format.
	var curr_hour = nd.getHours();
	var curr_min = nd.getMinutes();

	// determining the am/pm indicator
	if (curr_hour < 12)
		a_p = "am";
	else
		a_p = "pm";

	// finalizing hours depending on 24hr clock
	if (curr_hour == 0)
		curr_hour = 12;
	else if(curr_hour > 12)
		curr_hour -= 12;

	if (curr_min.length == 1)
		curr_min = "0" + curr_min;

	//Adding fields to update time zones.
	timestamp = curr_hour + ":" + curr_min + " " + a_p;

	return stringDt + " " + timestamp;

}

function GetUniqueSO(searchResult)
{
	nlapiLogExecution('ERROR', 'GetUniqueSO', 'GetUniqueSO');
	var ordersAlreadyAddedList = new Array();

	if(searchResult != null && searchResult.length > 0)
	{
		nlapiLogExecution('ERROR', 'searchResult.length', searchResult.length);

		for(var i = 0; i < searchResult.length; i++)
		{
			var currentTask = searchResult[i];
			var OrderNo = currentTask.getValue('custrecord_ship_order');
			if(!isValueAlreadyAdded(ordersAlreadyAddedList, OrderNo))
			{
				ordersAlreadyAddedList.push(OrderNo);
			}
		}
	}
	return ordersAlreadyAddedList;
}


/**
 * @param alreadyAddedList
 * @param currentValue
 * @returns {Boolean}
 */
function isValueAlreadyAdded(alreadyAddedList, currentValue)
{
	var alreadyAdded = false;

	for(var i = 0; i < alreadyAddedList.length; i++){
		if(alreadyAddedList[i] == currentValue){
			alreadyAdded = true;
			break;
		}
	}
	return alreadyAdded;
}

/**
 * @param fromdate,todate and soid
 * @returns searchresults
 */

function GetOpenTaskRecord(uniqueSo,fromdate,todate)
{
	try
	{
		var filter=new Array();
		if(uniqueSo!=null&&uniqueSo!="")
			filter.push(new nlobjSearchFilter('custrecord_ebiz_order_no',null,'anyof',uniqueSo));
		if ((fromdate != "" && fromdate!= null) && ( todate != "" && todate != null) ) 
		{
			nlapiLogExecution('ERROR', 'fromdate',fromdate);
			nlapiLogExecution('ERROR', 'todate', todate);

			filter.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'within', fromdate,todate));
		} 
		filter.push(new nlobjSearchFilter('custrecord_wms_status_flag',null,'anyof',28));//Pack-complete
		filter.push(new nlobjSearchFilter('custrecord_tasktype',null,'anyof',3));//pick
		filter.push(new nlobjSearchFilter('mainline','custrecord_ebiz_order_no','is','T'));
		//filter.push(new nlobjSearchFilter('custrecord_act_qty',null,'greaterthan',0));

		var column=new Array();
		//column[0]=new nlobjSearchColumn('custrecord_ebiz_order_no',null,'group');
		column[0]=new nlobjSearchColumn('name',null,'group');
		column[1]=new nlobjSearchColumn('custrecord_sku',null,'group');
		column[2]=new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
		column[3]=new nlobjSearchColumn('custrecord_ebiz_order_no',null,'max');
		column[4]=new nlobjSearchColumn('entity','custrecord_ebiz_order_no','group');
		column[5]=new nlobjSearchColumn('custrecord_act_qty',null,'max');
		var searchRecord=nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filter,column);

		return searchRecord;

	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetOpenTaskRecord',exp);
	}
}


/**
 * @param fromdate,todate and soid
 * @returns searchresults
 */
function Getpickcomplete_ItemFullFailedRecord(fromdate,todate,soid)
{
	try
	{
		var filtersso= new Array();
		if (soid != "") 
			filtersso.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'anyof',soid));

		if ((fromdate != "" && fromdate != null) && (todate != "" && todate != null) ) 
		{
			filtersso.push(new nlobjSearchFilter('custrecordact_begin_date', null, 'within', fromdate, todate));
		}

		filtersso.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', [8,28]));//28 STATUS.OUTBOUND.PACK_COMPLETE ;8 STATUS.OUTBOUND.PICK_CONFIRMED 
		filtersso.push(new nlobjSearchFilter('custrecord_tasktype', null,'anyof',3));//3=Pick
		filtersso.push(new nlobjSearchFilter('mainline','custrecord_ebiz_order_no','is', 'T'));
		filtersso.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',null,'isempty'));
		
		
		var columnso = new Array();
		columnso[0]=new nlobjSearchColumn('custrecord_sku',null,'group');
		columnso[1]=new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
		columnso[2]=new nlobjSearchColumn('name',null,'group');
		columnso[3]=new nlobjSearchColumn('entity','custrecord_ebiz_order_no','group');
        columnso[4]=new nlobjSearchColumn('custrecord_ebiz_order_no',null,'max');
        columnso[5]=new nlobjSearchColumn('custrecord_act_qty',null,'max');
        
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso, columnso);
		return searchresults;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in Getpickcomplete_ItemFullFailedRecord',exp);
	}
}


function Getpackcomplete_ItemFullFailedRecord(fromdate,todate,vTaskStatus,soid)
{
	try
	{
		var filtersso= new Array();
		if (soid != "") 
			filtersso.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'anyof',soid));

		if ((fromdate != "" && fromdate != null) && (todate != "" && todate != null) ) 
		{
			filtersso.push(new nlobjSearchFilter('custrecordact_begin_date', null, 'within', fromdate, todate));
		}

		filtersso.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', 28));//28 STATUS.OUTBOUND.PACK_COMPLETE ;8 STATUS.OUTBOUND.PICK_CONFIRMED 
		filtersso.push(new nlobjSearchFilter('custrecord_tasktype', null,'anyof',3));//3=Pick
		filtersso.push(new nlobjSearchFilter('mainline','custrecord_ebiz_order_no','is', 'T'));
		filtersso.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',null,'isempty'));

		var columnso = new Array();
		columnso[0]=new nlobjSearchColumn('custrecord_sku',null,'group');
		columnso[1]=new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
		columnso[2]=new nlobjSearchColumn('name',null,'group');
		columnso[3]=new nlobjSearchColumn('entity','custrecord_ebiz_order_no','group');
		columnso[4]=new nlobjSearchColumn('custrecord_ebiz_order_no',null,'max');
		columnso[5]=new nlobjSearchColumn('custrecord_act_qty',null,'max');

		columnso[2].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso, columnso);
		return searchresults;
	}
	catch(exp)
	{
	nlapiLogExecution('ERROR','Exception in Getpackcomplete_ItemFullFailedRecord',exp);	
	}
}


function PaymentHoldFailed_PickingCompleted(fromdate,todate,vTaskStatus,soid)
{
	try
	{
		var filtersso= new Array();
		if (soid != "") 
			filtersso.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null,'anyof',soid));

		if ((fromdate != "" && fromdate != null) && (todate != "" && todate != null) ) 
		{
			filtersso.push(new nlobjSearchFilter('custrecordact_begin_date', null, 'within', fromdate, todate));
		}

		filtersso.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,'anyof', 8));//28 STATUS.OUTBOUND.PACK_COMPLETE ;8 STATUS.OUTBOUND.PICK_CONFIRMED 
		filtersso.push(new nlobjSearchFilter('custrecord_tasktype', null,'anyof',3));//3=Pick
		filtersso.push(new nlobjSearchFilter('mainline','custrecord_ebiz_order_no','is', 'T'));
		filtersso.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no',null,'isempty'));
		filtersso.push(new nlobjSearchFilter('paymenteventresult','custrecord_ebiz_order_no','is', 'HOLD'));

		var columnso = new Array();
		columnso[0]=new nlobjSearchColumn('custrecord_sku',null,'group');
		columnso[1]=new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
		columnso[2]=new nlobjSearchColumn('name',null,'group');
		columnso[3]=new nlobjSearchColumn('entity','custrecord_ebiz_order_no','group');
		columnso[4]=new nlobjSearchColumn('custrecord_ebiz_order_no',null,'max');
		columnso[5]=new nlobjSearchColumn('custrecord_act_qty',null,'max');

		columnso[2].setSort();
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso, columnso);
		return searchresults;
	}
	catch(exp)
	{
	nlapiLogExecution('ERROR','Exception in PaymentHoldFailed_PickingCompleted',exp);	
	}
}
