/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inbound/Portlet/ebiz_TaskStatus_PL.js,v $
 *     	   $Revision: 1.2.4.1.4.1.4.3 $
 *     	   $Date: 2014/05/23 05:01:14 $
 *     	   $Author: nneelam $
 *     	   $Name: t_eBN_2014_1_StdBundle_3_38 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_TaskStatus_PL.js,v $
 * Revision 1.2.4.1.4.1.4.3  2014/05/23 05:01:14  nneelam
 * case#  20148501
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.2.4.1.4.1.4.2  2013/09/30 16:00:16  rmukkera
 * Case#  20124376
 *
 * Revision 1.2.4.1.4.1.4.1  2013/09/20 15:22:32  rmukkera
 * Case# 20124546
 *
 * Revision 1.2.4.1.4.1  2012/11/01 14:55:42  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.2.4.1  2012/04/02 12:47:17  schepuri
 * CASE201112/CR201113/LOG201121
 * fetching more than 1000 rec
 *
 * Revision 1.2  2011/08/22 11:24:52  rmukkera
 * CASE201112/CR201113/LOG201121
 * source code changes included both open and close tasks
 *
 * Revision 1.1  2011/08/19 16:52:22  skota
 * CASE201112/CR201113/LOG201121
 * Task Summary Portlet
 *
 * 
 *****************************************************************************/

  var socount=0;
	var cocount=0;
	
function taskStatusPortlet(portlet, column)
{  

	portlet.setTitle('Task&nbsp;Summary');  
	



	var  content='';
	portlet.setHtml( content );


}

function getOpenTaskStatus(status,maxno,sysdate)
{
	var filtersso = new Array();

	filtersso[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'is', status);
                     filtersso[1] = new nlobjSearchFilter('custrecordact_begin_date', null, 'on', sysdate); 
                     filtersso[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'noneof', '26');
	//filtersso[1] = new nlobjSearchFilter('custrecordact_begin_date', null, 'on', '3/30/2012');         
	if(maxno!=0)
	{
		filtersso.push(new nlobjSearchFilter('id',null,'greaterthan',maxno));
	}
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_tasktype');
	columns[1] = new nlobjSearchColumn('id');
	columns[1].setSort();

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso, columns);

	if(searchresults != null && searchresults.length>=1000)
	{
		nlapiLogExecution('ERROR','getOpenTaskStatus if',searchresults.length);
		socount += parseFloat(searchresults.length);
		/*if(searchresults.length>=1000)
		{*/
		nlapiLogExecution('ERROR','socounttst OT',socount);
		var mno=searchresults[searchresults.length-1].getId();    
		getOpenTaskStatus(status,mno,sysdate);
		/*}*/
	}
	else
	{
		
		if(searchresults != null)
		{
			nlapiLogExecution('ERROR','getOpenTaskStatus else',searchresults.length);
			socount += parseFloat(searchresults.length);
			nlapiLogExecution('ERROR','getOpenTaskStatus else',searchresults.length); 
		}
	}

	nlapiLogExecution('ERROR','socount OT',socount);
	nlapiLogExecution('ERROR','cnt OT',socount);
	return socount;

}

function getClosedTaskStatus(status,maxno,sysdate)
{
	var filters = new Array();

	filters[0] = new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'is', status);
                      filters[1] = new nlobjSearchFilter('custrecord_ebiztask_act_end_date', null, 'on',sysdate); 
                      filters[2] = new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag', null, 'noneof', '26');
	//filters[1] = new nlobjSearchFilter('custrecord_ebiztask_act_end_date', null, 'on', '3/30/2012');          
		if(maxno!=0)
	{
		filters.push(new nlobjSearchFilter('id',null,'greaterthan',maxno));
	}
	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebiztask_tasktype');
	column[1] = new nlobjSearchColumn('id');
	column[1].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filters, column);

	if(searchresults != null)

	{
		nlapiLogExecution('ERROR','getOpenTaskStatus CT',searchresults.length);

		cocount+= parseFloat(searchresults.length);

		if(searchresults.length>=1000)
		{
			var mno=searchresults[searchresults.length-1].getId();    
			getClosedTaskStatus(status,mno,sysdate);
		}
	}



	nlapiLogExecution('ERROR','cocountOT',cocount);



	return cocount;
}

function getTaskStatus(status,count)
{
	var sysdate=DateStamp();
	//var socount=0;	

	var resOpenTaskStatus = getOpenTaskStatus(status,0,sysdate,count);

	nlapiLogExecution('ERROR', ' Open Task count', resOpenTaskStatus);


	//    var count=0;  
	var resClosedTaskStatus = getClosedTaskStatus(status,0,sysdate,count);
	nlapiLogExecution('ERROR', 'closedTask count',resClosedTaskStatus);


	
	return parseFloat(resOpenTaskStatus)+parseFloat(resClosedTaskStatus );
}








