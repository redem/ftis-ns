/***************************************************************************
                                                                 
                                                  eBizNET Solutions Inc 
****************************************************************************
*
*  $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inbound/Client/ebiz_LPValueValidation_CL.js,v $
*  $Revision: 1.2.6.1.4.1 $
*  $Date: 2013/09/11 15:23:51 $
*  $Author: rmukkera $
*  $Name: t_NSWMS_2014_1_1_174 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_LPValueValidation_CL.js,v $
*  Revision 1.2.6.1.4.1  2013/09/11 15:23:51  rmukkera
*  Case# 20124376
*
*
****************************************************************************/

function LPRangeValidation()
{
    //var lpno = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_chkn_lp');
			//alert('Inside functuion');
	var lpno = nlapiGetFieldValue('custpage_lp');
			//alert("LP num is"+lpno);
	
    var type ="PALT";
    var vargetlpno = lpno;
    
	
	//var vargetlpno = strLpNo;

	//var vargetlpno = "PALT123344";
    var vResult = "";
  
    if (vargetlpno.length > 0) {
    
        var searchresults = nlapiSearchRecord('customrecord_ebiznet_lp_range', null, null, new nlobjSearchColumn('custrecord_ebiznet_lprange_lpprefix'));
       //alert(searchresults);
        for (var i = 0; i < Math.min(50, searchresults.length); i++) {
            try {
                var getLPPrefix = searchresults[i].getValue('custrecord_ebiznet_lprange_lpprefix');
                
                var recid = searchresults[i].getId();
				//alert("recid:"+recid);
                var transaction = nlapiLoadRecord('customrecord_ebiznet_lp_range', recid);
                
                var varBeginLPRange = transaction.getFieldValue('custrecord_ebiznet_lprange_begin');
                
                var varEndRange = transaction.getFieldValue('custrecord_ebiznet_lprange_end');
             //   alert("No varEndRange" + varEndRange);
                var getLPGenerationTypeValue = transaction.getFieldValue('custrecord_ebiznet_lprange_lpgentype');
            //    alert("getLPGenerationTypeValue" + getLPGenerationTypeValue);
                //			var getLPGenerationTypeText = transaction.getFieldText('custrecord_ebiznet_lprange_lpgentype');
                //			alert("getLPGenerationTypeText" + getLPGenerationTypeText);
                var getLPTypeValue = transaction.getFieldValue('custrecord_ebiznet_lprange_lptype');
              //  alert("getLPTypeValue " + getLPTypeValue);
               // var getLPTypeText = transaction.getFieldText('custrecord_ebiznet_lprange_lptype');
               // alert("getLPTypeText" + getLPTypeText);
                
               // alert("getLPTypeText.toUpperCase()" + getLPTypeText.toUpperCase());
                
                
               // if (getLPTypeValue.toUpperCase() == 4) {
			   if (getLPTypeValue == "1") {
                   // alert("sa firstCase" + lpno + type);
                    
                    var getLPrefix = transaction.getFieldValue('custrecord_ebiznet_lprange_lpprefix');
                   // alert("getLPrefix" + getLPrefix);
                    var LPprefixlen = getLPrefix.length;
                    var vLPLen = vargetlpno.substring(0, LPprefixlen).toUpperCase();
                  //  alert("vLPLen" + vLPLen);
                    
                    if (vLPLen == getLPrefix) {
                       // alert("vLPLen == getLPrefix" + "ifchecvk");
                        var varnum = vargetlpno.substring(LPprefixlen, vargetlpno.length);
                      //  alert("varnum" + varnum);
                        if (varnum.length > varEndRange.length) {
                          //  alert("varnum.length > varEndRange.length" + (varnum.length));
                            vResult = "N";
                            break;
                        }
                        
                        
                        if ((parseFloat(varnum) < parseFloat(varBeginLPRange)) || (parseFloat(varnum) > parseFloat(varEndRange))) {
                         //   alert("parseFloat(varnum) " + parseFloat(varnum));
                            vResult = "N";
                            break;
                        }
                        else {
                            vResult = "Y";
                            break;
                        }
                    }
                    else {
                        vResult = "N";
                    }
                    
                } //end of if statement
                else {
                    //alert("in else");
                }
                
            } 
            catch (err) {
                alert("exception" + err + "value is " );
                
            }
        } //end of for loop
        if (vResult == "Y") {
            //alert('LP is with in the range of numbers');
            return true;
            
        }
        else {
            alert('Invalid LP Range!');
            return false;
        }
    }
    else {
        alert('Invalid LP No');
        return false;
    }
}