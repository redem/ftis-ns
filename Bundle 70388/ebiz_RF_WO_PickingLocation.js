/***************************************************************************
  		   eBizNET Solutions Inc               
 ***************************************************************************
 **     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_WO_PickingLocation.js,v $
 *     	   $Revision: 1.1.2.2.4.8 $
 *     	   $Date: 2015/04/13 13:10:49 $
 *     	   $Author: schepuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_WO_PickingLocation.js,v $
 * Revision 1.1.2.2.4.8  2015/04/13 13:10:49  schepuri
 * case # 201412324
 *
 * Revision 1.1.2.2.4.7  2014/08/18 12:34:43  snimmakayala
 * Case: 20149972 & 20149973
 * JAWBONE WO RF PICKING ISSUES
 *
 * Revision 1.1.2.2.4.6  2014/08/15 15:36:36  snimmakayala
 * Case: 20149972 & 20149973
 * JAWBONE WO PICKING FIXES
 *
 * Revision 1.1.2.2.4.5  2014/06/13 08:52:30  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.2.4.4  2014/05/30 00:34:24  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.2.4.3  2014/02/05 15:08:33  sponnaganti
 * case# 20127056
 * For Keyboard Enter button to work
 *
 * Revision 1.1.2.2.4.2  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.2.4.1  2013/03/08 14:38:41  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.1.2.2  2012/12/15 00:29:18  skreddy
 * CASE201112/CR201113/LOG201121
 * added skip functionality
 *
 * Revision 1.1.2.1  2012/11/23 09:11:21  skreddy
 * CASE201112/CR201113/LOG201121
 * RF version of WO Assembly building process
 *
 * Revision 1.10.2.18.4.5  2012/10/11 10:27:04  gkalla

 * 
 *****************************************************************************/
function PickingLocation(request, response){
	if (request.getMethod() == 'GET') {
		var getFetchedBeginLocation = request.getParameter('custparam_beginLocation');
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('Error', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6;

		if( getLanguage == 'es_ES')
		{
			st1 = "IR A LA UBICACI&#211;N:";
			st2 = "INGRESAR / ESCANEO DE LA UBICACI&#211;N";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";
			st5 = "OVERRIDE";
			st6 = "SKIP";
		}
		else
		{
			st1 = "GO TO LOCATION: ";
			st2 = "ENTER/SCAN LOCATION ";
			st3 = "SEND";
			st4 = "PREV";
			st5 = "OVERRIDE";
			st6 = "SKIP";
		}


		var getwoid = request.getParameter('custparam_woid');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocation = request.getParameter('custparam_beginLocation');
		var getItem = request.getParameter('custparam_item');
		var getItemName = request.getParameter('custparam_itemname');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var getFetchedLocation = request.getParameter('custparam_beginLocationname');
		var vClusterNo = request.getParameter('custparam_clusterno');
		var vBatchNo = request.getParameter('custparam_batchno');	
		var whLocation = request.getParameter('custparam_whlocation');
		var whCompany = request.getParameter('custparam_whcompany');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var pickType=request.getParameter('custparam_picktype');

		nlapiLogExecution('Error', 'before pickType in loc get', pickType);

		var OrdNo=request.getParameter('name');
		var ContainerSize=request.getParameter('custparam_containersize');
		var vSOId=request.getParameter('custparam_ebizordno');
		var getZoneNo=request.getParameter('custparam_ebizzoneno');
		nlapiLogExecution('Error', 'getItemInternalId', getItemInternalId);
		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');
		nlapiLogExecution('Error', 'before getFetchedLocation', getFetchedLocation);
		nlapiLogExecution('Error', 'before getItemInternalId', getItemInternalId);
		if(getFetchedLocation==null || getFetchedLocation == "")
		{
			getFetchedLocation=NextLocation;
		}

		if(getItemInternalId==null || getItemInternalId == "")
		{
			getItemInternalId=NextItemInternalId;
		}
		//code added by santosh on 7Aug2012
		var Type='';
		if(request.getParameter('custparam_type') !=null &&  request.getParameter('custparam_type')!="")
		{
			Type=request.getParameter('custparam_type');
		}
		nlapiLogExecution('Error', 'Type', Type);
		//end of code on 7aug2012
		nlapiLogExecution('Error', 'after getFetchedLocation', getFetchedLocation);
		nlapiLogExecution('Error', 'after getItemInternalId', getItemInternalId);
		nlapiLogExecution('Error', 'getBeginLocation ID', getFetchedBeginLocation);
		nlapiLogExecution('Error', 'getBeginLocation', getFetchedLocation);

		nlapiLogExecution('Error', 'NextItemInternalId', NextItemInternalId); 
		//case# 20127056 starts (Now form name is passed correctly in function to work keyboard enter button)
		var functionkeyHtml=getFunctionkeyScript('_ebiz_rf_wo_piciking_loc'); 
		//case# 20127056 end
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterlocation').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_ebiz_rf_wo_piciking_loc' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + " <label>" + getFetchedLocation + "</label>";
		//html = html + "				<input type='hidden' name='hdntype' value=" + Type + ">";
		html = html + "				<input type='hidden' name='hdnWOid' value=" + getwoid + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value='" + getFetchedLocation + "'>";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemName' value=" + getItemName + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vClusterNo  + ">";
		html = html + "				<input type='hidden' name='hdnBatchNo' value=" + vBatchNo  + ">";	
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnwhCompany' value=" + whCompany + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";	
		html = html + "				<input type='hidden' name='hdnOrdNo' value=" + OrdNo + ">";
		//html = html + "				<input type='hidden' name='hdnsoid' value=" + vSOId + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + ContainerSize + ">";	
		//html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		//html = html + "				<input type='hidden' name='hdnebizzoneno' value=" + getZoneNo + ">";
		html = html + "				<input type='hidden' name='hdnebizzoneno' value=" + getZoneNo + ">";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st2;//ENTER/SCAN LOCATION 
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterlocation' id='enterlocation' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.focus();this.disabled=true; return false'/>";
		html = html + "					" + st4 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		//html = html + "					" + st5 + " <input name='cmdOverride' type='submit' value='F11'/>";
		html = html + "					" + st6 + " <input name='cmdSKIP' type='submit' value='F12'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterlocation').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {

		var getLanguage = request.getParameter('hdngetLanguage');

		var st7;

		if( getLanguage == 'es_ES')
		{
			st7 = "UBICACI&#211;N NO V&#193;LIDA";

		}
		else
		{
			st7 = "INVALID LOCATION";
		}


		var getBeginLocation=request.getParameter('hdnBeginLocation');
		var getBeginLocationId = request.getParameter('hdnBeginLocationId');
		var WOarray = new Array();
		var WOid=request.getParameter('hdnWOid');
		var OrdNo=request.getParameter('hdnOrdNo');
		var ClusNo = request.getParameter('hdnClusterNo');
		var NextShowLocation=request.getParameter('hdnBeginLocation');
		var NextShowItemId=request.getParameter('hdnItemInternalId');
		var vSOId=request.getParameter('hdnsoid');
		var vZoneId=request.getParameter('hdnebizzoneno');
		var vSkipId=request.getParameter('hdnskipid');
		WOarray["custparam_language"] = getLanguage;
		WOarray["custparam_skipid"] = request.getParameter('hdnskipid');
		//code added by santosh on 7Aug2012
		//var Type=request.getParameter('hdntype');//end of code

//		nlapiLogExecution('Error', 'Into Response', 'Into Response');
//		nlapiLogExecution('Error', 'NextShowLocation', NextShowLocation);
//		nlapiLogExecution('Error', 'NextShowItemId', NextShowItemId);
//		nlapiLogExecution('ERROR', 'WOid', WOid);
//		nlapiLogExecution('ERROR', 'OrdNo', OrdNo);
//		nlapiLogExecution('ERROR', 'vZoneId', vZoneId);
//		//nlapiLogExecution('ERROR', 'ClusNo', ClusNo);
//		//nlapiLogExecution('ERROR', 'vSkipId', vSkipId);
//		nlapiLogExecution('ERROR', 'request.getParameter(hdnpicktype)', request.getParameter('hdnpicktype'));
		WOarray["custparam_picktype"] = request.getParameter('hdnpicktype');

		var vPickType=request.getParameter('hdnpicktype');
		nlapiLogExecution('ERROR', 'vPickType',vPickType);
		nlapiLogExecution('ERROR', 'WOid',WOid);
		nlapiLogExecution('ERROR', 'vZoneId',vZoneId);
		nlapiLogExecution('ERROR', 'getBeginLocationId',getBeginLocationId);


		var RecCount;

		if(WOid != null && WOid !=""){
			var filterOpentask = new Array();
			filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', WOid));
			filterOpentask.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
			filterOpentask.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));	
			if(vZoneId!=null && vZoneId!="" && vZoneId!="null")
			{
				filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', vZoneId));
			}
//			if(getBeginLocationId!=null && getBeginLocationId!="" && getBeginLocationId!="null")
//			{
//			filterOpentask.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', getBeginLocationId));
//			}

			/*** The below code is merged from Endochoice account on 07thMar13 by Santosh as part of Standard bundle***/
			//soring order
			/*var WOcolumns = new Array();
			WOcolumns[0] = new nlobjSearchColumn('custrecord_line_no');
			WOcolumns[1] = new nlobjSearchColumn('custrecord_sku');
			WOcolumns[2] = new nlobjSearchColumn('custrecord_expe_qty');
			WOcolumns[3] = new nlobjSearchColumn('custrecord_lpno');
			WOcolumns[4] = new nlobjSearchColumn('custrecord_batch_no');
			WOcolumns[5] = new nlobjSearchColumn('custrecord_actbeginloc');
			WOcolumns[6] = new nlobjSearchColumn('custrecord_act_qty');   
			WOcolumns[7] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
			WOcolumns[8] = new nlobjSearchColumn('custrecord_invref_no');
			WOcolumns[9] = new nlobjSearchColumn('custrecord_wms_location');
			WOcolumns[10] = new nlobjSearchColumn('custrecord_comp_id');
			WOcolumns[11] = new nlobjSearchColumn('custrecord_ebiz_order_no');	
			WOcolumns[12] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			WOcolumns[13] = new nlobjSearchColumn('custrecord_sku_status');	
			WOcolumns[0].setSort();*/

			var WOcolumns = new Array();
			WOcolumns.push(new nlobjSearchColumn('custrecord_line_no'));
			WOcolumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
			WOcolumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
			WOcolumns.push(new nlobjSearchColumn('custrecord_sku'));
			WOcolumns.push(new nlobjSearchColumn('custrecord_expe_qty'));				
			WOcolumns.push(new nlobjSearchColumn('custrecord_lpno'));
			WOcolumns.push(new nlobjSearchColumn('custrecord_batch_no'));
			WOcolumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
			WOcolumns.push(new nlobjSearchColumn('custrecord_act_qty'));   
			WOcolumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
			WOcolumns.push(new nlobjSearchColumn('custrecord_invref_no'));
			WOcolumns.push(new nlobjSearchColumn('custrecord_wms_location'));
			WOcolumns.push(new nlobjSearchColumn('custrecord_comp_id'));
			WOcolumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));				
			WOcolumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));	
			WOcolumns.push(new nlobjSearchColumn('custrecord_sku_status'));	


			WOcolumns[1].setSort();
			WOcolumns[2].setSort();
			WOcolumns[3].setSort();
			WOcolumns[4].setSort(true);
			/*** up to here ***/

			nlapiLogExecution('ERROR', 'Before Search Record', TimeStampinSec());
			var WOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterOpentask,WOcolumns);	
			nlapiLogExecution('ERROR', 'After Search Record', TimeStampinSec());

			if(WOSearchResults!=null && WOSearchResults!='' && WOSearchResults.length>0)
			{
				if(WOSearchResults.length <= parseInt(vSkipId))
				{
					vSkipId=0;
				}
				var SearchResult = WOSearchResults[parseInt(vSkipId)];
				if(WOSearchResults.length > parseInt(vSkipId) + 1)
				{
					var WOSearchnextResult = WOSearchResults[parseInt(vSkipId) + 1];
					WOarray["custparam_nextlocation"] = WOSearchnextResult.getText('custrecord_actbeginloc');
					WOarray["custparam_nextiteminternalid"] = WOSearchnextResult.getValue('custrecord_ebiz_sku_no');
					WOarray["custparam_nextitem"] = WOSearchnextResult.getText('custrecord_sku');
					WOarray["custparam_nextserialno"] = WOSearchnextResult.getValue('custrecord_batch_no');
					nlapiLogExecution('ERROR', 'Next Location', WOarray["custparam_nextlocation"]);
					nlapiLogExecution('ERROR', 'Next SerialNo', WOarray["custparam_nextserialno"]);
				}
				else
				{
					var WOSearchnextResult = WOSearchResults[0];
					WOarray["custparam_nextlocation"] = WOSearchnextResult.getText('custrecord_actbeginloc');
					WOarray["custparam_nextiteminternalid"] = WOSearchnextResult.getValue('custrecord_ebiz_sku_no');
					WOarray["custparam_nextitem"] = WOSearchnextResult.getText('custrecord_sku');
					WOarray["custparam_nextserialno"] = WOSearchnextResult.getValue('custrecord_batch_no');
				}

				WOarray["custparam_woid"] =WOid; 
				WOarray["custparam_iteminternalid"] = SearchResult.getValue('custrecord_ebiz_sku_no');
				WOarray["custparam_item"] = SearchResult.getText('custrecord_sku');
				WOarray["custparam_recordinternalid"] = SearchResult.getId();
				WOarray["custparam_expectedquantity"] = SearchResult.getValue('custrecord_expe_qty');
				WOarray["custparam_beginLocation"] = SearchResult.getValue('custrecord_actbeginloc');
				WOarray["custparam_itemdescription"] = SearchResult.getValue('custrecord_skudesc');
				WOarray["custparam_dolineid"] = SearchResult.getValue('custrecord_ebiz_cntrl_no');
				WOarray["custparam_invoicerefno"] = SearchResult.getValue('custrecord_invref_no');
				WOarray["custparam_orderlineno"] = SearchResult.getValue('custrecord_line_no');
				WOarray["custparam_beginlocationname"] = SearchResult.getText('custrecord_actbeginloc');
				WOarray["custparam_batchno"] = SearchResult.getValue('custrecord_batch_no');
				WOarray["custparam_containerlpno"] = SearchResult.getValue('custrecord_lpno');
				WOarray["custparam_whlocation"] = SearchResult.getValue('custrecord_wms_location');
				WOarray["custparam_whcompany"] = SearchResult.getValue('custrecord_comp_id');
				WOarray["custparam_noofrecords"] = SearchResult.length;
				WOarray["custparam_ebizordno"] =  SearchResult.getValue('custrecord_ebiz_order_no');
				WOarray["custparam_itemstatus"] =  SearchResult.getValue('custrecord_sku_status');
				WOarray["custparam_ebizzoneno"] =  vZoneId;
				nlapiLogExecution('ERROR', 'SKU internalId', SearchResult.getValue('custrecord_ebiz_sku_no'));
				nlapiLogExecution('ERROR', 'SKU internalId', SearchResult.getText('custrecord_ebiz_sku_no'));

			}
		}
		var getEnteredLocation = request.getParameter('enterlocation');		
//		nlapiLogExecution('Error', 'Entered Location', getEnteredLocation);	
//		nlapiLogExecution('Error', 'Location', getBeginLocation);	
//		nlapiLogExecution('Error', 'Name',  WOarray["name"]);	
		var vClusterNo = request.getParameter('hdnClusterNo');

		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');
		var optskipEvent = request.getParameter('cmdSKIP');

		nlapiLogExecution('ERROR', 'getEnteredLocation.toUpperCase()', getEnteredLocation.toUpperCase());
		nlapiLogExecution('ERROR', 'getBeginLocation.toUpperCase()', getBeginLocation.toUpperCase());


		WOarray["custparam_error"] = st7;//'INVALID LOCATION';
		WOarray["custparam_screenno"] = 'WOLoc';

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to accept SO #.
		if (optedEvent == 'F7') {	

			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_summary', 'customdeploy_ebiz_rf_wo_summary_di', false, WOarray);


		}
		else if(optskipEvent == 'F12')
		{
			vSkipId= parseInt(vSkipId) + 1;
			nlapiLogExecution('Error', 'vSkipId in skip event',  vSkipId);
			WOarray["custparam_skipid"] = vSkipId;		
			WOarray["custparam_beginLocationname"]=null;
			WOarray["custparam_iteminternalid"]=null;
			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_location', 'customdeploy_ebiz_rf_wo_picking_loc_di', false, WOarray);
		}
		else {
			//To Remove case sensitive
			if (getEnteredLocation != '' && getEnteredLocation.toUpperCase() == getBeginLocation.toUpperCase()) {
				var LocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null, new nlobjSearchFilter('name', null, 'is', getEnteredLocation));

				//nlapiLogExecution('Error', 'Length of Location Search', LocationSearch.length);

				if (LocationSearch !=null && LocationSearch !='' && LocationSearch.length != 0) {
					for (var s = 0; s < LocationSearch.length; s++) {
						var getEndLocationInternalId = LocationSearch[s].getId();
						nlapiLogExecution('Error', 'End Location Id', getEndLocationInternalId);
					}

					WOarray["custparam_beginlocationname"] = getBeginLocation;
					WOarray["custparam_beginLocationname"] = getBeginLocation;// case# 201412324
					WOarray["custparam_endlocinternalid"] = getEndLocationInternalId;

					//To Remove case sensitive
					//SOarray["custparam_endlocation"] = getEnteredLocation;
					WOarray["custparam_endlocation"] = getBeginLocation;
					WOarray["custparam_clusterno"] = vClusterNo;	
					//nlapiLogExecution('Error', 'getBeginLocation',WOarray["custparam_beginLocation"]);
					var getBeginLocationid=	WOarray["custparam_beginLocation"];
					var wave=request.getParameter('hdnWOid');
					//nlapiLogExecution('Error', 'wave', request.getParameter('hdnWOid'));

					WOarray["custparam_remqty"]=0;

					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_item', 'customdeploy_ebiz_rf_wo_picking_item_di', false, WOarray);
					//response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_confirm', 'customdeploy_ebiz_rf_wo_pick_confirm_di', false, WOarray);
					nlapiLogExecution('Error', 'Done customrecord', 'Success');
				}
				else {
					WOarray["custparam_beginlocationname"]=NextShowLocation;
					WOarray["custparam_nextlocation"]=NextShowLocation;	
					WOarray["custparam_nextiteminternalid"]=NextShowItemId;	
					nlapiLogExecution('Error', 'NextShowLocation : ', NextShowLocation);
					nlapiLogExecution('Error', 'Error: ', 'Scanned a wrong location');
					nlapiLogExecution('Error', 'NextShowItemId : ', NextShowItemId);
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, WOarray);

				}
			}
			else if (getEnteredLocation != '' && getEnteredLocation.toUpperCase() != getBeginLocation.toUpperCase()) {
				var locationfound='F';
				if (WOSearchResults != null && WOSearchResults.length > 0) {
					for (var l = 0; locationfound=='F' && l < WOSearchResults.length; l++) {

						var actBeginLocation = WOSearchResults[l].getText('custrecord_actbeginloc');
						//To Remove case sensitive
						nlapiLogExecution('Error', 'getEnteredLocation : ', getEnteredLocation.toUpperCase());
						nlapiLogExecution('Error', 'actBeginLocation : ', actBeginLocation.toUpperCase());

						if((getEnteredLocation.toUpperCase()==actBeginLocation.toUpperCase())){

							locationfound='T';

							var pikgenresults=WOSearchResults[l];

							WOarray["custparam_woid"] = request.getParameter('hdnWOid');
							WOarray["custparam_recordinternalid"] = pikgenresults.getId();
							WOarray["custparam_containerlpno"] = pikgenresults.getValue('custrecord_lpno');
							WOarray["custparam_expectedquantity"] = pikgenresults.getValue('custrecord_expe_qty');
							WOarray["custparam_beginLocation"] = pikgenresults.getValue('custrecord_actbeginloc');
							WOarray["custparam_item"] = pikgenresults.getValue('custrecord_sku');
							WOarray["custparam_itemname"] = pikgenresults.getText('custrecord_sku');
							WOarray["custparam_itemdescription"] = pikgenresults.getValue('custrecord_skudesc');
							WOarray["custparam_iteminternalid"] = pikgenresults.getValue('custrecord_ebiz_sku_no');
							WOarray["custparam_dolineid"] = pikgenresults.getValue('custrecord_ebiz_cntrl_no');
							WOarray["custparam_invoicerefno"] = pikgenresults.getValue('custrecord_invref_no');
							WOarray["custparam_orderlineno"] = pikgenresults.getValue('custrecord_line_no');
							WOarray["custparam_beginLocationname"] = pikgenresults.getText('custrecord_actbeginloc');
							WOarray["custparam_batchno"] = pikgenresults.getValue('custrecord_batch_no');
							WOarray["custparam_whlocation"] = pikgenresults.getValue('custrecord_wms_location');
							WOarray["custparam_whcompany"] = pikgenresults.getValue('custrecord_comp_id');
							WOarray["name"] =  pikgenresults.getValue('name');
							WOarray["custparam_containersize"] =  pikgenresults.getValue('custrecord_container');
							WOarray["custparam_ebizordno"] =  pikgenresults.getValue('custrecord_ebiz_order_no');
							getBeginLocation = pikgenresults.getText('custrecord_actbeginloc');

							var vNextRecs=0;

							nlapiLogExecution('Error', 'Loop # : ', l+1);
							nlapiLogExecution('Error', 'WOSearchResults Length : ', WOSearchResults.length);

							if(l+1 < WOSearchResults.length)
								vNextRecs=l+1;
							else 
								vNextRecs=0;

							nlapiLogExecution('Error', 'vNextRecs : ', vNextRecs);

							if(WOSearchResults[vNextRecs].getText('custrecord_actbeginloc') != null && WOSearchResults[vNextRecs].getText('custrecord_actbeginloc') !='')
								WOarray["custparam_nextlocation"] = WOSearchResults[vNextRecs].getText('custrecord_actbeginloc');
							else
								WOarray["custparam_nextlocation"]='';

							if(WOSearchResults[vNextRecs].getValue('custrecord_ebiz_sku_no') != null && WOSearchResults[vNextRecs].getValue('custrecord_ebiz_sku_no') != '')	
								WOarray["custparam_nextiteminternalid"] = WOSearchResults[vNextRecs].getValue('custrecord_ebiz_sku_no');
							else
								WOarray["custparam_nextiteminternalid"]='';

							if(WOSearchResults[vNextRecs].getValue('custrecord_expe_qty') != null && WOSearchResults[vNextRecs].getValue('custrecord_expe_qty') != '')
								WOarray["custparam_nextexpectedquantity"] = WOSearchResults[vNextRecs].getValue('custrecord_expe_qty');
							else
								WOarray["custparam_nextexpectedquantity"]=0;

							nlapiLogExecution('ERROR', 'Next Location', WOarray["custparam_nextlocation"]);
							nlapiLogExecution('ERROR', 'Next Item Intr Id', WOarray["custparam_nextiteminternalid"]);


							var LocationSearch = nlapiSearchRecord('customrecord_ebiznet_location', null, new nlobjSearchFilter('name', null, 'is', getEnteredLocation));

							if (LocationSearch.length != 0) {
								for (var s = 0; s < LocationSearch.length; s++) {
									var getEndLocationInternalId = LocationSearch[s].getId();
									nlapiLogExecution('Error', 'End Location Id', getEndLocationInternalId);
								}

								WOarray["custparam_beginlocationname"] = getBeginLocation;
								WOarray["custparam_beginLocationname"] = getBeginLocation;// case# 201412324
								WOarray["custparam_endlocinternalid"] = getEndLocationInternalId;

								//To Remove case sensitive
								//SOarray["custparam_endlocation"] = getEnteredLocation;								 
								WOarray["custparam_endlocation"] = actBeginLocation;
								WOarray["custparam_clusterno"] = vClusterNo;	


								/*** The below code is merged from Endochoice account on 07thMar13 by Santosh as part of Standard bundle***/
								for (var t = 0; t < WOSearchResults.length; t++) {
									var actBegLocation = WOSearchResults[t].getText('custrecord_actbeginloc');

									if((getEnteredLocation.toUpperCase()==actBegLocation.toUpperCase())){
										nlapiSubmitField('customrecord_ebiznet_trn_opentask', WOSearchResults[t].getId(), 'custrecord_skiptask', 'Y');
									}

								} /*** up to here ***/
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_item', 'customdeploy_ebiz_rf_wo_picking_item_di', false, WOarray);					
							}						
						}
						//The below code is commented by Satish.N on 08/07/2012
//						else if(locationfound=='F')
//						{
//						var skiptask=SOSearchResults[l].getValue('custrecord_skiptask');
//						nlapiLogExecution('Error', 'skiptask1 : ', skiptask);
//						nlapiSubmitField('customrecord_ebiznet_trn_opentask', SOSearchResults[l].getId(), 'custrecord_skiptask', 'Y');

//						}	
						//Upto here.
					}
					if(locationfound=='F'){
						WOarray["custparam_beginlocationname"]=NextShowLocation;
						WOarray["custparam_nextlocation"]=NextShowLocation;	
						WOarray["custparam_nextiteminternalid"]=NextShowItemId;

						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, WOarray);
					}	
				}
			}
			else 
			{
				WOarray["custparam_beginlocationname"]=NextShowLocation;
				WOarray["custparam_nextlocation"]=NextShowLocation;	
				WOarray["custparam_nextiteminternalid"]=NextShowItemId;
				nlapiLogExecution('Error', 'NextShowLocation : ', NextShowLocation);
				nlapiLogExecution('Error', 'Error: ', 'Did not scan the location');
				nlapiLogExecution('Error', 'NextShowItemId : ', NextShowItemId);
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, WOarray);				
			}
		}
	}
}
