 function eBizfnOutboundStageInventoryMain(request, response)
{
	if (request.getMethod() == 'GET') 
	{
		eBizfnInventoryDetails();
	}
}

function eBiz_Hook_fn()
{
	//To Delete fully billed status records from eBiz Inventory(Outbound stage inventory records)
	eBizfnInventoryDetails();
}

function eBizfnInventoryDetails()
{var Filter = new Array();
//18=FLAG.INVENTORY.OUTBOUND
Filter[0] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', 18);	
Filter[1] = new nlobjSearchFilter('custrecord_invttasktype', null, 'anyof', 3);
Filter[2] = new nlobjSearchFilter('custrecord_ebiz_transaction_no', null, 'isnotempty');
//Filter[3] = new nlobjSearchFilter('custrecord_ebiz_inv_note1', null,'isnot', 'fullyBilled');
//	Filter[3] = new nlobjSearchFilter('custrecord_ebiz_inv_note1', null,'is', 'fullyBilled');
//Filter[3] = new nlobjSearchFilter('custrecord_ebiz_inv_note1', null,'is', 'closed');

	//Filter[1] = new nlobjSearchFilter('lastmodified',null,'before',vCurrentDate)	
//Filter[4] = new nlobjSearchFilter('lastmodified',null,'before',DateStamp())
Filter[3] = new nlobjSearchFilter('mainline', 'custrecord_ebiz_transaction_no','is', 'T');
//For Billed,Pending Billed
//Transfer Order:Pending Receipt:TrnfrOrd:F
//Transfer Order:Received:TrnfrOrd:G
Filter.push(new nlobjSearchFilter('status', 'custrecord_ebiz_transaction_no', 'anyof', ['SalesOrd:G','SalesOrd:F','SalesOrd:H','TrnfrOrd:G','TrnfrOrd:F']));	
var Column = new Array();
Column[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
Column[1] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
Column[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
Column[3] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
Column[4] = new nlobjSearchColumn('custrecord_ebiz_qoh');
Column[5] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
Column[6] = new nlobjSearchColumn('custrecord_invttasktype');
Column[7] = new nlobjSearchColumn('custrecord_ebiz_transaction_no');

Column[0].setSort(true);
Column[1].setSort(true);



var SearchRecord = nlapiSearchRecord('customrecord_ebiznet_createinv', null, Filter, Column);

var vContLP = "";
var vRecIntID = "";
var orderno="";
if(SearchRecord != null && SearchRecord != "")
{
	nlapiLogExecution('ERROR','In Create Inventory For Loop',SearchRecord.length);
	for( var count = 0; count < SearchRecord.length; count++)
	{
		//nlapiLogExecution('ERROR','In Create Inventory For Loop',count);
		vRecIntID = SearchRecord[count].getId();
		
		vContLP = SearchRecord[count].getValue('custrecord_ebiz_inv_lp');
		//nlapiLogExecution('ERROR','Container LP#',vContLP);
		
		orderno=SearchRecord[count].getValue('custrecord_ebiz_transaction_no');	
try
{
//vStatus = nlapiLookupField('transaction', orderno, 'status');	
//nlapiLogExecution('ERROR','vStatus ',vStatus );

//eBizfnUpdateChildRecords(vRecIntID,vStatus,orderno);	
eBizfnDeleteInventoryRecord(vRecIntID);
}
catch(exp)
{}
	}
}

}

//To verify the vContLP (custrecord_ebiz_inv_lp) value is in Open Task / Closed Task
function eBizTransaction(vContLP, vRecIntID)
{
	var vOrder = "";
	var vStatus = "";
	var vBoolDeleteRecord = false;
	
	var FilterClose = new Array();
	var ColumnClose = new Array();
	
	ColumnClose [0] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_contlp_no');
	ColumnClose [1] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_order_no');
	
	FilterClose[0] = new nlobjSearchFilter('custrecord_ebiztask_ebiz_contlp_no', null, 'is', vContLP);
    	//Task Type:: 3 - PICK
	FilterClose[1] = new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'is', 3);
	FilterClose[2] = new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no', null, 'noneof', '@NONE@');

	
	//** Container LP details verification in Closed Task
	var SearchCloseRecord = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, FilterClose, ColumnClose);
	
	if(SearchCloseRecord != null && SearchCloseRecord != "")
	{
		vBoolDeleteRecord = true;
		vOrder = SearchCloseRecord[0].getValue('custrecord_ebiztask_ebiz_order_no');
	}
	else
	{
		var FilterOpen = new Array();
		var ColumnOpen = new Array();
		
		ColumnOpen [0] = new nlobjSearchColumn('custrecord_container_lp_no');
		ColumnOpen [1] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		
		FilterOpen [0] = new nlobjSearchFilter('custrecord_container_lp_no', null, 'is',vContLP);
        	FilterOpen [1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', 3); // 3-PICK
        	FilterOpen [2] = new nlobjSearchFilter('custrecord_ebiz_order_no',null, 'noneof', '@NONE@');

		
        	//** Container LP details verification in Open Task if it is not found in Closed Task
		var SearchOpenRecord = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, FilterOpen, ColumnOpen);
		
		if(SearchOpenRecord != null&&SearchOpenRecord !="")
		{
			vBoolDeleteRecord = false;
			vOrder = SearchOpenRecord[0].getValue('custrecord_ebiz_order_no');
		}
		else
		{
			vOrder = null;
		}			
	}
	
	nlapiLogExecution('ERROR','vOrder ', vOrder);
		
	if( (vOrder != null) && (vOrder != "") )
	{		
		vStatus = nlapiLookupField('transaction', vOrder, 'status');
		nlapiLogExecution('ERROR','Order Status ', vStatus.toUpperCase() );
		
		vStatus = vStatus.toUpperCase();
		
		if( (vStatus == 'CLOSED') || (vStatus == 'FULLYBILLED') || (vStatus == 'RECEIVED') || (vStatus == 'PENDINGRECEIPT') || (vStatus == 'PENDINGBILLING') )
		{
			 vBoolDeleteRecord = true;	
			// eBizfnDeleteInventoryRecord(vRecIntID)
			 //** For now call eBizfnUpdateChildRecords to update the status even for eligible records. Later we can call delete function
			 eBizfnUpdateChildRecords(vRecIntID, vStatus, vOrder)
		}
		else
		{
			 vBoolDeleteRecord = false;
			 eBizfnUpdateChildRecords(vRecIntID, vStatus, vOrder)
		}		
	}
	
}

function eBizfnDeleteInventoryRecord(vRecIntID)
{
	vChildRecID = nlapiDeleteRecord('customrecord_ebiznet_createinv', vRecIntID);
	nlapiLogExecution('ERROR', 'Deleted Record', vChildRecID);
}

function eBizfnUpdateChildRecords(vId, vStatus, vOrder)
{
	var values = new Array();
	var fields = new Array();
	
	var vCurrentDate = DateStamp();
	var vNotes = "";
	vNotes = 'Outbound Clear Inventory Script Executed on '+ vCurrentDate; 
	
	fields[0] = 'custrecord_ebiz_inv_note1'; 
	values[0] = vStatus;
	
	fields[1] = 'custrecord_ebiz_inv_note2'; 
	values[1] = vNotes;//'V1';
	
	fields[2] = 'custrecord_ebiz_transaction_no';
	values[2] = vOrder;
	 
	var updatefields = nlapiSubmitField('customrecord_ebiznet_createinv', vId, fields, values);
	nlapiLogExecution('ERROR', 'Updated Record ID', vId);
}

function DateStamp()
{
	var now = new Date();
	return ((parseInt(now.getMonth()) + 1) + '/' + (parseInt(now.getDate())) + '/' + now.getFullYear());
}


/*
function eBizfnTesCode()
{
    var globalorderstatus = 'X';
    	
	var context = nlapiGetContext();
	nlapiLogExecution('ERROR','Remaining Usage',context.getRemainingUsage());
	   var CurrDate =DateStamp();
	var Filter = new Array();
	//Filter[0] = new nlobjSearchFilter('custrecord_ebiz_alloc_qty', null, 'isempty');
	//Filter[2] = new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'equal',0);
	//17=FLAG.INVENTORY.INBOUND //18=FLAG.INVENTORY.OUTBOUND //19=FLAG.INVENTORY.STORAGE

	Filter[0] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', 18);//18=FLAG.INVENTORY.OUTBOUND
	//Filter[1] = new nlobjSearchFilter('custrecord_ebiz_inv_note2', null, 'isempty');
	Filter[1] = new nlobjSearchFilter('lastmodified',null,'before',CurrDate)	
	var Column = new Array();
	
	Column[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	Column[1] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	Column[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	Column[3] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	Column[4] = new nlobjSearchColumn('custrecord_ebiz_qoh');
	Column[5] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
	Column[6] = new nlobjSearchColumn('custrecord_invttasktype');
	
	Column[0].setSort(true);
	Column[1].setSort(true);
	
	var SearchRecord = nlapiSearchRecord('customrecord_ebiznet_createinv', null, Filter, Column);
	
	var vBoolDeleteRecord = false;
	var vContLP = "";
	
	nlapiLogExecution('ERROR','Before IF','Before IF');

	if(SearchRecord != null && SearchRecord != "")
	{
		nlapiLogExecution('ERROR','in IF','in IF');

		for( var count = 0; count < 300; count++)
		{
			nlapiLogExecution('ERROR','in loop',count);
			var vRecIntID = SearchRecord[count].getId();
			//Here, we need to write function to verify the 'custrecord_ebiz_inv_lp' value is in Open Task or Closed Task
			vContLP = SearchRecord[count].getValue('custrecord_ebiz_inv_lp');
			
			//To verify whether the LP is shipped or not
			//vBoolDeleteRecord = eBizfnTask(vContLP);
			
			nlapiLogExecution('ERROR','vContLP',vContLP);
			
			//vBoolDeleteRecord = eBizTransaction(vContLP);
			
			vBoolDeleteRecordStr = eBizTransaction(vContLP);
			vBoolDeleteRecordStrArray = vBoolDeleteRecordStr.split('-');
			vBoolDeleteRecord = vBoolDeleteRecordStrArray[0];
			globalorderstatus = vBoolDeleteRecordStrArray[1];
			
			//nlapiLogExecution('ERROR','vBoolDeleteRecord',vBoolDeleteRecord);
			
			nlapiLogExecution('ERROR','vBoolDeleteRecord ', vBoolDeleteRecord);
				
			if(vBoolDeleteRecord == 'true')
			{
				eBizfnDeleteInventoryRecord(vRecIntID);
			}
			else
			{
				eBizfnUpdateChildRecords(vRecIntID, globalorderstatus);
			}

		}
	}
	
}
*/
