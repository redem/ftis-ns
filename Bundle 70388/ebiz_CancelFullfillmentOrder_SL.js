/***************************************************************************
�����������������������eBizNET Solutions Inc
****************************************************************************
*
*� $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_CancelFullfillmentOrder_SL.js,v $
*� $Revision: 1.3.6.1.4.1.6.2 $
*� $Date: 2015/04/13 09:18:12 $
*� $Author: snimmakayala $
*� $Name: t_eBN_2015_1_StdBundle_1_41 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_CancelFullfillmentOrder_SL.js,v $
*� Revision 1.3.6.1.4.1.6.2  2015/04/13 09:18:12  snimmakayala
*� 201412193
*�
*� Revision 1.3.6.1.4.3  2015/04/13 09:16:05  snimmakayala
*� 201412193
*�
*� Revision 1.3.6.1.4.2  2014/06/27 10:17:26  sponnaganti
*� case# 20149022
*� New UI Compatability issue fix
*�
*� Revision 1.3.6.1.4.1  2013/03/06 09:48:26  gkalla
*� CASE201112/CR201113/LOG201121
*� Lexjet Files merging for Standard bundle
*�
*� Revision 1.3.6.1  2012/10/30 06:10:08  spendyala
*� CASE201112/CR201113/LOG201121
*� Merged code form 2012.2 branch.
*�
*� Revision 1.3  2011/12/16 06:46:17  spendyala
*� CASE201112/CR201113/LOG201121
*� added functionality for display button using client script
*�
*� Revision 1.2  2011/12/14 07:20:47  spendyala
*� CASE201112/CR201113/LOG201121
*� changed the SO field and FO field to text box.
*�
*� Revision 1.1  2011/12/08 07:38:48  spendyala
*� CASE201112/CR201113/LOG201121
*� created a new script to delete the fullfillment order depending upon the sales order or fullfillment order no.
*�
*
****************************************************************************/


/**
 * @param request
 * @param response
 */
function CancelFullfillmentOrder(request, response){
	if (request.getMethod() == 'GET') 
	{
		var form=nlapiCreateForm('Delete Fulfillment Order');
		var tempflag=form.addField('custpage_tempflag','text','tempory flag').setDisplayType('hidden').setDefaultValue('true');
		var salesOrderfield=form.addField('custpage_salesorderno','text','Sales Order#');
		var msgflag=request.getParameter('custpage_flag');
		nlapiLogExecution('ERROR','msgflag',msgflag);
		if(msgflag=='success')
		{
			var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Fulfillment Order line Deleted Successfully','100%', null, null, null);</script></div>");
		}
		else if(msgflag=='error')
		{
			var SO=request.getParameter('custpage_so');
			var msg= form.addField('custpage_message', 'inlinehtml', null, null, null);
			var msgtext = "Invalid Sales Order# : "+SO;
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Error', '"+ msgtext + "', NLAlertDialog.TYPE_HIGH_PRIORITY,  '100%', null, null, null);</script></div>");
		}
		var fullfillmentOrderfield=form.addField('custpage_fullfillmentorder','text','Fulfillment Order#');
		form.setScript('customscript_ebiz_cancel_fullorder_cl');
		form.addSubmitButton('Display');
		response.writePage(form);
	}
	else
	{
		var tempflag=request.getParameter('custpage_tempflag');
		nlapiLogExecution('ERROR','tempflag',tempflag);
		if(tempflag!=null&&tempflag!="")
		{
			try{
				var SO=request.getParameter('custpage_salesorderno');
				var FO=request.getParameter('custpage_fullfillmentorder'); 
				nlapiLogExecution('ERROR','SO',SO);
				nlapiLogExecution('ERROR','FO',FO);
				if(SO!=null&&SO!="")
				{
					var SoId=GetSOInternalId(SO);
					nlapiLogExecution('ERROR','SoId',SoId);
					if(SoId!=null && SoId!="")
					{
						nlapiLogExecution('ERROR','ifSoId',SoId);
						//Displaysublist(SoId,FO);
						Displaysublist(SoId,FO,response);
					}
					else
					{
						nlapiLogExecution('ERROR','elseSoId',SoId);
						DisplayErrorMsg(SO);
					}

				}
				else
				{
					nlapiLogExecution('ERROR','elseSOnull',SO);
					//Displaysublist(SoId,FO);
					Displaysublist(SoId,FO,response);
				}
			}
			catch(exe)
			{
				nlapiLogExecution('ERROR','Exeception',exe);
			}
		}
		else
		{
			var form=nlapiCreateForm('Delete Fulfillment Order');
			var lineCnt = request.getLineItemCount('custpage_fullfillmentorder');
			nlapiLogExecution('ERROR','lineCnt',lineCnt);
			for ( var line = 0; line < lineCnt; line++) {
				nlapiLogExecution('ERROR','for','for');
				var confirmFlag= request.getLineItemValue('custpage_fullfillmentorder', 'custpage_confirm', line+1);	
				nlapiLogExecution('ERROR','confirmFlag',confirmFlag);
				if(confirmFlag=='T')
				{
					var recInternalId=request.getLineItemValue('custpage_fullfillmentorder', 'custpage_rec_internalid',line+1);
					var id=nlapiDeleteRecord('customrecord_ebiznet_ordline',recInternalId);
					nlapiLogExecution('ERROR','ID',id);
				}
			}
			var SOarray=new Array();
			SOarray ["custpage_flag"] = 'success';
			response.sendRedirect('SUITELET', 'customscript_ebiz_cancel_fullfillment_sl', 'customdeploy_ebiz_cancel_fullfillment_dl', false, SOarray );
		}
	}
}

/**
 * @param SOText
 * @returns
 */
function GetSOInternalId(SOText)
{
	var filter=new Array();
	filter.push(new nlobjSearchFilter('tranid',null,'is',SOText.toString()));
	filter.push(new nlobjSearchFilter('mainline',null,'is','T'));

	var searchrec=nlapiSearchRecord('salesorder',null,filter,null);
	if(searchrec!=null && searchrec !="")
	{
		var ActualSoID=searchrec[0].getId();
		nlapiLogExecution('ERROR','ActualSoID',ActualSoID);
	}
	return ActualSoID;
}

 /**
 * @param SoId
 * @param FO
 */
//function Displaysublist(SoId,FO)
function Displaysublist(SoId,FO,response)
{
	 nlapiLogExecution('ERROR','displaysublist',SoId);
	 nlapiLogExecution('ERROR','displaysublist',FO);
	 var form=nlapiCreateForm('Delete Fulfillment Order');
	 var salesOrderfield=form.addField('custpage_salesorderno','text','Sales Order#');
	 var fullfillmentOrderfield=form.addField('custpage_fullfillmentorder','text','Fullfillment Order#');
	 form.setScript('customscript_ebiz_cancel_fullorder_cl');
	 var temporyflag=form.addField('custpage_tempflag','text','tempory flag').setDisplayType('hidden');
	 var sublist = form.addSubList("custpage_fullfillmentorder", "list", "Fulfillment Order");
	 sublist.addField("custpage_confirm", "checkbox", "Delete");
	 sublist.addField("custpage_salesorder", "text", "Sales Order#");
	 sublist.addField("custpage_fullfillmentorder", "text", "Fulfillment Order#");
	 sublist.addField("custpage_ordline", "text", "Order Line#");
	 sublist.addField("custpage_item", "text", "Item");
	 sublist.addField("custpage_ordqty", "text", "Order Qty");
	 sublist.addField("custpage_wmsstatus", "text", "WMS Status");
	 sublist.addField("custpage_createddate", "text", "Created Date");
	 sublist.addField("custpage_rec_internalid", "text", "internal id").setDisplayType('hidden');
	 sublist.addField("custpage_pickgenqty", "text", "Pickgen Qty").setDisplayType('hidden');
	 sublist.addField("custpage_wmsstatusid", "text", "WMS Status ID").setDisplayType('hidden');
	 sublist.addField("custpage_shipqty", "text", "Ship Qty").setDisplayType('hidden');
	 var salesorder="";
	 var orderline="";
	 var item="";
	 var fullfillmentorder="";
	 var orderqty="";
	 var wmsstatus="";
	 var wmsstatusID="";
	 var createddate="";
	 var pickgenqty="";
	 var shipqty="";

	 var filter=new Array();

	 if(SoId!=null && SoId!="")
		 filter.push(new nlobjSearchFilter('custrecord_ns_ord',null,'is',SoId));
	 
	 if(FO!=null && FO!="")
		 filter.push(new nlobjSearchFilter('custrecord_lineord',null,'is',FO));
	 
	 // WMS Status Flag : 25-Edit, 26-Picks Failed
	// filter.push(new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [25,26]));

	 var column=new Array();
	 column[0]=new nlobjSearchColumn('custrecord_ns_ord');
	 column[1]=new nlobjSearchColumn('custrecord_ebiz_linesku');
	 column[2]=new nlobjSearchColumn('custrecord_lineord');
	 column[3]=new nlobjSearchColumn('custrecord_ordline');
	 column[4]=new nlobjSearchColumn('custrecord_ord_qty');
	 column[5]=new nlobjSearchColumn('custrecord_linestatus_flag');
	 column[6]=new nlobjSearchColumn('custrecord_record_linedate');
	 column[7]=new nlobjSearchColumn('custrecord_pickgen_qty');
	 column[8]=new nlobjSearchColumn('custrecord_ship_qty');
	 

	 var searchrecord=nlapiSearchRecord('customrecord_ebiznet_ordline',null,filter,column);

	 if(searchrecord!=null && searchrecord!="")
	 {
		 for ( var count = 0; count < searchrecord.length; count++)
		 {
			 searchrec=searchrecord[count];
			 salesorder=searchrec.getText('custrecord_ns_ord');
			 item=searchrec.getText('custrecord_ebiz_linesku');
			 fullfillmentorder=searchrec.getValue('custrecord_lineord');
			 orderline=searchrec.getValue('custrecord_ordline');
			 orderqty=searchrec.getValue('custrecord_ord_qty');
			 wmsstatus=searchrec.getText('custrecord_linestatus_flag');
			 createddate=searchrec.getValue('custrecord_record_linedate');
			 wmsstatusID=searchrec.getValue('custrecord_linestatus_flag');
			 pickgenqty=searchrec.getValue('custrecord_pickgen_qty');
			 shipqty=searchrec.getValue('custrecord_ship_qty');
			 
			 form.getSubList('custpage_fullfillmentorder').setLineItemValue('custpage_salesorder', count + 1, salesorder);
			 form.getSubList('custpage_fullfillmentorder').setLineItemValue('custpage_ordline', count + 1, orderline);
			 form.getSubList('custpage_fullfillmentorder').setLineItemValue('custpage_item', count + 1, item);			
			 form.getSubList('custpage_fullfillmentorder').setLineItemValue('custpage_fullfillmentorder', count + 1, fullfillmentorder);			 
			 form.getSubList('custpage_fullfillmentorder').setLineItemValue('custpage_ordqty', count + 1, orderqty);
			 form.getSubList('custpage_fullfillmentorder').setLineItemValue('custpage_wmsstatus', count + 1, wmsstatus);
			 form.getSubList('custpage_fullfillmentorder').setLineItemValue('custpage_createddate', count + 1, createddate);
			 form.getSubList('custpage_fullfillmentorder').setLineItemValue('custpage_rec_internalid', count + 1, searchrec.getId());
			 form.getSubList('custpage_fullfillmentorder').setLineItemValue('custpage_pickgenqty', count + 1, pickgenqty);
			 form.getSubList('custpage_fullfillmentorder').setLineItemValue('custpage_wmsstatusid', count + 1, wmsstatusID);
			 form.getSubList('custpage_fullfillmentorder').setLineItemValue('custpage_shipqty', count + 1, shipqty);
		 }
	 }
	 form.addButton('custpage_displaybtn','Display','Display()');
	 form.addSubmitButton('Delete');
	 response.writePage(form);
 }
 
 /**
 * @param SO
 */
function DisplayErrorMsg(SO)
 {
	 var SOarray=new Array();
	 SOarray ["custpage_flag"] = 'error';
	 SOarray ["custpage_so"]=SO;
	 response.sendRedirect('SUITELET', 'customscript_ebiz_cancel_fullfillment_sl', 'customdeploy_ebiz_cancel_fullfillment_dl', false, SOarray );
 }


