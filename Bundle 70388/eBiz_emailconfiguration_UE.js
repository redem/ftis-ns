/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/UserEvents/eBiz_emailconfiguration_UE.js,v $
 *     	   $Revision: 1.2.2.2 $
 *     	   $Date: 2015/05/11 22:21:12 $
 *     	   $Author: svanama $
 *     	   $Name: t_eBN_2015_1_StdBundle_1_101 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: eBiz_emailconfiguration_UE.js,v $
 * Revision 1.2.2.2  2015/05/11 22:21:12  svanama
 * no message
 *
  

 *****************************************************************************/
function passwordencryption(type){



	var vsalesOrderId="";
	var vcontainerLp="";
	var vebizOrdNo;
	var userId="";
	nlapiLogExecution('Debug', 'Log in iteration1', type);

	try{

		if(type=='create' || type=='edit')
		{
			var newRecord = nlapiGetNewRecord();
			var recid=newRecord.getId();
			var newemailpassword=nlapiGetFieldValue('custrecord_ebiz_emailpassword');//

			//Uptohere

			var oldRecord = nlapiGetOldRecord();
			var olddeviceuploadflag='F'
				var	oldemailpassword=nlapiGetFieldValue('custrecord_ebiz_emailpassword');
			var oldemailpwdlength=0;

			if(oldemailpassword!=null && oldemailpassword!='')
			{
				var oldemailpwdlength=oldemailpassword.length;
			}

			var str = 'newemailpassword. = ' + newemailpassword + '<br>';
			str = str + 'oldemailpassword. = ' + oldemailpassword + '<br>';
			str = str + 'oldemailpassword lenght. = ' + oldemailpwdlength + '<br>';


			nlapiLogExecution('Debug', 'Log in iteration1', str);

			var emailrec= nlapiLoadRecord('customrecord_email_config', recid);

			//custrecord_name of the custom free form text
			//to encrypt the value before saving in the database

			var encryptedstring= nlapiEncrypt(newemailpassword, "aes", "48656C6C6F0B0B0B0B0B0B0B0B0B0B0B");

			//to get the value of the encrypted text
			//var s = nlapiGetFieldValue('custrecord_name');
			//var decrypted= nlapiDecrypt(s , "aes", "48656C6C6F0B0B0B0B0B0B0B0B0B0B0B");

			if(newemailpassword!=null && newemailpassword!='')
			{			
				nlapiLogExecution('Debug', 'Encryptedstring is', encryptedstring);
				if(emailrec!=null && emailrec!='')
				{
					//The below code is for Carousal - REMSTAR integration.
					if(oldemailpwdlength<=15)
					{
						emailrec.setFieldValue('custrecord_ebiz_emailpassword',encryptedstring);
						var emailRecId = nlapiSubmitRecord(emailrec, false, true);
						nlapiLogExecution('ERROR', 'emailrec is',emailRecId + ' is Success');
					}
				}
			}
		}

	}


	catch(exp)
	{
		nlapiLogExecution('Debug', 'Exception in saving', exp);
	}


}

function encryptstring(passwordstring)
{
	// Create Base64 Object
	var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);
	i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},
	decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");
	while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}
	if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";
	for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}
	else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}
	else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}


	// Define the string
	var encryptstring = passwordstring;

	// Encode the String
	var encodedString = Base64.encode(encryptstring);
	// Outputs: "SGVsbG8gV29ybGQh"

	return encodedString;
}

function decryptstring(passwordstring)
{
	// Create Base64 Object
	var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);
	i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},
	decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");
	while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}
	if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";
	for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}
	else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}
	else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}


	// Define the string
	var decryptstring = passwordstring;

	// Decode the String
	var decodedString = Base64.decode(decryptstring);
	return decodedString;
	// Outputs: "Hello World!"
}

