/***************************************************************************
  eBizNET Solutions Inc               
 ****************************************************************************
 *
 * $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/ebiz_RF_CycleCountSKU.js,v $
 * $Revision: 1.9.2.6.4.6.2.29.2.1 $
 * $Date: 2015/11/16 15:36:44 $
 * $Author: rmukkera $
 * $Name: t_WMS_2015_2_StdBundle_1_152 $
 *
 * DESCRIPTION
 * Functionality
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_CycleCountSKU.js,v $
 * Revision 1.9.2.6.4.6.2.29.2.1  2015/11/16 15:36:44  rmukkera
 * case # 201415115
 *
 * Revision 1.9.2.6.4.6.2.29  2015/07/30 22:18:08  skreddy
 * Case# 201413656
 * 2015.2  issue fix
 *
 * Revision 1.9.2.6.4.6.2.28  2015/06/24 15:08:44  grao
 * SW issue fixes  201411235
 *
 * Revision 1.9.2.6.4.6.2.27  2015/06/23 13:10:11  grao
 * SW issue fixes  201413181
 *
 * Revision 1.9.2.6.4.6.2.26  2015/06/22 15:39:48  grao
 * SB issue fixes  201413181
 *
 * Revision 1.9.2.6.4.6.2.25  2015/05/18 15:54:44  nneelam
 * case# 201412616
 *
 * Revision 1.9.2.6.4.6.2.24  2015/04/30 13:09:41  schepuri
 * case# 201412516
 *
 * Revision 1.9.2.6.4.6.2.23  2015/01/22 07:34:32  skreddy
 * Case# 201411446
 * OI Prod issue fix
 *
 * Revision 1.9.2.6.4.6.2.22  2015/01/06 06:35:11  schepuri
 * issue#   201411235
 *
 * Revision 1.9.2.6.4.6.2.21  2014/11/06 15:12:27  sponnaganti
 * Case# 201410955
 * Stnd Bundle Issue fix
 *
 * Revision 1.9.2.6.4.6.2.20  2014/10/16 14:18:47  vmandala
 * Case# 201410657 std bundle issue fixed
 *
 * Revision 1.9.2.6.4.6.2.19  2014/09/26 15:29:07  sponnaganti
 * Case# 201410549
 * AlphaComm SB Issue fix
 *
 * Revision 1.9.2.6.4.6.2.18  2014/09/26 14:21:22  skavuri
 * Case# 201410522 Std Bundle issue Fixed
 *
 * Revision 1.9.2.6.4.6.2.17  2014/09/02 10:53:11  gkalla
 * case#20149227
 * ACE and JB cycle count issues
 *
 * Revision 1.9.2.6.4.6.2.16  2014/07/10 07:14:43  skavuri
 * Case# 20149268 Compatibility Issue Fixed
 *
 * Revision 1.9.2.6.4.6.2.15  2014/06/13 10:04:30  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.9.2.6.4.6.2.14  2014/05/30 00:34:20  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.9.2.6.4.6.2.13  2014/02/26 13:53:37  sponnaganti
 * case# 20127376
 *  (passing hdnItemName to CCarray["custparam_expitem"] to invalid item screen)
 *
 * Revision 1.9.2.6.4.6.2.12  2013/12/30 15:34:04  rmukkera
 * Case # 20126584
 *
 * Revision 1.9.2.6.4.6.2.11  2013/12/27 13:53:01  schepuri
 * 20126237
 *
 * Revision 1.9.2.6.4.6.2.10  2013/12/24 16:14:46  gkalla
 * case#20126523
 * Standard bundle issue
 *
 * Revision 1.9.2.6.4.6.2.9  2013/10/24 12:28:31  nneelam
 * Case# 20125186
 * cycle count new sku issue fix..
 *
 * Revision 1.9.2.6.4.6.2.8  2013/08/30 16:40:48  skreddy
 * Case# 20124145
 * standard bundle issue fix
 *
 * Revision 1.9.2.6.4.6.2.7  2013/08/14 06:36:26  grao
 * TSG SB: Issue fixes for accepting new item if expected item is null at the time of empty location or adding new item
 *
 * Revision 1.9.2.6.4.6.2.6  2013/08/03 21:16:08  snimmakayala
 * Case# 201214994
 * Cycle Count Issue Fixes
 *
 * Revision 1.9.2.6.4.6.2.5  2013/07/03 15:08:40  grao
 * TSG Sb issue Fixes
 * 20123235
 * SKU Validation in RF Cycle count
 *
 * Revision 1.9.2.6.4.6.2.4  2013/06/19 14:30:10  schepuri
 * Cycle count upc code  scanning issue
 *
 * Revision 1.9.2.6.4.6.2.3  2013/04/17 16:02:35  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.9.2.6.4.6.2.2  2013/03/01 14:34:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.9.2.6.4.6.2.1  2013/02/26 13:02:23  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Marged from Boombah.
 *
 * Revision 1.9.2.6.4.6  2013/02/20 22:30:57  kavitha
 * CASE201112/CR201113/LOG201121
 * Cycle count process - Issue fixes
 *
 * Revision 1.9.2.6.4.5  2013/02/01 07:58:15  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Cyclecountlatestfixed
 *
 * Revision 1.9.2.6.4.3  2012/10/05 06:34:12  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting Multiple language into given suggestions
 *
 * Revision 1.9.2.6.4.2  2012/09/26 12:34:15  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.9.2.6.4.1  2012/09/24 10:08:56  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.9.2.6  2012/09/03 13:09:11  schepuri
 * CASE201112/CR201113/LOG201121
 * Previous button is driven according to LP Required Rule Value.
 *
 * Revision 1.9.2.5  2012/07/31 10:40:52  schepuri
 * CASE201112/CR201113/LOG201121
 * Invalid item issue
 *
 * Revision 1.9.2.4  2012/04/30 10:02:37  spendyala
 * CASE201112/CR201113/LOG201121
 * While Searching of Item in ItemMaster,
 * 'name' filter is changed to 'nameinternal' and also checking weather the item is inactive state or not.
 *
 * Revision 1.9.2.3  2012/04/19 15:07:43  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Blind Cycle Count
 *
 * Revision 1.9.2.2  2012/03/16 14:08:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.9.2.1  2012/02/21 13:23:47  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.9  2011/12/08 07:30:10  spendyala
 * CASE201112/CR201113/LOG201121
 * added functionality to get the item internal id which is entered by the user against the actual item field.
 *
 *
 ****************************************************************************/


/**
 * @author LN
 *@version
 *@date
 */
function CycleCountSKU(request, response){
	if (request.getMethod() == 'GET') {

		var getLanguage = request.getParameter('custparam_language');
		var st1,st2,st3,st4,st5,st6,st7,st8;

		if( getLanguage == 'es_ES')
		{		
			st1 = "ART&#205;CULO ESPERADO:";
			st2 = "INGRESAR / ESCANEAR DEL ART&#205;CULO:";
			st3 = "CONT";
			st4 = "ANTERIOR";
			st5 = "INVENTARIO C&#205;CLICO SKU";

		}
		else
		{
			st1 = "EXP ITEM:";
			st2 = "ENTER/SCAN ACTUAL ITEM:";			
			st3 = "CONT";
			st4 = "PREV";
			st5 = "CYCLE COUNT SKU";

		}

		var getPlanNo = request.getParameter('custparam_planno');
		nlapiLogExecution('ERROR', 'getPlanNo', getPlanNo);

		var getRecordId = request.getParameter('custparam_recordid');
		var getBeginLocationId = request.getParameter('custparam_begin_location_id');
		var getBeginLocationName =  request.getParameter('custparam_begin_location_name');
		var getPackCode =  request.getParameter('custparam_packcode');
		var getExpectedQuantity =  request.getParameter('custparam_expqty');
		var getExpLPNo = request.getParameter('custparam_lpno');
		var getActLPNo = request.getParameter('custparam_actlp');
		var getExpItem = request.getParameter('custparam_expitem');
		var getExpItemDescription = request.getParameter('custparam_expitemdescription');
		var itemInternalId = request.getParameter('custparam_expiteminternalid');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var getItemStatus = request.getParameter('custparam_skustatus');
		var rulevalue =  request.getParameter('custparam_ruleValue');
var getSiteId=request.getParameter('custparam_siteid');
		nlapiLogExecution('DEBUG', 'getBeginLocation', NextLocation);
		nlapiLogExecution('DEBUG', 'RecordCount', RecordCount);
		nlapiLogExecution('ERROR', 'itemInternalId', itemInternalId);
		nlapiLogExecution('ERROR', 'getPackCode', getPackCode);
	nlapiLogExecution('ERROR', 'getSiteId', getSiteId);

		var blindItemRule=false;
		var vRoleLocation=getRoledBasedLocationNew();
		// case no 20126237
		nlapiLogExecution('ERROR', 'vRoleLocation', vRoleLocation);
		var sysruleFilters = new Array();//
		sysruleFilters.push(new nlobjSearchFilter('name', null, 'is', 'CYCN - Blind Item'));
		if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 'null' && vRoleLocation != 0)
		{
			vRoleLocation.push('@NONE@');
			if(vRoleLocation.indexOf(parseInt(getSiteId))!=-1)
			{				
				sysruleFilters.push(new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', vRoleLocation));
			}
		}
		
		var sysruleColumns = new Array();
		sysruleColumns[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
	var ruleValue = '';
		var systemRulesSearchresults = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, sysruleFilters, sysruleColumns);
		if(systemRulesSearchresults!=null && systemRulesSearchresults.length>0)
		{
		// case# 201411235
			for(var i=0;i<systemRulesSearchresults.length ;i++)
			{
				/*if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 'null' && vRoleLocation != 0)
				{
					if(vRoleLocation.indexOf(parseInt(getSiteId))!=-1)
					{
						ruleValue=systemRulesSearchresults[i].getValue('custrecord_ebizrulevalue');
						nlapiLogExecution('ERROR', 'ruleValue', ruleValue);
					}
				}*/
				
				ruleValue=systemRulesSearchresults[i].getValue('custrecord_ebizrulevalue');
				nlapiLogExecution('ERROR', 'ruleValue', ruleValue);
				
			}
			if(ruleValue=='N')
			{
				blindItemRule=false;
			}
			else
			{
				blindItemRule=true;
			}
		}
		else
		{
			blindItemRule=false;
		}

		if( blindItemRule==true)
		{
			getExpItem='';
		}

		var functionkeyHtml=getFunctionkeyScript('_rf_cyclecountsku'); 
		var html = "<html><head><title>" + st5 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";      
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('entersku').focus();";        
		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		//html = html + "	  alert(document.getElementById('cmdSend').disabled);";

		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";
		html = html +functionkeyHtml;
		html=html+"<SCRIPT LANGUAGE='javascript'>";
		html=html+"function noenter() {  return !(window.event && window.event.keyCode == 13); }</script>";
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cyclecountsku' method='POST'>";
		html = html + "		<table>";
		if( blindItemRule==false)
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st1 + "</td></tr>";
			html = html + "				<tr><td align = 'left'> <label>" + getExpItem + "</label></td></tr>";
		}
		html = html + "				<tr><td><input type='hidden' name='hdnItemName' value='" + getExpItem + "'></td>";
		html = html + "				<tr><td><input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + "></td>";
		html = html + "				<input type='hidden' name='hdnCycleCountPlanNo' value=" + getPlanNo + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationName' value=" + getBeginLocationName + ">";
		html = html + "				<input type='hidden' name='hdnRecordId' value=" + getRecordId + ">";
		html = html + "				<input type='hidden' name='hdnPackCode' value=" + getPackCode + ">";
		html = html + "				<input type='hidden' name='hdnItemStatus' value=" + getItemStatus + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";		
		html = html + "				<input type='hidden' name='hdnExpLPNo' value=" + request.getParameter('custparam_lpno') + "></td>";
		html = html + "				<input type='hidden' name='hdnActLPNo' value=" + request.getParameter('custparam_actlpno') + "></td>";
		html = html + "				<input type='hidden' name='hdnExpectedItemDescription' value=" + getExpItemDescription + ">";
		html = html + "				<input type='hidden' name='hdniteminternalid' value=" + itemInternalId + ">";   
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnnext' value=" + NextLocation + ">";
		html = html + "				<input type='hidden' name='hdnrulevalue' value=" + rulevalue + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				<input type='hidden' name='hdnvRoleLocation' value=" + vRoleLocation + ">";//added by surendra	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entersku' id='entersku' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false' style='width:40;height:40;'/>";
		html = html + "					"+ st4 +" <input name='cmdPrevious' type='submit' value='F7' style='width:40;height:40;'/>";
		html = html + "					SKIP <input name='cmdSkip' type='submit' value='F8' style='width:40;height:40;'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('entersku').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);

	}
	else 
	{
		try {
			var CCarray = new Array();

			var getLanguage = request.getParameter('hdngetLanguage');
			CCarray["custparam_language"] = getLanguage;

			nlapiLogExecution('ERROR', 'getLanguage', CCarray["custparam_language"]);


			var st6;
			if( getLanguage == 'es_ES')
			{
				st6 = "ART&#205;CULO INV&#193;LIDO";

			}
			else
			{
				st6 = "INVALID ITEM";
			}

			var optedEvent = request.getParameter('cmdPrevious');
			var optedEvent1 = request.getParameter('cmdSkip');
			var getActItem = request.getParameter('entersku');
			nlapiLogExecution('ERROR','getActItem',getActItem); 
			CCarray["custparam_error"] = st6;
			CCarray["custparam_screenno"] = '31';
			CCarray["custparam_recordid"] = request.getParameter('custparam_recordid');
			CCarray["custparam_planno"] = request.getParameter('custparam_planno');
			CCarray["custparam_begin_location_id"] = request.getParameter('custparam_begin_location_id');
			CCarray["custparam_begin_location_name"] = request.getParameter('custparam_begin_location_name');
			CCarray["custparam_lpno"] = request.getParameter('custparam_lpno');
			CCarray["custparam_packcode"] = request.getParameter('custparam_packcode');
			CCarray["custparam_skustatus"] = request.getParameter('custparam_skustatus');
			CCarray["custparam_expqty"] = request.getParameter('custparam_expqty');
			CCarray["custparam_actlp"] = request.getParameter('custparam_actlp');
			CCarray["custparam_expitem"] = request.getParameter('custparam_expitem');
			CCarray["custparam_planitem"] = request.getParameter('custparam_expitem');
//case	201412516
            CCarray["custparam_siteid"] =request.getParameter('custparam_siteid');
			//Case 20125186 start
			//Case # 20126584 Start
			if (getActItem !=null && getActItem !='' && getActItem != request.getParameter('custparam_expitem'))
			{//Case # 20126584 End
				CCarray["custparam_expitem"] = getActItem;
			}
			else
			{
				CCarray["custparam_expitem"] = request.getParameter('custparam_expitem');
			}
			// Case end
			CCarray["custparam_expitemdescription"] = request.getParameter('custparam_expitemdescription');
			CCarray["custparam_expiteminternalid"] = request.getParameter('hdniteminternalid');
			CCarray["custparam_noofrecords"] = request.getParameter('hdnRecCount');
			CCarray["custparam_nextlocation"] = request.getParameter('hdnnext');
			CCarray["custparam_ruleValue"] = request.getParameter('hdnrulevalue');

			var ruleValue=request.getParameter('hdnrulevalue');

			if (optedEvent == 'F7') 
			{
				nlapiLogExecution('ERROR', 'Cycle Count SKU F7 Pressed');
				nlapiLogExecution('ERROR', 'Cycle Count SKU F7 Pressed CCarray["custparam_expiteminternalid"]',CCarray["custparam_expiteminternalid"]);
				if(ruleValue=='Y')
				{
					response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_lp', 'customdeploy_rf_cyclecount_lp_di', false, CCarray);
				}
				else
				{
					response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_location', 'customdeploy_rf_cyclecount_location_di', false, CCarray);
				}
			}
			else if(optedEvent1=='F8')
			{
				nlapiLogExecution('ERROR','SKIP','SKIPthetask');
				nlapiLogExecution('ERROR', 'getRecordId', CCarray["custparam_recordid"]);
				var CCRec = nlapiLoadRecord('customrecord_ebiznet_cyclecountexe', request.getParameter('custparam_recordid'));
				var skipcount=CCRec.getFieldValue('custrecord_cyc_skip_task');
				nlapiLogExecution('ERROR','skipcount',skipcount);

				if(skipcount=='' || skipcount==null)
				{
					skipcount=0;
				}
				skipcount=parseInt(skipcount)+1;
				CCRec.setFieldValue('custrecord_cyc_skip_task',skipcount);
				var id=	nlapiSubmitRecord(CCRec,true);
				nlapiLogExecution('ERROR','skipid',id);


				response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_location', 'customdeploy_rf_cyclecount_location_di', false, CCarray);
			}
			else
			{
				//TSG Issue fixes case#:20123260
				//start
// case # 20124145  start and end
				if (getActItem  != '') //end
				{	
					nlapiLogExecution('ERROR','IF','chktpt');

					//*** The following code is merged from Bommbah on 02-26-2013 by Satish.N ***//

					/*var filter=new Array();
					filter.push(new nlobjSearchFilter('name',null,'is',getActItem.toString()));

					var searchrec=nlapiSearchRecord('item',null,filter,null);*/

					// Changed On 30/4/12 by Suman
//					var filter=new Array();
//					filter.push(new nlobjSearchFilter('nameinternal', null, 'is', getActItem.toString()));
//					filter.push(new nlobjSearchFilter('isinactive',null, 'is','F'));

//					var searchrec=nlapiSearchRecord('item',null,filter,null);
					// End of Changes as On 30/4/12
					nlapiLogExecution('ERROR','getActItem',getActItem);
					var actItemid = validateSKUId(getActItem,null,null);
					nlapiLogExecution('ERROR', 'After validateSKU',actItemid);

					//*** Upto here ***//

					if(actItemid[1]!=null && actItemid[1] !="")
					{
						var Actualitemid=actItemid[1];
						nlapiLogExecution('ERROR','ACTUAL ITEMID',Actualitemid);
						nlapiLogExecution('ERROR','ACTUAL ITEM TEXT',actItemid[2]);
						//Case# 201410522 starts
						//CCarray["custparam_expitem"] = getActItem; // Case# 20149268
						var aliasItemName=actItemid[2];
						//CCarray["custparam_expitem"] = aliasItemName;
						//case# 201410955 if item alias scanning then we are displaying itemname else actitem
						if(aliasItemName!=null && aliasItemName!="")
							CCarray["custparam_expitem"] = aliasItemName;
						else if(actItemid[2] !=null && actItemid[2]!='' )
						{
							CCarray["custparam_expitem"] = actItemid[2];
						}
						else
						{
							CCarray["custparam_expitem"] = getActItem;
						}
						//Case# 201410522 ends
						CCarray["custparam_actitem"] = Actualitemid;
						nlapiLogExecution('ERROR', 'Actual Item/Actual ItemInternalID', getActItem+'/'+Actualitemid);
						nlapiLogExecution('ERROR', 'CCarray["custparam_expiteminternalid"]', CCarray["custparam_expiteminternalid"]);
					
						//Getting system rule Is Packcode Skip Required? added by surendra on 13-08-14
						var vRoleLocation=request.getParameter('hdnvRoleLocation');
						var blindPackcodeRule="N";
						var sysruleFilters1 = new Array();
						sysruleFilters1.push(new nlobjSearchFilter('name', null, 'is', 'Is Packcode Skip Required?'));
						if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 'null'  && vRoleLocation != 0)						{
							  //case 201410657 start
							//vRoleLocation.push('@NONE@');
							//case 201410657 end
							sysruleFilters1.push(new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', vRoleLocation));
						}
						
						var sysruleColumns1 = new Array();
						sysruleColumns1[0] = new nlobjSearchColumn('custrecord_ebizrulevalue');
						var systemRulesSearchresults1 = nlapiSearchRecord('customrecord_ebiznet_sysrules', null, sysruleFilters1, sysruleColumns1);
						if(systemRulesSearchresults1!=null && systemRulesSearchresults1.length>0)
						{
							var ruleValue=systemRulesSearchresults1[0].getValue('custrecord_ebizrulevalue');
							nlapiLogExecution('ERROR', 'ruleValue of pack code', ruleValue);
							if(ruleValue=='N')
							{
								blindPackcodeRule="N";
							}
							else
							{
								blindPackcodeRule="Y";
							}
						}
						else
						{
							blindPackcodeRule="N";
						}
						nlapiLogExecution('ERROR', 'blindPackcodeRule', blindPackcodeRule);
						
						CCarray["custparam_packcoderequired"]=blindPackcodeRule;
					
						nlapiLogExecution('ERROR','blindPackcodeRule in post',blindPackcodeRule);
						//response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_packcode', 'customdeploy_rf_cyclecount_packcode_di', false, CCarray);
					
						if(blindPackcodeRule=="Y")
						{
							nlapiLogExecution('ERROR', 'Cycle Count Forward to quantity screen');
							
							
							nlapiLogExecution('ERROR', 'Item Internal Id',Actualitemid);
							if(Actualitemid !=null && Actualitemid !='')
							{
								var ItemType = nlapiLookupField('item', Actualitemid, ['recordType','custitem_ebizbatchlot']);
								
								nlapiLogExecution('ERROR', 'ItemType',ItemType.recordType );
								nlapiLogExecution('ERROR', 'Batch/Lot',ItemType.custitem_ebizbatchlot );
								if (ItemType.recordType == 'lotnumberedinventoryitem' ||ItemType.custitem_ebizbatchlot=='T' ) {
									response.sendRedirect('SUITELET', 'customscript_rf_cyclecountbatch', 'customdeploy_rf_cyclecountbatch_di', false, CCarray);
									return;
								}

							}
							
							response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_quantity', 'customdeploy_rf_cyclecount_quantity_di', false, CCarray);
						}
						else
						{
							nlapiLogExecution('ERROR', 'Cycle Count Forward to Packcode screen');
							response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_packcode', 'customdeploy_rf_cyclecount_packcode_di', false, CCarray);
						}
						//end
					}
					else
					{
						nlapiLogExecution('ERROR','ELSE of invalid item','chktpt');
						//case# 20127376 starts (passing hdnItemName to CCarray["custparam_expitem"] to invalid item screen)
						nlapiLogExecution('ERROR','CCarray["custparam_expitem"]',request.getParameter('hdnItemName'));
						CCarray["custparam_expitem"]=request.getParameter('hdnItemName');
						//case# 20127376 end
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
						nlapiLogExecution('DEBUG', 'Cycle Count invalid item');
					}
				}
				else 
				{
					nlapiLogExecution('ERROR','ELSE','chktpt');
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
					nlapiLogExecution('DEBUG', 'Cycle Count SKU not found');
				}
			}
		}
		catch(e)
		{
			nlapiLogExecution('ERROR','Catch','chktpt');
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
			nlapiLogExecution('DEBUG', 'Catch: Cycle Count SKU not found');
		}
	}
}
