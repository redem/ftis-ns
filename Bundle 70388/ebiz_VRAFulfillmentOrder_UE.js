function AutoAdjustFulfillmentOrders(type, form, request)
{

	var context = nlapiGetContext();
	nlapiLogExecution('DEBUG','Remaining usage at the start of AutoAdjustFulfillmentOrders ',context.getRemainingUsage());
	var status=nlapiGetFieldValue('status');
	var orderstatus=nlapiGetFieldValue('orderstatus');
	var orderstatustext=nlapiGetFieldText('orderstatus');
	nlapiLogExecution('DEBUG','status',status);
	nlapiLogExecution('DEBUG','orderstatus',orderstatus);
	nlapiLogExecution('DEBUG','orderstatustext',orderstatustext);
	
	try
	{
		nlapiLogExecution('DEBUG','Into AutoAdjustFulfillmentOrders',type);
		if(type !=  'delete' && context.getExecutionContext() != 'webstore')
		{
			var create_fulfillment_order = nlapiGetFieldValue('custbody_create_fulfillment_order');

			var soid = nlapiGetRecordId();
			nlapiLogExecution('DEBUG','soid',soid);
			var trantype = nlapiLookupField('transaction', soid, 'recordType');
			nlapiLogExecution('DEBUG','trantype',trantype);
			var searchresults = nlapiLoadRecord(trantype, soid); 
			if(searchresults !=null && searchresults!='')
			{
				var orderStatus=searchresults.getFieldValue('status');
				if(orderStatus !=null && orderStatus!='' &&((orderStatus=='Pending Return')||(orderStatus=='Partially Returned')||(orderStatus=='Pending Credit/Partially Returned')))
				{
					var soname = searchresults.getFieldValue('tranid');
					var shipcomplete = searchresults.getFieldValue('shipcomplete');
					var location = searchresults.getFieldValue('location');
					/***Below code is merged from Lexjet production account on 4th March 2013 by Ganesh for FO creation based on linelevel shipmethod***/
					var HeadShipmethod = searchresults.getFieldValue('shipmethod');
					var ItemLineShipping = searchresults.getFieldValue('ismultishipto');
					/***Upto here***/
					var toLocation="";
					if(trantype=="transferorder")
						toLocation= searchresults.getFieldValue('transferlocation');
					nlapiLogExecution('DEBUG','soname (AutoAdjustFulfillmentOrders)',soname);
					nlapiLogExecution('DEBUG','soid (AutoAdjustFulfillmentOrders)',soid);
					nlapiLogExecution('DEBUG','create_fulfillment_order (AutoAdjustFulfillmentOrders)',create_fulfillment_order);
					nlapiLogExecution('DEBUG','ItemLineShipping',ItemLineShipping);



					if(type=='create' && context.getExecutionContext() == 'webservices')
					{
						for(var i =0; i < nlapiGetLineItemCount('item'); i++)
						{
							var LineItem = nlapiGetLineItemValue('item', 'item', i + 1);
							var LineLocation = nlapiGetLineItemValue('item', 'location', i + 1);
							var LineItemStatus = nlapiGetLineItemValue('item', 'custcol_ebiznet_item_status', i + 1);
							nlapiLogExecution('ERROR','LineItem',LineItem);
							nlapiLogExecution('ERROR','LineLocation',LineLocation);
							nlapiLogExecution('ERROR','toLocation',toLocation);
							nlapiLogExecution('ERROR','LineItemStatus',LineItemStatus);
							var vTempLocation=LineLocation;
							if(LineLocation == null || LineLocation == '')
								vTempLocation=location;
							nlapiLogExecution('ERROR','vTempLocation',vTempLocation);
							if(LineItem != null && LineItem != '' && vTempLocation != null && vTempLocation != '' && (LineItemStatus == null || LineItemStatus==''))
							{	
								if(trantype=='transferorder')
									var searchresult = SetItemStatus("TransferOrder", LineItem, vTempLocation, toLocation);
								else
									var searchresult = SetItemStatus(trantype, LineItem, vTempLocation, null);
								nlapiLogExecution('ERROR','searchresult',searchresult);
								if (searchresult != null && searchresult != '') 
								{
									//nlapiSetLineItemValue('item', 'custcol_ebiznet_item_status', i+1, searchresult[0]);

									searchresults.setLineItemValue('item','custcol_ebiznet_item_status',i+1,searchresult[0]);
									if(searchresult[1] != null && searchresult[1] != '')
									{
										//nlapiSetLineItemValue('item', 'custcol_nswmspackcode',i+1, searchresult[1]);
										searchresults.setLineItemValue('item','custcol_nswmspackcode',i+1,searchresult[1]);
									}
								}
							}

						}
						//nlapiSubmitRecord(searchresults, true);	
					}
					if(create_fulfillment_order != null && create_fulfillment_order == 'T' )
					{	
						try
						{
							// To lock sales order
							if(soname != null && soname != '')
							{	
								nlapiLogExecution('DEBUG','soname before Locking',soname);
								nlapiLogExecution('DEBUG','trantype before Locking',trantype);
								var fulfillRec= nlapiCreateRecord('customrecord_ebiznet_lockrecs');
								fulfillRec.setFieldValue('name', soname);					
								fulfillRec.setFieldValue('custrecord_ebiznet_trantype', trantype);
								fulfillRec.setFieldValue('externalid', soname);
								fulfillRec.setFieldValue('custrecord_ebiznet_notes', 'Locked by UE');
								nlapiSubmitRecord(fulfillRec,false,true );
								nlapiLogExecution('DEBUG','Locked successfully');
							}
						}
						catch(e)
						{
							nlapiLogExecution('DEBUG', 'DEBUG', e.getCode());
							var wmsE = nlapiCreateError('FULFILLMENT_ORDER_RACE_COND_DETECTED', 'Another thread is currently processing this fulfillment order, which prevents your request from executed.', true);
							throw wmsE;
						}
						try
						{

							if(type == 'edit' || type=='create')  
							{	
								var nooflines = nlapiGetLineItemCount('item');

								nlapiLogExecution('DEBUG','nooflines',nooflines);

								if(nooflines>100)
								{
									var committedlinescount = getCommittedlinescount(soid);

									nlapiLogExecution('DEBUG','committedlinescount',committedlinescount);

									if(committedlinescount>100)
									{
										var ordsize = nlapiGetFieldValue('custbody_ebiz_ord_size');

										nlapiLogExecution('DEBUG','ordsize',ordsize);

										if(ordsize!='L')
										{
											nlapiSubmitField('salesorder',soid,'custbody_ebiz_ord_size','L');
										}

										return;
									}
								}

								// Delete the fulfillment order lines for those sales order lines deleted by 
								//user by clicking the button 'Remove' on SO screen
								
								var getAllLineNo=nlapiGetFieldValue('custbody_ebiz_lines_deleted');
								nlapiLogExecution('DEBUG','getAllLineNo',getAllLineNo);
								var shipcomplete = nlapiGetFieldValue('shipcomplete');
								if(getAllLineNo != null)
									deleteEditedLine(soid);

								var solinearray = new Array();
								//delete the record which are having the same SO,Lineno and with status E
								for(var i =0; i < nlapiGetLineItemCount('item'); i++)
								{
									var salesOrderLineNo = nlapiGetLineItemValue('item', 'line', i + 1);
									var qtyEntered = nlapiGetLineItemValue('item', 'quantity', i + 1);

									var currow = [salesOrderLineNo,qtyEntered];

									solinearray.push(currow);

//									if(salesOrderLineNo != null)
//									DeleteRecCreated(soid, salesOrderLineNo,shipcomplete);

//									nlapiLogExecution('DEBUG','qtyEntered',qtyEntered);
//									updateFulfillmentOrderByQtyEntered(salesOrderLineNo,soid,qtyEntered);
								}

								if(solinearray != null && solinearray!='' && solinearray.length>0)
								{
									DeleteOpenFOlines(soid, solinearray,shipcomplete);
									//updateFulfillmentOrderByQtyEntered(solinearray,soid);
								}
							}

							var cnt=searchresults.getLineItemCount('item');
							var createfo='T';
							//Case# 20124404Ã¯Â¿Â½ Start 

							try
							{
								if(trantype == 'salesorder')
									createfo ='T';// fnCreateFo(soid);
							}
							catch(exp)
							{
								nlapiLogExecution('DEBUG','Exception in fnCreateFo',exp);
								createfo='T';
							}

							nlapiLogExecution('DEBUG','createfo',createfo);
							//Case# 20124404Ã¯Â¿Â½ end 
							// The below code is for shipcomplete functionality - 11/09/2011
							if(createfo=='T' && shipcomplete=='T')
							{
								for (var j = 1; j <= parseFloat(cnt); j++) 
								{	
								//	var commtdqty = searchresults.getLineItemValue('item','quantitycommitted',j);
									var orderqty = searchresults.getLineItemValue('item', 'quantity', j); 
									//var fulfildQty = searchresults.getLineItemValue('item', 'quantityfulfilled', j);
									var linelocation = searchresults.getLineItemValue('item', 'location', j);
									var itemtype = searchresults.getLineItemValue('item', 'itemtype', j);
									var isLineclosed = searchresults.getLineItemValue('item', 'isclosed', j);

									if(linelocation!=null && linelocation!='')
										location=linelocation;

									var mwhsiteflag='F';
									var	islocationAliasExists=CheckLocationAlias(location);
									if(islocationAliasExists!=null && islocationAliasExists!='')
									{
										mwhsiteflag='T';
									}
									else
									{
										if(location!=null && location!='')
										{
											var fields = ['custrecord_ebizwhsite'];

											var locationcolumns = nlapiLookupField('Location', location, fields);
											if(locationcolumns!=null)
												mwhsiteflag = locationcolumns.custrecord_ebizwhsite;
										}
									}
/*
									if(fulfildQty==null || fulfildQty=='' || isNaN(fulfildQty))
									{
										fulfildQty=0;
									}*/
								/*	if(commtdqty==null || commtdqty=='' || isNaN(commtdqty))
									{
										commtdqty=0;
									}
*/
									//var totalcommtdqty=parseFloat(commtdqty)+parseFloat(fulfildQty);

									nlapiLogExecution('DEBUG','itemtype',itemtype);

									if(itemtype!='Description' && itemtype!='Discount' && itemtype!='Payment' && itemtype!='Markup' 
										&& itemtype!='Subtotal' && itemtype!='OthCharge' && itemtype!='Service' && itemtype!='noninventoryitem'
											&& itemtype!='NonInvtPart')
									{
										//case 20127066 : added new Lineclosed condition to If condition 
										if((mwhsiteflag=='T') && (parseFloat(orderqty) > parseFloat(0)) && (isLineclosed != 'T'))
										{
											createfo = 'F';
											continue;
										}
									}
								}
							}
							//upto here
							//alert(createfo);
							if(createfo=='T')
							{
								var alllinesfoqty = getFulfilmentqtyforallLines(soid);
								var foparent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); 
								var custTypearray = new Array();
								var solocationarray = new Array();
								var FOShipmethodArr=new Array();
								for (var t = 1; t <= parseFloat(cnt); t++) 
								{
									var customizationtype = searchresults.getLineItemValue('item','custcol_customizationcode',t);
									if(customizationtype==null || customizationtype=='')
										customizationtype=-1;

									custTypearray.push(customizationtype);
									/***Below code is merged from Lexjet production account on 4th March 2013 by Ganesh for FO creation based on linelevel shipmethod***/
									var vShipMethod=HeadShipmethod;
									var vLineShipmethod = searchresults.getLineItemValue('item','shipmethod',t);
									nlapiLogExecution('DEBUG','vLineShipmethod',vLineShipmethod);
									nlapiLogExecution('DEBUG','vLineShipmethod',searchresults.getLineItemValue('item','shipvia',t));
									nlapiLogExecution('DEBUG','vHeadShipmethod',searchresults.getFieldValue('shipvia'));
									if(vLineShipmethod==null || vLineShipmethod=='')
										vLineShipmethod=vShipMethod;
									if(ItemLineShipping=='T')
										vShipMethod=vLineShipmethod;

									if(vShipMethod==null || vShipMethod=='')
									{
										vShipMethod=-1;								
									}

									FOShipmethodArr.push(vShipMethod);

									/***Upto here ***/
									var solinelocation = searchresults.getLineItemValue('item', 'location', t);
									if(solinelocation == null || solinelocation =='')
									{
										solinelocation = location;
									}	

									if(solinelocation!=null && solinelocation!='')
										solocationarray.push(solinelocation);
								}

								nlapiLogExecution('DEBUG','custTypearray',custTypearray);
								custTypearray=removeDuplicateElement(custTypearray);
								nlapiLogExecution('DEBUG','distinct custTypearray',custTypearray);

								nlapiLogExecution('DEBUG','solocationarray',solocationarray);
								solocationarray=removeDuplicateElement(solocationarray);
								nlapiLogExecution('DEBUG','distinct solocationarray',solocationarray);

								/***Below code is merged from Lexjet production account on 4th March 2013 by Ganesh for FO creation based on linelevel shipmethod***/
								nlapiLogExecution('DEBUG','FOShipmethodArr',FOShipmethodArr);

								//case 20123302 start
								if(FOShipmethodArr!=undefined && FOShipmethodArr.length>0 ) 
									FOShipmethodArr=removeDuplicateElement(FOShipmethodArr);
								nlapiLogExecution('DEBUG','distinct FOShipmethodArr',FOShipmethodArr);
								/***Upto here***/
								//case 20123646  start
								var focreated=0;
								for (var h = 0; h <solocationarray.length; h++) 
								{

									var solocation = solocationarray[h];
									/***Below code is merged from Lexjet production account on 4th March 2013 by Ganesh for FO creation based on linelevel shipmethod***/
									for (var s = 0; s <custTypearray.length; s++) 
									{


										var customizetype = custTypearray[s];

										for(var z=0;z<FOShipmethodArr.length;z++)
										{
											var oldfulFillmentOrdNo='-1';
											var fullFillmentorderno='-1';	

											var FOShipmethod = FOShipmethodArr[z];

											for (var i = 1; i <= parseFloat(cnt); i++) 
											{
												var linecustomizetype = searchresults.getLineItemValue('item','custcol_customizationcode',i);

												var linecustomizetype = searchresults.getLineItemValue('item','custcol_customizationcode',i);

												var lineShipMethod = searchresults.getLineItemValue('item','shipmethod',i);


												if(linecustomizetype==null || linecustomizetype=='')
													linecustomizetype=-1;

												var linelocation = searchresults.getLineItemValue('item', 'location', i);
												if(linelocation != null && linelocation !='')
												{
													location = linelocation;
												}	

												var vTestShipmethod=HeadShipmethod;
												if(ItemLineShipping=='T' && lineShipMethod !=  null && lineShipMethod != '')
													vTestShipmethod=lineShipMethod;

												if(vTestShipmethod==null || vTestShipmethod=='')
													vTestShipmethod=-1;

												var str1 = 'solocation. = ' + solocation + '<br>';
												str1 = str1 + 'location. = ' + location + '<br>';
												str1 = str1 + 'Shipmethod. = ' + vTestShipmethod + '<br>';

												nlapiLogExecution('DEBUG', 'Location Values', str1);

												if(solocation==location)
												{										
													var str2 = 'customizetype. = ' + customizetype + '<br>';
													str2 = str2 + 'linecustomizetype. = ' + linecustomizetype + '<br>';

													nlapiLogExecution('DEBUG', 'Cutomize Type Values', str2);

													if(customizetype==linecustomizetype)
													{
														var str3 = 'FOShipmethod. = ' + FOShipmethod + '<br>';
														str3 = str3 + 'vTestShipmethod. = ' + vTestShipmethod + '<br>';

														nlapiLogExecution('DEBUG', 'Shipmethod Type Values', str3);
														if(FOShipmethod==vTestShipmethod)
														{	
															var variance=0;
															var soLine_Hold_flag = searchresults.getLineItemValue('item','custcol_create_fulfillment_order',i);				
															//var linecommtdqty = searchresults.getLineItemValue('item','quantitycommitted',i);
															var lineorderqty = searchresults.getLineItemValue('item', 'quantity', i); 
															//var linefulfildQty = searchresults.getLineItemValue('item', 'quantityfulfilled', i);

															if(lineorderqty==null || lineorderqty=='' || isNaN(lineorderqty))
															{
																lineorderqty=0;
															}
														/*	if(linefulfildQty==null || linefulfildQty=='' || isNaN(linefulfildQty))
															{
																linefulfildQty=0;
															}*/
															var linelocation = searchresults.getLineItemValue('item', 'location', i);
															if(linelocation != null && linelocation !='')
															{
																location = linelocation;
															}	

															var mwhsiteflag='F';
															var mwhsiteflag='F';
															var	islocationAliasExists=CheckLocationAlias(location);
															if(islocationAliasExists!=null && islocationAliasExists!='')
															{
																mwhsiteflag='T';
															}
															else
															{
																nlapiLogExecution('DEBUG','location',location);
																if(location!=null && location!='')
																{
																	var fields = ['custrecord_ebizwhsite'];

																	var locationcolumns = nlapiLookupField('Location', location, fields);
																	if(locationcolumns!=null)
																		mwhsiteflag = locationcolumns.custrecord_ebizwhsite;
																}
															}

															nlapiLogExecution('DEBUG','mwhsiteflag',mwhsiteflag);
															nlapiLogExecution('DEBUG','lineorderqty',lineorderqty);
															//nlapiLogExecution('DEBUG','linefulfildQty',linefulfildQty);
															//Get the close button
															
															if(mwhsiteflag=='T' && (parseFloat(lineorderqty)> parseFloat(0)))
															{
																nlapiLogExecution('DEBUG','soLine_Hold_flag',soLine_Hold_flag);

																if(soLine_Hold_flag=='T')
																{
																	var salesOrderLineNo = searchresults.getLineItemValue('item','line',i);
																	var commetedQty = searchresults.getLineItemValue('item','quantity',i);
																	/*if(commetedQty==null || commetedQty=='' || isNaN(commetedQty))
																	{
																		commetedQty=0;
																	}*/

																	nlapiLogExecution('DEBUG','commetedQty',parseFloat(commetedQty));

																	//var totalQuantity = getFulfilmentqty(soid, salesOrderLineNo);
																	var totalQuantity = getFulfilmentqtybyLine(soid, salesOrderLineNo,alllinesfoqty);
																	nlapiLogExecution('DEBUG','totalQuantity',totalQuantity);
																	if(totalQuantity == 0)
																		variance=commetedQty;
																	else
																	{
																		/*var shippedqty = searchresults.getLineItemValue('item', 'quantityfulfilled', i);
																		if(shippedqty==null || shippedqty=='' || isNaN(shippedqty))
																		{
																			shippedqty=0;
																		}*/
																		//var totalcommtdqty = parseFloat(commetedQty)+parseFloat(shippedqty);

																		if(parseFloat(totalQuantity) < parseFloat(commetedQty))
																		{
																			variance=parseFloat(commetedQty)-parseFloat(totalQuantity);
																		}
																		/*** The below code is merged from Factory Mation production account on March-01-2013 by Ganesh K as part of Standard bundle. Purpose is to generate DO based on qty picked directly by NS and eBiz FO qty***/
																		nlapiLogExecution('DEBUG','variance1',variance);

																		/*if(parseFloat(variance) > 0)
																		{
																			//-------------------------------------------------Change by Ganesh on 16th Jan 2013
																			if((parseFloat(variance)+ parseFloat(shippedqty)  + parseFloat(totalQuantity))>parseFloat(totalcommtdqty))
																			{
																				var totalInTransitFOQuantity = getFulfilmentqtyInProgNew(soid, salesOrderLineNo);
																				nlapiLogExecution('DEBUG','totalInTransitFOQuantity',totalInTransitFOQuantity);
																				variance = parseFloat(totalcommtdqty) - parseFloat(shippedqty) - parseFloat(totalInTransitFOQuantity);
																				//variance = parseFloat(totalcomtdqty) - parseFloat(shippedqty);

																			}
																		}*/
																		//-----------------------------------------------------------------------
																		/*//Temporary Fix
										if((parseFloat(variance)+ parseFloat(shippedqty))>parseFloat(totalcomtdqty))
										{
											variance = parseFloat(variance) - (parseFloat(totalcomtdqty)-parseFloat(variance));
											//New scenario
											variance = parseFloat(variance) - parseFloat(shippedqty);
										}*/
																	}
																	nlapiLogExecution('DEBUG','variance2',variance);
																	/***Upto here***/
																	if(variance > 0)
																	{	
																		var fullfilorderids='';
																		nlapiLogExecution('DEBUG','fullFillmentorderno',fullFillmentorderno);
																		nlapiLogExecution('DEBUG','oldfulFillmentOrdNo',oldfulFillmentOrdNo);
																		if(fullFillmentorderno!=oldfulFillmentOrdNo || fullFillmentorderno=='-1')
																		{
																			if(fullfilorderids == null || fullfilorderids == '')
																			{

																				//generate new fullfillment order no.
																				if(focreated==0)
																				{
																					fullFillmentorderno=fulfillmentOrderDetails(soid,soname);
																					var lineordsplit = fullFillmentorderno.split('.');
																					focreated = parseFloat(lineordsplit[1])+parseFloat(1);

																				}
																				else
																				{

																					//var nextvalue = parseFloat(focreated)+parseFloat(1);
																					fullFillmentorderno=soname+'.'+focreated;
																					focreated=parseInt(focreated)+parseInt(1);
																				}
																				oldfulFillmentOrdNo=fullFillmentorderno;
																				fullfilorderids = fullFillmentorderno;
																				nlapiLogExecution('DEBUG','fullfilorderids',fullfilorderids);
																				nlapiLogExecution('DEBUG','fullFillmentorderno',fullFillmentorderno);
																			}
																			else
																			{
																				nlapiLogExecution('DEBUG','fullfilorderids',fullfilorderids);
																				var lineordsplit = fullfilorderids.split('.');
																				var nextvalue = parseFloat(lineordsplit[1])+parseFloat(1);
																				fullFillmentorderno=lineordsplit[0]+'.'+nextvalue;
																				oldfulFillmentOrdNo=fullFillmentorderno;
																				fullfilorderids = fullFillmentorderno;
																				nlapiLogExecution('DEBUG','fullFillmentorderno',fullFillmentorderno);
																			}
																		}
//																		case  20123646  end

																		//createFulfillmentOrder(i, variance, fullFillmentorderno,searchresults);
																		createFulfillmentOrderBulk(i, variance, fullFillmentorderno,searchresults,foparent);
																		searchresults.setLineItemValue('item','custcol_transactionstatusflag',i,25);
																	}
																}
															}

														}
													}
												}
											}
										}
									}
								}
							}
							nlapiSubmitRecord(foparent);
							nlapiSubmitRecord(searchresults, true);	
						}
						catch(e)
						{
							nlapiLogExecution('DEBUG','exception in auto fulfillment creation',e.message);
						}
						finally
						{
							nlapiLogExecution('DEBUG','soname before unlocking',soname);
							nlapiLogExecution('DEBUG','trantype before unlocking',trantype);

							if(soname != null && soname != '')
							{	
								var ExternalIdFilters = new Array();
								ExternalIdFilters.push(new nlobjSearchFilter('name', null, 'is', soname));
								ExternalIdFilters.push(new nlobjSearchFilter('custrecord_ebiznet_trantype', null, 'is', trantype));
								var searchresultsExt = nlapiSearchRecord('customrecord_ebiznet_lockrecs', null, ExternalIdFilters, null);
								if(searchresultsExt != null && searchresultsExt != '')
								{
									nlapiLogExecution('DEBUG','Recordid before unlocking',searchresultsExt[0].getId());
									nlapiDeleteRecord('customrecord_ebiznet_lockrecs',searchresultsExt[0].getId());
									nlapiLogExecution('DEBUG','Unlocked successfully');
								}	
							}
						}

					}
				}
				else
					{
					nlapiLogExecution('DEBUG','Order Status','Invalid');	
					}
			}
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in AutoAdjustFulfillmentOrders',exp);
	}
	nlapiLogExecution('DEBUG','Remaining usage at the end of AutoAdjustFulfillmentOrders ',context.getRemainingUsage());
}

function createFulfillmentOrderBulk(lineno,variance,fullFillmentorderno,searchresults,foparent)
{
	var salesOrderNo = nlapiGetFieldValue('tranid');	
	var salesOrderInternalId = nlapiGetRecordId();	
	var salesOrderDate = nlapiGetFieldValue('trandate');
	var customer = nlapiGetFieldValue('entity');
	var priority = nlapiGetFieldValue('custbody_nswmspriority');
	var orderType = nlapiGetFieldValue('custbody_nswmssoordertype');
	var shipCarrierMethod = nlapiGetFieldValue('shipmethod');
	var wmsCarrier = nlapiGetFieldValue('custbody_salesorder_carrier');
	var wmsCarrierLineLevel = searchresults.getLineItemValue('item','custcol_salesorder_carrier_linelevel',lineno);
	var shipmethodLineLevel = searchresults.getLineItemValue('item','shipmethod',lineno);
	var company = nlapiGetFieldValue('custbody_nswms_company');
	var route = nlapiGetFieldValue('custbody_nswmssoroute');
	var location=nlapiGetFieldValue('location');
	var shipcomplete = nlapiGetFieldValue('shipcomplete');
	var locationname=nlapiGetFieldText('location');
	var itemlevelshipping=nlapiGetFieldValue('ismultishipto');
	var vShipDate = nlapiGetFieldValue('shipdate');				
	if(vShipDate==null||vShipDate==""||vShipDate=='undefined')
		vShipDate = nlapiGetFieldValue('custbody_nswmspoexpshipdate');

	nlapiLogExecution('DEBUG','customer',customer);

	if(customer==null || customer=='')
	{
		customer = searchresults.getFieldValue('entity');
		priority = searchresults.getFieldValue('custbody_nswmspriority');
		orderType = searchresults.getFieldValue('custbody_nswmssoordertype');		
		salesOrderDate = searchresults.getFieldValue('trandate');	
		shipCarrierMethod = searchresults.getFieldValue('shipmethod');
	}	

	var lineordsplit = fullFillmentorderno.split('.');
	var backordline = lineordsplit[1];
	var customizationtype = searchresults.getLineItemValue('item','custcol_customizationcode',lineno);
	var linelocation=searchresults.getLineItemValue('item','location',lineno);
	var linelocationname=searchresults.getLineItemText('item','location',lineno);
	if(linelocation!=null && linelocation!='')
	{
		location=linelocation;
		locationname = linelocationname;
	}
	nlapiLogExecution('DEBUG','location',location);
	var actuallocation=null;
	if(location!=null && location!='' && location!='null')
	{
		actuallocation=CheckLocationAlias(location);
	}

	nlapiLogExecution('DEBUG','location',actuallocation);
	//The below code is commented by Satish.N on 14NOV2013
	/*
	nlapiLogExecution('DEBUG','lineordsplit[1]',lineordsplit[1]);
	var actuallocation=null;
	var LocationAliasFOnumber='';
	if(location!=null && location!='' && location!='null')
	{
		actuallocation=CheckLocationAlias(location);
		//case 20124822 start : for channel allocation 
		nlapiLogExecution('DEBUG','into location alias',lineordsplit[1]);
		var nextvalue = parseFloat(lineordsplit[1])+parseFloat(1);
		LocationAliasFOnumber=lineordsplit[0]+'.'+nextvalue;
		//end of case 20124822
	}

	nlapiLogExecution('DEBUG','location',actuallocation);
	nlapiLogExecution('DEBUG','LocationAliasFOnumber',LocationAliasFOnumber);
	 */
	var salesOrderLineNo =searchresults.getLineItemValue('item','line',lineno);	
	var itemId =searchresults.getLineItemValue('item','item',lineno);
	var Shipment = searchresults.getLineItemValue('item','custcol_nswms_shipment_no',lineno);
	if(Shipment==null || Shipment=='')
		Shipment=searchresults.getFieldValue('custbody_shipment_no');
	var itemStatus =searchresults.getLineItemValue('item','custcol_ebiznet_item_status',lineno);
	var itemPackCode =searchresults.getLineItemValue('item','custcol_nswmspackcode',lineno);
	var defpackcode = '';

	var str = 'salesOrderNo. = ' + salesOrderNo + '<br>';
	str = str + 'salesOrderInternalId. = ' + salesOrderInternalId + '<br>';	
	str = str + 'wmsCarrierLineLevel. = ' + wmsCarrierLineLevel + '<br>';
	str = str + 'wmsCarrier. = ' + wmsCarrier + '<br>';
	str = str + 'lineno. = ' + lineno + '<br>';
	str = str + 'itemPackCode. = ' + itemPackCode + '<br>';
	str = str + 'itemStatus. = ' + itemStatus + '<br>';
	str = str + 'Shipment. = ' + Shipment + '<br>';
	str = str + 'locationname. = ' + locationname + '<br>';
	str = str + 'vShipDate. = ' + vShipDate + '<br>';
	str = str + 'shipCarrierMethod. = ' + shipCarrierMethod + '<br>';
	str = str + 'shipmethodLineLevel. = ' + shipmethodLineLevel + '<br>';
	str = str + 'salesOrderDate. = ' + salesOrderDate + '<br>';
	str = str + 'customer. = ' + customer + '<br>';
	str = str + 'priority. = ' + priority + '<br>';
	str = str + 'orderType. = ' + orderType + '<br>';

	nlapiLogExecution('DEBUG', 'Input Parameters', str);



	var statusbyloc='';

	//The following line of code is only for TNT
	//statusbyloc=getItemStatus(locationname);	

	if(statusbyloc!=null && statusbyloc!='')
	{
		orderLineRecord.setFieldText('custrecord_linesku_status', statusbyloc);
		nlapiLogExecution('DEBUG','itemStatus',statusbyloc);
	}
	else
	{
		// Sales Order Item Status
		var itemStatus =searchresults.getLineItemValue('item','custcol_ebiznet_item_status',lineno);
		nlapiLogExecution('DEBUG','itemStatus',itemStatus);		

		if( (itemStatus==null || itemStatus=='') || (itemPackCode==null || itemPackCode==''))
		{
			var fields = ['custitem_ebizoutbounddefskustatus','custitem_ebizdefpackcode'];
			var columns= nlapiLookupField('item',itemId,fields);
			if(columns!=null)
			{
				if(itemStatus==null || itemStatus=='')
					itemStatus = columns.custitem_ebizoutbounddefskustatus;
				defpackcode = columns.custitem_ebizdefpackcode;
			}
		}

		nlapiLogExecution('DEBUG','itemStatus',itemStatus);
		nlapiLogExecution('DEBUG','DefPackCde1',defpackcode);
	}

	// Sales Order Item Pack Code

	if(itemPackCode==null || itemPackCode=='')
	{
		itemPackCode=defpackcode;
	}

	nlapiLogExecution('DEBUG','itemPackCode',itemPackCode);

	// Sales Order Item Uom
	var baseUom = searchresults.getLineItemValue('item','custcol_nswmssobaseuom',lineno);
	//var vNSUOM = searchresults.getLineItemText('item','units', lineno);
	var vNSUOM = searchresults.getLineItemValue('item','units', lineno);
	nlapiLogExecution('DEBUG', 'Order UOM', vNSUOM);

	var veBizUOMQty=0;
	var vBaseUOMQty=0;
	var vUOMId='';

	if(vNSUOM!=null && vNSUOM!="")
	{
		nlapiLogExecution('DEBUG', 'Order UOM', vNSUOM);
		var eBizItemDims=geteBizItemDimensions(itemId);
		if(eBizItemDims!=null&&eBizItemDims.length>0)
		{
			for(z=0; z < eBizItemDims.length; z++)
			{
				nlapiLogExecution('DEBUG', 'Base UOM Flag', eBizItemDims[z].getValue('custrecord_ebizbaseuom'));
				if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
				{
					vBaseUOMQty = eBizItemDims[z].getValue('custrecord_ebizqty');
					nlapiLogExecution('DEBUG', 'vBaseUOMQty', vBaseUOMQty);
				}

				nlapiLogExecution('DEBUG', 'SKU Dim UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
				//if(vNSUOM == eBizItemDims[z].getText('custrecord_ebiznsuom'))
				if(vNSUOM == eBizItemDims[z].getValue('custrecord_ebiznsuom'))
				{					
					veBizUOMQty = eBizItemDims[z].getValue('custrecord_ebizqty');
					nlapiLogExecution('DEBUG', 'veBizUOMQty', veBizUOMQty);
					vUOMId = eBizItemDims[z].getValue('custrecord_ebizuomskudim');
					nlapiLogExecution('DEBUG', 'vUOMId', vUOMId);
				}
			}
			nlapiLogExecution('DEBUG', 'veBizUOMQty', veBizUOMQty);
			nlapiLogExecution('DEBUG', 'variance', variance);
			nlapiLogExecution('DEBUG', 'vBaseUOMQty', vBaseUOMQty);

			if(veBizUOMQty!=0)
				variance = parseFloat(variance)*parseFloat(veBizUOMQty)/parseFloat(vBaseUOMQty);
			nlapiLogExecution('DEBUG', 'vordqty', variance);
		}
	}
	
	var itemSubtype = nlapiLookupField('item', itemId, 'recordType');
	var fields = ['custitem_item_info_1', 'custitem_item_info_2', 'custitem_item_info_3'];
	var columnsval = nlapiLookupField(itemSubtype, itemId, fields);
	var skuinfo1val = columnsval.custitem_item_info_1;
	var skuinfo2val = columnsval.custitem_item_info_2;
	var skuinfo3val = columnsval.custitem_item_info_3;

	nlapiLogExecution('DEBUG', 'itemId', itemId);
	foparent.selectNewLineItem('recmachcustrecord_ebiz_fo_parent');

	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','name', salesOrderInternalId);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ns_ord', parseInt(salesOrderInternalId));
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ordline', parseInt(salesOrderLineNo));
	if(priority!=null && priority!='')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_order_priority', priority);
	if(orderType!=null && orderType!='')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_order_type', orderType);
	if(customer!=null && customer!='')
		//foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_customer', customer);
	if(shipmethodLineLevel!=null && shipmethodLineLevel!='')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_carrier', shipmethodLineLevel);
	else if(shipCarrierMethod!=null && shipCarrierMethod!='')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_carrier', shipCarrierMethod);
	if(wmsCarrierLineLevel != null && wmsCarrierLineLevel != "")
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_wmscarrier', wmsCarrierLineLevel);
//	else if (wmsCarrier!=null && wmsCarrier!='')
//		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_do_wmscarrier', wmsCarrier);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linestatus_flag', 25);
	if(route != null && route != "")
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_route_no', route);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_lineord', fullFillmentorderno);
	//The below code is commented by Satish.N on 14NOV2013
	//foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_lineord', fullFillmentorderno);
	if(company != null && company != "")
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ordline_company', company);
	if(vShipDate!='undefined')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_shipdate', vShipDate);
	if(salesOrderDate!='undefined')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_orderdate', salesOrderDate);

	//case # 20124380 Start
	if(salesOrderDate!='undefined')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_record_linedate', DateStamp());
	//case # 20124380 End

	if(customizationtype!=null && customizationtype!="" && customizationtype!='undefined')
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_ordline_customizetype', customizationtype);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_actuallocation', location);
	if(actuallocation!=null)
	{
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ordline_wms_location', actuallocation);
		//The below code is commented by Satish.N on 14NOV2013
		//foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_lineord', LocationAliasFOnumber);
	}
	else
	{
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ordline_wms_location', location);
		//The below code is commented by Satish.N on 14NOV2013
		//foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_lineord', fullFillmentorderno);
	}
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_shipcomplete', shipcomplete);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_backorderno', backordline);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ordline', salesOrderLineNo);
	if(itemId != null && itemId != "")
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ebiz_linesku', itemId);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_shipment_no', Shipment);
	if(itemStatus != null && itemStatus != "")
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linesku_status', itemStatus);
	if(itemPackCode != null && itemPackCode != "")
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linepackcode', itemPackCode);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_ord_qty', parseFloat(variance));
	if(vUOMId != null && vUOMId != "")
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_lineuom_id', vUOMId);
	if(skuinfo1val != null && skuinfo1val != "")
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_fulfilmentiteminfo1', skuinfo1val);
	if(skuinfo2val != null && skuinfo2val != "")
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_fulfilmentiteminfo2', skuinfo2val);
	if(skuinfo3val != null && skuinfo3val != "")
		foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_fulfilmentiteminfo3', skuinfo3val);
	foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_fo_parent','custrecord_linenotes2', 'Created by User Event');
	nlapiLogExecution('DEBUG', 'itemId', itemId);
	foparent.commitLineItem('recmachcustrecord_ebiz_fo_parent');

}
function DeleteOpenFOlines(soId, linesarray, shipcomplete)
{ 
	nlapiLogExecution('DEBUG','Into DeleteOpenFOlines');

	var str = 'soId. = ' + soId + '<br>';
	str = str + 'linesarray. = ' + linesarray + '<br>';
	str = str + 'shipcomplete. = ' + shipcomplete + '<br>';

	nlapiLogExecution('DEBUG', 'Function Parameters', str);

	var filter = new Array();
	filter[0]= new nlobjSearchFilter('custrecord_ns_ord', null, 'is', soId);

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_linestatus_flag') ;
	column[1] = new nlobjSearchColumn('custrecord_ordline').setSort();
	column[2] = new nlobjSearchColumn('custrecord_pickgen_qty') ;
	column[3] = new nlobjSearchColumn('custrecord_pickqty') ;

	var searchRec= nlapiSearchRecord('customrecord_ebiznet_ordline', null, filter, column);
	if(searchRec!=null)
		nlapiLogExecution('DEBUG','searchRec ',searchRec.length);

	var Id="";
	var Type="";

	if (searchRec)
	{
		nlapiLogExecution('DEBUG','linesarray.length',linesarray.length);

		for (var x = 0; x < linesarray.length; x++)
		{
			var solineno = linesarray[x][0];

			nlapiLogExecution('DEBUG','solineno',solineno);

			for (var s = 0; s < searchRec.length; s++)
			{
				var folineno = searchRec[s].getValue('custrecord_ordline');

				nlapiLogExecution('DEBUG','folineno',folineno);

				if(solineno == folineno)
				{
					Id = searchRec[s].getId();
					Type= searchRec[s].getRecordType();

					var fostatus = searchRec[s].getValue('custrecord_linestatus_flag');
					var pickgenqty = searchRec[s].getValue('custrecord_pickgen_qty');
					var pickqty = searchRec[s].getValue('custrecord_pickqty');

					if(pickgenqty==null || pickgenqty=='' || isNaN(pickgenqty))
						pickgenqty=0;

					if(pickqty==null || pickqty=='' || isNaN(pickqty))
						pickqty=0;

					var str1 = 'fostatus. = ' + fostatus + '<br>';
					str1 = str1 + 'pickgenqty. = ' + pickgenqty + '<br>';
					str1 = str1 + 'pickqty. = ' + pickqty + '<br>';

					nlapiLogExecution('DEBUG', 'FO Values', str1);

					if(fostatus==25 && parseFloat(pickgenqty)==0)
					{
						nlapiDeleteRecord(searchRec[s].getRecordType(),searchRec[s].getId());
						//nlapiLogExecution('DEBUG','Deleted record Id',Id);
					}
					else
					{
						nlapiSubmitField('customrecord_ebiznet_ordline',Id,'custrecord_shipcomplete',shipcomplete);
					}
				}
			}
		}
	}

	nlapiLogExecution('DEBUG','Out of DeleteOpenFOlines');
}
function getFulfilmentqtyforallLines(soid)
{
	nlapiLogExecution('DEBUG','Into getFulfilmentqtyforallLines');

	var searchresultsRcpts = new Array();

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'is', soid.toString());

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ord_qty',null,'sum');
	columns[1] = new nlobjSearchColumn('custrecord_ordline',null,'group');

	var searchresultsRcpts = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);

	nlapiLogExecution('DEBUG','Out of getFulfilmentqtyforallLines');

	return searchresultsRcpts;
}
function CheckLocationAlias(location)
{
	var OrginalLocation=null;
	var filters= new Array();
	if(location!=null && location!='' && location!='null')
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiz_loc_location',null,'anyof',location));
		var columns=new Array();
		columns.push(new nlobjSearchColumn('custrecord_ebiz_loc_aliaslocation'));
		resultSet=nlapiSearchRecord('customrecord_ebiz_locationalias',null,filters,columns);
		if(resultSet!=null && resultSet!='' && resultSet.length>0)
		{
			OrginalLocation=resultSet[0].getValue('custrecord_ebiz_loc_aliaslocation');
		}
	}
	return OrginalLocation;

}
function getFulfilmentqtybyLine(soid, salesOrderLineNo,alllinesfoqty)
{
	nlapiLogExecution('DEBUG','Into getFulfilmentqtybyLine');

	var foqty=0;

	if(alllinesfoqty!=null && alllinesfoqty!='' && alllinesfoqty.length>0)
	{
		for (var j = 0; j < alllinesfoqty.length; j++) 
		{
			var foline = alllinesfoqty[j].getValue('custrecord_ordline',null,'group');

			if(salesOrderLineNo==foline)
			{
				foqty=alllinesfoqty[j].getValue('custrecord_ord_qty',null, 'sum');
			}
		}
	}

	nlapiLogExecution('DEBUG','Out of getFulfilmentqtybyLine',foqty);
	return foqty;
}
function getFulfilmentqty(soid,lineno)
{
	var vqty=0;
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'is', soid.toString());
	filters[1] = new nlobjSearchFilter('custrecord_ordline', null, 'equalto', lineno);
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ord_qty',null,'sum');
	var searchresultsRcpts = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
	if (searchresultsRcpts != null) 
	{
		var val = searchresultsRcpts[0];
		vqty=val.getValue('custrecord_ord_qty',null, 'sum');
		//nlapiLogExecution('DEBUG','vqtysum',vqty);
		if(vqty==null)
			return 0;
		else
			return vqty;
	}
}
function fulfillmentOrderDetails(salesOrderId,salesordername)
{
	var salesOrderArray = new Array();
	var searchFulfillment =  getFullFillment(salesOrderId);
	//nlapiLogExecution('DEBUG','searchFulfillment :', searchFulfillment);

	//If searchfulfillment is null it indicates Its a New SO. 
	if(searchFulfillment == null){
		var fulfillmentNumber=salesordername +".1";
	}
	else{
		for(var i=0; i< searchFulfillment.length;i++)
		{
			var fullfillmentlineord = searchFulfillment[i].getValue('custrecord_lineord');
			var lineordsplit = fullfillmentlineord.split('.');
			salesOrderArray[i] = lineordsplit[1];
		}
		//MaxValue returns the max fullfillment order number
		var maximumValue = MaxValue(salesOrderArray);

		//adding 1 to the max fullfillment order number to get the new fullfillment order no.
		var fulfillmentValue = parseFloat(maximumValue) + parseFloat(1);
		nlapiLogExecution('DEBUG','fulfillment Value :', fulfillmentValue);

		//concatinating the fulfillmentvalue to salesorderid to get new fullfillment no.
		var fulfillmentNumber =  salesordername + "." + fulfillmentValue;
		//nlapiLogExecution('DEBUG','fulfillmentNumber :', fulfillmentNumber);
	}
	return fulfillmentNumber;
}
function MaxValue(array)
{
	var maximumNumber = array[0];
	for (var i = 0; i < array.length; i++) 
	{
		if (array[i] > maximumNumber) 
		{
			maximumNumber = array[i];
		}
	}
	return maximumNumber;
}

function getFullFillment(soId)
{	
	var ordlinefilters = new Array();
	ordlinefilters[0] = new nlobjSearchFilter('name', null, 'is', soId);
	var ordlinecolumns = new Array();  
	ordlinecolumns[0] = new nlobjSearchColumn('custrecord_lineord').setSort();        
	ordlinecolumns[1] = new nlobjSearchColumn('custrecord_ordline');
	ordlinecolumns[2] = new nlobjSearchColumn('custrecord_ord_qty'); // fulfillment ordered quantity
	ordlinecolumns[3] = new nlobjSearchColumn('custrecord_linestatus_flag'); //
	/***Below code is merged from Lexjet production account on 4th March 2013 by Ganesh ***/
	ordlinecolumns[4] = new nlobjSearchColumn('custrecord_ordline_wms_location'); //
	ordlinecolumns[5] = new nlobjSearchColumn('custrecord_do_carrier'); //
	/***Upto here ***/
	var ordlinesearchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, ordlinefilters, ordlinecolumns);
	return ordlinesearchresults;
}
/*It triggers before page is submitted (restrict deleteting the so after wave generation)*/
function VRADelete(type, form, request)
{	
	nlapiLogExecution('DEBUG','Into soDelete function',type);

	if(type ==  'delete')
	{
		var status=nlapiGetFieldValue('status');		
		var VRAId=nlapiGetFieldValue('id');
		nlapiLogExecution('DEBUG','VRAId ',VRAId);

		var filter = new Array();
		filter[0]= new nlobjSearchFilter('name', null, 'is', VRAId);
		var column = new Array();
		column[0] = new nlobjSearchColumn('custrecord_linestatus_flag'); 
		var searchRec = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filter, column);

		if(searchRec != null && searchRec != "")
		{
			var SODelStatusFlag=true;
			for(var p=0;p<searchRec.length;p++)
			{
				var vStatus=searchRec[p].getValue("custrecord_linestatus_flag");
				nlapiLogExecution('DEBUG','vStatus ',vStatus);
				if(vStatus != null &&  vStatus != '' && parseInt(vStatus) != 25 )
				{
					SODelStatusFlag=false;
				}	
			}
			nlapiLogExecution('DEBUG','SODelStatusFlag ',SODelStatusFlag);
			if(SODelStatusFlag == true)
			{	
				nlapiLogExecution('DEBUG','searchRec is null so rec can be deleted ',searchRec);
				for ( var s = 0; s < searchRec.length; s++) {
					var Id=nlapiDeleteRecord(searchRec[s].getRecordType(),searchRec[s].getId());
					nlapiLogExecution('DEBUG','Deleted record Id',Id);
				}
			}
			else				 
			{
				nlapiLogExecution('DEBUG','searchRec is not null so rec cannot  be deleted','');
				var ErrorMsg = nlapiCreateError('CannotDelete','Order is being processed in Main Warehouse. Do not delete', true);
				throw ErrorMsg;
			}
		}
	}

}
function beforeLoadUserevent(type, form, request)
{
	//Get the button
	var button = form.getButton('refund');
	 
	//Make sure that the button is not null
	if(button != null)
	   //Hide the button in the UI
	   button.setVisible(false);
}
function updateSeqNo(transType,seqno){
	nlapiLogExecution('DEBUG', 'Into updateSeqNo', seqno);

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_seqno');

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_trantype', null, 'is', transType);

	// Search in ebiznet_wave custom record for all rows for the given transaction type
	var results = nlapiSearchRecord('customrecord_ebiznet_wave', null, filters, columns);
	if(results != null){
		for (var t = 0; t < results.length; t++) {
			nlapiSubmitField('customrecord_ebiznet_wave', results[t].getId(), 'custrecord_seqno', parseFloat(seqno));
		}
	}

	nlapiLogExecution('DEBUG', 'Out of updateSeqNo', seqno);
}
function backOrderFulfillmentnew(type)
{
	nlapiLogExecution('DEBUG','Into  backOrderFulfillmentnew','');
	var context = nlapiGetContext();
	nlapiLogExecution('DEBUG','Remaining Usage 1',context.getRemainingUsage());
	
	var vintrid=0;
	var vinternalid=0;

	//get the seq no from wave record.
	vintrid = 1;//GetMaxTransactionNo('FO');
	nlapiLogExecution('DEBUG','internalid in search filter',vintrid);
	var filters = new Array();
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));						// Retrieve order lines
	filters.push(new nlobjSearchFilter('quantity', null, 'greaterthan', 0));		// Order Quantity > 0

	// Pending Return / Partially Returned / Pending Credit/Partially Returned
	filters.push(new nlobjSearchFilter('status', null, 'anyof', ['VendAuth:B','VendAuth:D','VendAuth:E']));
	filters.push(new nlobjSearchFilter('custbody_create_fulfillment_order', null, 'is', 'T'));
	//filters.push(new nlobjSearchFilter('custrecord_ebizwhsite', 'location', 'is', 'T'));//Location should be warehouse site
	filters.push(new nlobjSearchFilter('shipping', null, 'is', 'F'));//To avoid shipping lines.
	filters.push(new nlobjSearchFilter('taxline', null, 'is', 'F'));//To avoid tax lines.
	filters.push(new nlobjSearchFilter('custbody_ebiz_ord_size', null, 'isnot', 'L')); // To process less than 100 line orders only
	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');	
	columns[1] = new nlobjSearchColumn('line');
	columns[2] = new nlobjSearchColumn('custbody_create_fulfillment_order');
	columns[3] = new nlobjSearchColumn('quantitycommitted');
	columns[4] = new nlobjSearchColumn('quantity');
	columns[5] = new nlobjSearchColumn('trandate');
	columns[6] = new nlobjSearchColumn('entity');
	columns[7] = new nlobjSearchColumn('custbody_nswmspriority');
	columns[8] = new nlobjSearchColumn('shipmethod');
	columns[9] = new nlobjSearchColumn('custbody_salesorder_carrier');
	columns[10] = new nlobjSearchColumn('custbody_nswms_company');
	columns[11] = new nlobjSearchColumn('custbody_nswmssoroute');
	columns[12] = new nlobjSearchColumn('location');
	columns[13] = new nlobjSearchColumn('item');
	columns[14] = new nlobjSearchColumn('custcol_ebiznet_item_status');
	columns[15] = new nlobjSearchColumn('custcol_nswmspackcode');
	columns[16] = new nlobjSearchColumn('custcol_nswmssobaseuom');
	columns[17] = new nlobjSearchColumn('unit');
	columns[18] = new nlobjSearchColumn('tranid');
	columns[19] = new nlobjSearchColumn('shipcomplete');//Added 08-Nov-2011
	columns[20] = new nlobjSearchColumn('quantityshiprecv');
	columns[21] = new nlobjSearchColumn('custrecord_ebizwhsite','location');
	columns[22] = new nlobjSearchColumn('custcol_salesorder_carrier_linelevel');
	columns[23] = new nlobjSearchColumn('custcol_customizationcode');
	columns[24] = new nlobjSearchColumn('custbody_nswmspoexpshipdate');
	columns[25] = new nlobjSearchColumn('custbody_nswmssoordertype');
	columns[26] = new nlobjSearchColumn('custbody_shipment_no');
	columns[27] = new nlobjSearchColumn('shipdate');
	columns[28] = new nlobjSearchColumn('custcol_nswms_shipment_no');
	columns[29] = new nlobjSearchColumn('quantityuom');

	//true - Discending false - Ascending
	columns[0].setSort(true);
	columns[1].setSort();

	var VRAOrderLineDetails = new nlapiSearchRecord('vendorreturnauthorization', null, filters, columns);
	nlapiLogExecution('DEBUG','Remaining Usage 2',context.getRemainingUsage());
	if(VRAOrderLineDetails!=null && VRAOrderLineDetails!='')
	{	

		var sonumber = 0; //Added 08-Nov-2011
		var oldsoname='';
		var fullFillmentorderno='';
		var fonumberarr = new Array();
		var vtempflag=false;
		var createfo = 'T'; //Added 08-Nov-2011

		nlapiLogExecution('DEBUG','VRAOrderLineDetails.length ',VRAOrderLineDetails.length);
		for(var i=0;i<VRAOrderLineDetails.length;i++)
		{			
			var mwhsiteflag='F';
			vinternalid=VRAOrderLineDetails[i].getValue('internalid');
			var soname = VRAOrderLineDetails[i].getValue('tranid');	
			var solocation = VRAOrderLineDetails[i].getValue('location');
			var orderqty = VRAOrderLineDetails[i].getValue('quantity');
			var create_fulfillment_order= VRAOrderLineDetails[i].getValue('custbody_create_fulfillment_order');
			var salesOrderLineNo = VRAOrderLineDetails[i].getValue('line');
		//	var commetedQty = VRAOrderLineDetails[i].getValue('quantitycommitted');
			var shpcompleteflag = VRAOrderLineDetails[i].getValue('shipcomplete'); //Added 08-Nov-2011
		//	var shippedqty = VRAOrderLineDetails[i].getValue('quantityshiprecv');
			var customizationcode = VRAOrderLineDetails[i].getValue('custcol_customizationcode');
			var uomconvqty = VRAOrderLineDetails[i].getValue('quantityuom');
			var unit = VRAOrderLineDetails[i].getValue('unit');
			/***Below code is merged from Lexjet Production by Ganesh K on 4th Mar 2013***/
			var solineshipmethod = VRAOrderLineDetails[i].getValue('shipmethod');
			/***upto here***/
			var conversionrate = 1;
			if(unit!=null && unit!='')
			{
				conversionrate = parseFloat(orderqty)/parseFloat(uomconvqty);
			}


			var str = '# Loop. = ' + i + '<br>';
			str = str + 'vinternalid. = ' + vinternalid + '<br>';
			str = str + 'SO Name. = ' + soname + '<br>';
			str = str + 'SO Line. = ' + salesOrderLineNo + '<br>';	
			str = str + 'SO Location. = ' + solocation + '<br>';
			str = str + 'Customization Code. = ' + customizationcode + '<br>';
			str = str + 'Order Qty. = ' + orderqty + '<br>';
			str = str + 'Unit. = ' + unit + '<br>';
			str = str + 'UOM Qty. = ' + uomconvqty + '<br>';
			str = str + 'UOM Conversion Rate. = ' + conversionrate + '<br>';
			//str = str + 'Commetted Qty. = ' + commetedQty + '<br>';			
			//str = str + 'Shipped Qty. = ' + shippedqty + '<br>';
			str = str + 'create_fulfillment_order. = ' + create_fulfillment_order + '<br>';
			str = str + 'shpcompleteflag. = ' + shpcompleteflag + '<br>';

			nlapiLogExecution('DEBUG', 'SO Line Details1', str);		

			if(customizationcode==null || customizationcode=='')
				customizationcode=-1;

			if(uomconvqty==null || uomconvqty=='' || isNaN(uomconvqty))
				uomconvqty=1;

			if(orderqty==null || orderqty=='' || isNaN(orderqty))
				orderqty=0;
			
			orderqty=parseFloat(orderqty) * (-1);
			uomconvqty=parseFloat(uomconvqty) * (-1);

			/*if(shippedqty==null || shippedqty=='' || isNaN(shippedqty))
				shippedqty=0;
*/
			if(unit!=null && unit!='')
			{
				//commetedQty = parseFloat(commetedQty)/parseFloat(conversionrate);
				//shippedqty = parseFloat(shippedqty)/parseFloat(conversionrate);
			}

			// Assuming search results will return the data in the descending order of SO#

			if(soname != null)
			{
				var	islocationAliasExists=CheckLocationAlias(solocation);
				if(islocationAliasExists!=null && islocationAliasExists!='')
				{
					mwhsiteflag='T';
				}
				else
				{
					if(solocation!=null && solocation!='')
					{
						var fields = ['custrecord_ebizwhsite'];

						var locationcolumns = nlapiLookupField('Location', solocation, fields);
						if(locationcolumns!=null)
							mwhsiteflag = locationcolumns.custrecord_ebizwhsite;
					}
				}


				if(sonumber != soname)
				{
					sonumber = soname;

					try
					{
						createfo = 'T';//fnCreateFo(vinternalid);
					}
					catch(exp)
					{
						nlapiLogExecution('DEBUG','Exception in fnCreateFo',exp);
						createfo='T';
					}

					nlapiLogExecution('DEBUG','createfo',createfo);

					if(shpcompleteflag != null && shpcompleteflag == 'T'  && createfo=='T')
					{						
						for(var j=0;j<VRAOrderLineDetails.length;j++)
						{
							if(sonumber == VRAOrderLineDetails[j].getValue('tranid'))
							{
								var itemtype='';
								var solineordqty = VRAOrderLineDetails[j].getValue('quantity');
							//	var solinecmmtdqty = VRAOrderLineDetails[j].getValue('quantitycommitted');
								var lineitem=VRAOrderLineDetails[j].getValue('item');
								var shipmethod=VRAOrderLineDetails[j].getValue('shipmethod');

								var linelocation=VRAOrderLineDetails[j].getValue('location');
								if(linelocation!=null && linelocation!='')
									location=linelocation;

//								var mwhsiteflag='F';


								/*if(solinecmmtdqty==null || solinecmmtdqty=='' || isNaN(solinecmmtdqty))
									solinecmmtdqty=0;*/

								/*var solinefulfildqty = VRAOrderLineDetails[j].getValue('quantityshiprecv');

								if(solinefulfildqty == null || solinefulfildqty == '' || isNaN(solinefulfildqty))
									solinefulfildqty = 0;
*/
								//var totalcomtdqty=parseFloat(solinecmmtdqty)+parseFloat(solinefulfildqty);

								if(lineitem!=null && lineitem!='')
									itemtype = nlapiLookupField('item', lineitem, 'recordType');	

								nlapiLogExecution('DEBUG','itemtype',itemtype);

								if(itemtype!='descriptionitem' && itemtype!='discountitem' && itemtype!='Payment' && itemtype!='Markup' 
									&& itemtype!='Subtotal'  && itemtype!='otherchargeitem' && itemtype!='serviceitem' && itemtype!='noninventoryitem'
										&& itemtype!='NonInvtPart')
								{
									/*if(parseFloat(solineordqty) > parseFloat(0))
									{*/
										createfo = 'T';
										continue;
									/*}*/
								}
							}

						}

						nlapiLogExecution('DEBUG','Create FO',createfo);
					}						
				}				
			}

			var str = 'create_fulfillment_order. = ' + create_fulfillment_order + '<br>';
			str = str + 'createfo. = ' + createfo + '<br>';
			str = str + 'mwhsiteflag. = ' + mwhsiteflag + '<br>';

			nlapiLogExecution('DEBUG', 'Flag Details1', str);		

			if(create_fulfillment_order != null && create_fulfillment_order == 'T' && createfo == 'T' && mwhsiteflag=='T')
			{
				try
				{
					if(soname != null && soname != '')
					{	
						//To lock the sales order
						nlapiLogExecution('DEBUG','soname before Locking',soname);
						nlapiLogExecution('DEBUG','trantype before Locking','salesorder');
						var fulfillRec= nlapiCreateRecord('customrecord_ebiznet_lockrecs');
						fulfillRec.setFieldValue('name', soname);					
						fulfillRec.setFieldValue('custrecord_ebiznet_trantype', 'salesorder');
						fulfillRec.setFieldValue('externalid', soname);
						fulfillRec.setFieldValue('custrecord_ebiznet_notes', 'Locked by Scheduler');
						nlapiSubmitRecord(fulfillRec,false,true );
						nlapiLogExecution('DEBUG','Locked successfully');
					}
				}
				catch(e)
				{
					nlapiLogExecution('DEBUG', 'DEBUG', e.getCode());
					nlapiLogExecution('DEBUG', 'DEBUG', 'Another thread is currently processing this fulfillment order, which prevents your request from executed.');
					//var wmsE = nlapiCreateError('FULFILLMENT_ORDER_RACE_COND_DETECTED', 'Another thread is currently processing this fulfillment order, which prevents your request from executed.', true);
					//throw wmsE;
					continue;
				}
				try
				{
					var variance=0;
					var totalQuantity = getFulfilmentqty(VRAOrderLineDetails[i].getId(), salesOrderLineNo);
					nlapiLogExecution('DEBUG','Remaining Usage 3',context.getRemainingUsage());
					var str = 'SO Name. = ' + soname + '<br>';
					str = str + 'SO Line. = ' + salesOrderLineNo + '<br>';	
					str = str + 'SO Location. = ' + solocation + '<br>';
					str = str + 'Customization Code. = ' + customizationcode + '<br>';
					str = str + 'Order Qty. = ' + orderqty + '<br>';
					str = str + 'UOM Qty. = ' + uomconvqty + '<br>';
					//str = str + 'Commetted Qty. = ' + commetedQty + '<br>';
					str = str + 'Created FO Qty. = ' + totalQuantity + '<br>';
					//str = str + 'Shipped Qty. = ' + shippedqty + '<br>';

					nlapiLogExecution('DEBUG', 'SO Line Details', str);		
					//var totalcomtdqty=parseFloat(commetedQty)+parseFloat(shippedqty);
					nlapiLogExecution('DEBUG','totalQuantity',totalQuantity);
					nlapiLogExecution('DEBUG','orderqty',orderqty);
					if(totalQuantity == 0)
						variance=orderqty;
					else
					{
						if(parseFloat(totalQuantity) < parseFloat(orderqty))
						{
							variance=parseFloat(orderqty)-parseFloat(totalQuantity);
						}
					}

					nlapiLogExecution('DEBUG','Variance1',variance);

					/*if(variance > 0)
					{
						//-------------------------------------------------Change by Ganesh on 16th Jan 2013
						if((parseFloat(variance)+ parseFloat(shippedqty)  + parseFloat(totalQuantity))>parseFloat(totalcomtdqty))
						{
							var totalInTransitFOQuantity = getFulfilmentqtyInProgNew(VRAOrderLineDetails[i].getId(), salesOrderLineNo);
							nlapiLogExecution('DEBUG', 'Out of getFulfilmentqtyInProgNew - totalInTransitFOQuantity',totalInTransitFOQuantity);
							variance = parseFloat(totalcomtdqty) - parseFloat(shippedqty) - parseFloat(totalInTransitFOQuantity);

						}
						//-----------------------------------------------------------------------
					}	*/

					nlapiLogExecution('DEBUG','Variance2',variance);

					if(variance > 0)
					{
						if(oldsoname!=soname)
						{
							//generate new fullfillment order no.
							/***Below code is merged from Lexjet Production by Ganesh K on 4th Mar 2013***/
							//fullFillmentorderno=fulfillmentOrderDetailsforSchedulescript(salesOrderLineDetails[i].getId(),soname);
							fullFillmentorderno=fulfillmentOrderDetailsforSchedulescript(VRAOrderLineDetails[i].getId(),soname,solocation,solineshipmethod);
							oldsoname=soname;
							var curRow = [soname,fullFillmentorderno,customizationcode,solocation,solineshipmethod];
							fonumberarr.push(curRow);
						}
						else
						{
							if(fonumberarr!=null && fonumberarr!='' && fonumberarr.length>0)
							{
								var fonumberfound = 'F';
								for(var t=0;t<fonumberarr.length;t++)
								{
									if(fonumberarr[t][0]==soname && fonumberarr[t][2]==customizationcode && fonumberarr[t][3]==solocation && fonumberarr[t][4]==solineshipmethod)
									{
										fullFillmentorderno=fonumberarr[t][1];
										fonumberfound='T';
									}
								}
								if(fonumberfound=='F')
								{
									//fullFillmentorderno=fulfillmentOrderDetailsforSchedulescript(salesOrderLineDetails[i].getId(),soname);
									fullFillmentorderno=fulfillmentOrderDetailsforSchedulescript(VRAOrderLineDetails[i].getId(),soname,solocation,solineshipmethod);
									var curRow = [soname,fullFillmentorderno,customizationcode,solocation,solineshipmethod];
									/***upto here***/
									fonumberarr.push(curRow);
								}
							}
						}
						nlapiLogExecution('DEBUG','Remaining Usage 4',context.getRemainingUsage());
						nlapiLogExecution('DEBUG','fullFillmentorderno',fullFillmentorderno);

						if(fullFillmentorderno!=null && fullFillmentorderno!='')
						{
							var fulillmentarray = getFullFillmentbyline(VRAOrderLineDetails[i].getId(),salesOrderLineNo);
							if(fulillmentarray!=null && fulillmentarray!='' && fulillmentarray.length>0)
							{
								nlapiLogExecution('DEBUG','fulillmentarray length',fulillmentarray.length);	

								var existordqty = fulillmentarray[0].getValue('custrecord_ord_qty');
								var fointernalid = fulillmentarray[0].getId();
								var totalordqty = parseFloat(existordqty)+parseFloat(variance);

								var str = 'existordqty. = ' + existordqty + '<br>';
								str = str + 'totalordqty. = ' + totalordqty + '<br>';	
								str = str + 'variance. = ' + variance + '<br>';

								nlapiLogExecution('DEBUG', 'Qty Details', str);		

								nlapiLogExecution('DEBUG','Updating Fulfillment Order Line...');
								nlapiSubmitField('customrecord_ebiznet_ordline', fointernalid, 'custrecord_ord_qty', totalordqty);
							}
							else
							{
								createbackorderFulfillmentOrdernew(i, variance, fullFillmentorderno,VRAOrderLineDetails[i],soname,VRAOrderLineDetails[i].getId());
							}					

						}
						nlapiLogExecution('DEBUG','Remaining Usage 5',context.getRemainingUsage());							
					}
				}
				catch(e)
				{
					nlapiLogExecution('DEBUG','exception in FO scheduler',e.message);
				}
				finally
				{
					//To unlock the sales order
					nlapiLogExecution('DEBUG','soname before unlocking',soname);
					nlapiLogExecution('DEBUG','trantype before unlocking','salesorder');

					if(soname != null && soname != '')
					{	
						var ExternalIdFilters = new Array();
						ExternalIdFilters.push(new nlobjSearchFilter('name', null, 'is', soname));
						ExternalIdFilters.push(new nlobjSearchFilter('custrecord_ebiznet_trantype', null, 'is', 'salesorder'));
						var searchresultsExt = nlapiSearchRecord('customrecord_ebiznet_lockrecs', null, ExternalIdFilters, null);
						if(searchresultsExt != null && searchresultsExt != '')
						{
							nlapiLogExecution('DEBUG','Recordid before unlocking',searchresultsExt[0].getId());
							nlapiDeleteRecord('customrecord_ebiznet_lockrecs',searchresultsExt[0].getId());
							nlapiLogExecution('DEBUG','Unlocked successfully');
						}	
					}
				}
			}			
			//Update seq no in wave table if the remaining use is less than 500
			nlapiLogExecution('DEBUG','Remaining Usage',context.getRemainingUsage());
			if(context.getRemainingUsage()<500)
			{				
				nlapiLogExecution('DEBUG','vinternalid',vinternalid);
				//updateSeqNo('FO',vinternalid);
				nlapiLogExecution('DEBUG','break');
				vtempflag=true;
				break;
			}
		}
		nlapiLogExecution('DEBUG','VRAOrderLineDetails.length2 ',VRAOrderLineDetails.length);

		//Reset the seq no if all the sale orders are processed.
		if(VRAOrderLineDetails.length<1000&&vtempflag!=true)
		{
			//updateSeqNo('FO',0);
		}
		else
		{
			nlapiLogExecution('DEBUG','vinternalid',vinternalid);
			//updateSeqNo('FO',vinternalid);			
		}
	}

	nlapiLogExecution('DEBUG','Remaining Usage at the end ',context.getRemainingUsage());	
	nlapiLogExecution('DEBUG','Out of  backOrderFulfillmentnew','');
}
function getFullFillmentbyline(soId,lineno)
{
	nlapiLogExecution('DEBUG','Into  getFullFillmentbyline');
	nlapiLogExecution('DEBUG','soId',soId);
	nlapiLogExecution('DEBUG','lineno',lineno);

	var ordlinefilters = new Array();
	ordlinefilters[0] = new nlobjSearchFilter('name', null, 'is', soId);
	ordlinefilters[1] = new nlobjSearchFilter('custrecord_ordline', null, 'equalto', lineno);
	ordlinefilters[2] = new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', [25]);//EDIT

	var ordlinecolumns = new Array();  
	ordlinecolumns[0] = new nlobjSearchColumn('custrecord_lineord').setSort();        
	ordlinecolumns[1] = new nlobjSearchColumn('custrecord_ordline');
	ordlinecolumns[2] = new nlobjSearchColumn('custrecord_ord_qty'); // fulfillment ordered quantity
	ordlinecolumns[3] = new nlobjSearchColumn('custrecord_linestatus_flag'); //

	var ordlinesearchresults = nlapiSearchRecord('customrecord_ebiznet_ordline', null, ordlinefilters, ordlinecolumns);

	nlapiLogExecution('DEBUG','Out of  getFullFillmentbyline');

	return ordlinesearchresults;
}
function createbackorderFulfillmentOrdernew(lineno,variance,fullFillmentorderno,searchresults,tranid,salesOrderId)
{
	try
	{
		nlapiLogExecution('DEBUG','Into createbackorderFulfillmentOrdernew','');

		var str = 'salesOrderId. = ' + salesOrderId + '<br>';	
		str = str + 'tranid. = ' + tranid + '<br>';
		str = str + 'lineno. = ' + lineno + '<br>';
		str = str + 'variance. = ' + variance + '<br>';
		str = str + 'fullFillmentorderno. = ' + fullFillmentorderno + '<br>';


		nlapiLogExecution('DEBUG', 'Function Parameters', str);

		var salesOrderNo = tranid;	
		var salesOrderInternalId = salesOrderId;

		var salesOrderDate =searchresults.getValue('trandate');
		var customer = searchresults.getValue('entity');
		var priority = searchresults.getValue('custbody_nswmspriority');
		var orderType = searchresults.getValue('custbody_nswmssoordertype');
		nlapiLogExecution('DEBUG','orderType in',orderType);
		var shipCarrierMethod = searchresults.getValue('shipmethod');	
		var company = searchresults.getValue('custbody_nswms_company');
		var route = searchresults.getValue('custbody_nswmssoroute');
		var location=searchresults.getValue('location');
		var shipcomplete = searchresults.getValue('shipcomplete');
		var vShipDate = searchresults.getValue('shipdate');
		nlapiLogExecution('DEBUG','vShipDate',vShipDate);
		if(vShipDate==null||vShipDate==""||vShipDate=='undefined')
			vShipDate = searchresults.getValue('custbody_nswmspoexpshipdate');
		var customizationcode = searchresults.getValue('custcol_customizationcode');
		var vShippingNo = searchresults.getValue('custbody_shipment_no');
		var locationname=searchresults.getText('location');
		var vOrdType = searchresults.getValue('custbody_nswmssoordertype');

//		nlapiLogExecution('DEBUG','locationname',locationname);
//		var linelocation = searchresults.getLineItemValue('item','location',lineno);
//		if(linelocation!=null && linelocation!='' && linelocation!='null')
//		{
//		location=linelocation;
//		}
		var statusbyloc='';
		var actuallocation=null;
		if(location!=null && location!='' && location!='null')
		{
			actuallocation=CheckLocationAlias(location);
		}
		else
		{
			if(salesOrderInternalId!=null && salesOrderInternalId!='' && salesOrderInternalId!='')
			{
				var SoRecord = nlapiLoadRecord('vendorreturnauthorization', salesOrderInternalId);
				location = SoRecord.getFieldValue('location');				
				actuallocation=CheckLocationAlias(location);
			}
		}

		nlapiLogExecution('DEBUG','location',actuallocation);

		//The following line of code is only for TNT
		//statusbyloc=getItemStatus(locationname);	

		nlapiLogExecution('DEBUG','statusbyloc',statusbyloc);

		var lineordsplit = fullFillmentorderno.split('.');
		var backordline = lineordsplit[1];

		//*********header information************
		var orderLineRecord = nlapiCreateRecord('customrecord_ebiznet_ordline');
		orderLineRecord.setFieldValue('name', salesOrderInternalId);
		orderLineRecord.setFieldValue('custrecord_ns_ord', parseFloat(salesOrderInternalId));
		//orderLineRecord.setFieldValue('custrecord_ordline',parseFloat(salesOrderLineNo));
		orderLineRecord.setFieldValue('custrecord_do_order_priority', priority);
		orderLineRecord.setFieldValue('custrecord_do_order_type', orderType);
		orderLineRecord.setFieldValue('custrecord_do_customer', customer);
		orderLineRecord.setFieldValue('custrecord_do_carrier', shipCarrierMethod);	
		orderLineRecord.setFieldValue('custrecord_linestatus_flag', 25);
		orderLineRecord.setFieldValue('custrecord_route_no',route);
		orderLineRecord.setFieldValue('custrecord_lineord', fullFillmentorderno);
		orderLineRecord.setFieldValue('custrecord_ebiz_backorderno', backordline);
		orderLineRecord.setFieldValue('custrecord_ordline_company',company);
		if(actuallocation!=null)
		{
			orderLineRecord.setFieldValue('custrecord_ordline_wms_location',actuallocation);
		}
		else
		{
			orderLineRecord.setFieldValue('custrecord_ordline_wms_location',location);
		}
		orderLineRecord.setFieldValue('custrecord_actuallocation',location);
		orderLineRecord.setFieldValue('custrecord_record_linetime',TimeStamp());
		orderLineRecord.setFieldValue('custrecord_record_linedate',DateStamp());
		orderLineRecord.setFieldValue('custrecord_shipcomplete',shipcomplete);
		orderLineRecord.setFieldValue('custrecord_shipment_no',vShippingNo);
		if(vShipDate!='undefined')
			orderLineRecord.setFieldValue('custrecord_ebiz_shipdate',vShipDate);

		if(salesOrderDate!='undefined')
			orderLineRecord.setFieldValue('custrecord_ebiz_orderdate',salesOrderDate);

		if(customizationcode!=null && customizationcode!='' && customizationcode!=-1)
			orderLineRecord.setFieldValue('custrecord_ebiz_ordline_customizetype',customizationcode);

		//*******Item/Line information*************

		// Shipment
		var Shipment = searchresults.getValue('custcol_nswms_shipment_no');
		if(Shipment==null || Shipment=='')
			Shipment=searchresults.getValue('custbody_shipment_no');
		orderLineRecord.setFieldValue('custrecord_shipment_no', Shipment);
		nlapiLogExecution('DEBUG','Shipment',Shipment);

		//Sales Order Line #
		var salesOrderLineNo =searchresults.getValue('line');
		orderLineRecord.setFieldValue('custrecord_ordline', salesOrderLineNo);

		// Sales Order Item Id
		var itemId =searchresults.getValue('item');
		orderLineRecord.setFieldValue('custrecord_ebiz_linesku', itemId);

		var itemStatus =searchresults.getValue('custcol_ebiznet_item_status');
		var itemPackCode =searchresults.getValue('custcol_nswmspackcode');
		nlapiLogExecution('DEBUG','itemPackCode',itemPackCode);
		//var defpackcode = '1';
		var defpackcode = '';

		if(statusbyloc!=null && statusbyloc!='')
		{
			orderLineRecord.setFieldText('custrecord_linesku_status', statusbyloc);
			nlapiLogExecution('DEBUG','itemStatus',statusbyloc);
		}
		else
		{

			if( (itemStatus==null || itemStatus=='') || (itemPackCode==null || itemPackCode==''))
			{
				var fields = ['custitem_ebizoutbounddefskustatus','custitem_ebizdefpackcode'];
				var columns= nlapiLookupField('item',itemId,fields);
				if(columns!=null)
				{
					if(itemStatus==null || itemStatus=='')
						itemStatus = columns.custitem_ebizoutbounddefskustatus;
					defpackcode = columns.custitem_ebizdefpackcode;
				}
			}
		}

		if(itemStatus!=null && itemStatus!='')
		{
			orderLineRecord.setFieldValue('custrecord_linesku_status', itemStatus);
		}

		// Sales Order Item Pack Code

		if(itemPackCode==null || itemPackCode=='')
		{
			itemPackCode=defpackcode;
		}
		orderLineRecord.setFieldValue('custrecord_linepackcode', itemPackCode);
		nlapiLogExecution('DEBUG','itemPackCode',itemPackCode);

		// Sales Order Item Uom
		var baseUom = searchresults.getValue('custcol_nswmssobaseuom');
		nlapiLogExecution('DEBUG','baseUom',baseUom);
		if(baseUom != null && baseUom != '')
			//orderLineRecord.setFieldValue('custrecord_lineuom_id', parseFloat(baseUom));

		var vNSUOM = searchresults.getValue('unit');

		var veBizUOMQty=0;
		var vBaseUOMQty=0;

		if(vNSUOM!=null && vNSUOM!="")
		{
			var eBizItemDims=geteBizItemDimensions(itemId);
			if(eBizItemDims!=null&&eBizItemDims.length>0)
			{
				for(z=0; z < eBizItemDims.length; z++)
				{
					if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
					{
						vBaseUOMQty = eBizItemDims[z].getValue('custrecord_ebizqty');
					}

					if(vNSUOM == eBizItemDims[z].getValue('custrecord_ebiznsuom'))
					{
						veBizUOMQty = eBizItemDims[z].getValue('custrecord_ebizqty');
					}
				}

				if(veBizUOMQty==null || veBizUOMQty=='')
				{
					veBizUOMQty=vBaseUOMQty;
				}

				if(veBizUOMQty!=0)
					variance = parseFloat(variance)*parseFloat(veBizUOMQty)/parseFloat(vBaseUOMQty);
				nlapiLogExecution('DEBUG', 'variance', variance);
			}
		}

		// Sales Order Item Committed Quantity
		orderLineRecord.setFieldValue('custrecord_ord_qty', parseFloat(variance).toFixed(4));
		// Item Info for a particular Item
		var itemSubtype = nlapiLookupField('item', itemId, 'recordType');
		var fields = ['custitem_item_info_1', 'custitem_item_info_2', 'custitem_item_info_3'];
		var columnsval = nlapiLookupField(itemSubtype, itemId, fields);
		var skuinfo1val = columnsval.custitem_item_info_1;
		var skuinfo2val = columnsval.custitem_item_info_2;
		var skuinfo3val = columnsval.custitem_item_info_3;

		orderLineRecord.setFieldValue('custrecord_fulfilmentiteminfo1',skuinfo1val);
		orderLineRecord.setFieldValue('custrecord_fulfilmentiteminfo2',skuinfo2val);
		orderLineRecord.setFieldValue('custrecord_fulfilmentiteminfo3',skuinfo3val);
		orderLineRecord.setFieldValue('custrecord_linenotes2','Created by Schedule Script');
		if(vOrdType != null && vOrdType != '')			
			orderLineRecord.setFieldValue('custrecord_do_order_type',vOrdType);

		try
		{
			var wmsCarrier = nlapiGetFieldValue('custbody_salesorder_carrier');
			var wmscarrierlinelevel = searchresults.getValue('custcol_salesorder_carrier_linelevel');
			nlapiLogExecution('DEBUG', 'wmscarrier', wmsCarrier);
			nlapiLogExecution('DEBUG', 'wmscarrierlinelevel', wmscarrierlinelevel);

			if (wmscarrierlinelevel != null && wmscarrierlinelevel != "")
			{

				orderLineRecord.setFieldValue('custrecord_do_wmscarrier', wmscarrierlinelevel);
			}
		/*	else if (wmsCarrier != null && wmsCarrier != "")
			{

				orderLineRecord.setFieldValue('custrecord_do_wmscarrier', wmsCarrier);
			}*/

		}
		catch(exp) {
			nlapiLogExecution('DEBUG', 'Exception in wmsCarrier : ', exp);		
		}


		//submit the record 
		if(parseFloat(variance) != 0 ){
			var recordId = nlapiSubmitRecord(orderLineRecord);
			//updatesalesorderline(salesOrderInternalId,salesOrderLineNo,variance,'E');
			nlapiLogExecution('DEBUG','recordstatus','recordsubmited'+lineno);
		}		
	}
	catch(exp) {
		nlapiLogExecution('DEBUG', 'Exception in createbackorderFulfillmentOrdernew : ', exp);		
	}

	nlapiLogExecution('DEBUG','Out of createbackorderFulfillmentOrdernew','');
}
/***Below code is merged from Lexjet production account on 4th March 2013 by Ganesh ***/
function fulfillmentOrderDetailsforSchedulescript(salesOrderId,salesordername,solocation,solineshipmethod)
{/***Upto here ***/
	var fulfillmentNumber =  '';
	var salesOrderArray = new Array();
	var searchFulfillment =  getFullFillment(salesOrderId);
	//nlapiLogExecution('DEBUG','searchFulfillment :', searchFulfillment);

	//If searchfulfillment is null it indicates Its a New SO. 
	if(searchFulfillment == null){
		var fulfillmentNumber=salesordername +".1";
	}
	else{
		for(var i=0; i< searchFulfillment.length;i++)
		{
			var fullfillmentlineord = searchFulfillment[i].getValue('custrecord_lineord');
			var folinestatus = searchFulfillment[i].getValue('custrecord_linestatus_flag');
			/***Below code is merged from Lexjet production account on 4th March 2013 by Ganesh ***/
			var folocation = searchFulfillment[i].getValue('custrecord_ordline_wms_location'); //
			var foshipmethod = searchFulfillment[i].getValue('custrecord_do_carrier'); //
			/***Upto here***/
			var lineordsplit = fullfillmentlineord.split('.');
			salesOrderArray[i] = lineordsplit[1];
			/***Below code is merged from Lexjet production account on 4th March 2013 by Ganesh ***/
			if(folinestatus==25 && solocation==folocation && solineshipmethod==foshipmethod )
			{/***upto here ***/
				fulfillmentNumber=fullfillmentlineord;
				return fulfillmentNumber;
			}			
		}
		//MaxValue returns the max fullfillment order number
		var maximumValue = MaxValue(salesOrderArray);

		//adding 1 to the max fullfillment order number to get the new fullfillment order no.
		var fulfillmentValue = parseInt(maximumValue) + parseInt(1);
		nlapiLogExecution('DEBUG','fulfillment Value :', fulfillmentValue);

		//concatinating the fulfillmentvalue to salesorderid to get new fullfillment no.
		fulfillmentNumber =  salesordername + "." + fulfillmentValue;
		//nlapiLogExecution('DEBUG','fulfillmentNumber :', fulfillmentNumber);
	}
	return fulfillmentNumber;
}
function deleteEditedLine(soId)
{
	//reading the string from transaction body field
	var getAllLineNo = nlapiGetFieldValue('custbody_ebiz_lines_deleted');
	var shipcomplete = nlapiGetFieldValue('shipcomplete');
	nlapiLogExecution('DEBUG','getAllLineNo',getAllLineNo);

	var eachLineNo=new Array();

	//spliting at comma separator
	eachLineNo=getAllLineNo.split(',');
	nlapiLogExecution('DEBUG','ARRAYLENGTH',eachLineNo.length);

	for(var i=0;i<eachLineNo.length-1;i++)
	{
		//nlapiLogExecution('DEBUG','eachLineNo',eachLineNo[i]);
		//passing the lineNo which is deleted from SO to the function.
		DeleteRecCreated(soId, eachLineNo[i],shipcomplete);
	}

}
function DeleteRecCreated(soId, lineNo, shipcomplete)
{ 
	nlapiLogExecution('DEBUG','Into DeleteRecCreated');

	var str1 = 'soId. = ' + soId + '<br>';
	str1 = str1 + 'lineNo. = ' + lineNo + '<br>';
	str1 = str1 + 'shipcomplete. = ' + shipcomplete + '<br>';

	nlapiLogExecution('DEBUG', 'Function parameters', str1);

	var filter = new Array();
	filter[0]= new nlobjSearchFilter('name', null, 'is', soId);
	filter[1]= new nlobjSearchFilter('custrecord_ordline', null, 'equalto', lineNo);
	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_linestatus_flag') ;
	column[1] = new nlobjSearchColumn('custrecord_ordline') ;
	column[2] = new nlobjSearchColumn('custrecord_pickgen_qty') ;
	column[3] = new nlobjSearchColumn('custrecord_pickqty') ;

	var searchRec= nlapiSearchRecord('customrecord_ebiznet_ordline', null, filter, column);
	if(searchRec!=null)
		nlapiLogExecution('DEBUG','searchRec ',searchRec.length);
	var Id="";
	var Type="";

	if (searchRec)
	{
		for (var s = 0; s < searchRec.length; s++)
		{
			var searchresult = searchRec[s];
			nlapiLogExecution('DEBUG','searchresult',searchresult);
			Id = searchRec[s].getId();
			Type= searchRec[s].getRecordType();

			var fostatus = searchRec[s].getValue('custrecord_linestatus_flag');
			var pickgenqty = searchRec[s].getValue('custrecord_pickgen_qty');
			var pickqty = searchRec[s].getValue('custrecord_pickqty');

			if(pickgenqty==null || pickgenqty=='' || isNaN(pickgenqty))
				pickgenqty=0;

			if(pickqty==null || pickqty=='' || isNaN(pickqty))
				pickqty=0;

			var str = 'fostatus. = ' + fostatus + '<br>';
			str = str + 'pickgenqty. = ' + pickgenqty + '<br>';
			str = str + 'pickqty. = ' + pickqty + '<br>';

			nlapiLogExecution('DEBUG', 'FO Values', str);

			if(fostatus==25 && parseFloat(pickgenqty)==0)
			{
				nlapiDeleteRecord(searchRec[s].getRecordType(),searchRec[s].getId());
				//nlapiLogExecution('DEBUG','Deleted record Id',Id);
			}
			else
			{
				nlapiSubmitField('customrecord_ebiznet_ordline',Id,'custrecord_shipcomplete',shipcomplete);
			}
		}
	}
}