/**
 * 
 */
function GenerateBinLocation(request,response)
{
	if (request.getMethod() == 'GET') 
	{
		var form= nlapiCreateForm('Generate Bin Loction');
		Createfields(form);
		var vitem=request.getParameter('custparam_ebiz_assemItem');
		var varAssemblyQty=request.getParameter('custparam_ebiz_qty');
		var itemcount=request.getParameter('custparam_ebiz_itemcount');
		var items=request.getParameter('custparam_ebiz_items');
		fnGetDetails(vitem,varAssemblyQty,itemcount,items,form);
		response.writePage(form);
	}
	else
	{

	}
}

function Createfields(form)
{
	var itemSubList = form.addSubList("custpage_binlocation_list", "staticlist", "BinLoctionList");
	itemSubList.addField("custpage_component", "select", "Component","item").setDisplayType('inline');
	itemSubList.addField("custpage_lpno", "text", "LP#");
	itemSubList.addField("custpage_binlocation", "select", "Bin Location","customrecord_ebiznet_location").setDisplayType('inline');
	itemSubList.addField("custpage_qty", "text", "Qty");
	itemSubList.addField("custpage_lotnumber", "text", "Lot/Batch#");
	form.addSubmitButton('Allocate');
//	form.addButton('custpage_print','Print','Printreport('+vQbWave+')');
	
}

function fnGetDetails(vitem,varAssemblyQty,itemcount,items,form)
{
	var strItem="",strQty=0,vcomponent,vlp,vlocation,vqty,vlotno,vremainqty;
	var ItemType = nlapiLookupField('item', vitem, 'recordType');
	nlapiLogExecution('ERROR','ItemType',ItemType);
	var searchresultsitem;
	if(ItemType!=null && ItemType !='')
	{
		nlapiLogExecution('ERROR','Item Type1',ItemType);
		searchresultsitem = nlapiLoadRecord(ItemType, vitem); //1020
	}
	else
	{
		nlapiLogExecution('ERROR','Item Type is NULL ',ItemType);
	}
	var SkuNo=searchresultsitem.getFieldValue('itemid');
	var recCount=0; 

	var filters = new Array(); 
	filters[0] = new nlobjSearchFilter('itemid', null, 'is', SkuNo);//kit_id is the parameter name 

	var columns1 = new Array(); 
	columns1[0] = new nlobjSearchColumn( 'memberitem' ); 
	columns1[1] = new nlobjSearchColumn( 'memberquantity' );

	var j=0,k=1;
	var searchresults = nlapiSearchRecord( 'item', null, filters, columns1 ); 
	var vAlert='';
	for(var i=0; i<searchresults.length;i++) 
	{ 
		strItem = searchresults[i].getValue('memberitem');
		strQty = searchresults[i].getValue('memberquantity');
		nlapiLogExecution('ERROR','strItem',strItem);
		nlapiLogExecution('ERROR','strQty',strQty);
		var fields = ['custitem_item_family', 'custitem_item_group'];
		//Added by Sudheer on 121011 to make it general for all types of items

		//var columns = nlapiLookupField('inventoryitem', strItem, fields);

		var ComponentItemType = nlapiLookupField('item', strItem, 'recordType');
		nlapiLogExecution('ERROR','Component Item Type is',ComponentItemType);

		if (ComponentItemType != null && ComponentItemType !='')
		{
			var columns = nlapiLookupField(ComponentItemType, strItem, fields);
			var itemfamily = columns.custitem_item_family;
			var itemgroup = columns.custitem_item_group;
		}

		//PickStrategieKittoOrder(item, itemfamily, itemgroup, avlqty)    
		nlapiLogExecution('ERROR','Inventory Search',searchresults[i].getText('memberitem'));
		avlqty= (parseInt(strQty)*parseInt(varAssemblyQty));
		
		//var arryinvt=PickStrategieKittoOrder(strItem, itemfamily, itemgroup, avlqty);

		var arryinvt=PickStrategieKittoStock(strItem, itemfamily, itemgroup, avlqty);

		nlapiLogExecution('ERROR','Search End',searchresults[i].getText('memberitem'));
		// var searchresultsLine=getRecord(strItem)
		nlapiLogExecution('ERROR','arryinvt.length',arryinvt.length);

		var vitemqty=0;
		if(arryinvt.length>0)
		{
			nlapiLogExecution('ERROR', 'Inside If=', i);
			var lineno=i+1;
			for (j=0; j < arryinvt.length; j++) 
			{			
				var invtarray= arryinvt[j];  	
				vitem =searchresults[i].getValue('memberitem');		
				vitemText =searchresults[i].getText('memberitem');	
				vlp = invtarray[2];
				vqty = invtarray[0];
				vlocation = invtarray[1];

				var vrecid = invtarray[3];
				vlotno=invtarray[4];
				vremainqty=invtarray[5];
				nlapiLogExecution('ERROR', 'Outside If=', i);
                
//				vitemqty=parseFloat(vitemqty)+parseFloat(vqty);
//				}
//				if(avlqty > vitemqty )
//				{
//				vAlert=vAlert+vitemText+ ": You have only " + vitemqty +" available out of " +avlqty + "    \n";
//				}
//				else
//				{
//				nlapiLogExecution('ERROR','itemcount',itemcount);
//				for(var s=1;s<=itemcount;s++)
//				{
//				if(items.split('$')[s-1]==vitem)
//				{
//				form.getSubList('custpage_binlocation_list').setLineItemValue('custpage_component', i + 1, items.split('$')[s-1]);
				
				nlapiLogExecution('ERROR','remaining,qyt we get',avlqty+','+vqty);
				if(avlqty >= vqty)
				{
					form.getSubList('custpage_binlocation_list').setLineItemValue('custpage_component', lineno, vitem);
					form.getSubList('custpage_binlocation_list').setLineItemValue('custpage_lpno', lineno, vlp);
					form.getSubList('custpage_binlocation_list').setLineItemValue('custpage_binlocation', lineno, vlocation);
					form.getSubList('custpage_binlocation_list').setLineItemValue('custpage_lotnumber', lineno, vlotno);
					form.getSubList('custpage_binlocation_list').setLineItemValue('custpage_qty', lineno, vqty);
					lineno=lineno+1;
					avlqty=avlqty-vqty;
					
				}
				else
				{
					if(avlqty >0)
					{
						form.getSubList('custpage_binlocation_list').setLineItemValue('custpage_component', lineno, vitem);
						form.getSubList('custpage_binlocation_list').setLineItemValue('custpage_lpno', lineno, vlp);
						form.getSubList('custpage_binlocation_list').setLineItemValue('custpage_binlocation', lineno, vlocation);
						form.getSubList('custpage_binlocation_list').setLineItemValue('custpage_lotnumber', lineno, vlotno);
						form.getSubList('custpage_binlocation_list').setLineItemValue('custpage_qty', lineno, avlqty);
						lineno=lineno+1;
						
					}
					else
					{
						break;
					}
				}
//				}
//				else
//				nlapiCommitLineItem('item');
			}
//			}
		}
		else
		{
			nlapiLogExecution('ERROR','ifelse','DATEERROR');
//			vitemText =searchresults[i].getText('memberitem');
//			vAlert=vAlert+vitemText+ ": You have only 0 available out of " +avlqty + "    \n";	
			//alert(vAlert);
		}
	}
}

function PickStrategieKittoStock(item,itemfamily,itemgroup,avlqty)
{
	var actallocqty = 0;
	var Resultarray = new Array();
	nlapiLogExecution('ERROR','item',item);
	nlapiLogExecution('ERROR','itemgroup',itemgroup);
	nlapiLogExecution('ERROR','itemfamily',itemfamily);

	var filtersinvt = new Array();

	filtersinvt[0] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', item);
	filtersinvt[1] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof',['3','19']);//3-STATUS.INBOUND.PUTAWAY_COMPLETE OR 19-FLAG.INVENTORY.STORAGE
	//filtersinvt[1] = new nlobjSearchFilter('custrecord_invttasktype', null, 'anyof', [10]);
	//alert(item);

	//	filtersinvt[2] = new nlobjSearchFilter('custrecord_outboundinvlocgroupid', null, 'is', vlocgroupno);
	nlapiLogExecution('ERROR', "custrecord_ebiz_inv_sku: " + item);
	var columnsinvt = new Array();
	columnsinvt[0] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columnsinvt[1] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty');
	columnsinvt[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columnsinvt[3] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columnsinvt[4] = new nlobjSearchColumn('custrecord_ebiz_qoh');
	columnsinvt[5] = new nlobjSearchColumn('custrecord_pickseqno');
	columnsinvt[6] = new nlobjSearchColumn('custrecord_ebiz_inv_lot');
	columnsinvt[5].setSort();
	columnsinvt[4].setSort(true);

	var searchresultsinvt = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinvt, columnsinvt);
	for (var l = 0; searchresultsinvt != null && l < searchresultsinvt.length; l++)
	{
		var searchresult = searchresultsinvt[l];
		var actqty = searchresult.getValue('custrecord_ebiz_qoh');//1000
		var allocqty = searchresult.getValue('custrecord_ebiz_alloc_qty');//0
		var LP = searchresult.getValue('custrecord_ebiz_inv_lp');
		var vactLocation = searchresult.getValue('custrecord_ebiz_inv_binloc');
		var vactLocationtext = searchresult.getText('custrecord_ebiz_inv_binloc');
		var vlotno = searchresult.getValue('custrecord_ebiz_inv_lot');
		var vlotText = searchresult.getText('custrecord_ebiz_inv_lot');
		var Recid = searchresult.getId(); 
		
		nlapiLogExecution('ERROR', 'Recid1', Recid);
		if (isNaN(allocqty)) {
			allocqty = 0;
		}
		if (allocqty == "") {
			allocqty = 0;
		}
		if( parseInt(actqty)<0)
		{
			actqty=0;
		}
		nlapiLogExecution('ERROR', "allocqty: " + allocqty);
		nlapiLogExecution('ERROR', "LP: " + LP);

		var remainqty = actqty - allocqty;
		nlapiLogExecution('ERROR', 'remainqty' + remainqty);

		var cnfmqty = 0;
		nlapiLogExecution('ERROR', 'LOOP BEGIN');

		if (remainqty > 0) {
			nlapiLogExecution('ERROR', 'INTO LOOP');
			if ((avlqty - actallocqty) <= remainqty) {
				cnfmqty = avlqty - actallocqty;
				actallocqty = avlqty;
			}
			else {
				cnfmqty = remainqty;
				actallocqty = actallocqty + remainqty;
			}
			nlapiLogExecution('ERROR','cnfmqty', cnfmqty);
			nlapiLogExecution('ERROR','LotNo', vlotno);
			if (cnfmqty > 0) {
				//Resultarray
				var invtarray = new Array();
				invtarray[0]= cnfmqty;							
				invtarray[1]= vactLocation;
				invtarray[2] = LP;
				invtarray[3] = Recid;
				invtarray[4] = vlotText;
				invtarray[5] =remainqty;
				nlapiLogExecution('ERROR', 'RecId:',Recid);
				Resultarray.push(invtarray);								
			}
		}

		if ((avlqty - actallocqty) == 0) {
			nlapiLogExecution('ERROR', 'Into Break:');
			return Resultarray;
		}
	} 
	return Resultarray;  
}

