/***************************************************************************
�����������������������������
�����������������eBizNET Solutions Inc
****************************************************************************
*
*� $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inbound/Client/ebiz_CheckBinLocation_CL.js,v $
*� $Revision: 1.3.4.1.8.1 $
*� $Date: 2013/09/11 15:23:51 $
*� $Author: rmukkera $
*� $Name: t_NSWMS_2014_1_1_174 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_CheckBinLocation_CL.js,v $
*� Revision 1.3.4.1.8.1  2013/09/11 15:23:51  rmukkera
*� Case# 20124376
*�
*� Revision 1.3.4.1  2012/01/09 14:04:14  spendyala
*� CASE201112/CR201113/LOG201121
*� resolved issue for occurrence of unexpected error.
*�
*
****************************************************************************/


/**
 * @param type
 * @returns {Boolean}
 */
function fnCheckBinLocation(type)
{	
	var varBinLocName =  nlapiGetFieldValue('name');  
	var vLocation	 = nlapiGetFieldValue('custrecord_ebizsitelocf');  
	var vCompany	 = nlapiGetFieldValue('custrecord_ebizcompanylocf');  
	var vBinLocType = nlapiGetFieldValue('custrecord_ebizlocationtype');  

	/*
	 * Added on 1/4/2011 by Phani
	 * The below variable will fetch the internal id of the record selected.
	 */    
	if(varBinLocName!=null && varBinLocName!="" && vLocation!=null && vLocation!="" && vCompany!=null && vCompany!=""&&vBinLocType!=""&&vBinLocType!=null)
	{
		var vId = nlapiGetFieldValue('id');

		var filters = new Array();
		filters[0] = new nlobjSearchFilter('name', null, 'is',varBinLocName.replace(/\s+$/, ""));//Added by suman on 09/01/12 to trim spaces at right.
		filters[1] = new nlobjSearchFilter('custrecord_ebizsitelocf', null, 'anyof',[vLocation]);
		filters[2] = new nlobjSearchFilter('custrecord_ebizcompanylocf', null, 'anyof',[vCompany]);
		filters[3] = new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'anyof',[vBinLocType]);

		/*
		 * Added on 1/4/2011 by Phani
		 * The below variable will filter based on the internal id of the record selected.
		 */

		if (vId != null && vId != "") {
			filters[4] = new nlobjSearchFilter('internalid', null, 'noneof', vId);
		}

		var searchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters);

		if(searchresults)
		{
			alert("Bin Location already exists.");
			return false;
		}
		else
		{
			return true;
		} 
	}
	else	
	{
		return true;
	}
}