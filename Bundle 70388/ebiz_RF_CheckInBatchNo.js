/***************************************************************************
 eBizNET Solutions Inc 
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_CheckInBatchNo.js,v $
 *     	   $Revision: 1.7.2.5.4.9.2.31.2.2 $
 *     	   $Date: 2015/12/01 14:38:05 $
 *     	   $Author: deepshikha $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_206 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_CheckInBatchNo.js,v $
 * Revision 1.7.2.5.4.9.2.31.2.2  2015/12/01 14:38:05  deepshikha
 * 2015.2 Issue Fix
 * 201415093
 *
 * Revision 1.7.2.5.4.9.2.31.2.1  2015/11/06 15:49:42  skreddy
 * case 201415347
 * 2015.2  issue fix
 *
 * Revision 1.7.2.5.4.9.2.31  2015/08/07 19:05:18  snimmakayala
 * Case#: 201413571
 *
 * Revision 1.7.2.5.4.9.2.30  2015/08/04 14:11:40  grao
 * 2015.2   issue fixes  201413645
 *
 * Revision 1.7.2.5.4.9.2.29  2014/10/29 14:53:21  skavuri
 * Case# 201410517 Std bundle issue fixed
 *
 * Revision 1.7.2.5.4.9.2.28  2014/10/17 12:58:13  skavuri
 * Case# 201410580 Std bundle Issue fixed
 *
 * Revision 1.7.2.5.4.9.2.27  2014/09/26 15:03:53  sponnaganti
 * Case# 201410517
 * Stnd Bundle Issue fix
 *
 * Revision 1.7.2.5.4.9.2.26  2014/09/19 15:40:12  skavuri
 * case # 201410442 Std Bundle issue fixed
 *
 * Revision 1.7.2.5.4.9.2.25  2014/08/20 15:41:59  sponnaganti
 * Case# 201410033
 * Stnd Bundle Issue fix
 *
 * Revision 1.7.2.5.4.9.2.24  2014/07/24 15:26:13  nneelam
 * case#  20149650
 * New UI Issue Fix.
 *
 * Revision 1.7.2.5.4.9.2.23  2014/07/07 16:10:10  sponnaganti
 * case# 20149288
 * Comaptibility Issue fix
 *
 * Revision 1.7.2.5.4.9.2.22  2014/07/03 15:21:15  skavuri
 * Case# 20149238 Compatability Issue Fixed
 *
 * Revision 1.7.2.5.4.9.2.21  2014/06/27 10:54:10  sponnaganti
 * case# 20149006 20149059
 * Compatability Test Acc issue fix
 *
 * Revision 1.7.2.5.4.9.2.20  2014/06/18 13:52:06  grao
 * Case#: 20148958 New GUI account issue fixes
 *
 * Revision 1.7.2.5.4.9.2.19  2014/06/13 07:26:26  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.7.2.5.4.9.2.18  2014/06/06 06:43:36  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.7.2.5.4.9.2.17  2014/05/30 00:26:48  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.7.2.5.4.9.2.16  2014/02/17 14:48:26  rmukkera
 * Case # 20127169,20127193
 *
 * Revision 1.7.2.5.4.9.2.15  2014/01/24 13:41:22  schepuri
 * 20126901
 * standard bundle issue fix
 *
 * Revision 1.7.2.5.4.9.2.14  2013/12/24 15:13:06  rmukkera
 * Case # 20126458
 *
 * Revision 1.7.2.5.4.9.2.13  2013/11/08 14:36:43  schepuri
 * 20125257
 *
 * Revision 1.7.2.5.4.9.2.12  2013/10/24 13:30:04  rmukkera
 * Case# 20124514
 *
 * Revision 1.7.2.5.4.9.2.11  2013/10/07 13:49:04  schepuri
 * 20124823
 *
 * Revision 1.7.2.5.4.9.2.10  2013/09/26 15:47:57  rmukkera
 * Case# 20124514�
 *
 * Revision 1.7.2.5.4.9.2.9  2013/08/29 14:36:01  skreddy
 * Case# 20123285
 *  restict space charater in lot# feild
 *
 * Revision 1.7.2.5.4.9.2.8  2013/08/26 15:50:00  skreddy
 * Case# 20124071
 * standard bundle- issue fix
 *
 * Revision 1.7.2.5.4.9.2.7  2013/08/23 07:34:51  schepuri
 * case no 20123569
 *
 * Revision 1.7.2.5.4.9.2.6  2013/08/21 14:22:59  skreddy
 * Case# 20123285
 *  restict space charater in lot# feild
 *
 * Revision 1.7.2.5.4.9.2.5  2013/07/31 14:30:29  rrpulicherla
 * case# 20123676
 * GFT issues
 *
 * Revision 1.7.2.5.4.9.2.4  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.7.2.5.4.9.2.3  2013/06/04 07:26:55  skreddy
 * CASE201112/CR201113/LOG201121
 * Lot Parsing CR for PCT
 *
 * Revision 1.7.2.5.4.9.2.2  2013/04/17 16:04:01  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.7.2.5.4.9.2.1  2013/03/01 15:01:12  rmukkera
 * code is merged from FactoryMation production as part of Standard bundle
 *
 * Revision 1.7.2.5.4.9  2013/02/07 15:10:34  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.7.2.5.4.8  2013/02/07 08:37:49  skreddy
 * CASE201112/CR201113/LOG201121
 * RF Lot auto generating FIFO enhancement
 *
 * Revision 1.7.2.5.4.7  2013/01/24 03:30:55  kavitha
 * CASE201112/CR201113/LOG201121
 * CASE201213525 - Factory Mation - RF RMA issue fixes
 *
 * Revision 1.7.2.5.4.6  2013/01/08 15:41:28  gkalla
 * CASE201112/CR201113/LOG201121
 * Lexjet issue for batch entry irrespective of location
 *
 * Revision 1.7.2.5.4.5  2012/12/21 15:30:06  skreddy
 * CASE201112/CR201113/LOG201121
 * issues related The record type [] is invalid
 *
 * Revision 1.7.2.5.4.4  2012/11/01 14:55:35  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.7.2.5.4.3  2012/09/27 10:56:55  grao
 * CASE201112/CR201113/LOG201121
 *
 * Converting multiple language with given Spanish terms
 *
 * Revision 1.7.2.5.4.2  2012/09/26 12:47:07  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.7.2.5.4.1  2012/09/21 14:57:16  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multilanguage
 *
 * Revision 1.7.2.5  2012/06/28 14:33:46  spendyala
 * CASE201112/CR201113/LOG201121
 * Missing parameters while passing to query string.
 *
 * Revision 1.7.2.4  2012/04/20 10:46:21  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.7.2.3  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.7.2.2  2012/02/24 10:34:49  spendyala
 * CASE201112/CR201113/LOG201121
 * Passing BaseUomQty To Query String.
 *
 * Revision 1.7.2.1  2012/02/22 12:18:48  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.7  2011/12/23 16:21:51  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Inbound RF
 *
 * Revision 1.4  2011/06/30 13:06:55  pattili
 * CASE201112/CR201113/LOG201121
 * 1.Replenishment changes
 * 2. RF changes in Replenishment and Check-in
 *
 * Revision 1.3  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/

function CheckInBatchNo(request, response){
	if (request.getMethod() == 'GET') {

		//	Get the PO# from the previous screen, which is passed as a parameter		
		var getPONo = request.getParameter('custparam_poid');
		var getPOItem = request.getParameter('custparam_poitem');
		var getPOLineNo = request.getParameter('custparam_lineno');
		var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		var getPOInternalId = request.getParameter('custparam_pointernalid');
		var getPOQtyEntered = request.getParameter('custparam_poqtyentered');
		var getPOItemRemainingQty = request.getParameter('custparam_poitemremainingqty');
		var getPOLinePackCode = request.getParameter('custparam_polinepackcode');
		var getPOLineItemStatus = request.getParameter('custparam_polineitemstatus');
		var getItemCube = request.getParameter('custparam_itemcube');
		var getItemBaseUomQty=request.getParameter('custparam_baseuomqty');
//		var getItemQuantity = request.getParameter('hdnQuantity');
//		var getItemQuantityReceived = request.getParameter('hdnQuantityReceived');
		var getItemQuantity = request.getParameter('custparam_polinequantity');
		var getItemQuantityReceived = request.getParameter('custparam_polinequantityreceived');
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');
		var getWHLocation = request.getParameter('custparam_whlocation');
		nlapiLogExecution('DEBUG','getWHLocation',getWHLocation);
		var getPOLineItemStatusText = request.getParameter('custparam_polineitemstatustext');//Case# 201410580

		var trantype= request.getParameter('custparam_trantype');
		nlapiLogExecution('DEBUG','trantype', trantype);

		var st0,st1,st2,st3,st4;
		if( getLanguage == 'es_ES')
		{

			st0 = "CHECKIN LOTE #";
			st1 = "ART&#205;CULO";	
			st2 = "INGRESAR LOTE#:";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";


		}
		else
		{
			st0 = "CHECKIN  LOT#";
			st1 = "ITEM";	
			st2 = "ENTER LOT#:";
			st3 = "SEND";
			st4 = "PREV";



		}		


		var functionkeyHtml=getFunctionkeyScript('_rf_cyclecountsku'); 
		var html = "<html><head><title>" + st0 + "</title>" ;
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends      
		//html = html + " document.getElementById('enterbatch').focus();";   

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";

		html = html + "</script>";


		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cyclecountsku' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + ":  <label>'" + getPOItem + "'</label></td>";
		html = html + "				<input type='hidden' name='hdnItemStatus' value='" + getPOLineItemStatus + "'></td>";
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				<input type='hidden' name='hdnItemPackCode' value=" + getPOLinePackCode + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getItemQuantity + ">";
		html = html + "				<input type='hidden' name='hdnQuantityReceived' value=" + getItemQuantityReceived + ">";
		html = html + "				<input type='hidden' name='hdnItemCube' value=" + getItemCube + ">";
		html = html + "				<input type='hidden' name='hdnBaseUomQty' value=" + getItemBaseUomQty + ">";
		html = html + "				<input type='hidden' name='hdnItemRemaininingQuantity' value=" + parseFloat(getPOItemRemainingQty) + ">";
		html = html + "				<input type='hidden' name='hdnPOQuantityEntered' value=" + parseFloat(getPOQtyEntered) + ">";
		html = html + "				<input type='hidden' name='hdnPOInternalId' value=" + getPOInternalId + ">";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + getWHLocation + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				<input type='hidden' name='hdntrantype' value=" + trantype + ">";
		html = html + "				<input type='hidden' name='hdnitemstatustext' value='" + getPOLineItemStatusText + "'>";//Case# 201410580
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st2;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterbatch' id='enterbatch' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					" + st4 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterbatch').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		try {
			var POarray = new Array();
			var optedEvent = request.getParameter('cmdPrevious');
			var getBatch = request.getParameter('enterbatch');
			var ActualBatch = request.getParameter('enterbatch');
			var getLanguage = request.getParameter('hdngetLanguage');
			POarray["custparam_language"] = getLanguage;
			nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);


			var st8,st9;
			if( getLanguage == 'es_ES')
			{

				st8 = "LOT NO V&#193;LIDO #";
				st9 = "LOTE # IS NULL";

			}
			else
			{

				st8 = "INVALID LOT#";
				st9 = "LOT# IS NULL";


			}
			nlapiLogExecution('DEBUG', 'optedEvent', optedEvent);
			nlapiLogExecution('DEBUG', 'getBatch', getBatch);

			POarray["custparam_poid"] = request.getParameter('custparam_poid');
			POarray["custparam_poitem"] = request.getParameter('custparam_poitem');
			POarray["custparam_lineno"] = request.getParameter('custparam_lineno');
			POarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
			POarray["custparam_pointernalid"] = request.getParameter('hdnPOInternalId'); //request.getParameter('custparam_pointernalid');
			POarray["custparam_poqtyentered"] = request.getParameter('hdnPOQuantityEntered'); //request.getParameter('custparam_poqtyentered');
			POarray["custparam_poitemremainingqty"] = request.getParameter('hdnItemRemaininingQuantity'); //request.getParameter('custparam_poitemremainingqty');
			POarray["custparam_polinepackcode"] = request.getParameter('hdnItemPackCode');//request.getParameter('custparam_polinepackcode');
			POarray["custparam_polineitemstatus"] = request.getParameter('hdnItemStatus');
			POarray["custparam_polinequantity"] = request.getParameter('hdnQuantity'); //request.getParameter('custparam_polinequantity');
			POarray["custparam_polinequantityreceived"] = request.getParameter('hdnQuantityReceived'); //request.getParameter('custparam_polinequantityreceived');
			POarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
			POarray["custparam_itemcube"] = request.getParameter('custparam_itemcube');

			//code added on 24 feb 2012 by suman
			//To get the baseuom qty .
			POarray["custparam_baseuomqty"] = request.getParameter('hdnBaseUomQty');
			nlapiLogExecution('DEBUG','ITEMINFO[0]',POarray["custparam_itemcube"]);
			nlapiLogExecution('DEBUG','ITEMINFO[1]',POarray["custparam_baseuomqty"]);
			//end of code as of 24 feb 2012.

			POarray["custparam_actualbegindate"] = request.getParameter('hdnActualBeginDate');
			POarray["custparam_actualbegintime"] = request.getParameter('hdnActualBeginTime');
			POarray["custparam_actualbegintimeampm"] = request.getParameter('hdnActualBeginTimeAMPM');
			POarray["custparam_actualbegintime"] = request.getParameter('hdnActualBeginTime');
			POarray["custparam_actualbegintimeampm"] = request.getParameter('hdnActualBeginTimeAMPM');

			POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
			POarray["custparam_trantype"] = request.getParameter('hdntrantype');
			nlapiLogExecution('DEBUG', 'WH Location', POarray["custparam_whlocation"]);
			nlapiLogExecution('DEBUG', 'Fetched Item Id', POarray["custparam_fetcheditemid"]);
			POarray["custparam_polineitemstatustext"] = request.getParameter('hdnitemstatustext');//Case# 201410580
			POarray["custparam_error"] = st8;
			POarray["custparam_screenno"] = 'PLT2';
			var ItemType = '';					
			var batchflg = '';

			if (optedEvent == 'F7') {
				nlapiLogExecution('DEBUG', 'RF CHECKIN BATCH F7 Pressed');

				var trantype= nlapiLookupField('transaction',POarray["custparam_pointernalid"],'recordType');
				POarray["custparam_trantype"] = trantype;
				nlapiLogExecution('DEBUG', 'trantype',trantype);
				if(trantype=='purchaseorder')
				{
					response.sendRedirect('SUITELET', 'customscript_rf_checkin_item_status', 'customdeploy_rf_checkin_item_status_di', false, POarray);
				}
				else if(trantype=='returnauthorization')
				{
					response.sendRedirect('SUITELET', 'customscript_rf_rma_checkin_item_status', 'customdeploy_rf_rma_checkin_item_status', false, POarray);
				}//Case # 20127193 Start
				else if(trantype=='transferorder')
				{
					response.sendRedirect('SUITELET', 'customscript_rf_to_checkin_item_status', 'customdeploy_rf_to_checkin_item_status_d', false, POarray);	
				}//Case # 20127193 End

				//response.sendRedirect('SUITELET', 'customscript_rf_checkin_item_status', 'customdeploy_rf_checkin_item_status_di', false, POarray);
			}
			else {

				if(ActualBatch !=null && ActualBatch !='')
				{
					var result=ValidateSplCharacter(ActualBatch,'Lot #');
					nlapiLogExecution('ERROR','result',result);
					if(result == false)
					{
						POarray["custparam_error"] = 'SPECIAL CHARS NOT ALLOWED IN LOT#';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					}
					else
					{
						if(POarray["custparam_pointernalid"] !=null && POarray["custparam_pointernalid"] !='')
						{
							//case no 20125257
							//case# 20149006 starts
							var trantype= nlapiLookupField('transaction',POarray["custparam_pointernalid"],'recordType');
							var fields;
							if(trantype=='purchaseorder' || trantype=='returnauthorization')
								fields = ['recordType','location'];
							else
								fields = ['recordType','location','transferlocation'];
							var columns= nlapiLookupField('transaction',POarray["custparam_pointernalid"],fields);
							//var trantype= '';
							var vLocation = '';
							var ToLocation = '';
							//case 201415093
							try
							{
									var filterBatchn=new Array();


									filterBatchn[0]=new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',ActualBatch);

									var columns = new Array();
									columns[0] = new nlobjSearchColumn('custrecord_ebizlotbatch');

									columns[1] = new nlobjSearchColumn('isinactive');

									var rec=nlapiSearchRecord('customrecord_ebiznet_batch_entry',null,filterBatchn,columns);

									if(rec!=null && rec !='' && rec != 'null')
									{   

										var inactivflag = rec[0].getValue('isinactive');
										nlapiLogExecution('DEBUG', 'Entered Item batch into if', ActualBatch);


										if(inactivflag == 'T')
										{
											/*POarray["custparam_batchno"] = request.getParameter('hdnBatchNo');*/
											POarray["custparam_error"] = "Inactive Lot";
											response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
											nlapiLogExecution('DEBUG', 'Entered Item batch', ActualBatch);
											return;

										}
									}
								
							}
							catch(exc)
							{
									nlapiLogExecution('DEBUG', 'exc', exc);
							}
							
							//var trantype= nlapiLookupField('transaction',POarray["custparam_pointernalid"],'recordType');
							if(columns != null && columns != '')
							{
								//trantype = columns.recordType;
								vLocation = columns.location;
								ToLocation = columns.transferlocation;
							}


							nlapiLogExecution('DEBUG', 'Before Parsing ',getBatch);
							nlapiLogExecution('DEBUG', 'trantype ',trantype);
							nlapiLogExecution('DEBUG', 'vLocation ',vLocation);
							nlapiLogExecution('DEBUG', 'ToLocation ',ToLocation);
							if(trantype=='purchaseorder')
							{
								var vendor = nlapiLookupField('purchaseorder',POarray["custparam_pointernalid"],'entity');


								var getBatch = GetLotParsingResults(POarray["custparam_fetcheditemid"],vendor,getBatch);
								nlapiLogExecution('DEBUG', 'After Parsing ',getBatch);
							}
							
							var ctx = nlapiGetContext();
							if(ctx != null && ctx != '')
							{
								if(ctx.getFeature('advbinseriallotmgmt').toString() != null && ctx.getFeature('advbinseriallotmgmt').toString() != '')
									vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
								nlapiLogExecution('ERROR', 'vAdvBinManagement_if', vAdvBinManagement);
							}
							
							//case 20124071 start
							if(trantype=='transferorder')
							{
								var fields1 = ['transferlocation'];
								var columns1= nlapiLookupField('transaction',POarray["custparam_pointernalid"],fields1);
								ToLocation = columns1.transferlocation;
								var fields = ['recordType', 'custitem_ebizbatchlot'];
								var columns = nlapiLookupField('item', POarray["custparam_fetcheditemid"], fields);
								if(columns != null && columns != '')
								{
									ItemType = columns.recordType;					
									batchflg = columns.custitem_ebizbatchlot;
								}
								if(ItemType == "inventoryitem" && batchflg == 'T')
								{
									if(vLocation!=null && vLocation!='')
									{
										var fields = ['custrecord_ebizwhsite'];

										var locationcolumns = nlapiLookupField('Location', vLocation, fields);
										if(locationcolumns!=null)
											mwhsiteflag = locationcolumns.custrecord_ebizwhsite;
									}
									if(ToLocation!=null && ToLocation!='')
									{
										var fields = ['custrecord_ebizwhsite'];

										var locationcolumns = nlapiLookupField('Location', ToLocation, fields);
										if(locationcolumns!=null)
											Tomwhsiteflag = locationcolumns.custrecord_ebizwhsite;
									}
									nlapiLogExecution('Error', 'mwhsiteflag', mwhsiteflag);
									nlapiLogExecution('Error', 'Tomwhsiteflag', Tomwhsiteflag);
									nlapiLogExecution('Error', 'POarray[custparam_pointernalid]', POarray["custparam_pointernalid"]);

									if(mwhsiteflag == 'T' && Tomwhsiteflag == 'T')
									{
										batchflg = 'T';
									}
									else
										batchflg = 'F';
								}
							//	if(ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem" || batchflg == 'T')
								if(ItemType == "lotnumberedinventoryitem" || ItemType=="lotnumberedassemblyitem")
								{
									var IsValidSerailNumber='F';

									var trecord = nlapiLoadRecord('transferorder', POarray["custparam_pointernalid"]);

									var links=trecord.getLineItemCount('links');
									if(links!=null  && links!='')
									{
										for(var j=0;j<links &&  IsValidSerailNumber=='F';j++)
										{
											var id=trecord.getLineItemValue('links','id',(parseInt(j)+1));
											var linktype = trecord.getLineItemValue('links','type',(parseInt(j)+1));

											nlapiLogExecution('DEBUG', 'linktype',linktype);
											nlapiLogExecution('DEBUG', 'id',id);

											if(linktype=='Item Fulfillment'|| linktype=='Item Shipment')
											{
												var frecord = nlapiLoadRecord('itemfulfillment', id);
// case no start 20126901

												if(vAdvBinManagement)
												{	
													//case# 201410033 (if itemfulfillment have two or more lines then we taking itemcount based on this we are looping for getting Lot Number)
													var itemcount=frecord.getLineItemCount('item');
													for(var h=1;h<=itemcount;h++)
													{
														//frecord.selectLineItem('item', 1);
														frecord.selectLineItem('item', h);
														var item=frecord.getLineItemValue('item','item',h);
														if(item==POarray["custparam_fetcheditemid"])
														{	
															var compSubRecord = frecord.viewCurrentLineItemSubrecord('item','inventorydetail');
															//Case # 20127169Start
															if(compSubRecord !=null && compSubRecord!='' && compSubRecord!='null')
															{
																//Case # 20127169End
																var polinelength = compSubRecord.getLineItemCount('inventoryassignment');
																nlapiLogExecution('ERROR', 'polinelength', polinelength);
																var itemfulfilserialno;
																var itemTempfulfilserialno;
																for(var k=1;k<=polinelength ;k++)
																{
																	itemfulfilserialno=compSubRecord.getLineItemText('inventoryassignment','issueinventorynumber',k);
																	nlapiLogExecution('ERROR', 'itemfulfilserialno', itemfulfilserialno);
																	if(itemfulfilserialno!=null && itemfulfilserialno!='')
																	{
																		if(itemTempfulfilserialno == null || itemTempfulfilserialno == '')
																			itemTempfulfilserialno=itemfulfilserialno;
																		else										 
																			itemTempfulfilserialno= itemTempfulfilserialno + ',' + itemfulfilserialno;

																	}
																	nlapiLogExecution('ERROR', 'itemTempfulfilserialno', itemTempfulfilserialno);
																}
																var  serialnumbers= itemTempfulfilserialno;
																if(serialnumbers!=null && serialnumbers!='')
																{
																	var tserials=serialnumbers.split(',');
																	//nlapiLogExecution('DEBUG', 'serialnumbers',tserials[0]);
																	if(tserials!=null && tserials!='' && tserials.length>0)
																	{
																		if(tserials.indexOf(ActualBatch)!=-1)
																		{
																			nlapiLogExecution('DEBUG', 'getSerialNo111111',ActualBatch);
																			IsValidSerailNumber='T';
																			break;
																		}
																	}
																}
																// case no end 20126901
																//Case # 20127169Start
															}
														}
														//Case # 20127169End
													}
												}
												else
												{

													var fitemcount=frecord.getLineItemCount('item');
													for(var f=1;f<=fitemcount;f++)
													{
														var fitem=frecord.getLineItemValue('item','item',f);
														var fline=frecord.getLineItemValue('item','orderline',f);
														var pofline= fline-1;

														nlapiLogExecution('DEBUG', 'fitem',fitem);
														nlapiLogExecution('DEBUG', 'POarray[custparam_fetcheditemid]',POarray["custparam_fetcheditemid"]);

														if(fitem==POarray["custparam_fetcheditemid"]) //&& parseInt(getPOLineNo)==parseInt(pofline))
														{
															var  serialnumbers=frecord.getLineItemValue('item','serialnumbers',(parseInt(f)));
															nlapiLogExecution('DEBUG', 'serialnumbers',serialnumbers);
															/*if(serialnumbers!=null && serialnumbers!='')
											{
												if(serialnumbers.indexOf(ActualBatch)!=-1)
												{
													IsValidSerailNumber='T';
													break;
												}
											}*/
															if(serialnumbers!=null && serialnumbers!='')
															{
																var tserials=serialnumbers.split('');
																//nlapiLogExecution('DEBUG', 'serialnumbers',tserials[0]);
																if(tserials!=null && tserials!='' && tserials.length>0)
																{
																	if(tserials.indexOf(ActualBatch)!=-1)
																	{
																		nlapiLogExecution('DEBUG', 'getSerialNo111111',ActualBatch);
																		IsValidSerailNumber='T';
																		break;
																	}
																}
															}
															else
															{
																if(ItemType == "inventoryitem" && batchflg == 'T')
																{
																	nlapiLogExecution('DEBUG', 'serialnumbers new',serialnumbers);
																	var filters = new Array();
																	//filters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_cntrl_no', null, 'is', putPOId));
																	filters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no', null, 'anyof', POarray["custparam_pointernalid"]));
																	filters.push(new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', [3])); //taskType = pick
																	filters.push(new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag', null, 'anyof', [14])); // wmsStatusFlag=INBOUND/CHECK-IN
																	filters.push(new nlobjSearchFilter('custrecord_ebiztask_sku', null, 'anyof', POarray["custparam_fetcheditemid"])); 
																	filters.push(new nlobjSearchFilter('custrecord_ebiz_nsconf_refno_ebiztask', null, 'is', id)); 


																	var columns = new Array();
																	columns[0] = new nlobjSearchColumn('custrecord_ebiztask_batch_no');

																	// execute the  search, passing null filter and return columns
																	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filters, columns);

																	nlapiLogExecution('DEBUG', 'serialnumbers new1',serialnumbers);

																	var serialnumbers = '';
																	if(searchresults != null && searchresults != '')
																	{
																		serialnumbers = searchresults[0].getValue('custrecord_ebiztask_batch_no');

																		nlapiLogExecution('DEBUG', 'serialnumbers new new',serialnumbers);

																		if(serialnumbers.indexOf(ActualBatch)!=-1)
																		{
																			IsValidSerailNumber='T';
																			break;
																		}

																	}


																}

															}

														}
													}
												}//advanced bin management false
											}
										}
									}

									if(IsValidSerailNumber=='F')
									{
										//POarray["custparam_error"] = "Matching Serial Number required.</br>The Serial number on a transfer order receipt must have been fulfilled.";
										//Case# 201410442 starts
										//POarray["custparam_screenno"] = '2T';  
										POarray["custparam_screenno"] = 'PLT2';
										//Case# 201410442 ends
										POarray["custparam_error"] = "Lot# is not matching with Lot# picked on the same TO ";
										response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
										return; 
									}
								}


							}//end


						}

						//case# 20149288 starts
						/*//Case# 20149238 starts
						var getValidBatch = getBatchNo(POarray["custparam_fetcheditemid"],POarray["custparam_polinepackcode"],
								POarray["custparam_whlocation"],getBatch);
						if(getValidBatch=='' ||getValidBatch=='null' || getValidBatch==null)
						{
							POarray["custparam_error"] = st8;
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							nlapiLogExecution('DEBUG','Invalid Lot#','Invalid Lot#');
							return;
						}
						//Case# 20149238 ends*/
						//case# 20149288 ends
						var getSearchBatch = getBatchNoByMfcLot(POarray["custparam_fetcheditemid"],POarray["custparam_polinepackcode"],
								POarray["custparam_whlocation"],ActualBatch);

						/*var getSearchBatch = getBatchNo(POarray["custparam_fetcheditemid"],POarray["custparam_polinepackcode"],
						POarray["custparam_whlocation"],getBatch);*/

						POarray["custparam_actscanbatchno"] = ActualBatch;
						if(getSearchBatch != null && getSearchBatch != ''){
							//case# 20149288 starts
							//case # 20149650, Allowing Lot irrespective of lot and loc mapping but creating new lot record when lot is not a checkin location.
							/*var getValidBatch = getBatchNo(POarray["custparam_fetcheditemid"],POarray["custparam_polinepackcode"],
									POarray["custparam_whlocation"],getBatch);
							if(getValidBatch=='' ||getValidBatch=='null' || getValidBatch==null)
							{
								POarray["custparam_error"] = st8;
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								nlapiLogExecution('DEBUG','Invalid Lot#','Invalid Lot#');
								return;
							}*/
							//Case# 20149288 ends
							
							POarray["custparam_expdate"] = getSearchBatch[0].getValue('custrecord_ebizexpirydate');
							POarray["custparam_mfgdate"] = getSearchBatch[0].getValue('custrecord_ebizmfgdate');
							POarray["custparam_bestbeforedate"] = getSearchBatch[0].getValue('custrecord_ebizbestbeforedate');
							POarray["custparam_fifodate"]=getSearchBatch[0].getValue('custrecord_ebizfifodate');
							POarray["custparam_lastdate"]=getSearchBatch[0].getValue('custrecord_ebizlastavldate');
							POarray["custparam_batchno"] = getBatch;
							response.sendRedirect('SUITELET', 'customscript_rf_checkin_lp', 'customdeploy_rf_checkin_lp_di', false, POarray);
						}
						else{
							if (getBatch != '' && getBatch != null) {
								if(POarray["custparam_fetcheditemid"]!=null && POarray["custparam_fetcheditemid"]!='')
								{
									//case# 201410517 (Taking default values for CaptureExpirydate,CaptureFifodate)
									var CaptureExpirydate='F';
									var CaptureFifodate='F';
									var vSKUID=POarray["custparam_fetcheditemid"];
									nlapiLogExecution('DEBUG','vSKUID',vSKUID);
									var Itype = nlapiLookupField('item', vSKUID, 'recordType');
									var itemRecord = nlapiLoadRecord(Itype, vSKUID);
									var itemShelflife = itemRecord.getFieldValue('custitem_ebiz_item_shelf_life');
									CaptureExpirydate= itemRecord.getFieldValue('custitem_ebiz_item_cap_expiry_date');
									CaptureFifodate= itemRecord.getFieldValue('custitem_ebiz_item_cap_fifo_date');

									nlapiLogExecution('DEBUG','CaptureExpirydate',CaptureExpirydate);
									nlapiLogExecution('DEBUG','CaptureExpirydate',CaptureFifodate);
									nlapiLogExecution('DEBUG','itemShelflife',itemShelflife);
									POarray["custparam_batchno"] = getBatch;
									
									//Case# 201410517 starts
									if(CaptureExpirydate =='' ||CaptureExpirydate =='null' ||CaptureExpirydate ==null)
									{
										CaptureExpirydate='F';
									}
									if(CaptureFifodate =='' ||CaptureFifodate =='null' ||CaptureFifodate ==null)
									{
										CaptureFifodate='F';
									}
									//Case# 201410517 ends
									
									POarray["custparam_shelflife"] = itemShelflife;
									POarray["custparam_captureexpirydate"] = CaptureExpirydate;
									POarray["custparam_capturefifodate"] = CaptureFifodate;

									var d = new Date();
									var vExpiryDate='';
									if(itemShelflife !=null && itemShelflife!='')
									{
										//Case # 20126458 Start
										var ctx = nlapiGetContext();
										var setpreferencesdateformate = ctx.getPreference('DATEFORMAT');

										d.setDate(d.getDate()+parseInt(itemShelflife));
										nlapiLogExecution('DEBUG', 'setpreferencesdateformate',setpreferencesdateformate);
										if(setpreferencesdateformate=='DD/MM/YYYY' || setpreferencesdateformate=='DD-MM-YYYY')
										{
											vExpiryDate=((d.getDate())+"/"+(d.getMonth()+1)+"/"+(d.getFullYear()));
										}
										else
										{
											vExpiryDate=((d.getMonth()+1)+"/"+(d.getDate())+"/"+(d.getFullYear()));
										}
										//Case # 20126458 End
									}
									else
									{
										vExpiryDate='01/01/2099';										     
									}
									nlapiLogExecution('DEBUG', 'vExpiryDate',vExpiryDate);



									if(CaptureExpirydate =='T' ||(CaptureExpirydate =='T' && CaptureFifodate =='T'))
									{				
										response.sendRedirect('SUITELET', 'customscript_rf_checkin_exp_date', 'customdeploy_rf_checkin_exp_date_di', false, POarray);
									}
									else
										if(CaptureExpirydate !='T' && CaptureFifodate=='T' )
										{
											POarray["custparam_fifodate"]='';
											POarray["custparam_mfgdate"]='';
											POarray["custparam_bestbeforedate"]='';
											POarray["custparam_lastdate"]='';
											POarray["custparam_fifocode"]='';																			
											POarray["custparam_expdate"]= vExpiryDate;

											response.sendRedirect('SUITELET', 'customscript_rf_checkin_fifo_date', 'customdeploy_rf_checkin_fifo_date_di', false, POarray);
										}
										else
											if(CaptureExpirydate =='F' && CaptureFifodate =='F' )
											{

												POarray["custparam_fifodate"]=DateStamp();
												POarray["custparam_mfgdate"]='';
												POarray["custparam_bestbeforedate"]='';
												POarray["custparam_lastdate"]='';
												POarray["custparam_fifocode"]='';

												POarray["custparam_expdate"]= vExpiryDate;
												response.sendRedirect('SUITELET', 'customscript_rf_checkin_lp', 'customdeploy_rf_checkin_lp_di', false, POarray);
											}
								}
							}
							else {
								POarray["custparam_error"] = st9;
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							}
						}
					}
				} 
				else
				{
					POarray["custparam_error"] = st9;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				}
			}
		}		
		catch (e) {
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
			nlapiLogExecution('DEBUG', 'Catch: Location not found',e);

		}
	}
}

function getBatchNo(item, packCode,location, batch ){
	nlapiLogExecution('DEBUG', 'getBatchNo function');
	nlapiLogExecution('DEBUG', 'getitem',item);
	nlapiLogExecution('DEBUG', 'getpackcode',packCode);
	nlapiLogExecution('DEBUG', 'getlocation',location);
	nlapiLogExecution('DEBUG', 'getbatch',batch);

	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',[item]));
	//Case# 20149238 starts
	if(packCode!=null && packCode!='')
	filter.push(new nlobjSearchFilter('custrecord_ebizpackcode',null,'anyof',[packCode]));
	if(location!=null && location!='')
	filter.push(new nlobjSearchFilter('custrecord_ebizsitebatch',null,'anyof',[location]));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	//Case# 20149238 ends
	filter.push(new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',batch));

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebizexpirydate');
	column[1] = new nlobjSearchColumn('custrecord_ebizmfgdate');
	column[2] = new nlobjSearchColumn('custrecord_ebizbestbeforedate');
	column[3] = new nlobjSearchColumn('custrecord_ebizfifodate');
	column[4] = new nlobjSearchColumn('custrecord_ebizlastavldate');
	column[5] = new nlobjSearchColumn('custrecord_ebizfifocode');

	var searchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filter, column);

	return searchRecord;
}


function getBatchNoByMfcLot(item, packCode,location, batch ){
	nlapiLogExecution('DEBUG', 'getBatchNo function');
	nlapiLogExecution('DEBUG', 'getitem',item);
	nlapiLogExecution('DEBUG', 'getpackcode',packCode);
	nlapiLogExecution('DEBUG', 'getlocation',location);
	nlapiLogExecution('DEBUG', 'getbatch',batch);


	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebizexpirydate');
	column[1] = new nlobjSearchColumn('custrecord_ebizmfgdate');
	column[2] = new nlobjSearchColumn('custrecord_ebizbestbeforedate');
	column[3] = new nlobjSearchColumn('custrecord_ebizfifodate');
	column[4] = new nlobjSearchColumn('custrecord_ebizlastavldate');
	column[5] = new nlobjSearchColumn('custrecord_ebizfifocode');

//	check batch# with Batch Feild in Batch Entry
	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',[item]));
	filter.push(new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',batch));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var searchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filter, column);
	if(searchRecord == null || searchRecord == '')
	{
		//check batch# with ManufactureLot Feild in Batch Entry
		var filter = new Array();
		filter.push(new nlobjSearchFilter('custrecord_ebizsku',null,'anyof',[item]));
		//if(packCode!=null && packCode!='')
		//filter.push(new nlobjSearchFilter('custrecord_ebizpackcode',null,'anyof',[packCode]));
		/*if(location!=null && location!='')
	    filter.push(new nlobjSearchFilter('custrecord_ebizsitebatch',null,'anyof',[location]));*/
		//filter.push(new nlobjSearchFilter('custrecord_ebizlotbatch',null,'is',batch));
		filter.push(new nlobjSearchFilter('custrecord_ebiz_manufacturelot',null,'is',batch));	

		var searchRecord = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filter, column);
	}
	return searchRecord;
}

//case 20123285 start
function ValidateSplCharacter(string,name)
{
	var iChars = "*|,\":<>[]{}`\';()@&$#% ";
	var length=string.length;
	var flag = 'N';
	for(var i=0;i<length;i++)
	{
		if(iChars.indexOf(string.charAt(i))!=-1)
		{
			flag='Y';
			break;
		}
	}
	if(flag == 'Y')
	{
		return false;
	}
	else
	{
		return true;
	}

}
//end
