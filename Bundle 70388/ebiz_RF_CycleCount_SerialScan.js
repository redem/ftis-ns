/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************
 *
 *  $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_CycleCount_SerialScan.js,v $
 *  $Revision: 1.1.2.3.2.18.2.1 $
 *  $Date: 2015/11/16 15:34:20 $
 *  $Author: rmukkera $
 *  $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 * DESCRIPTION
 *  Functionality
 *
 * REVISION HISTORY
 *  $Log: ebiz_RF_CycleCount_SerialScan.js,v $
 *  Revision 1.1.2.3.2.18.2.1  2015/11/16 15:34:20  rmukkera
 *  case # 201415115
 *
 *  Revision 1.1.2.3.2.18  2015/07/16 15:16:58  grao
 *  2015.2   issue fixes  201413388
 *
 *  Revision 1.1.2.3.2.17  2014/09/23 15:26:08  sponnaganti
 *  Case# 20149822
 *  Stnd Bundle issue fixed
 *
 *  Revision 1.1.2.3.2.16  2014/09/11 15:40:26  sponnaganti
 *  Case# 20149822
 *  Stnd Bundle issue fixed
 *
 *  Revision 1.1.2.3.2.15  2014/06/13 10:18:21  skavuri
 *  Case# 20148882 (added Focus Functionality for Textbox)
 *
 *  Revision 1.1.2.3.2.14  2014/05/30 00:34:19  nneelam
 *  case#  20148622
 *  Stanadard Bundle Issue Fix.
 *
 *  Revision 1.1.2.3.2.13  2014/01/06 13:16:03  grao
 *  Case# 20126579 related issue fixes in Sb issue fixes
 *
 *  Revision 1.1.2.3.2.12  2013/10/11 14:12:16  rmukkera
 *  Case# 20124315�
 *
 *  Revision 1.1.2.3.2.11  2013/09/30 15:58:35  rmukkera
 *  Case#  20124567
 *
 *  Revision 1.1.2.3.2.10  2013/09/18 14:33:39  rmukkera
 *  Case# 20124315�
 *
 *  Revision 1.1.2.3.2.9  2013/09/16 15:39:43  rmukkera
 *  Case# 20124315
 *
 *  Revision 1.1.2.3.2.8  2013/07/01 15:29:09  nneelam
 *  Case# 20123245
 *  Serial# scanning at Recording through RF Issue Fix.
 *
 *  Revision 1.1.2.3.2.7  2013/05/03 01:00:41  kavitha
 *  CASE201112/CR201113/LOG201121
 *  Issue fixes
 *
 *  Revision 1.1.2.3.2.6  2013/05/02 09:21:12  gkalla
 *  CASE201112/CR201113/LOG201121
 *  As part of standard bundle. Fixed it for TSG, system is not going to focus on textbox after scanning first Serial#
 *
 *  Revision 1.1.2.3.2.5  2013/05/01 01:20:34  kavitha
 *  CASE201112/CR201113/LOG201121
 *  TSG Issue fixes
 *
 *  Revision 1.1.2.3.2.4  2013/04/30 15:57:08  skreddy
 *  CASE201112/CR201113/LOG201121
 *  Standard bundle issue fixes
 *
 *  Revision 1.1.2.3.2.3  2013/04/19 15:39:32  skreddy
 *  CASE201112/CR201113/LOG201121
 *  issue fixes
 *
 *  Revision 1.1.2.3.2.2  2013/04/17 16:02:37  skreddy
 *  CASE201112/CR201113/LOG201121
 *  added meta tag
 *
 *  Revision 1.1.2.3.2.1  2013/03/22 11:45:05  snimmakayala
 *  CASE201112/CR201113/LOG2012392
 *  Prod and UAT issue fixes.
 *
 *  Revision 1.1.2.3  2013/02/14 01:35:04  kavitha
 *  CASE201112/CR201113/LOG201121
 *  Cycle count process - Issue fixes
 *
 *  Revision 1.1.2.2  2013/02/06 03:32:58  kavitha
 *  CASE201112/CR201113/LOG201121
 *  Serial # functionality - Inventory process
 *
 *  Revision 1.1.2.1  2012/11/27 17:50:23  spendyala
 *  CASE201112/CR201113/LOG201121
 *  Serial Capturing file.
 *
 *
 ****************************************************************************/

function CycleCountSerialScan(request,response)
{
	
	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);
	
	if (request.getMethod() == 'GET') 
	{
		try
		{
			var getNumber = request.getParameter('custparam_number');
			if(getNumber==null||getNumber==""||getNumber=='NaN')
				getNumber=0;
			var getPlanNo = request.getParameter('custparam_planno');
			var getRecordId = request.getParameter('custparam_recordid');
			var getBeginLocationId = request.getParameter('custparam_begin_location_id');
			var getBeginLocationName = request.getParameter('custparam_begin_location_name');
			var getPackCode = request.getParameter('custparam_packcode');
			var getExpectedQuantity = request.getParameter('custparam_expqty');
			var getExpLPNo = request.getParameter('custparam_lpno');
			var getActLPNo = request.getParameter('custparam_actlp');
			var getActQty = request.getParameter('custparam_actqty');
			var getExpItem = request.getParameter('custparam_expitem');
			var getExpItemDescription = request.getParameter('custparam_expitemdescription');
			var getActualItem = request.getParameter('custparam_actitem');
			var getActualPackCode = request.getParameter('custparam_actitem');
			var getActualQuantity = request.getParameter('custparam_actualqty');
			//case# 20149822 starts (to ask serial numbers only for increased qty)
			nlapiLogExecution('DEBUG', 'getExpectedQuantity', getExpectedQuantity);
			nlapiLogExecution('DEBUG', 'getActualQuantity', getActualQuantity);
			var serialQuantity="";
			//if(getExpectedQuantity<getActualQuantity)
			/*if(parseInt(getExpectedQuantity)<parseInt(getActualQuantity))
				serialQuantity=parseInt(getActualQuantity)-parseInt(getExpectedQuantity);
			else*/
				serialQuantity=getActualQuantity;
			nlapiLogExecution('DEBUG', 'serialQuantity', serialQuantity);
			//case# 20149822 ends
			var getActualBatch=request.getParameter('custparam_actbatch');
			var RecordCount=request.getParameter('custparam_noofrecords');
			var NextLocation=request.getParameter('custparam_nextlocation');	
			var getActualItemStatus = request.getParameter('custparam_actitemstatus');
			nlapiLogExecution('DEBUG', 'getBeginLocation', NextLocation);
			nlapiLogExecution('DEBUG', 'RecordCount', RecordCount);
			var Confirmationflag=request.getParameter('custparam_confmsg');	
			
			var getSerialLP = request.getParameter('custparam_lpserialno');
			var planitem=request.getParameter('custparam_planitem');
			var filters = new Array();
			filters[0] = new nlobjSearchFilter('custrecord_cycle_count_plan', null, 'equalto', getPlanNo);
			filters[1] = new nlobjSearchFilter('custrecord_cyclestatus_flag', null, 'anyof', [20]);
			filters[2]=new nlobjSearchFilter('custrecord_cycact_beg_loc', null, 'anyof', getBeginLocationId);
			filters[3]=new nlobjSearchFilter('custrecord_cyclesku', null, 'anyof', getActualItem);
			var skucols=new Array();
			skucols[0]=new nlobjSearchColumn('custrecord_cycleexp_qty');
			skucols[1]=new nlobjSearchColumn('custrecord_cyclelpno');
			skucols[2]=new nlobjSearchColumn('custrecord_expcyclepc');
			skucols[3]=new nlobjSearchColumn('custrecord_expcycleabatch_no');
			skucols[4]=new nlobjSearchColumn('custrecord_cyc_skip_task').setSort(true);
			var SkuSearchResults=nlapiSearchRecord('customrecord_ebiznet_cyclecountexe', null, filters, skucols);
			if(SkuSearchResults!=null && SkuSearchResults!='' && SkuSearchResults.length>0)
			{
				nlapiLogExecution('ERROR', 'SkuSearchResults', SkuSearchResults);
				nlapiLogExecution('ERROR', 'SkuSearchResults.length', SkuSearchResults.length);
				var getActQty=0;
				var j = 0;
				var getActLP = "";
				var getLPcount = 0;
				
				nlapiLogExecution('ERROR', 'custparam_lpcount', request.getParameter('custparam_lpcount'));
				if(request.getParameter('custparam_lpcount') != null && request.getParameter('custparam_lpcount') != ""){
					j = parseInt(request.getParameter('custparam_lpcount'));
				}
				else
				{
					j = 0;
				}
				nlapiLogExecution('ERROR', 'j', j);
				
				if(j == 0)
				{
					getActLP=SkuSearchResults[j].getValue('custrecord_cyclelpno');
					getActQty=SkuSearchResults[j].getValue('custrecord_cycleexp_qty');
					
				}
				else
				{
					getActLP = request.getParameter('custparam_actlp');
					getActQty = request.getParameter('custparam_actqty');
				}					
				nlapiLogExecution('ERROR', 'getActLP', getActLP);
				nlapiLogExecution('ERROR', 'getActQty', getActQty);
				
				
				nlapiLogExecution('ERROR', 'getSerialLP', getSerialLP);
				if(getSerialLP != null)
				{
					nlapiLogExecution('ERROR', 'getSerialLP.indexOf(getActLP)', getSerialLP.indexOf(getActLP));
					if(getSerialLP.indexOf(getActLP) == -1)
					{	
						nlapiLogExecution('ERROR', 'into if', null);
						j = j + 1;
						getLPcount = j;
						nlapiLogExecution('ERROR', 'getLPcount', getLPcount);
							
						if(j < SkuSearchResults.length)
						{
							if(SkuSearchResults[j].getValue('custrecord_cycleexp_qty') != null && SkuSearchResults[j].getValue('custrecord_cycleexp_qty') != "")
							{
								getActQty=SkuSearchResults[j].getValue('custrecord_cycleexp_qty');
								nlapiLogExecution('ERROR', 'getActQty', getActQty);
							}
							if(SkuSearchResults[j].getValue('custrecord_cyclelpno') != null && SkuSearchResults[j].getValue('custrecord_cyclelpno') != "")
							{
								getActLPNo = SkuSearchResults[j].getValue('custrecord_cyclelpno');
								nlapiLogExecution('ERROR', 'getActLPNo', getActLPNo);
							}	
						}
						else
							getActLPNo = getActLP;
					}
					else
					{
						nlapiLogExecution('ERROR', 'else', null);
						
						var count = 0;
						var filterssertemp1 = new Array();
						filterssertemp1[0] = new nlobjSearchFilter('custrecord_tempserialstatus', null, 'is', 'F');
						filterssertemp1[1] = new nlobjSearchFilter('name', null, 'is', request.getParameter('custparam_recordid'));
						filterssertemp1[2] = new nlobjSearchFilter('custrecord_tempserialparentid', null, 'is', getActLP);

						var SrchRecordTmpSerial1 = nlapiSearchRecord('customrecord_ebiznettempserialentry', null, filterssertemp1,columnssertemp1);
						if(SrchRecordTmpSerial1 != null && SrchRecordTmpSerial1 != '' && SrchRecordTmpSerial1.length > 0)
						{
							count = SrchRecordTmpSerial1.length;
						}
						/*var TempSerialNoArray = new Array();
						if (getSerialLP != "") 
							TempSerialNoArray = getSerialLP.split(',');
						for (var t = 0; t <TempSerialNoArray.length; t++) {
							if (getActLP == TempSerialNoArray[t]) {
								count = count + 1;
							}
						}*/
						getLPcount = j;
						nlapiLogExecution('ERROR', 'count', count);
						nlapiLogExecution('ERROR', 'getActQty', getActQty);
						nlapiLogExecution('ERROR', 'getLPcount', getLPcount);
						
						if(count == getActQty)
						{ 
							j = j + 1;
							getLPcount = j;
							nlapiLogExecution('ERROR', 'getLPcount', getLPcount);
							
							if(j < SkuSearchResults.length)
							{
								if(SkuSearchResults[j].getValue('custrecord_cycleexp_qty') != null && SkuSearchResults[j].getValue('custrecord_cycleexp_qty') != "")
								{
									getActQty=SkuSearchResults[j].getValue('custrecord_cycleexp_qty');
									nlapiLogExecution('ERROR', 'getActQty', getActQty);
								}
								if(SkuSearchResults[j].getValue('custrecord_cyclelpno') != null && SkuSearchResults[j].getValue('custrecord_cyclelpno') != "")
									getActLPNo = SkuSearchResults[j].getValue('custrecord_cyclelpno');
								nlapiLogExecution('ERROR', 'getActLPNo', getActLPNo);
							}
							else
								getActLPNo = getActLP;
						}
					}						
				}
				else
				{
					getActLPNo = getActLP; 
				}				
			}			
			
			nlapiLogExecution('ERROR', 'getNumber', getNumber);
			if(getNumber != null && getNumber != '' && parseInt(getNumber)==0)
			{
				var filterssertemp1 = new Array();
				filterssertemp1[0] = new nlobjSearchFilter('custrecord_tempserialstatus', null, 'is', 'F');
				filterssertemp1[1] = new nlobjSearchFilter('name', null, 'is', getRecordId);
				var columnssertemp1 = new Array();
				columnssertemp1[0] = new nlobjSearchColumn('custrecord_tempserialnumber');
				columnssertemp1[1] = new nlobjSearchColumn('name');
				var SrchRecordTmpSerial1 = nlapiSearchRecord('customrecord_ebiznettempserialentry', null, filterssertemp1,columnssertemp1);
				if(SrchRecordTmpSerial1 != null && SrchRecordTmpSerial1 != '')
				{	
					var parent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); //create new parent record on fo line.
					var foparentid = nlapiSubmitRecord(parent); 
					var foparent = nlapiLoadRecord('customrecord_ebiz_throwaway_parent', foparentid);

					for (var j = 0; j < SrchRecordTmpSerial1.length; j++) {
						var TempRecord=SrchRecordTmpSerial1[j];
						foparent.selectNewLineItem('recmachcustrecord_ebiz_tempserial_child');
						nlapiLogExecution('ERROR', 'TempRecord.getId()',TempRecord.getId());
						nlapiLogExecution('ERROR', 'TempRecord.getValue(name)',TempRecord.getValue('name'));
						foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_tempserial_child', 'id', TempRecord.getId());
						foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_tempserial_child','name', TempRecord.getValue('name'));
						foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_tempserial_child','custrecord_ebiz_tempserial_note1', 'because of discontinue of serial number scanning we have marked this serial number as closed');
						foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_tempserial_child','custrecord_tempserialstatus', 'T');

						foparent.commitLineItem('recmachcustrecord_ebiz_tempserial_child');
					}
					nlapiSubmitRecord(foparent);
				}	
			}
			
			var siteid = "";
			var fields = ['custrecord_cyclesite_id'];
			var columns = nlapiLookupField('customrecord_ebiznet_cyclecountexe', request.getParameter('custparam_recordid'), fields);
			siteid = columns.custrecord_cyclesite_id;
			if(getActLPNo==null || getActLPNo=='')
			{
				getActLPNo = GetMaxLPNo(1, 1,siteid);
			}
			
			var functionkeyHtml=getFunctionkeyScript('_rf_cyclecountserialscan'); 
			var html = "<html><head><title>CYCLE COUNT</title>";
			html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
			html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
			//Case# 20148749 Refresh Functionality starts
			html = html + "var version = navigator.appVersion;";
			html = html + "document.onkeydown = function (e) {";
			html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
			html = html + "if ((version.indexOf('MSIE') != -1)) { ";
			html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
			html = html + "else {if (keycode == 116)return false;}";
			html = html + "};";
			//Case# 20148749 Refresh Functionality ends
			html = html + "nextPage = new String(history.forward());";          
			html = html + "if (nextPage == 'undefined')";     
			html = html + "{}";     
			html = html + "else";     
			html = html + "{  location.href = window.history.forward();"; 
			html = html + "} ";
			
			//html = html + " document.getElementById('enterserialno').focus();";        
			html = html + "</script>";
			html = html +functionkeyHtml;
			html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
			html = html + "	<form name='_rf_cyclecountserialscan' method='POST'>";
			html = html + "		<table>";
			html = html + "				<tr><td><input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
			html = html + "				<input type='hidden' name='hdnCycleCountPlanNo' value=" + getPlanNo + ">";
			html = html + "				<input type='hidden' name='hdnBeginLocationName' value=" + getBeginLocationName + ">";
			html = html + "				<input type='hidden' name='hdnRecordId' value=" + getRecordId + ">";
			html = html + "				<input type='hidden' name='hdnPackCode' value=" + getPackCode + ">";
			html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
			html = html + "				<input type='hidden' name='hdnExpLPNo' value=" + request.getParameter('custparam_lpno') + "></td>";
			html = html + "				<input type='hidden' name='hdnActLPNo' value=" + request.getParameter('custparam_actlpno') + "></td>";
			html = html + "				<input type='hidden' name='hdnExpectedItemDescription' value=" + getExpItemDescription + ">";
			html = html + "				<input type='hidden' name='hdnActualItem' value=" + getActualItem + ">";
			html = html + "				<input type='hidden' name='hdnActualPackCode' value=" + getActualPackCode + ">";
			html = html + "				<input type='hidden' name='hdnActualQuantity' value=" + getActualQuantity + ">";
			html = html + "				<input type='hidden' name='hdnSerialQuantity' value=" + serialQuantity + ">";//case# 20149822
			html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
			html = html + "				<input type='hidden' name='hdnnext' value=" + NextLocation + ">";
			html = html + "				<input type='hidden' name='hdnNumber' value=" + getNumber + ">";
			html = html + "				<input type='hidden' name='hdnActItemStatus' value=" + getActualItemStatus + ">";
			html = html + "				<input type='hidden' name='hdnActualLP' value=" + getActLPNo + ">";
			html = html + "				<input type='hidden' name='hdnActualQty' value=" + getActQty + ">";
			html = html + "				<input type='hidden' name='hdnSerialLP' value=" + getSerialLP + ">";
			html = html + "				<input type='hidden' name='hdnLPCount' value=" + getLPcount + ">";
			html = html + "				<input type='hidden' name='hdnplanitem' value='" + planitem + "'>";	
			html = html + "				<input type='hidden' name='hdnflag'>";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			//case# 20149822 starts
			//html = html + "				<td align = 'left'>" + (parseInt(getNumber) + 1) + " OF <label>" + getActualQuantity + "</label>";
			html = html + "				<td align = 'left'>" + (parseInt(getNumber) + 1) + " OF <label>" + serialQuantity + "</label>";
			//case# 20149822 ends
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>ENTER SERIAL NO: ";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'><input name='enterserialno' id='enterserialno' type='text'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>PARENT ID : <label>" + getActLPNo + "</label>";
			html = html + "			</tr>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>SEND <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
			html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "		 </table>";
			html = html + "	</form>";
			//Case# 20148882 (added Focus Functionality for Textbox)
			html = html + "<script type='text/javascript'>document.getElementById('enterserialno').focus();</script>";
			html = html + "</body>";
			html = html + "</html>";

			response.write(html);

		}
		catch(exp)
		{
			nlapiLogExecution('ERROR','Exception in Get',exp);
		}
	}
	else
	{
		try
		{
			var CCarray= new Array();
			var optedEvent = request.getParameter('cmdPrevious');
			var getSerialNo = request.getParameter('enterserialno');
			var getNumber = request.getParameter('hdnNumber');
			var getItemQuantity = request.getParameter('hdnActualQuantity');
			var getSerialQuantity = request.getParameter('hdnSerialQuantity');
			nlapiLogExecution('ERROR','getSerialQuantity',getSerialQuantity);
			CCarray["custparam_error"] = 'INVALID SERIAL#';
			CCarray["custparam_screenno"] = '33B';
			CCarray["custparam_recordid"] = request.getParameter('custparam_recordid');
			CCarray["custparam_planno"] = request.getParameter('custparam_planno');
			CCarray["custparam_begin_location_id"] =request.getParameter('custparam_begin_location_id');
			CCarray["custparam_begin_location_name"] = request.getParameter('custparam_begin_location_name');
			CCarray["custparam_lpno"] = request.getParameter('custparam_lpno');
			CCarray["custparam_packcode"] = request.getParameter('custparam_packcode');
			CCarray["custparam_expqty"] = request.getParameter('custparam_expqty');
			CCarray["custparam_actlp"] = request.getParameter('hdnActualLP');
			CCarray["custparam_actqty"] = request.getParameter('hdnActualQty');
			CCarray["custparam_expitem"] = request.getParameter('custparam_expitem');
			CCarray["custparam_expitemdescription"] = request.getParameter('custparam_expitemdescription');
			CCarray["custparam_actitem"] = request.getParameter('custparam_actitem');
			CCarray["custparam_actpackcode"] = request.getParameter('custparam_actpackcode');
			CCarray["custparam_expiteminternalid"] = request.getParameter('hdniteminternalid');
			CCarray["custparam_noofrecords"] = request.getParameter('hdnRecCount');
			CCarray["custparam_nextlocation"] = request.getParameter('hdnnext');
			CCarray["custparam_skustatus"] = request.getParameter('custparam_skustatus');
			CCarray["custparam_actbatch"] = request.getParameter('custparam_actbatch');
			CCarray["custparam_expqty"] = request.getParameter('hdnExpectedQuantity');
			CCarray["custparam_actualqty"] = request.getParameter('hdnActualQuantity');
			CCarray["custparam_number"] = parseInt(request.getParameter('custparam_number'));
			CCarray["custparam_confmsg"] = request.getParameter('custparam_confmsg');
			CCarray["custparam_actitemstatus"] = request.getParameter('hdnActItemStatus');
			CCarray["custparam_lpcount"] = request.getParameter('hdnLPCount');
			CCarray["custparam_planitem"] = request.getParameter('hdnplanitem');
			//Case # 20123245 start
			if(request.getParameter('custparam_serialno')!=null && request.getParameter('custparam_serialno')!='')
				CCarray["custparam_serialno"]=request.getParameter('custparam_serialno');
			//Case # 20123245 end
			
			var siteid = "";
			var fields = ['custrecord_cyclesite_id'];
			var columns = nlapiLookupField('customrecord_ebiznet_cyclecountexe', request.getParameter('custparam_recordid'), fields);
			siteid = columns.custrecord_cyclesite_id;
			if(request.getParameter('hdnActualLP')==null || request.getParameter('hdnActualLP')=='')
			{
			CCarray["custparam_actlp"] = GetMaxLPNo(1, 1,siteid);
			}


			if (sessionobj!=context.getUser()) {
				try 
				{
					if(sessionobj==null || sessionobj=='')
					{
						sessionobj=context.getUser();
						context.setSessionObject('session', sessionobj); 
					}
					if (optedEvent == 'F7') {
						nlapiLogExecution('ERROR', 'REPLENISHMENT LOCATION F7 Pressed');
						response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_quantity', 'customdeploy_rf_cyclecount_quantity_di', false, CCarray);
					}
					else if (getSerialNo == "") {
						//	if the 'Send' button is clicked without any option value entered,
						//  it has to show an error message. The next screen to which it has to navigate is to the error screen.
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
						nlapiLogExecution('ERROR', 'Enter Serial No', getSerialNo);
						CCarray["custparam_error"] = 'ENTER SERIAL NO';
					}
					else 
					{
						/*if actual qty entered is greater than the expected qty then
						 *we need to validate the serial#,if the given serial# exist against the item,wms status and pallet
						 *then we will update the given serial no with the exp lp(OR)
						 *If scanned serial# is not exist then will create a new serial no agianst the pallet and item with 
						 *wms flag storage.
						 */

						if(CCarray["custparam_expqty"] == null || CCarray["custparam_expqty"] == '')
						{
							CCarray["custparam_expqty"] = 0;
						}
						nlapiLogExecution('ERROR', 'Actual qty', parseInt(CCarray["custparam_actualqty"]));
						nlapiLogExecution('ERROR', 'Expected qty', parseInt(CCarray["custparam_expqty"]));
						if(parseInt(CCarray["custparam_actualqty"])>parseInt(CCarray["custparam_expqty"]))
						{
							nlapiLogExecution('ERROR', 'Into act qty greater than exp qty', CCarray["custparam_actualqty"])+'>'+parseInt(CCarray["custparam_expqty"]);
							getSerialNo=SerialNoIdentification(CCarray["custparam_actitem"],getSerialNo);
							nlapiLogExecution('ERROR', 'After Serial No Parsing', getSerialNo);
							var getActualEndDate = DateStamp();
							var getActualEndTime = TimeStamp();
							var TempSerialNoArray = new Array();
							var TempSerialNoIDArray =new Array();
							if (request.getParameter('custparam_serialno') != null) 
								TempSerialNoArray = request.getParameter('custparam_serialno').split(',');

//					if (getBeginLocation != '') 
//					{
					nlapiLogExecution('ERROR', 'INTO SERIAL ENTRY');
					//checking serial no's in already scanned one's
					for (var t = 0; t < TempSerialNoArray.length; t++) {
						if (getSerialNo == TempSerialNoArray[t]) {
							CCarray["custparam_error"] = "Serial No. Already Scanned";
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
							return;
						}
					}

					nlapiLogExecution('ERROR', 'INTO SERIAL ENTRY123');
					try{
						var serialfiltersser = new Array();					

						serialfiltersser.push( new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', CCarray["custparam_actitem"] ));
						serialfiltersser.push( new nlobjSearchFilter('custrecord_serialparentid', null, 'is', CCarray["custparam_actlp"]));				
						serialfiltersser.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['3','19']));
						serialfiltersser.push(new nlobjSearchFilter('custrecord_serialstatus', null, 'isnot', 'C'));

						var serialcols=new Array();
						serialcols.push(new nlobjSearchColumn('custrecord_serialnumber'));
						serialcols.push(new nlobjSearchColumn('internalid'));

						var	SerialSrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, serialfiltersser,serialcols);

						if(SerialSrchRecord!=null && SerialSrchRecord!='')
						{

							for(var i2 = 0;i2<SerialSrchRecord.length;i2++)
							{

								var serId = SerialSrchRecord[i2].getId();
								nlapiLogExecution('ERROR','serId',serId);

								var id = nlapiDeleteRecord('customrecord_ebiznetserialentry', serId);
								nlapiLogExecution('ERROR','success1','success');
							}


						}

					}
					catch(e2)
					{
						nlapiLogExecution('ERROR','exp2',e2);
					}				
					
					
					
					
					
					
					
					//checking serial no's in records
					nlapiLogExecution('ERROR','getserialno',getSerialNo);
					nlapiLogExecution('ERROR','CCarray[custparam_actitem]',CCarray["custparam_actitem"]);
					nlapiLogExecution('ERROR','CCarray[custparam_actlp]',CCarray["custparam_actlp"]);
					var filtersser = new Array();
					//filtersser.push( new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getSerialNo));
					if(CCarray["custparam_actitem"]!=null && CCarray["custparam_actitem"]!='' && CCarray["custparam_actitem"]!='null')
					{
						filtersser.push( new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', CCarray["custparam_actitem"] ));

						filtersser.push( new nlobjSearchFilter('custrecord_serialparentid', null, 'is', CCarray["custparam_actlp"]));

					}
					else
					{
						filtersser.push(new nlobjSearchFilter('custrecord_serialparentid', null, 'is', CCarray["custparam_actlp"]));
					}
					filtersser.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['3','19']));
					var cols=new Array();
					cols.push(new nlobjSearchColumn('custrecord_serialnumber'));

					filtersser.push( new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getSerialNo));
					//filtersser.push( new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getSerialNo));
					var	SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser,cols);

					if (SrchRecord ==null || SrchRecord =="") 
					{					



						nlapiLogExecution('ERROR','Into Create Serial Record');
						var customrecord = nlapiCreateRecord('customrecord_ebiznetserialentry');
						customrecord.setFieldValue('custrecord_serialitem', CCarray["custparam_actitem"]);
						customrecord.setFieldValue('custrecord_serialiteminternalid', CCarray["custparam_actitem"]);
						customrecord.setFieldValue('custrecord_serialparentid', CCarray["custparam_actlp"]);
						customrecord.setFieldValue('custrecord_serialqty', "1");
						customrecord.setFieldValue('custrecord_serialnumber', getSerialNo);
						customrecord.setFieldValue('custrecord_serialuomid', "1");
						customrecord.setFieldValue('custrecord_serialwmsstatus', '19');
						customrecord.setFieldValue('custrecord_serial_location', siteid);
						customrecord.setFieldValue('custrecord_serialstatus', 'C');						
						var rec = nlapiSubmitRecord(customrecord, false, true);
						nlapiLogExecution('ERROR','Recorde created',rec);


						//Here we are creating serial# into new Temp Serial entry custom record

						var customrecord = nlapiCreateRecord('customrecord_ebiznettempserialentry');
						customrecord.setFieldValue('name', request.getParameter('custparam_recordid'));
						customrecord.setFieldValue('custrecord_tempserialitem', CCarray["custparam_actitem"]);
						customrecord.setFieldValue('custrecord_tempserialparentid', CCarray["custparam_actlp"]);
						customrecord.setFieldValue('custrecord_tempserialnumber', getSerialNo);				 
						customrecord.setFieldValue('custrecord_tempserialstatus', 'F');
						var rec = nlapiSubmitRecord(customrecord, false, true);

						//Record created into temp serial entry

						//nlapiLogExecution('ERROR','custparam_lpserialnoarray',CCarray["custparam_lpserialnoarray"]);
						var vActLP = CCarray["custparam_actlp"];
						if (request.getParameter('custparam_lpserialno') == null || request.getParameter('custparam_lpserialno') == "") {

							//Case # 20123245 start
							CCarray["custparam_serialno"] = getSerialNo;
							//Case # 20123245 end

							//CCarray["custparam_serialnoID"] = serrialid;							
							CCarray["custparam_lpserialno"] = vActLP;

							//CCarray["custparam_lpserialnoarray"] = CCarray["custparam_actlp"] +"-"+getSerialNo;
						}
						else {

							//Case # 20123245 start
							CCarray["custparam_serialno"] = request.getParameter('custparam_serialno') + ',' + getSerialNo;
							//Case # 20123245 end

							//CCarray["custparam_serialnoID"] = request.getParameter('custparam_serialnoID') + ',' + serrialid;
							if(request.getParameter('custparam_lpserialno').indexOf(vActLP) == -1)
								CCarray["custparam_lpserialno"] = request.getParameter('custparam_lpserialno') + ',' + vActLP;
							else
								CCarray["custparam_lpserialno"] = request.getParameter('custparam_lpserialno');
							//CCarray["custparam_lpserialnoarray"] = request.getParameter('custparam_lpserialnoarray') + ',' + CCarray["custparam_actlp"] +"-"+getSerialNo;
						}
					}
					else 
					{
						for ( var i = 0; SrchRecord!= null && i < SrchRecord.length; i++) 
						{
							var searchresultserial;
							var serrialid;
							var serial=SrchRecord[i].getValue('custrecord_serialnumber');
							if(serial==getSerialNo)
							{
								searchresultserial = SrchRecord[i];
								serrialid=searchresultserial.getId();
								break;
							}
						}
						nlapiLogExecution('ERROR', 'serrialid', serrialid);

						var vActLP = CCarray["custparam_actlp"];
						if (request.getParameter('custparam_lpserialno') == null || request.getParameter('custparam_lpserialno') == "") {

							//Case # 20123245 start
							CCarray["custparam_serialno"] = getSerialNo;
							//Case # 20123245 end

							//CCarray["custparam_serialnoID"] = serrialid;							
							CCarray["custparam_lpserialno"] = vActLP;

							//CCarray["custparam_lpserialnoarray"] = CCarray["custparam_actlp"] +"-"+getSerialNo;
						}
						else {

							//Case # 20123245 start
							CCarray["custparam_serialno"] = request.getParameter('custparam_serialno') + ',' + getSerialNo;
							//Case # 20123245 end

							//CCarray["custparam_serialnoID"] = request.getParameter('custparam_serialnoID') + ',' + serrialid;
							if(request.getParameter('custparam_lpserialno').indexOf(vActLP) == -1)
								CCarray["custparam_lpserialno"] = request.getParameter('custparam_lpserialno') + ',' + vActLP;
							else
								CCarray["custparam_lpserialno"] = request.getParameter('custparam_lpserialno');
							//CCarray["custparam_lpserialnoarray"] = request.getParameter('custparam_lpserialnoarray') + ',' + CCarray["custparam_actlp"] +"-"+getSerialNo;
						}
						//nlapiLogExecution('ERROR', 'CCarray["custparam_lpserialnoarray"]', CCarray["custparam_lpserialnoarray"]);

						//Here we are creating serial# into new Temp Serial entry custom record

						var customrecord = nlapiCreateRecord('customrecord_ebiznettempserialentry');
						customrecord.setFieldValue('name', request.getParameter('custparam_recordid'));
						customrecord.setFieldValue('custrecord_tempserialitem', CCarray["custparam_actitem"]);
						customrecord.setFieldValue('custrecord_tempserialparentid', CCarray["custparam_actlp"]);
						customrecord.setFieldValue('custrecord_tempserialnumber', getSerialNo);	
						customrecord.setFieldValue('custrecord_tempserial_lpno', serrialid);	
						customrecord.setFieldValue('custrecord_tempserialstatus', 'F');
						var rec = nlapiSubmitRecord(customrecord, false, true);

						//Record created into temp serial entry

						TempSerialNoArray.push(getSerialNo);
					}
					CCarray["custparam_number"] = parseInt(getNumber) + 1;
					nlapiLogExecution('ERROR', '(getNumber + 1)', (parseInt(getNumber) + 1));
					nlapiLogExecution('ERROR', 'getItemQuantity', getItemQuantity);
					//if ((parseInt(getNumber) + 1) < parseInt(getItemQuantity)) {//case# 20149822
					nlapiLogExecution('ERROR', 'getSerialQuantity', getSerialQuantity);
					if ((parseInt(getNumber) + 1) < parseInt(getSerialQuantity)) {
						nlapiLogExecution('ERROR', 'Scanning Serial No.');
						response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_serialscan', 'customdeploy_rf_cyclecount_serialscan_di', false, CCarray);
						return;
					}
					else {
						nlapiLogExecution('ERROR', 'Inserting Into Records',CCarray["custparam_serialnoID"]);
						/*if(CCarray["custparam_serialnoID"] != null && CCarray["custparam_serialnoID"] != "")
						{
							TempSerialNoIDArray=CCarray["custparam_serialnoID"].split(',');
							for (var j = 0; j < TempSerialNoIDArray.length; j++) {
								nlapiLogExecution('ERROR', 'Inserting Serial NO.:', TempSerialNoIDArray[j]);
	//							if (SrchRecord!=null && SrchRecord!="") 
	//							{
								var vserialid =  TempSerialNoIDArray[j];
								nlapiLogExecution('ERROR', 'Serial ID Internal Id :',vserialid);

								nlapiLogExecution('ERROR', "Item" , CCarray["custparam_repskuno"]);

								var fields = new Array();
								var values = new Array();
								fields[0] = 'custrecord_serialwmsstatus';						
								//fields[1] = 'custrecord_serialparentid';
								fields[1] = 'custrecord_serialitem';
								fields[2] = 'custrecord_serialstatus';
								values[0] = '19';//FLAG.INVENTORY.STORAGE
								//values[1] = CCarray["custparam_actlp"];
								values[1] = CCarray["custparam_actitem"];
								values[2] = 'C';

								var updatefields = nlapiSubmitField('customrecord_ebiznetserialentry',vserialid, fields, values);
								nlapiLogExecution('ERROR', 'Record Updated :');
	//							}
							}
						}*/

						var filterssertemp1 = new Array();
						filterssertemp1[0] = new nlobjSearchFilter('custrecord_tempserialstatus', null, 'is', 'F');
						filterssertemp1[1] = new nlobjSearchFilter('name', null, 'is', request.getParameter('custparam_recordid'));
						filterssertemp1[2] = new nlobjSearchFilter('custrecord_tempserial_lpno', null, 'isnotempty');
						var columnssertemp1 = new Array();
						columnssertemp1[0] = new nlobjSearchColumn('custrecord_tempserialnumber');
						columnssertemp1[1] = new nlobjSearchColumn('name');
						columnssertemp1[2] = new nlobjSearchColumn('custrecord_tempserial_lpno');
						var SrchRecordTmpSerial1 = nlapiSearchRecord('customrecord_ebiznettempserialentry', null, filterssertemp1,columnssertemp1);
						if(SrchRecordTmpSerial1 != null && SrchRecordTmpSerial1 != '')
						{	
							var parent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); //create new parent record on fo line.
							var foparentid = nlapiSubmitRecord(parent); 
							var foparent = nlapiLoadRecord('customrecord_ebiz_throwaway_parent', foparentid);

							for (var j = 0; j < SrchRecordTmpSerial1.length; j++) {
								var vInsSerialNo=SrchRecordTmpSerial1[j].getValue('custrecord_tempserialnumber');
								var serrialid=SrchRecordTmpSerial1[j].getValue('custrecord_tempserial_lpno');
								nlapiLogExecution('ERROR', 'Updating Serial NO.:', vInsSerialNo + "/" + serrialid);

								var fieldser = new Array();
								var valueser = new Array();
								fieldser[0] = 'custrecord_serialwmsstatus';						
								//fields[1] = 'custrecord_serialparentid';
								fieldser[1] = 'custrecord_serialitem';
								fieldser[2] = 'custrecord_serialstatus';
								valueser[0] = '19';//FLAG.INVENTORY.STORAGE
								//values[1] = CCarray["custparam_actlp"];
								valueser[1] = CCarray["custparam_actitem"];
								valueser[2] = 'C';
								var updatefields = nlapiSubmitField('customrecord_ebiznetserialentry',serrialid, fieldser, valueser);
								nlapiLogExecution('ERROR', 'Record Updated :');								

								var TempRecord=SrchRecordTmpSerial1[j];
								foparent.selectNewLineItem('recmachcustrecord_ebiz_tempserial_child');
								nlapiLogExecution('ERROR', 'TempRecord.getId()',TempRecord.getId());
								nlapiLogExecution('ERROR', 'TempRecord.getValue(name)',TempRecord.getValue('name'));
								foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_tempserial_child', 'id', TempRecord.getId());
								foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_tempserial_child','name', TempRecord.getValue('name'));

								foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_tempserial_child','custrecord_tempserialstatus', 'T');

								foparent.commitLineItem('recmachcustrecord_ebiz_tempserial_child');
							}
							nlapiSubmitRecord(foparent);
						}
					}
					response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_confirm', 'customdeploy1', false, CCarray);
				}
				else
				{
					nlapiLogExecution('ERROR','getserialno',getSerialNo);
					nlapiLogExecution('ERROR','CCarray[custparam_actitem]',CCarray["custparam_actitem"]);
					nlapiLogExecution('ERROR','CCarray[custparam_actlp]',CCarray["custparam_actlp"]);

					getSerialNo=SerialNoIdentification(CCarray["custparam_actitem"],getSerialNo);
					nlapiLogExecution('ERROR', 'After Serial No Parsing', getSerialNo);
					var getActualEndDate = DateStamp();
					var getActualEndTime = TimeStamp();
					var TempSerialNoArray = new Array();
					var TempSerialNoIDArray =new Array();
					if (request.getParameter('custparam_serialno') != null) 
						TempSerialNoArray = request.getParameter('custparam_serialno').split(',');

//					if (getBeginLocation != '') 
//					{
					nlapiLogExecution('ERROR', 'INTO SERIAL ENTRY');
					//checking serial no's in already scanned one's
					for (var t = 0; t < TempSerialNoArray.length; t++) {
						if (getSerialNo == TempSerialNoArray[t]) {
							CCarray["custparam_error"] = "Serial No. Already Scanned";
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
							return;
						}
					}
					
					
					
					var serialfiltersser1 = new Array();					
					
					serialfiltersser1.push( new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', CCarray["custparam_actitem"] ));
					serialfiltersser1.push( new nlobjSearchFilter('custrecord_serialparentid', null, 'is', CCarray["custparam_actlp"]));				
					serialfiltersser1.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['3','19']));
					serialfiltersser1.push(new nlobjSearchFilter('custrecord_serialstatus', null, 'isnot', 'C'));
					
					var serialcols1=new Array();
					serialcols1.push(new nlobjSearchColumn('custrecord_serialnumber'));
					serialcols1.push(new nlobjSearchColumn('internalid'));
					
					var	SerialSrchRecord1 = nlapiSearchRecord('customrecord_ebiznetserialentry', null, serialfiltersser1,serialcols1);
					
					if(SerialSrchRecord1!=null && SerialSrchRecord1!='')
					{
						try
						{
							for(var i3 = 0;i3<SerialSrchRecord1.length;i3++)
							{

								var serId = SerialSrchRecord1[i3].getId();
								nlapiLogExecution('ERROR','serId',serId);

								var id = nlapiDeleteRecord('customrecord_ebiznetserialentry', serId);
								nlapiLogExecution('ERROR','success1','success');
							}
						}
						catch(e2)
						{
							nlapiLogExecution('ERROR','exp2',e2);
						}
						
					}

					//checking serial no's in records
					var filtersser = new Array();
					//filtersser.push( new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getSerialNo));
					if(CCarray["custparam_actitem"]!=null && CCarray["custparam_actitem"]!='' && CCarray["custparam_actitem"]!='null')
					{
						filtersser.push( new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', CCarray["custparam_actitem"] ));

						filtersser.push( new nlobjSearchFilter('custrecord_serialparentid', null, 'is', CCarray["custparam_actlp"]));

					}
					else
					{
						filtersser.push(new nlobjSearchFilter('custrecord_serialparentid', null, 'is', CCarray["custparam_actlp"]));
					}
					filtersser.push(new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['3','19']));
					filtersser.push( new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getSerialNo));
					var cols=new Array();
					cols.push(new nlobjSearchColumn('custrecord_serialnumber'));

					var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser,cols);
					

					
					if (SrchRecord ==null || SrchRecord=="") {
						//CCarray["custparam_serialnoID"] = serrialid;
						//CCarray["custparam_error"] = "Invalid Serial#";
						//response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
						//return;

						nlapiLogExecution('ERROR','Into Create Serial Record');
						var customrecord = nlapiCreateRecord('customrecord_ebiznetserialentry');
						customrecord.setFieldValue('custrecord_serialitem', CCarray["custparam_actitem"]);
						customrecord.setFieldValue('custrecord_serialiteminternalid', CCarray["custparam_actitem"]);
						customrecord.setFieldValue('custrecord_serialparentid', CCarray["custparam_actlp"]);
						customrecord.setFieldValue('custrecord_serialqty', "1");
						customrecord.setFieldValue('custrecord_serialnumber', getSerialNo);
						customrecord.setFieldValue('custrecord_serialuomid', "1");
						customrecord.setFieldValue('custrecord_serialwmsstatus', '19');
						customrecord.setFieldValue('custrecord_serial_location', siteid);
						customrecord.setFieldValue('custrecord_serialstatus', 'C');
						var rec = nlapiSubmitRecord(customrecord, false, true);
						nlapiLogExecution('ERROR','Recorde created',rec);




						//Here we are creating serial# into new Temp Serial entry custom record

						var customrecord = nlapiCreateRecord('customrecord_ebiznettempserialentry');
						customrecord.setFieldValue('name', request.getParameter('custparam_recordid'));
						customrecord.setFieldValue('custrecord_tempserialitem', CCarray["custparam_actitem"]);
						customrecord.setFieldValue('custrecord_tempserialparentid', CCarray["custparam_actlp"]);
						customrecord.setFieldValue('custrecord_tempserialnumber', getSerialNo);
						customrecord.setFieldValue('custrecord_tempserialstatus', 'F');
						var rec = nlapiSubmitRecord(customrecord, false, true);

						//Record created into temp serial entry

						var vActLP = CCarray["custparam_actlp"];
						if (request.getParameter('custparam_lpserialno') == null || request.getParameter('custparam_lpserialno') == "") {

							//Case # 20123245 start
							CCarray["custparam_serialno"] = getSerialNo;
							//Case # 20123245 end

							//CCarray["custparam_serialnoID"] = serrialid;							
							CCarray["custparam_lpserialno"] = vActLP;

							//CCarray["custparam_lpserialnoarray"] = CCarray["custparam_actlp"] +"-"+getSerialNo;
						}
						else {


							//Case # 20123245 start
							CCarray["custparam_serialno"] = request.getParameter('custparam_serialno') + ',' + getSerialNo;
							//Case # 20123245 end

							//CCarray["custparam_serialnoID"] = request.getParameter('custparam_serialnoID') + ',' + serrialid;
							if(request.getParameter('custparam_lpserialno').indexOf(vActLP) == -1)
								CCarray["custparam_lpserialno"] = request.getParameter('custparam_lpserialno') + ',' + vActLP;
							else
								CCarray["custparam_lpserialno"] = request.getParameter('custparam_lpserialno');
							//CCarray["custparam_lpserialnoarray"] = request.getParameter('custparam_lpserialnoarray') + ',' + CCarray["custparam_actlp"] +"-"+getSerialNo;
						}
					}
					else 
					{
						for ( var i = 0; SrchRecord!= null && i < SrchRecord.length; i++) 
						{
							var searchresultserial = SrchRecord[i];
							var serrialid=searchresultserial.getId();
						}
						nlapiLogExecution('ERROR', 'serrialid', serrialid);

						var vActLP = CCarray["custparam_actlp"];
						if (request.getParameter('custparam_lpserialno') == null || request.getParameter('custparam_lpserialno') == "") {

							//Case # 20123245 start
							CCarray["custparam_serialno"] = getSerialNo;
							//Case # 20123245 end

							//CCarray["custparam_serialnoID"] = serrialid;
							CCarray["custparam_lpserialno"] = vActLP;

							//CCarray["custparam_lpserialnoarray"] = CCarray["custparam_actlp"] +"-"+getSerialNo;
						}
						else {

							//Case # 20123245 start
							CCarray["custparam_serialno"] = request.getParameter('custparam_serialno') + ',' + getSerialNo;
							//Case # 20123245 end

							//CCarray["custparam_serialnoID"] = request.getParameter('custparam_serialnoID') + ',' + serrialid;
							if(request.getParameter('custparam_lpserialno').indexOf(vActLP) == -1)
								CCarray["custparam_lpserialno"] = request.getParameter('custparam_lpserialno') + ',' + vActLP;
							else
								CCarray["custparam_lpserialno"] = request.getParameter('custparam_lpserialno');
							//CCarray["custparam_lpserialnoarray"] = request.getParameter('custparam_lpserialnoarray') + ',' + CCarray["custparam_actlp"] +"-"+getSerialNo;
						}	

						//Here we are creating serial# into new Temp Serial entry custom record

						var customrecord = nlapiCreateRecord('customrecord_ebiznettempserialentry');
						customrecord.setFieldValue('name', request.getParameter('custparam_recordid'));
						customrecord.setFieldValue('custrecord_tempserialitem', CCarray["custparam_actitem"]);
						customrecord.setFieldValue('custrecord_tempserialparentid', CCarray["custparam_actlp"]);
						customrecord.setFieldValue('custrecord_tempserialnumber', getSerialNo);
						customrecord.setFieldValue('custrecord_tempserial_lpno', serrialid);	
						customrecord.setFieldValue('custrecord_tempserialstatus', 'F');
						var rec = nlapiSubmitRecord(customrecord, false, true);

						//Record created into temp serial entry

					}
					CCarray["custparam_number"] = parseInt(getNumber) + 1;
					TempSerialNoArray.push(getSerialNo);
					nlapiLogExecution('ERROR', '(getNumber + 1)', (parseInt(getNumber) + 1));
					nlapiLogExecution('ERROR', 'getItemQuantity', getItemQuantity);
					//if ((parseInt(getNumber) + 1) < parseInt(getItemQuantity)) {//case# 20149822
					nlapiLogExecution('ERROR', 'getSerialQuantity', getSerialQuantity);
					if ((parseInt(getNumber) + 1) < parseInt(getSerialQuantity)) {
						nlapiLogExecution('ERROR', 'Scanning Serial No.');
						response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_serialscan', 'customdeploy_rf_cyclecount_serialscan_di', false, CCarray);
						return;
					}
					else 
					{
						nlapiLogExecution('ERROR', 'Inserting Into Records',CCarray["custparam_serialnoID"]);
						/*if(CCarray["custparam_serialnoID"] != null && CCarray["custparam_serialnoID"] != "")
						{
							TempSerialNoIDArray=CCarray["custparam_serialnoID"].split(',');
							for (var j = 0; j < TempSerialNoIDArray.length; j++) {
								nlapiLogExecution('ERROR', 'Inserting Serial NO.:', TempSerialNoIDArray[j]);
	//							if (SrchRecord!=null && SrchRecord!="") 
	//							{
								var vserialid =  TempSerialNoIDArray[j];
								nlapiLogExecution('ERROR', 'Serial ID Internal Id :',vserialid);

								nlapiLogExecution('ERROR', "Item" , CCarray["custparam_repskuno"]);

								var fields = new Array();
								var values = new Array();
								fields[0] = 'custrecord_serialwmsstatus';						
								//fields[1] = 'custrecord_serialparentid';
								fields[1] = 'custrecord_serialitem';
								fields[2] = 'custrecord_serialstatus';
								values[0] = '19';//FLAG.INVENTORY.STORAGE
								//values[1] = CCarray["custparam_actlp"];
								values[1] = CCarray["custparam_actitem"];
								values[2] = 'C';

								var updatefields = nlapiSubmitField('customrecord_ebiznetserialentry',vserialid, fields, values);
								nlapiLogExecution('ERROR', 'Record Updated :');
	//							}
							}
						}*/

						var filterssertemp1 = new Array();
						filterssertemp1[0] = new nlobjSearchFilter('custrecord_tempserialstatus', null, 'is', 'F');
						filterssertemp1[1] = new nlobjSearchFilter('name', null, 'is', request.getParameter('custparam_recordid'));
						filterssertemp1[2] = new nlobjSearchFilter('custrecord_tempserial_lpno', null, 'isnotempty');
						var columnssertemp1 = new Array();
						columnssertemp1[0] = new nlobjSearchColumn('custrecord_tempserialnumber');
						columnssertemp1[1] = new nlobjSearchColumn('name');
						columnssertemp1[2] = new nlobjSearchColumn('custrecord_tempserial_lpno');
						var SrchRecordTmpSerial1 = nlapiSearchRecord('customrecord_ebiznettempserialentry', null, filterssertemp1,columnssertemp1);
						if(SrchRecordTmpSerial1 != null && SrchRecordTmpSerial1 != '')
						{	
							var parent = nlapiCreateRecord('customrecord_ebiz_throwaway_parent'); //create new parent record on fo line.
							var foparentid = nlapiSubmitRecord(parent); 
							var foparent = nlapiLoadRecord('customrecord_ebiz_throwaway_parent', foparentid);

							for (var j = 0; j < SrchRecordTmpSerial1.length; j++) {
								var vInsSerialNo=SrchRecordTmpSerial1[j].getValue('custrecord_tempserialnumber');
								var serrialid=SrchRecordTmpSerial1[j].getValue('custrecord_tempserial_lpno');
								nlapiLogExecution('ERROR', 'Updating Serial NO.:', vInsSerialNo + "/" + serrialid);

								var fieldser = new Array();
								var valueser = new Array();
								fieldser[0] = 'custrecord_serialwmsstatus';						
								//fields[1] = 'custrecord_serialparentid';
								fieldser[1] = 'custrecord_serialitem';
								fieldser[2] = 'custrecord_serialstatus';
								valueser[0] = '19';//FLAG.INVENTORY.STORAGE
								//values[1] = CCarray["custparam_actlp"];
								valueser[1] = CCarray["custparam_actitem"];
								valueser[2] = 'C';
								var updatefields = nlapiSubmitField('customrecord_ebiznetserialentry',serrialid, fieldser, valueser);
								nlapiLogExecution('ERROR', 'Record Updated :');								

								var TempRecord=SrchRecordTmpSerial1[j];
								foparent.selectNewLineItem('recmachcustrecord_ebiz_tempserial_child');
								nlapiLogExecution('ERROR', 'TempRecord.getId()',TempRecord.getId());
								nlapiLogExecution('ERROR', 'TempRecord.getValue(name)',TempRecord.getValue('name'));
								foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_tempserial_child', 'id', TempRecord.getId());
								foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_tempserial_child','name', TempRecord.getValue('name'));

								foparent.setCurrentLineItemValue('recmachcustrecord_ebiz_tempserial_child','custrecord_tempserialstatus', 'T');

								foparent.commitLineItem('recmachcustrecord_ebiz_tempserial_child');
							}
							nlapiSubmitRecord(foparent);
						}
					}
					if(parseInt(CCarray["custparam_actualqty"])<parseInt(CCarray["custparam_expqty"]))
					{

							}
							response.sendRedirect('SUITELET', 'customscript_rf_cyclecount_confirm', 'customdeploy1', false, CCarray);
						}
					}
				}
				catch (e)  {
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
				} finally {					
					context.setSessionObject('session', null);
					nlapiLogExecution('DEBUG', 'finally','block');

				}
			}
			else
			{
				CCarray["custparam_screenno"] = '28';
				CCarray["custparam_error"] = 'SERIAL# ALREADY IN PROCESS';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, CCarray);
			}
		}
		catch(exp)
		{
			nlapiLogExecution('ERROR','Exception in Post',exp);
		}
	}
}
