/***************************************************************************
���������������������������eBizNET Solutions Inc
****************************************************************************
*
*� $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Client/ebiz_CancelFullfillmentOrder_CL.js,v $
*� $Revision: 1.2.6.2.4.1.6.2 $
*� $Date: 2015/04/13 09:18:12 $
*� $Author: snimmakayala $
*� $Name: t_eBN_2015_1_StdBundle_1_41 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_CancelFullfillmentOrder_CL.js,v $
*� Revision 1.2.6.2.4.1.6.2  2015/04/13 09:18:12  snimmakayala
*� 201412193
*�
*� Revision 1.2.6.2.4.3  2015/04/13 09:16:05  snimmakayala
*� 201412193
*�
*� Revision 1.2.6.2.4.2  2015/03/31 07:38:23  skreddy
*� Case# 201412193
*� True fabProd  issue fix
*�
*� Revision 1.2.6.2.4.1  2013/03/06 09:48:26  gkalla
*� CASE201112/CR201113/LOG201121
*� Lexjet Files merging for Standard bundle
*�
*� Revision 1.2.6.2  2012/11/01 14:55:22  schepuri
*� CASE201112/CR201113/LOG201121
*� Decimal Qty Conversions
*�
*� Revision 1.2.6.1  2012/10/30 06:10:07  spendyala
*� CASE201112/CR201113/LOG201121
*� Merged code form 2012.2 branch.
*�
*� Revision 1.2  2011/12/16 06:47:46  spendyala
*� CASE201112/CR201113/LOG201121
*� added functionality w.r.t display button
*�
*� Revision 1.1  2011/12/14 07:21:46  spendyala
*� CASE201112/CR201113/LOG201121
*� added client script to validate the checkbox depending on the wms status flag.
*�
*
****************************************************************************/

/**
 * @param type
 * @param name
 * @param lineno
 * @returns {Boolean}
 */
function OnChange(type,name,lineno)
{
	if(name=='custpage_confirm')
	{
		var WmsStatusFlag = nlapiGetLineItemValue('custpage_fullfillmentorder','custpage_wmsstatusid',lineno);
		var ordqty = nlapiGetLineItemValue('custpage_fullfillmentorder','custpage_ordqty',lineno);
		var pickgenqty = nlapiGetLineItemValue('custpage_fullfillmentorder','custpage_pickgenqty',lineno);
		if(WmsStatusFlag!=25&&WmsStatusFlag!=26)
		{
			alert('This Line Item is already considered for outbound process in WMS, so you cannot delete it.');
			nlapiSetLineItemValue('custpage_fullfillmentorder','custpage_confirm',lineno,'F');
			return false;
		}
		else if(WmsStatusFlag == 26)
		{
			if(pickgenqty==null || pickgenqty=='')
				pickgenqty=0;

			if((parseInt(ordqty)-parseInt(pickgenqty)) < parseInt(ordqty))
			{
				alert('You cannot delete an order line item that has been taken up for outbound processing within the WMS.');
				nlapiSetLineItemValue('custpage_fullfillmentorder','custpage_confirm',lineno,'F');
				return false;
			}
		}
		return true;
	}
}


/**
 * @returns {Boolean}
 */
function OnSave()
{
	var linecount = nlapiGetLineItemCount('custpage_fullfillmentorder');
	var tempflag= nlapiGetFieldValue('custpage_tempflag');
	if(tempflag=="")
	{
		if(parseFloat(linecount) > 0)
		{
			var userselection = false;	
			var selectedcount = 0;

			for (var p = 1; p <= linecount; p++) 
			{
				var confirmflag = nlapiGetLineItemValue('custpage_fullfillmentorder', 'custpage_confirm', p);
				var vShipQty = nlapiGetLineItemValue('custpage_fullfillmentorder', 'custpage_shipqty', p);
				var vOrderLine = nlapiGetLineItemValue('custpage_fullfillmentorder', 'custpage_ordline', p);
				var vpickgenQty = nlapiGetLineItemValue('custpage_fullfillmentorder', 'custpage_pickgenqty', p);
				
				//alert('vShipQty'+vShipQty);
				//alert('vOrderLine'+vOrderLine);
				if((vpickgenQty !='' && vpickgenQty!=null && vpickgenQty !='null' && vpickgenQty !=0 )&& (confirmflag == 'T'))
				{
					//alert('vOrderLine'+vOrderLine);
					alert('You cannot delete an order line item that has been taken up for outbound processing within the WMS.');
					nlapiSetLineItemValue('custpage_fullfillmentorder','custpage_confirm',vOrderLine,'F');
					return false;
				}
				if (confirmflag == 'T') 
				{
					userselection = true;	        	
				}
			}
			if(userselection == false)
			{
				alert('Please select atleast one row');
				return false;
			}
			return true;
		}
		else if(parseFloat(linecount)==0)
		{
			return false;
		}
		else
		{
			var SO= nlapiGetFieldValue('custpage_salesorderno');
			var FO= nlapiGetFieldValue('custpage_fullfillmentorder');
			if(SO==""&&FO=="")
			{
				alert('Fields cant be left empty need to provide Atleast one value!');
				return false;
			}
			return true;
		}
	}
	else
	{
		var SO= nlapiGetFieldValue('custpage_salesorderno');
		var FO= nlapiGetFieldValue('custpage_fullfillmentorder');
		if(SO==""&&FO=="")
		{
			alert('Fields cant be left empty need to provide Atleast one value!');
			return false;
		}
		return true;
	}
}

/**
 * 
 */
function Display()
{
	nlapiSetFieldValue('custpage_tempflag','display');
	NLDoMainFormButtonAction("submitter",true);
}