/***************************************************************************
  eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_TwoStepReplenItem.js,v $
 *     	   $Revision: 1.1.2.2 $
 *     	   $Date: 2015/04/23 21:01:47 $
 *     	   $Author: grao $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_TwoStepReplenItem.js,v $
 * Revision 1.1.2.2  2015/04/23 21:01:47  grao
 * SB issue fixes  201412469
 *
 * Revision 1.1.2.1  2015/01/02 15:05:10  skreddy
 * Case# 201411349
 * Two step replen process
 *
 *****************************************************************************/

function ReplenishmentItem(request, response){
	if (request.getMethod() == 'GET') {
		var reportNo = request.getParameter('custparam_repno');
		var zoneno = request.getParameter('custparam_zoneno');
		var cartlpno= request.getParameter('custparam_cartlpno');
		var html=getItemScan(reportNo,zoneno,getLanguage,cartlpno);
		response.write(html);
	}
	else {
		try {
			var Reparray = new Array();
			
			var getLanguage = request.getParameter('hdngetLanguage');
			Reparray["custparam_language"] = getLanguage;
			nlapiLogExecution('ERROR', 'getLanguage', Reparray["custparam_language"]);
			nlapiLogExecution('ERROR', 'hdnReplenType', request.getParameter('hdnReplenType'));
			
			var st8,st9;
			if( getLanguage == 'es_ES' || getLanguage =='es_AR')
			{

				st8 = "UBICACI&#211;N NO COINCIDEN";
				st9 = "LA UBICACI&#211;N ES NULL";
			}
			else
			{
				st8 = "LOCATION DO NOT MATCH";
				st9 = "LOCATION IS NULL";
			}

			var optedEvent = request.getParameter('cmdPrevious');
			var optedException=request.getParameter('cmdException');

			var getNumber = request.getParameter('hdngetnumber');
			nlapiLogExecution('Error', 'in response getNumber', getNumber);

			var enteredSku = request.getParameter('entersku');
			var hdnBeginLocation = request.getParameter('hdnBeginLocation');			
			var reportNo = request.getParameter('hdnNo');	
			var beginLocID = request.getParameter('hdnbeginLocId');
			var zoneID = request.getParameter('hdnZoneId');
			var toloc = request.getParameter('hdnactendlocation');
			var whLoc= request.getParameter('hdnwhlocation');
			nlapiLogExecution('ERROR', 'reportNo', reportNo);
			nlapiLogExecution('ERROR', 'hdnBeginLocation', hdnBeginLocation);
			nlapiLogExecution('ERROR', 'zoneID', zoneID);
			Reparray["custparam_screenno"] = '2stepRpItm';
			var sku = request.getParameter('hdnSkuNo');
			var vBatchno = request.getParameter('hdnvbatchno');
			nlapiLogExecution('ERROR', 'zoneID', zoneID);
			nlapiLogExecution('ERROR', 'zoneID', zoneID);
			Reparray["custparam_repno"] = request.getParameter('hdnNo');
			Reparray["custparam_repbegloc"] = request.getParameter('hdnBeginLocation');;
			Reparray["custparam_repbeglocId"] = request.getParameter('hdnbeginLocId');
			Reparray["custparam_repsku"] = request.getParameter('hdnSku');
			Reparray["custparam_repexpqty"] = request.getParameter('hdnexpQty');
			
			Reparray["custparam_repskuno"] = request.getParameter('hdnSkuNo');
		
			Reparray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
			Reparray["custparam_reppicklocno"] = request.getParameter('hdnactendlocationid');
			Reparray["custparam_reppickloc"] = request.getParameter('hdnactendlocation');
			Reparray["custparam_zoneno"] = request.getParameter('hdnZoneId');
			Reparray["custparam_cartlpno"] = request.getParameter('hdnCartlpno');
			Reparray["custparam_batchno"] = request.getParameter('hdnvbatchno');
			
			Reparray["custparam_error"] ='PLS ENTER VALID ITEM';

			nlapiLogExecution('ERROR', 'test2', 'test2');  
			nlapiLogExecution('ERROR', 'ot internal which is being processed further', Reparray["custparam_repid"]);  

			if (optedEvent == 'F7') {
				nlapiLogExecution('ERROR', 'REPLENISHMENT LOCATION F7 Pressed');
				response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenlocation', 'customdeploy_ebiz_twostepreplenlocation', false, Reparray);
			}
			else if(optedException=='F6')
			{
				
				nlapiLogExecution('ERROR', 'entered','Exception');
				
				response.sendRedirect('SUITELET', 'customscript_ebiz_rf_twostp_itmexception', 'customdeploy_ebiz_rf_twostp_itmexception', false, Reparray);
				//response.sendRedirect('SUITELET', 'customscript_ebiz_hook_sl_1', 'customdeploy_ebiz_hook_sl_1', false, Reparray);
				return;
			}
			else {
				nlapiLogExecution('ERROR', 'entered','ENT');
				nlapiLogExecution('ERROR', 'sku',sku);
				if(enteredSku != '' && enteredSku !=null && enteredSku !='null')
				{
					var currItem = validateSKU(enteredSku, null, null,null,sku);
					nlapiLogExecution('ERROR', 'currItem',currItem);
					if(currItem!=null && currItem!='')
					{
						nlapiLogExecution('ERROR', 'enteredSku',enteredSku);
						nlapiLogExecution('ERROR', 'request.getParameter(hdnSku)',request.getParameter('hdnSku'));
//case# 201416066
						if(currItem != request.getParameter('hdnSku'))
								{
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
							return;
								}
						if(sku !=null && sku !='')
						{
							var ItemTypeRec = nlapiLookupField('item', sku, ['recordType', 'custitem_ebizbatchlot']);				
							var batchflag = ItemTypeRec.custitem_ebizbatchlot;
							var ItemType = ItemTypeRec.recordType;
							if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem" ||batchflag=='T')
							{
								Reparray["custparam_itemType"] ='lotnumberedinventoryitem';
								response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplen_batch', 'customdeploy_ebiz_twostepreplen_batch_di', false, Reparray);	
								return;
							}
						}
						response.sendRedirect('SUITELET', 'customscript_ebiz_twostepreplenqty', 'customdeploy_ebiz_twostepreplenqty', false, Reparray);
						//response.sendRedirect('SUITELET', 'customscript_ebiz_hook_sl_2', 'customdeploy_ebiz_hook_sl_2', false, Reparray);
					}
					else
					{
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
					}
				}
				else
					{
					Reparray["custparam_error"] ='PLS ENTER ITEM';
					nlapiLogExecution('ERROR', 'pls enter item');
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
					nlapiLogExecution('ERROR', 'Catch: Location not found');
					}
			}
		} 
		catch (e) {
			nlapiLogExecution('ERROR', 'exception',e);
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, Reparray);
			nlapiLogExecution('ERROR', 'Catch: Location not found');
		}
	}
}




function getLocation(gettoloc)
{
	var validlocation=false;
	nlapiLogExecution('ERROR', 'gettoloc', gettoloc);
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'is', gettoloc);
	filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

	var columns=new Array();

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);

	if(searchresults!=null && searchresults!='')
		validlocation=true;

	nlapiLogExecution('ERROR', 'validlocation', validlocation);

	return validlocation;

}

function getfromLocation(fromLoc)
{
	var fromvalidlocation=false;
	nlapiLogExecution('ERROR', 'fromLoc', fromLoc);
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'is', fromLoc);
	filters[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F');

	var columns=new Array();

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_location', null, filters, columns);

	if(searchresults!=null && searchresults!='')
		fromvalidlocation=true;

	nlapiLogExecution('ERROR', 'fromvalidlocation', fromvalidlocation);

	return fromvalidlocation;
}

function getItemScan(reportNo,zoneNo,getLanguage,cartLpNo)
{
	var total;

	nlapiLogExecution('ERROR', 'reportNo',reportNo);	
	nlapiLogExecution('ERROR', 'zoneNo',zoneNo);	


	var itemfilters = new Array();		
	if(reportNo != null && reportNo != "")
	{
		itemfilters.push(new nlobjSearchFilter('name', null, 'is', reportNo));
	}
	if(zoneNo != null && zoneNo != "")
	{
		itemfilters.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', zoneNo));
	}
	itemfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8]));
	itemfilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [20])); 

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
	columns[1] = new nlobjSearchColumn('custrecord_skiptask',null,'group');
	
	columns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
	columns[3] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
	columns[4] = new nlobjSearchColumn('description','custrecord_sku','group');
	columns[5] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
	columns[6] = new nlobjSearchColumn('custrecord_actendloc',null,'group');
	columns[7] = new nlobjSearchColumn('name',null,'group');	
	columns[8] = new nlobjSearchColumn('salesdescription','custrecord_sku','group');
	columns[9] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
	columns[10] = new nlobjSearchColumn('custrecord_batch_no',null,'group');


	columns[1].setSort();
	columns[9].setSort();

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, itemfilters, columns);
	if (searchresults != null) 
	{
		nlapiLogExecution('ERROR', 'Itemsearchresults.length', searchresults.length);  
		var	total = searchresults.length;
		var	beginLoc = searchresults[0].getText('custrecord_actbeginloc',null,'group');
		var	beginLocId = searchresults[0].getValue('custrecord_actbeginloc',null,'group');
		var	whlocation = searchresults[0].getValue('custrecord_wms_location',null,'group');
		var	actEndLocationId = searchresults[0].getValue('custrecord_actendloc',null,'group');
		var	actEndLocation = searchresults[0].getText('custrecord_actendloc',null,'group');
		var	sku =  searchresults[0].getText('custrecord_sku',null,'group');
		var	skuNo =  searchresults[0].getValue('custrecord_sku',null,'group');

		var	expQty = searchresults[0].getValue('custrecord_expe_qty',null,'sum');
		var	batchno = searchresults[0].getValue('custrecord_batch_no',null,'group');
		
		var	Itemdescription = '';

		/*if(searchresults[0].getValue('description','custrecord_sku','group') != null && searchresults[0].getValue('description','custrecord_sku','group') != "")
				{	
			Itemdescription = searchresults[0].getValue('description','custrecord_sku','group');
				}
		else if(searchresults[0].getValue('salesdescription','custrecord_sku','group') != null && searchresults[0].getValue('salesdescription','custrecord_sku','group') != "")
				{	
			Itemdescription = searchresults[0].getValue('salesdescription','custrecord_sku','group');
				}
		if(Itemdescription != null && Itemdescription !='')
		{

			Itemdescription = Itemdescription.substring(0, 20);
		}*/
		nlapiLogExecution('ERROR', 'skuNo', skuNo);
		var fields=new Array();
		fields[0]='name';
		fields[1]='custitem_ebizdescriptionitems';
		fields[2]='description';
		
		var rec=nlapiLookupField('item', skuNo, fields);

		//case no 20125087
		if(rec != null && rec != '')
		{
			sku=rec.name;
			Itemdescription=rec.custitem_ebizdescriptionitems;
			nlapiLogExecution('ERROR', 'custitem_ebizdescriptionitems', Itemdescription);
			if(Itemdescription==null || Itemdescription=='')
			{
				Itemdescription=rec.description;
				Itemdescription=Itemdescription.substring(0, 20)
			}
		}
		nlapiLogExecution('ERROR', 'Itemdescription', Itemdescription);
		//case20125658 and 20125602 start :spanish conversion

		var st0,st1,st2,st3,st4,st5,st6,st7,st8;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st0 = "DE UBICACI&#211;N :";
			st1 = "ART&#205;CULO :";
			st2 = "DESCRIPCI&#211;N DE ART&#205;CULO :";
			st3 = "CANTIDAD :";
			st4 = "ENTRAR / SCAN DE UBICACI&#211;N :";
			st5 = "ENVIAR";
			st6 = "ANTERIOR";
			st7 = "SIGUIENTE";
			st8 = "DE";

		}
		else
		{
			st0 = "FROM LOCATION :";
			st1 = "ITEM:";
			st2 = "ITEM DESCRIPTION: ";
			st3 = "QUANTITY: ";
			st4 = "ENTER/SCAN ITEM:";
			st5 = "SEND";
			st6 = "PREV";
			st7 = "EXCEPTION";//"NEXT";
			st8 = "OF";

		}


		//case20125658 and 20125602 end

		var functionkeyHtml=getFunctionkeyScript('_rf_replenishment_location'); 
		var html = "<html><head><title>REPLENISHMENT LOCATION</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		html = html + "function stopRKey(evt) { ";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";
		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_replenishment_location' method='POST'>";
		html = html + "		<table>";

		/*html = html + "			<tr>";
	html = html + "				<td align = 'left'>" + (parseFloat(getNumber) + 1) + " "+st8+" <label>" + parseFloat(total) + "</label>";
	html = html + "			</tr>";*/
		html = html + "			<tr>";
//		html = html + "				<td align = 'left'> "+st0+"  <label>" + beginLoc + "</label></td>";
		html = html + "				<td align = 'left'> ";
		html = html + "				<input type='hidden' name='hdnNo' value=" + reportNo + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + beginLoc + ">";

		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whlocation + ">";
		html = html + "				<input type='hidden' name='hdnactendlocationid' value=" + actEndLocationId + ">";
		html = html + "				<input type='hidden' name='hdnactendlocation' value=" + actEndLocation + ">";
		html = html + "				<input type='hidden' name='hdnbeginLocId' value=" + beginLocId + ">";
		html = html + "				<input type='hidden' name='hdnZoneId' value=" + zoneNo + ">";
		html = html + "				<input type='hidden' name='hdnSkuNo' value=" + skuNo + ">";
		html = html + "				<input type='hidden' name='hdnSku' value='" + sku + "'>";
		html = html + "				<input type='hidden' name='hdnexpQty' value=" + expQty + ">";
		html = html + "				<input type='hidden' name='hdnItemdescription' value=" + Itemdescription + ">";
		html = html + "				<input type='hidden' name='hdnCartlpno' value=" + cartLpNo + ">";
		
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				<input type='hidden' name='hdnvbatchno' value='" + batchno + "'>";	
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> "+st1+"  <label>" + sku + "</label></td>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> "+st2+" <label>" + Itemdescription + "</label></td>";
		html = html + "				</td>";
		html = html + "			</tr>";
		/*html = html + "			<tr>";
		html = html + "				<td align = 'left'> "+ st3+ " <label>" + expQty + "</label></td>";
		html = html + "				</td>";
		html = html + "			</tr>";*/
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st4;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='entersku' id='entersku' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st5+" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true;this.form.cmdSkip.disabled=true; return false'/>";
		html = html + "					"+st7+" <input name='cmdException' type='submit' value='F6'/>"+st6+" <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "<script type='text/javascript'>document.getElementById('enterfromloc').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";
		return html;
	}
}

function validateSKU(itemNo, location, company,poid,sku){
	nlapiLogExecution('DEBUG', 'validateSKU', 'Start');

	var inputParams = 'Item = ' + itemNo + '<br>';
	inputParams = inputParams + 'Location = ' + location + '<br>';
	inputParams = inputParams + 'Company = ' + company + '<br>';;
	inputParams = inputParams + 'PO Id = ' + poid;
	nlapiLogExecution('DEBUG', 'Input Parameters', inputParams);

	var currItem = eBiz_RF_GetItemForItemNo(itemNo, location);

	if(currItem == ""){
		currItem = eBiz_RF_GetItemBasedOnUPCCodeNew(itemNo, location,sku);
	}

	if(currItem==""){

		var skuAliasFilters = new Array();
		skuAliasFilters.push(new nlobjSearchFilter('name',null, 'is',itemNo));
		skuAliasFilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		if(location!=null && location!='' && location!='null')
		skuAliasFilters.push(new nlobjSearchFilter('custrecord_ebiz_location',null, 'is',location));


		var skuAliasCols = new Array();
		skuAliasCols[0] = new nlobjSearchColumn('custrecord_ebiz_item');

		var skuAliasResults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, skuAliasFilters, skuAliasCols);
		if(skuAliasResults!=null && skuAliasResults!='')
		currItem = skuAliasResults[0].getText('custrecord_ebiz_item');
	}

	
	
	if(currItem == ""){
		var vendor='';
		if(poid!=null && poid!='')
		{
			var po  = nlapiLoadRecord('purchaseorder',poid);
			vendor=po.getFieldValue('entity');
		}
		currItem = eBiz_RF_GetItemFromSKUDims(itemNo, location,vendor);
	}

	var logMsg = "";
	if(currItem == ""){
		logMsg = 'Unable to retrieve item';
		currItem = null;
	} else {
		logMsg = 'Item = ' + currItem;
	}

	nlapiLogExecution('DEBUG', 'Item Retrieved', logMsg);
	nlapiLogExecution('DEBUG', 'validateSKU', 'End');
	return currItem;
}
function eBiz_RF_GetItemBasedOnUPCCodeNew(itemNo, location,sku)
{
	  nlapiLogExecution('ERROR', 'eBiz_RF_GetItemBasedOnUPCCode', 'Start');
	  nlapiLogExecution('ERROR', 'sku', sku);
	    var currItem = "";
	    
	    //below code is commented becuase item detials are not fetching when scan UPC code
	    /*var filters = new Array();
	    filters.push(new nlobjSearchFilter('internalid', null, 'is', sku));
	    //if(location!=null && location!='')
	      //  filters.push(new nlobjSearchFilter('location', null, 'anyof', ['@NONE@',location]));
	
	    var columns = new Array();
	    columns[0] = new nlobjSearchColumn('itemid');
	    columns[1] = new nlobjSearchColumn('upccode');
	    columns[0].setSort(true);
	
	    var itemSearchResult = nlapiSearchRecord('item',null,filters,columns);
	    nlapiLogExecution('ERROR', 'itemSearchResult', itemSearchResult);
	    if(itemSearchResult != null && (itemSearchResult[0].getValue('upccode')==itemNo))
	        currItem = itemSearchResult[0].getValue('itemid');*/
		var filters = new Array();
		filters.push(new nlobjSearchFilter('upccode', null, 'is', itemNo));
		if(location!=null && location!='' && location!='null')
			filters.push(new nlobjSearchFilter('location', null, 'anyof', ['@NONE@',location]));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
//		var columns = new Array();
		//columns.push(new nlobjSearchColumn('itemid'));
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('itemid');
		columns[0].setSort(true);

		var itemSearchResults = nlapiSearchRecord('item', null, filters, columns);

		if(itemSearchResults != null)
			currItem = itemSearchResults[0].getValue('itemid');
	
	    var logMsg = 'Item = ' + currItem;
	    nlapiLogExecution('ERROR', 'Item Retrieved', logMsg);
	    nlapiLogExecution('ERROR', 'eBiz_RF_GetItemBasedOnUPCCode', 'End');
	    
	    return currItem;
}
