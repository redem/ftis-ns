/***************************************************************************
 eBizNET Solutions Inc                
 ***************************************************************************
 **     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/ebiz_RF_ClusPickSummTask.js,v $
 *     	   $Revision: 1.1.2.11.4.9.2.36.2.1 $
 *     	   $Date: 2015/11/24 11:10:26 $
 *     	   $Author: deepshikha $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_ClusPickSummTask.js,v $
 * Revision 1.1.2.11.4.9.2.36.2.1  2015/11/24 11:10:26  deepshikha
 * 2015.2 Issue Fix
 * 201414124
 *
 * Revision 1.1.2.11.4.9.2.36  2015/07/16 15:17:59  grao
 * 2015.2   issue fixes  201413083
 *
 * Revision 1.1.2.11.4.9.2.35  2015/05/26 13:27:26  schepuri
 * case# 201412862
 *
 * Revision 1.1.2.11.4.9.2.34  2015/04/17 15:25:50  grao
 * SB issue fixes  201412376
 *
 * Revision 1.1.2.11.4.9.2.33  2015/04/13 09:25:59  rrpulicherla
 * Case#201412277
 *
 * Revision 1.1.2.11.4.9.2.32  2014/11/26 14:07:51  skavuri
 * Case# 201411125,201411126 Std bundle issue fixed
 *
 * Revision 1.1.2.11.4.9.2.31  2014/11/12 14:38:31  sponnaganti
 * Case# 201411309  201410866
 * True Fab SB Issue fix
 *
 * Revision 1.1.2.11.4.9.2.30  2014/11/06 15:56:28  sponnaganti
 * Case# 201410960 and 201410961
 * True Fab SB Issue fix
 *
 * Revision 1.1.2.11.4.9.2.29  2014/09/24 10:53:23  grao
 * Case#: 201410504  Dc Dental Sb fixes
 *
 * Revision 1.1.2.11.4.9.2.28  2014/08/28 06:24:27  skreddy
 * case # 201410016
 * DCD SB issue fix
 *
 * Revision 1.1.2.11.4.9.2.27  2014/08/13 07:39:46  gkalla
 * case#20149227
 * JB-Cycle Count - Even though the Cycle Count is in process user should be able to pick the product
 *
 * Revision 1.1.2.11.4.9.2.26  2014/06/27 13:38:56  skavuri
 * Case# 20149083 Compatability Issue Fixed
 *
 * Revision 1.1.2.11.4.9.2.25  2014/06/13 13:19:16  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.11.4.9.2.24  2014/06/06 07:13:39  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.1.2.11.4.9.2.23  2014/05/30 00:41:01  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.11.4.9.2.22  2014/05/26 07:11:21  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148320
 *
 * Revision 1.1.2.11.4.9.2.21  2014/04/18 18:16:37  grao
 * Case# 20148064 related issue fixes in LL Sb
 *
 * Revision 1.1.2.11.4.9.2.20  2014/04/16 15:34:03  nneelam
 * case#  20148013
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.11.4.9.2.19  2014/03/12 06:33:34  skreddy
 * case 20127626
 * Deal med SB issue fixs
 *
 * Revision 1.1.2.11.4.9.2.18  2014/01/28 15:06:17  rmukkera
 * Case # 20126964
 *
 * Revision 1.1.2.11.4.9.2.17  2014/01/06 13:19:17  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.1.2.11.4.9.2.16  2013/12/10 14:51:25  nneelam
 * Case# 20126284
 * No serial scan while cluster picking.
 *
 * Revision 1.1.2.11.4.9.2.15  2013/11/22 14:18:14  schepuri
 * 20125891
 *
 * Revision 1.1.2.11.4.9.2.14  2013/09/11 14:25:29  skreddy
 * Case# 20124362
 * CWD SB  issue fix
 *
 * Revision 1.1.2.11.4.9.2.13  2013/09/06 23:33:17  nneelam
 * Case#.20124262�
 * Cluster Picking RF Issue Fix..
 *
 * Revision 1.1.2.11.4.9.2.12  2013/09/02 15:37:46  rmukkera
 * Case# 20124187
 *
 * Revision 1.1.2.11.4.9.2.11  2013/08/29 15:01:18  spendyala
 * CASE201112/CR201113/LOG201121
 * Given NEW SKIP option when there is an invt hold flag
 * against the inventory record
 *
 * Revision 1.1.2.11.4.9.2.10  2013/08/05 15:04:15  rrpulicherla
 * case#20123755
 * BB Cycle count issues
 *
 * Revision 1.1.2.11.4.9.2.9  2013/06/18 13:33:11  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * nls issue fixes
 *
 * Revision 1.1.2.11.4.9.2.8  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.2.11.4.9.2.7  2013/05/13 15:08:06  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle Issue Fixes
 *
 * Revision 1.1.2.11.4.9.2.6  2013/04/26 15:49:51  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fixes
 *
 * Revision 1.1.2.11.4.9.2.5  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.11.4.9.2.4  2013/04/12 11:03:24  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.1.2.11.4.9.2.3  2013/03/21 14:18:14  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.1.2.11.4.9.2.2  2013/03/19 11:47:21  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.1.2.11.4.9.2.1  2013/03/05 13:35:38  rmukkera
 * Merging of lexjet Bundle files to Standard bundle
 *
 * Revision 1.1.2.11.4.9  2013/02/06 14:38:20  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.1.2.11.4.8  2012/12/28 16:32:05  skreddy
 * CASE201112/CR201113/LOG201121
 * Lot Exception in Cluster Picking
 *
 * Revision 1.1.2.11.4.7  2012/12/17 15:06:57  schepuri
 * CASE201112/CR201113/LOG201121
 * checking for null
 *
 * Revision 1.1.2.11.4.6  2012/11/23 06:35:26  schepuri
 * CASE201112/CR201113/LOG201121
 * issue related to location null
 *
 * Revision 1.1.2.11.4.5  2012/11/01 14:55:23  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.1.2.11.4.4  2012/10/04 10:31:18  grao
 * CASE201112/CR201113/LOG201121
 * Converting multilanguage with given suggestions
 *
 * Revision 1.1.2.11.4.3  2012/10/02 22:45:03  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue Fixes for FISK,BOOMBAH and TDG.
 *
 * Revision 1.1.2.11.4.2  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.1.2.11.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.1.2.11  2012/09/09 02:42:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * SKIP issue fixes.
 *
 * Revision 1.1.2.10  2012/09/05 03:07:45  snimmakayala
 * CASE201112/CR201113/LOG201121
 * F7 (Previous Button) Issue.
 *
 * Revision 1.1.2.8  2012/09/03 13:52:07  schepuri
 * CASE201112/CR201113/LOG201121
 * added date stamp
 *
 * Revision 1.1.2.7  2012/08/27 07:05:07  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * clusterpickingReplen
 *
 * Revision 1.1.2.6  2012/04/27 13:15:36  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Cluster picking
 *
 * Revision 1.1.2.5  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.1.2.4  2012/02/28 01:31:32  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.3  2012/02/28 01:22:19  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.2  2012/02/24 11:22:30  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.1  2012/02/23 13:08:07  snimmakayala
 * CASE201112/CR201113/LOG201121
 * RF Cluster Picking Changes
 *
 * Revision 1.12  2012/02/20 14:25:46  gkalla
 * CASE201112/CR201113/LOG201121
 * Added Loc Exception
 *
 * Revision 1.11  2012/02/03 10:44:44  gkalla
 * CASE201112/CR201113/LOG201121
 * Added comment
 *
 * Revision 1.9  2012/01/11 14:33:07  rmukkera
 * CASE201112/CR201113/LOG201121
 * item description was added
 *
 * Revision 1.8  2011/12/12 12:58:54  schepuri
 * CASE201112/CR201113/LOG201121
 * adding new button for RF PICK Exception
 *
 * Revision 1.7  2011/09/29 16:17:23  gkalla
 * CASE201112/CR201113/LOG201121
 * To integrate Serial no entry in RF outbound
 *
 * Revision 1.6  2011/09/10 07:24:50  skota
 * CASE201112/CR201113/LOG201121
 * code changes to redirect to right suitelet for capturing serial nos
 *
 * Revision 1.5  2011/09/09 18:08:24  skota
 * CASE201112/CR201113/LOG201121
 * If condition change to consider serial out flag
 *
 * 
 *****************************************************************************/
function ClusterPickingSummTask(request, response){


	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);

	if (request.getMethod() == 'GET') {


		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st7,st8;
//		 case no 20125891
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st1 = "ART&#205;CULO:";
			st2 = "UBICACI&#211;N:";
			st3 = "CONF";
			st4 = "ANTERIOR";
			st5 = "PASE";
			st7 = "DESCRIPCI&#211;N DE ART&#205;CULO:";
			st8 = "Cantidad total SELECCI&#211;N :";
			st9="UBICACI&#211;N / EXCEPCI&#211;N LOT";

		}
		else
		{
			st1 = "ITEM: ";
			st2 = "LOCATION : ";
			st3 = "CONF";
			st4 = "PREV";
			st5 = "SKIP";
			st7 = "ITEM DESCRIPTION:";
			st8 = "TOTAL PICK QTY :";
			st9 = "LOC/LOT EXCEPTION";


		}

		var getWaveno = request.getParameter('custparam_waveno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocationId = request.getParameter('custparam_beginLocation');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');        
		var getEnteredLocation = request.getParameter('custparam_endlocation');
		var getBeginBinLocation = request.getParameter('custparam_beginlocationname');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var vclusterNo = request.getParameter('custparam_clusterno');		
		var vBatchno = request.getParameter('custparam_batchno');
		var RecordCount=request.getParameter('custparam_nooflocrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var NextItem=request.getParameter('custparam_nextitem');
		var name=request.getParameter('name');
		var ContainerSize=request.getParameter('custparam_containersize');
		var getOrderNo=request.getParameter('custparam_ebizordno');
		var pickType=request.getParameter('custparam_picktype');
		var whLocation = request.getParameter('custparam_whlocation');
		var IsSkippedTask = request.getParameter('custparam_istaskskipped');
		nlapiLogExecution('DEBUG', 'getEbizSO', getOrderNo);
		nlapiLogExecution('DEBUG', 'Begin Location', getBeginBinLocation);
		nlapiLogExecution('DEBUG', 'Next Location', NextLocation);		
		nlapiLogExecution('DEBUG', 'Next Item InternalId', NextItemInternalId);
		nlapiLogExecution('DEBUG', 'Next Item ', NextItem);
		nlapiLogExecution('DEBUG', 'IsSkippedTask', IsSkippedTask);
		var itemType=request.getParameter('custparam_itemType');
		var serialscanned = request.getParameter('custparam_serialscanned');
		var detailTask=request.getParameter('custparam_detailtask');
		var itemstatus=request.getParameter('custparam_itemstatus');
		var totalqty = request.getParameter('custparam_totalqty');
		
		var EntLoc = request.getParameter('custparam_EntLoc');
		var EntLocRec=request.getParameter('custparam_EntLocRec');
		var EntLocID=request.getParameter('custparam_EntLocId');
		var ExceptionFlag = request.getParameter('custparam_Exc');
		var EntLotID=request.getParameter('custparam_entLotId');
		/*var itemType=request.getParameter('custparam_itemType');
		var serialscanned = request.getParameter('custparam_serialscanned');
		var detailTask=request.getParameter('custparam_detailtask');
		nlapiLogExecution('ERROR', 'getEbizSO', getOrderNo);
		nlapiLogExecution('ERROR', 'Begin Location', getBeginBinLocation);
		nlapiLogExecution('ERROR', 'Next Location', NextLocation);		
		nlapiLogExecution('ERROR', 'Next Item InternalId', NextItemInternalId);
		nlapiLogExecution('ERROR', 'Next Item ', NextItem);
		nlapiLogExecution('ERROR', 'serialscanned ', serialscanned);

		var getItem = ItemRec.getFieldValue('itemid');
		var Itemdescription='';
		nlapiLogExecution('DEBUG', 'Location Name is', getItem);
		if(ItemRec.getFieldValue('description') != null && ItemRec.getFieldValue('description') != "")
		{	
			Itemdescription = ItemRec.getFieldValue('description');
		}
		else if(ItemRec.getFieldValue('salesdescription') != null && ItemRec.getFieldValue('salesdescription') != "")
		{	
			Itemdescription = ItemRec.getFieldValue('salesdescription');
		}	*/

		var getItem="";
		var Itemdescription='';
		if(getItemInternalId!=null&& getItemInternalId!="")
		{
			var filter=new Array();
			filter[0]=new nlobjSearchFilter("internalid",null,"anyof",getItemInternalId);

			var column=new Array();
			column[0]=new nlobjSearchColumn("type");
			column[1]=new nlobjSearchColumn("description");
			column[2]=new nlobjSearchColumn("itemid");

			var searchres=nlapiSearchRecord("item",null,filter,column);
			if(searchres!=null&&searchres!="")
			{
				getItem=searchres[0].getValue("itemid");
				Itemdescription=searchres[0].getValue("description");
			}
			Itemdescription = Itemdescription.substring(0, 100);		//case# 201412862
		}
		nlapiLogExecution('DEBUG', 'getItemdescription', Itemdescription);
		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends

		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";

		//html = html + " document.getElementById('cmdConfirm').focus();";   

		html = html + "function stopRKey(evt) { ";
		//html = html + "	  alert('evt');";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";

		html = html + "	document.onkeypress = stopRKey; ";


		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st2 +"<label>" + getEnteredLocation + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +"<label>" + getItem + "</label><br>"+ st7 +"<label>" + Itemdescription + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st8 +"<label>" + getExpectedQuantity + "</label>";
		//	html = html + "				<td align = 'left'>"+ st8 +"<label>" + totalqty + "</label>";
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
		html = html + "				<input type='hidden' name='hdnItem' value='" + getItem + "'>";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEnteredLocation' value=" + getEnteredLocation + ">";
		html = html + "				<input type='hidden' name='hdnBeginBinLocation' value=" + getBeginBinLocation + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vclusterNo + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value='" + vBatchno + "'>";//Case# 201411125
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnNextLocation' value=" + NextLocation + ">";
		html = html + "				<input type='hidden' name='hdnNextItemId' value=" + NextItemInternalId + ">";	
		html = html + "				<input type='hidden' name='hdnNextItem' value=" + NextItem + ">";		
		html = html + "				<input type='hidden' name='hdnName' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + ContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnebizOrdNo' value=" + getOrderNo + ">";
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnserialscanned' value=" + serialscanned + ">";
		html = html + "				<input type='hidden' name='hdnflag'>";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				<input type='hidden' name='hdnitemtype' value=" + itemType + ">";	
		html = html + "				<input type='hidden' name='hdndetailtask' value=" + detailTask + ">";	
		html = html + "				<input type='hidden' name='hdnitemstatus' value=" + itemstatus + ">";
		html = html + "				<input type='hidden' name='hdnEntLoc' value=" + EntLoc + ">";
		html = html + "				<input type='hidden' name='hdnEntLocRec' value=" + EntLocRec + ">";
		html = html + "				<input type='hidden' name='hdnEntLocID' value=" + EntLocID + ">";
		html = html + "				<input type='hidden' name='hdnExceptionFlag' value=" + ExceptionFlag + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";		
		html = html + "				<input type='hidden' name='hdnEntLotID' value='" + EntLotID + "'>";
		html = html + "				</td>";
		html = html + "			</tr>";		
//		html = html + "			<tr>";
//		html = html + "				<td align = 'left'>SUSPEND <input name='cmdSuspend' type='submit' value='F10'/>";
//		html = html + "				QTY EXCEPTION <input name='cmdPickException' type='submit' value='F2'/></td></tr>";
//		html = html + "				<tr><td align = 'left'>LOC EXCEPTION <input name='cmdlocexc' type='submit' value='F3'/></td></tr>";
		html = html + "				<tr><td align = 'left'>LOC/LOT EXCEPTION <input name='cmdlocexc' type='submit' value='F3'/></td></tr>";		
		html = html + "				<tr><td align = 'left'>" ;
		if(IsSkippedTask!="true")
			html = html + "				"+ st3 +"<input name='cmdConfirm' type='submit' id='cmdConfirm' value='ENT' onclick='this.form.hdnflag.value=this.value;this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; this.form.cmdSkip.disabled=true; return false'/>";
		html = html + "				"+ st4 +"<input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				"+ st5 +"<input name='cmdSkip' type='submit' value='F12'/></td></tr>";
		/*html = html + "				<tr><td align = 'left'>CONF AND STAGE <input name='cmdSuspend' type='submit' value='F11'/>";*/
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('cmdConfirm').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var getLanguage = request.getParameter('hdngetLanguage');

		var st6,st7,st8;

		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{

			st6 = "CONFIRME LA REPLENS ABIERTAS";
			st7 = "DESCRIPCI&#211;N DEL ART&#205;CULO:";
			st8 = "CANT TOTAL DE SELECCI&#211;N:";

		}
		else
		{

			st6 = "CONFIRM THE OPEN REPLENS";
			st7 = "ITEM DESCRIPTION: ";
			st8 = "TOTAL PICK QTY : ";
		}


		nlapiLogExecution('DEBUG', 'Into Response request.getParameter(cmdPickException)', request.getParameter('cmdPickException'));

		// This variable is to hold the SO# entered.
		var SOarray = new Array();
		SOarray["custparam_language"] = getLanguage;

		nlapiLogExecution('DEBUG', 'Into SOarray[custparam_language]', SOarray["custparam_language"]);

		SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
		SOarray["custparam_recordinternalid"] = request.getParameter('hdnRecordInternalId');
		SOarray["custparam_containerlpno"] = request.getParameter('hdnContainerLpNo');
		SOarray["custparam_expectedquantity"] = request.getParameter('hdnExpectedQuantity');
		SOarray["custparam_beginLocation"] = request.getParameter('hdnBeginLocationId');
		//SOarray["custparam_item"] = request.getParameter('hdnItem');
		SOarray["custparam_item"] = request.getParameter('hdnItem');
		SOarray["custparam_itemdescription"] = request.getParameter('hdnItemDescription');
		SOarray["custparam_iteminternalid"] = request.getParameter('hdnItemInternalId');
		SOarray["custparam_dolineid"] = request.getParameter('hdnDOLineId');
		SOarray["custparam_invoicerefno"] = request.getParameter('hdnInvoiceRefNo');
		SOarray["custparam_beginlocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
		SOarray["custparam_orderlineno"] = request.getParameter('hdnOrderLineNo');
		SOarray["custparam_clusterno"] = request.getParameter('hdnClusterNo');
		SOarray["custparam_batchno"] = request.getParameter('hdnbatchno');
		SOarray["custparam_nooflocrecords"] = request.getParameter('hdnRecCount');
		SOarray["custparam_nextlocation"] = request.getParameter('hdnNextLocation');
		SOarray["custparam_nextiteminternalid"] = request.getParameter('hdnNextItemId');	
		SOarray["custparam_nextitem"] = request.getParameter('hdnNextItem');
		SOarray["name"] = request.getParameter('hdnName');
		SOarray["custparam_containersize"] = request.getParameter('hdnContainerSize');
		SOarray["custparam_ebizordno"] = request.getParameter('hdnebizOrdNo');
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		SOarray["custparam_itemType"] = request.getParameter('hdnitemtype');
		SOarray["custparam_serialscanned"] = request.getParameter('hdnserialscanned');
		var serialscanned = request.getParameter('hdnserialscanned');
		var ItemStatus=request.getParameter('hdnitemstatus');
		SOarray["custparam_itemstatus"] = ItemStatus;
		// case# 201417167
		SOarray["custparam_entLotId"]  = request.getParameter('hdnEntLotID');

		nlapiLogExecution('DEBUG', 'Next Location', SOarray["custparam_nextlocation"]);
		nlapiLogExecution('DEBUG', 'Next Item InternalId', SOarray["custparam_nextiteminternalid"]);
		nlapiLogExecution('DEBUG', 'Next Item ', SOarray["custparam_nextitem"]);

		SOarray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
		//	if the previous button 'F7' is clicked, it has to go to the previous screen
		SOarray["custparam_totalqty"] = request.getParameter('custparam_totalqty');
//		case# 201416268
		var batchflag="F";
		var serialout="F";
		var ItemRecordType='';
		var itemflag = '';
		var ItemInternalId = request.getParameter('hdnItemInternalId');
		nlapiLogExecution('DEBUG', 'ItemInternalId ', ItemInternalId);
		var SkipStaging = getSystemRuleValue('Skip Staging?');
		nlapiLogExecution('DEBUG', 'SkipStaging', SkipStaging);
		if(ItemInternalId!=null && ItemInternalId !='')
		{
			var itemSubtype = nlapiLookupField('item', ItemInternalId, ['recordType', 'custitem_ebizserialin','custitem_ebizserialout', 'custitem_ebizbatchlot']);
			if(itemSubtype != null && itemSubtype != '')
			{
				batchflag=itemSubtype.custitem_ebizbatchlot;
				ItemRecordType=itemSubtype.recordType;
				serialout=itemSubtype.custitem_ebizserialout;
			}
			//var batchflag=itemSubtype.custitem_ebizbatchlot;
			//if (itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.custitem_ebizserialout == 'T')
			if (ItemRecordType == 'serializedinventoryitem' || serialout == 'T')
			{
				itemflag='S';//for serial item
			}
			else
			{
				itemflag='N';//for normal item
			}
		}				nlapiLogExecution('DEBUG', 'item flag', itemflag);



		if (sessionobj!=context.getUser()) {
			try
			{

				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}
				if (request.getParameter('cmdPrevious') == 'F7')
				{

					if (itemSubtype.recordType == 'lotnumberedinventoryitem' || itemSubtype.recordType == 'lotnumberedassemblyitem' || batchflag == 'T')

					{
						nlapiLogExecution('DEBUG', 'Clicked on Previous for batch screen', request.getParameter('cmdPrevious'));
						response.sendRedirect('SUITELET', 'customscript_rf_picking_batch', 'customdeploy_rf_picking_batch_di', false, SOarray);
					}
					else
					{

						nlapiLogExecution('DEBUG', 'Clicked on Previous', request.getParameter('cmdPrevious'));
						response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_item', 'customdeploy_rf_cluspicking_item', false, SOarray);
					}
				}
				else 
				{
					if (request.getParameter('hdnflag') == 'ENT') {

						nlapiLogExecution('DEBUG', 'Confirm Pick');

						var RecordInternalId = request.getParameter('hdnRecordInternalId');
						var ContainerLPNo = request.getParameter('hdnContainerLpNo');
						var ExpectedQuantity = request.getParameter('hdnExpectedQuantity');
						var BeginLocation = request.getParameter('hdnBeginLocationId');
						var Item = request.getParameter('hdnItem');
						var ItemDescription = request.getParameter('hdnItemDescription');

						var DoLineId = request.getParameter('hdnDOLineId'); // internal id of DO
						var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
						var BeginLocationName = request.getParameter('hdnBeginBinLocation');
						var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
						var EndLocation = request.getParameter('hdnEnteredLocation');
						var OrderLineNo = request.getParameter('hdnOrderLineNo');
						var NextShowLocation=request.getParameter('hdnNextLocation');

						var ContainerSize=request.getParameter('hdnContainerSize');
						var NextItemInternalId=request.getParameter('hdnNextItemId');
						var WaveNo = request.getParameter('hdnWaveNo');
						var ClusNo = request.getParameter('hdnClusterNo');
						var ebizOrdNo=request.getParameter('hdnebizOrdNo');
						var OrdName=request.getParameter('hdnName');
				var vBatchno = request.getParameter('hdnbatchno');
						nlapiLogExecution('DEBUG', 'OrdName', OrdName);
						nlapiLogExecution('DEBUG', 'Next Location', NextShowLocation);
						nlapiLogExecution('DEBUG', 'BeginLocation', BeginLocation);

						nlapiLogExecution('DEBUG', 'Before Item Fulfillment', 'Before Item Fulfillment');
				var getEndLocInternalId = request.getParameter('hdnEndLocInternalId');
				var EntLoc = request.getParameter('hdnEntLoc');
				var EntLocRec = request.getParameter('hdnEntLocRec');
				var EntLocID = request.getParameter('hdnEntLocID');
				var ExceptionFlag = request.getParameter('hdnExceptionFlag');
				var EntLotID = request.getParameter('hdnEntLotID');
				if(ExceptionFlag=='L')
				{
					if(EntLocID!=null&&EntLocID!="")
					{
						EndLocation=EntLocID;
						getEndLocInternalId = EntLocID;
						vBatchno = EntLotID;
					}
				}
				//detail task functionality
				nlapiLogExecution('DEBUG', 'EndLocation', EndLocation);
				nlapiLogExecution('DEBUG', 'getEndLocInternalId', getEndLocInternalId);
				var detailTask='';
				if(request.getParameter('custparam_detailtask')!=null && request.getParameter('custparam_detailtask')!='')
				{
					detailTask=request.getParameter('custparam_detailtask');
				}
				nlapiLogExecution('DEBUG', 'detailTask', detailTask);
				nlapiLogExecution('DEBUG', 'serialscanned', serialscanned);
				SOarray["custparam_detailtask"] = detailTask;
				var itemflag='';
				
			
				nlapiLogExecution('DEBUG', 'itemflag', itemflag);
				
					if(detailTask !='' && detailTask =='F' && (serialscanned == null || serialscanned == '') && itemflag =='S' )//Case# 20149083
				//case # 20148013
				//if((serialscanned == null || serialscanned == '' || serialscanned == 'null') && itemflag =='S' ) //Case# 20149083
					{

							/*var itemSubtype = nlapiLookupField('item', ItemInternalId, ['recordType', 'custitem_ebizserialin','custitem_ebizserialout', 'custitem_ebizbatchlot']);

				if (itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.custitem_ebizserialout == 'T') {
					SOarray["custparam_number"] = 0;
					SOarray["custparam_RecType"] = itemSubtype.recordType;
					SOarray["custparam_SerOut"] = itemSubtype.custitem_ebizserialout;
					SOarray["custparam_SerIn"] = itemSubtype.custitem_ebizserialin;
					response.sendRedirect('SUITELET', 'customscript_rf_pick_serialscan', 'customdeploy_rf_pick_serialscan_di', false, SOarray);
						return;*/
							//}
							// Case# 20149083 starts
							var itemSubtype = nlapiLookupField('item', ItemInternalId, ['recordType', 'custitem_ebizserialin','custitem_ebizserialout', 'custitem_ebizbatchlot']);
							nlapiLogExecution('DEBUG', 'itemSubtype is', itemSubtype);
							if (itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.custitem_ebizserialout == 'T') { 	// Case# 20149083 ends
								SOarray["custparam_number"] = 0;
								SOarray["custparam_RecType"] = itemSubtype.recordType;
								SOarray["custparam_SerOut"] = itemSubtype.custitem_ebizserialout;
								SOarray["custparam_SerIn"] = itemSubtype.custitem_ebizserialin;
								response.sendRedirect('SUITELET', 'customscript_rf_pick_serialscan', 'customdeploy_rf_pick_serialscan_di', false, SOarray);
								return;
							}
						}

						else
						{		

							/*var invtholdflag = IsInvtonHold(ItemInternalId,EndLocInternalId,ClusNo);

					if(invtholdflag=='T')
					{
						SOarray["custparam_screenno"] = 'PICKCC1';	
						SOarray["custparam_error"]='CYCLE COUNT IS IN PROGRESS FOR THIS LOCATION. PLEASE DO THE LOCATION EXCEPTION TO COMPLETE THIS PICK';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
					}*/

							//nlapiLogExecution('ERROR', 'Clicked on Confirm', request.getParameter('cmdConfirm'));
							//Changed the code for auto containerization
							var vAutoContainerFlag='';
							if(detailTask !='' && detailTask =='F' && serialscanned == "true")
								vAutoContainerFlag='Y';  //for serial item
							else if(detailTask !='' && detailTask =='F' && itemflag =='N')
								vAutoContainerFlag='Y';//for normal item
							else
								vAutoContainerFlag='N'; // Set 'Y' to skip RF dialog for scanning container LP
							SOarray["custparam_autocont"] = vAutoContainerFlag;
							nlapiLogExecution('DEBUG', 'vAutoContainerFlag', vAutoContainerFlag);

							if(vAutoContainerFlag=='Y')
							{
								var RcId=RecordInternalId;
								var EndLocation = EndLocInternalId;
								var TotalWeight=0;
								var getReason=request.getParameter('custparam_enteredReason');
								var SORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordInternalId);
								var getContainerLPNo= SORec.getFieldValue('custrecord_container_lp_no');
								getLPContainerSize= SORec.getFieldText('custrecord_container');

								SOarray["custparam_newcontainerlp"] = getContainerLPNo;
								var vPickType = request.getParameter('hdnpicktype');
								var vBatchno = request.getParameter('hdnbatchno');
								var PickQty = ExpectedQuantity;
								var vActqty = ExpectedQuantity;
								var SalesOrderInternalId = ebizOrdNo;
								var getContainerSize = getLPContainerSize;
								var  vdono = DoLineId;
								nlapiLogExecution('DEBUG', 'WaveNo', WaveNo);
								nlapiLogExecution('DEBUG', 'getContainerLPNo', getContainerLPNo);
								var opentaskcount=0;

								nlapiLogExecution('DEBUG', 'opentaskcount', WaveNo);
								nlapiLogExecution('DEBUG', 'opentaskcount', getContainerLPNo);
								nlapiLogExecution('DEBUG', 'opentaskcount', vPickType);
								nlapiLogExecution('DEBUG', 'opentaskcount', SalesOrderInternalId);
								nlapiLogExecution('DEBUG', 'opentaskcount', vdono);
								opentaskcount=getOpenTasksCount(WaveNo,getContainerLPNo,vPickType,SalesOrderInternalId,vdono);
								nlapiLogExecution('DEBUG', 'opentaskcount', opentaskcount);
								//For fine tuning pick confirmation and doing autopacking in user event after last item
								var IsitLastPick='F';
								if(opentaskcount > 1)
									IsitLastPick='F';
								else
									IsitLastPick='T';

								nlapiLogExecution('DEBUG', 'getItem', ItemInternalId);

								ConfirmPickTask(ItemInternalId,getContainerSize,vdono,RcId,vActqty,getContainerLPNo,EndLocation,getReason,
										PickQty,vBatchno,IsitLastPick,SalesOrderInternalId);
								//if(RecCount > 1)
								if(opentaskcount > 1)
								{
									var SOFilters = new Array();
									SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

									if(WaveNo != null && WaveNo != "")
									{

										SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
									}

									if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
									{
										nlapiLogExecution('DEBUG', 'ClusNo inside If', ClusNo);

										SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
									}

									if(OrdName!=null && OrdName!="" && OrdName!= "null")
									{
										nlapiLogExecution('DEBUG', 'OrdNo inside If', OrdName);

										SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
									}

									if(ebizOrdNo!=null && ebizOrdNo!="" && ebizOrdNo!= "null")
									{
										nlapiLogExecution('DEBUG', 'OrdNo inside If', ebizOrdNo);

										SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ebizOrdNo));
									}

									var SOColumns = new Array();
									SOColumns[0] = new nlobjSearchColumn('custrecord_lpno');
									SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
									SOColumns[2] = new nlobjSearchColumn('custrecord_sku');
									SOColumns[3] = new nlobjSearchColumn('custrecord_expe_qty');
									SOColumns[4] = new nlobjSearchColumn('custrecord_actbeginloc');		
									SOColumns[5] = new nlobjSearchColumn('custrecord_skudesc');
									SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
									SOColumns[7] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
									SOColumns[8] = new nlobjSearchColumn('custrecord_invref_no');
									SOColumns[9] = new nlobjSearchColumn('custrecord_line_no');
									SOColumns[10] = new nlobjSearchColumn('custrecord_actbeginloc');
									SOColumns[11] = new nlobjSearchColumn('custrecord_batch_no');
									SOColumns[12] = new nlobjSearchColumn('custrecord_wms_location');
									SOColumns[13] = new nlobjSearchColumn('custrecord_comp_id');
									SOColumns[14] = new nlobjSearchColumn('name');
									SOColumns[15] = new nlobjSearchColumn('custrecord_container');
									SOColumns[16] = new nlobjSearchColumn('custrecord_ebiz_order_no');

									SOColumns[1].setSort(false);
									SOColumns[2].setSort(false);
									SOColumns[3].setSort(true);

									var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
									nlapiLogExecution('DEBUG', 'Results', SOSearchResults);
									if (SOSearchResults != null && SOSearchResults.length > 0) 
									{
										var SOSearchResult = SOSearchResults[0];
										if(SOSearchResults.length > 1)
										{
											var SOSearchnextResult = SOSearchResults[1];
											SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
											SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
											nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
										}
										SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
										SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
										SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
										SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
										SOarray["custparam_beginLocation"] = SOSearchResult.getText('custrecord_actbeginloc');
										SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
										SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
										SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
										SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
										SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
										SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
										SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
										SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
										SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
										SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
										SOarray["custparam_nooflocrecords"] = SOSearchResults.length;		
										SOarray["name"] =  SOSearchResult.getValue('name');
										SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
										SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');

									}
									if(BeginLocation != NextShowLocation)
									{ 
										//response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);
										response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_location', 'customdeploy_rf_cluspicking_location', false, SOarray);
										nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Location');
									}
									else if(ItemInternalId != NextItemInternalId)
									{ 
										//response.sendRedirect('SUITELET', 'customscript_rf_picking_item', 'customdeploy_rf_picking_item', false, SOarray);

										response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_item', 'customdeploy_rf_cluspicking_item', false, SOarray);
										nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Item');
									}
									else if(ItemInternalId == NextItemInternalId)
									{ 
										//response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);
										response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_summtask', 'customdeploy_rf_cluspicking_summtask', false, SOarray);
										nlapiLogExecution('DEBUG', 'Navigating To1', 'Picking Qty Confirm');
									}
								}
								else{

									nlapiLogExecution('DEBUG', 'Before AutoPacking 1 : Sales Order InternalId', SalesOrderInternalId);
									//For fine tuning pick confirmation and doing autopacking in user event after last item
									//AutoPacking(SalesOrderInternalId);
									/*nlapiLogExecution('DEBUG', 'Navigating To1', 'Foot print');
							response.sendRedirect('SUITELET', 'customscript_rf_picking_footprint', 'customdeploy_rf_picking_footprint_di', false, SOarray);*/

									//nlapiLogExecution('DEBUG', 'Before AutoPacking 1 : Sales Order InternalId', SalesOrderInternalId);
									//For fine tuning pick confirmation and doing autopacking in user event after last item
									//AutoPacking(SalesOrderInternalId);
									nlapiLogExecution('DEBUG', 'Navigating To1', 'Stage Location');
									SOarray["custparam_picktype"] =  'CL';
									nlapiLogExecution('DEBUG', 'SkipStaging', SkipStaging);
									if(SkipStaging!="Y")
									{
										response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);
										
									}
									else
									{
										AutoStageRecordcreaton(SOarray);
									}
									//response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);					
								}

							}	
							else
							{
								nlapiLogExecution('DEBUG', 'intodetailTask', '');
								//detail task functionality
								var detailTask='';
								if(request.getParameter('custparam_detailtask')!=null && request.getParameter('custparam_detailtask')!='')
								{
									detailTask=request.getParameter('custparam_detailtask');
								}
								if(detailTask!='' && detailTask=='F')
								{
									var SOFilters = new Array();

									SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
									SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK

									if(getWaveno != null && getWaveno != "")
									{
										SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(getWaveno)));
									}

									if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
									{
										SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
									}

									/*if(ItemInternalId != null && ItemInternalId != "")
									{
										SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_sku_no', null, 'is', ItemInternalId));
									}

									if(BeginLocation != null && BeginLocation != "")
									{
										SOFilters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', BeginLocation));
									}*/

									var SOColumns = new Array();				
									//SOColumns[0] = new nlobjSearchColumn('custrecord_sku',null,'group');
									SOColumns[0] = new nlobjSearchColumn('formulanumeric',null,'group').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)");
									SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc','group');
									SOColumns[2] = new nlobjSearchColumn('custrecord_sku',null,'group');
									SOColumns[3] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
									SOColumns[4] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
									SOColumns[5] = new nlobjSearchColumn('custrecord_skudesc',null,'group');
									SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');					
									SOColumns[7] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
									SOColumns[8] = new nlobjSearchColumn('custrecord_comp_id',null,'group');
									SOColumns[9] = new nlobjSearchColumn('custrecord_container_lp_no',null,'group');
									SOColumns[10] = new nlobjSearchColumn('custrecord_container',null,'group');
									SOColumns[11] = new nlobjSearchColumn('custrecord_batch_no',null,'group');

									SOColumns[0].setSort();	//SKU
									SOColumns[1].setSort();	//Location
									SOColumns[2].setSort();//Container LP
									var locreccount=request.getParameter('hdnRecCount');
									var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
									if (SOSearchResults != null && SOSearchResults.length > 0) {
										nlapiLogExecution('DEBUG', 'SOSearchResults', SOSearchResults.length);
										for(var k=0;k<SOSearchResults.length;k++)
										{

											getContainerLpNo = SOSearchResults[k].getValue('custrecord_container_lp_no',null,'group');
											getLPContainerSize = SOSearchResults[k].getText('custrecord_container',null,'group');
											getQtybyContainer = SOSearchResults[k].getValue('custrecord_expe_qty',null,'sum');
											vRemainQty=parseFloat(ExpectedQuantity)-parseFloat(getQtybyContainer);
											BeginLocationName=SOSearchResults[k].getText('custrecord_actbeginloc',null,'group');
											var 	getBeginLocationId=SOSearchResults[k].getValue('custrecord_actbeginloc',null,'group');
											//var 	getEndLocInternalId='';
											getItemInternalId = SOSearchResults[k].getValue('custrecord_ebiz_sku_no',null,'group');
											var nextlocation=SOSearchResults[k+1];var NextShowLocation='';var NextShowItem='';
											if(nextlocation!=null)
											{
												NextShowLocation=nextlocation.getText('custrecord_actbeginloc',null,'group');
												NextShowItem=nextlocation.getValue('custrecord_ebiz_sku_no',null,'group');
											}
											ExpectedQuantity=vRemainQty;
											nlapiLogExecution('DEBUG', 'vRemainQty', vRemainQty);
											nlapiLogExecution('DEBUG', 'vClusterNo', ClusNo);
											nlapiLogExecution('DEBUG', 'getExpectedQuantity', ExpectedQuantity);
											nlapiLogExecution('DEBUG', 'getContainerLpNo', getContainerLpNo);
											nlapiLogExecution('DEBUG', 'getItemInternalId', getItemInternalId);
											nlapiLogExecution('DEBUG', 'NextShowLocation', NextShowLocation);
											nlapiLogExecution('DEBUG', 'NextShowItem', NextShowItem);

											var openreplns = new Array();
											openreplns=getOpenReplns(ItemInternalId,EndLocInternalId);
											openinventory=getQOHForAllSKUsinPFLocation(EndLocInternalId,ItemInternalId);
											var invtqty;
											if(openinventory!=null && openinventory!='')
											{
												invtqty=openinventory[0].getValue('custrecord_ebiz_alloc_qty',null,'sum');

												if(invtqty==null || invtqty=='')
												{
													invtqty=0;
												}
											}
											nlapiLogExecution('DEBUG', 'ExpectedQuantity', ExpectedQuantity);
											nlapiLogExecution('DEBUG', 'invtqty', invtqty);
											if((parseFloat(ExpectedQuantity)>parseFloat(invtqty)) && (openreplns!=null && openreplns!=''))
											{
												if(openreplns!=null && openreplns!='')
												{
													for(var i=0; i< openreplns.length;i++)
													{

														var taskpriority=openreplns[i].getValue('custrecord_taskpriority');
														var expqty=openreplns[i].getValue('custrecord_expe_qty');
														if(expqty == null || expqty == '')
														{
															expqty=0;
														}	
														nlapiLogExecution('DEBUG', 'expqty',expqty);
														nlapiLogExecution('DEBUG', 'getQuantity',ExpectedQuantity);
														//if(parseFloat(expqty)>=parseFloat(ExpectedQuantity))
														//{
														nlapiSubmitField('customrecord_ebiznet_trn_opentask', openreplns[i].getId(), 'custrecord_taskpriority', '0');
														nlapiSubmitField('customrecord_ebiznet_trn_opentask', RecordInternalId, 'custrecord_skiptask', 'Y');	
														var SOarray=new Array();
														SOarray=buildTaskArray(getWaveno,ClusNo,OrdName,ebizOrdNo,null);
														SOarray["custparam_skipid"] = vSkipId;
														if(NextShowLocation!=null && NextShowLocation!='')
															SOarray["custparam_screenno"] = 'CL4EXP';
														else
															SOarray["custparam_screenno"] = 'CL4LOCEXP';	
														SOarray["custparam_error"]=st6;//'CONFIRM THE OPEN REPLENS';
														response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
														return;
														//}
													}
												}
											}
											else
											{
										nlapiLogExecution('DEBUG', 'EndLocation', EndLocation);
										nlapiLogExecution('DEBUG', 'getEndLocInternalId', getEndLocInternalId);
												ConfirmPickTasks(ClusNo,getContainerLpNo,getItemInternalId,getBeginLocationId,getEndLocInternalId,getContainerLpNo);
												if(NextShowLocation !='')
												{					
													if(BeginLocationName != NextShowLocation)
													{ 
														SOarray["custparam_beginlocationname"]= NextShowLocation;
														SOarray["custparam_prevscreen"] =  "summtask";
														nlapiLogExecution('DEBUG', 'Navigating To', 'Picking Location');
														response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_location', 'customdeploy_rf_cluspicking_location', false, SOarray);
														return;
													}
													else if((BeginLocationName == NextShowLocation) && getItemInternalId != NextShowItem)
													{ 
														//SOarray["custparam_item"] = NextShowItemname;
														SOarray["custparam_item"] = NextShowItem;
														SOarray["custparam_iteminternalid"] = NextShowItem;
														SOarray["custparam_nooflocrecords"] = parseFloat(locreccount)-1;
														SOarray["custparam_prevscreen"] =  "summtask";
														nlapiLogExecution('DEBUG', 'Navigating To', 'Picking Item');
														response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_item', 'customdeploy_rf_cluspicking_item', false, SOarray);
														return;
													}

												}
												else{
													SOarray["custparam_prevscreen"] =  "summtask";
													nlapiLogExecution('DEBUG', 'Navigating To', 'Stage Location');
													SOarray["custparam_picktype"] =  'CL';
													nlapiLogExecution('DEBUG', 'SkipStaging', SkipStaging);
													if(SkipStaging!="Y")
													{
														response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);
														
													}
													else
													{
														AutoStageRecordcreaton(SOarray);
													}
													//response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);					
												}
											}

										}
										//
									}
								}
								else
								{
									nlapiLogExecution('DEBUG', 'detailTask', detailTask);
									nlapiLogExecution('DEBUG', 'Navigating To1', 'detail screen');
									SOarray["custparam_prevscreen"] =  "summtask";
									response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_dettask', 'customdeploy_rf_cluspicking_dettask', false, SOarray);
									return;
								}
							}

						}//
					}
					else if (request.getParameter('cmdConfirm') == 'F11') {
						nlapiLogExecution('DEBUG', 'Confirm and stage location');
						var WaveNo = request.getParameter('hdnWaveNo');
						var RecordInternalId = request.getParameter('hdnRecordInternalId');
						var ContainerLPNo = request.getParameter('hdnContainerLpNo');
						var ExpectedQuantity = request.getParameter('hdnExpectedQuantity');
						var BeginLocation = request.getParameter('hdnBeginLocationId');
						var Item = request.getParameter('hdnItem');
						var ItemDescription = request.getParameter('hdnItemDescription');
						var ItemInternalId = request.getParameter('hdnItemInternalId');
						var DoLineId = request.getParameter('hdnDOLineId'); // internal id of DO
						var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
						var BeginLocationName = request.getParameter('hdnBeginBinLocation');
						var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
						var EndLocation = request.getParameter('hdnEnteredLocation');
						var OrderLineNo = request.getParameter('hdnOrderLineNo');
						var NextShowLocation=request.getParameter('hdnNextLocation');
						var OrdName=request.getParameter('hdnName');
						var ContainerSize=request.getParameter('hdnContainerSize');
						var ebizOrdNo=request.getParameter('hdnebizOrdNo');
						var NextItemInternalId=request.getParameter('hdnNextItemId');
						nlapiLogExecution('DEBUG', 'OrdName', OrdName);
						nlapiLogExecution('DEBUG', 'Next Location', NextShowLocation);
						var vPickType = request.getParameter('hdnpicktype');
						nlapiLogExecution('DEBUG', 'Before Item Fulfillment', 'Before Item Fulfillment');
						//ItemFulfillment(WaveNo, RecordInternalId, ContainerLPNo, ExpectedQuantity, BeginLocation, Item, ItemDescription, ItemInternalId, DoLineId, InvoiceRefNo, BeginLocationName, EndLocInternalId, EndLocation, OrderLineNo)

						var itemSubtype = nlapiLookupField('item', ItemInternalId, ['recordType', 'custitem_ebizserialin','custitem_ebizserialout', 'custitem_ebizbatchlot']);

						//if (itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.custitem_ebizserialin == 'T') {
						if (itemSubtype.recordType == 'serializedinventoryitem' || itemSubtype.custitem_ebizserialout == 'T') {
							SOarray["custparam_number"] = 0;
							SOarray["custparam_RecType"] = itemSubtype.recordType;
							SOarray["custparam_SerOut"] = itemSubtype.custitem_ebizserialout;
							SOarray["custparam_SerIn"] = itemSubtype.custitem_ebizserialin;
							response.sendRedirect('SUITELET', 'customscript_rf_pick_serialscan', 'customdeploy_rf_pick_serialscan_di', false, SOarray);
						}
						else
						{		

							//nlapiLogExecution('DEBUG', 'Clicked on Confirm', request.getParameter('cmdConfirm'));
							//Changed the code for auto containerization 
							var RcId=RecordInternalId;
							var EndLocation = EndLocInternalId;
							var TotalWeight=0;
							var getReason=request.getParameter('custparam_enteredReason');
							var SORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordInternalId);
							var getContainerLPNo= SORec.getFieldValue('custrecord_container_lp_no');
							getLPContainerSize= SORec.getFieldText('custrecord_container');

							SOarray["custparam_newcontainerlp"] = getContainerLPNo;

							var vBatchno = request.getParameter('hdnbatchno');
							var PickQty = ExpectedQuantity;
							var vActqty = ExpectedQuantity;
							var SalesOrderInternalId = ebizOrdNo;
							var getContainerSize = getLPContainerSize;
							var  vdono = DoLineId;
							nlapiLogExecution('DEBUG', 'WaveNo', WaveNo);
							nlapiLogExecution('DEBUG', 'getContainerLPNo', getContainerLPNo);
							var opentaskcount=0;
							nlapiLogExecution('DEBUG', 'opentaskcount', WaveNo);
							nlapiLogExecution('DEBUG', 'opentaskcount', getContainerLPNo);
							nlapiLogExecution('DEBUG', 'opentaskcount', vPickType);
							nlapiLogExecution('DEBUG', 'opentaskcount', SalesOrderInternalId);
							nlapiLogExecution('DEBUG', 'opentaskcount', vdono);
							opentaskcount=getOpenTasksCount(WaveNo,getContainerLPNo,vPickType,SalesOrderInternalId,vdono);
							nlapiLogExecution('DEBUG', 'opentaskcount', opentaskcount);
							//For fine tuning pick confirmation and doing autopacking in user event after last item
							var IsitLastPick='F';
							if(opentaskcount > 1)
								IsitLastPick='F';
							else
								IsitLastPick='T';

							nlapiLogExecution('DEBUG', 'getItem', ItemInternalId);

							ConfirmPickTask(ItemInternalId,getContainerSize,vdono,RcId,vActqty,getContainerLPNo,EndLocation,getReason,
									PickQty,vBatchno,IsitLastPick,SalesOrderInternalId);
							//if(RecCount > 1)

							nlapiLogExecution('DEBUG', 'Before AutoPacking 1 : Sales Order InternalId', SalesOrderInternalId);
							//For fine tuning pick confirmation and doing autopacking in user event after last item
							//AutoPacking(SalesOrderInternalId);
							nlapiLogExecution('DEBUG', 'Navigating To1', 'Stage Location');
							SOarray["custparam_picktype"] =  'CL';
							nlapiLogExecution('DEBUG', 'SkipStaging', SkipStaging);
							if(SkipStaging!="Y")
							{
								response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);
								
							}
							else
							{
								AutoStageRecordcreaton(SOarray);
							}
							//response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray);						 


						}
					}
					else 
					{
						if (request.getParameter('cmdSkip') == 'F12') {
							nlapiLogExecution('DEBUG', 'Clicked on Location SKIP', request.getParameter('cmdSkip'));

							var BeginLocation = request.getParameter('hdnBeginLocationId');
							var Item = request.getParameter('hdnItem');
							var ItemDescription = request.getParameter('hdnItemDescription');
							var ItemInternalId = request.getParameter('hdnItemInternalId');					
							var WaveNo = request.getParameter('hdnWaveNo');
							var ClusNo = request.getParameter('hdnClusterNo');
							var ebizOrdNo=request.getParameter('hdnebizOrdNo');
							var OrdName=request.getParameter('hdnName');
							var isLastpick='F';

							var NextShowLocation=request.getParameter('hdnNextLocation');
							SOarray["custparam_beginlocationname"]= NextShowLocation;

							nlapiLogExecution('DEBUG', 'WaveNo', WaveNo);
							nlapiLogExecution('DEBUG', 'ClusNo', ClusNo);
							nlapiLogExecution('DEBUG', 'BeginLocation', BeginLocation);
							nlapiLogExecution('DEBUG', 'ItemInternalId', ItemInternalId);
							nlapiLogExecution('DEBUG', 'NextShowLocation', NextShowLocation);

							var SOFilters = new Array();

							SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
							SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK

							if(WaveNo != null && WaveNo != "")
							{
								SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
							}

							if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
							{
								SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
							}

//							if(ItemInternalId != null && ItemInternalId != "")
//							{
//							SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_sku_no', null, 'is', ItemInternalId));
//							}

//							if(BeginLocation != null && BeginLocation != "")
//							{
//							SOFilters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', BeginLocation));
//							}

							var SOColumns = new Array();
//							SOColumns[0] = new nlobjSearchColumn('custrecord_skiptask');
							//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
							SOColumns[0] = new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)");
							//Code end as on 290414
							SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
							SOColumns[2] = new nlobjSearchColumn('custrecord_expe_qty');
							SOColumns[3] = new nlobjSearchColumn('custrecord_sku');
							SOColumns[4] = new nlobjSearchColumn('custrecord_skudesc');
							SOColumns[5] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
							SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
							SOColumns[7] = new nlobjSearchColumn('custrecord_invref_no');
							SOColumns[8] = new nlobjSearchColumn('custrecord_line_no');
							SOColumns[9] = new nlobjSearchColumn('custrecord_actbeginloc');
							SOColumns[10] = new nlobjSearchColumn('custrecord_batch_no');
							SOColumns[11] = new nlobjSearchColumn('custrecord_wms_location');
							SOColumns[12] = new nlobjSearchColumn('custrecord_comp_id');
							SOColumns[13] = new nlobjSearchColumn('name');
							SOColumns[14] = new nlobjSearchColumn('custrecord_container');
							SOColumns[15] = new nlobjSearchColumn('custrecord_ebiz_order_no');
							SOColumns[16] = new nlobjSearchColumn('custrecord_container_lp_no');
							SOColumns[17] = new nlobjSearchColumn('custrecord_ebiz_clus_no');
							SOColumns[18] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
							SOColumns[19] = new nlobjSearchColumn('custrecord_lpno');

							SOColumns[0].setSort();	
							SOColumns[1].setSort();		//	Location Pick Sequence
							SOColumns[3].setSort();	

							var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
							if (SOSearchResults != null && SOSearchResults.length > 0) {
								nlapiLogExecution('DEBUG', 'SOSearchResults', SOSearchResults.length);
								for(var k=0;k<SOSearchResults.length;k++)
								{
									var itemintrid = SOSearchResults[k].getValue('custrecord_ebiz_sku_no');
									var beginlocid = SOSearchResults[k].getValue('custrecord_actbeginloc');
									//var skiptask = SOSearchResults[k].getValue('custrecord_skiptask');
									var skiptask = SOSearchResults[k].getValue(SOColumns[0]);//case# 201411309
									nlapiLogExecution('DEBUG', 'skiptask', skiptask);
									if((BeginLocation==beginlocid) && (ItemInternalId==itemintrid))
									{
										if(k==SOSearchResults.length-1)
										{
											isLastpick='T';
										}
										if(skiptask=='' || skiptask==null)
										{
											nlapiSubmitField('customrecord_ebiznet_trn_opentask', SOSearchResults[k].getId(), 'custrecord_skiptask', '1');
										}
										else
										{
											var vskip = parseFloat(skiptask)+1;
											nlapiSubmitField('customrecord_ebiznet_trn_opentask', SOSearchResults[k].getId(), 'custrecord_skiptask', vskip);
										}
									}
								}
							}
							var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
							if (SOSearchResults != null && SOSearchResults.length > 0) {
								for(var k=0;k<SOSearchResults.length;k++)
								{
									if(k==SOSearchResults.length-1)
									{
										isLastpick='T';
									}
									nlapiLogExecution('DEBUG', 'isLastpick', isLastpick);
									/*if(skiptask=='' || skiptask==null)
							{
								nlapiSubmitField('customrecord_ebiznet_trn_opentask', SOSearchResults[k].getId(), 'custrecord_skiptask', '0');
							}*/

									nlapiLogExecution('DEBUG', 'isLastpick', isLastpick);
									if(isLastpick=='F')
									{
										SOarray["custparam_waveno"] = SOSearchResults[k].getValue('custrecord_ebiz_wave_no');
										SOarray["custparam_recordinternalid"] = SOSearchResults[k].getId();
										SOarray["custparam_expectedquantity"] = SOSearchResults[k].getValue('custrecord_expe_qty');
										SOarray["custparam_beginLocation"] = SOSearchResults[k].getValue('custrecord_actbeginloc');
										SOarray["custparam_item"] = SOSearchResults[k].getValue('custrecord_sku');
										SOarray["custparam_itemdescription"] = SOSearchResults[k].getValue('custrecord_skudesc');
										SOarray["custparam_iteminternalid"] = SOSearchResults[k].getValue('custrecord_ebiz_sku_no');
										SOarray["custparam_dolineid"] = SOSearchResults[k].getValue('custrecord_ebiz_cntrl_no');
										SOarray["custparam_invoicerefno"] = SOSearchResults[k].getValue('custrecord_invref_no');
										SOarray["custparam_orderlineno"] = SOSearchResults[k].getValue('custrecord_line_no');
										SOarray["custparam_beginlocationname"] = SOSearchResults[k].getText('custrecord_actbeginloc');
										SOarray["custparam_clusterno"] = SOSearchResults[k].getValue('custrecord_ebiz_clus_no');
										SOarray["custparam_batchno"] = SOSearchResults[k].getValue('custrecord_batch_no');
										SOarray["custparam_containerlpno"] = SOSearchResults[k].getValue('custrecord_container_lp_no');
										SOarray["custparam_whlocation"] = SOSearchResults[k].getValue('custrecord_wms_location');
										SOarray["custparam_whcompany"] = SOSearchResults[k].getValue('custrecord_comp_id');
										SOarray["custparam_containersize"] =  SOSearchResults[k].getValue('custrecord_container');
										SOarray["custparam_ebizordno"] =  SOSearchResults[k].getValue('custrecord_ebiz_order_no');
										SOarray["name"] =  SOSearchResults[k].getValue('name');
										SOarray["custparam_picktype"] =  'CL';
										SOarray["custparam_nextlocation"] = '';
										SOarray["custparam_nextiteminternalid"] = '';
										SOarray["custparam_nextitem"] = '';

										SOarray["custparam_type"] ="Cluster";



										response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_location', 'customdeploy_rf_cluspicking_location', false, SOarray);
										break;
									}
									else
									{
										var SOarray=new Array();
										response.sendRedirect('SUITELET', 'customscript_rf_picking_cluster_no', 'customdeploy_rf_picking_cluster_no_di', false, SOarray);
										return;
									}
								}
							}
							nlapiLogExecution('DEBUG', 'isLastpick', isLastpick);
							if(isLastpick=='F')
							{
								response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_location', 'customdeploy_rf_cluspicking_location', false, SOarray);
								return;
							}
							else
							{
								var SOarray=new Array();
								response.sendRedirect('SUITELET', 'customscript_rf_picking_cluster_no', 'customdeploy_rf_picking_cluster_no_di', false, SOarray);
								return;
							}

//							response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_location', 'customdeploy_rf_cluspicking_location', false, SOarray);
						}
						else
						{
							if (request.getParameter('cmdQtyException') == 'F11') {
								nlapiLogExecution('DEBUG', 'Clicked on Quantity Exception', request.getParameter('cmdQtyException'));
								response.sendRedirect('SUITELET', 'customscript_rf_putaway_qty_exception', 'customdeploy_rf_putaway_qty_exception_di', false, SOarray);
								return;
							}
							else
							{
								if (request.getParameter('cmdPickException') == 'F2') {
									nlapiLogExecution('DEBUG', 'Clicked on cmdPickException', request.getParameter('cmdPickException'));
									response.sendRedirect('SUITELET', 'customscript_rf_pickingqtyexception', 'customdeploy_rf_pickingqtyexception_di', false, SOarray);
									return;
								}
								if (request.getParameter('cmdlocexc') == 'F3') {
									nlapiLogExecution('ERROR', 'Clicked on cmdLocException', request.getParameter('cmdlocexc'));
									if(request.getParameter('custparam_detailtask')!=null && request.getParameter('custparam_detailtask')!='')
									{
										detailTask=request.getParameter('custparam_detailtask');
									}
									nlapiLogExecution('ERROR', 'detailTask', detailTask);
									nlapiLogExecution('ERROR', 'serialscanned', serialscanned);
									SOarray["custparam_detailtask"] = detailTask;
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_loc_exception', 'customdeploy_ebiz_rf_pick_loc_exc_di', false, SOarray);
									return;
								}
							}
						}
					}
				}
				nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
			}
			catch (e)  {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');

			}
		}
		else
		{
			SOarray["custparam_screenno"] = 'CL1';
			SOarray["custparam_error"] = 'CONTAINER# ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			return;

		}
	}
}

function ConfirmPickTasks(vClusterNo,getFetchedContainerNo,ItemNo,BeginLocation,EndLocation,getEnteredContainerNo)
{
	nlapiLogExecution('DEBUG', 'into ConfirmPickTasks function');
	nlapiLogExecution('DEBUG', 'vClusterNo',vClusterNo);
	nlapiLogExecution('DEBUG', 'getFetchedContainerNo',getFetchedContainerNo);
	nlapiLogExecution('DEBUG', 'ItemNo',ItemNo);
	nlapiLogExecution('DEBUG', 'BeginLocation',BeginLocation);
	nlapiLogExecution('DEBUG', 'EndLocation',EndLocation);

	var SOFilters = new Array();

	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK

	if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));
	}

	if(ItemNo != null && ItemNo != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_sku_no', null, 'is', ItemNo));
	}

	if(BeginLocation != null && BeginLocation != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', BeginLocation));
	}

	if(getFetchedContainerNo != null && getFetchedContainerNo != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', getFetchedContainerNo));
	}

	var SOColumns = new Array();				
	SOColumns[0] = new nlobjSearchColumn('custrecord_sku');
	SOColumns[1] = new nlobjSearchColumn('custrecord_expe_qty');
	SOColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
	SOColumns[3] = new nlobjSearchColumn('custrecord_skudesc');
	SOColumns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no');					
	SOColumns[5] = new nlobjSearchColumn('custrecord_wms_location');
	SOColumns[6] = new nlobjSearchColumn('custrecord_comp_id');
	SOColumns[7] = new nlobjSearchColumn('custrecord_container_lp_no');
	SOColumns[8] = new nlobjSearchColumn('custrecord_container');
	SOColumns[9] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	SOColumns[10] = new nlobjSearchColumn('custrecord_ebiz_order_no');

	SOColumns[0].setSort();	//SKU
	SOColumns[2].setSort();	//Location
	SOColumns[7].setSort();//Container LP
	SOColumns[9].setSort();

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	if (SOSearchResults != null && SOSearchResults.length > 0) 
	{	
		nlapiLogExecution('DEBUG', 'SOSearchResults length',SOSearchResults.length);
		var olddono='-1';
		var totalpickqty=0;
		for (var i = 0; i < SOSearchResults.length; i++)
		{
			var IsitLastPick='F';
			var RcId=SOSearchResults[i].getId();
			var PickQty = SOSearchResults[i].getValue('custrecord_expe_qty');
			//var EndLocation=SOSearchResults[i].getValue('custrecord_actbeginloc');
			var vdono=SOSearchResults[i].getValue('custrecord_ebiz_cntrl_no');
			var SalesOrderInternalId=SOSearchResults[i].getValue('custrecord_ebiz_order_no');

//			var opentaskcount=0;

//			opentaskcount=getOpenTasksCount(vClusterNo,getFetchedContainerNo);

//			nlapiLogExecution('DEBUG', 'opentaskcount', opentaskcount);

//			//For fine tuning pick confirmation and doing autopacking in user event after last item
//			var IsitLastPick='F';
//			if(opentaskcount > 1)
//			IsitLastPick='F';
//			else
//			IsitLastPick='T';

			if(i==SOSearchResults.length-1)
				IsitLastPick='T';

			UpdateRFOpenTask(RcId, PickQty, '', getEnteredContainerNo, '','', EndLocation,'',
					PickQty,IsitLastPick,'','',SalesOrderInternalId);

			nlapiLogExecution('DEBUG', 'olddono',olddono);
			nlapiLogExecution('DEBUG', 'vdono',vdono);
			nlapiLogExecution('DEBUG', 'PickQty',PickQty);
			nlapiLogExecution('DEBUG', 'totalpickqty',totalpickqty);
			nlapiLogExecution('DEBUG', 'i',i);

			if(olddono!=vdono)
			{
				if(olddono=='-1')
				{
					totalpickqty=PickQty;
					if(i==SOSearchResults.length-1)
						UpdateRFFulfillOrdLine(vdono,totalpickqty);
				}
				else
				{
					UpdateRFFulfillOrdLine(olddono,totalpickqty);
					totalpickqty=PickQty;
					if(i==SOSearchResults.length-1)
						UpdateRFFulfillOrdLine(vdono,totalpickqty);
				}

				olddono=vdono;
			}
			else
			{
				totalpickqty=totalpickqty+PickQty;
				if(i==SOSearchResults.length-1)
					UpdateRFFulfillOrdLine(olddono,totalpickqty);
			}

			//UpdateRFFulfillOrdLine(vdono,PickQty);
		}		
	}

	nlapiLogExecution('DEBUG', 'out of ConfirmPickTasks function');
}
function ConfirmPickTask(ItemInternalId,getContainerSize,vdono,RcId,vActqty,getContainerLPNo,EndLocation,getReason,PickQty,
		vBatchno,IsitLastPick,SalesOrderInternalId)
{
	var arrDims = getSKUCubeAndWeightforconfirm(ItemInternalId, 1);
	var itemCube = 0;
	var itemWeight=0;	

	if (arrDims[0] != "" && (!isNaN(arrDims[1]))){
		//	these are commented because here  PickQty is excepted qty but we have to cal based on act qty
		itemCube = (parseFloat(PickQty) * parseFloat(arrDims[0]));
		itemWeight = (parseFloat(PickQty) * parseFloat(arrDims[1]));//	


	} 
	nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemCube', itemCube);
	nlapiLogExecution('DEBUG', 'checkInPOSTRequest:itemweight', itemWeight);
	nlapiLogExecution('DEBUG', 'ContainerSize:ContainerSize', getContainerSize);		


	var ContainerCube;					
	var containerInternalId;
	var ContainerSize;
	var vRemaningqty=0;
	nlapiLogExecution('DEBUG', 'getContainerSize', getContainerSize);	

	if(getContainerSize=="" || getContainerSize==null)
	{
		getContainerSize=ContainerSize;
		nlapiLogExecution('DEBUG', 'ContainerSize', ContainerSize);	
	}
	nlapiLogExecution('DEBUG', 'getContainerSize', getContainerSize);	
	var arrContainerDetails = getContainerCubeAndTarWeight("",getContainerSize);					
	if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) {

		ContainerCube =  parseFloat(arrContainerDetails[0]);						
		containerInternalId = arrContainerDetails[3];
		ContainerSize=arrContainerDetails[4];
		TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));//now added
	} 
	nlapiLogExecution('DEBUG', 'ContainerSizeInternalId', containerInternalId);	
	nlapiLogExecution('DEBUG', 'ContainerSize', ContainerSize);	

	//UpdateOpenTask,fulfillmentorder
	nlapiLogExecution('DEBUG', 'vdono', vdono);	

	UpdateRFOpenTask(RcId,vActqty, containerInternalId,getContainerLPNo,itemCube,itemWeight,EndLocation,getReason,
			PickQty,IsitLastPick,SalesOrderInternalId);
	UpdateRFFulfillOrdLine(vdono,vActqty);

	//create new record in opentask,fullfillordline when we have qty exception
	//vRemaningqty = vDisplayedQty-vActqty;
	vRemaningqty = PickQty-vActqty;
	var recordcount=1;//it is iterator in GUI
	nlapiLogExecution('DEBUG', 'vDisplayedQty', PickQty);	
	nlapiLogExecution('DEBUG', 'vActqty', vActqty);	
	if(PickQty != vActqty)
	{
		nlapiLogExecution('DEBUG', 'inexception condition1', 'inexception condition');	
		CreateNewShortPick(RcId,PickQty,vActqty,vdono,recordcount,getContainerLPNo,TotalWeight,vBatchno);//have to check for wt and cube calculation
	}
}

function getOpenTasksCount(waveno,containerlpno,vPickType,SalesOrderInternalId,vdono)
{
	var openreccount=0;
	var SOFilters = new Array();
	var SOColumns = new Array();

	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK	 
	SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', containerlpno));	

	if(vPickType =='W')
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(waveno)));
	else if(vPickType =='O')
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', parseFloat(vdono)));
	else if(vPickType =='S')
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', parseFloat(SalesOrderInternalId)));
	else if(vPickType =='OW')
	{	
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(waveno)));
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', parseFloat(vdono)));
	}
	else if(vPickType =='OS')
	{	
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', parseFloat(SalesOrderInternalId)));
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', parseFloat(vdono)));
	}
	else if(vPickType =='SW')
	{	
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(waveno)));
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', parseFloat(SalesOrderInternalId)));
	}
	else if(vPickType =='ALL')
	{	
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(waveno)));
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', parseFloat(SalesOrderInternalId)));
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', parseFloat(vdono)));
	}

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
	if(SOSearchResults!=null && SOSearchResults!=''&& SOSearchResults.length>0)
		openreccount=SOSearchResults.length;	

	return openreccount;

}

function ItemFulfillment(WaveNo, RecordInternalId, ContainerLPNo, ExpectedQuantity, BeginLocation, Item, ItemDescription, ItemInternalId, DoLineId, InvoiceRefNo, BeginLocationName, EndLocInternalId, EndLocation, OrderLineNo){
	var now = new Date();
	var lineCnt = request.getLineItemCount('custpage_items');

	nlapiLogExecution('DEBUG', "Line Count" + lineCnt);
	var invtfalg = "";
	var ShortPick = "";
	var invnotfound = "";

	//getting time

	var a_p = "";
	var d = new Date();
	var curr_hour = now.getHours();
	if (curr_hour < 12) {
		a_p = "am";
	}
	else {
		a_p = "pm";
	}
	if (curr_hour == 0) {
		curr_hour = 12;
	}
	if (curr_hour > 12) {
		curr_hour = curr_hour - 12;
	}

	var curr_min = now.getMinutes();

	curr_min = curr_min + "";

	if (curr_min.length == 1) {
		curr_min = "0" + curr_min;
	}

	/*    
     for (var k = 1; k <= lineCnt; k++)
     {
     var chkVal = request.getLineItemValue('custpage_items', 'custpage_so', k);
     var SONo
     var ItemName = request.getLineItemValue('custpage_items', 'custpage_itemname', k);
     var itemno = request.getLineItemValue('custpage_items', 'custpage_itemno', k);
     var Lineno = request.getLineItemValue('custpage_items', 'custpage_lineno', k);
     var pickqty = request.getLineItemValue('custpage_items', 'custpage_ordqty', k);
     var vinvrefno = request.getLineItemValue('custpage_items', 'custpage_invrefno', k);

     var WaveNo = request.getParameter('hdnWaveNo');
     var RecordInternalId = request.getParameter('hdnRecordInternalId');
     var ContainerLPNo = request.getParameter('hdnContainerLpNo');
     var ExpectedQuantity = request.getParameter('hdnExpectedQuantity');
     var BeginLocation = request.getParameter('hdnBeginLocationId');
     var Item = request.getParameter('hdnItem');
     var ItemDescription = request.getParameter('hdnItemDescription');
     var ItemInternalId = request.getParameter('hdnItemInternalId');
     var DoLineId = request.getParameter('hdnDOLineId');	//
     var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
     var BeginLocationName = request.getParameter('hdnBeginBinLocation');
     var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
     var EndLocation = getEnteredLocation;

     //        var Recid = request.getLineItemValue('custpage_items', 'custpage_recid', k);
     //        nlapiLogExecution('DEBUG', "Recid " + Recid)
	 */
	//updating allocqty in opentask
	var OpenTaskTransaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordInternalId);
	OpenTaskTransaction.setFieldValue('custrecord_wms_status_flag', '8');//Statusflag='C'
	OpenTaskTransaction.setFieldValue('custrecord_act_qty', parseFloat(ExpectedQuantity).toFixed(4));
	OpenTaskTransaction.setFieldValue('custrecordact_end_date', DateStamp());
	OpenTaskTransaction.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
	OpenTaskTransaction.setFieldValue('custrecord_actendloc', EndLocInternalId);

	nlapiLogExecution('DEBUG', 'Done PICK Record Insertion :', 'Success');

	//Updating DO 

	var DOLine = nlapiLoadRecord('customrecord_ebiznet_ordline', DoLineId);
	DOLine.setFieldValue('custrecord_linestatus_flag', '1'); //Statusflag='C'
	DOLine.setFieldValue('custrecord_pickqty', parseFloat(ExpectedQuantity).toFixed(4));
	var SalesOrderInternalId = DOLine.getFieldValue('name');

	try {
		nlapiLogExecution('DEBUG', 'Main Sales Order Internal Id', SalesOrderInternalId);
		var TransformRec = nlapiTransformRecord('salesorder', SalesOrderInternalId, 'itemfulfillment');

		var SOLength = TransformRec.getLineItemCount('item');
		nlapiLogExecution('DEBUG', "SO Length", SOLength);

		/*
         for (var j = 0; j < SOLength; j++)
         {
         nlapiLogExecution('DEBUG', "Get Line Value", TransformRec.getLineItemValue('item', 'line', j + 1));
         TransformRec.setLineItemValue('item', 'itemfulfillment', j + 1, 'F');
         }
         TransformRec.setLineItemValue('item', 'itemfulfillment', OrderLineNo, 'T');
         TransformRec.setLineItemValue('item', 'quantity', OrderLineNo, ExpectedQuantity);
         var TransformRecId = nlapiSubmitRecord(TransformRec, true);
		 */
		for (var j = 1; j <= SOLength; j++) {
			nlapiLogExecution('DEBUG', "Get Line Value" + TransformRec.getLineItemValue('item', 'line', j + 1));

			var item_id = TransformRec.getLineItemValue('item', 'item', j);
			var itemrec = TransformRec.getLineItemValue('item', 'itemreceive', j);
			var itemLineNo = TransformRec.getLineItemValue('item', 'line', j);

			if (itemLineNo == OrderLineNo) {
				TransformRec.selectLineItem('item', j);
				TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'T');
				TransformRec.setCurrentLineItemValue('item', 'quantity', ExpectedQuantity);
				TransformRec.setCurrentLineItemValue('item', 'location', 1);
				TransformRec.commitLineItem('item');
			}
			else {
				TransformRec.selectLineItem('item', j);
				TransformRec.setCurrentLineItemValue('item', 'itemreceive', 'F');
				TransformRec.commitLineItem('item');

			}
			//TransformRec.setLineItemValue('item', 'itemfulfillment', j + 1, 'F');
		}
		//			invtRec.setFieldValue('custrecord_ebiz_inv_loc', '1');//WH Location.

		//			TransformRec.setFieldValue('custbody_ebiz_fulfillmentord',vdono) 
		var TransformRecId = nlapiSubmitRecord(TransformRec, true);
	} 

	catch (e) {
		if (e instanceof nlobjError) 
			lapiLogExecution('DEBUG', 'system error', e.getCode() + '\n' + e.getDetails());
		else 
			nlapiLogExecution('DEBUG', 'unexpected error', e.toString());
	}

	///Updating qty,qoh in inventory
	nlapiLogExecution('DEBUG', 'Invoice Ref No', InvoiceRefNo);
	var InventoryTranaction = nlapiLoadRecord('customrecord_ebiznet_createinv', InvoiceRefNo);

	var InvAllocQty = InventoryTranaction.getFieldValue('custrecord_ebiz_alloc_qty');
	var InvQOH = InventoryTranaction.getFieldValue('custrecord_ebiz_qoh');

	if (isNaN(InvAllocQty)) {
		InvAllocQty = 0;
	}
	InventoryTranaction.setFieldValue('custrecord_ebiz_qoh', (parseFloat(InvQOH) - parseFloat(ExpectedQuantity)).toFixed(4));
	InventoryTranaction.setFieldValue('custrecord_ebiz_alloc_qty', (parseFloat(InvAllocQty) - parseFloat(ExpectedQuantity)).toFixed(4));
	var InventoryTranactionId = nlapiSubmitRecord(InventoryTranaction, false, true);
	nlapiSubmitRecord(OpenTaskTransaction, true);
	nlapiSubmitRecord(DOLine, true);

	//    }

	//    var form = nlapiCreateForm('Pick  Confirmation');
	//    form.addField('custpage_label', 'label', '<h2>Pick Confirmation Successful</h2>');

	//    response.writePage(form);
}
/**
 * 
 * @param RcId
 * @param PickQty
 * @param containerInternalId
 * @param getEnteredContainerNo
 * @param itemCube
 * @param itemWeight
 * @param EndLocation
 */
function UpdateRFOpenTask(RcId, PickQty, containerInternalId, getEnteredContainerNo, itemCube,
		itemWeight, EndLocation,vReason,ActPickQty,IsItLastPick,SalesOrderInternalId)
{
	nlapiLogExecution('DEBUG', 'into UpdateRFOpenTask containerlp: ', getEnteredContainerNo);
	nlapiLogExecution('DEBUG', 'into UpdateRFOpenTask EndLocation: ', EndLocation);
	var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RcId);
	var vinvrefno=transaction.getFieldValue('custrecord_invref_no');
	transaction.setFieldValue('custrecord_wms_status_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')
	if(PickQty!= null && PickQty !="")
		transaction.setFieldValue('custrecord_act_qty', parseFloat(PickQty).toFixed(4));
	if(containerInternalId!= null && containerInternalId !="")
		transaction.setFieldValue('custrecord_container', containerInternalId);
	if(EndLocation!=null && EndLocation!="")
		transaction.setFieldValue('custrecord_actendloc', EndLocation);	
	if(itemWeight!=null && itemWeight!="")
		transaction.setFieldValue('custrecord_total_weight', parseFloat(itemWeight).toFixed(4));
	if(itemCube!=null && itemCube!="")
		transaction.setFieldValue('custrecord_totalcube', parseFloat(itemCube).toFixed(4));	
	if(getEnteredContainerNo!=null && getEnteredContainerNo!="")
		transaction.setFieldValue('custrecord_container_lp_no', getEnteredContainerNo);
	transaction.setFieldValue('custrecord_act_end_date', DateStamp());
	transaction.setFieldValue('custrecord_actualendtime',TimeStamp());
	if(vReason!=null && vReason!="")
		transaction.setFieldValue('custrecord_notes', vReason);	
	var currentContext = nlapiGetContext();  
	var currentUserID = currentContext.getUser();
	transaction.setFieldValue('custrecord_upd_ebiz_user_no',currentUserID);
	nlapiLogExecution('DEBUG', 'IsItLastPick: ', IsItLastPick);
	transaction.setFieldValue('custrecord_device_upload_flag',IsItLastPick);
	var vemployee = request.getParameter('custpage_employee');
	nlapiLogExecution('DEBUG', 'vemployee :', vemployee);
	nlapiLogExecution('DEBUG', 'currentUserID :', currentUserID);
//	if (vemployee != null && vemployee != "") 
//	{
//	transaction.setFieldValue('custrecord_taskassignedto',vemployee);
//	} 
//	else if (currentUserID != null && currentUserID != "") 
//	{
//	transaction.setFieldValue('custrecord_taskassignedto',currentUserID);
//	}

	nlapiLogExecution('DEBUG', 'Updating RF Open Task Record Id: ', RcId);
	nlapiSubmitRecord(transaction, false, true);
	nlapiLogExecution('DEBUG', 'Updated RF Open Task successfully');

	deleteAllocations(vinvrefno,PickQty,getEnteredContainerNo,ActPickQty,SalesOrderInternalId);

}

/**
 * This function clear the allocation qty from inventory.
 * @param invrefno
 * @param PickQty
 */
function deleteAllocations(invrefno,pickqty,contlpno,ActPickQty,SalesOrderInternalId)
{
	nlapiLogExecution('DEBUG', 'Into deleteAllocations (Inv Ref NO)',invrefno);

	try
	{
		var scount=1;
		var invtrecid;
		var InvQOH = 0;
		LABL1: for(var i=0;i<scount;i++)
		{	

			nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);
			try
			{
				var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
				var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
				InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');

				if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {
					Invttran.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(Invallocqty)- parseFloat(ActPickQty)).toFixed(4));
				}

				Invttran.setFieldValue('custrecord_ebiz_qoh',(parseFloat(InvQOH) - parseFloat(ActPickQty)).toFixed(4)); 
				Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
				Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

				invtrecid = nlapiSubmitRecord(Invttran, false, true);
				nlapiLogExecution('DEBUG', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);

			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 

				nlapiLogExecution('Debug', 'Exception in RF Clust Picking : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}
		if(invtrecid!=null && invtrecid!='' && parseFloat(pickqty)>0)
			CreateSTGInvtRecord(invtrecid, contlpno,pickqty,SalesOrderInternalId);

		if((parseFloat(InvQOH) - parseFloat(ActPickQty)) == 0)
		{		
			nlapiLogExecution('DEBUG', 'Deleting record from inventory if QOH becomes zero', invrefno);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
		}
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in deleteAllocations',exp);
	}		
}


function UpdateRFFulfillOrdLine(vdono,vActqty)
{
	var pickqtyfinal =0;
	nlapiLogExecution('DEBUG', 'into UpdateRFFulfillOrdLine : ', vdono);

	if(vdono !=null && vdono!=''){
		var doline = nlapiLoadRecord('customrecord_ebiznet_ordline',vdono);
		doline.setFieldValue('custrecord_linestatus_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')
		doline.setFieldValue('custrecord_upddate', DateStamp());

		var oldpickQty=doline.getFieldValue('custrecord_pickqty');

		if(isNaN(oldpickQty) || oldpickQty == null || oldpickQty == '')
			oldpickQty=0;

		nlapiLogExecution('DEBUG', 'parseFloat(oldpickQty) in UpdateRFFulfillOrdLine', parseFloat(oldpickQty));	

		pickqtyfinal=parseFloat(oldpickQty)+parseFloat(vActqty);
		nlapiLogExecution('DEBUG', 'pickqtyfinal in blocklevel', pickqtyfinal);	
		doline.setFieldValue('custrecord_pickqty', parseFloat(pickqtyfinal).toFixed(4));
		doline.setFieldValue('custrecord_pickgen_qty', parseFloat(pickqtyfinal).toFixed(4));

		nlapiSubmitRecord(doline, false, true);
	}
}

/**
 * This function creats outbound staging inventory
 * @param invtrecid
 * @param vContLp
 * @param vqty
 */

function CreateSTGInvtRecord(invtrecid, vContLp,vqty,SalesOrderInternalId) 
{
	nlapiLogExecution('DEBUG', 'Into CreateSTGInvtRecord (Container LP)',vContLp);

	try
	{
		var stgmInvtRec = nlapiCopyRecord('customrecord_ebiznet_createinv',invtrecid);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_lp', vContLp);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(vqty).toFixed(4));
		stgmInvtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(vqty).toFixed(4));
		stgmInvtRec.setFieldValue('custrecord_ebiz_alloc_qty', '0');
		stgmInvtRec.setFieldValue('custrecord_wms_inv_status_flag', '18');	//FLAG.INVENTORY.OUTBOUND('O') 		
		stgmInvtRec.setFieldValue('custrecord_invttasktype', '3'); //Task Type - PICK
		stgmInvtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
		stgmInvtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');		
		if(SalesOrderInternalId!=null && SalesOrderInternalId!='')
			stgmInvtRec.setFieldValue('custrecord_ebiz_transaction_no', SalesOrderInternalId);	
		nlapiSubmitRecord(stgmInvtRec, false, true);
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG', 'Exception in CreateSTGInvtRecord',exp);
	}

	nlapiLogExecution('DEBUG', 'Out of CreateSTGInvtRecord');
}

function getOpenReplns(item,location)
{
	nlapiLogExecution('DEBUG', 'Into getOpenReplns');
	var PickFaceSearchResults= new Array();
	var PickFaceFilters = new Array();
	var PickFaceColumns = new Array();

	PickFaceFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [8])); // Task Type - RPLN	 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', item)); 
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_actendloc', null, 'is', location));	
	PickFaceFilters.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'isempty'));

	PickFaceColumns[0] = new nlobjSearchColumn('custrecord_expe_qty');
	PickFaceColumns[1] = new nlobjSearchColumn('custrecord_taskpriority');

	PickFaceSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, PickFaceFilters, PickFaceColumns);



	return PickFaceSearchResults;

}

function getQOHForAllSKUsinPFLocation(location,item){

	nlapiLogExecution('DEBUG','location',location);
	nlapiLogExecution('DEBUG','item',item);

	var filters = new Array();
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', location));	
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', item));
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', ['19','3']));

	var columns = new Array();	
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_alloc_qty',null,'sum');

	var pfSKUInvList = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);

	return pfSKUInvList;
}

function buildTaskArray(WaveNo,ClusNo,OrdNo,vSOId,vZoneId)
{
	var SOarray = new Array();
	var SOFilters = new Array();
	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

	if(WaveNo != null && WaveNo != "")
	{
		nlapiLogExecution('DEBUG', 'WaveNo inside If', WaveNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
	}

	if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
	{
		nlapiLogExecution('DEBUG', 'ClusNo inside If', ClusNo);

		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
	}

	if(OrdNo!=null && OrdNo!="" && OrdNo!= "null")
	{
		nlapiLogExecution('DEBUG', 'OrdNo inside If', OrdNo);

		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdNo));
	}

	if(vSOId!=null && vSOId!="" && vSOId!= "null")
	{
		nlapiLogExecution('DEBUG', 'SO Id inside If', vSOId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vSOId));
	}

	if(vZoneId!=null && vZoneId!="" && vZoneId!= "null")
	{
		nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
	}
	SOarray["custparam_ebizordno"] = vSOId;
	var SOColumns = new Array();
	//SOColumns[0] = new nlobjSearchColumn('custrecord_skiptask');
	SOColumns[0] = new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)");
	SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
	SOColumns[2] = new nlobjSearchColumn('custrecord_sku');
	SOColumns[3] = new nlobjSearchColumn('custrecord_expe_qty');
	SOColumns[4] = new nlobjSearchColumn('custrecord_actbeginloc');		
	SOColumns[5] = new nlobjSearchColumn('custrecord_skudesc');
	SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	SOColumns[7] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	SOColumns[8] = new nlobjSearchColumn('custrecord_invref_no');
	SOColumns[9] = new nlobjSearchColumn('custrecord_line_no');
	SOColumns[10] = new nlobjSearchColumn('custrecord_actbeginloc');
	SOColumns[11] = new nlobjSearchColumn('custrecord_batch_no');
	SOColumns[12] = new nlobjSearchColumn('custrecord_wms_location');
	SOColumns[13] = new nlobjSearchColumn('custrecord_comp_id');
	SOColumns[14] = new nlobjSearchColumn('name');
	SOColumns[15] = new nlobjSearchColumn('custrecord_container');
	SOColumns[16] = new nlobjSearchColumn('custrecord_ebiz_order_no');
	SOColumns[17] = new nlobjSearchColumn('custrecord_ebizzone_no');
	SOColumns[18] = new nlobjSearchColumn('custrecord_lpno');

	SOColumns[0].setSort(true);
	SOColumns[1].setSort(false);
	SOColumns[2].setSort(false);

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

	if (SOSearchResults != null && SOSearchResults.length > 0) {

		var SOSearchResult = SOSearchResults[0];
		if(SOSearchResults.length > 1)
		{
			var SOSearchnextResult = SOSearchResults[1];
			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
			nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
		}
		else
		{
			var SOSearchnextResult = SOSearchResults[0];
			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
			nlapiLogExecution('DEBUG', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
			nlapiLogExecution('DEBUG', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
		}

		SOarray["custparam_waveno"] = WaveNo;
		SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
		SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
		SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
		SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
		SOarray["custparam_endlocinternalid"] = SOSearchResult.getValue('custrecord_actbeginloc');		
		SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
		SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
		SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
		SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
		SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
		SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
		SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
		SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
		SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
		SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
		SOarray["custparam_noofrecords"] = SOSearchResults.length;		
		SOarray["name"] =  SOSearchResult.getValue('name');
		SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
		SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
		}
		else
			SOarray["custparam_ebizzoneno"] = '';
	}

	return SOarray;
}


function IsInvtonHold(ItemInternalId,EndLocInternalId,ClusNo)
{
	nlapiLogExecution('ERROR', 'Into IsInvtonHold');
	nlapiLogExecution('ERROR', 'ItemInternalId', ItemInternalId);
	nlapiLogExecution('ERROR', 'EndLocInternalId', EndLocInternalId);

	var holdflag='F';

	var SOFilters = new Array();

	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK
	var LPArray= new Array();
	//Case # 20126964� Start
	if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
	{
		nlapiLogExecution('DEBUG', 'ClusNo inside If', ClusNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
		SOFilters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', ItemInternalId));
		SOFilters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', EndLocInternalId));


		//SOFilters.push(new nlobjSearchFilter('internalid', null, 'is', getRecordInternalId));

		var SOColumns = new Array();


		SOColumns[0] = new nlobjSearchColumn('custrecord_from_lp_no');

		var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

		if(SOSearchResults!=null && SOSearchResults.length>0)
		{
			for(var k1=0;k1<SOSearchResults.length;k1++)
			{
				LPArray.push(SOSearchResults[k1].getValue('custrecord_from_lp_no'));
			}
		}
	}
	//Case # 20126964� End

	var SOFilters = new Array();
	var SOColumns = new Array();

	SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', ItemInternalId)); //Status Flag - Picks Generated
	SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', EndLocInternalId)); // Task Type - PICK	 
	SOFilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [19]));

	SOColumns[0] = new nlobjSearchColumn('custrecord_ebiz_cycl_count_hldflag' ); 
	SOColumns[1] = new nlobjSearchColumn('custrecord_ebiz_invholdflg' ); 
	SOColumns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp' ); 
	var searchresults = nlapiSearchRecord( 'customrecord_ebiznet_createinv', null, SOFilters, SOColumns ); 
	if(searchresults!=null && searchresults!='')
	{
		for(var z=0; z<searchresults.length;z++) 
		{										
			var cyclholdflag=searchresults[z].getValue('custrecord_ebiz_cycl_count_hldflag');
			var invtholdflag=searchresults[z].getValue('custrecord_ebiz_invholdflg');
			//Case # 20126964� Start
			var lp=searchresults[z].getValue('custrecord_ebiz_inv_lp');
			if(invtholdflag=='T' && (LPArray.indexOf(lp)!=-1))
			{//Case # 20126964� End
				holdflag='T';
				nlapiLogExecution('ERROR', 'Out of IsInvtonHold',holdflag);
				return holdflag;
			}
		}
	}

	nlapiLogExecution('ERROR', 'Out of IsInvtonHold',holdflag);

	return holdflag;	
}



function AutoStageRecordcreaton(SOarray)
{
	try
	{
		nlapiLogExecution('ERROR', 'AutoStageRecordcreaton',SOarray);
		var stgDirection="OUB";
		var fastpick = SOarray['custparam_fastpick'];
		nlapiLogExecution('ERROR', 'fastpick',fastpick);
		var getWaveno = SOarray['custparam_waveno'];
		var getRecordInternalId = SOarray['custparam_recordinternalid'];
		var getContainerLpNo = SOarray['custparam_newcontainerlp'];
		var getExpectedQuantity = SOarray['custparam_expectedquantity'];
		var getBeginLocationId = SOarray['custparam_beginLocation'];
		var getItemName = SOarray['custparam_itemname'];
		var getItemDescription = SOarray['custparam_itemdescription'];
		var getItemInternalId = SOarray['custparam_iteminternalid'];
		var getDOLineId = SOarray['custparam_dolineid'];
		var getInvoiceRefNo = SOarray['custparam_invoicerefno'];
		var getEndLocInternalId = SOarray['custparam_endlocinternalid'];
		var getEnteredLocation = SOarray['custparam_endlocation'];
		var getBeginBinLocation = SOarray['custparam_beginlocationname'];
		var getOrderLineNo = SOarray['custparam_orderlineno'];		
		var getSerialNo = SOarray['custparam_serialno'];
		var vClusterNo = SOarray['custparam_clusterno'];		
		var name = SOarray['name'];
		var pickType=SOarray['custparam_picktype'];	
		var vSOId=SOarray['custparam_ebizordno'];
		var RecordCount=SOarray['custparam_noofrecords'];
		var NextItemInternalId=SOarray['custparam_nextiteminternalid'];
		var ContainerSize=SOarray['custparam_containersize'];
		var getOrderNo=SOarray['custparam_ebizordno'];
		var whLocation = SOarray['custparam_whlocation'];
		var getZoneNo=SOarray['custparam_ebizzoneno'];
		var vBatchno = SOarray['custparam_batchno'];
		var getItem = '';
		var vondemandstage = SOarray['custparam_ondemandstage'];
		var vSkipId=0;
		if(SOarray['custparam_skipid'] !=null &&  SOarray['custparam_skipid'] !="")
			vSkipId=SOarray['custparam_skipid'];
		//Code Added for Displaying the Location Name
//		*****************************************
		var SOarray = new Array();
		var WaveNo = getWaveno;
		var RecordInternalId = getRecordInternalId;
		var ContainerLPNo = getContainerLpNo;			
		var ExpectedQuantity = getExpectedQuantity;
		var BeginLocation = getBeginLocationId;
		var Item = getItemName;
		var ItemName = getItemName;
		var ItemDescription = getItemDescription ;
		var ItemInternalId = getItemInternalId ;
		var DoLineId = getDOLineId ; // internal id of DO
		var InvoiceRefNo = getInvoiceRefNo ;
		var BeginLocationName = getBeginBinLocation ;
		var EndLocInternalId = getEndLocInternalId ;
		var EndLocation = getEnteredLocation;
		var OrderLineNo = getOrderLineNo ;
		var SerialNo = getSerialNo ;
		var vPickType = pickType;
		SOarray["custparam_ebizordno"] = vSOId;
		SOarray["custparam_picktype"] = vPickType;
		var vSite;
		var vCompany;
		var stgDirection="OUB";
		var vDoname=name;
		var vdono=getDOLineId;
		var vPrevinvrefno=getInvoiceRefNo ;
		//***********************************

		var Carrier;
		var Site;
		var Company;
		var stgOutDirection="OUB";
		var carriertype='';
		var wmscarrier='';
		var customizetype='';
		var ordertype='';
		var customer='';


		nlapiLogExecution('DEBUG', 'getDOLineId',getDOLineId); 
		nlapiLogExecution('DEBUG', 'WaveNo ', getWaveno);


		if(getDOLineId!=null && getDOLineId!='')
		{
			var columns = new Array();
			var filters = new Array();

			filters.push(new nlobjSearchFilter('internalid', null, 'is',getDOLineId));

			columns[0] = new nlobjSearchColumn('custrecord_do_wmscarrier');	
			columns[1] = new nlobjSearchColumn('custrecord_do_carrier');				
			columns[2] = new nlobjSearchColumn('custrecord_ebiz_wave');
			columns[3] = new nlobjSearchColumn('custrecord_ebiz_ordline_customizetype');
			columns[4] = new nlobjSearchColumn('custrecord_do_order_type');
			columns[5] = new nlobjSearchColumn('custrecord_do_customer');

			var searchresultsord = nlapiSearchRecord('customrecord_ebiznet_ordline', null, filters, columns);
			if(searchresultsord!=null && searchresultsord!='' && searchresultsord.length>0)
			{
				Carrier = searchresultsord[0].getValue('custrecord_do_carrier');
				customizetype = searchresultsord[0].getValue('custrecord_ebiz_ordline_customizetype');
				ordertype = searchresultsord[0].getValue('custrecord_do_order_type');
				customer = searchresultsord[0].getValue('custrecord_do_customer');
				nlapiLogExecution('DEBUG', 'Carrier',Carrier); 
				nlapiLogExecution('DEBUG', 'customizetype',customizetype); 
				nlapiLogExecution('DEBUG', 'ordertype',ordertype); 
				if(Carrier!=null && Carrier!='')
				{
					var filterscarr = new Array();
					var columnscarr = new Array();

					filterscarr.push(new nlobjSearchFilter('custrecord_carrier_nsmethod', null, 'is',Carrier));
					filterscarr.push(new nlobjSearchFilter('isinactive', null, 'is','F'));

					columnscarr[0] = new nlobjSearchColumn('custrecord_carrier_type');	
					columnscarr[1] = new nlobjSearchColumn('custrecord_carrier_service_level');	
					columnscarr[2] = new nlobjSearchColumn('id');

					var searchresultscarr = nlapiSearchRecord('customrecord_ebiznet_carrier', null, filterscarr, columnscarr);
					if(searchresultscarr!=null && searchresultscarr!='' && searchresultscarr.length>0)
					{
						carriertype = searchresultscarr[0].getValue('custrecord_carrier_type');
						wmscarrier = searchresultscarr[0].getId();
					}
				}

				if(Carrier==null || Carrier=='')
					wmscarrier=searchresultsord[0].getValue('custrecord_do_wmscarrier');
				if(getWaveno==null || getWaveno=='')
					getWaveno=searchresultsord[0].getValue('custrecord_ebiz_wave');
			}
		}

		nlapiLogExecution('DEBUG', 'carriertype',carriertype); 
		nlapiLogExecution('DEBUG', 'wmscarrier',wmscarrier); 
		nlapiLogExecution('DEBUG', 'WaveNo ', getWaveno);

		var stgLocation = '';
		var FetchBinLocation='';

		var stgLocationresults = GetStageLocationFromSTGM(name,getWaveno);
		if(stgLocationresults!=null && stgLocationresults!='' && stgLocationresults.length>0)
		{
			stgLocation = stgLocationresults[0].getValue('custrecord_actbeginloc');
			FetchBinLocation = stgLocationresults[0].getText('custrecord_actbeginloc');
		}

		nlapiLogExecution('DEBUG', 'Stage Location from STGM ', FetchBinLocation);
		nlapiLogExecution('DEBUG', 'Stage Location Id from STGM ', stgLocation);

		if(stgLocation==null || stgLocation=='')
		{

			stgLocation = GetPickStageLocation(getItemInternalId, wmscarrier, whLocation, Company,
					stgOutDirection,carriertype,customizetype,null,ordertype,customer);


			nlapiLogExecution('DEBUG', 'After Getting StagingLocation in page load::',stgLocation);
			var FetchBinLocation;
			if(stgLocation!=null && stgLocation!=-1)
			{
				var collocGroup = new Array();
				var filtersLocGroup = new Array();
				collocGroup[0] = new nlobjSearchColumn('name');
				filtersLocGroup.push(new nlobjSearchFilter('internalid', null, 'is',stgLocation));
				var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
				if(searchresultsloc != null && searchresultsloc != '')
					FetchBinLocation=searchresultsloc[0].getValue('name');
			}

			nlapiLogExecution('DEBUG', 'After Getting StagingLocation Name in page load::',FetchBinLocation);
		}
		var vCarrier = Carrier;
		var vstageLoc=stgLocation;
		var voldcontainer='';
		var vZoneId=getZoneNo;
		var vhdncarrier=wmscarrier;
		var vhdncarriertype=carriertype;
		var vhdncustomizetype=customizetype;
		var WHLocation = whLocation;
		var Stagedet=3;
		nlapiLogExecution('ERROR', 'Stagedet', Stagedet);

		var taskfilters = new Array();

		taskfilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'is', '3'));//Task Type - PICK
		taskfilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', ['8','28']));// Pick Confirm and Pack Complete
		if(WaveNo!=null && WaveNo!='')
			taskfilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));	
		if (vPickType!='CL') {
			if(Stagedet==3)//Carrier level
			{
				if(ContainerLPNo!=null && ContainerLPNo!='' && ContainerLPNo!='null')
					taskfilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', ContainerLPNo));
			}
		}
		if (vPickType=='CL') {
			taskfilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));	
		}

		var taskcolumns = new Array();
		taskcolumns[0] = new nlobjSearchColumn('custrecord_container_lp_no');
		taskcolumns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
		taskcolumns[2] = new nlobjSearchColumn('custrecord_invref_no');
		taskcolumns[3] = new nlobjSearchColumn('custrecord_expe_qty');
		taskcolumns[4] = new nlobjSearchColumn('custrecord_ebiz_order_no');
		taskcolumns[5] = new nlobjSearchColumn('name');
		taskcolumns[0].setSort();

		var tasksearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, taskfilters, taskcolumns);

		if(tasksearchresults!=null)
		{
			for (var i = 0; i < tasksearchresults.length; i++)
			{
				ContainerLPNo = tasksearchresults[i].getValue('custrecord_container_lp_no');
				Item = tasksearchresults[i].getValue('custrecord_ebiz_sku_no');
				var salesorderintrid = tasksearchresults[i].getValue('custrecord_ebiz_order_no');
				vDoname = tasksearchresults[i].getValue('name');

				nlapiLogExecution('DEBUG', 'voldcontainer', voldcontainer);
				nlapiLogExecution('DEBUG', 'ContainerLPNo', ContainerLPNo);
				nlapiLogExecution('DEBUG', 'salesorderintrid', salesorderintrid);
				nlapiLogExecution('DEBUG', 'vondemandstage', vondemandstage);

				if(voldcontainer!=ContainerLPNo)
				{
					CreateSTGMRecord(ContainerLPNo, WaveNo, RecordInternalId, vDoname, vCompany, WHLocation, vdono, 
							Item, stgDirection, vCarrier, vSite,stgLocation,vClusterNo,salesorderintrid,vondemandstage);

					voldcontainer=ContainerLPNo;
				}
			}
		}	
		if(vondemandstage == 'Y')
		{
			var SOarray = new Array();
			SOarray["custparam_fastpick"] = fastpick;
			response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, SOarray);
		}
		else
		{
			var vFootPrintFlag='N'; // Put Y to navigate to Dynacraft specific screen for scanning 'No. of foot prints/Pallets'
			if(vFootPrintFlag != 'Y')
			{
				if(vPickType != null && vPickType != '' && vPickType!='CL')
				{

					nlapiLogExecution('DEBUG', 'Inside Both Wave No condition', parseFloat(WaveNo));
					nlapiLogExecution('DEBUG', 'Inside Both Order No condition', DoLineId);

					var SOFilters = new Array();


					SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated					
					SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK
					SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_pickinprogress', null, 'is', 'F'));

					if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && WaveNo != null && WaveNo != "")
					{
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
					} 
					if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && vDoname!=null && vDoname!="" && vDoname!= "null")
					{
						nlapiLogExecution('DEBUG', 'OrdNo inside If', vDoname);
						SOFilters.push(new nlobjSearchFilter('name', null, 'is', vDoname));
					}
					if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && vSOId!=null && vSOId!="" && vSOId!= "null")
					{
						nlapiLogExecution('DEBUG', 'SO Id inside If', vSOId);
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vSOId));
					}
					if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
					{
						nlapiLogExecution('DEBUG', 'Zone # inside If', vZoneId);
						SOFilters.push(new nlobjSearchFilter('custrecord_ebizzone_no', null, 'is', vZoneId));			
					}

					if((vPickType.indexOf('C') != -1 || vPickType=='ALL') && ContainerLPNo!=null && ContainerLPNo!="" && ContainerLPNo!= "null")
					{
						nlapiLogExecution('DEBUG', 'Carton # inside If', ContainerLPNo);
						SOFilters.push(new nlobjSearchFilter('custrecord_container_lp_no', null, 'is', ContainerLPNo));			
					}


					var SOColumns = new Array();
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
					SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
					SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
					SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
					SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
					SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
					SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
					SOColumns.push(new nlobjSearchColumn('name'));
					SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
					SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_wave_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));

					SOColumns[0].setSort();
					SOColumns[1].setSort();
					SOColumns[2].setSort();
					SOColumns[3].setSort();
					SOColumns[4].setSort();
					SOColumns[5].setSort(true);

					var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
					nlapiLogExecution('DEBUG', 'SOSearchResults', SOSearchResults);
					if (SOSearchResults != null && SOSearchResults.length > 0 ) {
						nlapiLogExecution('DEBUG', 'into Search Results by Wave No', WaveNo);
						nlapiLogExecution('DEBUG', 'vSkipId', vSkipId);
						vSkipId=0;
						if(SOSearchResults.length <= parseFloat(vSkipId))
						{
							vSkipId=0;
						}
						//nlapiLogExecution('DEBUG', 'SearchResults of given Wave', getWaveNo);

						var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];
						getWaveNo = SOSearchResult.getValue('custrecord_ebiz_wave_no');

						nlapiLogExecution('DEBUG', 'LP# of given Wave', SOSearchResult.getValue('custrecord_lpno'));
						nlapiLogExecution('DEBUG', 'Exp.Qty of given Wave', SOSearchResult.getValue('custrecord_expe_qty'));
						nlapiLogExecution('DEBUG', 'Wave # of given Wave', getWaveNo);

						SOarray["custparam_waveno"] = getWaveNo;
						SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
						//SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
						SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
						SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
						SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
						SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
						SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
						SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
						SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
						SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
						SOarray["name"] = SOSearchResult.getValue('name');
						SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
						SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
						SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
						SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');	
						SOarray["custparam_clusterno"] = vClusterNo;
						SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
						SOarray["custparam_ebizordno"] = SOSearchResult.getValue('custrecord_ebiz_order_no');
						SOarray["custparam_fastpick"] = fastpick;
						SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
						if(vZoneId!=null && vZoneId!="")
						{
							SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
						}
						else
							SOarray["custparam_ebizzoneno"] = '';
						if(fastpick!='Y')
						{
							response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);
						}
						else
						{
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, SOarray);
						}							
						return;

					}
					else {
						nlapiLogExecution('DEBUG', 'Search Results by Wave No is NULL', WaveNo);
						var SOarray = new Array();
						SOarray["custparam_fastpick"] = fastpick;
						response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, SOarray);
					}


				}	
				else if (vPickType=='CL') {

					nlapiLogExecution('DEBUG', 'Inside Cluster No condition', vClusterNo);

					var SOFilters = new Array();

					SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
					SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK
					SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo));

					var SOColumns = new Array();
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
					SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
					SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
					SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
					SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
					SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
					SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
					SOColumns.push(new nlobjSearchColumn('name'));
					SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
					SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_clus_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_wave_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_container_lp_no'));

					SOColumns[0].setSort();
					SOColumns[1].setSort();
					SOColumns[2].setSort();
					SOColumns[3].setSort();
					SOColumns[4].setSort();
					SOColumns[5].setSort(true);
					SOColumns[19].setSort();	//	Container LP
					var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

					if (SOSearchResults != null && SOSearchResults.length > 0) {
						vSkipId=0;
						if(SOSearchResults.length <= parseFloat(vSkipId))
						{
							vSkipId=0;
						}

						var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];

						SOarray["custparam_waveno"] = SOSearchResult.getValue('custrecord_ebiz_wave_no');
						SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
						SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
						SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
						SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
						SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
						SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
						SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
						SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
						SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
						SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
						SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
						SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');						
						SOarray["custparam_clusterno"] = SOSearchResult.getValue('custrecord_ebiz_clus_no');
						SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
						SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
						SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
						SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
						SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
						SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
						SOarray["custparam_picktype"] =  'CL';
						SOarray["name"] =  SOSearchResult.getValue('name');

						nlapiLogExecution('DEBUG', 'Redirecting to Location Screen');
						response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_location', 'customdeploy_rf_cluspicking_location', false, SOarray);					

					}
					else {
						response.sendRedirect('SUITELET', 'customscript_rf_picking_cluster_no', 'customdeploy_rf_picking_cluster_no_di', false, null);
					}
				}
				else if (WaveNo != null && WaveNo != "" && vPickType!='CL') {
					nlapiLogExecution('DEBUG', 'Inside Wave No condition', parseFloat(WaveNo));

					var SOFilters = new Array();

					SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
					SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
					SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK

					var SOColumns = new Array();

					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
					SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
					SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
					SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
					SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
					SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
					SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
					SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
					SOColumns.push(new nlobjSearchColumn('name'));
					SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
					SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
					SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
					SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));
					SOColumns[19] = new nlobjSearchColumn('custrecord_lpno');
					SOColumns[20] = new nlobjSearchColumn('custrecord_container_lp_no');


					SOColumns[0].setSort();
					SOColumns[1].setSort();
					SOColumns[2].setSort();
					SOColumns[3].setSort();
					SOColumns[4].setSort();
					SOColumns[5].setSort(true);

					var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
					nlapiLogExecution('DEBUG', 'SOSearchResults', SOSearchResults);
					if (SOSearchResults != null && SOSearchResults.length > 0) {
						nlapiLogExecution('DEBUG', 'into Search Results by Wave No', WaveNo);
						vSkipId=0;
						if(SOSearchResults.length <= parseFloat(vSkipId))
						{
							vSkipId=0;
						}
						//nlapiLogExecution('DEBUG', 'SearchResults of given Wave', getWaveNo);
						var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];
						getWaveNo = SOSearchResult.getValue('custrecord_ebiz_wave_no');

						nlapiLogExecution('DEBUG', 'LP# of given Wave', SOSearchResult.getValue('custrecord_lpno'));
						nlapiLogExecution('DEBUG', 'Exp.Qty of given Wave', SOSearchResult.getValue('custrecord_expe_qty'));
						nlapiLogExecution('DEBUG', 'Wave # of given Wave', getWaveNo);

						SOarray["custparam_waveno"] = getWaveNo;
						SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
						//	SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
						SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_container_lp_no');
						SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
						SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
						SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
						SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
						SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
						SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
						SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
						SOarray["name"] = SOSearchResult.getValue('name');
						SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
						SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
						SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
						SOarray["custparam_beginlocationname"] = SOSearchResult.getText('custrecord_actbeginloc');	
						SOarray["custparam_clusterno"] = vClusterNo;
						SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
						SOarray["custparam_ebizordno"] = SOSearchResult.getValue('custrecord_ebiz_order_no');
						SOarray["custparam_fastpick"] = fastpick;

						if(fastpick!='Y')
						{
							response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);

						}
						else
						{
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pick_locanditem', 'customdeploy_ebiz_rf_pick_locanditem_di', false, SOarray);
						}
						return;

					}
					else {
						nlapiLogExecution('DEBUG', 'Search Results by Wave No is NULL', WaveNo);
						var SOarray = new Array();
						SOarray["custparam_fastpick"] = fastpick;
						response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, SOarray);
					}
				}
				else
				{
					var SOarray = new Array();
					SOarray["custparam_fastpick"] = fastpick;
					response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, SOarray);
				}

			}
			else
			{
				nlapiLogExecution('DEBUG', 'Navigating To1', 'Foot print');
				response.sendRedirect('SUITELET', 'customscript_rf_picking_footprint', 'customdeploy_rf_picking_footprint_di', false, SOarray);
			}
		}
	}
	catch(exp)
	{
		nlapiLogExecution("ERROR","AutoStageRecordcreaton",exp);
	}
}

function GetStageLocationFromSTGM(fono,waveno)
{
	nlapiLogExecution('DEBUG', 'Into GetStageLocationFromSTGM');
	nlapiLogExecution('DEBUG', 'fono', fono);
	nlapiLogExecution('DEBUG', 'waveno', waveno);

	var SOSearchResults = new Array();

	var SOFilters = new Array();

	if(fono!=null && fono!='')
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', fono));
	if(waveno!=null && waveno!='')
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', waveno));
	SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [13])); // Task Type - STGM

	var SOColumns = new Array();

	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('internalid'));	
	SOColumns[1].setSort(true);

	SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

	nlapiLogExecution('DEBUG', 'Out of GetStageLocationFromSTGM',SOSearchResults);

	return SOSearchResults;	
}


function GetPickStageLocation(Item, vCarrier, vSite, vCompany, stgDirection,vCarrierType,customizetype,StageBinInternalId,
		ordertype,vcustomer){

	nlapiLogExecution('DEBUG', 'into GetPickStageLocation', Item);
	nlapiLogExecution('DEBUG', 'Item', Item);
	nlapiLogExecution('DEBUG', 'vSite', vSite);
	nlapiLogExecution('DEBUG', 'vCompany', vCompany);
	nlapiLogExecution('DEBUG', 'stgDirection', stgDirection);
	nlapiLogExecution('DEBUG', 'vCarrier', vCarrier);
	nlapiLogExecution('DEBUG', 'vCarrierType', vCarrierType);
	nlapiLogExecution('DEBUG', 'ordertype', ordertype);
	nlapiLogExecution('DEBUG', 'customizetype', customizetype);
	var ItemFamily, ItemGroup, Location;
	var location_found = false;
	var LocArry = new Array();

	var fields = ['custitem_item_family', 'custitem_item_group', 'custitem_ebizdefskustatus', 'custitem_item_info_1', 'custitem_item_info_2', 'custitem_item_info_3'];
	var ItemFamily = "";
	var ItemGroup = "";
	var ItemStatus = "";
	var ItemInfo1 = "";
	var ItemInfo2 = "";
	var ItemInfo3 = "";
	var columns = nlapiLookupField('item', Item, fields);
	if(columns!=null){
		ItemFamily = columns.custitem_item_family;
		ItemGroup = columns.custitem_item_group;
		ItemStatus = columns.custitem_ebizdefskustatus;
		ItemInfo1 = columns.custitem_item_info_1;
		ItemInfo2 = columns.custitem_item_info_2;
		ItemInfo3 = columns.custitem_item_info_3;
	}

	nlapiLogExecution('DEBUG', 'ItemFamily', ItemFamily);
	nlapiLogExecution('DEBUG', 'ItemGroup', ItemGroup);
	nlapiLogExecution('DEBUG', 'ItemStatus', ItemStatus);
	nlapiLogExecution('DEBUG', 'ItemInfo1', ItemInfo1);
	nlapiLogExecution('DEBUG', 'ItemInfo2', ItemInfo2);
	nlapiLogExecution('DEBUG', 'ItemInfo3', ItemInfo3);

	nlapiLogExecution('DEBUG', 'Before Variable declaration');

	var locgroupid = 0;
	var zoneid = 0;
	var direction = "";
	if (stgDirection == 'INB') {
		direction = "1";
	}
	else 
		if (stgDirection == 'OUB') {
			direction = "2";
		}
		else
		{
			direction = "3";
		}

	var columns = new Array();
	var filters = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_sequencenostgrule');
	columns[0].setSort();
	columns[1] = new nlobjSearchColumn('custrecord_locationstgrule');
	columns[2] = new nlobjSearchColumn('custrecord_inboundlocationgroupstg');
	columns[3] = new nlobjSearchColumn('custrecord_outboundlocationgroup');
	columns[4] = new nlobjSearchColumn('custrecord_nooffootprints', 'custrecord_locationstgrule');

	if (ItemFamily != null && ItemFamily != "") {
		filters.push(new nlobjSearchFilter('custrecord_skufamilystgrule', null, 'anyof', ['@NONE@', ItemFamily]));
	}
	if (ItemGroup != null && ItemGroup != "") {
		filters.push(new nlobjSearchFilter('custrecord_skugroupstg', null, 'anyof', ['@NONE@', ItemGroup]));
	}
	if (Item != null && Item != "") {
		filters.push(new nlobjSearchFilter('custrecord_skustgrule', null, 'anyof', ['@NONE@', Item]));
	}
	if (vCarrier != null && vCarrier != "") {
		filters.push(new nlobjSearchFilter('custrecord_carrierstgrule', null, 'anyof', ['@NONE@', vCarrier]));

	} // code added from Boombah account on 25Feb13 by santosh
	else
		filters.push(new nlobjSearchFilter('custrecord_carrierstgrule', null, 'anyof', ['@NONE@']));
	// upto here
	if (vCarrierType != null && vCarrierType != "") {
		filters.push(new nlobjSearchFilter('custrecord_carriertypestgrule', null, 'anyof', ['@NONE@', vCarrierType]));
	}
	if (vSite != null && vSite != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizsitestage', null, 'anyof', ['@NONE@', vSite]));
	}
	if (vCompany != null && vCompany != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebizcompanystage', null, 'anyof', ['@NONE@', vCompany]));
	}
	if (ordertype != null && ordertype != "") {
		filters.push(new nlobjSearchFilter('custrecord_stagerule_ordtype', null, 'anyof', ['@NONE@', ordertype]));
	}
	else
		filters.push(new nlobjSearchFilter('custrecord_stagerule_ordtype', null, 'anyof', ['@NONE@']));
	if (customizetype != null && customizetype != "") {
		filters.push(new nlobjSearchFilter('custrecord_ebiz_stg_customizetype', null, 'anyof', ['@NONE@', customizetype]));

	} // code added from Boombah account on 25Feb13 by santosh
	else
		filters.push(new nlobjSearchFilter('custrecord_ebiz_stg_customizetype', null, 'anyof', ['@NONE@']));
	filters.push(new nlobjSearchFilter('custrecord_directionstgrule', null, 'anyof', [direction, '3']));
	filters.push(new nlobjSearchFilter('custrecord_stg_tasktype', null, 'noneof', ['14']));

	if (vcustomer != null && vcustomer != "") {
		filters.push(new nlobjSearchFilter('custrecord_stagerule_customer', null, 'anyof', ['@NONE@', vcustomer]));
	}
	nlapiLogExecution('DEBUG', 'Before Searching of Stageing Rules');


	var searchresults = nlapiSearchRecord('customrecord_ebiznet_stgrules', null, filters, columns);
	nlapiLogExecution('DEBUG', 'searchresults',searchresults);
	if (searchresults != null) {
		for (var i = 0; i < Math.min(10, searchresults.length); i++) {
			//GetPickStagLpCount                
			var vstgLocation = searchresults[i].getValue('custrecord_locationstgrule');

			var vstgLocationText = searchresults[i].getText('custrecord_locationstgrule');

			nlapiLogExecution('DEBUG', 'Stageing Location Value::', vstgLocation);
			nlapiLogExecution('DEBUG', 'Stageing Location Text::', vstgLocationText);
			var vnoofFootPrints = searchresults[i].getValue('custrecord_nooffootprints');
			nlapiLogExecution('DEBUG', 'vnoofFootPrints::', vnoofFootPrints);

			if (vstgLocation != null && vstgLocation != "") {
				var vLpCnt = GetPickStagLpCount(vstgLocation, vSite, vCompany);
				if (vnoofFootPrints != null && vnoofFootPrints != "") {
					if (parseFloat(vLpCnt) < parseFloat(vnoofFootPrints)) {
						return vstgLocation;
					}
				}
				else {
					return vstgLocation;
				}
			}
			nlapiLogExecution('DEBUG', 'final stagelocation::', vstgLocation);

			var LocGroup = "";
			if (stgDirection == 'INB') {
				LocGroup = searchresults[i].getValue('custrecord_inboundlocationgroupstg');
			}
			else {
				LocGroup = searchresults[i].getValue('custrecord_outboundlocationgroup');
			}

			nlapiLogExecution('DEBUG', 'final stagelocation::', vstgLocation);
			nlapiLogExecution('DEBUG', 'LocGroup::', LocGroup);

			if (LocGroup != null && LocGroup != "") {

				var collocGroup = new Array();
				var filtersLocGroup = new Array();
				collocGroup[0] = new nlobjSearchColumn('custrecord_startingpickseqno');
				collocGroup[0].setSort();
				collocGroup[1] = new nlobjSearchColumn('name');
				collocGroup[2] = new nlobjSearchColumn('custrecord_nooffootprints');
				if (stgDirection == 'INB') {
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_inboundlocgroupid', null, 'anyof', [LocGroup]));
				}
				else { 
					filtersLocGroup.push(new nlobjSearchFilter('custrecord_outboundlocgroupid', null, 'anyof', [LocGroup]));
				}

				var searchresultsloc = nlapiSearchRecord('customrecord_ebiznet_location', null, filtersLocGroup, collocGroup);
				nlapiLogExecution('DEBUG', 'searchresultsloc::', searchresultsloc);
				for (var k = 0; searchresultsloc != null && k < searchresultsloc.length; k++) {
					var vLocid=searchresultsloc[k].getId();
					nlapiLogExecution('DEBUG', 'vLocid::', vLocid);

					var vLpCnt = GetPickStagLpCount(vLocid, vSite, vCompany);
					var vnoofFootPrints = searchresultsloc[k].getValue('custrecord_nooffootprints');

					//--
					if (vnoofFootPrints != null && vnoofFootPrints != "") {
						if (parseFloat(vLpCnt) < parseFloat(vnoofFootPrints)) {
							return vLocid;
						}
					}
					else {
						return vLocid;
					}
				}
			}
		}
	}
	return -1;
}

/**
 * 
 * @param location
 * @param site
 * @param company
 * @returns {Number}
 */
function GetPickStagLpCount(location, site, company){
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_lp',null,'count');

	var outLPCount = 0;

	var filters= new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', [location]));

	if(site != null && site != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_loc', null, 'anyof', [site]));

	if(company != null && company != "")
		filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_company', null, 'anyof', [company]));

	var lpCount = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
	if(lpCount != null)
		outLPCount = lpCount[0].getValue('custrecord_ebiz_inv_lp', null, 'count');

	return outLPCount;
}

function CreateSTGMRecord(vContLp, vebizWaveNo, vebizOrdNo, vDoname, vCompany,vlocation,vdono,
		Item,stgDirection,vCarrier,vSite,stageLocation,vClusterNo,salesorderintrid,vondemandstage) {

	nlapiLogExecution('DEBUG', 'into CreateSTGMRecord');
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null,
			'is', '9')); //STATUS.OUTBOUND.PICK_GENERATED('G') 
	filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is',
			vebizWaveNo));
	if(vContLp!=null && vContLp!='' && vContLp!='null')
	{
		filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null,
				'is', vContLp));
	}
	else
	{
		filters.push(new nlobjSearchFilter('custrecord_container_lp_no', null,'isempty'));
	}

	if(vClusterNo!=null && vClusterNo!='')
		filters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null,
				'is', vClusterNo));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_sku_no');

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',
			null, filters, columns);
	if (searchresults == null || vondemandstage=='Y') {
		if(stageLocation==null || stageLocation=='')
			stageLocation = GetPickStageLocation(Item, vCarrier, vSite, vCompany,
					stgDirection,null,null,null,null,null);
		var STGMtask = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'stageLocation',stageLocation);
		nlapiLogExecution('DEBUG', 'Name',vDoname);
		nlapiLogExecution('DEBUG', 'DO NO',vdono);
		if(vDoname==null || vDoname=='')
			vDoname = vdono;
		STGMtask.setFieldValue('name', vDoname);
		STGMtask.setFieldValue('custrecord_ebiz_concept_po', vebizOrdNo);
		STGMtask.setFieldValue('custrecord_tasktype', '13');//STATUS.OUTBOUND.ORDERLINE_PARTIALLY_SHIPPED('PS')
		STGMtask.setFieldValue('custrecord_wms_location', vlocation);
		STGMtask.setFieldValue('custrecord_actbeginloc', stageLocation);
		STGMtask.setFieldValue('custrecord_actendloc', stageLocation);
		STGMtask.setFieldValue('custrecord_container_lp_no', vContLp);
		STGMtask.setFieldValue('custrecord_ebiz_cntrl_no', vdono);
		STGMtask.setFieldValue('custrecord_ebiz_receipt_no', vdono);
		STGMtask.setFieldValue('custrecord_ebiz_wave_no', vebizWaveNo);
		STGMtask.setFieldValue('custrecordact_begin_date', DateStamp());
		STGMtask.setFieldValue('custrecordact_end_date', DateStamp());
		STGMtask.setFieldValue('custrecord_actualendtime', TimeStamp());
		STGMtask.setFieldValue('custrecord_actualbegintime', TimeStamp());
		STGMtask.setFieldValue('custrecord_sku', Item);
		STGMtask.setFieldValue('custrecord_ebiz_sku_no', Item);
		STGMtask.setFieldValue('custrecord_ebiz_order_no', salesorderintrid);
		//STGMtask.setFieldValue('custrecord_comp_id', vCompany);
		var currentContext = nlapiGetContext();
		var currentUserID = currentContext.getUser();
		STGMtask.setFieldValue('custrecord_ebizuser', currentUserID);
		var retSubmitLP= nlapiSubmitRecord(STGMtask);
		nlapiLogExecution('DEBUG', 'out of CreateSTGMRecord');

		nlapiLogExecution('DEBUG', 'updating outbound inventory with satge location',stageLocation);
		updateStageInventorywithStageLoc(vContLp,stageLocation);
	}
}
//Case# 201413032....
function containerSizeCheck(ContainerSize,vsize,whsite)
{

	var mesg="";
	var filters = new Array();
	nlapiLogExecution('DEBUG', 'vsize::', vsize);

	filters.push(new nlobjSearchFilter('name', null, 'is', vsize));
	filters.push(new nlobjSearchFilter('custrecord_containertype', null, 'anyof', ['1','4']));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(whsite!=null && whsite!='')
		filters.push(new nlobjSearchFilter('custrecord_ebizsitecontainer', null, 'anyof', ['@NONE@',whsite]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('Internalid');

	containersizelist = nlapiSearchRecord('customrecord_ebiznet_container', null, filters, columns);
	nlapiLogExecution('DEBUG', 'list is::', containersizelist);

	if(containersizelist== null || containersizelist== "" || containersizelist.length==0)
	{	
		mesg="invalid carton size";
	}

	return mesg;
}
//Case# 201413032....
function updateStageInventorywithStageLoc(contlpno,stageLocation)
{
	nlapiLogExecution('DEBUG', 'into updateStageInventorywithStageLoc');
	nlapiLogExecution('DEBUG', 'contlpno',contlpno);
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null,'is', '18')); //FLAG.INVENTORY.OUTBOUND('O') 
	filters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null,'is', contlpno));
	filters.push(new nlobjSearchFilter('custrecord_invttasktype', null,'is', '3')); ////Task Type - PICK

	var columns = new Array();

	var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv',null, filters, columns);
	if (invtsearchresults != null && invtsearchresults != '' && invtsearchresults.length > 0) 
	{
		for (var i = 0; i < invtsearchresults.length; i++)
		{
			var invtid=invtsearchresults[i].getId();

			nlapiLogExecution('DEBUG', 'Deleting Outbound Inventory....',invtid);
			nlapiSubmitField('customrecord_ebiznet_createinv', invtid, 'custrecord_ebiz_inv_binloc', stageLocation);
		}		
	}

	nlapiLogExecution('DEBUG', 'out of updateStageInventorywithStageLoc');
}

