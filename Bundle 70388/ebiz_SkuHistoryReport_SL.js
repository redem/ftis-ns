/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Reports/Suitelet/ebiz_SkuHistoryReport_SL.js,v $
 *     	   $Revision: 1.2.4.4.4.1.4.4 $
 *     	   $Date: 2014/06/20 00:09:59 $
 *     	   $Author: nneelam $
 *     	   $Name: t_NSWMS_2014_1_3_125 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_SkuHistoryReport_SL.js,v $
 * Revision 1.2.4.4.4.1.4.4  2014/06/20 00:09:59  nneelam
 * case#  20149009
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.2.4.4.4.1.4.3  2014/01/29 06:24:44  skreddy
 * case :20126946
 * PCT Prod issue fix
 *
 * Revision 1.2.4.4.4.1.4.2  2013/09/02 07:38:07  schepuri
 * displying lot number in report
 *
 * Revision 1.2.4.4.4.1.4.1  2013/06/05 12:00:32  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.2.4.4.4.1  2012/11/01 14:55:15  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.2.4.4  2012/07/19 14:20:22  schepuri
 * CASE201112/CR201113/LOG201121
 * paging added
 *
 * Revision 1.2.4.3  2012/04/20 12:21:33  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.2.4.2  2012/02/15 14:26:54  spendyala
 * CASE201112/CR201113/LOG201121
 * Added SO/PO field to the sublist.
 *
 * Revision 1.2.4.1  2012/02/13 13:24:59  spendyala
 * CASE201112/CR201113/LOG201121
 * Made Item as  mandatory.
 *
 * Revision 1.2  2011/10/24 08:00:58  spendyala
 * CASE201112/CR201113/LOG201121
 * added functionality for fetching records more than 1000
 *
 * Revision 1.1  2011/10/21 13:51:49  spendyala
 * CASE201112/CR201113/LOG201121
 * script used to display sku history
 *





/**
 * function for display sku history in a grid.
 */

var OTsearchResultArray=new Array();

function getOTSearchResults(sku,fromdate,todate,maxno)
{

	var filter = new Array();

	if(sku != null && sku != "")
	{
		filter.push(new nlobjSearchFilter('custrecord_ebiz_sku_no', null,'is', sku));
	}
	if((fromdate != null && fromdate != "")&&(todate != null && todate != ""))
	{
		//filter.push(new nlobjSearchFilter('custrecordact_begin_date', null, 'within', fromdate, todate));
		filter.push(new nlobjSearchFilter('custrecord_act_end_date', null, 'within', fromdate, todate));
	}

	if(maxno!=-1)
	{
		filter.push(new nlobjSearchFilter('id',null,'greaterthan',maxno));
	}
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_tasktype').setSort('true');
	columns[1]=new nlobjSearchColumn('custrecord_sku');
	columns[2]=new nlobjSearchColumn('custrecord_skudesc');
	columns[3]=new nlobjSearchColumn('custrecord_batch_no');
	columns[4]=new nlobjSearchColumn('custrecord_sku_status');
	columns[5]=new nlobjSearchColumn('custrecord_actbeginloc');
	columns[6]=new nlobjSearchColumn('custrecord_actendloc');
	columns[7]=new nlobjSearchColumn('custrecord_act_qty');
	columns[8]=new nlobjSearchColumn('internalid').setSort();
	columns[9]=new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[10]=new nlobjSearchColumn('custrecord_act_end_date');

	var OTsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filter, columns);

	if(OTsearchresults!=null)
	{
		if(OTsearchresults.length>=1000)
		{
			var maxno1=OTsearchresults[OTsearchresults.length-1].getValue(columns[8]);
			//OTsearchResultArray.push(OTsearchresults);
			for(var t=0;t<OTsearchresults.length;t++)
			{
				//OTsearchResultArray.push(OTsearchresults[t]);
				var OTelements = new Array();
				OTelements.push(OTsearchresults[t].getText('custrecord_tasktype'));
				OTelements.push(OTsearchresults[t].getText('custrecord_sku'));
				OTelements.push(OTsearchresults[t].getValue('custrecord_skudesc'));
				OTelements.push(OTsearchresults[t].getValue('custrecord_batch_no'));
				OTelements.push(OTsearchresults[t].getText('custrecord_sku_status'));
				OTelements.push(OTsearchresults[t].getText('custrecord_actbeginloc'));
				OTelements.push(OTsearchresults[t].getText('custrecord_actendloc'));
				OTelements.push(OTsearchresults[t].getValue('custrecord_act_qty'));
				OTelements.push(OTsearchresults[t].getText('custrecord_ebiz_order_no'));
				OTelements.push(OTsearchresults[t].getValue('custrecord_act_end_date'));
				
				nlapiLogExecution('ERROR', 'OTelements',OTelements);
				
				OTsearchResultArray.push(OTelements);
				
			}
			getOTSearchResults(sku,fromdate,todate,maxno1);
		}
		else
		{
			//OTsearchResultArray.push(OTsearchresults);
			for(var t=0;t<OTsearchresults.length;t++)
			{
				//OTsearchResultArray.push(OTsearchresults[t]);
				var OTelements = new Array();
				OTelements.push(OTsearchresults[t].getText('custrecord_tasktype'));
				OTelements.push(OTsearchresults[t].getText('custrecord_sku'));
				OTelements.push(OTsearchresults[t].getValue('custrecord_skudesc'));
				OTelements.push(OTsearchresults[t].getValue('custrecord_batch_no'));
				OTelements.push(OTsearchresults[t].getText('custrecord_sku_status'));
				OTelements.push(OTsearchresults[t].getText('custrecord_actbeginloc'));
				OTelements.push(OTsearchresults[t].getText('custrecord_actendloc'));
				OTelements.push(OTsearchresults[t].getValue('custrecord_act_qty'));
				OTelements.push(OTsearchresults[t].getText('custrecord_ebiz_order_no'));
				OTelements.push(OTsearchresults[t].getValue('custrecord_act_end_date'));
				
				nlapiLogExecution('ERROR', 'OTelements2',OTelements);
				OTsearchResultArray.push(OTelements);
				
			}
			
		}
	}
	return OTsearchResultArray;
}


//var CTsearchResultArray=new Array();

function getCTSearchResults(sku,fromdate,todate,maxno)
{


	var filter1 = new Array();

	if(sku != null && sku != "")
	{
		filter1.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_sku_no', null,'is', sku));
	}
	if((fromdate != null && fromdate != "")&&(todate != null && todate != ""))
	{
		//filter1.push(new nlobjSearchFilter('custrecord_ebiztask_act_begin_date', null, 'within', fromdate,todate));
		filter1.push(new nlobjSearchFilter('custrecord_ebiztask_act_end_date', null, 'within', fromdate, todate));
	}

	if(maxno!=-1)
	{
		filter1.push(new nlobjSearchFilter('id',null,'greaterthan',maxno));
	}

	var columns1 = new Array();
	columns1[0] = new nlobjSearchColumn('custrecord_ebiztask_tasktype').setSort('true');
	columns1[1]=new nlobjSearchColumn('custrecord_ebiztask_sku');
	columns1[2]=new nlobjSearchColumn('custrecord_ebiztask_skudesc');
	columns1[3]=new nlobjSearchColumn('custrecord_ebiztask_batch_no');
	columns1[4]=new nlobjSearchColumn('custrecord_ebiztask_sku_status');
	columns1[5]=new nlobjSearchColumn('custrecord_ebiztask_actbeginloc');
	columns1[6]=new nlobjSearchColumn('custrecord_ebiztask_actendloc');
	columns1[7]=new nlobjSearchColumn('custrecord_ebiztask_act_qty');
	columns1[8]=new nlobjSearchColumn('internalid').setSort();
	columns1[9]=new nlobjSearchColumn('custrecord_ebiztask_ebiz_order_no');
	columns1[10]=new nlobjSearchColumn('custrecord_ebiztask_act_end_date');
	
	var CTsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filter1, columns1);

	if(CTsearchresults!=null)
	{
		if(CTsearchresults.length>=1000)
		{
			var maxno1=CTsearchresults[CTsearchresults.length-1].getValue(columns1[8]);
			for(var u=0;u<CTsearchresults.length;u++)
			{
				//OTsearchResultArray.push(CTsearchresults[u]);
				var CTelements = new Array();
				CTelements.push(CTsearchresults[u].getText('custrecord_ebiztask_tasktype'));
				CTelements.push(CTsearchresults[u].getText('custrecord_ebiztask_sku'));
				CTelements.push(CTsearchresults[u].getValue('custrecord_ebiztask_skudesc'));
				CTelements.push(CTsearchresults[u].getValue('custrecord_ebiztask_batch_no'));
				CTelements.push(CTsearchresults[u].getText('custrecord_ebiztask_sku_status'));
				CTelements.push(CTsearchresults[u].getText('custrecord_ebiztask_actbeginloc'));
				CTelements.push(CTsearchresults[u].getText('custrecord_ebiztask_actendloc'));
				CTelements.push(CTsearchresults[u].getValue('custrecord_ebiztask_act_qty'));
				CTelements.push(CTsearchresults[u].getText('custrecord_ebiztask_ebiz_order_no'));
				CTelements.push(CTsearchresults[u].getValue('custrecord_ebiztask_act_end_date'));
				
				OTsearchResultArray.push(CTelements);
			}

			//CTsearchResultArray.push(CTsearchresults);
			getCTSearchResults(sku,fromdate,todate,maxno1);
		}
		else
		{
			for(var u=0;u<CTsearchresults.length;u++)
			{
				//OTsearchResultArray.push(CTsearchresults[u]);
				var CTelements = new Array();
				CTelements.push(CTsearchresults[u].getText('custrecord_ebiztask_tasktype'));
				CTelements.push(CTsearchresults[u].getText('custrecord_ebiztask_sku'));
				CTelements.push(CTsearchresults[u].getValue('custrecord_ebiztask_skudesc'));
				CTelements.push(CTsearchresults[u].getValue('custrecord_ebiztask_batch_no'));
				CTelements.push(CTsearchresults[u].getText('custrecord_ebiztask_sku_status'));
				CTelements.push(CTsearchresults[u].getText('custrecord_ebiztask_actbeginloc'));
				CTelements.push(CTsearchresults[u].getText('custrecord_ebiztask_actendloc'));
				CTelements.push(CTsearchresults[u].getValue('custrecord_ebiztask_act_qty'));
				CTelements.push(CTsearchresults[u].getText('custrecord_ebiztask_ebiz_order_no'));
				CTelements.push(CTsearchresults[u].getValue('custrecord_ebiztask_act_end_date'));
				
				OTsearchResultArray.push(CTelements);
			}
			//CTsearchResultArray.push(CTsearchresults);
		}
	}
	return OTsearchResultArray;
}



function getInvAdjSearchResults(sku,fromdate,todate,maxno)
{




	var filter2 = new Array();

	if(sku != null && sku != "")
	{
		filter2.push(new nlobjSearchFilter('custrecord_ebizskuno', null,'anyof', sku));
	}

	if((fromdate != null && fromdate != "")&&(todate != null && todate != ""))
	{
		filter2.push(new nlobjSearchFilter('custrecord_ebiz_recorddate', null, 'within', fromdate,todate));
	}

	if(maxno!=-1)
	{
		filter2.push(new nlobjSearchFilter('id',null,'greaterthan',maxno));
	}

	//	filter2.push(new nlobjSearchFilter('custrecord_skustatus',null,'anyof',['9','10','11','18']));//move,invt,adjt,xfer
	var columns2 = new Array();
	columns2[0] = new nlobjSearchColumn('custrecord_ebiz_tasktype').setSort('true');
	columns2[1]=new nlobjSearchColumn('custrecord_ebizskuno');
	columns2[2]=new nlobjSearchColumn('description','custrecord_ebizskuno');
	columns2[3]=new nlobjSearchColumn('custrecord_skustatus');
	columns2[4]=new nlobjSearchColumn('custrecord_ebiz_binloc');
	columns2[5]=new nlobjSearchColumn('custrecord_ebiz_adjustqty');
	columns2[6]=new nlobjSearchColumn('internalid').setSort();
	columns2[7]=new nlobjSearchColumn('custrecord_ebiz_adjustqty');
	columns2[8]=new nlobjSearchColumn('custrecord_ebiz_recorddate');
	columns2[9]=new nlobjSearchColumn('custrecord_ebiz_batchno');
	
	var InvAdjsearchresults = nlapiSearchRecord('customrecord_ebiznet_invadj', null, filter2, columns2);

	if(InvAdjsearchresults!=null)
	{
		if(InvAdjsearchresults.length>=1000)
		{
			var maxno1=InvAdjsearchresults[InvAdjsearchresults.length-1].getValue(columns[6]);
			//OTsearchResultArray.push(InvAdjsearchresults);
			for(var v=0;v<InvAdjsearchresults.length;v++)
			{
				//OTsearchResultArray.push(InvAdjsearchresults[v]);
				var InvAdjelements = new Array();
				InvAdjelements.push(InvAdjsearchresults[v].getText('custrecord_ebiz_tasktype'));
				InvAdjelements.push(InvAdjsearchresults[v].getText('custrecord_ebizskuno'));
				InvAdjelements.push(InvAdjsearchresults[v].getValue('description','custrecord_ebizskuno'));
				InvAdjelements.push(InvAdjsearchresults[v].getValue('custrecord_ebiz_batchno'));
				InvAdjelements.push(InvAdjsearchresults[v].getText('custrecord_skustatus'));
				InvAdjelements.push(InvAdjsearchresults[v].getText('custrecord_ebiz_binloc'));
				InvAdjelements.push('');
				InvAdjelements.push(InvAdjsearchresults[v].getValue('custrecord_ebiz_adjustqty'));
				InvAdjelements.push('');
				InvAdjelements.push(InvAdjsearchresults[v].getValue('custrecord_ebiz_recorddate'));
				
				OTsearchResultArray.push(InvAdjelements);
			}
			getInvAdjSearchResults(sku,fromdate,todate,maxno1);
		}
		else
		{
			for(var v=0;v<InvAdjsearchresults.length;v++)
			{
				//OTsearchResultArray.push(InvAdjsearchresults[v]);
				var InvAdjelements = new Array();
				InvAdjelements.push(InvAdjsearchresults[v].getText('custrecord_ebiz_tasktype'));
				InvAdjelements.push(InvAdjsearchresults[v].getText('custrecord_ebizskuno'));
				InvAdjelements.push(InvAdjsearchresults[v].getValue('description','custrecord_ebizskuno'));
				InvAdjelements.push(InvAdjsearchresults[v].getValue('custrecord_ebiz_batchno'));
				InvAdjelements.push(InvAdjsearchresults[v].getText('custrecord_skustatus'));
				InvAdjelements.push(InvAdjsearchresults[v].getText('custrecord_ebiz_binloc'));
				InvAdjelements.push('');
				InvAdjelements.push(InvAdjsearchresults[v].getValue('custrecord_ebiz_adjustqty'));
				InvAdjelements.push('');
				InvAdjelements.push(InvAdjsearchresults[v].getValue('custrecord_ebiz_recorddate'));
				
				OTsearchResultArray.push(InvAdjelements);
			}
			//OTsearchResultArray.push(InvAdjsearchresults);
		}
	}
	return OTsearchResultArray;
}

/*

function getInvSearchResults(sku,fromdate,todate,maxno)
{

	var filter3 = new Array();

	if(sku != null && sku != "")
	{
		filter3.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null,'anyof', sku));
	}
//	if((fromdate != null && fromdate != "")&&(todate != null && todate != ""))
//	{
//	filter3.push(new nlobjSearchFilter('custrecord_ebiz_inv_recorddate', null, 'within', fromdate,todate));
//	}
	if(maxno!=-1)
	{
		filter3.push(new nlobjSearchFilter('id',null,'greaterthan',maxno));
	}

	var columns3 = new Array();
	columns3[0]=new nlobjSearchColumn('custrecord_invttasktype').setSort('true');
	columns3[1]=new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns3[2]=new nlobjSearchColumn('description','custrecord_ebiz_inv_sku');
	columns3[3]=new nlobjSearchColumn('custrecord_ebiz_inv_lot');
	columns3[4]=new nlobjSearchColumn('custrecord_ebiz_inv_sku_status');
	columns3[5]=new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns3[6]=new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columns3[7]=new nlobjSearchColumn('internalid').setSort();
	columns3[8]=new nlobjSearchColumn('custrecord_ebiz_transaction_no');
	var Invsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filter3, columns3);

	if(Invsearchresults!=null)
	{
		if(Invsearchresults.length>=1000)
		{
			var maxno1=Invsearchresults[Invsearchresults.length-1].getValue(columns[7]);
			//InvsearchResultArray.push(Invsearchresults);
			for(var w=0;w<Invsearchresults.length;w++)
			{
				//OTsearchResultArray.push(Invsearchresults[w]);
				var Invelements = new Array();
				Invelements.push(Invsearchresults[w].getText('custrecord_invttasktype'));
				Invelements.push(Invsearchresults[w].getText('custrecord_ebiz_inv_sku'));
				Invelements.push(Invsearchresults[w].getValue('description','custrecord_ebiz_inv_sku'));
				Invelements.push(Invsearchresults[w].getText('custrecord_ebiz_inv_lot'));
				Invelements.push(Invsearchresults[w].getText('custrecord_ebiz_inv_sku_status'));
				Invelements.push(Invsearchresults[w].getText('custrecord_ebiz_inv_binloc'));
				Invelements.push('');
				Invelements.push(Invsearchresults[w].getValue('custrecord_ebiz_inv_qty'));
				Invelements.push(Invsearchresults[w].getText('custrecord_ebiz_transaction_no'));
				Invelements.push('');
				
				OTsearchResultArray.push(Invelements);
			}
			getInvSearchResults(sku,fromdate,todate,maxno1);
		}
		else
		{
			for(var w=0;w<Invsearchresults.length;w++)
			{
				//OTsearchResultArray.push(Invsearchresults[w]);
				var Invelements = new Array();
				Invelements.push(Invsearchresults[w].getText('custrecord_invttasktype'));
				Invelements.push(Invsearchresults[w].getText('custrecord_ebiz_inv_sku'));
				Invelements.push(Invsearchresults[w].getValue('description','custrecord_ebiz_inv_sku'));
				Invelements.push(Invsearchresults[w].getText('custrecord_ebiz_inv_lot'));
				Invelements.push(Invsearchresults[w].getText('custrecord_ebiz_inv_sku_status'));
				Invelements.push(Invsearchresults[w].getText('custrecord_ebiz_inv_binloc'));
				Invelements.push('');
				Invelements.push(Invsearchresults[w].getValue('custrecord_ebiz_inv_qty'));
				Invelements.push(Invsearchresults[w].getText('custrecord_ebiz_transaction_no'));
				Invelements.push('');
				
				OTsearchResultArray.push(Invelements);
			}
			//InvsearchResultArray.push(Invsearchresults);
		}
	}
	return OTsearchResultArray;
}*/




function skuHistory(request, response)
{
	if (request.getMethod() == 'GET') 
	{
		//create form
		var form = nlapiCreateForm('Item History');
		form.setScript('customscript_inventoryclientvalidations');
		//adding From date 
		form.addField('custpage_fromdate', 'date', 'From Date').setMandatory( true );

		//adding a select field of type item
		form.addField('custpage_items', 'select', 'Item','item').setMandatory( true );

		//adding To date
		form.addField('custpage_todate', 'date', 'To Date').setMandatory( true );

		//adding button
		form.addSubmitButton('Display');

		response.writePage(form);
	}
	else
	{
		var sku = request.getParameter('custpage_items');
		var fromdate = request.getParameter('custpage_fromdate');
		var todate = request.getParameter('custpage_todate');

		nlapiLogExecution('ERROR','sku',sku);
		nlapiLogExecution('ERROR','fromdate',fromdate);
		nlapiLogExecution('ERROR','todate',todate);


		//create form
		var form = nlapiCreateForm('Item History');
		form.setScript('customscript_inventoryclientvalidations');
		//adding From date 
		var cFromDate = form.addField('custpage_fromdate', 'date', 'From Date').setMandatory( true );
		
		if(request.getParameter('custpage_fromdate')!='' && request.getParameter('custpage_fromdate')!=null)
		{
			cFromDate.setDefaultValue(request.getParameter('custpage_fromdate'));	
		}
		

		//adding a select field of type item
		var cItem = form.addField('custpage_items', 'select', 'Item','item').setMandatory( true );
		if(request.getParameter('custpage_items')!='' && request.getParameter('custpage_items')!=null)
		{
			cItem.setDefaultValue(request.getParameter('custpage_items'));	
		}
		//adding To date
		var cToDate = form.addField('custpage_todate', 'date', 'To Date').setMandatory( true );

		if(request.getParameter('custpage_todate')!='' && request.getParameter('custpage_todate')!=null)
		{
			cToDate.setDefaultValue(request.getParameter('custpage_todate'));	
		}
		//adding button
		form.addSubmitButton('Display');

		//create a sublist to display values.
		var sublist = form.addSubList("custpage_results", "list", "Item List");
		sublist.addField("custpage_slno", "text", "Sl No");
		sublist.addField("custpage_sopo", "text", "SO/PO");
		sublist.addField("custpage_tasktype", "text", "Task Type");
		sublist.addField("custpage_item", "text", "SKU");
		sublist.addField("custpage_description", "text", "Description");
		sublist.addField("custpage_lotbat", "text", "LOT#");
		sublist.addField("custpage_status", "text", "Status");
		sublist.addField("custpage_beginlocation", "text", "Begin Location");
		sublist.addField("custpage_endlocation", "text", "End Location");
		sublist.addField("custpage_quantity", "text", "Quantity");
		sublist.addField("custpage_transdate", "text", "Transaction Date");

		var searchresults1 = getOTSearchResults(sku,fromdate,todate,-1);
		var searchresults2 = getCTSearchResults(sku,fromdate,todate,-1);
		var searchresults3 = getInvAdjSearchResults(sku,fromdate,todate,-1);
		//var searchresults4 = getInvSearchResults(sku,fromdate,todate,-1);
		
		
		if(searchresults3 != null && searchresults3.length > 0){
			setPagingForSublist(searchresults3,form);
		}


		response.writePage(form);
	}
}


function setPagingForSublist(orderListArray,form)
{
	if(orderListArray != null && orderListArray.length > 0){
		nlapiLogExecution('Error', 'orderListArray ', orderListArray.length);
		//var orderListArray=new Array();		
		/*for(k=0;k<orderList.length;k++)
		{
			nlapiLogExecution('ERROR', 'orderList[k]', orderList[k]); 
			var ordsearchresult = orderList[k];

			if(ordsearchresult!=null)
			{
				nlapiLogExecution('ERROR', 'ordsearchresult.length ', ordsearchresult.length); 
				for(var j=0;j<ordsearchresult.length;j++)
				{
					orderListArray[orderListArray.length]=ordsearchresult[j];				
				}
			}
		}*/
		//nlapiLogExecution('ERROR', 'orderListArray.length ', orderListArray.length); 
		var test='';

		if(orderListArray.length>0)
		{

			if(orderListArray.length>25)
			{
				var pagesize = form.addField('custpage_pagesize', 'text', 'Page Size').setDisplayType('hidden');
				pagesize.setDisplaySize(10,10);
				pagesize.setLayoutType('outsidebelow', 'startrow');
				var select= form.addField('custpage_selectpage','select', 'Select Records');	
				select.setLayoutType('outsidebelow', 'startrow');			
				select.setDisplaySize(200,30);
				if (request.getMethod() == 'GET'){
					pagesize.setDefaultValue("25");
					pagesizevalue=25;
				}
				else
				{
					if(request.getParameter('custpage_pagesize')!=null)
					{
						pagesizevalue= request.getParameter('custpage_pagesize');
					}
					else
					{
						pagesizevalue= 25;
						pagesize.setDefaultValue("25");
					}
				}

				//this is to add the pageno's to the dropdown.
				var len=orderListArray.length/parseFloat(pagesizevalue);
				for(var k=1;k<=Math.ceil(len);k++)
				{

					var from;var to;

					to=parseFloat(k)*parseFloat(pagesizevalue);
					from=(parseFloat(to)-parseFloat(pagesizevalue))+1;

					if(parseFloat(to)>orderListArray.length)
					{
						to=orderListArray.length;
						test=from.toString()+","+to.toString(); 
					}

					var temp=from.toString()+" to "+to.toString();
					var tempto=from.toString()+","+to.toString();
					select.addSelectOption(tempto,temp);

				} 

				if (request.getMethod() == 'POST'){

					if(request.getParameter('custpage_selectpage')!=null ){

						select.setDefaultValue(request.getParameter('custpage_selectpage'));	

					}
					if(request.getParameter('custpage_pagesize')!=null ){

						pagesize.setDefaultValue(request.getParameter('custpage_pagesize'));	

					}
				}
			}
			else
			{
				pagesizevalue=parseFloat(orderListArray.length);
			}
			var minval=0;var maxval=parseFloat(pagesizevalue);
			if(parseFloat(pagesizevalue)>orderListArray.length)
			{
				maxval=orderListArray.length;
			}
			var selectno=request.getParameter('custpage_selectpage');
			if(selectno!=null )
			{
				var selectedPage= request.getParameter('custpage_selectpage');
				var selectedPageArray=selectedPage.split(',');			
				var diff=parseFloat(selectedPageArray[1])-(parseFloat(selectedPageArray[0])-1);
				nlapiLogExecution('ERROR', 'diff',diff);

				var pagevalue=request.getParameter('custpage_pagesize');
				nlapiLogExecution('ERROR', 'pagevalue',pagevalue);
				//if(pagevalue!=null)
if(pagevalue!=null&&(parseFloat(selectedPageArray[1])<=orderListArray.length))
				{

					if(parseFloat(diff)==parseFloat(pagevalue)|| test==selectno)
					{

						var selectedPageArray=selectno.split(',');	
						nlapiLogExecution('ERROR', 'selectedPageArray.length ', selectedPageArray.length);  
						minval=parseFloat(selectedPageArray[0])-1;
						nlapiLogExecution('ERROR', 'selectedPageArray[0] ', selectedPageArray[0]);  
						maxval=parseFloat(selectedPageArray[1]);
						nlapiLogExecution('ERROR', 'selectedPageArray[1] ', selectedPageArray[1]);  
					}
				}
			}


			var c=0;
			var minvalue;
			minvalue=minval;
			var index=1;
			for(var j = minvalue; j < maxval; j++){
				
				var searchresult = orderListArray[j];
				 
				vtasktype=orderListArray[j][0];
				vsku=orderListArray[j][1];
				vskudesc=orderListArray[j][2];
				vbatchno=orderListArray[j][3];
				vskuStatus=orderListArray[j][4];
				vactbeginloc=orderListArray[j][5];
				vactendloc=orderListArray[j][6];
				vqty=orderListArray[j][7];
				vorderid=orderListArray[j][8];
				vtransdate=orderListArray[j][9];
				
				

				if(vtasktype==null)
				{
					vtasktype='';
				}
				if(vsku==null)
				{
					vsku='';
				}
				if(vskudesc==null)
				{
					vskudesc='';
				}
				if(vbatchno==null)
				{
					vbatchno='';
				}
				if(vskuStatus==null)
				{
					vskuStatus='';
				}
				if(vactbeginloc==null)
				{
					vactbeginloc='';
				}
				if(vactendloc==null)
				{
					vactendloc='';
				}
				if(vqty==null)
				{
					vqty='';
				}
				if(vorderid==null)
				{
					vorderid='';
				}
				if(vtransdate==null)
				{
					vtransdate='';
				}


				form.getSubList('custpage_results').setLineItemValue('custpage_slno', index, (parseFloat(j)+1).toString());
				form.getSubList('custpage_results').setLineItemValue('custpage_sopo', index, vorderid.toString());
				form.getSubList('custpage_results').setLineItemValue('custpage_tasktype', index, vtasktype);
				form.getSubList('custpage_results').setLineItemValue('custpage_item', index, vsku);
				form.getSubList('custpage_results').setLineItemValue('custpage_description', index, vskudesc.toString());
				form.getSubList('custpage_results').setLineItemValue('custpage_lotbat', index, vbatchno);
				form.getSubList('custpage_results').setLineItemValue('custpage_status', index, vskuStatus);
				form.getSubList('custpage_results').setLineItemValue('custpage_beginlocation', index, vactbeginloc);
				form.getSubList('custpage_results').setLineItemValue('custpage_endlocation', index, vactendloc);
				form.getSubList('custpage_results').setLineItemValue('custpage_quantity', index, vqty);
				form.getSubList('custpage_results').setLineItemValue('custpage_transdate', index, vtransdate);
				
				index=index+1;

			}
		}
	}
}



var searchArray=new Array();
var searchArray1=new Array();
var searchArray2=new Array();
var searchArray3=new Array();

function getSearchResultsOpenTask(maxno,filter,columns)
{
	filter.push(new nlobjSearchFilter('id',null,'greaterthan',maxno));
	var search=nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filter, columns);
	searchArray.push(search);
	if(search.length >= 1000)
	{
		maxno=search[search.length-1].getValue(columns[8]);
		getSearchResultsOpenTask(maxno,filter,columns);
	}
	return searchArray ;
}

function getSearchResultsClosedTask(maxno,filter,columns)
{
	filter.push(new nlobjSearchFilter('id',null,'greaterthan',maxno));
	var search=nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filter, columns);
	searchArray1.push(search);
	if(search.length >= 1000)
	{
		maxno=search[search.length-1].getValue(columns[8]);
		getSearchResultsOpenTask(maxno,filter,columns);
	}
	return searchArray1 ;
}

function getSearchResultsInvAdj(maxno,filter,columns)
{
	filter.push(new nlobjSearchFilter('id',null,'greaterthan',maxno));
	var search=nlapiSearchRecord('customrecord_ebiznet_invadj', null, filter, columns);
	searchArray2.push(search);
	if(search.length >= 1000)
	{
		maxno=search[search.length-1].getValue(columns[6]);
		getSearchResultsOpenTask(maxno,filter,columns);
	}
	return searchArray2 ;
}

function getSearchResultsCreateInvt(maxno,filter,columns)
{
	filter.push(new nlobjSearchFilter('id',null,'greaterthan',maxno));
	var search=nlapiSearchRecord('customrecord_ebiznet_createinv', null, filter, columns);
	searchArray3.push(search);
	if(search.length >= 1000)
	{
		maxno=search[search.length-1].getValue(columns[7]);
		getSearchResultsOpenTask(maxno,filter,columns);
	}
	return searchArray3 ;
}


function DisplayOpenTask(searchvalue,count,countline,form)
{
	nlapiLogExecution('ERROR','SERACHVALUEopentask'+count,searchvalue.length);
	var vtasktyep,vsku,vskudesc,vbatchno,vskuStatus,vactbeginloc,vorderid,vactendloc='';
	for(var i=0;i<searchvalue.length;i++)
	{
		var searchresult=searchvalue[i];
		vtasktype=searchresult.getText('custrecord_tasktype');
		vsku=searchresult.getText('custrecord_sku');
		vskudesc=searchresult.getValue('custrecord_skudesc');
		vbatchno=searchresult.getText('custrecord_batch_no');
		vskuStatus=searchresult.getText('custrecord_sku_status');
		vactbeginloc=searchresult.getText('custrecord_actbeginloc');
		vactendloc=searchresult.getText('custrecord_actendloc');
		vqty=searchresult.getValue('custrecord_act_qty');
		vorderid=searchresult.getText('custrecord_ebiz_order_no');

		if(vtasktype==null)
		{
			vtasktype='';
		}
		if(vsku==null)
		{
			vsku='';
		}
		if(vskudesc==null)
		{
			vskudesc='';
		}
		if(vbatchno==null)
		{
			vbatchno='';
		}
		if(vskuStatus==null)
		{
			vskuStatus='';
		}
		if(vactbeginloc==null)
		{
			vactbeginloc='';
		}
		if(vactendloc==null)
		{
			vactendloc='';
		}
		if(vqty==null)
		{
			vqty='';
		}
		if(vorderid==null||vorderid=="")
		{
			vorderid='';
		}

		form.getSubList('custpage_results').setLineItemValue('custpage_slno', countline, ""+countline);
		form.getSubList('custpage_results').setLineItemValue('custpage_sopo', countline, vorderid);
		form.getSubList('custpage_results').setLineItemValue('custpage_tasktype', countline, vtasktype);
		form.getSubList('custpage_results').setLineItemValue('custpage_item', countline, vsku);
		form.getSubList('custpage_results').setLineItemValue('custpage_description', countline, vskudesc);
		form.getSubList('custpage_results').setLineItemValue('custpage_lotbat', countline, vbatchno);
		form.getSubList('custpage_results').setLineItemValue('custpage_status', countline, vskuStatus);
		form.getSubList('custpage_results').setLineItemValue('custpage_beginlocation', countline, vactbeginloc);
		form.getSubList('custpage_results').setLineItemValue('custpage_endlocation', countline, vactendloc);
		form.getSubList('custpage_results').setLineItemValue('custpage_quantity', countline, vqty);
		countline++;
	}
	nlapiLogExecution('ERROR','1stSearch'+count,countline);	
}

function DisplayClosedTask(searchvalue,count,countline,form)
{
	nlapiLogExecution('ERROR','SERACHvalueClosedTask'+count,searchvalue.length);
	var vtasktyep,vsku,vskudesc,vbatchno,vskuStatus,vactbeginloc,vorderid,vactendloc='';
	for(var i=0;i<searchvalue.length;i++)
	{
		var searchresult=searchvalue[i];
		vtasktype=searchresult.getText('custrecord_ebiztask_tasktype');
		vsku=searchresult.getText('custrecord_ebiztask_sku');
		vskudesc=searchresult.getValue('custrecord_ebiztask_skudesc');
		vbatchno=searchresult.getValue('custrecord_ebiztask_batch_no');
		vskuStatus=searchresult.getText('custrecord_ebiztask_sku_status');
		vactbeginloc=searchresult.getText('custrecord_ebiztask_actbeginloc');
		vactendloc=searchresult.getText('custrecord_ebiztask_actendloc');
		vqty=searchresult.getValue('custrecord_ebiztask_act_qty');
		vorderid=searchresult.getText('custrecord_ebiztask_ebiz_order_no');

		if(vtasktype==null)
		{
			vtasktype='';
		}
		if(vsku==null)
		{
			vsku='';
		}
		if(vskudesc==null)
		{
			vskudesc='';
		}
		if(vbatchno==null)
		{
			vbatchno='';
		}
		if(vskuStatus==null)
		{
			vskuStatus='';
		}
		if(vactbeginloc==null)
		{
			vactbeginloc='';
		}
		if(vactendloc==null)
		{
			vactendloc='';
		}
		if(vqty==null)
		{
			vqty='';
		}
		if(vorderid==null||vorderid=="")
		{
			vorderid='';
		}

		form.getSubList('custpage_results').setLineItemValue('custpage_slno', countline, ""+ countline);
		form.getSubList('custpage_results').setLineItemValue('custpage_tasktype', countline, vorderid);
		form.getSubList('custpage_results').setLineItemValue('custpage_tasktype', countline, vtasktype);
		form.getSubList('custpage_results').setLineItemValue('custpage_item', countline, vsku);
		form.getSubList('custpage_results').setLineItemValue('custpage_description', countline, vskudesc);
		form.getSubList('custpage_results').setLineItemValue('custpage_lotbat', countline, vbatchno);
		form.getSubList('custpage_results').setLineItemValue('custpage_status', countline, vskuStatus);
		form.getSubList('custpage_results').setLineItemValue('custpage_beginlocation', countline, vactbeginloc);
		form.getSubList('custpage_results').setLineItemValue('custpage_endlocation', countline, vactendloc);
		form.getSubList('custpage_results').setLineItemValue('custpage_quantity', countline, vqty);
		countline++;
	}
	nlapiLogExecution('ERROR','2stSearch'+count,countline);
}

function DisplayInvAdj(searchvalue,count,countline,form)
{

	nlapiLogExecution('ERROR','SERACHvalueInvAdj'+count,searchvalue.length);
	var vtasktyep,vsku,vskudesc,vbatchno,vskuStatus,vactbeginloc,vactendloc='';
	var vorderid='';
	for(var i=0;i<searchvalue.length;i++)
	{
		var searchresult=searchvalue[i];
		vtasktype=searchresult.getText('custrecord_ebiz_tasktype');
		vsku=searchresult.getText('custrecord_ebizskuno');
		vskudesc=searchresult.getValue('description','custrecord_ebizskuno');
		vbatchno='';
		vskuStatus=searchresult.getText('custrecord_skustatus');
		vactbeginloc=searchresult.getText('custrecord_ebiz_binloc');
		vactendloc='';
		vqty=searchresult.getValue('custrecord_ebiz_adjustqty');

		if(vtasktype==null)
		{
			vtasktype='';
		}
		if(vsku==null)
		{
			vsku='';
		}
		if(vskudesc==null)
		{
			vskudesc='';
		}
		if(vbatchno==null)
		{
			vbatchno='';
		}
		if(vskuStatus==null)
		{
			vskuStatus='';
		}
		if(vactbeginloc==null)
		{
			vactbeginloc='';
		}
		if(vactendloc==null)
		{
			vactendloc='';
		}
		if(vqty==null)
		{
			vqty='';
		}

		form.getSubList('custpage_results').setLineItemValue('custpage_slno', countline, ""+ countline);
		form.getSubList('custpage_results').setLineItemValue('custpage_tasktype', countline, vorderid);
		form.getSubList('custpage_results').setLineItemValue('custpage_tasktype', countline, vtasktype);
		form.getSubList('custpage_results').setLineItemValue('custpage_item', countline, vsku);
		form.getSubList('custpage_results').setLineItemValue('custpage_description', countline, vskudesc);
		form.getSubList('custpage_results').setLineItemValue('custpage_lotbat', countline, vbatchno);
		form.getSubList('custpage_results').setLineItemValue('custpage_status', countline, vskuStatus);
		form.getSubList('custpage_results').setLineItemValue('custpage_beginlocation', countline, vactbeginloc);
		form.getSubList('custpage_results').setLineItemValue('custpage_endlocation', countline, vactendloc);
		form.getSubList('custpage_results').setLineItemValue('custpage_quantity', countline, vqty);
		countline++;

	}
	nlapiLogExecution('ERROR','3rdSearch'+count,countline);
}

function DisplayCreateInv(searchvalue,count,countline,form)
{

	nlapiLogExecution('ERROR','SERACHvalueCreateInv'+count,searchvalue.length);
	var vtasktyep,vsku,vskudesc,vbatchno,vskuStatus,vactbeginloc,vorderid,vactendloc='';
	for(var i=0;i<searchvalue.length;i++)
	{
		var searchresult=searchvalue[i];
		vtasktype=searchresult.getText('custrecord_invttasktype');
		vsku=searchresult.getText('custrecord_ebiz_inv_sku');
		vskudesc=searchresult.getValue('description','custrecord_ebiz_inv_sku');
		vbatchno=searchresult.getText('custrecord_ebiz_inv_lot');
		vskuStatus=searchresult.getText('custrecord_ebiz_inv_sku_status');
		vactbeginloc=searchresult.getText('custrecord_ebiz_inv_binloc');
		vactendloc='';
		vqty=searchresult.getValue('custrecord_ebiz_inv_qty');
		vorderid=searchresult.getText('custrecord_ebiz_transaction_no');

		if(vtasktype==null)
		{
			vtasktype='';
		}
		if(vsku==null)
		{
			vsku='';
		}
		if(vskudesc==null)
		{
			vskudesc='';
		}
		if(vbatchno==null)
		{
			vbatchno='';
		}
		if(vskuStatus==null)
		{
			vskuStatus='';
		}
		if(vactbeginloc==null)
		{
			vactbeginloc='';
		}
		if(vactendloc==null)
		{
			vactendloc='';
		}
		if(vqty==null)
		{
			vqty='';
		}
		if(vorderid==null)
		{
			vorderid='';
		}

		form.getSubList('custpage_results').setLineItemValue('custpage_slno', countline, ""+ countline);
		form.getSubList('custpage_results').setLineItemValue('custpage_tasktype', countline, vorderid);
		form.getSubList('custpage_results').setLineItemValue('custpage_tasktype', countline, vtasktype);
		form.getSubList('custpage_results').setLineItemValue('custpage_item', countline, vsku);
		form.getSubList('custpage_results').setLineItemValue('custpage_description', countline, vskudesc);
		form.getSubList('custpage_results').setLineItemValue('custpage_lotbat', countline, vbatchno);
		form.getSubList('custpage_results').setLineItemValue('custpage_status', countline, vskuStatus);
		form.getSubList('custpage_results').setLineItemValue('custpage_beginlocation', countline, vactbeginloc);
		form.getSubList('custpage_results').setLineItemValue('custpage_endlocation', countline, vactendloc);
		form.getSubList('custpage_results').setLineItemValue('custpage_quantity', countline, vqty);
		countline++;

	}
	nlapiLogExecution('ERROR','4thSearch'+count,countline);
	
}