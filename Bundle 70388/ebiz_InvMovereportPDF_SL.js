/***************************************************************************
	  		   eBizNET Solutions Inc               
 ****************************************************************************/
/* Prologue - 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Reports/Suitelet/ebiz_InvMovereportPDF_SL.js,v $
 *     	   $Revision: 1.3.2.3.8.1 $
 *     	   $Date: 2015/07/02 15:23:32 $
 *     	   $Author: grao $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 * Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_InvMovereportPDF_SL.js,v $
 * Revision 1.3.2.3.8.1  2015/07/02 15:23:32  grao
 * 2015.2 issue fixes  201413073 and 201413114
 *
 * Revision 1.3.2.3  2012/06/11 22:25:10  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT and Prod issue fixes
 *
 * Revision 1.3.2.2  2012/05/14 13:58:33  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Inventory Report
 *
 * Revision 1.3.2.1  2012/04/20 12:11:46  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.3  2011/12/19 10:52:04  spendyala
 * CASE201112/CR201113/LOG201121
 * Added vendor part code and store bin location fields to the sublist.
 *
 * Revision 1.2  2011/12/14 18:42:24  gkalla
 * CASE201112/CR201113/LOG201121
 * Changed Deployment ids
 *
 * Revision 1.1  2011/12/14 14:18:00  mbpragada
 * CASE201112/CR201113/LOG201121
 * Inventory Move report  and pdf new files
 *
 
 *

 *****************************************************************************/

function InventoryMoveReportPDFSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		var form = nlapiCreateForm('Inventory Move Report');
		nlapiLogExecution('ERROR', 'into report', 'done');
		var binlocid = request.getParameter('custparam_binlocation');
		nlapiLogExecution('ERROR', 'binLoc Internal ID', binlocid);
	    var vfromdate=request.getParameter('custparam_fromdate');
	    var vtodate=request.getParameter('custparam_todate');
	    var vreportno=request.getParameter('custparam_reportno');
	    
	    //var replaceChar =/\$|,|@|#|~|`|\%|\*|\^|\&|\+|\=|\-|\_|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|\./g;
	    var replaceChar =/\$|,|@|#|~|`|\%|\*|\^|\&|\+|\=|\_|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\!|\$|\./g;
	    
		var itemId = request.getParameter('custparam_item');
		var vItemArr=new Array();
		if(itemId != null && itemId != "")
		{
			vItemArr = itemId.split('');
		}
		nlapiLogExecution('ERROR', 'item Internal ID', vItemArr);

		var LPvalue = request.getParameter('custparam_invtlp');
		nlapiLogExecution('ERROR', 'LP value', LPvalue);

		var vStatus = request.getParameter('custparam_itemstatus');
		nlapiLogExecution('ERROR', 'Item Status ', vStatus);

		var i = 0;
		var filtersinv = new Array();
		
		if (itemId != "") {
			filtersinv[i] = new nlobjSearchFilter('custrecord_sku', null, 'anyof', vItemArr);
			i++;
		}
		
		if (binlocid!=null && binlocid != "") {
			filtersinv[i] = new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', binlocid);
			i++;
		}
		if (itemId != "" && itemId !=null) {
			filtersinv[i] = new nlobjSearchFilter('custrecord_sku', null, 'anyof', vItemArr);
			i++;
		}
		if (vreportno != "" && vreportno !=null) {
			filtersinv[i] = new nlobjSearchFilter('custrecord_ebiz_report_no', null, 'is', vreportno);
			i++;
		}
		if (LPvalue != "" && LPvalue !=null) {
			filtersinv[i] = new nlobjSearchFilter('custrecord_from_lp_no', null, 'is', LPvalue);
			i++;
		}
		if (request.getParameter('custparam_location') != "") {
			filtersinv[i] = new nlobjSearchFilter('custrecord_wms_location', null, 'is', request.getParameter('custparam_location'));
			i++;
		}
		if (request.getParameter('custparam_company') != "") {
			filtersinv[i] = new nlobjSearchFilter('custrecord_comp_id', null, 'is', request.getParameter('custparam_company'));
			i++;
		}
		if (vStatus != null && vStatus != "") {
			filtersinv[i] = new nlobjSearchFilter('custrecord_sku_status', null, 'is', vStatus);
			i++;
		}
		
		filtersinv[i] = new nlobjSearchFilter('custrecord_sku_status', null, 'noneof', ['6']);
		i++;
		
		if ( (vfromdate != null && vfromdate != "") && (vtodate != null && vtodate != "") ){
			filtersinv[i] = new nlobjSearchFilter('custrecord_current_date', null, 'within', vfromdate, vtodate);		
			i++;
		}

		filtersinv[i] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['9','18']);
		i++;
		//filtersinv[i] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof',['3','19']);
		


		var columns = new Array();
		
		columns.push(new nlobjSearchColumn('internalid').setSort('true'));
		columns.push(new nlobjSearchColumn('name'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
		columns.push(new nlobjSearchColumn('custrecord_act_qty'));
		columns.push(new nlobjSearchColumn('custrecord_lpno'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_lpno'));
		columns.push(new nlobjSearchColumn('custrecord_tasktype'));
		columns.push(new nlobjSearchColumn('custrecord_actbeginloc'));;
		columns.push(new nlobjSearchColumn('custrecord_actendloc'));;

		columns.push(new nlobjSearchColumn('custrecord_sku'));
		columns.push(new nlobjSearchColumn('custrecord_skudesc'));
		columns.push(new nlobjSearchColumn('custrecord_batch_no'));
		columns.push(new nlobjSearchColumn('custrecord_sku_status'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_new_sku_status'));
		columns.push(new nlobjSearchColumn('custrecord_packcode'));;		
		columns.push(new nlobjSearchColumn('custrecord_lotnowithquantity'));
		columns.push(new nlobjSearchColumn('custrecord_expe_qty'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_new_lp'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_ot_adjtype'));
		columns.push(new nlobjSearchColumn('custrecord_from_lp_no'));
		columns.push(new nlobjSearchColumn('custrecord_wms_location'));
		columns.push(new nlobjSearchColumn('custrecord_ebiz_report_no'));
		//new fields
		columns.push(new nlobjSearchColumn('custitemstore_bin_location','custrecord_sku'));
		columns.push(new nlobjSearchColumn('vendorname','custrecord_sku'));
		columns.push(new nlobjSearchColumn('purchasedescription','custrecord_sku'));

		var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersinv, columns);
		
		
		
		var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body  font-size=\"5\">\n";
		nlapiLogExecution('ERROR','xml0 ',"<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body font-size=\"5\">\n");

		var strxml ="<table width='100%' >";
		strxml += "<tr ><td valign='bottom' align='center'  style='font-size:xx-large;'>";
		strxml += "Inventory Move Report";
		strxml += "</td>        </tr>   <tr><td>&nbsp;</td></tr> </table>";

		strxml +="<table  border='1' width='100%'> ";
		strxml =strxml+  "<tr style=\"font-weight:bold;background-color:gray;color:white;\"><td width='7%' style='border-width: 1px; border-color: #000000'>";
		strxml += "Bin Location";
		strxml += "</td>";
		strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
		strxml += "Item";
		strxml += "</td>";
		strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
		strxml += "Item Description";
		strxml += "</td>";
		strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
		strxml += "Vendor Part Code";
		strxml += "</td>";
//		strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
		strxml += "Item Status";
		strxml += "</td>";
		strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
		strxml += "Pack Code";
		strxml += "</td>";
		strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
		strxml += "LOT/Batch #";
		strxml += "</td>";	
		strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";			
		strxml += "LP#";
		strxml += "</td>";	
		strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";			
		strxml += "Old Quantity";
		strxml += "</td>";	
		strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";			
		strxml += "New Quantity";
		strxml += "</td>";	
		strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";			
		strxml += "New Item Status";
		strxml += "</td>";	
		strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";			
		strxml += "New Bin Location";
		strxml += "</td>";
		strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";			
		strxml += "New LP";
		strxml += "</td>";
		strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";			
		strxml += "Adjustment Type";
		strxml += "</td>";
		strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
		strxml += "Report #";
		strxml += "</td>";
		strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
		strxml += "Store Bin Location";
		strxml =strxml+  "</td></tr>";
	 
		if (invtsearchresults != null && invtsearchresults !="") {
			for (var i = 0; i < invtsearchresults.length; i++) {				 
				
				var invtsearchresult=invtsearchresults[i];
				
				var locationname = invtsearchresult.getText('custrecord_actbeginloc');
				var item = invtsearchresult.getValue('custrecord_sku');
				var itemname = invtsearchresult.getText('custrecord_sku');
				var lp = invtsearchresult.getValue('custrecord_from_lp_no');						 
				var lot = invtsearchresult.getValue('custrecord_lotnowithquantity');
				var lotText = invtsearchresult.getText('custrecord_lotnowithquantity');					
				var itemstatus = invtsearchresult.getText('custrecord_sku_status');
				var packcode = invtsearchresult.getValue('custrecord_packcode');					 
										
				//new
				var newitemstatus = invtsearchresult.getText('custrecord_ebiz_new_sku_status');
				var newbinloc = invtsearchresult.getText('custrecord_actendloc');
				var newqty = invtsearchresult.getValue('custrecord_expe_qty');
				var newlp = invtsearchresult.getValue('custrecord_ebiz_new_lp');
				var AdjustmentType = invtsearchresult.getText('custrecord_ebiz_ot_adjtype');
				var qty = invtsearchresult.getValue('custrecord_act_qty');
				var linereportno = invtsearchresult.getValue('custrecord_ebiz_report_no');

				var storbinlocation='';
                var vendornamefield='';
                var itemdesc='';
                storbinlocation=invtsearchresult.getValue('custitemstore_bin_location','custrecord_sku');
                vendornamefield=invtsearchresult.getValue('vendorname','custrecord_sku');
                itemdesc=invtsearchresult.getValue('purchasedescription','custrecord_sku');
                nlapiLogExecution('ERROR','storbinlocation',storbinlocation);
                nlapiLogExecution('ERROR','vendornamefield',vendornamefield);
                nlapiLogExecution('ERROR','itemname before replace',itemname);
                nlapiLogExecution('ERROR','itemdesc before replace',itemdesc);
                
                //if(itemname!=null && itemname!='')
                	//itemname=itemname.replace(replaceChar,'');
                
                if(itemdesc!=null && itemdesc!='')
                	itemdesc=itemdesc.replace(replaceChar,'');
                
                nlapiLogExecution('ERROR','itemname after replace',itemname);
                nlapiLogExecution('ERROR','itemdesc after replace',itemdesc);
				strxml =strxml+  "<tr><td width='7%' style='border-width: 1px; border-color: #000000'>";				
				strxml += locationname;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
				strxml += itemname.replace(/&/g,"&amp;");
				//strxml +=  itemname;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
				//strxml +=  itemdesc;
				strxml += itemdesc.replace(/&/g,"&amp;");
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
				strxml +=  vendornamefield;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
				strxml += itemstatus;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
				strxml += packcode;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
				strxml += lot;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";			
				strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
				strxml += lp;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'> ";					
				strxml += newqty;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";				
				strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
				strxml += qty;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";			
				strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
				strxml += newitemstatus;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";				
				strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
				strxml += newbinloc;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";					
				strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
				strxml += newlp;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";	
				strxml += AdjustmentType;
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";	
				strxml += linereportno;	
				strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				strxml += "<td width='7%' style='border-width: 1px; border-color: #000000'>";
				strxml +=  storbinlocation;
				strxml =strxml+  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
			}
		}
		strxml =strxml+"</table>";		
		strxml =strxml+ "\n</body>\n</pdf>";		
		nlapiLogExecution('ERROR', 'strxml', strxml);
		xml=xml +strxml;
		nlapiLogExecution('ERROR','XML',xml);

		var file = nlapiXMLToPDF(xml);	
		response.setContentType('PDF','InventoryMove.pdf');
		response.write( file.getValue() );
	}
	else //this is the POST block
	{

	}
}
