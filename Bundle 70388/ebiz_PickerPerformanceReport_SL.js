/* 
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Common/Suitelet/GeneralFunctions.js,v $
 *     	   $Revision: 1.86 $
 *     	   $Date: 2011/11/25 10:17:48 $
 *     	   $Author: rmukkera $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * 
 * PRAMETERS
 *
 * DESCRIPTION
 * 
 * REVISION HISTORY
 * $Log: GeneralFunctions.js,v $ */


function PickerPerformanceReport_SL(request,response)
{
	if (request.getMethod() == 'GET')
	{
		var form = nlapiCreateForm('Picker Performance Report');

		form.setScript('customscript_ebiz_pickerclientvalidation');
		//QB Fields Declaration
		var loadno = form.addField('custpage_load', 'text', 'Load #');	
		var fromdate = form.addField('custpage_fromdate', 'Date', 'Start Time');			
		var pullerid = form.addField('custpage_pullarid', 'select', 'Puller Id','Employee');		
		var todate = form.addField('custpage_todate', 'Date', 'Finish Time');

		var ordlinecolumns = new Array(); 	
		ordlinecolumns[0] = new nlobjSearchColumn('custrecord_linestatus_flag');	
		ordlinecolumns[1] = new nlobjSearchColumn('custrecord_ebiz_wave');			
		var ordlinefilters= new Array();
		ordlinefilters[0] = new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', ['15']);
		var ordlineSearchResults= nlapiSearchRecord('customrecord_ebiznet_ordline', null, ordlinefilters, ordlinecolumns);		
          
		var ordlineSearchResultsArr=new Array();
		//calling the unique function to get unique loadno from  ordlineSearchResults.
    	
		
		var  sublist = form.addSubList('custpage_pickerperformance','list','Picker Performance Report Details'); 
		sublist.addField('custrecord_select','checkbox', 'Select');	 
		sublist.addField('custrecord_loadno','text', 'Load #');	 
		sublist.addField('custrecord_pagesno','text', '# Pages').setDisplayType('entry');	  		
		sublist.addField('custrecord_palletsno','text', '# Pallets').setDisplayType('entry'); 
		sublist.addField('custrecord_cartons','text', '# Cartons').setDisplayType('entry');
		sublist.addField('custrecord_loadspulled','text', 'Loads Pulled');
		sublist.addField('custrecord_performance','text', 'Performance');
		sublist.addField('custrecord_pullerid','select', 'Puller Id','Employee');
	    var starttimefld=sublist.addField('custrecord_starttime','datetimetz', 'Start Time<br>(MM/DD/YYYY HH:MI)').setDisplayType('entry');
		var endtimefld=sublist.addField('custrecord_endtime','datetimetz', 'Finish Time<br>(MM/DD/YYYY HH:MI)').setDisplayType('entry');
		sublist.addField('custrecord_duration','text', 'Duration<br>(HH:MI:SS)');	
		starttimefld.setDisplaySize(20, 20); 
		endtimefld.setDisplaySize(20, 20); 
	   
	    	var pickerperformancecolumns = new Array(); 	
	        pickerperformancecolumns[0] = new nlobjSearchColumn('custrecord_ebiz_loadno');	
	        pickerperformancecolumns[1] = new nlobjSearchColumn('custrecord_ebiz_pullerid');	
	        pickerperformancecolumns[2] = new nlobjSearchColumn('custrecord_ebiz_noofpages');	
	        pickerperformancecolumns[3] = new nlobjSearchColumn('custrecord_ebiz_noofpallets');	
	        pickerperformancecolumns[4] = new nlobjSearchColumn('custrecord_ebiz_noofcartons');	
	        pickerperformancecolumns[5] = new nlobjSearchColumn('custrecord_ebiz_loadspulled');	
	        pickerperformancecolumns[6] = new nlobjSearchColumn('custrecord_ebiz_performance');	
	        pickerperformancecolumns[7] = new nlobjSearchColumn('custrecord_ebiz_starttime');	
	        pickerperformancecolumns[8] = new nlobjSearchColumn('custrecord_ebiz_finishtime');	
	        pickerperformancecolumns[9] = new nlobjSearchColumn('custrecord_ebiz_duration');
			
				var pickerPerformanceSearchResults= nlapiSearchRecord('customrecord_ebiz_pickerperformance_rpt', null, null, pickerperformancecolumns );
		
				var pickerPerformanceArr=new Array();
			for (var j = 0; pickerPerformanceSearchResults != null && j < pickerPerformanceSearchResults.length; j++) {
					pickerPerformanceArr[j]=pickerPerformanceSearchResults[j].getValue('custrecord_ebiz_loadno');
				}
			ordlineSearchResultsArr=unique(ordlineSearchResults,pickerPerformanceArr);
				nlapiLogExecution('ERROR', 'ordlineSearchResultsArrlength ', ordlineSearchResultsArr.length); 
				var date=DateStamp();var time =TimeStamp();
				var datetime=date+" "+time;
				for (var i = 0; ordlineSearchResultsArr != null && i < ordlineSearchResultsArr.length; i++) {
					var loadno=ordlineSearchResultsArr[i];			
					form.getSubList('custpage_pickerperformance').setLineItemValue('custrecord_loadno', i + 1, loadno);
					//form.getSubList('custpage_pickerperformance').setLineItemValue('custrecord_ebiz_starttime', i + 1, datetime);
					starttimefld.setDefaultValue(datetime);
				}

		form.addSubmitButton('Submit');
		response.writePage(form);
	}
	else
	{
		var form = nlapiCreateForm('Picker Performance Report');
		form.setScript('customscript_ebiz_pickerclientvalidation');
		//QB Fields Declaration
		var loadno = form.addField('custpage_load', 'text', 'Load #');		
		var fromdate = form.addField('custpage_fromdate', 'Date', 'Start Time');
		var pullerid = form.addField('custpage_pullarid', 'select', 'Puller Id','Employee');
		var todate = form.addField('custpage_todate', 'Date', 'Finish Time');		 
		form.addSubmitButton('Submit');

		if ((request.getParameter('custpage_load') != "" && request.getParameter('custpage_load') != null)  ) {
			loadno.setDefaultValue(request.getParameter('custpage_load'));

		} 
		if ((request.getParameter('custpage_pullarid') != "" && request.getParameter('custpage_pullarid') != null)  ) {
			pullerid.setDefaultValue(request.getParameter('custpage_pullarid'));
		} 
		if ((request.getParameter('custpage_fromdate') != "" && request.getParameter('custpage_fromdate') != null)  ) {
			fromdate.setDefaultValue(request.getParameter('custpage_fromdate'));
		} 
		if ((request.getParameter('custpage_todate') != "" && request.getParameter('custpage_todate') != null)  ) {
			todate.setDefaultValue(request.getParameter('custpage_todate'));
		} 
		var lineCount = request.getLineItemCount('custpage_pickerperformance');


		nlapiLogExecution('ERROR', 'lineCount ', lineCount);  


		var pickerperformancecolumns = new Array(); 	
		pickerperformancecolumns[0] = new nlobjSearchColumn('custrecord_ebiz_loadno');	
		pickerperformancecolumns[1] = new nlobjSearchColumn('custrecord_ebiz_pullerid');	
		pickerperformancecolumns[2] = new nlobjSearchColumn('custrecord_ebiz_noofpages');	
		pickerperformancecolumns[3] = new nlobjSearchColumn('custrecord_ebiz_noofpallets');	
		pickerperformancecolumns[4] = new nlobjSearchColumn('custrecord_ebiz_noofcartons');	
		pickerperformancecolumns[5] = new nlobjSearchColumn('custrecord_ebiz_loadspulled');	
		pickerperformancecolumns[6] = new nlobjSearchColumn('custrecord_ebiz_performance');	
		pickerperformancecolumns[7] = new nlobjSearchColumn('custrecord_ebiz_starttime');	
		pickerperformancecolumns[8] = new nlobjSearchColumn('custrecord_ebiz_finishtime');	
		pickerperformancecolumns[9] = new nlobjSearchColumn('custrecord_ebiz_duration');


		for (var i = 0;i < lineCount; i++) {
			nlapiLogExecution('ERROR', 'into forloop1 ', '');  
			var chk= request.getLineItemValue('custpage_pickerperformance', 'custrecord_select', i+1);
			nlapiLogExecution('ERROR', 'chkboxvalue ', chk);  
			if(chk=='T')
			{
				var loadno= request.getLineItemValue('custpage_pickerperformance', 'custrecord_loadno', i+1);
				var Pages=  request.getLineItemValue('custpage_pickerperformance', 'custrecord_pagesno', i+1);
				var Pallets=request.getLineItemValue('custpage_pickerperformance', 'custrecord_palletsno', i+1);
				var cartons=request.getLineItemValue('custpage_pickerperformance', 'custrecord_cartons', i+1);
				var loadspulled=request.getLineItemValue('custpage_pickerperformance', 'custrecord_loadspulled', i+1);
				var performance=request.getLineItemValue('custpage_pickerperformance', 'custrecord_performance', i+1);
				var pullerid=request.getLineItemValue('custpage_pickerperformance', 'custrecord_pullerid', i+1);
				var starttime=request.getLineItemValue('custpage_pickerperformance', 'custrecord_starttime', i+1);
				var finishtime=request.getLineItemValue('custpage_pickerperformance', 'custrecord_endtime', i+1);
				var duration=request.getLineItemValue('custpage_pickerperformance', 'custrecord_duration', i+1);

				if(finishtime!='' && finishtime!=null && starttime!='' && starttime!=null)
				{
					duration=get_time_difference(parseDate(starttime),parseDate(finishtime));
					nlapiLogExecution('ERROR', 'duration ', duration);  
				}


				var pickerperformancefilters= new Array();
				pickerperformancefilters[0] = new nlobjSearchFilter('name', null, 'is', loadno);
				var pickerPerformanceSearchResults= nlapiSearchRecord('customrecord_ebiz_pickerperformance_rpt', null, pickerperformancefilters, pickerperformancecolumns);
				var pickerrecord;
				if(pickerPerformanceSearchResults==null)
				{
					pickerrecord = nlapiCreateRecord('customrecord_ebiz_pickerperformance_rpt');
				}
				else
				{
					pickerrecord = nlapiLoadRecord('customrecord_ebiz_pickerperformance_rpt',pickerPerformanceSearchResults[0].getId());
				}

				pickerrecord.setFieldValue('name',loadno);
				pickerrecord.setFieldValue('custrecord_ebiz_loadno',loadno);
				pickerrecord.setFieldValue('custrecord_ebiz_noofpages',Pages);
				pickerrecord.setFieldValue('custrecord_ebiz_pullerid', pullerid );
				pickerrecord.setFieldValue('custrecord_ebiz_noofpallets',Pallets);
				pickerrecord.setFieldValue('custrecord_ebiz_noofcartons',cartons);					
				pickerrecord.setFieldValue('custrecord_ebiz_loadspulled', loadspulled);
				pickerrecord.setFieldValue('custrecord_ebiz_performance', performance);
				pickerrecord.setFieldValue('custrecord_ebiz_starttime', starttime);
				pickerrecord.setFieldValue('custrecord_ebiz_finishtime', finishtime);
				pickerrecord.setFieldValue('custrecord_ebiz_duration', duration);
				if(pickerrecord!=null)
				{
					var recid = nlapiSubmitRecord(pickerrecord);
				}

			}
		}



		nlapiLogExecution('ERROR', 'custpage_pullarid ', request.getParameter('custpage_pullarid'));
		nlapiLogExecution('ERROR', 'custpage_fromdate ', request.getParameter('custpage_fromdate'));

		//Detail fields Declaration
		var  sublist = form.addSubList('custpage_pickerperformance','list','Picker Performance Report Details'); 
		sublist.addField('custrecord_select','checkbox', 'Select');	 
		sublist.addField('custrecord_loadno','text', 'Load #');	 
		sublist.addField('custrecord_pagesno','text', '# Pages').setDisplayType('entry');		  		
		sublist.addField('custrecord_palletsno','text', '# Pallets').setDisplayType('entry');	 
		sublist.addField('custrecord_cartons','text', '# Cartons').setDisplayType('entry');	
		sublist.addField('custrecord_loadspulled','text', 'Loads Pulled');
		sublist.addField('custrecord_performance','text', 'Performance');
		sublist.addField('custrecord_pullerid','select', 'Puller Id','Employee');
	    var starttimefld=sublist.addField('custrecord_starttime','datetimetz', 'Start Time<br>(MM/DD/YYYY HH:MI)').setDisplayType('entry');	
	    var endtimefld= sublist.addField('custrecord_endtime','datetimetz', 'Finish Time<br>(MM/DD/YYYY HH:MI)').setDisplayType('entry');	
		sublist.addField('custrecord_duration','text', 'Duration<br>(HH:MI:SS)');	

		starttimefld.setDisplaySize(20, 20); 
		endtimefld.setDisplaySize(20, 20); 
		if ((request.getParameter('custpage_load') != "" && request.getParameter('custpage_load') != null) || (request.getParameter('custpage_pullarid') != "" && request.getParameter('custpage_pullarid') != null) || (request.getParameter('custpage_fromdate') != "" && request.getParameter('custpage_fromdate') != null) || (request.getParameter('custpage_todate') != "" && request.getParameter('custpage_todate') != null)  ) 
		{
			nlapiLogExecution('ERROR', 'into if of request.getParameter(custpage_load) ', request.getParameter('custpage_load'));  
			var filters	= new Array();			
			var k=0;			
			if ((request.getParameter('custpage_load') != "" &&  request.getParameter('custpage_load') != null))
			{
				filters[k]=new nlobjSearchFilter('name', null, 'is', request.getParameter('custpage_load'));
				k=k+1;
			}

			if ((request.getParameter('custpage_fromdate') != "" &&  request.getParameter('custpage_fromdate') != null))
			{
				filters[k]=new nlobjSearchFilter('custrecord_ebiz_starttime', null, 'on', request.getParameter('custpage_fromdate'));
				k=k+1;
			}
			if ((request.getParameter('custpage_todate') != "" &&  request.getParameter('custpage_todate') != null))
			{
				filters[k]=new nlobjSearchFilter('custrecord_ebiz_finishtime', null, 'on', request.getParameter('custpage_todate'));
				k=k+1;
			}
			if ((request.getParameter('custpage_pullarid') != "" &&  request.getParameter('custpage_pullarid') != null))
			{
				filters[k]=new nlobjSearchFilter('custrecord_ebiz_pullerid', null, 'is', request.getParameter('custpage_pullarid'));
			}
			var pickerPerformanceSearchResults= nlapiSearchRecord('customrecord_ebiz_pickerperformance_rpt', null, filters, pickerperformancecolumns);


	        

			var c=1;
			for (var i = 0; pickerPerformanceSearchResults != null && i < pickerPerformanceSearchResults.length; i++) {

				var tloadno=pickerPerformanceSearchResults[i].getValue('custrecord_ebiz_loadno');



					var loadno=pickerPerformanceSearchResults[i].getValue('custrecord_ebiz_loadno');
					var pullerid=pickerPerformanceSearchResults[i].getValue('custrecord_ebiz_pullerid');
					var noofpages=pickerPerformanceSearchResults[i].getValue('custrecord_ebiz_noofpages');
					var noofpallets=pickerPerformanceSearchResults[i].getValue('custrecord_ebiz_noofpallets');
					var noofcartons=pickerPerformanceSearchResults[i].getValue('custrecord_ebiz_noofcartons');
					var loadspulled=pickerPerformanceSearchResults[i].getValue('custrecord_ebiz_loadspulled');
					var performance=pickerPerformanceSearchResults[i].getValue('custrecord_ebiz_performance');
					var starttime=pickerPerformanceSearchResults[i].getValue('custrecord_ebiz_starttime');
					var finishtime=pickerPerformanceSearchResults[i].getValue('custrecord_ebiz_finishtime');
					var duration=pickerPerformanceSearchResults[i].getValue('custrecord_ebiz_duration');				


					nlapiLogExecution('ERROR', 'loadno ', loadno); 
					form.getSubList('custpage_pickerperformance').setLineItemValue('custrecord_loadno', c, loadno);
					nlapiLogExecution('ERROR', 'pullerid ', pullerid); 
					form.getSubList('custpage_pickerperformance').setLineItemValue('custrecord_pagesno', c, noofpages);
					nlapiLogExecution('ERROR', 'noofpages ', noofpages);
					form.getSubList('custpage_pickerperformance').setLineItemValue('custrecord_palletsno', c, noofpallets);
					nlapiLogExecution('ERROR', 'noofpallets ', noofpallets);
					form.getSubList('custpage_pickerperformance').setLineItemValue('custrecord_cartons', c, noofcartons);
					nlapiLogExecution('ERROR', 'noofcartons ', noofcartons);
					form.getSubList('custpage_pickerperformance').setLineItemValue('custrecord_loadspulled', c, loadspulled);
					nlapiLogExecution('ERROR', 'loadspulled ', loadspulled); 
					form.getSubList('custpage_pickerperformance').setLineItemValue('custrecord_performance', c, performance);
					nlapiLogExecution('ERROR', 'performance ', performance); 
					form.getSubList('custpage_pickerperformance').setLineItemValue('custrecord_starttime', c, starttime);
					nlapiLogExecution('ERROR', 'starttime ', starttime); 
					form.getSubList('custpage_pickerperformance').setLineItemValue('custrecord_endtime', c, finishtime);
					nlapiLogExecution('ERROR', 'finishtime ', finishtime); 
					form.getSubList('custpage_pickerperformance').setLineItemValue('custrecord_duration', c, duration);
					nlapiLogExecution('ERROR', 'duration ', duration); 
					form.getSubList('custpage_pickerperformance').setLineItemValue('custrecord_pullerid', c, pullerid);
					c=c+1;
				
			}
		}
		else
		{
			nlapiLogExecution('ERROR', 'into else of request.getParameter(custpage_load) ', request.getParameter('custpage_load'));  




			var ordlinecolumns = new Array(); 	
			ordlinecolumns[0] = new nlobjSearchColumn('custrecord_linestatus_flag');	
			ordlinecolumns[1] = new nlobjSearchColumn('custrecord_ebiz_wave');			
			var ordlinefilters= new Array();
			ordlinefilters[0] = new nlobjSearchFilter('custrecord_linestatus_flag', null, 'anyof', ['15']);
			var ordlineSearchResults= nlapiSearchRecord('customrecord_ebiznet_ordline', null, ordlinefilters, ordlinecolumns);		

			var ordlineSearchResultsArr=new Array();

			
			var pickerperformancecolumns = new Array(); 	
			pickerperformancecolumns[0] = new nlobjSearchColumn('custrecord_ebiz_loadno');	
			pickerperformancecolumns[1] = new nlobjSearchColumn('custrecord_ebiz_pullerid');	
			pickerperformancecolumns[2] = new nlobjSearchColumn('custrecord_ebiz_noofpages');	
			pickerperformancecolumns[3] = new nlobjSearchColumn('custrecord_ebiz_noofpallets');	
			pickerperformancecolumns[4] = new nlobjSearchColumn('custrecord_ebiz_noofcartons');	
			pickerperformancecolumns[5] = new nlobjSearchColumn('custrecord_ebiz_loadspulled');	
			pickerperformancecolumns[6] = new nlobjSearchColumn('custrecord_ebiz_performance');	
			pickerperformancecolumns[7] = new nlobjSearchColumn('custrecord_ebiz_starttime');	
			pickerperformancecolumns[8] = new nlobjSearchColumn('custrecord_ebiz_finishtime');	
			pickerperformancecolumns[9] = new nlobjSearchColumn('custrecord_ebiz_duration');

			var pickerPerformanceSearchResults= nlapiSearchRecord('customrecord_ebiz_pickerperformance_rpt', null, null, pickerperformancecolumns );

			var pickerPerformanceArr=new Array();
			for (var j = 0; pickerPerformanceSearchResults != null && j < pickerPerformanceSearchResults.length; j++) {
				pickerPerformanceArr[j]=pickerPerformanceSearchResults[j].getValue('custrecord_ebiz_loadno');
			}
			ordlineSearchResultsArr=unique(ordlineSearchResults,pickerPerformanceArr);
			nlapiLogExecution('ERROR', 'ordlineSearchResultsArrlength ', ordlineSearchResultsArr.length); 
			
			
			var date=DateStamp();var time =TimeStamp();
			var datetime=date+" "+time;
			for (var i = 0; ordlineSearchResultsArr != null && i < ordlineSearchResultsArr.length; i++) {
				var loadno=ordlineSearchResultsArr[i];			
				form.getSubList('custpage_pickerperformance').setLineItemValue('custrecord_loadno', i + 1, loadno);
				//form.getSubList('custpage_pickerperformance').setLineItemValue('custrecord_ebiz_starttime', i + 1, datetime);
				starttimefld.setDefaultValue(datetime);
			}
		}

		response.writePage(form);

	}
}

function unique(origArr,duparr) {  
   var newArr = [],  
       origLen,  
       found,  
       x, y;  
   var tempfound=false;
   if(origArr!=null && origArr!='')
   origLen = origArr.length
  
   for ( x = 0; x < origLen; x++ ) {  
       found = undefined;  
       if (duparr.indexOf(origArr[x].getValue('custrecord_ebiz_wave'))==-1)
    	   {
       for ( y = 0; y < newArr.length; y++ ) {    	
           if ( origArr[x].getValue('custrecord_ebiz_wave') === newArr[y] ) {  
             found = true;  
             break;  
           }  
       }  
       if ( !found && origArr[x].getValue('custrecord_ebiz_wave')!='') newArr.push( origArr[x].getValue('custrecord_ebiz_wave') );  
   }  
   }
  return newArr;  
}; 
if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}
function get_time_difference(earlierDate,laterDate)
{
	var nTotalDiff = laterDate.getTime() - earlierDate.getTime();
	var oDiff = new Object();

	//oDiff.days = Math.floor(nTotalDiff/1000/60/60/24);
	//nTotalDiff -= oDiff.days*1000*60*60*24;

	oDiff.hours = Math.floor(nTotalDiff/1000/60/60);
	nTotalDiff -= oDiff.hours*1000*60*60;

	oDiff.minutes = Math.floor(nTotalDiff/1000/60);
	nTotalDiff -= oDiff.minutes*1000*60;

	oDiff.seconds = Math.floor(nTotalDiff/1000);
	
	var dateobj1=oDiff['hours']+":"+oDiff['minutes']+":"+oDiff['seconds'];
	
	var dateobj=dateobj1.split(":");
	if(dateobj[0].length==1)
	{
		dateobj[0]="0"+dateobj[0];
	}
	if(dateobj[1].length==1)
	{
		dateobj[1]="0"+dateobj[1];
	}
	if(dateobj[2].length==1)
	{
		dateobj[2]="0"+dateobj[2];
	}
	return dateobj.join(":");

}


function parseDate(input) {
	var hourformat=input.split(' ');
	if(hourformat[2]=='pm'||hourformat[2]=='PM')
	{
		var parts = input.match(/(\d+)/g);
		var min=parts[3];
		var mins=parseInt(min)+parseInt(12);
		// new Date(year, month [, date [, hours[, minutes[, seconds[, ms]]]]])
		return new Date(parts[2], parts[0]-1, parts[1], // months are 0-based
				mins, parts[4]);
	}
	else
	{
		var parts = input.match(/(\d+)/g);
		// new Date(year, month [, date [, hours[, minutes[, seconds[, ms]]]]])
		return new Date(parts[2], parts[0]-1, parts[1], // months are 0-based
				parts[3], parts[4]);
	}
}
