/***************************************************************************
  ���������������eBizNET Solutions
 ****************************************************************************
 *
 *� $Source: /cvs/products/NSWMS/EoN/RF/Inbound/Suitelet/ebiz_RF_Cart_CheckInQty.js,v $
 *� $Revision: 1.2.2.17.4.6.2.47.2.9 $
 *� $Date: 2015/11/24 11:25:00 $
 *� $Author: aanchal $
 *� $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 * DESCRIPTION
 *� Functionality
 *
 * REVISION HISTORY
 *� $Log: ebiz_RF_Cart_CheckInQty.js,v $
 *� Revision 1.2.2.17.4.6.2.47.2.9  2015/11/24 11:25:00  aanchal
 *� 2015.2 issue fix
 *� 201415721
 *�
 *� Revision 1.2.2.17.4.6.2.47.2.8  2015/11/17 15:26:31  grao
 *� 2015.2 Issue Fixes 201415686
 *�
 *� Revision 1.2.2.17.4.6.2.47.2.7  2015/11/17 10:07:35  grao
 *� 2015.2 Issue Fixes 201415686
 *�
 *� Revision 1.2.2.17.4.6.2.47.2.6  2015/11/14 15:41:06  schepuri
 *� case# 201415555
 *�
 *� Revision 1.2.2.17.4.6.2.47.2.5  2015/11/12 13:18:19  schepuri
 *� case# 201415531,201415492
 *�
 *� Revision 1.2.2.17.4.6.2.47.2.4  2015/11/10 16:55:28  sponnaganti
 *� case# 201415557
 *� 2015.2 issue fix
 *�
 *� Revision 1.2.2.17.4.6.2.47.2.3  2015/11/06 15:49:07  grao
 *� 2015.2 Issue Fixes 201415456
 *�
 *� Revision 1.2.2.17.4.6.2.47.2.2  2015/11/05 17:40:39  grao
 *� 2015.2 Issue Fixes 201414609 and 201415087
 *�
 *� Revision 1.2.2.17.4.6.2.47.2.1  2015/09/16 15:40:57  sponnaganti
 *� 201414320
 *� We have issue while checking entered qty with recommended qty. It is calucating qty as already scanned qty plus entered qty.
 *�
 *� Revision 1.2.2.17.4.6.2.47  2015/08/21 10:57:50  nneelam
 *� case# 201414102
 *�
 *� Revision 1.2.2.17.4.6.2.46  2015/08/05 15:14:38  grao
 *� 2015.2   issue fixes  201413681
 *�
 *� Revision 1.2.2.17.4.6.2.45  2015/07/16 15:20:27  grao
 *� 2015.2   issue fixes  201412888
 *�
 *� Revision 1.2.2.17.4.6.2.44  2015/05/08 15:48:59  nneelam
 *� case# 201412691
 *�
 *� Revision 1.2.2.17.4.6.2.43  2015/01/22 13:35:42  schepuri
 *� issue # 201411405
 *�
 *� Revision 1.2.2.17.4.6.2.42  2015/01/07 13:56:51  schepuri
 *� issue#   201411155
 *�
 *� Revision 1.2.2.17.4.6.2.41  2015/01/06 06:35:40  schepuri
 *� issue#   201411243
 *�
 *� Revision 1.2.2.17.4.6.2.40  2014/11/07 12:25:43  vmandala
 *� case#  201410959 Stdbundle issue fixed
 *�
 *� Revision 1.2.2.17.4.6.2.39  2014/11/07 12:20:04  vmandala
 *� case#  201410986 Stdbundle issue fixed
 *�
 *� Revision 1.2.2.17.4.6.2.38  2014/11/05 10:28:59  sponnaganti
 *� Case# 201410745
 *� True Fab SB Issue fixed
 *�
 *� Revision 1.2.2.17.4.6.2.37  2014/10/17 14:16:01  schepuri
 *� 201410606 issue fix
 *�
 *� Revision 1.2.2.17.4.6.2.36  2014/08/28 11:24:19  sponnaganti
 *� Case# 201410152
 *� Stnd Bundle Issue fix
 *�
 *� Revision 1.2.2.17.4.6.2.35  2014/08/05 13:22:09  grao
 *� Case#: 20149630  Standard bundel  issue fixes
 *�
 *� Revision 1.2.2.17.4.6.2.34  2014/07/29 14:58:18  grao
 *� Case#: 20149630  New 2014.2 Compatibilty issue fixes
 *�
 *� Revision 1.2.2.17.4.6.2.33  2014/07/11 15:01:53  rmukkera
 *� Case # 20149382�
 *�
 *� Revision 1.2.2.17.4.6.2.32  2014/06/23 14:41:05  snimmakayala
 *� 20148941
 *�
 *� Revision 1.2.2.17.4.6.2.31  2014/06/17 15:05:57  rmukkera
 *� Case # 20148455�,20148471,20148705�,20148706,20148707
 *�
 *� Revision 1.2.2.17.4.6.2.30  2014/06/17 15:05:30  rmukkera
 *� Case # 20148455�,20148471,20148705�,20148706,20148707
 *�
 *� Revision 1.2.2.17.4.6.2.29  2014/06/16 14:57:56  rmukkera
 *� Case # 20148708
 *�
 *� Revision 1.2.2.17.4.6.2.28  2014/06/13 14:31:55  rmukkera
 *� Case # 20148455�
 *�
 *� Revision 1.2.2.17.4.6.2.27  2014/06/13 08:09:09  skavuri
 *� Case# 20148882 (added Focus Functionality for Textbox)
 *�
 *� Revision 1.2.2.17.4.6.2.26  2014/06/04 14:43:18  rmukkera
 *� Case # 20148706,20148707
 *�
 *� Revision 1.2.2.17.4.6.2.25  2014/05/30 12:17:43  rmukkera
 *� Case # 20148160�
 *�
 *� Revision 1.2.2.17.4.6.2.24  2014/05/30 00:26:47  nneelam
 *� case#  20148622
 *� Stanadard Bundle Issue Fix.
 *�
 *� Revision 1.2.2.17.4.6.2.23  2014/05/26 15:04:09  skreddy
 *� case # 20148476
 *� Sonic SB issue fix
 *�
 *� Revision 1.2.2.17.4.6.2.22  2014/05/23 15:22:55  sponnaganti
 *� case# 20148449 20148490
 *� Stnd Bundle Issue fix
 *�
 *� Revision 1.2.2.17.4.6.2.21  2014/05/21 15:33:39  skavuri
 *� Case # 20148449 SB Issue Fixed
 *�
 *� Revision 1.2.2.17.4.6.2.20  2014/05/02 06:42:41  sponnaganti
 *� case# 20148250
 *� (transaction type issue)
 *�
 *� Revision 1.2.2.17.4.6.2.19  2014/04/23 15:54:45  skavuri
 *� Case# 20148128 issue fixed
 *�
 *� Revision 1.2.2.17.4.6.2.18  2014/03/24 15:36:31  skavuri
 *� Case # 20127822 issue fixed
 *�
 *� Revision 1.2.2.17.4.6.2.17  2014/02/21 15:10:30  rmukkera
 *� Case# 20125411
 *� qty-upcdode
 *�
 *� Revision 1.2.2.17.4.6.2.16  2014/01/31 15:54:46  nneelam
 *� case#  20127022
 *� std bundle issue fix
 *�
 *� Revision 1.2.2.17.4.6.2.15  2013/12/13 15:32:32  nneelam
 *� Case# 20126178
 *� std issue fix
 *�
 *� Revision 1.2.2.17.4.6.2.14  2013/12/03 15:32:11  skreddy
 *� Case# 20126063
 *� 2014.1 stnd bundle issue fix
 *�
 *� Revision 1.2.2.17.4.6.2.13  2013/12/02 08:59:59  schepuri
 *� 20126048
 *�
 *� Revision 1.2.2.17.4.6.2.12  2013/11/28 14:54:26  rmukkera
 *� Case# 20126037�,20126038�
 *�
 *� Revision 1.2.2.17.4.6.2.11  2013/11/27 15:53:37  grao
 *� Case# 20125951 related issue fixes in SB 2014.1
 *�
 *� Revision 1.2.2.17.4.6.2.10  2013/09/26 15:55:48  grao
 *� Issue fixes for 20124616
 *�
 *� Revision 1.2.2.17.4.6.2.9  2013/08/29 15:20:43  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Check in with POoverage
 *�
 *� Revision 1.2.2.17.4.6.2.8  2013/08/23 07:15:34  schepuri
 *� case no 20123558
 *�
 *� Revision 1.2.2.17.4.6.2.7  2013/08/02 15:40:50  rmukkera
 *� Case# 20123695
 *� Issue fix for
 *� RF Cart Putaway :: When we enter the Cart LP screen is displaying invalid Cart LP message.
 *�
 *� Revision 1.2.2.17.4.6.2.6  2013/07/15 11:38:14  snimmakayala
 *� Case# 20123429
 *� GFT UAT ISSUE
 *�
 *� Revision 1.2.2.17.4.6.2.5  2013/06/11 14:30:41  schepuri
 *� Error Code Change ERROR to DEBUG
 *�
 *� Revision 1.2.2.17.4.6.2.4  2013/06/05 21:58:51  spendyala
 *� CASE201112/CR201113/LOG2012392
 *� Issue related missing actual time and date parameter.
 *�
 *� Revision 1.2.2.17.4.6.2.3  2013/05/15 07:36:54  snimmakayala
 *� CASE201112/CR201113/LOG2012392
 *� Prod and UAT issue fixes.
 *�
 *� Revision 1.2.2.17.4.6.2.2  2013/04/17 16:04:01  skreddy
 *� CASE201112/CR201113/LOG201121
 *� added meta tag
 *�
 *� Revision 1.2.2.17.4.6.2.1  2013/03/01 14:34:59  skreddy
 *� CASE201112/CR201113/LOG201121
 *� Merged from FactoryMation and change the Company name
 *�
 *� Revision 1.2.2.17.4.6  2013/02/07 15:10:34  schepuri
 *� CASE201112/CR201113/LOG201121
 *� disabling ENTER Button func added
 *�
 *� Revision 1.2.2.17.4.5  2012/12/11 14:52:14  schepuri
 *� CASE201112/CR201113/LOG201121
 *� upc code issue
 *�
 *� Revision 1.2.2.17.4.4  2012/11/01 14:55:35  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Decimal Qty Conversions
 *�
 *� Revision 1.2.2.17.4.3  2012/09/27 10:53:53  grao
 *� CASE201112/CR201113/LOG201121
 *�
 *� Converting multiple language with given Spanish terms
 *�
 *� Revision 1.2.2.17.4.2  2012/09/26 12:44:14  grao
 *� CASE201112/CR201113/LOG201121
 *� Converting Multi language without small characters
 *�
 *� Revision 1.2.2.17.4.1  2012/09/21 14:57:16  grao
 *� CASE201112/CR201113/LOG201121
 *� Converting Multilanguage
 *�
 *� Revision 1.2.2.17  2012/08/24 18:13:58  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *� NSUOMfield code changes
 *�
 *� Revision 1.2.2.16  2012/07/30 23:17:32  gkalla
 *� CASE201112/CR201113/LOG201121
 *� NSUOM issue
 *�
 *� Revision 1.2.2.15  2012/05/17 13:36:52  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Item recommended qty issue is resolved.
 *�
 *� Revision 1.2.2.14  2012/05/16 13:35:34  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Multi UOM changes
 *�
 *� Revision 1.2.2.13  2012/05/02 12:36:11  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *�
 *� RF Checkin changes
 *�
 *� Revision 1.2.2.12  2012/04/25 15:37:13  spendyala
 *� CASE201112/CR201113/LOG201121
 *� While Showing Recommended qty POoverage is not considered.
 *�
 *� Revision 1.2.2.11  2012/04/19 12:09:46  schepuri
 *� CASE201112/CR201113/LOG201121
 *� validation on checkin once after checkin already completed
 *�
 *� Revision 1.2.2.10  2012/04/17 10:38:39  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *�
 *� RF Cart To Checkin
 *�
 *� Revision 1.2.2.9  2012/04/16 14:59:35  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Calculating POoverage is moved to general function.
 *�
 *� Revision 1.2.2.8  2012/04/13 22:18:00  spendyala
 *� CASE201112/CR201113/LOG201121
 *� Depending upon POoverage Value mentioned in the item master
 *� value of poOverage will be calculated accordingly.
 *�
 *� Revision 1.2.2.7  2012/04/11 12:51:51  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *�
 *� display pickface locand qty
 *�
 *� Revision 1.2.2.6  2012/03/29 06:32:54  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Cart Checkin issues for TPP
 *�
 *� Revision 1.6  2012/03/29 06:13:39  snimmakayala
 *� CASE201112/CR201113/LOG201121
 *� Cart Checkin issues for TPP
 *�
 *� Revision 1.5  2012/03/21 07:19:39  spendyala
 *� CASE201112/CR201113/LOG201121
 *� meged code from branch.
 *�
 *� Revision 1.4  2012/03/13 15:32:45  spendyala
 *� CASE201112/CR201113/LOG201121
 *� code merged upto 1.2.2.4 from branch.
 *�
 *� Revision 1.3  2012/02/24 00:14:12  rrpulicherla
 *� CASE201112/CR201113/LOG201121
 *�
 *� buildshipunits
 *�
 *� Revision 1.2  2012/02/16 10:31:18  schepuri
 *� CASE201112/CR201113/LOG201121
 *� Added FunctionkeyScript
 *�
 *� Revision 1.1  2012/02/02 08:56:59  rmukkera
 *� CASE201112/CR201113/LOG201121
 *�   cartlp new file
 *�
 *
 ****************************************************************************/


function CheckInQty(request, response){
	if (request.getMethod() == 'GET') {
		var ItemDescription;
		var ItemQuantity;
		var ItemQuantityReceived;
		var ItemPackCode='1';
		var ItemStatus;
		var ItemStatusValue;
		var ItemCube;
		var UPC="";
		var ManufacturerNo="";
		var vPOoverageQty=0;
		var ItemPackCodeText='1';
		var getOptedField = request.getParameter('custparam_option');
		nlapiLogExecution('DEBUG', 'getOptedField', getOptedField);

		//	Get the PO#, PO Line Item, Line#, Entered Item and PO Internal Id 
		//  from the previous screen, which is passed as a parameter	
		var getPONo = request.getParameter('custparam_poid');
		var getPOItem = request.getParameter('custparam_poitem');
		nlapiLogExecution('DEBUG','getPOItem', getPOItem);
		var getPOLineNo = request.getParameter('custparam_lineno');
		var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		var getPOInternalId = request.getParameter('custparam_pointernalid');
		var getItemCube = request.getParameter('custparam_itemcube');
		var getWHLocation = request.getParameter('custparam_whlocation');
		var trantype= request.getParameter('custparam_trantype');
		nlapiLogExecution('ERROR','WH Location', getWHLocation);
		nlapiLogExecution('ERROR','trantype', trantype);

		if(trantype==null || trantype=='')
		{
			trantype = nlapiLookupField('transaction', getPOInternalId, 'recordType');
		}

		var poItemUOM='';
		var vbaseuom='';
		var vbaseuomqty='';
		var vuomqty='';
		var vuomlevel='';
		var enteredOption = "";
		var item_id='';
		enteredOption = request.getParameter('custparam_enteredOption');

		// Load a record into a variable from Purchase Order for the PO Internal Id
		var PORec = nlapiLoadRecord(trantype, getPOInternalId);

		// Fetched the count of lines available in the Purchase Order
		var LineItemCount = PORec.getLineItemCount('item');

		//Fetch the UPC code from item master.
		//Code added on 13Mar by suman.
		var poItemField = ['upccode','mpn'];
		nlapiLogExecution('DEBUG','getFetchedItemId', getFetchedItemId);
		var poItemColumn = nlapiLookupField('item', getFetchedItemId, poItemField);

		UPC=poItemColumn.upccode;
		ManufacturerNo=poItemColumn.mpn;

		// Loop into fetch the PO Line details for the line# passed.
		for (var i = 1; i <= LineItemCount; i++) {
			var lineno = PORec.getLineItemValue('item', 'line', i);

			if (lineno == getPOLineNo) {
				ItemDescription = PORec.getLineItemValue('item', 'description', i);
				poItemUOM = PORec.getLineItemValue('item', 'units', i);
				item_id = PORec.getLineItemText('item', 'item', i);
				nlapiLogExecution('DEBUG','getPOItem item_id', item_id);

				if (ItemDescription == null) {
					var poItemFields = ['custitem_ebizdescriptionitems'];
					var poItemColumns = nlapiLookupField('item', getFetchedItemId, poItemFields);

					ItemDescription = poItemColumns.custitem_ebizdescriptionitems;
					if (ItemDescription == null)
						ItemDescription = '';
				}
				ItemDescription = ItemDescription.substring(0, 20);

				ItemQuantity = PORec.getLineItemValue('item', 'quantity', i);
				if (ItemQuantity == null) {
					ItemQuantity = '';
				}

				if(trantype=='transferorder')
				{

					var ItemQuantityfulfilled = PORec.getLineItemValue('item', 'quantityfulfilled', i);
					nlapiLogExecution('DEBUG','ItemQuantityfulfilled', ItemQuantityfulfilled);
					if (ItemQuantityfulfilled == null || ItemQuantityfulfilled == '') {
						ItemQuantityfulfilled = 0;

					}

					if(parseFloat(ItemQuantity) > parseFloat(ItemQuantityfulfilled))
						ItemQuantity = ItemQuantityfulfilled;

				}

				ItemQuantityReceived = PORec.getLineItemValue('item', 'quantityreceived', i);
				if (ItemQuantityReceived == null) {
					ItemQuantityReceived = '';
				}

				if(PORec.getLineItemText('item', 'custcol_nswmspackcode', i) != null && PORec.getLineItemText('item', 'custcol_nswmspackcode', i) != '')
				{
					//	ItemPackCode = PORec.getLineItemText('item', 'custcol_nswmspackcode', i);
					ItemPackCodeText = PORec.getLineItemText('item', 'custcol_nswmspackcode', i);
					ItemPackCode = PORec.getLineItemValue('item', 'custcol_nswmspackcode', i);
				}


				ItemStatus = PORec.getLineItemText('item', 'custcol_ebiznet_item_status', i);

				ItemStatusValue = PORec.getLineItemValue('item', 'custcol_ebiznet_item_status', i);
				if (ItemStatus == null) {
					ItemStatus = '';
				}
			}
		}

		if(poItemUOM!=null && poItemUOM!='')
		{
			var eBizItemDims=geteBizItemDimensions(getFetchedItemId);
			if(eBizItemDims!=null&&eBizItemDims.length>0)
			{
				nlapiLogExecution('DEBUG', 'Item Dimesions Length', eBizItemDims.length);
				for(z=0; z < eBizItemDims.length; z++)
				{

					if(eBizItemDims[z].getValue('custrecord_ebizbaseuom') == 'T')
					{
						vbaseuom = eBizItemDims[z].getText('custrecord_ebizuomskudim');
						vbaseuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');					
					}
					nlapiLogExecution('DEBUG', 'poItemUOM', poItemUOM);
					nlapiLogExecution('DEBUG', 'DIM UOM', eBizItemDims[z].getValue('custrecord_ebiznsuom'));
					nlapiLogExecution('DEBUG', 'DIM UOM', eBizItemDims[z].getText('custrecord_ebiznsuom'));
					if(poItemUOM.trim() == eBizItemDims[z].getValue('custrecord_ebiznsuom').trim())
					{
						vuomlevel = eBizItemDims[z].getText('custrecord_ebizuomlevelskudim');
						vuomqty = eBizItemDims[z].getValue('custrecord_ebizqty');
					}
				}
				if(vuomqty==null || vuomqty=='')
				{
					vuomqty=vbaseuomqty;
				}

				if(ItemQuantity==null || ItemQuantity=='' || isNaN(ItemQuantity))
					ItemQuantity=0;
				else
					ItemQuantity = (parseFloat(ItemQuantity)*parseFloat(vuomqty))/parseFloat(vbaseuomqty);

				if(ItemQuantityReceived==null || ItemQuantityReceived=='' || isNaN(ItemQuantityReceived))
					ItemQuantityReceived=0;
				else
					ItemQuantityReceived = (parseFloat(ItemQuantityReceived)*parseFloat(vuomqty))/parseFloat(vbaseuomqty);			

			}
		}

		nlapiLogExecution('DEBUG', 'poItemUOM', poItemUOM);
		nlapiLogExecution('DEBUG', 'baseuom', vbaseuom);
		nlapiLogExecution('DEBUG', 'baseuomqty', vbaseuomqty);
		nlapiLogExecution('DEBUG', 'uomqty', vuomqty);
		nlapiLogExecution('DEBUG', 'uomlevel', vuomlevel);
		nlapiLogExecution('DEBUG', 'ItemQuantity', ItemQuantity);
		nlapiLogExecution('DEBUG', 'ItemQuantityReceived', ItemQuantityReceived);

		nlapiLogExecution('DEBUG','ItemPackCode', ItemPackCode);
		/*
		 * The below part of the code is to check if the PO Overage is allowed or not. 
		 * If the PO Overages are allowed, then the recommended quantity should be the pallet quantity.
		 * If the PO Overages are not allowed, then the recommended quantity should be the remaining quantity.
		 */        

		var poOverage=GetPoOverage(getFetchedItemId,getPOInternalId,getWHLocation);
		//var poOverage = checkPOOverage(getPOInternalId,getWHLocation, null);
		nlapiLogExecution('DEBUG','poOverage', poOverage);

		//if(poOverage==null || poOverage=='')
		if((poOverage==null || poOverage=='') && poOverage!=0)
		{
			poOverage = checkPOOverage(getPOInternalId,getWHLocation, null);
		}
//		var ItemRemaininingQuantity = parseFloat(ItemQuantity) - parseFloat(ItemQuantityReceived);

		var ItemRemaininingQuantity = itemRemainingQuantity(getPOInternalId, getFetchedItemId, getPOLineNo,
				ItemQuantity, ItemQuantityReceived, getWHLocation, null);

		
		
		
		var vOrderQuantity = 0;
		var vputgenQuantity = 0;
		
		var vtransactionFilters = new Array();
		vtransactionFilters[0] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', getPOInternalId);
		vtransactionFilters[1] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_sku_no', null, 'equalto', getFetchedItemId);
		vtransactionFilters[2] = new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', getPOLineNo);

		var vtransactionColumns = new Array();
		vtransactionColumns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_putgen_qty');
		vtransactionColumns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_order_qty');
		

		var vtransactionSearchresults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, vtransactionFilters, vtransactionColumns);
		
		if (vtransactionSearchresults != null && vtransactionSearchresults.length > 0)
		{
			for(var i = 0; i <= vtransactionSearchresults.length; i++)
			{
				vputgenQuantity = vtransactionSearchresults[0].getValue('custrecord_orderlinedetails_putgen_qty');
				vOrderQuantity =  vtransactionSearchresults[0].getValue('custrecord_orderlinedetails_order_qty');
			}
		}
		
		var palletQuantity = fetchPalletQuantity(getFetchedItemId,getWHLocation,null); //'9999';
		var ItemRecommendedQuantity =0;
		var hdnItemRecmdQtyWithPOoverage=0;
		/*nlapiLogExecution('DEBUG','ItemRemaininingQuantity', ItemRemaininingQuantity);
		nlapiLogExecution('DEBUG','palletQuantity', palletQuantity);
		nlapiLogExecution('DEBUG','ItemQuantity', ItemQuantity);
		 */
		if (poOverage == null || poOverage == ""){
			poOverage = 0;	
		}
		//case # 20126178  Start
		if(trantype!='purchaseorder'){
			poOverage = 0;
		}
		//End
		//else{
//		ItemRecommendedQuantity = Math.min (ItemRemaininingQuantity + (poOverage * ItemQuantity)/100, palletQuantity);
		hdnItemRecmdQtyWithPOoverage = Math.min (ItemRemaininingQuantity + (poOverage * ItemQuantity)/100, palletQuantity);
		ItemRecommendedQuantity = Math.min (hdnItemRecmdQtyWithPOoverage, palletQuantity);
		/*
			if(palletQuantity > ItemRemaininingQuantity){
				ItemRecommendedQuantity = ItemRemaininingQuantity;
			}
			else if (palletQuantity < ItemRemaininingQuantity){
				ItemRecommendedQuantity =  palletQuantity;
			}*/		

		//}
		nlapiLogExecution('DEBUG','ItemRecommendedQuantity', parseFloat(ItemRecommendedQuantity.toString()));
		var pickfaceRecommendedQuantity = priorityPutawayQuantity(getFetchedItemId,getWHLocation,null);
		nlapiLogExecution('DEBUG','Returned Pickface Recommended Quantity', pickfaceRecommendedQuantity);

		//get the pickfaceloc for the item.
		//Added on 13Mar By suman.
		var PickfaceBinLoc=GetPickFaceLocation(getFetchedItemId,getWHLocation);

		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime'); 
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');

		if(ItemRemaininingQuantity < 0)
			ItemRemaininingQuantity=0;


		nlapiLogExecution('DEBUG','ItemRemaininingQuantity', ItemRemaininingQuantity);
		nlapiLogExecution('DEBUG','palletQuantity', palletQuantity);
		nlapiLogExecution('DEBUG','ItemQuantity', ItemQuantity);

		var POarrayget = new Array();
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);

		var st17;
		if( getLanguage == 'es_ES' || getLanguage =='es_AR')
		{
			st17 = "Llegada ya terminado";

		}
		else
		{
			st17="Check in already completed";
		}
		POarrayget["custparam_language"] = getLanguage;
		POarrayget["custparam_error"] = st17;
		POarrayget["custparam_screenno"] = 'CRT4A';
		POarrayget["custparam_poid"] = request.getParameter('custparam_poid');
		//POarrayget["custparam_poitem"] = request.getParameter('custparam_poitem');
		POarrayget["custparam_poitem"] = item_id;
		POarrayget["custparam_lineno"] = request.getParameter('custparam_lineno');
		POarrayget["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
		POarrayget["custparam_pointernalid"] = request.getParameter('custparam_pointernalid');
		POarrayget["custparam_trantype"] = request.getParameter('custparam_trantype');
		POarrayget["custparam_itemcube"] = request.getParameter('custparam_itemcube');
		POarrayget["custparam_whlocation"] = request.getParameter('custparam_whlocation');
		POarrayget["custparam_cartlpno"] = request.getParameter('custparam_cartlpno');
		POarrayget["custparam_actualbegindate"] = request.getParameter('custparam_actualbegindate');
		POarrayget["custparam_actualbegintime"] = request.getParameter('custparam_actualbegintime');
		POarrayget["custparam_actualbegintimeampm"] = request.getParameter('custparam_actualbegintimeampm');

//		Added by Narasimha inorder to get the receipt type based on po.
		//case 201410986

		var ItemDimens=CheckItemDimens(getFetchedItemId, getWHLocation,ItemPackCode);

		if(ItemDimens == '' ||ItemDimens == null)
		{
			POarrayget["custparam_error"] ="Item Dimensions are not Configured for Item# "+ item_id;
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarrayget);
			return;

		}
		//case 201410986 end



		var POblindreceiptfilters=new Array();
		POblindreceiptfilters.push(new nlobjSearchFilter('tranid',null,'is',getPONo));
		POblindreceiptfilters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));


		var blindreceiptColumns = new Array();
		blindreceiptColumns[0] = new nlobjSearchColumn('custbody_nswmsporeceipttype');


		var blindreceiptSearchResults = nlapiSearchRecord('purchaseorder', null, POblindreceiptfilters, blindreceiptColumns);

		var receiptType='';
		if(blindreceiptSearchResults!=null && blindreceiptSearchResults!='')
		{
			receiptType=blindreceiptSearchResults[0].getValue('custbody_nswmsporeceipttype');
		}

		var poBlindReceipt='';


		nlapiLogExecution('DEBUG','receiptType',receiptType);

		if (receiptType != "" && receiptType != null) 
		{

			var receiptFieldsRec = ['custrecord_ebiz_blindreceipt'];
			var receiptColumns = nlapiLookupField('customrecord_ebiznet_receipt_type', receiptType, receiptFieldsRec);

			poBlindReceipt = receiptColumns.custrecord_ebiz_blindreceipt;

			nlapiLogExecution('DEBUG','poBlindReceipt', poBlindReceipt);
		}

		// Upto here
		if(poOverage > 0)
		{

			if(ItemQuantity!=0 && vputgenQuantity!=0)
			{
				if(parseFloat(ItemQuantity) == parseFloat(vputgenQuantity))
				{
					POarrayget["custparam_error"] = 'Ordered quantity received completely';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarrayget);
					return;
				}
			}
		}

		if(parseFloat(ItemRemaininingQuantity) == 0 && (parseFloat(ItemRecommendedQuantity) == 0||parseFloat(ItemRecommendedQuantity) < 0))
		{
			nlapiLogExecution('DEBUG','cknin completed ItemRemaininingQuantity', ItemRemaininingQuantity);
			nlapiLogExecution('DEBUG','cknin completed ItemRecommendedQuantity', ItemRecommendedQuantity);
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarrayget);
			return;
		}

		else
		{

			if (request.getMethod() == 'GET') 
			{	

				var getLanguage = request.getParameter('custparam_language');
				nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);

				var st0,st1,st2,st3,st4,st5,st6,st7,st8,st9,st10,st11,st12;
				if( getLanguage == 'es_ES' || getLanguage =='es_AR')
				{
					st0 = "";
					st1 = "ART&#205;CULO";
					st2 = "DESCRIPCI&#211;N DEL ART&#205;CULO";
					st3 = "UPC";
					st4 = "NO. DE MANUFACTURA";
					st5 = "ESTADO DEL ELEMENTO";
					st6 = "CANTIDAD RESTANTE";
					st7 = "CANTIDAD RECOMENDADA";
					st8 = "RECOGIDA Cantidad Face RECOMENDADO";
					st9 = "UBICACI&#211;N PRINCIPAL";
					st10 = "CANTIDAD";
					st11 = "ENVIAR";
					st12 = "ANTERIOR";		
					st13 = " CUDIGO DE EMPAQUE";
				}
				else
				{
					st0 = "";
					st1 = "ITEM";
					st2 = "ITEM DESC";
					st3 = "UPC";
					st4 = "MANF#";
					st5 = "ITEM STATUS";
					st6 = "REMAINING QTY";
					st7 = "RECOMMENDED QTY";
					st8 = "PICKFACE RECOMMENDED QTY";
					st9 = "PRIMARYLOC";
					st10 = "QTY";
					st11 = "SEND";
					st12 = "PREV";
					// 20126048
					st13= "PACK CODE: ";

				}

				
				
				
				var poItemField = ['recordType', 'custitem_ebizbatchlot','custitem_item_family','custitem_item_group',
						              'custitem_ebizserialin','custitem_ebiz_merge_fifodates','upccode','mpn'];
						var poItemColumn = nlapiLookupField('item', getFetchedItemId, poItemField);
						
						var ItemType = poItemColumn.recordType;					
						var batchflg = poItemColumn.custitem_ebizbatchlot;
						var itemfamId= poItemColumn.custitem_item_family;
						var itemgrpId= poItemColumn.custitem_item_group;
						var serialInflg = poItemColumn.custitem_ebizserialin;
						var mergefifodates=poItemColumn.custitem_ebiz_merge_fifodates;
						var UPC=poItemColumn.upccode;
						var ManufacturerNo=poItemColumn.mpn;
				
				var getNumber;
				if(request.getParameter('custparam_number')!= null && request.getParameter('custparam_number') != "")
					getNumber = request.getParameter('custparam_number');
				else if(request.getParameter('custparam_poqtyentered')!= null && request.getParameter('custparam_poqtyentered') != "")
				{
					getNumber = request.getParameter('custparam_poqtyentered');
				}
				else
				{
					getNumber=0;
				}
				var caseQty = fetchCaseQuantity(getFetchedItemId,getWHLocation,null);
				var functionkeyHtml=getFunctionkeyScript('_rf_checkin_qty'); 
				var html = "<html><head><title>" + st0 + "</title>";
				html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
				html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";  
				//Case# 20148749 Refresh Functionality starts
				html = html + "var version = navigator.appVersion;";
				html = html + "document.onkeydown = function (e) {";
				html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
				html = html + "if ((version.indexOf('MSIE') != -1)) { ";
				html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
				html = html + "else {if (keycode == 116)return false;}";
				html = html + "};";
				//Case# 20148749 Refresh Functionality ends
				html = html + "nextPage = new String(history.forward());";          
				html = html + "if (nextPage == 'undefined')";     
				html = html + "{}";     
				html = html + "else";     
				html = html + "{  location.href = window.history.forward();"; 
				html = html + "} ";
				//html = html + " document.getElementById('enterqty').focus();";        
				html = html + "function stopRKey(evt) { ";
				//html = html + "	  alert('evt');";
				html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
				html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
				html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
				html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
				html = html + "	  alert('System Processing, Please wait...');";
				html = html + "	  return false;}} ";
				html = html + "	} ";

				html = html + "	document.onkeypress = stopRKey; ";
				html = html + "</script>";
				html = html +functionkeyHtml;
				html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
				html = html + "	<form name='_rf_checkin_qty' method='POST'>";
				html = html + "		<table>";
				html = html + "			<tr>";
				html = html + "				<td align = 'left'>" + st1 + " : <label>" + item_id + "</label>";
				html = html + "				</td>";
				html = html + "			</tr>";
				html = html + "			<tr>";
				html = html + "				<td align = 'left'>" + st2 + ": <label>" + ItemDescription + "</label>";
				html = html + "				</td>";
				html = html + "			</tr><tr>";
				html = html + "				<td align = 'left'>" + st3 +  ": <label>" + UPC + "</label>";
				html = html + "				</td>";
				html = html + "			</tr><tr>";
				html = html + "				<td align = 'left'> " + st4 + ": <label>" + ManufacturerNo + "</label>";
				html = html + "				</td>";
				html = html + "			</tr>";
				html = html + "			<tr>";
				html = html + "				<td align = 'left'>" + st13 + ":  <label>" + ItemPackCodeText + "</label>";
				html = html + "				<input type='hidden' name='hdnItemPackCode' value=" + ItemPackCode + ">";
				html = html + "				<input type='hidden' name='hdnQuantity' value=" + ItemQuantity + ">";
				html = html + "				<input type='hidden' name='hdnQuantityReceived' value=" + ItemQuantityReceived + ">";
				nlapiLogExecution('DEBUG', 'ItemStatus113311', ItemStatus);
				html = html + "				<input type='hidden' name='hdnItemStatusValue' value=" + ItemStatusValue + "></td>";
				nlapiLogExecution('DEBUG', 'hdnItemStatus111', ItemStatusValue);
				html = html + "				<input type='hidden' name='hdnItemCube' value=" + getItemCube + "></td>";
				html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
				html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
				html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
				html = html + "				<input type='hidden' name='hdnOptedField' value=" + getOptedField + ">";
				html = html + "				<input type='hidden' name='hdnEnteredOption' value=" + enteredOption + ">";
				html = html + "				<input type='hidden' name='hdnPalletQuantity' value=" + palletQuantity + "></td>";
				//html = html + "				<input type='hidden' name='hdngetPOItem' value=" + getPOItem + "></td>";
				html = html + "				<input type='hidden' name='hdngetPOItem' value='" + item_id + "'></td>";// Case # 20127822
				//Added by Phani 03-25-2011
				html = html + "				<input type='hidden' name='hdnWhLocation' value=" + getWHLocation + ">";
				html = html + "				<input type='hidden' name='hdnpoOverage' value=" + poOverage + ">";
				html = html + "				<input type='hidden' name='hdntrantype' value=" + trantype + ">";
				html = html + "				<input type='hidden' name='hdnItemRecmdQtyWithPOoverage' value=" + hdnItemRecmdQtyWithPOoverage + ">";
				html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
				
				html = html + "				<input type='hidden' name='hdnitemtype' value=" + ItemType + ">";
				html = html + "				<input type='hidden' name='hdnserialin' value=" + serialInflg + ">";
				
				html = html + "			</tr>";
				html = html + "			<tr>";
				html = html + "				<td align = 'left'>" + st5 + ": <label>" + ItemStatus + "</label></td>";
				nlapiLogExecution('DEBUG', 'ItemStatus1', ItemStatus);
				html = html + "			</tr>";
				if(poBlindReceipt!='T'){
					html = html + "			<tr>";
					if(ItemRemaininingQuantity < 0)
						ItemRemaininingQuantity=0;
					//html = html + "				<td align = 'left'>" + st6 + ": <label>" + parseFloat(ItemRemaininingQuantity.toString()) + "</label>";
					html = html + "				<td align = 'left'>" + st6 + ": <label>" + parseFloat(ItemRemaininingQuantity).toFixed(4) + "  Each</label>";
					var pikQtyBreakup = getQuantityBreakdown(caseQty,ItemRemaininingQuantity);
					html = html + "	("+caseQty+"/Case:"+pikQtyBreakup+")";
					html = html + "				<input type='hidden' name='hdnItemRemaininingQuantity' value=" + parseFloat(ItemRemaininingQuantity).toFixed(4) + "></td>";
					html = html + "			</tr>";
					html = html + "			<tr>";
					//html = html + "				<td align = 'left'>" + st7 +  ": <label>" + parseFloat(ItemRecommendedQuantity.toString()) + " Each</label>";
					html = html + "				<td align = 'left'>" + st7 +  ": <label>" + parseFloat(ItemRecommendedQuantity).toFixed(4) + "</label>";
					var pikQtyBreakup = getQuantityBreakdown(caseQty,ItemRecommendedQuantity);
					html = html + "	("+caseQty+"/Case:"+pikQtyBreakup+")";
					html = html + "				<input type='hidden' name='hdnRecommendedQuantity' value=" + parseFloat(ItemRecommendedQuantity).toFixed(4) + "></td>";
					html = html + "			</tr>";
				}
				if(pickfaceRecommendedQuantity > 0)
				{
					html = html + "			<tr>";
					html = html + "				<td align = 'left'>" + st8 + ": <label>" + pickfaceRecommendedQuantity + "</label>";
					html = html + "				<input type='hidden' name='hdnPickfaceRecommendedQuantity' value=" + parseFloat(pickfaceRecommendedQuantity) + "></td>";
					html = html + "			</tr>";		

					html = html + "			<tr>";
					html = html + "				<td align = 'left'>" + st9 + ": <label>" + PickfaceBinLoc + "</label>";
					html = html + "				</td>";
					html = html + "			</tr>";
				}

				if(getNumber > 0)
				{
					html = html + "			<tr>";
					html = html + "				<td align = 'left'> SCANNED QTY: <label>" + getNumber + "</label>";
					html = html + "				<input type='hidden' name='hdngetnumber' value=" + getNumber + ">";
					html = html + "			</tr>";		
				}

				html = html + "			<tr>";
				html = html + "				<td align = 'left'>" + st10 + ": ";
				html = html + "				</td>";
				html = html + "			</tr>";
				html = html + "			<tr>";
				html = html + "				<td align = 'left'><input name='enterqty' id='enterqty' type='text' />";
				html = html + "				</td>";
				html = html + "			</tr>";
				html = html + "			<tr>";
				html = html + "				<td align = 'left'>" + st11 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
				html= html +"            NEXT <input name='cmdNext' type='submit' value='F8' />";
				html = html + "					" + st12 + " <input name='cmdPrevious' type='submit' value='F7'/>";
				html = html + "				</td>";
				html = html + "			</tr>";
				html = html + "		 </table>";
				html = html + "	</form>";
				//Case# 20148882 (added Focus Functionality for Textbox)
				html = html + "<script type='text/javascript'>document.getElementById('enterqty').focus();</script>";
				html = html + "</body>";
				html = html + "</html>";

				response.write(html);
			}
		}
	}
	else {
		nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is not null');

		var getActualBeginTime = request.getParameter('hdnActualBeginTime'); 
		var getActualBeginTimeAMPM = request.getParameter('hdnActualBeginTimeAMPM');

		var ActualBeginTime;

		// This variable is to hold the Quantity entered.
		var POarray = new Array();

		var getLanguage = request.getParameter('hdngetLanguage');
		POarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);


		var st13,st14,st15,st16;
		if( getLanguage == 'es_ES')
		{
			st13 = "CANTIDAD INV&#193;LIDA";
			st14 = "OBERTURA NO PERMITIDO";
			st15 = "CANTIDAD EXCEDE EL L&#205;MITE OBERTURA";
			st16 = "CANTIDAD DE PALETA no est&#225; definido para el art&#237;culo";
		}
		else
		{
			st13 = "INVALID QUANTITY";
			st14 = "OVERAGE NOT ALLOWED";
			st15 = "QUANTITY EXCEEDS OVERAGE LIMIT";
			st16 = "PALLET QUANTITY IS NOT DEFINED FOR ITEM :";
		}


		POarray["custparam_cartlpno"] = request.getParameter('custparam_cartlpno');
		POarray["custparam_poid"] = request.getParameter('custparam_poid');
		//POarray["custparam_poitem"] = request.getParameter('custparam_poitem');
		POarray["custparam_poitem"] =  request.getParameter('hdngetPOItem');
		POarray["custparam_lineno"] = request.getParameter('custparam_lineno');
		POarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
		POarray["custparam_pointernalid"] = request.getParameter('custparam_pointernalid');
		POarray["custparam_poqtyentered"] = request.getParameter('enterqty');
		POarray["custparam_poitemremainingqty"] = request.getParameter('hdnItemRemaininingQuantity');
		POarray["custparam_polinepackcode"] = request.getParameter('hdnItemPackCode');
		//nlapiLogExecution('DEBUG', 'request.getParameter(hdnItemStatus)', request.getParameter('hdnItemStatus'));
		POarray["custparam_polineitemstatusValue"] = request.getParameter('hdnItemStatusValue');
		//nlapiLogExecution('DEBUG', 'POarray["custparam_polineitemstatus"]', POarray["custparam_polineitemstatus"]);
		POarray["custparam_polinequantity"] = request.getParameter('hdnQuantity');
		POarray["custparam_polinequantityreceived"] = request.getParameter('hdnQuantityReceived');
		POarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
		POarray["custparam_trantype"] = request.getParameter('hdntrantype');
		POarray["custparam_itemcube"] = request.getParameter('hdnItemCube');
		
		var trantype  = request.getParameter('hdntrantype');

		nlapiLogExecution('DEBUG', 'Itemcubeparam', POarray["custparam_itemcube"]);
		nlapiLogExecution('DEBUG', 'Item quantity entered', POarray["custparam_poqtyentered"]);

		POarray["custparam_actualbegindate"] = request.getParameter('hdnActualBeginDate');
		POarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
		nlapiLogExecution('DEBUG', 'WH Location', POarray["custparam_whlocation"]);

		var poOverage = request.getParameter('hdnpoOverage');
//		var recommendedQuantity = request.getParameter('hdnRecommendedQuantity');
		var recommendedQuantity = request.getParameter('hdnItemRecmdQtyWithPOoverage');
		var palletQuantity = request.getParameter('hdnPalletQuantity');
		var pickfaceRecommendedQuantity = request.getParameter('hdnPickfaceRecommendedQuantity');

		POarray["custparam_actualbegintime"] = getActualBeginTime; //TimeArray[0];
		POarray["custparam_actualbegintimeampm"] = getActualBeginTimeAMPM;	//TimeArray[1];
		POarray["custparam_getPOItem"] = request.getParameter('hdngetPOItem');

		nlapiLogExecution('DEBUG', 'custparam_actualbegintime', POarray["custparam_actualbegintime"]);
		nlapiLogExecution('DEBUG', 'custparam_actualbegintimeampm', POarray["custparam_actualbegintimeampm"]);
		
		
		var getItemType = request.getParameter('hdnitemtype');
		var serialin = request.getParameter('hdnserialin');		

		POarray["custparam_error"] = st13;

		POarray["custparam_screenno"] = 'CRT4';

		var getOptedField = request.getParameter('hdnOptedField');
		POarray["custparam_option"] = getOptedField;
		var getEnteredQty=request.getParameter('enterqty');
		// to the previous screen.
		var getNumber = request.getParameter('hdngetnumber');
		POarray["custparam_number"]=getNumber;
		var optedEvent = request.getParameter('cmdPrevious');
		var optedEventNext = request.getParameter('cmdNext');
		nlapiLogExecution('DEBUG', 'optedEvent',optedEvent);
		nlapiLogExecution('DEBUG', 'optedEventNext',optedEventNext);
		nlapiLogExecution('DEBUG', 'optedField', POarray["custparam_option"]);

		//Case Start 20125950
		/*var venteredqty=request.getParameter('enterqty');

		if (isNaN(POarray["custparam_poqtyentered"]) || venteredqty.trim()=="")
		{
			nlapiLogExecution('DEBUG', 'poqty1', isNaN(POarray["custparam_poqtyentered"]));

			POarray["custparam_error"] = "Please enter Qty";
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);				

		} 
		else
		{*/
		//case 20125950 end.
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		var bulkLocation = request.getParameter('hdnEnteredOption');
		nlapiLogExecution('DEBUG','Entered Option', bulkLocation);
		var Erroflag='F';
		var ExpectedQuantity=request.getParameter('hdnItemRemaininingQuantity');
//		case# 201411243
		var ItemScaninQTY = getSystemRuleValue('Allow Item scan in Quantity field');

		
		var filters = new Array();	
		var poid;
		filters.push(new nlobjSearchFilter('tranid', null, 'is', POarray["custparam_poid"]));	
		//case# 20149365 starts (if line level location is not given)
		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));
		var columns=new Array();
		columns[0] = new nlobjSearchColumn('location');
		var posearch = nlapiSearchRecord(trantype, null, filters, null);
		var vlinelocation='';
		if(posearch!=null && posearch!='')
		{
			poid = posearch[0].getId();
			nlapiLogExecution('DEBUG', 'poid', poid);
			vlinelocation=posearch[0].getValue('location');
			nlapiLogExecution('DEBUG', 'vlinelocation', vlinelocation);
		}
		// case# 201415555
		
	
		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		if (optedEvent == 'F7') {

			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_checkinsku', 'customdeploy_ebiz_rf_cart_checkinsku_di', false, POarray);
			return;
		}
		else if(optedEventNext=='F8')
		{
			/*if (getNumber==null || getNumber=='' || getNumber=='null' || getNumber ==0)			
			{
				POarray["custparam_error"] = "Invalid Qty/UPC";
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);	
				return;
			}
			else
			{*/

			if(getNumber==null || getNumber=='' || getNumber=='null')
			{
				getNumber=0;
			}
			if(getEnteredQty=='' || getEnteredQty==null || getEnteredQty=='null')
			{
				getEnteredQty=0;
			}
			//case 201410959
			if(ItemScaninQTY!='Y')
			{
				
				if(getItemType == 'serializedinventoryitem' || getItemType == 'serializedassemblyitem' || serialin == 'T')
				{
					if(parseInt(getEnteredQty) != getEnteredQty)
					{
						POarray["custparam_error"] = "Decimal qty is not allowed for serial item"; 
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('ERROR', 'Decimal qty is not allowed for serial item', getEnteredQty);
						return;
					}
				}
				
				if (isNaN(POarray["custparam_poqtyentered"]) || isNaN(getEnteredQty))		
				{
					POarray["custparam_error"] = 'Invalid Qty/UPC';
					POarray["custparam_number"]="";
					POarray["custparam_poqtyentered"]="";
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					nlapiLogExecution('DEBUG', 'Invalid Quantity', POarray["custparam_poqtyentered"]);
					return;
				}
			}


			if(trantype == 'returnauthorization')
			{
				if(getItemType == 'serializedinventoryitem' || getItemType == 'serializedassemblyitem' || serialin == 'T')
				{
					if(parseInt(getEnteredQty) != getEnteredQty)
					{
						POarray["custparam_error"] = "Decimal quantity is not allowed for serial item"; 
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('ERROR', 'Decimal quantity is not allowed for serial item', getEnteredQty);
						return;
					}
				}
			}
//			Case # 20148706? Start

			var ItemId=request.getParameter('custparam_fetcheditemid');
			var returnedItemId='';
			//=validateSKU1(getEnteredQty, POarray["custparam_whlocation"], POarray["custparam_company"],POarray["custparam_pointernalid"]);
			nlapiLogExecution('DEBUG', 'ItemId', ItemId);
			nlapiLogExecution('DEBUG', 'returnedItemId', returnedItemId);
			var itemRecord ; 					
			//var itemRecordArr = eBiz_RF_GetItemForItemIdWithArrNew(POarray["custparam_poitem"],POarray["custparam_whlocation"]);
			
			
			var itemRecordArr = eBiz_RF_GetItemForItemIdWithArrNew(getEnteredQty,POarray["custparam_whlocation"]);
			
			if(itemRecordArr != null && itemRecordArr.length>0){
				var poLineDetails =
					eBiz_RF_GetPOLineDetailsForItemArr(POarray["custparam_poid"], itemRecordArr,trantype,vlinelocation);
				if(poLineDetails != null && poLineDetails != '')
				{
					var returnedItemId=poLineDetails[0].getValue('item');
					if(returnedItemId != null && returnedItemId != '')
					{
						nlapiLogExecution('ERROR', 'returnedItemId', returnedItemId);
						var itemcolumns = nlapiLookupField('item', returnedItemId, ['recordType']);
						var itemType = itemcolumns.recordType;

						nlapiLogExecution('ERROR', 'itemType', itemType);

						itemRecord = nlapiLoadRecord(itemType, returnedItemId);
					}	
				}	
			
			}
			else
			{
				if(getItemType == 'serializedinventoryitem' || getItemType == 'serializedassemblyitem' || serialin == 'T')
				{
					if(parseInt(POarray["custparam_poqtyentered"]) != POarray["custparam_poqtyentered"])
					{
						POarray["custparam_error"] = "Decimal quantity is not allowed for serial item"; 
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('ERROR', 'Decimal quantity is not allowed for serial item', getNumber);
						return;
					}
				}
			}
			var UomQTY=getEnteredQty;
			if(returnedItemId!=null && returnedItemId!='' )
			{
				if(returnedItemId==null  || parseInt(returnedItemId)!=parseInt(ItemId) )
				{
					POarray["custparam_error"] = 'Invalid Qty/UPC';
					POarray["custparam_number"]=getNumber;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_poqtyentered"]);
					return;
				}
				var itemUomQty=checkForItemAlias(returnedItemId,getEnteredQty);

				if(parseInt(itemUomQty)!=0)
				{
					UomQTY =itemUomQty;
				}
				else 
				{
					UomQTY =1;
				}
				nlapiLogExecution('DEBUG', 'UomQTY', UomQTY);
				getNumber=parseFloat(getNumber) + parseFloat(UomQTY);

			}
			else
			{
				getNumber=getEnteredQty;
			}


			nlapiLogExecution('DEBUG', 'getNumber', getNumber);

			//case# 201410152 (Not allowing to process when qty is Zero)
			if(parseFloat(getNumber)!=0)
			{
				// Case# 20148128 starts
				//if((parseFloat(getEnteredQty)+parseFloat(getNumber)) <= recommendedQuantity)
				if((parseFloat(getNumber)) <= recommendedQuantity)
					// Case# 20148128 ends
				{	
					//POarray["custparam_poqtyentered"]=parseFloat(getEnteredQty)+parseFloat(getNumber);// Case# 20148128
					POarray["custparam_poqtyentered"]=parseFloat(getNumber);// Case# 20148128
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_checkinitemsts', 'customdeploy_ebiz_rf_cart_checkinitemsts', false, POarray);
					return;
				}
				else
				{

					POarray["custparam_error"] = "Quantity Exceeds Overage limit";
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);	
					return;	
				}
			}
			//case# 201410152 starts
			else
			{
				POarray["custparam_error"] = "Invalid Qty";
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);	
				return;	
			}
			//case# 201410152 ends
			//}
		}
		else {
			//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
			//            if (optedEvent != '' && optedEvent != null) {

			//case 20126063 Start
			//case 201410959
			nlapiLogExecution('DEBUG', 'getEnteredQty132efesd',getEnteredQty);
			if(getEnteredQty != null && getEnteredQty != '' && getEnteredQty <= 0)// case# 201415492
			{
				POarray["custparam_error"] = 'Invalid Quantity';
				POarray["custparam_number"]="";
				POarray["custparam_poqtyentered"]="";
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'Invalid Quantity1', POarray["custparam_poqtyentered"]);
				return;
			}
			if(ItemScaninQTY!='Y')
			{

				if(getItemType == 'serializedinventoryitem' || getItemType == 'serializedassemblyitem' || serialin == 'T')
				{
					if(parseInt(getEnteredQty) != getEnteredQty)
					{
						POarray["custparam_error"] = "Decimal quantity is not allowed for serial item"; 
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('ERROR', 'Decimal quantity is not allowed for serial item', getEnteredQty);
						return;
					}
				}
				if (isNaN(POarray["custparam_poqtyentered"]) || isNaN(getEnteredQty))		
				{
					POarray["custparam_error"] = 'Invalid Qty/UPC';
					POarray["custparam_number"]="";
					POarray["custparam_poqtyentered"]="";
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					nlapiLogExecution('DEBUG', 'Invalid Quantity', POarray["custparam_poqtyentered"]);
					return;
				}

			if(getEnteredQty==null  || getEnteredQty=='' ||getEnteredQty=='null' )
			{
				POarray["custparam_error"] = 'Invalid Qty/UPC';
				POarray["custparam_number"]=getNumber;
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
				nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_poqtyentered"]);
				return;
			}

			}


			nlapiLogExecution('DEBUG', 'poqty1', isNaN(POarray["custparam_poqtyentered"]));

			if(getNumber==null || getNumber=='' || getNumber=='null')
			{
				getNumber=0;
			}
//			if user scan a barcode in the Qty screen then system should first check if there is an Item Alias record
			var ItemId=request.getParameter('custparam_fetcheditemid');
			var returnedItemId='';
			//=validateSKU1(getEnteredQty, POarray["custparam_whlocation"], POarray["custparam_company"],POarray["custparam_pointernalid"]);
			nlapiLogExecution('DEBUG', 'ItemId', ItemId);
			nlapiLogExecution('DEBUG', 'returnedItemId', returnedItemId);
			var itemRecord ; 
		
			var UomQTY=0;
			if(ItemScaninQTY == 'Y')
			{
				var itemRecordArr = eBiz_RF_GetItemForItemIdWithArrNew(getEnteredQty,POarray["custparam_whlocation"]);
				
				if(itemRecordArr != null && itemRecordArr.length>0){
					var poLineDetails =
						eBiz_RF_GetPOLineDetailsForItemArr(POarray["custparam_poid"], itemRecordArr,trantype,vlinelocation);
					if(poLineDetails != null && poLineDetails != '')
					{
						var returnedItemId=poLineDetails[0].getValue('item');
						if(returnedItemId != null && returnedItemId != '')
						{
							nlapiLogExecution('ERROR', 'returnedItemId', returnedItemId);
							var itemcolumns = nlapiLookupField('item', returnedItemId, ['recordType']);
							var itemType = itemcolumns.recordType;

							nlapiLogExecution('ERROR', 'itemType', itemType);

							itemRecord = nlapiLoadRecord(itemType, returnedItemId);
						}	
					}	
				
				}
				else
				{
					if(getItemType == 'serializedinventoryitem' || getItemType == 'serializedassemblyitem' || serialin == 'T')
					{
						if(parseInt(POarray["custparam_poqtyentered"]) != POarray["custparam_poqtyentered"])
						{
							POarray["custparam_error"] = "Decimal quantity is not allowed for serial item"; 
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							nlapiLogExecution('ERROR', 'Decimal quantity is not allowed for serial item', getNumber);
							return;
						}
					}
				}
				
				
				
				if(returnedItemId!=null && returnedItemId!='' )
				{
					
					nlapiLogExecution('DEBUG', 'returnedItemId', returnedItemId);
					if(returnedItemId==null  || parseInt(returnedItemId)!=parseInt(ItemId) )
					{
						POarray["custparam_error"] = 'Invalid Qty/UPC';
						POarray["custparam_number"]=getNumber;
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_poqtyentered"]);
						return;
					}
					var itemUomQty=checkForItemAlias(returnedItemId,getEnteredQty);
					nlapiLogExecution('DEBUG', 'itemUomQty', itemUomQty);
					if(parseInt(itemUomQty)!=0)
					{
						UomQTY =itemUomQty;
					}
					else 
					{
						UomQTY =1;
					}

				}
			}
			nlapiLogExecution('DEBUG', 'UomQTY', UomQTY);
			if(parseInt(UomQTY) !=0)
			{

				getNumber=parseFloat(getNumber) + parseFloat(UomQTY);
				nlapiLogExecution('DEBUG', 'getNumber in ENT', getNumber);
				nlapiLogExecution('DEBUG', 'getItemQuantity', ExpectedQuantity);
				var tempQty;
				if (poOverage == 0)
				{
					tempQty=parseFloat(ExpectedQuantity);

				}
				else
				{
					tempQty=parseFloat(recommendedQuantity);
				}
				if ((parseFloat(getNumber)) < parseFloat(tempQty))// case# 201411405 
				{
					POarray["custparam_number"]=getNumber;
					POarray["custparam_poqtyentered"] = parseFloat(getNumber) ;
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_checkinqty', 'customdeploy_ebiz_rf_cart_checkinqty_di', false, POarray);
					return;
				}
				else
				{
					POarray["custparam_poqtyentered"] = parseFloat(getNumber);// - parseFloat(itemUomQty);		



					//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
					//            if (optedEvent != '' && optedEvent != null) {

					nlapiLogExecution('DEBUG','Pickface Recommended Quantity', pickfaceRecommendedQuantity);

					if (poOverage == 0)
					{
						var recommendedQuantity1 = parseFloat(recommendedQuantity.toString());
						nlapiLogExecution('DEBUG','Quantity Entered', POarray["custparam_poqtyentered"]);
						nlapiLogExecution('DEBUG','Recommended Quantity', parseFloat(recommendedQuantity));

						/*if (parseFloat(POarray["custparam_poqtyentered"]) <= ExpectedQuantity)
						{*/
						if (POarray["custparam_poqtyentered"] > 0 && POarray["custparam_poqtyentered"] != "" && POarray["custparam_poqtyentered"] <= ExpectedQuantity)
						{
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_checkinitemsts', 'customdeploy_ebiz_rf_cart_checkinitemsts', false, POarray);
							return;
						}
						else{

							//if(parseFloat(getNumber) > parseFloat(ExpectedQuantity))
							if(parseFloat(getNumber) > parseFloat(recommendedQuantity1))
							{
								getNumber = parseFloat(getNumber)-parseFloat(itemUomQty);
							}

							POarray["custparam_number"]=getNumber;
							//case# 20148449 starts
							POarray["custparam_poqtyentered"]='';
							//case# 20148449 ends
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_poqtyentered"]);
							return;
						}
						/*}*/

						if (pickfaceRecommendedQuantity > 0)
						{
							if (parseFloat(POarray["custparam_poqtyentered"]) > parseFloat(pickfaceRecommendedQuantity))
							{	
								POarray["custparam_pickfaceexception"] = pickfaceRecommendedQuantity;
								/*
								 * If the option entered in the pickface exception is 'N', then allow the user to enter
								 * 	only the pickface recommended quantity
								 * else if the option entered in the pickface exception is 'Y', then generate a binlocation
								 * 	from the bulk location. Do not putaway to the pickface location.
								 */
								nlapiLogExecution('DEBUG','Erroflag', Erroflag);
								if ((bulkLocation != 'Y' || bulkLocation == null) && Erroflag=='F')
								{
									nlapiLogExecution('DEBUG','Into Pickface Recommended Quantity Validation','here');
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_pickface_exp', 'customdeploy_ebiz_rf_cart_pickface_exp', false, POarray);
									nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_pickfaceexception"]);
									return;
								}
							}
						}
					}
					else{
						var palletQuantity1 = parseFloat(palletQuantity.toString());
						var recommendedQuantity1 = parseFloat(recommendedQuantity.toString());

						nlapiLogExecution('DEBUG', 'palletQuantity1 ',palletQuantity1 );
						nlapiLogExecution('DEBUG', 'recommendedQuantity1 ',recommendedQuantity1 );
						if(palletQuantity1 >= 1)
						{
							if (POarray["custparam_poqtyentered"] > 0 && POarray["custparam_poqtyentered"] != "" 
								&& parseFloat(POarray["custparam_poqtyentered"]) <= recommendedQuantity1)
							{
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_checkinitemsts', 'customdeploy_ebiz_rf_cart_checkinitemsts', false, POarray);
								return;
							}
							else{
								POarray["custparam_error"] = 'QUANTITY EXCEEDS OVERAGE LIMIT';

								if(parseFloat(getNumber) > parseFloat(recommendedQuantity1))
								{
									getNumber = parseFloat(getNumber)-parseFloat(itemUomQty);
								}
								POarray["custparam_number"]=getNumber;
								//case# 20148449 starts
								POarray["custparam_poqtyentered"]='';
								//case# 20148449 ends
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_poqtyentered"]);
								return;
							}
						}
						else
						{
							POarray["custparam_number"]=getNumber;
							POarray["custparam_error"] = 'PALLET QUANTITY IS NOT DEFINED FOR ITEM : '+POarray["custparam_getPOItem"];
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							return;
						}
					}
				}
			}
			else
			{


				nlapiLogExecution('DEBUG', 'request.getParameter("custparam_number")', request.getParameter('custparam_number'));
				if(request.getParameter('custparam_number')!=null && request.getParameter('custparam_number')!='' && request.getParameter('custparam_number')!='null')
				{
					//POarray["custparam_poqtyentered"]= parseFloat(request.getParameter('custparam_number')) + parseFloat(getEnteredQty);
					POarray["custparam_poqtyentered"]= parseFloat(getEnteredQty);
					nlapiLogExecution('DEBUG', 'POarray["custparam_poqtyentered"]', POarray["custparam_poqtyentered"]);
				}
				else if(request.getParameter("custparam_poqtyentered") !=null && request.getParameter("custparam_poqtyentered")!='' && request.getParameter("custparam_poqtyentered")!='null')
				{
					//POarray["custparam_poqtyentered"]= parseFloat(request.getParameter('custparam_poqtyentered')) + parseFloat(getEnteredQty);
					POarray["custparam_poqtyentered"] = parseFloat(getEnteredQty);
				}
				else
				{

				}



				//case #20149382� start
				POarray["custparam_poqtyentered"]= parseFloat(getEnteredQty);
				//case #20149382� end
				var venteredqty=request.getParameter('enterqty');

				if (isNaN(POarray["custparam_poqtyentered"]) || venteredqty.trim()=="")
				{
					nlapiLogExecution('DEBUG', 'poqty1', isNaN(POarray["custparam_poqtyentered"]));

					POarray["custparam_error"] = "Please enter Quantity";//201415492

					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
					return;

				} 
				else
				{
					//case 20126063 end		

					nlapiLogExecution('DEBUG','Pickface Recommended Quantity', pickfaceRecommendedQuantity);

					if (poOverage == 0)
					{
						var recommendedQuantity1 = parseFloat(recommendedQuantity.toString());
						nlapiLogExecution('DEBUG','Quantity Entered', POarray["custparam_poqtyentered"]);
						nlapiLogExecution('DEBUG','Recommended Quantity', parseFloat(recommendedQuantity));

						if (parseFloat(POarray["custparam_poqtyentered"]) <= ExpectedQuantity)
						{
							if (POarray["custparam_poqtyentered"] > 0 && POarray["custparam_poqtyentered"] != "")
							{
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_checkinitemsts', 'customdeploy_ebiz_rf_cart_checkinitemsts', false, POarray);
								return;
							}
							else{
								//case# 20148449 starts
								POarray["custparam_poqtyentered"]='';
								//case# 20148449 ends
								nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_poqtyentered"]);
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								return;

							}
						}
						else if (parseFloat(POarray["custparam_poqtyentered"]) > ExpectedQuantity)
						{
							//	if the 'Send' button is clicked without any option value entered,
							//  it has to show an error message. The next screen to which it has to navigate is to the error screen.
							POarray["custparam_error"] = st14;
							//case# 20148449 starts
							POarray["custparam_poqtyentered"]='';
							//case# 20148449 ends
							Erroflag='T';
							nlapiLogExecution('DEBUG','Test4A', 'Test4A');
							nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_poqtyentered"]);
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							return;

						}
						else
						{
							POarray["custparam_error"] = st13;
							Erroflag='T';
							nlapiLogExecution('DEBUG','Test4AA', 'Test4AA');
							nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_poqtyentered"]);
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							return;

						}

						/*
						 * Check the pickface recommended quantity is less than the quantity entered.
						 * If the quantity entered is more than the pickface recommended quantity, ask the user to
						 * 		confirm if the quantity is to be putaway to the bulk location
						 * else
						 * 		follow the normal process of putting away the item to the assigned fixed pick location. 
						 */

						if (pickfaceRecommendedQuantity > 0)
						{
							if (parseFloat(POarray["custparam_poqtyentered"]) > parseFloat(pickfaceRecommendedQuantity))
							{	
								POarray["custparam_pickfaceexception"] = pickfaceRecommendedQuantity;
								/*
								 * If the option entered in the pickface exception is 'N', then allow the user to enter
								 * 	only the pickface recommended quantity
								 * else if the option entered in the pickface exception is 'Y', then generate a binlocation
								 * 	from the bulk location. Do not putaway to the pickface location.
								 */
								nlapiLogExecution('DEBUG','Erroflag', Erroflag);
								if ((bulkLocation != 'Y' || bulkLocation == null) && Erroflag=='F')
								{
									nlapiLogExecution('DEBUG','Into Pickface Recommended Quantity Validation','here');
									//need to clarify
									nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_pickfaceexception"]);
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_pickface_exp', 'customdeploy_ebiz_rf_cart_pickface_exp', false, POarray);
									return;

								}
							}
						}
					}
					else{
						var palletQuantity1 = parseFloat(palletQuantity.toString());
						var recommendedQuantity1 = parseFloat(recommendedQuantity.toString());

						if(palletQuantity1 >= 1)
						{
							if (POarray["custparam_poqtyentered"] > 0 && POarray["custparam_poqtyentered"] != "" 
								&& parseFloat(POarray["custparam_poqtyentered"]) <= recommendedQuantity1)
							{
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_checkinitemsts', 'customdeploy_ebiz_rf_cart_checkinitemsts', false, POarray);
							}
							else{
								POarray["custparam_error"] = st15;
								//case# 20148449 starts
								POarray["custparam_poqtyentered"]='';
								//case# 20148449 ends
								nlapiLogExecution('DEBUG', 'Entered Quantity', POarray["custparam_poqtyentered"]);
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
								return;

							}
						}
						else
						{
							POarray["custparam_error"] = st16 + POarray["custparam_getPOItem"];
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
							return;
						}
					}
				}
				nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
			}
			//}
		}//end of else loop.
	}
}


function fetchPalletQuantity(itemId, location, company)
{
	nlapiLogExecution('DEBUG','itemId',itemId);
	nlapiLogExecution('DEBUG','location in fetchPalletQuantity',location);
	var itemPalletQuantity = 0;
	var itemFilters = new Array();
	itemFilters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemId);
	itemFilters[1] = new nlobjSearchFilter('custrecord_ebizuomlevelskudim', null, 'is', 3);
	//case # 20127022 start, added below location filter.
	if(location!=null && location!='')
		itemFilters[2] = new nlobjSearchFilter('custrecord_ebizsiteskudim',null,'anyof',['@NONE@',location]);
	//End
	var itemColumns = new Array();
	itemColumns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	itemColumns[1] = new nlobjSearchColumn('custrecord_ebizsiteskudim');
	itemColumns[2] = new nlobjSearchColumn('custrecord_ebizcompanyskudimension');
	itemColumns[3] = new nlobjSearchColumn('custrecord_ebizqty'); 

	var itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, itemFilters, itemColumns);

	if (itemSearchResults != null)
	{
		itemPalletQuantity = itemSearchResults[0].getValue(itemColumns[3]);
		nlapiLogExecution('DEBUG','Item Pallet Quantity',itemPalletQuantity);
	}
	return itemPalletQuantity;
}

function checkPOOverage(POId,location,company){
	nlapiLogExecution('DEBUG','PO Internal Id', POId);
	var poOverage = 0;

	var poFields = ['custbody_nswmsporeceipttype'];
	var poColumns = nlapiLookupField('transaction', POId, poFields);

	var receiptType = poColumns.custbody_nswmsporeceipttype;

	nlapiLogExecution('DEBUG','here', receiptType);

	if (receiptType != "" && receiptType != null) 
	{
		var receiptFields = ['custrecord_po_overages'];
		var receiptColumns = nlapiLookupField('customrecord_ebiznet_receipt_type', receiptType, receiptFields);

		nlapiLogExecution('DEBUG','here', receiptType);

		poOverage = receiptColumns.custrecord_po_overages;
	}
	nlapiLogExecution('DEBUG','Out of check PO Overage', poOverage);
	return poOverage;
}

function itemRemainingQuantity(poInternalId, itemId, lineno, itemQuantity, itemQuantityReceived,location, company){
	/*
	 * Fetch the check-in, putconfirm quantity from the transaction order details for the PO# and the line#
	 * if check-in quantity is 0 (Zero)
	 * 	remaining quantity = order quantity
	 * else if check-in quantity has value 
	 *		remaining quantity = order quantity - (check-in quantity) - quantity received
	 */

	nlapiLogExecution('DEBUG','itemId',itemId);
	nlapiLogExecution('DEBUG','lineno',lineno);
	nlapiLogExecution('DEBUG','poInternalId',poInternalId);

	nlapiLogExecution('DEBUG','Item Remaining Quantity','About to calculate');
	var systemRule= GetSystemRuleForPostItemReceiptby(location);
	nlapiLogExecution('DEBUG','systemRule',systemRule);

	var checkinQuantity = 0;
	var remainingQuantity = 0;
	var putConfirmQuantity = 0;

	var transactionFilters = new Array();
	transactionFilters[0] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', poInternalId);
	transactionFilters[1] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_sku_no', null, 'equalto', itemId);
	transactionFilters[2] = new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', lineno);

	var transactionColumns = new Array();
	transactionColumns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_ebiz_sku_no');
	transactionColumns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_orderline_no');
	transactionColumns[2] = new nlobjSearchColumn('custrecord_orderlinedetails_checkin_qty');
	transactionColumns[3] = new nlobjSearchColumn('custrecord_orderlinedetails_putconf_qty');

	var transactionSearchresults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, transactionFilters, transactionColumns);

	if (transactionSearchresults != null && transactionSearchresults.length > 0)
	{
		for(var i = 0; i <= transactionSearchresults.length; i++)
		{
			checkinQuantity = transactionSearchresults[0].getValue('custrecord_orderlinedetails_checkin_qty');
			putConfirmQuantity = transactionSearchresults[0].getValue('custrecord_orderlinedetails_putconf_qty');
		}
	}

	if(checkinQuantity == "")
	{
		checkinQuantity = 0;
	}

	if(putConfirmQuantity == "")
	{
		putConfirmQuantity = 0;
	}

	nlapiLogExecution('DEBUG','Check-In Quantity',checkinQuantity);
	nlapiLogExecution('DEBUG','Quantity',itemQuantity);
	nlapiLogExecution('DEBUG','Received Quantity',itemQuantityReceived);
	nlapiLogExecution('DEBUG','Put Confirm Quantity',putConfirmQuantity);

	if (checkinQuantity == 0)
	{
		remainingQuantity = parseFloat(itemQuantity);
	}
	else if(systemRule !='PO')
	{
		remainingQuantity = parseFloat(itemQuantity) - (parseFloat(checkinQuantity)-parseFloat(putConfirmQuantity)) - parseFloat(itemQuantityReceived);
		nlapiLogExecution('DEBUG','Remaining Quantity',remainingQuantity);

		/*		if(putConfirmQuantity == 0)		
		{
			remainingQuantity = parseFloat(itemQuantity) - parseFloat(checkinQuantity) - parseFloat(itemQuantityReceived);
			nlapiLogExecution('DEBUG','Remaining Quantity: Checked-in',remainingQuantity);
		}
		else
		{
			remainingQuantity = parseFloat(itemQuantity) - parseFloat(itemQuantityReceived);
			nlapiLogExecution('DEBUG','Remaining Quantity: Putaway Confirmed',remainingQuantity);
		}
		 */
	}
	else
	{
		var GetTotalQty = GetTotalReceivedQty(poInternalId, itemId, lineno);
		nlapiLogExecution('DEBUG','GetTotalQty',GetTotalQty);
		var DirectQty = parseFloat(itemQuantityReceived)-(parseFloat(GetTotalQty));
		nlapiLogExecution('DEBUG','DirectQty',DirectQty);

		remainingQuantity = parseFloat(itemQuantity) - (parseFloat(checkinQuantity)+parseFloat(DirectQty));
		nlapiLogExecution('DEBUG','remainingQuantity',remainingQuantity);
	}
	return remainingQuantity;
}

function priorityPutawayQuantity(itemId, location, company)
{
	nlapiLogExecution('DEBUG','Priority Putaway Quantity','Beginning');

//	/*
//	* Search for the item in Pick Face Location custom record. Fetch the required columns 
//	* viz., Replen Quantity, Maximum Quantity, Bin Location


	var priorityPutawayQuantity = 0;

	var priorityPutawayFilters = new Array();
	priorityPutawayFilters.push(new nlobjSearchFilter('custrecord_pickfacesku', null,'is', itemId));
	priorityPutawayFilters.push(new nlobjSearchFilter('custrecord_priorityputaway', null, 'is', 'T'));	
	priorityPutawayFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	
	if(location!=null && location!='')
		priorityPutawayFilters.push(new nlobjSearchFilter('custrecord_pickface_location', null, 'anyof', [location]));

	var priorityPutawayColumns = new Array();
	priorityPutawayColumns[0] = new nlobjSearchColumn('custrecord_replenqty');
	priorityPutawayColumns[1] = new nlobjSearchColumn('custrecord_maxqty');
	priorityPutawayColumns[2] = new nlobjSearchColumn('custrecord_pickfacesku');
	priorityPutawayColumns[3] = new nlobjSearchColumn('custrecord_pickbinloc');

	var priorityPutawaySearchresults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc',null,priorityPutawayFilters,priorityPutawayColumns);

	if (priorityPutawaySearchresults != null)
	{
		for(var i = 0; i < priorityPutawaySearchresults.length; i++)
		{
			var ReplenQuantity = priorityPutawaySearchresults[0].getValue('custrecord_replenqty');
			var MaximumQuantity = priorityPutawaySearchresults[0].getValue('custrecord_maxqty');
			var pickfaceItem = priorityPutawaySearchresults[0].getValue('custrecord_pickfacesku');
			var binLocation = priorityPutawaySearchresults[0].getValue('custrecord_pickbinloc');
		}

		nlapiLogExecution('DEBUG','Maximum Quantity',MaximumQuantity);
		nlapiLogExecution('DEBUG','binLocation',binLocation);

//		/*
//		* Search for the data in inventory for the item, bin location and in storage locations. 
//		* Retrieve the quantity for those locations and sum the quantity for the individual lines fetched.

		var inventoryQuantity = 0;

		var inventoryFilters = new Array();
		inventoryFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', itemId));
		inventoryFilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', binLocation));
		inventoryFilters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3,19]));

		var inventoryColumns = new Array();
		inventoryColumns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');

		var inventorySearchResults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, inventoryFilters, inventoryColumns);

		if (inventorySearchResults != null)
		{
			for(var i = 0; i < inventorySearchResults.length; i++)
			{
				inventoryQuantity = parseFloat(inventoryQuantity) + parseFloat(inventorySearchResults[i].getValue('custrecord_ebiz_qoh'));
			}
		}

		nlapiLogExecution('DEBUG','Inventory Quantity',parseFloat(inventoryQuantity));

//		/*
//		* Search for any open tasks viz., Putaway, Replenishment, Inventory Move tasks

		var opentaskInventoryQuantity = 0;

		var opentaskInventoryFilters = new Array();
		opentaskInventoryFilters[0] = new nlobjSearchFilter('custrecord_sku', null, 'is', itemId);
		opentaskInventoryFilters[1] = new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', binLocation);
		opentaskInventoryFilters[2] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [2,8,9]);
		opentaskInventoryFilters[3] = new nlobjSearchFilter('custrecord_actendloc', null, 'anyof', ['@NONE@']);

		var opentaskInventoryColumns = new Array();
		opentaskInventoryColumns[0] = new nlobjSearchColumn('custrecord_expe_qty');

		var opentaskInventorySearch = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, opentaskInventoryFilters, opentaskInventoryColumns);

		if (opentaskInventorySearch != null)
		{
			for (var i = 0; i < opentaskInventorySearch.length; i++)
			{
				opentaskInventoryQuantity = opentaskInventoryQuantity + parseFloat(opentaskInventorySearch[i].getValue('custrecord_expe_qty')); 
			}
		}
		nlapiLogExecution('DEBUG','Opentask Inventory Quantity',opentaskInventoryQuantity);
//		/*
//		* Calculate the Recommended quantity for the pick face location from the above quantity retrieved.

		var pickfaceRecommendedQuantity = parseFloat(MaximumQuantity) - (parseFloat(inventoryQuantity)+ parseFloat(opentaskInventoryQuantity));

		nlapiLogExecution('DEBUG','Pickface Recommended Quantity',pickfaceRecommendedQuantity);
	}
	return pickfaceRecommendedQuantity;
}


/**
 * @param getFetchedItemId
 * @returns
 */
function GetPickFaceLocation(getFetchedItemId,location)
{
	try{
		var binloc;
		var PickFaceArray=new Array();
		var PickFaceFilter = new Array();


		PickFaceFilter.push(new nlobjSearchFilter('custrecord_pickfacesku', null,'is', getFetchedItemId));
		PickFaceFilter.push(new nlobjSearchFilter('custrecord_priorityputaway', null, 'is', 'T'));	
		PickFaceFilter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	
		if(location!=null && location!='')
			PickFaceFilter.push(new nlobjSearchFilter('custrecord_pickface_location', null, 'anyof', [location]));

		var PickFaceColumn = new Array();
		PickFaceColumn[0] = new nlobjSearchColumn('custrecord_pickbinloc');

		var PickFaceSearchresults = nlapiSearchRecord('customrecord_ebiznet_pickfaceloc',null,PickFaceFilter,PickFaceColumn);
		if(PickFaceSearchresults!=null&&PickFaceSearchresults!="")
		{
			var Rec=PickFaceSearchresults.length;
			if(parseFloat(Rec)==1)
				binloc=PickFaceSearchresults[0].getText('custrecord_pickbinloc');
			else
				binloc="";

		}
		return binloc;
	}
	catch(exp)
	{
		nlapiLogExecution('DEBUG','Exception in GetPickFaceLocation function ',exp);
	}
}

function geteBizItemDimensions(itemid)
{
	var searchRec = new Array();
	var filter = new Array();
	filter.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemid));
	filter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));	

	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebizuomskudim') ;
	column[1] = new nlobjSearchColumn('custrecord_ebizqty') ;
	column[2] = new nlobjSearchColumn('custrecord_ebizbaseuom') ;
	column[3] = new nlobjSearchColumn('custrecord_ebiznsuom') ;
	column[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim') ;
	searchRec= nlapiSearchRecord('customrecord_ebiznet_skudims', null, filter, column);

	return searchRec;

}
function validateSKU1(itemNo, location, company,poid){
	nlapiLogExecution('DEBUG', 'validateSKU', 'Start');

	var inputParams = 'Item = ' + itemNo + '<br>';
	inputParams = inputParams + 'Location = ' + location + '<br>';
	inputParams = inputParams + 'Company = ' + company + '<br>';;
	inputParams = inputParams + 'PO Id = ' + poid;
	nlapiLogExecution('DEBUG', 'Input Parameters', inputParams);

	var currItem = eBiz_RF_GetItemForItemNo1(itemNo);



	if(currItem==""){

		var skuAliasFilters = new Array();
		skuAliasFilters.push(new nlobjSearchFilter('name',null, 'is',itemNo));
		skuAliasFilters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		if(location !=null && location!='')
			skuAliasFilters.push(new nlobjSearchFilter('custrecord_ebiz_location',null, 'is',location));


		var skuAliasCols = new Array();
		skuAliasCols[0] = new nlobjSearchColumn('custrecord_ebiz_item');

		var skuAliasResults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, skuAliasFilters, skuAliasCols);
		if(skuAliasResults !=null && skuAliasResults!='')
		{
			currItem = skuAliasResults[0].getValue('custrecord_ebiz_item');
		}
	}

	if(currItem == ""){
		currItem = eBiz_RF_GetItemBasedOnUPCCode(itemNo, location);
	}

	if(currItem == ""){
		var vendor='';
		if(poid!=null && poid!='')
		{
			//case# 20148250 starts (transaction type issue)
			var trantype = nlapiLookupField('transaction', poid, 'recordType');

			var TransformRecord =nlapiLoadRecord(trantype, poid);

			//var po  = nlapiLoadRecord('purchaseorder',poid);
			vendor=TransformRecord.getFieldValue('entity');
			//case# 20148250 end
		}
		currItem = eBiz_RF_GetItemFromSKUDims(itemNo, location,vendor);


	}

	var logMsg = "";
	if(currItem == ""){
		logMsg = 'Unable to retrieve item';
		currItem = null;
	} else {

		logMsg = 'Item = ' + currItem;
		nlapiLogExecution('DEBUG', 'Item Retrieved', logMsg);
		if(currItem !='' && isNaN(currItem))
		{
			var itemRecord = eBiz_RF_GetItemForItemId(currItem, location);
			currItem=itemRecord.getId();
			logMsg = 'Item = ' + currItem;
		}
	}

	nlapiLogExecution('DEBUG', 'Item Retrieved', logMsg);
	nlapiLogExecution('DEBUG', 'validateSKU', 'End');
	return currItem;
}

function eBiz_RF_GetItemForItemNo1(itemNo,location){
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemForItemNo', 'Start');
	nlapiLogExecution('DEBUG', 'Input Item No', itemNo);

	var currItem = "";

	var sitemno = "";
	var context = nlapiGetContext();
	var userAccountId = context.getCompany();
	if(userAccountId=='1285441')
	{
		sitemno = checkserialritem(itemNo);
	}
	if(sitemno != "")
	{
		itemNo = sitemno;
	}	

	var filters = new Array();
	filters.push(new nlobjSearchFilter('nameinternal', null, 'is', itemNo));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	if(location!=null && location!='' && location!='null')
		filters.push(new nlobjSearchFilter('location', null, 'anyof', ['@NONE@',location]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('itemid');
	columns[0].setSort(true);


	var itemSearchResults = nlapiSearchRecord('item', null, filters, columns);

	if(itemSearchResults != null)
		currItem = itemSearchResults[0].getId();

	var logMsg = 'Item = ' + currItem;
	nlapiLogExecution('DEBUG', 'Item Retrieved', logMsg);
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemForItemNo', 'End');

	return currItem;
}
function checkForItemAlias(ItemId,ItemAlias)
{

	return eBiz_RF_GetItemQTYForItemAlias(ItemId,ItemAlias);

}
function eBiz_RF_GetItemQTYForItemAlias(ItemId,ItemAlias){
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemCubeForItem', 'Start');

	var BaseUOM=eBiz_RF_GetUOMAgainstItemAlias(ItemId,ItemAlias);

	var BaseUomQty=0;
	if(BaseUOM !='')
	{
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', ItemId);
		filters[1] = new nlobjSearchFilter('custrecord_ebizuomskudim', null, 'anyof', BaseUOM);

		var columns = new Array();	
		columns[0] = new nlobjSearchColumn('custrecord_ebizqty');

		var itemDimSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);

		if(itemDimSearchResults != null && itemDimSearchResults.length > 0){

			//To get the baseuom qty .
			BaseUomQty=itemDimSearchResults[0].getValue('custrecord_ebizqty');
			//end of code as of 13 feb 2012.
		}



		nlapiLogExecution('DEBUG', 'Retrieved BaseUomQty', 'BaseUomQty = ' + BaseUomQty);
		nlapiLogExecution('DEBUG', 'eBiz_RF_GetItemCubeForItem', 'End');
	}
	return BaseUomQty;
}

function eBiz_RF_GetUOMAgainstItemAlias(ItemId,ItemAlias)
{
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetUOMAgainstItemAlias', 'Start');

	var baseuom = "";

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_ebiz_item', null, 'anyof', ItemId));
	if(ItemAlias !=null && ItemAlias!='')
		filters.push(new nlobjSearchFilter('name', null, 'is', ItemAlias));
	/*if(location!=null && location!='')
		filters.push(new nlobjSearchFilter('location', null, 'anyof', ['@NONE@',location]));*/

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_uom');

	var itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_sku_alias', null, filters, columns);

	if(itemSearchResults != null)
		baseuom = itemSearchResults[0].getValue('custrecord_ebiz_uom');

	var logMsg = 'baseuom = ' + baseuom;
	nlapiLogExecution('DEBUG', 'baseuom Retrieved', logMsg);
	nlapiLogExecution('DEBUG', 'eBiz_RF_GetUOMAgainstItemAlias', 'End');

	return baseuom;
}


function GetSystemRuleForPostItemReceiptby(whloc) //Case# 20149154
{
	try
	{
		var rulevalue='';	
		var filter=new Array();
		filter.push(new nlobjSearchFilter('name',null,'is','Post Item Receipt by'));
		//filter.push(new nlobjSearchFilter('custrecord_ebizsite',null,'anyof',['@NONE@',whLocation]));

		// case no start 20126968
		var vRoleLocation=getRoledBasedLocation();
		var resloc=new Array();
		resloc.push("@NONE@");

		nlapiLogExecution('ERROR','vRoleLocation',vRoleLocation);
		//case# 20149824
		if(whloc!=null && whloc!='')
		{
			filter.push(new nlobjSearchFilter('custrecord_ebizsite',null,'anyof',['@NONE@',whloc]));
		}
		else if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
		{
			for(var count=0;count<vRoleLocation.length;count++)
				resloc.push(vRoleLocation[count]);
			filter.push(new nlobjSearchFilter('custrecord_ebizsite', null, 'anyof', resloc));
		}
		// Case# 20149154 starts
		/*else
			{
			filter.push(new nlobjSearchFilter('custrecord_ebizsite',null,'anyof',['@NONE@',whloc]));
			}*/
		// Case# 20149154 ends
		// case no end 20126968
		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebizrulevalue');
		column[1]=new nlobjSearchColumn('custrecord_ebizsite');

		var searchresult=nlapiSearchRecord('customrecord_ebiznet_sysrules',null,filter,column);
		var siteval = '';
		if(searchresult!=null && searchresult!="")
		{
			for(var k = 0;k<searchresult.length;k++)
			{
				siteval = searchresult[k].getValue('custrecord_ebizsite');
				nlapiLogExecution('ERROR','whloc',whloc);
				nlapiLogExecution('ERROR','siteval',siteval);

				if(whloc!=null && whloc!='')
				{
					if(whloc == siteval)
					{
						rulevalue=searchresult[k].getValue('custrecord_ebizrulevalue');
						break;
					}
				}
			}

			if(rulevalue==null || rulevalue=='')
			{
				for(var z = 0;z<searchresult.length;z++)
				{
					siteval = searchresult[z].getValue('custrecord_ebizsite');

					if(siteval==null || siteval=='')
					{
						rulevalue=searchresult[z].getValue('custrecord_ebizrulevalue');
						break;
					}
				}

			}
		}

		nlapiLogExecution('ERROR','rulevalue',rulevalue);
		if(rulevalue!=null && rulevalue!='undefined' && rulevalue!='null' && rulevalue!='')
		{
			if(rulevalue.trim()!='LP' && rulevalue.trim()!='PO')
			{
				rulevalue='LP';
			}
		}
		else
		{
			rulevalue='LP';
		}
		nlapiLogExecution('ERROR','rulevalue',rulevalue);
		return rulevalue;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in GetSystemRuleForPostItemReceiptby',exp);
	}
}

//case 201410986
function CheckItemDimens(itemId, location,poItemPackcode)
{
	nlapiLogExecution('DEBUG', 'CheckItemDimens-itemId ',itemId);
	nlapiLogExecution('DEBUG', 'CheckItemDimens-location ',location);
	nlapiLogExecution('DEBUG', 'CheckItemDimens-poItemPackcode ',poItemPackcode);
	var itemSearchResults='';
	var itemFilters = new Array();
	itemFilters.push(new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', itemId));
	itemFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	//case 201410846
	if(location!=null&&location!='')
		itemFilters.push(new nlobjSearchFilter('custrecord_ebizsiteskudim',null,'anyof',['@NONE@',location]));


	if(poItemPackcode!=null && poItemPackcode!='' && poItemPackcode!='null')
		itemFilters.push(new nlobjSearchFilter('custrecord_ebizpackcodeskudim', null, 'is',poItemPackcode));

	var itemColumns = new Array();
	itemColumns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	itemColumns[1] = new nlobjSearchColumn('custrecord_ebizsiteskudim');
	itemColumns[2] = new nlobjSearchColumn('custrecord_ebizcompanyskudimension');
	itemColumns[3] = new nlobjSearchColumn('custrecord_ebizqty'); 	
	itemColumns[4] = new nlobjSearchColumn('custrecord_ebizuomlevelskudim').setSort(true); 	

	itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, itemFilters, itemColumns);
	return itemSearchResults;
}

