/***************************************************************************
                                                                 
                                                  eBizNET Solutions Inc 
****************************************************************************
*
*  $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inbound/UserEvents/ebiz_receipt_import_UE.js,v $
*  $Revision: 1.1.10.1 $
*  $Date: 2013/09/11 15:23:51 $
*  $Author: rmukkera $
*  $Name: t_NSWMS_2014_1_1_174 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_receipt_import_UE.js,v $
*  Revision 1.1.10.1  2013/09/11 15:23:51  rmukkera
*  Case# 20124376
*
*
****************************************************************************/
function fnReceiptEntry(type, form, request)
{
	nlapiLogExecution('ERROR','type',type);

	if(type=='create' || type=='edit')
	{ 

		var poimpid=nlapiGetFieldValue('custrecord_ebiz_poreceipt_pono');
		nlapiLogExecution('ERROR','poimpid',poimpid);

		var poimpline=nlapiGetFieldValue('custrecord_ebiz_poreceipt_lineno');
		nlapiLogExecution('ERROR','poimpline',poimpline);

		var poimpitem=nlapiGetFieldValue('custrecord_ebiz_poreceipt_item');
		nlapiLogExecution('ERROR','poimpitem',poimpitem);

		var poimplineQty=nlapiGetFieldValue('custrecord_ebiz_poreceipt_polineordqty');
		nlapiLogExecution('ERROR','poimplineQty',poimplineQty);

		var poimpTrailNo=nlapiGetFieldValue('custrecord_ebiz_poreceipt_trailerno');
		nlapiLogExecution('ERROR','poimpTrailNo',poimpTrailNo);  


		/*var filter = new Array();
		filter.push(new nlobjSearchFilter('tranid',null,'is',poimpid));
		filter.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));
		//filter.push(new nlobjSearchFilter('lineno', null, 'is', '1'));

		var columns = new Array();
		columns.push(new nlobjSearchColumn('tranid'));
		columns.push(new nlobjSearchColumn('line'));
		columns.push(new nlobjSearchColumn('quantity'));
		columns.push(new nlobjSearchColumn('item'));
		columns.push(new nlobjSearchColumn('mainline'));
		columns.push(new nlobjSearchColumn('status')); 
		columns.push(new nlobjSearchColumn('location'));
		columns.push(new nlobjSearchColumn('custbody_nswms_company'));

		var poload= nlapiSearchRecord('purchaseorder',null,filter,columns);*/

		var poload = nlapiLoadRecord('purchaseorder', poimpid); 
		if(poload != null && poload != "")
		{
			var vLineCount=poload.getLineItemCount('item');
			//nlapiLogExecution('ERROR','vLineCount',vLineCount);
			var pointernalid;

			var polocation;

			var poComp= poload.getFieldValue('custbody_nswms_company');
			polocation= poload.getFieldValue('location');
			for(var s=1;s<=vLineCount;s++)
			{
				var poline;
				var poqty;
				var poitem;
				var itemid;
				var postatus;
				itemid = poload.getLineItemValue('item', 'item', s);
				nlapiLogExecution('ERROR','Into PO Exists');
				poline= poload.getLineItemValue('item', 'line', s);
				nlapiLogExecution('ERROR', 'poimpline', poimpline);
				nlapiLogExecution('ERROR', 'poline', poline);
				nlapiLogExecution('ERROR', 'itemid', itemid);
				nlapiLogExecution('ERROR', 'poimpitem', poimpitem);
				nlapiLogExecution('ERROR', 'poimpid', poimpid);
				if(poimpline != null && poimpline != "" && poimpline==poline)
				{ 
					if(itemid == poimpitem)
					{
						nlapiLogExecution('ERROR', 'poimpline', poimpline);
						//var pointernalid= poDataSearch[s].getId(); 

						poqty= poload.getLineItemValue('item', 'quantity', s);
						poitem= poload.getLineItemText('item', 'item', s);

						postatus= poload.getLineItemValue('item', 'custcol_ebiznet_item_status', s); 
					}
					else
					{
						nlapiLogExecution('ERROR', 'Invalid Item ', poimpitem);
						var cannotDelError = nlapiCreateError('CannotDelete', ' Invalid Item ' + poimpitem, true);
						throw cannotDelError; //throw this error object, do not catch it
					}

				}
			}

			nlapiLogExecution('ERROR','polocation',polocation); 
			nlapiLogExecution('ERROR','poComp',poComp); 
			//nlapiLogExecution('ERROR','pointernalid',pointernalid); 
			var poOverage = checkPOOverage(poimpid,polocation, null);
			var ItemRecommendedQuantity =0;
			if (poOverage == null || poOverage == ""){
				poOverage = 0;
				ItemRecommendedQuantity=poqty;
			}
			else
			{	
				ItemRecommendedQuantity = parseFloat(poqty) + ((parseFloat(poqty)* parseFloat(poOverage))/100);
			}

			var filterRcpt = new Array();
			filterRcpt.push(new nlobjSearchFilter('custrecord_ebiz_poreceipt_pono',null,'is',poimpid));
			filterRcpt.push(new nlobjSearchFilter('custrecord_ebiz_poreceipt_lineno', null, 'is', poline));
			filterRcpt.push(new nlobjSearchFilter('custrecord_ebiz_poreceipt_item', null, 'anyof', poimpitem));
			//filterRcpt.push(new nlobjSearchFilter('custrecord_ebiz_poreceipt_receiptno', null, 'is', poimpTrailNo));
			//filterRcpt.push(new nlobjSearchFilter('custrecord_ebiz_poreceipt_trailerno', null, 'is', poimpTrailNo));

			//filterRcpt.push(new nlobjSearchFilter('custrecord_ebiz_poreceipt_polineordqty', null, 'equalto', poimplineQty));

			//filter.push(new nlobjSearchFilter('lineno', null, 'is', '1'));

			var rcptColumns = new Array();
			rcptColumns[0] = new nlobjSearchColumn('custrecord_ebiz_poreceipt_receiptqty');
			 

			var poReceiptSearch= nlapiSearchRecord('customrecord_ebiznet_trn_poreceipt',null,filterRcpt,rcptColumns);
			nlapiLogExecution('ERROR', 'poimpline afterduplicatechecking', poimpid);
			nlapiLogExecution('ERROR','ItemRecommendedQuantity ', ItemRecommendedQuantity);
			if(poReceiptSearch != null && poReceiptSearch != "")
			{	
				nlapiLogExecution('ERROR', 'poReceiptSearch.length ', poReceiptSearch.length);
				var totRecQty=0;
				for(var j=0;j<poReceiptSearch.length;j++)
				{
					nlapiLogExecution('ERROR','qty ', poReceiptSearch[j].getValue('custrecord_ebiz_poreceipt_receiptqty'));
					 
					if(poReceiptSearch[j].getValue('custrecord_ebiz_poreceipt_receiptqty') != null && poReceiptSearch[j].getValue('custrecord_ebiz_poreceipt_receiptqty') != "")
					{
						totRecQty=parseFloat(totRecQty) + parseFloat(poReceiptSearch[j].getValue('custrecord_ebiz_poreceipt_receiptqty'));
					}	

				}
				nlapiLogExecution('ERROR', 'totRecQty ', totRecQty);
				
				if((parseFloat(totRecQty)+ parseFloat(poimplineQty))>ItemRecommendedQuantity)
				{
					//error PO Overage
					nlapiLogExecution('ERROR', 'PO Overage Exceeds poOverage ', poOverage);
					var cannotDelError = nlapiCreateError('CannotDelete', ' PO Overage Exceeds : ', true);
					throw cannotDelError;
				}

			}
			else
			{
				if(parseFloat(poimplineQty)>ItemRecommendedQuantity)
				{
					//error PO Overage
					nlapiLogExecution('ERROR', 'PO Overage Exceeds poOverage ', poOverage);
					var cannotDelError = nlapiCreateError('CannotDelete', ' PO Overage Exceeds : ', true);
					throw cannotDelError;
				}
			}	

			if(TrailerExists(poimpTrailNo)==false)
			{
				//To Create New Trailer
				nlapiLogExecution('ERROR', 'Creating New Trailer');
				CreateNewTrailer(poimpTrailNo,polocation,poComp);
			} 

		}
		else
		{
			//invalid PO#
			nlapiLogExecution('ERROR', 'Invalid PO# ', poimpid);
			var cannotDelError = nlapiCreateError('CannotDelete', ' Invalid PO# : ' + poimpid, true);
			throw cannotDelError;
		}  
	}
}

function checkPOOverage(POId,location,company){
	nlapiLogExecution('ERROR','into Check PO Overage ', POId);
	var poOverage = 0;

	var poFields = ['custbody_nswmsporeceipttype'];
	var poColumns = nlapiLookupField('transaction', POId, poFields);

	var receiptType = poColumns.custbody_nswmsporeceipttype;

	nlapiLogExecution('ERROR','here', receiptType);

	if (receiptType != "" && receiptType != null) 
	{
		var receiptFields = ['custrecord_po_overages'];
		var receiptColumns = nlapiLookupField('customrecord_ebiznet_receipt_type', receiptType, receiptFields);

		nlapiLogExecution('ERROR','here', receiptType);

		poOverage = receiptColumns.custrecord_po_overages;
	}
	nlapiLogExecution('ERROR','Out of check PO Overage', poOverage);
	return poOverage;
}
function TrailerExists(trailer)
{

	nlapiLogExecution('ERROR','into Trailer Exists ', trailer);
	var filterTrl = new Array();
	filterTrl.push(new nlobjSearchFilter('custrecord_ebizappointmenttrailer',null,'is',trailer));  
	var poTrailerSearch= nlapiSearchRecord('customrecord_ebiznet_trailer',null,filterTrl,null);
	if(poTrailerSearch!= null && poTrailerSearch != "")
	{
		nlapiLogExecution('ERROR','Out of Trailer Exists', true);
		return true;		
	}
	else
	{
		nlapiLogExecution('ERROR','Out of Trailer Exists', false);
		return false;
	}

}
function CreateNewTrailer(poimpTrailNo,location,company)
{
	//Create New Trailer
	var customRecord = nlapiCreateRecord('customrecord_ebiznet_trailer'); 
	nlapiLogExecution('ERROR', 'Create dummy Trailer :customrecord_ebiznet_trailer');

	customRecord.setFieldValue('name', poimpTrailNo);
	customRecord.setFieldValue('custrecord_ebizappointmenttrailer', poimpTrailNo);
	customRecord.setFieldValue('custrecord_ebizsitetrailer', location);
	//company='1';//for testing purpose
	if(company != "")
		customRecord.setFieldValue('custrecord_ebizcompanytrailer', company);
	customRecord.setFieldValue('custrecord_ebizmastershipper', poimpTrailNo); 
	var recordId = nlapiSubmitRecord(customRecord);
	nlapiLogExecution('ERROR', 'Out of New Trailer ',recordId);
}