/***************************************************************************
  		   eBizNET Solutions Ltd               
 ***************************************************************************
 **     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Outbound/Suitelet/ebiz_RF_picking_footprint.js,v $
 *     	   $Revision: 1.1.2.11.4.4.4.6 $
 *     	   $Date: 2014/06/13 12:45:45 $
 *     	   $Author: skavuri $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_picking_footprint.js,v $
 * Revision 1.1.2.11.4.4.4.6  2014/06/13 12:45:45  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.11.4.4.4.5  2014/05/30 00:41:03  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.11.4.4.4.4  2014/05/26 07:08:43  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20148320
 *
 * Revision 1.1.2.11.4.4.4.3  2013/11/22 08:40:38  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#201216680
 *
 * Revision 1.1.2.11.4.4.4.2  2013/06/11 14:30:19  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.1.2.11.4.4.4.1  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.11.4.4  2012/11/01 14:55:23  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.1.2.11.4.3  2012/10/07 23:06:03  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production Issue Fixes for FISK,BOOMBAH and TDG.
 *
 * Revision 1.1.2.11.4.2  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.1.2.11.4.1  2012/09/25 07:13:53  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Language
 *
 * Revision 1.1.2.11  2012/08/08 17:07:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Location override issue for Dynacraft
 *
 * Revision 1.1.2.10  2012/08/07 21:18:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.1.2.9  2012/08/07 12:06:40  spendyala
 * CASE201112/CR201113/LOG201121
 * Restricting user not to enter any character rather than numerics in the text box.
 *
 * Revision 1.1.2.8  2012/07/19 05:22:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Performance Tuning
 *
 * Revision 1.1.2.7  2012/07/09 06:50:56  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT Issue Fixes
 *
 * Revision 1.1.2.6  2012/04/18 14:27:31  mbpragada
 * CASE201112/CR201113/LOG201121
 * added Skip Location functionality
 *
 * Revision 1.1.2.5  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.1.2.4  2012/02/20 15:24:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.1.2.3  2012/02/08 15:27:30  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * functional keys
 *
 * Revision 1.1.2.2  2012/02/08 12:35:51  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge to brach
 *
 * Revision 1.1  2012/01/23 23:21:42  gkalla
 * CASE201112/CR201113/LOG201121
 * RF Picking customized
 * 
 *
 * 
 *****************************************************************************/
function PickingFootPrint(request, response){
	if (request.getMethod() == 'GET') {
		var getWaveno = request.getParameter('custparam_waveno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocationId = request.getParameter('custparam_beginLocation');
		//        var getItem = request.getParameter('custparam_item');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemName = request.getParameter('custparam_itemname');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');        
		var getEnteredLocation = request.getParameter('custparam_endlocation');
		var getBeginBinLocation = request.getParameter('custparam_beginlocationname');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var vclusterNo = request.getParameter('custparam_clusterno');			
		var vBatchno = request.getParameter('custparam_batchno');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var name=request.getParameter('name');
		var ContainerSize=request.getParameter('custparam_containersize');
		var getOrderNo=request.getParameter('custparam_ebizordno');
		var pickType=request.getParameter('custparam_picktype');
		//var vSOId=request.getParameter('custparam_ebizordno');
		var getContainerLpNo = request.getParameter('custparam_newcontainerlp');
		nlapiLogExecution('DEBUG', 'getContainerLpNo ', getContainerLpNo);
		nlapiLogExecution('DEBUG', 'getEbizSO', getOrderNo);
		nlapiLogExecution('DEBUG', 'getBeginLocation', NextLocation);
		nlapiLogExecution('DEBUG', 'NextItemInternalId', NextItemInternalId);
		nlapiLogExecution('DEBUG', 'FO InternalId', getDOLineId);

		if(getOrderNo==null || getOrderNo=='')
		{
			if(getDOLineId!=null && getDOLineId!='')
			{
				var FOFields = ['name'];
				var FOColumns = nlapiLookupField('customrecord_ebiznet_ordline', getDOLineId, FOFields);
				if(FOColumns != null && FOColumns != '')
				{
					getOrderNo=FOColumns.name;
				}
			}
		}

		var getItem = '';
		var Itemdescription='';

		if(getItemName==null || getItemName=='')
		{
			//removed as on 051113 by suman to increase the performance.
			/*var Itype = nlapiLookupField('item', getItemInternalId, 'recordType');
			var ItemRec = nlapiLoadRecord(Itype, getItemInternalId);
			var getItem = ItemRec.getFieldValue('itemid');
			var Itemdescription='';
			nlapiLogExecution('ERROR', 'Location Name is', getItem);
			if(ItemRec.getFieldValue('description') != null && ItemRec.getFieldValue('description') != "")
			{	
				Itemdescription = ItemRec.getFieldValue('description');
			}*/
			var filter=new Array();
			if(getItemInternalId!=null&&getItemInternalId!="")
			filter[0]=new nlobjSearchFilter("internalid",null,"anyof",getItemInternalId);

			var column=new Array();
			column[0]=new nlobjSearchColumn("itemid");
			column[1]=new nlobjSearchColumn("description");
			var searchres=nlapiSearchRecord("item",null,filter,column);
			if(searchres!=null&&searchres!="")
			{
				getItem=searchres[0].getValue("itemid");
				Itemdescription=searchres[0].getValue("description");
			}
			//end of code as on 051113			
		}
		else
		{
			getItem=getItemName;
		}

		nlapiLogExecution('DEBUG', 'getItem', getItem);
		nlapiLogExecution('DEBUG', 'getEbizSO', getOrderNo);

		var vSOName;

		if(getOrderNo!=null && getOrderNo!='')
		{
			var SOFields = ['tranid'];
			var SOColumns = nlapiLookupField('salesorder', getOrderNo, SOFields);

			if(SOColumns != null && SOColumns != '')
			{
				vSOName=SOColumns.tranid;
			}	
		}
		nlapiLogExecution('DEBUG', 'vSOName', vSOName);
		nlapiLogExecution('DEBUG', 'getItemdescription', Itemdescription);
		var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');
		
		var getLanguage = request.getParameter('custparam_language');	
	    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
	    
		var st0,st1,st2,st3,st4;
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "VENTAS DE PEDIDO #";
			st2 = "ENTER / SCAN HUELLA:";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";
			
    	}
		else
		{
			st0 = "";
			st1 = "SALES ORDER#";
			st2 = "ENTER/SCAN FOOT PRINT:";
			st3 = "SEND";
			st4 = "PREV";
			
			
		}    	
     
	
		
		var html = "<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		//html = html + " document.getElementById('enterfootprint').focus();";        
		html = html + "</script>";
		html = html + "<SCRIPT LANGUAGE='javascript'>";
		html = html + "function validate(frm){";
  
		html = html + "var val=document.getElementById('enterfootprint').value;";
		html = html + "var vparam='0123456789';";
		 
		html = html + "for (var i=0; i<val.length; i++) {";
		 
		html = html + "if (vparam.indexOf(val.charAt(i),0) == -1){"; 
		html = html + "alert('Should contains only numbers');return false;";
		//html = html + "document.getElementById('enterfootprint').focus();";
		html = html + "}";
		html = html + "}";
		html = html + "return true;";
		html = html + "}";
		
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_cluster_no' method='POST' onSubmit='return validate(this)'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st1 + " : <la bel>" + vSOName + "</label>";
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemName' value=" + getItemName + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEnteredLocation' value=" + getEnteredLocation + ">";
		html = html + "				<input type='hidden' name='hdnBeginBinLocation' value=" + getBeginBinLocation + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vclusterNo + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value=" + vBatchno + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnNextLocation' value=" + NextLocation + ">";
		html = html + "				<input type='hidden' name='hdnNextItemId' value=" + NextItemInternalId + ">";		
		html = html + "				<input type='hidden' name='hdnName' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + ContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnebizOrdNo' value=" + getOrderNo + ">";
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		/*html = html + "			<tr>";
		html = html + "				<td align = 'left'>LOCATION : <label>" + getEnteredLocation + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>ITEM : <label>" + getItem + "</label><br>ITEM DESCRIPTION: <label>" + Itemdescription + "</label>";
		html = html + "				</td>";
		html = html + "			</tr>";*/
		html = html + "			<tr>";
		html = html + "				<td align = 'left'> " + st2;
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterfootprint' id='enterfootprint' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					" + st4 + " <input name='cmdPrevious' type='submit' value='F7'/>"; 
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterfootprint').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		nlapiLogExecution('DEBUG', 'Pick Type',  request.getParameter('hdnpicktype'));

		// This variable is to hold the SO# entered.
		var SOarray = new Array();
		  var getLanguage = request.getParameter('hdngetLanguage');
			SOarray["custparam_language"] = getLanguage;
			nlapiLogExecution('DEBUG', 'getLanguage', SOarray["custparam_language"]);    	
			
			var st9,st10;
			if( getLanguage == 'es_ES')
			{			
				st9 = "FOOTPRINT V&#193;LIDA #";
				st10 = "FOOTPRINT";
			}
			else
			{			
				st9 = "INVALID FOOTPRINT #";
				st10 = "FOOTPRINT";
			}
		SOarray["custparam_error"] = st9;
		SOarray["custparam_screenno"] = st10;
		SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
		SOarray["custparam_recordinternalid"] = request.getParameter('hdnRecordInternalId');
		SOarray["custparam_containerlpno"] = request.getParameter('hdnContainerLpNo');
		SOarray["custparam_expectedquantity"] = request.getParameter('hdnExpectedQuantity');
		SOarray["custparam_beginLocation"] = request.getParameter('hdnBeginLocationId');
		SOarray["custparam_item"] = request.getParameter('hdnItem');
		SOarray["custparam_itemname"] = request.getParameter('hdnItemName');
		SOarray["custparam_itemdescription"] = request.getParameter('hdnItemDescription');
		SOarray["custparam_iteminternalid"] = request.getParameter('hdnItemInternalId');
		SOarray["custparam_dolineid"] = request.getParameter('hdnDOLineId');
		SOarray["custparam_invoicerefno"] = request.getParameter('hdnInvoiceRefNo');
		SOarray["custparam_beginlocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
		SOarray["custparam_orderlineno"] = request.getParameter('hdnOrderLineNo');
		SOarray["custparam_clusterno"] = request.getParameter('hdnClusterNo');
		SOarray["custparam_batchno"] = request.getParameter('hdnbatchno');
		SOarray["custparam_noofrecords"] = request.getParameter('hdnRecCount');;
		SOarray["custparam_nextlocation"] = request.getParameter('hdnNextLocation');
		SOarray["custparam_nextiteminternalid"] = request.getParameter('hdnNextItemId');		
		SOarray["name"] = request.getParameter('hdnName');
		SOarray["custparam_containersize"] = request.getParameter('hdnContainerSize');
		SOarray["custparam_ebizordno"] = request.getParameter('hdnebizOrdNo');
		SOarray["custparam_newcontainerlp"] = request.getParameter('hdnContainerLpNo');
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		var vSkipId=request.getParameter('hdnskipid');
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');
		var vClusterNo = request.getParameter('hdnClusterNo');
		var vFootprint= request.getParameter('enterfootprint');
		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		if (request.getParameter('cmdPrevious') == 'F7') {
			nlapiLogExecution('DEBUG', 'Clicked on Previous', request.getParameter('cmdPrevious'));
			response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);
		}
		else 
			if(vFootprint!=null&&vFootprint!="")
			{
				nlapiLogExecution('DEBUG', 'Confirm Pick');
				var WaveNo = request.getParameter('hdnWaveNo');
				var RecordInternalId = request.getParameter('hdnRecordInternalId');
				var ContainerLPNo = request.getParameter('hdnContainerLpNo');
				var ExpectedQuantity = request.getParameter('hdnExpectedQuantity');
				var BeginLocation = request.getParameter('hdnBeginLocationId');
				var Item = request.getParameter('hdnItem');
				var ItemName = request.getParameter('hdnItemName');
				var ItemDescription = request.getParameter('hdnItemDescription');
				var ItemInternalId = request.getParameter('hdnItemInternalId');
				var DoLineId = request.getParameter('hdnDOLineId'); // internal id of DO
				var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
				var BeginLocationName = request.getParameter('hdnBeginBinLocation');
				var EndLocInternalId = request.getParameter('hdnEndLocInternalId');
				var EndLocation = request.getParameter('hdnEnteredLocation');
				var OrderLineNo = request.getParameter('hdnOrderLineNo');
				var NextShowLocation=request.getParameter('hdnNextLocation');
				var OrdName=request.getParameter('hdnName');
				var ContainerSize=request.getParameter('hdnContainerSize');
				var NextItemInternalId=request.getParameter('hdnNextItemId');
				var ebizOrdNo=request.getParameter('hdnebizOrdNo');
				var vPickType=request.getParameter('hdnpicktype');
				nlapiLogExecution('DEBUG', 'OrdName', OrdName);
				nlapiLogExecution('DEBUG', 'Next Location', NextShowLocation);
				nlapiLogExecution('DEBUG', 'BeginLocation', BeginLocation);
				nlapiLogExecution('DEBUG', 'vPickType', vPickType);

				nlapiLogExecution('DEBUG', 'Before Foot print');
				//ItemFulfillment(WaveNo, RecordInternalId, ContainerLPNo, ExpectedQuantity, BeginLocation, Item, ItemDescription, ItemInternalId, DoLineId, InvoiceRefNo, BeginLocationName, EndLocInternalId, EndLocation, OrderLineNo)


				var RcId=RecordInternalId;
				var EndLocation = EndLocInternalId;
				var TotalWeight=0;
				var getReason=request.getParameter('custparam_enteredReason');
				var SORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordInternalId);
				var getContainerLPNo= SORec.getFieldValue('custrecord_container_lp_no');
				getLPContainerSize= SORec.getFieldText('custrecord_container');
				var vBatchno = request.getParameter('hdnbatchno');
				var PickQty = ExpectedQuantity;
				var vActqty = ExpectedQuantity;
				var SalesOrderInternalId = ebizOrdNo;
				var getContainerSize = getLPContainerSize;
				var  vdono = DoLineId;
				nlapiLogExecution('DEBUG', 'WaveNo', WaveNo);
				nlapiLogExecution('DEBUG', 'getContainerLPNo', getContainerLPNo); 
				nlapiLogExecution('DEBUG', 'RcId', RcId); 
				UpdateRFOpenTask(RcId,vFootprint);

				if(vPickType != null && vPickType != '')
				{

					nlapiLogExecution('DEBUG', 'Inside Both Wave No condition', parseFloat(WaveNo));
					nlapiLogExecution('DEBUG', 'Inside Both Order No condition', DoLineId);

					var SOFilters = new Array();


					SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated					
					SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK

					if(vPickType =='W')
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
					else if(vPickType =='O')
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', parseFloat(DoLineId)));
					else if(vPickType =='S')
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', parseFloat(SalesOrderInternalId)));
					else if(vPickType =='OW')
					{	
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', parseFloat(DoLineId)));
					}
					else if(vPickType =='OS')
					{	
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', parseFloat(SalesOrderInternalId)));
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', parseFloat(DoLineId)));
					}
					else if(vPickType =='SW')
					{	
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', parseFloat(SalesOrderInternalId)));
					}
					else if(vPickType =='ALL')
					{	
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', parseFloat(SalesOrderInternalId)));
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'is', parseFloat(DoLineId)));
					} 

					var SOColumns = new Array();
//					SOColumns[0] = new nlobjSearchColumn('custrecord_skiptask');	
					//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
					SOColumns[0]=new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)");
					//Code end as on 290414
					SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
					SOColumns[2] = new nlobjSearchColumn('custrecord_sku');
					SOColumns[3] = new nlobjSearchColumn('custrecord_expe_qty');
					SOColumns[4] = new nlobjSearchColumn('custrecord_actbeginloc');		
					SOColumns[5] = new nlobjSearchColumn('custrecord_skudesc');
					SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
					SOColumns[7] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
					SOColumns[8] = new nlobjSearchColumn('custrecord_invref_no');
					SOColumns[9] = new nlobjSearchColumn('custrecord_line_no');
					SOColumns[10] = new nlobjSearchColumn('custrecord_actbeginloc');
					SOColumns[11] = new nlobjSearchColumn('custrecord_batch_no');
					SOColumns[12] = new nlobjSearchColumn('custrecord_wms_location');
					SOColumns[13] = new nlobjSearchColumn('custrecord_comp_id');
					SOColumns[14] = new nlobjSearchColumn('name');
					SOColumns[15] = new nlobjSearchColumn('custrecord_container');
					SOColumns[16] = new nlobjSearchColumn('custrecord_ebiz_order_no');
					SOColumns[17] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
					SOColumns[18] = new nlobjSearchColumn('custrecord_lpno');

					SOColumns[0].setSort();
					SOColumns[1].setSort(false);
					SOColumns[2].setSort(false);
					SOColumns[3].setSort(true);

					var SOarray = new Array();
					var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
					nlapiLogExecution('DEBUG', 'SOSearchResults', SOSearchResults);
					if (SOSearchResults != null && SOSearchResults.length > 0) {
						if(SOSearchResults.length <= parseFloat(vSkipId))
						{
							vSkipId=0;
						}
						nlapiLogExecution('DEBUG', 'into Search Results by Wave No', WaveNo);
						//nlapiLogExecution('DEBUG', 'SearchResults of given Wave', getWaveNo);
						var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];
						getWaveNo = SOSearchResult.getValue('custrecord_ebiz_wave_no');

						nlapiLogExecution('DEBUG', 'LP# of given Wave', SOSearchResult.getValue('custrecord_lpno'));
						nlapiLogExecution('DEBUG', 'Exp.Qty of given Wave', SOSearchResult.getValue('custrecord_expe_qty'));
						nlapiLogExecution('DEBUG', 'Wave # of given Wave', getWaveNo);

						SOarray["custparam_waveno"] = getWaveNo;
						SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
						SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
						SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
						SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
						SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
						SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
						SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
						SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
						SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
						SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
						SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
						SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
						SOarray["custparam_clusterno"] = vClusterNo;
						SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
						response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);
						nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');

					}
					else {
						nlapiLogExecution('DEBUG', 'Search Results by Wave No is NULL', WaveNo);
						response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, null);
					}


				}	
				else
				{	
					if (vClusterNo != null && vClusterNo != "") {

						nlapiLogExecution('DEBUG', 'Inside Cluster No condition', vClusterNo);

						var SOFilters = new Array();

						SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', parseFloat(vClusterNo)));
						SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK

						var SOColumns = new Array();

//						SOColumns[0] = new nlobjSearchColumn('custrecord_skiptask');
						//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
						SOColumns[0]=new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)");
						//Code end as on 290414
						SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
						SOColumns[2] = new nlobjSearchColumn('custrecord_sku');
						SOColumns[3] = new nlobjSearchColumn('custrecord_expe_qty');
						SOColumns[4] = new nlobjSearchColumn('custrecord_actbeginloc');		
						SOColumns[5] = new nlobjSearchColumn('custrecord_skudesc');
						SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
						SOColumns[7] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
						SOColumns[8] = new nlobjSearchColumn('custrecord_invref_no');
						SOColumns[9] = new nlobjSearchColumn('custrecord_line_no');
						SOColumns[10] = new nlobjSearchColumn('custrecord_actbeginloc');
						SOColumns[11] = new nlobjSearchColumn('custrecord_batch_no');
						SOColumns[12] = new nlobjSearchColumn('custrecord_wms_location');
						SOColumns[13] = new nlobjSearchColumn('custrecord_comp_id');
						SOColumns[14] = new nlobjSearchColumn('name');
						SOColumns[15] = new nlobjSearchColumn('custrecord_container');
						SOColumns[16] = new nlobjSearchColumn('custrecord_ebiz_order_no');
						SOColumns[17] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
						SOColumns[18] = new nlobjSearchColumn('custrecord_lpno');

						SOColumns[0].setSort();		
						SOColumns[1].setSort(false);
						SOColumns[2].setSort(false);
						SOColumns[3].setSort(true);

						var SOarray = new Array();
						var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

						if (SOSearchResults != null && SOSearchResults.length > 0) {
							if(SOSearchResults.length <= parseFloat(vSkipId))
							{
								vSkipId=0;
							}
							nlapiLogExecution('DEBUG', 'into Search Results by Cluster No', vClusterNo);
							//nlapiLogExecution('DEBUG', 'SearchResults of given Wave', getWaveNo);
							var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];
							getWaveNo = SOSearchResult.getValue('custrecord_ebiz_wave_no');
							// SOarray["custparam_waveno"] = getWaveNo;
							nlapiLogExecution('DEBUG', 'LP# of given Wave', SOSearchResult.getValue('custrecord_lpno'));
							nlapiLogExecution('DEBUG', 'Exp.Qty of given Wave', SOSearchResult.getValue('custrecord_expe_qty'));
							nlapiLogExecution('DEBUG', 'Wave # of given Wave', getWaveNo);
							SOarray["custparam_waveno"] = getWaveNo;
							SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
							SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
							SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
							SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
							SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
							SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
							SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
							SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
							SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
							SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
							SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
							SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
							SOarray["custparam_clusterno"] = vClusterNo;
							SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
							response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);
							nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');

						}
						else {
							response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, null);
						}
					}
					else if (WaveNo != null && WaveNo != "") {
						nlapiLogExecution('DEBUG', 'Inside Wave No condition', parseFloat(WaveNo));

						var SOFilters = new Array();

						SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9])); //Status Flag - Picks Generated
						SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
						SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK

						var SOColumns = new Array();
//						SOColumns[0] = new nlobjSearchColumn('custrecord_skiptask');		
						//Skip Id is of type Freeform Text when we sort this system showing incorrect records.
						SOColumns[0]=new nlobjSearchColumn('formulanumeric').setFormula("nvl(TO_NUMBER({custrecord_skiptask}),0)");
						//Code end as on 290414
						SOColumns[1] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
						SOColumns[2] = new nlobjSearchColumn('custrecord_sku');
						SOColumns[3] = new nlobjSearchColumn('custrecord_expe_qty');
						SOColumns[4] = new nlobjSearchColumn('custrecord_actbeginloc');		
						SOColumns[5] = new nlobjSearchColumn('custrecord_skudesc');
						SOColumns[6] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
						SOColumns[7] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
						SOColumns[8] = new nlobjSearchColumn('custrecord_invref_no');
						SOColumns[9] = new nlobjSearchColumn('custrecord_line_no');
						SOColumns[10] = new nlobjSearchColumn('custrecord_actbeginloc');
						SOColumns[11] = new nlobjSearchColumn('custrecord_batch_no');
						SOColumns[12] = new nlobjSearchColumn('custrecord_wms_location');
						SOColumns[13] = new nlobjSearchColumn('custrecord_comp_id');
						SOColumns[14] = new nlobjSearchColumn('name');
						SOColumns[15] = new nlobjSearchColumn('custrecord_container');
						SOColumns[16] = new nlobjSearchColumn('custrecord_ebiz_order_no');
						SOColumns[17] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
						SOColumns[18] = new nlobjSearchColumn('custrecord_lpno');

						SOColumns[0].setSort();
						SOColumns[1].setSort(false);
						SOColumns[2].setSort(false);
						SOColumns[3].setSort(true);

						var SOarray = new Array();
						var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
						nlapiLogExecution('DEBUG', 'SOSearchResults', SOSearchResults);
						if (SOSearchResults != null && SOSearchResults.length > 0) {
							if(SOSearchResults.length <= parseFloat(vSkipId))
							{
								vSkipId=0;
							}
							nlapiLogExecution('DEBUG', 'into Search Results by Wave No', WaveNo);
							//nlapiLogExecution('DEBUG', 'SearchResults of given Wave', getWaveNo);
							var SOSearchResult = SOSearchResults[parseFloat(vSkipId)];
							getWaveNo = SOSearchResult.getValue('custrecord_ebiz_wave_no');

							nlapiLogExecution('DEBUG', 'LP# of given Wave', SOSearchResult.getValue('custrecord_lpno'));
							nlapiLogExecution('DEBUG', 'Exp.Qty of given Wave', SOSearchResult.getValue('custrecord_expe_qty'));
							nlapiLogExecution('DEBUG', 'Wave # of given Wave', getWaveNo);

							SOarray["custparam_waveno"] = getWaveNo;
							SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
							SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
							SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
							SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
							SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
							SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
							SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
							SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
							SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
							SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
							SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
							SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
							SOarray["custparam_clusterno"] = vClusterNo;
							SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
							response.sendRedirect('SUITELET', 'customscript_rf_picking_location', 'customdeploy_rf_picking_location_di', false, SOarray);
							nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');

						}
						else {
							nlapiLogExecution('DEBUG', 'Search Results by Wave No is NULL', WaveNo);
							response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, null);
						}
					}
					else
					{
						response.sendRedirect('SUITELET', 'customscript_rf_picking_container', 'customdeploy_rf_picking_container_di', false, null);
					}
				}


				/*nlapiLogExecution('DEBUG', 'Before AutoPacking 1 : Sales Order InternalId', SalesOrderInternalId);			 
			nlapiLogExecution('DEBUG', 'Navigating To1', 'StageLocation'); 			 
			response.sendRedirect('SUITELET', 'customscript_rf_picking_stage_loc', 'customdeploy_rf_picking_stage_loc_di', false, SOarray); 
			nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');*/

			}
			else //if (getEnteredContainerNo != '' && getEnteredContainerNo == getFetchedContainerNo) 
			{
				nlapiLogExecution('DEBUG', 'Error: ', 'Foor print not found');
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);				
			}
	}
}


function UpdateRFOpenTask(RcId,vFootPrint)
{ 
	var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RcId);

	if(vFootPrint!=null && vFootPrint!="")
		transaction.setFieldValue('custrecord_ebizfootprint', vFootPrint); 
	nlapiLogExecution('DEBUG', 'vFootPrint ', vFootPrint);
	nlapiSubmitRecord(transaction, false, true);
	nlapiLogExecution('DEBUG', 'Updated foot print RF Open Task successfully');
}

