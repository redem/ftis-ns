/***************************************************************************
  		   eBizNET Solutions Inc               
 ***************************************************************************
 **     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Inbound/Suitelet/ebiz_RF_PutawayMergeLP.js,v $
 *     	   $Revision: 1.2.4.2.4.2.4.3 $
 *     	   $Date: 2014/05/30 00:26:50 $
 *     	   $Author: nneelam $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_PutawayMergeLP.js,v $
 * Revision 1.2.4.2.4.2.4.3  2014/05/30 00:26:50  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.2.4.2.4.2.4.2  2013/06/11 14:30:41  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.2.4.2.4.2.4.1  2013/04/17 16:04:01  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.2.4.2.4.2  2012/09/26 12:48:01  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.2.4.2.4.1  2012/09/24 10:07:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.2.4.2  2012/03/16 13:56:23  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.2.4.1  2012/02/22 12:39:53  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.4  2012/02/17 13:38:43  schepuri
 * CASE201112/CR201113/LOG201121
 * Added CVS Header
 *
 * Revision 1.3  2012/02/17 13:32:31  schepuri
 * CASE201112/CR201113/LOG201121
 * Added FunctionkeyScript
 * 
 */
function PutwayMergeLp(request,response){
	if(request.getMethod()=='GET'){

		var getRecordCount = request.getParameter('custparam_recordcount');
		nlapiLogExecution('DEBUG', 'getFetchedLocationId ', getFetchedLocation);
		var getLPNo = request.getParameter('custparam_lpno');
		var getQuantity = request.getParameter('custparam_quantity');
		var getFetchedLocation = request.getParameter('custparam_beginlocation');
		var getFetchedItem = request.getParameter('custparam_item');
		var getFetchedItemDescription = request.getParameter('custparam_itemDescription');
		var getFetchedLocationId = request.getParameter('custparam_location');
		var getPONo = request.getParameter('custparam_pono');
		var getLineNo = request.getParameter('custparam_lineno');
		var getRecordId = request.getParameter('custparam_recordid');
		var getCartLPNo = request.getParameter('custparam_cartno');
		nlapiLogExecution('DEBUG', 'getCartLPNo ', getCartLPNo);
		nlapiLogExecution('DEBUG', 'getFetchedItem ', getFetchedItem);
		var getOptedField = request.getParameter('custparam_option');
		var getInventoryLocation=request.getParameter('custparam_locationLP');
		var getLpno1=request.getParameter('custparam_lpNo1');
		var functionkeyHtml=getFunctionkeyScript('_rf_putaway_mergelp'); 
		var getLanguage = request.getParameter('custparam_language');
	    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
		
		
		var st0,st1;
		
		if( getLanguage == 'es_ES')
		{
			st0 = "";
			st1 = "&#191;ES USTED DESEE COMBINAR EN...";	
						
		}
		else
		{
			st0 = "";
			st1 = "DO YOU WANT TO MERGE ON"; 
					
		}
		
		var html="<html><head><title>" + st0 + "</title>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html+="<form name='_rf_putaway_mergelp' method='POST'>";
		html+="<b></b>" + st1 + "</br>";
		html+="LP# : <label>" + getLpno1 + "</label></br>";
		html+="<input name='cmdyes' type='submit' value='Yes'/>";
		html+="<input name='cmdno' type='submit' value='No'/>";
		html = html + "	<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
		response.write(html);
	}
	else{

		var optedEvent = request.getParameter('cmdno');
		if(optedEvent=='No')
		{
			response.sendRedirect('SUITELET', 'customscript_rf_putaway_complete', 'customdeploy_rf_putaway_complete_di', false, optedEvent);
		}
		else
		{


		}

	}

}