/***************************************************************************
 eBizNET Solutions
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Suitelet/ebiz_ReplenConfirm_SL.js,v $
<<<<<<< ebiz_ReplenConfirm_SL.js
 *     	   $Revision: 1.11.2.6.4.6.2.24.2.2 $
 *     	   $Date: 2015/09/22 16:27:55 $
 *     	   $Author: rrpulicherla $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
=======
 *     	   $Revision: 1.11.2.6.4.6.2.24.2.2 $
 *     	   $Date: 2015/09/22 16:27:55 $
 *     	   $Author: rrpulicherla $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
>>>>>>> 1.11.2.6.4.6.2.12
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_ReplenConfirm_SL.js,v $
 * Revision 1.11.2.6.4.6.2.24.2.2  2015/09/22 16:27:55  rrpulicherla
 * no message
 *
 * Revision 1.11.2.6.4.6.2.24.2.1  2015/09/21 14:02:22  deepshikha
 * 2015.2 issueFix
 * 201414466
 *
 * Revision 1.11.2.6.4.6.2.24  2015/08/24 14:33:05  schepuri
 * case# 201413937
 *
 * Revision 1.11.2.6.4.6.2.23  2015/08/05 15:13:41  grao
 * 2015.2   issue fixes  201413052
 *
 * Revision 1.11.2.6.4.6.2.22  2015/07/13 13:23:42  schepuri
 * case# 201413376
 *
 * Revision 1.11.2.6.4.6.2.21  2015/06/22 15:44:46  grao
 * SW issue fixes  201412905
 *
 * Revision 1.11.2.6.4.6.2.20  2015/05/22 13:24:16  schepuri
 * case# 201412870
 *
 * Revision 1.11.2.6.4.6.2.19  2015/05/20 15:47:16  nneelam
 * case# 201412662
 *
 * Revision 1.11.2.6.4.6.2.18  2014/11/14 13:00:15  skavuri
 * Srikanth Added for LL_Production _Purpose(RR_Reference)
 *
 * Revision 1.11.2.6.4.6.2.17  2014/08/04 15:55:55  skavuri
 * Case# 20148543 SB Issue Fixed
 *
 * Revision 1.11.2.6.4.6.2.16  2014/07/31 15:48:12  skavuri
 * Case# 20148643 SB Issue Fixed
 *
 * Revision 1.11.2.6.4.6.2.15  2014/07/11 15:54:29  sponnaganti
 * Case# 20149400
 * Compatibility Issue fix
 *
 * Revision 1.11.2.6.4.6.2.14  2014/06/20 14:56:22  rmukkera
 * Case # 20148739
 *
 * Revision 1.11.2.6.4.6.2.13  2014/06/18 13:57:34  grao
 * Case#: 20148955  New GUI account issue fixes
 *
 * Revision 1.11.2.6.4.6.2.12  2014/03/13 09:53:37  nneelam
 * case#  20127533
 * Standard Bundle Issue Fix.
 *
 * Revision 1.11.2.6.4.6.2.11  2014/03/10 16:28:13  sponnaganti
 * case# 20127533
 * (Pickface location remCube is updating with negative values issue)
 *
 * Revision 1.11.2.6.4.6.2.10  2014/03/04 12:34:52  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20127481
 *
 * Revision 1.11.2.6.4.6.2.9  2014/02/25 23:45:07  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#20127243
 *
 * Revision 1.11.2.6.4.6.2.8  2013/10/22 16:10:17  gkalla
 * Case# 20125248
 * Dynacraft Replen Confirm issue
 *
 * Revision 1.11.2.6.4.6.2.7  2013/09/26 15:48:56  skreddy
 * Case# 20124511
 * issue rellated to confirm replenishment
 *
 * Revision 1.11.2.6.4.6.2.6  2013/08/05 06:40:51  schepuri
 * Case# 20123680,20123415
 * ryonet sb rplen confirm issue fix
 *
 * Revision 1.11.2.6.4.6.2.5  2013/07/24 15:18:11  rrpulicherla
 * case# 20123588
 * GFT Issue fixes
 *
 * Revision 1.11.2.6.4.6.2.4  2013/03/15 09:28:45  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.11.2.6.4.6.2.3  2013/03/12 15:35:11  skreddy
 * CASE201112/CR201113/LOG201121
 * Qty Exception in  GUI Replensiment Process
 *
 * Revision 1.11.2.6.4.6.2.2  2013/03/01 14:34:54  skreddy
 * CASE201112/CR201113/LOG201121
 * Merged from FactoryMation and change the Company name
 *
 * Revision 1.11.2.6.4.6.2.1  2013/02/26 12:52:09  rmukkera
 * code merged from boombah production bundle on 26th feb 2013
 * t_eBN_2013_1_StdBundle_1
 *
 * Revision 1.11.2.6.4.6  2013/02/06 01:07:00  kavitha
 * CASE201112/CR201113/LOG201121
 * Serial # functionality - Inventory process
 *
 * Revision 1.11.2.6.4.5  2013/01/02 15:43:22  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Inventory Sync issue fixes
 * .
 *
 * Revision 1.11.2.6.4.4  2012/12/21 09:29:26  mbpragada
 * 20120490
 *
 * Revision 1.11.2.6.4.3  2012/12/11 03:52:01  kavitha
 * CASE201112/CR201113/LOG201121
 * Incorporated Serial # functionality
 *
 * Revision 1.11.2.6.4.2  2012/11/11 03:40:30  snimmakayala
 * CASE201112/CR201113/LOG201121
 * GSUSA UAT issue fixes.
 *
 * Revision 1.11.2.6.4.1  2012/11/01 14:54:57  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.11.2.6  2012/08/24 16:12:39  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Replen confirm
 *
 * Revision 1.11.2.5  2012/08/23 15:43:57  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * Replen confirm
 *
 * Revision 1.11.2.4  2012/08/09 07:49:36  schepuri
 * CASE201112/CR201113/LOG201121
 * default sku status
 *
 * Revision 1.11.2.3  2012/04/06 14:03:27  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Performance Tunning
 *
 * Revision 1.11.2.2  2012/03/22 08:11:10  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 *  Replenishment
 *
 * Revision 1.11.2.1  2012/02/07 12:34:06  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Replenishment issue fixes
 *
 * Revision 1.11  2012/01/05 17:24:36  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.10  2011/07/29 11:04:51  pattili
 * CASE201112/CR201113/LOG201121
 * Corrected few minor issues
 *
 * Revision 1.9  2011/07/21 04:42:55  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 * Revision 1.10  2011/07/20 12:57:48  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 * Revision 1.4  2011/07/20 11:30:04  schepuri
 * CASE201112/CR201113/LOG201121
 * Quantity Exception in RF
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/

/**
 * 
 * nlobjSearchColumn from customrecord_ebiznet_trn_opentask
 */
function getOpentaskReplenishmentColumns(){
	var opentaskReplenishmentColumns = new Array();

	opentaskReplenishmentColumns[0] = new nlobjSearchColumn('custrecord_tasktype');
	opentaskReplenishmentColumns[1] = new nlobjSearchColumn('custrecordact_begin_date');
	opentaskReplenishmentColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
	opentaskReplenishmentColumns[3] = new nlobjSearchColumn('custrecord_expe_qty');
	opentaskReplenishmentColumns[4] = new nlobjSearchColumn('custrecord_actendloc');
	opentaskReplenishmentColumns[5] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
	opentaskReplenishmentColumns[6] = new nlobjSearchColumn('custrecord_lpno');
	opentaskReplenishmentColumns[7] = new nlobjSearchColumn('name');
	opentaskReplenishmentColumns[8] = new nlobjSearchColumn('custrecord_ebiz_clus_no');
	opentaskReplenishmentColumns[9] = new nlobjSearchColumn('custrecord_sku');
	opentaskReplenishmentColumns[9] = new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc');
	opentaskReplenishmentColumns[10] = new nlobjSearchColumn('custrecord_sku');
	opentaskReplenishmentColumns[11] = new nlobjSearchColumn('custrecord_from_lp_no');
	opentaskReplenishmentColumns[12] = new nlobjSearchColumn('custrecord_batch_no');
	opentaskReplenishmentColumns[13] = new nlobjSearchColumn('custrecord_wms_location');

	opentaskReplenishmentColumns[9].setSort(false);
	opentaskReplenishmentColumns[10].setSort(false);

	return opentaskReplenishmentColumns;
}

/**
 * 
 * nlobjSearchFilter from customrecord_ebiznet_trn_opentask
 */
function getOpentaskReplenishmentFilters(request){
	var opentaskReplenishmentFilters = new Array();
	opentaskReplenishmentFilters[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'is', '8');
	opentaskReplenishmentFilters[1] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9,20]);
	opentaskReplenishmentFilters[2] = new nlobjSearchFilter('name', null, 'is', request.getParameter('custparam_reportid'));
	//case# 20149400 starts
	var RoleLocation=getRoledBasedLocationNew();
	if((RoleLocation !=null && RoleLocation !='' && RoleLocation !=0))
	{
		opentaskReplenishmentFilters[3] = new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', RoleLocation);
	}
	//case# 20149400 ends
	return opentaskReplenishmentFilters;
}

/**
 * 
 * two step Replenishment main function
 */
function confirmReplenSuitelet(request, response){
	if (request.getMethod() == 'GET') {
		//create the Suitelet form
		var form = nlapiCreateForm('Confirm Replenishment');
		form.setScript('customscript_confirmrplnclientvalidation');

		//Adding  ItemSubList
		var ItemSubList = form.addSubList("custpage_replen_items", "list", "ItemList");
		ItemSubList.addField("custpage_replen_sel", "checkbox", "Select").setDefaultValue('T');
		//ItemSubList.addField("custpage_replen_sel", "checkbox", "Select");
		ItemSubList.addField("custpage_replen_status", "select", "Task Type", "customrecord_ebiznet_tasktype").setDisplayType('inline');
		ItemSubList.addField("custpage_replen_begindate", "text", "Begin Date");
		ItemSubList.addField("custpage_replen_beginloc", "select", "Begin Location", "customrecord_ebiznet_location").setDisplayType('inline');// case# 201413937
		ItemSubList.addField("custpage_replen_endloc", "select", "End Location", "customrecord_ebiznet_location").setDisplayType('inline');
		ItemSubList.addField("custpage_replen_item", "select", "Item", "Item");
		ItemSubList.addField("custpage_replen_lp", "text", "LP").setDisplayType('entry');
		ItemSubList.addField("custpage_replen_rplnqty", "text", "Qty").setDisplayType('entry');
		ItemSubList.addField("custpage_replen_reportno", "text", "Report#").setDisplayType('entry');
		ItemSubList.addField("custpage_replen_cluster", "text", "Cluster#").setDisplayType('entry');
		ItemSubList.addField("custpage_replen_recid", "text", "recid").setDisplayType('hidden');
		ItemSubList.addField("custpage_replen_fromlp", "text", "From LP").setDisplayType('hidden');
		ItemSubList.addField("custpage_replen_expqty", "text", "Exp Qty").setDisplayType('hidden');
		ItemSubList.addField("custpage_replen_batchno", "text", "Batch#").setDisplayType('hidden');
		ItemSubList.addField("custpage_replen_itemtype", "text", "item type#").setDisplayType('hidden');
		ItemSubList.addField("custpage_replen_remainingcube", "text", "Rem Cube#").setDisplayType('hidden');
		ItemSubList.addField("custpage_replen_whlocation", "text", "whLocation").setDisplayType('hidden');
		ItemSubList.addField("custpage_replen_oldlp", "text", "oldlp").setDisplayType('hidden');
		ItemSubList.addMarkAllButtons();
		var vitem = "", strItem = "", strQty = 0, vcomponent, vlp, vlocation, vinvlocation, vqty = 0, vpfqty = 0, strMaxQty;
		var TaskType, BeginDate, BeginLoc, EndLoc, ExpQty, LP, Item, RecId,reportNo,clusterNo,Batchno,whLocation;
		var FromLP = "";
		var recCount = 0;

		var opentaskReplenishmentFilters = getOpentaskReplenishmentFilters(request);

		var opentaskReplenishmentColumns = getOpentaskReplenishmentColumns();

		var j = 0, k = 0;
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, opentaskReplenishmentFilters, opentaskReplenishmentColumns);
		if (searchresults != null) {

			for (var i = 0; i < searchresults.length; i++) {
				TaskType = searchresults[i].getValue('custrecord_tasktype');
				BeginDate = searchresults[i].getValue('custrecordact_begin_date');
				BeginLoc = searchresults[i].getValue('custrecord_actbeginloc');
				EndLoc = searchresults[i].getValue('custrecord_actendloc');
				ExpQty = searchresults[i].getValue('custrecord_expe_qty');
				LP = searchresults[i].getValue('custrecord_lpno');
				Item = searchresults[i].getValue('custrecord_ebiz_sku_no');
				reportNo= searchresults[i].getValue('name');
				clusterNo= searchresults[i].getValue('custrecord_ebiz_clus_no');
				FromLP = searchresults[i].getValue('custrecord_from_lp_no');
				Batchno=searchresults[i].getValue('custrecord_batch_no');
				whLocation=searchresults[i].getValue('custrecord_wms_location');
				//RecId = searchresults[j].getId();
				RecId = searchresults[i].getId();
				var varsku = searchresults[i].getValue('custrecord_sku');
				
				var ItemType ='';
				nlapiLogExecution('ERROR', 'varsku', varsku);
				var filter=new Array();
				filter[0]=new nlobjSearchFilter("internalid",null,"anyof",varsku);
				var column=new Array();
				column[0]=new nlobjSearchColumn("type");
				var searchres1=nlapiSearchRecord("item",null,filter,column);
				if(searchres1!=null&&searchres1!="")
				{
					ItemType=searchres1[0].recordType;
				}
				//code start to check for the remaining cube.
				
				var arrDims = getSKUCubeAndWeight(Item, 1);
				var itemCube = 0;
				var vTotalCubeValue = 0;
				//alert('itmid '+itmid);
				//alert('qty '+qty);
				//alert('location '+location);
				if((arrDims[0] != "") && (!isNaN(arrDims[0]))) 
				{
					var uomqty = ((parseFloat(ExpQty))/(parseFloat(arrDims[1])));			
					itemCube = ((parseFloat(uomqty)) * (parseFloat(arrDims[0])));
				}
				var vOldRemainingCube = GeteLocCube(EndLoc);
				vTotalCubeValue = parseFloat(vOldRemainingCube)-parseFloat(itemCube);
				
				
				
				//code end to check for remaining cube.
				nlapiLogExecution('ERROR', 'ItemType123', ItemType);
				nlapiLogExecution("ERROR", "Batchno ", Batchno);
				form.getSubList('custpage_replen_items').setLineItemValue('custpage_replen_status', i + 1, TaskType);
				form.getSubList('custpage_replen_items').setLineItemValue('custpage_replen_begindate', i + 1, BeginDate);
				form.getSubList('custpage_replen_items').setLineItemValue('custpage_replen_beginloc', i + 1, BeginLoc);
				form.getSubList('custpage_replen_items').setLineItemValue('custpage_replen_endloc', i + 1, EndLoc);
				form.getSubList('custpage_replen_items').setLineItemValue('custpage_replen_rplnqty', i + 1, ExpQty);
				form.getSubList('custpage_replen_items').setLineItemValue('custpage_replen_lp', i + 1, LP);
				form.getSubList('custpage_replen_items').setLineItemValue('custpage_replen_oldlp', i + 1, LP);
	
				form.getSubList('custpage_replen_items').setLineItemValue('custpage_replen_item', i + 1, Item);
				form.getSubList('custpage_replen_items').setLineItemValue('custpage_replen_reportno', i + 1, reportNo);
				form.getSubList('custpage_replen_items').setLineItemValue('custpage_replen_cluster', i + 1, clusterNo);
				form.getSubList('custpage_replen_items').setLineItemValue('custpage_replen_recid', i + 1, RecId);
				form.getSubList('custpage_replen_items').setLineItemValue('custpage_replen_fromlp', i + 1, FromLP);
				form.getSubList('custpage_replen_items').setLineItemValue('custpage_replen_expqty', i + 1, ExpQty);
				form.getSubList('custpage_replen_items').setLineItemValue('custpage_replen_batchno', i + 1, Batchno);
				form.getSubList('custpage_replen_items').setLineItemValue('custpage_replen_itemtype', i + 1, ItemType);
				form.getSubList('custpage_replen_items').setLineItemValue('custpage_replen_remainingcube', i + 1, vTotalCubeValue);
				form.getSubList('custpage_replen_items').setLineItemValue('custpage_replen_whlocation', i + 1, whLocation);
				
			}
		}
		form.addSubmitButton('Confirm Replenishment');  
		response.writePage(form);
	}
	else 
	{

		var form = nlapiCreateForm('Confirm Replenishment');

		//  Create a Record in Opentask
		var updateDate= new Date();
		//a Date object to be used for a random value

		var lineCnt = request.getLineItemCount('custpage_replen_items');
		form.setScript('customscript_confirmrplnclientvalidation'); // Case# 20148643
		var result = 'false';
		try
		{
			for (var x = 1; x <= lineCnt; x++) 
			{	
				if (request.getLineItemValue('custpage_replen_items', 'custpage_replen_sel', x) == 'T') 
				{
					//nlapiLogExecution("ERROR", "Inside Loop" ,x);		
					var varsku = request.getLineItemValue('custpage_replen_items', 'custpage_replen_item', x);
					var varqty = request.getLineItemValue('custpage_replen_items', 'custpage_replen_rplnqty', x);//this is expqty
					var recid  = request.getLineItemValue('custpage_replen_items', 'custpage_replen_recid', x);
					var lp=request.getLineItemValue('custpage_replen_items', 'custpage_replen_lp', x);
					var location = request.getLineItemValue('custpage_replen_items', 'custpage_replen_endloc', x);
					var beginlocation = request.getLineItemValue('custpage_replen_items', 'custpage_replen_beginloc', x); 		
					var batchno = request.getLineItemValue('custpage_replen_items', 'custpage_replen_batchno', x); 		
					nlapiLogExecution("ERROR", "opentask Id" ,recid);
					nlapiLogExecution("ERROR", "batchno" ,batchno);
					var exceptedQty = request.getLineItemValue('custpage_replen_items', 'custpage_replen_expqty', x); 	
					var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recid);
					var wmsstatusflag = transaction.getFieldValue('custrecord_wms_status_flag');
					var actualenddate = transaction.getFieldValue('custrecord_act_end_date');
					var waveno = transaction.getFieldValue('custrecord_ebiz_wave_no');
					nlapiLogExecution("ERROR", "wmsstatusflag" ,wmsstatusflag);
					nlapiLogExecution("ERROR", "actualenddate" ,actualenddate);
					//if(wmsstatusflag=='20' && actualenddate!=null && actualenddate!='' ){
					if(wmsstatusflag=='20' && (actualenddate==null || actualenddate=='') )// case# 201412870
					{
						result = 'true';
						nlapiLogExecution("ERROR", "afteropentask Id" ,recid);
						var varsitelocation = transaction.getFieldValue('custrecord_site_id');
						transaction.setFieldValue('custrecord_actualendtime', TimeStamp());
						transaction.setFieldValue('custrecord_act_end_date', DateStamp());
						transaction.setFieldValue('custrecord_tasktype', 8);
						transaction.setFieldValue('custrecord_wms_status_flag','19');

						var varsku=transaction.getFieldValue('custrecord_ebiz_sku_no');
						var ebizTaskno=transaction.getFieldValue('custrecord_ebiz_task_no');
						var invtLpno=transaction.getFieldValue('custrecord_lpno');
						var FromLpno=transaction.getFieldValue('custrecord_from_lp_no');
						var whLocation=transaction.getFieldValue('custrecord_wms_location');
						var binLocation=transaction.getFieldValue('custrecord_actendloc');
						var beginLocation = transaction.getFieldValue('custrecord_actbeginloc');
						var reportno = transaction.getFieldValue('name');
						var itemStatus=transaction.getFieldValue('custrecord_sku_status');
						var fromInvRef=transaction.getFieldValue('custrecord_invref_no');
						//var itemStatus = getDefaultItemStatus();
						var accountNumber = getAccountNumber(whLocation);

						var ExpQty = transaction.getFieldValue('custrecord_expe_qty');
						var InvtRef = transaction.getFieldValue('custrecord_invref_no');
						nlapiLogExecution("ERROR", "ExpQty Id" ,ExpQty);
						nlapiLogExecution("ERROR", "invtLpno" ,invtLpno);
						nlapiLogExecution("ERROR", "lp" ,lp);
						invtLpno = lp;
						nlapiLogExecution("ERROR", "invtLpno111111" ,invtLpno);						
						// Repln qty is less than Expected Qty 
						if(parseFloat(varqty)< parseFloat(ExpQty))
						{
							var RemQty = parseFloat(ExpQty)-parseFloat(varqty);
							DeallocateRemainingInvt(RemQty,InvtRef);
							transaction.setFieldValue('custrecord_act_qty', varqty);
							//varqty = RemQty;
						}
						else
						{
							transaction.setFieldValue('custrecord_act_qty', ExpQty);
						}
						var stageLocation=GetStageLocation(varsku, "", whLocation, "", "BOTH");//shylaja

						//if stage location is not defined(1 step replen)
						if(stageLocation == -1){

							if(fromInvRef == null || fromInvRef == '')
							{
								var bulkLocationInventoryResults=getRecord(varsku, beginLocation, FromLpno,itemStatus);
								//var bulkLocationInventoryResults=getRecord(varsku, beginLocation, invtLpno);
								if(bulkLocationInventoryResults != null && bulkLocationInventoryResults != '')
									fromInvRef=bulkLocationInventoryResults[0].getid();
							}
							if(fromInvRef != null && fromInvRef != '')
								updateBulkLocation(fromInvRef, varsku, beginLocation, varqty, itemStatus,exceptedQty);

							var invtresults=getRecord(varsku, binLocation,invtLpno,itemStatus);
							var invtresultsofreplens=getRecordofreplen(varsku, binLocation,invtLpno,itemStatus,batchno);
							updateOrCreatePickfaceInventory(invtresultsofreplens, varsku, varqty, invtLpno, binLocation, 
									itemStatus, accountNumber,whLocation,batchno,waveno);

						}
						//if stage location is defined(2 step replen)
						else{
							//	if(ebizTaskno != null && ebizTaskno != ""){         
							//		var invrecord =createstagInvtRecord(varsku, varsku,  varqty, invtLpno, whLocation,binLocation, itemStatus, accountNumber);
							// Update the status (from I to R) of the task which is paired with the present task (R) and updating invRefNo
							//		updateStageTask(ebizTaskno, invrecord,invtLpno);
							// Update inventory for the begin location
							if(fromInvRef == null || fromInvRef == '')
							{
								var bulkLocationInventoryResults=getRecord(varsku, beginLocation, FromLpno,itemStatus);
								//var bulkLocationInventoryResults=getRecord(varsku, beginLocation, invtLpno);
								if(bulkLocationInventoryResults != null && bulkLocationInventoryResults != '')
									fromInvRef=bulkLocationInventoryResults[0].getId();
							}
							if(fromInvRef != null && fromInvRef != '')
								updateBulkLocation(fromInvRef, varsku, beginLocation, varqty, itemStatus,exceptedQty);
							//	}

							var invRefNo=transaction.getFieldValue('custrecord_invref_no');                           
							var vresult = nlapiSubmitRecord(transaction);

							nlapiLogExecution('ERROR','Inventory Ref No', invRefNo);
							//var invtresults=getRecord(varsku, binLocation,FromLpno);
							var invtresults=getRecord(varsku, binLocation,invtLpno,itemStatus,batchno);
							var invtresultsofreplens=getRecordofreplen(varsku, binLocation,invtLpno,itemStatus,batchno);

							if(invRefNo != null && invRefNo != "")
							{
								//Update Inventory in pickface bin location
								updateOrCreatePickfaceInventory(invtresultsofreplens, varsku, varqty, invtLpno, binLocation, 
										itemStatus, accountNumber,whLocation,batchno,waveno);
								var LocationType = nlapiLookupField('customrecord_ebiznet_location', beginLocation, ['custrecord_ebizlocationtype']);

								if (LocationType.custrecord_ebizlocationtype == '8' )
								{
									DeleteInventoryRecord(invRefNo);
								}
							}
							//nlapiSubmitRecord(transaction, true);	//check it
						}
						nlapiSubmitRecord(transaction, true);
						if(invtresultsofreplens != null && invtresultsofreplens != "")
						{
							var columns = nlapiLookupField('customrecord_ebiznet_createinv', invtresultsofreplens[0].getId(),['custrecord_ebiz_inv_lp']);
							if(columns.custrecord_ebiz_inv_lp != null && columns.custrecord_ebiz_inv_lp != "")
							{        						
								invtLpno = columns.custrecord_ebiz_inv_lp;	
								nlapiLogExecution('ERROR','columns.custrecord_ebiz_inv_lp', columns.custrecord_ebiz_inv_lp);
							}            		
						} 
						CreateLPrecord(invtLpno);

						//Case# 20148543 starts
						//var fields = ['recordType'];
						//var columns = nlapiLookupField('item', varsku, fields);
						//var ItemType = columns.recordType;
						var ItemType ='';
						nlapiLogExecution('ERROR', 'varsku', varsku);
						var filter=new Array();
						filter[0]=new nlobjSearchFilter("internalid",null,"anyof",varsku);
						var column=new Array();
						column[0]=new nlobjSearchColumn("type");
						var searchres=nlapiSearchRecord("item",null,filter,column);
						if(searchres!=null&&searchres!="")
						{
							ItemType=searchres[0].recordType;
						}
						// Case# 20148543 ends
						nlapiLogExecution('ERROR','ItemType',ItemType);

						if (ItemType == "serializedinventoryitem" || ItemType == "serializedassemblyitem") 
						{
							AdjustSerialNumbers(varqty,varsku,FromLpno,invtLpno,binLocation);
						}
					}

				} // end of if for the main grid			
			}
		}
		catch (e) {
			nlapiLogExecution('ERROR', 'Updating OPEN TASK Record failed', e);
		}
		var msg= form.addField('custpage_message', 'inlinehtml', null, null, null);
		if(result == 'true')
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Replen Tasks confirmed successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
		else
			msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'Replen Tasks Already confirmed successfully', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
		response.writePage(form);
	}    
}

/**
 * getting records from create inventory
 * @param itemid
 * @param location
 * @param invtLPNo
 * @returns
 */
function getRecord(itemid,location, invtLPNo,itemStatus,batchno)
{
	nlapiLogExecution('ERROR', 'getRecord', location);
	nlapiLogExecution('ERROR', 'getRecord_invtLPNo', invtLPNo);
	var filtersso = new Array();
	var socount=0;		

	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', itemid));
	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', location));
	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', itemStatus));
	filtersso.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [19]));
	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0)); //sri added for LL_Production purpose(RR_Reference).
	if(batchno!=null && batchno!='')
		filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'is', batchno));
	if(invtLPNo != null && invtLPNo != '')
		filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', invtLPNo));

	nlapiLogExecution('ERROR', 'custrecord_ebiz_inv_sku', itemid);
	nlapiLogExecution('ERROR', 'custrecord_ebiz_inv_binloc', location);
	nlapiLogExecution('ERROR', 'custrecord_ebiz_inv_lp', invtLPNo);
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_qoh');

	var searchresultsLine = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersso, columns);
	if(searchresultsLine != null)
	{
		nlapiLogExecution('ERROR', 'Inventory count', searchresultsLine.length);
		socount=searchresultsLine.length; 
	} 
	else
	{ 
		nlapiLogExecution('ERROR', 'inside else if results a', 'null');
	}       
	return searchresultsLine;
}

function getRecordofreplen(itemid,location, invtLPNo,itemStatus,batchno)
{
	nlapiLogExecution('ERROR', 'getRecord', location);
	var filtersso = new Array();
	var socount=0;		

	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'anyof', itemid));
	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'anyof', location));
	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', itemStatus));
	filtersso.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [19]));
	filtersso.push(new nlobjSearchFilter('custrecord_ebiz_qoh', null, 'greaterthan', 0)); //sri added for LL_prod_purpose
	if(batchno!=null && batchno!='')
		filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'is', batchno));
	//if(invtLPNo != null && invtLPNo != '')
		//filtersso.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null, 'is', invtLPNo));

	nlapiLogExecution('ERROR', 'custrecord_ebiz_inv_sku', itemid);
	nlapiLogExecution('ERROR', 'custrecord_ebiz_inv_binloc', location);
	nlapiLogExecution('ERROR', 'custrecord_ebiz_inv_lp', invtLPNo);
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_qty');
	columns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	columns[4] = new nlobjSearchColumn('custrecord_ebiz_qoh');

	var searchresultsLine = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersso, columns);
	if(searchresultsLine != null)
	{
		nlapiLogExecution('ERROR', 'Inventory count', searchresultsLine.length);
		socount=searchresultsLine.length; 
	} 
	else
	{ 
		nlapiLogExecution('ERROR', 'inside else if results a', 'null');
	}       
	return searchresultsLine;
}



/**
updating status flag and invrefno in Open Task
 */
function updateStageTask(recId, invrecord,invtLpno)
{
	nlapiLogExecution('ERROR', 'transactionresult', recId); 
	nlapiLogExecution('ERROR', 'invrecord', invrecord); 
	try {

//		var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', recId);
//		transaction.setFieldValue('custrecord_invref_no', invrecord);
//		transaction.setFieldValue('custrecord_wms_status_flag', 20);
//		transaction.setFieldValue('custrecord_lpno', invtLpno);
//		var result = nlapiSubmitRecord(transaction);

		var fieldNames = new Array(); 
		fieldNames.push('custrecord_invref_no');  
		fieldNames.push('custrecord_wms_status_flag');
		fieldNames.push('custrecord_lpno');

		var newValues = new Array(); 
		newValues.push(invrecord);
		newValues.push('20');
		newValues.push(invtLpno);

		nlapiSubmitField('customrecord_ebiznet_trn_opentask', recId, fieldNames, newValues);


	} 
	catch (e) {
		nlapiLogExecution('ERROR', 'Updating OPEN TASK Record failed in updateStagTask', e);
	}
}

//function updateBulkLocation(bulkLocationInventoryResults, itemID, beginLocation, varqty, itemStatus)
function updateBulkLocation(bulkLocationInventoryRecId, itemID, beginLocation, varqty, itemStatus,exceptedQty)
{	
	nlapiLogExecution("ERROR", "Into updateBulkLocation");		

	if(bulkLocationInventoryRecId != null)
	{	
		//var bulkLocationInventoryRecId = bulkLocationInventoryResults[0].getId();								
		var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', bulkLocationInventoryRecId);
		var inventoryQty = transaction.getFieldValue('custrecord_ebiz_inv_qty');
		var inventoryAllocQty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
		var inventoryQOHQty = transaction.getFieldValue('custrecord_ebiz_qoh');

		if (inventoryQty == null || isNaN(inventoryQty) || inventoryQty == "") 
		{
			inventoryQty = 0;
		}

		if (inventoryAllocQty == null || isNaN(inventoryAllocQty) || inventoryAllocQty == "" || inventoryAllocQty<0) 
		{
			inventoryAllocQty = 0;
		}

		if (varqty == null || isNaN(varqty) || varqty == "") 
		{
			varqty = 0;
		}

		if (inventoryQOHQty == null || isNaN(inventoryQOHQty) || inventoryQOHQty == "") 
		{
			inventoryQOHQty = 0;
		}

		var str = 'inventoryAllocQty.' + inventoryAllocQty + '<br>';
		str = str + 'inventoryQty.' + inventoryQty + '<br>';
		str = str + 'inventoryQOHQty.' + inventoryQOHQty + '<br>';
		str = str + 'varqty. ' + varqty + '<br>';	

		nlapiLogExecution('ERROR', 'Qty Details1', str);

		//inventoryAllocQty = parseFloat(inventoryAllocQty) - parseFloat(varqty);				
		inventoryQty =	parseFloat(inventoryQty) - parseFloat(varqty);
		var QOHQty = parseFloat(inventoryQOHQty)- parseFloat(varqty);	

		var str = 'inventoryAllocQty.' + inventoryAllocQty + '<br>';
		str = str + 'inventoryQty.' + inventoryQty + '<br>';	
		str = str + 'QOHQty. ' + QOHQty + '<br>';	
		str = str + 'varqty. ' + varqty + '<br>';	

		nlapiLogExecution('ERROR', 'Qty Details', str);
		/*code merged from boombah on feb25th 2013 BY RADHIKA */
		if(varqty==0 )
		{
			nlapiLogExecution("ERROR", "exceptedQty", exceptedQty);
			inventoryAllocQty = parseInt(inventoryAllocQty) - parseInt(exceptedQty);
			nlapiLogExecution("ERROR", "inventoryAllocQty1", inventoryAllocQty);
			transaction.setFieldValue('custrecord_ebiz_alloc_qty',inventoryAllocQty);
		}
		else
		{
			/* code added from FactoryMation on 28Feb13 by santosh, decimal conversion */
			if((parseInt(inventoryAllocQty)>0) && (parseInt(inventoryAllocQty)>=parseInt(varqty)))
				inventoryAllocQty = parseInt(inventoryAllocQty) - parseInt(varqty);
			nlapiLogExecution('ERROR', 'inventoryAllocQty', inventoryAllocQty);
			transaction.setFieldValue('custrecord_ebiz_alloc_qty',parseFloat(inventoryAllocQty).toFixed(5));
		}

		//transaction.setFieldValue('custrecord_ebiz_alloc_qty',inventoryAllocQty);
		/*upto here */
		transaction.setFieldValue('custrecord_ebiz_inv_sku_status',itemStatus);
		transaction.setFieldValue('custrecord_ebiz_qoh',parseFloat(QOHQty).toFixed(5));
		/* upto here */
		transaction.setFieldValue('custrecord_ebiz_callinv', 'N');
		transaction.setFieldValue('custrecord_ebiz_displayfield', 'N');
		nlapiSubmitRecord(transaction);		
		//Case # 20148739 Start
		/*var retValue =  LocationCubeUpdation(itemID, beginLocation, varqty, 'P');*/	
		//Case # 20148739 end
		if(parseFloat(QOHQty)<=0)
		{
			nlapiLogExecution('ERROR', 'Deleting ZERO qty invetory record...',bulkLocationInventoryRecId);
			nlapiDeleteRecord('customrecord_ebiznet_createinv', bulkLocationInventoryRecId);
		}

	}

	nlapiLogExecution("ERROR", "Out of updateBulkLocation");		
}

/**
 * 
 * Below is the search criteria to fetch the default item status from the item status master
 */
function getDefaultItemStatus(){

	var filtersitemstatus = new Array();
	filtersitemstatus[0] = new nlobjSearchFilter('custrecord_defaultskustaus', null, 'is', 'T');

	var searchresultsitemstatus = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, filtersitemstatus, null);
	if(searchresultsitemstatus != null && searchresultsitemstatus.length > 0)
	{
		var itemStatus = searchresultsitemstatus[0].getId();
		//nlapiLogExecution("ERROR", "searchresultsitemstatus[0].getId()", itemStatus);
	}
	return itemStatus;
}

/**
 * 
 * @param whLocation
 * @returns
 */
function getAccountNumber(whLocation){
	var filtersAccount = new Array();
	filtersAccount[0] = new nlobjSearchFilter('custrecord_location', null, 'is', whLocation);

	var columnsAccount = new Array();
	columnsAccount[0]=new nlobjSearchColumn('custrecord_accountno');                

	var accountSearchResults = nlapiSearchRecord('customrecord_inventoryaccountno', null, filtersAccount, columnsAccount);              

	if (accountSearchResults != null){
		var accountNumber = accountSearchResults[0].getValue('custrecord_accountno');                                
		//nlapiLogExecution('ERROR', 'Account #',accountNumber);
	}

	return accountNumber;
}

/**
 * 
 * @param vitem
 * @param varsku
 * @param varqty
 * @param lp
 * @param whLocation
 * @param binLocation
 * @param itemStatus
 * @param accountNumber
 * @returns
 */
function createstagInvtRecord(vitem, varsku, varqty, lp, whLocation, binLocation, itemStatus, accountNumber)
{
	var invrecord = nlapiCreateRecord('customrecord_ebiznet_createinv');
	//invrecord.setFieldValue('name', vitem+1);
	invrecord.setFieldValue('name', vitem);
	invrecord.setFieldValue('custrecord_ebiz_inv_sku', varsku);
	invrecord.setFieldValue('custrecord_ebiz_inv_note1','Replenished');
	invrecord.setFieldValue('custrecord_ebiz_qoh', parseFloat(varqty).toFixed(5));
	invrecord.setFieldValue('custrecord_ebiz_callinv', 'N');
	invrecord.setFieldValue('custrecord_ebiz_inv_lp', lp);
	invrecord.setFieldValue('custrecord_ebiz_inv_binloc', binLocation);
	invrecord.setFieldValue('custrecord_ebiz_inv_loc', whLocation);
	invrecord.setFieldValue('custrecord_ebiz_inv_sku_status', itemStatus);
	invrecord.setFieldValue('custrecord_ebiz_inv_packcode', 1);
	invrecord.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(varqty).toFixed(5));
	invrecord.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);
	invrecord.setFieldValue('custrecord_ebiz_displayfield', 'N');
	invrecord.setFieldValue('custrecord_wms_inv_status_flag','19');
	invrecord.setFieldValue('custrecord_invttasktype', 8);
	nlapiLogExecution('ERROR', 'while creating inventory varsku', varsku);
	nlapiLogExecution('ERROR', 'while creating inventory binLocation', binLocation);
	nlapiLogExecution('ERROR', 'while creating inventory lp', lp);
	var retval=nlapiSubmitRecord(invrecord);
	return retval;
}

/**
 * 
 * @param invtresults
 */
function updateOrCreatePickfaceInventory(invtresults, varsku, varqty, invtLpno, binLocation, 
		itemStatus, accountNumber,whLocation,batchno,waveno){
	if(invtresults != null)
	{
		nlapiLogExecution("ERROR", "Inventory Exists", invtresults[0].getId());	
		var invtintrid=invtresults[0].getId();
		var scount=1; 

		LABL1: for(var i=0;i<scount;i++)
		{
			nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);

			try
			{
				var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invtintrid);								
				var invtqty = transaction.getFieldValue('custrecord_ebiz_qoh');									
				var fifioDtExist = transaction.getFieldValue('custrecord_ebiz_inv_fifo');// case# 201413376
				var lotnoExistforitem = transaction.getFieldValue('custrecord_ebiz_inv_lot');

				if (invtqty == null || isNaN(invtqty) || invtqty == "") {
					invtqty = 0;
				}

		invtqty = parseFloat(invtqty) + parseFloat(varqty);						
		transaction.setFieldValue('custrecord_ebiz_inv_sku_status',itemStatus);	
		transaction.setFieldValue('custrecord_ebiz_qoh',parseFloat(invtqty).toFixed(5));	
		transaction.setFieldValue('custrecord_ebiz_callinv','N');	
		transaction.setFieldValue('custrecord_ebiz_displayfield','N');	
		if((lotnoExistforitem == null || lotnoExistforitem == '')&& (fifioDtExist == null || fifioDtExist == ''))
			transaction.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());

				nlapiLogExecution("ERROR", "Quantity Assigned ", invtqty);						
				nlapiSubmitRecord(transaction,false,true);	
			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 

				nlapiLogExecution('Debug', 'Exception in Replen Confirm : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}
		updateInvRefforOpenPickTasks(varsku,binLocation,invtintrid,waveno);
	}
	else
	{
		nlapiLogExecution("ERROR", "Inventory Not Exists", varsku);		
		nlapiLogExecution("ERROR", "batchno", batchno);	
		nlapiLogExecution("ERROR", "invtLpno_cretae", invtLpno);	
		var invrecord = nlapiCreateRecord('customrecord_ebiznet_createinv');
		invrecord.setFieldValue('name', varsku);					
		invrecord.setFieldValue('custrecord_ebiz_inv_sku', varsku); 	
		invrecord.setFieldValue('custrecord_ebiz_inv_note1','Replenished');
		invrecord.setFieldValue('custrecord_ebiz_qoh', parseFloat(varqty).toFixed(5));
		invrecord.setFieldValue('custrecord_ebiz_callinv', 'N');		
		invrecord.setFieldValue('custrecord_ebiz_inv_lp', invtLpno);		
		invrecord.setFieldValue('custrecord_ebiz_inv_binloc', binLocation);	
		invrecord.setFieldValue('custrecord_ebiz_inv_sku_status', itemStatus);
		invrecord.setFieldValue('custrecord_ebiz_inv_packcode', 1);
		invrecord.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(varqty).toFixed(5));
		//invrecord.setFieldValue('custrecord_ebiz_inv_lot', batchno);
		if(batchno!="" && batchno!=null)
		{

			var filterspor = new Array();
			filterspor.push(new nlobjSearchFilter('name', null, 'is', batchno));
			if(whLocation!=null && whLocation!="")
				filterspor.push(new nlobjSearchFilter('custrecord_ebizsitebatch', null, 'anyof', whLocation));
			if(varsku!=null && varsku!="")
				filterspor.push(new nlobjSearchFilter('custrecord_ebizsku', null, 'anyof', varsku));

			var column=new Array();
			column[0]=new nlobjSearchColumn('custrecord_ebizexpirydate');
			column[1]=new nlobjSearchColumn('custrecord_ebizfifodate');

			var receiptsearchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterspor,column);
			if(receiptsearchresults!=null)
			{
				var getlotnoid= receiptsearchresults[0].getId();
				nlapiLogExecution('ERROR', 'getlotnoid', getlotnoid);
				var expdate=receiptsearchresults[0].getValue('custrecord_ebizexpirydate');
				var vfifodate=receiptsearchresults[0].getValue('custrecord_ebizfifodate');
				invrecord.setFieldValue('custrecord_ebiz_inv_lot', getlotnoid);
				invrecord.setFieldValue('custrecord_ebiz_expdate', expdate);
				invrecord.setFieldValue('custrecord_ebiz_inv_fifo', vfifodate);
				nlapiLogExecution('DEBUG', 'expdate', expdate);
				nlapiLogExecution('DEBUG', 'vfifodate', vfifodate);
			}

		}
		else
			invrecord.setFieldValue('custrecord_ebiz_inv_fifo', DateStamp());
		if(accountNumber!=null && accountNumber!='')
			invrecord.setFieldValue('custrecord_ebiz_inv_account_no', accountNumber);
		invrecord.setFieldValue('custrecord_ebiz_displayfield', 'N');
		invrecord.setFieldValue('custrecord_wms_inv_status_flag','19');
		invrecord.setFieldValue('custrecord_invttasktype', 8);
		if(whLocation!=null && whLocation!='')
			invrecord.setFieldValue('custrecord_ebiz_inv_loc', whLocation);

		var invtrefno = nlapiSubmitRecord(invrecord,false,true);

		updateInvRefforOpenPickTasks(varsku,binLocation,invtrefno,waveno);
	}
	//Case # 20148739 Start
	/*var retValue =  LocationCubeUpdation(varsku, binLocation, varqty, 'M');
	nlapiLogExecution('ERROR', 'updateOrCreatePickfaceInventory LocCubeUpdation', 'Success');*/
	//Case # 20148739  end
}

function updateInvRefforOpenPickTasks(itemno,binlocation,invrefno,waveno)
{
	try
	{
		nlapiLogExecution('ERROR', 'Into  updateInvRefforOpenPickTasks');

		var str = 'itemno. = ' + itemno + '<br>';
		str = str + 'binlocation. = ' + binlocation + '<br>';	
		str = str + 'invrefno. = ' + invrefno + '<br>';
		str = str + 'waveno. = ' + waveno + '<br>';

		nlapiLogExecution('ERROR', 'Parameter Details', str);

		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', binlocation));
		filters.push(new nlobjSearchFilter('custrecord_sku', null, 'anyof', itemno));
		filters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));	 // TASK TYPE - PICK	
		filters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	 // STATUS - PICKS GENERATED
		filters.push(new nlobjSearchFilter('custrecord_invref_no', null, 'isempty'));
		//if(waveno!=null && waveno!='')
		//filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(waveno)));
		var taskcolumn=new Array();
		taskcolumn[0]=new nlobjSearchColumn('custrecord_expe_qty');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, taskcolumn);

		if(searchresults!=null && searchresults!='')
		{
			for (var u = 0; u < searchresults.length; u++) {
				var taskrecid = searchresults[u].getId();
				var taskExpectedqty=searchresults[u].getValue('custrecord_expe_qty');
				nlapiLogExecution('ERROR', 'taskrecid', taskrecid);

				var scount=1; 

				LABL1: for(var i=0;i<scount;i++)
				{
					try
					{
						if(taskrecid!=null && taskrecid!='')
							nlapiSubmitField('customrecord_ebiznet_trn_opentask', taskrecid, 'custrecord_invref_no', invrefno);

						var picktransaction = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);

						var currAllocationqty=picktransaction.getFieldValue('custrecord_ebiz_alloc_qty');	

						if (taskExpectedqty == null || isNaN(taskExpectedqty) || taskExpectedqty == "") {
							taskExpectedqty = 0;
						}

						if (currAllocationqty == null || isNaN(currAllocationqty) || currAllocationqty == "" || currAllocationqty<0) {
							currAllocationqty = 0;
						}

						totallocatedqty = parseFloat(taskExpectedqty) + parseFloat(currAllocationqty);						

						picktransaction.setFieldValue('custrecord_ebiz_alloc_qty',parseFloat(totallocatedqty).toFixed(4));	
						picktransaction.setFieldValue('custrecord_ebiz_callinv','N');	
						picktransaction.setFieldValue('custrecord_ebiz_displayfield','N');									
						nlapiSubmitRecord(picktransaction,false,true);
					}

					catch(ex)
					{
						var exCode='CUSTOM_RECORD_COLLISION'; 
						var wmsE='Inventory record being updated by another user. Please try again...';
						if (ex instanceof nlobjError) 
						{	
							wmsE=ex.getCode() + '\n' + ex.getDetails();
							exCode=ex.getCode();
						}
						else
						{
							wmsE=ex.toString();
							exCode=ex.toString();
						} 

						nlapiLogExecution('Debug', 'Exception in Replen Confirm : ', wmsE); 
						if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
						{ 
							scount=scount+1;
							continue LABL1;
						}
						else break LABL1;
					}
				}
			}
		}
	}
	catch(e)
	{
		nlapiLogExecution('ERROR', 'updateInvRefforOpenPickTasks', e);
	}

	nlapiLogExecution('ERROR', 'Out of  updateInvRefforOpenPickTasks');
}

function LocationCubeUpdation(itemID, beginLocation, varqty, optype)
{
	var arrDims = getSKUCubeAndWeight(itemID, 1);
	var itemCube = 0;
	var vTotalCubeValue = 0;
	nlapiLogExecution('ERROR', 'arrDims[0]', arrDims[0]);
	//case# 20127533 starts (Pickface location remCube is updating with negative values issue)
	/*if ((arrDims["BaseUOMItemCube"] != "") && (!isNaN(arrDims["BaseUOMItemCube"]))) 
	{
		nlapiLogExecution('ERROR', 'itemCube if', itemCube);
		var uomqty = ((parseFloat(varqty))/(parseFloat(arrDims["BaseUOMQty"])));			
		itemCube = ((parseFloat(uomqty)) * (parseFloat(arrDims["BaseUOMItemCube"])));
		nlapiLogExecution('ERROR', 'itemCube', itemCube);
	} */

	if((arrDims[0] != "") && (!isNaN(arrDims[0]))) 
	{
		nlapiLogExecution('ERROR', 'itemCube if', itemCube);
		var uomqty = ((parseFloat(varqty))/(parseFloat(arrDims[1])));			
		itemCube = ((parseFloat(uomqty)) * (parseFloat(arrDims[0])));
		nlapiLogExecution('ERROR', 'itemCube', itemCube);
	}

	nlapiLogExecution('ERROR', 'beginLocationCube', beginLocation);
	var vOldRemainingCube = GeteLocCube(beginLocation);
	nlapiLogExecution('ERROR', 'vOldRemainingCube', vOldRemainingCube);

	if(optype == "P")
		vTotalCubeValue = parseFloat(itemCube)+ parseFloat(vOldRemainingCube);
	else
		vTotalCubeValue = parseFloat(vOldRemainingCube)-parseFloat(itemCube);
	//case# 20127533 end
	nlapiLogExecution('ERROR', 'vOldLocationRemainingCube', vTotalCubeValue);	

	var retValue="";

	if(vTotalCubeValue>=0){
		retValue =  UpdateLocCube(beginLocation,vTotalCubeValue);
		nlapiLogExecution('ERROR', 'LocCubeUpdation', retValue);
	}

	return retValue; 
}

/**
 * 
 */
function DeleteInventoryRecord(invRefNo)
{
	nlapiLogExecution('ERROR','Inside DeleteInvtRecCreatedforCHKNTask ','Function');
	var Ifilters = new Array();
	Ifilters[0] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [19]);
	Ifilters[1] = new nlobjSearchFilter('custrecord_invttasktype', null, 'anyof', [8]);
	Ifilters[2] = new nlobjSearchFilter('internalid', null, 'is', invRefNo);

	var invtId = "";
	var invtType = "";
	var searchInventory= nlapiSearchRecord('customrecord_ebiznet_createinv', null, Ifilters, null);
	if (searchInventory){
		for (var s = 0; s < searchInventory.length; s++) {
			invtId = searchInventory[s].getId();
			invtType= searchInventory[s].getRecordType();
			nlapiDeleteRecord(searchInventory[s].getRecordType(),searchInventory[s].getId());
			nlapiLogExecution('ERROR','Inventory record deleted ',invtId);
		}
	}
}

function CreateLPrecord(invtLpno)
{
	var filtersmlp = new Array();
	filtersmlp[0] = new nlobjSearchFilter('name', null, 'is', invtLpno);

	var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

	if (SrchRecord != null && SrchRecord.length > 0) {
		nlapiLogExecution('ERROR', 'LP FOUND');

		lpExists = 'Y';
	}
	else {
		nlapiLogExecution('ERROR', 'LP NOT FOUND');
		var customrecord = nlapiCreateRecord('customrecord_ebiznet_master_lp');
		customrecord.setFieldValue('name', invtLpno);
		customrecord.setFieldValue('custrecord_ebiz_lpmaster_lp', invtLpno);
		var rec = nlapiSubmitRecord(customrecord, false, true);
	}
}

function AdjustSerialNumbers(qty,item,lp,newlp,binLocation)
{	
	nlapiLogExecution('ERROR','AdjustSerialNumbers qty',qty);
	nlapiLogExecution('ERROR','item',item);
	nlapiLogExecution('ERROR','lp',lp);
	nlapiLogExecution('ERROR','binLocation',binLocation);
	if(parseInt(qty) > 0)
	{
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
		filters[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
		filters[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'isnot', 'D');

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
		var searchResults = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters, columns);             

		if(searchResults != null && searchResults != "")
		{ 
			for (var i = 0; i < searchResults.length; i++) 
			{   
				if(searchResults[i].getValue('custrecord_serialnumber') != null && searchResults[i].getValue('custrecord_serialnumber') != "")
				{
					var SerialNo = searchResults[i].getValue('custrecord_serialnumber');
					var InternalID = searchResults[i].getId();

					var LoadSerialNumbers = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
					LoadSerialNumbers.setFieldValue('custrecord_serialparentid', newlp);
					LoadSerialNumbers.setFieldValue('custrecord_serialbinlocation', binLocation);
					LoadSerialNumbers.setFieldValue('custrecord_serialstatus', '');

					var recid = nlapiSubmitRecord(LoadSerialNumbers, false, true);
					nlapiLogExecution('ERROR', 'Serial Entry rec id',recid);
				}
			}
		}		

		var filters1 = new Array();
		filters1[0] = new nlobjSearchFilter('custrecord_serialiteminternalid', null, 'is', item);
		filters1[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lp);
		filters1[2] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'D');

		var columns1 = new Array();
		columns1[0] = new nlobjSearchColumn('custrecord_serialnumber');
		var searchResults1 = nlapiSearchRecord('customrecord_ebiznetserialentry',null, filters1, columns1);             

		if(searchResults1 != null && searchResults1 != "")
		{ 
			for (var i = 0; i < searchResults1.length; i++) 
			{   
				if(searchResults1[i].getValue('custrecord_serialnumber') != null && searchResults1[i].getValue('custrecord_serialnumber') != "")
				{
					var SerialNo1 = searchResults1[i].getValue('custrecord_serialnumber');
					var InternalID1 = searchResults1[i].getId();

					var LoadSerialNumbers1 = nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID1);
					LoadSerialNumbers1.setFieldValue('custrecord_serialstatus', '');

					var recid1 = nlapiSubmitRecord(LoadSerialNumbers1, false, true);
					nlapiLogExecution('ERROR', 'Serial Entry rec id',recid1);
				}
			}
		}
	}
}



//Deallocate allocate Qty if Expected qty is less than Repln Qty
function DeallocateRemainingInvt(RemQty,InvtRef)
{
	try{
		nlapiLogExecution('ERROR', 'Deallocation of Inventory', 'CreateInvt');
		nlapiLogExecution('ERROR', 'RemQty', RemQty);
		nlapiLogExecution('ERROR', 'InvtRef', InvtRef);
		var scount=1;


		LABL1: for(var i=0;i<scount;i++)
		{
			nlapiLogExecution('Debug', 'CUSTOM_RECORD_COLLISION', i);

			try
			{
				var transaction = nlapiLoadRecord('customrecord_ebiznet_createinv',InvtRef);
				var qty = transaction.getFieldValue('custrecord_ebiz_qoh');
				var allocqty = transaction.getFieldValue('custrecord_ebiz_alloc_qty');
				var vLP=transaction.getFieldValue('custrecord_ebiz_inv_lp');

				nlapiLogExecution('ERROR', 'qty', qty);
				nlapiLogExecution('ERROR', 'allocqty', allocqty);
				nlapiLogExecution('ERROR', 'RemQty', RemQty);
				transaction.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(allocqty)- parseFloat(RemQty)));
				nlapiSubmitRecord(transaction, false, true); 
			}
			catch(ex)
			{
				var exCode='CUSTOM_RECORD_COLLISION'; 
				var wmsE='Inventory record being updated by another user. Please try again...';
				if (ex instanceof nlobjError) 
				{	
					wmsE=ex.getCode() + '\n' + ex.getDetails();
					exCode=ex.getCode();
				}
				else
				{
					wmsE=ex.toString();
					exCode=ex.toString();
				} 

				nlapiLogExecution('Debug', 'Exception in Replen confirm From Location : ', wmsE); 
				if(exCode=='CUSTOM_RECORD_COLLISION' || exCode=='UNEXPECTED_ERROR'  || exCode =='RCRD_HAS_BEEN_CHANGED')
				{ 
					scount=scount+1;
					continue LABL1;
				}
				else break LABL1;
			}
		}
	}
	catch(e)
	{
		nlapiLogExecution('ERROR', 'Exception in Deallocation', e);
	}
}
