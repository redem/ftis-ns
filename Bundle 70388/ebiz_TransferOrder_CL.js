/*******************************************************************************
 * eBizNET Solutions
 ******************************************************************************/
/*
 * Prologue - INTFMGR_defaults.sql
 * ***************************************************************************
 * 
 * $Source:
 * /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Outbound/Client/ebiz_SalesOrder_CL.js,v $
 * $Revision: 1.1.2.6.2.1 $ $Date: 2015/10/20 10:50:00 $ $Author:
 * rmukkera $
 * 
 * eBizNET version and checksum stamp. Do not remove. $eBiznet_VER:
 * .............. $eBizNET_SUM: ..... PRAMETERS
 * 
 * 
 * DESCRIPTION
 * 
 * Default Data for Interfaces
 * 
 * NOTES AND WARNINGS
 * 
 * INITATED FROM
 * 
 * REVISION HISTORY $Log: ebiz_TransferOrder_CL.js,v $
 * REVISION HISTORY Revision 1.1.2.6.2.1  2015/10/20 10:50:00  grao
 * REVISION HISTORY  CT issue fixes 201415135
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.1.2.6  2015/06/12 15:49:07  grao
 * REVISION HISTORY SB issue fixes 201413064
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.1.2.5  2015/05/11 15:40:27  skreddy
 * REVISION HISTORY Case# 201412704
 * REVISION HISTORY Hilmot  SB issue fix
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.1.2.4  2015/04/15 13:20:54  schepuri
 * REVISION HISTORY case # 201412340
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.1.2.3  2014/09/18 14:44:33  skavuri
 * REVISION HISTORY Case# 201410420 std bundle issue fixed.
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.1.2.2  2014/08/14 14:53:43  skavuri
 * REVISION HISTORY Case# 201410031 Std Bundle Issue Fixed
 * REVISION HISTORY
 * REVISION HISTORY Revision 1.1.2.1  2014/01/29 15:19:33  sponnaganti
 * REVISION HISTORY Case# 20126969
 * REVISION HISTORY New Clientscript added for TO
 * REVISION HISTORY For validating qty field , item field selected line remove after wave generation(Should not allow user to change/remove)
 * REVISION HISTORY
 */

function onDropChangeType(type, name) {
	// alert("type " + type);

	// alert("name " + name);

	var toId = nlapiGetFieldValue('id');

	// alert("toId " + toId);

	if (toId != null && toId != '' && toId >= 1) {
		if (name == 'quantity') {
			var lineNo = nlapiGetCurrentLineItemValue('item', 'line');
			var lineCnt = nlapiGetLineItemCount('item');
			// alert("lineNo " + lineNo)
			var searchRecords = getRecordDetails(toId, lineNo, 'noneof');
			//alert("searchRecords " + searchRecords)
			if (searchRecords != null && searchRecords != ''
				&& searchRecords.length > 0) {
				var searchresults = nlapiLoadRecord('transferorder', toId);
				for ( var s = 1; s <= lineCnt; s++) {
					var vLineno = searchresults.getLineItemValue('item',
							'line', s);
					var vOrderQty = searchresults.getLineItemValue('item',
							'quantity', s);
					var vlinechangedQty = nlapiGetCurrentLineItemValue('item',
					'quantity');

					// /alert("vlinechangedQty:" + vlinechangedQty);
					// alert("vOrderQty:" + vOrderQty);

					if (vlinechangedQty < vOrderQty) {
						var Itemid = searchresults.getLineItemValue('item',
								'item', s);
						nlapiSetCurrentLineItemValue('item', 'quantity',
								vOrderQty, false);
						alert("Order is being processed in Main Warehouse. Do not make Qty changes, Contact Warehouse Manager.");

						return false;
					}
				}

			}

		}
		if (name == 'item') {
			var lineNo = nlapiGetCurrentLineItemValue('item', 'line');
			var lineCnt = nlapiGetLineItemCount('item');

			var searchRecords = getRecordDetails(toId, lineNo, 'noneof');

			if (searchRecords != null && searchRecords != ''
				&& searchRecords.length > 0) {
				var searchresults = nlapiLoadRecord('transferorder', toId);
				for ( var s = 1; s <= lineCnt; s++) {
					var vLineno = searchresults.getLineItemValue('item',
							'line', s);
					var vOldItem = searchresults.getLineItemValue('item',
							'item', s);
					var vItem = nlapiGetCurrentLineItemValue('item', 'item');
					// alert(vLineno);
					if ((vLineno == lineNo) && (vOldItem != vItem)) {
						var Itemid = searchresults.getLineItemValue('item',
								'item', s);
						alert("Order is being processed in Main Warehouse. Do not make Item changes, Contact Warehouse Manager.");
						nlapiSetCurrentLineItemValue('item', 'item', vOldItem,
								false);
						return false;
					}

				}
			}

		}
		if(name == 'isclosed'){
			
			//var itemid = itemId=nlapiGetCurrentLineItemValue('item','item');
			var lineNo = nlapiGetCurrentLineItemValue('item','line');
			var Poid = nlapiGetFieldValue('id');
			
			var isClosed=nlapiGetCurrentLineItemValue('item','isclosed');
			
			if(isClosed == 'T'){
				
				var transactionFilters = new Array();
				transactionFilters[0] = new nlobjSearchFilter('custrecord_orderlinedetails_ebiz_ord_no', null, 'equalto', Poid);
				transactionFilters[1] = new nlobjSearchFilter('custrecord_orderlinedetails_orderline_no', null, 'equalto', lineNo);

				var transactionColumns = new Array();
				transactionColumns[0] = new nlobjSearchColumn('custrecord_orderlinedetails_ebiz_sku_no');
				transactionColumns[1] = new nlobjSearchColumn('custrecord_orderlinedetails_orderline_no');

				var transactionSearchresults = nlapiSearchRecord('customrecord_ebiznet_order_line_details', null, transactionFilters, transactionColumns);
				
				if(transactionSearchresults!=null && transactionSearchresults!=''){
					alert('The Selected Line# is Processing, So cannot Close it.');
					nlapiSetCurrentLineItemValue('item', 'isclosed','F', false);
					return false;
				}

				
			}
			
		}
		
	}

	if (name == 'item') 
	{
		var fromLocation = nlapiGetFieldValue('location');
		var toLocation= nlapiGetFieldValue('transferlocation');
		
		var lineNo = nlapiGetCurrentLineItemValue('item', 'line');
		var lineCnt = nlapiGetLineItemCount('item');
		var selecteditem = nlapiGetCurrentLineItemValue('item', 'item');

		//alert('selecteditem '+ selecteditem);
		if(selecteditem != null && selecteditem != '')
		{
			var searchresult = SetItemStatus("TransferOrder", selecteditem, fromLocation, toLocation);

			if (searchresult != null && searchresult != '') 
			{

				//alert('custcol_ebiznet_item_status '+ searchresult[0]);
				nlapiSetCurrentLineItemValue('item', 'custcol_ebiznet_item_status', searchresult[0], false);

			}

		}
	}

}
function getRecordDetails(soId, lineNo, condn) {
	var filter = new Array();

	filter.push(new nlobjSearchFilter('name', null, 'is', soId));
	filter.push(new nlobjSearchFilter('custrecord_ordline', null, 'equalto',
			lineNo));
	filter.push(new nlobjSearchFilter('custrecord_linestatus_flag', null,
			condn, [ 25 ]));

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ord_qty', null, 'sum');
	columns[1] = new nlobjSearchColumn('custrecord_ordline', null, 'group');
	columns[2] = new nlobjSearchColumn('name', null, 'group');

	var searchRecords = nlapiSearchRecord('customrecord_ebiznet_ordline', null,
			filter, columns);
	return searchRecords;
}

function DeleteRMALine(type) {

	var PoId = nlapiGetFieldValue('id');
	var lineNo = nlapiGetCurrentLineItemValue('item', 'line');
	// alert("lineNo;"+lineNo);
	if (lineNo != null && lineNo != "" && lineNo != "null" && PoId != null
			&& PoId != "" && PoId != "null") {

		// alert("lineNo:" + lineNo);
		var searchRecords = getRecordDetails(PoId, lineNo, 'noneof');
		// var RecExist = GetRecord(PoId,lineNo);

		 var getLineNo=nlapiGetFieldValue('custbody_ebiz_lines_deleted');
		var afterconcatinate;

		if (searchRecords != null && searchRecords != '') {

			alert("Order is being processed in Main Warehouse.So you cannot delete it.");
			return false;
		} else {
			if(getLineNo==null||getLineNo=='')
				afterconcatinate=lineNo.toString()+',';
			else
				afterconcatinate=getLineNo+lineNo.toString()+',';
			nlapiSetFieldValue('custbody_ebiz_lines_deleted', afterconcatinate, 'F', true);	

			/*
			 * if(getLineNo==null||getLineNo=='')
			 * afterconcatinate=lineNo.toString()+','; else
			 * afterconcatinate=getLineNo+lineNo.toString()+',';
			 * nlapiSetFieldValue('custbody_ebiz_lines_deleted',
			 * afterconcatinate, 'F', true);
			 */
			return true;
		}
	} else {
		return true;
	}

}
//Case# 201410031 starts
function onAddLine(type,name)
{
	var itemstatus=nlapiGetCurrentLineItemValue('item','custcol_ebiznet_item_status');
	//if(type == 'item')
	//{
	//alert('itemstatus'+itemstatus);
	var location = nlapiGetFieldValue('location');
	var itemStatusFilters = new Array();
	itemStatusFilters[0] = new nlobjSearchFilter('isinactive',null, 'is','F');
	itemStatusFilters[1] = new nlobjSearchFilter('custrecord_allowrcvskustatus',null, 'is','T');
	if(location != null && location != '')
		itemStatusFilters[2] = new nlobjSearchFilter('custrecord_ebizsiteskus',null, 'anyof',location);
	var itemStatusColumns = new Array();
	itemStatusColumns[0] = new nlobjSearchColumn('internalid');
	itemStatusColumns[1] = new nlobjSearchColumn('name');
	itemStatusColumns[0].setSort();
	itemStatusColumns[1].setSort();
	var itemStatusSearchResult = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, itemStatusFilters, itemStatusColumns);
	if (itemStatusSearchResult != null){
		var errorflag='F';
		for (var i = 0; i < itemStatusSearchResult.length; i++) 
		{		
			if(itemstatus ==itemStatusSearchResult[i].getId())
			{
				errorflag='T';
			}
		}
		if(errorflag=='F'){
			alert('Item Status not match with From Location');
			return false;
		}
	}
	//}
	return true;
}
//Case# 201410031 ends
//Case# 201410420 starts
function onSave(type)
{
	var toId = nlapiGetFieldValue('id');
	if (toId != null && toId != '' && toId >= 1) {
	var filter1 = new Array();
	filter1.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof',toId));
	filter1.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3]));
	filter1.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'noneof', [29,32]));

	var search1 = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filter1, null);
	/*if(search1!=null)
		{
		alert("Order is being processed. Any changes are not allowed");
		return false;
	
		}*/
	}
	
	var ordertype=nlapiGetFieldText('custbody_nswmssoordertype');
	var vloc1=nlapiGetFieldValue('location');
	//alert(receipttype);
	//alert(vloc1);
	var rcfilters=new Array();
var itemlevelshipping=nlapiGetFieldValue('ismultishipto');
	if(itemlevelshipping==null)
		itemlevelshipping='F';
//	alert('item level shipping'+itemlevelshipping);
	if(itemlevelshipping!='T'){
		var shipCarrierMethod = nlapiGetFieldValue('shipmethod');
		if(shipCarrierMethod == null || shipCarrierMethod == ''){
			alert('Please select ship method');
			return false;
		}

}
	
	if(ordertype!=null && ordertype!='')
	{// case# 201412340
		rcfilters.push(new nlobjSearchFilter('name', null, 'is', ordertype));
		if(vloc1!=null && vloc1!='')
			rcfilters.push(new nlobjSearchFilter('custrecord_ebizsiteordtyp', null, 'anyof', vloc1));

		var rcsearchresults=nlapiSearchRecord('customrecord_ebiznet_order_type', null, rcfilters,null);
		if(rcsearchresults==null)
		{
			alert('Order type not matched with From location');
			return false;
		}
		else
		{
			return true;
		}
	}
	else
	{
		return true;
	}
}
