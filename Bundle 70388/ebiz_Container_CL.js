
/***************************************************************************
                      eBizNET
                  eBizNET Solutions Inc
****************************************************************************
*
*  $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Masters/Client/Attic/ebiz_Container_CL.js,v $
*  $Revision: 1.1.2.1 $
*  $Date: 2012/09/04 07:29:53 $
*  $Author: spendyala $
*  $Name: t_NSWMS_2012_2_1_30 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_Container_CL.js,v $
*  Revision 1.1.2.1  2012/09/04 07:29:53  spendyala
*  CASE201112/CR201113/LOG201121
*  New Client script for container table.
*
*
****************************************************************************/

function OnChange(type,name)
{
	if(name=='custrecord_containersku')
	{
		alert('This item field is only a reference field and can be used to customize cartonization by customers. It does not automatically restrict cartonization in any way.');
		return true;
	}
}