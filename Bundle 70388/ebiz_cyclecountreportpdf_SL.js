function cyclecountreportpdf(request, response) {
	if (request.getMethod() == 'GET') {

		var form = nlapiCreateForm('Resolve');
		
		var varCycleCountPlanNo = request
				.getParameter('custparam_cyclecountplan');
		var CycWhLocation = request
				.getParameter('custparam_cyclecountwhlocation');
		var Cycitem = request.getParameter('custparam_cyclecountitem')
		var CycbinLoc = request.getParameter('custparam_cyclecountbinloc');
		

		
		var filefound = nlapiLoadFile('Images/LOGOCOMP.jpg');
		if (filefound) {
			nlapiLogExecution('ERROR', 'Event', 'file;' + filefound.getId());
			var imageurl = filefound.getURL();
			// finalimageurl = url + imageurl;//+';';
			finalimageurl = imageurl;// +';';
			finalimageurl = finalimageurl.replace(/&/g, "&amp;");

		} else {
			nlapiLogExecution('ERROR', 'Event', 'No file;');
		}
		
		
		var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<head><macrolist><macro id='myfooter'></macro></macrolist></head><body  font-size=\"7\"  size=\"A4-landscape\"    padding-top=\" 0mm\"   footer='myfooter' footer-height='20mm'>\n";

		
		var strxml = "<table width='100%' >";
	
	

	strxml += "<tr ><td valign='middle' align='left'><img src='"
			+ finalimageurl
			+ "'></img></td><td valign='middle' align='left'  style='font-size:xx-large;'>";
	strxml += "Cycle Count Report ";
	strxml += "</td><td align='right'>&nbsp;</td></tr>";
	
	strxml +="<tr><td></td><td></td><td align='right' valign='middle' style='font-size:small'><p align='right'>Cycle Count Plan no#:" + varCycleCountPlanNo + "</p></td></tr>";
	//strxml +="<tr><td> 21 Dwight Place</td></tr>";
	//strxml +="<tr><td> Fairfield NJ</td></tr>";
	//strxml +="<tr><td> United States of America</td></tr>";
	strxml +="<tr><td></td></tr></table>";
	
	strxml += "<table  width='100%'>";
	strxml += "<tr style=\"font-weight:bold;background-color:gray;color:white;\">";

	strxml += "<td width='7%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";
	strxml += " Location";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

	strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";
	strxml += "Expected Item";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
	
	strxml += "<td width='15%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";
	strxml += "Actual Item";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

	strxml += "<td width='7%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";
	strxml += " Expected Qty";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

	strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";
	strxml += "Actual Qty";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
	
	strxml += "<td width='18%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";
	strxml += "Qty Variance";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

	strxml += "<td width='18%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";
	strxml += "$ Variance";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

	strxml += "<td width='10%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";
	strxml += "Expected LP";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

	strxml += "<td width='10%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";// uncommented
	strxml += "Actual LP";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

	strxml += "<td width='10%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";
	strxml += "Expected LOT#";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

	strxml += "<td width='15%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";
	strxml += "Actual LOT#";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

	strxml += "<td width='15%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";
	strxml += "Expected Item Status";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

	strxml += "<td width='15%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";// uncommented
	strxml += "Actual Item Status";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";

	strxml += "<td width='9%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";
	strxml += "Current Inventory";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
	
	strxml += "<td width='12%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";
	strxml += "Qty to be adjusted";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
	
	strxml += "<td width='12%' style='border-width: 1px; border-color: #000000; font-size:medium;'>";
	strxml += "Status";
	strxml += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
	
	
	
	
	var varCCplanNo = request.getParameter("custpage_cyclecountplan");
	var isplanclosed='F';

	
	try {
		
		
		if(varCCplanNo!=null && varCCplanNo!='')
		{
			var fields = ['custrecord_cyccplan_close', 'custrecord_ebiz_include_emploc'];
			var columns = nlapiLookupField('customrecord_ebiznet_cylc_createplan', varCCplanNo, fields);
			isplanclosed = columns.custrecord_cyccplan_close;
		}
		nlapiLogExecution('ERROR', 'isplanclosed', isplanclosed);
	//	if(isplanclosed=='F'){
			

			
				var hiddenfieldshowvalue;
				var hiddenfieldshow;
				hiddenfieldshow=request.getParameter('custparam_hiddenfieldshow');
				hiddenfieldshow=('F');
				if(request.getParameter('custparam_hiddenfieldshow') != null && request.getParameter('custparam_hiddenfieldshow') != '')
				{
					hiddenfieldshowvalue = request.getParameter('custparam_hiddenfieldshow');
				}

				hiddenfieldshow=hiddenfieldshowvalue;

				nlapiLogExecution('ERROR', 'custparam_hiddenfieldshow', request.getParameter('custparam_hiddenfieldshow'));
				nlapiLogExecution('ERROR', 'hiddenfieldshowvalue', hiddenfieldshowvalue);
				nlapiLogExecution('ERROR', 'hiddenfieldshow', hiddenfieldshow);

				var hiddenCycleCountPlan;
				hiddenCycleCountPlan=request.getParameter('custparam_cyclecountplan');
				nlapiLogExecution('ERROR', 'hiddenCycleCountPlan', hiddenCycleCountPlan);
				var hiddenQueryParams;
				//var hiddenfieldselectpage;
				//hiddenfieldselectpage.setDefaultValue('F');

				//var k = form.getField('restype').getSelectOptions();

				//var chkconfirmtohost=form.addField('custpage_confirmtohost', 'checkbox','Update GL#').setDefaultValue('T');

				//addFieldsToForm(form);
				nlapiLogExecution('ERROR', 'custparam_restypeflag', request.getParameter('custparam_restypeflag'));
				var orderList = getOrdersForCycleCount(request,0,form);

				/*if(((request.getParameter('custparam_restypeflag')=='cyclecountsa' && hiddenfieldshowvalue != 'D') && (request.getParameter('custparam_restypeflag')=='cyclecountsa' && hiddenfieldshowvalue != 'ND'))|| hiddenfieldshowvalue == 'ALL')
				{*/
					nlapiLogExecution('ERROR', 'orderList.lenght tst in all', orderList.length);
					if(orderList != null && orderList.length > 0){
						//setPagingForSublist(orderList,form);
						
						

						
						
						//displaysublist(form,orderList,hiddenfieldshowvalue);
						
						

						var loc, locText, expsku, expskutext, actskutext, actsku, expqty, actqty, explp, actlp, expskustatus, actskustatus;
						var actskustatusvalue, planno, name, opentaskid, intid, siteis, actlot, explot, expskustatusvalue, expserialno, actserialno;
						var dollarvariance;
						var status;

						
						
						 
						var i = 0;
						searchResults=orderList;
						if (searchResults != null && searchResults != "") {
							var orderListArray = new Array();
							for (k = 0; k < searchResults.length; k++) {
								// nlapiLogExecution('ERROR', 'searchResults[k]', searchResults[k]);
								var ordsearchresult = searchResults[k];

								if (ordsearchresult != null) {
									// nlapiLogExecution('ERROR', 'ordsearchresult.length new tst',
									// ordsearchresult.length);
									for ( var j = 0; j < ordsearchresult.length; j++) {
										orderListArray[orderListArray.length] = ordsearchresult[j];
									}
								}
							}

							nlapiLogExecution('ERROR', 'searchresults.length in new sublist bind',
									searchResults.length);
							var ShowAll, ShowDis, ShowNonDis;
							if((request.getParameter('custparam_hiddenfieldshow')==null && request.getParameter('custparam_hiddenfieldshow')=='') ||request.getParameter('custparam_hiddenfieldshow')=='undefined')
								{
								ShowDis='T';
								
								}
							
							
							if (request.getParameter('custparam_restypeflag') == 'cyclecountsa'
									&& (request.getParameter('custparam_hiddenfieldshow') == '' || request
											.getParameter('custparam_hiddenfieldshow') == null)) {
								nlapiLogExecution('ERROR', 'ShowAll if', ShowAll);
								// vsa = request.getParameter('restypeflag');
								ShowAll = 'T';
							} else if (request.getParameter('custparam_hiddenfieldshow') == 'ALL') {
								nlapiLogExecution('ERROR', 'ShowAll else', ShowAll);
								ShowAll = 'T';
							}

							if (request.getParameter('custparam_restypeflag') == 'cyclecountsd'
									&& (request.getParameter('custparam_hiddenfieldshow') == '' || request
											.getParameter('custparam_hiddenfieldshow') == null)) {
								nlapiLogExecution('ERROR', 'ShowDis if', ShowDis);
								// vsa = request.getParameter('restypeflag');
								ShowDis = 'T';
							} else if (request.getParameter('custparam_hiddenfieldshow') == 'D') {
								nlapiLogExecution('ERROR', 'ShowDis else', ShowDis);
								ShowDis = 'T';
							}

							if (request.getParameter('custparam_restypeflag') == 'cyclecountsnd'
									&& (request.getParameter('custparam_hiddenfieldshow') == '' || request
											.getParameter('custparam_hiddenfieldshow') == null)) {
								nlapiLogExecution('ERROR', 'ShowNonDis if', ShowNonDis);
								// vsa = request.getParameter('restypeflag');
								ShowNonDis = 'T';
							} else if (request.getParameter('custparam_hiddenfieldshow') == 'ND') {
								nlapiLogExecution('ERROR', 'ShowNonDis else', ShowNonDis);
								ShowNonDis = 'T';
							}

							nlapiLogExecution('ERROR', 'ShowAll in bind', ShowAll);
							nlapiLogExecution('ERROR', 'ShowDis in bind', ShowDis);
							nlapiLogExecution('ERROR', 'ShowNonDis in bind', ShowNonDis);

							for ( var m = 0; m < orderListArray.length; m++) {
								nlapiLogExecution('ERROR', 'm', m);

								loc = orderListArray[m].getValue('custrecord_cycact_beg_loc');
								locText = orderListArray[m].getText('custrecord_cycact_beg_loc');
								expsku = orderListArray[m]
										.getValue('custrecord_cyclecount_exp_ebiz_sku_no');
								expskutext = orderListArray[m].getText('custrecord_cyclesku');
								// actsku = orderListArray[m].getValue('custrecord_cycleact_sku');
								actskutext = orderListArray[m].getText('custrecord_cycleact_sku');
								actsku = orderListArray[m]
										.getValue('custrecord_cyclecount_act_ebiz_sku_no');
								expqty = orderListArray[m].getValue('custrecord_cycleexp_qty');
								actqty = orderListArray[m].getValue('custrecord_cycle_act_qty');
								explp = orderListArray[m].getValue('custrecord_cyclelpno');
								actlp = orderListArray[m].getValue('custrecord_cycact_lpno');

								expserialno = orderListArray[m]
										.getValue('custrecord_cycle_expserialno');
								actserialno = orderListArray[m]
										.getValue('custrecord_cycle_serialno');

								nlapiLogExecution('ERROR', 'expserialno in show discrepancy',
										expserialno);
								nlapiLogExecution('ERROR', 'actserialno in show discrepancy',
										actserialno);

								expskustatus = orderListArray[m]
										.getText('custrecord_expcyclesku_status');
								expskustatusvalue = orderListArray[m]
										.getValue('custrecord_expcyclesku_status');
								actskustatus = orderListArray[m]
										.getText('custrecord_cyclesku_status');
								actskustatusvalue = orderListArray[m]
										.getValue('custrecord_cyclesku_status');

								if (actskustatus == null || actskustatus == "")
									actskustatus = expskustatus;
								if (actskustatusvalue == null || actskustatusvalue == "")
									actskustatusvalue = expskustatusvalue;

								nlapiLogExecution('ERROR', 'actskustatus in show discrepancy',
										actskustatus);
								nlapiLogExecution('ERROR', 'expskustatus in show discrepancy',
										expskustatus);

								planno = orderListArray[m].getValue('custrecord_cycle_count_plan');
								name = orderListArray[m].getValue('custrecord_cycle_count_plan');
								opentaskid = orderListArray[m].getValue('custrecord_opentaskid');
								invtid = orderListArray[m].getValue('custrecord_invtid');
								siteid = orderListArray[m].getValue('custrecord_cyclesite_id');
								actlot = orderListArray[m].getValue('custrecord_cycleabatch_no');
								explot = orderListArray[m].getValue('custrecord_expcycleabatch_no');
								status = orderListArray[m].getText('custrecord_cyclestatus_flag');

								// Code Added By Ganapathi on 26th Nov 2012
								var filters = new Array();
								if (actsku != null && actsku != '')
									filters.push(new nlobjSearchFilter('internalid', null, 'is',
											actsku));
								filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

								var columns = new Array();
								columns[0] = new nlobjSearchColumn('cost');
								columns[1] = new nlobjSearchColumn('averagecost');
								columns[2] = new nlobjSearchColumn('itemid');

								var itemdetails = new nlapiSearchRecord('item', null, filters,
										columns);
								if (itemdetails != null) {
									vItemname = itemdetails[0].getValue('itemid');
									vCost = itemdetails[0].getValue('cost');
									nlapiLogExecution('ERROR', 'vCost', vCost);
									vAvgCost = itemdetails[0].getValue('averagecost');
									nlapiLogExecution('ERROR', 'Average Cost', vAvgCost);
								}

								if (actqty == null || actqty == "") {
									nlapiLogExecution('ERROR', 'actqty:', actqty);
									actqty = 0;
								}

								if (expqty == null || expqty == "") {
									nlapiLogExecution('ERROR', 'expqty: ', expqty);
									expqty = 0;
								}

								dollarvariance = (parseFloat(actqty) - parseFloat(expqty))
										* vAvgCost;

								nlapiLogExecution('ERROR', 'dollarvariance', dollarvariance);

								if (isNaN(dollarvariance) || dollarvariance == null
										|| dollarvariance == "") {
									nlapiLogExecution('ERROR', 'dollarvariance: ', dollarvariance);
									dollarvariance = 0;
								} else {
									dollarvariance = Math.round(dollarvariance, 2);
								}

								nlapiLogExecution('ERROR', 'planno', planno);
								nlapiLogExecution('ERROR','actserialno',actserialno);
								nlapiLogExecution('ERROR','expserialno',expserialno);
								var varianceinExpserialNo = '';
								var varianceinActserialNo = '';
								if (actserialno != null && actserialno != '' && expserialno != null
										&& expserialno != '') {
									actserialno = actserialno.trim();
									expserialno = expserialno.trim();
									
									if (expserialno != null && expserialno != '')
										var getexpserialArr = expserialno.split(',');

									if (actserialno != null && actserialno != '')
										var getactserialArr = actserialno.split(',');

									if (getexpserialArr != null && getexpserialArr != '') {
										for ( var p = 0; p < getexpserialArr.length; p++) {
											if (actserialno != null && actserialno != '') {
												if (actserialno.indexOf(getexpserialArr[p]) == -1)// not
												// found
												// temSeriIdArr
												{
													if (varianceinExpserialNo == "") {
														varianceinExpserialNo = getexpserialArr[p];
													} else {
														varianceinExpserialNo = varianceinExpserialNo
																+ "," + getexpserialArr[p];
													}
												}
											}
										}
									}
									nlapiLogExecution('ERROR', 'varianceinExpserialNo',
											varianceinExpserialNo);

									if (getactserialArr != null && getactserialArr != '') {
										for ( var q = 0; q < getactserialArr.length; q++) {
											if (getexpserialArr != null && getexpserialArr != '') {

												if (getexpserialArr.indexOf(getactserialArr[q]) == -1)// not
												// found
												// temSeriIdArr
												{
													if (varianceinActserialNo == "") {
														varianceinActserialNo = getactserialArr[q];
													} else {
														varianceinActserialNo = varianceinActserialNo
																+ "," + getactserialArr[q];
													}
												}
											}
										}
									}
									nlapiLogExecution('ERROR', 'varianceinActserialNo',
											varianceinActserialNo);
								}

							/*	
								 * nlapiLogExecution('ERROR', 'expqty', expqty);
								 * nlapiLogExecution('ERROR', 'actqty', actqty);
								 * nlapiLogExecution('ERROR', 'expsku', expsku);
								 * nlapiLogExecution('ERROR', 'actsku', actsku);
								 * nlapiLogExecution('ERROR', 'explot', explot);
								 * nlapiLogExecution('ERROR', 'actlot', actlot);
								 * nlapiLogExecution('ERROR', 'explp', explp);
								 * nlapiLogExecution('ERROR', 'actlp', actlp);
								 * nlapiLogExecution('ERROR', 'expskustatusvalue',
								 * expskustatusvalue); nlapiLogExecution('ERROR',
								 * 'actskustatusvalue', actskustatusvalue);
								 */

								nlapiLogExecution('ERROR', 'orderListArray[m].getId()',
										orderListArray[m].getId());
								// var serialnourl = nlapiResolveURL('SUITELET',
								// 'customscript_lprelated_serial', 'customdeploy_lprelated_serial')
								// + "&custparam_item=" + actsku+ "&custparam_planno=" + planno;
								var serialnourl = nlapiResolveURL('SUITELET',
										'customscript_lprelated_serial',
										'customdeploy_lprelated_serial')
										+ "&custparam_expserial="
										+ null
										+ "&custparam_actserial="
										+ null
										+ "&custparam_expskustatus="
										+ expskustatusvalue
										+ "&custparam_actskustatus="
										+ actskustatusvalue
										+ "&custparam_recid=" + orderListArray[m].getId();
								// case 20124074 start
								var Invturl = nlapiResolveURL('SUITELET',
										'customscript_ebiz_resolve_expitemurl',
										'customdeploy_ebiz_resolve_expitemurl_di')
										+ "&custparam_expsku="
										+ actsku
										+ "&custparam_planno="
										+ planno;
								// end

								var invtfilters = new Array();
								invtfilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null,
										'anyof', loc));
								invtfilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null,
										'anyof', actsku));
								invtfilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null,
										'is', actlp));
								invtfilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
								
								var invtcolumns = new Array();
								invtcolumns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
								invtcolumns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_loc');
								invtcolumns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
								invtcolumns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
								invtcolumns[4] = new nlobjSearchColumn('custrecord_ebiz_avl_qty');
								invtcolumns[0].setSort();
								var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv',
										null, invtfilters, invtcolumns);
								if (invtsearchresults != null && invtsearchresults != '')
									var	currentinvt = invtsearchresults[0]
											.getValue('custrecord_ebiz_avl_qty');

								if (currentinvt == 'undefined' || currentinvt == null || currentinvt == ''
										|| isNaN(currentinvt)) {
									currentinvt = 0;
								}
								
								// if((request.getParameter('restypeflag')=='cyclecountsd' ||
								// hiddenfieldshowvalue == 'D') && (expqty != actqty || expsku !=
								// actsku || expserialno != actserialno || explot != actlot || explp
								// != actlp ))
							/*	if ((ShowDis == 'T')
										&& (expqty != actqty || expsku != actsku
												|| explot != actlot || explp != actlp
												|| expskustatusvalue != actskustatusvalue
												|| varianceinExpserialNo != "" || varianceinActserialNo != "")) {
									i = i + 1;
									nlapiLogExecution('ERROR',
											'request.getParameter(restypeflag) in if binding',
											request.getParameter('custparam_restypeflag'));
									nlapiLogExecution('ERROR',
											'hiddenfieldshowvalue in if binding',
											hiddenfieldshowvalue);
									nlapiLogExecution('ERROR', 'orderListArray[m].getId() inside',
											orderListArray[m].getId());

									
									 * form.getSubList('custpage_items').setLineItemValue('custpage_locationtext',
									 * i, locText);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_location',
									 * i, loc);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expsku',
									 * i, expsku);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expskutext',
									 * i, expskutext);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actsku',
									 * i, actsku);
									 * //form.getSubList('custpage_items').setLineItemValue('custpage_actskutext',
									 * i, actskutext); //case 20124074 start
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actskutextlink',
									 * i, "<a href=\"#\" onclick=\"javascript:window.open('" +
									 * Invturl + "');\" >" + actskutext + "</a>"); //end
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expqty',
									 * i, expqty);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actqty',
									 * i, actqty);
									 * 
									 * form.getSubList('custpage_items').setLineItemValue('custpage_dollarvariance',
									 * i, dollarvariance);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_qtyvariance',
									 * i, parseFloat(actqty) - parseFloat(expqty));
									 * form.getSubList('custpage_items').setLineItemValue('custpage_qtytobeadjust',
									 * i, parseFloat(actqty) - parseFloat(expqty));
									 * form.getSubList('custpage_items').setLineItemValue('custpage_explp',
									 * i, explp);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actlp',
									 * i, actlp);
									 * 
									 * //form.getSubList('custpage_items').setLineItemValue('custpage_expserial',
									 * i, expserialno);
									 * //form.getSubList('custpage_items').setLineItemValue('custpage_actserial',
									 * i, actserialno);
									 * 
									 * 
									 * 
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expbatch',
									 * i, explot);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actbatch',
									 * i, actlot);
									 * 
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expskustatus',
									 * i, expskustatus);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expskustatusvalue',
									 * i, expskustatusvalue);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actskustatus',
									 * i, actskustatus);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actskustatusvalue',
									 * i, actskustatusvalue);
									 * 
									 * form.getSubList('custpage_items').setLineItemValue('custpage_id',
									 * i, orderListArray[m].getId());
									 * form.getSubList('custpage_items').setLineItemValue('custpage_planno',
									 * i, planno);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_name',
									 * i, name);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_opentaskid',
									 * i, opentaskid);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_invtid',
									 * i, invtid);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_site',
									 * i, siteid);
									 

									
									
									
									

									strxml += "<tr><td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += locText;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += expskutext;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actskutext;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += expqty;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actqty;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += parseFloat(actqty) - parseFloat(expqty);
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += dollarvariance;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += explp;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actlp;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += explot;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actlot;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += expskustatus;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actskustatus;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += currentinvt;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += parseFloat(actqty) - parseFloat(expqty);
									strxml += "</td>";
									
									
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += status;
									strxml += "</td></tr>";
									
									
									
									
									
									
								}*/

								// else if((request.getParameter('restypeflag')=='cyclecountsnd' ||
								// hiddenfieldshowvalue == 'ND') && (expqty == actqty && expsku ==
								// actsku && expserialno == actserialno && explot == actlot && explp
								// == actlp ))
							/*	else if ((ShowNonDis == 'T')
										&& (expqty == actqty && expsku == actsku
												&& explot == actlot && explp == actlp
												&& expskustatusvalue == actskustatusvalue
												&& varianceinExpserialNo == "" && varianceinActserialNo == "")) {

									i = i + 1;
									nlapiLogExecution('ERROR',
											'request.getParameter(restypeflag) in else binding',
											request.getParameter('custparam_restypeflag'));
									nlapiLogExecution('ERROR',
											'hiddenfieldshowvalue in else binding',
											hiddenfieldshowvalue);
									nlapiLogExecution('ERROR',
											'orderListArray[m].getId() else inside',
											orderListArray[m].getId());

									
									 * form.getSubList('custpage_items').setLineItemValue('custpage_locationtext',
									 * i, locText);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_location',
									 * i, loc);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expsku',
									 * i, expsku);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expskutext',
									 * i, expskutext);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actsku',
									 * i, actsku);
									 * //form.getSubList('custpage_items').setLineItemValue('custpage_actskutext',
									 * i, actskutext);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actskutextlink',
									 * i, "<a href=\"#\" onclick=\"javascript:window.open('" +
									 * serialnourl + "');\" >" + actskutext + "</a>");
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expqty',
									 * i, expqty);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actqty',
									 * i, actqty);
									 * 
									 * form.getSubList('custpage_items').setLineItemValue('custpage_dollarvariance',
									 * i, dollarvariance);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_qtyvariance',
									 * i, parseFloat(actqty) - parseFloat(expqty));
									 * form.getSubList('custpage_items').setLineItemValue('custpage_qtytobeadjust',
									 * i, parseFloat(actqty) - parseFloat(expqty));
									 * form.getSubList('custpage_items').setLineItemValue('custpage_explp',
									 * i, explp);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actlp',
									 * i, actlp);
									 * 
									 * //form.getSubList('custpage_items').setLineItemValue('custpage_expserial',
									 * i, expserialno);
									 * //form.getSubList('custpage_items').setLineItemValue('custpage_actserial',
									 * i, actserialno);
									 * 
									 * 
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expbatch',
									 * i, explot);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actbatch',
									 * i, actlot);
									 * 
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expskustatus',
									 * i, expskustatus);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expskustatusvalue',
									 * i, expskustatusvalue);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actskustatus',
									 * i, actskustatus);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actskustatusvalue',
									 * i, actskustatusvalue);
									 * 
									 * form.getSubList('custpage_items').setLineItemValue('custpage_id',
									 * i, orderListArray[m].getId());
									 * form.getSubList('custpage_items').setLineItemValue('custpage_planno',
									 * i, planno);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_name',
									 * i, name);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_opentaskid',
									 * i, opentaskid);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_invtid',
									 * i, invtid);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_site',
									 * i, siteid);
									 
									
									
									
									strxml += "<tr><td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += locText;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += expskutext;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actskutext;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += expqty;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actqty;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += parseFloat(actqty) - parseFloat(expqty);
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += dollarvariance;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += explp;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actlp;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += explot;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actlot;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += expskustatus;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actskustatus;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += currentinvt;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += parseFloat(actqty) - parseFloat(expqty);
									strxml += "</td></tr>";
									
									
								}*/
								
						//		else if ((ShowAll == 'T')) {

									i = i + 1;
									nlapiLogExecution('ERROR',
											'request.getParameter(restypeflag) in else binding',
											request.getParameter('custparam_restypeflag'));
									nlapiLogExecution('ERROR',
											'hiddenfieldshowvalue in else binding',
											hiddenfieldshowvalue);
									nlapiLogExecution('ERROR',
											'orderListArray[m].getId() else inside',
											orderListArray[m].getId());

							/*		
									 * form.getSubList('custpage_items').setLineItemValue('custpage_locationtext',
									 * i, locText);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_location',
									 * i, loc);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expsku',
									 * i, expsku);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expskutext',
									 * i, expskutext);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actsku',
									 * i, actsku);
									 * //form.getSubList('custpage_items').setLineItemValue('custpage_actskutext',
									 * i, actskutext);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actskutextlink',
									 * i, "<a href=\"#\" onclick=\"javascript:window.open('" +
									 * serialnourl + "');\" >" + actskutext + "</a>");
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expqty',
									 * i, expqty);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actqty',
									 * i, actqty);
									 * 
									 * form.getSubList('custpage_items').setLineItemValue('custpage_dollarvariance',
									 * i, dollarvariance);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_qtyvariance',
									 * i, parseFloat(actqty) - parseFloat(expqty));
									 * form.getSubList('custpage_items').setLineItemValue('custpage_qtytobeadjust',
									 * i, parseFloat(actqty) - parseFloat(expqty));
									 * form.getSubList('custpage_items').setLineItemValue('custpage_explp',
									 * i, explp);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actlp',
									 * i, actlp);
									 * 
									 * //form.getSubList('custpage_items').setLineItemValue('custpage_expserial',
									 * i, expserialno);
									 * //form.getSubList('custpage_items').setLineItemValue('custpage_actserial',
									 * i, actserialno);
									 * 
									 * 
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expbatch',
									 * i, explot);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actbatch',
									 * i, actlot);
									 * 
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expskustatus',
									 * i, expskustatus);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expskustatusvalue',
									 * i, expskustatusvalue);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actskustatus',
									 * i, actskustatus);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actskustatusvalue',
									 * i, actskustatusvalue);
									 * 
									 * form.getSubList('custpage_items').setLineItemValue('custpage_id',
									 * i, orderListArray[m].getId());
									 * form.getSubList('custpage_items').setLineItemValue('custpage_planno',
									 * i, planno);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_name',
									 * i, name);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_opentaskid',
									 * i, opentaskid);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_invtid',
									 * i, invtid);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_site',
									 * i, siteid);
									 */
									
									
									
									strxml += "<tr><td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += locText;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += expskutext;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actskutext;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += expqty;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actqty;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += parseFloat(actqty) - parseFloat(expqty);
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += dollarvariance;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += explp;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actlp;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += explot;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actlot;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += expskustatus;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actskustatus;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += currentinvt;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += parseFloat(actqty) - parseFloat(expqty);
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += status;
									strxml += "</td></tr>";
									
									
							//	}

								
								
							}
						}

						
						
						
					}
			//	}
				if((request.getParameter('custparam_restypeflag')=='cyclecountsd' || hiddenfieldshowvalue == 'D') || (request.getParameter('custparam_restypeflag')=='cyclecountsnd' || hiddenfieldshowvalue == 'ND'))
				{
					nlapiLogExecution('ERROR', 'orderList.lenght tst in D or ND', orderList.length);
					if(orderList != null && orderList.length > 0){
						
					
						//displaysublist(form,orderList,hiddenfieldshowvalue);
						
						

						var loc, locText, expsku, expskutext, actskutext, actsku, expqty, actqty, explp, actlp, expskustatus, actskustatus;
						var actskustatusvalue, planno, name, opentaskid, intid, siteis, actlot, explot, expskustatusvalue, expserialno, actserialno;
						var dollarvariance;

						
						
						 
						var i = 0;
						searchResults=orderList;
						if (searchResults != null && searchResults != "") {
							var orderListArray = new Array();
							for (k = 0; k < searchResults.length; k++) {
								// nlapiLogExecution('ERROR', 'searchResults[k]', searchResults[k]);
								var ordsearchresult = searchResults[k];

								if (ordsearchresult != null) {
									// nlapiLogExecution('ERROR', 'ordsearchresult.length new tst',
									// ordsearchresult.length);
									for ( var j = 0; j < ordsearchresult.length; j++) {
										orderListArray[orderListArray.length] = ordsearchresult[j];
									}
								}
							}

							nlapiLogExecution('ERROR', 'searchresults.length in new sublist bind',
									searchResults.length);
							var ShowAll, ShowDis, ShowNonDis;
							if((request.getParameter('custparam_hiddenfieldshow')==null && request.getParameter('custparam_hiddenfieldshow')=='') ||request.getParameter('custparam_hiddenfieldshow')=='undefined')
								{
								ShowDis='T';
								
								}
							
							
							if (request.getParameter('custparam_restypeflag') == 'cyclecountsa'
									&& (request.getParameter('custparam_hiddenfieldshow') == '' || request
											.getParameter('custparam_hiddenfieldshow') == null)) {
								nlapiLogExecution('ERROR', 'ShowAll if', ShowAll);
								// vsa = request.getParameter('restypeflag');
								ShowAll = 'T';
							} else if (request.getParameter('custparam_hiddenfieldshow') == 'ALL') {
								nlapiLogExecution('ERROR', 'ShowAll else', ShowAll);
								ShowAll = 'T';
							}

							if (request.getParameter('custparam_restypeflag') == 'cyclecountsd'
									&& (request.getParameter('custparam_hiddenfieldshow') == '' || request
											.getParameter('custparam_hiddenfieldshow') == null)) {
								nlapiLogExecution('ERROR', 'ShowDis if', ShowDis);
								// vsa = request.getParameter('restypeflag');
								ShowDis = 'T';
							} else if (request.getParameter('custparam_hiddenfieldshow') == 'D') {
								nlapiLogExecution('ERROR', 'ShowDis else', ShowDis);
								ShowDis = 'T';
							}

							if (request.getParameter('custparam_restypeflag') == 'cyclecountsnd'
									&& (request.getParameter('custparam_hiddenfieldshow') == '' || request
											.getParameter('custparam_hiddenfieldshow') == null)) {
								nlapiLogExecution('ERROR', 'ShowNonDis if', ShowNonDis);
								// vsa = request.getParameter('restypeflag');
								ShowNonDis = 'T';
							} else if (request.getParameter('custparam_hiddenfieldshow') == 'ND') {
								nlapiLogExecution('ERROR', 'ShowNonDis else', ShowNonDis);
								ShowNonDis = 'T';
							}

							nlapiLogExecution('ERROR', 'ShowAll in bind', ShowAll);
							nlapiLogExecution('ERROR', 'ShowDis in bind', ShowDis);
							nlapiLogExecution('ERROR', 'ShowNonDis in bind', ShowNonDis);

							for ( var m = 0; m < orderListArray.length; m++) {
								nlapiLogExecution('ERROR', 'm', m);

								loc = orderListArray[m].getValue('custrecord_cycact_beg_loc');
								locText = orderListArray[m].getText('custrecord_cycact_beg_loc');
								expsku = orderListArray[m]
										.getValue('custrecord_cyclecount_exp_ebiz_sku_no');
								expskutext = orderListArray[m].getText('custrecord_cyclesku');
								// actsku = orderListArray[m].getValue('custrecord_cycleact_sku');
								actskutext = orderListArray[m].getText('custrecord_cycleact_sku');
								actsku = orderListArray[m]
										.getValue('custrecord_cyclecount_act_ebiz_sku_no');
								expqty = orderListArray[m].getValue('custrecord_cycleexp_qty');
								actqty = orderListArray[m].getValue('custrecord_cycle_act_qty');
								explp = orderListArray[m].getValue('custrecord_cyclelpno');
								actlp = orderListArray[m].getValue('custrecord_cycact_lpno');

								expserialno = orderListArray[m]
										.getValue('custrecord_cycle_expserialno');
								actserialno = orderListArray[m]
										.getValue('custrecord_cycle_serialno');

								nlapiLogExecution('ERROR', 'expserialno in show discrepancy',
										expserialno);
								nlapiLogExecution('ERROR', 'actserialno in show discrepancy',
										actserialno);

								expskustatus = orderListArray[m]
										.getText('custrecord_expcyclesku_status');
								expskustatusvalue = orderListArray[m]
										.getValue('custrecord_expcyclesku_status');
								actskustatus = orderListArray[m]
										.getText('custrecord_cyclesku_status');
								actskustatusvalue = orderListArray[m]
										.getValue('custrecord_cyclesku_status');

								if (actskustatus == null || actskustatus == "")
									actskustatus = expskustatus;
								if (actskustatusvalue == null || actskustatusvalue == "")
									actskustatusvalue = expskustatusvalue;

								nlapiLogExecution('ERROR', 'actskustatus in show discrepancy',
										actskustatus);
								nlapiLogExecution('ERROR', 'expskustatus in show discrepancy',
										expskustatus);

								planno = orderListArray[m].getValue('custrecord_cycle_count_plan');
								name = orderListArray[m].getValue('custrecord_cycle_count_plan');
								opentaskid = orderListArray[m].getValue('custrecord_opentaskid');
								invtid = orderListArray[m].getValue('custrecord_invtid');
								siteid = orderListArray[m].getValue('custrecord_cyclesite_id');
								actlot = orderListArray[m].getValue('custrecord_cycleabatch_no');
								explot = orderListArray[m].getValue('custrecord_expcycleabatch_no');

								// Code Added By Ganapathi on 26th Nov 2012
								var filters = new Array();
								if (actsku != null && actsku != '')
									filters.push(new nlobjSearchFilter('internalid', null, 'is',
											actsku));
								filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

								var columns = new Array();
								columns[0] = new nlobjSearchColumn('cost');
								columns[1] = new nlobjSearchColumn('averagecost');
								columns[2] = new nlobjSearchColumn('itemid');

								var itemdetails = new nlapiSearchRecord('item', null, filters,
										columns);
								if (itemdetails != null) {
									vItemname = itemdetails[0].getValue('itemid');
									vCost = itemdetails[0].getValue('cost');
									nlapiLogExecution('ERROR', 'vCost', vCost);
									vAvgCost = itemdetails[0].getValue('averagecost');
									nlapiLogExecution('ERROR', 'Average Cost', vAvgCost);
								}

								if (actqty == null || actqty == "") {
									nlapiLogExecution('ERROR', 'actqty:', actqty);
									actqty = 0;
								}

								if (expqty == null || expqty == "") {
									nlapiLogExecution('ERROR', 'expqty: ', expqty);
									expqty = 0;
								}

								dollarvariance = (parseFloat(actqty) - parseFloat(expqty))
										* vAvgCost;

								nlapiLogExecution('ERROR', 'dollarvariance', dollarvariance);

								if (isNaN(dollarvariance) || dollarvariance == null
										|| dollarvariance == "") {
									nlapiLogExecution('ERROR', 'dollarvariance: ', dollarvariance);
									dollarvariance = 0;
								} else {
									dollarvariance = Math.round(dollarvariance, 2);
								}

								nlapiLogExecution('ERROR', 'planno', planno);
								nlapiLogExecution('ERROR','actserialno',actserialno);
								nlapiLogExecution('ERROR','expserialno',expserialno);
								var varianceinExpserialNo = '';
								var varianceinActserialNo = '';
								if (actserialno != null && actserialno != '' && expserialno != null
										&& expserialno != '') {
									actserialno = actserialno.trim();
									expserialno = expserialno.trim();
									
									if (expserialno != null && expserialno != '')
										var getexpserialArr = expserialno.split(',');

									if (actserialno != null && actserialno != '')
										var getactserialArr = actserialno.split(',');

									if (getexpserialArr != null && getexpserialArr != '') {
										for ( var p = 0; p < getexpserialArr.length; p++) {
											if (actserialno != null && actserialno != '') {
												if (actserialno.indexOf(getexpserialArr[p]) == -1)// not
												// found
												// temSeriIdArr
												{
													if (varianceinExpserialNo == "") {
														varianceinExpserialNo = getexpserialArr[p];
													} else {
														varianceinExpserialNo = varianceinExpserialNo
																+ "," + getexpserialArr[p];
													}
												}
											}
										}
									}
									nlapiLogExecution('ERROR', 'varianceinExpserialNo',
											varianceinExpserialNo);

									if (getactserialArr != null && getactserialArr != '') {
										for ( var q = 0; q < getactserialArr.length; q++) {
											if (getexpserialArr != null && getexpserialArr != '') {

												if (getexpserialArr.indexOf(getactserialArr[q]) == -1)// not
												// found
												// temSeriIdArr
												{
													if (varianceinActserialNo == "") {
														varianceinActserialNo = getactserialArr[q];
													} else {
														varianceinActserialNo = varianceinActserialNo
																+ "," + getactserialArr[q];
													}
												}
											}
										}
									}
									nlapiLogExecution('ERROR', 'varianceinActserialNo',
											varianceinActserialNo);
								}

							/*	
								 * nlapiLogExecution('ERROR', 'expqty', expqty);
								 * nlapiLogExecution('ERROR', 'actqty', actqty);
								 * nlapiLogExecution('ERROR', 'expsku', expsku);
								 * nlapiLogExecution('ERROR', 'actsku', actsku);
								 * nlapiLogExecution('ERROR', 'explot', explot);
								 * nlapiLogExecution('ERROR', 'actlot', actlot);
								 * nlapiLogExecution('ERROR', 'explp', explp);
								 * nlapiLogExecution('ERROR', 'actlp', actlp);
								 * nlapiLogExecution('ERROR', 'expskustatusvalue',
								 * expskustatusvalue); nlapiLogExecution('ERROR',
								 * 'actskustatusvalue', actskustatusvalue);
								 */

								nlapiLogExecution('ERROR', 'orderListArray[m].getId()',
										orderListArray[m].getId());
								// var serialnourl = nlapiResolveURL('SUITELET',
								// 'customscript_lprelated_serial', 'customdeploy_lprelated_serial')
								// + "&custparam_item=" + actsku+ "&custparam_planno=" + planno;
								var serialnourl = nlapiResolveURL('SUITELET',
										'customscript_lprelated_serial',
										'customdeploy_lprelated_serial')
										+ "&custparam_expserial="
										+ null
										+ "&custparam_actserial="
										+ null
										+ "&custparam_expskustatus="
										+ expskustatusvalue
										+ "&custparam_actskustatus="
										+ actskustatusvalue
										+ "&custparam_recid=" + orderListArray[m].getId();
								// case 20124074 start
								var Invturl = nlapiResolveURL('SUITELET',
										'customscript_ebiz_resolve_expitemurl',
										'customdeploy_ebiz_resolve_expitemurl_di')
										+ "&custparam_expsku="
										+ actsku
										+ "&custparam_planno="
										+ planno;
								// end

								var invtfilters = new Array();
								invtfilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null,
										'anyof', loc));
								invtfilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null,
										'anyof', actsku));
								invtfilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null,
										'is', actlp));
								invtfilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
								
								var invtcolumns = new Array();
								invtcolumns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
								invtcolumns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_loc');
								invtcolumns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
								invtcolumns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
								invtcolumns[4] = new nlobjSearchColumn('custrecord_ebiz_avl_qty');
								invtcolumns[0].setSort();
								var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv',
										null, invtfilters, invtcolumns);
								if (invtsearchresults != null && invtsearchresults != '')
									var	currentinvt = invtsearchresults[0]
											.getValue('custrecord_ebiz_avl_qty');

								if (currentinvt == 'undefined' || currentinvt == null || currentinvt == ''
										|| isNaN(currentinvt)) {
									currentinvt = 0;
								}
								
								// if((request.getParameter('restypeflag')=='cyclecountsd' ||
								// hiddenfieldshowvalue == 'D') && (expqty != actqty || expsku !=
								// actsku || expserialno != actserialno || explot != actlot || explp
								// != actlp ))
								if ((ShowDis == 'T')
										&& (expqty != actqty || expsku != actsku
												|| explot != actlot || explp != actlp
												|| expskustatusvalue != actskustatusvalue
												|| varianceinExpserialNo != "" || varianceinActserialNo != "")) {
									i = i + 1;
									nlapiLogExecution('ERROR',
											'request.getParameter(restypeflag) in if binding',
											request.getParameter('custparam_restypeflag'));
									nlapiLogExecution('ERROR',
											'hiddenfieldshowvalue in if binding',
											hiddenfieldshowvalue);
									nlapiLogExecution('ERROR', 'orderListArray[m].getId() inside',
											orderListArray[m].getId());

									/*
									 * form.getSubList('custpage_items').setLineItemValue('custpage_locationtext',
									 * i, locText);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_location',
									 * i, loc);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expsku',
									 * i, expsku);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expskutext',
									 * i, expskutext);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actsku',
									 * i, actsku);
									 * //form.getSubList('custpage_items').setLineItemValue('custpage_actskutext',
									 * i, actskutext); //case 20124074 start
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actskutextlink',
									 * i, "<a href=\"#\" onclick=\"javascript:window.open('" +
									 * Invturl + "');\" >" + actskutext + "</a>"); //end
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expqty',
									 * i, expqty);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actqty',
									 * i, actqty);
									 * 
									 * form.getSubList('custpage_items').setLineItemValue('custpage_dollarvariance',
									 * i, dollarvariance);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_qtyvariance',
									 * i, parseFloat(actqty) - parseFloat(expqty));
									 * form.getSubList('custpage_items').setLineItemValue('custpage_qtytobeadjust',
									 * i, parseFloat(actqty) - parseFloat(expqty));
									 * form.getSubList('custpage_items').setLineItemValue('custpage_explp',
									 * i, explp);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actlp',
									 * i, actlp);
									 * 
									 * //form.getSubList('custpage_items').setLineItemValue('custpage_expserial',
									 * i, expserialno);
									 * //form.getSubList('custpage_items').setLineItemValue('custpage_actserial',
									 * i, actserialno);
									 * 
									 * 
									 * 
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expbatch',
									 * i, explot);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actbatch',
									 * i, actlot);
									 * 
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expskustatus',
									 * i, expskustatus);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expskustatusvalue',
									 * i, expskustatusvalue);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actskustatus',
									 * i, actskustatus);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actskustatusvalue',
									 * i, actskustatusvalue);
									 * 
									 * form.getSubList('custpage_items').setLineItemValue('custpage_id',
									 * i, orderListArray[m].getId());
									 * form.getSubList('custpage_items').setLineItemValue('custpage_planno',
									 * i, planno);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_name',
									 * i, name);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_opentaskid',
									 * i, opentaskid);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_invtid',
									 * i, invtid);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_site',
									 * i, siteid);
									 
*/
									
									
									
									

									strxml += "<tr><td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += locText;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += expskutext;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actskutext;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += expqty;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actqty;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += parseFloat(actqty) - parseFloat(expqty);
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += dollarvariance;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += explp;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actlp;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += explot;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actlot;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += expskustatus;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actskustatus;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += currentinvt;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += parseFloat(actqty) - parseFloat(expqty);
									strxml += "</td></tr>";
									
									
									
									
									
									
									
									
									
									
								}

								// else if((request.getParameter('restypeflag')=='cyclecountsnd' ||
								// hiddenfieldshowvalue == 'ND') && (expqty == actqty && expsku ==
								// actsku && expserialno == actserialno && explot == actlot && explp
								// == actlp ))
								else if ((ShowNonDis == 'T')
										&& (expqty == actqty && expsku == actsku
												&& explot == actlot && explp == actlp
												&& expskustatusvalue == actskustatusvalue
												&& varianceinExpserialNo == "" && varianceinActserialNo == "")) {

									i = i + 1;
									nlapiLogExecution('ERROR',
											'request.getParameter(restypeflag) in else binding',
											request.getParameter('custparam_restypeflag'));
									nlapiLogExecution('ERROR',
											'hiddenfieldshowvalue in else binding',
											hiddenfieldshowvalue);
									nlapiLogExecution('ERROR',
											'orderListArray[m].getId() else inside',
											orderListArray[m].getId());

							/*		
									 * form.getSubList('custpage_items').setLineItemValue('custpage_locationtext',
									 * i, locText);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_location',
									 * i, loc);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expsku',
									 * i, expsku);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expskutext',
									 * i, expskutext);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actsku',
									 * i, actsku);
									 * //form.getSubList('custpage_items').setLineItemValue('custpage_actskutext',
									 * i, actskutext);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actskutextlink',
									 * i, "<a href=\"#\" onclick=\"javascript:window.open('" +
									 * serialnourl + "');\" >" + actskutext + "</a>");
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expqty',
									 * i, expqty);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actqty',
									 * i, actqty);
									 * 
									 * form.getSubList('custpage_items').setLineItemValue('custpage_dollarvariance',
									 * i, dollarvariance);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_qtyvariance',
									 * i, parseFloat(actqty) - parseFloat(expqty));
									 * form.getSubList('custpage_items').setLineItemValue('custpage_qtytobeadjust',
									 * i, parseFloat(actqty) - parseFloat(expqty));
									 * form.getSubList('custpage_items').setLineItemValue('custpage_explp',
									 * i, explp);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actlp',
									 * i, actlp);
									 * 
									 * //form.getSubList('custpage_items').setLineItemValue('custpage_expserial',
									 * i, expserialno);
									 * //form.getSubList('custpage_items').setLineItemValue('custpage_actserial',
									 * i, actserialno);
									 * 
									 * 
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expbatch',
									 * i, explot);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actbatch',
									 * i, actlot);
									 * 
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expskustatus',
									 * i, expskustatus);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expskustatusvalue',
									 * i, expskustatusvalue);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actskustatus',
									 * i, actskustatus);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actskustatusvalue',
									 * i, actskustatusvalue);
									 * 
									 * form.getSubList('custpage_items').setLineItemValue('custpage_id',
									 * i, orderListArray[m].getId());
									 * form.getSubList('custpage_items').setLineItemValue('custpage_planno',
									 * i, planno);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_name',
									 * i, name);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_opentaskid',
									 * i, opentaskid);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_invtid',
									 * i, invtid);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_site',
									 * i, siteid);
									 */
									
									
									
									strxml += "<tr><td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += locText;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += expskutext;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actskutext;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += expqty;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actqty;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += parseFloat(actqty) - parseFloat(expqty);
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += dollarvariance;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += explp;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actlp;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += explot;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actlot;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += expskustatus;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actskustatus;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += currentinvt;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += parseFloat(actqty) - parseFloat(expqty);
									strxml += "</td></tr>";
									
									
								}
								
								else if ((ShowAll == 'T')) {

									i = i + 1;
									nlapiLogExecution('ERROR',
											'request.getParameter(restypeflag) in else binding',
											request.getParameter('custparam_restypeflag'));
									nlapiLogExecution('ERROR',
											'hiddenfieldshowvalue in else binding',
											hiddenfieldshowvalue);
									nlapiLogExecution('ERROR',
											'orderListArray[m].getId() else inside',
											orderListArray[m].getId());

							/*		
									 * form.getSubList('custpage_items').setLineItemValue('custpage_locationtext',
									 * i, locText);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_location',
									 * i, loc);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expsku',
									 * i, expsku);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expskutext',
									 * i, expskutext);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actsku',
									 * i, actsku);
									 * //form.getSubList('custpage_items').setLineItemValue('custpage_actskutext',
									 * i, actskutext);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actskutextlink',
									 * i, "<a href=\"#\" onclick=\"javascript:window.open('" +
									 * serialnourl + "');\" >" + actskutext + "</a>");
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expqty',
									 * i, expqty);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actqty',
									 * i, actqty);
									 * 
									 * form.getSubList('custpage_items').setLineItemValue('custpage_dollarvariance',
									 * i, dollarvariance);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_qtyvariance',
									 * i, parseFloat(actqty) - parseFloat(expqty));
									 * form.getSubList('custpage_items').setLineItemValue('custpage_qtytobeadjust',
									 * i, parseFloat(actqty) - parseFloat(expqty));
									 * form.getSubList('custpage_items').setLineItemValue('custpage_explp',
									 * i, explp);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actlp',
									 * i, actlp);
									 * 
									 * //form.getSubList('custpage_items').setLineItemValue('custpage_expserial',
									 * i, expserialno);
									 * //form.getSubList('custpage_items').setLineItemValue('custpage_actserial',
									 * i, actserialno);
									 * 
									 * 
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expbatch',
									 * i, explot);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actbatch',
									 * i, actlot);
									 * 
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expskustatus',
									 * i, expskustatus);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_expskustatusvalue',
									 * i, expskustatusvalue);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actskustatus',
									 * i, actskustatus);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_actskustatusvalue',
									 * i, actskustatusvalue);
									 * 
									 * form.getSubList('custpage_items').setLineItemValue('custpage_id',
									 * i, orderListArray[m].getId());
									 * form.getSubList('custpage_items').setLineItemValue('custpage_planno',
									 * i, planno);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_name',
									 * i, name);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_opentaskid',
									 * i, opentaskid);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_invtid',
									 * i, invtid);
									 * form.getSubList('custpage_items').setLineItemValue('custpage_site',
									 * i, siteid);
									 */
									
									
									
									strxml += "<tr><td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += locText;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += expskutext;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actskutext;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += expqty;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actqty;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += parseFloat(actqty) - parseFloat(expqty);
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += dollarvariance;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += explp;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actlp;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += explot;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actlot;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += expskustatus;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += actskustatus;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += currentinvt;
									strxml += "</td>";
									
									strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
									strxml += parseFloat(actqty) - parseFloat(expqty);
									strxml += "</td></tr>";
									
									
								}

								
								
							}
						}

						
						
						
						
						
						
						
					}
				}
			
	//	}
		else
		{
			showInlineMessage(form, 'Confirmation', 'Plan# :  ' + varCCplanNo + ' is Closed ' , '');
		}
	} 
	catch (e) {
		nlapiLogExecution('ERROR', 'Catch', e);
	}

		
		
		
		
		
		
		
		
		
		
		strxml = strxml + "</table>";
		strxml = strxml + "\n</body>\n</pdf>";
		xml = xml + strxml;

		var file = nlapiXMLToPDF(xml);
		response.setContentType('PDF', 'CycleCountResolve.pdf');
		response.write(file.getValue());
		
		
		

	}
}



var searchResultArray = new Array();
function getOrdersForCycleCount(request, maxno) {

	var localVarArray;
	// this is to maintain the querystring parameters while refreshing the
	// paging drop down
	if (request.getMethod() == 'POST') {
		var queryparams = request.getParameter('custparam_qeryparams');
		nlapiLogExecution('Error', 'queryparams', queryparams);
		// queryparams=queryparams.
		var tempArray = new Array();
		tempArray.push(queryparams.split(','));
		localVarArray = tempArray;
		nlapiLogExecution('Error', 'tempArray', tempArray.length);
	} else {
		localVarArray = validateRequestParams(request);
		nlapiLogExecution('Error', 'localVarArrayinelse', localVarArray);
	}

	// Get all the search filters for wave generation
	var filters = new Array();
	filters = specifyInvtFilters(localVarArray, maxno);
	nlapiLogExecution('Error', 'afterspecifyfilters', localVarArray);
	// Adding search columns
	var columns = new Array();
	columns = addColumnsForSearch();
	nlapiLogExecution('Error', 'afterspecifycolumns', localVarArray);
	var orderList = nlapiSearchRecord('customrecord_ebiznet_cyclecountexe',
			null, filters, columns);
	if (orderList != null && orderList.length >= 1000) {
		nlapiLogExecution('Error', 'orderList.length', orderList.length);
		searchResultArray.push(orderList);
		var maxno = orderList[orderList.length - 1].getValue(columns[21]);
		getOrdersForCycleCount(request, maxno);
	} else {
		nlapiLogExecution('Error', 'orderList.length', orderList.length);
		searchResultArray.push(orderList);
	}
	return searchResultArray;

}

function addColumnsForSearch() {
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_cycact_beg_loc');
	columns[1] = new nlobjSearchColumn('custrecord_cyclecount_exp_ebiz_sku_no');
	columns[2] = new nlobjSearchColumn('custrecord_cycleact_sku');
	columns[3] = new nlobjSearchColumn('custrecord_cycleexp_qty');
	columns[4] = new nlobjSearchColumn('custrecord_cycle_act_qty');
	columns[5] = new nlobjSearchColumn('custrecord_cyclelpno');
	columns[6] = new nlobjSearchColumn('custrecord_cycact_lpno');
	columns[7] = new nlobjSearchColumn('custrecord_cycle_count_plan');
	columns[8] = new nlobjSearchColumn('custrecord_opentaskid');
	columns[9] = new nlobjSearchColumn('custrecord_invtid');
	columns[10] = new nlobjSearchColumn('custrecord_expcyclesku_status');
	columns[11] = new nlobjSearchColumn('custrecord_cyclesku_status');
	columns[12] = new nlobjSearchColumn('custrecord_cyclesite_id');
	columns[13] = new nlobjSearchColumn('custrecord_cyclesku');
	columns[14] = new nlobjSearchColumn('custrecord_cyclecount_act_ebiz_sku_no');
	columns[15] = new nlobjSearchColumn('custrecord_expcycleabatch_no');
	columns[16] = new nlobjSearchColumn('custrecord_cycleabatch_no');

	columns[17] = new nlobjSearchColumn('custrecord_cycle_serialno');
	columns[18] = new nlobjSearchColumn('custrecord_cycle_expserialno');
	columns[19] = new nlobjSearchColumn('custrecord_cyclestatus_flag');

	return columns;

}

function specifyInvtFilters(localVarArray, maxno) {
	nlapiLogExecution('ERROR', 'localVarArray[0]', localVarArray[0]);
	// define search filters
	var filters = new Array();

	if (localVarArray[0] != "" && localVarArray[0] != null)
		filters.push(new nlobjSearchFilter('custrecord_cycle_count_plan', null,
				'equalto', localVarArray[0]));
	nlapiLogExecution('ERROR','localVarArray[1]',localVarArray[1]);
	if (localVarArray[1] != "" && localVarArray[1] != null)
		filters.push(new nlobjSearchFilter('custrecord_cyclesite_id', null,
				'anyof', localVarArray[0][1]));
	if (localVarArray[2] != "" && localVarArray[2] != null)
		filters.push(new nlobjSearchFilter('custrecord_cyclesku', null,
				'anyof', localVarArray[0][2]));
	if (localVarArray[3] != "" && localVarArray[3] != null)
		filters.push(new nlobjSearchFilter('custrecord_cycact_beg_loc', null,
				'anyof', localVarArray[3]));

	filters.push(new nlobjSearchFilter('custrecord_cyclestatus_flag', null,
			'anyof', [ 31,22 ]));// Cycle
	// Count
	// Record
	var vRoleLocation = getRoledBasedLocation();
	nlapiLogExecution('ERROR','vRoleLocation',vRoleLocation);
	if ((localVarArray[1] == null || localVarArray[1] == '')
			&& (vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)) {
		filters.push(new nlobjSearchFilter('custrecord_cyclesite_id', null,
				'is', vRoleLocation));
		nlapiLogExecution('ERROR','vRoleLocation in if',vRoleLocation);
	}
	return filters;

	// define search filters
	// var filters = new Array();

	// var filters = new Array();

	// if(vfilterarr[0] != "" && vfilterarr[0] != null)
	// filters.push(new nlobjSearchFilter('custrecord_cycle_count_plan', null,
	// 'equalto',vfilterarr[0]));

	// if(vfilterarr[1] != "" && vfilterarr[1] != null)
	// filters.push(new nlobjSearchFilter('custrecord_cyclesite_id', null,
	// 'anyof',vfilterarr[1]));
	// if(vfilterarr[2] != "" && vfilterarr[2] != null)
	// filters.push(new nlobjSearchFilter('custrecord_cyclesku', null,
	// 'anyof',vfilterarr[2]));
	// if(vfilterarr[3] != "" && vfilterarr[3] != null)
	// filters.push(new nlobjSearchFilter('custrecord_cycact_beg_loc', null,
	// 'anyof',vfilterarr[3]));

	// filters.push(new nlobjSearchFilter('custrecord_cyclestatus_flag', null,
	// 'anyof', [31]));//Cycle Count Record
	// var vRoleLocation=getRoledBasedLocation();
	// if((vfilterarr[1]==null || vfilterarr[1]=='') && (vRoleLocation != null
	// &&
	// vRoleLocation != '' && vRoleLocation != 0))
	// {
	// filters.push(new nlobjSearchFilter('custrecord_cyclesite_id', null, 'is',
	// vRoleLocation));
	// }
	// return filters;

}

function validateRequestParams(request) {
	var CyclecountPlan = "";
	var CycwhLoc = "";
	var CycItem = "";
	var CycBinloc = "";
	var localVarArray = new Array();
	if (request.getParameter('custparam_cyclecountplan') != null
			&& request.getParameter('custparam_cyclecountplan') != "") {
		CyclecountPlan = request.getParameter('custparam_cyclecountplan');
	}

	if (request.getParameter('custparam_cyclecountwhlocation') != null
			&& request.getParameter('custparam_cyclecountwhlocation') != "") {
		CycwhLoc = request.getParameter('custparam_cyclecountwhlocation');
	}

	if (request.getParameter('custparam_cyclecountitem') != null
			&& request.getParameter('custparam_cyclecountitem') != "") {

		CycItem = request.getParameter('custparam_cyclecountitem');
	}

	if (request.getParameter('custparam_cyclecountbinloc') != null
			&& request.getParameter('custparam_cyclecountbinloc') != "") {
		CycBinloc = request.getParameter('custparam_cyclecountbinloc');
	}

	if (request.getParameter('custparam_qeryparams') == null) {
		var hiddenQueryParams;
		nlapiLogExecution('ERROR', 'localVarArray', localVarArray.toString());
		//hiddenQueryParams.setDefaultValue(localVarArray.toString());
		hiddenQueryParams=localVarArray.toString();
	}

	nlapiLogExecution('ERROR', 'CycwhLoc', CycwhLoc);
	nlapiLogExecution('ERROR', 'CycItem', CycItem);
	nlapiLogExecution('ERROR', 'CycBinloc', CycBinloc);

	var currentRow = [ CyclecountPlan, CycwhLoc, CycItem, CycBinloc ];

	localVarArray.push(currentRow);
	return localVarArray;

}

function getSerialNoCSV(arrayLP) {
	var csv = "";
	nlapiLogExecution('ERROR', 'arrayLP.length', arrayLP.length);
	if (arrayLP.length == 1) {
		csv = arrayLP[0][0];
	} else {
		for ( var i = 0; i < arrayLP.length; i++) {
			if (i == arrayLP.length - 1) {
				csv += arrayLP[i][0];
			} else {
				csv += arrayLP[i][0] + ",";
			}
		}
	}
	return csv;
}

function fetchPalletQuantity(itemId, location, company) {
	nlapiLogExecution('ERROR', 'itemId', itemId);
	nlapiLogExecution('ERROR', 'location in fetchPalletQuantity', location);
	var itemPalletQuantity = 0;
	var itemFilters = new Array();
	itemFilters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null,
			'is', itemId);
	itemFilters[1] = new nlobjSearchFilter('custrecord_ebizuomlevelskudim',
			null, 'is', 3);

	var itemColumns = new Array();
	itemColumns[0] = new nlobjSearchColumn('custrecord_ebizitemdims');
	itemColumns[1] = new nlobjSearchColumn('custrecord_ebizsiteskudim');
	itemColumns[2] = new nlobjSearchColumn('custrecord_ebizcompanyskudimension');
	itemColumns[3] = new nlobjSearchColumn('custrecord_ebizqty');

	var itemSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims',
			null, itemFilters, itemColumns);

	if (itemSearchResults != null) {
		itemPalletQuantity = itemSearchResults[0].getValue(itemColumns[3]);
		nlapiLogExecution('ERROR', 'Item Pallet Quantity', itemPalletQuantity);
	}
	return itemPalletQuantity;
}

/*
 * Set line item level values based on search results
 */
function setLineItemValuesFromSearch(searchResults, i) {
	var loc, locText, expsku, expskutext, actskutext, actsku, expqty, actqty, explp, actlp, expskustatus, actskustatus;
	var actskustatusvalue, planno, name, opentaskid, intid, siteis, actlot, explot, expskustatusvalue, expserialno, actserialno;
	var dollarvariance,currentinvt;

	/*
	 * Searching records from custom record and dynamically adding to the
	 * sublist lines
	 */
	var searchresult = searchResults;
	nlapiLogExecution('ERROR', 'searchresults.length', searchresult.length);

	loc = searchresult.getValue('custrecord_cycact_beg_loc');
	locText = searchresult.getText('custrecord_cycact_beg_loc');
	expsku = searchresult.getValue('custrecord_cyclecount_exp_ebiz_sku_no');
	expskutext = searchresult.getText('custrecord_cyclesku');
	// actsku = searchresult.getValue('custrecord_cycleact_sku');
	actskutext = searchresult.getText('custrecord_cycleact_sku');
	actsku = searchresult.getValue('custrecord_cyclecount_act_ebiz_sku_no');
	expqty = searchresult.getValue('custrecord_cycleexp_qty');
	actqty = searchresult.getValue('custrecord_cycle_act_qty');
	explp = searchresult.getValue('custrecord_cyclelpno');
	actlp = searchresult.getValue('custrecord_cycact_lpno');

	expserialno = searchresult.getValue('custrecord_cycle_expserialno');
	actserialno = searchresult.getValue('custrecord_cycle_serialno');

	nlapiLogExecution('ERROR', 'expserialno in show all', expserialno);
	nlapiLogExecution('ERROR', 'actserialno in show all', actserialno);

	expskustatus = searchresult.getText('custrecord_expcyclesku_status');
	expskustatusvalue = searchresult.getValue('custrecord_expcyclesku_status');
	actskustatus = searchresult.getText('custrecord_cyclesku_status');
	actskustatusvalue = searchresult.getValue('custrecord_cyclesku_status');

	if (actskustatus == null || actskustatus == "")
		actskustatus = expskustatus;
	if (actskustatusvalue == null || actskustatusvalue == "")
		actskustatusvalue = expskustatusvalue;

	planno = searchresult.getValue('custrecord_cycle_count_plan');
	name = searchresult.getValue('custrecord_cycle_count_plan');
	opentaskid = searchresult.getValue('custrecord_opentaskid');
	invtid = searchresult.getValue('custrecord_invtid');
	siteid = searchresult.getValue('custrecord_cyclesite_id');
	actlot = searchresult.getValue('custrecord_cycleabatch_no');
	explot = searchresult.getValue('custrecord_expcycleabatch_no');
	status = searchresult.getText('custrecord_cyclestatus_flag');

	var vItemname = '';
	var vCost = 0;
	var vAvgCost = 0;

	// Code Added By Ganapathi on 26th Nov 2012
	if (actsku != null && actsku != '') {
		var filters = new Array();
		filters.push(new nlobjSearchFilter('internalid', null, 'is', actsku));
		filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

		var columns = new Array();
		columns[0] = new nlobjSearchColumn('cost');
		columns[1] = new nlobjSearchColumn('averagecost');
		columns[2] = new nlobjSearchColumn('itemid');

		var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
		if (itemdetails != null) {
			vItemname = itemdetails[0].getValue('itemid');
			vCost = itemdetails[0].getValue('cost');
			nlapiLogExecution('ERROR', 'vCost', vCost);
			vAvgCost = itemdetails[0].getValue('averagecost');
			nlapiLogExecution('ERROR', 'Average Cost', vAvgCost);
		}
	}

	if (actqty == null || actqty == "") {
		nlapiLogExecution('ERROR', 'actqty:', actqty);
		actqty = 0;
	}

	if (expqty == null || expqty == "") {
		nlapiLogExecution('ERROR', 'expqty: ', expqty);
		expqty = 0;
	}

	dollarvariance = (parseFloat(actqty) - parseFloat(expqty)) * vAvgCost;

	nlapiLogExecution('ERROR', 'dollarvariance', dollarvariance);

	if (isNaN(dollarvariance) || dollarvariance == null || dollarvariance == "") {
		nlapiLogExecution('ERROR', 'dollarvariance: ', dollarvariance);
		dollarvariance = 0;
	} else {
		// dollarvariance=Math.round(dollarvariance,2);
		dollarvariance = Math.round(dollarvariance * 100) / 100;
		// parseFloat(dollarvariance).round(2);

	}

	// code to get custpage_qtytobeadjust value based on RF picking done between
	// record and resolve

	nlapiLogExecution('ERROR', 'actsku', actsku);
	nlapiLogExecution('ERROR', 'loc', loc);
	nlapiLogExecution('ERROR', 'actskustatusvalue', actskustatusvalue);
	nlapiLogExecution('ERROR', 'expskustatusvalue', expskustatusvalue);
	nlapiLogExecution('ERROR', 'actlp', actlp);

	var QoH = 0;
	var NewExpQty = 0, NewActQty = 0, ExpQuantity = 0, QtytobeAdjusted = 0, SkuStatusValue;

	if (actskustatusvalue == null || actskustatusvalue == ''
			|| actskustatusvalue == 'null') {
		actskustatusvalue = expskustatusvalue;
	}
	var filterslp = new Array();
	// filterslp[0] = new nlobjSearchFilter('custrecord_invttasktype', null,
	// 'anyof', [2, 10]);
	filterslp.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag',
			null, 'anyof', [ 19 ]));

	if (actsku != null && actsku != '')
		filterslp.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null,
				'anyof', actsku));

	if (loc != null && loc != '')
		filterslp.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc',
				null, 'anyof', loc));

	if (actskustatusvalue != null && actskustatusvalue != '')
		filterslp.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku_status',
				null, 'anyof', actskustatusvalue));

	if (actlp != null && actlp != '')
		filterslp.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null,
				'is', actlp));

	var columnlp = new Array();
	columnlp[0] = new nlobjSearchColumn('custrecord_ebiz_qoh');
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_createinv',
			null, filterslp, columnlp);

	if (searchresults != null && searchresults != '') {
		QoH = searchresults[0].getValue('custrecord_ebiz_qoh');
		if (parseFloat(QoH) != parseFloat(expqty)) {
			nlapiLogExecution('ERROR', 'in not equal if QoH', QoH);
			nlapiLogExecution('ERROR', 'in not equal if expqty', expqty);
			nlapiLogExecution('ERROR', 'in not equal if actqty', actqty);

			ExpQuantity = parseFloat(expqty) - parseFloat(QoH);
			nlapiLogExecution('ERROR', 'in not equal if ExpQuantity',
					ExpQuantity);
			NewActQty = parseFloat(actqty) - parseFloat(ExpQuantity);
			nlapiLogExecution('ERROR', 'in not equal if NewActQty', NewActQty);
			/*
			 * if(parseFloat(NewActQty) == 0) { QtytobeAdjusted = 0; } else {
			 */
			QtytobeAdjusted = parseFloat(NewActQty) - parseFloat(QoH);
			// }

			nlapiLogExecution('ERROR', 'after ExpQuantity', ExpQuantity);
			nlapiLogExecution('ERROR', 'after NewActQty', NewActQty);
			nlapiLogExecution('ERROR', 'after QtytobeAdjusted', QtytobeAdjusted);

		} else {
			nlapiLogExecution('ERROR', 'in not equal else', 'else');
			QtytobeAdjusted = parseFloat(actqty) - parseFloat(expqty);
		}
	} else {
		nlapiLogExecution('ERROR', 'in search result null else', 'else');
		var tempqty = 0;
		if (parseFloat(expqty) < parseFloat(actqty)) {
			NewActQty = parseFloat(actqty) - parseFloat(expqty);
			QtytobeAdjusted = parseFloat(NewActQty) - parseFloat(tempqty);
		} else {
			QtytobeAdjusted = parseFloat(tempqty) - parseFloat(actqty);
		}
		// if(dollarvariance=='0')
		// {
		// QtytobeAdjusted = '0';
		// }
	}

	// var serialnourl = nlapiResolveURL('SUITELET',
	// 'customscript_lprelated_serial', 'customdeploy_lprelated_serial') +
	// "&custparam_item=" + actsku + "&custparam_planno=" + planno;
	var serialnourl = nlapiResolveURL('SUITELET',
			'customscript_lprelated_serial', 'customdeploy_lprelated_serial')
			+ "&custparam_expserial="
			+ null
			+ "&custparam_actserial="
			+ null
			+ "&custparam_expskustatus="
			+ expskustatusvalue
			+ "&custparam_actskustatus="
			+ actskustatusvalue
			+ "&custparam_recid=" + searchresult.getId();
	var Invturl = nlapiResolveURL('SUITELET',
			'customscript_ebiz_resolve_expitemurl',
			'customdeploy_ebiz_resolve_expitemurl_di')
			+ "&custparam_expsku=" + actsku + "&custparam_planno=" + planno;
	var BinlocInvturl = nlapiResolveURL('SUITELET',
			'customscript_ebiz_cycbinloc_invt',
			'customdeploy_ebiz_cycbinloc_invt_di')
			+ "&custparam_binloc=" + loc + "&custparam_planno=" + planno;

	nlapiLogExecution('ERROR', 'actsku', actsku);
	nlapiLogExecution('ERROR', 'loc', loc);
	nlapiLogExecution('ERROR', 'actlp', actlp);

	var invtfilters = new Array();
	invtfilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null,
			'anyof', loc));
	invtfilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null,
			'anyof', actsku));
	invtfilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null,
			'is', actlp));
	invtfilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	
	var invtcolumns = new Array();
	invtcolumns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
	invtcolumns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_loc');
	invtcolumns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
	invtcolumns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
	invtcolumns[4] = new nlobjSearchColumn('custrecord_ebiz_avl_qty');
	invtcolumns[0].setSort();
	var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv',
			null, invtfilters, invtcolumns);
	if (invtsearchresults != null && invtsearchresults != '')
			currentinvt = invtsearchresults[0]
				.getValue('custrecord_ebiz_avl_qty');

	if (currentinvt == 'undefined' || currentinvt == null || currentinvt == ''
			|| isNaN(currentinvt)) {
		currentinvt = 0;
	}
	// form.getSubList('custpage_items').setLineItemValue('custpage_locationtext',
	// i + 1, locText);
	/*
	 * form.getSubList('custpage_items').setLineItemValue('custpage_locationtext',
	 * i + 1, "<a href='"+ BinlocInvturl +"' target='_blank'>"+ locText + "</a>");
	 * form.getSubList('custpage_items').setLineItemValue('custpage_location', i +
	 * 1, loc);
	 * form.getSubList('custpage_items').setLineItemValue('custpage_expsku', i +
	 * 1, expsku);
	 * form.getSubList('custpage_items').setLineItemValue('custpage_expskutext',
	 * i + 1, expskutext);
	 * 
	 * form.getSubList('custpage_items').setLineItemValue('custpage_actsku', i +
	 * 1, actsku);
	 * //form.getSubList('custpage_items').setLineItemValue('custpage_actskutext',
	 * i + 1, actskutext);
	 * //form.getSubList('custpage_items').setLineItemValue('custpage_actskutext',
	 * i + 1, "<a href='"+ Invturl +"' target='_blank'>"+ actskutext + "</a>");
	 * form.getSubList('custpage_items').setLineItemValue('custpage_actskutextlink',
	 * i + 1, "<a href=\"#\" onclick=\"javascript:window.open('" + Invturl +
	 * "');\" >" + actskutext + "</a>");
	 * form.getSubList('custpage_items').setLineItemValue('custpage_expqty', i +
	 * 1, expqty);
	 * form.getSubList('custpage_items').setLineItemValue('custpage_actqty', i +
	 * 1, actqty);
	 * 
	 * form.getSubList('custpage_items').setLineItemValue('custpage_dollarvariance',
	 * i + 1, dollarvariance);
	 * form.getSubList('custpage_items').setLineItemValue('custpage_qtyvariance',
	 * i + 1, parseFloat(actqty) - parseFloat(expqty));
	 * //form.getSubList('custpage_items').setLineItemValue('custpage_qtytobeadjust',
	 * i + 1, parseFloat(actqty) - parseFloat(expqty));
	 * form.getSubList('custpage_items').setLineItemValue('custpage_qtytobeadjust',
	 * i + 1, parseFloat(QtytobeAdjusted));
	 * form.getSubList('custpage_items').setLineItemValue('custpage_explp', i +
	 * 1, explp);
	 * form.getSubList('custpage_items').setLineItemValue('custpage_actlp', i +
	 * 1, actlp);
	 * 
	 * //form.getSubList('custpage_items').setLineItemValue('custpage_expserial',
	 * i + 1, expserialno);
	 * //form.getSubList('custpage_items').setLineItemValue('custpage_actserial',
	 * i + 1, actserialno);
	 * 
	 * 
	 * form.getSubList('custpage_items').setLineItemValue('custpage_expbatch', i +
	 * 1, explot);
	 * form.getSubList('custpage_items').setLineItemValue('custpage_actbatch', i +
	 * 1, actlot);
	 * 
	 * form.getSubList('custpage_items').setLineItemValue('custpage_expskustatus',
	 * i + 1, expskustatus);
	 * form.getSubList('custpage_items').setLineItemValue('custpage_expskustatusvalue',
	 * i + 1, expskustatusvalue);
	 * form.getSubList('custpage_items').setLineItemValue('custpage_actskustatus',
	 * i + 1, actskustatus);
	 * form.getSubList('custpage_items').setLineItemValue('custpage_actskustatusvalue',
	 * i + 1, actskustatusvalue);
	 * 
	 * form.getSubList('custpage_items').setLineItemValue('custpage_id', i + 1,
	 * searchresult.getId());
	 * form.getSubList('custpage_items').setLineItemValue('custpage_planno', i +
	 * 1, planno);
	 * form.getSubList('custpage_items').setLineItemValue('custpage_name', i +
	 * 1, name);
	 * form.getSubList('custpage_items').setLineItemValue('custpage_opentaskid',
	 * i + 1, opentaskid);
	 * form.getSubList('custpage_items').setLineItemValue('custpage_invtid', i +
	 * 1, invtid);
	 * form.getSubList('custpage_items').setLineItemValue('custpage_site', i +
	 * 1, siteid);
	 * form.getSubList('custpage_items').setLineItemValue('custpage_currentinvt',
	 * i + 1, currentinvt);
	 */
	
	
	
	strxml += "<tr><td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
	strxml += locText;
	strxml += "</td>";
	
	strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
	strxml += expskutext;
	strxml += "</td>";
	
	strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
	strxml += actskutext;
	strxml += "</td>";
	
	strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
	strxml += expqty;
	strxml += "</td>";
	
	strxml += "<tr><td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
	strxml += actqty;
	strxml += "</td>";
	
	strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
	strxml += parseFloat(actqty) - parseFloat(expqty);
	strxml += "</td>";
	
	strxml += "<tr><td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
	strxml += dollarvariance;
	strxml += "</td>";
	
	strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
	strxml += explp;
	strxml += "</td>";
	
	strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
	strxml += actlp;
	strxml += "</td>";
	
	strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
	strxml += explot;
	strxml += "</td>";
	
	strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
	strxml += actlot;
	strxml += "</td>";
	
	strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
	strxml += expskustatus;
	strxml += "</td>";
	
	strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
	strxml += actskustatus;
	strxml += "</td>";
	
	strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
	strxml += currentinvt;
	strxml += "</td>";
	
	strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
	strxml += parseFloat(actqty) - parseFloat(expqty);
	strxml += "</td>";
	
	strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
	strxml += status;
	strxml += "</td></tr>";
	
	
	
	nlapiLogExecution('ERROR', 'siteid ', siteid);
	nlapiLogExecution('ERROR', 'after loc', loc);
	nlapiLogExecution('ERROR', 'after searchresultinvtid', invtid);
	nlapiLogExecution('ERROR', 'after expsku', expsku);
	nlapiLogExecution('ERROR', 'after actsku', actsku);

}

/*function AdjustSerialNumbers(localSerialNoArray, qty, item, lp, restype,
		CyccRecID, varianceinExpserialNo) {
	try {
		nlapiLogExecution('ERROR', 'AdjustSerialNumbers qty', parseFloat(qty));
		nlapiLogExecution('ERROR', 'item', item);
		nlapiLogExecution('ERROR', 'lp', lp);
		nlapiLogExecution('ERROR', 'restype', restype);
		nlapiLogExecution('ERROR', 'CyccRecID', CyccRecID);
		if (restype == 'resolve' || restype == 'close') {
			var fields = [ 'custrecord_cycle_serialno',
					'custrecord_cycle_expserialno' ];
			var columns = nlapiLookupField(
					'customrecord_ebiznet_cyclecountexe', CyccRecID, fields);
			var ActSerialNos = columns.custrecord_cycle_serialno;
			nlapiLogExecution('ERROR', 'ActSerialNos in AdjustSerialNumbers: ',
					ActSerialNos);
			var ExpSerialNos = columns.custrecord_cycle_expserialno;
			nlapiLogExecution('ERROR', 'ExpSerialNos in AdjustSerialNumbers: ',
					ExpSerialNos);

			if (parseFloat(qty) < 0) {
				var filters = new Array();
				filters[0] = new nlobjSearchFilter(
						'custrecord_serialiteminternalid', null, 'is', item);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid',
						null, 'is', lp);
				filters[2] = new nlobjSearchFilter('custrecord_serialstatus',
						null, 'isnot', 'D');
				filters[3] = new nlobjSearchFilter(
						'custrecord_serialwmsstatus', null, 'anyof', [ 3, 19 ]);

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults = nlapiSearchRecord(
						'customrecord_ebiznetserialentry', null, filters,
						columns);

				if (searchResults != null && searchResults != ""
						&& ActSerialNos == "") {
					nlapiLogExecution('ERROR',
							'searchResults.length in qty<0 in if: ',
							searchResults.length);
					for ( var i = 0; i < searchResults.length; i++) {
						if (searchResults[i]
								.getValue('custrecord_serialnumber') != null
								&& searchResults[i]
										.getValue('custrecord_serialnumber') != "") {
							var SerialNo = searchResults[i]
									.getValue('custrecord_serialnumber');
							var InternalID = searchResults[i].getId();

							// var currentRow = [SerialNo];
							// localSerialNoArray.push(currentRow);
							// nlapiLogExecution('ERROR', 'localSerialNoArray :
							// ',
							// localSerialNoArray.toString());

							// var id =
							// nlapiDeleteRecord('customrecord_ebiznetserialentry',
							// InternalID);
							// nlapiLogExecution('ERROR', 'Deleted Actual Record
							// :
							// ', id);
						}
					}
				} else {

					var filters = new Array();
					filters[0] = new nlobjSearchFilter(
							'custrecord_serialiteminternalid', null, 'is', item);
					filters[1] = new nlobjSearchFilter(
							'custrecord_serialparentid', null, 'is', lp);

					var columns = new Array();
					columns[0] = new nlobjSearchColumn(
							'custrecord_serialnumber');
					var searchResults = nlapiSearchRecord(
							'customrecord_ebiznetserialentry', null, filters,
							columns);

					if (searchResults != null && searchResults != "") {
						nlapiLogExecution('ERROR',
								'searchResults.length in qty<0 in else: ',
								searchResults.length);
						for ( var i = 0; i < searchResults.length; i++) {
							if (searchResults[i]
									.getValue('custrecord_serialnumber') != null
									&& searchResults[i]
											.getValue('custrecord_serialnumber') != "") {
								var SerialNo = searchResults[i]
										.getValue('custrecord_serialnumber');
								if (ActSerialNos.indexOf(SerialNo) == -1) {
									if (varianceinExpserialNo.indexOf(SerialNo) != -1) {
										nlapiLogExecution('ERROR',
												'SerialNo in ActSerialNos : ',
												SerialNo);
										var InternalID = searchResults[i]
												.getId();

										// var currentRow = [SerialNo];
										// localSerialNoArray.push(currentRow);
										// nlapiLogExecution('ERROR',
										// 'localSerialNoArray : ',
										// localSerialNoArray.toString());

										
										 * var LoadSerialNumbers =
										 * nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
										 * LoadSerialNumbers.setFieldValue('custrecord_serialwmsstatus',
										 * '32');
										 * 
										 * var recid =
										 * nlapiSubmitRecord(LoadSerialNumbers,
										 * false, true);
										 * nlapiLogExecution('ERROR', 'Serial
										 * Entry rec id',recid);
										 

										var fieldNames = new Array();
										var newValues = new Array();

										fieldNames
												.push('custrecord_serialwmsstatus');
										newValues.push('32');

										nlapiSubmitField(
												'customrecord_ebiznetserialentry',
												InternalID, fieldNames,
												newValues);
									}

									// var id =
									// nlapiDeleteRecord('customrecord_ebiznetserialentry',
									// InternalID);
									// nlapiLogExecution('ERROR', 'Deleted
									// Actual
									// Record : ', id);
								}
							}
						}
					}
				}
			} else if (parseFloat(qty) > 0) {
				var filters = new Array();
				filters[0] = new nlobjSearchFilter(
						'custrecord_serialiteminternalid', null, 'is', item);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid',
						null, 'is', lp);
				filters[2] = new nlobjSearchFilter('custrecord_serialstatus',
						null, 'is', 'I');

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults = nlapiSearchRecord(
						'customrecord_ebiznetserialentry', null, filters,
						columns);

				if (searchResults != null && searchResults != "") {
					nlapiLogExecution('ERROR', 'searchResults.length : ',
							searchResults.length);
					for ( var i = 0; i < searchResults.length; i++) {
						if (searchResults[i]
								.getValue('custrecord_serialnumber') != null
								&& searchResults[i]
										.getValue('custrecord_serialnumber') != "") {
							var SerialNo = searchResults[i]
									.getValue('custrecord_serialnumber');
							
							 * var InternalID = searchResults[i].getId();
							 * 
							 * var currentRow = [SerialNo];
							 * localSerialNoArray.push(currentRow);
							 * nlapiLogExecution('ERROR', 'localSerialNoArray : ',
							 * localSerialNoArray.toString());
							 * 
							 * var LoadSerialNumbers =
							 * nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
							 * LoadSerialNumbers.setFieldValue('custrecord_serialstatus',
							 * '');
							 * 
							 * var recid = nlapiSubmitRecord(LoadSerialNumbers,
							 * false, true); nlapiLogExecution('ERROR', 'Serial
							 * Entry rec id',recid);
							 
							if (ActSerialNos.indexOf(SerialNo) == -1) {
								if (varianceinExpserialNo.indexOf(SerialNo) != -1) {
									nlapiLogExecution(
											'ERROR',
											'SerialNo in ActSerialNos if qty>0: ',
											SerialNo);
									var InternalID = searchResults[i].getId();

									// var currentRow = [SerialNo];
									// localSerialNoArray.push(currentRow);
									// nlapiLogExecution('ERROR',
									// 'localSerialNoArray : ',
									// localSerialNoArray.toString());

									
									 * var LoadSerialNumbers =
									 * nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
									 * LoadSerialNumbers.setFieldValue('custrecord_serialwmsstatus',
									 * '32');
									 * 
									 * var recid =
									 * nlapiSubmitRecord(LoadSerialNumbers,
									 * false, true); nlapiLogExecution('ERROR',
									 * 'Serial Entry rec id',recid);
									 

									var fieldNames = new Array();
									var newValues = new Array();

									fieldNames
											.push('custrecord_serialwmsstatus');
									newValues.push('32');

									nlapiSubmitField(
											'customrecord_ebiznetserialentry',
											InternalID, fieldNames, newValues);
								}

								// var id =
								// nlapiDeleteRecord('customrecord_ebiznetserialentry',
								// InternalID);
								// nlapiLogExecution('ERROR', 'Deleted Actual
								// Record : ', id);
							}
						}
					}
				}
			} else if (parseFloat(qty) == 0) {
				var filters = new Array();
				filters[0] = new nlobjSearchFilter(
						'custrecord_serialiteminternalid', null, 'is', item);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid',
						null, 'is', lp);

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults = nlapiSearchRecord(
						'customrecord_ebiznetserialentry', null, filters,
						columns);

				if (searchResults != null && searchResults != "") {
					for ( var i = 0; i < searchResults.length; i++) {
						if (searchResults[i]
								.getValue('custrecord_serialnumber') != null
								&& searchResults[i]
										.getValue('custrecord_serialnumber') != "") {
							var SerialNo = searchResults[i]
									.getValue('custrecord_serialnumber');
							// var InternalID = searchResults[i].getId();

							// added now
							if (ActSerialNos.indexOf(SerialNo) == -1) {
								var InternalID = searchResults[i].getId();
								// nlapiLogExecution('ERROR', 'Serial Entry rec
								// id to tst',InternalID);

								// var currentRow = [SerialNo];
								// localSerialNoArray.push(currentRow);
								// nlapiLogExecution('ERROR',
								// 'localSerialNoArray in resolve and qty ==0:
								// ', localSerialNoArray.toString());

								
								 * var LoadSerialNumbers =
								 * nlapiLoadRecord('customrecord_ebiznetserialentry',InternalID);
								 * LoadSerialNumbers.setFieldValue('custrecord_serialwmsstatus',
								 * '32');
								 * 
								 * var recid =
								 * nlapiSubmitRecord(LoadSerialNumbers, false,
								 * true);
								 
								// nlapiLogExecution('ERROR', 'Serial Entry rec
								// id',recid);
								var fieldNames = new Array();
								var newValues = new Array();

								fieldNames.push('custrecord_serialwmsstatus');
								newValues.push('32');

								nlapiSubmitField(
										'customrecord_ebiznetserialentry',
										InternalID, fieldNames, newValues);

							}
							// added now

							// var id =
							// nlapiDeleteRecord('customrecord_ebiznetserialentry',
							// InternalID);
							// nlapiLogExecution('ERROR', 'Deleted Actual Record
							// : ', id);

						}
					}
				}
			}
		}
		if (restype == 'ignore' || restype == 'recount') {
			if (parseFloat(qty) < 0) {
				var filters = new Array();
				filters[0] = new nlobjSearchFilter(
						'custrecord_serialiteminternalid', null, 'is', item);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid',
						null, 'is', lp);
				filters[2] = new nlobjSearchFilter('custrecord_serialstatus',
						null, 'is', 'D');

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults = nlapiSearchRecord(
						'customrecord_ebiznetserialentry', null, filters,
						columns);

				if (searchResults != null && searchResults != "") {
					for ( var i = 0; i < searchResults.length; i++) {
						if (searchResults[i]
								.getValue('custrecord_serialnumber') != null
								&& searchResults[i]
										.getValue('custrecord_serialnumber') != "") {
							var SerialNo = searchResults[i]
									.getValue('custrecord_serialnumber');
							var InternalID = searchResults[i].getId();

							// var currentRow = [SerialNo];
							// localSerialNoArray.push(currentRow);
							// nlapiLogExecution('ERROR', 'localSerialNoArray :
							// ', localSerialNoArray.toString());

							var LoadSerialNumbers = nlapiLoadRecord(
									'customrecord_ebiznetserialentry',
									InternalID);
							LoadSerialNumbers.setFieldValue(
									'custrecord_serialstatus', '');

							var recid = nlapiSubmitRecord(LoadSerialNumbers,
									false, true);
							nlapiLogExecution('ERROR', 'Serial Entry rec id',
									recid);
						}
					}
				}
			} else if (parseFloat(qty) > 0) {
				var filters = new Array();
				filters[0] = new nlobjSearchFilter(
						'custrecord_serialiteminternalid', null, 'is', item);
				filters[1] = new nlobjSearchFilter('custrecord_serialparentid',
						null, 'is', lp);
				filters[2] = new nlobjSearchFilter('custrecord_serialstatus',
						null, 'is', 'I');

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_serialnumber');
				var searchResults = nlapiSearchRecord(
						'customrecord_ebiznetserialentry', null, filters,
						columns);

				if (searchResults != null && searchResults != "") {
					nlapiLogExecution('ERROR', 'searchResults.length : ',
							searchResults.length);
					for ( var i = 0; i < searchResults.length; i++) {
						if (searchResults[i]
								.getValue('custrecord_serialnumber') != null
								&& searchResults[i]
										.getValue('custrecord_serialnumber') != "") {
							var SerialNo = searchResults[i]
									.getValue('custrecord_serialnumber');
							var InternalID = searchResults[i].getId();

							// var currentRow = [SerialNo];
							// localSerialNoArray.push(currentRow);
							// nlapiLogExecution('ERROR', 'localSerialNoArray :
							// ', localSerialNoArray.toString());

							var id = nlapiDeleteRecord(
									'customrecord_ebiznetserialentry',
									InternalID);
							nlapiLogExecution('ERROR',
									'Deleted Actual Record : ', id);
						}
					}
				}
			}
		}

	} catch (e) {
		nlapiLogExecution('ERROR', 'Catch', e);
	}
	return localSerialNoArray;
}*/

/*function displaysublist(form,searchResults, hiddenfieldshowvalue) {
	var loc, locText, expsku, expskutext, actskutext, actsku, expqty, actqty, explp, actlp, expskustatus, actskustatus;
	var actskustatusvalue, planno, name, opentaskid, intid, siteis, actlot, explot, expskustatusvalue, expserialno, actserialno;
	var dollarvariance,currentinvt;

	
	 * Searching records from custom record and dynamically adding to the
	 * sublist lines
	 
	var i = 0;

	if (searchResults != null && searchResults != "") {
		var orderListArray = new Array();
		for (k = 0; k < searchResults.length; k++) {
			// nlapiLogExecution('ERROR', 'searchResults[k]', searchResults[k]);
			var ordsearchresult = searchResults[k];

			if (ordsearchresult != null) {
				// nlapiLogExecution('ERROR', 'ordsearchresult.length new tst',
				// ordsearchresult.length);
				for ( var j = 0; j < ordsearchresult.length; j++) {
					orderListArray[orderListArray.length] = ordsearchresult[j];
				}
			}
		}

		nlapiLogExecution('ERROR', 'searchresults.length in new sublist bind',
				searchResults.length);
		var ShowAll, ShowDis, ShowNonDis;
		if (request.getParameter('custparam_restypeflag') == 'cyclecountsa'
				&& (request.getParameter('custparam_hiddenfieldshow') == '' || request
						.getParameter('custparam_hiddenfieldshow') == null)) {
			nlapiLogExecution('ERROR', 'ShowAll if', ShowAll);
			// vsa = request.getParameter('restypeflag');
			ShowAll = 'T';
		} else if (request.getParameter('custparam_hiddenfieldshow') == 'ALL') {
			nlapiLogExecution('ERROR', 'ShowAll else', ShowAll);
			ShowAll = 'T';
		}

		if (request.getParameter('custparam_restypeflag') == 'cyclecountsd'
				&& (request.getParameter('custparam_hiddenfieldshow') == '' || request
						.getParameter('custparam_hiddenfieldshow') == null)) {
			nlapiLogExecution('ERROR', 'ShowDis if', ShowDis);
			// vsa = request.getParameter('restypeflag');
			ShowDis = 'T';
		} else if (request.getParameter('custparam_hiddenfieldshow') == 'D') {
			nlapiLogExecution('ERROR', 'ShowDis else', ShowDis);
			ShowDis = 'T';
		}

		if (request.getParameter('custparam_restypeflag') == 'cyclecountsnd'
				&& (request.getParameter('custparam_hiddenfieldshow') == '' || request
						.getParameter('custparam_hiddenfieldshow') == null)) {
			nlapiLogExecution('ERROR', 'ShowNonDis if', ShowNonDis);
			// vsa = request.getParameter('restypeflag');
			ShowNonDis = 'T';
		} else if (request.getParameter('custparam_hiddenfieldshow') == 'ND') {
			nlapiLogExecution('ERROR', 'ShowNonDis else', ShowNonDis);
			ShowNonDis = 'T';
		}

		nlapiLogExecution('ERROR', 'ShowAll in bind', ShowAll);
		nlapiLogExecution('ERROR', 'ShowDis in bind', ShowDis);
		nlapiLogExecution('ERROR', 'ShowNonDis in bind', ShowNonDis);

		for ( var m = 0; m < orderListArray.length; m++) {
			nlapiLogExecution('ERROR', 'm', m);

			loc = orderListArray[m].getValue('custrecord_cycact_beg_loc');
			locText = orderListArray[m].getText('custrecord_cycact_beg_loc');
			expsku = orderListArray[m]
					.getValue('custrecord_cyclecount_exp_ebiz_sku_no');
			expskutext = orderListArray[m].getText('custrecord_cyclesku');
			// actsku = orderListArray[m].getValue('custrecord_cycleact_sku');
			actskutext = orderListArray[m].getText('custrecord_cycleact_sku');
			actsku = orderListArray[m]
					.getValue('custrecord_cyclecount_act_ebiz_sku_no');
			expqty = orderListArray[m].getValue('custrecord_cycleexp_qty');
			actqty = orderListArray[m].getValue('custrecord_cycle_act_qty');
			explp = orderListArray[m].getValue('custrecord_cyclelpno');
			actlp = orderListArray[m].getValue('custrecord_cycact_lpno');

			expserialno = orderListArray[m]
					.getValue('custrecord_cycle_expserialno');
			actserialno = orderListArray[m]
					.getValue('custrecord_cycle_serialno');

			nlapiLogExecution('ERROR', 'expserialno in show discrepancy',
					expserialno);
			nlapiLogExecution('ERROR', 'actserialno in show discrepancy',
					actserialno);

			expskustatus = orderListArray[m]
					.getText('custrecord_expcyclesku_status');
			expskustatusvalue = orderListArray[m]
					.getValue('custrecord_expcyclesku_status');
			actskustatus = orderListArray[m]
					.getText('custrecord_cyclesku_status');
			actskustatusvalue = orderListArray[m]
					.getValue('custrecord_cyclesku_status');

			if (actskustatus == null || actskustatus == "")
				actskustatus = expskustatus;
			if (actskustatusvalue == null || actskustatusvalue == "")
				actskustatusvalue = expskustatusvalue;

			nlapiLogExecution('ERROR', 'actskustatus in show discrepancy',
					actskustatus);
			nlapiLogExecution('ERROR', 'expskustatus in show discrepancy',
					expskustatus);

			planno = orderListArray[m].getValue('custrecord_cycle_count_plan');
			name = orderListArray[m].getValue('custrecord_cycle_count_plan');
			opentaskid = orderListArray[m].getValue('custrecord_opentaskid');
			invtid = orderListArray[m].getValue('custrecord_invtid');
			siteid = orderListArray[m].getValue('custrecord_cyclesite_id');
			actlot = orderListArray[m].getValue('custrecord_cycleabatch_no');
			explot = orderListArray[m].getValue('custrecord_expcycleabatch_no');

			// Code Added By Ganapathi on 26th Nov 2012
			var filters = new Array();
			if (actsku != null && actsku != '')
				filters.push(new nlobjSearchFilter('internalid', null, 'is',
						actsku));
			filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

			var columns = new Array();
			columns[0] = new nlobjSearchColumn('cost');
			columns[1] = new nlobjSearchColumn('averagecost');
			columns[2] = new nlobjSearchColumn('itemid');

			var itemdetails = new nlapiSearchRecord('item', null, filters,
					columns);
			if (itemdetails != null) {
				vItemname = itemdetails[0].getValue('itemid');
				vCost = itemdetails[0].getValue('cost');
				nlapiLogExecution('ERROR', 'vCost', vCost);
				vAvgCost = itemdetails[0].getValue('averagecost');
				nlapiLogExecution('ERROR', 'Average Cost', vAvgCost);
			}

			if (actqty == null || actqty == "") {
				nlapiLogExecution('ERROR', 'actqty:', actqty);
				actqty = 0;
			}

			if (expqty == null || expqty == "") {
				nlapiLogExecution('ERROR', 'expqty: ', expqty);
				expqty = 0;
			}

			dollarvariance = (parseFloat(actqty) - parseFloat(expqty))
					* vAvgCost;

			nlapiLogExecution('ERROR', 'dollarvariance', dollarvariance);

			if (isNaN(dollarvariance) || dollarvariance == null
					|| dollarvariance == "") {
				nlapiLogExecution('ERROR', 'dollarvariance: ', dollarvariance);
				dollarvariance = 0;
			} else {
				dollarvariance = Math.round(dollarvariance, 2);
			}

			nlapiLogExecution('ERROR', 'planno', planno);
			nlapiLogExecution('ERROR','actserialno',actserialno);
			nlapiLogExecution('ERROR','expserialno',expserialno);
			var varianceinExpserialNo = '';
			var varianceinActserialNo = '';
			if (actserialno != null && actserialno != '' && expserialno != null
					&& expserialno != '') {
				actserialno = actserialno.trim();
				expserialno = expserialno.trim();
				
				if (expserialno != null && expserialno != '')
					var getexpserialArr = expserialno.split(',');

				if (actserialno != null && actserialno != '')
					var getactserialArr = actserialno.split(',');

				if (getexpserialArr != null && getexpserialArr != '') {
					for ( var p = 0; p < getexpserialArr.length; p++) {
						if (actserialno != null && actserialno != '') {
							if (actserialno.indexOf(getexpserialArr[p]) == -1)// not
							// found
							// temSeriIdArr
							{
								if (varianceinExpserialNo == "") {
									varianceinExpserialNo = getexpserialArr[p];
								} else {
									varianceinExpserialNo = varianceinExpserialNo
											+ "," + getexpserialArr[p];
								}
							}
						}
					}
				}
				nlapiLogExecution('ERROR', 'varianceinExpserialNo',
						varianceinExpserialNo);

				if (getactserialArr != null && getactserialArr != '') {
					for ( var q = 0; q < getactserialArr.length; q++) {
						if (getexpserialArr != null && getexpserialArr != '') {

							if (getexpserialArr.indexOf(getactserialArr[q]) == -1)// not
							// found
							// temSeriIdArr
							{
								if (varianceinActserialNo == "") {
									varianceinActserialNo = getactserialArr[q];
								} else {
									varianceinActserialNo = varianceinActserialNo
											+ "," + getactserialArr[q];
								}
							}
						}
					}
				}
				nlapiLogExecution('ERROR', 'varianceinActserialNo',
						varianceinActserialNo);
			}

			
			 * nlapiLogExecution('ERROR', 'expqty', expqty);
			 * nlapiLogExecution('ERROR', 'actqty', actqty);
			 * nlapiLogExecution('ERROR', 'expsku', expsku);
			 * nlapiLogExecution('ERROR', 'actsku', actsku);
			 * nlapiLogExecution('ERROR', 'explot', explot);
			 * nlapiLogExecution('ERROR', 'actlot', actlot);
			 * nlapiLogExecution('ERROR', 'explp', explp);
			 * nlapiLogExecution('ERROR', 'actlp', actlp);
			 * nlapiLogExecution('ERROR', 'expskustatusvalue',
			 * expskustatusvalue); nlapiLogExecution('ERROR',
			 * 'actskustatusvalue', actskustatusvalue);
			 

			nlapiLogExecution('ERROR', 'orderListArray[m].getId()',
					orderListArray[m].getId());
			// var serialnourl = nlapiResolveURL('SUITELET',
			// 'customscript_lprelated_serial', 'customdeploy_lprelated_serial')
			// + "&custparam_item=" + actsku+ "&custparam_planno=" + planno;
			var serialnourl = nlapiResolveURL('SUITELET',
					'customscript_lprelated_serial',
					'customdeploy_lprelated_serial')
					+ "&custparam_expserial="
					+ null
					+ "&custparam_actserial="
					+ null
					+ "&custparam_expskustatus="
					+ expskustatusvalue
					+ "&custparam_actskustatus="
					+ actskustatusvalue
					+ "&custparam_recid=" + orderListArray[m].getId();
			// case 20124074 start
			var Invturl = nlapiResolveURL('SUITELET',
					'customscript_ebiz_resolve_expitemurl',
					'customdeploy_ebiz_resolve_expitemurl_di')
					+ "&custparam_expsku="
					+ actsku
					+ "&custparam_planno="
					+ planno;
			// end

			
			var invtfilters = new Array();
			invtfilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null,
					'anyof', loc));
			invtfilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_sku', null,
					'anyof', actsku));
			invtfilters.push(new nlobjSearchFilter('custrecord_ebiz_inv_lp', null,
					'is', actlp));
			invtfilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			
			var invtcolumns = new Array();
			invtcolumns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
			invtcolumns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_loc');
			invtcolumns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
			invtcolumns[3] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
			invtcolumns[4] = new nlobjSearchColumn('custrecord_ebiz_avl_qty');
			invtcolumns[0].setSort();
			var invtsearchresults = nlapiSearchRecord('customrecord_ebiznet_createinv',
					null, invtfilters, invtcolumns);
			if (invtsearchresults != null && invtsearchresults != '')
					currentinvt = invtsearchresults[0]
						.getValue('custrecord_ebiz_avl_qty');

			if (currentinvt == 'undefined' || currentinvt == null || currentinvt == ''
					|| isNaN(currentinvt)) {
				currentinvt = 0;
			}
			// if((request.getParameter('restypeflag')=='cyclecountsd' ||
			// hiddenfieldshowvalue == 'D') && (expqty != actqty || expsku !=
			// actsku || expserialno != actserialno || explot != actlot || explp
			// != actlp ))
			if ((ShowDis == 'T')
					&& (expqty != actqty || expsku != actsku
							|| explot != actlot || explp != actlp
							|| expskustatusvalue != actskustatusvalue
							|| varianceinExpserialNo != "" || varianceinActserialNo != "")) {
				i = i + 1;
				nlapiLogExecution('ERROR',
						'request.getParameter(restypeflag) in if binding',
						request.getParameter('custparam_restypeflag'));
				nlapiLogExecution('ERROR',
						'hiddenfieldshowvalue in if binding',
						hiddenfieldshowvalue);
				nlapiLogExecution('ERROR', 'orderListArray[m].getId() inside',
						orderListArray[m].getId());

				
				 * form.getSubList('custpage_items').setLineItemValue('custpage_locationtext',
				 * i, locText);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_location',
				 * i, loc);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_expsku',
				 * i, expsku);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_expskutext',
				 * i, expskutext);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_actsku',
				 * i, actsku);
				 * //form.getSubList('custpage_items').setLineItemValue('custpage_actskutext',
				 * i, actskutext); //case 20124074 start
				 * form.getSubList('custpage_items').setLineItemValue('custpage_actskutextlink',
				 * i, "<a href=\"#\" onclick=\"javascript:window.open('" +
				 * Invturl + "');\" >" + actskutext + "</a>"); //end
				 * form.getSubList('custpage_items').setLineItemValue('custpage_expqty',
				 * i, expqty);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_actqty',
				 * i, actqty);
				 * 
				 * form.getSubList('custpage_items').setLineItemValue('custpage_dollarvariance',
				 * i, dollarvariance);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_qtyvariance',
				 * i, parseFloat(actqty) - parseFloat(expqty));
				 * form.getSubList('custpage_items').setLineItemValue('custpage_qtytobeadjust',
				 * i, parseFloat(actqty) - parseFloat(expqty));
				 * form.getSubList('custpage_items').setLineItemValue('custpage_explp',
				 * i, explp);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_actlp',
				 * i, actlp);
				 * 
				 * //form.getSubList('custpage_items').setLineItemValue('custpage_expserial',
				 * i, expserialno);
				 * //form.getSubList('custpage_items').setLineItemValue('custpage_actserial',
				 * i, actserialno);
				 * 
				 * 
				 * 
				 * form.getSubList('custpage_items').setLineItemValue('custpage_expbatch',
				 * i, explot);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_actbatch',
				 * i, actlot);
				 * 
				 * form.getSubList('custpage_items').setLineItemValue('custpage_expskustatus',
				 * i, expskustatus);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_expskustatusvalue',
				 * i, expskustatusvalue);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_actskustatus',
				 * i, actskustatus);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_actskustatusvalue',
				 * i, actskustatusvalue);
				 * 
				 * form.getSubList('custpage_items').setLineItemValue('custpage_id',
				 * i, orderListArray[m].getId());
				 * form.getSubList('custpage_items').setLineItemValue('custpage_planno',
				 * i, planno);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_name',
				 * i, name);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_opentaskid',
				 * i, opentaskid);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_invtid',
				 * i, invtid);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_site',
				 * i, siteid);
				 

				
				
				
				

				strxml += "<tr><td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += locText;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += expskutext;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += actskutext;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += expqty;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += actqty;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += (parseFloat(actqty) - parseFloat(expqty));
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += dollarvariance;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += explp;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += actlp;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += explot;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += actlot;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += expskustatus;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += actskustatus;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += currentinvt;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += (parseFloat(actqty) - parseFloat(expqty));
				strxml += "</td></tr>";
				
				
				
				
				
				
				
				
				
				
			}

			// else if((request.getParameter('restypeflag')=='cyclecountsnd' ||
			// hiddenfieldshowvalue == 'ND') && (expqty == actqty && expsku ==
			// actsku && expserialno == actserialno && explot == actlot && explp
			// == actlp ))
			else if ((ShowNonDis == 'T')
					&& (expqty == actqty && expsku == actsku
							&& explot == actlot && explp == actlp
							&& expskustatusvalue == actskustatusvalue
							&& varianceinExpserialNo == "" && varianceinActserialNo == "")) {

				i = i + 1;
				nlapiLogExecution('ERROR',
						'request.getParameter(restypeflag) in else binding',
						request.getParameter('custparam_restypeflag'));
				nlapiLogExecution('ERROR',
						'hiddenfieldshowvalue in else binding',
						hiddenfieldshowvalue);
				nlapiLogExecution('ERROR',
						'orderListArray[m].getId() else inside',
						orderListArray[m].getId());

				
				 * form.getSubList('custpage_items').setLineItemValue('custpage_locationtext',
				 * i, locText);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_location',
				 * i, loc);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_expsku',
				 * i, expsku);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_expskutext',
				 * i, expskutext);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_actsku',
				 * i, actsku);
				 * //form.getSubList('custpage_items').setLineItemValue('custpage_actskutext',
				 * i, actskutext);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_actskutextlink',
				 * i, "<a href=\"#\" onclick=\"javascript:window.open('" +
				 * serialnourl + "');\" >" + actskutext + "</a>");
				 * form.getSubList('custpage_items').setLineItemValue('custpage_expqty',
				 * i, expqty);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_actqty',
				 * i, actqty);
				 * 
				 * form.getSubList('custpage_items').setLineItemValue('custpage_dollarvariance',
				 * i, dollarvariance);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_qtyvariance',
				 * i, parseFloat(actqty) - parseFloat(expqty));
				 * form.getSubList('custpage_items').setLineItemValue('custpage_qtytobeadjust',
				 * i, parseFloat(actqty) - parseFloat(expqty));
				 * form.getSubList('custpage_items').setLineItemValue('custpage_explp',
				 * i, explp);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_actlp',
				 * i, actlp);
				 * 
				 * //form.getSubList('custpage_items').setLineItemValue('custpage_expserial',
				 * i, expserialno);
				 * //form.getSubList('custpage_items').setLineItemValue('custpage_actserial',
				 * i, actserialno);
				 * 
				 * 
				 * form.getSubList('custpage_items').setLineItemValue('custpage_expbatch',
				 * i, explot);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_actbatch',
				 * i, actlot);
				 * 
				 * form.getSubList('custpage_items').setLineItemValue('custpage_expskustatus',
				 * i, expskustatus);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_expskustatusvalue',
				 * i, expskustatusvalue);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_actskustatus',
				 * i, actskustatus);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_actskustatusvalue',
				 * i, actskustatusvalue);
				 * 
				 * form.getSubList('custpage_items').setLineItemValue('custpage_id',
				 * i, orderListArray[m].getId());
				 * form.getSubList('custpage_items').setLineItemValue('custpage_planno',
				 * i, planno);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_name',
				 * i, name);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_opentaskid',
				 * i, opentaskid);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_invtid',
				 * i, invtid);
				 * form.getSubList('custpage_items').setLineItemValue('custpage_site',
				 * i, siteid);
				 
				
				
				
				strxml += "<tr><td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += locText;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += expskutext;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += actskutext;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += expqty;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += actqty;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += (parseFloat(actqty) - parseFloat(expqty));
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += dollarvariance;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += explp;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += actlp;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += explot;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += actlot;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += expskustatus;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += actskustatus;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += currentinvt;
				strxml += "</td>";
				
				strxml += "<td width='16%' style='border-width: 1px; border-color: #000000; font-size:small;'>";
				strxml += (parseFloat(actqty) - parseFloat(expqty));
				strxml += "</td></tr>";
				
				
			}
		}
	}
}*/


/*function GetNSCallFlag(Status,NewStatus)
{
	nlapiLogExecution('ERROR', 'Into GetNSCallFlag', '');
	nlapiLogExecution('ERROR', 'Status', Status);
	nlapiLogExecution('ERROR', 'New Status', NewStatus);
	var result=new Array();
	var retFlag='N';
	var OldMapLocation;
	var NewMapLocation;
	var OldLocation;
	var NewLocation;
	if(Status!=null && Status!="")
	{
		var OldIStatus = nlapiLoadRecord('customrecord_ebiznet_sku_status', Status);
		if(OldIStatus!= null && OldIStatus != "")
		{
			OldMapLocation=OldIStatus.getFieldValue('custrecord_item_status_location_map');
			OldLocation=OldIStatus.getFieldValue('custrecord_ebizsiteskus');
			OldIStatus=null;
		} 
	}
	if(NewStatus!=null && NewStatus!="")
	{
		var NewIStatus = nlapiLoadRecord('customrecord_ebiznet_sku_status', NewStatus);
		if(NewIStatus!= null && NewIStatus != "")
		{
			NewMapLocation=NewIStatus.getFieldValue('custrecord_item_status_location_map');
			NewLocation=NewIStatus.getFieldValue('custrecord_ebizsiteskus');
		} 
	}
	nlapiLogExecution('ERROR', 'OldLocation', OldLocation);
	nlapiLogExecution('ERROR', 'NewLocation', NewLocation);
	if(OldMapLocation != null && OldMapLocation !="" && NewMapLocation != null && NewMapLocation != "")
	{
		if(OldMapLocation==NewMapLocation)
			//if(OldLocation==NewLocation)
		{
			retFlag='N';
			result.push('N');
		}
		else
		{
			result.push('Y');
		}
		result.push(OldMapLocation);
		result.push(NewMapLocation);
		result.push(OldLocation);
		result.push(NewLocation);
	}
	nlapiLogExecution('ERROR', 'retFlag', retFlag);
	nlapiLogExecution('ERROR', 'OldMapLocation', OldMapLocation);
	nlapiLogExecution('ERROR', 'NewMapLocation', NewMapLocation);
	nlapiLogExecution('ERROR', 'Out of GetNSCallFlag', '');
	return result;
}*/


/*function GetMWFlag(WHLocation)
{
	nlapiLogExecution('ERROR', 'Into GetMWFlag : WHLocation', WHLocation);
	var retFlag='Y';
	var MWHSiteFlag;

	if(WHLocation!=null && WHLocation!="")
	{
		var filters = new Array();
		filters.push(new nlobjSearchFilter('internalid', null, 'is', WHLocation));
		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_ebizwhsite')); 

		var WHOld = nlapiSearchRecord('location', null, filters, columns);

		if(WHOld!= null && WHOld != "" && WHOld.length>0)
		{
			MWHSiteFlag=WHOld[0].getValue('custrecord_ebizwhsite');
			nlapiLogExecution('ERROR', 'MWHSiteFlag', MWHSiteFlag);
			WHOld=null;
		} 
	} 
	if(MWHSiteFlag != null && MWHSiteFlag !="" )
	{
		if(MWHSiteFlag=='F')
			retFlag='N';
		else
			retFlag='Y';

	}
	nlapiLogExecution('ERROR', 'retFlag', retFlag);
	nlapiLogExecution('ERROR', 'Out of GetMWFlag ', '');
	return retFlag;
}*/

/*function  InvokeNSInventoryTransfer(item,itemstatus,loc,toloc,qty,lot)
{
	try{
		nlapiLogExecution('ERROR', 'Into InvokeNSInventoryTransfer');		
		nlapiLogExecution('ERROR', 'item', item);		
		nlapiLogExecution('ERROR', 'itemstatus', itemstatus);
		nlapiLogExecution('ERROR', 'loc', loc);
		nlapiLogExecution('ERROR', 'toloc', toloc);
		nlapiLogExecution('ERROR', 'qty', qty);
		nlapiLogExecution('ERROR', 'lot', lot);

		var ItemType='';

		if(item!= null && item != "")
		{
			var columns = nlapiLookupField('item', item, [ 'recordType','custitem_ebizserialout' ]);
			ItemType = columns.recordType;
		}

		nlapiLogExecution('ERROR', 'ItemType', ItemType);
		var confirmLotToNS='Y';
		confirmLotToNS=GetConfirmLotToNS(loc);
		var vItemname;	

		var filters = new Array();          
		filters.push(new nlobjSearchFilter('internalid', null, 'is',item));
		// Changed On 30/4/12 by Suman
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		// End of Changes as On 30/4/12
		var columns = new Array();		
		columns[0] = new nlobjSearchColumn('itemid');
		var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
		if (itemdetails !=null) 
		{
			vItemname=itemdetails[0].getValue('itemid');
		}

		var invttransfer = nlapiCreateRecord('inventorytransfer');			

		var subs = nlapiGetContext().getFeature('subsidiaries');		
		if(subs==true)
		{
			var vSubsidiaryVal=getSubsidiaryNew(loc);
			if(vSubsidiaryVal != null && vSubsidiaryVal != '')
				invttransfer.setFieldValue('subsidiary', vSubsidiaryVal);
		}		

		invttransfer.setFieldValue('location', loc);//from Location
		invttransfer.setFieldValue('transferlocation', toloc);
		invttransfer.selectNewLineItem('inventory');
		invttransfer.setCurrentLineItemValue('inventory', 'item', item);		
		invttransfer.setCurrentLineItemValue('inventory', 'adjustqtyby', parseFloat(qty));


		if (ItemType == "lotnumberedinventoryitem" || ItemType == "lotnumberedassemblyitem") 
			//if(lot!=null && lot!='')
		{

			vItemname=vItemname.replace(/ /g,"-");

			if(confirmLotToNS=='N')
				lot=vItemname;	

			//For advanced Bin serial Lot management check
			var vAdvBinManagement=false;
			var ctx = nlapiGetContext();
			if(ctx != null && ctx != '')
			{
				if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
					vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
			}  
			nlapiLogExecution('ERROR', 'vAdvBinManagement', vAdvBinManagement);

			if(vAdvBinManagement)//If advanced bin serial lot management check is true then we are creating subrecord for lot/serial
			{
				var compSubRecord = invttransfer.createCurrentLineItemSubrecord('inventory','inventorydetail');
				nlapiLogExecution('ERROR', 'test1', 'test1');
				compSubRecord.selectNewLineItem('inventoryassignment');
				compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', qty);
				//compSubRecord.setCurrentLineItemValue('inventoryassignment', 'binnumber', vSubsidiaryVal);
				compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lot);

				compSubRecord.commitLineItem('inventoryassignment');
				compSubRecord.commit();
			}
			else
			{
				if(parseFloat(qty)<0)
					qty=parseFloat(qty)*(-1);
				nlapiLogExecution('ERROR', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");
				invttransfer.setCurrentLineItemValue('inventory', 'serialnumbers', lot + "(" + parseFloat(qty) + ")");
			}
		}

		invttransfer.commitLineItem('inventory'); 
		nlapiSubmitRecord(invttransfer, true, true);
		nlapiLogExecution('ERROR', 'Out of InvokeNSInventoryTransfer');
	}


	catch(exp) {
		nlapiLogExecution('ERROR', 'Exception in InvokeNSInventoryTransferNew ', exp);	

	}
}*/

/*function createInvtAdjtRecord(binloc,skuno,expqty,explp,accoutno,whsite,adjustqty,serialnumbers,NSAdjustid)
{
	var invAdjustCustRecord = nlapiCreateRecord('customrecord_ebiznet_invadj'); 
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_binloc', binloc);
	invAdjustCustRecord.setFieldValue('custrecord_ebizskuno', skuno);
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_expeqty', parseFloat(expqty).toFixed(4));
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_invjlpno', explp);

	if(accoutno!=null && accoutno!='')
	{
		invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjtype', accoutno);
	}

	invAdjustCustRecord.setFieldValue('custrecord_ebiz_siteid', whsite);
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjustqty', parseFloat(adjustqty).toFixed(4));
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_tasktype', 7);
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_recorddate', DateStamp());
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_rectime', TimeStamp());
	invAdjustCustRecord.setFieldValue('custrecord_ebiz_updateduserno', nlapiGetContext().getUser());
	if(NSAdjustid != null && NSAdjustid != '')
		invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjustno', NSAdjustid);//NS asdjustment id
	if(serialnumbers != null && serialnumbers != '')
	{
		invAdjustCustRecord.setFieldValue('custrecord_ebiz_adjserialnumbers', serialnumbers);
	}

	var id = nlapiSubmitRecord(invAdjustCustRecord, false, true);
}*/

/*function getSKUCubeAndWeight(skuNo, uom){
	nlapiLogExecution('ERROR', 'getSKUCubeAndWeight', 'Start');
	var timestamp1 = new Date();
	var cube = 0;
	var BaseUOMQty = 0;    
	var dimArray = new Array();
	if (uom == "")
		uom = 1;

	nlapiLogExecution('DEBUG', 'getSKUCubeAndWeight:SKU info', skuNo);
	nlapiLogExecution('DEBUG', 'getSKUCubeAndWeight:UOM', uom);

	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ebizitemdims', null, 'is', skuNo);
	//case # 20123248 start
	filters[1] = new nlobjSearchFilter('custrecord_ebizuomskudim', null, 'is', uom);
	//case # 20123248 end
	filters[2] = new nlobjSearchFilter('custrecord_ebizbaseuom', null, 'is', 'T');

	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebizcube');
	columns[1] = new nlobjSearchColumn('custrecord_ebizqty');

	var t1 = new Date();
	var skuDimsSearchResults = nlapiSearchRecord('customrecord_ebiznet_skudims', null, filters, columns);
	var t2 = new Date();
	nlapiLogExecution('ERROR', 'nlapiSearchRecord:getSKUCubeAndWeight:skudims',
			getElapsedTimeDuration(t1, t2));

	if(skuDimsSearchResults != null){
		for (var i = 0; i < skuDimsSearchResults.length; i++) {
			var skuDim = skuDimsSearchResults[i];
			cube = skuDim.getValue('custrecord_ebizcube');
			BaseUOMQty = skuDim.getValue('custrecord_ebizqty');
		}
	}

	dimArray[0] = cube;
	dimArray[1] = BaseUOMQty;

	//dimArray["BaseUOMItemCube"] = cube;
	//dimArray["BaseUOMQty"] = BaseUOMQty;

	var timestamp2 = new Date();
	nlapiLogExecution('DEBUG', 'getSKUCubeAndWeight Duration', getElapsedTimeDuration(timestamp1, timestamp2));

	nlapiLogExecution('ERROR', 'getSKUCubeAndWeight:Cube', cube);
	nlapiLogExecution('ERROR', 'getSKUCubeAndWeight:qty', BaseUOMQty);
	nlapiLogExecution('ERROR', 'getSKUCubeAndWeight:length', dimArray.length);
	nlapiLogExecution('ERROR', 'getSKUCubeAndWeight:Cube1', dimArray[0]);
	nlapiLogExecution('ERROR', 'getSKUCubeAndWeight:qty1', dimArray[1]);
	return dimArray;
}*/

/*function GeteLocCube(LocID){
	var remainingCube = 0;
	try {
		var fields = ['custrecord_remainingcube'];
		var columns= nlapiLookupField('customrecord_ebiznet_location',LocID,fields);
		remainingCube = columns.custrecord_remainingcube;
	}catch(exception) {
		nlapiLogExecution('DEBUG', 'GeteLocCube:Lookup for remainingCube failed', exception);
	}
	nlapiLogExecution('DEBUG', 'GeteLocCube:Remaining Cube for Location', remainingCube);
	return remainingCube;
}*/

/*function getLotBatchId(lotbatchtext)
{
	var batchid='';
	var filters = new Array();          
	filters.push(new nlobjSearchFilter('custrecord_ebizlotbatch', null, 'is',lotbatchtext));
	filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid');
	var batchdetails = new nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filters, columns);
	if (batchdetails !=null && batchdetails !='' && batchdetails.length>0) 
	{
		batchid=batchdetails[0].getValue('internalid');
	}
	return batchid;
}*/


/**
 * Passing item,item status, qty and location to NS
 * 
 * @param item
 * @param itemstatus
 * @param loc
 * @param qty
 * @param notes
 */
/*function  InvokeNSInventoryAdjustmentNew(item,itemstatus,loc,qty,notes,tasktype,adjusttype,lot,AccNo)
{
	try{
		var vCost;
		var vAvgCost;
		var vItemname;
		var NSid='';
		var confirmLotToNS='N';
		nlapiLogExecution('ERROR', 'item::', item);
		nlapiLogExecution('ERROR', 'itemstatus::', itemstatus);
		nlapiLogExecution('ERROR', 'Location info::', loc);
		nlapiLogExecution('ERROR', 'Task Type::', tasktype);
		nlapiLogExecution('ERROR', 'Adjustment Type::', adjusttype);
		nlapiLogExecution('ERROR', 'qty::', qty);
		nlapiLogExecution('ERROR', 'AccNo::', AccNo);
		nlapiLogExecution('ERROR', 'lot::', lot);
		//alert("AccNo :: "+ AccNo);
		confirmLotToNS=GetConfirmLotToNS(loc);

		var filters = new Array();          
		filters.push(new nlobjSearchFilter('internalid', null, 'is',item));

		// Changed On 30/4/12 by Suman
		filters.push(new nlobjSearchFilter('isinactive',null, 'is','F'));
		// End of Changes as On 30/4/12
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('cost');
		columns[1] = new nlobjSearchColumn('averagecost');
		columns[2] = new nlobjSearchColumn('itemid');

		var itemdetails = new nlapiSearchRecord('item', null, filters, columns);
		//alert("itemdetails " + itemdetails);
		if (itemdetails !=null) 
		{
			vItemname=itemdetails[0].getValue('itemid');
			vCost = itemdetails[0].getValue('cost');
			nlapiLogExecution('ERROR', 'vCost', vCost);
			vAvgCost = itemdetails[0].getValue('averagecost');
			nlapiLogExecution('ERROR', 'Average Cost', vAvgCost);	         
		}		

		var vItemMapLocation =  getItemStatusMapLoc(itemstatus);	
		nlapiLogExecution('ERROR', 'vItemMapLocation', vItemMapLocation);
		if(AccNo==null || AccNo=="")
		{		
			AccNo = getAccountNo(loc,vItemMapLocation);
			nlapiLogExecution('ERROR', 'Fetcht Account from Inventory AccountNo', vAccountNo);
		}
		//alert("NS Qty "+ parseFloat(qty));
//case # 20124072
		//if(lot==null || lot=='' || lot=='null')
		//{//end
			var outAdj = nlapiCreateRecord('inventoryadjustment');			

			outAdj.setFieldValue('account', AccNo);
			outAdj.setFieldValue('memo', notes);
			outAdj.setFieldValue('adjlocation', vItemMapLocation);
			outAdj.selectNewLineItem('inventory');
			outAdj.setCurrentLineItemValue('inventory', 'item', item);
			outAdj.setCurrentLineItemValue('inventory', 'location', vItemMapLocation);	
			outAdj.setCurrentLineItemValue('inventory', 'adjustqtyby', parseFloat(qty));
		//}


		//alert('Hi1');
		//alert("vAvgCost "+ vAvgCost);
		if(vAvgCost != null &&  vAvgCost != "" && outAdj!=null && outAdj!='' && outAdj!='null')	
		{
			nlapiLogExecution('ERROR', 'into if cond vAvgCost', vAvgCost);
			outAdj.setCurrentLineItemValue('inventory', 'unitcost', vAvgCost);
		}
		else
		{
			nlapiLogExecution('ERROR', 'into else cond.unit cost', vCost);
			if(outAdj!=null && outAdj!='' && outAdj!='null')	
				outAdj.setCurrentLineItemValue('inventory', 'unitcost',  vCost);
		}

		var vAdvBinManagement=false;

		var ctx = nlapiGetContext();
		if(ctx != null && ctx != '')
		{
			if(ctx.getFeature('advbinseriallotmgmt') != null && ctx.getFeature('advbinseriallotmgmt') != '')
				vAdvBinManagement=ctx.getFeature('advbinseriallotmgmt');
		}  
		nlapiLogExecution('ERROR', 'vAdvBinManagement', vAdvBinManagement);

		if(lot!=null && lot!='')
		{
			var fields = ['recordType','custitem_ebizserialin'];
			var columns = nlapiLookupField('item', item, fields);
			var vItemType = columns.recordType;
			nlapiLogExecution('ERROR','vItemType',vItemType);

			var serialInflg="F";		
			serialInflg = columns.custitem_ebizserialin;	

			if(vAdvBinManagement)
			{
				if (vItemType == "lotnumberedinventoryitem" || vItemType == "lotnumberedassemblyitem")
				{
					var compSubRecord = outAdj.createCurrentLineItemSubrecord('inventory','inventorydetail');

					if(confirmLotToNS=='N')
						lot=vItemname;			

					nlapiLogExecution('ERROR', 'lot', lot);

					compSubRecord.selectNewLineItem('inventoryassignment');
					compSubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', qty);
					compSubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lot);

					compSubRecord.commitLineItem('inventoryassignment');

					compSubRecord.commit();

				}
			}
			if (vItemType == "lotnumberedinventoryitem" || vItemType=="lotnumberedassemblyitem")
			{
				vItemname=vItemname.replace(/ /g,"-");

				if(parseFloat(qty)<0)
					qty=parseFloat(qty)*(-1);

				if(confirmLotToNS=='N')
					lot=vItemname;			

				nlapiLogExecution('ERROR', 'lot', lot);

				nlapiLogExecution('ERROR', 'LotNowithQty', lot + "(" + parseFloat(qty) + ")");
				outAdj.setCurrentLineItemValue('inventory', 'serialnumbers', lot + "(" + parseFloat(qty) + ")");
			}
			else if (vItemType == "serializedinventoryitem" || vItemType == "serializedassemblyitem" || serialInflg == "T")
			{
				var cnt=lot.split('@');

				for(var n=0;n<cnt.length;n++)
				{
					var lotnumbers=cnt[n];
					if(lotnumbers!=null && lotnumbers!='' && lotnumbers!='null')
					{
						var vItemMapLocation =  getItemStatusMapLoc(itemstatus);	
						nlapiLogExecution('ERROR', 'vItemMapLocation', vItemMapLocation);
						if(AccNo==null || AccNo=="")
						{		
							AccNo = getAccountNo(loc,vItemMapLocation);
							nlapiLogExecution('ERROR', 'Fetcht Account from Inventory AccountNo', vAccountNo);
						}
						var outAdj = nlapiCreateRecord('inventoryadjustment');	
						outAdj.setFieldValue('account', AccNo);
						outAdj.setFieldValue('memo', notes);
						outAdj.setFieldValue('adjlocation', vItemMapLocation);
						outAdj.selectNewLineItem('inventory');
						outAdj.setCurrentLineItemValue('inventory', 'item', item);
						outAdj.setCurrentLineItemValue('inventory', 'location', vItemMapLocation);	
						outAdj.setCurrentLineItemValue('inventory', 'adjustqtyby', parseFloat(qty));
						if(vAvgCost != null &&  vAvgCost != "" && outAdj!=null && outAdj!='' && outAdj!='null')	
						{
							nlapiLogExecution('ERROR', 'into if cond vAvgCost', vAvgCost);
							outAdj.setCurrentLineItemValue('inventory', 'unitcost', vAvgCost);
						}
						else
						{
							nlapiLogExecution('ERROR', 'into else cond.unit cost', vCost);
							if(outAdj!=null && outAdj!='' && outAdj!='null')	
								outAdj.setCurrentLineItemValue('inventory', 'unitcost',  vCost);
						}

						nlapiLogExecution('ERROR', 'lot with serial no', lotnumbers);

						nlapiLogExecution('ERROR', 'LotNowithQty', lotnumbers);
						outAdj.setCurrentLineItemValue('inventory', 'serialnumbers', lotnumbers);


						var subs = nlapiGetContext().getFeature('subsidiaries');
						nlapiLogExecution('ERROR', 'subs', subs);
						if(subs==true)
						{
							var vSubsidiaryVal=getSubsidiaryNew(loc);
							if(vSubsidiaryVal != null && vSubsidiaryVal != '')
								outAdj.setFieldValue('subsidiary', vSubsidiaryVal);
							nlapiLogExecution('ERROR', 'vSubsidiaryVal', vSubsidiaryVal);
						}
						outAdj.commitLineItem('inventory');
						NSid=nlapiSubmitRecord(outAdj, true, true);
						//alert("Success NS");
						nlapiLogExecution('ERROR', 'NSid', NSid);
					}
				}	
			}
			//}
		}
		//alert('Hi3');
		if (vItemType != "serializedinventoryitem" && vItemType != "serializedassemblyitem" && serialInflg != "T")
		{
			var vItemMapLocation =  getItemStatusMapLoc(itemstatus);	
			nlapiLogExecution('ERROR', 'vItemMapLocation', vItemMapLocation);
			if(AccNo==null || AccNo=="")
			{		
				AccNo = getAccountNo(loc,vItemMapLocation);
				nlapiLogExecution('ERROR', 'Fetcht Account from Inventory AccountNo', vAccountNo);
			}
			var subs = nlapiGetContext().getFeature('subsidiaries');
			nlapiLogExecution('ERROR', 'subs', subs);
			if(subs==true)
			{
				var vSubsidiaryVal=getSubsidiaryNew(loc);
				if(vSubsidiaryVal != null && vSubsidiaryVal != '')
					outAdj.setFieldValue('subsidiary', vSubsidiaryVal);
				nlapiLogExecution('ERROR', 'vSubsidiaryVal', vSubsidiaryVal);
			}
			outAdj.commitLineItem('inventory');
			NSid=nlapiSubmitRecord(outAdj, true, true);
			//alert("Success NS");
			nlapiLogExecution('ERROR', 'NSid', NSid);

			nlapiLogExecution('ERROR', 'type argument', 'type is create');
		}
	}


	catch(e) {
		if (e instanceof nlobjError) 
		{
			// nlapiLogExecution('ERROR', 'system error', e.getCode() + '\n' + e.getDetails());
			nlapiLogExecution('ERROR','system error' + '\n' + 'Error: ' + e.getCode()+ '\n' + e.getDetails() );
		}

		else 
		{
			//nlapiLogExecution('ERROR', 'unexpected error', e.toString());
			nlapiLogExecution('ERROR','unexpected error' + '\n' + 'Error: ' + e.toString() );
		}

		nlapiLogExecution('ERROR', 'Exception in InvokeNSInventoryAdjustmentNew ', e);
		//alert("Error" + exp.toString());

	}
	return NSid;
}*/

/*function getAccountNo(location,itemstatusmaploc)
{
	nlapiLogExecution('ERROR', 'location:', location);
	nlapiLogExecution('ERROR', 'itemstatusmaploc:', itemstatusmaploc);
	var accountno;	
	var filtersAccNo = new Array(); 
	filtersAccNo.push(new nlobjSearchFilter('custrecord_location', null, 'is', location));
	filtersAccNo.push(new nlobjSearchFilter('custrecord_inventorynslocation', null, 'is', itemstatusmaploc));        						
	var colsAcc = new Array();
	colsAcc[0]=new nlobjSearchColumn('custrecord_accountno');                
	var Accsearchresults = nlapiSearchRecord('customrecord_inventoryaccountno', null, filtersAccNo, colsAcc);
	nlapiLogExecution('ERROR', 'Accsearchresults', Accsearchresults);
	if(Accsearchresults!=null)
		accountno = Accsearchresults[0].getValue('custrecord_accountno');	
	return accountno;
}*/

/*function getStockAdjustmentAccountNoNew(loc,tasktype,adjusttype)
{	
	var StAdjustmentAccountNo=new Array();
	var filterStAccNo = new Array();
	var colsStAccNo = new Array();
	//alert("loc : "+loc);
	if(loc!=null)
		filterStAccNo.push(new nlobjSearchFilter('custrecord_ebiz_adjtype_location',null,'anyof',[loc]));	

	if(tasktype!=null)
		filterStAccNo.push(new nlobjSearchFilter('custrecord_adjusttasktype',null,'anyof',[tasktype]));


	if(adjusttype!=null && adjusttype!="")
		filterStAccNo.push(new nlobjSearchFilter('internalid',null,'is',adjusttype));

	filterStAccNo.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	colsStAccNo[0] = new nlobjSearchColumn('custrecord_ebiz_mapto_ns_glaccount');
	colsStAccNo[1] = new nlobjSearchColumn('custrecord_ebiz_to_ns_glaccount');
	colsStAccNo[2] = new nlobjSearchColumn('internalid');

	var StockAdjustAccResults = nlapiSearchRecord('customrecord_ebiznet_stockadj_types', null, filterStAccNo, colsStAccNo);
	//alert("StockAdjustAccResults " + StockAdjustAccResults);
	//alert("StockAdjustAccResults " + StockAdjustAccResults.length);
	if(StockAdjustAccResults !=null && StockAdjustAccResults != "" && StockAdjustAccResults.length>0)
	{
		//alert("1 " + StockAdjustAccResults[0].getValue('custrecord_ebiz_mapto_ns_glaccount'));
		StAdjustmentAccountNo.push(StockAdjustAccResults[0].getValue('custrecord_ebiz_mapto_ns_glaccount'));
		StAdjustmentAccountNo.push(StockAdjustAccResults[0].getValue('internalid'));

		//alert("2 " + StockAdjustAccResults[0].getValue('custrecord_ebiz_to_ns_glaccount'));
		if(StockAdjustAccResults[0].getValue('custrecord_ebiz_to_ns_glaccount') != null && StockAdjustAccResults[0].getValue('custrecord_ebiz_to_ns_glaccount') != "")
			StAdjustmentAccountNo.push(StockAdjustAccResults[0].getValue('custrecord_ebiz_to_ns_glaccount'));


	}	

	return StAdjustmentAccountNo;	
}*/

/**
 * Updates the bin location volume for the specified location
 * @param locnId
 * @param remainingCube
 */
/*function UpdateLocCubeNew(locnId, remainingCube){
	//alert("Val1 "+ locnId);
	//alert("Val2 "+ remainingCube);
	var fields = new Array();
	var values = new Array();
	fields[0] = 'custrecord_remainingcube';
	values[0] = parseFloat(remainingCube).toFixed(4);
	//alert("parse "+ parseFloat(remainingCube));
	//nlapiLogExecution('ERROR','Location Internal Id', locnId);

	//nlapiLogExecution('ERROR','Location Internal IdremainingCube', remainingCube);

	//alert("into");
	//var t1 = new Date();
	nlapiSubmitField('customrecord_ebiznet_location', locnId, fields, values);
	//alert("Success");
	//var t2 = new Date();
	//nlapiLogExecution('ERROR', 'nlapiSubmitField:UpdateLocCube:location', getElapsedTimeDuration(t1, t2));
}*/



