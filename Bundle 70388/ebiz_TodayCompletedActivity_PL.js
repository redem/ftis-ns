/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Portlet/Attic/ebiz_TodayCompletedActivity_PL.js,v $
 *     	   $Revision: 1.1.2.3.4.2.4.4.4.2 $
 *     	   $Date: 2015/08/31 10:08:56 $
 *     	   $Author: rrpulicherla $
 *     	   $Name: t_eBN_2015_2_StdBundle_1_273 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_TodayCompletedActivity_PL.js,v $
 * Revision 1.1.2.3.4.2.4.4.4.2  2015/08/31 10:08:56  rrpulicherla
 * Case#201413343
 *
 * Revision 1.1.2.3.4.2.4.4.4.1  2014/12/31 13:23:17  spendyala
 * Prod issue fixes
 *
 * Revision 1.1.2.3.4.2.4.4  2013/09/30 15:59:44  rmukkera
 * Case#  20124376
 *
 * Revision 1.1.2.3.4.2.4.3  2013/09/20 15:22:59  rmukkera
 * Case# 20124546
 *
 * Revision 1.1.2.3.4.2.4.2  2013/07/05 23:36:57  spendyala
 * case# 20123312
 * Invalid internal id issue fixed.
 *
 * Revision 1.1.2.3.4.2.4.1  2013/03/05 14:56:05  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Lexjet production as part of Standard bundle
 *
 * Revision 1.1.2.3.4.2  2012/11/01 14:55:22  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.1.2.3.4.1  2012/10/11 10:27:04  gkalla
 * CASE201112/CR201113/LOG201121
 * Pick confirmation and other inventory issues
 *
 * Revision 1.1.2.3  2012/09/05 16:41:23  gkalla
 * CASE201112/CR201113/LOG201121
 * Portlet changes
 *
 * Revision 1.2.4.1  2012/04/02 12:47:17  schepuri
 * CASE201112/CR201113/LOG201121
 * fetching more than 1000 rec
 *
 * Revision 1.2  2011/08/22 11:24:52  rmukkera
 * CASE201112/CR201113/LOG201121
 * source code changes included both open and close tasks
 *
 * Revision 1.1  2011/08/19 16:52:22  skota
 * CASE201112/CR201113/LOG201121
 * Task Summary Portlet
 *
 * 
 *****************************************************************************/

var socount=0;
var cocount=0;

function TodaywarehousePortlet(portlet, column)
{  

	portlet.setTitle('Today\'s Completed Activity');  
	var filters = new Array();    	
	filters[0] = new nlobjSearchFilter('custrecord_ebiz_show_on_dashboard', null, 'is', 'T');
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_ebiz_task_desc');
	var chdata="";var chdl="";var chl=""; 
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_tasktype', null, filters, columns);

	if(searchresults !=null)
	{

		for ( var i = 0; i < searchresults.length; i++) {
			var tasktype = searchresults[i].getValue(columns[0]); 
			tasktype=tasktype.replace(/ /g,"-");
			var id = searchresults[i].getId(); 
			var count = 0;
			socount=0;
			cocount=0;
			nlapiLogExecution('ERROR','main id',id);

			var taskcount=getTaskStatus(id,count);
			if(taskcount!=0)
			{

				chdata=chdata+taskcount+","; 
				chl=chl+taskcount+"|"; 
				// chl=chl+tasktype+"("+taskcount+")"+"|";
				chdl=chdl+tasktype+"("+taskcount+")"+"|";



			}
		}
		chdata = chdata.substring(0, chdata.length-1); 
		chl = chl.substring(0, chl.length-1); 
		chdl = chdl.substring(0, chdl.length-1); 
	}


	//var  content="<div align=center><img src= http://chart.apis.google.com/chart";
	var  content="<div align=center><img src= http://chart.googleapis.com/chart";
	content+="?chxs=0,676767,12.5";
	content+="&chxt=x";
	content+="&chs=460x185";
	content+="&cht=p";
	content+="&chds=500,200";   
	chdata="t:"+chdata;
	content+="&chd="+chdata+"";
	content+="&chds=a";
	content+="&chl="+chl;
	content+="&chdl="+chdl+"";

	content+="&chtt=Today\'s&nbsp;Completed&nbsp;Activity&chco=ff0099|2E8B57|00FF00|0000FF|AFDCEC|307D7E|387C44|FBB917|8A4117|F660AB|817339|87CEEB|FE4557";  
	content+=" /></div>";
	portlet.setHtml( content );



}

function getOpenTaskStatus(status,maxno,sysdate)
{
	/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh as part of Standard bundle***/
	var vRoleLocation=getRoledBasedLocation();
	/*** Up to here ***/ 
	var filtersso = new Array();

	filtersso[0] = new nlobjSearchFilter('custrecord_tasktype', null, 'is', status);
	filtersso[1] = new nlobjSearchFilter('custrecord_act_end_date', null, 'on', sysdate);
	if(status == '3')
		filtersso[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'noneof', [26,30]);// 
	//filtersso[2] = new nlobjSearchFilter('custrecord_act_end_date',  null, 'isempty');         
	//filtersso[1] = new nlobjSearchFilter('custrecordact_begin_date', null, 'on', '3/30/2012');         
	/*** The below code is merged from Lexjet production account on 04thMar13 by Santosh as part of Standard bundle***/
	nlapiLogExecution('ERROR','vRoleLocation',vRoleLocation);
	if(vRoleLocation != null && vRoleLocation != '' && vRoleLocation != 0)
	{
		filtersso.push(new nlobjSearchFilter('custrecord_wms_location', null, 'anyof', vRoleLocation));     
	}
	/*** Up to here ***/ 
	if(maxno!=0)
	{
		filtersso.push(new nlobjSearchFilter('id',null,'greaterthan',maxno));
	}
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('custrecord_tasktype');
	columns[1] = new nlobjSearchColumn('internalid');
	columns[1].setSort();

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso, columns);

	if(searchresults != null && searchresults.length>=1000)
	{
		nlapiLogExecution('ERROR','getOpenTaskStatus if',searchresults.length);
		socount += parseFloat(searchresults.length);
		/*if(searchresults.length>=1000)
		{*/
		nlapiLogExecution('ERROR','socounttst OT',socount);
		var mno=searchresults[searchresults.length-1].getId();    
		getOpenTaskStatus(status,mno,sysdate);
		/*}*/
	}
	else
	{

		if(searchresults != null)
		{
			nlapiLogExecution('ERROR','getOpenTaskStatus else',searchresults.length);
			socount += parseFloat(searchresults.length);
			nlapiLogExecution('ERROR','getOpenTaskStatus else',searchresults.length); 
		}
	}

	nlapiLogExecution('ERROR','socount OT',socount);
	nlapiLogExecution('ERROR','cnt OT',socount);
	return socount;

}

function getClosedTaskStatus(status,maxno,sysdate)
{
	var filters = new Array();

	filters[0] = new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'is', status);
	filters[1] = new nlobjSearchFilter('custrecord_ebiztask_act_end_date', null, 'on',sysdate); 
	if(status == '3')
		filters[2] = new nlobjSearchFilter('custrecord_ebiztask_wms_status_flag', null, 'noneof', [26,30]);//
	// filters[2] = new nlobjSearchFilter('custrecord_ebiztask_act_end_date',  null, 'isempty');         
	//filters[1] = new nlobjSearchFilter('custrecord_ebiztask_act_end_date', null, 'on', '3/30/2012');          
	if(maxno!=0)
	{
		filters.push(new nlobjSearchFilter('id',null,'greaterthan',maxno));
	}
	var column = new Array();
	column[0] = new nlobjSearchColumn('custrecord_ebiztask_tasktype');
	column[1] = new nlobjSearchColumn('internalid');
	column[1].setSort();
	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filters, column);

	if(searchresults != null)

	{
		nlapiLogExecution('ERROR','getOpenTaskStatus CT',searchresults.length);

		cocount+= parseFloat(searchresults.length);

		if(searchresults.length>=1000)
		{
			var mno=searchresults[searchresults.length-1].getId();    
			getClosedTaskStatus(status,mno,sysdate);
		}
	}



	nlapiLogExecution('ERROR','cocountOT',cocount);



	return cocount;
}

function getTaskStatus(status,count)
{
	var sysdate=DateStamp();
	//var socount=0;	

	var resOpenTaskStatus = getOpenTaskStatus(status,0,sysdate,count);

	nlapiLogExecution('ERROR', ' Open Task count', resOpenTaskStatus);


	//    var count=0;  
	var resClosedTaskStatus = getClosedTaskStatus(status,0,sysdate,count);
	nlapiLogExecution('ERROR', 'closedTask count',resClosedTaskStatus);



	return parseFloat(resOpenTaskStatus)+parseFloat(resClosedTaskStatus );
}







