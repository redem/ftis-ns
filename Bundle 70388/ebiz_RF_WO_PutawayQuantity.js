/***************************************************************************
 eBizNET Solutions Inc              
 ***************************************************************************
 **     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_WO_PutawayQuantity.js,v $
 *     	   $Revision: 1.1.2.2.2.5 $
 *     	   $Date: 2015/11/20 14:52:06 $
 *     	   $Author: skreddy $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_WO_PutawayQuantity.js,v $
 * Revision 1.1.2.2.2.5  2015/11/20 14:52:06  skreddy
 * case 201415773
 * 2015.2 issue fix
 *
 * Revision 1.1.2.2.2.4  2015/11/09 10:06:44  schepuri
 * case# 201415487
 *
 * Revision 1.1.2.2.2.3  2015/11/06 15:50:34  skreddy
 * case 201415423
 * 2015.2  issue fix
 *
 * Revision 1.1.2.2.2.2  2015/11/05 14:19:28  schepuri
 * case# 201415354
 *
 * Revision 1.1.2.2.2.1  2015/11/04 14:00:53  schepuri
 * case# 201415352,201415353
 *
 * Revision 1.1.2.2  2015/02/17 13:08:32  schepuri
 * issue# 201411367
 *
 * Revision 1.1.2.1  2014/08/04 09:14:10  nneelam
 * case#  20149371
 * Stanadard Bundle Partial WO Issue Fix.
 *
 * Revision 1.1.2.1.4.7  2014/04/17 15:52:33  skavuri
 * Case # 20148055 SB issue fixed
 *
 * Revision 1.1.2.1.4.6  2014/02/14 06:23:57  skreddy
 * case 20127124
 * Nucourse SB issue fix
 *
 * Revision 1.1.2.1.4.5  2014/02/05 15:06:02  sponnaganti
 * case# 20127056
 * For Keyboard Enter button to work
 *
 * Revision 1.1.2.1.4.4  2013/06/14 07:05:45  gkalla
 * CASE201112/CR201113/LOG201121
 * PCT Case# 20122979. To select item status while RF WO confirm build
 *
 * Revision 1.1.2.1.4.3  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.1.4.2  2013/03/19 11:48:12  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.1.2.1.4.1  2013/03/08 14:38:41  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.1.2.1  2012/11/23 09:40:04  skreddy
 * CASE201112/CR201113/LOG201121
 * RF version of WO Assembly building process
 *
 * Revision 1.10.2.18.4.5  2012/10/11 10:27:04  gkalla

 * 
 *****************************************************************************/
function PutawayQuantity(request, response){
	if (request.getMethod() == 'GET') {
		var getFetchedBeginLocation = request.getParameter('custparam_beginLocation');
		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('Error', 'getlangugae', getLanguage);
		var st1,st2,st3,st4,st5,st6,st7;

		if( getLanguage == 'es_ES')
		{
			st1 = "IR A LA UBICACI&#211;N:";
			st2 = "INGRESAR / ESCANEO DE LA UBICACI&#211;N";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";
			st5 = "";
			st6 = "";

		}
		else
		{
			st1 = "ASSEMBLY ITEM  ";
			st2 = "ENTER QUANTITY ";
			st3 = "SEND";
			st4 = "PREV";
			st5 = "BUILD QTY ";
			st6 = "WO# ";
			st7 = "LOT#";
		}


		var getWONo = request.getParameter('custparam_woname');
		var getWOItem = request.getParameter('custparam_item');
		var getFetchedItemId = request.getParameter('custparam_fetcheditemid');
		var getWOInternalId = request.getParameter('custparam_woid');
		var getWOQtyEntered = request.getParameter('custparam_expectedquantity');
		var getWHLocation = request.getParameter('custparam_whlocation');
		var getnoofrecords =request.getParameter('custparam_noofrecords');
		var getwoStatus =request.getParameter('custparam_wostatus');
		var getWOLinePackCode = request.getParameter('custparam_packcode');
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getFetchedLocation = request.getParameter('custparam_beginLocationname');
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');

		var getexpdate = request.getParameter('custparam_expdate');
		var getmfgdate=request.getParameter('custparam_mfgdate');
		var getbestbeforedate=request.getParameter('custparam_bestbeforedate');
		var getfifodate=request.getParameter('custparam_fifodate');
		var getlastdate=request.getParameter('custparam_lastdate');
		var getbatchno=request.getParameter('custparam_batchno');
		var getItemType = request.getParameter('custparam_itemtype');
		var getFifoCode = request.getParameter('custparam_fifocode');

		nlapiLogExecution('Error', 'getWONo', getWONo);
		nlapiLogExecution('Error', 'getFetchedItemId', getFetchedItemId);
		nlapiLogExecution('Error', 'getItemType', getItemType);


		//case# 20127056 starts (Now form name is passed correctly in function to work keyboard enter button)
		var functionkeyHtml=getFunctionkeyScript('_rf_putawayloc'); 
		//case# 21012 end
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=480, user-scalable=no'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
		html = html + " document.getElementById('enterlocation').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_putawayloc' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st6 + ":  <label>" + getWONo + "</label></td>";
		html = html + "				</td>";
		html = html + "			</tr>";		

		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +" :  <label>" + getWOItem + "</label></td>";
		//html = html + "				<input type='hidden' name='hdntype' value=" + Type + ">";
		html = html + "				<input type='hidden' name='hdnWOid' value=" + getWOInternalId + ">";
		html = html + "				<input type='hidden' name='hdnWOStatus' value=" + getwoStatus + "></td>";
		html = html + "				<input type='hidden' name='hdnWOItemId' value=" + getFetchedItemId + "></td>";
		html = html + "				<input type='hidden' name='hdnItemPackCode' value=" + getWOLinePackCode + ">";
		html = html + "				<input type='hidden' name='hdnnoofrecords' value=" + getnoofrecords + ">";
		html = html + "				<input type='hidden' name='hdnWOName' value=" + getWONo + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginDate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdnActualBeginTimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + getFetchedLocation + ">";
		html = html + "				<input type='hidden' name='hdnwoqtyenetered' value=" + getWOQtyEntered + ">";
		html = html + "				<input type='hidden' name='hdnBatchno' value=" + getbatchno + ">";
		html = html + "				<input type='hidden' name='hdnexpdate' value=" + getexpdate + ">";
		html = html + "				<input type='hidden' name='hdnmfgdate' value=" + getmfgdate + ">";
		html = html + "				<input type='hidden' name='hdnbestbeforedate' value=" + getbestbeforedate + ">";
		html = html + "				<input type='hidden' name='hdnlastdate' value=" + getlastdate + ">";
		html = html + "				<input type='hidden' name='hdnItemType' value=" + getItemType + ">";
		html = html + "				<input type='hidden' name='hdnFifoCode' value=" + getFifoCode + ">";
		html = html + "				<input type='hidden' name='hdnFifodate' value=" + getfifodate + ">";
		html = html + "				<input type='hidden' name='hdnWhLocation' value=" + getWHLocation + ">";

		html = html + "				</td>";
		html = html + "			</tr>";

		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st5 + ":  <label>" + getWOQtyEntered + "</label></td>";
		html = html + "				</td>";
		html = html + "			</tr>";

		if(getbatchno != null && getbatchno!= '')
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st7 + ":  <label>" + getbatchno + "</label></td>";
			html = html + "				</td>";
			html = html + "			</tr>";
		}
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+st2;//ENTER/SCAN LOCATION 
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterqty' type='text' value='" + getWOQtyEntered + "'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + st3 + " <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.focus();this.disabled=true; return false'/>";
		html = html + "					" + st4 + " <input name='cmdPrevious' type='submit' value='F7'/>";
		//html = html + "					" + st5 + " <input name='cmdOverride' type='submit' value='F11'/>";
		//html = html + "					" + st6 + " <input name='cmdSKIP' type='submit' value='F12'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {

		var getLanguage = request.getParameter('hdngetLanguage');

		var st7;

		if( getLanguage == 'es_ES')
		{
			st7 = "UBICACI&#211;N NO V&#193;LIDA";

		}
		else
		{
			st7 = "INVALID QUANTITY";
		}


		var getBeginLocation='';
		var WOarray = new Array();
		var WOid=request.getParameter('hdnWOid');

		var ItemType = request.getParameter('hdnItemType');

		nlapiLogExecution('Error', 'Into Response', 'Into Response');

		nlapiLogExecution('ERROR', 'WOid', WOid);
		nlapiLogExecution('ERROR', 'ItemType', ItemType);

		var RecCount;


		var getEnteredQty = request.getParameter('enterqty');

		var getExpectedQty = request.getParameter('hdnwoqtyenetered');

		//	nlapiLogExecution('Error', 'Entered Location', getEnteredLocation);	
		nlapiLogExecution('Error', 'Location', getBeginLocation);	
		nlapiLogExecution('Error', 'Name',  WOarray["name"]);	


		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		WOarray["custparam_woid"] = request.getParameter('custparam_woid');
		WOarray["custparam_item"] = request.getParameter('custparam_item');
		WOarray["custparam_fetcheditemid"] = request.getParameter('custparam_fetcheditemid');
		WOarray["custparam_expectedquantity"] = request.getParameter('enterqty');
		WOarray["custparam_whlocation"] = request.getParameter('hdnWhLocation');
		WOarray["custparam_noofrecords"] = request.getParameter('hdnnoofrecords');
		WOarray["custparam_woname"] = request.getParameter('hdnWOName');

		WOarray["custparam_actualbegindate"] = request.getParameter('hdnActualBeginDate');
		WOarray["custparam_actualbegintime"] = request.getParameter('hdnActualBeginTime');
		WOarray["custparam_actualbegintimeampm"] = request.getParameter('hdnActualBeginTimeAMPM');
		WOarray["custparam_actualbegintime"] = request.getParameter('hdnActualBeginTime');
		WOarray["custparam_actualbegintimeampm"] = request.getParameter('hdnActualBeginTimeAMPM');
		WOarray["custparam_wostatus"] = request.getParameter('hdnWOStatus');
		WOarray["custparam_packcode"] = request.getParameter('hdnItemPackCode');
		WOarray["custparam_language"] = request.getParameter('hdngetLanguage');
		WOarray["custparam_itemtype"] = request.getParameter('hdnItemType');

		WOarray["custparam_expdate"] = request.getParameter('hdnexpdate');
		WOarray["custparam_mfgdate"] = request.getParameter('hdnmfgdate');
		WOarray["custparam_bestbeforedate"] = request.getParameter('hdnbestbeforedate');
		WOarray["custparam_fifodate"] = request.getParameter('hdnFifodate');
		WOarray["custparam_lastdate"]= request.getParameter('hdnlastdate');
		WOarray["custparam_fifocode"] = request.getParameter('hdnFifoCode');
		WOarray["custparam_batchno"] = request.getParameter('hdnBatchno');



		WOarray["custparam_error"] = st7;//'INVALID LOCATION';
		WOarray["custparam_screenno"] = 'WOPutawayGenQty';


		if (optedEvent == 'F7') {	

			response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_gen', 'customdeploy_ebiz_rf_wo_putaway_gen_di', false, WOarray);
		}
		else {
			if((isNaN(getEnteredQty)) ||(getEnteredQty<0))
				getEnteredQty='';

			if (getEnteredQty != '' && getEnteredQty != null) {

				nlapiLogExecution('Error', 'IF');	

				nlapiLogExecution('Error', 'parseFloat(getEnteredQty)',parseFloat(getEnteredQty));	
				nlapiLogExecution('Error', 'parseFloat(getExpectedQty)',parseFloat(getExpectedQty));	

				if(parseFloat(getEnteredQty)>parseFloat(getExpectedQty)){

					nlapiLogExecution('Error', 'EnteredQty Greater than Expected.');	

					WOarray["custparam_expectedquantity"] = request.getParameter('hdnwoqtyenetered');
					WOarray["custparam_error"] = "ENTERED QTY SHOULD NOT BE GREATER THAN BUILD QTY";
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, WOarray);
					return false;
				}
//				case# 201411367
				else if(parseFloat(getEnteredQty)== 0){

					nlapiLogExecution('Error', 'EnteredQty Greater than Expected.');	

					WOarray["custparam_expectedquantity"] = request.getParameter('hdnwoqtyenetered');
					WOarray["custparam_error"] = "ENTERED QTY SHOULD NOT BE ZERO";
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, WOarray);
					return false;
				}
				else if(parseFloat(getEnteredQty)<=parseFloat(getExpectedQty)){

					nlapiLogExecution('Error', 'else if.');	

					var now = new Date();
					//a Date object to be used for a random value
					var now = new Date();
					//now= now.getHours();
					//Getting time in hh:mm tt format.
					var a_p = "";
					var d = new Date();
					var curr_hour = now.getHours();
					if (curr_hour < 12) {
						a_p = "am";
					}
					else {
						a_p = "pm";
					}
					if (curr_hour == 0) {
						curr_hour = 12;
					}
					if (curr_hour > 12) {
						curr_hour = curr_hour - 12;
					}

					var curr_min = now.getMinutes();

					curr_min = curr_min + "";

					if (curr_min.length == 1) {
						curr_min = "0" + curr_min;
					}

					var asmitem = request.getParameter('custparam_fetcheditemid');

					var expqty = getEnteredQty;


					nlapiLogExecution('ERROR','asmitem',asmitem);
					nlapiLogExecution('ERROR','expqty',expqty);

					var memitemqty = new Array();
					//memitemqty =fngetMemItemQty(asmitem,expqty);
					//memitemqty =fngetMemItemQty(WOid,getExpectedQty,expqty);
					memitemqty =fngetMemItemQty(WOid,expqty,getExpectedQty);
					nlapiLogExecution('ERROR','memitemqty',memitemqty);

					nlapiLogExecution('ERROR','WOid',WOid);

					var filter= new Array();
					filter.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', WOid));

					filter.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', 3));
					filter.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', 8));


					var columns = new Array();	

					//columns.push(new nlobjSearchColumn('custrecord_ebiz_order_no',null,'group'));			 
					columns.push(new nlobjSearchColumn('custrecord_line_no',null,'group'));
					columns.push(new nlobjSearchColumn('custrecord_wms_status_flag',null,'group'));
					columns.push(new nlobjSearchColumn('custrecord_act_qty',null,'sum'));
					columns.push(new nlobjSearchColumn('custrecord_sku',null,'group'));

					var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filter, columns);
					if(searchresults!=null && searchresults!=''){
						nlapiLogExecution('ERROR','searchresults.length',searchresults.length);
						for(var j=0; j<searchresults.length; j++){
							var skuitem = 	searchresults[j].getValue('custrecord_sku',null,'group');
							var qty = searchresults[j].getValue('custrecord_act_qty',null,'sum');
							for(var m = 0; m<memitemqty.length; m++){
								nlapiLogExecution('ERROR','memitemqty.length',memitemqty.length);
								var memitem = memitemqty[m][0];
								var memqty = memitemqty[m][2];
								if(skuitem==memitem){
//									case# 201417104
									if(parseInt(searchresults.length) == memitemqty.length)
									{
										if(qty<memqty){
											nlapiLogExecution('ERROR','qty',qty);
											nlapiLogExecution('ERROR','memqty',memqty);
											//WOarray["custparam_expectedquantity"] = request.getParameter('getEnteredQty');
											WOarray["custparam_expectedquantity"] = request.getParameter('hdnwoqtyenetered');// case# 201415354
											nlapiLogExecution('ERROR','WOarray["custparam_expectedquantity"]',WOarray["custparam_expectedquantity"]);
											nlapiLogExecution('ERROR','request.getParameter(getEnteredQty)',request.getParameter('getEnteredQty'));
											nlapiLogExecution('ERROR','request.getParameter(hdnwoqtyenetered)',request.getParameter('hdnwoqtyenetered'));
											WOarray["custparam_error"] = "DONT HAVE SUFFICIENT PICKS TO CONFIRM BUILD";
											response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, WOarray);
											return false; 
										}
									}
									else
									{
										WOarray["custparam_expectedquantity"] = request.getParameter('hdnwoqtyenetered');// case# 201415354
										WOarray["custparam_error"] = "DONT HAVE SUFFICIENT PICKS TO CONFIRM BUILD";
										nlapiLogExecution('ERROR','insufficent invt 2','insufficent invt 2');
										response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, WOarray);
										return false; 
									}

								}
							}
						}
					}


					var ItemStatusFilters = new Array();
					ItemStatusFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', WOid));
					ItemStatusFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', 3));
					ItemStatusFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', 8));
					ItemStatusFilters.push(new nlobjSearchFilter('custrecord_notes', null, 'isempty'));


					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_line_no');
					columns[1] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
					columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
					columns[3] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
					columns[4] = new nlobjSearchColumn('custrecord_lpno');
					columns[5] = new nlobjSearchColumn('custrecord_actbeginloc');
					columns[6] = new nlobjSearchColumn('custrecord_sku');
					columns[7] = new nlobjSearchColumn('custrecord_invref_no');
					columns[8] = new nlobjSearchColumn('custrecord_sku_status');
					columns[9] = new nlobjSearchColumn('custrecord_packcode');
					columns[10] = new nlobjSearchColumn('name');
					columns[11] = new nlobjSearchColumn('custrecord_batch_no');
					columns[12] = new nlobjSearchColumn('custrecord_from_lp_no');
					columns[13] = new nlobjSearchColumn('custrecord_kit_refno');
					columns[14] = new nlobjSearchColumn('custrecord_last_member_component');
					columns[15] = new nlobjSearchColumn('custrecord_parent_sku_no');
					columns[16] = new nlobjSearchColumn('custrecord_serial_no');
					columns[17] = new nlobjSearchColumn('custrecord_comp_id');
					columns[18] = new nlobjSearchColumn('custrecord_container_lp_no');
					columns[19] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
					columns[20] = new nlobjSearchColumn('custrecord_ebiz_order_no');
					columns[21] = new nlobjSearchColumn('custrecord_wms_location');
					columns[22] = new nlobjSearchColumn('custrecord_container');			

					var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, ItemStatusFilters, columns);

					var vline, vitem, vqty, vso, vmainline, vrecid, vlocation, vLpno, vSKU, vlocationid, vinvrefno, vskustatus, vpackcode, vdono, vBatchno, vfromlp;
					var vkitrefno, lastkititem, kititem, vserialno, vCompany, vContlpNo, vEbizWaveNo, vSointernid, warehouseLocation,vcontainersize;


					for (var i = 0; searchresults != null && i < searchresults.length; i++) 
					{

						nlapiLogExecution('ERROR','searchresults.length',searchresults.length);

						var searchresult = searchresults[i];

						vline = searchresult.getValue('custrecord_line_no');
						vitem = searchresult.getValue('custrecord_ebiz_sku_no');

						vqty = searchresult.getValue('custrecord_expe_qty');
						vso = searchresult.getValue('custrecord_ebiz_cntrl_no');
						vLpno = searchresult.getValue('custrecord_container_lp_no');
						vlocationid = searchresult.getValue('custrecord_actbeginloc');

						vlocation = searchresult.getText('custrecord_actbeginloc');



						vskustatusvalue = searchresult.getValue('custrecord_sku_status');
						vSKU = searchresult.getText('custrecord_sku');
						vinvrefno = searchresult.getValue('custrecord_invref_no');
						vrecid = searchresult.getId();

						vskustatus = searchresult.getText('custrecord_sku_status');
						vpackcode = searchresult.getValue('custrecord_packcode');
						vdono = searchresult.getValue('name');
						vfromlp = searchresult.getValue('custrecord_from_lp_no');
						vBatchno = searchresult.getValue('custrecord_batch_no');
						vkitrefno = searchresult.getValue('custrecord_kit_refno');
						lastkititem = searchresult.getValue('custrecord_last_member_component');
						kititem = searchresult.getValue('custrecord_parent_sku_no');
						vserialno = searchresult.getValue('custrecord_serial_no');
						vCompany = searchresult.getValue('custrecord_comp_id');
						vContlpNo = searchresult.getValue('custrecord_container_lp_no');
						vEbizWaveNo = searchresult.getValue('custrecord_ebiz_wave_no');
						vSointernid = searchresult.getValue('custrecord_ebiz_order_no');
						warehouseLocation = searchresult.getValue('custrecord_wms_location');
						vcontainersize = searchresult.getValue('custrecord_container');


						for(var m = 0; m<memitemqty.length; m++){

							nlapiLogExecution('ERROR','memitemqty.length',memitemqty.length);

							var memitem = memitemqty[m][0];
							var memqty = memitemqty[m][2];

							nlapiLogExecution('ERROR','memitem',memitem);
							nlapiLogExecution('ERROR','kititem',kititem);
							nlapiLogExecution('ERROR','vitem',vitem);
							if(vitem==memitem && parseFloat(memqty)>0){

								var expQty;

								if(vqty>=memqty)
									expQty = memqty;
								else
									expQty = vqty; 

								var remQty = vqty - memqty;

								nlapiLogExecution('ERROR','expQty',expQty);
								nlapiLogExecution('ERROR','remQty',remQty);
								nlapiLogExecution('ERROR','memqty',memqty);

								memitemqty[m][2] = parseFloat(memqty)-parseFloat(expQty);

								var currentContext = nlapiGetContext();
								var currentUserID = currentContext.getUser();

								var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', vrecid);
								//	transaction.setFieldValue('custrecord_wms_status_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')
								transaction.setFieldValue('custrecord_act_qty', parseFloat(expQty).toFixed(4));
								transaction.setFieldValue('custrecord_expe_qty', parseFloat(expQty).toFixed(4));
								//	transaction.setFieldValue('custrecord_actendloc', vlocationid);
								//	transaction.setFieldValue('custrecord_container', vcontainersize);
								//	transaction.setFieldValue('custrecord_total_weight', parseFloat(itemWeight).toFixed(4));
								//	transaction.setFieldValue('custrecord_totalcube', parseFloat(itemCube).toFixed(4));
								//transaction.setFieldValue('custrecord_batch_no', vBatchno);				
								//transaction.setFieldValue('custrecord_container_lp_no', vContlpNo);
								//transaction.setFieldValue('custrecord_act_end_date', DateStamp());
								//	transaction.setFieldValue('custrecord_actualendtime',((curr_hour) + ":" + (curr_min) + " " + a_p));
								transaction.setFieldValue('custrecord_upd_ebiz_user_no',currentUserID);		
								transaction.setFieldValue('custrecord_taskassignedto',currentUserID);
								transaction.setFieldValue('custrecord_notes','IN PROCESS');
								//transaction.setFieldValue('custrecord_ebiz_nsconfirm_ref_no',id);


								if(remQty!=0 && remQty>0){
									var createopentaskrec =  nlapiCopyRecord('customrecord_ebiznet_trn_opentask',vrecid);		    	
									createopentaskrec.setFieldValue('custrecord_expe_qty', parseFloat(remQty).toFixed(4));
									createopentaskrec.setFieldValue('custrecord_act_qty', parseFloat(remQty).toFixed(4));
									createopentaskrec.setFieldValue('custrecord_upd_ebiz_user_no', currentUserID);
									createopentaskrec.setFieldValue('custrecord_ebizuser', currentUserID);
									createopentaskrec.setFieldValue('name', searchresult.getValue('name'));
									nlapiSubmitRecord(createopentaskrec, false, true);
								}

								nlapiSubmitRecord(transaction, false, true);

								break;
							}
						}

					}
					nlapiLogExecution('ERROR','parseFloat(getEnteredQty)',parseFloat(getEnteredQty));
					WOarray["custparam_expectedquantity"] = parseFloat(getEnteredQty);
					var columns='';
					if(asmitem !=null && asmitem !='' && asmitem !='null')
					{
						var fields = ['recordType', 'custitem_ebizserialin','custitem_ebizbatchlot'];
						columns = nlapiLookupField('item', asmitem, fields);
					}

					var serialInflg="F";
					var batchflag="F";
					var itemSubtype='';
					if(columns != null && columns!='')
					{
						itemSubtype = columns.recordType;	
						serialInflg = columns.custitem_ebizserialin;
						batchflag= columns.custitem_ebizbatchlot;

					}
					nlapiLogExecution('ERROR','itemSubtype',itemSubtype);
					if (itemSubtype == "lotnumberedinventoryitem" || itemSubtype=="lotnumberedassemblyitem" || batchflag=="T" )
					{
						WOarray["custparam_itemtype"] = "lotnumbereditem" ;
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_batchno', 'customdeploy_ebiz_rf_wo_putaway_batch_di', false, WOarray);
						return;
					}
					else
					{
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_loc', 'customdeploy_ebiz_rf_wo_putaway_loc_di', false, WOarray);	
						return;
					}

				}
				else{
					WOarray["custparam_expectedquantity"] = request.getParameter('hdnwoqtyenetered');
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_putaway_loc', 'customdeploy_ebiz_rf_wo_putaway_loc_di', false, WOarray);
					return;
				}
			}
			else{
				WOarray["custparam_error"] = "PLEASE ENTER VALID QTY TO BUILD";
				WOarray["custparam_expectedquantity"] = request.getParameter('hdnwoqtyenetered');
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, WOarray);
				return;
			}

		}
	}
}

/*function fngetMemItemQty(vitem,vReqQty){

	var filters = new Array(); 

	filters[0] = new nlobjSearchFilter('internalid', null, 'is', vitem);//kit_id is the parameter name 
	nlapiLogExecution('DEBUG', 'vitem', vitem); 
	var columns1 = new Array(); 
	columns1[0] = new nlobjSearchColumn( 'memberitem' ); 
	columns1[1] = new nlobjSearchColumn( 'memberquantity' );

	var searchresults = nlapiSearchRecord( 'item', null, filters, columns1 );  
	var kititemsarr = new Array();
	for(var q=0; searchresults!=null && q<searchresults.length;q++) 
	{
		var vSubArr=new Array();
		vSubArr.push(searchresults[q].getValue('memberitem'));
		var vMemQty=searchresults[q].getValue('memberquantity');
		if(vMemQty == null || vMemQty == '')
			vMemQty=0;
		var vActQty= parseFloat(vMemQty) * parseFloat(vReqQty);
		vSubArr.push(searchresults[q].getValue('memberquantity'));
		vSubArr.push(vActQty);
		kititemsarr.push(vSubArr);
	}

	return kititemsarr;

}*/

function fngetMemItemQty(woid,vReqQty,vMainQty){

	nlapiLogExecution('ERROR','vReqQty',vReqQty);
	nlapiLogExecution('ERROR','vMainQty',vMainQty);

	var filters = new Array(); 

	filters.push(new nlobjSearchFilter('internalid', null, 'is', woid));//kit_id is the parameter name 
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'F'));
//	nlapiLogExecution('DEBUG', 'vitem', vitem); 
	var columns1 = new Array(); 
	columns1[0] = new nlobjSearchColumn( 'item' ); 
	columns1[1] = new nlobjSearchColumn( 'quantity' );
	columns1[2] = new nlobjSearchColumn( 'line' );

	var searchresults = nlapiSearchRecord( 'workorder', null, filters, columns1 );  
	var kititemsarr = new Array();
	for(var q=0; searchresults!=null && q<searchresults.length;q++) 
	{
		var vSubArr=new Array();
		vSubArr.push(searchresults[q].getValue('item'));
		var vMemQty=searchresults[q].getValue('quantity');
		nlapiLogExecution('ERROR','vMemQty',vMemQty);
		if(vMemQty == null || vMemQty == '')
			vMemQty=0;
		var vActQty= (parseFloat(vMemQty)/parseFloat(vMainQty)) * parseFloat(vReqQty);
		vSubArr.push(searchresults[q].getValue('quantity'));
		vSubArr.push(vActQty);
		vSubArr.push(searchresults[q].getValue('line'));
		kititemsarr.push(vSubArr);
	}

	return kititemsarr;

}


