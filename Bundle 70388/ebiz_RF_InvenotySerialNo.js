/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_InvenotySerialNo.js,v $
 *     	   $Revision: 1.1.2.4.2.11.2.4 $
 *     	   $Date: 2015/05/26 10:58:28 $
 *     	   $Author: skreddy $
 *     	   $Name: t_eBN_2015_1_StdBundle_1_127 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_InvenotySerialNo.js,v $
 * Revision 1.1.2.4.2.11.2.4  2015/05/26 10:58:28  skreddy
 * Case# 201412839
 * sighwarehouse SB issue fix
 *
 * Revision 1.1.2.4.2.11.2.3  2014/11/19 14:56:01  schepuri
 * 201411089,201410985�  issue fix
 *
 * Revision 1.1.2.4.2.11.2.2  2014/07/08 15:06:53  sponnaganti
 * case# 20148325
 * Compatibility issue fix
 *
 * Revision 1.1.2.4.2.11.2.1  2014/07/04 15:28:57  sponnaganti
 * case# 20148325
 * Compatibility Issue Fix
 *
 * Revision 1.1.2.4.2.11  2014/06/13 10:02:22  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.4.2.10  2014/06/06 08:24:14  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.1.2.4.2.9  2014/05/30 00:34:20  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.4.2.8  2014/03/05 12:05:18  snimmakayala
 * Case #: 20127451
 *
 * Revision 1.1.2.4.2.7  2014/01/06 13:16:03  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.1.2.4.2.6  2013/11/27 16:54:43  nneelam
 * Case# 20125426
 * Navigation to previous screen based on serial number.
 *
 * Revision 1.1.2.4.2.5  2013/11/18 13:50:06  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * Surftech Issue fixes
 *
 * Revision 1.1.2.4.2.4  2013/10/19 13:31:53  skreddy
 * Case# 20124749
 * standard bundle issue fix
 *
 * Revision 1.1.2.4.2.3  2013/05/02 15:37:15  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fixes
 *
 * Revision 1.1.2.4.2.2  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.4.2.1  2013/03/15 09:28:43  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 * 
 *

 *****************************************************************************/
function InvenotySerial(request, response){
	
	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);
	
	if (request.getMethod() == 'GET') {
		nlapiLogExecution('ERROR', 'Into CheckinSerial');

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('Error', 'getlangugae', getLanguage);
		var st1,st2,st3,st4;

		if( getLanguage == 'es_ES')
		{
			st1 = "ENTRAR EN SERIE NO : ";
			st2 = "PADRE ID : ";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";

		}
		else
		{
			st1 = "ENTER SERIAL NO: ";
			st2 = "PARENT ID : ";
			st3 = "SEND ";
			st4 = "PREV";

		}

		var ruleDet = IsCustomScreenRequired('RFINVENTORYSERIALSCAN');
		if(ruleDet!=null && ruleDet!='')
		{
			var vScreenType = ruleDet[0].getValue('custrecord_ebizrulevalue');
			var vScriptId = ruleDet[0].getValue('custrecord_ebizrule_scriptid');
			var vDeployId = ruleDet[0].getValue('custrecord_ebizrule_deployid');

			nlapiLogExecution('DEBUG', 'vScreenType', vScreenType);
			nlapiLogExecution('DEBUG', 'vScriptId', vScriptId);
			nlapiLogExecution('DEBUG', 'vDeployId', vDeployId);			
		}


		var totqtymoved=request.getParameter('custparam_totqtymoveed');
		var getMoveQuantity = request.getParameter('custparam_moveqty');
		var getUOMId = request.getParameter('custparam_uomid');
		var getUOM = request.getParameter('custparam_uom');
		var getStatus = request.getParameter('custparam_status');
		var getStatusId = request.getParameter('custparam_statusid');
		var getLOTId = request.getParameter('custparam_lotid');
		var getLOTNo = request.getParameter('custparam_lot'); 
		var getItemType = request.getParameter('custparam_itemtype');
		var getBatchNo = request.getParameter('custparam_batch'); 	 
		var locationId=request.getParameter('custparam_locationId');//
		nlapiLogExecution('ERROR', 'locationId ', request.getParameter('custparam_locationId')); 
		var getexistingLP=request.getParameter('custparam_existingLP');//
		var getBinLocationId = request.getParameter('custparam_imbinlocationid');
		var getBinLocation = request.getParameter('custparam_imbinlocationname');
		var getItemId = request.getParameter('custparam_imitemid');
		var getItem = request.getParameter('custparam_imitem');
		var getTotQuantity = request.getParameter('custparam_totquantity');
		var getAvailQuantity = request.getParameter('custparam_availquantity');	 
		var getLP = request.getParameter('custparam_lp');
		var getLPId = request.getParameter('custparam_lpid');
		var getMoveQty = request.getParameter('custparam_moveqty');
		var getInvtRecID=request.getParameter('custparam_invtrecid');
		nlapiLogExecution('ERROR', 'getInvtRecID ',getInvtRecID);
//		var locationid=request.getParameter('custparam_locationId');
		var getBatchId = request.getParameter('custparam_batchid');
		var getActualBeginDate = request.getParameter('custparam_actualbegindate');
		var getActualBeginTime = request.getParameter('custparam_actualbegintime');
		var getActualBeginTimeAMPM = request.getParameter('custparam_actualbegintimeampm');

		var getNewBinLocId= request.getParameter('custparam_newbinlocationid');
		var getNewBinLocation= request.getParameter('custparam_newbinlocationname');
		var getNewStatus=request.getParameter('custparam_skustatus');
		var adjustmenttype=request.getParameter('custparam_adjusttype');
		var getNewLP=request.getParameter('custparam_newLP');
		var ItemDesc = request.getParameter('custparam_itemdesc');
		var compId=request.getParameter('custparam_compid');//
		var fifodate=request.getParameter('custparam_fifodate');//
		var getSerialOut = request.getParameter('custparam_SerOut');//
		var getSerialIn = request.getParameter('custparam_SerIn');
		//case# 20148325 starts (serial numbers not updating because of mergelp)
		var putmethodID=request.getParameter('custparam_NewPutMethodID');
		//case# 20148325 ends 
		nlapiLogExecution('ERROR', 'compId ',compId);
		var getNumber;
		nlapiLogExecution('ERROR', 'custparam_number ',request.getParameter('custparam_number'));
		if(request.getParameter('custparam_number')!= null && request.getParameter('custparam_number') != "")
			getNumber = request.getParameter('custparam_number');
		else
			getNumber=0;
		nlapiLogExecution('ERROR', 'getNumber ',getNumber);
		//case# 201412839 strat
		var	tempBinLocationId=request.getParameter('custparam_hdntmplocation');
		var tempItemId=request.getParameter('custparam_hdntempitem');
		var tempLP=request.getParameter('custparam_hdntemplp');
		//case# 201412839 end
		if(vScreenType=='CUSTOM')
		{
			var IMarray = new Array();
			IMarray["custparam_language"] = getLanguage;
			IMarray["custparam_imbinlocationname"] = request.getParameter('custparam_imbinlocationname');
			IMarray["custparam_imitem"] = request.getParameter('custparam_imitem');
			IMarray["custparam_itemdesc"] = request.getParameter('custparam_itemdesc');
			IMarray["custparam_status"] = request.getParameter('custparam_status');
			IMarray["custparam_statusid"] = request.getParameter('custparam_statusid');
			IMarray["custparam_totquantity"] = request.getParameter('custparam_totquantity');
			IMarray["custparam_availquantity"] = request.getParameter('custparam_availquantity');
			IMarray["custparam_lp"] = request.getParameter('custparam_lp');
			IMarray["custparam_imitemid"] = request.getParameter('custparam_imitemid');
			IMarray["custparam_imbinlocationid"] = request.getParameter('custparam_imbinlocationid');
			IMarray["custparam_batch"] = request.getParameter('custparam_batch');		
			IMarray["custparam_newLP"] = request.getParameter('custparam_newLP');
			IMarray["custparam_moveqty"] = request.getParameter('custparam_moveqty');
			IMarray["custparam_uomid"] = request.getParameter('custparam_uomid');
			IMarray["custparam_uom"] = request.getParameter('custparam_uom');
			IMarray["custparam_lotid"] = request.getParameter('custparam_lotid');
			IMarray["custparam_lot"] = request.getParameter('custparam_lot');
			IMarray["custparam_lpid"] = request.getParameter('custparam_lpid');
			IMarray["custparam_itemtype"] = request.getParameter('custparam_itemtype');
			IMarray["custparam_actualbegindate"] = request.getParameter('custparam_actualbegindate');
			IMarray["custparam_actualbegintime"] = request.getParameter('custparam_actualbegintime');
			IMarray["custparam_actualbegintimeampm"] = request.getParameter('custparam_actualbegintimeampm');
			IMarray["custparam_invtrecid"] = request.getParameter('custparam_invtrecid');
			IMarray["custparam_totqtymoveed"] = request.getParameter('custparam_totqtymoveed');
			IMarray["custparam_locationId"] = request.getParameter('custparam_locationId');
			IMarray["custparam_compid"] = request.getParameter('custparam_compid');
			IMarray["custparam_fifodate"] = request.getParameter('custparam_fifodate');
			IMarray["custparam_serialno"] = request.getParameter('custparam_serialno');
			IMarray["custparam_number"] = request.getParameter('custparam_number');
			IMarray["custparam_skustatus"]=request.getParameter('custparam_skustatus');
			IMarray["custparam_newbinlocationid"] = request.getParameter('custparam_newbinlocationid');
			IMarray["custparam_newbinlocationname"] = request.getParameter('custparam_newbinlocationname');
			IMarray["custparam_adjusttype"]=request.getParameter('custparam_adjusttype');
			IMarray["custparam_tempserialid"]=request.getParameter('custparam_tempserialid');
			IMarray["custparam_MergrLP"]=request.getParameter('custparam_MergrLP');
			//case# 20148325 starts
			IMarray["custparam_NewPutMethodID"] = request.getParameter('custparam_NewPutMethodID');
			//case# 20148325 ends
			response.sendRedirect('SUITELET', vScriptId, vDeployId, false, IMarray );
			return;
		}
		var functionkeyHtml=getFunctionkeyScript('_rf_inventoryserialno'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";   
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
		
		//html = html + " document.getElementById('enterserialno').focus();";        
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_inventoryserialno' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		//  html = html + "				<td align = 'left'>UOM ID : <label>" + getUOMIDText + "</label>";
		html = html + "				<input type='hidden' name='hdnBeginLocationId' value=" + getBinLocationId + ">";
		html = html + "				<input type='hidden' name='hdnBinLocation' value=" + getBinLocation + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + ItemDesc + ">";
		html = html + "				<input type='hidden' name='hdnItemId' value=" + getItemId + ">";
		html = html + "				<input type='hidden' name='hdnFromLp' value=" + getLP + ">";			
		html = html + "				<input type='hidden' name='hdnItemType' value=" + getItemType + ">";
		html = html + "				<input type='hidden' name='hdnItemStatus' value=" + getStatus + ">";
		html = html + "				<input type='hidden' name='hdnItemStatusId' value=" + getStatusId + ">";
		html = html + "				<input type='hidden' name='hdnTotalQty' value=" + getTotQuantity + ">";
		html = html + "				<input type='hidden' name='hdnAvlQty' value=" + getAvailQuantity + ">";
		html = html + "				<input type='hidden' name='hdnSerOut' value=" + getSerialOut + ">";
		html = html + "				<input type='hidden' name='hdnSerIn' value=" + getSerialIn + ">";
		html = html + "				<input type='hidden' name='hdngetnumber' value=" + getNumber + ">";
		html = html + "				<input type='hidden' name='hdngetnewLP' value=" + getNewLP + ">";
		html = html + "				<input type='hidden' name='hdngetbatchno' value=" + getBatchNo + ">";
		html = html + "				<input type='hidden' name='hdngetmoveQty' value=" + getMoveQuantity + ">";
		html = html + "				<input type='hidden' name='hdngetUOMId' value=" + getUOMId + ">";
		html = html + "				<input type='hidden' name='hdngetUOM' value=" + getUOM + ">";
		html = html + "				<input type='hidden' name='hdngetlotid' value=" + getLOTId + ">";
		html = html + "				<input type='hidden' name='hdngetlotno' value=" + getLOTNo + ">";
		html = html + "				<input type='hidden' name='hdngetlpid' value=" + getLPId + ">";
		html = html + "				<input type='hidden' name='hdngetactualbgndate' value=" + getActualBeginDate + ">";
		html = html + "				<input type='hidden' name='hdngetactualbgntime' value=" + getActualBeginTime + ">";
		html = html + "				<input type='hidden' name='hdngetactualbgntimeAMPM' value=" + getActualBeginTimeAMPM + ">";
		html = html + "				<input type='hidden' name='hdngetInvRecid' value=" + getInvtRecID + ">";
		html = html + "				<input type='hidden' name='hdngettotqtymved' value=" + totqtymoved + ">";
		html = html + "				<input type='hidden' name='hdngetlocid' value=" + locationId + ">";
		html = html + "				<input type='hidden' name='hdngetcompid' value=" + compId + ">";
		html = html + "				<input type='hidden' name='hdngetfifodate' value=" + fifodate + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	

		html = html + "				<input type='hidden' name='hdnexistingLP' value=" + getexistingLP + ">";
		html = html + "					<input type='hidden' name='hdnLP' value=" + getLP + ">";
		html = html + "					<input type='hidden' name='hdnMoveQty' value=" + getMoveQty + ">";
		html = html + "				    <input type='hidden' name='hdnNewbinlocid' value=" + getNewBinLocId + ">";
		html = html + "				    <input type='hidden' name='hdnNewbinlocname' value=" + getNewBinLocation + ">";
		html = html + "				    <input type='hidden' name='hdnNewstatus' value=" + getNewStatus + ">";
		html = html + "				    <input type='hidden' name='hdnAdjustment' value=" + adjustmenttype + ">";
		//case# 20148325 starts
		html = html + "				    <input type='hidden' name='hdnputmethodid' value='" + putmethodID + "'>";
		//case# 20148325 ends
		//case# 201412839
		html = html + "				    <input type='hidden' name='hdntmplocation' value=" + tempBinLocationId + ">";
		html = html + "				    <input type='hidden' name='hdntemplp' value='" + tempLP + "'>";
		html = html + "				    <input type='hidden' name='hdntempitem' value='" + tempItemId + "'>";
		//case# 201412839
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>" + (parseFloat(getNumber) + 1) + " OF <label>" + getMoveQuantity + "</label>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +"";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterserialno' id='enterserialno' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st2 +"<label>" + getLP + "</label>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st3 +" <input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "					"+ st4 +" <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterserialno').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else {
		nlapiLogExecution('ERROR', 'Into Response', 'Into Response');

		var getLanguage = request.getParameter('hdngetLanguage');
		var IMarray = new Array();
		var st1,st5,st6,st7;

		if( getLanguage == 'es_ES')
		{
			st1 = "ENTRAR EN SERIE NO : ";
			st5 = "SERIAL # YA ESCANEADOS";
			st6 = "SERIAL # NO EXISTE";
			st7 = "SERIAL # YA EXISTE";

		}
		else
		{
			st1 = "ENTER SERIAL NO: ";
			st5 = "SERIAL # ALREADY SCANNED";
			st6 = "SERIAL # DOES NOT EXIST";
			st7 = "SERIAL # ALREADY EXISTS";
		}


		IMarray["custparam_language"] = getLanguage;
		IMarray["custparam_imbinlocationname"] = request.getParameter('hdnBinLocation');
		IMarray["custparam_imitem"] = request.getParameter('hdnItem');
		IMarray["custparam_itemdesc"] = request.getParameter('hdnItemDescription');
		IMarray["custparam_status"] = request.getParameter('hdnItemStatus');
		IMarray["custparam_statusid"] = request.getParameter('hdnItemStatusId');
		IMarray["custparam_totquantity"] = request.getParameter('hdnTotalQty');
		IMarray["custparam_availquantity"] = request.getParameter('hdnAvlQty');
		IMarray["custparam_lp"] = request.getParameter('hdnFromLp');

		IMarray["custparam_imitemid"] = request.getParameter('hdnItemId');
		IMarray["custparam_imbinlocationid"] = request.getParameter('hdnBeginLocationId');

		IMarray["custparam_batch"] = request.getParameter('hdngetbatchno');		
		IMarray["custparam_newLP"] = request.getParameter('hdngetnewLP');
		IMarray["custparam_moveqty"] = request.getParameter('hdngetmoveQty');
		IMarray["custparam_uomid"] = request.getParameter('hdngetUOMId');
		IMarray["custparam_uom"] = request.getParameter('hdngetUOM');
		IMarray["custparam_lotid"] = request.getParameter('hdngetlotid');
		IMarray["custparam_lot"] = request.getParameter('hdngetlotno');
		IMarray["custparam_lpid"] = request.getParameter('hdngetlpid');
		IMarray["custparam_itemtype"] = request.getParameter('hdnItemType');
		IMarray["custparam_actualbegindate"] = request.getParameter('hdngetactualbgndate');
		IMarray["custparam_actualbegintime"] = request.getParameter('hdngetactualbgntime');
		IMarray["custparam_actualbegintimeampm"] = request.getParameter('hdngetactualbgntimeAMPM');
		IMarray["custparam_invtrecid"] = request.getParameter('hdngetInvRecid');
		IMarray["custparam_totqtymoveed"] = request.getParameter('hdngettotqtymved');
		IMarray["custparam_locationId"] = request.getParameter('hdngetlocid');
		IMarray["custparam_compid"] = request.getParameter('hdngetcompid');
		IMarray["custparam_fifodate"] = request.getParameter('hdngetfifodate');

		//IMarray["custparam_skustatus"]=getNewStatus;

		IMarray["custparam_serialno"] = request.getParameter('custparam_serialno');
		IMarray["custparam_number"] = request.getParameter('hdngetnumber');
		IMarray["custparam_skustatus"]=request.getParameter('hdnNewstatus');
		IMarray["custparam_newbinlocationid"] = request.getParameter('hdnNewbinlocid');
		IMarray["custparam_newbinlocationname"] = request.getParameter('hdnNewbinlocname');
		IMarray["custparam_adjusttype"]=request.getParameter('hdnAdjustment');
		IMarray["custparam_tempserialid"]=request.getParameter('custparam_tempserialid');
		IMarray["custparam_MergrLP"]=request.getParameter('custparam_MergrLP');
		//case# 20148325 starts
		IMarray["custparam_NewPutMethodID"] = request.getParameter('hdnputmethodid');
		//case# 20148325 ends
		//case# 201412839 start parameter missing.
		IMarray["custparam_hdntmplocation"] = request.getParameter('hdntmplocation');
		IMarray["custparam_hdntemplp"] = request.getParameter('hdntemplp');
		IMarray["custparam_hdntempitem"] = request.getParameter('hdntempitem');
		//case# 201412839 start
		var getSerialNo = request.getParameter('enterserialno');
		var getItemQuantity =request.getParameter('hdngetmoveQty');

		var getItemType =request.getParameter('hdnItemType');		
		var getSerOut =request.getParameter('hdnSerOut');
		var getSerIn =request.getParameter('hdnSerIn');
		var getItemId =request.getParameter('hdnItemId');
		var getLPNo = request.getParameter('hdnFromLp');
		var getNumber = request.getParameter('hdngetnumber');
		
		var getBinLocationId = request.getParameter('custparam_imbinlocationid');
		var getBinLocation = request.getParameter('custparam_imbinlocationname');
		var getNewBinLocId= request.getParameter('custparam_newbinlocationid');
		var getNewBinLocation= request.getParameter('custparam_newbinlocationname');
		//case# 20148325 starts
		var putmethodID=request.getParameter('custparam_NewPutMethodID');
		nlapiLogExecution('ERROR', 'putmethodID', putmethodID);
		//case# 20148325 ends
		nlapiLogExecution('ERROR', 'getBinLocationId', getBinLocationId);
		nlapiLogExecution('ERROR', 'getBinLocation', getBinLocation);
		nlapiLogExecution('ERROR', 'getNewBinLocId', getNewBinLocId);
		nlapiLogExecution('ERROR', 'getNewBinLocation', getNewBinLocation);

		IMarray["custparam_SerIn"] = getSerIn;


		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		if (sessionobj!=context.getUser()) {
			try
			{

				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}
				
		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to RF Main Menu.
		if (optedEvent == 'F7') {

			nlapiLogExecution('ERROR', 'into previous', 'F7');
			var TempSerialNoIdArray =new Array();
			var TempSerial
			if (request.getParameter('custparam_tempserialid') != null && request.getParameter('custparam_tempserialid') !='') 
				TempSerialNoIdArray = request.getParameter('custparam_serialno').split(',');

			nlapiLogExecution('ERROR', 'TempSerialNoIdArray', TempSerialNoIdArray);
			nlapiLogExecution('ERROR', 'IMarray["custparam_MergrLP"]', IMarray["custparam_MergrLP"]);
			nlapiLogExecution('ERROR', 'IMarray["custparam_lp"]', IMarray["custparam_lp"]);
//case # 20125426, Added condition for navigation to screen based on the serial number.
			if((parseFloat(getNumber) + 1)==1){
				response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_status', 'customdeploy_rf_inventory_move_status_di', false, IMarray);	
				
			}
			else{
				var tempserailstore=new Array();
				nlapiLogExecution('ERROR', 'IMarray["custparam_serialno"]1', IMarray["custparam_serialno"]);
				
				var templastSerialno=IMarray["custparam_serialno"].split(",");
				
				nlapiLogExecution('ERROR', 'templastSerialno.length', templastSerialno.length);
				
				var lasttempserialno=templastSerialno[templastSerialno.length-1];
				
				
				var tempfiltersser = new Array();
				tempfiltersser[0] = new nlobjSearchFilter('custrecord_tempserialnumber', null, 'is', lasttempserialno);
				tempfiltersser[1] = new nlobjSearchFilter('custrecord_tempserialparentid', null, 'is', IMarray["custparam_lp"]);

				var tempSrchRecord = nlapiSearchRecord('customrecord_ebiznettempserialentry', null, tempfiltersser);
				if(tempSrchRecord!=null){
					var internalId=tempSrchRecord[0].getId();
					nlapiDeleteRecord('customrecord_ebiznettempserialentry',internalId);
					
				}
				
				
				
				var tempserailstore=IMarray["custparam_serialno"].substring(0,IMarray["custparam_serialno"].lastIndexOf(","));
				TempSerialNoArray=tempserailstore;
				IMarray["custparam_serialno"] =TempSerialNoArray;
				nlapiLogExecution('ERROR', 'IMarray["custparam_serialno"]2', IMarray["custparam_serialno"]);
				IMarray["custparam_number"] = parseFloat(getNumber) - 1;
				
				response.sendRedirect('SUITELET', 'customscript_rf_inventory_serialscan', 'customdeploy_rf_inventory_serialscan_di', false, IMarray);
				return;
				
			}	
			//End
			/*if(TempSerialNoIdArray !=null && TempSerialNoIdArray !='' && TempSerialNoIdArray.length>0)
			{
				var vLP='';
				if(IMarray["custparam_MergrLP"] !=null && IMarray["custparam_MergrLP"] !='')
					vLP=IMarray["custparam_MergrLP"];
				else
					vLP=IMarray["custparam_lp"];

				nlapiLogExecution('ERROR', 'vLP', vLP);

				for (var k = 0; k < TempSerialNoIdArray.length; k++)
				{
					var fields = new Array();
					var values = new Array();
					//fields[0] = 'custrecord_serialwmsstatus';						
					fields[0] = 'custrecord_serialparentid';
					//fields[2] = 'custrecord_serialitem';
					fields[1] = 'custrecord_serialbinlocation';
					//values[0] = '19';//FLAG.INVENTORY.STORAGE
					values[0] = vLP;
					//values[2] = getItemId;
					values[1] = '';

					var updatefields = nlapiSubmitField('customrecord_ebiznetserialentry',TempSerialNoIdArray[k], fields, values);

				}

			}*/

			
		}
		//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
		//            if (optedEvent != '' && optedEvent != null) {
		else 
			if (getSerialNo == "") {
				//	if the 'Send' button is clicked without any option value entered,
				//  it has to show an error message. The next screen to which it has to navigate is to the error screen.
				IMarray["custparam_error"] = st1;//'ENTER SERIAL NO';
				IMarray["custparam_screenno"] = '22AB';
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
				nlapiLogExecution('ERROR', 'Enter Serial No', getSerialNo);

			}
			else {
				nlapiLogExecution('ERROR', 'Serial No', getSerialNo);
				getSerialNo=SerialNoIdentification(getItemId,getSerialNo);
				nlapiLogExecution('ERROR', 'After Serial No Parsing', getSerialNo);
				var getActualEndDate = DateStamp();
				var getActualEndTime = TimeStamp();
				var TempSerialNoArray = new Array();
				var TempSerialNoID='';
				if (request.getParameter('custparam_serialno') != null) 
					TempSerialNoArray = request.getParameter('custparam_serialno').split(',');



				nlapiLogExecution('ERROR', 'INTO SERIAL ENTRY');
				//checking serial no's in already scanned one's
				/*for (var t = 0; t < TempSerialNoArray.length; t++) {
					if (getSerialNo == TempSerialNoArray[t]) {
						IMarray["custparam_error"] = st5;//"Serial # Already Scanned";//"Serial No. Already Scanned";
						IMarray["custparam_screenno"] = '22AB';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
						return;
					}
				}*/

				var fields = ['recordType', 'custitem_ebizserialin'];
				var columns = nlapiLookupField('item', request.getParameter('hdnItemId'), fields);
				var vItemType = columns.recordType;
				nlapiLogExecution('ERROR','vItemType',vItemType);

				var serialInflg="F";		
				serialInflg = columns.custitem_ebizserialin;
				nlapiLogExecution('ERROR','serialInflg',serialInflg);
				nlapiLogExecution('ERROR','IMarray["custparam_lp"]',IMarray["custparam_lp"]);

				if(vItemType == 'serializedinventoryitem' || serialInflg == 'T')
				{
					nlapiLogExecution('ERROR', 'getSerialNo', getSerialNo);

					//checking serial no's in records
					var filtersser = new Array();
					filtersser[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getSerialNo);
					if(IMarray["custparam_lp"]!=null && IMarray["custparam_lp"]!='')
						filtersser[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', IMarray["custparam_lp"]);
					
					var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser);
					nlapiLogExecution('ERROR', 'SrchRecord', SrchRecord);

					if (SrchRecord==null) {
						IMarray["custparam_error"] = st6;//"Serial # Does Not Exist";//"Serial No. Does Not Exists";
						IMarray["custparam_screenno"] = '22AB';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
						return;
					}
					nlapiLogExecution('ERROR','custparam_newLP',IMarray["custparam_newLP"]);
					var updatedSerialLP=IMarray["custparam_newLP"];
					//case  20124749� start
					var ItemStatus='';
					if(IMarray["custparam_skustatus"] != null && IMarray["custparam_skustatus"] !='')
						ItemStatus = IMarray["custparam_skustatus"];
					else
						ItemStatus = IMarray["custparam_statusid"];
					//case  20124749�end
					
					nlapiLogExecution('ERROR','ItemStatus',ItemStatus);
					//case# 20148325 starts 
					nlapiLogExecution('ERROR','putmethodID',putmethodID);
					var varMergeLP="T";
					if(putmethodID!=null && putmethodID!='')
						varMergeLP = isMergeLP(putmethodID);
					nlapiLogExecution('DEBUG', 'varMergeLP', varMergeLP);
					//if(varMergeLP=="T")
					if((getBinLocationId!=getNewBinLocId)&& varMergeLP=="T")
					{
						var mergedLP=GetInvtLp(IMarray["custparam_imitemid"],ItemStatus,1,IMarray["custparam_newbinlocationid"],
								IMarray["custparam_lotid"],IMarray["custparam_fifodate"]);
						if(mergedLP!=null&&mergedLP!="")
							updatedSerialLP=mergedLP;
						nlapiLogExecution('DEBUG', 'mergedLP', mergedLP);
					}
					//case# 20148325 ends
					if(getBinLocationId==getNewBinLocId)
					{
						updatedSerialLP=IMarray["custparam_newLP"];
					}
					nlapiLogExecution('ERROR', 'updatedSerialLP', updatedSerialLP);
					
					//case# 20125426, Checking if given serial number is available in temp serial table.
					var tempfiltersser = new Array();
					tempfiltersser[0] = new nlobjSearchFilter('custrecord_tempserialnumber', null, 'is', getSerialNo);
					if(IMarray["custparam_lp"]!=null && IMarray["custparam_lp"]!='')
						tempfiltersser[1] = new nlobjSearchFilter('custrecord_tempserialparentid', null, 'is', IMarray["custparam_lp"]);

					var tempSrchRecord = nlapiSearchRecord('customrecord_ebiznettempserialentry', null, tempfiltersser);
					
					if (tempSrchRecord==null) {
						var customrecord = nlapiCreateRecord('customrecord_ebiznettempserialentry'); 
						customrecord.setFieldValue('custrecord_tempserialnumber', getSerialNo);					
						customrecord.setFieldValue('custrecord_tempserialwmsstatus', '19');//with status FLAG.INVENTORY.STORAGE
						customrecord.setFieldValue('name', getSerialNo);
						customrecord.setFieldValue('custrecord_tempserialparentid', getLPNo);
						customrecord.setFieldValue('custrecord_tempserialitem', getItemId);
						customrecord.setFieldValue('custrecord_tempserialbinlocation', IMarray["custparam_newbinlocationid"]);
						//commit the record to NetSuite
						var recid = nlapiSubmitRecord(customrecord, true);
					}
					else{
						IMarray["custparam_error"] = st6;//"Serial # Does Not Exist";//"Serial No. Does Not Exists";
						IMarray["custparam_screenno"] = '22AB';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
						return;
					}
					
					//End
					if(getItemQuantity==(parseFloat(getNumber) + 1)){
						
						nlapiLogExecution('ERROR', "getItemQuantity==(parseFloat(getNumber) + 1)");
						
						nlapiLogExecution('ERROR', 'IMarray["custparam_lp"]',IMarray["custparam_lp"]);
						
						/*var serialarray=IMarray["custparam_serialno"].split(",");
						
						nlapiLogExecution('ERROR', 'serialarray',serialarray);*/
						
						var temp1filtersser = new Array();
							temp1filtersser[0] = new nlobjSearchFilter('custrecord_tempserialparentid', null, 'is', IMarray["custparam_lp"]);
							temp1filtersser[1] = new nlobjSearchFilter('custrecord_tempserialbinlocation', null, 'is',IMarray["custparam_newbinlocationid"] );

						
						
						var temp1columns=new Array();
						temp1columns[0]=new nlobjSearchColumn('custrecord_tempserialparentid');
						temp1columns[1]=new nlobjSearchColumn('custrecord_tempserialnumber');
						
						
						var temp1SrchRecord = nlapiSearchRecord('customrecord_ebiznettempserialentry', null, temp1filtersser,temp1columns);
						
						nlapiLogExecution('ERROR', 'temp1SrchRecord',temp1SrchRecord);
						
						
						for ( var i = 0; i < temp1SrchRecord.length && i<=getItemQuantity; i++) {
							var tmpParenttserial = temp1SrchRecord[i].getValue('custrecord_tempserialparentid');
							var tmpSerialno = temp1SrchRecord[i].getValue('custrecord_tempserialnumber');
							
							var tempId=temp1SrchRecord[i].getId();
							
							nlapiLogExecution('ERROR', "tmpParenttserial",tmpParenttserial);
						   nlapiLogExecution('ERROR', "tmpSerialno",tmpSerialno);
							
							var filtersser1 = new Array();
							filtersser1[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', tmpSerialno);
							filtersser1[1] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', tmpParenttserial);
							
							var SrchRecord1 = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser1);
							if(SrchRecord1!=null){
								
								 nlapiLogExecution('ERROR', "(SrchRecord1!=null)");
								
								var vserialid = SrchRecord1[0].getId();
								
								nlapiLogExecution('ERROR', "vserialid",vserialid);
								
// case no 201411089,201410985

var NewItemLP = IMarray["custparam_newLP"];
								if(NewItemLP == "" || NewItemLP == null)
								{
									NewItemLP = tmpParenttserial;
								}
								var fields = new Array();
								var values = new Array();
								fields[0] = 'custrecord_serialwmsstatus';						
								fields[1] = 'custrecord_serialparentid';
								fields[2] = 'custrecord_serialitem';
								fields[3] = 'custrecord_serialbinlocation';
fields[4] = 'custrecord_serial_lpno';
								values[0] = '19';//FLAG.INVENTORY.STORAGE
								values[1] = updatedSerialLP;
								values[2] = getItemId;
								values[3] = IMarray["custparam_newbinlocationid"];
	values[4] = NewItemLP;
								var updatefields = nlapiSubmitField('customrecord_ebiznetserialentry',vserialid, fields, values);
								
								nlapiDeleteRecord('customrecord_ebiznettempserialentry',tempId);
								
								nlapiLogExecution('ERROR', "Serial Record updated successfully");
							}
							
							
							
							
							//var vserialid = searchresultserial.getId();
							nlapiLogExecution('ERROR', 'Serial ID Internal Id :',vserialid);

							nlapiLogExecution('ERROR', "Item" , getItemId);

						

							nlapiLogExecution('ERROR', 'Record Updated :');
							if (request.getParameter('custparam_tempserialid') == null || request.getParameter('custparam_tempserialid') == "") 
							{
								IMarray["custparam_tempserialid"] = vserialid;
							}
							else 
							{
								IMarray["custparam_tempserialid"] = request.getParameter('custparam_tempserialid') + ',' + vserialid;
							}

						}

					}
				}
				else
				{
					var filtersser = new Array();
					filtersser[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getSerialNo);

					var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser);
					nlapiLogExecution('ERROR', 'SrchRecord', SrchRecord);

					if (SrchRecord!=null) {
						IMarray["custparam_error"] = st7;//"Serial # Already Exists";
						IMarray["custparam_screenno"] = '22AB';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
						return;
					}

					nlapiLogExecution('ERROR', "Item Id " , getItemId);

					var customrecord = nlapiCreateRecord('customrecord_ebiznetserialentry'); 
					customrecord.setFieldValue('custrecord_serialnumber', getSerialNo);					
					customrecord.setFieldValue('custrecord_serialwmsstatus', '19');//with status FLAG.INVENTORY.STORAGE
					customrecord.setFieldValue('name', getSerialNo);
					customrecord.setFieldValue('custrecord_serialparentid', getLPNo);
					customrecord.setFieldValue('custrecord_serialitem', getItemId);
					customrecord.setFieldValue('custrecord_serialbinlocation', IMarray["custparam_newbinlocationid"]);
					//commit the record to NetSuite
					var recid = nlapiSubmitRecord(customrecord, true);
				}


				if (request.getParameter('custparam_serialno') == null || request.getParameter('custparam_serialno') == "") 
				{
					IMarray["custparam_serialno"] = getSerialNo;
				}
				else 
				{
					IMarray["custparam_serialno"] = request.getParameter('custparam_serialno') + ',' + getSerialNo;
				}

				IMarray["custparam_number"] = parseFloat(getNumber) + 1;
				TempSerialNoArray.push(getSerialNo);
				nlapiLogExecution('ERROR', 'IMarray custparam_number', IMarray["custparam_number"]);
				nlapiLogExecution('ERROR', '(getNumber + 1)', (parseFloat(getNumber) + 1));
				nlapiLogExecution('ERROR', 'getItemQuantity', getItemQuantity);
				if ((parseFloat(getNumber) + 1) < parseFloat(getItemQuantity)) 
				{
					nlapiLogExecution('ERROR', 'Scanning Serial No.');
					response.sendRedirect('SUITELET', 'customscript_rf_inventory_serialscan', 'customdeploy_rf_inventory_serialscan_di', false, IMarray);
					return;
				}
				else 
				{
					nlapiLogExecution('ERROR', 'Inserting Into Records');

					//response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_endloc', 'customdeploy_rf_inventory_move_endloc_di', false, IMarray);
					response.sendRedirect('SUITELET', 'customscript_rf_inventory_move_complete', 'customdeploy_rf_inventory_move_comple_di', false, IMarray);
				}
			}
			}
			catch (e)  {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');

			}
		}
		else
			{
			IMarray["custparam_screenno"] = '18';
			IMarray["custparam_error"] = 'SERIAL# ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, IMarray);
			}
	}

	nlapiLogExecution('ERROR', 'Done customrecord', 'Success');
}


function GetInvtLp(ItemId,ItemStatusId,getPackCode,NewBinLocationId,LOTNoId,fifodate)
{
	try
	{
		var invExistLp="";
		nlapiLogExecution('ERROR','Into GetInvtLp');
		var filters = new Array();
		var i = 0;

		nlapiLogExecution('ERROR', 'ItemId',ItemId);
		if (ItemId != null && ItemId != '') {		
			filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_sku', null, 'is', ItemId);
			i++;
		}
		nlapiLogExecution('ERROR', 'ItemStatusId',ItemStatusId);
		if(ItemStatusId!=null && ItemStatusId!='')
		{
			filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_sku_status', null, 'is', ItemStatusId);
			i++;
		}

		nlapiLogExecution('ERROR', 'packcode',getPackCode);
		if(getPackCode !=null && getPackCode!="")
		{
			filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_packcode', null, 'is', getPackCode);
			i++;
		}
		nlapiLogExecution('ERROR', 'NewBinLocationId',NewBinLocationId);
		if(NewBinLocationId!=null&&NewBinLocationId!="")
		{
			filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_binloc', null, 'is', NewBinLocationId);
			i++;
		}
		nlapiLogExecution('ERROR', 'LOTNoId',LOTNoId);
		if(LOTNoId!=null&&LOTNoId!="")
		{
			filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_lot', null, 'anyof', [LOTNoId]);
			i++;
		}
		nlapiLogExecution('ERROR', 'fifodate',fifodate);
		if(fifodate!=null&&fifodate!="")
		{
			filters[i] = new nlobjSearchFilter('custrecord_ebiz_inv_fifo', null, 'on', fifodate);
		}

		filters.push(new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [19,3]));
		filters.push(new nlobjSearchFilter('custrecord_ebiz_qoh',null,'greaterthan',0));
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_qoh'); 
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
		var inventorysearch = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filters, columns);
		//case# 20148325 starts
		/*if(inventorysearch!=null&&inventorysearch!="")
		{
			invExistLp = inventorysearch[0].getValue('custrecord_ebiz_inv_lp');
		}*/
		if(inventorysearch != null && inventorysearch != '' && inventorysearch.length>0)
		{
			nlapiLogExecution('ERROR', 'Inventory found in same location for same item',inventorysearch.length);

			for(var k=0; k<inventorysearch.length;k++)
			{

				invExistLp = inventorysearch[k].getValue('custrecord_ebiz_inv_lp');	
				nlapiLogExecution('ERROR', 'invExistLp for loop',invExistLp);
				
			}
		}
		//case# 20148325 ends
		nlapiLogExecution('ERROR','invExistLp',invExistLp);
		return invExistLp;
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR','Exception in UpdateSerialNo',exp);
	}
}
//case# 20148325 starts
function isMergeLP(methodid)
{
	var varMergeLP = 'F';
	var filtersputmethod = new Array();
	if(methodid!=null && methodid!='')
		filtersputmethod.push(new nlobjSearchFilter('internalid', null, 'anyof', methodid));
	filtersputmethod.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

	var colsputmethod = new Array();
	colsputmethod[0] = new nlobjSearchColumn('custrecord_mergelp');            

	var putwmethodsearchresults = nlapiSearchRecord('customrecord_ebiznet_putaway_method', null, filtersputmethod, colsputmethod);

	if(putwmethodsearchresults != null &&putwmethodsearchresults != ''&& putwmethodsearchresults.length > 0)
	{

		varMergeLP = putwmethodsearchresults[0].getValue('custrecord_mergelp');
	}

	return varMergeLP;
}
//case# 20148325 ends
