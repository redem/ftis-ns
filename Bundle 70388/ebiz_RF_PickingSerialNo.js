/***************************************************************************
 eBizNET Solutions Inc              
 ***************************************************************************
 **     	   $Source: /cvs/products/NSWMS/EoN/RF/Outbound/Suitelet/ebiz_RF_PickingSerialNo.js,v $
 *     	   $Revision: 1.6.4.9.4.5.2.41.2.3 $
 *     	   $Date: 2015/11/18 10:35:52 $
 *     	   $Author: gkalla $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_PickingSerialNo.js,v $
 * Revision 1.6.4.9.4.5.2.41.2.3  2015/11/18 10:35:52  gkalla
 * case# 201415739
 * Expected quantity updated after scanned last serial number. So in detail task screen displaying wrong qty.
 *
 * Revision 1.6.4.9.4.5.2.41.2.2  2015/11/06 16:03:34  aanchal
 * 2015.2 Issue Fix
 * 201415096
 *
 * Revision 1.6.4.9.4.5.2.41.2.1  2015/10/08 15:21:40  aanchal
 * 2015.2 issues
 * 201414688
 *
 * Revision 1.6.4.9.4.5.2.41  2015/08/21 15:32:13  grao
 * 2015.2   issue fixes 201414015
 *
 * Revision 1.6.4.9.4.5.2.40  2015/08/11 15:38:19  grao
 * 2015.2   issue fixes  201413666
 *
 * Revision 1.6.4.9.4.5.2.39  2015/08/04 11:22:28  schepuri
 * case# 201413730
 *
 * Revision 1.6.4.9.4.5.2.38  2015/07/24 15:31:12  grao
 * 2015.2   issue fixes  201413033
 *
 * Revision 1.6.4.9.4.5.2.37  2015/07/07 15:50:19  nneelam
 * case# 201413333
 *
 * Revision 1.6.4.9.4.5.2.36  2015/06/16 07:30:37  nneelam
 * case# 201413020
 *
 * Revision 1.6.4.9.4.5.2.35  2015/05/18 13:30:09  schepuri
 * case# 201412826
 *
 * Revision 1.6.4.9.4.5.2.34  2014/11/28 10:51:34  skavuri
 * Case# 201411153 Std bundle issue fixed
 *
 * Revision 1.6.4.9.4.5.2.33  2014/10/22 14:11:30  schepuri
 * 201410744 issue fix
 *
 * Revision 1.6.4.9.4.5.2.32  2014/09/08 16:16:40  sponnaganti
 * case# 20148261
 * Stnd Bundle issue fix
 *
 * Revision 1.6.4.9.4.5.2.31  2014/07/07 06:01:20  skreddy
 * case # 20149302
 * Dealmed prod issue fix
 *
 * Revision 1.6.4.9.4.5.2.30  2014/06/13 12:23:38  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.6.4.9.4.5.2.29  2014/05/30 00:41:05  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.6.4.9.4.5.2.28  2014/03/07 15:53:22  skreddy
 * case 20127595
 * demo account isue fixs
 *
 * Revision 1.6.4.9.4.5.2.27  2014/03/05 12:03:30  snimmakayala
 * Case #: 20127499
 *
 * Revision 1.6.4.9.4.5.2.26  2014/01/06 13:19:17  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.6.4.9.4.5.2.25  2013/12/19 15:25:53  nneelam
 * case# 20126286
 * Std Bundle Issue Fix.
 *
 * Revision 1.6.4.9.4.5.2.24  2013/11/22 08:40:38  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue fixed related to case#201216680
 *
 * Revision 1.6.4.9.4.5.2.23  2013/11/08 14:17:26  schepuri
 * 20125623
 *
 * Revision 1.6.4.9.4.5.2.22  2013/10/10 15:47:48  rmukkera
 * Case# 20124742�
 *
 * Revision 1.6.4.9.4.5.2.21  2013/08/05 06:37:14  schepuri
 * cesium prod location display issue fix
 *
 * Revision 1.6.4.9.4.5.2.20  2013/08/01 06:29:54  grao
 * case# 20123672
 * TSG SB:Updating Location(Site) in Serial entry related issue fixes
 *
 * Revision 1.6.4.9.4.5.2.19  2013/06/25 10:07:08  gkalla
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fix
 *
 * Revision 1.6.4.9.4.5.2.18  2013/06/24 15:13:35  gkalla
 * CASE201112/CR201113/LOG201121
 * As part of Standard Bundle case# 20122757,20123135,20123137
 *
 * Revision 1.6.4.9.4.5.2.17  2013/06/19 22:56:07  gkalla
 * CASE201112/CR201113/LOG201121
 * As part of Demo issue
 *
 * Revision 1.6.4.9.4.5.2.16  2013/06/19 15:30:26  rrpulicherla
 * CASE201112/CR201113/LOG201121
 * pickingserialno
 *
 * Revision 1.6.4.9.4.5.2.15  2013/06/17 14:07:05  skreddy
 * CASE201112/CR201113/LOG201121
 * issue rellated to serial numbers
 *
 * Revision 1.6.4.9.4.5.2.14  2013/06/11 14:30:20  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.6.4.9.4.5.2.13  2013/06/10 06:12:36  skreddy
 * CASE201112/CR201113/LOG201121
 * RF order picking for serial Item
 *
 * Revision 1.6.4.9.4.5.2.12  2013/06/05 12:02:07  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.6.4.9.4.5.2.11  2013/05/13 15:08:06  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle Issue Fixes
 *
 * Revision 1.6.4.9.4.5.2.10  2013/05/07 15:37:53  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle Issue Fixes
 *
 * Revision 1.6.4.9.4.5.2.9  2013/04/26 15:49:51  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fixes
 *
 * Revision 1.6.4.9.4.5.2.8  2013/04/24 15:54:26  skreddy
 * CASE201112/CR201113/LOG201121
 * Standard bundle issue fixes
 *
 * Revision 1.6.4.9.4.5.2.7  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.6.4.9.4.5.2.6  2013/04/15 14:59:41  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.6.4.9.4.5.2.5  2013/04/12 11:04:06  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.6.4.9.4.5.2.4  2013/04/10 15:49:39  skreddy
 * CASE201112/CR201113/LOG201121
 * issue related to accepting invalid serial no
 *
 * Revision 1.6.4.9.4.5.2.3  2013/04/02 14:15:44  rmukkera
 * issue Fix related to vaidating the serailno
 *
 * Revision 1.6.4.9.4.5.2.2  2013/03/22 11:44:30  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.6.4.9.4.5.2.1  2013/03/18 06:45:56  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.6.4.9.4.5  2013/02/06 14:38:20  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.6.4.9.4.4  2012/11/27 17:56:03  spendyala
 * CASE201112/CR201113/LOG201121
 * Serial Capturing file.
 *
 * Revision 1.6.4.9.4.3  2012/11/01 14:55:23  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.6.4.9.4.2  2012/09/26 12:28:40  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi language without small characters
 *
 * Revision 1.6.4.9.4.1  2012/09/24 14:23:04  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Lnaguage
 *
 * Revision 1.6.4.9  2012/09/03 13:52:07  schepuri
 * CASE201112/CR201113/LOG201121
 * added date stamp
 *
 * Revision 1.6.4.8  2012/06/18 07:37:13  snimmakayala
 * CASE201112/CR201113/LOG201121
 * UAT and Prod issue fixes
 *
 * Revision 1.6.4.7  2012/06/11 13:45:52  spendyala
 * CASE201112/CR201113/LOG201121
 * passing parameters to query string was missing.
 *
 * Revision 1.6.4.6  2012/05/28 09:37:11  gkalla
 * CASE201112/CR201113/LOG201121
 * Issue fix for QC Check
 *
 * Revision 1.6.4.5  2012/04/18 14:27:31  mbpragada
 * CASE201112/CR201113/LOG201121
 * added Skip Location functionality
 *
 * Revision 1.6.4.4  2012/04/17 10:52:40  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * RF Packing
 *
 * Revision 1.6.4.3  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.6.4.2  2012/02/08 15:27:30  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * functional keys
 *
 * Revision 1.6.4.1  2012/02/08 12:35:51  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code Merge to brach
 *
 * Revision 1.6  2011/10/04 14:38:51  rrpulicherla
 * CASE201112/CR201113/LOG201121
 *
 * TO Functionality New Files
 *
 * Revision 1.5  2011/10/03 14:41:35  snimmakayala
 * CASE201112/CR201113/LOG20112
 * Error Screen added for Serial# wrong entry
 *
 * Revision 1.4  2011/09/30 14:31:51  gkalla
 * CASE201112/CR201113/LOG201121
 * For BOM Report in Work order screen
 *
 * Revision 1.3  2011/09/29 16:17:23  gkalla
 * CASE201112/CR201113/LOG201121
 * To integrate Serial no entry in RF outbound
 *
 * Revision 1.2  2011/09/10 14:21:13  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.1  2011/09/10 07:20:05  skota
 * CASE201112/CR201113/LOG201121
 * New suitelet to capture serial nos during outbound
 *
 * 
 *****************************************************************************/
function PickSerial(request, response){

	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);

	if (request.getMethod() == 'GET') {
		nlapiLogExecution('DEBUG', 'Into CheckinSerial');
		
		var fastpick = request.getParameter('custparam_fastpick');

		nlapiLogExecution('ERROR', 'fastpick', fastpick);

		var getLanguage = request.getParameter('custparam_language');
		nlapiLogExecution('DEBUG', 'getlangugae', getLanguage);
		var st1,st2,st3,st4;

		if( getLanguage == 'es_ES')
		{
			st1 = "ENTRAR EN SERIE NO : ";
			st2 = "PADRE ID : ";
			st3 = "ENVIAR";
			st4 = "ANTERIOR";

		}
		else
		{
			st1 = "ENTER SERIAL NO: ";
			st2 = "PARENT ID : ";
			st3 = "SEND ";
			st4 = "PREV";

		}

		var ruleDet = IsCustomScreenRequired('RFPICKINGSERIALSCAN');

		if(ruleDet!=null && ruleDet!='')
		{
			var vScreenType = ruleDet[0].getValue('custrecord_ebizrulevalue');
			var vScriptId = ruleDet[0].getValue('custrecord_ebizrule_scriptid');
			var vDeployId = ruleDet[0].getValue('custrecord_ebizrule_deployid');

			nlapiLogExecution('DEBUG', 'vScreenType', vScreenType);
			nlapiLogExecution('DEBUG', 'vScriptId', vScriptId);
			nlapiLogExecution('DEBUG', 'vDeployId', vDeployId);			
		}

		var getWaveno = request.getParameter('custparam_waveno');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getExpectedQuantity = request.getParameter('custparam_expectedquantity');
		nlapiLogExecution('DEBUG', 'getExpectedQuantity', getExpectedQuantity);	
		var getRemainingQty1 =request.getParameter('custparam_remainqty');
		nlapiLogExecution('DEBUG', 'getRemainingQty1', getRemainingQty1);
		var getBeginLocationId = request.getParameter('custparam_beginLocation');
		var getItemDescription = request.getParameter('custparam_itemdescription');
		var getItemInternalId = request.getParameter('custparam_iteminternalid');
		var getDOLineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getEndLocInternalId = request.getParameter('custparam_endlocinternalid');
		var vClusterNo = request.getParameter('custparam_clusterno');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var getItemType = request.getParameter('custparam_RecType');
		var getSerialOut = request.getParameter('custparam_SerOut');
		var getSerialIn = request.getParameter('custparam_SerIn');
		var getOrdName = request.getParameter('name');
		var getEbizOrd = request.getParameter('custparam_ebizordno');
		var getEnteredLocation = request.getParameter('custparam_endlocation');
		var getBeginBinLocation = request.getParameter('custparam_beginlocationname');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var whLocation = request.getParameter('custparam_whlocation');
		var vBatchno = request.getParameter('custparam_batchno');
		var RecordCount=request.getParameter('custparam_noofrecords');
		var NextLocation=request.getParameter('custparam_nextlocation');
		var name=request.getParameter('name');
		var ContainerSize=request.getParameter('custparam_containersize');
		var getOrderNo=request.getParameter('custparam_ebizordno');
		var detailTask=request.getParameter('custparam_detailtask');
		var getEnteredContainerNo=request.getParameter('custparam_newcontainerlp');
		var pickType=request.getParameter('custparam_picktype');
		var RecordCount=request.getParameter('custparam_nooflocrecords');
		var EntLoc = request.getParameter('custparam_EntLoc');
		var EntLocRec=request.getParameter('custparam_EntLocRec');
		var EntLocID=request.getParameter('custparam_EntLocId');
		var ExceptionFlag = request.getParameter('custparam_Exc');
		var EntLotID=request.getParameter('custparam_entLotId');

		var filter=new Array();
		if(getItemInternalId!=null&&getItemInternalId!="")
			filter[0]=new nlobjSearchFilter("internalid",null,"anyof",getItemInternalId);

		var column=new Array();
		column[0]=new nlobjSearchColumn("itemid");
		column[1]=new nlobjSearchColumn("description");
		var Itemdescription='';
		var getItem="";
		var searchres=nlapiSearchRecord("item",null,filter,column);
		if(searchres!=null&&searchres!="")
		{
			getItem=searchres[0].getValue("itemid");
			Itemdescription=searchres[0].getValue("description");
		}
		//end of code as on 051113
		nlapiLogExecution('DEBUG', 'detailTask  is', detailTask);
		nlapiLogExecution('DEBUG', 'pickType  is', pickType);

		nlapiLogExecution('DEBUG', 'getRecordInternalId', getRecordInternalId);
		var NextRecordId=request.getParameter('custparam_nextrecordid');
		nlapiLogExecution('DEBUG', 'NextRecordId', NextRecordId);
		var getContainerLPNo='';
		var getParentLPNo='';
		var getParentLPNoArray=new Array();
		if(pickType!='CL')
		{
			var SORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', getRecordInternalId);
			getContainerLPNo= SORec.getFieldValue('custrecord_container_lp_no');
			getParentLPNo =  SORec.getFieldValue('custrecord_lpno');
		}
		else
		{


			var SOFilters = new Array();

			SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
			SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));			//	Task Type - PICK

			if(getWaveno != null && getWaveno != "")
			{
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseInt(getWaveno)));
			}

			if(vClusterNo!= null && vClusterNo!="" && vClusterNo!= "null")
			{
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', vClusterNo.toString()));
			}

			if(getItemInternalId != null && getItemInternalId != "")
			{
				SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_sku_no', null, 'is', getItemInternalId));
			}

			if(getBeginBinLocation != null && getBeginBinLocation != "")
			{
				SOFilters.push(new nlobjSearchFilter('custrecord_actbeginloc', null, 'anyof', getBeginLocationId));
			}

			var SOColumns = new Array();				
			SOColumns[0] = new nlobjSearchColumn('custrecord_sku',null,'group');
			SOColumns[1] = new nlobjSearchColumn('custrecord_expe_qty',null,'sum');
			SOColumns[2] = new nlobjSearchColumn('custrecord_actbeginloc',null,'group');
			SOColumns[3] = new nlobjSearchColumn('custrecord_skudesc',null,'group');
			SOColumns[4] = new nlobjSearchColumn('custrecord_ebiz_sku_no',null,'group');					
			SOColumns[5] = new nlobjSearchColumn('custrecord_wms_location',null,'group');
			SOColumns[6] = new nlobjSearchColumn('custrecord_comp_id',null,'group');
			SOColumns[7] = new nlobjSearchColumn('custrecord_container_lp_no',null,'group');
			SOColumns[8] = new nlobjSearchColumn('custrecord_container',null,'group');
			SOColumns[9] = new nlobjSearchColumn('custrecord_from_lp_no',null,'group');
			SOColumns[10] = new nlobjSearchColumn('custrecord_container_lp_no',null,'group');

			SOColumns[0].setSort();	//SKU
			SOColumns[2].setSort();	//Location
			SOColumns[7].setSort();//Container LP

			var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);
			if(SOSearchResults!=null && SOSearchResults!='')
			{
				nlapiLogExecution('DEBUG', 'SOSearchResults length', SOSearchResults.length);
				//getContainerLPNo= SOSearchResults[0].getValue('custrecord_from_lp_no',null,'group');
				getContainerLPNo= SOSearchResults[0].getValue('custrecord_container_lp_no',null,'group');
				getParentLPNo = SOSearchResults[0].getValue('custrecord_from_lp_no',null,'group');

			
			for(var f=0;f<SOSearchResults.length;f++)
			{
				getParentLPNoArray.push(SOSearchResults[f].getValue('custrecord_from_lp_no',null,'group'));
				/*if(getParentLPNoArray=="")
					getParentLPNoArray=SOSearchResults[f].getValue('custrecord_from_lp_no',null,'group');
				else
					getParentLPNoArray=getParentLPNoArray+","+SOSearchResults[f].getValue('custrecord_from_lp_no',null,'group');*/
			}
		}
			// case# 201416821
			/*
			for(var z=0;z<getParentLPNoArray.length;z++){
				nlapiLogExecution('DEBUG', 'getParentLPNoArray[z]', getParentLPNoArray[z]);
			var filtersser = new Array();
			filtersser[0] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', getParentLPNoArray[z]);
			filtersser[1] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['3','19']);
			
			var SOColumnser = new Array();				
			SOColumnser[0] = new nlobjSearchColumn('custrecord_serialparentid');
			
			var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser,SOColumnser);
			
			if(SrchRecord!=null && SrchRecord!='')
				getParentLPNo = SrchRecord[0].getValue('custrecord_serialparentid');
			nlapiLogExecution('DEBUG', 'getParentLPNo', getParentLPNo);
			}*/
			
			nlapiLogExecution('DEBUG', 'getParentLPNoArray', getParentLPNoArray);
		}

		var getNumber;
		if(request.getParameter('custparam_number')!= null && request.getParameter('custparam_number') != "")
			getNumber = request.getParameter('custparam_number');
		else
			getNumber=0;
		var getLPNo;
		if(request.getParameter('custrecord_from_lp_no')!= null && request.getParameter('custrecord_from_lp_no') != "")
			getLPNo = request.getParameter('custrecord_from_lp_no');

		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');

		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');
		nlapiLogExecution('DEBUG', 'getContainerLPNo', getContainerLPNo);
		nlapiLogExecution('DEBUG', 'getParentLPNo', getParentLPNo);

		var getnextExpectedQuantity = request.getParameter('custparam_nextexpectedquantity');
		var getRemQty = request.getParameter('custparam_remqty');
		var getPickQty=request.getParameter('custparam_enteredqty');
		
		var TotOrderqty = request.getParameter('custparam_TotOrderqty');

		if(vScreenType=='CUSTOM')
		{
			var SOarray = new Array();
			SOarray["custparam_language"] = getLanguage;
			SOarray["custparam_detailtask"] = request.getParameter('custparam_detailtask');
			SOarray["custparam_waveno"] = request.getParameter('custparam_waveno');
			SOarray["custparam_recordinternalid"] = request.getParameter('custparam_recordinternalid');
			SOarray["custparam_containerlpno"] = request.getParameter('custparam_containerlpno');
			SOarray["custparam_newcontainerlp"] = request.getParameter('custparam_newcontainerlp');
			SOarray["custparam_expectedquantity"] = request.getParameter('custparam_expectedquantity');
			SOarray["custparam_beginLocation"] = request.getParameter('custparam_beginLocation');
			SOarray["custparam_item"] = request.getParameter('custparam_item');
			SOarray["custparam_itemdescription"] = request.getParameter('custparam_itemdescription');
			SOarray["custparam_iteminternalid"] = request.getParameter('custparam_iteminternalid');
			SOarray["custparam_dolineid"] = request.getParameter('custparam_dolineid');
			SOarray["custparam_invoicerefno"] = request.getParameter('custparam_invoicerefno');
			SOarray["custparam_beginlocationname"] = request.getParameter('custparam_beginlocationname');
			SOarray["custparam_endlocinternalid"] = request.getParameter('custparam_endlocinternalid');
			SOarray["custparam_endlocation"] = request.getParameter('custparam_endlocation');
			SOarray["custparam_orderlineno"] = request.getParameter('custparam_orderlineno');
			SOarray["custparam_clusterno"] = request.getParameter('custparam_clusterno');
			SOarray["custparam_noofrecords"] = RecordCount;
			SOarray["custparam_batchno"] = request.getParameter('custparam_batchno');
			SOarray["custparam_nextlocation"] = request.getParameter('custparam_nextlocation');
			SOarray["name"] = request.getParameter('name');
			SOarray["custparam_containersize"] = request.getParameter('custparam_containersize');
			SOarray["custparam_ebizordno"] = request.getParameter('custparam_ebizordno');
			SOarray["custparam_whlocation"] = request.getParameter('custparam_whlocation');
			SOarray["custparam_RecType"] = request.getParameter('custparam_RecType');
			SOarray["custparam_nooflocrecords"] = request.getParameter('custparam_nooflocrecords');
			SOarray["custparam_nextrecordid"]=request.getParameter('custparam_nextrecordid');
			SOarray["custparam_picktype"]=request.getParameter('custparam_picktype');
			SOarray["custparam_number"]=request.getParameter('custparam_number');

			response.sendRedirect('SUITELET', vScriptId, vDeployId, false, SOarray );
			return;

		}	



		var functionkeyHtml=getFunctionkeyScript('_rf_checkin_lp'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>"; 
		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";
	//	html = html + " document.getElementById('enterserialno').focus();";
		html = html + "function stopRKey(evt) { ";
		html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
		html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
		html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
		html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
		html = html + "	  alert('System Processing, Please wait...');";
		html = html + "	  return false;}} ";
		html = html + "	} ";
		html = html + "	document.onkeypress = stopRKey; ";
		html = html + "</script>";
		html = html +functionkeyHtml;
		html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_checkin_lp' method='POST'>";
		html = html + "		<table><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>";
		html = html + "			<tr>";
		html = html + "				<input type='hidden' name='hdnWaveNo' value=" + getWaveno + ">";
		html = html + "				<input type='hidden' name='hdnFetchedContainerLPNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";		
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLPNo + ">";
		html = html + "				<input type='hidden' name='hdnExpectedQuantity' value=" + getExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationId' value=" + getBeginLocationId + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemDescription' value=" + getItemDescription + ">";
		html = html + "				<input type='hidden' name='hdnItemInternalId' value=" + getItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnDOLineId' value=" + getDOLineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnEndLocInternalId' value=" + getEndLocInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEnteredLocation' value=" + getEnteredLocation + ">";
		html = html + "				<input type='hidden' name='hdnfastpick' value=" + fastpick + ">";
		//		case no Issue: 20123645  
		html = html + "				<input type='hidden' name='hdnBeginBinLocation' value='" + getBeginBinLocation + "'>";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdngetnumber' value=" + getNumber + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + vClusterNo + ">";
		html = html + "				<input type='hidden' name='hdnItemType' value=" + getItemType + ">";
		html = html + "				<input type='hidden' name='hdnSerOut' value=" + getSerialOut + ">";
		html = html + "				<input type='hidden' name='hdnSerIn' value=" + getSerialIn + ">";
		html = html + "				<input type='hidden' name='hdnOrdName' value=" + getOrdName + ">";
		html = html + "				<input type='hidden' name='hdnEbizOrdNo' value=" + getEbizOrd + ">";
		html = html + "				<input type='hidden' name='hdnFromLp' value=" + getLPNo + ">";	
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + RecordCount + ">";
		html = html + "				<input type='hidden' name='hdnbatchno' value='" + vBatchno + "'>"; //Case# 201411153
		html = html + "				<input type='hidden' name='hdnNextLocation' value=" + NextLocation + ">";
		html = html + "				<input type='hidden' name='hdnName' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + ContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnebizOrdNo' value=" + getOrderNo + ">";
		html = html + "				<input type='hidden' name='hdnNextItemId' value=" + NextItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";	
		html = html + "				<input type='hidden' name='hdnextExpectedQuantity' value=" + getnextExpectedQuantity + ">";	
		html = html + "				<input type='hidden' name='hdnActQty' value=" + getPickQty + ">";
		html = html + "				<input type='hidden' name='hdnremqty' value=" + getRemQty + ">";
		html = html + "				<input type='hidden' name='hdnEntLoc' value=" + EntLoc + ">";
		html = html + "				<input type='hidden' name='hdnEntLocRec' value=" + EntLocRec + ">";
		html = html + "				<input type='hidden' name='hdnEntLocID' value=" + EntLocID + ">";
		html = html + "				<input type='hidden' name='hdnExceptionFlag' value=" + ExceptionFlag + ">";
		html = html + "				<input type='hidden' name='hdnEntLotID' value=" + EntLotID + ">";
		html = html + "				<input type='hidden' name='hdndetailTask' value=" + detailTask + ">";
		html = html + "				<input type='hidden' name='hdnParentLP' value=" + getParentLPNo + ">";
		html = html + "				<input type='hidden' name='hdnNextrecordid' value=" + NextRecordId + ">";
		html = html + "				<input type='hidden' name='hdnLpArray' value=" + getParentLPNoArray + ">";
		html = html + "				<input type='hidden' name='hdntotalordqty' value=" + TotOrderqty + ">";
		html = html + "				<input type='hidden' name='hdnremainingqty' value=" + getRemainingQty1 + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		if(getPickQty != null && getPickQty !='' && !isNaN(getPickQty))
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + (parseFloat(getNumber) + 1) + " OF <label>" + getPickQty + "</label>";
			html = html + "			</tr>";
		}
		else
		{
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + (parseFloat(getNumber) + 1) + " OF <label>" + getExpectedQuantity + "</label>";
			html = html + "			</tr>";
		}
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>"+ st1 +"";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterserialno' id='enterserialno' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'>"+ st2 +"<label>" + getContainerLPNo + "</label>";
		html = html + "				<td align = 'left'>"+ st2 +"<label>" + getParentLPNo + "</label>";		
		html = html + "			</tr>";
		html = html + "			<tr>";
		//html = html + "				<td align = 'left'>"+ st2 +"<input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true; return false'/>";
		html = html + "				<td align = 'left'>"+ st3 +"<input name='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
		html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterserialno').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);

	}
	else {
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');

		var getLanguage = request.getParameter('hdngetLanguage');
		var detailTask=request.getParameter('hdndetailTask');
		var SOarray = new Array();
		var st1,st5,st6,st7;

		if( getLanguage == 'es_ES')
		{
			st1 = "ENTRAR EN SERIE NO : ";
			st5 = "SERIAL # YA ESCANEADOS";
			st6 = "SERIAL # NO EXISTE";
			st7 = "SERIAL # YA EXISTE";

		}
		else
		{
			st1 = "ENTER SERIAL NO: ";
			st5 = "SERIAL # ALREADY SCANNED";
			st6 = "INVALID SERIAL NO";
			st7 = "SERIAL # ALREADY EXISTS";
		}

		var getParentLPNoArray=request.getParameter('hdnLpArray');
		nlapiLogExecution('ERROR', 'getParentLPNoArray in post', getParentLPNoArray);
		SOarray["custparam_language"] = getLanguage;
		var RecCount=request.getParameter('hdnRecCount');
		SOarray["custparam_remainqty"] = request.getParameter('hdnremainingqty');
		nlapiLogExecution('ERROR', 'SOarray["custparam_remainqty"]213123', SOarray["custparam_remainqty"]);
		SOarray["custparam_detailtask"] = request.getParameter('hdndetailTask');
		SOarray["custparam_waveno"] = request.getParameter('hdnWaveNo');
		SOarray["custparam_recordinternalid"] = request.getParameter('hdnRecordInternalId');
		SOarray["custparam_containerlpno"] = request.getParameter('hdnContainerLpNo');
		SOarray["custparam_newcontainerlp"] = request.getParameter('hdnEnteredContainerLPNo');
		SOarray["custparam_expectedquantity"] = request.getParameter('hdnExpectedQuantity');
		SOarray["custparam_beginLocation"] = request.getParameter('hdnBeginLocationId');
		SOarray["custparam_item"] = request.getParameter('hdnItem');
		SOarray["custparam_itemdescription"] = request.getParameter('hdnItemDescription');
		SOarray["custparam_iteminternalid"] = request.getParameter('hdnItemInternalId');
		SOarray["custparam_dolineid"] = request.getParameter('hdnDOLineId');
		SOarray["custparam_invoicerefno"] = request.getParameter('hdnInvoiceRefNo');
		SOarray["custparam_beginlocationname"] = request.getParameter('hdnBeginBinLocation');
		SOarray["custparam_endlocinternalid"] = request.getParameter('hdnEndLocInternalId');
		SOarray["custparam_endlocation"] = request.getParameter('hdnEnteredLocation');
		SOarray["custparam_orderlineno"] = request.getParameter('hdnOrderLineNo');
		SOarray["custparam_clusterno"] = request.getParameter('hdnClusterNo');
		SOarray["custparam_noofrecords"] = RecCount;
		SOarray["custparam_batchno"] = request.getParameter('hdnbatchno');
		SOarray["custparam_nextlocation"] = request.getParameter('hdnNextLocation');
		SOarray["name"] = request.getParameter('hdnName');
		SOarray["custparam_containersize"] = request.getParameter('hdnContainerSize');
		SOarray["custparam_ebizordno"] = request.getParameter('hdnebizOrdNo');
		SOarray["custparam_whlocation"] = request.getParameter('hdnwhlocation');
		SOarray["custparam_RecType"] = request.getParameter('hdnItemType');
		SOarray["custparam_nooflocrecords"] = request.getParameter('hdnRecCount');
		SOarray["custparam_nextrecordid"]=request.getParameter('hdnNextrecordid');
		var getOrdName=request.getParameter('hdnOrdName');
		var getEbizOrd=request.getParameter('hdnEbizOrdNo');
		var getSerialNo = request.getParameter('enterserialno');
		var getNumber = request.getParameter('hdngetnumber');
		var getFromLP= request.getParameter('hdnFromLp'); 
		var getOrdLineNo = request.getParameter('hdnOrderLineNo');
		var getContainerLP = request.getParameter('hdnContainerLpNo');
		var vSkipId=request.getParameter('hdnskipid');
		var getParentLPNo=request.getParameter('hdnParentLP');
		var whlocation = request.getParameter('hdnwhlocation');
		
		var fastpick = request.getParameter('hdnfastpick');
		var TotalOrderqty = request.getParameter('hdntotalordqty');	

		nlapiLogExecution('ERROR', 'fastpick', fastpick);

		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');
		SOarray["custparam_serialno"] = request.getParameter('custparam_serialno');
		SOarray["custparam_number"] = parseFloat(request.getParameter('custparam_number'));
		SOarray["custparam_nextexpectedquantity"] = request.getParameter('hdnextExpectedQuantity');
		var getPickQty = request.getParameter('hdnActQty');
		var getItemQuantity;
		if(getPickQty != null && getPickQty !='' && !isNaN(getPickQty))
			getItemQuantity =request.getParameter('hdnActQty');
		else
			getItemQuantity =request.getParameter('hdnExpectedQuantity');

		nlapiLogExecution('DEBUG', 'SOarray["custparam_nextrecordid"]', SOarray["custparam_nextrecordid"]);
		nlapiLogExecution('DEBUG', 'getItemQuantity', getItemQuantity);
		var getItemType =request.getParameter('hdnItemType');		
		var getSerOut =request.getParameter('hdnSerOut');
		var getSerIn =request.getParameter('hdnSerIn');
		var getItemId =request.getParameter('hdnItemInternalId');

		SOarray["custparam_nextiteminternalid"] = request.getParameter('hdnNextItemId');
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');

		SOarray["name"] = getOrdName;
		SOarray["custparam_ebizordno"] = getEbizOrd;
		SOarray["custrecord_from_lp_no"] = getNumber;
		SOarray["custparam_SerIn"] = getSerIn;
		//SOarray["custparam_enteredQty"] = request.getParameter('hdnActQty');
		SOarray["custparam_enteredqty"] = request.getParameter('hdnActQty');
		SOarray["custparam_remqty"] = request.getParameter('hdnremqty');
		SOarray["custparam_SerOut"] = getSerOut;

		var EntLoc = request.getParameter('hdnEntLoc');
		var EntLocRec = request.getParameter('hdnEntLocRec');
		var EntLocID = request.getParameter('hdnEntLocID');
		var ExceptionFlag = request.getParameter('hdnExceptionFlag');
		var EntLotID = request.getParameter('hdnEntLotID');

		SOarray["custparam_EntLoc"] = request.getParameter('hdnEntLoc');
		SOarray["custparam_EntLocRec"] = request.getParameter('hdnEntLocRec');
		SOarray["custparam_EntLocId"] = request.getParameter('hdnEntLocID');
		SOarray["custparam_Exc"] = request.getParameter('hdnExceptionFlag');
		SOarray["custparam_entLotId"] = request.getParameter('hdnEntLotID');
		
		SOarray["custparam_fastpick"] = fastpick;
		SOarray["custparam_TotOrderqty"] = TotalOrderqty;



		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var optedEvent = request.getParameter('cmdPrevious');

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to RF Main Menu.

		if (sessionobj!=context.getUser()) {
			try
			{

				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}
				if (optedEvent == 'F7') {
					nlapiLogExecution('DEBUG', 'hdnpicktype',request.getParameter('hdnpicktype'));
					var vPickType = "";
					if(request.getParameter('hdnpicktype') != null && request.getParameter('hdnpicktype') != "")
					{
						vPickType = request.getParameter('hdnpicktype');
					}
					if(vPickType == "CL")
					{
						//response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_summtask', 'customdeploy_rf_cluspicking_summtask', false, SOarray);
						response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_dettask', 'customdeploy_rf_cluspicking_dettask', false, SOarray);	// case# 201412986											
					}
					else
					{
						if(fastpick!='Y')
						{
							response.sendRedirect('SUITELET', 'customscript_rf_picking_confirm', 'customdeploy_rf_picking_confirm_di', false, SOarray);

						}
						else
						response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pickingcarton', 'customdeploy_ebiz_rf_pickingcarton', false, SOarray);
						
					}
				}
				//	if the previous F7 is not clicked and based on the option selected, it navigates to the corresponding screen
				//            if (optedEvent != '' && optedEvent != null) {
				else 
					if (getSerialNo == "") {
						//	if the 'Send' button is clicked without any option value entered,
						//  it has to show an error message. The next screen to which it has to navigate is to the error screen.
						SOarray["custparam_error"] = st1;//'ENTER SERIAL NO';
						SOarray["custparam_screenno"] = '41';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						nlapiLogExecution('DEBUG', 'Enter Serial No', getSerialNo);

					}
					else {
						nlapiLogExecution('DEBUG', 'Serial No', getSerialNo);
						// case no 20125623
						//getSerialNo=SerialNoIdentification(getItemId,getSerialNo);
						//nlapiLogExecution('DEBUG', 'After Serial No Parsing', getSerialNo);
						var getActualEndDate = DateStamp();
						var getActualEndTime = TimeStamp();
						var TempSerialNoArray = new Array();
						if (request.getParameter('custparam_serialno') != null) 
							TempSerialNoArray = request.getParameter('custparam_serialno').split(',');



						nlapiLogExecution('DEBUG', 'INTO SERIAL ENTRY');
						//checking serial no's in already scanned one's
						for (var t = 0; t < TempSerialNoArray.length; t++) {
							if (getSerialNo == TempSerialNoArray[t]) {
								SOarray["custparam_error"] = st5;//"Serial # Already Scanned";//"Serial No. Already Scanned";
								SOarray["custparam_screenno"] = '41';
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								return;
							}
						}
						/*if(getItemType != 'serializedinventoryitem' && getSerOut == 'T' && getSerIn != 'T')
				{
					SOarray["custparam_number"] = parseFloat(request.getParameter('custparam_number')) + 1;
				}
				else*/ 
						//if(getItemType == 'serializedinventoryitem' || (getSerOut == 'T'  && getSerIn == 'T'))

						if(getItemType == 'serializedinventoryitem' || getSerIn == 'T' || getItemType == 'serializedassemblyitem' )
						{
							nlapiLogExecution('DEBUG', 'getSerialNo', getSerialNo);
							nlapiLogExecution('DEBUG', 'getItemId', getItemId);
							nlapiLogExecution('DEBUG', 'getContainerLP', getContainerLP);
							nlapiLogExecution('DEBUG', 'getParentLPNoArray.length', getParentLPNoArray.length);
							//checking serial no's in records
							var vLPNoArray=new Array();
							vLPNoArray=getParentLPNoArray.split(',');
							LABL1: for(var h=0;h<vLPNoArray.length;h++)
							{
								var filtersser = new Array();
								filtersser[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getSerialNo);

								if(getItemId !=null && getItemId!='')
									filtersser[1] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', getItemId);
								filtersser[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['3','19']);
								if(vPickType != "CL")
								{
									if(getParentLPNo !=null && getParentLPNo!='')
									{
										//filtersser[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', getContainerLP);
										filtersser[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', getParentLPNo);

									}
								}
								else
								{
									if(vLPNoArray[h] !=null && vLPNoArray[h]!='')
									{
										//filtersser[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', getContainerLP);
										filtersser[3] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', vLPNoArray[h]);

									}
								}
								var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser);
								nlapiLogExecution('DEBUG', 'SrchRecord', SrchRecord);
								nlapiLogExecution('DEBUG', 'vLPNoArray[h]', vLPNoArray[h]);
								if(SrchRecord!=null)
								{
									getParentLPNo=vLPNoArray[h];
									break LABL1;
								}
								else
								{
									continue LABL1;
								}
							}
							if (SrchRecord==null) {
								//case# 20126286 Changed the Error Msg.
								SOarray["custparam_error"] = "GIVEN SERIAL NO. NOT IN STORAGE.";//"Serial # Does Not Exist";//"Serial No. Does Not Exists";
								SOarray["custparam_screenno"] = '41';
								response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
								return;
							}
							for ( var i = 0; i < SrchRecord.length; i++) {
								var searchresultserial = SrchRecord[i];
								var vserialid = searchresultserial.getId();
								nlapiLogExecution('DEBUG', 'Serial ID Internal Id :',vserialid);

								nlapiLogExecution('DEBUG', "ebizSO" , getEbizOrd);
								nlapiLogExecution('DEBUG', "vLineNo" , getOrdLineNo);
								nlapiLogExecution('DEBUG', "Item" , getItemId);

								var fields = new Array();
								var values = new Array();
								fields[0] = 'custrecord_serialwmsstatus';
								fields[1] = 'custrecord_serialebizsono';
								fields[2] = 'custrecord_serialsono';
								fields[3] = 'custrecord_serialsolineno';
								fields[4] = 'custrecord_serialparentid';
								fields[5] = 'custrecord_serialitem';
								fields[6] = 'custrecord_serial_location';
								fields[7] = 'custrecord_serialcontlpno';// case no 201412826
								
								values[0] = '8';//STATUS.OUTBOUND.PICK_CONFIRMED
								values[1] = getEbizOrd;
								values[2] = getOrdName;
								values[3] = getOrdLineNo;
								values[4] = getContainerLP;
								values[5] = getItemId;
								values[6] = whlocation;
								values[7] = getContainerLP;

								var updatefields = nlapiSubmitField(
										'customrecord_ebiznetserialentry',
										vserialid, fields, values);
								nlapiLogExecution('DEBUG', 'Record Updated :');
							}
							/*else {
						// nlapiLogExecution('DEBUG', 'SERIAL NO NOT FOUND');
						if (request.getParameter('custparam_serialno') == null || request.getParameter('custparam_serialno') == "") {
							SOarray["custparam_serialno"] = getSerialNo;
						}
						else {
							SOarray["custparam_serialno"] = request.getParameter('custparam_serialno') + ',' + getSerialNo;
						}

						//SOarray["custparam_number"] = parseFloat(request.getParameter('custparam_number')) + 1;
					}*/
						}
						else
						{
							nlapiLogExecution('DEBUG', 'getItemId', getItemId);
							var filtersser = new Array();
							filtersser[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', getSerialNo);
							if(getItemId !=null && getItemId!='')
								filtersser[1] = new nlobjSearchFilter('custrecord_serialitem', null, 'anyof', getItemId);
							filtersser[2] = new nlobjSearchFilter('custrecord_serialwmsstatus', null, 'anyof', ['19']);


							var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser);
							nlapiLogExecution('DEBUG', 'SrchRecord', SrchRecord);

							if (SrchRecord!=null && SrchRecord!='' ) {
								nlapiLogExecution('DEBUG', 'SERIAL NUMBER', 'EXISTS');
								for ( var i = 0; i < SrchRecord.length; i++) {
									var searchresultserial = SrchRecord[i];
									var vserialid = searchresultserial.getId();
									nlapiLogExecution('DEBUG', 'Serial ID Internal Id :',vserialid);

									nlapiLogExecution('DEBUG', "ebizSO" , getEbizOrd);
									nlapiLogExecution('DEBUG', "vLineNo" , getOrdLineNo);
									nlapiLogExecution('DEBUG', "Item" , getItemId);

									var fields = new Array();
									var values = new Array();
									/*fields[0] = 'custrecord_serialwmsstatus';
							fields[1] = 'custrecord_serialebizsono';
							fields[2] = 'custrecord_serialsono';
							fields[3] = 'custrecord_serialsolineno';
							fields[4] = 'custrecord_serialparentid';
							fields[5] = 'custrecord_serialitem';
							values[0] = '8';//STATUS.OUTBOUND.PICK_CONFIRMED
							values[1] = getEbizOrd;
							values[2] = getOrdName;
							values[3] = getOrdLineNo;
							values[4] = getContainerLP;
							values[5] = getItemId;*/

									fields[0] = 'custrecord_serialparentid';
									fields[1] = 'custrecord_serialitem';
									fields[2] = 'custrecord_serial_location';
									values[0] = getContainerLP;
									values[1] = getItemId;
									values[2] = whlocation;

									var updatefields = nlapiSubmitField(
											'customrecord_ebiznetserialentry',
											vserialid, fields, values);
									nlapiLogExecution('DEBUG', 'Record Updated :');
								}


								/*SOarray["custparam_error"] = st7;//"Serial # Already Exists";
						SOarray["custparam_screenno"] = '41';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;*/
							}
							else
							{

								nlapiLogExecution('DEBUG', 'getOrdName ',getOrdName);

								nlapiLogExecution('DEBUG', "ebizSO" , getEbizOrd);
								nlapiLogExecution('DEBUG', "vLineNo" , getOrdLineNo);
								nlapiLogExecution('DEBUG', "Item Id " , getItemId);

								var customrecord = nlapiCreateRecord('customrecord_ebiznetserialentry'); 
								customrecord.setFieldValue('custrecord_serialnumber', getSerialNo);					
						customrecord.setFieldValue('custrecord_serialwmsstatus', '8');//with status STORAGE
								customrecord.setFieldValue('custrecord_serialebizsono', getEbizOrd);
								customrecord.setFieldValue('custrecord_serialsono', getOrdName);
								customrecord.setFieldValue('custrecord_serialsolineno', getOrdLineNo);
								customrecord.setFieldValue('name', getSerialNo);
								customrecord.setFieldValue('custrecord_serialparentid', getContainerLP);
								customrecord.setFieldValue('custrecord_serialitem', getItemId);
								//commit the record to NetSuite
								var recid = nlapiSubmitRecord(customrecord, true);
							}
						}


						if (request.getParameter('custparam_serialno') == null || request.getParameter('custparam_serialno') == "") 
						{
							SOarray["custparam_serialno"] = getSerialNo;
						}
						else 
						{
							SOarray["custparam_serialno"] = request.getParameter('custparam_serialno') + ',' + getSerialNo;
						}

						//SOarray["custparam_number"] = parseFloat(request.getParameter('custparam_number')) + 1;

						SOarray["custparam_number"] = (parseFloat(getNumber)) + 1;

						TempSerialNoArray.push(getSerialNo);
						nlapiLogExecution('DEBUG', '(getNumber + 1)', (parseFloat(getNumber) + 1));
						nlapiLogExecution('DEBUG', 'getItemQuantity', getItemQuantity);
						if ((parseFloat(getNumber) + 1) < parseFloat(getItemQuantity)) {
							nlapiLogExecution('DEBUG', 'Scanning Serial No.');
							response.sendRedirect('SUITELET', 'customscript_rf_pick_serialscan', 'customdeploy_rf_pick_serialscan_di', false, SOarray);
							return;

						}
						else {
							nlapiLogExecution('DEBUG', 'Inserting Into Records');
							nlapiLogExecution('DEBUG', 'hdnpicktype',request.getParameter('hdnpicktype'));
							nlapiLogExecution('DEBUG', 'hdnpicktype',request.getParameter('hdnpicktype'));
							nlapiLogExecution('DEBUG', 'detailTask',detailTask);
							nlapiLogExecution('DEBUG', 'SOarray["custparam_detailtask"]',SOarray["custparam_detailtask"]);
							var vPickType = "";

							var expqty = getItemQuantity;
							var vOTTaskId = request.getParameter('custparam_recordinternalid');

							nlapiLogExecution('DEBUG', 'expqty',expqty);
							nlapiLogExecution('DEBUG', 'vOTTaskId',vOTTaskId);
							
							
							/*var SOFilters = new Array();
							var SOColumns = new Array();
														
							SOFilters.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3])); // Task Type - PICK	

							if(getEbizOrd!=null&&getEbizOrd!="")
								SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', getEbizOrd));

							if(getParentLPNo!=null && getParentLPNo!='' && getParentLPNo!='null')
								SOFilters.push(new nlobjSearchFilter('custrecord_lpno', null, 'is', getParentLPNo));	
						
							//SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_nsconfirm_ref_no', null, 'isempty'));

							SOColumns[0] = new nlobjSearchColumn('custrecord_expe_qty');
							//SOColumns[1] = new nlobjSearchColumn('custrecord_device_upload_flag');

							var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

							if(SOSearchResults!=null && SOSearchResults!=''&& SOSearchResults.length>0)
							{
								nlapiLogExecution('DEBUG', 'SOSearchResults.length', SOSearchResults.length);
								nlapiLogExecution('DEBUG', 'SOSearchResults[0].getId()', SOSearchResults[0].getId());
								for (var i = 0; i < SOSearchResults.length; i++)
								{
									nlapiSubmitField('customrecord_ebiznet_trn_opentask', SOSearchResults[0].getId(),'custrecord_expe_qty', expqty);
								}
							}*/
							
							
							
							

							/*if(vOTTaskId != null && vOTTaskId != '')
							{
								var vOTRec=nlapiLoadRecord('customrecord_ebiznet_trn_opentask',vOTTaskId);
								vOTRec.setFieldValue('custrecord_expe_qty', expqty);
								nlapiSubmitRecord(vOTRec, false, true);
							}*/
							if(request.getParameter('hdnpicktype') != null && request.getParameter('hdnpicktype') != "")
							{
								vPickType = request.getParameter('hdnpicktype');
							}
							if(vPickType == "CL" && detailTask != '' && detailTask !='F')
							{
								SOarray["custparam_serialscanned"] = "true";
								response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_dettask', 'customdeploy_rf_cluspicking_dettask', false, SOarray);						
							}
							else if(vPickType != "CL"  )
							{
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_pickingcarton', 'customdeploy_ebiz_rf_pickingcarton', false, SOarray);
							}
							else if(vPickType == "CL" && detailTask != '' && detailTask =='F')
							{
								SOarray["custparam_serialscanned"] = "true";
								response.sendRedirect('SUITELET', 'customscript_rf_cluspicking_summtask', 'customdeploy_rf_cluspicking_summtask', false, SOarray);
								return;
							}
						}
					}
			}
			catch (e)  {
				nlapiLogExecution('DEBUG', 'catch before finally',e);
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');

			}
		}
		else
		{
			SOarray["custparam_screenno"] = '12';
			SOarray["custparam_error"] = 'SERIAL# ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
		}
	}

	nlapiLogExecution('DEBUG', 'Done customrecord', 'Success');
}

function custChknPutwRecCreation(po, poValue, quan, lineno, remQty, lineCnt, ItemId, ItemName, ItemDesc, ItemStatus, PackCode, BaseUOM, LP, BinLoc, RcvQty, ActualEndDate, ActualEndTime, poreceiptno, BeginLocation, ActualBeginDate, ActualBeginTime, ActualBeginTimeAMPM, BeginLocationId, BatchNo, MfgDate, ExpDate, BestBeforeDate, LastDate, FifoDate, FifoCode, itemrectype){
	nlapiLogExecution('DEBUG', 'Into custChknPutwRecCreation', 'custChknPutwRecCreation');
	var now = new Date();
	var stagelocid, docklocid;
	var stagesearchresult = new nlapiSearchRecord('customrecord_ebiznet_location', null, new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'anyof', [8]));
	if (stagesearchresult != null && stagesearchresult.length > 0) {
		stagelocid = stagesearchresult[0].getId();
	}
	var docksearchresult = new nlapiSearchRecord('customrecord_ebiznet_location', null, new nlobjSearchFilter('custrecord_ebizlocationtype', null, 'anyof', [3]));
	if (docksearchresult != null && docksearchresult.length > 0) {
		docklocid = docksearchresult[0].getId();
	}
	if (BinLoc != null) //Creating custom record with CHKN and PUTW task,
	{
		//create the openTask record With CHKN Task Type
		var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating CHKN Record', 'TRN_OPENTASK');

		//populating the fields
		customrecord.setFieldValue('name', poValue);
		customrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		//        customrecord.setFieldValue('custrecord_ebiz_sku_no', ItemName);
		customrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		customrecord.setFieldValue('custrecord_act_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_lpno', LP);
		customrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		customrecord.setFieldValue('custrecord_line_no', lineno);
		customrecord.setFieldValue('custrecord_packcode', PackCode);
		customrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		customrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		customrecord.setFieldValue('custrecord_tasktype', 1); //For Check-In (CHKN)
		//        customrecord.setFieldValue('custrecord_actbeginloc', BinLoc);
		customrecord.setFieldValue('custrecord_actbeginloc', stagelocid);
		customrecord.setFieldValue('custrecord_actendloc', docklocid);
		//        customrecord.setFieldValue('custrecord_actendloc', 'INB-1');
		customrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		//Added for Item name and desc
		customrecord.setFieldValue('custrecord_sku', ItemId);
		customrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//For eBiznet Receipt Number .
		customrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		//        customrecord.setFieldValue('custrecordact_begin_date', (parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
		customrecord.setFieldValue('custrecordact_begin_date', ActualBeginDate);
		//        customrecord.setFieldValue('custrecord_act_end_date', (parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
		customrecord.setFieldValue('custrecord_act_end_date', ActualEndDate);

		/*        
         //Adding fields to update time zones.
         //        customrecord.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
         customrecord.setFieldValue('custrecord_actualbegintime', ActualBeginDate);
         //        customrecord.setFieldValue('custrecord_actualendtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
         customrecord.setFieldValue('custrecord_actualendtime', ActualEndTime);
         customrecord.setFieldValue('custrecord_recordtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
         customrecord.setFieldValue('custrecord_recordupdatetime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
         customrecord.setFieldValue('custrecord_current_date', (parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
         customrecord.setFieldValue('custrecord_upd_date', (parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
		 */
		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		customrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		nlapiLogExecution('DEBUG', 'getActualBeginTime', getActualBeginTime);
		customrecord.setFieldValue('custrecord_actualendtime', ActualEndTime);
		customrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		customrecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
		customrecord.setFieldValue('custrecord_current_date', DateStamp());
		customrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		//        customrecord.setFieldValue('custrecord_status_flag', 'C');
		customrecord.setFieldValue('custrecord_wms_status_flag', 1);

		//LN,Fields for Lot/Batch Sku's
		if (itemrectype == 'lotnumberedinventoryitem') {
			customrecord.setFieldValue('custrecord_batch_no', BatchNo);
			customrecord.setFieldValue('custrecord_expirydate', MfgDate);
			customrecord.setFieldValue('custrecord_fifodate', FifoDate);
		}

		nlapiLogExecution('DEBUG', 'Submitting CHKN record', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(customrecord);
		nlapiLogExecution('DEBUG', 'Done CHKN Record Insertion :', 'Success');

		//create the openTask record With PUTW Task Type

		var putwrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating PUTW record', 'TRN_OPENTASK');

		//populating the fields
		putwrecord.setFieldValue('name', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		//putwrecord.setFieldValue('custrecord_ebiz_sku_no', ItemName);
		putwrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		//        putwrecord.setFieldValue('custrecord_act_qty', quan);
		putwrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		putwrecord.setFieldValue('custrecord_lpno', LP);
		putwrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		putwrecord.setFieldValue('custrecord_line_no', lineno);
		putwrecord.setFieldValue('custrecord_packcode', PackCode);
		putwrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		putwrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		putwrecord.setFieldValue('custrecord_tasktype', 2); //For Putaway (PUTW)
		nlapiLogExecution('DEBUG', 'Fetched Begin Location', BeginLocation);

		putwrecord.setFieldValue('custrecord_actbeginloc', BeginLocationId);
		putwrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		//For eBiznet Receipt Number .
		putwrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		putwrecord.setFieldValue('custrecordact_begin_date', DateStamp());


		//Adding fields to update time zones.
		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		putwrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		putwrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		putwrecord.setFieldValue('custrecord_current_date', DateStamp());
		putwrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		//        putwrecord.setFieldValue('custrecord_status_flag', 'L');
		putwrecord.setFieldValue('custrecord_wms_status_flag', 2);

		//Added for Item name and desc
		putwrecord.setFieldValue('custrecord_sku', ItemId);
		putwrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//LN,Fields for Lot/Batch Sku's
		if (itemrectype == 'lotnumberedinventoryitem') {
			putwrecord.setFieldValue('custrecord_batch_no', BatchNo);
			putwrecord.setFieldValue('custrecord_expirydate', MfgDate);
			putwrecord.setFieldValue('custrecord_fifodate', FifoDate);
		}

		//commit the record to NetSuite
		var putwrecordid = nlapiSubmitRecord(putwrecord);
		nlapiLogExecution('DEBUG', 'Done PUTW Record Insertion ', 'TRN_OPENTASK');

	}//end if binloc is not null
	else {
		//create the openTask record With CHKN Task Type
		var customrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating customrecord', 'TRN_OPENTASK');

		//populating the fields
		customrecord.setFieldValue('name', poValue);
		customrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		//        customrecord.setFieldValue('custrecord_ebiz_sku_no', ItemName);
		customrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		customrecord.setFieldValue('custrecord_act_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		customrecord.setFieldValue('custrecord_lpno', LP);
		customrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		customrecord.setFieldValue('custrecord_line_no', lineno);
		customrecord.setFieldValue('custrecord_packcode', PackCode);
		customrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		customrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		customrecord.setFieldValue('custrecord_tasktype', 1); //For Check-In (CHKN)
		customrecord.setFieldValue('custrecord_actbeginloc', stagelocid);
		customrecord.setFieldValue('custrecord_actendloc', docklocid);
		customrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		customrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);


		customrecord.setFieldValue('custrecordact_begin_date', ActualBeginDate);
		customrecord.setFieldValue('custrecord_act_end_date', ActualEndDate);
		//Adding fields to update time zones.
		var getActualBeginTime = ActualBeginTime + ' ' + ActualBeginTimeAMPM;
		customrecord.setFieldValue('custrecord_actualbegintime', getActualBeginTime);
		customrecord.setFieldValue('custrecord_actualendtime', ActualEndTime);
		customrecord.setFieldValue('custrecord_recordtime', TimeStamp());
		customrecord.setFieldValue('custrecord_recordupdatetime', TimeStamp());
		customrecord.setFieldValue('custrecord_current_date', DateStamp());
		customrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		//        customrecord.setFieldValue('custrecord_status_flag', 'C');
		customrecord.setFieldValue('custrecord_wms_status_flag', 1);

		//Added for Item name and desc
		customrecord.setFieldValue('custrecord_sku', ItemId);
		customrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//LN,Fields for Lot/Batch Sku's
		if (itemrectype == 'lotnumberedinventoryitem') {
			customrecord.setFieldValue('custrecord_batch_no', BatchNo);
			customrecord.setFieldValue('custrecord_expirydate', MfgDate);
			customrecord.setFieldValue('custrecord_fifodate', FifoDate);
		}

		nlapiLogExecution('DEBUG', 'Submitting customrecord', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(customrecord);
		nlapiLogExecution('DEBUG', 'Done CHKN Record Insertion :', 'Success');

		//create the openTask record With PUTW Task Type
		var putwrecord = nlapiCreateRecord('customrecord_ebiznet_trn_opentask');
		nlapiLogExecution('DEBUG', 'Creating customrecord', 'TRN_OPENTASK');

		//populating the fields
		putwrecord.setFieldValue('name', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_concept_po', poValue);
		putwrecord.setFieldValue('custrecord_ebiz_sku_no', ItemId);
		//        putwrecord.setFieldValue('custrecord_act_qty', quan);
		putwrecord.setFieldValue('custrecord_expe_qty', parseFloat(quan).toFixed(4));
		putwrecord.setFieldValue('custrecord_lpno', LP);
		putwrecord.setFieldValue('custrecord_ebiz_lpno', LP);
		putwrecord.setFieldValue('custrecord_line_no', lineno);
		putwrecord.setFieldValue('custrecord_packcode', PackCode);
		putwrecord.setFieldValue('custrecord_sku_status', ItemStatus);
		putwrecord.setFieldValue('custrecord_uom_id', BaseUOM);
		putwrecord.setFieldValue('custrecord_tasktype', 2); //For Putaway (PUTW)
		//customrecord.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
		// customrecord.setFieldValue('custrecord_actbeginloc', BinLoc);
		//customrecord.setFieldValue('custrecord_actendloc', BinLoc);
		putwrecord.setFieldValue('custrecord_ebiz_cntrl_no', po);

		putwrecord.setFieldValue('custrecord_ebiz_receipt_no', poreceiptno);

		//Adding fields to update time zones.

		//customrecord.setFieldValue('custrecord_actualbegintime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
		// customrecord.setFieldValue('custrecord_actualendtime',   ((curr_hour)+":"+(curr_min)+" " +a_p));
		//customrecord.setFieldValue('custrecord_recordtime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
		//customrecord.setFieldValue('custrecord_recordupdatetime', ((curr_hour) + ":" + (curr_min) + " " + a_p));
		/*
         putwrecord.setFieldValue('custrecord_current_date', (parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
         putwrecord.setFieldValue('custrecord_upd_date', (parseFloat(now.getMonth()) + 1) + '/' + (parseFloat(now.getDate())) + '/' + now.getFullYear());
		 */
		putwrecord.setFieldValue('custrecord_current_date', DateStamp());
		putwrecord.setFieldValue('custrecord_upd_date', DateStamp());

		//Status flag .
		//        customrecord.setFieldValue('custrecord_status_flag', 'N');
		putwrecord.setFieldValue('custrecord_wms_status_flag', 6);

		//Added for Item name and desc
		putwrecord.setFieldValue('custrecord_sku', ItemId);
		putwrecord.setFieldValue('custrecord_skudesc', ItemDesc);

		//LN,Fields for Lot/Batch Sku's
		if (itemrectype == 'lotnumberedinventoryitem') {
			putwrecord.setFieldValue('custrecord_batch_no', BatchNo);
			putwrecord.setFieldValue('custrecord_expirydate', MfgDate);
			putwrecord.setFieldValue('custrecord_fifodate', FifoDate);
		}

		nlapiLogExecution('DEBUG', 'Submitting customrecord', 'TRN_OPENTASK');

		//commit the record to NetSuite
		var recid = nlapiSubmitRecord(putwrecord);
		nlapiLogExecution('DEBUG', 'Done CHKN Record Insertion :', 'Success');
	}
}
