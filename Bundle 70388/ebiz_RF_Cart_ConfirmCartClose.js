
/***************************************************************************
 eBizNET Solutions Inc
****************************************************************************
*
*� $Source: /cvs/products/eBizNET4.5/EoN/RF/Inbound/Suitelet/ebiz_RF_Cart_ConfirmCartClose.js,v $
*� $Revision: 1.2.2.2.4.2.4.7 $
*� $Date: 2014/06/13 07:53:40 $
*� $Author: skavuri $
*� $Name: t_NSWMS_2014_1_3_125 $
*
* DESCRIPTION
*� Functionality
*
* REVISION HISTORY
*� $Log: ebiz_RF_Cart_ConfirmCartClose.js,v $
*� Revision 1.2.2.2.4.2.4.7  2014/06/13 07:53:40  skavuri
*� Case# 20148882 (added Focus Functionality for Textbox)
*�
*� Revision 1.2.2.2.4.2.4.6  2014/06/06 06:21:21  skavuri
*� Case# 20148749 (Refresh Functionality ) SB Issue Fixed
*�
*� Revision 1.2.2.2.4.2.4.5  2014/05/30 00:26:47  nneelam
*� case#  20148622
*� Stanadard Bundle Issue Fix.
*�
*� Revision 1.2.2.2.4.2.4.4  2013/07/15 14:26:38  rrpulicherla
*� case# 20123391
*� GFT Issue fixes
*�
*� Revision 1.2.2.2.4.2.4.3  2013/06/11 14:30:41  schepuri
*� Error Code Change ERROR to DEBUG
*�
*� Revision 1.2.2.2.4.2.4.2  2013/04/17 15:53:17  skreddy
*� CASE201112/CR201113/LOG201121
*� issue fixes
*�
*� Revision 1.2.2.2.4.2.4.1  2013/03/19 11:53:20  snimmakayala
*� CASE201112/CR201113/LOG2012392
*� Production and UAT issue fixes.
*�
*� Revision 1.2.2.2.4.2  2012/11/11 03:40:30  snimmakayala
*� CASE201112/CR201113/LOG201121
*� GSUSA UAT issue fixes.
*�
*� Revision 1.2.2.2.4.1  2012/09/21 14:57:16  grao
*� CASE201112/CR201113/LOG201121
*� Converting Multilanguage
*�
*� Revision 1.2.2.2  2012/03/15 12:57:10  schepuri
*� CASE201112/CR201113/LOG201121
*� new file
*�
*� Revision 1.2  2012/03/14 13:41:45  rmukkera
*� CASE201112/CR201113/LOG201121
*� cartno added in the message
*�
*� Revision 1.1  2012/03/13 14:08:41  rmukkera
*� CASE201112/CR201113/LOG201121
*� new file
*�
*
****************************************************************************/

function ConfirmCartClose(request, response)
{
	if (request.getMethod() == 'GET') 
	{    

		    var getCartLP = request.getParameter('custparam_cartlpno');	    
		    
		    var getLanguage = request.getParameter('custparam_language');
			/*POarray["custparam_language"] = getLanguage;*/
			nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
	    	
			
			var st0,st1,st2;
			if( getLanguage == 'es_ES')
			{
				st0 = "";
				st1 = "SI";
				st2 = "NO";
			}
			else
			{
				st0 = "";
				st1 = "YES";
				st2 = "NO";
			}
			var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
			var html = "<html><head><title>" + st0 + "</title>";
			html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
			html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
			//Case# 20148749 Refresh Functionality starts
			html = html + "var version = navigator.appVersion;";
			html = html + "document.onkeydown = function (e) {";
			html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
			html = html + "if ((version.indexOf('MSIE') != -1)) { ";
			html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
			html = html + "else {if (keycode == 116)return false;}";
			html = html + "};";
			//Case# 20148749 Refresh Functionality ends
			//html = html + " document.getElementById('cmdSend').focus();";        
			html = html + "</script>";     
			html = html +functionkeyHtml;
			html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
			html = html + "	<form name='_rf_cluster_no' method='POST'>";
			html = html + "		<table>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>Is Receiving completed with the CART# : "+getCartLP+" ?";	 
			html = html + "				</td>";
			html = html + "			</tr>"; 
			html = html + "			<tr><td></td></tr>"; 
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st1 + " <input name='cmdSend' id='cmdSend' type='submit' value='ENT'/>";
			html = html + "				" + st2 + "	 <input name='cmdPrevious' type='submit' value='F7'/>";    
			html = html + "				<input type='hidden' name='hdncartlp' value='" + getCartLP + "'>";
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "		 </table>";
			html = html + "	</form>";
			//Case# 20148882 (added Focus Functionality for Textbox)
			html = html + "<script type='text/javascript'>document.getElementById('cmdSend').focus();</script>";
			html = html + "</body>";
			html = html + "</html>";

			response.write(html);
		
	}
	else 
	{
		var optedEvent = request.getParameter('cmdPrevious');
		var POarray =new Array();
		if (optedEvent == 'F7') {
			POarray["custparam_cartlpno"] =  request.getParameter('hdncartlp');			
			response.sendRedirect('SUITELET', 'customscript_rf_putaway_cartlp', 'customdeploy_rf_putaway_cartlp_di', false, POarray);
		}
		else
		{ 

			var filtersmlp = new Array(); 
			filtersmlp[0] = new nlobjSearchFilter('custrecord_ebiz_lpmaster_lp', null, 'is', request.getParameter('hdncartlp'));

			var SrchRecord = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, filtersmlp);

			if (SrchRecord != null && SrchRecord.length > 0) {
				var rec=nlapiLoadRecord('customrecord_ebiznet_master_lp',SrchRecord[0].getId());
				rec.setFieldValue('custrecord_ebiz_cart_closeflag', 'T');
				
				var recid = nlapiSubmitRecord(rec, false, true);
				if(recid!=null)
				{
					var filters = new Array();				
					filters[0] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [2,6]);
					filters[1] = new nlobjSearchFilter('custrecord_transport_lp', null, 'is', request.getParameter('hdncartlp'));

					var columns = new Array();
					columns[0] = new nlobjSearchColumn('custrecord_transport_lp');
					columns[1] = new nlobjSearchColumn('custrecord_lpno');
					columns[2] = new nlobjSearchColumn('custrecord_expe_qty');
					columns[3] = new nlobjSearchColumn('custrecord_actbeginloc');
					columns[4] = new nlobjSearchColumn('custrecord_sku');
					columns[5] = new nlobjSearchColumn('custrecord_skudesc');
					var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
					var POarray = new Array();
					
					var getLanguage = request.getParameter('hdngetLanguage');
					POarray["custparam_language"] = getLanguage;
					nlapiLogExecution('DEBUG', 'getLanguage', POarray["custparam_language"]);
			    	
					var getOptedField = request.getParameter('hdnOptedField');



					if (searchresults != null && searchresults.length > 0 ) {
						POarray["custparam_option"] = getOptedField;
						POarray["custparam_recordcount"] = searchresults.length;			       
						POarray["custparam_screenno"] = 'CRT9';
						nlapiLogExecution('DEBUG', 'SearchResults of given LP', 'LP is found');
						POarray["custparam_cartno"] = searchresults[0].getValue('custrecord_transport_lp');
						nlapiLogExecution('DEBUG', 'get LP', POarray["custparam_lpno"]);						
						var getLPId = searchresults[0].getId();
						nlapiLogExecution('DEBUG', 'get LP Internal Id', getLPId);						
						POarray["custparam_quantity"] = searchresults[0].getValue('custrecord_expe_qty');
						POarray["custparam_location"] = searchresults[0].getValue('custrecord_actbeginloc');
						nlapiLogExecution('DEBUG', 'Location', POarray["custparam_location"]);
						POarray["custparam_item"] = searchresults[0].getValue('custrecord_sku');
						POarray["custparam_itemtext"] = searchresults[0].getText('custrecord_sku');
						POarray["custparam_itemDescription"] = searchresults[0].getValue('custrecord_skudesc');
						POarray["custparam_lpno"] = searchresults[0].getValue('custrecord_lpno');
						if (POarray["custparam_location"] != null) {
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawayconfirm', 'customdeploy_ebiz_rf_cart_putawayconfirm', false, POarray);


						}
						else
						{
							nlapiLogExecution('DEBUG', 'SearchResults', 'Opening a new screen to scan the Item');
							response.sendRedirect('SUITELET', 'customscript_ebiz_rf_cart_putawaysku', 'customdeploy_ebiz_rf_cart_putawaysku_di', false, POarray);

						}
					}
				
	                    else {
	                        response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
	                        nlapiLogExecution('DEBUG', 'SearchResults ', 'Length is null');
	                    }
				}
	             
	            
			}
			else {
				nlapiLogExecution('DEBUG', 'cart LP NOT FOUND');

			}


		}
	}
}