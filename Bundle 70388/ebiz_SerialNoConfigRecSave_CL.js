/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************
 *
 * $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inbound/Client/ebiz_SerialNoConfigRecSave_CL.js,v $
 * $Revision: 1.2.10.2.4.12.2.4 $
 * $Date: 2015/12/01 13:04:36 $
 * $Author: schepuri $
 * $Name: t_WMS_2015_2_StdBundle_1_205 $
 *
 * DESCRIPTION
 * Functionality
 *
 * REVISION HISTORY
 * $Log: ebiz_SerialNoConfigRecSave_CL.js,v $
 * Revision 1.2.10.2.4.12.2.4  2015/12/01 13:04:36  schepuri
 * case# 201415871
 *
 * Revision 1.2.10.2.4.12.2.3  2015/11/02 23:33:10  sponnaganti
 * case# 201415286
 * Issue is same screen is using for entering serial numbers. Here we have one
 * search based on order number because of this getting exception.
 *
 * Revision 1.2.10.2.4.12.2.2  2015/11/02 14:42:54  grao
 * 2015.2 Issue Fixes 201415248
 *
 * Revision 1.2.10.2.4.12.2.1  2015/10/27 14:26:52  aanchal
 * 2015.2 issue fixes
 * 201415123
 *
 * Revision 1.2.10.2.4.12  2015/07/23 15:34:28  skreddy
 * Case# 201413310
 * SW  SB  issue fix
 *
 * Revision 1.2.10.2.4.11  2015/06/12 15:23:08  nneelam
 * case# 201413006
 *
 * Revision 1.2.10.2.4.10  2014/02/07 14:49:15  rmukkera
 * Case # 20127054
 *
 * Revision 1.2.10.2.4.9  2014/02/03 14:12:31  rmukkera
 * Case # 20126899
 *
 * Revision 1.2.10.2.4.8  2014/01/09 14:06:37  grao
 * Case# 20126703 related issue fixes in Sb issue fixes
 *
 * Revision 1.2.10.2.4.7  2014/01/08 13:51:36  schepuri
 * 20126701
 *
 * Revision 1.2.10.2.4.6  2014/01/07 14:39:26  grao
 * Case# 20126632 related issue fixes in Sb issue fixes
 *
 * Revision 1.2.10.2.4.5  2013/11/01 12:58:56  rmukkera
 * Case# 20125223
 *
 * Revision 1.2.10.2.4.4  2013/09/06 07:34:45  rmukkera
 * Case# 20124245
 *
 * Revision 1.2.10.2.4.3  2013/04/03 20:40:48  kavitha
 * CASE201112/CR201113/LOG2012392
 * TSG Issue fixes
 *
 * Revision 1.2.10.2.4.2  2013/03/22 11:44:30  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.2.10.2.4.1  2013/03/18 06:45:56  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 *
 ****************************************************************************/

function SerialRecSave()
{
	try {

		var cnt = nlapiGetLineItemCount('custpage_items');

		var poId = nlapiGetFieldValue('custpage_respserialordno');
		var poText = nlapiGetFieldText('custpage_respserialordno');
		var serialskid = nlapiGetFieldValue('custpage_serialsku');
		var serialskuline = nlapiGetFieldValue('custpage_serialskuline');
		var serialskulp = nlapiGetFieldValue('custpage_seriallp');
		var trantype=nlapiGetFieldValue('custpage_trantype');
		var frmName = nlapiGetFieldValue('custpage_serialformname');
		var seriallocation = nlapiGetFieldValue('custpage_seriallocation');
		var serialbinlocation = nlapiGetFieldValue('custpage_serialbinlocation');
		var SerialArray = new Array();
		Errorflag = "N";
		var trecord=null;
		var vConunt=0;
		for (var p = 1; p <= cnt; p++) {
			var lineUom = nlapiGetLineItemValue('custpage_items', 'custpage_serial_uom', p);
			//var lineParentId= nlapiGetLineItemValue('custpage_items','custpage_serialparentid',p);
			var lineSerialNo = nlapiGetLineItemValue('custpage_items', 'custpage_serial_serialno', p);
			var itemfulfillserialno = nlapiGetLineItemValue('custpage_items', 'custpage_serial_fulfillserialno', p);
			var serialArray = itemfulfillserialno.split(',');
			//Commented because unwanted code is executing no need to check with open task serial no's (open pick tasks were moved to closed task after Order is shipped)
			/*var openTaskSerialNo="";
			var filter1=new Array();

			filter1=new nlobjSearchFilter('custrecord_ebiz_order_no',null,'anyof',poId);
			var column1 = new Array();
			column1= new nlobjSearchColumn('custrecord_serial_no');
				var searchRecord1 = nlapiSearchRecord('customrecord_ebiznet_trn_opentask',null,filter1,column1);

				if(searchRecord1!=null&&searchRecord1!="")
					{
					for (var z=0;z<searchRecord1.length;z++)
					openTaskSerialNo=searchRecord1[z].getValue('custrecord_serial_no');
					}*/



			// case no 20126701
			if(trantype=='transferorder')
			{	


				/*if(lineSerialNo!=openTaskSerialNo)
				{

				alert('Invalid Serial no!!.Enter the serial no given at the time of picking');
				return false;
				}*/
				if(serialArray!=null && serialArray!='')
				{
					var vSerialMatch='F';
					for( var k = 0;k<serialArray.length;k++)
					{ 	 
						//	alert("lineSerialNo:" + lineSerialNo);
						//	alert("serialArray[k]:" + serialArray[k]);

						if(lineSerialNo==serialArray[k])
						{
							vSerialMatch='T';
							break;
						}
					}
					//	alert("vSerialMatch:" + vSerialMatch);
					if(vSerialMatch =='F')
					{
						alert('Matching Serial Number required.The Serial number on a transfer order receipt must have been fulfilled.');
						return false;
					}	
				}
			}


			/*	alert("poId:" + poId);
			var trecord = nlapiLoadRecord('transferorder', poId);
			var links=trecord.getLineItemCount('links');
			var id=trecord.getLineItemValue('links','id',p);
			var frecord = nlapiLoadRecord('itemfulfillment', id);
			frecord.selectLineItem('item', 1);
			var compSubRecord = frecord.viewCurrentLineItemSubrecord('item','inventorydetail');

			var fulfillserialno = compSubRecord.getLineItemValue('inventoryassignment','issueinventorynumber_display',p);

			alert("fulfillserialno:" + fulfillserialno);
			alert("lineSerialNo:" + lineSerialNo);
			if(lineSerialNo!=fulfillserialno)
			{
				alert('Matching Serial Number required.</br>The Serial number on a transfer order receipt must have been fulfilled.')
				return false;
			}*/


			/*if( trantype!=undefined && trantype=='transferorder')
			{
				var IsValidSerailNumber='F';
				if(trecord == null )
				{
					trecord = nlapiLoadRecord('transferorder', poId);
				}

				var links=trecord.getLineItemCount('links');
				if(links!=null  && links!='')
				{
					for(var j=0;j<links &&  IsValidSerailNumber=='F';j++)
					{
						var id=trecord.getLineItemValue('links','id',(parseInt(j)+1));
						var linktype = trecord.getLineItemValue('links','type',(parseInt(j)+1));

						nlapiLogExecution('DEBUG', 'linktype',linktype);
						nlapiLogExecution('DEBUG', 'id',id);

						if(linktype=='Item Fulfillment')
						{
							var frecord = nlapiLoadRecord('itemfulfillment', id);
							var fitemcount=frecord.getLineItemCount('item');
							for(var f=1;f<=fitemcount;f++)
							{
								var fitem=frecord.getLineItemValue('item','item',f);
								var fline=frecord.getLineItemValue('item','orderline',f);
								var pofline= fline-1;

								nlapiLogExecution('DEBUG', 'fitem',fitem);
								nlapiLogExecution('DEBUG', 'POarray[custparam_fetcheditemid]',serialskid);

								if(fitem==serialskid) //&& parseInt(getPOLineNo)==parseInt(pofline))
								{
									var  serialnumbers=frecord.getLineItemValue('item','serialnumbers',(parseInt(f)));
									nlapiLogExecution('DEBUG', 'serialnumbers',serialnumbers);
									if(serialnumbers!=null && serialnumbers!='')
									{
										if(serialnumbers.indexOf(ActualScanBatch)!=-1)
										{
											IsValidSerailNumber='T';
											break;
										}
									}
									if(serialnumbers!=null && serialnumbers!='')
									{
										var tserials=serialnumbers;//.split('');
										//nlapiLogExecution('DEBUG', 'serialnumbers',tserials[0]);
										if(tserials!=null && tserials!='' && tserials.length>0)
										{
											if(tserials.indexOf(lineSerialNo)!=-1)
											{
												nlapiLogExecution('DEBUG', 'getSerialNo111111',lineSerialNo);
												IsValidSerailNumber='T';
												break;
											}
										}
									}

								}
							}
						}
					}

					if(IsValidSerailNumber=='F')
					{
						//POarray["custparam_error"] = "Matching Serial Number required.</br>The Serial number on a transfer order receipt must have been fulfilled.";
						POarray["custparam_error"] = "Lot# is not matching with Lot# picked on the same TO ";
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, POarray);
						return; 
					}
				}


			}


			 */









			var filtersser = new Array();
			if(lineSerialNo != null && lineSerialNo!='' )
			{
				
				if(lineSerialNo.indexOf(' ') != -1)
				{
					alert('Serial numbers may not contain the Space character');
					return false;
				}
				
				
				filtersser[0] = new nlobjSearchFilter('custrecord_serialnumber', null, 'is', lineSerialNo);

				var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filtersser,null);
				if(SrchRecord != null && SrchRecord != '' && trantype!=undefined && trantype!='transferorder' ) 
				{
					alert('Serial # '+ lineSerialNo + ' Already Exists');
					//Errorflag = "Y";
					return false;
				}
				else
				{
					if(SerialArray.indexOf(lineSerialNo)== -1)
					{
						SerialArray.push(lineSerialNo);
					}
					else
					{
						vConunt =vConunt+1;
					}
				}
			}
		}
		if(SerialArray.length>0)
		{
			//for fine tunning below condition is commented. added new condition
			/*var count=0;
			for(i=0; i<SerialArray.length ;i++ )
			{
				for(j=0; j<SerialArray.length ;j++ ){
					if(SerialArray[i] == SerialArray[j] && j != i)
						count=count+1;
				}
			}
			if(count>0)
			{
				alert('Provide Different SerialNumbers');
				Errorflag = "Y";
				return false;
			}*/
			
			

			if(vConunt>0)
			{
				alert('Provide Different SerialNumbers');
				Errorflag = "Y";
				return false;
			}
			var noofSerialsRequired=nlapiGetLineItemCount('custpage_items');

			if(parseInt(noofSerialsRequired)!=parseInt(SerialArray.length))
			{
				alert('Please Enter all Serial Numbers');
				return false;
			}
		}//Case # 20125459� Start
		else
		{
			alert('Please Enter Serial Numbers');
			Errorflag = "Y";
			return false;
		}
//		Case # 20125459� End.
		if(Errorflag == "N") 
		{
			if(frmName != null && (frmName == "InvtAdjustment"))
			{
				var lineParentId= nlapiGetLineItemValue('custpage_items','custpage_serialparentid',1);


				var filters = new Array();
				if(lineParentId != null && lineParentId!='')
				{

					filters[0] = new nlobjSearchFilter('custrecord_serialparentid', null, 'is', lineParentId);
					filters[1] = new nlobjSearchFilter('custrecord_serialstatus', null, 'is', 'I');
					var SrchRecord = nlapiSearchRecord('customrecord_ebiznetserialentry', null, filters,null);

					if(SrchRecord != null && SrchRecord != '' && SrchRecord.length>0) 
					{
//						alert(SrchRecord.length);
						for(var n=0;n<SrchRecord.length;n++)
						{
							nlapiDeleteRecord('customrecord_ebiznetserialentry',SrchRecord[n].getId());
						}
					}
				}
			}

			/*for (var p = 1; p <= cnt; p++) {

				var lineUom = nlapiGetLineItemValue('custpage_items', 'custpage_serial_uom', p);
				//var lineParentId= nlapiGetLineItemValue('custpage_items','custpage_serialparentid',p);
				var lineSerialNo = nlapiGetLineItemValue('custpage_items', 'custpage_serial_serialno', p);

				var createSerialEntry = nlapiCreateRecord('customrecord_ebiznetserialentry');
				if(poText != null && poText != "")
				{
					createSerialEntry.setFieldValue('name', poText);
				}
				else
				{
					createSerialEntry.setFieldValue('name', lineSerialNo);
				}

				if(trantype != undefined){	
					if(trantype=='returnauthorization')
					{
						createSerialEntry.setFieldValue('custrecord_ebiz_serial_rmano', poText);
						createSerialEntry.setFieldValue('custrecord_ebiz_serial_rmalineno', serialskuline);
						createSerialEntry.setFieldValue('custrecord_serialebizrmano', poId);
					}//Case #20126899�start
					else if(trantype=='purchaseorder' || trantype=='transferorder' )
					{//Case #20126899�end
						createSerialEntry.setFieldValue('custrecord_serialpono', poText);
						createSerialEntry.setFieldValue('custrecord_serialpolineno', serialskuline);
						createSerialEntry.setFieldValue('custrecord_serialebizpono', poId);

					}
				}

				createSerialEntry.setFieldValue('custrecord_serialitem', serialskid);
				createSerialEntry.setFieldValue('custrecord_serialiteminternalid', serialskid);
				createSerialEntry.setFieldValue('custrecord_serialparentid', serialskulp);
				createSerialEntry.setFieldValue('custrecord_serialqty', '1');
				createSerialEntry.setFieldValue('custrecord_serialnumber', lineSerialNo);
				createSerialEntry.setFieldValue('custrecord_serialuomid', lineUom);
				if(frmName != null && (frmName == "InvtAdjustment" || frmName == "CyclecountRecord"))
				{
					createSerialEntry.setFieldValue('custrecord_serialwmsstatus', 3);
					createSerialEntry.setFieldValue('custrecord_serialstatus', 'I');
				}
				else if(frmName != null && frmName == "CreateInvt")
				{
					createSerialEntry.setFieldValue('custrecord_serialwmsstatus', 19);
				}
				else
				{
					createSerialEntry.setFieldValue('custrecord_serialwmsstatus', 1);
				}
				if(seriallocation != null && seriallocation != "")
					createSerialEntry.setFieldValue('custrecord_serial_location', seriallocation);
				if(serialbinlocation != null && serialbinlocation != "")
					createSerialEntry.setFieldValue('custrecord_serialbinlocation', serialbinlocation);
				var recid = nlapiSubmitRecord(createSerialEntry);
				alert('recid '+ recid);
			}*/
		}
	}
	catch(exp)
	{
		alert('Into Exception. '+exp);
		return false;
	}
	return true;
}
function onChange()
{
	//Case # 20125223� Start

	var cnt = nlapiGetLineItemCount('custpage_items');

	var poId = nlapiGetFieldValue('custpage_respserialordno');
	var poText = nlapiGetFieldText('custpage_respserialordno');
	var serialskid = nlapiGetFieldValue('custpage_serialsku');
	var serialskuline = nlapiGetFieldValue('custpage_serialskuline');
	var serialskulp = nlapiGetFieldValue('custpage_seriallp');
	var trantype=nlapiGetFieldValue('custpage_trantype');
	var frmName = nlapiGetFieldValue('custpage_serialfrmname');
	var seriallocation = nlapiGetFieldValue('custpage_seriallocation');
	var serialbinlocation = nlapiGetFieldValue('custpage_serialbinlocation');
	var SerialArray = new Array();
	Errorflag = "N";
	var trecord=null;

	var lineUom = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_serial_uom');
	//var lineParentId= nlapiGetLineItemValue('custpage_items','custpage_serialparentid',p);
	var lineSerialNo = nlapiGetCurrentLineItemValue('custpage_items', 'custpage_serial_serialno');

	if( trantype!=undefined && trantype=='transferorder')
	{
		var IsValidSerailNumber='F';
		if(trecord == null )
		{
			trecord = nlapiLoadRecord('transferorder', poId);
		}

		var links=trecord.getLineItemCount('links');
		if(links!=null  && links!='')
		{
			for(var j=0;j<links &&  IsValidSerailNumber=='F';j++)
			{
				var id=trecord.getLineItemValue('links','id',(parseInt(j)+1));
				var linktype = trecord.getLineItemValue('links','type',(parseInt(j)+1));

				nlapiLogExecution('DEBUG', 'linktype',linktype);
				nlapiLogExecution('DEBUG', 'id',id);

				if(linktype=='Item Fulfillment')
				{
					var frecord = nlapiLoadRecord('itemfulfillment', id);
					var fitemcount=frecord.getLineItemCount('item');
					for(var f=1;f<=fitemcount;f++)
					{
						var fitem=frecord.getLineItemValue('item','item',f);
						var fline=frecord.getLineItemValue('item','orderline',f);
						var pofline= fline-1;

						nlapiLogExecution('DEBUG', 'fitem',fitem);
						nlapiLogExecution('DEBUG', 'POarray[custparam_fetcheditemid]',serialskid);

						if(fitem==serialskid) //&& parseInt(getPOLineNo)==parseInt(pofline))
						{
							var  serialnumbers=frecord.getLineItemValue('item','serialnumbers',(parseInt(f)));
							nlapiLogExecution('DEBUG', 'serialnumbers',serialnumbers);
							/*if(serialnumbers!=null && serialnumbers!='')
								{
									if(serialnumbers.indexOf(ActualScanBatch)!=-1)
									{
										IsValidSerailNumber='T';
										break;
									}
								}*/
							if(serialnumbers!=null && serialnumbers!='')
							{
								var tserials=serialnumbers;//.split('');
								//nlapiLogExecution('DEBUG', 'serialnumbers',tserials[0]);
								if(tserials!=null && tserials!='' && tserials.length>0)
								{
									if(tserials.indexOf(lineSerialNo)!=-1)
									{
										nlapiLogExecution('DEBUG', 'getSerialNo111111',lineSerialNo);
										return true;

									}
									else
									{
										alert('Lot# is not matching with Lot# picked on the same TO');
										nlapiSetCurrentLineItemValue('custpage_items', 'custpage_serial_serialno','',false,false);
										return false;
									}
								}
							}

						}
					}
				}
			}


		}


	}
	else
	{
		return true;
	}




	//Case # 20125223�End.

}
if (!Array.prototype.indexOf)
{
	Array.prototype.indexOf = function(elt /*, from*/)
	{
		var len = this.length;

		var from = Number(arguments[1]) || 0;
		from = (from < 0)
		? Math.ceil(from)
				: Math.floor(from);
		if (from < 0)
			from += len;

		for (; from < len; from++)
		{
			if (from in this &&
					this[from] === elt)
				return from;
		}
		return -1;
	};
}
