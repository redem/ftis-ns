/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Outbound/Suitelet/ebiz_task_management_oub_SL.js,v $
 *     	   $Revision: 1.8.4.7.4.1.4.11.2.2 $
 *     	   $Date: 2015/11/27 16:02:49 $
 *     	   $Author: deepshikha $
 *     	   $Name: t_WMS_2015_2_StdBundle_1_195 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_task_management_oub_SL.js,v $
 * Revision 1.8.4.7.4.1.4.11.2.2  2015/11/27 16:02:49  deepshikha
 * 2015.2 Issue Fix
 * 201414483
 *
 * Revision 1.8.4.7.4.1.4.11.2.1  2015/11/27 15:58:42  deepshikha
 * 2015.2 Issue Fix
 * 201414483
 *
 * Revision 1.8.4.7.4.1.4.11  2015/04/10 21:28:10  skreddy
 * Case# 201412323�
 * changed the url path which was hard coded
 *
 * Revision 1.8.4.7.4.1.4.10  2014/12/15 06:55:51  schepuri
 * 201411136
 *
 * Revision 1.8.4.7.4.1.4.9  2014/11/27 15:33:25  grao
 * Case# 201411151 DC Dental Issue fixes
 *
 * Revision 1.8.4.7.4.1.4.8  2013/12/26 14:25:14  schepuri
 * 20126489
 *
 * Revision 1.8.4.7.4.1.4.7  2013/12/12 15:29:24  nneelam
 * Case# 20126069
 * Oub Task mngmt LP drop down issue fix..
 *
 * Revision 1.8.4.7.4.1.4.6  2013/11/01 13:42:28  rmukkera
 * Case# 20125188
 *
 * Revision 1.8.4.7.4.1.4.5  2013/04/23 15:24:56  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.8.4.7.4.1.4.4  2013/04/19 06:20:04  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.8.4.7.4.1.4.3  2013/04/17 16:06:33  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.8.4.7.4.1.4.2  2013/04/16 15:03:38  grao
 * CASE201112/CR201113/LOG201121
 * Standard bundle issues fixes
 *
 * Revision 1.8.4.7.4.1.4.1  2013/03/26 13:27:42  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Prod and UAT issue fixes.
 *
 * Revision 1.8.4.7.4.1  2012/11/01 14:55:02  schepuri
 * CASE201112/CR201113/LOG201121
 * Decimal Qty Conversions
 *
 * Revision 1.8.4.7  2012/09/13 12:47:49  spendyala
 * CASE201112/CR201113/LOG201121
 * Issue related to populating the values to the grid.
 *
 * Revision 1.8.4.6  2012/08/10 14:47:08  spendyala
 * CASE201112/CR201113/LOG201121
 * Added functionailty to fetch more than 1000 records
 * in wave and order query block
 *
 * Revision 1.8.4.5  2012/04/20 14:27:58  schepuri
 * CASE201112/CR201113/LOG201121
 * changing the Label of Batch #  field to Lot#
 *
 * Revision 1.8.4.4  2012/02/20 15:24:24  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Stable bundle issue fixes
 *
 * Revision 1.8.4.3  2012/02/08 14:29:11  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fixing  related so dropdown more than 1000 rec
 *
 * Revision 1.8.4.2  2012/02/07 12:34:50  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Task Management issue fixes
 *
 * Revision 1.11  2012/02/02 06:20:34  schepuri
 * CASE201112/CR201113/LOG201121
 * issue fix is merged from  1.8.4.1
 *
 * Revision 1.10  2012/01/31 08:28:44  spendyala
 * CASE201112/CR201113/LOG201121
 * Added waveno,packcode,expectedqty and containerlp to the sublist.
 *
 * Revision 1.9  2012/01/30 11:46:33  spendyala
 * CASE201112/CR201113/LOG201121
 * In the So# dropdown previously we are showing  internal id, Need to show sales order#. Also show SO# in the Ref# field of the sub list, previously  it is blank and Added wave# to the search criteria.
 *
 * Revision 1.8  2011/10/21 09:08:08  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Code changes to show only respective tasks for the respective screens
 * i.e Inbound Task Management screen should display only inbound task related data when querying the report without selecting task type.
 *
 * Revision 1.7  2011/10/17 14:26:05  schepuri
 * CASE201112/CR201113/LOG201121
 * added from date & to date in QB
 *
 * Revision 1.6  2011/10/17 14:20:18  schepuri
 * CASE201112/CR201113/LOG201121
 * added from date & to date in QB
 *
 * Revision 1.5  2011/09/22 11:53:52  vrgurujala
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.4  2011/09/08 21:34:11  snimmakayala
 * CASE201112/CR201113/LOG201121
 * Outbound Storage Fix
 *
 * Revision 1.3  2011/07/20 12:51:51  pattili
 * CASE201112/CR201113/LOG201121
 * Added the CVS tag which was missing in this file
 *
 * Revision 1.4  2011/07/20 11:30:04  schepuri
 * CASE201112/CR201113/LOG201121
 * Quantity Exception in RF
 *
 * Revision 1.3  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.2  2011/04/13 07:18:47  pattili
 * CASE201112/CR201113/LOG201121
 * Added CVS Header in all the screens.
 *
 *
 *****************************************************************************/
function ebiznet_TaskManagement_oub(request, response){

	if (request.getMethod() == 'GET') {
		//create the Suitelet form
		var form = nlapiCreateForm('Outbound Task Management');

		//add the SO field and make it a hyperlink

		var tasktype = form.addField('custpage_tasktype', 'multiselect', 'Task Type');

		//new nlobjSearchFilter('mainline', null, 'is', 'T')
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_ebiz_process_direction', null, 'is', [2]);
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('name');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_task_desc');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_tasktype', null, filters, columns);
		for (var i = 0; searchresults != null && i < Math.min(500, searchresults.length); i++) {

			tasktype.addSelectOption(searchresults[i].getId(), searchresults[i].getValue('name') + ' - ' + searchresults[i].getValue('custrecord_ebiz_task_desc'));
		}
		//tasktype.setLayoutType('startrow');

		var taskstatus = form.addField('custpage_taskstatus', 'select', 'Task Status');
		taskstatus.addSelectOption('ALL', 'ALL');
		taskstatus.addSelectOption('C', 'COMPLETE');
		taskstatus.addSelectOption('I', 'INCOMPLETE');
		taskstatus.addSelectOption('S', 'SUSPEND');
		//taskstatus.setLayoutType('endrow');

		var priority = form.addField('custpage_priority', 'select', 'Priority');
		priority.addSelectOption('', '');
		priority.addSelectOption('HIGH', 'HIGH');
		priority.addSelectOption('LOW', 'LOW');
		//priority.setLayoutType('startrow','startrow');  





		//Get distinct so's and its IDs into dropdown  
		var orderso = form.addField('custpage_orderso', 'select', 'SO/TO/VRA');
		/*orderso.addSelectOption('', '');
		var filtersso = new Array();
		filtersso[0] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'isnot', '');
		filtersso[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3, 4]);
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso, new nlobjSearchColumn('custrecord_ebiz_order_no'));
		var ordersAlreadyAddedList = new Array();
		for (var i = 0; i < Math.min(500, searchresults.length); i++) {
//			var res = form.getField('custpage_orderso').getSelectOptions(searchresults[i].getValue('custrecord_ebiz_order_no'), 'is');
//			if (res != null) {
//			nlapiLogExecution('DEBUG', 'res.length', res.length + searchresults[i].getValue('custrecord_ebiz_order_no'));
//			if (res.length > 0) {
//			continue;
//			}
//			}
			if(!isValueAlreadyAdded(ordersAlreadyAddedList, searchresults[i].getValue('custrecord_ebiz_order_no'))){
				orderso.addSelectOption(searchresults[i].getValue('custrecord_ebiz_order_no'), searchresults[i].getText('custrecord_ebiz_order_no'));
				ordersAlreadyAddedList.push(searchresults[i].getValue('custrecord_ebiz_order_no'));
			}
		}*/

		fillsalesorderField(form, orderso,-1);

		var sku = form.addField('custpage_sku', 'select', 'ITEM', 'item');
		//sku.setLayoutType('endrow');
		//priority.addSelectOption('H','HIGH');  

		var lotbatch = form.addField('custpage_lotbatch', 'select', 'LOT/Batch#','customrecord_ebiznet_batch_entry');
		/*lotbatch.addSelectOption('', '');
		var filterslot = new Array();
		filterslot[0] = new nlobjSearchFilter('custrecord_batch_no', null, 'isnot', '');
		filterslot[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3, 4]);
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterslot, new nlobjSearchColumn('custrecord_batch_no'));
		for (var i = 0; searchresults != null && i < Math.min(500, searchresults.length); i++) {
			 var res = form.getField('custpage_lotbatch').getSelectOptions(searchresults[i].getValue('custrecord_batch_no'), 'is');
			if (res != null) {
				// nlapiLogExecution('DEBUG', 'res.length', res.length + searchresults[i].getValue('custpage_lotbatch'));
				if (res.length > 0) {
					continue;
				}
			}
			lotbatch.addSelectOption(searchresults[i].getValue('custrecord_batch_no'), searchresults[i].getValue('custrecord_batch_no'));
		}*/
		//lotbatch.setLayoutType('startrow','startrow');

		var beginlocation = form.addField('custpage_beginlocation', 'select', 'Begin Location', 'customrecord_ebiznet_location');
		// priority.addSelectOption('H','HIGH');  
		//beginlocation.setLayoutType('endrow');


		var endlocation = form.addField('custpage_endlocation', 'select', 'End Location', 'customrecord_ebiznet_location');
		//  priority.addSelectOption('H','HIGH');  
		// endlocation.setLayoutType('startrow','startrow');

		var lp = form.addField('custpage_lp', 'select', 'LP','customrecord_ebiznet_master_lp');
		/*lp.addSelectOption('', '');
		var filterslp = new Array();
		filterslp[0] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'isnot', '');
		filterslp[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3, 4]);
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterslp, new nlobjSearchColumn('custrecord_lpno'));
		for (var i = 0; searchresults!=null && i < searchresults.length; i++) {
			var res = form.getField('custpage_lp').getSelectOptions(searchresults[i].getValue('custrecord_lpno'), 'is');
			if (res != null) {
				//nlapiLogExecution('DEBUG', 'res.length', res.length + searchresults[i].getValue('custrecord_lpno'));
				if (res.length > 0) {
					continue;
				}
			}
			lp.addSelectOption(searchresults[i].getValue('custrecord_lpno'), searchresults[i].getValue('custrecord_lpno'));
		}*/

		var employee = form.addField('custpage_employee', 'select', 'Employee', 'employee');

		var ClusterNoField = form.addField('custpage_custerno', 'select', 'Cluster #');
		ClusterNoField.setLayoutType('startrow', 'none');
		ClusterNoField.addSelectOption("", "");
		var filtersob = new Array();
		filtersob.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'isnotempty'));
		
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_clus_no');
		
		var searchcluster = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersob, columns);
		
		//var searchcluster = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, null, new nlobjSearchColumn('custrecord_ebiz_clus_no'));
		if(searchcluster!=null)
		{
			for (var i = 0; i < searchcluster.length; i++) {
				if(searchcluster[i].getValue('custrecord_ebiz_clus_no')!=null && searchcluster[i].getValue('custrecord_ebiz_clus_no')!="")
				{
					var res=  form.getField('custpage_custerno').getSelectOptions(searchcluster[i].getValue('custrecord_ebiz_clus_no'), 'is');
					if (res != null) {				
						if (res.length > 0) {
							continue;
						}
					}

					ClusterNoField.addSelectOption(searchcluster[i].getValue('custrecord_ebiz_clus_no'), searchcluster[i].getValue('custrecord_ebiz_clus_no'));
				}
			}
		}

		//code added on 30/01/12 by suman
		//Added new search criteria i.e., Wave # to the form.
		var WaveField = form.addField('custpage_wave', 'select', 'Wave #');
		WaveField.addSelectOption("", "");
		addWaveNo(form,WaveField,-1);

		//end of code added.

		var fromdate = form.addField('custpage_fromdate', 'date', 'From Date').setDefaultValue(DateStamp());
		var todate = form.addField('custpage_todate', 'date', 'To Date').setDefaultValue(DateStamp());

		form.addSubmitButton('Display');

		response.writePage(form);
	}
	else {
		var form = nlapiCreateForm("Outbound Task Management");
		//form.addPageLink('crosslink', 'Go Back', 'https://system.netsuite.com/app/site/hosting/scriptlet.nl?script=138&deploy=1');
		var linkURL = nlapiResolveURL('SUITELET', 'customscript_task_management_oub', 'customdeploy_task_management_oub_di');
        form.addPageLink('crosslink', 'Go Back',linkURL);

		//add the SO field and make it a hyperlink

		var tasktype = form.addField('custpage_tasktype', 'multiselect', 'Task Type');

		//new nlobjSearchFilter('mainline', null, 'is', 'T')
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_ebiz_process_direction', null, 'is', [2]);
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('name');
		columns[1] = new nlobjSearchColumn('custrecord_ebiz_task_desc');
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_tasktype', null, filters, columns);
		for (var i = 0; i < Math.min(500, searchresults.length); i++) {

			tasktype.addSelectOption(searchresults[i].getId(), searchresults[i].getValue('name') + ' - ' + searchresults[i].getValue('custrecord_ebiz_task_desc'));
		}
		tasktype.setDefaultValue(request.getParameter('custpage_tasktype'));
		//tasktype.setLayoutType('startrow');

		var taskstatus = form.addField('custpage_taskstatus', 'select', 'Task Status');
		taskstatus.addSelectOption('ALL', 'ALL');
		taskstatus.addSelectOption('C', 'COMPLETE');
		taskstatus.addSelectOption('I', 'INCOMPLETE');
		taskstatus.addSelectOption('S', 'SUSPEND');
		taskstatus.setDefaultValue(request.getParameter('custpage_taskstatus'));
		//taskstatus.setLayoutType('endrow');

		var priority = form.addField('custpage_priority', 'select', 'Priority');
		priority.addSelectOption('', '');
		priority.addSelectOption('HIGH', 'HIGH');
		priority.addSelectOption('LOW', 'LOW');
		priority.setDefaultValue(request.getParameter('custpage_priority'));
		//priority.setLayoutType('startrow','startrow');       




		//Get distinct so's and its IDs into dropdown  
		var orderso = form.addField('custpage_orderso', 'select', 'SO/TO/VRA');
		/*orderso.addSelectOption('', '');
		var filtersso = new Array();
		filtersso[0] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'isnot', '');
		filtersso[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3, 4]);
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersso, new nlobjSearchColumn('custrecord_ebiz_order_no'));
		var ordersAlreadyAddedList = new Array();
		for (var i = 0; i < Math.min(500, searchresults.length); i++) {
//			var res = form.getField('custpage_orderso').getSelectOptions(searchresults[i].getValue('custrecord_ebiz_order_no'), 'is');
//			if (res != null) {
//			nlapiLogExecution('DEBUG', 'res.length', res.length + searchresults[i].getValue('custrecord_ebiz_order_no'));
//			if (res.length > 0) {
//			continue;
//			}
//			}
			if(!isValueAlreadyAdded(ordersAlreadyAddedList, searchresults[i].getValue('custrecord_ebiz_order_no'))){
				orderso.addSelectOption(searchresults[i].getValue('custrecord_ebiz_order_no'), searchresults[i].getText('custrecord_ebiz_order_no'));
				ordersAlreadyAddedList.push(searchresults[i].getValue('custrecord_ebiz_order_no'));
			}
		}
		orderso.setDefaultValue(request.getParameter('custpage_orderso'));*/
		fillsalesorderField(form, orderso,-1);

		var sku = form.addField('custpage_sku', 'select', 'ITEM', 'item');
		sku.setDefaultValue(request.getParameter('custpage_sku'));
		//sku.setLayoutType('endrow');
		//priority.addSelectOption('H','HIGH');  

		var lotbatch = form.addField('custpage_lotbatch', 'select', 'LOT/Batch#','customrecord_ebiznet_batch_entry');
		/*lotbatch.addSelectOption('', '');
		var filterslot = new Array();
		filterslot[0] = new nlobjSearchFilter('custrecord_batch_no', null, 'isnot', '');
		filterslot[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3, 4]);
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterslot, new nlobjSearchColumn('custrecord_batch_no'));
		for (var i = 0; searchresults != null && i < Math.min(500, searchresults.length); i++) {
			var res = form.getField('custpage_lotbatch').getSelectOptions(searchresults[i].getValue('custrecord_batch_no'), 'is');
			if (res != null) {
				// nlapiLogExecution('DEBUG', 'res.length', res.length + searchresults[i].getValue('custpage_lotbatch'));
				if (res.length > 0) {
					continue;
				}
			}
			lotbatch.addSelectOption(searchresults[i].getValue('custrecord_batch_no'), searchresults[i].getValue('custrecord_batch_no'));
		}
		lotbatch.setDefaultValue(request.getParameter('custpage_lotbatch'));*/
		//lotbatch.setLayoutType('startrow','startrow');

		var beginlocation = form.addField('custpage_beginlocation', 'select', 'Begin Location', 'customrecord_ebiznet_location');
		beginlocation.setDefaultValue(request.getParameter('custpage_beginlocation'));
		// priority.addSelectOption('H','HIGH');  
		//beginlocation.setLayoutType('endrow');


		var endlocation = form.addField('custpage_endlocation', 'select', 'End Location', 'customrecord_ebiznet_location');
		endlocation.setDefaultValue(request.getParameter('custpage_endlocation'));
		//  priority.addSelectOption('H','HIGH');  
		// endlocation.setLayoutType('startrow','startrow');

		var lp = form.addField('custpage_lp', 'select', 'LP','customrecord_ebiznet_master_lp');
		/*lp.addSelectOption('', '');
		var filterslp = new Array();
		filterslp[0] = new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'isnot', '');
		filterslp[1] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3, 4]);
		var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterslp, new nlobjSearchColumn('custrecord_lpno'));
		for (var i = 0; i < Math.min(500, searchresults.length); i++) {
			var res = form.getField('custpage_lp').getSelectOptions(searchresults[i].getValue('custrecord_lpno'), 'is');
			if (res != null) {
				nlapiLogExecution('DEBUG', 'res.length', res.length + searchresults[i].getValue('custrecord_lpno'));
				if (res.length > 0) {
					continue;
				}
			}
			lp.addSelectOption(searchresults[i].getValue('custrecord_lpno'), searchresults[i].getValue('custrecord_lpno'));
		}
		lp.setDefaultValue(request.getParameter('custpage_lp'));*/

		var employee = form.addField('custpage_employee', 'select', 'Employee', 'employee');
		employee.setDefaultValue(request.getParameter('custpage_employee'));

		var ClusterNoField = form.addField('custpage_custerno', 'select', 'Cluster #');
		ClusterNoField.setLayoutType('startrow', 'none');
		ClusterNoField.addSelectOption("", "");

		var filtersob = new Array();
		filtersob.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'isnotempty'));
		
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('custrecord_ebiz_clus_no');
		
		var searchcluster = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filtersob, columns);
		//var searchcluster = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, null, new nlobjSearchColumn('custrecord_ebiz_clus_no'));
		if(searchcluster!=null)
		{
			for (var i = 0; i < searchcluster.length; i++) {
				if(searchcluster[i].getValue('custrecord_ebiz_clus_no')!=null && searchcluster[i].getValue('custrecord_ebiz_clus_no')!="")
				{
					var res=  form.getField('custpage_custerno').getSelectOptions(searchcluster[i].getValue('custrecord_ebiz_clus_no'), 'is');
					if (res != null) {				
						if (res.length > 0) {
							continue;
						}
					}

					ClusterNoField.addSelectOption(searchcluster[i].getValue('custrecord_ebiz_clus_no'), searchcluster[i].getValue('custrecord_ebiz_clus_no'));
				}
			}
		}


		//code added on 30/01/12 by suman
		//Added new search criteria i.e., Wave # to the form.
		var WaveField = form.addField('custpage_wave', 'select', 'Wave #');
		WaveField.addSelectOption("", "");
		var wave=addWaveNo(form,WaveField,-1);
		var vStatusId;
		//end of code added.


		var fromdate = form.addField('custpage_fromdate', 'date', 'From Date');
		var todate = form.addField('custpage_todate', 'date', 'To Date');

		tasktype.setDefaultValue(request.getParameter('custpage_tasktype'));        
		taskstatus.setDefaultValue(request.getParameter('custpage_taskstatus'));        
		priority.setDefaultValue(request.getParameter('custpage_priority'));        
		nlapiLogExecution('ERROR', 'selected sales order', request.getParameter('custpage_orderso')); 
		orderso.setDefaultValue(request.getParameter('custpage_orderso'));        
		sku.setDefaultValue(request.getParameter('custpage_sku'));        
		lotbatch.setDefaultValue(request.getParameter('custpage_lotbatch'));       
		beginlocation.setDefaultValue(request.getParameter('custpage_beginlocation'));        
		endlocation.setDefaultValue(request.getParameter('custpage_endlocation'));       
		lp.setDefaultValue(request.getParameter('custpage_lp'));       
		employee.setDefaultValue(request.getParameter('custpage_employee'));
		fromdate.setDefaultValue(request.getParameter('custpage_fromdate'));
		todate.setDefaultValue(request.getParameter('custpage_todate'));
		//wave.setDefaultValue(request.getParameter('custpage_wave'));
		form.addSubmitButton('Display');

		//create a sublist to display values.
		var sublist = form.addSubList("custpage_results", "list", "Result List");
		sublist.addField("custpage_slno", "text", "Sl.No");
		sublist.addField("custpage_taskno", "text", "Ref #");
		sublist.addField("custpage_waveno","text","Wave #");
		sublist.addField("custpage_tasktype", "text", "Task Type");
		sublist.addField("custpage_taskpriority", "text", "Task Priority").setDisplayType('hidden');
		sublist.addField("custpage_item", "select", "Item", "item").setDisplayType('inline');
		sublist.addField("custpage_packcode","text","Pack Code");
		sublist.addField("custpage_lotbat", "text", "Lot/Batch #");
		sublist.addField("custpage_lp", "text", "LP");
		sublist.addField("custpage_actqty","text","Actual Qty");
		sublist.addField("custpage_expectedqty","text","Qty");
		sublist.addField("custpage_begintime", "text", "Begin Time");
		sublist.addField("custpage_endtime", "text", "End Time");
		sublist.addField("custpage_beginlocation", "select", "Begin Location", "customrecord_ebiznet_location").setDisplayType('inline');
		sublist.addField("custpage_endlocation", "select", "End Location", "customrecord_ebiznet_location").setDisplayType('inline');
		sublist.addField("custpage_status", "text", "Status");
		sublist.addField("custpage_containerlp","text","Container LP");
		sublist.addField("custpage_putzone", "text", "Put Zone").setDisplayType('hidden');
		sublist.addField("custpage_clusternumber", "text", "Cluster #");
		sublist.addField("custpage_user", "text", "User");
		// sublist.addField("custpage_refno", "text", "Ref #");

		// define search filters
		var value = request.getParameter('custpage_tasktype');
		nlapiLogExecution('ERROR', 'Value:', value);
		var taskarray = new Array();
		taskarray = value.split('');
		for (var p = 0; p < taskarray.length; p++) {
			nlapiLogExecution('ERROR', 'TaskArray:', taskarray[p]);
		}

		var opentaskserchresultcount=0;
		//nlapiLogExecution('ERROR', 'request.getParameter(custpage_fromdate)', request.getParameter('custpage_fromdate'));
		try {
			var i = 0;
			var filters = new Array();
			var param = request.getParameter('custpage_sku');
			nlapiLogExecution('ERROR', 'Length:', taskarray.length);
			if (taskarray.length != 0 && value != '') {
				filters[i] = new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', taskarray);
				nlapiLogExecution('ERROR', 'filters[i]', taskarray);
				i++;
			}
			if (request.getParameter('custpage_taskstatus') != "") {

				nlapiLogExecution('ERROR', 'Inside Task Status', request.getParameter('custpage_taskstatus'));
				// filters[i] = new nlobjSearchFilter('custrecordact_begin_date', null, 'on', request.getParameter('custpage_fromdate'));
				//i++;
				vStatusId=request.getParameter('custpage_taskstatus');
			}
			if (request.getParameter('custpage_orderso') != "") {
				nlapiLogExecution('ERROR', 'Inside PO', request.getParameter('custpage_orderso'));
				filters[i] = new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'is', request.getParameter('custpage_orderso'));
				i++;
			}
			if (request.getParameter('custpage_sku') != "") {
				nlapiLogExecution('ERROR', 'Inside SKU Param', request.getParameter('custpage_sku'));
				filters[i] = new nlobjSearchFilter('custrecord_ebiz_sku_no', null, 'is', request.getParameter('custpage_sku'));
				i++;
			}

			if (request.getParameter('custpage_lotbatch') != "") {
				nlapiLogExecution('ERROR', 'Inside Lot/Batch', request.getParameter('custpage_lotbatch'));
// case no 20126489
				var filterslot = new Array();
				filterslot[0] = new nlobjSearchFilter('internalid', null, 'anyof', request.getParameter('custpage_lotbatch'));

				var searchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterslot, new nlobjSearchColumn('custrecord_ebizlotbatch'));
				var lotbatchtext = '';
				if(searchresults != null && searchresults != '')
				{
					lotbatchtext = searchresults[0].getValue('custrecord_ebizlotbatch');

				}
				
				
				if(lotbatchtext != null && lotbatchtext != '')
				{
					nlapiLogExecution('ERROR', 'Inside lotbatchtext', lotbatchtext);
					filters[i] = new nlobjSearchFilter('custrecord_batch_no', null, 'is', lotbatchtext);
					i++;
				}
			}

			if (request.getParameter('custpage_beginlocation') != "") {
				nlapiLogExecution('ERROR', 'Inside Begin Loc', request.getParameter('custpage_beginlocation'));
				filters[i] = new nlobjSearchFilter('custrecord_actbeginloc', null, 'is', request.getParameter('custpage_beginlocation'));
				i++;
			}
			if (request.getParameter('custpage_endlocation') != "") {
				nlapiLogExecution('ERROR', 'Inside End Loc', request.getParameter('custpage_endlocation'));
				filters[i] = new nlobjSearchFilter('custrecord_actendloc', null, 'is', request.getParameter('custpage_endlocation'));
				i++;
			}
			if (request.getParameter('custpage_lp') != "") {
				nlapiLogExecution('ERROR', 'Inside LP', request.getParameter('custpage_lp'));
				//case# 20126069. Added the below filter to get the lp from master lp based on id.
				var lpfilters = new Array();
				lpfilters[0] = new nlobjSearchFilter('internalid', null, 'is', request.getParameter('custpage_lp'));
				var lpcolumns = new Array();
				lpcolumns[0] = new nlobjSearchColumn('custrecord_ebiz_lpmaster_lp');
				var lpsearchresults = nlapiSearchRecord('customrecord_ebiznet_master_lp', null, lpfilters, lpcolumns);
				
				var lp=lpsearchresults[0].getValue('custrecord_ebiz_lpmaster_lp');
				//End
				
				filters[i] = new nlobjSearchFilter('custrecord_lpno', null, 'is', lp);
				i++;
			}

			if(request.getParameter('custpage_custerno') !="")
			{
				filters[i] = new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', request.getParameter('custpage_custerno'));
				i = i + 1;
			}

			if ((request.getParameter('custpage_fromdate') != "" && request.getParameter('custpage_fromdate') != null) && (request.getParameter('custpage_todate') != "" && request.getParameter('custpage_todate') != null) ) {
				nlapiLogExecution('ERROR', 'fromdate', request.getParameter('custpage_fromdate'));
				nlapiLogExecution('ERROR', 'todate', request.getParameter('custpage_todate'));

				filters[i] = new nlobjSearchFilter('custrecordact_begin_date', null, 'within', request.getParameter('custpage_fromdate'), request.getParameter('custpage_todate') );
				i++;
			}  

			filters.push(new nlobjSearchFilter('custrecord_ebiz_process_direction', 'custrecord_tasktype', 'is', [2]));

			//added on 30/01/12 by suman
			//filter criteria for wave #
			if(request.getParameter('custpage_wave') !="")
			{
				filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', request.getParameter('custpage_wave')));
			}
			//end of filter criteria for wave#

			
             if (request.getParameter('custpage_employee')!= "")
             {
             nlapiLogExecution('ERROR', 'Inside Employee', request.getParameter('custpage_employee'));
             filters[i] = new nlobjSearchFilter('custrecord_taskassignedto', null, 'is', request.getParameter('custpage_employee'));
             i++;
             }
			 
			// return opportunity sales rep, customer custom field, and customer ID
			var columns = new Array();
			columns[0] = new nlobjSearchColumn('custrecord_tasktype');
			columns[1] = new nlobjSearchColumn('custrecord_ebiz_lpno');
			columns[2] = new nlobjSearchColumn('custrecord_actbeginloc');
			columns[3] = new nlobjSearchColumn('custrecord_actendloc');
			columns[4] = new nlobjSearchColumn('custrecordact_begin_date');
			columns[5] = new nlobjSearchColumn('custrecord_act_end_date');
			columns[6] = new nlobjSearchColumn('custrecord_actualbegintime');
			columns[7] = new nlobjSearchColumn('custrecord_actualendtime');
			columns[8] = new nlobjSearchColumn('custrecord_ebiz_sku_no');
			columns[9] = new nlobjSearchColumn('custrecord_ebiz_task_no');
			columns[10] = new nlobjSearchColumn('custrecord_batch_no');
			columns[11] = new nlobjSearchColumn('custrecord_taskassignedto');
			columns[12] = new nlobjSearchColumn('custrecord_ebiz_clus_no');
			columns[13] = new nlobjSearchColumn('custrecord_ebiz_order_no');
			columns[14] = new nlobjSearchColumn('internalid').setSort(true);
			columns[15] = new nlobjSearchColumn('custrecord_ebiz_wave_no');
			columns[16] = new nlobjSearchColumn('custrecord_container_lp_no');
			columns[17] = new nlobjSearchColumn('custrecord_packcode');
			columns[18] = new nlobjSearchColumn('custrecord_expe_qty');
			columns[19] = new nlobjSearchColumn('custrecord_wms_status_flag');
			columns[20] = new nlobjSearchColumn('custrecord_act_qty');
			
			

			// execute the  search, passing null filter and return columns
			var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, columns);
			if(searchresults !=null)
			{
				nlapiLogExecution('ERROR', 'open task searchresults', searchresults.length);
				opentaskserchresultcount=searchresults.length;
			}
		} 
		catch (error) {
			nlapiLogExecution('ERROR', 'Into Catch Block', error);
		}
		var tasktype, lp, beginloc, endloc, begindte, enddate, begintime, endtime, itemname, dstatus, taskno, lotbat,user,vclusterno;
		var waveno,containerlp,packcode,expectedqty,actqty;
		var vlinecount=1;
		var vWMSFlag='';
		/*Searching records from custom record and dynamically adding to the sublist lines */
		for (var i = 0; searchresults != null && i < searchresults.length; i++) {
			var searchresult = searchresults[i];
			tasktype = searchresult.getText('custrecord_tasktype');
			lp = searchresult.getValue('custrecord_ebiz_lpno');
			beginloc = searchresult.getValue('custrecord_actbeginloc');
			endloc = searchresult.getValue('custrecord_actendloc');
			begindte = searchresult.getValue('custrecordact_begin_date');
			enddate = searchresult.getValue('custrecord_act_end_date');
			begintime = searchresult.getValue('custrecord_actualbegintime');
			endtime = searchresult.getValue('custrecord_actualendtime');
			itemname = searchresult.getValue('custrecord_ebiz_sku_no');
//			taskno = searchresult.getValue('custrecord_ebiz_task_no');
			taskno = searchresult.getText('custrecord_ebiz_order_no');
			lotbat = searchresult.getValue('custrecord_batch_no');
			user = searchresult.getText('custrecord_taskassignedto');
			vclusterno = searchresult.getValue('custrecord_ebiz_clus_no');
			waveno=searchresult.getValue('custrecord_ebiz_wave_no');
			containerlp=searchresult.getValue('custrecord_container_lp_no');
			packcode=searchresult.getValue('custrecord_packcode');
			expectedqty=searchresult.getValue('custrecord_expe_qty');
			actqty=searchresult.getValue('custrecord_act_qty');
			vWMSFlag=searchresult.getValue('custrecord_wms_status_flag');


			if (enddate != null && enddate != '') 
				dstatus = 'COMPLETED';
			else 
				dstatus = 'PENDING';
			
			if( vWMSFlag ==26)
				dstatus = 'FAILED';
			
			if((vStatusId == 'I' && dstatus == 'PENDING') || (vStatusId == 'C' && dstatus == 'COMPLETED')  || vStatusId == 'ALL')
			{	
				nlapiLogExecution('ERROR','INTO IF',vlinecount);

				form.getSubList('custpage_results').setLineItemValue('custpage_slno', vlinecount, vlinecount.toString());
				form.getSubList('custpage_results').setLineItemValue('custpage_taskno', vlinecount, taskno);
				form.getSubList('custpage_results').setLineItemValue('custpage_tasktype', vlinecount, tasktype);
				form.getSubList('custpage_results').setLineItemValue('custpage_taskpriority', vlinecount, 'HIGH');
				form.getSubList('custpage_results').setLineItemValue('custpage_status', vlinecount, dstatus);
				form.getSubList('custpage_results').setLineItemValue('custpage_begintime', vlinecount, (begindte + " " + begintime));
				form.getSubList('custpage_results').setLineItemValue('custpage_endtime', vlinecount, (enddate + " " + endtime));
				form.getSubList('custpage_results').setLineItemValue('custpage_beginlocation', vlinecount, beginloc);
				form.getSubList('custpage_results').setLineItemValue('custpage_endlocation', vlinecount, endloc);
				form.getSubList('custpage_results').setLineItemValue('custpage_putzone', vlinecount, endloc);
				//form.getSubList('custpage_results').setLineItemValue('custpage_refno', i + 1, '888');
				form.getSubList('custpage_results').setLineItemValue('custpage_item', vlinecount, itemname);
				form.getSubList('custpage_results').setLineItemValue('custpage_lp', vlinecount, lp);
				form.getSubList('custpage_results').setLineItemValue('custpage_user', vlinecount, user);
				form.getSubList('custpage_results').setLineItemValue('custpage_lotbat', vlinecount, lotbat);
				form.getSubList('custpage_results').setLineItemValue('custpage_clusternumber', vlinecount, vclusterno);
				form.getSubList('custpage_results').setLineItemValue('custpage_waveno', vlinecount, waveno);
				form.getSubList('custpage_results').setLineItemValue('custpage_containerlp', vlinecount, containerlp);
				form.getSubList('custpage_results').setLineItemValue('custpage_packcode', vlinecount, packcode);
				form.getSubList('custpage_results').setLineItemValue('custpage_expectedqty', vlinecount, expectedqty);
				form.getSubList('custpage_results').setLineItemValue('custpage_actqty', vlinecount, actqty);
				
				vlinecount++;
			}

		}

		//closed tas details added
		if(vStatusId == 'C' || vStatusId == 'ALL')
		{
			try {
				var i = 0;
				var filters = new Array();
				var param = request.getParameter('custpage_sku');
				nlapiLogExecution('ERROR', 'Length:', taskarray.length);
				if (taskarray.length != 0 && value != '') {
					filters[i] = new nlobjSearchFilter('custrecord_ebiztask_tasktype', null, 'anyof', taskarray);
					nlapiLogExecution('ERROR', 'filters[i]', taskarray);
					i++;
				}
				if (request.getParameter('custpage_taskstatus') != "") {

					nlapiLogExecution('ERROR', 'Inside Task Status', request.getParameter('custpage_taskstatus'));
					// filters[i] = new nlobjSearchFilter('custrecordact_begin_date', null, 'on', request.getParameter('custpage_fromdate'));
					//i++;
				}
				if (request.getParameter('custpage_orderso') != "") {
					nlapiLogExecution('ERROR', 'Inside PO', request.getParameter('custpage_orderso'));
					filters[i] = new nlobjSearchFilter('custrecord_ebiztask_ebiz_order_no', null, 'is', request.getParameter('custpage_orderso'));
					i++;
				}
				if (request.getParameter('custpage_sku') != "") {
					nlapiLogExecution('ERROR', 'Inside SKU Param', request.getParameter('custpage_sku'));
					filters[i] = new nlobjSearchFilter('custrecord_ebiztask_ebiz_sku_no', null, 'is', request.getParameter('custpage_sku'));
					i++;
				}

				if (request.getParameter('custpage_lotbatch') != "") {
					nlapiLogExecution('ERROR', 'Inside Lot/Batch closedtask', request.getParameter('custpage_lotbatch'));
// case no 20126489
					var filterslot = new Array();
					filterslot[0] = new nlobjSearchFilter('internalid', null, 'anyof', request.getParameter('custpage_lotbatch'));

					var searchresults = nlapiSearchRecord('customrecord_ebiznet_batch_entry', null, filterslot, new nlobjSearchColumn('custrecord_ebizlotbatch'));
					var lotbatchtext = '';
					if(searchresults != null && searchresults != '')
					{
						lotbatchtext = searchresults[0].getValue('custrecord_ebizlotbatch');

					}
					
					
					if(lotbatchtext != null && lotbatchtext != '')
					{
						nlapiLogExecution('ERROR', 'Inside lotbatchtext', lotbatchtext);
						filters[i] = new nlobjSearchFilter('custrecord_ebiztask_batch_no', null, 'is', lotbatchtext);
						i++;
					}
				}

				if (request.getParameter('custpage_beginlocation') != "") {
					nlapiLogExecution('ERROR', 'Inside Begin Loc', request.getParameter('custpage_beginlocation'));
					filters[i] = new nlobjSearchFilter('custrecord_ebiztask_actbeginloc', null, 'is', request.getParameter('custpage_beginlocation'));
					i++;
				}
				if (request.getParameter('custpage_endlocation') != "") {
					nlapiLogExecution('ERROR', 'Inside End Loc', request.getParameter('custpage_endlocation'));
					filters[i] = new nlobjSearchFilter('custrecord_ebiztask_actendloc', null, 'is', request.getParameter('custpage_endlocation'));
					i++;
				}
				if (request.getParameter('custpage_lp') != "") {
					nlapiLogExecution('ERROR', 'Inside LP', request.getParameter('custpage_lp'));
					filters[i] = new nlobjSearchFilter('custrecord_ebiztask_lpno', null, 'is', request.getParameter('custpage_lp'));
					i++;
				}

				if(request.getParameter('custpage_custerno') !="")
				{
					filters[i] = new nlobjSearchFilter('custrecord_ebiztask_cluster_no', null, 'is', request.getParameter('custpage_custerno'));
					i = i + 1;
				}

				if ((request.getParameter('custpage_fromdate') != "" && request.getParameter('custpage_fromdate') != null) && (request.getParameter('custpage_todate') != "" && request.getParameter('custpage_todate') != null) ) {
					nlapiLogExecution('ERROR', 'fromdate', request.getParameter('custpage_fromdate'));
					nlapiLogExecution('ERROR', 'todate', request.getParameter('custpage_todate'));

					filters[i] = new nlobjSearchFilter('custrecord_ebiztask_act_begin_date', null, 'within', request.getParameter('custpage_fromdate'), request.getParameter('custpage_todate') );
					i++;
				}  

				//added on 30/01/12 by suman
				//filter criteria for wave #
				if(request.getParameter('custpage_wave') !="")
				{
					filters.push(new nlobjSearchFilter('custrecord_ebiztask_ebiz_wave_no', null, 'is', request.getParameter('custpage_wave')));
				}
				//end of filter criteria for wave#
				 if (request.getParameter('custpage_employee')!= "")
	             {
	             nlapiLogExecution('ERROR', 'Inside Employee', request.getParameter('custpage_employee'));
	             filters[i] = new nlobjSearchFilter('custrecord_ebiztask_taskassgndid', null, 'is', request.getParameter('custpage_employee'));
	             i++;
	             }

				filters.push(new nlobjSearchFilter('custrecord_ebiz_process_direction', 'custrecord_ebiztask_tasktype', 'is', [2]));	

				var columns = new Array();
				columns[0] = new nlobjSearchColumn('custrecord_ebiztask_tasktype');
				columns[1] = new nlobjSearchColumn('custrecord_ebiztask_lpno');
				columns[2] = new nlobjSearchColumn('custrecord_ebiztask_actbeginloc');
				columns[3] = new nlobjSearchColumn('custrecord_ebiztask_actendloc');
				columns[4] = new nlobjSearchColumn('custrecord_ebiztask_act_begin_date');
				columns[5] = new nlobjSearchColumn('custrecord_ebiztask_act_end_date');
				columns[6] = new nlobjSearchColumn('custrecord_ebiztask_actualbegintime');
				columns[7] = new nlobjSearchColumn('custrecord_ebiztask_actualendtime');
				columns[8] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_sku_no');
				columns[9] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_task_no');
				columns[10] = new nlobjSearchColumn('custrecord_ebiztask_batch_no');
				columns[11] = new nlobjSearchColumn('custrecord_ebiztask_taskassgndid');
				columns[12] = new nlobjSearchColumn('custrecord_ebiztask_cluster_no');
				columns[13] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_order_no');
				columns[14] = new nlobjSearchColumn('internalid').setSort(true);
				columns[15] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_wave_no');
				columns[16] = new nlobjSearchColumn('custrecord_ebiztask_ebiz_contlp_no');
				columns[17] = new nlobjSearchColumn('custrecord_ebiztask_packcode');
				columns[18] = new nlobjSearchColumn('custrecord_ebiztask_expe_qty');
				columns[19] = new nlobjSearchColumn('custrecord_ebiztask_act_qty');
				

				var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_ebiztask', null, filters, columns);
				if(searchresults !=null)
					nlapiLogExecution('ERROR', 'closed task searchresults', searchresults.length);
			} 
			catch (error) {
				nlapiLogExecution('ERROR', 'Into Catch Block', error);
			}
			var intcnt=0;
			var tasktype, lp, beginloc, endloc, begindte, enddate, begintime, endtime, itemname, dstatus, taskno, lotbat,user,vclusterno;
			var waveno,containerlp,packcode,expectedqty,actqty;
			opentaskserchresultcount=vlinecount;
			/*Searching records from custom record and dynamically adding to the sublist lines */
			for (var i = 0; searchresults != null && i < searchresults.length; i++) 
			{
				nlapiLogExecution('ERROR','INTO IF',opentaskserchresultcount);
				intcnt=i;
				var searchresult = searchresults[i];

				tasktype = searchresult.getText('custrecord_ebiztask_tasktype');
				lp = searchresult.getValue('custrecord_ebiztask_lpno');
				beginloc = searchresult.getValue('custrecord_ebiztask_actbeginloc');
				endloc = searchresult.getValue('custrecord_ebiztask_actendloc');
				begindte = searchresult.getValue('custrecord_ebiztask_act_begin_date');
				enddate = searchresult.getValue('custrecord_ebiztask_act_end_date');
				begintime = searchresult.getValue('custrecord_ebiztask_actualbegintime');
				endtime = searchresult.getValue('custrecord_ebiztask_actualendtime');
				itemname = searchresult.getValue('custrecord_ebiztask_ebiz_sku_no');
//				taskno = searchresult.getValue('custrecord_ebiztask_ebiz_task_no');
				taskno = searchresult.getText('custrecord_ebiztask_ebiz_order_no');
				lotbat = searchresult.getValue('custrecord_ebiztask_batch_no');
				user = searchresult.getText('custrecord_ebiztask_taskassgndid');
				vclusterno = searchresult.getValue('custrecord_ebiztask_cluster_no');
				waveno = searchresult.getValue('custrecord_ebiztask_ebiz_wave_no');
				containerlp = searchresult.getText('custrecord_ebiztask_ebiz_contlp_no');
				packcode = searchresult.getValue('custrecord_ebiztask_packcode');
				expectedqty=searchresult.getValue('custrecord_ebiztask_expe_qty');
				actqty=searchresult.getValue('custrecord_ebiztask_act_qty');

				if (enddate != null && enddate != '') 
					dstatus = 'COMPLETED';
				else 
					dstatus = 'PENDING';

				//form.getSubList('custpage_results').setLineItemValue('custpage_slno', opentaskserchresultcount + 1, opentaskserchresultcount + 1);
				form.getSubList('custpage_results').setLineItemValue('custpage_slno', opentaskserchresultcount, opentaskserchresultcount.toString());
				form.getSubList('custpage_results').setLineItemValue('custpage_taskno', opentaskserchresultcount, taskno);
				form.getSubList('custpage_results').setLineItemValue('custpage_tasktype', opentaskserchresultcount, tasktype);
				form.getSubList('custpage_results').setLineItemValue('custpage_taskpriority', opentaskserchresultcount, 'HIGH');
				form.getSubList('custpage_results').setLineItemValue('custpage_status', opentaskserchresultcount, dstatus);
				form.getSubList('custpage_results').setLineItemValue('custpage_begintime', opentaskserchresultcount, (begindte + " " + begintime));
				form.getSubList('custpage_results').setLineItemValue('custpage_endtime', opentaskserchresultcount, (enddate + " " + endtime));
				form.getSubList('custpage_results').setLineItemValue('custpage_beginlocation', opentaskserchresultcount, beginloc);
				form.getSubList('custpage_results').setLineItemValue('custpage_endlocation', opentaskserchresultcount, endloc);
				form.getSubList('custpage_results').setLineItemValue('custpage_putzone', opentaskserchresultcount, endloc);
				//form.getSubList('custpage_results').setLineItemValue('custpage_refno', i + 1, '888');
				form.getSubList('custpage_results').setLineItemValue('custpage_item', opentaskserchresultcount, itemname);
				form.getSubList('custpage_results').setLineItemValue('custpage_lp', opentaskserchresultcount, lp);
				form.getSubList('custpage_results').setLineItemValue('custpage_user', opentaskserchresultcount, user);
				form.getSubList('custpage_results').setLineItemValue('custpage_lotbat', opentaskserchresultcount, lotbat);
				form.getSubList('custpage_results').setLineItemValue('custpage_clusternumber', opentaskserchresultcount, vclusterno);

				form.getSubList('custpage_results').setLineItemValue('custpage_waveno', opentaskserchresultcount, waveno);
				form.getSubList('custpage_results').setLineItemValue('custpage_containerlp', opentaskserchresultcount, containerlp);
				form.getSubList('custpage_results').setLineItemValue('custpage_packcode', opentaskserchresultcount, packcode);
				form.getSubList('custpage_results').setLineItemValue('custpage_expectedqty', opentaskserchresultcount, expectedqty);
				form.getSubList('custpage_results').setLineItemValue('custpage_actqty', opentaskserchresultcount, actqty);

				opentaskserchresultcount=opentaskserchresultcount+1;
			}
		}

		response.writePage(form);

	}
}

/**
 * @param form
 */
function addWaveNo(form,WaveField,maxno){

	var wavesAlreadyAddedList=new Array();
	var filters = new Array();
	var Columns = new Array();
	Columns[0] = new nlobjSearchColumn('custrecord_ebiz_wave_no');	 
	//Columns[0].setSort(true);
	Columns.push(new nlobjSearchColumn('id').setSort(true));

	filters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'isNotEmpty'));
	if(maxno!=-1)
	{
		filters.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno)));
	}

	var searchresults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filters, Columns);
	if (searchresults != null) {	
		for (var i = 0; i < Math.min(1000, searchresults.length); i++) {

			if(!isValueAlreadyAdded(wavesAlreadyAddedList, parseFloat(searchresults[i].getValue('custrecord_ebiz_wave_no')))){
				WaveField.addSelectOption(searchresults[i].getValue('custrecord_ebiz_wave_no'), searchresults[i].getValue('custrecord_ebiz_wave_no'));
				wavesAlreadyAddedList.push(parseFloat(searchresults[i].getValue('custrecord_ebiz_wave_no')));
			}
		}
	}

	if(searchresults!=null && searchresults.length>=1000)
	{
		var maxno=searchresults[searchresults.length-1].getValue('id');		
		addWaveNo(form, WaveField,maxno);	
	}

}

/**
 * @param alreadyAddedList
 * @param currentValue
 * @returns {Boolean}
 */
function isValueAlreadyAdded(alreadyAddedList, currentValue){
	var alreadyAdded = false;

	for(var j = 0; j < alreadyAddedList.length; j++){
		if(alreadyAddedList[j] == currentValue){
			alreadyAdded = true;
			break;
		}
	}

	return alreadyAdded;
}
function fillsalesorderField(form, salesorderField,maxno){
	var salesorderFilers = new Array();
	if(maxno!=-1)
	{
		//salesorderFilers.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'lessthan', parseFloat(maxno)));
		salesorderFilers.push(new nlobjSearchFilter('id', null, 'lessthan', parseFloat(maxno)));
	}
	salesorderFilers.push(new nlobjSearchFilter('custrecord_ebiz_cntrl_no', null, 'isnot', ''));
	salesorderFilers.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', [3, 4]));
	salesorderField.addSelectOption("", "");
	var columns=new Array();
	columns[0]=new nlobjSearchColumn('custrecord_ebiz_order_no');
	columns[1]=new nlobjSearchColumn('id').setSort(true);

	var customerSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, salesorderFilers,columns);
	var ordersAlreadyAddedList = new Array();
	for (var i = 0; customerSearchResults != null && i < customerSearchResults.length; i++) {
//		nlapiLogExecution('ERROR', 'customerSearchResults', customerSearchResults.length); 
		if(customerSearchResults[i].getValue('custrecord_ebiz_order_no') != null && customerSearchResults[i].getValue('custrecord_ebiz_order_no') != "" && customerSearchResults[i].getValue('custrecord_ebiz_order_no') != " ")
		{
//			nlapiLogExecution('ERROR', 'tranid1', i); 
			var resdo = form.getField('custpage_orderso').getSelectOptions(customerSearchResults[i].getValue('custrecord_ebiz_order_no'), 'is');
			if (resdo != null) {
				if (resdo.length > 0) {
					continue;
				}
			}
			/*if(!isValueAlreadyAdded(ordersAlreadyAddedList, customerSearchResults[i].getValue('tranid'))){
				nlapiLogExecution('ERROR', 'tranid1', i); 
				salesorderField.addSelectOption(customerSearchResults[i].getValue('tranid'), customerSearchResults[i].getText('tranid'));
				ordersAlreadyAddedList.push(customerSearchResults[i].getValue('tranid'));
			}*/
		}
		salesorderField.addSelectOption(customerSearchResults[i].getValue('custrecord_ebiz_order_no'), customerSearchResults[i].getText('custrecord_ebiz_order_no'));
//		nlapiLogExecution('ERROR', 'tranid2', customerSearchResults[i].getValue('custrecord_ebiz_order_no')); 
	}
	/*
	if(customerSearchResults!=null && customerSearchResults.length>=1000)
	{
		nlapiLogExecution('ERROR', 'morethan1000rec', ''); 
		var column=new Array();
		column[0]=new nlobjSearchColumn('custrecord_ebiz_order_no');		
		var OrderSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, salesorderFilers,column);
		var maxno=OrderSearchResults[OrderSearchResults.length-1].getValue(columns[1]);		
		fillsalesorderField(form, salesorderField,maxno);	
	}*/

	if(customerSearchResults!=null && customerSearchResults.length>=1000)
	{
		var maxno=customerSearchResults[customerSearchResults.length-1].getValue('id');		
		fillsalesorderField(form, salesorderField,maxno);	
	}


}
