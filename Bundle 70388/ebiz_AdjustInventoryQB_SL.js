/***************************************************************************
 eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/WMS_UI/Transactions/Inventory/Suitelet/ebiz_AdjustInventoryQB_SL.js,v $
 *     	   $Revision: 1.5.4.1.4.2.4.1 $
 *     	   $Date: 2013/03/15 09:28:45 $
 *     	   $Author: snimmakayala $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 *$Log: ebiz_AdjustInventoryQB_SL.js,v $
 *Revision 1.5.4.1.4.2.4.1  2013/03/15 09:28:45  snimmakayala
 *CASE201112/CR201113/LOG2012392
 *Production and UAT issue fixes.
 *
 *Revision 1.5.4.1.4.2  2013/01/24 03:05:27  kavitha
 *CASE201112/CR201113/LOG201121
 *Converted Bin Location,LP,Location dropdown - DBCombo
 *
 *Revision 1.5.4.1.4.1  2012/12/24 13:19:06  schepuri
 *CASE201112/CR201113/LOG201121
 *modified item,itemstatus to multiselect
 *
 *Revision 1.5.4.1  2012/04/21 09:11:41  schepuri
 *CASE201112/CR201113/LOG201121
 *added binloc,lp,itemstatus fields in QB
 *
 *Revision 1.5  2011/08/20 07:22:41  vrgurujala
 *CASE201112/CR201113/LOG201121
 *
 *Revision 1.4  2011/06/15 08:01:04  skota
 *CASE201112/CR201113/LOG201121
 *code changes to check 'Active' status while populating company drop down and removed the condition 'Math.min(500)' while populating item and location drop downs
 *
 *****************************************************************************/
function AdjustQBInventory(request, response){
	if (request.getMethod() == 'GET') 
	{
		var form = nlapiCreateForm('Inventory Adjustment');

		var binLocationField = form.addField('custpage_binlocation', 'select', 'Bin Location','customrecord_ebiznet_location');
		//binLocationField.addSelectOption('', '');
		// var varItem = form.addField('custpage_item', 'select', 'Item','Item');
		var varItem = form.addField('custpage_item', 'multiselect', 'Item','Item');

		//varItem.addSelectOption('', '');        

		/*          
        var searchresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, null, new nlobjSearchColumn('custrecord_ebiz_inv_sku').setSort());

        if(searchresults != null)
        {
        	nlapiLogExecution('ERROR', 'Inventory record count for Item', searchresults.length);

	        for (var i = 0; i < searchresults.length; i++) 
			{
	            var res = form.getField('custpage_item').getSelectOptions(searchresults[i].getText('custrecord_ebiz_inv_sku'), 'is');

				if (res != null) 
				{
	                if (res.length > 0)					
	                    continue;	                
	            }	            
	            varItem.addSelectOption(searchresults[i].getValue('custrecord_ebiz_inv_sku'), searchresults[i].getText('custrecord_ebiz_inv_sku'));
	        }
        }        
		 */
		//	var invtlp = form.addField('custpage_invtlp', 'select', 'LP','customrecord_ebiznet_master_lp');
		//invtlp.addSelectOption('', '');

		var invtlp = form.addField('custpage_invtlp', 'select', 'LP','customrecord_ebiznet_master_lp');

//		var varLoc = form.addField('custpage_location', 'select', 'Location');
//		varLoc.addSelectOption('', '');

		var varLoc = form.addField('custpage_location', 'select', 'Location','location');
		varLoc.setMandatory(true);

		/*var searchlocresults = nlapiSearchRecord('customrecord_ebiznet_createinv', null, null, new nlobjSearchColumn('custrecord_ebiz_inv_loc'));

        if(searchlocresults != null)
        {
        	nlapiLogExecution('ERROR', 'Inventory record count for Location', searchlocresults.length);

	        for (var i = 0; i < searchlocresults.length; i++) 
	        {
	            var res = form.getField('custpage_location').getSelectOptions(searchlocresults[i].getText('custrecord_ebiz_inv_loc'), 'is');
	            if (res != null) {
	                if (res.length > 0)
	                    continue;	                
	            }
	            varLoc.addSelectOption(searchlocresults[i].getValue('custrecord_ebiz_inv_loc'), searchlocresults[i].getText('custrecord_ebiz_inv_loc'));
	        }
        }*/

//		var varComp = form.addField('custpage_company', 'select', 'Company');
//		varComp.addSelectOption('', '');
		var varComp = form.addField('custpage_company', 'select', 'Company','customrecord_ebiznet_company');
		var vItemStatus = form.addField('custpage_itemstatus', 'select', 'Item Status','customrecord_ebiznet_sku_status');
//		var searchcompresults = nlapiSearchRecord('customrecord_ebiznet_company', null, new nlobjSearchFilter('isinactive', null, 'is', 'F'), new nlobjSearchColumn('Name'));

//		if (searchcompresults != null)
//		{
//		for (var i = 0; i < searchcompresults.length; i++) 
//		{
//		var res = form.getField('custpage_company').getSelectOptions(searchcompresults[i].getValue('Name'), 'is');
//		if (res != null) 
//		{
//		if (res.length > 0) 
//		continue;                
//		}
//		if(searchcompresults[i].getValue('Name') != "")
//		varComp.addSelectOption(searchcompresults[i].getValue('Name'), searchcompresults[i].getValue('Name'));
//		}
//		}         

		//3 - In bound Storage
		//19 - Inventory Storage
		//var filtersinv = new Array();
		//filtersinv[0] = new nlobjSearchFilter('custrecord_wms_inv_status_flag', null, 'anyof', [3, 19]);
		//var columns = new Array();
		//columns[0] = new nlobjSearchColumn('custrecord_ebiz_inv_binloc');
		//columns[1] = new nlobjSearchColumn('custrecord_ebiz_inv_sku');
		//columns[2] = new nlobjSearchColumn('custrecord_ebiz_inv_lp');
		//columns[1].setSort();
		//columns[1].setSort();		
		//columns[2].setSort();
		//var searchInventory = nlapiSearchRecord('customrecord_ebiznet_createinv', null, filtersinv, columns);
		//if (searchInventory != null) 
		//{
		//	for (var i = 0; i < searchInventory.length; i++) 
		//	{
		//		var res = form.getField('custpage_binlocation').getSelectOptions(searchInventory[i].getText('custrecord_ebiz_inv_binloc'), 'is');
		//		if (res != null) 
		//		{
		//			if (res.length > 0) 
		//				continue;                    
		//		}
		//		binLocationField.addSelectOption(searchInventory[i].getValue('custrecord_ebiz_inv_binloc'), searchInventory[i].getText('custrecord_ebiz_inv_binloc'));
		//	}
		//	for (var i = 0; i < searchInventory.length; i++) 
		//	{
		//		invtlp.addSelectOption(searchInventory[i].getValue('custrecord_ebiz_inv_lp'), searchInventory[i].getValue('custrecord_ebiz_inv_lp'));
		//	}
		/*var vItemStatus = form.addField('custpage_itemstatus', 'select', 'Item Status');
			vItemStatus.addSelectOption('', '');
			var filtersStatus = new Array();
			filtersStatus.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			var columnsstatus = new Array();
			columnsstatus.push(new nlobjSearchColumn('custrecord_statusdescriptionskustatus'));
			var searchStatus = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, filtersStatus, columnsstatus);
			if (searchStatus != null) 
			{
				for (var i = 0; i < searchStatus.length; i++) 
				{
					var res = form.getField('custpage_itemstatus').getSelectOptions(searchStatus[i].getValue('custrecord_statusdescriptionskustatus'), 'is');
					if (res != null) 
					{
						if (res.length > 0) 
							continue;                    
					}
					vItemStatus.addSelectOption(searchStatus[i].getId(), searchStatus[i].getValue('custrecord_statusdescriptionskustatus'));
				}
			}*/


		/*	var vItemStatus = form.addField('custpage_itemstatus', 'multiselect', 'Item Status');
			var filtersStatus = new Array();
			filtersStatus.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			var columnsstatus = new Array();
			columnsstatus.push(new nlobjSearchColumn('custrecord_statusdescriptionskustatus'));
			var searchStatus = nlapiSearchRecord('customrecord_ebiznet_sku_status', null, filtersStatus, columnsstatus);
			for (var i = 0; searchStatus != null && i < Math.min(500, searchStatus.length); i++) {

				nlapiLogExecution('ERROR', 'first',searchStatus[i].getId() + ',' + searchStatus[i].getValue('custrecord_statusdescriptionskustatus') );

				vItemStatus.addSelectOption(searchStatus[i].getId(), searchStatus[i].getValue('custrecord_statusdescriptionskustatus'));
			}*/
		var button = form.addSubmitButton('Display');
		//}
		//else 
		//{
		//	var msg = form.addField('custpage_message', 'inlinehtml', null, null, null);
		//	msg.setDefaultValue("<div id='div__alert' align='center'></div><script>showAlertBox('div__alert', 'Confirmation', 'No Records', NLAlertDialog.TYPE_LOWEST_PRIORITY,  '100%', null, null, null);</script></div>");
		//}
		response.writePage(form);
	}
	else 
	{    
		var vargetitem = request.getParameter('custpage_item');
		var vargetloc  = request.getParameter('custpage_location');



		var Adjustarray = new Array();
		Adjustarray["custpage_item"] = vargetitem;
		Adjustarray["custpage_loc"] = vargetloc;   

		Adjustarray["custpage_binlocation"] = request.getParameter('custpage_binlocation');

		var lpname = "";
		if(request.getParameter('custpage_invtlp') !=null && request.getParameter('custpage_invtlp') !=""){
			var fields = ['custrecord_ebiz_lpmaster_lp'];
			var columns = nlapiLookupField('customrecord_ebiznet_master_lp', request.getParameter('custpage_invtlp'), fields);
			lpname = columns.custrecord_ebiz_lpmaster_lp;
		}
		Adjustarray["custpage_invtlp"] = lpname; 
		Adjustarray["custpage_itemstatus"] = request.getParameter('custpage_itemstatus');
		nlapiLogExecution('ERROR', 'LP valueQB', Adjustarray["custpage_invtlp"]);
		response.sendRedirect('SUITELET', 'customscript_adjust_inventory_main', 'customdeploy_adjust_inventory_main_di', false, Adjustarray);
	}
}
