/***************************************************************************
  eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/NSWMS/EoN/RF/Inventory/Suitelet/Attic/ebiz_RF_WO_PickingQuantityException.js,v $
 *     	   $Revision: 1.1.2.1.2.12 $
 *     	   $Date: 2014/10/21 13:07:06 $
 *     	   $Author: vmandala $
 *     	   $Name: b_WMS_2015_2_StdBundle_Issues $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_WO_PickingQuantityException.js,v $
 * Revision 1.1.2.1.2.12  2014/10/21 13:07:06  vmandala
 * Case# 201410796 Stdbundle issue fixed
 *
 * Revision 1.1.2.1.2.11  2014/09/01 16:14:42  gkalla
 * case#201410172
 * JB Commented last pick task code
 *
 * Revision 1.1.2.1.2.10  2014/08/15 15:36:36  snimmakayala
 * Case: 20149972 & 20149973
 * JAWBONE WO PICKING FIXES
 *
 * Revision 1.1.2.1.2.9  2014/06/17 15:37:22  skavuri
 * Case# 20148354 SB Issue Fixed.
 *
 * Revision 1.1.2.1.2.8  2014/06/13 08:51:27  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.1.2.1.2.7  2014/05/30 00:34:24  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.1.2.1.2.6  2014/05/12 15:32:28  skavuri
 * Case# 20148354 SB Issue fixed
 *
 * Revision 1.1.2.1.2.5  2014/01/06 13:16:03  grao
 * Case# 20126579 related issue fixes in Sb issue fixes
 *
 * Revision 1.1.2.1.2.4  2013/12/04 16:17:26  skreddy
 * Case# 20126149
 * 2014.1 stnd bundle issue fix
 *
 * Revision 1.1.2.1.2.3  2013/04/17 16:02:37  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.1.2.1.2.2  2013/03/19 11:48:12  snimmakayala
 * CASE201112/CR201113/LOG2012392
 * Production and UAT issue fixes.
 *
 * Revision 1.1.2.1.2.1  2013/03/08 14:38:41  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Endochoice as part of Standard bundle
 *
 * Revision 1.1.2.1  2013/02/18 06:26:35  skreddy
 * CASE201112/CR201113/LOG201121
 *  Qty exception in Wo Picking
 *
 * Revision 1.2.2.22  2012/09/04 20:45:33  snimmakayala
 * CASE201112/CR201113/LOG201121
 *
 *
 *
 *****************************************************************************/

function WOPickQuantityException(request, response)
{

	var context = nlapiGetContext();
	var sessionobj = context.getSessionObject('session');
	var user=context.getUser();	
	nlapiLogExecution('DEBUG', 'textobj', sessionobj);
	nlapiLogExecution('DEBUG', 'user', user);
	if (request.getMethod() == 'GET') 
	{
		//var getItemName="",getItemNo="",getQuantity="";

		var getWOid = request.getParameter('custparam_woid');
		var getRecordInternalId = request.getParameter('custparam_recordinternalid');
		var getContainerLpNo = request.getParameter('custparam_containerlpno');
		var getQuantity = request.getParameter('custparam_expectedquantity');
		var getBeginLocation = request.getParameter('custparam_beginLocation');
		var getItem = request.getParameter('custparam_item');
		var getItemName = request.getParameter('custparam_itemname');
		var getItemDesc = request.getParameter('custparam_itemdescription');
		var getItemNo = request.getParameter('custparam_iteminternalid');
		var getdoLoineId = request.getParameter('custparam_dolineid');
		var getInvoiceRefNo = request.getParameter('custparam_invoicerefno');
		var getBeginLocationName = request.getParameter('custparam_beginlocationname');
		var getEndLocationInternalId = request.getParameter('custparam_endlocinternalid');
		var getEndLocation = request.getParameter('custparam_endlocation');
		var getOrderLineNo = request.getParameter('custparam_orderlineno');
		var getClusterNo = request.getParameter('custparam_clusterno');
		var getBatchNo = request.getParameter('custparam_batchno');
		var getNoofRecords = request.getParameter('custparam_noofrecords');
		var getNextLocation = request.getParameter('custparam_nextlocation');
		var name = request.getParameter('name');
		var getRecCount = request.getParameter('custparam_RecCount');
		var getContainerSize = request.getParameter('custparam_containersize');
		var getEbizOrdNo = request.getParameter('custparam_ebizordno');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var whLocation = request.getParameter('custparam_whlocation');
		//parameters erquired if serializedinventoryitem
		var getnumber = request.getParameter('custparam_number');
		var getRecType = request.getParameter('custparam_RecType');
		var getSerOut = request.getParameter('custparam_SerOut');
		var getSerIn = request.getParameter('custparam_SerIn');
		var NextItemInternalId=request.getParameter('custparam_nextiteminternalid');
		var getNextExpectedQuantity = request.getParameter('custparam_nextexpectedquantity');
		var NextRecordId=request.getParameter('custparam_nextrecordid');
		var Exceptionflag=request.getParameter('custparam_Exceptionflag');
		var itemType=request.getParameter('custparam_itemType');
		var ActBatchno = request.getParameter('custparam_Actbatchno');
		var ExpBatchno=request.getParameter('custparam_Expbatchno');
		var remainpickqty=request.getParameter('custparam_remainpickqty');
		nlapiLogExecution('DEBUG', 'remainpickqty', remainpickqty);
		nlapiLogExecution('DEBUG', 'NextItemInternalId', NextItemInternalId);
		var pickType=request.getParameter('custparam_picktype');
		// Case# 20148354 starts
		var getWaveNo=request.getParameter('custparam_waveno');
		nlapiLogExecution('ERROR', 'getWaveNo', getWaveNo);
		// Case# 20148354 ends
		var vSkipId=0;
		if(request.getParameter('custparam_skipid') !=null &&  request.getParameter('custparam_skipid') !="")
			vSkipId=request.getParameter('custparam_skipid');

		var getZoneNo=request.getParameter('custparam_ebizzoneno');
		nlapiLogExecution('ERROR', 'getRecordInternalId', getRecordInternalId);
		nlapiLogExecution('ERROR', 'getContainerLpNo', getContainerLpNo);
		nlapiLogExecution('ERROR', 'getQuantity', getQuantity);
		nlapiLogExecution('ERROR', 'getBeginLocation', getBeginLocation);
		nlapiLogExecution('ERROR', 'getItem', getItem);
		nlapiLogExecution('ERROR', 'getItemName', getItemName);
		nlapiLogExecution('ERROR', 'getItemDesc', getItemDesc);
		nlapiLogExecution('ERROR', 'getItemNo', getItemNo);
		nlapiLogExecution('ERROR', 'getdoLoineId', getdoLoineId);
		nlapiLogExecution('ERROR', 'getInvoiceRefNo', getInvoiceRefNo);
		nlapiLogExecution('ERROR', 'getBeginLocationName', getBeginLocationName);
		nlapiLogExecution('ERROR', 'getEndLocationInternalId', getEndLocationInternalId);
		nlapiLogExecution('ERROR', 'getEndLocation', getEndLocation);
		nlapiLogExecution('ERROR', 'getOrderLineNo', getOrderLineNo);
		nlapiLogExecution('ERROR', 'getClusterNo', getClusterNo);
		nlapiLogExecution('ERROR', 'getBatchNo', getBatchNo);
		nlapiLogExecution('ERROR', 'getNoofRecords', getNoofRecords);
		nlapiLogExecution('ERROR', 'getNextLocation', getNextLocation);
		nlapiLogExecution('ERROR', 'name', name);
		nlapiLogExecution('ERROR', 'getRecCount', getRecCount);
		nlapiLogExecution('ERROR', 'getContainerSize', getContainerSize);
		nlapiLogExecution('ERROR', 'getEbizOrdNo', getEbizOrdNo);
		nlapiLogExecution('ERROR', 'getnumber', getnumber);
		nlapiLogExecution('ERROR', 'getRecType', getRecType);
		nlapiLogExecution('ERROR', 'getSerOut', getSerOut);
		nlapiLogExecution('ERROR', 'getSerIn', getSerIn);
		nlapiLogExecution('ERROR', 'getNextExpectedQuantity', getNextExpectedQuantity);

		nlapiLogExecution('DEBUG', 'getBeginLocation', getBeginLocation);
		nlapiLogExecution('DEBUG', 'NextItemInternalId', NextItemInternalId);

		var IsPickFaceLoc = 'F';

		IsPickFaceLoc = isPickFaceLocation(getItemNo,getEndLocationInternalId);
		if(IsPickFaceLoc=='T' && Exceptionflag!='Y')
		{
			var SOarray=new Array();
			SOarray["custparam_screenno"] = 'WOPickQtyExcep';
			SOarray["custparam_error"]='CONFIRM THE OPEN REPLENS';
			SOarray["custparam_woid"] = getWOid;
			SOarray["custparam_waveno"] = getWaveNo;
			SOarray["custparam_recordinternalid"] = getRecordInternalId;
			SOarray["custparam_containerlpno"] = getContainerLpNo;
			SOarray["custparam_expectedquantity"] = getQuantity;
			SOarray["custparam_beginLocation"] = getBeginLocation;
			SOarray["custparam_item"] = getItem;
			SOarray["custparam_itemname"] = getItemName;
			SOarray["custparam_itemdescription"] = getItemDesc;
			SOarray["custparam_iteminternalid"] = getItemNo;
			SOarray["custparam_dolineid"] = getdoLoineId;
			SOarray["custparam_invoicerefno"] = getInvoiceRefNo;
			SOarray["custparam_beginlocationname"] = getBeginLocationName;
			SOarray["custparam_endlocinternalid"] = getEndLocationInternalId;
			SOarray["custparam_endlocation"] = getEndLocation;
			SOarray["custparam_orderlineno"] = getOrderLineNo;
			SOarray["custparam_clusterno"] = getClusterNo;
			SOarray["custparam_batchno"] = getBatchNo;
			SOarray["custparam_noofrecords"] = getNoofRecords;
			SOarray["custparam_nextlocation"] = getNextLocation;
			SOarray["name"] = name;
			SOarray["custparam_containersize"] = getContainerSize;
			SOarray["custparam_ebizordno"] = getEbizOrdNo;
			SOarray["custparam_number"] = getnumber;
			SOarray["custparam_RecType"] = getRecType;
			SOarray["custparam_SerOut"] = getSerOut;
			SOarray["custparam_SerIn"] = getSerIn;
			SOarray["custparam_nextiteminternalid"] = NextItemInternalId;
			SOarray["custparam_nextexpectedquantity"]=getNextExpectedQuantity;
			SOarray["custparam_nextrecordid"]=NextRecordId;
			SOarray["custparam_itemType"] = itemType;
			SOarray["custparam_Actbatchno"]=request.getParameter('custparam_Actbatchno');
			SOarray["custparam_Expbatchno"] = request.getParameter('custparam_Expbatchno');
			SOarray["custparam_remainpickqty"] = request.getParameter('custparam_remainpickqty');

			if(getZoneNo!=null && getZoneNo!="")
			{
				SOarray["custparam_ebizzoneno"] =  getZoneNo;
			}
			else
				SOarray["custparam_ebizzoneno"] = '';


			var openreplns = new Array();
			openreplns=getOpenReplns(getItemNo,getEndLocationInternalId);
			if(openreplns!=null && openreplns!='' && openreplns.length>0)
			{
				for(var i=0; i< openreplns.length;i++)
				{
					var taskpriority=openreplns[i].getValue('custrecord_taskpriority');
					var expqty=openreplns[i].getValue('custrecord_expe_qty');
					if(expqty == null || expqty == '')
					{
						expqty=0;
					}	
					nlapiLogExecution('ERROR', 'expqty',expqty);
					nlapiLogExecution('ERROR', 'getQuantity',getQuantity);
					if(parseFloat(expqty)>=parseFloat(getQuantity))
					{
						nlapiSubmitField('customrecord_ebiznet_trn_opentask', openreplns[i].getId(), 'custrecord_taskpriority', '0');
						nlapiSubmitField('customrecord_ebiznet_trn_opentask', getRecordInternalId, 'custrecord_skiptask', 'Y');	
						var SOarray=new Array();
						nlapiLogExecution('ERROR', 'pickType',pickType);
						SOarray=buildTaskArray(getWaveNo,getClusterNo,name,getEbizOrdNo,getZoneNo,pickType);

						SOarray["custparam_skipid"] = vSkipId;
						SOarray["custparam_screenno"] = 'WOPickQtyExcep';
						SOarray["custparam_error"]='CONFIRM THE OPEN REPLENS';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						return;
					}
					else
					{
						nlapiLogExecution('ERROR', 'Into Else');
						var replengen = generateReplenishment(getEndLocationInternalId,getItemNo,whLocation,getWaveNo);
						if(replengen==true)
						{
							nlapiSubmitField('customrecord_ebiznet_trn_opentask', getRecordInternalId, 'custrecord_skiptask', 'Y');
							var SOarray=new Array();
							nlapiLogExecution('ERROR', 'pickType',pickType);
							SOarray=buildTaskArray(getWaveNo,getClusterNo,name,getEbizOrdNo,getZoneNo,pickType);

							SOarray["custparam_skipid"] = vSkipId;
							SOarray["custparam_screenno"] = 'WOPickQtyExcep';
							SOarray["custparam_error"]='CONFIRM THE OPEN REPLENS';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							return;
						}
					}	
				}
			}
			else
			{
				var replengen = generateReplenishment(getEndLocationInternalId,getItemNo,whLocation,getWaveNo);
				if(replengen==true)
				{
					nlapiSubmitField('customrecord_ebiznet_trn_opentask', getRecordInternalId, 'custrecord_skiptask', 'Y');
					var SOarray=new Array();
					nlapiLogExecution('ERROR', 'pickType',pickType);
					SOarray=buildTaskArray(getWaveNo,getClusterNo,name,getEbizOrdNo,getZoneNo,pickType);

					SOarray["custparam_skipid"] = vSkipId;
					SOarray["custparam_screenno"] = 'WOPickQtyExcep';
					SOarray["custparam_error"]='CONFIRM THE OPEN REPLENS';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;
				}
				else
				{
					nlapiSubmitField('customrecord_ebiznet_trn_opentask', getRecordInternalId, 'custrecord_skiptask', 'Y');
					var SOarray=new Array();
					nlapiLogExecution('ERROR', 'pickType',pickType);
					SOarray=buildTaskArray(getWaveNo,getClusterNo,name,getEbizOrdNo,getZoneNo,pickType);

					SOarray["custparam_skipid"] = vSkipId;
					SOarray["custparam_screenno"] = 'WOPickQtyExcep';
					//SOarray["custparam_error"]='CONFIRM THE OPEN REPLENS';
					SOarray["custparam_woid"] = getWOid;// case# 201416832
					SOarray["custparam_error"]='Insufficient inventory in Bulk location';
					Exceptionflag='Y';
					SOarray["custparam_Exceptionflag"]=Exceptionflag;
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					return;
				}

			}
		}

		if(getBeginLocation==null)
		{
			getBeginLocation=getNextLocation;
		}
		if(getItemNo==null)
		{
			getItemNo=NextItemInternalId;
		}
		//general func is added in script library
		var functionkeyHtml=getFunctionkeyScript('_rf_pickingquantityexception'); 
		var html = "<html><head>";
		html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";

		html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";

		//Case# 20148749 Refresh Functionality starts
		html = html + "var version = navigator.appVersion;";
		html = html + "document.onkeydown = function (e) {";
		html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
		html = html + "if ((version.indexOf('MSIE') != -1)) { ";
		html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
		html = html + "else {if (keycode == 116)return false;}";
		html = html + "};";
		//Case# 20148749 Refresh Functionality ends
		html = html + "nextPage = new String(history.forward());";          
		html = html + "if (nextPage == 'undefined')";     
		html = html + "{}";     
		html = html + "else";     
		html = html + "{  location.href = window.history.forward();"; 
		html = html + "} ";

		//html = html + " document.getElementById('enterlp').focus();";

		html = html + "</script>";  

		html = html +functionkeyHtml;
		html = html + "</head>";
		html = html + "<body onkeydown='return OnKeyDown_CL();'>";
		html = html + "	<form name='_rf_pickingquantityexception' method='POST'>";
		html = html + "		<table>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>Item : <label>" + getItem + "</label>";
		html = html + "				<input type='hidden' name='hdnWOid' value=" + getWOid + ">";
		html = html + "				<input type='hidden' name='hdnRecordInternalId' value=" + getRecordInternalId + ">";
		html = html + "				<input type='hidden' name='hdnContainerLpNo' value=" + getContainerLpNo + ">";
		html = html + "				<input type='hidden' name='hdnQuantity' value=" + getQuantity + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocation' value=" + getBeginLocation + ">";
		html = html + "				<input type='hidden' name='hdnItem' value=" + getItem + ">";
		html = html + "				<input type='hidden' name='hdnItemName' value=" + getItemName + ">";
		html = html + "				<input type='hidden' name='hdnItemDesc' value=" + getItemDesc + ">";
		html = html + "				<input type='hidden' name='hdnItemNo' value=" + getItemNo + ">";
		html = html + "				<input type='hidden' name='hdndoLoineId' value=" + getdoLoineId + ">";
		html = html + "				<input type='hidden' name='hdnInvoiceRefNo' value=" + getInvoiceRefNo + ">";
		html = html + "				<input type='hidden' name='hdnBeginLocationName' value=" + getBeginLocationName + ">";
		html = html + "				<input type='hidden' name='hdnEndLocationInternalId' value=" + getEndLocationInternalId + ">";
		html = html + "				<input type='hidden' name='hdnEndLocation' value=" + getEndLocation + ">";
		html = html + "				<input type='hidden' name='hdnOrderLineNo' value=" + getOrderLineNo + ">";
		html = html + "				<input type='hidden' name='hdnClusterNo' value=" + getClusterNo + ">";
		html = html + "				<input type='hidden' name='hdnBatchNo' value=" + getBatchNo + ">";
		html = html + "				<input type='hidden' name='hdnNoofRecords' value=" + getNoofRecords + ">";
		html = html + "				<input type='hidden' name='hdnNextLocation' value=" + getNextLocation + ">";
		html = html + "				<input type='hidden' name='hdnname' value=" + name + ">";
		html = html + "				<input type='hidden' name='hdnRecCount' value=" + getRecCount + ">";
		html = html + "				<input type='hidden' name='hdnContainerSize' value=" + getContainerSize + ">";
		html = html + "				<input type='hidden' name='hdnEbizOrdNo' value=" + getEbizOrdNo + ">";
		html = html + "				<input type='hidden' name='hdnNextItemId' value=" + NextItemInternalId + ">";	
		html = html + "				<input type='hidden' name='hdnnumber' value=" + getnumber + ">";
		html = html + "				<input type='hidden' name='hdnRecType' value=" + getRecType + ">";
		html = html + "				<input type='hidden' name='hdnSerOut' value=" + getSerOut + ">";
		html = html + "				<input type='hidden' name='hdnSerIn' value=" + getSerIn + ">";
		html = html + "				<input type='hidden' name='hdnNextItemId' value=" + NextItemInternalId + ">";
		html = html + "				<input type='hidden' name='hdnpicktype' value=" + pickType + ">";
		html = html + "				<input type='hidden' name='hdnwhlocation' value=" + whLocation + ">";
		html = html + "				<input type='hidden' name='hdnebizzoneno' value=" + getZoneNo + ">";
		html = html + "				<input type='hidden' name='hdnflag'>";
		html = html + "				<input type='hidden' name='hdnskipid' value=" + vSkipId + ">";
		html = html + "				<input type='hidden' name='hdnNextexpectedqty' value=" + getNextExpectedQuantity + ">";
		html = html + "				<input type='hidden' name='hdnNextrecordid' value=" + NextRecordId + ">";
		html = html + "				<input type='hidden' name='hdnitemtype' value=" + itemType + ">";
		html = html + "				<input type='hidden' name='hdnactlot' value=" + ActBatchno + ">";
		html = html + "				<input type='hidden' name='hdnexplot' value=" + ExpBatchno + ">";
		html = html + "				<input type='hidden' name='hdnremainpickqty' value=" + remainpickqty + ">";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>Enter Actual Quantity ";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterquantity' id='enterquantity' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>REASON : ";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterreason' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>Enter Remaining Quantity ";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'><input name='enterremainqty' type='text'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "			<tr>";
		html = html + "				<td align = 'left'>SEND <input name='cmdConfirm' type='submit' value='F8' onclick='this.form.hdnflag.value=this.value;this.form.submit();this.disabled=true; return false'/>";
		html = html + "					PREV <input name='cmdPrevious' type='submit' value='F7'/>";
		html = html + "				</td>";
		html = html + "			</tr>";
		html = html + "		 </table>";
		html = html + "	</form>";
		//Case# 20148882 (added Focus Functionality for Textbox)
		html = html + "<script type='text/javascript'>document.getElementById('enterquantity').focus();</script>";
		html = html + "</body>";
		html = html + "</html>";

		response.write(html);
	}
	else 
	{
		nlapiLogExecution('ERROR', 'SearchResults ', 'Length is not null');

		// This variable is to hold the LP entered.
		var getEnteredQuantity = request.getParameter('enterquantity');
		nlapiLogExecution('ERROR', 'getEnteredQuantity', getEnteredQuantity);

		var getEnteredReason = request.getParameter('enterreason');
		var vZoneId=request.getParameter('hdnebizzoneno');
		nlapiLogExecution('ERROR', 'getEnteredReason', getEnteredReason);

		var getwoid =request.getParameter('hdnWOid');
		var vlineno =request.getParameter('hdnOrderLineNo');
		var vSkipId=request.getParameter('hdnskipid');
		nlapiLogExecution('ERROR', 'vSkipId', vSkipId);

		//var optedEvent = request.getParameter('cmdPrevious');

		var SOarray = new Array();
		var SOarray = new Array();
		var getwoid =request.getParameter('hdnWOid');
		var RecordInternalId = request.getParameter('hdnRecordInternalId');
		var ContainerLpNo = request.getParameter('hdnContainerLpNo');
		var FetchedQuantity = request.getParameter('hdnQuantity');
		var BeginLocation = request.getParameter('hdnBeginLocation');
		var Item = request.getParameter('hdnItem');
		var ItemName = request.getParameter('hdnItemName');
		var ItemDesc = request.getParameter('hdnItemDesc');
		var ItemNo = request.getParameter('hdnItemNo');
		var doLoineId = request.getParameter('hdndoLoineId');
		var InvoiceRefNo = request.getParameter('hdnInvoiceRefNo');
		var BeginLocationName = request.getParameter('hdnBeginLocationName');
		var EndLocationInternalId = request.getParameter('hdnEndLocationInternalId');
		var EndLocation = request.getParameter('hdnEndLocation');
		var OrderLineNo = request.getParameter('hdnOrderLineNo');
		var ClusterNo = request.getParameter('hdnClusterNo');
		var BatchNo = request.getParameter('hdnBatchNo');
		var NoofRecords = request.getParameter('hdnNoofRecords');
		var NextLocation = request.getParameter('hdnNextLocation');
		var name = request.getParameter('hdnname');
		var RecCount = request.getParameter('hdnRecCount');
		var ContainerSize = request.getParameter('hdnContainerSize');
		var EbizOrdNo = request.getParameter('hdnEbizOrdNo');
		var NextItemInternalId=request.getParameter('hdnNextItemId');
		var NextrecordID=request.getParameter('hdnNextrecordid');
		var ItemType=request.getParameter('hdnitemtype');	
		var vbatch= request.getParameter('hdnexplot');

		var number = request.getParameter('hdnnumber');
		var RecType = request.getParameter('hdnRecType');
		var SerOut = request.getParameter('hdnSerOut');
		var SerIn = request.getParameter('hdnSerIn');
		var remainqty = request.getParameter('enterremainqty');
		if(remainqty==null || remainqty=='')
			remainqty=0;


		SOarray["custparam_screenno"] = 'WOPickQtyExcep';
		SOarray["custparam_nextiteminternalid"] = request.getParameter('hdnNextItemId');
		SOarray["custparam_picktype"] = request.getParameter('hdnpicktype');
		SOarray["custparam_whlocation"] = request.getParameter('hdnwhlocation');

		nlapiLogExecution('ERROR', 'getEnteredQuantity', getEnteredQuantity);
		nlapiLogExecution('ERROR', 'getFetchedQuantity', FetchedQuantity);
		nlapiLogExecution('ERROR', 'RecordInternalId', RecordInternalId);
		nlapiLogExecution('ERROR', 'ContainerLpNo', ContainerLpNo);
		nlapiLogExecution('ERROR', 'FetchedQuantity', FetchedQuantity);
		nlapiLogExecution('ERROR', 'BeginLocation', BeginLocation);
		nlapiLogExecution('ERROR', 'Item', Item);
		nlapiLogExecution('ERROR', 'ItemName', ItemName);
		nlapiLogExecution('ERROR', 'ItemDesc', ItemDesc);
		nlapiLogExecution('ERROR', 'ItemNo', ItemNo);
		nlapiLogExecution('ERROR', 'doLoineId', doLoineId);
		nlapiLogExecution('ERROR', 'InvoiceRefNo', InvoiceRefNo);
		nlapiLogExecution('ERROR', 'BeginLocationName', BeginLocationName);
		nlapiLogExecution('ERROR', 'EndLocationInternalId', EndLocationInternalId);
		nlapiLogExecution('ERROR', 'EndLocation', EndLocation);
		nlapiLogExecution('ERROR', 'OrderLineNo', OrderLineNo);
		nlapiLogExecution('ERROR', 'ClusterNo', ClusterNo);
		nlapiLogExecution('ERROR', 'BatchNo', BatchNo);
		nlapiLogExecution('ERROR', 'NoofRecords', NoofRecords);
		nlapiLogExecution('ERROR', 'NextLocation', NextLocation);
		nlapiLogExecution('ERROR', 'name', name);
		nlapiLogExecution('ERROR', 'ContainerSize', ContainerSize);
		nlapiLogExecution('ERROR', 'EbizOrdNo', EbizOrdNo);
		nlapiLogExecution('ERROR', 'number', number);
		nlapiLogExecution('ERROR', 'RecType', RecType);
		nlapiLogExecution('ERROR', 'SerOut', SerOut);
		nlapiLogExecution('ERROR', 'SerIn', SerIn);


		SOarray["custparam_enteredQty"] = getEnteredQuantity;
		SOarray["custparam_enteredReason"] = getEnteredReason;
		//SOarray["custparam_fetchedQty"] = FetchedQuantity;

		SOarray["custparam_woid"] = getwoid;
		SOarray["custparam_recordinternalid"] = RecordInternalId;
		SOarray["custparam_containerlpno"] = ContainerLpNo;
		SOarray["custparam_expectedquantity"] = FetchedQuantity;
		SOarray["custparam_beginLocation"] = BeginLocation;
		SOarray["custparam_item"] = Item;
		SOarray["custparam_itemname"] = ItemName;
		SOarray["custparam_itemdescription"] = ItemDesc;
		SOarray["custparam_iteminternalid"] = ItemNo;
		SOarray["custparam_dolineid"] = doLoineId;
		SOarray["custparam_invoicerefno"] = InvoiceRefNo;
		SOarray["custparam_beginlocationname"] = BeginLocationName;
		SOarray["custparam_endlocinternalid"] = EndLocationInternalId;
		SOarray["custparam_endlocation"] =request.getParameter('custparam_endlocation');
		SOarray["custparam_orderlineno"] = OrderLineNo;
		SOarray["custparam_clusterno"] = ClusterNo;
		SOarray["custparam_batchno"] = BatchNo;
		SOarray["custparam_noofrecords"] = NoofRecords;
		SOarray["custparam_nextlocation"] = NextLocation;
		SOarray["name"] = name;
		SOarray["custparam_containersize"] = ContainerSize;
		SOarray["custparam_ebizordno"] = EbizOrdNo;
		SOarray["custparam_number"] = number;
		SOarray["custparam_RecType"] = RecType;
		SOarray["custparam_SerOut"] = SerOut;
		SOarray["custparam_SerIn"] = SerIn;
		SOarray["custparam_nextiteminternalid"] = request.getParameter('hdnNextItemId');
		SOarray["custparam_nextexpectedquantity"]=request.getParameter('hdnNextexpectedqty');
		SOarray["custparam_nextrecordid"]=request.getParameter('hdnNextrecordid');
		SOarray["custparam_itemType"] = ItemType;
		SOarray["custparam_Actbatchno"]=request.getParameter('hdnactlot');
		SOarray["custparam_Expbatchno"] = request.getParameter('hdnexplot');
		SOarray["custparam_remainpickqty"] = request.getParameter('hdnremainpickqty');
		SOarray["custparam_ebizzoneno"] = request.getParameter('hdnebizzoneno');

		var nextexpqty = request.getParameter('hdnNextexpectedqty');

		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  request.getParameter('hdnebizzoneno');
		}
		else
			SOarray["custparam_ebizzoneno"] = '';

		var vSkipId=request.getParameter('hdnskipid');
		SOarray["custparam_skipid"] = request.getParameter('hdnskipid');		
		SOarray["custparam_remqty"] = remainqty;


		nlapiLogExecution('ERROR', 'hdnNextItemId', request.getParameter('hdnNextItemId'));

		var filters = new Array(); 			 
		filters[0] = new nlobjSearchFilter('custrecord_sku', null, 'is', ItemNo);	
		filters[1] = new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', getwoid);	
		filters[2] = new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]);
		if(vZoneId!=null && vZoneId!="" && vZoneId!="null")
		{
			filters[3] =new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', vZoneId);
		}

		var columns1 = new Array(); 
		columns1[0] = new nlobjSearchColumn('custrecord_parent_sku_no' ); 

		var searchresultskititem = nlapiSearchRecord( 'customrecord_ebiznet_trn_opentask', null, filters, columns1 ); 
		if(searchresultskititem!=null && searchresultskititem!='')
		{
			var vkititem = searchresultskititem[0].getValue('custrecord_parent_sku_no');
			var vkititemtext = searchresultskititem[0].getText('custrecord_parent_sku_no');
		}

		var kititemTypesku = '';

		if(vkititem!=null && vkititem!='')
		{
			var kititemTypesku = nlapiLookupField('item', vkititem, 'recordType');
		}

		nlapiLogExecution('ERROR', 'kititemTypesku', kititemTypesku);

		if(kititemTypesku=='kititem')
		{
			var searchresultsitem = nlapiLoadRecord(kititemTypesku, vkititem);
			var SkuNo=searchresultsitem.getFieldValue('itemid');

			var kitfilters = new Array(); 			 
			kitfilters[0] = new nlobjSearchFilter('itemid', null, 'is', SkuNo);	

			var kitcolumns1 = new Array(); 
			kitcolumns1[0] = new nlobjSearchColumn( 'memberitem' ); 			
			kitcolumns1[1] = new nlobjSearchColumn( 'memberquantity' );

			var searchresults = nlapiSearchRecord( 'item', null, kitfilters, kitcolumns1 ); 
			if(searchresults!=null && searchresults!='')
			{

				for(var w=0; w<searchresults.length;w++) 
				{
					fulfilmentItem = searchresults[w].getValue('memberitem');
					memberitemqty = searchresults[w].getValue('memberquantity');

					if(fulfilmentItem==ItemNo)
					{
						var kitqty=Math.floor(getEnteredQuantity/memberitemqty)*memberitemqty;

						if((getEnteredQuantity!=kitqty) && (parseFloat(getEnteredQuantity) < parseFloat(FetchedQuantity)))
						{	
							SOarray["custparam_screenno"] = 'WOPickQtyExcep';	
							SOarray["custparam_error"] = 'Pick in multiples of 2 as this pick is part of Kit Package item';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							nlapiLogExecution('ERROR', 'Error: ', 'Pick in multiples of 2 as this pick is part of Kit Package item');
							return;

						}

					}
				}
			}	

		}

		if (sessionobj!=context.getUser()) {
			try
			{

				if(sessionobj==null || sessionobj=='')
				{
					sessionobj=context.getUser();
					context.setSessionObject('session', sessionobj); 
				}
//				if the previous button 'F7' is clicked, it has to go to the previous screen 
				if (request.getParameter('cmdPrevious') == 'F7') {
					nlapiLogExecution('ERROR', 'Clicked on Previous', request.getParameter('cmdPrevious'));
					response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_confirm', 'customdeploy_ebiz_rf_wo_pick_confirm_di', false, SOarray);
				}
				else 
					nlapiLogExecution('ERROR', 'Clicked on Confirm', request.getParameter('cmdConfirm'));
				if (request.getParameter('hdnflag') == 'F8') {
					if(getEnteredQuantity==null || getEnteredQuantity=='')
					{
						SOarray["custparam_error"] = 'Actual Qty should not be blank';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						nlapiLogExecution('ERROR', 'Error: ', 'Actual Qty should not be blank');
						return;
					}

					if(getEnteredQuantity==null || getEnteredQuantity=='' || getEnteredQuantity==' ' || isNaN(getEnteredQuantity))
						getEnteredQuantity=0;

					nlapiLogExecution('ERROR', 'getEnteredQuantity after validate', getEnteredQuantity);

					//The following variable should be 'Y' for Dynacraft.
					var vAutoContainerFlag='N';
					SOarray["custparam_autocont"] = vAutoContainerFlag;
					nlapiLogExecution('ERROR', 'vAutoContainerFlag', vAutoContainerFlag);
					if(getEnteredQuantity!=null || getEnteredQuantity!='')
					{
						/*if(getEnteredQuantity==null || getEnteredQuantity=='')
				{
					SOarray["custparam_error"] = 'Actual Qty should not be blank';
					response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
					nlapiLogExecution('ERROR', 'Error: ', 'Actual Qty should not be blank');
					return;
				}*/

						if(isNaN(getEnteredQuantity))
							getEnteredQuantity=0;
						//case 201410796 
						//if(parseFloat(getEnteredQuantity) > parseFloat(FetchedQuantity))
						var totalqty=(parseFloat(getEnteredQuantity)) + (parseFloat(remainqty));

						if(parseFloat(getEnteredQuantity) > parseFloat(FetchedQuantity) || parseFloat(totalqty) > parseFloat(FetchedQuantity))
						{				

							SOarray["custparam_error"] = 'Pick Qty should not be greater than Ordered Qty';
							response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
							nlapiLogExecution('ERROR', 'Error: ', 'Pick Qty should not be greater than Ordered Qty');
							return;
						}
						var RcId=RecordInternalId;
						var EndLocation = request.getParameter('hdnEndLocationInternalId');
						var TotalWeight=0;
						var getReason=request.getParameter('custparam_enteredReason');
						var SORec = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RecordInternalId);
						var getContainerLPNo= SORec.getFieldValue('custrecord_container_lp_no');
						getLPContainerSize= SORec.getFieldText('custrecord_container');

						SOarray["custparam_newcontainerlp"] = getContainerLPNo;
						var vPickType = request.getParameter('hdnpicktype');
						var vBatchno = request.getParameter('hdnbatchno');
						var PickQty = FetchedQuantity;
						var vActqty = getEnteredQuantity;
						var SalesOrderInternalId = EbizOrdNo;
						var getContainerSize = getLPContainerSize;
						var  vdono = doLoineId;
						nlapiLogExecution('ERROR', 'getContainerLPNo', getContainerLPNo);
						var opentaskcount=0;


						nlapiLogExecution('ERROR', 'opentaskcount', getContainerLPNo);
						nlapiLogExecution('ERROR', 'opentaskcount', vPickType);
						nlapiLogExecution('ERROR', 'opentaskcount', SalesOrderInternalId);
						nlapiLogExecution('ERROR', 'opentaskcount', vdono);
						nlapiLogExecution('ERROR', 'ordName', name);
						nlapiLogExecution('ERROR', 'vZoneId', vZoneId);
						//opentaskcount=getOpenTasksCount(WaveNo,getContainerLPNo,vPickType,SalesOrderInternalId,name,vZoneId,ClusterNo);

						opentaskcount=getOpenTasksCount(getwoid,vZoneId);
						nlapiLogExecution('ERROR', 'opentaskcount', opentaskcount);
						//For fine tuning pick confirmation and doing autopacking in user event after last item
						var IsitLastPick='F';
						/*if(opentaskcount > parseFloat(vSkipId) + 1)
							IsitLastPick='F';
						else
							IsitLastPick='T';*/

						nlapiLogExecution('ERROR', 'getItem', ItemNo);



						ConfirmPickTask(ItemNo,getContainerSize,vdono,RcId,vActqty,getContainerLPNo,EndLocation,getReason,PickQty,vBatchno,
								IsitLastPick,remainqty,kititemTypesku,SalesOrderInternalId,vkititem);
						//if(RecCount > 1)
						//if(opentaskcount > parseFloat(vSkipId) + 1)
						var filterOpentask = new Array();
						filterOpentask.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
						filterOpentask.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));

						if(getwoid != null && getwoid != "")
						{
							filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', getwoid));
						}
						if(vZoneId!=null && vZoneId!="" && vZoneId!="null")
						{
							filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', vZoneId));
						}

						var SOColumns=new Array();
						SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
						SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
						SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
						SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_skiptask')); 
						SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
						SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
						SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
						SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
						SOColumns.push(new nlobjSearchColumn('name'));
						SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
						SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
						SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
						SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));

						SOColumns[1].setSort();
						SOColumns[2].setSort();
						SOColumns[3].setSort();
						SOColumns[4].setSort(true);


						var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterOpentask, SOColumns);
						nlapiLogExecution('ERROR', 'Results', SOSearchResults);
						if (SOSearchResults != null && SOSearchResults.length > 0) 
						{
							nlapiLogExecution('ERROR', 'SOSearchResults length', SOSearchResults.length);

							var SOSearchResult = SOSearchResults[0];							
							SOarray["custparam_woid"] = getwoid;
							SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
							SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
							SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
							SOarray["custparam_beginLocation"] = SOSearchResult.getText('custrecord_actbeginloc');
							SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
							SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
							SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
							SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
							SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
							SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
							SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
							SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
							SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
							SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
							SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
							SOarray["custparam_noofrecords"] = SOSearchResults.length;		
							SOarray["name"] =  SOSearchResult.getValue('name');
							SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
							SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
							NextBatch=SOSearchResult.getValue('custrecord_batch_no');

							if(SOSearchResults.length > 1)
							{
								var SOSearchnextResult = SOSearchResults[1];
								SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
								SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
								SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
								SOarray["custparam_nextbatchno"] = SOSearchnextResult.getValue('custrecord_batch_no');
								nlapiLogExecution('ERROR', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
								nlapiLogExecution('ERROR', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));	
								nlapiLogExecution('ERROR', 'Next Qty', SOSearchnextResult.getValue('custrecord_expe_qty'));	
							}
							else
							{
								var SOSearchnextResult = SOSearchResults[0];
								SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
								SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
								SOarray["custparam_nextbatchno"] = SOSearchnextResult.getValue('custrecord_batch_no');
								nlapiLogExecution('ERROR', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
								nlapiLogExecution('ERROR', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));	
							}

						}

						if(opentaskcount >  1)
						{

							nlapiLogExecution('ERROR', 'BeginLocation',BeginLocation);
							nlapiLogExecution('ERROR', 'NextLocation',NextLocation);
							nlapiLogExecution('ERROR', 'ItemNo',ItemNo);
							nlapiLogExecution('ERROR', 'NextItemInternalId',NextItemInternalId);
							nlapiLogExecution('ERROR', 'vbatch', vbatch);
							nlapiLogExecution('ERROR', 'NextBatch', NextBatch);

							if(BeginLocation != NextLocation)
							{ 
								SOarray["custparam_beginLocationname"]=NextLocation;
								SOarray["custparam_iteminternalid"]=NextItemInternalId;
								SOarray["custparam_expectedquantity"]=null;
								nlapiLogExecution('ERROR', 'Navigating To1', 'Picking Location');
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_location', 'customdeploy_ebiz_rf_wo_picking_loc_di', false, SOarray);								

							}
							else if(ItemNo != NextItemInternalId)
							{ 
								nlapiLogExecution('ERROR', 'Navigating To1', 'Picking Item');
								SOarray["custparam_iteminternalid"] = NextItemInternalId;
								SOarray["custparam_beginLocationname"]=null;
								SOarray["custparam_expectedquantity"]=null;
								//SOarray["custparam_iteminternalid"]=null;
								response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_item', 'customdeploy_ebiz_rf_wo_picking_item_di', false, SOarray);								

							}//case 20126149?start :changed NextShowItem to NextItemInternalId  
							else if(ItemNo == NextItemInternalId)
							{ //case 20126149?end
								if(NextBatch == vbatch)
								{
									SOarray["custparam_iteminternalid"] = NextItemInternalId;
									SOarray["custparam_beginLocationname"]=null;
									SOarray["custparam_expectedquantity"]=nextexpqty;
									nlapiLogExecution('ERROR', 'Navigating To1', 'Same Item and  same Batch');
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_confirm', 'customdeploy_ebiz_rf_wo_pick_confirm_di', false, SOarray);
								}
								else
								{
									nlapiLogExecution('ERROR', 'Navigating To1', 'Same Item ,Diff Batch');
									response.sendRedirect('SUITELET', 'customscript_ebiz_rf_wo_picking_batch', 'customdeploy_ebiz_rf_wo_picking_batch_di', false, SOarray);
								}

							}

						}
						else{


							nlapiLogExecution('ERROR', 'Navigating To1', 'Stage Location');
							response.sendRedirect('SUITELET', 'customscript_rf_wo_picking_stage_loc', 'customdeploy_rf_wo_picking_stage_loc_di', false, SOarray);					
						}

					}
					else
					{
						SOarray["custparam_error"] = 'Actual Qty should not be blank';
						response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
						nlapiLogExecution('ERROR', 'Error: ', 'Actual Qty should not be blank');
						return;

					}
				}
			}
			catch (e)  {
				response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
			} finally {					
				context.setSessionObject('session', null);
				nlapiLogExecution('DEBUG', 'finally','block');

			}
		}
		else
		{
			SOarray["custparam_screenno"] = 'WOPicking';
			SOarray["custparam_error"] = 'ORDER ALREADY IN PROCESS';
			response.sendRedirect('SUITELET', 'customscript_rf_error', 'customdeploy_rf_error_di', false, SOarray);
		}
	}
}




function getOpenTasksCount(vWOId,vZoneId)
{
	nlapiLogExecution('ERROR', 'Into getOpenTasksCount');


	var openreccount=0;
	var filterOpentask = new Array();
	filterOpentask.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));	//	Status - Picks Generated
	filterOpentask.push(new nlobjSearchFilter('custrecord_tasktype', null, 'anyof', ['3']));

	if(vWOId != null && vWOId != "")
	{
		filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', vWOId));
	}
	if(vZoneId!=null && vZoneId!="" && vZoneId!="null")
	{
		filterOpentask.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', vZoneId));
	}


	var WOcolumns = new Array();
	WOcolumns[0] = new nlobjSearchColumn('custrecord_line_no');
	WOcolumns[1] = new nlobjSearchColumn('custrecord_sku');
	WOcolumns[2] = new nlobjSearchColumn('custrecord_expe_qty');
	WOcolumns[3] = new nlobjSearchColumn('custrecord_lpno');
	WOcolumns[4] = new nlobjSearchColumn('custrecord_batch_no');
	WOcolumns[5] = new nlobjSearchColumn('custrecord_actbeginloc');
	WOcolumns[6] = new nlobjSearchColumn('custrecord_act_qty');   
	WOcolumns[7] = new nlobjSearchColumn('custrecord_ebiz_cntrl_no');
	WOcolumns[8] = new nlobjSearchColumn('custrecord_invref_no');
	WOcolumns[9] = new nlobjSearchColumn('custrecord_wms_location');
	WOcolumns[10] = new nlobjSearchColumn('custrecord_comp_id');
	WOcolumns[11] = new nlobjSearchColumn('custrecord_ebiz_order_no');				

	WOcolumns[0].setSort();
	var WOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, filterOpentask,WOcolumns);	

	if(WOSearchResults!=null && WOSearchResults!='' && WOSearchResults.length>0)
	{
		openreccount=WOSearchResults.length;
	}



	return openreccount;

}



function ConfirmPickTask(ItemInternalId,getContainerSize,vdono,RcId,vActqty,getContainerLPNo,EndLocation,getReason,PickQty,
		vBatchno,IsitLastPick,remainqty,kititemTypesku,SalesOrderInternalId,vkititem)
{

	nlapiLogExecution('ERROR', 'ConfirmPickTask', 'sucess');
	nlapiLogExecution('ERROR', 'EndLocation', EndLocation);

	var arrDims = getSKUCubeAndWeightforconfirm(ItemInternalId, 1);
	var itemCube = 0;
	var itemWeight=0;	

	if (arrDims[0] != "" && (!isNaN(arrDims[1]))){
		//	these are commented because here  PickQty is excepted qty but we have to cal based on act qty
		itemCube = (parseFloat(PickQty) * parseFloat(arrDims[0]));
		itemWeight = (parseFloat(PickQty) * parseFloat(arrDims[1]));//	


	} 
	nlapiLogExecution('ERROR', 'checkInPOSTRequest:itemCube', itemCube);
	nlapiLogExecution('ERROR', 'checkInPOSTRequest:itemweight', itemWeight);
	nlapiLogExecution('ERROR', 'ContainerSize:ContainerSize', getContainerSize);		


	var ContainerCube;					
	var containerInternalId;
	var ContainerSize;
	var vRemaningqty=0;
	nlapiLogExecution('ERROR', 'getContainerSize', getContainerSize);	

	if(getContainerSize=="" || getContainerSize==null)
	{
		getContainerSize=ContainerSize;
		nlapiLogExecution('ERROR', 'ContainerSize', ContainerSize);	
	}
	nlapiLogExecution('ERROR', 'getContainerSize', getContainerSize);	
	var arrContainerDetails = getContainerCubeAndTarWeight("",getContainerSize);					
	if (arrContainerDetails[0] != "" && (!isNaN(arrContainerDetails[1]))) {

		ContainerCube =  parseFloat(arrContainerDetails[0]);						
		containerInternalId = arrContainerDetails[3];
		ContainerSize=arrContainerDetails[4];
		TotalWeight = (parseFloat(itemWeight) + parseFloat(arrContainerDetails[1]));//now added
	} 
	nlapiLogExecution('ERROR', 'ContainerSizeInternalId', containerInternalId);	
	nlapiLogExecution('ERROR', 'ContainerSize', ContainerSize);	

	//UpdateOpenTask,fulfillmentorder
	nlapiLogExecution('ERROR', 'vdono', vdono);	

	UpdateRFOpenTask(RcId,vActqty, containerInternalId,getContainerLPNo,itemCube,itemWeight,EndLocation,
			getReason,PickQty,IsitLastPick,remainqty);
	//UpdateRFFulfillOrdLine(vdono,vActqty);

	//create new record in opentask,fullfillordline when we have qty exception
	//vRemaningqty = vDisplayedQty-vActqty;
	vRemaningqty = PickQty-vActqty;
	var recordcount=1;//it is iterator in GUI
	nlapiLogExecution('ERROR', 'vDisplayedQty', PickQty);	
	nlapiLogExecution('ERROR', 'vActqty', vActqty);	
	if(isNaN(PickQty))
		PickQty=0;

	if(isNaN(vActqty))
		vActqty=0;

	if(isNaN(remainqty))
		remainqty=0;

	if(PickQty != vActqty)
	{			
		CreateRemainingQtyPick(RcId,PickQty,vActqty,vdono,recordcount,getContainerLPNo,null,vBatchno,remainqty);//have to check for wt and cube calculation
	}

}

/**
 * 
 * @param RcId
 * @param PickQty
 * @param containerInternalId
 * @param getEnteredContainerNo
 * @param itemCube
 * @param itemWeight
 * @param EndLocation
 */
function UpdateRFOpenTask(RcId, PickQty, containerInternalId, getEnteredContainerNo, itemCube,
		itemWeight, EndLocation,vReason,ActPickQty,IsItLastPick,remainqty)
{
	nlapiLogExecution('ERROR', 'into UpdateRFOpenTask containerlp: ', getEnteredContainerNo);
	var transaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RcId);
	var vinvrefno=transaction.getFieldValue('custrecord_invref_no');
	var vebizordno = transaction.getFieldValue('custrecord_ebiz_order_no');

	transaction.setFieldValue('custrecord_wms_status_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')
	if(PickQty!= null && PickQty !="")
		transaction.setFieldValue('custrecord_act_qty', parseFloat(PickQty).toFixed(4));
	transaction.setFieldValue('custrecord_expe_qty', parseFloat(ActPickQty).toFixed(4));
	if(containerInternalId!= null && containerInternalId !="")
		transaction.setFieldValue('custrecord_container', containerInternalId);
	if(EndLocation!=null && EndLocation!="")
		transaction.setFieldValue('custrecord_actendloc', EndLocation);	
	if(itemWeight!=null && itemWeight!="")
		transaction.setFieldValue('custrecord_total_weight', parseFloat(itemWeight).toFixed(4));
	if(itemCube!=null && itemCube!="")
		transaction.setFieldValue('custrecord_totalcube', parseFloat(itemCube).toFixed(4));	
	if(getEnteredContainerNo!=null && getEnteredContainerNo!="")
		transaction.setFieldValue('custrecord_container_lp_no', getEnteredContainerNo);
	transaction.setFieldValue('custrecord_act_end_date', DateStamp());
	transaction.setFieldValue('custrecord_actualendtime',TimeStamp());
	if(vReason!=null && vReason!="")
		transaction.setFieldValue('custrecord_notes', vReason);	
	var currentContext = nlapiGetContext();  
	var currentUserID = currentContext.getUser();
	transaction.setFieldValue('custrecord_upd_ebiz_user_no',currentUserID);
	nlapiLogExecution('ERROR', 'IsItLastPick: ', IsItLastPick);
	transaction.setFieldValue('custrecord_device_upload_flag',IsItLastPick);
	var vemployee = request.getParameter('custpage_employee');
	nlapiLogExecution('ERROR', 'vemployee :', vemployee);
	nlapiLogExecution('ERROR', 'currentUserID :', currentUserID);
//	if (vemployee != null && vemployee != "") 
//	{
//	transaction.setFieldValue('custrecord_taskassignedto',vemployee);
//	} 
//	else if (currentUserID != null && currentUserID != "") 
//	{
//	transaction.setFieldValue('custrecord_taskassignedto',currentUserID);
//	}

	nlapiLogExecution('ERROR', 'Updating RF Open Task Record Id: ', RcId);
	nlapiSubmitRecord(transaction, false, true);
	nlapiLogExecution('ERROR', 'Updated RF Open Task successfully');

	deleteAllocations(vinvrefno,PickQty,getEnteredContainerNo,ActPickQty,remainqty,vebizordno);

}

/**
 * This function clear the allocation qty from inventory.
 * @param invrefno
 * @param PickQty
 */
function deleteAllocations(invrefno,pickqty,contlpno,ActPickQty,remainqty,vebizordno)
{
	nlapiLogExecution('ERROR', 'Into deleteAllocations (Inv Ref NO)',invrefno);
	nlapiLogExecution('ERROR', 'Into deleteAllocations (pickqty)',pickqty);
	nlapiLogExecution('ERROR', 'Into deleteAllocations (ActPickQty)',ActPickQty);
	nlapiLogExecution('ERROR', 'Into deleteAllocations (remainqty)',remainqty);
	nlapiLogExecution('ERROR', 'Into deleteAllocations (vebizordno)',vebizordno);

	try
	{
		var Invttran = nlapiLoadRecord('customrecord_ebiznet_createinv', invrefno);
		var Invallocqty = Invttran.getFieldValue('custrecord_ebiz_alloc_qty');
		var InvQOH = Invttran.getFieldValue('custrecord_ebiz_qoh');
		var vLP=Invttran.getFieldValue('custrecord_ebiz_inv_lp');

		if (Invallocqty != null && Invallocqty != "" && Invallocqty>0) {
			Invttran.setFieldValue('custrecord_ebiz_alloc_qty',(parseFloat(Invallocqty)- parseFloat(pickqty)).toFixed(5));
		}

		if(remainqty==null||remainqty=='')
			remainqty=0;

		//var newqoh = (parseFloat(InvQOH) - parseFloat(ActPickQty)) + parseFloat(remainqty);

		var newqoh = parseFloat(InvQOH) - parseFloat(pickqty);

		Invttran.setFieldValue('custrecord_ebiz_qoh',parseFloat(newqoh).toFixed(5)); 
		Invttran.setFieldValue('custrecord_ebiz_callinv', 'N');
		Invttran.setFieldValue('custrecord_ebiz_displayfield', 'N');

		var invtrecid = nlapiSubmitRecord(Invttran, false, true);
		nlapiLogExecution('ERROR', 'Allocations deleted successfully (Inventory Record ID)',invtrecid);

		if(invtrecid!=null && invtrecid!='')
			CreateSTGInvtRecord(invtrecid, vLP,pickqty,vebizordno);

		if(parseFloat(newqoh) == 0)
		{		
			nlapiLogExecution('Error', 'Deleting record from inventory if QOH becomes zero', invrefno);
			var id = nlapiDeleteRecord('customrecord_ebiznet_createinv', invrefno);				
		}
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR', 'Exception in deleteAllocations',exp);
	}		
}


function UpdateRFFulfillOrdLine(vdono,vActqty)
{
	var pickqtyfinal =0;
	nlapiLogExecution('ERROR', 'into UpdateRFFulfillOrdLine : ', vdono);

	var doline = nlapiLoadRecord('customrecord_ebiznet_ordline',vdono);	

	var oldpickQty=doline.getFieldValue('custrecord_pickqty');
	var ordQty=doline.getFieldValue('custrecord_ord_qty');

	if(isNaN(oldpickQty))
		oldpickQty=0;

	if(isNaN(ordQty))
		ordQty=0;

	if(isNaN(vActqty))
		vActqty=0;

	pickqtyfinal=parseFloat(oldpickQty)+parseFloat(vActqty);

	var str = 'pickqtyfinal. = ' + pickqtyfinal + '<br>';
	str = str + 'oldpickQty. = ' + oldpickQty + '<br>';	
	str = str + 'ordQty. = ' + ordQty + '<br>';	
	str = str + 'vActqty. = ' + vActqty + '<br>';	

	nlapiLogExecution('ERROR', 'Qty Details', str);

	if(parseFloat(ordQty)>=parseFloat(pickqtyfinal))
	{
		doline.setFieldValue('custrecord_upddate', DateStamp());
		doline.setFieldValue('custrecord_pickqty', parseFloat(pickqtyfinal).toFixed(4));
		doline.setFieldValue('custrecord_pickgen_qty', parseFloat(pickqtyfinal).toFixed(4));

		if(parseFloat(pickqtyfinal)<parseFloat(ordQty))
			doline.setFieldValue('custrecord_linestatus_flag', '11'); //STATUS.OUTBOUND.PARTIALLY_PROCESSED
		else
			doline.setFieldValue('custrecord_linestatus_flag', '8'); //STATUS.OUTBOUND.PICK_CONFIRMED('C')

		nlapiSubmitRecord(doline, false, true);
	}

	nlapiLogExecution('ERROR', 'Out of UpdateRFFulfillOrdLine ');
}
function CreateRemainingQtyPick(RcId,vDisplayedQty,pickqty,vdono,recordcount,getEnteredContainerNo,TotalWeight,newLot,remainqty)
{
	nlapiLogExecution('ERROR', 'into CreateNewShortPickOpenTask : ', 'CreateNewShortPickOpenTask');
	nlapiLogExecution('ERROR', 'RcId ', 'RcId');

	var vRemaningqty=0,vname,vCompany1,vcontainer,vcontrolno,vcontainerLPno,vItemno,vEbizItemNo,vEbizWaveNo1,vLineno1,vlpno1,
	vpackcode1,vSkuStatus1,vTaskType1,vUOMid1;
	var vEbizZone1,vUOMLevel1,vEbizReceiptNo1,vInvRefNo1,vEbizUser1,vFromLP1,vEbizOrdNo1=0,vWMSLocation1,vBeginLoc1,vLot;
	var vname2,vsku2,vlineord2,vlineord2,vordlineno2,vOrdQty2,vPackCode2,vPickGenFlag2,vPickGenFlag2,vPickGenQty2,vPickQty2,
	vlineskustatus2,vlinestatusflag2;
	var vUOMid2,vBackOrdFlag2,vParentOrd2,vOrdType2,vCustomer2,vDoCarrirer2,vWMSLocation2,vLineCompany2,vWMSCarrier2,vOrderPriority2;
	var vAdjustType1;
	var pickqtyfinal=0;
	var ordqtyfinal=0;
	var currentContext = nlapiGetContext();
	var currentUserID = currentContext.getUser();

	var Opentasktransaction = nlapiLoadRecord('customrecord_ebiznet_trn_opentask', RcId);

	vname = Opentasktransaction.getFieldValue('name');
	vCompany1 = Opentasktransaction.getFieldValue('custrecord_comp_id');
	vContainer = Opentasktransaction.getFieldValue('custrecord_container');
	vControlno = Opentasktransaction.getFieldValue('custrecord_ebiz_cntrl_no');
	vContainerLPno = Opentasktransaction.getFieldValue('custrecord_container_lp_no');
	vItemno = Opentasktransaction.getFieldValue('custrecord_sku');
	vEbizItemNo = Opentasktransaction.getFieldValue('custrecord_ebiz_sku_no');
	vEbizWaveNo1 = Opentasktransaction.getFieldValue('custrecord_ebiz_wave_no');
	vLineno1 = Opentasktransaction.getFieldValue('custrecord_line_no');
	vlpno1 = Opentasktransaction.getFieldValue('custrecord_lpno');
	vpackcode1 = Opentasktransaction.getFieldValue('custrecord_packcode');
	vSkuStatus1 = Opentasktransaction.getFieldValue('custrecord_sku_status');
	vTaskType1 = Opentasktransaction.getFieldValue('custrecord_tasktype');
	vBeginLoc1 = Opentasktransaction.getFieldValue('custrecord_actbeginloc');
	vUOMLevel1 = Opentasktransaction.getFieldValue('custrecord_uom_level');
	vEbizReceiptNo1 = Opentasktransaction.getFieldValue('custrecord_ebiz_receipt_no');
	vInvRefNo1 = Opentasktransaction.getFieldValue('custrecord_invref_no');
	vFromLP1 = Opentasktransaction.getFieldValue('custrecord_from_lp_no'); 
	vEbizOrdNo1 = Opentasktransaction.getFieldValue('custrecord_ebiz_order_no');
	vWMSLocation1 = Opentasktransaction.getFieldValue('custrecord_wms_location');
	vEbizRule1 = Opentasktransaction.getFieldValue('custrecord_ebizrule_no');
	vEbizZone1 = Opentasktransaction.getFieldValue('custrecord_ebizzone_no');
	vEbizUser1 = Opentasktransaction.getFieldValue('custrecord_ebizuser');
	vLot = Opentasktransaction.getFieldValue('custrecord_batch_no');

	nlapiLogExecution('ERROR', 'pickqty', pickqty);
	nlapiLogExecution('ERROR', 'vDisplayedQty', vDisplayedQty);
	nlapiLogExecution('ERROR', 'remainqty', remainqty);
	vRemaningqty = (parseFloat(pickqty)+parseFloat(remainqty))-parseFloat(vDisplayedQty);

	var vRemPickQty = parseFloat(vDisplayedQty)- parseFloat(pickqty);

	var PickExpOpenTaskRec=nlapiCreateRecord('customrecord_ebiznet_trn_opentask');

	PickExpOpenTaskRec.setFieldValue('name', vname);
	PickExpOpenTaskRec.setFieldValue('custrecordact_begin_date', DateStamp());
	PickExpOpenTaskRec.setFieldValue('custrecord_comp_id', vCompany1);
	PickExpOpenTaskRec.setFieldValue('custrecord_container', vContainer);
	PickExpOpenTaskRec.setFieldValue('custrecord_current_date', DateStamp());
	PickExpOpenTaskRec.setFieldValue('custrecord_ebiz_cntrl_no', vControlno);
	PickExpOpenTaskRec.setFieldValue('custrecord_container_lp_no', vContainerLPno);
	PickExpOpenTaskRec.setFieldValue('custrecord_sku', vItemno);
	PickExpOpenTaskRec.setFieldValue('custrecord_ebiz_sku_no', vEbizItemNo);
	PickExpOpenTaskRec.setFieldValue('custrecord_ebiz_wave_no', vEbizWaveNo1);
	PickExpOpenTaskRec.setFieldValue('custrecord_expe_qty', parseFloat(vRemPickQty).toFixed(4));
	PickExpOpenTaskRec.setFieldValue('custrecord_line_no', vLineno1);
	PickExpOpenTaskRec.setFieldValue('custrecord_lpno', vlpno1);
	PickExpOpenTaskRec.setFieldValue('custrecord_packcode', vpackcode1);
	PickExpOpenTaskRec.setFieldValue('custrecord_sku_status', vSkuStatus1);
	PickExpOpenTaskRec.setFieldValue('custrecord_wms_status_flag', '9'); //Status Flag - Short Pick 
	PickExpOpenTaskRec.setFieldValue('custrecord_tasktype', vTaskType1);
	PickExpOpenTaskRec.setFieldValue('custrecord_totalcube', '');
	PickExpOpenTaskRec.setFieldValue('custrecord_total_weight', '');
	PickExpOpenTaskRec.setFieldValue('custrecord_ebizzone_no', vEbizZone1);
	PickExpOpenTaskRec.setFieldValue('custrecord_uom_level', vUOMLevel1);
	PickExpOpenTaskRec.setFieldValue('custrecord_ebiz_receipt_no', vEbizReceiptNo1);
	PickExpOpenTaskRec.setFieldValue('custrecord_actualbegintime', TimeStamp());
	PickExpOpenTaskRec.setFieldValue('custrecord_invref_no', vInvRefNo1);
	PickExpOpenTaskRec.setFieldValue('custrecord_from_lp_no', vFromLP1);
	PickExpOpenTaskRec.setFieldValue('custrecord_ebiz_order_no', vEbizOrdNo1);
	PickExpOpenTaskRec.setFieldValue('custrecord_wms_location', vWMSLocation1);
	//PickExpOpenTaskRec.setFieldValue('custrecord_ebizrule_no', vEbizRule1);
	//PickExpOpenTaskRec.setFieldValue('custrecord_ebizmethod_no', vEbizMethod1);
	PickExpOpenTaskRec.setFieldValue('custrecord_ebizuser', currentUserID);					
	PickExpOpenTaskRec.setFieldValue('custrecord_upd_ebiz_user_no',currentUserID);
	PickExpOpenTaskRec.setFieldValue('custrecord_actbeginloc',vBeginLoc1);
	PickExpOpenTaskRec.setFieldValue('custrecord_batch_no',vLot);

	var vemployee = request.getParameter('custpage_employee');

	if (vemployee != null && vemployee != "") 
	{
		PickExpOpenTaskRec.setFieldValue('custrecord_taskassignedto',vemployee);
	} 
	else 
	{
		PickExpOpenTaskRec.setFieldValue('custrecord_taskassignedto',currentUserID);
	}
	nlapiLogExecution('ERROR', 'before CreateNewShortPickOpenTask : ', 'before CreateNewShortPickOpenTask');
	var vnotes1="This is from Confirm picks qty Exception";
	nlapiSubmitRecord(PickExpOpenTaskRec, false, true);

//	var getfulfillrecid = nlapiSubmitRecord(PickExpFulfillOrdLineRec, false, true);
}


/**
 * This function creats outbound staging inventory
 * @param invtrecid
 * @param vContLp
 * @param vqty
 */

function CreateSTGInvtRecord(invtrecid, vContLp,vqty,vebizordno) 
{
	nlapiLogExecution('ERROR', 'Into CreateSTGInvtRecord (Container LP)',vContLp);

	try
	{
		var stgmInvtRec = nlapiCopyRecord('customrecord_ebiznet_createinv',invtrecid);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_lp', vContLp);
		stgmInvtRec.setFieldValue('custrecord_ebiz_inv_qty', parseFloat(vqty).toFixed(4));
		stgmInvtRec.setFieldValue('custrecord_ebiz_qoh', parseFloat(vqty).toFixed(4));
		stgmInvtRec.setFieldValue('custrecord_ebiz_alloc_qty', '0');
		stgmInvtRec.setFieldValue('custrecord_wms_inv_status_flag', '36');	//FLAG.INVENTORY.WIP	
		stgmInvtRec.setFieldValue('custrecord_invttasktype', '3'); //Task Type - PICK
		stgmInvtRec.setFieldValue('custrecord_ebiz_callinv', 'N');
		stgmInvtRec.setFieldValue('custrecord_ebiz_displayfield', 'N');				
		if(vebizordno!=null && vebizordno!='')
			stgmInvtRec.setFieldValue('custrecord_ebiz_transaction_no', vebizordno);	

		nlapiSubmitRecord(stgmInvtRec, false, true);
	}
	catch(exp)
	{
		nlapiLogExecution('ERROR', 'Exception in CreateSTGInvtRecord',exp);
	}

	nlapiLogExecution('ERROR', 'Out of CreateSTGInvtRecord');
}

function buildTaskArray(WaveNo,ClusNo,OrdName,ebizOrdNo,vZoneId,vPickType)
{
	var SOarray = new Array();
	var SOFilters = new Array();
	SOFilters.push(new nlobjSearchFilter('custrecord_wms_status_flag', null, 'anyof', [9]));

	if((vPickType.indexOf('W') != -1 || vPickType=='ALL') && WaveNo != null && WaveNo != "")
	{
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_wave_no', null, 'is', parseFloat(WaveNo)));
	}
	if(ClusNo!= null && ClusNo!="" && ClusNo!= "null")
	{
		nlapiLogExecution('ERROR', 'ClusNo inside If', ClusNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_clus_no', null, 'is', ClusNo));
	}
	if((vPickType.indexOf('O') != -1 || vPickType=='ALL') && OrdName!=null && OrdName!="" && OrdName!= "null")
	{
		nlapiLogExecution('ERROR', 'OrdNo inside If', OrdName);
		SOFilters.push(new nlobjSearchFilter('name', null, 'is', OrdName));
	}
	if((vPickType.indexOf('S') != -1 || vPickType=='ALL') && ebizOrdNo!=null && ebizOrdNo!="" && ebizOrdNo!= "null")
	{
		nlapiLogExecution('ERROR', 'SO Id inside If', ebizOrdNo);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_order_no', null, 'anyof', ebizOrdNo));
	}
	if((vPickType.indexOf('Z') != -1 || vPickType=='ALL') && vZoneId!=null && vZoneId!="" && vZoneId!= "null")
	{
		nlapiLogExecution('ERROR', 'Zone # inside If', vZoneId);
		SOFilters.push(new nlobjSearchFilter('custrecord_ebiz_zoneid', null, 'anyof', vZoneId));			
	}

	var SOColumns = new Array();// Case# 20148354 

	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_order_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_skiptask'));
	SOColumns.push(new nlobjSearchColumn('custrecord_bin_locgroup_seq'));
	SOColumns.push(new nlobjSearchColumn('custrecord_startingpickseqno','custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_sku'));
	SOColumns.push(new nlobjSearchColumn('custrecord_expe_qty'));
	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));		
	SOColumns.push(new nlobjSearchColumn('custrecord_skudesc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_sku_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_cntrl_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_invref_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_line_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_actbeginloc'));
	SOColumns.push(new nlobjSearchColumn('custrecord_batch_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_wms_location'));
	SOColumns.push(new nlobjSearchColumn('custrecord_comp_id'));
	SOColumns.push(new nlobjSearchColumn('name'));
	SOColumns.push(new nlobjSearchColumn('custrecord_container'));				
	SOColumns.push(new nlobjSearchColumn('custrecord_ebizzone_no'));
	SOColumns.push(new nlobjSearchColumn('custrecord_lpno'));
	SOColumns.push(new nlobjSearchColumn('custrecord_ebiz_zoneid'));

	SOColumns[0].setSort();
	SOColumns[1].setSort();
	SOColumns[2].setSort();
	SOColumns[3].setSort();
	SOColumns[4].setSort();
	SOColumns[5].setSort(true);


	SOarray["custparam_ebizordno"] = ebizOrdNo;

	var SOSearchResults = nlapiSearchRecord('customrecord_ebiznet_trn_opentask', null, SOFilters, SOColumns);

	if (SOSearchResults != null && SOSearchResults.length > 0) {

		var SOSearchResult = SOSearchResults[0];
		if(SOSearchResults.length > 1)
		{
			var SOSearchnextResult = SOSearchResults[1];
			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			nlapiLogExecution('ERROR', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
			nlapiLogExecution('ERROR', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
		}
		else
		{
			var SOSearchnextResult = SOSearchResults[0];
			SOarray["custparam_nextlocation"] = SOSearchnextResult.getText('custrecord_actbeginloc');
			SOarray["custparam_nextiteminternalid"] = SOSearchnextResult.getValue('custrecord_ebiz_sku_no');
			SOarray["custparam_nextexpectedquantity"] = SOSearchnextResult.getValue('custrecord_expe_qty');
			nlapiLogExecution('ERROR', 'Next Location', SOSearchnextResult.getText('custrecord_actbeginloc'));
			nlapiLogExecution('ERROR', 'Next Item Intr Id', SOSearchnextResult.getValue('custrecord_ebiz_sku_no'));
		}

		SOarray["custparam_waveno"] = WaveNo;
		SOarray["custparam_recordinternalid"] = SOSearchResult.getId();
		SOarray["custparam_containerlpno"] = SOSearchResult.getValue('custrecord_lpno');
		SOarray["custparam_expectedquantity"] = SOSearchResult.getValue('custrecord_expe_qty');
		SOarray["custparam_beginLocation"] = SOSearchResult.getValue('custrecord_actbeginloc');
		SOarray["custparam_endlocinternalid"] = SOSearchResult.getValue('custrecord_actbeginloc');		
		SOarray["custparam_item"] = SOSearchResult.getValue('custrecord_sku');
		SOarray["custparam_itemname"] = SOSearchResult.getText('custrecord_sku');
		SOarray["custparam_itemdescription"] = SOSearchResult.getValue('custrecord_skudesc');
		SOarray["custparam_iteminternalid"] = SOSearchResult.getValue('custrecord_ebiz_sku_no');
		SOarray["custparam_dolineid"] = SOSearchResult.getValue('custrecord_ebiz_cntrl_no');
		SOarray["custparam_invoicerefno"] = SOSearchResult.getValue('custrecord_invref_no');
		SOarray["custparam_orderlineno"] = SOSearchResult.getValue('custrecord_line_no');
		SOarray["custparam_beginLocationname"] = SOSearchResult.getText('custrecord_actbeginloc');
		SOarray["custparam_batchno"] = SOSearchResult.getValue('custrecord_batch_no');
		SOarray["custparam_whlocation"] = SOSearchResult.getValue('custrecord_wms_location');
		SOarray["custparam_whcompany"] = SOSearchResult.getValue('custrecord_comp_id');
		SOarray["custparam_noofrecords"] = SOSearchResults.length;		
		SOarray["name"] =  SOSearchResult.getValue('name');
		SOarray["custparam_containersize"] =  SOSearchResult.getValue('custrecord_container');
		SOarray["custparam_ebizordno"] =  SOSearchResult.getValue('custrecord_ebiz_order_no');
		SOarray["custparam_picktype"] =vPickType;
		if(vZoneId!=null && vZoneId!="")
		{
			SOarray["custparam_ebizzoneno"] =  SOSearchResult.getValue('custrecord_ebizzone_no');
		}
		else
			SOarray["custparam_ebizzoneno"] = '';
	}

	return SOarray;
}
