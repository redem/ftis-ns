/***************************************************************************
                                                                 
                                                  eBizNET Solutions Inc 
****************************************************************************
*
*  $Source: /cvs/products/NSWMS/EoN/WMS_UI/Transactions/Inventory/Client/Attic/ebiz_CycleCount_CL.js,v $
*  $Revision: 1.1.2.11.2.1 $
*  $Date: 2015/11/10 17:00:05 $
*  $Author: sponnaganti $
*  $Name: t_WMS_2015_2_StdBundle_1_112 $
*
* DESCRIPTION
*  Functionality
*
* REVISION HISTORY
*  $Log: ebiz_CycleCount_CL.js,v $
*  Revision 1.1.2.11.2.1  2015/11/10 17:00:05  sponnaganti
*  case# 201415521
*  2015.2 issue fix
*
*  Revision 1.1.2.11  2014/06/27 07:51:20  skavuri
*  Case# 20149062 Compatability Issue Fixed
*
*  Revision 1.1.2.10  2014/06/11 15:29:42  sponnaganti
*  case# 20148823
*  Stnd Bundle Issue Fix
*
*  Revision 1.1.2.9  2014/04/21 16:06:22  skavuri
*  Case # 20148095 SB Issue fixed
*
*  Revision 1.1.2.8  2014/03/14 15:26:44  nneelam
*  case#  20126201
*  Cycle Count Report
*
*  Revision 1.1.2.7  2014/02/24 15:39:11  sponnaganti
*  case# 20127266
*  (vClosed=='T' is replaced with vClosed =='F')
*
*  Revision 1.1.2.6  2013/10/25 16:11:01  skreddy
*  Case# 20125324
*  standard bundle  issue fix
*
*  Revision 1.1.2.5  2013/09/12 15:37:59  nneelam
*  Case#. 20124018
*  Alert msg when location is not selected Issue Fix..
*
*  Revision 1.1.2.4  2013/09/11 15:23:51  rmukkera
*  Case# 20124376
*
*
****************************************************************************/
//Case# 20149062 starts
var eventtype;
function  myPageInit(type){
	QtyOldValue = nlapiGetFieldValue('custrecord_ebiz_inv_qty');
	eventtype=type;
}
//Case# 20149062 ends
function ebiznet_CycleCount_CL(fld){

	var item = nlapiGetFieldValue('custrecord_ebiz_sku');
	var binlocation = nlapiGetFieldValue('custrecord_ebiz_cycl_binloc');
	var lp = nlapiGetFieldValue('custrecord_cycl_lp');	
	var Closed = nlapiGetFieldValue('custrecord_cyccplan_close');
	var itemfamily=nlapiGetFieldValue('custrecord_ebiz_skufamily');
	var itemgroup=nlapiGetFieldValue('custrecord_ebiz_skugroup');
	var binlocgrp=nlapiGetFieldValue('custrecord_ebiz_locationgroup');
	var packcode=nlapiGetFieldValue('custrecord_ebiz_cycl_packcode');
	var batch=nlapiGetFieldValue('custrecord_ebiz_batch');
	var aisle=nlapiGetFieldValue('custrecord_ebiz_aisle');
	var level=nlapiGetFieldValue('custrecord_ebiz_level');
	var comp=nlapiGetFieldValue('custrecord_cycle_count_company');
	
	var itemValue=nlapiGetFieldValue('custrecord_ebiz_sku_multisel');
	var gitemArray = new Array();
	var itemArray = new Array();
	if(itemValue != '' && itemValue != null)
		gitemArray = itemValue.split('');

	if(gitemArray!=null)
	{
		for(var x=0; x< gitemArray.length;x++)
		{
			//alert('gitemArray[x] :' +gitemArray[x]);
			itemArray.push(gitemArray[x]);
		}
	}


	//case # 20124018 Start
	var loc=nlapiGetFieldValue('custrecord_ebiz_cyclecntplan_location');
	if(loc==null || loc==''){
		alert('Please enter value for Location');
		return false;
	}
	//case # 20124018 End

	var filters = new Array();
	var Columns = new Array();
	if(loc !=null && loc !='')
	{
		filters.push(new nlobjSearchFilter('custrecord_ebiz_cyclecntplan_location',null,'anyof',[loc]));
	}
	if(item != '' && item != null){
		filters.push(new nlobjSearchFilter('custrecord_ebiz_sku',null,'anyof',[item]));
	}

	if(binlocation != "" && binlocation != null ){
		filters.push(new nlobjSearchFilter('custrecord_ebiz_cycl_binloc',null,'anyof',[binlocation]));
	}
	
	if(lp != '' && lp != null){
		filters.push(new nlobjSearchFilter('custrecord_cycl_lp',null,'anyof',[lp]));
	}
	//case 20125324 start : Added filter conditions 
	if(itemfamily != '' && itemfamily != null){
		filters.push(new nlobjSearchFilter('custrecord_ebiz_skufamily',null,'anyof',[itemfamily]));
	}
	if(itemgroup != '' && itemgroup != null){
		filters.push(new nlobjSearchFilter('custrecord_ebiz_skugroup',null,'anyof',[itemgroup]));
	}
	if(binlocgrp != '' && binlocgrp != null){
		filters.push(new nlobjSearchFilter('custrecord_ebiz_locationgroup',null,'anyof',[binlocgrp]));
	}
	if(packcode != '' && packcode != null){
		filters.push(new nlobjSearchFilter('custrecord_ebiz_cycl_packcode',null,'anyof',[packcode]));
	}
	if(batch != '' && batch != null){
		filters.push(new nlobjSearchFilter('custrecord_ebiz_batch',null,'anyof',[batch]));
	}
	if(aisle != '' && aisle != null){
		filters.push(new nlobjSearchFilter('custrecord_ebiz_aisle',null,'is',[aisle]));
	}
	if(level != '' && level != null){
		filters.push(new nlobjSearchFilter('custrecord_ebiz_level',null,'is',[level]));
	}
	if(comp != '' && comp != null){
		filters.push(new nlobjSearchFilter('custrecord_cycle_count_company',null,'anyof',[comp]));
	}
	if(itemArray != '' && itemArray != null){
		filters.push(new nlobjSearchFilter('custrecord_ebiz_sku_multisel',null,'anyof',itemArray));
	}
	//case 20125324 end 
	Columns[0] = new nlobjSearchColumn('custrecord_cyccplan_close');   
	Columns[1] = new nlobjSearchColumn('custrecord_cycl_lp');  

	var CycleCountResults = nlapiSearchRecord('customrecord_ebiznet_cylc_createplan', null, filters, Columns);

	if(CycleCountResults != null && CycleCountResults != '')
	{

		//for (var j = 0; j < CycleCountResults.length; j++) {
			
			//Case# 20123251 start
			//alert("CycleCountResults :" + CycleCountResults.length);
			//Case# 20123251 end

			var vClosed=CycleCountResults[0].getValue('custrecord_cyccplan_close');
			var vlp = CycleCountResults[0].getValue('custrecord_cycl_lp');
			//alert("vClosed :" + vClosed);
			//alert("Closed :" + Closed);
			
			//case# 20127266 starts(vClosed=='T' is replaced with vClosed =='F')
			// Case# 20148095 starts vClosed =='F' is replaced with vClosed=='T')
			// Case# 20149062 starts
			if(Closed=='F')
			{
				if(vClosed =='F')
				{
				if(eventtype!='edit')
					{
					alert("Plan Already Exists");
					return false;	

				}
				else
				{
					return true;				
				}		
			}
			else
				{
				return true;
				}		
				}
			else
			{
				return true;				
			}		
			//case# 20127266 end
			// Case# 20149062 ends
	//	}


	}
	else
	{
		return true;
	}



}
function Printreportpdf(){
	
	var varCycleCountPlanNo = nlapiGetFieldValue('custpage_cyclecountplan');
	nlapiLogExecution('ERROR', 'varCycleCountPlanNo',varCycleCountPlanNo);
	var CycWhLocation = nlapiGetFieldValue('custpage_cyclecountwhlocation');
	var Cycitem=nlapiGetFieldValue('custpage_cyclecountitem');
	
	var CycbinLoc=nlapiGetFieldValue('custpage_cyclecountbinloc');
	
	var queryparams=nlapiGetFieldValue('custpage_qeryparams');
	
	var hiddenfieldselectpage=nlapiGetFieldValue('custpage_hiddenfieldselectpage');
	
	var hiddenfieldshow=nlapiGetFieldValue('custpage_hiddenfieldshow');
	var radiobtnflag=nlapiGetFieldValue('restypeflag');
	
	
	

	var CycPDFURL = nlapiResolveURL('SUITELET','customscript_ebiz_cyclecountreportpdf','customdeploy_ebiz_cyclecountreportpdf_di');
	//alert(CycPDFURL);
	nlapiLogExecution('ERROR', 'CycPDFURL',CycPDFURL);					
	CycPDFURL = CycPDFURL + '&custparam_cyclecountplan='+ varCycleCountPlanNo+'&custparam_cyclecountwhlocation='+CycWhLocation+'&custparam_cyclecountitem='+Cycitem+'&custparam_cyclecountbinloc='+CycbinLoc+'&custparam_qeryparams='+queryparams+'&custparam_hiddenfieldselectpage='+hiddenfieldselectpage+'&custparam_hiddenfieldshow='+hiddenfieldshow+'&custparam_restypeflag='+radiobtnflag;
	nlapiLogExecution('ERROR', 'CycPDFURL',CycPDFURL);
	//alert(CycPDFURL);
	window.open(CycPDFURL);

}
