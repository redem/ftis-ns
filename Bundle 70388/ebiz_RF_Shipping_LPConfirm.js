/***************************************************************************
  eBizNET Solutions Inc
 ****************************************************************************/
/* 
 ****************************************************************************
 *
 *     	   $Source: /cvs/products/eBizNET4.5/EoN/RF/Outbound/Suitelet/ebiz_RF_Shipping_LPConfirm.js,v $
 *     	   $Revision: 1.2.4.2.4.3.2.6 $
 *     	   $Date: 2014/06/13 11:03:11 $
 *     	   $Author: skavuri $
 *     	   $Name: t_NSWMS_2014_1_3_125 $
 *
 *   eBizNET version and checksum stamp.  Do not remove.
 *   $eBiznet_VER: .............. $eBizNET_SUM: .....
 * PRAMETERS
 *
 *
 * DESCRIPTION
 *
 *  	Default Data for Interfaces
 *
 * NOTES AND WARNINGS
 *
 * INITATED FROM
 *
 * REVISION HISTORY
 * $Log: ebiz_RF_Shipping_LPConfirm.js,v $
 * Revision 1.2.4.2.4.3.2.6  2014/06/13 11:03:11  skavuri
 * Case# 20148882 (added Focus Functionality for Textbox)
 *
 * Revision 1.2.4.2.4.3.2.5  2014/06/06 07:00:01  skavuri
 * Case# 20148749 (Refresh ( F5 Button) Functionality ) SB Issue Fixed
 *
 * Revision 1.2.4.2.4.3.2.4  2014/05/30 00:41:06  nneelam
 * case#  20148622
 * Stanadard Bundle Issue Fix.
 *
 * Revision 1.2.4.2.4.3.2.3  2013/06/11 14:30:20  schepuri
 * Error Code Change ERROR to DEBUG
 *
 * Revision 1.2.4.2.4.3.2.2  2013/04/18 07:30:11  skreddy
 * CASE201112/CR201113/LOG201121
 * added meta tag
 *
 * Revision 1.2.4.2.4.3.2.1  2013/03/05 14:47:50  skreddy
 * CASE201112/CR201113/LOG201121
 * Code merged from Lexjet production as part of Standard bundle
 *
 * Revision 1.2.4.2.4.3  2013/02/15 14:59:20  schepuri
 * CASE201112/CR201113/LOG201121
 * disabling ENTER Button func added
 *
 * Revision 1.2.4.2.4.2  2012/12/24 13:22:06  schepuri
 * CASE201112/CR201113/LOG201121
 * added new screen for multiple sites
 *
 * Revision 1.2.4.2.4.1  2012/09/25 07:13:53  grao
 * CASE201112/CR201113/LOG201121
 * Converting Multi Language
 *
 * Revision 1.2.4.2  2012/03/19 13:28:12  spendyala
 * CASE201112/CR201113/LOG201121
 * Disable-button functionality is been added.
 *
 * Revision 1.2.4.1  2012/02/22 13:02:16  schepuri
 * CASE201112/CR201113/LOG201121
 * function Key Script code merged
 *
 * Revision 1.2  2011/10/03 08:25:05  snimmakayala
 * CASE201112/CR201113/LOG20112
 *
 * Revision 1.1  2011/09/23 16:22:25  gkalla
 * CASE201112/CR201113/LOG201121
 *
 * Revision 1.4  2011/04/25 12:13:13  pattili
 * CASE201112/CR201113/LOG201121
 * Set the focus to the first control on the screen after load.
 *
 * Revision 1.3  2011/04/21 07:42:42  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * RF Packing Functionality
 *
 * Revision 1.2  2011/04/21 07:03:02  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * RF Packing Functionality
 *
 * Revision 1.1  2011/04/19 14:19:06  kpmalleswarapu
 * CASE201112/CR201113/LOG201121
 * RF Packing Functionality
 *
 *
 *****************************************************************************/
function ShipLPConfirm(request, response)
{
	var matchFound = true;
	if (request.getMethod() == 'GET') 
	{    
		var whLocation = request.getParameter('custparam_locationId');
		var getShipLP = request.getParameter('custparam_shiplpno');
		if(getShipLP==null || getShipLP =="")
		{
			var getLanguage = request.getParameter('custparam_language');	
		    nlapiLogExecution('DEBUG', 'getLanguage', getLanguage);
		    
			var st0,st1,st2,st3;
			if( getLanguage == 'es_ES')
			{
				st0 = "";
				st1 = "&#191;QUIERES CREAR EL NUEVO BUQUE LP?";
				st2 = "SI";
				st3 = "NO";
				
		  	}
			else
			{
				st0 = "";
				st1 = "DO YOU WANT TO CREATE THE NEW SHIP LP?";
				st2 = "YES";
				st3 = "NO";
			}    	
			var functionkeyHtml=getFunctionkeyScript('_rf_cluster_no'); 
			var html = "<html><head><title>" + st0 + " </title>";
			html = html + "<meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'>";
			html = html + "<SCRIPT LANGUAGE='javascript' for='window' EVENT='onload()'>";       
			//Case# 20148749 Refresh Functionality starts
			html = html + "var version = navigator.appVersion;";
			html = html + "document.onkeydown = function (e) {";
			html = html + "var keycode = (window.event) ? event.keyCode : e.keyCode;"; 
			html = html + "if ((version.indexOf('MSIE') != -1)) { ";
			html = html + "	if (keycode == 116) {event.keyCode = 0;event.returnValue = false;return false;}}"; 
			html = html + "else {if (keycode == 116)return false;}";
			html = html + "};";
			//Case# 20148749 Refresh Functionality ends
			//html = html + " document.getElementById('cmdSend').focus();";        
			html = html + "function stopRKey(evt) { ";
			//html = html + "	  alert('evt');";
			html = html + "	  var evt = (evt) ? evt : ((event) ? event : null); ";
			html = html + "	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);"; 
			html = html + "	  if ((evt.keyCode == 13) && ((node.type=='text') || (node.type=='submit'))){";
			//html = html + "	  alert(document.getElementById('cmdSend').disabled);";
			
			html = html + "	  if(document.getElementById('cmdSend').disabled==true){";
			html = html + "	  alert('System Processing, Please wait...');";
			html = html + "	  return false;}} ";
			html = html + "	} ";

			html = html + "	document.onkeypress = stopRKey; ";
			html = html + "</script>";        
			html = html +functionkeyHtml;
			html = html + "</head><body onkeydown='return OnKeyDown_CL();'>";
			html = html + "	<form name='_rf_cluster_no' method='POST'>";
			html = html + "		<table>";
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>"+ st1;	 
			html = html + "				<input type='hidden' name='hdngetLanguage' value=" + getLanguage + ">";
			html = html + "				<input type='hidden' name='hdnwhLocation' value=" + whLocation + ">";
			html = html + "				</td>";
			html = html + "			</tr>"; 
			html = html + "			<tr>";
			html = html + "				<td align = 'left'>" + st2 + " <input name='cmdSend' id='cmdSend' type='submit' value='ENT' onclick='this.form.submit();this.disabled=true;this.form.cmdPrevious.disabled=true; return false'/>";
			html = html + "					" + st3 + " <input name='cmdPrevious' type='submit' value='F7'/>";        
			html = html + "				</td>";
			html = html + "			</tr>";
			html = html + "		 </table>";
			html = html + "	</form>";
			//Case# 20148882 (added Focus Functionality for Textbox)
			html = html + "<script type='text/javascript'>document.getElementById('cmdSend').focus();</script>";
			html = html + "</body>";
			html = html + "</html>";

			response.write(html);
		}
	}
	else 
	{
		nlapiLogExecution('DEBUG', 'Into Response', 'Into Response');
		var whLocation = request.getParameter('hdnwhLocation');
		nlapiLogExecution('DEBUG', 'whLocation', whLocation);
		var SOarray =new Array();
		SOarray["custparam_locationId"]=whLocation;
		// This variable is to get the value when the previous 'F7' button is clicked, in order to navigate
		// to the previous screen.
		var getLanguage = request.getParameter('hdngetLanguage');
		SOarray["custparam_language"] = getLanguage;
		nlapiLogExecution('DEBUG', 'getLanguage', SOarray["custparam_language"]); 
		var optedEvent = request.getParameter('cmdPrevious');       

		//	if the previous button 'F7' is clicked, it has to go to the previous screen 
		//  ie., it has to go to Shipping menu.
		if (optedEvent == 'F7') {
			//response.sendRedirect('SUITELET', 'customscript_rf_picking_container_no', 'customdeploy_rf_picking_container_no_di', false, SOarray);
			response.sendRedirect('SUITELET', 'customscript_rf_shipping_shiplp', 'customdeploy_rf_shipping_shiplp_di', false, SOarray);
		}
		else // TO create Auto generated SHIP LP#
		{      
			nlapiLogExecution('DEBUG', 'Entering to CartonLP ', '');
			var AutoShipLPNo;
			var AutoShipLPNo=getAutoShipLP(whLocation);
			//AutoShipLPNo='SHIP1001';
			SOarray["custparam_shiplpno"] = AutoShipLPNo;

			response.sendRedirect('SUITELET', 'customscript_rf_shipping_cartonlp', 'customdeploy_rf_shipping_cartonlp_di', false, SOarray);
			return;
		}
	}
}

/**
 * Function to get auto generated Ship LP# from lp range
 *  
 * @returns Auto generated ShipLP
 * 
 */
function getAutoShipLP(whLocation)
{
	var AutoShipLPNo="1";	 
	AutoShipLPNo=GetMaxLPNo('1', '3',whLocation);// for Auto generated with SHIP LPType
	return AutoShipLPNo;
}

